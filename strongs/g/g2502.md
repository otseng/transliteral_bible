# Ἰωσίας

[Iōsias](https://www.blueletterbible.org/lexicon/g2502)

Definition: Josias (2x), "whom Jehovah heals", king of Judah, who restored among the Jews the worship of the true God, and after a reign of thirty one years was slain in battle about 611 BC

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/2502.html)

[LSJ](https://lsj.gr/wiki/Ἰωσίας)

[NET Bible](http://classic.net.bible.org/strong.php?id=2502)

[Bible Bento](https://biblebento.com/dictionary/G2502.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2502/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2502.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2502)

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2502)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/josias.html)