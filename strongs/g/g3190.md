# Μελεά

[Melea](https://www.blueletterbible.org/lexicon/g3190)

Definition: Melea (1x), "my dear friend: object of care", the son of Joseph in the genealogy of Christ

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/3190.html)

[LSJ](https://lsj.gr/wiki/Μελεά)

[NET Bible](http://classic.net.bible.org/strong.php?id=3190)

[Bible Bento](https://biblebento.com/dictionary/G3190.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3190/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3190.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3190)

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3190)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/melea.html)

[Video Bible](https://www.videobible.com/bible-dictionary/melea)