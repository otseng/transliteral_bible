# ἄροτρον

[arotron](https://www.blueletterbible.org/lexicon/g723)

Definition: plough (1x), plow

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0723)

[Study Light](https://www.studylight.org/lexicons/greek/723.html)

[Bible Hub](https://biblehub.com/str/greek/723.htm)

[LSJ](https://lsj.gr/wiki/ἄροτρον)

[NET Bible](http://classic.net.bible.org/strong.php?id=723)

[Bible Bento](https://biblebento.com/dictionary/G0723.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0723/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0723.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g723)