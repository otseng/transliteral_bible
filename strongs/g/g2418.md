# ἱερουργέω

[hierourgeō](https://www.blueletterbible.org/lexicon/g2418)

Definition: minister (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2418)

[Study Light](https://www.studylight.org/lexicons/greek/2418.html)

[Bible Hub](https://biblehub.com/str/greek/2418.htm)

[LSJ](https://lsj.gr/wiki/ἱερουργέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2418)

[Bible Bento](https://biblebento.com/dictionary/G2418.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2418/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2418.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2418)

