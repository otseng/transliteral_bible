# γυνή

[gynē](https://www.blueletterbible.org/lexicon/g1135)

Definition: women (129x), wife (92x), female, virgin, widow

Part of speech: feminine noun

Occurs 221 times in 200 verses

Hebrew: ['ishshah](../h/h802.md), [naʿărâ](../h/h5291.md)

Derived words: gynecology

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1135)

[Study Light](https://www.studylight.org/lexicons/greek/1135.html)

[Bible Hub](https://biblehub.com/str/greek/1135.htm)

[LSJ](https://lsj.gr/wiki/γυνή)

[NET Bible](http://classic.net.bible.org/strong.php?id=1135)

[Bible Bento](https://biblebento.com/dictionary/G1135.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1135/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1135.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1135)