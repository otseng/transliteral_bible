# καταφεύγω

[katapheugō](https://www.blueletterbible.org/lexicon/g2703)

Definition: flee (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2703)

[Study Light](https://www.studylight.org/lexicons/greek/2703.html)

[Bible Hub](https://biblehub.com/str/greek/2703.htm)

[LSJ](https://lsj.gr/wiki/καταφεύγω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2703)

[Bible Bento](https://biblebento.com/dictionary/G2703.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2703/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2703.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2703)
