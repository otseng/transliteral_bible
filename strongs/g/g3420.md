# μνήμη

[mnēmē](https://www.blueletterbible.org/lexicon/g3420)

Definition: remembrance (1x), memory

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3420)

[Study Light](https://www.studylight.org/lexicons/greek/3420.html)

[Bible Hub](https://biblehub.com/str/greek/3420.htm)

[LSJ](https://lsj.gr/wiki/μνήμη)

[NET Bible](http://classic.net.bible.org/strong.php?id=3420)

[Bible Bento](https://biblebento.com/dictionary/G3420.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3420/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3420.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3420)

