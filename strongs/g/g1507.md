# εἱλίσσω

[heilissō](https://www.blueletterbible.org/lexicon/g1507)

Definition: roll together (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1507)

[Study Light](https://www.studylight.org/lexicons/greek/1507.html)

[Bible Hub](https://biblehub.com/str/greek/1507.htm)

[LSJ](https://lsj.gr/wiki/εἱλίσσω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1507)

[Bible Bento](https://biblebento.com/dictionary/G1507.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1507/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1507.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1507)

