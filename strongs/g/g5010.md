# τάξις

[taxis](https://www.blueletterbible.org/lexicon/g5010)

Definition: order (10x), due or right order, orderly condition

Part of speech: feminine noun

Occurs 10 times in 9 verses

Derived words: taxidermy

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5010)

[Study Light](https://www.studylight.org/lexicons/greek/5010.html)

[Bible Hub](https://biblehub.com/str/greek/5010.htm)

[LSJ](https://lsj.gr/wiki/τάξις)

[NET Bible](http://classic.net.bible.org/strong.php?id=5010)

[Bible Bento](https://biblebento.com/dictionary/G5010.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5010/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5010.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5010)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%84%CE%AC%CE%BE%CE%B9%CF%82)

[Precept Austin](https://www.preceptaustin.org/colossians_24-71#gd)
