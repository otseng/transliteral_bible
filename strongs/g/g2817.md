# κληρονομία

[klēronomia](https://www.blueletterbible.org/lexicon/g2817)

Definition: inheritance (14x)

Part of speech: feminine noun

Occurs 14 times in 14 verses

Hebrew: [nachalah](../h/h5159.md), [gôrāl](../h/h1486.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2817)

[Study Light](https://www.studylight.org/lexicons/greek/2817.html)

[Bible Hub](https://biblehub.com/str/greek/2817.htm)

[LSJ](https://lsj.gr/wiki/κληρονομία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2817)

[Bible Bento](https://biblebento.com/dictionary/G2817.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2817/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2817.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2817)
