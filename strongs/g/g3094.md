# Μαγδαληνή

[Magdalēnē](https://www.blueletterbible.org/lexicon/g3094)

Definition: Magdalene (12x), "a tower"

Part of speech: proper feminine noun

Occurs 12 times in 12 verses

Greek: [Magadan][g3093.md]

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3094)

[Study Light](https://www.studylight.org/lexicons/greek/3094.html)

[Bible Hub](https://biblehub.com/str/greek/3094.htm)

[LSJ](https://lsj.gr/wiki/Μαγδαληνή)

[NET Bible](http://classic.net.bible.org/strong.php?id=3094)

[Bible Bento](https://biblebento.com/dictionary/G3094.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3094/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3094.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3094)

[Wikitionary](https://en.wiktionary.org/wiki/Magdalene)

[Wikipedia](https://en.wikipedia.org/wiki/Mary_Magdalene)