# ἐπαισχύνομαι

[epaischynomai](https://www.blueletterbible.org/lexicon/g1870)

Definition: be ashamed (11x), to feel shame for something, disgraced

Part of speech: verb

Occurs 21 times in 9 verses

Hebrew: [buwsh](../h/h954.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1870)

[Study Light](https://www.studylight.org/lexicons/greek/1870.html)

[Bible Hub](https://biblehub.com/str/greek/1870.htm)

[LSJ](https://lsj.gr/wiki/ἐπαισχύνομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1870)

[Bible Bento](https://biblebento.com/dictionary/G1870.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1870/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1870.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1870)