# τολμητής

[tolmētēs](https://www.blueletterbible.org/lexicon/g5113)

Definition: presumptuous (1x), audacious, daring

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5113)

[Study Light](https://www.studylight.org/lexicons/greek/5113.html)

[Bible Hub](https://biblehub.com/str/greek/5113.htm)

[LSJ](https://lsj.gr/wiki/τολμητής)

[NET Bible](http://classic.net.bible.org/strong.php?id=5113)

[Bible Bento](https://biblebento.com/dictionary/G5113.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5113/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5113.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5113)

