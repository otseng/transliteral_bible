# ἀπερίτμητος

[aperitmētos](https://www.blueletterbible.org/lexicon/g564)

Definition: uncircumcised (1x), those whose heart and ears are covered

Part of speech: adjective

Occurs 1 times in 1 verses

Hebrew: [ʿārēl](../h/h6189.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0564)

[Study Light](https://www.studylight.org/lexicons/greek/564.html)

[Bible Hub](https://biblehub.com/str/greek/564.htm)

[LSJ](https://lsj.gr/wiki/ἀπερίτμητος)

[NET Bible](http://classic.net.bible.org/strong.php?id=564)

[Bible Bento](https://biblebento.com/dictionary/G0564.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0564/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0564.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g564)
