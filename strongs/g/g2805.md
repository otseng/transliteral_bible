# κλαυθμός

[klauthmos](https://www.blueletterbible.org/lexicon/g2805)

Definition: weeping (6x), wailing (2x), weep (1x), lamentation

Part of speech: masculine noun

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2805)

[Study Light](https://www.studylight.org/lexicons/greek/2805.html)

[Bible Hub](https://biblehub.com/str/greek/2805.htm)

[LSJ](https://lsj.gr/wiki/κλαυθμός)

[NET Bible](http://classic.net.bible.org/strong.php?id=2805)

[Bible Bento](https://biblebento.com/dictionary/G2805.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2805/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2805.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2805)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=36068)