# αἰχμαλωτίζω

[aichmalōtizō](https://www.blueletterbible.org/lexicon/g163)

Definition: bring into captivity (2x), lead away captive (1x), to capture ones mind, captivate, take prisoner

Part of speech: verb

Occurs 4 times in 3 verses

Hebrew: [gālâ](../h/h1540.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0163)

[Study Light](https://www.studylight.org/lexicons/greek/163.html)

[Bible Hub](https://biblehub.com/str/greek/163.htm)

[LSJ](https://lsj.gr/wiki/αἰχμαλωτίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=163)

[Bible Bento](https://biblebento.com/dictionary/G0163.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0163/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0163.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g163)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CE%B9%CF%87%CE%BC%CE%B1%CE%BB%CF%89%CF%84%CE%AF%CE%B6%CF%89)