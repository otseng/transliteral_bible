# ἀπιστέω

[apisteō](https://www.blueletterbible.org/lexicon/g569)

Definition: believe not (7x), disbelieve

Part of speech: verb

Occurs 8 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0569)

[Study Light](https://www.studylight.org/lexicons/greek/569.html)

[Bible Hub](https://biblehub.com/str/greek/569.htm)

[LSJ](https://lsj.gr/wiki/ἀπιστέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=569)

[Bible Bento](https://biblebento.com/dictionary/G0569.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0569/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0569.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g569)