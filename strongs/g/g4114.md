# πλάτος

[platos](https://www.blueletterbible.org/lexicon/g4114)

Definition: breadth (4x), width

Part of speech: neuter noun

Occurs 4 times in 3 verses

Hebrew: [rōḥaḇ](../h/h7341.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4114)

[Study Light](https://www.studylight.org/lexicons/greek/4114.html)

[Bible Hub](https://biblehub.com/str/greek/4114.htm)

[LSJ](https://lsj.gr/wiki/πλάτος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4114)

[Bible Bento](https://biblebento.com/dictionary/G4114.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4114/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4114.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4114)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%BB%CE%AC%CF%84%CE%BF%CF%82)
