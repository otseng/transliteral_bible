# ὧδε

[hōde](https://www.blueletterbible.org/lexicon/g5602)

Definition: here (44x), hither (13x), in this place (1x), this place (1x), there (1x).

Part of speech: adverb

Occurs 60 times in 56 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5602)

[Study Light](https://www.studylight.org/lexicons/greek/5602.html)

[Bible Hub](https://biblehub.com/str/greek/5602.htm)

[LSJ](https://lsj.gr/wiki/ὧδε)

[NET Bible](http://classic.net.bible.org/strong.php?id=5602)

[Bible Bento](https://biblebento.com/dictionary/G5602.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5602/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5602.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5602)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BD%A7%CE%B4%CE%B5)