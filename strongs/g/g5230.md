# ὑπέρακμος

[hyperakmos](https://www.blueletterbible.org/lexicon/g5230)

Definition: pass the flower of (one's) age (with G5600) (1x), beyond the bloom or prime of life, overripe, plump and ripe

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5230)

[Study Light](https://www.studylight.org/lexicons/greek/5230.html)

[Bible Hub](https://biblehub.com/str/greek/5230.htm)

[LSJ](https://lsj.gr/wiki/ὑπέρακμος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5230)

[Bible Bento](https://biblebento.com/dictionary/G5230.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5230/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5230.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5230)
