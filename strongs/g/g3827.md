# πάμπολυς

[pampolys](https://www.blueletterbible.org/lexicon/g3827)

Definition: very great (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/3827.html)

[Bible Hub](https://biblehub.com/str/greek/3827.htm)

[LSJ](https://lsj.gr/wiki/πάμπολυς)

[NET Bible](http://classic.net.bible.org/strong.php?id=3827)

[Bible Bento](https://biblebento.com/dictionary/G3827.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3827/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3827.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3827)

