# ἱεροσυλέω

[hierosyleō](https://www.blueletterbible.org/lexicon/g2416)

Definition: commit sacrilege (1x), to rob a temple

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2416)

[Study Light](https://www.studylight.org/lexicons/greek/2416.html)

[Bible Hub](https://biblehub.com/str/greek/2416.htm)

[LSJ](https://lsj.gr/wiki/ἱεροσυλέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2416)

[Bible Bento](https://biblebento.com/dictionary/G2416.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2416/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2416.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2416)
