# νοσσός

[nossos](https://www.blueletterbible.org/lexicon/g3502)

Definition: young (1x), young bird

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3502)

[Study Light](https://www.studylight.org/lexicons/greek/3502.html)

[Bible Hub](https://biblehub.com/str/greek/3502.htm)

[LSJ](https://lsj.gr/wiki/νοσσός)

[NET Bible](http://classic.net.bible.org/strong.php?id=3502)

[Bible Bento](https://biblebento.com/dictionary/G3502.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3502/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3502.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3502)