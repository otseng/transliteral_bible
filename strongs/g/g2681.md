# κατασκηνόω

[kataskēnoō](https://www.blueletterbible.org/lexicon/g2681)

Definition: lodge (3x), rest (1x), dwell

Part of speech: verb

Occurs 4 times in 4 verses

Synonyms: [rest](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Rest)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2681)

[Study Light](https://www.studylight.org/lexicons/greek/2681.html)

[Bible Hub](https://biblehub.com/str/greek/2681.htm)

[LSJ](https://lsj.gr/wiki/κατασκηνόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2681)

[Bible Bento](https://biblebento.com/dictionary/G2681.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2681/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2681.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2681)