# τρεῖς

[treis](https://www.blueletterbible.org/lexicon/g5140)

Definition: three (69x).

Part of speech: feminine noun

Occurs 69 times in 60 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5140)

[Study Light](https://www.studylight.org/lexicons/greek/5140.html)

[Bible Hub](https://biblehub.com/str/greek/5140.htm)

[LSJ](https://lsj.gr/wiki/τρεῖς)

[NET Bible](http://classic.net.bible.org/strong.php?id=5140)

[Bible Bento](https://biblebento.com/dictionary/G5140.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5140/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5140.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5140)
