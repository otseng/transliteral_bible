# χώρα

[chōra](https://www.blueletterbible.org/lexicon/g5561)

Definition: country (15x), region (5x), land (3x), field (2x), ground (1x), coast (1x), territory

Part of speech: feminine noun

Occurs 27 times in 27 verses

Hebrew: ['ăḏāmâ](../h/h127.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5561)

[Study Light](https://www.studylight.org/lexicons/greek/5561.html)

[Bible Hub](https://biblehub.com/str/greek/5561.htm)

[LSJ](https://lsj.gr/wiki/χώρα)

[NET Bible](http://classic.net.bible.org/strong.php?id=5561)

[Bible Bento](https://biblebento.com/dictionary/G5561.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5561/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5561.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5561)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%87%CF%8E%CF%81%CE%B1)

[Wikipedia](https://en.wikipedia.org/wiki/Kh%C3%B4ra)