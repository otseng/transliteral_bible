# κιννάμωμον

[kinnamōmon](https://www.blueletterbible.org/lexicon/g2792)

Definition: cinnamon (1x).

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2792)

[Study Light](https://www.studylight.org/lexicons/greek/2792.html)

[Bible Hub](https://biblehub.com/str/greek/2792.htm)

[LSJ](https://lsj.gr/wiki/κιννάμωμον)

[NET Bible](http://classic.net.bible.org/strong.php?id=2792)

[Bible Bento](https://biblebento.com/dictionary/G2792.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2792/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2792.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2792)

