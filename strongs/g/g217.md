# ἅλας

[halas](https://www.blueletterbible.org/lexicon/g217)

Definition: salt (8x), prudence, wisdom and grace exhibited in speech

Part of speech: neuter noun

Occurs 8 times in 4 verses

Hebrew: [melaḥ](../h/h4417.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0217)

[Study Light](https://www.studylight.org/lexicons/greek/217.html)

[Bible Hub](https://biblehub.com/str/greek/217.htm)

[LSJ](https://lsj.gr/wiki/ἅλας)

[NET Bible](http://classic.net.bible.org/strong.php?id=217)

[Bible Bento](https://biblebento.com/dictionary/G0217.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0217/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0217.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g217)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%85%CE%BB%CE%B1%CF%82)

[Resounding the faith](https://resoundingthefaith.com/2020/06/greek-%E1%BC%85%CE%BB%CE%B1%CF%82-halas-latin-sal/)
