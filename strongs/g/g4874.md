# συναναμίγνυμι

[synanamignymi](https://www.blueletterbible.org/lexicon/g4874)

Definition: company with (1x), keep company (1x), have company with (1x), to mix up together

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4874)

[Study Light](https://www.studylight.org/lexicons/greek/4874.html)

[Bible Hub](https://biblehub.com/str/greek/4874.htm)

[LSJ](https://lsj.gr/wiki/συναναμίγνυμι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4874)

[Bible Bento](https://biblebento.com/dictionary/G4874.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4874/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4874.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4874)
