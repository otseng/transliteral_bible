# μία

[mia](https://www.blueletterbible.org/lexicon/g3391)

Definition: one (62x), first (8x), a certain (4x), a (3x), the other (1x), agree (with G4160) (with G1106) (1x), only one, someone

Part of speech: adjective

Occurs 79 times in 70 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3391)

[Study Light](https://www.studylight.org/lexicons/greek/3391.html)

[Bible Hub](https://biblehub.com/str/greek/3391.htm)

[LSJ](https://lsj.gr/wiki/μία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3391)

[Bible Bento](https://biblebento.com/dictionary/G3391.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3391/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3391.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3391)