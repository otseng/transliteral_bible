# ἅπαξ

[hapax](https://www.blueletterbible.org/lexicon/g530)

Definition: once (15x).

Part of speech: adverb

Occurs 15 times in 15 verses

Hebrew: ['echad](../h/h259.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g530)

[Study Light](https://www.studylight.org/lexicons/greek/530.html)

[Bible Hub](https://biblehub.com/str/greek/530.htm)

[LSJ](https://lsj.gr/wiki/ἅπαξ)

[NET Bible](http://classic.net.bible.org/strong.php?id=530)

[Bible Bento](https://biblebento.com/dictionary/G530.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/530/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/530.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g530)

