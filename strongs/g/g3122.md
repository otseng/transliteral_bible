# μάλιστα

[malista](https://www.blueletterbible.org/lexicon/g3122)

Definition: specially (5x), especially (4x), chiefly (2x), most of all (1x).

Part of speech: adverb

Occurs 12 times in 12 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3122)

[Study Light](https://www.studylight.org/lexicons/greek/3122.html)

[Bible Hub](https://biblehub.com/str/greek/3122.htm)

[LSJ](https://lsj.gr/wiki/μάλιστα)

[NET Bible](http://classic.net.bible.org/strong.php?id=3122)

[Bible Bento](https://biblebento.com/dictionary/G3122.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3122/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3122.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3122)

