# παρανομία

[paranomia](https://www.blueletterbible.org/lexicon/g3892)

Definition: iniquity (1x), breach of law, transgression, wickedness

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3892)

[Study Light](https://www.studylight.org/lexicons/greek/3892.html)

[Bible Hub](https://biblehub.com/str/greek/3892.htm)

[LSJ](https://lsj.gr/wiki/παρανομία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3892)

[Bible Bento](https://biblebento.com/dictionary/G3892.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3892/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3892.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3892)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%B1%CF%81%CE%B1%CE%BD%CE%BF%CE%BC%CE%AF%CE%B1)
