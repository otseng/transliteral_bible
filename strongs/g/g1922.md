# ἐπίγνωσις

[epignōsis](https://www.blueletterbible.org/lexicon/g1922)

Definition: knowledge (16x), acknowledging (3x), acknowledgement (1x), discernment, acquaintance

Part of speech: feminine noun

Occurs 20 times in 20 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1922)

[Study Light](https://www.studylight.org/lexicons/greek/1922.html)

[Bible Hub](https://biblehub.com/str/greek/1922.htm)

[LSJ](https://lsj.gr/wiki/ἐπίγνωσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=1922)

[Bible Bento](https://biblebento.com/dictionary/G1922.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1922/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1922.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1922)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%90%CF%80%CE%AF%CE%B3%CE%BD%CF%89%CF%83%CE%B9%CF%82)

[Precept Austin](https://www.preceptaustin.org/2_peter_12#knowledge)