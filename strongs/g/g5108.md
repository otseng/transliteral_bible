# τοιοῦτος

[toioutos](https://www.blueletterbible.org/lexicon/g5108)

Definition: such (39x), such thing (11x), such an one (8x), like (1x), such a man (1x), such a fellow (1x)

Part of speech: adjective

Occurs 61 times in 59 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5108)

[Study Light](https://www.studylight.org/lexicons/greek/5108.html)

[Bible Hub](https://biblehub.com/str/greek/5108.htm)

[LSJ](https://lsj.gr/wiki/τοιοῦτος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5108)

[Bible Bento](https://biblebento.com/dictionary/G5108.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5108/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5108.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5108)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%84%CE%BF%CE%B9%CE%BF%E1%BF%A6%CF%84%CE%BF%CF%82)
