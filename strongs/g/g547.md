# ἀπειλή

[apeilē](https://www.blueletterbible.org/lexicon/g547)

Definition: threatening (3x), straitly (1x), threat

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0547)

[Study Light](https://www.studylight.org/lexicons/greek/547.html)

[Bible Hub](https://biblehub.com/str/greek/547.htm)

[LSJ](https://lsj.gr/wiki/ἀπειλή)

[NET Bible](http://classic.net.bible.org/strong.php?id=547)

[Bible Bento](https://biblebento.com/dictionary/G0547.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0547/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0547.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g547)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CF%80%CE%B5%CE%B9%CE%BB%CE%AE)
