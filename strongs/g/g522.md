# ἀπαίρω

[apairō](https://www.blueletterbible.org/lexicon/g522)

Definition: take away (2x), take (1x), to lift off, take or carry away, remove

Part of speech: verb

Occurs 3 times in 3 verses

Hebrew: [ruwm](../h/h7311.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0522)

[Study Light](https://www.studylight.org/lexicons/greek/522.html)

[Bible Hub](https://biblehub.com/str/greek/522.htm)

[LSJ](https://lsj.gr/wiki/ἀπαίρω)

[NET Bible](http://classic.net.bible.org/strong.php?id=522)

[Bible Bento](https://biblebento.com/dictionary/G0522.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0522/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0522.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g522)
