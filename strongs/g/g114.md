# ἀθετέω

[atheteō](https://www.blueletterbible.org/lexicon/g114)

Definition: despise (8x), reject (4x), bring to nothing (1x), frustrate (1x), disannul (1x), cast off (1x), disregard, refuse, slight, frustrate, cast off

Part of speech: verb

Occurs 17 times in 12 verses

Hebrew: [mā'as](../h/h3988.md)

Synonyms: [despise](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Despise)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0114)

[Study Light](https://www.studylight.org/lexicons/greek/114.html)

[Bible Hub](https://biblehub.com/str/greek/114.htm)

[LSJ](https://lsj.gr/wiki/ἀθετέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=114)

[Bible Bento](https://biblebento.com/dictionary/G0114.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0114/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0114.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g114)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34764)