# χαλκίον

[chalkion](https://www.blueletterbible.org/lexicon/g5473)

Definition: brasen vessel (1x).

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/5473.html)

[Bible Hub](https://biblehub.com/str/greek/5473.htm)

[LSJ](https://lsj.gr/wiki/χαλκίον)

[NET Bible](http://classic.net.bible.org/strong.php?id=5473)

[Bible Bento](https://biblebento.com/dictionary/G5473.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5473/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5473.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5473)

