# ἀρνέομαι

[arneomai](https://www.blueletterbible.org/lexicon/g720)

Definition: deny (29x), refuse (2x), reject

Part of speech: verb

Occurs 33 times in 28 verses

Hebrew: [kāḥaš](../h/h3584.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0720)

[Study Light](https://www.studylight.org/lexicons/greek/720.html)

[Bible Hub](https://biblehub.com/str/greek/720.htm)

[LSJ](https://lsj.gr/wiki/ἀρνέομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=720)

[Bible Bento](https://biblebento.com/dictionary/G0720.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0720/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0720.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g720)