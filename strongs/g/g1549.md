# ἔκγονος

[enkonos](https://www.blueletterbible.org/lexicon/g1549)

Definition: nephew (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1549)

[Study Light](https://www.studylight.org/lexicons/greek/1549.html)

[Bible Hub](https://biblehub.com/str/greek/1549.htm)

[LSJ](https://lsj.gr/wiki/ἔκγονος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1549)

[Bible Bento](https://biblebento.com/dictionary/G1549.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1549/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1549.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1549)

