# πτύσμα

[ptysma](https://www.blueletterbible.org/lexicon/g4427)

Definition: spittle (1x), saliva

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4427)

[Study Light](https://www.studylight.org/lexicons/greek/4427.html)

[Bible Hub](https://biblehub.com/str/greek/4427.htm)

[LSJ](https://lsj.gr/wiki/πτύσμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=4427)

[Bible Bento](https://biblebento.com/dictionary/G4427.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4427/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4427.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4427)