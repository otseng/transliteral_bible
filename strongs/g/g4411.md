# πρωτοκλισία

[prōtoklisia](https://www.blueletterbible.org/lexicon/g4411)

Definition: uppermost room (2x), chief room (2x), highest room (1x), first reclining place, a reclining first (in the place of honor) at the dinner-bed

Part of speech: feminine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4411)

[Study Light](https://www.studylight.org/lexicons/greek/4411.html)

[Bible Hub](https://biblehub.com/str/greek/4411.htm)

[LSJ](https://lsj.gr/wiki/πρωτοκλισία)

[NET Bible](http://classic.net.bible.org/strong.php?id=4411)

[Bible Bento](https://biblebento.com/dictionary/G4411.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4411/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4411.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4411)