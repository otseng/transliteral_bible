# ἔσθησις

[esthēsis](https://www.blueletterbible.org/lexicon/g2067)

Definition: garment (1x), clothing, apparel

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2067)

[Study Light](https://www.studylight.org/lexicons/greek/2067.html)

[Bible Hub](https://biblehub.com/str/greek/2067.htm)

[LSJ](https://lsj.gr/wiki/ἔσθησις)

[NET Bible](http://classic.net.bible.org/strong.php?id=2067)

[Bible Bento](https://biblebento.com/dictionary/G2067.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2067/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2067.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2067)
