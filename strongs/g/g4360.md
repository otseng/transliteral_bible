# προσοχθίζω

[prosochthizō](https://www.blueletterbible.org/lexicon/g4360)

Definition: be grieved with (2x), to be wroth or displeased with, to be disgusted with, to feel indignant at

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4360)

[Study Light](https://www.studylight.org/lexicons/greek/4360.html)

[Bible Hub](https://biblehub.com/str/greek/4360.htm)

[LSJ](https://lsj.gr/wiki/προσοχθίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4360)

[Bible Bento](https://biblebento.com/dictionary/G4360.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4360/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4360.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4360)
