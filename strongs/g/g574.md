# ἁπλῶς

[haplōs](https://www.blueletterbible.org/lexicon/g574)

Definition: liberal (1x), simply, openly, frankly, sincerely 

Part of speech: adverb

Occurs 1 times in 1 verses

Greek: [haplotēs](../g/g572.md), [haplous](../g/g573.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0574)

[Study Light](https://www.studylight.org/lexicons/greek/574.html)

[Bible Hub](https://biblehub.com/str/greek/574.htm)

[LSJ](https://lsj.gr/wiki/ἁπλῶς)

[NET Bible](http://classic.net.bible.org/strong.php?id=574)

[Bible Bento](https://biblebento.com/dictionary/G574.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/574/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/574.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g574)
