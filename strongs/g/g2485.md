# ἰχθύδιον

[ichthydion](https://www.blueletterbible.org/lexicon/g2485)

Definition: little fish (1x), small fish (1x).

Part of speech: neuter noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2485)

[Study Light](https://www.studylight.org/lexicons/greek/2485.html)

[Bible Hub](https://biblehub.com/str/greek/2485.htm)

[LSJ](https://lsj.gr/wiki/ἰχθύδιον)

[NET Bible](http://classic.net.bible.org/strong.php?id=2485)

[Bible Bento](https://biblebento.com/dictionary/G2485.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2485/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2485.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2485)
