# ὁμιλέω

[homileō](https://www.blueletterbible.org/lexicon/g3656)

Definition: talk (2x), commune with (1x), commune together (1x), converse with, associate with, stay with

Part of speech: verb

Occurs 4 times in 4 verses

Hebrew: [dabar](../h/h1696.md)

Derived words: [homiletics](https://en.wikipedia.org/wiki/Homiletics), [homily](https://en.wikipedia.org/wiki/Homily)

Synonyms: [talk](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Talk)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3656)

[Study Light](https://www.studylight.org/lexicons/greek/3656.html)

[Bible Hub](https://biblehub.com/str/greek/3656.htm)

[LSJ](https://lsj.gr/wiki/ὁμιλέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3656)

[Bible Bento](https://biblebento.com/dictionary/G3656.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3656/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3656.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3656)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BD%81%CE%BC%CE%B9%CE%BB%E1%BF%B6)

[Precept Austin](https://www.preceptaustin.org/acts-20-commentary#talk)