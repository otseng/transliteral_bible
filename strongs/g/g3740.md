# ὁσάκις

[hosakis](https://www.blueletterbible.org/lexicon/g3740)

Definition: as often as (with G302) (1x), as often as (with G1437) (1x), as oft as (with G302) (1x).

Part of speech: adverb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3740)

[Study Light](https://www.studylight.org/lexicons/greek/3740.html)

[Bible Hub](https://biblehub.com/str/greek/3740.htm)

[LSJ](https://lsj.gr/wiki/ὁσάκις)

[NET Bible](http://classic.net.bible.org/strong.php?id=3740)

[Bible Bento](https://biblebento.com/dictionary/G3740.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3740/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3740.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3740)

