# βέβηλος

[bebēlos](https://www.blueletterbible.org/lexicon/g952)

Definition: profane (4x), profane person (1x), unhallowed, common, public place

Part of speech: adjective

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0952)

[Study Light](https://www.studylight.org/lexicons/greek/952.html)

[Bible Hub](https://biblehub.com/str/greek/952.htm)

[LSJ](https://lsj.gr/wiki/βέβηλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=952)

[Bible Bento](https://biblebento.com/dictionary/G0952.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/952/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/952.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g952)
