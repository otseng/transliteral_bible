# προστρέχω

[prostrechō](https://www.blueletterbible.org/lexicon/g4370)

Definition: run to (1x), run (1x), run thither to (1x).

Part of speech: verb

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4370)

[Study Light](https://www.studylight.org/lexicons/greek/4370.html)

[Bible Hub](https://biblehub.com/str/greek/4370.htm)

[LSJ](https://lsj.gr/wiki/προστρέχω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4370)

[Bible Bento](https://biblebento.com/dictionary/G4370.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4370/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4370.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4370)
