# Γαμαλιήλ

[Gamaliēl](https://www.blueletterbible.org/lexicon/g1059)

Definition: Gamaliel (2x), "my recompenser is God"

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1059)

[Study Light](https://www.studylight.org/lexicons/greek/1059.html)

[Bible Hub](https://biblehub.com/str/greek/1059.htm)

[LSJ](https://lsj.gr/wiki/Γαμαλιήλ)

[NET Bible](http://classic.net.bible.org/strong.php?id=1059)

[Bible Bento](https://biblebento.com/dictionary/G1059.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1059/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1059.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1059)

[Wikipedia](https://en.wikipedia.org/wiki/Gamaliel)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/gamaliel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/gamaliel)