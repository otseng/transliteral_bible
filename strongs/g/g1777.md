# ἔνοχος

[enochos](https://www.blueletterbible.org/lexicon/g1777)

Definition: in danger of (5x), guilty of (4x), subject to (1x), liable, subject to, under obligation

Part of speech: adjective

Occurs 10 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1777)

[Study Light](https://www.studylight.org/lexicons/greek/1777.html)

[Bible Hub](https://biblehub.com/str/greek/1777.htm)

[LSJ](https://lsj.gr/wiki/ἔνοχος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1777)

[Bible Bento](https://biblebento.com/dictionary/G1777.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1777/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1777.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1777)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%94%CE%BD%CE%BF%CF%87%CE%BF%CF%82)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=37332)

[Precept Austin](https://www.preceptaustin.org/matthew_521-22#l)