# διαλαλέω

[dialaleō](https://www.blueletterbible.org/lexicon/g1255)

Definition: noise abroad (1x), commune (1x), to converse together, to talk with

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1255)

[Study Light](https://www.studylight.org/lexicons/greek/1255.html)

[Bible Hub](https://biblehub.com/str/greek/1255.htm)

[LSJ](https://lsj.gr/wiki/διαλαλέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1255)

[Bible Bento](https://biblebento.com/dictionary/G1255.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1255/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1255.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1255)