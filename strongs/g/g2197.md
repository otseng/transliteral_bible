# Ζαχαρίας

[Zacharias](https://www.blueletterbible.org/lexicon/g2197)

Definition: Zacharias (11x), "remembered of Jehovah"

Part of speech: proper masculine noun

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2197)

[Study Light](https://www.studylight.org/lexicons/greek/2197.html)

[Bible Hub](https://biblehub.com/str/greek/2197.htm)

[LSJ](https://lsj.gr/wiki/Ζαχαρίας)

[NET Bible](http://classic.net.bible.org/strong.php?id=2197)

[Bible Bento](https://biblebento.com/dictionary/G2197.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2197/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2197.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2197)

[Wiktionary](https://en.wiktionary.org/wiki/Zacharias)

[Wikipedia](https://en.wikipedia.org/wiki/Zechariah_%28New_Testament_figure%29)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zacharias.html)