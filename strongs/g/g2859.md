# κόλπος

[kolpos](https://www.blueletterbible.org/lexicon/g2859)

Definition: bosom (5x), creek (1x), the front of the body between the arms, the hollow formed by the upper forepart of a rather loose garment bound by a girdle or sash, gulf, bay, creek, womb, cavity

Part of speech: masculine noun

Occurs 6 times in 6 verses

Hebrew: [ḥêq](../h/h2436.md)

Derived words: colpitis

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2859)

[Study Light](https://www.studylight.org/lexicons/greek/2859.html)

[Bible Hub](https://biblehub.com/str/greek/2859.htm)

[LSJ](https://lsj.gr/wiki/κόλπος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2859)

[Bible Bento](https://biblebento.com/dictionary/G2859.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2859/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2859.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2859)

[The Kolpos of the Father (Jn. 1:18) as the Womb of God in the Greek Tradition](https://www.questia.com/library/journal/1P3-4317242101/the-kolpos-of-the-father-jn-1-18-as-the-womb-of)