# μαμωνᾶς

[mamōnas](https://www.blueletterbible.org/lexicon/g3126)

Definition: mammon (4x), treasure, riches, wealth, assets, money

Part of speech: masculine noun

Occurs 4 times in 4 verses

Derived words: [mammon](https://en.wikipedia.org/wiki/Mammon_in_literature,_film,_and_popular_culture)

Notes: Aramaic מָמוֹנָא māmōnā - wealth, profit

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3126)

[Study Light](https://www.studylight.org/lexicons/greek/3126.html)

[Bible Hub](https://biblehub.com/str/greek/3126.htm)

[LSJ](https://lsj.gr/wiki/μαμωνᾶς)

[NET Bible](http://classic.net.bible.org/strong.php?id=3126)

[Bible Bento](https://biblebento.com/dictionary/G3126.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3126/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3126.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3126)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BC%CE%B1%CE%BC%CF%89%CE%BD%E1%BE%B6%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Mammon)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=36064)

[New Advent](http://catholicencyclopedia.newadvent.com/cathen/09580b.htm)