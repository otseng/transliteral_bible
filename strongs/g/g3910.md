# παραυτίκα

[parautika](https://www.blueletterbible.org/lexicon/g3910)

Definition: but for a moment (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3910)

[Study Light](https://www.studylight.org/lexicons/greek/3910.html)

[Bible Hub](https://biblehub.com/str/greek/3910.htm)

[LSJ](https://lsj.gr/wiki/παραυτίκα)

[NET Bible](http://classic.net.bible.org/strong.php?id=3910)

[Bible Bento](https://biblebento.com/dictionary/G3910.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3910/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3910.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3910)
