# βαστάζω

[bastazō](https://www.blueletterbible.org/lexicon/g941)

Definition: bear (23x), carry (3x), take up (1x), endure

Part of speech: verb

Occurs 29 times in 27 verses

Greek: [basis](../g/g939.md)

Hebrew: [natah](../h/h5186.md), [nasa'](../h/h5375.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0941)

[Study Light](https://www.studylight.org/lexicons/greek/941.html)

[Bible Hub](https://biblehub.com/str/greek/941.htm)

[LSJ](https://lsj.gr/wiki/βαστάζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=941)

[Bible Bento](https://biblebento.com/dictionary/G0941.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0941/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0941.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g941)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B2%CE%B1%CF%83%CF%84%CE%AC%CE%B6%CF%89)