# κόκκινος

[kokkinos](https://www.blueletterbible.org/lexicon/g2847)

Definition: scarlet (4x), scarlet colour (1x), scarlet coloured (1x)

Part of speech: adjective

Occurs 6 times in 6 verses

Hebrew: [tôlāʿ](../h/h8438.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2847)

[Study Light](https://www.studylight.org/lexicons/greek/2847.html)

[Bible Hub](https://biblehub.com/str/greek/2847.htm)

[LSJ](https://lsj.gr/wiki/κόκκινος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2847)

[Bible Bento](https://biblebento.com/dictionary/G2847.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2847/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2847.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2847)
