# χειροτονέω

[cheirotoneō](https://www.blueletterbible.org/lexicon/g5500)

Definition: ordain (3x), choose (1x), to elect, create, appoint

Part of speech: verb

Occurs 3 times in 2 verses

Hebrew: [bāḥar](../h/h977.md)

Synonyms: [choose](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Choose)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5500)

[Study Light](https://www.studylight.org/lexicons/greek/5500.html)

[Bible Hub](https://biblehub.com/str/greek/5500.htm)

[LSJ](https://lsj.gr/wiki/χειροτονέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5500)

[Bible Bento](https://biblebento.com/dictionary/G5500.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5500/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5500.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5500)

[Precept Austin](https://www.preceptaustin.org/acts-14-commentary#appointed)
