# ὑπεράνω

[hyperanō](https://www.blueletterbible.org/lexicon/g5231)

Definition: far above (2x), over (1x).

Part of speech: adverb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5231)

[Study Light](https://www.studylight.org/lexicons/greek/5231.html)

[Bible Hub](https://biblehub.com/str/greek/5231.htm)

[LSJ](https://lsj.gr/wiki/ὑπεράνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5231)

[Bible Bento](https://biblebento.com/dictionary/G5231.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5231/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5231.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5231)

