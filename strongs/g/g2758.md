# κενόω

[kenoō](https://www.blueletterbible.org/lexicon/g2758)

Definition: make void (2x), make of none effect (1x), make of no reputation (1x), be in vain (1x), make empty, to abase, falsify, to completely eliminate elements of high status or rank by eliminating all privileges or prerogatives associated with such status or rank, remove the content of something, pouring something out

Part of speech: verb

Occurs 7 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2758)

[Study Light](https://www.studylight.org/lexicons/greek/2758.html)

[Bible Hub](https://biblehub.com/str/greek/2758.htm)

[LSJ](https://lsj.gr/wiki/κενόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2758)

[Bible Bento](https://biblebento.com/dictionary/G2758.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2758/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2758.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2758)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=11067)

[Precept Austin](https://www.preceptaustin.org/philippians_27_commentary#emptied)
