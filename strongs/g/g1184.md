# δεκτός

[dektos](https://www.blueletterbible.org/lexicon/g1184)

Definition: accepted (3x), acceptable (2x), favorable

Part of speech: adjective

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1184)

[Study Light](https://www.studylight.org/lexicons/greek/1184.html)

[Bible Hub](https://biblehub.com/str/greek/1184.htm)

[LSJ](https://lsj.gr/wiki/δεκτός)

[NET Bible](http://classic.net.bible.org/strong.php?id=1184)

[Bible Bento](https://biblebento.com/dictionary/G1184.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1184/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1184.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1184)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=37797)

[Precept Austin](https://www.preceptaustin.org/philippians_414-18#a)