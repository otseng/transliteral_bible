# εὐμετάδοτος

[eumetadotos](https://www.blueletterbible.org/lexicon/g2130)

Definition: ready to distribute (1x), ready or free to impart, liberal

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2130)

[Study Light](https://www.studylight.org/lexicons/greek/2130.html)

[Bible Hub](https://biblehub.com/str/greek/2130.htm)

[LSJ](https://lsj.gr/wiki/εὐμετάδοτος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2130)

[Bible Bento](https://biblebento.com/dictionary/G2130.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2130/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2130.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2130)
