# ἔστω

[estō](https://www.blueletterbible.org/lexicon/g2077)

Definition: let be (10x), be (5x), not translated (1x).

Part of speech: verb

Occurs 16 times in 16 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2077)

[Study Light](https://www.studylight.org/lexicons/greek/2077.html)

[Bible Hub](https://biblehub.com/str/greek/2077.htm)

[LSJ](https://lsj.gr/wiki/ἔστω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2077)

[Bible Bento](https://biblebento.com/dictionary/G2077.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2077/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2077.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2077)

