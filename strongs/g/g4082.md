# πήρα

[pēra](https://www.blueletterbible.org/lexicon/g4082)

Definition: scrip (6x), wallet, leather sack, pouch, bag

Part of speech: feminine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4082)

[Study Light](https://www.studylight.org/lexicons/greek/4082.html)

[Bible Hub](https://biblehub.com/str/greek/4082.htm)

[LSJ](https://lsj.gr/wiki/πήρα)

[NET Bible](http://classic.net.bible.org/strong.php?id=4082)

[Bible Bento](https://biblebento.com/dictionary/G4082.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4082/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4082.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4082)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%AE%CF%81%CE%B1)

[Precept Austin](https://www.preceptaustin.org/luke-9-commentary#bag)