# μόδιος

[modios](https://www.blueletterbible.org/lexicon/g3426)

Definition: bushel (3x)

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3426)

[Study Light](https://www.studylight.org/lexicons/greek/3426.html)

[Bible Hub](https://biblehub.com/str/greek/3426.htm)

[LSJ](https://lsj.gr/wiki/μόδιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3426)

[Bible Bento](https://biblebento.com/dictionary/G3426.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3426/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3426.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3426)

[Sizes](https://www.sizes.com/units/modios.htm)