# δίλογος

[dilogos](https://www.blueletterbible.org/lexicon/g1351)

Definition: doubletongued (1x). double in speech, saying one thing with one person another with another (with the intent to deceive)

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1351)

[Study Light](https://www.studylight.org/lexicons/greek/1351.html)

[Bible Hub](https://biblehub.com/str/greek/1351.htm)

[LSJ](https://lsj.gr/wiki/δίλογος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1351)

[Bible Bento](https://biblebento.com/dictionary/G1351.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1351/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1351.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1351)
