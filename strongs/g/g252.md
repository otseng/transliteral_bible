# ἁλυκός

[halykos](https://www.blueletterbible.org/lexicon/g252)

Definition: salt (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0252)

[Study Light](https://www.studylight.org/lexicons/greek/252.html)

[Bible Hub](https://biblehub.com/str/greek/252.htm)

[LSJ](https://lsj.gr/wiki/ἁλυκός)

[NET Bible](http://classic.net.bible.org/strong.php?id=252)

[Bible Bento](https://biblebento.com/dictionary/G252.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/252/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/252.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g252)

