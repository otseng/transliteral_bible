# πλεονάζω

[pleonazō](https://www.blueletterbible.org/lexicon/g4121)

Definition: abound (6x), abundant (1x), have over (1x), make to increase (1x), to superabound, to exist in abundance, be in the majority

Part of speech: verb

Occurs 9 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4121)

[Study Light](https://www.studylight.org/lexicons/greek/4121.html)

[Bible Hub](https://biblehub.com/str/greek/4121.htm)

[LSJ](https://lsj.gr/wiki/πλεονάζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4121)

[Bible Bento](https://biblebento.com/dictionary/G4121.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4121/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4121.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4121)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%BB%CE%B5%CE%BF%CE%BD%CE%AC%CE%B6%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34278)

[Precept Austin](https://www.preceptaustin.org/philippians_414-18#i)
