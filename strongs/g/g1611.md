# ἔκστασις

[ekstasis](https://www.blueletterbible.org/lexicon/g1611)

Definition: trance (3x), be amazed(2x), amazement (1x), astonishment (1x)

Part of speech: feminine noun

Occurs 7 times in 7 verses

Derived words: ecstasy

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1611)

[Study Light](https://www.studylight.org/lexicons/greek/1611.html)

[Bible Hub](https://biblehub.com/str/greek/1611.htm)

[LSJ](https://lsj.gr/wiki/ἔκστασις)

[NET Bible](http://classic.net.bible.org/strong.php?id=1611)

[Bible Bento](https://biblebento.com/dictionary/G1611.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1611/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1611.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1611)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%94%CE%BA%CF%83%CF%84%CE%B1%CF%83%CE%B9%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Ecstasy_%28philosophy%29)

[Precept Austin](https://www.preceptaustin.org/luke-5-commentary#ast)