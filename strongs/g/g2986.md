# λαμπρός

[lampros](https://www.blueletterbible.org/lexicon/g2986)

Definition: bright (2x), goodly (2x), white (2x), gorgeous (1x), gay (1x), clear (1x), brilliant, splendid, magnificent

Part of speech: adjective

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2986)

[Study Light](https://www.studylight.org/lexicons/greek/2986.html)

[Bible Hub](https://biblehub.com/str/greek/2986.htm)

[LSJ](https://lsj.gr/wiki/λαμπρός)

[NET Bible](http://classic.net.bible.org/strong.php?id=2986)

[Bible Bento](https://biblebento.com/dictionary/G2986.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2986/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2986.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2986)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BB%CE%B1%CE%BC%CF%80%CF%81%CF%8C%CF%82)