# χαλινός

[chalinos](https://www.blueletterbible.org/lexicon/g5469)

Definition: bit (1x), bridle (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5469)

[Study Light](https://www.studylight.org/lexicons/greek/5469.html)

[Bible Hub](https://biblehub.com/str/greek/5469.htm)

[LSJ](https://lsj.gr/wiki/χαλινός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5469)

[Bible Bento](https://biblebento.com/dictionary/G5469.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5469/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5469.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5469)

