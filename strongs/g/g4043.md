# περιπατέω

[peripateō](https://www.blueletterbible.org/lexicon/g4043)

Definition: walk (93x), go (1x), walk about (1x), be occupied (1x), live

Part of speech: verb

Occurs 97 times in 90 verses

Hebrew: [halak](../h/h1980.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4043)

[Study Light](https://www.studylight.org/lexicons/greek/4043.html)

[Bible Hub](https://biblehub.com/str/greek/4043.htm)

[LSJ](https://lsj.gr/wiki/περιπατέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4043)

[Bible Bento](https://biblebento.com/dictionary/G4043.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4043/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4043.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4043)

[Greek word studies](http://greekwordstudies.blogspot.com/2007/03/walk.html)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=36056)