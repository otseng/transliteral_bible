# ἀγενεαλόγητος

[agenealogētos](https://www.blueletterbible.org/lexicon/g35)

Definition: without descent (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0035)

[Study Light](https://www.studylight.org/lexicons/greek/35.html)

[Bible Hub](https://biblehub.com/str/greek/35.htm)

[LSJ](https://lsj.gr/wiki/ἀγενεαλόγητος)

[NET Bible](http://classic.net.bible.org/strong.php?id=35)

[Bible Bento](https://biblebento.com/dictionary/G0035.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/35/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/35.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g35)
