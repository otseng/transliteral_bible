# ἐκζητέω

[ekzēteō](https://www.blueletterbible.org/lexicon/g1567)

Definition: require (2x), seek after (2x), diligently (1x), seek carefully (1x), enquire (1x), beg, crave

Part of speech: verb

Occurs 8 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1567)

[Study Light](https://www.studylight.org/lexicons/greek/1567.html)

[Bible Hub](https://biblehub.com/str/greek/1567.htm)

[LSJ](https://lsj.gr/wiki/ἐκζητέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1567)

[Bible Bento](https://biblebento.com/dictionary/G1567.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1567/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1567.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1567)