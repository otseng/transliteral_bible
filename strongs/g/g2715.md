# κατεξουσιάζω

[katexousiazō](https://www.blueletterbible.org/lexicon/g2715)

Definition: exercise authority upon (2x), wield power

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2715)

[Study Light](https://www.studylight.org/lexicons/greek/2715.html)

[Bible Hub](https://biblehub.com/str/greek/2715.htm)

[LSJ](https://lsj.gr/wiki/κατεξουσιάζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2715)

[Bible Bento](https://biblebento.com/dictionary/G2715.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2715/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2715.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2715)