# πυλών

[pylōn](https://www.blueletterbible.org/lexicon/g4440)

Definition: gate (17x), porch, portal, door-way of a building or city

Part of speech: masculine noun

Occurs 18 times in 12 verses

Hebrew: [peṯaḥ](../h/h6607.md)

Derived words: pylon

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4440)

[Study Light](https://www.studylight.org/lexicons/greek/4440.html)

[Bible Hub](https://biblehub.com/str/greek/4440.htm)

[LSJ](https://lsj.gr/wiki/πυλών)

[NET Bible](http://classic.net.bible.org/strong.php?id=4440)

[Bible Bento](https://biblebento.com/dictionary/G4440.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4440/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4440.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4440)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CF%85%CE%BB%CF%8E%CE%BD)

[Wikipedia](https://en.wikipedia.org/wiki/Pylon_%28architecture%29)