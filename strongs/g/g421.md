# ἀνεξιχνίαστος

[anexichniastos](https://www.blueletterbible.org/lexicon/g421)

Definition: past finding out (1x), unsearchable (1x), that cannot be comprehended

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0421)

[Study Light](https://www.studylight.org/lexicons/greek/421.html)

[Bible Hub](https://biblehub.com/str/greek/421.htm)

[LSJ](https://lsj.gr/wiki/ἀνεξιχνίαστος)

[NET Bible](http://classic.net.bible.org/strong.php?id=421)

[Bible Bento](https://biblebento.com/dictionary/G0421.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0421/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0421.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g421)
