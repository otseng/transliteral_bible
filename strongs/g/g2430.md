# Ἰκόνιον

[Ikonion](https://www.blueletterbible.org/lexicon/g2430)

Definition: Iconium (6x), "little image", a famous city of Asia Minor, which was the capital of Lycaonia

Part of speech: proper locative noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2430)

[Study Light](https://www.studylight.org/lexicons/greek/2430.html)

[Bible Hub](https://biblehub.com/str/greek/2430.htm)

[LSJ](https://lsj.gr/wiki/Ἰκόνιον)

[NET Bible](http://classic.net.bible.org/strong.php?id=2430)

[Bible Bento](https://biblebento.com/dictionary/G2430.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2430/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2430.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2430)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%B8%CE%BA%CF%8C%CE%BD%CE%B9%CE%BF%CE%BD)

[Wikipedia](https://en.wikipedia.org/wiki/Konya)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/iconium.html)

[Video Bible](https://www.videobible.com/bible-dictionary/iconium)