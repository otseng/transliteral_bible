# οἰκτίρω

[oiktirō](https://www.blueletterbible.org/lexicon/g3627)

Definition: have compassion on (1x), have compassion (1x), to pity

Part of speech: verb

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3627)

[Study Light](https://www.studylight.org/lexicons/greek/3627.html)

[Bible Hub](https://biblehub.com/str/greek/3627.htm)

[LSJ](https://lsj.gr/wiki/οἰκτίρω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3627)

[Bible Bento](https://biblebento.com/dictionary/G3627.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3627/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3627.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3627)
