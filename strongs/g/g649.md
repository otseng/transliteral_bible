# ἀποστέλλω

[apostellō](https://www.blueletterbible.org/lexicon/g649)

Definition: send (110x), send forth (15x), send away (4x), send out (2x), forth, send off, to order to go a place appointed

Part of speech: verb

Occurs 143 times in 130 verses

Greek: [apostolos](../g/g652.md)

Hebrew: [shalach](../h/h7971.md)

Derived words: apostle

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0649)

[Study Light](https://www.studylight.org/lexicons/greek/649.html)

[Bible Hub](https://biblehub.com/str/greek/649.htm)

[LSJ](https://lsj.gr/wiki/ἀποστέλλω)

[NET Bible](http://classic.net.bible.org/strong.php?id=649)

[Bible Bento](https://biblebento.com/dictionary/G0649.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0649/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0649.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g649)