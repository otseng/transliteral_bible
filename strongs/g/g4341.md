# προσκαλέω

[proskaleō](https://www.blueletterbible.org/lexicon/g4341)

Definition: call unto (20x), call (7x), call for (2x), call to (1x), invite, summon, cite as witness

Part of speech: verb

Occurs 31 times in 30 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4341)

[Study Light](https://www.studylight.org/lexicons/greek/4341.html)

[Bible Hub](https://biblehub.com/str/greek/4341.htm)

[LSJ](https://lsj.gr/wiki/προσκαλέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4341)

[Bible Bento](https://biblebento.com/dictionary/G4341.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4341/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4341.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4341)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CF%81%CE%BF%CF%83%CE%BA%CE%B1%CE%BB%CE%AD%CF%89)
