# ἀνεκλάλητος

[aneklalētos](https://www.blueletterbible.org/lexicon/g412)

Definition: unspeakable (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0412)

[Study Light](https://www.studylight.org/lexicons/greek/412.html)

[Bible Hub](https://biblehub.com/str/greek/412.htm)

[LSJ](https://lsj.gr/wiki/ἀνεκλάλητος)

[NET Bible](http://classic.net.bible.org/strong.php?id=412)

[Bible Bento](https://biblebento.com/dictionary/G412.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/412/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/412.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g412)

