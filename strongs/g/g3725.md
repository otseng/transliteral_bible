# ὅριον

[horion](https://www.blueletterbible.org/lexicon/g3725)

Definition: coast (10x), border (1x), boundary, region

Part of speech: neuter noun

Occurs 11 times in 10 verses

Derived words: horizon

Hebrew: [gᵊḇûl](../h/h1366.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3725)

[Study Light](https://www.studylight.org/lexicons/greek/3725.html)

[Bible Hub](https://biblehub.com/str/greek/3725.htm)

[LSJ](https://lsj.gr/wiki/ὅριον)

[NET Bible](http://classic.net.bible.org/strong.php?id=3725)

[Bible Bento](https://biblebento.com/dictionary/G3725.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3725/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3725.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3725)