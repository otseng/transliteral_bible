# Ζεβεδαῖος

[Zebedaios](https://www.blueletterbible.org/lexicon/g2199)

Definition: Zebedee (12x), "my gift"

Part of speech: proper masculine noun

Occurs 12 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2199)

[Study Light](https://www.studylight.org/lexicons/greek/2199.html)

[Bible Hub](https://biblehub.com/str/greek/2199.htm)

[LSJ](https://lsj.gr/wiki/Ζεβεδαῖος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2199)

[Bible Bento](https://biblebento.com/dictionary/G2199.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2199/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2199.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2199)

[Video Bible](https://www.videobible.com/bible-dictionary/zebedee)