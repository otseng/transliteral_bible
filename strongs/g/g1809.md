# ἐξαιτέω

[exaiteō](https://www.blueletterbible.org/lexicon/g1809)

Definition: desire (1x), to ask from, demand of

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1809)

[Study Light](https://www.studylight.org/lexicons/greek/1809.html)

[Bible Hub](https://biblehub.com/str/greek/1809.htm)

[LSJ](https://lsj.gr/wiki/ἐξαιτέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1809)

[Bible Bento](https://biblebento.com/dictionary/G1809.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1809/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1809.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1809)

[Precept Austin](https://www.preceptaustin.org/luke-22-commentary#demand)