# ἐκφύω

[ekphyō](https://www.blueletterbible.org/lexicon/g1631)

Definition: put forth (2x), to generate or produce from

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1631)

[Study Light](https://www.studylight.org/lexicons/greek/1631.html)

[Bible Hub](https://biblehub.com/str/greek/1631.htm)

[LSJ](https://lsj.gr/wiki/ἐκφύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1631)

[Bible Bento](https://biblebento.com/dictionary/G1631.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1631/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1631.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1631)
