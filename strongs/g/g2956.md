# Κυρηναῖος

[Kyrēnaios](https://www.blueletterbible.org/lexicon/g2956)

Definition: Cyrene (3x), Cyrenian (3x), a native of Cyrene

Part of speech: proper masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2956)

[Study Light](https://www.studylight.org/lexicons/greek/2956.html)

[Bible Hub](https://biblehub.com/str/greek/2956.htm)

[LSJ](https://lsj.gr/wiki/Κυρηναῖος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2956)

[Bible Bento](https://biblebento.com/dictionary/G2956.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2956/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2956.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2956)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/cyrene.html)

[Video Bible](https://www.videobible.com/bible-dictionary/cyrene)