# εὐσεβῶς

[eusebōs](https://www.blueletterbible.org/lexicon/g2153)

Definition: godly (2x), piously

Part of speech: adverb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2153)

[Study Light](https://www.studylight.org/lexicons/greek/2153.html)

[Bible Hub](https://biblehub.com/str/greek/2153.htm)

[LSJ](https://lsj.gr/wiki/εὐσεβῶς)

[NET Bible](http://classic.net.bible.org/strong.php?id=2153)

[Bible Bento](https://biblebento.com/dictionary/G2153.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2153/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2153.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2153)
