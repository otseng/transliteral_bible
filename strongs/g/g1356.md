# διοπετής

[diopetēs](https://www.blueletterbible.org/lexicon/g1356)

Definition: which fell down from Jupiter (1x), an image of the Ephesian Artemis which was supposed to have fallen from heaven

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1356)

[Study Light](https://www.studylight.org/lexicons/greek/1356.html)

[Bible Hub](https://biblehub.com/str/greek/1356.htm)

[LSJ](https://lsj.gr/wiki/διοπετής)

[NET Bible](http://classic.net.bible.org/strong.php?id=1356)

[Bible Bento](https://biblebento.com/dictionary/G1356.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1356/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1356.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1356)
