# παραβιάζομαι

[parabiazomai](https://www.blueletterbible.org/lexicon/g3849)

Definition: constrain (2x), compel, to constrain one by entreaties

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3849)

[Study Light](https://www.studylight.org/lexicons/greek/3849.html)

[Bible Hub](https://biblehub.com/str/greek/3849.htm)

[LSJ](https://lsj.gr/wiki/παραβιάζομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=3849)

[Bible Bento](https://biblebento.com/dictionary/G3849.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3849/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3849.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3849)
