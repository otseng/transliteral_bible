# Καναναῖος

[Kananaios](https://www.blueletterbible.org/lexicon/g2581)

Definition: Canaanite (2x), "zealous", the surname of apostle Simon, otherwise known as "Simon Zelotes"

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

Hebrew: [Kᵊnaʿănî](../h/h3669.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2581)

[Study Light](https://www.studylight.org/lexicons/greek/2581.html)

[Bible Hub](https://biblehub.com/str/greek/2581.htm)

[LSJ](https://lsj.gr/wiki/Καναναῖος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2581)

[Bible Bento](https://biblebento.com/dictionary/G2581.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2581/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2581.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2581)
