# Ἑβραϊστί

[Hebraïsti](https://www.blueletterbible.org/lexicon/g1447)

Definition: in the Hebrew tongue (3x), in the Hebrew (2x), in Hebrew (1x), Hebrew language, Chaldee

Part of speech: adverb

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1447)

[Study Light](https://www.studylight.org/lexicons/greek/1447.html)

[Bible Hub](https://biblehub.com/str/greek/1447.htm)

[LSJ](https://lsj.gr/wiki/Ἑβραϊστί)

[NET Bible](http://classic.net.bible.org/strong.php?id=1447)

[Bible Bento](https://biblebento.com/dictionary/G1447.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1447/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1447.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1447)