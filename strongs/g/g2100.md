# εὐαρεστέω

[euaresteō](https://www.blueletterbible.org/lexicon/g2100)

Definition: please (2x), be well pleased (1x), to gratify entirely

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2100)

[Study Light](https://www.studylight.org/lexicons/greek/2100.html)

[Bible Hub](https://biblehub.com/str/greek/2100.htm)

[LSJ](https://lsj.gr/wiki/εὐαρεστέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2100)

[Bible Bento](https://biblebento.com/dictionary/G2100.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2100/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2100.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2100)
