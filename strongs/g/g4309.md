# προορίζω

[proorizō](https://www.blueletterbible.org/lexicon/g4309)

Definition: predestinate (4x), determine before (1x), ordain (1x), "before appoint"

Part of speech: verb

Occurs 7 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4309)

[Study Light](https://www.studylight.org/lexicons/greek/4309.html)

[Bible Hub](https://biblehub.com/str/greek/4309.htm)

[LSJ](https://lsj.gr/wiki/προορίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4309)

[Bible Bento](https://biblebento.com/dictionary/G4309.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4309/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4309.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4309)

[Sermon Index](https://www.preceptaustin.org/romans_829-30#predestined)

[Ministry Magazine](https://www.ministrymagazine.org/archive/2014/03/predestination-a-theology-of-divine-intention)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/4309-proorizo-predestine.htm)
