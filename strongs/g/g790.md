# ἀστατέω

[astateō](https://www.blueletterbible.org/lexicon/g790)

Definition: have no certain dwelling place (1x), to wander about, to rove without a settled abode

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0790)

[Study Light](https://www.studylight.org/lexicons/greek/790.html)

[Bible Hub](https://biblehub.com/str/greek/790.htm)

[LSJ](https://lsj.gr/wiki/ἀστατέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=790)

[Bible Bento](https://biblebento.com/dictionary/G0790.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0790/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0790.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g790)
