# ὀρθοποδέω

[orthopodeō](https://www.blueletterbible.org/lexicon/g3716)

Definition: walk uprightly (1x), to walk in a straight course

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3716)

[Study Light](https://www.studylight.org/lexicons/greek/3716.html)

[Bible Hub](https://biblehub.com/str/greek/3716.htm)

[LSJ](https://lsj.gr/wiki/ὀρθοποδέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3716)

[Bible Bento](https://biblebento.com/dictionary/G3716.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3716/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3716.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3716)
