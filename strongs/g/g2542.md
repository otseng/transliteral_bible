# Καισάρεια

[Kaisareia](https://www.blueletterbible.org/lexicon/g2542)

Definition: Caesarea (of Palestine) (15x), Caesarea (Philippi) (2x), "severed", Caesarea of Philippi was situated at the foot of Lebanon near the sources of the Jordan in Gaulanitis, and formerly called Paneas; but afterward being rebuilt by Philip the tetrarch, it was called by him Caesarea, in honour of Tiberias Caesar; subsequently called Neronias by Agrippa II, in honour of Nero.  Built by Herod the Great about 25–13 BCE as a major port.

Part of speech: proper locative noun

Occurs 17 times in 17 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2542)

[Study Light](https://www.studylight.org/lexicons/greek/2542.html)

[Bible Hub](https://biblehub.com/str/greek/2542.htm)

[LSJ](https://lsj.gr/wiki/Καισάρεια)

[NET Bible](http://classic.net.bible.org/strong.php?id=2542)

[Bible Bento](https://biblebento.com/dictionary/G2542.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2542/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2542.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2542)

[Wikipedia](https://en.wikipedia.org/wiki/Caesarea)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/caesarea.html)

[Video Bible](https://www.videobible.com/bible-dictionary/caesarea)