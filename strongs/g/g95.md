# ἀδίκως

[adikōs](https://www.blueletterbible.org/lexicon/g95)

Definition: wrongfully (1x), unjustly, undeserved, without fault

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0095)

[Study Light](https://www.studylight.org/lexicons/greek/95.html)

[Bible Hub](https://biblehub.com/str/greek/95.htm)

[LSJ](https://lsj.gr/wiki/ἀδίκως)

[NET Bible](http://classic.net.bible.org/strong.php?id=95)

[Bible Bento](https://biblebento.com/dictionary/G95.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/95/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/95.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g95)

