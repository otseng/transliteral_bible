# ἀνακεφαλαιόω

[anakephalaioō](https://www.blueletterbible.org/lexicon/g346)

Definition: briefly comprehend (1x), gather together in one (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0346)

[Study Light](https://www.studylight.org/lexicons/greek/346.html)

[Bible Hub](https://biblehub.com/str/greek/346.htm)

[LSJ](https://lsj.gr/wiki/ἀνακεφαλαιόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=346)

[Bible Bento](https://biblebento.com/dictionary/G0346.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/346/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/346.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g346)
