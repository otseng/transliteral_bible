# περιέχω

[periechō](https://www.blueletterbible.org/lexicon/g4023)

Definition: be astonished (with G2285) (1x), after this manner (with G5126) (with G5176) (1x), be contained (1x), to surround, encompass

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4023)

[Study Light](https://www.studylight.org/lexicons/greek/4023.html)

[Bible Hub](https://biblehub.com/str/greek/4023.htm)

[LSJ](https://lsj.gr/wiki/περιέχω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4023)

[Bible Bento](https://biblebento.com/dictionary/G4023.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4023/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4023.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4023)