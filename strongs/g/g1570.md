# ἔκθετος

[ekthetos](https://www.blueletterbible.org/lexicon/g1570)

Definition: cast out (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1570)

[Study Light](https://www.studylight.org/lexicons/greek/1570.html)

[Bible Hub](https://biblehub.com/str/greek/1570.htm)

[LSJ](https://lsj.gr/wiki/ἔκθετος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1570)

[Bible Bento](https://biblebento.com/dictionary/G1570.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1570/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1570.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1570)
