# προσήλυτος

[prosēlytos](https://www.blueletterbible.org/lexicon/g4339)

Definition: proselyte (4x)

Part of speech: adjective

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4339)

[Study Light](https://www.studylight.org/lexicons/greek/4339.html)

[Bible Hub](https://biblehub.com/str/greek/4339.htm)

[LSJ](https://lsj.gr/wiki/προσήλυτος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4339)

[Bible Bento](https://biblebento.com/dictionary/G4339.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4339/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4339.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4339)
