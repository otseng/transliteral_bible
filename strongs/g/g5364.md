# φιλανθρώπως

[philanthrōpōs](https://www.blueletterbible.org/lexicon/g5364)

Definition: courteously (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5364)

[Study Light](https://www.studylight.org/lexicons/greek/5364.html)

[Bible Hub](https://biblehub.com/str/greek/5364.htm)

[LSJ](https://lsj.gr/wiki/φιλανθρώπως)

[NET Bible](http://classic.net.bible.org/strong.php?id=5364)

[Bible Bento](https://biblebento.com/dictionary/G5364.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5364/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5364.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5364)

