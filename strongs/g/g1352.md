# διό

[dio](https://www.blueletterbible.org/lexicon/g1352)

Definition: wherefore (41x), therefore (10x), for which cause (2x), consequently

Part of speech: conjunction

Occurs 53 times in 52 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1352)

[Study Light](https://www.studylight.org/lexicons/greek/1352.html)

[Bible Hub](https://biblehub.com/str/greek/1352.htm)

[LSJ](https://lsj.gr/wiki/διό)

[NET Bible](http://classic.net.bible.org/strong.php?id=1352)

[Bible Bento](https://biblebento.com/dictionary/G1352.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1352/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1352.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1352)
