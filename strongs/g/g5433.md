# φρυάσσω

[phryassō](https://www.blueletterbible.org/lexicon/g5433)

Definition: rage (1x), to neigh, stamp the ground, prance, snort, to make a tumult

Part of speech: verb

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5433)

[Study Light](https://www.studylight.org/lexicons/greek/5433.html)

[Bible Hub](https://biblehub.com/str/greek/5433.htm)

[LSJ](https://lsj.gr/wiki/φρυάσσω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5433)

[Bible Bento](https://biblebento.com/dictionary/G5433.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5433/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5433.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5433)
