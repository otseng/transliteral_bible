# καλύπτω

[kalyptō](https://www.blueletterbible.org/lexicon/g2572)

Definition: cover (5x), hide (3x), veil, cover up, conceal

Part of speech: verb

Occurs 8 times in 7 verses

Hebrew: [kāsâ](../h/h3680.md), [sāṯam](../h/h5640.md), [cakak](../h/h5526.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2572)

[Study Light](https://www.studylight.org/lexicons/greek/2572.html)

[Bible Hub](https://biblehub.com/str/greek/2572.htm)

[LSJ](https://lsj.gr/wiki/καλύπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2572)

[Bible Bento](https://biblebento.com/dictionary/G2572.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2572/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2572.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2572)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%B1%CE%BB%CF%8D%CF%80%CF%84%CF%89)