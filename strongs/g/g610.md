# ἀπόκριμα

[apokrima](https://www.blueletterbible.org/lexicon/g610)

Definition: sentence (1x), judicial decision

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0610)

[Study Light](https://www.studylight.org/lexicons/greek/610.html)

[Bible Hub](https://biblehub.com/str/greek/610.htm)

[LSJ](https://lsj.gr/wiki/ἀπόκριμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=610)

[Bible Bento](https://biblebento.com/dictionary/G0610.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0610/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0610.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g610)
