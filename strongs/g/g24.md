# ἀγανάκτησις

[aganaktēsis](https://www.blueletterbible.org/lexicon/g24)

Definition: indignation (1x), irritation, vexation

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0024)

[Study Light](https://www.studylight.org/lexicons/greek/24.html)

[Bible Hub](https://biblehub.com/str/greek/24.htm)

[LSJ](https://lsj.gr/wiki/ἀγανάκτησις)

[NET Bible](http://classic.net.bible.org/strong.php?id=24)

[Bible Bento](https://biblebento.com/dictionary/G0024.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0024/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0024.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g24)
