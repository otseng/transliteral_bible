# ποιμήν

[poimēn](https://www.blueletterbible.org/lexicon/g4166)

Definition: shepherd (15x), Shepherd (2x), pastor (1x), herdsman

Part of speech: masculine noun

Occurs 18 times in 17 verses

Greek: [poieō](../g/g4160.md)

Hebrew: [ra`ah](../h/h7462.md), [ra'ah](../h/h7200.md)

Notes: The tasks of a Near Eastern shepherd were: - to watch for enemies trying to attack the sheep - to defend the sheep from attackers - to heal the wounded and sick sheep - to find and save lost or trapped sheep - to love them, sharing their lives and so earning their trust.

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4166)

[Study Light](https://www.studylight.org/lexicons/greek/4166.html)

[Bible Hub](https://biblehub.com/str/greek/4166.htm)

[LSJ](https://lsj.gr/wiki/ποιμήν)

[NET Bible](http://classic.net.bible.org/strong.php?id=4166)

[Bible Bento](https://biblebento.com/dictionary/G4166.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4166/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4166.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4166)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%BF%CE%B9%CE%BC%CE%AE%CE%BD)

[Saints and Shepherds](https://ofsaintsandshepherds.com/word-studies/leadership-roles/shepherds/poimen/)