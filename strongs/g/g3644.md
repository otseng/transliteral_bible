# ὀλοθρευτής

[olothreutēs](https://www.blueletterbible.org/lexicon/g3644)

Definition: destroyer (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3644)

[Study Light](https://www.studylight.org/lexicons/greek/3644.html)

[Bible Hub](https://biblehub.com/str/greek/3644.htm)

[LSJ](https://lsj.gr/wiki/ὀλοθρευτής)

[NET Bible](http://classic.net.bible.org/strong.php?id=3644)

[Bible Bento](https://biblebento.com/dictionary/G3644.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3644/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3644.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3644)
