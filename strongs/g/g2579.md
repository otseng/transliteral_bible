# κἄν

[kan](https://www.blueletterbible.org/lexicon/g2579)

Definition: though (4x), and if (3x), if but (2x), also if (1x), at the least (1x), and if so much as (1x), yet (1x).

Part of speech: particle

Occurs 13 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2579)

[Study Light](https://www.studylight.org/lexicons/greek/2579.html)

[Bible Hub](https://biblehub.com/str/greek/2579.htm)

[LSJ](https://lsj.gr/wiki/κἄν)

[NET Bible](http://classic.net.bible.org/strong.php?id=2579)

[Bible Bento](https://biblebento.com/dictionary/G2579.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2579/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2579.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2579)

