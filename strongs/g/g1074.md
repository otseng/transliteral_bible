# γενεά

[genea](https://www.blueletterbible.org/lexicon/g1074)

Definition: generation (37x), time (2x), age (2x), nation (1x)

Part of speech: feminine noun

Occurs 42 times in 37 verses

Derived words: generation, [Genea](https://en.wikipedia.org/wiki/Genea)

Greek: [genos](../g/g1085.md)

Hebrew: [mišpāḥâ](../h/h4940.md), [dôr](../h/h1755.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1074)

[Study Light](https://www.studylight.org/lexicons/greek/1074.html)

[Bible Hub](https://biblehub.com/str/greek/1074.htm)

[LSJ](https://lsj.gr/wiki/γενεά)

[NET Bible](http://classic.net.bible.org/strong.php?id=1074)

[Bible Bento](https://biblebento.com/dictionary/G1074.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1074/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1074.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1074)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B3%CE%B5%CE%BD%CE%B5%CE%AC)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/1074-genea-generation.htm)
