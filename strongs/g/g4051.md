# περίσσευμα

[perisseuma](https://www.blueletterbible.org/lexicon/g4051)

Definition: abundance (4x), that was left (1x), in which one delights, over and above, residue, remains

Part of speech: neuter noun

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4051)

[Study Light](https://www.studylight.org/lexicons/greek/4051.html)

[Bible Hub](https://biblehub.com/str/greek/4051.htm)

[LSJ](https://lsj.gr/wiki/περίσσευμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=4051)

[Bible Bento](https://biblebento.com/dictionary/G4051.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4051/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4051.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4051)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33455)