# εὐθυδρομέω

[euthydromeō](https://www.blueletterbible.org/lexicon/g2113)

Definition: come with a straight course (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2113)

[Study Light](https://www.studylight.org/lexicons/greek/2113.html)

[Bible Hub](https://biblehub.com/str/greek/2113.htm)

[LSJ](https://lsj.gr/wiki/εὐθυδρομέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2113)

[Bible Bento](https://biblebento.com/dictionary/G2113.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2113/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2113.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2113)
