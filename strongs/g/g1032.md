# βρύω

[bryō](https://www.blueletterbible.org/lexicon/g1032)

Definition: send forth (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1032)

[Study Light](https://www.studylight.org/lexicons/greek/1032.html)

[Bible Hub](https://biblehub.com/str/greek/1032.htm)

[LSJ](https://lsj.gr/wiki/βρύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1032)

[Bible Bento](https://biblebento.com/dictionary/G1032.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1032/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1032.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1032)

