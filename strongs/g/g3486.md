# Ναούμ

[Naoum](https://www.blueletterbible.org/lexicon/g3486)

Definition: Naum (1x), "consolation", son of Esli and father of Amos, in the genealogy of Christ

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/3486.html)

[LSJ](https://lsj.gr/wiki/Ναούμ)

[NET Bible](http://classic.net.bible.org/strong.php?id=3486)

[Bible Bento](https://biblebento.com/dictionary/G3486.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3486/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3486.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3486)

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3486)

