# καρποφορέω

[karpophoreō](https://www.blueletterbible.org/lexicon/g2592)

Definition: bring forth fruit (6x), bear fruit (1x), be fruitful (1x), to be fertile

Part of speech: verb

Occurs 8 times in 8 verses

Usage: [Karpophoreo.com](http://www.karpophoreo.com/)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2592)

[Study Light](https://www.studylight.org/lexicons/greek/2592.html)

[Bible Hub](https://biblehub.com/str/greek/2592.htm)

[LSJ](https://lsj.gr/wiki/καρποφορέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2592)

[Bible Bento](https://biblebento.com/dictionary/G2592.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2592/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2592.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2592)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33588)