# μασάομαι

[masaomai](https://www.blueletterbible.org/lexicon/g3145)

Definition: gnaw (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3145)

[Study Light](https://www.studylight.org/lexicons/greek/3145.html)

[Bible Hub](https://biblehub.com/str/greek/3145.htm)

[LSJ](https://lsj.gr/wiki/μασάομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=3145)

[Bible Bento](https://biblebento.com/dictionary/G3145.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3145/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3145.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3145)

