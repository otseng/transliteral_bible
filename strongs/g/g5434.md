# φρύγανον

[phryganon](https://www.blueletterbible.org/lexicon/g5434)

Definition: stick (1x), a dry stick, a twig, all dry sticks, bush wood, fire wood, or similar material used as fuel of straw, stubble

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5434)

[Study Light](https://www.studylight.org/lexicons/greek/5434.html)

[Bible Hub](https://biblehub.com/str/greek/5434.htm)

[LSJ](https://lsj.gr/wiki/φρύγανον)

[NET Bible](http://classic.net.bible.org/strong.php?id=5434)

[Bible Bento](https://biblebento.com/dictionary/G5434.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5434/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5434.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5434)
