# ἑτοίμως

[hetoimōs](https://www.blueletterbible.org/lexicon/g2093)

Definition: ready (3x).

Part of speech: adverb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2093)

[Study Light](https://www.studylight.org/lexicons/greek/2093.html)

[Bible Hub](https://biblehub.com/str/greek/2093.htm)

[LSJ](https://lsj.gr/wiki/ἑτοίμως)

[NET Bible](http://classic.net.bible.org/strong.php?id=2093)

[Bible Bento](https://biblebento.com/dictionary/G2093.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2093/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2093.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2093)
