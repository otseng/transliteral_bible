# καταβραβεύω

[katabrabeuō](https://www.blueletterbible.org/lexicon/g2603)

Definition: beguile of (one's) reward (1x), to defraud or beguile of the prize of victory

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2603)

[Study Light](https://www.studylight.org/lexicons/greek/2603.html)

[Bible Hub](https://biblehub.com/str/greek/2603.htm)

[LSJ](https://lsj.gr/wiki/καταβραβεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2603)

[Bible Bento](https://biblebento.com/dictionary/G2603.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2603/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2603.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2603)

[Sermon Index](https://www.sermonindex.net/modules/articles/index.php?view=article&aid=33824)
