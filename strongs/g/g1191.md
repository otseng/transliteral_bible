# Δέρβη

[Derbē](https://www.blueletterbible.org/lexicon/g1191)

Definition: Derbe (3x), "tanner: tanner of skin: coverer with skin", a city of Lycaonia, on the confines of Isauria

Part of speech: proper locative noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1191)

[Study Light](https://www.studylight.org/lexicons/greek/1191.html)

[Bible Hub](https://biblehub.com/str/greek/1191.htm)

[LSJ](https://lsj.gr/wiki/Δέρβη)

[NET Bible](http://classic.net.bible.org/strong.php?id=1191)

[Bible Bento](https://biblebento.com/dictionary/G1191.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1191/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1191.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1191)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/derbe.html)

[Video Bible](https://www.videobible.com/bible-dictionary/derbe)