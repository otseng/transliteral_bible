# μέτωπον

[metōpon](https://www.blueletterbible.org/lexicon/g3359)

Definition: forehead (8x).

Part of speech: neuter noun

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3359)

[Study Light](https://www.studylight.org/lexicons/greek/3359.html)

[Bible Hub](https://biblehub.com/str/greek/3359.htm)

[LSJ](https://lsj.gr/wiki/μέτωπον)

[NET Bible](http://classic.net.bible.org/strong.php?id=3359)

[Bible Bento](https://biblebento.com/dictionary/G3359.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3359/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3359.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3359)

