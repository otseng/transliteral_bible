# φάτνη

[phatnē](https://www.blueletterbible.org/lexicon/g5336)

Definition: manger (3x), stall (1x), crib for fodder

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5336)

[Study Light](https://www.studylight.org/lexicons/greek/5336.html)

[Bible Hub](https://biblehub.com/str/greek/5336.htm)

[LSJ](https://lsj.gr/wiki/φάτνη)

[NET Bible](http://classic.net.bible.org/strong.php?id=5336)

[Bible Bento](https://biblebento.com/dictionary/G5336.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5336/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5336.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5336)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%86%CE%AC%CF%84%CE%BD%CE%B7)

[Wikipedia](https://en.wikipedia.org/wiki/Manger)