# Ἡρῳδιανοί

[Hērōdianoi](https://www.blueletterbible.org/lexicon/g2265)

Definition: Herodians (3x)

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2265)

[Study Light](https://www.studylight.org/lexicons/greek/2265.html)

[Bible Hub](https://biblehub.com/str/greek/2265.htm)

[LSJ](https://lsj.gr/wiki/Ἡρῳδιανοί)

[NET Bible](http://classic.net.bible.org/strong.php?id=2265)

[Bible Bento](https://biblebento.com/dictionary/G2265.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2265/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2265.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2265)

[Wikipedia](https://en.wikipedia.org/wiki/Herodians)
