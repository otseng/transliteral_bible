# κληρονόμος

[klēronomos](https://www.blueletterbible.org/lexicon/g2818)

Definition: heir (15x), possessor

Part of speech: masculine noun

Occurs 15 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2818)

[Study Light](https://www.studylight.org/lexicons/greek/2818.html)

[Bible Hub](https://biblehub.com/str/greek/2818.htm)

[LSJ](https://lsj.gr/wiki/κληρονόμος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2818)

[Bible Bento](https://biblebento.com/dictionary/G2818.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2818/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2818.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2818)
