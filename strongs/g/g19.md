# ἀγαθωσύνη

[agathōsynē](https://www.blueletterbible.org/lexicon/g19)

Definition: goodness (4x), uprightness of heart and life, goodness, kindness

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0019)

[Study Light](https://www.studylight.org/lexicons/greek/19.html)

[Bible Hub](https://biblehub.com/str/greek/19.htm)

[LSJ](https://lsj.gr/wiki/ἀγαθωσύνη)

[NET Bible](http://classic.net.bible.org/strong.php?id=19)

[Bible Bento](https://biblebento.com/dictionary/G0019.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0019/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0019.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g19)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CE%B3%CE%B1%CE%B8%CF%89%CF%83%CF%8D%CE%BD%CE%B7)
