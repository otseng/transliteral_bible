# Ἀχάζ

[Achaz](https://www.blueletterbible.org/lexicon/g881)

Definition: Achaz (2x), "possessor"

Part of speech: proper masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0881)

[Study Light](https://www.studylight.org/lexicons/greek/881.html)

[Bible Hub](https://biblehub.com/str/greek/881.htm)

[LSJ](https://lsj.gr/wiki/Ἀχάζ)

[NET Bible](http://classic.net.bible.org/strong.php?id=881)

[Bible Bento](https://biblebento.com/dictionary/G0881.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0881/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0881.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g881)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/achaz.html)