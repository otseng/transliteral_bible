# ὑποπλέω

[hypopleō](https://www.blueletterbible.org/lexicon/g5284)

Definition: sail under (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5284)

[Study Light](https://www.studylight.org/lexicons/greek/5284.html)

[Bible Hub](https://biblehub.com/str/greek/5284.htm)

[LSJ](https://lsj.gr/wiki/ὑποπλέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5284)

[Bible Bento](https://biblebento.com/dictionary/G5284.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5284/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5284.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5284)

