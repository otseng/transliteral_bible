# ζόφος

[zophos](https://www.blueletterbible.org/lexicon/g2217)

Definition: darkness (2x), mist (1x), blackness (1x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2217)

[Study Light](https://www.studylight.org/lexicons/greek/2217.html)

[Bible Hub](https://biblehub.com/str/greek/2217.htm)

[LSJ](https://lsj.gr/wiki/ζόφος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2217)

[Bible Bento](https://biblebento.com/dictionary/G2217.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2217/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2217.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2217)

