# μετάθεσις

[metathesis](https://www.blueletterbible.org/lexicon/g3331)

Definition: change (1x), translation (1x), removing (1x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3331)

[Study Light](https://www.studylight.org/lexicons/greek/3331.html)

[Bible Hub](https://biblehub.com/str/greek/3331.htm)

[LSJ](https://lsj.gr/wiki/μετάθεσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=3331)

[Bible Bento](https://biblebento.com/dictionary/G3331.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3331/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3331.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3331)
