# Λεββαῖος

[Lebbaios](https://www.blueletterbible.org/lexicon/g3002)

Definition: Lebbaeus (1x), Lebbaeus = "a man of heart", one name of Jude

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3002)

[Study Light](https://www.studylight.org/lexicons/greek/3002.html)

[Bible Hub](https://biblehub.com/str/greek/3002.htm)

[LSJ](https://lsj.gr/wiki/Λεββαῖος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3002)

[Bible Bento](https://biblebento.com/dictionary/G3002.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3002/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3002.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3002)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/L/lebbaeus.html)