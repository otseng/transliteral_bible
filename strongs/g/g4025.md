# περίθεσις

[perithesis](https://www.blueletterbible.org/lexicon/g4025)

Definition: wearing (1x), the adornment consisting of the golden ornaments wont to be placed around the head or the body

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4025)

[Study Light](https://www.studylight.org/lexicons/greek/4025.html)

[Bible Hub](https://biblehub.com/str/greek/4025.htm)

[LSJ](https://lsj.gr/wiki/περίθεσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=4025)

[Bible Bento](https://biblebento.com/dictionary/G4025.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4025/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4025.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4025)

