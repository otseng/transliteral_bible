# ἱλαρότης

[hilarotēs](https://www.blueletterbible.org/lexicon/g2432)

Definition: cheerfulness (1x), alacrity

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2432)

[Study Light](https://www.studylight.org/lexicons/greek/2432.html)

[Bible Hub](https://biblehub.com/str/greek/2432.htm)

[LSJ](https://lsj.gr/wiki/ἱλαρότης)

[NET Bible](http://classic.net.bible.org/strong.php?id=2432)

[Bible Bento](https://biblebento.com/dictionary/G2432.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2432/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2432.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2432)
