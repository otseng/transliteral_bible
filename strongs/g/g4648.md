# σκοπέω

[skopeō](https://www.blueletterbible.org/lexicon/g4648)

Definition: mark (2x), take heed (1x), look on (1x), look at (1x), consider (1x), to view closely, to examine, to fix one’s eyes upon, to observe, to behold, to contemplate, to mark, to pay attention, aim at, to direct one’s attention upon a thing, to look at critically, spy out

Part of speech: verb

Occurs 8 times in 6 verses

Greek: [episkopos](../g/g1985.md), [allotriepiskopos](../g/g244.md)

Derived words: scope, microscope, telescope, horoscope, gyroscope, [scopophobia](https://en.wikipedia.org/wiki/Scopophobia), [-scope](https://www.thefreedictionary.com/words-that-end-in-scope), [-scopy](https://www.thefreedictionary.com/words-that-end-in-scopy)

Synonyms: [look](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Look)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4648)

[Study Light](https://www.studylight.org/lexicons/greek/4648.html)

[Bible Hub](https://biblehub.com/str/greek/4648.htm)

[LSJ](https://lsj.gr/wiki/σκοπέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4648)

[Bible Bento](https://biblebento.com/dictionary/G4648.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4648/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4648.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4648)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CE%BA%CE%BF%CF%80%CE%AD%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34388)

[Precept Austin](https://www.preceptaustin.org/philippians_317-21#observe)
