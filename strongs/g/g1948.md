# ἐπικρίνω

[epikrinō](https://www.blueletterbible.org/lexicon/g1948)

Definition: give sentence (1x), to adjudge, approve by one's decision, decree

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1948)

[Study Light](https://www.studylight.org/lexicons/greek/1948.html)

[Bible Hub](https://biblehub.com/str/greek/1948.htm)

[LSJ](https://lsj.gr/wiki/ἐπικρίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1948)

[Bible Bento](https://biblebento.com/dictionary/G1948.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1948/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1948.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1948)
