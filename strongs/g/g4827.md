# συμμαθητής

[symmathētēs](https://www.blueletterbible.org/lexicon/g4827)

Definition: fellowdisciples (1x), a fellow disciple

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4827)

[Study Light](https://www.studylight.org/lexicons/greek/4827.html)

[Bible Hub](https://biblehub.com/str/greek/4827.htm)

[LSJ](https://lsj.gr/wiki/συμμαθητής)

[NET Bible](http://classic.net.bible.org/strong.php?id=4827)

[Bible Bento](https://biblebento.com/dictionary/G4827.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4827/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4827.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4827)