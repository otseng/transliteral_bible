# συνάγω

[synagō](https://www.blueletterbible.org/lexicon/g4863)

Definition: gather (15x), be gathered together (12x), gather together (9x), come together (6x), be gathered (4x), be assembled (3x), take in (3x), assemble, collect

Part of speech: verb

Derived words: synagogue

Occurs 73 times in 62 verses

Hebrew: ['āsap̄](../h/h622.md), [qāhal](../h/h6950.md)

Derived words: [synergy](https://www.dictionary.com/browse/synergy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4863)

[Study Light](https://www.studylight.org/lexicons/greek/4863.html)

[Bible Hub](https://biblehub.com/str/greek/4863.htm)

[LSJ](https://lsj.gr/wiki/συνάγω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4863)

[Bible Bento](https://biblebento.com/dictionary/G4863.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4863/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4863.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4863)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CF%85%CE%BD%CE%AC%CE%B3%CF%89)

[The Continuum](https://anglicancontinuum.blogspot.com/2011/08/synago.html)
