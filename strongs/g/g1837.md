# ἐξηχέω

[exēcheō](https://www.blueletterbible.org/lexicon/g1837)

Definition: sound out (1x), emit, sound, resound

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1837)

[Study Light](https://www.studylight.org/lexicons/greek/1837.html)

[Bible Hub](https://biblehub.com/str/greek/1837.htm)

[LSJ](https://lsj.gr/wiki/ἐξηχέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1837)

[Bible Bento](https://biblebento.com/dictionary/G1837.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1837/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1837.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1837)
