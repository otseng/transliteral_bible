# κόμη

[komē](https://www.blueletterbible.org/lexicon/g2864)

Definition: hair (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2864)

[Study Light](https://www.studylight.org/lexicons/greek/2864.html)

[Bible Hub](https://biblehub.com/str/greek/2864.htm)

[LSJ](https://lsj.gr/wiki/κόμη)

[NET Bible](http://classic.net.bible.org/strong.php?id=2864)

[Bible Bento](https://biblebento.com/dictionary/G2864.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2864/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2864.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2864)
