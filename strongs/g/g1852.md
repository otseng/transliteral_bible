# ἐξυπνίζω

[exypnizō](https://www.blueletterbible.org/lexicon/g1852 )

Definition: awake out of sleep (1x)

Part of speech: verb

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1852)

[Study Light](https://www.studylight.org/lexicons/greek/1852.html)

[Bible Hub](https://biblehub.com/str/greek/1852.htm)

[LSJ](https://lsj.gr/wiki/ἐξυπνίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1852)

[Bible Bento](https://biblebento.com/dictionary/G1852.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1852/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1852.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1852)