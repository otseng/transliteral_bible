# σοφός

[sophos](https://www.blueletterbible.org/lexicon/g4680)

Definition: wise (22x), skilled, expert, learned

Part of speech: adjective

Occurs 22 times in 21 verses

Greek: [sophia](https://www.blueletterbible.org/lexicon/g4678)

Hebrew: [ḥakîm](../h/h2445.md), [ḥāḵām](../h/h2450.md)

Synonyms: [wise](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Wise)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4680)

[Study Light](https://www.studylight.org/lexicons/greek/4680.html)

[Bible Hub](https://biblehub.com/str/greek/4680.htm)

[LSJ](https://lsj.gr/wiki/σοφός)

[NET Bible](http://classic.net.bible.org/strong.php?id=4680)

[Bible Bento](https://biblebento.com/dictionary/G4680.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4680/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4680.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4680)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/4680-sophos-wise.htm)
