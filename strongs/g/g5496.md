# χειραγωγέω

[cheiragōgeō](https://www.blueletterbible.org/lexicon/g5496)

Definition: lead by the hand (2x)

Part of speech: verb

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5496)

[Study Light](https://www.studylight.org/lexicons/greek/5496.html)

[Bible Hub](https://biblehub.com/str/greek/5496.htm)

[LSJ](https://lsj.gr/wiki/χειραγωγέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5496)

[Bible Bento](https://biblebento.com/dictionary/G5496.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5496/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5496.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5496)
