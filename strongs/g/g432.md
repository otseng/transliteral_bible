# ἄνηθον

[anēthon](https://www.blueletterbible.org/lexicon/g432)

Definition: anise (1x), dill, plant used as a spice and for medicine

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0432)

[Study Light](https://www.studylight.org/lexicons/greek/432.html)

[Bible Hub](https://biblehub.com/str/greek/432.htm)

[LSJ](https://lsj.gr/wiki/ἄνηθον)

[NET Bible](http://classic.net.bible.org/strong.php?id=432)

[Bible Bento](https://biblebento.com/dictionary/G0432.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0432/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0432.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g432)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%84%CE%BD%CE%B7%CE%B8%CE%BF%CE%BD)

[Wikipedia](https://en.wikipedia.org/wiki/Dill)
