# ἀφοράω

[aphoraō](https://www.blueletterbible.org/lexicon/g872)

Definition: look (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g872)

[Study Light](https://www.studylight.org/lexicons/greek/872.html)

[Bible Hub](https://biblehub.com/str/greek/872.htm)

[LSJ](https://lsj.gr/wiki/ἀφοράω)

[NET Bible](http://classic.net.bible.org/strong.php?id=872)

[Bible Bento](https://biblebento.com/dictionary/G872.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/872/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/872.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g872)

