# Ἀδάμ

[Adam](https://www.blueletterbible.org/lexicon/g76)

Definition: Adam (9x), "the red earth", the first man, the parent of the whole human family

Part of speech: proper masculine noun

Occurs 9 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0076)

[Study Light](https://www.studylight.org/lexicons/greek/76.html)

[Bible Hub](https://biblehub.com/str/greek/76.htm)

[LSJ](https://lsj.gr/wiki/Ἀδάμ)

[NET Bible](http://classic.net.bible.org/strong.php?id=76)

[Bible Bento](https://biblebento.com/dictionary/G0076.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0076/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0076.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g76)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%88%CE%B4%CE%AC%CE%BC)

[Wikipedia](https://en.wikipedia.org/wiki/Adam)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/adam.html)

[Video Bible](https://www.videobible.com/bible-dictionary/adam)