# εὐφημία

[euphēmia](https://www.blueletterbible.org/lexicon/g2162)

Definition: good report (1x), praise

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2162)

[Study Light](https://www.studylight.org/lexicons/greek/2162.html)

[Bible Hub](https://biblehub.com/str/greek/2162.htm)

[LSJ](https://lsj.gr/wiki/εὐφημία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2162)

[Bible Bento](https://biblebento.com/dictionary/G2162.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2162/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2162.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2162)
