# φύω

[phyō](https://www.blueletterbible.org/lexicon/g5453)

Definition: spring up (1x), spring (1x), as soon as it be sprung up (1x), beget, bring forth, produce, germinate, grow

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5453)

[Study Light](https://www.studylight.org/lexicons/greek/5453.html)

[Bible Hub](https://biblehub.com/str/greek/5453.htm)

[LSJ](https://lsj.gr/wiki/φύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5453)

[Bible Bento](https://biblebento.com/dictionary/G5453.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5453/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5453.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5453)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%86%CF%8D%CF%89)