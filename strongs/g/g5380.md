# φιλόνεικος

[philoneikos](https://www.blueletterbible.org/lexicon/g5380)

Definition: contentious (1x), fond of strife

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5380)

[Study Light](https://www.studylight.org/lexicons/greek/5380.html)

[Bible Hub](https://biblehub.com/str/greek/5380.htm)

[LSJ](https://lsj.gr/wiki/φιλόνεικος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5380)

[Bible Bento](https://biblebento.com/dictionary/G5380.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5380/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5380.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5380)
