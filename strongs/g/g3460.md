# Μύρα

[Myra](https://www.blueletterbible.org/lexicon/g3460)

Definition: Myra (1x), "myrrh: myrtle juice", an important town in Lycia, on the southwest coast of Asia Minor, on the river Andriacus, 2.5 miles (4 km) from the mouth

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3460)

[Study Light](https://www.studylight.org/lexicons/greek/3460.html)

[Bible Hub](https://biblehub.com/str/greek/3460.htm)

[LSJ](https://lsj.gr/wiki/Μύρα)

[NET Bible](http://classic.net.bible.org/strong.php?id=3460)

[Bible Bento](https://biblebento.com/dictionary/G3460.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3460/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3460.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3460)

[Wikipedia](https://en.wikipedia.org/wiki/Myra)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/myra.html)

[Video Bible](https://www.videobible.com/bible-dictionary/myra)