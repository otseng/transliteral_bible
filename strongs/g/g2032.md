# ἐπουράνιος

[epouranios](https://www.blueletterbible.org/lexicon/g2032)

Definition: heavenly (16x), celestial (2x), in heaven (1x), high (1x), above the sky

Part of speech: adjective

Occurs 21 times in 18 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2032)

[Study Light](https://www.studylight.org/lexicons/greek/2032.html)

[Bible Hub](https://biblehub.com/str/greek/2032.htm)

[LSJ](https://lsj.gr/wiki/ἐπουράνιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2032)

[Bible Bento](https://biblebento.com/dictionary/G2032.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2032/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2032.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2032)

[Faithlife](https://sermons.faithlife.com/sermons/261923-a-glimpse-of-heaven)