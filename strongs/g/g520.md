# ἀπάγω

[apagō](https://www.blueletterbible.org/lexicon/g520)

Definition: lead away (10x), lead (2x), put to death (1x), bring (1x), take away (1x), carry away (1x), take away to trial, abduct

Part of speech: verb

Occurs 21 times in 16 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0520)

[Study Light](https://www.studylight.org/lexicons/greek/520.html)

[Bible Hub](https://biblehub.com/str/greek/520.htm)

[LSJ](https://lsj.gr/wiki/ἀπάγω)

[NET Bible](http://classic.net.bible.org/strong.php?id=520)

[Bible Bento](https://biblebento.com/dictionary/G0520.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0520/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0520.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g520)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CF%80%CE%AC%CE%B3%CF%89)