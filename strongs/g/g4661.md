# σκῦλον

[skylon](https://www.blueletterbible.org/lexicon/g4661)

Definition: spoils (1x), the weapons and valuables stripped off from an enemy

Part of speech: neuter noun

Occurs 1 times in 1 verses

Hebrew: [šālāl](../h/h7998.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4661)

[Study Light](https://www.studylight.org/lexicons/greek/4661.html)

[Bible Hub](https://biblehub.com/str/greek/4661.htm)

[LSJ](https://lsj.gr/wiki/σκῦλον)

[NET Bible](http://classic.net.bible.org/strong.php?id=4661)

[Bible Bento](https://biblebento.com/dictionary/G4661.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4661/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4661.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4661)