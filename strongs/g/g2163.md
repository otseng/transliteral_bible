# εὔφημος

[euphēmos](https://www.blueletterbible.org/lexicon/g2163)

Definition: of good report (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2163)

[Study Light](https://www.studylight.org/lexicons/greek/2163.html)

[Bible Hub](https://biblehub.com/str/greek/2163.htm)

[LSJ](https://lsj.gr/wiki/εὔφημος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2163)

[Bible Bento](https://biblebento.com/dictionary/G2163.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2163/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2163.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2163)

