# καταποντίζω

[katapontizō](https://www.blueletterbible.org/lexicon/g2670)

Definition: sink (1x), drown (1x), to plunge or sink into the sea

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2670)

[Study Light](https://www.studylight.org/lexicons/greek/2670.html)

[Bible Hub](https://biblehub.com/str/greek/2670.htm)

[LSJ](https://lsj.gr/wiki/καταποντίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2670)

[Bible Bento](https://biblebento.com/dictionary/G2670.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2670/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2670.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2670)
