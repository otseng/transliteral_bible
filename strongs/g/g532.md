# ἀπαρασκεύαστος

[aparaskeuastos](https://www.blueletterbible.org/lexicon/g532)

Definition: unprepared (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0532)

[Study Light](https://www.studylight.org/lexicons/greek/532.html)

[Bible Hub](https://biblehub.com/str/greek/532.htm)

[LSJ](https://lsj.gr/wiki/ἀπαρασκεύαστος)

[NET Bible](http://classic.net.bible.org/strong.php?id=532)

[Bible Bento](https://biblebento.com/dictionary/G0532.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0532/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0532.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g532)
