# ἀβαρής

[abarēs](https://www.blueletterbible.org/lexicon/g4)

Definition: not burdensome (1x), not heavy

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0004)

[Study Light](https://www.studylight.org/lexicons/greek/4.html)

[Bible Hub](https://biblehub.com/str/greek/4.htm)

[LSJ](https://lsj.gr/wiki/ἀβαρής)

[NET Bible](http://classic.net.bible.org/strong.php?id=4)

[Bible Bento](https://biblebento.com/dictionary/G0004.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0004/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0004.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4)
