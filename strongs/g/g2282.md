# θάλπω

[thalpō](https://www.blueletterbible.org/lexicon/g2282)

Definition: cherish (2x), keep warm, brood

Part of speech: verb

Occurs 2 times in 2 verses

Hebrew: [sāḵan](../h/h5532.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2282)

[Study Light](https://www.studylight.org/lexicons/greek/2282.html)

[Bible Hub](https://biblehub.com/str/greek/2282.htm)

[LSJ](https://lsj.gr/wiki/θάλπω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2282)

[Bible Bento](https://biblebento.com/dictionary/G2282.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2282/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2282.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2282)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B8%CE%AC%CE%BB%CF%80%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=35947)

[Precept Austin](https://www.preceptaustin.org/ephesians_528-30#cherishes)