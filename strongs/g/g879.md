# ἀφυπνόω

[aphypnoō](https://www.blueletterbible.org/lexicon/g879)

Definition: fall asleep (1x), to fall off to sleep

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0879)

[Study Light](https://www.studylight.org/lexicons/greek/879.html)

[Bible Hub](https://biblehub.com/str/greek/879.htm)

[LSJ](https://lsj.gr/wiki/ἀφυπνόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=879)

[Bible Bento](https://biblebento.com/dictionary/G0879.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0879/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0879.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g879)