# κωφός

[kōphos](https://www.blueletterbible.org/lexicon/g2974)

Definition: dumb (8x), deaf (5x), speechless (1x), blunted, dull in hearing

Part of speech: adjective

Occurs 14 times in 12 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2974)

[Study Light](https://www.studylight.org/lexicons/greek/2974.html)

[Bible Hub](https://biblehub.com/str/greek/2974.htm)

[LSJ](https://lsj.gr/wiki/κωφός)

[NET Bible](http://classic.net.bible.org/strong.php?id=2974)

[Bible Bento](https://biblebento.com/dictionary/G2974.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2974/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2974.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2974)

[Wiktionary](https://en.wiktionary.org/wiki/κουφός)