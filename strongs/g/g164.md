# αἰχμάλωτος

[aichmalōtos](https://www.blueletterbible.org/lexicon/g164)

Definition: captive (1x), prisoner of war

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0164)

[Study Light](https://www.studylight.org/lexicons/greek/164.html)

[Bible Hub](https://biblehub.com/str/greek/164.htm)

[LSJ](https://lsj.gr/wiki/αἰχμάλωτος)

[NET Bible](http://classic.net.bible.org/strong.php?id=164)

[Bible Bento](https://biblebento.com/dictionary/G0164.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0164/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0164.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g164)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%E1%BC%B0%CF%87%CE%BC%CE%AC%CE%BB%CF%89%CF%84%CE%BF%CF%82)