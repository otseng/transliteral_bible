# παλιγγενεσία

[paliggenesia](https://www.blueletterbible.org/lexicon/g3824)

Definition: regeneration (2x), new birth, reproduction, renewal, recreation, Messianic restoration, the renewal of the world to take place after its destruction by fire

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3824)

[Study Light](https://www.studylight.org/lexicons/greek/3824.html)

[Bible Hub](https://biblehub.com/str/greek/3824.htm)

[LSJ](https://lsj.gr/wiki/παλιγγενεσία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3824)

[Bible Bento](https://biblebento.com/dictionary/G3824.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3824/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3824.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3824)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34759)

[Precept Austin](https://www.preceptaustin.org/titus_34-8#regeneration)