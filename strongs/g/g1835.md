# ἑξήκοντα

[hexēkonta](https://www.blueletterbible.org/lexicon/g1835)

Definition: threescore (4x), sixty (3x), sixtyfold (1x).

Part of speech: indeclinable noun

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1835)

[Study Light](https://www.studylight.org/lexicons/greek/1835.html)

[Bible Hub](https://biblehub.com/str/greek/1835.htm)

[LSJ](https://lsj.gr/wiki/ἑξήκοντα)

[NET Bible](http://classic.net.bible.org/strong.php?id=1835)

[Bible Bento](https://biblebento.com/dictionary/G1835.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1835/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1835.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1835)

