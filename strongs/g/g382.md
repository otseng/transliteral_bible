# ἀναρίθμητος

[anarithmētos](https://www.blueletterbible.org/lexicon/g382)

Definition: innumerable (1x), without number

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0382)

[Study Light](https://www.studylight.org/lexicons/greek/382.html)

[Bible Hub](https://biblehub.com/str/greek/382.htm)

[LSJ](https://lsj.gr/wiki/ἀναρίθμητος)

[NET Bible](http://classic.net.bible.org/strong.php?id=382)

[Bible Bento](https://biblebento.com/dictionary/G382.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/382/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/382.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g382)
