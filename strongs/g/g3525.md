# νήφω

[nēphō](https://www.blueletterbible.org/lexicon/g3525)

Definition: be sober (4x), watch (2x), to be temperate, dispassionate, circumspect, to abstain from wine, to be calm and collected in spirit

Part of speech: verb

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3525)

[Study Light](https://www.studylight.org/lexicons/greek/3525.html)

[Bible Hub](https://biblehub.com/str/greek/3525.htm)

[LSJ](https://lsj.gr/wiki/νήφω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3525)

[Bible Bento](https://biblebento.com/dictionary/G3525.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3525/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3525.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3525)
