# ἀρχισυνάγωγος

[archisynagōgos](https://www.blueletterbible.org/lexicon/g752)

Definition: ruler of the synagogue (7x), chief ruler of the synagogue (2x)

Part of speech: masculine noun

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0752)

[Study Light](https://www.studylight.org/lexicons/greek/752.html)

[Bible Hub](https://biblehub.com/str/greek/752.htm)

[LSJ](https://lsj.gr/wiki/ἀρχισυνάγωγος)

[NET Bible](http://classic.net.bible.org/strong.php?id=752)

[Bible Bento](https://biblebento.com/dictionary/G0752.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0752/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0752.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g752)