# θεῖος

[theios](https://www.blueletterbible.org/lexicon/g2304)

Definition: divine (2x), Godhead (1x), a general name of deities or divinities as used by the Greeks, godlike

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2304)

[Study Light](https://www.studylight.org/lexicons/greek/2304.html)

[Bible Hub](https://biblehub.com/str/greek/2304.htm)

[LSJ](https://lsj.gr/wiki/θεῖος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2304)

[Bible Bento](https://biblebento.com/dictionary/G2304.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2304/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2304.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2304)
