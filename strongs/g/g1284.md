# διαρρήσσω

[diarrēssō](https://www.blueletterbible.org/lexicon/g1284)

Definition: rend (3x), break (2x), tear

Part of speech: verb

Occurs 5 times in 5 verses

Hebrew: [qāraʿ](../h/h7167.md), [dāḵā'](../h/h1792.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1284)

[Study Light](https://www.studylight.org/lexicons/greek/1284.html)

[Bible Hub](https://biblehub.com/str/greek/1284.htm)

[LSJ](https://lsj.gr/wiki/διαρρήσσω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1284)

[Bible Bento](https://biblebento.com/dictionary/G1284.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1284/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1284.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1284)