# βασκαίνω

[baskainō](https://www.blueletterbible.org/lexicon/g940)

Definition: bewitch (1x), to bring evil on one by feigning praise or an evil eye, to slander, traduce him, give or put the evil eye

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0940)

[Study Light](https://www.studylight.org/lexicons/greek/940.html)

[Bible Hub](https://biblehub.com/str/greek/940.htm)

[LSJ](https://lsj.gr/wiki/βασκαίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=940)

[Bible Bento](https://biblebento.com/dictionary/G0940.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/940/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/940.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g940)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B2%CE%B1%CF%83%CE%BA%CE%B1%CE%AF%CE%BD%CF%89)
