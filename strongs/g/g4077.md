# πηγή

[pēgē](https://www.blueletterbible.org/lexicon/g4077)

Definition: fountain (8x), well (4x), source, supply

Part of speech: feminine noun

Occurs 12 times in 11 verses

Hebrew: [māqôr](../h/h4726.md), [bowr](../h/h953.md), [maʿyān](../h/h4599.md)

Derived words: [Pegasus](https://en.wikipedia.org/wiki/Pegasus), [Pegasides](https://en.wikipedia.org/wiki/Pegasides), [pegomancy](https://en.wiktionary.org/wiki/pegomancy), [pega-](https://www.thefreedictionary.com/words-that-start-with-pega)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4077)

[Study Light](https://www.studylight.org/lexicons/greek/4077.html)

[Bible Hub](https://biblehub.com/str/greek/4077.htm)

[LSJ](https://lsj.gr/wiki/πηγή)

[NET Bible](http://classic.net.bible.org/strong.php?id=4077)

[Bible Bento](https://biblebento.com/dictionary/G4077.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4077/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4077.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4077)