# ἀντίλημψις

[antilēmpsis](https://www.blueletterbible.org/lexicon/g484)

Definition: help (1x), aid, mutual acceptance

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0484)

[Study Light](https://www.studylight.org/lexicons/greek/484.html)

[Bible Hub](https://biblehub.com/str/greek/484.htm)

[LSJ](https://lsj.gr/wiki/ἀντίλημψις)

[NET Bible](http://classic.net.bible.org/strong.php?id=484)

[Bible Bento](https://biblebento.com/dictionary/G0484.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0484/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0484.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g484)
