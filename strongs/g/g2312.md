# θεοδίδακτος

[theodidaktos](https://www.blueletterbible.org/lexicon/g2312)

Definition: taught of God (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2312)

[Study Light](https://www.studylight.org/lexicons/greek/2312.html)

[Bible Hub](https://biblehub.com/str/greek/2312.htm)

[LSJ](https://lsj.gr/wiki/θεοδίδακτος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2312)

[Bible Bento](https://biblebento.com/dictionary/G2312.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2312/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2312.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2312)

