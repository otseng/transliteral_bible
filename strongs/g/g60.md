# ἀγοραῖος

[agoraios](https://www.blueletterbible.org/lexicon/g60)

Definition: baser sort (1x), law (1x), of or belonging to the market place, hucksters, petty traffickers, retail dealers, idlers, loungers, the common sort, low, mean vulgar

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0060)

[Study Light](https://www.studylight.org/lexicons/greek/60.html)

[Bible Hub](https://biblehub.com/str/greek/60.htm)

[LSJ](https://lsj.gr/wiki/ἀγοραῖος)

[NET Bible](http://classic.net.bible.org/strong.php?id=60)

[Bible Bento](https://biblebento.com/dictionary/G0060.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0060/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0060.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g60)
