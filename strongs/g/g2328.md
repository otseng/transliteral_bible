# θερμαίνω

[thermainō](https://www.blueletterbible.org/lexicon/g2328)

Definition: warm (one's) self (5x), be warmed (1x), make warm, to heat

Part of speech: verb

Occurs 6 times in 5 verses

Hebrew: [yāḥam](../h/h3179.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2328)

[Study Light](https://www.studylight.org/lexicons/greek/2328.html)

[Bible Hub](https://biblehub.com/str/greek/2328.htm)

[LSJ](https://lsj.gr/wiki/θερμαίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2328)

[Bible Bento](https://biblebento.com/dictionary/G2328.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2328/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2328.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2328)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B8%CE%B5%CF%81%CE%BC%CE%B1%CE%AF%CE%BD%CF%89)