# συμμιμητής

[symmimētēs](https://www.blueletterbible.org/lexicon/g4831)

Definition: follower together (1x), an imitator of others, follower together.

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4831)

[Study Light](https://www.studylight.org/lexicons/greek/4831.html)

[Bible Hub](https://biblehub.com/str/greek/4831.htm)

[LSJ](https://lsj.gr/wiki/συμμιμητής)

[NET Bible](http://classic.net.bible.org/strong.php?id=4831)

[Bible Bento](https://biblebento.com/dictionary/G4831.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4831/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4831.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4831)
