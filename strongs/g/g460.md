# ἀνόμως

[anomōs](https://www.blueletterbible.org/lexicon/g460)

Definition: without law (2x), without the knowledge of the law, lawlessly

Part of speech: adverb

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0460)

[Study Light](https://www.studylight.org/lexicons/greek/460.html)

[Bible Hub](https://biblehub.com/str/greek/460.htm)

[LSJ](https://lsj.gr/wiki/ἀνόμως)

[NET Bible](http://classic.net.bible.org/strong.php?id=460)

[Bible Bento](https://biblebento.com/dictionary/G0460.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0460/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0460.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g460)
