# κνήθω

[knēthō](https://www.blueletterbible.org/lexicon/g2833)

Definition: have itching (1x), to scratch, tickle, make to itch, desirous of hearing something pleasant

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2833)

[Study Light](https://www.studylight.org/lexicons/greek/2833.html)

[Bible Hub](https://biblehub.com/str/greek/2833.htm)

[LSJ](https://lsj.gr/wiki/κνήθω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2833)

[Bible Bento](https://biblebento.com/dictionary/G2833.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2833/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2833.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2833)
