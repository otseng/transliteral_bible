# ἐνώπιον

[enōpion](https://www.blueletterbible.org/lexicon/g1799)

Definition: before (64x), in the sight of (16x), in the presence of (7x), in (one's) sight (5x), in (one's) presence (2x), to (1x), not translated (2x), in the sight of, in the presence of, face to face

Part of speech: preposition

Occurs 98 times in 89 verses

Greek: [optanomai](../g/g3700.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1799)

[Study Light](https://www.studylight.org/lexicons/greek/1799.html)

[Bible Hub](https://biblehub.com/str/greek/1799.htm)

[LSJ](https://lsj.gr/wiki/ἐνώπιον)

[NET Bible](http://classic.net.bible.org/strong.php?id=1799)

[Bible Bento](https://biblebento.com/dictionary/G1799.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1799/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1799.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1799)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%90%CE%BD%CF%8E%CF%80%CE%B9%CE%BF%CE%BD)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34648)