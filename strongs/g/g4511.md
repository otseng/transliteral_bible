# ῥύσις

[rhysis](https://www.blueletterbible.org/lexicon/g4511)

Definition: issue (3x), flowing issue, flow

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4511)

[Study Light](https://www.studylight.org/lexicons/greek/4511.html)

[Bible Hub](https://biblehub.com/str/greek/4511.htm)

[LSJ](https://lsj.gr/wiki/ῥύσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=4511)

[Bible Bento](https://biblebento.com/dictionary/G4511.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4511/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4511.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4511)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BF%A5%CF%8D%CF%83%CE%B9%CF%82)