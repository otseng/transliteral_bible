# καταξιόω

[kataxioō](https://www.blueletterbible.org/lexicon/g2661)

Definition: count worthy (2x), account worthy (2x), to account worthy, judge worthy, to deem entirely deserving

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2661)

[Study Light](https://www.studylight.org/lexicons/greek/2661.html)

[Bible Hub](https://biblehub.com/str/greek/2661.htm)

[LSJ](https://lsj.gr/wiki/καταξιόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2661)

[Bible Bento](https://biblebento.com/dictionary/G2661.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2661/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2661.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2661)

[Bob Wilkin](https://faithalone.org/magazine/y1989/89june3.html)