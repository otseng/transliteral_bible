# ἄγνωστος

[agnōstos](https://www.blueletterbible.org/lexicon/g57)

Definition: unknown (1x), unknowable, unintelligible, unrecognizable

Part of speech: adjective

Occurs 1 times in 1 verses

Derived words: agnostic

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0057)

[Study Light](https://www.studylight.org/lexicons/greek/57.html)

[Bible Hub](https://biblehub.com/str/greek/57.htm)

[LSJ](https://lsj.gr/wiki/ἄγνωστος)

[NET Bible](http://classic.net.bible.org/strong.php?id=57)

[Bible Bento](https://biblebento.com/dictionary/G0057.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0057/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0057.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g57)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%84%CE%B3%CE%BD%CF%89%CF%83%CF%84%CE%BF%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Unknown_God)

[Precept Austin](https://www.preceptaustin.org/acts-17-commentary#u)
