# Πάφος

[Paphos](https://www.blueletterbible.org/lexicon/g3974)

Definition: Paphos (2x), "boiling or hot", a maritime city on the west end of Cyprus, with a harbour. It was the residence of a Roman proconsul. "Old Paphos" was noted for the worship and shrine of Venus (Aphrodite) and lay some 7 miles (10 km) to the south-east of it.

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3974)

[Study Light](https://www.studylight.org/lexicons/greek/3974.html)

[Bible Hub](https://biblehub.com/str/greek/3974.htm)

[LSJ](https://lsj.gr/wiki/Πάφος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3974)

[Bible Bento](https://biblebento.com/dictionary/G3974.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3974/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3974.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3974)

[Wiktionary](https://en.wiktionary.org/wiki/Paphos)

[Bible Places](https://www.bibleplaces.com/paphos-cyprus/)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/paphos.html)

[Video Bible](https://www.videobible.com/bible-dictionary/paphos)