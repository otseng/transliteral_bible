# ἐμπλοκή

[emplokē](https://www.blueletterbible.org/lexicon/g1708)

Definition: plaiting (1x), an interweaving, braiding, a knot, an elaborate gathering of one's hair into knots

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1708)

[Study Light](https://www.studylight.org/lexicons/greek/1708.html)

[Bible Hub](https://biblehub.com/str/greek/1708.htm)

[LSJ](https://lsj.gr/wiki/ἐμπλοκή)

[NET Bible](http://classic.net.bible.org/strong.php?id=1708)

[Bible Bento](https://biblebento.com/dictionary/G1708.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1708/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1708.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1708)

