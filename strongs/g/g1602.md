# ἐκπλέω

[ekpleō](https://www.blueletterbible.org/lexicon/g1602)

Definition: sail (1x), sail thence (1x), sail away (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1602)

[Study Light](https://www.studylight.org/lexicons/greek/1602.html)

[Bible Hub](https://biblehub.com/str/greek/1602.htm)

[LSJ](https://lsj.gr/wiki/ἐκπλέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1602)

[Bible Bento](https://biblebento.com/dictionary/G1602.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1602/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1602.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1602)

