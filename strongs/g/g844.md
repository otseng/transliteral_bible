# αὐτόματος

[automatos](https://www.blueletterbible.org/lexicon/g844)

Definition: of (one's) self (1x), of (one's) own accord (1x).

Part of speech: adjective

Occurs 2 times in 2 verses

Derived words: automaton, automatic

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0844)

[Study Light](https://www.studylight.org/lexicons/greek/844.html)

[Bible Hub](https://biblehub.com/str/greek/844.htm)

[LSJ](https://lsj.gr/wiki/αὐτόματος)

[NET Bible](http://classic.net.bible.org/strong.php?id=844)

[Bible Bento](https://biblebento.com/dictionary/G0844.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0844/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0844.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g844)
