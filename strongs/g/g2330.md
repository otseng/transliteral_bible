# θέρος

[theros](https://www.blueletterbible.org/lexicon/g2330)

Definition: summer (3x), heat, harvest

Part of speech: neuter noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2330)

[Study Light](https://www.studylight.org/lexicons/greek/2330.html)

[Bible Hub](https://biblehub.com/str/greek/2330.htm)

[LSJ](https://lsj.gr/wiki/θέρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2330)

[Bible Bento](https://biblebento.com/dictionary/G2330.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2330/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2330.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2330)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B8%CE%AD%CF%81%CE%BF%CF%82)