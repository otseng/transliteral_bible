# διά

[dia](https://www.blueletterbible.org/lexicon/g1223)

Definition: by (241x), through (88x), with (16x), for (58x), for ... sake (47x), therefore (with G5124) (44x), for this cause (with G5124) (14x), because (52x), miscellaneous (86x).

Part of speech: preposition

Occurs 638 times in 573 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1223)

[Study Light](https://www.studylight.org/lexicons/greek/1223.html)

[Bible Hub](https://biblehub.com/str/greek/1223.htm)

[LSJ](https://lsj.gr/wiki/διά)

[NET Bible](http://classic.net.bible.org/strong.php?id=1223)

[Bible Bento](https://biblebento.com/dictionary/G1223.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1223/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1223.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1223)
