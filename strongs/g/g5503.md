# χήρα

[chēra](https://www.blueletterbible.org/lexicon/g5503)

Definition: widow (27x)

Part of speech: feminine noun

Occurs 27 times in 25 verses

Hebrew: ['almānâ](../h/h490.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5503)

[Study Light](https://www.studylight.org/lexicons/greek/5503.html)

[Bible Hub](https://biblehub.com/str/greek/5503.htm)

[LSJ](https://lsj.gr/wiki/χήρα)

[NET Bible](http://classic.net.bible.org/strong.php?id=5503)

[Bible Bento](https://biblebento.com/dictionary/G5503.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5503/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5503.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5503)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%87%CE%AE%CF%81%CE%B1)