# Κορνήλιος

[Kornēlios](https://www.blueletterbible.org/lexicon/g2883)

Definition: Cornelius (10x), "of a horn", a Roman centurion of the Italian cohort stationed in Caesarea who converted to Christianity

Part of speech: proper masculine noun

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2883)

[Study Light](https://www.studylight.org/lexicons/greek/2883.html)

[Bible Hub](https://biblehub.com/str/greek/2883.htm)

[LSJ](https://lsj.gr/wiki/Κορνήλιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2883)

[Bible Bento](https://biblebento.com/dictionary/G2883.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2883/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2883.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2883)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%9A%CE%BF%CF%81%CE%BD%CE%AE%CE%BB%CE%B9%CE%BF%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Cornelius_the_Centurion)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/cornelius.html)

[Video Bible](https://www.videobible.com/bible-dictionary/cornelius)