# διαζώννυμι

[diazōnnymi](https://www.blueletterbible.org/lexicon/g1241)

Definition: gird (3x), bind

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1241)

[Study Light](https://www.studylight.org/lexicons/greek/1241.html)

[Bible Hub](https://biblehub.com/str/greek/1241.htm)

[LSJ](https://lsj.gr/wiki/διαζώννυμι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1241)

[Bible Bento](https://biblebento.com/dictionary/G1241.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1241/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1241.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1241)