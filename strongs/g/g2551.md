# κακολογέω

[kakologeō](https://www.blueletterbible.org/lexicon/g2551)

Definition: curse (2x), speak evil of (2x), speak evil, revile

Part of speech: verb

Occurs 4 times in 4 verses

Synonyms: [curse](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Curse)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2551)

[Study Light](https://www.studylight.org/lexicons/greek/2551.html)

[Bible Hub](https://biblehub.com/str/greek/2551.htm)

[LSJ](https://lsj.gr/wiki/κακολογέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2551)

[Bible Bento](https://biblebento.com/dictionary/G2551.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2551/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2551.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2551)