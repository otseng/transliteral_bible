# συμφυλέτης

[symphyletēs](https://www.blueletterbible.org/lexicon/g4853)

Definition: countryman (1x), native of the same country

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4853)

[Study Light](https://www.studylight.org/lexicons/greek/4853.html)

[Bible Hub](https://biblehub.com/str/greek/4853.htm)

[LSJ](https://lsj.gr/wiki/συμφυλέτης)

[NET Bible](http://classic.net.bible.org/strong.php?id=4853)

[Bible Bento](https://biblebento.com/dictionary/G4853.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4853/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4853.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4853)
