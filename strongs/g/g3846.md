# παραβάλλω

[paraballō](https://www.blueletterbible.org/lexicon/g3846)

Definition: compare (1x), arrive (1x), throw along side

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3846)

[Study Light](https://www.studylight.org/lexicons/greek/3846.html)

[Bible Hub](https://biblehub.com/str/greek/3846.htm)

[LSJ](https://lsj.gr/wiki/παραβάλλω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3846)

[Bible Bento](https://biblebento.com/dictionary/G3846.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3846/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3846.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3846)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%B1%CF%81%CE%B1%CE%B2%CE%AC%CE%BB%CE%BB%CF%89)