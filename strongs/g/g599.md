# ἀποθνῄσκω

[apothnēskō](https://www.blueletterbible.org/lexicon/g599)

Definition: die (98x), dead (29x), at the point of death (with G3195) (1x), perish (1x), lay a dying (1x), point of death, slain

Part of speech: verb

Occurs 122 times in 99 verses

Greek: [apo](../g/g575.md), [thnēskō](../g/g2348.md)

Hebrew: [gāvaʿ](../h/h1478.md), [karath](../h/h3772.md), [muwth](../h/h4191.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0599)

[Study Light](https://www.studylight.org/lexicons/greek/599.html)

[Bible Hub](https://biblehub.com/str/greek/599.htm)

[LSJ](https://lsj.gr/wiki/ἀποθνῄσκω)

[NET Bible](http://classic.net.bible.org/strong.php?id=599)

[Bible Bento](https://biblebento.com/dictionary/G0599.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0599/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0599.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g599)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/0599-apothnesko-die.htm)
