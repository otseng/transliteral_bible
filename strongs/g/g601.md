# ἀποκαλύπτω

[apokalyptō](https://www.blueletterbible.org/lexicon/g601)

Definition: reveal (26x), to uncover, lay open what has been veiled or covered up, make bare, make manifest, disclose what before was unknown

Part of speech: verb

Occurs 32 times in 26 verses

Greek: [apokalypsis](../g/g602.md)

Hebrew: [gālâ](../h/h1540.md)

Derived words: [apocalypse](https://en.wiktionary.org/wiki/apocalypse)

Synonyms: [manifest](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Manifest)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0601)

[Study Light](https://www.studylight.org/lexicons/greek/601.html)

[Bible Hub](https://biblehub.com/str/greek/601.htm)

[LSJ](https://lsj.gr/wiki/ἀποκαλύπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=601)

[Bible Bento](https://biblebento.com/dictionary/G0601.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0601/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0601.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g601)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/0601-apokalupto-reveal.htm)
