# σκληροτράχηλος

[sklērotrachēlos](https://www.blueletterbible.org/lexicon/g4644)

Definition: stiffnecked (1x)

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4644)

[Study Light](https://www.studylight.org/lexicons/greek/4644.html)

[Bible Hub](https://biblehub.com/str/greek/4644.htm)

[LSJ](https://lsj.gr/wiki/σκληροτράχηλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4644)

[Bible Bento](https://biblebento.com/dictionary/G4644.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4644/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4644.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4644)
