# ἀπολύτρωσις

[apolytrōsis](https://www.blueletterbible.org/lexicon/g629)

Definition: redemption (9x), deliverance (1x), ransom

Part of speech: feminine noun

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0629)

[Study Light](https://www.studylight.org/lexicons/greek/629.html)

[Bible Hub](https://biblehub.com/str/greek/629.htm)

[LSJ](https://lsj.gr/wiki/ἀπολύτρωσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=629)

[Bible Bento](https://biblebento.com/dictionary/G0629.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0629/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0629.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g629)