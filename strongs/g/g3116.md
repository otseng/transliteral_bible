# μακροθύμως

[makrothymōs](https://www.blueletterbible.org/lexicon/g3116)

Definition: patiently (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3116)

[Study Light](https://www.studylight.org/lexicons/greek/3116.html)

[Bible Hub](https://biblehub.com/str/greek/3116.htm)

[LSJ](https://lsj.gr/wiki/μακροθύμως)

[NET Bible](http://classic.net.bible.org/strong.php?id=3116)

[Bible Bento](https://biblebento.com/dictionary/G3116.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3116/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3116.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3116)
