# Νῶε

[Nōe](https://www.blueletterbible.org/lexicon/g3575)

Definition: Noe (5x), Noah (3x), "rest", the tenth in descent from Adam, second father of the human family

Part of speech: proper masculine noun

Occurs 8 times in 8 verses

Hebrew: [Nōaḥ](../h/h5146.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3575)

[Study Light](https://www.studylight.org/lexicons/greek/3575.html)

[Bible Hub](https://biblehub.com/str/greek/3575.htm)

[LSJ](https://lsj.gr/wiki/Νῶε)

[NET Bible](http://classic.net.bible.org/strong.php?id=3575)

[Bible Bento](https://biblebento.com/dictionary/G3575.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3575/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3575.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3575)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%9D%E1%BF%B6%CE%B5)

[Wikipedia](https://en.wikipedia.org/wiki/Noah)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/noe.html)