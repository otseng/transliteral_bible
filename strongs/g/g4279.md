# προεπαγγέλλω

[proepaggello](https://www.blueletterbible.org/lexicon/g4279)

Definition: promise afore (1x), to announce before

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4279)

[Study Light](https://www.studylight.org/lexicons/greek/4279.html)

[Bible Hub](https://biblehub.com/str/greek/4279.htm)

[LSJ](https://lsj.gr/wiki/προεπαγγέλλω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4279)

[Bible Bento](https://biblebento.com/dictionary/G4279.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4279/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4279.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4279)
