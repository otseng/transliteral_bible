# τὰ νῦν

[ta nyn](https://www.blueletterbible.org/lexicon/g3569)

Definition: now (4x), but now (1x).

Part of speech: adverb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3569)

[Study Light](https://www.studylight.org/lexicons/greek/3569.html)

[Bible Hub](https://biblehub.com/str/greek/3569.htm)

[LSJ](https://lsj.gr/wiki/τὰ νῦν)

[NET Bible](http://classic.net.bible.org/strong.php?id=3569)

[Bible Bento](https://biblebento.com/dictionary/G3569.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3569/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3569.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3569)

