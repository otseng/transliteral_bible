# ἀσέλγεια

[aselgeia](https://www.blueletterbible.org/lexicon/g766)

Definition: lasciviousness (6x), wantonness (2x), filthy (1x), unbridled lust, excess, licentiousness, lasciviousness, wantonness, outrageousness, shamelessness, insolence, lewdness, lewd sexual immorality

Part of speech: feminine noun

Occurs 9 times in 9 verses

Synonyms: [lust](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Lust)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0766)

[Study Light](https://www.studylight.org/lexicons/greek/766.html)

[Bible Hub](https://biblehub.com/str/greek/766.htm)

[LSJ](https://lsj.gr/wiki/ἀσέλγεια)

[NET Bible](http://classic.net.bible.org/strong.php?id=766)

[Bible Bento](https://biblebento.com/dictionary/G0766.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0766/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0766.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g766)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CF%83%CE%AD%CE%BB%CE%B3%CE%B5%CE%B9%CE%B1)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34347)

[Precept Austin](https://www.preceptaustin.org/romans_13_notes_pt3#sensuality)