# πόλις

[polis](https://www.blueletterbible.org/lexicon/g4172)

Definition: city (164x), body of citizens, city-state

Part of speech: feminine noun

Occurs 164 times in 155 verses

Hebrew: [ʿîr](../h/h5892.md), [qiryâ](../h/h7151.md)

Derived words: politics, metropolis, police, policy, polity, Acropolis, Indianapolis

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4172)

[Study Light](https://www.studylight.org/lexicons/greek/4172.html)

[Bible Hub](https://biblehub.com/str/greek/4172.htm)

[LSJ](https://lsj.gr/wiki/πόλις)

[NET Bible](http://classic.net.bible.org/strong.php?id=4172)

[Bible Bento](https://biblebento.com/dictionary/G4172.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4172/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4172.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4172)
