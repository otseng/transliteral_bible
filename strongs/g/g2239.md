# ἦθος

[ēthos](https://www.blueletterbible.org/lexicon/g2239)

Definition: manners (1x), custom, usage, morals, character

Part of speech: neuter noun

Occurs 1 times in 1 verses

Derived words: ethics

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2239)

[Study Light](https://www.studylight.org/lexicons/greek/2239.html)

[Bible Hub](https://biblehub.com/str/greek/2239.htm)

[LSJ](https://lsj.gr/wiki/ἦθος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2239)

[Bible Bento](https://biblebento.com/dictionary/G2239.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2239/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2239.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2239)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%A6%CE%B8%CE%BF%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Ethos)

[Precept Austin](https://www.preceptaustin.org/luke-1-commentary#custom)
