# πενθερός

[pentheros](https://www.blueletterbible.org/lexicon/g3995)

Definition: father in law (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3995)

[Study Light](https://www.studylight.org/lexicons/greek/3995.html)

[Bible Hub](https://biblehub.com/str/greek/3995.htm)

[LSJ](https://lsj.gr/wiki/πενθερός)

[NET Bible](http://classic.net.bible.org/strong.php?id=3995)

[Bible Bento](https://biblebento.com/dictionary/G3995.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3995/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3995.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3995)

