# σωφρονέω

[sōphroneō](https://www.blueletterbible.org/lexicon/g4993)

Definition: be in right mind (2x), be sober (2x), be sober minded (1x), soberly (1x)

Part of speech: verb

Occurs 7 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4993)

[Study Light](https://www.studylight.org/lexicons/greek/4993.html)

[Bible Hub](https://biblehub.com/str/greek/4993.htm)

[LSJ](https://lsj.gr/wiki/σωφρονέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4993)

[Bible Bento](https://biblebento.com/dictionary/G4993.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4993/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4993.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4993)