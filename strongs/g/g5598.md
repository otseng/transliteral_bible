# Ω

[ō](https://www.blueletterbible.org/lexicon/g5598)

Definition: Omega (4x).

Part of speech: indeclinable noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5598)

[Study Light](https://www.studylight.org/lexicons/greek/5598.html)

[Bible Hub](https://biblehub.com/str/greek/5598.htm)

[LSJ](https://lsj.gr/wiki/Ω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5598)

[Bible Bento](https://biblebento.com/dictionary/G5598.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5598/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5598.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5598)
