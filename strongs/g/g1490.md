# εἰ δὲ μή (γε)

[ei de mē (ge)](https://www.blueletterbible.org/lexicon/g1490)

Definition: or else (3x), else (4x), if not (2x), if otherwise (2x), if not (1x), or else (1x), otherwise (1x).

Part of speech: conjunction

Occurs 14 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1490)

[Study Light](https://www.studylight.org/lexicons/greek/1490.html)

[Bible Hub](https://biblehub.com/str/greek/1490.htm)

[LSJ](https://lsj.gr/wiki/εἰ δὲ μή (γε))

[NET Bible](http://classic.net.bible.org/strong.php?id=1490)

[Bible Bento](https://biblebento.com/dictionary/G1490.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1490/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1490.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1490)

