# παραδειγματίζω

[paradeigmatizō](https://www.blueletterbible.org/lexicon/g3856)

Definition: make a public example (1x), put to open shame (1x), to expose to public disgrace

Part of speech: verb

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3856)

[Study Light](https://www.studylight.org/lexicons/greek/3856.html)

[Bible Hub](https://biblehub.com/str/greek/3856.htm)

[LSJ](https://lsj.gr/wiki/παραδειγματίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3856)

[Bible Bento](https://biblebento.com/dictionary/G3856.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3856/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3856.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3856)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%B1%CF%81%CE%B1%CE%B4%CE%B5%CE%B9%CE%B3%CE%BC%CE%B1%CF%84%CE%AF%CE%B6%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34708)

[Precept Austin](https://www.preceptaustin.org/hebrews_66#put)