# ἔμπροσθεν

[emprosthen](https://www.blueletterbible.org/lexicon/g1715)

Definition: before (41x), in (one's) sight (2x), of (1x), against (1x), in the sight of (1x), in the presence of (1x), at (1x), in front

Part of speech: adverb

Occurs 49 times in 45 verses

Hebrew: ['āḥôr](../h/h268.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1715)

[Study Light](https://www.studylight.org/lexicons/greek/1715.html)

[Bible Hub](https://biblehub.com/str/greek/1715.htm)

[LSJ](https://lsj.gr/wiki/ἔμπροσθεν)

[NET Bible](http://classic.net.bible.org/strong.php?id=1715)

[Bible Bento](https://biblebento.com/dictionary/G1715.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1715/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1715.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1715)