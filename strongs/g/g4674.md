# σός

[sos](https://www.blueletterbible.org/lexicon/g4674)

Definition: thy (13x), thine (9x), thine own (3x), thy goods (1x), thy friends (1x).

Part of speech: pronoun

Occurs 26 times in 23 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4674)

[Study Light](https://www.studylight.org/lexicons/greek/4674.html)

[Bible Hub](https://biblehub.com/str/greek/4674.htm)

[LSJ](https://lsj.gr/wiki/σός)

[NET Bible](http://classic.net.bible.org/strong.php?id=4674)

[Bible Bento](https://biblebento.com/dictionary/G4674.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4674/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4674.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4674)

