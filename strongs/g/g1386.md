# δόλιος

[dolios](https://www.blueletterbible.org/lexicon/g1386)

Definition: deceitful (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

Hebrew: [mirmah](../h/h4820.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1386)

[Study Light](https://www.studylight.org/lexicons/greek/1386.html)

[Bible Hub](https://biblehub.com/str/greek/1386.htm)

[LSJ](https://lsj.gr/wiki/δόλιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1386)

[Bible Bento](https://biblebento.com/dictionary/G1386.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1386/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1386.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1386)
