# διαλέγομαι

[dialegomai](https://www.blueletterbible.org/lexicon/g1256)

Definition: dispute (6x), reason with (2x), reason (2x), preach unto (1x), preach (1x), speak (1x), to converse, discourse with one, argue, discuss, debate

Part of speech: verb

Occurs 13 times in 13 verses

Hebrew: [riyb](../h/h7378.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1256)

[Study Light](https://www.studylight.org/lexicons/greek/1256.html)

[Bible Hub](https://biblehub.com/str/greek/1256.htm)

[LSJ](https://lsj.gr/wiki/διαλέγομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1256)

[Bible Bento](https://biblebento.com/dictionary/G1256.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1256/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1256.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1256)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B4%CE%B9%CE%B1%CE%BB%CE%AD%CE%B3%CE%BF%CE%BC%CE%B1%CE%B9)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34728)

[Precept Austin](https://www.preceptaustin.org/acts-17-commentary#r)
