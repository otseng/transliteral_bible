# ἰχθύς

[ichthys](https://www.blueletterbible.org/lexicon/g2486)

Definition: fish (20x)

Part of speech: masculine noun

Occurs 20 times in 18 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2486)

[Study Light](https://www.studylight.org/lexicons/greek/2486.html)

[Bible Hub](https://biblehub.com/str/greek/2486.htm)

[LSJ](https://lsj.gr/wiki/ἰχθύς)

[NET Bible](http://classic.net.bible.org/strong.php?id=2486)

[Bible Bento](https://biblebento.com/dictionary/G2486.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2486/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2486.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2486)

[Wiktionary](https://en.wiktionary.org/wiki/ichthys)

[Wikipedia](https://en.wikipedia.org/wiki/Ichthys)

[New Advent](http://www.newadvent.org/cathen/06083a.htm)

[Symbols](https://www.symbols.com/symbol/ichthys)