# ὑποστολή

[hypostolē](https://www.blueletterbible.org/lexicon/g5289)

Definition: of them who draw back (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5289)

[Study Light](https://www.studylight.org/lexicons/greek/5289.html)

[Bible Hub](https://biblehub.com/str/greek/5289.htm)

[LSJ](https://lsj.gr/wiki/ὑποστολή)

[NET Bible](http://classic.net.bible.org/strong.php?id=5289)

[Bible Bento](https://biblebento.com/dictionary/G5289.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5289/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5289.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5289)
