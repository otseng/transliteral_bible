# ἀποστάσιον

[apostasion](https://www.blueletterbible.org/lexicon/g647)

Definition: divorcement (2x), writing of divorcement (1x), repudiation

Part of speech: neuter noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0647)

[Study Light](https://www.studylight.org/lexicons/greek/647.html)

[Bible Hub](https://biblehub.com/str/greek/647.htm)

[LSJ](https://lsj.gr/wiki/ἀποστάσιον)

[NET Bible](http://classic.net.bible.org/strong.php?id=647)

[Bible Bento](https://biblebento.com/dictionary/G0647.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0647/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0647.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g647)