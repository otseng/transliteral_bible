# πυρράζω

[pyrrazō](https://www.blueletterbible.org/lexicon/g4449)

Definition: be red (2x), to become glowing, grow red

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4449)

[Study Light](https://www.studylight.org/lexicons/greek/4449.html)

[Bible Hub](https://biblehub.com/str/greek/4449.htm)

[LSJ](https://lsj.gr/wiki/πυρράζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4449)

[Bible Bento](https://biblebento.com/dictionary/G4449.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4449/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4449.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4449)
