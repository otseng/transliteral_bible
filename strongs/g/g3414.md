# μνᾶ

[mna](https://www.blueletterbible.org/lexicon/g3414)

Definition: pound (9x). In the NT, a weight and sum of money equal to 100 drachmae, one talent was 100 pounds, a pound equalled 10 1/3 oz. (300 gm)

Part of speech: feminine noun

Occurs 9 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3414)

[Study Light](https://www.studylight.org/lexicons/greek/3414.html)

[Bible Hub](https://biblehub.com/str/greek/3414.htm)

[LSJ](https://lsj.gr/wiki/μνᾶ)

[NET Bible](http://classic.net.bible.org/strong.php?id=3414)

[Bible Bento](https://biblebento.com/dictionary/G3414.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3414/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3414.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3414)