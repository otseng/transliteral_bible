# οἷος

[hoios](https://www.blueletterbible.org/lexicon/g3634)

Definition: such as (6x), as (3x), which (2x), what manner (1x), so as (1x), what manner of man (1x), what (1x)

Part of speech: pronoun

Occurs 15 times in 12 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3634)

[Study Light](https://www.studylight.org/lexicons/greek/3634.html)

[Bible Hub](https://biblehub.com/str/greek/3634.htm)

[LSJ](https://lsj.gr/wiki/οἷος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3634)

[Bible Bento](https://biblebento.com/dictionary/G3634.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3634/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3634.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3634)