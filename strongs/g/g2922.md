# κριτήριον

[kritērion](https://www.blueletterbible.org/lexicon/g2922)

Definition: to judge (1x), judgment (1x), judgment seat (1x).

Part of speech: neuter noun

Occurs 3 times in 3 verses

Hebrew: [mishpat](../h/h4941.md)

Derived words: criterion

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2922)

[Study Light](https://www.studylight.org/lexicons/greek/2922.html)

[Bible Hub](https://biblehub.com/str/greek/2922.htm)

[LSJ](https://lsj.gr/wiki/κριτήριον)

[NET Bible](http://classic.net.bible.org/strong.php?id=2922)

[Bible Bento](https://biblebento.com/dictionary/G2922.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2922/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2922.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2922)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CF%81%CE%B9%CF%84%CE%AE%CF%81%CE%B9%CE%BF%CE%BD)
