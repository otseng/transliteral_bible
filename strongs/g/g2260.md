# ἤπερ

[ēper](https://www.blueletterbible.org/lexicon/g2260)

Definition: than (1x).

Part of speech: particle

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2260)

[Study Light](https://www.studylight.org/lexicons/greek/2260.html)

[Bible Hub](https://biblehub.com/str/greek/2260.htm)

[LSJ](https://lsj.gr/wiki/ἤπερ)

[NET Bible](http://classic.net.bible.org/strong.php?id=2260)

[Bible Bento](https://biblebento.com/dictionary/G2260.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2260/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2260.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2260)

