# συγκύπτω

[sygkyptō](https://www.blueletterbible.org/lexicon/g4794)

Definition: bow together (1x), to bend completely forwards, to be bowed together, to stoop altogether

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4794)

[Study Light](https://www.studylight.org/lexicons/greek/4794.html)

[Bible Hub](https://biblehub.com/str/greek/4794.htm)

[LSJ](https://lsj.gr/wiki/συγκύπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4794)

[Bible Bento](https://biblebento.com/dictionary/G4794.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4794/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4794.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4794)