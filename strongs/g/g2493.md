# Ἰωήλ

[Iōēl](https://www.blueletterbible.org/lexicon/g2493)

Definition: Joel (1x), "to whom Jehovah is God"

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

Hebrew: [Yô'Ēl](../h/h3100.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2493)

[Study Light](https://www.studylight.org/lexicons/greek/2493.html)

[Bible Hub](https://biblehub.com/str/greek/2493.htm)

[LSJ](https://lsj.gr/wiki/Ἰωήλ)

[NET Bible](http://classic.net.bible.org/strong.php?id=2493)

[Bible Bento](https://biblebento.com/dictionary/G2493.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2493/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2493.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2493)

[Wikipedia](https://en.wikipedia.org/wiki/Joel_%28prophet%29)

[Video Bible](https://www.videobible.com/bible-dictionary/joel)