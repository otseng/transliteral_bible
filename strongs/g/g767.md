# ἄσημος

[asēmos](https://www.blueletterbible.org/lexicon/g767)

Definition: mean (1x), unmarked or unstamped, unknown, of no mark, insignificant, ignoble

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0767)

[Study Light](https://www.studylight.org/lexicons/greek/767.html)

[Bible Hub](https://biblehub.com/str/greek/767.htm)

[LSJ](https://lsj.gr/wiki/ἄσημος)

[NET Bible](http://classic.net.bible.org/strong.php?id=767)

[Bible Bento](https://biblebento.com/dictionary/G0767.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0767/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0767.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g767)
