# πεῖρα

[peira](https://www.blueletterbible.org/lexicon/g3984)

Definition: assay (with G2983) (1x), trial (1x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3984)

[Study Light](https://www.studylight.org/lexicons/greek/3984.html)

[Bible Hub](https://biblehub.com/str/greek/3984.htm)

[LSJ](https://lsj.gr/wiki/πεῖρα)

[NET Bible](http://classic.net.bible.org/strong.php?id=3984)

[Bible Bento](https://biblebento.com/dictionary/G3984.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3984/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3984.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3984)
