# τεσσαράκοντα

[tessarakonta](https://www.blueletterbible.org/lexicon/g5062)

Definition: forty (22x).

Part of speech: adjective

Occurs 22 times in 21 verses

Greek: [tessares](../g/g5064.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5062)

[Study Light](https://www.studylight.org/lexicons/greek/5062.html)

[Bible Hub](https://biblehub.com/str/greek/5062.htm)

[LSJ](https://lsj.gr/wiki/τεσσαράκοντα)

[NET Bible](http://classic.net.bible.org/strong.php?id=5062)

[Bible Bento](https://biblebento.com/dictionary/G5062.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5062/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5062.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5062)

