# τίνω

[tinō](https://www.blueletterbible.org/lexicon/g5099)

Definition: be punished (with G1349) (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5099)

[Study Light](https://www.studylight.org/lexicons/greek/5099.html)

[Bible Hub](https://biblehub.com/str/greek/5099.htm)

[LSJ](https://lsj.gr/wiki/τίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5099)

[Bible Bento](https://biblebento.com/dictionary/G5099.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5099/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5099.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5099)
