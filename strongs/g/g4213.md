# πόσις

[posis](https://www.blueletterbible.org/lexicon/g4213)

Definition: drink (3x)

Part of speech: feminine noun

Occurs 3 times in 3 verses

Mnemonic: potion

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4213)

[Study Light](https://www.studylight.org/lexicons/greek/4213.html)

[Bible Hub](https://biblehub.com/str/greek/4213.htm)

[LSJ](https://lsj.gr/wiki/πόσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=4213)

[Bible Bento](https://biblebento.com/dictionary/G4213.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4213/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4213.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4213)