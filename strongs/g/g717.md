# Ἁρμαγεδών

[harmagedōn](https://www.blueletterbible.org/lexicon/g717)

Definition: Armageddon (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0717)

[Study Light](https://www.studylight.org/lexicons/greek/717.html)

[Bible Hub](https://biblehub.com/str/greek/717.htm)

[LSJ](https://lsj.gr/wiki/Ἁρμαγεδών)

[NET Bible](http://classic.net.bible.org/strong.php?id=717)

[Bible Bento](https://biblebento.com/dictionary/G717.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/717/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/717.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g717)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/armageddon.html)