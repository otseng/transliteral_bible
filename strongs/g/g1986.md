# ἐπισπάω

[epispaō](https://www.blueletterbible.org/lexicon/g1986)

Definition: become circumcised (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1986)

[Study Light](https://www.studylight.org/lexicons/greek/1986.html)

[Bible Hub](https://biblehub.com/str/greek/1986.htm)

[LSJ](https://lsj.gr/wiki/ἐπισπάω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1986)

[Bible Bento](https://biblebento.com/dictionary/G1986.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1986/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1986.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1986)
