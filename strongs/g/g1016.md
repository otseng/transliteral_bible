# βοῦς

[bous](https://www.blueletterbible.org/lexicon/g1016)

Definition: ox (8x), cow

Part of speech: masculine noun

Occurs 8 times in 7 verses

Hebrew: [par](../h/h6499.md), [bāqār](../h/h1241.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1016)

[Study Light](https://www.studylight.org/lexicons/greek/1016.html)

[Bible Hub](https://biblehub.com/str/greek/1016.htm)

[LSJ](https://lsj.gr/wiki/βοῦς)

[NET Bible](http://classic.net.bible.org/strong.php?id=1016)

[Bible Bento](https://biblebento.com/dictionary/G1016.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1016/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1016.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1016)