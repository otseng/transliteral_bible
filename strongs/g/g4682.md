# σπαράσσω

[sparassō](https://www.blueletterbible.org/lexicon/g4682)

Definition: tear (3x), rend (1x), convulse, mangle

Part of speech: verb

Occurs 4 times in 4 verses

Derived words: [Sparagmos](https://en.wikipedia.org/wiki/Sparagmos)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4682)

[Study Light](https://www.studylight.org/lexicons/greek/4682.html)

[Bible Hub](https://biblehub.com/str/greek/4682.htm)

[LSJ](https://lsj.gr/wiki/σπαράσσω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4682)

[Bible Bento](https://biblebento.com/dictionary/G4682.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4682/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4682.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4682)