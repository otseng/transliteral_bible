# ἀποδοκιμάζω

[apodokimazō](https://www.blueletterbible.org/lexicon/g593)

Definition: reject (7x), disallow (2x), disapprove, condemn

Part of speech: verb

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0593)

[Study Light](https://www.studylight.org/lexicons/greek/593.html)

[Bible Hub](https://biblehub.com/str/greek/593.htm)

[LSJ](https://lsj.gr/wiki/ἀποδοκιμάζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=593)

[Bible Bento](https://biblebento.com/dictionary/G0593.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0593/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0593.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g593)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CF%80%CE%BF%CE%B4%CE%BF%CE%BA%CE%B9%CE%BC%CE%AC%CE%B6%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34762)