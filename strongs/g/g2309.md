# θέλω

[thelō](https://www.blueletterbible.org/lexicon/g2309)

Definition: will/would (159x), will/would have (16x), desire (13x), desirous (3x), list (3x), to will (2x), wish, have rather, willing, wilt, need, require, have, love

Part of speech: verb

Occurs 213 times in 201 verses

Greek: [thelēma](../g/g2307.md)

Hebrew: [ḥāp̄ēṣ](../h/h2654.md)

Synonyms: [will](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Will)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2309)

[Study Light](https://www.studylight.org/lexicons/greek/2309.html)

[Bible Hub](https://biblehub.com/str/greek/2309.htm)

[LSJ](https://lsj.gr/wiki/θέλω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2309)

[Bible Bento](https://biblebento.com/dictionary/G2309.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2309/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2309.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2309)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B8%CE%AD%CE%BB%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33846)

[Precept Austin](https://www.preceptaustin.org/romans_914-18#d)