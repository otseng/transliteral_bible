# βίαιος

[biaios](https://www.blueletterbible.org/lexicon/g972)

Definition: mighty (1x), violent, forcible

Part of speech: adjective

Occurs 1 times in 1 verses

Synonyms: [mighty](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Mighty)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0972)

[Study Light](https://www.studylight.org/lexicons/greek/972.html)

[Bible Hub](https://biblehub.com/str/greek/972.htm)

[LSJ](https://lsj.gr/wiki/βίαιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=972)

[Bible Bento](https://biblebento.com/dictionary/G0972.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0972/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0972.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g972)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B2%CE%AF%CE%B1%CE%B9%CE%BF%CF%82)
