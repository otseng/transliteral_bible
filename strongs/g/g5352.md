# φθινοπωρινός

[phthinopōrinos](https://www.blueletterbible.org/lexicon/g5352)

Definition: whose fruit withereth (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5352)

[Study Light](https://www.studylight.org/lexicons/greek/5352.html)

[Bible Hub](https://biblehub.com/str/greek/5352.htm)

[LSJ](https://lsj.gr/wiki/φθινοπωρινός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5352)

[Bible Bento](https://biblebento.com/dictionary/G5352.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5352/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5352.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5352)

