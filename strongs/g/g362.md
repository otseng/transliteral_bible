# ἀναμένω

[anamenō](https://www.blueletterbible.org/lexicon/g362)

Definition: wait for (1x), to wait for one (with the added notion of patience and trust), expect

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0362)

[Study Light](https://www.studylight.org/lexicons/greek/362.html)

[Bible Hub](https://biblehub.com/str/greek/362.htm)

[LSJ](https://lsj.gr/wiki/ἀναμένω)

[NET Bible](http://classic.net.bible.org/strong.php?id=362)

[Bible Bento](https://biblebento.com/dictionary/G0362.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/362/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/362.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g362)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CE%BD%CE%B1%CE%BC%CE%AD%CE%BD%CF%89)
