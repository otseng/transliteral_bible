# ἐντρέφω

[entrephō](https://www.blueletterbible.org/lexicon/g1789)

Definition: nourish up in (1x), to educate, form the mind

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1789)

[Study Light](https://www.studylight.org/lexicons/greek/1789.html)

[Bible Hub](https://biblehub.com/str/greek/1789.htm)

[LSJ](https://lsj.gr/wiki/ἐντρέφω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1789)

[Bible Bento](https://biblebento.com/dictionary/G1789.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1789/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1789.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1789)
