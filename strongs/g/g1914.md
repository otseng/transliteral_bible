# ἐπιβλέπω

[epiblepō](https://www.blueletterbible.org/lexicon/g1914)

Definition: regard (1x), look (1x), have respect to (1x), gaze upon, supervise

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1914)

[Study Light](https://www.studylight.org/lexicons/greek/1914.html)

[Bible Hub](https://biblehub.com/str/greek/1914.htm)

[LSJ](https://lsj.gr/wiki/ἐπιβλέπω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1914)

[Bible Bento](https://biblebento.com/dictionary/G1914.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1914/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1914.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1914)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B5%CF%80%CE%B9%CE%B2%CE%BB%CE%AD%CF%80%CF%89)