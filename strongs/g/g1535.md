# εἴτε

[eite](https://www.blueletterbible.org/lexicon/g1535)

Definition: or (33x), whether (28x), or whether (3x), if (1x).

Part of speech: conjunction

Occurs 65 times in 29 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1535)

[Study Light](https://www.studylight.org/lexicons/greek/1535.html)

[Bible Hub](https://biblehub.com/str/greek/1535.htm)

[LSJ](https://lsj.gr/wiki/εἴτε)

[NET Bible](http://classic.net.bible.org/strong.php?id=1535)

[Bible Bento](https://biblebento.com/dictionary/G1535.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1535/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1535.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1535)

