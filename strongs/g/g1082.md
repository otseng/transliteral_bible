# Γεννησαρέτ

[Gennēsaret](https://www.blueletterbible.org/lexicon/g1082)

Definition: Gennesaret = "a harp", a lake also called the sea of Galilee or the sea of Tiberias The lake 12 by 7 miles (20 by 11 km) and 700 feet (210 m) below the Mediterranean Sea, a very lovely and fertile region on the Sea of Galilee

Part of speech: proper locative noun

Occurs 3 times in 3 verses

Hebrew: [Kinnᵊrôṯ](../h/h3672.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1082)

[Study Light](https://www.studylight.org/lexicons/greek/1082.html)

[Bible Hub](https://biblehub.com/str/greek/1082.htm)

[LSJ](https://lsj.gr/wiki/Γεννησαρέτ)

[NET Bible](http://classic.net.bible.org/strong.php?id=1082)

[Bible Bento](https://biblebento.com/dictionary/G1082.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1082/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1082.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1082)

[Wikipedia](https://en.wikipedia.org/wiki/Kinneret_%28archaeological_site%29)

[Bible History](https://www.bible-history.com/geography/ancient-israel/gennesaret.html)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/gennesaret.html)

[Video Bible](https://www.videobible.com/bible-dictionary/gennesaret)