# περιλείπομαι

[perileipomai](https://www.blueletterbible.org/lexicon/g4035)

Definition: remain (2x), to leave over, survive

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4035)

[Study Light](https://www.studylight.org/lexicons/greek/4035.html)

[Bible Hub](https://biblehub.com/str/greek/4035.htm)

[LSJ](https://lsj.gr/wiki/περιλείπομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4035)

[Bible Bento](https://biblebento.com/dictionary/G4035.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4035/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4035.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4035)
