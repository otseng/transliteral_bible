# γῆρας

[gēras](https://www.blueletterbible.org/lexicon/g1094)

Definition: old age (1x)

Part of speech: neuter noun

Occurs 1 times in 1 verses

Derived words: geriatric

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1094)

[Study Light](https://www.studylight.org/lexicons/greek/1094.html)

[Bible Hub](https://biblehub.com/str/greek/1094.htm)

[LSJ](https://lsj.gr/wiki/γῆρας)

[NET Bible](http://classic.net.bible.org/strong.php?id=1094)

[Bible Bento](https://biblebento.com/dictionary/G1094.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1094/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1094.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1094)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B3%E1%BF%86%CF%81%CE%B1%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Geras)