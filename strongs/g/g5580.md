# ψευδόχριστος

[pseudochristos](https://www.blueletterbible.org/lexicon/g5580)

Definition: false Christ (2x)

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5580)

[Study Light](https://www.studylight.org/lexicons/greek/5580.html)

[Bible Hub](https://biblehub.com/str/greek/5580.htm)

[LSJ](https://lsj.gr/wiki/ψευδόχριστος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5580)

[Bible Bento](https://biblebento.com/dictionary/G5580.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5580/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5580.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5580)