# συλλαμβάνω

[syllambanō](https://www.blueletterbible.org/lexicon/g4815)

Definition: take (8x), conceive (5x), help (2x), catch (1x), to take hold, catch, help, gather

Part of speech: verb

Occurs 17 times in 16 verses

Derived words: syllabus

Synonyms: [take](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Take)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4815)

[Study Light](https://www.studylight.org/lexicons/greek/4815.html)

[Bible Hub](https://biblehub.com/str/greek/4815.htm)

[LSJ](https://lsj.gr/wiki/συλλαμβάνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4815)

[Bible Bento](https://biblebento.com/dictionary/G4815.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4815/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4815.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4815)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CF%85%CE%BB%CE%BB%CE%B1%CE%BC%CE%B2%CE%AC%CE%BD%CF%89)

[Resounding the faith](http://resoundingthefaith.com/2018/11/greek-%CF%83%CF%85%CE%BB%CE%BB%CE%B1%CE%BC%CE%B2%CE%AC%CE%BD%CF%89-syllambano/)