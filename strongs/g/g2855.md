# κολλυβιστής

[kollybistēs](https://www.blueletterbible.org/lexicon/g2855)

Definition: moneychanger (2x), charger (1x), coin dealer, banker

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2855)

[Study Light](https://www.studylight.org/lexicons/greek/2855.html)

[Bible Hub](https://biblehub.com/str/greek/2855.htm)

[LSJ](https://lsj.gr/wiki/κολλυβιστής)

[NET Bible](http://classic.net.bible.org/strong.php?id=2855)

[Bible Bento](https://biblebento.com/dictionary/G2855.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2855/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2855.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2855)