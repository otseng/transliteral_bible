# ἑλίσσω

[helissō](https://www.blueletterbible.org/lexicon/g1667)

Definition: fold them up (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1667)

[Study Light](https://www.studylight.org/lexicons/greek/1667.html)

[Bible Hub](https://biblehub.com/str/greek/1667.htm)

[LSJ](https://lsj.gr/wiki/ἑλίσσω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1667)

[Bible Bento](https://biblebento.com/dictionary/G1667.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1667/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1667.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1667)
