# προσάγω

[prosagō](https://www.blueletterbible.org/lexicon/g4317)

Definition: bring (3x), draw near (1x).

Part of speech: verb

Occurs 4 times in 4 verses

Hebrew: [nāḡaš](../h/h5066.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4317)

[Study Light](https://www.studylight.org/lexicons/greek/4317.html)

[Bible Hub](https://biblehub.com/str/greek/4317.htm)

[LSJ](https://lsj.gr/wiki/προσάγω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4317)

[Bible Bento](https://biblebento.com/dictionary/G4317.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4317/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4317.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4317)

