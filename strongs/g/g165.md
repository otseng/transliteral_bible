# αἰών

[aiōn](https://www.blueletterbible.org/lexicon/g165)

Definition: ever (71x), world (38x), never (with G3364) (with G1519) (with G3588) (6x), evermore (4x), age (2x), eternal (2x), forever, eternal, age, evermore, perpetuity of time, eternity, timeless

Part of speech: masculine noun

Occurs 128 times in 102 verses

Hebrew: [`owlam](../h/h5769.md), [tāmîḏ](../h/h8548.md)

Derived words: [aeon](https://en.wikipedia.org/wiki/Aeon), [eon](https://www.etymonline.com/word/eon)

Synonyms: [world](https://simple.uniquebibleapp.com/book/Synonyms/Greek#World)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0165)

[Study Light](https://www.studylight.org/lexicons/greek/165.html)

[Bible Hub](https://biblehub.com/str/greek/165.htm)

[LSJ](https://lsj.gr/wiki/αἰών)

[NET Bible](http://classic.net.bible.org/strong.php?id=165)

[Bible Bento](https://biblebento.com/dictionary/G0165.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0165/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0165.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g165)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%E1%BC%B0%CF%8E%CE%BD)

[Precept Austin](https://www.preceptaustin.org/titus_212#age%20165%20aion)

[Aion_(deity)](https://en.wikipedia.org/wiki/Aion_%28deity%29)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/0165-aion-age-ever.htm)
