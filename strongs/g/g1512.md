# εἴ περ

[ei per](https://www.blueletterbible.org/lexicon/g1512)

Definition: if so be that (3x), though (1x), seeing (1x), if so be (1x).

Part of speech: conjunction

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1512)

[Study Light](https://www.studylight.org/lexicons/greek/1512.html)

[Bible Hub](https://biblehub.com/str/greek/1512.htm)

[LSJ](https://lsj.gr/wiki/εἴ περ)

[NET Bible](http://classic.net.bible.org/strong.php?id=1512)

[Bible Bento](https://biblebento.com/dictionary/G1512.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1512/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1512.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1512)

