# χωλός

[chōlos](https://www.blueletterbible.org/lexicon/g5560)

Definition: lame (10x), halt (4x), cripple (1x)

Part of speech: adjective

Occurs 15 times in 15 verses

Hebrew: [pissēaḥ](../h/h6455.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5560)

[Study Light](https://www.studylight.org/lexicons/greek/5560.html)

[Bible Hub](https://biblehub.com/str/greek/5560.htm)

[LSJ](https://lsj.gr/wiki/χωλός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5560)

[Bible Bento](https://biblebento.com/dictionary/G5560.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5560/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5560.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5560)