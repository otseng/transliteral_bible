# θαυμάζω

[thaumazō](https://www.blueletterbible.org/lexicon/g2296)

Definition: marvel (29x), wonder (14x), have in admiration (1x), admire (1x), marvelled (with G2258) (1x), variations of 'wonder' (1x)

Part of speech: verb

Occurs 48 times in 46 verses

Greek: [thaumastos](../g/g2298.md)

Hebrew: [tāmah](../h/h8539.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2296)

[Study Light](https://www.studylight.org/lexicons/greek/2296.html)

[Bible Hub](https://biblehub.com/str/greek/2296.htm)

[LSJ](https://lsj.gr/wiki/θαυμάζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2296)

[Bible Bento](https://biblebento.com/dictionary/G2296.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2296/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2296.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2296)

[Wiktionary](https://en.wiktionary.org/wiki/θαυμάζω)