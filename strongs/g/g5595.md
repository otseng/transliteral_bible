# ψωμίζω

[psōmizō](https://www.blueletterbible.org/lexicon/g5595)

Definition: bestow to feed (1x), feed (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5595)

[Study Light](https://www.studylight.org/lexicons/greek/5595.html)

[Bible Hub](https://biblehub.com/str/greek/5595.htm)

[LSJ](https://lsj.gr/wiki/ψωμίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5595)

[Bible Bento](https://biblebento.com/dictionary/G5595.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5595/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5595.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5595)
