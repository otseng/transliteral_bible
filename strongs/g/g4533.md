# Σαλμών

[Salmōn](https://www.blueletterbible.org/lexicon/g4533)

Definition: Salmon (3x), "raiment: a garment", the father of Boaz in the genealogy of Christ

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/4533.html)

[LSJ](https://lsj.gr/wiki/Σαλμών)

[NET Bible](http://classic.net.bible.org/strong.php?id=4533)

[Bible Bento](https://biblebento.com/dictionary/G4533.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4533/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4533.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4533)

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4533)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/salmon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/salmon)