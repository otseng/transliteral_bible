# ἀνακύπτω

[anakyptō](https://www.blueletterbible.org/lexicon/g352)

Definition: lift up (one's) self (3x), look up (1x), stand up, look up, arise

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0352)

[Study Light](https://www.studylight.org/lexicons/greek/352.html)

[Bible Hub](https://biblehub.com/str/greek/352.htm)

[LSJ](https://lsj.gr/wiki/ἀνακύπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=352)

[Bible Bento](https://biblebento.com/dictionary/G0352.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0352/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0352.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g352)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CE%BD%CE%B1%CE%BA%CF%8D%CF%80%CF%84%CF%89)