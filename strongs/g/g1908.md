# ἐπηρεάζω

[epēreazō](https://www.blueletterbible.org/lexicon/g1908)

Definition: despitefully use (2x), falsely accuse (1x), treat abusively, accuse falsely, insult

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1908)

[Study Light](https://www.studylight.org/lexicons/greek/1908.html)

[Bible Hub](https://biblehub.com/str/greek/1908.htm)

[LSJ](https://lsj.gr/wiki/ἐπηρεάζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1908)

[Bible Bento](https://biblebento.com/dictionary/G1908.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1908/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1908.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1908)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B5%CF%80%CE%B7%CF%81%CE%B5%CE%AC%CE%B6%CF%89)