# θεωρέω

[theōreō](https://www.blueletterbible.org/lexicon/g2334)

Definition: see (40x), behold (11x), perceive (4x), consider (1x), look on (1x)

Part of speech: verb

Occurs 61 times in 55 verses

Hebrew: [šûr](../h/h7789.md), [ra'ah](../h/h7200.md)

Synonyms: [look](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Look)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2334)

[Study Light](https://www.studylight.org/lexicons/greek/2334.html)

[Bible Hub](https://biblehub.com/str/greek/2334.htm)

[LSJ](https://lsj.gr/wiki/θεωρέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2334)

[Bible Bento](https://biblebento.com/dictionary/G2334.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2334/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2334.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2334)