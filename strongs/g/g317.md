# ἀναγκαστῶς

[anagkastōs](https://www.blueletterbible.org/lexicon/g317)

Definition: by constraint (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0317)

[Study Light](https://www.studylight.org/lexicons/greek/317.html)

[Bible Hub](https://biblehub.com/str/greek/317.htm)

[LSJ](https://lsj.gr/wiki/ἀναγκαστῶς)

[NET Bible](http://classic.net.bible.org/strong.php?id=317)

[Bible Bento](https://biblebento.com/dictionary/G317.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/317/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/317.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g317)

