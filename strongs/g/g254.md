# ἅλυσις

[halysis](https://www.blueletterbible.org/lexicon/g254)

Definition: chain (10x), bonds (1x)

Part of speech: feminine noun

Occurs 11 times in 10 verses

Derived words: [halysis](https://en.wikipedia.org/wiki/Halysis)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0254)

[Study Light](https://www.studylight.org/lexicons/greek/254.html)

[Bible Hub](https://biblehub.com/str/greek/254.htm)

[LSJ](https://lsj.gr/wiki/ἅλυσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=254)

[Bible Bento](https://biblebento.com/dictionary/G0254.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0254/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0254.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g254)