# ἀσχήμων

[aschēmōn](https://www.blueletterbible.org/lexicon/g809)

Definition: uncomely (1x), deformed, deformed, inelegant

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0809)

[Study Light](https://www.studylight.org/lexicons/greek/809.html)

[Bible Hub](https://biblehub.com/str/greek/809.htm)

[LSJ](https://lsj.gr/wiki/ἀσχήμων)

[NET Bible](http://classic.net.bible.org/strong.php?id=809)

[Bible Bento](https://biblebento.com/dictionary/G0809.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0809/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0809.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g809)
