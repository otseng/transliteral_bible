# ἀθυμέω

[athymeō](https://www.blueletterbible.org/lexicon/g120)

Definition: be discouraged (1x), to be disheartened, dispirited, broken in spirit

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0120)

[Study Light](https://www.studylight.org/lexicons/greek/120.html)

[Bible Hub](https://biblehub.com/str/greek/120.htm)

[LSJ](https://lsj.gr/wiki/ἀθυμέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=120)

[Bible Bento](https://biblebento.com/dictionary/G0120.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/120/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/120.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g120)
