# Σπανία

[spania](https://www.blueletterbible.org/lexicon/g4681)

Definition: Spain (2x), "scarceness"

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4681)

[Study Light](https://www.studylight.org/lexicons/greek/4681.html)

[Bible Hub](https://biblehub.com/str/greek/4681.htm)

[LSJ](https://lsj.gr/wiki/Σπανία)

[NET Bible](http://classic.net.bible.org/strong.php?id=4681)

[Bible Bento](https://biblebento.com/dictionary/G4681.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4681/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4681.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4681)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/spain.html)

[Video Bible](https://www.videobible.com/bible-dictionary/spain)