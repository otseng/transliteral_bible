# μέθη

[methē](https://www.blueletterbible.org/lexicon/g3178)

Definition: drunkenness (3x), intoxication

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3178)

[Study Light](https://www.studylight.org/lexicons/greek/3178.html)

[Bible Hub](https://biblehub.com/str/greek/3178.htm)

[LSJ](https://lsj.gr/wiki/μέθη)

[NET Bible](http://classic.net.bible.org/strong.php?id=3178)

[Bible Bento](https://biblebento.com/dictionary/G3178.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3178/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3178.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3178)
