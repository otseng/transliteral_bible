# λιθόστρωτος

[lithostrōtos](https://www.blueletterbible.org/lexicon/g3038)

Definition: Pavement (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3038)

[Study Light](https://www.studylight.org/lexicons/greek/3038.html)

[Bible Hub](https://biblehub.com/str/greek/3038.htm)

[LSJ](https://lsj.gr/wiki/λιθόστρωτος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3038)

[Bible Bento](https://biblebento.com/dictionary/G3038.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3038/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3038.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3038)

