# κλείς

[kleis](https://www.blueletterbible.org/lexicon/g2807)

Definition: key (6x), bolt, bar

Part of speech: feminine noun

Occurs 6 times in 6 verses

Derived words: [kleitoris](https://www.etymonline.com/word/clitoris)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2807)

[Study Light](https://www.studylight.org/lexicons/greek/2807.html)

[Bible Hub](https://biblehub.com/str/greek/2807.htm)

[LSJ](https://lsj.gr/wiki/κλείς)

[NET Bible](http://classic.net.bible.org/strong.php?id=2807)

[Bible Bento](https://biblebento.com/dictionary/G2807.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2807/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2807.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2807)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%BB%CE%B5%CE%AF%CF%82)