# λειτουργία

[leitourgia](https://www.blueletterbible.org/lexicon/g3009)

Definition: service (3x), ministry (2x), ministration (1x), a service or ministry of the priests relative to the prayers and sacrifices offered to God, ministration, service, public function (as priest ("liturgy") or almsgiver)

Part of speech: feminine noun

Occurs 6 times in 6 verses

Hebrew: [mĕla'kah](../h/h4399.md)

Derived words: [liturgy](https://en.wikipedia.org/wiki/Liturgy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3009)

[Study Light](https://www.studylight.org/lexicons/greek/3009.html)

[Bible Hub](https://biblehub.com/str/greek/3009.htm)

[LSJ](https://lsj.gr/wiki/λειτουργία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3009)

[Bible Bento](https://biblebento.com/dictionary/G3009.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3009/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3009.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3009)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BB%CE%B5%CE%B9%CF%84%CE%BF%CF%85%CF%81%CE%B3%CE%AF%CE%B1)

[Precept Austin](https://www.preceptaustin.org/hebrews_921-22#m)