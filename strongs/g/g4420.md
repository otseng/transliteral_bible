# πτέρυξ

[pteryx](https://www.blueletterbible.org/lexicon/g4420)

Definition: wing (5x)

Part of speech: feminine noun

Occurs 5 times in 5 verses

Hebrew: [kanaph](../h/h3671.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4420)

[Study Light](https://www.studylight.org/lexicons/greek/4420.html)

[Bible Hub](https://biblehub.com/str/greek/4420.htm)

[LSJ](https://lsj.gr/wiki/πτέρυξ)

[NET Bible](http://classic.net.bible.org/strong.php?id=4420)

[Bible Bento](https://biblebento.com/dictionary/G4420.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4420/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4420.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4420)