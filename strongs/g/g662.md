# ἀποτολμάω

[apotolmaō](https://www.blueletterbible.org/lexicon/g662)

Definition: be very bold (1x), to venture plainly

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0662)

[Study Light](https://www.studylight.org/lexicons/greek/662.html)

[Bible Hub](https://biblehub.com/str/greek/662.htm)

[LSJ](https://lsj.gr/wiki/ἀποτολμάω)

[NET Bible](http://classic.net.bible.org/strong.php?id=662)

[Bible Bento](https://biblebento.com/dictionary/G0662.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0662/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0662.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g662)
