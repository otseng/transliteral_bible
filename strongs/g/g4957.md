# συσταυρόω

[systauroō](https://www.blueletterbible.org/lexicon/g4957)

Definition: crucify with (5x), to crucify along with

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4957)

[Study Light](https://www.studylight.org/lexicons/greek/4957.html)

[Bible Hub](https://biblehub.com/str/greek/4957.htm)

[LSJ](https://lsj.gr/wiki/συσταυρόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4957)

[Bible Bento](https://biblebento.com/dictionary/G4957.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4957/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4957.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4957)