# ψευδοδιδάσκαλος

[pseudodidaskalos](https://www.blueletterbible.org/lexicon/g5572)

Definition: false teacher (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5572)

[Study Light](https://www.studylight.org/lexicons/greek/5572.html)

[Bible Hub](https://biblehub.com/str/greek/5572.htm)

[LSJ](https://lsj.gr/wiki/ψευδοδιδάσκαλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5572)

[Bible Bento](https://biblebento.com/dictionary/G5572.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5572/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5572.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5572)

