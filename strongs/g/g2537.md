# καινός

[kainos](https://www.blueletterbible.org/lexicon/g2537)

Definition: new (44x), recent, unheard of, novel, strange

Part of speech: adjective

Occurs 44 times in 38 verses

Hebrew: [ḥāḏāš](../h/h2319.md)

Usage: [Kainos](https://www.kainos.com/)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2537)

[Study Light](https://www.studylight.org/lexicons/greek/2537.html)

[Bible Hub](https://biblehub.com/str/greek/2537.htm)

[LSJ](https://lsj.gr/wiki/καινός)

[NET Bible](http://classic.net.bible.org/strong.php?id=2537)

[Bible Bento](https://biblebento.com/dictionary/G2537.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2537/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2537.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2537)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%B1%CE%B9%CE%BD%CF%8C%CF%82)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34497)

[Precept Austin](https://www.preceptaustin.org/ephesians_423-24#new)