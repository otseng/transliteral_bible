# ἀναβολή

[anabolē](https://www.blueletterbible.org/lexicon/g311)

Definition: delay (with G4160) (1x), a putting off

Part of speech: feminine noun

Occurs 1 times in 1 verses

Derived words: [anaboly](https://www.merriam-webster.com/dictionary/anaboly)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0311)

[Study Light](https://www.studylight.org/lexicons/greek/311.html)

[Bible Hub](https://biblehub.com/str/greek/311.htm)

[LSJ](https://lsj.gr/wiki/ἀναβολή)

[NET Bible](http://classic.net.bible.org/strong.php?id=311)

[Bible Bento](https://biblebento.com/dictionary/G0311.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0311/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0311.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g311)
