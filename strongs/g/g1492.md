# εἴδω

[eidō](https://www.blueletterbible.org/lexicon/g1492)

Definition: know (281x), cannot tell (with G3756) (8x), know how (7x), wist (6x), miscellaneous (19x), see (314x), behold (17x), look (5x), perceive (5x), lo, visible appearance, physically seen, form, shape

Part of speech: verb

Occurs 691 times in 625 verses

Hebrew: [yada`](../h/h3045.md), [ra'ah](../h/h7200.md), [šûr](../h/h7789.md)

Synonyms: [know](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Know), [look](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Look)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1492)

[Study Light](https://www.studylight.org/lexicons/greek/1492.html)

[Bible Hub](https://biblehub.com/str/greek/1492.htm)

[LSJ](https://lsj.gr/wiki/εἴδω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1492)

[Bible Bento](https://biblebento.com/dictionary/G1492.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1492/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1492.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1492)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34331)

[knowing, Knowing, and KNOWing](http://www.eido.info/study/Notes/2001/03_13_01.htm)

[Deep Strength](https://deepstrength.wordpress.com/2014/08/06/idols/)