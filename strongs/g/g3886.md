# παραλύω

[paralyō](https://www.blueletterbible.org/lexicon/g3886)

Definition: sick of the palsy (2x), taken with palsy (2x), feeble (1x)

Part of speech: verb

Occurs 5 times in 5 verses

Hebrew: [rāp̄â](../h/h7503.md)

Derived words: paralytic

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3886)

[Study Light](https://www.studylight.org/lexicons/greek/3886.html)

[Bible Hub](https://biblehub.com/str/greek/3886.htm)

[LSJ](https://lsj.gr/wiki/παραλύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3886)

[Bible Bento](https://biblebento.com/dictionary/G3886.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3886/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3886.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3886)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%B1%CF%81%CE%B1%CE%BB%CF%8D%CF%89)