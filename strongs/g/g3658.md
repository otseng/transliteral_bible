# ὅμιλος

[homilos](https://www.blueletterbible.org/lexicon/g3658)

Definition: company (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3658)

[Study Light](https://www.studylight.org/lexicons/greek/3658.html)

[Bible Hub](https://biblehub.com/str/greek/3658.htm)

[LSJ](https://lsj.gr/wiki/ὅμιλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3658)

[Bible Bento](https://biblebento.com/dictionary/G3658.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3658/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3658.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3658)

