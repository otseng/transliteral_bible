# ψεῦδος

[pseudos](https://www.blueletterbible.org/lexicon/g5579)

Definition: lie (7x), lying (2x), falsehood

Part of speech: neuter noun

Occurs 9 times in 9 verses

Hebrew: [kaḥaš](../h/h3585.md), [mirmah](../h/h4820.md), [kazab](../h/h3577.md)

Derived words: [pseudo-](https://en.wiktionary.org/wiki/Category:English_words_prefixed_with_pseudo-)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5579)

[Study Light](https://www.studylight.org/lexicons/greek/5579.html)

[Bible Hub](https://biblehub.com/str/greek/5579.htm)

[LSJ](https://lsj.gr/wiki/ψεῦδος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5579)

[Bible Bento](https://biblebento.com/dictionary/G5579.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5579/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5579.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5579)

[Wikipedia](https://en.wikipedia.org/wiki/Pseudo-)

[Wiktionary](https://en.wiktionary.org/wiki/pseudo)