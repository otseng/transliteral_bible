# ἐπιβαρέω

[epibareō](https://www.blueletterbible.org/lexicon/g1912)

Definition: overcharge (1x), be chargeable unto (1x), be chargeable to (1x), to put a burden upon, to load

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1912)

[Study Light](https://www.studylight.org/lexicons/greek/1912.html)

[Bible Hub](https://biblehub.com/str/greek/1912.htm)

[LSJ](https://lsj.gr/wiki/ἐπιβαρέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1912)

[Bible Bento](https://biblebento.com/dictionary/G1912.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1912/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1912.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1912)
