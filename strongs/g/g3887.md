# παραμένω

[paramenō](https://www.blueletterbible.org/lexicon/g3887)

Definition: continue (2x), abide (1x).

Part of speech: verb

Occurs 3 times in 3 verses

Synonyms: [abide](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Abide)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3887)

[Study Light](https://www.studylight.org/lexicons/greek/3887.html)

[Bible Hub](https://biblehub.com/str/greek/3887.htm)

[LSJ](https://lsj.gr/wiki/παραμένω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3887)

[Bible Bento](https://biblebento.com/dictionary/G3887.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3887/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3887.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3887)
