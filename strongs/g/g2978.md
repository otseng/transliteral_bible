# λαῖλαψ

[lailaps](https://www.blueletterbible.org/lexicon/g2978)

Definition: storm (2x), tempest (1x), whirlwind, squall

Part of speech: feminine noun

Occurs 3 times in 3 verses

Hebrew: [sûp̄â](../h/h5492.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2978)

[Study Light](https://www.studylight.org/lexicons/greek/2978.html)

[Bible Hub](https://biblehub.com/str/greek/2978.htm)

[LSJ](https://lsj.gr/wiki/λαῖλαψ)

[NET Bible](http://classic.net.bible.org/strong.php?id=2978)

[Bible Bento](https://biblebento.com/dictionary/G2978.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2978/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2978.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2978)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=35877)