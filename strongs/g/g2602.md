# καταβολή

[katabolē](https://www.blueletterbible.org/lexicon/g2602)

Definition: foundation (10x), to conceive (with G1519) (1x), laying down, structure

Part of speech: feminine noun

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2602)

[Study Light](https://www.studylight.org/lexicons/greek/2602.html)

[Bible Hub](https://biblehub.com/str/greek/2602.htm)

[LSJ](https://lsj.gr/wiki/καταβολή)

[NET Bible](http://classic.net.bible.org/strong.php?id=2602)

[Bible Bento](https://biblebento.com/dictionary/G2602.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2602/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2602.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2602)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%B1%CF%84%CE%B1%CE%B2%CE%BF%CE%BB%CE%AE)