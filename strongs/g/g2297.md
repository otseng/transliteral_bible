# θαυμάσιος

[thaumasios](https://www.blueletterbible.org/lexicon/g2297)

Definition: wonderful (1x), wonderful deeds, wonders, miracle

Part of speech: adjective

Occurs 1 times in 1 verses

Greek: [tharseō](../g/g2293.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2297)

[Study Light](https://www.studylight.org/lexicons/greek/2297.html)

[Bible Hub](https://biblehub.com/str/greek/2297.htm)

[LSJ](https://lsj.gr/wiki/θαυμάσιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2297)

[Bible Bento](https://biblebento.com/dictionary/G2297.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2297/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2297.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2297)
