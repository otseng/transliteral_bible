# ποταπός

[potapos](https://www.blueletterbible.org/lexicon/g4217)

Definition: what manner of (4x), what (1x), what manner of man (1x), what manner of person (1x), what possible sort, how great, how wonderful

Part of speech: adjective

Occurs 7 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4217)

[Study Light](https://www.studylight.org/lexicons/greek/4217.html)

[Bible Hub](https://biblehub.com/str/greek/4217.htm)

[LSJ](https://lsj.gr/wiki/ποταπός)

[NET Bible](http://classic.net.bible.org/strong.php?id=4217)

[Bible Bento](https://biblebento.com/dictionary/G4217.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4217/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4217.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4217)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34239)

[Precept Austin](https://www.preceptaustin.org/1john_31_commentary#g)