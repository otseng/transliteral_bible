# δυσμή

[dysmē](https://www.blueletterbible.org/lexicon/g1424)

Definition: west (5x), sunset

Part of speech: feminine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1424)

[Study Light](https://www.studylight.org/lexicons/greek/1424.html)

[Bible Hub](https://biblehub.com/str/greek/1424.htm)

[LSJ](https://lsj.gr/wiki/δυσμή)

[NET Bible](http://classic.net.bible.org/strong.php?id=1424)

[Bible Bento](https://biblebento.com/dictionary/G1424.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1424/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1424.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1424)