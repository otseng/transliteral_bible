# καταφιλέω

[kataphileō](https://www.blueletterbible.org/lexicon/g2705)

Definition: kiss (6x), kiss again and again, kiss tenderly, kiss much

Part of speech: verb

Occurs 6 times in 6 verses

Hebrew: [nashaq](../h/h5401.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2705)

[Study Light](https://www.studylight.org/lexicons/greek/2705.html)

[Bible Hub](https://biblehub.com/str/greek/2705.htm)

[LSJ](https://lsj.gr/wiki/καταφιλέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2705)

[Bible Bento](https://biblebento.com/dictionary/G2705.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2705/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2705.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2705)

[Precept Austin](https://www.preceptaustin.org/acts_2028-38_commentary#k)