# λούω

[louō](https://www.blueletterbible.org/lexicon/g3068)

Definition: wash (6x), bathe entire body

Part of speech: verb

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3068)

[Study Light](https://www.studylight.org/lexicons/greek/3068.html)

[Bible Hub](https://biblehub.com/str/greek/3068.htm)

[LSJ](https://lsj.gr/wiki/λούω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3068)

[Bible Bento](https://biblebento.com/dictionary/G3068.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3068/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3068.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3068)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BB%CE%BF%CF%8D%CF%89)