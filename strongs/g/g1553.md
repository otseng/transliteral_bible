# ἐκδημέω

[ekdēmeō](https://www.blueletterbible.org/lexicon/g1553)

Definition: be absent (3x), to be or live abroad, emigrate, depart

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1553)

[Study Light](https://www.studylight.org/lexicons/greek/1553.html)

[Bible Hub](https://biblehub.com/str/greek/1553.htm)

[LSJ](https://lsj.gr/wiki/ἐκδημέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1553)

[Bible Bento](https://biblebento.com/dictionary/G1553.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1553/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1553.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1553)
