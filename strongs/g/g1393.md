# Δορκάς

[Dorkas](https://www.blueletterbible.org/lexicon/g1393)

Definition: Dorcas (2x), "gazelle"

Part of speech: proper feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1393)

[Study Light](https://www.studylight.org/lexicons/greek/1393.html)

[Bible Hub](https://biblehub.com/str/greek/1393.htm)

[LSJ](https://lsj.gr/wiki/Δορκάς)

[NET Bible](http://classic.net.bible.org/strong.php?id=1393)

[Bible Bento](https://biblebento.com/dictionary/G1393.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1393/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1393.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1393)

[Wikipedia](https://en.wikipedia.org/wiki/Dorcas)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/dorcas.html)