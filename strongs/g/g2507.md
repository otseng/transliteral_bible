# καθαιρέω

[kathaireō](https://www.blueletterbible.org/lexicon/g2507)

Definition: take down (4x), destroy (2x), put down (1x), pull down (1x), cast down (1x), without the notion of violence: to detach from the cross, with the use of force: to throw down, demolish

Part of speech: verb

Occurs 11 times in 9 verses

Synonyms: [destroy](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Destroy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2507)

[Study Light](https://www.studylight.org/lexicons/greek/2507.html)

[Bible Hub](https://biblehub.com/str/greek/2507.htm)

[LSJ](https://lsj.gr/wiki/καθαιρέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2507)

[Bible Bento](https://biblebento.com/dictionary/G2507.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2507/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2507.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2507)
