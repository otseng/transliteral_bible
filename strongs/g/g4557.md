# σαρδόνυξ

[sardonyx](https://www.blueletterbible.org/lexicon/g4557)

Definition: sardonyx (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4557)

[Study Light](https://www.studylight.org/lexicons/greek/4557.html)

[Bible Hub](https://biblehub.com/str/greek/4557.htm)

[LSJ](https://lsj.gr/wiki/σαρδόνυξ)

[NET Bible](http://classic.net.bible.org/strong.php?id=4557)

[Bible Bento](https://biblebento.com/dictionary/G4557.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4557/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4557.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4557)

