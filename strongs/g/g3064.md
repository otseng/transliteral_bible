# λοιποῦ

[loipou](https://www.blueletterbible.org/lexicon/g3064)

Definition: from henceforth (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3064)

[Study Light](https://www.studylight.org/lexicons/greek/3064.html)

[Bible Hub](https://biblehub.com/str/greek/3064.htm)

[LSJ](https://lsj.gr/wiki/λοιποῦ)

[NET Bible](http://classic.net.bible.org/strong.php?id=3064)

[Bible Bento](https://biblebento.com/dictionary/G3064.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3064/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3064.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3064)

