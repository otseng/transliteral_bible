# παιδευτής

[paideutēs](https://www.blueletterbible.org/lexicon/g3810)

Definition: instructor (1x), which corrected (1x), teacher, discipliner

Part of speech: masculine noun

Occurs 2 times in 2 verses

Hebrew: [mûsār](../h/h4148.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3810)

[Study Light](https://www.studylight.org/lexicons/greek/3810.html)

[Bible Hub](https://biblehub.com/str/greek/3810.htm)

[LSJ](https://lsj.gr/wiki/παιδευτής)

[NET Bible](http://classic.net.bible.org/strong.php?id=3810)

[Bible Bento](https://biblebento.com/dictionary/G3810.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3810/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3810.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3810)
