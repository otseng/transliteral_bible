# ὑστέρησις

[hysterēsis](https://www.blueletterbible.org/lexicon/g5304)

Definition: want (2x), poverty

Part of speech: feminine noun

Occurs 2 times in 2 verses

Greek: [hystereō](../g/g5302.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5304)

[Study Light](https://www.studylight.org/lexicons/greek/5304.html)

[Bible Hub](https://biblehub.com/str/greek/5304.htm)

[LSJ](https://lsj.gr/wiki/ὑστέρησις)

[NET Bible](http://classic.net.bible.org/strong.php?id=5304)

[Bible Bento](https://biblebento.com/dictionary/G5304.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5304/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5304.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5304)