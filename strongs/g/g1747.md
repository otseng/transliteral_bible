# ἐνέδρα

[enedra](https://www.blueletterbible.org/lexicon/g1747)

Definition: laying with (with G4160) (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1747)

[Study Light](https://www.studylight.org/lexicons/greek/1747.html)

[Bible Hub](https://biblehub.com/str/greek/1747.htm)

[LSJ](https://lsj.gr/wiki/ἐνέδρα)

[NET Bible](http://classic.net.bible.org/strong.php?id=1747)

[Bible Bento](https://biblebento.com/dictionary/G1747.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1747/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1747.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1747)

