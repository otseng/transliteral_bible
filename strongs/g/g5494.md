# χειμών

[cheimōn](https://www.blueletterbible.org/lexicon/g5494)

Definition: winter (4x), tempest (1x), foul weather (1x), rainy weather, storm, cold

Part of speech: masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5494)

[Study Light](https://www.studylight.org/lexicons/greek/5494.html)

[Bible Hub](https://biblehub.com/str/greek/5494.htm)

[LSJ](https://lsj.gr/wiki/χειμών)

[NET Bible](http://classic.net.bible.org/strong.php?id=5494)

[Bible Bento](https://biblebento.com/dictionary/G5494.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5494/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5494.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5494)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%87%CE%B5%CE%B9%CE%BC%CF%8E%CE%BD)