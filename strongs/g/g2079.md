# ἐσχάτως

[eschatōs](https://www.blueletterbible.org/lexicon/g2079)

Definition: lie at the point of death (with G2292) (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2079)

[Study Light](https://www.studylight.org/lexicons/greek/2079.html)

[Bible Hub](https://biblehub.com/str/greek/2079.htm)

[LSJ](https://lsj.gr/wiki/ἐσχάτως)

[NET Bible](http://classic.net.bible.org/strong.php?id=2079)

[Bible Bento](https://biblebento.com/dictionary/G2079.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2079/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2079.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2079)

