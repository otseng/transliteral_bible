# ἀνάκειμαι

[anakeimai](https://www.blueletterbible.org/lexicon/g345)

Definition: sit at meat (5x), guests (2x), sit (2x), sit down (1x), be set down (1x), lie (1x), lean (1x), at the table (1x), recline

Part of speech: verb

Occurs 14 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0345)

[Study Light](https://www.studylight.org/lexicons/greek/345.html)

[Bible Hub](https://biblehub.com/str/greek/345.htm)

[LSJ](https://lsj.gr/wiki/ἀνάκειμαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=345)

[Bible Bento](https://biblebento.com/dictionary/G0345.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0345/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0345.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g345)