# Ἁνανίας

[Hananias](https://www.blueletterbible.org/lexicon/g367)

Definition: Ananias (of Damascus) (6x), Ananias (of Jerusalem) (3x), Ananias (high priest) (2x), "whom Jehovah has graciously given"

Part of speech: proper masculine noun

Occurs 11 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0367)

[Study Light](https://www.studylight.org/lexicons/greek/367.html)

[Bible Hub](https://biblehub.com/str/greek/367.htm)

[LSJ](https://lsj.gr/wiki/Ἁνανίας)

[NET Bible](http://classic.net.bible.org/strong.php?id=367)

[Bible Bento](https://biblebento.com/dictionary/G0367.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0367/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0367.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g367)

[Wikipedia](https://en.wikipedia.org/wiki/Ananias_of_Damascus)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ananias.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ananias)