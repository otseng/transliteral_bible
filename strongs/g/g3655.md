# ὄμβρος

[ombros](https://www.blueletterbible.org/lexicon/g3655)

Definition: shower (1x), thunder storm, violent storm, accompanied by high wind with thunder and lightning

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3655)

[Study Light](https://www.studylight.org/lexicons/greek/3655.html)

[Bible Hub](https://biblehub.com/str/greek/3655.htm)

[LSJ](https://lsj.gr/wiki/ὄμβρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3655)

[Bible Bento](https://biblebento.com/dictionary/G3655.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3655/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3655.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3655)