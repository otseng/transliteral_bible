# αὐξάνω

[auxanō](https://www.blueletterbible.org/lexicon/g837)

Definition: grow (12x), increase (7x), give the increase (2x), grow up (1x), augment, enlarge

Part of speech: verb

Occurs 22 times in 22 verses

Hebrew: [parah](../h/h6509.md), [tsalach](../h/h6743.md)

Derived words: [auxanology](https://medical-dictionary.thefreedictionary.com/auxanology), [auxanometer](https://en.wikipedia.org/wiki/Auxanometer), [auxo-](https://en.wiktionary.org/wiki/Category:English_words_prefixed_with_auxo-)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0837)

[Study Light](https://www.studylight.org/lexicons/greek/837.html)

[Bible Hub](https://biblehub.com/str/greek/837.htm)

[LSJ](https://lsj.gr/wiki/αὐξάνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=837)

[Bible Bento](https://biblebento.com/dictionary/G0837.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0837/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0837.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g837)

[Resounding the faith](http://resoundingthefaith.com/2017/02/%E2%80%8Egreek-%CE%B1%E1%BD%90%CE%BE%CE%AC%CE%BD%CF%89-auxano/)