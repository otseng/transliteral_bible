# ῥιπή

[rhipē](https://www.blueletterbible.org/lexicon/g4493)

Definition: twinkling (1x), stroke, beat, jerk, blink

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4493)

[Study Light](https://www.studylight.org/lexicons/greek/4493.html)

[Bible Hub](https://biblehub.com/str/greek/4493.htm)

[LSJ](https://lsj.gr/wiki/ῥιπή)

[NET Bible](http://classic.net.bible.org/strong.php?id=4493)

[Bible Bento](https://biblebento.com/dictionary/G4493.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4493/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4493.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4493)
