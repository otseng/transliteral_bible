# φιλότεκνος

[philoteknos](https://www.blueletterbible.org/lexicon/g5388)

Definition: love (one's) children (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5388)

[Study Light](https://www.studylight.org/lexicons/greek/5388.html)

[Bible Hub](https://biblehub.com/str/greek/5388.htm)

[LSJ](https://lsj.gr/wiki/φιλότεκνος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5388)

[Bible Bento](https://biblebento.com/dictionary/G5388.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5388/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5388.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5388)
