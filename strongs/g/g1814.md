# ἐξάλλομαι

[exallomai](https://www.blueletterbible.org/lexicon/g1814)

Definition: leap up (1x)

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1814)

[Study Light](https://www.studylight.org/lexicons/greek/1814.html)

[Bible Hub](https://biblehub.com/str/greek/1814.htm)

[LSJ](https://lsj.gr/wiki/ἐξάλλομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1814)

[Bible Bento](https://biblebento.com/dictionary/G1814.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1814/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1814.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1814)
