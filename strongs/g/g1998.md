# ἐπισυντρέχω

[episyntrechō](https://www.blueletterbible.org/lexicon/g1998)

Definition: come running together (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/1998.html)

[Bible Hub](https://biblehub.com/str/greek/1998.htm)

[LSJ](https://lsj.gr/wiki/ἐπισυντρέχω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1998)

[Bible Bento](https://biblebento.com/dictionary/G1998.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1998/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1998.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1998)

