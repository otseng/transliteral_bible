# ἐπιλαμβάνομαι

[epilambanomai](https://www.blueletterbible.org/lexicon/g1949)

Definition: take (7x), take by (3x), catch (2x), take on (2x), lay hold on (2x), take hold of (2x), lay hold upon (1x).

Part of speech: verb

Occurs 20 times in 18 verses

Hebrew: [ḥāzaq](../h/h2388.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1949)

[Study Light](https://www.studylight.org/lexicons/greek/1949.html)

[Bible Hub](https://biblehub.com/str/greek/1949.htm)

[LSJ](https://lsj.gr/wiki/ἐπιλαμβάνομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1949)

[Bible Bento](https://biblebento.com/dictionary/G1949.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1949/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1949.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1949)
