# κεντυρίων

[kentyriōn](https://www.blueletterbible.org/lexicon/g2760)

Definition: centurion (3x), an officer in the Roman army

Part of speech: masculine noun

Occurs 3 times in 3 verses

Derived words: centurion

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2760)

[Study Light](https://www.studylight.org/lexicons/greek/2760.html)

[Bible Hub](https://biblehub.com/str/greek/2760.htm)

[LSJ](https://lsj.gr/wiki/κεντυρίων)

[NET Bible](http://classic.net.bible.org/strong.php?id=2760)

[Bible Bento](https://biblebento.com/dictionary/G2760.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2760/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2760.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2760)

[Wikipedia](https://en.wikipedia.org/wiki/Centurion)