# εὐαγγέλιον

[euaggelion](https://www.blueletterbible.org/lexicon/g2098)

Definition: gospel (46x), gospel of Christ (11x), gospel of God (7x), gospel of the Kingdom (3x), good tidings, good message

Part of speech: neuter noun

Occurs 77 times in 74 verses

Hebrew: [bᵊśôrâ](../h/h1309.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2098)

[Study Light](https://www.studylight.org/lexicons/greek/2098.html)

[Bible Hub](https://biblehub.com/str/greek/2098.htm)

[LSJ](https://lsj.gr/wiki/εὐαγγέλιον)

[NET Bible](http://classic.net.bible.org/strong.php?id=2098)

[Bible Bento](https://biblebento.com/dictionary/G2098.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2098/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2098.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2098)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B5%E1%BD%90%CE%B1%CE%B3%CE%B3%CE%AD%CE%BB%CE%B9%CE%BF%CE%BD)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34166)

[Glen Davis](https://glenandpaula.com/wordpress/archives/2010/02/25/pre-christian-uses-of-gospel)

[Word frequency information](http://www.perseus.tufts.edu/hopper/wordfreq?lang=greek&lookup=eu%29agge%2Flion)