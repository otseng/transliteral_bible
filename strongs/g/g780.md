# ἀσμένως

[asmenōs](https://www.blueletterbible.org/lexicon/g780)

Definition: gladly (2x), with joy, gladness

Part of speech: adverb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0780)

[Study Light](https://www.studylight.org/lexicons/greek/780.html)

[Bible Hub](https://biblehub.com/str/greek/780.htm)

[LSJ](https://lsj.gr/wiki/ἀσμένως)

[NET Bible](http://classic.net.bible.org/strong.php?id=780)

[Bible Bento](https://biblebento.com/dictionary/G0780.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0780/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0780.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g780)
