# Ῥοῦφος

[Rhouphos](https://www.blueletterbible.org/lexicon/g4504)

Definition: Rufus (2x), "red"

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4504)

[Study Light](https://www.studylight.org/lexicons/greek/4504.html)

[Bible Hub](https://biblehub.com/str/greek/4504.htm)

[LSJ](https://lsj.gr/wiki/Ῥοῦφος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4504)

[Bible Bento](https://biblebento.com/dictionary/G4504.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4504/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4504.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4504)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/R/rufus.html)