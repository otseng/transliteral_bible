# οὔτε

[oute](https://www.blueletterbible.org/lexicon/g3777)

Definition: neither (44x), nor (40x), nor yet (4x), no not (1x), not (1x), yet not (1x), miscellaneous (3x).

Part of speech: adverb

Occurs 94 times in 45 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3777)

[Study Light](https://www.studylight.org/lexicons/greek/3777.html)

[Bible Hub](https://biblehub.com/str/greek/3777.htm)

[LSJ](https://lsj.gr/wiki/οὔτε)

[NET Bible](http://classic.net.bible.org/strong.php?id=3777)

[Bible Bento](https://biblebento.com/dictionary/G3777.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3777/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3777.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3777)

