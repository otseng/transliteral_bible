# ἐσμέν

[esmen](https://www.blueletterbible.org/lexicon/g2070)

Definition: are (49x), have hope (with G1679) (1x), was (1x), be (1x), have our being (1x).

Part of speech: verb

Occurs 55 times in 51 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2070)

[Study Light](https://www.studylight.org/lexicons/greek/2070.html)

[Bible Hub](https://biblehub.com/str/greek/2070.htm)

[LSJ](https://lsj.gr/wiki/ἐσμέν)

[NET Bible](http://classic.net.bible.org/strong.php?id=2070)

[Bible Bento](https://biblebento.com/dictionary/G2070.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2070/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2070.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2070)
