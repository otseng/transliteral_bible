# γέλως

[gelōs](https://www.blueletterbible.org/lexicon/g1071)

Definition: laughter (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

Hebrew: [śᵊḥôq](../h/h7814.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1071)

[Study Light](https://www.studylight.org/lexicons/greek/1071.html)

[Bible Hub](https://biblehub.com/str/greek/1071.htm)

[LSJ](https://lsj.gr/wiki/γέλως)

[NET Bible](http://classic.net.bible.org/strong.php?id=1071)

[Bible Bento](https://biblebento.com/dictionary/G1071.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1071/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1071.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1071)

