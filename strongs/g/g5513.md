# χλιαρός

[chliaros](https://www.blueletterbible.org/lexicon/g5513)

Definition: lukewarm (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5513)

[Study Light](https://www.studylight.org/lexicons/greek/5513.html)

[Bible Hub](https://biblehub.com/str/greek/5513.htm)

[LSJ](https://lsj.gr/wiki/χλιαρός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5513)

[Bible Bento](https://biblebento.com/dictionary/G5513.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5513/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5513.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5513)

