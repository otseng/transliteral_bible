# βασίλισσα

[basilissa](https://www.blueletterbible.org/lexicon/g938)

Definition: queen (4x)

Part of speech: feminine noun

Occurs 4 times in 4 verses

Hebrew: [malkâ](../h/h4436.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0938)

[Study Light](https://www.studylight.org/lexicons/greek/938.html)

[Bible Hub](https://biblehub.com/str/greek/938.htm)

[LSJ](https://lsj.gr/wiki/βασίλισσα)

[NET Bible](http://classic.net.bible.org/strong.php?id=938)

[Bible Bento](https://biblebento.com/dictionary/G0938.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0938/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0938.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g938)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B2%CE%B1%CF%83%CE%AF%CE%BB%CE%B9%CF%83%CF%83%CE%B1)

[Wikipedia](https://en.wikipedia.org/wiki/Basilissa_%28disambiguation%29)