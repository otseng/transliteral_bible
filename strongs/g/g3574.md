# νυχθήμερον

[nychthēmeron](https://www.blueletterbible.org/lexicon/g3574)

Definition: a night and a day (1x).

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3574)

[Study Light](https://www.studylight.org/lexicons/greek/3574.html)

[Bible Hub](https://biblehub.com/str/greek/3574.htm)

[LSJ](https://lsj.gr/wiki/νυχθήμερον)

[NET Bible](http://classic.net.bible.org/strong.php?id=3574)

[Bible Bento](https://biblebento.com/dictionary/G3574.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3574/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3574.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3574)

