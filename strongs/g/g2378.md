# θυσία

[thysia](https://www.blueletterbible.org/lexicon/g2378)

Definition: sacrifice (29x), victim, offering

Part of speech: feminine noun

Occurs 29 times in 29 verses

Greek: [thysiastērion](../g/g2379.md)

Hebrew: [zebach](../h/h2077.md), [minchah](../h/h4503.md), [ʿōlâ](../h/h5930.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2378)

[Study Light](https://www.studylight.org/lexicons/greek/2378.html)

[Bible Hub](https://biblehub.com/str/greek/2378.htm)

[LSJ](https://lsj.gr/wiki/θυσία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2378)

[Bible Bento](https://biblebento.com/dictionary/G2378.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2378/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2378.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2378)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B8%CF%85%CF%83%CE%AF%CE%B1)

[Hellenica World](http://www.hellenicaworld.com/Greece/LX/en/Thysia.html)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/2378-thusia-sacrifice.htm)
