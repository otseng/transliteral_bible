# ὑπερέκεινα

[hyperekeina](https://www.blueletterbible.org/lexicon/g5238)

Definition: beyond (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5238)

[Study Light](https://www.studylight.org/lexicons/greek/5238.html)

[Bible Hub](https://biblehub.com/str/greek/5238.htm)

[LSJ](https://lsj.gr/wiki/ὑπερέκεινα)

[NET Bible](http://classic.net.bible.org/strong.php?id=5238)

[Bible Bento](https://biblebento.com/dictionary/G5238.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5238/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5238.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5238)

