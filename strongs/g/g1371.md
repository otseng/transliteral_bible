# διχοτομέω

[dichotomeō](https://www.blueletterbible.org/lexicon/g1371)

Definition: cut sunder (1x), cut in asunder (1x), cut into two parts, cruel method of punishment used by the Hebrews and others of cutting one in two, cut up by scourging, scourge severely

Part of speech: verb

Occurs 4 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1371)

[Study Light](https://www.studylight.org/lexicons/greek/1371.html)

[Bible Hub](https://biblehub.com/str/greek/1371.htm)

[LSJ](https://lsj.gr/wiki/διχοτομέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1371)

[Bible Bento](https://biblebento.com/dictionary/G1371.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1371/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1371.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1371)

[Precept Austin](https://www.preceptaustin.org/luke-12-commentary#cut)

[Resounding the Faith](http://resoundingthefaith.com/2017/04/greek-%CE%B4%CE%B9%CF%87%CE%BF%CF%84%CE%BF%CE%BC%CE%AD%CF%89-dichotomeo/)