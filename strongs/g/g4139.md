# πλησίον

[plēsion](https://www.blueletterbible.org/lexicon/g4139)

Definition: neighbour (16x), near (1x), friend, fellow member of the covenant community, neighbor

Part of speech: adverb

Occurs 17 times in 17 verses

Hebrew: [rea`](../h/h7453.md)

Derived words: [plesiochronous](https://en.wikipedia.org/wiki/Plesiochronous_system), plesiosaur, [plesi-](https://wordinfo.info/unit/1697)

Synonyms: [neighbor](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Neighbor)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4139)

[Study Light](https://www.studylight.org/lexicons/greek/4139.html)

[Bible Hub](https://biblehub.com/str/greek/4139.htm)

[LSJ](https://lsj.gr/wiki/πλησίον)

[NET Bible](http://classic.net.bible.org/strong.php?id=4139)

[Bible Bento](https://biblebento.com/dictionary/G4139.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4139/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4139.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4139)

[Theological Sushi](http://theologicalsushi.blogspot.com/2016/07/who-is-my-neighbor.html)