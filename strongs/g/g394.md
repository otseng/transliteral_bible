# ἀνατίθημι

[anatithēmi](https://www.blueletterbible.org/lexicon/g394)

Definition: declare (1x), communicate (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g394)

[Study Light](https://www.studylight.org/lexicons/greek/394.html)

[Bible Hub](https://biblehub.com/str/greek/394.htm)

[LSJ](https://lsj.gr/wiki/ἀνατίθημι)

[NET Bible](http://classic.net.bible.org/strong.php?id=394)

[Bible Bento](https://biblebento.com/dictionary/G394.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/394/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/394.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g394)

