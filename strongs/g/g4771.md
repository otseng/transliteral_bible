# σύ

[sy](https://www.blueletterbible.org/lexicon/g4771)

Definition: thou (178x), you

Part of speech: pronoun

Occurs 178 times in 163 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4771)

[Study Light](https://www.studylight.org/lexicons/greek/4771.html)

[Bible Hub](https://biblehub.com/str/greek/4771.htm)

[LSJ](https://lsj.gr/wiki/σύ)

[NET Bible](http://classic.net.bible.org/strong.php?id=4771)

[Bible Bento](https://biblebento.com/dictionary/G4771.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4771/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4771.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4771)
