# κενοφωνία

[kenophōnia](https://www.blueletterbible.org/lexicon/g2757)

Definition: vain babblings (2x), empty discussion, discussion of vain and useless matters

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2757)

[Study Light](https://www.studylight.org/lexicons/greek/2757.html)

[Bible Hub](https://biblehub.com/str/greek/2757.htm)

[LSJ](https://lsj.gr/wiki/κενοφωνία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2757)

[Bible Bento](https://biblebento.com/dictionary/G2757.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2757/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2757.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2757)
