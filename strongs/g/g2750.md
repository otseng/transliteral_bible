# κειρία

[keiria](https://www.blueletterbible.org/lexicon/g2750)

Definition: graveclothes (1x), winding sheet, band

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2750)

[Study Light](https://www.studylight.org/lexicons/greek/2750.html)

[Bible Hub](https://biblehub.com/str/greek/2750.htm)

[LSJ](https://lsj.gr/wiki/κειρία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2750)

[Bible Bento](https://biblebento.com/dictionary/G2750.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2750/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2750.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2750)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%B5%CE%B9%CF%81%CE%AF%CE%B1)