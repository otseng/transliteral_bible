# φημί

[phēmi](https://www.blueletterbible.org/lexicon/g5346)

Definition: say (57x), affirm (1x), declare, speak

Part of speech: verb

Occurs 58 times in 57 verses

Greek: [prophētēs](../g/g4396.md)

Synonyms: [say](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Say)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5346)

[Study Light](https://www.studylight.org/lexicons/greek/5346.html)

[Bible Hub](https://biblehub.com/str/greek/5346.htm)

[LSJ](https://lsj.gr/wiki/φημί)

[NET Bible](http://classic.net.bible.org/strong.php?id=5346)

[Bible Bento](https://biblebento.com/dictionary/G5346.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5346/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5346.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5346)