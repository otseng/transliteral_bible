# Ἰεσσαί

[Iessai](https://www.blueletterbible.org/lexicon/g2421)

Definition: Jesse (5x), "wealthy", the father of David the king

Part of speech: proper masculine noun

Occurs 5 times in 5 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/2421.html)

[LSJ](https://lsj.gr/wiki/Ἰεσσαί)

[NET Bible](http://classic.net.bible.org/strong.php?id=2421)

[Bible Bento](https://biblebento.com/dictionary/G2421.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2421/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2421.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2421)

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2421)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jesse.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jesse)