# συναλλάσσω

[synallassō](https://www.blueletterbible.org/lexicon/g4900)

Definition: set at one again (with G1515) (with G1519) (1x), to reconcile, to drive together

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4900)

[Study Light](https://www.studylight.org/lexicons/greek/4900.html)

[Bible Hub](https://biblehub.com/str/greek/4900.htm)

[LSJ](https://lsj.gr/wiki/συναλλάσσω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4900)

[Bible Bento](https://biblebento.com/dictionary/G4900.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4900/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4900.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4900)
