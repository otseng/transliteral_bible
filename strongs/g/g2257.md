# ἡμῶν

[hēmōn](https://www.blueletterbible.org/lexicon/g2257)

Definition: our (313x), us (82x), we (12x)

Part of speech: pronoun

Occurs 410 times in 365 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2257)

[Study Light](https://www.studylight.org/lexicons/greek/2257.html)

[Bible Hub](https://biblehub.com/str/greek/2257.htm)

[LSJ](https://lsj.gr/wiki/ἡμῶν)

[NET Bible](http://classic.net.bible.org/strong.php?id=2257)

[Bible Bento](https://biblebento.com/dictionary/G2257.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2257/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2257.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2257)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%A1%CE%BC%E1%BF%B6%CE%BD)