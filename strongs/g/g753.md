# ἀρχιτέκτων

[architektōn](https://www.blueletterbible.org/lexicon/g753)

Definition: masterbuilder (1x), chief constructor, master builder, architect

Part of speech: masculine noun

Occurs 1 times in 1 verses

Derived words: architect

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0753)

[Study Light](https://www.studylight.org/lexicons/greek/753.html)

[Bible Hub](https://biblehub.com/str/greek/753.htm)

[LSJ](https://lsj.gr/wiki/ἀρχιτέκτων)

[NET Bible](http://classic.net.bible.org/strong.php?id=753)

[Bible Bento](https://biblebento.com/dictionary/G0753.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0753/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0753.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g753)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CF%81%CF%87%CE%B9%CF%84%CE%AD%CE%BA%CF%84%CF%89%CE%B)
