# φονεύς

[phoneus](https://www.blueletterbible.org/lexicon/g5406)

Definition: murderer (7x), criminal, homicide

Part of speech: masculine noun

Occurs 7 times in 7 verses

Greek: [phoneuō](../g/g5407.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5406)

[Study Light](https://www.studylight.org/lexicons/greek/5406.html)

[Bible Hub](https://biblehub.com/str/greek/5406.htm)

[LSJ](https://lsj.gr/wiki/φονεύς)

[NET Bible](http://classic.net.bible.org/strong.php?id=5406)

[Bible Bento](https://biblebento.com/dictionary/G5406.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5406/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5406.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5406)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%86%CE%BF%CE%BD%CE%B5%CF%8D%CF%82)

[Precept Austin](https://www.preceptaustin.org/acts-7-commentary#murder)
