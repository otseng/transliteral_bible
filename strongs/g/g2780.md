# κηπουρός

[kēpouros](https://www.blueletterbible.org/lexicon/g2780)

Definition: gardener (1x)

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2780)

[Study Light](https://www.studylight.org/lexicons/greek/2780.html)

[Bible Hub](https://biblehub.com/str/greek/2780.htm)

[LSJ](https://lsj.gr/wiki/κηπουρός)

[NET Bible](http://classic.net.bible.org/strong.php?id=2780)

[Bible Bento](https://biblebento.com/dictionary/G2780.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2780/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2780.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2780)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%B7%CF%80%CE%BF%CF%85%CF%81%CF%8C%CF%82)