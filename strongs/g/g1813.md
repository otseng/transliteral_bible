# ἐξαλείφω

[exaleiphō](https://www.blueletterbible.org/lexicon/g1813)

Definition: blot out (3x), wipe away (2x), to anoint or wash in every part, obliterate, delete, cancel, erase, expunge

Part of speech: verb

Occurs 6 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1813)

[Study Light](https://www.studylight.org/lexicons/greek/1813.html)

[Bible Hub](https://biblehub.com/str/greek/1813.htm)

[LSJ](https://lsj.gr/wiki/ἐξαλείφω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1813)

[Bible Bento](https://biblebento.com/dictionary/G1813.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1813/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1813.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1813)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B5%CE%BE%CE%B1%CE%BB%CE%B5%CE%AF%CF%86%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33670)

[Precept Austin](https://www.preceptaustin.org/colossians_214-151#canceled)
