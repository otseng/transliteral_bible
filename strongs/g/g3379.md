# μήποτε

[mēpote](https://www.blueletterbible.org/lexicon/g3379)

Definition: lest (12x), lest at any time (7x), whether or not (1x), lest haply (with G2443) (1x), if peradventure (1x), no ... not at all (1x), perhaps

Part of speech: conjunction

Occurs 28 times in 25 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3379)

[Study Light](https://www.studylight.org/lexicons/greek/3379.html)

[Bible Hub](https://biblehub.com/str/greek/3379.htm)

[LSJ](https://lsj.gr/wiki/μήποτε)

[NET Bible](http://classic.net.bible.org/strong.php?id=3379)

[Bible Bento](https://biblebento.com/dictionary/G3379.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3379/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3379.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3379)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BC%CE%AE%CF%80%CE%BF%CF%84%CE%B5)
