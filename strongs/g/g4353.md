# προσκυνητής

[proskynētēs](https://www.blueletterbible.org/lexicon/g4353)

Definition: worshipper (1x)

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4353)

[Study Light](https://www.studylight.org/lexicons/greek/4353.html)

[Bible Hub](https://biblehub.com/str/greek/4353.htm)

[LSJ](https://lsj.gr/wiki/προσκυνητής)

[NET Bible](http://classic.net.bible.org/strong.php?id=4353)

[Bible Bento](https://biblebento.com/dictionary/G4353.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4353/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4353.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4353)