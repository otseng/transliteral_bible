# ὀκτώ

[oktō](https://www.blueletterbible.org/lexicon/g3638)

Definition: eight (6x), eighteen (with G1176) (with G2532) (3x).

Part of speech: indeclinable noun

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3638)

[Study Light](https://www.studylight.org/lexicons/greek/3638.html)

[Bible Hub](https://biblehub.com/str/greek/3638.htm)

[LSJ](https://lsj.gr/wiki/ὀκτώ)

[NET Bible](http://classic.net.bible.org/strong.php?id=3638)

[Bible Bento](https://biblebento.com/dictionary/G3638.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3638/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3638.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3638)

