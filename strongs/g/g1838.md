# ἕξις

[hexis](https://www.blueletterbible.org/lexicon/g1838)

Definition: use (1x), a power acquired by custom, practice

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1838)

[Study Light](https://www.studylight.org/lexicons/greek/1838.html)

[Bible Hub](https://biblehub.com/str/greek/1838.htm)

[LSJ](https://lsj.gr/wiki/ἕξις)

[NET Bible](http://classic.net.bible.org/strong.php?id=1838)

[Bible Bento](https://biblebento.com/dictionary/G1838.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1838/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1838.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1838)
