# σείω

[seiō](https://www.blueletterbible.org/lexicon/g4579)

Definition: shake (3x), move (1x), quake (1x), to shake, agitate, cause to tremble, to quake for fear, to agitate the mind, to rock

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4579)

[Study Light](https://www.studylight.org/lexicons/greek/4579.html)

[Bible Hub](https://biblehub.com/str/greek/4579.htm)

[LSJ](https://lsj.gr/wiki/σείω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4579)

[Bible Bento](https://biblebento.com/dictionary/G4579.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4579/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4579.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4579)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CE%B5%CE%AF%CF%89)
