# ὡσαύτως

[hōsautōs](https://www.blueletterbible.org/lexicon/g5615)

Definition: likewise (13x), in like manner (2x), even so (1x), after the same manner (1x).

Part of speech: adverb

Occurs 17 times in 17 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5615)

[Study Light](https://www.studylight.org/lexicons/greek/5615.html)

[Bible Hub](https://biblehub.com/str/greek/5615.htm)

[LSJ](https://lsj.gr/wiki/ὡσαύτως)

[NET Bible](http://classic.net.bible.org/strong.php?id=5615)

[Bible Bento](https://biblebento.com/dictionary/G5615.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5615/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5615.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5615)
