# ναί

[nai](https://www.blueletterbible.org/lexicon/g3483)

Definition: yea (23x), even so (5x), yes (3x), truth (1x), verily (1x), surely (1x), a primary particle of strong affirmation

Part of speech: particle

Occurs 34 times in 29 verses

Synonyms: [verily](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Verily)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3483)

[Study Light](https://www.studylight.org/lexicons/greek/3483.html)

[Bible Hub](https://biblehub.com/str/greek/3483.htm)

[LSJ](https://lsj.gr/wiki/ναί)

[NET Bible](http://classic.net.bible.org/strong.php?id=3483)

[Bible Bento](https://biblebento.com/dictionary/G3483.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3483/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3483.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3483)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BD%CE%B1%CE%AF)