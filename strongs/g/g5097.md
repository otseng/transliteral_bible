# τιμωρέω

[timōreō](https://www.blueletterbible.org/lexicon/g5097)

Definition: punish (2x), to take vengeance on one, protect one's honor, avenge

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5097)

[Study Light](https://www.studylight.org/lexicons/greek/5097.html)

[Bible Hub](https://biblehub.com/str/greek/5097.htm)

[LSJ](https://lsj.gr/wiki/τιμωρέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5097)

[Bible Bento](https://biblebento.com/dictionary/G5097.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5097/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5097.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5097)
