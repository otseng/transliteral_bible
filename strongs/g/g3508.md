# Νεφθαλίμ

[Nephthalim](https://www.blueletterbible.org/lexicon/g3508)

Definition: Nephthalim (3x), Naphtali = "wrestling"

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3508)

[Study Light](https://www.studylight.org/lexicons/greek/3508.html)

[Bible Hub](https://biblehub.com/str/greek/3508.htm)

[LSJ](https://lsj.gr/wiki/Νεφθαλίμ)

[NET Bible](http://classic.net.bible.org/strong.php?id=3508)

[Bible Bento](https://biblebento.com/dictionary/G3508.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3508/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3508.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3508)
