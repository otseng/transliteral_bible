# αἰτίαμα

[aitiama](https://www.blueletterbible.org/lexicon/g157)

Definition: complaint (1x), to accuse, bring a charge against

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0157)

[Study Light](https://www.studylight.org/lexicons/greek/157.html)

[Bible Hub](https://biblehub.com/str/greek/157.htm)

[LSJ](https://lsj.gr/wiki/αἰτίαμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=157)

[Bible Bento](https://biblebento.com/dictionary/G0157.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0157/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0157.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g157)
