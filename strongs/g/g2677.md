# καταρτισμός

[katartismos](https://www.blueletterbible.org/lexicon/g2677)

Definition: perfecting (1x), complete furnishing, equipping

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2677)

[Study Light](https://www.studylight.org/lexicons/greek/2677.html)

[Bible Hub](https://biblehub.com/str/greek/2677.htm)

[LSJ](https://lsj.gr/wiki/καταρτισμός)

[NET Bible](http://classic.net.bible.org/strong.php?id=2677)

[Bible Bento](https://biblebento.com/dictionary/G2677.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2677/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2677.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2677)
