# ὄρθριος

[orthrios](https://www.blueletterbible.org/lexicon/g3721)

Definition: early (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3721)

[Study Light](https://www.studylight.org/lexicons/greek/3721.html)

[Bible Hub](https://biblehub.com/str/greek/3721.htm)

[LSJ](https://lsj.gr/wiki/ὄρθριος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3721)

[Bible Bento](https://biblebento.com/dictionary/G3721.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3721/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3721.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3721)

