# συζάω

[syzaō](https://www.blueletterbible.org/lexicon/g4800)

Definition: live with (3x), to live together with one, co-survive

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4800)

[Study Light](https://www.studylight.org/lexicons/greek/4800.html)

[Bible Hub](https://biblehub.com/str/greek/4800.htm)

[LSJ](https://lsj.gr/wiki/συζάω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4800)

[Bible Bento](https://biblebento.com/dictionary/G4800.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4800/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4800.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4800)
