# ἀναθεματίζω

[anathematizō](https://www.blueletterbible.org/lexicon/g332)

Definition: curse (1x), bind under a curse (1x), bind with an oath (1x), bind under a great curse (with G331) (1x), bind with an oath, to declare one's self liable to the severest divine penalties, to declare or vow under penalty of execration

Part of speech: verb

Occurs 6 times in 4 verses

Derived words: [anathema](https://en.wikipedia.org/wiki/Anathema)

Synonyms: [curse](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Curse)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0332)

[Study Light](https://www.studylight.org/lexicons/greek/332.html)

[Bible Hub](https://biblehub.com/str/greek/332.htm)

[LSJ](https://lsj.gr/wiki/ἀναθεματίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=332)

[Bible Bento](https://biblebento.com/dictionary/G0332.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0332/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0332.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g332)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CE%BD%CE%B1%CE%B8%CE%B5%CE%BC%CE%B1%CF%84%CE%AF%CE%B6%CF%89)