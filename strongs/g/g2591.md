# Κάρπος

[karpos](https://www.blueletterbible.org/lexicon/g2591)

Definition: Carpus (1x), "fruit", a Christian at Troas

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2591)

[Study Light](https://www.studylight.org/lexicons/greek/2591.html)

[Bible Hub](https://biblehub.com/str/greek/2591.htm)

[LSJ](https://lsj.gr/wiki/Κάρπος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2591)

[Bible Bento](https://biblebento.com/dictionary/G2591.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2591/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2591.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2591)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/carpus.html)

[Video Bible](https://www.videobible.com/bible-dictionary/carpus)