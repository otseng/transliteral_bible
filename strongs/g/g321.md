# ἀνάγω

[anagō](https://www.blueletterbible.org/lexicon/g321)

Definition: bring (3x), loose (3x), sail (3x), launch (3x), depart (3x), launch out, set sail, put to sea

Part of speech: verb

Occurs 27 times in 24 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0321)

[Study Light](https://www.studylight.org/lexicons/greek/321.html)

[Bible Hub](https://biblehub.com/str/greek/321.htm)

[LSJ](https://lsj.gr/wiki/ἀνάγω)

[NET Bible](http://classic.net.bible.org/strong.php?id=321)

[Bible Bento](https://biblebento.com/dictionary/G0321.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0321/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0321.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g321)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CE%BD%CE%AC%CE%B3%CF%89)

[Precept Austin](https://www.preceptaustin.org/acts-20-commentary#setsail)