# διότι

[dioti](https://www.blueletterbible.org/lexicon/g1360)

Definition: because (10x), for (8x), because that (3x), therefore (1x).

Part of speech: conjunction

Occurs 22 times in 21 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1360)

[Study Light](https://www.studylight.org/lexicons/greek/1360.html)

[Bible Hub](https://biblehub.com/str/greek/1360.htm)

[LSJ](https://lsj.gr/wiki/διότι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1360)

[Bible Bento](https://biblebento.com/dictionary/G1360.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1360/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1360.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1360)

