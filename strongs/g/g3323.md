# Μεσσίας

[Messias](https://www.blueletterbible.org/lexicon/g3323)

Definition: Messias (2x), the Greek form of Messiah

Part of speech: masculine noun

Occurs 2 times in 2 verses

Hebrew: [mashiyach](../h/h4899.md)

Derived words: Messiah

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3323)

[Study Light](https://www.studylight.org/lexicons/greek/3323.html)

[Bible Hub](https://biblehub.com/str/greek/3323.htm)

[LSJ](https://lsj.gr/wiki/Μεσσίας)

[NET Bible](http://classic.net.bible.org/strong.php?id=3323)

[Bible Bento](https://biblebento.com/dictionary/G3323.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3323/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3323.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3323)