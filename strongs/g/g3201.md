# μέμφομαι

[memphomai](https://www.blueletterbible.org/lexicon/g3201)

Definition: find fault (3x), blame, accuse

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3201)

[Study Light](https://www.studylight.org/lexicons/greek/3201.html)

[Bible Hub](https://biblehub.com/str/greek/3201.htm)

[LSJ](https://lsj.gr/wiki/μέμφομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=3201)

[Bible Bento](https://biblebento.com/dictionary/G3201.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3201/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3201.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3201)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BC%CE%AD%CE%BC%CF%86%CE%BF%CE%BC%CE%B1%CE%B9)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34062)