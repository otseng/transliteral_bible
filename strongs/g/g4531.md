# σαλεύω

[saleuō](https://www.blueletterbible.org/lexicon/g4531)

Definition: shake (10x), move (1x), shake together (1x), that are shaken (1x), which cannot be shaken (with G3361) (1x), stir up (1x), agitate, overthrow

Part of speech: verb

Occurs 17 times in 14 verses

Hebrew: [mowt](../h/h4131.md), [ra`am](../h/h7481.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4531)

[Study Light](https://www.studylight.org/lexicons/greek/4531.html)

[Bible Hub](https://biblehub.com/str/greek/4531.htm)

[LSJ](https://lsj.gr/wiki/σαλεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4531)

[Bible Bento](https://biblebento.com/dictionary/G4531.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4531/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4531.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4531)

[Precept Austin](https://www.preceptaustin.org/acts-17-commentary#a)