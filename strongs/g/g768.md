# Ἀσήρ

[asēr](https://www.blueletterbible.org/lexicon/g768)

Definition: Aser (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0768)

[Study Light](https://www.studylight.org/lexicons/greek/768.html)

[Bible Hub](https://biblehub.com/str/greek/768.htm)

[LSJ](https://lsj.gr/wiki/Ἀσήρ)

[NET Bible](http://classic.net.bible.org/strong.php?id=768)

[Bible Bento](https://biblebento.com/dictionary/G768.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/768/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/768.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g768)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/aser.html)