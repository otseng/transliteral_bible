# Εὔβουλος

[euboulos](https://www.blueletterbible.org/lexicon/g2103)

Definition: Eubulus (1x), "prudent", a Christian at Rome

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2103)

[Study Light](https://www.studylight.org/lexicons/greek/2103.html)

[Bible Hub](https://biblehub.com/str/greek/2103.htm)

[LSJ](https://lsj.gr/wiki/Εὔβουλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2103)

[Bible Bento](https://biblebento.com/dictionary/G2103.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2103/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2103.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2103)

[Wikipedia](https://en.wikipedia.org/wiki/Eubuleus)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eubulus.html)

[Video Bible](https://www.videobible.com/bible-dictionary/eubulus)