# ἀμετακίνητος

[ametakinētos](https://www.blueletterbible.org/lexicon/g277)

Definition: unmoveable (1x), firmly persistent, secure

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0277)

[Study Light](https://www.studylight.org/lexicons/greek/277.html)

[Bible Hub](https://biblehub.com/str/greek/277.htm)

[LSJ](https://lsj.gr/wiki/ἀμετακίνητος)

[NET Bible](http://classic.net.bible.org/strong.php?id=277)

[Bible Bento](https://biblebento.com/dictionary/G0277.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0277/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0277.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g277)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CE%BC%CE%B5%CF%84%CE%B1%CE%BA%CE%AF%CE%BD%CE%B7%CF%84%CE%BF%CF%82)
