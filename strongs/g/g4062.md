# περιτρέπω

[peritrepō](https://www.blueletterbible.org/lexicon/g4062)

Definition: make mad (with G3130) (with G1519) (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4062)

[Study Light](https://www.studylight.org/lexicons/greek/4062.html)

[Bible Hub](https://biblehub.com/str/greek/4062.htm)

[LSJ](https://lsj.gr/wiki/περιτρέπω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4062)

[Bible Bento](https://biblebento.com/dictionary/G4062.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4062/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4062.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4062)

