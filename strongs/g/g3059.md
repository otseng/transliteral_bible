# λοιδορία

[loidoria](https://www.blueletterbible.org/lexicon/g3059)

Definition: railing (2x), to speak reproachfully (with G5484) (1x).

Part of speech: feminine noun

Occurs 3 times in 2 verses

Hebrew: [rîḇ](../h/h7379.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3059)

[Study Light](https://www.studylight.org/lexicons/greek/3059.html)

[Bible Hub](https://biblehub.com/str/greek/3059.htm)

[LSJ](https://lsj.gr/wiki/λοιδορία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3059)

[Bible Bento](https://biblebento.com/dictionary/G3059.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3059/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3059.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3059)

