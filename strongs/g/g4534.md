# Σαλμώνη

[Salmōnē](https://www.blueletterbible.org/lexicon/g4534)

Definition: Salmone (1x), "clothed", a bold promontory on the east point of the island of Crete

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4534)

[Study Light](https://www.studylight.org/lexicons/greek/4534.html)

[Bible Hub](https://biblehub.com/str/greek/4534.htm)

[LSJ](https://lsj.gr/wiki/Σαλμώνη)

[NET Bible](http://classic.net.bible.org/strong.php?id=4534)

[Bible Bento](https://biblebento.com/dictionary/G4534.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4534/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4534.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4534)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/salmone.html)

[Video Bible](https://www.videobible.com/bible-dictionary/salmone)