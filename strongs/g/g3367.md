# μηδείς

[mēdeis](https://www.blueletterbible.org/lexicon/g3367)

Definition: no man (32x), nothing (27x), no (16x), none (6x), not (1x), anything (2x), miscellaneous (7x), nobody, no one, nothing

Part of speech: adjective

Occurs 93 times in 88 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3367)

[Study Light](https://www.studylight.org/lexicons/greek/3367.html)

[Bible Hub](https://biblehub.com/str/greek/3367.htm)

[LSJ](https://lsj.gr/wiki/μηδείς)

[NET Bible](http://classic.net.bible.org/strong.php?id=3367)

[Bible Bento](https://biblebento.com/dictionary/G3367.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3367/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3367.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3367)