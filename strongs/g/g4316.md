# προσαγορεύω

[prosagoreuō](https://www.blueletterbible.org/lexicon/g4316)

Definition: call (1x), to speak to, to address, accost, salute

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4316)

[Study Light](https://www.studylight.org/lexicons/greek/4316.html)

[Bible Hub](https://biblehub.com/str/greek/4316.htm)

[LSJ](https://lsj.gr/wiki/προσαγορεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4316)

[Bible Bento](https://biblebento.com/dictionary/G4316.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4316/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4316.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4316)

[Sermon Index](https://www.sermonindex.net/modules/articles/index.php?view=article&aid=33841)
