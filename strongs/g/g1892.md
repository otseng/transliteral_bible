# ἐπεγείρω

[epegeirō](https://www.blueletterbible.org/lexicon/g1892)

Definition: raise (1x), stir up (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/1892.html)

[Bible Hub](https://biblehub.com/str/greek/1892.htm)

[LSJ](https://lsj.gr/wiki/ἐπεγείρω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1892)

[Bible Bento](https://biblebento.com/dictionary/G1892.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1892/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1892.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1892)

