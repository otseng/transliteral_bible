# ἀντιπίπτω

[antipiptō](https://www.blueletterbible.org/lexicon/g496)

Definition: resist (1x)

Part of speech: verb

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0496)

[Study Light](https://www.studylight.org/lexicons/greek/496.html)

[Bible Hub](https://biblehub.com/str/greek/496.htm)

[LSJ](https://lsj.gr/wiki/ἀντιπίπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=496)

[Bible Bento](https://biblebento.com/dictionary/G0496.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0496/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0496.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g496)
