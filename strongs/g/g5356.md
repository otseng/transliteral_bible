# φθορά

[phthora](https://www.blueletterbible.org/lexicon/g5356)

Definition: corruption (7x), to perish (with G1519) (1x), destroy (1x), destruction, decay, ruin

Part of speech: feminine noun

Occurs 9 times in 8 verses

Synonyms: [destroy](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Destroy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5356)

[Study Light](https://www.studylight.org/lexicons/greek/5356.html)

[Bible Hub](https://biblehub.com/str/greek/5356.htm)

[LSJ](https://lsj.gr/wiki/φθορά)

[NET Bible](http://classic.net.bible.org/strong.php?id=5356)

[Bible Bento](https://biblebento.com/dictionary/G5356.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5356/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5356.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5356)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%86%CE%B8%CE%BF%CF%81%CE%AC)

[CARM](https://carm.org/word-study-destruction-phthora)
