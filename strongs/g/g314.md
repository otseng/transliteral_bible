# ἀναγινώσκω

[anaginōskō](https://www.blueletterbible.org/lexicon/g314)

Definition: read (33x), acknowledge, to recognize, to know accurately

Part of speech: verb

Occurs 41 times in 30 verses

Hebrew: [qara'](../h/h7121.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0314)

[Study Light](https://www.studylight.org/lexicons/greek/314.html)

[Bible Hub](https://biblehub.com/str/greek/314.htm)

[LSJ](https://lsj.gr/wiki/ἀναγινώσκω)

[NET Bible](http://classic.net.bible.org/strong.php?id=314)

[Bible Bento](https://biblebento.com/dictionary/G0314.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0314/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0314.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g314)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CE%BD%CE%B1%CE%B3%CE%B9%CE%BD%CF%8E%CF%83%CE%BA%CF%89)