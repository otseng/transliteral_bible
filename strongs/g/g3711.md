# ὀργίλος

[orgilos](https://www.blueletterbible.org/lexicon/g3711)

Definition: soon angry (1x), prone to anger

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3711)

[Study Light](https://www.studylight.org/lexicons/greek/3711.html)

[Bible Hub](https://biblehub.com/str/greek/3711.htm)

[LSJ](https://lsj.gr/wiki/ὀργίλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3711)

[Bible Bento](https://biblebento.com/dictionary/G3711.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3711/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3711.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3711)
