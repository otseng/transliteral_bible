# ἐπακροάομαι

[epakroaomai](https://www.blueletterbible.org/lexicon/g1874)

Definition: hear (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Synonyms: [hear](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Hear)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1874)

[Study Light](https://www.studylight.org/lexicons/greek/1874.html)

[Bible Hub](https://biblehub.com/str/greek/1874.htm)

[LSJ](https://lsj.gr/wiki/ἐπακροάομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1874)

[Bible Bento](https://biblebento.com/dictionary/G1874.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1874/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1874.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1874)
