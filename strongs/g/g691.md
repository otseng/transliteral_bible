# ἀργέω

[argeō](https://www.blueletterbible.org/lexicon/g691)

Definition: linger (1x), delay

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0691)

[Study Light](https://www.studylight.org/lexicons/greek/691.html)

[Bible Hub](https://biblehub.com/str/greek/691.htm)

[LSJ](https://lsj.gr/wiki/ἀργέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=691)

[Bible Bento](https://biblebento.com/dictionary/G691.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/691/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/691.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g691)

