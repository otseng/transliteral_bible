# θεριστής

[theristēs](https://www.blueletterbible.org/lexicon/g2327)

Definition: reaper (2x), a harvester

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2327)

[Study Light](https://www.studylight.org/lexicons/greek/2327.html)

[Bible Hub](https://biblehub.com/str/greek/2327.htm)

[LSJ](https://lsj.gr/wiki/θεριστής)

[NET Bible](http://classic.net.bible.org/strong.php?id=2327)

[Bible Bento](https://biblebento.com/dictionary/G2327.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2327/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2327.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2327)
