# μενοῦν

[menoun](https://www.blueletterbible.org/lexicon/g3304)

Definition: yea rather (1x), nay but (1x), yea verily (1x), yea doubtless (1x), nay surely, nay rather, yea doubtless

Part of speech: particle

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3304)

[Study Light](https://www.studylight.org/lexicons/greek/3304.html)

[Bible Hub](https://biblehub.com/str/greek/3304.htm)

[LSJ](https://lsj.gr/wiki/μενοῦν)

[NET Bible](http://classic.net.bible.org/strong.php?id=3304)

[Bible Bento](https://biblebento.com/dictionary/G3304.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3304/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3304.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3304)
