# πλεῖστος

[pleistos](https://www.blueletterbible.org/lexicon/g4118)

Definition: most (2x), very great (1x), the largest number or very large, to the greatest extent

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4118)

[Study Light](https://www.studylight.org/lexicons/greek/4118.html)

[Bible Hub](https://biblehub.com/str/greek/4118.htm)

[LSJ](https://lsj.gr/wiki/πλεῖστος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4118)

[Bible Bento](https://biblebento.com/dictionary/G4118.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4118/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4118.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4118)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%BB%CE%B5%E1%BF%96%CF%83%CF%84%CE%BF%CF%82)
