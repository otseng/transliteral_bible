# συμμέτοχος

[symmetochos](https://www.blueletterbible.org/lexicon/g4830)

Definition: partaker (2x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4830)

[Study Light](https://www.studylight.org/lexicons/greek/4830.html)

[Bible Hub](https://biblehub.com/str/greek/4830.htm)

[LSJ](https://lsj.gr/wiki/συμμέτοχος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4830)

[Bible Bento](https://biblebento.com/dictionary/G4830.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4830/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4830.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4830)
