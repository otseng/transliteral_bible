# διακατελέγχομαι

[diakatelegchomai](https://www.blueletterbible.org/lexicon/g1246)

Definition: convince (1x), to confute with rivalry and effort or in a contest, to prove downright

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1246)

[Study Light](https://www.studylight.org/lexicons/greek/1246.html)

[Bible Hub](https://biblehub.com/str/greek/1246.htm)

[LSJ](https://lsj.gr/wiki/διακατελέγχομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1246)

[Bible Bento](https://biblebento.com/dictionary/G1246.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1246/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1246.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1246)
