# συνεγείρω

[synegeirō](https://www.blueletterbible.org/lexicon/g4891)

Definition: risen with (2x), raised us up together (1x), to cause to raise together, to rouse (from death) in company with

Part of speech: verb

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4891)

[Study Light](https://www.studylight.org/lexicons/greek/4891.html)

[Bible Hub](https://biblehub.com/str/greek/4891.htm)

[LSJ](https://lsj.gr/wiki/συνεγείρω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4891)

[Bible Bento](https://biblebento.com/dictionary/G4891.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4891/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4891.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4891)