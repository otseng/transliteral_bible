# ἔχθρα

[echthra](https://www.blueletterbible.org/lexicon/g2189)

Definition: enmity (5x), hatred (1x), hostility

Part of speech: feminine noun

Occurs 6 times in 6 verses

Hebrew: [śin'â](../h/h8135.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2189)

[Study Light](https://www.studylight.org/lexicons/greek/2189.html)

[Bible Hub](https://biblehub.com/str/greek/2189.htm)

[LSJ](https://lsj.gr/wiki/ἔχθρα)

[NET Bible](http://classic.net.bible.org/strong.php?id=2189)

[Bible Bento](https://biblebento.com/dictionary/G2189.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2189/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2189.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2189)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%94%CF%87%CE%B8%CF%81%CE%B1)