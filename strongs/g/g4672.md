# Σολομών

[Solomōn](https://www.blueletterbible.org/lexicon/g4672)

Definition: Solomon (12x), "peaceful"

Part of speech: proper masculine noun

Occurs 12 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4672)

[Study Light](https://www.studylight.org/lexicons/greek/4672.html)

[Bible Hub](https://biblehub.com/str/greek/4672.htm)

[LSJ](https://lsj.gr/wiki/Σολομών)

[NET Bible](http://classic.net.bible.org/strong.php?id=4672)

[Bible Bento](https://biblebento.com/dictionary/G4672.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4672/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4672.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4672)

[Wikipedia](https://en.wikipedia.org/wiki/Solomon)

[Britannica](https://www.britannica.com/biography/Solomon)

[Jewish Virtual Library](https://www.jewishvirtuallibrary.org/king-solomon)

[My Jewish Learning](https://www.myjewishlearning.com/article/king-solomon-his-kingdom/)

[Bible Odyssey](https://metro.bibleodyssey.com/articles/solomon/)

[New Advent](https://www.newadvent.org/cathen/14135b.htm)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/solomon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/solomon)