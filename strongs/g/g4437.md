# πυκνός

[pyknos](https://www.blueletterbible.org/lexicon/g4437)

Definition: often (2x), oftener (1x), frequent, thick, dense, compact, crafty, cunning

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4437)

[Study Light](https://www.studylight.org/lexicons/greek/4437.html)

[Bible Hub](https://biblehub.com/str/greek/4437.htm)

[LSJ](https://lsj.gr/wiki/πυκνός)

[NET Bible](http://classic.net.bible.org/strong.php?id=4437)

[Bible Bento](https://biblebento.com/dictionary/G4437.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4437/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4437.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4437)

[Wikipedia](https://en.wiktionary.org/wiki/%CF%80%CF%85%CE%BA%CE%BD%CF%8C%CF%82)
