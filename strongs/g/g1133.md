# γυναικάριον

[gynaikarion](https://www.blueletterbible.org/lexicon/g1133)

Definition: silly woman (1x), foolish woman

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1133)

[Study Light](https://www.studylight.org/lexicons/greek/1133.html)

[Bible Hub](https://biblehub.com/str/greek/1133.htm)

[LSJ](https://lsj.gr/wiki/γυναικάριον)

[NET Bible](http://classic.net.bible.org/strong.php?id=1133)

[Bible Bento](https://biblebento.com/dictionary/G1133.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1133/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1133.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1133)
