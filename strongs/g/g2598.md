# καταβάλλω

[kataballō](https://www.blueletterbible.org/lexicon/g2598)

Definition: cast down (2x), lay (1x).

Part of speech: verb

Occurs 3 times in 3 verses

Hebrew: [nāṯaṣ](../h/h5422.md), [pāraṣ](../h/h6555.md), [harac](../h/h2040.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2598)

[Study Light](https://www.studylight.org/lexicons/greek/2598.html)

[Bible Hub](https://biblehub.com/str/greek/2598.htm)

[LSJ](https://lsj.gr/wiki/καταβάλλω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2598)

[Bible Bento](https://biblebento.com/dictionary/G2598.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2598/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2598.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2598)
