# ἀλάλητος

[alalētos](https://www.blueletterbible.org/lexicon/g215)

Definition: which cannot be uttered (1x), unspeakable

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0215)

[Study Light](https://www.studylight.org/lexicons/greek/215.html)

[Bible Hub](https://biblehub.com/str/greek/215.htm)

[LSJ](https://lsj.gr/wiki/ἀλάλητος)

[NET Bible](http://classic.net.bible.org/strong.php?id=215)

[Bible Bento](https://biblebento.com/dictionary/G0215.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0215/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0215.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g215)
