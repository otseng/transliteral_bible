# ὅστις

[hostis](https://www.blueletterbible.org/lexicon/g3748)

Definition: which (82x), who (30x), whosoever (12x), that (8x), whatsoever (with G302) (4x), whosoever (with G302) (3x), whatsoever (with G3956) (with G302) (2x), miscellaneous (13x).

Part of speech: pronoun

Occurs 153 times in 147 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3748)

[Study Light](https://www.studylight.org/lexicons/greek/3748.html)

[Bible Hub](https://biblehub.com/str/greek/3748.htm)

[LSJ](https://lsj.gr/wiki/ὅστις)

[NET Bible](http://classic.net.bible.org/strong.php?id=3748)

[Bible Bento](https://biblebento.com/dictionary/G3748.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3748/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3748.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3748)
