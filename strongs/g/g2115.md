# εὔθυμος

[euthymos](https://www.blueletterbible.org/lexicon/g2115)

Definition: of good cheer (1x), more cheerfully (1x), well disposed, kind

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2115)

[Study Light](https://www.studylight.org/lexicons/greek/2115.html)

[Bible Hub](https://biblehub.com/str/greek/2115.htm)

[LSJ](https://lsj.gr/wiki/εὔθυμος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2115)

[Bible Bento](https://biblebento.com/dictionary/G2115.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2115/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2115.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2115)
