# φραγελλόω

[phragelloō](https://www.blueletterbible.org/lexicon/g5417)

Definition: scourge (2x)

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5417)

[Study Light](https://www.studylight.org/lexicons/greek/5417.html)

[Bible Hub](https://biblehub.com/str/greek/5417.htm)

[LSJ](https://lsj.gr/wiki/φραγελλόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5417)

[Bible Bento](https://biblebento.com/dictionary/G5417.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5417/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5417.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5417)