# διοδεύω

[diodeuō](https://www.blueletterbible.org/lexicon/g1353)

Definition: go throughout (1x), pass through (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1353)

[Study Light](https://www.studylight.org/lexicons/greek/1353.html)

[Bible Hub](https://biblehub.com/str/greek/1353.htm)

[LSJ](https://lsj.gr/wiki/διοδεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1353)

[Bible Bento](https://biblebento.com/dictionary/G1353.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1353/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1353.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1353)

[Precept Austin](https://www.preceptaustin.org/luke-8-commentary#going)
