# χρηστεύομαι

[chrēsteuomai](https://www.blueletterbible.org/lexicon/g5541)

Definition: be kind (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Synonyms: [kind](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Kind)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5541)

[Study Light](https://www.studylight.org/lexicons/greek/5541.html)

[Bible Hub](https://biblehub.com/str/greek/5541.htm)

[LSJ](https://lsj.gr/wiki/χρηστεύομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=5541)

[Bible Bento](https://biblebento.com/dictionary/G5541.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5541/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5541.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5541)
