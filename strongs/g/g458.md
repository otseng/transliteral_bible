# ἀνομία

[anomia](https://www.blueletterbible.org/lexicon/g458)

Definition: iniquity (12x), unrighteousness (1x), transgress the law (with G4160) (1x), transgression of the law (1x), wickedness, lawlessness, "without law"

Part of speech: feminine noun

Occurs 16 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0458)

[Study Light](https://www.studylight.org/lexicons/greek/458.html)

[Bible Hub](https://biblehub.com/str/greek/458.htm)

[LSJ](https://lsj.gr/wiki/ἀνομία)

[NET Bible](http://classic.net.bible.org/strong.php?id=458)

[Bible Bento](https://biblebento.com/dictionary/G0458.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0458/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0458.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g458)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34351)

[Precept Austin](https://www.preceptaustin.org/romans_618-20#lawlessness)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/0458-anomia-lawlessness.htm)
