# παλαιός

[palaios](https://www.blueletterbible.org/lexicon/g3820)

Definition: old (18x), old wine (1x), ancient, antiquated, worn out

Part of speech: adjective

Occurs 19 times in 15 verses

Derived words: paleontologist, paleolithic, [paleo-](https://www.thefreedictionary.com/words-that-start-with-paleo)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3820)

[Study Light](https://www.studylight.org/lexicons/greek/3820.html)

[Bible Hub](https://biblehub.com/str/greek/3820.htm)

[LSJ](https://lsj.gr/wiki/παλαιός)

[NET Bible](http://classic.net.bible.org/strong.php?id=3820)

[Bible Bento](https://biblebento.com/dictionary/G3820.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3820/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3820.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3820)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%B1%CE%BB%CE%B1%CE%B9%CF%8C%CF%82)