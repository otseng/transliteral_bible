# διακρίνω

[diakrinō](https://www.blueletterbible.org/lexicon/g1252)

Definition: doubt (5x), judge (3x), discern (2x), contend (2x), waver (2x), miscellaneous (5x), waver, separate, contend, detect, vacillating, stagger

Part of speech: verb

Occurs 24 times in 18 verses

Synonyms: [doubt](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Doubt)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1252)

[Study Light](https://www.studylight.org/lexicons/greek/1252.html)

[Bible Hub](https://biblehub.com/str/greek/1252.htm)

[LSJ](https://lsj.gr/wiki/διακρίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1252)

[Bible Bento](https://biblebento.com/dictionary/G1252.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1252/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1252.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1252)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B4%CE%B9%CE%B1%CE%BA%CF%81%CE%AF%CE%BD%CF%89)

[Precept Austin](https://www.preceptaustin.org/james_15-6#d)

[Temple Swallow](http://templeswallow.blogspot.com/2011/03/whats-in-word-diakrino-doubt.html)
