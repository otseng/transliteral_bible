# σπόγγος

[spoggos](https://www.blueletterbible.org/lexicon/g4699)

Definition: spunge (3x), sponge

Part of speech: masculine noun

Occurs 3 times in 3 verses

Usage: [Spoggos](https://www.spoggos.com/)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4699)

[Study Light](https://www.studylight.org/lexicons/greek/4699.html)

[Bible Hub](https://biblehub.com/str/greek/4699.htm)

[LSJ](https://lsj.gr/wiki/σπόγγος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4699)

[Bible Bento](https://biblebento.com/dictionary/G4699.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4699/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4699.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4699)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CF%80%CF%8C%CE%B3%CE%B3%CE%BF%CF%82)