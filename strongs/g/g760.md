# Ἀσά

[Asa](https://www.blueletterbible.org/lexicon/g760)

Definition: Asa (2x), "physician, or cure"

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0760)

[Study Light](https://www.studylight.org/lexicons/greek/760.html)

[Bible Hub](https://biblehub.com/str/greek/760.htm)

[LSJ](https://lsj.gr/wiki/Ἀσά)

[NET Bible](http://classic.net.bible.org/strong.php?id=760)

[Bible Bento](https://biblebento.com/dictionary/G0760.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0760/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0760.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g760)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/asa.html)

[Video Bible](https://www.videobible.com/bible-dictionary/asa)