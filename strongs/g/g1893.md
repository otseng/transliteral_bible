# ἐπεί

[epei](https://www.blueletterbible.org/lexicon/g1893)

Definition: because (7x), otherwise (4x), for then (3x), else (3x), seeing (3x), forasmuch as (2x), for that (1x), miscellaneous (4x).

Part of speech: conjunction

Occurs 27 times in 26 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1893)

[Study Light](https://www.studylight.org/lexicons/greek/1893.html)

[Bible Hub](https://biblehub.com/str/greek/1893.htm)

[LSJ](https://lsj.gr/wiki/ἐπεί)

[NET Bible](http://classic.net.bible.org/strong.php?id=1893)

[Bible Bento](https://biblebento.com/dictionary/G1893.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1893/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1893.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1893)

