# ἀπαρνέομαι

[aparneomai](https://www.blueletterbible.org/lexicon/g533)

Definition: deny (13x), deny utterly, forget one's self, disown, completely reject

Part of speech: verb

Occurs 14 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0533)

[Study Light](https://www.studylight.org/lexicons/greek/533.html)

[Bible Hub](https://biblehub.com/str/greek/533.htm)

[LSJ](https://lsj.gr/wiki/ἀπαρνέομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=533)

[Bible Bento](https://biblebento.com/dictionary/G0533.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0533/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0533.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g533)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CF%80%CE%B1%CF%81%CE%BD%CE%AD%CE%BF%CE%BC%CE%B1%CE%B9)

[Sermon Index](https://www.preceptaustin.org/luke-22-commentary#deny)