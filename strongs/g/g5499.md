# χειροποίητος

[cheiropoiētos](https://www.blueletterbible.org/lexicon/g5499)

Definition: made with hands (5x), made by hands (1x), manufactured, of human construction

Part of speech: adjective

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5499)

[Study Light](https://www.studylight.org/lexicons/greek/5499.html)

[Bible Hub](https://biblehub.com/str/greek/5499.htm)

[LSJ](https://lsj.gr/wiki/χειροποίητος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5499)

[Bible Bento](https://biblebento.com/dictionary/G5499.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5499/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5499.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5499)
