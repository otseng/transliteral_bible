# γερουσία

[gerousia](https://www.blueletterbible.org/lexicon/g1087)

Definition: senate (1x), council of elders, Jewish Sanhedrin

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1087)

[Study Light](https://www.studylight.org/lexicons/greek/1087.html)

[Bible Hub](https://biblehub.com/str/greek/1087.htm)

[LSJ](https://lsj.gr/wiki/γερουσία)

[NET Bible](http://classic.net.bible.org/strong.php?id=1087)

[Bible Bento](https://biblebento.com/dictionary/G1087.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1087/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1087.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1087)
