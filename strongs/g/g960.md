# Βέροια

[Beroia](https://www.blueletterbible.org/lexicon/g960)

Definition: Berea (2x), "well watered"

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0960)

[Study Light](https://www.studylight.org/lexicons/greek/960.html)

[Bible Hub](https://biblehub.com/str/greek/960.htm)

[LSJ](https://lsj.gr/wiki/Βέροια)

[NET Bible](http://classic.net.bible.org/strong.php?id=960)

[Bible Bento](https://biblebento.com/dictionary/G0960.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0960/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0960.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g960)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/berea.html)

[Video Bible](https://www.videobible.com/bible-dictionary/berea)