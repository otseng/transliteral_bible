# ῥυτίς

[rhytis](https://www.blueletterbible.org/lexicon/g4512)

Definition: wrinkle (1x)

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4512)

[Study Light](https://www.studylight.org/lexicons/greek/4512.html)

[Bible Hub](https://biblehub.com/str/greek/4512.htm)

[LSJ](https://lsj.gr/wiki/ῥυτίς)

[NET Bible](http://classic.net.bible.org/strong.php?id=4512)

[Bible Bento](https://biblebento.com/dictionary/G4512.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4512/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4512.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4512)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BF%A5%CF%85%CF%84%CE%AF%CF%82)
