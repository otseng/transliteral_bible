# κατοικητήριον

[katoikētērion](https://www.blueletterbible.org/lexicon/g2732)

Definition: habitation (2x).

Part of speech: neuter noun

Occurs 2 times in 2 verses

Hebrew: [māʿôn](../h/h4583.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2732)

[Study Light](https://www.studylight.org/lexicons/greek/2732.html)

[Bible Hub](https://biblehub.com/str/greek/2732.htm)

[LSJ](https://lsj.gr/wiki/κατοικητήριον)

[NET Bible](http://classic.net.bible.org/strong.php?id=2732)

[Bible Bento](https://biblebento.com/dictionary/G2732.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2732/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2732.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2732)
