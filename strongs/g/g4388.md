# προτίθημι

[protithēmi](https://www.blueletterbible.org/lexicon/g4388)

Definition: purpose (2x), set forth (1x).

Part of speech: verb

Occurs 3 times in 3 verses

Hebrew: [shiyth](../h/h7896.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4388)

[Study Light](https://www.studylight.org/lexicons/greek/4388.html)

[Bible Hub](https://biblehub.com/str/greek/4388.htm)

[LSJ](https://lsj.gr/wiki/προτίθημι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4388)

[Bible Bento](https://biblebento.com/dictionary/G4388.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4388/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4388.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4388)

