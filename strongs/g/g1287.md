# διασκορπίζω

[diaskorpizō](https://www.blueletterbible.org/lexicon/g1287)

Definition: straw (2x), scatter abroad (2x), scatter (2x), waste (2x), disperse (1x), disperse, squander

Part of speech: verb

Occurs 11 times in 9 verses

Greek: [diaspora](../g/g1290.md)

Hebrew: [pāraḏ](../h/h6504.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1287)

[Study Light](https://www.studylight.org/lexicons/greek/1287.html)

[Bible Hub](https://biblehub.com/str/greek/1287.htm)

[LSJ](https://lsj.gr/wiki/διασκορπίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1287)

[Bible Bento](https://biblebento.com/dictionary/G1287.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1287/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1287.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1287)

[Precept Austin](https://www.preceptaustin.org/luke-15-commentary#squandered)