# εὐεργεσία

[euergesia](https://www.blueletterbible.org/lexicon/g2108)

Definition: good deed done (1x), benefit (1x)

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2108)

[Study Light](https://www.studylight.org/lexicons/greek/2108.html)

[Bible Hub](https://biblehub.com/str/greek/2108.htm)

[LSJ](https://lsj.gr/wiki/εὐεργεσία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2108)

[Bible Bento](https://biblebento.com/dictionary/G2108.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2108/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2108.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2108)
