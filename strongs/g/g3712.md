# ὀργυιά

[orguia](https://www.blueletterbible.org/lexicon/g3712)

Definition: fathom (2x).

Part of speech: feminine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3712)

[Study Light](https://www.studylight.org/lexicons/greek/3712.html)

[Bible Hub](https://biblehub.com/str/greek/3712.htm)

[LSJ](https://lsj.gr/wiki/ὀργυιά)

[NET Bible](http://classic.net.bible.org/strong.php?id=3712)

[Bible Bento](https://biblebento.com/dictionary/G3712.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3712/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3712.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3712)

