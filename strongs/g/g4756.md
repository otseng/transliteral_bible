# στρατιά

[stratia](https://www.blueletterbible.org/lexicon/g4756)

Definition: host (2x), band of soldiers, army, company, band

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4756)

[Study Light](https://www.studylight.org/lexicons/greek/4756.html)

[Bible Hub](https://biblehub.com/str/greek/4756.htm)

[LSJ](https://lsj.gr/wiki/στρατιά)

[NET Bible](http://classic.net.bible.org/strong.php?id=4756)

[Bible Bento](https://biblebento.com/dictionary/G4756.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4756/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4756.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4756)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CF%84%CF%81%CE%B1%CF%84%CE%B9%CE%AC)