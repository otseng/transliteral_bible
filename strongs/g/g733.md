# ἀρσενοκοίτης

[arsenokoitēs](https://www.blueletterbible.org/lexicon/g733)

Definition: abuser of (one's) self with mankind (1x), defile (one's) self with mankind (1x), one who lies with a male as with a female, sodomite, homosexual, "a male who lies with males"

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0733)

[Study Light](https://www.studylight.org/lexicons/greek/733.html)

[Bible Hub](https://biblehub.com/str/greek/733.htm)

[LSJ](https://lsj.gr/wiki/ἀρσενοκοίτης)

[NET Bible](http://classic.net.bible.org/strong.php?id=733)

[Bible Bento](https://biblebento.com/dictionary/G0733.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0733/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0733.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g733)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CF%81%CF%83%CE%B5%CE%BD%CE%BF%CE%BA%CE%BF%CE%AF%CF%84%CE%B7%CF%82)
