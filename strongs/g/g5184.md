# Τύρος

[Tyros](https://www.blueletterbible.org/lexicon/g5184)

Definition: Tyre (11x),  "a rock", a Phoenician city on the Mediterranean, very ancient, large, splendid, flourishing in commerce, and powerful by land and sea

Part of speech: feminine noun

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5184)

[Study Light](https://www.studylight.org/lexicons/greek/5184.html)

[Bible Hub](https://biblehub.com/str/greek/5184.htm)

[LSJ](https://lsj.gr/wiki/Τύρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5184)

[Bible Bento](https://biblebento.com/dictionary/G5184.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5184/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5184.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5184)

[Wikipedia](https://en.wikipedia.org/wiki/Tyre,_Lebanon)