# Ἑλλάς

[Hellas](https://www.blueletterbible.org/lexicon/g1671)

Definition: Greece (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1671)

[Study Light](https://www.studylight.org/lexicons/greek/1671.html)

[Bible Hub](https://biblehub.com/str/greek/1671.htm)

[LSJ](https://lsj.gr/wiki/Ἑλλάς)

[NET Bible](http://classic.net.bible.org/strong.php?id=1671)

[Bible Bento](https://biblebento.com/dictionary/G1671.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1671/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1671.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1671)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%99%CE%BB%CE%BB%CE%AC%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Name_of_Greece)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/greece.html)

[Video Bible](https://www.videobible.com/bible-dictionary/greece)