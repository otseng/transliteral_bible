# διαμάχομαι

[diamachomai](https://www.blueletterbible.org/lexicon/g1264)

Definition: strive (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1264)

[Study Light](https://www.studylight.org/lexicons/greek/1264.html)

[Bible Hub](https://biblehub.com/str/greek/1264.htm)

[LSJ](https://lsj.gr/wiki/διαμάχομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1264)

[Bible Bento](https://biblebento.com/dictionary/G1264.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1264/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1264.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1264)

