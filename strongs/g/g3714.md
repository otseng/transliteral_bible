# ὀρεινός

[oreinos](https://www.blueletterbible.org/lexicon/g3714)

Definition: hill (2x), hill country, mountainous area

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3714)

[Study Light](https://www.studylight.org/lexicons/greek/3714.html)

[Bible Hub](https://biblehub.com/str/greek/3714.htm)

[LSJ](https://lsj.gr/wiki/ὀρεινός)

[NET Bible](http://classic.net.bible.org/strong.php?id=3714)

[Bible Bento](https://biblebento.com/dictionary/G3714.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3714/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3714.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3714)