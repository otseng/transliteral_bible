# ἰσχυρός

[ischyros](https://www.blueletterbible.org/lexicon/g2478)

Definition: mighty (10x), strong (9x), strong man (5x), boisterous (1x), powerful (1x), valiant (1x)

Part of speech: adjective

Occurs 27 times in 25 verses

Hebrew: [kāḇēḏ](../h/h3515.md), ['amats](../h/h553.md), ['atsuwm](../h/h6099.md)

Synonyms: [mighty](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Mighty)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2478)

[Study Light](https://www.studylight.org/lexicons/greek/2478.html)

[Bible Hub](https://biblehub.com/str/greek/2478.htm)

[LSJ](https://lsj.gr/wiki/ἰσχυρός)

[NET Bible](http://classic.net.bible.org/strong.php?id=2478)

[Bible Bento](https://biblebento.com/dictionary/G2478.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2478/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2478.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2478)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%B0%CF%83%CF%87%CF%85%CF%81%CF%8C%CF%82)