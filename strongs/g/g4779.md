# συγκαλέω

[sygkaleō](https://www.blueletterbible.org/lexicon/g4779)

Definition: call together (8x), assemble, convoke

Part of speech: verb

Occurs 12 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4779)

[Study Light](https://www.studylight.org/lexicons/greek/4779.html)

[Bible Hub](https://biblehub.com/str/greek/4779.htm)

[LSJ](https://lsj.gr/wiki/συγκαλέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4779)

[Bible Bento](https://biblebento.com/dictionary/G4779.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4779/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4779.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4779)
