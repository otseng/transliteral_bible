# ἄλαλος

[alalos](https://www.blueletterbible.org/lexicon/g216)

Definition: dumb (3x), mute, speechless, "no speech"

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0216)

[Study Light](https://www.studylight.org/lexicons/greek/216.html)

[Bible Hub](https://biblehub.com/str/greek/216.htm)

[LSJ](https://lsj.gr/wiki/ἄλαλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=216)

[Bible Bento](https://biblebento.com/dictionary/G0216.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0216/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0216.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g216)