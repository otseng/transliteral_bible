# αἴσθησις

[aisthēsis](https://www.blueletterbible.org/lexicon/g144)

Definition: judgment (1x), cognition, discernment, perception

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0144)

[Study Light](https://www.studylight.org/lexicons/greek/144.html)

[Bible Hub](https://biblehub.com/str/greek/144.htm)

[LSJ](https://lsj.gr/wiki/αἴσθησις)

[NET Bible](http://classic.net.bible.org/strong.php?id=144)

[Bible Bento](https://biblebento.com/dictionary/G0144.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/144/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/144.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g144)
