# τραχηλίζω

[trachēlizō](https://www.blueletterbible.org/lexicon/g5136)

Definition: open (1x), to lay bare, uncover, expose

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5136)

[Study Light](https://www.studylight.org/lexicons/greek/5136.html)

[Bible Hub](https://biblehub.com/str/greek/5136.htm)

[LSJ](https://lsj.gr/wiki/τραχηλίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5136)

[Bible Bento](https://biblebento.com/dictionary/G5136.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5136/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5136.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5136)
