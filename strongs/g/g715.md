# ἄρκτος

[arktos](https://www.blueletterbible.org/lexicon/g715)

Definition: bear (1x).

Part of speech: masculine/feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0715)

[Study Light](https://www.studylight.org/lexicons/greek/715.html)

[Bible Hub](https://biblehub.com/str/greek/715.htm)

[LSJ](https://lsj.gr/wiki/ἄρκτος)

[NET Bible](http://classic.net.bible.org/strong.php?id=715)

[Bible Bento](https://biblebento.com/dictionary/G715.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/715/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/715.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g715)

