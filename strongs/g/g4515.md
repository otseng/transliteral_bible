# Ῥωμαϊστί

[Rhōmaïsti](https://www.blueletterbible.org/lexicon/g4515)

Definition: Latin (1x), Roman language

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4515)

[Study Light](https://www.studylight.org/lexicons/greek/4515.html)

[Bible Hub](https://biblehub.com/str/greek/4515.htm)

[LSJ](https://lsj.gr/wiki/Ῥωμαϊστί)

[NET Bible](http://classic.net.bible.org/strong.php?id=4515)

[Bible Bento](https://biblebento.com/dictionary/G4515.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4515/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4515.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4515)