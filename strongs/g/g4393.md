# προφέρω

[propherō](https://www.blueletterbible.org/lexicon/g4393)

Definition: bring forth (2x).

Part of speech: verb

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4393)

[Study Light](https://www.studylight.org/lexicons/greek/4393.html)

[Bible Hub](https://biblehub.com/str/greek/4393.htm)

[LSJ](https://lsj.gr/wiki/προφέρω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4393)

[Bible Bento](https://biblebento.com/dictionary/G4393.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4393/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4393.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4393)

