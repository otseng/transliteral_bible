# θεμελιόω

[themelioō](https://www.blueletterbible.org/lexicon/g2311)

Definition: found (2x), ground (2x), lay the foundation (1x), settle (1x)., to make stable, establish, ground securely

Part of speech: verb

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2311)

[Study Light](https://www.studylight.org/lexicons/greek/2311.html)

[Bible Hub](https://biblehub.com/str/greek/2311.htm)

[LSJ](https://lsj.gr/wiki/θεμελιόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2311)

[Bible Bento](https://biblebento.com/dictionary/G2311.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2311/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2311.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2311)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33990)

[Precept Austin](https://www.preceptaustin.org/1_peter_510-14#establish)