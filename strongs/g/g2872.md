# κοπιάω

[kopiaō](https://www.blueletterbible.org/lexicon/g2872)

Definition: labour (16x), bestow labour (3x), toil (3x), be wearied (1x), weary, toil, exhausted

Part of speech: verb

Occurs 28 times in 21 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2872)

[Study Light](https://www.studylight.org/lexicons/greek/2872.html)

[Bible Hub](https://biblehub.com/str/greek/2872.htm)

[LSJ](https://lsj.gr/wiki/κοπιάω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2872)

[Bible Bento](https://biblebento.com/dictionary/G2872.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2872/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2872.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2872)