# εἰστρέχω

[eistrechō](https://www.blueletterbible.org/lexicon/g1532)

Definition: run in (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1532)

[Study Light](https://www.studylight.org/lexicons/greek/1532.html)

[Bible Hub](https://biblehub.com/str/greek/1532.htm)

[LSJ](https://lsj.gr/wiki/εἰστρέχω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1532)

[Bible Bento](https://biblebento.com/dictionary/G1532.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1532/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1532.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1532)

