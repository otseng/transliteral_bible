# ἀρχαῖος

[archaios](https://www.blueletterbible.org/lexicon/g744)

Definition: old (8x), of old time (3x), a good while ago (with G575) (with G2250) (1x), that has been from the beginning, original, primal, old ancient, primeval

Part of speech: adjective

Occurs 12 times in 12 verses

Derived words: archaic, [archaeo-](https://www.thefreedictionary.com/words-that-start-with-archaeo)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0744)

[Study Light](https://www.studylight.org/lexicons/greek/744.html)

[Bible Hub](https://biblehub.com/str/greek/744.htm)

[LSJ](https://lsj.gr/wiki/ἀρχαῖος)

[NET Bible](http://classic.net.bible.org/strong.php?id=744)

[Bible Bento](https://biblebento.com/dictionary/G0744.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0744/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0744.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g744)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CF%81%CF%87%CE%B1%E1%BF%96%CE%BF%CF%82)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34523)