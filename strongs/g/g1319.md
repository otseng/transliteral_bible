# διδασκαλία

[didaskalia](https://www.blueletterbible.org/lexicon/g1319)

Definition: doctrine (19x), teaching (1x), learning (1x), instruction, precepts, education

Part of speech: feminine noun

Occurs 21 times in 21 verses

Greek: [didaskalos](../g/g1320.md)

Hebrew: [lamad](../h/h3925.md)

Derived words: [didaskalia](https://en.wikipedia.org/wiki/Didaskalia_%28theatre%29)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1319)

[Study Light](https://www.studylight.org/lexicons/greek/1319.html)

[Bible Hub](https://biblehub.com/str/greek/1319.htm)

[LSJ](https://lsj.gr/wiki/διδασκαλία)

[NET Bible](http://classic.net.bible.org/strong.php?id=1319)

[Bible Bento](https://biblebento.com/dictionary/G1319.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1319/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1319.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1319)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B4%CE%B9%CE%B4%CE%B1%CF%83%CE%BA%CE%B1%CE%BB%CE%AF%CE%B1)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33911)

[Precept Austin](https://www.preceptaustin.org/titus_21-10#doctrine)