# φυσικός

[physikos](https://www.blueletterbible.org/lexicon/g5446)

Definition: natural (3x), produced by nature, inborn, physical

Part of speech: adjective

Occurs 3 times in 3 verses

Derived words: physical

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5446)

[Study Light](https://www.studylight.org/lexicons/greek/5446.html)

[Bible Hub](https://biblehub.com/str/greek/5446.htm)

[LSJ](https://lsj.gr/wiki/φυσικός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5446)

[Bible Bento](https://biblebento.com/dictionary/G5446.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5446/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5446.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5446)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%86%CF%85%CF%83%CE%B9%CE%BA%CF%8C%CF%82)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34487)
