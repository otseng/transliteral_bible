# τούτῳ

[toutō](https://www.blueletterbible.org/lexicon/g5129)

Definition: this (59x), him (10x), hereby (with G1722) (8x), herein (with G1722) (7x), miscellaneous (5x).

Part of speech: pronoun

Occurs 89 times in 87 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5129)

[Study Light](https://www.studylight.org/lexicons/greek/5129.html)

[Bible Hub](https://biblehub.com/str/greek/5129.htm)

[LSJ](https://lsj.gr/wiki/τούτῳ)

[NET Bible](http://classic.net.bible.org/strong.php?id=5129)

[Bible Bento](https://biblebento.com/dictionary/G5129.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5129/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5129.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5129)

