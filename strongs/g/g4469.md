# ῥακά

[rhaka](https://www.blueletterbible.org/lexicon/g4469)

Definition: Raca (1x), empty, i.e. a senseless, empty headed man, O, empty one, i.e. thou worthless

Part of speech: Aramaism

Hebrew: [reyq](../h/h7386.md)

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4469)

[Study Light](https://www.studylight.org/lexicons/greek/4469.html)

[Bible Hub](https://biblehub.com/str/greek/4469.htm)

[LSJ](https://lsj.gr/wiki/ῥακά)

[NET Bible](http://classic.net.bible.org/strong.php?id=4469)

[Bible Bento](https://biblebento.com/dictionary/G4469.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4469/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4469.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4469)
