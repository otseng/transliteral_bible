# κρέας

[kreas](https://www.blueletterbible.org/lexicon/g2907)

Definition: flesh (2x), butcher's meat

Part of speech: neuter noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2907)

[Study Light](https://www.studylight.org/lexicons/greek/2907.html)

[Bible Hub](https://biblehub.com/str/greek/2907.htm)

[LSJ](https://lsj.gr/wiki/κρέας)

[NET Bible](http://classic.net.bible.org/strong.php?id=2907)

[Bible Bento](https://biblebento.com/dictionary/G2907.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2907/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2907.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2907)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CF%81%CE%AD%CE%B1%CF%82)
