# προβάλλω

[proballō](https://www.blueletterbible.org/lexicon/g4261)

Definition: shoot forth (1x), put forward (1x), variations of 'put forward' (1x), throw forward, germinate

Part of speech: verb

Occurs 4 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4261)

[Study Light](https://www.studylight.org/lexicons/greek/4261.html)

[Bible Hub](https://biblehub.com/str/greek/4261.htm)

[LSJ](https://lsj.gr/wiki/προβάλλω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4261)

[Bible Bento](https://biblebento.com/dictionary/G4261.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4261/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4261.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4261)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CF%81%CE%BF%CE%B2%CE%AC%CE%BB%CE%BB%CF%89)
