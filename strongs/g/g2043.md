# ἐρείδω

[ereidō](https://www.blueletterbible.org/lexicon/g2043)

Definition: stick fast (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2043)

[Study Light](https://www.studylight.org/lexicons/greek/2043.html)

[Bible Hub](https://biblehub.com/str/greek/2043.htm)

[LSJ](https://lsj.gr/wiki/ἐρείδω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2043)

[Bible Bento](https://biblebento.com/dictionary/G2043.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2043/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2043.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2043)

