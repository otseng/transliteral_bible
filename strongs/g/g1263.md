# διαμαρτύρομαι

[diamartyromai](https://www.blueletterbible.org/lexicon/g1263)

Definition: testify (11x), charge (3x), witness (1x), to attest, testify to, solemnly affirm

Part of speech: verb

Occurs 15 times in 15 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1263)

[Study Light](https://www.studylight.org/lexicons/greek/1263.html)

[Bible Hub](https://biblehub.com/str/greek/1263.htm)

[LSJ](https://lsj.gr/wiki/διαμαρτύρομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1263)

[Bible Bento](https://biblebento.com/dictionary/G1263.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1263/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1263.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1263)