# κατάρα

[katara](https://www.blueletterbible.org/lexicon/g2671)

Definition: curse (3x), cursing (2x), cursed (1x).

Part of speech: feminine noun

Occurs 6 times in 5 verses

Hebrew: [qᵊlālâ](../h/h7045.md)

Synonyms: [curse](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Curse)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2671)

[Study Light](https://www.studylight.org/lexicons/greek/2671.html)

[Bible Hub](https://biblehub.com/str/greek/2671.htm)

[LSJ](https://lsj.gr/wiki/κατάρα)

[NET Bible](http://classic.net.bible.org/strong.php?id=2671)

[Bible Bento](https://biblebento.com/dictionary/G2671.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2671/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2671.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2671)
