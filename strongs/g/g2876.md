# κόραξ

[korax](https://www.blueletterbible.org/lexicon/g2876)

Definition: raven (1x)

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2876)

[Study Light](https://www.studylight.org/lexicons/greek/2876.html)

[Bible Hub](https://biblehub.com/str/greek/2876.htm)

[LSJ](https://lsj.gr/wiki/κόραξ)

[NET Bible](http://classic.net.bible.org/strong.php?id=2876)

[Bible Bento](https://biblebento.com/dictionary/G2876.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2876/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2876.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2876)