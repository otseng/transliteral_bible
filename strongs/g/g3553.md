# νόσημα

[nosēma](https://www.blueletterbible.org/lexicon/g3553)

Definition: disease (1x), sickness, plague

Part of speech: neuter noun

Occurs 1 times in 1 verses

Derived words: [nosema](https://en.oxforddictionaries.com/definition/nosema)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3553)

[Study Light](https://www.studylight.org/lexicons/greek/3553.html)

[Bible Hub](https://biblehub.com/str/greek/3553.htm)

[LSJ](https://lsj.gr/wiki/νόσημα)

[NET Bible](http://classic.net.bible.org/strong.php?id=3553)

[Bible Bento](https://biblebento.com/dictionary/G3553.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3553/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3553.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3553)