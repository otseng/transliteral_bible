# Σιδώνιος

[Sidōnios](https://www.blueletterbible.org/lexicon/g4606)

Definition: Sidon (1x)

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4606)

[Study Light](https://www.studylight.org/lexicons/greek/4606.html)

[Bible Hub](https://biblehub.com/str/greek/4606.htm)

[LSJ](https://lsj.gr/wiki/Σιδώνιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4606)

[Bible Bento](https://biblebento.com/dictionary/G4606.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4606/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4606.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4606)
