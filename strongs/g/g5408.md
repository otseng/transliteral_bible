# φόνος

[phonos](https://www.blueletterbible.org/lexicon/g5408)

Definition: murder (8x), slaughter (1x), be slain (with G599) (1x), homicide

Part of speech: masculine noun

Occurs 10 times in 10 verses

Greek: [phoneuō](../g/g5407.md)

Hebrew: [ratsach](../h/h7523.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5408)

[Study Light](https://www.studylight.org/lexicons/greek/5408.html)

[Bible Hub](https://biblehub.com/str/greek/5408.htm)

[LSJ](https://lsj.gr/wiki/φόνος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5408)

[Bible Bento](https://biblebento.com/dictionary/G5408.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5408/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5408.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5408)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%86%CF%8C%CE%BD%CE%BF%CF%82)