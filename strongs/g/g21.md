# ἀγαλλιάω

[agalliaō](https://www.blueletterbible.org/lexicon/g21)

Definition: rejoice (7x), be exceeding glad (1x), be glad (1x), greatly rejoice (1x), with exceeding joy (1x), to exult, rejoice exceedingly, be exceeding glad

Part of speech: verb

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0021)

[Study Light](https://www.studylight.org/lexicons/greek/21.html)

[Bible Hub](https://biblehub.com/str/greek/21.htm)

[LSJ](https://lsj.gr/wiki/ἀγαλλιάω)

[NET Bible](http://classic.net.bible.org/strong.php?id=21)

[Bible Bento](https://biblebento.com/dictionary/G0021.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0021/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0021.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g21)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CE%B3%CE%B1%CE%BB%CE%BB%CE%B9%CE%AC%CF%89)