# Μολόχ

[Moloch](https://www.blueletterbible.org/lexicon/g3434)

Definition: Moloch (1x)

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3434)

[Study Light](https://www.studylight.org/lexicons/greek/3434.html)

[Bible Hub](https://biblehub.com/str/greek/3434.htm)

[LSJ](https://lsj.gr/wiki/Μολόχ)

[NET Bible](http://classic.net.bible.org/strong.php?id=3434)

[Bible Bento](https://biblebento.com/dictionary/G3434.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3434/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3434.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3434)

[Wiktionary](https://en.wiktionary.org/wiki/Moloch)

[Wikipedia](https://en.wikipedia.org/wiki/Moloch)

[Precept Austin](https://www.preceptaustin.org/acts-7-commentary#moloch)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/moloch.html)