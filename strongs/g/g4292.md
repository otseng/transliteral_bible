# προκαλέω

[prokaleō](https://www.blueletterbible.org/lexicon/g4292)

Definition: provoke (1x), to call forth to one's self, to challenge to a combat or contest with one, irritate, challenge

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4292)

[Study Light](https://www.studylight.org/lexicons/greek/4292.html)

[Bible Hub](https://biblehub.com/str/greek/4292.htm)

[LSJ](https://lsj.gr/wiki/προκαλέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4292)

[Bible Bento](https://biblebento.com/dictionary/G4292.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4292/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4292.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4292)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33691)

[Precept Austin](https://www.preceptaustin.org/galatians_524-26#challenging)