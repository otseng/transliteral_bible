# κάμπτω

[kamptō](https://www.blueletterbible.org/lexicon/g2578)

Definition: bow (4x), bend

Part of speech: verb

Occurs 5 times in 4 verses

Hebrew: [kara'](../h/h3766.md), [kāp̄ap̄](../h/h3721.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2578)

[Study Light](https://www.studylight.org/lexicons/greek/2578.html)

[Bible Hub](https://biblehub.com/str/greek/2578.htm)

[LSJ](https://lsj.gr/wiki/κάμπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2578)

[Bible Bento](https://biblebento.com/dictionary/G2578.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2578/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2578.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2578)
