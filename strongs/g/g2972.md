# Κῶς

[Kōs](https://www.blueletterbible.org/lexicon/g2972)

Definition: Coos (1x), "a public prison", a small island of the Aegean Sea, over against the cities of Cnidus and Halicarnassus, celebrated for its fertility and esp. for its abundance of wine and corn

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2972)

[Study Light](https://www.studylight.org/lexicons/greek/2972.html)

[Bible Hub](https://biblehub.com/str/greek/2972.htm)

[LSJ](https://lsj.gr/wiki/Κῶς)

[NET Bible](http://classic.net.bible.org/strong.php?id=2972)

[Bible Bento](https://biblebento.com/dictionary/G2972.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2972/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2972.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2972)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/coos.html)