# ἄρρην

[arrēn](https://www.blueletterbible.org/lexicon/730)

Definition: male (4x), man (3x), man child (1x), man child (with G5207) (1x)

Part of speech: adjective

Occurs 9 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0730)

[Study Light](https://www.studylight.org/lexicons/greek/730.html)

[Bible Hub](https://biblehub.com/str/greek/730.htm)

[LSJ](https://lsj.gr/wiki/ἄρρην)

[NET Bible](http://classic.net.bible.org/strong.php?id=730)

[Bible Bento](https://biblebento.com/dictionary/G0730.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0730/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0730.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g730)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%84%CF%81%CF%81%CE%B7%CE%BD)