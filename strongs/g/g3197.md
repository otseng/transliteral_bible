# Μελχί

[Melchi](https://www.blueletterbible.org/lexicon/g3197)

Occurs 2 times in 2 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/3197.html)

[LSJ](https://lsj.gr/wiki/Μελχί)

[NET Bible](http://classic.net.bible.org/strong.php?id=3197)

[Bible Bento](https://biblebento.com/dictionary/G3197.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3197/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3197.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3197)

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3197)

