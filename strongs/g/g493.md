# Ἀντίπας

[antipas](https://www.blueletterbible.org/lexicon/g493)

Definition: Antipas (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0493)

[Study Light](https://www.studylight.org/lexicons/greek/493.html)

[Bible Hub](https://biblehub.com/str/greek/493.htm)

[LSJ](https://lsj.gr/wiki/Ἀντίπας)

[NET Bible](http://classic.net.bible.org/strong.php?id=493)

[Bible Bento](https://biblebento.com/dictionary/G493.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/493/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/493.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g493)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/antipas.html)

[Video Bible](https://www.videobible.com/bible-dictionary/antipas)