# τροχός

[trochos](https://www.blueletterbible.org/lexicon/g5164)

Definition: course (1x).

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5164)

[Study Light](https://www.studylight.org/lexicons/greek/5164.html)

[Bible Hub](https://biblehub.com/str/greek/5164.htm)

[LSJ](https://lsj.gr/wiki/τροχός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5164)

[Bible Bento](https://biblebento.com/dictionary/G5164.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5164/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5164.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5164)

