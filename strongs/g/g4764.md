# στρῆνος

[strēnos](https://www.blueletterbible.org/lexicon/g4764)

Definition: delicacy (1x), excessive strength which longs to break forth, over strength, strenuousness

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4764)

[Study Light](https://www.studylight.org/lexicons/greek/4764.html)

[Bible Hub](https://biblehub.com/str/greek/4764.htm)

[LSJ](https://lsj.gr/wiki/στρῆνος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4764)

[Bible Bento](https://biblebento.com/dictionary/G4764.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4764/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4764.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4764)

