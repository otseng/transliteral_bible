# βδελυκτός

[bdelyktos](https://www.blueletterbible.org/lexicon/g947)

Definition: abominable (1x), detestable

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0947)

[Study Light](https://www.studylight.org/lexicons/greek/947.html)

[Bible Hub](https://biblehub.com/str/greek/947.htm)

[LSJ](https://lsj.gr/wiki/βδελυκτός)

[NET Bible](http://classic.net.bible.org/strong.php?id=947)

[Bible Bento](https://biblebento.com/dictionary/G0947.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/947/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/947.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g947)
