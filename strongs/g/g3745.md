# ὅσος

[hosos](https://www.blueletterbible.org/lexicon/g3745)

Definition: as many as (24x), whatsoever (9x), that (9x), whatsoever things (8x), whatsoever (with G302 7 as long as) (5x), how great things (5x), what (4x), as great as, as far as, how much, how many, whoever

Part of speech: pronoun

Occurs 116 times in 104 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3745)

[Study Light](https://www.studylight.org/lexicons/greek/3745.html)

[Bible Hub](https://biblehub.com/str/greek/3745.htm)

[LSJ](https://lsj.gr/wiki/ὅσος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3745)

[Bible Bento](https://biblebento.com/dictionary/G3745.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3745/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3745.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3745)
