# Ἰσαάκ

[Isaak](https://www.blueletterbible.org/lexicon/g2464)

Definition: Isaac (20x), "to laugh", son of Abraham and Sarah

Part of speech: proper masculine noun

Occurs 20 times in 18 verses

Hebrew: [Yiṣḥāq](../h/h3327.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2464)

[Study Light](https://www.studylight.org/lexicons/greek/2464.html)

[Bible Hub](https://biblehub.com/str/greek/2464.htm)

[LSJ](https://lsj.gr/wiki/Ἰσαάκ)

[NET Bible](http://classic.net.bible.org/strong.php?id=2464)

[Bible Bento](https://biblebento.com/dictionary/G2464.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2464/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2464.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2464)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%B8%CF%83%CE%B1%CE%AC%CE%BA)

[Wikipedia](https://en.wikipedia.org/wiki/Isaac)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/isaac.html)

[Video Bible](https://www.videobible.com/bible-dictionary/isaac)