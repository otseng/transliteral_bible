# ἐπιθανάτιος

[epithanatios](https://www.blueletterbible.org/lexicon/g1935)

Definition: appoint to death (1x), doomed to death

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1935)

[Study Light](https://www.studylight.org/lexicons/greek/1935.html)

[Bible Hub](https://biblehub.com/str/greek/1935.htm)

[LSJ](https://lsj.gr/wiki/ἐπιθανάτιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1935)

[Bible Bento](https://biblebento.com/dictionary/G1935.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1935/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1935.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1935)
