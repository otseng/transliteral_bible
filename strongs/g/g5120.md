# τοῦ

[tou](https://www.blueletterbible.org/lexicon/g5120)

Definition: his (1x).

Part of speech: article

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5120)

[Study Light](https://www.studylight.org/lexicons/greek/5120.html)

[Bible Hub](https://biblehub.com/str/greek/5120.htm)

[LSJ](https://lsj.gr/wiki/τοῦ)

[NET Bible](http://classic.net.bible.org/strong.php?id=5120)

[Bible Bento](https://biblebento.com/dictionary/G5120.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5120/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5120.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5120)

