# ἐπίστασις

[epistasis](https://www.blueletterbible.org/lexicon/g1999)

Definition: that which comes upon (1x), a raising up (with G4160) (1x), a hostile banding together or concourse

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1999)

[Study Light](https://www.studylight.org/lexicons/greek/1999.html)

[Bible Hub](https://biblehub.com/str/greek/1999.htm)

[LSJ](https://lsj.gr/wiki/ἐπίστασις)

[NET Bible](http://classic.net.bible.org/strong.php?id=1999)

[Bible Bento](https://biblebento.com/dictionary/G1999.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1999/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1999.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1999)
