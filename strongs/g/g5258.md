# ὕπνος

[hypnos](https://www.blueletterbible.org/lexicon/g5258)

Definition: sleep (6x)

Part of speech: masculine noun

Occurs 6 times in 5 verses

Derived words: hypnosis, [hypno-](https://www.thefreedictionary.com/words-that-start-with-hypno)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5258)

[Study Light](https://www.studylight.org/lexicons/greek/5258.html)

[Bible Hub](https://biblehub.com/str/greek/5258.htm)

[LSJ](https://lsj.gr/wiki/ὕπνος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5258)

[Bible Bento](https://biblebento.com/dictionary/G5258.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5258/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5258.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5258)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BD%95%CF%80%CE%BD%CE%BF%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Hypnos)

[Mythology](https://mythology.net/greek/greek-gods/hypnos/)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-definitions/5258-hupnos.htm)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/5258-hupnos-sleep.htm)
