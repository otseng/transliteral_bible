# ἀδικέω

[adikeō](https://www.blueletterbible.org/lexicon/g91)

Definition: hurt (10x), do wrong (8x), wrong (2x), suffer wrong (2x), be unjust (2x), take wrong (1x), injure (1x), be an offender (1x), act unjustly or wickedly, to sin, damage, harm

Part of speech: verb

Occurs 36 times in 23 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0091)

[Study Light](https://www.studylight.org/lexicons/greek/91.html)

[Bible Hub](https://biblehub.com/str/greek/91.htm)

[LSJ](https://lsj.gr/wiki/ἀδικέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=91)

[Bible Bento](https://biblebento.com/dictionary/G0091.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0091/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0091.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g91)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CE%B4%CE%B9%CE%BA%CE%AD%CF%89)