# Ὀζίας

[Ozias](https://www.blueletterbible.org/lexicon/g3604)

Definition: Ozias (2x), Uzziah, strength of Jehovah, son of Amaziah, king of Judah from B.C. 810 - 758

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/3604.html)

[LSJ](https://lsj.gr/wiki/Ὀζίας)

[NET Bible](http://classic.net.bible.org/strong.php?id=3604)

[Bible Bento](https://biblebento.com/dictionary/G3604.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3604/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3604.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3604)

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3604)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/O/ozias.html)