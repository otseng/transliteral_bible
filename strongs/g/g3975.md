# παχύνω

[pachynō](https://www.blueletterbible.org/lexicon/g3975)

Definition: wax gross (2x), to make thick, to make fat, fatten, to make stupid

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3975)

[Study Light](https://www.studylight.org/lexicons/greek/3975.html)

[Bible Hub](https://biblehub.com/str/greek/3975.htm)

[LSJ](https://lsj.gr/wiki/παχύνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3975)

[Bible Bento](https://biblebento.com/dictionary/G3975.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3975/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3975.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3975)
