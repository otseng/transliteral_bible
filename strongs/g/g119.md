# ἄθλησις

[athlēsis](https://www.blueletterbible.org/lexicon/g119)

Definition: fight (1x), to contest, to combat, to strive, struggle, hard trial

Part of speech: feminine noun

Occurs 1 times in 1 verses

Synonyms: [fight](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Fight)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0119)

[Study Light](https://www.studylight.org/lexicons/greek/119.html)

[Bible Hub](https://biblehub.com/str/greek/119.htm)

[LSJ](https://lsj.gr/wiki/ἄθλησις)

[NET Bible](http://classic.net.bible.org/strong.php?id=119)

[Bible Bento](https://biblebento.com/dictionary/G119.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/119/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/119.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g119)
