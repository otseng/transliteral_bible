# σύμφημι

[symphēmi](https://www.blueletterbible.org/lexicon/g4852)

Definition: consent unto (1x), confess

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4852)

[Study Light](https://www.studylight.org/lexicons/greek/4852.html)

[Bible Hub](https://biblehub.com/str/greek/4852.htm)

[LSJ](https://lsj.gr/wiki/σύμφημι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4852)

[Bible Bento](https://biblebento.com/dictionary/G4852.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4852/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4852.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4852)
