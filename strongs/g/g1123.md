# γραπτός

[graptos](https://www.blueletterbible.org/lexicon/g1123)

Definition: written (1x), inscribed, painted

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1123)

[Study Light](https://www.studylight.org/lexicons/greek/1123.html)

[Bible Hub](https://biblehub.com/str/greek/1123.htm)

[LSJ](https://lsj.gr/wiki/γραπτός)

[NET Bible](http://classic.net.bible.org/strong.php?id=1123)

[Bible Bento](https://biblebento.com/dictionary/G1123.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1123/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1123.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1123)

