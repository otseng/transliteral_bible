# κατανοέω

[katanoeō](https://www.blueletterbible.org/lexicon/g2657)

Definition: consider (7x), behold (4x), perceive (2x), discover (1x), perceive, remark, observe, understand, behold, consider, look carefully

Part of speech: verb

Occurs 14 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2657)

[Study Light](https://www.studylight.org/lexicons/greek/2657.html)

[Bible Hub](https://biblehub.com/str/greek/2657.htm)

[LSJ](https://lsj.gr/wiki/κατανοέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2657)

[Bible Bento](https://biblebento.com/dictionary/G2657.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2657/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2657.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2657)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34515)

[Precept Austin](https://www.preceptaustin.org/romans_419-21#contemplated)