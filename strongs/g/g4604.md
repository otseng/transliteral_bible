# σίδηρος

[sidēros](https://www.blueletterbible.org/lexicon/g4604)

Definition: iron (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4604)

[Study Light](https://www.studylight.org/lexicons/greek/4604.html)

[Bible Hub](https://biblehub.com/str/greek/4604.htm)

[LSJ](https://lsj.gr/wiki/σίδηρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4604)

[Bible Bento](https://biblebento.com/dictionary/G4604.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4604/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4604.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4604)

