# προαιτιάομαι

[proaitiaomai](https://www.blueletterbible.org/lexicon/g4256)

Definition: prove before (1x), to bring a charge against previously

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4256)

[Study Light](https://www.studylight.org/lexicons/greek/4256.html)

[Bible Hub](https://biblehub.com/str/greek/4256.htm)

[LSJ](https://lsj.gr/wiki/προαιτιάομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4256)

[Bible Bento](https://biblebento.com/dictionary/G4256.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4256/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4256.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4256)