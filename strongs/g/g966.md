# Βηθσαϊδά

[Bēthsaïda](https://www.blueletterbible.org/lexicon/g966)

Definition: Bethsaida (7x), "house of fish", a small fishing village on the west shore of Lake Gennesaret, home of Andrew, Peter, Philip and John

Part of speech: proper locative noun

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0966)

[Study Light](https://www.studylight.org/lexicons/greek/966.html)

[Bible Hub](https://biblehub.com/str/greek/966.htm)

[LSJ](https://lsj.gr/wiki/Βηθσαϊδά)

[NET Bible](http://classic.net.bible.org/strong.php?id=966)

[Bible Bento](https://biblebento.com/dictionary/G0966.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0966/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0966.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g966)

[Wikipedia](https://en.wikipedia.org/wiki/Bethsaida)

[Bible Places](https://www.bibleplaces.com/bethsaida/)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bethsaida.html)

[Video Bible](https://www.videobible.com/bible-dictionary/bethsaida)