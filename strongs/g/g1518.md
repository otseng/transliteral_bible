# εἰρηνοποιός

[eirēnopoios](https://www.blueletterbible.org/lexicon/g1518)

Definition: peacemakers (1x)

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1518)

[Study Light](https://www.studylight.org/lexicons/greek/1518.html)

[Bible Hub](https://biblehub.com/str/greek/1518.htm)

[LSJ](https://lsj.gr/wiki/εἰρηνοποιός)

[NET Bible](http://classic.net.bible.org/strong.php?id=1518)

[Bible Bento](https://biblebento.com/dictionary/G1518.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1518/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1518.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1518)