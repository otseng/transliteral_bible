# ἁρμόζω

[harmozō](https://www.blueletterbible.org/lexicon/g718)

Definition: espouse (1x), to join, to fit together, to betroth a daughter to any one, to woo

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0718)

[Study Light](https://www.studylight.org/lexicons/greek/718.html)

[Bible Hub](https://biblehub.com/str/greek/718.htm)

[LSJ](https://lsj.gr/wiki/ἁρμόζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=718)

[Bible Bento](https://biblebento.com/dictionary/G0718.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0718/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0718.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g718)
