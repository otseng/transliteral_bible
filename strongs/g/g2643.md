# καταλλαγή

[katallagē](https://www.blueletterbible.org/lexicon/g2643)

Definition: reconciliation (2x), atonement (1x), reconciling (1x), exchanging equivalent values, adjustment of a difference, reconciliation, restoration to favour

Part of speech: feminine noun

Occurs 4 times in 4 verses

Hebrew: [kāp̄ar](../h/h3722.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2643)

[Study Light](https://www.studylight.org/lexicons/greek/2643.html)

[Bible Hub](https://biblehub.com/str/greek/2643.htm)

[LSJ](https://lsj.gr/wiki/καταλλαγή)

[NET Bible](http://classic.net.bible.org/strong.php?id=2643)

[Bible Bento](https://biblebento.com/dictionary/G2643.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2643/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2643.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2643)

[Precept Austin](https://www.preceptaustin.org/reconciliation-katallage-greek-word-study)
