# βασιλεύω

[basileuō](https://www.blueletterbible.org/lexicon/g936)

Definition: reign (20x), king (1x), exercise kingly power

Part of speech: verb

Occurs 23 times in 18 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0936)

[Study Light](https://www.studylight.org/lexicons/greek/936.html)

[Bible Hub](https://biblehub.com/str/greek/936.htm)

[LSJ](https://lsj.gr/wiki/βασιλεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=936)

[Bible Bento](https://biblebento.com/dictionary/G0936.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0936/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0936.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g936)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B2%CE%B1%CF%83%CE%B9%CE%BB%CE%B5%CF%8D%CF%89)