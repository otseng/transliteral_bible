# Χριστός

[Christos](https://www.blueletterbible.org/lexicon/g5547)

Definition: Christ (569x), Anointed, Messiah

Part of speech: adjective

Occurs 569 times in 530 verses

Hebrew: [mashiyach](../h/h4899.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5547)

[Study Light](https://www.studylight.org/lexicons/greek/5547.html)

[Bible Hub](https://biblehub.com/str/greek/5547.htm)

[LSJ](https://lsj.gr/wiki/Χριστός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5547)

[Bible Bento](https://biblebento.com/dictionary/G5547.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5547/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5547.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5547)

[Wikipedia - Christ (title)](https://en.wikipedia.org/wiki/Christ_%28title%29)

[Christ meaning](http://www.abarim-publications.com/Meaning/Christ.html)