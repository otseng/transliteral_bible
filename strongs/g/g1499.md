# εἰ καί

[ei kai](https://www.blueletterbible.org/lexicon/g1499)

Definition: though (14x), if (5x), and if (2x), if that (1x), if also (1x).

Part of speech: conjunction

Occurs 23 times in 23 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1499)

[Study Light](https://www.studylight.org/lexicons/greek/1499.html)

[Bible Hub](https://biblehub.com/str/greek/1499.htm)

[LSJ](https://lsj.gr/wiki/εἰ καί)

[NET Bible](http://classic.net.bible.org/strong.php?id=1499)

[Bible Bento](https://biblebento.com/dictionary/G1499.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1499/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1499.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1499)

