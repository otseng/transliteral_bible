# ἔγκλημα

[egklēma](https://www.blueletterbible.org/lexicon/g1462)

Definition: laid to (one's) charge (1x), crime laid against (one) (1x), accusation

Part of speech: neuter noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1462)

[Study Light](https://www.studylight.org/lexicons/greek/1462.html)

[Bible Hub](https://biblehub.com/str/greek/1462.htm)

[LSJ](https://lsj.gr/wiki/ἔγκλημα)

[NET Bible](http://classic.net.bible.org/strong.php?id=1462)

[Bible Bento](https://biblebento.com/dictionary/G1462.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1462/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1462.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1462)
