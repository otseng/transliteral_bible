# παραρρέω

[pararreō](https://www.blueletterbible.org/lexicon/g3901)

Definition: let slip (1x), to glide by: lest we be carried by, pass by

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3901)

[Study Light](https://www.studylight.org/lexicons/greek/3901.html)

[Bible Hub](https://biblehub.com/str/greek/3901.htm)

[LSJ](https://lsj.gr/wiki/παραρρέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3901)

[Bible Bento](https://biblebento.com/dictionary/G3901.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3901/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3901.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3901)
