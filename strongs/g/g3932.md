# παρθενία

[parthenia](https://www.blueletterbible.org/lexicon/g3932)

Definition: virginity (1x)

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3932)

[Study Light](https://www.studylight.org/lexicons/greek/3932.html)

[Bible Hub](https://biblehub.com/str/greek/3932.htm)

[LSJ](https://lsj.gr/wiki/παρθενία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3932)

[Bible Bento](https://biblebento.com/dictionary/G3932.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3932/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3932.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3932)

[Wikipedia](https://en.wikipedia.org/wiki/Parthenia_%28music%29)