# μοσχοποιέω

[moschopoieō](https://www.blueletterbible.org/lexicon/g3447)

Definition: make a calf (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3447)

[Study Light](https://www.studylight.org/lexicons/greek/3447.html)

[Bible Hub](https://biblehub.com/str/greek/3447.htm)

[LSJ](https://lsj.gr/wiki/μοσχοποιέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3447)

[Bible Bento](https://biblebento.com/dictionary/G3447.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3447/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3447.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3447)
