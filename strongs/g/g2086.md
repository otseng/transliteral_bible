# ἑτεροζυγέω

[heterozygeō](https://www.blueletterbible.org/lexicon/g2086)

Definition: be unequally yoked together with (1x), to associate discordantly, "different yoke"

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2086)

[Study Light](https://www.studylight.org/lexicons/greek/2086.html)

[Bible Hub](https://biblehub.com/str/greek/2086.htm)

[LSJ](https://lsj.gr/wiki/ἑτεροζυγέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2086)

[Bible Bento](https://biblebento.com/dictionary/G2086.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2086/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2086.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2086)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%91%CF%84%CE%B5%CF%81%CE%BF%CE%B6%CF%85%CE%B3%CE%AD%CF%89)
