# σβέννυμι

[sbennymi](https://www.blueletterbible.org/lexicon/g4570)

Definition: quench (7x), go out (1x), extinguish, stifle

Part of speech: verb

Occurs 12 times in 8 verses

Hebrew: [kāḇâ](../h/h3518.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4570)

[Study Light](https://www.studylight.org/lexicons/greek/4570.html)

[Bible Hub](https://biblehub.com/str/greek/4570.htm)

[LSJ](https://lsj.gr/wiki/σβέννυμι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4570)

[Bible Bento](https://biblebento.com/dictionary/G4570.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4570/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4570.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4570)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CE%B2%CE%AD%CE%BD%CE%BD%CF%85%CE%BC%CE%B9)