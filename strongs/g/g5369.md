# φιλήδονος

[philēdonos](https://www.blueletterbible.org/lexicon/g5369)

Definition: lover of pleasure (1x), hedonistic

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5369)

[Study Light](https://www.studylight.org/lexicons/greek/5369.html)

[Bible Hub](https://biblehub.com/str/greek/5369.htm)

[LSJ](https://lsj.gr/wiki/φιλήδονος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5369)

[Bible Bento](https://biblebento.com/dictionary/G5369.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5369/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5369.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5369)
