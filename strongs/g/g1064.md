# γαστήρ

[gastēr](https://www.blueletterbible.org/lexicon/g1064)

Definition: be with child (with G1722) (with G2192) (5x), with child (with G1722) (with G2192) (2x), womb (1x), belly (1x), womb, stomach

Part of speech: feminine noun

Occurs 9 times in 9 verses

Hebrew: [qereḇ](../h/h7130.md)

Derived words: [gaster](https://en.wikipedia.org/wiki/Gaster_%28insect_anatomy%29)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1064)

[Study Light](https://www.studylight.org/lexicons/greek/1064.html)

[Bible Hub](https://biblehub.com/str/greek/1064.htm)

[LSJ](https://lsj.gr/wiki/γαστήρ)

[NET Bible](http://classic.net.bible.org/strong.php?id=1064)

[Bible Bento](https://biblebento.com/dictionary/G1064.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1064/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1064.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1064)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B3%CE%B1%CF%83%CF%84%CE%AE%CF%81)