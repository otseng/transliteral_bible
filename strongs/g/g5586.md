# ψῆφος

[psēphos](https://www.blueletterbible.org/lexicon/g5586)

Definition: stone (2x), voice (1x), a vote (on account of the use of pebbles in voting)

Part of speech: feminine noun

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5586)

[Study Light](https://www.studylight.org/lexicons/greek/5586.html)

[Bible Hub](https://biblehub.com/str/greek/5586.htm)

[LSJ](https://lsj.gr/wiki/ψῆφος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5586)

[Bible Bento](https://biblebento.com/dictionary/G5586.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5586/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5586.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5586)
