# μάννα

[manna](https://www.blueletterbible.org/lexicon/g3131)

Definition: manna (5x)

Part of speech: neuter noun

Occurs 5 times in 5 verses

Hebrew: [man](../h/h4478.md)

Derived words: manna

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3131)

[Study Light](https://www.studylight.org/lexicons/greek/3131.html)

[Bible Hub](https://biblehub.com/str/greek/3131.htm)

[LSJ](https://lsj.gr/wiki/μάννα)

[NET Bible](http://classic.net.bible.org/strong.php?id=3131)

[Bible Bento](https://biblebento.com/dictionary/G3131.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3131/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3131.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3131)

[Wikipedia](https://en.wikipedia.org/wiki/Manna)

[New Advent](http://www.newadvent.org/cathen/09604a.htm)

[Jewish Encyclopedia](http://www.jewishencyclopedia.com/articles/10366-manna)