# Ναθαναήλ

[Nathanaēl](https://www.blueletterbible.org/lexicon/g3482)

Definition: Nathanael (6x), "gift of God"

Part of speech: proper masculine noun

Occurs 6 times in 6 verses

Notes: commonly thought to be the same person as Bartholomew

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3482)

[Study Light](https://www.studylight.org/lexicons/greek/3482.html)

[Bible Hub](https://biblehub.com/str/greek/3482.htm)

[LSJ](https://lsj.gr/wiki/Ναθαναήλ)

[NET Bible](http://classic.net.bible.org/strong.php?id=3482)

[Bible Bento](https://biblebento.com/dictionary/G3482.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3482/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3482.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3482)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/nathanael.html)

[Video Bible](https://www.videobible.com/bible-dictionary/nathanael)