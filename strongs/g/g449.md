# ἄνιπτος

[aniptos](https://www.blueletterbible.org/lexicon/g449)

Definition: unwashen (3x)

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0449)

[Study Light](https://www.studylight.org/lexicons/greek/449.html)

[Bible Hub](https://biblehub.com/str/greek/449.htm)

[LSJ](https://lsj.gr/wiki/ἄνιπτος)

[NET Bible](http://classic.net.bible.org/strong.php?id=449)

[Bible Bento](https://biblebento.com/dictionary/G0449.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0449/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0449.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g449)