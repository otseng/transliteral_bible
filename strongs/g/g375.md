# ἀναπέμπω

[anapempō](https://www.blueletterbible.org/lexicon/g375)

Definition: send (2x), send again (2x).

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g375)

[Study Light](https://www.studylight.org/lexicons/greek/375.html)

[Bible Hub](https://biblehub.com/str/greek/375.htm)

[LSJ](https://lsj.gr/wiki/ἀναπέμπω)

[NET Bible](http://classic.net.bible.org/strong.php?id=375)

[Bible Bento](https://biblebento.com/dictionary/G375.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/375/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/375.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g375)

