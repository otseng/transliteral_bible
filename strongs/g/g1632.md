# ἐκχέω

[ekcheō](https://www.blueletterbible.org/lexicon/g1632)

Definition: pour out (12x), shed (4x), shed forth (1x), spill (1x), run out (1x), shed (5x), run greedily (1x), shed abroad (1x), gush out (1x), spill (1x), gush out, emitted in quantity

Part of speech: verb

Occurs 28 times in 28 verses

Hebrew: [šāp̄aḵ](../h/h8210.md), [nāṯaḵ](../h/h5413.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1632)

[Study Light](https://www.studylight.org/lexicons/greek/1632.html)

[Bible Hub](https://biblehub.com/str/greek/1632.htm)

[LSJ](https://lsj.gr/wiki/ἐκχέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1632)

[Bible Bento](https://biblebento.com/dictionary/G1632.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1632/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1632.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1632)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%90%CE%BA%CF%87%CE%AD%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34620)