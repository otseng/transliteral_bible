# μάστιξ

[mastix](https://www.blueletterbible.org/lexicon/g3148)

Definition: plague (4x), scourging (2x), scourge, misfortune, calamity

Part of speech: feminine noun

Occurs 6 times in 6 verses

Hebrew: [šôṭ](../h/h7752.md)

Derived words: [-mastix](https://wikivisually.com/wiki/-mastix)

Synonyms: [plague](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Plague)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3148)

[Study Light](https://www.studylight.org/lexicons/greek/3148.html)

[Bible Hub](https://biblehub.com/str/greek/3148.htm)

[LSJ](https://lsj.gr/wiki/μάστιξ)

[NET Bible](http://classic.net.bible.org/strong.php?id=3148)

[Bible Bento](https://biblebento.com/dictionary/G3148.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3148/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3148.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3148)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BC%CE%AC%CF%83%CF%84%CE%B9%CE%BE)

[Wikipedia](https://en.wikipedia.org/wiki/-mastix)