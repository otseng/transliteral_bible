# ὄρος

[oros](https://www.blueletterbible.org/lexicon/g3735)

Definition: mountain (41x), mount (21x), hill (3x)

Part of speech: neuter noun

Occurs 65 times in 65 verses

Greek: [airō](../g/g142.md), [ouranos](../g/g3772.md)

Hebrew: [giḇʿâ](../h/h1389.md), [har](../h/h2022.md)

Derived words: orology, orography, [oro-](http://www.thefreedictionary.com/words-that-start-with-oro)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3735)

[Study Light](https://www.studylight.org/lexicons/greek/3735.html)

[Bible Hub](https://biblehub.com/str/greek/3735.htm)

[LSJ](https://lsj.gr/wiki/ὄρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3735)

[Bible Bento](https://biblebento.com/dictionary/G3735.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3735/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3735.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3735)