# ἀνάστασις

[anastasis](https://www.blueletterbible.org/lexicon/g386)

Definition: resurrection (39x), rising again (1x), that should rise (1x), raised to life again (with G1537) (1x), stand again

Part of speech: feminine noun

Occurs 42 times in 40 verses

Greek: [anistēmi](../g/g450.md)

Hebrew: [quwm](../h/h6965.md), [qîmâ](../h/h7012.md)

Derived words: [anastasis](https://en.wiktionary.org/wiki/anastasis)

Synonyms: [resurrect](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Resurrect)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0386)

[Study Light](https://www.studylight.org/lexicons/greek/386.html)

[Bible Hub](https://biblehub.com/str/greek/386.htm)

[LSJ](https://lsj.gr/wiki/ἀνάστασις)

[NET Bible](http://classic.net.bible.org/strong.php?id=386)

[Bible Bento](https://biblebento.com/dictionary/G0386.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0386/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0386.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g386)

[Wiktionary](https://en.wiktionary.org/wiki/anastasis)

[Wikipedia](https://en.wikipedia.org/wiki/Resurrection)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/0386-anastasis-resurrection.htm)
