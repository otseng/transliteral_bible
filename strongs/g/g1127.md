# γρηγορέω

[grēgoreō](https://www.blueletterbible.org/lexicon/g1127)

Definition: watch (21x), wake (1x), be vigilant (1x), be cautious, take heed, be vigilant

Part of speech: verb

Occurs 24 times in 23 verses

Greek: [egeirō](../g/g1453.md)

Hebrew: ['amad](../h/h5975.md), [šāqaḏ](../h/h8245.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1127)

[Study Light](https://www.studylight.org/lexicons/greek/1127.html)

[Bible Hub](https://biblehub.com/str/greek/1127.htm)

[LSJ](https://lsj.gr/wiki/γρηγορέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1127)

[Bible Bento](https://biblebento.com/dictionary/G1127.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1127/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1127.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1127)