# ἰδέα

[idea](https://www.blueletterbible.org/lexicon/g2397)

Definition: countenance (1x), form, external appearance

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2397)

[Study Light](https://www.studylight.org/lexicons/greek/2397.html)

[Bible Hub](https://biblehub.com/str/greek/2397.htm)

[LSJ](https://lsj.gr/wiki/ἰδέα)

[NET Bible](http://classic.net.bible.org/strong.php?id=2397)

[Bible Bento](https://biblebento.com/dictionary/G2397.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2397/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2397.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2397)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%B0%CE%B4%CE%AD%CE%B1)
