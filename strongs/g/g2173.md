# εὔχρηστος

[euchrēstos](https://www.blueletterbible.org/lexicon/g2173)

Definition: profitable (2x), meet for use (1x).

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2173)

[Study Light](https://www.studylight.org/lexicons/greek/2173.html)

[Bible Hub](https://biblehub.com/str/greek/2173.htm)

[LSJ](https://lsj.gr/wiki/εὔχρηστος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2173)

[Bible Bento](https://biblebento.com/dictionary/G2173.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2173/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2173.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2173)
