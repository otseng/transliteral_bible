# προπάσχω

[propaschō](https://www.blueletterbible.org/lexicon/g4310)

Definition: suffer before (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4310)

[Study Light](https://www.studylight.org/lexicons/greek/4310.html)

[Bible Hub](https://biblehub.com/str/greek/4310.htm)

[LSJ](https://lsj.gr/wiki/προπάσχω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4310)

[Bible Bento](https://biblebento.com/dictionary/G4310.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4310/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4310.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4310)
