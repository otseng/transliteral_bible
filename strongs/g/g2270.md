# ἡσυχάζω

[hēsychazō](https://www.blueletterbible.org/lexicon/g2270)

Definition: hold (one's) peace (2x), rest (1x), cease (1x), be quiet (1x), to keep still, to cease from labor

Part of speech: verb

Occurs 5 times in 5 verses

Hebrew: [shabath](../h/h7673.md)

Synonyms: [rest](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Rest)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2270)

[Study Light](https://www.studylight.org/lexicons/greek/2270.html)

[Bible Hub](https://biblehub.com/str/greek/2270.htm)

[LSJ](https://lsj.gr/wiki/ἡσυχάζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2270)

[Bible Bento](https://biblebento.com/dictionary/G2270.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2270/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2270.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2270)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%A1%CF%83%CF%85%CF%87%CE%AC%CE%B6%CF%89)