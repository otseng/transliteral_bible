# τελώνιον

[telōnion](https://www.blueletterbible.org/lexicon/g5058)

Definition: receipt of custom (3x), toll house, place of toll, tax office, the place in which the tax collector sat to collect the taxes

Part of speech: neuter noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5058)

[Study Light](https://www.studylight.org/lexicons/greek/5058.html)

[Bible Hub](https://biblehub.com/str/greek/5058.htm)

[LSJ](https://lsj.gr/wiki/τελώνιον)

[NET Bible](http://classic.net.bible.org/strong.php?id=5058)

[Bible Bento](https://biblebento.com/dictionary/G5058.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5058/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5058.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5058)