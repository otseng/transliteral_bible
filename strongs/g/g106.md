# ἄζυμος

[azymos](https://www.blueletterbible.org/lexicon/g106)

Definition: unleavened bread (8x), unleavened (1x), unleavened loaves used in the paschal feast of the Jews

Part of speech: adjective

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0106)

[Study Light](https://www.studylight.org/lexicons/greek/106.html)

[Bible Hub](https://biblehub.com/str/greek/106.htm)

[LSJ](https://lsj.gr/wiki/ἄζυμος)

[NET Bible](http://classic.net.bible.org/strong.php?id=106)

[Bible Bento](https://biblebento.com/dictionary/G0106.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0106/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0106.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g106)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%AC%CE%B6%CF%85%CE%BC%CE%BF%CF%82)