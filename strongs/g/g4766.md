# στρωννύω

[strōnnyō](https://www.blueletterbible.org/lexicon/g4766)

Definition: spread (2x), straw (2x), furnish (2x), make (one's) bed (1x), make bed

Part of speech: verb

Occurs 8 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4766)

[Study Light](https://www.studylight.org/lexicons/greek/4766.html)

[Bible Hub](https://biblehub.com/str/greek/4766.htm)

[LSJ](https://lsj.gr/wiki/στρωννύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4766)

[Bible Bento](https://biblebento.com/dictionary/G4766.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4766/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4766.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4766)
