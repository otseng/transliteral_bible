# τρυφάω

[tryphaō](https://www.blueletterbible.org/lexicon/g5171)

Definition: live in pleasure (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5171)

[Study Light](https://www.studylight.org/lexicons/greek/5171.html)

[Bible Hub](https://biblehub.com/str/greek/5171.htm)

[LSJ](https://lsj.gr/wiki/τρυφάω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5171)

[Bible Bento](https://biblebento.com/dictionary/G5171.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5171/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5171.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5171)

