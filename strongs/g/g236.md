# ἀλλάσσω

[allassō](https://www.blueletterbible.org/lexicon/g236)

Definition: change (6x), to exchange one thing for another, to transform

Part of speech: verb

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0236)

[Study Light](https://www.studylight.org/lexicons/greek/236.html)

[Bible Hub](https://biblehub.com/str/greek/236.htm)

[LSJ](https://lsj.gr/wiki/ἀλλάσσω)

[NET Bible](http://classic.net.bible.org/strong.php?id=236)

[Bible Bento](https://biblebento.com/dictionary/G0236.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0236/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0236.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g236)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CE%BB%CE%BB%CE%AC%CE%B6%CF%89)

[Sermon Index](https://www.sermonindex.net/modules/articles/index.php?view=article&aid=33692)

[Precept Austin](https://www.preceptaustin.org/romans_122-23#e)
