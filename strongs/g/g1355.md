# διόπερ

[dioper](https://www.blueletterbible.org/lexicon/g1355)

Definition: wherefore (3x).

Part of speech: conjunction

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1355)

[Study Light](https://www.studylight.org/lexicons/greek/1355.html)

[Bible Hub](https://biblehub.com/str/greek/1355.htm)

[LSJ](https://lsj.gr/wiki/διόπερ)

[NET Bible](http://classic.net.bible.org/strong.php?id=1355)

[Bible Bento](https://biblebento.com/dictionary/G1355.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1355/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1355.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1355)

