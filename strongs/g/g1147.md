# δάκτυλος

[daktylos](https://www.blueletterbible.org/lexicon/g1147)

Definition: finger (8x), digit

Part of speech: masculine noun

Occurs 8 times in 8 verses

Greek: [daktylios](../g/g1146.md)

Derived words: [date (fruit)](https://en.wiktionary.org/wiki/date)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1147)

[Study Light](https://www.studylight.org/lexicons/greek/1147.html)

[Bible Hub](https://biblehub.com/str/greek/1147.htm)

[LSJ](https://lsj.gr/wiki/δάκτυλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1147)

[Bible Bento](https://biblebento.com/dictionary/G1147.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1147/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1147.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1147)

[Wiktionary](https://en.wiktionary.org/wiki/δάχτυλο)