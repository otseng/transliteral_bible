# ἐμπνέω

[empneō](https://www.blueletterbible.org/lexicon/g1709)

Definition: breathe out (1x), threatenings and slaughter were so to speak the element from which he drew his breath

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1709)

[Study Light](https://www.studylight.org/lexicons/greek/1709.html)

[Bible Hub](https://biblehub.com/str/greek/1709.htm)

[LSJ](https://lsj.gr/wiki/ἐμπνέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1709)

[Bible Bento](https://biblebento.com/dictionary/G1709.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1709/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1709.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1709)
