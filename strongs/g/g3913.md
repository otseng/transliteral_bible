# παραφρονία

[paraphronia](https://www.blueletterbible.org/lexicon/g3913)

Definition: madness (1x), insanity, foolhardiness

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3913)

[Study Light](https://www.studylight.org/lexicons/greek/3913.html)

[Bible Hub](https://biblehub.com/str/greek/3913.htm)

[LSJ](https://lsj.gr/wiki/παραφρονία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3913)

[Bible Bento](https://biblebento.com/dictionary/G3913.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3913/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3913.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3913)

