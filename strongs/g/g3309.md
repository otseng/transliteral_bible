# μεριμνάω

[merimnaō](https://www.blueletterbible.org/lexicon/g3309)

Definition: take thought (11x), care (5x), be careful (2x), have care (1x), to be anxious, to be troubled with cares

Part of speech: verb

Occurs 24 times in 17 verses

Hebrew: [dā'aḡ](../h/h1672.md), [ragaz](../h/h7264.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3309)

[Study Light](https://www.studylight.org/lexicons/greek/3309.html)

[Bible Hub](https://biblehub.com/str/greek/3309.htm)

[LSJ](https://lsj.gr/wiki/μεριμνάω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3309)

[Bible Bento](https://biblebento.com/dictionary/G3309.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3309/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3309.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3309)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BC%CE%B5%CF%81%CE%B9%CE%BC%CE%BD%CE%AC%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33526)

[Precept Austin](https://www.preceptaustin.org/philippians_46-71#anxious%20merimnao)

[Tim Lane](https://timlane.org/blog/how-does-the-bible-define-worry)

[Pepperdine Boone Center for the Family](https://family.pepperdine.edu/blog/posts/merimnaoblogpost.htm)

[Got fruit](https://gotfruitblog.wordpress.com/2007/09/26/careful-is-not-what-you-think/)