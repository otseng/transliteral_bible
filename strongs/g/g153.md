# αἰσχύνω

[aischynō](https://www.blueletterbible.org/lexicon/g153)

Definition: be ashamed (5x), to dishonour, to disgrace

Part of speech: verb

Occurs 7 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0153)

[Study Light](https://www.studylight.org/lexicons/greek/153.html)

[Bible Hub](https://biblehub.com/str/greek/153.htm)

[LSJ](https://lsj.gr/wiki/αἰσχύνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=153)

[Bible Bento](https://biblebento.com/dictionary/G0153.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0153/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0153.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g153)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%E1%BC%B0%CF%83%CF%87%CF%8D%CE%BD%CF%89)