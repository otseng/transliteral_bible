# πτηνός

[ptēnos](https://www.blueletterbible.org/lexicon/g4421)

Definition: bird (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4421)

[Study Light](https://www.studylight.org/lexicons/greek/4421.html)

[Bible Hub](https://biblehub.com/str/greek/4421.htm)

[LSJ](https://lsj.gr/wiki/πτηνός)

[NET Bible](http://classic.net.bible.org/strong.php?id=4421)

[Bible Bento](https://biblebento.com/dictionary/G4421.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4421/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4421.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4421)
