# παραιτέομαι

[paraiteomai](https://www.blueletterbible.org/lexicon/g3868)

Definition: refuse (5x), excuse (2x), make excuse (1x), avoid (1x), reject (1x), intreat (1x), deprecate, decline, shun

Part of speech: verb

Occurs 11 times in 9 verses

Hebrew: [mā'ēn](../h/h3985.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3868)

[Study Light](https://www.studylight.org/lexicons/greek/3868.html)

[Bible Hub](https://biblehub.com/str/greek/3868.htm)

[LSJ](https://lsj.gr/wiki/παραιτέομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=3868)

[Bible Bento](https://biblebento.com/dictionary/G3868.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3868/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3868.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3868)