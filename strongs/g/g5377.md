# φιλόθεος

[philotheos](https://www.blueletterbible.org/lexicon/g5377)

Definition: lover of God (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5377)

[Study Light](https://www.studylight.org/lexicons/greek/5377.html)

[Bible Hub](https://biblehub.com/str/greek/5377.htm)

[LSJ](https://lsj.gr/wiki/φιλόθεος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5377)

[Bible Bento](https://biblebento.com/dictionary/G5377.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5377/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5377.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5377)
