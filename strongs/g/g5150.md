# τρίμηνος

[trimēnos](https://www.blueletterbible.org/lexicon/g5150)

Definition: three months (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5150)

[Study Light](https://www.studylight.org/lexicons/greek/5150.html)

[Bible Hub](https://biblehub.com/str/greek/5150.htm)

[LSJ](https://lsj.gr/wiki/τρίμηνος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5150)

[Bible Bento](https://biblebento.com/dictionary/G5150.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5150/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5150.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5150)

