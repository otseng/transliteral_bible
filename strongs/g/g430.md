# ἀνέχω

[anechō](https://www.blueletterbible.org/lexicon/g430)

Definition: suffer (7x), bear with (4x), forbear (2x), endure (2x), hold up, sustain, put up with

Part of speech: verb

Occurs 17 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0430)

[Study Light](https://www.studylight.org/lexicons/greek/430.html)

[Bible Hub](https://biblehub.com/str/greek/430.htm)

[LSJ](https://lsj.gr/wiki/ἀνέχω)

[NET Bible](http://classic.net.bible.org/strong.php?id=430)

[Bible Bento](https://biblebento.com/dictionary/G0430.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0430/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0430.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g430)