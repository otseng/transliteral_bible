# σπυρίς

[spyris](https://www.blueletterbible.org/lexicon/g4711)

Definition: basket (5x), reed basket, basket used to transport money

Part of speech: feminine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4711)

[Study Light](https://www.studylight.org/lexicons/greek/4711.html)

[Bible Hub](https://biblehub.com/str/greek/4711.htm)

[LSJ](https://lsj.gr/wiki/σπυρίς)

[NET Bible](http://classic.net.bible.org/strong.php?id=4711)

[Bible Bento](https://biblebento.com/dictionary/G4711.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4711/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4711.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4711)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CF%80%CF%85%CF%81%CE%AF%CF%82)