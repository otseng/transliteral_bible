# ἑκούσιος

[hekousios](https://www.blueletterbible.org/lexicon/g1595)

Definition: willingly (with G2596) (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

Greek: [hekōn](../g/g1635.md)

Hebrew: [nāḏaḇ](../h/h5068.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1595)

[Study Light](https://www.studylight.org/lexicons/greek/1595.html)

[Bible Hub](https://biblehub.com/str/greek/1595.htm)

[LSJ](https://lsj.gr/wiki/ἑκούσιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1595)

[Bible Bento](https://biblebento.com/dictionary/G1595.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1595/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1595.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1595)

