# ἐκδέχομαι

[ekdechomai](https://www.blueletterbible.org/lexicon/g1551)

Definition: wait for (3x), look for (2x), tarry for (1x), expect (1x), wait (1x).

Part of speech: verb

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1551)

[Study Light](https://www.studylight.org/lexicons/greek/1551.html)

[Bible Hub](https://biblehub.com/str/greek/1551.htm)

[LSJ](https://lsj.gr/wiki/ἐκδέχομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1551)

[Bible Bento](https://biblebento.com/dictionary/G1551.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1551/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1551.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1551)
