# κλέπτης

[kleptēs](https://www.blueletterbible.org/lexicon/g2812)

Definition: thief (16x), embezzler, deceitful person

Part of speech: masculine noun

Occurs 16 times in 16 verses

Greek: [kleptō](../g/g2813.md)

Hebrew: [gannāḇ](../h/h1590.md)

Derived words: kleptomaniac, [klepto-](https://www.thefreedictionary.com/words-that-start-with-klepto)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2812)

[Study Light](https://www.studylight.org/lexicons/greek/2812.html)

[Bible Hub](https://biblehub.com/str/greek/2812.htm)

[LSJ](https://lsj.gr/wiki/κλέπτης)

[NET Bible](http://classic.net.bible.org/strong.php?id=2812)

[Bible Bento](https://biblebento.com/dictionary/G2812.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2812/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2812.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2812)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%BB%CE%AD%CF%80%CF%84%CE%B7%CF%82)

[Precept Austin](https://www.preceptaustin.org/1thessalonians_54-5#t)