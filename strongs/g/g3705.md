# ὅραμα

[horama](https://www.blueletterbible.org/lexicon/g3705)

Definition: vision (11x), sight (1x), sight divinely granted in an ecstasy or in a sleep

Part of speech: neuter noun

Occurs 12 times in 12 verses

Hebrew: [mar'ê](../h/h4758.md)

Synonyms: [vision](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Vision)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3705)

[Study Light](https://www.studylight.org/lexicons/greek/3705.html)

[Bible Hub](https://biblehub.com/str/greek/3705.htm)

[LSJ](https://lsj.gr/wiki/ὅραμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=3705)

[Bible Bento](https://biblebento.com/dictionary/G3705.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3705/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3705.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3705)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BD%85%CF%81%CE%B1%CE%BC%CE%B1)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/3705-horama-vision.htm)
