# οἰκοδομέω

[oikodomeō](https://www.blueletterbible.org/lexicon/g3618)

Definition: build (24x), edify (7x), builder (5x), build up (1x), be in building (1x), embolden (1x), establish

Part of speech: verb

Occurs 42 times in 38 verses

Hebrew: [bānâ](../h/h1129.md), [kuwn](../h/h3559.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3618)

[Study Light](https://www.studylight.org/lexicons/greek/3618.html)

[Bible Hub](https://biblehub.com/str/greek/3618.htm)

[LSJ](https://lsj.gr/wiki/οἰκοδομέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3618)

[Bible Bento](https://biblebento.com/dictionary/G3618.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3618/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3618.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3618)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BF%E1%BC%B0%CE%BA%CE%BF%CE%B4%CE%BF%CE%BC%CE%AD%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33658)

[Precept Austin](https://www.preceptaustin.org/1thessalonians_59-11#b)