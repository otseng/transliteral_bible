# ἀνάλημψις

[analēmpsis](https://www.blueletterbible.org/lexicon/g354)

Definition: receive up (1x), ascension, taking up

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0354)

[Study Light](https://www.studylight.org/lexicons/greek/354.html)

[Bible Hub](https://biblehub.com/str/greek/354.htm)

[LSJ](https://lsj.gr/wiki/ἀνάλημψις)

[NET Bible](http://classic.net.bible.org/strong.php?id=354)

[Bible Bento](https://biblebento.com/dictionary/G0354.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0354/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0354.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g354)