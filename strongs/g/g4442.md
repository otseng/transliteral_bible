# πῦρ

[pyr](https://www.blueletterbible.org/lexicon/g4442)

Definition: fire (73x), fiery (1x)

Part of speech: neuter noun

Occurs 74 times in 73 verses

Hebrew: ['esh](../h/h784.md)

Derived words: pyrotechnic, pyre, [pyro-](https://www.thefreedictionary.com/pyro-)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4442)

[Study Light](https://www.studylight.org/lexicons/greek/4442.html)

[Bible Hub](https://biblehub.com/str/greek/4442.htm)

[LSJ](https://lsj.gr/wiki/πῦρ)

[NET Bible](http://classic.net.bible.org/strong.php?id=4442)

[Bible Bento](https://biblebento.com/dictionary/G4442.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4442/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4442.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4442)