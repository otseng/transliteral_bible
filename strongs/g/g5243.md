# ὑπερηφανία

[hyperēphania](https://www.blueletterbible.org/lexicon/g5243)

Definition: pride (1x), haughtiness, arrogance

Part of speech: feminine noun

Occurs 1 times in 1 verses

Hebrew: [gā'ôn](../h/h1347.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5243)

[Study Light](https://www.studylight.org/lexicons/greek/5243.html)

[Bible Hub](https://biblehub.com/str/greek/5243.htm)

[LSJ](https://lsj.gr/wiki/ὑπερηφανία)

[NET Bible](http://classic.net.bible.org/strong.php?id=5243)

[Bible Bento](https://biblebento.com/dictionary/G5243.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5243/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5243.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5243)