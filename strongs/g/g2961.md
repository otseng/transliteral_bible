# κυριεύω

[kyrieuō](https://www.blueletterbible.org/lexicon/g2961)

Definition: have dominion over (4x), exercise lordship over (1x), be Lord of (1x), lords (1x), to exercise influence upon, to have power over

Part of speech: verb

Occurs 9 times in 7 verses

Hebrew: [radah](../h/h7287.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2961)

[Study Light](https://www.studylight.org/lexicons/greek/2961.html)

[Bible Hub](https://biblehub.com/str/greek/2961.htm)

[LSJ](https://lsj.gr/wiki/κυριεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2961)

[Bible Bento](https://biblebento.com/dictionary/G2961.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2961/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2961.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2961)
