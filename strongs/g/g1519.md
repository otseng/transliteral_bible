# εἰς

[eis](https://www.blueletterbible.org/lexicon/g1519)

Definition: into (573x), to (281x), unto (207x), for (140x), in (138x), on (58x), toward (29x), against (26x)

Part of speech: preposition

Occurs 1,775 times in 1,513 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1519)

[Study Light](https://www.studylight.org/lexicons/greek/1519.html)

[Bible Hub](https://biblehub.com/str/greek/1519.htm)

[LSJ](https://lsj.gr/wiki/εἰς)

[NET Bible](http://classic.net.bible.org/strong.php?id=1519)

[Bible Bento](https://biblebento.com/dictionary/G1519.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1519/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1519.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1519)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B5%E1%BC%B0%CF%82)
