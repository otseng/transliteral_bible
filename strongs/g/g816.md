# ἀτενίζω

[atenizō](https://www.blueletterbible.org/lexicon/g816)

Definition: look steadfastly (2x), behold steadfastly (2x), fasten (one's) eyes (2x), look earnestly on (1x), look earnestly upon (1x), look up steadfastly (1x), behold earnestly (1x), gaze upon

Part of speech: verb

Occurs 15 times in 14 verses

Synonyms: [look](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Look)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0816)

[Study Light](https://www.studylight.org/lexicons/greek/816.html)

[Bible Hub](https://biblehub.com/str/greek/816.htm)

[LSJ](https://lsj.gr/wiki/ἀτενίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=816)

[Bible Bento](https://biblebento.com/dictionary/G0816.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0816/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0816.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g816)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CF%84%CE%B5%CE%BD%CE%AF%CE%B6%CF%89)