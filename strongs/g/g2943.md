# κυκλόθεν

[kyklothen](https://www.blueletterbible.org/lexicon/g2943)

Definition: round about (3x), about (1x).

Part of speech: adverb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2943)

[Study Light](https://www.studylight.org/lexicons/greek/2943.html)

[Bible Hub](https://biblehub.com/str/greek/2943.htm)

[LSJ](https://lsj.gr/wiki/κυκλόθεν)

[NET Bible](http://classic.net.bible.org/strong.php?id=2943)

[Bible Bento](https://biblebento.com/dictionary/G2943.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2943/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2943.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2943)

