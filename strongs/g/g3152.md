# μάταιος

[mataios](https://www.blueletterbible.org/lexicon/g3152)

Definition: vain (5x), vanities (1x), idle, devoid of force, truth, success, result; useless, of no purpose

Part of speech: adjective

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3152)

[Study Light](https://www.studylight.org/lexicons/greek/3152.html)

[Bible Hub](https://biblehub.com/str/greek/3152.htm)

[LSJ](https://lsj.gr/wiki/μάταιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3152)

[Bible Bento](https://biblebento.com/dictionary/G3152.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3152/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3152.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3152)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BC%CE%AC%CF%84%CE%B1%CE%B9%CE%BF%CF%82)
