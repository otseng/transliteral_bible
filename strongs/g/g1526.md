# εἰσί

[eisi](https://www.blueletterbible.org/lexicon/g1526)

Definition: are (135x), be (14x), were (7x), have (2x), not translated (1x), miscellaneous (4x).

Part of speech: verb

Occurs 163 times in 146 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1526)

[Study Light](https://www.studylight.org/lexicons/greek/1526.html)

[Bible Hub](https://biblehub.com/str/greek/1526.htm)

[LSJ](https://lsj.gr/wiki/εἰσί)

[NET Bible](http://classic.net.bible.org/strong.php?id=1526)

[Bible Bento](https://biblebento.com/dictionary/G1526.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1526/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1526.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1526)

