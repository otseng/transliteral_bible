# φιλάδελφος

[philadelphos](https://www.blueletterbible.org/lexicon/g5361)

Definition: love as brethren (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5361)

[Study Light](https://www.studylight.org/lexicons/greek/5361.html)

[Bible Hub](https://biblehub.com/str/greek/5361.htm)

[LSJ](https://lsj.gr/wiki/φιλάδελφος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5361)

[Bible Bento](https://biblebento.com/dictionary/G5361.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5361/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5361.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5361)

