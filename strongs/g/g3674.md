# ὁμοῦ

[homou](https://www.blueletterbible.org/lexicon/g3674)

Definition: together (3x), same place and time

Occurs 3 times in 3 verses

Derived words: homogenous

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3674)

[Study Light](https://www.studylight.org/lexicons/greek/3674.html)

[Bible Hub](https://biblehub.com/str/greek/3674.htm)

[LSJ](https://lsj.gr/wiki/ὁμοῦ)

[NET Bible](http://classic.net.bible.org/strong.php?id=3674)

[Bible Bento](https://biblebento.com/dictionary/G3674.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3674/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3674.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3674)