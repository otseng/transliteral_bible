# πρύμνα

[prymna](https://www.blueletterbible.org/lexicon/g4403)

Definition: hinder part of ship (1x), stern (1x), hinder part (1x), stern, back of the ship

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4403)

[Study Light](https://www.studylight.org/lexicons/greek/4403.html)

[Bible Hub](https://biblehub.com/str/greek/4403.htm)

[LSJ](https://lsj.gr/wiki/πρύμνα)

[NET Bible](http://classic.net.bible.org/strong.php?id=4403)

[Bible Bento](https://biblebento.com/dictionary/G4403.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4403/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4403.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4403)