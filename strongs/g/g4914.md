# συνήθεια

[synētheia](https://www.blueletterbible.org/lexicon/g4914)

Definition: custom (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4914)

[Study Light](https://www.studylight.org/lexicons/greek/4914.html)

[Bible Hub](https://biblehub.com/str/greek/4914.htm)

[LSJ](https://lsj.gr/wiki/συνήθεια)

[NET Bible](http://classic.net.bible.org/strong.php?id=4914)

[Bible Bento](https://biblebento.com/dictionary/G4914.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4914/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4914.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4914)
