# Ἰούδας

[Ioudas](https://www.blueletterbible.org/lexicon/g2455)

Definition: Judas (Iscariot) (22x), Juda (Son of Jacob) (7x), Judah (Son of Jacob) (1x), Judas (Son of Jacob) (2x), Judas (Brother of James) (3x), Jude (Brother of James) (1x), Judas Barsabas (3x), Juda (Ancestors of Jesus) (2x), "he shall be praised"

Part of speech: masculine noun

Occurs 42 times in 41 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2455)

[Study Light](https://www.studylight.org/lexicons/greek/2455.html)

[Bible Hub](https://biblehub.com/str/greek/2455.htm)

[LSJ](https://lsj.gr/wiki/Ἰούδας)

[NET Bible](http://classic.net.bible.org/strong.php?id=2455)

[Bible Bento](https://biblebento.com/dictionary/G2455.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2455/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2455.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2455)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%B8%CE%BF%CF%8D%CE%B4%CE%B1%CF%82)