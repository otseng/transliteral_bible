# κρύσταλλος

[krystallos](https://www.blueletterbible.org/lexicon/g2930)

Definition: crystal (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2930)

[Study Light](https://www.studylight.org/lexicons/greek/2930.html)

[Bible Hub](https://biblehub.com/str/greek/2930.htm)

[LSJ](https://lsj.gr/wiki/κρύσταλλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2930)

[Bible Bento](https://biblebento.com/dictionary/G2930.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2930/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2930.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2930)

