# ὅσιος

[hosios](https://www.blueletterbible.org/lexicon/g3741)

Definition: holy (4x), Holy One (2x), mercies (1x), shall be (1x), undefiled by sin, free from wickedness, religiously observing every moral obligation, pure holy, pious, sacred, hallowed, sinless, devout

Part of speech: adjective

Occurs 8 times in 8 verses

Hebrew: [chaciyd](../h/h2623.md)

Synonyms: [holy](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Holy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3741)

[Study Light](https://www.studylight.org/lexicons/greek/3741.html)

[Bible Hub](https://biblehub.com/str/greek/3741.htm)

[LSJ](https://lsj.gr/wiki/ὅσιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3741)

[Bible Bento](https://biblebento.com/dictionary/G3741.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3741/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3741.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3741)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BD%85%CF%83%CE%B9%CE%BF%CF%82)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33867)

[Precept Austin](https://www.preceptaustin.org/1thessalonians_210-12#devoutly)
