# στενός

[stenos](https://www.blueletterbible.org/lexicon/g4728)

Definition: strait (3x), narrow, tight

Part of speech: adjective

Occurs 3 times in 3 verses

Derived words: stenography

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4728)

[Study Light](https://www.studylight.org/lexicons/greek/4728.html)

[Bible Hub](https://biblehub.com/str/greek/4728.htm)

[LSJ](https://lsj.gr/wiki/στενός)

[NET Bible](http://classic.net.bible.org/strong.php?id=4728)

[Bible Bento](https://biblebento.com/dictionary/G4728.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4728/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4728.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4728)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CF%84%CE%B5%CE%BD%CF%8C%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Steno)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34486)

[Precept Austin](https://www.preceptaustin.org/luke_1322-30_commentary#na)