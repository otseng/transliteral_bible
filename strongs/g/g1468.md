# ἐγκρατής

[egkratēs](https://www.blueletterbible.org/lexicon/g1468)

Definition: temperate (1x), strong, robust, self-controlled

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1468)

[Study Light](https://www.studylight.org/lexicons/greek/1468.html)

[Bible Hub](https://biblehub.com/str/greek/1468.htm)

[LSJ](https://lsj.gr/wiki/ἐγκρατής)

[NET Bible](http://classic.net.bible.org/strong.php?id=1468)

[Bible Bento](https://biblebento.com/dictionary/G1468.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1468/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1468.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1468)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%90%CE%B3%CE%BA%CF%81%CE%B1%CF%84%CE%AE%CF%82)
