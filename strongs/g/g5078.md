# τέχνη

[technē](https://www.blueletterbible.org/lexicon/g5078)

Definition: art (1x), occupation (1x), craft (1x), trade

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5078)

[Study Light](https://www.studylight.org/lexicons/greek/5078.html)

[Bible Hub](https://biblehub.com/str/greek/5078.htm)

[LSJ](https://lsj.gr/wiki/τέχνη)

[NET Bible](http://classic.net.bible.org/strong.php?id=5078)

[Bible Bento](https://biblebento.com/dictionary/G5078.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5078/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5078.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5078)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%84%CE%AD%CF%87%CE%BD%CE%B7)
