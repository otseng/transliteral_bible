# πολύσπλαγχνος

[polysplagchnos](https://www.blueletterbible.org/lexicon/g4184)

Definition: very pitiful (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4184)

[Study Light](https://www.studylight.org/lexicons/greek/4184.html)

[Bible Hub](https://biblehub.com/str/greek/4184.htm)

[LSJ](https://lsj.gr/wiki/πολύσπλαγχνος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4184)

[Bible Bento](https://biblebento.com/dictionary/G4184.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4184/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4184.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4184)

