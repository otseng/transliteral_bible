# δικαιοκρισία

[dikaiokrisia](https://www.blueletterbible.org/lexicon/g1341)

Definition: righteous judgment (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1341)

[Study Light](https://www.studylight.org/lexicons/greek/1341.html)

[Bible Hub](https://biblehub.com/str/greek/1341.htm)

[LSJ](https://lsj.gr/wiki/δικαιοκρισία)

[NET Bible](http://classic.net.bible.org/strong.php?id=1341)

[Bible Bento](https://biblebento.com/dictionary/G1341.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1341/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1341.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1341)
