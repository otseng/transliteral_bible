# φρόνιμος

[phronimos](https://www.blueletterbible.org/lexicon/g5429)

Definition: wise (14x), intelligent, prudent, thoughtful

Part of speech: adjective

Occurs 14 times in 14 verses

Hebrew: [tāḇûn](../h/h8394.md)

Synonyms: [wise](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Wise)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5429)

[Study Light](https://www.studylight.org/lexicons/greek/5429.html)

[Bible Hub](https://biblehub.com/str/greek/5429.htm)

[LSJ](https://lsj.gr/wiki/φρόνιμος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5429)

[Bible Bento](https://biblebento.com/dictionary/G5429.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5429/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5429.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5429)