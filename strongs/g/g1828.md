# ἐξέλκω

[exelkō](https://www.blueletterbible.org/lexicon/g1828)

Definition: draw away (1x), in hunting and fishing as game is lured from its hiding place, so man by lure is allured from the safety of self-restraint to sin

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1828)

[Study Light](https://www.studylight.org/lexicons/greek/1828.html)

[Bible Hub](https://biblehub.com/str/greek/1828.htm)

[LSJ](https://lsj.gr/wiki/ἐξέλκω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1828)

[Bible Bento](https://biblebento.com/dictionary/G1828.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1828/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1828.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1828)

