# ἄκανθα

[akantha](https://www.blueletterbible.org/lexicon/g173)

Definition: thorns (14x), bramble, prickle

Part of speech: feminine noun

Occurs 14 times in 11 verses

Hebrew: [qowts](../h/h6975.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0173)

[Study Light](https://www.studylight.org/lexicons/greek/173.html)

[Bible Hub](https://biblehub.com/str/greek/173.htm)

[LSJ](https://lsj.gr/wiki/ἄκανθα)

[NET Bible](http://classic.net.bible.org/strong.php?id=173)

[Bible Bento](https://biblebento.com/dictionary/G0173.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0173/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0173.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g173)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%84%CE%BA%CE%B1%CE%BD%CE%B8%CE%B1)

[Wikipedia](https://en.wikipedia.org/wiki/Acantha)