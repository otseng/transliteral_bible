# λουτρόν

[loutron](https://www.blueletterbible.org/lexicon/g3067)

Definition: washing (2x), bathing, bath, the act of bathing

Part of speech: neuter noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3067)

[Study Light](https://www.studylight.org/lexicons/greek/3067.html)

[Bible Hub](https://biblehub.com/str/greek/3067.htm)

[LSJ](https://lsj.gr/wiki/λουτρόν)

[NET Bible](http://classic.net.bible.org/strong.php?id=3067)

[Bible Bento](https://biblebento.com/dictionary/G3067.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3067/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3067.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3067)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BB%CE%BF%CF%85%CF%84%CF%81%CF%8C%CE%BD)
