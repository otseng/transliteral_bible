# ἐφάπαξ

[ephapax](https://www.blueletterbible.org/lexicon/g2178)

Definition: once (5x).

Part of speech: adverb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2178)

[Study Light](https://www.studylight.org/lexicons/greek/2178.html)

[Bible Hub](https://biblehub.com/str/greek/2178.htm)

[LSJ](https://lsj.gr/wiki/ἐφάπαξ)

[NET Bible](http://classic.net.bible.org/strong.php?id=2178)

[Bible Bento](https://biblebento.com/dictionary/G2178.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2178/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2178.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2178)

