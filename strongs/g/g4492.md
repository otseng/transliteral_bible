# ῥιζόω

[rhizoō](https://www.blueletterbible.org/lexicon/g4492)

Definition: root (2x), become stable

Part of speech: verb

Occurs 2 times in 2 verses

Derived words: [rhizome](https://www.merriam-webster.com/dictionary/rhizome)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4492)

[Study Light](https://www.studylight.org/lexicons/greek/4492.html)

[Bible Hub](https://biblehub.com/str/greek/4492.htm)

[LSJ](https://lsj.gr/wiki/ῥιζόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4492)

[Bible Bento](https://biblebento.com/dictionary/G4492.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4492/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4492.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4492)

[Sermon Index](https://www.sermonindex.net/modules/articles/index.php?view=article&aid=35448)
