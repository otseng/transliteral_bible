# Νικόπολις

[nikopolis](https://www.blueletterbible.org/lexicon/g3533)

Definition: Nicopolis (2x), "city of victory", 

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3533)

[Study Light](https://www.studylight.org/lexicons/greek/3533.html)

[Bible Hub](https://biblehub.com/str/greek/3533.htm)

[LSJ](https://lsj.gr/wiki/Νικόπολις)

[NET Bible](http://classic.net.bible.org/strong.php?id=3533)

[Bible Bento](https://biblebento.com/dictionary/G3533.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3533/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3533.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3533)

[Wikipedia](https://en.wikipedia.org/wiki/Nicopolis)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/nicopolis.html)

[Video Bible](https://www.videobible.com/bible-dictionary/nicopolis)