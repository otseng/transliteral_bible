# παρανομέω

[paranomeō](https://www.blueletterbible.org/lexicon/g3891)

Definition: contrary to the law (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3891)

[Study Light](https://www.studylight.org/lexicons/greek/3891.html)

[Bible Hub](https://biblehub.com/str/greek/3891.htm)

[LSJ](https://lsj.gr/wiki/παρανομέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3891)

[Bible Bento](https://biblebento.com/dictionary/G3891.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3891/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3891.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3891)
