# σχίζω

[schizō](https://www.blueletterbible.org/lexicon/g4977)

Definition: rend (5x), divide (2x), open (1x), break (1x), make a rent (1x), rend, split

Part of speech: verb

Occurs 11 times in 9 verses

Derived words: schizophrenia, [schizo-](https://en.wiktionary.org/wiki/Category:English_words_prefixed_with_schizo-)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4977)

[Study Light](https://www.studylight.org/lexicons/greek/4977.html)

[Bible Hub](https://biblehub.com/str/greek/4977.htm)

[LSJ](https://lsj.gr/wiki/σχίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4977)

[Bible Bento](https://biblebento.com/dictionary/G4977.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4977/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4977.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4977)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CF%87%CE%AF%CE%B6%CF%89)