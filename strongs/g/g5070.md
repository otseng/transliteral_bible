# τετρακισχίλιοι

[tetrakischilioi](https://www.blueletterbible.org/lexicon/g5070)

Definition: four thousand (5x).

Part of speech: adjective

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5070)

[Study Light](https://www.studylight.org/lexicons/greek/5070.html)

[Bible Hub](https://biblehub.com/str/greek/5070.htm)

[LSJ](https://lsj.gr/wiki/τετρακισχίλιοι)

[NET Bible](http://classic.net.bible.org/strong.php?id=5070)

[Bible Bento](https://biblebento.com/dictionary/G5070.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5070/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5070.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5070)

