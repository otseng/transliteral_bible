# διακωλύω

[diakōlyō](https://www.blueletterbible.org/lexicon/g1254)

Definition: forbid (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1254)

[Study Light](https://www.studylight.org/lexicons/greek/1254.html)

[Bible Hub](https://biblehub.com/str/greek/1254.htm)

[LSJ](https://lsj.gr/wiki/διακωλύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1254)

[Bible Bento](https://biblebento.com/dictionary/G1254.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1254/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1254.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1254)

