# Χριστιανός

[Christianos](https://www.blueletterbible.org/lexicon/g5546)

Definition: Christian (3x)

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5546)

[Study Light](https://www.studylight.org/lexicons/greek/5546.html)

[Bible Hub](https://biblehub.com/str/greek/5546.htm)

[LSJ](https://lsj.gr/wiki/Χριστιανός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5546)

[Bible Bento](https://biblebento.com/dictionary/G5546.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5546/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5546.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5546)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%87%CF%81%CE%B9%CF%83%CF%84%CE%B9%CE%B1%CE%BD%CF%8C%CF%82)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33701)

[Precept Austin](https://www.preceptaustin.org/1peter_verse_by_verse_410-19#christian)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/christian.html)