# ἐξαπορέω

[exaporeō](https://www.blueletterbible.org/lexicon/g1820)

Definition: despair (1x), in despair (1x), to be utterly at loss, be utterly destitute of measures or resources, to renounce all hope, be in despair

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1820)

[Study Light](https://www.studylight.org/lexicons/greek/1820.html)

[Bible Hub](https://biblehub.com/str/greek/1820.htm)

[LSJ](https://lsj.gr/wiki/ἐξαπορέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1820)

[Bible Bento](https://biblebento.com/dictionary/G1820.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1820/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1820.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1820)
