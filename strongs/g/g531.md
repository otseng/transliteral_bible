# ἀπαράβατος

[aparabatos](https://www.blueletterbible.org/lexicon/g531)

Definition: unchangeable (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0531)

[Study Light](https://www.studylight.org/lexicons/greek/531.html)

[Bible Hub](https://biblehub.com/str/greek/531.htm)

[LSJ](https://lsj.gr/wiki/ἀπαράβατος)

[NET Bible](http://classic.net.bible.org/strong.php?id=531)

[Bible Bento](https://biblebento.com/dictionary/G0531.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/531/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/531.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g531)
