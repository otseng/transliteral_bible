# προσκόπτω

[proskoptō](https://www.blueletterbible.org/lexicon/g4350)

Definition: stumble (3x), stumble at (2x), dash (2x), beat upon (1x), surge against, trip up

Part of speech: verb

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4350)

[Study Light](https://www.studylight.org/lexicons/greek/4350.html)

[Bible Hub](https://biblehub.com/str/greek/4350.htm)

[LSJ](https://lsj.gr/wiki/προσκόπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4350)

[Bible Bento](https://biblebento.com/dictionary/G4350.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4350/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4350.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4350)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=35897)