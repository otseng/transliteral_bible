# ἀνόητος

[anoētos](https://www.blueletterbible.org/lexicon/g453)

Definition: foolish (4x), fool (1x), unwise (1x), not understood, unintelligible, stupid

Part of speech: adjective

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0453)

[Study Light](https://www.studylight.org/lexicons/greek/453.html)

[Bible Hub](https://biblehub.com/str/greek/453.htm)

[LSJ](https://lsj.gr/wiki/ἀνόητος)

[NET Bible](http://classic.net.bible.org/strong.php?id=453)

[Bible Bento](https://biblebento.com/dictionary/G0453.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0453/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0453.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g453)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CE%BD%CF%8C%CE%B7%CF%84%CE%BF%CF%82)
