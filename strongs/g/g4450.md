# πυρρός

[pyrros](https://www.blueletterbible.org/lexicon/g4450)

Definition: red (2x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4450)

[Study Light](https://www.studylight.org/lexicons/greek/4450.html)

[Bible Hub](https://biblehub.com/str/greek/4450.htm)

[LSJ](https://lsj.gr/wiki/πυρρός)

[NET Bible](http://classic.net.bible.org/strong.php?id=4450)

[Bible Bento](https://biblebento.com/dictionary/G4450.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4450/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4450.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4450)

