# φάντασμα

[phantasma](https://www.blueletterbible.org/lexicon/g5326)

Definition: spirit (2x), apparition, appearance

Part of speech: neuter noun

Occurs 2 times in 2 verses

Derived words: phantom

Hebrew: [ḥizzāyôn](../h/h2384.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5326)

[Study Light](https://www.studylight.org/lexicons/greek/5326.html)

[Bible Hub](https://biblehub.com/str/greek/5326.htm)

[LSJ](https://lsj.gr/wiki/φάντασμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=5326)

[Bible Bento](https://biblebento.com/dictionary/G5326.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5326/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5326.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5326)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%86%CE%AC%CE%BD%CF%84%CE%B1%CF%83%CE%BC%CE%B1)

[Wikipedia](https://en.wikipedia.org/wiki/Phantasma)