# ἐνθυμέομαι

[enthymeomai](https://www.blueletterbible.org/lexicon/g1760)

Definition: think (3x), to bring to mind, revolve in mind, ponder, to think, to deliberate, to be inspirited

Part of speech: verb

Occurs 3 times in 3 verses

Derived words: [Enthymeme](https://en.wikipedia.org/wiki/Enthymeme)

Synonyms: [think](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Think)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1760)

[Study Light](https://www.studylight.org/lexicons/greek/1760.html)

[Bible Hub](https://biblehub.com/str/greek/1760.htm)

[LSJ](https://lsj.gr/wiki/ἐνθυμέομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1760)

[Bible Bento](https://biblebento.com/dictionary/G1760.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1760/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1760.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1760)
