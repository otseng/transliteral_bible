# ὀφειλέτης

[opheiletēs](https://www.blueletterbible.org/lexicon/g3781)

Definition: debtor (5x), sinner (1x), which owed (1x), delinquent, transgressor

Part of speech: masculine noun

Occurs 7 times in 7 verses

Greek: [opheilō](../g/g3784.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3781)

[Study Light](https://www.studylight.org/lexicons/greek/3781.html)

[Bible Hub](https://biblehub.com/str/greek/3781.htm)

[LSJ](https://lsj.gr/wiki/ὀφειλέτης)

[NET Bible](http://classic.net.bible.org/strong.php?id=3781)

[Bible Bento](https://biblebento.com/dictionary/G3781.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3781/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3781.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3781)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BD%80%CF%86%CE%B5%CE%B9%CE%BB%CE%AD%CF%84%CE%B7%CF%82)