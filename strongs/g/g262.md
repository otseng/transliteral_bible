# ἀμαράντινος

[amarantinos](https://www.blueletterbible.org/lexicon/g262)

Definition: that fadeth not away (1x), a symbol of perpetuity and immortality, a flower so called because it never withers or fades, and when plucked off revives if moistened with water

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0262)

[Study Light](https://www.studylight.org/lexicons/greek/262.html)

[Bible Hub](https://biblehub.com/str/greek/262.htm)

[LSJ](https://lsj.gr/wiki/ἀμαράντινος)

[NET Bible](http://classic.net.bible.org/strong.php?id=262)

[Bible Bento](https://biblebento.com/dictionary/G262.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/262/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/262.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g262)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CE%BC%CE%B1%CF%81%CE%AC%CE%BD%CF%84%CE%B9%CE%BD%CE%BF%CF%82)
