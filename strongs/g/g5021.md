# τάσσω

[tassō](https://www.blueletterbible.org/lexicon/g5021)

Definition: appoint (3x), ordain (2x), set (1x), determine (1x), addict (1x), to put in order, to station, to arrange soldiers, array, marshal, to fall in, form up, to order, command

Part of speech: verb

Occurs 8 times in 8 verses

Greek: [epitassō](../g/g2004.md), [antitassō](../g/g498.md)

Hebrew: [śûm](../h/h7760.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5021)

[Study Light](https://www.studylight.org/lexicons/greek/5021.html)

[Bible Hub](https://biblehub.com/str/greek/5021.htm)

[LSJ](https://lsj.gr/wiki/τάσσω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5021)

[Bible Bento](https://biblebento.com/dictionary/G5021.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5021/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5021.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5021)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%84%CE%AC%CF%83%CF%83%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33534)

[Precept Austin](https://www.preceptaustin.org/romans_13_word_studies#e)
