# διϋλίζω

[diÿlizō](https://www.blueletterbible.org/lexicon/g1368)

Definition: strain at (1x), to filter through, strain through, pour through a filter, strain out

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1368)

[Study Light](https://www.studylight.org/lexicons/greek/1368.html)

[Bible Hub](https://biblehub.com/str/greek/1368.htm)

[LSJ](https://lsj.gr/wiki/διϋλίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1368)

[Bible Bento](https://biblebento.com/dictionary/G1368.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1368/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1368.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1368)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B4%CE%B9%CF%85%CE%BB%CE%AF%CE%B6%CF%89)
