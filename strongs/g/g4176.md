# πολιτεύομαι

[politeuomai](https://www.blueletterbible.org/lexicon/g4176)

Definition: live (1x), let (one's) conversation be (1x), to administer civil affairs, manage the state, to behave as a citizen, conduct yourselves

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4176)

[Study Light](https://www.studylight.org/lexicons/greek/4176.html)

[Bible Hub](https://biblehub.com/str/greek/4176.htm)

[LSJ](https://lsj.gr/wiki/πολιτεύομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4176)

[Bible Bento](https://biblebento.com/dictionary/G4176.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4176/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4176.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4176)

[Precept Austin](https://www.preceptaustin.org/philippians_127-28#conduct%204176%20politeuomai)
