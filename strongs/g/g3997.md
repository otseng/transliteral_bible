# πένθος

[penthos](https://www.blueletterbible.org/lexicon/g3997)

Definition: sorrow (3x), mourning (2x).

Part of speech: neuter noun

Occurs 5 times in 4 verses

Hebrew: [mispēḏ](../h/h4553.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3997)

[Study Light](https://www.studylight.org/lexicons/greek/3997.html)

[Bible Hub](https://biblehub.com/str/greek/3997.htm)

[LSJ](https://lsj.gr/wiki/πένθος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3997)

[Bible Bento](https://biblebento.com/dictionary/G3997.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3997/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3997.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3997)
