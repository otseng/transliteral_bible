# ἀνάπηρος

[anapēros](https://www.blueletterbible.org/lexicon/g376)

Definition: maimed (2x), crippled, disabled in the limbs

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0376)

[Study Light](https://www.studylight.org/lexicons/greek/376.html)

[Bible Hub](https://biblehub.com/str/greek/376.htm)

[LSJ](https://lsj.gr/wiki/ἀνάπηρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=376)

[Bible Bento](https://biblebento.com/dictionary/G0376.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0376/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0376.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g376)