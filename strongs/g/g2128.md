# εὐλογητός

[eulogētos](https://www.blueletterbible.org/lexicon/g2128)

Definition: blessed (said of God) (8x), praised

Part of speech: adjective

Occurs 8 times in 8 verses

Synonyms: [blessed](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Blessed)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2128)

[Study Light](https://www.studylight.org/lexicons/greek/2128.html)

[Bible Hub](https://biblehub.com/str/greek/2128.htm)

[LSJ](https://lsj.gr/wiki/εὐλογητός)

[NET Bible](http://classic.net.bible.org/strong.php?id=2128)

[Bible Bento](https://biblebento.com/dictionary/G2128.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2128/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2128.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2128)