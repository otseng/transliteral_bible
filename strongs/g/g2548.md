# κἀκεῖνος

[kakeinos](https://www.blueletterbible.org/lexicon/g2548)

Definition: and he (4x), and they (3x), he also (3x), and them (2x), and the other (2x), and him (2x), they also (2x), him also (1x), miscellaneous (4x).

Part of speech: conjunction

Occurs 23 times in 23 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2548)

[Study Light](https://www.studylight.org/lexicons/greek/2548.html)

[Bible Hub](https://biblehub.com/str/greek/2548.htm)

[LSJ](https://lsj.gr/wiki/κἀκεῖνος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2548)

[Bible Bento](https://biblebento.com/dictionary/G2548.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2548/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2548.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2548)

