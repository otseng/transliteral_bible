# πεντακόσιοι

[pentakosioi](https://www.blueletterbible.org/lexicon/g4001)

Definition: five hundred (2x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4001)

[Study Light](https://www.studylight.org/lexicons/greek/4001.html)

[Bible Hub](https://biblehub.com/str/greek/4001.htm)

[LSJ](https://lsj.gr/wiki/πεντακόσιοι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4001)

[Bible Bento](https://biblebento.com/dictionary/G4001.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4001/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4001.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4001)

