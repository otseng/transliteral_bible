# ἐλέγχω

[elegchō](https://www.blueletterbible.org/lexicon/g1651)

Definition: reprove (6x), rebuke (5x), convince (4x), tell (one's) fault (1x), convict (1x), convict, correct, admonish, question, test, audit

Part of speech: verb

Occurs 18 times in 17 verses

Greek: [elegchos](../g/g1650.md)

Hebrew: [yakach](../h/h3198.md), [gāʿar](../h/h1605.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1651)

[Study Light](https://www.studylight.org/lexicons/greek/1651.html)

[Bible Hub](https://biblehub.com/str/greek/1651.htm)

[LSJ](https://lsj.gr/wiki/ἐλέγχω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1651)

[Bible Bento](https://biblebento.com/dictionary/G1651.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1651/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1651.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1651)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B5%CE%BB%CE%AD%CE%B3%CF%87%CF%89)