# χαρίζομαι

[charizomai](https://www.blueletterbible.org/lexicon/g5483)

Definition: forgive (11x), give (6x), freely give (2x), deliver (2x), grant (1x), frankly forgive (1x), pardon, deliver, freely give

Part of speech: verb

Occurs 24 times in 19 verses

Greek: [charis](../g/g5485.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5483)

[Study Light](https://www.studylight.org/lexicons/greek/5483.html)

[Bible Hub](https://biblehub.com/str/greek/5483.htm)

[LSJ](https://lsj.gr/wiki/χαρίζομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=5483)

[Bible Bento](https://biblebento.com/dictionary/G5483.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5483/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5483.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5483)

[Greek word studies](http://greekwordstudies.blogspot.com/2007/04/forgive.html)

[Internet Bible College](http://internetbiblecollege.net/lessons/hebrew%20and%20greek%20words%20for%20forgiveness%20or%20pardon.htm)

[Precious Seed](https://www.preciousseed.org/article_detail.cfm?articleID=2039)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34100)

[Ministry magazine](https://www.ministrymagazine.org/archive/1963/07/forgiveness-in-the-new-testament)