# ἐπέκεινα

[epekeina](https://www.blueletterbible.org/lexicon/g1900)

Definition: beyond (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1900)

[Study Light](https://www.studylight.org/lexicons/greek/1900.html)

[Bible Hub](https://biblehub.com/str/greek/1900.htm)

[LSJ](https://lsj.gr/wiki/ἐπέκεινα)

[NET Bible](http://classic.net.bible.org/strong.php?id=1900)

[Bible Bento](https://biblebento.com/dictionary/G1900.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1900/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1900.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1900)

