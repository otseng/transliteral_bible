# φρουρέω

[phroureō](https://www.blueletterbible.org/lexicon/g5432)

Definition: keep (3x), keep with a garrison (1x), to mount guard as a sentinel (post spies at gates); figuratively, to hem in, protect

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5432)

[Study Light](https://www.studylight.org/lexicons/greek/5432.html)

[Bible Hub](https://biblehub.com/str/greek/5432.htm)

[LSJ](https://lsj.gr/wiki/φρουρέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5432)

[Bible Bento](https://biblebento.com/dictionary/G5432.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5432/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5432.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5432)
