# δικαίωσις

[dikaiōsis](https://www.blueletterbible.org/lexicon/g1347)

Definition: justification (2x), the act of God declaring men free from guilt and acceptable to him

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1347)

[Study Light](https://www.studylight.org/lexicons/greek/1347.html)

[Bible Hub](https://biblehub.com/str/greek/1347.htm)

[LSJ](https://lsj.gr/wiki/δικαίωσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=1347)

[Bible Bento](https://biblebento.com/dictionary/G1347.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1347/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1347.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1347)
