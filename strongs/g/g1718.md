# ἐμφανίζω

[emphanizō](https://www.blueletterbible.org/lexicon/g1718)

Definition: inform (3x), be manifest (2x), appear (2x), signify (1x), show (1x), declare plainly (1x), manifest, disclose, make known, make clear

Part of speech: verb

Occurs 10 times in 10 verses

Synonyms: [manifest](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Manifest)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1718)

[Study Light](https://www.studylight.org/lexicons/greek/1718.html)

[Bible Hub](https://biblehub.com/str/greek/1718.htm)

[LSJ](https://lsj.gr/wiki/ἐμφανίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1718)

[Bible Bento](https://biblebento.com/dictionary/G1718.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1718/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1718.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1718)

[Wiktionary](https://www.preceptaustin.org/hebrews_1113-16#mc)