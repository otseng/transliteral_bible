# ἔπαυλις

[epaulis](https://www.blueletterbible.org/lexicon/g1886)

Definition: habitation (1x), dwelling

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1886)

[Study Light](https://www.studylight.org/lexicons/greek/1886.html)

[Bible Hub](https://biblehub.com/str/greek/1886.htm)

[LSJ](https://lsj.gr/wiki/ἔπαυλις)

[NET Bible](http://classic.net.bible.org/strong.php?id=1886)

[Bible Bento](https://biblebento.com/dictionary/G1886.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1886/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1886.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1886)
