# φέγγος

[pheggos](https://www.blueletterbible.org/lexicon/g5338)

Definition: light (3x)

Part of speech: neuter noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5338)

[Study Light](https://www.studylight.org/lexicons/greek/5338.html)

[Bible Hub](https://biblehub.com/str/greek/5338.htm)

[LSJ](https://lsj.gr/wiki/φέγγος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5338)

[Bible Bento](https://biblebento.com/dictionary/G5338.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5338/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5338.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5338)