# Ἰεριχώ

[Ierichō](https://www.blueletterbible.org/lexicon/g2410)

Definition: Jericho (7x), "place of fragrance", a noted city, abounding in balsam, honey, cyprus, myrobalanus, roses and other fragrant products. It was near the north shore of the Dead Sea in the tribe of Benjamin, between Jerusalem and the Jordan River

Part of speech: proper locative noun

Occurs 7 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2410)

[Study Light](https://www.studylight.org/lexicons/greek/2410.html)

[Bible Hub](https://biblehub.com/str/greek/2410.htm)

[LSJ](https://lsj.gr/wiki/Ἰεριχώ)

[NET Bible](http://classic.net.bible.org/strong.php?id=2410)

[Bible Bento](https://biblebento.com/dictionary/G2410.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2410/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2410.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2410)

[Wikipedia](https://en.wikipedia.org/wiki/Jericho)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jericho.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jericho)