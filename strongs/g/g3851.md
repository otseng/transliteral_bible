# παραβολεύομαι

[paraboleuomai](https://www.blueletterbible.org/lexicon/g3851)

Definition: regard not (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3851)

[Study Light](https://www.studylight.org/lexicons/greek/3851.html)

[Bible Hub](https://biblehub.com/str/greek/3851.htm)

[LSJ](https://lsj.gr/wiki/παραβολεύομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=3851)

[Bible Bento](https://biblebento.com/dictionary/G3851.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3851/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3851.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3851)

