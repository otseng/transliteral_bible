# παρατείνω

[parateinō](https://www.blueletterbible.org/lexicon/g3905)

Definition: continue (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3905)

[Study Light](https://www.studylight.org/lexicons/greek/3905.html)

[Bible Hub](https://biblehub.com/str/greek/3905.htm)

[LSJ](https://lsj.gr/wiki/παρατείνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3905)

[Bible Bento](https://biblebento.com/dictionary/G3905.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3905/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3905.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3905)

