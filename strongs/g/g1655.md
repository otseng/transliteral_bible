# ἐλεήμων

[eleēmōn](https://www.blueletterbible.org/lexicon/g1655)

Definition: merciful (2x), compassionate

Part of speech: adjective

Occurs 2 times in 2 verses

Hebrew: [ḥanwn](../h/h2587.md), [raḥwm](../h/h7349.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1655)

[Study Light](https://www.studylight.org/lexicons/greek/1655.html)

[Bible Hub](https://biblehub.com/str/greek/1655.htm)

[LSJ](https://lsj.gr/wiki/ἐλεήμων)

[NET Bible](http://classic.net.bible.org/strong.php?id=1655)

[Bible Bento](https://biblebento.com/dictionary/G1655.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1655/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1655.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1655)