# σκάνδαλον

[skandalon](https://www.blueletterbible.org/lexicon/g4625)

Definition: offence (9x), stumbling block (3x), occasion of stumbling (1x), occasion to fall (1x), thing that offends (1x), trap, snare

Part of speech: neuter noun

Occurs 15 times in 13 verses

Greek: [skandalizō](../g/g4624.md)

Derived words: scandal

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4625)

[Study Light](https://www.studylight.org/lexicons/greek/4625.html)

[Bible Hub](https://biblehub.com/str/greek/4625.htm)

[LSJ](https://lsj.gr/wiki/σκάνδαλον)

[NET Bible](http://classic.net.bible.org/strong.php?id=4625)

[Bible Bento](https://biblebento.com/dictionary/G4625.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4625/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4625.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4625)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34516)

[NT Greek Word Studies](http://ntgreek-wordstudies.com/skandalon.php)

[Wikipedia - stumbling block](https://en.wikipedia.org/wiki/Stumbling_block)
