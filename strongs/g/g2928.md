# κρύπτω

[kryptō](https://www.blueletterbible.org/lexicon/g2928)

Definition: hide (11x), hide (one's) self (2x), keep secret (1x), secretly (1x), hidden (1x).

Part of speech: verb

Occurs 16 times in 15 verses

Greek: [apokryptō](../g/g613.md), [kryptos](../g/g2927.md)

Hebrew: [taman](../h/h2934.md), [kāḥaḏ](../h/h3582.md), [cathar](../h/h5641.md), [chaba'](../h/h2244.md), ['alam](../h/h5956.md), [tsaphan](../h/h6845.md)

Derived words: cryptography

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2928)

[Study Light](https://www.studylight.org/lexicons/greek/2928.html)

[Bible Hub](https://biblehub.com/str/greek/2928.htm)

[LSJ](https://lsj.gr/wiki/κρύπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2928)

[Bible Bento](https://biblebento.com/dictionary/G2928.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2928/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2928.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2928)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CF%81%CF%8D%CF%80%CF%84%CF%89)