# βόρβορος

[borboros](https://www.blueletterbible.org/lexicon/g1004)

Definition: mire (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1004)

[Study Light](https://www.studylight.org/lexicons/greek/1004.html)

[Bible Hub](https://biblehub.com/str/greek/1004.htm)

[LSJ](https://lsj.gr/wiki/βόρβορος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1004)

[Bible Bento](https://biblebento.com/dictionary/G1004.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1004/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1004.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1004)

