# αὐγή

[augē](https://www.blueletterbible.org/lexicon/g827)

Definition: break of day (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0827)

[Study Light](https://www.studylight.org/lexicons/greek/827.html)

[Bible Hub](https://biblehub.com/str/greek/827.htm)

[LSJ](https://lsj.gr/wiki/αὐγή)

[NET Bible](http://classic.net.bible.org/strong.php?id=827)

[Bible Bento](https://biblebento.com/dictionary/G827.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/827/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/827.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g827)

