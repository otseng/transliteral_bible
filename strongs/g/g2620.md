# κατακαυχάομαι

[katakauchaomai](https://www.blueletterbible.org/lexicon/g2620)

Definition: boast against (1x), rejoice against (1x), glory (1x), boast (1x), to glory against, to exult over, to boast one's self to the injury

Part of speech: verb

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2620)

[Study Light](https://www.studylight.org/lexicons/greek/2620.html)

[Bible Hub](https://biblehub.com/str/greek/2620.htm)

[LSJ](https://lsj.gr/wiki/κατακαυχάομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=2620)

[Bible Bento](https://biblebento.com/dictionary/G2620.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2620/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2620.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2620)
