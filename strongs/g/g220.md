# ἀλέκτωρ

[alektōr](https://www.blueletterbible.org/lexicon/g220)

Definition: cock (12x), rooster

Part of speech: masculine noun

Occurs 12 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0220)

[Study Light](https://www.studylight.org/lexicons/greek/220.html)

[Bible Hub](https://biblehub.com/str/greek/220.htm)

[LSJ](https://lsj.gr/wiki/ἀλέκτωρ)

[NET Bible](http://classic.net.bible.org/strong.php?id=220)

[Bible Bento](https://biblebento.com/dictionary/G0220.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0220/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0220.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g220)