# ἀφεδρών

[aphedrōn](https://www.blueletterbible.org/lexicon/g856)

Definition: draught (2x), a place where the human waste discharges are dumped, toilet, latrine

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0856)

[Study Light](https://www.studylight.org/lexicons/greek/856.html)

[Bible Hub](https://biblehub.com/str/greek/856.htm)

[LSJ](https://lsj.gr/wiki/ἀφεδρών)

[NET Bible](http://classic.net.bible.org/strong.php?id=856)

[Bible Bento](https://biblebento.com/dictionary/G0856.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0856/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0856.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g856)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CF%86%CE%B5%CE%B4%CF%81%CF%8E%CE%BD)