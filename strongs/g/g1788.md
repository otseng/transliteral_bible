# ἐντρέπω

[entrepō](https://www.blueletterbible.org/lexicon/g1788)

Definition: reverence (4x), regard (2x), be ashamed (2x), shame (1x), invert, to turn about

Part of speech: verb

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1788)

[Study Light](https://www.studylight.org/lexicons/greek/1788.html)

[Bible Hub](https://biblehub.com/str/greek/1788.htm)

[LSJ](https://lsj.gr/wiki/ἐντρέπω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1788)

[Bible Bento](https://biblebento.com/dictionary/G1788.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1788/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1788.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1788)
