# συγκάμπτω

[sygkamptō](https://www.blueletterbible.org/lexicon/g4781)

Definition: bow down (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4781)

[Study Light](https://www.studylight.org/lexicons/greek/4781.html)

[Bible Hub](https://biblehub.com/str/greek/4781.htm)

[LSJ](https://lsj.gr/wiki/συγκάμπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4781)

[Bible Bento](https://biblebento.com/dictionary/G4781.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4781/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4781.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4781)
