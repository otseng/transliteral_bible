# ἐπαγγελία

[epaggelia](https://www.blueletterbible.org/lexicon/g1860)

Definition: promise (52x), message (1x), announcement, a divine assurance of good

Part of speech: feminine noun

Occurs 53 times in 51 verses

Greek: [epaggellomai](../g/g1861.md)

Hebrew: [dabar](../h/h1696.md), [pārāšâ](../h/h6575.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1860)

[Study Light](https://www.studylight.org/lexicons/greek/1860.html)

[Bible Hub](https://biblehub.com/str/greek/1860.htm)

[LSJ](https://lsj.gr/wiki/ἐπαγγελία)

[NET Bible](http://classic.net.bible.org/strong.php?id=1860)

[Bible Bento](https://biblebento.com/dictionary/G1860.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1860/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1860.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1860)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34672)

[Precept Austin](https://www.preceptaustin.org/2_peter_38-13#promise)