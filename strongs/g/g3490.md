# ναύκληρος

[nauklēros](https://www.blueletterbible.org/lexicon/g3490)

Definition: owner of a ship (1x), a ship owner, ship master, captain

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3490)

[Study Light](https://www.studylight.org/lexicons/greek/3490.html)

[Bible Hub](https://biblehub.com/str/greek/3490.htm)

[LSJ](https://lsj.gr/wiki/ναύκληρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3490)

[Bible Bento](https://biblebento.com/dictionary/G3490.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3490/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3490.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3490)
