# θλῖψις

[thlipsis](https://www.blueletterbible.org/lexicon/g2347)

Definition: tribulation (21x), affliction (17x), trouble (3x), anguish (1x), persecution (1x), burdened (1x), to be afflicted (with G1519) (1x) oppression, distress, straits

Part of speech: feminine noun

Occurs 45 times in 43 verses

Hebrew: [laḥaṣ](../h/h3906.md), ['oniy](../h/h6040.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2347)

[Study Light](https://www.studylight.org/lexicons/greek/2347.html)

[Bible Hub](https://biblehub.com/str/greek/2347.htm)

[LSJ](https://lsj.gr/wiki/θλῖψις)

[NET Bible](http://classic.net.bible.org/strong.php?id=2347)

[Bible Bento](https://biblebento.com/dictionary/G2347.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2347/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2347.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2347)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B8%CE%BB%E1%BF%96%CF%88%CE%B9%CF%82)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33905)

[Precept Austin](https://www.preceptaustin.org/index.php/tribulation-thlipsis_greek_word_study)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/2347-thlipsis-tribulation.htm)
