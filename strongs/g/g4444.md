# πύργος

[pyrgos](https://www.blueletterbible.org/lexicon/g4444)

Definition: tower (4x), a fortified structure rising to a considerable height, to repel a hostile attack or to enable a watchman to see in every direction, castle, palace, fortress

Part of speech: masculine noun

Occurs 4 times in 4 verses

Hebrew: [mᵊṣûrâ](../h/h4694.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4444)

[Study Light](https://www.studylight.org/lexicons/greek/4444.html)

[Bible Hub](https://biblehub.com/str/greek/4444.htm)

[LSJ](https://lsj.gr/wiki/πύργος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4444)

[Bible Bento](https://biblebento.com/dictionary/G4444.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4444/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4444.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4444)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CF%8D%CF%81%CE%B3%CE%BF%CF%82)
