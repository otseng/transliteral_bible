# φωλεός

[phōleos](https://www.blueletterbible.org/lexicon/g5454)

Definition: hole (2x), lair, burrow, den

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5454)

[Study Light](https://www.studylight.org/lexicons/greek/5454.html)

[Bible Hub](https://biblehub.com/str/greek/5454.htm)

[LSJ](https://lsj.gr/wiki/φωλεός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5454)

[Bible Bento](https://biblebento.com/dictionary/G5454.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5454/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5454.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5454)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%86%CF%89%CE%BB%CE%B5%CF%8C%CF%82)