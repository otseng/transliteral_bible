# σύσσωμος

[syssōmos](https://www.blueletterbible.org/lexicon/g4954)

Definition: of the same body (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4954)

[Study Light](https://www.studylight.org/lexicons/greek/4954.html)

[Bible Hub](https://biblehub.com/str/greek/4954.htm)

[LSJ](https://lsj.gr/wiki/σύσσωμος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4954)

[Bible Bento](https://biblebento.com/dictionary/G4954.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4954/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4954.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4954)
