# εἰσακούω

[eisakouō](https://www.blueletterbible.org/lexicon/g1522)

Definition: hear (5x), to give heed to, comply with admonition, to obey, to listen to, assent to, a request, to be heard, have request granted

Part of speech: verb

Occurs 6 times in 5 verses

Synonyms: [hear](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Hear)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1522)

[Study Light](https://www.studylight.org/lexicons/greek/1522.html)

[Bible Hub](https://biblehub.com/str/greek/1522.htm)

[LSJ](https://lsj.gr/wiki/εἰσακούω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1522)

[Bible Bento](https://biblebento.com/dictionary/G1522.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1522/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1522.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1522)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B5%CE%B9%CF%83%CE%B1%CE%BA%CE%BF%CF%8D%CF%89)
