# αὐτόφωρος

[autophōros](https://www.blueletterbible.org/lexicon/g1888)

Definition: in the very act (thief) (1x)

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1888)

[Study Light](https://www.studylight.org/lexicons/greek/1888.html)

[Bible Hub](https://biblehub.com/str/greek/1888.htm)

[LSJ](https://lsj.gr/wiki/αὐτόφωρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1888)

[Bible Bento](https://biblebento.com/dictionary/G1888.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1888/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1888.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1888)