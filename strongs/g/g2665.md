# καταπέτασμα

[katapetasma](https://www.blueletterbible.org/lexicon/g2665)

Definition: veil (6x), curtain, the name given to the two curtains in the temple at Jerusalem, one of them at the entrance to the temple separated the Holy Place from the outer court, the other veiled the Holy of Holies from the Holy Place

Part of speech: neuter noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2665)

[Study Light](https://www.studylight.org/lexicons/greek/2665.html)

[Bible Hub](https://biblehub.com/str/greek/2665.htm)

[LSJ](https://lsj.gr/wiki/καταπέτασμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=2665)

[Bible Bento](https://biblebento.com/dictionary/G2665.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2665/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2665.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2665)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=36040)