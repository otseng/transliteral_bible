# Πόρκιος

[Porkios](https://www.blueletterbible.org/lexicon/g4201)

Definition: Porcius (1x), "swinish"

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4201)

[Study Light](https://www.studylight.org/lexicons/greek/4201.html)

[Bible Hub](https://biblehub.com/str/greek/4201.htm)

[LSJ](https://lsj.gr/wiki/Πόρκιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4201)

[Bible Bento](https://biblebento.com/dictionary/G4201.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4201/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4201.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4201)
