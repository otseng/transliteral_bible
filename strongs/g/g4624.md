# σκανδαλίζω

[skandalizō](https://www.blueletterbible.org/lexicon/g4624)

Definition: offend (28x), make to offend (2x), trip up, snare

Part of speech: verb

Occurs 35 times in 28 verses

Greek: [skandalon](../g/g4625.md)

Derived words: scandal, scandalous

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4624)

[Study Light](https://www.studylight.org/lexicons/greek/4624.html)

[Bible Hub](https://biblehub.com/str/greek/4624.htm)

[LSJ](https://lsj.gr/wiki/σκανδαλίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4624)

[Bible Bento](https://biblebento.com/dictionary/G4624.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4624/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4624.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4624)

[Wikipedia](https://en.wikipedia.org/wiki/Stumbling_block)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CE%BA%CE%B1%CE%BD%CE%B4%CE%B1%CE%BB%CE%AF%CE%B6%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34032)

[Precept Austin](https://www.preceptaustin.org/matthew_529-30#s)