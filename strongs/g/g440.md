# ἄνθραξ

[anthrax](https://www.blueletterbible.org/lexicon/g440)

Definition: coals of fire (1x), black

Part of speech: adjective

Occurs 1 times in 1 verses

Hebrew: [gechel](../h/h1513.md)

Derived words: anthrax

Notes: The Arabians call things that cause very acute mental pain "burning coals of the heart" and "fire of the liver". 

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0440)

[Study Light](https://www.studylight.org/lexicons/greek/440.html)

[Bible Hub](https://biblehub.com/str/greek/440.htm)

[LSJ](https://lsj.gr/wiki/ἄνθραξ)

[NET Bible](http://classic.net.bible.org/strong.php?id=440)

[Bible Bento](https://biblebento.com/dictionary/G0440.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0440/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0440.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g440)
