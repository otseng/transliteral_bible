# ἐγκόπτω

[egkoptō](https://www.blueletterbible.org/lexicon/g1465)

Definition: hinder (3x), be tedious unto (1x), to cut into, to impede one's course by cutting off his way, detain

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1465)

[Study Light](https://www.studylight.org/lexicons/greek/1465.html)

[Bible Hub](https://biblehub.com/str/greek/1465.htm)

[LSJ](https://lsj.gr/wiki/ἐγκόπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1465)

[Bible Bento](https://biblebento.com/dictionary/G1465.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1465/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1465.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1465)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34225)
