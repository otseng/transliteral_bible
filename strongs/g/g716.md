# ἅρμα

[harma](https://www.blueletterbible.org/lexicon/g716)

Definition: chariot (4x).

Part of speech: neuter noun

Occurs 4 times in 4 verses

Hebrew: [merkāḇâ](../h/h4818.md), [reḵeḇ](../h/h7393.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0716)

[Study Light](https://www.studylight.org/lexicons/greek/716.html)

[Bible Hub](https://biblehub.com/str/greek/716.htm)

[LSJ](https://lsj.gr/wiki/ἅρμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=716)

[Bible Bento](https://biblebento.com/dictionary/G0716.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0716/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0716.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g716)
