# ἐλωΐ

[elōï](https://www.blueletterbible.org/lexicon/g1682)

Definition: Eloi = "my God" (Aramaic)

Part of speech: Aramaism

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1682)

[Study Light](https://www.studylight.org/lexicons/greek/1682.html)

[Bible Hub](https://biblehub.com/str/greek/1682.htm)

[LSJ](https://lsj.gr/wiki/ἐλωΐ)

[NET Bible](http://classic.net.bible.org/strong.php?id=1682)

[Bible Bento](https://biblebento.com/dictionary/G1682.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1682/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1682.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1682)