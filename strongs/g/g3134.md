# μαράνα θά

[marana tha](https://www.blueletterbible.org/lexicon/g3134)

Definition: Maranatha (1x).

Part of speech: interjection

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3134)

[Study Light](https://www.studylight.org/lexicons/greek/3134.html)

[Bible Hub](https://biblehub.com/str/greek/3134.htm)

[LSJ](https://lsj.gr/wiki/μαράνα θά)

[NET Bible](http://classic.net.bible.org/strong.php?id=3134)

[Bible Bento](https://biblebento.com/dictionary/G3134.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3134/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3134.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3134)

[Wikipedia](https://en.wikipedia.org/wiki/Maranatha)