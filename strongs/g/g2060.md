# Ἑρμῆς

[Hermēs](https://www.blueletterbible.org/lexicon/g2060)

Definition: Mercurius (1x), Hermes a Christian (1x), Mercury, "herald of the gods"

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2060)

[Study Light](https://www.studylight.org/lexicons/greek/2060.html)

[Bible Hub](https://biblehub.com/str/greek/2060.htm)

[LSJ](https://lsj.gr/wiki/Ἑρμῆς)

[NET Bible](http://classic.net.bible.org/strong.php?id=2060)

[Bible Bento](https://biblebento.com/dictionary/G2060.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2060/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2060.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2060)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%99%CF%81%CE%BC%E1%BF%86%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Hermes)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/mercurius.html)