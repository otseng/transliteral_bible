# κατηχέω

[katēcheō](https://www.blueletterbible.org/lexicon/g2727)

Definition: instruct (3x), teach (3x), inform (2x)

Part of speech: verb

Occurs 8 times in 7 verses

Derived words: [Catechesis](https://en.wikipedia.org/wiki/Catechesis), [catechism](https://en.wiktionary.org/wiki/catechism)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2727)

[Study Light](https://www.studylight.org/lexicons/greek/2727.html)

[Bible Hub](https://biblehub.com/str/greek/2727.htm)

[LSJ](https://lsj.gr/wiki/κατηχέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2727)

[Bible Bento](https://biblebento.com/dictionary/G2727.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2727/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2727.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2727)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%B1%CF%84%CE%B7%CF%87%CE%AD%CF%89)