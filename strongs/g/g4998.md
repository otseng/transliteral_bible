# σώφρων

[sōphrōn](https://www.blueletterbible.org/lexicon/g4998)

Definition: sober (2x), temperate (1x), discreet (1x).

Part of speech: adjective

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4998)

[Study Light](https://www.studylight.org/lexicons/greek/4998.html)

[Bible Hub](https://biblehub.com/str/greek/4998.htm)

[LSJ](https://lsj.gr/wiki/σώφρων)

[NET Bible](http://classic.net.bible.org/strong.php?id=4998)

[Bible Bento](https://biblebento.com/dictionary/G4998.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4998/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4998.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4998)
