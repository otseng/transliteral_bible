# ὕψωμα

[hypsōma](https://www.blueletterbible.org/lexicon/g5313)

Definition: height (1x), high thing (1x).

Part of speech: neuter noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5313)

[Study Light](https://www.studylight.org/lexicons/greek/5313.html)

[Bible Hub](https://biblehub.com/str/greek/5313.htm)

[LSJ](https://lsj.gr/wiki/ὕψωμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=5313)

[Bible Bento](https://biblebento.com/dictionary/G5313.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5313/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5313.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5313)
