# ἐδαφίζω

[edaphizō](https://www.blueletterbible.org/lexicon/g1474)

Definition: lay even with the ground (1x), to throw to the ground, to raze

Part of speech: verb

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1474)

[Study Light](https://www.studylight.org/lexicons/greek/1474.html)

[Bible Hub](https://biblehub.com/str/greek/1474.htm)

[LSJ](https://lsj.gr/wiki/ἐδαφίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1474)

[Bible Bento](https://biblebento.com/dictionary/G1474.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1474/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1474.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1474)
