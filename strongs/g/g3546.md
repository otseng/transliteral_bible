# νόμισμα

[nomisma](https://www.blueletterbible.org/lexicon/g3546)

Definition: money (1x), coin, legal tender

Part of speech: neuter noun

Occurs 1 times in 1 verses

Derived words: numismatics

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3546)

[Study Light](https://www.studylight.org/lexicons/greek/3546.html)

[Bible Hub](https://biblehub.com/str/greek/3546.htm)

[LSJ](https://lsj.gr/wiki/νόμισμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=3546)

[Bible Bento](https://biblebento.com/dictionary/G3546.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3546/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3546.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3546)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BD%CF%8C%CE%BC%CE%B9%CF%83%CE%BC%CE%B1)

[Wikipedia](https://en.wikipedia.org/wiki/Nomisma)
