# ὁδηγός

[hodēgos](https://www.blueletterbible.org/lexicon/g3595)

Definition: guide (4x), leader (1x), conductor, driver

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3595)

[Study Light](https://www.studylight.org/lexicons/greek/3595.html)

[Bible Hub](https://biblehub.com/str/greek/3595.htm)

[LSJ](https://lsj.gr/wiki/ὁδηγός)

[NET Bible](http://classic.net.bible.org/strong.php?id=3595)

[Bible Bento](https://biblebento.com/dictionary/G3595.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3595/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3595.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3595)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BF%CE%B4%CE%B7%CE%B3%CF%8C%CF%82)
