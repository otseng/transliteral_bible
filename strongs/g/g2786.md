# Κηφᾶς

[Kēphas](https://www.blueletterbible.org/lexicon/g2786)

Definition: Cephas (6x), stone, rock

Part of speech: proper masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2786)

[Study Light](https://www.studylight.org/lexicons/greek/2786.html)

[Bible Hub](https://biblehub.com/str/greek/2786.htm)

[LSJ](https://lsj.gr/wiki/Κηφᾶς)

[NET Bible](http://classic.net.bible.org/strong.php?id=2786)

[Bible Bento](https://biblebento.com/dictionary/G2786.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2786/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2786.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2786)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/cephas.html)