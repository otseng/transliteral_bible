# πλατύς

[platys](https://www.blueletterbible.org/lexicon/g4116)

Definition: wide (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4116)

[Study Light](https://www.studylight.org/lexicons/greek/4116.html)

[Bible Hub](https://biblehub.com/str/greek/4116.htm)

[LSJ](https://lsj.gr/wiki/πλατύς)

[NET Bible](http://classic.net.bible.org/strong.php?id=4116)

[Bible Bento](https://biblebento.com/dictionary/G4116.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4116/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4116.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4116)

