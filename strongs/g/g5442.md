# φυλάσσω

[phylassō](https://www.blueletterbible.org/lexicon/g5442)

Definition: keep (23x), observe (2x), beware (2x), keep (one's) self (1x), save (1x), be ... ware (1x), guard, maintain, shun, avoid, remember

Part of speech: verb

Occurs 32 times in 30 verses

Greek: [phylakē](../g/g5438.md)

Hebrew: [shamar](../h/h8104.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5442)

[Study Light](https://www.studylight.org/lexicons/greek/5442.html)

[Bible Hub](https://biblehub.com/str/greek/5442.htm)

[LSJ](https://lsj.gr/wiki/φυλάσσω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5442)

[Bible Bento](https://biblebento.com/dictionary/G5442.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5442/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5442.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5442)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%86%CF%85%CE%BB%CE%AC%CF%83%CF%83%CF%89)