# ἐνωτίζομαι

[enōtizomai](https://www.blueletterbible.org/lexicon/g1801)

Definition: hearken to (1x), to receive into the ear, to give ear to, listen

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1801)

[Study Light](https://www.studylight.org/lexicons/greek/1801.html)

[Bible Hub](https://biblehub.com/str/greek/1801.htm)

[LSJ](https://lsj.gr/wiki/ἐνωτίζομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1801)

[Bible Bento](https://biblebento.com/dictionary/G1801.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1801/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1801.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1801)
