# πορθέω

[portheō](https://www.blueletterbible.org/lexicon/g4199)

Definition: destroy (2x), waste (1x), overthrow, to ravage, to sack

Part of speech: verb

Occurs 3 times in 3 verses

Synonyms: [destroy](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Destroy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4199)

[Study Light](https://www.studylight.org/lexicons/greek/4199.html)

[Bible Hub](https://biblehub.com/str/greek/4199.htm)

[LSJ](https://lsj.gr/wiki/πορθέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4199)

[Bible Bento](https://biblebento.com/dictionary/G4199.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4199/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4199.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4199)
