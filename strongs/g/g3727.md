# ὅρκος

[horkos](https://www.blueletterbible.org/lexicon/g3727)

Definition: oath (10x), pledged or promised with an oath

Part of speech: masculine noun

Occurs 10 times in 10 verses

Hebrew: [šᵊḇûʿâ](../h/h7621.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3727)

[Study Light](https://www.studylight.org/lexicons/greek/3727.html)

[Bible Hub](https://biblehub.com/str/greek/3727.htm)

[LSJ](https://lsj.gr/wiki/ὅρκος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3727)

[Bible Bento](https://biblebento.com/dictionary/G3727.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3727/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3727.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3727)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BD%85%CF%81%CE%BA%CE%BF%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Horkos)