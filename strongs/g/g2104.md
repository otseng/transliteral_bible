# εὐγενής

[eugenēs](https://www.blueletterbible.org/lexicon/g2104)

Definition: nobleman (with G444) (1x), more noble (1x), noble (1x).

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2104)

[Study Light](https://www.studylight.org/lexicons/greek/2104.html)

[Bible Hub](https://biblehub.com/str/greek/2104.htm)

[LSJ](https://lsj.gr/wiki/εὐγενής)

[NET Bible](http://classic.net.bible.org/strong.php?id=2104)

[Bible Bento](https://biblebento.com/dictionary/G2104.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2104/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2104.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2104)