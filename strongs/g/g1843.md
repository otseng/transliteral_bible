# ἐξομολογέω

[exomologeō](https://www.blueletterbible.org/lexicon/g1843)

Definition: confess (8x), thank (2x), promise (1x), profess, acknowledge

Part of speech: verb

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1843)

[Study Light](https://www.studylight.org/lexicons/greek/1843.html)

[Bible Hub](https://biblehub.com/str/greek/1843.htm)

[LSJ](https://lsj.gr/wiki/ἐξομολογέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1843)

[Bible Bento](https://biblebento.com/dictionary/G1843.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1843/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1843.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1843)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%90%CE%BE%CE%BF%CE%BC%CE%BF%CE%BB%CF%8C%CE%B3%CE%B7%CF%83%CE%B9%CF%82)