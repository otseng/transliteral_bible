# ὕλη

[hylē](https://www.blueletterbible.org/lexicon/g5208)

Definition: matter (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5208)

[Study Light](https://www.studylight.org/lexicons/greek/5208.html)

[Bible Hub](https://biblehub.com/str/greek/5208.htm)

[LSJ](https://lsj.gr/wiki/ὕλη)

[NET Bible](http://classic.net.bible.org/strong.php?id=5208)

[Bible Bento](https://biblebento.com/dictionary/G5208.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5208/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5208.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5208)

