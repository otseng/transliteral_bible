# ἐπιούσιος

[epiousios](https://www.blueletterbible.org/lexicon/g1967)

Definition: daily (2x), this day, next day

Part of speech: adjective

Occurs 2 times in 2 verses

Greek: [epeimi](../g/g1966.md)

Hebrew: [tāmîḏ](../h/h8548.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1967)

[Study Light](https://www.studylight.org/lexicons/greek/1967.html)

[Bible Hub](https://biblehub.com/str/greek/1967.htm)

[LSJ](https://lsj.gr/wiki/ἐπιούσιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1967)

[Bible Bento](https://biblebento.com/dictionary/G1967.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1967/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1967.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1967)