# Νινευή

[Nineuē](https://www.blueletterbible.org/lexicon/g3535)

Definition: Nineve (1x), Nineveh, "offspring of ease: offspring abiding", capital of the ancient kingdom of Assyria

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3535)

[Study Light](https://www.studylight.org/lexicons/greek/3535.html)

[Bible Hub](https://biblehub.com/str/greek/3535.htm)

[LSJ](https://lsj.gr/wiki/Νινευή)

[NET Bible](http://classic.net.bible.org/strong.php?id=3535)

[Bible Bento](https://biblebento.com/dictionary/G3535.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3535/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3535.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3535)

[Wikipedia](https://en.wikipedia.org/wiki/Nineveh)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/nineve.html)