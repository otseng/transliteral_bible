# κάλαμος

[kalamos](https://www.blueletterbible.org/lexicon/g2563)

Definition: reed (11x), pen (1x), rod, staff

Part of speech: masculine noun

Occurs 12 times in 12 verses

Hebrew: [qānê](../h/h7070.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2563)

[Study Light](https://www.studylight.org/lexicons/greek/2563.html)

[Bible Hub](https://biblehub.com/str/greek/2563.htm)

[LSJ](https://lsj.gr/wiki/κάλαμος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2563)

[Bible Bento](https://biblebento.com/dictionary/G2563.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2563/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2563.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2563)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%AC%CE%BB%CE%B1%CE%BC%CE%BF%CF%82)

[Precept Austin](https://www.preceptaustin.org/luke-7-commentary#reed)