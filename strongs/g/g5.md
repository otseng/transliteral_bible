# ἀββα

[abba](https://www.blueletterbible.org/lexicon/g5)

Definition: Abba (Aramaic), father

Occurs 3 times in 3 verses

Hebrew: ['ab](../h/h2.md)

Part of speech: Aramaism

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0005)

[Study Light](https://www.studylight.org/lexicons/greek/5.html)

[Bible Hub](https://biblehub.com/str/greek/5.htm)

[LSJ](https://lsj.gr/wiki/ἀββα)

[NET Bible](http://classic.net.bible.org/strong.php?id=5)

[Bible Bento](https://biblebento.com/dictionary/G0005.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0005/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0005.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CE%B2%CE%B2%CE%B1)

[Wikipedia](https://en.wikipedia.org/wiki/Ab_%28Semitic%29)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33443)

[Precept Austin](https://www.preceptaustin.org/abba-father)
