# ἀποκλείω

[apokleiō](https://www.blueletterbible.org/lexicon/g608)

Definition: shut up (1x)

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0608)

[Study Light](https://www.studylight.org/lexicons/greek/608.html)

[Bible Hub](https://biblehub.com/str/greek/608.htm)

[LSJ](https://lsj.gr/wiki/ἀποκλείω)

[NET Bible](http://classic.net.bible.org/strong.php?id=608)

[Bible Bento](https://biblebento.com/dictionary/G0608.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0608/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0608.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g608)