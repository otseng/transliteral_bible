# ἴαμα

[iama](https://www.blueletterbible.org/lexicon/g2386)

Definition: healing (3x), remedy

Part of speech: neuter noun

Occurs 3 times in 3 verses

Hebrew: [marpē'](../h/h4832.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2386)

[Study Light](https://www.studylight.org/lexicons/greek/2386.html)

[Bible Hub](https://biblehub.com/str/greek/2386.htm)

[LSJ](https://lsj.gr/wiki/ἴαμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=2386)

[Bible Bento](https://biblebento.com/dictionary/G2386.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2386/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2386.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2386)
