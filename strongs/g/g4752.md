# στρατεία

[strateia](https://www.blueletterbible.org/lexicon/g4752)

Definition: warfare (2x), an expedition, campaign, military service

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4752)

[Study Light](https://www.studylight.org/lexicons/greek/4752.html)

[Bible Hub](https://biblehub.com/str/greek/4752.htm)

[LSJ](https://lsj.gr/wiki/στρατεία)

[NET Bible](http://classic.net.bible.org/strong.php?id=4752)

[Bible Bento](https://biblebento.com/dictionary/G4752.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4752/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4752.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4752)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CF%84%CF%81%CE%B1%CF%84%CE%B5%CE%AF%CE%B1)

[Wikipedia](https://en.wikipedia.org/wiki/Strateia)
