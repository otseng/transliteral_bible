# ἄρρητος

[arrētos](https://www.blueletterbible.org/lexicon/g731)

Definition: unspeakable (1x), unsaid, unspoken, inexpressible

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0731)

[Study Light](https://www.studylight.org/lexicons/greek/731.html)

[Bible Hub](https://biblehub.com/str/greek/731.htm)

[LSJ](https://lsj.gr/wiki/ἄρρητος)

[NET Bible](http://classic.net.bible.org/strong.php?id=731)

[Bible Bento](https://biblebento.com/dictionary/G0731.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0731/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/731.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g731)
