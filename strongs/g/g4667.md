# Σμύρνα

[smyrna](https://www.blueletterbible.org/lexicon/g4667)

Definition: Smyrna (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4667)

[Study Light](https://www.studylight.org/lexicons/greek/4667.html)

[Bible Hub](https://biblehub.com/str/greek/4667.htm)

[LSJ](https://lsj.gr/wiki/Σμύρνα)

[NET Bible](http://classic.net.bible.org/strong.php?id=4667)

[Bible Bento](https://biblebento.com/dictionary/G4667.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4667/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4667.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4667)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/smyrna.html)

[Video Bible](https://www.videobible.com/bible-dictionary/smyrna)