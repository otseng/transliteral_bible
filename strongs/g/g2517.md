# καθεξῆς

[kathexēs](https://www.blueletterbible.org/lexicon/g2517)

Definition: in order (2x), afterward (1x), after (1x), by order (1x)

Part of speech: adverb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2517)

[Study Light](https://www.studylight.org/lexicons/greek/2517.html)

[Bible Hub](https://biblehub.com/str/greek/2517.htm)

[LSJ](https://lsj.gr/wiki/καθεξῆς)

[NET Bible](http://classic.net.bible.org/strong.php?id=2517)

[Bible Bento](https://biblebento.com/dictionary/G2517.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2517/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2517.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2517)
