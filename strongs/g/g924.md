# Βαρτιμαῖος

[Bartimaios](https://www.blueletterbible.org/lexicon/g924)

Definition: Bartimaeus (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g924)

[Study Light](https://www.studylight.org/lexicons/greek/924.html)

[Bible Hub](https://biblehub.com/str/greek/924.htm)

[LSJ](https://lsj.gr/wiki/Βαρτιμαῖος)

[NET Bible](http://classic.net.bible.org/strong.php?id=924)

[Bible Bento](https://biblebento.com/dictionary/G924.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/924/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/924.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g924)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bartimaeus.html)

[Video Bible](https://www.videobible.com/bible-dictionary/bartimaeus)