# προτρέπω

[protrepō](https://www.blueletterbible.org/lexicon/g4389)

Definition: exhort (1x), to urge forwards, exhort, encourage

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4389)

[Study Light](https://www.studylight.org/lexicons/greek/4389.html)

[Bible Hub](https://biblehub.com/str/greek/4389.htm)

[LSJ](https://lsj.gr/wiki/προτρέπω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4389)

[Bible Bento](https://biblebento.com/dictionary/G4389.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4389/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4389.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4389)

[Precept Austin](https://www.preceptaustin.org/acts-18-commentary#encourage)
