# ἄχρι

[achri](https://www.blueletterbible.org/lexicon/g891)

Definition: until (14x), unto (13x), till (3x), till (with G3739) (with G302) (3x), until (with G3739) (2x), while (with G3739) (2x), even to (2x), miscellaneous (7x).

Part of speech: preposition/conjunction

Occurs 49 times in 49 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g891)

[Study Light](https://www.studylight.org/lexicons/greek/891.html)

[Bible Hub](https://biblehub.com/str/greek/891.htm)

[LSJ](https://lsj.gr/wiki/ἄχρι)

[NET Bible](http://classic.net.bible.org/strong.php?id=891)

[Bible Bento](https://biblebento.com/dictionary/G891.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/891/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/891.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g891)

