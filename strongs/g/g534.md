# ἀπάρτι

[aparti](https://www.blueletterbible.org/lexicon/g534)

Definition: from henceforth (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g534)

[Study Light](https://www.studylight.org/lexicons/greek/534.html)

[Bible Hub](https://biblehub.com/str/greek/534.htm)

[LSJ](https://lsj.gr/wiki/ἀπάρτι)

[NET Bible](http://classic.net.bible.org/strong.php?id=534)

[Bible Bento](https://biblebento.com/dictionary/G534.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/534/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/534.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g534)

