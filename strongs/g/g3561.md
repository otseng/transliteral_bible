# νεομηνία

[neomēnia](https://www.blueletterbible.org/lexicon/g3561)

Definition: new moon (1x), the Jewish festival of the new moon

Part of speech: feminine noun

Occurs 1 times in 1 verses

Derived words: [neomenia](https://en.wiktionary.org/wiki/neomenia)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3561)

[Study Light](https://www.studylight.org/lexicons/greek/3561.html)

[Bible Hub](https://biblehub.com/str/greek/3561.htm)

[LSJ](https://lsj.gr/wiki/νεομηνία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3561)

[Bible Bento](https://biblebento.com/dictionary/G3561.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3561/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3561.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3561)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/3561-noumenia-new-month.htm)
