# Πέτρος

[Petros](https://www.blueletterbible.org/lexicon/g4074)

Definition: Peter (161x), stone (1x), "rock"

Part of speech: proper masculine noun

Occurs 162 times in 157 verses

Derived words: petrified

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4074)

[Study Light](https://www.studylight.org/lexicons/greek/4074.html)

[Bible Hub](https://biblehub.com/str/greek/4074.htm)

[LSJ](https://lsj.gr/wiki/Πέτρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4074)

[Bible Bento](https://biblebento.com/dictionary/G4074.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4074/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4074.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4074)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/peter.html)

[Video Bible](https://www.videobible.com/bible-dictionary/peter)