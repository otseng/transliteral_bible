# Ἐπαίνετος

[epainetos](https://www.blueletterbible.org/lexicon/g1866)

Definition: Epaenetus (1x), "praiseworthy"

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1866)

[Study Light](https://www.studylight.org/lexicons/greek/1866.html)

[Bible Hub](https://biblehub.com/str/greek/1866.htm)

[LSJ](https://lsj.gr/wiki/Ἐπαίνετος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1866)

[Bible Bento](https://biblebento.com/dictionary/G1866.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1866/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1866.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1866)

[Video Bible](https://www.videobible.com/bible-dictionary/epaenetus)