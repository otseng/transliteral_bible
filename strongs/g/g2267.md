# Ἡρῳδίων

[hērōdiōn](https://www.blueletterbible.org/lexicon/g2267)

Definition: Herodian, "heroic"

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2267)

[Study Light](https://www.studylight.org/lexicons/greek/2267.html)

[Bible Hub](https://biblehub.com/str/greek/2267.htm)

[LSJ](https://lsj.gr/wiki/Ἡρῳδίων)

[NET Bible](http://classic.net.bible.org/strong.php?id=2267)

[Bible Bento](https://biblebento.com/dictionary/G2267.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2267/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2267.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2267)

[Video Bible](https://www.videobible.com/bible-dictionary/herodian,)