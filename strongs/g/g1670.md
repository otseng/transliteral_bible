# ἕλκω

[helkō](https://www.blueletterbible.org/lexicon/g1670)

Definition: draw (8x), drag, lead, impel, pull along a surface

Part of speech: verb

Occurs 8 times in 8 verses

Hebrew: [mashak](../h/h4900.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1670)

[Study Light](https://www.studylight.org/lexicons/greek/1670.html)

[Bible Hub](https://biblehub.com/str/greek/1670.htm)

[LSJ](https://lsj.gr/wiki/ἕλκω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1670)

[Bible Bento](https://biblebento.com/dictionary/G1670.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1670/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1670.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1670)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%95%CE%BB%CE%BA%CF%89)

[Help me with Bible study](http://helpmewithbiblestudy.org/9Salvation/DefDraws_Mar.aspx#sthash.Oy9ArG0c.dpbs)

[Tom Hobson](https://www.patheos.com/blogs/tomhobson/2018/02/drag-versus-draw-god-bring-people-faith/)