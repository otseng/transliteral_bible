# ἀναγγέλλω

[anaggellō](https://www.blueletterbible.org/lexicon/g312)

Definition: tell (6x), show (6x), declare (3x), rehearse (1x), speak (1x), report (1x).

Part of speech: verb

Occurs 20 times in 18 verses

Hebrew: [nāḡaḏ](../h/h5046.md), [sāp̄ar](../h/h5608.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0312)

[Study Light](https://www.studylight.org/lexicons/greek/312.html)

[Bible Hub](https://biblehub.com/str/greek/312.htm)

[LSJ](https://lsj.gr/wiki/ἀναγγέλλω)

[NET Bible](http://classic.net.bible.org/strong.php?id=312)

[Bible Bento](https://biblebento.com/dictionary/G0312.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0312/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0312.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g312)
