# ἐκπλήρωσις

[ekplērōsis](https://www.blueletterbible.org/lexicon/g1604)

Definition: accomplishment (1x), a completing, fulfilment

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1604)

[Study Light](https://www.studylight.org/lexicons/greek/1604.html)

[Bible Hub](https://biblehub.com/str/greek/1604.htm)

[LSJ](https://lsj.gr/wiki/ἐκπλήρωσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=1604)

[Bible Bento](https://biblebento.com/dictionary/G1604.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1604/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1604.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1604)
