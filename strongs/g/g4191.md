# πονηρότερος

[ponēroteros](https://www.blueletterbible.org/lexicon/g4191)

Definition:  more wicked (2x), more evil

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4191)

[Study Light](https://www.studylight.org/lexicons/greek/4191.html)

[Bible Hub](https://biblehub.com/str/greek/4191.htm)

[LSJ](https://lsj.gr/wiki/πονηρότερος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4191)

[Bible Bento](https://biblebento.com/dictionary/G4191.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4191/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4191.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4191)