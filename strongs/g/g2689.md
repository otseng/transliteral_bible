# καταστολή

[katastolē](https://www.blueletterbible.org/lexicon/g2689)

Definition: apparel (1x), a garment let down, dress, attire

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2689)

[Study Light](https://www.studylight.org/lexicons/greek/2689.html)

[Bible Hub](https://biblehub.com/str/greek/2689.htm)

[LSJ](https://lsj.gr/wiki/καταστολή)

[NET Bible](http://classic.net.bible.org/strong.php?id=2689)

[Bible Bento](https://biblebento.com/dictionary/G2689.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2689/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2689.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2689)
