# ἀναβάλλω

[anaballō](https://www.blueletterbible.org/lexicon/g306)

Definition: defer (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g306)

[Study Light](https://www.studylight.org/lexicons/greek/306.html)

[Bible Hub](https://biblehub.com/str/greek/306.htm)

[LSJ](https://lsj.gr/wiki/ἀναβάλλω)

[NET Bible](http://classic.net.bible.org/strong.php?id=306)

[Bible Bento](https://biblebento.com/dictionary/G306.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/306/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/306.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g306)

