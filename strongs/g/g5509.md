# χιτών

[chitōn](https://www.blueletterbible.org/lexicon/g5509)

Definition: coat (9x), garment (1x), clothes (1x), tunic, shirt

Part of speech: masculine noun

Occurs 11 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5509)

[Study Light](https://www.studylight.org/lexicons/greek/5509.html)

[Bible Hub](https://biblehub.com/str/greek/5509.htm)

[LSJ](https://lsj.gr/wiki/χιτών)

[NET Bible](http://classic.net.bible.org/strong.php?id=5509)

[Bible Bento](https://biblebento.com/dictionary/G5509.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5509/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5509.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5509)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%87%CE%B9%CF%84%CF%8E%CE%BD)

[Wikipedia](https://en.wikipedia.org/wiki/Chiton_%28costume%29)

[Precept Austin](https://www.preceptaustin.org/jude_23_commentary#g)