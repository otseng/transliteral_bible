# ἑπτά

[hepta](https://www.blueletterbible.org/lexicon/g2033)

Definition: seven (86x), seventh (1x)

Part of speech: indeclinable noun

Occurs 87 times in 63 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2033)

[Study Light](https://www.studylight.org/lexicons/greek/2033.html)

[Bible Hub](https://biblehub.com/str/greek/2033.htm)

[LSJ](https://lsj.gr/wiki/ἑπτά)

[NET Bible](http://classic.net.bible.org/strong.php?id=2033)

[Bible Bento](https://biblebento.com/dictionary/G2033.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2033/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2033.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2033)