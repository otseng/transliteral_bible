# δεσπότης

[despotēs](https://www.blueletterbible.org/lexicon/g1203)

Definition: Lord (5x), master (5x)

Part of speech: masculine noun

Occurs 10 times in 10 verses

Derived words: [despot](https://en.wiktionary.org/wiki/despot)

Synonyms: [lord](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Lord)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1203)

[Study Light](https://www.studylight.org/lexicons/greek/1203.html)

[Bible Hub](https://biblehub.com/str/greek/1203.htm)

[LSJ](https://lsj.gr/wiki/δεσπότης)

[NET Bible](http://classic.net.bible.org/strong.php?id=1203)

[Bible Bento](https://biblebento.com/dictionary/G1203.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1203/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1203.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1203)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B4%CE%B5%CF%83%CF%80%CF%8C%CF%84%CE%B7%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Despot_%28court_title%29)

[Precept Austin](https://www.preceptaustin.org/1_peter_218-25#d)