# φιλοτιμέομαι

[philotimeomai](https://www.blueletterbible.org/lexicon/g5389)

Definition: strive (1x), labour (1x), study (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5389)

[Study Light](https://www.studylight.org/lexicons/greek/5389.html)

[Bible Hub](https://biblehub.com/str/greek/5389.htm)

[LSJ](https://lsj.gr/wiki/φιλοτιμέομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=5389)

[Bible Bento](https://biblebento.com/dictionary/G5389.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5389/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5389.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5389)
