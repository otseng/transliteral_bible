# Ἀρέτας

[aretas](https://www.blueletterbible.org/lexicon/g702)

Definition: Aretas (1x), "graver", An Arabian king.  Aretas' daughter, Phasaelis, married Herod Antipas.  Herod divorces her to marry his brother's wife, Herodias.

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0702)

[Study Light](https://www.studylight.org/lexicons/greek/702.html)

[Bible Hub](https://biblehub.com/str/greek/702.htm)

[LSJ](https://lsj.gr/wiki/Ἀρέτας)

[NET Bible](http://classic.net.bible.org/strong.php?id=702)

[Bible Bento](https://biblebento.com/dictionary/G0702.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0702/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/702.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g702)

[Wiktionary](https://en.wiktionary.org/wiki/Aretas)

[Wikipedia](https://en.wikipedia.org/wiki/Aretas)

[Jewish Encyclopedia](http://www.jewishencyclopedia.com/articles/1752-aretas)

[Precept Austin]()

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/aretas.html)

[Video Bible](https://www.videobible.com/bible-dictionary/aretas)