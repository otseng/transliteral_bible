# ὑποστρέφω

[hypostrephō](https://www.blueletterbible.org/lexicon/g5290)

Definition: return (28x), return again (3x), turn back (1x), turn again (1x), return back again (1x), come again (1x)

Part of speech: verb

Occurs 36 times in 35 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5290)

[Study Light](https://www.studylight.org/lexicons/greek/5290.html)

[Bible Hub](https://biblehub.com/str/greek/5290.htm)

[LSJ](https://lsj.gr/wiki/ὑποστρέφω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5290)

[Bible Bento](https://biblebento.com/dictionary/G5290.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5290/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5290.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5290)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BD%91%CF%80%CE%BF%CF%83%CF%84%CF%81%CE%AD%CF%86%CF%89)