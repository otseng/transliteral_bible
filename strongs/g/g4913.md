# συνήδομαι

[synēdomai](https://www.blueletterbible.org/lexicon/g4913)

Definition: delight (1x), to rejoice together with

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4913)

[Study Light](https://www.studylight.org/lexicons/greek/4913.html)

[Bible Hub](https://biblehub.com/str/greek/4913.htm)

[LSJ](https://lsj.gr/wiki/συνήδομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4913)

[Bible Bento](https://biblebento.com/dictionary/G4913.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4913/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4913.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4913)
