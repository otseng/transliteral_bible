# ἀγγεῖον

[aggeion](https://www.blueletterbible.org/lexicon/g30)

Definition: vessel (2x), receptacle, a pail, a reservoir

Part of speech: neuter noun

Occurs 2 times in 2 verses

Hebrew: [neḇel](../h/h5035.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0030)

[Study Light](https://www.studylight.org/lexicons/greek/30.html)

[Bible Hub](https://biblehub.com/str/greek/30.htm)

[LSJ](https://lsj.gr/wiki/ἀγγεῖον)

[NET Bible](http://classic.net.bible.org/strong.php?id=30)

[Bible Bento](https://biblebento.com/dictionary/G0030.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0030/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0030.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g30)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CE%B3%CE%B3%CE%B5%E1%BF%96%CE%BF%CE%BD)
