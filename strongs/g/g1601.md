# ἐκπίπτω

[ekpiptō](https://www.blueletterbible.org/lexicon/g1601)

Definition: fall (7x), fall off (2x), be cast (1x), take none effect (1x), fall away (1x), fail (1x), variations of 'fallen' (1x), wither, to drift

Part of speech: verb

Occurs 14 times in 13 verses

Greek: [ek](../g/g1537.md), [piptō](../g/g4098.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1601)

[Study Light](https://www.studylight.org/lexicons/greek/1601.html)

[Bible Hub](https://biblehub.com/str/greek/1601.htm)

[LSJ](https://lsj.gr/wiki/ἐκπίπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1601)

[Bible Bento](https://biblebento.com/dictionary/G1601.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1601/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1601.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1601)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%90%CE%BA%CF%80%CE%AF%CF%80%CF%84%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34026)

[Precept Austin](https://www.preceptaustin.org/romans_96-8#f)
