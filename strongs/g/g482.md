# ἀντιλαμβάνω

[antilambanō](https://www.blueletterbible.org/lexicon/g482)

Definition: help (1x), support (1x), partaker (1x), support, support, assist

Part of speech: verb

Occurs 3 times in 3 verses

Hebrew: [kāp̄ap̄](../h/h3721.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0482)

[Study Light](https://www.studylight.org/lexicons/greek/482.html)

[Bible Hub](https://biblehub.com/str/greek/482.htm)

[LSJ](https://lsj.gr/wiki/ἀντιλαμβάνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=482)

[Bible Bento](https://biblebento.com/dictionary/G0482.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0482/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0482.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g482)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34217)