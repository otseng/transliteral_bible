# ἐλάχιστος

[elachistos](https://www.blueletterbible.org/lexicon/g1646)

Definition: least (9x), very small (2x), smallest (1x), very little (1x).

Part of speech: adjective

Occurs 13 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1646)

[Study Light](https://www.studylight.org/lexicons/greek/1646.html)

[Bible Hub](https://biblehub.com/str/greek/1646.htm)

[LSJ](https://lsj.gr/wiki/ἐλάχιστος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1646)

[Bible Bento](https://biblebento.com/dictionary/G1646.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1646/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1646.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1646)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%90%CE%BB%CE%AC%CF%87%CE%B9%CF%83%CF%84%CE%BF%CF%82)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34365)