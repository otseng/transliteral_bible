# λιθοβολέω

[lithoboleō](https://www.blueletterbible.org/lexicon/g3036)

Definition: stone (8x), cast stone (1x), pelt with stones

Part of speech: verb

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3036)

[Study Light](https://www.studylight.org/lexicons/greek/3036.html)

[Bible Hub](https://biblehub.com/str/greek/3036.htm)

[LSJ](https://lsj.gr/wiki/λιθοβολέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3036)

[Bible Bento](https://biblebento.com/dictionary/G3036.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3036/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3036.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3036)

[Kurt Michaelson](http://www.kurtmichaelson.org/2009/01/)