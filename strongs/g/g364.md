# ἀνάμνησις

[anamnēsis](https://www.blueletterbible.org/lexicon/g364)

Definition: remembrance (3x), remembrance again (1x), recollection

Part of speech: feminine noun

Occurs 4 times in 4 verses

Derived words: [Anamnesis](https://en.wikipedia.org/wiki/Anamnesis_%28Christianity%29)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0364)

[Study Light](https://www.studylight.org/lexicons/greek/364.html)

[Bible Hub](https://biblehub.com/str/greek/364.htm)

[LSJ](https://lsj.gr/wiki/ἀνάμνησις)

[NET Bible](http://classic.net.bible.org/strong.php?id=364)

[Bible Bento](https://biblebento.com/dictionary/G0364.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0364/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0364.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g364)

[Wikipedia](https://en.wikipedia.org/wiki/Anamnesis_%28Christianity%29)