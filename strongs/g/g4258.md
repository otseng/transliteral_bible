# προαμαρτάνω

[proamartanō](https://www.blueletterbible.org/lexicon/g4258)

Definition: sin already (1x), sin heretofore (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4258)

[Study Light](https://www.studylight.org/lexicons/greek/4258.html)

[Bible Hub](https://biblehub.com/str/greek/4258.htm)

[LSJ](https://lsj.gr/wiki/προαμαρτάνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4258)

[Bible Bento](https://biblebento.com/dictionary/G4258.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4258/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4258.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4258)
