# ἀντίδικος

[antidikos](https://www.blueletterbible.org/lexicon/g476)

Definition: adversary (5x), opponent, enemy

Part of speech: masculine noun

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0476)

[Study Light](https://www.studylight.org/lexicons/greek/476.html)

[Bible Hub](https://biblehub.com/str/greek/476.htm)

[LSJ](https://lsj.gr/wiki/ἀντίδικος)

[NET Bible](http://classic.net.bible.org/strong.php?id=476)

[Bible Bento](https://biblebento.com/dictionary/G0476.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0476/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0476.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g476)