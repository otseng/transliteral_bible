# ἀποδιορίζω

[apodiorizō](https://www.blueletterbible.org/lexicon/g592)

Definition: separate (one's) self (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0592)

[Study Light](https://www.studylight.org/lexicons/greek/592.html)

[Bible Hub](https://biblehub.com/str/greek/592.htm)

[LSJ](https://lsj.gr/wiki/ἀποδιορίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=592)

[Bible Bento](https://biblebento.com/dictionary/G592.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/592/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/592.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g592)

