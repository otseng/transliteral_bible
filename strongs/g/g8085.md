# ἄλφα

[alpha](https://www.blueletterbible.org/lexicon/g8085)

Definition: Alpha (4x).

Part of speech: ?

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g8085)

[Study Light](https://www.studylight.org/lexicons/greek/8085.html)

[Bible Hub](https://biblehub.com/str/greek/8085.htm)

[LSJ](https://lsj.gr/wiki/ἄλφα)

[NET Bible](http://classic.net.bible.org/strong.php?id=8085)

[Bible Bento](https://biblebento.com/dictionary/G8085.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8085/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/8085.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g8085)

