# ἔντρομος

[entromos](https://www.blueletterbible.org/lexicon/g1790)

Definition: tremble (with G1096) (1x), trembling (1x), quake (1x), terrified

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1790)

[Study Light](https://www.studylight.org/lexicons/greek/1790.html)

[Bible Hub](https://biblehub.com/str/greek/1790.htm)

[LSJ](https://lsj.gr/wiki/ἔντρομος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1790)

[Bible Bento](https://biblebento.com/dictionary/G1790.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1790/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1790.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1790)
