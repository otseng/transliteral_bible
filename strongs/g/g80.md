# ἀδελφός

[adelphos](https://www.blueletterbible.org/lexicon/g80)

Definition: brethren (226x), brother (113x), brother's (6x), brother's way (1x), fellow believer, from the same womb

Part of speech: masculine noun

Occurs 346 times in 319 verses

Hebrew: ['ach](../h/h251.md)

Derived words: Philadelphia, [adelphogamy](https://en.wiktionary.org/wiki/adelphogamy), [adelphous](https://en.wiktionary.org/wiki/adelphous), [adelphoparasite](https://www.encyclopedia.com/science/dictionaries-thesauruses-pictures-and-press-releases/adelphoparasite)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0080)

[Study Light](https://www.studylight.org/lexicons/greek/80.html)

[Bible Hub](https://biblehub.com/str/greek/80.htm)

[LSJ](https://lsj.gr/wiki/ἀδελφός)

[NET Bible](http://classic.net.bible.org/strong.php?id=80)

[Bible Bento](https://biblebento.com/dictionary/G0080.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0080/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0080.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g80)

[Bible Researcher](http://www.bible-researcher.com/adelphos.html)

[Douglas Jacoby](https://www.douglasjacoby.com/linguistic-insight-adelphos/)