# ἀρχηγός

[archēgos](https://www.blueletterbible.org/lexicon/g747)

Definition: prince (2x), captain (1x), author (1x), the chief leader

Part of speech: adjective

Occurs 4 times in 4 verses

Greek: [archōn](../g/g758.md)

Hebrew: [paqad](../h/h6485.md), [nāśî'](../h/h5387.md), ['allûp̄](../h/h441.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0747)

[Study Light](https://www.studylight.org/lexicons/greek/747.html)

[Bible Hub](https://biblehub.com/str/greek/747.htm)

[LSJ](https://lsj.gr/wiki/ἀρχηγός)

[NET Bible](http://classic.net.bible.org/strong.php?id=747)

[Bible Bento](https://biblebento.com/dictionary/G0747.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0747/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0747.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g747)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CF%81%CF%87%CE%B7%CE%B3%CF%8C%CF%82)
