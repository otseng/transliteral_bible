# βασανιστής

[basanistēs](https://www.blueletterbible.org/lexicon/g930)

Definition: tormentor (1x), torturer, an inquisitor, torturer also used of a jailer doubtless because the business of torturing was also assigned to him

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0930)

[Study Light](https://www.studylight.org/lexicons/greek/930.html)

[Bible Hub](https://biblehub.com/str/greek/930.htm)

[LSJ](https://lsj.gr/wiki/βασανιστής)

[NET Bible](http://classic.net.bible.org/strong.php?id=930)

[Bible Bento](https://biblebento.com/dictionary/G0930.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0930/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0930.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g930)
