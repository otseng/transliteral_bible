# ἀποτίθημι

[apotithēmi](https://www.blueletterbible.org/lexicon/g659)

Definition: put off (2x), lay aside (2x), lay down (1x), cast off (1x), put away (1x), lay apart (1x).

Part of speech: verb

Occurs 10 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0659)

[Study Light](https://www.studylight.org/lexicons/greek/659.html)

[Bible Hub](https://biblehub.com/str/greek/659.htm)

[LSJ](https://lsj.gr/wiki/ἀποτίθημι)

[NET Bible](http://classic.net.bible.org/strong.php?id=659)

[Bible Bento](https://biblebento.com/dictionary/G0659.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0659/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0659.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g659)
