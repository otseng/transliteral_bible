# Συρία

[Syria](https://www.blueletterbible.org/lexicon/g4947)

Definition: Syria (8x), "exalted"

Part of speech: proper locative noun

Occurs 8 times in 8 verses

Derived words: Syria

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4947)

[Study Light](https://www.studylight.org/lexicons/greek/4947.html)

[Bible Hub](https://biblehub.com/str/greek/4947.htm)

[LSJ](https://lsj.gr/wiki/Συρία)

[NET Bible](http://classic.net.bible.org/strong.php?id=4947)

[Bible Bento](https://biblebento.com/dictionary/G4947.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4947/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4947.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4947)

[Wiktionary](https://en.wiktionary.org/wiki/Syria)

[Wikipedia](https://en.wikipedia.org/wiki/Syria)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/syria.html)

[Video Bible](https://www.videobible.com/bible-dictionary/syria)