# κουφίζω

[kouphizō](https://www.blueletterbible.org/lexicon/g2893)

Definition: lighten (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2893)

[Study Light](https://www.studylight.org/lexicons/greek/2893.html)

[Bible Hub](https://biblehub.com/str/greek/2893.htm)

[LSJ](https://lsj.gr/wiki/κουφίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2893)

[Bible Bento](https://biblebento.com/dictionary/G2893.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2893/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2893.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2893)
