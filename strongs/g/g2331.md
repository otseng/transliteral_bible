# Θεσσαλονικεύς

[Thessalonikeus](https://www.blueletterbible.org/lexicon/g2331)

Definition: Thessalonians (5x), of Thessalonica (1x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2331)

[Study Light](https://www.studylight.org/lexicons/greek/2331.html)

[Bible Hub](https://biblehub.com/str/greek/2331.htm)

[LSJ](https://lsj.gr/wiki/Θεσσαλονικεύς)

[NET Bible](http://classic.net.bible.org/strong.php?id=2331)

[Bible Bento](https://biblebento.com/dictionary/G2331.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2331/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2331.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2331)
