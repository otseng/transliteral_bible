# ἐγκεντρίζω

[egkentrizō](https://www.blueletterbible.org/lexicon/g1461)

Definition: graff in (4x), graff (1x), graff into (1x), graft in

Part of speech: verb

Occurs 6 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1461)

[Study Light](https://www.studylight.org/lexicons/greek/1461.html)

[Bible Hub](https://biblehub.com/str/greek/1461.htm)

[LSJ](https://lsj.gr/wiki/ἐγκεντρίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1461)

[Bible Bento](https://biblebento.com/dictionary/G1461.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1461/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1461.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1461)
