# πρωτεύω

[prōteuō](https://www.blueletterbible.org/lexicon/g4409)

Definition: have the preeminence (1x), to be first, hold the first place

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4409)

[Study Light](https://www.studylight.org/lexicons/greek/4409.html)

[Bible Hub](https://biblehub.com/str/greek/4409.htm)

[LSJ](https://lsj.gr/wiki/πρωτεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4409)

[Bible Bento](https://biblebento.com/dictionary/G4409.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4409/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4409.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4409)
