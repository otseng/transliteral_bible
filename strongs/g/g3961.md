# πατέω

[pateō](https://www.blueletterbible.org/lexicon/g3961)

Definition: tread (3x), tread down (1x), tread under feet (1x), crush with the feet, treat with insult and contempt

Part of speech: verb

Occurs 5 times in 5 ve

Hebrew: [dûš](../h/h1758.md), [dāraḵ](../h/h1869.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3961)

[Study Light](https://www.studylight.org/lexicons/greek/3961.html)

[Bible Hub](https://biblehub.com/str/greek/3961.htm)

[LSJ](https://lsj.gr/wiki/πατέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3961)

[Bible Bento](https://biblebento.com/dictionary/G3961.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3961/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3961.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3961)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%B1%CF%84%CE%AD%CF%89)