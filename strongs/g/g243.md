# ἄλλος

[allos](https://www.blueletterbible.org/lexicon/g243)

Definition: other(s) (81x), another (62x), some (11x), one (4x)

Part of speech: adjective

Occurs 160 times in 141 verses

Greek: [allēlōn](../g/g240.md)

Synonyms: [some](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Some)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0243)

[Study Light](https://www.studylight.org/lexicons/greek/243.html)

[Bible Hub](https://biblehub.com/str/greek/243.htm)

[LSJ](https://lsj.gr/wiki/ἄλλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=243)

[Bible Bento](https://biblebento.com/dictionary/G0243.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0243/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0243.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g243)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%84%CE%BB%CE%BB%CE%BF%CF%82)
