# ἀνεμίζω

[anemizō](https://www.blueletterbible.org/lexicon/g416)

Definition: driven with the wind (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0416)

[Study Light](https://www.studylight.org/lexicons/greek/416.html)

[Bible Hub](https://biblehub.com/str/greek/416.htm)

[LSJ](https://lsj.gr/wiki/ἀνεμίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=416)

[Bible Bento](https://biblebento.com/dictionary/G416.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/416/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/416.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g416)
