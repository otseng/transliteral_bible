# δολόω

[doloō](https://www.blueletterbible.org/lexicon/g1389)

Definition: handle ... deceitfully (1x), to corrupt, adulterate, divine truth by mingling with it wrong notions

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1389)

[Study Light](https://www.studylight.org/lexicons/greek/1389.html)

[Bible Hub](https://biblehub.com/str/greek/1389.htm)

[LSJ](https://lsj.gr/wiki/δολόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1389)

[Bible Bento](https://biblebento.com/dictionary/G1389.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1389/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1389.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1389)

[Precept Austin](https://www.preceptaustin.org/2corinthians_41-2_commentary#h)
