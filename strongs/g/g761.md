# ἀσάλευτος

[asaleutos](https://www.blueletterbible.org/lexicon/g761)

Definition: unmoveable (1x), which cannot be moved (1x), unshaken, not liable to overthrow and disorder, firm stable

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0761)

[Study Light](https://www.studylight.org/lexicons/greek/761.html)

[Bible Hub](https://biblehub.com/str/greek/761.htm)

[LSJ](https://lsj.gr/wiki/ἀσάλευτος)

[NET Bible](http://classic.net.bible.org/strong.php?id=761)

[Bible Bento](https://biblebento.com/dictionary/G0761.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0761/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0761.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g761)
