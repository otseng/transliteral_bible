# διαλογισμός

[dialogismos](https://www.blueletterbible.org/lexicon/g1261)

Definition: thought (9x), reasoning (1x), imagination (1x), doubtful (1x), disputing (1x), doubting (1x), imagining, discussion, meditation, disputing

Part of speech: masculine noun

Occurs 14 times in 14 verses

Derived words: dialogue

Hebrew: [maḥăšāḇâ](../h/h4284.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1261)

[Study Light](https://www.studylight.org/lexicons/greek/1261.html)

[Bible Hub](https://biblehub.com/str/greek/1261.htm)

[LSJ](https://lsj.gr/wiki/διαλογισμός)

[NET Bible](http://classic.net.bible.org/strong.php?id=1261)

[Bible Bento](https://biblebento.com/dictionary/G1261.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1261/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1261.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1261)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B4%CE%B9%CE%B1%CE%BB%CE%BF%CE%B3%CE%AF%CE%B6%CE%BF%CE%BC%CE%B1%CE%B9)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34727)

[Precept Austin](https://www.preceptaustin.org/philippians_214-16#disputing)