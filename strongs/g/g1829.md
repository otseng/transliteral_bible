# ἐξέραμα

[exerama](https://www.blueletterbible.org/lexicon/g1829)

Definition: vomit (1x).

Part of speech: neuter noun

Occurs 1 times in 1 verses

Hebrew: [qî'](../h/h6958.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1829)

[Study Light](https://www.studylight.org/lexicons/greek/1829.html)

[Bible Hub](https://biblehub.com/str/greek/1829.htm)

[LSJ](https://lsj.gr/wiki/ἐξέραμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=1829)

[Bible Bento](https://biblebento.com/dictionary/G1829.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1829/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1829.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1829)

