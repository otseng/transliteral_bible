# λογεία

[logeia](https://www.blueletterbible.org/lexicon/g3048)

Definition: collection (1x), gatherings (1x), contribution, money gathered for the relief of the poor

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3048)

[Study Light](https://www.studylight.org/lexicons/greek/3048.html)

[Bible Hub](https://biblehub.com/str/greek/3048.htm)

[LSJ](https://lsj.gr/wiki/λογεία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3048)

[Bible Bento](https://biblebento.com/dictionary/G3048.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3048/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3048.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3048)
