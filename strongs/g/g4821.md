# συμβασιλεύω

[symbasileuō](https://www.blueletterbible.org/lexicon/g4821)

Definition: reign with (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4821)

[Study Light](https://www.studylight.org/lexicons/greek/4821.html)

[Bible Hub](https://biblehub.com/str/greek/4821.htm)

[LSJ](https://lsj.gr/wiki/συμβασιλεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4821)

[Bible Bento](https://biblebento.com/dictionary/G4821.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4821/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4821.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4821)
