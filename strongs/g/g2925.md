# κρούω

[krouō](https://www.blueletterbible.org/lexicon/g2925)

Definition: knock (9x): at the door, rap

Part of speech: verb

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2925)

[Study Light](https://www.studylight.org/lexicons/greek/2925.html)

[Bible Hub](https://biblehub.com/str/greek/2925.htm)

[LSJ](https://lsj.gr/wiki/κρούω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2925)

[Bible Bento](https://biblebento.com/dictionary/G2925.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2925/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2925.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2925)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CF%81%CE%BF%CF%8D%CF%89)