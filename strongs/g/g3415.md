# μνάομαι

[mnaomai](https://www.blueletterbible.org/lexicon/g3415)

Definition: remember (16x), be mindful (2x), be had in remembrance (1x), in remembrance (1x), come in remembrance (1x), remind, be mindful, in remembrance

Part of speech: verb

Occurs 21 times in 21 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3415)

[Study Light](https://www.studylight.org/lexicons/greek/3415.html)

[Bible Hub](https://biblehub.com/str/greek/3415.htm)

[LSJ](https://lsj.gr/wiki/μνάομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=3415)

[Bible Bento](https://biblebento.com/dictionary/G3415.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3415/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3415.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3415)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BC%CE%BD%CE%AC%CE%BF%CE%BC%CE%B1%CE%B9)