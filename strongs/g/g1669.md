# ἑλκόω

[helkoō](https://www.blueletterbible.org/lexicon/g1669)

Definition: full of sores (1x), to be ulcerated

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1669)

[Study Light](https://www.studylight.org/lexicons/greek/1669.html)

[Bible Hub](https://biblehub.com/str/greek/1669.htm)

[LSJ](https://lsj.gr/wiki/ἑλκόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1669)

[Bible Bento](https://biblebento.com/dictionary/G1669.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1669/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1669.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1669)