# καταδιώκω

[katadiōkō](https://www.blueletterbible.org/lexicon/g2614)

Definition: follow after (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [dalaq](../h/h1814.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2614)

[Study Light](https://www.studylight.org/lexicons/greek/2614.html)

[Bible Hub](https://biblehub.com/str/greek/2614.htm)

[LSJ](https://lsj.gr/wiki/καταδιώκω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2614)

[Bible Bento](https://biblebento.com/dictionary/G2614.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2614/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2614.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2614)

