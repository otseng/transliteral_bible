# ὑπομονή

[hypomonē](https://www.blueletterbible.org/lexicon/g5281)

Definition: patience (29x), enduring (1x), patient continuance (1x), patient waiting (1x), steadfastness, constancy, endurance, perseverance

Part of speech: feminine noun

Occurs 32 times in 31 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5281)

[Study Light](https://www.studylight.org/lexicons/greek/5281.html)

[Bible Hub](https://biblehub.com/str/greek/5281.htm)

[LSJ](https://lsj.gr/wiki/ὑπομονή)

[NET Bible](http://classic.net.bible.org/strong.php?id=5281)

[Bible Bento](https://biblebento.com/dictionary/G5281.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5281/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5281.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5281)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BD%91%CF%80%CE%BF%CE%BC%CE%BF%CE%BD%CE%AE)