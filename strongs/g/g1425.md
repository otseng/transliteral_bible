# δυσνόητος

[dysnoētos](https://www.blueletterbible.org/lexicon/g1425)

Definition: hard to be understood (1x), difficult of perception

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1425)

[Study Light](https://www.studylight.org/lexicons/greek/1425.html)

[Bible Hub](https://biblehub.com/str/greek/1425.htm)

[LSJ](https://lsj.gr/wiki/δυσνόητος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1425)

[Bible Bento](https://biblebento.com/dictionary/G1425.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1425/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1425.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1425)

