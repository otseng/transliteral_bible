# προστάσσω

[prostassō](https://www.blueletterbible.org/lexicon/g4367)

Definition:  command (6x), bid (1x), to assign or ascribe to, join to, to appoint, to define, prescribe

Part of speech: verb

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4367)

[Study Light](https://www.studylight.org/lexicons/greek/4367.html)

[Bible Hub](https://biblehub.com/str/greek/4367.htm)

[LSJ](https://lsj.gr/wiki/προστάσσω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4367)

[Bible Bento](https://biblebento.com/dictionary/G4367.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4367/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4367.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4367)

[Precept Austin](https://www.preceptaustin.org/acts-17-commentary#appointed)