# καταλέγω

[katalegō](https://www.blueletterbible.org/lexicon/g2639)

Definition: take into the number (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2639)

[Study Light](https://www.studylight.org/lexicons/greek/2639.html)

[Bible Hub](https://biblehub.com/str/greek/2639.htm)

[LSJ](https://lsj.gr/wiki/καταλέγω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2639)

[Bible Bento](https://biblebento.com/dictionary/G2639.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2639/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2639.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2639)

