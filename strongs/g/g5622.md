# ὠφέλεια

[ōpheleia](https://www.blueletterbible.org/lexicon/g5622)

Definition: profit (1x), advantage (1x), usefulness, benefit

Part of speech: feminine noun

Occurs 2 times in 2 verses

Usage: [Ophelia](https://en.wikipedia.org/wiki/Ophelia) 

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5622)

[Study Light](https://www.studylight.org/lexicons/greek/5622.html)

[Bible Hub](https://biblehub.com/str/greek/5622.htm)

[LSJ](https://lsj.gr/wiki/ὠφέλεια)

[NET Bible](http://classic.net.bible.org/strong.php?id=5622)

[Bible Bento](https://biblebento.com/dictionary/G5622.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5622/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5622.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5622)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BD%A0%CF%86%CE%AD%CE%BB%CE%B5%CE%B9%CE%B1)