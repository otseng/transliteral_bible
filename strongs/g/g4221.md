# ποτήριον

[potērion](https://www.blueletterbible.org/lexicon/g4221)

Definition: cup (33x), fate, one's lot or experience

Part of speech: neuter noun

Occurs 33 times in 30 verses

Hebrew: [kowc](../h/h3563.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4221)

[Study Light](https://www.studylight.org/lexicons/greek/4221.html)

[Bible Hub](https://biblehub.com/str/greek/4221.htm)

[LSJ](https://lsj.gr/wiki/ποτήριον)

[NET Bible](http://classic.net.bible.org/strong.php?id=4221)

[Bible Bento](https://biblebento.com/dictionary/G4221.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4221/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4221.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4221)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%BF%CF%84%CE%AE%CF%81%CE%B9%CE%BF%CE%BD)