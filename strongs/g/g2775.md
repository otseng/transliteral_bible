# κεφαλιόω

[kephalioō](https://www.blueletterbible.org/lexicon/g2775)

Definition: wound in the head (1x), to smite or wound in the head, to smite on the cheek

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2775)

[Study Light](https://www.studylight.org/lexicons/greek/2775.html)

[Bible Hub](https://biblehub.com/str/greek/2775.htm)

[LSJ](https://lsj.gr/wiki/κεφαλιόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2775)

[Bible Bento](https://biblebento.com/dictionary/G2775.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2775/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2775.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2775)