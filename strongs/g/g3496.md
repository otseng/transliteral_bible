# Νέα πόλις

[Nea Polis](https://www.blueletterbible.org/lexicon/g3496)

Definition: Neapolis (1x), "new city", a maritime city of Macedonia, on the gulf of Syrymon, having a port and colonised by Chalcidians

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3496)

[Study Light](https://www.studylight.org/lexicons/greek/3496.html)

[Bible Hub](https://biblehub.com/str/greek/3496.htm)

[LSJ](https://lsj.gr/wiki/Νέα πόλις)

[NET Bible](http://classic.net.bible.org/strong.php?id=3496)

[Bible Bento](https://biblebento.com/dictionary/G3496.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3496/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3496.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3496)

[Wikipedia](https://en.wikipedia.org/wiki/Neapolis_%28Thrace%29)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/neapolis.html)

[Video Bible](https://www.videobible.com/bible-dictionary/neapolis)