# Ἰωάννης

[Iōannēs](https://www.blueletterbible.org/lexicon/g2491)

Definition: John (the Baptist) (92x), John (the apostle) (36x), John (Mark) (4x), John (the chief priest) (1x), "Jehovah is a gracious giver"

Part of speech: proper masculine noun

Occurs 133 times in 130 verses

Hebrew: [Yôḥānān](../h/h3110.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2491)

[Study Light](https://www.studylight.org/lexicons/greek/2491.html)

[Bible Hub](https://biblehub.com/str/greek/2491.htm)

[LSJ](https://lsj.gr/wiki/Ἰωάννης)

[NET Bible](http://classic.net.bible.org/strong.php?id=2491)

[Bible Bento](https://biblebento.com/dictionary/G2491.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2491/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2491.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2491)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/john.html)

[Video Bible](https://www.videobible.com/bible-dictionary/john)