# Λασαία

[Lasaia](https://www.blueletterbible.org/lexicon/g2996)

Definition: Lasea (1x), "shaggy", a city in Crete on the coast near Fair Havens

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2996)

[Study Light](https://www.studylight.org/lexicons/greek/2996.html)

[Bible Hub](https://biblehub.com/str/greek/2996.htm)

[LSJ](https://lsj.gr/wiki/Λασαία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2996)

[Bible Bento](https://biblebento.com/dictionary/G2996.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2996/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2996.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2996)

[Video Bible](https://www.videobible.com/bible-dictionary/lasea)