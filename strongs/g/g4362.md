# προσπήγνυμι

[prospēgnymi](https://www.blueletterbible.org/lexicon/g4362)

Definition: crucify (1x), fasten, impale

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4362)

[Study Light](https://www.studylight.org/lexicons/greek/4362.html)

[Bible Hub](https://biblehub.com/str/greek/4362.htm)

[LSJ](https://lsj.gr/wiki/προσπήγνυμι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4362)

[Bible Bento](https://biblebento.com/dictionary/G4362.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4362/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4362.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4362)
