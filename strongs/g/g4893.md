# συνείδησις

[syneidēsis](https://www.blueletterbible.org/lexicon/g4893)

Definition: conscience (32x), moral consciousness

Part of speech: feminine noun

Occurs 32 times in 30 verses

Greek: [synoraō](../g/g4894.md)

Hebrew: [madāʿ](../h/h4093.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4893)

[Study Light](https://www.studylight.org/lexicons/greek/4893.html)

[Bible Hub](https://biblehub.com/str/greek/4893.htm)

[LSJ](https://lsj.gr/wiki/συνείδησις)

[NET Bible](http://classic.net.bible.org/strong.php?id=4893)

[Bible Bento](https://biblebento.com/dictionary/G4893.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4893/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4893.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4893)

[Menno Knight](https://mennoknight.wordpress.com/2014/04/19/a-biblical-exploration-of-the-term-conscience/)