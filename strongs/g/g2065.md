# ἐρωτάω

[erōtaō](https://www.blueletterbible.org/lexicon/g2065)

Definition: ask (23x), beseech (14x), pray (14x), desire (6x), intreat (1x), question, beg, entreat, solicit

Part of speech: verb

Occurs 59 times in 57 verses

Greek: [eperōtaō](../g/g1905.md)

Hebrew: [sha'al](../h/h7592.md)

Derived words: [erotesis](https://www.wordnik.com/words/erotesis), [erotetic](https://www.wordnik.com/words/erotetic)

Synonyms: [pray](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Pray)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2065)

[Study Light](https://www.studylight.org/lexicons/greek/2065.html)

[Bible Hub](https://biblehub.com/str/greek/2065.htm)

[LSJ](https://lsj.gr/wiki/ἐρωτάω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2065)

[Bible Bento](https://biblebento.com/dictionary/G2065.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2065/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2065.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2065)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%90%CF%81%CF%89%CF%84%CE%AC%CF%89)

[Study Light - figures of speech](https://www.studylight.org/lexicons/bullinger/erotesis-or-interrogating.html)
