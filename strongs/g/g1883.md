# ἐπάνω

[epanō](https://www.blueletterbible.org/lexicon/g1883)

Definition: over (6x), on (4x), thereon (with G846) (3x), upon (3x), above (3x), more than (1x).

Part of speech: adverb

Occurs 20 times in 18 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1883)

[Study Light](https://www.studylight.org/lexicons/greek/1883.html)

[Bible Hub](https://biblehub.com/str/greek/1883.htm)

[LSJ](https://lsj.gr/wiki/ἐπάνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1883)

[Bible Bento](https://biblebento.com/dictionary/G1883.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1883/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1883.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1883)

