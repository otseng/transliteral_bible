# ζηλωτής

[zēlōtēs](https://www.blueletterbible.org/lexicon/g2207)

Definition: zealous (5x), most eagerly desirous of, vehemently contending for a thing

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2207)

[Study Light](https://www.studylight.org/lexicons/greek/2207.html)

[Bible Hub](https://biblehub.com/str/greek/2207.htm)

[LSJ](https://lsj.gr/wiki/ζηλωτής)

[NET Bible](http://classic.net.bible.org/strong.php?id=2207)

[Bible Bento](https://biblebento.com/dictionary/G2207.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2207/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2207.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2207)
