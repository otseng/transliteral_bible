# Σίμων

[Simōn](https://www.blueletterbible.org/lexicon/g4613)

Definition: Simon (Peter) (49x), Simon (Zelotes) (4x), Simon (father of Judas) (4x), Simon (Magus) (4x), Simon (the tanner) (4x), Simon (the Pharisee) (3x), Simon (of Cyrene) (3x), Simon (brother of Jesus) (2x), Simon (the leper) (2x)

Part of speech: proper masculine noun

Occurs 75 times in 70 verses

Hebrew: [Šimʿôn](../h/h8095.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4613)

[Study Light](https://www.studylight.org/lexicons/greek/4613.html)

[Bible Hub](https://biblehub.com/str/greek/4613.htm)

[LSJ](https://lsj.gr/wiki/Σίμων)

[NET Bible](http://classic.net.bible.org/strong.php?id=4613)

[Bible Bento](https://biblebento.com/dictionary/G4613.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4613/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4613.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4613)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/simon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/simon)