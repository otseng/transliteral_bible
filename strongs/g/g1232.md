# διαγνωρίζω

[diagnōrizō](https://www.blueletterbible.org/lexicon/g1232)

Definition: make known abroad (1x), to discriminate

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1232)

[Study Light](https://www.studylight.org/lexicons/greek/1232.html)

[Bible Hub](https://biblehub.com/str/greek/1232.htm)

[LSJ](https://lsj.gr/wiki/διαγνωρίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1232)

[Bible Bento](https://biblebento.com/dictionary/G1232.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1232/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1232.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1232)