# ἀπέχω

[apechō](https://www.blueletterbible.org/lexicon/g568)

Definition: be (5x), have (4x), receive (2x), to have wholly or in full, to have received, it is enough, sufficient, to be away, absent, distant, to hold one's self off, abstain

Part of speech: verb

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0568)

[Study Light](https://www.studylight.org/lexicons/greek/568.html)

[Bible Hub](https://biblehub.com/str/greek/568.htm)

[LSJ](https://lsj.gr/wiki/ἀπέχω)

[NET Bible](http://classic.net.bible.org/strong.php?id=568)

[Bible Bento](https://biblebento.com/dictionary/G0568.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0568/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0568.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g568)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CF%80%CE%AD%CF%87%CF%89)
