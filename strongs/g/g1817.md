# ἐξανίστημι

[exanistēmi](https://www.blueletterbible.org/lexicon/g1817)

Definition: raise up (2x), rise up (1x)

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1817)

[Study Light](https://www.studylight.org/lexicons/greek/1817.html)

[Bible Hub](https://biblehub.com/str/greek/1817.htm)

[LSJ](https://lsj.gr/wiki/ἐξανίστημι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1817)

[Bible Bento](https://biblebento.com/dictionary/G1817.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1817/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1817.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1817)
