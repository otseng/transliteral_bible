# Περσίς

[persis](https://www.blueletterbible.org/lexicon/g4069)

Definition: Persis = a Persian woman, a Christian woman at Rome

Part of speech: proper feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4069)

[Study Light](https://www.studylight.org/lexicons/greek/4069.html)

[Bible Hub](https://biblehub.com/str/greek/4069.htm)

[LSJ](https://lsj.gr/wiki/Περσίς)

[NET Bible](http://classic.net.bible.org/strong.php?id=4069)

[Bible Bento](https://biblebento.com/dictionary/G4069.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4069/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4069.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4069)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/persis.html)

[Video Bible](https://www.videobible.com/bible-dictionary/persis)