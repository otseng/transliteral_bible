# συνοχή

[synochē](https://www.blueletterbible.org/lexicon/g4928)

Definition: distress (1x), anguish (1x), holding together, a narrowing, anxiety

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4928)

[Study Light](https://www.studylight.org/lexicons/greek/4928.html)

[Bible Hub](https://biblehub.com/str/greek/4928.htm)

[LSJ](https://lsj.gr/wiki/συνοχή)

[NET Bible](http://classic.net.bible.org/strong.php?id=4928)

[Bible Bento](https://biblebento.com/dictionary/G4928.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4928/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4928.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4928)
