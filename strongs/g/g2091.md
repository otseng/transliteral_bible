# ἑτοιμασία

[hetoimasia](https://www.blueletterbible.org/lexicon/g2091)

Definition: preparation (1x), readiness

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2091)

[Study Light](https://www.studylight.org/lexicons/greek/2091.html)

[Bible Hub](https://biblehub.com/str/greek/2091.htm)

[LSJ](https://lsj.gr/wiki/ἑτοιμασία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2091)

[Bible Bento](https://biblebento.com/dictionary/G2091.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2091/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2091.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2091)
