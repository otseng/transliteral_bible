# Ἀμώς

[Amōs](https://www.blueletterbible.org/lexicon/g301)

Definition: Amos (1x), "burden"

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0301)

[Study Light](https://www.studylight.org/lexicons/greek/0301.html)

[LSJ](https://lsj.gr/wiki/Ἀμώς)

[NET Bible](http://classic.net.bible.org/strong.php?id=301)

[Bible Bento](https://biblebento.com/dictionary/G0301.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0301/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0301.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g301)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/amos.html)

[Video Bible](https://www.videobible.com/bible-dictionary/amos)