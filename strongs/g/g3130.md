# μανία

[mania](https://www.blueletterbible.org/lexicon/g3130)

Definition: mad (1x), madness, frenzy, enthusiasm, rage, fury, excitement, insanity

Part of speech: feminine noun

Occurs 1 times in 1 verses

Derived words: mania

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3130)

[Study Light](https://www.studylight.org/lexicons/greek/3130.html)

[Bible Hub](https://biblehub.com/str/greek/3130.htm)

[LSJ](https://lsj.gr/wiki/μανία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3130)

[Bible Bento](https://biblebento.com/dictionary/G3130.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3130/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3130.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3130)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BC%CE%B1%CE%BD%CE%AF%CE%B1)

[Precept Austin](https://www.preceptaustin.org/acts-26-commentary#mad)
