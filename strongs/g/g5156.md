# τρόμος

[tromos](https://www.blueletterbible.org/lexicon/g5156)

Definition: trembling (4x), tremble (with G2192) (1x), quaking with fear, quivering, terror

Part of speech: masculine noun

Occurs 5 times in 5 verses

Greek: [tremō](../g/g5141.md)

Hebrew: [ra'ad](../h/h7461.md), [rᵊṯēṯ](../h/h7578.md)

Derived words: tremor

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5156)

[Study Light](https://www.studylight.org/lexicons/greek/5156.html)

[Bible Hub](https://biblehub.com/str/greek/5156.htm)

[LSJ](https://lsj.gr/wiki/τρόμος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5156)

[Bible Bento](https://biblebento.com/dictionary/G5156.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5156/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5156.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5156)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%84%CF%81%CF%8C%CE%BC%CE%BF%CF%82)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=35977)