# διάβολος

[diabolos](https://www.blueletterbible.org/lexicon/g1228)

Definition: devil (35x), false accuser (2x), slanderer (1x)

Part of speech: adjective

Occurs 38 times in 36 verses

Greek: [ballō](../g/g906.md)

Hebrew: [tsar](../h/h6862.md), [satan](../h/h7854.md)

Derived words: diabolical

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1228)

[Study Light](https://www.studylight.org/lexicons/greek/1228.html)

[Bible Hub](https://biblehub.com/str/greek/1228.htm)

[LSJ](https://lsj.gr/wiki/διάβολος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1228)

[Bible Bento](https://biblebento.com/dictionary/G1228.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1228/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1228.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1228)

[Learn Greek Online](https://learn-greek-online.com/ask-greek/677/diavallein-dia-ballein-dia-ballo)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/1228-diabolos-devil.htm)
