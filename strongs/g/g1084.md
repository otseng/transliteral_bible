# γεννητός

[gennētos](https://www.blueletterbible.org/lexicon/g1084)

Definition: that is born (2x), begotten

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1084)

[Study Light](https://www.studylight.org/lexicons/greek/1084.html)

[Bible Hub](https://biblehub.com/str/greek/1084.htm)

[LSJ](https://lsj.gr/wiki/γεννητός)

[NET Bible](http://classic.net.bible.org/strong.php?id=1084)

[Bible Bento](https://biblebento.com/dictionary/G1084.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1084/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1084.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1084)