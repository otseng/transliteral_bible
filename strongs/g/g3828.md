# Παμφυλία

[Pamphylia](https://www.blueletterbible.org/lexicon/g3828)

Definition: Pamphylia (5x), "of every tribe", a province in Asia Minor, bounded on the east by Cilicia, on the west by Lycia and Phrygia Minor, on the north by Galatia and Cappadocia, and on the south by the Mediterranean Sea

Part of speech: proper locative noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3828)

[Study Light](https://www.studylight.org/lexicons/greek/3828.html)

[Bible Hub](https://biblehub.com/str/greek/3828.htm)

[LSJ](https://lsj.gr/wiki/Παμφυλία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3828)

[Bible Bento](https://biblebento.com/dictionary/G3828.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3828/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3828.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3828)

[Wikipedia](https://en.wikipedia.org/wiki/Pamphylia)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/pamphylia.html)

[Video Bible](https://www.videobible.com/bible-dictionary/pamphylia)