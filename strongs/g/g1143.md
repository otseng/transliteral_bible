# δάκνω

[daknō](https://www.blueletterbible.org/lexicon/g1143)

Definition: bite (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [nāšaḵ](../h/h5391.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1143)

[Study Light](https://www.studylight.org/lexicons/greek/1143.html)

[Bible Hub](https://biblehub.com/str/greek/1143.htm)

[LSJ](https://lsj.gr/wiki/δάκνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1143)

[Bible Bento](https://biblebento.com/dictionary/G1143.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1143/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1143.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1143)
