# κλυδωνίζομαι

[klydōnizomai](https://www.blueletterbible.org/lexicon/g2831)

Definition: toss to and fro (1x), fluctuate, rock, sway

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2831)

[Study Light](https://www.studylight.org/lexicons/greek/2831.html)

[Bible Hub](https://biblehub.com/str/greek/2831.htm)

[LSJ](https://lsj.gr/wiki/κλυδωνίζομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=2831)

[Bible Bento](https://biblebento.com/dictionary/G2831.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2831/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2831.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2831)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%BB%CF%85%CE%B4%CF%89%CE%BD%CE%AF%CE%B6%CE%BF%CE%BC%CE%B1%CE%B9)
