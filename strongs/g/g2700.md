# κατατοξεύω

[katatoxeuō](https://www.blueletterbible.org/lexicon/g2700)

Definition: thrust through (1x), shoot down with an arrow 

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2700)

[Study Light](https://www.studylight.org/lexicons/greek/2700.html)

[Bible Hub](https://biblehub.com/str/greek/2700.htm)

[LSJ](https://lsj.gr/wiki/κατατοξεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2700)

[Bible Bento](https://biblebento.com/dictionary/G2700.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2700/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2700.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2700)
