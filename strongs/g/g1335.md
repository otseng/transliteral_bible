# διήγησις

[diēgēsis](https://www.blueletterbible.org/lexicon/g1335)

Definition: declaration (1x), narrative

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1335)

[Study Light](https://www.studylight.org/lexicons/greek/1335.html)

[Bible Hub](https://biblehub.com/str/greek/1335.htm)

[LSJ](https://lsj.gr/wiki/διήγησις)

[NET Bible](http://classic.net.bible.org/strong.php?id=1335)

[Bible Bento](https://biblebento.com/dictionary/G1335.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1335/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1335.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1335)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B4%CE%B9%CE%AE%CE%B3%CE%B7%CF%83%CE%B9%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Diegesis)