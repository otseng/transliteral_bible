# ἐφίστημι

[ephistēmi](https://www.blueletterbible.org/lexicon/g2186)

Definition: come upon (6x), come (4x), stand (3x), stand by (3x)

Part of speech: verb

Occurs 22 times in 21 verses

Hebrew: [camak](../h/h5564.md)

Synonyms: [come](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Come)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2186)

[Study Light](https://www.studylight.org/lexicons/greek/2186.html)

[Bible Hub](https://biblehub.com/str/greek/2186.htm)

[LSJ](https://lsj.gr/wiki/ἐφίστημι)

[NET Bible](http://classic.net.bible.org/strong.php?id=2186)

[Bible Bento](https://biblebento.com/dictionary/G2186.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2186/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2186.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2186)
