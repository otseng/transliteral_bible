# ἐκδίδωμι

[ekdidōmi](https://www.blueletterbible.org/lexicon/g1554)

Definition: let out (3x), let forth (1x), to give out, give up, give over, farm out, to let out for hire, to let out for one's advantage

Part of speech: verb

Occurs 7 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1554)

[Study Light](https://www.studylight.org/lexicons/greek/1554.html)

[Bible Hub](https://biblehub.com/str/greek/1554.htm)

[LSJ](https://lsj.gr/wiki/ἐκδίδωμι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1554)

[Bible Bento](https://biblebento.com/dictionary/G1554.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1554/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1554.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1554)
