# μισθός

[misthos](https://www.blueletterbible.org/lexicon/g3408)

Definition: reward (24x), hire (3x), wages (2x), hire, dues paid

Part of speech: masculine noun

Occurs 29 times in 28 verses

Hebrew: [śāḵār](../h/h7939.md), [gamal](../h/h1580.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3408)

[Study Light](https://www.studylight.org/lexicons/greek/3408.html)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BC%CE%B9%CF%83%CE%B8%CF%8C%CF%82)

[Bible Hub](https://biblehub.com/topical/w/wages.htm)

[LSJ](https://lsj.gr/wiki/μισθός)

[NET Bible](http://classic.net.bible.org/strong.php?id=3408)

[Bible Bento](https://biblebento.com/dictionary/G3408.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3408/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3408.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3408)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=35434)
