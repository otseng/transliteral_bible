# παραγγελία

[paraggelia](https://www.blueletterbible.org/lexicon/g3852)

Definition: commandment (2x), charge (2x), straitly (1x)

Part of speech: feminine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3852)

[Study Light](https://www.studylight.org/lexicons/greek/3852.html)

[Bible Hub](https://biblehub.com/str/greek/3852.htm)

[LSJ](https://lsj.gr/wiki/παραγγελία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3852)

[Bible Bento](https://biblebento.com/dictionary/G3852.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3852/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3852.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3852)
