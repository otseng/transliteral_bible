# ἔλαιον

[elaion](https://www.blueletterbible.org/lexicon/g1637)

Definition: oil (11x), olive oil

Part of speech: neuter noun

Occurs 11 times in 11 verses

Hebrew: [šemen](../h/h8081.md), [yiṣhār](../h/h3323.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1637)

[Study Light](https://www.studylight.org/lexicons/greek/1637.html)

[Bible Hub](https://biblehub.com/str/greek/1637.htm)

[LSJ](https://lsj.gr/wiki/ἔλαιον)

[NET Bible](http://classic.net.bible.org/strong.php?id=1637)

[Bible Bento](https://biblebento.com/dictionary/G1637.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1637/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1637.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1637)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%94%CE%BB%CE%B1%CE%B9%CE%BF%CE%BD)