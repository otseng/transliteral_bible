# ἀναφαίνω

[anaphainō](https://www.blueletterbible.org/lexicon/g398)

Definition: appear (1x), discover (1x), to bring to light, hold up to view, show, be made apparent

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0398)

[Study Light](https://www.studylight.org/lexicons/greek/398.html)

[Bible Hub](https://biblehub.com/str/greek/398.htm)

[LSJ](https://lsj.gr/wiki/ἀναφαίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=398)

[Bible Bento](https://biblebento.com/dictionary/G0398.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0398/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0398.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g398)