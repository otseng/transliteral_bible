# συκοφαντέω

[sykophanteō](https://www.blueletterbible.org/lexicon/g4811)

Definition: accuse falsely (1x), take by false accusation (1x), to accuse wrongfully, to calumniate, to attack by malicious devices, to exact money wrongfully, to extort from, defraud

Part of speech: verb

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4811)

[Study Light](https://www.studylight.org/lexicons/greek/4811.html)

[Bible Hub](https://biblehub.com/str/greek/4811.htm)

[LSJ](https://lsj.gr/wiki/συκοφαντέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4811)

[Bible Bento](https://biblebento.com/dictionary/G4811.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4811/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4811.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4811)