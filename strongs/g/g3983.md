# πεινάω

[peinaō](https://www.blueletterbible.org/lexicon/g3983)

Definition: hunger (10x), be an hungred (9x), be hungry (3x), hungry (1x), be in need, crave

Part of speech: verb

Occurs 24 times in 23 verses

Greek: [penēs](../g/g3993.md)

Hebrew: [rāʿēḇ](../h/h7456.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3983)

[Study Light](https://www.studylight.org/lexicons/greek/3983.html)

[Bible Hub](https://biblehub.com/str/greek/3983.htm)

[LSJ](https://lsj.gr/wiki/πεινάω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3983)

[Bible Bento](https://biblebento.com/dictionary/G3983.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3983/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3983.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3983)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%B5%CE%B9%CE%BD%CE%AC%CF%89)
