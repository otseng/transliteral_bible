# ἐκτός

[ektos](https://www.blueletterbible.org/lexicon/g1622)

Definition: out of (2x), outside (1x), other than (1x), without (1x), be excepted (1x), except (with G1508) (1x), unless (with G1508) (1x), but (with G1508) (1x).

Part of speech: adverb

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1622)

[Study Light](https://www.studylight.org/lexicons/greek/1622.html)

[Bible Hub](https://biblehub.com/str/greek/1622.htm)

[LSJ](https://lsj.gr/wiki/ἐκτός)

[NET Bible](http://classic.net.bible.org/strong.php?id=1622)

[Bible Bento](https://biblebento.com/dictionary/G1622.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1622/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1622.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1622)
