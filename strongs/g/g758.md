# ἄρχων

[archōn](https://www.blueletterbible.org/lexicon/g758)

Definition: ruler (22x), prince (11x), chief (2x), magistrate (1x), chief ruler (1x), commander

Part of speech: masculine noun

Occurs 37 times in 36 verses

Greek: [archēgos](../g/g747.md)

Hebrew: [śar](../h/h8269.md), [nāḡîḏ](../h/h5057.md), [nāśî'](../h/h5387.md)

Derived words: monarchy, [-archy](https://en.wiktionary.org/wiki/Category:English_words_suffixed_with_-archy)

Synonyms: [ruler](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Ruler)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0758)

[Study Light](https://www.studylight.org/lexicons/greek/758.html)

[Bible Hub](https://biblehub.com/str/greek/758.htm)

[LSJ](https://lsj.gr/wiki/ἄρχων)

[NET Bible](http://classic.net.bible.org/strong.php?id=758)

[Bible Bento](https://biblebento.com/dictionary/G0758.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0758/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0758.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g758)

[Wikipedia](https://en.wikipedia.org/wiki/Archon)

[Oxford Classical Dictionary](http://oxfordre.com/classics/view/10.1093/acrefore/9780199381135.001.0001/acrefore-9780199381135-e-695#acrefore-9780199381135-e-695)

[Saints and Shepherds](https://ofsaintsandshepherds.com/word-studies/authority-words/rule/archon/)