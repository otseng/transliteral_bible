# λογισμός

[logismos](https://www.blueletterbible.org/lexicon/g3053)

Definition: thought (1x), imagination (1x), a reckoning, computation, a judgment, decision

Part of speech: masculine noun

Occurs 2 times in 2 verses

Hebrew: [maḥăšāḇâ](../h/h4284.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3053)

[Study Light](https://www.studylight.org/lexicons/greek/3053.html)

[Bible Hub](https://biblehub.com/str/greek/3053.htm)

[LSJ](https://lsj.gr/wiki/λογισμός)

[NET Bible](http://classic.net.bible.org/strong.php?id=3053)

[Bible Bento](https://biblebento.com/dictionary/G3053.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3053/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3053.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3053)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BB%CE%BF%CE%B3%CE%B9%CF%83%CE%BC%CF%8C%CF%82)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=35850)

[Precept Austin](https://www.preceptaustin.org/2corinthians_103-5_exposition#s)
