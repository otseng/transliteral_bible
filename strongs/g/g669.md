# ἀποφθέγγομαι

[apophtheggomai](https://www.blueletterbible.org/lexicon/g669)

Definition: utterance (1x), speak forth (1x), say (1x), pronounce, boldly declare

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0669)

[Study Light](https://www.studylight.org/lexicons/greek/669.html)

[Bible Hub](https://biblehub.com/str/greek/669.htm)

[LSJ](https://lsj.gr/wiki/ἀποφθέγγομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=669)

[Bible Bento](https://biblebento.com/dictionary/G0669.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0669/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0669.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g669)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CF%80%CE%BF%CF%86%CE%B8%CE%AD%CE%B3%CE%B3%CE%BF%CE%BC%CE%B1%CE%B9)
