# πρεσβύτερος

[presbyteros](https://www.blueletterbible.org/lexicon/g4245)

Definition: elder (64x), old man (1x), eldest (1x), elder woman (1x)

Part of speech: adjective

Occurs 67 times in 67 verses

Greek: [presbyterion](../g/g4244.md), [episkopos](../g/g1985.md)

Hebrew: [zāqēn](../h/h2205.md)

Derived words: presbyter, presbyterian

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4245)

[Study Light](https://www.studylight.org/lexicons/greek/4245.html)

[Bible Hub](https://biblehub.com/str/greek/4245.htm)

[LSJ](https://lsj.gr/wiki/πρεσβύτερος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4245)

[Bible Bento](https://biblebento.com/dictionary/G4245.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4245/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4245.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4245)

[Wiktionary](https://en.wiktionary.org/wiki/πρέσβυς)

[Wikipedia](https://en.wikipedia.org/wiki/Presbyter)