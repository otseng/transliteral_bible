# ἱερατεία

[hierateia](https://www.blueletterbible.org/lexicon/g2405)

Definition: priest's office (1x), office of the priesthood (1x), the office of a priest

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2405)

[Study Light](https://www.studylight.org/lexicons/greek/2405.html)

[Bible Hub](https://biblehub.com/str/greek/2405.htm)

[LSJ](https://lsj.gr/wiki/ἱερατεία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2405)

[Bible Bento](https://biblebento.com/dictionary/G2405.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2405/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2405.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2405)
