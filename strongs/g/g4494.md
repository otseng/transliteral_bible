# ῥιπίζω

[rhipizō](https://www.blueletterbible.org/lexicon/g4494)

Definition: toss (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4494)

[Study Light](https://www.studylight.org/lexicons/greek/4494.html)

[Bible Hub](https://biblehub.com/str/greek/4494.htm)

[LSJ](https://lsj.gr/wiki/ῥιπίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4494)

[Bible Bento](https://biblebento.com/dictionary/G4494.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4494/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4494.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4494)
