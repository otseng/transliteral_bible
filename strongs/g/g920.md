# Βαριωνᾶ

[Bariōna](https://www.blueletterbible.org/lexicon/g920)

Definition: Barjona (1x), "son of Jonah"

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0920)

[Study Light](https://www.studylight.org/lexicons/greek/920.html)

[Bible Hub](https://biblehub.com/str/greek/920.htm)

[LSJ](https://lsj.gr/wiki/Βαριωνᾶ)

[NET Bible](http://classic.net.bible.org/strong.php?id=920)

[Bible Bento](https://biblebento.com/dictionary/G0920.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0920/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0920.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g920)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/barjona.html)