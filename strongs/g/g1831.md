# ἐξέρχομαι

[exerchomai](https://www.blueletterbible.org/lexicon/g1831)

Definition: go out (60x), come (34x), depart (28x), go (25x), go forth (25x), come out (23x), come forth (9x), come forth, escape

Part of speech: verb

Occurs 230 times in 216 verses

Greek: [ek](../g/g1537.md), [erchomai](../g/g2064.md)

Hebrew: [halak](../h/h1980.md), [ʿālâ](../h/h5927.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1831)

[Study Light](https://www.studylight.org/lexicons/greek/1831.html)

[Bible Hub](https://biblehub.com/str/greek/1831.htm)

[LSJ](https://lsj.gr/wiki/ἐξέρχομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1831)

[Bible Bento](https://biblebento.com/dictionary/G1831.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1831/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1831.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1831)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%90%CE%BE%CE%AD%CF%81%CF%87%CE%BF%CE%BC%CE%B1%CE%B9)