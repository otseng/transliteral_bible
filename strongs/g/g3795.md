# ὀψάριον

[opsarion](https://www.blueletterbible.org/lexicon/g3795)

Definition: fish (4x), small fish (1x)

Part of speech: neuter noun

Occurs 5 times in 5 verses

Derived words: [opson](https://en.wikipedia.org/wiki/Opson)

Synonyms: [fish](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Fish)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3795)

[Study Light](https://www.studylight.org/lexicons/greek/3795.html)

[Bible Hub](https://biblehub.com/str/greek/3795.htm)

[LSJ](https://lsj.gr/wiki/ὀψάριον)

[NET Bible](http://classic.net.bible.org/strong.php?id=3795)

[Bible Bento](https://biblebento.com/dictionary/G3795.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3795/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3795.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3795)