# ῥῆμα

[rhēma](https://www.blueletterbible.org/lexicon/g4487)

Definition: word (56x), saying (9x), thing (3x), no thing (with G3756) (1x), uttered by the living voice, speech

Part of speech: neuter noun

Occurs 70 times in 67 verses

Hebrew: ['emer](../h/h561.md), [dabar](../h/h1697.md), [millâ](../h/h4405.md)

Synonyms: [word](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Word)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4487)

[Study Light](https://www.studylight.org/lexicons/greek/4487.html)

[Bible Hub](https://biblehub.com/str/greek/4487.htm)

[LSJ](https://lsj.gr/wiki/ῥῆμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=4487)

[Bible Bento](https://biblebento.com/dictionary/G4487.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4487/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4487.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4487)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BF%A5%E1%BF%86%CE%BC%CE%B1)

[Wikipedia](https://en.wikipedia.org/wiki/Rhema)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/4487-rhema-word.htm)
