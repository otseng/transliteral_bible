# σωματικός

[sōmatikos](https://www.blueletterbible.org/lexicon/g4984)

Definition: bodily (2x), corporeal

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4984)

[Study Light](https://www.studylight.org/lexicons/greek/4984.html)

[Bible Hub](https://biblehub.com/str/greek/4984.htm)

[LSJ](https://lsj.gr/wiki/σωματικός)

[NET Bible](http://classic.net.bible.org/strong.php?id=4984)

[Bible Bento](https://biblebento.com/dictionary/G4984.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4984/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4984.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4984)