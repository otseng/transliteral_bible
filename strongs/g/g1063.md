# γάρ

[gar](https://www.blueletterbible.org/lexicon/g1063)

Definition: for (1,027x), miscellaneous (28x), not translated (12x).

Part of speech: conjunction

Occurs 1,067 times in 1,016 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1063)

[Study Light](https://www.studylight.org/lexicons/greek/1063.html)

[Bible Hub](https://biblehub.com/str/greek/1063.htm)

[LSJ](https://lsj.gr/wiki/γάρ)

[NET Bible](http://classic.net.bible.org/strong.php?id=1063)

[Bible Bento](https://biblebento.com/dictionary/G1063.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1063/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1063.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1063)
