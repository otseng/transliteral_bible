# ὀξύς

[oxys](https://www.blueletterbible.org/lexicon/g3691)

Definition: sharp (7x), swift (1x), quick, rapid

Part of speech: adjective

Occurs 8 times in 7 verses

Derived words: [oxy-](https://www.yourdictionary.com/oxy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3691)

[Study Light](https://www.studylight.org/lexicons/greek/3691.html)

[Bible Hub](https://biblehub.com/str/greek/3691.htm)

[LSJ](https://lsj.gr/wiki/ὀξύς)

[NET Bible](http://classic.net.bible.org/strong.php?id=3691)

[Bible Bento](https://biblebento.com/dictionary/G3691.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3691/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3691.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3691)