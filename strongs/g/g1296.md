# διαταγή

[diatagē](https://www.blueletterbible.org/lexicon/g1296)

Definition: disposition (1x), ordinance (1x), arrangement, ordinance

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1296)

[Study Light](https://www.studylight.org/lexicons/greek/1296.html)

[Bible Hub](https://biblehub.com/str/greek/1296.htm)

[LSJ](https://lsj.gr/wiki/διαταγή)

[NET Bible](http://classic.net.bible.org/strong.php?id=1296)

[Bible Bento](https://biblebento.com/dictionary/G1296.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1296/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1296.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1296)
