# ἀποπνίγω

[apopnigō](https://www.blueletterbible.org/lexicon/g638)

Definition: choke (3x), suffocate, drown, stifle

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0638)

[Study Light](https://www.studylight.org/lexicons/greek/638.html)

[Bible Hub](https://biblehub.com/str/greek/638.htm)

[LSJ](https://lsj.gr/wiki/ἀποπνίγω)

[NET Bible](http://classic.net.bible.org/strong.php?id=638)

[Bible Bento](https://biblebento.com/dictionary/G0638.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0638/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0638.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g638)