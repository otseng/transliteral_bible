# σπουδάζω

[spoudazō](https://www.blueletterbible.org/lexicon/g4704)

Definition: endeavour (3x), do diligence (2x), be diligent (2x), give diligence (1x), be forward (1x), labour (1x), study (1x), to be conscientious, zealous and earnest in discharging a duty or obligation

Part of speech: verb

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4704)

[Study Light](https://www.studylight.org/lexicons/greek/4704.html)

[Bible Hub](https://biblehub.com/str/greek/4704.htm)

[LSJ](https://lsj.gr/wiki/σπουδάζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4704)

[Bible Bento](https://biblebento.com/dictionary/G4704.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4704/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4704.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4704)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CF%80%CE%BF%CF%85%CE%B4%CE%AC%CE%B6%CF%89)

[Sermon Index](https://www.sermonindex.net/modules/articles/index.php?view=article&aid=35895)

[Skip Moen](https://skipmoen.com/2014/07/do-your-best/)
