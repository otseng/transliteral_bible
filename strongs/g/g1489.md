# εἴγε

[eige](https://www.blueletterbible.org/lexicon/g1489)

Definition: if so be that (2x), if (2x), if yet (1x).

Part of speech: conjunction

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1489)

[Study Light](https://www.studylight.org/lexicons/greek/1489.html)

[Bible Hub](https://biblehub.com/str/greek/1489.htm)

[LSJ](https://lsj.gr/wiki/εἴγε)

[NET Bible](http://classic.net.bible.org/strong.php?id=1489)

[Bible Bento](https://biblebento.com/dictionary/G1489.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1489/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1489.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1489)

