# ῥαββί

[rhabbi](https://www.blueletterbible.org/lexicon/g4461)

Definition: Master (Christ) (9x), Rabbi (Christ) (5x), rabbi (3x), my great one, my honorable sir

Part of speech: masculine noun

Hebrew: [rab](../h/h7227.md)

Occurs 17 times in 15 verses (only found in gospels)

Derived words: [Rabbi](https://en.wikipedia.org/wiki/Rabbi)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4461)

[Study Light](https://www.studylight.org/lexicons/greek/4461.html)

[Bible Hub](https://biblehub.com/str/greek/4461.htm)

[LSJ](https://lsj.gr/wiki/ῥαββί)

[NET Bible](http://classic.net.bible.org/strong.php?id=4461)

[Bible Bento](https://biblebento.com/dictionary/G4461.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4461/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4461.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4461)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BF%A5%CE%B1%CE%B2%CE%B2%CE%AF)

[Wikipedia](https://en.wikipedia.org/wiki/Rabbi)
