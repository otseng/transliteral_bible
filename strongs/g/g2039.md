# ἐργασία

[ergasia](https://www.blueletterbible.org/lexicon/g2039)

Definition: gain (3x), craft (1x), diligence (1x), work (1x), performing, profit, endeavour, practice

Part of speech: feminine noun

Occurs 6 times in 6 verses

Hebrew: [ʿăḇāḏ](../h/h5652.md), [ma`aseh](../h/h4639.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2039)

[Study Light](https://www.studylight.org/lexicons/greek/2039.html)

[Bible Hub](https://biblehub.com/str/greek/2039.htm)

[LSJ](https://lsj.gr/wiki/ἐργασία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2039)

[Bible Bento](https://biblebento.com/dictionary/G2039.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2039/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2039.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2039)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%90%CF%81%CE%B3%CE%B1%CF%83%CE%AF%CE%B1)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34628)