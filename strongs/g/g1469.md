# ἐγκρίνω

[egkrinō](https://www.blueletterbible.org/lexicon/g1469)

Definition: make of the number (1x), to reckon among, judge among, to judge one worthy of being admitted to a certain class

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1469)

[Study Light](https://www.studylight.org/lexicons/greek/1469.html)

[Bible Hub](https://biblehub.com/str/greek/1469.htm)

[LSJ](https://lsj.gr/wiki/ἐγκρίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1469)

[Bible Bento](https://biblebento.com/dictionary/G1469.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1469/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1469.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1469)
