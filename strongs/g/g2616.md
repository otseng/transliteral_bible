# καταδυναστεύω

[katadynasteuō](https://www.blueletterbible.org/lexicon/g2616)

Definition: oppress (2x)

Part of speech: verb

Occurs 3 times in 2 verses

Hebrew: [yānâ](../h/h3238.md), [ʿāšaq](../h/h6231.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2616)

[Study Light](https://www.studylight.org/lexicons/greek/2616.html)

[Bible Hub](https://biblehub.com/str/greek/2616.htm)

[LSJ](https://lsj.gr/wiki/καταδυναστεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2616)

[Bible Bento](https://biblebento.com/dictionary/G2616.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2616/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2616.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2616)
