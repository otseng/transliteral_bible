# δούλη

[doulē](https://www.blueletterbible.org/lexicon/g1399)

Definition: handmaiden (2x), handmaid (1x), female slave

Part of speech: feminine noun

Occurs 3 times in 3 verses

Hebrew: [šip̄ḥâ](../h/h8198.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1399)

[Study Light](https://www.studylight.org/lexicons/greek/1399.html)

[Bible Hub](https://biblehub.com/str/greek/1399.htm)

[LSJ](https://lsj.gr/wiki/δούλη)

[NET Bible](http://classic.net.bible.org/strong.php?id=1399)

[Bible Bento](https://biblebento.com/dictionary/G1399.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1399/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1399.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1399)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B4%CE%BF%CF%8D%CE%BB%CE%B7)