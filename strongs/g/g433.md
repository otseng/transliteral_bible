# ἀνήκω

[anēkō](https://www.blueletterbible.org/lexicon/g433)

Definition: be convenient (2x), be fit (1x), be proper

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0433)

[Study Light](https://www.studylight.org/lexicons/greek/433.html)

[Bible Hub](https://biblehub.com/str/greek/433.htm)

[LSJ](https://lsj.gr/wiki/ἀνήκω)

[NET Bible](http://classic.net.bible.org/strong.php?id=433)

[Bible Bento](https://biblebento.com/dictionary/G0433.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/433/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/433.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g433)
