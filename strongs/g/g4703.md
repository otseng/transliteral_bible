# σπόρος

[sporos](https://www.blueletterbible.org/lexicon/g4703)

Definition: seed (4x), seed sown (1x), sowing

Part of speech: masculine noun

Occurs 5 times in 5 verses

Derived words: spore

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4703)

[Study Light](https://www.studylight.org/lexicons/greek/4703.html)

[Bible Hub](https://biblehub.com/str/greek/4703.htm)

[LSJ](https://lsj.gr/wiki/σπόρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4703)

[Bible Bento](https://biblebento.com/dictionary/G4703.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4703/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4703.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4703)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CF%80%CF%8C%CF%81%CE%BF%CF%82)