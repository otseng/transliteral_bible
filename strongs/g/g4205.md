# πόρνος

[pornos](https://www.blueletterbible.org/lexicon/g4205)

Definition: fornicator (5x), whoremonger (5x).

Part of speech: masculine noun

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4205)

[Study Light](https://www.studylight.org/lexicons/greek/4205.html)

[Bible Hub](https://biblehub.com/str/greek/4205.htm)

[LSJ](https://lsj.gr/wiki/πόρνος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4205)

[Bible Bento](https://biblebento.com/dictionary/G4205.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4205/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4205.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4205)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/4204-porne-prostitute.htm)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/4205-pornos-fornicator.htm)
