# οὐ μή

[ou mē](https://www.blueletterbible.org/lexicon/g3364)

Definition: not (56x), in no wise (6x), no (6x), never (with G1519) (with G165) (with G3588) (6x), no more at all (with G2089) (5x), not translated (1x), miscellaneous (14x).

Part of speech: particle

Occurs 85 times in 85 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3364)

[Study Light](https://www.studylight.org/lexicons/greek/3364.html)

[Bible Hub](https://biblehub.com/str/greek/3364.htm)

[LSJ](https://lsj.gr/wiki/οὐ μή)

[NET Bible](http://classic.net.bible.org/strong.php?id=3364)

[Bible Bento](https://biblebento.com/dictionary/G3364.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3364/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3364.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3364)

