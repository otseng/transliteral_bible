# πείθω

[peithō](https://www.blueletterbible.org/lexicon/g3982)

Definition: persuade (22x), trust (8x), obey (7x), have confidence (6x), believe (3x), be confident (2x), miscellaneous (7x), make friend, convince, yield to

Part of speech: verb

Occurs 63 times in 55 verses

Greek: [pistis](../g/g4102.md)

Hebrew: [batach](../h/h982.md), [sûṯ](../h/h5496.md)

Synonyms: [trust](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Trust)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3982)

[Study Light](https://www.studylight.org/lexicons/greek/3982.html)

[Bible Hub](https://biblehub.com/str/greek/3982.htm)

[LSJ](https://lsj.gr/wiki/πείθω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3982)

[Bible Bento](https://biblebento.com/dictionary/G3982.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3982/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3982.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3982)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%B5%CE%AF%CE%B8%CF%89)

[Wikipedia](https://en.wikipedia.org/wiki/Peitho)

[Precept Austin](https://www.preceptaustin.org/romans_215-29#c)
