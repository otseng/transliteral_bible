# θηλάζω

[thēlazō](https://www.blueletterbible.org/lexicon/g2337)

Definition: give suck (4x), suck (1x), suckling (1x), nurse, breastfeed

Part of speech: verb

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2337)

[Study Light](https://www.studylight.org/lexicons/greek/2337.html)

[Bible Hub](https://biblehub.com/str/greek/2337.htm)

[LSJ](https://lsj.gr/wiki/θηλάζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2337)

[Bible Bento](https://biblebento.com/dictionary/G2337.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2337/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2337.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2337)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B8%CE%B7%CE%BB%CE%AC%CE%B6%CF%89)