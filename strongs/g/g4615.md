# σίναπι

[sinapi](https://www.blueletterbible.org/lexicon/g4615)

Definition: mustard seed (5x), a small thing which grows to a remarkable size, the name of a plant which in oriental countries grows from a very small seed and attains to the height of a tree, 10 feet (3 m) and more

Part of speech: neuter noun

Occurs 5 times in 5 verses

Derived words: [Sinapis alba](https://en.wiktionary.org/wiki/Sinapis_alba)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4615)

[Study Light](https://www.studylight.org/lexicons/greek/4615.html)

[Bible Hub](https://biblehub.com/str/greek/4615.htm)

[LSJ](https://lsj.gr/wiki/σίναπι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4615)

[Bible Bento](https://biblebento.com/dictionary/G4615.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4615/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4615.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4615)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CE%AF%CE%BD%CE%B1%CF%80%CE%B9)