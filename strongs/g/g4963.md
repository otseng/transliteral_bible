# συστροφή

[systrophē](https://www.blueletterbible.org/lexicon/g4963)

Definition: concourse (1x), band together (1x), a secret combination, a coalition, conspiracy, a riot

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4963)

[Study Light](https://www.studylight.org/lexicons/greek/4963.html)

[Bible Hub](https://biblehub.com/str/greek/4963.htm)

[LSJ](https://lsj.gr/wiki/συστροφή)

[NET Bible](http://classic.net.bible.org/strong.php?id=4963)

[Bible Bento](https://biblebento.com/dictionary/G4963.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4963/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4963.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4963)
