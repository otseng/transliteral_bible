# θυμομαχέω

[thymomacheō](https://www.blueletterbible.org/lexicon/g2371)

Definition: highly displeased (1x)

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2371)

[Study Light](https://www.studylight.org/lexicons/greek/2371.html)

[Bible Hub](https://biblehub.com/str/greek/2371.htm)

[LSJ](https://lsj.gr/wiki/θυμομαχέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2371)

[Bible Bento](https://biblebento.com/dictionary/G2371.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2371/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2371.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2371)
