# ὑποτύπωσις

[hypotypōsis](https://www.blueletterbible.org/lexicon/g5296)

Definition: pattern (1x), form (1x), outline, sketch, brief and summary exposition, example

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5296)

[Study Light](https://www.studylight.org/lexicons/greek/5296.html)

[Bible Hub](https://biblehub.com/str/greek/5296.htm)

[LSJ](https://lsj.gr/wiki/ὑποτύπωσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=5296)

[Bible Bento](https://biblebento.com/dictionary/G5296.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5296/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5296.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5296)
