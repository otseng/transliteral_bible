# θνῄσκω

[thnēskō](https://www.blueletterbible.org/lexicon/g2348)

Definition: be dead (10x), die (1x), dead man (1x), dead (1x)

Part of speech: verb

Occurs 15 times in 13 verses

Hebrew: [muwth](../h/h4191.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2348)

[Study Light](https://www.studylight.org/lexicons/greek/2348.html)

[Bible Hub](https://biblehub.com/str/greek/2348.htm)

[LSJ](https://lsj.gr/wiki/θνῄσκω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2348)

[Bible Bento](https://biblebento.com/dictionary/G2348.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2348/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2348.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2348)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B8%CE%BD%E1%BF%84%CF%83%CE%BA%CF%89)