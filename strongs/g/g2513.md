# καθαρός

[katharos](https://www.blueletterbible.org/lexicon/g2513)

Definition: pure (17x), clean (10x), clear (1x), chaste, purified

Part of speech: adjective

Occurs 28 times in 24 verses

Derived words: catharsis

Hebrew: [zāḵ](../h/h2134.md)

Synonyms: [clean](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Clean)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2513)

[Study Light](https://www.studylight.org/lexicons/greek/2513.html)

[Bible Hub](https://biblehub.com/str/greek/2513.htm)

[LSJ](https://lsj.gr/wiki/καθαρός)

[NET Bible](http://classic.net.bible.org/strong.php?id=2513)

[Bible Bento](https://biblebento.com/dictionary/G2513.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2513/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2513.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2513)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%B1%CE%B8%CE%B1%CF%81%CF%8C%CF%82)
