# καταβιβάζω

[katabibazō](https://www.blueletterbible.org/lexicon/g2601)

Definition: bring down (1x), thrust down (1x), bring down, thrust down, cast down

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2601)

[Study Light](https://www.studylight.org/lexicons/greek/2601.html)

[Bible Hub](https://biblehub.com/str/greek/2601.htm)

[LSJ](https://lsj.gr/wiki/καταβιβάζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2601)

[Bible Bento](https://biblebento.com/dictionary/G2601.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2601/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2601.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2601)