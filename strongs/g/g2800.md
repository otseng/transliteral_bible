# κλάσις

[klasis](https://www.blueletterbible.org/lexicon/g2800)

Definition: breaking (2x)

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2800)

[Study Light](https://www.studylight.org/lexicons/greek/2800.html)

[Bible Hub](https://biblehub.com/str/greek/2800.htm)

[LSJ](https://lsj.gr/wiki/κλάσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=2800)

[Bible Bento](https://biblebento.com/dictionary/G2800.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2800/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2800.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2800)
