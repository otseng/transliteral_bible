# προσλαλέω

[proslaleō](https://www.blueletterbible.org/lexicon/g4354)

Definition: speak to (1x), speak with (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/4354.html)

[Bible Hub](https://biblehub.com/str/greek/4354.htm)

[LSJ](https://lsj.gr/wiki/προσλαλέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4354)

[Bible Bento](https://biblebento.com/dictionary/G4354.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4354/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4354.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4354)

