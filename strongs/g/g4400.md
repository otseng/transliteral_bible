# προχειρίζω

[procheirizō](https://www.blueletterbible.org/lexicon/g4400)

Definition: choose (1x), make (1x), to put into the hand, to deliver into the hands, to take into one's hands, appoint, to purpose

Part of speech: verb

Occurs 2 times in 2 verses

Synonyms: [choose](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Choose)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4400)

[Study Light](https://www.studylight.org/lexicons/greek/4400.html)

[Bible Hub](https://biblehub.com/str/greek/4400.htm)

[LSJ](https://lsj.gr/wiki/προχειρίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4400)

[Bible Bento](https://biblebento.com/dictionary/G4400.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4400/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4400.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4400)

[Precept Austin](https://www.preceptaustin.org/acts-21-commentary#appointed)
