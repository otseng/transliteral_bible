# ἄλφα

[alpha](https://www.blueletterbible.org/lexicon/g1)

Definition: Alpha (4x).

Part of speech: indeclinable noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0001)

[Study Light](https://www.studylight.org/lexicons/greek/1.html)

[Bible Hub](https://biblehub.com/str/greek/1.htm)

[LSJ](https://lsj.gr/wiki/ἄλφα)

[NET Bible](http://classic.net.bible.org/strong.php?id=1)

[Bible Bento](https://biblebento.com/dictionary/G1.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1)
