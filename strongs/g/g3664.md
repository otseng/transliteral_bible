# ὅμοιος

[homoios](https://www.blueletterbible.org/lexicon/g3664)

Definition: like (47x).

Part of speech: adjective

Occurs 47 times in 43 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3664)

[Study Light](https://www.studylight.org/lexicons/greek/3664.html)

[Bible Hub](https://biblehub.com/str/greek/3664.htm)

[LSJ](https://lsj.gr/wiki/ὅμοιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3664)

[Bible Bento](https://biblebento.com/dictionary/G3664.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3664/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3664.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3664)

