# ἔκφοβος

[ekphobos](https://www.blueletterbible.org/lexicon/g1630)

Definition: sore afraid (1x), exceedingly fear (with G1510) (1x), stricken with fear or terror, exceedingly, frightened, terrified

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1630)

[Study Light](https://www.studylight.org/lexicons/greek/1630.html)

[Bible Hub](https://biblehub.com/str/greek/1630.htm)

[LSJ](https://lsj.gr/wiki/ἔκφοβος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1630)

[Bible Bento](https://biblebento.com/dictionary/G1630.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1630/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1630.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1630)