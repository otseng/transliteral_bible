# πλησμονή

[plēsmonē](https://www.blueletterbible.org/lexicon/g4140)

Definition: satisfying (1x), indulgence of the flesh, gratification

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4140)

[Study Light](https://www.studylight.org/lexicons/greek/4140.html)

[Bible Hub](https://biblehub.com/str/greek/4140.htm)

[LSJ](https://lsj.gr/wiki/πλησμονή)

[NET Bible](http://classic.net.bible.org/strong.php?id=4140)

[Bible Bento](https://biblebento.com/dictionary/G4140.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4140/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4140.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4140)
