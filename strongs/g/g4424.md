# Πτολεμαΐς

[Ptolemaïs](https://www.blueletterbible.org/lexicon/g4424)

Definition: Ptolemais (1x), "warlike", a maritime city of Phoenicia, which got its name, apparently, from Ptolemy Lathyrus, who captured it 103 BC, and rebuilt it more beautifully

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4424)

[Study Light](https://www.studylight.org/lexicons/greek/4424.html)

[Bible Hub](https://biblehub.com/str/greek/4424.htm)

[LSJ](https://lsj.gr/wiki/Πτολεμαΐς)

[NET Bible](http://classic.net.bible.org/strong.php?id=4424)

[Bible Bento](https://biblebento.com/dictionary/G4424.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4424/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4424.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4424)

[Wikipedia](https://en.wikipedia.org/wiki/Ptolemais,_Cyrenaica)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/ptolemais.html)