# ἀπερισπάστως

[aperispastōs](https://www.blueletterbible.org/lexicon/g563)

Definition: without distraction (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0563)

[Study Light](https://www.studylight.org/lexicons/greek/563.html)

[Bible Hub](https://biblehub.com/str/greek/563.htm)

[LSJ](https://lsj.gr/wiki/ἀπερισπάστως)

[NET Bible](http://classic.net.bible.org/strong.php?id=563)

[Bible Bento](https://biblebento.com/dictionary/G0563.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0563/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0563.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g563)
