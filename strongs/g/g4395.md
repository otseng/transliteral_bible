# προφητεύω

[prophēteuō](https://www.blueletterbible.org/lexicon/g4395)

Definition: prophesy (28x), speak forth by divine inspirations, to predict, o foretell events

Part of speech: verb

Occurs 30 times in 27 verses

Greek: [prophētēs](../g/g4396.md)

Hebrew: [nāḇā'](../h/h5012.md), [nāṭap̄](../h/h5197.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4395)

[Study Light](https://www.studylight.org/lexicons/greek/4395.html)

[Bible Hub](https://biblehub.com/str/greek/4395.htm)

[LSJ](https://lsj.gr/wiki/προφητεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4395)

[Bible Bento](https://biblebento.com/dictionary/G4395.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4395/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4395.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4395)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CF%81%CE%BF%CF%86%CE%B7%CF%84%CE%B5%CF%8D%CF%89)