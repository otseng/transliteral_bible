# ἀρχιερατικός

[archieratikos](https://www.blueletterbible.org/lexicon/g748)

Definition: of the high priest (1x), high priestly, pontifical

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0748)

[Study Light](https://www.studylight.org/lexicons/greek/748.html)

[Bible Hub](https://biblehub.com/str/greek/748.htm)

[LSJ](https://lsj.gr/wiki/ἀρχιερατικός)

[NET Bible](http://classic.net.bible.org/strong.php?id=748)

[Bible Bento](https://biblebento.com/dictionary/G0748.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0748/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0748.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g748)
