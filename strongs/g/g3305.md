# μέντοι

[mentoi](https://www.blueletterbible.org/lexicon/g3305)

Definition: yet (2x), nevertheless (2x), howbeit (1x), but (1x), not translated (2x).

Part of speech: particle

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3305)

[Study Light](https://www.studylight.org/lexicons/greek/3305.html)

[Bible Hub](https://biblehub.com/str/greek/3305.htm)

[LSJ](https://lsj.gr/wiki/μέντοι)

[NET Bible](http://classic.net.bible.org/strong.php?id=3305)

[Bible Bento](https://biblebento.com/dictionary/G3305.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3305/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3305.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3305)

