# ἱκετηρία

[hiketēria](https://www.blueletterbible.org/lexicon/g2428)

Definition: supplication (1x), an olive branch

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2428)

[Study Light](https://www.studylight.org/lexicons/greek/2428.html)

[Bible Hub](https://biblehub.com/str/greek/2428.htm)

[LSJ](https://lsj.gr/wiki/ἱκετηρία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2428)

[Bible Bento](https://biblebento.com/dictionary/G2428.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2428/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2428.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2428)
