# τοπάζιον

[topazion](https://www.blueletterbible.org/lexicon/g5116)

Definition: topaz (1x).

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5116)

[Study Light](https://www.studylight.org/lexicons/greek/5116.html)

[Bible Hub](https://biblehub.com/str/greek/5116.htm)

[LSJ](https://lsj.gr/wiki/τοπάζιον)

[NET Bible](http://classic.net.bible.org/strong.php?id=5116)

[Bible Bento](https://biblebento.com/dictionary/G5116.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5116/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5116.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5116)

