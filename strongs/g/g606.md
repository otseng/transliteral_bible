# ἀπόκειμαι

[apokeimai](https://www.blueletterbible.org/lexicon/g606)

Definition: lay up (3x), appoint (1x), reserved, reserved for one awaiting him

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0606)

[Study Light](https://www.studylight.org/lexicons/greek/606.html)

[Bible Hub](https://biblehub.com/str/greek/606.htm)

[LSJ](https://lsj.gr/wiki/ἀπόκειμαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=606)

[Bible Bento](https://biblebento.com/dictionary/G0606.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0606/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0606.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g606)
