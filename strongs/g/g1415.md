# δυνατός

[dynatos](https://www.blueletterbible.org/lexicon/g1415)

Definition: possible (13x), able (10x), mighty (6x), strong (3x), could (1x), power (1x), mighty man (1x)

Part of speech: adjective

Occurs 35 times in 35 verses

Hebrew: ['atsuwm](../h/h6099.md)

Synonyms: [mighty](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Mighty)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1415)

[Study Light](https://www.studylight.org/lexicons/greek/1415.html)

[Bible Hub](https://biblehub.com/str/greek/1415.htm)

[LSJ](https://lsj.gr/wiki/δυνατός)

[NET Bible](http://classic.net.bible.org/strong.php?id=1415)

[Bible Bento](https://biblebento.com/dictionary/G1415.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1415/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1415.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1415)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B4%CF%85%CE%BD%CE%B1%CF%84%CF%8C%CF%82)