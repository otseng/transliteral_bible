# κατακλίνω

[kataklinō](https://www.blueletterbible.org/lexicon/g2625)

Definition: sit down (1x), sit at meat (1x), make sit down (1x).

Part of speech: verb

Occurs 5 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2625)

[Study Light](https://www.studylight.org/lexicons/greek/2625.html)

[Bible Hub](https://biblehub.com/str/greek/2625.htm)

[LSJ](https://lsj.gr/wiki/κατακλίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2625)

[Bible Bento](https://biblebento.com/dictionary/G2625.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2625/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2625.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2625)