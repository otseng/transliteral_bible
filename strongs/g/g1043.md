# Γαβριήλ

[Gabriēl](https://www.blueletterbible.org/lexicon/g1043)

Definition: Gabriel (2x), "man of God", one of the angel princes or chiefs of the angels

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1043)

[Study Light](https://www.studylight.org/lexicons/greek/1043.html)

[Bible Hub](https://biblehub.com/str/greek/1043.htm)

[LSJ](https://lsj.gr/wiki/Γαβριήλ)

[NET Bible](http://classic.net.bible.org/strong.php?id=1043)

[Bible Bento](https://biblebento.com/dictionary/G1043.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1043/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1043.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1043)

[Wikipedia](https://en.wikipedia.org/wiki/Gabriel)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/gabriel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/gabriel)