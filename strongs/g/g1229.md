# διαγγέλλω

[diaggellō](https://www.blueletterbible.org/lexicon/g1229)

Definition: preach (1x), signify (1x), declare (1x), publish abroad

Part of speech: verb

Occurs 3 times in 3 verses

Hebrew: [sāp̄ar](../h/h5608.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1229)

[Study Light](https://www.studylight.org/lexicons/greek/1229.html)

[Bible Hub](https://biblehub.com/str/greek/1229.htm)

[LSJ](https://lsj.gr/wiki/διαγγέλλω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1229)

[Bible Bento](https://biblebento.com/dictionary/G1229.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1229/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1229.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1229)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34663)