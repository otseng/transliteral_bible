# νῆστις

[nēstis](https://www.blueletterbible.org/lexicon/g3523)

Definition: fasting (2x), not having eaten

Part of speech: feminine noun

Occurs 2 times in 2 verses

Greek: [nēsteuō](../g/g3522.md), [esthiō](../g/g2068.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3523)

[Study Light](https://www.studylight.org/lexicons/greek/3523.html)

[Bible Hub](https://biblehub.com/str/greek/3523.htm)

[LSJ](https://lsj.gr/wiki/νῆστις)

[NET Bible](http://classic.net.bible.org/strong.php?id=3523)

[Bible Bento](https://biblebento.com/dictionary/G3523.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3523/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3523.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3523)