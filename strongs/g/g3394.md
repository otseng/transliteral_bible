# μιασμός

[miasmos](https://www.blueletterbible.org/lexicon/g3394)

Definition: uncleanness (1x), the act of defiling, defilement, pollution

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3394)

[Study Light](https://www.studylight.org/lexicons/greek/3394.html)

[Bible Hub](https://biblehub.com/str/greek/3394.htm)

[LSJ](https://lsj.gr/wiki/μιασμός)

[NET Bible](http://classic.net.bible.org/strong.php?id=3394)

[Bible Bento](https://biblebento.com/dictionary/G3394.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3394/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3394.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3394)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BC%CE%AF%CE%B1%CF%83%CE%BC%CE%B1)
