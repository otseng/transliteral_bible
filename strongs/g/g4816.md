# συλλέγω

[syllegō](https://www.blueletterbible.org/lexicon/g4816)

Definition: gather (5x), gather up (2x), gather together (1x).

Part of speech: verb

Occurs 9 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4816)

[Study Light](https://www.studylight.org/lexicons/greek/4816.html)

[Bible Hub](https://biblehub.com/str/greek/4816.htm)

[LSJ](https://lsj.gr/wiki/συλλέγω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4816)

[Bible Bento](https://biblebento.com/dictionary/G4816.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4816/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4816.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4816)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CF%85%CE%BB%CE%BB%CE%AD%CE%B3%CF%89)
