# πληθύνω

[plēthynō](https://www.blueletterbible.org/lexicon/g4129)

Definition: multiply (11x), abound (1x)

Part of speech: verb

Occurs 13 times in 11 verses

Hebrew: [rabah](../h/h7235.md)

Derived words: plethora

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4129)

[Study Light](https://www.studylight.org/lexicons/greek/4129.html)

[Bible Hub](https://biblehub.com/str/greek/4129.htm)

[LSJ](https://lsj.gr/wiki/πληθύνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4129)

[Bible Bento](https://biblebento.com/dictionary/G4129.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4129/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4129.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4129)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%BB%CE%B7%CE%B8%CF%8D%CE%BD%CF%89)