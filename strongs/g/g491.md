# Ἀντιοχεύς

[Antiocheus](https://www.blueletterbible.org/lexicon/g491)

Definition: of Antioch (1x)

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0491)

[Study Light](https://www.studylight.org/lexicons/greek/491.html)

[Bible Hub](https://biblehub.com/str/greek/491.htm)

[LSJ](https://lsj.gr/wiki/Ἀντιοχεύς)

[NET Bible](http://classic.net.bible.org/strong.php?id=491)

[Bible Bento](https://biblebento.com/dictionary/G0491.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0491/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0491.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g491)
