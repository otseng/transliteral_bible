# λύτρον

[lytron](https://www.blueletterbible.org/lexicon/g3083)

Definition: ransom (2x), the price for redeeming, redemption price

Part of speech: neuter noun

Occurs 2 times in 2 verses

Hebrew: [kōp̄er](../h/h3724.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3083)

[Study Light](https://www.studylight.org/lexicons/greek/3083.html)

[Bible Hub](https://biblehub.com/str/greek/3083.htm)

[LSJ](https://lsj.gr/wiki/λύτρον)

[NET Bible](http://classic.net.bible.org/strong.php?id=3083)

[Bible Bento](https://biblebento.com/dictionary/G3083.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3083/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3083.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3083)