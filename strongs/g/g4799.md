# σύγχυσις

[sygchysis](https://www.blueletterbible.org/lexicon/g4799)

Definition: confusion (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Derived words: [Synchysis](https://en.wikipedia.org/wiki/Synchysis)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4799)

[Study Light](https://www.studylight.org/lexicons/greek/4799.html)

[Bible Hub](https://biblehub.com/str/greek/4799.htm)

[LSJ](https://lsj.gr/wiki/σύγχυσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=4799)

[Bible Bento](https://biblebento.com/dictionary/G4799.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4799/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4799.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4799)
