# περικάθαρμα

[perikatharma](https://www.blueletterbible.org/lexicon/g4027)

Definition: filth (1x), refuse

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4027)

[Study Light](https://www.studylight.org/lexicons/greek/4027.html)

[Bible Hub](https://biblehub.com/str/greek/4027.htm)

[LSJ](https://lsj.gr/wiki/περικάθαρμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=4027)

[Bible Bento](https://biblebento.com/dictionary/G4027.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4027/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4027.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4027)
