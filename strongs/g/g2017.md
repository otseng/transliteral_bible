# ἐπιφαύσκω

[epiphauskō](https://www.blueletterbible.org/lexicon/g2017)

Definition: give light (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2017)

[Study Light](https://www.studylight.org/lexicons/greek/2017.html)

[Bible Hub](https://biblehub.com/str/greek/2017.htm)

[LSJ](https://lsj.gr/wiki/ἐπιφαύσκω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2017)

[Bible Bento](https://biblebento.com/dictionary/G2017.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2017/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2017.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2017)

