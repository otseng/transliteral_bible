# τοῖχος

[toichos](https://www.blueletterbible.org/lexicon/g5109)

Definition: wall (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

Hebrew: [gāḏēr](../h/h1447.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5109)

[Study Light](https://www.studylight.org/lexicons/greek/5109.html)

[Bible Hub](https://biblehub.com/str/greek/5109.htm)

[LSJ](https://lsj.gr/wiki/τοῖχος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5109)

[Bible Bento](https://biblebento.com/dictionary/G5109.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5109/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5109.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5109)
