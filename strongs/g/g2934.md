# κτῆνος

[ktēnos](https://www.blueletterbible.org/lexicon/g2934)

Definition: beast (4x), livestock

Part of speech: neuter noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2934)

[Study Light](https://www.studylight.org/lexicons/greek/2934.html)

[Bible Hub](https://biblehub.com/str/greek/2934.htm)

[LSJ](https://lsj.gr/wiki/κτῆνος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2934)

[Bible Bento](https://biblebento.com/dictionary/G2934.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2934/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2934.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2934)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CF%84%E1%BF%86%CE%BD%CE%BF%CF%82)