# ἐξεραυνάω

[exeraunaō](https://www.blueletterbible.org/lexicon/g1830)

Definition: search diligently (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1830)

[Study Light](https://www.studylight.org/lexicons/greek/1830.html)

[Bible Hub](https://biblehub.com/str/greek/1830.htm)

[LSJ](https://lsj.gr/wiki/ἐξεραυνάω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1830)

[Bible Bento](https://biblebento.com/dictionary/G1830.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1830/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1830.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1830)

