# ἀντιτάσσω

[antitassō](https://www.blueletterbible.org/lexicon/g498)

Definition: resist (4x), oppose (one's) self (1x).

Part of speech: verb

Occurs 6 times in 5 verses

Greek: [anti](../g/g473.md), [tassō](../g/g5021.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0498)

[Study Light](https://www.studylight.org/lexicons/greek/498.html)

[Bible Hub](https://biblehub.com/str/greek/498.htm)

[LSJ](https://lsj.gr/wiki/ἀντιτάσσω)

[NET Bible](http://classic.net.bible.org/strong.php?id=498)

[Bible Bento](https://biblebento.com/dictionary/G0498.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0498/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0498.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g498)
