# ἀπωθέω

[apōtheō](https://www.blueletterbible.org/lexicon/g683)

Definition: cast away (2x), thrust away (1x), put from (1x), thrust from (1x), put away (1x)

Part of speech: verb

Occurs 10 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0683)

[Study Light](https://www.studylight.org/lexicons/greek/683.html)

[Bible Hub](https://biblehub.com/str/greek/683.htm)

[LSJ](https://lsj.gr/wiki/ἀπωθέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=683)

[Bible Bento](https://biblebento.com/dictionary/G0683.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0683/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0683.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g683)
