# Χαρράν

[Charran](https://www.blueletterbible.org/lexicon/g5488)

Definition: Charran (2x), "a mountaineer", a city in Mesopotamia, of great antiquity and made famous by the defeat of Crassus

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5488)

[Study Light](https://www.studylight.org/lexicons/greek/5488.html)

[Bible Hub](https://biblehub.com/str/greek/5488.htm)

[LSJ](https://lsj.gr/wiki/Χαρράν)

[NET Bible](http://classic.net.bible.org/strong.php?id=5488)

[Bible Bento](https://biblebento.com/dictionary/G5488.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5488/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5488.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5488)

[Wikipedia](https://en.wikipedia.org/wiki/Haran_%28biblical_place%29)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/charran.html)