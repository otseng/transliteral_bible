# ὑδρωπικός

[hydrōpikos](https://www.blueletterbible.org/lexicon/g5203)

Definition: have the dropsy (1x), suffering from dropsy

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5203)

[Study Light](https://www.studylight.org/lexicons/greek/5203.html)

[Bible Hub](https://biblehub.com/str/greek/5203.htm)

[LSJ](https://lsj.gr/wiki/ὑδρωπικός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5203)

[Bible Bento](https://biblebento.com/dictionary/G5203.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5203/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5203.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5203)