# γένεσις

[genesis](https://www.blueletterbible.org/lexicon/g1078)

Definition: generation (1x), natural (1x), nature (1x), source, origin

Part of speech: feminine noun

Occurs 3 times in 3 verses

Hebrew: [towlĕdah](../h/h8435.md), [yalad](../h/h3205.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1078)

[Study Light](https://www.studylight.org/lexicons/greek/1078.html)

[Bible Hub](https://biblehub.com/str/greek/1078.htm)

[LSJ](https://lsj.gr/wiki/γένεσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=1078)

[Bible Bento](https://biblebento.com/dictionary/G1078.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1078/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1078.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1078)

