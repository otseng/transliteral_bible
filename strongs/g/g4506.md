# ῥύομαι

[rhyomai](https://www.blueletterbible.org/lexicon/g4506)

Definition: deliver (17x), Deliverer (1x), rescue, protect, ransom

Part of speech: verb

Occurs 19 times in 16 verses

Greek: [reō](../g/g4482.md), [rhysis](../g/g4511.md)

Hebrew: [mālaṭ](../h/h4422.md), [chalats](../h/h2502.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4506)

[Study Light](https://www.studylight.org/lexicons/greek/4506.html)

[Bible Hub](https://biblehub.com/str/greek/4506.htm)

[LSJ](https://lsj.gr/wiki/ῥύομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4506)

[Bible Bento](https://biblebento.com/dictionary/G4506.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4506/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4506.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4506)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BF%A5%CF%8D%CE%BF%CE%BC%CE%B1%CE%B9)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33826)