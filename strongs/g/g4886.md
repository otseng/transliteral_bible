# σύνδεσμος

[syndesmos](https://www.blueletterbible.org/lexicon/g4886)

Definition: bond (3x), band (1x), ligaments by which the members of the human body are united together

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4886)

[Study Light](https://www.studylight.org/lexicons/greek/4886.html)

[Bible Hub](https://biblehub.com/str/greek/4886.htm)

[LSJ](https://lsj.gr/wiki/σύνδεσμος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4886)

[Bible Bento](https://biblebento.com/dictionary/G4886.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4886/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4886.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4886)
