# ὄγκος

[ogkos](https://www.blueletterbible.org/lexicon/g3591)

Definition: weight (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3591)

[Study Light](https://www.studylight.org/lexicons/greek/3591.html)

[Bible Hub](https://biblehub.com/str/greek/3591.htm)

[LSJ](https://lsj.gr/wiki/ὄγκος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3591)

[Bible Bento](https://biblebento.com/dictionary/G3591.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3591/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3591.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3591)
