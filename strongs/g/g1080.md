# γεννάω

[gennaō](https://www.blueletterbible.org/lexicon/g1080)

Definition: begat (49x), be born (39x), bear (2x), gender (2x), bring forth (1x), be delivered (1x), procreate, produce

Part of speech: verb

Occurs 105 times in 65 verses

Greek: [gennēma](../g/g1081.md)

Hebrew: [tᵊḇû'â](../h/h8393.md), [yalad](../h/h3205.md), [harah](../h/h2029.md)

Derived words: generate

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1080)

[Study Light](https://www.studylight.org/lexicons/greek/1080.html)

[Bible Hub](https://biblehub.com/str/greek/1080.htm)

[LSJ](https://lsj.gr/wiki/γεννάω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1080)

[Bible Bento](https://biblebento.com/dictionary/G1080.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1080/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1080.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1080)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B3%CE%B5%CE%BD%CE%BD%CE%AC%CF%89)

[Resounding the faith](http://resoundingthefaith.com/2018/12/greek-%CE%B3%CE%B5%CE%BD%CE%BD%CE%AC%CF%89-gennao/)