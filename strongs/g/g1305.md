# διατροφή

[diatrophē](https://www.blueletterbible.org/lexicon/g1305)

Definition: food (1x), sustenance, nourishment

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1305)

[Study Light](https://www.studylight.org/lexicons/greek/1305.html)

[Bible Hub](https://biblehub.com/str/greek/1305.htm)

[LSJ](https://lsj.gr/wiki/διατροφή)

[NET Bible](http://classic.net.bible.org/strong.php?id=1305)

[Bible Bento](https://biblebento.com/dictionary/G1305.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1305/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1305.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1305)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B4%CE%B9%CE%B1%CF%84%CF%81%CE%BF%CF%86%CE%AE)
