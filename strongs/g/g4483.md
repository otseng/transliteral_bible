# ῥέω

[rheō](https://www.blueletterbible.org/lexicon/g4483)

Definition: speak (12x), say (9x), speak of (3x), command (1x), make (1x), flow, stream, gush

Part of speech: verb

Usage: [rheo-](https://www.thefreedictionary.com/words-that-start-with-rheo)

Occurs 26 times in 26 verses

Notes: Used 20 times in Matthew, 6 times in all other books.  Mostly used in reference to things spoken by prophets.

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4483)

[Study Light](https://www.studylight.org/lexicons/greek/4483.html)

[Bible Hub](https://biblehub.com/str/greek/4483.htm)

[LSJ](https://lsj.gr/wiki/ῥέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4483)

[Bible Bento](https://biblebento.com/dictionary/G4483.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4483/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4483.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4483)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BF%A5%CE%AD%CF%89)
