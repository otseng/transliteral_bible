# θρέμμα

[thremma](https://www.blueletterbible.org/lexicon/g2353)

Definition: cattle (1x).

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2353)

[Study Light](https://www.studylight.org/lexicons/greek/2353.html)

[Bible Hub](https://biblehub.com/str/greek/2353.htm)

[LSJ](https://lsj.gr/wiki/θρέμμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=2353)

[Bible Bento](https://biblebento.com/dictionary/G2353.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2353/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2353.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2353)

