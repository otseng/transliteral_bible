# Ἰούδα

[Iouda](https://www.blueletterbible.org/lexicon/g2448)

Definition: Juda (3x), Judah, "he shall be praised"

Part of speech: proper locative noun

Occurs 3 times in 2 verses

Greek: [Ioudaios](../g/g2453.md)

Hebrew: [Yehuwdah](../h/h3063.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2448)

[Study Light](https://www.studylight.org/lexicons/greek/2448.html)

[Bible Hub](https://biblehub.com/str/greek/2448.htm)

[LSJ](https://lsj.gr/wiki/Ἰούδα)

[NET Bible](http://classic.net.bible.org/strong.php?id=2448)

[Bible Bento](https://biblebento.com/dictionary/G2448.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2448/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2448.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2448)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%B8%CE%BF%CF%85%CE%B4%CE%AC)

[Wikipedia](https://en.wikipedia.org/wiki/Judah_%28son_of_Jacob%29)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/juda.html)