# βέλος

[belos](https://www.blueletterbible.org/lexicon/g956)

Definition: dart (1x), a missile, javelin, arrow

Part of speech: neuter noun

Occurs 1 times in 1 verses

Hebrew: [chets](../h/h2671.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0956)

[Study Light](https://www.studylight.org/lexicons/greek/956.html)

[Bible Hub](https://biblehub.com/str/greek/956.htm)

[LSJ](https://lsj.gr/wiki/βέλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=956)

[Bible Bento](https://biblebento.com/dictionary/G0956.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0956/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0956.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g956)
