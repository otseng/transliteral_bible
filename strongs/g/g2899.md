# κράσπεδον

[kraspedon](https://www.blueletterbible.org/lexicon/g2899)

Definition: border (3x), hem (2x), the fringe of a garment, tassel, tuft

Part of speech: neuter noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2899)

[Study Light](https://www.studylight.org/lexicons/greek/2899.html)

[Bible Hub](https://biblehub.com/str/greek/2899.htm)

[LSJ](https://lsj.gr/wiki/κράσπεδον)

[NET Bible](http://classic.net.bible.org/strong.php?id=2899)

[Bible Bento](https://biblebento.com/dictionary/G2899.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2899/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2899.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2899)

[Precept Austin](https://www.preceptaustin.org/luke-8-commentary#fringe)