# ἀμώμητος

[amōmētos](https://www.blueletterbible.org/lexicon/g298)

Definition: without rebuke (1x), blameless (1x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0298)

[Study Light](https://www.studylight.org/lexicons/greek/298.html)

[Bible Hub](https://biblehub.com/str/greek/298.htm)

[LSJ](https://lsj.gr/wiki/ἀμώμητος)

[NET Bible](http://classic.net.bible.org/strong.php?id=298)

[Bible Bento](https://biblebento.com/dictionary/G0298.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/298/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/298.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g298)
