# πλήν

[plēn](https://www.blueletterbible.org/lexicon/g4133)

Definition: but (14x), nevertheless (8x), notwithstanding (4x), but rather (2x), except (1x), than (1x), save (1x), moreover, besides, but, nevertheless, besides, except

Part of speech: adverb

Occurs 31 times in 31 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4133)

[Study Light](https://www.studylight.org/lexicons/greek/4133.html)

[Bible Hub](https://biblehub.com/str/greek/4133.htm)

[LSJ](https://lsj.gr/wiki/πλήν)

[NET Bible](http://classic.net.bible.org/strong.php?id=4133)

[Bible Bento](https://biblebento.com/dictionary/G4133.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4133/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4133.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4133)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%BB%CE%AE%CE%BD)