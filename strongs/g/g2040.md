# ἐργάτης

[ergatēs](https://www.blueletterbible.org/lexicon/g2040)

Definition: labourer (10x), workman (3x), worker (3x).

Part of speech: masculine noun

Occurs 16 times in 15 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2040)

[Study Light](https://www.studylight.org/lexicons/greek/2040.html)

[Bible Hub](https://biblehub.com/str/greek/2040.htm)

[LSJ](https://lsj.gr/wiki/ἐργάτης)

[NET Bible](http://classic.net.bible.org/strong.php?id=2040)

[Bible Bento](https://biblebento.com/dictionary/G2040.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2040/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2040.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2040)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%90%CF%81%CE%B3%CE%AC%CF%84%CE%B7%CF%82)

[Precept Austin](https://www.preceptaustin.org/2_timothy_215-19#w)