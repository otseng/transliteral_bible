# ἄπειρος

[apeiros](https://www.blueletterbible.org/lexicon/g552)

Definition: unskilful (1x), inexperienced in, without experience of

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0552)

[Study Light](https://www.studylight.org/lexicons/greek/552.html)

[Bible Hub](https://biblehub.com/str/greek/552.htm)

[LSJ](https://lsj.gr/wiki/ἄπειρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=552)

[Bible Bento](https://biblebento.com/dictionary/G0552.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/552/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/552.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g552)
