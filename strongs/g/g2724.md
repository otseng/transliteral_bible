# κατηγορία

[katēgoria](https://www.blueletterbible.org/lexicon/g2724)

Definition: accusation (3x), accused (1x), charge, "against agora"

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2724)

[Study Light](https://www.studylight.org/lexicons/greek/2724.html)

[Bible Hub](https://biblehub.com/str/greek/2724.htm)

[LSJ](https://lsj.gr/wiki/κατηγορία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2724)

[Bible Bento](https://biblebento.com/dictionary/G2724.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2724/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2724.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2724)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33477)

[Precept Austin](https://www.preceptaustin.org/titus_15-9#accused)