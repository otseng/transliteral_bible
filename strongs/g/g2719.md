# κατεσθίω

[katesthiō](https://www.blueletterbible.org/lexicon/g2719)

Definition: devour (10x), eat up (3x), devour up (2x), eaten me up, consumed me

Part of speech: verb

Occurs 20 times in 15 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2719)

[Study Light](https://www.studylight.org/lexicons/greek/2719.html)

[Bible Hub](https://biblehub.com/str/greek/2719.htm)

[LSJ](https://lsj.gr/wiki/κατεσθίω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2719)

[Bible Bento](https://biblebento.com/dictionary/G2719.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2719/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2719.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2719)