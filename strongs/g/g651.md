# ἀποστολή

[apostolē](https://www.blueletterbible.org/lexicon/g651)

Definition: apostleship (4x), commission, mission

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0651)

[Study Light](https://www.studylight.org/lexicons/greek/651.html)

[Bible Hub](https://biblehub.com/str/greek/651.htm)

[LSJ](https://lsj.gr/wiki/ἀποστολή)

[NET Bible](http://classic.net.bible.org/strong.php?id=651)

[Bible Bento](https://biblebento.com/dictionary/G0651.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0651/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0651.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g651)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CF%80%CE%BF%CF%83%CF%84%CE%BF%CE%BB%CE%AE)
