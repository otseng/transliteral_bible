# σκάπτω

[skaptō](https://www.blueletterbible.org/lexicon/g4626)

Definition: dig (3x)

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4626)

[Study Light](https://www.studylight.org/lexicons/greek/4626.html)

[Bible Hub](https://biblehub.com/str/greek/4626.htm)

[LSJ](https://lsj.gr/wiki/σκάπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4626)

[Bible Bento](https://biblebento.com/dictionary/G4626.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4626/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4626.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4626)