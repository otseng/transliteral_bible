# ἄφαντος

[aphantos](https://www.blueletterbible.org/lexicon/g855)

Definition: vanish out of sight (with G575) (1x), taken out of sight, made invisible

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0855)

[Study Light](https://www.studylight.org/lexicons/greek/855.html)

[Bible Hub](https://biblehub.com/str/greek/855.htm)

[LSJ](https://lsj.gr/wiki/ἄφαντος)

[NET Bible](http://classic.net.bible.org/strong.php?id=855)

[Bible Bento](https://biblebento.com/dictionary/G0855.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0855/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0855.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g855)
