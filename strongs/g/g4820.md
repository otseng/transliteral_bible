# συμβάλλω

[symballō](https://www.blueletterbible.org/lexicon/g4820)

Definition: ponder (1x), make (1x), confer (1x), encounter (1x), help (1x), meet with (1x), to bring together in one's mind, consider, converse

Part of speech: verb

Occurs 6 times in 6 verses

Usage: [Symbaloo](https://www.symbaloo.com) 

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4820)

[Study Light](https://www.studylight.org/lexicons/greek/4820.html)

[Bible Hub](https://biblehub.com/str/greek/4820.htm)

[LSJ](https://lsj.gr/wiki/συμβάλλω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4820)

[Bible Bento](https://biblebento.com/dictionary/G4820.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4820/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4820.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4820)