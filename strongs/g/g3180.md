# μεθοδεία

[methodeia](https://www.blueletterbible.org/lexicon/g3180)

Definition: lie in wait (1x), wile (1x), cunning arts, deceit, craft, trickery, deliberate planning or a systematic approach

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3180)

[Study Light](https://www.studylight.org/lexicons/greek/3180.html)

[Bible Hub](https://biblehub.com/str/greek/3180.htm)

[LSJ](https://lsj.gr/wiki/μεθοδεία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3180)

[Bible Bento](https://biblebento.com/dictionary/G3180.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3180/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3180.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3180)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34452)

[Precept Austin](https://www.preceptaustin.org/ephesians_611#schemes)