# ἀδόκιμος

[adokimos](https://www.blueletterbible.org/lexicon/g96)

Definition: reprobate (6x), castaway (1x), rejected (1x), not standing the test, not approved, properly used of metals and coins, that which does not prove itself such as it ought, worthless, disqualified

Part of speech: adjective

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0096)

[Study Light](https://www.studylight.org/lexicons/greek/96.html)

[Bible Hub](https://biblehub.com/str/greek/96.htm)

[LSJ](https://lsj.gr/wiki/ἀδόκιμος)

[NET Bible](http://classic.net.bible.org/strong.php?id=96)

[Bible Bento](https://biblebento.com/dictionary/G0096.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0096/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0096.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g96)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CE%B4%CF%8C%CE%BA%CE%B9%CE%BC%CE%BF%CF%82)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33839)

[Precept Austin](https://www.preceptaustin.org/titus_116#worthless)
