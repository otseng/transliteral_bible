# ζώννυμι

[zōnnymi](https://www.blueletterbible.org/lexicon/g2224)

Definition: gird (2x).

Part of speech: verb

Occurs 2 times in 1 verses

Greek: [anazōnnymi](../g/g328.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2224)

[Study Light](https://www.studylight.org/lexicons/greek/2224.html)

[Bible Hub](https://biblehub.com/str/greek/2224.htm)

[LSJ](https://lsj.gr/wiki/ζώννυμι)

[NET Bible](http://classic.net.bible.org/strong.php?id=2224)

[Bible Bento](https://biblebento.com/dictionary/G2224.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2224/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2224.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2224)

