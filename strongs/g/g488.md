# ἀντιμετρέω

[antimetreō](https://www.blueletterbible.org/lexicon/g488)

Definition: measure again (2x), measure back, to measure in return, repay

Part of speech: verb

Occurs 4 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0488)

[Study Light](https://www.studylight.org/lexicons/greek/488.html)

[Bible Hub](https://biblehub.com/str/greek/488.htm)

[LSJ](https://lsj.gr/wiki/ἀντιμετρέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=488)

[Bible Bento](https://biblebento.com/dictionary/G0488.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0488/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0488.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g488)