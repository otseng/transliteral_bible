# μίλιον

[milion](https://www.blueletterbible.org/lexicon/g3400)

Definition: mile (1x), among the Romans the distance of a thousand paces or eight stadia, about 1.5 km 

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3400)

[Study Light](https://www.studylight.org/lexicons/greek/3400.html)

[Bible Hub](https://biblehub.com/str/greek/3400.htm)

[LSJ](https://lsj.gr/wiki/μίλιον)

[NET Bible](http://classic.net.bible.org/strong.php?id=3400)

[Bible Bento](https://biblebento.com/dictionary/G3400.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3400/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3400.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3400)
