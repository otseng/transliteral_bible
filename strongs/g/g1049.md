# γαζοφυλάκιον

[gazophylakion](https://www.blueletterbible.org/lexicon/g1049)

Definition: treasury, offertory (box)

Part of speech: neuter noun

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1049)

[Study Light](https://www.studylight.org/lexicons/greek/1049.html)

[Bible Hub](https://biblehub.com/str/greek/1049.htm)

[LSJ](https://lsj.gr/wiki/γαζοφυλάκιον)

[NET Bible](http://classic.net.bible.org/strong.php?id=1049)

[Bible Bento](https://biblebento.com/dictionary/G1049.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1049/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1049.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1049)

[Wiktionary](https://en.wiktionary.org/wiki/gazophylacium)