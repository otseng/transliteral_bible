# ψευδάδελφος

[pseudadelphos](https://www.blueletterbible.org/lexicon/g5569)

Definition: false brother (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5569)

[Study Light](https://www.studylight.org/lexicons/greek/5569.html)

[Bible Hub](https://biblehub.com/str/greek/5569.htm)

[LSJ](https://lsj.gr/wiki/ψευδάδελφος)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5569)

[NET Bible](http://classic.net.bible.org/strong.php?id=5569)

[Bible Bento](https://biblebento.com/dictionary/G5569.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5569/greek)
