# θύϊνος

[thuinos](https://www.blueletterbible.org/lexicon/g2367)

Definition: thyine (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2367)

[Study Light](https://www.studylight.org/lexicons/greek/2367.html)

[Bible Hub](https://biblehub.com/str/greek/2367.htm)

[LSJ](https://lsj.gr/wiki/θύϊνος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2367)

[Bible Bento](https://biblebento.com/dictionary/G2367.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2367/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2367.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2367)

