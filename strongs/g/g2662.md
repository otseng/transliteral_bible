# καταπατέω

[katapateō](https://www.blueletterbible.org/lexicon/g2662)

Definition: tread underfoot (2x), trample (1x), tread down (1x), tread (1x), treat with rudeness and insult, spurn, treat with insulting neglect, reject with disdain

Part of speech: verb

Occurs 5 times in 5 verses

Hebrew: [rāmas](../h/h7429.md), [dûš](../h/h1758.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2662)

[Study Light](https://www.studylight.org/lexicons/greek/2662.html)

[Bible Hub](https://biblehub.com/str/greek/2662.htm)

[LSJ](https://lsj.gr/wiki/καταπατέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2662)

[Bible Bento](https://biblebento.com/dictionary/G2662.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2662/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2662.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2662)