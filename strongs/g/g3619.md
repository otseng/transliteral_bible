# οἰκοδομή

[oikodomē](https://www.blueletterbible.org/lexicon/g3619)

Definition: edifying (7x), building (6x), edification (4x), wherewith (one) may edify (1x), build a house

Part of speech: feminine noun

Occurs 18 times in 18 verses

Greek: [oikos](../g/g3624.md), [dōma](../g/g1430.md)

Hebrew: [bîrâ](../h/h1002.md), [binyān](../h/h1146.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3619)

[Study Light](https://www.studylight.org/lexicons/greek/3619.html)

[Bible Hub](https://biblehub.com/str/greek/3619.htm)

[LSJ](https://lsj.gr/wiki/οἰκοδομή)

[NET Bible](http://classic.net.bible.org/strong.php?id=3619)

[Bible Bento](https://biblebento.com/dictionary/G3619.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3619/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3619.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3619)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BF%CE%B9%CE%BA%CE%BF%CE%B4%CE%BF%CE%BC%CE%AE)

[Precept Austin](https://www.preceptaustin.org/ephesians_221-22#b)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33657)