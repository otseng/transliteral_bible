# ἐγκαίνια

[egkainia](https://www.blueletterbible.org/lexicon/g1456)

Definition: feast of dedication (1x).

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1456)

[Study Light](https://www.studylight.org/lexicons/greek/1456.html)

[Bible Hub](https://biblehub.com/str/greek/1456.htm)

[LSJ](https://lsj.gr/wiki/ἐγκαίνια)

[NET Bible](http://classic.net.bible.org/strong.php?id=1456)

[Bible Bento](https://biblebento.com/dictionary/G1456.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1456/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1456.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1456)

