# κομάω

[komaō](https://www.blueletterbible.org/lexicon/g2863)

Definition: have long hair (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2863)

[Study Light](https://www.studylight.org/lexicons/greek/2863.html)

[Bible Hub](https://biblehub.com/str/greek/2863.htm)

[LSJ](https://lsj.gr/wiki/κομάω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2863)

[Bible Bento](https://biblebento.com/dictionary/G2863.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2863/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2863.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2863)
