# εἰσκαλέομαι

[eiskaleomai](https://www.blueletterbible.org/lexicon/g1528)

Definition: call in (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1528)

[Study Light](https://www.studylight.org/lexicons/greek/1528.html)

[Bible Hub](https://biblehub.com/str/greek/1528.htm)

[LSJ](https://lsj.gr/wiki/εἰσκαλέομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1528)

[Bible Bento](https://biblebento.com/dictionary/G1528.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1528/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1528.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1528)

