# ἀνάκρισις

[anakrisis](https://www.blueletterbible.org/lexicon/g351)

Definition: examination (1x), investigation

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0351)

[Study Light](https://www.studylight.org/lexicons/greek/351.html)

[Bible Hub](https://biblehub.com/str/greek/351.htm)

[LSJ](https://lsj.gr/wiki/ἀνάκρισις)

[NET Bible](http://classic.net.bible.org/strong.php?id=351)

[Bible Bento](https://biblebento.com/dictionary/G0351.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0351/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0351.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g351)
