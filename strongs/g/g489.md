# ἀντιμισθία

[antimisthia](https://www.blueletterbible.org/lexicon/g489)

Definition: recompense (2x), a reward given in compensation, requital

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0489)

[Study Light](https://www.studylight.org/lexicons/greek/489.html)

[Bible Hub](https://biblehub.com/str/greek/489.htm)

[LSJ](https://lsj.gr/wiki/ἀντιμισθία)

[NET Bible](http://classic.net.bible.org/strong.php?id=489)

[Bible Bento](https://biblebento.com/dictionary/G0489.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0489/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0489.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g489)
