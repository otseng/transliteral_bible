# Τέρτυλλος

[Tertyllos](https://www.blueletterbible.org/lexicon/g5061)

Definition: Tertullus (2x), "triple-hardened", a Roman orator

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5061)

[Study Light](https://www.studylight.org/lexicons/greek/5061.html)

[Bible Hub](https://biblehub.com/str/greek/5061.htm)

[LSJ](https://lsj.gr/wiki/Τέρτυλλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5061)

[Bible Bento](https://biblebento.com/dictionary/G5061.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5061/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5061.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5061)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/T/tertullus.html)

[Video Bible](https://www.videobible.com/bible-dictionary/tertullus)