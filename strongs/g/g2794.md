# κίνδυνος

[kindynos](https://www.blueletterbible.org/lexicon/g2794)

Definition: peril (9x), danger

Part of speech: masculine noun

Occurs 9 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2794)

[Study Light](https://www.studylight.org/lexicons/greek/2794.html)

[Bible Hub](https://biblehub.com/str/greek/2794.htm)

[LSJ](https://lsj.gr/wiki/κίνδυνος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2794)

[Bible Bento](https://biblebento.com/dictionary/G2794.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2794/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2794.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2794)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%AF%CE%BD%CE%B4%CF%85%CE%BD%CE%BF%CF%82)
