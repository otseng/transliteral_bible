# ὥρα

[hōra](https://www.blueletterbible.org/lexicon/g5610)

Definition: hour (89x), time (11x), season (3x)

Part of speech: feminine noun

Occurs 108 times in 100 verses

Hebrew: [ʿēṯ](../h/h6256.md)

Derived words: hour

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5610)

[Study Light](https://www.studylight.org/lexicons/greek/5610.html)

[Bible Hub](https://biblehub.com/str/greek/5610.htm)

[LSJ](https://lsj.gr/wiki/ὥρα)

[NET Bible](http://classic.net.bible.org/strong.php?id=5610)

[Bible Bento](https://biblebento.com/dictionary/G5610.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5610/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5610.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5610)