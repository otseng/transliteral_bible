# δουλαγωγέω

[doulagōgeō](https://www.blueletterbible.org/lexicon/g1396)

Definition: bring into subjection (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1396)

[Study Light](https://www.studylight.org/lexicons/greek/1396.html)

[Bible Hub](https://biblehub.com/str/greek/1396.htm)

[LSJ](https://lsj.gr/wiki/δουλαγωγέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1396)

[Bible Bento](https://biblebento.com/dictionary/G1396.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1396/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1396.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1396)
