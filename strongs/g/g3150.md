# ματαιολογία

[mataiologia](https://www.blueletterbible.org/lexicon/g3150)

Definition: vain jangling (1x), vain talking, empty talk

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3150)

[Study Light](https://www.studylight.org/lexicons/greek/3150.html)

[Bible Hub](https://biblehub.com/str/greek/3150.htm)

[LSJ](https://lsj.gr/wiki/ματαιολογία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3150)

[Bible Bento](https://biblebento.com/dictionary/G3150.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3150/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3150.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3150)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BC%CE%B1%CF%84%CE%B1%CE%B9%CE%BF%CE%BB%CE%BF%CE%B3%CE%AF%CE%B1)
