# ἅλλομαι

[hallomai](https://www.blueletterbible.org/lexicon/g242)

Definition: leap (2x), spring up (1x)

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0242)

[Study Light](https://www.studylight.org/lexicons/greek/242.html)

[Bible Hub](https://biblehub.com/str/greek/242.htm)

[LSJ](https://lsj.gr/wiki/ἅλλομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=242)

[Bible Bento](https://biblebento.com/dictionary/G0242.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0242/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0242.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g242)
