# προσπίπτω

[prospiptō](https://www.blueletterbible.org/lexicon/g4363)

Definition: fall down before (5x), beat upon (1x), fall down at (1x), fall (1x), to fall forwards, fall down, prostrate one's self before, in homage or supplication: at one's feet

Part of speech: verb

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4363)

[Study Light](https://www.studylight.org/lexicons/greek/4363.html)

[Bible Hub](https://biblehub.com/str/greek/4363.htm)

[LSJ](https://lsj.gr/wiki/προσπίπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4363)

[Bible Bento](https://biblebento.com/dictionary/G4363.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4363/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4363.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4363)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CF%81%CE%BF%CF%83%CF%80%CE%AF%CF%80%CF%84%CF%89)