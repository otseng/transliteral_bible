# κοιμάω

[koimaō](https://www.blueletterbible.org/lexicon/g2837)

Definition: sleep (10x), fall asleep (4x), be asleep (2x), fall on sleep (1x), be dead (1x), dead, quiet

Part of speech: verb

Occurs 19 times in 18 verses

Hebrew: [shakab](../h/h7901.md), [yashen](../h/h3462.md), [rāḏam](../h/h7290.md)

Derived words: [cemetary](https://www.etymonline.com/word/cemetery)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2837)

[Study Light](https://www.studylight.org/lexicons/greek/2837.html)

[Bible Hub](https://biblehub.com/str/greek/2837.htm)

[LSJ](https://lsj.gr/wiki/κοιμάω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2837)

[Bible Bento](https://biblebento.com/dictionary/G2837.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2837/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2837.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2837)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%BF%CE%B9%CE%BC%CE%AC%CF%89)

[Precept Austin](https://www.preceptaustin.org/1corinthians_156-8#asleep)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34031)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-definitions/2837-koimao.htm)
