# ἐπιγράφω

[epigraphō](https://www.blueletterbible.org/lexicon/g1924)

Definition: write (2x), write over (1x), write thereon (1x), with this inscription (with G1722) (with G3639) (1x), inscribe

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1924)

[Study Light](https://www.studylight.org/lexicons/greek/1924.html)

[Bible Hub](https://biblehub.com/str/greek/1924.htm)

[LSJ](https://lsj.gr/wiki/ἐπιγράφω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1924)

[Bible Bento](https://biblebento.com/dictionary/G1924.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1924/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1924.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1924)