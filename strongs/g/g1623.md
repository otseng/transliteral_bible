# ἕκτος

[ektos](https://www.blueletterbible.org/lexicon/g1623)

Definition: sixth (14x).

Part of speech: adjective

Occurs 14 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1623)

[Study Light](https://www.studylight.org/lexicons/greek/1623.html)

[Bible Hub](https://biblehub.com/str/greek/1623.htm)

[LSJ](https://lsj.gr/wiki/ἕκτος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1623)

[Bible Bento](https://biblebento.com/dictionary/G1623.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1623/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1623.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1623)

