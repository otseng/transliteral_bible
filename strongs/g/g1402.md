# δουλόω

[douloō](https://www.blueletterbible.org/lexicon/g1402)

Definition: become servant (2x), bring into bondage (2x), be under bondage (1x), given (1x), make servant (1x), in bondage (1x), give myself wholly to one's needs and service, make myself a bondman to him

Part of speech: masculine noun

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1402)

[Study Light](https://www.studylight.org/lexicons/greek/1402.html)

[Bible Hub](https://biblehub.com/str/greek/1402.htm)

[LSJ](https://lsj.gr/wiki/δουλόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1402)

[Bible Bento](https://biblebento.com/dictionary/G1402.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1402/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1402.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1402)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33969)

[Precept Austin](https://www.preceptaustin.org/2_peter_219#e)
