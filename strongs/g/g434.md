# ἀνήμερος

[anēmeros](https://www.blueletterbible.org/lexicon/g434)

Definition: fierce (1x), not tame, savage

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0434)

[Study Light](https://www.studylight.org/lexicons/greek/434.html)

[Bible Hub](https://biblehub.com/str/greek/434.htm)

[LSJ](https://lsj.gr/wiki/ἀνήμερος)

[NET Bible](http://classic.net.bible.org/strong.php?id=434)

[Bible Bento](https://biblebento.com/dictionary/G0434.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/434/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/434.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g434)
