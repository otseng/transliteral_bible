# εἴσοδος

[eisodos](https://www.blueletterbible.org/lexicon/g1529)

Definition: coming (1x), entering in (1x), entrance in (1x), to enter into (with G1519) (1x), entrance (1x).

Part of speech: feminine noun

Occurs 5 times in 5 verses

Hebrew: [peṯaḥ](../h/h6607.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1529)

[Study Light](https://www.studylight.org/lexicons/greek/1529.html)

[Bible Hub](https://biblehub.com/str/greek/1529.htm)

[LSJ](https://lsj.gr/wiki/εἴσοδος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1529)

[Bible Bento](https://biblebento.com/dictionary/G1529.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1529/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1529.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1529)
