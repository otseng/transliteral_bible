# χωρέω

[chōreō](https://www.blueletterbible.org/lexicon/g5562)

Definition: receive (3x), contain (2x), come (1x), go (1x), have place (1x), cannot receive (with G3756) (1x), be room to receive (1x), contain, space, room

Part of speech: verb

Occurs 12 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5562)

[Study Light](https://www.studylight.org/lexicons/greek/5562.html)

[Bible Hub](https://biblehub.com/str/greek/5562.htm)

[LSJ](https://lsj.gr/wiki/χωρέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5562)

[Bible Bento](https://biblebento.com/dictionary/G5562.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5562/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5562.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5562)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%87%CF%89%CF%81%CE%AD%CF%89)