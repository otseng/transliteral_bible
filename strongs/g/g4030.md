# περικεφαλαία

[perikephalaia](https://www.blueletterbible.org/lexicon/g4030)

Definition: helmet (2x)

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4030)

[Study Light](https://www.studylight.org/lexicons/greek/4030.html)

[Bible Hub](https://biblehub.com/str/greek/4030.htm)

[LSJ](https://lsj.gr/wiki/περικεφαλαία)

[NET Bible](http://classic.net.bible.org/strong.php?id=4030)

[Bible Bento](https://biblebento.com/dictionary/G4030.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4030/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4030.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4030)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34216)

[Precept Austin](https://www.preceptaustin.org/ephesians_616-17#helmet)