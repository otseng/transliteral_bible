# λόγος

[logos](https://www.blueletterbible.org/lexicon/g3056)

Definition: word (218x), saying (50x), account (8x), speech (8x), Word (Christ) (7x), thing (5x), tidings(1x), matter, decree, discourse, doctrine, teaching, narrative, reason, cause, ground, work

Part of speech: masculine noun

Occurs 331 times in 316 verses

Hebrew: [dabar](../h/h1697.md)

Derived words: logo, theology, dialogue, logic

Synonyms: [word](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Word)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3056)

[Study Light](https://www.studylight.org/lexicons/greek/3056.html)

[Bible Hub](https://biblehub.com/str/greek/3056.htm)

[LSJ](https://lsj.gr/wiki/λόγος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3056)

[Bible Bento](https://biblebento.com/dictionary/G3056.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3056/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3056.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3056)

[Wikipedia](https://en.wikipedia.org/wiki/Logos)

[Logos vs Rhema](https://blog.biblesforamerica.org/two-important-greek-words-in-the-bible-emlogosem-and-emrhemaem/)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/3056-logos-word.htm)
