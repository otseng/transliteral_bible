# φιλοφρόνως

[philophronōs](https://www.blueletterbible.org/lexicon/g5390)

Definition: courteously (1x), kindly, in a friendly manner

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5390)

[Study Light](https://www.studylight.org/lexicons/greek/5390.html)

[Bible Hub](https://biblehub.com/str/greek/5390.htm)

[LSJ](https://lsj.gr/wiki/φιλοφρόνως)

[NET Bible](http://classic.net.bible.org/strong.php?id=5390)

[Bible Bento](https://biblebento.com/dictionary/G5390.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5390/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5390.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5390)
