# μακρός

[makros](https://www.blueletterbible.org/lexicon/g3117)

Definition: long (3x), far (2x), remote, long lasting

Part of speech: adjective

Occurs 5 times in 5 verses

Greek: [makran](../g/g3112.md), [makrothen](../g/g3113.md)

Hebrew: ['ōreḵ](../h/h753.md)

Derived words: [macron](https://en.wiktionary.org/wiki/macron#English), macroscopic, [macro-](https://en.wiktionary.org/wiki/macro-)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3117)

[Study Light](https://www.studylight.org/lexicons/greek/3117.html)

[Bible Hub](https://biblehub.com/str/greek/3117.htm)

[LSJ](https://lsj.gr/wiki/μακρός)

[NET Bible](http://classic.net.bible.org/strong.php?id=3117)

[Bible Bento](https://biblebento.com/dictionary/G3117.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3117/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3117.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3117)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BC%CE%B1%CE%BA%CF%81%CF%8C%CF%82)