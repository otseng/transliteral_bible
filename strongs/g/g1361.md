# Διοτρεφής

[diotrephēs](https://www.blueletterbible.org/lexicon/g1361)

Definition: Diotrephes (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1361)

[Study Light](https://www.studylight.org/lexicons/greek/1361.html)

[Bible Hub](https://biblehub.com/str/greek/1361.htm)

[LSJ](https://lsj.gr/wiki/Διοτρεφής)

[NET Bible](http://classic.net.bible.org/strong.php?id=1361)

[Bible Bento](https://biblebento.com/dictionary/G1361.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1361/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1361.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1361)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/diotrephes.html)

[Video Bible](https://www.videobible.com/bible-dictionary/diotrephes)