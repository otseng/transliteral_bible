# ἀποβλέπω

[apoblepō](https://www.blueletterbible.org/lexicon/g578)

Definition: have respect (1x), to look at attentively, to look with steadfast mental gaze

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0578)

[Study Light](https://www.studylight.org/lexicons/greek/578.html)

[Bible Hub](https://biblehub.com/str/greek/578.htm)

[LSJ](https://lsj.gr/wiki/ἀποβλέπω)

[NET Bible](http://classic.net.bible.org/strong.php?id=578)

[Bible Bento](https://biblebento.com/dictionary/G578.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/578/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/578.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g578)
