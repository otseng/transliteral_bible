# ἵνα

[hina](https://www.blueletterbible.org/lexicon/g2443)

Definition: that (486x), to (76x), miscellaneous (8x).

Part of speech: conjunction

Occurs 570 times in 535 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2443)

[Study Light](https://www.studylight.org/lexicons/greek/2443.html)

[Bible Hub](https://biblehub.com/str/greek/2443.htm)

[LSJ](https://lsj.gr/wiki/ἵνα)

[NET Bible](http://classic.net.bible.org/strong.php?id=2443)

[Bible Bento](https://biblebento.com/dictionary/G2443.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2443/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2443.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2443)
