# τέλειος

[teleios](https://www.blueletterbible.org/lexicon/g5046)

Definition: perfect (17x), man (1x), of full age (1x), finished, complete, mature, fully developed, full grown, brought to its end, finished, wanting nothing necessary to completeness, in good working order

Part of speech: adjective

Occurs 19 times in 17 verses

Hebrew: [šālēm](../h/h8003.md), [tamiym](../h/h8549.md)

Synonyms: [perfect](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Perfect)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5046)

[Study Light](https://www.studylight.org/lexicons/greek/5046.html)

[Bible Hub](https://biblehub.com/str/greek/5046.htm)

[LSJ](https://lsj.gr/wiki/τέλειος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5046)

[Bible Bento](https://biblebento.com/dictionary/G5046.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5046/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5046.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5046)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%84%CE%AD%CE%BB%CE%B5%CE%B9%CE%BF%CF%82)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34591)