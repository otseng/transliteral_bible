# Μιχαήλ

[Michaēl](https://www.blueletterbible.org/lexicon/g3413)

Definition: Michael (2x), "who is like God", the first of the chief princes or archangels who is supposed to be the guardian angel of the Israelites

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

Hebrew: [Mîḵā'ēl](../h/h4317.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3413)

[Study Light](https://www.studylight.org/lexicons/greek/3413.html)

[Bible Hub](https://biblehub.com/str/greek/3413.htm)

[LSJ](https://lsj.gr/wiki/Μιχαήλ)

[NET Bible](http://classic.net.bible.org/strong.php?id=3413)

[Bible Bento](https://biblebento.com/dictionary/G3413.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3413/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3413.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3413)

[Wikipedia](https://en.wikipedia.org/wiki/Michael_%28archangel%29)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/michael.html)

[Video Bible](https://www.videobible.com/bible-dictionary/michael)