# Ἄρτεμις

[Artemis](https://www.blueletterbible.org/lexicon/g735)

Definition: Diana (5x), "complete light: flow restrained", one of the Greek gods; Artemis, that is to say the so called Tauric or Persian or Ephesian Artemis, the goddess of many Asiatic people, to be distinguished from the Artemis of the Greeks, the sister of Apollo. A very splendid temple was built to her at Ephesus, which was set on fire by Herostratus and reduced to ashes; but afterwards in the times of Alexander the Great, it was rebuilt in a style of greater magnificence.

Part of speech: proper feminine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0735)

[Study Light](https://www.studylight.org/lexicons/greek/735.html)

[Bible Hub](https://biblehub.com/str/greek/735.htm)

[LSJ](https://lsj.gr/wiki/Ἄρτεμις)

[NET Bible](http://classic.net.bible.org/strong.php?id=735)

[Bible Bento](https://biblebento.com/dictionary/G0735.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0735/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0735.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g735)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%8C%CF%81%CF%84%CE%B5%CE%BC%CE%B9%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Artemis)

[Greek Mythology](https://www.greekmythology.com/Olympians/Artemis/artemis.html)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/diana.html)