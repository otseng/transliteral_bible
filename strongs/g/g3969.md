# Πατροβᾶς

[patrobas](https://www.blueletterbible.org/lexicon/g3969)

Definition: Patrobas = paternal, a certain Christian in Rome

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3969)

[Study Light](https://www.studylight.org/lexicons/greek/3969.html)

[Bible Hub](https://biblehub.com/str/greek/3969.htm)

[LSJ](https://lsj.gr/wiki/Πατροβᾶς)

[NET Bible](http://classic.net.bible.org/strong.php?id=3969)

[Bible Bento](https://biblebento.com/dictionary/G3969.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3969/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3969.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3969)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/patrobas.html)

[Video Bible](https://www.videobible.com/bible-dictionary/patrobas)