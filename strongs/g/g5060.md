# Τέρτιος

[tertios](https://www.blueletterbible.org/lexicon/g5060)

Definition: Tertius = third, amanuensis of Paul in writing the epistle to the Romans

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5060)

[Study Light](https://www.studylight.org/lexicons/greek/5060.html)

[Bible Hub](https://biblehub.com/str/greek/5060.htm)

[LSJ](https://lsj.gr/wiki/Τέρτιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5060)

[Bible Bento](https://biblebento.com/dictionary/G5060.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5060/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5060.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5060)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/T/tertius.html)

[Video Bible](https://www.videobible.com/bible-dictionary/tertius)