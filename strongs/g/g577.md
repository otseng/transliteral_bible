# ἀποβάλλω

[apoballō](https://www.blueletterbible.org/lexicon/g577)

Definition: cast away (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0577)

[Study Light](https://www.studylight.org/lexicons/greek/577.html)

[Bible Hub](https://biblehub.com/str/greek/577.htm)

[LSJ](https://lsj.gr/wiki/ἀποβάλλω)

[NET Bible](http://classic.net.bible.org/strong.php?id=577)

[Bible Bento](https://biblebento.com/dictionary/G577.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/577/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/577.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g577)
