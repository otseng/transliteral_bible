# Κλεοπᾶς

[Kleopas](https://www.blueletterbible.org/lexicon/g2810)

Definition: Cleopas (1x), "of a renowned father"

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

Greek: [Klōpas](../g/g2832.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2810)

[Study Light](https://www.studylight.org/lexicons/greek/2810.html)

[Bible Hub](https://biblehub.com/str/greek/2810.htm)

[LSJ](https://lsj.gr/wiki/Κλεοπᾶς)

[NET Bible](http://classic.net.bible.org/strong.php?id=2810)

[Bible Bento](https://biblebento.com/dictionary/G2810.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2810/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2810.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2810)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%9A%CE%BB%CE%B5%CE%BF%CF%80%E1%BE%B6%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Cleopas)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/cleopas.html)

[Video Bible](https://www.videobible.com/bible-dictionary/cleopas)