# ἀμπελών

[ampelōn](https://www.blueletterbible.org/lexicon/g290)

Definition: vineyard (23x)

Part of speech: masculine noun

Occurs 23 times in 21 verses

Hebrew: [kerem](../h/h3754.md)

Derived words: [ampelotherapy](https://en.wikipedia.org/wiki/Grape_therapy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0290)

[Study Light](https://www.studylight.org/lexicons/greek/290.html)

[Bible Hub](https://biblehub.com/str/greek/290.htm)

[LSJ](https://lsj.gr/wiki/ἀμπελών)

[NET Bible](http://classic.net.bible.org/strong.php?id=290)

[Bible Bento](https://biblebento.com/dictionary/G0290.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0290/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0290.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g290)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CE%BC%CF%80%CE%B5%CE%BB%CF%8E%CE%BD)

[Wikipedia](https://en.wikipedia.org/wiki/Ampelos)