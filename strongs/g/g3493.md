# Ναχώρ

[Nachōr](https://www.blueletterbible.org/lexicon/g3493)

Definition: Nachor (1x), "snorting", was the name of two persons in the family of Abraham, an ancestor of Christ

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/3493.html)

[LSJ](https://lsj.gr/wiki/Ναχώρ)

[NET Bible](http://classic.net.bible.org/strong.php?id=3493)

[Bible Bento](https://biblebento.com/dictionary/G3493.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3493/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3493.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3493)

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3493)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/nachor.html)