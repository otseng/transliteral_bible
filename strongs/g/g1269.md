# διανεύω

[dianeuō](https://www.blueletterbible.org/lexicon/g1269)

Definition: beckoned (with G2258) (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1269)

[Study Light](https://www.studylight.org/lexicons/greek/1269.html)

[Bible Hub](https://biblehub.com/str/greek/1269.htm)

[LSJ](https://lsj.gr/wiki/διανεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1269)

[Bible Bento](https://biblebento.com/dictionary/G1269.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1269/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1269.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1269)

