# λαμβάνω

[lambanō](https://www.blueletterbible.org/lexicon/g2983)

Definition: receive (133x), take (106x), have (3x), catch (3x), remove

Part of speech: verb

Occurs 272 times in 248 verses

Hebrew: [lāḵaḏ](../h/h3920.md), [tāp̄aś](../h/h8610.md)

Synonyms: [take](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Take)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2983)

[Study Light](https://www.studylight.org/lexicons/greek/2983.html)

[Bible Hub](https://biblehub.com/str/greek/2983.htm)

[LSJ](https://lsj.gr/wiki/λαμβάνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2983)

[Bible Bento](https://biblebento.com/dictionary/G2983.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2983/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2983.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2983)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BB%CE%B1%CE%BC%CE%B2%CE%AC%CE%BD%CF%89)

[Precept Austin](https://www.preceptaustin.org/1_john_59_commentary#r)