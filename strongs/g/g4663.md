# σκώληξ

[skōlēx](https://www.blueletterbible.org/lexicon/g4663)

Definition: worm (3x), maggot, a worm, spec. that kind which preys upon dead bodies

Part of speech: masculine noun

Occurs 3 times in 3 verses

Hebrew: [tôlāʿ](../h/h8438.md)

Derived words: [scolex](https://www.collinsdictionary.com/us/dictionary/english/scolex), [scolite](https://www.merriam-webster.com/dictionary/scolite), [Scolecite](https://celestialearthminerals.com/atlas-of-minerals/scolecite/)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4663)

[Study Light](https://www.studylight.org/lexicons/greek/4663.html)

[Bible Hub](https://biblehub.com/str/greek/4663.htm)

[LSJ](https://lsj.gr/wiki/σκώληξ)

[NET Bible](http://classic.net.bible.org/strong.php?id=4663)

[Bible Bento](https://biblebento.com/dictionary/G4663.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4663/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4663.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4663)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CE%BA%CF%8E%CE%BB%CE%B7%CE%BE)