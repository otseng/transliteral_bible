# Ἀπολλῶς

[Apollōs](https://www.blueletterbible.org/lexicon/g625)

Definition: Apollos (10x), a learned Jew from Alexandria and mighty in the scriptures who became a Christian and a teacher of Christianity

Part of speech: proper masculine noun

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0625)

[Study Light](https://www.studylight.org/lexicons/greek/625.html)

[Bible Hub](https://biblehub.com/str/greek/625.htm)

[LSJ](https://lsj.gr/wiki/Ἀπολλῶς)

[NET Bible](http://classic.net.bible.org/strong.php?id=625)

[Bible Bento](https://biblebento.com/dictionary/G0625.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0625/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0625.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g625)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%88%CF%80%CE%BF%CE%BB%CE%BB%E1%BF%B6%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Apollos)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/apollos.html)

[Video Bible](https://www.videobible.com/bible-dictionary/apollos)