# συναρπάζω

[synarpazō](https://www.blueletterbible.org/lexicon/g4884)

Definition: catch (4x), to seize by force

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4884)

[Study Light](https://www.studylight.org/lexicons/greek/4884.html)

[Bible Hub](https://biblehub.com/str/greek/4884.htm)

[LSJ](https://lsj.gr/wiki/συναρπάζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4884)

[Bible Bento](https://biblebento.com/dictionary/G4884.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4884/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4884.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4884)
