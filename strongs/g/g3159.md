# Μαθθίας

[Maththias](https://www.blueletterbible.org/lexicon/g3159)

Definition: Matthias (2x), "gift of God"

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3159)

[Study Light](https://www.studylight.org/lexicons/greek/3159.html)

[Bible Hub](https://biblehub.com/str/greek/3159.htm)

[LSJ](https://lsj.gr/wiki/Μαθθίας)

[NET Bible](http://classic.net.bible.org/strong.php?id=3159)

[Bible Bento](https://biblebento.com/dictionary/G3159.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3159/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3159.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3159)

[Wikipedia](https://en.wikipedia.org/wiki/Saint_Matthias)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/matthias.html)

[Video Bible](https://www.videobible.com/bible-dictionary/matthias)