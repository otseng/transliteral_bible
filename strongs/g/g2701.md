# κατατρέχω

[katatrechō](https://www.blueletterbible.org/lexicon/g2701)

Definition: run down (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2701)

[Study Light](https://www.studylight.org/lexicons/greek/2701.html)

[Bible Hub](https://biblehub.com/str/greek/2701.htm)

[LSJ](https://lsj.gr/wiki/κατατρέχω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2701)

[Bible Bento](https://biblebento.com/dictionary/G2701.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2701/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2701.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2701)

