# κάκωσις

[kakōsis](https://www.blueletterbible.org/lexicon/g2561)

Definition: affliction (1x), ill treatment, ill usage, maltreatment

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2561)

[Study Light](https://www.studylight.org/lexicons/greek/2561.html)

[Bible Hub](https://biblehub.com/str/greek/2561.htm)

[LSJ](https://lsj.gr/wiki/κάκωσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=2561)

[Bible Bento](https://biblebento.com/dictionary/G2561.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2561/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2561.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2561)
