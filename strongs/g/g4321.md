# προσαναλίσκω

[prosanaliskō](https://www.blueletterbible.org/lexicon/g4321)

Definition: spend (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4321)

[Study Light](https://www.studylight.org/lexicons/greek/4321.html)

[Bible Hub](https://biblehub.com/str/greek/4321.htm)

[LSJ](https://lsj.gr/wiki/προσαναλίσκω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4321)

[Bible Bento](https://biblebento.com/dictionary/G4321.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4321/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4321.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4321)

