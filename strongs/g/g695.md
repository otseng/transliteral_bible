# ἀργυροκόπος

[argyrokopos](https://www.blueletterbible.org/lexicon/g695)

Definition: silversmith (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0695)

[Study Light](https://www.studylight.org/lexicons/greek/695.html)

[Bible Hub](https://biblehub.com/str/greek/695.htm)

[LSJ](https://lsj.gr/wiki/ἀργυροκόπος)

[NET Bible](http://classic.net.bible.org/strong.php?id=695)

[Bible Bento](https://biblebento.com/dictionary/G0695.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0695/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0695.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g695)
