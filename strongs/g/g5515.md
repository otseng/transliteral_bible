# χλωρός

[chlōros](https://www.blueletterbible.org/lexicon/g5515)

Definition: green (3x), pale (1x), yellow

Part of speech: adjective

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5515)

[Study Light](https://www.studylight.org/lexicons/greek/5515.html)

[Bible Hub](https://biblehub.com/str/greek/5515.htm)

[LSJ](https://lsj.gr/wiki/χλωρός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5515)

[Bible Bento](https://biblebento.com/dictionary/G5515.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5515/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5515.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5515)

