# ἀγνόημα

[agnoēma](https://www.blueletterbible.org/lexicon/g51)

Definition: error (1x), shortcoming

Part of speech: neuter noun

Occurs 1 times in 1 verses

Greek: [agnoeō](../g/g50.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0051)

[Study Light](https://www.studylight.org/lexicons/greek/51.html)

[Bible Hub](https://biblehub.com/str/greek/51.htm)

[LSJ](https://lsj.gr/wiki/ἀγνόημα)

[NET Bible](http://classic.net.bible.org/strong.php?id=51)

[Bible Bento](https://biblebento.com/dictionary/G0051.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/51/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/51.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g51)
