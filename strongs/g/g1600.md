# ἐκπετάννυμι

[ekpetannymi](https://www.blueletterbible.org/lexicon/g1600)

Definition: stretch forth (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [pāraṣ](../h/h6555.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1600)

[Study Light](https://www.studylight.org/lexicons/greek/1600.html)

[Bible Hub](https://biblehub.com/str/greek/1600.htm)

[LSJ](https://lsj.gr/wiki/ἐκπετάννυμι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1600)

[Bible Bento](https://biblebento.com/dictionary/G1600.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1600/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1600.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1600)
