# ὠφέλιμος

[ōphelimos](https://www.blueletterbible.org/lexicon/g5624)

Definition: profitable (3x), profit (with G2076) (1x)

Part of speech: adjective

Occurs 3 times in 3 verses

Greek: [ophelos](../g/g3786.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5624)

[Study Light](https://www.studylight.org/lexicons/greek/5624.html)

[Bible Hub](https://biblehub.com/str/greek/5624.htm)

[LSJ](https://lsj.gr/wiki/ὠφέλιμος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5624)

[Bible Bento](https://biblebento.com/dictionary/G5624.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5624/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5624.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5624)
