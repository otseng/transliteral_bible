# κτῆμα

[ktēma](https://www.blueletterbible.org/lexicon/g2933)

Definition: possession (4x), property

Part of speech: neuter noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2933)

[Study Light](https://www.studylight.org/lexicons/greek/2933.html)

[Bible Hub](https://biblehub.com/str/greek/2933.htm)

[LSJ](https://lsj.gr/wiki/κτῆμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=2933)

[Bible Bento](https://biblebento.com/dictionary/G2933.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2933/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2933.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2933)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CF%84%E1%BF%86%CE%BC%CE%B1)