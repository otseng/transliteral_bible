# νουθεσία

[nouthesia](https://www.blueletterbible.org/lexicon/g3559)

Definition: admonition (3x), exhortation, mild rebuke or warning

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3559)

[Study Light](https://www.studylight.org/lexicons/greek/3559.html)

[Bible Hub](https://biblehub.com/str/greek/3559.htm)

[LSJ](https://lsj.gr/wiki/νουθεσία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3559)

[Bible Bento](https://biblebento.com/dictionary/G3559.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3559/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3559.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3559)
