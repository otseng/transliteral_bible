# εὐδία

[eudia](https://www.blueletterbible.org/lexicon/g2105)

Definition: fair weather (1x), a serene sky

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2105)

[Study Light](https://www.studylight.org/lexicons/greek/2105.html)

[Bible Hub](https://biblehub.com/str/greek/2105.htm)

[LSJ](https://lsj.gr/wiki/εὐδία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2105)

[Bible Bento](https://biblebento.com/dictionary/G2105.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2105/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2105.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2105)
