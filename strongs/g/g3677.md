# ὄναρ

[onar](https://www.blueletterbible.org/lexicon/g3677)

Definition: dream (6x).

Part of speech: neuter noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3677)

[Study Light](https://www.studylight.org/lexicons/greek/3677.html)

[Bible Hub](https://biblehub.com/str/greek/3677.htm)

[LSJ](https://lsj.gr/wiki/ὄναρ)

[NET Bible](http://classic.net.bible.org/strong.php?id=3677)

[Bible Bento](https://biblebento.com/dictionary/G3677.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3677/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3677.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3677)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BD%84%CE%BD%CE%B1%CF%81)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/3677-onar-dream.htm)
