# πολίτης

[politēs](https://www.blueletterbible.org/lexicon/g4177)

Definition: citizen (3x), fellow countryman

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4177)

[Study Light](https://www.studylight.org/lexicons/greek/4177.html)

[Bible Hub](https://biblehub.com/str/greek/4177.htm)

[LSJ](https://lsj.gr/wiki/πολίτης)

[NET Bible](http://classic.net.bible.org/strong.php?id=4177)

[Bible Bento](https://biblebento.com/dictionary/G4177.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4177/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4177.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4177)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%BF%CE%BB%CE%AF%CF%84%CE%B7%CF%82)