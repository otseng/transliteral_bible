# τελείωσις

[teleiōsis](https://www.blueletterbible.org/lexicon/g5050)

Definition: performance (1x), perfection (1x), accomplishment, consummation, completion, verification

Part of speech: feminine noun

Occurs 2 times in 2 verses

Derived words: [spermateliosis](https://www.merriam-webster.com/dictionary/spermateliosis)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5050)

[Study Light](https://www.studylight.org/lexicons/greek/5050.html)

[Bible Hub](https://biblehub.com/str/greek/5050.htm)

[LSJ](https://lsj.gr/wiki/τελείωσις)

[NET Bible](http://classic.net.bible.org/strong.php?id=5050)

[Bible Bento](https://biblebento.com/dictionary/G5050.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5050/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5050.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5050)