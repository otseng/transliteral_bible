# φύλλον

[phyllon](https://www.blueletterbible.org/lexicon/g5444)

Definition: leaf (6x)

Part of speech: neuter noun

Occurs 6 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5444)

[Study Light](https://www.studylight.org/lexicons/greek/5444.html)

[Bible Hub](https://biblehub.com/str/greek/5444.htm)

[LSJ](https://lsj.gr/wiki/φύλλον)

[NET Bible](http://classic.net.bible.org/strong.php?id=5444)

[Bible Bento](https://biblebento.com/dictionary/G5444.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5444/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5444.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5444)

[Wiktionary](https://en.wiktionary.org/wiki/phyllon)