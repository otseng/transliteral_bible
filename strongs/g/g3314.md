# μεσημβρία

[mesēmbria](https://www.blueletterbible.org/lexicon/g3314)

Definition: south (1x), noon (1x), midday

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3314)

[Study Light](https://www.studylight.org/lexicons/greek/3314.html)

[Bible Hub](https://biblehub.com/str/greek/3314.htm)

[LSJ](https://lsj.gr/wiki/μεσημβρία)

[NET Bible](http://classic.net.bible.org/strong.php?id=3314)

[Bible Bento](https://biblebento.com/dictionary/G3314.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3314/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3314.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3314)
