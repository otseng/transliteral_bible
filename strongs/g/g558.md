# ἀπελεύθερος

[apeleutheros](https://www.blueletterbible.org/lexicon/g558)

Definition: freeman (1x), a slave that has been released from servitude

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0558)

[Study Light](https://www.studylight.org/lexicons/greek/558.html)

[Bible Hub](https://biblehub.com/str/greek/558.htm)

[LSJ](https://lsj.gr/wiki/ἀπελεύθερος)

[NET Bible](http://classic.net.bible.org/strong.php?id=558)

[Bible Bento](https://biblebento.com/dictionary/G0558.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0558/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0558.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g558)
