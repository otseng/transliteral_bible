# κέρδος

[kerdos](https://www.blueletterbible.org/lexicon/g2771)

Definition: gain (2x), lucre (1x), advantage

Part of speech: neuter noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2771)

[Study Light](https://www.studylight.org/lexicons/greek/2771.html)

[Bible Hub](https://biblehub.com/str/greek/2771.htm)

[LSJ](https://lsj.gr/wiki/κέρδος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2771)

[Bible Bento](https://biblebento.com/dictionary/G2771.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2771/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2771.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2771)
