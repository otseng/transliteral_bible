# ἐναντίος

[enantios](https://www.blueletterbible.org/lexicon/g1727)

Definition: contrary (6x), against (2x), opposite, antagonistic, an opponent

Part of speech: adjective

Occurs 8 times in 8 verses

Derived words: [enantiomorph](https://www.dictionary.com/browse/enantiomorph)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1727)

[Study Light](https://www.studylight.org/lexicons/greek/1727.html)

[Bible Hub](https://biblehub.com/str/greek/1727.htm)

[LSJ](https://lsj.gr/wiki/ἐναντίος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1727)

[Bible Bento](https://biblebento.com/dictionary/G1727.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1727/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1727.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1727)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%90%CE%BD%CE%B1%CE%BD%CF%84%CE%AF%CE%BF%CF%82)
