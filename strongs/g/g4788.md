# συγκλείω

[sygkleiō](https://www.blueletterbible.org/lexicon/g4788)

Definition: conclude (2x), inclose (1x), shut up (1x).

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4788)

[Study Light](https://www.studylight.org/lexicons/greek/4788.html)

[Bible Hub](https://biblehub.com/str/greek/4788.htm)

[LSJ](https://lsj.gr/wiki/συγκλείω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4788)

[Bible Bento](https://biblebento.com/dictionary/G4788.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4788/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4788.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4788)
