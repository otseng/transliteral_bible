# εὐπροσωπέω

[euprosōpeō](https://www.blueletterbible.org/lexicon/g2146)

Definition: make a fair show (1x), to make a display

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2146)

[Study Light](https://www.studylight.org/lexicons/greek/2146.html)

[Bible Hub](https://biblehub.com/str/greek/2146.htm)

[LSJ](https://lsj.gr/wiki/εὐπροσωπέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2146)

[Bible Bento](https://biblebento.com/dictionary/G2146.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2146/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2146.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2146)
