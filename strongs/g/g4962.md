# συστρέφω

[systrephō](https://www.blueletterbible.org/lexicon/g4962)

Definition: gather (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [qāḇaṣ](../h/h6908.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4962)

[Study Light](https://www.studylight.org/lexicons/greek/4962.html)

[Bible Hub](https://biblehub.com/str/greek/4962.htm)

[LSJ](https://lsj.gr/wiki/συστρέφω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4962)

[Bible Bento](https://biblebento.com/dictionary/G4962.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4962/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4962.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4962)

