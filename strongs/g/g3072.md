# Λυκαονιστί

[Lykaonisti](https://www.blueletterbible.org/lexicon/g3072)

Definition: speech of Lycaonia (1x)

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3072)

[Study Light](https://www.studylight.org/lexicons/greek/3072.html)

[Bible Hub](https://biblehub.com/str/greek/3072.htm)

[LSJ](https://lsj.gr/wiki/Λυκαονιστί)

[NET Bible](http://classic.net.bible.org/strong.php?id=3072)

[Bible Bento](https://biblebento.com/dictionary/G3072.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3072/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3072.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3072)
