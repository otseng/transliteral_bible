# ἔριον

[erion](https://www.blueletterbible.org/lexicon/g2053)

Definition: wool (2x).

Part of speech: neuter noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2053)

[Study Light](https://www.studylight.org/lexicons/greek/2053.html)

[Bible Hub](https://biblehub.com/str/greek/2053.htm)

[LSJ](https://lsj.gr/wiki/ἔριον)

[NET Bible](http://classic.net.bible.org/strong.php?id=2053)

[Bible Bento](https://biblebento.com/dictionary/G2053.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2053/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2053.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2053)
