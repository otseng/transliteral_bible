# κράζω

[krazō](https://www.blueletterbible.org/lexicon/g2896)

Definition: cry (40x), cry out (19x), croak, scream, shriek, shout, (onomatopoeic word)

Part of speech: verb

Occurs 59 times in 58 verses

Hebrew: [qara'](../h/h7121.md), [rûaʿ](../h/h7321.md)

Synonyms: [cry](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Cry)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2896)

[Study Light](https://www.studylight.org/lexicons/greek/2896.html)

[Bible Hub](https://biblehub.com/str/greek/2896.htm)

[LSJ](https://lsj.gr/wiki/κράζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2896)

[Bible Bento](https://biblebento.com/dictionary/G2896.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2896/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2896.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2896)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CF%81%CE%AC%CE%B6%CF%89)

[Precept Austin](https://www.preceptaustin.org/romans_814-15#c)