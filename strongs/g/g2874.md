# κοπρία

[kopria](https://www.blueletterbible.org/lexicon/g2874)

Definition: dunghill (1x), dung (with G906) (1x), manure

Part of speech: feminine noun

Occurs 2 times in 2 verses

Hebrew: [dōmen](../h/h1828.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2874)

[Study Light](https://www.studylight.org/lexicons/greek/2874.html)

[Bible Hub](https://biblehub.com/str/greek/2874.htm)

[LSJ](https://lsj.gr/wiki/κοπρία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2874)

[Bible Bento](https://biblebento.com/dictionary/G2874.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2874/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2874.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2874)