# μωμάομαι

[mōmaomai](https://www.blueletterbible.org/lexicon/g3469)

Definition: blame (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3469)

[Study Light](https://www.studylight.org/lexicons/greek/3469.html)

[Bible Hub](https://biblehub.com/str/greek/3469.htm)

[LSJ](https://lsj.gr/wiki/μωμάομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=3469)

[Bible Bento](https://biblebento.com/dictionary/G3469.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3469/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3469.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3469)
