# Λευίς

[Leuis](https://www.blueletterbible.org/lexicon/g3018)

Definition: Levi (3x), "joined", a collector of customs

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3018)

[Study Light](https://www.studylight.org/lexicons/greek/3018.html)

[Bible Hub](https://biblehub.com/str/greek/3018.htm)

[LSJ](https://lsj.gr/wiki/Λευίς)

[NET Bible](http://classic.net.bible.org/strong.php?id=3018)

[Bible Bento](https://biblebento.com/dictionary/G3018.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3018/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3018.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3018)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/L/levi.html)