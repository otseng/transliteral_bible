# φθαρτός

[phthartos](https://www.blueletterbible.org/lexicon/g5349)

Definition: corruptible (6x), perishing, decaying, rot

Part of speech: adjective

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5349)

[Study Light](https://www.studylight.org/lexicons/greek/5349.html)

[Bible Hub](https://biblehub.com/str/greek/5349.htm)

[LSJ](https://lsj.gr/wiki/φθαρτός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5349)

[Bible Bento](https://biblebento.com/dictionary/G5349.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5349/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5349.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5349)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34595)