# παρουσία

[parousia](https://www.blueletterbible.org/lexicon/g3952)

Definition: coming (22x), presence (2x), arrival, advent, return, official visit, arrival or visit of a king or emperor, "being beside", arrival and a consequent presence with.

Part of speech: feminine noun

Occurs 24 times in 24 verses

Latin: [advent](https://en.wikipedia.org/wiki/Advent)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3952)

[Study Light](https://www.studylight.org/lexicons/greek/3952.html)

[Bible Hub](https://biblehub.com/str/greek/3952.htm)

[LSJ](https://lsj.gr/wiki/παρουσία)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%B1%CF%81%CE%BF%CF%85%CF%83%CE%AF%CE%B1)

[Wikipedia](https://en.wikipedia.org/wiki/Parousia)

[Net Bible](http://classic.net.bible.org/dictionary.php?word=Parousia)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34647)

[Precept Austin](https://www.preceptaustin.org/2_peter_115-18#coming)

[Encyclopedia.com](https://www.encyclopedia.com/religion/encyclopedias-almanacs-transcripts-and-maps/parousia)

[Encyclopedia of the Bible](https://www.biblegateway.com/resources/encyclopedia-of-the-bible/Parousia)

[Lexicon Concordance](http://lexiconcordance.com/greek/3952.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3952)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3952/greek)

[Bible Bento](https://biblebento.com/dictionary/G3952.html)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/3952-parousia-coming.htm)
