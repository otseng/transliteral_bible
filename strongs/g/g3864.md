# παραθαλάσσιος

[parathalassios](https://www.blueletterbible.org/lexicon/g3864)

Definition: upon the sea coast (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3864)

[Study Light](https://www.studylight.org/lexicons/greek/3864.html)

[Bible Hub](https://biblehub.com/str/greek/3864.htm)

[LSJ](https://lsj.gr/wiki/παραθαλάσσιος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3864)

[Bible Bento](https://biblebento.com/dictionary/G3864.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3864/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3864.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3864)

