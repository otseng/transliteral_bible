# ἀπάτωρ

[apatōr](https://www.blueletterbible.org/lexicon/g540)

Definition: without father (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0540)

[Study Light](https://www.studylight.org/lexicons/greek/540.html)

[Bible Hub](https://biblehub.com/str/greek/540.htm)

[LSJ](https://lsj.gr/wiki/ἀπάτωρ)

[NET Bible](http://classic.net.bible.org/strong.php?id=540)

[Bible Bento](https://biblebento.com/dictionary/G0540.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/540/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/540.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g540)
