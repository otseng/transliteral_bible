# ἐκθαμβέω

[ekthambeō](https://www.blueletterbible.org/lexicon/g1568)

Definition: be affrighted (2x), sore amazed (1x), greatly amazed (1x), to throw into terror or amazement, to be thoroughly amazed, astounded

Part of speech: verb

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1568)

[Study Light](https://www.studylight.org/lexicons/greek/1568.html)

[Bible Hub](https://biblehub.com/str/greek/1568.htm)

[LSJ](https://lsj.gr/wiki/ἐκθαμβέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1568)

[Bible Bento](https://biblebento.com/dictionary/G1568.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1568/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1568.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1568)

[Steadfast Lutherans](https://steadfastlutherans.org/2014/03/sore-amazed-what-does-this-mean/)