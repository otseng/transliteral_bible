# εὔσπλαγχνος

[eusplagchnos](https://www.blueletterbible.org/lexicon/g2155)

Definition: tenderhearted (1x), pitiful (1x), compassionate, sympathetic, "good bowels"

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2155)

[Study Light](https://www.studylight.org/lexicons/greek/2155.html)

[Bible Hub](https://biblehub.com/str/greek/2155.htm)

[LSJ](https://lsj.gr/wiki/εὔσπλαγχνος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2155)

[Bible Bento](https://biblebento.com/dictionary/G2155.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2155/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2155.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2155)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=35946)

[Renner](https://renner.org/be-pitiful-be-courteous/)