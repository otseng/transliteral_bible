# ἐπιπόθησις

[epipothēsis](https://www.blueletterbible.org/lexicon/g1972)

Definition: earnestly desire (1x), vehemently desire (1x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1972)

[Study Light](https://www.studylight.org/lexicons/greek/1972.html)

[Bible Hub](https://biblehub.com/str/greek/1972.htm)

[LSJ](https://lsj.gr/wiki/ἐπιπόθησις)

[NET Bible](http://classic.net.bible.org/strong.php?id=1972)

[Bible Bento](https://biblebento.com/dictionary/G1972.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1972/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1972.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1972)
