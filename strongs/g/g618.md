# ἀπολαμβάνω

[apolambanō](https://www.blueletterbible.org/lexicon/g618)

Definition: receive (10x), take aside (1x), receive ... again (1x), receive back

Part of speech: verb

Occurs 14 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0618)

[Study Light](https://www.studylight.org/lexicons/greek/618.html)

[Bible Hub](https://biblehub.com/str/greek/618.htm)

[LSJ](https://lsj.gr/wiki/ἀπολαμβάνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=618)

[Bible Bento](https://biblebento.com/dictionary/G0618.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0618/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0618.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g618)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CF%80%CE%BF%CE%BB%CE%B1%CE%BC%CE%B2%CE%AC%CE%BD%CF%89)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34735)