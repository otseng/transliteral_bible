# γογγύζω

[goggyzō](https://www.blueletterbible.org/lexicon/g1111)

Definition: murmur (8x), grumble, mutter

Part of speech: verb

Occurs 8 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1111)

[Study Light](https://www.studylight.org/lexicons/greek/1111.html)

[Bible Hub](https://biblehub.com/str/greek/1111.htm)

[LSJ](https://lsj.gr/wiki/γογγύζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1111)

[Bible Bento](https://biblebento.com/dictionary/G1111.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1111/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1111.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1111)