# διασπορά

[diaspora](https://www.blueletterbible.org/lexicon/g1290)

Definition: dispersed (1x), scatter abroad (1x), scattered (1x), "through sowing"

Part of speech: feminine noun

Occurs 3 times in 3 verses

Greek: [diaskorpizō](../g/g1287.md)

Derived words: [diaspora](https://en.wikipedia.org/wiki/Diaspora)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1290)

[Study Light](https://www.studylight.org/lexicons/greek/1290.html)

[Bible Hub](https://biblehub.com/str/greek/1290.htm)

[LSJ](https://lsj.gr/wiki/διασπορά)

[NET Bible](http://classic.net.bible.org/strong.php?id=1290)

[Bible Bento](https://biblebento.com/dictionary/G1290.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1290/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1290.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1290)

[Wiktionary](https://en.wiktionary.org/wiki/diaspora)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=35472)

[Precept Austin](https://www.preceptaustin.org/1peter_verse_by_verse_11-12#scatter%20diaspora)