# Πρίσκα

[priska](https://www.blueletterbible.org/lexicon/g4251)

Definition: Prisca (1x), "ancient", a Christian woman, the wife of Aquila

Part of speech: proper feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4251)

[Study Light](https://www.studylight.org/lexicons/greek/4251.html)

[Bible Hub](https://biblehub.com/str/greek/4251.htm)

[LSJ](https://lsj.gr/wiki/Πρίσκα)

[NET Bible](http://classic.net.bible.org/strong.php?id=4251)

[Bible Bento](https://biblebento.com/dictionary/G4251.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4251/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4251.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4251)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/prisca.html)