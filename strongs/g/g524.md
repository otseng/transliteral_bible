# ἀπαλγέω

[apalgeō](https://www.blueletterbible.org/lexicon/g524)

Definition: be past feeling (1x), to cease to feel pain or grief, to become callous, insensible to pain, apathetic

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0524)

[Study Light](https://www.studylight.org/lexicons/greek/524.html)

[Bible Hub](https://biblehub.com/str/greek/524.htm)

[LSJ](https://lsj.gr/wiki/ἀπαλγέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=524)

[Bible Bento](https://biblebento.com/dictionary/G0524.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/524/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/524.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g524)

[Sermon Index](https://www.sermonindex.net/modules/articles/index.php?view=article&aid=34579)