# ἐλευθερόω

[eleutheroō](https://www.blueletterbible.org/lexicon/g1659)

Definition: make free (6x), deliver (1x), set at liberty

Part of speech: verb

Occurs 12 times in 7 verses

Greek: [eleutheros](../g/g1658.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1659)

[Study Light](https://www.studylight.org/lexicons/greek/1659.html)

[Bible Hub](https://biblehub.com/str/greek/1659.htm)

[LSJ](https://lsj.gr/wiki/ἐλευθερόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1659)

[Bible Bento](https://biblebento.com/dictionary/G1659.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1659/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1659.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1659)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34112)