# σωρεύω

[sōreuō](https://www.blueletterbible.org/lexicon/g4987)

Definition: heap (1x), lade (1x), to overwhelm one with a heap of anything, to load one with the consciousness of many sins

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4987)

[Study Light](https://www.studylight.org/lexicons/greek/4987.html)

[Bible Hub](https://biblehub.com/str/greek/4987.htm)

[LSJ](https://lsj.gr/wiki/σωρεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4987)

[Bible Bento](https://biblebento.com/dictionary/G4987.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4987/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4987.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4987)
