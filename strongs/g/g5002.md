# τακτός

[taktos](https://www.blueletterbible.org/lexicon/g5002)

Definition: set (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5002)

[Study Light](https://www.studylight.org/lexicons/greek/5002.html)

[Bible Hub](https://biblehub.com/str/greek/5002.htm)

[LSJ](https://lsj.gr/wiki/τακτός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5002)

[Bible Bento](https://biblebento.com/dictionary/G5002.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5002/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5002.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5002)

