# ἀπόχρησις

[apochrēsis](https://www.blueletterbible.org/lexicon/g671)

Definition: using (1x), abuse, misuse

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0671)

[Study Light](https://www.studylight.org/lexicons/greek/671.html)

[Bible Hub](https://biblehub.com/str/greek/671.htm)

[LSJ](https://lsj.gr/wiki/ἀπόχρησις)

[NET Bible](http://classic.net.bible.org/strong.php?id=671)

[Bible Bento](https://biblebento.com/dictionary/G0671.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/671/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/671.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g671)
