# τροφή

[trophē](https://www.blueletterbible.org/lexicon/g5160)

Definition: meat (11x), food (2x), some meat (2x), nourishment

Part of speech: feminine noun

Occurs 16 times in 16 verses

Hebrew: [lechem](../h/h3899.md), ['ōḵel](../h/h400.md)

Derived words: [-trophy](https://en.wiktionary.org/wiki/Category:English_words_suffixed_with_-trophy)

Synonyms: [meat](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Meat)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5160)

[Study Light](https://www.studylight.org/lexicons/greek/5160.html)

[Bible Hub](https://biblehub.com/str/greek/5160.htm)

[LSJ](https://lsj.gr/wiki/τροφή)

[NET Bible](http://classic.net.bible.org/strong.php?id=5160)

[Bible Bento](https://biblebento.com/dictionary/G5160.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5160/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5160.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5160)