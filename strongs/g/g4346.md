# πρόσκλισις

[prosklisis](https://www.blueletterbible.org/lexicon/g4346)

Definition: partiality (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4346)

[Study Light](https://www.studylight.org/lexicons/greek/4346.html)

[Bible Hub](https://biblehub.com/str/greek/4346.htm)

[LSJ](https://lsj.gr/wiki/πρόσκλισις)

[NET Bible](http://classic.net.bible.org/strong.php?id=4346)

[Bible Bento](https://biblebento.com/dictionary/G4346.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4346/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4346.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4346)
