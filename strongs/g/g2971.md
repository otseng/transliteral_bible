# κώνωψ

[kōnōps](https://www.blueletterbible.org/lexicon/g2971)

Definition: gnat (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2971)

[Study Light](https://www.studylight.org/lexicons/greek/2971.html)

[Bible Hub](https://biblehub.com/str/greek/2971.htm)

[LSJ](https://lsj.gr/wiki/κώνωψ)

[NET Bible](http://classic.net.bible.org/strong.php?id=2971)

[Bible Bento](https://biblebento.com/dictionary/G2971.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2971/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2971.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2971)

