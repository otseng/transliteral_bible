# ὑψηλοφρονέω

[hypsēlophroneō](https://www.blueletterbible.org/lexicon/g5309)

Definition: be highminded (2x), proud, arrogant

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5309)

[Study Light](https://www.studylight.org/lexicons/greek/5309.html)

[Bible Hub](https://biblehub.com/str/greek/5309.htm)

[LSJ](https://lsj.gr/wiki/ὑψηλοφρονέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5309)

[Bible Bento](https://biblebento.com/dictionary/G5309.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5309/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5309.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5309)
