# φέρω

[pherō](https://www.blueletterbible.org/lexicon/g5342)

Definition: bring (34x), bear (8x), bring forth (5x), come (3x), reach (2x), endure (2x), carry (1x), bear, bring forth, come, reach, endure, carry

Part of speech: verb

Occurs 66 times in 58 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5342)

[Study Light](https://www.studylight.org/lexicons/greek/5342.html)

[Bible Hub](https://biblehub.com/str/greek/5342.htm)

[LSJ](https://lsj.gr/wiki/φέρω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5342)

[Bible Bento](https://biblebento.com/dictionary/G5342.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5342/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5342.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5342)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%86%CE%AD%CF%81%CF%89)