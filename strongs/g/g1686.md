# ἐμβάπτω

[embaptō](https://www.blueletterbible.org/lexicon/g1686)

Definition: dip (3x), wet by contact with fluid

Part of speech: verb

Occurs 3 times in 3 verses


## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1686)

[Study Light](https://www.studylight.org/lexicons/greek/1686.html)

[Bible Hub](https://biblehub.com/str/greek/1686.htm)

[LSJ](https://lsj.gr/wiki/ἐμβάπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1686)

[Bible Bento](https://biblebento.com/dictionary/G1686.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1686/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1686.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1686)