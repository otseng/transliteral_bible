# μένω

[menō](https://www.blueletterbible.org/lexicon/g3306)

Definition: abide (61x), remain (16x), dwell (15x), continue (11x), tarry (9x), endure (3x), stay at a friend's house, persevere, wait for under adversity, endure, "I remain"

Part of speech: verb

Occurs 127 times in 105 verses

Hebrew: [lûn](../h/h3885.md), [yāḥal](../h/h3176.md), [yashab](../h/h3427.md), [ḥāḵâ](../h/h2442.md)

Derived words: [checkmate](https://en.wikipedia.org/wiki/Checkmate)

Synonyms: [abide](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Abide), [dwell](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Dwell)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3306)

[Study Light](https://www.studylight.org/lexicons/greek/3306.html)

[Bible Hub](https://biblehub.com/str/greek/3306.htm)

[LSJ](https://lsj.gr/wiki/μένω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3306)

[Bible Bento](https://biblebento.com/dictionary/G3306.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3306/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3306.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3306)

[Blogos - Μένω: Abide](http://www.blogos.org/exploringtheword/GG-meno.php)

[Abide in Christ](http://www.abideinchrist.com/keys/abide.html)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=36494)

[Walk Worthy](http://www.make-my-christian-life-work.com/abide.html)
