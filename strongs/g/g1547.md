# ἐκγαμίζω

[ekgamizō](https://www.blueletterbible.org/lexicon/g1547)

Definition: give in marriage (5x), to marry off a daughter

Part of speech: verb

Occurs 6 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1547)

[Study Light](https://www.studylight.org/lexicons/greek/1547.html)

[Bible Hub](https://biblehub.com/str/greek/1547.htm)

[LSJ](https://lsj.gr/wiki/ἐκγαμίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1547)

[Bible Bento](https://biblebento.com/dictionary/G1547.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1547/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1547.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1547)