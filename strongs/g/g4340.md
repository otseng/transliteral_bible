# πρόσκαιρος

[proskairos](https://www.blueletterbible.org/lexicon/g4340)

Definition: for a while (1x), for a time (1x), temporal (1x), for a season (1x).

Part of speech: adjective

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4340)

[Study Light](https://www.studylight.org/lexicons/greek/4340.html)

[Bible Hub](https://biblehub.com/str/greek/4340.htm)

[LSJ](https://lsj.gr/wiki/πρόσκαιρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4340)

[Bible Bento](https://biblebento.com/dictionary/G4340.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4340/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4340.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4340)
