# ἐπισκοπέω

[episkopeō](https://www.blueletterbible.org/lexicon/g1983)

Definition: look diligently (1x), take the oversight (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1983)

[Study Light](https://www.studylight.org/lexicons/greek/1983.html)

[Bible Hub](https://biblehub.com/str/greek/1983.htm)

[LSJ](https://lsj.gr/wiki/ἐπισκοπέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1983)

[Bible Bento](https://biblebento.com/dictionary/G1983.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1983/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1983.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1983)
