# παραθεωρέω

[paratheōreō](https://www.blueletterbible.org/lexicon/g3865)

Definition: neglect (1x), overlook

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3865)

[Study Light](https://www.studylight.org/lexicons/greek/3865.html)

[Bible Hub](https://biblehub.com/str/greek/3865.htm)

[LSJ](https://lsj.gr/wiki/παραθεωρέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3865)

[Bible Bento](https://biblebento.com/dictionary/G3865.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3865/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3865.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3865)
