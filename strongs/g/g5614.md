# ὡσαννά

[hōsanna](https://www.blueletterbible.org/lexicon/g5614)

Definition: Hosanna (6x), "oh save", "save please"

Part of speech: interjection

Hebrew: [yasha'](../h/h3467.md) [na'](../h/h4994.md)

Occurs 6 times in 5 verses

Derived words: Hosanna

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5614)

[Study Light](https://www.studylight.org/lexicons/greek/5614.html)

[Bible Hub](https://biblehub.com/str/greek/5614.htm)

[LSJ](https://lsj.gr/wiki/ὡσαννά)

[NET Bible](http://classic.net.bible.org/strong.php?id=5614)

[Bible Bento](https://biblebento.com/dictionary/G5614.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5614/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5614.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5614)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%89%CF%83%CE%B1%CE%BD%CE%BD%CE%AC)

[Precept Austin](https://www.preceptaustin.org/hosanna-save_now)

[Wikipedia](https://en.wikipedia.org/wiki/Hosanna)

[Desiring God](https://www.desiringgod.org/messages/hosanna)

[Crosswalk](https://www.crosswalk.com/faith/bible-study/what-does-the-word-hosanna-mean.html)