# ἀλλότριος

[allotrios](https://www.blueletterbible.org/lexicon/g245)

Definition: stranger (4x), another man's (4x), strange (2x), other men's (2x), other (1x), alien (1x), hostile, foreign

Part of speech: adjective

Occurs 14 times in 13 verses

Hebrew: [nēḵār](../h/h5236.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0245)

[Study Light](https://www.studylight.org/lexicons/greek/245.html)

[Bible Hub](https://biblehub.com/str/greek/245.htm)

[LSJ](https://lsj.gr/wiki/ἀλλότριος)

[NET Bible](http://classic.net.bible.org/strong.php?id=245)

[Bible Bento](https://biblebento.com/dictionary/G0245.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0245/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0245.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g245)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CE%BB%CE%BB%CF%8C%CF%84%CF%81%CE%B9%CE%BF%CF%82)