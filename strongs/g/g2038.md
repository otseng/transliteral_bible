# ἐργάζομαι

[ergazomai](https://www.blueletterbible.org/lexicon/g2038)

Definition: work (22x), wrought (7x), do (3x), minister about (1x), forbear working (with G3361) (1x), labour for (1x), labour (1x), commit (1x), trade by (1x), trade (1x), do business, exercise, toil

Part of speech: verb

Occurs 39 times in 37 verses

Greek: [ergon](../g/g2041.md), [katergazomai](../g/g2716.md)

Hebrew: ['abad](../h/h5647.md), ['asah](../h/h6213.md), [mĕla'kah](../h/h4399.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2038)

[Study Light](https://www.studylight.org/lexicons/greek/2038.html)

[Bible Hub](https://biblehub.com/str/greek/2038.htm)

[LSJ](https://lsj.gr/wiki/ἐργάζομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=2038)

[Bible Bento](https://biblebento.com/dictionary/G2038.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2038/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2038.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2038)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%90%CF%81%CE%B3%CE%AC%CE%B6%CE%BF%CE%BC%CE%B1%CE%B9)

[NT Words](http://ntwords.com/Works.htm)
