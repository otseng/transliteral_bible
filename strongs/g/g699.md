# ἀρεσκεία

[areskeia](https://www.blueletterbible.org/lexicon/g699)

Definition: pleasing (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0699)

[Study Light](https://www.studylight.org/lexicons/greek/699.html)

[Bible Hub](https://biblehub.com/str/greek/699.htm)

[LSJ](https://lsj.gr/wiki/ἀρεσκεία)

[NET Bible](http://classic.net.bible.org/strong.php?id=699)

[Bible Bento](https://biblebento.com/dictionary/G0699.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/699/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/699.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g699)

[Sermon Index](https://www.sermonindex.net/modules/articles/index.php?view=article&aid=34611)
