# ἐπιδύω

[epidyō](https://www.blueletterbible.org/lexicon/g1931)

Definition: go down (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1931)

[Study Light](https://www.studylight.org/lexicons/greek/1931.html)

[Bible Hub](https://biblehub.com/str/greek/1931.htm)

[LSJ](https://lsj.gr/wiki/ἐπιδύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1931)

[Bible Bento](https://biblebento.com/dictionary/G1931.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1931/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1931.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1931)

