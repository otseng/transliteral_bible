# μεταβαίνω

[metabainō](https://www.blueletterbible.org/lexicon/g3327)

Definition: depart (7x), remove (2x), pass (2x), go (1x), remove, pass

Part of speech: verb

Occurs 12 times in 11 verses

Derived words: [metabasis](https://en.wiktionary.org/wiki/metabasis), metabolism

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3327)

[Study Light](https://www.studylight.org/lexicons/greek/3327.html)

[Bible Hub](https://biblehub.com/str/greek/3327.htm)

[LSJ](https://lsj.gr/wiki/μεταβαίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3327)

[Bible Bento](https://biblebento.com/dictionary/G3327.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3327/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3327.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3327)