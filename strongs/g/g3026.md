# λῆρος

[lēros](https://www.blueletterbible.org/lexicon/g3026)

Definition: idle tales (1x), idle talk, nonsense

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3026)

[Study Light](https://www.studylight.org/lexicons/greek/3026.html)

[Bible Hub](https://biblehub.com/str/greek/3026.htm)

[LSJ](https://lsj.gr/wiki/λῆρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3026)

[Bible Bento](https://biblebento.com/dictionary/G3026.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3026/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3026.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3026)
