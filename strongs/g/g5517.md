# χοϊκός

[choikos](https://www.blueletterbible.org/lexicon/g5517)

Definition: earthly (4x), dusty or dirty, made of earth

Part of speech: adjective

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5517)

[Study Light](https://www.studylight.org/lexicons/greek/5517.html)

[Bible Hub](https://biblehub.com/str/greek/5517.htm)

[LSJ](https://lsj.gr/wiki/χοϊκός)

[NET Bible](http://classic.net.bible.org/strong.php?id=5517)

[Bible Bento](https://biblebento.com/dictionary/G5517.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5517/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5517.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5517)
