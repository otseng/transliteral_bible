# ἀσέβεια

[asebeia](https://www.blueletterbible.org/lexicon/g763)

Definition: ungodliness (4x), ungodly (2x), want of reverence towards God, impiety, wickedness

Part of speech: feminine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0763)

[Study Light](https://www.studylight.org/lexicons/greek/763.html)

[Bible Hub](https://biblehub.com/str/greek/763.htm)

[LSJ](https://lsj.gr/wiki/ἀσέβεια)

[NET Bible](http://classic.net.bible.org/strong.php?id=763)

[Bible Bento](https://biblebento.com/dictionary/G0763.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0763/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0763.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g763)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CF%83%CE%AD%CE%B2%CE%B5%CE%B9%CE%B1)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=36013)