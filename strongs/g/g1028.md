# βροχή

[brochē](https://www.blueletterbible.org/lexicon/g1028)

Definition: rain (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

Hebrew: [gešem](../h/h1653.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1028)

[Study Light](https://www.studylight.org/lexicons/greek/1028.html)

[Bible Hub](https://biblehub.com/str/greek/1028.htm)

[LSJ](https://lsj.gr/wiki/βροχή)

[NET Bible](http://classic.net.bible.org/strong.php?id=1028)

[Bible Bento](https://biblebento.com/dictionary/G1028.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1028/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1028.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1028)

