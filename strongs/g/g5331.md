# φαρμακεία

[pharmakeia](https://www.blueletterbible.org/lexicon/g5331)

Definition: sorcery (2x), witchcraft (1x), the use or the administering of drugs, magic, medication

Part of speech: feminine noun

Occurs 3 times in 3 verses

Derived words: pharmacy

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5331)

[Study Light](https://www.studylight.org/lexicons/greek/5331.html)

[Bible Hub](https://biblehub.com/str/greek/5331.htm)

[LSJ](https://lsj.gr/wiki/φαρμακεία)

[NET Bible](http://classic.net.bible.org/strong.php?id=5331)

[Bible Bento](https://biblebento.com/dictionary/G5331.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5331/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5331.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5331)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%86%CE%B1%CF%81%CE%BC%CE%B1%CE%BA%CE%B5%CE%AF%CE%B1)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=35832)

[Precept Austin](https://www.preceptaustin.org/galatians_519-20#sorcery)