# κατακόπτω

[katakoptō](https://www.blueletterbible.org/lexicon/g2629)

Definition: cut (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [kāṯaṯ](../h/h3807.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2629)

[Study Light](https://www.studylight.org/lexicons/greek/2629.html)

[Bible Hub](https://biblehub.com/str/greek/2629.htm)

[LSJ](https://lsj.gr/wiki/κατακόπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2629)

[Bible Bento](https://biblebento.com/dictionary/G2629.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2629/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2629.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2629)

