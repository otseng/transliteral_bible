# μεθερμηνεύω

[methermēneuō](https://www.blueletterbible.org/lexicon/g3177)

Definition: being interpreted (6x), be by interpretation (1x), translate, interpret, to translate into the language of one with whom I wish to communicate

Part of speech: verb

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3177)

[Study Light](https://www.studylight.org/lexicons/greek/3177.html)

[Bible Hub](https://biblehub.com/str/greek/3177.htm)

[LSJ](https://lsj.gr/wiki/μεθερμηνεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3177)

[Bible Bento](https://biblebento.com/dictionary/G3177.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3177/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3177.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3177)