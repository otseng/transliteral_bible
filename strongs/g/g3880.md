# παραλαμβάνω

[paralambanō](https://www.blueletterbible.org/lexicon/g3880)

Definition: take (30x), receive (15x), take unto (2x), take up (2x), take away (1x), to join to oneself

Part of speech: verb

Occurs 54 times in 50 verses

Synonyms: [take](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Take)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3880)

[Study Light](https://www.studylight.org/lexicons/greek/3880.html)

[Bible Hub](https://biblehub.com/str/greek/3880.htm)

[LSJ](https://lsj.gr/wiki/παραλαμβάνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3880)

[Bible Bento](https://biblebento.com/dictionary/G3880.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3880/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3880.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3880)