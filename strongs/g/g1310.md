# διαφημίζω

[diaphēmizō](https://www.blueletterbible.org/lexicon/g1310)

Definition: spread abroad (one's) fame (1x), be commonly reported (1x), blaze abroad (1x), advertise

Part of speech: verb

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1310)

[Study Light](https://www.studylight.org/lexicons/greek/1310.html)

[Bible Hub](https://biblehub.com/str/greek/1310.htm)

[LSJ](https://lsj.gr/wiki/διαφημίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1310)

[Bible Bento](https://biblebento.com/dictionary/G1310.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1310/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1310.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1310)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B4%CE%B9%CE%B1%CF%86%CE%B7%CE%BC%CE%AF%CE%B6%CF%89)