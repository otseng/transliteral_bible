# κύριος

[kyrios](https://www.blueletterbible.org/lexicon/g2962)

Definition: Lord (667x), lord (54x), master (11x), sir (6x), Sir (6x), miscellaneous (4x), supreme authority, head of household, mister, owner

Part of speech: masculine noun

Occurs 748 times in 687 verses

Hebrew: ['adonay](../h/h136.md), ['adown](../h/h113.md), [śar](../h/h8269.md)

Synonyms: [lord](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Lord), [master](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Master)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2962)

[Study Light](https://www.studylight.org/lexicons/greek/2962.html)

[Bible Hub](https://biblehub.com/str/greek/2962.htm)

[LSJ](https://lsj.gr/wiki/κύριος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2962)

[Bible Bento](https://biblebento.com/dictionary/G2962.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2962/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2962.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2962)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CF%8D%CF%81%CE%B9%CE%BF%CF%82)

[Wikipedia - Kyrios](https://en.wikipedia.org/wiki/Kyrios)

[NT Greek Word Studies](http://ntgreek-wordstudies.com/kurios.php)