# πλείων

[pleiōn](https://www.blueletterbible.org/lexicon/g4119)

Definition: more (23x), many (12x), greater (5x), further (with G1909) (3x), most (2x), more part (2x)

Part of speech: adjective

Occurs 57 times in 55 verses

Greek: [polys](../g/g4183.md)

Hebrew: [rōḇ](../h/h7230.md), [śāḇāʿ](../h/h7647.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4119)

[Study Light](https://www.studylight.org/lexicons/greek/4119.html)

[Bible Hub](https://biblehub.com/str/greek/4119.htm)

[LSJ](https://lsj.gr/wiki/πλείων)

[NET Bible](http://classic.net.bible.org/strong.php?id=4119)

[Bible Bento](https://biblebento.com/dictionary/G4119.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4119/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4119.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4119)