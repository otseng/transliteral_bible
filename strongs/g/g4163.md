# ποιητής

[poiētēs](https://www.blueletterbible.org/lexicon/g4163)

Definition: doer (5x), poet (1x), a maker, a producer, author, performer

Part of speech: masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4163)

[Study Light](https://www.studylight.org/lexicons/greek/4163.html)

[Bible Hub](https://biblehub.com/str/greek/4163.htm)

[LSJ](https://lsj.gr/wiki/ποιητής)

[NET Bible](http://classic.net.bible.org/strong.php?id=4163)

[Bible Bento](https://biblebento.com/dictionary/G4163.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4163/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4163.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4163)
