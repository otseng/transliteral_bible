# διαβεβαιόομαι

[diabebaioomai](https://www.blueletterbible.org/lexicon/g1226)

Definition: affirm (1x), affirm constantly (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1226)

[Study Light](https://www.studylight.org/lexicons/greek/1226.html)

[Bible Hub](https://biblehub.com/str/greek/1226.htm)

[LSJ](https://lsj.gr/wiki/διαβεβαιόομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1226)

[Bible Bento](https://biblebento.com/dictionary/G1226.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1226/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1226.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1226)
