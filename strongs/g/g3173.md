# μέγας

[megas](https://www.blueletterbible.org/lexicon/g3173)

Definition: great (150x), loud (33x), great one

Part of speech: adjective

Occurs 195 times in 185 verses

Hebrew: [gadowl](../h/h1419.md)

Derived words: megaphone, megachurch, megabyte, [mega-](https://en.wiktionary.org/wiki/mega-)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3173)

[Study Light](https://www.studylight.org/lexicons/greek/3173.html)

[Bible Hub](https://biblehub.com/str/greek/3173.htm)

[LSJ](https://lsj.gr/wiki/μέγας)

[NET Bible](http://classic.net.bible.org/strong.php?id=3173)

[Bible Bento](https://biblebento.com/dictionary/G3173.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3173/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3173.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3173)