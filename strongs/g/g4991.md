# σωτηρία

[sōtēria](https://www.blueletterbible.org/lexicon/g4991)

Definition: salvation (40x), the (one) be saved (1x), deliver (with G1325) (1x), health (1x), saving (1x), that (one) be saved (with G1519) (1x), deliverance, preservation, safety

Part of speech: feminine noun

Occurs 45 times in 43 verses

Greek: [sōtēr](../g/g4990.md), [sōzō](../g/g4982.md)

Hebrew: [yĕshuw`ah](../h/h3444.md), [yesha`](../h/h3468.md), [pᵊlêṭâ](../h/h6413.md)

Derived words: soteriology, soteriological

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4991)

[Study Light](https://www.studylight.org/lexicons/greek/4991.html)

[Bible Hub](https://biblehub.com/str/greek/4991.htm)

[LSJ](https://lsj.gr/wiki/σωτηρία)

[NET Bible](http://classic.net.bible.org/strong.php?id=4991)

[Bible Bento](https://biblebento.com/dictionary/G4991.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4991/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4991.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4991)

[Wikipedia](https://en.wikipedia.org/wiki/Soteria_%28mythology%29)

[Precept Austin](https://www.preceptaustin.org/salvation-soteria_greek_word_study)

[John Uebersax](http://www.john-uebersax.com/plato/words/soteria.htm)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/4991-soteria-salvation.htm)
