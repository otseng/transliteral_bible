# σύμβουλος

[symboulos](https://www.blueletterbible.org/lexicon/g4825)

Definition: counsellor (1x), adviser

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4825)

[Study Light](https://www.studylight.org/lexicons/greek/4825.html)

[Bible Hub](https://biblehub.com/str/greek/4825.htm)

[LSJ](https://lsj.gr/wiki/σύμβουλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4825)

[Bible Bento](https://biblebento.com/dictionary/G4825.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4825/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4825.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4825)
