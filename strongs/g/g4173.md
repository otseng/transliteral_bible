# πολιτάρχης

[politarchēs](https://www.blueletterbible.org/lexicon/g4173)

Definition: ruler of the city (2x), a town-officer

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4173)

[Study Light](https://www.studylight.org/lexicons/greek/4173.html)

[Bible Hub](https://biblehub.com/str/greek/4173.htm)

[LSJ](https://lsj.gr/wiki/πολιτάρχης)

[NET Bible](http://classic.net.bible.org/strong.php?id=4173)

[Bible Bento](https://biblebento.com/dictionary/G4173.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4173/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4173.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4173)
