# ἐξορκιστής

[exorkistēs](https://www.blueletterbible.org/lexicon/g1845)

Definition: exorcist (1x)

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1845)

[Study Light](https://www.studylight.org/lexicons/greek/1845.html)

[Bible Hub](https://biblehub.com/str/greek/1845.htm)

[LSJ](https://lsj.gr/wiki/ἐξορκιστής)

[NET Bible](http://classic.net.bible.org/strong.php?id=1845)

[Bible Bento](https://biblebento.com/dictionary/G1845.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1845/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1845.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1845)
