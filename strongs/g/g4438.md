# πυκτεύω

[pykteuō](https://www.blueletterbible.org/lexicon/g4438)

Definition: fight (1x), to box

Part of speech: verb

Occurs 1 times in 1 verses

Synonyms: [fight](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Fight)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4438)

[Study Light](https://www.studylight.org/lexicons/greek/4438.html)

[Bible Hub](https://biblehub.com/str/greek/4438.htm)

[LSJ](https://lsj.gr/wiki/πυκτεύω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4438)

[Bible Bento](https://biblebento.com/dictionary/G4438.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4438/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4438.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4438)
