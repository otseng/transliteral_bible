# φιλάργυρος

[philargyros](https://www.blueletterbible.org/lexicon/g5366)

Definition: covetous (2x), loving money

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5366)

[Study Light](https://www.studylight.org/lexicons/greek/5366.html)

[Bible Hub](https://biblehub.com/str/greek/5366.htm)

[LSJ](https://lsj.gr/wiki/φιλάργυρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5366)

[Bible Bento](https://biblebento.com/dictionary/G5366.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5366/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5366.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5366)