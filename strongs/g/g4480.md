# ῥέδη

[rhedē](https://www.blueletterbible.org/lexicon/g4480)

Definition: chariot (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4480)

[Study Light](https://www.studylight.org/lexicons/greek/4480.html)

[Bible Hub](https://biblehub.com/str/greek/4480.htm)

[LSJ](https://lsj.gr/wiki/ῥέδη)

[NET Bible](http://classic.net.bible.org/strong.php?id=4480)

[Bible Bento](https://biblebento.com/dictionary/G4480.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4480/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4480.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4480)

