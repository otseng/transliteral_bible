# ἀναλαμβάνω

[analambanō](https://www.blueletterbible.org/lexicon/g353)

Definition: take up (4x), receive up (3x), take (3x), take in (2x), take into (1x)

Part of speech: verb

Occurs 14 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0353)

[Study Light](https://www.studylight.org/lexicons/greek/353.html)

[Bible Hub](https://biblehub.com/str/greek/353.htm)

[LSJ](https://lsj.gr/wiki/ἀναλαμβάνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=353)

[Bible Bento](https://biblebento.com/dictionary/G0353.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0353/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0353.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g353)
