# τοσοῦτος

[tosoutos](https://www.blueletterbible.org/lexicon/g5118)

Definition: so much (7x), so great (5x), so many (4x), so long (2x), as large (1x), these many (1x), so many things (1x).

Part of speech: adjective

Occurs 21 times in 19 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5118)

[Study Light](https://www.studylight.org/lexicons/greek/5118.html)

[Bible Hub](https://biblehub.com/str/greek/5118.htm)

[LSJ](https://lsj.gr/wiki/τοσοῦτος)

[NET Bible](http://classic.net.bible.org/strong.php?id=5118)

[Bible Bento](https://biblebento.com/dictionary/G5118.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5118/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5118.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5118)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%84%CE%BF%CF%83%CE%BF%E1%BF%A6%CF%84%CE%BF%CF%82)