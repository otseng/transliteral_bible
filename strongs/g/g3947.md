# παροξύνω

[paroxynō](https://www.blueletterbible.org/lexicon/g3947)

Definition: stir (1x), easily provoked (1x), to make sharp, sharpen, to stimulate, spur on, urge

Part of speech: verb

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3947)

[Study Light](https://www.studylight.org/lexicons/greek/3947.html)

[Bible Hub](https://biblehub.com/str/greek/3947.htm)

[LSJ](https://lsj.gr/wiki/παροξύνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3947)

[Bible Bento](https://biblebento.com/dictionary/G3947.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3947/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3947.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3947)
