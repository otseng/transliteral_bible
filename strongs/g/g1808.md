# ἐξαίρω

[exairō](https://www.blueletterbible.org/lexicon/g1808)

Definition: take away (1x), put away (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1808)

[Study Light](https://www.studylight.org/lexicons/greek/1808.html)

[Bible Hub](https://biblehub.com/str/greek/1808.htm)

[LSJ](https://lsj.gr/wiki/ἐξαίρω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1808)

[Bible Bento](https://biblebento.com/dictionary/G1808.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1808/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1808.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1808)
