# ταραχή

[tarachē](https://www.blueletterbible.org/lexicon/g5016)

Definition: trouble (1x), troubling (1x), sedition, disturbance

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5016)

[Study Light](https://www.studylight.org/lexicons/greek/5016.html)

[Bible Hub](https://biblehub.com/str/greek/5016.htm)

[LSJ](https://lsj.gr/wiki/ταραχή)

[NET Bible](http://classic.net.bible.org/strong.php?id=5016)

[Bible Bento](https://biblebento.com/dictionary/G5016.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5016/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5016.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5016)