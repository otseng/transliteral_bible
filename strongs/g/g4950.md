# Σύρτις

[Syrtis](https://www.blueletterbible.org/lexicon/g4950)

Definition: quicksands (1x), the name of two places in the African or Libyan Sea between Carthage and Cyrenaicia, full of shallows and sandbanks, and therefore destructive to ships, the Syrtis Major or great bay on the north coast of Africa

Part of speech: proper feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4950)

[Study Light](https://www.studylight.org/lexicons/greek/4950.html)

[Bible Hub](https://biblehub.com/str/greek/4950.htm)

[LSJ](https://lsj.gr/wiki/Σύρτις)

[NET Bible](http://classic.net.bible.org/strong.php?id=4950)

[Bible Bento](https://biblebento.com/dictionary/G4950.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4950/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4950.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4950)

[Wikipedia](https://en.wikipedia.org/wiki/Syrtis)
