# vτέκνον

[teknon](https://www.blueletterbible.org/lexicon/g5043)

Definition: child (77x), son (21x), daughter (1x), offspring, children

Part of speech: neuter noun

Occurs 99 times in 91 verses

Greek: [huios](../g/g5207.md), [paidion](../g/g3813.md)

Hebrew: [ben](../h/h1121.md), [bar](../h/h1248.md), [yalad](../h/h3205.md), [bath](../h/h1323.md), [ṭap̄](../h/h2945.md)

Synonyms: [child](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Child)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5043)

[Study Light](https://www.studylight.org/lexicons/greek/5043.html)

[Bible Hub](https://biblehub.com/str/greek/5043.htm)

[LSJ](https://lsj.gr/wiki/vτέκνον)

[NET Bible](http://classic.net.bible.org/strong.php?id=5043)

[Bible Bento](https://biblebento.com/dictionary/G5043.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5043/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5043.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5043)

[TEKNON vs HUIOS? what are you? which one do you want to become?](https://heartofthekingyouthministry.wordpress.com/2011/12/13/teknon-vs-huios-what-are-you-which-one-do-you-want-to-become/)