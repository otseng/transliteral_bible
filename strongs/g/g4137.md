# πληρόω

[plēroō](https://www.blueletterbible.org/lexicon/g4137)

Definition: fulfil (51x), fill (19x), be full (7x), complete (2x), end (2x)

Part of speech: verb

Occurs 95 times in 90 verses

Hebrew: [mālā'](../h/h4390.md)

Derived words: plethora

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4137)

[Study Light](https://www.studylight.org/lexicons/greek/4137.html)

[Bible Hub](https://biblehub.com/str/greek/4137.htm)

[LSJ](https://lsj.gr/wiki/πληρόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4137)

[Bible Bento](https://biblebento.com/dictionary/G4137.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4137/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4137.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4137)

[Wiktionary](https://en.wiktionary.org/wiki/πλήθω)