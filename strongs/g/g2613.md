# καταδικάζω

[katadikazō](https://www.blueletterbible.org/lexicon/g2613)

Definition: condemn, to give judgment against (one), to pronounce guilty

Part of speech: verb

Occurs 6 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2613)

[Study Light](https://www.studylight.org/lexicons/greek/2613.html)

[Bible Hub](https://biblehub.com/str/greek/2613.htm)

[LSJ](https://lsj.gr/wiki/καταδικάζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2613)

[Bible Bento](https://biblebento.com/dictionary/G2613.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2613/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2613.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2613)

[Precept Austin](https://www.preceptaustin.org/luke-6-commentary#condemn)

[In Search of Truth](http://www.insearchoftruth.org/articles/word_study_judge.html)