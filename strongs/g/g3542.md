# νομή

[nomē](https://www.blueletterbible.org/lexicon/g3542)

Definition: pasture (1x), eat (with G2192) (1x), food, growth, increase

Part of speech: feminine noun

Occurs 2 times in 2 verses

Hebrew: [mirʿê](../h/h4829.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3542)

[Study Light](https://www.studylight.org/lexicons/greek/3542.html)

[Bible Hub](https://biblehub.com/str/greek/3542.htm)

[LSJ](https://lsj.gr/wiki/νομή)

[NET Bible](http://classic.net.bible.org/strong.php?id=3542)

[Bible Bento](https://biblebento.com/dictionary/G3542.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3542/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3542.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3542)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BD%CE%BF%CE%BC%CE%AE)