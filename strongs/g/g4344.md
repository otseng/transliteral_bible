# προσκεφάλαιον

[proskephalaion](https://www.blueletterbible.org/lexicon/g4344)

Definition: pillow (1x).

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4344)

[Study Light](https://www.studylight.org/lexicons/greek/4344.html)

[Bible Hub](https://biblehub.com/str/greek/4344.htm)

[LSJ](https://lsj.gr/wiki/προσκεφάλαιον)

[NET Bible](http://classic.net.bible.org/strong.php?id=4344)

[Bible Bento](https://biblebento.com/dictionary/G4344.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4344/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4344.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4344)

