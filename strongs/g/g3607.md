# ὀθόνη

[othonē](https://www.blueletterbible.org/lexicon/g3607)

Definition: sheet (2x)

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3607)

[Study Light](https://www.studylight.org/lexicons/greek/3607.html)

[Bible Hub](https://biblehub.com/str/greek/3607.htm)

[LSJ](https://lsj.gr/wiki/ὀθόνη)

[NET Bible](http://classic.net.bible.org/strong.php?id=3607)

[Bible Bento](https://biblebento.com/dictionary/G3607.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3607/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3607.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3607)
