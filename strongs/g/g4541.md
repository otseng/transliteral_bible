# Σαμαρίτης

[Samaritēs](https://www.blueletterbible.org/lexicon/g4541)

Definition: Samaritans (9x)

Part of speech: proper masculine noun

Occurs 9 times in 9 verses

Hebrew: [Šōmrōnî](../h/h8118.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4541)

[Study Light](https://www.studylight.org/lexicons/greek/4541.html)

[Bible Hub](https://biblehub.com/str/greek/4541.htm)

[LSJ](https://lsj.gr/wiki/Σαμαρίτης)

[NET Bible](http://classic.net.bible.org/strong.php?id=4541)

[Bible Bento](https://biblebento.com/dictionary/G4541.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4541/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4541.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4541)

[Wiktionary](https://en.wiktionary.org/wiki/Samaritan)

[Wikipedia](https://en.wikipedia.org/wiki/Samaritans)

[New World Encyclopedia](http://www.newworldencyclopedia.org/entry/Samaritan)

[Bible History](https://www.bible-history.com/Samaritans/SAMARITANSBrief_History.htm)

[Bible.ca](https://www.bible.ca/archeology/bible-archeology-samaritans.htm)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/samaritans.html)