# χρονίζω

[chronizō](https://www.blueletterbible.org/lexicon/g5549)

Definition: delay (2x), tarry (2x), tarry so long (1x), linger, spend time

Part of speech: verb

Occurs 6 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5549)

[Study Light](https://www.studylight.org/lexicons/greek/5549.html)

[Bible Hub](https://biblehub.com/str/greek/5549.htm)

[LSJ](https://lsj.gr/wiki/χρονίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5549)

[Bible Bento](https://biblebento.com/dictionary/G5549.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5549/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5549.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5549)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%87%CF%81%CE%BF%CE%BD%CE%AF%CE%B6%CF%89)