# πρός

[pros](https://www.blueletterbible.org/lexicon/g4314)

Definition: unto (340x), to (203x), with (43x), for (25x), against (24x), among (20x), at (11x), not translated (6x), miscellaneous (53x), variations of 'to' (1x), towards

Part of speech: preposition

Occurs 711 times in 660 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4314)

[Study Light](https://www.studylight.org/lexicons/greek/4314.html)

[Bible Hub](https://biblehub.com/str/greek/4314.htm)

[LSJ](https://lsj.gr/wiki/πρός)

[NET Bible](http://classic.net.bible.org/strong.php?id=4314)

[Bible Bento](https://biblebento.com/dictionary/G4314.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4314/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4314.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4314)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CF%81%CF%8C%CF%82)
