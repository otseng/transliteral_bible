# ἐπικουρία

[epikouria](https://www.blueletterbible.org/lexicon/g1947)

Definition: help (1x), aid, succour

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1947)

[Study Light](https://www.studylight.org/lexicons/greek/1947.html)

[Bible Hub](https://biblehub.com/str/greek/1947.htm)

[LSJ](https://lsj.gr/wiki/ἐπικουρία)

[NET Bible](http://classic.net.bible.org/strong.php?id=1947)

[Bible Bento](https://biblebento.com/dictionary/G1947.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1947/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1947.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1947)
