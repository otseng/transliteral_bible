# κλίβανος

[klibanos](https://www.blueletterbible.org/lexicon/g2823)

Definition: oven (2x), earthen pot used for baking in

Part of speech: masculine noun

Occurs 2 times in 2 verses

Hebrew: [tannûr](../h/h8574.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2823)

[Study Light](https://www.studylight.org/lexicons/greek/2823.html)

[Bible Hub](https://biblehub.com/str/greek/2823.htm)

[LSJ](https://lsj.gr/wiki/κλίβανος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2823)

[Bible Bento](https://biblebento.com/dictionary/G2823.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2823/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2823.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2823)