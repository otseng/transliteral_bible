# ἐγκατοικέω

[egkatoikeō](https://www.blueletterbible.org/lexicon/g1460)

Definition: dwell among (1x), reside

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1460)

[Study Light](https://www.studylight.org/lexicons/greek/1460.html)

[Bible Hub](https://biblehub.com/str/greek/1460.htm)

[LSJ](https://lsj.gr/wiki/ἐγκατοικέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=1460)

[Bible Bento](https://biblebento.com/dictionary/G1460.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1460/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1460.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1460)

