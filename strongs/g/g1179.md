# Δεκάπολις

[Dekapolis](https://www.blueletterbible.org/lexicon/g1179)

Definition: Decapolis, "ten cities", according to Pliny, these cities were: Damascus, Opoton, Philadelphia, Raphana, Scythopolis, Gadara, Hippondion, Pella, Galasa, and Canatha (Gill)

Part of speech: proper locative noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1179)

[Study Light](https://www.studylight.org/lexicons/greek/1179.html)

[Bible Hub](https://biblehub.com/str/greek/1179.htm)

[LSJ](https://lsj.gr/wiki/Δεκάπολις)

[NET Bible](http://classic.net.bible.org/strong.php?id=1179)

[Bible Bento](https://biblebento.com/dictionary/G1179.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1179/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1179.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1179)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%94%CE%B5%CE%BA%CE%AC%CF%80%CE%BF%CE%BB%CE%B9%CF%82)

[Wikipedia](https://en.wikipedia.org/wiki/Decapolis)

[Video Bible](https://www.videobible.com/bible-dictionary/decapolis,)