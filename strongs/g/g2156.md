# εὐσχημόνως

[euschēmonōs](https://www.blueletterbible.org/lexicon/g2156)

Definition: honestly (2x), decently (1x).

Part of speech: adverb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2156)

[Study Light](https://www.studylight.org/lexicons/greek/2156.html)

[Bible Hub](https://biblehub.com/str/greek/2156.htm)

[LSJ](https://lsj.gr/wiki/εὐσχημόνως)

[NET Bible](http://classic.net.bible.org/strong.php?id=2156)

[Bible Bento](https://biblebento.com/dictionary/G2156.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2156/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2156.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2156)
