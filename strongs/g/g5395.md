# φλόξ

[phlox](https://www.blueletterbible.org/lexicon/g5395)

Definition: flame (6x), flaming (1x), blaze

Part of speech: feminine noun

Occurs 7 times in 7 verses

Hebrew: [šalheḇeṯ](../h/h7957.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5395)

[Study Light](https://www.studylight.org/lexicons/greek/5395.html)

[Bible Hub](https://biblehub.com/str/greek/5395.htm)

[LSJ](https://lsj.gr/wiki/φλόξ)

[NET Bible](http://classic.net.bible.org/strong.php?id=5395)

[Bible Bento](https://biblebento.com/dictionary/G5395.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5395/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5395.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5395)