# παρακοή

[parakoē](https://www.blueletterbible.org/lexicon/g3876)

Definition: disobedient (3x), inattention, disbelieved, a hearing amiss

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3876)

[Study Light](https://www.studylight.org/lexicons/greek/3876.html)

[Bible Hub](https://biblehub.com/str/greek/3876.htm)

[LSJ](https://lsj.gr/wiki/παρακοή)

[NET Bible](http://classic.net.bible.org/strong.php?id=3876)

[Bible Bento](https://biblebento.com/dictionary/G3876.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3876/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3876.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3876)

[Saints and Shepherds](https://ofsaintsandshepherds.com/word-studies/authority-words/disobedience-2/parakoe/)
