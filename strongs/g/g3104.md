# Μεννά

[Menna](https://www.blueletterbible.org/lexicon/g3104)

Definition: Menan (1x), "soothsayer: enchanted", one of the ancestors of Joseph in the genealogy of Jesus Christ

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/3104.html)

[LSJ](https://lsj.gr/wiki/Μεννά)

[NET Bible](http://classic.net.bible.org/strong.php?id=3104)

[Bible Bento](https://biblebento.com/dictionary/G3104.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3104/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3104.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3104)

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3104)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/menan.html)