# κάλυμμα

[kalymma](https://www.blueletterbible.org/lexicon/g2571)

Definition: vail (4x), veil, covering

Part of speech: neuter noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2571)

[Study Light](https://www.studylight.org/lexicons/greek/2571.html)

[Bible Hub](https://biblehub.com/str/greek/2571.htm)

[LSJ](https://lsj.gr/wiki/κάλυμμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=2571)

[Bible Bento](https://biblebento.com/dictionary/G2571.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2571/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2571.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2571)
