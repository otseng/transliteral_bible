# ἐμμαίνομαι

[emmainomai](https://www.blueletterbible.org/lexicon/g1693)

Definition: mad against (1x), to rage against one

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1693)

[Study Light](https://www.studylight.org/lexicons/greek/1693.html)

[Bible Hub](https://biblehub.com/str/greek/1693.htm)

[LSJ](https://lsj.gr/wiki/ἐμμαίνομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=1693)

[Bible Bento](https://biblebento.com/dictionary/G1693.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1693/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1693.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1693)
