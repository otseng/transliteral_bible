# γραφή

[graphē](https://www.blueletterbible.org/lexicon/g1124)

Definition: scripture (51x), writing, document

Part of speech: feminine noun

Occurs 51 times in 51 verses

Greek: [graphō](../g/g1125.md)

Hebrew: [kᵊṯāḇ](../h/h3791.md), [miḏrāš](../h/h4097.md)

Derived words: photograph, graphology, telegraph, [-graph](https://www.morewords.com/ends-with/graph/)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1124)

[Study Light](https://www.studylight.org/lexicons/greek/1124.html)

[Bible Hub](https://biblehub.com/str/greek/1124.htm)

[LSJ](https://lsj.gr/wiki/γραφή)

[NET Bible](http://classic.net.bible.org/strong.php?id=1124)

[Bible Bento](https://biblebento.com/dictionary/G1124.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1124/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1124.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1124)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B3%CF%81%CE%B1%CF%86%CE%AE)

[Oxford reference](http://www.oxfordreference.com/view/10.1093/oi/authority.20110803095904177)
