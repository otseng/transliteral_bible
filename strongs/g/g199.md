# ἀκριβῶς

[akribōs](https://www.blueletterbible.org/lexicon/g199)

Definition: diligently (2x), perfect (1x), perfectly (1x), circumspectly (1x), exactly, accurately, perfect, careful

Part of speech: adverb

Occurs 5 times in 5 verses

Synonyms: [perfect](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Perfect)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0199)

[Study Light](https://www.studylight.org/lexicons/greek/199.html)

[Bible Hub](https://biblehub.com/str/greek/199.htm)

[LSJ](https://lsj.gr/wiki/ἀκριβῶς)

[NET Bible](http://classic.net.bible.org/strong.php?id=199)

[Bible Bento](https://biblebento.com/dictionary/G0199.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0199/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0199.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g199)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CE%BA%CF%81%CE%B9%CE%B2%CF%8E%CF%82)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33475)

[Precept Austin](https://www.preceptaustin.org/ephesians_515-16#circumspectly)