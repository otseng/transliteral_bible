# σέ

[se](https://www.blueletterbible.org/lexicon/g4571)

Definition: thee (178x), thou (16x), thy house (1x), not translated (2x).

Part of speech: pronoun

Occurs 197 times in 179 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4571)

[Study Light](https://www.studylight.org/lexicons/greek/4571.html)

[Bible Hub](https://biblehub.com/str/greek/4571.htm)

[LSJ](https://lsj.gr/wiki/σέ)

[NET Bible](http://classic.net.bible.org/strong.php?id=4571)

[Bible Bento](https://biblebento.com/dictionary/G4571.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4571/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4571.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4571)

