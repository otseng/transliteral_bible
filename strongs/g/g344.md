# ἀνακάμπτω

[anakamptō](https://www.blueletterbible.org/lexicon/g344)

Definition: return (3x), turn again (1x).

Part of speech: verb

Occurs 4 times in 4 verses

Hebrew: [shuwb](../h/h7725.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0344)

[Study Light](https://www.studylight.org/lexicons/greek/344.html)

[Bible Hub](https://biblehub.com/str/greek/344.htm)

[LSJ](https://lsj.gr/wiki/ἀνακάμπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=344)

[Bible Bento](https://biblebento.com/dictionary/G344.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/344/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/344.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g344)
