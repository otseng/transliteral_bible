# συζεύγνυμι

[syzeugnymi](https://www.blueletterbible.org/lexicon/g4801)

Definition: join together (2x), yoke together, unite

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4801)

[Study Light](https://www.studylight.org/lexicons/greek/4801.html)

[Bible Hub](https://biblehub.com/str/greek/4801.htm)

[LSJ](https://lsj.gr/wiki/συζεύγνυμι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4801)

[Bible Bento](https://biblebento.com/dictionary/G4801.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4801/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4801.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4801)