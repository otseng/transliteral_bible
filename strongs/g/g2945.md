# κύκλῳ

[kyklō](https://www.blueletterbible.org/lexicon/g2945)

Definition: round about (6x), round (1x).

Part of speech: adverb

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2945)

[Study Light](https://www.studylight.org/lexicons/greek/2945.html)

[Bible Hub](https://biblehub.com/str/greek/2945.htm)

[LSJ](https://lsj.gr/wiki/κύκλῳ)

[NET Bible](http://classic.net.bible.org/strong.php?id=2945)

[Bible Bento](https://biblebento.com/dictionary/G2945.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2945/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2945.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2945)

