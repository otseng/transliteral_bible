# ἀλείφω

[aleiphō](https://www.blueletterbible.org/lexicon/g0218)

Definition: anoint (9x).

Part of speech: ?

Occurs 9 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0218)

[Study Light](https://www.studylight.org/lexicons/greek/0218.html)

[Bible Hub](https://biblehub.com/str/greek/0218.htm)

[LSJ](https://lsj.gr/wiki/ἀλείφω)

[NET Bible](http://classic.net.bible.org/strong.php?id=0218)

[Bible Bento](https://biblebento.com/dictionary/G0218.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0218/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0218.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g0218)

