# πίμπλημι

[pimplēmi](https://www.blueletterbible.org/lexicon/g4130)

Definition: fill (18x), accomplish (4x), furnish (1x), full ... come (1x), full of

Part of speech: verb

Occurs 27 times in 24 verses

Hebrew: [mālā'](../h/h4390.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4130)

[Study Light](https://www.studylight.org/lexicons/greek/4130.html)

[Bible Hub](https://biblehub.com/str/greek/4130.htm)

[LSJ](https://lsj.gr/wiki/πίμπλημι)

[NET Bible](http://classic.net.bible.org/strong.php?id=4130)

[Bible Bento](https://biblebento.com/dictionary/G4130.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4130/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4130.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4130)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%AF%CE%BC%CF%80%CE%BB%CE%B7%CE%BC%CE%B9)