# ὀκνηρός

[oknēros](https://www.blueletterbible.org/lexicon/g3636)

Definition: slothful (2x), grievous (1x), sluggish, backward

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3636)

[Study Light](https://www.studylight.org/lexicons/greek/3636.html)

[Bible Hub](https://biblehub.com/str/greek/3636.htm)

[LSJ](https://lsj.gr/wiki/ὀκνηρός)

[NET Bible](http://classic.net.bible.org/strong.php?id=3636)

[Bible Bento](https://biblebento.com/dictionary/G3636.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3636/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3636.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3636)
