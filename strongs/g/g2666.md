# καταπίνω

[katapinō](https://www.blueletterbible.org/lexicon/g2666)

Definition: swallow (4x), swallow (1x), drown (1x), devour (1x), gulp entire

Part of speech: verb

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2666)

[Study Light](https://www.studylight.org/lexicons/greek/2666.html)

[Bible Hub](https://biblehub.com/str/greek/2666.htm)

[LSJ](https://lsj.gr/wiki/καταπίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2666)

[Bible Bento](https://biblebento.com/dictionary/G2666.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2666/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2666.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2666)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%B1%CF%84%CE%B1%CF%80%CE%AF%CE%BD%CF%89)

[Precept Austin](https://www.preceptaustin.org/1_peter_58-14#devour%202666%20katapino)