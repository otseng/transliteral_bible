# δοῦλος

[doulos](https://www.blueletterbible.org/lexicon/g1401)

Definition: servant (120x), bond (6x), bondman (1x), slave, bondman, man of servile condition, attendant

Part of speech: masculine noun

Occurs 127 times in 119 verses

Hebrew: ['ebed](../h/h5650.md)

Synonyms: [servant](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Servant)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1401)

[Study Light](https://www.studylight.org/lexicons/greek/1401.html)

[Bible Hub](https://biblehub.com/str/greek/1401.htm)

[LSJ](https://lsj.gr/wiki/δοῦλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1401)

[Bible Bento](https://biblebento.com/dictionary/G1401.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1401/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1401.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1401)

[Wiktionary](https://en.wiktionary.org/wiki/δούλος)

[Precept Austin](https://www.preceptaustin.org/index.php/philippians_11-8)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=35821)

[Wikipedia - Slavery](https://en.wikipedia.org/wiki/Slavery_in_ancient_Greece)