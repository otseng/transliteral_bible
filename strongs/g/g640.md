# ἀπορία

[aporia](https://www.blueletterbible.org/lexicon/g640)

Definition: perplexity (1x), state of quandary, impassable, difficult

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0640)

[Study Light](https://www.studylight.org/lexicons/greek/640.html)

[Bible Hub](https://biblehub.com/str/greek/640.htm)

[LSJ](https://lsj.gr/wiki/ἀπορία)

[NET Bible](http://classic.net.bible.org/strong.php?id=640)

[Bible Bento](https://biblebento.com/dictionary/G0640.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0640/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0640.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g640)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CF%80%CE%BF%CF%81%CE%AF%CE%B1)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34596)

[Precept Austin](https://www.preceptaustin.org/luke-21-commentary#perplexity)