# Σιδών

[Sidōn](https://www.blueletterbible.org/lexicon/g4605)

Definition: Sidon, an ancient and wealthy city of Phoenicia, on the east coast of the Mediterranean Sea, less than 20 miles (30 km) north of Tyre

Part of speech: proper locative noun

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4605)

[Study Light](https://www.studylight.org/lexicons/greek/4605.html)

[Bible Hub](https://biblehub.com/str/greek/4605.htm)

[LSJ](https://lsj.gr/wiki/Σιδών)

[NET Bible](http://classic.net.bible.org/strong.php?id=4605)

[Bible Bento](https://biblebento.com/dictionary/G4605.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4605/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4605.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4605)

[Wikipedia](https://en.wikipedia.org/wiki/Sidon)

[Video Bible](https://www.videobible.com/bible-dictionary/sidon,)