# στοά

[stoa](https://www.blueletterbible.org/lexicon/g4745)

Definition: porch (4x), a portico, a covered colonnade where people can stand or walk protected from the weather and the heat of the sun, a colonnade or interior piazza, roofed enclosure supported by pillars, portico, colonnade, piazza, arcade, cloister, covered walkway

Part of speech: feminine noun

Occurs 4 times in 4 verses

Hebrew: ['êlām](../h/h361.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4745)

[Study Light](https://www.studylight.org/lexicons/greek/4745.html)

[Bible Hub](https://biblehub.com/str/greek/4745.htm)

[LSJ](https://lsj.gr/wiki/στοά)

[NET Bible](http://classic.net.bible.org/strong.php?id=4745)

[Bible Bento](https://biblebento.com/dictionary/G4745.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4745/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4745.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4745)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%83%CF%84%CE%BF%CE%AC)

[Wikipedia](https://en.wikipedia.org/wiki/Stoa)
