# ἀδικία

[adikia](https://www.blueletterbible.org/lexicon/g93)

Definition: unrighteousness (16x), iniquity (6x), unjust (2x), wrong (1x), injustice, "not right"

Part of speech: feminine noun

Occurs 25 times in 24 verses

Hebrew: [resha`](../h/h7562.md), ['evel](../h/h5766.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0093)

[Study Light](https://www.studylight.org/lexicons/greek/93.html)

[Bible Hub](https://biblehub.com/str/greek/93.htm)

[LSJ](https://lsj.gr/wiki/ἀδικία)

[NET Bible](http://classic.net.bible.org/strong.php?id=93)

[Bible Bento](https://biblebento.com/dictionary/G0093.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0093/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0093.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g93)

[Wikipedia](https://en.wikipedia.org/wiki/Adikia)

[Wiktionary](https://en.wiktionary.org/wiki/αδικία)

[Sermon Index](https://www.preceptaustin.org/romans_129-31#unrighteousness)

[Precept Austin](https://www.preceptaustin.org/romans_129-31#unrighteousness)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/0093-adikia-unrighteousness.htm)
