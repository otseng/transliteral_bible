# Γολγοθᾶ

[Golgotha](https://www.blueletterbible.org/lexicon/g1115)

Definition: Golgotha (3x), "skull", the name of a place outside Jerusalem where Jesus was crucified

Part of speech: proper locative noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1115)

[Study Light](https://www.studylight.org/lexicons/greek/1115.html)

[Bible Hub](https://biblehub.com/str/greek/1115.htm)

[LSJ](https://lsj.gr/wiki/Γολγοθᾶ)

[NET Bible](http://classic.net.bible.org/strong.php?id=1115)

[Bible Bento](https://biblebento.com/dictionary/G1115.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1115/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1115.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1115)

[Wikipedia](https://en.wikipedia.org/wiki/Calvary)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/golgotha.html)

[Video Bible](https://www.videobible.com/bible-dictionary/golgotha)