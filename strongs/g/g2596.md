# κατά

[kata](https://www.blueletterbible.org/lexicon/g2596)

Definition: according to (107x), after (61x), against (58x), in (36x), by (27x), daily (with G2250) (15x), as (11x)

Part of speech: preposition

Occurs 482 times in 436 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2596)

[Study Light](https://www.studylight.org/lexicons/greek/2596.html)

[Bible Hub](https://biblehub.com/str/greek/2596.htm)

[LSJ](https://lsj.gr/wiki/κατά)

[NET Bible](http://classic.net.bible.org/strong.php?id=2596)

[Bible Bento](https://biblebento.com/dictionary/G2596.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2596/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2596.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2596)