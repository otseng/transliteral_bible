# δυσεντερία

[dysenteria](https://www.blueletterbible.org/lexicon/g1420)

Definition: bloody flux (1x), dysentery, bowel ailment

Part of speech: neuter noun

Occurs 1 times in 1 verses

Derived words: dysentery

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1420)

[Study Light](https://www.studylight.org/lexicons/greek/1420.html)

[Bible Hub](https://biblehub.com/str/greek/1420.htm)

[LSJ](https://lsj.gr/wiki/δυσεντερία)

[NET Bible](http://classic.net.bible.org/strong.php?id=1420)

[Bible Bento](https://biblebento.com/dictionary/G1420.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1420/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1420.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1420)
