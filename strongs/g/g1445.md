# Ἑβραῖος

[Hebraios](https://www.blueletterbible.org/lexicon/g1445)

Definition: Hebrew (5x), those who live in Palestine and use the language of the country, all Jewish Christians, whether they spoke Aramaic or Greek

Part of speech: masculine noun

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1445)

[Study Light](https://www.studylight.org/lexicons/greek/1445.html)

[Bible Hub](https://biblehub.com/str/greek/1445.htm)

[LSJ](https://lsj.gr/wiki/Ἑβραῖος)

[NET Bible](http://classic.net.bible.org/strong.php?id=1445)

[Bible Bento](https://biblebento.com/dictionary/G1445.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1445/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1445.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1445)
