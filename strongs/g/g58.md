# ἀγορά

[agora](https://www.blueletterbible.org/lexicon/g58)

Definition: market (6x), marketplace (4x), street (1x), assembly for elections and debate and trials, market place, central public space

Part of speech: feminine noun

Occurs 11 times in 11 verses

Derived words: agoraphobia

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0058)

[Study Light](https://www.studylight.org/lexicons/greek/58.html)

[Bible Hub](https://biblehub.com/str/greek/58.htm)

[LSJ](https://lsj.gr/wiki/ἀγορά)

[NET Bible](http://classic.net.bible.org/strong.php?id=58)

[Bible Bento](https://biblebento.com/dictionary/G0058.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0058/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0058.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g58)

[Wiktionary](https://en.wiktionary.org/wiki/agora)

[Wikipedia](https://en.wikipedia.org/wiki/Agora)