# ῥυπαίνω

[rhypainō](https://www.blueletterbible.org/lexicon/g4510)

Definition: filthy (2x).

Part of speech: verb

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4510)

[Study Light](https://www.studylight.org/lexicons/greek/4510.html)

[Bible Hub](https://biblehub.com/str/greek/4510.htm)

[LSJ](https://lsj.gr/wiki/ῥυπαίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4510)

[Bible Bento](https://biblebento.com/dictionary/G4510.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4510/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4510.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4510)

