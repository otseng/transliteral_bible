# ὀρθρίζω

[orthrizō](https://www.blueletterbible.org/lexicon/g3719)

Definition: come early in the morning (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [šāḵam](../h/h7925.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3719)

[Study Light](https://www.studylight.org/lexicons/greek/3719.html)

[Bible Hub](https://biblehub.com/str/greek/3719.htm)

[LSJ](https://lsj.gr/wiki/ὀρθρίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=3719)

[Bible Bento](https://biblebento.com/dictionary/G3719.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3719/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3719.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3719)

[Precept Austin](https://www.preceptaustin.org/luke-21-commentary#arise)