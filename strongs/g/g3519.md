# νησίον

[nēsion](https://www.blueletterbible.org/lexicon/g3519)

Definition: island (1x), an islet

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3519)

[Study Light](https://www.studylight.org/lexicons/greek/3519.html)

[Bible Hub](https://biblehub.com/str/greek/3519.htm)

[LSJ](https://lsj.gr/wiki/νησίον)

[NET Bible](http://classic.net.bible.org/strong.php?id=3519)

[Bible Bento](https://biblebento.com/dictionary/G3519.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3519/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3519.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3519)
