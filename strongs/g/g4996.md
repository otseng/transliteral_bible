# σωφρόνως

[sōphronōs](https://www.blueletterbible.org/lexicon/g4996)

Definition: soberly (1x), with sound mind, soberly, temperately, discreetly

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4996)

[Study Light](https://www.studylight.org/lexicons/greek/4996.html)

[Bible Hub](https://biblehub.com/str/greek/4996.htm)

[LSJ](https://lsj.gr/wiki/σωφρόνως)

[NET Bible](http://classic.net.bible.org/strong.php?id=4996)

[Bible Bento](https://biblebento.com/dictionary/G4996.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4996/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4996.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4996)
