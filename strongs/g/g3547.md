# νομοδιδάσκαλος

[nomodidaskalos](https://www.blueletterbible.org/lexicon/g3547)

Definition: doctor of the law (2x), teacher of the law (1x)

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3547)

[Study Light](https://www.studylight.org/lexicons/greek/3547.html)

[Bible Hub](https://biblehub.com/str/greek/3547.htm)

[LSJ](https://lsj.gr/wiki/νομοδιδάσκαλος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3547)

[Bible Bento](https://biblebento.com/dictionary/G3547.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3547/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3547.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3547)