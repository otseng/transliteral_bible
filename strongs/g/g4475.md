# ῥάπισμα

[rhapisma](https://www.blueletterbible.org/lexicon/g4475)

Definition: strike with the palm of (one's) hand (with G906) (1x), strike with the palm of (one's) hand (with G1325) (1x), smite with (one's) hand (with G1325) (1x), blow with a rod, slap in the face

Part of speech: neuter noun

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4475)

[Study Light](https://www.studylight.org/lexicons/greek/4475.html)

[Bible Hub](https://biblehub.com/str/greek/4475.htm)

[LSJ](https://lsj.gr/wiki/ῥάπισμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=4475)

[Bible Bento](https://biblebento.com/dictionary/G4475.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4475/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4475.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4475)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%81%CE%AC%CF%80%CE%B9%CF%83%CE%BC%CE%B1)