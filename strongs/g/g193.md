# ἀκρατής

[akratēs](https://www.blueletterbible.org/lexicon/g193)

Definition: incontinent (1x), without self-control, intemperate

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0193)

[Study Light](https://www.studylight.org/lexicons/greek/193.html)

[Bible Hub](https://biblehub.com/str/greek/193.htm)

[LSJ](https://lsj.gr/wiki/ἀκρατής)

[NET Bible](http://classic.net.bible.org/strong.php?id=193)

[Bible Bento](https://biblebento.com/dictionary/G0193.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/193/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/193.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g193)
