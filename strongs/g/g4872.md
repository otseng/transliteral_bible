# συναναβαίνω

[synanabainō](https://www.blueletterbible.org/lexicon/g4872)

Definition: come up with (2x)

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4872)

[Study Light](https://www.studylight.org/lexicons/greek/4872.html)

[Bible Hub](https://biblehub.com/str/greek/4872.htm)

[LSJ](https://lsj.gr/wiki/συναναβαίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4872)

[Bible Bento](https://biblebento.com/dictionary/G4872.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4872/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4872.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4872)
