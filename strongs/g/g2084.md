# ἑτερόγλωσσος

[heteroglōssos](https://www.blueletterbible.org/lexicon/g2084)

Definition: other tongue (1x), one who speaks a foreign language

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2084)

[Study Light](https://www.studylight.org/lexicons/greek/2084.html)

[Bible Hub](https://biblehub.com/str/greek/2084.htm)

[LSJ](https://lsj.gr/wiki/ἑτερόγλωσσος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2084)

[Bible Bento](https://biblebento.com/dictionary/G2084.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2084/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2084.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2084)
