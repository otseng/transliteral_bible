# ἑξῆς

[hexēs](https://www.blueletterbible.org/lexicon/g1836)

Definition: next (1x), next day (1x), day after (1x), day following (1x), morrow (1x).

Part of speech: adverb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1836)

[Study Light](https://www.studylight.org/lexicons/greek/1836.html)

[Bible Hub](https://biblehub.com/str/greek/1836.htm)

[LSJ](https://lsj.gr/wiki/ἑξῆς)

[NET Bible](http://classic.net.bible.org/strong.php?id=1836)

[Bible Bento](https://biblebento.com/dictionary/G1836.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1836/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1836.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1836)

