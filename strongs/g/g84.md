# ἀδήλως

[adēlōs](https://www.blueletterbible.org/lexicon/g84)

Definition: uncertainly (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g84)

[Study Light](https://www.studylight.org/lexicons/greek/84.html)

[Bible Hub](https://biblehub.com/str/greek/84.htm)

[LSJ](https://lsj.gr/wiki/ἀδήλως)

[NET Bible](http://classic.net.bible.org/strong.php?id=84)

[Bible Bento](https://biblebento.com/dictionary/G84.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/84/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/84.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g84)

