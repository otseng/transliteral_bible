# μεθόριον

[methorion](https://www.blueletterbible.org/lexicon/g3181)

Definition: border (1x).

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/3181.html)

[Bible Hub](https://biblehub.com/str/greek/3181.htm)

[LSJ](https://lsj.gr/wiki/μεθόριον)

[NET Bible](http://classic.net.bible.org/strong.php?id=3181)

[Bible Bento](https://biblebento.com/dictionary/G3181.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3181/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3181.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3181)

