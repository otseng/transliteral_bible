# πιστόω

[pistoō](https://www.blueletterbible.org/lexicon/g4104)

Definition: be assured of (1x), to make faithful, render trustworthy, to be firmly persuaded of

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4104)

[Study Light](https://www.studylight.org/lexicons/greek/4104.html)

[Bible Hub](https://biblehub.com/str/greek/4104.htm)

[LSJ](https://lsj.gr/wiki/πιστόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4104)

[Bible Bento](https://biblebento.com/dictionary/G4104.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4104/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4104.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4104)
