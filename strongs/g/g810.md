# ἀσωτία

[asōtia](https://www.blueletterbible.org/lexicon/g810)

Definition: riot (2x), excess (1x), profligacy, prodigality, unsavedness

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0810)

[Study Light](https://www.studylight.org/lexicons/greek/810.html)

[Bible Hub](https://biblehub.com/str/greek/810.htm)

[LSJ](https://lsj.gr/wiki/ἀσωτία)

[NET Bible](http://classic.net.bible.org/strong.php?id=810)

[Bible Bento](https://biblebento.com/dictionary/G0810.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/810/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/810.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g810)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CF%83%CF%89%CF%84%CE%AF%CE%B1)
