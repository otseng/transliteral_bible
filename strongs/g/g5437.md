# φυγή

[phygē](https://www.blueletterbible.org/lexicon/g5437)

Definition: flight (2x)

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5437)

[Study Light](https://www.studylight.org/lexicons/greek/5437.html)

[Bible Hub](https://biblehub.com/str/greek/5437.htm)

[LSJ](https://lsj.gr/wiki/φυγή)

[NET Bible](http://classic.net.bible.org/strong.php?id=5437)

[Bible Bento](https://biblebento.com/dictionary/G5437.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5437/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5437.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5437)