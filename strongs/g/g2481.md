# ἴσως

[isōs](https://www.blueletterbible.org/lexicon/g2481)

Definition: it may be (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2481)

[Study Light](https://www.studylight.org/lexicons/greek/2481.html)

[Bible Hub](https://biblehub.com/str/greek/2481.htm)

[LSJ](https://lsj.gr/wiki/ἴσως)

[NET Bible](http://classic.net.bible.org/strong.php?id=2481)

[Bible Bento](https://biblebento.com/dictionary/G2481.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2481/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2481.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2481)

