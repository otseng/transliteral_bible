# πρόβατον

[probaton](https://www.blueletterbible.org/lexicon/g4263)

Definition: sheep (40x), sheepfold (with G833) (1x)

Part of speech: neuter noun

Occurs 41 times in 37 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4263)

[Study Light](https://www.studylight.org/lexicons/greek/4263.html)

[Bible Hub](https://biblehub.com/str/greek/4263.htm)

[LSJ](https://lsj.gr/wiki/πρόβατον)

[NET Bible](http://classic.net.bible.org/strong.php?id=4263)

[Bible Bento](https://biblebento.com/dictionary/G4263.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4263/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4263.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4263)

[Wiktionary](https://en.wiktionary.org/wiki/πρόβατον)