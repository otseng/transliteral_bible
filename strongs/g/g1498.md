# εἴην

[eiēn](https://www.blueletterbible.org/lexicon/g1498)

Definition: should be (3x), be (3x), meant (2x), might be (1x), should mean (1x), wert (1x), not translated (1x).

Part of speech: verb

Occurs 13 times in 12 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1498)

[Study Light](https://www.studylight.org/lexicons/greek/1498.html)

[Bible Hub](https://biblehub.com/str/greek/1498.htm)

[LSJ](https://lsj.gr/wiki/εἴην)

[NET Bible](http://classic.net.bible.org/strong.php?id=1498)

[Bible Bento](https://biblebento.com/dictionary/G1498.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1498/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1498.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1498)
