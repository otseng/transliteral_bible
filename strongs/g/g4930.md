# συντέλεια

[synteleia](https://www.blueletterbible.org/lexicon/g4930)

Definition: end (6x), completion, consummation, end

Part of speech: feminine noun

Occurs 6 times in 6 verses

Synonyms: [end](https://simple.uniquebibleapp.com/book/Synonyms/Greek#End)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4930)

[Study Light](https://www.studylight.org/lexicons/greek/4930.html)

[Bible Hub](https://biblehub.com/str/greek/4930.htm)

[LSJ](https://lsj.gr/wiki/συντέλεια)

[NET Bible](http://classic.net.bible.org/strong.php?id=4930)

[Bible Bento](https://biblebento.com/dictionary/G4930.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4930/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4930.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4930)
