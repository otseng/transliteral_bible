# ἀπόβλητος

[apoblētos](https://www.blueletterbible.org/lexicon/g579)

Definition: be refused (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g579)

[Study Light](https://www.studylight.org/lexicons/greek/579.html)

[Bible Hub](https://biblehub.com/str/greek/579.htm)

[LSJ](https://lsj.gr/wiki/ἀπόβλητος)

[NET Bible](http://classic.net.bible.org/strong.php?id=579)

[Bible Bento](https://biblebento.com/dictionary/G579.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/579/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/579.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g579)

