# ἄγριος

[agrios](https://www.blueletterbible.org/lexicon/g66)

Definition: wild (2x), raging (1x). living or growing in the fields or woods, non-domesticated

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0066)

[Study Light](https://www.studylight.org/lexicons/greek/66.html)

[Bible Hub](https://biblehub.com/str/greek/66.htm)

[LSJ](https://lsj.gr/wiki/ἄγριος)

[NET Bible](http://classic.net.bible.org/strong.php?id=66)

[Bible Bento](https://biblebento.com/dictionary/G0066.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0066/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0066.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g66)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%84%CE%B3%CF%81%CE%B9%CE%BF%CF%82)
