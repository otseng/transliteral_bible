# ἐρημόω

[erēmoō](https://www.blueletterbible.org/lexicon/g2049)

Definition: bring to desolation (2x), desolate (1x), come to nought (1x), make desolate (1x), lay waste, to ruin, despoil one, strip her of her treasures

Part of speech: verb

Occurs 5 times in 5 verses

Hebrew: [shabar](../h/h7665.md), [šammâ](../h/h8047.md), [shadad](../h/h7703.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2049)

[Study Light](https://www.studylight.org/lexicons/greek/2049.html)

[Bible Hub](https://biblehub.com/str/greek/2049.htm)

[LSJ](https://lsj.gr/wiki/ἐρημόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2049)

[Bible Bento](https://biblebento.com/dictionary/G2049.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2049/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2049.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2049)