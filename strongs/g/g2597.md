# καταβαίνω

[katabainō](https://www.blueletterbible.org/lexicon/g2597)

Definition: come down (41x), descend (18x), go down (17x), fall down (1x), step down (1x), get down (1x), fall (1x), variations of 'come down' (1x), be cast down to the lowest state of wretchedness and shame

Part of speech: verb

Occurs 86 times in 80 verses

Hebrew: [yarad](../h/h3381.md)

Derived words: [katabasis](https://en.wikipedia.org/wiki/Katabasis)

Synonyms: [come](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Come)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2597)

[Study Light](https://www.studylight.org/lexicons/greek/2597.html)

[Bible Hub](https://biblehub.com/str/greek/2597.htm)

[LSJ](https://lsj.gr/wiki/καταβαίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2597)

[Bible Bento](https://biblebento.com/dictionary/G2597.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2597/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2597.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2597)