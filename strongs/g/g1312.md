# διαφθορά

[diaphthora](https://www.blueletterbible.org/lexicon/g1312)

Definition: corruption (6x), destruction, decay, decomposition of body

Part of speech: feminine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1312)

[Study Light](https://www.studylight.org/lexicons/greek/1312.html)

[Bible Hub](https://biblehub.com/str/greek/1312.htm)

[LSJ](https://lsj.gr/wiki/διαφθορά)

[NET Bible](http://classic.net.bible.org/strong.php?id=1312)

[Bible Bento](https://biblebento.com/dictionary/G1312.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1312/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1312.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1312)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B4%CE%B9%CE%B1%CF%86%CE%B8%CE%BF%CF%81%CE%AC)

[Precept Austin](https://www.preceptaustin.org/acts-2-commentary#decay)
