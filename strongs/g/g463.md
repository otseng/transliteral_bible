# ἀνοχή

[anochē](https://www.blueletterbible.org/lexicon/g463)

Definition: forbearance (2x), toleration, holding back, cessation of hostilities; armistice, truce, "to bear"

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0463)

[Study Light](https://www.studylight.org/lexicons/greek/463.html)

[Bible Hub](https://biblehub.com/str/greek/463.htm)

[LSJ](https://lsj.gr/wiki/ἀνοχή)

[NET Bible](http://classic.net.bible.org/strong.php?id=463)

[Bible Bento](https://biblebento.com/dictionary/G0463.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0463/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0463.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g463)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BC%80%CE%BD%CE%BF%CF%87%CE%AE)
