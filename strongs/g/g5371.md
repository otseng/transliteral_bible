# Φιλήμων

[philēmōn](https://www.blueletterbible.org/lexicon/g5371)

Definition: Philemon (2x), "one who kisses", a resident of Colosse, converted to Christianity by Paul, and the recipient of the letter bearing his name

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5371)

[Study Light](https://www.studylight.org/lexicons/greek/5371.html)

[Bible Hub](https://biblehub.com/str/greek/5371.htm)

[LSJ](https://lsj.gr/wiki/Φιλήμων)

[NET Bible](http://classic.net.bible.org/strong.php?id=5371)

[Bible Bento](https://biblebento.com/dictionary/G5371.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5371/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5371.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5371)

[Wikipedia](https://en.wikipedia.org/wiki/Philemon_%28biblical_figure%29)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/philemon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/philemon)