# ἐπείπερ

[epeiper](https://www.blueletterbible.org/lexicon/g1897)

Definition: seeing (1x), since indeed, since at all events

Part of speech: conjunction

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1897)

[Study Light](https://www.studylight.org/lexicons/greek/1897.html)

[Bible Hub](https://biblehub.com/str/greek/1897.htm)

[LSJ](https://lsj.gr/wiki/ἐπείπερ)

[NET Bible](http://classic.net.bible.org/strong.php?id=1897)

[Bible Bento](https://biblebento.com/dictionary/G1897.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1897/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1897.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1897)