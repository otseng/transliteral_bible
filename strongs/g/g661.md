# ἀποτίνω

[apotinō](https://www.blueletterbible.org/lexicon/g661)

Definition: repay (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0661)

[Study Light](https://www.studylight.org/lexicons/greek/661.html)

[Bible Hub](https://biblehub.com/str/greek/661.htm)

[LSJ](https://lsj.gr/wiki/ἀποτίνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=661)

[Bible Bento](https://biblebento.com/dictionary/G0661.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/661/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/661.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g661)
