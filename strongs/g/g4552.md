# σάπφιρος

[sapphiros](https://www.blueletterbible.org/lexicon/g4552)

Definition: sapphire (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4552)

[Study Light](https://www.studylight.org/lexicons/greek/4552.html)

[Bible Hub](https://biblehub.com/str/greek/4552.htm)

[LSJ](https://lsj.gr/wiki/σάπφιρος)

[NET Bible](http://classic.net.bible.org/strong.php?id=4552)

[Bible Bento](https://biblebento.com/dictionary/G4552.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4552/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4552.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4552)

