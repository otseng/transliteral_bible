# ἀντίλυτρον

[antilytron](https://www.blueletterbible.org/lexicon/g487)

Definition: ransom (1x), redemption-price

Part of speech: neuter noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0487)

[Study Light](https://www.studylight.org/lexicons/greek/487.html)

[Bible Hub](https://biblehub.com/str/greek/487.htm)

[LSJ](https://lsj.gr/wiki/ἀντίλυτρον)

[NET Bible](http://classic.net.bible.org/strong.php?id=487)

[Bible Bento](https://biblebento.com/dictionary/G0487.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/487/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/487.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g487)
