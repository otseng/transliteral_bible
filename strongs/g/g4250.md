# πρίν

[prin](https://www.blueletterbible.org/lexicon/g4250)

Definition: before (11x), before that (2x), ere (1x).

Part of speech: adverb

Occurs 14 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4250)

[Study Light](https://www.studylight.org/lexicons/greek/4250.html)

[Bible Hub](https://biblehub.com/str/greek/4250.htm)

[LSJ](https://lsj.gr/wiki/πρίν)

[NET Bible](http://classic.net.bible.org/strong.php?id=4250)

[Bible Bento](https://biblebento.com/dictionary/G4250.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4250/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4250.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4250)

