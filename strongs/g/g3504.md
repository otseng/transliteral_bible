# νεόφυτος

[neophytos](https://www.blueletterbible.org/lexicon/g3504)

Definition: novice (1x), young convert 

Part of speech: adjective

Occurs 1 times in 1 verses

Derived words: neophyte

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3504)

[Study Light](https://www.studylight.org/lexicons/greek/3504.html)

[Bible Hub](https://biblehub.com/str/greek/3504.htm)

[LSJ](https://lsj.gr/wiki/νεόφυτος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3504)

[Bible Bento](https://biblebento.com/dictionary/G3504.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3504/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3504.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3504)
