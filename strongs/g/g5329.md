# Φαρές

[Phares](https://www.blueletterbible.org/lexicon/g5329)

Definition: Phares (3x), a breach, the son of Judah and Tamar, his daughter-in-law

Part of speech: proper masculine noun

Occurs 3 times in 2 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/5329.html)

[LSJ](https://lsj.gr/wiki/Φαρές)

[NET Bible](http://classic.net.bible.org/strong.php?id=5329)

[Bible Bento](https://biblebento.com/dictionary/G5329.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5329/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5329.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5329)

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5329)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/phares.html)