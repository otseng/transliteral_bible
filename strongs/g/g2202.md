# ζευκτηρία

[zeuktēria](https://www.blueletterbible.org/lexicon/g2202)

Definition: band (1x), fastening

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2202)

[Study Light](https://www.studylight.org/lexicons/greek/2202.html)

[Bible Hub](https://biblehub.com/str/greek/2202.htm)

[LSJ](https://lsj.gr/wiki/ζευκτηρία)

[NET Bible](http://classic.net.bible.org/strong.php?id=2202)

[Bible Bento](https://biblebento.com/dictionary/G2202.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2202/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2202.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2202)
