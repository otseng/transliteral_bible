# Κλωπᾶς

[Klōpas](https://www.blueletterbible.org/lexicon/g2832)

Definition: Cleophas (1x), Cleopas

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

Greek: [Kleopas](../g/g2810.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2832)

[Study Light](https://www.studylight.org/lexicons/greek/2832.html)

[Bible Hub](https://biblehub.com/str/greek/2832.htm)

[LSJ](https://lsj.gr/wiki/Κλωπᾶς)

[NET Bible](http://classic.net.bible.org/strong.php?id=2832)

[Bible Bento](https://biblebento.com/dictionary/G2832.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2832/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2832.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2832)

[Wikipedia](https://en.wikipedia.org/wiki/Cleopas)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/cleophas.html)