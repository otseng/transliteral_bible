# προκόπτω

[prokoptō](https://www.blueletterbible.org/lexicon/g4298)

Definition: increase (2x), be far spent (1x), profit (1x), proceed (1x), wax (1x).

Part of speech: verb

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4298)

[Study Light](https://www.studylight.org/lexicons/greek/4298.html)

[Bible Hub](https://biblehub.com/str/greek/4298.htm)

[LSJ](https://lsj.gr/wiki/προκόπτω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4298)

[Bible Bento](https://biblebento.com/dictionary/G4298.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4298/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4298.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4298)

[Precept Austin](https://www.preceptaustin.org/romans_1312#p)
