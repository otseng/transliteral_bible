# μυελός

[myelos](https://www.blueletterbible.org/lexicon/g3452)

Definition: marrow (1x)

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3452)

[Study Light](https://www.studylight.org/lexicons/greek/3452.html)

[Bible Hub](https://biblehub.com/str/greek/3452.htm)

[LSJ](https://lsj.gr/wiki/μυελός)

[NET Bible](http://classic.net.bible.org/strong.php?id=3452)

[Bible Bento](https://biblebento.com/dictionary/G3452.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3452/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3452.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3452)
