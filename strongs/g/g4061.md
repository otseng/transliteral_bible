# περιτομή

[peritomē](https://www.blueletterbible.org/lexicon/g4061)

Definition: circumcision (35x), circumcised (1x)

Part of speech: feminine noun

Occurs 36 times in 32 verses

Greek: [peritemnō](../g/g4059.md)

Hebrew: [muwl](../h/h4135.md), [namal](../h/h5243.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4061)

[Study Light](https://www.studylight.org/lexicons/greek/4061.html)

[Bible Hub](https://biblehub.com/str/greek/4061.htm)

[LSJ](https://lsj.gr/wiki/περιτομή)

[NET Bible](http://classic.net.bible.org/strong.php?id=4061)

[Bible Bento](https://biblebento.com/dictionary/G4061.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4061/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4061.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4061)

[Wiktionary](https://en.wiktionary.org/wiki/%CF%80%CE%B5%CF%81%CE%B9%CF%84%CE%BF%CE%BC%CE%AE)

[Wikipedia - Circumcision controversies](https://en.wikipedia.org/wiki/Circumcision_controversies)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/4061-peritome-circumcision.htm)
