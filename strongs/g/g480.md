# ἀντίκειμαι

[antikeimai](https://www.blueletterbible.org/lexicon/g480)

Definition: adversary (5x), be contrary (2x), oppose (1x).

Part of speech: verb

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0480)

[Study Light](https://www.studylight.org/lexicons/greek/480.html)

[Bible Hub](https://biblehub.com/str/greek/480.htm)

[LSJ](https://lsj.gr/wiki/ἀντίκειμαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=480)

[Bible Bento](https://biblebento.com/dictionary/G0480.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0480/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0480.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g480)

[Precept Austin](https://www.preceptaustin.org/galatians_517-18#opposition)