# Γαλατικός

[Galatikos](https://www.blueletterbible.org/lexicon/g1054)

Definition: of Galatia (2x)

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1054)

[Study Light](https://www.studylight.org/lexicons/greek/1054.html)

[Bible Hub](https://biblehub.com/str/greek/1054.htm)

[LSJ](https://lsj.gr/wiki/Γαλατικός)

[NET Bible](http://classic.net.bible.org/strong.php?id=1054)

[Bible Bento](https://biblebento.com/dictionary/G1054.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1054/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1054.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1054)
