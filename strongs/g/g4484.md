# Ῥήγιον

[Rhēgion](https://www.blueletterbible.org/lexicon/g4484)

Definition: Rhegium (1x), "breach", an Italian town situated on the Bruttian coast, just at the southern entrance of the Straits of Messina

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4484)

[Study Light](https://www.studylight.org/lexicons/greek/4484.html)

[Bible Hub](https://biblehub.com/str/greek/4484.htm)

[LSJ](https://lsj.gr/wiki/Ῥήγιον)

[NET Bible](http://classic.net.bible.org/strong.php?id=4484)

[Bible Bento](https://biblebento.com/dictionary/G4484.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4484/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4484.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4484)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/R/rhegium.html)

[Video Bible](https://www.videobible.com/bible-dictionary/rhegium)