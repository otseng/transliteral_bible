# πρηνής

[prēnēs](https://www.blueletterbible.org/lexicon/g4248)

Definition: headlong (1x), leaning (falling) forward

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4248)

[Study Light](https://www.studylight.org/lexicons/greek/4248.html)

[Bible Hub](https://biblehub.com/str/greek/4248.htm)

[LSJ](https://lsj.gr/wiki/πρηνής)

[NET Bible](http://classic.net.bible.org/strong.php?id=4248)

[Bible Bento](https://biblebento.com/dictionary/G4248.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4248/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4248.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4248)
