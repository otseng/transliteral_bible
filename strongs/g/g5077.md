# τεφρόω

[tephroō](https://www.blueletterbible.org/lexicon/g5077)

Definition: turn into ashes (1x), reduce to ashes, consume

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5077)

[Study Light](https://www.studylight.org/lexicons/greek/5077.html)

[Bible Hub](https://biblehub.com/str/greek/5077.htm)

[LSJ](https://lsj.gr/wiki/τεφρόω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5077)

[Bible Bento](https://biblebento.com/dictionary/G5077.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5077/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5077.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5077)

