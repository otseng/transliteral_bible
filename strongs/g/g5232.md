# ὑπεραυξάνω

[hyperauxanō](https://www.blueletterbible.org/lexicon/g5232)

Definition: grow exceedingly (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5232)

[Study Light](https://www.studylight.org/lexicons/greek/5232.html)

[Bible Hub](https://biblehub.com/str/greek/5232.htm)

[LSJ](https://lsj.gr/wiki/ὑπεραυξάνω)

[NET Bible](http://classic.net.bible.org/strong.php?id=5232)

[Bible Bento](https://biblebento.com/dictionary/G5232.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5232/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5232.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5232)
