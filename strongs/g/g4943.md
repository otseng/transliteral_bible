# συνυπουργέω

[synypourgeō](https://www.blueletterbible.org/lexicon/g4943)

Definition: help together (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g4943)

[Study Light](https://www.studylight.org/lexicons/greek/4943.html)

[Bible Hub](https://biblehub.com/str/greek/4943.htm)

[LSJ](https://lsj.gr/wiki/συνυπουργέω)

[NET Bible](http://classic.net.bible.org/strong.php?id=4943)

[Bible Bento](https://biblebento.com/dictionary/G4943.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4943/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/4943.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g4943)
