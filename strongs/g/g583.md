# ἀπογράφω

[apographō](https://www.blueletterbible.org/lexicon/g583)

Definition: tax (3x), write (1x), copy, enter into records, record the names and property and income

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0583)

[Study Light](https://www.studylight.org/lexicons/greek/583.html)

[Bible Hub](https://biblehub.com/str/greek/583.htm)

[LSJ](https://lsj.gr/wiki/ἀπογράφω)

[NET Bible](http://classic.net.bible.org/strong.php?id=583)

[Bible Bento](https://biblebento.com/dictionary/G0583.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0583/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0583.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g583)