# κατέναντι

[katenanti](https://www.blueletterbible.org/lexicon/g2713)

Definition: over against (4x), before (1x).

Part of speech: adverb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2713)

[Study Light](https://www.studylight.org/lexicons/greek/2713.html)

[Bible Hub](https://biblehub.com/str/greek/2713.htm)

[LSJ](https://lsj.gr/wiki/κατέναντι)

[NET Bible](http://classic.net.bible.org/strong.php?id=2713)

[Bible Bento](https://biblebento.com/dictionary/G2713.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2713/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2713.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2713)

