# κῆνσος

[kēnsos](https://www.blueletterbible.org/lexicon/g2778)

Definition: tribute (4x), tax, census (among the Romans, denoting a register and valuation of property in accordance with which taxes were paid), in the NT the tax or tribute levied on individuals and to be paid yearly. (our capitation or poll tax)

Part of speech: masculine noun

Occurs 4 times in 4 verses

Derived words: census

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2778)

[Study Light](https://www.studylight.org/lexicons/greek/2778.html)

[Bible Hub](https://biblehub.com/str/greek/2778.htm)

[LSJ](https://lsj.gr/wiki/κῆνσος)

[NET Bible](http://classic.net.bible.org/strong.php?id=2778)

[Bible Bento](https://biblebento.com/dictionary/G2778.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2778/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2778.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2778)