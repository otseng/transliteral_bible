# κρυσταλλίζω

[krystallizō](https://www.blueletterbible.org/lexicon/g2929)

Definition: clear as crystal (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2929)

[Study Light](https://www.studylight.org/lexicons/greek/2929.html)

[Bible Hub](https://biblehub.com/str/greek/2929.htm)

[LSJ](https://lsj.gr/wiki/κρυσταλλίζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=2929)

[Bible Bento](https://biblebento.com/dictionary/G2929.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2929/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2929.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2929)

