# ἀμύνομαι

[amynomai](https://www.blueletterbible.org/lexicon/g292)

Definition: defend (1x)

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0292)

[Study Light](https://www.studylight.org/lexicons/greek/292.html)

[Bible Hub](https://biblehub.com/str/greek/292.htm)

[LSJ](https://lsj.gr/wiki/ἀμύνομαι)

[NET Bible](http://classic.net.bible.org/strong.php?id=292)

[Bible Bento](https://biblebento.com/dictionary/G0292.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0292/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0292.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g292)
