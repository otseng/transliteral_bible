# μήτε

[mēte](https://www.blueletterbible.org/lexicon/g3383)

Definition: neither (20x), nor (15x), so much as (1x), or (1x).

Part of speech: conjunction

Occurs 37 times in 18 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3383)

[Study Light](https://www.studylight.org/lexicons/greek/3383.html)

[Bible Hub](https://biblehub.com/str/greek/3383.htm)

[LSJ](https://lsj.gr/wiki/μήτε)

[NET Bible](http://classic.net.bible.org/strong.php?id=3383)

[Bible Bento](https://biblebento.com/dictionary/G3383.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3383/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3383.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3383)

