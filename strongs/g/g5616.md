# ὡσεί

[hōsei](https://www.blueletterbible.org/lexicon/g5616)

Definition: about (18x), as (7x), like (5x), as it had been (2x), as it were (1x), like as (1x).

Part of speech: adverb

Occurs 34 times in 34 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g5616)

[Study Light](https://www.studylight.org/lexicons/greek/5616.html)

[Bible Hub](https://biblehub.com/str/greek/5616.htm)

[LSJ](https://lsj.gr/wiki/ὡσεί)

[NET Bible](http://classic.net.bible.org/strong.php?id=5616)

[Bible Bento](https://biblebento.com/dictionary/G5616.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5616/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/5616.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g5616)

