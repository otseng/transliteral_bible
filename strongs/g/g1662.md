# Ἐλιακείμ

[Eliakeim](https://www.blueletterbible.org/lexicon/g1662)

Definition: Eliakim (3x), "raising up by God", the eldest son of Abiud or Judah, bother of Joseph, and father of Azor Mt. 1:13, son of Melea, and father of Jonan Lk 3:30,31

Part of speech: proper masculine noun

Occurs 3 times in 2 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/greek/1662.html)

[LSJ](https://lsj.gr/wiki/Ἐλιακείμ)

[NET Bible](http://classic.net.bible.org/strong.php?id=1662)

[Bible Bento](https://biblebento.com/dictionary/G1662.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1662/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/1662.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g1662)

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g1662)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eliakim.html)

[Video Bible](https://www.videobible.com/bible-dictionary/eliakim)