# ἀνακράζω

[anakrazō](https://www.blueletterbible.org/lexicon/g349)

Definition: cry out (5x), scream

Part of speech: verb

Occurs 5 times in 5 verses

Hebrew: [rûaʿ](../h/h7321.md)

Synonyms: [cry](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Cry)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0349)

[Study Light](https://www.studylight.org/lexicons/greek/349.html)

[Bible Hub](https://biblehub.com/str/greek/349.htm)

[LSJ](https://lsj.gr/wiki/ἀνακράζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=349)

[Bible Bento](https://biblebento.com/dictionary/G0349.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0349/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0349.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g349)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%B1%CE%BD%CE%B1%CE%BA%CF%81%CE%AC%CE%B6%CF%89)