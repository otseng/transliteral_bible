# κέλευσμα

[keleusma](https://www.blueletterbible.org/lexicon/g2752)

Definition: shout (1x), an order, command, spec. a stimulating cry, either that by which animals are roused and urged on by man, as horses by charioteers, hounds by hunters, etc., or that by which a signal is given to men, e.g. to rowers by the master of a ship, to soldiers by a commander (with a loud summons, a trumpet call)

Part of speech: neuter noun

Occurs 1 times in 1 verses

Synonyms: [cry](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Cry)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2752)

[Study Light](https://www.studylight.org/lexicons/greek/2752.html)

[Bible Hub](https://biblehub.com/str/greek/2752.htm)

[LSJ](https://lsj.gr/wiki/κέλευσμα)

[NET Bible](http://classic.net.bible.org/strong.php?id=2752)

[Bible Bento](https://biblebento.com/dictionary/G2752.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2752/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2752.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2752)

[Wiktionary](https://en.wiktionary.org/wiki/%CE%BA%CE%AD%CE%BB%CE%B5%CF%85%CF%83%CE%BC%CE%B1)

[Sermon Index](https://www.sermonindex.net/modules/articles/index.php?view=article&aid=35800)
