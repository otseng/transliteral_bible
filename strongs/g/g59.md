# ἀγοράζω

[agorazō](https://www.blueletterbible.org/lexicon/g59)

Definition: buy (28x), redeem (3x), to be in the market place, to attend it, to go to market, buy a slave at the agora

Part of speech: verb

Occurs 31 times in 31 verses

Hebrew: [pāḏâ](../h/h6299.md)

Synonyms: [redeem](https://simple.uniquebibleapp.com/book/Synonyms/Greek#Redeem)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g0059)

[Study Light](https://www.studylight.org/lexicons/greek/59.html)

[Bible Hub](https://biblehub.com/str/greek/59.htm)

[LSJ](https://lsj.gr/wiki/ἀγοράζω)

[NET Bible](http://classic.net.bible.org/strong.php?id=59)

[Bible Bento](https://biblebento.com/dictionary/G0059.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0059/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/0059.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g59)

[Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=33645)

[Precept Austin](https://www.preceptaustin.org/1corinthians_620_commentary#b)

[Resounding the Faith](http://resoundingthefaith.com/2018/04/%E2%80%8Egreek-%E1%BC%80%CE%B3%CE%BF%CF%81%CE%AC%CE%B6%CF%89-agorazo/)