# λεῖος

[leios](https://www.blueletterbible.org/lexicon/g3006)

Definition: smooth (1x), level

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3006)

[Study Light](https://www.studylight.org/lexicons/greek/3006.html)

[Bible Hub](https://biblehub.com/str/greek/3006.htm)

[LSJ](https://lsj.gr/wiki/λεῖος)

[NET Bible](http://classic.net.bible.org/strong.php?id=3006)

[Bible Bento](https://biblebento.com/dictionary/G3006.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3006/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3006.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3006)