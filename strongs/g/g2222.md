# ζωή

[zōē](https://www.blueletterbible.org/lexicon/g2222)

Definition: life (133x), lifetime (1x), fulness of life, real life

Part of speech: feminine noun

Occurs 134 times in 126 verses

Hebrew: [chay](../h/h2416.md)

Derived words: zoology

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g2222)

[Study Light](https://www.studylight.org/lexicons/greek/2222.html)

[Bible Hub](https://biblehub.com/str/greek/2222.htm)

[LSJ](https://lsj.gr/wiki/ζωή)

[NET Bible](http://classic.net.bible.org/strong.php?id=2222)

[Bible Bento](https://biblebento.com/dictionary/G2222.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2222/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/2222.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g2222)

[Logos Apostolic](https://www.logosapostolic.org/greek-word-studies/2222-zoe-life.htm)
