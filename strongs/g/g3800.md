# ὀψώνιον

[opsōnion](https://www.blueletterbible.org/lexicon/g3800)

Definition: wage (3x), charges (1x), a soldier's pay, allowance, rations

Part of speech: neuter noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g3800)

[Study Light](https://www.studylight.org/lexicons/greek/3800.html)

[Bible Hub](https://biblehub.com/str/greek/3800.htm)

[LSJ](https://lsj.gr/wiki/ὀψώνιον)

[NET Bible](http://classic.net.bible.org/strong.php?id=3800)

[Bible Bento](https://biblebento.com/dictionary/G3800.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3800/greek)

[Lexicon Concordance](http://lexiconcordance.com/greek/3800.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/g3800)

[Wiktionary](https://en.wiktionary.org/wiki/%E1%BD%80%CF%88%CF%8E%CE%BD%CE%B9%CE%BF%CE%BD)