# שַׁאֲרָה

[ša'ărâ](https://www.blueletterbible.org/lexicon/h7608)

Definition: near kinswoman (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7608)

[Study Light](https://www.studylight.org/lexicons/hebrew/7608.html)

[Bible Hub](https://biblehub.com/str/hebrew/7608.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07608)

[Bible Bento](https://biblebento.com/dictionary/H7608.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7608/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7608.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7608)
