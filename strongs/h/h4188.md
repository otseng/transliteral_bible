# מוּשִׁי

[Mûšî](https://www.blueletterbible.org/lexicon/h4188)

Definition: Mushites (2x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4188)

[Study Light](https://www.studylight.org/lexicons/hebrew/4188.html)

[Bible Hub](https://biblehub.com/str/hebrew/4188.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04188)

[Bible Bento](https://biblebento.com/dictionary/H4188.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4188/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4188.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4188)
