# נְפִיסִים

[nᵊp̄îsîm](https://www.blueletterbible.org/lexicon/h5304)

Definition: Nephusim (2x), "scattered spices"

Part of speech: proper plural noun

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5304)

[Study Light](https://www.studylight.org/lexicons/hebrew/5304.html)

[Bible Hub](https://biblehub.com/str/hebrew/5304.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05304)

[Bible Bento](https://biblebento.com/dictionary/H5304.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5304/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5304.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5304)
