# יוֹסֵף

[Yôsēp̄](https://www.blueletterbible.org/lexicon/h3130)

Definition: Joseph (213x), "Jehovah has added"

Part of speech: proper masculine noun

Occurs 213 times in 193 verses

Hebrew: [yāsap̄](../h/h3254.md)

Greek: [Iōsēph](../g/g2501.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3130)

[Study Light](https://www.studylight.org/lexicons/hebrew/3130.html)

[Bible Hub](https://biblehub.com/str/hebrew/3130.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03130)

[Bible Bento](https://biblebento.com/dictionary/H3130.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3130/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3130.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3130)

[Wikipedia](https://en.wikipedia.org/wiki/Joseph_%28Genesis%29)

[Britannica](https://www.britannica.com/biography/Joseph-biblical-figure)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/joseph.html)

[Video Bible](https://www.videobible.com/bible-dictionary/joseph)