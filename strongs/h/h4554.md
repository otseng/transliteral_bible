# מִסְפּוֹא

[mispô'](https://www.blueletterbible.org/lexicon/h4554)

Definition: provender (5x), fodder, feed

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4554)

[Study Light](https://www.studylight.org/lexicons/hebrew/4554.html)

[Bible Hub](https://biblehub.com/str/hebrew/4554.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04554)

[Bible Bento](https://biblebento.com/dictionary/H4554.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4554/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4554.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4554)
