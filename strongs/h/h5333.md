# נְצִיב

[nᵊṣîḇ](https://www.blueletterbible.org/lexicon/h5333)

Definition: garrison (9x), officer (1x), pillar (1x), variant (1x).

Part of speech: masculine noun

Occurs 12 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5333)

[Study Light](https://www.studylight.org/lexicons/hebrew/5333.html)

[Bible Hub](https://biblehub.com/str/hebrew/5333.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05333)

[Bible Bento](https://biblebento.com/dictionary/H5333.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5333/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5333.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5333)
