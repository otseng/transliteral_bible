# נָאוֶה

[nā'vê](https://www.blueletterbible.org/lexicon/h5000)

Definition: comely (6x), seemly (2x), becometh (1x).

Part of speech: adjective

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5000)

[Study Light](https://www.studylight.org/lexicons/hebrew/5000.html)

[Bible Hub](https://biblehub.com/str/hebrew/5000.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05000)

[Bible Bento](https://biblebento.com/dictionary/H5000.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5000/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5000.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5000)
