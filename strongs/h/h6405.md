# פַּלֵּט

[pallēṭ](https://www.blueletterbible.org/lexicon/h6405)

Definition: deliverance (1x), escape (3x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

Hebrew: [mip̄lāṭ](../h/h4655.md), [palat](../h/h6403.md), [pālîṭ](../h/h6412.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6405)

[Study Light](https://www.studylight.org/lexicons/hebrew/6405.html)

[Bible Hub](https://biblehub.com/str/hebrew/6405.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06405)

[Bible Bento](https://biblebento.com/dictionary/H6405.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6405/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6405.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6405)
