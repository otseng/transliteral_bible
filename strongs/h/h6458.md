# פָּסַל

[pāsal](https://www.blueletterbible.org/lexicon/h6458)

Definition: hew (5x), graven (1x).

Part of speech: verb

Occurs 6 times in 6 verses

Hebrew: [pāsîl](../h/h6456.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6458)

[Study Light](https://www.studylight.org/lexicons/hebrew/6458.html)

[Bible Hub](https://biblehub.com/str/hebrew/6458.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06458)

[Bible Bento](https://biblebento.com/dictionary/H6458.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6458/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6458.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6458)
