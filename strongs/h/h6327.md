# פּוּץ

[puwts](https://www.blueletterbible.org/lexicon/h6327)

Definition: scatter (48x), scatter abroad (6x), disperse (3x), spread abroad (2x), cast abroad (2x), drive (1x), break to pieces (1x), shake to pieces (1x), dash to pieces (1x), retired (1x).

Part of speech: verb

Occurs 67 times in 66 verses

Hebrew: [tᵊp̄ôṣâ](../h/h8600.md)

Greek: [diaspeirō](../g/g1289.md)

Edenics: shot put

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6327)

[Study Light](https://www.studylight.org/lexicons/hebrew/6327.html)

[Bible Hub](https://biblehub.com/str/hebrew/6327.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06327)

[Bible Bento](https://biblebento.com/dictionary/H6327.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6327/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6327.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6327)
