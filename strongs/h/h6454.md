# פָּסֵחַ

[Pāsēaḥ](https://www.blueletterbible.org/lexicon/h6454)

Definition: Paseah (3x), Phaseah (1x).

Part of speech: proper masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6454)

[Study Light](https://www.studylight.org/lexicons/hebrew/6454.html)

[Bible Hub](https://biblehub.com/str/hebrew/6454.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06454)

[Bible Bento](https://biblebento.com/dictionary/H6454.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6454/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6454.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6454)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/paseah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/paseah)