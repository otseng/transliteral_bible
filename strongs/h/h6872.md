# צְרוֹר

[ṣᵊrôr](https://www.blueletterbible.org/lexicon/h6872)

Definition: bundle (4x), bag (3x), bindeth (1x), grain (1x), stone (1x), Zeror (1x).

Part of speech: masculine noun

Occurs 10 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6872)

[Study Light](https://www.studylight.org/lexicons/hebrew/6872.html)

[Bible Hub](https://biblehub.com/str/hebrew/6872.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06872)

[Bible Bento](https://biblebento.com/dictionary/H6872.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6872/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6872.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6872)
