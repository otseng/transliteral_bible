# עָנָו

[`anav](https://www.blueletterbible.org/lexicon/h6035)

Definition: meek (13x), humble (5x), poor (5x), lowly (2x), very meek (1x), depressed, unpretentious

Part of speech: masculine noun

Occurs 26 times in 24 verses

Hebrew: [ʿānâ](../h/h6031.md), [ʿănāvâ](../h/h6038.md)

Greek: [praos](../g/g4235.md), [penēs](../g/g3993.md), [tapeinoō](../g/g5013.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6035)

[Study Light](https://www.studylight.org/lexicons/hebrew/6035.html)

[Bible Hub](https://biblehub.com/str/hebrew/6035.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A2%D6%B8%D7%A0%D6%B8%D7%95)

[NET Bible](http://classic.net.bible.org/strong.php?id=06035)

[Bible Bento](https://biblebento.com/dictionary/H6035.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6035/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6035.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6035)
