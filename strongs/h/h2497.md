# חֵלֹן

[ḥēlōn](https://www.blueletterbible.org/lexicon/h2497)

Definition: Helon (5x).

Part of speech: proper masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2497)

[Study Light](https://www.studylight.org/lexicons/hebrew/2497.html)

[Bible Hub](https://biblehub.com/str/hebrew/2497.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02497)

[Bible Bento](https://biblebento.com/dictionary/H2497.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2497/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2497.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2497)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/helon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/helon)