# חַרְשָׁא

[Ḥaršā'](https://www.blueletterbible.org/lexicon/h2797)

Definition: Harsha (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2797)

[Study Light](https://www.studylight.org/lexicons/hebrew/2797.html)

[Bible Hub](https://biblehub.com/str/hebrew/2797.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02797)

[Bible Bento](https://biblebento.com/dictionary/H2797.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2797/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2797.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2797)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/harsha.html)

[Video Bible](https://www.videobible.com/bible-dictionary/harsha)