# יִבְנְיָה

[Yiḇnᵊyâ](https://www.blueletterbible.org/lexicon/h2997)

Definition: Ibneiah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2997)

[Study Light](https://www.studylight.org/lexicons/hebrew/2997.html)

[Bible Hub](https://biblehub.com/str/hebrew/2997.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02997)

[Bible Bento](https://biblebento.com/dictionary/H2997.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2997/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2997.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2997)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/ibneiah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ibneiah)