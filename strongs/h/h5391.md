# נָשַׁךְ

[nāšaḵ](https://www.blueletterbible.org/lexicon/h5391)

Definition: bite (14x), lend upon usury (2x).

Part of speech: verb

Occurs 16 times in 14 verses

Greek: [daknō](../g/g1143.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5391)

[Study Light](https://www.studylight.org/lexicons/hebrew/5391.html)

[Bible Hub](https://biblehub.com/str/hebrew/5391.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05391)

[Bible Bento](https://biblebento.com/dictionary/H5391.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5391/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5391.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5391)
