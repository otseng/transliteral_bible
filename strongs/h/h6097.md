# עֵצָה

[ʿēṣâ](https://www.blueletterbible.org/lexicon/h6097)

Definition: tree (1x).

Part of speech: collective feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6097)

[Study Light](https://www.studylight.org/lexicons/hebrew/6097.html)

[Bible Hub](https://biblehub.com/str/hebrew/6097.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06097)

[Bible Bento](https://biblebento.com/dictionary/H6097.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6097/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6097.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6097)
