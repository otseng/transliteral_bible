# מֶגְרָפָה

[meḡrāp̄â](https://www.blueletterbible.org/lexicon/h4053)

Definition: clod (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4053)

[Study Light](https://www.studylight.org/lexicons/hebrew/4053.html)

[Bible Hub](https://biblehub.com/str/hebrew/4053.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04053)

[Bible Bento](https://biblebento.com/dictionary/H4053.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4053/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4053.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4053)
