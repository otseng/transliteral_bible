# חֶלְדַּי

[Ḥelday](https://www.blueletterbible.org/lexicon/h2469)

Definition: Heldai (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2469)

[Study Light](https://www.studylight.org/lexicons/hebrew/2469.html)

[Bible Hub](https://biblehub.com/str/hebrew/2469.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02469)

[Bible Bento](https://biblebento.com/dictionary/H2469.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2469/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2469.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2469)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/heldai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/heldai)