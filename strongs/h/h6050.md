# עֲנָן

[ʿănān](https://www.blueletterbible.org/lexicon/h6050)

Definition: cloud (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6050)

[Study Light](https://www.studylight.org/lexicons/hebrew/6050.html)

[Bible Hub](https://biblehub.com/str/hebrew/6050.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06050)

[Bible Bento](https://biblebento.com/dictionary/H6050.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6050/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6050.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6050)
