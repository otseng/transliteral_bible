# עוּת

[ʿûṯ](https://www.blueletterbible.org/lexicon/h5790)

Definition: speak (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5790)

[Study Light](https://www.studylight.org/lexicons/hebrew/5790.html)

[Bible Hub](https://biblehub.com/str/hebrew/5790.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05790)

[Bible Bento](https://biblebento.com/dictionary/H5790.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5790/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5790.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5790)
