# פֶּגַע

[peḡaʿ](https://www.blueletterbible.org/lexicon/h6294)

Definition: occurrent (1x), chance (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

Hebrew: [pāḡaʿ](../h/h6293.md)

Names: [Paḡʿî'ēl](../h/h6295.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6294)

[Study Light](https://www.studylight.org/lexicons/hebrew/6294.html)

[Bible Hub](https://biblehub.com/str/hebrew/6294.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06294)

[Bible Bento](https://biblebento.com/dictionary/H6294.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6294/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6294.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6294)
