# אָפִיל

['āp̄îl](https://www.blueletterbible.org/lexicon/h648)

Definition: grown up (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h648)

[Study Light](https://www.studylight.org/lexicons/hebrew/648.html)

[Bible Hub](https://biblehub.com/str/hebrew/648.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0648)

[Bible Bento](https://biblebento.com/dictionary/H648.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/648/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/648.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h648)
