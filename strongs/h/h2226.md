# זֶרַח

[Zeraḥ](https://www.blueletterbible.org/lexicon/h2226)

Definition: Zerah (20x), Zarah (1x).

Part of speech: proper masculine noun

Occurs 21 times in 21 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2226)

[Study Light](https://www.studylight.org/lexicons/hebrew/2226.html)

[Bible Hub](https://biblehub.com/str/hebrew/2226.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02226)

[Bible Bento](https://biblebento.com/dictionary/H2226.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2226/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2226.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2226)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zerah.html)