# חֵפֶשׂ

[ḥēp̄eś](https://www.blueletterbible.org/lexicon/h2665)

Definition: a search (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2665)

[Study Light](https://www.studylight.org/lexicons/hebrew/2665.html)

[Bible Hub](https://biblehub.com/str/hebrew/2665.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02665)

[Bible Bento](https://biblebento.com/dictionary/H2665.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2665/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2665.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2665)
