# שָׁדַד

[shadad](https://www.blueletterbible.org/lexicon/h7703)

Definition: spoil (30x), spoiler (11x), waste (8x), destroy (2x), robbers (2x), miscellaneous (5x), plunder, overpower, make desolate

Part of speech: verb

Occurs 58 times in 47 verses

Greek: [diarpazō](../g/g1283.md), [erēmoō](../g/g2049.md)

Synonyms: [destroy](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Destroy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7703)

[Study Light](https://www.studylight.org/lexicons/hebrew/7703.html)

[Bible Hub](https://biblehub.com/str/hebrew/7703.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07703)

[Bible Bento](https://biblebento.com/dictionary/H7703.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7703/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7703.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7703)
