# רָחוֹק

[rachowq](https://www.blueletterbible.org/lexicon/h7350)

Definition: (far, afar...) off (39x), far (30x), long ago (3x), far from (3x), come (2x), afar (2x), old (2x), far abroad (1x), long (1x), space (1x), distant

Part of speech: adjective, masculine noun

Occurs 84 times in 84 verses

Hebrew: [merḥāq](../h/h4801.md), [rachaq](../h/h7368.md), [rāḥēq](../h/h7369.md)

Greek: [makran](../g/g3112.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7350)

[Study Light](https://www.studylight.org/lexicons/hebrew/7350.html)

[Bible Hub](https://biblehub.com/str/hebrew/7350.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A8%D6%B8%D7%97%D7%95%D6%B9%D7%A7)

[NET Bible](http://classic.net.bible.org/strong.php?id=07350)

[Bible Bento](https://biblebento.com/dictionary/H7350.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7350/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7350.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7350)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A8%D7%97%D7%95%D7%A7)
