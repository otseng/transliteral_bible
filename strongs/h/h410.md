# אֵל

['el](https://www.blueletterbible.org/lexicon/h410)

Definition: God (213x), god (16x), power (4x), mighty (5x), goodly (1x), great (1x), idols (1x), Immanuel (with H6005) (2x), might (1x), strong (1x)

Part of speech: masculine noun

Occurs 245 times in 235 verses

Hebrew: ['ayil](../h/h352.md)

Names: [Yisra'el](../h/h3478.md), ['Ĕlîšāʿ](../h/h477.md), [Yᵊḥezqē'L](../h/h3168.md), [Dinîyē'L](../h/h1840.md), [Gaḇrî'Ēl](../h/h1403.md), [Mîḵā'ēl](../h/h4317.md), [Yô'Ēl](../h/h3100.md)

Places: [YizrᵊʿE'L](../h/h3157.md)

Greek: [theos](../g/g2316.md)

Synonyms: [God](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#God), [mighty](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Mighty)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0410)

[Study Light](https://www.studylight.org/lexicons/hebrew/410.html)

[Bible Hub](https://biblehub.com/str/hebrew/410.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%90%D7%9C)

[NET Bible](http://classic.net.bible.org/strong.php?id=0410)

[Bible Bento](https://biblebento.com/dictionary/H0410.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0410/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0410.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h410)
