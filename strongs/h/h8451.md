# תּוֹרָה

[towrah](https://www.blueletterbible.org/lexicon/h8451)

Definition: law (219x), direction, instruction, custom, manner

Part of speech: feminine noun

Occurs 219 times in 213 

Hebrew: [yārâ](../h/h3384.md)

Greek: [nomos](../g/g3551.md)

Usage: Torah

Synonyms: [law](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Law)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8451)

[Study Light](https://www.studylight.org/lexicons/hebrew/8451.html)

[Bible Hub](https://biblehub.com/str/hebrew/8451.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08451)

[Bible Bento](https://biblebento.com/dictionary/H8451.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8451/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8451.html)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%AA%D6%BC%D7%95%D7%A8%D7%94)

[Wikipedia](https://en.wikipedia.org/wiki/Torah)

[My Jewish Learning](https://www.myjewishlearning.com/article/the-torah/)

[Britannica](https://www.britannica.com/topic/Torah)

[Jewish Virtual Library](https://www.jewishvirtuallibrary.org/the-written-law-torah)

[Judaism 101](http://www.jewfaq.org/torah.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/1426382/jewish/Torah.htm)

[Jewish Encyclopedia](http://www.jewishencyclopedia.com/articles/14446-torah)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8451)

[Logos Apostolic](https://www.logosapostolic.org/hebrew-word-studies/8451-torah-law.htm)

[119 Ministries](https://www.119ministries.com/119-blog/commandments-statutes-ordinances-and-judgmentswhats-the-difference/)