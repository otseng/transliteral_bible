# חָכְמוֹת

[ḥāḵmôṯ](https://www.blueletterbible.org/lexicon/h2454)

Definition: wisdom (4x), every wise (1x).

Part of speech: feminine noun

Occurs 5 times in 5 verses

Hebrew: [ḥakîm](../h/h2445.md), [ḥāḵam](../h/h2449.md), [ḥāḵām](../h/h2450.md), [ḥāḵmâ](../h/h2451.md), [ḥāḵmâ](../h/h2452.md)

Greek: [sophia](../g/g4678.md)

Edenics: acumen, hook, hokum, hocus

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2454)

[Study Light](https://www.studylight.org/lexicons/hebrew/2454.html)

[Bible Hub](https://biblehub.com/str/hebrew/2454.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02454)

[Bible Bento](https://biblebento.com/dictionary/H2454.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2454/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2454.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2454)
