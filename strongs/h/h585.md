# אֲנָחָה

['anachah](https://www.blueletterbible.org/lexicon/h585)

Definition: sighing (5x), groanings (4x), sighs (1x), mourning (1x).

Part of speech: feminine noun

Occurs 11 times in 11 verses

Hebrew: ['ānaḥ](../h/h584.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0585)

[Study Light](https://www.studylight.org/lexicons/hebrew/585.html)

[Bible Hub](https://biblehub.com/str/hebrew/585.htm)

[Morfix](https://www.morfix.co.il/en/%D7%90%D6%B2%D7%A0%D6%B8%D7%97%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=0585)

[Bible Bento](https://biblebento.com/dictionary/H0585.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0585/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0585.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h585)
