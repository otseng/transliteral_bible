# סַל

[sal](https://www.blueletterbible.org/lexicon/h5536)

Definition: basket (15x).

Part of speech: masculine noun

Occurs 15 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5536)

[Study Light](https://www.studylight.org/lexicons/hebrew/5536.html)

[Bible Hub](https://biblehub.com/str/hebrew/5536.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05536)

[Bible Bento](https://biblebento.com/dictionary/H5536.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5536/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5536.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5536)
