# עָנִי

[`aniy](https://www.blueletterbible.org/lexicon/h6041)

Definition: poor (58x), afflicted (15x), lowly (1x), man (1x), variant (3x), humble

Part of speech: adjective

Occurs 80 times in 78 verses

Hebrew: [ʿānâ](../h/h6031.md), [`oniy](../h/h6040.md)

Greek: [penēs](../g/g3993.md), [tapeinos](../g/g5011.md)

Edenics: alms

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6041)

[Study Light](https://www.studylight.org/lexicons/hebrew/6041.html)

[Bible Hub](https://biblehub.com/str/hebrew/6041.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A2%D6%B8%D7%A0%D6%B4%D7%99)

[NET Bible](http://classic.net.bible.org/strong.php?id=06041)

[Bible Bento](https://biblebento.com/dictionary/H6041.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6041/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6041.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6041)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A2%D7%A0%D7%99)
