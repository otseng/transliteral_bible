# חֶלְקִי

[ḥelqî](https://www.blueletterbible.org/lexicon/h2516)

Definition: Helekites (1x).

Part of speech: masculine patrial noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2516)

[Study Light](https://www.studylight.org/lexicons/hebrew/2516.html)

[Bible Hub](https://biblehub.com/str/hebrew/2516.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02516)

[Bible Bento](https://biblebento.com/dictionary/H2516.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2516/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2516.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2516)
