# אֲבִידָן

['ăḇîḏān](https://www.blueletterbible.org/lexicon/h27)

Definition: Abidan (5x).

Part of speech: proper masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h27)

[Study Light](https://www.studylight.org/lexicons/hebrew/27.html)

[Bible Hub](https://biblehub.com/str/hebrew/27.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=027)

[Bible Bento](https://biblebento.com/dictionary/H27.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/27/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/27.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h27)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/abidan.html)

[Video Bible](https://www.videobible.com/bible-dictionary/abidan)