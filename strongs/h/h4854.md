# מַשָּׂא

[maśśā'](https://www.blueletterbible.org/lexicon/h4854)

Definition: Massa (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4854)

[Study Light](https://www.studylight.org/lexicons/hebrew/4854.html)

[Bible Hub](https://biblehub.com/str/hebrew/4854.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04854)

[Bible Bento](https://biblebento.com/dictionary/H4854.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4854/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4854.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4854)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/massa.html)

[Video Bible](https://www.videobible.com/bible-dictionary/massa)