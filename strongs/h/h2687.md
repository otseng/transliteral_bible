# חָצָץ

[ḥāṣāṣ](https://www.blueletterbible.org/lexicon/h2687)

Definition: gravel (1x), gravel stones (1x), arrows (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

Hebrew: [chets](../h/h2671.md), [ḥāṣaṣ](../h/h2686.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2687)

[Study Light](https://www.studylight.org/lexicons/hebrew/2687.html)

[Bible Hub](https://biblehub.com/str/hebrew/2687.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02687)

[Bible Bento](https://biblebento.com/dictionary/H2687.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2687/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2687.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2687)
