# שֵׁנִי

[šēnî](https://www.blueletterbible.org/lexicon/h8145)

Definition: second (87x), other (37x), time (13x), again (7x), another (7x), more (3x), either (1x), second rank (1x).

Part of speech: adjective, masculine/feminine noun

Occurs 157 times in 152 verses

Greek: [deuteros](../g/g1208.md)

Edenics: second

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8145)

[Study Light](https://www.studylight.org/lexicons/hebrew/8145.html)

[Bible Hub](https://biblehub.com/str/hebrew/8145.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08145)

[Bible Bento](https://biblebento.com/dictionary/H8145.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8145/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8145.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8145)
