# פִּצֵּץ

[Piṣṣēṣ](https://www.blueletterbible.org/lexicon/h6483)

Definition: Aphses (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6483)

[Study Light](https://www.studylight.org/lexicons/hebrew/6483.html)

[Bible Hub](https://biblehub.com/str/hebrew/6483.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06483)

[Bible Bento](https://biblebento.com/dictionary/H6483.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6483/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6483.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6483)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/aphses.html)