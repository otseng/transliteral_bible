# אֹחַ

['ōaḥ](https://www.blueletterbible.org/lexicon/h255)

Definition: doleful creatures (1x), jackal, hyena

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0255)

[Study Light](https://www.studylight.org/lexicons/hebrew/255.html)

[Bible Hub](https://biblehub.com/str/hebrew/255.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0255)

[Bible Bento](https://biblebento.com/dictionary/H255.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/255/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/255.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h255)
