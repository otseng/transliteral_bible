# רָחֵל

[rāḥēl](https://www.blueletterbible.org/lexicon/h7353)

Definition: ewe (2x), sheep (2x).

Part of speech: feminine noun

Occurs 4 times in 4 verses

Names: [Rāḥēl](../h/h7354.md)

Greek: [amnos](../g/g286.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7353)

[Study Light](https://www.studylight.org/lexicons/hebrew/7353.html)

[Bible Hub](https://biblehub.com/str/hebrew/7353.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07353)

[Bible Bento](https://biblebento.com/dictionary/H7353.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7353/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7353.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7353)
