# יְדִדוּת

[yᵊḏiḏûṯ](https://www.blueletterbible.org/lexicon/h3033)

Definition: dearly beloved (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3033)

[Study Light](https://www.studylight.org/lexicons/hebrew/3033.html)

[Bible Hub](https://biblehub.com/str/hebrew/3033.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03033)

[Bible Bento](https://biblebento.com/dictionary/H3033.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3033/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3033.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3033)
