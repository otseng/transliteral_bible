# מוֹקֵד

[môqēḏ](https://www.blueletterbible.org/lexicon/h4168)

Definition: hearth (1x), burnings (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4168)

[Study Light](https://www.studylight.org/lexicons/hebrew/4168.html)

[Bible Hub](https://biblehub.com/str/hebrew/4168.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04168)

[Bible Bento](https://biblebento.com/dictionary/H4168.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4168/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4168.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4168)
