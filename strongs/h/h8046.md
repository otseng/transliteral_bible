# שְׁמַד

[šᵊmaḏ](https://www.blueletterbible.org/lexicon/h8046)

Definition: consume (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [šāmaḏ](../h/h8045.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8046)

[Study Light](https://www.studylight.org/lexicons/hebrew/8046.html)

[Bible Hub](https://biblehub.com/str/hebrew/8046.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08046)

[Bible Bento](https://biblebento.com/dictionary/H8046.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8046/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8046.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8046)
