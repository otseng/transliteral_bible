# יִשַׁי

[Yišay](https://www.blueletterbible.org/lexicon/h3448)

Definition: Jesse (42x), "possess"

Part of speech: proper masculine noun

Occurs 42 times in 39 verses

Hebrew: [yēš](../h/h3426.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3448)

[Study Light](https://www.studylight.org/lexicons/hebrew/3448.html)

[Bible Hub](https://biblehub.com/str/hebrew/3448.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03448)

[Bible Bento](https://biblebento.com/dictionary/H3448.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3448/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3448.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3448)

[Wikipedia](https://en.wikipedia.org/wiki/Jesse_%28biblical_figure%29)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jesse.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jesse)