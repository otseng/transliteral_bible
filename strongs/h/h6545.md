# פֶּרַע

[peraʿ](https://www.blueletterbible.org/lexicon/h6545)

Definition: lock (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6545)

[Study Light](https://www.studylight.org/lexicons/hebrew/6545.html)

[Bible Hub](https://biblehub.com/str/hebrew/6545.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06545)

[Bible Bento](https://biblebento.com/dictionary/H6545.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6545/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6545.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6545)
