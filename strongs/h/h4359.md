# מִכְלָל

[miḵlāl](https://www.blueletterbible.org/lexicon/h4359)

Definition: perfection (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

Hebrew: [kālîl](../h/h3632.md), [kālal](../h/h3634.md)

## Articles

[Study Light](https://www.studylight.org/lexicons/hebrew/4359.html)

[Bible Hub](https://biblehub.com/str/hebrew/4359.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04359)

[Bible Bento](https://biblebento.com/dictionary/H4359.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4359/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4359.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4359)
