# יַלְקוּט

[yalqûṭ](https://www.blueletterbible.org/lexicon/h3219)

Definition: scrip (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3219)

[Study Light](https://www.studylight.org/lexicons/hebrew/3219.html)

[Bible Hub](https://biblehub.com/str/hebrew/3219.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03219)

[Bible Bento](https://biblebento.com/dictionary/H3219.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3219/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3219.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3219)
