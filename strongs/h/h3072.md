# יְהֹוָה צִדְקֵנוּ

[Yᵊhōvâ Ṣiḏqēnû](https://www.blueletterbible.org/lexicon/h3072)

Definition: LORD our Righteousness (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

Synonyms: [God](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#God)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3072)

[Study Light](https://www.studylight.org/lexicons/hebrew/3072.html)

[Bible Hub](https://biblehub.com/str/hebrew/3072.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03072)

[Bible Bento](https://biblebento.com/dictionary/H3072.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3072/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3072.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3072)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/L/lord.html)

[Video Bible](https://www.videobible.com/bible-dictionary/lord)