# הָרַס

[harac](https://www.blueletterbible.org/lexicon/h2040)

Definition: throw down (13x), break down (9x), overthrow (5x), destroy (4x), pull down (3x), break through (2x), ruined (2x), beat down (1x), pluck down (1x), break (1x), destroyers (1x), utterly (1x).

Part of speech: verb

Occurs 43 times in 42 verses

Greek: [kataballō](../g/g2598.md)

Synonyms: [destroy](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Destroy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2040)

[Study Light](https://www.studylight.org/lexicons/hebrew/2040.html)

[Bible Hub](https://biblehub.com/str/hebrew/2040.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02040)

[Bible Bento](https://biblebento.com/dictionary/H2040.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2040/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2040.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2040)
