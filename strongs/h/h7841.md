# שְׁחַרְיָה

[Šᵊḥaryâ](https://www.blueletterbible.org/lexicon/h7841)

Definition: Shehariah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7841)

[Study Light](https://www.studylight.org/lexicons/hebrew/7841.html)

[Bible Hub](https://biblehub.com/str/hebrew/7841.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07841)

[Bible Bento](https://biblebento.com/dictionary/H7841.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7841/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7841.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7841)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shehariah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shehariah)