# כָּתַשׁ

[kāṯaš](https://www.blueletterbible.org/lexicon/h3806)

Definition: bray (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3806)

[Study Light](https://www.studylight.org/lexicons/hebrew/3806.html)

[Bible Hub](https://biblehub.com/str/hebrew/3806.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03806)

[Bible Bento](https://biblebento.com/dictionary/H3806.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3806/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3806.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3806)
