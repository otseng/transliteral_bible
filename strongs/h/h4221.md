# מֹחַ

[mōaḥ](https://www.blueletterbible.org/lexicon/h4221)

Definition: marrow (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4221)

[Study Light](https://www.studylight.org/lexicons/hebrew/4221.html)

[Bible Hub](https://biblehub.com/str/hebrew/4221.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04221)

[Bible Bento](https://biblebento.com/dictionary/H4221.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4221/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4221.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4221)
