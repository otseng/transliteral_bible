# יָעַף

[yāʿap̄](https://www.blueletterbible.org/lexicon/h3286)

Definition: weary (4x), faint (4x), fly (1x).

Part of speech: verb

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3286)

[Study Light](https://www.studylight.org/lexicons/hebrew/3286.html)

[Bible Hub](https://biblehub.com/str/hebrew/3286.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03286)

[Bible Bento](https://biblebento.com/dictionary/H3286.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3286/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3286.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3286)
