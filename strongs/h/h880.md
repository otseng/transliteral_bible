# בְּאֵרָה

[Bᵊ'Ērâ](https://www.blueletterbible.org/lexicon/h880)

Definition: Beerah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h880)

[Study Light](https://www.studylight.org/lexicons/hebrew/880.html)

[Bible Hub](https://biblehub.com/str/hebrew/880.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0880)

[Bible Bento](https://biblebento.com/dictionary/H880.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/880/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/880.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h880)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/beerah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/beerah)