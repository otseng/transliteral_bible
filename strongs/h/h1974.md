# הִלּוּל

[hillûl](https://www.blueletterbible.org/lexicon/h1974)

Definition: praise (1x), make merry (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

Synonyms: [praise](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Praise)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1974)

[Study Light](https://www.studylight.org/lexicons/hebrew/1974.html)

[Bible Hub](https://biblehub.com/str/hebrew/1974.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01974)

[Bible Bento](https://biblebento.com/dictionary/H1974.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1974/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1974.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1974)
