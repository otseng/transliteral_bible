# בָּרוּת

[bārûṯ](https://www.blueletterbible.org/lexicon/h1267)

Definition: meat (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1267)

[Study Light](https://www.studylight.org/lexicons/hebrew/1267.html)

[Bible Hub](https://biblehub.com/str/hebrew/1267.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01267)

[Bible Bento](https://biblebento.com/dictionary/H1267.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1267/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1267.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1267)
