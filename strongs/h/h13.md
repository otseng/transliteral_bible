# אָבְדָן

['āḇḏān](https://www.blueletterbible.org/lexicon/h13)

Definition: destruction (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

Hebrew: ['abad](../h/h6.md), ['ōḇēḏ](../h/h8.md), ['ăḇēḏâ](../h/h9.md), ['Ăḇadôn](../h/h11.md), [pîḏ](../h/h6365.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h13)

[Study Light](https://www.studylight.org/lexicons/hebrew/13.html)

[Bible Hub](https://biblehub.com/str/hebrew/13.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=013)

[Bible Bento](https://biblebento.com/dictionary/H13.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/13/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/13.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h13)
