# אַרְנֶבֶת

['arneḇeṯ](https://www.blueletterbible.org/lexicon/h768)

Definition: hare (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h768)

[Study Light](https://www.studylight.org/lexicons/hebrew/768.html)

[Bible Hub](https://biblehub.com/str/hebrew/768.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0768)

[Bible Bento](https://biblebento.com/dictionary/H768.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/768/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/768.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h768)
