# צֹעַן

[Ṣōʿan](https://www.blueletterbible.org/lexicon/h6814)

Definition: Zoan (7x).

Part of speech: proper locative noun

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6814)

[Study Light](https://www.studylight.org/lexicons/hebrew/6814.html)

[Bible Hub](https://biblehub.com/str/hebrew/6814.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06814)

[Bible Bento](https://biblebento.com/dictionary/H6814.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6814/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6814.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6814)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zoan.html)

[Video Bible](https://www.videobible.com/bible-dictionary/zoan)