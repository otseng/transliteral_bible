# יוֹעֵד

[YôʿĒḏ](https://www.blueletterbible.org/lexicon/h3133)

Definition: Joed (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3133)

[Study Light](https://www.studylight.org/lexicons/hebrew/3133.html)

[Bible Hub](https://biblehub.com/str/hebrew/3133.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03133)

[Bible Bento](https://biblebento.com/dictionary/H3133.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3133/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3133.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3133)

[Video Bible](https://www.videobible.com/bible-dictionary/joed)