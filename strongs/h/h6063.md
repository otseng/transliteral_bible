# עָנֵר

[ʿānēr](https://www.blueletterbible.org/lexicon/h6063)

Definition: Aner (3x).

Part of speech: proper locative noun, proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6063)

[Study Light](https://www.studylight.org/lexicons/hebrew/6063.html)

[Bible Hub](https://biblehub.com/str/hebrew/6063.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06063)

[Bible Bento](https://biblebento.com/dictionary/H6063.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6063/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6063.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6063)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/aner.html)

[Video Bible](https://www.videobible.com/bible-dictionary/aner)