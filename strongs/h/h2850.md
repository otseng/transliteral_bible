# חִתִּי

[Ḥitî](https://www.blueletterbible.org/lexicon/h2850)

Definition: Hittite (48x).

Part of speech: masculine patrial noun

Occurs 48 times in 47 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2850)

[Study Light](https://www.studylight.org/lexicons/hebrew/2850.html)

[Bible Hub](https://biblehub.com/str/hebrew/2850.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02850)

[Bible Bento](https://biblebento.com/dictionary/H2850.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2850/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2850.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2850)
