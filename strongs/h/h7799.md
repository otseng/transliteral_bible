# שׁוּשַׁן

[šûšan](https://www.blueletterbible.org/lexicon/h7799)

Definition: lily (13x), Shoshannim (2x).

Part of speech: masculine noun

Occurs 15 times in 15 verses

Places: [Šûšan](../h/h7800.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7799)

[Study Light](https://www.studylight.org/lexicons/hebrew/7799.html)

[Bible Hub](https://biblehub.com/str/hebrew/7799.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07799)

[Bible Bento](https://biblebento.com/dictionary/H7799.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7799/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7799.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7799)
