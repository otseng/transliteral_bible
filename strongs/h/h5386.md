# נְשִׁי

[nᵊšî](https://www.blueletterbible.org/lexicon/h5386)

Definition: debt (1x).

Part of speech: masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5386)

[Study Light](https://www.studylight.org/lexicons/hebrew/5386.html)

[Bible Hub](https://biblehub.com/str/hebrew/5386.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05386)

[Bible Bento](https://biblebento.com/dictionary/H5386.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5386/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5386.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5386)
