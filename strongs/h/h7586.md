# שָׁאוּל

[Šā'ûl](https://www.blueletterbible.org/lexicon/h7586)

Definition: Saul (399x), Shaul (7x), "desired"

Part of speech: proper masculine noun

Occurs 406 times in 335 verses

Hebrew: [sha'al](../h/h7592.md)

Greek: [Saoul](../g/g4549.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7586)

[Study Light](https://www.studylight.org/lexicons/hebrew/7586.html)

[Bible Hub](https://biblehub.com/str/hebrew/7586.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07586)

[Bible Bento](https://biblebento.com/dictionary/H7586.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7586/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7586.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7586)

[Wikipedia](https://en.wikipedia.org/wiki/Saul)

[Britannica](https://www.britannica.com/biography/Saul-king-of-Israel)

[My Jewish Learning](https://www.myjewishlearning.com/article/king-saul/)

[Catholic Encyclopedia](https://www.newadvent.org/cathen/13486d.htm)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/saul.html)

[Video Bible](https://www.videobible.com/bible-dictionary/saul)