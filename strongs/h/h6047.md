# עֲנָמִים

[ʿĂnāmîm](https://www.blueletterbible.org/lexicon/h6047)

Definition: Anamim (2x).

Part of speech: proper noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6047)

[Study Light](https://www.studylight.org/lexicons/hebrew/6047.html)

[Bible Hub](https://biblehub.com/str/hebrew/6047.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06047)

[Bible Bento](https://biblebento.com/dictionary/H6047.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6047/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6047.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6047)
