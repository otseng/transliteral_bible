# קָצַף

[qāṣap̄](https://www.blueletterbible.org/lexicon/h7107)

Definition: wroth (22x), wrath (5x), displeased (3x), angry (2x), angered (1x), fret (1x).

Part of speech: verb

Occurs 34 times in 32 verses

Hebrew: [qᵊṣap̄](../h/h7108.md), [qeṣep̄](../h/h7110.md)

Greek: [thymos](../g/g2372.md)

Synonyms: [wrath](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Wrath)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7107)

[Study Light](https://www.studylight.org/lexicons/hebrew/7107.html)

[Bible Hub](https://biblehub.com/str/hebrew/7107.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07107)

[Bible Bento](https://biblebento.com/dictionary/H7107.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7107/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7107.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7107)
