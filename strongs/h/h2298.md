# חַד

[ḥaḏ](https://www.blueletterbible.org/lexicon/h2298)

Definition: one (5x), first (4x), a (4x), together (1x).

Part of speech: adjective

Occurs 14 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2298)

[Study Light](https://www.studylight.org/lexicons/hebrew/2298.html)

[Bible Hub](https://biblehub.com/str/hebrew/2298.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02298)

[Bible Bento](https://biblebento.com/dictionary/H2298.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2298/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2298.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2298)
