# שְׁפַרְפָּר

[šᵊp̄arpār](https://www.blueletterbible.org/lexicon/h8238)

Definition: early in the morning (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8238)

[Study Light](https://www.studylight.org/lexicons/hebrew/8238.html)

[Bible Hub](https://biblehub.com/str/hebrew/8238.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08238)

[Bible Bento](https://biblebento.com/dictionary/H8238.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8238/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8238.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8238)
