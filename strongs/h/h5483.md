# סוּס

[sûs](https://www.blueletterbible.org/lexicon/h5483)

Definition: horse (133x), crane (2x), horseback (2x), horseback (with H7392) (2x), horsehoofs (with H6119) (1x).

Part of speech: masculine noun

Occurs 141 times in 130 verses

Hebrew: [sûsâ](../h/h5484.md)

Greek: [hippos](../g/g2462.md)

Edenics: pegasus

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5483)

[Study Light](https://www.studylight.org/lexicons/hebrew/5483.html)

[Bible Hub](https://biblehub.com/str/hebrew/5483.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05483)

[Bible Bento](https://biblebento.com/dictionary/H5483.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5483/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5483.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5483)
