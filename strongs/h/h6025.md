# עֵנָב

[ʿēnāḇ](https://www.blueletterbible.org/lexicon/h6025)

Definition: grape (18x), wine (1x).

Part of speech: masculine noun

Occurs 19 times in 17 verses

Greek: [staphylē](../g/g4718.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6025)

[Study Light](https://www.studylight.org/lexicons/hebrew/6025.html)

[Bible Hub](https://biblehub.com/str/hebrew/6025.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06025)

[Bible Bento](https://biblebento.com/dictionary/H6025.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6025/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6025.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6025)
