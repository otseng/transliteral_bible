# עָלַס

[ʿālas](https://www.blueletterbible.org/lexicon/h5965)

Definition: rejoice (1x), peacock (1x), solace (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5965)

[Study Light](https://www.studylight.org/lexicons/hebrew/5965.html)

[Bible Hub](https://biblehub.com/str/hebrew/5965.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05965)

[Bible Bento](https://biblebento.com/dictionary/H5965.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5965/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5965.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5965)
