# חָטַם

[ḥāṭam](https://www.blueletterbible.org/lexicon/h2413)

Definition: refrain (1x), to hold in

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2413)

[Study Light](https://www.studylight.org/lexicons/hebrew/2413.html)

[Bible Hub](https://biblehub.com/str/hebrew/2413.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02413)

[Bible Bento](https://biblebento.com/dictionary/H2413.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2413/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2413.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2413)
