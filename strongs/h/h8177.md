# שְׂעַר

[śᵊʿar](https://www.blueletterbible.org/lexicon/h8177)

Definition: hair (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8177)

[Study Light](https://www.studylight.org/lexicons/hebrew/8177.html)

[Bible Hub](https://biblehub.com/str/hebrew/8177.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08177)

[Bible Bento](https://biblebento.com/dictionary/H8177.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8177/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8177.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8177)
