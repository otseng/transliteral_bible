# תַּרְתָּק

[Tartāq](https://www.blueletterbible.org/lexicon/h8662)

Definition: Tartak (1x).

Part of speech: proper noun with reference to deity

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8662)

[Study Light](https://www.studylight.org/lexicons/hebrew/8662.html)

[Bible Hub](https://biblehub.com/str/hebrew/8662.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08662)

[Bible Bento](https://biblebento.com/dictionary/H8662.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8662/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8662.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8662)
