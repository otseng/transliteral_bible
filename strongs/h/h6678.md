# צוֹבָא

[Ṣôḇā'](https://www.blueletterbible.org/lexicon/h6678)

Definition: Zobah (10x), Zobah (2x).

Part of speech: proper locative noun

Occurs 12 times in 12 verses

Hebrew: [Ṣîḇā'](../h/h6717.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6678)

[Study Light](https://www.studylight.org/lexicons/hebrew/6678.html)

[Bible Hub](https://biblehub.com/str/hebrew/6678.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06678)

[Bible Bento](https://biblebento.com/dictionary/H6678.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6678/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6678.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6678)

[Video Bible](https://www.videobible.com/bible-dictionary/zobah)