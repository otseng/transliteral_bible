# הֶמֶס

[hemes](https://www.blueletterbible.org/lexicon/h2003)

Definition: melting (1x).

Part of speech: masculine plural noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2003)

[Study Light](https://www.studylight.org/lexicons/hebrew/2003.html)

[Bible Hub](https://biblehub.com/str/hebrew/2003.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02003)

[Bible Bento](https://biblebento.com/dictionary/H2003.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2003/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2003.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2003)
