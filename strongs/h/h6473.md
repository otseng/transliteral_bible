# פָּעַר

[pāʿar](https://www.blueletterbible.org/lexicon/h6473)

Definition: open (3x), gaped (1x).

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6473)

[Study Light](https://www.studylight.org/lexicons/hebrew/6473.html)

[Bible Hub](https://biblehub.com/str/hebrew/6473.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06473)

[Bible Bento](https://biblebento.com/dictionary/H6473.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6473/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6473.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6473)
