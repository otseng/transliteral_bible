# עֲרוּגָה

[ʿărûḡâ](https://www.blueletterbible.org/lexicon/h6170)

Definition: bed (2x), furrow (2x).

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6170)

[Study Light](https://www.studylight.org/lexicons/hebrew/6170.html)

[Bible Hub](https://biblehub.com/str/hebrew/6170.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06170)

[Bible Bento](https://biblebento.com/dictionary/H6170.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6170/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6170.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6170)
