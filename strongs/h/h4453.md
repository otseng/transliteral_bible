# מֶלְצַר

[melṣar](https://www.blueletterbible.org/lexicon/h4453)

Definition: Melzar (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4453)

[Study Light](https://www.studylight.org/lexicons/hebrew/4453.html)

[Bible Hub](https://biblehub.com/str/hebrew/4453.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04453)

[Bible Bento](https://biblebento.com/dictionary/H4453.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4453/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4453.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4453)
