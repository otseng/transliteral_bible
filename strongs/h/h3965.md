# מַאֲבוּס

[ma'ăḇûs](https://www.blueletterbible.org/lexicon/h3965)

Definition: storehouse (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3965)

[Study Light](https://www.studylight.org/lexicons/hebrew/3965.html)

[Bible Hub](https://biblehub.com/str/hebrew/3965.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03965)

[Bible Bento](https://biblebento.com/dictionary/H3965.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3965/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3965.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3965)
