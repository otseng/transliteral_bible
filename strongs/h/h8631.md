# תְּקֵף

[tᵊqēp̄](https://www.blueletterbible.org/lexicon/h8631)

Definition: ...strong (3x), harden (1x), make firm (1x).

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8631)

[Study Light](https://www.studylight.org/lexicons/hebrew/8631.html)

[Bible Hub](https://biblehub.com/str/hebrew/8631.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08631)

[Bible Bento](https://biblebento.com/dictionary/H8631.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8631/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8631.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8631)
