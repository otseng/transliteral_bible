# נֵרְדְּ

[nērdᵊ](https://www.blueletterbible.org/lexicon/h5373)

Definition: spikenard (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5373)

[Study Light](https://www.studylight.org/lexicons/hebrew/5373.html)

[Bible Hub](https://biblehub.com/str/hebrew/5373.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05373)

[Bible Bento](https://biblebento.com/dictionary/H5373.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5373/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5373.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5373)
