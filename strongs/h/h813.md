# אַשְׁכְּנַז

['Aškᵊnaz](https://www.blueletterbible.org/lexicon/h813)

Definition: Ashkenaz (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0813)

[Study Light](https://www.studylight.org/lexicons/hebrew/813.html)

[Bible Hub](https://biblehub.com/str/hebrew/813.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0813)

[Bible Bento](https://biblebento.com/dictionary/H813.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/813/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/813.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h813)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ashkenaz.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ashkenaz)