# לַחַשׁ

[laḥaš](https://www.blueletterbible.org/lexicon/h3908)

Definition: enchantment (1x), orator (1x), earing (1x), prayer (1x), charmed (1x), amulets

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3908)

[Study Light](https://www.studylight.org/lexicons/hebrew/3908.html)

[Bible Hub](https://biblehub.com/str/hebrew/3908.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03908)

[Bible Bento](https://biblebento.com/dictionary/H3908.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3908/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3908.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3908)
