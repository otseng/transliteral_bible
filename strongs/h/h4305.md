# מָטַר

[matar](https://www.blueletterbible.org/lexicon/h4305)

Definition: rain (17x).

Part of speech: verb

Occurs 17 times in 14 verses

Hebrew: [māṭār](../h/h4306.md)

Edenics: meteorology

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4305)

[Study Light](https://www.studylight.org/lexicons/hebrew/4305.html)

[Bible Hub](https://biblehub.com/str/hebrew/4305.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04305)

[Bible Bento](https://biblebento.com/dictionary/H4305.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4305/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4305.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4305)
