# אִי

['î](https://www.blueletterbible.org/lexicon/h336)

Definition: island (1x), not

Part of speech: adverb

Occurs 1 times in 1 verses

Hebrew: ['ay](../h/h335.md), ['î](../h/h339.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h336)

[Study Light](https://www.studylight.org/lexicons/hebrew/336.html)

[Bible Hub](https://biblehub.com/str/hebrew/336.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0336)

[Bible Bento](https://biblebento.com/dictionary/H336.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/336/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/336.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h336)
