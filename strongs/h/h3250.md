# יִסּוֹר

[yissôr](https://www.blueletterbible.org/lexicon/h3250)

Definition: instruct (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3250)

[Study Light](https://www.studylight.org/lexicons/hebrew/3250.html)

[Bible Hub](https://biblehub.com/str/hebrew/3250.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03250)

[Bible Bento](https://biblebento.com/dictionary/H3250.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3250/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3250.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3250)
