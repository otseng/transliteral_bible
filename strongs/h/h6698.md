# צַוָּר

[Ṣaûār](https://www.blueletterbible.org/lexicon/h6698)

Definition: Zur (5x).

Part of speech: proper masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6698)

[Study Light](https://www.studylight.org/lexicons/hebrew/6698.html)

[Bible Hub](https://biblehub.com/str/hebrew/6698.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06698)

[Bible Bento](https://biblebento.com/dictionary/H6698.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6698/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6698.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6698)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zur.html)

[Video Bible](https://www.videobible.com/bible-dictionary/zur)