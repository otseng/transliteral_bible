# חַרְטֹם

[ḥarṭōm](https://www.blueletterbible.org/lexicon/h2748)

Definition: magician (11x), diviner, astrologer, engraver, writer (only in derivative sense of one possessed of occult knowledge)

Part of speech: masculine noun

Occurs 11 times in 10 verses

Hebrew: [ḥereṭ](../h/h2747.md)

Greek: [magos](../g/g3097.md), [pharmakos](../g/g5333.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2748)

[Study Light](https://www.studylight.org/lexicons/hebrew/2748.html)

[Bible Hub](https://biblehub.com/str/hebrew/2748.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02748)

[Bible Bento](https://biblebento.com/dictionary/H2748.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2748/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2748.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2748)
