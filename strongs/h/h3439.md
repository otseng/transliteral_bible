# יְשוֹחָיָה

[YᵊshÔḥāyâ](https://www.blueletterbible.org/lexicon/h3439)

Definition: Jeshohaiah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3439)

[Study Light](https://www.studylight.org/lexicons/hebrew/3439.html)

[Bible Hub](https://biblehub.com/str/hebrew/3439.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03439)

[Bible Bento](https://biblebento.com/dictionary/H3439.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3439/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3439.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3439)

[Video Bible](https://www.videobible.com/bible-dictionary/jeshohaiah)