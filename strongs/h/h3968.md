# מֵאָה

[mē'â](https://www.blueletterbible.org/lexicon/h3968)

Definition: Meah (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3968)

[Study Light](https://www.studylight.org/lexicons/hebrew/3968.html)

[Bible Hub](https://biblehub.com/str/hebrew/3968.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03968)

[Bible Bento](https://biblebento.com/dictionary/H3968.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3968/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3968.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3968)
