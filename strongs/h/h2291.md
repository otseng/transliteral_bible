# חַגִּי

[ḥagî](https://www.blueletterbible.org/lexicon/h2291)

Definition: Haggi (2x), Haggites (1x).

Part of speech: adjective, proper masculine noun

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2291)

[Study Light](https://www.studylight.org/lexicons/hebrew/2291.html)

[Bible Hub](https://biblehub.com/str/hebrew/2291.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02291)

[Bible Bento](https://biblebento.com/dictionary/H2291.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2291/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2291.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2291)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/haggi.html)

[Video Bible](https://www.videobible.com/bible-dictionary/haggi)