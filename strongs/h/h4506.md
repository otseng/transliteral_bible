# מָנַחַת

[mānaḥaṯ](https://www.blueletterbible.org/lexicon/h4506)

Definition: Manahath (3x).

Part of speech: proper locative noun, proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4506)

[Study Light](https://www.studylight.org/lexicons/hebrew/4506.html)

[Bible Hub](https://biblehub.com/str/hebrew/4506.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04506)

[Bible Bento](https://biblebento.com/dictionary/H4506.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4506/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4506.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4506)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/manahath.html)

[Video Bible](https://www.videobible.com/bible-dictionary/manahath)