# חָפֵר

[ḥāp̄ēr](https://www.blueletterbible.org/lexicon/h2659)

Definition: confounded (6x), ashamed (4x), shame (4x), confusion (2x), reproach (1x).

Part of speech: verb

Occurs 17 times in 17 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2659)

[Study Light](https://www.studylight.org/lexicons/hebrew/2659.html)

[Bible Hub](https://biblehub.com/str/hebrew/2659.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02659)

[Bible Bento](https://biblebento.com/dictionary/H2659.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2659/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2659.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2659)
