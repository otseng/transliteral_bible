# מְנֵא

[mᵊnē'](https://www.blueletterbible.org/lexicon/h4484)

Definition: MENE (3x), a weight or measurement; usually 50 shekels but maybe 60 shekels

Part of speech: masculine noun

Occurs 3 times in 2 verses

Hebrew: [mᵊnā'](../h/h4483.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4484)

[Study Light](https://www.studylight.org/lexicons/hebrew/4484.html)

[Bible Hub](https://biblehub.com/str/hebrew/4484.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04484)

[Bible Bento](https://biblebento.com/dictionary/H4484.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4484/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4484.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4484)
