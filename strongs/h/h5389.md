# נָשִׁין

[nāšîn](https://www.blueletterbible.org/lexicon/h5389)

Definition: wives (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5389)

[Study Light](https://www.studylight.org/lexicons/hebrew/5389.html)

[Bible Hub](https://biblehub.com/str/hebrew/5389.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05389)

[Bible Bento](https://biblebento.com/dictionary/H5389.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5389/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5389.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5389)
