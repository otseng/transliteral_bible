# רָזֶה

[rāzê](https://www.blueletterbible.org/lexicon/h7330)

Definition: lean (2x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7330)

[Study Light](https://www.studylight.org/lexicons/hebrew/7330.html)

[Bible Hub](https://biblehub.com/str/hebrew/7330.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07330)

[Bible Bento](https://biblebento.com/dictionary/H7330.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7330/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7330.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7330)
