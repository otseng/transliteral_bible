# מַעֲבָר

[maʿăḇār](https://www.blueletterbible.org/lexicon/h4569)

Definition: ford (4x), passage (6x), pass (1x).

Part of speech: masculine noun

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4569)

[Study Light](https://www.studylight.org/lexicons/hebrew/4569.html)

[Bible Hub](https://biblehub.com/str/hebrew/4569.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04569)

[Bible Bento](https://biblebento.com/dictionary/H4569.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4569/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4569.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4569)
