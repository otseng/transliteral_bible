# שְׁבָא

[Šᵊḇā'](https://www.blueletterbible.org/lexicon/h7614)

Definition: Sheba (23x), a nation in southern Arabia

Part of speech: proper locative noun, proper masculine noun

Occurs 23 times in 23 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7614)

[Study Light](https://www.studylight.org/lexicons/hebrew/7614.html)

[Bible Hub](https://biblehub.com/str/hebrew/7614.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07614)

[Bible Bento](https://biblebento.com/dictionary/H7614.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7614/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7614.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7614)

[Wikipedia](https://en.wikipedia.org/wiki/Sheba)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/sheba.html)

[Video Bible](https://www.videobible.com/bible-dictionary/sheba)