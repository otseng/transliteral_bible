# עָב

[ʿāḇ](https://www.blueletterbible.org/lexicon/h5646)

Definition: thick plank (2x), thick beam (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5646)

[Study Light](https://www.studylight.org/lexicons/hebrew/5646.html)

[Bible Hub](https://biblehub.com/str/hebrew/5646.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05646)

[Bible Bento](https://biblebento.com/dictionary/H5646.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5646/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5646.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5646)
