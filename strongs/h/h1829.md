# דִּמְנָה

[Dimnâ](https://www.blueletterbible.org/lexicon/h1829)

Definition: Dimnah (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1829)

[Study Light](https://www.studylight.org/lexicons/hebrew/1829.html)

[Bible Hub](https://biblehub.com/str/hebrew/1829.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01829)

[Bible Bento](https://biblebento.com/dictionary/H1829.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1829/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1829.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1829)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/dimnah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/dimnah)