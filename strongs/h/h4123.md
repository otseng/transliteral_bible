# מַהֲתַלָה

[mahăṯalâ](https://www.blueletterbible.org/lexicon/h4123)

Definition: deceits (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4123)

[Study Light](https://www.studylight.org/lexicons/hebrew/4123.html)

[Bible Hub](https://biblehub.com/str/hebrew/4123.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04123)

[Bible Bento](https://biblebento.com/dictionary/H4123.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4123/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4123.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4123)
