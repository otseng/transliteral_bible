# סָרַח

[sāraḥ](https://www.blueletterbible.org/lexicon/h5628)

Definition: hang (2x), stretch (2x), spreading (1x), exceeding (1x), vanished (1x).

Part of speech: verb

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5628)

[Study Light](https://www.studylight.org/lexicons/hebrew/5628.html)

[Bible Hub](https://biblehub.com/str/hebrew/5628.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05628)

[Bible Bento](https://biblebento.com/dictionary/H5628.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5628/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5628.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5628)
