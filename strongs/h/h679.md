# אַצִּיל

['aṣṣîl](https://www.blueletterbible.org/lexicon/h679)

Definition: armhole (with H3027) (2x), great (1x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h679)

[Study Light](https://www.studylight.org/lexicons/hebrew/679.html)

[Bible Hub](https://biblehub.com/str/hebrew/679.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0679)

[Bible Bento](https://biblebento.com/dictionary/H679.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/679/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/679.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h679)
