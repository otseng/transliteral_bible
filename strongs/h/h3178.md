# יַחְלְאֵלִי

[yaḥlᵊ'ēlî](https://www.blueletterbible.org/lexicon/h3178)

Definition: Jahleelites (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3178)

[Study Light](https://www.studylight.org/lexicons/hebrew/3178.html)

[Bible Hub](https://biblehub.com/str/hebrew/3178.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03178)

[Bible Bento](https://biblebento.com/dictionary/H3178.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3178/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3178.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3178)
