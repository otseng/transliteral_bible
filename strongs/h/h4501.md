# מְנוֹרָה

[mᵊnôrâ](https://www.blueletterbible.org/lexicon/h4501)

Definition: candlestick (40x).

Part of speech: feminine noun

Occurs 42 times in 31 verses

Hebrew: [mānôr](../h/h4500.md)

Greek: [lychnia](../g/g3087.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4501)

[Study Light](https://www.studylight.org/lexicons/hebrew/4501.html)

[Bible Hub](https://biblehub.com/str/hebrew/4501.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04501)

[Bible Bento](https://biblebento.com/dictionary/H4501.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4501/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4501.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4501)
