# בֵּרֹתִי

[Bērōṯî](https://www.blueletterbible.org/lexicon/h1307)

Definition: Berothite (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1307)

[Study Light](https://www.studylight.org/lexicons/hebrew/1307.html)

[Bible Hub](https://biblehub.com/str/hebrew/1307.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01307)

[Bible Bento](https://biblebento.com/dictionary/H1307.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1307/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1307.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1307)
