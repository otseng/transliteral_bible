# סְבָאִי

[sᵊḇā'î](https://www.blueletterbible.org/lexicon/h5436)

Definition: Sabeans (2x).

Part of speech: proper gentilic noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5436)

[Study Light](https://www.studylight.org/lexicons/hebrew/5436.html)

[Bible Hub](https://biblehub.com/str/hebrew/5436.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05436)

[Bible Bento](https://biblebento.com/dictionary/H5436.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5436/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5436.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5436)
