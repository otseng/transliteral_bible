# בָּצַע

[batsa`](https://www.blueletterbible.org/lexicon/h1214)

Definition: cut me off (2x), gained (2x), given (2x), greedy (2x), coveteth (1x), covetous (1x), cut (1x), finish (1x), fulfilled (1x), get (1x), performed (1x), wounded (1x).

Part of speech: verb

Occurs 16 times in 16 verses

Hebrew: [beṣaʿ](../h/h1215.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1214)

[Study Light](https://www.studylight.org/lexicons/hebrew/1214.html)

[Bible Hub](https://biblehub.com/str/hebrew/1214.htm)

[Morfix](https://www.morfix.co.il/en/%D7%91%D6%BC%D6%B8%D7%A6%D6%B7%D7%A2)

[NET Bible](http://classic.net.bible.org/strong.php?id=01214)

[Bible Bento](https://biblebento.com/dictionary/H1214.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1214/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1214.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1214)
