# יָקוֹשׁ

[yāqôš](https://www.blueletterbible.org/lexicon/h3353)

Definition: fowler (2x), snare (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

Hebrew: [yāqōš](../h/h3369.md), [mowqesh](../h/h4170.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3353)

[Study Light](https://www.studylight.org/lexicons/hebrew/3353.html)

[Bible Hub](https://biblehub.com/str/hebrew/3353.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03353)

[Bible Bento](https://biblebento.com/dictionary/H3353.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3353/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3353.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3353)
