# קֹבֶל

[qōḇel](https://www.blueletterbible.org/lexicon/h6904)

Definition: war (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6904)

[Study Light](https://www.studylight.org/lexicons/hebrew/6904.html)

[Bible Hub](https://biblehub.com/str/hebrew/6904.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06904)

[Bible Bento](https://biblebento.com/dictionary/H6904.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6904/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6904.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6904)
