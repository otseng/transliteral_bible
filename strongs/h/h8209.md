# שַׁפִּיר

[šapîr](https://www.blueletterbible.org/lexicon/h8209)

Definition: fair (2x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8209)

[Study Light](https://www.studylight.org/lexicons/hebrew/8209.html)

[Bible Hub](https://biblehub.com/str/hebrew/8209.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08209)

[Bible Bento](https://biblebento.com/dictionary/H8209.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8209/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8209.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8209)
