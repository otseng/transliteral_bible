# חַטָּיָא

[ḥaṭṭāyā'](https://www.blueletterbible.org/lexicon/h2409)

Definition: sin offering (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2409)

[Study Light](https://www.studylight.org/lexicons/hebrew/2409.html)

[Bible Hub](https://biblehub.com/str/hebrew/2409.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02409)

[Bible Bento](https://biblebento.com/dictionary/H2409.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2409/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2409.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2409)
