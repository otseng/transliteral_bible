# מוּצַק

[mûṣaq](https://www.blueletterbible.org/lexicon/h4164)

Definition: straitness (1x), straitened (1x), vexation (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4164)

[Study Light](https://www.studylight.org/lexicons/hebrew/4164.html)

[Bible Hub](https://biblehub.com/str/hebrew/4164.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04164)

[Bible Bento](https://biblebento.com/dictionary/H4164.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4164/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4164.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4164)
