# גָּלַל

[gālal](https://www.blueletterbible.org/lexicon/h1556)

Definition: roll (9x), roll ... (3x), seek occasion (1x), wallow (1x), trust (1x), commit (1x), remove (1x), run down (1x).

Part of speech: verb

Occurs 18 times in 18 verses

Hebrew: [mᵊḡillâ](../h/h4039.md), [gal](../h/h1530.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1556)

[Study Light](https://www.studylight.org/lexicons/hebrew/1556.html)

[Bible Hub](https://biblehub.com/str/hebrew/1556.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01556)

[Bible Bento](https://biblebento.com/dictionary/H1556.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1556/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1556.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1556)
