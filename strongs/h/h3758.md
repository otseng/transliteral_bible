# כַּרְמִיל

[karmîl](https://www.blueletterbible.org/lexicon/h3758)

Definition: crimson (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3758)

[Study Light](https://www.studylight.org/lexicons/hebrew/3758.html)

[Bible Hub](https://biblehub.com/str/hebrew/3758.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03758)

[Bible Bento](https://biblebento.com/dictionary/H3758.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3758/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3758.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3758)
