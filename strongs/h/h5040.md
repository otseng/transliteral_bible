# נַבְלוּת

[naḇlûṯ](https://www.blueletterbible.org/lexicon/h5040)

Definition: lewdness (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5040)

[Study Light](https://www.studylight.org/lexicons/hebrew/5040.html)

[Bible Hub](https://biblehub.com/str/hebrew/5040.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05040)

[Bible Bento](https://biblebento.com/dictionary/H5040.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5040/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5040.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5040)
