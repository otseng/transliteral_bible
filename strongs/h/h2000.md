# הָמַם

[hāmam](https://www.blueletterbible.org/lexicon/h2000)

Definition: discomfit (5x), destroy (3x), vex (1x), crush (1x), break (1x), consume 1 trouble (1x).

Part of speech: verb

Occurs 16 times in 14 verses

Greek: [existēmi](../g/g1839.md)

Synonyms: [destroy](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Destroy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2000)

[Study Light](https://www.studylight.org/lexicons/hebrew/2000.html)

[Bible Hub](https://biblehub.com/str/hebrew/2000.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02000)

[Bible Bento](https://biblebento.com/dictionary/H2000.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2000/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2000.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2000)
