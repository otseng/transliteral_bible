# נָשַׁב

[nāšaḇ](https://www.blueletterbible.org/lexicon/h5380)

Definition: blow (2x), drive away (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5380)

[Study Light](https://www.studylight.org/lexicons/hebrew/5380.html)

[Bible Hub](https://biblehub.com/str/hebrew/5380.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05380)

[Bible Bento](https://biblebento.com/dictionary/H5380.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5380/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5380.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5380)
