# מַשְׁחִית

[mašḥîṯ](https://www.blueletterbible.org/lexicon/h4889)

Definition: destroy (4x), corruption (2x), destruction (2x), set a trap (1x), destroying (1x), utterly (1x).

Part of speech: masculine noun

Occurs 11 times in 11 verses

Hebrew: [mišḥāṯ](../h/h4893.md), [shachath](../h/h7843.md), [šᵊḥaṯ](../h/h7844.md)

Synonyms: [destroy](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Destroy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4889)

[Study Light](https://www.studylight.org/lexicons/hebrew/4889.html)

[Bible Hub](https://biblehub.com/str/hebrew/4889.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04889)

[Bible Bento](https://biblebento.com/dictionary/H4889.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4889/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4889.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4889)
