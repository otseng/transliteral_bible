# חָמוֹת

[ḥāmôṯ](https://www.blueletterbible.org/lexicon/h2545)

Definition: mother in law (11x).

Part of speech: feminine noun

Occurs 11 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2545)

[Study Light](https://www.studylight.org/lexicons/hebrew/2545.html)

[Bible Hub](https://biblehub.com/str/hebrew/2545.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02545)

[Bible Bento](https://biblebento.com/dictionary/H2545.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2545/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2545.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2545)
