# צֹחַר

[ṣōḥar](https://www.blueletterbible.org/lexicon/h6714)

Definition: Zoar (4x), Jezoar (1x).

Part of speech: proper masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6714)

[Study Light](https://www.studylight.org/lexicons/hebrew/6714.html)

[Bible Hub](https://biblehub.com/str/hebrew/6714.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06714)

[Bible Bento](https://biblebento.com/dictionary/H6714.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6714/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6714.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6714)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zoar.html)

[Video Bible](https://www.videobible.com/bible-dictionary/zoar)