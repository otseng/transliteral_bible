# כַּבּוֹן

[Kabôn](https://www.blueletterbible.org/lexicon/h3522)

Definition: Cabbon (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3522)

[Study Light](https://www.studylight.org/lexicons/hebrew/3522.html)

[Bible Hub](https://biblehub.com/str/hebrew/3522.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03522)

[Bible Bento](https://biblebento.com/dictionary/H3522.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3522/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3522.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3522)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/cabbon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/cabbon)