# יְשֻׁרוּן

[Yᵊšurûn](https://www.blueletterbible.org/lexicon/h3484)

Definition: Jeshurun (3x), Jesurun (1x), symbolic name for Israel, "upright one"

Part of speech: proper masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3484)

[Study Light](https://www.studylight.org/lexicons/hebrew/3484.html)

[Bible Hub](https://biblehub.com/str/hebrew/3484.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03484)

[Bible Bento](https://biblebento.com/dictionary/H3484.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3484/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3484.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3484)

[Wikipedia](https://en.wikipedia.org/wiki/Jeshurun)

[Encyclopedia.com](https://www.encyclopedia.com/religion/encyclopedias-almanacs-transcripts-and-maps/jeshurun)

[Chaim Bentorah](https://www.chaimbentorah.com/2016/02/word-study-jeshurun-%D7%99%EF%AC%AA%D7%A8%D7%A0/)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jeshurun.html)