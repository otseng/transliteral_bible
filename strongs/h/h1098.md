# בְּלִיל

[bᵊlîl](https://www.blueletterbible.org/lexicon/h1098)

Definition: fodder (1x), corn (1x), provender (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1098)

[Study Light](https://www.studylight.org/lexicons/hebrew/1098.html)

[Bible Hub](https://biblehub.com/str/hebrew/1098.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01098)

[Bible Bento](https://biblebento.com/dictionary/H1098.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1098/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1098.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1098)
