# נָקַשׁ

[nāqaš](https://www.blueletterbible.org/lexicon/h5367)

Definition: lay a snare (2x), snare (2x), catch (1x).

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5367)

[Study Light](https://www.studylight.org/lexicons/hebrew/5367.html)

[Bible Hub](https://biblehub.com/str/hebrew/5367.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05367)

[Bible Bento](https://biblebento.com/dictionary/H5367.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5367/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5367.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5367)
