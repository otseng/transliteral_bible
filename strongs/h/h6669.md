# צָהֹב

[ṣāhōḇ](https://www.blueletterbible.org/lexicon/h6669)

Definition: yellow (3x).

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6669)

[Study Light](https://www.studylight.org/lexicons/hebrew/6669.html)

[Bible Hub](https://biblehub.com/str/hebrew/6669.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06669)

[Bible Bento](https://biblebento.com/dictionary/H6669.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6669/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6669.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6669)
