# אָשַׁם

['asham](https://www.blueletterbible.org/lexicon/h816)

Definition: guilty (14x), desolate (6x), offend (6x), trespass (4x), certainly (1x), destroy (1x), faulty (1x), greatly (1x), offence (1x).

Part of speech: verb

Occurs 35 times in 32 verses

Hebrew: ['āšām](../h/h817.md)

Edenics: ashame

Synonyms: [destroy](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Destroy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0816)

[Study Light](https://www.studylight.org/lexicons/hebrew/816.html)

[Bible Hub](https://biblehub.com/str/hebrew/816.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%90%D7%A9%D7%9D)

[NET Bible](http://classic.net.bible.org/strong.php?id=0816)

[Bible Bento](https://biblebento.com/dictionary/H0816.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0816/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0816.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h816)
