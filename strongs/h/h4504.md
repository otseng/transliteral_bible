# מִנְחָה

[minḥâ](https://www.blueletterbible.org/lexicon/h4504)

Definition: meat offering (1x), oblation (1x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

Hebrew: [minchah](../h/h4503.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4504)

[Study Light](https://www.studylight.org/lexicons/hebrew/4504.html)

[Bible Hub](https://biblehub.com/str/hebrew/4504.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04504)

[Bible Bento](https://biblebento.com/dictionary/H4504.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4504/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4504.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4504)
