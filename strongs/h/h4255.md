# מַחְלְקָה

[maḥlᵊqâ](https://www.blueletterbible.org/lexicon/h4255)

Definition: courses (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4255)

[Study Light](https://www.studylight.org/lexicons/hebrew/4255.html)

[Bible Hub](https://biblehub.com/str/hebrew/4255.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04255)

[Bible Bento](https://biblebento.com/dictionary/H4255.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4255/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4255.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4255)
