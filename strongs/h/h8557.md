# תֶּמֶס

[temes](https://www.blueletterbible.org/lexicon/h8557)

Definition: melt (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8557)

[Study Light](https://www.studylight.org/lexicons/hebrew/8557.html)

[Bible Hub](https://biblehub.com/str/hebrew/8557.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08557)

[Bible Bento](https://biblebento.com/dictionary/H8557.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8557/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8557.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8557)
