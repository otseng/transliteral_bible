# חָסַן

[ḥāsan](https://www.blueletterbible.org/lexicon/h2630)

Definition: laid up (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [ḥăsîn](../h/h2626.md), [ḥēsen](../h/h2632.md), [ḥōsen](../h/h2633.md), [ḥāsōn](../h/h2634.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2630)

[Study Light](https://www.studylight.org/lexicons/hebrew/2630.html)

[Bible Hub](https://biblehub.com/str/hebrew/2630.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02630)

[Bible Bento](https://biblebento.com/dictionary/H2630.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2630/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2630.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2630)
