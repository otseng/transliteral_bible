# תָּוָה

[tāvâ](https://www.blueletterbible.org/lexicon/h8427)

Definition: scrabble (1x), set (1x).

Part of speech: verb

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8427)

[Study Light](https://www.studylight.org/lexicons/hebrew/8427.html)

[Bible Hub](https://biblehub.com/str/hebrew/8427.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08427)

[Bible Bento](https://biblebento.com/dictionary/H8427.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8427/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8427.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8427)
