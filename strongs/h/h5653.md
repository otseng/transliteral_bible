# עַבְדָּא

[ʿAḇdā'](https://www.blueletterbible.org/lexicon/h5653)

Definition: Abda (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5653)

[Study Light](https://www.studylight.org/lexicons/hebrew/5653.html)

[Bible Hub](https://biblehub.com/str/hebrew/5653.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05653)

[Bible Bento](https://biblebento.com/dictionary/H5653.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5653/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5653.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5653)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/abda.html)

[Video Bible](https://www.videobible.com/bible-dictionary/abda)