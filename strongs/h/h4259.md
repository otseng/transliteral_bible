# מְחֹלָתִי

[mᵊḥōlāṯî](https://www.blueletterbible.org/lexicon/h4259)

Definition: Meholathite (2x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4259)

[Study Light](https://www.studylight.org/lexicons/hebrew/4259.html)

[Bible Hub](https://biblehub.com/str/hebrew/4259.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04259)

[Bible Bento](https://biblebento.com/dictionary/H4259.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4259/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4259.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4259)
