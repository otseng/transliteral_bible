# חָדַשׁ

[ḥādaš](https://www.blueletterbible.org/lexicon/h2318)

Definition: renew (7x), repair (3x).

Part of speech: verb

Occurs 10 times in 10 verses

Hebrew: [ḥāḏāš](../h/h2319.md), [ḥōḏeš](../h/h2320.md), [ḥăḏaṯ](../h/h2323.md)

Greek: [anakainizō](../g/g340.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2318)

[Study Light](https://www.studylight.org/lexicons/hebrew/2318.html)

[Bible Hub](https://biblehub.com/str/hebrew/2318.htm)

[Morfix](https://www.morfix.co.il/en/%D7%97%D6%B8%D7%93%D6%B7%D7%A9%D7%81)

[NET Bible](http://classic.net.bible.org/strong.php?id=02318)

[Bible Bento](https://biblebento.com/dictionary/H2318.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2318/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2318.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2318)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%97%D7%93%D7%A9)
