# צִפַּר

[ṣipar](https://www.blueletterbible.org/lexicon/h6853)

Definition: fowl (3x), like birds (1x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6853)

[Study Light](https://www.studylight.org/lexicons/hebrew/6853.html)

[Bible Hub](https://biblehub.com/str/hebrew/6853.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06853)

[Bible Bento](https://biblebento.com/dictionary/H6853.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6853/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6853.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6853)
