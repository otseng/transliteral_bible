# מָא

[mā'](https://www.blueletterbible.org/lexicon/h3964)

Definition: what (1x).

Part of speech: interrogative pronoun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3964)

[Study Light](https://www.studylight.org/lexicons/hebrew/3964.html)

[Bible Hub](https://biblehub.com/str/hebrew/3964.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03964)

[Bible Bento](https://biblebento.com/dictionary/H3964.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3964/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3964.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3964)
