# רָשַׁשׁ

[rāšaš](https://www.blueletterbible.org/lexicon/h7567)

Definition: impoverish (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7567)

[Study Light](https://www.studylight.org/lexicons/hebrew/7567.html)

[Bible Hub](https://biblehub.com/str/hebrew/7567.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07567)

[Bible Bento](https://biblebento.com/dictionary/H7567.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7567/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7567.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7567)
