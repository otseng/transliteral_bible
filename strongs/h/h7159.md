# קָרַם

[qāram](https://www.blueletterbible.org/lexicon/h7159)

Definition: cover (2x).

Part of speech: verb

Occurs 2 times in 2 verses

Synonyms: [cover](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Cover)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7159)

[Study Light](https://www.studylight.org/lexicons/hebrew/7159.html)

[Bible Hub](https://biblehub.com/str/hebrew/7159.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07159)

[Bible Bento](https://biblebento.com/dictionary/H7159.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7159/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7159.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7159)
