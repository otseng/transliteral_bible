# חֲבַר

[ḥăḇar](https://www.blueletterbible.org/lexicon/h2269)

Definition: fellows (2x), companions (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

Greek: [philos](../g/g5384.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2269)

[Study Light](https://www.studylight.org/lexicons/hebrew/2269.html)

[Bible Hub](https://biblehub.com/str/hebrew/2269.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02269)

[Bible Bento](https://biblebento.com/dictionary/H2269.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2269/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2269.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2269)
