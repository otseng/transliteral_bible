# קָשַׁר

[qāšar](https://www.blueletterbible.org/lexicon/h7194)

Definition: conspired (18x), bind (14x), made (5x), stronger (2x), miscellaneous (5x), tie, confine

Part of speech: verb

Occurs 44 times in 44 verses

Hebrew: [qešer](../h/h7195.md), [qiššurîm](../h/h7196.md)

Greek: [synagō](../g/g4863).md

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7194)

[Study Light](https://www.studylight.org/lexicons/hebrew/7194.html)

[Bible Hub](https://biblehub.com/str/hebrew/7194.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07194)

[Bible Bento](https://biblebento.com/dictionary/H7194.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7194/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7194.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7194)
