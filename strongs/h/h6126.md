# עַקּוּב

[ʿAqqûḇ](https://www.blueletterbible.org/lexicon/h6126)

Definition: Akkub (8x).

Part of speech: proper masculine noun

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6126)

[Study Light](https://www.studylight.org/lexicons/hebrew/6126.html)

[Bible Hub](https://biblehub.com/str/hebrew/6126.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06126)

[Bible Bento](https://biblebento.com/dictionary/H6126.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6126/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6126.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6126)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/akkub.html)

[Video Bible](https://www.videobible.com/bible-dictionary/akkub)