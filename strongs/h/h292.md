# אֲחִינָדָב

['Ăḥînāḏāḇ](https://www.blueletterbible.org/lexicon/h292)

Definition: Ahinadab (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h292)

[Study Light](https://www.studylight.org/lexicons/hebrew/292.html)

[Bible Hub](https://biblehub.com/str/hebrew/292.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0292)

[Bible Bento](https://biblebento.com/dictionary/H292.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/292/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/292.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h292)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ahinadab.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ahinadab)