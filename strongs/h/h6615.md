# פְּתַיּוּת

[pᵊṯayyûṯ](https://www.blueletterbible.org/lexicon/h6615)

Definition: simple (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6615)

[Study Light](https://www.studylight.org/lexicons/hebrew/6615.html)

[Bible Hub](https://biblehub.com/str/hebrew/6615.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06615)

[Bible Bento](https://biblebento.com/dictionary/H6615.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6615/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6615.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6615)
