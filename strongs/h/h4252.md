# מַחֲלָף

[maḥălāp̄](https://www.blueletterbible.org/lexicon/h4252)

Definition: knives (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4252)

[Study Light](https://www.studylight.org/lexicons/hebrew/4252.html)

[Bible Hub](https://biblehub.com/str/hebrew/4252.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04252)

[Bible Bento](https://biblebento.com/dictionary/H4252.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4252/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4252.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4252)
