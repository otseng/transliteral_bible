# חָתַת

[ḥāṯaṯ](https://www.blueletterbible.org/lexicon/h2865)

Definition: dismayed (27x), afraid (6x), break in pieces (6x), broken (3x), break down (2x), abolished (1x), affrighted (1x), amazed (1x), chapt (1x), confound (1x), discouraged (1x), go down (1x), beaten down (1x), scarest (1x), terrify (1x).

Part of speech: verb

Occurs 51 times in 45 verses

Hebrew: [ḥaṯ](../h/h2844.md), [ḥitâ](../h/h2847.md), [ḥăṯaṯ](../h/h2866.md), [mᵊḥitâ](../h/h4288.md)

Greek: [ptoeō](../g/g4422.md), [deiliaō](../g/g1168.md), [phobeō](../g/g5399.md), [syntribō](../g/g4937.md)

Synonyms: [fear](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Fear)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2865)

[Study Light](https://www.studylight.org/lexicons/hebrew/2865.html)

[Bible Hub](https://biblehub.com/str/hebrew/2865.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02865)

[Bible Bento](https://biblebento.com/dictionary/H2865.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2865/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2865.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2865)
