# יִדְאֲלָה

[Yiḏ'Ălâ](https://www.blueletterbible.org/lexicon/h3030)

Definition: Idalah (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3030)

[Study Light](https://www.studylight.org/lexicons/hebrew/3030.html)

[Bible Hub](https://biblehub.com/str/hebrew/3030.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03030)

[Bible Bento](https://biblebento.com/dictionary/H3030.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3030/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3030.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3030)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/idalah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/idalah)