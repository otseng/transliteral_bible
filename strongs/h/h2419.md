# חִיאֵל

[Ḥî'Ēl](https://www.blueletterbible.org/lexicon/h2419)

Definition: Hiel (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2419)

[Study Light](https://www.studylight.org/lexicons/hebrew/2419.html)

[Bible Hub](https://biblehub.com/str/hebrew/2419.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02419)

[Bible Bento](https://biblebento.com/dictionary/H2419.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2419/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2419.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2419)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hiel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/hiel)