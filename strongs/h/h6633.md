# צְבָא

[ṣᵊḇā'](https://www.blueletterbible.org/lexicon/h6633)

Definition: fight (4x), assemble (3x), mustered (2x), warred (2x), perform (1x), wait (1x).

Part of speech: verb

Occurs 14 times in 12 verses

Synonyms: [fight](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Fight)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6633)

[Study Light](https://www.studylight.org/lexicons/hebrew/6633.html)

[Bible Hub](https://biblehub.com/str/hebrew/6633.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06633)

[Bible Bento](https://biblebento.com/dictionary/H6633.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6633/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6633.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6633)
