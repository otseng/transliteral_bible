# צֹנֵא

[tsone'](https://www.blueletterbible.org/lexicon/h6792)

Definition: sheep (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

Hebrew: [tso'n](../h/h6629.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6792)

[Study Light](https://www.studylight.org/lexicons/hebrew/6792.html)

[Bible Hub](https://biblehub.com/str/hebrew/6792.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06792)

[Bible Bento](https://biblebento.com/dictionary/H6792.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6792/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6792.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6792)
