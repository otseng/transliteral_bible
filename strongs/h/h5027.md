# נָבַט

[nabat](https://www.blueletterbible.org/lexicon/h5027)

Definition: look (36x), behold (13x), consider (5x), regard (4x), see (4x), respect (3x), look down (2x), look about (1x), look back (1x).

Part of speech: verb

Occurs 69 times in 67 verses

Greek: [blepō](../g/g991.md)

Edenics: video, visage, visit, vista, visiter, witness, wait

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5027)

[Study Light](https://www.studylight.org/lexicons/hebrew/5027.html)

[Bible Hub](https://biblehub.com/str/hebrew/5027.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05027)

[Bible Bento](https://biblebento.com/dictionary/H5027.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5027/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5027.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5027)
