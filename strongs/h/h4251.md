# מַחֲלֻיִים

[maḥăluyîm](https://www.blueletterbible.org/lexicon/h4251)

Definition: disease (1x).

Part of speech: masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4251)

[Study Light](https://www.studylight.org/lexicons/hebrew/4251.html)

[Bible Hub](https://biblehub.com/str/hebrew/4251.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04251)

[Bible Bento](https://biblebento.com/dictionary/H4251.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4251/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4251.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4251)
