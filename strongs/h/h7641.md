# שִׁבֹּלֶת

[šibōleṯ](https://www.blueletterbible.org/lexicon/h7641)

Definition: ears (11x), ears of corn (3x), branches (1x), channel (1x), floods (1x), Shibboleth (1x), waterflood (with H4325) (1x).

Part of speech: feminine noun

Occurs 19 times in 16 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7641)

[Study Light](https://www.studylight.org/lexicons/hebrew/7641.html)

[Bible Hub](https://biblehub.com/str/hebrew/7641.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07641)

[Bible Bento](https://biblebento.com/dictionary/H7641.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7641/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7641.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7641)
