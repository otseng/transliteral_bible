# פּוּק

[pûq](https://www.blueletterbible.org/lexicon/h6328)

Definition: stumble (1x), move (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6328)

[Study Light](https://www.studylight.org/lexicons/hebrew/6328.html)

[Bible Hub](https://biblehub.com/str/hebrew/6328.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06328)

[Bible Bento](https://biblebento.com/dictionary/H6328.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6328/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6328.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6328)
