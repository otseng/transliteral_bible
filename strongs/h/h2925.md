# טַלְטֵלָה

[ṭalṭēlâ](https://www.blueletterbible.org/lexicon/h2925)

Definition: captivity (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Synonyms: [captivity](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Captivity)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2925)

[Study Light](https://www.studylight.org/lexicons/hebrew/2925.html)

[Bible Hub](https://biblehub.com/str/hebrew/2925.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02925)

[Bible Bento](https://biblebento.com/dictionary/H2925.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2925/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2925.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2925)
