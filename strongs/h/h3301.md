# יִפְדְּיָה

[Yip̄Dᵊyâ](https://www.blueletterbible.org/lexicon/h3301)

Definition: Iphedeiah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3301)

[Study Light](https://www.studylight.org/lexicons/hebrew/3301.html)

[Bible Hub](https://biblehub.com/str/hebrew/3301.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03301)

[Bible Bento](https://biblebento.com/dictionary/H3301.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3301/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3301.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3301)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/iphedeiah.html)