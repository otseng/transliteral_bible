# סַד

[saḏ](https://www.blueletterbible.org/lexicon/h5465)

Definition: stocks (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5465)

[Study Light](https://www.studylight.org/lexicons/hebrew/5465.html)

[Bible Hub](https://biblehub.com/str/hebrew/5465.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05465)

[Bible Bento](https://biblebento.com/dictionary/H5465.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5465/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5465.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5465)
