# בֵּית־עֵקֶד

[Bêṯ-ʿĒqeḏ](https://www.blueletterbible.org/lexicon/h1044)

Definition: shearing house (1x), non translated variant (1x).

Part of speech: proper locative noun

Occurs 4 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1044)

[Study Light](https://www.studylight.org/lexicons/hebrew/1044.html)

[Bible Hub](https://biblehub.com/str/hebrew/1044.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01044)

[Bible Bento](https://biblebento.com/dictionary/H1044.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1044/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1044.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1044)
