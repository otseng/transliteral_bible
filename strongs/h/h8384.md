# תְּאֵנָה

[tĕ'en](https://www.blueletterbible.org/lexicon/h8384)

Definition: fig tree (23x), fig (16x)

Part of speech: feminine noun

Occurs 39 times in 35 verses

Greek: [sykē](../g/g4808.md), [sykon](../g/g4810.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8384)

[Study Light](https://www.studylight.org/lexicons/hebrew/8384.html)

[Bible Hub](https://biblehub.com/str/hebrew/8384.htm)

[Morfix](https://www.morfix.co.il/en/%D7%AA%D6%BC%D6%B0%D7%90%D6%B5%D7%A0%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=08384)

[Bible Bento](https://biblebento.com/dictionary/H8384.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8384/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8384.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8384)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%AA%D7%90%D7%A0%D7%94)

[Wikipedia](https://en.wikipedia.org/wiki/Figs_in_the_Bible)

[Jewish Virtual Library](https://www.jewishvirtuallibrary.org/fig)

[One for Israel](https://www.oneforisrael.org/bible-based-teaching-from-israel/figs-in-the-bible/)

[Biblical Anthropology](http://biblicalanthropology.blogspot.com/2012/05/fig-tree-in-biblical-symbolism.html)

[Hope of Israel](https://www.hope-of-israel.org/symbolfigtree.html)