# בֶּן־חֶסֶד

[Ben-Ḥeseḏ](https://www.blueletterbible.org/lexicon/h1136)

Definition: son of Hesed (1x).

Part of speech: proper masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1136)

[Study Light](https://www.studylight.org/lexicons/hebrew/1136.html)

[Bible Hub](https://biblehub.com/str/hebrew/1136.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01136)

[Bible Bento](https://biblebento.com/dictionary/H1136.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1136/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1136.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1136)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/son.html)