# אָרַר

['arar](https://www.blueletterbible.org/lexicon/h779)

Definition: curse (62x), bitterly (1x)

Part of speech: verb

Occurs 63 times in 52 verses

Hebrew: [mᵊ'ērâ](../h/h3994.md)

Greek: [kataraomai](../g/g2672.md)

Synonyms: [curse](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Curse)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0779)

[Study Light](https://www.studylight.org/lexicons/hebrew/779.html)

[Bible Hub](https://biblehub.com/str/hebrew/779.htm)

[Morfix](https://www.morfix.co.il/en/%D7%90%D6%B8%D7%A8%D6%B7%D7%A8)

[NET Bible](http://classic.net.bible.org/strong.php?id=0779)

[Bible Bento](https://biblebento.com/dictionary/H0779.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0779/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0779.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h779)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%90%D7%A8%D7%A8)
