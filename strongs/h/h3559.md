# כּוּן

[kuwn](https://www.blueletterbible.org/lexicon/h3559)

Definition: prepare (85x), establish (58x), ready (17x), stablish (5x), provide (5x), right (5x), fixed (4x), set (4x), direct (3x), order (3x), fashion (3x), variant (2x), certain (2x), confirmed (2x), firm (2x), preparation (2x), miscellaneous (17x).

Part of speech: verb

Occurs 221 times in 210 verses

Hebrew: ['āḵēn](../h/h403.md), [kēn](../h/h3651.md), [kēn](../h/h3653.md), [kᵊnēmā'](../h/h3660.md), [māḵôn](../h/h4349.md)

Names: [Yᵊhôyāḵîn](../h/h3078.md)

Greek: [hetoimazō](../g/g2090.md), [kataskeuazō](../g/g2680.md), [oikodomeō](../g/g3618.md), [syniēmi](../g/g4920.md), [tithēmi](../g/g5087.md), [anorthoō](../g/g461.md), [hetoimos](../g/g2092.md)

Pictoral: "hand seed", plant a seed

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3559)

[Study Light](https://www.studylight.org/lexicons/hebrew/3559.html)

[Bible Hub](https://biblehub.com/str/hebrew/3559.htm)

[Morfix](https://www.morfix.co.il/en/%D7%9B%D6%BC%D7%95%D6%BC%D7%9F)

[NET Bible](http://classic.net.bible.org/strong.php?id=03559)

[Bible Bento](https://biblebento.com/dictionary/H3559.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3559/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3559.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3559)
