# יוֹבֵל

[yôḇēl](https://www.blueletterbible.org/lexicon/h3104)

Definition: jubile (21x), ram's horn (5x), trumpet (1x).

Part of speech: masculine noun

Occurs 27 times in 25 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3104)

[Study Light](https://www.studylight.org/lexicons/hebrew/3104.html)

[Bible Hub](https://biblehub.com/str/hebrew/3104.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03104)

[Bible Bento](https://biblebento.com/dictionary/H3104.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3104/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3104.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3104)
