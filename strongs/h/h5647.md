# עָבַד

[`abad](https://www.blueletterbible.org/lexicon/h5647)

Definition: serve (227x), do (15x), till (9x), servant (5x), work (5x), worshippers (5x), service (4x), dress (2x), labour (2x), ear (2x), to slave

Part of speech: verb

Occurs 293 times in 263 verses

Hebrew: ['ebed](../h/h5650.md), [ʿăḇāḏ](../h/h5652.md), [ʿăḇōḏâ](../h/h5656.md)

Names: [ʿŌḇaḏyâ](../h/h5662.md)

Greek: [ergazomai](../g/g2038.md), [latreuō](../g/g3000.md), [douleuō](../g/g1398.md), [katergazomai](../g/g2716.md)

Edenic: obedient

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5647)

[Study Light](https://www.studylight.org/lexicons/hebrew/5647.html)

[Bible Hub](https://biblehub.com/str/hebrew/5647.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A2%D6%B8%D7%91%D6%B7%D7%93)

[NET Bible](http://classic.net.bible.org/strong.php?id=05647)

[Bible Bento](https://biblebento.com/dictionary/H5647.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5647/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5647.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5647)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A2%D7%91%D7%93)
