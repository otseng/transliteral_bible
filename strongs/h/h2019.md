# הֲפַכְפַּךְ

[hăp̄aḵpaḵ](https://www.blueletterbible.org/lexicon/h2019)

Definition: froward (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2019)

[Study Light](https://www.studylight.org/lexicons/hebrew/2019.html)

[Bible Hub](https://biblehub.com/str/hebrew/2019.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02019)

[Bible Bento](https://biblebento.com/dictionary/H2019.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2019/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2019.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2019)
