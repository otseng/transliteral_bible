# יָדָע

[Yāḏāʿ](https://www.blueletterbible.org/lexicon/h3047)

Definition: Jada (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3047)

[Study Light](https://www.studylight.org/lexicons/hebrew/3047.html)

[Bible Hub](https://biblehub.com/str/hebrew/3047.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03047)

[Bible Bento](https://biblebento.com/dictionary/H3047.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3047/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3047.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3047)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jada.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jada)