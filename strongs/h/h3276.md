# יָעַל

[yāʿal](https://www.blueletterbible.org/lexicon/h3276)

Definition: profit (19x), at all (1x), set forward (1x), good (1x), profitable (1x).

Part of speech: verb

Occurs 23 times in 21 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3276)

[Study Light](https://www.studylight.org/lexicons/hebrew/3276.html)

[Bible Hub](https://biblehub.com/str/hebrew/3276.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03276)

[Bible Bento](https://biblebento.com/dictionary/H3276.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3276/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3276.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3276)
