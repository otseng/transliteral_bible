# אֶקְדָּח

['eqdāḥ](https://www.blueletterbible.org/lexicon/h688)

Definition: carbuncle (1x), fiery glow, sparkle

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0688)

[Study Light](https://www.studylight.org/lexicons/hebrew/688.html)

[Bible Hub](https://biblehub.com/str/hebrew/688.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0688)

[Bible Bento](https://biblebento.com/dictionary/H688.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/688/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/688.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h688)
