# לְוָת

[lᵊvāṯ](https://www.blueletterbible.org/lexicon/h3890)

Definition: thee (1x).

Part of speech: preposition

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3890)

[Study Light](https://www.studylight.org/lexicons/hebrew/3890.html)

[Bible Hub](https://biblehub.com/str/hebrew/3890.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03890)

[Bible Bento](https://biblebento.com/dictionary/H3890.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3890/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3890.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3890)
