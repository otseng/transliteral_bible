# מַשָּׁא

[maššā'](https://www.blueletterbible.org/lexicon/h4855)

Definition: usury (2x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4855)

[Study Light](https://www.studylight.org/lexicons/hebrew/4855.html)

[Bible Hub](https://biblehub.com/str/hebrew/4855.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04855)

[Bible Bento](https://biblebento.com/dictionary/H4855.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4855/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4855.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4855)
