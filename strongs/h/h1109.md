# בִּלְעָם

[Bilʿām](https://www.blueletterbible.org/lexicon/h1109)

Definition: Balaam (60x), Bileam (1x).

Part of speech: proper locative noun, proper masculine noun

Occurs 61 times in 57 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1109)

[Study Light](https://www.studylight.org/lexicons/hebrew/1109.html)

[Bible Hub](https://biblehub.com/str/hebrew/1109.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01109)

[Bible Bento](https://biblebento.com/dictionary/H1109.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1109/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1109.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1109)

[Wikipedia](https://en.wikipedia.org/wiki/Balaam)

[Biblical Archaeology](https://www.biblicalarchaeology.org/daily/biblical-topics/hebrew-bible/balaam-part-one/)

[My Jewish Learning](https://www.myjewishlearning.com/article/balaam-the-prophet/)

[Jewish Virtual Library](https://www.jewishvirtuallibrary.org/balaam)

[Jewish Encyclopedia](https://www.jewishencyclopedia.com/articles/2395-balaam)

[Chabad](https://www.chabad.org/library/article_cdo/aid/3715894/jewish/Balaam-and-Balak-The-Full-Story.htm)

[Catholic.com](https://www.catholic.com/encyclopedia/balaam)

[The Torah](https://www.thetorah.com/article/balaam-the-seer-from-the-bible-to-the-deir-alla-inscription)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/balaam.html)

[Video Bible](https://www.videobible.com/bible-dictionary/balaam)