# מֶרְכָּבָה

[merkāḇâ](https://www.blueletterbible.org/lexicon/h4818)

Definition: chariot (44x).

Part of speech: feminine noun

Occurs 44 times in 41 verses

Hebrew: [merkāḇ](../h/h4817.md), [rakāḇ](../h/h7395.md), [riḵbâ](../h/h7396.md), [rᵊḵûḇ](../h/h7398.md)

Greek: [harma](../g/g716.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4818)

[Study Light](https://www.studylight.org/lexicons/hebrew/4818.html)

[Bible Hub](https://biblehub.com/str/hebrew/4818.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04818)

[Bible Bento](https://biblebento.com/dictionary/H4818.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4818/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4818.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4818)
