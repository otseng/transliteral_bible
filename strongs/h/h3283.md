# יָעֵן

[yāʿēn](https://www.blueletterbible.org/lexicon/h3283)

Definition: ostrich (1x).

Part of speech: feminine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3283)

[Study Light](https://www.studylight.org/lexicons/hebrew/3283.html)

[Bible Hub](https://biblehub.com/str/hebrew/3283.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03283)

[Bible Bento](https://biblebento.com/dictionary/H3283.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3283/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3283.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3283)
