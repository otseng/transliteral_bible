# רִצְפָּה

[Riṣpâ](https://www.blueletterbible.org/lexicon/h7532)

Definition: Rizpah (4x).

Part of speech: proper feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7532)

[Study Light](https://www.studylight.org/lexicons/hebrew/7532.html)

[Bible Hub](https://biblehub.com/str/hebrew/7532.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07532)

[Bible Bento](https://biblebento.com/dictionary/H7532.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7532/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7532.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7532)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/R/rizpah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/rizpah)