# בְּרִיחַ

[bᵊrîaḥ](https://www.blueletterbible.org/lexicon/h1280)

Definition: bar (40x), fugitive (1x), bolt, prison bar

Part of speech: masculine noun

Occurs 42 times in 37 verses

Hebrew: [bāraḥ](../h/h1272.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1280)

[Study Light](https://www.studylight.org/lexicons/hebrew/1280.html)

[Bible Hub](https://biblehub.com/str/hebrew/1280.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01280)

[Bible Bento](https://biblebento.com/dictionary/H1280.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1280/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1280.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1280)
