# שָׁחַר

[šāḥar](https://www.blueletterbible.org/lexicon/h7835)

Definition: black (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7835)

[Study Light](https://www.studylight.org/lexicons/hebrew/7835.html)

[Bible Hub](https://biblehub.com/str/hebrew/7835.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07835)

[Bible Bento](https://biblebento.com/dictionary/H7835.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7835/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7835.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7835)
