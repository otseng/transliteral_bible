# תּוּר

[tûr](https://www.blueletterbible.org/lexicon/h8446)

Definition: search (11x), search out (3x), spy out (2x), seek (2x), chapmen (with H582) (1x), descry (1x), espied (1x), excellent (1x), merchantmen (with H582) (1x).

Part of speech: verb

Occurs 23 times in 22 verses

Hebrew: [tôr](../h/h8447.md), [tôr](../h/h8448.md), [tôrâ](../h/h8452.md)

Pictoral: mark of man

Synonyms: [seek](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Seek)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8446)

[Study Light](https://www.studylight.org/lexicons/hebrew/8446.html)

[Bible Hub](https://biblehub.com/str/hebrew/8446.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08446)

[Bible Bento](https://biblebento.com/dictionary/H8446.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8446/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8446.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8446)
