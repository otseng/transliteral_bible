# קֹרֵא

[Qōrē'](https://www.blueletterbible.org/lexicon/h6981)

Definition: Kore (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6981)

[Study Light](https://www.studylight.org/lexicons/hebrew/6981.html)

[Bible Hub](https://biblehub.com/str/hebrew/6981.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06981)

[Bible Bento](https://biblebento.com/dictionary/H6981.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6981/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6981.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6981)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/K/kore.html)

[Video Bible](https://www.videobible.com/bible-dictionary/kore)