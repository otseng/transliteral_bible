# נַעַל

[naʿal](https://www.blueletterbible.org/lexicon/h5275)

Definition: shoe (20x), dryshod (1x), shoelatchet (with H8288) (1x).

Part of speech: feminine noun

Occurs 22 times in 22 verses

Hebrew: [nāʿal](../h/h5274.md)

Greek: [hypodēma](../g/g5266.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5275)

[Study Light](https://www.studylight.org/lexicons/hebrew/5275.html)

[Bible Hub](https://biblehub.com/str/hebrew/5275.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05275)

[Bible Bento](https://biblebento.com/dictionary/H5275.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5275/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5275.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5275)
