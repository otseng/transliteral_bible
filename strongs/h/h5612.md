# סֵפֶר

[sēp̄er](https://www.blueletterbible.org/lexicon/h5612)

Definition: book (138x), letter (29x), evidence (8x), bill (4x), learning (2x), register (1x), learned (with H3045) (1x), scroll (1x), legal document, certificate of divorce, deed of purchase, indictment, sign

Part of speech: masculine/feminine noun

Occurs 188 times in 174 verses

Hebrew: [sāp̄ar](../h/h5608.md), [sᵊp̄ar](../h/h5609.md), [sᵊp̄ār](../h/h5610.md), [sᵊp̄ōrâ](../h/h5615.md)

Greek: [biblion](../g/g975.md)

Edenics: gospel

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5612)

[Study Light](https://www.studylight.org/lexicons/hebrew/5612.html)

[Bible Hub](https://biblehub.com/str/hebrew/5612.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05612)

[Bible Bento](https://biblebento.com/dictionary/H5612.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5612/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5612.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5612)
