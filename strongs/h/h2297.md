# חַד

[ḥaḏ](https://www.blueletterbible.org/lexicon/h2297)

Definition: one (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2297)

[Study Light](https://www.studylight.org/lexicons/hebrew/2297.html)

[Bible Hub](https://biblehub.com/str/hebrew/2297.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02297)

[Bible Bento](https://biblebento.com/dictionary/H2297.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2297/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2297.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2297)
