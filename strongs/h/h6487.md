# פִּקָּדוֹן

[piqqāḏôn](https://www.blueletterbible.org/lexicon/h6487)

Definition: that which was delivered (2x), store (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6487)

[Study Light](https://www.studylight.org/lexicons/hebrew/6487.html)

[Bible Hub](https://biblehub.com/str/hebrew/6487.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06487)

[Bible Bento](https://biblebento.com/dictionary/H6487.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6487/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6487.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6487)
