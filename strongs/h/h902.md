# בִּגְוַי

[Biḡvay](https://www.blueletterbible.org/lexicon/h902)

Definition: Bigvai (6x).

Part of speech: proper masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h902)

[Study Light](https://www.studylight.org/lexicons/hebrew/902.html)

[Bible Hub](https://biblehub.com/str/hebrew/902.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0902)

[Bible Bento](https://biblebento.com/dictionary/H902.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/902/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/902.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h902)

[Video Bible](https://www.videobible.com/bible-dictionary/bigvai)