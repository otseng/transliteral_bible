# כָּסַל

[kāsal](https://www.blueletterbible.org/lexicon/h3688)

Definition: foolish (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [kᵊsîl](../h/h3684.md), [kᵊsîlûṯ](../h/h3687.md), [kesel](../h/h3689.md), [kislâ](../h/h3690.md), [sāḵāl](../h/h5530.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3688)

[Study Light](https://www.studylight.org/lexicons/hebrew/3688.html)

[Bible Hub](https://biblehub.com/str/hebrew/3688.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03688)

[Bible Bento](https://biblebento.com/dictionary/H3688.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3688/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3688.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3688)
