# דֶּשֶׁן

[dešen](https://www.blueletterbible.org/lexicon/h1880)

Definition: ashes (8x), fatness (7x).

Part of speech: masculine noun

Occurs 15 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1880)

[Study Light](https://www.studylight.org/lexicons/hebrew/1880.html)

[Bible Hub](https://biblehub.com/str/hebrew/1880.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01880)

[Bible Bento](https://biblebento.com/dictionary/H1880.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1880/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1880.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1880)
