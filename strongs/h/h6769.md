# צִלְּתַי

[Ṣillᵊṯay](https://www.blueletterbible.org/lexicon/h6769)

Definition: Zilthai (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6769)

[Study Light](https://www.studylight.org/lexicons/hebrew/6769.html)

[Bible Hub](https://biblehub.com/str/hebrew/6769.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06769)

[Bible Bento](https://biblebento.com/dictionary/H6769.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6769/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6769.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6769)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zilthai.html)