# כָּהַן

[kāhan](https://www.blueletterbible.org/lexicon/h3547)

Definition: priest's office (20x), decketh (1x), office of a priest (1x), priest (1x).

Part of speech: verb

Occurs 23 times in 23 verses

Hebrew: [kōhēn](../h/h3548.md)

Greek: [hiereus](../g/g2409.md), [leitourgeō](../g/g3008.md), [archiereus](../g/g749.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3547)

[Study Light](https://www.studylight.org/lexicons/hebrew/3547.html)

[Bible Hub](https://biblehub.com/str/hebrew/3547.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03547)

[Bible Bento](https://biblebento.com/dictionary/H3547.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3547/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3547.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3547)
