# מֶסֶךְ

[meseḵ](https://www.blueletterbible.org/lexicon/h4538)

Definition: mixture (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4538)

[Study Light](https://www.studylight.org/lexicons/hebrew/4538.html)

[Bible Hub](https://biblehub.com/str/hebrew/4538.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04538)

[Bible Bento](https://biblebento.com/dictionary/H4538.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4538/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4538.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4538)
