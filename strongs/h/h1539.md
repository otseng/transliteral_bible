# גֶּלֶד

[geleḏ](https://www.blueletterbible.org/lexicon/h1539)

Definition: skin (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1539)

[Study Light](https://www.studylight.org/lexicons/hebrew/1539.html)

[Bible Hub](https://biblehub.com/str/hebrew/1539.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01539)

[Bible Bento](https://biblebento.com/dictionary/H1539.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1539/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1539.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1539)
