# קוּץ

[qûṣ](https://www.blueletterbible.org/lexicon/h6973)

Definition: abhor (3x), weary (2x), loath (1x), distressed (1x), vex (1x), grieved (1x).

Part of speech: verb

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6973)

[Study Light](https://www.studylight.org/lexicons/hebrew/6973.html)

[Bible Hub](https://biblehub.com/str/hebrew/6973.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06973)

[Bible Bento](https://biblebento.com/dictionary/H6973.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6973/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6973.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6973)
