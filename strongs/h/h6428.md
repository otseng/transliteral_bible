# פָּלַשׁ

[pālaš](https://www.blueletterbible.org/lexicon/h6428)

Definition: wallow (3x), roll (1x), variant (1x).

Part of speech: verb

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6428)

[Study Light](https://www.studylight.org/lexicons/hebrew/6428.html)

[Bible Hub](https://biblehub.com/str/hebrew/6428.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06428)

[Bible Bento](https://biblebento.com/dictionary/H6428.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6428/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6428.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6428)
