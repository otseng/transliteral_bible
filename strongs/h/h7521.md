# רָצָה

[ratsah](https://www.blueletterbible.org/lexicon/h7521)

Definition: accept (22x), please (6x), pleasure (6x), delight (5x), enjoy (4x), favourable (3x), acceptable (1x), accomplish (1x), affection (1x), approve (1x)

Part of speech: verb

Occurs 57 times in 55 verses

Hebrew: [ratsown](../h/h7522.md)

Greek: [dokimos](../g/g1384.md), [apodechomai](../g/g588.md), [euodoo](../g/g2137.md), [thelō](../g/g2309.md), [agapaō](../g/g25.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7521)

[Study Light](https://www.studylight.org/lexicons/hebrew/7521.html)

[Bible Hub](https://biblehub.com/str/hebrew/7521.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A8%D6%B8%D7%A6%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=07521)

[Bible Bento](https://biblebento.com/dictionary/H7521.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7521/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7521.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7521)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A8%D7%A6%D7%94)
