# זָבָד

[Zāḇāḏ](https://www.blueletterbible.org/lexicon/h2066)

Definition: Zabad (8x).

Part of speech: proper masculine noun

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2066)

[Study Light](https://www.studylight.org/lexicons/hebrew/2066.html)

[Bible Hub](https://biblehub.com/str/hebrew/2066.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02066)

[Bible Bento](https://biblebento.com/dictionary/H2066.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2066/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2066.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2066)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zabad.html)

[Video Bible](https://www.videobible.com/bible-dictionary/zabad)