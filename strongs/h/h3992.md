# מָאַר

[mā'ar](https://www.blueletterbible.org/lexicon/h3992)

Definition: fretting (3x), pricking (1x).

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3992)

[Study Light](https://www.studylight.org/lexicons/hebrew/3992.html)

[Bible Hub](https://biblehub.com/str/hebrew/3992.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03992)

[Bible Bento](https://biblebento.com/dictionary/H3992.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3992/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3992.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3992)
