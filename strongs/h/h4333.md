# מִישָׁאֵל

[Mîšā'ēl](https://www.blueletterbible.org/lexicon/h4333)

Definition: Mishael (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4333)

[Study Light](https://www.studylight.org/lexicons/hebrew/4333.html)

[Bible Hub](https://biblehub.com/str/hebrew/4333.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04333)

[Bible Bento](https://biblebento.com/dictionary/H4333.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4333/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4333.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4333)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/mishael.html)

[Video Bible](https://www.videobible.com/bible-dictionary/mishael)