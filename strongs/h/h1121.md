# בֵּן

[ben](https://www.blueletterbible.org/lexicon/h1121)

Definition: son (2,978x), children (1,568x), old (135x), first (51x), man (20x), young (18x), young (with H1241) (17x), child (10x), stranger (10x), people (5x), miscellaneous (92x), corn, breed

Part of speech: masculine noun

Occurs 4,906 times in 3,654 verses

Hebrew: [bēn](../h/h1123.md), [bar](../h/h1247.md), [bath](../h/h1323.md)

Names: [Binyāmîn](../h/h1144.md)

Greek: [teknon](../g/g5043.md), [huios](../g/g5207.md), [paidion](../g/g3813.md)

Pictoral: house seed

Synonyms: [people](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#People)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1121)

[Study Light](https://www.studylight.org/lexicons/hebrew/1121.html)

[Bible Hub](https://biblehub.com/str/hebrew/1121.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%91%D7%9F)

[NET Bible](http://classic.net.bible.org/strong.php?id=01121)

[Bible Bento](https://biblebento.com/dictionary/H1121.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1121/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1121.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1121)
