# תַּעֲצֻמוֹת

[taʿăṣumôṯ](https://www.blueletterbible.org/lexicon/h8592)

Definition: power (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8592)

[Study Light](https://www.studylight.org/lexicons/hebrew/8592.html)

[Bible Hub](https://biblehub.com/str/hebrew/8592.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08592)

[Bible Bento](https://biblebento.com/dictionary/H8592.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8592/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8592.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8592)
