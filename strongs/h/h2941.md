# טַעַם

[ṭaʿam](https://www.blueletterbible.org/lexicon/h2941)

Definition: commandment (2x), matter (1x), commanded (1x), accounts (1x).

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2941)

[Study Light](https://www.studylight.org/lexicons/hebrew/2941.html)

[Bible Hub](https://biblehub.com/str/hebrew/2941.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02941)

[Bible Bento](https://biblebento.com/dictionary/H2941.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2941/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2941.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2941)
