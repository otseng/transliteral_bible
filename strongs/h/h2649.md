# חִפָּזוֹן

[ḥipāzôn](https://www.blueletterbible.org/lexicon/h2649)

Definition: haste (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2649)

[Study Light](https://www.studylight.org/lexicons/hebrew/2649.html)

[Bible Hub](https://biblehub.com/str/hebrew/2649.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02649)

[Bible Bento](https://biblebento.com/dictionary/H2649.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2649/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2649.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2649)
