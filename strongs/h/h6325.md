# פּוּנֹן

[Pûnōn](https://www.blueletterbible.org/lexicon/h6325)

Definition: Punon (2x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6325)

[Study Light](https://www.studylight.org/lexicons/hebrew/6325.html)

[Bible Hub](https://biblehub.com/str/hebrew/6325.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06325)

[Bible Bento](https://biblebento.com/dictionary/H6325.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6325/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6325.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6325)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/punon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/punon)