# שֹׁבֶל

[šōḇel](https://www.blueletterbible.org/lexicon/h7640)

Definition: leg (1x), flowing skirt, train

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7640)

[Study Light](https://www.studylight.org/lexicons/hebrew/7640.html)

[Bible Hub](https://biblehub.com/str/hebrew/7640.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07640)

[Bible Bento](https://biblebento.com/dictionary/H7640.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7640/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7640.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7640)
