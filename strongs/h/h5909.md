# עַכְבָּר

[ʿaḵbār](https://www.blueletterbible.org/lexicon/h5909)

Definition: mouse (6x).

Part of speech: masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5909)

[Study Light](https://www.studylight.org/lexicons/hebrew/5909.html)

[Bible Hub](https://biblehub.com/str/hebrew/5909.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05909)

[Bible Bento](https://biblebento.com/dictionary/H5909.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5909/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5909.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5909)
