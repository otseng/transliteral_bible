# עָנַד

[ʿānaḏ](https://www.blueletterbible.org/lexicon/h6029)

Definition: tie (1x), bind (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6029)

[Study Light](https://www.studylight.org/lexicons/hebrew/6029.html)

[Bible Hub](https://biblehub.com/str/hebrew/6029.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06029)

[Bible Bento](https://biblebento.com/dictionary/H6029.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6029/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6029.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6029)
