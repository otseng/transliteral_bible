# יַעֲקָן

[YaʿĂqān](https://www.blueletterbible.org/lexicon/h3292)

Definition: Jakan (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3292)

[Study Light](https://www.studylight.org/lexicons/hebrew/3292.html)

[Bible Hub](https://biblehub.com/str/hebrew/3292.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03292)

[Bible Bento](https://biblebento.com/dictionary/H3292.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3292/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3292.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3292)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jakan.html)