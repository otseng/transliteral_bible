# מָרָה

[Mārâ](https://www.blueletterbible.org/lexicon/h4785)

Definition: Marah (5x), "bitter", the spring with bitter water which was 3 days travel from the crossing place of the Red Sea in the peninsula of Sinai

Part of speech: proper feminine noun

Occurs 5 times in 3 verses

Hebrew: [mar](../h/h4751.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4785)

[Study Light](https://www.studylight.org/lexicons/hebrew/4785.html)

[Bible Hub](https://biblehub.com/str/hebrew/4785.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04785)

[Bible Bento](https://biblebento.com/dictionary/H4785.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4785/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4785.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4785)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/marah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/marah)