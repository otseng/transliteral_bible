# יָבָל

[Yāḇāl](https://www.blueletterbible.org/lexicon/h2988)

Definition: stream (1x), course (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2988)

[Study Light](https://www.studylight.org/lexicons/hebrew/2988.html)

[Bible Hub](https://biblehub.com/str/hebrew/2988.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02988)

[Bible Bento](https://biblebento.com/dictionary/H2988.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2988/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2988.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2988)
