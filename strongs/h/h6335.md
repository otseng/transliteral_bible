# פּוּשׁ

[pûš](https://www.blueletterbible.org/lexicon/h6335)

Definition: spread (1x), grow up (1x), grown fat (1x), scattered (1x).

Part of speech: verb

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6335)

[Study Light](https://www.studylight.org/lexicons/hebrew/6335.html)

[Bible Hub](https://biblehub.com/str/hebrew/6335.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06335)

[Bible Bento](https://biblebento.com/dictionary/H6335.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6335/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6335.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6335)
