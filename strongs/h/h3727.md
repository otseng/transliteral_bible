# כַּפֹּרֶת

[kapōreṯ](https://www.blueletterbible.org/lexicon/h3727)

Definition: mercy seat (26x), mercy seatward (1x).

Part of speech: feminine noun

Occurs 27 times in 22 verses

Hebrew: [kāp̄ar](../h/h3722.md)

Greek: [hilastērion](../g/g2435.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3727)

[Study Light](https://www.studylight.org/lexicons/hebrew/3727.html)

[Bible Hub](https://biblehub.com/str/hebrew/3727.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03727)

[Bible Bento](https://biblebento.com/dictionary/H3727.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3727/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3727.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3727)

[Wikipedia](https://en.wikipedia.org/wiki/Mercy_seat)

