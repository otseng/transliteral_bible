# מִדְיָן

[Miḏyān](https://www.blueletterbible.org/lexicon/h4080)

Definition: Midian (39x), Midianite (20x), "strife"

Part of speech: proper locative noun, proper masculine noun

Occurs 59 times in 55 verses

Hebrew: [miḏyān](../h/h4079.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4080)

[Study Light](https://www.studylight.org/lexicons/hebrew/4080.html)

[Bible Hub](https://biblehub.com/str/hebrew/4080.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04080)

[Bible Bento](https://biblebento.com/dictionary/H4080.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4080/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4080.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4080)

[Wikipedia](https://en.wikipedia.org/wiki/Midian)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/midian.html)

[Video Bible](https://www.videobible.com/bible-dictionary/midian)