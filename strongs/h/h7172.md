# קַרְקַע

[qarqaʿ](https://www.blueletterbible.org/lexicon/h7172)

Definition: floor (6x), other (1x), bottom (1x).

Part of speech: masculine noun

Occurs 8 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7172)

[Study Light](https://www.studylight.org/lexicons/hebrew/7172.html)

[Bible Hub](https://biblehub.com/str/hebrew/7172.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07172)

[Bible Bento](https://biblebento.com/dictionary/H7172.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7172/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7172.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7172)
