# תַּאֲלָה

[ta'ălâ](https://www.blueletterbible.org/lexicon/h8381)

Definition: curse (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Synonyms: [curse](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Curse)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8381)

[Study Light](https://www.studylight.org/lexicons/hebrew/8381.html)

[Bible Hub](https://biblehub.com/str/hebrew/8381.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08381)

[Bible Bento](https://biblebento.com/dictionary/H8381.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8381/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8381.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8381)
