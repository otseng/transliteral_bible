# יָקְתְאֵל

[Yāqṯᵊ'Ēl](https://www.blueletterbible.org/lexicon/h3371)

Definition: Joktheel (2x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3371)

[Study Light](https://www.studylight.org/lexicons/hebrew/3371.html)

[Bible Hub](https://biblehub.com/str/hebrew/3371.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03371)

[Bible Bento](https://biblebento.com/dictionary/H3371.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3371/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3371.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3371)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/joktheel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/joktheel)