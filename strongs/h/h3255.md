# יְסַף

[yᵊsap̄](https://www.blueletterbible.org/lexicon/h3255)

Definition: added (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3255)

[Study Light](https://www.studylight.org/lexicons/hebrew/3255.html)

[Bible Hub](https://biblehub.com/str/hebrew/3255.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03255)

[Bible Bento](https://biblebento.com/dictionary/H3255.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3255/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3255.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3255)
