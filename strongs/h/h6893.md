# קָאַת

[qā'aṯ](https://www.blueletterbible.org/lexicon/h6893)

Definition: pelican (3x), cormorant (2x).

Part of speech: feminine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6893)

[Study Light](https://www.studylight.org/lexicons/hebrew/6893.html)

[Bible Hub](https://biblehub.com/str/hebrew/6893.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06893)

[Bible Bento](https://biblebento.com/dictionary/H6893.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6893/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6893.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6893)
