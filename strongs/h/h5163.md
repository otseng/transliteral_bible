# נַחַם

[Naḥam](https://www.blueletterbible.org/lexicon/h5163)

Definition: Naham (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5163)

[Study Light](https://www.studylight.org/lexicons/hebrew/5163.html)

[Bible Hub](https://biblehub.com/str/hebrew/5163.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05163)

[Bible Bento](https://biblebento.com/dictionary/H5163.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5163/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5163.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5163)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/naham.html)

[Video Bible](https://www.videobible.com/bible-dictionary/naham)