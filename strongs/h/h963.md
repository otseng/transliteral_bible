# בִּזָּיוֹן

[bizzāyôn](https://www.blueletterbible.org/lexicon/h963)

Definition: contempt (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

Hebrew: [bazah](../h/h959.md), [bāzô](../h/h960.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h963)

[Study Light](https://www.studylight.org/lexicons/hebrew/963.html)

[Bible Hub](https://biblehub.com/str/hebrew/963.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0963)

[Bible Bento](https://biblebento.com/dictionary/H963.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/963/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/963.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h963)
