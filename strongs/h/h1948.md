# הוֹלֵלוּת

[hôlēlûṯ](https://www.blueletterbible.org/lexicon/h1948)

Definition: madness (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1948)

[Study Light](https://www.studylight.org/lexicons/hebrew/1948.html)

[Bible Hub](https://biblehub.com/str/hebrew/1948.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01948)

[Bible Bento](https://biblebento.com/dictionary/H1948.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1948/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1948.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1948)
