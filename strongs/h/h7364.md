# רָחַץ

[rāḥaṣ](https://www.blueletterbible.org/lexicon/h7364)

Definition: wash (53x), bathe (18x), wash away (1x).

Part of speech: verb

Occurs 72 times in 71 verses

Hebrew: [raḥṣâ](../h/h7367.md)

Greek: [niptō](../g/g3538.md)

Edenics: rinse

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7364)

[Study Light](https://www.studylight.org/lexicons/hebrew/7364.html)

[Bible Hub](https://biblehub.com/str/hebrew/7364.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07364)

[Bible Bento](https://biblebento.com/dictionary/H7364.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7364/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7364.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7364)
