# מַחְצֵב

[maḥṣēḇ](https://www.blueletterbible.org/lexicon/h4274)

Definition: hewn (2x), hewed (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4274)

[Study Light](https://www.studylight.org/lexicons/hebrew/4274.html)

[Bible Hub](https://biblehub.com/str/hebrew/4274.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04274)

[Bible Bento](https://biblebento.com/dictionary/H4274.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4274/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4274.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4274)
