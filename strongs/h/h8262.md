# שָׁקַץ

[šāqaṣ](https://www.blueletterbible.org/lexicon/h8262)

Definition: abomination (2x), abominable (2x), abhor (1x), utterly (1x), detest (1x).

Part of speech: verb

Occurs 7 times in 6 verses

Hebrew: [šiqqûṣ](../h/h8251.md), [šeqeṣ](../h/h8263.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8262)

[Study Light](https://www.studylight.org/lexicons/hebrew/8262.html)

[Bible Hub](https://biblehub.com/str/hebrew/8262.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08262)

[Bible Bento](https://biblebento.com/dictionary/H8262.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8262/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8262.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8262)
