# כְּרָא

[kᵊrā'](https://www.blueletterbible.org/lexicon/h3735)

Definition: grieved (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3735)

[Study Light](https://www.studylight.org/lexicons/hebrew/3735.html)

[Bible Hub](https://biblehub.com/str/hebrew/3735.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03735)

[Bible Bento](https://biblebento.com/dictionary/H3735.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3735/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3735.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3735)
