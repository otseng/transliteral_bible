# חֹל

[ḥōl](https://www.blueletterbible.org/lexicon/h2455)

Definition: profane (2x), profane place (2x), common (2x), unholy (1x).

Part of speech: masculine noun

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2455)

[Study Light](https://www.studylight.org/lexicons/hebrew/2455.html)

[Bible Hub](https://biblehub.com/str/hebrew/2455.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02455)

[Bible Bento](https://biblebento.com/dictionary/H2455.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2455/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2455.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2455)
