# אֶצְבַּע

['etsba`](https://www.blueletterbible.org/lexicon/h676)

Definition: finger (32x), toe

Part of speech: feminine noun

Occurs 32 times in 28 verses

Hebrew: [`etsem](../h/h6106.md), ['ets](../h/h6086.md)

Edenics: stab, mitten

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0676)

[Study Light](https://www.studylight.org/lexicons/hebrew/676.html)

[Bible Hub](https://biblehub.com/str/hebrew/676.htm)

[Morfix](https://www.morfix.co.il/en/%D7%90%D6%B6%D7%A6%D6%B0%D7%91%D6%BC%D6%B7%D7%A2)

[NET Bible](http://classic.net.bible.org/strong.php?id=0676)

[Bible Bento](https://biblebento.com/dictionary/H0676.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0676/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0676.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h676)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%90%D7%A6%D7%91%D7%A2)
