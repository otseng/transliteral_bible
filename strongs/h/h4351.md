# מְכוֹרָה

[mᵊḵôrâ](https://www.blueletterbible.org/lexicon/h4351)

Definition: birth (1x), nativity (1x), habitation (1x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4351)

[Study Light](https://www.studylight.org/lexicons/hebrew/4351.html)

[Bible Hub](https://biblehub.com/str/hebrew/4351.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04351)

[Bible Bento](https://biblebento.com/dictionary/H4351.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4351/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4351.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4351)
