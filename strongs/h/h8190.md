# שַׁעַשְׁגַּז

[ŠaʿAšgaz](https://www.blueletterbible.org/lexicon/h8190)

Definition: Shaashgaz (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8190)

[Study Light](https://www.studylight.org/lexicons/hebrew/8190.html)

[Bible Hub](https://biblehub.com/str/hebrew/8190.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08190)

[Bible Bento](https://biblebento.com/dictionary/H8190.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8190/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8190.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8190)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shaashgaz.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shaashgaz)