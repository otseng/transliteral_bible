# יְהוֹאָשׁ

[Yᵊhô'Āš](https://www.blueletterbible.org/lexicon/h3060)

Definition: Jehoash (17x), "given by the Lord"

Part of speech: proper masculine noun

Occurs 17 times in 16 verses

Hebrew: [Yĕhovah](../h/h3068.md), ['esh](../h/h784.md)

Names: [Yô'Āš](../h/h3101.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3060)

[Study Light](https://www.studylight.org/lexicons/hebrew/3060.html)

[Bible Hub](https://biblehub.com/str/hebrew/3060.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03060)

[Bible Bento](https://biblebento.com/dictionary/H3060.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3060/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3060.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3060)

[Wikipedia](https://en.wikipedia.org/wiki/Jehoash_of_Judah)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jehoash.html)