# עַמְרָמִי

[ʿAmrāmî](https://www.blueletterbible.org/lexicon/h6020)

Definition: Amramites (2x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6020)

[Study Light](https://www.studylight.org/lexicons/hebrew/6020.html)

[Bible Hub](https://biblehub.com/str/hebrew/6020.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06020)

[Bible Bento](https://biblebento.com/dictionary/H6020.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6020/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6020.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6020)
