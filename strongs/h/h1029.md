# בֵּית הַשִׁטָּה

[Bêṯ Hašiṭṭâ](https://www.blueletterbible.org/lexicon/h1029)

Definition: Bethshittah (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1029)

[Study Light](https://www.studylight.org/lexicons/hebrew/1029.html)

[Bible Hub](https://biblehub.com/str/hebrew/1029.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01029)

[Bible Bento](https://biblebento.com/dictionary/H1029.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1029/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1029.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1029)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bethshittah.html)