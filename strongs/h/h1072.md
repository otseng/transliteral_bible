# בִּכְרָה

[biḵrâ](https://www.blueletterbible.org/lexicon/h1072)

Definition: dromedary (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1072)

[Study Light](https://www.studylight.org/lexicons/hebrew/1072.html)

[Bible Hub](https://biblehub.com/str/hebrew/1072.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01072)

[Bible Bento](https://biblebento.com/dictionary/H1072.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1072/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1072.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1072)
