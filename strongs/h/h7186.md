# קָשֶׁה

[qāšê](https://www.blueletterbible.org/lexicon/h7186)

Definition: stiffnecked (with H6203) (6x), hard (5x), roughly (5x), cruel (3x), grievous (3x), sore (2x), churlish (1x), hardhearted (1x), heavy (1x), miscellaneous (9x).

Part of speech: adjective

Occurs 36 times in 36 verses

Hebrew: [qāšâ](../h/h7185.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7186)

[Study Light](https://www.studylight.org/lexicons/hebrew/7186.html)

[Bible Hub](https://biblehub.com/str/hebrew/7186.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07186)

[Bible Bento](https://biblebento.com/dictionary/H7186.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7186/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7186.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7186)
