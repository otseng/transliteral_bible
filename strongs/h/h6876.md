# צֹרִי

[ṣōrî](https://www.blueletterbible.org/lexicon/h6876)

Definition: ...of Tyre (5x).

Part of speech: adjective patrial

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6876)

[Study Light](https://www.studylight.org/lexicons/hebrew/6876.html)

[Bible Hub](https://biblehub.com/str/hebrew/6876.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06876)

[Bible Bento](https://biblebento.com/dictionary/H6876.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6876/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6876.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6876)
