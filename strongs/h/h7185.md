# קָשָׁה

[qāšâ](https://www.blueletterbible.org/lexicon/h7185)

Definition: harden (12x), hard (4x), stiffnecked (with H6203) (2x), grievous (2x), miscellaneous (8x), to be hard pressed, make burdensome, make grievous

Part of speech: verb

Occurs 28 times in 28 verses

Hebrew: [qāšê](../h/h7186.md), [qesheth](../h/h7198.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7185)

[Study Light](https://www.studylight.org/lexicons/hebrew/7185.html)

[Bible Hub](https://biblehub.com/str/hebrew/7185.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07185)

[Bible Bento](https://biblebento.com/dictionary/H7185.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7185/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7185.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7185)
