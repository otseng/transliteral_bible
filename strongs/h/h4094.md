# מַדְקָרָה

[maḏqārâ](https://www.blueletterbible.org/lexicon/h4094)

Definition: piercings (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4094)

[Study Light](https://www.studylight.org/lexicons/hebrew/4094.html)

[Bible Hub](https://biblehub.com/str/hebrew/4094.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04094)

[Bible Bento](https://biblebento.com/dictionary/H4094.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4094/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4094.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4094)
