# חָנֵס

[ḥānēs](https://www.blueletterbible.org/lexicon/h2609)

Definition: Hanes (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2609)

[Study Light](https://www.studylight.org/lexicons/hebrew/2609.html)

[Bible Hub](https://biblehub.com/str/hebrew/2609.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02609)

[Bible Bento](https://biblebento.com/dictionary/H2609.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2609/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2609.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2609)

[Video Bible](https://www.videobible.com/bible-dictionary/hanes)