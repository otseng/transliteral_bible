# פַּרְצִי

[parṣî](https://www.blueletterbible.org/lexicon/h6558)

Definition: Pharzites (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6558)

[Study Light](https://www.studylight.org/lexicons/hebrew/6558.html)

[Bible Hub](https://biblehub.com/str/hebrew/6558.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06558)

[Bible Bento](https://biblebento.com/dictionary/H6558.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6558/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6558.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6558)
