# קְצָת

[qᵊṣāṯ](https://www.blueletterbible.org/lexicon/h7118)

Definition: end (2x), partly (2x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7118)

[Study Light](https://www.studylight.org/lexicons/hebrew/7118.html)

[Bible Hub](https://biblehub.com/str/hebrew/7118.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07118)

[Bible Bento](https://biblebento.com/dictionary/H7118.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7118/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7118.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7118)
