# שַׁמָּה

[šammâ](https://www.blueletterbible.org/lexicon/h8047)

Definition: astonishment (13x), desolation (12x), desolate (10x), waste (3x), wonderful (1x).

Part of speech: feminine noun

Occurs 40 times in 39 verses

Hebrew: [šāmēm](../h/h8074.md)

Names: [Šammâ](../h/h8048.md)

Greek: [atimazō](../g/g818.md), [erēmoō](../g/g2049.md), [apollymi](../g/g622.md), [apōleia](../g/g684.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8047)

[Study Light](https://www.studylight.org/lexicons/hebrew/8047.html)

[Bible Hub](https://biblehub.com/str/hebrew/8047.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08047)

[Bible Bento](https://biblebento.com/dictionary/H8047.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8047/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8047.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8047)
