# מַשֶּׁה

[maššê](https://www.blueletterbible.org/lexicon/h4874)

Definition: creditor (with H1167) (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4874)

[Study Light](https://www.studylight.org/lexicons/hebrew/4874.html)

[Bible Hub](https://biblehub.com/str/hebrew/4874.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04874)

[Bible Bento](https://biblebento.com/dictionary/H4874.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4874/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4874.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4874)
