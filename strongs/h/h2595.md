# חֲנִית

[ḥănîṯ](https://www.blueletterbible.org/lexicon/h2595)

Definition: spear (41x), javelin (6x).

Part of speech: feminine noun

Occurs 48 times in 40 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2595)

[Study Light](https://www.studylight.org/lexicons/hebrew/2595.html)

[Bible Hub](https://biblehub.com/str/hebrew/2595.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02595)

[Bible Bento](https://biblebento.com/dictionary/H2595.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2595/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2595.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2595)
