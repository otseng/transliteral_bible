# חָלָק

[ḥālāq](https://www.blueletterbible.org/lexicon/h2509)

Definition: flattering (2x), smooth (1x), smoother (2x), deceitful

Part of speech: adjective

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2509)

[Study Light](https://www.studylight.org/lexicons/hebrew/2509.html)

[Bible Hub](https://biblehub.com/str/hebrew/2509.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02509)

[Bible Bento](https://biblebento.com/dictionary/H2509.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2509/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2509.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2509)
