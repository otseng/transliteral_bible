# דֵּן

[dēn](https://www.blueletterbible.org/lexicon/h1836)

Definition: this (38x), these (3x), thus (2x), hereafter (1x), that (1x), miscellaneous (12x).

Part of speech: adverb, demonstrative pronoun

Occurs 58 times in 53 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1836)

[Study Light](https://www.studylight.org/lexicons/hebrew/1836.html)

[Bible Hub](https://biblehub.com/str/hebrew/1836.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01836)

[Bible Bento](https://biblebento.com/dictionary/H1836.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1836/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1836.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1836)
