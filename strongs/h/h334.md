# אִטֵּר

['iṭṭēr](https://www.blueletterbible.org/lexicon/h334)

Definition: lefthanded (with H3025) (2x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h334)

[Study Light](https://www.studylight.org/lexicons/hebrew/334.html)

[Bible Hub](https://biblehub.com/str/hebrew/334.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0334)

[Bible Bento](https://biblebento.com/dictionary/H334.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/334/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/334.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h334)
