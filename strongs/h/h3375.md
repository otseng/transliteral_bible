# יִרְאוֹן

[Yir'Ôn](https://www.blueletterbible.org/lexicon/h3375)

Definition: Iron (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3375)

[Study Light](https://www.studylight.org/lexicons/hebrew/3375.html)

[Bible Hub](https://biblehub.com/str/hebrew/3375.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03375)

[Bible Bento](https://biblebento.com/dictionary/H3375.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3375/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3375.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3375)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/iron.html)