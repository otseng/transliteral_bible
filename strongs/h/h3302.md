# יָפָה

[yāp̄â](https://www.blueletterbible.org/lexicon/h3302)

Definition: fair (4x), beautiful (2x), deck it (1x), fairer (1x).

Part of speech: verb

Occurs 7 times in 7 verses

Hebrew: [yāp̄ê](../h/h3303.md), [yᵊp̄î](../h/h3308.md)

Names: [Yāp̄Vô](../h/h3305.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3302)

[Study Light](https://www.studylight.org/lexicons/hebrew/3302.html)

[Bible Hub](https://biblehub.com/str/hebrew/3302.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03302)

[Bible Bento](https://biblebento.com/dictionary/H3302.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3302/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3302.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3302)
