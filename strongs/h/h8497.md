# תָּכָה

[tāḵâ](https://www.blueletterbible.org/lexicon/h8497)

Definition: sat down (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8497)

[Study Light](https://www.studylight.org/lexicons/hebrew/8497.html)

[Bible Hub](https://biblehub.com/str/hebrew/8497.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08497)

[Bible Bento](https://biblebento.com/dictionary/H8497.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8497/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8497.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8497)
