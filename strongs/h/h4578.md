# מֵעֶה

[me'ah](https://www.blueletterbible.org/lexicon/h4578)

Definition: bowels (27x), belly (3x), heart (1x), womb (1x), uterus

Part of speech: masculine noun

Occurs 32 times in 30 verses

Greek: [koilia](../g/g2836.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4578)

[Study Light](https://www.studylight.org/lexicons/hebrew/4578.html)

[Bible Hub](https://biblehub.com/str/hebrew/4578.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04578)

[Bible Bento](https://biblebento.com/dictionary/H4578.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4578/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4578.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4578)
