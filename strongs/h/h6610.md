# פִּתָּחוֹן

[pitāḥôn](https://www.blueletterbible.org/lexicon/h6610)

Definition: open (1x), opening (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6610)

[Study Light](https://www.studylight.org/lexicons/hebrew/6610.html)

[Bible Hub](https://biblehub.com/str/hebrew/6610.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06610)

[Bible Bento](https://biblebento.com/dictionary/H6610.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6610/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6610.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6610)
