# מַאֲכָל

[ma'akal](https://www.blueletterbible.org/lexicon/h3978)

Definition: meat (22x), food (5x), fruit (1x), manner (1x), victual (1x)

Part of speech: masculine noun

Occurs 30 times in 29 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3978)

[Study Light](https://www.studylight.org/lexicons/hebrew/3978.html)

[Bible Hub](https://biblehub.com/str/hebrew/3978.htm)

[Morfix](https://www.morfix.co.il/en/%D7%9E%D6%B7%D7%90%D6%B2%D7%9B%D6%B8%D7%9C)

[NET Bible](http://classic.net.bible.org/strong.php?id=03978)

[Bible Bento](https://biblebento.com/dictionary/H3978.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3978/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3978.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3978)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%9E%D7%90%D7%9B%D7%9C)
