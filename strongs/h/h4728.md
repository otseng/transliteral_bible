# מַקָּחָה

[maqqāḥâ](https://www.blueletterbible.org/lexicon/h4728)

Definition: ware (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4728)

[Study Light](https://www.studylight.org/lexicons/hebrew/4728.html)

[Bible Hub](https://biblehub.com/str/hebrew/4728.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04728)

[Bible Bento](https://biblebento.com/dictionary/H4728.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4728/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4728.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4728)
