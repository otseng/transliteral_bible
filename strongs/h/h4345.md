# מַכְבֵּר

[maḵbēr](https://www.blueletterbible.org/lexicon/h4345)

Definition: grate (6x).

Part of speech: masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4345)

[Study Light](https://www.studylight.org/lexicons/hebrew/4345.html)

[Bible Hub](https://biblehub.com/str/hebrew/4345.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04345)

[Bible Bento](https://biblebento.com/dictionary/H4345.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4345/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4345.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4345)
