# פָּדוֹן

[Pāḏôn](https://www.blueletterbible.org/lexicon/h6303)

Definition: Padon (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6303)

[Study Light](https://www.studylight.org/lexicons/hebrew/6303.html)

[Bible Hub](https://biblehub.com/str/hebrew/6303.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06303)

[Bible Bento](https://biblebento.com/dictionary/H6303.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6303/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6303.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6303)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/padon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/padon)