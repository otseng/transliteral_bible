# מִשְׂגָּב

[misgab](https://www.blueletterbible.org/lexicon/h4869)

Definition: defence (7x), refuge (5x), high tower (3x), high fort (1x), Misgab (1x), high place, refuge, secure height, retreat, stronghold, cliff, tower, hideout

Part of speech: masculine noun, proper locative noun

Occurs 17 times in 16 verses

Hebrew: [śāḡaḇ](../h/h7682.md)

Greek: [ischyros](../g/g2478.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4869)

[Study Light](https://www.studylight.org/lexicons/hebrew/4869.html)

[Bible Hub](https://biblehub.com/str/hebrew/4869.htm)

[Morfix](https://www.morfix.co.il/en/%D7%9E%D6%B4%D7%A9%D6%B0%D7%82%D7%92%D6%BC%D6%B8%D7%91)

[NET Bible](http://classic.net.bible.org/strong.php?id=04869)

[Bible Bento](https://biblebento.com/dictionary/H4869.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4869/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4869.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4869)
