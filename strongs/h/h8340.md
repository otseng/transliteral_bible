# שֵׁשְׁבַּצַּר

[Šēšbaṣṣar](https://www.blueletterbible.org/lexicon/h8340)

Definition: Sheshbazzar (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8340)

[Study Light](https://www.studylight.org/lexicons/hebrew/8340.html)

[Bible Hub](https://biblehub.com/str/hebrew/8340.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08340)

[Bible Bento](https://biblebento.com/dictionary/H8340.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8340/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8340.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8340)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/sheshbazzar.html)