# דֶּבֶר

[deḇer](https://www.blueletterbible.org/lexicon/h1698)

Definition: pestilence (47x), plagues (1x), murrain (1x).

Part of speech: masculine noun

Occurs 49 times in 48 verses

Hebrew: [dabar](../h/h1696.md)

Greek: [thanatos](../g/g2288.md), [loimos](../g/g3061.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1698)

[Study Light](https://www.studylight.org/lexicons/hebrew/1698.html)

[Bible Hub](https://biblehub.com/str/hebrew/1698.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01698)

[Bible Bento](https://biblebento.com/dictionary/H1698.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1698/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1698.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1698)
