# גְּלָה

[gᵊlâ](https://www.blueletterbible.org/lexicon/h1541)

Definition: reveal (7x), brought over (1x), carried away (1x).

Part of speech: verb

Occurs 9 times in 8 verses

Hebrew: [gālâ](../h/h1540.md), [ʿālâ](../h/h5927.md), [`aliylah](../h/h5949.md), [ʿālal](../h/h5953.md), [ʿălal](../h/h5954.md)

Pictoral: eye/foot staff, teach/knowledge staff/yoke, "experience the staff", "experience the yoke"

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1541)

[Study Light](https://www.studylight.org/lexicons/hebrew/1541.html)

[Bible Hub](https://biblehub.com/str/hebrew/1541.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01541)

[Bible Bento](https://biblebento.com/dictionary/H1541.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1541/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1541.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1541)
