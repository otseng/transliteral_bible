# חָרַץ

[ḥāraṣ](https://www.blueletterbible.org/lexicon/h2782)

Definition: determined (6x), move (2x), decide (1x), bestir (1x), maim (1x), decreed (1x).

Part of speech: verb

Occurs 12 times in 12 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2782)

[Study Light](https://www.studylight.org/lexicons/hebrew/2782.html)

[Bible Hub](https://biblehub.com/str/hebrew/2782.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02782)

[Bible Bento](https://biblebento.com/dictionary/H2782.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2782/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2782.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2782)
