# סַמְגַּר־נְבוּ

[Samgar-Nᵊḇû](https://www.blueletterbible.org/lexicon/h5562)

Definition: Samgarnebo (1x).

Part of speech: proper masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5562)

[Study Light](https://www.studylight.org/lexicons/hebrew/5562.html)

[Bible Hub](https://biblehub.com/str/hebrew/5562.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05562)

[Bible Bento](https://biblebento.com/dictionary/H5562.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5562/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5562.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5562)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/samgarnebo.html)