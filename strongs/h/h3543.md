# כָּהָה

[kāhâ](https://www.blueletterbible.org/lexicon/h3543)

Definition: dim (3x), fail (1x), faint (1x), darkened (1x), utterly (1x), restrained (1x).

Part of speech: verb

Occurs 8 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3543)

[Study Light](https://www.studylight.org/lexicons/hebrew/3543.html)

[Bible Hub](https://biblehub.com/str/hebrew/3543.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03543)

[Bible Bento](https://biblebento.com/dictionary/H3543.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3543/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3543.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3543)
