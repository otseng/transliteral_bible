# עַזּוּר

[ʿAzzûr](https://www.blueletterbible.org/lexicon/h5809)

Definition: Azur (2x), Azzur (1x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5809)

[Study Light](https://www.studylight.org/lexicons/hebrew/5809.html)

[Bible Hub](https://biblehub.com/str/hebrew/5809.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05809)

[Bible Bento](https://biblebento.com/dictionary/H5809.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5809/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5809.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5809)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/azur.html)