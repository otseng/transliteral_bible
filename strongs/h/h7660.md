# שָׁבַץ

[šāḇaṣ](https://www.blueletterbible.org/lexicon/h7660)

Definition: embroider (1x), set (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7660)

[Study Light](https://www.studylight.org/lexicons/hebrew/7660.html)

[Bible Hub](https://biblehub.com/str/hebrew/7660.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07660)

[Bible Bento](https://biblebento.com/dictionary/H7660.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7660/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7660.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7660)
