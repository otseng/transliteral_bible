# סָרַף

[sārap̄](https://www.blueletterbible.org/lexicon/h5635)

Definition: burn (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5635)

[Study Light](https://www.studylight.org/lexicons/hebrew/5635.html)

[Bible Hub](https://biblehub.com/str/hebrew/5635.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05635)

[Bible Bento](https://biblebento.com/dictionary/H5635.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5635/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5635.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5635)
