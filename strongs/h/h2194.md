# זָעַם

[za`am](https://www.blueletterbible.org/lexicon/h2194)

Definition: indignation (4x), defy (3x), abhor (2x), angry (2x), abominable (1x).

Part of speech: verb

Occurs 12 times in 11 verses

Hebrew: [zaʿam](../h/h2195.md)

Greek: [orgē](../g/g3709.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2194)

[Study Light](https://www.studylight.org/lexicons/hebrew/2194.html)

[Bible Hub](https://biblehub.com/str/hebrew/2194.htm)

[Morfix](https://www.morfix.co.il/en/%D7%96%D6%B8%D7%A2%D6%B7%D7%9D)

[NET Bible](http://classic.net.bible.org/strong.php?id=02194)

[Bible Bento](https://biblebento.com/dictionary/H2194.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2194/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2194.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2194)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%96%D7%A2%D7%9D)
