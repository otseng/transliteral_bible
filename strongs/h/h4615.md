# מַעֲמַקִּים

[maʿămaqqîm](https://www.blueletterbible.org/lexicon/h4615)

Definition: depths (3x), deep (2x).

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4615)

[Study Light](https://www.studylight.org/lexicons/hebrew/4615.html)

[Bible Hub](https://biblehub.com/str/hebrew/4615.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04615)

[Bible Bento](https://biblebento.com/dictionary/H4615.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4615/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4615.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4615)
