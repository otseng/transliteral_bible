# נוּם

[nûm](https://www.blueletterbible.org/lexicon/h5123)

Definition: slumber (5x), slept (1x).

Part of speech: verb

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5123)

[Study Light](https://www.studylight.org/lexicons/hebrew/5123.html)

[Bible Hub](https://biblehub.com/str/hebrew/5123.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05123)

[Bible Bento](https://biblebento.com/dictionary/H5123.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5123/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5123.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5123)
