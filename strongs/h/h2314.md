# חָדַר

[ḥāḏar](https://www.blueletterbible.org/lexicon/h2314)

Definition: entereth...privy chambers (1x), encompass, surround, enclose

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [ḥeḏer](../h/h2315.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2314)

[Study Light](https://www.studylight.org/lexicons/hebrew/2314.html)

[Bible Hub](https://biblehub.com/str/hebrew/2314.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02314)

[Bible Bento](https://biblebento.com/dictionary/H2314.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2314/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2314.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2314)
