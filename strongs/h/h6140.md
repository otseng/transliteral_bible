# עָקַשׁ

[ʿāqaš](https://www.blueletterbible.org/lexicon/h6140)

Definition: perverse (2x), pervert (2x), crooked (1x).

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6140)

[Study Light](https://www.studylight.org/lexicons/hebrew/6140.html)

[Bible Hub](https://biblehub.com/str/hebrew/6140.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06140)

[Bible Bento](https://biblebento.com/dictionary/H6140.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6140/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6140.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6140)
