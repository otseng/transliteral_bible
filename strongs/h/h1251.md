# בַּר

[bar](https://www.blueletterbible.org/lexicon/h1251)

Definition: field (8x).

Part of speech: masculine noun

Occurs 8 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1251)

[Study Light](https://www.studylight.org/lexicons/hebrew/1251.html)

[Bible Hub](https://biblehub.com/str/hebrew/1251.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01251)

[Bible Bento](https://biblebento.com/dictionary/H1251.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1251/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1251.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1251)
