# עָכָן

[ʿĀḵān](https://www.blueletterbible.org/lexicon/h5912)

Definition: Achan (6x).

Part of speech: proper masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5912)

[Study Light](https://www.studylight.org/lexicons/hebrew/5912.html)

[Bible Hub](https://biblehub.com/str/hebrew/5912.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05912)

[Bible Bento](https://biblebento.com/dictionary/H5912.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5912/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5912.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5912)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/achan.html)

[Video Bible](https://www.videobible.com/bible-dictionary/achan)