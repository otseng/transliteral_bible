# לְחֵנָה

[lᵊḥēnâ](https://www.blueletterbible.org/lexicon/h3904)

Definition: concubine (3x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3904)

[Study Light](https://www.studylight.org/lexicons/hebrew/3904.html)

[Bible Hub](https://biblehub.com/str/hebrew/3904.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03904)

[Bible Bento](https://biblebento.com/dictionary/H3904.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3904/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3904.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3904)
