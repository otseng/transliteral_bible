# דַּד

[daḏ](https://www.blueletterbible.org/lexicon/h1717)

Definition: breast (2x), teat (2x), nipple

Part of speech: masculine noun

Occurs 4 times in 4 verses

Hebrew: [dôḏ](../h/h1730.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1717)

[Study Light](https://www.studylight.org/lexicons/hebrew/1717.html)

[Bible Hub](https://biblehub.com/str/hebrew/1717.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01717)

[Bible Bento](https://biblebento.com/dictionary/H1717.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1717/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1717.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1717)
