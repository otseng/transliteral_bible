# בֻּנִּי

[Bunnî](https://www.blueletterbible.org/lexicon/h1138)

Definition: Bunni (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1138)

[Study Light](https://www.studylight.org/lexicons/hebrew/1138.html)

[Bible Hub](https://biblehub.com/str/hebrew/1138.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01138)

[Bible Bento](https://biblebento.com/dictionary/H1138.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1138/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1138.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1138)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bunni.html)

[Video Bible](https://www.videobible.com/bible-dictionary/bunni)