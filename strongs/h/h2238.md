# זֶרֶשׁ

[Zereš](https://www.blueletterbible.org/lexicon/h2238)

Definition: Zeresh (4x).

Part of speech: proper feminine noun

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2238)

[Study Light](https://www.studylight.org/lexicons/hebrew/2238.html)

[Bible Hub](https://biblehub.com/str/hebrew/2238.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02238)

[Bible Bento](https://biblebento.com/dictionary/H2238.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2238/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2238.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2238)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zeresh.html)

[Video Bible](https://www.videobible.com/bible-dictionary/zeresh)