# כְּרַז

[kᵊraz](https://www.blueletterbible.org/lexicon/h3745)

Definition: proclamation (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Greek: [kēryssō](../g/g2784.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3745)

[Study Light](https://www.studylight.org/lexicons/hebrew/3745.html)

[Bible Hub](https://biblehub.com/str/hebrew/3745.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03745)

[Bible Bento](https://biblebento.com/dictionary/H3745.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3745/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3745.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3745)
