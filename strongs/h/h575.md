# אָן

['ān](https://www.blueletterbible.org/lexicon/h575)

Definition: whither, how, where, whithersoever, hither.

Part of speech: adverb

Occurs 43 times in 33 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h575)

[Study Light](https://www.studylight.org/lexicons/hebrew/575.html)

[Bible Hub](https://biblehub.com/str/hebrew/575.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0575)

[Bible Bento](https://biblebento.com/dictionary/H575.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/575/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/575.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h575)
