# יְרִיָּה

[Yᵊrîyâ](https://www.blueletterbible.org/lexicon/h3404)

Definition: Jerijah (2x), Jeriah (1x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3404)

[Study Light](https://www.studylight.org/lexicons/hebrew/3404.html)

[Bible Hub](https://biblehub.com/str/hebrew/3404.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03404)

[Bible Bento](https://biblebento.com/dictionary/H3404.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3404/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3404.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3404)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jerijah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jerijah)