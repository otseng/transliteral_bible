# יִלּוֹד

[yillôḏ](https://www.blueletterbible.org/lexicon/h3209)

Definition: born (5x).

Part of speech: adjective

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3209)

[Study Light](https://www.studylight.org/lexicons/hebrew/3209.html)

[Bible Hub](https://biblehub.com/str/hebrew/3209.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03209)

[Bible Bento](https://biblebento.com/dictionary/H3209.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3209/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3209.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3209)
