# יִצְרִי

[Yiṣrî](https://www.blueletterbible.org/lexicon/h3339)

Definition: Izri (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3339)

[Study Light](https://www.studylight.org/lexicons/hebrew/3339.html)

[Bible Hub](https://biblehub.com/str/hebrew/3339.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03339)

[Bible Bento](https://biblebento.com/dictionary/H3339.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3339/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3339.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3339)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/izri.html)