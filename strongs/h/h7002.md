# קִטֵּר

[qiṭṭēr](https://www.blueletterbible.org/lexicon/h7002)

Definition: incense (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Hebrew: [qᵊṭôrâ](../h/h6988.md), [qāṭar](../h/h6999.md), [qāṭar](../h/h7000.md), [qᵊṭōreṯ](../h/h7004.md), [qîṭôr](../h/h7008.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7002)

[Study Light](https://www.studylight.org/lexicons/hebrew/7002.html)

[Bible Hub](https://biblehub.com/str/hebrew/7002.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07002)

[Bible Bento](https://biblebento.com/dictionary/H7002.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7002/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7002.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7002)
