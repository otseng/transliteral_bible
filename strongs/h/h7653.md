# שִׂבְעָה

[śiḇʿâ](https://www.blueletterbible.org/lexicon/h7653)

Definition: fulness (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7653)

[Study Light](https://www.studylight.org/lexicons/hebrew/7653.html)

[Bible Hub](https://biblehub.com/str/hebrew/7653.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07653)

[Bible Bento](https://biblebento.com/dictionary/H7653.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7653/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7653.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7653)
