# בִּלְתִּי

[biltî](https://www.blueletterbible.org/lexicon/h1115)

Definition: (122x), but, except, save, nothing, lest, no, from, inasmuch, and not.

Part of speech: feminine adjective, adverb, substantive

Occurs 112 times in 107 verses

Greek: [alla](../g/g235.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1115)

[Study Light](https://www.studylight.org/lexicons/hebrew/1115.html)

[Bible Hub](https://biblehub.com/str/hebrew/1115.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01115)

[Bible Bento](https://biblebento.com/dictionary/H1115.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1115/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1115.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1115)
