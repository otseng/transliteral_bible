# יָצַת

[yāṣaṯ](https://www.blueletterbible.org/lexicon/h3341)

Definition: kindle (12x), burned (7x), set (7x), burn up (2x), desolate (1x).

Part of speech: verb

Occurs 31 times in 28 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3341)

[Study Light](https://www.studylight.org/lexicons/hebrew/3341.html)

[Bible Hub](https://biblehub.com/str/hebrew/3341.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03341)

[Bible Bento](https://biblebento.com/dictionary/H3341.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3341/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3341.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3341)
