# אֹהַד

['ōhaḏ](https://www.blueletterbible.org/lexicon/h161)

Definition: Ohad (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h161)

[Study Light](https://www.studylight.org/lexicons/hebrew/161.html)

[Bible Hub](https://biblehub.com/str/hebrew/161.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0161)

[Bible Bento](https://biblebento.com/dictionary/H161.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/161/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/161.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h161)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/O/ohad.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ohad)