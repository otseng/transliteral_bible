# חֹסֶר

[ḥōser](https://www.blueletterbible.org/lexicon/h2640)

Definition: ...want (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2640)

[Study Light](https://www.studylight.org/lexicons/hebrew/2640.html)

[Bible Hub](https://biblehub.com/str/hebrew/2640.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02640)

[Bible Bento](https://biblebento.com/dictionary/H2640.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2640/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2640.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2640)
