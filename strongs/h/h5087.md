# נָדַר

[nāḏar](https://www.blueletterbible.org/lexicon/h5087)

Definition: vow (30x), made (1x).

Part of speech: verb

Occurs 31 times in 28 verses

Greek: [euchomai](../g/g2172.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5087)

[Study Light](https://www.studylight.org/lexicons/hebrew/5087.html)

[Bible Hub](https://biblehub.com/str/hebrew/5087.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05087)

[Bible Bento](https://biblebento.com/dictionary/H5087.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5087/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5087.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5087)
