# חַבּוּרָה

[ḥabûrâ](https://www.blueletterbible.org/lexicon/h2250)

Definition: stripe (3x), hurt (1x), wounds (1x), blueness (1x), bruise (1x).

Part of speech: feminine noun

Occurs 7 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2250)

[Study Light](https://www.studylight.org/lexicons/hebrew/2250.html)

[Bible Hub](https://biblehub.com/str/hebrew/2250.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02250)

[Bible Bento](https://biblebento.com/dictionary/H2250.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2250/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2250.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2250)
