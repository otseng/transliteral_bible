# כִּבְשָׂה

[kiḇśâ](https://www.blueletterbible.org/lexicon/h3535)

Definition: ewe lamb (6x), lamb (2x).

Part of speech: feminine noun

Occurs 8 times in 8 verses

Synonyms: [lamb](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Lamb)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3535)

[Study Light](https://www.studylight.org/lexicons/hebrew/3535.html)

[Bible Hub](https://biblehub.com/str/hebrew/3535.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03535)

[Bible Bento](https://biblebento.com/dictionary/H3535.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3535/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3535.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3535)
