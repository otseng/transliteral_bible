# רְאוּבֵן

[Rᵊ'ûḇēn](https://www.blueletterbible.org/lexicon/h7205)

Definition: Reuben (72x).

Part of speech: proper masculine noun

Occurs 72 times in 68 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7205)

[Study Light](https://www.studylight.org/lexicons/hebrew/7205.html)

[Bible Hub](https://biblehub.com/str/hebrew/7205.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07205)

[Bible Bento](https://biblebento.com/dictionary/H7205.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7205/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7205.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7205)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/R/reuben.html)

[Video Bible](https://www.videobible.com/bible-dictionary/reuben)