# תָּפַף

[tāp̄ap̄](https://www.blueletterbible.org/lexicon/h8608)

Definition: playing with timbrels (1x), tabering (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8608)

[Study Light](https://www.studylight.org/lexicons/hebrew/8608.html)

[Bible Hub](https://biblehub.com/str/hebrew/8608.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08608)

[Bible Bento](https://biblebento.com/dictionary/H8608.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8608/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8608.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8608)
