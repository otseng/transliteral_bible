# קַל

[qal](https://www.blueletterbible.org/lexicon/h7031)

Definition: swift (9x), swiftly (2x), swifter (1x), light (1x).

Part of speech: adjective

Occurs 13 times in 13 verses

Hebrew: [qowl](../h/h6963.md), [qālôn](../h/h7036.md), [qᵊlālâ](../h/h7045.md), [qeles](../h/h7047.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7031)

[Study Light](https://www.studylight.org/lexicons/hebrew/7031.html)

[Bible Hub](https://biblehub.com/str/hebrew/7031.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07031)

[Bible Bento](https://biblebento.com/dictionary/H7031.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7031/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7031.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7031)
