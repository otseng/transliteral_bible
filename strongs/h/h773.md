# אַרְעִית

['arʿîṯ](https://www.blueletterbible.org/lexicon/h773)

Definition: bottom (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h773)

[Study Light](https://www.studylight.org/lexicons/hebrew/773.html)

[Bible Hub](https://biblehub.com/str/hebrew/773.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0773)

[Bible Bento](https://biblebento.com/dictionary/H773.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/773/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/773.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h773)
