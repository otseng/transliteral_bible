# פָּלָא

[pala'](https://www.blueletterbible.org/lexicon/h6381)

Definition: (wondrous, marvellous...) work (18x), wonders (9x), marvellous (8x), wonderful (8x), ...things (6x), hard (5x), wondrous (3x), wondrously (2x), marvellously (2x), performing (2x), miscellaneous (8x).

Part of speech: verb

Occurs 73 times in 69 verses

Hebrew: [pele'](../h/h6382.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6381)

[Study Light](https://www.studylight.org/lexicons/hebrew/6381.html)

[Bible Hub](https://biblehub.com/str/hebrew/6381.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A4%D6%BC%D6%B8%D7%9C%D6%B8%D7%90)

[NET Bible](http://classic.net.bible.org/strong.php?id=06381)

[Bible Bento](https://biblebento.com/dictionary/H6381.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6381/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6381.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6381)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A4%D7%9C%D7%90)
