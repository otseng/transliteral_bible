# רָצַץ

[rāṣaṣ](https://www.blueletterbible.org/lexicon/h7533)

Definition: oppressed (6x), broken (4x), break (3x), bruised (2x), crush (2x), discouraged (1x), struggle together (1x), violence

Part of speech: verb

Occurs 19 times in 18 verses

Hebrew: [mᵊruṣâ](../h/h4835.md)

Greek: [syntribō](../g/g4937.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7533)

[Study Light](https://www.studylight.org/lexicons/hebrew/7533.html)

[Bible Hub](https://biblehub.com/str/hebrew/7533.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07533)

[Bible Bento](https://biblebento.com/dictionary/H7533.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7533/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7533.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7533)
