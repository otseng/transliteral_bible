# מְדָן

[mᵊḏān](https://www.blueletterbible.org/lexicon/h4091)

Definition: Medan (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4091)

[Study Light](https://www.studylight.org/lexicons/hebrew/4091.html)

[Bible Hub](https://biblehub.com/str/hebrew/4091.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04091)

[Bible Bento](https://biblebento.com/dictionary/H4091.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4091/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4091.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4091)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/medan.html)

[Video Bible](https://www.videobible.com/bible-dictionary/medan)