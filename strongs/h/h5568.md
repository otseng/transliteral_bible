# סָמַר

[sāmar](https://www.blueletterbible.org/lexicon/h5568)

Definition: tremble (1x), stood up (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5568)

[Study Light](https://www.studylight.org/lexicons/hebrew/5568.html)

[Bible Hub](https://biblehub.com/str/hebrew/5568.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05568)

[Bible Bento](https://biblebento.com/dictionary/H5568.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5568/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5568.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5568)
