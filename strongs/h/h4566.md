# מַעְבָּד

[maʿbāḏ](https://www.blueletterbible.org/lexicon/h4566)

Definition: works (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4566)

[Study Light](https://www.studylight.org/lexicons/hebrew/4566.html)

[Bible Hub](https://biblehub.com/str/hebrew/4566.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04566)

[Bible Bento](https://biblebento.com/dictionary/H4566.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4566/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4566.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4566)
