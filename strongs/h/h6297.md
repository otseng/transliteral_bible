# פֶּגֶר

[peḡer](https://www.blueletterbible.org/lexicon/h6297)

Definition: carcase (14x), dead body (6x), corpse (2x), monument, stela

Part of speech: masculine noun

Occurs 22 times in 21 verses

Hebrew: [pāḡar](../h/h6296.md)

Greek: [kōlon](../g/g2966.md), [nekros](../g/g3498.md), [ptōma](../g/g4430.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6297)

[Study Light](https://www.studylight.org/lexicons/hebrew/6297.html)

[Bible Hub](https://biblehub.com/str/hebrew/6297.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06297)

[Bible Bento](https://biblebento.com/dictionary/H6297.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6297/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6297.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6297)
