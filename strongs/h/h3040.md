# יְדִידָה

[Yᵊḏîḏâ](https://www.blueletterbible.org/lexicon/h3040)

Definition: Jedidah (1x).

Part of speech: proper feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3040)

[Study Light](https://www.studylight.org/lexicons/hebrew/3040.html)

[Bible Hub](https://biblehub.com/str/hebrew/3040.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03040)

[Bible Bento](https://biblebento.com/dictionary/H3040.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3040/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3040.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3040)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jedidah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jedidah)