# עַדְנָא

[ʿAḏnā'](https://www.blueletterbible.org/lexicon/h5733)

Definition: Adna (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5733)

[Study Light](https://www.studylight.org/lexicons/hebrew/5733.html)

[Bible Hub](https://biblehub.com/str/hebrew/5733.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05733)

[Bible Bento](https://biblebento.com/dictionary/H5733.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5733/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5733.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5733)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/adna.html)

[Video Bible](https://www.videobible.com/bible-dictionary/adna)