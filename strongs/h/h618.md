# אָסָם

['āsām](https://www.blueletterbible.org/lexicon/h618)

Definition: storehouse (1x), barn (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h618)

[Study Light](https://www.studylight.org/lexicons/hebrew/618.html)

[Bible Hub](https://biblehub.com/str/hebrew/618.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0618)

[Bible Bento](https://biblebento.com/dictionary/H618.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/618/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/618.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h618)
