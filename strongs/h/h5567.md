# סָמַן

[sāman](https://www.blueletterbible.org/lexicon/h5567)

Definition: appointed (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5567)

[Study Light](https://www.studylight.org/lexicons/hebrew/5567.html)

[Bible Hub](https://biblehub.com/str/hebrew/5567.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05567)

[Bible Bento](https://biblebento.com/dictionary/H5567.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5567/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5567.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5567)
