# צְמָרַיִם

[Ṣᵊmārayim](https://www.blueletterbible.org/lexicon/h6787)

Definition: Zemaraim (2x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6787)

[Study Light](https://www.studylight.org/lexicons/hebrew/6787.html)

[Bible Hub](https://biblehub.com/str/hebrew/6787.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06787)

[Bible Bento](https://biblebento.com/dictionary/H6787.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6787/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6787.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6787)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zemaraim.html)

[Video Bible](https://www.videobible.com/bible-dictionary/zemaraim)