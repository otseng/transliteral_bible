# עֲרָבָה

[`arabah](https://www.blueletterbible.org/lexicon/h6160)

Definition: plain (42x), desert (9x), wilderness (5x), Arabah (2x), champaign (1x), evenings (1x), heavens (1x)

Part of speech: feminine noun

Occurs 61 times in 57 verses

Greek: [erēmos](../g/g2048.md)

Edenics: Arabia

Synonyms: [desert](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Desert)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6160)

[Study Light](https://www.studylight.org/lexicons/hebrew/6160.html)

[Bible Hub](https://biblehub.com/str/hebrew/6160.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A2%D6%B2%D7%A8%D6%B8%D7%91%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=06160)

[Bible Bento](https://biblebento.com/dictionary/H6160.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6160/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6160.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6160)

[Wiktionary](https://en.wiktionary.org/wiki/ערבה)
