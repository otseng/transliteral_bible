# רְבָה

[rᵊḇâ](https://www.blueletterbible.org/lexicon/h7236)

Definition: grow (5x), great (1x).

Part of speech: verb

Occurs 6 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7236)

[Study Light](https://www.studylight.org/lexicons/hebrew/7236.html)

[Bible Hub](https://biblehub.com/str/hebrew/7236.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07236)

[Bible Bento](https://biblebento.com/dictionary/H7236.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7236/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7236.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7236)
