# אֲבִיהוּד

['Ăḇîhûḏ](https://www.blueletterbible.org/lexicon/h31)

Definition: Ahihud (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h31)

[Study Light](https://www.studylight.org/lexicons/hebrew/31.html)

[Bible Hub](https://biblehub.com/str/hebrew/31.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=031)

[Bible Bento](https://biblebento.com/dictionary/H31.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/31/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/31.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h31)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ahihud.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ahihud)