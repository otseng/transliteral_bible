# אֲנָא

['ănā'](https://www.blueletterbible.org/lexicon/h576)

Definition: I (14x), me (2x).

Part of speech: personal pronoun

Occurs 16 times in 16 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h576)

[Study Light](https://www.studylight.org/lexicons/hebrew/576.html)

[Bible Hub](https://biblehub.com/str/hebrew/576.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0576)

[Bible Bento](https://biblebento.com/dictionary/H576.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/576/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/576.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h576)
