# קְרָבָה

[qᵊrāḇâ](https://www.blueletterbible.org/lexicon/h7132)

Definition: draw near (1x), approach (1x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7132)

[Study Light](https://www.studylight.org/lexicons/hebrew/7132.html)

[Bible Hub](https://biblehub.com/str/hebrew/7132.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07132)

[Bible Bento](https://biblebento.com/dictionary/H7132.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7132/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7132.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7132)
