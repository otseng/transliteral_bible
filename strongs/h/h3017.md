# יָגוּר

[Yāḡûr](https://www.blueletterbible.org/lexicon/h3017)

Definition: Jagur (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3017)

[Study Light](https://www.studylight.org/lexicons/hebrew/3017.html)

[Bible Hub](https://biblehub.com/str/hebrew/3017.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03017)

[Bible Bento](https://biblebento.com/dictionary/H3017.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3017/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3017.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3017)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jagur.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jagur)