# תּוֹר

[tôr](https://www.blueletterbible.org/lexicon/h8447)

Definition: turn (2x), row (1x), border (1x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

Hebrew: [tûr](../h/h8446.md), [tôr](../h/h8448.md), [tôrâ](../h/h8452.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8447)

[Study Light](https://www.studylight.org/lexicons/hebrew/8447.html)

[Bible Hub](https://biblehub.com/str/hebrew/8447.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08447)

[Bible Bento](https://biblebento.com/dictionary/H8447.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8447/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8447.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8447)
