# אֱלִיאָב

['Ĕlî'āḇ](https://www.blueletterbible.org/lexicon/h446)

Definition: Eliab (21x).

Part of speech: proper masculine noun

Occurs 21 times in 20 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h446)

[Study Light](https://www.studylight.org/lexicons/hebrew/446.html)

[Bible Hub](https://biblehub.com/str/hebrew/446.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0446)

[Bible Bento](https://biblebento.com/dictionary/H446.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/446/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/446.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h446)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eliab.html)

[Video Bible](https://www.videobible.com/bible-dictionary/eliab)