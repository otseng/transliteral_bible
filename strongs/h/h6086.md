# עֵץ

['ets](https://www.blueletterbible.org/lexicon/h6086)

Definition: tree (162x), wood (107x), timber (23x), stick (14x), gallows (8x), staff (4x), stock (4x), carpenter (with H2796) (2x), branches (1x), helve (1x), planks (1x), stalks (1x)

Part of speech: masculine noun

Occurs 328 times in 288 verses

Hebrew: [`etsem](../h/h6106.md)

Greek: [dendron](../g/g1186.md)

Edenics: stick

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6086)

[Study Light](https://www.studylight.org/lexicons/hebrew/6086.html)

[Bible Hub](https://biblehub.com/str/hebrew/6086.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A2%D6%B5%D7%A5)

[NET Bible](http://classic.net.bible.org/strong.php?id=06086)

[Bible Bento](https://biblebento.com/dictionary/H6086.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6086/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6086.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6086)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A2%D7%A5)
