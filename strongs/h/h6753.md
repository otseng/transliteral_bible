# צְלֶלְפּוֹנִי

[Ṣᵊlelpônî](https://www.blueletterbible.org/lexicon/h6753)

Definition: Hazelelponi (1x).

Part of speech: proper feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6753)

[Study Light](https://www.studylight.org/lexicons/hebrew/6753.html)

[Bible Hub](https://biblehub.com/str/hebrew/6753.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06753)

[Bible Bento](https://biblebento.com/dictionary/H6753.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6753/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6753.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6753)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hazelelponi.html)