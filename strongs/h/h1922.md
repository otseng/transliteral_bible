# הֲדַר

[hăḏar](https://www.blueletterbible.org/lexicon/h1922)

Definition: honour (2x), glorified (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1922)

[Study Light](https://www.studylight.org/lexicons/hebrew/1922.html)

[Bible Hub](https://biblehub.com/str/hebrew/1922.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01922)

[Bible Bento](https://biblebento.com/dictionary/H1922.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1922/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1922.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1922)
