# דְּמָה

[dᵊmâ](https://www.blueletterbible.org/lexicon/h1821)

Definition: like (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1821)

[Study Light](https://www.studylight.org/lexicons/hebrew/1821.html)

[Bible Hub](https://biblehub.com/str/hebrew/1821.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01821)

[Bible Bento](https://biblebento.com/dictionary/H1821.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1821/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1821.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1821)
