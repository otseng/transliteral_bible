# שֹׁחַד

[shachad](https://www.blueletterbible.org/lexicon/h7810)

Definition: gift (10x), reward (7x), bribes (3x), present (2x), bribery (1x).

Part of speech: masculine noun

Occurs 23 times in 21 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7810)

[Study Light](https://www.studylight.org/lexicons/hebrew/7810.html)

[Bible Hub](https://biblehub.com/str/hebrew/7810.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07810)

[Bible Bento](https://biblebento.com/dictionary/H7810.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7810/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7810.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7810)
