# עֲזָּתִי

[ʿĂzzāṯî](https://www.blueletterbible.org/lexicon/h5841)

Definition: Gazites (1x), Gazathites (1x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5841)

[Study Light](https://www.studylight.org/lexicons/hebrew/5841.html)

[Bible Hub](https://biblehub.com/str/hebrew/5841.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05841)

[Bible Bento](https://biblebento.com/dictionary/H5841.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5841/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5841.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5841)
