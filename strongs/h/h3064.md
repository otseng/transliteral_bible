# יְהוּדִי

[Yᵊhûḏî](https://www.blueletterbible.org/lexicon/h3064)

Definition: Jew (74x), Jew (with H376) (1x), Judah (1x).

Part of speech: masculine noun

Occurs 81 times in 69 verses

Hebrew: [Yehuwdah](../h/h3063.md), [yᵊhûḏîṯ](../h/h3066.md)

Greek: [Ioudaios](../g/g2453.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3064)

[Study Light](https://www.studylight.org/lexicons/hebrew/3064.html)

[Bible Hub](https://biblehub.com/str/hebrew/3064.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03064)

[Bible Bento](https://biblebento.com/dictionary/H3064.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3064/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3064.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3064)
