# עֶגְלָה

[ʿeḡlâ](https://www.blueletterbible.org/lexicon/h5697)

Definition: heifer (12x), cow (1x), calf (1x).

Part of speech: feminine noun

Occurs 14 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5697)

[Study Light](https://www.studylight.org/lexicons/hebrew/5697.html)

[Bible Hub](https://biblehub.com/str/hebrew/5697.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05697)

[Bible Bento](https://biblebento.com/dictionary/H5697.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5697/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5697.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5697)
