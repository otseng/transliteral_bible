# אַחֲרִית

['aḥărîṯ](https://www.blueletterbible.org/lexicon/h320)

Definition: latter (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Hebrew: ['aḥar](../h/h310.md), ['aḥărîṯ](../h/h319.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h320)

[Study Light](https://www.studylight.org/lexicons/hebrew/320.html)

[Bible Hub](https://biblehub.com/str/hebrew/320.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0320)

[Bible Bento](https://biblebento.com/dictionary/H320.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/320/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/320.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h320)
