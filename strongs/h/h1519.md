# גִּיחַ

[gîaḥ](https://www.blueletterbible.org/lexicon/h1519)

Definition: strove (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1519)

[Study Light](https://www.studylight.org/lexicons/hebrew/1519.html)

[Bible Hub](https://biblehub.com/str/hebrew/1519.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01519)

[Bible Bento](https://biblebento.com/dictionary/H1519.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1519/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1519.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1519)
