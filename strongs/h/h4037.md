# מַגְזֵרָה

[maḡzērâ](https://www.blueletterbible.org/lexicon/h4037)

Definition: axe (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4037)

[Study Light](https://www.studylight.org/lexicons/hebrew/4037.html)

[Bible Hub](https://biblehub.com/str/hebrew/4037.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04037)

[Bible Bento](https://biblebento.com/dictionary/H4037.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4037/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4037.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4037)
