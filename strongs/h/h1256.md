# בְּרָאיָה

[Bᵊrā'Yâ](https://www.blueletterbible.org/lexicon/h1256)

Definition: Beraiah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1256)

[Study Light](https://www.studylight.org/lexicons/hebrew/1256.html)

[Bible Hub](https://biblehub.com/str/hebrew/1256.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01256)

[Bible Bento](https://biblebento.com/dictionary/H1256.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1256/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1256.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1256)

[Video Bible](https://www.videobible.com/bible-dictionary/beraiah)