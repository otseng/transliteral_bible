# יָרַד

[yarad](https://www.blueletterbible.org/lexicon/h3381)

Definition: (come, go, etc) down (340x), descend (18x), variant (2x), fell (2x), let (1x), abundantly (1x), down by (1x), indeed (1x), put off (1x), light off (1x), out (1x), sank (1x), subdued (1x), take (1x).

Part of speech: verb

Occurs 388 times in 344 verses

Hebrew: [môrāḏ](../h/h4174.md)

Names: [Yardēn](../h/h3383.md)

Greek: [erchomai](../g/g2064.md), [katabainō](../g/g2597.md)

Pictoral: man at door

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3381)

[Study Light](https://www.studylight.org/lexicons/hebrew/3381.html)

[Bible Hub](https://biblehub.com/str/hebrew/3381.htm)

[Morfix](https://www.morfix.co.il/en/%D7%99%D6%B8%D7%A8%D6%B7%D7%93)

[NET Bible](http://classic.net.bible.org/strong.php?id=03381)

[Bible Bento](https://biblebento.com/dictionary/H3381.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3381/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3381.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3381)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%99%D7%A8%D7%93#Hebrew)
