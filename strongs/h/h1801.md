# דָּלַג

[dālaḡ](https://www.blueletterbible.org/lexicon/h1801)

Definition: leap (5x).

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1801)

[Study Light](https://www.studylight.org/lexicons/hebrew/1801.html)

[Bible Hub](https://biblehub.com/str/hebrew/1801.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01801)

[Bible Bento](https://biblebento.com/dictionary/H1801.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1801/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1801.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1801)
