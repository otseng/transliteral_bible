# תַּהְפֻּכוֹת

[tahpuḵôṯ](https://www.blueletterbible.org/lexicon/h8419)

Definition: froward (4x), frowardness (3x), froward things (2x), perverse things (1x).

Part of speech: feminine noun

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8419)

[Study Light](https://www.studylight.org/lexicons/hebrew/8419.html)

[Bible Hub](https://biblehub.com/str/hebrew/8419.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08419)

[Bible Bento](https://biblebento.com/dictionary/H8419.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8419/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8419.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8419)
