# בֵּית־עֲֲנָת

[Bêṯ-ʿĂnāṯ](https://www.blueletterbible.org/lexicon/h1043)

Definition: Bethanath (3x).

Part of speech: proper locative noun

Occurs 6 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1043)

[Study Light](https://www.studylight.org/lexicons/hebrew/1043.html)

[Bible Hub](https://biblehub.com/str/hebrew/1043.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01043)

[Bible Bento](https://biblebento.com/dictionary/H1043.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1043/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1043.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1043)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bethanath.html)