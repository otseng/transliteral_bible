# דָּֽבְרַת

[Dāḇraṯ](https://www.blueletterbible.org/lexicon/h1705)

Definition: Daberath (2x), Dabareh (1x).

Part of speech: proper locative noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1705)

[Study Light](https://www.studylight.org/lexicons/hebrew/1705.html)

[Bible Hub](https://biblehub.com/str/hebrew/1705.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01705)

[Bible Bento](https://biblebento.com/dictionary/H1705.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1705/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1705.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1705)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/daberath.html)

[Video Bible](https://www.videobible.com/bible-dictionary/daberath)