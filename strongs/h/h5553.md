# סֶלַע

[cela`](https://www.blueletterbible.org/lexicon/h5553)

Definition: rock (57x), strong hold (1x), stones (1x), stony (1x).

Part of speech: masculine noun

Occurs 60 times in 54 verses

Greek: [petra](../g/g4073.md)

Edenics: silicon

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5553)

[Study Light](https://www.studylight.org/lexicons/hebrew/5553.html)

[Bible Hub](https://biblehub.com/str/hebrew/5553.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A1%D6%B6%D7%9C%D6%B7%D7%A2)

[NET Bible](http://classic.net.bible.org/strong.php?id=05553)

[Bible Bento](https://biblebento.com/dictionary/H5553.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5553/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5553.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5553)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A1%D7%9C%D7%A2)
