# גַּחַר

[Gaḥar](https://www.blueletterbible.org/lexicon/h1515)

Definition: Gahar (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1515)

[Study Light](https://www.studylight.org/lexicons/hebrew/1515.html)

[Bible Hub](https://biblehub.com/str/hebrew/1515.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01515)

[Bible Bento](https://biblebento.com/dictionary/H1515.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1515/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1515.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1515)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/gahar.html)