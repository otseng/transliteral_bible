# נֶשֶׁר

[nesheׁr](https://www.blueletterbible.org/lexicon/h5404)

Definition: eagle (26x), vulture

Part of speech: masculine noun

Occurs 26 times in 26 verses

Greek: [aetos](../g/g105.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5404)

[Study Light](https://www.studylight.org/lexicons/hebrew/5404.html)

[Bible Hub](https://biblehub.com/str/hebrew/5404.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A0%D6%B6%D7%A9%D6%B6%D7%81%D7%A8)

[NET Bible](http://classic.net.bible.org/strong.php?id=05404)

[Bible Bento](https://biblebento.com/dictionary/H5404.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5404/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5404.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5404)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A0%D7%A9%D7%A8)
