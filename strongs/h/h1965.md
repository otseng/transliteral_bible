# הֵיכַל

[hêḵal](https://www.blueletterbible.org/lexicon/h1965)

Definition: temple (8x), palace (5x).

Part of speech: masculine noun

Occurs 13 times in 10 verses

Hebrew: [heykal](../h/h1964.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1965)

[Study Light](https://www.studylight.org/lexicons/hebrew/1965.html)

[Bible Hub](https://biblehub.com/str/hebrew/1965.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01965)

[Bible Bento](https://biblebento.com/dictionary/H1965.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1965/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1965.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1965)
