# אַרְכֻבָּה

['arḵubâ](https://www.blueletterbible.org/lexicon/h755)

Definition: knees (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h755)

[Study Light](https://www.studylight.org/lexicons/hebrew/755.html)

[Bible Hub](https://biblehub.com/str/hebrew/755.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0755)

[Bible Bento](https://biblebento.com/dictionary/H755.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/755/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/755.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h755)
