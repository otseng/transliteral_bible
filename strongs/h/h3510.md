# כָּאַב

[kā'aḇ](https://www.blueletterbible.org/lexicon/h3510)

Definition: sorrowful (2x), sore (1x), have pain (1x), made sad (1x), mar (1x), make sore (1x), grieving (1x).

Part of speech: verb

Occurs 8 times in 8 verses

Hebrew: [maḵ'ōḇ](../h/h4341.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3510)

[Study Light](https://www.studylight.org/lexicons/hebrew/3510.html)

[Bible Hub](https://biblehub.com/str/hebrew/3510.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03510)

[Bible Bento](https://biblebento.com/dictionary/H3510.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3510/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3510.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3510)
