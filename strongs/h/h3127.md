# יוֹנֶקֶת

[yôneqeṯ](https://www.blueletterbible.org/lexicon/h3127)

Definition: branch (4x), tender branch (1x), young twigs (1x).

Part of speech: feminine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3127)

[Study Light](https://www.studylight.org/lexicons/hebrew/3127.html)

[Bible Hub](https://biblehub.com/str/hebrew/3127.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03127)

[Bible Bento](https://biblebento.com/dictionary/H3127.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3127/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3127.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3127)
