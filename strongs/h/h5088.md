# נֶדֶר

[neḏer](https://www.blueletterbible.org/lexicon/h5088)

Definition: vow (58x), vowed (2x), promise

Part of speech: masculine noun

Occurs 60 times in 57 verses

Greek: [euchē](../g/g2171.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5088)

[Study Light](https://www.studylight.org/lexicons/hebrew/5088.html)

[Bible Hub](https://biblehub.com/str/hebrew/5088.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05088)

[Bible Bento](https://biblebento.com/dictionary/H5088.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5088/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5088.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5088)
