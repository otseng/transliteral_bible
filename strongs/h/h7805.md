# שָׁזַף

[šāzap̄](https://www.blueletterbible.org/lexicon/h7805)

Definition: see (2x), look (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7805)

[Study Light](https://www.studylight.org/lexicons/hebrew/7805.html)

[Bible Hub](https://biblehub.com/str/hebrew/7805.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07805)

[Bible Bento](https://biblebento.com/dictionary/H7805.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7805/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7805.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7805)
