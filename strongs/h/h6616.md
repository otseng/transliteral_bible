# פָּתִיל

[pāṯîl](https://www.blueletterbible.org/lexicon/h6616)

Definition: lace (4x), bracelet (2x), wire (1x), ribband (1x), bound (1x), thread (1x), line (1x).

Part of speech: masculine noun

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6616)

[Study Light](https://www.studylight.org/lexicons/hebrew/6616.html)

[Bible Hub](https://biblehub.com/str/hebrew/6616.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06616)

[Bible Bento](https://biblebento.com/dictionary/H6616.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6616/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6616.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6616)
