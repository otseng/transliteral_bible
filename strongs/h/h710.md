# אַרְגְוָן

['arḡᵊvān](https://www.blueletterbible.org/lexicon/h710)

Definition: purple (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h710)

[Study Light](https://www.studylight.org/lexicons/hebrew/710.html)

[Bible Hub](https://biblehub.com/str/hebrew/710.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0710)

[Bible Bento](https://biblebento.com/dictionary/H710.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/710/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/710.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h710)
