# בַּקְבּוּק

[baqbûq](https://www.blueletterbible.org/lexicon/h1228)

Definition: bottle (2x), cruse (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1228)

[Study Light](https://www.studylight.org/lexicons/hebrew/1228.html)

[Bible Hub](https://biblehub.com/str/hebrew/1228.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01228)

[Bible Bento](https://biblebento.com/dictionary/H1228.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1228/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1228.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1228)
