# פָּדַע

[pāḏaʿ](https://www.blueletterbible.org/lexicon/h6308)

Definition: deliver (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6308)

[Study Light](https://www.studylight.org/lexicons/hebrew/6308.html)

[Bible Hub](https://biblehub.com/str/hebrew/6308.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06308)

[Bible Bento](https://biblebento.com/dictionary/H6308.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6308/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6308.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6308)
