# מְזַמֶּרֶה

[mᵊzammerê](https://www.blueletterbible.org/lexicon/h4212)

Definition: snuffer (5x).

Part of speech: feminine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4212)

[Study Light](https://www.studylight.org/lexicons/hebrew/4212.html)

[Bible Hub](https://biblehub.com/str/hebrew/4212.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04212)

[Bible Bento](https://biblebento.com/dictionary/H4212.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4212/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4212.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4212)
