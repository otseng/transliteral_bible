# אָרַח

['āraḥ](https://www.blueletterbible.org/lexicon/h732)

Definition: wayfaring man (4x), goeth (1x).

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h732)

[Study Light](https://www.studylight.org/lexicons/hebrew/732.html)

[Bible Hub](https://biblehub.com/str/hebrew/732.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0732)

[Bible Bento](https://biblebento.com/dictionary/H732.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/732/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/732.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h732)
