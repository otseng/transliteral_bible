# אֶשְׁתַּדּוּר

['eštadûr](https://www.blueletterbible.org/lexicon/h849)

Definition: sedition (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h849)

[Study Light](https://www.studylight.org/lexicons/hebrew/849.html)

[Bible Hub](https://biblehub.com/str/hebrew/849.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0849)

[Bible Bento](https://biblebento.com/dictionary/H849.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/849/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/849.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h849)
