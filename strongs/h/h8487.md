# תֵּימָן

[Têmān](https://www.blueletterbible.org/lexicon/h8487)

Definition: Teman (11x).

Part of speech: proper locative noun, proper masculine noun

Occurs 11 times in 11 verses

Hebrew: [têmān](../h/h8486.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8487)

[Study Light](https://www.studylight.org/lexicons/hebrew/8487.html)

[Bible Hub](https://biblehub.com/str/hebrew/8487.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08487)

[Bible Bento](https://biblebento.com/dictionary/H8487.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8487/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8487.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8487)

[Video Bible](https://www.videobible.com/bible-dictionary/teman)