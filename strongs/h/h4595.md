# מַעֲטָפָה

[maʿăṭāp̄â](https://www.blueletterbible.org/lexicon/h4595)

Definition: mantle (1x), cloak, overtunic

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4595)

[Study Light](https://www.studylight.org/lexicons/hebrew/4595.html)

[Bible Hub](https://biblehub.com/str/hebrew/4595.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04595)

[Bible Bento](https://biblebento.com/dictionary/H4595.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4595/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4595.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4595)
