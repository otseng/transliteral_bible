# רְאוּמָה

[Rᵊ'ûmâ](https://www.blueletterbible.org/lexicon/h7208)

Definition: Reumah (1x).

Part of speech: proper feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7208)

[Study Light](https://www.studylight.org/lexicons/hebrew/7208.html)

[Bible Hub](https://biblehub.com/str/hebrew/7208.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07208)

[Bible Bento](https://biblebento.com/dictionary/H7208.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7208/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7208.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7208)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/R/reumah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/reumah)