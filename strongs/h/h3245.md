# יָסַד

[yacad](https://www.blueletterbible.org/lexicon/h3245)

Definition: foundation (15x), lay (8x), founded (8x), ordain (2x), counsel (2x), established (2x), foundation (with H3117) (1x), appointed (1x), instructed (1x), set (1x), sure (1x).

Part of speech: verb

Occurs 47 times in 41 verses

Hebrew: [mûsāḏ](../h/h4143.md), [môsāḏ](../h/h4144.md), [mûsāḏâ](../h/h4145.md), [mowcadah](../h/h4146.md)

Greek: [archō](../g/g757.md), [ktizō](../g/g2936.md), [synagō](../g/g4863.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3245)

[Study Light](https://www.studylight.org/lexicons/hebrew/3245.html)

[Bible Hub](https://biblehub.com/str/hebrew/3245.htm)

[Morfix](https://www.morfix.co.il/en/%D7%99%D6%B8%D7%A1%D6%B7%D7%93)

[NET Bible](http://classic.net.bible.org/strong.php?id=03245)

[Bible Bento](https://biblebento.com/dictionary/H3245.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3245/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3245.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3245)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%99%D7%99%D7%A1%D7%93)
