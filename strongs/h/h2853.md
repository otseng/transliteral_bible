# חָתַל

[ḥāṯal](https://www.blueletterbible.org/lexicon/h2853)

Definition: swaddle (1x), at all (1x).

Part of speech: verb

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2853)

[Study Light](https://www.studylight.org/lexicons/hebrew/2853.html)

[Bible Hub](https://biblehub.com/str/hebrew/2853.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02853)

[Bible Bento](https://biblebento.com/dictionary/H2853.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2853/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2853.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2853)
