# שֵׁן

[Šēn](https://www.blueletterbible.org/lexicon/h8129)

Definition: Shen (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8129)

[Study Light](https://www.studylight.org/lexicons/hebrew/8129.html)

[Bible Hub](https://biblehub.com/str/hebrew/8129.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08129)

[Bible Bento](https://biblebento.com/dictionary/H8129.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8129/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8129.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8129)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shen.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shen)