# שְׁלַם

[šᵊlam](https://www.blueletterbible.org/lexicon/h8000)

Definition: finish (2x), deliver (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8000)

[Study Light](https://www.studylight.org/lexicons/hebrew/8000.html)

[Bible Hub](https://biblehub.com/str/hebrew/8000.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08000)

[Bible Bento](https://biblebento.com/dictionary/H8000.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8000/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8000.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8000)
