# גָּד

[Gāḏ](https://www.blueletterbible.org/lexicon/h1409)

Definition: troop (2x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1409)

[Study Light](https://www.studylight.org/lexicons/hebrew/1409.html)

[Bible Hub](https://biblehub.com/str/hebrew/1409.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01409)

[Bible Bento](https://biblebento.com/dictionary/H1409.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1409/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1409.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1409)
