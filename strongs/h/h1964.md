# הֵיכָל

[heykal](https://www.blueletterbible.org/lexicon/h1964)

Definition: temple (70x), palace (10x), hall, sanctuary

Part of speech: masculine noun

Occurs 80 times in 76 verses

Hebrew: [hêḵal](../h/h1965.md)

Greek: [naos](../g/g3485.md)

Edenics: hall, hollow

Pictoral: tame yoke

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1964)

[Study Light](https://www.studylight.org/lexicons/hebrew/1964.html)

[Bible Hub](https://biblehub.com/str/hebrew/1964.htm)

[Morfix](https://www.morfix.co.il/en/%D7%94%D6%B5%D7%99%D7%9B%D6%B8%D7%9C)

[NET Bible](http://classic.net.bible.org/strong.php?id=01964)

[Bible Bento](https://biblebento.com/dictionary/H1964.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1964/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1964.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1964)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%94%D7%99%D7%9B%D7%9C)
