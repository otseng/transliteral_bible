# נֵד

[nēḏ](https://www.blueletterbible.org/lexicon/h5067)

Definition: heap (6x).

Part of speech: masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5067)

[Study Light](https://www.studylight.org/lexicons/hebrew/5067.html)

[Bible Hub](https://biblehub.com/str/hebrew/5067.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05067)

[Bible Bento](https://biblebento.com/dictionary/H5067.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5067/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5067.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5067)
