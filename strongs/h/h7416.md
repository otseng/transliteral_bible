# רִמּוֹן

[rimmôn](https://www.blueletterbible.org/lexicon/h7416)

Definition: pomegranate (31x), pomegranate tree (1x).

Part of speech: masculine noun

Occurs 32 times in 25 verses

Names: [Rimmôn](../h/h7417.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7416)

[Study Light](https://www.studylight.org/lexicons/hebrew/7416.html)

[Bible Hub](https://biblehub.com/str/hebrew/7416.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07416)

[Bible Bento](https://biblebento.com/dictionary/H7416.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7416/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7416.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7416)
