# זְמִירָה

[Zᵊmîrâ](https://www.blueletterbible.org/lexicon/h2160)

Definition: Zemira (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2160)

[Study Light](https://www.studylight.org/lexicons/hebrew/2160.html)

[Bible Hub](https://biblehub.com/str/hebrew/2160.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02160)

[Bible Bento](https://biblebento.com/dictionary/H2160.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2160/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2160.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2160)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zemira.html)