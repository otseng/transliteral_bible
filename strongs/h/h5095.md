# נָהַל

[nāhal](https://www.blueletterbible.org/lexicon/h5095)

Definition: guide (5x), lead (3x), fed (1x), carried (1x).

Part of speech: verb

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5095)

[Study Light](https://www.studylight.org/lexicons/hebrew/5095.html)

[Bible Hub](https://biblehub.com/str/hebrew/5095.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05095)

[Bible Bento](https://biblebento.com/dictionary/H5095.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5095/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5095.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5095)
