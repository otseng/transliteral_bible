# דִּישׁוֹן

[dîšôn](https://www.blueletterbible.org/lexicon/h1787)

Definition: Dishon (7x).

Part of speech: proper masculine noun

Occurs 7 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1787)

[Study Light](https://www.studylight.org/lexicons/hebrew/1787.html)

[Bible Hub](https://biblehub.com/str/hebrew/1787.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01787)

[Bible Bento](https://biblebento.com/dictionary/H1787.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1787/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1787.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1787)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/dishon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/dishon)