# אָחוֹת

['āḥôṯ](https://www.blueletterbible.org/lexicon/h269)

Definition: sister(s) (106x), another (6x), together with (with H802) (1x), other (1x).

Part of speech: feminine noun

Occurs 119 times in 104 verses

Hebrew: ['ach](../h/h251.md)

Greek: [adelphē](../g/g79.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0269)

[Study Light](https://www.studylight.org/lexicons/hebrew/269.html)

[Bible Hub](https://biblehub.com/str/hebrew/269.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0269)

[Bible Bento](https://biblebento.com/dictionary/H269.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/269/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/269.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h269)
