# דְּבִיר

[dᵊḇîr](https://www.blueletterbible.org/lexicon/h1687)

Definition: oracle (16x).

Part of speech: masculine noun

Occurs 16 times in 16 verses

Hebrew: [dabar](../h/h1696.md), [dabar](../h/h1697.md), [dabrâ](../h/h1703.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1687)

[Study Light](https://www.studylight.org/lexicons/hebrew/1687.html)

[Bible Hub](https://biblehub.com/str/hebrew/1687.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01687)

[Bible Bento](https://biblebento.com/dictionary/H1687.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1687/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1687.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1687)
