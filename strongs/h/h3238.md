# יָנָה

[yānâ](https://www.blueletterbible.org/lexicon/h3238)

Definition: oppress (11x), vex (4x), destroy (1x), oppressor (1x), proud (1x), do wrong (1x), oppression (1x), thrust out.

Part of speech: verb

Occurs 19 times in 19 verses

Greek: [kataponeō](../g/g2669.md), [katadynasteuō](../g/g2616.md)

Synonyms: [destroy](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Destroy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3238)

[Study Light](https://www.studylight.org/lexicons/hebrew/3238.html)

[Bible Hub](https://biblehub.com/str/hebrew/3238.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03238)

[Bible Bento](https://biblebento.com/dictionary/H3238.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3238/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3238.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3238)
