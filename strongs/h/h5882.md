# עֵין עֶגְלַיִם

[ʿÊn ʿEḡlayim](https://www.blueletterbible.org/lexicon/h5882)

Definition: Eneglaim (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5882)

[Study Light](https://www.studylight.org/lexicons/hebrew/5882.html)

[Bible Hub](https://biblehub.com/str/hebrew/5882.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05882)

[Bible Bento](https://biblebento.com/dictionary/H5882.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5882/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5882.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5882)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eneglaim.html)

[Video Bible](https://www.videobible.com/bible-dictionary/eneglaim)