# גֵּבִים

[gēḇîm](https://www.blueletterbible.org/lexicon/h1374)

Definition: Gebim (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1374)

[Study Light](https://www.studylight.org/lexicons/hebrew/1374.html)

[Bible Hub](https://biblehub.com/str/hebrew/1374.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01374)

[Bible Bento](https://biblebento.com/dictionary/H1374.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1374/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1374.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1374)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/gebim.html)

[Video Bible](https://www.videobible.com/bible-dictionary/gebim)