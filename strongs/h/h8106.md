# שֶׁמֶר

[Šemer](https://www.blueletterbible.org/lexicon/h8106)

Definition: Shemer (2x), Shamer (2x), Shamed (1x).

Part of speech: proper masculine noun

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8106)

[Study Light](https://www.studylight.org/lexicons/hebrew/8106.html)

[Bible Hub](https://biblehub.com/str/hebrew/8106.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08106)

[Bible Bento](https://biblebento.com/dictionary/H8106.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8106/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8106.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8106)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shemer.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shemer)