# אֲחִישַׁחַר

['Ăḥîšaḥar](https://www.blueletterbible.org/lexicon/h300)

Definition: Ahishahar (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h300)

[Study Light](https://www.studylight.org/lexicons/hebrew/300.html)

[Bible Hub](https://biblehub.com/str/hebrew/300.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0300)

[Bible Bento](https://biblebento.com/dictionary/H300.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/300/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/300.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h300)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ahishahar.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ahishahar)