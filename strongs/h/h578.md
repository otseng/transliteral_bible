# אָנָה

['ānâ](https://www.blueletterbible.org/lexicon/h578)

Definition: lament (1x), mourn (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0578)

[Study Light](https://www.studylight.org/lexicons/hebrew/578.html)

[Bible Hub](https://biblehub.com/str/hebrew/578.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0578)

[Bible Bento](https://biblebento.com/dictionary/H578.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/578/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/578.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h578)
