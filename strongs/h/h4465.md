# מִמְכָּר

[mimkār](https://www.blueletterbible.org/lexicon/h4465)

Definition: that ... sold (4x), sale (2x), that which cometh of the sale (1x), ought (1x), ware (1x), sold (1x).

Part of speech: masculine noun

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4465)

[Study Light](https://www.studylight.org/lexicons/hebrew/4465.html)

[Bible Hub](https://biblehub.com/str/hebrew/4465.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04465)

[Bible Bento](https://biblebento.com/dictionary/H4465.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4465/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4465.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4465)
