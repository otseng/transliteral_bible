# שַׁלּוּם

[Šallûm](https://www.blueletterbible.org/lexicon/h7967)

Definition: Shallum (27x).

Part of speech: proper masculine noun

Occurs 27 times in 26 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7967)

[Study Light](https://www.studylight.org/lexicons/hebrew/7967.html)

[Bible Hub](https://biblehub.com/str/hebrew/7967.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07967)

[Bible Bento](https://biblebento.com/dictionary/H7967.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7967/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7967.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7967)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shallum.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shallum)