# סִלָּא

[sillā'](https://www.blueletterbible.org/lexicon/h5538)

Definition: Silla (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5538)

[Study Light](https://www.studylight.org/lexicons/hebrew/5538.html)

[Bible Hub](https://biblehub.com/str/hebrew/5538.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05538)

[Bible Bento](https://biblebento.com/dictionary/H5538.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5538/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5538.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5538)
