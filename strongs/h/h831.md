# אַשְׁקְלוֹן

['Ašqᵊlôn](https://www.blueletterbible.org/lexicon/h831)

Definition: Ashkelon (9x), Askelon (3x).

Part of speech: proper locative noun

Occurs 12 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h831)

[Study Light](https://www.studylight.org/lexicons/hebrew/831.html)

[Bible Hub](https://biblehub.com/str/hebrew/831.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0831)

[Bible Bento](https://biblebento.com/dictionary/H831.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/831/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/831.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h831)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ashkelon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ashkelon)