# צִי

[ṣî](https://www.blueletterbible.org/lexicon/h6716)

Definition: ship (4x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6716)

[Study Light](https://www.studylight.org/lexicons/hebrew/6716.html)

[Bible Hub](https://biblehub.com/str/hebrew/6716.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06716)

[Bible Bento](https://biblebento.com/dictionary/H6716.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6716/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6716.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6716)
