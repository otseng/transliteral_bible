# רָמוּת

[rāmûṯ](https://www.blueletterbible.org/lexicon/h7419)

Definition: height (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7419)

[Study Light](https://www.studylight.org/lexicons/hebrew/7419.html)

[Bible Hub](https://biblehub.com/str/hebrew/7419.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07419)

[Bible Bento](https://biblebento.com/dictionary/H7419.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7419/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7419.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7419)
