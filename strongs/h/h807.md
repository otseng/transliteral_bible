# אֲשִׁימָא

['Ăšîmā'](https://www.blueletterbible.org/lexicon/h807)

Definition: Ashima (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h807)

[Study Light](https://www.studylight.org/lexicons/hebrew/807.html)

[Bible Hub](https://biblehub.com/str/hebrew/807.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0807)

[Bible Bento](https://biblebento.com/dictionary/H807.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/807/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/807.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h807)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ashima.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ashima)