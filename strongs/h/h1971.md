# הַכָּרָה

[hakārâ](https://www.blueletterbible.org/lexicon/h1971)

Definition: show (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1971)

[Study Light](https://www.studylight.org/lexicons/hebrew/1971.html)

[Bible Hub](https://biblehub.com/str/hebrew/1971.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01971)

[Bible Bento](https://biblebento.com/dictionary/H1971.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1971/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1971.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1971)
