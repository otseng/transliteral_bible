# חָפַז

[ḥāp̄az](https://www.blueletterbible.org/lexicon/h2648)

Definition: haste (3x), to haste (3x), make haste (2x), tremble (1x).

Part of speech: verb

Occurs 10 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2648)

[Study Light](https://www.studylight.org/lexicons/hebrew/2648.html)

[Bible Hub](https://biblehub.com/str/hebrew/2648.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02648)

[Bible Bento](https://biblebento.com/dictionary/H2648.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2648/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2648.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2648)
