# חַגִּית

[Ḥagîṯ](https://www.blueletterbible.org/lexicon/h2294)

Definition: Haggith (5x).

Part of speech: proper feminine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2294)

[Study Light](https://www.studylight.org/lexicons/hebrew/2294.html)

[Bible Hub](https://biblehub.com/str/hebrew/2294.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02294)

[Bible Bento](https://biblebento.com/dictionary/H2294.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2294/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2294.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2294)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/haggith.html)

[Video Bible](https://www.videobible.com/bible-dictionary/haggith)