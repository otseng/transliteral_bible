# יָשֻׁבִי

[yāšuḇî](https://www.blueletterbible.org/lexicon/h3432)

Definition: Jashubites (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3432)

[Study Light](https://www.studylight.org/lexicons/hebrew/3432.html)

[Bible Hub](https://biblehub.com/str/hebrew/3432.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03432)

[Bible Bento](https://biblebento.com/dictionary/H3432.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3432/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3432.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3432)
