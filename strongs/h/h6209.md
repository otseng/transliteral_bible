# עָרַר

[ʿārar](https://www.blueletterbible.org/lexicon/h6209)

Definition: make bare (1x), raise up (1x), utterly (1x), broken (1x).

Part of speech: verb

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6209)

[Study Light](https://www.studylight.org/lexicons/hebrew/6209.html)

[Bible Hub](https://biblehub.com/str/hebrew/6209.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06209)

[Bible Bento](https://biblebento.com/dictionary/H6209.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6209/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6209.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6209)
