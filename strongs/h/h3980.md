# מַאֲכֹלֶת

[ma'ăḵōleṯ](https://www.blueletterbible.org/lexicon/h3980)

Definition: fuel (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3980)

[Study Light](https://www.studylight.org/lexicons/hebrew/3980.html)

[Bible Hub](https://biblehub.com/str/hebrew/3980.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03980)

[Bible Bento](https://biblebento.com/dictionary/H3980.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3980/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3980.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3980)
