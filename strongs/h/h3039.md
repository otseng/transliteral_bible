# יָדִיד

[yāḏîḏ](https://www.blueletterbible.org/lexicon/h3039)

Definition: beloved (5x), wellbeloved (2x), loves (1x), amiable (1x).

Part of speech: adjective, masculine noun

Occurs 9 times in 8 verses

Hebrew: [dôḏ](../h/h1730.md)

Greek: [agapētos](../g/g27.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3039)

[Study Light](https://www.studylight.org/lexicons/hebrew/3039.html)

[Bible Hub](https://biblehub.com/str/hebrew/3039.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03039)

[Bible Bento](https://biblebento.com/dictionary/H3039.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3039/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3039.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3039)
