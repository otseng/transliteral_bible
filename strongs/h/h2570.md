# חֹמֶשׁ

[ḥōmeš](https://www.blueletterbible.org/lexicon/h2570)

Definition: fifth (4x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2570)

[Study Light](https://www.studylight.org/lexicons/hebrew/2570.html)

[Bible Hub](https://biblehub.com/str/hebrew/2570.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02570)

[Bible Bento](https://biblebento.com/dictionary/H2570.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2570/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2570.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2570)
