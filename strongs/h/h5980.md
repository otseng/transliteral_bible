# עֻמָּה

[ʿummâ](https://www.blueletterbible.org/lexicon/h5980)

Definition: against (26x), beside (2x), answerable (1x), at (1x), hard (1x), points (1x).

Part of speech: feminine noun

Occurs 32 times in 28 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5980)

[Study Light](https://www.studylight.org/lexicons/hebrew/5980.html)

[Bible Hub](https://biblehub.com/str/hebrew/5980.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05980)

[Bible Bento](https://biblebento.com/dictionary/H5980.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5980/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5980.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5980)
