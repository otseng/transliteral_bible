# יֵרָקוֹן

[yērāqôn](https://www.blueletterbible.org/lexicon/h3420)

Definition: mildew (5x), paleness (1x).

Part of speech: masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3420)

[Study Light](https://www.studylight.org/lexicons/hebrew/3420.html)

[Bible Hub](https://biblehub.com/str/hebrew/3420.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03420)

[Bible Bento](https://biblebento.com/dictionary/H3420.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3420/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3420.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3420)
