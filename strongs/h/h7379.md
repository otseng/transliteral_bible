# רִיב

[rîḇ](https://www.blueletterbible.org/lexicon/h7379)

Definition: cause (24x), strife (16x), controversy (13x), contention (2x), miscellaneous (7x), fight, case, contest, suit

Part of speech: masculine noun

Occurs 62 times in 61 verses

Hebrew: [riyb](../h/h7378.md), [rîḇ](../h/h7379.md)

Greek: [machē](../g/g3163.md), [antilogia](../g/g485.md), [loidoria](../g/g3059.md)

Edenics: rift

Synonyms: [fight](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Fight)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7379)

[Study Light](https://www.studylight.org/lexicons/hebrew/7379.html)

[Bible Hub](https://biblehub.com/str/hebrew/7379.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07379)

[Bible Bento](https://biblebento.com/dictionary/H7379.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7379/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7379.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7379)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A8%D7%99%D7%91)
