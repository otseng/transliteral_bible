# דּוּמִיָּה

[dûmîyâ](https://www.blueletterbible.org/lexicon/h1747)

Definition: wait (2x), silence (1x), silent (1x).

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1747)

[Study Light](https://www.studylight.org/lexicons/hebrew/1747.html)

[Bible Hub](https://biblehub.com/str/hebrew/1747.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01747)

[Bible Bento](https://biblebento.com/dictionary/H1747.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1747/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1747.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1747)
