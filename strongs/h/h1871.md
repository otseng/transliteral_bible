# דַּרְכְּמוֹן

[darkᵊmôn](https://www.blueletterbible.org/lexicon/h1871)

Definition: dram (4x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1871)

[Study Light](https://www.studylight.org/lexicons/hebrew/1871.html)

[Bible Hub](https://biblehub.com/str/hebrew/1871.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01871)

[Bible Bento](https://biblebento.com/dictionary/H1871.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1871/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1871.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1871)
