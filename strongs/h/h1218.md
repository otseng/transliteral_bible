# בָּצְקַת

[Bāṣqaṯ](https://www.blueletterbible.org/lexicon/h1218)

Definition: Bozkath (1x), Boscath (1x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1218)

[Study Light](https://www.studylight.org/lexicons/hebrew/1218.html)

[Bible Hub](https://biblehub.com/str/hebrew/1218.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01218)

[Bible Bento](https://biblebento.com/dictionary/H1218.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1218/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1218.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1218)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bozkath.html)

[Video Bible](https://www.videobible.com/bible-dictionary/bozkath)