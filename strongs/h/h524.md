# אֻמָּה

['ummâ](https://www.blueletterbible.org/lexicon/h524)

Definition: nation (8x).

Part of speech: feminine noun

Occurs 8 times in 8 verses

Hebrew: ['ummah](../h/h523.md)

Greek: [ethnos](../g/g1484.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h524)

[Study Light](https://www.studylight.org/lexicons/hebrew/524.html)

[Bible Hub](https://biblehub.com/str/hebrew/524.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0524)

[Bible Bento](https://biblebento.com/dictionary/H524.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/524/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/524.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h524)
