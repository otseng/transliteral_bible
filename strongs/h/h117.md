# אַדִּיר

['addiyr](https://www.blueletterbible.org/lexicon/h117)

Definition: nobles (7x), excellent (4x), mighty (5x), principal (3x), famous (2x), gallant (1x), glorious (1x), goodly (1x), lordly (1x), noble one (1x), worthies (1x).

Part of speech: adjective

Occurs 27 times in 25 verses

Hebrew: ['adereṯ](../h/h155.md)

Synonyms: [mighty](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Mighty)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0117)

[Study Light](https://www.studylight.org/lexicons/hebrew/117.html)

[Bible Hub](https://biblehub.com/str/hebrew/117.htm)

[Morfix](https://www.morfix.co.il/en/%D7%90%D6%B7%D7%93%D6%BC%D6%B4%D7%99%D7%A8)

[NET Bible](http://classic.net.bible.org/strong.php?id=0117)

[Bible Bento](https://biblebento.com/dictionary/H0117.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0117/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0117.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h117)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%90%D7%93%D7%99%D7%A8)
