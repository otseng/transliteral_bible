# מַתְּנַי

[Matnay](https://www.blueletterbible.org/lexicon/h4982)

Definition: Mattenai (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4982)

[Study Light](https://www.studylight.org/lexicons/hebrew/4982.html)

[Bible Hub](https://biblehub.com/str/hebrew/4982.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04982)

[Bible Bento](https://biblebento.com/dictionary/H4982.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4982/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4982.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4982)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/mattenai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/mattenai)