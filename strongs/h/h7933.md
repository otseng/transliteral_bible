# שֶׁכֶן

[šeḵen](https://www.blueletterbible.org/lexicon/h7933)

Definition: habitation (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7933)

[Study Light](https://www.studylight.org/lexicons/hebrew/7933.html)

[Bible Hub](https://biblehub.com/str/hebrew/7933.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07933)

[Bible Bento](https://biblebento.com/dictionary/H7933.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7933/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7933.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7933)
