# גִּבְעָא

[GiḇʿĀ'](https://www.blueletterbible.org/lexicon/h1388)

Definition: Gibea (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1388)

[Study Light](https://www.studylight.org/lexicons/hebrew/1388.html)

[Bible Hub](https://biblehub.com/str/hebrew/1388.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01388)

[Bible Bento](https://biblebento.com/dictionary/H1388.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1388/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1388.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1388)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/gibea.html)

[Video Bible](https://www.videobible.com/bible-dictionary/gibea)