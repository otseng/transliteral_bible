# קְנִזִּי

[qᵊnizzî](https://www.blueletterbible.org/lexicon/h7074)

Definition: Kenezite (3x), Kenizzites (1x).

Part of speech: proper masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7074)

[Study Light](https://www.studylight.org/lexicons/hebrew/7074.html)

[Bible Hub](https://biblehub.com/str/hebrew/7074.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07074)

[Bible Bento](https://biblebento.com/dictionary/H7074.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7074/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7074.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7074)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/K/kenezite.html)