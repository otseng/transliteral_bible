# יְרַקְרַק

[yᵊraqraq](https://www.blueletterbible.org/lexicon/h3422)

Definition: greenish (2x), yellow (1x).

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3422)

[Study Light](https://www.studylight.org/lexicons/hebrew/3422.html)

[Bible Hub](https://biblehub.com/str/hebrew/3422.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03422)

[Bible Bento](https://biblebento.com/dictionary/H3422.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3422/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3422.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3422)
