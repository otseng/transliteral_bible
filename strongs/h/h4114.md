# מַהְפֵּכָה

[mahpēḵâ](https://www.blueletterbible.org/lexicon/h4114)

Definition: overthrow (6x).

Part of speech: feminine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4114)

[Study Light](https://www.studylight.org/lexicons/hebrew/4114.html)

[Bible Hub](https://biblehub.com/str/hebrew/4114.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04114)

[Bible Bento](https://biblebento.com/dictionary/H4114.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4114/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4114.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4114)
