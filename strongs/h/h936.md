# בּוּז

[bûz](https://www.blueletterbible.org/lexicon/h936)

Definition: despise (10x), contemned (1x), utterly (inf. for emphasis) (1x).

Part of speech: verb

Occurs 12 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h936)

[Study Light](https://www.studylight.org/lexicons/hebrew/936.html)

[Bible Hub](https://biblehub.com/str/hebrew/936.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0936)

[Bible Bento](https://biblebento.com/dictionary/H936.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/936/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/936.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h936)
