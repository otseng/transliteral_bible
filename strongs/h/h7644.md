# שֶׁבְנָא

[šeḇnā'](https://www.blueletterbible.org/lexicon/h7644)

Definition: Shebna (9x), a person of high position in the court of king Hezekiah of Judah; subsequently the secretary of Hezekiah

Part of speech: proper masculine noun

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7644)

[Study Light](https://www.studylight.org/lexicons/hebrew/7644.html)

[Bible Hub](https://biblehub.com/str/hebrew/7644.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07644)

[Bible Bento](https://biblebento.com/dictionary/H7644.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7644/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7644.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7644)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shebna.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shebna)