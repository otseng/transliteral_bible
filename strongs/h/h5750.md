# עוֹד

[ʿôḏ](https://www.blueletterbible.org/lexicon/h5750)

Definition: (488x), again, more, good while, longer, else, since, yet, still.

Part of speech: adverb, substantive

Occurs 488 times in 459 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5750)

[Study Light](https://www.studylight.org/lexicons/hebrew/5750.html)

[Bible Hub](https://biblehub.com/str/hebrew/5750.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05750)

[Bible Bento](https://biblebento.com/dictionary/H5750.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5750/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5750.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5750)
