# עוּר

[ʿûr](https://www.blueletterbible.org/lexicon/h5782)

Definition: (stir, lift....) up (40x), awake (25x), wake (6x), raise (6x), arise (1x), master (1x), raised out (1x), variant (1x), rouse, awake from darkness

Part of speech: verb

Occurs 80 times in 65 verses

Hebrew: [ʿāvar](../h/h5786.md)

Greek: [exegeirō](../g/g1825.md), [egeirō](../g/g1453.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5782)

[Study Light](https://www.studylight.org/lexicons/hebrew/5782.html)

[Bible Hub](https://biblehub.com/str/hebrew/5782.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05782)

[Bible Bento](https://biblebento.com/dictionary/H5782.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5782/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5782.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5782)
