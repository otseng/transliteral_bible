# עוֹבֵד

[ʿÔḇēḏ](https://www.blueletterbible.org/lexicon/h5744)

Definition: Obed (10x).

Part of speech: proper masculine noun

Occurs 10 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5744)

[Study Light](https://www.studylight.org/lexicons/hebrew/5744.html)

[Bible Hub](https://biblehub.com/str/hebrew/5744.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05744)

[Bible Bento](https://biblebento.com/dictionary/H5744.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5744/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5744.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5744)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/O/obed.html)

[Video Bible](https://www.videobible.com/bible-dictionary/obed)