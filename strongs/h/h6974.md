# קוּץ

[quwts](https://www.blueletterbible.org/lexicon/h6974)

Definition: awake (18x), wake (2x), arise (1x), watch (1x).

Part of speech: verb

Occurs 22 times in 21 verses

Greek: [anistēmi](../g/g450.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6974)

[Study Light](https://www.studylight.org/lexicons/hebrew/6974.html)

[Bible Hub](https://biblehub.com/str/hebrew/6974.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A7%D7%95%D6%BC%D7%A5)

[NET Bible](http://classic.net.bible.org/strong.php?id=06974)

[Bible Bento](https://biblebento.com/dictionary/H6974.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6974/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6974.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6974)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A7%D7%95%D7%A5)
