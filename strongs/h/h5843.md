# עֵטָא

[ʿēṭā'](https://www.blueletterbible.org/lexicon/h5843)

Definition: counsel (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5843)

[Study Light](https://www.studylight.org/lexicons/hebrew/5843.html)

[Bible Hub](https://biblehub.com/str/hebrew/5843.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05843)

[Bible Bento](https://biblebento.com/dictionary/H5843.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5843/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5843.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5843)
