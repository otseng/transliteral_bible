# מָרַר

[mārar](https://www.blueletterbible.org/lexicon/h4843)

Definition: bitterness (3x), bitter (2x), bitterly (2x), choler (2x), grieved (2x), vexed (2x), bitterness (with H4751) (1x), grieved him (1x), provoke (1x).

Part of speech: verb

Occurs 14 times in 13 verses

Hebrew: [memer](../h/h4470.md), [marah](../h/h4784.md), [mārâ](../h/h4787.md), [mᵊrî](../h/h4805.md), [mᵊrîrûṯ](../h/h4814.md), [mᵊrîrî](../h/h4815.md), [tamrûrîm](../h/h8563.md)

Synonyms: [bitter](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Bitter)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4843)

[Study Light](https://www.studylight.org/lexicons/hebrew/4843.html)

[Bible Hub](https://biblehub.com/str/hebrew/4843.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04843)

[Bible Bento](https://biblebento.com/dictionary/H4843.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4843/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4843.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4843)
