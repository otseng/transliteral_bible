# גְּעָרָה

[ge`arah](https://www.blueletterbible.org/lexicon/h1606)

Definition: rebuke (13x), reproof (2x).

Part of speech: feminine noun

Occurs 15 times in 14 verses

Hebrew: [gāʿar](../h/h1605.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1606)

[Study Light](https://www.studylight.org/lexicons/hebrew/1606.html)

[Bible Hub](https://biblehub.com/str/hebrew/1606.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01606)

[Bible Bento](https://biblebento.com/dictionary/H1606.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1606/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1606.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1606)
