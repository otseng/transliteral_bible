# חֲבִתִּים

[ḥăḇitîm](https://www.blueletterbible.org/lexicon/h2281)

Definition: pans (1x).

Part of speech: masculine plural noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2281)

[Study Light](https://www.studylight.org/lexicons/hebrew/2281.html)

[Bible Hub](https://biblehub.com/str/hebrew/2281.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02281)

[Bible Bento](https://biblebento.com/dictionary/H2281.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2281/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2281.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2281)
