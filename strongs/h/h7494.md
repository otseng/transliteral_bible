# רַעַשׁ

[raʿaš](https://www.blueletterbible.org/lexicon/h7494)

Definition: earthquake (6x), rushing (3x), shake (3x), fierceness (1x), confused noise (1x), commotion (1x), rattling (1x), quaking (1x).

Part of speech: masculine noun

Occurs 17 times in 16 verses

Hebrew: [rāʿaš](../h/h7493.md)

Greek: [seismos](../g/g4578.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7494)

[Study Light](https://www.studylight.org/lexicons/hebrew/7494.html)

[Bible Hub](https://biblehub.com/str/hebrew/7494.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07494)

[Bible Bento](https://biblebento.com/dictionary/H7494.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7494/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7494.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7494)
