# מָסַךְ

[māsaḵ](https://www.blueletterbible.org/lexicon/h4537)

Definition: mingle (5x), produce by mixing

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4537)

[Study Light](https://www.studylight.org/lexicons/hebrew/4537.html)

[Bible Hub](https://biblehub.com/str/hebrew/4537.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04537)

[Bible Bento](https://biblebento.com/dictionary/H4537.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4537/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4537.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4537)
