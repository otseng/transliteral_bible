# פָּתַת

[pāṯaṯ](https://www.blueletterbible.org/lexicon/h6626)

Definition: part (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6626)

[Study Light](https://www.studylight.org/lexicons/hebrew/6626.html)

[Bible Hub](https://biblehub.com/str/hebrew/6626.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06626)

[Bible Bento](https://biblebento.com/dictionary/H6626.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6626/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6626.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6626)
