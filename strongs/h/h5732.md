# עִדָּן

[ʿidān](https://www.blueletterbible.org/lexicon/h5732)

Definition: time (13x).

Part of speech: masculine noun

Occurs 13 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5732)

[Study Light](https://www.studylight.org/lexicons/hebrew/5732.html)

[Bible Hub](https://biblehub.com/str/hebrew/5732.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05732)

[Bible Bento](https://biblebento.com/dictionary/H5732.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5732/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5732.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5732)
