# הָרָן

[Hārān](https://www.blueletterbible.org/lexicon/h2039)

Definition: Haran (7x).

Part of speech: proper locative noun, proper masculine noun

Occurs 7 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2039)

[Study Light](https://www.studylight.org/lexicons/hebrew/2039.html)

[Bible Hub](https://biblehub.com/str/hebrew/2039.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02039)

[Bible Bento](https://biblebento.com/dictionary/H2039.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2039/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2039.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2039)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/haran.html)

[Video Bible](https://www.videobible.com/bible-dictionary/haran)