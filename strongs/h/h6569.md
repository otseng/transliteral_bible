# פֶּרֶשׁ

[pereš](https://www.blueletterbible.org/lexicon/h6569)

Definition: dung (7x).

Part of speech: masculine noun

Occurs 7 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6569)

[Study Light](https://www.studylight.org/lexicons/hebrew/6569.html)

[Bible Hub](https://biblehub.com/str/hebrew/6569.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06569)

[Bible Bento](https://biblebento.com/dictionary/H6569.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6569/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6569.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6569)
