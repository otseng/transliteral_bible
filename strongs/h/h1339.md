# בַּת־שֶׁבַע

[Baṯ-Šeḇaʿ](https://www.blueletterbible.org/lexicon/h1339)

Definition: Bathsheba (11x), "daughter of an oath"

Part of speech: proper feminine noun

Occurs 22 times in 11 verses

Hebrew: [bath](../h/h1323.md), [šeḇaʿ](../h/h7651.md), [shaba`](../h/h7650.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1339)

[Study Light](https://www.studylight.org/lexicons/hebrew/1339.html)

[Bible Hub](https://biblehub.com/str/hebrew/1339.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01339)

[Bible Bento](https://biblebento.com/dictionary/H1339.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1339/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1339.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1339)

[Wikipedia](https://en.wikipedia.org/wiki/Bathsheba)

[Jewish Women's Archive](https://jwa.org/encyclopedia/article/bathsheba-bible)

[Bible Fandom](https://bible.fandom.com/wiki/Bathsheba)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bathsheba.html)

[Video Bible](https://www.videobible.com/bible-dictionary/bathsheba)