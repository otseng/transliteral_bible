# כֹּפֶר

[kōp̄er](https://www.blueletterbible.org/lexicon/h3724)

Definition: ransom (8x), satisfaction (2x), bribe (2x), camphire (2x), pitch (1x), sum of money (1x), village (1x).

Part of speech: masculine noun

Occurs 17 times in 17 verses

Hebrew: [kipur](../h/h3725.md), [kāp̄ar](../h/h3722.md)

Greek: [lytron](../g/g3083.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3724)

[Study Light](https://www.studylight.org/lexicons/hebrew/3724.html)

[Bible Hub](https://biblehub.com/str/hebrew/3724.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03724)

[Bible Bento](https://biblebento.com/dictionary/H3724.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3724/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3724.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3724)
