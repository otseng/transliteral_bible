# תַּקִּיף

[taqqîp̄](https://www.blueletterbible.org/lexicon/h8623)

Definition: mightier (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8623)

[Study Light](https://www.studylight.org/lexicons/hebrew/8623.html)

[Bible Hub](https://biblehub.com/str/hebrew/8623.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08623)

[Bible Bento](https://biblebento.com/dictionary/H8623.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8623/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8623.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8623)
