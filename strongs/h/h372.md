# אִיעֶזֶר

['ÎʿEzer](https://www.blueletterbible.org/lexicon/h372)

Definition: Jeezer (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h372)

[Study Light](https://www.studylight.org/lexicons/hebrew/372.html)

[Bible Hub](https://biblehub.com/str/hebrew/372.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0372)

[Bible Bento](https://biblebento.com/dictionary/H372.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/372/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/372.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h372)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jeezer.html)