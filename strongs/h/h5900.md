# עִירוּ

[ʿÎrû](https://www.blueletterbible.org/lexicon/h5900)

Definition: Iru (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5900)

[Study Light](https://www.studylight.org/lexicons/hebrew/5900.html)

[Bible Hub](https://biblehub.com/str/hebrew/5900.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05900)

[Bible Bento](https://biblebento.com/dictionary/H5900.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5900/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5900.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5900)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/iru.html)

[Video Bible](https://www.videobible.com/bible-dictionary/iru)