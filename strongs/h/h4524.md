# מֵסַב

[mēsaḇ](https://www.blueletterbible.org/lexicon/h4524)

Definition: round about (3x), compass me about (1x), at his table (1x).

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4524)

[Study Light](https://www.studylight.org/lexicons/hebrew/4524.html)

[Bible Hub](https://biblehub.com/str/hebrew/4524.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04524)

[Bible Bento](https://biblebento.com/dictionary/H4524.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4524/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4524.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4524)
