# רָגֵעַ

[rāḡēaʿ](https://www.blueletterbible.org/lexicon/h7282)

Definition: quiet (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7282)

[Study Light](https://www.studylight.org/lexicons/hebrew/7282.html)

[Bible Hub](https://biblehub.com/str/hebrew/7282.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07282)

[Bible Bento](https://biblebento.com/dictionary/H7282.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7282/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7282.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7282)
