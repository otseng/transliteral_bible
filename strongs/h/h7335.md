# רָזַם

[rāzam](https://www.blueletterbible.org/lexicon/h7335)

Definition: wink (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7335)

[Study Light](https://www.studylight.org/lexicons/hebrew/7335.html)

[Bible Hub](https://biblehub.com/str/hebrew/7335.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07335)

[Bible Bento](https://biblebento.com/dictionary/H7335.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7335/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7335.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7335)
