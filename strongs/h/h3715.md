# כְּפִיר

[kephiyr](https://www.blueletterbible.org/lexicon/h3715)

Definition: lion (30x), villages (1x), young (1x).

Part of speech: masculine noun

Occurs 32 times in 32 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3715)

[Study Light](https://www.studylight.org/lexicons/hebrew/3715.html)

[Bible Hub](https://biblehub.com/str/hebrew/3715.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03715)

[Bible Bento](https://biblebento.com/dictionary/H3715.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3715/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3715.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3715)
