# מֻרְאָה

[mur'â](https://www.blueletterbible.org/lexicon/h4760)

Definition: crop (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4760)

[Study Light](https://www.studylight.org/lexicons/hebrew/4760.html)

[Bible Hub](https://biblehub.com/str/hebrew/4760.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04760)

[Bible Bento](https://biblebento.com/dictionary/H4760.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4760/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4760.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4760)
