# כָּלָא

[kālā'](https://www.blueletterbible.org/lexicon/h3607)

Definition: shut up (4x), stayed (3x), refrained (2x), restrained (2x), withhold (2x), keep back (1x), finish (1x), forbid (1x), kept (1x), retain (1x).

Part of speech: verb

Occurs 18 times in 17 verses

Hebrew: [kele'](../h/h3608.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3607)

[Study Light](https://www.studylight.org/lexicons/hebrew/3607.html)

[Bible Hub](https://biblehub.com/str/hebrew/3607.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03607)

[Bible Bento](https://biblebento.com/dictionary/H3607.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3607/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3607.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3607)
