# יָחַל

[yāḥal](https://www.blueletterbible.org/lexicon/h3176)

Definition: hope (22x), wait (12x), tarry (3x), trust (2x), variant (2x), stayed (1x).

Part of speech: verb

Occurs 42 times in 41 verses

Greek: [menō](../g/g3306.md)

Hebrew: [tôḥeleṯ](../h/h8431.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3176)

[Study Light](https://www.studylight.org/lexicons/hebrew/3176.html)

[Bible Hub](https://biblehub.com/str/hebrew/3176.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03176)

[Bible Bento](https://biblebento.com/dictionary/H3176.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3176/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3176.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3176)
