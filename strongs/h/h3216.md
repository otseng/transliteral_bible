# יָלַע

[yālaʿ](https://www.blueletterbible.org/lexicon/h3216)

Definition: devoureth (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [tôlāʿ](../h/h8438.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3216)

[Study Light](https://www.studylight.org/lexicons/hebrew/3216.html)

[Bible Hub](https://biblehub.com/str/hebrew/3216.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03216)

[Bible Bento](https://biblebento.com/dictionary/H3216.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3216/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3216.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3216)
