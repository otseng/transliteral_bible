# עָרוּם

[`aruwm](https://www.blueletterbible.org/lexicon/h6175)

Definition: prudent (8x), crafty (2x), subtil (1x), sly, sensible, cunning, deceitful

Part of speech: adjective

Occurs 11 times in 11 verses

Hebrew: ['arowm](../h/h6174.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6175)

[Study Light](https://www.studylight.org/lexicons/hebrew/6175.html)

[Bible Hub](https://biblehub.com/str/hebrew/6175.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A2%D6%B8%D7%A8%D7%95%D6%BC%D7%9D)

[NET Bible](http://classic.net.bible.org/strong.php?id=06175)

[Bible Bento](https://biblebento.com/dictionary/H6175.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6175/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6175.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6175)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A2%D7%A8%D7%95%D7%9D)
