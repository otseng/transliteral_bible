# עֲקַר

[ʿăqar](https://www.blueletterbible.org/lexicon/h6132)

Definition: plucked up by the roots (1x).

Part of speech: verb

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6132)

[Study Light](https://www.studylight.org/lexicons/hebrew/6132.html)

[Bible Hub](https://biblehub.com/str/hebrew/6132.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06132)

[Bible Bento](https://biblebento.com/dictionary/H6132.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6132/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6132.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6132)
