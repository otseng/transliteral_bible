# צָנַע

[ṣānaʿ](https://www.blueletterbible.org/lexicon/h6800)

Definition: lowly (1x), humbly (1x).

Part of speech: verb

Occurs 2 times in 2 verses

Greek: [tapeinos](../g/g5011.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6800)

[Study Light](https://www.studylight.org/lexicons/hebrew/6800.html)

[Bible Hub](https://biblehub.com/str/hebrew/6800.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06800)

[Bible Bento](https://biblebento.com/dictionary/H6800.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6800/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6800.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6800)
