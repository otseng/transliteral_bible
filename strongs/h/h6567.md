# פָּרַשׁ

[pāraš](https://www.blueletterbible.org/lexicon/h6567)

Definition: shew (1x), scatter (1x), declare (1x), distinctly (1x), sting (1x), separate, clarify, pierce, disperse

Part of speech: verb

Occurs 5 times in 5 verses

Greek: [didaskō](../g/g1321.md), [Pharisaios](../g/g5330.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6567)

[Study Light](https://www.studylight.org/lexicons/hebrew/6567.html)

[Bible Hub](https://biblehub.com/str/hebrew/6567.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06567)

[Bible Bento](https://biblebento.com/dictionary/H6567.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6567/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6567.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6567)
