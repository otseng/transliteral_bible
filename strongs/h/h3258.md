# יַעְבֵּץ

[YaʿBēṣ](https://www.blueletterbible.org/lexicon/h3258)

Definition: Jabez (4x).

Part of speech: proper locative noun, proper masculine noun

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3258)

[Study Light](https://www.studylight.org/lexicons/hebrew/3258.html)

[Bible Hub](https://biblehub.com/str/hebrew/3258.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03258)

[Bible Bento](https://biblebento.com/dictionary/H3258.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3258/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3258.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3258)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jabez.html)