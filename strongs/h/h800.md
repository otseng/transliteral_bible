# אֶשָּׁה

['eššâ](https://www.blueletterbible.org/lexicon/h800)

Definition: fire (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h800)

[Study Light](https://www.studylight.org/lexicons/hebrew/800.html)

[Bible Hub](https://biblehub.com/str/hebrew/800.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0800)

[Bible Bento](https://biblebento.com/dictionary/H800.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/800/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/800.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h800)
