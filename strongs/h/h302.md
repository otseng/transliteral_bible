# אֲחִיתֹפֶל

['Ăḥîṯōp̄El](https://www.blueletterbible.org/lexicon/h302)

Definition: Ahithophel (20x), "my brother is foolish", counsellor of David, Bathsheba's grandfather

Part of speech: proper masculine noun

Occurs 20 times in 17 verses

Hebrew: ['ach](../h/h251.md), [tāp̄ēl](../h/h8602.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h302)

[Study Light](https://www.studylight.org/lexicons/hebrew/302.html)

[Bible Hub](https://biblehub.com/str/hebrew/302.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0302)

[Bible Bento](https://biblebento.com/dictionary/H302.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/302/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/302.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h302)

[Wikipedia](https://en.wikipedia.org/wiki/Ahitophel)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ahithophel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ahithophel)