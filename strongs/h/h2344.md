# חוֹל

[ḥôl](https://www.blueletterbible.org/lexicon/h2344)

Definition: sand (23x).

Part of speech: masculine noun

Occurs 23 times in 23 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2344)

[Study Light](https://www.studylight.org/lexicons/hebrew/2344.html)

[Bible Hub](https://biblehub.com/str/hebrew/2344.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02344)

[Bible Bento](https://biblebento.com/dictionary/H2344.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2344/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2344.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2344)
