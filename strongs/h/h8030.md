# שִׁלְשָׁה

[Šilšâ](https://www.blueletterbible.org/lexicon/h8030)

Definition: Shilshah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8030)

[Study Light](https://www.studylight.org/lexicons/hebrew/8030.html)

[Bible Hub](https://biblehub.com/str/hebrew/8030.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08030)

[Bible Bento](https://biblebento.com/dictionary/H8030.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8030/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8030.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8030)

[Video Bible](https://www.videobible.com/bible-dictionary/shilshah)