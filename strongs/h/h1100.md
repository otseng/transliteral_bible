# בְּלִיַּעַל

[beliya`al](https://www.blueletterbible.org/lexicon/h1100)

Definition: Belial (16x), wicked (5x), ungodly (3x), evil (1x), naughty 1 ungodly men (1x), worthlessness

Part of speech: masculine noun

Occurs 27 times in 26 verses

Greek: [anomia](../g/g458.md), [belial](../g/g955.md) 

Synonyms: [evil](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Evil)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1100)

[Study Light](https://www.studylight.org/lexicons/hebrew/1100.html)

[Bible Hub](https://biblehub.com/str/hebrew/1100.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01100)

[Bible Bento](https://biblebento.com/dictionary/H1100.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1100/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1100.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1100)

[Wikipedia](https://en.wikipedia.org/wiki/Belial)

[New Advent](https://www.newadvent.org/cathen/02408a.htm)

[Jewish Encyclopedia](http://jewishencyclopedia.com/articles/2805-belial)
