# קַלָּע

[qallāʿ](https://www.blueletterbible.org/lexicon/h7051)

Definition: slinger (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7051)

[Study Light](https://www.studylight.org/lexicons/hebrew/7051.html)

[Bible Hub](https://biblehub.com/str/hebrew/7051.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07051)

[Bible Bento](https://biblebento.com/dictionary/H7051.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7051/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7051.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7051)
