# אַחְיוֹ

['Aḥyô](https://www.blueletterbible.org/lexicon/h283)

Definition: Ahio (6x).

Part of speech: proper masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h283)

[Study Light](https://www.studylight.org/lexicons/hebrew/283.html)

[Bible Hub](https://biblehub.com/str/hebrew/283.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0283)

[Bible Bento](https://biblebento.com/dictionary/H283.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/283/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/283.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h283)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ahio.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ahio)