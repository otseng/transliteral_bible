# בֶּן־גֶּבֶר

[Ben-Geḇer](https://www.blueletterbible.org/lexicon/h1127)

Definition: son of Geber (1x).

Part of speech: proper masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1127)

[Study Light](https://www.studylight.org/lexicons/hebrew/1127.html)

[Bible Hub](https://biblehub.com/str/hebrew/1127.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01127)

[Bible Bento](https://biblebento.com/dictionary/H1127.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1127/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1127.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1127)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/son.html)