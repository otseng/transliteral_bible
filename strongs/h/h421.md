# אָלָה

['ālâ](https://www.blueletterbible.org/lexicon/h421)

Definition: lament (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h421)

[Study Light](https://www.studylight.org/lexicons/hebrew/421.html)

[Bible Hub](https://biblehub.com/str/hebrew/421.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0421)

[Bible Bento](https://biblebento.com/dictionary/H421.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/421/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/421.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h421)
