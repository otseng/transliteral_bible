# בַּר

[bar](https://www.blueletterbible.org/lexicon/h1248)

Definition: son (4x)

Part of speech: masculine noun

Occurs 4 times in 2 verses

Hebrew: [ben](../h/h1121.md)

Greek: [teknon](../g/g5043.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1248)

[Study Light](https://www.studylight.org/lexicons/hebrew/1248.html)

[Bible Hub](https://biblehub.com/str/hebrew/1248.htm)

[Morfix](https://www.morfix.co.il/en/%D7%91%D6%BC%D6%B7%D7%A8)

[NET Bible](http://classic.net.bible.org/strong.php?id=01248)

[Bible Bento](https://biblebento.com/dictionary/H1248.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1248/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1248.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1248)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%91%D7%A8)
