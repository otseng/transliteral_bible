# חִדֶּקֶל

[Ḥideqel](https://www.blueletterbible.org/lexicon/h2313)

Definition: Hiddekel (2x), "rapid", Tigris

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2313)

[Study Light](https://www.studylight.org/lexicons/hebrew/2313.html)

[Bible Hub](https://biblehub.com/str/hebrew/2313.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02313)

[Bible Bento](https://biblebento.com/dictionary/H2313.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2313/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2313.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2313)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hiddekel.html)