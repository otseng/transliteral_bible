# שָׁבוּעַ

[šāḇûaʿ](https://www.blueletterbible.org/lexicon/h7620)

Definition: week (19x), seven (1x).

Part of speech: masculine noun

Occurs 20 times in 17 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7620)

[Study Light](https://www.studylight.org/lexicons/hebrew/7620.html)

[Bible Hub](https://biblehub.com/str/hebrew/7620.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07620)

[Bible Bento](https://biblebento.com/dictionary/H7620.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7620/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7620.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7620)
