# יָרִיב

[yārîḇ](https://www.blueletterbible.org/lexicon/h3401)

Definition: contend (2x), strive (1x), opponent, adversary

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3401)

[Study Light](https://www.studylight.org/lexicons/hebrew/3401.html)

[Bible Hub](https://biblehub.com/str/hebrew/3401.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03401)

[Bible Bento](https://biblebento.com/dictionary/H3401.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3401/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3401.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3401)
