# רִמּוֹן פֶּרֶץ

[Rimmôn Pereṣ](https://www.blueletterbible.org/lexicon/h7428)

Definition: Rimmonparez (2x).

Part of speech: proper locative noun

Occurs 4 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7428)

[Study Light](https://www.studylight.org/lexicons/hebrew/7428.html)

[Bible Hub](https://biblehub.com/str/hebrew/7428.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07428)

[Bible Bento](https://biblebento.com/dictionary/H7428.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7428/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7428.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7428)
