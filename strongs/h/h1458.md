# גַּו

[gav](https://www.blueletterbible.org/lexicon/h1458)

Definition: back (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1458)

[Study Light](https://www.studylight.org/lexicons/hebrew/1458.html)

[Bible Hub](https://biblehub.com/str/hebrew/1458.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01458)

[Bible Bento](https://biblebento.com/dictionary/H1458.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1458/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1458.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1458)
