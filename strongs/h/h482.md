# אֵלֶם

['ēlem](https://www.blueletterbible.org/lexicon/h482)

Definition: congregation (1x).

Part of speech: masculine adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h482)

[Study Light](https://www.studylight.org/lexicons/hebrew/482.html)

[Bible Hub](https://biblehub.com/str/hebrew/482.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0482)

[Bible Bento](https://biblebento.com/dictionary/H482.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/482/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/482.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h482)
