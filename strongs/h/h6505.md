# פֶּרֶד

[pereḏ](https://www.blueletterbible.org/lexicon/h6505)

Definition: mule (15x).

Part of speech: masculine noun

Occurs 14 times in 12 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6505)

[Study Light](https://www.studylight.org/lexicons/hebrew/6505.html)

[Bible Hub](https://biblehub.com/str/hebrew/6505.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06505)

[Bible Bento](https://biblebento.com/dictionary/H6505.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6505/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6505.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6505)
