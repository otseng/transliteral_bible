# מְתֹם

[mᵊṯōm](https://www.blueletterbible.org/lexicon/h4974)

Definition: soundness (3x), men (1x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

Hebrew: [tom](../h/h8537.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4974)

[Study Light](https://www.studylight.org/lexicons/hebrew/4974.html)

[Bible Hub](https://biblehub.com/str/hebrew/4974.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04974)

[Bible Bento](https://biblebento.com/dictionary/H4974.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4974/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4974.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4974)
