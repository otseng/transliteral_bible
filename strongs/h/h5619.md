# סָקַל

[sāqal](https://www.blueletterbible.org/lexicon/h5619)

Definition: stone (15x), surely (2x), cast (1x), gather out (1x), gather out stones (1x), stoning (1x), threw (1x).

Part of speech: verb

Occurs 22 times in 20 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5619)

[Study Light](https://www.studylight.org/lexicons/hebrew/5619.html)

[Bible Hub](https://biblehub.com/str/hebrew/5619.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05619)

[Bible Bento](https://biblebento.com/dictionary/H5619.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5619/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5619.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5619)
