# עֲזַרְאֵל

[ʿĂzar'Ēl](https://www.blueletterbible.org/lexicon/h5832)

Definition: Azareel (5x), Azarael (1x).

Part of speech: proper masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5832)

[Study Light](https://www.studylight.org/lexicons/hebrew/5832.html)

[Bible Hub](https://biblehub.com/str/hebrew/5832.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05832)

[Bible Bento](https://biblebento.com/dictionary/H5832.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5832/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5832.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5832)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/azareel.html)