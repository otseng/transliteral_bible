# נֶחֱלָמִי

[neḥĕlāmî](https://www.blueletterbible.org/lexicon/h5161)

Definition: Nehelamite (3x).

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5161)

[Study Light](https://www.studylight.org/lexicons/hebrew/5161.html)

[Bible Hub](https://biblehub.com/str/hebrew/5161.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05161)

[Bible Bento](https://biblebento.com/dictionary/H5161.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5161/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5161.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5161)
