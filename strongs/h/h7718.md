# שֹׁהַם

[šōham](https://www.blueletterbible.org/lexicon/h7718)

Definition: onyx (11x).

Part of speech: masculine noun

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7718)

[Study Light](https://www.studylight.org/lexicons/hebrew/7718.html)

[Bible Hub](https://biblehub.com/str/hebrew/7718.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07718)

[Bible Bento](https://biblebento.com/dictionary/H7718.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7718/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7718.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7718)
