# דַּיָּה

[dayyâ](https://www.blueletterbible.org/lexicon/h1772)

Definition: vulture (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1772)

[Study Light](https://www.studylight.org/lexicons/hebrew/1772.html)

[Bible Hub](https://biblehub.com/str/hebrew/1772.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01772)

[Bible Bento](https://biblebento.com/dictionary/H1772.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1772/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1772.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1772)
