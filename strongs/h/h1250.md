# בַּר

[bar](https://www.blueletterbible.org/lexicon/h1250)

Definition: corn (9x), wheat (5x).

Part of speech: masculine noun

Occurs 14 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1250)

[Study Light](https://www.studylight.org/lexicons/hebrew/1250.html)

[Bible Hub](https://biblehub.com/str/hebrew/1250.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01250)

[Bible Bento](https://biblebento.com/dictionary/H1250.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1250/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1250.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1250)
