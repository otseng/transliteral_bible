# גַּו

[gav](https://www.blueletterbible.org/lexicon/h1459)

Definition: midst (10x), within the same (1x), wherein (1x), therein (1x).

Part of speech: masculine noun

Occurs 13 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1459)

[Study Light](https://www.studylight.org/lexicons/hebrew/1459.html)

[Bible Hub](https://biblehub.com/str/hebrew/1459.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01459)

[Bible Bento](https://biblebento.com/dictionary/H1459.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1459/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1459.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1459)
