# שָׁפָה

[šāp̄â](https://www.blueletterbible.org/lexicon/h8192)

Definition: high (1x), stick out (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8192)

[Study Light](https://www.studylight.org/lexicons/hebrew/8192.html)

[Bible Hub](https://biblehub.com/str/hebrew/8192.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08192)

[Bible Bento](https://biblebento.com/dictionary/H8192.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8192/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8192.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8192)
