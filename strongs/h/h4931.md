# מִשְׁמֶרֶת

[mišmereṯ](https://www.blueletterbible.org/lexicon/h4931)

Definition: charge (50x), ward (9x), watch (7x), keep (7x), ordinance (3x), offices (1x), safeguard (1x), obligation, injunction, post

Part of speech: feminine noun

Occurs 78 times in 69 verses

Hebrew: [shamar](../h/h8104.md), [mišmār](../h/h4929.md)

Greek: [parembolē](../g/g3925.md), [phylakē](../g/g5438.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4931)

[Study Light](https://www.studylight.org/lexicons/hebrew/4931.html)

[Bible Hub](https://biblehub.com/str/hebrew/4931.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04931)

[Bible Bento](https://biblebento.com/dictionary/H4931.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4931/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4931.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4931)
