# זָבַח

[zabach](https://www.blueletterbible.org/lexicon/h2076)

Definition: sacrifice (85x), offer (39x), kill (5x), slay (5x).

Part of speech: verb

Occurs 134 times in 127 verses

Hebrew: [zebach](../h/h2077.md), [mizbeach](../h/h4196.md)

Greek: [prospherō](../g/g4374.md)

Root: [זָבַח](http://www.semiticroots.net/index.php?r=word/view&id=27)

Synonyms: [kill](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Kill)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2076)

[Study Light](https://www.studylight.org/lexicons/hebrew/2076.html)

[Bible Hub](https://biblehub.com/str/hebrew/2076.htm)

[Morfix](https://www.morfix.co.il/en/%D7%96%D6%B8%D7%91%D6%B7%D7%97)

[NET Bible](http://classic.net.bible.org/strong.php?id=02076)

[Bible Bento](https://biblebento.com/dictionary/H2076.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2076/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2076.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2076)
