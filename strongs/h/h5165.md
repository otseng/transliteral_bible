# נֶחָמָה

[neḥāmâ](https://www.blueletterbible.org/lexicon/h5165)

Definition: comfort (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

Hebrew: [niḥum](../h/h5150.md), [nacham](../h/h5162.md), [nōḥam](../h/h5164.md)

Synonyms: [comfort](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Comfort)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5165)

[Study Light](https://www.studylight.org/lexicons/hebrew/5165.html)

[Bible Hub](https://biblehub.com/str/hebrew/5165.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05165)

[Bible Bento](https://biblebento.com/dictionary/H5165.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5165/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5165.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5165)
