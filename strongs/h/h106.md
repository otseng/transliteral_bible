# אֶגְרֹף

['eḡrōp̄](https://www.blueletterbible.org/lexicon/h106)

Definition: fist (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0106)

[Study Light](https://www.studylight.org/lexicons/hebrew/106.html)

[Bible Hub](https://biblehub.com/str/hebrew/106.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0106)

[Bible Bento](https://biblebento.com/dictionary/H106.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/106/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/106.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h106)
