# פַּרְבָּר

[Parbār](https://www.blueletterbible.org/lexicon/h6503)

Definition: Parbar (2x), suburb (1x).

Part of speech: proper locative noun

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6503)

[Study Light](https://www.studylight.org/lexicons/hebrew/6503.html)

[Bible Hub](https://biblehub.com/str/hebrew/6503.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06503)

[Bible Bento](https://biblebento.com/dictionary/H6503.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6503/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6503.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6503)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/parbar.html)

[Video Bible](https://www.videobible.com/bible-dictionary/parbar)