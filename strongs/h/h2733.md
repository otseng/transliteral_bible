# חֲרֹדִי

[Ḥărōḏî](https://www.blueletterbible.org/lexicon/h2733)

Definition: Harodite (2x).

Part of speech: masculine patrial noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2733)

[Study Light](https://www.studylight.org/lexicons/hebrew/2733.html)

[Bible Hub](https://biblehub.com/str/hebrew/2733.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02733)

[Bible Bento](https://biblebento.com/dictionary/H2733.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2733/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2733.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2733)
