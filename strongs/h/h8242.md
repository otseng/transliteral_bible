# שַׂק

[śaq](https://www.blueletterbible.org/lexicon/h8242)

Definition: sackcloth (41x), sack (6x), sackclothes (1x).

Part of speech: masculine noun

Occurs 48 times in 46 verses

Hebrew: [šāqaq](../h/h8264.md)

Greek: [sakkos](../g/g4526.md)

Edenics: sack

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8242)

[Study Light](https://www.studylight.org/lexicons/hebrew/8242.html)

[Bible Hub](https://biblehub.com/str/hebrew/8242.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08242)

[Bible Bento](https://biblebento.com/dictionary/H8242.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8242/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8242.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8242)

[Wikipedia](https://en.wikipedia.org/wiki/Sackcloth)

[Jewish Encyclopedia](https://jewishencyclopedia.com/articles/12981-sackcloth)

[Bible Study Tools](https://www.biblestudytools.com/bible-study/topical-studies/why-did-people-wear-sackcloth-and-ashes-when-they-grieved.html)

