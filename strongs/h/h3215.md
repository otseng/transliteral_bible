# יְלָלָה

[yᵊlālâ](https://www.blueletterbible.org/lexicon/h3215)

Definition: howling (5x).

Part of speech: feminine noun

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3215)

[Study Light](https://www.studylight.org/lexicons/hebrew/3215.html)

[Bible Hub](https://biblehub.com/str/hebrew/3215.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03215)

[Bible Bento](https://biblebento.com/dictionary/H3215.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3215/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3215.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3215)
