# חֶרֶשׁ

[Ḥereš](https://www.blueletterbible.org/lexicon/h2792)

Definition: Heresh (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2792)

[Study Light](https://www.studylight.org/lexicons/hebrew/2792.html)

[Bible Hub](https://biblehub.com/str/hebrew/2792.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02792)

[Bible Bento](https://biblebento.com/dictionary/H2792.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2792/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2792.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2792)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/heresh.html)

[Video Bible](https://www.videobible.com/bible-dictionary/heresh)