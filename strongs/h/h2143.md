# זֵכֶר

[zeker](https://www.blueletterbible.org/lexicon/h2143)

Definition: remembrance (11x), memorial (5x), memory (5x), remembered (1x), scent (1x), remain, remnant

Part of speech: masculine noun

Occurs 23 times in 23 verses

Hebrew: ['azkārâ](../h/h234.md), [zāḵûr](../h/h2138.md), [zakar](../h/h2142.md), [zāḵār](../h/h2145.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2143)

[Study Light](https://www.studylight.org/lexicons/hebrew/2143.html)

[Bible Hub](https://biblehub.com/str/hebrew/2143.htm)

[Morfix](https://www.morfix.co.il/en/%D7%96%D6%B5%D7%9B%D6%B6%D7%A8)

[NET Bible](http://classic.net.bible.org/strong.php?id=02143)

[Bible Bento](https://biblebento.com/dictionary/H2143.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2143/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2143.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2143)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%96%D7%9B%D7%A8)
