# לְבָנָה

[lᵊḇānâ](https://www.blueletterbible.org/lexicon/h3842)

Definition: moon (3x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3842)

[Study Light](https://www.studylight.org/lexicons/hebrew/3842.html)

[Bible Hub](https://biblehub.com/str/hebrew/3842.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03842)

[Bible Bento](https://biblebento.com/dictionary/H3842.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3842/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3842.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3842)
