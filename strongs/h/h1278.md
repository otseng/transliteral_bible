# בְּרִיאָה

[bᵊrî'â](https://www.blueletterbible.org/lexicon/h1278)

Definition: new thing (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Hebrew: [bara'](../h/h1254.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1278)

[Study Light](https://www.studylight.org/lexicons/hebrew/1278.html)

[Bible Hub](https://biblehub.com/str/hebrew/1278.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01278)

[Bible Bento](https://biblebento.com/dictionary/H1278.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1278/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1278.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1278)
