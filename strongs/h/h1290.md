# בֶּרֶךְ

[bereḵ](https://www.blueletterbible.org/lexicon/h1290)

Definition: knee (25x).

Part of speech: feminine noun

Occurs 26 times in 25 verses

Hebrew: [barak](../h/h1288.md), [bᵊraḵ](../h/h1289.md), [bereḵ](../h/h1291.md), [bĕrakah](../h/h1293.md), ['aḇrēḵ](../h/h86.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1290)

[Study Light](https://www.studylight.org/lexicons/hebrew/1290.html)

[Bible Hub](https://biblehub.com/str/hebrew/1290.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01290)

[Bible Bento](https://biblebento.com/dictionary/H1290.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1290/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1290.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1290)
