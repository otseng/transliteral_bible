# מֶלְתָּחָה

[meltāḥâ](https://www.blueletterbible.org/lexicon/h4458)

Definition: vestry (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4458)

[Study Light](https://www.studylight.org/lexicons/hebrew/4458.html)

[Bible Hub](https://biblehub.com/str/hebrew/4458.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04458)

[Bible Bento](https://biblebento.com/dictionary/H4458.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4458/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4458.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4458)
