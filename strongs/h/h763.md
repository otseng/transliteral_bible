# אֲרַם נַהֲרַיִם

['Ăram nahărayim](https://www.blueletterbible.org/lexicon/h763)

Definition: Mesopotamia (5x), Aramnaharaim (1x).

Part of speech: proper locative noun

Occurs 10 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0763)

[Study Light](https://www.studylight.org/lexicons/hebrew/763.html)

[Bible Hub](https://biblehub.com/str/hebrew/763.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0763)

[Bible Bento](https://biblebento.com/dictionary/H763.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/763/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/763.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h763)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/mesopotamia.html)

[Video Bible](https://www.videobible.com/bible-dictionary/mesopotamia)