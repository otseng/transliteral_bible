# רוּעַ

[rûaʿ](https://www.blueletterbible.org/lexicon/h7321)

Definition: shout (23x), noise (7x), ..alarm (4x), cry (4x), triumph (3x), smart (1x), miscellaneous (4x).

Part of speech: verb

Occurs 44 times in 40 verses

Greek: [krazō](../g/g2896.md), [anakrazō](../g/g349.md), [boaō](../g/g994.md), [anaboaō](../g/g310.md), [kēryssō](../g/g2784.md)

Synonyms: [cry](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Cry)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7321)

[Study Light](https://www.studylight.org/lexicons/hebrew/7321.html)

[Bible Hub](https://biblehub.com/str/hebrew/7321.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07321)

[Bible Bento](https://biblebento.com/dictionary/H7321.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7321/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7321.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7321)
