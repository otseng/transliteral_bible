# מַדְמֵנָה

[maḏmēnâ](https://www.blueletterbible.org/lexicon/h4088)

Definition: Madmenah (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4088)

[Study Light](https://www.studylight.org/lexicons/hebrew/4088.html)

[Bible Hub](https://biblehub.com/str/hebrew/4088.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04088)

[Bible Bento](https://biblebento.com/dictionary/H4088.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4088/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4088.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4088)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/madmenah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/madmenah)