# הוֹתִיר

[Hôṯîr](https://www.blueletterbible.org/lexicon/h1956)

Definition: Hothir (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1956)

[Study Light](https://www.studylight.org/lexicons/hebrew/1956.html)

[Bible Hub](https://biblehub.com/str/hebrew/1956.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01956)

[Bible Bento](https://biblebento.com/dictionary/H1956.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1956/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1956.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1956)

[Video Bible](https://www.videobible.com/bible-dictionary/hothir)