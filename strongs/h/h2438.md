# חִירָם

[Ḥîrām](https://www.blueletterbible.org/lexicon/h2438)

Definition: Hiram (23x), variant (1x).

Part of speech: proper masculine noun

Occurs 26 times in 21 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2438)

[Study Light](https://www.studylight.org/lexicons/hebrew/2438.html)

[Bible Hub](https://biblehub.com/str/hebrew/2438.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02438)

[Bible Bento](https://biblebento.com/dictionary/H2438.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2438/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2438.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2438)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hiram.html)

[Video Bible](https://www.videobible.com/bible-dictionary/hiram)