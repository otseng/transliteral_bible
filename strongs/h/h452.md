# אֵלִיָּה

['Ēlîyâ](https://www.blueletterbible.org/lexicon/h452)

Definition: Elijah (69x), Eliah (2x), "Yah is God", prophet of the reign of Ahab

Part of speech: proper masculine noun

Occurs 71 times in 65 verses

Hebrew: ['el](../h/h410.md), [Yahh](../h/h3050.md)

Greek: [Ēlias](../g/g2243.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h452)

[Study Light](https://www.studylight.org/lexicons/hebrew/452.html)

[Bible Hub](https://biblehub.com/str/hebrew/452.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0452)

[Bible Bento](https://biblebento.com/dictionary/H452.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/452/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/452.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h452)

[Wikipedia](https://en.wikipedia.org/wiki/Elijah)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/elijah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/elijah)