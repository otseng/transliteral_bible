# פְּצלוֹת

[pᵊṣlôṯ](https://www.blueletterbible.org/lexicon/h6479)

Definition: strake (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6479)

[Study Light](https://www.studylight.org/lexicons/hebrew/6479.html)

[Bible Hub](https://biblehub.com/str/hebrew/6479.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06479)

[Bible Bento](https://biblebento.com/dictionary/H6479.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6479/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6479.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6479)
