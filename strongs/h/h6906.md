# קָבַע

[qāḇaʿ](https://www.blueletterbible.org/lexicon/h6906)

Definition: rob (4x), spoil (2x).

Part of speech: verb

Occurs 6 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6906)

[Study Light](https://www.studylight.org/lexicons/hebrew/6906.html)

[Bible Hub](https://biblehub.com/str/hebrew/6906.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06906)

[Bible Bento](https://biblebento.com/dictionary/H6906.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6906/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6906.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6906)
