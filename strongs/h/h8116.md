# שִׁמְרִית

[Šimrîṯ](https://www.blueletterbible.org/lexicon/h8116)

Definition: Shimrith (1x).

Part of speech: proper feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8116)

[Study Light](https://www.studylight.org/lexicons/hebrew/8116.html)

[Bible Hub](https://biblehub.com/str/hebrew/8116.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08116)

[Bible Bento](https://biblebento.com/dictionary/H8116.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8116/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8116.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8116)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shimrith.html)