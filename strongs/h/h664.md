# אֲפֵקָה

['Ăp̄Ēqâ](https://www.blueletterbible.org/lexicon/h664)

Definition: Aphekah (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h664)

[Study Light](https://www.studylight.org/lexicons/hebrew/664.html)

[Bible Hub](https://biblehub.com/str/hebrew/664.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0664)

[Bible Bento](https://biblebento.com/dictionary/H664.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/664/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/664.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h664)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/aphekah.html)