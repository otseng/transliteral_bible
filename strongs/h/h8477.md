# תַּחַשׁ

[Taḥaš](https://www.blueletterbible.org/lexicon/h8477)

Definition: Thahash (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8477)

[Study Light](https://www.studylight.org/lexicons/hebrew/8477.html)

[Bible Hub](https://biblehub.com/str/hebrew/8477.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08477)

[Bible Bento](https://biblebento.com/dictionary/H8477.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8477/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8477.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8477)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/T/thahash.html)