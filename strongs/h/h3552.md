# כּוּב

[Kûḇ](https://www.blueletterbible.org/lexicon/h3552)

Definition: Chub (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3552)

[Study Light](https://www.studylight.org/lexicons/hebrew/3552.html)

[Bible Hub](https://biblehub.com/str/hebrew/3552.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03552)

[Bible Bento](https://biblebento.com/dictionary/H3552.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3552/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3552.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3552)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/chub.html)