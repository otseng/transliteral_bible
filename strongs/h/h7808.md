# שֵׂחַ

[śēaḥ](https://www.blueletterbible.org/lexicon/h7808)

Definition: thought (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7808)

[Study Light](https://www.studylight.org/lexicons/hebrew/7808.html)

[Bible Hub](https://biblehub.com/str/hebrew/7808.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07808)

[Bible Bento](https://biblebento.com/dictionary/H7808.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7808/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7808.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7808)
