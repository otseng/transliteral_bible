# פָּרָז

[pārāz](https://www.blueletterbible.org/lexicon/h6518)

Definition: villages (1x).

Part of speech: masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6518)

[Study Light](https://www.studylight.org/lexicons/hebrew/6518.html)

[Bible Hub](https://biblehub.com/str/hebrew/6518.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06518)

[Bible Bento](https://biblebento.com/dictionary/H6518.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6518/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6518.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6518)
