# מַרְבֵּק

[marbēq](https://www.blueletterbible.org/lexicon/h4770)

Definition: stall (2x), fat (1x), fatted (1x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4770)

[Study Light](https://www.studylight.org/lexicons/hebrew/4770.html)

[Bible Hub](https://biblehub.com/str/hebrew/4770.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04770)

[Bible Bento](https://biblebento.com/dictionary/H4770.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4770/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4770.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4770)
