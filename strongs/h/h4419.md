# מַלָּח

[mallāḥ](https://www.blueletterbible.org/lexicon/h4419)

Definition: mariners (4x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4419)

[Study Light](https://www.studylight.org/lexicons/hebrew/4419.html)

[Bible Hub](https://biblehub.com/str/hebrew/4419.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04419)

[Bible Bento](https://biblebento.com/dictionary/H4419.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4419/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4419.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4419)
