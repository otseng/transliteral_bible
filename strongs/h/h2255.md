# חֲבַל

[ḥăḇal](https://www.blueletterbible.org/lexicon/h2255)

Definition: destroy (5x), hurt (1x).

Part of speech: verb

Occurs 6 times in 6 verses

Hebrew: [chabal](../h/h2254.md), [chebel](../h/h2256.md), [ḥăḇal](../h/h2257.md)

Synonyms: [destroy](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Destroy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2255)

[Study Light](https://www.studylight.org/lexicons/hebrew/2255.html)

[Bible Hub](https://biblehub.com/str/hebrew/2255.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02255)

[Bible Bento](https://biblebento.com/dictionary/H2255.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2255/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2255.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2255)
