# בִּינָה

[bînâ](https://www.blueletterbible.org/lexicon/h998)

Definition: understanding (32x), wisdom (2x), knowledge (1x), meaning (1x), perfectly (1x), understand (1x).

Part of speech: feminine noun

Occurs 38 times in 38 verses

Hebrew: [bîn](../h/h995.md)

Greek: [nous](../g/g3563.md), [synesis](../g/g4907.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0998)

[Study Light](https://www.studylight.org/lexicons/hebrew/998.html)

[Bible Hub](https://biblehub.com/str/hebrew/998.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0998)

[Bible Bento](https://biblebento.com/dictionary/H998.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/998/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/998.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h998)
