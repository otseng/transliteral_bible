# רֹן

[rōn](https://www.blueletterbible.org/lexicon/h7438)

Definition: song (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7438)

[Study Light](https://www.studylight.org/lexicons/hebrew/7438.html)

[Bible Hub](https://biblehub.com/str/hebrew/7438.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07438)

[Bible Bento](https://biblebento.com/dictionary/H7438.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7438/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7438.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7438)
