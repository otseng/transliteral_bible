# בֵּית־גָּדֵר

[Bêṯ-Gāḏēr](https://www.blueletterbible.org/lexicon/h1013)

Definition: Bethgader (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1013)

[Study Light](https://www.studylight.org/lexicons/hebrew/1013.html)

[Bible Hub](https://biblehub.com/str/hebrew/1013.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01013)

[Bible Bento](https://biblebento.com/dictionary/H1013.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1013/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1013.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1013)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bethgader.html)