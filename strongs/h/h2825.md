# חֲשֵׁכָה

[ḥăšēḵâ](https://www.blueletterbible.org/lexicon/h2825)

Definition: darkness (5x).

Part of speech: feminine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2825)

[Study Light](https://www.studylight.org/lexicons/hebrew/2825.html)

[Bible Hub](https://biblehub.com/str/hebrew/2825.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02825)

[Bible Bento](https://biblebento.com/dictionary/H2825.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2825/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2825.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2825)
