# טָחָה

[ṭāḥâ](https://www.blueletterbible.org/lexicon/h2909)

Definition: bowshot (with H7198) (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2909)

[Study Light](https://www.studylight.org/lexicons/hebrew/2909.html)

[Bible Hub](https://biblehub.com/str/hebrew/2909.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02909)

[Bible Bento](https://biblebento.com/dictionary/H2909.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2909/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2909.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2909)
