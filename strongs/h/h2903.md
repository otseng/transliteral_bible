# טוֹטָפוֹת

[ṭôṭāp̄ôṯ](https://www.blueletterbible.org/lexicon/h2903)

Definition: frontlet (3x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2903)

[Study Light](https://www.studylight.org/lexicons/hebrew/2903.html)

[Bible Hub](https://biblehub.com/str/hebrew/2903.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02903)

[Bible Bento](https://biblebento.com/dictionary/H2903.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2903/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2903.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2903)
