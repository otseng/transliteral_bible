# רוּם

[rûm](https://www.blueletterbible.org/lexicon/h7313)

Definition: lift up (2x), extol (1x), set up (1x).

Part of speech: verb

Occurs 4 times in 4 verses

Hebrew: [ruwm](../h/h7311.md), [rûm](../h/h7312.md), [rûm](../h/h7314.md), [rôm](../h/h7315.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7313)

[Study Light](https://www.studylight.org/lexicons/hebrew/7313.html)

[Bible Hub](https://biblehub.com/str/hebrew/7313.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07313)

[Bible Bento](https://biblebento.com/dictionary/H7313.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7313/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7313.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7313)
