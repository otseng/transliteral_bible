# קָרַע

[qāraʿ](https://www.blueletterbible.org/lexicon/h7167)

Definition: rent (54x), tear (4x), rend away (2x), cut (1x), cut out (1x), surely (1x).

Part of speech: verb

Occurs 63 times in 60 verses

Hebrew: [qᵊrāʿîm](../h/h7168.md)

Greek: [diarrēssō](../g/g1284.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7167)

[Study Light](https://www.studylight.org/lexicons/hebrew/7167.html)

[Bible Hub](https://biblehub.com/str/hebrew/7167.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07167)

[Bible Bento](https://biblebento.com/dictionary/H7167.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7167/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7167.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7167)
