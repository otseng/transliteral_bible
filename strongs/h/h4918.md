# מְשֻׁלָּם

[Mᵊšullām](https://www.blueletterbible.org/lexicon/h4918)

Definition: Meshullam (25x).

Part of speech: proper masculine noun

Occurs 25 times in 25 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4918)

[Study Light](https://www.studylight.org/lexicons/hebrew/4918.html)

[Bible Hub](https://biblehub.com/str/hebrew/4918.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04918)

[Bible Bento](https://biblebento.com/dictionary/H4918.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4918/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4918.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4918)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/meshullam.html)

[Video Bible](https://www.videobible.com/bible-dictionary/meshullam)