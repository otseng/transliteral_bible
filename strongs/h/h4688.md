# מְצוֹלָה

[mᵊṣôlâ](https://www.blueletterbible.org/lexicon/h4688)

Definition: deep (5x), deeps (3x), depths (2x), bottom (1x).

Part of speech: feminine noun

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4688)

[Study Light](https://www.studylight.org/lexicons/hebrew/4688.html)

[Bible Hub](https://biblehub.com/str/hebrew/4688.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04688)

[Bible Bento](https://biblebento.com/dictionary/H4688.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4688/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4688.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4688)
