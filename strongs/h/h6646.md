# צְבִיָּה

[ṣᵊḇîyâ](https://www.blueletterbible.org/lexicon/h6646)

Definition: roe (2x), doe, gazelle

Part of speech: feminine noun

Occurs 2 times in 2 verses

Hebrew: [ṣᵊḇî](../h/h6643.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6646)

[Study Light](https://www.studylight.org/lexicons/hebrew/6646.html)

[Bible Hub](https://biblehub.com/str/hebrew/6646.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06646)

[Bible Bento](https://biblebento.com/dictionary/H6646.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6646/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6646.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6646)
