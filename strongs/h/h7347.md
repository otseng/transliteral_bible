# רֵחַיִם

[rēḥayim](https://www.blueletterbible.org/lexicon/h7347)

Definition: mill (2x), millstone (2x), nether (1x).

Part of speech: dual masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7347)

[Study Light](https://www.studylight.org/lexicons/hebrew/7347.html)

[Bible Hub](https://biblehub.com/str/hebrew/7347.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07347)

[Bible Bento](https://biblebento.com/dictionary/H7347.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7347/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7347.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7347)
