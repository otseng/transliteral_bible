# אֵיל פָּארָן

['êl pā'rān](https://www.blueletterbible.org/lexicon/h364)

Definition: Elparan (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0364)

[Study Light](https://www.studylight.org/lexicons/hebrew/364.html)

[Bible Hub](https://biblehub.com/str/hebrew/364.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0364)

[Bible Bento](https://biblebento.com/dictionary/H364.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/364/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/364.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h364)
