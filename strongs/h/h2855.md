# חֶתְלֹן

[Ḥeṯlōn](https://www.blueletterbible.org/lexicon/h2855)

Definition: Hethlon (2x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2855)

[Study Light](https://www.studylight.org/lexicons/hebrew/2855.html)

[Bible Hub](https://biblehub.com/str/hebrew/2855.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02855)

[Bible Bento](https://biblebento.com/dictionary/H2855.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2855/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2855.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2855)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hethlon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/hethlon)