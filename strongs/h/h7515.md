# רָפַשׂ

[rāp̄aś](https://www.blueletterbible.org/lexicon/h7515)

Definition: foul (2x), trouble (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7515)

[Study Light](https://www.studylight.org/lexicons/hebrew/7515.html)

[Bible Hub](https://biblehub.com/str/hebrew/7515.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07515)

[Bible Bento](https://biblebento.com/dictionary/H7515.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7515/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7515.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7515)
