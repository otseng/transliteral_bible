# אַשְׂרִיאֵל

['Aśrî'Ēl](https://www.blueletterbible.org/lexicon/h844)

Definition: Ashriel (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h844)

[Study Light](https://www.studylight.org/lexicons/hebrew/844.html)

[Bible Hub](https://biblehub.com/str/hebrew/844.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0844)

[Bible Bento](https://biblebento.com/dictionary/H844.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/844/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/844.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h844)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ashriel.html)