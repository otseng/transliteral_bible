# עָרָה

[ʿārâ](https://www.blueletterbible.org/lexicon/h6169)

Definition: paper reeds (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6169)

[Study Light](https://www.studylight.org/lexicons/hebrew/6169.html)

[Bible Hub](https://biblehub.com/str/hebrew/6169.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06169)

[Bible Bento](https://biblebento.com/dictionary/H6169.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6169/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6169.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6169)
