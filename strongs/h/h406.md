# אִכָּר

['ikār](https://www.blueletterbible.org/lexicon/h406)

Definition: husbandman (5x), plowman (2x).

Part of speech: masculine noun

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0406)

[Study Light](https://www.studylight.org/lexicons/hebrew/406.html)

[Bible Hub](https://biblehub.com/str/hebrew/406.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0406)

[Bible Bento](https://biblebento.com/dictionary/H406.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/406/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/406.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h406)
