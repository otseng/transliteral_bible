# מוּשִׁי

[Mûšî](https://www.blueletterbible.org/lexicon/h4187)

Definition: Mushi (8x).

Part of speech: proper masculine noun

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4187)

[Study Light](https://www.studylight.org/lexicons/hebrew/4187.html)

[Bible Hub](https://biblehub.com/str/hebrew/4187.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04187)

[Bible Bento](https://biblebento.com/dictionary/H4187.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4187/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4187.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4187)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/mushi.html)

[Video Bible](https://www.videobible.com/bible-dictionary/mushi)