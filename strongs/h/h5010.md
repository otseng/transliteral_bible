# נָאַר

[nā'ar](https://www.blueletterbible.org/lexicon/h5010)

Definition: made void (1x), abhorred (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5010)

[Study Light](https://www.studylight.org/lexicons/hebrew/5010.html)

[Bible Hub](https://biblehub.com/str/hebrew/5010.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05010)

[Bible Bento](https://biblebento.com/dictionary/H5010.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5010/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5010.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5010)
