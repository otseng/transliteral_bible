# בֵּית־הַכֶּרֶם

[Bêṯ-Hakerem](https://www.blueletterbible.org/lexicon/h1021)

Definition: Bethhaccerem (2x).

Part of speech: proper locative noun

Occurs 4 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1021)

[Study Light](https://www.studylight.org/lexicons/hebrew/1021.html)

[Bible Hub](https://biblehub.com/str/hebrew/1021.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01021)

[Bible Bento](https://biblebento.com/dictionary/H1021.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1021/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1021.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1021)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bethhaccerem.html)