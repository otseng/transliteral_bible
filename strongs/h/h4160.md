# מוּץ

[mûṣ](https://www.blueletterbible.org/lexicon/h4160)

Definition: extortioner (1x), oppressor

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: ['ammîṣ](../h/h533.md), ['amats](../h/h553.md), [māṣā'](../h/h4672.md), [maṣṣâ](../h/h4682.md), [māṣaṣ](../h/h4711.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4160)

[Study Light](https://www.studylight.org/lexicons/hebrew/4160.html)

[Bible Hub](https://biblehub.com/str/hebrew/4160.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04160)

[Bible Bento](https://biblebento.com/dictionary/H4160.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4160/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4160.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4160)
