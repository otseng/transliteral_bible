# חָרַק

[ḥāraq](https://www.blueletterbible.org/lexicon/h2786)

Definition: gnash (5x).

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2786)

[Study Light](https://www.studylight.org/lexicons/hebrew/2786.html)

[Bible Hub](https://biblehub.com/str/hebrew/2786.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02786)

[Bible Bento](https://biblebento.com/dictionary/H2786.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2786/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2786.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2786)
