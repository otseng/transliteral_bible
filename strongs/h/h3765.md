# כִּרְסֵם

[kirsēm](https://www.blueletterbible.org/lexicon/h3765)

Definition: waste (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3765)

[Study Light](https://www.studylight.org/lexicons/hebrew/3765.html)

[Bible Hub](https://biblehub.com/str/hebrew/3765.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03765)

[Bible Bento](https://biblebento.com/dictionary/H3765.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3765/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3765.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3765)
