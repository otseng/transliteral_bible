# פָּצַע

[pāṣaʿ](https://www.blueletterbible.org/lexicon/h6481)

Definition: wounded (3x), bruise, crush

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6481)

[Study Light](https://www.studylight.org/lexicons/hebrew/6481.html)

[Bible Hub](https://biblehub.com/str/hebrew/6481.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06481)

[Bible Bento](https://biblebento.com/dictionary/H6481.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6481/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6481.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6481)
