# מֵפִיץ

[mēp̄îṣ](https://www.blueletterbible.org/lexicon/h4650)

Definition: maul (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4650)

[Study Light](https://www.studylight.org/lexicons/hebrew/4650.html)

[Bible Hub](https://biblehub.com/str/hebrew/4650.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04650)

[Bible Bento](https://biblebento.com/dictionary/H4650.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4650/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4650.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4650)
