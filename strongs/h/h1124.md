# בְּנָא

[bᵊnā'](https://www.blueletterbible.org/lexicon/h1124)

Definition: build (17x), building (3x), builded (with H1934) (1x), make (1x).

Part of speech: verb

Occurs 22 times in 19 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1124)

[Study Light](https://www.studylight.org/lexicons/hebrew/1124.html)

[Bible Hub](https://biblehub.com/str/hebrew/1124.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01124)

[Bible Bento](https://biblebento.com/dictionary/H1124.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1124/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1124.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1124)
