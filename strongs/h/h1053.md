# בֵּית שֶׁמֶשׁ

[Bêṯ Šemeš](https://www.blueletterbible.org/lexicon/h1053)

Definition: Bethshemesh (21x).

Part of speech: proper locative noun

Occurs 42 times in 19 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1053)

[Study Light](https://www.studylight.org/lexicons/hebrew/1053.html)

[Bible Hub](https://biblehub.com/str/hebrew/1053.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01053)

[Bible Bento](https://biblebento.com/dictionary/H1053.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1053/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1053.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1053)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bethshemesh.html)