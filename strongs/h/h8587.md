# תַּעֲלֻמָה

[taʿălumâ](https://www.blueletterbible.org/lexicon/h8587)

Definition: secret (2x), thing that is hid (1x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8587)

[Study Light](https://www.studylight.org/lexicons/hebrew/8587.html)

[Bible Hub](https://biblehub.com/str/hebrew/8587.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08587)

[Bible Bento](https://biblebento.com/dictionary/H8587.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8587/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8587.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8587)
