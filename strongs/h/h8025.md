# שָׁלַף

[šālap̄](https://www.blueletterbible.org/lexicon/h8025)

Definition: draw (22x), pluck off (2x), grow up (1x).

Part of speech: verb

Occurs 25 times in 24 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8025)

[Study Light](https://www.studylight.org/lexicons/hebrew/8025.html)

[Bible Hub](https://biblehub.com/str/hebrew/8025.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08025)

[Bible Bento](https://biblebento.com/dictionary/H8025.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8025/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8025.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8025)
