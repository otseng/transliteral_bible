# אָכֵן

['āḵēn](https://www.blueletterbible.org/lexicon/h403)

Definition: surely (9x), but (3x), verily (2x), truly (2x), certainly (1x), nevertheless (1x).

Part of speech: adverb

Occurs 18 times in 17 verses

Hebrew: [kuwn](../h/h3559.md), [kēn](../h/h3651.md), [kēn](../h/h3653.md), [kᵊnēmā'](../h/h3660.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0403)

[Study Light](https://www.studylight.org/lexicons/hebrew/403.html)

[Bible Hub](https://biblehub.com/str/hebrew/403.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0403)

[Bible Bento](https://biblebento.com/dictionary/H403.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/403/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/403.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h403)
