# גֶּשֶׁם

[gešem](https://www.blueletterbible.org/lexicon/h1653)

Definition: rain (31x), shower (4x).

Part of speech: masculine noun

Occurs 35 times in 33 verses

Greek: [brochē](../g/g1028.md), [hyetos](../g/g5205.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1653)

[Study Light](https://www.studylight.org/lexicons/hebrew/1653.html)

[Bible Hub](https://biblehub.com/str/hebrew/1653.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01653)

[Bible Bento](https://biblebento.com/dictionary/H1653.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1653/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1653.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1653)
