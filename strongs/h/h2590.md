# חָנַט

[ḥānaṭ](https://www.blueletterbible.org/lexicon/h2590)

Definition: embalm (4x), put forth (1x).

Part of speech: masculine plural abstract noun, verb

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2590)

[Study Light](https://www.studylight.org/lexicons/hebrew/2590.html)

[Bible Hub](https://biblehub.com/str/hebrew/2590.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02590)

[Bible Bento](https://biblebento.com/dictionary/H2590.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2590/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2590.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2590)
