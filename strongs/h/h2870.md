# טָֽבְאֵל

[ṭāḇ'ēl](https://www.blueletterbible.org/lexicon/h2870)

Definition: Tabeel (1x), Tabeal (1x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2870)

[Study Light](https://www.studylight.org/lexicons/hebrew/2870.html)

[Bible Hub](https://biblehub.com/str/hebrew/2870.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02870)

[Bible Bento](https://biblebento.com/dictionary/H2870.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2870/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2870.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2870)

[Video Bible](https://www.videobible.com/bible-dictionary/tabeel)