# גֶּבֶא

[geḇe'](https://www.blueletterbible.org/lexicon/h1360)

Definition: pit (2x), marishes (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1360)

[Study Light](https://www.studylight.org/lexicons/hebrew/1360.html)

[Bible Hub](https://biblehub.com/str/hebrew/1360.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01360)

[Bible Bento](https://biblebento.com/dictionary/H1360.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1360/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1360.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1360)
