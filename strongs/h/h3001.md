# יָבֵשׁ

[yāḇēš](https://www.blueletterbible.org/lexicon/h3001)

Definition: dry up (27x), withered (22x), confounded (9x), ashamed (7x), dry (7x), wither away (2x), clean (1x), shamed (1x), shamefully (1x), utterly (1x).

Part of speech: verb

Occurs 73 times in 62 verses

Greek: [xērainō](../g/g3583.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3001)

[Study Light](https://www.studylight.org/lexicons/hebrew/3001.html)

[Bible Hub](https://biblehub.com/str/hebrew/3001.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03001)

[Bible Bento](https://biblebento.com/dictionary/H3001.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3001/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3001.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3001)
