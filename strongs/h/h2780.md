# חָרֵף

[Ḥārēp̄](https://www.blueletterbible.org/lexicon/h2780)

Definition: Hareph (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2780)

[Study Light](https://www.studylight.org/lexicons/hebrew/2780.html)

[Bible Hub](https://biblehub.com/str/hebrew/2780.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02780)

[Bible Bento](https://biblebento.com/dictionary/H2780.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2780/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2780.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2780)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hareph.html)

[Video Bible](https://www.videobible.com/bible-dictionary/hareph)