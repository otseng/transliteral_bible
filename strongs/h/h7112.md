# קָצַץ

[qāṣaṣ](https://www.blueletterbible.org/lexicon/h7112)

Definition: cut off (6x), utmost (3x), cut in pieces (2x), cut (1x), cut asunder (1x), cut in sunder (1x).

Part of speech: verb

Occurs 14 times in 14 verses

Hebrew: [qēṣ](../h/h7093.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7112)

[Study Light](https://www.studylight.org/lexicons/hebrew/7112.html)

[Bible Hub](https://biblehub.com/str/hebrew/7112.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07112)

[Bible Bento](https://biblebento.com/dictionary/H7112.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7112/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7112.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7112)
