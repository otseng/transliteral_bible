# תָּעָה

[tāʿâ](https://www.blueletterbible.org/lexicon/h8582)

Definition: err (17x), astray (12x), wander (10x), seduced (3x), stagger (2x), out of the way (2x), away (1x), deceived (1x), miscellaneous (2x).

Part of speech: verb

Occurs 51 times in 45 verses

Greek: [planaō](../g/g4105.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8582)

[Study Light](https://www.studylight.org/lexicons/hebrew/8582.html)

[Bible Hub](https://biblehub.com/str/hebrew/8582.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08582)

[Bible Bento](https://biblebento.com/dictionary/H8582.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8582/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8582.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8582)
