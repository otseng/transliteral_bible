# מַהֲרַי

[Mahăray](https://www.blueletterbible.org/lexicon/h4121)

Definition: Maharai (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4121)

[Study Light](https://www.studylight.org/lexicons/hebrew/4121.html)

[Bible Hub](https://biblehub.com/str/hebrew/4121.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04121)

[Bible Bento](https://biblebento.com/dictionary/H4121.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4121/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4121.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4121)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/maharai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/maharai)