# אֲבּישָׁלוֹם

['Ăbyšālôm](https://www.blueletterbible.org/lexicon/h53)

Definition: Absalom (109x), Abishalom (2x), "father is peace", third son of David, killer of first-born son Amnon, leader of revolt against his father

Part of speech: proper masculine noun

Occurs 111 times in 92 verses

Hebrew: ['ab](../h/h1.md), [shalowm](../h/h7965.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h53)

[Study Light](https://www.studylight.org/lexicons/hebrew/53.html)

[Bible Hub](https://biblehub.com/str/hebrew/53.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=053)

[Bible Bento](https://biblebento.com/dictionary/H53.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/53/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/53.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h53)

[Wikipedia](https://en.wikipedia.org/wiki/Absalom)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/absalom.html)

[Video Bible](https://www.videobible.com/bible-dictionary/absalom)