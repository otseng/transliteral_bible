# פָּרַץ

[pāraṣ](https://www.blueletterbible.org/lexicon/h6555)

Definition: break down (11x), break forth (5x), increase (5x), break (4x), abroad (3x), breach (2x), break in (2x), made (2x), break out (2x), pressed (2x), break up (2x), break away (1x), breaker (1x), compelled (1x), miscellaneous (6x).

Part of speech: verb

Occurs 49 times in 48 verses

Greek: [apollymi](../g/g622.md), [ekpetannymi](../g/g1600.md), [thrauō](../g/g2352.md), [kataballō](../g/g2598.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6555)

[Study Light](https://www.studylight.org/lexicons/hebrew/6555.html)

[Bible Hub](https://biblehub.com/str/hebrew/6555.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06555)

[Bible Bento](https://biblebento.com/dictionary/H6555.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6555/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6555.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6555)
