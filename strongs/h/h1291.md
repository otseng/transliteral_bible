# בֶּרֶךְ

[bereḵ](https://www.blueletterbible.org/lexicon/h1291)

Definition: knee (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Hebrew: [barak](../h/h1288.md), [bᵊraḵ](../h/h1289.md), [bereḵ](../h/h1290.md), [bĕrakah](../h/h1293.md), ['aḇrēḵ](../h/h86.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1291)

[Study Light](https://www.studylight.org/lexicons/hebrew/1291.html)

[Bible Hub](https://biblehub.com/str/hebrew/1291.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01291)

[Bible Bento](https://biblebento.com/dictionary/H1291.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1291/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1291.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1291)
