# פַּרְסִי

[parsî](https://www.blueletterbible.org/lexicon/h6542)

Definition: Persian (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6542)

[Study Light](https://www.studylight.org/lexicons/hebrew/6542.html)

[Bible Hub](https://biblehub.com/str/hebrew/6542.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06542)

[Bible Bento](https://biblebento.com/dictionary/H6542.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6542/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6542.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6542)
