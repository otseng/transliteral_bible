# מְרָט

[mᵊrāṭ](https://www.blueletterbible.org/lexicon/h4804)

Definition: plucked (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4804)

[Study Light](https://www.studylight.org/lexicons/hebrew/4804.html)

[Bible Hub](https://biblehub.com/str/hebrew/4804.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04804)

[Bible Bento](https://biblebento.com/dictionary/H4804.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4804/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4804.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4804)
