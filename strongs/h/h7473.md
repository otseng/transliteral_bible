# רֹעֶה

[rōʿê](https://www.blueletterbible.org/lexicon/h7473)

Definition: shepherd (2x).

Part of speech: verb

Occurs 11 times in 8 verses

Hebrew: [ra`ah](../h/h7462.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7473)

[Study Light](https://www.studylight.org/lexicons/hebrew/7473.html)

[Bible Hub](https://biblehub.com/str/hebrew/7473.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07473)

[Bible Bento](https://biblebento.com/dictionary/H7473.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7473/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7473.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7473)
