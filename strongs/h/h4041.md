# מְגַמָּה

[mᵊḡammâ](https://www.blueletterbible.org/lexicon/h4041)

Definition: sup up (1x), accumulation, assembling

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4041)

[Study Light](https://www.studylight.org/lexicons/hebrew/4041.html)

[Bible Hub](https://biblehub.com/str/hebrew/4041.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04041)

[Bible Bento](https://biblebento.com/dictionary/H4041.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4041/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4041.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4041)
