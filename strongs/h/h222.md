# אוּרִיאֵל

['Ûrî'Ēl](https://www.blueletterbible.org/lexicon/h222)

Definition: Uriel (4x).

Part of speech: proper masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h222)

[Study Light](https://www.studylight.org/lexicons/hebrew/222.html)

[Bible Hub](https://biblehub.com/str/hebrew/222.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0222)

[Bible Bento](https://biblebento.com/dictionary/H222.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/222/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/222.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h222)

[Video Bible](https://www.videobible.com/bible-dictionary/uriel)