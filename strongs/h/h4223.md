# מְחָא

[mᵊḥā'](https://www.blueletterbible.org/lexicon/h4223)

Definition: smote (2x), can (1x), hanged (1x).

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4223)

[Study Light](https://www.studylight.org/lexicons/hebrew/4223.html)

[Bible Hub](https://biblehub.com/str/hebrew/4223.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04223)

[Bible Bento](https://biblebento.com/dictionary/H4223.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4223/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4223.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4223)
