# רִיפוֹת

[rîp̄ôṯ](https://www.blueletterbible.org/lexicon/h7383)

Definition: ground corn (1x), wheat (1x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7383)

[Study Light](https://www.studylight.org/lexicons/hebrew/7383.html)

[Bible Hub](https://biblehub.com/str/hebrew/7383.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07383)

[Bible Bento](https://biblebento.com/dictionary/H7383.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7383/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7383.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7383)
