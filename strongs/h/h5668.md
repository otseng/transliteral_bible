# עֲבוּר

[ʿăḇûr](https://www.blueletterbible.org/lexicon/h5668)

Definition: (49x), sake, that, because of, to, to the intent that.

Part of speech: conjunction, preposition

Occurs 49 times in 47 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5668)

[Study Light](https://www.studylight.org/lexicons/hebrew/5668.html)

[Bible Hub](https://biblehub.com/str/hebrew/5668.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05668)

[Bible Bento](https://biblebento.com/dictionary/H5668.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5668/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5668.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5668)
