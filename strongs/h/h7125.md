# קָרָא

[qārā'](https://www.blueletterbible.org/lexicon/h7125)

Definition: meet (76x), against (40x), come (2x), help (1x), seek (1x), way (1x).

Part of speech: masculine noun

Occurs 100 times in 98 verses

Hebrew: [miqrā'](../h/h4744.md), [qara'](../h/h7121.md), [qārā'](../h/h7122.md), [qᵊrā'](../h/h7123.md), [qᵊrî'â](../h/h7150.md)

Pictoral: gather men

Synonyms: [seek](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Seek)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7125)

[Study Light](https://www.studylight.org/lexicons/hebrew/7125.html)

[Bible Hub](https://biblehub.com/str/hebrew/7125.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07125)

[Bible Bento](https://biblebento.com/dictionary/H7125.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7125/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7125.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7125)
