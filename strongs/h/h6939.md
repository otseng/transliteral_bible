# קִדְרוֹן

[Qiḏrôn](https://www.blueletterbible.org/lexicon/h6939)

Definition: Kidron (11x).

Part of speech: proper noun with reference to a stream

Occurs 11 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6939)

[Study Light](https://www.studylight.org/lexicons/hebrew/6939.html)

[Bible Hub](https://biblehub.com/str/hebrew/6939.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06939)

[Bible Bento](https://biblebento.com/dictionary/H6939.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6939/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6939.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6939)
