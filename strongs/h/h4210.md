# מִזְמוֹר

[mizmôr](https://www.blueletterbible.org/lexicon/h4210)

Definition: psalm (57x).

Part of speech: masculine noun

Occurs 57 times in 57 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4210)

[Study Light](https://www.studylight.org/lexicons/hebrew/4210.html)

[Bible Hub](https://biblehub.com/str/hebrew/4210.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04210)

[Bible Bento](https://biblebento.com/dictionary/H4210.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4210/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4210.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4210)
