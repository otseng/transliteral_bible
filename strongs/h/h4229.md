# מָחָה

[māḥâ](https://www.blueletterbible.org/lexicon/h4229)

Definition: (blot, put, etc)...out (17x), destroy (6x), wipe (4x), blot (3x), wipe away (2x), abolished (1x), marrow (1x), reach (1x), utterly (1x).

Part of speech: verb

Occurs 36 times in 32 verses

Greek: [aphaireō](../g/g851.md)

Synonyms: [destroy](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Destroy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4229)

[Study Light](https://www.studylight.org/lexicons/hebrew/4229.html)

[Bible Hub](https://biblehub.com/str/hebrew/4229.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04229)

[Bible Bento](https://biblebento.com/dictionary/H4229.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4229/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4229.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4229)
