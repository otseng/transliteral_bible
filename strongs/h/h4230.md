# מְחוּגָה

[mᵊḥûḡâ](https://www.blueletterbible.org/lexicon/h4230)

Definition: compass (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4230)

[Study Light](https://www.studylight.org/lexicons/hebrew/4230.html)

[Bible Hub](https://biblehub.com/str/hebrew/4230.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04230)

[Bible Bento](https://biblebento.com/dictionary/H4230.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4230/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4230.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4230)
