# מְסוּכָה

[mᵊsûḵâ](https://www.blueletterbible.org/lexicon/h4534)

Definition: thorn hedge (1x).

Part of speech: ?

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4534)

[Study Light](https://www.studylight.org/lexicons/hebrew/4534.html)

[Bible Hub](https://biblehub.com/str/hebrew/4534.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04534)

[Bible Bento](https://biblebento.com/dictionary/H4534.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4534/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4534.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4534)
