# רָוַח

[rāvaḥ](https://www.blueletterbible.org/lexicon/h7304)

Definition: refreshed (2x), large (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7304)

[Study Light](https://www.studylight.org/lexicons/hebrew/7304.html)

[Bible Hub](https://biblehub.com/str/hebrew/7304.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07304)

[Bible Bento](https://biblebento.com/dictionary/H7304.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7304/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7304.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7304)
