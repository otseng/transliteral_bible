# מְצֵלֶת

[mᵊṣēleṯ](https://www.blueletterbible.org/lexicon/h4700)

Definition: cymbal (13x).

Part of speech: dual feminine noun

Occurs 13 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4700)

[Study Light](https://www.studylight.org/lexicons/hebrew/4700.html)

[Bible Hub](https://biblehub.com/str/hebrew/4700.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04700)

[Bible Bento](https://biblebento.com/dictionary/H4700.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4700/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4700.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4700)
