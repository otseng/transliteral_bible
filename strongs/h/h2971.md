# יָאִיר

[Yā'Îr](https://www.blueletterbible.org/lexicon/h2971)

Definition: Jair (9x).

Part of speech: proper masculine noun

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2971)

[Study Light](https://www.studylight.org/lexicons/hebrew/2971.html)

[Bible Hub](https://biblehub.com/str/hebrew/2971.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02971)

[Bible Bento](https://biblebento.com/dictionary/H2971.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2971/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2971.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2971)
