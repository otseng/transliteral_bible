# כִּסְלֵו

[Kislēv](https://www.blueletterbible.org/lexicon/h3691)

Definition: Chisleu (2x).

Part of speech: proper masculine noun

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3691)

[Study Light](https://www.studylight.org/lexicons/hebrew/3691.html)

[Bible Hub](https://biblehub.com/str/hebrew/3691.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03691)

[Bible Bento](https://biblebento.com/dictionary/H3691.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3691/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3691.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3691)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/chisleu.html)