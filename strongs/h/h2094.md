# זָהַר

[zāhar](https://www.blueletterbible.org/lexicon/h2094)

Definition: warn (18x), admonish (2x), teach (1x), shine (1x).

Part of speech: verb

Occurs 23 times in 19 verses

Hebrew: [zᵊhar](../h/h2095.md)

Greek: [diastellō](../g/g1291.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2094)

[Study Light](https://www.studylight.org/lexicons/hebrew/2094.html)

[Bible Hub](https://biblehub.com/str/hebrew/2094.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02094)

[Bible Bento](https://biblebento.com/dictionary/H2094.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2094/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2094.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2094)
