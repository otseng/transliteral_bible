# בִּרְשַׁע

[biršaʿ](https://www.blueletterbible.org/lexicon/h1306)

Definition: Birsha (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1306)

[Study Light](https://www.studylight.org/lexicons/hebrew/1306.html)

[Bible Hub](https://biblehub.com/str/hebrew/1306.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01306)

[Bible Bento](https://biblebento.com/dictionary/H1306.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1306/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1306.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1306)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/birsha.html)

[Video Bible](https://www.videobible.com/bible-dictionary/birsha)