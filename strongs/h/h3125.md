# יְוָנִי

[yᵊvānî](https://www.blueletterbible.org/lexicon/h3125)

Definition: Grecian (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3125)

[Study Light](https://www.studylight.org/lexicons/hebrew/3125.html)

[Bible Hub](https://biblehub.com/str/hebrew/3125.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03125)

[Bible Bento](https://biblebento.com/dictionary/H3125.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3125/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3125.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3125)
