# נַפְתָּלִי

[Nap̄tālî](https://www.blueletterbible.org/lexicon/h5321)

Definition: Naphtali (50x).

Part of speech: proper locative noun, proper masculine noun

Occurs 51 times in 47 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5321)

[Study Light](https://www.studylight.org/lexicons/hebrew/5321.html)

[Bible Hub](https://biblehub.com/str/hebrew/5321.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05321)

[Bible Bento](https://biblebento.com/dictionary/H5321.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5321/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5321.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5321)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/naphtali.html)

[Video Bible](https://www.videobible.com/bible-dictionary/naphtali)