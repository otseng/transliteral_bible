# סֵפֶל

[sēp̄el](https://www.blueletterbible.org/lexicon/h5602)

Definition: bowl (1x), dish (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5602)

[Study Light](https://www.studylight.org/lexicons/hebrew/5602.html)

[Bible Hub](https://biblehub.com/str/hebrew/5602.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05602)

[Bible Bento](https://biblebento.com/dictionary/H5602.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5602/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5602.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5602)
