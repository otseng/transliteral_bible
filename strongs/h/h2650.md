# חֻפִּים

[Ḥupîm](https://www.blueletterbible.org/lexicon/h2650)

Definition: Huppim (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2650)

[Study Light](https://www.studylight.org/lexicons/hebrew/2650.html)

[Bible Hub](https://biblehub.com/str/hebrew/2650.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02650)

[Bible Bento](https://biblebento.com/dictionary/H2650.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2650/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2650.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2650)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/huppim.html)

[Video Bible](https://www.videobible.com/bible-dictionary/huppim)