# יַהֲלֹם

[yahălōm](https://www.blueletterbible.org/lexicon/h3095)

Definition: diamond (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3095)

[Study Light](https://www.studylight.org/lexicons/hebrew/3095.html)

[Bible Hub](https://biblehub.com/str/hebrew/3095.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03095)

[Bible Bento](https://biblebento.com/dictionary/H3095.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3095/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3095.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3095)
