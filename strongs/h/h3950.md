# לָקַט

[lāqaṭ](https://www.blueletterbible.org/lexicon/h3950)

Definition: gather (23x), glean (12x), gather up (2x).

Part of speech: verb

Occurs 37 times in 34 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3950)

[Study Light](https://www.studylight.org/lexicons/hebrew/3950.html)

[Bible Hub](https://biblehub.com/str/hebrew/3950.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03950)

[Bible Bento](https://biblebento.com/dictionary/H3950.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3950/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3950.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3950)
