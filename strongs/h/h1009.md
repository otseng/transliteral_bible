# בֵּית אַרְבְּאֵל

[Bêṯ 'Arbᵊ'Ēl](https://www.blueletterbible.org/lexicon/h1009)

Definition: Betharbel (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1009)

[Study Light](https://www.studylight.org/lexicons/hebrew/1009.html)

[Bible Hub](https://biblehub.com/str/hebrew/1009.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01009)

[Bible Bento](https://biblebento.com/dictionary/H1009.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1009/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1009.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1009)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/betharbel.html)