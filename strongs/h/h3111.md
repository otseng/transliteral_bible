# יוֹיָדָע

[Yôyāḏāʿ](https://www.blueletterbible.org/lexicon/h3111)

Definition: Joiada (4x), Jehoiada (1x).

Part of speech: proper masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3111)

[Study Light](https://www.studylight.org/lexicons/hebrew/3111.html)

[Bible Hub](https://biblehub.com/str/hebrew/3111.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03111)

[Bible Bento](https://biblebento.com/dictionary/H3111.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3111/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3111.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3111)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/joiada.html)