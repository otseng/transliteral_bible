# קְרִי

[qᵊrî](https://www.blueletterbible.org/lexicon/h7147)

Definition: contrary (7x).

Part of speech: masculine noun

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7147)

[Study Light](https://www.studylight.org/lexicons/hebrew/7147.html)

[Bible Hub](https://biblehub.com/str/hebrew/7147.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07147)

[Bible Bento](https://biblebento.com/dictionary/H7147.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7147/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7147.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7147)
