# מַשְׂכֹּרֶת

[maśkōreṯ](https://www.blueletterbible.org/lexicon/h4909)

Definition: wages (3x), reward (1x).

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4909)

[Study Light](https://www.studylight.org/lexicons/hebrew/4909.html)

[Bible Hub](https://biblehub.com/str/hebrew/4909.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04909)

[Bible Bento](https://biblebento.com/dictionary/H4909.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4909/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4909.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4909)
