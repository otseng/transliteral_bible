# יְהוֹצָדָק

[Yᵊhôṣāḏāq](https://www.blueletterbible.org/lexicon/h3087)

Definition: Josedech (6x), Jehozadak (2x).

Part of speech: proper masculine noun

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3087)

[Study Light](https://www.studylight.org/lexicons/hebrew/3087.html)

[Bible Hub](https://biblehub.com/str/hebrew/3087.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03087)

[Bible Bento](https://biblebento.com/dictionary/H3087.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3087/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3087.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3087)
