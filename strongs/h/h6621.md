# פֶּתַע

[peṯaʿ](https://www.blueletterbible.org/lexicon/h6621)

Definition: suddenly (4x), instant (2x), very (1x).

Part of speech: substantive

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6621)

[Study Light](https://www.studylight.org/lexicons/hebrew/6621.html)

[Bible Hub](https://biblehub.com/str/hebrew/6621.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06621)

[Bible Bento](https://biblebento.com/dictionary/H6621.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6621/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6621.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6621)
