# רָוֶה

[rāvê](https://www.blueletterbible.org/lexicon/h7302)

Definition: watered (2x), drunkenness (1x).

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7302)

[Study Light](https://www.studylight.org/lexicons/hebrew/7302.html)

[Bible Hub](https://biblehub.com/str/hebrew/7302.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07302)

[Bible Bento](https://biblebento.com/dictionary/H7302.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7302/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7302.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7302)
