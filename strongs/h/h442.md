# אָלוּשׁ

['Ālûš](https://www.blueletterbible.org/lexicon/h442)

Definition: Alush (2x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h442)

[Study Light](https://www.studylight.org/lexicons/hebrew/442.html)

[Bible Hub](https://biblehub.com/str/hebrew/442.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0442)

[Bible Bento](https://biblebento.com/dictionary/H442.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/442/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/442.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h442)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/alush.html)

[Video Bible](https://www.videobible.com/bible-dictionary/alush)