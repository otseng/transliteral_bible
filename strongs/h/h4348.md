# מִכְוָה

[miḵvâ](https://www.blueletterbible.org/lexicon/h4348)

Definition: burning (4x), burneth (1x).

Part of speech: feminine noun

Occurs 5 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4348)

[Study Light](https://www.studylight.org/lexicons/hebrew/4348.html)

[Bible Hub](https://biblehub.com/str/hebrew/4348.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04348)

[Bible Bento](https://biblebento.com/dictionary/H4348.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4348/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4348.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4348)
