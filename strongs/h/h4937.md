# מַשְׁעֵן

[mašʿēn](https://www.blueletterbible.org/lexicon/h4937)

Definition: stay (5x), support, staff, protector or sustenance

Part of speech: masculine noun

Occurs 5 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4937)

[Study Light](https://www.studylight.org/lexicons/hebrew/4937.html)

[Bible Hub](https://biblehub.com/str/hebrew/4937.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04937)

[Bible Bento](https://biblebento.com/dictionary/H4937.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4937/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4937.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4937)
