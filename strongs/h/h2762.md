# חֲרַכִּים

[ḥărakîm](https://www.blueletterbible.org/lexicon/h2762)

Definition: lattice (1x).

Part of speech: masculine plural noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2762)

[Study Light](https://www.studylight.org/lexicons/hebrew/2762.html)

[Bible Hub](https://biblehub.com/str/hebrew/2762.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02762)

[Bible Bento](https://biblebento.com/dictionary/H2762.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2762/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2762.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2762)
