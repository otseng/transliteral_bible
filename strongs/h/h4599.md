# מַעְיָן

[maʿyān](https://www.blueletterbible.org/lexicon/h4599)

Definition: fountain (16x), well (5x), springs (2x).

Part of speech: masculine noun

Occurs 23 times in 23 verses

Hebrew: ['ayin](../h/h5869.md)

Greek: [pēgē](../g/g4077.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4599)

[Study Light](https://www.studylight.org/lexicons/hebrew/4599.html)

[Bible Hub](https://biblehub.com/str/hebrew/4599.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04599)

[Bible Bento](https://biblebento.com/dictionary/H4599.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4599/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4599.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4599)
