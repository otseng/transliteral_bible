# אַף

['aph](https://www.blueletterbible.org/lexicon/h637)

Definition: (17x), also, even, yet, moreover, yea, with, low, therefore, much

Part of speech: adverb, conjunction

Occurs 17 times in 17 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0637)

[Study Light](https://www.studylight.org/lexicons/hebrew/637.html)

[Bible Hub](https://biblehub.com/str/hebrew/637.htm)

[Morfix](https://www.morfix.co.il/en/%D7%90%D6%B7%D7%A3)

[NET Bible](http://classic.net.bible.org/strong.php?id=0637)

[Bible Bento](https://biblebento.com/dictionary/H0637.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0637/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0637.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h637)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%90%D7%A3)
