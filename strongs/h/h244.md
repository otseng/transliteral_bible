# אָזְנִי

['āznî](https://www.blueletterbible.org/lexicon/h244)

Definition: Ozni (1x), Oznites (1x).

Part of speech: proper masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h244)

[Study Light](https://www.studylight.org/lexicons/hebrew/244.html)

[Bible Hub](https://biblehub.com/str/hebrew/244.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0244)

[Bible Bento](https://biblebento.com/dictionary/H244.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/244/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/244.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h244)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/O/ozni.html)