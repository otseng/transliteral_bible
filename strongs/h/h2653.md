# חָפַף

[ḥāp̄ap̄](https://www.blueletterbible.org/lexicon/h2653)

Definition: cover (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Synonyms: [cover](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Cover)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2653)

[Study Light](https://www.studylight.org/lexicons/hebrew/2653.html)

[Bible Hub](https://biblehub.com/str/hebrew/2653.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02653)

[Bible Bento](https://biblebento.com/dictionary/H2653.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2653/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2653.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2653)
