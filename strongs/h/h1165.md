# בְּעִִיר

[bᵊʿîr](https://www.blueletterbible.org/lexicon/h1165)

Definition: beast (4x), cattle (2x).

Part of speech: collective masculine noun

Occurs 7 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1165)

[Study Light](https://www.studylight.org/lexicons/hebrew/1165.html)

[Bible Hub](https://biblehub.com/str/hebrew/1165.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01165)

[Bible Bento](https://biblebento.com/dictionary/H1165.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1165/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1165.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1165)
