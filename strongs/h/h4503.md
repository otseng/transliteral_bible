# מִנְחָה

[minchah](https://www.blueletterbible.org/lexicon/h4503)

Definition: offering (164x), present (28x), gift (7x), oblation (6x), sacrifice (5x), meat (1x), trubute, 

Part of speech: feminine noun

Occurs 211 times in 194 verses

Hebrew: [minḥâ](../h/h4504.md)

Greek: [dōron](../g/g1435.md), [thysia](../g/g2378.md)

Pictoral: continue outside

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4503)

[Study Light](https://www.studylight.org/lexicons/hebrew/4503.html)

[Bible Hub](https://biblehub.com/str/hebrew/4503.htm)

[Morfix](https://www.morfix.co.il/en/%D7%9E%D6%B4%D7%A0%D6%B0%D7%97%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=04503)

[Bible Bento](https://biblebento.com/dictionary/H4503.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4503/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4503.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4503)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%9E%D7%A0%D7%97%D7%94)
