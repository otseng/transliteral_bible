# פַּלְדָּה

[paldâ](https://www.blueletterbible.org/lexicon/h6393)

Definition: torches (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6393)

[Study Light](https://www.studylight.org/lexicons/hebrew/6393.html)

[Bible Hub](https://biblehub.com/str/hebrew/6393.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06393)

[Bible Bento](https://biblebento.com/dictionary/H6393.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6393/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6393.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6393)
