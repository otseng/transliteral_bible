# יָקַץ

[yāqaṣ](https://www.blueletterbible.org/lexicon/h3364)

Definition: awake (11x).

Part of speech: verb

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3364)

[Study Light](https://www.studylight.org/lexicons/hebrew/3364.html)

[Bible Hub](https://biblehub.com/str/hebrew/3364.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03364)

[Bible Bento](https://biblebento.com/dictionary/H3364.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3364/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3364.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3364)
