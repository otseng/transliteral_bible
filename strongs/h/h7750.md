# שׂוּט

[śûṭ](https://www.blueletterbible.org/lexicon/h7750)

Definition: turn aside (2x).

Part of speech: verb

Occurs 2 times in 2 verses

Hebrew: [śēṭ](../h/h7846.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7750)

[Study Light](https://www.studylight.org/lexicons/hebrew/7750.html)

[Bible Hub](https://biblehub.com/str/hebrew/7750.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07750)

[Bible Bento](https://biblebento.com/dictionary/H7750.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7750/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7750.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7750)
