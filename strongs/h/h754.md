# אַרְכָּה

['arkâ](https://www.blueletterbible.org/lexicon/h754)

Definition: prolonged (1x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h754)

[Study Light](https://www.studylight.org/lexicons/hebrew/754.html)

[Bible Hub](https://biblehub.com/str/hebrew/754.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0754)

[Bible Bento](https://biblebento.com/dictionary/H754.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/754/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/754.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h754)
