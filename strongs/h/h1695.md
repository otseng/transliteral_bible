# דָּבֵק

[dāḇēq](https://www.blueletterbible.org/lexicon/h1695)

Definition: cleave (1x), join (1x), stick (1x).

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1695)

[Study Light](https://www.studylight.org/lexicons/hebrew/1695.html)

[Bible Hub](https://biblehub.com/str/hebrew/1695.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01695)

[Bible Bento](https://biblebento.com/dictionary/H1695.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1695/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1695.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1695)
