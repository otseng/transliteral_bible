# אָתוֹן

['āṯôn](https://www.blueletterbible.org/lexicon/h860)

Definition: ass (34x).

Part of speech: feminine noun

Occurs 34 times in 28 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0860)

[Study Light](https://www.studylight.org/lexicons/hebrew/860.html)

[Bible Hub](https://biblehub.com/str/hebrew/860.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0860)

[Bible Bento](https://biblebento.com/dictionary/H860.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/860/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/860.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h860)
