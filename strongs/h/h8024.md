# שֵׁלָנִי

[šēlānî](https://www.blueletterbible.org/lexicon/h8024)

Definition: Shelanites (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8024)

[Study Light](https://www.studylight.org/lexicons/hebrew/8024.html)

[Bible Hub](https://biblehub.com/str/hebrew/8024.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08024)

[Bible Bento](https://biblebento.com/dictionary/H8024.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8024/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8024.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8024)
