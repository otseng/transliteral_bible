# יִשְׂרְאֵלִית

[yiśrᵊ'ēlîṯ](https://www.blueletterbible.org/lexicon/h3482)

Definition: Israelitess (3x).

Part of speech: feminine adjective

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3482)

[Study Light](https://www.studylight.org/lexicons/hebrew/3482.html)

[Bible Hub](https://biblehub.com/str/hebrew/3482.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03482)

[Bible Bento](https://biblebento.com/dictionary/H3482.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3482/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3482.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3482)
