# דָּנִי

[dānî](https://www.blueletterbible.org/lexicon/h1839)

Definition: Danite (4x), Dan (1x).

Part of speech: adjective

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1839)

[Study Light](https://www.studylight.org/lexicons/hebrew/1839.html)

[Bible Hub](https://biblehub.com/str/hebrew/1839.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01839)

[Bible Bento](https://biblebento.com/dictionary/H1839.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1839/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1839.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1839)
