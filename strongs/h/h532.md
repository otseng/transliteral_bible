# אָמִי

['Āmî](https://www.blueletterbible.org/lexicon/h532)

Definition: Ami (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h532)

[Study Light](https://www.studylight.org/lexicons/hebrew/532.html)

[Bible Hub](https://biblehub.com/str/hebrew/532.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0532)

[Bible Bento](https://biblebento.com/dictionary/H532.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/532/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/532.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h532)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ami.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ami)