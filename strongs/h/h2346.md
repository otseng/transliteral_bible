# חוֹמָה

[ḥômâ](https://www.blueletterbible.org/lexicon/h2346)

Definition: wall (131x), walled (2x), town wall, the wall around a city, fortification

Part of speech: feminine noun

Occurs 133 times in 123 verses

Greek: [teichos](../g/g5038.md)

Edenics: home

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2346)

[Study Light](https://www.studylight.org/lexicons/hebrew/2346.html)

[Bible Hub](https://biblehub.com/str/hebrew/2346.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02346)

[Bible Bento](https://biblebento.com/dictionary/H2346.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2346/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2346.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2346)
