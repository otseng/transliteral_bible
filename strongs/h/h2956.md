# טָרַד

[ṭāraḏ](https://www.blueletterbible.org/lexicon/h2956)

Definition: continual (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2956)

[Study Light](https://www.studylight.org/lexicons/hebrew/2956.html)

[Bible Hub](https://biblehub.com/str/hebrew/2956.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02956)

[Bible Bento](https://biblebento.com/dictionary/H2956.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2956/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2956.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2956)
