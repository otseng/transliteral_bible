# מַלְכָּם

[Malkām](https://www.blueletterbible.org/lexicon/h4445)

Definition: Milcom (3x), Malcham (1x).

Part of speech: proper masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4445)

[Study Light](https://www.studylight.org/lexicons/hebrew/4445.html)

[Bible Hub](https://biblehub.com/str/hebrew/4445.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04445)

[Bible Bento](https://biblebento.com/dictionary/H4445.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4445/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4445.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4445)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/milcom.html)