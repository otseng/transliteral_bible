# כּוֹרֶשׁ

[Kôreš](https://www.blueletterbible.org/lexicon/h3566)

Definition: Cyrus (15x), "posses thou the furnace", the king of Persia and conqueror of Babylon; first ruler of Persia to make a decree allowing the Israelite exiles to return to Jerusalem

Part of speech: proper masculine noun

Occurs 15 times in 13 verses

Names: [Kôreš](../h/h3567.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3566)

[Study Light](https://www.studylight.org/lexicons/hebrew/3566.html)

[Bible Hub](https://biblehub.com/str/hebrew/3566.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03566)

[Bible Bento](https://biblebento.com/dictionary/H3566.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3566/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3566.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3566)

[Wikipedia](https://en.wikipedia.org/wiki/Cyrus_the_Great)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/cyrus.html)

[Video Bible](https://www.videobible.com/bible-dictionary/cyrus)