# אוּזַי

['Ûzay](https://www.blueletterbible.org/lexicon/h186)

Definition: Uzai (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h186)

[Study Light](https://www.studylight.org/lexicons/hebrew/186.html)

[Bible Hub](https://biblehub.com/str/hebrew/186.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0186)

[Bible Bento](https://biblebento.com/dictionary/H186.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/186/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/186.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h186)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/U/uzai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/uzai)