# חָרָן

[Ḥārān](https://www.blueletterbible.org/lexicon/h2771)

Definition: Haran (12x), "mountaineer", the city to which Abraham migrated when he left Ur of the Chaldees and where he stayed until his father died before leaving for the promised land; located in Mesopotamia in Padan-aram at the foot of Mount Masius between the Khabour and the Euphrates 

Part of speech: proper locative noun, proper masculine noun

Occurs 12 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2771)

[Study Light](https://www.studylight.org/lexicons/hebrew/2771.html)

[Bible Hub](https://biblehub.com/str/hebrew/2771.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02771)

[Bible Bento](https://biblebento.com/dictionary/H2771.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2771/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2771.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2771)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/haran.html)

[Video Bible](https://www.videobible.com/bible-dictionary/haran)