# יָקְשָׁן

[Yāqšān](https://www.blueletterbible.org/lexicon/h3370)

Definition: Jokshan (4x).

Part of speech: proper masculine noun

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3370)

[Study Light](https://www.studylight.org/lexicons/hebrew/3370.html)

[Bible Hub](https://biblehub.com/str/hebrew/3370.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03370)

[Bible Bento](https://biblebento.com/dictionary/H3370.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3370/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3370.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3370)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jokshan.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jokshan)