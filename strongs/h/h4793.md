# מֵרוֹץ

[mērôṣ](https://www.blueletterbible.org/lexicon/h4793)

Definition: race (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

Hebrew: [mᵊrûṣâ](../h/h4794.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4793)

[Study Light](https://www.studylight.org/lexicons/hebrew/4793.html)

[Bible Hub](https://biblehub.com/str/hebrew/4793.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04793)

[Bible Bento](https://biblebento.com/dictionary/H4793.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4793/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4793.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4793)
