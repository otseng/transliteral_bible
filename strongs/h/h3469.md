# יִשְׁעִי

[YišʿÎ](https://www.blueletterbible.org/lexicon/h3469)

Definition: Ishi (5x).

Part of speech: proper masculine noun

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3469)

[Study Light](https://www.studylight.org/lexicons/hebrew/3469.html)

[Bible Hub](https://biblehub.com/str/hebrew/3469.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03469)

[Bible Bento](https://biblebento.com/dictionary/H3469.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3469/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3469.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3469)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/ishi.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ishi)