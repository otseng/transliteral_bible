# מִכְתָּב

[miḵtāḇ](https://www.blueletterbible.org/lexicon/h4385)

Definition: writing (9x).

Part of speech: masculine noun

Occurs 9 times in 8 verses

Hebrew: [kāṯaḇ](../h/h3789.md), [kᵊṯaḇ](../h/h3790.md), [kᵊṯāḇ](../h/h3791.md), [kᵊṯāḇ](../h/h3792.md), [kᵊṯōḇeṯ](../h/h3793.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4385)

[Study Light](https://www.studylight.org/lexicons/hebrew/4385.html)

[Bible Hub](https://biblehub.com/str/hebrew/4385.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04385)

[Bible Bento](https://biblebento.com/dictionary/H4385.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4385/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4385.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4385)
