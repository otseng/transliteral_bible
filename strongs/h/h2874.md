# טֶבַח

[ṭeḇaḥ](https://www.blueletterbible.org/lexicon/h2874)

Definition: slaughter (9x), slay (1x), sore (1x), beast (1x).

Part of speech: masculine noun

Occurs 12 times in 12 verses

Hebrew: [ṭāḇaḥ](../h/h2873.md), [ṭabāḥ](../h/h2876.md), [ṭabāḥ](../h/h2877.md), [ṭabāḥâ](../h/h2879.md), [maṭbēaḥ](../h/h4293.md)

Names: [Ṭeḇaḥ](../h/h2875.md)

Synonyms: [kill](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Kill)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2874)

[Study Light](https://www.studylight.org/lexicons/hebrew/2874.html)

[Bible Hub](https://biblehub.com/str/hebrew/2874.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02874)

[Bible Bento](https://biblebento.com/dictionary/H2874.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2874/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2874.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2874)
