# חֵלֶם

[Ḥēlem](https://www.blueletterbible.org/lexicon/h2494)

Definition: Helem (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2494)

[Study Light](https://www.studylight.org/lexicons/hebrew/2494.html)

[Bible Hub](https://biblehub.com/str/hebrew/2494.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02494)

[Bible Bento](https://biblebento.com/dictionary/H2494.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2494/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2494.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2494)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/helem.html)