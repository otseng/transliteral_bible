# עֲבֵד נְגוֹ

[ʿĂḇēḏ Nᵊḡô](https://www.blueletterbible.org/lexicon/h5664)

Definition: Abednego (1x).

Part of speech: proper masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5664)

[Study Light](https://www.studylight.org/lexicons/hebrew/5664.html)

[Bible Hub](https://biblehub.com/str/hebrew/5664.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05664)

[Bible Bento](https://biblebento.com/dictionary/H5664.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5664/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5664.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5664)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/abednego.html)