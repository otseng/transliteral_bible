# חֶשְׁבּוֹן

[Hešbôn](https://www.blueletterbible.org/lexicon/h2809)

Definition: Heshbon (38x).

Part of speech: proper locative noun

Occurs 38 times in 37 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2809)

[Study Light](https://www.studylight.org/lexicons/hebrew/2809.html)

[Bible Hub](https://biblehub.com/str/hebrew/2809.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02809)

[Bible Bento](https://biblebento.com/dictionary/H2809.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2809/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2809.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2809)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/heshbon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/heshbon)