# אֲתַר

['ăṯar](https://www.blueletterbible.org/lexicon/h870)

Definition: place (5x), after (3x).

Part of speech: masculine noun

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h870)

[Study Light](https://www.studylight.org/lexicons/hebrew/870.html)

[Bible Hub](https://biblehub.com/str/hebrew/870.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0870)

[Bible Bento](https://biblebento.com/dictionary/H870.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/870/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/870.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h870)
