# שְׁנָה

[šᵊnâ](https://www.blueletterbible.org/lexicon/h8133)

Definition: change (14x), diverse (5x), alter (2x).

Part of speech: verb

Occurs 22 times in 21 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8133)

[Study Light](https://www.studylight.org/lexicons/hebrew/8133.html)

[Bible Hub](https://biblehub.com/str/hebrew/8133.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08133)

[Bible Bento](https://biblebento.com/dictionary/H8133.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8133/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8133.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8133)
