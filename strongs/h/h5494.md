# סוּר

[sûr](https://www.blueletterbible.org/lexicon/h5494)

Definition: degenerate (1x).

Part of speech: participle

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5494)

[Study Light](https://www.studylight.org/lexicons/hebrew/5494.html)

[Bible Hub](https://biblehub.com/str/hebrew/5494.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05494)

[Bible Bento](https://biblebento.com/dictionary/H5494.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5494/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5494.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5494)
