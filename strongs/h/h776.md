# אֶרֶץ

['erets](https://www.blueletterbible.org/lexicon/h776)

Definition: land (1,543x), earth (712x), country (140x), ground (98x), world (4x), way (3x), common (1x), field (1x), nations (1x), wilderness (with H4057) (1x)

Part of speech: feminine noun

Occurs 2,504 times in 2,191 verses

Hebrew: ['ăraʿ](../h/h772.md), ['ăraq](../h/h778.md)

Greek: [gē](../g/g1093.md), [kosmos](../g/g2889.md)

Edenics: earth

Synonyms: [land](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Land)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0776)

[Study Light](https://www.studylight.org/lexicons/hebrew/776.html)

[Bible Hub](https://biblehub.com/str/hebrew/776.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%90%D7%A8%D7%A5)

[Morfix](https://www.morfix.co.il/en/%D7%90%D6%B6%D7%A8%D6%B6%D7%A5)

[NET Bible](http://classic.net.bible.org/strong.php?id=0776)

[Bible Bento](https://biblebento.com/dictionary/H0776.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0776/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0776.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h776)
