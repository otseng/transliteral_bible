# קָלַע

[qālaʿ](https://www.blueletterbible.org/lexicon/h7049)

Definition: sling (4x), carve (3x).

Part of speech: verb

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7049)

[Study Light](https://www.studylight.org/lexicons/hebrew/7049.html)

[Bible Hub](https://biblehub.com/str/hebrew/7049.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07049)

[Bible Bento](https://biblebento.com/dictionary/H7049.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7049/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7049.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7049)
