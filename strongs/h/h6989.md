# קְטוּרָה

[Qᵊṭûrâ](https://www.blueletterbible.org/lexicon/h6989)

Definition: Keturah (4x).

Part of speech: proper feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6989)

[Study Light](https://www.studylight.org/lexicons/hebrew/6989.html)

[Bible Hub](https://biblehub.com/str/hebrew/6989.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06989)

[Bible Bento](https://biblebento.com/dictionary/H6989.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6989/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6989.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6989)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/K/keturah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/keturah)