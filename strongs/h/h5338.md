# נְצַל

[nᵊṣal](https://www.blueletterbible.org/lexicon/h5338)

Definition: deliver (2x), rescue (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5338)

[Study Light](https://www.studylight.org/lexicons/hebrew/5338.html)

[Bible Hub](https://biblehub.com/str/hebrew/5338.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05338)

[Bible Bento](https://biblebento.com/dictionary/H5338.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5338/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5338.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5338)
