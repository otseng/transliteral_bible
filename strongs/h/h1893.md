# הֶבֶל

[Hebel](https://www.blueletterbible.org/lexicon/h1893)

Definition: Abel (8x), "breath", second son of Adam and Eve, killed by his brother Cain

Part of speech: proper masculine noun

Occurs 8 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1893)

[Study Light](https://www.studylight.org/lexicons/hebrew/1893.html)

[Bible Hub](https://biblehub.com/str/hebrew/1893.htm)

[Morfix](https://www.morfix.co.il/en/%D7%94%D6%B6%D7%91%D6%B6%D7%9C)

[NET Bible](http://classic.net.bible.org/strong.php?id=01893)

[Bible Bento](https://biblebento.com/dictionary/H1893.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1893/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1893.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1893)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%94%D7%91%D7%9C)

[Wikipedia](https://en.wikipedia.org/wiki/Cain_and_Abel)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/abel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/abel)