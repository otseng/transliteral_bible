# עָרוֹם

['arowm](https://www.blueletterbible.org/lexicon/h6174)

Definition: naked (16x), bare, fully or partially nude

Part of speech: adjective

Occurs 16 times in 15 verses

Hebrew: [ʿervâ](../h/h6172.md), [`aruwm](../h/h6175.md)

Greek: [gymnos](../g/g1131.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6174)

[Study Light](https://www.studylight.org/lexicons/hebrew/6174.html)

[Bible Hub](https://biblehub.com/str/hebrew/6174.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A2%D7%A8%D7%95%D7%9D)

[NET Bible](http://classic.net.bible.org/strong.php?id=06174)

[Bible Bento](https://biblebento.com/dictionary/H6174.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6174/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6174.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6174)