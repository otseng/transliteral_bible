# סָפַף

[sāp̄ap̄](https://www.blueletterbible.org/lexicon/h5605)

Definition: doorkeeper (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [caph](../h/h5592.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5605)

[Study Light](https://www.studylight.org/lexicons/hebrew/5605.html)

[Bible Hub](https://biblehub.com/str/hebrew/5605.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05605)

[Bible Bento](https://biblebento.com/dictionary/H5605.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5605/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5605.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5605)
