# פֶּרֶץ

[Pereṣ](https://www.blueletterbible.org/lexicon/h6557)

Definition: Pharez (12x), Perez (3x).

Part of speech: proper masculine noun

Occurs 15 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6557)

[Study Light](https://www.studylight.org/lexicons/hebrew/6557.html)

[Bible Hub](https://biblehub.com/str/hebrew/6557.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06557)

[Bible Bento](https://biblebento.com/dictionary/H6557.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6557/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6557.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6557)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/pharez.html)