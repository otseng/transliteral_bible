# קַיָּם

[qayyām](https://www.blueletterbible.org/lexicon/h7011)

Definition: sure (1x), steadfast (1x).

Part of speech: adjective

Occurs 2 times in 2 verses

Hebrew: [quwm](../h/h6965.md), [qûm](../h/h6966.md), [qômâ](../h/h6967.md), [qômmîyûṯ](../h/h6968.md), [qᵊyām](../h/h7010.md), [qîmâ](../h/h7012.md), [tᵊqûmâ](../h/h8617.md), [tᵊqômēm](../h/h8618.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7011)

[Study Light](https://www.studylight.org/lexicons/hebrew/7011.html)

[Bible Hub](https://biblehub.com/str/hebrew/7011.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07011)

[Bible Bento](https://biblebento.com/dictionary/H7011.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7011/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7011.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7011)
