# תְּמוֹל

[tᵊmôl](https://www.blueletterbible.org/lexicon/h8543)

Definition: times past (with H8032) (7x), heretofore (with H8032) (6x), yesterday (4x), as (3x), beforetime (with H8032) (2x), about these days (1x).

Part of speech: adverb

Occurs 23 times in 22 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8543)

[Study Light](https://www.studylight.org/lexicons/hebrew/8543.html)

[Bible Hub](https://biblehub.com/str/hebrew/8543.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08543)

[Bible Bento](https://biblebento.com/dictionary/H8543.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8543/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8543.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8543)
