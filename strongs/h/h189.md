# אֱוִי

['Ĕvî](https://www.blueletterbible.org/lexicon/h189)

Definition: Evi (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h189)

[Study Light](https://www.studylight.org/lexicons/hebrew/189.html)

[Bible Hub](https://biblehub.com/str/hebrew/189.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0189)

[Bible Bento](https://biblebento.com/dictionary/H189.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/189/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/189.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h189)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/evi.html)

[Video Bible](https://www.videobible.com/bible-dictionary/evi)