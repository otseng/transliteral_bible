# כּוּשִׁית

[kûšîṯ](https://www.blueletterbible.org/lexicon/h3571)

Definition: Ethiopian (2x).

Part of speech: feminine adjective

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3571)

[Study Light](https://www.studylight.org/lexicons/hebrew/3571.html)

[Bible Hub](https://biblehub.com/str/hebrew/3571.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03571)

[Bible Bento](https://biblebento.com/dictionary/H3571.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3571/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3571.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3571)
