# חָשַׁב

[chashab](https://www.blueletterbible.org/lexicon/h2803)

Definition: count (23x), devise (22x), think (18x), imagine (9x), cunning (8x), reckon (7x), purpose (6x), esteem (6x), account (5x), impute (4x), forecast (2x), regard (2x), workman (2x), conceived (1x), miscellaneous (9x).

Part of speech: verb

Occurs 124 times in 122 verses

Hebrew: [ḥiššāḇôn](../h/h2810.md), [maḥăšāḇâ](../h/h4284.md)

Greek: [logizomai](../g/g3049.md), [meletaō](../g/g3191.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2803)

[Study Light](https://www.studylight.org/lexicons/hebrew/2803.html)

[Bible Hub](https://biblehub.com/str/hebrew/2803.htm)

[Morfix](https://www.morfix.co.il/en/%D7%97%D6%B8%D7%A9%D6%B7%D7%81%D7%91)

[NET Bible](http://classic.net.bible.org/strong.php?id=02803)

[Bible Bento](https://biblebento.com/dictionary/H2803.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2803/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2803.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2803)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%97%D7%A9%D7%91)
