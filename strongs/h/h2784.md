# חַרְצֻבָּה

[ḥarṣubâ](https://www.blueletterbible.org/lexicon/h2784)

Definition: band (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2784)

[Study Light](https://www.studylight.org/lexicons/hebrew/2784.html)

[Bible Hub](https://biblehub.com/str/hebrew/2784.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02784)

[Bible Bento](https://biblebento.com/dictionary/H2784.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2784/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2784.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2784)
