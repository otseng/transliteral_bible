# בְּאשׁ

[bᵊ'š](https://www.blueletterbible.org/lexicon/h889)

Definition: stink (3x), stench, foul odour

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0889)

[Study Light](https://www.studylight.org/lexicons/hebrew/889.html)

[Bible Hub](https://biblehub.com/str/hebrew/889.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0889)

[Bible Bento](https://biblebento.com/dictionary/H889.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/889/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/889.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h889)
