# אֲבִינֹעַם

['ĂḇînōʿAm](https://www.blueletterbible.org/lexicon/h42)

Definition: Abinoam (4x).

Part of speech: proper masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h42)

[Study Light](https://www.studylight.org/lexicons/hebrew/42.html)

[Bible Hub](https://biblehub.com/str/hebrew/42.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=042)

[Bible Bento](https://biblebento.com/dictionary/H42.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/42/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/42.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h42)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/abinoam.html)

[Video Bible](https://www.videobible.com/bible-dictionary/abinoam)