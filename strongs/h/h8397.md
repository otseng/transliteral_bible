# תֶּבֶל

[teḇel](https://www.blueletterbible.org/lexicon/h8397)

Definition: confusion (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8397)

[Study Light](https://www.studylight.org/lexicons/hebrew/8397.html)

[Bible Hub](https://biblehub.com/str/hebrew/8397.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08397)

[Bible Bento](https://biblebento.com/dictionary/H8397.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8397/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8397.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8397)
