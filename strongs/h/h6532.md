# פָּרֹכֶת

[pārōḵeṯ](https://www.blueletterbible.org/lexicon/h6532)

Definition: vail (25x).

Part of speech: feminine noun

Occurs 25 times in 23 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6532)

[Study Light](https://www.studylight.org/lexicons/hebrew/6532.html)

[Bible Hub](https://biblehub.com/str/hebrew/6532.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06532)

[Bible Bento](https://biblebento.com/dictionary/H6532.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6532/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6532.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6532)
