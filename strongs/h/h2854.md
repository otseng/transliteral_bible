# חֲתֻלָּה

[ḥăṯullâ](https://www.blueletterbible.org/lexicon/h2854)

Definition: swaddlingband (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2854)

[Study Light](https://www.studylight.org/lexicons/hebrew/2854.html)

[Bible Hub](https://biblehub.com/str/hebrew/2854.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02854)

[Bible Bento](https://biblebento.com/dictionary/H2854.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2854/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2854.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2854)
