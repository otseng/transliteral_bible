# עָרַץ

[ʿāraṣ](https://www.blueletterbible.org/lexicon/h6206)

Definition: afraid (3x), fear (3x), dread (2x), terribly (2x), break (1x), affrighted (1x), oppress (1x), prevail (1x), terrified (1x).

Part of speech: verb

Occurs 15 times in 15 verses

Synonyms: [fear](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Fear)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6206)

[Study Light](https://www.studylight.org/lexicons/hebrew/6206.html)

[Bible Hub](https://biblehub.com/str/hebrew/6206.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06206)

[Bible Bento](https://biblebento.com/dictionary/H6206.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6206/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6206.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6206)
