# שִׁמְעֹנִי

[Šimʿōnî](https://www.blueletterbible.org/lexicon/h8099)

Definition: Simeonite (3x), Simeon (1x).

Part of speech: proper masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8099)

[Study Light](https://www.studylight.org/lexicons/hebrew/8099.html)

[Bible Hub](https://biblehub.com/str/hebrew/8099.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08099)

[Bible Bento](https://biblebento.com/dictionary/H8099.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8099/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8099.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8099)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/simeonite.html)