# שִׁשִּׁי

[šiššî](https://www.blueletterbible.org/lexicon/h8345)

Definition: sixth (25x), sixth part (3x).

Part of speech: adjective, masculine/feminine noun

Occurs 28 times in 26 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8345)

[Study Light](https://www.studylight.org/lexicons/hebrew/8345.html)

[Bible Hub](https://biblehub.com/str/hebrew/8345.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08345)

[Bible Bento](https://biblebento.com/dictionary/H8345.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8345/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8345.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8345)
