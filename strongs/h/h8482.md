# תַּחְתִּי

[taḥtî](https://www.blueletterbible.org/lexicon/h8482)

Definition: nether parts (5x), nether (4x), lowest (3x), lower (2x), lower parts (2x), miscellaneous (3x).

Part of speech: adjective

Occurs 19 times in 19 verses

Greek: [bathos](../g/g899.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8482)

[Study Light](https://www.studylight.org/lexicons/hebrew/8482.html)

[Bible Hub](https://biblehub.com/str/hebrew/8482.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08482)

[Bible Bento](https://biblebento.com/dictionary/H8482.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8482/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8482.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8482)
