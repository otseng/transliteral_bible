# צאי

[ṣ'y](https://www.blueletterbible.org/lexicon/h6674)

Definition: filthy (2x).

Part of speech: adjective, masculine noun

Occurs 2 times in 2 verses

Hebrew: [ṣē'â](../h/h6627.md), [ṣ'h](../h/h6675.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6674)

[Study Light](https://www.studylight.org/lexicons/hebrew/6674.html)

[Bible Hub](https://biblehub.com/str/hebrew/6674.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06674)

[Bible Bento](https://biblebento.com/dictionary/H6674.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6674/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6674.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6674)
