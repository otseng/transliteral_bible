# מִפְרָשׂ

[mip̄rāś](https://www.blueletterbible.org/lexicon/h4666)

Definition: spreadings (1x), spreadest forth (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4666)

[Study Light](https://www.studylight.org/lexicons/hebrew/4666.html)

[Bible Hub](https://biblehub.com/str/hebrew/4666.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04666)

[Bible Bento](https://biblebento.com/dictionary/H4666.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4666/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4666.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4666)
