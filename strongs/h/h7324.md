# רִיק

[rîq](https://www.blueletterbible.org/lexicon/h7324)

Definition: (pour, draw...) out (7x), empty (7x), draw (3x), armed (1x), pour forth (1x).

Part of speech: verb

Occurs 19 times in 19 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7324)

[Study Light](https://www.studylight.org/lexicons/hebrew/7324.html)

[Bible Hub](https://biblehub.com/str/hebrew/7324.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07324)

[Bible Bento](https://biblebento.com/dictionary/H7324.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7324/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7324.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7324)
