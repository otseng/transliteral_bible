# מִזְבֵּחַ

[mizbeach](https://www.blueletterbible.org/lexicon/h4196)

Definition: altar (402x), sacrifice, offering, place of slaughter

Part of speech: masculine noun

Occurs 402 times in 338 verses

Hebrew: ['ări'êl](../h/h741.md), [zabach](../h/h2076.md), [maḏbaḥ](../h/h4056.md)

Greek: [thysiastērion](../g/g2379.md), [bōmos](../g/g1041.md)

Root: [זָבַח](http://www.semiticroots.net/index.php?r=word/view&id=27)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4196)

[Study Light](https://www.studylight.org/lexicons/hebrew/4196.html)

[Bible Hub](https://biblehub.com/str/hebrew/4196.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04196)

[Bible Bento](https://biblebento.com/dictionary/H4196.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4196/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4196.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4196)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%9E%D7%96%D7%91%D7%97)
