# זְעֵיר

[zᵊʿêr](https://www.blueletterbible.org/lexicon/h2192)

Definition: little (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2192)

[Study Light](https://www.studylight.org/lexicons/hebrew/2192.html)

[Bible Hub](https://biblehub.com/str/hebrew/2192.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02192)

[Bible Bento](https://biblebento.com/dictionary/H2192.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2192/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2192.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2192)
