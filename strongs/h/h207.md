# אוֹנוֹ

['Ônô](https://www.blueletterbible.org/lexicon/h207)

Definition: Ono (5x).

Part of speech: proper locative noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h207)

[Study Light](https://www.studylight.org/lexicons/hebrew/207.html)

[Bible Hub](https://biblehub.com/str/hebrew/207.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0207)

[Bible Bento](https://biblebento.com/dictionary/H207.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/207/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/207.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h207)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/O/ono.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ono)