# מַשְׂכִּיל

[Maśkîl](https://www.blueletterbible.org/lexicon/h4905)

Definition: Maschil (13x).

Part of speech: masculine noun

Occurs 13 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4905)

[Study Light](https://www.studylight.org/lexicons/hebrew/4905.html)

[Bible Hub](https://biblehub.com/str/hebrew/4905.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04905)

[Bible Bento](https://biblebento.com/dictionary/H4905.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4905/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4905.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4905)
