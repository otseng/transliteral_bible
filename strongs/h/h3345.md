# יְקַד

[yᵊqaḏ](https://www.blueletterbible.org/lexicon/h3345)

Definition: burning (8x).

Part of speech: verb

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3345)

[Study Light](https://www.studylight.org/lexicons/hebrew/3345.html)

[Bible Hub](https://biblehub.com/str/hebrew/3345.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03345)

[Bible Bento](https://biblebento.com/dictionary/H3345.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3345/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3345.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3345)
