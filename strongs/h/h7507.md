# רְפִידָה

[rᵊp̄îḏâ](https://www.blueletterbible.org/lexicon/h7507)

Definition: bottom (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7507)

[Study Light](https://www.studylight.org/lexicons/hebrew/7507.html)

[Bible Hub](https://biblehub.com/str/hebrew/7507.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07507)

[Bible Bento](https://biblebento.com/dictionary/H7507.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7507/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7507.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7507)
