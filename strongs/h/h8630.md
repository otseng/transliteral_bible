# תָּקַף

[tāqap̄](https://www.blueletterbible.org/lexicon/h8630)

Definition: prevail (3x).

Part of speech: verb

Occurs 4 times in 4 verses

Hebrew: [tōqep̄](../h/h8633.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8630)

[Study Light](https://www.studylight.org/lexicons/hebrew/8630.html)

[Bible Hub](https://biblehub.com/str/hebrew/8630.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08630)

[Bible Bento](https://biblebento.com/dictionary/H8630.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8630/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8630.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8630)
