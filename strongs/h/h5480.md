# סוּךְ

[sûḵ](https://www.blueletterbible.org/lexicon/h5480)

Definition: anoint... (8x), at all (1x), pour in anointing

Part of speech: verb

Occurs 9 times in 8 verses

Greek: [aleiphō](../g/g0218.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5480)

[Study Light](https://www.studylight.org/lexicons/hebrew/5480.html)

[Bible Hub](https://biblehub.com/str/hebrew/5480.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05480)

[Bible Bento](https://biblebento.com/dictionary/H5480.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5480/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5480.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5480)
