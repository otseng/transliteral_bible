# אֲחַשְׁדַּרְפְּנִין

['ăḥašdarpᵊnîn](https://www.blueletterbible.org/lexicon/h324)

Definition: princes (9x).

Part of speech: masculine noun

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h324)

[Study Light](https://www.studylight.org/lexicons/hebrew/324.html)

[Bible Hub](https://biblehub.com/str/hebrew/324.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0324)

[Bible Bento](https://biblebento.com/dictionary/H324.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/324/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/324.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h324)
