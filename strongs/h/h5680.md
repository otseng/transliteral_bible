# עִבְרִי

[ʿiḇrî](https://www.blueletterbible.org/lexicon/h5680)

Definition: Hebrew (29x), Hebrew woman (2x), Hebrew (with H376) (1x), Hebrewess (1x), Hebrew man (1x), "one from beyond"

Part of speech: adjective, proper noun

Occurs 34 times in 32 verses

Hebrew: [ʿēḇer](https://www.blueletterbible.org/lexicon/h5677)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5680)

[Study Light](https://www.studylight.org/lexicons/hebrew/5680.html)

[Bible Hub](https://biblehub.com/str/hebrew/5680.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05680)

[Bible Bento](https://biblebento.com/dictionary/H5680.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5680/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5680.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5680)

[Wikipedia](https://en.wikipedia.org/wiki/Hebrews)

[Britannica](https://www.britannica.com/topic/Hebrew)

[Jewish Virtual Library](https://www.jewishvirtuallibrary.org/who-were-the-hebrews#google_vignette)

[Chabad](https://www.chabad.org/library/article_cdo/aid/800868/jewish/Who-Are-the-Hebrews.htm)

[Fandom](https://religion.fandom.com/wiki/Hebrews)
