# מַרְעִית

[marʿîṯ](https://www.blueletterbible.org/lexicon/h4830)

Definition: pasture (9x), flock (1x).

Part of speech: feminine noun

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4830)

[Study Light](https://www.studylight.org/lexicons/hebrew/4830.html)

[Bible Hub](https://biblehub.com/str/hebrew/4830.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04830)

[Bible Bento](https://biblebento.com/dictionary/H4830.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4830/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4830.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4830)
