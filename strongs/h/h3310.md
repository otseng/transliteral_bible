# יַפְלֵט

[Yap̄Lēṭ](https://www.blueletterbible.org/lexicon/h3310)

Definition: Japhlet (3x).

Part of speech: proper masculine noun

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3310)

[Study Light](https://www.studylight.org/lexicons/hebrew/3310.html)

[Bible Hub](https://biblehub.com/str/hebrew/3310.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03310)

[Bible Bento](https://biblebento.com/dictionary/H3310.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3310/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3310.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3310)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/japhlet.html)

[Video Bible](https://www.videobible.com/bible-dictionary/japhlet)