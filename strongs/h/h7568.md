# רֶשֶׁת

[rešeṯ](https://www.blueletterbible.org/lexicon/h7568)

Definition: net (20x), network (with H4639) (1x).

Part of speech: feminine noun

Occurs 22 times in 21 verses

Hebrew: [yarash](../h/h3423.md)

Greek: [diktyon](../g/g1350.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7568)

[Study Light](https://www.studylight.org/lexicons/hebrew/7568.html)

[Bible Hub](https://biblehub.com/str/hebrew/7568.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07568)

[Bible Bento](https://biblebento.com/dictionary/H7568.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7568/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7568.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7568)
