# שֶׂרֶט

[śereṭ](https://www.blueletterbible.org/lexicon/h8296)

Definition: cuttings (2x).

Part of speech: masculine/feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8296)

[Study Light](https://www.studylight.org/lexicons/hebrew/8296.html)

[Bible Hub](https://biblehub.com/str/hebrew/8296.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08296)

[Bible Bento](https://biblebento.com/dictionary/H8296.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8296/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8296.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8296)
