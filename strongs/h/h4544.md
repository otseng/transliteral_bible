# מִסְכֵּנֻת

[miskēnuṯ](https://www.blueletterbible.org/lexicon/h4544)

Definition: scarceness (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4544)

[Study Light](https://www.studylight.org/lexicons/hebrew/4544.html)

[Bible Hub](https://biblehub.com/str/hebrew/4544.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04544)

[Bible Bento](https://biblebento.com/dictionary/H4544.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4544/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4544.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4544)
