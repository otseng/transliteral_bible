# עָמֵל

[ʿāmēl](https://www.blueletterbible.org/lexicon/h6001)

Definition: labour (4x), take (2x), workman (1x), misery (1x), wicked (1x).

Part of speech: verbal adjective, masculine noun

Occurs 9 times in 9 verses

Hebrew: [ʿāmal](../h/h5998.md), [`amal](../h/h5999.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6001)

[Study Light](https://www.studylight.org/lexicons/hebrew/6001.html)

[Bible Hub](https://biblehub.com/str/hebrew/6001.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06001)

[Bible Bento](https://biblebento.com/dictionary/H6001.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6001/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6001.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6001)
