# צֹבֵבָה

[Ṣōḇēḇâ](https://www.blueletterbible.org/lexicon/h6637)

Definition: Zobebah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6637)

[Study Light](https://www.studylight.org/lexicons/hebrew/6637.html)

[Bible Hub](https://biblehub.com/str/hebrew/6637.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06637)

[Bible Bento](https://biblebento.com/dictionary/H6637.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6637/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6637.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6637)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zobebah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/zobebah)