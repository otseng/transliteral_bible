# שָׁמַן

[šāman](https://www.blueletterbible.org/lexicon/h8080)

Definition: wax fat (3x), make fat (1x), became fat (1x).

Part of speech: verb

Occurs 5 times in 4 verses

Hebrew: [šemen](../h/h8081.md), [šāmēn](../h/h8082.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8080)

[Study Light](https://www.studylight.org/lexicons/hebrew/8080.html)

[Bible Hub](https://biblehub.com/str/hebrew/8080.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08080)

[Bible Bento](https://biblebento.com/dictionary/H8080.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8080/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8080.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8080)
