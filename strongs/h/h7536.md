# רֹק

[rōq](https://www.blueletterbible.org/lexicon/h7536)

Definition: spit (1x), spiting (1x), spittle (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7536)

[Study Light](https://www.studylight.org/lexicons/hebrew/7536.html)

[Bible Hub](https://biblehub.com/str/hebrew/7536.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07536)

[Bible Bento](https://biblebento.com/dictionary/H7536.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7536/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7536.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7536)
