# נָתַךְ

[nāṯaḵ](https://www.blueletterbible.org/lexicon/h5413)

Definition: pour out (7x), melt (4x), poured (3x), poured forth (3x), gathered (1x), molten (1x), dropped (1x), gathered together (1x).

Part of speech: verb

Occurs 21 times in 19 verses

Greek: [ekcheō](../g/g1632.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5413)

[Study Light](https://www.studylight.org/lexicons/hebrew/5413.html)

[Bible Hub](https://biblehub.com/str/hebrew/5413.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05413)

[Bible Bento](https://biblebento.com/dictionary/H5413.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5413/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5413.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5413)
