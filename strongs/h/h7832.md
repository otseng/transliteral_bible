# שָׂחַק

[śāḥaq](https://www.blueletterbible.org/lexicon/h7832)

Definition: play (10x), laugh (10x), rejoice (3x), scorn (3x), sport (3x), merry (2x), mock (2x), deride (1x), derision (1x), mockers (1x).

Part of speech: verb

Occurs 36 times in 36 verses

Greek: [gelaō](../g/g1070.md), [katagelaō](../g/g2606.md), [empaizō](../g/g1702.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7832)

[Study Light](https://www.studylight.org/lexicons/hebrew/7832.html)

[Bible Hub](https://biblehub.com/str/hebrew/7832.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07832)

[Bible Bento](https://biblebento.com/dictionary/H7832.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7832/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7832.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7832)
