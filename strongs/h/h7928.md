# שֶׁכֶם

[Šeḵem](https://www.blueletterbible.org/lexicon/h7928)

Definition: Shechem (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7928)

[Study Light](https://www.studylight.org/lexicons/hebrew/7928.html)

[Bible Hub](https://biblehub.com/str/hebrew/7928.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07928)

[Bible Bento](https://biblebento.com/dictionary/H7928.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7928/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7928.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7928)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shechem.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shechem)