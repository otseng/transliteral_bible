# הַדָּבַר

[hadāḇar](https://www.blueletterbible.org/lexicon/h1907)

Definition: counsellor (4x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1907)

[Study Light](https://www.studylight.org/lexicons/hebrew/1907.html)

[Bible Hub](https://biblehub.com/str/hebrew/1907.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01907)

[Bible Bento](https://biblebento.com/dictionary/H1907.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1907/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1907.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1907)
