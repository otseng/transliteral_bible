# כְּבָר

[kᵊḇār](https://www.blueletterbible.org/lexicon/h3528)

Definition: already (5x), now (4x).

Part of speech: adverb

Occurs 9 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3528)

[Study Light](https://www.studylight.org/lexicons/hebrew/3528.html)

[Bible Hub](https://biblehub.com/str/hebrew/3528.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03528)

[Bible Bento](https://biblebento.com/dictionary/H3528.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3528/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3528.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3528)
