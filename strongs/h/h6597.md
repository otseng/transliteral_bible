# פִּתְאֹם

[piṯ'ōm](https://www.blueletterbible.org/lexicon/h6597)

Definition: suddenly (22x), sudden (2x), straightway (1x).

Part of speech: adverb, substantive

Occurs 25 times in 25 verses

Greek: [eutheōs](../g/g2112.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6597)

[Study Light](https://www.studylight.org/lexicons/hebrew/6597.html)

[Bible Hub](https://biblehub.com/str/hebrew/6597.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06597)

[Bible Bento](https://biblebento.com/dictionary/H6597.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6597/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6597.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6597)
