# אָב

['ab](https://www.blueletterbible.org/lexicon/h1)

Definition: father (1,205x), chief (2x), families (2x), desire (1x), fatherless (with H369) (1x), forefathers (with H7223) (1x), patrimony (1x), prince (1x), principal (1x)

Part of speech: masculine noun

Occurs 1,215 times in 1,061 verses

Aramaic: ['ab](../h/h2.md)

Names: ['Aḇrām](../h/h87.md), ['Ăḇîšaḡ](../h/h49.md), ['Aḥ'Āḇ](../h/h256.md), [Mô'āḇ](../h/h4124.md)

Greek: [patēr](../g/g3962.md)

Edenics: paw, papa, abbot

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0001)

[Study Light](https://www.studylight.org/lexicons/hebrew/1.html)

[Bible Hub](https://biblehub.com/str/hebrew/1.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%90%D6%B8%D7%91)

[NET Bible](http://classic.net.bible.org/strong.php?id=01)

[Bible Bento](https://biblebento.com/dictionary/H0001.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0001/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0001.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1)