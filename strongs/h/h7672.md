# שְׁבַשׁ

[šᵊḇaš](https://www.blueletterbible.org/lexicon/h7672)

Definition: astonied (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7672)

[Study Light](https://www.studylight.org/lexicons/hebrew/7672.html)

[Bible Hub](https://biblehub.com/str/hebrew/7672.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07672)

[Bible Bento](https://biblebento.com/dictionary/H7672.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7672/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7672.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7672)
