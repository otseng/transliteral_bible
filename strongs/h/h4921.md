# מְשִׁלֵּמִית

[Mᵊšillēmîṯ](https://www.blueletterbible.org/lexicon/h4921)

Definition: Meshillemith (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4921)

[Study Light](https://www.studylight.org/lexicons/hebrew/4921.html)

[Bible Hub](https://biblehub.com/str/hebrew/4921.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04921)

[Bible Bento](https://biblebento.com/dictionary/H4921.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4921/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4921.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4921)

[Video Bible](https://www.videobible.com/bible-dictionary/meshillemith)