# גָּזַז

[gāzaz](https://www.blueletterbible.org/lexicon/h1494)

Definition: shear (5x), sheepshearer (3x), shearers (3x), cut off (1x), poll (1x), shave (1x), cut down (1x).

Part of speech: verb

Occurs 15 times in 15 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1494)

[Study Light](https://www.studylight.org/lexicons/hebrew/1494.html)

[Bible Hub](https://biblehub.com/str/hebrew/1494.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01494)

[Bible Bento](https://biblebento.com/dictionary/H1494.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1494/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1494.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1494)
