# עָשַׁר

[ʿāšar](https://www.blueletterbible.org/lexicon/h6238)

Definition: rich (14x), enrich (3x), richer (1x).

Part of speech: verb

Occurs 17 times in 17 verses

Hebrew: [ʿāšîr](../h/h6223.md), [ʿōšer](../h/h6239.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6238)

[Study Light](https://www.studylight.org/lexicons/hebrew/6238.html)

[Bible Hub](https://biblehub.com/str/hebrew/6238.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06238)

[Bible Bento](https://biblebento.com/dictionary/H6238.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6238/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6238.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6238)
