# פָּנַק

[pānaq](https://www.blueletterbible.org/lexicon/h6445)

Definition: delicately bring up (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6445)

[Study Light](https://www.studylight.org/lexicons/hebrew/6445.html)

[Bible Hub](https://biblehub.com/str/hebrew/6445.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06445)

[Bible Bento](https://biblebento.com/dictionary/H6445.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6445/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6445.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6445)
