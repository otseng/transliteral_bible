# נָסַע

[nāsaʿ](https://www.blueletterbible.org/lexicon/h5265)

Definition: journey (41x), departed (30x), remove (28x), forward (18x), went (8x), go away (3x), brought (3x), set forth (2x), go forth (3x), get (1x), set aside (1x), miscellaneous (8x).

Part of speech: verb

Occurs 146 times in 140 verses

Edenics: NASA

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5265)

[Study Light](https://www.studylight.org/lexicons/hebrew/5265.html)

[Bible Hub](https://biblehub.com/str/hebrew/5265.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05265)

[Bible Bento](https://biblebento.com/dictionary/H5265.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5265/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5265.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5265)
