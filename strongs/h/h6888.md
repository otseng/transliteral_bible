# צְרֵרָה

[Ṣᵊrērâ](https://www.blueletterbible.org/lexicon/h6888)

Definition: Zererath (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6888)

[Study Light](https://www.studylight.org/lexicons/hebrew/6888.html)

[Bible Hub](https://biblehub.com/str/hebrew/6888.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06888)

[Bible Bento](https://biblebento.com/dictionary/H6888.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6888/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6888.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6888)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zererath.html)