# יַעֲרָה

[yaʿărâ](https://www.blueletterbible.org/lexicon/h3295)

Definition: forest (1x), honeycomb (1,706x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3295)

[Study Light](https://www.studylight.org/lexicons/hebrew/3295.html)

[Bible Hub](https://biblehub.com/str/hebrew/3295.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03295)

[Bible Bento](https://biblebento.com/dictionary/H3295.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3295/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3295.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3295)
