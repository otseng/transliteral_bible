# עֲרִיפִים

[ʿărîp̄îm](https://www.blueletterbible.org/lexicon/h6183)

Definition: heavens (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6183)

[Study Light](https://www.studylight.org/lexicons/hebrew/6183.html)

[Bible Hub](https://biblehub.com/str/hebrew/6183.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06183)

[Bible Bento](https://biblebento.com/dictionary/H6183.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6183/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6183.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6183)
