# דּוּר

[dûr](https://www.blueletterbible.org/lexicon/h1753)

Definition: dwell (5x), inhabitant (2x).

Part of speech: verb

Occurs 13 times in 6 verses

Hebrew: [dôr](../h/h1755.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1753)

[Study Light](https://www.studylight.org/lexicons/hebrew/1753.html)

[Bible Hub](https://biblehub.com/str/hebrew/1753.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01753)

[Bible Bento](https://biblebento.com/dictionary/H1753.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1753/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1753.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1753)
