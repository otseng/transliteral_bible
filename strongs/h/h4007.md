# מַבָּט

[mabāṭ](https://www.blueletterbible.org/lexicon/h4007)

Definition: expectation (3x), object of hope or confidence

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4007)

[Study Light](https://www.studylight.org/lexicons/hebrew/4007.html)

[Bible Hub](https://biblehub.com/str/hebrew/4007.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04007)

[Bible Bento](https://biblebento.com/dictionary/H4007.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4007/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4007.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4007)
