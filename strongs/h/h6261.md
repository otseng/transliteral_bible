# עִתִּי

[ʿitî](https://www.blueletterbible.org/lexicon/h6261)

Definition: fit (1x), timely, ready

Part of speech: adjective

Occurs 1 times in 1 verses

Pictoral: eye cross hand

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6261)

[Study Light](https://www.studylight.org/lexicons/hebrew/6261.html)

[Bible Hub](https://biblehub.com/str/hebrew/6261.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06261)

[Bible Bento](https://biblebento.com/dictionary/H6261.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6261/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6261.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6261)
