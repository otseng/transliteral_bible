# עָפַר

[ʿāp̄ar](https://www.blueletterbible.org/lexicon/h6080)

Definition: cast (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [`aphar](../h/h6083.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6080)

[Study Light](https://www.studylight.org/lexicons/hebrew/6080.html)

[Bible Hub](https://biblehub.com/str/hebrew/6080.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06080)

[Bible Bento](https://biblebento.com/dictionary/H6080.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6080/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6080.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6080)
