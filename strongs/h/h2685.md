# חֲצַף

[ḥăṣap̄](https://www.blueletterbible.org/lexicon/h2685)

Definition: hasty (1x), urgent (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2685)

[Study Light](https://www.studylight.org/lexicons/hebrew/2685.html)

[Bible Hub](https://biblehub.com/str/hebrew/2685.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02685)

[Bible Bento](https://biblebento.com/dictionary/H2685.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2685/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2685.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2685)
