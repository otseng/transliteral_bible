# רָחָם

[rāḥām](https://www.blueletterbible.org/lexicon/h7360)

Definition: gier eagle (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7360)

[Study Light](https://www.studylight.org/lexicons/hebrew/7360.html)

[Bible Hub](https://biblehub.com/str/hebrew/7360.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07360)

[Bible Bento](https://biblebento.com/dictionary/H7360.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7360/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7360.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7360)
