# בּוּקָה

[bûqâ](https://www.blueletterbible.org/lexicon/h950)

Definition: empty (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h950)

[Study Light](https://www.studylight.org/lexicons/hebrew/950.html)

[Bible Hub](https://biblehub.com/str/hebrew/950.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0950)

[Bible Bento](https://biblebento.com/dictionary/H950.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/950/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/950.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h950)
