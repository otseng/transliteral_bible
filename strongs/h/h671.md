# אֲפָֽרְסְכָיֵא

['Ăp̄ārsᵊḵāyē'](https://www.blueletterbible.org/lexicon/h671)

Definition: Apharsachites (2x), Apharsathchites (1x).

Part of speech: proper masculine plural noun (of Persian origin)

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h671)

[Study Light](https://www.studylight.org/lexicons/hebrew/671.html)

[Bible Hub](https://biblehub.com/str/hebrew/671.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0671)

[Bible Bento](https://biblebento.com/dictionary/H671.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/671/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/671.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h671)
