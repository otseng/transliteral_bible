# בְּרוֹשׁ

[bᵊrôš](https://www.blueletterbible.org/lexicon/h1265)

Definition: fir tree (13x), fir (7x), cypress, juniper, pine 

Part of speech: masculine noun

Occurs 20 times in 20 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1265)

[Study Light](https://www.studylight.org/lexicons/hebrew/1265.html)

[Bible Hub](https://biblehub.com/str/hebrew/1265.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01265)

[Bible Bento](https://biblebento.com/dictionary/H1265.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1265/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1265.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1265)

[Plants of the Bible](https://ww2.odu.edu/~lmusselm/plant/bible/cypress.php)
