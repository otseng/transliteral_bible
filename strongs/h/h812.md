# אֶשְׁכֹּל

['eškōl](https://www.blueletterbible.org/lexicon/h812)

Definition: Eshcol (6x).

Part of speech: masculine noun, proper locative noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0812)

[Study Light](https://www.studylight.org/lexicons/hebrew/812.html)

[Bible Hub](https://biblehub.com/str/hebrew/812.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0812)

[Bible Bento](https://biblebento.com/dictionary/H812.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/812/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/812.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h812)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eshcol.html)

[Video Bible](https://www.videobible.com/bible-dictionary/eshcol)