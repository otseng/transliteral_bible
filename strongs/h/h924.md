# בְּהִילוּ

[bᵊhîlû](https://www.blueletterbible.org/lexicon/h924)

Definition: haste (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h924)

[Study Light](https://www.studylight.org/lexicons/hebrew/924.html)

[Bible Hub](https://biblehub.com/str/hebrew/924.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0924)

[Bible Bento](https://biblebento.com/dictionary/H924.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/924/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/924.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h924)
