# צָפָה

[tsaphah](https://www.blueletterbible.org/lexicon/h6822)

Definition: watchman (20x), watch (8x), behold (2x), look (2x), espy (1x), look up (1x), waited (1x), look well (1x), variant for Zophim (1x), spy, observe

Part of speech: verb

Occurs 37 times in 35 verses

Hebrew: [ṣipîyâ](../h/h6836.md), [miṣpê](../h/h4707.md)

Greek: [epiblepō](../g/g1914.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6822)

[Study Light](https://www.studylight.org/lexicons/hebrew/6822.html)

[Bible Hub](https://biblehub.com/str/hebrew/6822.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A6%D6%B8%D7%A4%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=06822)

[Bible Bento](https://biblebento.com/dictionary/H6822.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6822/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6822.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6822)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A6%D7%A4%D7%94)
