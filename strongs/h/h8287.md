# שָׁרוּחֶן

[Šārûḥen](https://www.blueletterbible.org/lexicon/h8287)

Definition: Sharuhen (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8287)

[Study Light](https://www.studylight.org/lexicons/hebrew/8287.html)

[Bible Hub](https://biblehub.com/str/hebrew/8287.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08287)

[Bible Bento](https://biblebento.com/dictionary/H8287.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8287/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8287.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8287)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/sharuhen.html)

[Video Bible](https://www.videobible.com/bible-dictionary/sharuhen)