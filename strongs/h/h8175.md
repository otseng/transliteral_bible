# שָׂעַר

[śāʿar](https://www.blueletterbible.org/lexicon/h8175)

Definition: be (horrible) afraid (4x), come like (to take away as with) a whirlwind (2x), be tempestuous (1x), hurl as a storm (1x).

Part of speech: verb

Occurs 8 times in 8 verses

Synonyms: [fear](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Fear)

## Articles

[Study Light](https://www.studylight.org/lexicons/hebrew/8175.html)

[Bible Hub](https://biblehub.com/str/hebrew/8175.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08175)

[Bible Bento](https://biblebento.com/dictionary/H8175.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8175/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8175.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8175)
