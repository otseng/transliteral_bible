# יִמְנָע

[Yimnāʿ](https://www.blueletterbible.org/lexicon/h3234)

Definition: Imna (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3234)

[Study Light](https://www.studylight.org/lexicons/hebrew/3234.html)

[Bible Hub](https://biblehub.com/str/hebrew/3234.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03234)

[Bible Bento](https://biblebento.com/dictionary/H3234.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3234/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3234.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3234)

[Video Bible](https://www.videobible.com/bible-dictionary/imna)