# אֱלִיעֶזֶר

['Ĕlîʿezer](https://www.blueletterbible.org/lexicon/h461)

Definition: Eliezer (14x).

Part of speech: proper masculine noun

Occurs 14 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0461)

[Study Light](https://www.studylight.org/lexicons/hebrew/461.html)

[Bible Hub](https://biblehub.com/str/hebrew/461.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0461)

[Bible Bento](https://biblebento.com/dictionary/H461.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/461/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/461.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h461)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eliezer.html)

[Video Bible](https://www.videobible.com/bible-dictionary/eliezer)