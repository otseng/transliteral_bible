# קָטַר

[qāṭar](https://www.blueletterbible.org/lexicon/h7000)

Definition: joined (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [qᵊṭôrâ](../h/h6988.md), [qāṭar](../h/h6999.md), [qiṭṭēr](../h/h7002.md), [qᵊṭōreṯ](../h/h7004.md), [qîṭôr](../h/h7008.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7000)

[Study Light](https://www.studylight.org/lexicons/hebrew/7000.html)

[Bible Hub](https://biblehub.com/str/hebrew/7000.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07000)

[Bible Bento](https://biblebento.com/dictionary/H7000.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7000/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7000.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7000)
