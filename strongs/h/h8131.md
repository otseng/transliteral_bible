# שְׂנֵא

[śᵊnē'](https://www.blueletterbible.org/lexicon/h8131)

Definition: them that hate thee (1x).

Part of speech: verb

Occurs 2 times in 1 verses

Hebrew: [sane'](../h/h8130.md), [śin'â](../h/h8135.md), [śᵊnî'](../h/h8146.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8131)

[Study Light](https://www.studylight.org/lexicons/hebrew/8131.html)

[Bible Hub](https://biblehub.com/str/hebrew/8131.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08131)

[Bible Bento](https://biblebento.com/dictionary/H8131.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8131/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8131.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8131)
