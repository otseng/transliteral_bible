# מִקְלַעַת

[miqlaʿaṯ](https://www.blueletterbible.org/lexicon/h4734)

Definition: carved (1x), figures (1x), carving (1x), gravings (1x).

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4734)

[Study Light](https://www.studylight.org/lexicons/hebrew/4734.html)

[Bible Hub](https://biblehub.com/str/hebrew/4734.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04734)

[Bible Bento](https://biblebento.com/dictionary/H4734.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4734/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4734.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4734)
