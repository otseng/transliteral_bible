# מִיכָל

[mîḵāl](https://www.blueletterbible.org/lexicon/h4323)

Definition: brook (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4323)

[Study Light](https://www.studylight.org/lexicons/hebrew/4323.html)

[Bible Hub](https://biblehub.com/str/hebrew/4323.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04323)

[Bible Bento](https://biblebento.com/dictionary/H4323.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4323/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4323.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4323)
