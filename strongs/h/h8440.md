# תּוֹלָעִי

[tôlāʿî](https://www.blueletterbible.org/lexicon/h8440)

Definition: Tolaites (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8440)

[Study Light](https://www.studylight.org/lexicons/hebrew/8440.html)

[Bible Hub](https://biblehub.com/str/hebrew/8440.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08440)

[Bible Bento](https://biblebento.com/dictionary/H8440.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8440/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8440.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8440)
