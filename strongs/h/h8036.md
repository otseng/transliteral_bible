# שֻׁם

[šum](https://www.blueletterbible.org/lexicon/h8036)

Definition: name (11x), named (with H7761) (1x).

Part of speech: masculine noun

Occurs 12 times in 10 verses

Hebrew: [shem](../h/h8034.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8036)

[Study Light](https://www.studylight.org/lexicons/hebrew/8036.html)

[Bible Hub](https://biblehub.com/str/hebrew/8036.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08036)

[Bible Bento](https://biblebento.com/dictionary/H8036.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8036/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8036.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8036)
