# מִשְׁבְּצוֹת

[mišbᵊṣôṯ](https://www.blueletterbible.org/lexicon/h4865)

Definition: ouches (8x), wrought (1x).

Part of speech: feminine noun

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4865)

[Study Light](https://www.studylight.org/lexicons/hebrew/4865.html)

[Bible Hub](https://biblehub.com/str/hebrew/4865.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04865)

[Bible Bento](https://biblebento.com/dictionary/H4865.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4865/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4865.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4865)
