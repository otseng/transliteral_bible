# סַלְסִלָּה

[salsillâ](https://www.blueletterbible.org/lexicon/h5552)

Definition: baskets (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5552)

[Study Light](https://www.studylight.org/lexicons/hebrew/5552.html)

[Bible Hub](https://biblehub.com/str/hebrew/5552.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05552)

[Bible Bento](https://biblebento.com/dictionary/H5552.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5552/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5552.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5552)
