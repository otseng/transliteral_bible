# חֵצִי

[ḥēṣî](https://www.blueletterbible.org/lexicon/h2678)

Definition: arrow (4x), variant (1x).

Part of speech: masculine noun

Occurs 14 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2678)

[Study Light](https://www.studylight.org/lexicons/hebrew/2678.html)

[Bible Hub](https://biblehub.com/str/hebrew/2678.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02678)

[Bible Bento](https://biblebento.com/dictionary/H2678.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2678/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2678.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2678)
