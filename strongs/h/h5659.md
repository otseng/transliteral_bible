# עַבְדוּת

[ʿaḇḏûṯ](https://www.blueletterbible.org/lexicon/h5659)

Definition: bondage (3x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5659)

[Study Light](https://www.studylight.org/lexicons/hebrew/5659.html)

[Bible Hub](https://biblehub.com/str/hebrew/5659.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05659)

[Bible Bento](https://biblebento.com/dictionary/H5659.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5659/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5659.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5659)
