# בֵּית־הַשִּׁמְשִׁי

[bêṯ-haššimšî](https://www.blueletterbible.org/lexicon/h1030)

Definition: Bethshemite (2x).

Part of speech: adjective

Occurs 4 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1030)

[Study Light](https://www.studylight.org/lexicons/hebrew/1030.html)

[Bible Hub](https://biblehub.com/str/hebrew/1030.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01030)

[Bible Bento](https://biblebento.com/dictionary/H1030.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1030/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1030.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1030)
