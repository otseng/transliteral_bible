# יְהוּדִית

[yᵊhûḏîṯ](https://www.blueletterbible.org/lexicon/h3067)

Definition: Judith (1x).

Part of speech: proper feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3067)

[Study Light](https://www.studylight.org/lexicons/hebrew/3067.html)

[Bible Hub](https://biblehub.com/str/hebrew/3067.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03067)

[Bible Bento](https://biblebento.com/dictionary/H3067.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3067/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3067.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3067)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/judith.html)

[Video Bible](https://www.videobible.com/bible-dictionary/judith)