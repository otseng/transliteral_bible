# יָשַׁט

[yāšaṭ](https://www.blueletterbible.org/lexicon/h3447)

Definition: hold out (3x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3447)

[Study Light](https://www.studylight.org/lexicons/hebrew/3447.html)

[Bible Hub](https://biblehub.com/str/hebrew/3447.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03447)

[Bible Bento](https://biblebento.com/dictionary/H3447.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3447/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3447.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3447)
