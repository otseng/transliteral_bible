# סָפָה

[sāp̄â](https://www.blueletterbible.org/lexicon/h5595)

Definition: consume (6x), destroy (5x), add (3x), perish (2x), augment (1x), heap (1x), joined (1x), put (1x).

Part of speech: verb

Occurs 20 times in 20 verses

Synonyms: [destroy](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Destroy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5595)

[Study Light](https://www.studylight.org/lexicons/hebrew/5595.html)

[Bible Hub](https://biblehub.com/str/hebrew/5595.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05595)

[Bible Bento](https://biblebento.com/dictionary/H5595.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5595/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5595.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5595)
