# דֶּלֶף

[delep̄](https://www.blueletterbible.org/lexicon/h1812)

Definition: dropping (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1812)

[Study Light](https://www.studylight.org/lexicons/hebrew/1812.html)

[Bible Hub](https://biblehub.com/str/hebrew/1812.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01812)

[Bible Bento](https://biblebento.com/dictionary/H1812.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1812/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1812.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1812)
