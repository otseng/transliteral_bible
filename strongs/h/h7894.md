# שִׁישָׁא

[Šîšā'](https://www.blueletterbible.org/lexicon/h7894)

Definition: Shisha (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7894)

[Study Light](https://www.studylight.org/lexicons/hebrew/7894.html)

[Bible Hub](https://biblehub.com/str/hebrew/7894.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07894)

[Bible Bento](https://biblebento.com/dictionary/H7894.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7894/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7894.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7894)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shisha.html)