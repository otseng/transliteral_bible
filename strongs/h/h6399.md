# פְּלַח

[pᵊlaḥ](https://www.blueletterbible.org/lexicon/h6399)

Definition: serve (9x), ministers (1x).

Part of speech: verb

Occurs 10 times in 10 verses

Greek: [douleuō](../g/g1398.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6399)

[Study Light](https://www.studylight.org/lexicons/hebrew/6399.html)

[Bible Hub](https://biblehub.com/str/hebrew/6399.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06399)

[Bible Bento](https://biblebento.com/dictionary/H6399.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6399/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6399.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6399)
