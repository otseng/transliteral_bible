# קְשִׂיטָה

[qᵊśîṭâ](https://www.blueletterbible.org/lexicon/h7192)

Definition: piece of money (2x), piece of silver (1x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7192)

[Study Light](https://www.studylight.org/lexicons/hebrew/7192.html)

[Bible Hub](https://biblehub.com/str/hebrew/7192.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07192)

[Bible Bento](https://biblebento.com/dictionary/H7192.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7192/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7192.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7192)
