# תְּחִנָּה

[tĕchinnah](https://www.blueletterbible.org/lexicon/h8467)

Definition: supplication (23x), favour (1x), grace (1x).

Part of speech: feminine noun

Occurs 25 times in 24 verses

Hebrew: [ḥîn](../h/h2433.md), [ḥēn](../h/h2580.md), [ḥānnôṯ](../h/h2589.md)

Greek: [deēsis](../g/g1162.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8467)

[Study Light](https://www.studylight.org/lexicons/hebrew/8467.html)

[Bible Hub](https://biblehub.com/str/hebrew/8467.htm)

[Morfix](https://www.morfix.co.il/en/%D7%AA%D6%BC%D6%B0%D7%97%D6%B4%D7%A0%D6%BC%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=08467)

[Bible Bento](https://biblebento.com/dictionary/H8467.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8467/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8467.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8467)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%AA%D7%97%D7%A0%D7%94)
