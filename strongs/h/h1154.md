# בֵּסֶר

[bēser](https://www.blueletterbible.org/lexicon/h1154)

Definition: unripe grape (1x).

Part of speech: collective masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1154)

[Study Light](https://www.studylight.org/lexicons/hebrew/1154.html)

[Bible Hub](https://biblehub.com/str/hebrew/1154.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01154)

[Bible Bento](https://biblebento.com/dictionary/H1154.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1154/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1154.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1154)
