# קְוֻצּוֹת

[qᵊvuṣṣôṯ](https://www.blueletterbible.org/lexicon/h6977)

Definition: lock (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6977)

[Study Light](https://www.studylight.org/lexicons/hebrew/6977.html)

[Bible Hub](https://biblehub.com/str/hebrew/6977.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06977)

[Bible Bento](https://biblebento.com/dictionary/H6977.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6977/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6977.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6977)
