# אַהַב

['ahaḇ](https://www.blueletterbible.org/lexicon/h158)

Definition: lovers (1x), loving (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

Hebrew: ['ahab](../h/h157.md), ['ōhaḇ](../h/h159.md), ['ahăḇâ](../h/h160.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h158)

[Study Light](https://www.studylight.org/lexicons/hebrew/158.html)

[Bible Hub](https://biblehub.com/str/hebrew/158.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0158)

[Bible Bento](https://biblebento.com/dictionary/H158.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/158/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/158.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h158)
