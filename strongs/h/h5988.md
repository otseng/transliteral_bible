# עַמִּיאֵל

[ʿammî'ēl](https://www.blueletterbible.org/lexicon/h5988)

Definition: Ammiel (6x).

Part of speech: proper masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5988)

[Study Light](https://www.studylight.org/lexicons/hebrew/5988.html)

[Bible Hub](https://biblehub.com/str/hebrew/5988.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05988)

[Bible Bento](https://biblebento.com/dictionary/H5988.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5988/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5988.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5988)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ammiel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ammiel)