# רָעַף

[rāʿap̄](https://www.blueletterbible.org/lexicon/h7491)

Definition: drop (3x), drop down (1x), distil (1x), drip

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7491)

[Study Light](https://www.studylight.org/lexicons/hebrew/7491.html)

[Bible Hub](https://biblehub.com/str/hebrew/7491.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07491)

[Bible Bento](https://biblebento.com/dictionary/H7491.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7491/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7491.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7491)
