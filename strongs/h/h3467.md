# יָשַׁע

[yasha'](https://www.blueletterbible.org/lexicon/h3467)

Definition: save (149x), saviour (15x), deliver (13x), help (12x), preserved (5x), salvation (3x), avenging (2x), at all (1x), avenged (1x), defend (1x), rescue (1x), safe (1x), victory (1x).

Part of speech: verb

Occurs 205 times in 198 verses

Hebrew: [yĕshuw`ah](../h/h3444.md), [yasha'](../h/h3467.md), [yesha`](../h/h3468.md), [môšāʿâ](../h/h4190.md), [tᵊšûʿâ](../h/h8668.md)

Names: [Yᵊhôšûaʿ](../h/h3091.md), [Yᵊšaʿyâ](../h/h3470.md), [Hôšēaʿ](../h/h1954.md)

Greek: [sōzō](../g/g4982.md), [sōtēr](../g/g4990.md)

Pictoral: destroyer watches

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3467)

[Study Light](https://www.studylight.org/lexicons/hebrew/3467.html)

[Bible Hub](https://biblehub.com/str/hebrew/3467.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03467)

[Bible Bento](https://biblebento.com/dictionary/H3467.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3467/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3467.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3467)
