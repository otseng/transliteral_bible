# אֶשְׁעָן

['EšʿĀn](https://www.blueletterbible.org/lexicon/h824)

Definition: Eshean (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h824)

[Study Light](https://www.studylight.org/lexicons/hebrew/824.html)

[Bible Hub](https://biblehub.com/str/hebrew/824.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0824)

[Bible Bento](https://biblebento.com/dictionary/H824.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/824/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/824.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h824)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eshean.html)