# הָסָה

[hāsâ](https://www.blueletterbible.org/lexicon/h2013)

Definition: keep silence (3x), hold your peace (2x), hold your tongue 1 still (1x), silence (1x).

Part of speech: interjection, verb

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2013)

[Study Light](https://www.studylight.org/lexicons/hebrew/2013.html)

[Bible Hub](https://biblehub.com/str/hebrew/2013.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02013)

[Bible Bento](https://biblebento.com/dictionary/H2013.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2013/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2013.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2013)
