# דְּלָיָה

[Dᵊlāyâ](https://www.blueletterbible.org/lexicon/h1806)

Definition: Delaiah (6x), Dalaiah (1x).

Part of speech: proper masculine noun

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1806)

[Study Light](https://www.studylight.org/lexicons/hebrew/1806.html)

[Bible Hub](https://biblehub.com/str/hebrew/1806.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01806)

[Bible Bento](https://biblebento.com/dictionary/H1806.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1806/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1806.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1806)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/delaiah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/delaiah)