# בַּעַל זְבוּב

[BaʿAl Zᵊḇûḇ](https://www.blueletterbible.org/lexicon/h1176)

Definition: Baalzebub (4x), "Lord of the flies"

Part of speech: proper masculine noun

Occurs 8 times in 4 verses

Hebrew: [BaʿAl](../h/h1168.md), [zᵊḇûḇ](../h/h2070.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1176)

[Study Light](https://www.studylight.org/lexicons/hebrew/1176.html)

[Bible Hub](https://biblehub.com/str/hebrew/1176.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01176)

[Bible Bento](https://biblebento.com/dictionary/H1176.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1176/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1176.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1176)

[Wikipedia](https://en.wikipedia.org/wiki/Beelzebub)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/baalzebub.html)