# מִפְשָׂעָה

[mip̄śāʿâ](https://www.blueletterbible.org/lexicon/h4667)

Definition: buttocks (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Hebrew: [pāśaʿ](../h/h6585.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4667)

[Study Light](https://www.studylight.org/lexicons/hebrew/4667.html)

[Bible Hub](https://biblehub.com/str/hebrew/4667.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04667)

[Bible Bento](https://biblebento.com/dictionary/H4667.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4667/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4667.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4667)
