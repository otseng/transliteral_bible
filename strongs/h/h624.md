# אָסֹף

['āsōp̄](https://www.blueletterbible.org/lexicon/h624)

Definition: Asuppim (2x), threshold (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h624)

[Study Light](https://www.studylight.org/lexicons/hebrew/624.html)

[Bible Hub](https://biblehub.com/str/hebrew/624.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0624)

[Bible Bento](https://biblebento.com/dictionary/H624.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/624/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/624.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h624)
