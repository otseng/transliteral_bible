# רִמָּה

[rimmâ](https://www.blueletterbible.org/lexicon/h7415)

Definition: worm (7x).

Part of speech: feminine noun

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7415)

[Study Light](https://www.studylight.org/lexicons/hebrew/7415.html)

[Bible Hub](https://biblehub.com/str/hebrew/7415.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07415)

[Bible Bento](https://biblebento.com/dictionary/H7415.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7415/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7415.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7415)
