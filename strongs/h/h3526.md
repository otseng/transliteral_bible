# כָּבַס

[kāḇas](https://www.blueletterbible.org/lexicon/h3526)

Definition: wash (47x), fuller (4x), to wash (by stamping with the feet)

Part of speech: verb

Occurs 51 times in 48 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3526)

[Study Light](https://www.studylight.org/lexicons/hebrew/3526.html)

[Bible Hub](https://biblehub.com/str/hebrew/3526.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03526)

[Bible Bento](https://biblebento.com/dictionary/H3526.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3526/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3526.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3526)
