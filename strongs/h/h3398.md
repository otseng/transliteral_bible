# יַרְחָע

[Yarḥāʿ](https://www.blueletterbible.org/lexicon/h3398)

Definition: Jarha (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3398)

[Study Light](https://www.studylight.org/lexicons/hebrew/3398.html)

[Bible Hub](https://biblehub.com/str/hebrew/3398.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03398)

[Bible Bento](https://biblebento.com/dictionary/H3398.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3398/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3398.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3398)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jarha.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jarha)