# לַחְמָס

[Laḥmās](https://www.blueletterbible.org/lexicon/h3903)

Definition: Lahmam (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3903)

[Study Light](https://www.studylight.org/lexicons/hebrew/3903.html)

[Bible Hub](https://biblehub.com/str/hebrew/3903.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03903)

[Bible Bento](https://biblebento.com/dictionary/H3903.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3903/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3903.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3903)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/L/lahmam.html)

[Video Bible](https://www.videobible.com/bible-dictionary/lahmam)