# עֶבְרֹן

[ʿEḇrōn](https://www.blueletterbible.org/lexicon/h5683)

Definition: Hebron (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5683)

[Study Light](https://www.studylight.org/lexicons/hebrew/5683.html)

[Bible Hub](https://biblehub.com/str/hebrew/5683.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05683)

[Bible Bento](https://biblebento.com/dictionary/H5683.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5683/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5683.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5683)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hebron.html)

[Video Bible](https://www.videobible.com/bible-dictionary/hebron)