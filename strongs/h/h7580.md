# שָׁאַג

[šā'aḡ](https://www.blueletterbible.org/lexicon/h7580)

Definition: roar (19x), mightily (1x), variations of 'roar' (1x).

Part of speech: verb

Occurs 21 times in 17 verses

Greek: [mykaomai](../g/g3455.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7580)

[Study Light](https://www.studylight.org/lexicons/hebrew/7580.html)

[Bible Hub](https://biblehub.com/str/hebrew/7580.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07580)

[Bible Bento](https://biblebento.com/dictionary/H7580.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7580/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7580.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7580)
