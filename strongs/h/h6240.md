# עֶשֶׂר

[ʿeśer](https://www.blueletterbible.org/lexicon/h6240)

Definition: eleven (with H259) (9x), eleven (with H6249) (6x), eleventh (with H6249) (13x), eleventh (with H259) (4x), twelve (with H8147) (106x), twelfth (with H8147) (21x), thirteen (with H7969) (13x), thirteenth (with H7969) (11x), etc to nineteen (152x).

Part of speech: masculine/feminine noun

Occurs 338 times in 292 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6240)

[Study Light](https://www.studylight.org/lexicons/hebrew/6240.html)

[Bible Hub](https://biblehub.com/str/hebrew/6240.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06240)

[Bible Bento](https://biblebento.com/dictionary/H6240.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6240/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6240.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6240)
