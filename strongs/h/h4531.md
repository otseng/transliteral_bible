# מַסָּה

[massâ](https://www.blueletterbible.org/lexicon/h4531)

Definition: temptation (4x), trial (1x).

Part of speech: feminine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4531)

[Study Light](https://www.studylight.org/lexicons/hebrew/4531.html)

[Bible Hub](https://biblehub.com/str/hebrew/4531.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04531)

[Bible Bento](https://biblebento.com/dictionary/H4531.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4531/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4531.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4531)
