# דְּרוֹר

[dᵊrôr](https://www.blueletterbible.org/lexicon/h1865)

Definition: liberty (7x), pure (1x).

Part of speech: masculine noun

Occurs 8 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1865)

[Study Light](https://www.studylight.org/lexicons/hebrew/1865.html)

[Bible Hub](https://biblehub.com/str/hebrew/1865.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01865)

[Bible Bento](https://biblebento.com/dictionary/H1865.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1865/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1865.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1865)
