# עַנְתֹתִיָּה

[ʿAnṯōṯîyâ](https://www.blueletterbible.org/lexicon/h6070)

Definition: Antothijah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6070)

[Study Light](https://www.studylight.org/lexicons/hebrew/6070.html)

[Bible Hub](https://biblehub.com/str/hebrew/6070.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06070)

[Bible Bento](https://biblebento.com/dictionary/H6070.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6070/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6070.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6070)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/antothijah.html)