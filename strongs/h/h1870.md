# דֶּרֶךְ

[derek](https://www.blueletterbible.org/lexicon/h1870)

Definition: way (590x), toward (31x), journey (23x), manner (8x), conversation, highway, road, path

Part of speech: masculine noun

Occurs 705 times in 627 verses

Hebrew: [dāraḵ](../h/h1869.md)

Greek: [hodos](../g/g3598.md)

Edenics: direction

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1870)

[Study Light](https://www.studylight.org/lexicons/hebrew/1870.html)

[Bible Hub](https://biblehub.com/str/hebrew/1870.htm)

[Morfix](https://www.morfix.co.il/en/%D7%93%D6%BC%D6%B6%D7%A8%D6%B6%D7%9A%D6%B0)

[NET Bible](http://classic.net.bible.org/strong.php?id=01870)

[Bible Bento](https://biblebento.com/dictionary/H1870.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1870/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1870.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1870)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%93%D7%A8%D7%9A)
