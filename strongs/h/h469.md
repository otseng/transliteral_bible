# אֱלִיצָפָן

['Ĕlîṣāp̄ān](https://www.blueletterbible.org/lexicon/h469)

Definition: Elizaphan (4x), Elzaphan (2x).

Part of speech: proper masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h469)

[Study Light](https://www.studylight.org/lexicons/hebrew/469.html)

[Bible Hub](https://biblehub.com/str/hebrew/469.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0469)

[Bible Bento](https://biblebento.com/dictionary/H469.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/469/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/469.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h469)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/elizaphan.html)

[Video Bible](https://www.videobible.com/bible-dictionary/elizaphan)