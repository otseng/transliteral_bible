# בְּעָא

[bᵊʿā'](https://www.blueletterbible.org/lexicon/h1156)

Definition: seek (3x), ask (3x), desire (3x), pray (3x), request (3x), make (petition) (1x).

Part of speech: verb

Occurs 12 times in 12 verses

Synonyms: [pray](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Pray), [seek](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Seek)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1156)

[Study Light](https://www.studylight.org/lexicons/hebrew/1156.html)

[Bible Hub](https://biblehub.com/str/hebrew/1156.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01156)

[Bible Bento](https://biblebento.com/dictionary/H1156.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1156/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1156.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1156)
