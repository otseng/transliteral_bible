# בֵּצַי

[Bēṣay](https://www.blueletterbible.org/lexicon/h1209)

Definition: Bezai (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1209)

[Study Light](https://www.studylight.org/lexicons/hebrew/1209.html)

[Bible Hub](https://biblehub.com/str/hebrew/1209.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01209)

[Bible Bento](https://biblebento.com/dictionary/H1209.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1209/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1209.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1209)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bezai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/bezai)