# מַנְעַנְעִים

[manʿanʿîm](https://www.blueletterbible.org/lexicon/h4517)

Definition: cornet (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4517)

[Study Light](https://www.studylight.org/lexicons/hebrew/4517.html)

[Bible Hub](https://biblehub.com/str/hebrew/4517.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04517)

[Bible Bento](https://biblebento.com/dictionary/H4517.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4517/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4517.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4517)
