# בֶּשֶׂם

[beśem](https://www.blueletterbible.org/lexicon/h1314)

Definition: spice (24x), sweet odours (2x), sweet (2x), sweet smell (1x).

Part of speech: masculine noun

Occurs 29 times in 25 verses

Hebrew: [bāśām](../h/h1313.md)

Greek: [thymiama](../g/g2368.md), [myron](../g/g3464.md), [arōma](../g/g759.md), [osmē](../g/g3744.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1314)

[Study Light](https://www.studylight.org/lexicons/hebrew/1314.html)

[Bible Hub](https://biblehub.com/str/hebrew/1314.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01314)

[Bible Bento](https://biblebento.com/dictionary/H1314.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1314/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1314.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1314)
