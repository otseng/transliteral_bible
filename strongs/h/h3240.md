# יָנַח

[yānaḥ](https://www.blueletterbible.org/lexicon/h3240)

Definition: leave (24x), up (10x), lay (8x), suffer (5x), place (4x), put (4x), set (4x), ...down (4x), let alone (4x), ...him (2x), bestowed (1x), leave off (1x), pacifieth (1x), still (1x), withdraw (1x), withhold (1x).

Part of speech: verb

Occurs 75 times in 73 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3240)

[Study Light](https://www.studylight.org/lexicons/hebrew/3240.html)

[Bible Hub](https://biblehub.com/str/hebrew/3240.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03240)

[Bible Bento](https://biblebento.com/dictionary/H3240.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3240/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3240.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3240)
