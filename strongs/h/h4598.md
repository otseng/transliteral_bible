# מְעִיל

[mᵊʿîl](https://www.blueletterbible.org/lexicon/h4598)

Definition: robe (19x), mantle (7x), cloke (1x), coat (1x).

Part of speech: masculine noun

Occurs 28 times in 27 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4598)

[Study Light](https://www.studylight.org/lexicons/hebrew/4598.html)

[Bible Hub](https://biblehub.com/str/hebrew/4598.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04598)

[Bible Bento](https://biblebento.com/dictionary/H4598.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4598/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4598.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4598)
