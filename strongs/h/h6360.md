# פַּטִּישׁ

[paṭṭîš](https://www.blueletterbible.org/lexicon/h6360)

Definition: hammer (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6360)

[Study Light](https://www.studylight.org/lexicons/hebrew/6360.html)

[Bible Hub](https://biblehub.com/str/hebrew/6360.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06360)

[Bible Bento](https://biblebento.com/dictionary/H6360.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6360/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6360.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6360)
