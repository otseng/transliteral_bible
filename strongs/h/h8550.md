# תֻּמִּים

[Tummîm](https://www.blueletterbible.org/lexicon/h8550)

Definition: Thummim (5x).

Part of speech: proper masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8550)

[Study Light](https://www.studylight.org/lexicons/hebrew/8550.html)

[Bible Hub](https://biblehub.com/str/hebrew/8550.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08550)

[Bible Bento](https://biblebento.com/dictionary/H8550.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8550/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8550.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8550)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/T/thummim.html)

[Video Bible](https://www.videobible.com/bible-dictionary/thummim)