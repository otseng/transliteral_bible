# נוּד

[nuwd](https://www.blueletterbible.org/lexicon/h5110)

Definition: bemoan (7x), remove (5x), vagabond (2x), flee (1x), get (1x), mourn (1x), move (1x), pity (1x), shaken (1x), skippedst (1x), sorry (1x), wag (1x), wandering (1x).

Part of speech: verb

Occurs 24 times in 24 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5110)

[Study Light](https://www.studylight.org/lexicons/hebrew/5110.html)

[Bible Hub](https://biblehub.com/str/hebrew/5110.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A0%D7%95%D6%BC%D7%93)

[NET Bible](http://classic.net.bible.org/strong.php?id=05110)

[Bible Bento](https://biblebento.com/dictionary/H5110.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5110/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5110.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5110)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A0%D7%95%D7%95%D7%93)
