# חֹבֶרֶת

[ḥōḇereṯ](https://www.blueletterbible.org/lexicon/h2279)

Definition: coupling (2x), couple (2x).

Part of speech: feminine noun

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2279)

[Study Light](https://www.studylight.org/lexicons/hebrew/2279.html)

[Bible Hub](https://biblehub.com/str/hebrew/2279.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02279)

[Bible Bento](https://biblebento.com/dictionary/H2279.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2279/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2279.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2279)
