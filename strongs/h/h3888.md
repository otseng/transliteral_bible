# לוּשׁ

[lûš](https://www.blueletterbible.org/lexicon/h3888)

Definition: knead (5x).

Part of speech: verb

Occurs 6 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3888)

[Study Light](https://www.studylight.org/lexicons/hebrew/3888.html)

[Bible Hub](https://biblehub.com/str/hebrew/3888.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03888)

[Bible Bento](https://biblebento.com/dictionary/H3888.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3888/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3888.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3888)
