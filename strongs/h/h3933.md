# לַעַג

[laʿaḡ](https://www.blueletterbible.org/lexicon/h3933)

Definition: scorn (4x), derision (3x).

Part of speech: masculine noun

Occurs 7 times in 7 verses

Greek: [oneidos](../g/g3681.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3933)

[Study Light](https://www.studylight.org/lexicons/hebrew/3933.html)

[Bible Hub](https://biblehub.com/str/hebrew/3933.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03933)

[Bible Bento](https://biblebento.com/dictionary/H3933.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3933/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3933.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3933)
