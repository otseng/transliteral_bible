# אֲדַלְיָא

['Ăḏalyā'](https://www.blueletterbible.org/lexicon/h118)

Definition: Adalia (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h118)

[Study Light](https://www.studylight.org/lexicons/hebrew/118.html)

[Bible Hub](https://biblehub.com/str/hebrew/118.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0118)

[Bible Bento](https://biblebento.com/dictionary/H118.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/118/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/118.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h118)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/adalia.html)

[Video Bible](https://www.videobible.com/bible-dictionary/adalia)