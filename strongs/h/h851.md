# אֶשְׁתְּמֹעַ

['Eštᵊmōaʿ](https://www.blueletterbible.org/lexicon/h851)

Definition: Eshtemoa (5x), Eshtemoh (1x).

Part of speech: proper locative noun, proper masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h851)

[Study Light](https://www.studylight.org/lexicons/hebrew/851.html)

[Bible Hub](https://biblehub.com/str/hebrew/851.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0851)

[Bible Bento](https://biblebento.com/dictionary/H851.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/851/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/851.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h851)

[Video Bible](https://www.videobible.com/bible-dictionary/eshtemoa)