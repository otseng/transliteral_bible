# דָּלַף

[dālap̄](https://www.blueletterbible.org/lexicon/h1811)

Definition: pour out (1x), melt (1x), drop through (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1811)

[Study Light](https://www.studylight.org/lexicons/hebrew/1811.html)

[Bible Hub](https://biblehub.com/str/hebrew/1811.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01811)

[Bible Bento](https://biblebento.com/dictionary/H1811.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1811/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1811.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1811)
