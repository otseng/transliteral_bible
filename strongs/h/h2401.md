# חֲטָאָה

[ḥăṭā'â](https://www.blueletterbible.org/lexicon/h2401)

Definition: sin (7x), sin offering (1x).

Part of speech: feminine noun

Occurs 8 times in 8 verses

Hebrew: [chatta'ath](../h/h2403.md), [chata'](../h/h2398.md)

Synonyms: [sin](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Sin)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2401)

[Study Light](https://www.studylight.org/lexicons/hebrew/2401.html)

[Bible Hub](https://biblehub.com/str/hebrew/2401.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02401)

[Bible Bento](https://biblebento.com/dictionary/H2401.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2401/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2401.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2401)
