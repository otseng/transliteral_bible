# נַחֲרַי

[Naḥăray](https://www.blueletterbible.org/lexicon/h5171)

Definition: Naharai (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5171)

[Study Light](https://www.studylight.org/lexicons/hebrew/5171.html)

[Bible Hub](https://biblehub.com/str/hebrew/5171.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05171)

[Bible Bento](https://biblebento.com/dictionary/H5171.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5171/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5171.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5171)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/naharai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/naharai)