# חָצוֹר חֲדַתָּה

[Ḥāṣôr Ḥăḏatâ](https://www.blueletterbible.org/lexicon/h2675)

Definition: Hazor, Hadattah (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2675)

[Study Light](https://www.studylight.org/lexicons/hebrew/2675.html)

[Bible Hub](https://biblehub.com/str/hebrew/2675.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02675)

[Bible Bento](https://biblebento.com/dictionary/H2675.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2675/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2675.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2675)

[Video Bible](https://www.videobible.com/bible-dictionary/hazor,)