# בּוּשׁ

[buwsh](https://www.blueletterbible.org/lexicon/h954)

Definition: ashamed (72x), confounded (21x), shame (9x), all 2 (inf. for emphasis), confusion (1x), delayed (1x), dry (1x), long (1x), shamed (1x), disconcerted, disappointed, confusion, embarrassed

Part of speech: verb

Occurs 109 times in 100 verses

Hebrew: [bûšâ](../h/h955.md), [bšeṯ](../h/h1322.md)

Greek: [epaischynomai](../g/g1870.md)

Edenics: blush

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0954)

[Study Light](https://www.studylight.org/lexicons/hebrew/954.html)

[Bible Hub](https://biblehub.com/str/hebrew/954.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%91%D7%95%D7%A9)

[NET Bible](http://classic.net.bible.org/strong.php?id=0954)

[Bible Bento](https://biblebento.com/dictionary/H0954.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0954/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0954.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h954)