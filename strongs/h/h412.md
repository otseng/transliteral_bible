# אֵל

['ēl](https://www.blueletterbible.org/lexicon/h412)

Definition: these (1x).

Part of speech: demonstrative particle

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h412)

[Study Light](https://www.studylight.org/lexicons/hebrew/412.html)

[Bible Hub](https://biblehub.com/str/hebrew/412.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0412)

[Bible Bento](https://biblebento.com/dictionary/H412.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/412/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/412.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h412)
