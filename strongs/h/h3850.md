# לֹד

[Lōḏ](https://www.blueletterbible.org/lexicon/h3850)

Definition: Lod (4x).

Part of speech: proper locative noun, proper masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3850)

[Study Light](https://www.studylight.org/lexicons/hebrew/3850.html)

[Bible Hub](https://biblehub.com/str/hebrew/3850.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03850)

[Bible Bento](https://biblebento.com/dictionary/H3850.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3850/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3850.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3850)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/L/lod.html)

[Video Bible](https://www.videobible.com/bible-dictionary/lod)