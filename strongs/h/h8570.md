# תְּנוּבָה

[tᵊnûḇâ](https://www.blueletterbible.org/lexicon/h8570)

Definition: fruit (3x), increase (2x).

Part of speech: feminine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8570)

[Study Light](https://www.studylight.org/lexicons/hebrew/8570.html)

[Bible Hub](https://biblehub.com/str/hebrew/8570.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08570)

[Bible Bento](https://biblebento.com/dictionary/H8570.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8570/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8570.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8570)
