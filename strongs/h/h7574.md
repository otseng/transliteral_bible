# רֹתֶם

[rōṯem](https://www.blueletterbible.org/lexicon/h7574)

Definition: juniper (2x), juniper tree (2x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7574)

[Study Light](https://www.studylight.org/lexicons/hebrew/7574.html)

[Bible Hub](https://biblehub.com/str/hebrew/7574.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07574)

[Bible Bento](https://biblebento.com/dictionary/H7574.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7574/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7574.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7574)
