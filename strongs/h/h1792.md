# דָּכָא

[dāḵā'](https://www.blueletterbible.org/lexicon/h1792)

Definition: break (3x), break in pieces (3x), crush (3x), bruise (2x), destroy (2x), contrite (1x), smite (1x), oppress (1x), beat to pieces (1x), humble (1x).

Part of speech: verb

Occurs 18 times in 18 verses

Hebrew: [hālam](../h/h1986.md), [kāṯaṯ](../h/h3807.md), [māḥaṣ](../h/h4272.md), [naga`](../h/h5060.md), [nāḡap̄](../h/h5062.md), [nakah](../h/h5221.md), [sāp̄aq](../h/h5606.md)

Greek: [diarrēssō](../g/g1284.md)

Synonyms: [destroy](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Destroy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1792)

[Study Light](https://www.studylight.org/lexicons/hebrew/1792.html)

[Bible Hub](https://biblehub.com/str/hebrew/1792.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01792)

[Bible Bento](https://biblebento.com/dictionary/H1792.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1792/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1792.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1792)
