# מְגוּרָה

[mᵊḡûrâ](https://www.blueletterbible.org/lexicon/h4035)

Definition: fear (2x), barn (1x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4035)

[Study Light](https://www.studylight.org/lexicons/hebrew/4035.html)

[Bible Hub](https://biblehub.com/str/hebrew/4035.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04035)

[Bible Bento](https://biblebento.com/dictionary/H4035.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4035/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4035.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4035)
