# חֶלְקַי

[Ḥelqay](https://www.blueletterbible.org/lexicon/h2517)

Definition: Helkai (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2517)

[Study Light](https://www.studylight.org/lexicons/hebrew/2517.html)

[Bible Hub](https://biblehub.com/str/hebrew/2517.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02517)

[Bible Bento](https://biblebento.com/dictionary/H2517.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2517/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2517.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2517)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/helkai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/helkai)