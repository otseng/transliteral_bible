# חֲיָא

[ḥăyā'](https://www.blueletterbible.org/lexicon/h2418)

Definition: live (5x), kept alive (1x).

Part of speech: verb

Occurs 6 times in 6 verses

Synonyms: [live](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Live)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2418)

[Study Light](https://www.studylight.org/lexicons/hebrew/2418.html)

[Bible Hub](https://biblehub.com/str/hebrew/2418.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02418)

[Bible Bento](https://biblebento.com/dictionary/H2418.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2418/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2418.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2418)
