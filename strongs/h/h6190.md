# עָרְלָה

[ʿārlâ](https://www.blueletterbible.org/lexicon/h6190)

Definition: foreskin (13x), uncircumcised (3x).

Part of speech: feminine noun

Occurs 16 times in 16 verses

Hebrew: [ʿārēl](../h/h6188.md), [ʿārēl](../h/h6189.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6190)

[Study Light](https://www.studylight.org/lexicons/hebrew/6190.html)

[Bible Hub](https://biblehub.com/str/hebrew/6190.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06190)

[Bible Bento](https://biblebento.com/dictionary/H6190.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6190/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6190.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6190)
