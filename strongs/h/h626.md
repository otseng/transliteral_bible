# אֲסֵפָה

['ăsēp̄â](https://www.blueletterbible.org/lexicon/h626)

Definition: together (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Hebrew: ['āsap̄](../h/h622.md), [saphah](../h/h8193.md), [śāp̄ām](../h/h8222.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0626)

[Study Light](https://www.studylight.org/lexicons/hebrew/626.html)

[Bible Hub](https://biblehub.com/str/hebrew/626.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0626)

[Bible Bento](https://biblebento.com/dictionary/H626.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/626/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/626.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h626)
