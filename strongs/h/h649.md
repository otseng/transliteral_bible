# אַפַּיִם

['Apayim](https://www.blueletterbible.org/lexicon/h649)

Definition: Appaim (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h649)

[Study Light](https://www.studylight.org/lexicons/hebrew/649.html)

[Bible Hub](https://biblehub.com/str/hebrew/649.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0649)

[Bible Bento](https://biblebento.com/dictionary/H649.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/649/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/649.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h649)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/appaim.html)

[Video Bible](https://www.videobible.com/bible-dictionary/appaim)