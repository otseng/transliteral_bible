# עֶרְיָה

[ʿeryâ](https://www.blueletterbible.org/lexicon/h6181)

Definition: bare (4x), naked (1x), quite (1x).

Part of speech: feminine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6181)

[Study Light](https://www.studylight.org/lexicons/hebrew/6181.html)

[Bible Hub](https://biblehub.com/str/hebrew/6181.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06181)

[Bible Bento](https://biblebento.com/dictionary/H6181.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6181/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6181.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6181)
