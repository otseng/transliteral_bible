# אֶתְנַן

['Eṯnan](https://www.blueletterbible.org/lexicon/h869)

Definition: Ethnan (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h869)

[Study Light](https://www.studylight.org/lexicons/hebrew/869.html)

[Bible Hub](https://biblehub.com/str/hebrew/869.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0869)

[Bible Bento](https://biblebento.com/dictionary/H869.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/869/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/869.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h869)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/ethnan.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ethnan)