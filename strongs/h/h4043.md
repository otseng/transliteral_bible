# מָגֵן

[magen](https://www.blueletterbible.org/lexicon/h4043)

Definition: shield (48x), buckler (9x), armed (2x), defence (2x), rulers (1x), scales (with H650) (1x), protection

Part of speech: masculine noun

Occurs 63 times in 60 verses

Hebrew: [māḡan](../h/h4042.md)

Greek: [thyreos](../g/g2375.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4043)

[Study Light](https://www.studylight.org/lexicons/hebrew/4043.html)

[Bible Hub](https://biblehub.com/str/hebrew/4043.htm)

[Morfix](https://www.morfix.co.il/en/%D7%9E%D6%B8%D7%92%D6%B5%D7%9F)

[NET Bible](http://classic.net.bible.org/strong.php?id=04043)

[Bible Bento](https://biblebento.com/dictionary/H4043.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4043/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4043.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4043)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%9E%D7%92%D7%9F)
