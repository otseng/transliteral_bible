# בָּרִיא

[bārî'](https://www.blueletterbible.org/lexicon/h1277)

Definition: fat (5x), rank (2x), fatfleshed (with H1320) (2x), firm (1x), fatter (1x), fed (1x), plenteous (1x).

Part of speech: adjective

Occurs 13 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1277)

[Study Light](https://www.studylight.org/lexicons/hebrew/1277.html)

[Bible Hub](https://biblehub.com/str/hebrew/1277.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01277)

[Bible Bento](https://biblebento.com/dictionary/H1277.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1277/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1277.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1277)
