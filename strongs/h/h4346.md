# מִכְבָּר

[miḵbār](https://www.blueletterbible.org/lexicon/h4346)

Definition: thick cloth (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4346)

[Study Light](https://www.studylight.org/lexicons/hebrew/4346.html)

[Bible Hub](https://biblehub.com/str/hebrew/4346.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04346)

[Bible Bento](https://biblebento.com/dictionary/H4346.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4346/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4346.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4346)
