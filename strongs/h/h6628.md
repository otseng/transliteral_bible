# צֶאֱלִים

[ṣe'ĕlîm](https://www.blueletterbible.org/lexicon/h6628)

Definition: shady trees (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6628)

[Study Light](https://www.studylight.org/lexicons/hebrew/6628.html)

[Bible Hub](https://biblehub.com/str/hebrew/6628.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06628)

[Bible Bento](https://biblebento.com/dictionary/H6628.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6628/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6628.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6628)
