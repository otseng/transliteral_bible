# בֵּית רָפָא

[Bêṯ Rāp̄Ā'](https://www.blueletterbible.org/lexicon/h1051)

Definition: Bethrapha (1x).

Part of speech: proper masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1051)

[Study Light](https://www.studylight.org/lexicons/hebrew/1051.html)

[Bible Hub](https://biblehub.com/str/hebrew/1051.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01051)

[Bible Bento](https://biblebento.com/dictionary/H1051.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1051/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1051.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1051)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bethrapha.html)