# אַלּוֹן

['allôn](https://www.blueletterbible.org/lexicon/h437)

Definition: oak (8x).

Part of speech: masculine noun

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0437)

[Study Light](https://www.studylight.org/lexicons/hebrew/437.html)

[Bible Hub](https://biblehub.com/str/hebrew/437.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0437)

[Bible Bento](https://biblebento.com/dictionary/H437.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/437/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/437.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h437)
