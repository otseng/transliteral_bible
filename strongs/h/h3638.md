# כִּלְמַד

[Kilmaḏ](https://www.blueletterbible.org/lexicon/h3638)

Definition: Chilmad (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3638)

[Study Light](https://www.studylight.org/lexicons/hebrew/3638.html)

[Bible Hub](https://biblehub.com/str/hebrew/3638.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03638)

[Bible Bento](https://biblebento.com/dictionary/H3638.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3638/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3638.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3638)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/chilmad.html)

[Video Bible](https://www.videobible.com/bible-dictionary/chilmad)