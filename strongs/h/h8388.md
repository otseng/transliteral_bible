# תָּאַר

[tā'ar](https://www.blueletterbible.org/lexicon/h8388)

Definition: draw (5x), mark out (2x).

Part of speech: verb

Occurs 8 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8388)

[Study Light](https://www.studylight.org/lexicons/hebrew/8388.html)

[Bible Hub](https://biblehub.com/str/hebrew/8388.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08388)

[Bible Bento](https://biblebento.com/dictionary/H8388.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8388/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8388.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8388)
