# הֶרֶג

[hereḡ](https://www.blueletterbible.org/lexicon/h2027)

Definition: slaughter (4x), slain (1x).

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2027)

[Study Light](https://www.studylight.org/lexicons/hebrew/2027.html)

[Bible Hub](https://biblehub.com/str/hebrew/2027.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02027)

[Bible Bento](https://biblebento.com/dictionary/H2027.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2027/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2027.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2027)
