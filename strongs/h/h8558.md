# תָּמָר

[tāmār](https://www.blueletterbible.org/lexicon/h8558)

Definition: palm tree (12x).

Part of speech: masculine noun

Occurs 12 times in 12 verses

Hebrew: [timmōrâ](../h/h8561.md)

Greek: [phoinix](../g/g5404.md)

Names: [Tāmār](../h/h8559.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8558)

[Study Light](https://www.studylight.org/lexicons/hebrew/8558.html)

[Bible Hub](https://biblehub.com/str/hebrew/8558.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08558)

[Bible Bento](https://biblebento.com/dictionary/H8558.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8558/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8558.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8558)
