# יְהוֹשׁוּעַ

[Yᵊhôšûaʿ](https://www.blueletterbible.org/lexicon/h3091)

Definition: Joshua (218x), “Jehovah is salvation”

Part of speech: proper masculine noun

Occurs 218 times in 199 verses

Hebrew: [Yĕhovah](../h/h3068.md), [yasha'](../h/h3467.md)

Greek: [Iēsous](../g/g2424.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3091)

[Study Light](https://www.studylight.org/lexicons/hebrew/3091.html)

[Bible Hub](https://biblehub.com/str/hebrew/3091.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03091)

[Bible Bento](https://biblebento.com/dictionary/H3091.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3091/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3091.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3091)

[Wikipedia](https://en.wikipedia.org/wiki/Joshua)

[Wiktionary](https://en.wiktionary.org/wiki/Joshua)

[Biblical Archaeology](https://www.biblicalarchaeology.org/daily/biblical-topics/hebrew-bible/joshua-in-the-bible/)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/joshua.html)

[Video Bible](https://www.videobible.com/bible-dictionary/joshua)