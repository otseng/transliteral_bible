# גַּד

[gaḏ](https://www.blueletterbible.org/lexicon/h1407)

Definition: coriander (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1407)

[Study Light](https://www.studylight.org/lexicons/hebrew/1407.html)

[Bible Hub](https://biblehub.com/str/hebrew/1407.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01407)

[Bible Bento](https://biblebento.com/dictionary/H1407.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1407/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1407.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1407)
