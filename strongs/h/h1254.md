# בָּרָא

[bara'](https://www.blueletterbible.org/lexicon/h1254)

Definition: create (42x), creator (3x), choose (2x), make (2x), cut down (2x), dispatch (1x), done (1x), make fat (1x)

Part of speech: verb

Occurs 54 times in 46 verses

Hebrew: [bᵊrî'â](../h/h1278.md)

Greek: [ktizō](../g/g2936.md), [ginomai](../g/g1096.md)

Edenics: parent, prepare

Pictoral: family of heads

Notes: only refers to God

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1254)

[Study Light](https://www.studylight.org/lexicons/hebrew/1254.html)

[Bible Hub](https://biblehub.com/str/hebrew/1254.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%91%D7%A8%D7%90)

[NET Bible](http://classic.net.bible.org/strong.php?id=01254)

[Bible Bento](https://biblebento.com/dictionary/H1254.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1254/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1254.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1254)

[Logos Apostolic](https://www.logosapostolic.org/hebrew-word-studies/1254-bara-create.htm)
