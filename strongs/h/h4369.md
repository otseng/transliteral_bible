# מְכֻנָה

[mᵊḵunâ](https://www.blueletterbible.org/lexicon/h4369)

Definition: base (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4369)

[Study Light](https://www.studylight.org/lexicons/hebrew/4369.html)

[Bible Hub](https://biblehub.com/str/hebrew/4369.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04369)

[Bible Bento](https://biblebento.com/dictionary/H4369.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4369/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4369.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4369)
