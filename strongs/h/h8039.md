# שִׁמְאָה

[Šim'Â](https://www.blueletterbible.org/lexicon/h8039)

Definition: Shimeah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8039)

[Study Light](https://www.studylight.org/lexicons/hebrew/8039.html)

[Bible Hub](https://biblehub.com/str/hebrew/8039.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08039)

[Bible Bento](https://biblebento.com/dictionary/H8039.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8039/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8039.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8039)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shimeah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shimeah)