# תְּלָתִין

[tᵊlāṯîn](https://www.blueletterbible.org/lexicon/h8533)

Definition: thirty (2x).

Part of speech: indeclinable noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8533)

[Study Light](https://www.studylight.org/lexicons/hebrew/8533.html)

[Bible Hub](https://biblehub.com/str/hebrew/8533.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08533)

[Bible Bento](https://biblebento.com/dictionary/H8533.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8533/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8533.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8533)
