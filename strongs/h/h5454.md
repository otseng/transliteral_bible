# סַבְתָּא

[Saḇtā'](https://www.blueletterbible.org/lexicon/h5454)

Definition: Sabta (1x), Sabtah (1x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5454)

[Study Light](https://www.studylight.org/lexicons/hebrew/5454.html)

[Bible Hub](https://biblehub.com/str/hebrew/5454.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05454)

[Bible Bento](https://biblebento.com/dictionary/H5454.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5454/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5454.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5454)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/sabta.html)