# כָּסַף

[kacaph](https://www.blueletterbible.org/lexicon/h3700)

Definition: desire (2x), long (2x), greedy (1x), sore (1x).

Part of speech: verb

Occurs 6 times in 5 verses

Greek: [epithymia](../g/g1939.md)

Synonyms: [lust](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Lust)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3700)

[Study Light](https://www.studylight.org/lexicons/hebrew/3700.html)

[Bible Hub](https://biblehub.com/str/hebrew/3700.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03700)

[Bible Bento](https://biblebento.com/dictionary/H3700.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3700/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3700.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3700)
