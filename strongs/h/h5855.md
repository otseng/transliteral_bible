# עַטְרוֹת שׁוֹפָן

[ʿAṭrôṯ Šôp̄Ān](https://www.blueletterbible.org/lexicon/h5855)

Definition: Atroth Shophan (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5855)

[Study Light](https://www.studylight.org/lexicons/hebrew/5855.html)

[Bible Hub](https://biblehub.com/str/hebrew/5855.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05855)

[Bible Bento](https://biblebento.com/dictionary/H5855.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5855/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5855.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5855)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/atroth.html)