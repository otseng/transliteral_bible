# יָלַל

[yālal](https://www.blueletterbible.org/lexicon/h3213)

Definition: howl (29x), howlings (1x), variant (1x).

Part of speech: verb

Occurs 31 times in 28 verses

Greek: [ololyzō](../g/g3649.md)

Edenics: yell

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3213)

[Study Light](https://www.studylight.org/lexicons/hebrew/3213.html)

[Bible Hub](https://biblehub.com/str/hebrew/3213.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03213)

[Bible Bento](https://biblebento.com/dictionary/H3213.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3213/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3213.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3213)
