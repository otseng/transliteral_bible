# רָדִיד

[rāḏîḏ](https://www.blueletterbible.org/lexicon/h7289)

Definition: veil (1x), vails (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/hebrew/7289.html)

[Bible Hub](https://biblehub.com/str/hebrew/7289.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07289)

[Bible Bento](https://biblebento.com/dictionary/H7289.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7289/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7289.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7289)
