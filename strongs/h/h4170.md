# מוֹקֵשׁ

[mowqesh](https://www.blueletterbible.org/lexicon/h4170)

Definition: snare (20x), gin (3x), trap (2x), ensnared (1x), snared (1x), bait, lure

Part of speech: masculine noun

Occurs 27 times in 27 verses

Hebrew: [yāqōš](../h/h3369.md), [yāqôš](../h/h3353.md)

Greek: [pagis](../g/g3803.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4170)

[Study Light](https://www.studylight.org/lexicons/hebrew/4170.html)

[Bible Hub](https://biblehub.com/str/hebrew/4170.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04170)

[Bible Bento](https://biblebento.com/dictionary/H4170.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4170/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4170.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4170)
