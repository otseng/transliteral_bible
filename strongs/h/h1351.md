# גָּאַל

[gā'al](https://www.blueletterbible.org/lexicon/h1351)

Definition: pollute (7x), defile (3x), stain (1x).

Part of speech: verb

Occurs 11 times in 9 verses

Greek: [molynō](../g/g3435.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1351)

[Study Light](https://www.studylight.org/lexicons/hebrew/1351.html)

[Bible Hub](https://biblehub.com/str/hebrew/1351.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01351)

[Bible Bento](https://biblebento.com/dictionary/H1351.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1351/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1351.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1351)
