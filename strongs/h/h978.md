# בַּחֲרוּמִי

[baḥărûmî](https://www.blueletterbible.org/lexicon/h978)

Definition: Baharumite (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h978)

[Study Light](https://www.studylight.org/lexicons/hebrew/978.html)

[Bible Hub](https://biblehub.com/str/hebrew/978.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0978)

[Bible Bento](https://biblebento.com/dictionary/H978.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/978/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/978.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h978)
