# נָשָׂא

[nasa'](https://www.blueletterbible.org/lexicon/h5375)

Definition: (bare, lift, etc...) up (219x), bear (115x), take (58x), bare (34x), carry (30x), (take, carry)..away (22x), borne (22x), armourbearer (18x), forgive (16x), accept (12x), exalt (8x), regard (5x), obtained (4x), respect (3x), miscellaneous (74x), respect

Part of speech: verb

Occurs 654 times in 611 verses

Hebrew: ['āḥaz](../h/h270.md), [lāḵaḏ](../h/h3920.md), [laqach](../h/h3947.md), [leqaḥ](../h/h3948.md), [nāśaḡ](../h/h5381.md), [cuwr](../h/h5493.md), [nāśî'](../h/h5387.md), [maśśā'](../h/h4853.md)

Synonyms: [forgive](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Forgive)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5375)

[Study Light](https://www.studylight.org/lexicons/hebrew/5375.html)

[Bible Hub](https://biblehub.com/str/hebrew/5375.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A0%D6%B8%D7%A9%D6%B8%D7%82%D7%90)

[NET Bible](http://classic.net.bible.org/strong.php?id=05375)

[Bible Bento](https://biblebento.com/dictionary/H5375.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5375/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5375.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5375)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A0%D7%A9%D7%90)
