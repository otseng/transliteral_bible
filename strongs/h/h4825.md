# מֶרֶס

[Meres](https://www.blueletterbible.org/lexicon/h4825)

Definition: Meres (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4825)

[Study Light](https://www.studylight.org/lexicons/hebrew/4825.html)

[Bible Hub](https://biblehub.com/str/hebrew/4825.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04825)

[Bible Bento](https://biblebento.com/dictionary/H4825.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4825/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4825.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4825)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/meres.html)

[Video Bible](https://www.videobible.com/bible-dictionary/meres)