# יָגַע

[yaga`](https://www.blueletterbible.org/lexicon/h3021)

Definition: weary (13x), labour (12x), fainted (1x), to be exhausted, to gasp, to be fatigued

Part of speech: verb

Occurs 26 times in 25 verses

Hebrew: [yᵊḡîaʿ](../h/h3018.md)

Greek: [diaponeomai](../g/g1278.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3021)

[Study Light](https://www.studylight.org/lexicons/hebrew/3021.html)

[Bible Hub](https://biblehub.com/str/hebrew/3021.htm)

[Morfix](https://www.morfix.co.il/en/%D7%99%D6%B8%D7%92%D6%B7%D7%A2)

[NET Bible](http://classic.net.bible.org/strong.php?id=03021)

[Bible Bento](https://biblebento.com/dictionary/H3021.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3021/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3021.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3021)
