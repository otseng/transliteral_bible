# נְשָׁמָה

[neshamah](https://www.blueletterbible.org/lexicon/h5397)

Definition: breath (17x), blast (3x), spirit (2x), inspiration (1x), souls (1x).

Part of speech: feminine noun

Occurs 24 times in 24 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5397)

[Study Light](https://www.studylight.org/lexicons/hebrew/5397.html)

[Bible Hub](https://biblehub.com/str/hebrew/5397.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05397)

[Bible Bento](https://biblebento.com/dictionary/H5397.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5397/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5397.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5397)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A0%D7%A9%D7%9E%D7%94)
