# סַלְכָה

[Salḵâ](https://www.blueletterbible.org/lexicon/h5548)

Definition: Salchah (2x), Salcah (2x).

Part of speech: proper locative noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5548)

[Study Light](https://www.studylight.org/lexicons/hebrew/5548.html)

[Bible Hub](https://biblehub.com/str/hebrew/5548.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05548)

[Bible Bento](https://biblebento.com/dictionary/H5548.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5548/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5548.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5548)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/salchah.html)