# דִּבְרָה

[diḇrâ](https://www.blueletterbible.org/lexicon/h1700)

Definition: cause (1x), order (1x), estate (1x), end (1x), regard (1x).

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1700)

[Study Light](https://www.studylight.org/lexicons/hebrew/1700.html)

[Bible Hub](https://biblehub.com/str/hebrew/1700.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01700)

[Bible Bento](https://biblebento.com/dictionary/H1700.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1700/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1700.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1700)
