# רָהַב

[rāhaḇ](https://www.blueletterbible.org/lexicon/h7292)

Definition: make sure (1x), behave proudly (1x), overcome (1x), strengthen (1x).

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7292)

[Study Light](https://www.studylight.org/lexicons/hebrew/7292.html)

[Bible Hub](https://biblehub.com/str/hebrew/7292.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07292)

[Bible Bento](https://biblebento.com/dictionary/H7292.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7292/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7292.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7292)
