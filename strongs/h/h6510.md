# פָּרָה

[pārâ](https://www.blueletterbible.org/lexicon/h6510)

Definition: kine (18x), heifer (6x), cow (2x).

Part of speech: feminine noun

Occurs 26 times in 22 verses

Hebrew: [par](../h/h6499.md)

Greek: [damalis](../g/g1151.md)

Pictoral: open head

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6510)

[Study Light](https://www.studylight.org/lexicons/hebrew/6510.html)

[Bible Hub](https://biblehub.com/str/hebrew/6510.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06510)

[Bible Bento](https://biblebento.com/dictionary/H6510.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6510/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6510.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6510)
