# עֵין רִמּוֹן

[ʿÊn Rimmôn](https://www.blueletterbible.org/lexicon/h5884)

Definition: Enrimmon (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5884)

[Study Light](https://www.studylight.org/lexicons/hebrew/5884.html)

[Bible Hub](https://biblehub.com/str/hebrew/5884.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05884)

[Bible Bento](https://biblebento.com/dictionary/H5884.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5884/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5884.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5884)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/enrimmon.html)