# חָח

[ḥāḥ](https://www.blueletterbible.org/lexicon/h2397)

Definition: hook (5x), chain (2x), bracelet (1x).

Part of speech: masculine noun

Occurs 8 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2397)

[Study Light](https://www.studylight.org/lexicons/hebrew/2397.html)

[Bible Hub](https://biblehub.com/str/hebrew/2397.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02397)

[Bible Bento](https://biblebento.com/dictionary/H2397.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2397/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2397.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2397)
