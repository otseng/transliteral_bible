# בַּעֲרָא

[BaʿĂrā'](https://www.blueletterbible.org/lexicon/h1199)

Definition: Baara (1x).

Part of speech: proper feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1199)

[Study Light](https://www.studylight.org/lexicons/hebrew/1199.html)

[Bible Hub](https://biblehub.com/str/hebrew/1199.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01199)

[Bible Bento](https://biblebento.com/dictionary/H1199.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1199/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1199.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1199)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/baara.html)

[Video Bible](https://www.videobible.com/bible-dictionary/baara)