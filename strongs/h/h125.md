# אֲדַמְדָּם

['ăḏamdām](https://www.blueletterbible.org/lexicon/h125)

Definition: reddish (6x).

Part of speech: adjective

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h125)

[Study Light](https://www.studylight.org/lexicons/hebrew/125.html)

[Bible Hub](https://biblehub.com/str/hebrew/125.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0125)

[Bible Bento](https://biblebento.com/dictionary/H125.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/125/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/125.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h125)
