# גְּבָל

[Gᵊḇāl](https://www.blueletterbible.org/lexicon/h1381)

Definition: Gebal (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1381)

[Study Light](https://www.studylight.org/lexicons/hebrew/1381.html)

[Bible Hub](https://biblehub.com/str/hebrew/1381.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01381)

[Bible Bento](https://biblebento.com/dictionary/H1381.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1381/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1381.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1381)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/gebal.html)

[Video Bible](https://www.videobible.com/bible-dictionary/gebal)