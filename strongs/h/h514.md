# אֶלְתְּקֵא

['Eltᵊqē'](https://www.blueletterbible.org/lexicon/h514)

Definition: Eltekeh (2x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h514)

[Study Light](https://www.studylight.org/lexicons/hebrew/514.html)

[Bible Hub](https://biblehub.com/str/hebrew/514.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0514)

[Bible Bento](https://biblebento.com/dictionary/H514.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/514/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/514.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h514)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eltekeh.html)

[Video Bible](https://www.videobible.com/bible-dictionary/eltekeh)