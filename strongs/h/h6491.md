# פָּקַח

[paqach](https://www.blueletterbible.org/lexicon/h6491)

Definition: open (20x), observant

Part of speech: verb

Occurs 20 times in 18 verses

Hebrew: [piqqēaḥ](../h/h6493.md)

Names: [Peqaḥ](../h/h6492.md)

Greek: [anoigō](../g/g455.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6491)

[Study Light](https://www.studylight.org/lexicons/hebrew/6491.html)

[Bible Hub](https://biblehub.com/str/hebrew/6491.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A4%D6%BC%D6%B8%D7%A7%D6%B7%D7%97)

[NET Bible](http://classic.net.bible.org/strong.php?id=06491)

[Bible Bento](https://biblebento.com/dictionary/H6491.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6491/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6491.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6491)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A4%D7%A7%D7%97)
