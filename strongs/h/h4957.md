# מַשְׂרֵקָה

[maśrēqâ](https://www.blueletterbible.org/lexicon/h4957)

Definition: Masrekah (2x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4957)

[Study Light](https://www.studylight.org/lexicons/hebrew/4957.html)

[Bible Hub](https://biblehub.com/str/hebrew/4957.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04957)

[Bible Bento](https://biblebento.com/dictionary/H4957.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4957/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4957.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4957)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/masrekah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/masrekah)