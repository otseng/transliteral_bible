# יַד

[yaḏ](https://www.blueletterbible.org/lexicon/h3028)

Definition: hand (16x), power (1x).

Part of speech: feminine noun

Occurs 17 times in 16 verses

Hebrew: [yad](../h/h3027.md), [yaḵ](../h/h3197.md)

Greek: [cheir](../g/g5495.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3028)

[Study Light](https://www.studylight.org/lexicons/hebrew/3028.html)

[Bible Hub](https://biblehub.com/str/hebrew/3028.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03028)

[Bible Bento](https://biblebento.com/dictionary/H3028.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3028/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3028.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3028)
