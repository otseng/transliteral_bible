# הָלַךְ

[halak](https://www.blueletterbible.org/lexicon/h1980)

Definition: go (217x), walk (156x), come (16x), ...away (7x), ...along (6x), proceed, depart, traverse

Part of speech: verb

Occurs 500 times in 468 

Hebrew: [hûḵ](../h/h1946.md), [hālîḵ](../h/h1978.md), [hălîḵâ](../h/h1979.md), [hălaḵ](../h/h1981.md), [yālaḵ](../h/h3212.md), [mahălāḵ](../h/h4108.md), [mahălāḵ](../h/h4109.md), [tahălûḵâ](../h/h8418.md)

Greek: [exerchomai](../g/g1831.md), [peripateō](../g/g4043.md)

Pictoral: staff in palm

Edenics: walk

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1980)

[Study Light](https://www.studylight.org/lexicons/hebrew/1980.html)

[Bible Hub](https://biblehub.com/str/hebrew/1980.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%94%D7%9C%D7%9A)

[NET Bible](http://classic.net.bible.org/strong.php?id=01980)

[Bible Bento](https://biblebento.com/dictionary/H1980.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1980/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1980.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1980)
