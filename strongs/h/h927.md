# בְּהַל

[bᵊhal](https://www.blueletterbible.org/lexicon/h927)

Definition: trouble (8x), haste (3x).

Part of speech: verb

Occurs 11 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h927)

[Study Light](https://www.studylight.org/lexicons/hebrew/927.html)

[Bible Hub](https://biblehub.com/str/hebrew/927.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0927)

[Bible Bento](https://biblebento.com/dictionary/H927.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/927/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/927.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h927)
