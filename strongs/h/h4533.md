# מַסְוֶה

[masvê](https://www.blueletterbible.org/lexicon/h4533)

Definition: vail (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4533)

[Study Light](https://www.studylight.org/lexicons/hebrew/4533.html)

[Bible Hub](https://biblehub.com/str/hebrew/4533.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04533)

[Bible Bento](https://biblebento.com/dictionary/H4533.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4533/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4533.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4533)
