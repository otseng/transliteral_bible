# אֻזֶּן שֶׁאֱרָה

['Uzzen Še'Ĕrâ](https://www.blueletterbible.org/lexicon/h242)

Definition: Uzzensherah (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h242)

[Study Light](https://www.studylight.org/lexicons/hebrew/242.html)

[Bible Hub](https://biblehub.com/str/hebrew/242.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0242)

[Bible Bento](https://biblebento.com/dictionary/H242.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/242/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/242.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h242)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/U/uzzensherah.html)