# יַחְמַי

[Yaḥmay](https://www.blueletterbible.org/lexicon/h3181)

Definition: Jahmai (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3181)

[Study Light](https://www.studylight.org/lexicons/hebrew/3181.html)

[Bible Hub](https://biblehub.com/str/hebrew/3181.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03181)

[Bible Bento](https://biblebento.com/dictionary/H3181.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3181/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3181.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3181)

[Video Bible](https://www.videobible.com/bible-dictionary/jahmai)