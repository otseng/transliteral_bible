# קָצֵר

[qāṣēr](https://www.blueletterbible.org/lexicon/h7116)

Definition: small (2x), few (1x), soon (1x), hasty (1x).

Part of speech: adjective

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7116)

[Study Light](https://www.studylight.org/lexicons/hebrew/7116.html)

[Bible Hub](https://biblehub.com/str/hebrew/7116.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07116)

[Bible Bento](https://biblebento.com/dictionary/H7116.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7116/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7116.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7116)
