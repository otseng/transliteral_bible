# יָלַךְ

[yālaḵ](https://www.blueletterbible.org/lexicon/h3212)

Definition: go (628x), walk (122x), come (77x), depart (66x), ...away (20x), follow (20x), get (14x), lead (17x), brought (8x), carry (5x), bring (4x), miscellaneous (62x).

Part of speech: verb

Occurs 1,044 times in 936 verses

Hebrew: [hûḵ](../h/h1946.md), [hālîḵ](../h/h1978.md), [hălîḵâ](../h/h1979.md), [halak](../h/h1980.md), [hălaḵ](../h/h1981.md), [mahălāḵ](../h/h4108.md), [mahălāḵ](../h/h4109.md), [mal'ak](../h/h4397.md), [tahălûḵâ](../h/h8418.md)

Greek: [akoloutheō](../g/g190.md), [poreuō](../g/g4198.md)

Pictoral: staff in palm

Edenics: walk

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3212)

[Study Light](https://www.studylight.org/lexicons/hebrew/3212.html)

[Bible Hub](https://biblehub.com/str/hebrew/3212.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03212)

[Bible Bento](https://biblebento.com/dictionary/H3212.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3212/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3212.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3212)
