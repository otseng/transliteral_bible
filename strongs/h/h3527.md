# כָּבַר

[kāḇar](https://www.blueletterbible.org/lexicon/h3527)

Definition: multiply (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3527)

[Study Light](https://www.studylight.org/lexicons/hebrew/3527.html)

[Bible Hub](https://biblehub.com/str/hebrew/3527.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03527)

[Bible Bento](https://biblebento.com/dictionary/H3527.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3527/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3527.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3527)
