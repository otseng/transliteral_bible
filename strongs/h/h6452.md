# פָּסַח

[pāsaḥ](https://www.blueletterbible.org/lexicon/h6452)

Definition: pass over (4x), halt (1x), become lame (1x), leap (1x).

Part of speech: verb

Occurs 8 times in 8 verses

Hebrew: [pecach](../h/h6453.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6452)

[Study Light](https://www.studylight.org/lexicons/hebrew/6452.html)

[Bible Hub](https://biblehub.com/str/hebrew/6452.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06452)

[Bible Bento](https://biblebento.com/dictionary/H6452.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6452/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6452.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6452)
