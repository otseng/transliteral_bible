# כְּסַף

[kᵊsap̄](https://www.blueletterbible.org/lexicon/h3702)

Definition: silver (12x), money (1x).

Part of speech: masculine noun

Occurs 13 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3702)

[Study Light](https://www.studylight.org/lexicons/hebrew/3702.html)

[Bible Hub](https://biblehub.com/str/hebrew/3702.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03702)

[Bible Bento](https://biblebento.com/dictionary/H3702.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3702/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3702.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3702)
