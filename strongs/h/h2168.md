# זָמַר

[zāmar](https://www.blueletterbible.org/lexicon/h2168)

Definition: prune (3x), trim

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2168)

[Study Light](https://www.studylight.org/lexicons/hebrew/2168.html)

[Bible Hub](https://biblehub.com/str/hebrew/2168.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02168)

[Bible Bento](https://biblebento.com/dictionary/H2168.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2168/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2168.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2168)
