# צַחַר

[ṣaḥar](https://www.blueletterbible.org/lexicon/h6713)

Definition: white (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6713)

[Study Light](https://www.studylight.org/lexicons/hebrew/6713.html)

[Bible Hub](https://biblehub.com/str/hebrew/6713.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06713)

[Bible Bento](https://biblebento.com/dictionary/H6713.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6713/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6713.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6713)
