# פִּרְחָח

[pirḥāḥ](https://www.blueletterbible.org/lexicon/h6526)

Definition: youth (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6526)

[Study Light](https://www.studylight.org/lexicons/hebrew/6526.html)

[Bible Hub](https://biblehub.com/str/hebrew/6526.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06526)

[Bible Bento](https://biblebento.com/dictionary/H6526.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6526/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6526.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6526)
