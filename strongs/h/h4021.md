# מִגְבָּעָה

[miḡbāʿâ](https://www.blueletterbible.org/lexicon/h4021)

Definition: bonnet (4x).

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4021)

[Study Light](https://www.studylight.org/lexicons/hebrew/4021.html)

[Bible Hub](https://biblehub.com/str/hebrew/4021.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04021)

[Bible Bento](https://biblebento.com/dictionary/H4021.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4021/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4021.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4021)
