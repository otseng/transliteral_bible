# צֶפַע

[ṣep̄aʿ](https://www.blueletterbible.org/lexicon/h6848)

Definition: cockatrice (4x), adder (1x), viper, snake, serpent

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6848)

[Study Light](https://www.studylight.org/lexicons/hebrew/6848.html)

[Bible Hub](https://biblehub.com/str/hebrew/6848.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06848)

[Bible Bento](https://biblebento.com/dictionary/H6848.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6848/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6848.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6848)
