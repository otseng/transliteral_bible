# עָסַס

[ʿāsas](https://www.blueletterbible.org/lexicon/h6072)

Definition: tread down (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6072)

[Study Light](https://www.studylight.org/lexicons/hebrew/6072.html)

[Bible Hub](https://biblehub.com/str/hebrew/6072.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06072)

[Bible Bento](https://biblebento.com/dictionary/H6072.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6072/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6072.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6072)
