# צָרַב

[ṣāraḇ](https://www.blueletterbible.org/lexicon/h6866)

Definition: burned (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6866)

[Study Light](https://www.studylight.org/lexicons/hebrew/6866.html)

[Bible Hub](https://biblehub.com/str/hebrew/6866.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06866)

[Bible Bento](https://biblebento.com/dictionary/H6866.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6866/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6866.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6866)
