# נְטֹפָתִי

[Nᵊṭōp̄āṯî](https://www.blueletterbible.org/lexicon/h5200)

Definition: Netophathite (10x), Netophathi (1x).

Part of speech: adjective patrial

Occurs 11 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5200)

[Study Light](https://www.studylight.org/lexicons/hebrew/5200.html)

[Bible Hub](https://biblehub.com/str/hebrew/5200.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05200)

[Bible Bento](https://biblebento.com/dictionary/H5200.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5200/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5200.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5200)
