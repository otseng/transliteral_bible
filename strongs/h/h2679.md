# חֲצִי הַמְּנֻחוֹת

[Ḥăṣî hammᵊnuḥôṯ](https://www.blueletterbible.org/lexicon/h2679)

Definition: Manahethites (1x).

Part of speech: proper masculine collective noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2679)

[Study Light](https://www.studylight.org/lexicons/hebrew/2679.html)

[Bible Hub](https://biblehub.com/str/hebrew/2679.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02679)

[Bible Bento](https://biblebento.com/dictionary/H2679.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2679/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2679.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2679)
