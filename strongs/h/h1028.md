# בֵּית הָרָן

[Bêṯ Hārān](https://www.blueletterbible.org/lexicon/h1028)

Definition: Bethharan (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1028)

[Study Light](https://www.studylight.org/lexicons/hebrew/1028.html)

[Bible Hub](https://biblehub.com/str/hebrew/1028.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01028)

[Bible Bento](https://biblebento.com/dictionary/H1028.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1028/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1028.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1028)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bethharan.html)