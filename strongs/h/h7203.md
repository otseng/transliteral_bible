# רֹאֶה

[rō'ê](https://www.blueletterbible.org/lexicon/h7203)

Definition: vision (1x).

Part of speech: masculine noun

Occurs 6 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7203)

[Study Light](https://www.studylight.org/lexicons/hebrew/7203.html)

[Bible Hub](https://biblehub.com/str/hebrew/7203.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07203)

[Bible Bento](https://biblebento.com/dictionary/H7203.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7203/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7203.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7203)
