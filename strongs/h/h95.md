# אֲגוֹרָה

['ăḡôrâ](https://www.blueletterbible.org/lexicon/h95)

Definition: piece (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h95)

[Study Light](https://www.studylight.org/lexicons/hebrew/95.html)

[Bible Hub](https://biblehub.com/str/hebrew/95.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=095)

[Bible Bento](https://biblebento.com/dictionary/H95.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/95/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/95.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h95)
