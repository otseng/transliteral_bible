# שְׁנָה

[šᵊnâ](https://www.blueletterbible.org/lexicon/h8139)

Definition: sleep (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Hebrew: [šēnā'](../h/h8142.md)

Synonyms: [sleep](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Sleep)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8139)

[Study Light](https://www.studylight.org/lexicons/hebrew/8139.html)

[Bible Hub](https://biblehub.com/str/hebrew/8139.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08139)

[Bible Bento](https://biblebento.com/dictionary/H8139.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8139/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8139.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8139)
