# נִי

[nî](https://www.blueletterbible.org/lexicon/h5204)

Definition: wailing (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5204)

[Study Light](https://www.studylight.org/lexicons/hebrew/5204.html)

[Bible Hub](https://biblehub.com/str/hebrew/5204.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05204)

[Bible Bento](https://biblebento.com/dictionary/H5204.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5204/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5204.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5204)
