# חֵצִי

[ḥēṣî](https://www.blueletterbible.org/lexicon/h2677)

Definition: half (108x), midst (8x), part (4x), midnight (with H3915) (4x), middle (1x).

Part of speech: masculine noun

Occurs 123 times in 101 verses

Greek: [ēmisys](../g/g2255.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2677)

[Study Light](https://www.studylight.org/lexicons/hebrew/2677.html)

[Bible Hub](https://biblehub.com/str/hebrew/2677.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02677)

[Bible Bento](https://biblebento.com/dictionary/H2677.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2677/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2677.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2677)
