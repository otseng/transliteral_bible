# סְבָךְ

[sᵊḇāḵ](https://www.blueletterbible.org/lexicon/h5442)

Definition: thicket (3x), thick (1x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5442)

[Study Light](https://www.studylight.org/lexicons/hebrew/5442.html)

[Bible Hub](https://biblehub.com/str/hebrew/5442.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05442)

[Bible Bento](https://biblebento.com/dictionary/H5442.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5442/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5442.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5442)
