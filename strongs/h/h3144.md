# יוֹשָׁה

[Yôšâ](https://www.blueletterbible.org/lexicon/h3144)

Definition: Joshah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3144)

[Study Light](https://www.studylight.org/lexicons/hebrew/3144.html)

[Bible Hub](https://biblehub.com/str/hebrew/3144.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03144)

[Bible Bento](https://biblebento.com/dictionary/H3144.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3144/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3144.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3144)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/joshah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/joshah)