# קֶמַח

[qemaḥ](https://www.blueletterbible.org/lexicon/h7058)

Definition: meal (10x), flour (4x).

Part of speech: masculine noun

Occurs 14 times in 14 verses

Greek: [aleuron](../g/g224.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7058)

[Study Light](https://www.studylight.org/lexicons/hebrew/7058.html)

[Bible Hub](https://biblehub.com/str/hebrew/7058.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07058)

[Bible Bento](https://biblebento.com/dictionary/H7058.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7058/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7058.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7058)
