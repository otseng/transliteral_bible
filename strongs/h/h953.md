# בּוֹר

[bowr](https://www.blueletterbible.org/lexicon/h953)

Definition: pit (42x), cistern (4x), dungeon (11x), well (9x), dungeon (with H1004) (2x), fountain (1x).

Part of speech: masculine noun

Occurs 69 times in 64 verses

Hebrew: [bᵊ'ēr](../h/h875.md), [bō'r](../h/h877.md)

Greek: [pēgē](../g/g4077.md)

Pictoral: palm of man, dig 

Edenics: bore, bowl

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0953)

[Study Light](https://www.studylight.org/lexicons/hebrew/953.html)

[Bible Hub](https://biblehub.com/str/hebrew/953.htm)

[Morfix](https://www.morfix.co.il/en/%D7%91%D6%BC%D7%95%D6%B9%D7%A8)

[NET Bible](http://classic.net.bible.org/strong.php?id=0953)

[Bible Bento](https://biblebento.com/dictionary/H0953.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0953/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0953.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h953)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%91%D7%95%D7%A8)
