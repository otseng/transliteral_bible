# חָנוּן

[Ḥānûn](https://www.blueletterbible.org/lexicon/h2586)

Definition: Hanun (11x).

Part of speech: proper masculine noun

Occurs 11 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2586)

[Study Light](https://www.studylight.org/lexicons/hebrew/2586.html)

[Bible Hub](https://biblehub.com/str/hebrew/2586.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02586)

[Bible Bento](https://biblebento.com/dictionary/H2586.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2586/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2586.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2586)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hanun.html)

[Video Bible](https://www.videobible.com/bible-dictionary/hanun)