# חָרֵב

[ḥārēḇ](https://www.blueletterbible.org/lexicon/h2720)

Definition: waste (6x), dry (2x), desolate (2x).

Part of speech: adjective

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2720)

[Study Light](https://www.studylight.org/lexicons/hebrew/2720.html)

[Bible Hub](https://biblehub.com/str/hebrew/2720.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02720)

[Bible Bento](https://biblebento.com/dictionary/H2720.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2720/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2720.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2720)
