# מִשְׁקָל

[mišqāl](https://www.blueletterbible.org/lexicon/h4948)

Definition: weight (47x), weigh (2x).

Part of speech: masculine noun

Occurs 49 times in 42 verses

Hebrew: [mišqāl](../h/h4948.md), [šāqal](../h/h8254.md), [šeqel](../h/h8255.md), [tᵊqal](../h/h8625.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4948)

[Study Light](https://www.studylight.org/lexicons/hebrew/4948.html)

[Bible Hub](https://biblehub.com/str/hebrew/4948.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04948)

[Bible Bento](https://biblebento.com/dictionary/H4948.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4948/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4948.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4948)
