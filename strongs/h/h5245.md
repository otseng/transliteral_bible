# נְמַר

[nᵊmar](https://www.blueletterbible.org/lexicon/h5245)

Definition: leopard (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5245)

[Study Light](https://www.studylight.org/lexicons/hebrew/5245.html)

[Bible Hub](https://biblehub.com/str/hebrew/5245.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05245)

[Bible Bento](https://biblebento.com/dictionary/H5245.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5245/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5245.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5245)
