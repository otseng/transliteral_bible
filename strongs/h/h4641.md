# מַעֲשֵׂיָה

[MaʿĂśêâ](https://www.blueletterbible.org/lexicon/h4641)

Definition: Maaseiah (23x).

Part of speech: proper masculine noun

Occurs 23 times in 23 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4641)

[Study Light](https://www.studylight.org/lexicons/hebrew/4641.html)

[Bible Hub](https://biblehub.com/str/hebrew/4641.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04641)

[Bible Bento](https://biblebento.com/dictionary/H4641.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4641/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4641.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4641)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/maaseiah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/maaseiah)