# בֹּכִים

[Bōḵîm](https://www.blueletterbible.org/lexicon/h1066)

Definition: Bochim (2x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1066)

[Study Light](https://www.studylight.org/lexicons/hebrew/1066.html)

[Bible Hub](https://biblehub.com/str/hebrew/1066.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01066)

[Bible Bento](https://biblebento.com/dictionary/H1066.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1066/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1066.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1066)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bochim.html)