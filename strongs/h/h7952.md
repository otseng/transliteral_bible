# שָׁלָה

[šālâ](https://www.blueletterbible.org/lexicon/h7952)

Definition: negligent (1x), deceive (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7952)

[Study Light](https://www.studylight.org/lexicons/hebrew/7952.html)

[Bible Hub](https://biblehub.com/str/hebrew/7952.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07952)

[Bible Bento](https://biblebento.com/dictionary/H7952.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7952/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7952.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7952)
