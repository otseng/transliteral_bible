# קֶדֶם

[qeḏem](https://www.blueletterbible.org/lexicon/h6924)

Definition: east (32x), old (17x), eastward (11x), ancient (6x), east side (5x), before (3x), east part (2x), ancient time (2x), aforetime (1x), eternal (1x), miscellaneous (7x).

Part of speech: adverb, masculine noun

Occurs 87 times in 83 verses

Hebrew: [mizrach](../h/h4217.md), [qāḏîm](../h/h6921.md), [qadam](../h/h6923.md), [qŏḏām](../h/h6925.md), [qḏmh](../h/h6926.md), [qaḏmâ](../h/h6927.md), [qaḏmâ](../h/h6928.md)

Greek: [anatolē](../g/g395.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6924)

[Study Light](https://www.studylight.org/lexicons/hebrew/6924.html)

[Bible Hub](https://biblehub.com/str/hebrew/6924.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06924)

[Bible Bento](https://biblebento.com/dictionary/H6924.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6924/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6924.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6924)
