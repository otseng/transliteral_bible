# יְתִב

[yᵊṯiḇ](https://www.blueletterbible.org/lexicon/h3488)

Definition: sit (4x), dwell (1x).

Part of speech: verb

Occurs 5 times in 5 verses

Hebrew: [yashab](../h/h3427.md), [môšāḇ](../h/h4186.md), [šîḇâ](../h/h7870.md), [tôšāḇ](../h/h8453.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3488)

[Study Light](https://www.studylight.org/lexicons/hebrew/3488.html)

[Bible Hub](https://biblehub.com/str/hebrew/3488.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03488)

[Bible Bento](https://biblebento.com/dictionary/H3488.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3488/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3488.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3488)
