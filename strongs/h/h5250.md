# נִמְשִׁי

[Nimšî](https://www.blueletterbible.org/lexicon/h5250)

Definition: Nimshi (5x).

Part of speech: proper masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5250)

[Study Light](https://www.studylight.org/lexicons/hebrew/5250.html)

[Bible Hub](https://biblehub.com/str/hebrew/5250.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05250)

[Bible Bento](https://biblebento.com/dictionary/H5250.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5250/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5250.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5250)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/nimshi.html)

[Video Bible](https://www.videobible.com/bible-dictionary/nimshi)