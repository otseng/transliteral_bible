# כּוּשִׁי

[Kûšî](https://www.blueletterbible.org/lexicon/h3569)

Definition: Ethiopian (15x), Cushi (8x).

Part of speech: adjective

Occurs 23 times in 19 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3569)

[Study Light](https://www.studylight.org/lexicons/hebrew/3569.html)

[Bible Hub](https://biblehub.com/str/hebrew/3569.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03569)

[Bible Bento](https://biblebento.com/dictionary/H3569.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3569/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3569.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3569)
