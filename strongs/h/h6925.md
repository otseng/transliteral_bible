# קֳדָם

[qŏḏām](https://www.blueletterbible.org/lexicon/h6925)

Definition: before (29x), before (with H4481) (2x), of (with H4481) (2x), him (2x), miscellaneous (7x).

Part of speech: preposition

Occurs 46 times in 38 verses

Hebrew: [qāḏîm](../h/h6921.md), [qadam](../h/h6923.md), [qeḏem](../h/h6924.md), [qḏmh](../h/h6926.md), [qaḏmâ](../h/h6927.md), [qaḏmâ](../h/h6928.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6925)

[Study Light](https://www.studylight.org/lexicons/hebrew/6925.html)

[Bible Hub](https://biblehub.com/str/hebrew/6925.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06925)

[Bible Bento](https://biblebento.com/dictionary/H6925.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6925/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6925.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6925)
