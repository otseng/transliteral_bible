# יָקִים

[Yāqîm](https://www.blueletterbible.org/lexicon/h3356)

Definition: Jakim (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3356)

[Study Light](https://www.studylight.org/lexicons/hebrew/3356.html)

[Bible Hub](https://biblehub.com/str/hebrew/3356.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03356)

[Bible Bento](https://biblebento.com/dictionary/H3356.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3356/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3356.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3356)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jakim.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jakim)