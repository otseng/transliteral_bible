# חִבֻּק

[ḥibuq](https://www.blueletterbible.org/lexicon/h2264)

Definition: folding (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2264)

[Study Light](https://www.studylight.org/lexicons/hebrew/2264.html)

[Bible Hub](https://biblehub.com/str/hebrew/2264.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02264)

[Bible Bento](https://biblebento.com/dictionary/H2264.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2264/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2264.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2264)
