# שֵׁאת

[šē'ṯ](https://www.blueletterbible.org/lexicon/h7612)

Definition: desolation (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7612)

[Study Light](https://www.studylight.org/lexicons/hebrew/7612.html)

[Bible Hub](https://biblehub.com/str/hebrew/7612.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07612)

[Bible Bento](https://biblebento.com/dictionary/H7612.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7612/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7612.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7612)
