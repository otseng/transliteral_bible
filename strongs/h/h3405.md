# יְרֵחוֹ

[Yᵊrēḥô](https://www.blueletterbible.org/lexicon/h3405)

Definition: Jericho (57x).7, "city of the moon", "fragrant"

Part of speech: proper locative noun

Occurs 57 times in 53 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3405)

[Study Light](https://www.studylight.org/lexicons/hebrew/3405.html)

[Bible Hub](https://biblehub.com/str/hebrew/3405.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03405)

[Bible Bento](https://biblebento.com/dictionary/H3405.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3405/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3405.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3405)

[Wikipedia](https://en.wikipedia.org/wiki/Jericho)

[Britannica](https://www.britannica.com/place/Jericho-West-Bank)

[Jewish Virtual Library](https://www.jewishvirtuallibrary.org/jericho)

[Khan Academy](https://www.khanacademy.org/humanities/big-history-project/agriculture-civilization/first-cities-states/a/jericho-7)

[Bible Places](https://www.bibleplaces.com/jericho/)

[World History](https://www.worldhistory.org/article/951/early-jericho/)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jericho.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jericho)