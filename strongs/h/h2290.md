# חֲגוֹר

[chagowr](https://www.blueletterbible.org/lexicon/h2290)

Definition: girdle (3x), apron (1x), armour (1x), gird (1x)

Part of speech: proper masculine noun

Occurs 6 times in 6 verses

Hebrew: [ḥăḡôr](../h/h2289.md), [ḥāḡar](../h/h2296.md)

Greek: [zōnē](../g/g2223.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2290)

[Study Light](https://www.studylight.org/lexicons/hebrew/2290.html)

[Bible Hub](https://biblehub.com/str/hebrew/2290.htm)

[Morfix](https://www.morfix.co.il/en/%D7%97%D6%B2%D7%92%D7%95%D6%B9%D7%A8)

[NET Bible](http://classic.net.bible.org/strong.php?id=02290)

[Bible Bento](https://biblebento.com/dictionary/H2290.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2290/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2290.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2290)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%97%D7%92%D7%95%D7%A8)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/girdle.html)