# דּוּחַ

[dûaḥ](https://www.blueletterbible.org/lexicon/h1740)

Definition: wash (2x), purge (1x), cast me out (1x).

Part of speech: verb

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1740)

[Study Light](https://www.studylight.org/lexicons/hebrew/1740.html)

[Bible Hub](https://biblehub.com/str/hebrew/1740.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01740)

[Bible Bento](https://biblebento.com/dictionary/H1740.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1740/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1740.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1740)
