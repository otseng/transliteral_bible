# פֶּלֶא

[pele'](https://www.blueletterbible.org/lexicon/h6382)

Definition: wonder (8x), wonderful (3x), wonderfully (1x), marvellous (1x).

Part of speech: masculine noun

Occurs 13 times in 13 verses

Hebrew: [pala'](../h/h6381.md)

Greek: [thaumastos](../g/g2298.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6382)

[Study Light](https://www.studylight.org/lexicons/hebrew/6382.html)

[Bible Hub](https://biblehub.com/str/hebrew/6382.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06382)

[Bible Bento](https://biblebento.com/dictionary/H6382.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6382/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6382.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6382)
