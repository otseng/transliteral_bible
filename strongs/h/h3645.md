# כְּמוֹשׁ

[kᵊmôš](https://www.blueletterbible.org/lexicon/h3645)

Definition: Chemosh (8x).

Part of speech: proper noun with reference to deity

Occurs 9 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3645)

[Study Light](https://www.studylight.org/lexicons/hebrew/3645.html)

[Bible Hub](https://biblehub.com/str/hebrew/3645.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03645)

[Bible Bento](https://biblebento.com/dictionary/H3645.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3645/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3645.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3645)
