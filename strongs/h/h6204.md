# עָרְפָּה

[ʿĀrpâ](https://www.blueletterbible.org/lexicon/h6204)

Definition: Orpah (2x).

Part of speech: proper feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6204)

[Study Light](https://www.studylight.org/lexicons/hebrew/6204.html)

[Bible Hub](https://biblehub.com/str/hebrew/6204.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06204)

[Bible Bento](https://biblebento.com/dictionary/H6204.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6204/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6204.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6204)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/O/orpah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/orpah)