# כְּרָן

[kᵊrān](https://www.blueletterbible.org/lexicon/h3763)

Definition: Cheran (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3763)

[Study Light](https://www.studylight.org/lexicons/hebrew/3763.html)

[Bible Hub](https://biblehub.com/str/hebrew/3763.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03763)

[Bible Bento](https://biblebento.com/dictionary/H3763.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3763/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3763.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3763)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/cheran.html)

[Video Bible](https://www.videobible.com/bible-dictionary/cheran)