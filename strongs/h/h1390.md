# גִּבְעָה

[giḇʿâ](https://www.blueletterbible.org/lexicon/h1390)

Definition: Gibeah (44x), "hill"

Part of speech: proper locative noun

Occurs 43 times in 42 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1390)

[Study Light](https://www.studylight.org/lexicons/hebrew/1390.html)

[Bible Hub](https://biblehub.com/str/hebrew/1390.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01390)

[Bible Bento](https://biblebento.com/dictionary/H1390.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1390/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1390.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1390)

[Wikipedia](https://en.wikipedia.org/wiki/Gibeah)

[Britannica](https://www.britannica.com/place/Gibeah)

[Bible Atlas](https://bibleatlas.org/gibeah.htm)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/gibeah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/gibeah)