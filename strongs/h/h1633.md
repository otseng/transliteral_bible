# גָּרַם

[gāram](https://www.blueletterbible.org/lexicon/h1633)

Definition: break (2x), gnaw bones (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1633)

[Study Light](https://www.studylight.org/lexicons/hebrew/1633.html)

[Bible Hub](https://biblehub.com/str/hebrew/1633.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01633)

[Bible Bento](https://biblebento.com/dictionary/H1633.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1633/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1633.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1633)
