# שִׂיב

[śîḇ](https://www.blueletterbible.org/lexicon/h7867)

Definition: grayheaded (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7867)

[Study Light](https://www.studylight.org/lexicons/hebrew/7867.html)

[Bible Hub](https://biblehub.com/str/hebrew/7867.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07867)

[Bible Bento](https://biblebento.com/dictionary/H7867.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7867/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7867.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7867)
