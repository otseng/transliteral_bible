# בִּאוּשׁ

[bi'ûš](https://www.blueletterbible.org/lexicon/h873)

Definition: bad (1x).

Part of speech: feminine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h873)

[Study Light](https://www.studylight.org/lexicons/hebrew/873.html)

[Bible Hub](https://biblehub.com/str/hebrew/873.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0873)

[Bible Bento](https://biblebento.com/dictionary/H873.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/873/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/873.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h873)
