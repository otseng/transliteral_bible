# זָרַח

[zāraḥ](https://www.blueletterbible.org/lexicon/h2224)

Definition: arise (8x), rise (6x), rise up (2x), shine (1x), up (1x).

Part of speech: verb

Occurs 18 times in 17 verses

Hebrew: [zeraḥ](../h/h2225.md), [mizrach](../h/h4217.md)

Greek: [anatellō](../g/g393.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2224)

[Study Light](https://www.studylight.org/lexicons/hebrew/2224.html)

[Bible Hub](https://biblehub.com/str/hebrew/2224.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02224)

[Bible Bento](https://biblebento.com/dictionary/H2224.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2224/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2224.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2224)
