# שֶׁמַע

[Šemaʿ](https://www.blueletterbible.org/lexicon/h8087)

Definition: Shema (5x).

Part of speech: proper masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8087)

[Study Light](https://www.studylight.org/lexicons/hebrew/8087.html)

[Bible Hub](https://biblehub.com/str/hebrew/8087.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08087)

[Bible Bento](https://biblebento.com/dictionary/H8087.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8087/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8087.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8087)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shema.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shema)