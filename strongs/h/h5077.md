# נָדָה

[nāḏâ](https://www.blueletterbible.org/lexicon/h5077)

Definition: cast you out (1x), put far away (1x), drive (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5077)

[Study Light](https://www.studylight.org/lexicons/hebrew/5077.html)

[Bible Hub](https://biblehub.com/str/hebrew/5077.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05077)

[Bible Bento](https://biblebento.com/dictionary/H5077.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5077/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5077.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5077)
