# יַפְלֵטִי

[yap̄lēṭî](https://www.blueletterbible.org/lexicon/h3311)

Definition: Japhleti (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3311)

[Study Light](https://www.studylight.org/lexicons/hebrew/3311.html)

[Bible Hub](https://biblehub.com/str/hebrew/3311.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03311)

[Bible Bento](https://biblebento.com/dictionary/H3311.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3311/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3311.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3311)
