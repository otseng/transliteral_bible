# נָצִיר

[nāṣîr](https://www.blueletterbible.org/lexicon/h5336)

Definition: preserved (1x)

Part of speech: adjective

Occurs 1 times in 1 verses

Hebrew: [nāṣar](../h/h5341.md), [nēṣer](../h/h5342.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5336)

[Study Light](https://www.studylight.org/lexicons/hebrew/5336.html)

[Bible Hub](https://biblehub.com/str/hebrew/5336.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05336)

[Bible Bento](https://biblebento.com/dictionary/H5336.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5336/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5336.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5336)
