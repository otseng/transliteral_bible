# מִשְׂרְפוֹת מַיִם

[Miśrᵊp̄Ôṯ Mayim](https://www.blueletterbible.org/lexicon/h4956)

Definition: Misrephothmaim (2x).

Part of speech: proper locative noun

Occurs 4 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4956)

[Study Light](https://www.studylight.org/lexicons/hebrew/4956.html)

[Bible Hub](https://biblehub.com/str/hebrew/4956.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04956)

[Bible Bento](https://biblebento.com/dictionary/H4956.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4956/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4956.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4956)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/misrephothmaim.html)