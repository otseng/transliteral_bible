# מִסְתָּר

[mictar](https://www.blueletterbible.org/lexicon/h4565)

Definition: secret places (7x), secretly (2x), secret (1x).

Part of speech: masculine noun

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4565)

[Study Light](https://www.studylight.org/lexicons/hebrew/4565.html)

[Bible Hub](https://biblehub.com/str/hebrew/4565.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04565)

[Bible Bento](https://biblebento.com/dictionary/H4565.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4565/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4565.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4565)
