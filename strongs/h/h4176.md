# מוֹרֶה

[Môrê](https://www.blueletterbible.org/lexicon/h4176)

Definition: Moreh (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4176)

[Study Light](https://www.studylight.org/lexicons/hebrew/4176.html)

[Bible Hub](https://biblehub.com/str/hebrew/4176.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04176)

[Bible Bento](https://biblebento.com/dictionary/H4176.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4176/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4176.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4176)
