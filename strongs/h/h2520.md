# חֶלְקָת

[Ḥelqāṯ](https://www.blueletterbible.org/lexicon/h2520)

Definition: Helkath (2x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2520)

[Study Light](https://www.studylight.org/lexicons/hebrew/2520.html)

[Bible Hub](https://biblehub.com/str/hebrew/2520.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02520)

[Bible Bento](https://biblebento.com/dictionary/H2520.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2520/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2520.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2520)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/helkath.html)

[Video Bible](https://www.videobible.com/bible-dictionary/helkath)