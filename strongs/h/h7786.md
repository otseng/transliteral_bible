# שׂוּר

[śûr](https://www.blueletterbible.org/lexicon/h7786)

Definition: reign (1x), have power (1x), made prince (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7786)

[Study Light](https://www.studylight.org/lexicons/hebrew/7786.html)

[Bible Hub](https://biblehub.com/str/hebrew/7786.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07786)

[Bible Bento](https://biblebento.com/dictionary/H7786.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7786/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7786.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7786)
