# יְרוּשְׁלֵם

[Yᵊrûšlēm](https://www.blueletterbible.org/lexicon/h3390)

Definition: Jerusalem (26x), "teaching of peace", the chief city of Palestine and capital of the united kingdom and the nation of Judah after the split

Part of speech: proper locative noun

Occurs 26 times in 25 verses

Hebrew: [yārâ](../h/h3384.md), [shalam](../h/h7999.md)

Names: [Yĕruwshalaim](../h/h3389.md)

Greek: [Hierosolyma](../g/g2414.md), [Ierousalēm](../g/g2419.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3390)

[Study Light](https://www.studylight.org/lexicons/hebrew/3390.html)

[Bible Hub](https://biblehub.com/str/hebrew/3390.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03390)

[Bible Bento](https://biblebento.com/dictionary/H3390.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3390/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3390.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3390)

[Wikipedia](https://en.wikipedia.org/wiki/Jerusalem)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jerusalem.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jerusalem)