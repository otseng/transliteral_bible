# רָעָב

[rāʿāḇ](https://www.blueletterbible.org/lexicon/h7458)

Definition: famine (87x), hunger (8x), dearth (5x), famished (1x).

Part of speech: masculine noun

Occurs 101 times in 88 verses

Hebrew: [rāʿēḇ](../h/h7456.md), [rāʿēḇ](../h/h7457.md), [rᵊʿāḇôn](../h/h7459.md)

Greek: [limos](../g/g3042.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7458)

[Study Light](https://www.studylight.org/lexicons/hebrew/7458.html)

[Bible Hub](https://biblehub.com/str/hebrew/7458.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07458)

[Bible Bento](https://biblebento.com/dictionary/H7458.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7458/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7458.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7458)
