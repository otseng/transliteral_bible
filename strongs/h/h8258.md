# שְׁקַעֲרוּרָה

[šᵊqaʿărûrâ](https://www.blueletterbible.org/lexicon/h8258)

Definition: hollow strakes (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8258)

[Study Light](https://www.studylight.org/lexicons/hebrew/8258.html)

[Bible Hub](https://biblehub.com/str/hebrew/8258.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08258)

[Bible Bento](https://biblebento.com/dictionary/H8258.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8258/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8258.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8258)
