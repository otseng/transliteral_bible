# שׁוּף

[shuwph](https://www.blueletterbible.org/lexicon/h7779)

Definition: bruise (2x), break (1x), cover (1x), crush

Part of speech: verb

Occurs 4 times in 3 verses

Synonyms: [cover](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Cover)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7779)

[Study Light](https://www.studylight.org/lexicons/hebrew/7779.html)

[Bible Hub](https://biblehub.com/str/hebrew/7779.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A9%D7%81%D7%95%D6%BC%D7%A3)

[NET Bible](http://classic.net.bible.org/strong.php?id=07779)

[Bible Bento](https://biblebento.com/dictionary/H7779.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7779/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7779.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7779)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A9%D7%95%D7%A3)
