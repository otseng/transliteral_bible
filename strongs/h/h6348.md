# פָּחַז

[pāḥaz](https://www.blueletterbible.org/lexicon/h6348)

Definition: light (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6348)

[Study Light](https://www.studylight.org/lexicons/hebrew/6348.html)

[Bible Hub](https://biblehub.com/str/hebrew/6348.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06348)

[Bible Bento](https://biblebento.com/dictionary/H6348.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6348/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6348.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6348)
