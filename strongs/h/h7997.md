# שָׁלַל

[šālal](https://www.blueletterbible.org/lexicon/h7997)

Definition: spoil (8x), take (5x), fall (1x), prey (1x), purpose (1x).

Part of speech: verb

Occurs 16 times in 12 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7997)

[Study Light](https://www.studylight.org/lexicons/hebrew/7997.html)

[Bible Hub](https://biblehub.com/str/hebrew/7997.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07997)

[Bible Bento](https://biblebento.com/dictionary/H7997.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7997/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7997.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7997)
