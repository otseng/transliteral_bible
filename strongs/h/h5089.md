# נֹהַּ

[nōha](https://www.blueletterbible.org/lexicon/h5089)

Definition: wailing (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5089)

[Study Light](https://www.studylight.org/lexicons/hebrew/5089.html)

[Bible Hub](https://biblehub.com/str/hebrew/5089.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05089)

[Bible Bento](https://biblebento.com/dictionary/H5089.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5089/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5089.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5089)
