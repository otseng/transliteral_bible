# יְהוּדַי

[yᵊhûḏay](https://www.blueletterbible.org/lexicon/h3062)

Definition: Jews (10x).

Part of speech: proper plural noun

Occurs 10 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3062)

[Study Light](https://www.studylight.org/lexicons/hebrew/3062.html)

[Bible Hub](https://biblehub.com/str/hebrew/3062.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03062)

[Bible Bento](https://biblebento.com/dictionary/H3062.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3062/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3062.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3062)
