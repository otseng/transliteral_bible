# תּוֹרָה

[tôrâ](https://www.blueletterbible.org/lexicon/h8452)

Definition: manner (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Hebrew: [tûr](../h/h8446.md), [tôr](../h/h8447.md), [tôr](../h/h8448.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8452)

[Study Light](https://www.studylight.org/lexicons/hebrew/8452.html)

[Bible Hub](https://biblehub.com/str/hebrew/8452.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08452)

[Bible Bento](https://biblebento.com/dictionary/H8452.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8452/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8452.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8452)
