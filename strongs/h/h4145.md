# מוּסָדָה

[mûsāḏâ](https://www.blueletterbible.org/lexicon/h4145)

Definition: grounded (1x), foundation, appointment

Part of speech: feminine noun

Occurs 2 times in 2 verses

Hebrew: [yacad](../h/h3245.md), [mûsāḏ](../h/h4143.md), [môsāḏ](../h/h4144.md), [mowcadah](../h/h4146.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4145)

[Study Light](https://www.studylight.org/lexicons/hebrew/4145.html)

[Bible Hub](https://biblehub.com/str/hebrew/4145.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04145)

[Bible Bento](https://biblebento.com/dictionary/H4145.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4145/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4145.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4145)
