# מוּעָקָה

[mûʿāqâ](https://www.blueletterbible.org/lexicon/h4157)

Definition: affliction (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4157)

[Study Light](https://www.studylight.org/lexicons/hebrew/4157.html)

[Bible Hub](https://biblehub.com/str/hebrew/4157.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04157)

[Bible Bento](https://biblebento.com/dictionary/H4157.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4157/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4157.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4157)
