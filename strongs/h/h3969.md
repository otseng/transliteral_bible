# מְאָה

[mᵊ'â](https://www.blueletterbible.org/lexicon/h3969)

Definition: hundred (8x).

Part of speech: feminine noun

Occurs 8 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3969)

[Study Light](https://www.studylight.org/lexicons/hebrew/3969.html)

[Bible Hub](https://biblehub.com/str/hebrew/3969.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03969)

[Bible Bento](https://biblebento.com/dictionary/H3969.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3969/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3969.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3969)
