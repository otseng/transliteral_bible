# בְּדֹלַח

[bᵊḏōlaḥ](https://www.blueletterbible.org/lexicon/h916)

Definition: bdellium (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0916)

[Study Light](https://www.studylight.org/lexicons/hebrew/916.html)

[Bible Hub](https://biblehub.com/str/hebrew/916.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0916)

[Bible Bento](https://biblebento.com/dictionary/H916.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/916/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/916.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h916)
