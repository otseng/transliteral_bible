# רְפָאִים

[rᵊp̄ā'îm](https://www.blueletterbible.org/lexicon/h7496)

Definition: dead (7x), deceased (1x).

Part of speech: masculine plural noun

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7496)

[Study Light](https://www.studylight.org/lexicons/hebrew/7496.html)

[Bible Hub](https://biblehub.com/str/hebrew/7496.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07496)

[Bible Bento](https://biblebento.com/dictionary/H7496.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7496/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7496.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7496)
