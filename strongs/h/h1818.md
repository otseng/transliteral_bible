# דָּם

[dam](https://www.blueletterbible.org/lexicon/h1818)

Definition: blood (342x), bloody (15x), person (with H5315) (1x), bloodguiltiness (1x), bloodthirsty (with H582) (1x), life, liquid

Part of speech: masculine noun

Occurs 361 times in 295 verses

Hebrew: [damam](../h/h1826.md)

Greek: [haima](../g/g129.md)

Edenics: dawn, damp, damn, dong (Chinese - east)

Pictoral: moving liquid

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1818)

[Study Light](https://www.studylight.org/lexicons/hebrew/1818.html)

[Bible Hub](https://biblehub.com/str/hebrew/1818.htm)

[Morfix](https://www.morfix.co.il/en/%D7%93%D6%BC%D6%B8%D7%9D)

[NET Bible](http://classic.net.bible.org/strong.php?id=01818)

[Bible Bento](https://biblebento.com/dictionary/H1818.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1818/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1818.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1818)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%93%D7%9D)

## Notes

One of oldest words. [Proto-Semitic](https://en.wiktionary.org/wiki/Reconstruction:Proto-Semitic/dam-) 