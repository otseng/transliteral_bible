# חֵמָה

[chemah](https://www.blueletterbible.org/lexicon/h2534)

Definition: fury (67x), wrath (34x), poison (6x), furious (4x), displeasure (3x), rage (2x), anger (1x), bottles (1x), furious (with H1167) (1x), furiously (1x), heat (1x), indignation (1x), wrathful (1x), wroth (1x).

Part of speech: feminine noun

Occurs 124 times in 117 verses

Hebrew: [yāḥam](../h/h3179.md)

Greek: [thymos](../g/g2372.md), [orgē](../g/g3709.md)

Synonyms: [wrath](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Wrath)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2534)

[Study Light](https://www.studylight.org/lexicons/hebrew/2534.html)

[Bible Hub](https://biblehub.com/str/hebrew/2534.htm)

[Morfix](https://www.morfix.co.il/%D7%97%D6%B5%D7%9E%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=02534)

[Bible Bento](https://biblebento.com/dictionary/H2534.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2534/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2534.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2534)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%97%D7%9E%D7%94)
