# שָׁחַל

[šāḥal](https://www.blueletterbible.org/lexicon/h7826)

Definition: lion (4x), fierce lion (3x).

Part of speech: masculine noun

Occurs 7 times in 7 verses

Greek: [leōn](../g/g3023.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7826)

[Study Light](https://www.studylight.org/lexicons/hebrew/7826.html)

[Bible Hub](https://biblehub.com/str/hebrew/7826.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07826)

[Bible Bento](https://biblebento.com/dictionary/H7826.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7826/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7826.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7826)
