# פְּרַס

[pᵊras](https://www.blueletterbible.org/lexicon/h6537)

Definition: UPHARSIN (1x), PERES (1x), divided (1x).

Part of speech: masculine noun, verb

Occurs 3 times in 2 verses

Hebrew: [pāras](../h/h6536.md), [pāraś](../h/h6566.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6537)

[Study Light](https://www.studylight.org/lexicons/hebrew/6537.html)

[Bible Hub](https://biblehub.com/str/hebrew/6537.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06537)

[Bible Bento](https://biblebento.com/dictionary/H6537.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6537/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6537.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6537)
