# קָיָא

[qāyā'](https://www.blueletterbible.org/lexicon/h7006)

Definition: spue (1x), vomit, vomit up, disgorge

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [qîqāyôn](../h/h7021.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7006)

[Study Light](https://www.studylight.org/lexicons/hebrew/7006.html)

[Bible Hub](https://biblehub.com/str/hebrew/7006.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07006)

[Bible Bento](https://biblebento.com/dictionary/H7006.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7006/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7006.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7006)
