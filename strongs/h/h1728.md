# דּוּג

[dûḡ](https://www.blueletterbible.org/lexicon/h1728)

Definition: fisher (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1728)

[Study Light](https://www.studylight.org/lexicons/hebrew/1728.html)

[Bible Hub](https://biblehub.com/str/hebrew/1728.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01728)

[Bible Bento](https://biblebento.com/dictionary/H1728.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1728/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1728.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1728)
