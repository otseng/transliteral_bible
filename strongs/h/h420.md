# אֶלְדָּעָה

['Eldāʿâ](https://www.blueletterbible.org/lexicon/h420)

Definition: Eldaah (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0420)

[Study Light](https://www.studylight.org/lexicons/hebrew/420.html)

[Bible Hub](https://biblehub.com/str/hebrew/420.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0420)

[Bible Bento](https://biblebento.com/dictionary/H420.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/420/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/420.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h420)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eldaah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/eldaah)