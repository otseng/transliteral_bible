# גִּבְלִי

[giḇlî](https://www.blueletterbible.org/lexicon/h1382)

Definition: Giblites (1x), stonesquarers (1x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1382)

[Study Light](https://www.studylight.org/lexicons/hebrew/1382.html)

[Bible Hub](https://biblehub.com/str/hebrew/1382.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01382)

[Bible Bento](https://biblebento.com/dictionary/H1382.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1382/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1382.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1382)
