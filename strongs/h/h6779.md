# צָמַח

[ṣāmaḥ](https://www.blueletterbible.org/lexicon/h6779)

Definition: grow (13x), spring forth (6x), spring up (4x), grow up (2x), bring forth (2x), bud (2x), spring out (2x), beareth (1x), bud forth (1x).

Part of speech: verb

Occurs 33 times in 32 verses

Hebrew: [ṣemaḥ](../h/h6780.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6779)

[Study Light](https://www.studylight.org/lexicons/hebrew/6779.html)

[Bible Hub](https://biblehub.com/str/hebrew/6779.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06779)

[Bible Bento](https://biblebento.com/dictionary/H6779.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6779/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6779.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6779)
