# כָּהֵן

[kāhēn](https://www.blueletterbible.org/lexicon/h3549)

Definition: priest (8x).

Part of speech: masculine noun

Occurs 8 times in 8 verses

Hebrew: [kōhēn](../h/h3548.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3549)

[Study Light](https://www.studylight.org/lexicons/hebrew/3549.html)

[Bible Hub](https://biblehub.com/str/hebrew/3549.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03549)

[Bible Bento](https://biblebento.com/dictionary/H3549.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3549/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3549.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3549)
