# פָּגַשׁ

[pāḡaš](https://www.blueletterbible.org/lexicon/h6298)

Definition: meet (14x), join, encounter

Part of speech: verb

Occurs 14 times in 14 verses

Greek: [synantaō](../g/g4876.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6298)

[Study Light](https://www.studylight.org/lexicons/hebrew/6298.html)

[Bible Hub](https://biblehub.com/str/hebrew/6298.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06298)

[Bible Bento](https://biblebento.com/dictionary/H6298.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6298/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6298.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6298)
