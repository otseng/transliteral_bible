# בֵּית־דָּגוֹן

[Bêṯ-Dāḡôn](https://www.blueletterbible.org/lexicon/h1016)

Definition: Bethdagon (2x).

Part of speech: proper locative noun

Occurs 4 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1016)

[Study Light](https://www.studylight.org/lexicons/hebrew/1016.html)

[Bible Hub](https://biblehub.com/str/hebrew/1016.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01016)

[Bible Bento](https://biblebento.com/dictionary/H1016.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1016/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1016.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1016)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bethdagon.html)