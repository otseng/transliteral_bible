# מוֹרָט

[môrāṭ](https://www.blueletterbible.org/lexicon/h4178)

Definition: peeled (2x), furbished (2x), bright (1x), scoured, polished, smooth

Part of speech: participle

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4178)

[Study Light](https://www.studylight.org/lexicons/hebrew/4178.html)

[Bible Hub](https://biblehub.com/str/hebrew/4178.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04178)

[Bible Bento](https://biblebento.com/dictionary/H4178.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4178/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4178.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4178)
