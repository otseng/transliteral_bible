# מַסֵּכָה

[massēḵâ](https://www.blueletterbible.org/lexicon/h4541)

Definition: image (18x), molten (7x), covering (2x), vail (1x), a pouring, libation, molten metal, cast image, drink offering 

Part of speech: feminine noun

Occurs 28 times in 28 verses

Hebrew: [nacak](../h/h5258.md)

Greek: [eidōlon](../g/g1497.md), [eikōn](../g/g1504.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4541)

[Study Light](https://www.studylight.org/lexicons/hebrew/4541.html)

[Bible Hub](https://biblehub.com/str/hebrew/4541.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04541)

[Bible Bento](https://biblebento.com/dictionary/H4541.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4541/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4541.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4541)
