# גָּדִי

[Gāḏî](https://www.blueletterbible.org/lexicon/h1424)

Definition: Gadi (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1424)

[Study Light](https://www.studylight.org/lexicons/hebrew/1424.html)

[Bible Hub](https://biblehub.com/str/hebrew/1424.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01424)

[Bible Bento](https://biblebento.com/dictionary/H1424.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1424/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1424.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1424)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/gadi.html)

[Video Bible](https://www.videobible.com/bible-dictionary/gadi)