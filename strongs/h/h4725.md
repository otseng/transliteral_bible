# מָקוֹם

[maqowm](https://www.blueletterbible.org/lexicon/h4725)

Definition: place (391x), home (3x), room (3x), whithersoever (2x), open (1x), space (1x), country (1x).

Part of speech: masculine noun

Occurs 402 times in 379 verses

Hebrew: [qômâ](../h/h6967.md), [qāmâ](../h/h7054.md), [tᵊqûmâ](../h/h8617.md)

Greek: [topos](../g/g5117.md)

Edenics: market

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4725)

[Study Light](https://www.studylight.org/lexicons/hebrew/4725.html)

[Bible Hub](https://biblehub.com/str/hebrew/4725.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%9E%D7%A7%D7%95%D7%9D)

[NET Bible](http://classic.net.bible.org/strong.php?id=04725)

[Bible Bento](https://biblebento.com/dictionary/H4725.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4725/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4725.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4725)