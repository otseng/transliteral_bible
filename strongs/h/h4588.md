# מָעוּף

[māʿûp̄](https://www.blueletterbible.org/lexicon/h4588)

Definition: dimness (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4588)

[Study Light](https://www.studylight.org/lexicons/hebrew/4588.html)

[Bible Hub](https://biblehub.com/str/hebrew/4588.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04588)

[Bible Bento](https://biblebento.com/dictionary/H4588.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4588/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4588.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4588)
