# אַיֶּלֶת

['ayyeleṯ](https://www.blueletterbible.org/lexicon/h365)

Definition: hind (2x), Aijeleth (1x), doe, deer

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h365)

[Study Light](https://www.studylight.org/lexicons/hebrew/365.html)

[Bible Hub](https://biblehub.com/str/hebrew/365.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0365)

[Bible Bento](https://biblebento.com/dictionary/H365.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/365/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/365.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h365)
