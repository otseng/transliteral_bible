# בָּכַר

[bāḵar](https://www.blueletterbible.org/lexicon/h1069)

Definition: firstborn (1x), new fruit (1x), firstling (1x), first child.

Part of speech: verb

Occurs 4 times in 4 verses

Greek: [ginomai](../g/g1096.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1069)

[Study Light](https://www.studylight.org/lexicons/hebrew/1069.html)

[Bible Hub](https://biblehub.com/str/hebrew/1069.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01069)

[Bible Bento](https://biblebento.com/dictionary/H1069.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1069/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1069.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1069)
