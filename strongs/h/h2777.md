# חַרְסוּת

[ḥarsûṯ](https://www.blueletterbible.org/lexicon/h2777)

Definition: east (1x).

Part of speech: feminine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2777)

[Study Light](https://www.studylight.org/lexicons/hebrew/2777.html)

[Bible Hub](https://biblehub.com/str/hebrew/2777.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02777)

[Bible Bento](https://biblebento.com/dictionary/H2777.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2777/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2777.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2777)
