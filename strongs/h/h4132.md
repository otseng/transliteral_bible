# מוֹט

[môṭ](https://www.blueletterbible.org/lexicon/h4132)

Definition: bar (2x), moved (2x), upon a staff (1x), yoke (1x).

Part of speech: masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4132)

[Study Light](https://www.studylight.org/lexicons/hebrew/4132.html)

[Bible Hub](https://biblehub.com/str/hebrew/4132.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04132)

[Bible Bento](https://biblebento.com/dictionary/H4132.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4132/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4132.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4132)
