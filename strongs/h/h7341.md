# רֹחַב

[rōḥaḇ](https://www.blueletterbible.org/lexicon/h7341)

Definition: breadth (74x), broad (21x), thickness (2x), largeness (1x), thick (1x), as broad as (with H3651) (1x), wideness (1x).

Part of speech: masculine noun

Occurs 101 times in 89 verses

Hebrew: [rāḥaḇ](../h/h7337.md), [raḥaḇ](../h/h7338.md), [rᵊḥōḇ](../h/h7339.md)

Greek: [platos](../g/g4114.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7341)

[Study Light](https://www.studylight.org/lexicons/hebrew/7341.html)

[Bible Hub](https://biblehub.com/str/hebrew/7341.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07341)

[Bible Bento](https://biblebento.com/dictionary/H7341.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7341/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7341.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7341)
