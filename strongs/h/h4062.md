# מַדְהֵבָה

[maḏhēḇâ](https://www.blueletterbible.org/lexicon/h4062)

Definition: golden city (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4062)

[Study Light](https://www.studylight.org/lexicons/hebrew/4062.html)

[Bible Hub](https://biblehub.com/str/hebrew/4062.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04062)

[Bible Bento](https://biblebento.com/dictionary/H4062.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4062/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4062.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4062)
