# הוֹן

[hôn](https://www.blueletterbible.org/lexicon/h1952)

Definition: rich (11x), substance (7x), wealth (5x), enough (2x), nought (1x), price

Part of speech: interjection, masculine noun

Occurs 26 times in 26 verses

Hebrew: [hîn](../h/h1969.md), [himmô](../h/h1994.md), [hēn](../h/h2004.md)

Greek: [ploutos](../g/g4149.md)

Pictoral: continually looking

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1952)

[Study Light](https://www.studylight.org/lexicons/hebrew/1952.html)

[Bible Hub](https://biblehub.com/str/hebrew/1952.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01952)

[Bible Bento](https://biblebento.com/dictionary/H1952.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1952/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1952.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1952)
