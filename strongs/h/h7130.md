# קֶרֶב

[qereḇ](https://www.blueletterbible.org/lexicon/h7130)

Definition: among (76x), midst (73x), within (24x), inwards (22x), in (6x), miscellaneous (26x), entrails

Part of speech: masculine noun

Occurs 227 times in 220 verses

Hebrew: [qāraḇ](../h/h7126.md), [qᵊrēḇ](../h/h7127.md), [qārēḇ](../h/h7131.md), [qarowb](../h/h7138.md)

Greek: [mesos](../g/g3319.md), [gastēr](../g/g1064.md), [koilia](../g/g2836.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7130)

[Study Light](https://www.studylight.org/lexicons/hebrew/7130.html)

[Bible Hub](https://biblehub.com/str/hebrew/7130.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07130)

[Bible Bento](https://biblebento.com/dictionary/H7130.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7130/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7130.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7130)
