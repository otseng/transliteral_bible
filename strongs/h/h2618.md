# חֶסֶד

[Ḥeseḏ](https://www.blueletterbible.org/lexicon/h2618)

Definition: variant (1x).

Part of speech: proper masculine noun

Occurs 0 times in 0 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2618)

[Study Light](https://www.studylight.org/lexicons/hebrew/2618.html)

[Bible Hub](https://biblehub.com/str/hebrew/2618.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02618)

[Bible Bento](https://biblebento.com/dictionary/H2618.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2618/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2618.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2618)
