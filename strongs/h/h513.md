# אֶלְתּוֹלַד

['Eltôlaḏ](https://www.blueletterbible.org/lexicon/h513)

Definition: Eltolad (2x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h513)

[Study Light](https://www.studylight.org/lexicons/hebrew/513.html)

[Bible Hub](https://biblehub.com/str/hebrew/513.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0513)

[Bible Bento](https://biblebento.com/dictionary/H513.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/513/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/513.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h513)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eltolad.html)

[Video Bible](https://www.videobible.com/bible-dictionary/eltolad)