# שִׁלְשׁוֹם

[šilšôm](https://www.blueletterbible.org/lexicon/h8032)

Definition: times past (with H8543) (7x), heretofore (with H8543) (6x), before (3x), past 0865 (2x), beforetime (with H8543) (2x), beforetime (with H865) (1x), miscellaneous (4x).

Part of speech: adverb

Occurs 25 times in 25 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8032)

[Study Light](https://www.studylight.org/lexicons/hebrew/8032.html)

[Bible Hub](https://biblehub.com/str/hebrew/8032.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08032)

[Bible Bento](https://biblebento.com/dictionary/H8032.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8032/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8032.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8032)
