# בַּוַּי

[Baûay](https://www.blueletterbible.org/lexicon/h942)

Definition: Bavai (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h942)

[Study Light](https://www.studylight.org/lexicons/hebrew/942.html)

[Bible Hub](https://biblehub.com/str/hebrew/942.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0942)

[Bible Bento](https://biblebento.com/dictionary/H942.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/942/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/942.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h942)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bavai.html)