# יָעָה

[yāʿâ](https://www.blueletterbible.org/lexicon/h3261)

Definition: sweep away (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3261)

[Study Light](https://www.studylight.org/lexicons/hebrew/3261.html)

[Bible Hub](https://biblehub.com/str/hebrew/3261.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03261)

[Bible Bento](https://biblebento.com/dictionary/H3261.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3261/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3261.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3261)
