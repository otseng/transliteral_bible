# תַּלְאוּבָת

[tal'ûḇāṯ](https://www.blueletterbible.org/lexicon/h8514)

Definition: great drought (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8514)

[Study Light](https://www.studylight.org/lexicons/hebrew/8514.html)

[Bible Hub](https://biblehub.com/str/hebrew/8514.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08514)

[Bible Bento](https://biblebento.com/dictionary/H8514.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8514/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8514.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8514)
