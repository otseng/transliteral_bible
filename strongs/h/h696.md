# אֹרֶב

['ōreḇ](https://www.blueletterbible.org/lexicon/h696)

Definition: wait (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h696)

[Study Light](https://www.studylight.org/lexicons/hebrew/696.html)

[Bible Hub](https://biblehub.com/str/hebrew/696.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0696)

[Bible Bento](https://biblebento.com/dictionary/H696.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/696/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/696.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h696)
