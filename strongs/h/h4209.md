# מְזִמָּה

[mezimmah](https://www.blueletterbible.org/lexicon/h4209)

Definition: discretion (4x), wicked device (3x), device (3x), thought (3x), intents (1x), mischievous device (1x), wickedly (1x), witty inventions (1x), lewdness (1x), mischievous (1x).

Part of speech: feminine noun

Occurs 19 times in 19 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4209)

[Study Light](https://www.studylight.org/lexicons/hebrew/4209.html)

[Bible Hub](https://biblehub.com/str/hebrew/4209.htm)

[Morfix](https://www.morfix.co.il/en/%D7%9E%D6%B0%D7%96%D6%B4%D7%9E%D6%BC%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=04209)

[Bible Bento](https://biblebento.com/dictionary/H4209.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4209/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4209.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4209)
