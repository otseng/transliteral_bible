# עֶצֶם

[`etsem](https://www.blueletterbible.org/lexicon/h6106)

Definition: bone (104x), selfsame (11x), same (5x), body (2x), very (2x), life (1x), strength (1x).

Part of speech: feminine noun

Occurs 126 times in 108 verses

Hebrew: ['ets](../h/h6086.md), ['etsba`](../h/h676.md)

Greek: [osteon](../g/g3747.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6106)

[Study Light](https://www.studylight.org/lexicons/hebrew/6106.html)

[Bible Hub](https://biblehub.com/str/hebrew/6106.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A2%D6%B6%D7%A6%D6%B6%D7%9D)

[NET Bible](http://classic.net.bible.org/strong.php?id=06106)

[Bible Bento](https://biblebento.com/dictionary/H6106.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6106/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6106.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6106)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A2%D7%A6%D7%9D)
