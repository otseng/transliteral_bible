# נְצִיחַ

[Nᵊṣîaḥ](https://www.blueletterbible.org/lexicon/h5335)

Definition: Neziah (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5335)

[Study Light](https://www.studylight.org/lexicons/hebrew/5335.html)

[Bible Hub](https://biblehub.com/str/hebrew/5335.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05335)

[Bible Bento](https://biblebento.com/dictionary/H5335.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5335/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5335.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5335)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/neziah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/neziah)