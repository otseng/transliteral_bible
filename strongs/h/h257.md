# אַחְבָּן

['Aḥbān](https://www.blueletterbible.org/lexicon/h257)

Definition: Ahban (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h257)

[Study Light](https://www.studylight.org/lexicons/hebrew/257.html)

[Bible Hub](https://biblehub.com/str/hebrew/257.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0257)

[Bible Bento](https://biblebento.com/dictionary/H257.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/257/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/257.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h257)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ahban.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ahban)