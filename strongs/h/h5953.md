# עָלַל

[ʿālal](https://www.blueletterbible.org/lexicon/h5953)

Definition: glean (4x), done (3x), abuse (3x), mock (2x), affecteth (1x), children (1x), do (1x), defiled (1x), practise (1x), throughly (1x), wrought wonderfully (1x), wrought (1x).

Part of speech: verb

Occurs 20 times in 18 verses

Hebrew: [gālâ](../h/h1540.md), [gᵊlâ](../h/h1541.md), [ʿālâ](../h/h5927.md), [`aliylah](../h/h5949.md), [ʿălal](../h/h5954.md), [ʿōl](../h/h5923.md), (../h/h4611.md)

Greek: [loidoreō](../g/g3058.md)

Pictoral: eye/foot staff, teach/knowledge staff/yoke, "experience the staff", "experience the yoke"

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5953)

[Study Light](https://www.studylight.org/lexicons/hebrew/5953.html)

[Bible Hub](https://biblehub.com/str/hebrew/5953.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05953)

[Bible Bento](https://biblebento.com/dictionary/H5953.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5953/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5953.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5953)
