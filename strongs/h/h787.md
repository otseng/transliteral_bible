# אשׁ

['š](https://www.blueletterbible.org/lexicon/h787)

Definition: foundation (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h787)

[Study Light](https://www.studylight.org/lexicons/hebrew/787.html)

[Bible Hub](https://biblehub.com/str/hebrew/787.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0787)

[Bible Bento](https://biblebento.com/dictionary/H787.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/787/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/787.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h787)
