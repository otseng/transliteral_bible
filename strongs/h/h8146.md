# שְׂנִיא

[śᵊnî'](https://www.blueletterbible.org/lexicon/h8146)

Definition: hated (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

Hebrew: [sane'](../h/h8130.md), [śᵊnē'](../h/h8131.md), [śin'â](../h/h8135.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8146)

[Study Light](https://www.studylight.org/lexicons/hebrew/8146.html)

[Bible Hub](https://biblehub.com/str/hebrew/8146.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08146)

[Bible Bento](https://biblebento.com/dictionary/H8146.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8146/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8146.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8146)
