# שׁוּק

[šûq](https://www.blueletterbible.org/lexicon/h7784)

Definition: street (4x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7784)

[Study Light](https://www.studylight.org/lexicons/hebrew/7784.html)

[Bible Hub](https://biblehub.com/str/hebrew/7784.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07784)

[Bible Bento](https://biblebento.com/dictionary/H7784.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7784/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7784.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7784)
