# שְׁמִטָּה

[šᵊmiṭṭâ](https://www.blueletterbible.org/lexicon/h8059)

Definition: release (5x).

Part of speech: feminine noun

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8059)

[Study Light](https://www.studylight.org/lexicons/hebrew/8059.html)

[Bible Hub](https://biblehub.com/str/hebrew/8059.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08059)

[Bible Bento](https://biblebento.com/dictionary/H8059.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8059/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8059.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8059)
