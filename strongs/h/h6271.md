# עֲתַלְיָה

[ʿĂṯalyâ](https://www.blueletterbible.org/lexicon/h6271)

Definition: Athaliah (17x).

Part of speech: proper feminine noun, proper masculine noun

Occurs 17 times in 17 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6271)

[Study Light](https://www.studylight.org/lexicons/hebrew/6271.html)

[Bible Hub](https://biblehub.com/str/hebrew/6271.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06271)

[Bible Bento](https://biblebento.com/dictionary/H6271.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6271/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6271.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6271)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/athaliah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/athaliah)