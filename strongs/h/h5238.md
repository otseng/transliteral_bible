# נְכֹת

[nᵊḵōṯ](https://www.blueletterbible.org/lexicon/h5238)

Definition: precious things (2x), treasure, valuables

Part of speech: feminine noun

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5238)

[Study Light](https://www.studylight.org/lexicons/hebrew/5238.html)

[Bible Hub](https://biblehub.com/str/hebrew/5238.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05238)

[Bible Bento](https://biblebento.com/dictionary/H5238.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5238/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5238.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5238)
