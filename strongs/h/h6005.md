# עִמָּנוּאֵל

[ʿimmānû'ēl](https://www.blueletterbible.org/lexicon/h6005)

Definition: Immanuel (with H410) (2x), "God with us" or "with us is God"

Part of speech: proper masculine noun

Occurs 4 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6005)

[Study Light](https://www.studylight.org/lexicons/hebrew/6005.html)

[Bible Hub](https://biblehub.com/str/hebrew/6005.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06005)

[Bible Bento](https://biblebento.com/dictionary/H6005.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6005/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6005.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6005)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/immanuel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/immanuel)