# בֵּית־רְחוֹב

[Bêṯ-Rᵊḥôḇ](https://www.blueletterbible.org/lexicon/h1050)

Definition: Bethrehob (2x).

Part of speech: proper locative noun

Occurs 4 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1050)

[Study Light](https://www.studylight.org/lexicons/hebrew/1050.html)

[Bible Hub](https://biblehub.com/str/hebrew/1050.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01050)

[Bible Bento](https://biblebento.com/dictionary/H1050.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1050/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1050.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1050)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bethrehob.html)