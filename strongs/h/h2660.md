# חֵפֶר

[Ḥēp̄er](https://www.blueletterbible.org/lexicon/h2660)

Definition: Hepher (9x).

Part of speech: proper locative noun, proper masculine noun

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2660)

[Study Light](https://www.studylight.org/lexicons/hebrew/2660.html)

[Bible Hub](https://biblehub.com/str/hebrew/2660.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02660)

[Bible Bento](https://biblebento.com/dictionary/H2660.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2660/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2660.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2660)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hepher.html)

[Video Bible](https://www.videobible.com/bible-dictionary/hepher)