# עָכְרָן

[ʿāḵrān](https://www.blueletterbible.org/lexicon/h5918)

Definition: Ocran (5x).

Part of speech: proper masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5918)

[Study Light](https://www.studylight.org/lexicons/hebrew/5918.html)

[Bible Hub](https://biblehub.com/str/hebrew/5918.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05918)

[Bible Bento](https://biblebento.com/dictionary/H5918.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5918/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5918.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5918)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/O/ocran.html)