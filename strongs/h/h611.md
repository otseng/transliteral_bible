# אָסוֹן

['āsôn](https://www.blueletterbible.org/lexicon/h611)

Definition: mischief (3x), mischief follow (2x).

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h611)

[Study Light](https://www.studylight.org/lexicons/hebrew/611.html)

[Bible Hub](https://biblehub.com/str/hebrew/611.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0611)

[Bible Bento](https://biblebento.com/dictionary/H611.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/611/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/611.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h611)
