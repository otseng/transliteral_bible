# בָּקַשׁ

[bāqaš](https://www.blueletterbible.org/lexicon/h1245)

Definition: seek (189x), require (14x), request (4x), seek out (4x), enquired (3x), besought (2x), ask (2x), sought for (2x), begging (1x), desire (1x), get (1x), inquisition (1x), procureth (1x).

Part of speech: verb

Occurs 225 times in 215 

Hebrew: [baqqāšâ](../h/h1246.md)

Greek: [eperōtaō](../g/g1905.md), [zēteō](../g/g2212.md), [parakaleō](../g/g3870.md)

Edenics: beseech

Synonyms: [seek](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Seek)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1245)

[Study Light](https://www.studylight.org/lexicons/hebrew/1245.html)

[Bible Hub](https://biblehub.com/str/hebrew/1245.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01245)

[Bible Bento](https://biblebento.com/dictionary/H1245.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1245/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1245.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1245)
