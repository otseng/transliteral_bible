# נְדִיבָה

[nᵊḏîḇâ](https://www.blueletterbible.org/lexicon/h5082)

Definition: soul (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5082)

[Study Light](https://www.studylight.org/lexicons/hebrew/5082.html)

[Bible Hub](https://biblehub.com/str/hebrew/5082.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05082)

[Bible Bento](https://biblebento.com/dictionary/H5082.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5082/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5082.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5082)
