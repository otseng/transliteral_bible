# גֵּאָה

[gē'â](https://www.blueletterbible.org/lexicon/h1344)

Definition: pride (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Hebrew: [gē'](../h/h1341.md), [ga`avah](../h/h1346.md), [gā'ôn](../h/h1347.md), [ge'uwth](../h/h1348.md), [ga'ăyôn](../h/h1349.md)

Synonyms: [pride](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Pride)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1344)

[Study Light](https://www.studylight.org/lexicons/hebrew/1344.html)

[Bible Hub](https://biblehub.com/str/hebrew/1344.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01344)

[Bible Bento](https://biblebento.com/dictionary/H1344.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1344/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1344.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1344)
