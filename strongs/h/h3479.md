# יִשְׂרָאֵל

[Yiśrā'Ēl](https://www.blueletterbible.org/lexicon/h3479)

Definition: Israel (8x).

Part of speech: proper masculine noun

Occurs 8 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3479)

[Study Light](https://www.studylight.org/lexicons/hebrew/3479.html)

[Bible Hub](https://biblehub.com/str/hebrew/3479.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03479)

[Bible Bento](https://biblebento.com/dictionary/H3479.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3479/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3479.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3479)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/israel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/israel)