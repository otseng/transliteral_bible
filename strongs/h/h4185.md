# מוּשׁ

[mûš](https://www.blueletterbible.org/lexicon/h4185)

Definition: depart (11x), remove (6x), take away (1x), gone back (1x), cease (1x), variant (1x).

Part of speech: verb

Occurs 21 times in 19 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4185)

[Study Light](https://www.studylight.org/lexicons/hebrew/4185.html)

[Bible Hub](https://biblehub.com/str/hebrew/4185.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04185)

[Bible Bento](https://biblebento.com/dictionary/H4185.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4185/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4185.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4185)
