# גִּדְעֹנִי

[giḏʿōnî](https://www.blueletterbible.org/lexicon/h1441)

Definition: Gideoni (5x).

Part of speech: proper masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1441)

[Study Light](https://www.studylight.org/lexicons/hebrew/1441.html)

[Bible Hub](https://biblehub.com/str/hebrew/1441.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01441)

[Bible Bento](https://biblebento.com/dictionary/H1441.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1441/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1441.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1441)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/gideoni.html)

[Video Bible](https://www.videobible.com/bible-dictionary/gideoni)