# עָגִיל

[ʿāḡîl](https://www.blueletterbible.org/lexicon/h5694)

Definition: earring (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/hebrew/5694.html)

[Bible Hub](https://biblehub.com/str/hebrew/5694.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05694)

[Bible Bento](https://biblebento.com/dictionary/H5694.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5694/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5694.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5694)
