# מְגַלָּה

[mᵊḡallâ](https://www.blueletterbible.org/lexicon/h4040)

Definition: roll (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4040)

[Study Light](https://www.studylight.org/lexicons/hebrew/4040.html)

[Bible Hub](https://biblehub.com/str/hebrew/4040.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04040)

[Bible Bento](https://biblebento.com/dictionary/H4040.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4040/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4040.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4040)
