# אַלְלַי

['allay](https://www.blueletterbible.org/lexicon/h480)

Definition: woe (2x).

Part of speech: interjection

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h480)

[Study Light](https://www.studylight.org/lexicons/hebrew/480.html)

[Bible Hub](https://biblehub.com/str/hebrew/480.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0480)

[Bible Bento](https://biblebento.com/dictionary/H480.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/480/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/480.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h480)
