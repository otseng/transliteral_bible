# חוּד

[ḥûḏ](https://www.blueletterbible.org/lexicon/h2330)

Definition: put forth (4x).

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2330)

[Study Light](https://www.studylight.org/lexicons/hebrew/2330.html)

[Bible Hub](https://biblehub.com/str/hebrew/2330.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02330)

[Bible Bento](https://biblebento.com/dictionary/H2330.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2330/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2330.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2330)
