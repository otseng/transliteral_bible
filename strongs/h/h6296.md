# פָּגַר

[pāḡar](https://www.blueletterbible.org/lexicon/h6296)

Definition: faint (2x).

Part of speech: verb

Occurs 2 times in 2 verses

Hebrew: [peḡer](../h/h6297.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6296)

[Study Light](https://www.studylight.org/lexicons/hebrew/6296.html)

[Bible Hub](https://biblehub.com/str/hebrew/6296.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06296)

[Bible Bento](https://biblebento.com/dictionary/H6296.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6296/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6296.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6296)
