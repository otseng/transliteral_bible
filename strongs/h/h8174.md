# שַׁעַף

[ŠaʿAp̄](https://www.blueletterbible.org/lexicon/h8174)

Definition: Shaaph (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8174)

[Study Light](https://www.studylight.org/lexicons/hebrew/8174.html)

[Bible Hub](https://biblehub.com/str/hebrew/8174.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08174)

[Bible Bento](https://biblebento.com/dictionary/H8174.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8174/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8174.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8174)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shaaph.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shaaph)