# תָּלַע

[tālaʿ](https://www.blueletterbible.org/lexicon/h8529)

Definition: scarlet (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8529)

[Study Light](https://www.studylight.org/lexicons/hebrew/8529.html)

[Bible Hub](https://biblehub.com/str/hebrew/8529.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08529)

[Bible Bento](https://biblebento.com/dictionary/H8529.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8529/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8529.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8529)
