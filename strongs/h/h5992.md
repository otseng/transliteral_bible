# עַמִּינָדָב

[ʿAmmînāḏāḇ](https://www.blueletterbible.org/lexicon/h5992)

Definition: Amminadab (13x).

Part of speech: proper masculine noun

Occurs 13 times in 12 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5992)

[Study Light](https://www.studylight.org/lexicons/hebrew/5992.html)

[Bible Hub](https://biblehub.com/str/hebrew/5992.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05992)

[Bible Bento](https://biblebento.com/dictionary/H5992.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5992/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5992.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5992)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/amminadab.html)

[Video Bible](https://www.videobible.com/bible-dictionary/amminadab)