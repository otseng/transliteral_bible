# נָקַר

[nāqar](https://www.blueletterbible.org/lexicon/h5365)

Definition: put out (2x), thrust out (1x), pick it out (1x), pierced (1x), digged (1x).

Part of speech: verb

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5365)

[Study Light](https://www.studylight.org/lexicons/hebrew/5365.html)

[Bible Hub](https://biblehub.com/str/hebrew/5365.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05365)

[Bible Bento](https://biblebento.com/dictionary/H5365.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5365/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5365.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5365)
