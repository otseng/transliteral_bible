# רוֹם

[rôm](https://www.blueletterbible.org/lexicon/h7315)

Definition: high (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

Hebrew: [ruwm](../h/h7311.md), [rûm](../h/h7312.md), [rûm](../h/h7313.md), [rûm](../h/h7314.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7315)

[Study Light](https://www.studylight.org/lexicons/hebrew/7315.html)

[Bible Hub](https://biblehub.com/str/hebrew/7315.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07315)

[Bible Bento](https://biblebento.com/dictionary/H7315.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7315/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7315.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7315)
