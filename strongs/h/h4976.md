# מַתָּן

[matān](https://www.blueletterbible.org/lexicon/h4976)

Definition: gift (5x).

Part of speech: masculine noun

Occurs 5 times in 5 verses

Hebrew: [matānâ](../h/h4979.md), [nathan](../h/h5414.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4976)

[Study Light](https://www.studylight.org/lexicons/hebrew/4976.html)

[Bible Hub](https://biblehub.com/str/hebrew/4976.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04976)

[Bible Bento](https://biblebento.com/dictionary/H4976.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4976/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4976.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4976)
