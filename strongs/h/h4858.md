# מַשָּׂאָה

[maśśā'â](https://www.blueletterbible.org/lexicon/h4858)

Definition: burden (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4858)

[Study Light](https://www.studylight.org/lexicons/hebrew/4858.html)

[Bible Hub](https://biblehub.com/str/hebrew/4858.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04858)

[Bible Bento](https://biblebento.com/dictionary/H4858.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4858/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4858.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4858)
