# אֵשֶׁל

['ēšel](https://www.blueletterbible.org/lexicon/h815)

Definition: grove (1x), tree (2x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0815)

[Study Light](https://www.studylight.org/lexicons/hebrew/815.html)

[Bible Hub](https://biblehub.com/str/hebrew/815.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0815)

[Bible Bento](https://biblebento.com/dictionary/H815.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/815/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/815.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h815)
