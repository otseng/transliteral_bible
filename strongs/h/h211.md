# אוֹפִיר

['Ôp̄îr](https://www.blueletterbible.org/lexicon/h211)

Definition: Ophir (13x), "reducing to ashes", the name of a son of Joktan, and of a gold region in the East

Part of speech: proper locative noun, proper masculine noun

Occurs 13 times in 12 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0211)

[Study Light](https://www.studylight.org/lexicons/hebrew/211.html)

[Bible Hub](https://biblehub.com/str/hebrew/211.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0211)

[Bible Bento](https://biblebento.com/dictionary/H211.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/211/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/211.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h211)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/O/ophir.html)