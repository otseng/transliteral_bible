# צוּלָה

[ṣûlâ](https://www.blueletterbible.org/lexicon/h6683)

Definition: deep (1x), ocean depth

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6683)

[Study Light](https://www.studylight.org/lexicons/hebrew/6683.html)

[Bible Hub](https://biblehub.com/str/hebrew/6683.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06683)

[Bible Bento](https://biblebento.com/dictionary/H6683.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6683/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6683.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6683)
