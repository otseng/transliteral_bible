# יָקְדְּעָם

[YāqdᵊʿĀm](https://www.blueletterbible.org/lexicon/h3347)

Definition: Jokdeam (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3347)

[Study Light](https://www.studylight.org/lexicons/hebrew/3347.html)

[Bible Hub](https://biblehub.com/str/hebrew/3347.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03347)

[Bible Bento](https://biblebento.com/dictionary/H3347.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3347/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3347.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3347)

[Video Bible](https://www.videobible.com/bible-dictionary/jokdeam)