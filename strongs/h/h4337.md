# מֵישָׁע

[Mêšāʿ](https://www.blueletterbible.org/lexicon/h4337)

Definition: Mesha (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4337)

[Study Light](https://www.studylight.org/lexicons/hebrew/4337.html)

[Bible Hub](https://biblehub.com/str/hebrew/4337.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04337)

[Bible Bento](https://biblebento.com/dictionary/H4337.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4337/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4337.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4337)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/mesha.html)

[Video Bible](https://www.videobible.com/bible-dictionary/mesha)