# חֻמְטָה

[Ḥumṭâ](https://www.blueletterbible.org/lexicon/h2547)

Definition: Humtah (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2547)

[Study Light](https://www.studylight.org/lexicons/hebrew/2547.html)

[Bible Hub](https://biblehub.com/str/hebrew/2547.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02547)

[Bible Bento](https://biblebento.com/dictionary/H2547.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2547/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2547.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2547)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/humtah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/humtah)