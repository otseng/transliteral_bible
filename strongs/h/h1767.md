# דַּי

[day](https://www.blueletterbible.org/lexicon/h1767)

Definition: enough (6x), sufficient (5x), from (5x), when (3x), since (3x), able (3x), miscellaneous (13x).

Part of speech: preposition, substantive

Occurs 39 times in 35 verses

Greek: [hikanos](../g/g2425.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1767)

[Study Light](https://www.studylight.org/lexicons/hebrew/1767.html)

[Bible Hub](https://biblehub.com/str/hebrew/1767.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01767)

[Bible Bento](https://biblebento.com/dictionary/H1767.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1767/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1767.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1767)
