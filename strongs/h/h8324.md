# שָׁרַר

[sharar](https://www.blueletterbible.org/lexicon/h8324)

Definition: enemies (5x).

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8324)

[Study Light](https://www.studylight.org/lexicons/hebrew/8324.html)

[Bible Hub](https://biblehub.com/str/hebrew/8324.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A9%D6%B8%D7%81%D7%A8%D6%B7%D7%A8)

[NET Bible](http://classic.net.bible.org/strong.php?id=08324)

[Bible Bento](https://biblebento.com/dictionary/H8324.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8324/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8324.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8324)
