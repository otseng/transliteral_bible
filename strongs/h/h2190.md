# זַעֲוָן

[zaʿăvān](https://www.blueletterbible.org/lexicon/h2190)

Definition: Zavan (1x), Zaavan (1x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2190)

[Study Light](https://www.studylight.org/lexicons/hebrew/2190.html)

[Bible Hub](https://biblehub.com/str/hebrew/2190.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02190)

[Bible Bento](https://biblebento.com/dictionary/H2190.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2190/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2190.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2190)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zavan.html)