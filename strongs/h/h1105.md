# בֶּלַע

[belaʿ](https://www.blueletterbible.org/lexicon/h1105)

Definition: devouring (1x), swallowed (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

Hebrew: [bālaʿ](../h/h1104.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1105)

[Study Light](https://www.studylight.org/lexicons/hebrew/1105.html)

[Bible Hub](https://biblehub.com/str/hebrew/1105.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01105)

[Bible Bento](https://biblebento.com/dictionary/H1105.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1105/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1105.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1105)
