# סֵעֵף

[sēʿēp̄](https://www.blueletterbible.org/lexicon/h5588)

Definition: thoughts (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5588)

[Study Light](https://www.studylight.org/lexicons/hebrew/5588.html)

[Bible Hub](https://biblehub.com/str/hebrew/5588.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05588)

[Bible Bento](https://biblebento.com/dictionary/H5588.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5588/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5588.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5588)
