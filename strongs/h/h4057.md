# מִדְבָּר

[midbar](https://www.blueletterbible.org/lexicon/h4057)

Definition: wilderness (255x), desert (13x), south (1x), speech (1x), wilderness (with H776) (1x).

Part of speech: masculine noun

Occurs 271 times in 257 verses

Hebrew: [dabar](../h/h1696.md)

Greek: [erēmos](../g/g2048.md)

Edenics: mud, barbarian, word, barro, barrio

Synonyms: [desert](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Desert)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4057)

[Study Light](https://www.studylight.org/lexicons/hebrew/4057.html)

[Bible Hub](https://biblehub.com/str/hebrew/4057.htm)

[Morfix](https://www.morfix.co.il/en/%D7%9E%D6%B4%D7%93%D6%B0%D7%91%D6%BC%D6%B8%D7%A8)

[NET Bible](http://classic.net.bible.org/strong.php?id=04057)

[Bible Bento](https://biblebento.com/dictionary/H4057.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4057/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4057.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4057)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%9E%D7%93%D7%91%D7%A8)
