# שֶׁבַר

[šeḇar](https://www.blueletterbible.org/lexicon/h7667)

Definition: destruction (21x), breach (7x), hurt (4x), breaking (3x), affliction (2x), bruise (2x), crashing (1x), interpretation (1x), vexation (1x), miscellaneous (2x), fracture, crushing, breach, crash, ruin, shattering

Part of speech: masculine noun

Occurs 44 times in 41 verses

Hebrew: [shabar](../h/h7665.md), [šēḇer](../h/h7668.md)

Greek: [syntrimma](../g/g4938.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7667)

[Study Light](https://www.studylight.org/lexicons/hebrew/7667.html)

[Bible Hub](https://biblehub.com/str/hebrew/7667.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07667)

[Bible Bento](https://biblebento.com/dictionary/H7667.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7667/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7667.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7667)
