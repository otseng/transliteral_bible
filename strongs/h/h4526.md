# מִסְגֶּרֶת

[misgereṯ](https://www.blueletterbible.org/lexicon/h4526)

Definition: border (14x), close places (2x), hole (1x).

Part of speech: feminine noun

Occurs 17 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4526)

[Study Light](https://www.studylight.org/lexicons/hebrew/4526.html)

[Bible Hub](https://biblehub.com/str/hebrew/4526.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04526)

[Bible Bento](https://biblebento.com/dictionary/H4526.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4526/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4526.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4526)
