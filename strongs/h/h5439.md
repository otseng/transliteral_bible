# סָבִיב

[cabiyb](https://www.blueletterbible.org/lexicon/h5439)

Definition: round about (252x), on every side (26x), about 24 compass (2x), about us (2x), circuits (1x), about them (1x).

Part of speech: adjective, adverb, substantive

Occurs 308 times in 282 verses

Hebrew: [cabab](../h/h5437.md), [zᵊḇûḇ](../h/h2070.md)

Pictoral: turning of the inside

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5439)

[Study Light](https://www.studylight.org/lexicons/hebrew/5439.html)

[Bible Hub](https://biblehub.com/str/hebrew/5439.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A1%D6%B8%D7%91%D6%B4%D7%99%D7%91)

[NET Bible](http://classic.net.bible.org/strong.php?id=05439)

[Bible Bento](https://biblebento.com/dictionary/H5439.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5439/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5439.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5439)
