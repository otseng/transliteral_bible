# שְׂבָכָה

[śᵊḇāḵâ](https://www.blueletterbible.org/lexicon/h7639)

Definition: network (7x), wreath (3x), wreathen work (2x), checker (1x), lattice (1x), snare (1x).

Part of speech: feminine noun

Occurs 16 times in 12 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7639)

[Study Light](https://www.studylight.org/lexicons/hebrew/7639.html)

[Bible Hub](https://biblehub.com/str/hebrew/7639.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07639)

[Bible Bento](https://biblebento.com/dictionary/H7639.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7639/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7639.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7639)
