# עַבְדִּי

[ʿAḇdî](https://www.blueletterbible.org/lexicon/h5660)

Definition: Abdi (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5660)

[Study Light](https://www.studylight.org/lexicons/hebrew/5660.html)

[Bible Hub](https://biblehub.com/str/hebrew/5660.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05660)

[Bible Bento](https://biblebento.com/dictionary/H5660.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5660/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5660.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5660)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/abdi.html)

[Video Bible](https://www.videobible.com/bible-dictionary/abdi)