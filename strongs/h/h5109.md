# נוֹבָי

[Nôḇāy](https://www.blueletterbible.org/lexicon/h5109)

Definition: Nebai (1x).

Part of speech: proper masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5109)

[Study Light](https://www.studylight.org/lexicons/hebrew/5109.html)

[Bible Hub](https://biblehub.com/str/hebrew/5109.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05109)

[Bible Bento](https://biblebento.com/dictionary/H5109.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5109/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5109.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5109)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/nebai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/nebai)