# שְׁמֹנִים

[šᵊmōnîm](https://www.blueletterbible.org/lexicon/h8084)

Definition: four fourscore (34x), eighty (3x), eightieth (1x).

Part of speech: adjective

Occurs 38 times in 37 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8084)

[Study Light](https://www.studylight.org/lexicons/hebrew/8084.html)

[Bible Hub](https://biblehub.com/str/hebrew/8084.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08084)

[Bible Bento](https://biblebento.com/dictionary/H8084.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8084/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8084.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8084)
