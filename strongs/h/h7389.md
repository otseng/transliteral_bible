# רֵישׁ

[rêš](https://www.blueletterbible.org/lexicon/h7389)

Definition: poverty (7x).

Part of speech: masculine noun

Occurs 7 times in 7 verses

Hebrew: [rûš](../h/h7326.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7389)

[Study Light](https://www.studylight.org/lexicons/hebrew/7389.html)

[Bible Hub](https://biblehub.com/str/hebrew/7389.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07389)

[Bible Bento](https://biblebento.com/dictionary/H7389.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7389/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7389.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7389)
