# קָבַב

[qāḇaḇ](https://www.blueletterbible.org/lexicon/h6895)

Definition: curse (7x), at all (1x).

Part of speech: verb

Occurs 9 times in 9 verses

Synonyms: [curse](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Curse)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6895)

[Study Light](https://www.studylight.org/lexicons/hebrew/6895.html)

[Bible Hub](https://biblehub.com/str/hebrew/6895.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06895)

[Bible Bento](https://biblebento.com/dictionary/H6895.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6895/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6895.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6895)
