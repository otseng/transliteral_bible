# דְּבַח

[dᵊḇaḥ](https://www.blueletterbible.org/lexicon/h1684)

Definition: offer (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1684)

[Study Light](https://www.studylight.org/lexicons/hebrew/1684.html)

[Bible Hub](https://biblehub.com/str/hebrew/1684.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01684)

[Bible Bento](https://biblebento.com/dictionary/H1684.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1684/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1684.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1684)
