# חָסֹן

[ḥāsōn](https://www.blueletterbible.org/lexicon/h2634)

Definition: strong (2x).

Part of speech: adjective

Occurs 2 times in 2 verses

Hebrew: [ḥăsîn](../h/h2626.md), [ḥāsan](../h/h2630.md), [ḥēsen](../h/h2632.md), [ḥōsen](../h/h2633.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2634)

[Study Light](https://www.studylight.org/lexicons/hebrew/2634.html)

[Bible Hub](https://biblehub.com/str/hebrew/2634.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02634)

[Bible Bento](https://biblebento.com/dictionary/H2634.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2634/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2634.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2634)
