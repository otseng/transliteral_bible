# אֲחִימוֹת

['Ăḥîmôṯ](https://www.blueletterbible.org/lexicon/h287)

Definition: Ahimoth (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h287)

[Study Light](https://www.studylight.org/lexicons/hebrew/287.html)

[Bible Hub](https://biblehub.com/str/hebrew/287.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0287)

[Bible Bento](https://biblebento.com/dictionary/H287.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/287/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/287.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h287)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ahimoth.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ahimoth)