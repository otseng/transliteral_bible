# שָׁשָׁא

[šāšā'](https://www.blueletterbible.org/lexicon/h8338)

Definition: leave the sixth part of thee (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8338)

[Study Light](https://www.studylight.org/lexicons/hebrew/8338.html)

[Bible Hub](https://biblehub.com/str/hebrew/8338.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08338)

[Bible Bento](https://biblebento.com/dictionary/H8338.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8338/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8338.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8338)
