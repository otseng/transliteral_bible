# עֹצֶב

[ʿōṣeḇ](https://www.blueletterbible.org/lexicon/h6090)

Definition: sorrow (2x), wicked (1x), idol (1x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

Hebrew: [ʿāṣaḇ](../h/h6087.md), [ʿăṣaḇ](../h/h6088.md), [maʿăṣēḇâ](../h/h4620.md), [`itstsabown](../h/h6093.md)

Synonyms: [idol](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Idol)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6090)

[Study Light](https://www.studylight.org/lexicons/hebrew/6090.html)

[Bible Hub](https://biblehub.com/str/hebrew/6090.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06090)

[Bible Bento](https://biblebento.com/dictionary/H6090.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6090/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6090.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6090)
