# שְׁחוֹר

[šᵊḥôr](https://www.blueletterbible.org/lexicon/h7815)

Definition: coal (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7815)

[Study Light](https://www.studylight.org/lexicons/hebrew/7815.html)

[Bible Hub](https://biblehub.com/str/hebrew/7815.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07815)

[Bible Bento](https://biblebento.com/dictionary/H7815.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7815/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7815.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7815)
