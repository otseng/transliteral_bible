# מִשְׁאָל

[Miš'Āl](https://www.blueletterbible.org/lexicon/h4861)

Definition: Misheal (1x), Mishal (1x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4861)

[Study Light](https://www.studylight.org/lexicons/hebrew/4861.html)

[Bible Hub](https://biblehub.com/str/hebrew/4861.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04861)

[Bible Bento](https://biblebento.com/dictionary/H4861.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4861/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4861.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4861)
