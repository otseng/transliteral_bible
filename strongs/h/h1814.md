# דָּלַק

[dalaq](https://www.blueletterbible.org/lexicon/h1814)

Definition: pursue (2x), kindle (2x), chase (1x), persecute (1x), persecutors (1x), burning (1x), inflame (1x).

Part of speech: verb

Occurs 9 times in 9 verses

Greek: [katadiōkō](../g/g2614.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1814)

[Study Light](https://www.studylight.org/lexicons/hebrew/1814.html)

[Bible Hub](https://biblehub.com/str/hebrew/1814.htm)

[Morfix](https://www.morfix.co.il/en/%D7%93%D6%BC%D6%B8%D7%9C%D6%B7%D7%A7)

[NET Bible](http://classic.net.bible.org/strong.php?id=01814)

[Bible Bento](https://biblebento.com/dictionary/H1814.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1814/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1814.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1814)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%93%D7%9C%D7%A7)
