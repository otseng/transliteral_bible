# גּוּב

[gûḇ](https://www.blueletterbible.org/lexicon/h1461)

Definition: husbandman (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1461)

[Study Light](https://www.studylight.org/lexicons/hebrew/1461.html)

[Bible Hub](https://biblehub.com/str/hebrew/1461.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01461)

[Bible Bento](https://biblebento.com/dictionary/H1461.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1461/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1461.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1461)
