# תָּלַל

[tālal](https://www.blueletterbible.org/lexicon/h8524)

Definition: eminent (1x).

Part of speech: adjective, verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8524)

[Study Light](https://www.studylight.org/lexicons/hebrew/8524.html)

[Bible Hub](https://biblehub.com/str/hebrew/8524.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08524)

[Bible Bento](https://biblebento.com/dictionary/H8524.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8524/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8524.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8524)
