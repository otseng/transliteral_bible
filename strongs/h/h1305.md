# בָּרַר

[bārar](https://www.blueletterbible.org/lexicon/h1305)

Definition: pure (5x), choice (2x), chosen (2x), clean (2x), clearly (1x), manifest (1x), bright (1x), purge out (1x), polished (1x), purge (1x), purified (1x).

Part of speech: verb

Occurs 18 times in 16 verses

Greek: [hagiazō](../g/g37.md)

Synonyms: [clean](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Clean)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1305)

[Study Light](https://www.studylight.org/lexicons/hebrew/1305.html)

[Bible Hub](https://biblehub.com/str/hebrew/1305.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01305)

[Bible Bento](https://biblebento.com/dictionary/H1305.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1305/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1305.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1305)
