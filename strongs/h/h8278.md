# שְׂרָד

[śᵊrāḏ](https://www.blueletterbible.org/lexicon/h8278)

Definition: service (4x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8278)

[Study Light](https://www.studylight.org/lexicons/hebrew/8278.html)

[Bible Hub](https://biblehub.com/str/hebrew/8278.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08278)

[Bible Bento](https://biblebento.com/dictionary/H8278.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8278/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8278.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8278)
