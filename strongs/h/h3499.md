# יֶתֶר

[yeṯer](https://www.blueletterbible.org/lexicon/h3499)

Definition: rest (63x), remnant (14x), residue (8x), leave (4x), excellency (3x), withs (3x), cord (1x), exceeding (1x), excellent (1x), more (1x), plentifully (1x), string (1x).

Part of speech: masculine noun

Occurs 102 times in 95 verses

Hebrew: [yāṯar](../h/h3498.md)

Greek: [perissos](../g/g4053.md)

Edenics: yesterday

Synonyms: [rest](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Rest)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3499)

[Study Light](https://www.studylight.org/lexicons/hebrew/3499.html)

[Bible Hub](https://biblehub.com/str/hebrew/3499.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03499)

[Bible Bento](https://biblebento.com/dictionary/H3499.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3499/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3499.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3499)
