# לוּדִיי

[Lûḏîy](https://www.blueletterbible.org/lexicon/h3866)

Definition: Ludim (2x), Lydians (1x).

Part of speech: adjective

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3866)

[Study Light](https://www.studylight.org/lexicons/hebrew/3866.html)

[Bible Hub](https://biblehub.com/str/hebrew/3866.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03866)

[Bible Bento](https://biblebento.com/dictionary/H3866.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3866/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3866.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3866)
