# עוּז

[ʿûz](https://www.blueletterbible.org/lexicon/h5756)

Definition: gather... (2x), gather (1x), retire (1x), to take refuge, bring to refuge, seek refuge

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5756)

[Study Light](https://www.studylight.org/lexicons/hebrew/5756.html)

[Bible Hub](https://biblehub.com/str/hebrew/5756.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05756)

[Bible Bento](https://biblebento.com/dictionary/H5756.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5756/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5756.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5756)
