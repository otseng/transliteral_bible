# יִזְלִיאָה

[Yizlî'Â](https://www.blueletterbible.org/lexicon/h3152)

Definition: Jezliah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3152)

[Study Light](https://www.studylight.org/lexicons/hebrew/3152.html)

[Bible Hub](https://biblehub.com/str/hebrew/3152.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03152)

[Bible Bento](https://biblebento.com/dictionary/H3152.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3152/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3152.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3152)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jezliah.html)