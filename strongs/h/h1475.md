# גּוּמָּץ

[gûmmāṣ](https://www.blueletterbible.org/lexicon/h1475)

Definition: pit (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1475)

[Study Light](https://www.studylight.org/lexicons/hebrew/1475.html)

[Bible Hub](https://biblehub.com/str/hebrew/1475.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01475)

[Bible Bento](https://biblebento.com/dictionary/H1475.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1475/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1475.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1475)
