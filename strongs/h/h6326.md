# פּוּעָה

[pûʿâ](https://www.blueletterbible.org/lexicon/h6326)

Definition: Puah (1x).

Part of speech: proper feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6326)

[Study Light](https://www.studylight.org/lexicons/hebrew/6326.html)

[Bible Hub](https://biblehub.com/str/hebrew/6326.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06326)

[Bible Bento](https://biblebento.com/dictionary/H6326.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6326/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6326.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6326)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/puah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/puah)