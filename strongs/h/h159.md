# אֹהַב

['ōhaḇ](https://www.blueletterbible.org/lexicon/h159)

Definition: loves (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

Hebrew: ['ahab](../h/h157.md), ['ahaḇ](../h/h158.md), ['ahăḇâ](../h/h160.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h159)

[Study Light](https://www.studylight.org/lexicons/hebrew/159.html)

[Bible Hub](https://biblehub.com/str/hebrew/159.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0159)

[Bible Bento](https://biblebento.com/dictionary/H159.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/159/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/159.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h159)
