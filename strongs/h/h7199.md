# קַשָּׁת

[qaššāṯ](https://www.blueletterbible.org/lexicon/h7199)

Definition: archer (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7199)

[Study Light](https://www.studylight.org/lexicons/hebrew/7199.html)

[Bible Hub](https://biblehub.com/str/hebrew/7199.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07199)

[Bible Bento](https://biblebento.com/dictionary/H7199.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7199/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7199.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7199)
