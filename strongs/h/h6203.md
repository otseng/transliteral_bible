# עֹרֶף

[ʿōrep̄](https://www.blueletterbible.org/lexicon/h6203)

Definition: neck (17x), back (7x), stiffnecked (with H7186) (4x), stiffnecked (3x), backs (with H310) (1x), stiffnecked (with H7185) (1x).

Part of speech: masculine noun

Occurs 33 times in 32 verses

Hebrew: [ʿārap̄](../h/h6202.md)

Greek: [trachēlos](../g/g5137.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6203)

[Study Light](https://www.studylight.org/lexicons/hebrew/6203.html)

[Bible Hub](https://biblehub.com/str/hebrew/6203.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06203)

[Bible Bento](https://biblebento.com/dictionary/H6203.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6203/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6203.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6203)
