# צִיּוּן

[ṣîyûn](https://www.blueletterbible.org/lexicon/h6725)

Definition: title (1x), waymark (1x), sign (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6725)

[Study Light](https://www.studylight.org/lexicons/hebrew/6725.html)

[Bible Hub](https://biblehub.com/str/hebrew/6725.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06725)

[Bible Bento](https://biblebento.com/dictionary/H6725.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6725/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6725.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6725)
