# אַלְמָן

['almān](https://www.blueletterbible.org/lexicon/h488)

Definition: forsaken (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

Hebrew: ['almōn](../h/h489.md), ['almānâ](../h/h490.md), ['almānûṯ](../h/h491.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h488)

[Study Light](https://www.studylight.org/lexicons/hebrew/488.html)

[Bible Hub](https://biblehub.com/str/hebrew/488.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0488)

[Bible Bento](https://biblebento.com/dictionary/H488.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/488/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/488.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h488)
