# תֵּקַע

[tēqaʿ](https://www.blueletterbible.org/lexicon/h8629)

Definition: sound (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8629)

[Study Light](https://www.studylight.org/lexicons/hebrew/8629.html)

[Bible Hub](https://biblehub.com/str/hebrew/8629.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08629)

[Bible Bento](https://biblebento.com/dictionary/H8629.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8629/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8629.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8629)
