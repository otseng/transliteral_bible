# רַךְ

[raḵ](https://www.blueletterbible.org/lexicon/h7390)

Definition: tender (9x), soft (3x), fainthearted (with H3824) (1x), one (1x), weak (1x), tenderhearted (with H3824) (1x).

Part of speech: adjective

Occurs 16 times in 16 verses

Hebrew: [rāḵaḵ](../h/h7401.md)

Greek: [asthenēs](../g/g772.md), [malakos](../g/g3120.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7390)

[Study Light](https://www.studylight.org/lexicons/hebrew/7390.html)

[Bible Hub](https://biblehub.com/str/hebrew/7390.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07390)

[Bible Bento](https://biblebento.com/dictionary/H7390.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7390/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7390.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7390)
