# מָזוֹן

[māzôn](https://www.blueletterbible.org/lexicon/h4202)

Definition: meat (1x), victuals (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4202)

[Study Light](https://www.studylight.org/lexicons/hebrew/4202.html)

[Bible Hub](https://biblehub.com/str/hebrew/4202.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04202)

[Bible Bento](https://biblebento.com/dictionary/H4202.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4202/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4202.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4202)
