# מִדָּה

[midâ](https://www.blueletterbible.org/lexicon/h4060)

Definition: measure (37x), piece (7x), stature (4x), size (3x), meteyard (1x), garments (1x), tribute (1x), wide (1x).

Part of speech: feminine noun

Occurs 55 times in 53 verses

Hebrew: [maḏ](../h/h4055.md)

Greek: [arithmos](../g/g706.md), [metron](../g/g3358.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4060)

[Study Light](https://www.studylight.org/lexicons/hebrew/4060.html)

[Bible Hub](https://biblehub.com/str/hebrew/4060.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04060)

[Bible Bento](https://biblebento.com/dictionary/H4060.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4060/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4060.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4060)
