# צָהַל

[ṣāhal](https://www.blueletterbible.org/lexicon/h6670)

Definition: cry aloud (2x), bellow (1x), neighed (1x), cry out (1x), rejoiced (1x), shine (1x), shout (1x), lift up (1x).

Part of speech: verb

Occurs 10 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6670)

[Study Light](https://www.studylight.org/lexicons/hebrew/6670.html)

[Bible Hub](https://biblehub.com/str/hebrew/6670.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06670)

[Bible Bento](https://biblebento.com/dictionary/H6670.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6670/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6670.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6670)
