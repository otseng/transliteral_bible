# מֵאמַר

[mē'mar](https://www.blueletterbible.org/lexicon/h3983)

Definition: appointment (1x), word (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

Hebrew: [ma'ămār](../h/h3982.md), [millâ](../h/h4405.md), [millâ](../h/h4406.md), [mālal](../h/h4448.md), [mᵊlal](../h/h4449.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3983)

[Study Light](https://www.studylight.org/lexicons/hebrew/3983.html)

[Bible Hub](https://biblehub.com/str/hebrew/3983.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03983)

[Bible Bento](https://biblebento.com/dictionary/H3983.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3983/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3983.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3983)
