# מְרֹרָה

[mᵊrōrâ](https://www.blueletterbible.org/lexicon/h4846)

Definition: gall (2x), bitter (1x), bitter things (1x).

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4846)

[Study Light](https://www.studylight.org/lexicons/hebrew/4846.html)

[Bible Hub](https://biblehub.com/str/hebrew/4846.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04846)

[Bible Bento](https://biblebento.com/dictionary/H4846.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4846/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4846.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4846)
