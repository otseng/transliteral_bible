# מַטְאֲטֵא

[maṭ'ăṭē'](https://www.blueletterbible.org/lexicon/h4292)

Definition: besom (1x), broom

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4292)

[Study Light](https://www.studylight.org/lexicons/hebrew/4292.html)

[Bible Hub](https://biblehub.com/str/hebrew/4292.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04292)

[Bible Bento](https://biblebento.com/dictionary/H4292.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4292/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4292.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4292)
