# שְׂכַל

[śᵊḵal](https://www.blueletterbible.org/lexicon/h7920)

Definition: consider (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [sakal](../h/h7919.md), [śēḵel](../h/h7922.md), [śāḵlᵊṯānû](../h/h7924.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7920)

[Study Light](https://www.studylight.org/lexicons/hebrew/7920.html)

[Bible Hub](https://biblehub.com/str/hebrew/7920.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07920)

[Bible Bento](https://biblebento.com/dictionary/H7920.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7920/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7920.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7920)
