# עֻזָּא

[ʿUzzā'](https://www.blueletterbible.org/lexicon/h5798)

Definition: Uzza (10x), Uzzah (4x).

Part of speech: proper locative noun, proper masculine noun

Occurs 14 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5798)

[Study Light](https://www.studylight.org/lexicons/hebrew/5798.html)

[Bible Hub](https://biblehub.com/str/hebrew/5798.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05798)

[Bible Bento](https://biblebento.com/dictionary/H5798.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5798/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5798.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5798)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/U/uzza.html)