# שִׂיאוֹן

[Śî'ôn](https://www.blueletterbible.org/lexicon/h7865)

Definition: Sion (1x).

Part of speech: proper noun with reference to a mountain

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7865)

[Study Light](https://www.studylight.org/lexicons/hebrew/7865.html)

[Bible Hub](https://biblehub.com/str/hebrew/7865.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07865)

[Bible Bento](https://biblebento.com/dictionary/H7865.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7865/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7865.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7865)
