# בִּטְחָה

[biṭḥâ](https://www.blueletterbible.org/lexicon/h985)

Definition: confidence (1x), trust

Part of speech: feminine noun

Occurs 1 times in 1 verses

Hebrew: [batach](../h/h982.md), [betach](../h/h983.md), [biṭṭāḥôn](../h/h986.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0985)

[Study Light](https://www.studylight.org/lexicons/hebrew/985.html)

[Bible Hub](https://biblehub.com/str/hebrew/985.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0985)

[Bible Bento](https://biblebento.com/dictionary/H985.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/985/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/985.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h985)
