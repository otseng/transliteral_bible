# שֻׁפִּים

[Šupîm](https://www.blueletterbible.org/lexicon/h8206)

Definition: Shuppim (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8206)

[Study Light](https://www.studylight.org/lexicons/hebrew/8206.html)

[Bible Hub](https://biblehub.com/str/hebrew/8206.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08206)

[Bible Bento](https://biblebento.com/dictionary/H8206.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8206/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8206.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8206)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shuppim.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shuppim)