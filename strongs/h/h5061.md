# נֶגַע

[neḡaʿ](https://www.blueletterbible.org/lexicon/h5061)

Definition: plague (65x), sore (5x), stroke (4x), stripes (2x), stricken (1x), wound (1x).

Part of speech: masculine noun

Occurs 78 times in 62 verses

Hebrew: [dāḵā'](../h/h1792.md), [hālam](../h/h1986.md), [kāṯaṯ](../h/h3807.md), [māḥaṣ](../h/h4272.md), [naga`](../h/h5060.md), [nāḡap̄](../h/h5062.md), [nakah](../h/h5221.md), [sāp̄aq](../h/h5606.md)

Synonyms: [plague](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Plague)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5061)

[Study Light](https://www.studylight.org/lexicons/hebrew/5061.html)

[Bible Hub](https://biblehub.com/str/hebrew/5061.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05061)

[Bible Bento](https://biblebento.com/dictionary/H5061.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5061/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5061.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5061)
