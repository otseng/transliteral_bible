# תְּהוֹם

[tĕhowm](https://www.blueletterbible.org/lexicon/h8415)

Definition: deep (20x), depth (15x), deep places (1x), abyss, subterranean waters, primeval ocean

Part of speech: masculine/feminine noun

Occurs 36 times in 35 verses

Greek: [abyssos](../g/g12.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8415)

[Study Light](https://www.studylight.org/lexicons/hebrew/8415.html)

[Bible Hub](https://biblehub.com/str/hebrew/8415.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08415)

[Bible Bento](https://biblebento.com/dictionary/H8415.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8415/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8415.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8415)