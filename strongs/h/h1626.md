# גֵּרָה

[gērâ](https://www.blueletterbible.org/lexicon/h1626)

Definition: gerah (5x).

Part of speech: feminine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1626)

[Study Light](https://www.studylight.org/lexicons/hebrew/1626.html)

[Bible Hub](https://biblehub.com/str/hebrew/1626.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01626)

[Bible Bento](https://biblebento.com/dictionary/H1626.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1626/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1626.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1626)
