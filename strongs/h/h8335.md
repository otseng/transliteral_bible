# שָׁרֵת

[šārēṯ](https://www.blueletterbible.org/lexicon/h8335)

Definition: ministry (1x), minister (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8335)

[Study Light](https://www.studylight.org/lexicons/hebrew/8335.html)

[Bible Hub](https://biblehub.com/str/hebrew/8335.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08335)

[Bible Bento](https://biblebento.com/dictionary/H8335.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8335/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8335.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8335)
