# מְבַשְּׁלָה

[mᵊḇaššᵊlâ](https://www.blueletterbible.org/lexicon/h4018)

Definition: boiling places (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4018)

[Study Light](https://www.studylight.org/lexicons/hebrew/4018.html)

[Bible Hub](https://biblehub.com/str/hebrew/4018.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04018)

[Bible Bento](https://biblebento.com/dictionary/H4018.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4018/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4018.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4018)
