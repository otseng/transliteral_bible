# עָגֹל

[ʿāḡōl](https://www.blueletterbible.org/lexicon/h5696)

Definition: round (6x).

Part of speech: adjective

Occurs 6 times in 5 verses

Hebrew: [ʿēḡel](../h/h5695.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5696)

[Study Light](https://www.studylight.org/lexicons/hebrew/5696.html)

[Bible Hub](https://biblehub.com/str/hebrew/5696.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05696)

[Bible Bento](https://biblebento.com/dictionary/H5696.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5696/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5696.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5696)
