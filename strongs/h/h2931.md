# טָמֵא

[tame'](https://www.blueletterbible.org/lexicon/h2931)

Definition: unclean (79x), defiled (5x), infamous (1x), polluted (1x), pollution (1x).

Part of speech: adjective

Occurs 87 times in 78 verses

Hebrew: [ṭāmē'](../h/h2930.md)

Pictoral: container of water

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2931)

[Study Light](https://www.studylight.org/lexicons/hebrew/2931.html)

[Bible Hub](https://biblehub.com/str/hebrew/2931.htm)

[Morfix](https://www.morfix.co.il/en/%D7%98%D6%B8%D7%9E%D6%B5%D7%90)

[NET Bible](http://classic.net.bible.org/strong.php?id=02931)

[Bible Bento](https://biblebento.com/dictionary/H2931.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2931/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2931.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2931)
