# פַּשְׁחוּר

[Pašḥûr](https://www.blueletterbible.org/lexicon/h6583)

Definition: Pashur (14x).

Part of speech: proper masculine noun

Occurs 14 times in 12 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6583)

[Study Light](https://www.studylight.org/lexicons/hebrew/6583.html)

[Bible Hub](https://biblehub.com/str/hebrew/6583.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06583)

[Bible Bento](https://biblebento.com/dictionary/H6583.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6583/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6583.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6583)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/pashur.html)