# שַׁחֶפֶת

[šaḥep̄eṯ](https://www.blueletterbible.org/lexicon/h7829)

Definition: consumption (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7829)

[Study Light](https://www.studylight.org/lexicons/hebrew/7829.html)

[Bible Hub](https://biblehub.com/str/hebrew/7829.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07829)

[Bible Bento](https://biblebento.com/dictionary/H7829.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7829/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7829.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7829)
