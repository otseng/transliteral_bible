# כָּסַם

[kāsam](https://www.blueletterbible.org/lexicon/h3697)

Definition: poll (1x), only (1x).

Part of speech: verb

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3697)

[Study Light](https://www.studylight.org/lexicons/hebrew/3697.html)

[Bible Hub](https://biblehub.com/str/hebrew/3697.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03697)

[Bible Bento](https://biblebento.com/dictionary/H3697.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3697/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3697.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3697)
