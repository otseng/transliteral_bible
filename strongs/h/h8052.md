# שְׁמוּעָה

[šᵊmûʿâ](https://www.blueletterbible.org/lexicon/h8052)

Definition: rumour (9x), tidings (8x), report (4x), fame (2x), bruit (1x), doctrine (1x), mentioned (1x), news (1x).

Part of speech: feminine noun

Occurs 27 times in 24 verses

Hebrew: [šāmēm](../h/h8074.md)

Greek: [phēmē](../g/g5345.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8052)

[Study Light](https://www.studylight.org/lexicons/hebrew/8052.html)

[Bible Hub](https://biblehub.com/str/hebrew/8052.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08052)

[Bible Bento](https://biblebento.com/dictionary/H8052.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8052/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8052.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8052)
