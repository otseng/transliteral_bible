# פְּתוּאֵל

[Pᵊṯû'Ēl](https://www.blueletterbible.org/lexicon/h6602)

Definition: Pethuel (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6602)

[Study Light](https://www.studylight.org/lexicons/hebrew/6602.html)

[Bible Hub](https://biblehub.com/str/hebrew/6602.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06602)

[Bible Bento](https://biblebento.com/dictionary/H6602.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6602/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6602.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6602)

[Video Bible](https://www.videobible.com/bible-dictionary/pethuel)