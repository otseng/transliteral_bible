# גֵּרָה

[gērâ](https://www.blueletterbible.org/lexicon/h1625)

Definition: cud (11x).

Part of speech: feminine noun

Occurs 11 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1625)

[Study Light](https://www.studylight.org/lexicons/hebrew/1625.html)

[Bible Hub](https://biblehub.com/str/hebrew/1625.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01625)

[Bible Bento](https://biblebento.com/dictionary/H1625.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1625/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1625.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1625)
