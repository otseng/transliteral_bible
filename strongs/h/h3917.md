# לִילִית

[lîlîṯ](https://www.blueletterbible.org/lexicon/h3917)

Definition: screech owl (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3917)

[Study Light](https://www.studylight.org/lexicons/hebrew/3917.html)

[Bible Hub](https://biblehub.com/str/hebrew/3917.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03917)

[Bible Bento](https://biblebento.com/dictionary/H3917.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3917/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3917.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3917)
