# דָּרְבוֹן

[dārḇôn](https://www.blueletterbible.org/lexicon/h1861)

Definition: goad (2x).

Part of speech: masculine/feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1861)

[Study Light](https://www.studylight.org/lexicons/hebrew/1861.html)

[Bible Hub](https://biblehub.com/str/hebrew/1861.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01861)

[Bible Bento](https://biblebento.com/dictionary/H1861.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1861/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1861.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1861)
