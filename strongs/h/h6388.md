# פֶּלֶג

[peleḡ](https://www.blueletterbible.org/lexicon/h6388)

Definition: river (9x), stream (1x), channel

Part of speech: masculine noun

Occurs 10 times in 10 verses

Hebrew: [yᵊ'ōr](../h/h2975.md), [pᵊlagâ](../h/h6390.md)

Names: [Peleḡ](../h/h6389.md)

Synonyms: [river](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#River)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6388)

[Study Light](https://www.studylight.org/lexicons/hebrew/6388.html)

[Bible Hub](https://biblehub.com/str/hebrew/6388.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06388)

[Bible Bento](https://biblebento.com/dictionary/H6388.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6388/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6388.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6388)
