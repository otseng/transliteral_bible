# כָּבוּל

[Kāḇûl](https://www.blueletterbible.org/lexicon/h3521)

Definition: Cabul (2x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3521)

[Study Light](https://www.studylight.org/lexicons/hebrew/3521.html)

[Bible Hub](https://biblehub.com/str/hebrew/3521.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03521)

[Bible Bento](https://biblebento.com/dictionary/H3521.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3521/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3521.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3521)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/cabul.html)

[Video Bible](https://www.videobible.com/bible-dictionary/cabul)