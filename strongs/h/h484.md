# אַלְמֻגִּים

['almugîm](https://www.blueletterbible.org/lexicon/h484)

Definition: almug (3x).

Part of speech: masculine noun

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h484)

[Study Light](https://www.studylight.org/lexicons/hebrew/484.html)

[Bible Hub](https://biblehub.com/str/hebrew/484.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0484)

[Bible Bento](https://biblebento.com/dictionary/H484.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/484/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/484.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h484)
