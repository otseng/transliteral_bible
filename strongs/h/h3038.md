# יְדוּתוּן

[Yᵊḏûṯûn](https://www.blueletterbible.org/lexicon/h3038)

Definition: Jeduthun (17x).

Part of speech: proper masculine noun

Occurs 20 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3038)

[Study Light](https://www.studylight.org/lexicons/hebrew/3038.html)

[Bible Hub](https://biblehub.com/str/hebrew/3038.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03038)

[Bible Bento](https://biblebento.com/dictionary/H3038.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3038/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3038.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3038)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jeduthun.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jeduthun)