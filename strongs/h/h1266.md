# בְּרוֹת

[bᵊrôṯ](https://www.blueletterbible.org/lexicon/h1266)

Definition: fir (1x).

Part of speech: masculine plural noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1266)

[Study Light](https://www.studylight.org/lexicons/hebrew/1266.html)

[Bible Hub](https://biblehub.com/str/hebrew/1266.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01266)

[Bible Bento](https://biblebento.com/dictionary/H1266.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1266/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1266.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1266)
