# עֲגָלָה

[ʿăḡālâ](https://www.blueletterbible.org/lexicon/h5699)

Definition: cart (15x), wagon (9x), chariot (1x).

Part of speech: feminine noun

Occurs 25 times in 20 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5699)

[Study Light](https://www.studylight.org/lexicons/hebrew/5699.html)

[Bible Hub](https://biblehub.com/str/hebrew/5699.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05699)

[Bible Bento](https://biblebento.com/dictionary/H5699.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5699/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5699.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5699)
