# זָמָם

[zāmām](https://www.blueletterbible.org/lexicon/h2162)

Definition: wicked device (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2162)

[Study Light](https://www.studylight.org/lexicons/hebrew/2162.html)

[Bible Hub](https://biblehub.com/str/hebrew/2162.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02162)

[Bible Bento](https://biblebento.com/dictionary/H2162.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2162/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2162.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2162)
