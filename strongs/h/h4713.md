# מִצְרִי

[Miṣrî](https://www.blueletterbible.org/lexicon/h4713)

Definition: Egyptian (25x), Egyptian (with H376) (3x), Egypt (1x), Egyptian women (1x).

Part of speech: adjective

Occurs 72 times in 63 verses

Hebrew: [Mitsrayim](../h/h4714.md)

Greek: [Aigyptios](../g/g124.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4713)

[Study Light](https://www.studylight.org/lexicons/hebrew/4713.html)

[Bible Hub](https://biblehub.com/str/hebrew/4713.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04713)

[Bible Bento](https://biblebento.com/dictionary/H4713.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4713/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4713.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4713)
