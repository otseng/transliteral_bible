# כָּכָה

[kāḵâ](https://www.blueletterbible.org/lexicon/h3602)

Definition: thus, so, after, this, even so, in such a case.

Part of speech: adverb

Occurs 37 times in 35 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3602)

[Study Light](https://www.studylight.org/lexicons/hebrew/3602.html)

[Bible Hub](https://biblehub.com/str/hebrew/3602.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03602)

[Bible Bento](https://biblebento.com/dictionary/H3602.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3602/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3602.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3602)
