# אָדֹם

['āḏōm](https://www.blueletterbible.org/lexicon/h122)

Definition: red (8x), ruddy (1x).

Part of speech: adjective

Occurs 9 times in 7 verses

Places: ['Ĕḏōm](../h/h123.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0122)

[Study Light](https://www.studylight.org/lexicons/hebrew/122.html)

[Bible Hub](https://biblehub.com/str/hebrew/122.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0122)

[Bible Bento](https://biblebento.com/dictionary/H122.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/122/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/122.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h122)
