# מֵמַד

[mēmaḏ](https://www.blueletterbible.org/lexicon/h4461)

Definition: measures (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4461)

[Study Light](https://www.studylight.org/lexicons/hebrew/4461.html)

[Bible Hub](https://biblehub.com/str/hebrew/4461.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04461)

[Bible Bento](https://biblebento.com/dictionary/H4461.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4461/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4461.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4461)
