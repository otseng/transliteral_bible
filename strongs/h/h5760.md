# עֲוִיל

[ʿăvîl](https://www.blueletterbible.org/lexicon/h5760)

Definition: ungodly (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5760)

[Study Light](https://www.studylight.org/lexicons/hebrew/5760.html)

[Bible Hub](https://biblehub.com/str/hebrew/5760.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05760)

[Bible Bento](https://biblebento.com/dictionary/H5760.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5760/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5760.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5760)
