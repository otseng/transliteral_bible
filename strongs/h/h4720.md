# מִקְדָּשׁ

[miqdash](https://www.blueletterbible.org/lexicon/h4720)

Definition: sanctuary (69x), holy place (3x), chapel (1x), hallowed part (1x), a consecrated thing or place, temple

Part of speech: masculine noun

Occurs 74 times in 71 verses

Hebrew: [qadash](../h/h6942.md)

Greek: [hagion](../g/g39.md), [hagiasmos](../g/g38.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4720)

[Study Light](https://www.studylight.org/lexicons/hebrew/4720.html)

[Bible Hub](https://biblehub.com/str/hebrew/4720.htm)

[Morfix](https://www.morfix.co.il/en/%D7%9E%D6%B4%D7%A7%D6%B0%D7%93%D6%BC%D6%B8%D7%A9%D7%81)

[NET Bible](http://classic.net.bible.org/strong.php?id=04720)

[Bible Bento](https://biblebento.com/dictionary/H4720.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4720/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4720.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4720)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%9E%D7%A7%D7%93%D7%A9)
