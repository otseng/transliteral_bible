# אֲרֻבָּה

['ărubâ](https://www.blueletterbible.org/lexicon/h699)

Definition: windows (8x), chimney (1x), lattice, sluice

Part of speech: feminine noun

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0699)

[Study Light](https://www.studylight.org/lexicons/hebrew/699.html)

[Bible Hub](https://biblehub.com/str/hebrew/699.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0699)

[Bible Bento](https://biblebento.com/dictionary/H699.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/699/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/699.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h699)
