# לָאָה

[lā'â](https://www.blueletterbible.org/lexicon/h3811)

Definition: weary (15x), grieve (2x), faint (1x), loath (1x).

Part of speech: verb

Occurs 19 times in 18 verses

Greek: [agōn](../g/g73.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3811)

[Study Light](https://www.studylight.org/lexicons/hebrew/3811.html)

[Bible Hub](https://biblehub.com/str/hebrew/3811.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03811)

[Bible Bento](https://biblebento.com/dictionary/H3811.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3811/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3811.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3811)
