# אֲבִישַׁג

['Ăḇîšaḡ](https://www.blueletterbible.org/lexicon/h49)

Definition: Abishag (5x), “my father is a wanderer”, David’s beautiful young nurse

Part of speech: proper feminine noun

Occurs 5 times in 5 verses

Hebrew: ['ab](../h/h1.md), [šāḡâ](../h/h7686.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h49)

[Study Light](https://www.studylight.org/lexicons/hebrew/49.html)

[Bible Hub](https://biblehub.com/str/hebrew/49.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=049)

[Bible Bento](https://biblebento.com/dictionary/H49.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/49/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/49.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h49)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/abishag.html)

[Video Bible](https://www.videobible.com/bible-dictionary/abishag)