# שָׁעָה

[šāʿâ](https://www.blueletterbible.org/lexicon/h8159)

Definition: look (5x), respect (3x), dismay (2x), turn (1x), regard (1x), spare (1x), be dim (1x), depart (1x), gaze

Part of speech: verb

Occurs 15 times in 15 verses

Greek: [thaumazō](../g/g2296.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8159)

[Study Light](https://www.studylight.org/lexicons/hebrew/8159.html)

[Bible Hub](https://biblehub.com/str/hebrew/8159.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08159)

[Bible Bento](https://biblebento.com/dictionary/H8159.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8159/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8159.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8159)
