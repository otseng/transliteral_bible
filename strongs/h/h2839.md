# חִשֻּׁק

[ḥiššuq](https://www.blueletterbible.org/lexicon/h2839)

Definition: felloes (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2839)

[Study Light](https://www.studylight.org/lexicons/hebrew/2839.html)

[Bible Hub](https://biblehub.com/str/hebrew/2839.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02839)

[Bible Bento](https://biblebento.com/dictionary/H2839.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2839/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2839.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2839)
