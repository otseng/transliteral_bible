# לָאֵל

[lā'ēl](https://www.blueletterbible.org/lexicon/h3815)

Definition: Lael (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3815)

[Study Light](https://www.studylight.org/lexicons/hebrew/3815.html)

[Bible Hub](https://biblehub.com/str/hebrew/3815.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03815)

[Bible Bento](https://biblebento.com/dictionary/H3815.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3815/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3815.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3815)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/L/lael.html)

[Video Bible](https://www.videobible.com/bible-dictionary/lael)