# שַׂרְעַפִּים

[śarʿapîm](https://www.blueletterbible.org/lexicon/h8312)

Definition: thought (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8312)

[Study Light](https://www.studylight.org/lexicons/hebrew/8312.html)

[Bible Hub](https://biblehub.com/str/hebrew/8312.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08312)

[Bible Bento](https://biblebento.com/dictionary/H8312.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8312/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8312.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8312)
