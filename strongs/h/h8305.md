# שְׂרִיקָה

[śᵊrîqâ](https://www.blueletterbible.org/lexicon/h8305)

Definition: fine (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8305)

[Study Light](https://www.studylight.org/lexicons/hebrew/8305.html)

[Bible Hub](https://biblehub.com/str/hebrew/8305.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08305)

[Bible Bento](https://biblebento.com/dictionary/H8305.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8305/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8305.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8305)
