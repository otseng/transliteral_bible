# שְׁאֵל

[šᵊ'ēl](https://www.blueletterbible.org/lexicon/h7593)

Definition: ask (3x), require (2x), demand (1x).

Part of speech: verb

Occurs 6 times in 6 verses

Hebrew: [miš'ālâ](../h/h4862.md), [sha'al](../h/h7592.md), [šᵊ'ēlâ](../h/h7595.md), [šᵊ'ēlâ](../h/h7596.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7593)

[Study Light](https://www.studylight.org/lexicons/hebrew/7593.html)

[Bible Hub](https://biblehub.com/str/hebrew/7593.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07593)

[Bible Bento](https://biblebento.com/dictionary/H7593.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7593/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7593.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7593)
