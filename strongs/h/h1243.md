# בַּקָּרָה

[baqqārâ](https://www.blueletterbible.org/lexicon/h1243)

Definition: seek (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Synonyms: [seek](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Seek)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1243)

[Study Light](https://www.studylight.org/lexicons/hebrew/1243.html)

[Bible Hub](https://biblehub.com/str/hebrew/1243.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01243)

[Bible Bento](https://biblebento.com/dictionary/H1243.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1243/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1243.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1243)
