# אִישְׁהוֹד

['Îšhôḏ](https://www.blueletterbible.org/lexicon/h379)

Definition: Ishod (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h379)

[Study Light](https://www.studylight.org/lexicons/hebrew/379.html)

[Bible Hub](https://biblehub.com/str/hebrew/379.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0379)

[Bible Bento](https://biblebento.com/dictionary/H379.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/379/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/379.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h379)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/ishod.html)