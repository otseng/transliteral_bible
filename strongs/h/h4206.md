# מְזִיחַ

[mᵊzîaḥ](https://www.blueletterbible.org/lexicon/h4206)

Definition: strength (2x), girdle (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4206)

[Study Light](https://www.studylight.org/lexicons/hebrew/4206.html)

[Bible Hub](https://biblehub.com/str/hebrew/4206.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04206)

[Bible Bento](https://biblebento.com/dictionary/H4206.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4206/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4206.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4206)
