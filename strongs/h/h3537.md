# כַּד

[kaḏ](https://www.blueletterbible.org/lexicon/h3537)

Definition: pitcher (14x), barrel (4x).

Part of speech: feminine noun

Occurs 18 times in 17 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3537)

[Study Light](https://www.studylight.org/lexicons/hebrew/3537.html)

[Bible Hub](https://biblehub.com/str/hebrew/3537.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03537)

[Bible Bento](https://biblebento.com/dictionary/H3537.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3537/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3537.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3537)
