# אֶלְעוּזַי

['ElʿÛzay](https://www.blueletterbible.org/lexicon/h498)

Definition: Eluzai (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h498)

[Study Light](https://www.studylight.org/lexicons/hebrew/498.html)

[Bible Hub](https://biblehub.com/str/hebrew/498.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0498)

[Bible Bento](https://biblebento.com/dictionary/H498.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/498/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/498.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h498)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eluzai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/eluzai)