# חֵשֶׁק

[ḥēšeq](https://www.blueletterbible.org/lexicon/h2837)

Definition: desire (1x), that (1x), pleasure (1x), desire (with H2836) (1x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2837)

[Study Light](https://www.studylight.org/lexicons/hebrew/2837.html)

[Bible Hub](https://biblehub.com/str/hebrew/2837.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02837)

[Bible Bento](https://biblebento.com/dictionary/H2837.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2837/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2837.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2837)
