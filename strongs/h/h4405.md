# מִלָּה

[millâ](https://www.blueletterbible.org/lexicon/h4405)

Definition: word (23x), speech (6x), say (2x), speaking (2x), answer (with H7725) (1x), byword (1x), matter (1x), speak (1x), talking (1x).

Part of speech: feminine noun

Occurs 38 times in 38 verses

Hebrew: [ma'ămār](../h/h3982.md), [mē'mar](../h/h3983.md), [millâ](../h/h4406.md), [mālal](../h/h4448.md), [mᵊlal](../h/h4449.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4405)

[Study Light](https://www.studylight.org/lexicons/hebrew/4405.html)

[Bible Hub](https://biblehub.com/str/hebrew/4405.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04405)

[Bible Bento](https://biblebento.com/dictionary/H4405.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4405/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4405.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4405)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%9E%D7%9C%D7%94)
