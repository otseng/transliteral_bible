# מַטֶּה

[maṭṭê](https://www.blueletterbible.org/lexicon/h4294)

Definition: tribe (182x), rod (52x), staff (15x), staves (1x), tribe (with H4294) (1x).

Part of speech: masculine noun

Occurs 252 times in 205 verses

Hebrew: [natah](../h/h5186.md)

Greek: [phylē](../g/g5443.md)

Pictoral: liquid contained

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4294)

[Study Light](https://www.studylight.org/lexicons/hebrew/4294.html)

[Bible Hub](https://biblehub.com/str/hebrew/4294.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04294)

[Bible Bento](https://biblebento.com/dictionary/H4294.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4294/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4294.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4294)
