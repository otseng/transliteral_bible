# מַדּוּעַ

[madûaʿ](https://www.blueletterbible.org/lexicon/h4069)

Definition: wherefore, why, how.

Part of speech: adverb

Occurs 72 times in 71 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4069)

[Study Light](https://www.studylight.org/lexicons/hebrew/4069.html)

[Bible Hub](https://biblehub.com/str/hebrew/4069.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04069)

[Bible Bento](https://biblebento.com/dictionary/H4069.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4069/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4069.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4069)
