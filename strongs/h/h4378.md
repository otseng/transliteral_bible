# מַכָּר

[makār](https://www.blueletterbible.org/lexicon/h4378)

Definition: acquaintance (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4378)

[Study Light](https://www.studylight.org/lexicons/hebrew/4378.html)

[Bible Hub](https://biblehub.com/str/hebrew/4378.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04378)

[Bible Bento](https://biblebento.com/dictionary/H4378.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4378/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4378.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4378)
