# הֵנָּה

[hēnnâ](https://www.blueletterbible.org/lexicon/h2008)

Definition: (50x), hither, here, now, way, to...fro, since, hitherto, thus far.

Part of speech: adverb

Occurs 50 times in 42 verses

Hebrew: [hinneh](../h/h2009.md)

Greek: [idou](../g/g2400.md)

Pictoral: look continually

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2008)

[Study Light](https://www.studylight.org/lexicons/hebrew/2008.html)

[Bible Hub](https://biblehub.com/str/hebrew/2008.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02008)

[Bible Bento](https://biblebento.com/dictionary/H2008.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2008/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2008.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2008)
