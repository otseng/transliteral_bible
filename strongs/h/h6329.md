# פּוּק

[pûq](https://www.blueletterbible.org/lexicon/h6329)

Definition: obtain (3x), further (1x), get (1x), draw out (1x), affording (1x).

Part of speech: verb

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6329)

[Study Light](https://www.studylight.org/lexicons/hebrew/6329.html)

[Bible Hub](https://biblehub.com/str/hebrew/6329.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06329)

[Bible Bento](https://biblebento.com/dictionary/H6329.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6329/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6329.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6329)
