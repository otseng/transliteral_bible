# שְׁלֻמִיאֵל

[šᵊlumî'ēl](https://www.blueletterbible.org/lexicon/h8017)

Definition: Shelumiel (5x).

Part of speech: proper masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8017)

[Study Light](https://www.studylight.org/lexicons/hebrew/8017.html)

[Bible Hub](https://biblehub.com/str/hebrew/8017.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08017)

[Bible Bento](https://biblebento.com/dictionary/H8017.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8017/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8017.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8017)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shelumiel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shelumiel)