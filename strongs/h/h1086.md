# בָּלָה

[bālâ](https://www.blueletterbible.org/lexicon/h1086)

Definition: wax old (9x), become old (2x), consume (2x), waste (1x), enjoy (1x), non translated variant (1x), to wear out, become old, decay

Part of speech: verb

Occurs 16 times in 15 verses

Greek: [palaioō](../g/g3822.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1086)

[Study Light](https://www.studylight.org/lexicons/hebrew/1086.html)

[Bible Hub](https://biblehub.com/str/hebrew/1086.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01086)

[Bible Bento](https://biblebento.com/dictionary/H1086.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1086/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1086.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1086)
