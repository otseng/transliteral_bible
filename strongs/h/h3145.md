# יוֹשַׁוְיָה

[Yôšavyâ](https://www.blueletterbible.org/lexicon/h3145)

Definition: Joshaviah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3145)

[Study Light](https://www.studylight.org/lexicons/hebrew/3145.html)

[Bible Hub](https://biblehub.com/str/hebrew/3145.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03145)

[Bible Bento](https://biblebento.com/dictionary/H3145.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3145/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3145.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3145)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/joshaviah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/joshaviah)