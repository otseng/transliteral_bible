# יִשְׁוָה

[Yišvâ](https://www.blueletterbible.org/lexicon/h3438)

Definition: Ishuai (1x), Isuah (1x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3438)

[Study Light](https://www.studylight.org/lexicons/hebrew/3438.html)

[Bible Hub](https://biblehub.com/str/hebrew/3438.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03438)

[Bible Bento](https://biblebento.com/dictionary/H3438.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3438/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3438.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3438)
