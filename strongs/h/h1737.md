# דּוֹדַי

[Dôḏay](https://www.blueletterbible.org/lexicon/h1737)

Definition: Dodai (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1737)

[Study Light](https://www.studylight.org/lexicons/hebrew/1737.html)

[Bible Hub](https://biblehub.com/str/hebrew/1737.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01737)

[Bible Bento](https://biblebento.com/dictionary/H1737.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1737/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1737.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1737)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/dodai.html)