# מוֹעַדְיָה

[MôʿAḏyâ](https://www.blueletterbible.org/lexicon/h4153)

Definition: Moadiah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4153)

[Study Light](https://www.studylight.org/lexicons/hebrew/4153.html)

[Bible Hub](https://biblehub.com/str/hebrew/4153.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04153)

[Bible Bento](https://biblebento.com/dictionary/H4153.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4153/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4153.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4153)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/moadiah.html)