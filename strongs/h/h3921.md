# לֶכֶד

[leḵeḏ](https://www.blueletterbible.org/lexicon/h3921)

Definition: taken (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

Hebrew: ['āḥaz](../h/h270.md), [lāḵaḏ](../h/h3920.md), [laqach](../h/h3947.md), [leqaḥ](../h/h3948.md), [nasa'](../h/h5375.md), [nāśaḡ](../h/h5381.md), [cuwr](../h/h5493.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3921)

[Study Light](https://www.studylight.org/lexicons/hebrew/3921.html)

[Bible Hub](https://biblehub.com/str/hebrew/3921.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03921)

[Bible Bento](https://biblebento.com/dictionary/H3921.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3921/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3921.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3921)
