# בַּז

[baz](https://www.blueletterbible.org/lexicon/h957)

Definition: prey (18x), spoil (4x), spoiled (2x), booty (1x).

Part of speech: masculine noun

Occurs 25 times in 25 verses

Hebrew: [bāzaz](../h/h962.md)

Greek: [diarpazō](../g/g1283.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0957)

[Study Light](https://www.studylight.org/lexicons/hebrew/957.html)

[Bible Hub](https://biblehub.com/str/hebrew/957.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0957)

[Bible Bento](https://biblebento.com/dictionary/H957.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/957/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/957.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h957)
