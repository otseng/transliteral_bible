# צִיר

[ṣîr](https://www.blueletterbible.org/lexicon/h6736)

Definition: idol (1x), variant (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

Synonyms: [idol](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Idol)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6736)

[Study Light](https://www.studylight.org/lexicons/hebrew/6736.html)

[Bible Hub](https://biblehub.com/str/hebrew/6736.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06736)

[Bible Bento](https://biblebento.com/dictionary/H6736.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6736/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6736.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6736)
