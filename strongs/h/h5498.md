# סָחַב

[sāḥaḇ](https://www.blueletterbible.org/lexicon/h5498)

Definition: draw (2x), draw out (2x), tear (1x).

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5498)

[Study Light](https://www.studylight.org/lexicons/hebrew/5498.html)

[Bible Hub](https://biblehub.com/str/hebrew/5498.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05498)

[Bible Bento](https://biblebento.com/dictionary/H5498.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5498/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5498.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5498)
