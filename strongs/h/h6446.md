# פַּס

[pas](https://www.blueletterbible.org/lexicon/h6446)

Definition: colours (5x).

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6446)

[Study Light](https://www.studylight.org/lexicons/hebrew/6446.html)

[Bible Hub](https://biblehub.com/str/hebrew/6446.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06446)

[Bible Bento](https://biblebento.com/dictionary/H6446.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6446/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6446.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6446)
