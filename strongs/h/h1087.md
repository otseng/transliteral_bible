# בָּלֶה

[bālê](https://www.blueletterbible.org/lexicon/h1087)

Definition: old (5x).

Part of speech: adjective

Occurs 5 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1087)

[Study Light](https://www.studylight.org/lexicons/hebrew/1087.html)

[Bible Hub](https://biblehub.com/str/hebrew/1087.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01087)

[Bible Bento](https://biblebento.com/dictionary/H1087.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1087/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1087.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1087)
