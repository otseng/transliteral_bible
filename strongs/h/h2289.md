# חֲגוֹר

[ḥăḡôr](https://www.blueletterbible.org/lexicon/h2289)

Definition: girdle (3x), girdled (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

Hebrew: [chagowr](../h/h2290.md), [ḥāḡar](../h/h2296.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2289)

[Study Light](https://www.studylight.org/lexicons/hebrew/2289.html)

[Bible Hub](https://biblehub.com/str/hebrew/2289.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02289)

[Bible Bento](https://biblebento.com/dictionary/H2289.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2289/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2289.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2289)
