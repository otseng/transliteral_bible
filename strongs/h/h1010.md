# בֵּית בַּעַל מְעון

[Bêṯ BaʿAl MᵊʿVn](https://www.blueletterbible.org/lexicon/h1010)

Definition: Bethbaalmeon (1x), Bethmeon (1x).

Part of speech: proper locative noun

Occurs 5 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1010)

[Study Light](https://www.studylight.org/lexicons/hebrew/1010.html)

[Bible Hub](https://biblehub.com/str/hebrew/1010.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01010)

[Bible Bento](https://biblebento.com/dictionary/H1010.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1010/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1010.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1010)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bethbaalmeon.html)