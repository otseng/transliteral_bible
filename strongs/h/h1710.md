# דָּגָה

[dāḡâ](https://www.blueletterbible.org/lexicon/h1710)

Definition: fish (15x).

Part of speech: feminine noun

Occurs 15 times in 13 verses

Hebrew: [dag](../h/h1709.md)

Greek: [ichthys](../g/g2486.md)

Pictoral: door foot, opening and closing of door like a fish tail

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1710)

[Study Light](https://www.studylight.org/lexicons/hebrew/1710.html)

[Bible Hub](https://biblehub.com/str/hebrew/1710.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01710)

[Bible Bento](https://biblebento.com/dictionary/H1710.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1710/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1710.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1710)
