# פָּרַשׂ

[pāraś](https://www.blueletterbible.org/lexicon/h6566)

Definition: spread (31x), spread forth (12x), spread out (6x), spread abroad (5x), stretch (3x), stretch forth (2x), stretch out (2x), scattered (2x), breaketh (1x), lay open (1x), chop in pieces (1x), spread up (1x).

Part of speech: verb

Occurs 67 times in 66 verses

Hebrew: [pāras](../h/h6536.md), [pᵊras](../h/h6537.md)

Greek: [ekteinō](../g/g1614.md), [ballō](../g/g906.md), [diaspeirō](../g/g1289.md), [ekpetannymi](../g/g1600.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6566)

[Study Light](https://www.studylight.org/lexicons/hebrew/6566.html)

[Bible Hub](https://biblehub.com/str/hebrew/6566.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06566)

[Bible Bento](https://biblebento.com/dictionary/H6566.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6566/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6566.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6566)
