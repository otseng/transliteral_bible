# מוּצָק

[mûṣāq](https://www.blueletterbible.org/lexicon/h4165)

Definition: casting (1x), hardness (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4165)

[Study Light](https://www.studylight.org/lexicons/hebrew/4165.html)

[Bible Hub](https://biblehub.com/str/hebrew/4165.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04165)

[Bible Bento](https://biblebento.com/dictionary/H4165.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4165/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4165.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4165)
