# חֲבַצִּנְיָה

[Ḥăḇaṣṣinyâ](https://www.blueletterbible.org/lexicon/h2262)

Definition: Habaziniah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2262)

[Study Light](https://www.studylight.org/lexicons/hebrew/2262.html)

[Bible Hub](https://biblehub.com/str/hebrew/2262.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02262)

[Bible Bento](https://biblebento.com/dictionary/H2262.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2262/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2262.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2262)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/habaziniah.html)