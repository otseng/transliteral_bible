# הֲתֻלִּים

[hăṯullîm](https://www.blueletterbible.org/lexicon/h2049)

Definition: mocker (1x).

Part of speech: masculine plural noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2049)

[Study Light](https://www.studylight.org/lexicons/hebrew/2049.html)

[Bible Hub](https://biblehub.com/str/hebrew/2049.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02049)

[Bible Bento](https://biblebento.com/dictionary/H2049.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2049/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2049.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2049)
