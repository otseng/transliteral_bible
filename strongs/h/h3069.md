# יְהֹוָה

[Yᵊhōvâ](https://www.blueletterbible.org/lexicon/h3069)

Definition: GOD (304x), LORD (1x).

Part of speech: proper noun with reference to deity

Occurs 306 times in 296 verses

Hebrew: [Yĕhovah](../h/h3068.md)

Synonyms: [God](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#God)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3069)

[Study Light](https://www.studylight.org/lexicons/hebrew/3069.html)

[Bible Hub](https://biblehub.com/str/hebrew/3069.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03069)

[Bible Bento](https://biblebento.com/dictionary/H3069.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3069/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3069.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3069)
