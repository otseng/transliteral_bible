# שָׂשׂוֹן

[śāśôn](https://www.blueletterbible.org/lexicon/h8342)

Definition: joy (15x), gladness (3x), mirth (3x), rejoicing (1x).

Part of speech: masculine noun

Occurs 22 times in 22 verses

Hebrew: [śûś](../h/h7797.md)

Greek: [agalliasis](../g/g20.md), [euphrosynē](../g/g2167.md), [chara](../g/g5479.md)

Synonyms: [joy](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Joy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8342)

[Study Light](https://www.studylight.org/lexicons/hebrew/8342.html)

[Bible Hub](https://biblehub.com/str/hebrew/8342.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08342)

[Bible Bento](https://biblebento.com/dictionary/H8342.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8342/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8342.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8342)
