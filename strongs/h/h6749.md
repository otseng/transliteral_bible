# צָלַל

[ṣālal](https://www.blueletterbible.org/lexicon/h6749)

Definition: sink (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6749)

[Study Light](https://www.studylight.org/lexicons/hebrew/6749.html)

[Bible Hub](https://biblehub.com/str/hebrew/6749.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06749)

[Bible Bento](https://biblebento.com/dictionary/H6749.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6749/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6749.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6749)
