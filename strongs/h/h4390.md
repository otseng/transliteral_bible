# מָלָא

[mālā'](https://www.blueletterbible.org/lexicon/h4390)

Definition: fill (107x), full (48x), fulfil (28x), consecrate (15x), accomplish (7x), replenish (7x), wholly (6x), set (6x), expired (3x), fully (2x), gather (2x), overflow (2x), satisfy (2x), miscellaneous (14x).

Part of speech: verb

Occurs 250 times in 242 verses

Hebrew: [mᵊlā'](../h/h4391.md), [mālē'](../h/h4392.md)

Greek: [plēroō](../g/g4137.md), [poreuō](../g/g4198.md), [teleioō](../g/g5048.md), [synteleō](../g/g4931.md), [pimplēmi](../g/g4130.md), [anaplēroō](../g/g378.md), [gemō](../g/g1073.md)

Edenics: mellow

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4390)

[Study Light](https://www.studylight.org/lexicons/hebrew/4390.html)

[Bible Hub](https://biblehub.com/str/hebrew/4390.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04390)

[Bible Bento](https://biblebento.com/dictionary/H4390.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4390/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4390.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4390)
