# יָאַל

[yā'al](https://www.blueletterbible.org/lexicon/h2973)

Definition: foolishly (1x), fool (1x), foolish (1x), dote (1x).

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2973)

[Study Light](https://www.studylight.org/lexicons/hebrew/2973.html)

[Bible Hub](https://biblehub.com/str/hebrew/2973.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02973)

[Bible Bento](https://biblebento.com/dictionary/H2973.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2973/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2973.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2973)
