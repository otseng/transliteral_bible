# פּוּתִי

[pûṯî](https://www.blueletterbible.org/lexicon/h6336)

Definition: Puhites (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6336)

[Study Light](https://www.studylight.org/lexicons/hebrew/6336.html)

[Bible Hub](https://biblehub.com/str/hebrew/6336.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06336)

[Bible Bento](https://biblebento.com/dictionary/H6336.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6336/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6336.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6336)
