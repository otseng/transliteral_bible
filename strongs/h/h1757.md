# דּוּרָא

[Dûrā'](https://www.blueletterbible.org/lexicon/h1757)

Definition: Dura (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1757)

[Study Light](https://www.studylight.org/lexicons/hebrew/1757.html)

[Bible Hub](https://biblehub.com/str/hebrew/1757.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01757)

[Bible Bento](https://biblebento.com/dictionary/H1757.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1757/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1757.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1757)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/dura.html)

[Video Bible](https://www.videobible.com/bible-dictionary/dura)