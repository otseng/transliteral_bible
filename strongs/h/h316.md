# אֲחַרְחֵל

['Ăḥarḥēl](https://www.blueletterbible.org/lexicon/h316)

Definition: Aharhel (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h316)

[Study Light](https://www.studylight.org/lexicons/hebrew/316.html)

[Bible Hub](https://biblehub.com/str/hebrew/316.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0316)

[Bible Bento](https://biblebento.com/dictionary/H316.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/316/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/316.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h316)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/aharhel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/aharhel)