# בִּלְשָׁן

[Bilšān](https://www.blueletterbible.org/lexicon/h1114)

Definition: Bilshan (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1114)

[Study Light](https://www.studylight.org/lexicons/hebrew/1114.html)

[Bible Hub](https://biblehub.com/str/hebrew/1114.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01114)

[Bible Bento](https://biblebento.com/dictionary/H1114.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1114/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1114.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1114)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bilshan.html)

[Video Bible](https://www.videobible.com/bible-dictionary/bilshan)