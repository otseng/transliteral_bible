# סוּגַר

[sûḡar](https://www.blueletterbible.org/lexicon/h5474)

Definition: ward (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5474)

[Study Light](https://www.studylight.org/lexicons/hebrew/5474.html)

[Bible Hub](https://biblehub.com/str/hebrew/5474.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05474)

[Bible Bento](https://biblebento.com/dictionary/H5474.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5474/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5474.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5474)
