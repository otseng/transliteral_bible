# מָעוֹז

[māʿôz](https://www.blueletterbible.org/lexicon/h4581)

Definition: strength (24x), strong (4x), fortress (3x), hold (2x), forces (1x), fort (1x), rock (1x), strengthen (1x).

Part of speech: masculine noun

Occurs 37 times in 35 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4581)

[Study Light](https://www.studylight.org/lexicons/hebrew/4581.html)

[Bible Hub](https://biblehub.com/str/hebrew/4581.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04581)

[Bible Bento](https://biblebento.com/dictionary/H4581.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4581/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4581.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4581)
