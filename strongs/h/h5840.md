# עַזְרִיקָם

[ʿAzrîqām](https://www.blueletterbible.org/lexicon/h5840)

Definition: Azrikam (6x).

Part of speech: proper masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5840)

[Study Light](https://www.studylight.org/lexicons/hebrew/5840.html)

[Bible Hub](https://biblehub.com/str/hebrew/5840.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05840)

[Bible Bento](https://biblebento.com/dictionary/H5840.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5840/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5840.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5840)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/azrikam.html)

[Video Bible](https://www.videobible.com/bible-dictionary/azrikam)