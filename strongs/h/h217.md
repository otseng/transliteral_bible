# אוּר

['ûr](https://www.blueletterbible.org/lexicon/h217)

Definition: fire(s) (5x), light (1x).

Part of speech: masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0217)

[Study Light](https://www.studylight.org/lexicons/hebrew/217.html)

[Bible Hub](https://biblehub.com/str/hebrew/217.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0217)

[Bible Bento](https://biblebento.com/dictionary/H217.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/217/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/217.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h217)
