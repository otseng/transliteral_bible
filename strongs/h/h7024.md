# קִיר

[qîr](https://www.blueletterbible.org/lexicon/h7024)

Definition: Kir (5x).

Part of speech: proper locative noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7024)

[Study Light](https://www.studylight.org/lexicons/hebrew/7024.html)

[Bible Hub](https://biblehub.com/str/hebrew/7024.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07024)

[Bible Bento](https://biblebento.com/dictionary/H7024.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7024/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7024.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7024)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/K/kir.html)

[Video Bible](https://www.videobible.com/bible-dictionary/kir)