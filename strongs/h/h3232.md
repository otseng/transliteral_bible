# יִמְנָה

[Yimnâ](https://www.blueletterbible.org/lexicon/h3232)

Definition: Jimnah (2x), Imnah (2x), Jimnites (1x).

Part of speech: patrial noun, proper masculine noun

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3232)

[Study Light](https://www.studylight.org/lexicons/hebrew/3232.html)

[Bible Hub](https://biblehub.com/str/hebrew/3232.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03232)

[Bible Bento](https://biblebento.com/dictionary/H3232.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3232/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3232.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3232)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jimnah.html)