# יְבוּסִי

[Yᵊḇûsî](https://www.blueletterbible.org/lexicon/h2983)

Definition: Jebusite (41x).

Part of speech: masculine patrial noun

Occurs 41 times in 39 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2983)

[Study Light](https://www.studylight.org/lexicons/hebrew/2983.html)

[Bible Hub](https://biblehub.com/str/hebrew/2983.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02983)

[Bible Bento](https://biblebento.com/dictionary/H2983.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2983/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2983.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2983)
