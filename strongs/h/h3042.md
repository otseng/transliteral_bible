# יְדָיָה

[Yᵊḏāyâ](https://www.blueletterbible.org/lexicon/h3042)

Definition: Jedaiah (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3042)

[Study Light](https://www.studylight.org/lexicons/hebrew/3042.html)

[Bible Hub](https://biblehub.com/str/hebrew/3042.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03042)

[Bible Bento](https://biblebento.com/dictionary/H3042.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3042/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3042.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3042)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jedaiah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jedaiah)