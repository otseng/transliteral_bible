# יַעְזֵיר

[Yaʿzêr](https://www.blueletterbible.org/lexicon/h3270)

Definition: Jazer (11x), Jaazer (2x).

Part of speech: proper locative noun

Occurs 13 times in 12 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3270)

[Study Light](https://www.studylight.org/lexicons/hebrew/3270.html)

[Bible Hub](https://biblehub.com/str/hebrew/3270.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03270)

[Bible Bento](https://biblebento.com/dictionary/H3270.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3270/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3270.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3270)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jazer.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jazer)