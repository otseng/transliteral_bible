# לְוִי

[Lᵊvî](https://www.blueletterbible.org/lexicon/h3881)

Definition: Levite (286x), "joined to"

Part of speech: adjective

Occurs 286 times in 258 verses

Hebrew: [Lēvî](../h/h3878.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3881)

[Study Light](https://www.studylight.org/lexicons/hebrew/3881.html)

[Bible Hub](https://biblehub.com/str/hebrew/3881.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03881)

[Bible Bento](https://biblebento.com/dictionary/H3881.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3881/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3881.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3881)

[Wikipedia](https://en.wikipedia.org/wiki/Levite)

[Chabad](https://www.chabad.org/library/article_cdo/aid/4254752/jewish/Who-Were-the-Levites.htm)

[The Torah](https://www.thetorah.com/article/who-were-the-levites)

[New World Encyclopedia](https://www.newworldencyclopedia.org/entry/Levite)

[Jewish Encyclopedia](https://www.jewishencyclopedia.com/articles/9866-levites-temple-servants)
