# גְּרָר

[gᵊrār](https://www.blueletterbible.org/lexicon/h1642)

Definition: Gerar (10x).

Part of speech: proper locative noun

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1642)

[Study Light](https://www.studylight.org/lexicons/hebrew/1642.html)

[Bible Hub](https://biblehub.com/str/hebrew/1642.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01642)

[Bible Bento](https://biblebento.com/dictionary/H1642.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1642/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1642.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1642)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/gerar.html)

[Video Bible](https://www.videobible.com/bible-dictionary/gerar)