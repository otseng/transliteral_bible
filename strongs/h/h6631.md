# צֶאֱצָאִים

[ṣe'ĕṣā'îm](https://www.blueletterbible.org/lexicon/h6631)

Definition: offspring (9x), that cometh forth (1x), which cometh out (1x).

Part of speech: masculine noun

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6631)

[Study Light](https://www.studylight.org/lexicons/hebrew/6631.html)

[Bible Hub](https://biblehub.com/str/hebrew/6631.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06631)

[Bible Bento](https://biblebento.com/dictionary/H6631.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6631/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6631.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6631)
