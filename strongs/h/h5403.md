# נְשַׁר

[nᵊšar](https://www.blueletterbible.org/lexicon/h5403)

Definition: eagle (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5403)

[Study Light](https://www.studylight.org/lexicons/hebrew/5403.html)

[Bible Hub](https://biblehub.com/str/hebrew/5403.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05403)

[Bible Bento](https://biblebento.com/dictionary/H5403.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5403/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5403.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5403)
