# תְּחוֹת

[tᵊḥôṯ](https://www.blueletterbible.org/lexicon/h8460)

Definition: under (4x).

Part of speech: preposition

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8460)

[Study Light](https://www.studylight.org/lexicons/hebrew/8460.html)

[Bible Hub](https://biblehub.com/str/hebrew/8460.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08460)

[Bible Bento](https://biblebento.com/dictionary/H8460.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8460/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8460.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8460)
