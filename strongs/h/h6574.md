# פַרְשְׁדֹן

[p̄aršᵊḏōn](https://www.blueletterbible.org/lexicon/h6574)

Definition: dirt (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6574)

[Study Light](https://www.studylight.org/lexicons/hebrew/6574.html)

[Bible Hub](https://biblehub.com/str/hebrew/6574.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06574)

[Bible Bento](https://biblebento.com/dictionary/H6574.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6574/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6574.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6574)
