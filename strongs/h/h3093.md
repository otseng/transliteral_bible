# יָהִיר

[yāhîr](https://www.blueletterbible.org/lexicon/h3093)

Definition: haughty (1x), proud (1x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3093)

[Study Light](https://www.studylight.org/lexicons/hebrew/3093.html)

[Bible Hub](https://biblehub.com/str/hebrew/3093.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03093)

[Bible Bento](https://biblebento.com/dictionary/H3093.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3093/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3093.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3093)
