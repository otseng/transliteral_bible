# תַּבְעֵרָה

[taḇʿērâ](https://www.blueletterbible.org/lexicon/h8404)

Definition: Taberah (2x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8404)

[Study Light](https://www.studylight.org/lexicons/hebrew/8404.html)

[Bible Hub](https://biblehub.com/str/hebrew/8404.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08404)

[Bible Bento](https://biblebento.com/dictionary/H8404.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8404/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8404.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8404)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/T/taberah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/taberah)