# בָּל

[bāl](https://www.blueletterbible.org/lexicon/h1079)

Definition: heart (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1079)

[Study Light](https://www.studylight.org/lexicons/hebrew/1079.html)

[Bible Hub](https://biblehub.com/str/hebrew/1079.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01079)

[Bible Bento](https://biblebento.com/dictionary/H1079.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1079/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1079.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1079)
