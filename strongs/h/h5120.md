# נוּט

[nûṭ](https://www.blueletterbible.org/lexicon/h5120)

Definition: moved (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5120)

[Study Light](https://www.studylight.org/lexicons/hebrew/5120.html)

[Bible Hub](https://biblehub.com/str/hebrew/5120.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05120)

[Bible Bento](https://biblebento.com/dictionary/H5120.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5120/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5120.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5120)
