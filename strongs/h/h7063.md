# קמוש

[qmvש](https://www.blueletterbible.org/lexicon/h7063)

Definition: thorn (1x).

Part of speech: collective masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7063)

[Study Light](https://www.studylight.org/lexicons/hebrew/7063.html)

[Bible Hub](https://biblehub.com/str/hebrew/7063.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07063)

[Bible Bento](https://biblebento.com/dictionary/H7063.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7063/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7063.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7063)
