# כֵּהֶה

[kēhê](https://www.blueletterbible.org/lexicon/h3544)

Definition: somewhat dark (5x), darkish (1x), wax dim (1x), smoking (1x), heaviness (1x), dark color, coffee color

Part of speech: adjective

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3544)

[Study Light](https://www.studylight.org/lexicons/hebrew/3544.html)

[Bible Hub](https://biblehub.com/str/hebrew/3544.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03544)

[Bible Bento](https://biblebento.com/dictionary/H3544.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3544/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3544.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3544)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%9B%D7%94%D7%94)
