# חֲלוּשָׁה

[ḥălûšâ](https://www.blueletterbible.org/lexicon/h2476)

Definition: being overcome (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2476)

[Study Light](https://www.studylight.org/lexicons/hebrew/2476.html)

[Bible Hub](https://biblehub.com/str/hebrew/2476.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02476)

[Bible Bento](https://biblebento.com/dictionary/H2476.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2476/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2476.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2476)
