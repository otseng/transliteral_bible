# יִבֵּם

[yibēm](https://www.blueletterbible.org/lexicon/h2992)

Definition: perform the duty of an husband's brother (2x), marry (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2992)

[Study Light](https://www.studylight.org/lexicons/hebrew/2992.html)

[Bible Hub](https://biblehub.com/str/hebrew/2992.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02992)

[Bible Bento](https://biblebento.com/dictionary/H2992.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2992/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2992.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2992)
