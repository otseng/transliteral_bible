# זוּעַ

[zûaʿ](https://www.blueletterbible.org/lexicon/h2111)

Definition: moved (1x), tremble (1x), vex (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2111)

[Study Light](https://www.studylight.org/lexicons/hebrew/2111.html)

[Bible Hub](https://biblehub.com/str/hebrew/2111.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02111)

[Bible Bento](https://biblebento.com/dictionary/H2111.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2111/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2111.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2111)
