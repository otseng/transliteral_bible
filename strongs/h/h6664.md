# צֶדֶק

[tsedeq](https://www.blueletterbible.org/lexicon/h6664)

Definition: righteousness (77x), just (11x), justice (10x), righteous (8x), righteously (3x), right (3x), righteous cause (1x), unrighteousness (1x), to be right

Part of speech: masculine noun

Occurs 116 times in 109 verses

Hebrew: [tsaddiyq](../h/h6662.md), [ṣāḏaq](../h/h6663.md), [ṣiḏqâ](../h/h6665.md), [tsedaqah](../h/h6666.md)

Names: [Ṣḏqyh](../h/h6667.md), [Malkî-ṣeḏeq](../h/h4442.md)

Greek: [dikaiosynē](../g/g1343.md), [dikaioō](../g/g1344.md)

Edenics: syndicate

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6664)

[Study Light](https://www.studylight.org/lexicons/hebrew/6664.html)

[Bible Hub](https://biblehub.com/str/hebrew/6664.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A6%D6%B6%D7%93%D6%B6%D7%A7)

[NET Bible](http://classic.net.bible.org/strong.php?id=06664)

[Bible Bento](https://biblebento.com/dictionary/H6664.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6664/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6664.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6664)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A6%D7%93%D7%A7)

[Hebrew Thoughts](https://www.studylight.org/language-studies/hebrew-thoughts.html?article=554)

[Logos Apostolic](https://www.logosapostolic.org/hebrew-word-studies/6664-tsedeq-righteousness.htm)
