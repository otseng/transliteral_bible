# צִיצִת

[ṣîṣiṯ](https://www.blueletterbible.org/lexicon/h6734)

Definition: fringe (3x), lock (1x), tassel

Part of speech: feminine noun

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6734)

[Study Light](https://www.studylight.org/lexicons/hebrew/6734.html)

[Bible Hub](https://biblehub.com/str/hebrew/6734.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06734)

[Bible Bento](https://biblebento.com/dictionary/H6734.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6734/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6734.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6734)

[Wikipedia](https://en.wikipedia.org/wiki/Tzitzit)

[Chabad](https://www.chabad.org/library/article_cdo/aid/537949/jewish/What-Is-Tzitzit-and-Tallit.htm)

[Aish](https://aish.com/tzitzit-2/)

[The Torah](https://www.thetorah.com/article/what-do-tzitzit-represent)
