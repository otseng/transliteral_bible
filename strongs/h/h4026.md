# מִגְדָּל

[miḡdāl](https://www.blueletterbible.org/lexicon/h4026)

Definition: tower (47x), castles (1x), flowers (1x), pulpit (1x).

Part of speech: masculine noun

Occurs 49 times in 44 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4026)

[Study Light](https://www.studylight.org/lexicons/hebrew/4026.html)

[Bible Hub](https://biblehub.com/str/hebrew/4026.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04026)

[Bible Bento](https://biblebento.com/dictionary/H4026.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4026/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4026.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4026)
