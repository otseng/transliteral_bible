# מִבְנֶה

[miḇnê](https://www.blueletterbible.org/lexicon/h4011)

Definition: frame (1x), structure, building

Part of speech: masculine noun

Occurs 1 times in 1 verses

Greek: [oikodomē](../g/g3619.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4011)

[Study Light](https://www.studylight.org/lexicons/hebrew/4011.html)

[Bible Hub](https://biblehub.com/str/hebrew/4011.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04011)

[Bible Bento](https://biblebento.com/dictionary/H4011.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4011/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4011.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4011)
