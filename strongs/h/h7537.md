# רָקַב

[rāqaḇ](https://www.blueletterbible.org/lexicon/h7537)

Definition: rot (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7537)

[Study Light](https://www.studylight.org/lexicons/hebrew/7537.html)

[Bible Hub](https://biblehub.com/str/hebrew/7537.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07537)

[Bible Bento](https://biblebento.com/dictionary/H7537.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7537/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7537.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7537)
