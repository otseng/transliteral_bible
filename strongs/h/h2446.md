# חֲכַלְיָה

[Ḥăḵalyâ](https://www.blueletterbible.org/lexicon/h2446)

Definition: Hachaliah (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2446)

[Study Light](https://www.studylight.org/lexicons/hebrew/2446.html)

[Bible Hub](https://biblehub.com/str/hebrew/2446.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02446)

[Bible Bento](https://biblebento.com/dictionary/H2446.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2446/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2446.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2446)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hachaliah.html)