# לֵאָה

[Lē'â](https://www.blueletterbible.org/lexicon/h3812)

Definition: Leah (34x).

Part of speech: proper feminine noun

Occurs 34 times in 32 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3812)

[Study Light](https://www.studylight.org/lexicons/hebrew/3812.html)

[Bible Hub](https://biblehub.com/str/hebrew/3812.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03812)

[Bible Bento](https://biblebento.com/dictionary/H3812.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3812/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3812.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3812)

[Video Bible](https://www.videobible.com/bible-dictionary/leah)