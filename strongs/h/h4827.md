# מֵרַע

[mēraʿ](https://www.blueletterbible.org/lexicon/h4827)

Definition: mischief (1x).

Part of speech: participle

Occurs 1 times in 1 verses

Hebrew: [ra`a`](../h/h7489.md), [ra`](../h/h7451.md), [ra`a`](../h/h7489.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4827)

[Study Light](https://www.studylight.org/lexicons/hebrew/4827.html)

[Bible Hub](https://biblehub.com/str/hebrew/4827.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04827)

[Bible Bento](https://biblebento.com/dictionary/H4827.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4827/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4827.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4827)
