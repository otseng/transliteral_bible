# יָגֹר

[yāḡōr](https://www.blueletterbible.org/lexicon/h3025)

Definition: afraid (4x), fear (1x).

Part of speech: verb

Occurs 5 times in 5 verses

Synonyms: [fear](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Fear)

## Articles

[Study Light](https://www.studylight.org/lexicons/hebrew/3025.html)

[Bible Hub](https://biblehub.com/str/hebrew/3025.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03025)

[Bible Bento](https://biblebento.com/dictionary/H3025.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3025/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3025.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3025)
