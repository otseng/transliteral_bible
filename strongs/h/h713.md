# אַרְגָּמָן

['argāmān](https://www.blueletterbible.org/lexicon/h713)

Definition: purple (38x).

Part of speech: masculine noun

Occurs 38 times in 38 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0713)

[Study Light](https://www.studylight.org/lexicons/hebrew/713.html)

[Bible Hub](https://biblehub.com/str/hebrew/713.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0713)

[Bible Bento](https://biblebento.com/dictionary/H713.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/713/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/713.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h713)
