# יְהוֹשַבְעַת

[YᵊhôשAḇʿAṯ](https://www.blueletterbible.org/lexicon/h3090)

Definition: Jehoshabeath (2x).

Part of speech: proper feminine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3090)

[Study Light](https://www.studylight.org/lexicons/hebrew/3090.html)

[Bible Hub](https://biblehub.com/str/hebrew/3090.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03090)

[Bible Bento](https://biblebento.com/dictionary/H3090.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3090/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3090.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3090)
