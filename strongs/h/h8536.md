# תַּמָּה

[tammâ](https://www.blueletterbible.org/lexicon/h8536)

Definition: there (2x), where (1x), thence (1x).

Part of speech: adverb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8536)

[Study Light](https://www.studylight.org/lexicons/hebrew/8536.html)

[Bible Hub](https://biblehub.com/str/hebrew/8536.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08536)

[Bible Bento](https://biblebento.com/dictionary/H8536.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8536/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8536.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8536)
