# תּוֹעֵבָה

[tôʿēḇâ](https://www.blueletterbible.org/lexicon/h8441)

Definition: abomination (113x), abominable thing (2x), abominable (2x).

Part of speech: feminine noun

Occurs 118 times in 112 verses

Hebrew: [ta`ab](../h/h8581.md), [šiqqûṣ](../h/h8251.md)

Greek: [anomia](../g/g458.md)

Synonyms: [abomination](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Abomination)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8441)

[Study Light](https://www.studylight.org/lexicons/hebrew/8441.html)

[Bible Hub](https://biblehub.com/str/hebrew/8441.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08441)

[Bible Bento](https://biblebento.com/dictionary/H8441.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8441/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8441.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8441)

[Wikipedia](https://en.wikipedia.org/wiki/Abomination_%28Bible%29)

[Jewish Virtual Library](https://www.jewishvirtuallibrary.org/abomination-jewish-virtual-library)

[Jewish Encyclopedia](https://www.jewishencyclopedia.com/articles/352-abomination)
