# קֶרֶן הַפּוּךְ

[Qeren Hapûḵ](https://www.blueletterbible.org/lexicon/h7163)

Definition: Kerenhappuch (1x).

Part of speech: proper feminine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7163)

[Study Light](https://www.studylight.org/lexicons/hebrew/7163.html)

[Bible Hub](https://biblehub.com/str/hebrew/7163.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07163)

[Bible Bento](https://biblebento.com/dictionary/H7163.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7163/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7163.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7163)
