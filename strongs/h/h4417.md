# מֶלַח

[melaḥ](https://www.blueletterbible.org/lexicon/h4417)

Definition: salt (27x), saltpits (with H4379) (1x).

Part of speech: masculine noun

Occurs 29 times in 26 verses

Hebrew: [mālaḥ](../h/h4414.md)

Greek: [halas](../g/g217.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4417)

[Study Light](https://www.studylight.org/lexicons/hebrew/4417.html)

[Bible Hub](https://biblehub.com/str/hebrew/4417.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04417)

[Bible Bento](https://biblebento.com/dictionary/H4417.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4417/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4417.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4417)
