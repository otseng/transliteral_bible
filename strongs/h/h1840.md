# דִּנִיֵּאל

[Dinîyē'L](https://www.blueletterbible.org/lexicon/h1840)

Definition: Daniel (29x), "God is my judge"

Part of speech: proper masculine noun

Occurs 32 times in 28 verses

Hebrew: [Dān](../h/h1835.md), ['el](../h/h410.md), [Dānîyē'L](../h/h1841.md)

Greek: [Daniēl](../g/g1158.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1840)

[Study Light](https://www.studylight.org/lexicons/hebrew/1840.html)

[Bible Hub](https://biblehub.com/str/hebrew/1840.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01840)

[Bible Bento](https://biblebento.com/dictionary/H1840.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1840/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1840.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1840)

[Wikipedia](https://en.wikipedia.org/wiki/Daniel_%28biblical_figure%29)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/daniel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/daniel)