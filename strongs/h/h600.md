# אֲנַף

['ănap̄](https://www.blueletterbible.org/lexicon/h600)

Definition: face (1x), visage (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h600)

[Study Light](https://www.studylight.org/lexicons/hebrew/600.html)

[Bible Hub](https://biblehub.com/str/hebrew/600.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0600)

[Bible Bento](https://biblebento.com/dictionary/H600.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/600/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/600.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h600)
