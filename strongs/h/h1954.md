# הוֹשֵׁעַ

[Hôšēaʿ](https://www.blueletterbible.org/lexicon/h1954)

Definition: Hoshea (11x), Hosea (3x), Oshea (3x), "salvation

Part of speech: proper masculine noun

Occurs 16 times in 15 verses

Hebrew: [yasha'](../h/h3467.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1954)

[Study Light](https://www.studylight.org/lexicons/hebrew/1954.html)

[Bible Hub](https://biblehub.com/str/hebrew/1954.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01954)

[Bible Bento](https://biblebento.com/dictionary/H1954.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1954/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1954.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1954)

[Wikipedia](https://en.wikipedia.org/wiki/Hosea)

[Video Bible](https://www.videobible.com/bible-dictionary/hoshea)