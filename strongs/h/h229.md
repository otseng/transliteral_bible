# אֶזְבַּי

['Ezbay](https://www.blueletterbible.org/lexicon/h229)

Definition: Ezbai (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h229)

[Study Light](https://www.studylight.org/lexicons/hebrew/229.html)

[Bible Hub](https://biblehub.com/str/hebrew/229.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0229)

[Bible Bento](https://biblebento.com/dictionary/H229.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/229/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/229.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h229)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/ezbai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ezbai)