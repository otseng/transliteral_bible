# שַׁחַף

[šaḥap̄](https://www.blueletterbible.org/lexicon/h7828)

Definition: cuckow (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7828)

[Study Light](https://www.studylight.org/lexicons/hebrew/7828.html)

[Bible Hub](https://biblehub.com/str/hebrew/7828.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07828)

[Bible Bento](https://biblebento.com/dictionary/H7828.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7828/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7828.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7828)
