# חֲסִידָה

[ḥăsîḏâ](https://www.blueletterbible.org/lexicon/h2624)

Definition: stork (5x), feathers (1x).

Part of speech: feminine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2624)

[Study Light](https://www.studylight.org/lexicons/hebrew/2624.html)

[Bible Hub](https://biblehub.com/str/hebrew/2624.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02624)

[Bible Bento](https://biblebento.com/dictionary/H2624.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2624/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2624.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2624)
