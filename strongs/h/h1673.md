# דֹּאֵג

[Dō'Ēḡ](https://www.blueletterbible.org/lexicon/h1673)

Definition: Doeg (6x).

Part of speech: proper masculine noun

Occurs 9 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1673)

[Study Light](https://www.studylight.org/lexicons/hebrew/1673.html)

[Bible Hub](https://biblehub.com/str/hebrew/1673.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01673)

[Bible Bento](https://biblebento.com/dictionary/H1673.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1673/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1673.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1673)

[Video Bible](https://www.videobible.com/bible-dictionary/doeg)