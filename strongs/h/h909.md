# בָּדַד

[bāḏaḏ](https://www.blueletterbible.org/lexicon/h909)

Definition: alone (3x).

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h909)

[Study Light](https://www.studylight.org/lexicons/hebrew/909.html)

[Bible Hub](https://biblehub.com/str/hebrew/909.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0909)

[Bible Bento](https://biblebento.com/dictionary/H909.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/909/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/909.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h909)
