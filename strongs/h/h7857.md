# שָׁטַף

[šāṭap̄](https://www.blueletterbible.org/lexicon/h7857)

Definition: overflow (20x), rinsed (3x), wash away (2x), drown (1x), flowing (1x), miscellaneous (4x).

Part of speech: verb

Occurs 31 times in 30 verses

Greek: [niptō](../g/g3538.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7857)

[Study Light](https://www.studylight.org/lexicons/hebrew/7857.html)

[Bible Hub](https://biblehub.com/str/hebrew/7857.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07857)

[Bible Bento](https://biblebento.com/dictionary/H7857.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7857/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7857.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7857)
