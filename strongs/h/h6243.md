# עשרין

[ʿשryn](https://www.blueletterbible.org/lexicon/h6243)

Definition: twenty (1x).

Part of speech: masculine/feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6243)

[Study Light](https://www.studylight.org/lexicons/hebrew/6243.html)

[Bible Hub](https://biblehub.com/str/hebrew/6243.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06243)

[Bible Bento](https://biblebento.com/dictionary/H6243.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6243/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6243.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6243)
