# דֶּבֶק

[deḇeq](https://www.blueletterbible.org/lexicon/h1694)

Definition: joint (2x), sodering (1x), soldering, joining, appendage, riveting

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1694)

[Study Light](https://www.studylight.org/lexicons/hebrew/1694.html)

[Bible Hub](https://biblehub.com/str/hebrew/1694.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01694)

[Bible Bento](https://biblebento.com/dictionary/H1694.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1694/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1694.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1694)
