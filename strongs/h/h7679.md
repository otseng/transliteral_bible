# שָׂגָא

[śāḡā'](https://www.blueletterbible.org/lexicon/h7679)

Definition: magnify (1x), increase (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7679)

[Study Light](https://www.studylight.org/lexicons/hebrew/7679.html)

[Bible Hub](https://biblehub.com/str/hebrew/7679.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07679)

[Bible Bento](https://biblebento.com/dictionary/H7679.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7679/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7679.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7679)
