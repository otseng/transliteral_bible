# עֳפִי

[ʿŏp̄î](https://www.blueletterbible.org/lexicon/h6073)

Definition: branches (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6073)

[Study Light](https://www.studylight.org/lexicons/hebrew/6073.html)

[Bible Hub](https://biblehub.com/str/hebrew/6073.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06073)

[Bible Bento](https://biblebento.com/dictionary/H6073.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6073/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6073.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6073)
