# יְעוּשׁ

[Yᵊʿûš](https://www.blueletterbible.org/lexicon/h3266)

Definition: Jeush (5x), Jehush (1x).

Part of speech: proper masculine noun

Occurs 10 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3266)

[Study Light](https://www.studylight.org/lexicons/hebrew/3266.html)

[Bible Hub](https://biblehub.com/str/hebrew/3266.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03266)

[Bible Bento](https://biblebento.com/dictionary/H3266.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3266/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3266.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3266)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jeush.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jeush)