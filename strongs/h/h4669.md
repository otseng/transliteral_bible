# מִפְתָּח

[mip̄tāḥ](https://www.blueletterbible.org/lexicon/h4669)

Definition: opening (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

Hebrew: [pāṯaḥ](../h/h6605.md), [pĕthach](../h/h6606.md), [peṯaḥ](../h/h6607.md), [pēṯaḥ](../h/h6608.md), [pᵊṯîḥâ](../h/h6609.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4669)

[Study Light](https://www.studylight.org/lexicons/hebrew/4669.html)

[Bible Hub](https://biblehub.com/str/hebrew/4669.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04669)

[Bible Bento](https://biblebento.com/dictionary/H4669.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4669/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4669.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4669)
