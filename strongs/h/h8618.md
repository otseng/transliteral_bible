# תְּקוֹמֵם

[tᵊqômēm](https://www.blueletterbible.org/lexicon/h8618)

Definition: that rise up against thee (1x).

Part of speech: masculine noun, participle

Occurs 1 times in 1 verses

Hebrew: [quwm](../h/h6965.md), [qûm](../h/h6966.md), [qômâ](../h/h6967.md), [qômmîyûṯ](../h/h6968.md), [qᵊyām](../h/h7010.md), [qayyām](../h/h7011.md), [qîmâ](../h/h7012.md), [tᵊqûmâ](../h/h8617.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8618)

[Study Light](https://www.studylight.org/lexicons/hebrew/8618.html)

[Bible Hub](https://biblehub.com/str/hebrew/8618.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08618)

[Bible Bento](https://biblebento.com/dictionary/H8618.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8618/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8618.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8618)
