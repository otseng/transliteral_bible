# שׁוּר

[šûr](https://www.blueletterbible.org/lexicon/h7793)

Definition: Shur (6x).

Part of speech: proper locative noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7793)

[Study Light](https://www.studylight.org/lexicons/hebrew/7793.html)

[Bible Hub](https://biblehub.com/str/hebrew/7793.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07793)

[Bible Bento](https://biblebento.com/dictionary/H7793.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7793/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7793.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7793)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shur.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shur)