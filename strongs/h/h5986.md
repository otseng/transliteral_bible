# עָמוֹס

[ʿĀmôs](https://www.blueletterbible.org/lexicon/h5986)

Definition: Amos (7x), "burden"

Part of speech: proper masculine noun

Occurs 7 times in 7 verses

Hebrew: [ʿāmas](../h/h6006.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5986)

[Study Light](https://www.studylight.org/lexicons/hebrew/5986.html)

[Bible Hub](https://biblehub.com/str/hebrew/5986.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05986)

[Bible Bento](https://biblebento.com/dictionary/H5986.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5986/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5986.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5986)

[Wikipedia](https://en.wikipedia.org/wiki/Amos_%28prophet%29)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/amos.html)

[Video Bible](https://www.videobible.com/bible-dictionary/amos)