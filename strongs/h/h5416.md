# נָתָן

[Nāṯān](https://www.blueletterbible.org/lexicon/h5416)

Definition: Nathan (42x).

Part of speech: proper masculine noun

Occurs 42 times in 39 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5416)

[Study Light](https://www.studylight.org/lexicons/hebrew/5416.html)

[Bible Hub](https://biblehub.com/str/hebrew/5416.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05416)

[Bible Bento](https://biblebento.com/dictionary/H5416.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5416/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5416.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5416)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/nathan.html)

[Video Bible](https://www.videobible.com/bible-dictionary/nathan)