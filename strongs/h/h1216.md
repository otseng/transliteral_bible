# בָּצֵק

[bāṣēq](https://www.blueletterbible.org/lexicon/h1216)

Definition: swell (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1216)

[Study Light](https://www.studylight.org/lexicons/hebrew/1216.html)

[Bible Hub](https://biblehub.com/str/hebrew/1216.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01216)

[Bible Bento](https://biblebento.com/dictionary/H1216.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1216/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1216.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1216)
