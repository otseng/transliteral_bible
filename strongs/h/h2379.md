# חֲזוֹת

[ḥăzôṯ](https://www.blueletterbible.org/lexicon/h2379)

Definition: sight thereof (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

Hebrew: [chazah](../h/h2372.md), [ḥēzev](../h/h2376.md), [ḥāzôn](../h/h2377.md), [ḥāzûṯ](../h/h2380.md), [ḥizzāyôn](../h/h2384.md), [maḥăzê](../h/h4236.md), [meḥĕzâ](../h/h4237.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2379)

[Study Light](https://www.studylight.org/lexicons/hebrew/2379.html)

[Bible Hub](https://biblehub.com/str/hebrew/2379.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02379)

[Bible Bento](https://biblebento.com/dictionary/H2379.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2379/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2379.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2379)
