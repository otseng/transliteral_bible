# נֵדֶה

[nēḏê](https://www.blueletterbible.org/lexicon/h5078)

Definition: gifts (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5078)

[Study Light](https://www.studylight.org/lexicons/hebrew/5078.html)

[Bible Hub](https://biblehub.com/str/hebrew/5078.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05078)

[Bible Bento](https://biblebento.com/dictionary/H5078.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5078/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5078.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5078)
