# זָבַל

[zāḇal](https://www.blueletterbible.org/lexicon/h2082)

Definition: dwell with me (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2082)

[Study Light](https://www.studylight.org/lexicons/hebrew/2082.html)

[Bible Hub](https://biblehub.com/str/hebrew/2082.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02082)

[Bible Bento](https://biblebento.com/dictionary/H2082.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2082/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2082.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2082)
