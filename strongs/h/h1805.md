# דְּלִי

[dᵊlî](https://www.blueletterbible.org/lexicon/h1805)

Definition: bucket (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1805)

[Study Light](https://www.studylight.org/lexicons/hebrew/1805.html)

[Bible Hub](https://biblehub.com/str/hebrew/1805.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01805)

[Bible Bento](https://biblebento.com/dictionary/H1805.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1805/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1805.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1805)
