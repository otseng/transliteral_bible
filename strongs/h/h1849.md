# דָּפַק

[dāp̄aq](https://www.blueletterbible.org/lexicon/h1849)

Definition: overdrive (1x), knock (1x), beat (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1849)

[Study Light](https://www.studylight.org/lexicons/hebrew/1849.html)

[Bible Hub](https://biblehub.com/str/hebrew/1849.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01849)

[Bible Bento](https://biblebento.com/dictionary/H1849.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1849/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1849.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1849)
