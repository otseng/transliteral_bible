# אֲבִיגַיִל

['Ăḇîḡayil](https://www.blueletterbible.org/lexicon/h26)

Definition: Abigail (17x), "my father is joy"

Part of speech: proper feminine noun

Occurs 19 times in 17 verses

Hebrew: [gîl](../h/h1524.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h26)

[Study Light](https://www.studylight.org/lexicons/hebrew/26.html)

[Bible Hub](https://biblehub.com/str/hebrew/26.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=026)

[Bible Bento](https://biblebento.com/dictionary/H26.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/26/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/26.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h26)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/abigail.html)

[Video Bible](https://www.videobible.com/bible-dictionary/abigail)