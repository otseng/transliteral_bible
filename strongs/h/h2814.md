# חָשָׁה

[ḥāšâ](https://www.blueletterbible.org/lexicon/h2814)

Definition: hold...peace (9x), still (4x), silence (2x), silent (1x).

Part of speech: verb

Occurs 16 times in 16 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2814)

[Study Light](https://www.studylight.org/lexicons/hebrew/2814.html)

[Bible Hub](https://biblehub.com/str/hebrew/2814.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02814)

[Bible Bento](https://biblebento.com/dictionary/H2814.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2814/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2814.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2814)
