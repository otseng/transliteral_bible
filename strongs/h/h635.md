# אֶסְתֵּר

['Estēr](https://www.blueletterbible.org/lexicon/h635)

Definition: Esther (55x).

Part of speech: proper feminine noun

Occurs 55 times in 45 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h635)

[Study Light](https://www.studylight.org/lexicons/hebrew/635.html)

[Bible Hub](https://biblehub.com/str/hebrew/635.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0635)

[Bible Bento](https://biblebento.com/dictionary/H635.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/635/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/635.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h635)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/esther.html)

[Video Bible](https://www.videobible.com/bible-dictionary/esther)