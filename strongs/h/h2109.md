# זוּן

[zûn](https://www.blueletterbible.org/lexicon/h2109)

Definition: fed (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2109)

[Study Light](https://www.studylight.org/lexicons/hebrew/2109.html)

[Bible Hub](https://biblehub.com/str/hebrew/2109.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02109)

[Bible Bento](https://biblebento.com/dictionary/H2109.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2109/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2109.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2109)
