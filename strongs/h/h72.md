# אֶבֶן הָעֶזֶר

['eḇen hāʿezer](https://www.blueletterbible.org/lexicon/h72)

Definition: Ebenezer (3x), "stone of help"

Part of speech: feminine noun

Occurs 6 times in 3 verses

Notes: memorial stone erected by Samuel to mark where God helped Israel to defeat the Philistines — north of Jerusalem

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h72)

[Study Light](https://www.studylight.org/lexicons/hebrew/72.html)

[Bible Hub](https://biblehub.com/str/hebrew/72.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=072)

[Bible Bento](https://biblebento.com/dictionary/H72.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/72/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/72.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h72)
