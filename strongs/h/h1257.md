# בַּרְבֻּרִים

[barburîm](https://www.blueletterbible.org/lexicon/h1257)

Definition: fowl (1x).

Part of speech: masculine plural noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1257)

[Study Light](https://www.studylight.org/lexicons/hebrew/1257.html)

[Bible Hub](https://biblehub.com/str/hebrew/1257.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01257)

[Bible Bento](https://biblebento.com/dictionary/H1257.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1257/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1257.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1257)
