# פֶּרַח

[peraḥ](https://www.blueletterbible.org/lexicon/h6525)

Definition: flower (14x), bud (2x), blossom (1x).

Part of speech: masculine noun

Occurs 17 times in 15 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6525)

[Study Light](https://www.studylight.org/lexicons/hebrew/6525.html)

[Bible Hub](https://biblehub.com/str/hebrew/6525.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06525)

[Bible Bento](https://biblebento.com/dictionary/H6525.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6525/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6525.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6525)
