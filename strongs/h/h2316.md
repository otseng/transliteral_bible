# חֲדַר

[ḥăḏar](https://www.blueletterbible.org/lexicon/h2316)

Definition: Hadar (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2316)

[Study Light](https://www.studylight.org/lexicons/hebrew/2316.html)

[Bible Hub](https://biblehub.com/str/hebrew/2316.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02316)

[Bible Bento](https://biblebento.com/dictionary/H2316.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2316/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2316.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2316)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hadar.html)