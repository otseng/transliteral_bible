# תְּפִלָּה

[tĕphillah](https://www.blueletterbible.org/lexicon/h8605)

Definition: prayer (77x), intercession, supplication, entreaty

Part of speech: feminine noun

Occurs 77 times in 70 verses

Hebrew: [palal](../h/h6419.md), [pᵊlîlâ](../h/h6415.md), [pᵊlîlî](../h/h6416.md), [pᵊlîlîyâ](../h/h6417.md)

Greek: [proseuchē](../g/g4335.md)

Pictoral: speak to authority

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8605)

[Study Light](https://www.studylight.org/lexicons/hebrew/8605.html)

[Bible Hub](https://biblehub.com/str/hebrew/8605.htm)

[Morfix](https://www.morfix.co.il/en/%D7%AA%D6%BC%D6%B0%D7%A4%D6%B4%D7%9C%D6%BC%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=08605)

[Bible Bento](https://biblebento.com/dictionary/H8605.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8605/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8605.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8605)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%AA%D7%A4%D7%99%D7%9C%D7%94)
