# צָוָה

[tsavah](https://www.blueletterbible.org/lexicon/h6680)

Definition: command (430x), charge (39x), commandment (9x), appoint (5x), bade (3x), order (3x), commander (1x), to direct, guide towards goal

Part of speech: verb

Occurs 494 times in 475 verses

Hebrew: [mitsvah](../h/h4687.md), [ṣav](../h/h6673.md)

Greek: [entellō](../g/g1781.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6680)

[Study Light](https://www.studylight.org/lexicons/hebrew/6680.html)

[Bible Hub](https://biblehub.com/str/hebrew/6680.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A6%D6%B8%D7%95%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=06680)

[Bible Bento](https://biblebento.com/dictionary/H6680.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6680/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6680.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6680)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A6%D7%95%D7%94)

[Ancient Hebrew](https://www.ancient-hebrew.org/living-words/the-living-words-command.htm)
