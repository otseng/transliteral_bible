# תְּאַשּׁוּר

[tᵊ'aššûr](https://www.blueletterbible.org/lexicon/h8391)

Definition: box tree (1x), box (1x), cypress or cedar

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8391)

[Study Light](https://www.studylight.org/lexicons/hebrew/8391.html)

[Bible Hub](https://biblehub.com/str/hebrew/8391.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08391)

[Bible Bento](https://biblebento.com/dictionary/H8391.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8391/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8391.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8391)

[Plants of the Bible](https://ww2.odu.edu/~lmusselm/plant/bible/cypress.php)
