# בַּהַט

[bahaṭ](https://www.blueletterbible.org/lexicon/h923)

Definition: red (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h923)

[Study Light](https://www.studylight.org/lexicons/hebrew/923.html)

[Bible Hub](https://biblehub.com/str/hebrew/923.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0923)

[Bible Bento](https://biblebento.com/dictionary/H923.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/923/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/923.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h923)
