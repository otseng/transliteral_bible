# לָהֵם

[lāhēm](https://www.blueletterbible.org/lexicon/h3859)

Definition: wounds (2x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3859)

[Study Light](https://www.studylight.org/lexicons/hebrew/3859.html)

[Bible Hub](https://biblehub.com/str/hebrew/3859.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03859)

[Bible Bento](https://biblebento.com/dictionary/H3859.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3859/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3859.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3859)
