# עֵרָנִי

[ʿērānî](https://www.blueletterbible.org/lexicon/h6198)

Definition: Eranites (1x).

Part of speech: proper adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6198)

[Study Light](https://www.studylight.org/lexicons/hebrew/6198.html)

[Bible Hub](https://biblehub.com/str/hebrew/6198.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06198)

[Bible Bento](https://biblebento.com/dictionary/H6198.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6198/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6198.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6198)
