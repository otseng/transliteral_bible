# מוּת

[muwth](https://www.blueletterbible.org/lexicon/h4191)

Definition: die (424x), dead (130x), slay (100x), death (83x), surely (50x), kill (31x), dead man (3x), dead body (2x), in no wise (2x)

Part of speech: verb

Occurs 835 times in 694 verses

Hebrew: [môṯ](../h/h4193.md), [maveth](../h/h4194.md), [māmôṯ](../h/h4463.md)

Names: [Mᵊṯûšelaḥ](../h/h4968.md)

Greek: [thnēskō](../g/g2348.md), [apothnēskō](../g/g599.md)

Edenics: Checkmate, matador, meat

Synonyms: [kill](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Kill)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4191)

[Study Light](https://www.studylight.org/lexicons/hebrew/4191.html)

[Bible Hub](https://biblehub.com/str/hebrew/4191.htm)

[Morfix](https://www.morfix.co.il/en/%D7%9E%D7%95%D6%BC%D7%AA)

[NET Bible](http://classic.net.bible.org/strong.php?id=04191)

[Bible Bento](https://biblebento.com/dictionary/H4191.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4191/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4191.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4191)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%9E%D7%95%D7%AA)

[Logos Apostolic](https://www.logosapostolic.org/hebrew-word-studies/4191-muth-dead.htm)
