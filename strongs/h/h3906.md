# לַחַץ

[laḥaṣ](https://www.blueletterbible.org/lexicon/h3906)

Definition: oppression (7x), affliction (5x), distress, pressure

Part of speech: masculine noun

Occurs 12 times in 10 verses

Hebrew: [lāḥaṣ](../h/h3905.md)

Greek: [thlipsis](../g/g2347.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3906)

[Study Light](https://www.studylight.org/lexicons/hebrew/3906.html)

[Bible Hub](https://biblehub.com/str/hebrew/3906.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03906)

[Bible Bento](https://biblebento.com/dictionary/H3906.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3906/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3906.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3906)
