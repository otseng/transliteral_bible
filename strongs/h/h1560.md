# גְּלָל

[gᵊlāl](https://www.blueletterbible.org/lexicon/h1560)

Definition: great (2x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1560)

[Study Light](https://www.studylight.org/lexicons/hebrew/1560.html)

[Bible Hub](https://biblehub.com/str/hebrew/1560.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01560)

[Bible Bento](https://biblebento.com/dictionary/H1560.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1560/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1560.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1560)
