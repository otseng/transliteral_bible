# מֹרֶךְ

[mōreḵ](https://www.blueletterbible.org/lexicon/h4816)

Definition: faintness (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4816)

[Study Light](https://www.studylight.org/lexicons/hebrew/4816.html)

[Bible Hub](https://biblehub.com/str/hebrew/4816.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04816)

[Bible Bento](https://biblebento.com/dictionary/H4816.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4816/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4816.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4816)
