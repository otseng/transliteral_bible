# אֵלָה

['Ēlâ](https://www.blueletterbible.org/lexicon/h425)

Definition: Elah (16x).

Part of speech: proper locative noun, proper masculine noun

Occurs 16 times in 15 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h425)

[Study Light](https://www.studylight.org/lexicons/hebrew/425.html)

[Bible Hub](https://biblehub.com/str/hebrew/425.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0425)

[Bible Bento](https://biblebento.com/dictionary/H425.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/425/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/425.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h425)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/elah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/elah)