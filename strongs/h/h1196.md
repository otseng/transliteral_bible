# בַּעֲנָה

[BaʿĂnâ](https://www.blueletterbible.org/lexicon/h1196)

Definition: Baanah (9x).

Part of speech: proper masculine noun

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1196)

[Study Light](https://www.studylight.org/lexicons/hebrew/1196.html)

[Bible Hub](https://biblehub.com/str/hebrew/1196.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01196)

[Bible Bento](https://biblebento.com/dictionary/H1196.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1196/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1196.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1196)

[Video Bible](https://www.videobible.com/bible-dictionary/baanah)