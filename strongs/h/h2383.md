# חֶזְיוֹן

[Ḥezyôn](https://www.blueletterbible.org/lexicon/h2383)

Definition: Hezion (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2383)

[Study Light](https://www.studylight.org/lexicons/hebrew/2383.html)

[Bible Hub](https://biblehub.com/str/hebrew/2383.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02383)

[Bible Bento](https://biblebento.com/dictionary/H2383.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2383/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2383.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2383)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hezion.html)

[Video Bible](https://www.videobible.com/bible-dictionary/hezion)