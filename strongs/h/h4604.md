# מַעַל

[maʿal](https://www.blueletterbible.org/lexicon/h4604)

Definition: trespass (17x), transgression (6x), trespassed (2x), falsehood (1x), grievously (1x), sore (1x), very (1x).

Part of speech: masculine noun

Occurs 28 times in 28 verses

Hebrew: [māʿal](../h/h4603.md)

Greek: [adikia](../g/g93.md), [apostasia](../g/g646.md), [paraptōma](../g/g3900.md)

Edenics: malevolent

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4604)

[Study Light](https://www.studylight.org/lexicons/hebrew/4604.html)

[Bible Hub](https://biblehub.com/str/hebrew/4604.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04604)

[Bible Bento](https://biblebento.com/dictionary/H4604.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4604/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4604.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4604)
