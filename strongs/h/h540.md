# אֲמַן

['ăman](https://www.blueletterbible.org/lexicon/h540)

Definition: believe (1x), sure (1x), faithful (1x).

Part of speech: verb

Occurs 3 times in 3 verses

Hebrew: ['ēmûn](../h/h529.md), ['ĕmûnâ](../h/h530.md), ['aman](../h/h539.md), ['amen](../h/h543.md), ['ōmen](../h/h544.md), ['emeth](../h/h571.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h540)

[Study Light](https://www.studylight.org/lexicons/hebrew/540.html)

[Bible Hub](https://biblehub.com/str/hebrew/540.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0540)

[Bible Bento](https://biblebento.com/dictionary/H540.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/540/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/540.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h540)
