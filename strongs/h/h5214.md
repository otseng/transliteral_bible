# נִיר

[nîr](https://www.blueletterbible.org/lexicon/h5214)

Definition: break up (2x).

Part of speech: verb

Occurs 2 times in 2 verses

Hebrew: [nîr](../h/h5216.md), [nᵊhārâ](../h/h5105.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5214)

[Study Light](https://www.studylight.org/lexicons/hebrew/5214.html)

[Bible Hub](https://biblehub.com/str/hebrew/5214.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05214)

[Bible Bento](https://biblebento.com/dictionary/H5214.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5214/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5214.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5214)
