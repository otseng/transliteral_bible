# לוּץ

[luwts](https://www.blueletterbible.org/lexicon/h3887)

Definition: scorner (14x), scorn (4x), interpreter (2x), mocker (2x), ambassadors (1x), derision (1x), mock (1x), scornful (1x), teachers (1x)

Part of speech: verb

Occurs 27 times in 26 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3887)

[Study Light](https://www.studylight.org/lexicons/hebrew/3887.html)

[Bible Hub](https://biblehub.com/str/hebrew/3887.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03887)

[Bible Bento](https://biblebento.com/dictionary/H3887.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3887/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3887.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3887)