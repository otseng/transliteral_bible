# עָרַב

[ʿāraḇ](https://www.blueletterbible.org/lexicon/h6150)

Definition: evening (2x), darkened (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6150)

[Study Light](https://www.studylight.org/lexicons/hebrew/6150.html)

[Bible Hub](https://biblehub.com/str/hebrew/6150.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06150)

[Bible Bento](https://biblebento.com/dictionary/H6150.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6150/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6150.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6150)
