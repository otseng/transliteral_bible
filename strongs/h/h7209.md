# רְאִי

[rᵊ'î](https://www.blueletterbible.org/lexicon/h7209)

Definition: looking glass (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7209)

[Study Light](https://www.studylight.org/lexicons/hebrew/7209.html)

[Bible Hub](https://biblehub.com/str/hebrew/7209.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07209)

[Bible Bento](https://biblebento.com/dictionary/H7209.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7209/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7209.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7209)
