# כְּמוֹ

[kᵊmô](https://www.blueletterbible.org/lexicon/h3644)

Definition: and when, as thyself, like me, according to it, worth.

Part of speech: adverb, conjunction

Occurs 142 times in 126 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3644)

[Study Light](https://www.studylight.org/lexicons/hebrew/3644.html)

[Bible Hub](https://biblehub.com/str/hebrew/3644.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03644)

[Bible Bento](https://biblebento.com/dictionary/H3644.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3644/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3644.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3644)
