# צִפֹּרֶן

[ṣipōren](https://www.blueletterbible.org/lexicon/h6856)

Definition: nail (1x), point (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6856)

[Study Light](https://www.studylight.org/lexicons/hebrew/6856.html)

[Bible Hub](https://biblehub.com/str/hebrew/6856.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06856)

[Bible Bento](https://biblebento.com/dictionary/H6856.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6856/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6856.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6856)
