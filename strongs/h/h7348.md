# רְחוּם

[Rᵊḥûm](https://www.blueletterbible.org/lexicon/h7348)

Definition: Rehum (8x).

Part of speech: proper masculine noun

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7348)

[Study Light](https://www.studylight.org/lexicons/hebrew/7348.html)

[Bible Hub](https://biblehub.com/str/hebrew/7348.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07348)

[Bible Bento](https://biblebento.com/dictionary/H7348.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7348/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7348.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7348)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/R/rehum.html)

[Video Bible](https://www.videobible.com/bible-dictionary/rehum)