# יָאָה

[yā'â](https://www.blueletterbible.org/lexicon/h2969)

Definition: appertain (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2969)

[Study Light](https://www.studylight.org/lexicons/hebrew/2969.html)

[Bible Hub](https://biblehub.com/str/hebrew/2969.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02969)

[Bible Bento](https://biblebento.com/dictionary/H2969.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2969/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2969.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2969)
