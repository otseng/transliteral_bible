# שׁוּשַׁן

[Šûšan](https://www.blueletterbible.org/lexicon/h7800)

Definition: Shushan (21x), "lily", the winter residence of the Persian kings

Part of speech: proper locative noun

Occurs 21 times in 19 verses

Hebrew: [šûšan](../h/h7799.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7800)

[Study Light](https://www.studylight.org/lexicons/hebrew/7800.html)

[Bible Hub](https://biblehub.com/str/hebrew/7800.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07800)

[Bible Bento](https://biblebento.com/dictionary/H7800.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7800/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7800.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7800)

[Wikipedia](https://en.wikipedia.org/wiki/Susa)

[Jewish Encyclopedia](https://www.jewishencyclopedia.com/articles/13621-shushan)

[Encyclopedia](https://www.encyclopedia.com/religion/encyclopedias-almanacs-transcripts-and-maps/shushan)

[Bible Hub Topic](https://biblehub.com/topical/s/shushan.htm)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shushan.html)