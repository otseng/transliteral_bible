# רָטַשׁ

[rāṭaš](https://www.blueletterbible.org/lexicon/h7376)

Definition: dash...pieces (5x), dash (1x).

Part of speech: verb

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7376)

[Study Light](https://www.studylight.org/lexicons/hebrew/7376.html)

[Bible Hub](https://biblehub.com/str/hebrew/7376.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07376)

[Bible Bento](https://biblebento.com/dictionary/H7376.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7376/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7376.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7376)
