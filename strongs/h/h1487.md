# גּוּשׁ

[gûš](https://www.blueletterbible.org/lexicon/h1487)

Definition: clod (1x).

Part of speech: masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1487)

[Study Light](https://www.studylight.org/lexicons/hebrew/1487.html)

[Bible Hub](https://biblehub.com/str/hebrew/1487.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01487)

[Bible Bento](https://biblebento.com/dictionary/H1487.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1487/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1487.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1487)
