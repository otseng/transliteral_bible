# אֲדָר

['ăḏār](https://www.blueletterbible.org/lexicon/h143)

Definition: Adar (8x).

Part of speech: proper noun

Occurs 8 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h143)

[Study Light](https://www.studylight.org/lexicons/hebrew/143.html)

[Bible Hub](https://biblehub.com/str/hebrew/143.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0143)

[Bible Bento](https://biblebento.com/dictionary/H143.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/143/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/143.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h143)
