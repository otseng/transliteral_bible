# עַצְלָה

[ʿaṣlâ](https://www.blueletterbible.org/lexicon/h6103)

Definition: slothfulness (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6103)

[Study Light](https://www.studylight.org/lexicons/hebrew/6103.html)

[Bible Hub](https://biblehub.com/str/hebrew/6103.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06103)

[Bible Bento](https://biblebento.com/dictionary/H6103.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6103/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6103.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6103)
