# יְהוֹנָתָן

[Yᵊhônāṯān](https://www.blueletterbible.org/lexicon/h3083)

Definition: Jonathan (76x), Jehonathan (6x).

Part of speech: proper masculine noun

Occurs 80 times in 72 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3083)

[Study Light](https://www.studylight.org/lexicons/hebrew/3083.html)

[Bible Hub](https://biblehub.com/str/hebrew/3083.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03083)

[Bible Bento](https://biblebento.com/dictionary/H3083.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3083/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3083.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3083)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jonathan.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jonathan)