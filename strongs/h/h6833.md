# צִפּוֹר

[tsippowr](https://www.blueletterbible.org/lexicon/h6833)

Definition: bird (32x), fowl (6x), sparrow (2x).

Part of speech: feminine noun

Occurs 40 times in 36 verses

Hebrew: [ṣāp̄ar](../h/h6852.md)

Greek: [orneon](../g/g3732.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6833)

[Study Light](https://www.studylight.org/lexicons/hebrew/6833.html)

[Bible Hub](https://biblehub.com/str/hebrew/6833.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A6%D6%B4%D7%A4%D6%BC%D7%95%D6%B9%D7%A8)

[NET Bible](http://classic.net.bible.org/strong.php?id=06833)

[Bible Bento](https://biblebento.com/dictionary/H6833.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6833/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6833.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6833)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A6%D7%99%D7%A4%D7%95%D7%A8)
