# מַטְמוֹן

[maṭmôn](https://www.blueletterbible.org/lexicon/h4301)

Definition: treasure (4x), riches (1x).

Part of speech: masculine noun

Occurs 5 times in 5 verses

Hebrew: [taman](../h/h2934.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4301)

[Study Light](https://www.studylight.org/lexicons/hebrew/4301.html)

[Bible Hub](https://biblehub.com/str/hebrew/4301.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04301)

[Bible Bento](https://biblebento.com/dictionary/H4301.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4301/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4301.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4301)
