# נָזַר

[nāzar](https://www.blueletterbible.org/lexicon/h5144)

Definition: separate... (9x), consecrate (1x).

Part of speech: verb

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5144)

[Study Light](https://www.studylight.org/lexicons/hebrew/5144.html)

[Bible Hub](https://biblehub.com/str/hebrew/5144.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05144)

[Bible Bento](https://biblebento.com/dictionary/H5144.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5144/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5144.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5144)
