# חֹם

[ḥōm](https://www.blueletterbible.org/lexicon/h2527)

Definition: heat (9x), hot (4x), warm (1x).

Part of speech: masculine noun

Occurs 15 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2527)

[Study Light](https://www.studylight.org/lexicons/hebrew/2527.html)

[Bible Hub](https://biblehub.com/str/hebrew/2527.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02527)

[Bible Bento](https://biblebento.com/dictionary/H2527.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2527/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2527.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2527)
