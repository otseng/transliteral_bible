# סַנְסַנָּה

[sansannâ](https://www.blueletterbible.org/lexicon/h5578)

Definition: Sansannah (1x).

Part of speech: proper feminine locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5578)

[Study Light](https://www.studylight.org/lexicons/hebrew/5578.html)

[Bible Hub](https://biblehub.com/str/hebrew/5578.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05578)

[Bible Bento](https://biblebento.com/dictionary/H5578.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5578/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5578.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5578)
