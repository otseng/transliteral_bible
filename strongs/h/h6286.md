# פָּאַר

[pā'ar](https://www.blueletterbible.org/lexicon/h6286)

Definition: glorify (7x), beautify (3x), boast (1x), go over the boughs (1x), Glory (1x), vaunt (1x).

Part of speech: verb

Occurs 14 times in 14 verses

Hebrew: [tip̄'ārâ](../h/h8597.md)

Places: [Pā'rān](../h/h6290.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6286)

[Study Light](https://www.studylight.org/lexicons/hebrew/6286.html)

[Bible Hub](https://biblehub.com/str/hebrew/6286.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06286)

[Bible Bento](https://biblebento.com/dictionary/H6286.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6286/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6286.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6286)
