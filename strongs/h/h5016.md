# נְבוּאָה

[nᵊḇû'â](https://www.blueletterbible.org/lexicon/h5016)

Definition: prophecy (3x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

Greek: [prophēteia](../g/g4394.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5016)

[Study Light](https://www.studylight.org/lexicons/hebrew/5016.html)

[Bible Hub](https://biblehub.com/str/hebrew/5016.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05016)

[Bible Bento](https://biblebento.com/dictionary/H5016.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5016/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5016.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5016)
