# בֶּן־אוֹנִי

[ben-'ônî](https://www.blueletterbible.org/lexicon/h1126)

Definition: Benoni (1x).

Part of speech: proper masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1126)

[Study Light](https://www.studylight.org/lexicons/hebrew/1126.html)

[Bible Hub](https://biblehub.com/str/hebrew/1126.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01126)

[Bible Bento](https://biblebento.com/dictionary/H1126.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1126/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1126.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1126)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/benoni.html)