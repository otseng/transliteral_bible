# מִשְׂרָה

[miśrâ](https://www.blueletterbible.org/lexicon/h4951)

Definition: government (2x), rule, dominion, empire

Part of speech: feminine noun

Occurs 2 times in 2 verses

Hebrew: [seren](../h/h5633.md)

Pictoral: turn head

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4951)

[Study Light](https://www.studylight.org/lexicons/hebrew/4951.html)

[Bible Hub](https://biblehub.com/str/hebrew/4951.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04951)

[Bible Bento](https://biblebento.com/dictionary/H4951.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4951/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4951.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4951)
