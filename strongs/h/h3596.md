# כִּילַי

[kîlay](https://www.blueletterbible.org/lexicon/h3596)

Definition: churl (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3596)

[Study Light](https://www.studylight.org/lexicons/hebrew/3596.html)

[Bible Hub](https://biblehub.com/str/hebrew/3596.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03596)

[Bible Bento](https://biblebento.com/dictionary/H3596.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3596/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3596.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3596)
