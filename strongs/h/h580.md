# אֲנוּ

['ănû](https://www.blueletterbible.org/lexicon/h580)

Definition: we (1x).

Part of speech: personal pronoun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h580)

[Study Light](https://www.studylight.org/lexicons/hebrew/580.html)

[Bible Hub](https://biblehub.com/str/hebrew/580.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0580)

[Bible Bento](https://biblebento.com/dictionary/H580.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/580/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/580.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h580)
