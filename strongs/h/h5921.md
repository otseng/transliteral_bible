# עַל

[ʿal](https://www.blueletterbible.org/lexicon/h5921)

Definition: upon, in, on, over, by, for, both, beyond, through, throughout, against, beside, forth, off, from off.

Part of speech: conjunction, preposition

Occurs 5,781 times in 4,491 verses

Hebrew: [ʿal](../h/h5920.md), [ʿal](../h/h5922.md), [maʿal](../h/h4605.md), [maʿălâ](../h/h4609.md)

Pictoral: knowledge staff

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5921)

[Study Light](https://www.studylight.org/lexicons/hebrew/5921.html)

[Bible Hub](https://biblehub.com/str/hebrew/5921.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05921)

[Bible Bento](https://biblebento.com/dictionary/H5921.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5921/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5921.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5921)
