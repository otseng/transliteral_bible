# עֱלִי

[ʿĕlî](https://www.blueletterbible.org/lexicon/h5940)

Definition: pestle (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5940)

[Study Light](https://www.studylight.org/lexicons/hebrew/5940.html)

[Bible Hub](https://biblehub.com/str/hebrew/5940.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05940)

[Bible Bento](https://biblebento.com/dictionary/H5940.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5940/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5940.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5940)
