# מָאַס

[mā'as](https://www.blueletterbible.org/lexicon/h3988)

Definition: despise (25x), refuse (9x), reject (19x), abhor (4x), become loathsome (1x), melt away (1x), miscellaneous (17x).

Part of speech: verb

Occurs 76 times in 69 verses

Hebrew: [mā'ôs](../h/h3973.md), [mā'as](../h/h3988.md), [māsas](../h/h4549.md)

Greek: [atheteō](../g/g114.md), [exoutheneō](../g/g1848.md), [miseō](../g/g3404.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3988)

[Study Light](https://www.studylight.org/lexicons/hebrew/3988.html)

[Bible Hub](https://biblehub.com/str/hebrew/3988.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03988)

[Bible Bento](https://biblebento.com/dictionary/H3988.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3988/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3988.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3988)
