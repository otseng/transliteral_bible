# אַרְגְוָן

['arḡᵊvān](https://www.blueletterbible.org/lexicon/h711)

Definition: scarlet (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h711)

[Study Light](https://www.studylight.org/lexicons/hebrew/711.html)

[Bible Hub](https://biblehub.com/str/hebrew/711.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0711)

[Bible Bento](https://biblebento.com/dictionary/H711.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/711/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/711.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h711)
