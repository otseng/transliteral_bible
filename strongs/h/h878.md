# בְּאֵרָא

[Bᵊ'Ērā'](https://www.blueletterbible.org/lexicon/h878)

Definition: Beera (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h878)

[Study Light](https://www.studylight.org/lexicons/hebrew/878.html)

[Bible Hub](https://biblehub.com/str/hebrew/878.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0878)

[Bible Bento](https://biblebento.com/dictionary/H878.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/878/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/878.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h878)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/beera.html)

[Video Bible](https://www.videobible.com/bible-dictionary/beera)