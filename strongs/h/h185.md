# אַוָּה

['aûâ](https://www.blueletterbible.org/lexicon/h185)

Definition: desire (3x), lust after (3x), pleasure (1x).

Part of speech: feminine noun

Occurs 7 times in 7 verses

Greek: [epithymia](../g/g1939.md)

Synonyms: [lust](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Lust)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h185)

[Study Light](https://www.studylight.org/lexicons/hebrew/185.html)

[Bible Hub](https://biblehub.com/str/hebrew/185.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0185)

[Bible Bento](https://biblebento.com/dictionary/H185.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/185/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/185.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h185)
