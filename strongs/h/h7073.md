# קְנַז

[qᵊnaz](https://www.blueletterbible.org/lexicon/h7073)

Definition: Kenaz (11x).

Part of speech: proper masculine noun

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7073)

[Study Light](https://www.studylight.org/lexicons/hebrew/7073.html)

[Bible Hub](https://biblehub.com/str/hebrew/7073.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07073)

[Bible Bento](https://biblebento.com/dictionary/H7073.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7073/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7073.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7073)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/K/kenaz.html)

[Video Bible](https://www.videobible.com/bible-dictionary/kenaz)