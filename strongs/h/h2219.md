# זָרָה

[zārâ](https://www.blueletterbible.org/lexicon/h2219)

Definition: scatter (19x), disperse (8x), fan (4x), spread (2x), winnowed (2x), cast away (1x), scatter away (1x), compass (1x), strawed (1x).

Part of speech: verb

Occurs 39 times in 38 verses

Hebrew: [zûr](../h/h2114.md), [zārar](../h/h2237.md)

Greek: [speirō](../g/g4687.md)

Pictoral: harvest heads

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2219)

[Study Light](https://www.studylight.org/lexicons/hebrew/2219.html)

[Bible Hub](https://biblehub.com/str/hebrew/2219.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02219)

[Bible Bento](https://biblebento.com/dictionary/H2219.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2219/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2219.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2219)
