# רָמָתִי

[rāmāṯî](https://www.blueletterbible.org/lexicon/h7435)

Definition: Ramathite (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7435)

[Study Light](https://www.studylight.org/lexicons/hebrew/7435.html)

[Bible Hub](https://biblehub.com/str/hebrew/7435.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07435)

[Bible Bento](https://biblebento.com/dictionary/H7435.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7435/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7435.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7435)
