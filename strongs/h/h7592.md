# שָׁאַל

[sha'al](https://www.blueletterbible.org/lexicon/h7592)

Definition: ask (94x), enquire (22x), desire (9x), require (7x), borrow (6x), salute (4x), demand (4x), lent (4x), request (3x), earnestly (2x), beg (2x)

Part of speech: verb

Occurs 173 times in 157 verses

Hebrew: [miš'ālâ](../h/h4862.md), [šᵊ'ēl](../h/h7593.md), [šᵊ'ēlâ](../h/h7595.md), [šᵊ'ēlâ](../h/h7596.md)

Names: [Šā'ûl](../h/h7586.md)

Greek: [aiteō](../g/g154.md), [daneizō](../g/g1155.md), [erōtaō](../g/g2065.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7592)

[Study Light](https://www.studylight.org/lexicons/hebrew/7592.html)

[Bible Hub](https://biblehub.com/str/hebrew/7592.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A9%D6%B8%D7%81%D7%90%D6%B7%D7%9C)

[NET Bible](http://classic.net.bible.org/strong.php?id=07592)

[Bible Bento](https://biblebento.com/dictionary/H7592.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7592/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7592.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7592)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A9%D7%90%D7%9C)
