# נֶכֶד

[neḵeḏ](https://www.blueletterbible.org/lexicon/h5220)

Definition: nephew (2x), son's son (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5220)

[Study Light](https://www.studylight.org/lexicons/hebrew/5220.html)

[Bible Hub](https://biblehub.com/str/hebrew/5220.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05220)

[Bible Bento](https://biblebento.com/dictionary/H5220.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5220/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5220.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5220)
