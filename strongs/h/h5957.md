# עָלַם

[ʿālam](https://www.blueletterbible.org/lexicon/h5957)

Definition: ever (12x), everlasting (4x), old (2x), ever (with H5705) (1x), never (1x).

Part of speech: masculine noun

Occurs 20 times in 15 verses

Hebrew: [`owlam](../h/h5769.md), [`alam](../h/h5956.md), [ʿêlôm](../h/h5865.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5957)

[Study Light](https://www.studylight.org/lexicons/hebrew/5957.html)

[Bible Hub](https://biblehub.com/str/hebrew/5957.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05957)

[Bible Bento](https://biblebento.com/dictionary/H5957.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5957/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5957.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5957)
