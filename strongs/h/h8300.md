# שָׂרִיד

[śārîḏ](https://www.blueletterbible.org/lexicon/h8300)

Definition: remain (12x), remaining (9x), left (3x), remnant (2x), alive (1x), rest (1x).

Part of speech: masculine noun

Occurs 29 times in 28 verses

Synonyms: [rest](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Rest)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8300)

[Study Light](https://www.studylight.org/lexicons/hebrew/8300.html)

[Bible Hub](https://biblehub.com/str/hebrew/8300.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08300)

[Bible Bento](https://biblebento.com/dictionary/H8300.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8300/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8300.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8300)
