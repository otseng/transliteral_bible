# צַיִד

[ṣayiḏ](https://www.blueletterbible.org/lexicon/h6718)

Definition: venison (8x), hunter (3x), victuals (2x), provision (2x), hunting (1x), catch (1x), food (1x), hunting (1x).

Part of speech: masculine noun

Occurs 17 times in 16 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6718)

[Study Light](https://www.studylight.org/lexicons/hebrew/6718.html)

[Bible Hub](https://biblehub.com/str/hebrew/6718.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06718)

[Bible Bento](https://biblebento.com/dictionary/H6718.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6718/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6718.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6718)
