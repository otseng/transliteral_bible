# אַבְדָן

['aḇḏān](https://www.blueletterbible.org/lexicon/h12)

Definition: destruction (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h12)

[Study Light](https://www.studylight.org/lexicons/hebrew/12.html)

[Bible Hub](https://biblehub.com/str/hebrew/12.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=012)

[Bible Bento](https://biblebento.com/dictionary/H12.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/12/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/12.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h12)
