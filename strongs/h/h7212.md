# רְאִית

[rᵊ'îṯ](https://www.blueletterbible.org/lexicon/h7212)

Definition: beholding (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7212)

[Study Light](https://www.studylight.org/lexicons/hebrew/7212.html)

[Bible Hub](https://biblehub.com/str/hebrew/7212.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07212)

[Bible Bento](https://biblebento.com/dictionary/H7212.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7212/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7212.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7212)
