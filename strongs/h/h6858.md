# צֶפֶת

[ṣep̄eṯ](https://www.blueletterbible.org/lexicon/h6858)

Definition: chapiter (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6858)

[Study Light](https://www.studylight.org/lexicons/hebrew/6858.html)

[Bible Hub](https://biblehub.com/str/hebrew/6858.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06858)

[Bible Bento](https://biblebento.com/dictionary/H6858.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6858/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6858.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6858)
