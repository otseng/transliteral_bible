# בְּדַר

[bᵊḏar](https://www.blueletterbible.org/lexicon/h921)

Definition: scatter (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h921)

[Study Light](https://www.studylight.org/lexicons/hebrew/921.html)

[Bible Hub](https://biblehub.com/str/hebrew/921.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0921)

[Bible Bento](https://biblebento.com/dictionary/H921.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/921/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/921.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h921)
