# בֶּלַע

[Belaʿ](https://www.blueletterbible.org/lexicon/h1106)

Definition: Bela (13x), Belah (1x).

Part of speech: proper locative noun, proper masculine noun

Occurs 14 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1106)

[Study Light](https://www.studylight.org/lexicons/hebrew/1106.html)

[Bible Hub](https://biblehub.com/str/hebrew/1106.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01106)

[Bible Bento](https://biblebento.com/dictionary/H1106.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1106/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1106.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1106)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bela.html)

[Video Bible](https://www.videobible.com/bible-dictionary/bela)