# רָפוּא

[rāp̄û'](https://www.blueletterbible.org/lexicon/h7505)

Definition: Raphu (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7505)

[Study Light](https://www.studylight.org/lexicons/hebrew/7505.html)

[Bible Hub](https://biblehub.com/str/hebrew/7505.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07505)

[Bible Bento](https://biblebento.com/dictionary/H7505.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7505/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7505.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7505)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/R/raphu.html)

[Video Bible](https://www.videobible.com/bible-dictionary/raphu)