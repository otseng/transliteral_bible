# הֵידָד

[hêḏāḏ](https://www.blueletterbible.org/lexicon/h1959)

Definition: shout (7x), cheer

Part of speech: masculine noun

Occurs 7 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1959)

[Study Light](https://www.studylight.org/lexicons/hebrew/1959.html)

[Bible Hub](https://biblehub.com/str/hebrew/1959.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01959)

[Bible Bento](https://biblebento.com/dictionary/H1959.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1959/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1959.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1959)
