# קִישׁ

[Qîš](https://www.blueletterbible.org/lexicon/h7027)

Definition: Kish (21x).

Part of speech: proper masculine noun

Occurs 21 times in 17 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7027)

[Study Light](https://www.studylight.org/lexicons/hebrew/7027.html)

[Bible Hub](https://biblehub.com/str/hebrew/7027.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07027)

[Bible Bento](https://biblebento.com/dictionary/H7027.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7027/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7027.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7027)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/K/kish.html)

[Video Bible](https://www.videobible.com/bible-dictionary/kish)