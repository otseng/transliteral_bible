# מָרָד

[mārāḏ](https://www.blueletterbible.org/lexicon/h4779)

Definition: rebellious (2x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4779)

[Study Light](https://www.studylight.org/lexicons/hebrew/4779.html)

[Bible Hub](https://biblehub.com/str/hebrew/4779.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04779)

[Bible Bento](https://biblebento.com/dictionary/H4779.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4779/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4779.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4779)
