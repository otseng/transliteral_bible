# צָלָה

[ṣālâ](https://www.blueletterbible.org/lexicon/h6740)

Definition: roast (3x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6740)

[Study Light](https://www.studylight.org/lexicons/hebrew/6740.html)

[Bible Hub](https://biblehub.com/str/hebrew/6740.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06740)

[Bible Bento](https://biblebento.com/dictionary/H6740.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6740/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6740.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6740)
