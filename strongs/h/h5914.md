# עֶכֶס

[ʿeḵes](https://www.blueletterbible.org/lexicon/h5914)

Definition: stocks (1x), tinkling ornaments (1x), anklet

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5914)

[Study Light](https://www.studylight.org/lexicons/hebrew/5914.html)

[Bible Hub](https://biblehub.com/str/hebrew/5914.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05914)

[Bible Bento](https://biblebento.com/dictionary/H5914.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5914/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5914.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5914)
