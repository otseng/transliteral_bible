# פּוֹרָתָא

[Pôrāṯā'](https://www.blueletterbible.org/lexicon/h6334)

Definition: Poratha (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6334)

[Study Light](https://www.studylight.org/lexicons/hebrew/6334.html)

[Bible Hub](https://biblehub.com/str/hebrew/6334.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06334)

[Bible Bento](https://biblebento.com/dictionary/H6334.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6334/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6334.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6334)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/poratha.html)

[Video Bible](https://www.videobible.com/bible-dictionary/poratha)