# חוּר

[Ḥûr](https://www.blueletterbible.org/lexicon/h2352)

Definition: hole (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2352)

[Study Light](https://www.studylight.org/lexicons/hebrew/2352.html)

[Bible Hub](https://biblehub.com/str/hebrew/2352.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02352)

[Bible Bento](https://biblebento.com/dictionary/H2352.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2352/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2352.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2352)
