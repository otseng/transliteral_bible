# זָהַם

[zāham](https://www.blueletterbible.org/lexicon/h2092)

Definition: abhorreth (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2092)

[Study Light](https://www.studylight.org/lexicons/hebrew/2092.html)

[Bible Hub](https://biblehub.com/str/hebrew/2092.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02092)

[Bible Bento](https://biblebento.com/dictionary/H2092.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2092/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2092.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2092)
