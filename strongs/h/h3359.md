# יְקַמְיָה

[Yᵊqamyâ](https://www.blueletterbible.org/lexicon/h3359)

Definition: Jekamiah (2x), Jecamiah (1x).

Part of speech: proper masculine noun

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3359)

[Study Light](https://www.studylight.org/lexicons/hebrew/3359.html)

[Bible Hub](https://biblehub.com/str/hebrew/3359.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03359)

[Bible Bento](https://biblebento.com/dictionary/H3359.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3359/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3359.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3359)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jekamiah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jekamiah)