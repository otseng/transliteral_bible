# כַּר

[kar](https://www.blueletterbible.org/lexicon/h3733)

Definition: lamb (10x), pasture (2x), ram (2x), furniture (1x), captains (1x), basket saddle

Part of speech: masculine noun

Occurs 16 times in 15 verses

Synonyms: [lamb](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Lamb)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3733)

[Study Light](https://www.studylight.org/lexicons/hebrew/3733.html)

[Bible Hub](https://biblehub.com/str/hebrew/3733.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03733)

[Bible Bento](https://biblebento.com/dictionary/H3733.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3733/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3733.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3733)
