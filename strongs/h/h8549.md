# תָּמִים

[tamiym](https://www.blueletterbible.org/lexicon/h8549)

Definition: without blemish (44x), perfect (18x), upright (8x), without spot (6x), uprightly (4x), whole (4x), sincerely (2x), complete (1x), full (1x), miscellaneous (3x).

Part of speech: adjective

Occurs 91 times in 85 verses

Hebrew: [tamam](../h/h8552.md)

Greek: [teleios](../g/g5046.md), [holos](../g/g3650.md)

Synonyms: [perfect](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Perfect)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8549)

[Study Light](https://www.studylight.org/lexicons/hebrew/8549.html)

[Bible Hub](https://biblehub.com/str/hebrew/8549.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08549)

[Bible Bento](https://biblebento.com/dictionary/H8549.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8549/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8549.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8549)
