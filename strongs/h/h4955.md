# מִשְׂרָפוֹת

[miśrāp̄ôṯ](https://www.blueletterbible.org/lexicon/h4955)

Definition: burnings (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4955)

[Study Light](https://www.studylight.org/lexicons/hebrew/4955.html)

[Bible Hub](https://biblehub.com/str/hebrew/4955.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04955)

[Bible Bento](https://biblebento.com/dictionary/H4955.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4955/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4955.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4955)
