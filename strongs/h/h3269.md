# יַעֲזִיָּהוּ

[YaʿĂzîyâû](https://www.blueletterbible.org/lexicon/h3269)

Definition: Jaaziah (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3269)

[Study Light](https://www.studylight.org/lexicons/hebrew/3269.html)

[Bible Hub](https://biblehub.com/str/hebrew/3269.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03269)

[Bible Bento](https://biblebento.com/dictionary/H3269.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3269/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3269.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3269)

[Video Bible](https://www.videobible.com/bible-dictionary/jaaziah)