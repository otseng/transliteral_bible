# אַחֲלַי

['aḥălay](https://www.blueletterbible.org/lexicon/h305)

Definition: O that (1x), would God (1x).

Part of speech: interjection

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h305)

[Study Light](https://www.studylight.org/lexicons/hebrew/305.html)

[Bible Hub](https://biblehub.com/str/hebrew/305.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0305)

[Bible Bento](https://biblebento.com/dictionary/H305.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/305/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/305.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h305)
