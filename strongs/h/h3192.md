# יָטְבָה

[Yāṭḇâ](https://www.blueletterbible.org/lexicon/h3192)

Definition: Jotbah (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3192)

[Study Light](https://www.studylight.org/lexicons/hebrew/3192.html)

[Bible Hub](https://biblehub.com/str/hebrew/3192.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03192)

[Bible Bento](https://biblebento.com/dictionary/H3192.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3192/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3192.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3192)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jotbah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jotbah)