# קוּשָׁיָהוּ

[Qûšāyâû](https://www.blueletterbible.org/lexicon/h6984)

Definition: Kushaiah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6984)

[Study Light](https://www.studylight.org/lexicons/hebrew/6984.html)

[Bible Hub](https://biblehub.com/str/hebrew/6984.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06984)

[Bible Bento](https://biblebento.com/dictionary/H6984.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6984/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6984.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6984)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/K/kushaiah.html)