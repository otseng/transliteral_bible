# רוּם

[rûm](https://www.blueletterbible.org/lexicon/h7312)

Definition: haughtiness (3x), high (2x), height (1x).

Part of speech: masculine noun

Occurs 6 times in 6 verses

Hebrew: [ruwm](../h/h7311.md), [rûm](../h/h7313.md), [rûm](../h/h7314.md), [rôm](../h/h7315.md)

Greek: [hyperēphanos](../g/g5244.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7312)

[Study Light](https://www.studylight.org/lexicons/hebrew/7312.html)

[Bible Hub](https://biblehub.com/str/hebrew/7312.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07312)

[Bible Bento](https://biblebento.com/dictionary/H7312.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7312/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7312.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7312)
