# יַבֶּשֶׁת

[yabešeṯ](https://www.blueletterbible.org/lexicon/h3006)

Definition: dry (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3006)

[Study Light](https://www.studylight.org/lexicons/hebrew/3006.html)

[Bible Hub](https://biblehub.com/str/hebrew/3006.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03006)

[Bible Bento](https://biblebento.com/dictionary/H3006.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3006/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3006.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3006)
