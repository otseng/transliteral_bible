# אָגֵם

['āḡēm](https://www.blueletterbible.org/lexicon/h99)

Definition: pools (1x), stagnant water

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0099)

[Study Light](https://www.studylight.org/lexicons/hebrew/99.html)

[Bible Hub](https://biblehub.com/str/hebrew/99.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=099)

[Bible Bento](https://biblebento.com/dictionary/H99.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/99/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/99.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h99)
