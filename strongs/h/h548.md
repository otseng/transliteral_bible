# אֲמָנָה

['ămānâ](https://www.blueletterbible.org/lexicon/h548)

Definition: sure (1x), portion (1x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h548)

[Study Light](https://www.studylight.org/lexicons/hebrew/548.html)

[Bible Hub](https://biblehub.com/str/hebrew/548.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0548)

[Bible Bento](https://biblebento.com/dictionary/H548.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/548/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/548.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h548)
