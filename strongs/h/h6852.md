# צָפַר

[ṣāp̄ar](https://www.blueletterbible.org/lexicon/h6852)

Definition: depart early (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6852)

[Study Light](https://www.studylight.org/lexicons/hebrew/6852.html)

[Bible Hub](https://biblehub.com/str/hebrew/6852.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06852)

[Bible Bento](https://biblebento.com/dictionary/H6852.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6852/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6852.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6852)
