# סָלְעָם

[sālʿām](https://www.blueletterbible.org/lexicon/h5556)

Definition: locust (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5556)

[Study Light](https://www.studylight.org/lexicons/hebrew/5556.html)

[Bible Hub](https://biblehub.com/str/hebrew/5556.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05556)

[Bible Bento](https://biblebento.com/dictionary/H5556.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5556/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5556.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5556)
