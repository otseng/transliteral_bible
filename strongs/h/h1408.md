# גַּד

[gaḏ](https://www.blueletterbible.org/lexicon/h1408)

Definition: non translated variant (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1408)

[Study Light](https://www.studylight.org/lexicons/hebrew/1408.html)

[Bible Hub](https://biblehub.com/str/hebrew/1408.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01408)

[Bible Bento](https://biblebento.com/dictionary/H1408.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1408/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1408.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1408)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/non.html)