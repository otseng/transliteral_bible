# חֶדֶל

[ḥeḏel](https://www.blueletterbible.org/lexicon/h2309)

Definition: world (1x), rest, cessation, the state of the dead

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2309)

[Study Light](https://www.studylight.org/lexicons/hebrew/2309.html)

[Bible Hub](https://biblehub.com/str/hebrew/2309.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02309)

[Bible Bento](https://biblebento.com/dictionary/H2309.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2309/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2309.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2309)
