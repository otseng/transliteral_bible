# צוּף

[ṣûp̄](https://www.blueletterbible.org/lexicon/h6688)

Definition: honeycomb (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6688)

[Study Light](https://www.studylight.org/lexicons/hebrew/6688.html)

[Bible Hub](https://biblehub.com/str/hebrew/6688.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06688)

[Bible Bento](https://biblebento.com/dictionary/H6688.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6688/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6688.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6688)
