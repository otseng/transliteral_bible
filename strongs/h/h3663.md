# כְּנַנְיָה

[Kᵊnanyâ](https://www.blueletterbible.org/lexicon/h3663)

Definition: Chenaniah (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3663)

[Study Light](https://www.studylight.org/lexicons/hebrew/3663.html)

[Bible Hub](https://biblehub.com/str/hebrew/3663.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03663)

[Bible Bento](https://biblebento.com/dictionary/H3663.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3663/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3663.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3663)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/chenaniah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/chenaniah)