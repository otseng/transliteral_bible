# עָפַל

[ʿāp̄al](https://www.blueletterbible.org/lexicon/h6075)

Definition: lifted up (1x), presumed (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6075)

[Study Light](https://www.studylight.org/lexicons/hebrew/6075.html)

[Bible Hub](https://biblehub.com/str/hebrew/6075.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06075)

[Bible Bento](https://biblebento.com/dictionary/H6075.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6075/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6075.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6075)
