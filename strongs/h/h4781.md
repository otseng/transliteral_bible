# מְרֹדַךְ

[Mᵊrōḏaḵ](https://www.blueletterbible.org/lexicon/h4781)

Definition: Merodach (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4781)

[Study Light](https://www.studylight.org/lexicons/hebrew/4781.html)

[Bible Hub](https://biblehub.com/str/hebrew/4781.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04781)

[Bible Bento](https://biblebento.com/dictionary/H4781.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4781/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4781.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4781)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/merodach.html)

[Video Bible](https://www.videobible.com/bible-dictionary/merodach)