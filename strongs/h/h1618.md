# גָּרָב

[gārāḇ](https://www.blueletterbible.org/lexicon/h1618)

Definition: scurvy (2x), scab (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1618)

[Study Light](https://www.studylight.org/lexicons/hebrew/1618.html)

[Bible Hub](https://biblehub.com/str/hebrew/1618.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01618)

[Bible Bento](https://biblebento.com/dictionary/H1618.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1618/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1618.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1618)
