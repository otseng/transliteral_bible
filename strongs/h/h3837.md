# לָבָן

[Lāḇān](https://www.blueletterbible.org/lexicon/h3837)

Definition: Laban (55x), "white", son of Bethuel, brother of Rebekah, and father of Leah and Rachel 

Part of speech: proper locative noun, proper masculine noun

Occurs 55 times in 47 verses

Hebrew: [lāḇān](../h/h3836.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3837)

[Study Light](https://www.studylight.org/lexicons/hebrew/3837.html)

[Bible Hub](https://biblehub.com/str/hebrew/3837.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03837)

[Bible Bento](https://biblebento.com/dictionary/H3837.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3837/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3837.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3837)

[Wikipedia](https://en.wikipedia.org/wiki/Laban_%28Bible%29)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/L/laban.html)

[Video Bible](https://www.videobible.com/bible-dictionary/laban)