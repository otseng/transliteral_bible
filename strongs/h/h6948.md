# קְדֵשָׁה

[qᵊḏēšâ](https://www.blueletterbible.org/lexicon/h6948)

Definition: harlot (4x), whore (1x), cult prostitute

Part of speech: feminine noun

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6948)

[Study Light](https://www.studylight.org/lexicons/hebrew/6948.html)

[Bible Hub](https://biblehub.com/str/hebrew/6948.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06948)

[Bible Bento](https://biblebento.com/dictionary/H6948.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6948/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6948.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6948)
