# חוּפָמִי

[ḥûp̄āmî](https://www.blueletterbible.org/lexicon/h2350)

Definition: Huphamites (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2350)

[Study Light](https://www.studylight.org/lexicons/hebrew/2350.html)

[Bible Hub](https://biblehub.com/str/hebrew/2350.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02350)

[Bible Bento](https://biblebento.com/dictionary/H2350.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2350/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2350.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2350)
