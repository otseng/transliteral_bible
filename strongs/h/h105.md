# אֲגַרְטָל

['ăḡarṭāl](https://www.blueletterbible.org/lexicon/h105)

Definition: chargers (2x).

Part of speech: masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h105)

[Study Light](https://www.studylight.org/lexicons/hebrew/105.html)

[Bible Hub](https://biblehub.com/str/hebrew/105.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0105)

[Bible Bento](https://biblebento.com/dictionary/H105.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/105/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/105.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h105)
