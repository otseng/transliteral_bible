# עָגַב

[ʿāḡaḇ](https://www.blueletterbible.org/lexicon/h5689)

Definition: doted (6x), lovers (1x).

Part of speech: verb

Occurs 8 times in 7 verses

Hebrew: [ʿăḡāḇîm](../h/h5690.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5689)

[Study Light](https://www.studylight.org/lexicons/hebrew/5689.html)

[Bible Hub](https://biblehub.com/str/hebrew/5689.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05689)

[Bible Bento](https://biblebento.com/dictionary/H5689.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5689/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5689.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5689)
