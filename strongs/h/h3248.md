# יְסוּדָה

[yᵊsûḏâ](https://www.blueletterbible.org/lexicon/h3248)

Definition: foundation (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3248)

[Study Light](https://www.studylight.org/lexicons/hebrew/3248.html)

[Bible Hub](https://biblehub.com/str/hebrew/3248.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03248)

[Bible Bento](https://biblebento.com/dictionary/H3248.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3248/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3248.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3248)
