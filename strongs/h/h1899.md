# הֶגֶה

[heḡê](https://www.blueletterbible.org/lexicon/h1899)

Definition: sound (1x), tale (1x), mourning (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1899)

[Study Light](https://www.studylight.org/lexicons/hebrew/1899.html)

[Bible Hub](https://biblehub.com/str/hebrew/1899.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01899)

[Bible Bento](https://biblebento.com/dictionary/H1899.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1899/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1899.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1899)
