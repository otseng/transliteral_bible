# מִכְרֶה

[miḵrê](https://www.blueletterbible.org/lexicon/h4379)

Definition: saltpit (with H4417) (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4379)

[Study Light](https://www.studylight.org/lexicons/hebrew/4379.html)

[Bible Hub](https://biblehub.com/str/hebrew/4379.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04379)

[Bible Bento](https://biblebento.com/dictionary/H4379.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4379/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4379.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4379)
