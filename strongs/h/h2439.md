# חִישׁ

[ḥîš](https://www.blueletterbible.org/lexicon/h2439)

Definition: haste (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2439)

[Study Light](https://www.studylight.org/lexicons/hebrew/2439.html)

[Bible Hub](https://biblehub.com/str/hebrew/2439.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02439)

[Bible Bento](https://biblebento.com/dictionary/H2439.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2439/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2439.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2439)
