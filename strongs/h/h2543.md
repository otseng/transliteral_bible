# חֲמוֹר

[chamowr](https://www.blueletterbible.org/lexicon/h2543)

Definition: ass (96x), donkey

Part of speech: masculine noun

Occurs 96 times in 93 verses

Hebrew: [ḥāmar](../h/h2560.md)

Greek: [onos](../g/g3688.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2543)

[Study Light](https://www.studylight.org/lexicons/hebrew/2543.html)

[Bible Hub](https://biblehub.com/str/hebrew/2543.htm)

[Morfix](https://www.morfix.co.il/en/%D7%97%D6%B2%D7%9E%D7%95%D6%B9%D7%A8)

[NET Bible](http://classic.net.bible.org/strong.php?id=02543)

[Bible Bento](https://biblebento.com/dictionary/H2543.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2543/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2543.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2543)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%97%D7%9E%D7%95%D7%A8)
