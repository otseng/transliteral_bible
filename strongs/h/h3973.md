# מָאוֹס

[mā'ôs](https://www.blueletterbible.org/lexicon/h3973)

Definition: refuse (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3973)

[Study Light](https://www.studylight.org/lexicons/hebrew/3973.html)

[Bible Hub](https://biblehub.com/str/hebrew/3973.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03973)

[Bible Bento](https://biblebento.com/dictionary/H3973.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3973/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3973.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3973)
