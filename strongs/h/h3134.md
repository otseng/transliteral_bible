# יוֹעֶזֶר

[YôʿEzer](https://www.blueletterbible.org/lexicon/h3134)

Definition: Joezer (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3134)

[Study Light](https://www.studylight.org/lexicons/hebrew/3134.html)

[Bible Hub](https://biblehub.com/str/hebrew/3134.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03134)

[Bible Bento](https://biblebento.com/dictionary/H3134.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3134/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3134.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3134)

[Video Bible](https://www.videobible.com/bible-dictionary/joezer)