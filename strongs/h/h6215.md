# עֵשָׂו

[ʿĒśāv](https://www.blueletterbible.org/lexicon/h6215)

Definition: Esau (97x), "hairy", eldest son of Isaac and Rebecca and twin brother of Jacob, progenitor of the Edomites

Part of speech: proper masculine noun

Occurs 97 times in 82 verses

Greek: [Ēsau](../g/g2269.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6215)

[Study Light](https://www.studylight.org/lexicons/hebrew/6215.html)

[Bible Hub](https://biblehub.com/str/hebrew/6215.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06215)

[Bible Bento](https://biblebento.com/dictionary/H6215.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6215/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6215.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6215)

[Wikipedia](https://en.wikipedia.org/wiki/Esau)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/esau.html)

[Video Bible](https://www.videobible.com/bible-dictionary/esau)