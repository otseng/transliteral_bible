# חֲצַר עֵינוֹן

[Ḥăṣar ʿÊnôn](https://www.blueletterbible.org/lexicon/h2703)

Definition: Hazarenan (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2703)

[Study Light](https://www.studylight.org/lexicons/hebrew/2703.html)

[Bible Hub](https://biblehub.com/str/hebrew/2703.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02703)

[Bible Bento](https://biblebento.com/dictionary/H2703.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2703/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2703.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2703)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hazarenan.html)