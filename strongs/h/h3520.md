# כְּבוּדָּה

[kᵊḇûdâ](https://www.blueletterbible.org/lexicon/h3520)

Definition: carriage (1x), glorious (1x), stately (1x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3520)

[Study Light](https://www.studylight.org/lexicons/hebrew/3520.html)

[Bible Hub](https://biblehub.com/str/hebrew/3520.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03520)

[Bible Bento](https://biblebento.com/dictionary/H3520.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3520/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3520.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3520)
