# חֻפָּה

[ḥupâ](https://www.blueletterbible.org/lexicon/h2646)

Definition: chamber (1x), closet (1x), defence (1x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2646)

[Study Light](https://www.studylight.org/lexicons/hebrew/2646.html)

[Bible Hub](https://biblehub.com/str/hebrew/2646.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02646)

[Bible Bento](https://biblebento.com/dictionary/H2646.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2646/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2646.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2646)
