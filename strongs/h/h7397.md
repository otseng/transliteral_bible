# רֵכָה

[Rēḵâ](https://www.blueletterbible.org/lexicon/h7397)

Definition: Rechabites (4x), Rechah (1x).

Part of speech: proper locative noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7397)

[Study Light](https://www.studylight.org/lexicons/hebrew/7397.html)

[Bible Hub](https://biblehub.com/str/hebrew/7397.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07397)

[Bible Bento](https://biblebento.com/dictionary/H7397.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7397/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7397.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7397)
