# גִּבְעֹנִי

[Giḇʿōnî](https://www.blueletterbible.org/lexicon/h1393)

Definition: Gibeonite (8x), "hilly"

Part of speech: adjective

Occurs 8 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1393)

[Study Light](https://www.studylight.org/lexicons/hebrew/1393.html)

[Bible Hub](https://biblehub.com/str/hebrew/1393.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01393)

[Bible Bento](https://biblebento.com/dictionary/H1393.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1393/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1393.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1393)
