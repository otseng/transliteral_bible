# סָבַל

[sāḇal](https://www.blueletterbible.org/lexicon/h5445)

Definition: carry (4x), bear (3x), labour (1x), burden (1x).

Part of speech: verb

Occurs 9 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5445)

[Study Light](https://www.studylight.org/lexicons/hebrew/5445.html)

[Bible Hub](https://biblehub.com/str/hebrew/5445.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05445)

[Bible Bento](https://biblebento.com/dictionary/H5445.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5445/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5445.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5445)
