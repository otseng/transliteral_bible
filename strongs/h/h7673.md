# שָׁבַת

[shabath](https://www.blueletterbible.org/lexicon/h7673)

Definition: cease (47x), rest (11x), away (3x), fail (2x), celebrate (1x), desist from exertion

Part of speech: verb

Occurs 73 times in 67 verses

Hebrew: [shabbath](../h/h7676.md), [mišbāṯ](../h/h4868.md)

Greek: [anapauō](../g/g373.md), [hēsychazō](../g/g2270.md)

Synonyms: [rest](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Rest)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7673)

[Study Light](https://www.studylight.org/lexicons/hebrew/7673.html)

[Bible Hub](https://biblehub.com/str/hebrew/7673.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A9%D7%91%D7%AA)

[NET Bible](http://classic.net.bible.org/strong.php?id=07673)

[Bible Bento](https://biblebento.com/dictionary/H7673.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7673/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7673.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7673)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A9%D7%91%D7%AA)

[Klein dictionary](https://www.sefaria.org/Klein_Dictionary%2C_%D7%A9%D6%B8%D7%81%D7%92%D7%95%D6%BC%D7%9D?lang=bi)

