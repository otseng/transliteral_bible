# מִשְׁטָר

[mišṭār](https://www.blueletterbible.org/lexicon/h4896)

Definition: dominion (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

Hebrew: [šāṭar](../h/h7860.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4896)

[Study Light](https://www.studylight.org/lexicons/hebrew/4896.html)

[Bible Hub](https://biblehub.com/str/hebrew/4896.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04896)

[Bible Bento](https://biblebento.com/dictionary/H4896.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4896/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4896.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4896)
