# מִקְשֶׁה

[miqšê](https://www.blueletterbible.org/lexicon/h4748)

Definition: hair (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4748)

[Study Light](https://www.studylight.org/lexicons/hebrew/4748.html)

[Bible Hub](https://biblehub.com/str/hebrew/4748.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04748)

[Bible Bento](https://biblebento.com/dictionary/H4748.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4748/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4748.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4748)
