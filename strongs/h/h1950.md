# הוֹמָם

[Hômām](https://www.blueletterbible.org/lexicon/h1950)

Definition: Homam (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1950)

[Study Light](https://www.studylight.org/lexicons/hebrew/1950.html)

[Bible Hub](https://biblehub.com/str/hebrew/1950.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01950)

[Bible Bento](https://biblebento.com/dictionary/H1950.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1950/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1950.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1950)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/homam.html)