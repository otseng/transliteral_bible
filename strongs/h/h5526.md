# סָכַךְ

[cakak](https://www.blueletterbible.org/lexicon/h5526)

Definition: cover (15x), covering (2x), defence (1x), defendest (1x), hedge in (1x), join together (1x), set (1x), shut up (1x), to cover with one's body

Part of speech: verb

Occurs 24 times in 23 verses

Greek: [kalyptō](../g/g2572.md)

Synonyms: [cover](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Cover)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5526)

[Study Light](https://www.studylight.org/lexicons/hebrew/5526.html)

[Bible Hub](https://biblehub.com/str/hebrew/5526.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A1%D6%B8%D7%9B%D6%B7%D7%9A%D6%B0)

[NET Bible](http://classic.net.bible.org/strong.php?id=05526)

[Bible Bento](https://biblebento.com/dictionary/H5526.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5526/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5526.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5526)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A1%D7%9B%D7%9A)
