# אֻרְוָה

['urvâ](https://www.blueletterbible.org/lexicon/h723)

Definition: stall (3x).

Part of speech: feminine noun

Occurs 3 times in 3 verses

Hebrew: ['ārâ](../h/h717.md), ['ārôn](../h/h727.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h723)

[Study Light](https://www.studylight.org/lexicons/hebrew/723.html)

[Bible Hub](https://biblehub.com/str/hebrew/723.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0723)

[Bible Bento](https://biblebento.com/dictionary/H723.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/723/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/723.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h723)
