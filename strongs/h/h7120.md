# קֹר

[qōr](https://www.blueletterbible.org/lexicon/h7120)

Definition: cold (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7120)

[Study Light](https://www.studylight.org/lexicons/hebrew/7120.html)

[Bible Hub](https://biblehub.com/str/hebrew/7120.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07120)

[Bible Bento](https://biblebento.com/dictionary/H7120.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7120/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7120.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7120)
