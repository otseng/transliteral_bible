# אֲחוּמַי

['Ăḥûmay](https://www.blueletterbible.org/lexicon/h267)

Definition: Ahumai (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h267)

[Study Light](https://www.studylight.org/lexicons/hebrew/267.html)

[Bible Hub](https://biblehub.com/str/hebrew/267.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0267)

[Bible Bento](https://biblebento.com/dictionary/H267.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/267/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/267.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h267)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ahumai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ahumai)