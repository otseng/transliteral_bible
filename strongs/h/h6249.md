# עַשְׁתֵּי

[ʿaštê](https://www.blueletterbible.org/lexicon/h6249)

Definition: eleven (with H2640) (19x).

Part of speech: masculine/feminine noun

Occurs 19 times in 18 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6249)

[Study Light](https://www.studylight.org/lexicons/hebrew/6249.html)

[Bible Hub](https://biblehub.com/str/hebrew/6249.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06249)

[Bible Bento](https://biblebento.com/dictionary/H6249.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6249/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6249.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6249)
