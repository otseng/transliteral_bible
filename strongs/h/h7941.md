# שֵׁכָר

[šēḵār](https://www.blueletterbible.org/lexicon/h7941)

Definition: strong drink (21x), strong wine (1x), drunkard (1x).

Part of speech: masculine noun

Occurs 23 times in 20 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7941)

[Study Light](https://www.studylight.org/lexicons/hebrew/7941.html)

[Bible Hub](https://biblehub.com/str/hebrew/7941.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07941)

[Bible Bento](https://biblebento.com/dictionary/H7941.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7941/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7941.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7941)
