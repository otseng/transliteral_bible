# חוֹר

[ḥôr](https://www.blueletterbible.org/lexicon/h2355)

Definition: network (1x), white cloth, white stuff

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2355)

[Study Light](https://www.studylight.org/lexicons/hebrew/2355.html)

[Bible Hub](https://biblehub.com/str/hebrew/2355.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02355)

[Bible Bento](https://biblebento.com/dictionary/H2355.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2355/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2355.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2355)
