# קְצִיץ

[Qᵊṣîṣ](https://www.blueletterbible.org/lexicon/h7104)

Definition: Keziz (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7104)

[Study Light](https://www.studylight.org/lexicons/hebrew/7104.html)

[Bible Hub](https://biblehub.com/str/hebrew/7104.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07104)

[Bible Bento](https://biblebento.com/dictionary/H7104.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7104/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7104.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7104)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/K/keziz.html)