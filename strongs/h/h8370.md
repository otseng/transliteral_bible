# שְׁתַר בּוֹזְנַי

[Šᵊṯar Bôznay](https://www.blueletterbible.org/lexicon/h8370)

Definition: Shetharboznai (4x).

Part of speech: proper masculine noun

Occurs 8 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8370)

[Study Light](https://www.studylight.org/lexicons/hebrew/8370.html)

[Bible Hub](https://biblehub.com/str/hebrew/8370.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08370)

[Bible Bento](https://biblebento.com/dictionary/H8370.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8370/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8370.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8370)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shetharboznai.html)