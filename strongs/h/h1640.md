# גָּרַף

[gārap̄](https://www.blueletterbible.org/lexicon/h1640)

Definition: sweep away (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1640)

[Study Light](https://www.studylight.org/lexicons/hebrew/1640.html)

[Bible Hub](https://biblehub.com/str/hebrew/1640.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01640)

[Bible Bento](https://biblebento.com/dictionary/H1640.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1640/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1640.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1640)
