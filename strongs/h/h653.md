# אֲפֵלָה

['ăp̄ēlâ](https://www.blueletterbible.org/lexicon/h653)

Definition: darkness (6x), gloominess (2x), dark (1x), thick (1x).

Part of speech: feminine noun

Occurs 10 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0653)

[Study Light](https://www.studylight.org/lexicons/hebrew/653.html)

[Bible Hub](https://biblehub.com/str/hebrew/653.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0653)

[Bible Bento](https://biblebento.com/dictionary/H653.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/653/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/653.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h653)
