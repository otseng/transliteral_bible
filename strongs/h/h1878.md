# דָּשֵׁן

[dāšēn](https://www.blueletterbible.org/lexicon/h1878)

Definition: made fat (6x), wax fat (1x), take away ashes (1x), anoint (1x), receive his ashes (1x), accept (1x).

Part of speech: verb

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1878)

[Study Light](https://www.studylight.org/lexicons/hebrew/1878.html)

[Bible Hub](https://biblehub.com/str/hebrew/1878.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01878)

[Bible Bento](https://biblebento.com/dictionary/H1878.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1878/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1878.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1878)
