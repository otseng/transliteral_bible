# נֹגַהּ

[Nōḡah](https://www.blueletterbible.org/lexicon/h5052)

Definition: Nogah (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5052)

[Study Light](https://www.studylight.org/lexicons/hebrew/5052.html)

[Bible Hub](https://biblehub.com/str/hebrew/5052.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05052)

[Bible Bento](https://biblebento.com/dictionary/H5052.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5052/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5052.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5052)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/nogah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/nogah)