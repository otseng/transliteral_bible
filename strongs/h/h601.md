# אֲנָפָה

['ănāp̄â](https://www.blueletterbible.org/lexicon/h601)

Definition: heron (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h601)

[Study Light](https://www.studylight.org/lexicons/hebrew/601.html)

[Bible Hub](https://biblehub.com/str/hebrew/601.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0601)

[Bible Bento](https://biblebento.com/dictionary/H601.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/601/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/601.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h601)
