# מָהַר

[māhar](https://www.blueletterbible.org/lexicon/h4117)

Definition: endow (1x), surely (1x).

Part of speech: verb

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4117)

[Study Light](https://www.studylight.org/lexicons/hebrew/4117.html)

[Bible Hub](https://biblehub.com/str/hebrew/4117.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04117)

[Bible Bento](https://biblebento.com/dictionary/H4117.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4117/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4117.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4117)
