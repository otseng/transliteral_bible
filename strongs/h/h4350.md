# מְכוֹנָה

[mᵊḵônâ](https://www.blueletterbible.org/lexicon/h4350)

Definition: base (23x).

Part of speech: feminine noun

Occurs 24 times in 18 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4350)

[Study Light](https://www.studylight.org/lexicons/hebrew/4350.html)

[Bible Hub](https://biblehub.com/str/hebrew/4350.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04350)

[Bible Bento](https://biblebento.com/dictionary/H4350.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4350/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4350.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4350)
