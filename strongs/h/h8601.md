# תֻּפִינִים

[tup̄înîm](https://www.blueletterbible.org/lexicon/h8601)

Definition: baken (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8601)

[Study Light](https://www.studylight.org/lexicons/hebrew/8601.html)

[Bible Hub](https://biblehub.com/str/hebrew/8601.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08601)

[Bible Bento](https://biblebento.com/dictionary/H8601.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8601/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8601.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8601)
