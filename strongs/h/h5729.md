# עֶדֶן

[ʿEḏen](https://www.blueletterbible.org/lexicon/h5729)

Definition: Eden (3x).

Part of speech: proper locative noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5729)

[Study Light](https://www.studylight.org/lexicons/hebrew/5729.html)

[Bible Hub](https://biblehub.com/str/hebrew/5729.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05729)

[Bible Bento](https://biblebento.com/dictionary/H5729.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5729/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5729.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5729)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eden.html)