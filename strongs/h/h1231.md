# בֻּקִּי

[Buqqî](https://www.blueletterbible.org/lexicon/h1231)

Definition: Bukki (5x).

Part of speech: proper masculine noun

Occurs 5 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1231)

[Study Light](https://www.studylight.org/lexicons/hebrew/1231.html)

[Bible Hub](https://biblehub.com/str/hebrew/1231.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01231)

[Bible Bento](https://biblebento.com/dictionary/H1231.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1231/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1231.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1231)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bukki.html)

[Video Bible](https://www.videobible.com/bible-dictionary/bukki)