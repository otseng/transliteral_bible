# יְצֵב

[yᵊṣēḇ](https://www.blueletterbible.org/lexicon/h3321)

Definition: truth (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3321)

[Study Light](https://www.studylight.org/lexicons/hebrew/3321.html)

[Bible Hub](https://biblehub.com/str/hebrew/3321.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03321)

[Bible Bento](https://biblebento.com/dictionary/H3321.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3321/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3321.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3321)
