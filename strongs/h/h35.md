# אֲבִיּוֹנָה

['ăḇîyônâ](https://www.blueletterbible.org/lexicon/h35)

Definition: desire (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h35)

[Study Light](https://www.studylight.org/lexicons/hebrew/35.html)

[Bible Hub](https://biblehub.com/str/hebrew/35.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=035)

[Bible Bento](https://biblebento.com/dictionary/H35.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/35/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/35.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h35)
