# זוּל

[zûl](https://www.blueletterbible.org/lexicon/h2107)

Definition: lavish (1x), despise (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2107)

[Study Light](https://www.studylight.org/lexicons/hebrew/2107.html)

[Bible Hub](https://biblehub.com/str/hebrew/2107.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02107)

[Bible Bento](https://biblebento.com/dictionary/H2107.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2107/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2107.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2107)
