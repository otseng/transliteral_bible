# אַכְזָב

['aḵzāḇ](https://www.blueletterbible.org/lexicon/h391)

Definition: liar (1x), lie (1x).

Part of speech: adjective

Occurs 2 times in 2 verses

Hebrew: [kāzaḇ](../h/h3576.md), [kazab](../h/h3577.md), [kᵊḏaḇ](../h/h3538.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h391)

[Study Light](https://www.studylight.org/lexicons/hebrew/391.html)

[Bible Hub](https://biblehub.com/str/hebrew/391.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0391)

[Bible Bento](https://biblebento.com/dictionary/H391.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/391/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/391.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h391)
