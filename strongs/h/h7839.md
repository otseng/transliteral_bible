# שַׁחֲרוּת

[šaḥărûṯ](https://www.blueletterbible.org/lexicon/h7839)

Definition: youth (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7839)

[Study Light](https://www.studylight.org/lexicons/hebrew/7839.html)

[Bible Hub](https://biblehub.com/str/hebrew/7839.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07839)

[Bible Bento](https://biblebento.com/dictionary/H7839.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7839/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7839.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7839)
