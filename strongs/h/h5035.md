# נֶבֶל

[neḇel](https://www.blueletterbible.org/lexicon/h5035)

Definition: psalteries (22x), bottle (8x), viol (4x), flagons (1x), pitchers (1x), vessel (1x), variant (1x), a skin-bag, jar, pitcher, harp, lute, guitar, musical instrument

Part of speech: masculine noun

Occurs 38 times in 37 verses

Greek: [aggeion](../g/g30.md), [askos](../g/g779.md), [kithara](../g/g2788.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5035)

[Study Light](https://www.studylight.org/lexicons/hebrew/5035.html)

[Bible Hub](https://biblehub.com/str/hebrew/5035.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05035)

[Bible Bento](https://biblebento.com/dictionary/H5035.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5035/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5035.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5035)
