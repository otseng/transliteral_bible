# פְּתִיגִיל

[pᵊṯîḡîl](https://www.blueletterbible.org/lexicon/h6614)

Definition: stomacher (1x), rich or expensive robe

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6614)

[Study Light](https://www.studylight.org/lexicons/hebrew/6614.html)

[Bible Hub](https://biblehub.com/str/hebrew/6614.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06614)

[Bible Bento](https://biblebento.com/dictionary/H6614.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6614/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6614.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6614)
