# עַד

[ʿaḏ](https://www.blueletterbible.org/lexicon/h5706)

Definition: prey (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5706)

[Study Light](https://www.studylight.org/lexicons/hebrew/5706.html)

[Bible Hub](https://biblehub.com/str/hebrew/5706.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05706)

[Bible Bento](https://biblebento.com/dictionary/H5706.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5706/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5706.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5706)
