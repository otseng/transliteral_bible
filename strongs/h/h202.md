# אוֹן

['ôn](https://www.blueletterbible.org/lexicon/h202)

Definition: strength (7x), might (2x), force (1x), goods (1x), substance (1x).

Part of speech: masculine noun

Occurs 12 times in 12 verses

Hebrew: ['aven](../h/h205.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0202)

[Study Light](https://www.studylight.org/lexicons/hebrew/202.html)

[Bible Hub](https://biblehub.com/str/hebrew/202.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0202)

[Bible Bento](https://biblebento.com/dictionary/H202.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/202/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/202.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h202)
