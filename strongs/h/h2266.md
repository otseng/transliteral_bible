# חָבַר

[ḥāḇar](https://www.blueletterbible.org/lexicon/h2266)

Definition: couple (8x), join (8x), couple together (4x), join together (3x), compact (1x), charmer (with H2267) (1x), charming (with H2267) (1x), have fellowship (1x), league (1x), heap up (1x).

Part of speech: verb

Occurs 29 times in 25 verses

Hebrew: [Ḥeḇrôn](../h/h2275.md)

Greek: [exartizō](../g/g1822.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2266)

[Study Light](https://www.studylight.org/lexicons/hebrew/2266.html)

[Bible Hub](https://biblehub.com/str/hebrew/2266.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02266)

[Bible Bento](https://biblebento.com/dictionary/H2266.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2266/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2266.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2266)
