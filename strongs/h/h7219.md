# רֹאשׁ

[rō'š](https://www.blueletterbible.org/lexicon/h7219)

Definition: gall (9x), venom (1x), poison (1x), hemlock (1x).

Part of speech: masculine noun

Occurs 12 times in 12 verses

Greek: [cholē](../g/g5521.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7219)

[Study Light](https://www.studylight.org/lexicons/hebrew/7219.html)

[Bible Hub](https://biblehub.com/str/hebrew/7219.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07219)

[Bible Bento](https://biblebento.com/dictionary/H7219.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7219/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7219.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7219)
