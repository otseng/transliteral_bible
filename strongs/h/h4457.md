# מַלְקָחַיִם

[malqāḥayim](https://www.blueletterbible.org/lexicon/h4457)

Definition: tongs (5x), snuffers (1x).

Part of speech: masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4457)

[Study Light](https://www.studylight.org/lexicons/hebrew/4457.html)

[Bible Hub](https://biblehub.com/str/hebrew/4457.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04457)

[Bible Bento](https://biblebento.com/dictionary/H4457.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4457/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4457.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4457)
