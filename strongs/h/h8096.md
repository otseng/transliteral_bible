# שִׁמְעִי

[Šimʿî](https://www.blueletterbible.org/lexicon/h8096)

Definition: Shimei (41x), Shimhi (1x), Shimi (1x).

Part of speech: proper masculine noun

Occurs 44 times in 40 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8096)

[Study Light](https://www.studylight.org/lexicons/hebrew/8096.html)

[Bible Hub](https://biblehub.com/str/hebrew/8096.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08096)

[Bible Bento](https://biblebento.com/dictionary/H8096.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8096/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8096.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8096)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shimei.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shimei)