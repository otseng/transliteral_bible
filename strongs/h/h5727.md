# עָדַן

[ʿāḏan](https://www.blueletterbible.org/lexicon/h5727)

Definition: delighted themselves (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5727)

[Study Light](https://www.studylight.org/lexicons/hebrew/5727.html)

[Bible Hub](https://biblehub.com/str/hebrew/5727.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05727)

[Bible Bento](https://biblebento.com/dictionary/H5727.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5727/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5727.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5727)
