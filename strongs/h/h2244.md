# חָבָא

[chaba'](https://www.blueletterbible.org/lexicon/h2244)

Definition: hide (31x), held (1x), secretly (1x), withdraw

Part of speech: verb

Occurs 33 times in 33 verses

Hebrew: [maḥăḇē'](../h/h4224.md), [ḥāḇâ](../h/h2247.md)

Greek: [kryptō](../g/g2928.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2244)

[Study Light](https://www.studylight.org/lexicons/hebrew/2244.html)

[Bible Hub](https://biblehub.com/str/hebrew/2244.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02244)

[Bible Bento](https://biblebento.com/dictionary/H2244.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2244/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2244.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2244)
