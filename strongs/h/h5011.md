# נֹב

[nōḇ](https://www.blueletterbible.org/lexicon/h5011)

Definition: Nob (6x).

Part of speech: proper locative noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5011)

[Study Light](https://www.studylight.org/lexicons/hebrew/5011.html)

[Bible Hub](https://biblehub.com/str/hebrew/5011.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05011)

[Bible Bento](https://biblebento.com/dictionary/H5011.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5011/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5011.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5011)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/nob.html)

[Video Bible](https://www.videobible.com/bible-dictionary/nob)