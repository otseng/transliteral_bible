# בָּדָא

[bāḏā'](https://www.blueletterbible.org/lexicon/h908)

Definition: devise (1x), feign (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h908)

[Study Light](https://www.studylight.org/lexicons/hebrew/908.html)

[Bible Hub](https://biblehub.com/str/hebrew/908.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0908)

[Bible Bento](https://biblebento.com/dictionary/H908.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/908/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/908.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h908)
