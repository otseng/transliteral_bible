# גְּדוּד

[gᵊḏûḏ](https://www.blueletterbible.org/lexicon/h1417)

Definition: furrow (1x), cutting (1x).

Part of speech: masculine/feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1417)

[Study Light](https://www.studylight.org/lexicons/hebrew/1417.html)

[Bible Hub](https://biblehub.com/str/hebrew/1417.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01417)

[Bible Bento](https://biblebento.com/dictionary/H1417.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1417/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1417.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1417)
