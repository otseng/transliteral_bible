# אָגַר

['āḡar](https://www.blueletterbible.org/lexicon/h103)

Definition: gather (3x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h103)

[Study Light](https://www.studylight.org/lexicons/hebrew/103.html)

[Bible Hub](https://biblehub.com/str/hebrew/103.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0103)

[Bible Bento](https://biblebento.com/dictionary/H103.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/103/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/103.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h103)
