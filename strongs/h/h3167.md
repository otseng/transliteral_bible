# יַחְזְיָה

[Yaḥzᵊyâ](https://www.blueletterbible.org/lexicon/h3167)

Definition: Jahaziah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3167)

[Study Light](https://www.studylight.org/lexicons/hebrew/3167.html)

[Bible Hub](https://biblehub.com/str/hebrew/3167.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03167)

[Bible Bento](https://biblebento.com/dictionary/H3167.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3167/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3167.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3167)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jahaziah.html)