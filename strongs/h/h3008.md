# יִגְאָל

[Yiḡ'āl](https://www.blueletterbible.org/lexicon/h3008)

Definition: Igal (2x), Igeal (1x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3008)

[Study Light](https://www.studylight.org/lexicons/hebrew/3008.html)

[Bible Hub](https://biblehub.com/str/hebrew/3008.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03008)

[Bible Bento](https://biblebento.com/dictionary/H3008.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3008/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3008.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3008)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/igal.html)

[Video Bible](https://www.videobible.com/bible-dictionary/igal)