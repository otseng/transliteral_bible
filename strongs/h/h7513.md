# רַפְסֹדוֹת

[rap̄sōḏôṯ](https://www.blueletterbible.org/lexicon/h7513)

Definition: flote (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7513)

[Study Light](https://www.studylight.org/lexicons/hebrew/7513.html)

[Bible Hub](https://biblehub.com/str/hebrew/7513.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07513)

[Bible Bento](https://biblebento.com/dictionary/H7513.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7513/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7513.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7513)
