# אֲבִישַׁי

['Ăḇîšay](https://www.blueletterbible.org/lexicon/h52)

Definition: Abishai (25x).

Part of speech: proper masculine noun

Occurs 25 times in 24 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h52)

[Study Light](https://www.studylight.org/lexicons/hebrew/52.html)

[Bible Hub](https://biblehub.com/str/hebrew/52.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=052)

[Bible Bento](https://biblebento.com/dictionary/H52.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/52/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/52.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h52)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/abishai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/abishai)