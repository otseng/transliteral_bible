# רְמַלְיָהוּ

[Rᵊmalyâû](https://www.blueletterbible.org/lexicon/h7425)

Definition: Remaliah (13x), "protected by Jehovah", father of king Pekah of the northern kingdom of Israel

Part of speech: proper masculine noun

Occurs 13 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7425)

[Study Light](https://www.studylight.org/lexicons/hebrew/7425.html)

[Bible Hub](https://biblehub.com/str/hebrew/7425.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07425)

[Bible Bento](https://biblebento.com/dictionary/H7425.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7425/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7425.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7425)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/R/remaliah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/remaliah)