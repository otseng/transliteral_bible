# חַרְחוּר

[Ḥarḥûr](https://www.blueletterbible.org/lexicon/h2744)

Definition: Harhur (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2744)

[Study Light](https://www.studylight.org/lexicons/hebrew/2744.html)

[Bible Hub](https://biblehub.com/str/hebrew/2744.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02744)

[Bible Bento](https://biblebento.com/dictionary/H2744.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2744/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2744.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2744)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/harhur.html)

[Video Bible](https://www.videobible.com/bible-dictionary/harhur)