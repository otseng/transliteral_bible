# כְּלוּב

[Kᵊlûḇ](https://www.blueletterbible.org/lexicon/h3620)

Definition: Chelub (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3620)

[Study Light](https://www.studylight.org/lexicons/hebrew/3620.html)

[Bible Hub](https://biblehub.com/str/hebrew/3620.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03620)

[Bible Bento](https://biblebento.com/dictionary/H3620.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3620/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3620.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3620)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/chelub.html)

[Video Bible](https://www.videobible.com/bible-dictionary/chelub)