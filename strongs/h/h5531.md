# סִכְלוּת

[siḵlûṯ](https://www.blueletterbible.org/lexicon/h5531)

Definition: folly (5x), foolishness (2x).

Part of speech: masculine noun

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5531)

[Study Light](https://www.studylight.org/lexicons/hebrew/5531.html)

[Bible Hub](https://biblehub.com/str/hebrew/5531.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05531)

[Bible Bento](https://biblebento.com/dictionary/H5531.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5531/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5531.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5531)
