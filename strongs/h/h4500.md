# מָנוֹר

[mānôr](https://www.blueletterbible.org/lexicon/h4500)

Definition: beam (4x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

Hebrew: [mᵊnôrâ](../h/h4501.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4500)

[Study Light](https://www.studylight.org/lexicons/hebrew/4500.html)

[Bible Hub](https://biblehub.com/str/hebrew/4500.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04500)

[Bible Bento](https://biblebento.com/dictionary/H4500.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4500/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4500.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4500)
