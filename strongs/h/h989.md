# בְּטֵל

[bᵊṭēl](https://www.blueletterbible.org/lexicon/h989)

Definition: cease (5x), hindered (1x).

Part of speech: verb

Occurs 6 times in 5 verses

Greek: [katargeō](../g/g2673.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h989)

[Study Light](https://www.studylight.org/lexicons/hebrew/989.html)

[Bible Hub](https://biblehub.com/str/hebrew/989.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0989)

[Bible Bento](https://biblebento.com/dictionary/H989.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/989/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/989.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h989)
