# תָּעַב

[ta`ab](https://www.blueletterbible.org/lexicon/h8581)

Definition: abhor (14x), abominable (6x), abominably (1x), utterly (1x), be detested

Part of speech: verb

Occurs 22 times in 20 verses

Hebrew: [tôʿēḇâ](../h/h8441.md)

Greek: [anomia](../g/g458.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8581)

[Study Light](https://www.studylight.org/lexicons/hebrew/8581.html)

[Bible Hub](https://biblehub.com/str/hebrew/8581.htm)

[Morfix](https://www.morfix.co.il/en/%D7%AA%D6%BC%D6%B8%D7%A2%D6%B7%D7%91)

[NET Bible](http://classic.net.bible.org/strong.php?id=08581)

[Bible Bento](https://biblebento.com/dictionary/H8581.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8581/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8581.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8581)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%AA%D7%99%D7%A2%D7%91)
