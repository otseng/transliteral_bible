# בִּלְעֲדֵי

[bilʿăḏê](https://www.blueletterbible.org/lexicon/h1107)

Definition: beside (7x), save (4x), without (4x), not in me (1x), not (1x).

Part of speech: adverb

Occurs 17 times in 16 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1107)

[Study Light](https://www.studylight.org/lexicons/hebrew/1107.html)

[Bible Hub](https://biblehub.com/str/hebrew/1107.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01107)

[Bible Bento](https://biblebento.com/dictionary/H1107.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1107/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1107.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1107)
