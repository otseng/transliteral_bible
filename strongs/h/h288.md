# אֲחִימֶלֶךְ

['Ăḥîmeleḵ](https://www.blueletterbible.org/lexicon/h288)

Definition: Ahimelech (17x).

Part of speech: proper masculine noun

Occurs 17 times in 16 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h288)

[Study Light](https://www.studylight.org/lexicons/hebrew/288.html)

[Bible Hub](https://biblehub.com/str/hebrew/288.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0288)

[Bible Bento](https://biblebento.com/dictionary/H288.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/288/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/288.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h288)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ahimelech.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ahimelech)