# סְאָה

[sᵊ'â](https://www.blueletterbible.org/lexicon/h5429)

Definition: measure (9x).

Part of speech: feminine noun

Occurs 9 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5429)

[Study Light](https://www.studylight.org/lexicons/hebrew/5429.html)

[Bible Hub](https://biblehub.com/str/hebrew/5429.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05429)

[Bible Bento](https://biblebento.com/dictionary/H5429.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5429/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5429.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5429)
