# מַלְאָכוּת

[mal'āḵûṯ](https://www.blueletterbible.org/lexicon/h4400)

Definition: message (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4400)

[Study Light](https://www.studylight.org/lexicons/hebrew/4400.html)

[Bible Hub](https://biblehub.com/str/hebrew/4400.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04400)

[Bible Bento](https://biblebento.com/dictionary/H4400.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4400/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4400.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4400)
