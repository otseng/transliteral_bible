# עֱזוּז

[ʿĕzûz](https://www.blueletterbible.org/lexicon/h5807)

Definition: strength (2x), might (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5807)

[Study Light](https://www.studylight.org/lexicons/hebrew/5807.html)

[Bible Hub](https://biblehub.com/str/hebrew/5807.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05807)

[Bible Bento](https://biblebento.com/dictionary/H5807.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5807/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5807.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5807)
