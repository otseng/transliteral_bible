# בָּהִיר

[bāhîr](https://www.blueletterbible.org/lexicon/h925)

Definition: bright (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h925)

[Study Light](https://www.studylight.org/lexicons/hebrew/925.html)

[Bible Hub](https://biblehub.com/str/hebrew/925.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0925)

[Bible Bento](https://biblebento.com/dictionary/H925.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/925/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/925.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h925)
