# אֶחָד

['echad](https://www.blueletterbible.org/lexicon/h259)

Definition: one (687x), first (36x), another (35x), other (30x), any (18x), once (13x), eleven (with H6240) (13x), every (10x), certain (9x), an (7x), some (7x)

Part of speech: adjective

Occurs 952 times in 739 verses

Greek: [hapax](../g/g530.md), [allēlōn](../g/g240.md), [prōtos](../g/g4413.md)

Edenics: each

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0259)

[Study Light](https://www.studylight.org/lexicons/hebrew/259.html)

[Bible Hub](https://biblehub.com/str/hebrew/259.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0259)

[Bible Bento](https://biblebento.com/dictionary/H0259.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0259/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0259.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h259)
