# גָּרָה

[gārâ](https://www.blueletterbible.org/lexicon/h1624)

Definition: stir up (6x), meddle (4x), contend (3x), strive (1x).

Part of speech: verb

Occurs 15 times in 13 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1624)

[Study Light](https://www.studylight.org/lexicons/hebrew/1624.html)

[Bible Hub](https://biblehub.com/str/hebrew/1624.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01624)

[Bible Bento](https://biblebento.com/dictionary/H1624.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1624/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1624.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1624)
