# יְהוֹזָבָד

[Yᵊhôzāḇāḏ](https://www.blueletterbible.org/lexicon/h3075)

Definition: Jehozabad (4x).

Part of speech: proper masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3075)

[Study Light](https://www.studylight.org/lexicons/hebrew/3075.html)

[Bible Hub](https://biblehub.com/str/hebrew/3075.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03075)

[Bible Bento](https://biblebento.com/dictionary/H3075.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3075/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3075.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3075)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/jehozabad.html)

[Video Bible](https://www.videobible.com/bible-dictionary/jehozabad)