# עָלַע

[ʿālaʿ](https://www.blueletterbible.org/lexicon/h5966)

Definition: suck up (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5966)

[Study Light](https://www.studylight.org/lexicons/hebrew/5966.html)

[Bible Hub](https://biblehub.com/str/hebrew/5966.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05966)

[Bible Bento](https://biblebento.com/dictionary/H5966.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5966/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5966.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5966)
