# אַלְמֹן

['almōn](https://www.blueletterbible.org/lexicon/h489)

Definition: widowhood (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

Hebrew: ['almān](../h/h488.md), ['almānâ](../h/h490.md), ['almānûṯ](../h/h491.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0489)

[Study Light](https://www.studylight.org/lexicons/hebrew/489.html)

[Bible Hub](https://biblehub.com/str/hebrew/489.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0489)

[Bible Bento](https://biblebento.com/dictionary/H489.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/489/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/489.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h489)
