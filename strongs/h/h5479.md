# סוֹטַי

[Sôṭay](https://www.blueletterbible.org/lexicon/h5479)

Definition: Sotai (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5479)

[Study Light](https://www.studylight.org/lexicons/hebrew/5479.html)

[Bible Hub](https://biblehub.com/str/hebrew/5479.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05479)

[Bible Bento](https://biblebento.com/dictionary/H5479.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5479/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5479.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5479)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/sotai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/sotai)