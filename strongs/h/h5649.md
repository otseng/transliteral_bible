# עֲבַד

[ʿăḇaḏ](https://www.blueletterbible.org/lexicon/h5649)

Definition: servant (7x).

Part of speech: masculine noun

Occurs 8 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5649)

[Study Light](https://www.studylight.org/lexicons/hebrew/5649.html)

[Bible Hub](https://biblehub.com/str/hebrew/5649.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05649)

[Bible Bento](https://biblebento.com/dictionary/H5649.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5649/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5649.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5649)
