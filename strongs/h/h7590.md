# שוט

[shâʼṭ](https://www.blueletterbible.org/lexicon/h7590)

Definition: despise (3x), hate, treat with contempt or despite

Part of speech: verb

Occurs 3 times in 3 verses

Synonyms: [hate](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Hate)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7590)

[Study Light](https://www.studylight.org/lexicons/hebrew/7590.html)

[Bible Hub](https://biblehub.com/str/hebrew/7590.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07590)

[Bible Bento](https://biblebento.com/dictionary/H7590.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7590/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7590.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7590)
