# זֶרֶד

[zereḏ](https://www.blueletterbible.org/lexicon/h2218)

Definition: Zered (3x), Zared (1x).

Part of speech: proper locative noun

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2218)

[Study Light](https://www.studylight.org/lexicons/hebrew/2218.html)

[Bible Hub](https://biblehub.com/str/hebrew/2218.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02218)

[Bible Bento](https://biblebento.com/dictionary/H2218.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2218/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2218.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2218)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zered.html)

[Video Bible](https://www.videobible.com/bible-dictionary/zered)