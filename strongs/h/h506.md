# אֶלֶף

['elep̄](https://www.blueletterbible.org/lexicon/h506)

Definition: thousand (4x).

Part of speech: masculine noun

Occurs 5 times in 2 verses

Hebrew: ['eleph](../h/h504.md), ['elep̄](../h/h505.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h506)

[Study Light](https://www.studylight.org/lexicons/hebrew/506.html)

[Bible Hub](https://biblehub.com/str/hebrew/506.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0506)

[Bible Bento](https://biblebento.com/dictionary/H506.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/506/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/506.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h506)
