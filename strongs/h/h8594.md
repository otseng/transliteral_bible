# תַּעֲרוּבָה

[taʿărûḇâ](https://www.blueletterbible.org/lexicon/h8594)

Definition: hostage (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8594)

[Study Light](https://www.studylight.org/lexicons/hebrew/8594.html)

[Bible Hub](https://biblehub.com/str/hebrew/8594.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08594)

[Bible Bento](https://biblebento.com/dictionary/H8594.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8594/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8594.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8594)
