# שַׁוְעָה

[shav`ah](https://www.blueletterbible.org/lexicon/h7775)

Definition: cry (11x).

Part of speech: feminine noun

Occurs 11 times in 11 verses

Synonyms: [cry](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Cry)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7775)

[Study Light](https://www.studylight.org/lexicons/hebrew/7775.html)

[Bible Hub](https://biblehub.com/str/hebrew/7775.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07775)

[Bible Bento](https://biblebento.com/dictionary/H7775.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7775/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7775.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7775)
