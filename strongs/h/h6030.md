# עָנָה

[`anah](https://www.blueletterbible.org/lexicon/h6030)

Definition: answer (242x), hear (42x), testify (12x), speak (8x), sing (4x), bear (3x), cry (2x), witness (2x), give (1x), miscellaneous (13x).

Part of speech: verb

Occurs 330 times in 317 verses

Hebrew: [maʿănê](../h/h4617.md), [ʿănâ](../h/h6032.md)

Greek: [apokrinomai](../g/g611.md), [akouō](../g/g191.md), [epakouō](../g/g1873.md), [apokrisis](../g/g612.md), [martyreō](../g/g3140.md), [katamartyreō](../g/g2649.md), [adō](../g/g103.md)

Edenics: answer, know

Synonyms: [hear](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Hear)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6030)

[Study Light](https://www.studylight.org/lexicons/hebrew/6030.html)

[Bible Hub](https://biblehub.com/str/hebrew/6030.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A2%D6%B8%D7%A0%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=06030)

[Bible Bento](https://biblebento.com/dictionary/H6030.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6030/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6030.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6030)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A2%D7%A0%D7%94)
