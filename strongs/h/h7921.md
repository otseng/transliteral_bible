# שָׁכֹל

[šāḵōl](https://www.blueletterbible.org/lexicon/h7921)

Definition: bereave (10x), barren (2x), childless (2x), cast young (2x), cast a calf (1x), lost children (1x), rob of children (1x), deprived (1x), miscellaneous (5x).

Part of speech: verb

Occurs 24 times in 22 verses

Greek: [astheneō](../g/g770.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7921)

[Study Light](https://www.studylight.org/lexicons/hebrew/7921.html)

[Bible Hub](https://biblehub.com/str/hebrew/7921.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07921)

[Bible Bento](https://biblebento.com/dictionary/H7921.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7921/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7921.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7921)
