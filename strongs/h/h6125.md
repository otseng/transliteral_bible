# עָקָה

[ʿāqâ](https://www.blueletterbible.org/lexicon/h6125)

Definition: oppression (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6125)

[Study Light](https://www.studylight.org/lexicons/hebrew/6125.html)

[Bible Hub](https://biblehub.com/str/hebrew/6125.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06125)

[Bible Bento](https://biblebento.com/dictionary/H6125.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6125/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6125.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6125)
