# רֶקַח

[reqaḥ](https://www.blueletterbible.org/lexicon/h7544)

Definition: spiced (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7544)

[Study Light](https://www.studylight.org/lexicons/hebrew/7544.html)

[Bible Hub](https://biblehub.com/str/hebrew/7544.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07544)

[Bible Bento](https://biblebento.com/dictionary/H7544.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7544/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7544.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7544)
