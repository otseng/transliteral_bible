# תַּנְחֻמֶת

[Tanḥumeṯ](https://www.blueletterbible.org/lexicon/h8576)

Definition: Tanhumeth (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8576)

[Study Light](https://www.studylight.org/lexicons/hebrew/8576.html)

[Bible Hub](https://biblehub.com/str/hebrew/8576.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08576)

[Bible Bento](https://biblebento.com/dictionary/H8576.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8576/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8576.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8576)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/T/tanhumeth.html)

[Video Bible](https://www.videobible.com/bible-dictionary/tanhumeth)