# זִיו

[zîv](https://www.blueletterbible.org/lexicon/h2122)

Definition: countenance (4x), brightness (2x).

Part of speech: masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2122)

[Study Light](https://www.studylight.org/lexicons/hebrew/2122.html)

[Bible Hub](https://biblehub.com/str/hebrew/2122.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02122)

[Bible Bento](https://biblebento.com/dictionary/H2122.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2122/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2122.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2122)
