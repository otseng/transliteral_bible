# שָׂקַד

[śāqaḏ](https://www.blueletterbible.org/lexicon/h8244)

Definition: bound (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8244)

[Study Light](https://www.studylight.org/lexicons/hebrew/8244.html)

[Bible Hub](https://biblehub.com/str/hebrew/8244.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08244)

[Bible Bento](https://biblebento.com/dictionary/H8244.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8244/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8244.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8244)
