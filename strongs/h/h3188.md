# יַחַשׂ

[yaḥaś](https://www.blueletterbible.org/lexicon/h3188)

Definition: genealogy (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3188)

[Study Light](https://www.studylight.org/lexicons/hebrew/3188.html)

[Bible Hub](https://biblehub.com/str/hebrew/3188.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03188)

[Bible Bento](https://biblebento.com/dictionary/H3188.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3188/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3188.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3188)
