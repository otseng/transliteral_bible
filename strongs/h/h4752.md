# מַר

[mar](https://www.blueletterbible.org/lexicon/h4752)

Definition: drop (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4752)

[Study Light](https://www.studylight.org/lexicons/hebrew/4752.html)

[Bible Hub](https://biblehub.com/str/hebrew/4752.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04752)

[Bible Bento](https://biblebento.com/dictionary/H4752.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4752/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4752.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4752)
