# אַרְיוֹךְ

['aryôḵ](https://www.blueletterbible.org/lexicon/h746)

Definition: Arioch (7x).

Part of speech: proper masculine noun

Occurs 7 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0746)

[Study Light](https://www.studylight.org/lexicons/hebrew/746.html)

[Bible Hub](https://biblehub.com/str/hebrew/746.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0746)

[Bible Bento](https://biblebento.com/dictionary/H746.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/746/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/746.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h746)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/arioch.html)

[Video Bible](https://www.videobible.com/bible-dictionary/arioch)