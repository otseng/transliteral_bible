# חַבָּר

[ḥabār](https://www.blueletterbible.org/lexicon/h2271)

Definition: companions (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2271)

[Study Light](https://www.studylight.org/lexicons/hebrew/2271.html)

[Bible Hub](https://biblehub.com/str/hebrew/2271.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02271)

[Bible Bento](https://biblebento.com/dictionary/H2271.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2271/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2271.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2271)
