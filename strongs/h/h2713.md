# חָקַר

[chaqar](https://www.blueletterbible.org/lexicon/h2713)

Definition: search (12x), search out (9x), found out (2x), seek out (1x), seek (1x), sounded (1x), try (1x), investigate, examine, explore

Part of speech: verb

Occurs 27 times in 26 verses

Hebrew: [ḥēqer](../h/h2714.md)

Greek: [eraunaō](../g/g2045.md), [erōtaō](../g/g2065.md)

Synonyms: [seek](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Seek)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2713)

[Study Light](https://www.studylight.org/lexicons/hebrew/2713.html)

[Bible Hub](https://biblehub.com/str/hebrew/2713.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02713)

[Bible Bento](https://biblebento.com/dictionary/H2713.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2713/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2713.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2713)
