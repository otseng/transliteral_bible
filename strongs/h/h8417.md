# תָּהֳלָה

[tāhŏlâ](https://www.blueletterbible.org/lexicon/h8417)

Definition: folly (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8417)

[Study Light](https://www.studylight.org/lexicons/hebrew/8417.html)

[Bible Hub](https://biblehub.com/str/hebrew/8417.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08417)

[Bible Bento](https://biblebento.com/dictionary/H8417.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8417/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8417.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8417)
