# טוֹב

[towb](https://www.blueletterbible.org/lexicon/h2896)

Definition: good (361x), better (72x), well (20x), goodness (16x), goodly (9x), best (8x), merry (7x), fair (7x), prosperity (6x), precious (4x), fine (3x), wealth (3x), beautiful (2x), fairer (2x), favour (2x), glad (2x)

Part of speech: adjective, feminine noun, masculine noun

Occurs 559 times in 517 verses

Hebrew: [ṭôḇ](../h/h2895.md), [ṭûḇ](../h/h2898.md), [yatab](../h/h3190.md)

Greek: [agathos](../g/g18.md)

Pictoral: basket (surround) house

Usage: [mazel tov](https://en.wikipedia.org/wiki/Mazel_tov)

Synonyms: [good](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Good)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2896)

[Study Light](https://www.studylight.org/lexicons/hebrew/2896.html)

[Bible Hub](https://biblehub.com/str/hebrew/2896.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%98%D7%95%D7%91)

[NET Bible](http://classic.net.bible.org/strong.php?id=02896)

[Bible Bento](https://biblebento.com/dictionary/H2896.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2896/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2896.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2896)