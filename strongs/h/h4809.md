# מְרִיבָה

[Mᵊrîḇâ](https://www.blueletterbible.org/lexicon/h4809)

Definition: Meribah (7x), strong's synonym (2x).

Part of speech: proper locative noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4809)

[Study Light](https://www.studylight.org/lexicons/hebrew/4809.html)

[Bible Hub](https://biblehub.com/str/hebrew/4809.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04809)

[Bible Bento](https://biblebento.com/dictionary/H4809.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4809/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4809.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4809)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/meribah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/meribah)