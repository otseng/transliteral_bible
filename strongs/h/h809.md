# אֲשִׁישָׁה

['ăšîšâ](https://www.blueletterbible.org/lexicon/h809)

Definition: flagon (4x), raisin-cake, cake of raisins

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h809)

[Study Light](https://www.studylight.org/lexicons/hebrew/809.html)

[Bible Hub](https://biblehub.com/str/hebrew/809.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0809)

[Bible Bento](https://biblebento.com/dictionary/H809.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/809/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/809.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h809)
