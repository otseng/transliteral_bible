# כְּהַל

[kᵊhal](https://www.blueletterbible.org/lexicon/h3546)

Definition: able (2x), could (2x).

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3546)

[Study Light](https://www.studylight.org/lexicons/hebrew/3546.html)

[Bible Hub](https://biblehub.com/str/hebrew/3546.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03546)

[Bible Bento](https://biblebento.com/dictionary/H3546.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3546/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3546.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3546)
