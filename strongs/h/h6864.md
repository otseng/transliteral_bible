# צֹר

[ṣōr](https://www.blueletterbible.org/lexicon/h6864)

Definition: sharp stone (1x), flint (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

Hebrew: [tsuwr](../h/h6697.md)

Names: [Ṣōr](../h/h6865.md)

Greek: [psēphos](../g/g5586.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6864)

[Study Light](https://www.studylight.org/lexicons/hebrew/6864.html)

[Bible Hub](https://biblehub.com/str/hebrew/6864.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06864)

[Bible Bento](https://biblebento.com/dictionary/H6864.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6864/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6864.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6864)
