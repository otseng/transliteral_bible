# יוֹיָקִים

[Yôyāqîm](https://www.blueletterbible.org/lexicon/h3113)

Definition: Joiakim (4x).

Part of speech: proper masculine noun

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3113)

[Study Light](https://www.studylight.org/lexicons/hebrew/3113.html)

[Bible Hub](https://biblehub.com/str/hebrew/3113.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03113)

[Bible Bento](https://biblebento.com/dictionary/H3113.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3113/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3113.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3113)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/J/joiakim.html)

[Video Bible](https://www.videobible.com/bible-dictionary/joiakim)