# אֱלֹהִים

['Elohiym](https://www.blueletterbible.org/lexicon/h430)

Definition: God (2,346x), god (244x), judge (5x), GOD (1x), goddess (2x), great (2x), mighty (2x), angels (1x), exceeding (1x), God-ward (with H4136) (1x), godly (1x), rulers, divine ones

Part of speech: masculine noun

Occurs 2,606 times in 2,249 

Greek: [theos](../g/g2316.md)

Synonyms: [God](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#God), [mighty](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Mighty)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0430)

[Study Light](https://www.studylight.org/lexicons/hebrew/430.html)

[Bible Hub](https://biblehub.com/str/hebrew/430.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0430)

[Bible Bento](https://biblebento.com/dictionary/H0430.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0430/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0430.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h430)