# נְמִבְזָה

[nᵊmiḇzâ](https://www.blueletterbible.org/lexicon/h5240)

Definition: vile (1x).

Part of speech: participle

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5240)

[Study Light](https://www.studylight.org/lexicons/hebrew/5240.html)

[Bible Hub](https://biblehub.com/str/hebrew/5240.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05240)

[Bible Bento](https://biblebento.com/dictionary/H5240.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5240/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5240.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5240)
