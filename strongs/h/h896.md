# בַּבְלַי

[Baḇlay](https://www.blueletterbible.org/lexicon/h896)

Definition: Babylonian (1x).

Part of speech: plural emphatic substantival adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h896)

[Study Light](https://www.studylight.org/lexicons/hebrew/896.html)

[Bible Hub](https://biblehub.com/str/hebrew/896.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0896)

[Bible Bento](https://biblebento.com/dictionary/H896.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/896/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/896.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h896)
