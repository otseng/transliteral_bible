# עַדְלַי

[ʿAḏlay](https://www.blueletterbible.org/lexicon/h5724)

Definition: Adlai (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5724)

[Study Light](https://www.studylight.org/lexicons/hebrew/5724.html)

[Bible Hub](https://biblehub.com/str/hebrew/5724.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05724)

[Bible Bento](https://biblebento.com/dictionary/H5724.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5724/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5724.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5724)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/adlai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/adlai)