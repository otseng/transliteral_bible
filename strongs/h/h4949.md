# מִשְׁקֶלֶת

[mišqeleṯ](https://www.blueletterbible.org/lexicon/h4949)

Definition: plummet (2x), level, levelling tool or instrument

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4949)

[Study Light](https://www.studylight.org/lexicons/hebrew/4949.html)

[Bible Hub](https://biblehub.com/str/hebrew/4949.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04949)

[Bible Bento](https://biblebento.com/dictionary/H4949.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4949/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4949.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4949)
