# גִּינַת

[Gînaṯ](https://www.blueletterbible.org/lexicon/h1527)

Definition: Ginath (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1527)

[Study Light](https://www.studylight.org/lexicons/hebrew/1527.html)

[Bible Hub](https://biblehub.com/str/hebrew/1527.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01527)

[Bible Bento](https://biblebento.com/dictionary/H1527.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1527/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1527.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1527)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/ginath.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ginath)