# חֶמֶד

[ḥemeḏ](https://www.blueletterbible.org/lexicon/h2531)

Definition: desirable (3x), pleasant (2x), variant (1x).

Part of speech: masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2531)

[Study Light](https://www.studylight.org/lexicons/hebrew/2531.html)

[Bible Hub](https://biblehub.com/str/hebrew/2531.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02531)

[Bible Bento](https://biblebento.com/dictionary/H2531.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2531/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2531.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2531)
