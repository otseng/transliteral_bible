# אָבֵל

['āḇēl](https://www.blueletterbible.org/lexicon/h57)

Definition: mourn (8x).

Part of speech: adjective

Occurs 8 times in 8 verses

Hebrew: ['āḇal](../h/h56.md), ['ēḇel](../h/h60.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0057)

[Study Light](https://www.studylight.org/lexicons/hebrew/57.html)

[Bible Hub](https://biblehub.com/str/hebrew/57.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=057)

[Bible Bento](https://biblebento.com/dictionary/H57.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/57/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/57.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h57)
