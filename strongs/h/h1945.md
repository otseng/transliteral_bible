# הוֹי

[hôy](https://www.blueletterbible.org/lexicon/h1945)

Definition: woe (36x), Ah (7x), Ho (4x), O (3x), Alas (2x).

Part of speech: interjection

Occurs 51 times in 47 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1945)

[Study Light](https://www.studylight.org/lexicons/hebrew/1945.html)

[Bible Hub](https://biblehub.com/str/hebrew/1945.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01945)

[Bible Bento](https://biblebento.com/dictionary/H1945.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1945/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1945.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1945)
