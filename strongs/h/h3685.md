# כְּסִיל

[kᵊsîl](https://www.blueletterbible.org/lexicon/h3685)

Definition: Orion (3x), constellation (1x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3685)

[Study Light](https://www.studylight.org/lexicons/hebrew/3685.html)

[Bible Hub](https://biblehub.com/str/hebrew/3685.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03685)

[Bible Bento](https://biblebento.com/dictionary/H3685.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3685/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3685.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3685)
