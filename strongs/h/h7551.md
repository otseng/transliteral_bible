# רָקַם

[rāqam](https://www.blueletterbible.org/lexicon/h7551)

Definition: needlework (with H4639) (4x), needlework (2x), embroiderer (2x), curiously wrought (1x).

Part of speech: verb

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7551)

[Study Light](https://www.studylight.org/lexicons/hebrew/7551.html)

[Bible Hub](https://biblehub.com/str/hebrew/7551.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07551)

[Bible Bento](https://biblebento.com/dictionary/H7551.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7551/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7551.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7551)
