# כִּלְאָב

[Kil'Āḇ](https://www.blueletterbible.org/lexicon/h3609)

Definition: Chileab (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3609)

[Study Light](https://www.studylight.org/lexicons/hebrew/3609.html)

[Bible Hub](https://biblehub.com/str/hebrew/3609.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03609)

[Bible Bento](https://biblebento.com/dictionary/H3609.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3609/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3609.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3609)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/chileab.html)

[Video Bible](https://www.videobible.com/bible-dictionary/chileab)