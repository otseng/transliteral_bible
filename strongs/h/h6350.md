# פַּחֲזוּת

[paḥăzûṯ](https://www.blueletterbible.org/lexicon/h6350)

Definition: lightness (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6350)

[Study Light](https://www.studylight.org/lexicons/hebrew/6350.html)

[Bible Hub](https://biblehub.com/str/hebrew/6350.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06350)

[Bible Bento](https://biblebento.com/dictionary/H6350.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6350/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6350.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6350)
