# חָלַל

[ḥālal](https://www.blueletterbible.org/lexicon/h2490)

Definition: begin (52x), profane (36x), pollute (23x), defile (9x), break (4x), wounded (3x), eat (2x), slay (2x), first (1x), gather grapes (1x), inheritance (1x), began men (1x), piped (1x), players (1x), prostitute (1x), sorrow (1x), stain (1x), eat as common things (1x), bore through, drill, pierce

Part of speech: verb

Occurs 143 times in 132 verses

Hebrew: [ḥālîlâ](../h/h2486.md), [ḥālāl](../h/h2491.md)

Greek: [archomai](../g/g756.md), [enarchomai](../g/g1728.md), [bebēloō](../g/g953.md), [koinoō](../g/g2840.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2490)

[Study Light](https://www.studylight.org/lexicons/hebrew/2490.html)

[Bible Hub](https://biblehub.com/str/hebrew/2490.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02490)

[Bible Bento](https://biblebento.com/dictionary/H2490.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2490/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2490.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2490)
