# גְּדֵרָה

[gᵊḏērâ](https://www.blueletterbible.org/lexicon/h1448)

Definition: hedge (4x), fold (3x), wall (1x), sheepfold (1x), sheepcote (1x).

Part of speech: feminine noun

Occurs 11 times in 11 verses

## Articles

[Study Light](https://www.studylight.org/lexicons/hebrew/1448.html)

[Bible Hub](https://biblehub.com/str/hebrew/1448.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01448)

[Bible Bento](https://biblebento.com/dictionary/H1448.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1448/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1448.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1448)
