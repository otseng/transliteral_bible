# אֲדֹנִיקָם

['Ăḏōnîqām](https://www.blueletterbible.org/lexicon/h140)

Definition: Adonikam (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h140)

[Study Light](https://www.studylight.org/lexicons/hebrew/140.html)

[Bible Hub](https://biblehub.com/str/hebrew/140.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0140)

[Bible Bento](https://biblebento.com/dictionary/H140.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/140/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/140.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h140)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/adonikam.html)

[Video Bible](https://www.videobible.com/bible-dictionary/adonikam)