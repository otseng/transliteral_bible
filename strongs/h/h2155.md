# זִמָּה

[Zimmâ](https://www.blueletterbible.org/lexicon/h2155)

Definition: Zimmah (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2155)

[Study Light](https://www.studylight.org/lexicons/hebrew/2155.html)

[Bible Hub](https://biblehub.com/str/hebrew/2155.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02155)

[Bible Bento](https://biblebento.com/dictionary/H2155.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2155/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2155.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2155)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zimmah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/zimmah)