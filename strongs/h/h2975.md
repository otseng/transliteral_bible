# יְאֹר

[yᵊ'ōr](https://www.blueletterbible.org/lexicon/h2975)

Definition: river (53x), brooks (5x), flood (5x), streams (1x).

Part of speech: masculine noun

Occurs 65 times in 48 verses

Hebrew: [peleḡ](../h/h6388.md)

Greek: [potamos](../g/g4215.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2975)

[Study Light](https://www.studylight.org/lexicons/hebrew/2975.html)

[Bible Hub](https://biblehub.com/str/hebrew/2975.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02975)

[Bible Bento](https://biblebento.com/dictionary/H2975.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2975/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2975.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2975)
