# רֶכֶב

[reḵeḇ](https://www.blueletterbible.org/lexicon/h7393)

Definition: chariot (115x), millstone (3x), wagons (1x), variant (1x).

Part of speech: masculine noun

Occurs 120 times in 104 verses

Hebrew: [rāḵaḇ](../h/h7392.md), [rakāḇ](../h/h7395.md), [riḵbâ](../h/h7396.md), [rᵊḵûḇ](../h/h7398.md)

Greek: [harma](../g/g716.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7393)

[Study Light](https://www.studylight.org/lexicons/hebrew/7393.html)

[Bible Hub](https://biblehub.com/str/hebrew/7393.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07393)

[Bible Bento](https://biblebento.com/dictionary/H7393.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7393/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7393.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7393)
