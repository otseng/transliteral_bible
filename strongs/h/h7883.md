# שִׁיחוֹר

[šîḥôr](https://www.blueletterbible.org/lexicon/h7883)

Definition: Sihor (3x), Shihor (1x).

Part of speech: proper feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7883)

[Study Light](https://www.studylight.org/lexicons/hebrew/7883.html)

[Bible Hub](https://biblehub.com/str/hebrew/7883.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07883)

[Bible Bento](https://biblebento.com/dictionary/H7883.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7883/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7883.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7883)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/sihor.html)