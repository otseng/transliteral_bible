# עֶזְרָה

[ʿezrâ](https://www.blueletterbible.org/lexicon/h5833)

Definition: help (25x), helpers (1x).

Part of speech: feminine noun

Occurs 26 times in 25 verses

Hebrew: ['ezer](../h/h5828.md)

Names: [ʿEzrā'](../h/h5830.md)

Greek: [boētheia](../g/g996.md), [boētheō](../g/g997.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5833)

[Study Light](https://www.studylight.org/lexicons/hebrew/5833.html)

[Bible Hub](https://biblehub.com/str/hebrew/5833.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05833)

[Bible Bento](https://biblebento.com/dictionary/H5833.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5833/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5833.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5833)
