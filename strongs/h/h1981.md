# הֲלַךְ

[hălaḵ](https://www.blueletterbible.org/lexicon/h1981)

Definition: walk (3x).

Part of speech: verb

Occurs 3 times in 3 verses

Hebrew: [hûḵ](../h/h1946.md), [hālîḵ](../h/h1978.md), [hălîḵâ](../h/h1979.md), [halak](../h/h1980.md), [yālaḵ](../h/h3212.md), [mahălāḵ](../h/h4108.md), [mahălāḵ](../h/h4109.md), [tahălûḵâ](../h/h8418.md)

Pictoral: staff in palm

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1981)

[Study Light](https://www.studylight.org/lexicons/hebrew/1981.html)

[Bible Hub](https://biblehub.com/str/hebrew/1981.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01981)

[Bible Bento](https://biblebento.com/dictionary/H1981.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1981/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1981.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1981)
