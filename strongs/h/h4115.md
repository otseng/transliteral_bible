# מַהְפֶּכֶת

[mahpeḵeṯ](https://www.blueletterbible.org/lexicon/h4115)

Definition: prison (2x), stocks (2x).

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4115)

[Study Light](https://www.studylight.org/lexicons/hebrew/4115.html)

[Bible Hub](https://biblehub.com/str/hebrew/4115.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04115)

[Bible Bento](https://biblebento.com/dictionary/H4115.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4115/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4115.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4115)
