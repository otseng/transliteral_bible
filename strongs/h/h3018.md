# יְגִיעַ

[yᵊḡîaʿ](https://www.blueletterbible.org/lexicon/h3018)

Definition: labour (15x), work (1x).

Part of speech: masculine noun

Occurs 16 times in 16 verses

Hebrew: [yaga'](../h/h3021.md)

Greek: [ergon](../g/g2041.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3018)

[Study Light](https://www.studylight.org/lexicons/hebrew/3018.html)

[Bible Hub](https://biblehub.com/str/hebrew/3018.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03018)

[Bible Bento](https://biblebento.com/dictionary/H3018.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3018/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3018.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3018)
