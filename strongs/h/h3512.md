# כָּאָה

[kā'â](https://www.blueletterbible.org/lexicon/h3512)

Definition: sad (1x), broken (1x), grieved (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3512)

[Study Light](https://www.studylight.org/lexicons/hebrew/3512.html)

[Bible Hub](https://biblehub.com/str/hebrew/3512.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03512)

[Bible Bento](https://biblebento.com/dictionary/H3512.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3512/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3512.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3512)
