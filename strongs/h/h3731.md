# כַּפְתּוֹר

[Kap̄Tôr](https://www.blueletterbible.org/lexicon/h3731)

Definition: Caphtor (3x).

Part of speech: proper locative noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3731)

[Study Light](https://www.studylight.org/lexicons/hebrew/3731.html)

[Bible Hub](https://biblehub.com/str/hebrew/3731.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03731)

[Bible Bento](https://biblebento.com/dictionary/H3731.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3731/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3731.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3731)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/caphtor.html)