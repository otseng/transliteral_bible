# דִּין

[diyn](https://www.blueletterbible.org/lexicon/h1777)

Definition: judge (18x), plead the cause (2x), contend (1x), execute (1x), plead (1x), strife (1x).

Part of speech: verb

Occurs 24 times in 24 verses

Hebrew: [mᵊḏînâ](../h/h4082.md)

Edenics: deem

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1777)

[Study Light](https://www.studylight.org/lexicons/hebrew/1777.html)

[Bible Hub](https://biblehub.com/str/hebrew/1777.htm)

[Morfix](https://www.morfix.co.il/en/%D7%93%D6%BC%D6%B4%D7%99%D7%9F)

[NET Bible](http://classic.net.bible.org/strong.php?id=01777)

[Bible Bento](https://biblebento.com/dictionary/H1777.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1777/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1777.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1777)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%93%D7%99%D7%9F)
