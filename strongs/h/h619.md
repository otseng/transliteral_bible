# אַסְנָה

['Asnâ](https://www.blueletterbible.org/lexicon/h619)

Definition: Asnah (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h619)

[Study Light](https://www.studylight.org/lexicons/hebrew/619.html)

[Bible Hub](https://biblehub.com/str/hebrew/619.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0619)

[Bible Bento](https://biblebento.com/dictionary/H619.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/619/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/619.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h619)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/asnah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/asnah)