# חַדְלַי

[Ḥaḏlay](https://www.blueletterbible.org/lexicon/h2311)

Definition: Hadlai (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2311)

[Study Light](https://www.studylight.org/lexicons/hebrew/2311.html)

[Bible Hub](https://biblehub.com/str/hebrew/2311.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02311)

[Bible Bento](https://biblebento.com/dictionary/H2311.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2311/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2311.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2311)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/hadlai.html)

[Video Bible](https://www.videobible.com/bible-dictionary/hadlai)