# תֹּהוּ

[tohuw](https://www.blueletterbible.org/lexicon/h8414)

Definition: vain (4x), vanity (4x), confusion (3x), without form (2x), wilderness (2x), nought (2x), nothing (1x), empty place (1x), waste (1x), desolate, formless, empty

Part of speech: masculine noun

Occurs 20 times in 19 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8414)

[Study Light](https://www.studylight.org/lexicons/hebrew/8414.html)

[Bible Hub](https://biblehub.com/str/hebrew/8414.htm)

[Wikipedia](https://en.wikipedia.org/wiki/Tohu_wa-bohu)

[Wikivisually](https://wikivisually.com/wiki/Tohu_wa-bohu)

[The Real Genesis Creation Story](http://www.sunnybrookepub.com/without_form_and_void.html)

[NET Bible](http://classic.net.bible.org/strong.php?id=08414)

[Bible Bento](https://biblebento.com/dictionary/H8414.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8414/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8414.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8414))