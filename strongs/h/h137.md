# אֲדֹנִי־בֶזֶק

['Ăḏōnî-Ḇezeq](https://www.blueletterbible.org/lexicon/h137)

Definition: Adonibezek (3x).

Part of speech: proper masculine noun

Occurs 6 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h137)

[Study Light](https://www.studylight.org/lexicons/hebrew/137.html)

[Bible Hub](https://biblehub.com/str/hebrew/137.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0137)

[Bible Bento](https://biblebento.com/dictionary/H137.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/137/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/137.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h137)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/adonibezek.html)