# כְּתָב

[kᵊṯāḇ](https://www.blueletterbible.org/lexicon/h3791)

Definition: writing (14x), register (2x), scripture (1x).

Part of speech: masculine noun

Occurs 16 times in 16 verses

Hebrew: [kāṯaḇ](../h/h3789.md), [kᵊṯaḇ](../h/h3790.md), [kᵊṯāḇ](../h/h3792.md), [kᵊṯōḇeṯ](../h/h3793.md), [miḵtāḇ](../h/h4385.md)

Greek: [graphē](../g/g1124.md)

Edenics: tablet

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3791)

[Study Light](https://www.studylight.org/lexicons/hebrew/3791.html)

[Bible Hub](https://biblehub.com/str/hebrew/3791.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03791)

[Bible Bento](https://biblebento.com/dictionary/H3791.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3791/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3791.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3791)
