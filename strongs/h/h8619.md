# תָּקוֹעַ

[tāqôaʿ](https://www.blueletterbible.org/lexicon/h8619)

Definition: trumpet (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8619)

[Study Light](https://www.studylight.org/lexicons/hebrew/8619.html)

[Bible Hub](https://biblehub.com/str/hebrew/8619.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08619)

[Bible Bento](https://biblebento.com/dictionary/H8619.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8619/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8619.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8619)
