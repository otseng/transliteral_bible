# בָּצִיר

[bāṣîr](https://www.blueletterbible.org/lexicon/h1210)

Definition: vintage (7x).

Part of speech: masculine noun

Occurs 7 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1210)

[Study Light](https://www.studylight.org/lexicons/hebrew/1210.html)

[Bible Hub](https://biblehub.com/str/hebrew/1210.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01210)

[Bible Bento](https://biblebento.com/dictionary/H1210.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1210/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1210.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1210)
