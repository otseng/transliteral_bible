# חֶדְוָה

[ḥeḏvâ](https://www.blueletterbible.org/lexicon/h2304)

Definition: gladness (1x), joy (1x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

Synonyms: [joy](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Joy)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2304)

[Study Light](https://www.studylight.org/lexicons/hebrew/2304.html)

[Bible Hub](https://biblehub.com/str/hebrew/2304.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02304)

[Bible Bento](https://biblebento.com/dictionary/H2304.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2304/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2304.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2304)
