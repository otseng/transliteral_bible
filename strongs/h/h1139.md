# בְּנֵי־בְרַק

[Bᵊnê-Ḇᵊraq](https://www.blueletterbible.org/lexicon/h1139)

Definition: Beneberak (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1139)

[Study Light](https://www.studylight.org/lexicons/hebrew/1139.html)

[Bible Hub](https://biblehub.com/str/hebrew/1139.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01139)

[Bible Bento](https://biblebento.com/dictionary/H1139.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1139/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1139.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1139)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/beneberak.html)