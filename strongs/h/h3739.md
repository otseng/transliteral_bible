# כָּרָה

[kārâ](https://www.blueletterbible.org/lexicon/h3739)

Definition: buy (2x), prepared (1x), banquet (1x).

Part of speech: verb

Occurs 3 times in 3 verses

Hebrew: [karah](../h/h3738.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3739)

[Study Light](https://www.studylight.org/lexicons/hebrew/3739.html)

[Bible Hub](https://biblehub.com/str/hebrew/3739.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03739)

[Bible Bento](https://biblebento.com/dictionary/H3739.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3739/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3739.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3739)
