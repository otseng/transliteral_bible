# שְׁיָא

[Šᵊyā'](https://www.blueletterbible.org/lexicon/h7864)

Definition: Sheva (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7864)

[Study Light](https://www.studylight.org/lexicons/hebrew/7864.html)

[Bible Hub](https://biblehub.com/str/hebrew/7864.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07864)

[Bible Bento](https://biblebento.com/dictionary/H7864.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7864/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7864.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7864)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/sheva.html)

[Video Bible](https://www.videobible.com/bible-dictionary/sheva)