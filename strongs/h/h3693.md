# כְּסָלוֹן

[Kᵊsālôn](https://www.blueletterbible.org/lexicon/h3693)

Definition: Chesalon (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3693)

[Study Light](https://www.studylight.org/lexicons/hebrew/3693.html)

[Bible Hub](https://biblehub.com/str/hebrew/3693.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03693)

[Bible Bento](https://biblebento.com/dictionary/H3693.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3693/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3693.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3693)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/C/chesalon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/chesalon)