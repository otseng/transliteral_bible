# חָקָה

[ḥāqâ](https://www.blueletterbible.org/lexicon/h2707)

Definition: portrayed (2x), carved work (1x), set a print (1x).

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2707)

[Study Light](https://www.studylight.org/lexicons/hebrew/2707.html)

[Bible Hub](https://biblehub.com/str/hebrew/2707.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02707)

[Bible Bento](https://biblebento.com/dictionary/H2707.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2707/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2707.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2707)
