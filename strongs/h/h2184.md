# זְנוּת

[zᵊnûṯ](https://www.blueletterbible.org/lexicon/h2184)

Definition: whoredom (9x).

Part of speech: feminine noun

Occurs 9 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2184)

[Study Light](https://www.studylight.org/lexicons/hebrew/2184.html)

[Bible Hub](https://biblehub.com/str/hebrew/2184.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02184)

[Bible Bento](https://biblebento.com/dictionary/H2184.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2184/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2184.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2184)
