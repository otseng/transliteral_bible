# פּוּגָה

[pûḡâ](https://www.blueletterbible.org/lexicon/h6314)

Definition: rest (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

Synonyms: [rest](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Rest)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6314)

[Study Light](https://www.studylight.org/lexicons/hebrew/6314.html)

[Bible Hub](https://biblehub.com/str/hebrew/6314.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06314)

[Bible Bento](https://biblebento.com/dictionary/H6314.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6314/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6314.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6314)
