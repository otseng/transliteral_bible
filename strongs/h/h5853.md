# עַטְרוֹת אַדָּר

[ʿAṭrôṯ 'Adār](https://www.blueletterbible.org/lexicon/h5853)

Definition: Atarothadar (1x), Atarothaddar (1x).

Part of speech: proper locative noun

Occurs 4 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5853)

[Study Light](https://www.studylight.org/lexicons/hebrew/5853.html)

[Bible Hub](https://biblehub.com/str/hebrew/5853.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05853)

[Bible Bento](https://biblebento.com/dictionary/H5853.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5853/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5853.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5853)
