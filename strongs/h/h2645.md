# חָפָה

[ḥāp̄â](https://www.blueletterbible.org/lexicon/h2645)

Definition: covered (7x), overlaid (4x), cieled (1x).

Part of speech: verb

Occurs 12 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2645)

[Study Light](https://www.studylight.org/lexicons/hebrew/2645.html)

[Bible Hub](https://biblehub.com/str/hebrew/2645.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02645)

[Bible Bento](https://biblebento.com/dictionary/H2645.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2645/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2645.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2645)
