# פָּלְחָן

[pālḥān](https://www.blueletterbible.org/lexicon/h6402)

Definition: service (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6402)

[Study Light](https://www.studylight.org/lexicons/hebrew/6402.html)

[Bible Hub](https://biblehub.com/str/hebrew/6402.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06402)

[Bible Bento](https://biblebento.com/dictionary/H6402.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6402/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6402.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6402)
