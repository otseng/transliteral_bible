# דָּנָה יַעַן

[Dānâ YaʿAn](https://www.blueletterbible.org/lexicon/h1842)

Definition: Danjaan (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1842)

[Study Light](https://www.studylight.org/lexicons/hebrew/1842.html)

[Bible Hub](https://biblehub.com/str/hebrew/1842.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01842)

[Bible Bento](https://biblebento.com/dictionary/H1842.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1842/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1842.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1842)
