# נֵכָר

[nēḵār](https://www.blueletterbible.org/lexicon/h5236)

Definition: strange (17x), stranger (with H1121) (10x), stranger (7x), alien (1x).

Part of speech: masculine noun

Occurs 36 times in 35 verses

Hebrew: [nāḵar](../h/h5234.md)

Greek: [allogenēs](../g/g241.md), [allotrios](../g/g245.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5236)

[Study Light](https://www.studylight.org/lexicons/hebrew/5236.html)

[Bible Hub](https://biblehub.com/str/hebrew/5236.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05236)

[Bible Bento](https://biblebento.com/dictionary/H5236.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5236/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5236.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5236)
