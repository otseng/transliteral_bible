# חָסַד

[ḥāsaḏ](https://www.blueletterbible.org/lexicon/h2616)

Definition: show thyself merciful (2x), put to shame (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2616)

[Study Light](https://www.studylight.org/lexicons/hebrew/2616.html)

[Bible Hub](https://biblehub.com/str/hebrew/2616.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02616)

[Bible Bento](https://biblebento.com/dictionary/H2616.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2616/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2616.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2616)
