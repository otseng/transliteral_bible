# עַוָּתָה

[ʿaûāṯâ](https://www.blueletterbible.org/lexicon/h5792)

Definition: my wrong (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5792)

[Study Light](https://www.studylight.org/lexicons/hebrew/5792.html)

[Bible Hub](https://biblehub.com/str/hebrew/5792.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05792)

[Bible Bento](https://biblebento.com/dictionary/H5792.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5792/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5792.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5792)
