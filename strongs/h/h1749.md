# דּוֹנָג

[dônāḡ](https://www.blueletterbible.org/lexicon/h1749)

Definition: wax (4x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1749)

[Study Light](https://www.studylight.org/lexicons/hebrew/1749.html)

[Bible Hub](https://biblehub.com/str/hebrew/1749.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01749)

[Bible Bento](https://biblebento.com/dictionary/H1749.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1749/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1749.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1749)
