# כְּנָעָה

[kᵊnāʿâ](https://www.blueletterbible.org/lexicon/h3666)

Definition: wares (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3666)

[Study Light](https://www.studylight.org/lexicons/hebrew/3666.html)

[Bible Hub](https://biblehub.com/str/hebrew/3666.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03666)

[Bible Bento](https://biblebento.com/dictionary/H3666.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3666/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3666.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3666)
