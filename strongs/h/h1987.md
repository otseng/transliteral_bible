# הֵלֶם

[Hēlem](https://www.blueletterbible.org/lexicon/h1987)

Definition: Helem (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1987)

[Study Light](https://www.studylight.org/lexicons/hebrew/1987.html)

[Bible Hub](https://biblehub.com/str/hebrew/1987.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01987)

[Bible Bento](https://biblebento.com/dictionary/H1987.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1987/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1987.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1987)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/H/helem.html)