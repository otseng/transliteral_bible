# דֶּגֶל

[deḡel](https://www.blueletterbible.org/lexicon/h1714)

Definition: standard (13x), banner (1x).

Part of speech: masculine noun

Occurs 14 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1714)

[Study Light](https://www.studylight.org/lexicons/hebrew/1714.html)

[Bible Hub](https://biblehub.com/str/hebrew/1714.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01714)

[Bible Bento](https://biblebento.com/dictionary/H1714.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1714/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1714.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1714)
