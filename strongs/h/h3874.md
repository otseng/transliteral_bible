# לוּט

[lûṭ](https://www.blueletterbible.org/lexicon/h3874)

Definition: wrapped (2x), cast (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3874)

[Study Light](https://www.studylight.org/lexicons/hebrew/3874.html)

[Bible Hub](https://biblehub.com/str/hebrew/3874.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03874)

[Bible Bento](https://biblebento.com/dictionary/H3874.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3874/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3874.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3874)
