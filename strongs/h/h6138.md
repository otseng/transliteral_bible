# עֶקְרוֹן

[ʿEqrôn](https://www.blueletterbible.org/lexicon/h6138)

Definition: Ekron (22x).

Part of speech: proper locative noun

Occurs 22 times in 20 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6138)

[Study Light](https://www.studylight.org/lexicons/hebrew/6138.html)

[Bible Hub](https://biblehub.com/str/hebrew/6138.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06138)

[Bible Bento](https://biblebento.com/dictionary/H6138.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6138/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6138.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6138)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/ekron.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ekron)