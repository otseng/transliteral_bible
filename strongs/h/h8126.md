# שֻׁמָתִי

[Šumāṯî](https://www.blueletterbible.org/lexicon/h8126)

Definition: Shumathites (1x).

Part of speech: gentilic adjective, proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8126)

[Study Light](https://www.studylight.org/lexicons/hebrew/8126.html)

[Bible Hub](https://biblehub.com/str/hebrew/8126.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08126)

[Bible Bento](https://biblebento.com/dictionary/H8126.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8126/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8126.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8126)

[Video Bible](https://www.videobible.com/bible-dictionary/shumathites)