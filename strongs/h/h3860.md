# לָהֵן

[lāhēn](https://www.blueletterbible.org/lexicon/h3860)

Definition: for them (2x).

Part of speech: conjunction

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3860)

[Study Light](https://www.studylight.org/lexicons/hebrew/3860.html)

[Bible Hub](https://biblehub.com/str/hebrew/3860.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03860)

[Bible Bento](https://biblebento.com/dictionary/H3860.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3860/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3860.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3860)
