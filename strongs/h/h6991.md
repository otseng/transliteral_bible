# קָטַל

[qāṭal](https://www.blueletterbible.org/lexicon/h6991)

Definition: slay (2x), kill (1x).

Part of speech: verb

Occurs 3 times in 3 verses

Synonyms: [kill](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Kill)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6991)

[Study Light](https://www.studylight.org/lexicons/hebrew/6991.html)

[Bible Hub](https://biblehub.com/str/hebrew/6991.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06991)

[Bible Bento](https://biblebento.com/dictionary/H6991.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6991/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6991.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6991)
