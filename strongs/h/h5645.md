# עָב

[`ab](https://www.blueletterbible.org/lexicon/h5645)

Definition: cloud (29x), clay (1x), thick (1x), thickets (1x).

Part of speech: masculine noun

Occurs 32 times in 32 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5645)

[Study Light](https://www.studylight.org/lexicons/hebrew/5645.html)

[Bible Hub](https://biblehub.com/str/hebrew/5645.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A2%D6%B8%D7%91)

[NET Bible](http://classic.net.bible.org/strong.php?id=05645)

[Bible Bento](https://biblebento.com/dictionary/H5645.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5645/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5645.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5645)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A2%D7%91)
