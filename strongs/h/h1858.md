# דַּר

[dar](https://www.blueletterbible.org/lexicon/h1858)

Definition: white (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1858)

[Study Light](https://www.studylight.org/lexicons/hebrew/1858.html)

[Bible Hub](https://biblehub.com/str/hebrew/1858.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01858)

[Bible Bento](https://biblebento.com/dictionary/H1858.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1858/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1858.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1858)
