# דַּלְפוֹן

[Dalp̄Ôn](https://www.blueletterbible.org/lexicon/h1813)

Definition: Dalphon (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1813)

[Study Light](https://www.studylight.org/lexicons/hebrew/1813.html)

[Bible Hub](https://biblehub.com/str/hebrew/1813.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01813)

[Bible Bento](https://biblebento.com/dictionary/H1813.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1813/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1813.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1813)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/dalphon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/dalphon)