# עֵשֶׁק

[ʿĒšeq](https://www.blueletterbible.org/lexicon/h6232)

Definition: Eshek (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6232)

[Study Light](https://www.studylight.org/lexicons/hebrew/6232.html)

[Bible Hub](https://biblehub.com/str/hebrew/6232.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06232)

[Bible Bento](https://biblebento.com/dictionary/H6232.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6232/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6232.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6232)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eshek.html)