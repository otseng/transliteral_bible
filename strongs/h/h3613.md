# כָּלֵב אֶפְרָתָה

[Kālēḇ 'Ep̄Rāṯâ](https://www.blueletterbible.org/lexicon/h3613)

Definition: Calebephratah (1x).

Part of speech: proper locative noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3613)

[Study Light](https://www.studylight.org/lexicons/hebrew/3613.html)

[Bible Hub](https://biblehub.com/str/hebrew/3613.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03613)

[Bible Bento](https://biblebento.com/dictionary/H3613.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3613/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3613.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3613)
