# שָׁגַל

[šāḡal](https://www.blueletterbible.org/lexicon/h7693)

Definition: be lien (1x), variant (3x).

Part of speech: verb

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7693)

[Study Light](https://www.studylight.org/lexicons/hebrew/7693.html)

[Bible Hub](https://biblehub.com/str/hebrew/7693.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07693)

[Bible Bento](https://biblebento.com/dictionary/H7693.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7693/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7693.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7693)
