# אָבָק

['āḇāq](https://www.blueletterbible.org/lexicon/h80)

Definition: dust (5x), powder (1x).

Part of speech: masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0080)

[Study Light](https://www.studylight.org/lexicons/hebrew/80.html)

[Bible Hub](https://biblehub.com/str/hebrew/80.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=080)

[Bible Bento](https://biblebento.com/dictionary/H80.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/80/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/80.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h80)
