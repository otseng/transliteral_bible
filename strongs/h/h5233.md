# נֶכֶס

[neḵes](https://www.blueletterbible.org/lexicon/h5233)

Definition: wealth (4x), riches (1x).

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5233)

[Study Light](https://www.studylight.org/lexicons/hebrew/5233.html)

[Bible Hub](https://biblehub.com/str/hebrew/5233.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05233)

[Bible Bento](https://biblebento.com/dictionary/H5233.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5233/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5233.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5233)
