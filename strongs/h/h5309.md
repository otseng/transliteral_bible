# נֶפֶל

[nep̄el](https://www.blueletterbible.org/lexicon/h5309)

Definition: untimely birth (3x), miscarriage

Part of speech: masculine noun

Occurs 3 times in 3 verses

Hebrew: [mapāl](../h/h4651.md), [mapālâ](../h/h4654.md), [mapeleṯ](../h/h4658.md), [naphal](../h/h5307.md), [nᵊp̄al](../h/h5308.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5309)

[Study Light](https://www.studylight.org/lexicons/hebrew/5309.html)

[Bible Hub](https://biblehub.com/str/hebrew/5309.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05309)

[Bible Bento](https://biblebento.com/dictionary/H5309.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5309/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5309.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5309)
