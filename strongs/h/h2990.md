# יַבָּל

[yabāl](https://www.blueletterbible.org/lexicon/h2990)

Definition: wen (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2990)

[Study Light](https://www.studylight.org/lexicons/hebrew/2990.html)

[Bible Hub](https://biblehub.com/str/hebrew/2990.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02990)

[Bible Bento](https://biblebento.com/dictionary/H2990.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2990/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2990.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2990)
