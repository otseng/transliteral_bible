# לָמַד

[lamad](https://www.blueletterbible.org/lexicon/h3925)

Definition: teach (56x), learn (22x), instruct (3x), diligently (1x), expert (1x), skilful (1x), teachers (1x), unaccustomed (with H3808) (1x).

Part of speech: verb

Occurs 86 times in 80 verses

Hebrew: [limmûḏ](../h/h3928.md)

Greek: [didaskō](../g/g1321.md), [didaskalia](../g/g1319.md), [manthanō](../g/g3129.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3925)

[Study Light](https://www.studylight.org/lexicons/hebrew/3925.html)

[Bible Hub](https://biblehub.com/str/hebrew/3925.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03925)

[Bible Bento](https://biblebento.com/dictionary/H3925.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3925/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3925.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3925)
