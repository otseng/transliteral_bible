# עֹלָה

[ʿōlâ](https://www.blueletterbible.org/lexicon/h5930)

Definition: burnt offering (264x), burnt sacrifice (21x), ascent (1x), go up (1x), stairway, steps, the whole burnt-offering (beast or fowl) is entirely consumed and goes up in the flame of the altar to God expressing the ascent of the soul in worship.

Part of speech: feminine noun

Occurs 289 times in 262 verses

Hebrew: [ʿālâ](../h/h5927.md)

Greek: [thysia](../g/g2378.md), [holokautōma](../g/g3646.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5930)

[Study Light](https://www.studylight.org/lexicons/hebrew/5930.html)

[Bible Hub](https://biblehub.com/str/hebrew/5930.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05930)

[Bible Bento](https://biblebento.com/dictionary/H5930.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5930/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5930.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5930)
