# יוֹמָם

[yômām](https://www.blueletterbible.org/lexicon/h3119)

Definition: day (41x), daytime (7x), daily (2x), time (1x).

Part of speech: adverb, substantive

Occurs 51 times in 50 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3119)

[Study Light](https://www.studylight.org/lexicons/hebrew/3119.html)

[Bible Hub](https://biblehub.com/str/hebrew/3119.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03119)

[Bible Bento](https://biblebento.com/dictionary/H3119.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3119/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3119.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3119)
