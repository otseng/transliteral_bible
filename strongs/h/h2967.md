# טַרְפְּלָיֵא

[Ṭarpᵊlāyē'](https://www.blueletterbible.org/lexicon/h2967)

Definition: Tarpelites (1x).

Part of speech: masculine patrial noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2967)

[Study Light](https://www.studylight.org/lexicons/hebrew/2967.html)

[Bible Hub](https://biblehub.com/str/hebrew/2967.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02967)

[Bible Bento](https://biblebento.com/dictionary/H2967.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2967/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2967.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2967)
