# סְגוֹר

[sᵊḡôr](https://www.blueletterbible.org/lexicon/h5458)

Definition: gold (1x), caul (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5458)

[Study Light](https://www.studylight.org/lexicons/hebrew/5458.html)

[Bible Hub](https://biblehub.com/str/hebrew/5458.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05458)

[Bible Bento](https://biblebento.com/dictionary/H5458.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5458/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5458.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5458)
