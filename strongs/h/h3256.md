# יָסַר

[yacar](https://www.blueletterbible.org/lexicon/h3256)

Definition: chastise (21x), instruct (8x), correct (7x), taught (2x), bound (1x), punish (1x), reformed (1x), reproveth (1x), sore (1x)

Part of speech: verb

Occurs 43 times in 38 verses

Greek: [paideuō](../g/g3811.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3256)

[Study Light](https://www.studylight.org/lexicons/hebrew/3256.html)

[Bible Hub](https://biblehub.com/str/hebrew/3256.htm)

[Morfix](https://www.morfix.co.il/en/%D7%99%D6%B8%D7%A1%D6%B7%D7%A8)

[NET Bible](http://classic.net.bible.org/strong.php?id=03256)

[Bible Bento](https://biblebento.com/dictionary/H3256.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3256/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3256.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3256)
