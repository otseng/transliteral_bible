# קֹבָה

[qōḇâ](https://www.blueletterbible.org/lexicon/h6897)

Definition: belly (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6897)

[Study Light](https://www.studylight.org/lexicons/hebrew/6897.html)

[Bible Hub](https://biblehub.com/str/hebrew/6897.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06897)

[Bible Bento](https://biblebento.com/dictionary/H6897.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6897/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6897.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6897)
