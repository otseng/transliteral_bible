# שָׁוֵה

[šāvê](https://www.blueletterbible.org/lexicon/h7740)

Definition: Shaveh (1x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7740)

[Study Light](https://www.studylight.org/lexicons/hebrew/7740.html)

[Bible Hub](https://biblehub.com/str/hebrew/7740.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07740)

[Bible Bento](https://biblebento.com/dictionary/H7740.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7740/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7740.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7740)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shaveh.html)

[Video Bible](https://www.videobible.com/bible-dictionary/shaveh)