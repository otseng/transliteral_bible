# מָתָק

[māṯāq](https://www.blueletterbible.org/lexicon/h4988)

Definition: feed sweetly (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4988)

[Study Light](https://www.studylight.org/lexicons/hebrew/4988.html)

[Bible Hub](https://biblehub.com/str/hebrew/4988.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04988)

[Bible Bento](https://biblebento.com/dictionary/H4988.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4988/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4988.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4988)
