# בַּת

[baṯ](https://www.blueletterbible.org/lexicon/h1325)

Definition: bath (2x).

Part of speech: masculine noun

Occurs 2 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1325)

[Study Light](https://www.studylight.org/lexicons/hebrew/1325.html)

[Bible Hub](https://biblehub.com/str/hebrew/1325.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01325)

[Bible Bento](https://biblebento.com/dictionary/H1325.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1325/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1325.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1325)
