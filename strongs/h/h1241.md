# בָּקָר

[bāqār](https://www.blueletterbible.org/lexicon/h1241)

Definition: ox (78x), herd (44x), beeves (7x), young (18x), young (with H1121) (17x), bullock (6x), bullock (with H1121) (2x), calf (with H1121) (2x), heifer (2x), kine (2x), bulls (1x), cattle (1x), cow's (1x), great (1x).

Part of speech: masculine noun

Occurs 183 times in 172 verses

Hebrew: [bāqar](../h/h1239.md)

Greek: [bous](../g/g1016.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1241)

[Study Light](https://www.studylight.org/lexicons/hebrew/1241.html)

[Bible Hub](https://biblehub.com/str/hebrew/1241.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01241)

[Bible Bento](https://biblebento.com/dictionary/H1241.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1241/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1241.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1241)
