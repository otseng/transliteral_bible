# מַרְבִּית

[marbîṯ](https://www.blueletterbible.org/lexicon/h4768)

Definition: increase (2x), greatest part (1x), greatest (1x), multitude (1x).

Part of speech: feminine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4768)

[Study Light](https://www.studylight.org/lexicons/hebrew/4768.html)

[Bible Hub](https://biblehub.com/str/hebrew/4768.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04768)

[Bible Bento](https://biblebento.com/dictionary/H4768.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4768/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4768.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4768)
