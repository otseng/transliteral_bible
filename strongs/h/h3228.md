# יָמִינִי

[yāmînî](https://www.blueletterbible.org/lexicon/h3228)

Definition: Jaminites (1x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3228)

[Study Light](https://www.studylight.org/lexicons/hebrew/3228.html)

[Bible Hub](https://biblehub.com/str/hebrew/3228.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03228)

[Bible Bento](https://biblebento.com/dictionary/H3228.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3228/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3228.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3228)
