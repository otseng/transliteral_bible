# פָּשַׁט

[pāšaṭ](https://www.blueletterbible.org/lexicon/h6584)

Definition: strip (13x), put off (6x), flay (4x), invaded (4x), spoil (3x), strip off (2x), fell (2x), spread abroad (1x), forward (1x), invasion (1x), pull off (1x), made a road (1x), rushed (1x), set (1x), spread (1x), ran upon (1x).

Part of speech: verb

Occurs 43 times in 42 verses

Greek: [ekdyō](../g/g1562.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6584)

[Study Light](https://www.studylight.org/lexicons/hebrew/6584.html)

[Bible Hub](https://biblehub.com/str/hebrew/6584.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06584)

[Bible Bento](https://biblebento.com/dictionary/H6584.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6584/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6584.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6584)
