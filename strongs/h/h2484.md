# חֶלְיָה

[ḥelyâ](https://www.blueletterbible.org/lexicon/h2484)

Definition: jewel (1x).

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2484)

[Study Light](https://www.studylight.org/lexicons/hebrew/2484.html)

[Bible Hub](https://biblehub.com/str/hebrew/2484.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02484)

[Bible Bento](https://biblebento.com/dictionary/H2484.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2484/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2484.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2484)
