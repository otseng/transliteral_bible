# אָיֹם

['āyōm](https://www.blueletterbible.org/lexicon/h366)

Definition: terrible (3x).

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h366)

[Study Light](https://www.studylight.org/lexicons/hebrew/366.html)

[Bible Hub](https://biblehub.com/str/hebrew/366.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0366)

[Bible Bento](https://biblebento.com/dictionary/H366.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/366/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/366.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h366)
