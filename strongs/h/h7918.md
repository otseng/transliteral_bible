# שָׁכַך

[šāḵaḵ](https://www.blueletterbible.org/lexicon/h7918)

Definition: pacified (1x), appeased (1x), set (1x), asswage (1x), cease (1x).

Part of speech: verb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7918)

[Study Light](https://www.studylight.org/lexicons/hebrew/7918.html)

[Bible Hub](https://biblehub.com/str/hebrew/7918.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07918)

[Bible Bento](https://biblebento.com/dictionary/H7918.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7918/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7918.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7918)
