# דִּין

[dîn](https://www.blueletterbible.org/lexicon/h1780)

Definition: judgment (5x).

Part of speech: masculine noun

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1780)

[Study Light](https://www.studylight.org/lexicons/hebrew/1780.html)

[Bible Hub](https://biblehub.com/str/hebrew/1780.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01780)

[Bible Bento](https://biblebento.com/dictionary/H1780.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1780/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1780.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1780)
