# מִזְרָק

[mizrāq](https://www.blueletterbible.org/lexicon/h4219)

Definition: basons (11x), bowl (21x).

Part of speech: masculine noun

Occurs 32 times in 32 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4219)

[Study Light](https://www.studylight.org/lexicons/hebrew/4219.html)

[Bible Hub](https://biblehub.com/str/hebrew/4219.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04219)

[Bible Bento](https://biblebento.com/dictionary/H4219.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4219/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4219.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4219)
