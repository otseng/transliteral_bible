# שָׁלָל

[šālāl](https://www.blueletterbible.org/lexicon/h7998)

Definition: spoil (63x), prey (10x).

Part of speech: masculine noun

Occurs 74 times in 64 verses

Hebrew: [šālal](../h/h7997.md)

Greek: [harpagē](../g/g724.md), [skylon](../g/g4661.md)


## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7998)

[Study Light](https://www.studylight.org/lexicons/hebrew/7998.html)

[Bible Hub](https://biblehub.com/str/hebrew/7998.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07998)

[Bible Bento](https://biblebento.com/dictionary/H7998.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7998/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7998.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7998)
