# מִשְׁפָּט

[mishpat](https://www.blueletterbible.org/lexicon/h4941)

Definition: judgment (296x), manner (38x), right (18x), cause (12x), ordinance (11x), lawful (7x), order (5x), worthy (3x), fashion (3x), custom (2x), discretion (2x), law (2x), measure (2x), sentence (2x), verdict, trial, ruling, justice, decide

Part of speech: masculine noun

Occurs 421 times in 406 verses

Hebrew: [šᵊp̄ôṭ](../h/h8196.md), [shaphat](../h/h8199.md), [šᵊp̄aṭ](../h/h8200.md), [šep̄eṭ](../h/h8201.md)

Greek: [krima](../g/g2917.md), [kritērion](../g/g2922.md), [krisis](../g/g2920.md), [entolē](../g/g1785.md), [nomos](../g/g3551.md), [dikē](../g/g1349.md), [dikaios](../g/g1342.md)

Synonyms: [law](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Law)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4941)

[Study Light](https://www.studylight.org/lexicons/hebrew/4941.html)

[Bible Hub](https://biblehub.com/str/hebrew/4941.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%9E%D7%A9%D7%A4%D7%98)

[NET Bible](http://classic.net.bible.org/strong.php?id=04941)

[Bible Bento](https://biblebento.com/dictionary/H4941.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4941/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4941.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4941)

[Light of the World](https://kaylened.wordpress.com/2020/11/06/prescribed-the-ancient-hebrew-meaning/)

[Eden's Bridge](https://edensbridge.org/2012/01/11/on-justice-and-righteousness-mishpat-tsadaq-strongs-4941-6663/)
