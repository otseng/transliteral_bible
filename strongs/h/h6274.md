# עָתְנִיאֵל

[ʿĀṯnî'Ēl](https://www.blueletterbible.org/lexicon/h6274)

Definition: Othniel (7x).

Part of speech: proper masculine noun

Occurs 7 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6274)

[Study Light](https://www.studylight.org/lexicons/hebrew/6274.html)

[Bible Hub](https://biblehub.com/str/hebrew/6274.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06274)

[Bible Bento](https://biblebento.com/dictionary/H6274.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6274/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6274.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6274)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/O/othniel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/othniel)