# חָבָה

[ḥāḇâ](https://www.blueletterbible.org/lexicon/h2247)

Definition: hide... (5x).

Part of speech: verb

Occurs 5 times in 5 verses

Hebrew: [chaba'](../h/h2244.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2247)

[Study Light](https://www.studylight.org/lexicons/hebrew/2247.html)

[Bible Hub](https://biblehub.com/str/hebrew/2247.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02247)

[Bible Bento](https://biblebento.com/dictionary/H2247.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2247/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2247.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2247)
