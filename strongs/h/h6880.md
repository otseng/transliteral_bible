# צִרְעָה

[ṣirʿâ](https://www.blueletterbible.org/lexicon/h6880)

Definition: hornet (3x).

Part of speech: collective feminine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6880)

[Study Light](https://www.studylight.org/lexicons/hebrew/6880.html)

[Bible Hub](https://biblehub.com/str/hebrew/6880.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06880)

[Bible Bento](https://biblebento.com/dictionary/H6880.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6880/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6880.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6880)
