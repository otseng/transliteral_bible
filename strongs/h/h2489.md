# חֵלְכָה

[cheleka'](https://www.blueletterbible.org/lexicon/h2489)

Definition: poor (2x), variant (2x).

Part of speech: adjective

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2489)

[Study Light](https://www.studylight.org/lexicons/hebrew/2489.html)

[Bible Hub](https://biblehub.com/str/hebrew/2489.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02489)

[Bible Bento](https://biblebento.com/dictionary/H2489.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2489/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2489.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2489)
