# אָחוּ

['āḥû](https://www.blueletterbible.org/lexicon/h260)

Definition: meadow (2x), flag (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h260)

[Study Light](https://www.studylight.org/lexicons/hebrew/260.html)

[Bible Hub](https://biblehub.com/str/hebrew/260.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0260)

[Bible Bento](https://biblebento.com/dictionary/H260.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/260/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/260.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h260)
