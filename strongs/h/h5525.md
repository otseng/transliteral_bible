# סֻכִּי

[Sukî](https://www.blueletterbible.org/lexicon/h5525)

Definition: Sukkiims (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5525)

[Study Light](https://www.studylight.org/lexicons/hebrew/5525.html)

[Bible Hub](https://biblehub.com/str/hebrew/5525.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05525)

[Bible Bento](https://biblebento.com/dictionary/H5525.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5525/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5525.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5525)
