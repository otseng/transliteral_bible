# רַב

[raḇ](https://www.blueletterbible.org/lexicon/h7228)

Definition: archer (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7228)

[Study Light](https://www.studylight.org/lexicons/hebrew/7228.html)

[Bible Hub](https://biblehub.com/str/hebrew/7228.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07228)

[Bible Bento](https://biblebento.com/dictionary/H7228.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7228/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7228.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7228)
