# שֶׁל

[šel](https://www.blueletterbible.org/lexicon/h7945)

Definition: though (1x), for whose cause (1x), for my sake (1x).

Part of speech: particle

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7945)

[Study Light](https://www.studylight.org/lexicons/hebrew/7945.html)

[Bible Hub](https://biblehub.com/str/hebrew/7945.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07945)

[Bible Bento](https://biblebento.com/dictionary/H7945.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7945/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7945.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7945)
