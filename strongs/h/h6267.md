# עַתִּיק

[ʿatîq](https://www.blueletterbible.org/lexicon/h6267)

Definition: ancient (1x), drawn (1x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6267)

[Study Light](https://www.studylight.org/lexicons/hebrew/6267.html)

[Bible Hub](https://biblehub.com/str/hebrew/6267.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06267)

[Bible Bento](https://biblebento.com/dictionary/H6267.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6267/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6267.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6267)
