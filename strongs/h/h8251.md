# שִׁקּוּץ

[šiqqûṣ](https://www.blueletterbible.org/lexicon/h8251)

Definition: abomination (20x), detestable things (5x), detestable (1x), abominable filth (1x), abominable idols (1x), exceptionally loathsome, hateful, sinful, wicked, or vile

Part of speech: masculine noun

Occurs 28 times in 26 verses

Hebrew: [šāqaṣ](../h/h8262.md), [šeqeṣ](../h/h8263.md), [tôʿēḇâ](../h/h8441.md)

Greek: [bdelygma](../g/g946.md)

Synonyms: [abomination](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Abomination)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8251)

[Study Light](https://www.studylight.org/lexicons/hebrew/8251.html)

[Bible Hub](https://biblehub.com/str/hebrew/8251.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08251)

[Bible Bento](https://biblebento.com/dictionary/H8251.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8251/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8251.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8251)

[Wikipedia](https://en.wikipedia.org/wiki/Abomination_%28Bible%29)

[Jewish Virtual Library](https://www.jewishvirtuallibrary.org/abomination-jewish-virtual-library)

[Jewish Encyclopedia](https://www.jewishencyclopedia.com/articles/352-abomination)
