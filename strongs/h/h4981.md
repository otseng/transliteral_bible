# מִתְנִי

[Miṯnî](https://www.blueletterbible.org/lexicon/h4981)

Definition: Mithnite (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4981)

[Study Light](https://www.studylight.org/lexicons/hebrew/4981.html)

[Bible Hub](https://biblehub.com/str/hebrew/4981.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04981)

[Bible Bento](https://biblebento.com/dictionary/H4981.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4981/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4981.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4981)
