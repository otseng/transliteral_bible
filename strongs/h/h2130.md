# זִיפִי

[Zîp̄î](https://www.blueletterbible.org/lexicon/h2130)

Definition: Ziphites (2x), Ziphims (1x).

Part of speech: masculine patrial noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2130)

[Study Light](https://www.studylight.org/lexicons/hebrew/2130.html)

[Bible Hub](https://biblehub.com/str/hebrew/2130.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02130)

[Bible Bento](https://biblebento.com/dictionary/H2130.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2130/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2130.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2130)
