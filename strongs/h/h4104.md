# מְהוּמָן

[Mᵊhûmān](https://www.blueletterbible.org/lexicon/h4104)

Definition: Mehuman (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4104)

[Study Light](https://www.studylight.org/lexicons/hebrew/4104.html)

[Bible Hub](https://biblehub.com/str/hebrew/4104.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04104)

[Bible Bento](https://biblebento.com/dictionary/H4104.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4104/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4104.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4104)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/mehuman.html)

[Video Bible](https://www.videobible.com/bible-dictionary/mehuman)