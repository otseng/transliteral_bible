# אָרָה

['ārâ](https://www.blueletterbible.org/lexicon/h717)

Definition: pluck (1x), gather (1x).

Part of speech: verb

Occurs 2 times in 2 verses

Hebrew: ['urvâ](../h/h723.md), ['ārôn](../h/h727.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h717)

[Study Light](https://www.studylight.org/lexicons/hebrew/717.html)

[Bible Hub](https://biblehub.com/str/hebrew/717.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0717)

[Bible Bento](https://biblebento.com/dictionary/H717.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/717/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/717.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h717)
