# בְּכוֹרַת

[Bᵊḵôraṯ](https://www.blueletterbible.org/lexicon/h1064)

Definition: Bechorath (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1064)

[Study Light](https://www.studylight.org/lexicons/hebrew/1064.html)

[Bible Hub](https://biblehub.com/str/hebrew/1064.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01064)

[Bible Bento](https://biblebento.com/dictionary/H1064.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1064/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1064.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1064)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bechorath.html)