# שֵׁרָה

[šērâ](https://www.blueletterbible.org/lexicon/h8285)

Definition: bracelet (1x), wrist-band 

Part of speech: feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8285)

[Study Light](https://www.studylight.org/lexicons/hebrew/8285.html)

[Bible Hub](https://biblehub.com/str/hebrew/8285.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08285)

[Bible Bento](https://biblebento.com/dictionary/H8285.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8285/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8285.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8285)
