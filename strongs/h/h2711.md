# חֵקֶק

[ḥēqeq](https://www.blueletterbible.org/lexicon/h2711)

Definition: thoughts (1x), decrees (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2711)

[Study Light](https://www.studylight.org/lexicons/hebrew/2711.html)

[Bible Hub](https://biblehub.com/str/hebrew/2711.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02711)

[Bible Bento](https://biblebento.com/dictionary/H2711.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2711/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2711.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2711)
