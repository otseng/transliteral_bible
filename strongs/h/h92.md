# אֲגֻדָּה

['ăḡudâ](https://www.blueletterbible.org/lexicon/h92)

Definition: troop (2x), bunch (1x), burdens (1x).

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0092)

[Study Light](https://www.studylight.org/lexicons/hebrew/92.html)

[Bible Hub](https://biblehub.com/str/hebrew/92.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=092)

[Bible Bento](https://biblebento.com/dictionary/H92.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/92/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/92.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h92)
