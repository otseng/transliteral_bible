# בַּעְשָׁא

[BaʿŠā'](https://www.blueletterbible.org/lexicon/h1201)

Definition: Baasha (28x).

Part of speech: proper masculine noun

Occurs 28 times in 26 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1201)

[Study Light](https://www.studylight.org/lexicons/hebrew/1201.html)

[Bible Hub](https://biblehub.com/str/hebrew/1201.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01201)

[Bible Bento](https://biblebento.com/dictionary/H1201.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1201/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1201.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1201)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/baasha.html)

[Video Bible](https://www.videobible.com/bible-dictionary/baasha)