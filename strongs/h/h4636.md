# מַעֲרֹם

[maʿărōm](https://www.blueletterbible.org/lexicon/h4636)

Definition: naked (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4636)

[Study Light](https://www.studylight.org/lexicons/hebrew/4636.html)

[Bible Hub](https://biblehub.com/str/hebrew/4636.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04636)

[Bible Bento](https://biblebento.com/dictionary/H4636.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4636/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4636.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4636)
