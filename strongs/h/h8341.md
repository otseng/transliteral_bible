# שִׁשָּׁה

[šiššâ](https://www.blueletterbible.org/lexicon/h8341)

Definition: sixth part (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8341)

[Study Light](https://www.studylight.org/lexicons/hebrew/8341.html)

[Bible Hub](https://biblehub.com/str/hebrew/8341.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08341)

[Bible Bento](https://biblebento.com/dictionary/H8341.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8341/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8341.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8341)
