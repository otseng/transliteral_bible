# אַרְאֵֵלִי

['ar'ēlî](https://www.blueletterbible.org/lexicon/h692)

Definition: Areli (2x), Arelite (1x).

Part of speech: proper masculine noun

Occurs 3 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h692)

[Study Light](https://www.studylight.org/lexicons/hebrew/692.html)

[Bible Hub](https://biblehub.com/str/hebrew/692.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0692)

[Bible Bento](https://biblebento.com/dictionary/H692.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/692/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/692.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h692)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/areli.html)

[Video Bible](https://www.videobible.com/bible-dictionary/areli)