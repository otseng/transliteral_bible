# נִשֵּׂאת

[niśśē'ṯ](https://www.blueletterbible.org/lexicon/h5379)

Definition: gift (1x).

Part of speech: passive participle/feminine infinitive

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5379)

[Study Light](https://www.studylight.org/lexicons/hebrew/5379.html)

[Bible Hub](https://biblehub.com/str/hebrew/5379.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05379)

[Bible Bento](https://biblebento.com/dictionary/H5379.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5379/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5379.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5379)
