# גֶּדֶר

[geḏer](https://www.blueletterbible.org/lexicon/h1444)

Definition: wall (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1444)

[Study Light](https://www.studylight.org/lexicons/hebrew/1444.html)

[Bible Hub](https://biblehub.com/str/hebrew/1444.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01444)

[Bible Bento](https://biblebento.com/dictionary/H1444.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1444/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1444.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1444)
