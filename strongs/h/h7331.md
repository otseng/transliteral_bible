# רְזוֹן

[Rᵊzôn](https://www.blueletterbible.org/lexicon/h7331)

Definition: Rezon (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7331)

[Study Light](https://www.studylight.org/lexicons/hebrew/7331.html)

[Bible Hub](https://biblehub.com/str/hebrew/7331.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07331)

[Bible Bento](https://biblebento.com/dictionary/H7331.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7331/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7331.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7331)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/R/rezon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/rezon)