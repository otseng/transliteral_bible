# מַשָּׁאָה

[maššā'â](https://www.blueletterbible.org/lexicon/h4859)

Definition: thing (1x), debt (1x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

Greek: [opheilēma](../g/g3783.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4859)

[Study Light](https://www.studylight.org/lexicons/hebrew/4859.html)

[Bible Hub](https://biblehub.com/str/hebrew/4859.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04859)

[Bible Bento](https://biblebento.com/dictionary/H4859.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4859/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4859.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4859)
