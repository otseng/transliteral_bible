# עֲנָשׁ

[ʿănāš](https://www.blueletterbible.org/lexicon/h6065)

Definition: confiscation (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6065)

[Study Light](https://www.studylight.org/lexicons/hebrew/6065.html)

[Bible Hub](https://biblehub.com/str/hebrew/6065.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06065)

[Bible Bento](https://biblebento.com/dictionary/H6065.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6065/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6065.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6065)
