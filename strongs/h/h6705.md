# צָחַח

[ṣāḥaḥ](https://www.blueletterbible.org/lexicon/h6705)

Definition: whiter (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6705)

[Study Light](https://www.studylight.org/lexicons/hebrew/6705.html)

[Bible Hub](https://biblehub.com/str/hebrew/6705.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06705)

[Bible Bento](https://biblebento.com/dictionary/H6705.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6705/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6705.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6705)
