# שָׁחַט

[šāḥaṭ](https://www.blueletterbible.org/lexicon/h7819)

Definition: kill (42x), slay (36x), offer (1x), shot out (1x), slaughter (1x).

Part of speech: feminine noun, verb

Occurs 82 times in 70 verses

Greek: [sphazō](../g/g4969.md)

Edenics: shoot

Synonyms: [kill](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Kill)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7819)

[Study Light](https://www.studylight.org/lexicons/hebrew/7819.html)

[Bible Hub](https://biblehub.com/str/hebrew/7819.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07819)

[Bible Bento](https://biblebento.com/dictionary/H7819.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7819/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7819.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7819)
