# אַרְבַּעְתָּיִם

['arbaʿtāyim](https://www.blueletterbible.org/lexicon/h706)

Definition: fourfold (1x).

Part of speech: adverb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h706)

[Study Light](https://www.studylight.org/lexicons/hebrew/706.html)

[Bible Hub](https://biblehub.com/str/hebrew/706.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0706)

[Bible Bento](https://biblebento.com/dictionary/H706.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/706/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/706.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h706)
