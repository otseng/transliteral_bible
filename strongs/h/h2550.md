# חָמַל

[ḥāmal](https://www.blueletterbible.org/lexicon/h2550)

Definition: pity (18x), spare (18x), have compassion (5x).

Part of speech: verb

Occurs 41 times in 40 verses

Hebrew: [ḥemlâ](../h/h2551.md)

Greek: [pheidomai](../g/g5339.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2550)

[Study Light](https://www.studylight.org/lexicons/hebrew/2550.html)

[Bible Hub](https://biblehub.com/str/hebrew/2550.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02550)

[Bible Bento](https://biblebento.com/dictionary/H2550.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2550/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2550.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2550)
