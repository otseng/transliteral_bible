# מוֹטָה

[môṭâ](https://www.blueletterbible.org/lexicon/h4133)

Definition: yoke (8x), band (2x), stave (1x), heavy (1x).

Part of speech: feminine noun

Occurs 12 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4133)

[Study Light](https://www.studylight.org/lexicons/hebrew/4133.html)

[Bible Hub](https://biblehub.com/str/hebrew/4133.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04133)

[Bible Bento](https://biblebento.com/dictionary/H4133.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4133/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4133.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4133)
