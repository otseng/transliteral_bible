# חָוָח

[ḥāvāḥ](https://www.blueletterbible.org/lexicon/h2337)

Definition: thicket (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2337)

[Study Light](https://www.studylight.org/lexicons/hebrew/2337.html)

[Bible Hub](https://biblehub.com/str/hebrew/2337.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02337)

[Bible Bento](https://biblebento.com/dictionary/H2337.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2337/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2337.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2337)
