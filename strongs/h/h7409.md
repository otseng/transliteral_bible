# רֶכֶשׁ

[reḵeš](https://www.blueletterbible.org/lexicon/h7409)

Definition: mule (2x), dromedaries (1x), swift beast (1x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7409)

[Study Light](https://www.studylight.org/lexicons/hebrew/7409.html)

[Bible Hub](https://biblehub.com/str/hebrew/7409.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07409)

[Bible Bento](https://biblebento.com/dictionary/H7409.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7409/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7409.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7409)
