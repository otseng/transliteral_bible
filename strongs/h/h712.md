# אַרְגָּז

['argāz](https://www.blueletterbible.org/lexicon/h712)

Definition: coffer (3x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h712)

[Study Light](https://www.studylight.org/lexicons/hebrew/712.html)

[Bible Hub](https://biblehub.com/str/hebrew/712.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0712)

[Bible Bento](https://biblebento.com/dictionary/H712.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/712/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/712.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h712)
