# נִחֻם

[niḥum](https://www.blueletterbible.org/lexicon/h5150)

Definition: comfort (1x), comfortable (1x), repentings (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

Hebrew: [nacham](../h/h5162.md), [nōḥam](../h/h5164.md), [neḥāmâ](../h/h5165.md)

Synonyms: [comfort](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Comfort)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5150)

[Study Light](https://www.studylight.org/lexicons/hebrew/5150.html)

[Bible Hub](https://biblehub.com/str/hebrew/5150.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05150)

[Bible Bento](https://biblebento.com/dictionary/H5150.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5150/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5150.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5150)
