# תֹּרֶן

[tōren](https://www.blueletterbible.org/lexicon/h8650)

Definition: mast (2x), beacon (1x).

Part of speech: masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8650)

[Study Light](https://www.studylight.org/lexicons/hebrew/8650.html)

[Bible Hub](https://biblehub.com/str/hebrew/8650.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08650)

[Bible Bento](https://biblebento.com/dictionary/H8650.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8650/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8650.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8650)
