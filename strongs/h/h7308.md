# רוּחַ

[rûaḥ](https://www.blueletterbible.org/lexicon/h7308)

Definition: spirit (8x), wind (2x), mind (1x).

Part of speech: feminine noun

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7308)

[Study Light](https://www.studylight.org/lexicons/hebrew/7308.html)

[Bible Hub](https://biblehub.com/str/hebrew/7308.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07308)

[Bible Bento](https://biblebento.com/dictionary/H7308.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7308/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7308.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7308)
