# פִּתֹם

[piṯōm](https://www.blueletterbible.org/lexicon/h6619)

Definition: Pithom (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6619)

[Study Light](https://www.studylight.org/lexicons/hebrew/6619.html)

[Bible Hub](https://biblehub.com/str/hebrew/6619.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06619)

[Bible Bento](https://biblebento.com/dictionary/H6619.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6619/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6619.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6619)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/P/pithom.html)

[Video Bible](https://www.videobible.com/bible-dictionary/pithom)