# מָכִיר

[Māḵîr](https://www.blueletterbible.org/lexicon/h4353)

Definition: Machir (22x).

Part of speech: proper masculine noun

Occurs 22 times in 20 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4353)

[Study Light](https://www.studylight.org/lexicons/hebrew/4353.html)

[Bible Hub](https://biblehub.com/str/hebrew/4353.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04353)

[Bible Bento](https://biblebento.com/dictionary/H4353.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4353/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4353.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4353)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/M/machir.html)

[Video Bible](https://www.videobible.com/bible-dictionary/machir)