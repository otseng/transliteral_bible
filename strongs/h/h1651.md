# גְּשׁוּרִי

[Gᵊšûrî](https://www.blueletterbible.org/lexicon/h1651)

Definition: Geshurites (4x), Geshuri (2x).

Part of speech: adjective

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1651)

[Study Light](https://www.studylight.org/lexicons/hebrew/1651.html)

[Bible Hub](https://biblehub.com/str/hebrew/1651.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01651)

[Bible Bento](https://biblebento.com/dictionary/H1651.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1651/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1651.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1651)
