# בֵּן

[Bēn](https://www.blueletterbible.org/lexicon/h1122)

Definition: Ben (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1122)

[Study Light](https://www.studylight.org/lexicons/hebrew/1122.html)

[Bible Hub](https://biblehub.com/str/hebrew/1122.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01122)

[Bible Bento](https://biblebento.com/dictionary/H1122.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1122/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1122.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1122)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/ben.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ben)