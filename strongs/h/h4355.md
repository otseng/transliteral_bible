# מָכַךְ

[māḵaḵ](https://www.blueletterbible.org/lexicon/h4355)

Definition: brought low (2x), decay (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4355)

[Study Light](https://www.studylight.org/lexicons/hebrew/4355.html)

[Bible Hub](https://biblehub.com/str/hebrew/4355.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04355)

[Bible Bento](https://biblebento.com/dictionary/H4355.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4355/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4355.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4355)
