# קָל

[qāl](https://www.blueletterbible.org/lexicon/h7032)

Definition: sound (4x), voice (3x).

Part of speech: masculine noun

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7032)

[Study Light](https://www.studylight.org/lexicons/hebrew/7032.html)

[Bible Hub](https://biblehub.com/str/hebrew/7032.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07032)

[Bible Bento](https://biblebento.com/dictionary/H7032.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7032/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7032.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7032)
