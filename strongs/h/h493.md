# אֶלְנַעַם

['ElnaʿAm](https://www.blueletterbible.org/lexicon/h493)

Definition: Elnaam (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h493)

[Study Light](https://www.studylight.org/lexicons/hebrew/493.html)

[Bible Hub](https://biblehub.com/str/hebrew/493.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0493)

[Bible Bento](https://biblebento.com/dictionary/H493.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/493/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/493.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h493)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/elnaam.html)

[Video Bible](https://www.videobible.com/bible-dictionary/elnaam)