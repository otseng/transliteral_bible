# מַת

[math](https://www.blueletterbible.org/lexicon/h4962)

Definition: men (14x), few (2x), few (with H4557) (1x), friends (1x), number (1x), persons (1x), small (1x), with (1x).

Part of speech: masculine noun

Occurs 22 times in 21 verses

Names: [Mᵊṯûšelaḥ](../h/h4968.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4962)

[Study Light](https://www.studylight.org/lexicons/hebrew/4962.html)

[Bible Hub](https://biblehub.com/str/hebrew/4962.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04962)

[Bible Bento](https://biblebento.com/dictionary/H4962.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4962/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4962.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4962)
