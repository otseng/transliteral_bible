# אֲבֵדָה

['ăḇēḏâ](https://www.blueletterbible.org/lexicon/h9)

Definition: lost thing (3x), that which was lost (1x).

Part of speech: feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h9)

[Study Light](https://www.studylight.org/lexicons/hebrew/9.html)

[Bible Hub](https://biblehub.com/str/hebrew/9.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=09)

[Bible Bento](https://biblebento.com/dictionary/H9.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/9/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/9.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h9)
