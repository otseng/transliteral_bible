# יְדַע

[yᵊḏaʿ](https://www.blueletterbible.org/lexicon/h3046)

Definition: known (24x), know (18x), certify (4x), teach (1x).

Part of speech: verb

Occurs 47 times in 42 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3046)

[Study Light](https://www.studylight.org/lexicons/hebrew/3046.html)

[Bible Hub](https://biblehub.com/str/hebrew/3046.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03046)

[Bible Bento](https://biblebento.com/dictionary/H3046.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3046/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3046.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3046)
