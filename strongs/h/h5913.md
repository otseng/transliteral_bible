# עָכַס

[ʿāḵas](https://www.blueletterbible.org/lexicon/h5913)

Definition: tinkling (1x), make a tinkling ornament

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5913)

[Study Light](https://www.studylight.org/lexicons/hebrew/5913.html)

[Bible Hub](https://biblehub.com/str/hebrew/5913.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05913)

[Bible Bento](https://biblebento.com/dictionary/H5913.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5913/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5913.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5913)
