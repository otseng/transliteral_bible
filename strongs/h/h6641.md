# צָבוּעַ

[ṣāḇûaʿ](https://www.blueletterbible.org/lexicon/h6641)

Definition: speckled (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6641)

[Study Light](https://www.studylight.org/lexicons/hebrew/6641.html)

[Bible Hub](https://biblehub.com/str/hebrew/6641.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06641)

[Bible Bento](https://biblebento.com/dictionary/H6641.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6641/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6641.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6641)
