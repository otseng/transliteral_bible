# שֹׁד

[shod](https://www.blueletterbible.org/lexicon/h7701)

Definition: spoil (10x), destruction (7x), desolation (2x), robbery (2x), wasting (2x), oppression (1x), spoiler (1x), havoc, violence, destruction, devastation, ruin

Part of speech: masculine noun

Occurs 25 times in 24 verses

Hebrew: [šûḏ](../h/h7736.md)

Greek: [adikos](../g/g94.md), [kakos](../g/g2556.md), [syntrimma](../g/g4938.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7701)

[Study Light](https://www.studylight.org/lexicons/hebrew/7701.html)

[Bible Hub](https://biblehub.com/str/hebrew/7701.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07701)

[Bible Bento](https://biblebento.com/dictionary/H7701.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7701/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7701.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7701)
