# כְּנָת

[kᵊnāṯ](https://www.blueletterbible.org/lexicon/h3675)

Definition: companion (7x).

Part of speech: masculine noun

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3675)

[Study Light](https://www.studylight.org/lexicons/hebrew/3675.html)

[Bible Hub](https://biblehub.com/str/hebrew/3675.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03675)

[Bible Bento](https://biblebento.com/dictionary/H3675.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3675/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3675.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3675)
