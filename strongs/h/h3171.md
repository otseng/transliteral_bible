# יְחִיאֵל

[Yᵊḥî'Ēl](https://www.blueletterbible.org/lexicon/h3171)

Definition: Jehiel (14x).

Part of speech: proper masculine noun

Occurs 15 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3171)

[Study Light](https://www.studylight.org/lexicons/hebrew/3171.html)

[Bible Hub](https://biblehub.com/str/hebrew/3171.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03171)

[Bible Bento](https://biblebento.com/dictionary/H3171.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3171/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3171.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3171)

[Video Bible](https://www.videobible.com/bible-dictionary/jehiel)