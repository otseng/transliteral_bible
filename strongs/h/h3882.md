# לִוְיָתָן

[livyāṯān](https://www.blueletterbible.org/lexicon/h3882)

Definition: leviathan (6x).

Part of speech: masculine noun

Occurs 6 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3882)

[Study Light](https://www.studylight.org/lexicons/hebrew/3882.html)

[Bible Hub](https://biblehub.com/str/hebrew/3882.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03882)

[Bible Bento](https://biblebento.com/dictionary/H3882.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3882/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3882.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3882)
