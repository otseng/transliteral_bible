# גֵּאוּת

[ge'uwth](https://www.blueletterbible.org/lexicon/h1348)

Definition: pride (2x), majesty (2x), proudly (1x), raging (1x), lifting up (1x), excellent things (1x).

Part of speech: feminine noun

Occurs 8 times in 8 verses

Hebrew: [gē'](../h/h1341.md), [gē'â](../h/h1344.md), [ga`avah](../h/h1346.md), [gā'ôn](../h/h1347.md), [ga'ăyôn](../h/h1349.md)

Synonyms: [pride](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Pride)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1348)

[Study Light](https://www.studylight.org/lexicons/hebrew/1348.html)

[Bible Hub](https://biblehub.com/str/hebrew/1348.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01348)

[Bible Bento](https://biblebento.com/dictionary/H1348.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1348/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1348.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1348)
