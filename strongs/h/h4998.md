# נָאָה

[nā'â](https://www.blueletterbible.org/lexicon/h4998)

Definition: becometh (1x), comely (1x), beautiful (1x).

Part of speech: verb

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4998)

[Study Light](https://www.studylight.org/lexicons/hebrew/4998.html)

[Bible Hub](https://biblehub.com/str/hebrew/4998.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04998)

[Bible Bento](https://biblebento.com/dictionary/H4998.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4998/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4998.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4998)
