# עָדִין

[ʿĀḏîn](https://www.blueletterbible.org/lexicon/h5720)

Definition: Adin (4x).

Part of speech: proper masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5720)

[Study Light](https://www.studylight.org/lexicons/hebrew/5720.html)

[Bible Hub](https://biblehub.com/str/hebrew/5720.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05720)

[Bible Bento](https://biblebento.com/dictionary/H5720.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5720/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5720.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5720)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/adin.html)

[Video Bible](https://www.videobible.com/bible-dictionary/adin)