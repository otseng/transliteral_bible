# שַׁדְרַךְ

[Šaḏraḵ](https://www.blueletterbible.org/lexicon/h7714)

Definition: Shadrach (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7714)

[Study Light](https://www.studylight.org/lexicons/hebrew/7714.html)

[Bible Hub](https://biblehub.com/str/hebrew/7714.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07714)

[Bible Bento](https://biblebento.com/dictionary/H7714.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7714/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7714.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7714)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/shadrach.html)