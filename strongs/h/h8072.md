# שַׂמְלָה

[Śamlâ](https://www.blueletterbible.org/lexicon/h8072)

Definition: Samlah (4x).

Part of speech: proper masculine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8072)

[Study Light](https://www.studylight.org/lexicons/hebrew/8072.html)

[Bible Hub](https://biblehub.com/str/hebrew/8072.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08072)

[Bible Bento](https://biblebento.com/dictionary/H8072.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8072/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8072.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8072)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/samlah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/samlah)