# פִּתּוּחַ

[pitûaḥ](https://www.blueletterbible.org/lexicon/h6603)

Definition: engravings (5x), graving (2x), carved (1x), grave (with H6605) (1x), graven (1x), carved work (1x).

Part of speech: masculine noun

Occurs 11 times in 11 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6603)

[Study Light](https://www.studylight.org/lexicons/hebrew/6603.html)

[Bible Hub](https://biblehub.com/str/hebrew/6603.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06603)

[Bible Bento](https://biblebento.com/dictionary/H6603.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6603/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6603.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6603)
