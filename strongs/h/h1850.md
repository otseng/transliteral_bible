# דָּפְקָה

[Dāp̄Qâ](https://www.blueletterbible.org/lexicon/h1850)

Definition: Dophkah (2x).

Part of speech: proper locative noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1850)

[Study Light](https://www.studylight.org/lexicons/hebrew/1850.html)

[Bible Hub](https://biblehub.com/str/hebrew/1850.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01850)

[Bible Bento](https://biblebento.com/dictionary/H1850.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1850/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1850.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1850)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/dophkah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/dophkah)