# מַרְבַד

[marḇaḏ](https://www.blueletterbible.org/lexicon/h4765)

Definition: covering of tapestry (2x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4765)

[Study Light](https://www.studylight.org/lexicons/hebrew/4765.html)

[Bible Hub](https://biblehub.com/str/hebrew/4765.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04765)

[Bible Bento](https://biblebento.com/dictionary/H4765.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4765/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4765.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4765)
