# אֲרֻחָה

['ăruḥâ](https://www.blueletterbible.org/lexicon/h737)

Definition: allowance (2x), diet (2x), dinner (1x), victuals (1x).

Part of speech: feminine noun

Occurs 6 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h737)

[Study Light](https://www.studylight.org/lexicons/hebrew/737.html)

[Bible Hub](https://biblehub.com/str/hebrew/737.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0737)

[Bible Bento](https://biblebento.com/dictionary/H737.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/737/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/737.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h737)
