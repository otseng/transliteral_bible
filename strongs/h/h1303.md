# בַּרְקָנִים

[barqānîm](https://www.blueletterbible.org/lexicon/h1303)

Definition: briers (2x).

Part of speech: masculine plural noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1303)

[Study Light](https://www.studylight.org/lexicons/hebrew/1303.html)

[Bible Hub](https://biblehub.com/str/hebrew/1303.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01303)

[Bible Bento](https://biblebento.com/dictionary/H1303.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1303/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1303.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1303)
