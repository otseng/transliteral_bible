# בָּחַן

[bachan](https://www.blueletterbible.org/lexicon/h974)

Definition: try (19x), prove (7x), examine (1x), tempt (1x), trial (1x), compare, scrutinize, test, search

Part of speech: verb

Occurs 29 times in 28 verses

Hebrew: [baḥan](../h/h975.md), [bōḥan](../h/h976.md)

Greek: [dokimazō](../g/g1381.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0974)

[Study Light](https://www.studylight.org/lexicons/hebrew/974.html)

[Bible Hub](https://biblehub.com/str/hebrew/974.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0974)

[Bible Bento](https://biblebento.com/dictionary/H0974.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0974/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0974.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h974)
