# נְחֻשְׁתָּא

[Nᵊḥuštā'](https://www.blueletterbible.org/lexicon/h5179)

Definition: Nehushta (1x).

Part of speech: proper feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5179)

[Study Light](https://www.studylight.org/lexicons/hebrew/5179.html)

[Bible Hub](https://biblehub.com/str/hebrew/5179.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05179)

[Bible Bento](https://biblebento.com/dictionary/H5179.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5179/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5179.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5179)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/N/nehushta.html)

[Video Bible](https://www.videobible.com/bible-dictionary/nehushta)