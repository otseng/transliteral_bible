# בְּרַם

[bᵊram](https://www.blueletterbible.org/lexicon/h1297)

Definition: but (2x), yet (2x), nevertheless (1x).

Part of speech: adverb

Occurs 5 times in 5 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1297)

[Study Light](https://www.studylight.org/lexicons/hebrew/1297.html)

[Bible Hub](https://biblehub.com/str/hebrew/1297.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01297)

[Bible Bento](https://biblebento.com/dictionary/H1297.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1297/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1297.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1297)
