# גַּחֶלֶת

[gechel](https://www.blueletterbible.org/lexicon/h1513)

Definition: coals (17x), coals of fire (1x), to glow or kindle, ember

Part of speech: feminine noun

Occurs 18 times in 18 verses

Names: [Gaḥam](../h/h1514.md)

Greek: [anthrax](../g/g440.md)

Notes: The Arabians call things that cause very acute mental pain "burning coals of the heart" and "fire of the liver". 

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1513)

[Study Light](https://www.studylight.org/lexicons/hebrew/1513.html)

[Bible Hub](https://biblehub.com/str/hebrew/1513.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01513)

[Bible Bento](https://biblebento.com/dictionary/H1513.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1513/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1513.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1513)
