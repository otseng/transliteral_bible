# שֵׁנָא

[šēnā'](https://www.blueletterbible.org/lexicon/h8142)

Definition: sleep (23x).

Part of speech: feminine noun

Occurs 23 times in 23 verses

Hebrew: [šᵊnâ](../h/h8139.md)

Greek: [katheudō](../g/g2518.md)

Synonyms: [sleep](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Sleep)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8142)

[Study Light](https://www.studylight.org/lexicons/hebrew/8142.html)

[Bible Hub](https://biblehub.com/str/hebrew/8142.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08142)

[Bible Bento](https://biblebento.com/dictionary/H8142.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8142/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8142.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8142)

[Logos Apostolic](https://www.logosapostolic.org/hebrew-word-studies/8142-shenah-sleep.htm)
