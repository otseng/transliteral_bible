# מֵרֹנֹתִי

[mērōnōṯî](https://www.blueletterbible.org/lexicon/h4824)

Definition: Meronothite (2x).

Part of speech: adjective

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4824)

[Study Light](https://www.studylight.org/lexicons/hebrew/4824.html)

[Bible Hub](https://biblehub.com/str/hebrew/4824.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04824)

[Bible Bento](https://biblebento.com/dictionary/H4824.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4824/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4824.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4824)
