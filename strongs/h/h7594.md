# שְׁאָל

[Šᵊ'Āl](https://www.blueletterbible.org/lexicon/h7594)

Definition: Sheal (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7594)

[Study Light](https://www.studylight.org/lexicons/hebrew/7594.html)

[Bible Hub](https://biblehub.com/str/hebrew/7594.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07594)

[Bible Bento](https://biblebento.com/dictionary/H7594.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7594/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7594.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7594)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/S/sheal.html)

[Video Bible](https://www.videobible.com/bible-dictionary/sheal)