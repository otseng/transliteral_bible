# אֲבִיאֵל

['Ăḇî'Ēl](https://www.blueletterbible.org/lexicon/h22)

Definition: Abiel (3x).

Part of speech: proper masculine noun

Occurs 3 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h22)

[Study Light](https://www.studylight.org/lexicons/hebrew/22.html)

[Bible Hub](https://biblehub.com/str/hebrew/22.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=022)

[Bible Bento](https://biblebento.com/dictionary/H22.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/22/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/22.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h22)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/abiel.html)

[Video Bible](https://www.videobible.com/bible-dictionary/abiel)