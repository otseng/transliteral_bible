# צוּם

[ṣûm](https://www.blueletterbible.org/lexicon/h6684)

Definition: fast (20x), at all (1x), abstain from food

Part of speech: verb

Occurs 21 times in 17 verses

Hebrew: [ṣôm](../h/h6685.md)

Greek: [nēsteuō](../g/g3522.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6684)

[Study Light](https://www.studylight.org/lexicons/hebrew/6684.html)

[Bible Hub](https://biblehub.com/str/hebrew/6684.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06684)

[Bible Bento](https://biblebento.com/dictionary/H6684.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6684/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6684.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6684)
