# זָרָא

[zārā'](https://www.blueletterbible.org/lexicon/h2214)

Definition: loathsome (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2214)

[Study Light](https://www.studylight.org/lexicons/hebrew/2214.html)

[Bible Hub](https://biblehub.com/str/hebrew/2214.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02214)

[Bible Bento](https://biblebento.com/dictionary/H2214.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2214/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2214.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2214)
