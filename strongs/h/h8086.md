# שְׁמַע

[šᵊmaʿ](https://www.blueletterbible.org/lexicon/h8086)

Definition: hear (8x), obey (1x).

Part of speech: verb

Occurs 9 times in 9 verses

Synonyms: [hear](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Hear)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8086)

[Study Light](https://www.studylight.org/lexicons/hebrew/8086.html)

[Bible Hub](https://biblehub.com/str/hebrew/8086.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08086)

[Bible Bento](https://biblebento.com/dictionary/H8086.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8086/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8086.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8086)
