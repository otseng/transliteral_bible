# מְכַסֶּה

[mᵊḵassê](https://www.blueletterbible.org/lexicon/h4374)

Definition: that which covers (2x), cover (1x), clothing (1x).

Part of speech: masculine noun

Occurs 4 times in 4 verses

Hebrew: [kāsâ](../h/h3680.md), [kāśâ](../h/h3780.md), [miḵsê](../h/h4372.md)

Synonyms: [cover](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Cover)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4374)

[Study Light](https://www.studylight.org/lexicons/hebrew/4374.html)

[Bible Hub](https://biblehub.com/str/hebrew/4374.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04374)

[Bible Bento](https://biblebento.com/dictionary/H4374.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4374/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4374.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4374)
