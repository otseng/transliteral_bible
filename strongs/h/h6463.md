# פָּעָה

[pāʿâ](https://www.blueletterbible.org/lexicon/h6463)

Definition: cry (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Synonyms: [cry](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Cry)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6463)

[Study Light](https://www.studylight.org/lexicons/hebrew/6463.html)

[Bible Hub](https://biblehub.com/str/hebrew/6463.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06463)

[Bible Bento](https://biblebento.com/dictionary/H6463.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6463/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6463.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6463)
