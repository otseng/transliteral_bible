# יוּבָל

[yûḇāl](https://www.blueletterbible.org/lexicon/h3105)

Definition: river (1x).

Part of speech: masculine noun

Occurs 1 times in 1 verses

Synonyms: [river](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#River)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3105)

[Study Light](https://www.studylight.org/lexicons/hebrew/3105.html)

[Bible Hub](https://biblehub.com/str/hebrew/3105.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03105)

[Bible Bento](https://biblebento.com/dictionary/H3105.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3105/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3105.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3105)
