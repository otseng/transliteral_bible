# עִיר

[ʿÎr](https://www.blueletterbible.org/lexicon/h5893)

Definition: Ir (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5893)

[Study Light](https://www.studylight.org/lexicons/hebrew/5893.html)

[Bible Hub](https://biblehub.com/str/hebrew/5893.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05893)

[Bible Bento](https://biblebento.com/dictionary/H5893.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5893/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5893.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5893)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/I/ir.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ir)