# חִתִּית

[ḥitîṯ](https://www.blueletterbible.org/lexicon/h2851)

Definition: terror (8x).

Part of speech: feminine noun

Occurs 9 times in 8 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2851)

[Study Light](https://www.studylight.org/lexicons/hebrew/2851.html)

[Bible Hub](https://biblehub.com/str/hebrew/2851.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02851)

[Bible Bento](https://biblebento.com/dictionary/H2851.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2851/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2851.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2851)
