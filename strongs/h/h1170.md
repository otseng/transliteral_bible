# בַּעַל בְּרִית

[BaʿAl Bᵊrîṯ](https://www.blueletterbible.org/lexicon/h1170)

Definition: Baalberith (2x).

Part of speech: proper masculine noun

Occurs 4 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1170)

[Study Light](https://www.studylight.org/lexicons/hebrew/1170.html)

[Bible Hub](https://biblehub.com/str/hebrew/1170.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01170)

[Bible Bento](https://biblebento.com/dictionary/H1170.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1170/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1170.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1170)
