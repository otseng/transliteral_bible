# כִּלָּיוֹן

[killāyôn](https://www.blueletterbible.org/lexicon/h3631)

Definition: failing (1x), consumption (1x), completion, destruction, annihilation 

Part of speech: masculine noun

Occurs 2 times in 2 verses

Edenics: kill

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3631)

[Study Light](https://www.studylight.org/lexicons/hebrew/3631.html)

[Bible Hub](https://biblehub.com/str/hebrew/3631.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03631)

[Bible Bento](https://biblebento.com/dictionary/H3631.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3631/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3631.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3631)
