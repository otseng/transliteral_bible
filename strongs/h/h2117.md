# זָזָא

[Zāzā'](https://www.blueletterbible.org/lexicon/h2117)

Definition: Zaza (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2117)

[Study Light](https://www.studylight.org/lexicons/hebrew/2117.html)

[Bible Hub](https://biblehub.com/str/hebrew/2117.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02117)

[Bible Bento](https://biblebento.com/dictionary/H2117.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2117/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2117.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2117)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zaza.html)

[Video Bible](https://www.videobible.com/bible-dictionary/zaza)