# כְּלִמָּה

[kĕlimmah](https://www.blueletterbible.org/lexicon/h3639)

Definition: shame (20x), confusion (6x), dishonour (3x), reproach (1x), disgrace, humiliation

Part of speech: feminine noun

Occurs 30 times in 29 verses

Hebrew: [kālam](../h/h3637.md)

Greek: [entropē](../g/g1791.md), [oneidismos](../g/g3680.md), [oneidos](../g/g3681.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3639)

[Study Light](https://www.studylight.org/lexicons/hebrew/3639.html)

[Bible Hub](https://biblehub.com/str/hebrew/3639.htm)

[Morfix](https://www.morfix.co.il/en/%D7%9B%D6%BC%D6%B0%D7%9C%D6%B4%D7%9E%D6%BC%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=03639)

[Bible Bento](https://biblebento.com/dictionary/H3639.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3639/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3639.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3639)
