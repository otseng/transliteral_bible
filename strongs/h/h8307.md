# שְׁרִירוּת

[šᵊrîrûṯ](https://www.blueletterbible.org/lexicon/h8307)

Definition: imagination (9x), lust (1x).

Part of speech: feminine noun

Occurs 10 times in 10 verses

Synonyms: [lust](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Lust)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8307)

[Study Light](https://www.studylight.org/lexicons/hebrew/8307.html)

[Bible Hub](https://biblehub.com/str/hebrew/8307.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08307)

[Bible Bento](https://biblebento.com/dictionary/H8307.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8307/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8307.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8307)
