# צפון

[ṣp̄vn](https://www.blueletterbible.org/lexicon/h6827)

Definition: Zephon (1x).

Part of speech: proper masculine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6827)

[Study Light](https://www.studylight.org/lexicons/hebrew/6827.html)

[Bible Hub](https://biblehub.com/str/hebrew/6827.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06827)

[Bible Bento](https://biblebento.com/dictionary/H6827.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6827/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6827.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6827)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/Z/zephon.html)