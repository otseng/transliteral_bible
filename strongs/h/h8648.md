# תְּרֵין

[tᵊrên](https://www.blueletterbible.org/lexicon/h8648)

Definition: twelve (with H6236) (2x), two (1x), second (1x).

Part of speech: masculine/feminine noun

Occurs 4 times in 4 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8648)

[Study Light](https://www.studylight.org/lexicons/hebrew/8648.html)

[Bible Hub](https://biblehub.com/str/hebrew/8648.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08648)

[Bible Bento](https://biblebento.com/dictionary/H8648.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8648/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8648.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8648)
