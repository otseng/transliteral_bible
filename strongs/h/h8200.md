# שְׁפַט

[šᵊp̄aṭ](https://www.blueletterbible.org/lexicon/h8200)

Definition: magistrates (1x).

Part of speech: verb

Occurs 1 times in 1 verses

Hebrew: [mishpat](../h/h4941.md), [šᵊp̄ôṭ](../h/h8196.md), [shaphat](../h/h8199.md), [šep̄eṭ](../h/h8201.md)

Edenics: scepter

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8200)

[Study Light](https://www.studylight.org/lexicons/hebrew/8200.html)

[Bible Hub](https://biblehub.com/str/hebrew/8200.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08200)

[Bible Bento](https://biblebento.com/dictionary/H8200.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8200/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8200.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8200)
