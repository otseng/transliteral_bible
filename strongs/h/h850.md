# אֶשְׁתּוֹן

['Eštôn](https://www.blueletterbible.org/lexicon/h850)

Definition: Eshton (2x).

Part of speech: proper masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h850)

[Study Light](https://www.studylight.org/lexicons/hebrew/850.html)

[Bible Hub](https://biblehub.com/str/hebrew/850.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0850)

[Bible Bento](https://biblebento.com/dictionary/H850.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/850/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/850.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h850)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/E/eshton.html)

[Video Bible](https://www.videobible.com/bible-dictionary/eshton)