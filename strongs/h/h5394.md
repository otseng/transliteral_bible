# נָשַׁל

[nāšal](https://www.blueletterbible.org/lexicon/h5394)

Definition: cast out (1x), put out (1x), slip (1x), put off (1x), loose (1x), cast (1x), drive (1x).

Part of speech: verb

Occurs 7 times in 7 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5394)

[Study Light](https://www.studylight.org/lexicons/hebrew/5394.html)

[Bible Hub](https://biblehub.com/str/hebrew/5394.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05394)

[Bible Bento](https://biblebento.com/dictionary/H5394.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5394/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5394.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5394)
