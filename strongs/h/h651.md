# אָפֵל

['āp̄ēl](https://www.blueletterbible.org/lexicon/h651)

Definition: very dark (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h651)

[Study Light](https://www.studylight.org/lexicons/hebrew/651.html)

[Bible Hub](https://biblehub.com/str/hebrew/651.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0651)

[Bible Bento](https://biblebento.com/dictionary/H651.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/651/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/651.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h651)
