# מִטְפַּחַת

[miṭpaḥaṯ](https://www.blueletterbible.org/lexicon/h4304)

Definition: vail (1x), wimples (1x), wide cloak

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4304)

[Study Light](https://www.studylight.org/lexicons/hebrew/4304.html)

[Bible Hub](https://biblehub.com/str/hebrew/4304.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04304)

[Bible Bento](https://biblebento.com/dictionary/H4304.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4304/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4304.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4304)
