# אַמָּה

['Ammâ](https://www.blueletterbible.org/lexicon/h522)

Definition: Ammah (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h522)

[Study Light](https://www.studylight.org/lexicons/hebrew/522.html)

[Bible Hub](https://biblehub.com/str/hebrew/522.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=0522)

[Bible Bento](https://biblebento.com/dictionary/H522.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/522/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/522.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h522)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/A/ammah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/ammah)