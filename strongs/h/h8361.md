# שִׁתִּין

[šitîn](https://www.blueletterbible.org/lexicon/h8361)

Definition: threescore (4x).

Part of speech: adjective, indeclinable noun

Occurs 4 times in 3 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8361)

[Study Light](https://www.studylight.org/lexicons/hebrew/8361.html)

[Bible Hub](https://biblehub.com/str/hebrew/8361.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08361)

[Bible Bento](https://biblebento.com/dictionary/H8361.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8361/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8361.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8361)
