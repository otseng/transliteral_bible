# דּוּשׁ

[dûš](https://www.blueletterbible.org/lexicon/h1759)

Definition: tread it down (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1759)

[Study Light](https://www.studylight.org/lexicons/hebrew/1759.html)

[Bible Hub](https://biblehub.com/str/hebrew/1759.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01759)

[Bible Bento](https://biblebento.com/dictionary/H1759.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1759/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1759.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1759)
