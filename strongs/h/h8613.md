# תָּפְתֶּה

[tāp̄tê](https://www.blueletterbible.org/lexicon/h8613)

Definition: Tophet (1x), "place of fire", a place in the southeast end of the valley of the son of Hinnom south of Jerusalem

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8613)

[Study Light](https://www.studylight.org/lexicons/hebrew/8613.html)

[Bible Hub](https://biblehub.com/str/hebrew/8613.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=08613)

[Bible Bento](https://biblebento.com/dictionary/H8613.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8613/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8613.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8613)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/T/tophet.html)