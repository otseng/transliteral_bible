# דָּגוֹן

[Dāḡôn](https://www.blueletterbible.org/lexicon/h1712)

Definition: Dagon (13x), "a fish"

Part of speech: proper masculine noun

Occurs 13 times in 7 verses

Notes: Philistine deity of fertility; represented with the face and hands of a man and the tail of a fish

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1712)

[Study Light](https://www.studylight.org/lexicons/hebrew/1712.html)

[Bible Hub](https://biblehub.com/str/hebrew/1712.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01712)

[Bible Bento](https://biblebento.com/dictionary/H1712.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1712/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1712.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1712)

[Wikipedia](https://en.wikipedia.org/wiki/Dagon)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/D/dagon.html)

[Video Bible](https://www.videobible.com/bible-dictionary/dagon)