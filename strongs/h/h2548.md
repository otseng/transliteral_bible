# חָמִיץ

[ḥāmîṣ](https://www.blueletterbible.org/lexicon/h2548)

Definition: clean (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

Synonyms: [clean](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Clean)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h2548)

[Study Light](https://www.studylight.org/lexicons/hebrew/2548.html)

[Bible Hub](https://biblehub.com/str/hebrew/2548.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=02548)

[Bible Bento](https://biblebento.com/dictionary/H2548.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/2548/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/2548.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h2548)
