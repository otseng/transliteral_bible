# גֹּעָה

[GōʿÂ](https://www.blueletterbible.org/lexicon/h1601)

Definition: Goath (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1601)

[Study Light](https://www.studylight.org/lexicons/hebrew/1601.html)

[Bible Hub](https://biblehub.com/str/hebrew/1601.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01601)

[Bible Bento](https://biblebento.com/dictionary/H1601.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1601/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1601.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1601)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/G/goath.html)