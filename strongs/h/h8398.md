# תֵּבֵל

[tebel](https://www.blueletterbible.org/lexicon/h8398)

Definition: world (35x), habitable part (1x).

Part of speech: feminine noun

Occurs 36 times in 36 verses

Greek: [kosmos](../g/g2889.md)

Edenics: world, word

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h8398)

[Study Light](https://www.studylight.org/lexicons/hebrew/8398.html)

[Bible Hub](https://biblehub.com/str/hebrew/8398.htm)

[Morfix](https://www.morfix.co.il/en/%D7%AA%D6%BC%D6%B5%D7%91%D6%B5%D7%9C)

[NET Bible](http://classic.net.bible.org/strong.php?id=08398)

[Bible Bento](https://biblebento.com/dictionary/H8398.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/8398/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/8398.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h8398)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%AA%D7%91%D7%9C)

[Logos Apostolic](https://www.logosapostolic.org/hebrew-word-studies/8398-tevel-world.htm)
