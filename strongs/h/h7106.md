# קָצַע

[qāṣaʿ](https://www.blueletterbible.org/lexicon/h7106)

Definition: cause to scrape (1x), corner (1x).

Part of speech: verb

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7106)

[Study Light](https://www.studylight.org/lexicons/hebrew/7106.html)

[Bible Hub](https://biblehub.com/str/hebrew/7106.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07106)

[Bible Bento](https://biblebento.com/dictionary/H7106.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7106/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7106.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7106)
