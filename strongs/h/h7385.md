# רִיק

[riyq](https://www.blueletterbible.org/lexicon/h7385)

Definition: vain (7x), vanity (2x), no purpose (1x), empty (1x), vain thing (1x), boring

Part of speech: masculine noun

Occurs 12 times in 12 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7385)

[Study Light](https://www.studylight.org/lexicons/hebrew/7385.html)

[Bible Hub](https://biblehub.com/str/hebrew/7385.htm)

[Morfix](https://www.morfix.co.il/en/%D7%A8%D6%B4%D7%99%D7%A7)

[NET Bible](http://classic.net.bible.org/strong.php?id=07385)

[Bible Bento](https://biblebento.com/dictionary/H7385.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7385/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7385.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7385)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A8%D7%99%D7%A7)
