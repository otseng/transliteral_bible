# קָרֵחַ

[Qārēaḥ](https://www.blueletterbible.org/lexicon/h7143)

Definition: Kareah (13x), Careah (1x).

Part of speech: proper masculine noun

Occurs 14 times in 14 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7143)

[Study Light](https://www.studylight.org/lexicons/hebrew/7143.html)

[Bible Hub](https://biblehub.com/str/hebrew/7143.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07143)

[Bible Bento](https://biblebento.com/dictionary/H7143.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7143/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7143.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7143)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/K/kareah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/kareah)