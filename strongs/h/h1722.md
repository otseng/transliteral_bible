# דְּהַב

[dᵊhaḇ](https://www.blueletterbible.org/lexicon/h1722)

Definition: gold (14x), golden (9x).

Part of speech: masculine noun

Occurs 23 times in 23 verses

Hebrew: [zāhāḇ](../h/h2091.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1722)

[Study Light](https://www.studylight.org/lexicons/hebrew/1722.html)

[Bible Hub](https://biblehub.com/str/hebrew/1722.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01722)

[Bible Bento](https://biblebento.com/dictionary/H1722.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1722/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1722.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1722)
