# רֶבַע

[reḇaʿ](https://www.blueletterbible.org/lexicon/h7252)

Definition: lying down (1x).

Part of speech: verb

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7252)

[Study Light](https://www.studylight.org/lexicons/hebrew/7252.html)

[Bible Hub](https://biblehub.com/str/hebrew/7252.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07252)

[Bible Bento](https://biblebento.com/dictionary/H7252.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7252/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7252.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7252)
