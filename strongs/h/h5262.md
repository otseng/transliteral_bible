# נֶסֶךְ

[necek](https://www.blueletterbible.org/lexicon/h5262)

Definition: offering (59x), image (4x), cover withal (1x), drink offering

Part of speech: masculine noun

Occurs 64 times in 62 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h5262)

[Study Light](https://www.studylight.org/lexicons/hebrew/5262.html)

[Bible Hub](https://biblehub.com/str/hebrew/5262.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=05262)

[Bible Bento](https://biblebento.com/dictionary/H5262.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/5262/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/5262.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h5262)
