# אֲחֻזָּה

['achuzzah](https://www.blueletterbible.org/lexicon/h272)

Definition: possession(s) (66x)

Part of speech: feminine noun

Occurs 66 times in 58 verses

Hebrew: ['āḥaz](../h/h270.md)

Greek: [kataschesis](../g/g2697.md), [klēronomia](../g/g2817.md)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h0272)

[Study Light](https://www.studylight.org/lexicons/hebrew/272.html)

[Bible Hub](https://biblehub.com/str/hebrew/272.htm)

[Morfix](https://www.morfix.co.il/en/%D7%90%D6%B2%D7%97%D6%BB%D7%96%D6%BC%D6%B8%D7%94)

[NET Bible](http://classic.net.bible.org/strong.php?id=0272)

[Bible Bento](https://biblebento.com/dictionary/H0272.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0272/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0272.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h272)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%A0%D7%97%D7%9C%D7%94)
