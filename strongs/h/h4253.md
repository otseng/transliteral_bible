# מַחְלָפָה

[maḥlāp̄â](https://www.blueletterbible.org/lexicon/h4253)

Definition: lock (2x).

Part of speech: feminine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h4253)

[Study Light](https://www.studylight.org/lexicons/hebrew/4253.html)

[Bible Hub](https://biblehub.com/str/hebrew/4253.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=04253)

[Bible Bento](https://biblebento.com/dictionary/H4253.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/4253/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/4253.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h4253)
