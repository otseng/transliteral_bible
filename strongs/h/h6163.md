# עֲרָבִי

[ʿĂrāḇî](https://www.blueletterbible.org/lexicon/h6163)

Definition: Arabian (9x).

Part of speech: proper patrial adjective

Occurs 10 times in 9 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6163)

[Study Light](https://www.studylight.org/lexicons/hebrew/6163.html)

[Bible Hub](https://biblehub.com/str/hebrew/6163.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06163)

[Bible Bento](https://biblebento.com/dictionary/H6163.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6163/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6163.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6163)
