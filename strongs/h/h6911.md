# קִבְצַיִם

[Qiḇṣayim](https://www.blueletterbible.org/lexicon/h6911)

Definition: Kibzaim (1x).

Part of speech: proper locative noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h6911)

[Study Light](https://www.studylight.org/lexicons/hebrew/6911.html)

[Bible Hub](https://biblehub.com/str/hebrew/6911.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=06911)

[Bible Bento](https://biblebento.com/dictionary/H6911.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/6911/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/6911.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h6911)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/K/kibzaim.html)

[Video Bible](https://www.videobible.com/bible-dictionary/kibzaim)