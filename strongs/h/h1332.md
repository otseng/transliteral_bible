# בִּתְיָה

[Biṯyâ](https://www.blueletterbible.org/lexicon/h1332)

Definition: Bithiah (1x).

Part of speech: proper feminine noun

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1332)

[Study Light](https://www.studylight.org/lexicons/hebrew/1332.html)

[Bible Hub](https://biblehub.com/str/hebrew/1332.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01332)

[Bible Bento](https://biblebento.com/dictionary/H1332.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1332/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1332.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1332)

[Biblical Cyclopedia](https://www.biblicalcyclopedia.com/B/bithiah.html)

[Video Bible](https://www.videobible.com/bible-dictionary/bithiah)