# בְּרִיעִי

[bᵊrîʿî](https://www.blueletterbible.org/lexicon/h1284)

Definition: Beriites (1x).

Part of speech: adjective

Occurs 1 times in 1 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h1284)

[Study Light](https://www.studylight.org/lexicons/hebrew/1284.html)

[Bible Hub](https://biblehub.com/str/hebrew/1284.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=01284)

[Bible Bento](https://biblebento.com/dictionary/H1284.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/1284/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/1284.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h1284)
