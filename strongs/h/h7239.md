# רִבּוֹא

[ribô'](https://www.blueletterbible.org/lexicon/h7239)

Definition: ....thousand (4x), forty, etc...(thousand) (4x), ten thousand (2x), variant (1x).

Part of speech: feminine noun

Occurs 11 times in 10 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7239)

[Study Light](https://www.studylight.org/lexicons/hebrew/7239.html)

[Bible Hub](https://biblehub.com/str/hebrew/7239.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07239)

[Bible Bento](https://biblebento.com/dictionary/H7239.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7239/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7239.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7239)
