# יֶרֶק

[yereq](https://www.blueletterbible.org/lexicon/h3418)

Definition: green (4x), green thing (2x).

Part of speech: masculine noun

Occurs 6 times in 6 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h3418)

[Study Light](https://www.studylight.org/lexicons/hebrew/3418.html)

[Bible Hub](https://biblehub.com/str/hebrew/3418.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=03418)

[Bible Bento](https://biblebento.com/dictionary/H3418.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/3418/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/3418.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h3418)
