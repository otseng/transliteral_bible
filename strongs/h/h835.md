# אֶשֶׁר

['esher](https://www.blueletterbible.org/lexicon/h835)

Definition: blessed (27x), happy (18x), happiness, blessedness, often used as interjection

Part of speech: masculine noun

Occurs 45 times in 42 verses

Hebrew: ['āšar](../h/h833.md), [yashar](../h/h3477.md)

Greek: [makarios](../g/g3107.md)

Synonyms: [blessed](https://simple.uniquebibleapp.com/book/Synonyms/Hebrew#Blessed)

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h835)

[Study Light](https://www.studylight.org/lexicons/hebrew/835.html)

[Bible Hub](https://biblehub.com/str/hebrew/835.htm)

[Wiktionary](https://en.wiktionary.org/wiki/%D7%90%D7%A9%D7%A8)

[NET Bible](http://classic.net.bible.org/strong.php?id=0835)

[Bible Bento](https://biblebento.com/dictionary/H0835.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/0835/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/0835.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h835)
