# שִׁבָּרוֹן

[šibārôn](https://www.blueletterbible.org/lexicon/h7670)

Definition: destruction (1x), breaking (1x).

Part of speech: masculine noun

Occurs 2 times in 2 verses

## Articles

[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h7670)

[Study Light](https://www.studylight.org/lexicons/hebrew/7670.html)

[Bible Hub](https://biblehub.com/str/hebrew/7670.htm)

[NET Bible](http://classic.net.bible.org/strong.php?id=07670)

[Bible Bento](https://biblebento.com/dictionary/H7670.html)

[Bible Study Company](https://biblestudycompany.com/reader/strongs/7670/hebrew)

[Lexicon Concordance](http://lexiconcordance.com/hebrew/7670.html)

[New Jerusalem](http://www.newjerusalem.org/Strongs/h7670)
