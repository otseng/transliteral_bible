# [Proverbs 30](https://www.blueletterbible.org/kjv/proverbs/30)

<a name="proverbs_30_1"></a>Proverbs 30:1

The [dabar](../../strongs/h/h1697.md) of ['Āḡûr](../../strongs/h/h94.md) the [ben](../../strongs/h/h1121.md) of [Yāqê](../../strongs/h/h3348.md), even the [maśśā'](../../strongs/h/h4853.md): the [geḇer](../../strongs/h/h1397.md) [nᵊ'um](../../strongs/h/h5002.md) unto ['Îṯî'Ēl](../../strongs/h/h384.md), even unto ['Îṯî'Ēl](../../strongs/h/h384.md) and ['Uḵāl](../../strongs/h/h401.md),

<a name="proverbs_30_2"></a>Proverbs 30:2

Surely I am more [baʿar](../../strongs/h/h1198.md) than any ['iysh](../../strongs/h/h376.md), and have not the [bînâ](../../strongs/h/h998.md) of an ['āḏām](../../strongs/h/h120.md).

<a name="proverbs_30_3"></a>Proverbs 30:3

I neither [lamad](../../strongs/h/h3925.md) [ḥāḵmâ](../../strongs/h/h2451.md), nor [yada'](../../strongs/h/h3045.md) the [da'ath](../../strongs/h/h1847.md) of the [qadowsh](../../strongs/h/h6918.md).

<a name="proverbs_30_4"></a>Proverbs 30:4

Who hath [ʿālâ](../../strongs/h/h5927.md) into [shamayim](../../strongs/h/h8064.md), or [yarad](../../strongs/h/h3381.md)? who hath ['āsap̄](../../strongs/h/h622.md) the [ruwach](../../strongs/h/h7307.md) in his [ḥōp̄en](../../strongs/h/h2651.md)? who hath [tsarar](../../strongs/h/h6887.md) the [mayim](../../strongs/h/h4325.md) in a [śimlâ](../../strongs/h/h8071.md)? who hath [quwm](../../strongs/h/h6965.md) all the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md)? what is his [shem](../../strongs/h/h8034.md), and what is his [ben](../../strongs/h/h1121.md) [shem](../../strongs/h/h8034.md), if thou canst [yada'](../../strongs/h/h3045.md)?

<a name="proverbs_30_5"></a>Proverbs 30:5

Every ['imrah](../../strongs/h/h565.md) of ['ĕlvôha](../../strongs/h/h433.md) is [tsaraph](../../strongs/h/h6884.md): he is a [magen](../../strongs/h/h4043.md) unto them that put their [chacah](../../strongs/h/h2620.md) in him.

<a name="proverbs_30_6"></a>Proverbs 30:6

[yāsap̄](../../strongs/h/h3254.md) thou not unto his [dabar](../../strongs/h/h1697.md), lest he [yakach](../../strongs/h/h3198.md) thee, and thou be found a [kāzaḇ](../../strongs/h/h3576.md).

<a name="proverbs_30_7"></a>Proverbs 30:7

[šᵊnayim](../../strongs/h/h8147.md) things have I [sha'al](../../strongs/h/h7592.md) of thee; [mānaʿ](../../strongs/h/h4513.md) me them not before I [muwth](../../strongs/h/h4191.md):

<a name="proverbs_30_8"></a>Proverbs 30:8

[rachaq](../../strongs/h/h7368.md) from me [shav'](../../strongs/h/h7723.md) and [dabar](../../strongs/h/h1697.md) [kazab](../../strongs/h/h3577.md): [nathan](../../strongs/h/h5414.md) me neither [rêš](../../strongs/h/h7389.md) nor [ʿōšer](../../strongs/h/h6239.md); [taraph](../../strongs/h/h2963.md) me with [lechem](../../strongs/h/h3899.md) [choq](../../strongs/h/h2706.md) for me:

<a name="proverbs_30_9"></a>Proverbs 30:9

Lest I be [sāׂbaʿ](../../strongs/h/h7646.md), and [kāḥaš](../../strongs/h/h3584.md) thee, and ['āmar](../../strongs/h/h559.md), Who is [Yĕhovah](../../strongs/h/h3068.md)? or lest I be [yarash](../../strongs/h/h3423.md), and [ganab](../../strongs/h/h1589.md), and [tāp̄aś](../../strongs/h/h8610.md) the [shem](../../strongs/h/h8034.md) of my ['Elohiym](../../strongs/h/h430.md).

<a name="proverbs_30_10"></a>Proverbs 30:10

[lāšan](../../strongs/h/h3960.md) not an ['ebed](../../strongs/h/h5650.md) unto his ['adown](../../strongs/h/h113.md), lest he [qālal](../../strongs/h/h7043.md) thee, and thou be found ['asham](../../strongs/h/h816.md).

<a name="proverbs_30_11"></a>Proverbs 30:11

There is a [dôr](../../strongs/h/h1755.md) that [qālal](../../strongs/h/h7043.md) their ['ab](../../strongs/h/h1.md), and doth not [barak](../../strongs/h/h1288.md) their ['em](../../strongs/h/h517.md).

<a name="proverbs_30_12"></a>Proverbs 30:12

There is a [dôr](../../strongs/h/h1755.md) that are [tahowr](../../strongs/h/h2889.md) in their own ['ayin](../../strongs/h/h5869.md), and yet is not [rāḥaṣ](../../strongs/h/h7364.md) from their [ṣ'h](../../strongs/h/h6675.md).

<a name="proverbs_30_13"></a>Proverbs 30:13

There is a [dôr](../../strongs/h/h1755.md), O how [ruwm](../../strongs/h/h7311.md) are their ['ayin](../../strongs/h/h5869.md)! and their ['aph'aph](../../strongs/h/h6079.md) are [nasa'](../../strongs/h/h5375.md).

<a name="proverbs_30_14"></a>Proverbs 30:14

There is a [dôr](../../strongs/h/h1755.md), whose [šēn](../../strongs/h/h8127.md) are as [chereb](../../strongs/h/h2719.md), and their [mᵊṯallᵊʿâ](../../strongs/h/h4973.md) as [ma'ăḵeleṯ](../../strongs/h/h3979.md), to ['akal](../../strongs/h/h398.md) the ['aniy](../../strongs/h/h6041.md) from off the ['erets](../../strongs/h/h776.md), and the ['ebyown](../../strongs/h/h34.md) from among ['āḏām](../../strongs/h/h120.md).

<a name="proverbs_30_15"></a>Proverbs 30:15

The [ʿălûqâ](../../strongs/h/h5936.md) hath [šᵊnayim](../../strongs/h/h8147.md) [bath](../../strongs/h/h1323.md), crying, [yāhaḇ](../../strongs/h/h3051.md), [yāhaḇ](../../strongs/h/h3051.md). There are [šālôš](../../strongs/h/h7969.md) things that are never [sāׂbaʿ](../../strongs/h/h7646.md), yea, ['arbaʿ](../../strongs/h/h702.md) things ['āmar](../../strongs/h/h559.md) not, It is [hôn](../../strongs/h/h1952.md):

<a name="proverbs_30_16"></a>Proverbs 30:16

The [shĕ'owl](../../strongs/h/h7585.md); and the [ʿōṣer](../../strongs/h/h6115.md) [raḥam](../../strongs/h/h7356.md); the ['erets](../../strongs/h/h776.md) that is not [sāׂbaʿ](../../strongs/h/h7646.md) with [mayim](../../strongs/h/h4325.md); and the ['esh](../../strongs/h/h784.md) that ['āmar](../../strongs/h/h559.md) not, It is [hôn](../../strongs/h/h1952.md).

<a name="proverbs_30_17"></a>Proverbs 30:17

The ['ayin](../../strongs/h/h5869.md) that [lāʿaḡ](../../strongs/h/h3932.md) at his ['ab](../../strongs/h/h1.md), and [bûz](../../strongs/h/h936.md) to [yᵊqāhâ](../../strongs/h/h3349.md) his ['em](../../strongs/h/h517.md), the [ʿōrēḇ](../../strongs/h/h6158.md) of the [nachal](../../strongs/h/h5158.md) shall [nāqar](../../strongs/h/h5365.md) it, and the [ben](../../strongs/h/h1121.md) [nesheׁr](../../strongs/h/h5404.md) shall ['akal](../../strongs/h/h398.md) it.

<a name="proverbs_30_18"></a>Proverbs 30:18

There be [šālôš](../../strongs/h/h7969.md) things which are too [pala'](../../strongs/h/h6381.md) for me, yea, ['arbaʿ](../../strongs/h/h702.md) which I [yada'](../../strongs/h/h3045.md) not:

<a name="proverbs_30_19"></a>Proverbs 30:19

The [derek](../../strongs/h/h1870.md) of a [nesheׁr](../../strongs/h/h5404.md) in the [shamayim](../../strongs/h/h8064.md); the [derek](../../strongs/h/h1870.md) of a [nachash](../../strongs/h/h5175.md) upon a [tsuwr](../../strongs/h/h6697.md); the [derek](../../strongs/h/h1870.md) of a ['ŏnîyâ](../../strongs/h/h591.md) in the [leb](../../strongs/h/h3820.md) of the [yam](../../strongs/h/h3220.md); and the [derek](../../strongs/h/h1870.md) of a [geḇer](../../strongs/h/h1397.md) with a [ʿalmâ](../../strongs/h/h5959.md).

<a name="proverbs_30_20"></a>Proverbs 30:20

Such is the [derek](../../strongs/h/h1870.md) of a [na'aph](../../strongs/h/h5003.md) ['ishshah](../../strongs/h/h802.md); she ['akal](../../strongs/h/h398.md), and [māḥâ](../../strongs/h/h4229.md) her [peh](../../strongs/h/h6310.md), and ['āmar](../../strongs/h/h559.md), I have [pa'al](../../strongs/h/h6466.md) no ['aven](../../strongs/h/h205.md).

<a name="proverbs_30_21"></a>Proverbs 30:21

For [šālôš](../../strongs/h/h7969.md) things the ['erets](../../strongs/h/h776.md) is [ragaz](../../strongs/h/h7264.md), and for ['arbaʿ](../../strongs/h/h702.md) which it [yakol](../../strongs/h/h3201.md) [nasa'](../../strongs/h/h5375.md):

<a name="proverbs_30_22"></a>Proverbs 30:22

For an ['ebed](../../strongs/h/h5650.md) when he [mālaḵ](../../strongs/h/h4427.md); and a [nabal](../../strongs/h/h5036.md) when he is [sāׂbaʿ](../../strongs/h/h7646.md) with [lechem](../../strongs/h/h3899.md);

<a name="proverbs_30_23"></a>Proverbs 30:23

For a [sane'](../../strongs/h/h8130.md) when she is [bāʿal](../../strongs/h/h1166.md); and a [šip̄ḥâ](../../strongs/h/h8198.md) that is [yarash](../../strongs/h/h3423.md) to her [gᵊḇereṯ](../../strongs/h/h1404.md).

<a name="proverbs_30_24"></a>Proverbs 30:24

There be ['arbaʿ](../../strongs/h/h702.md) things which are [qāṭān](../../strongs/h/h6996.md) upon the ['erets](../../strongs/h/h776.md), but they are [ḥāḵam](../../strongs/h/h2449.md) [ḥāḵām](../../strongs/h/h2450.md):

<a name="proverbs_30_25"></a>Proverbs 30:25

The [nᵊmālâ](../../strongs/h/h5244.md) are an ['am](../../strongs/h/h5971.md) not ['az](../../strongs/h/h5794.md), yet they [kuwn](../../strongs/h/h3559.md) their [lechem](../../strongs/h/h3899.md) in the [qayiṣ](../../strongs/h/h7019.md);

<a name="proverbs_30_26"></a>Proverbs 30:26

The [šāp̄ān](../../strongs/h/h8227.md) are but an ['atsuwm](../../strongs/h/h6099.md) ['am](../../strongs/h/h5971.md), yet [śûm](../../strongs/h/h7760.md) they their [bayith](../../strongs/h/h1004.md) in the [cela'](../../strongs/h/h5553.md);

<a name="proverbs_30_27"></a>Proverbs 30:27

The ['arbê](../../strongs/h/h697.md) have no [melek](../../strongs/h/h4428.md), yet go they [yāṣā'](../../strongs/h/h3318.md) all of them by [ḥāṣaṣ](../../strongs/h/h2686.md);

<a name="proverbs_30_28"></a>Proverbs 30:28

The [śᵊmāmîṯ](../../strongs/h/h8079.md) [tāp̄aś](../../strongs/h/h8610.md) with her [yad](../../strongs/h/h3027.md), and is in [melek](../../strongs/h/h4428.md) [heykal](../../strongs/h/h1964.md).

<a name="proverbs_30_29"></a>Proverbs 30:29

There be [šālôš](../../strongs/h/h7969.md) things which [ṣaʿaḏ](../../strongs/h/h6806.md) [yatab](../../strongs/h/h3190.md), yea, ['arbaʿ](../../strongs/h/h702.md) are [ṭôḇ](../../strongs/h/h2895.md) in [yālaḵ](../../strongs/h/h3212.md):

<a name="proverbs_30_30"></a>Proverbs 30:30

A [layiš](../../strongs/h/h3918.md) which is [gibôr](../../strongs/h/h1368.md) among [bĕhemah](../../strongs/h/h929.md), and [shuwb](../../strongs/h/h7725.md) not [paniym](../../strongs/h/h6440.md) any;

<a name="proverbs_30_31"></a>Proverbs 30:31

A [māṯnayim](../../strongs/h/h4975.md) H2223; an he [tayiš](../../strongs/h/h8495.md) ['av](../../strongs/h/h176.md); and a [melek](../../strongs/h/h4428.md), against whom there is no rising ['alqûm](../../strongs/h/h510.md).

<a name="proverbs_30_32"></a>Proverbs 30:32

If thou hast done [nabel](../../strongs/h/h5034.md) in [nasa'](../../strongs/h/h5375.md) thyself, or if thou hast thought [zāmam](../../strongs/h/h2161.md), lay thine [yad](../../strongs/h/h3027.md) upon thy [peh](../../strongs/h/h6310.md).

<a name="proverbs_30_33"></a>Proverbs 30:33

Surely the [mîṣ](../../strongs/h/h4330.md) of [chalab](../../strongs/h/h2461.md) [yāṣā'](../../strongs/h/h3318.md) [ḥem'â](../../strongs/h/h2529.md), and the [mîṣ](../../strongs/h/h4330.md) of the ['aph](../../strongs/h/h639.md) [yāṣā'](../../strongs/h/h3318.md) [dam](../../strongs/h/h1818.md): so the [mîṣ](../../strongs/h/h4330.md) of ['aph](../../strongs/h/h639.md) [yāṣā'](../../strongs/h/h3318.md) [rîḇ](../../strongs/h/h7379.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 29](proverbs_29.md) - [Proverbs 31](proverbs_31.md)