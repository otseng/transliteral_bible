# [Proverbs 27](https://www.blueletterbible.org/kjv/proverbs/27)

<a name="proverbs_27_1"></a>Proverbs 27:1

[halal](../../strongs/h/h1984.md) not thyself of to [māḥār](../../strongs/h/h4279.md); for thou [yada'](../../strongs/h/h3045.md) not what a [yowm](../../strongs/h/h3117.md) may [yalad](../../strongs/h/h3205.md).

<a name="proverbs_27_2"></a>Proverbs 27:2

Let another [zûr](../../strongs/h/h2114.md) [halal](../../strongs/h/h1984.md) thee, and not thine own [peh](../../strongs/h/h6310.md); a [nāḵrî](../../strongs/h/h5237.md), and not thine own [saphah](../../strongs/h/h8193.md).

<a name="proverbs_27_3"></a>Proverbs 27:3

An ['eben](../../strongs/h/h68.md) is [kōḇeḏ](../../strongs/h/h3514.md), and the [ḥôl](../../strongs/h/h2344.md) [nēṭel](../../strongs/h/h5192.md); but an ['ĕvîl](../../strongs/h/h191.md) [ka'ac](../../strongs/h/h3708.md) is [kāḇēḏ](../../strongs/h/h3515.md) than them [šᵊnayim](../../strongs/h/h8147.md).

<a name="proverbs_27_4"></a>Proverbs 27:4

[chemah](../../strongs/h/h2534.md) is ['aḵzᵊrîyûṯ](../../strongs/h/h395.md), and ['aph](../../strongs/h/h639.md) is [šeṭep̄](../../strongs/h/h7858.md); but who is able to ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [qin'â](../../strongs/h/h7068.md)?

<a name="proverbs_27_5"></a>Proverbs 27:5

[gālâ](../../strongs/h/h1540.md) [tôḵēḥâ](../../strongs/h/h8433.md) is [towb](../../strongs/h/h2896.md) than [cathar](../../strongs/h/h5641.md) ['ahăḇâ](../../strongs/h/h160.md).

<a name="proverbs_27_6"></a>Proverbs 27:6

['aman](../../strongs/h/h539.md) are the [peṣaʿ](../../strongs/h/h6482.md) of an ['ahab](../../strongs/h/h157.md); but the [nᵊšîqâ](../../strongs/h/h5390.md) of a [sane'](../../strongs/h/h8130.md) are [ʿāṯar](../../strongs/h/h6280.md).

<a name="proverbs_27_7"></a>Proverbs 27:7

The [śāḇēaʿ](../../strongs/h/h7649.md) [nephesh](../../strongs/h/h5315.md) [bûs](../../strongs/h/h947.md) a [nōp̄eṯ](../../strongs/h/h5317.md); but to the [rāʿēḇ](../../strongs/h/h7457.md) [nephesh](../../strongs/h/h5315.md) every [mar](../../strongs/h/h4751.md) is [māṯôq](../../strongs/h/h4966.md).

<a name="proverbs_27_8"></a>Proverbs 27:8

As a [tsippowr](../../strongs/h/h6833.md) that [nāḏaḏ](../../strongs/h/h5074.md) from her [qēn](../../strongs/h/h7064.md), so is an ['iysh](../../strongs/h/h376.md) that [nāḏaḏ](../../strongs/h/h5074.md) from his [maqowm](../../strongs/h/h4725.md).

<a name="proverbs_27_9"></a>Proverbs 27:9

[šemen](../../strongs/h/h8081.md) and [qᵊṭōreṯ](../../strongs/h/h7004.md) [samach](../../strongs/h/h8055.md) the [leb](../../strongs/h/h3820.md): so doth the [meṯeq](../../strongs/h/h4986.md) of a [rea'](../../strongs/h/h7453.md) by [nephesh](../../strongs/h/h5315.md) ['etsah](../../strongs/h/h6098.md).

<a name="proverbs_27_10"></a>Proverbs 27:10

Thine own [rea'](../../strongs/h/h7453.md), and thy ['ab](../../strongs/h/h1.md) [rea'](../../strongs/h/h7453.md), ['azab](../../strongs/h/h5800.md) not; neither [bow'](../../strongs/h/h935.md) into thy ['ach](../../strongs/h/h251.md) [bayith](../../strongs/h/h1004.md) in the [yowm](../../strongs/h/h3117.md) of thy ['êḏ](../../strongs/h/h343.md): for [towb](../../strongs/h/h2896.md) is a [šāḵēn](../../strongs/h/h7934.md) that is [qarowb](../../strongs/h/h7138.md) than an ['ach](../../strongs/h/h251.md) far [rachowq](../../strongs/h/h7350.md).

<a name="proverbs_27_11"></a>Proverbs 27:11

My [ben](../../strongs/h/h1121.md), be [ḥāḵam](../../strongs/h/h2449.md), and make my [leb](../../strongs/h/h3820.md) [samach](../../strongs/h/h8055.md), that I may [shuwb](../../strongs/h/h7725.md) [dabar](../../strongs/h/h1697.md) him that [ḥārap̄](../../strongs/h/h2778.md) me.

<a name="proverbs_27_12"></a>Proverbs 27:12

An ['aruwm](../../strongs/h/h6175.md) [ra'ah](../../strongs/h/h7200.md) the [ra'](../../strongs/h/h7451.md), and [cathar](../../strongs/h/h5641.md) himself; but the [pᵊṯî](../../strongs/h/h6612.md) ['abar](../../strongs/h/h5674.md), and are [ʿānaš](../../strongs/h/h6064.md).

<a name="proverbs_27_13"></a>Proverbs 27:13

[laqach](../../strongs/h/h3947.md) his [beḡeḏ](../../strongs/h/h899.md) that is [ʿāraḇ](../../strongs/h/h6148.md) for a [zûr](../../strongs/h/h2114.md), and take a [chabal](../../strongs/h/h2254.md) of him for a [nāḵrî](../../strongs/h/h5237.md).

<a name="proverbs_27_14"></a>Proverbs 27:14

He that [barak](../../strongs/h/h1288.md) his [rea'](../../strongs/h/h7453.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md), [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), it shall be [chashab](../../strongs/h/h2803.md) a [qᵊlālâ](../../strongs/h/h7045.md) to him.

<a name="proverbs_27_15"></a>Proverbs 27:15

A [ṭāraḏ](../../strongs/h/h2956.md) [delep̄](../../strongs/h/h1812.md) in a [saḡrîr](../../strongs/h/h5464.md) [yowm](../../strongs/h/h3117.md) and a [miḏyān](../../strongs/h/h4079.md) [māḏôn](../../strongs/h/h4066.md) ['ishshah](../../strongs/h/h802.md) are [šāvâ](../../strongs/h/h7737.md).

<a name="proverbs_27_16"></a>Proverbs 27:16

Whosoever [tsaphan](../../strongs/h/h6845.md) her [tsaphan](../../strongs/h/h6845.md) the [ruwach](../../strongs/h/h7307.md), and the [šemen](../../strongs/h/h8081.md) of his [yamiyn](../../strongs/h/h3225.md), which [qara'](../../strongs/h/h7121.md) itself.

<a name="proverbs_27_17"></a>Proverbs 27:17

[barzel](../../strongs/h/h1270.md) [ḥāḏaḏ](../../strongs/h/h2300.md) [barzel](../../strongs/h/h1270.md); so an ['iysh](../../strongs/h/h376.md) [ḥāḏaḏ](../../strongs/h/h2300.md) the [paniym](../../strongs/h/h6440.md) of his [rea'](../../strongs/h/h7453.md).

<a name="proverbs_27_18"></a>Proverbs 27:18

Whoso [nāṣar](../../strongs/h/h5341.md) the [tĕ'en](../../strongs/h/h8384.md) shall ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) thereof: so he that [shamar](../../strongs/h/h8104.md) on his ['adown](../../strongs/h/h113.md) shall be [kabad](../../strongs/h/h3513.md).

<a name="proverbs_27_19"></a>Proverbs 27:19

As in [mayim](../../strongs/h/h4325.md) [paniym](../../strongs/h/h6440.md) answereth to [paniym](../../strongs/h/h6440.md), so the [leb](../../strongs/h/h3820.md) of ['āḏām](../../strongs/h/h120.md) to ['āḏām](../../strongs/h/h120.md).

<a name="proverbs_27_20"></a>Proverbs 27:20

[shĕ'owl](../../strongs/h/h7585.md) and ['Ăḇadôn](../../strongs/h/h11.md) H10 are [lō'](../../strongs/h/h3808.md) [sāׂbaʿ](../../strongs/h/h7646.md); so the ['ayin](../../strongs/h/h5869.md) of ['āḏām](../../strongs/h/h120.md) are never [sāׂbaʿ](../../strongs/h/h7646.md).

<a name="proverbs_27_21"></a>Proverbs 27:21

As the [maṣrēp̄](../../strongs/h/h4715.md) for [keceph](../../strongs/h/h3701.md), and the [kûr](../../strongs/h/h3564.md) for [zāhāḇ](../../strongs/h/h2091.md); so is an ['iysh](../../strongs/h/h376.md) [peh](../../strongs/h/h6310.md) his [mahălāl](../../strongs/h/h4110.md).

<a name="proverbs_27_22"></a>Proverbs 27:22

Though thou shouldest [kāṯaš](../../strongs/h/h3806.md) an ['ĕvîl](../../strongs/h/h191.md) in a [maḵtēš](../../strongs/h/h4388.md) [tavek](../../strongs/h/h8432.md) [rîp̄ôṯ](../../strongs/h/h7383.md) with a [ʿĕlî](../../strongs/h/h5940.md), yet will not his ['iûeleṯ](../../strongs/h/h200.md) [cuwr](../../strongs/h/h5493.md) from him.

<a name="proverbs_27_23"></a>Proverbs 27:23

Be thou [yada'](../../strongs/h/h3045.md) to [yada'](../../strongs/h/h3045.md) the [paniym](../../strongs/h/h6440.md) of thy [tso'n](../../strongs/h/h6629.md), and [shiyth](../../strongs/h/h7896.md) [leb](../../strongs/h/h3820.md) to thy [ʿēḏer](../../strongs/h/h5739.md).

<a name="proverbs_27_24"></a>Proverbs 27:24

For [ḥōsen](../../strongs/h/h2633.md) are not ['owlam](../../strongs/h/h5769.md): and doth the [nēzer](../../strongs/h/h5145.md) endure to [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md)?

<a name="proverbs_27_25"></a>Proverbs 27:25

The [chatsiyr](../../strongs/h/h2682.md) [gālâ](../../strongs/h/h1540.md), and the [deše'](../../strongs/h/h1877.md) [ra'ah](../../strongs/h/h7200.md) itself, and ['eseb](../../strongs/h/h6212.md) of the [har](../../strongs/h/h2022.md) are ['āsap̄](../../strongs/h/h622.md).

<a name="proverbs_27_26"></a>Proverbs 27:26

The [keḇeś](../../strongs/h/h3532.md) are for thy [lᵊḇûš](../../strongs/h/h3830.md), and the [ʿatûḏ](../../strongs/h/h6260.md) are the [mᵊḥîr](../../strongs/h/h4242.md) of the [sadeh](../../strongs/h/h7704.md).

<a name="proverbs_27_27"></a>Proverbs 27:27

And thou shalt have [ʿēz](../../strongs/h/h5795.md) [chalab](../../strongs/h/h2461.md) [day](../../strongs/h/h1767.md) for thy [lechem](../../strongs/h/h3899.md), for the [lechem](../../strongs/h/h3899.md) of thy [bayith](../../strongs/h/h1004.md), and for the [chay](../../strongs/h/h2416.md) for thy [naʿărâ](../../strongs/h/h5291.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 26](proverbs_26.md) - [Proverbs 28](proverbs_28.md)