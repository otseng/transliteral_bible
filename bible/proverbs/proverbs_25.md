# [Proverbs 25](https://www.blueletterbible.org/kjv/proverbs/25)

<a name="proverbs_25_1"></a>Proverbs 25:1

These are also [māšāl](../../strongs/h/h4912.md) of [Šᵊlōmô](../../strongs/h/h8010.md), which the ['enowsh](../../strongs/h/h582.md) of [Ḥizqîyâ](../../strongs/h/h2396.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) ['athaq](../../strongs/h/h6275.md).

<a name="proverbs_25_2"></a>Proverbs 25:2

It is the [kabowd](../../strongs/h/h3519.md) of ['Elohiym](../../strongs/h/h430.md) to [cathar](../../strongs/h/h5641.md) a [dabar](../../strongs/h/h1697.md): but the [kabowd](../../strongs/h/h3519.md) of [melek](../../strongs/h/h4428.md) is to [chaqar](../../strongs/h/h2713.md) a [dabar](../../strongs/h/h1697.md).

<a name="proverbs_25_3"></a>Proverbs 25:3

The [shamayim](../../strongs/h/h8064.md) for [rûm](../../strongs/h/h7312.md), and the ['erets](../../strongs/h/h776.md) for [ʿōmeq](../../strongs/h/h6011.md), and the [leb](../../strongs/h/h3820.md) of [melek](../../strongs/h/h4428.md) is [ḥēqer](../../strongs/h/h2714.md).

<a name="proverbs_25_4"></a>Proverbs 25:4

[hāḡâ](../../strongs/h/h1898.md) the [sîḡ](../../strongs/h/h5509.md) from the [keceph](../../strongs/h/h3701.md), and there shall [yāṣā'](../../strongs/h/h3318.md) a [kĕliy](../../strongs/h/h3627.md) for the [tsaraph](../../strongs/h/h6884.md).

<a name="proverbs_25_5"></a>Proverbs 25:5

[hāḡâ](../../strongs/h/h1898.md) the [rasha'](../../strongs/h/h7563.md) from [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md), and his [kicce'](../../strongs/h/h3678.md) shall be [kuwn](../../strongs/h/h3559.md) in [tsedeq](../../strongs/h/h6664.md).

<a name="proverbs_25_6"></a>Proverbs 25:6

[hāḏar](../../strongs/h/h1921.md) not thyself in the [paniym](../../strongs/h/h6440.md) of the [melek](../../strongs/h/h4428.md), and ['amad](../../strongs/h/h5975.md) not in the [maqowm](../../strongs/h/h4725.md) of [gadowl](../../strongs/h/h1419.md) men:

<a name="proverbs_25_7"></a>Proverbs 25:7

For [towb](../../strongs/h/h2896.md) it is that it be ['āmar](../../strongs/h/h559.md) unto thee, [ʿālâ](../../strongs/h/h5927.md) hither; than that thou shouldest be put [šāp̄ēl](../../strongs/h/h8213.md) in the [paniym](../../strongs/h/h6440.md) of the [nāḏîḇ](../../strongs/h/h5081.md) whom thine ['ayin](../../strongs/h/h5869.md) have [ra'ah](../../strongs/h/h7200.md).

<a name="proverbs_25_8"></a>Proverbs 25:8

[yāṣā'](../../strongs/h/h3318.md) not [mahēr](../../strongs/h/h4118.md) to [riyb](../../strongs/h/h7378.md), lest thou know not what to ['asah](../../strongs/h/h6213.md) in the ['aḥărîṯ](../../strongs/h/h319.md) thereof, when thy [rea'](../../strongs/h/h7453.md) hath put thee to [kālam](../../strongs/h/h3637.md).

<a name="proverbs_25_9"></a>Proverbs 25:9

[riyb](../../strongs/h/h7378.md) thy [rîḇ](../../strongs/h/h7379.md) with thy [rea'](../../strongs/h/h7453.md) himself; and [gālâ](../../strongs/h/h1540.md) not a [sôḏ](../../strongs/h/h5475.md) to ['aḥēr](../../strongs/h/h312.md):

<a name="proverbs_25_10"></a>Proverbs 25:10

Lest he that [shama'](../../strongs/h/h8085.md) it put thee to [ḥāsaḏ](../../strongs/h/h2616.md), and thine [dibâ](../../strongs/h/h1681.md) turn not [shuwb](../../strongs/h/h7725.md).

<a name="proverbs_25_11"></a>Proverbs 25:11

A [dabar](../../strongs/h/h1697.md) ['ôp̄ān](../../strongs/h/h212.md) H655 [dabar](../../strongs/h/h1696.md) is like [tapûaḥ](../../strongs/h/h8598.md) of [zāhāḇ](../../strongs/h/h2091.md) in [maśkîṯ](../../strongs/h/h4906.md) of [keceph](../../strongs/h/h3701.md).

<a name="proverbs_25_12"></a>Proverbs 25:12

As a [nezem](../../strongs/h/h5141.md) of [zāhāḇ](../../strongs/h/h2091.md), and an [ḥălî](../../strongs/h/h2481.md) of fine [keṯem](../../strongs/h/h3800.md), so is a [ḥāḵām](../../strongs/h/h2450.md) [yakach](../../strongs/h/h3198.md) upon a [shama'](../../strongs/h/h8085.md) ['ozen](../../strongs/h/h241.md).

<a name="proverbs_25_13"></a>Proverbs 25:13

As the [tsinnah](../../strongs/h/h6793.md) of [šeleḡ](../../strongs/h/h7950.md) in the [yowm](../../strongs/h/h3117.md) of [qāṣîr](../../strongs/h/h7105.md), so is an ['aman](../../strongs/h/h539.md) [ṣîr](../../strongs/h/h6735.md) to them that [shalach](../../strongs/h/h7971.md) him: for he [shuwb](../../strongs/h/h7725.md) the [nephesh](../../strongs/h/h5315.md) of his ['adown](../../strongs/h/h113.md).

<a name="proverbs_25_14"></a>Proverbs 25:14

['iysh](../../strongs/h/h376.md) [halal](../../strongs/h/h1984.md) himself of a [sheqer](../../strongs/h/h8267.md) [mataṯ](../../strongs/h/h4991.md) is like [nāśî'](../../strongs/h/h5387.md) and [ruwach](../../strongs/h/h7307.md) without [gešem](../../strongs/h/h1653.md).

<a name="proverbs_25_15"></a>Proverbs 25:15

By ['ōreḵ](../../strongs/h/h753.md) ['aph](../../strongs/h/h639.md) is a [qāṣîn](../../strongs/h/h7101.md) [pāṯâ](../../strongs/h/h6601.md), and a [raḵ](../../strongs/h/h7390.md) [lashown](../../strongs/h/h3956.md) [shabar](../../strongs/h/h7665.md) the [gerem](../../strongs/h/h1634.md).

<a name="proverbs_25_16"></a>Proverbs 25:16

Hast thou [māṣā'](../../strongs/h/h4672.md) [dĕbash](../../strongs/h/h1706.md)? ['akal](../../strongs/h/h398.md) so much as is [day](../../strongs/h/h1767.md) for thee, lest thou be [sāׂbaʿ](../../strongs/h/h7646.md) therewith, and [qî'](../../strongs/h/h6958.md) it.

<a name="proverbs_25_17"></a>Proverbs 25:17

[yāqar](../../strongs/h/h3365.md) thy [regel](../../strongs/h/h7272.md) from thy [rea'](../../strongs/h/h7453.md) [bayith](../../strongs/h/h1004.md); lest he be [sāׂbaʿ](../../strongs/h/h7646.md) of thee, and so [sane'](../../strongs/h/h8130.md) thee.

<a name="proverbs_25_18"></a>Proverbs 25:18

An ['iysh](../../strongs/h/h376.md) that ['anah](../../strongs/h/h6030.md) [sheqer](../../strongs/h/h8267.md) ['ed](../../strongs/h/h5707.md) against his [rea'](../../strongs/h/h7453.md) is a [mēp̄îṣ](../../strongs/h/h4650.md), and a [chereb](../../strongs/h/h2719.md), and a [šānan](../../strongs/h/h8150.md) [chets](../../strongs/h/h2671.md).

<a name="proverbs_25_19"></a>Proverbs 25:19

[miḇṭāḥ](../../strongs/h/h4009.md) in a [bāḡaḏ](../../strongs/h/h898.md) in [yowm](../../strongs/h/h3117.md) of [tsarah](../../strongs/h/h6869.md) is like a [rōʿâ](../../strongs/h/h7465.md) [šēn](../../strongs/h/h8127.md), and a [regel](../../strongs/h/h7272.md) out of [mûʿāḏeṯ](../../strongs/h/h4154.md).

<a name="proverbs_25_20"></a>Proverbs 25:20

As he that [ʿāḏâ](../../strongs/h/h5710.md) a [beḡeḏ](../../strongs/h/h899.md) in [qārâ](../../strongs/h/h7135.md) [yowm](../../strongs/h/h3117.md), and as [ḥōmeṣ](../../strongs/h/h2558.md) upon [neṯer](../../strongs/h/h5427.md), so is he that [shiyr](../../strongs/h/h7891.md) [šîr](../../strongs/h/h7892.md) to a [ra'](../../strongs/h/h7451.md) [leb](../../strongs/h/h3820.md).

<a name="proverbs_25_21"></a>Proverbs 25:21

If thine [sane'](../../strongs/h/h8130.md) be [rāʿēḇ](../../strongs/h/h7457.md), give him [lechem](../../strongs/h/h3899.md) to ['akal](../../strongs/h/h398.md); and if he be [ṣāmē'](../../strongs/h/h6771.md), give him [mayim](../../strongs/h/h4325.md) to [šāqâ](../../strongs/h/h8248.md):

<a name="proverbs_25_22"></a>Proverbs 25:22

For thou shalt [ḥāṯâ](../../strongs/h/h2846.md) [gechel](../../strongs/h/h1513.md) upon his [ro'sh](../../strongs/h/h7218.md), and [Yĕhovah](../../strongs/h/h3068.md) shall [shalam](../../strongs/h/h7999.md) thee.

<a name="proverbs_25_23"></a>Proverbs 25:23

The [ṣāp̄ôn](../../strongs/h/h6828.md) [ruwach](../../strongs/h/h7307.md) driveth [chuwl](../../strongs/h/h2342.md) [gešem](../../strongs/h/h1653.md): so doth a [za'am](../../strongs/h/h2194.md) [paniym](../../strongs/h/h6440.md) a [cether](../../strongs/h/h5643.md) [lashown](../../strongs/h/h3956.md).

<a name="proverbs_25_24"></a>Proverbs 25:24

It is [towb](../../strongs/h/h2896.md) to [yashab](../../strongs/h/h3427.md) in the [pinnâ](../../strongs/h/h6438.md) of the [gāḡ](../../strongs/h/h1406.md), than with a [miḏyān](../../strongs/h/h4079.md) [māḏôn](../../strongs/h/h4066.md) ['ishshah](../../strongs/h/h802.md) and in a [Ḥeḇer](../../strongs/h/h2267.md) [bayith](../../strongs/h/h1004.md).

<a name="proverbs_25_25"></a>Proverbs 25:25

As [qar](../../strongs/h/h7119.md) [mayim](../../strongs/h/h4325.md) to a [ʿāyēp̄](../../strongs/h/h5889.md) [nephesh](../../strongs/h/h5315.md), so is [towb](../../strongs/h/h2896.md) [šᵊmûʿâ](../../strongs/h/h8052.md) from a [merḥāq](../../strongs/h/h4801.md) ['erets](../../strongs/h/h776.md).

<a name="proverbs_25_26"></a>Proverbs 25:26

A [tsaddiyq](../../strongs/h/h6662.md) man falling [mowt](../../strongs/h/h4131.md) [paniym](../../strongs/h/h6440.md) the [rasha'](../../strongs/h/h7563.md) is as a [rāp̄aś](../../strongs/h/h7515.md) [maʿyān](../../strongs/h/h4599.md), and a [shachath](../../strongs/h/h7843.md) [māqôr](../../strongs/h/h4726.md).

<a name="proverbs_25_27"></a>Proverbs 25:27

It is not [towb](../../strongs/h/h2896.md) to ['akal](../../strongs/h/h398.md) [rabah](../../strongs/h/h7235.md) [dĕbash](../../strongs/h/h1706.md): so for men to [ḥēqer](../../strongs/h/h2714.md) their own [kabowd](../../strongs/h/h3519.md) is not [kabowd](../../strongs/h/h3519.md).

<a name="proverbs_25_28"></a>Proverbs 25:28

['iysh](../../strongs/h/h376.md) that hath no [maʿṣār](../../strongs/h/h4623.md) over his own [ruwach](../../strongs/h/h7307.md) is like a [ʿîr](../../strongs/h/h5892.md) that is [pāraṣ](../../strongs/h/h6555.md), and without [ḥômâ](../../strongs/h/h2346.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 24](proverbs_24.md) - [Proverbs 26](proverbs_26.md)