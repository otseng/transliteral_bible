# [Proverbs 26](https://www.blueletterbible.org/kjv/proverbs/26)

<a name="proverbs_26_1"></a>Proverbs 26:1

As [šeleḡ](../../strongs/h/h7950.md) in [qayiṣ](../../strongs/h/h7019.md), and as [māṭār](../../strongs/h/h4306.md) in [qāṣîr](../../strongs/h/h7105.md), so [kabowd](../../strongs/h/h3519.md) is not [nā'vê](../../strongs/h/h5000.md) for a [kᵊsîl](../../strongs/h/h3684.md).

<a name="proverbs_26_2"></a>Proverbs 26:2

As the [tsippowr](../../strongs/h/h6833.md) by [nuwd](../../strongs/h/h5110.md), as the [dᵊrôr](../../strongs/h/h1866.md) by ['uwph](../../strongs/h/h5774.md), so the [qᵊlālâ](../../strongs/h/h7045.md) [ḥinnām](../../strongs/h/h2600.md) shall not [bow'](../../strongs/h/h935.md).

<a name="proverbs_26_3"></a>Proverbs 26:3

A [šôṭ](../../strongs/h/h7752.md) for the [sûs](../../strongs/h/h5483.md), a [meṯeḡ](../../strongs/h/h4964.md) for the [chamowr](../../strongs/h/h2543.md), and a [shebet](../../strongs/h/h7626.md) for the [kᵊsîl](../../strongs/h/h3684.md) [gēv](../../strongs/h/h1460.md).

<a name="proverbs_26_4"></a>Proverbs 26:4

['anah](../../strongs/h/h6030.md) not a [kᵊsîl](../../strongs/h/h3684.md) according to his ['iûeleṯ](../../strongs/h/h200.md), lest thou also be [šāvâ](../../strongs/h/h7737.md) unto him.

<a name="proverbs_26_5"></a>Proverbs 26:5

['anah](../../strongs/h/h6030.md) a [kᵊsîl](../../strongs/h/h3684.md) according to his ['iûeleṯ](../../strongs/h/h200.md), lest he be [ḥāḵām](../../strongs/h/h2450.md) in his own ['ayin](../../strongs/h/h5869.md).

<a name="proverbs_26_6"></a>Proverbs 26:6

He that [shalach](../../strongs/h/h7971.md) a [dabar](../../strongs/h/h1697.md) by the [yad](../../strongs/h/h3027.md) of a [kᵊsîl](../../strongs/h/h3684.md) [qāṣâ](../../strongs/h/h7096.md) the [regel](../../strongs/h/h7272.md), and [šāṯâ](../../strongs/h/h8354.md) [chamac](../../strongs/h/h2555.md).

<a name="proverbs_26_7"></a>Proverbs 26:7

The [šôq](../../strongs/h/h7785.md) of the [pissēaḥ](../../strongs/h/h6455.md) are not [dālal](../../strongs/h/h1809.md): so is a [māšāl](../../strongs/h/h4912.md) in the [peh](../../strongs/h/h6310.md) of [kᵊsîl](../../strongs/h/h3684.md).

<a name="proverbs_26_8"></a>Proverbs 26:8

As he that [ṣᵊrôr](../../strongs/h/h6872.md) [tsarar](../../strongs/h/h6887.md) an ['eben](../../strongs/h/h68.md) in a [margēmâ](../../strongs/h/h4773.md), so is he that [nathan](../../strongs/h/h5414.md) [kabowd](../../strongs/h/h3519.md) to a [kᵊsîl](../../strongs/h/h3684.md).

<a name="proverbs_26_9"></a>Proverbs 26:9

As a [ḥôaḥ](../../strongs/h/h2336.md) [ʿālâ](../../strongs/h/h5927.md) into the [yad](../../strongs/h/h3027.md) of a [šikôr](../../strongs/h/h7910.md), so is a [māšāl](../../strongs/h/h4912.md) in the [peh](../../strongs/h/h6310.md) of [kᵊsîl](../../strongs/h/h3684.md).

<a name="proverbs_26_10"></a>Proverbs 26:10

The [rab](../../strongs/h/h7227.md) that [chuwl](../../strongs/h/h2342.md) all things both [śāḵar](../../strongs/h/h7936.md) the [kᵊsîl](../../strongs/h/h3684.md), and [śāḵar](../../strongs/h/h7936.md) ['abar](../../strongs/h/h5674.md).

<a name="proverbs_26_11"></a>Proverbs 26:11

As a [keleḇ](../../strongs/h/h3611.md) [shuwb](../../strongs/h/h7725.md) to his [qē'](../../strongs/h/h6892.md), so a [kᵊsîl](../../strongs/h/h3684.md) [šānâ](../../strongs/h/h8138.md) to his ['iûeleṯ](../../strongs/h/h200.md).

<a name="proverbs_26_12"></a>Proverbs 26:12

[ra'ah](../../strongs/h/h7200.md) thou an ['iysh](../../strongs/h/h376.md) [ḥāḵām](../../strongs/h/h2450.md) in his own ['ayin](../../strongs/h/h5869.md)? there is more [tiqvâ](../../strongs/h/h8615.md) of a [kᵊsîl](../../strongs/h/h3684.md) than of him.

<a name="proverbs_26_13"></a>Proverbs 26:13

The [ʿāṣēl](../../strongs/h/h6102.md) ['āmar](../../strongs/h/h559.md), There is a [šāḥal](../../strongs/h/h7826.md) in the [derek](../../strongs/h/h1870.md); an ['ariy](../../strongs/h/h738.md) is in the [rᵊḥōḇ](../../strongs/h/h7339.md).

<a name="proverbs_26_14"></a>Proverbs 26:14

As the [deleṯ](../../strongs/h/h1817.md) [cabab](../../strongs/h/h5437.md) upon his [ṣîr](../../strongs/h/h6735.md), so doth the [ʿāṣēl](../../strongs/h/h6102.md) upon his [mittah](../../strongs/h/h4296.md).

<a name="proverbs_26_15"></a>Proverbs 26:15

The [ʿāṣēl](../../strongs/h/h6102.md) [taman](../../strongs/h/h2934.md) his [yad](../../strongs/h/h3027.md) in his [ṣallaḥaṯ](../../strongs/h/h6747.md); it [lā'â](../../strongs/h/h3811.md) him to bring it [shuwb](../../strongs/h/h7725.md) to his [peh](../../strongs/h/h6310.md).

<a name="proverbs_26_16"></a>Proverbs 26:16

The [ʿāṣēl](../../strongs/h/h6102.md) is [ḥāḵām](../../strongs/h/h2450.md) in his own ['ayin](../../strongs/h/h5869.md) than [šeḇaʿ](../../strongs/h/h7651.md) that can [shuwb](../../strongs/h/h7725.md) a [ṭaʿam](../../strongs/h/h2940.md).

<a name="proverbs_26_17"></a>Proverbs 26:17

He that ['abar](../../strongs/h/h5674.md), and ['abar](../../strongs/h/h5674.md) with [rîḇ](../../strongs/h/h7379.md) belonging not to him, is like one that [ḥāzaq](../../strongs/h/h2388.md) a [keleḇ](../../strongs/h/h3611.md) by the ['ozen](../../strongs/h/h241.md).

<a name="proverbs_26_18"></a>Proverbs 26:18

As a [lāhâ](../../strongs/h/h3856.md) who [yārâ](../../strongs/h/h3384.md) [zîqôṯ](../../strongs/h/h2131.md), [chets](../../strongs/h/h2671.md), and [maveth](../../strongs/h/h4194.md),

<a name="proverbs_26_19"></a>Proverbs 26:19

So is the ['iysh](../../strongs/h/h376.md) that [rāmâ](../../strongs/h/h7411.md) his [rea'](../../strongs/h/h7453.md), and ['āmar](../../strongs/h/h559.md), Am not I in [śāḥaq](../../strongs/h/h7832.md)?

<a name="proverbs_26_20"></a>Proverbs 26:20

Where ['ep̄es](../../strongs/h/h657.md) ['ets](../../strongs/h/h6086.md) is, there the ['esh](../../strongs/h/h784.md) [kāḇâ](../../strongs/h/h3518.md): so where there is no [nirgān](../../strongs/h/h5372.md), the [māḏôn](../../strongs/h/h4066.md) [šāṯaq](../../strongs/h/h8367.md).

<a name="proverbs_26_21"></a>Proverbs 26:21

As [peḥām](../../strongs/h/h6352.md) are to [gechel](../../strongs/h/h1513.md), and ['ets](../../strongs/h/h6086.md) to ['esh](../../strongs/h/h784.md); so is a [miḏyān](../../strongs/h/h4079.md) [māḏôn](../../strongs/h/h4066.md) ['iysh](../../strongs/h/h376.md) to [ḥārar](../../strongs/h/h2787.md) [rîḇ](../../strongs/h/h7379.md).

<a name="proverbs_26_22"></a>Proverbs 26:22

The [dabar](../../strongs/h/h1697.md) of a [nirgān](../../strongs/h/h5372.md) are as [lāhēm](../../strongs/h/h3859.md), and they [yarad](../../strongs/h/h3381.md) into the [ḥeḏer](../../strongs/h/h2315.md) of the [beten](../../strongs/h/h990.md).

<a name="proverbs_26_23"></a>Proverbs 26:23

[dalaq](../../strongs/h/h1814.md) [saphah](../../strongs/h/h8193.md) and a [ra'](../../strongs/h/h7451.md) [leb](../../strongs/h/h3820.md) are like a [ḥereś](../../strongs/h/h2789.md) [ṣāp̄â](../../strongs/h/h6823.md) with [keceph](../../strongs/h/h3701.md) [sîḡ](../../strongs/h/h5509.md).

<a name="proverbs_26_24"></a>Proverbs 26:24

He that [sane'](../../strongs/h/h8130.md) [nāḵar](../../strongs/h/h5234.md) with his [saphah](../../strongs/h/h8193.md), and [shiyth](../../strongs/h/h7896.md) [mirmah](../../strongs/h/h4820.md) [qereḇ](../../strongs/h/h7130.md) him;

<a name="proverbs_26_25"></a>Proverbs 26:25

When he [qowl](../../strongs/h/h6963.md) [chanan](../../strongs/h/h2603.md), ['aman](../../strongs/h/h539.md) him not: for there are [šeḇaʿ](../../strongs/h/h7651.md) [tôʿēḇâ](../../strongs/h/h8441.md) in his [leb](../../strongs/h/h3820.md).

<a name="proverbs_26_26"></a>Proverbs 26:26

Whose [śin'â](../../strongs/h/h8135.md) is [kāsâ](../../strongs/h/h3680.md) by [maššā'ôn](../../strongs/h/h4860.md), his [ra'](../../strongs/h/h7451.md) shall be [gālâ](../../strongs/h/h1540.md) before the [qāhēl](../../strongs/h/h6951.md).

<a name="proverbs_26_27"></a>Proverbs 26:27

Whoso [karah](../../strongs/h/h3738.md) a [shachath](../../strongs/h/h7845.md) shall [naphal](../../strongs/h/h5307.md) therein: and he that [gālal](../../strongs/h/h1556.md) an ['eben](../../strongs/h/h68.md), it will [shuwb](../../strongs/h/h7725.md) upon him.

<a name="proverbs_26_28"></a>Proverbs 26:28

A [sheqer](../../strongs/h/h8267.md) [lashown](../../strongs/h/h3956.md) [sane'](../../strongs/h/h8130.md) those that are [dak](../../strongs/h/h1790.md) by it; and a [ḥālāq](../../strongs/h/h2509.md) [peh](../../strongs/h/h6310.md) ['asah](../../strongs/h/h6213.md) [maḏḥê](../../strongs/h/h4072.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 25](proverbs_25.md) - [Proverbs 27](proverbs_27.md)