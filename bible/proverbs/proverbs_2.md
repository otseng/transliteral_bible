# [Proverbs 2](https://www.blueletterbible.org/kjv/proverbs/2)

<a name="proverbs_2_1"></a>Proverbs 2:1

My [ben](../../strongs/h/h1121.md), if thou wilt [laqach](../../strongs/h/h3947.md) my ['emer](../../strongs/h/h561.md), and [tsaphan](../../strongs/h/h6845.md) my [mitsvah](../../strongs/h/h4687.md) with thee;

<a name="proverbs_2_2"></a>Proverbs 2:2

So that thou [qashab](../../strongs/h/h7181.md) thine ['ozen](../../strongs/h/h241.md) unto [ḥāḵmâ](../../strongs/h/h2451.md), and [natah](../../strongs/h/h5186.md) thine [leb](../../strongs/h/h3820.md) to [tāḇûn](../../strongs/h/h8394.md);

<a name="proverbs_2_3"></a>Proverbs 2:3

Yea, if thou [qara'](../../strongs/h/h7121.md) after [bînâ](../../strongs/h/h998.md), and [nathan](../../strongs/h/h5414.md) thy [qowl](../../strongs/h/h6963.md) for [tāḇûn](../../strongs/h/h8394.md);

<a name="proverbs_2_4"></a>Proverbs 2:4

If thou [bāqaš](../../strongs/h/h1245.md) her as [keceph](../../strongs/h/h3701.md), and [ḥāp̄aś](../../strongs/h/h2664.md) for her as for [maṭmôn](../../strongs/h/h4301.md);

<a name="proverbs_2_5"></a>Proverbs 2:5

Then shalt thou [bîn](../../strongs/h/h995.md) the [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md), and [māṣā'](../../strongs/h/h4672.md) the [da'ath](../../strongs/h/h1847.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="proverbs_2_6"></a>Proverbs 2:6

For [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) [ḥāḵmâ](../../strongs/h/h2451.md): out of his [peh](../../strongs/h/h6310.md) cometh [da'ath](../../strongs/h/h1847.md) and [tāḇûn](../../strongs/h/h8394.md).

<a name="proverbs_2_7"></a>Proverbs 2:7

He [tsaphan](../../strongs/h/h6845.md) [tsaphan](../../strongs/h/h6845.md) [tûšîyâ](../../strongs/h/h8454.md) for the [yashar](../../strongs/h/h3477.md): he is a [magen](../../strongs/h/h4043.md) to them that [halak](../../strongs/h/h1980.md) [tom](../../strongs/h/h8537.md).

<a name="proverbs_2_8"></a>Proverbs 2:8

He [nāṣar](../../strongs/h/h5341.md) the ['orach](../../strongs/h/h734.md) of [mishpat](../../strongs/h/h4941.md), and [shamar](../../strongs/h/h8104.md) the [derek](../../strongs/h/h1870.md) of his [chaciyd](../../strongs/h/h2623.md).

<a name="proverbs_2_9"></a>Proverbs 2:9

Then shalt thou [bîn](../../strongs/h/h995.md) [tsedeq](../../strongs/h/h6664.md), and [mishpat](../../strongs/h/h4941.md), and [meyshar](../../strongs/h/h4339.md); yea, every [towb](../../strongs/h/h2896.md) [ma'gal](../../strongs/h/h4570.md).

<a name="proverbs_2_10"></a>Proverbs 2:10

When [ḥāḵmâ](../../strongs/h/h2451.md) [bow'](../../strongs/h/h935.md) into thine [leb](../../strongs/h/h3820.md), and [da'ath](../../strongs/h/h1847.md) is [nāʿēm](../../strongs/h/h5276.md) unto thy [nephesh](../../strongs/h/h5315.md);

<a name="proverbs_2_11"></a>Proverbs 2:11

[mezimmah](../../strongs/h/h4209.md) shall [shamar](../../strongs/h/h8104.md) thee, [tāḇûn](../../strongs/h/h8394.md) shall [nāṣar](../../strongs/h/h5341.md) thee:

<a name="proverbs_2_12"></a>Proverbs 2:12

To [natsal](../../strongs/h/h5337.md) thee from the [derek](../../strongs/h/h1870.md) of the [ra'](../../strongs/h/h7451.md) man, from the ['iysh](../../strongs/h/h376.md) that [dabar](../../strongs/h/h1696.md) [tahpuḵôṯ](../../strongs/h/h8419.md);

<a name="proverbs_2_13"></a>Proverbs 2:13

Who ['azab](../../strongs/h/h5800.md) the ['orach](../../strongs/h/h734.md) of [yōšer](../../strongs/h/h3476.md), to [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of [choshek](../../strongs/h/h2822.md);

<a name="proverbs_2_14"></a>Proverbs 2:14

Who [śāmēaḥ](../../strongs/h/h8056.md) to ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md), and [giyl](../../strongs/h/h1523.md) in the [tahpuḵôṯ](../../strongs/h/h8419.md) of the [ra'](../../strongs/h/h7451.md);

<a name="proverbs_2_15"></a>Proverbs 2:15

Whose ['orach](../../strongs/h/h734.md) are [ʿiqqēš](../../strongs/h/h6141.md), and they [Lûz](../../strongs/h/h3868.md) in their [ma'gal](../../strongs/h/h4570.md):

<a name="proverbs_2_16"></a>Proverbs 2:16

To [natsal](../../strongs/h/h5337.md) thee from the [zûr](../../strongs/h/h2114.md) ['ishshah](../../strongs/h/h802.md), even from the [nāḵrî](../../strongs/h/h5237.md) which [chalaq](../../strongs/h/h2505.md) with her ['emer](../../strongs/h/h561.md);

<a name="proverbs_2_17"></a>Proverbs 2:17

Which ['azab](../../strongs/h/h5800.md) the ['allûp̄](../../strongs/h/h441.md) of her [nāʿur](../../strongs/h/h5271.md), and [shakach](../../strongs/h/h7911.md) the [bĕriyth](../../strongs/h/h1285.md) of her ['Elohiym](../../strongs/h/h430.md).

<a name="proverbs_2_18"></a>Proverbs 2:18

For her [bayith](../../strongs/h/h1004.md) [šûaḥ](../../strongs/h/h7743.md) unto [maveth](../../strongs/h/h4194.md), and her [ma'gal](../../strongs/h/h4570.md) unto the [rᵊp̄ā'îm](../../strongs/h/h7496.md).

<a name="proverbs_2_19"></a>Proverbs 2:19

None that [bow'](../../strongs/h/h935.md) unto her [shuwb](../../strongs/h/h7725.md), neither take they [nāśaḡ](../../strongs/h/h5381.md) of the ['orach](../../strongs/h/h734.md) of [chay](../../strongs/h/h2416.md).

<a name="proverbs_2_20"></a>Proverbs 2:20

That thou mayest [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of [towb](../../strongs/h/h2896.md) men, and [shamar](../../strongs/h/h8104.md) the ['orach](../../strongs/h/h734.md) of the [tsaddiyq](../../strongs/h/h6662.md).

<a name="proverbs_2_21"></a>Proverbs 2:21

For the [yashar](../../strongs/h/h3477.md) shall [shakan](../../strongs/h/h7931.md) in the ['erets](../../strongs/h/h776.md), and the [tamiym](../../strongs/h/h8549.md) shall [yāṯar](../../strongs/h/h3498.md) in it.

<a name="proverbs_2_22"></a>Proverbs 2:22

But the [rasha'](../../strongs/h/h7563.md) shall be [karath](../../strongs/h/h3772.md) from the ['erets](../../strongs/h/h776.md), and the [bāḡaḏ](../../strongs/h/h898.md) shall be [nāsaḥ](../../strongs/h/h5255.md) of it.

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 1](proverbs_1.md) - [Proverbs 3](proverbs_3.md)