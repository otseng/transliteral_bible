# [Proverbs 4](https://www.blueletterbible.org/kjv/proverbs/4)

<a name="proverbs_4_1"></a>Proverbs 4:1

[shama'](../../strongs/h/h8085.md), ye [ben](../../strongs/h/h1121.md), the [mûsār](../../strongs/h/h4148.md) of an ['ab](../../strongs/h/h1.md), and [qashab](../../strongs/h/h7181.md) to [yada'](../../strongs/h/h3045.md) [bînâ](../../strongs/h/h998.md).

<a name="proverbs_4_2"></a>Proverbs 4:2

For I [nathan](../../strongs/h/h5414.md) you [towb](../../strongs/h/h2896.md) [leqaḥ](../../strongs/h/h3948.md), ['azab](../../strongs/h/h5800.md) ye not my [towrah](../../strongs/h/h8451.md).

<a name="proverbs_4_3"></a>Proverbs 4:3

For I was my ['ab](../../strongs/h/h1.md) [ben](../../strongs/h/h1121.md), [raḵ](../../strongs/h/h7390.md) and [yāḥîḏ](../../strongs/h/h3173.md) beloved in the [paniym](../../strongs/h/h6440.md) of my ['em](../../strongs/h/h517.md).

<a name="proverbs_4_4"></a>Proverbs 4:4

He [yārâ](../../strongs/h/h3384.md) me also, and ['āmar](../../strongs/h/h559.md) unto me, Let thine [leb](../../strongs/h/h3820.md) [tamak](../../strongs/h/h8551.md) my [dabar](../../strongs/h/h1697.md): [shamar](../../strongs/h/h8104.md) my [mitsvah](../../strongs/h/h4687.md), and [ḥāyâ](../../strongs/h/h2421.md).

<a name="proverbs_4_5"></a>Proverbs 4:5

[qānâ](../../strongs/h/h7069.md) [ḥāḵmâ](../../strongs/h/h2451.md), [qānâ](../../strongs/h/h7069.md) [bînâ](../../strongs/h/h998.md): [shakach](../../strongs/h/h7911.md) it not; neither [natah](../../strongs/h/h5186.md) from the ['emer](../../strongs/h/h561.md) of my [peh](../../strongs/h/h6310.md).

<a name="proverbs_4_6"></a>Proverbs 4:6

['azab](../../strongs/h/h5800.md) her not, and she shall [shamar](../../strongs/h/h8104.md) thee: ['ahab](../../strongs/h/h157.md) her, and she shall [nāṣar](../../strongs/h/h5341.md) thee.

<a name="proverbs_4_7"></a>Proverbs 4:7

[ḥāḵmâ](../../strongs/h/h2451.md) is the [re'shiyth](../../strongs/h/h7225.md); therefore [qānâ](../../strongs/h/h7069.md) [ḥāḵmâ](../../strongs/h/h2451.md): and with all thy [qinyān](../../strongs/h/h7075.md) get [bînâ](../../strongs/h/h998.md).

<a name="proverbs_4_8"></a>Proverbs 4:8

[sālal](../../strongs/h/h5549.md) her, and she shall [ruwm](../../strongs/h/h7311.md) thee: she shall bring thee to [kabad](../../strongs/h/h3513.md), when thou dost [ḥāḇaq](../../strongs/h/h2263.md) her.

<a name="proverbs_4_9"></a>Proverbs 4:9

She shall [nathan](../../strongs/h/h5414.md) to thine [ro'sh](../../strongs/h/h7218.md) a [livyâ](../../strongs/h/h3880.md) of [ḥēn](../../strongs/h/h2580.md): an [ʿăṭārâ](../../strongs/h/h5850.md) of [tip̄'ārâ](../../strongs/h/h8597.md) shall she [māḡan](../../strongs/h/h4042.md) to thee.

<a name="proverbs_4_10"></a>Proverbs 4:10

[shama'](../../strongs/h/h8085.md), O my [ben](../../strongs/h/h1121.md), and [laqach](../../strongs/h/h3947.md) my ['emer](../../strongs/h/h561.md); and the [šānâ](../../strongs/h/h8141.md) of thy [chay](../../strongs/h/h2416.md) shall be [rabah](../../strongs/h/h7235.md).

<a name="proverbs_4_11"></a>Proverbs 4:11

I have [yārâ](../../strongs/h/h3384.md) thee in the [derek](../../strongs/h/h1870.md) of [ḥāḵmâ](../../strongs/h/h2451.md); I have [dāraḵ](../../strongs/h/h1869.md) thee in [yōšer](../../strongs/h/h3476.md) [ma'gal](../../strongs/h/h4570.md).

<a name="proverbs_4_12"></a>Proverbs 4:12

When thou [yālaḵ](../../strongs/h/h3212.md), thy [ṣaʿaḏ](../../strongs/h/h6806.md) shall not be [yāṣar](../../strongs/h/h3334.md); and when thou [rûṣ](../../strongs/h/h7323.md), thou shalt not [kashal](../../strongs/h/h3782.md).

<a name="proverbs_4_13"></a>Proverbs 4:13

[ḥāzaq](../../strongs/h/h2388.md) of [mûsār](../../strongs/h/h4148.md); let her not [rāp̄â](../../strongs/h/h7503.md): [nāṣar](../../strongs/h/h5341.md) her; for she is thy [chay](../../strongs/h/h2416.md).

<a name="proverbs_4_14"></a>Proverbs 4:14

[bow'](../../strongs/h/h935.md) not into the ['orach](../../strongs/h/h734.md) of the [rasha'](../../strongs/h/h7563.md), and ['āšar](../../strongs/h/h833.md) not in the [derek](../../strongs/h/h1870.md) of [ra'](../../strongs/h/h7451.md) men.

<a name="proverbs_4_15"></a>Proverbs 4:15

[pāraʿ](../../strongs/h/h6544.md) it, ['abar](../../strongs/h/h5674.md) not by it, [śāṭâ](../../strongs/h/h7847.md) from it, and ['abar](../../strongs/h/h5674.md).

<a name="proverbs_4_16"></a>Proverbs 4:16

For they [yashen](../../strongs/h/h3462.md) not, except they have done [ra'a'](../../strongs/h/h7489.md); and their [šēnā'](../../strongs/h/h8142.md) is [gāzal](../../strongs/h/h1497.md), unless they cause some to [kashal](../../strongs/h/h3782.md) [kashal](../../strongs/h/h3782.md).

<a name="proverbs_4_17"></a>Proverbs 4:17

For they [lāḥam](../../strongs/h/h3898.md) the [lechem](../../strongs/h/h3899.md) of [resha'](../../strongs/h/h7562.md), and [šāṯâ](../../strongs/h/h8354.md) the [yayin](../../strongs/h/h3196.md) of [chamac](../../strongs/h/h2555.md).

<a name="proverbs_4_18"></a>Proverbs 4:18

But the ['orach](../../strongs/h/h734.md) of the [tsaddiyq](../../strongs/h/h6662.md) is as the [nogahh](../../strongs/h/h5051.md) ['owr](../../strongs/h/h216.md), that ['owr](../../strongs/h/h215.md) [halak](../../strongs/h/h1980.md) and more unto the [kuwn](../../strongs/h/h3559.md) [yowm](../../strongs/h/h3117.md).

<a name="proverbs_4_19"></a>Proverbs 4:19

The [derek](../../strongs/h/h1870.md) of the [rasha'](../../strongs/h/h7563.md) is as ['ăp̄ēlâ](../../strongs/h/h653.md): they [yada'](../../strongs/h/h3045.md) not at what they [kashal](../../strongs/h/h3782.md).

<a name="proverbs_4_20"></a>Proverbs 4:20

My [ben](../../strongs/h/h1121.md), [qashab](../../strongs/h/h7181.md) to my [dabar](../../strongs/h/h1697.md); [natah](../../strongs/h/h5186.md) thine ['ozen](../../strongs/h/h241.md) unto my ['emer](../../strongs/h/h561.md).

<a name="proverbs_4_21"></a>Proverbs 4:21

Let them not [Lûz](../../strongs/h/h3868.md) from thine ['ayin](../../strongs/h/h5869.md); [shamar](../../strongs/h/h8104.md) them in the [tavek](../../strongs/h/h8432.md) of thine [lebab](../../strongs/h/h3824.md).

<a name="proverbs_4_22"></a>Proverbs 4:22

For they are [chay](../../strongs/h/h2416.md) unto those that [māṣā'](../../strongs/h/h4672.md) them, and [marpē'](../../strongs/h/h4832.md) to all their [basar](../../strongs/h/h1320.md).

<a name="proverbs_4_23"></a>Proverbs 4:23

[nāṣar](../../strongs/h/h5341.md) thy [leb](../../strongs/h/h3820.md) with all [mišmār](../../strongs/h/h4929.md); for out of it are the [tôṣā'ôṯ](../../strongs/h/h8444.md) of [chay](../../strongs/h/h2416.md).

<a name="proverbs_4_24"></a>Proverbs 4:24

[cuwr](../../strongs/h/h5493.md) from thee a [ʿiqqᵊšûṯ](../../strongs/h/h6143.md) [peh](../../strongs/h/h6310.md), and [lᵊzûṯ](../../strongs/h/h3891.md) [saphah](../../strongs/h/h8193.md) put [rachaq](../../strongs/h/h7368.md) from thee.

<a name="proverbs_4_25"></a>Proverbs 4:25

Let thine ['ayin](../../strongs/h/h5869.md) [nabat](../../strongs/h/h5027.md) [nōḵaḥ](../../strongs/h/h5227.md), and let thine ['aph'aph](../../strongs/h/h6079.md) [yashar](../../strongs/h/h3474.md) before thee.

<a name="proverbs_4_26"></a>Proverbs 4:26

[pālas](../../strongs/h/h6424.md) the [ma'gal](../../strongs/h/h4570.md) of thy [regel](../../strongs/h/h7272.md), and let all thy [derek](../../strongs/h/h1870.md) be [kuwn](../../strongs/h/h3559.md).

<a name="proverbs_4_27"></a>Proverbs 4:27

[natah](../../strongs/h/h5186.md) not to the [yamiyn](../../strongs/h/h3225.md) nor to the [śᵊmō'l](../../strongs/h/h8040.md): [cuwr](../../strongs/h/h5493.md) thy [regel](../../strongs/h/h7272.md) from [ra'](../../strongs/h/h7451.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 3](proverbs_3.md) - [Proverbs 5](proverbs_5.md)