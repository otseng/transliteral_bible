# [Proverbs 29](https://www.blueletterbible.org/kjv/proverbs/29)

<a name="proverbs_29_1"></a>Proverbs 29:1

['iysh](../../strongs/h/h376.md), that being often [tôḵēḥâ](../../strongs/h/h8433.md) [qāšâ](../../strongs/h/h7185.md) his [ʿōrep̄](../../strongs/h/h6203.md), shall [peṯaʿ](../../strongs/h/h6621.md) be [shabar](../../strongs/h/h7665.md), and that without [marpē'](../../strongs/h/h4832.md).

<a name="proverbs_29_2"></a>Proverbs 29:2

When the [tsaddiyq](../../strongs/h/h6662.md) are in [rabah](../../strongs/h/h7235.md), the ['am](../../strongs/h/h5971.md) [samach](../../strongs/h/h8055.md): but when the [rasha'](../../strongs/h/h7563.md) [mashal](../../strongs/h/h4910.md), the ['am](../../strongs/h/h5971.md) ['ānaḥ](../../strongs/h/h584.md).

<a name="proverbs_29_3"></a>Proverbs 29:3

['iysh](../../strongs/h/h376.md) ['ahab](../../strongs/h/h157.md) [ḥāḵmâ](../../strongs/h/h2451.md) [samach](../../strongs/h/h8055.md) his ['ab](../../strongs/h/h1.md): but he that [ra'ah](../../strongs/h/h7462.md) with [zānâ](../../strongs/h/h2181.md) ['abad](../../strongs/h/h6.md) his [hôn](../../strongs/h/h1952.md).

<a name="proverbs_29_4"></a>Proverbs 29:4

The [melek](../../strongs/h/h4428.md) by [mishpat](../../strongs/h/h4941.md) ['amad](../../strongs/h/h5975.md) the ['erets](../../strongs/h/h776.md): but ['iysh](../../strongs/h/h376.md) that receiveth [tᵊrûmâ](../../strongs/h/h8641.md) [harac](../../strongs/h/h2040.md) it.

<a name="proverbs_29_5"></a>Proverbs 29:5

A [geḇer](../../strongs/h/h1397.md) that [chalaq](../../strongs/h/h2505.md) his [rea'](../../strongs/h/h7453.md) [pāraś](../../strongs/h/h6566.md) a [rešeṯ](../../strongs/h/h7568.md) for his [pa'am](../../strongs/h/h6471.md).

<a name="proverbs_29_6"></a>Proverbs 29:6

In the [pesha'](../../strongs/h/h6588.md) of a [ra'](../../strongs/h/h7451.md) ['iysh](../../strongs/h/h376.md) there is a [mowqesh](../../strongs/h/h4170.md): but the [tsaddiyq](../../strongs/h/h6662.md) doth [ranan](../../strongs/h/h7442.md) and [śāmēaḥ](../../strongs/h/h8056.md).

<a name="proverbs_29_7"></a>Proverbs 29:7

The [tsaddiyq](../../strongs/h/h6662.md) [yada'](../../strongs/h/h3045.md) the [diyn](../../strongs/h/h1779.md) of the [dal](../../strongs/h/h1800.md): but the [rasha'](../../strongs/h/h7563.md) [bîn](../../strongs/h/h995.md) not to [da'ath](../../strongs/h/h1847.md) it.

<a name="proverbs_29_8"></a>Proverbs 29:8

[lāṣôn](../../strongs/h/h3944.md) ['enowsh](../../strongs/h/h582.md) [puwach](../../strongs/h/h6315.md) a [qiryâ](../../strongs/h/h7151.md) into a [puwach](../../strongs/h/h6315.md): but [ḥāḵām](../../strongs/h/h2450.md) [shuwb](../../strongs/h/h7725.md) ['aph](../../strongs/h/h639.md).

<a name="proverbs_29_9"></a>Proverbs 29:9

If a [ḥāḵām](../../strongs/h/h2450.md) ['iysh](../../strongs/h/h376.md) [shaphat](../../strongs/h/h8199.md) with an ['ĕvîl](../../strongs/h/h191.md) ['iysh](../../strongs/h/h376.md), whether he [ragaz](../../strongs/h/h7264.md) or [śāḥaq](../../strongs/h/h7832.md), there is no [naḥaṯ](../../strongs/h/h5183.md).

<a name="proverbs_29_10"></a>Proverbs 29:10

The ['enowsh](../../strongs/h/h582.md) [dam](../../strongs/h/h1818.md) [sane'](../../strongs/h/h8130.md) the [tām](../../strongs/h/h8535.md): but the [yashar](../../strongs/h/h3477.md) [bāqaš](../../strongs/h/h1245.md) his [nephesh](../../strongs/h/h5315.md).

<a name="proverbs_29_11"></a>Proverbs 29:11

A [kᵊsîl](../../strongs/h/h3684.md) [yāṣā'](../../strongs/h/h3318.md) all his [ruwach](../../strongs/h/h7307.md): but a [ḥāḵām](../../strongs/h/h2450.md) [shabach](../../strongs/h/h7623.md) it in till ['āḥôr](../../strongs/h/h268.md).

<a name="proverbs_29_12"></a>Proverbs 29:12

If a [mashal](../../strongs/h/h4910.md) [qashab](../../strongs/h/h7181.md) to [dabar](../../strongs/h/h1697.md) [sheqer](../../strongs/h/h8267.md), all his [sharath](../../strongs/h/h8334.md) are [rasha'](../../strongs/h/h7563.md).

<a name="proverbs_29_13"></a>Proverbs 29:13

The [rûš](../../strongs/h/h7326.md) and the [ṯḵ](../../strongs/h/h8501.md) ['iysh](../../strongs/h/h376.md) [pāḡaš](../../strongs/h/h6298.md): [Yĕhovah](../../strongs/h/h3068.md) ['owr](../../strongs/h/h215.md) [šᵊnayim](../../strongs/h/h8147.md) their ['ayin](../../strongs/h/h5869.md).

<a name="proverbs_29_14"></a>Proverbs 29:14

The [melek](../../strongs/h/h4428.md) that ['emeth](../../strongs/h/h571.md) [shaphat](../../strongs/h/h8199.md) the [dal](../../strongs/h/h1800.md), his [kicce'](../../strongs/h/h3678.md) shall be [kuwn](../../strongs/h/h3559.md) for [ʿaḏ](../../strongs/h/h5703.md).

<a name="proverbs_29_15"></a>Proverbs 29:15

The [shebet](../../strongs/h/h7626.md) and [tôḵēḥâ](../../strongs/h/h8433.md) [nathan](../../strongs/h/h5414.md) [ḥāḵmâ](../../strongs/h/h2451.md): but a [naʿar](../../strongs/h/h5288.md) [shalach](../../strongs/h/h7971.md) to himself bringeth his ['em](../../strongs/h/h517.md) to [buwsh](../../strongs/h/h954.md).

<a name="proverbs_29_16"></a>Proverbs 29:16

When the [rasha'](../../strongs/h/h7563.md) are [rabah](../../strongs/h/h7235.md), [pesha'](../../strongs/h/h6588.md) [rabah](../../strongs/h/h7235.md): but the [tsaddiyq](../../strongs/h/h6662.md) shall [ra'ah](../../strongs/h/h7200.md) their [mapeleṯ](../../strongs/h/h4658.md).

<a name="proverbs_29_17"></a>Proverbs 29:17

[yacar](../../strongs/h/h3256.md) thy [ben](../../strongs/h/h1121.md), and he shall give thee [nuwach](../../strongs/h/h5117.md); yea, he shall [nathan](../../strongs/h/h5414.md) [maʿăḏān](../../strongs/h/h4574.md) unto thy [nephesh](../../strongs/h/h5315.md).

<a name="proverbs_29_18"></a>Proverbs 29:18

Where there is no [ḥāzôn](../../strongs/h/h2377.md), the ['am](../../strongs/h/h5971.md) [pāraʿ](../../strongs/h/h6544.md): but he that [shamar](../../strongs/h/h8104.md) the [towrah](../../strongs/h/h8451.md), ['esher](../../strongs/h/h835.md) is he.

<a name="proverbs_29_19"></a>Proverbs 29:19

An ['ebed](../../strongs/h/h5650.md) will not be [yacar](../../strongs/h/h3256.md) by [dabar](../../strongs/h/h1697.md): for though he [bîn](../../strongs/h/h995.md) he will not [maʿănê](../../strongs/h/h4617.md).

<a name="proverbs_29_20"></a>Proverbs 29:20

[chazah](../../strongs/h/h2372.md) thou an ['iysh](../../strongs/h/h376.md) that is ['ûṣ](../../strongs/h/h213.md) in his [dabar](../../strongs/h/h1697.md)? there is more [tiqvâ](../../strongs/h/h8615.md) of a [kᵊsîl](../../strongs/h/h3684.md) than of him.

<a name="proverbs_29_21"></a>Proverbs 29:21

He that [pānaq](../../strongs/h/h6445.md) his ['ebed](../../strongs/h/h5650.md) from a [nōʿar](../../strongs/h/h5290.md) shall have him become his [mānôn](../../strongs/h/h4497.md) at the ['aḥărîṯ](../../strongs/h/h319.md).

<a name="proverbs_29_22"></a>Proverbs 29:22

An ['aph](../../strongs/h/h639.md) ['iysh](../../strongs/h/h376.md) stirreth [gārâ](../../strongs/h/h1624.md) [māḏôn](../../strongs/h/h4066.md), and a [chemah](../../strongs/h/h2534.md) [baʿal](../../strongs/h/h1167.md) [rab](../../strongs/h/h7227.md) [pesha'](../../strongs/h/h6588.md).

<a name="proverbs_29_23"></a>Proverbs 29:23

An ['āḏām](../../strongs/h/h120.md) [ga'avah](../../strongs/h/h1346.md) shall bring him [šāp̄ēl](../../strongs/h/h8213.md): but [kabowd](../../strongs/h/h3519.md) shall [tamak](../../strongs/h/h8551.md) the [šāp̄āl](../../strongs/h/h8217.md) in [ruwach](../../strongs/h/h7307.md).

<a name="proverbs_29_24"></a>Proverbs 29:24

Whoso is [chalaq](../../strongs/h/h2505.md) with a [gannāḇ](../../strongs/h/h1590.md) [sane'](../../strongs/h/h8130.md) his own [nephesh](../../strongs/h/h5315.md): he [shama'](../../strongs/h/h8085.md) ['alah](../../strongs/h/h423.md), and [nāḡaḏ](../../strongs/h/h5046.md) it not.

<a name="proverbs_29_25"></a>Proverbs 29:25

The [ḥărāḏâ](../../strongs/h/h2731.md) of ['āḏām](../../strongs/h/h120.md) [nathan](../../strongs/h/h5414.md) a [mowqesh](../../strongs/h/h4170.md): but whoso [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md) shall be [śāḡaḇ](../../strongs/h/h7682.md).

<a name="proverbs_29_26"></a>Proverbs 29:26

[rab](../../strongs/h/h7227.md) [bāqaš](../../strongs/h/h1245.md) the [mashal](../../strongs/h/h4910.md) [paniym](../../strongs/h/h6440.md); but every ['iysh](../../strongs/h/h376.md) [mishpat](../../strongs/h/h4941.md) cometh from [Yĕhovah](../../strongs/h/h3068.md).

<a name="proverbs_29_27"></a>Proverbs 29:27

An ['evel](../../strongs/h/h5766.md) ['iysh](../../strongs/h/h376.md) is a [tôʿēḇâ](../../strongs/h/h8441.md) to the [tsaddiyq](../../strongs/h/h6662.md): and he that is [yashar](../../strongs/h/h3477.md) in the [derek](../../strongs/h/h1870.md) is [tôʿēḇâ](../../strongs/h/h8441.md) to the [rasha'](../../strongs/h/h7563.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 28](proverbs_28.md) - [Proverbs 30](proverbs_30.md)