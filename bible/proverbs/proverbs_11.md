# [Proverbs 11](https://www.blueletterbible.org/kjv/proverbs/11)

<a name="proverbs_11_1"></a>Proverbs 11:1

A [mirmah](../../strongs/h/h4820.md) [mō'znayim](../../strongs/h/h3976.md) is [tôʿēḇâ](../../strongs/h/h8441.md) to [Yĕhovah](../../strongs/h/h3068.md): but a [šālēm](../../strongs/h/h8003.md) ['eben](../../strongs/h/h68.md) is his [ratsown](../../strongs/h/h7522.md).

<a name="proverbs_11_2"></a>Proverbs 11:2

When [zāḏôn](../../strongs/h/h2087.md) [bow'](../../strongs/h/h935.md), then [bow'](../../strongs/h/h935.md) [qālôn](../../strongs/h/h7036.md): but with the [ṣānaʿ](../../strongs/h/h6800.md) is [ḥāḵmâ](../../strongs/h/h2451.md).

<a name="proverbs_11_3"></a>Proverbs 11:3

The [tummâ](../../strongs/h/h8538.md) of the [yashar](../../strongs/h/h3477.md) shall [nachah](../../strongs/h/h5148.md) them: but the [selep̄](../../strongs/h/h5558.md) of [bāḡaḏ](../../strongs/h/h898.md) shall [shadad](../../strongs/h/h7703.md) [shadad](../../strongs/h/h7703.md) them.

<a name="proverbs_11_4"></a>Proverbs 11:4

[hôn](../../strongs/h/h1952.md) [yāʿal](../../strongs/h/h3276.md) not in the [yowm](../../strongs/h/h3117.md) of ['ebrah](../../strongs/h/h5678.md): but [tsedaqah](../../strongs/h/h6666.md) [natsal](../../strongs/h/h5337.md) from [maveth](../../strongs/h/h4194.md).

<a name="proverbs_11_5"></a>Proverbs 11:5

The [tsedaqah](../../strongs/h/h6666.md) of the [tamiym](../../strongs/h/h8549.md) shall [yashar](../../strongs/h/h3474.md) his [derek](../../strongs/h/h1870.md): but the [rasha'](../../strongs/h/h7563.md) shall [naphal](../../strongs/h/h5307.md) by his own [rišʿâ](../../strongs/h/h7564.md).

<a name="proverbs_11_6"></a>Proverbs 11:6

The [tsedaqah](../../strongs/h/h6666.md) of the [yashar](../../strongs/h/h3477.md) shall [natsal](../../strongs/h/h5337.md) them: but [bāḡaḏ](../../strongs/h/h898.md) shall be [lāḵaḏ](../../strongs/h/h3920.md) in their own [havvah](../../strongs/h/h1942.md).

<a name="proverbs_11_7"></a>Proverbs 11:7

When a [rasha'](../../strongs/h/h7563.md) ['āḏām](../../strongs/h/h120.md) [maveth](../../strongs/h/h4194.md), his [tiqvâ](../../strongs/h/h8615.md) shall ['abad](../../strongs/h/h6.md): and the [tôḥeleṯ](../../strongs/h/h8431.md) of ['aven](../../strongs/h/h205.md) ['abad](../../strongs/h/h6.md).

<a name="proverbs_11_8"></a>Proverbs 11:8

The [tsaddiyq](../../strongs/h/h6662.md) is [chalats](../../strongs/h/h2502.md) of [tsarah](../../strongs/h/h6869.md), and the [rasha'](../../strongs/h/h7563.md) [bow'](../../strongs/h/h935.md) in his stead.

<a name="proverbs_11_9"></a>Proverbs 11:9

An [ḥānēp̄](../../strongs/h/h2611.md) with his [peh](../../strongs/h/h6310.md) [shachath](../../strongs/h/h7843.md) his [rea'](../../strongs/h/h7453.md): but through [da'ath](../../strongs/h/h1847.md) shall the [tsaddiyq](../../strongs/h/h6662.md) be [chalats](../../strongs/h/h2502.md).

<a name="proverbs_11_10"></a>Proverbs 11:10

When it goeth [ṭûḇ](../../strongs/h/h2898.md) with the [tsaddiyq](../../strongs/h/h6662.md), the [qiryâ](../../strongs/h/h7151.md) ['alats](../../strongs/h/h5970.md): and when the [rasha'](../../strongs/h/h7563.md) ['abad](../../strongs/h/h6.md), there is [rinnah](../../strongs/h/h7440.md).

<a name="proverbs_11_11"></a>Proverbs 11:11

By the [bĕrakah](../../strongs/h/h1293.md) of the [yashar](../../strongs/h/h3477.md) the [qereṯ](../../strongs/h/h7176.md) is [ruwm](../../strongs/h/h7311.md): but it is [harac](../../strongs/h/h2040.md) by the [peh](../../strongs/h/h6310.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="proverbs_11_12"></a>Proverbs 11:12

He that is [ḥāsēr](../../strongs/h/h2638.md) of [leb](../../strongs/h/h3820.md) [bûz](../../strongs/h/h936.md) his [rea'](../../strongs/h/h7453.md): but an ['iysh](../../strongs/h/h376.md) of [tāḇûn](../../strongs/h/h8394.md) holdeth his [ḥāraš](../../strongs/h/h2790.md).

<a name="proverbs_11_13"></a>Proverbs 11:13

A [halak](../../strongs/h/h1980.md) [rāḵîl](../../strongs/h/h7400.md) [gālâ](../../strongs/h/h1540.md) [sôḏ](../../strongs/h/h5475.md): but he that is of an ['aman](../../strongs/h/h539.md) [ruwach](../../strongs/h/h7307.md) [kāsâ](../../strongs/h/h3680.md) the [dabar](../../strongs/h/h1697.md).

<a name="proverbs_11_14"></a>Proverbs 11:14

Where no [taḥbulôṯ](../../strongs/h/h8458.md) is, the ['am](../../strongs/h/h5971.md) [naphal](../../strongs/h/h5307.md): but in the [rōḇ](../../strongs/h/h7230.md) of [ya'ats](../../strongs/h/h3289.md) there is [tᵊšûʿâ](../../strongs/h/h8668.md).

<a name="proverbs_11_15"></a>Proverbs 11:15

He that is [ʿāraḇ](../../strongs/h/h6148.md) for a [zûr](../../strongs/h/h2114.md) shall [rûaʿ](../../strongs/h/h7321.md) [ra'](../../strongs/h/h7451.md) for it: and he that [sane'](../../strongs/h/h8130.md) [tāqaʿ](../../strongs/h/h8628.md) is [batach](../../strongs/h/h982.md).

<a name="proverbs_11_16"></a>Proverbs 11:16

A [ḥēn](../../strongs/h/h2580.md) ['ishshah](../../strongs/h/h802.md) [tamak](../../strongs/h/h8551.md) [kabowd](../../strongs/h/h3519.md): and [ʿārîṣ](../../strongs/h/h6184.md) men [tamak](../../strongs/h/h8551.md) [ʿōšer](../../strongs/h/h6239.md).

<a name="proverbs_11_17"></a>Proverbs 11:17

The [checed](../../strongs/h/h2617.md) ['iysh](../../strongs/h/h376.md) doeth [gamal](../../strongs/h/h1580.md) to his own [nephesh](../../strongs/h/h5315.md): but he that is ['aḵzārî](../../strongs/h/h394.md) [ʿāḵar](../../strongs/h/h5916.md) his own [šᵊ'ēr](../../strongs/h/h7607.md).

<a name="proverbs_11_18"></a>Proverbs 11:18

The [rasha'](../../strongs/h/h7563.md) ['asah](../../strongs/h/h6213.md) a [sheqer](../../strongs/h/h8267.md) [pe'ullah](../../strongs/h/h6468.md): but to him that [zāraʿ](../../strongs/h/h2232.md) [tsedaqah](../../strongs/h/h6666.md) shall be an ['emeth](../../strongs/h/h571.md) [śeḵer](../../strongs/h/h7938.md).

<a name="proverbs_11_19"></a>Proverbs 11:19

As [tsedaqah](../../strongs/h/h6666.md) tendeth to [chay](../../strongs/h/h2416.md): so he that [radaph](../../strongs/h/h7291.md) [ra'](../../strongs/h/h7451.md) pursueth it to his own [maveth](../../strongs/h/h4194.md).

<a name="proverbs_11_20"></a>Proverbs 11:20

They that are of a [ʿiqqēš](../../strongs/h/h6141.md) [leb](../../strongs/h/h3820.md) are [tôʿēḇâ](../../strongs/h/h8441.md) to [Yĕhovah](../../strongs/h/h3068.md): but such as are [tamiym](../../strongs/h/h8549.md) in their [derek](../../strongs/h/h1870.md) are his [ratsown](../../strongs/h/h7522.md).

<a name="proverbs_11_21"></a>Proverbs 11:21

Though [yad](../../strongs/h/h3027.md) join in [yad](../../strongs/h/h3027.md), the [ra'](../../strongs/h/h7451.md) shall not be [naqah](../../strongs/h/h5352.md): but the [zera'](../../strongs/h/h2233.md) of the [tsaddiyq](../../strongs/h/h6662.md) shall be [mālaṭ](../../strongs/h/h4422.md).

<a name="proverbs_11_22"></a>Proverbs 11:22

As a [nezem](../../strongs/h/h5141.md) of [zāhāḇ](../../strongs/h/h2091.md) in a [ḥăzîr](../../strongs/h/h2386.md) ['aph](../../strongs/h/h639.md), so is a [yāp̄ê](../../strongs/h/h3303.md) ['ishshah](../../strongs/h/h802.md) which is [cuwr](../../strongs/h/h5493.md) [ṭaʿam](../../strongs/h/h2940.md).

<a name="proverbs_11_23"></a>Proverbs 11:23

The [ta'avah](../../strongs/h/h8378.md) of the [tsaddiyq](../../strongs/h/h6662.md) is only [towb](../../strongs/h/h2896.md): but the [tiqvâ](../../strongs/h/h8615.md) of the [rasha'](../../strongs/h/h7563.md) is ['ebrah](../../strongs/h/h5678.md).

<a name="proverbs_11_24"></a>Proverbs 11:24

There [yēš](../../strongs/h/h3426.md) that [p̄zr](../../strongs/h/h6340.md), and yet [yāsap̄](../../strongs/h/h3254.md); and there is that [ḥāśaḵ](../../strongs/h/h2820.md) more than is [yōšer](../../strongs/h/h3476.md), but it tendeth to [maḥsôr](../../strongs/h/h4270.md).

<a name="proverbs_11_25"></a>Proverbs 11:25

The [bĕrakah](../../strongs/h/h1293.md) [nephesh](../../strongs/h/h5315.md) shall be made [dāšēn](../../strongs/h/h1878.md): and he that [rāvâ](../../strongs/h/h7301.md) shall be [yārâ](../../strongs/h/h3384.md) also himself.

<a name="proverbs_11_26"></a>Proverbs 11:26

He that [mānaʿ](../../strongs/h/h4513.md) [bar](../../strongs/h/h1250.md), the [lĕom](../../strongs/h/h3816.md) shall [nāqaḇ](../../strongs/h/h5344.md) him: but [bĕrakah](../../strongs/h/h1293.md) shall be upon the [ro'sh](../../strongs/h/h7218.md) of him that [šāḇar](../../strongs/h/h7666.md) it.

<a name="proverbs_11_27"></a>Proverbs 11:27

He that [šāḥar](../../strongs/h/h7836.md) [towb](../../strongs/h/h2896.md) [bāqaš](../../strongs/h/h1245.md) [ratsown](../../strongs/h/h7522.md): but he that [darash](../../strongs/h/h1875.md) [ra'](../../strongs/h/h7451.md), it shall [bow'](../../strongs/h/h935.md) unto him.

<a name="proverbs_11_28"></a>Proverbs 11:28

He that [batach](../../strongs/h/h982.md) in his [ʿōšer](../../strongs/h/h6239.md) shall [naphal](../../strongs/h/h5307.md): but the [tsaddiyq](../../strongs/h/h6662.md) shall [pāraḥ](../../strongs/h/h6524.md) as an ['aleh](../../strongs/h/h5929.md).

<a name="proverbs_11_29"></a>Proverbs 11:29

He that [ʿāḵar](../../strongs/h/h5916.md) his own [bayith](../../strongs/h/h1004.md) shall [nāḥal](../../strongs/h/h5157.md) the [ruwach](../../strongs/h/h7307.md): and the ['ĕvîl](../../strongs/h/h191.md) shall be ['ebed](../../strongs/h/h5650.md) to the [ḥāḵām](../../strongs/h/h2450.md) of [leb](../../strongs/h/h3820.md).

<a name="proverbs_11_30"></a>Proverbs 11:30

The [pĕriy](../../strongs/h/h6529.md) of the [tsaddiyq](../../strongs/h/h6662.md) is an ['ets](../../strongs/h/h6086.md) of [chay](../../strongs/h/h2416.md); and he that [laqach](../../strongs/h/h3947.md) [nephesh](../../strongs/h/h5315.md) is [ḥāḵām](../../strongs/h/h2450.md).

<a name="proverbs_11_31"></a>Proverbs 11:31

Behold, the [tsaddiyq](../../strongs/h/h6662.md) shall be [shalam](../../strongs/h/h7999.md) in the ['erets](../../strongs/h/h776.md): much more the [rasha'](../../strongs/h/h7563.md) and the [chata'](../../strongs/h/h2398.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 10](proverbs_10.md) - [Proverbs 12](proverbs_12.md)