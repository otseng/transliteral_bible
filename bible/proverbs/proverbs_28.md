# [Proverbs 28](https://www.blueletterbible.org/kjv/proverbs/28)

<a name="proverbs_28_1"></a>Proverbs 28:1

The [rasha'](../../strongs/h/h7563.md) [nûs](../../strongs/h/h5127.md) when no man [radaph](../../strongs/h/h7291.md): but the [tsaddiyq](../../strongs/h/h6662.md) are [batach](../../strongs/h/h982.md) as a [kephiyr](../../strongs/h/h3715.md).

<a name="proverbs_28_2"></a>Proverbs 28:2

For the [pesha'](../../strongs/h/h6588.md) of an ['erets](../../strongs/h/h776.md) [rab](../../strongs/h/h7227.md) are the [śar](../../strongs/h/h8269.md) thereof: but by an ['āḏām](../../strongs/h/h120.md) of [bîn](../../strongs/h/h995.md) and [yada'](../../strongs/h/h3045.md) the [kēn](../../strongs/h/h3651.md) thereof shall be ['arak](../../strongs/h/h748.md).

<a name="proverbs_28_3"></a>Proverbs 28:3

A [rûš](../../strongs/h/h7326.md) [geḇer](../../strongs/h/h1397.md) that [ʿāšaq](../../strongs/h/h6231.md) the [dal](../../strongs/h/h1800.md) is like a [sāḥap̄](../../strongs/h/h5502.md) [māṭār](../../strongs/h/h4306.md) which leaveth no [lechem](../../strongs/h/h3899.md).

<a name="proverbs_28_4"></a>Proverbs 28:4

They that ['azab](../../strongs/h/h5800.md) the [towrah](../../strongs/h/h8451.md) [halal](../../strongs/h/h1984.md) the [rasha'](../../strongs/h/h7563.md): but such as [shamar](../../strongs/h/h8104.md) the [towrah](../../strongs/h/h8451.md) [gārâ](../../strongs/h/h1624.md) with them.

<a name="proverbs_28_5"></a>Proverbs 28:5

[ra'](../../strongs/h/h7451.md) ['enowsh](../../strongs/h/h582.md) [bîn](../../strongs/h/h995.md) not [mishpat](../../strongs/h/h4941.md): but they that [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md) [bîn](../../strongs/h/h995.md) all things.

<a name="proverbs_28_6"></a>Proverbs 28:6

[towb](../../strongs/h/h2896.md) is the [rûš](../../strongs/h/h7326.md) that [halak](../../strongs/h/h1980.md) in his [tom](../../strongs/h/h8537.md), than he that is [ʿiqqēš](../../strongs/h/h6141.md) in his [derek](../../strongs/h/h1870.md), though he be [ʿāšîr](../../strongs/h/h6223.md).

<a name="proverbs_28_7"></a>Proverbs 28:7

Whoso [nāṣar](../../strongs/h/h5341.md) the [towrah](../../strongs/h/h8451.md) is a [bîn](../../strongs/h/h995.md) [ben](../../strongs/h/h1121.md): but he that is a [ra'ah](../../strongs/h/h7462.md) of [zālal](../../strongs/h/h2151.md) men [kālam](../../strongs/h/h3637.md) his ['ab](../../strongs/h/h1.md).

<a name="proverbs_28_8"></a>Proverbs 28:8

He that by [neshek](../../strongs/h/h5392.md) and [tarbîṯ](../../strongs/h/h8636.md) [rabah](../../strongs/h/h7235.md) his [hôn](../../strongs/h/h1952.md), he shall [qāḇaṣ](../../strongs/h/h6908.md) it for him that will [chanan](../../strongs/h/h2603.md) the [dal](../../strongs/h/h1800.md).

<a name="proverbs_28_9"></a>Proverbs 28:9

He that [cuwr](../../strongs/h/h5493.md) his ['ozen](../../strongs/h/h241.md) from [shama'](../../strongs/h/h8085.md) the [towrah](../../strongs/h/h8451.md), even his [tĕphillah](../../strongs/h/h8605.md) shall be [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="proverbs_28_10"></a>Proverbs 28:10

Whoso causeth the [yashar](../../strongs/h/h3477.md) to go [šāḡâ](../../strongs/h/h7686.md) in a [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md), he shall [naphal](../../strongs/h/h5307.md) himself into his own [šᵊḥûṯ](../../strongs/h/h7816.md): but the [tamiym](../../strongs/h/h8549.md) shall have [towb](../../strongs/h/h2896.md) things in [nāḥal](../../strongs/h/h5157.md).

<a name="proverbs_28_11"></a>Proverbs 28:11

The [ʿāšîr](../../strongs/h/h6223.md) ['iysh](../../strongs/h/h376.md) is [ḥāḵām](../../strongs/h/h2450.md) in his own ['ayin](../../strongs/h/h5869.md); but the [dal](../../strongs/h/h1800.md) that hath [bîn](../../strongs/h/h995.md) searcheth him [chaqar](../../strongs/h/h2713.md).

<a name="proverbs_28_12"></a>Proverbs 28:12

When [tsaddiyq](../../strongs/h/h6662.md) men do ['alats](../../strongs/h/h5970.md), there is [rab](../../strongs/h/h7227.md) [tip̄'ārâ](../../strongs/h/h8597.md): but when the [rasha'](../../strongs/h/h7563.md) [quwm](../../strongs/h/h6965.md), an ['āḏām](../../strongs/h/h120.md) is [ḥāp̄aś](../../strongs/h/h2664.md).

<a name="proverbs_28_13"></a>Proverbs 28:13

He that [kāsâ](../../strongs/h/h3680.md) his [pesha'](../../strongs/h/h6588.md) shall not [tsalach](../../strongs/h/h6743.md): but whoso [yadah](../../strongs/h/h3034.md) and ['azab](../../strongs/h/h5800.md) them shall have [racham](../../strongs/h/h7355.md).

<a name="proverbs_28_14"></a>Proverbs 28:14

['esher](../../strongs/h/h835.md) is the ['āḏām](../../strongs/h/h120.md) that [pachad](../../strongs/h/h6342.md) [tāmîḏ](../../strongs/h/h8548.md): but he that [qāšâ](../../strongs/h/h7185.md) his [leb](../../strongs/h/h3820.md) shall [naphal](../../strongs/h/h5307.md) into [ra'](../../strongs/h/h7451.md).

<a name="proverbs_28_15"></a>Proverbs 28:15

As a [nāham](../../strongs/h/h5098.md) ['ariy](../../strongs/h/h738.md), and a [šāqaq](../../strongs/h/h8264.md) [dōḇ](../../strongs/h/h1677.md); so is a [rasha'](../../strongs/h/h7563.md) [mashal](../../strongs/h/h4910.md) over the [dal](../../strongs/h/h1800.md) ['am](../../strongs/h/h5971.md).

<a name="proverbs_28_16"></a>Proverbs 28:16

The [nāḡîḏ](../../strongs/h/h5057.md) that [ḥāsēr](../../strongs/h/h2638.md) [tāḇûn](../../strongs/h/h8394.md) is also a [rab](../../strongs/h/h7227.md) [maʿăšaqqâ](../../strongs/h/h4642.md): but he that [sane'](../../strongs/h/h8130.md) [beṣaʿ](../../strongs/h/h1215.md) shall ['arak](../../strongs/h/h748.md) his [yowm](../../strongs/h/h3117.md).

<a name="proverbs_28_17"></a>Proverbs 28:17

An ['āḏām](../../strongs/h/h120.md) that doeth [ʿāšaq](../../strongs/h/h6231.md) to the [dam](../../strongs/h/h1818.md) of any [nephesh](../../strongs/h/h5315.md) shall [nûs](../../strongs/h/h5127.md) to the [bowr](../../strongs/h/h953.md); let no man [tamak](../../strongs/h/h8551.md) him.

<a name="proverbs_28_18"></a>Proverbs 28:18

Whoso [halak](../../strongs/h/h1980.md) [tamiym](../../strongs/h/h8549.md) shall be [yasha'](../../strongs/h/h3467.md): but he that is [ʿāqaš](../../strongs/h/h6140.md) in his [derek](../../strongs/h/h1870.md) shall [naphal](../../strongs/h/h5307.md) at ['echad](../../strongs/h/h259.md).

<a name="proverbs_28_19"></a>Proverbs 28:19

He that ['abad](../../strongs/h/h5647.md) his ['ăḏāmâ](../../strongs/h/h127.md) shall have [sāׂbaʿ](../../strongs/h/h7646.md) of [lechem](../../strongs/h/h3899.md): but he that [radaph](../../strongs/h/h7291.md) after [reyq](../../strongs/h/h7386.md) persons shall have [rêš](../../strongs/h/h7389.md).

<a name="proverbs_28_20"></a>Proverbs 28:20

An ['ĕmûnâ](../../strongs/h/h530.md) ['iysh](../../strongs/h/h376.md) shall [rab](../../strongs/h/h7227.md) with [bĕrakah](../../strongs/h/h1293.md): but he that maketh ['ûṣ](../../strongs/h/h213.md) to be [ʿāšar](../../strongs/h/h6238.md) shall not be [naqah](../../strongs/h/h5352.md).

<a name="proverbs_28_21"></a>Proverbs 28:21

To have [nāḵar](../../strongs/h/h5234.md) of [paniym](../../strongs/h/h6440.md) is not [towb](../../strongs/h/h2896.md): for for a [paṯ](../../strongs/h/h6595.md) of [lechem](../../strongs/h/h3899.md) that [geḇer](../../strongs/h/h1397.md) will [pāšaʿ](../../strongs/h/h6586.md).

<a name="proverbs_28_22"></a>Proverbs 28:22

['iysh](../../strongs/h/h376.md) that [bahal](../../strongs/h/h926.md) to be [hôn](../../strongs/h/h1952.md) hath a [ra'](../../strongs/h/h7451.md) ['ayin](../../strongs/h/h5869.md), and [yada'](../../strongs/h/h3045.md) not that [ḥeser](../../strongs/h/h2639.md) shall [bow'](../../strongs/h/h935.md) upon him.

<a name="proverbs_28_23"></a>Proverbs 28:23

He that [yakach](../../strongs/h/h3198.md) an ['āḏām](../../strongs/h/h120.md) ['aḥar](../../strongs/h/h310.md) shall [māṣā'](../../strongs/h/h4672.md) more [ḥēn](../../strongs/h/h2580.md) than he that [chalaq](../../strongs/h/h2505.md) with the [lashown](../../strongs/h/h3956.md).

<a name="proverbs_28_24"></a>Proverbs 28:24

Whoso [gāzal](../../strongs/h/h1497.md) his ['ab](../../strongs/h/h1.md) or his ['em](../../strongs/h/h517.md), and ['āmar](../../strongs/h/h559.md), It is no [pesha'](../../strongs/h/h6588.md); the same is the [ḥāḇēr](../../strongs/h/h2270.md) of an ['iysh](../../strongs/h/h376.md) [shachath](../../strongs/h/h7843.md).

<a name="proverbs_28_25"></a>Proverbs 28:25

He that is of a [rāḥāḇ](../../strongs/h/h7342.md) [nephesh](../../strongs/h/h5315.md) stirreth [gārâ](../../strongs/h/h1624.md) [māḏôn](../../strongs/h/h4066.md): but he that putteth his [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md) shall be made [dāšēn](../../strongs/h/h1878.md).

<a name="proverbs_28_26"></a>Proverbs 28:26

He that [batach](../../strongs/h/h982.md) in his own [leb](../../strongs/h/h3820.md) is a [kᵊsîl](../../strongs/h/h3684.md): but whoso [halak](../../strongs/h/h1980.md) [ḥāḵmâ](../../strongs/h/h2451.md), he shall be [mālaṭ](../../strongs/h/h4422.md).

<a name="proverbs_28_27"></a>Proverbs 28:27

He that [nathan](../../strongs/h/h5414.md) unto the [rûš](../../strongs/h/h7326.md) shall not [maḥsôr](../../strongs/h/h4270.md): but he that ['alam](../../strongs/h/h5956.md) his ['ayin](../../strongs/h/h5869.md) shall have [rab](../../strongs/h/h7227.md) a [mᵊ'ērâ](../../strongs/h/h3994.md).

<a name="proverbs_28_28"></a>Proverbs 28:28

When the [rasha'](../../strongs/h/h7563.md) [quwm](../../strongs/h/h6965.md), ['āḏām](../../strongs/h/h120.md) [cathar](../../strongs/h/h5641.md) themselves: but when they ['abad](../../strongs/h/h6.md), the [tsaddiyq](../../strongs/h/h6662.md) [rabah](../../strongs/h/h7235.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 27](proverbs_27.md) - [Proverbs 29](proverbs_29.md)