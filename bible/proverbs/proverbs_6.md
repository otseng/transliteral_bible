# [Proverbs 6](https://www.blueletterbible.org/kjv/proverbs/6)

<a name="proverbs_6_1"></a>Proverbs 6:1

My [ben](../../strongs/h/h1121.md), if thou be [ʿāraḇ](../../strongs/h/h6148.md) for thy [rea'](../../strongs/h/h7453.md), if thou hast [tāqaʿ](../../strongs/h/h8628.md) thy [kaph](../../strongs/h/h3709.md) with a [zûr](../../strongs/h/h2114.md),

<a name="proverbs_6_2"></a>Proverbs 6:2

Thou art [yāqōš](../../strongs/h/h3369.md) with the ['emer](../../strongs/h/h561.md) of thy [peh](../../strongs/h/h6310.md), thou art [lāḵaḏ](../../strongs/h/h3920.md) with the ['emer](../../strongs/h/h561.md) of thy [peh](../../strongs/h/h6310.md).

<a name="proverbs_6_3"></a>Proverbs 6:3

['asah](../../strongs/h/h6213.md) this ['ēp̄ô](../../strongs/h/h645.md), my [ben](../../strongs/h/h1121.md), and [natsal](../../strongs/h/h5337.md) thyself, when thou art [bow'](../../strongs/h/h935.md) into the [kaph](../../strongs/h/h3709.md) of thy [rea'](../../strongs/h/h7453.md); [yālaḵ](../../strongs/h/h3212.md), [rāp̄aś](../../strongs/h/h7511.md) thyself, and make [rāhaḇ](../../strongs/h/h7292.md) thy [rea'](../../strongs/h/h7453.md).

<a name="proverbs_6_4"></a>Proverbs 6:4

[nathan](../../strongs/h/h5414.md) not [šēnā'](../../strongs/h/h8142.md) to thine ['ayin](../../strongs/h/h5869.md), nor [tᵊnûmâ](../../strongs/h/h8572.md) to thine ['aph'aph](../../strongs/h/h6079.md).

<a name="proverbs_6_5"></a>Proverbs 6:5

[natsal](../../strongs/h/h5337.md) thyself as a [ṣᵊḇî](../../strongs/h/h6643.md) from the [yad](../../strongs/h/h3027.md) of the hunter, and as a [tsippowr](../../strongs/h/h6833.md) from the [yad](../../strongs/h/h3027.md) of the [yāqôš](../../strongs/h/h3353.md).

<a name="proverbs_6_6"></a>Proverbs 6:6

[yālaḵ](../../strongs/h/h3212.md) to the [nᵊmālâ](../../strongs/h/h5244.md), thou [ʿāṣēl](../../strongs/h/h6102.md); [ra'ah](../../strongs/h/h7200.md) her [derek](../../strongs/h/h1870.md), and be [ḥāḵam](../../strongs/h/h2449.md):

<a name="proverbs_6_7"></a>Proverbs 6:7

Which having no [qāṣîn](../../strongs/h/h7101.md), [šāṭar](../../strongs/h/h7860.md), or [mashal](../../strongs/h/h4910.md),

<a name="proverbs_6_8"></a>Proverbs 6:8

[kuwn](../../strongs/h/h3559.md) her [lechem](../../strongs/h/h3899.md) in the [qayiṣ](../../strongs/h/h7019.md), and ['āḡar](../../strongs/h/h103.md) her [ma'akal](../../strongs/h/h3978.md) in the [qāṣîr](../../strongs/h/h7105.md).

<a name="proverbs_6_9"></a>Proverbs 6:9

How long wilt thou [shakab](../../strongs/h/h7901.md), O [ʿāṣēl](../../strongs/h/h6102.md)? when wilt thou [quwm](../../strongs/h/h6965.md) out of thy [šēnā'](../../strongs/h/h8142.md)?

<a name="proverbs_6_10"></a>Proverbs 6:10

Yet a [mᵊʿaṭ](../../strongs/h/h4592.md) [šēnā'](../../strongs/h/h8142.md), a [mᵊʿaṭ](../../strongs/h/h4592.md) [tᵊnûmâ](../../strongs/h/h8572.md), a [mᵊʿaṭ](../../strongs/h/h4592.md) [ḥibuq](../../strongs/h/h2264.md) of the [yad](../../strongs/h/h3027.md) to [shakab](../../strongs/h/h7901.md):

<a name="proverbs_6_11"></a>Proverbs 6:11

So shall thy [rêš](../../strongs/h/h7389.md) [bow'](../../strongs/h/h935.md) as one that [halak](../../strongs/h/h1980.md), and thy [maḥsôr](../../strongs/h/h4270.md) as a [magen](../../strongs/h/h4043.md) ['iysh](../../strongs/h/h376.md).

<a name="proverbs_6_12"></a>Proverbs 6:12

A [beliya'al](../../strongs/h/h1100.md) ['āḏām](../../strongs/h/h120.md), an ['aven](../../strongs/h/h205.md) ['iysh](../../strongs/h/h376.md), [halak](../../strongs/h/h1980.md) with an [ʿiqqᵊšûṯ](../../strongs/h/h6143.md) [peh](../../strongs/h/h6310.md).

<a name="proverbs_6_13"></a>Proverbs 6:13

He [qāraṣ](../../strongs/h/h7169.md) with his ['ayin](../../strongs/h/h5869.md), he [mālal](../../strongs/h/h4448.md) with his [regel](../../strongs/h/h7272.md), he [yārâ](../../strongs/h/h3384.md) with his ['etsba'](../../strongs/h/h676.md);

<a name="proverbs_6_14"></a>Proverbs 6:14

[tahpuḵôṯ](../../strongs/h/h8419.md) is in his [leb](../../strongs/h/h3820.md), he [ḥāraš](../../strongs/h/h2790.md) [ra'](../../strongs/h/h7451.md) [ʿēṯ](../../strongs/h/h6256.md); he [shalach](../../strongs/h/h7971.md) [māḏôn](../../strongs/h/h4066.md) [mᵊḏān](../../strongs/h/h4090.md).

<a name="proverbs_6_15"></a>Proverbs 6:15

Therefore shall his ['êḏ](../../strongs/h/h343.md) [bow'](../../strongs/h/h935.md) [piṯ'ōm](../../strongs/h/h6597.md); [peṯaʿ](../../strongs/h/h6621.md) shall he be [shabar](../../strongs/h/h7665.md) without [marpē'](../../strongs/h/h4832.md).

<a name="proverbs_6_16"></a>Proverbs 6:16

These [šēš](../../strongs/h/h8337.md) things doth [Yĕhovah](../../strongs/h/h3068.md) [sane'](../../strongs/h/h8130.md): yea, [šeḇaʿ](../../strongs/h/h7651.md) are a [tôʿēḇâ](../../strongs/h/h8441.md) unto [nephesh](../../strongs/h/h5315.md):

<a name="proverbs_6_17"></a>Proverbs 6:17

A [ruwm](../../strongs/h/h7311.md) ['ayin](../../strongs/h/h5869.md), a [sheqer](../../strongs/h/h8267.md) [lashown](../../strongs/h/h3956.md), and [yad](../../strongs/h/h3027.md) that [šāp̄aḵ](../../strongs/h/h8210.md) [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md),

<a name="proverbs_6_18"></a>Proverbs 6:18

A [leb](../../strongs/h/h3820.md) that [ḥāraš](../../strongs/h/h2790.md) ['aven](../../strongs/h/h205.md) [maḥăšāḇâ](../../strongs/h/h4284.md), [regel](../../strongs/h/h7272.md) that be [māhar](../../strongs/h/h4116.md) in [rûṣ](../../strongs/h/h7323.md) to [ra'](../../strongs/h/h7451.md),

<a name="proverbs_6_19"></a>Proverbs 6:19

A [sheqer](../../strongs/h/h8267.md) ['ed](../../strongs/h/h5707.md) that [puwach](../../strongs/h/h6315.md) [kazab](../../strongs/h/h3577.md), and he that [shalach](../../strongs/h/h7971.md) [mᵊḏān](../../strongs/h/h4090.md) among ['ach](../../strongs/h/h251.md).

<a name="proverbs_6_20"></a>Proverbs 6:20

My [ben](../../strongs/h/h1121.md), [nāṣar](../../strongs/h/h5341.md) thy ['ab](../../strongs/h/h1.md) [mitsvah](../../strongs/h/h4687.md), and [nāṭaš](../../strongs/h/h5203.md) not the [towrah](../../strongs/h/h8451.md) of thy ['em](../../strongs/h/h517.md):

<a name="proverbs_6_21"></a>Proverbs 6:21

[qāšar](../../strongs/h/h7194.md) them [tāmîḏ](../../strongs/h/h8548.md) upon thine [leb](../../strongs/h/h3820.md), and [ʿānaḏ](../../strongs/h/h6029.md) them about thy [gargᵊrôṯ](../../strongs/h/h1621.md).

<a name="proverbs_6_22"></a>Proverbs 6:22

When thou [halak](../../strongs/h/h1980.md), it shall [nachah](../../strongs/h/h5148.md) thee; when thou [shakab](../../strongs/h/h7901.md), it shall [shamar](../../strongs/h/h8104.md) thee; and when thou [quwts](../../strongs/h/h6974.md), it shall [śîaḥ](../../strongs/h/h7878.md) with thee.

<a name="proverbs_6_23"></a>Proverbs 6:23

For the [mitsvah](../../strongs/h/h4687.md) is a [nîr](../../strongs/h/h5216.md); and the [towrah](../../strongs/h/h8451.md) is ['owr](../../strongs/h/h216.md); and [tôḵēḥâ](../../strongs/h/h8433.md) of [mûsār](../../strongs/h/h4148.md) are the [derek](../../strongs/h/h1870.md) of [chay](../../strongs/h/h2416.md):

<a name="proverbs_6_24"></a>Proverbs 6:24

To [shamar](../../strongs/h/h8104.md) thee from the [ra'](../../strongs/h/h7451.md) ['ishshah](../../strongs/h/h802.md), from the [ḥelqâ](../../strongs/h/h2513.md) of the [lashown](../../strongs/h/h3956.md) of a [nāḵrî](../../strongs/h/h5237.md).

<a name="proverbs_6_25"></a>Proverbs 6:25

[chamad](../../strongs/h/h2530.md) not after her [yᵊp̄î](../../strongs/h/h3308.md) in thine [lebab](../../strongs/h/h3824.md); neither let her [laqach](../../strongs/h/h3947.md) thee with her ['aph'aph](../../strongs/h/h6079.md).

<a name="proverbs_6_26"></a>Proverbs 6:26

For by means [bᵊʿaḏ](../../strongs/h/h1157.md) a [zānâ](../../strongs/h/h2181.md) ['ishshah](../../strongs/h/h802.md) a man is brought to a [kikār](../../strongs/h/h3603.md) of [lechem](../../strongs/h/h3899.md): and the ['ishshah](../../strongs/h/h802.md) ['iysh](../../strongs/h/h376.md) will [ṣûḏ](../../strongs/h/h6679.md) for the [yāqār](../../strongs/h/h3368.md) [nephesh](../../strongs/h/h5315.md).

<a name="proverbs_6_27"></a>Proverbs 6:27

Can an ['iysh](../../strongs/h/h376.md) [ḥāṯâ](../../strongs/h/h2846.md) ['esh](../../strongs/h/h784.md) in his [ḥêq](../../strongs/h/h2436.md), and his [beḡeḏ](../../strongs/h/h899.md) not be [śārap̄](../../strongs/h/h8313.md)?

<a name="proverbs_6_28"></a>Proverbs 6:28

Can ['iysh](../../strongs/h/h376.md) [halak](../../strongs/h/h1980.md) upon [gechel](../../strongs/h/h1513.md), and his [regel](../../strongs/h/h7272.md) not be [kāvâ](../../strongs/h/h3554.md)?

<a name="proverbs_6_29"></a>Proverbs 6:29

So he that [bow'](../../strongs/h/h935.md) to his [rea'](../../strongs/h/h7453.md) ['ishshah](../../strongs/h/h802.md); whosoever [naga'](../../strongs/h/h5060.md) her shall not be [naqah](../../strongs/h/h5352.md).

<a name="proverbs_6_30"></a>Proverbs 6:30

Men do not [bûz](../../strongs/h/h936.md) a [gannāḇ](../../strongs/h/h1590.md), if he [ganab](../../strongs/h/h1589.md) to [mālā'](../../strongs/h/h4390.md) his [nephesh](../../strongs/h/h5315.md) when he is [rāʿēḇ](../../strongs/h/h7456.md);

<a name="proverbs_6_31"></a>Proverbs 6:31

But if he be [māṣā'](../../strongs/h/h4672.md), he shall [shalam](../../strongs/h/h7999.md) [šiḇʿāṯayim](../../strongs/h/h7659.md); he shall [nathan](../../strongs/h/h5414.md) all the [hôn](../../strongs/h/h1952.md) of his [bayith](../../strongs/h/h1004.md).

<a name="proverbs_6_32"></a>Proverbs 6:32

But whoso committeth [na'aph](../../strongs/h/h5003.md) with an ['ishshah](../../strongs/h/h802.md) [ḥāsēr](../../strongs/h/h2638.md) [leb](../../strongs/h/h3820.md): he that ['asah](../../strongs/h/h6213.md) it [shachath](../../strongs/h/h7843.md) his own [nephesh](../../strongs/h/h5315.md).

<a name="proverbs_6_33"></a>Proverbs 6:33

A [neḡaʿ](../../strongs/h/h5061.md) and [qālôn](../../strongs/h/h7036.md) shall he [māṣā'](../../strongs/h/h4672.md); and his [cherpah](../../strongs/h/h2781.md) shall not be [māḥâ](../../strongs/h/h4229.md).

<a name="proverbs_6_34"></a>Proverbs 6:34

For [qin'â](../../strongs/h/h7068.md) is the [chemah](../../strongs/h/h2534.md) of a [geḇer](../../strongs/h/h1397.md): therefore he will not [ḥāmal](../../strongs/h/h2550.md) in the [yowm](../../strongs/h/h3117.md) of [nāqām](../../strongs/h/h5359.md).

<a name="proverbs_6_35"></a>Proverbs 6:35

He will not [nasa'](../../strongs/h/h5375.md) [paniym](../../strongs/h/h6440.md) any [kōp̄er](../../strongs/h/h3724.md); neither will he ['āḇâ](../../strongs/h/h14.md), though thou [rabah](../../strongs/h/h7235.md) [shachad](../../strongs/h/h7810.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 5](proverbs_5.md) - [Proverbs 7](proverbs_7.md)