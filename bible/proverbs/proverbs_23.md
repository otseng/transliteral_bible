# [Proverbs 23](https://www.blueletterbible.org/kjv/proverbs/23)

<a name="proverbs_23_1"></a>Proverbs 23:1

When thou [yashab](../../strongs/h/h3427.md) to [lāḥam](../../strongs/h/h3898.md) with a [mashal](../../strongs/h/h4910.md), [bîn](../../strongs/h/h995.md) [bîn](../../strongs/h/h995.md) what is [paniym](../../strongs/h/h6440.md) thee:

<a name="proverbs_23_2"></a>Proverbs 23:2

And [śûm](../../strongs/h/h7760.md) a [śakîn](../../strongs/h/h7915.md) to thy [lōaʿ](../../strongs/h/h3930.md), if thou be a [baʿal](../../strongs/h/h1167.md) to [nephesh](../../strongs/h/h5315.md).

<a name="proverbs_23_3"></a>Proverbs 23:3

Be not ['āvâ](../../strongs/h/h183.md) of his [maṭʿām](../../strongs/h/h4303.md): for they are [kazab](../../strongs/h/h3577.md) [lechem](../../strongs/h/h3899.md).

<a name="proverbs_23_4"></a>Proverbs 23:4

[yaga'](../../strongs/h/h3021.md) not to be [ʿāšar](../../strongs/h/h6238.md): [ḥāḏal](../../strongs/h/h2308.md) from thine own [bînâ](../../strongs/h/h998.md).

<a name="proverbs_23_5"></a>Proverbs 23:5

Wilt thou ['uwph](../../strongs/h/h5774.md) ['uwph](../../strongs/h/h5774.md) thine ['ayin](../../strongs/h/h5869.md) upon that which is not? for riches ['asah](../../strongs/h/h6213.md) ['asah](../../strongs/h/h6213.md) themselves [kanaph](../../strongs/h/h3671.md); they fly ['uwph](../../strongs/h/h5774.md) ['uwph](../../strongs/h/h5774.md) as a [nesheׁr](../../strongs/h/h5404.md) toward [shamayim](../../strongs/h/h8064.md).

<a name="proverbs_23_6"></a>Proverbs 23:6

[lāḥam](../../strongs/h/h3898.md) thou not the [lechem](../../strongs/h/h3899.md) of him that hath a [ra'](../../strongs/h/h7451.md) ['ayin](../../strongs/h/h5869.md), neither ['āvâ](../../strongs/h/h183.md) thou his [maṭʿām](../../strongs/h/h4303.md):

<a name="proverbs_23_7"></a>Proverbs 23:7

For as he [šāʿar](../../strongs/h/h8176.md) in his [nephesh](../../strongs/h/h5315.md), so is he: ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), ['āmar](../../strongs/h/h559.md) he to thee; but his [leb](../../strongs/h/h3820.md) is not with thee.

<a name="proverbs_23_8"></a>Proverbs 23:8

The [paṯ](../../strongs/h/h6595.md) which thou hast ['akal](../../strongs/h/h398.md) shalt thou [qî'](../../strongs/h/h6958.md), and [shachath](../../strongs/h/h7843.md) thy [na'iym](../../strongs/h/h5273.md) [dabar](../../strongs/h/h1697.md).

<a name="proverbs_23_9"></a>Proverbs 23:9

[dabar](../../strongs/h/h1696.md) not in the ['ozen](../../strongs/h/h241.md) of a [kᵊsîl](../../strongs/h/h3684.md): for he will [bûz](../../strongs/h/h936.md) the [śēḵel](../../strongs/h/h7922.md) of thy [millâ](../../strongs/h/h4405.md).

<a name="proverbs_23_10"></a>Proverbs 23:10

[nāsaḡ](../../strongs/h/h5253.md) not the ['owlam](../../strongs/h/h5769.md) [gᵊḇûl](../../strongs/h/h1366.md); and [bow'](../../strongs/h/h935.md) not into the [sadeh](../../strongs/h/h7704.md) of the [yathowm](../../strongs/h/h3490.md):

<a name="proverbs_23_11"></a>Proverbs 23:11

For their [gā'al](../../strongs/h/h1350.md) is [ḥāzāq](../../strongs/h/h2389.md); he shall [riyb](../../strongs/h/h7378.md) their [rîḇ](../../strongs/h/h7379.md) with thee.

<a name="proverbs_23_12"></a>Proverbs 23:12

[bow'](../../strongs/h/h935.md) thine [leb](../../strongs/h/h3820.md) unto [mûsār](../../strongs/h/h4148.md), and thine ['ozen](../../strongs/h/h241.md) to the ['emer](../../strongs/h/h561.md) of [da'ath](../../strongs/h/h1847.md).

<a name="proverbs_23_13"></a>Proverbs 23:13

[mānaʿ](../../strongs/h/h4513.md) not [mûsār](../../strongs/h/h4148.md) from the [naʿar](../../strongs/h/h5288.md): for if thou [nakah](../../strongs/h/h5221.md) him with the [shebet](../../strongs/h/h7626.md), he shall not [muwth](../../strongs/h/h4191.md).

<a name="proverbs_23_14"></a>Proverbs 23:14

Thou shalt [nakah](../../strongs/h/h5221.md) him with the [shebet](../../strongs/h/h7626.md), and shalt [natsal](../../strongs/h/h5337.md) his [nephesh](../../strongs/h/h5315.md) from [shĕ'owl](../../strongs/h/h7585.md).

<a name="proverbs_23_15"></a>Proverbs 23:15

My [ben](../../strongs/h/h1121.md), if thine [leb](../../strongs/h/h3820.md) be [ḥāḵam](../../strongs/h/h2449.md), my [leb](../../strongs/h/h3820.md) shall [samach](../../strongs/h/h8055.md), even ['ănî](../../strongs/h/h589.md).

<a name="proverbs_23_16"></a>Proverbs 23:16

Yea, my [kilyah](../../strongs/h/h3629.md) shall [ʿālaz](../../strongs/h/h5937.md), when thy [saphah](../../strongs/h/h8193.md) [dabar](../../strongs/h/h1696.md) [meyshar](../../strongs/h/h4339.md).

<a name="proverbs_23_17"></a>Proverbs 23:17

Let not thine [leb](../../strongs/h/h3820.md) [qānā'](../../strongs/h/h7065.md) [chatta'](../../strongs/h/h2400.md): but be thou in the [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) all the [yowm](../../strongs/h/h3117.md) long.

<a name="proverbs_23_18"></a>Proverbs 23:18

For surely there [yēš](../../strongs/h/h3426.md) an ['aḥărîṯ](../../strongs/h/h319.md); and thine [tiqvâ](../../strongs/h/h8615.md) shall not be [karath](../../strongs/h/h3772.md).

<a name="proverbs_23_19"></a>Proverbs 23:19

[shama'](../../strongs/h/h8085.md) thou, my [ben](../../strongs/h/h1121.md), and be [ḥāḵam](../../strongs/h/h2449.md), and ['āšar](../../strongs/h/h833.md) thine [leb](../../strongs/h/h3820.md) in the [derek](../../strongs/h/h1870.md).

<a name="proverbs_23_20"></a>Proverbs 23:20

Be not among [yayin](../../strongs/h/h3196.md) [sāḇā'](../../strongs/h/h5433.md); among [zālal](../../strongs/h/h2151.md) of [basar](../../strongs/h/h1320.md):

<a name="proverbs_23_21"></a>Proverbs 23:21

For the [sāḇā'](../../strongs/h/h5433.md) and the [zālal](../../strongs/h/h2151.md) shall come to [yarash](../../strongs/h/h3423.md): and [nûmâ](../../strongs/h/h5124.md) shall [labash](../../strongs/h/h3847.md) a man with [qᵊrāʿîm](../../strongs/h/h7168.md).

<a name="proverbs_23_22"></a>Proverbs 23:22

[shama'](../../strongs/h/h8085.md) unto thy ['ab](../../strongs/h/h1.md) that [yalad](../../strongs/h/h3205.md) thee, and [bûz](../../strongs/h/h936.md) not thy ['em](../../strongs/h/h517.md) when she is [zāqēn](../../strongs/h/h2204.md).

<a name="proverbs_23_23"></a>Proverbs 23:23

[qānâ](../../strongs/h/h7069.md) the ['emeth](../../strongs/h/h571.md), and [māḵar](../../strongs/h/h4376.md) it not; also [ḥāḵmâ](../../strongs/h/h2451.md), and [mûsār](../../strongs/h/h4148.md), and [bînâ](../../strongs/h/h998.md).

<a name="proverbs_23_24"></a>Proverbs 23:24

The ['ab](../../strongs/h/h1.md) of the [tsaddiyq](../../strongs/h/h6662.md) shall [gîl](../../strongs/h/h1524.md) [giyl](../../strongs/h/h1523.md): and he that [yalad](../../strongs/h/h3205.md) a [ḥāḵām](../../strongs/h/h2450.md) shall have [samach](../../strongs/h/h8055.md) of him.

<a name="proverbs_23_25"></a>Proverbs 23:25

Thy ['ab](../../strongs/h/h1.md) and thy ['em](../../strongs/h/h517.md) shall be [samach](../../strongs/h/h8055.md), and she that [yalad](../../strongs/h/h3205.md) thee shall [giyl](../../strongs/h/h1523.md).

<a name="proverbs_23_26"></a>Proverbs 23:26

My [ben](../../strongs/h/h1121.md), [nathan](../../strongs/h/h5414.md) me thine [leb](../../strongs/h/h3820.md), and let thine ['ayin](../../strongs/h/h5869.md) [ratsah](../../strongs/h/h7521.md) [nāṣar](../../strongs/h/h5341.md) my [derek](../../strongs/h/h1870.md).

<a name="proverbs_23_27"></a>Proverbs 23:27

For a [zānâ](../../strongs/h/h2181.md) is a [ʿāmōq](../../strongs/h/h6013.md) [šûḥâ](../../strongs/h/h7745.md); and a [nāḵrî](../../strongs/h/h5237.md) is a [tsar](../../strongs/h/h6862.md) [bᵊ'ēr](../../strongs/h/h875.md).

<a name="proverbs_23_28"></a>Proverbs 23:28

She also lieth in ['arab](../../strongs/h/h693.md) as for a [ḥeṯep̄](../../strongs/h/h2863.md), and [yāsap̄](../../strongs/h/h3254.md) the [bāḡaḏ](../../strongs/h/h898.md) among ['āḏām](../../strongs/h/h120.md).

<a name="proverbs_23_29"></a>Proverbs 23:29

Who hath ['owy](../../strongs/h/h188.md)? who hath ['ăḇôy](../../strongs/h/h17.md)? who hath [miḏyān](../../strongs/h/h4079.md) [māḏôn](../../strongs/h/h4066.md)? who hath [śîaḥ](../../strongs/h/h7879.md)? who hath [peṣaʿ](../../strongs/h/h6482.md) without [ḥinnām](../../strongs/h/h2600.md)? who hath [ḥaḵlilûṯ](../../strongs/h/h2448.md) of ['ayin](../../strongs/h/h5869.md)?

<a name="proverbs_23_30"></a>Proverbs 23:30

They that ['āḥar](../../strongs/h/h309.md) at the [yayin](../../strongs/h/h3196.md); they that [bow'](../../strongs/h/h935.md) to [chaqar](../../strongs/h/h2713.md) [mimsāḵ](../../strongs/h/h4469.md).

<a name="proverbs_23_31"></a>Proverbs 23:31

[ra'ah](../../strongs/h/h7200.md) not thou upon the [yayin](../../strongs/h/h3196.md) when it is ['āḏam](../../strongs/h/h119.md), when it [nathan](../../strongs/h/h5414.md) his ['ayin](../../strongs/h/h5869.md) in the [kowc](../../strongs/h/h3563.md) [kîs](../../strongs/h/h3599.md), when it [halak](../../strongs/h/h1980.md) itself [meyshar](../../strongs/h/h4339.md).

<a name="proverbs_23_32"></a>Proverbs 23:32

At the ['aḥărîṯ](../../strongs/h/h319.md) it [nāšaḵ](../../strongs/h/h5391.md) like a [nachash](../../strongs/h/h5175.md), and [pāraš](../../strongs/h/h6567.md) like a [ṣep̄aʿ](../../strongs/h/h6848.md).

<a name="proverbs_23_33"></a>Proverbs 23:33

Thine ['ayin](../../strongs/h/h5869.md) shall [ra'ah](../../strongs/h/h7200.md) [zûr](../../strongs/h/h2114.md), and thine [leb](../../strongs/h/h3820.md) shall [dabar](../../strongs/h/h1696.md) [tahpuḵôṯ](../../strongs/h/h8419.md).

<a name="proverbs_23_34"></a>Proverbs 23:34

Yea, thou shalt be as he that [shakab](../../strongs/h/h7901.md) in the [leb](../../strongs/h/h3820.md) of the [yam](../../strongs/h/h3220.md), or as he that [shakab](../../strongs/h/h7901.md) upon the [ro'sh](../../strongs/h/h7218.md) of a [ḥibēl](../../strongs/h/h2260.md).

<a name="proverbs_23_35"></a>Proverbs 23:35

They have [nakah](../../strongs/h/h5221.md) me, shalt thou say, and I was not [ḥālâ](../../strongs/h/h2470.md); they have [hālam](../../strongs/h/h1986.md) me, and I [yada'](../../strongs/h/h3045.md) it not: when shall I [quwts](../../strongs/h/h6974.md)? I will [bāqaš](../../strongs/h/h1245.md) it [yāsap̄](../../strongs/h/h3254.md) again.

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 22](proverbs_22.md) - [Proverbs 24](proverbs_24.md)