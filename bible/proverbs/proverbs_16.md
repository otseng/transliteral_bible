# [Proverbs 16](https://www.blueletterbible.org/kjv/proverbs/16)

<a name="proverbs_16_1"></a>Proverbs 16:1

The [maʿărāḵ](../../strongs/h/h4633.md) of the [leb](../../strongs/h/h3820.md) in ['āḏām](../../strongs/h/h120.md), and the [maʿănê](../../strongs/h/h4617.md) of the [lashown](../../strongs/h/h3956.md), is from [Yĕhovah](../../strongs/h/h3068.md).

<a name="proverbs_16_2"></a>Proverbs 16:2

All the [derek](../../strongs/h/h1870.md) of an ['iysh](../../strongs/h/h376.md) are [zāḵ](../../strongs/h/h2134.md) in his own ['ayin](../../strongs/h/h5869.md); but [Yĕhovah](../../strongs/h/h3068.md) [tāḵan](../../strongs/h/h8505.md) the [ruwach](../../strongs/h/h7307.md).

<a name="proverbs_16_3"></a>Proverbs 16:3

[gālal](../../strongs/h/h1556.md) thy [ma'aseh](../../strongs/h/h4639.md) unto [Yĕhovah](../../strongs/h/h3068.md), and thy [maḥăšāḇâ](../../strongs/h/h4284.md) shall be [kuwn](../../strongs/h/h3559.md).

<a name="proverbs_16_4"></a>Proverbs 16:4

[Yĕhovah](../../strongs/h/h3068.md) hath [pa'al](../../strongs/h/h6466.md) all things for [maʿănê](../../strongs/h/h4617.md): yea, even the [rasha'](../../strongs/h/h7563.md) for the [yowm](../../strongs/h/h3117.md) of [ra'](../../strongs/h/h7451.md).

<a name="proverbs_16_5"></a>Proverbs 16:5

Every one that is [gāḇâ](../../strongs/h/h1362.md) in [leb](../../strongs/h/h3820.md) is a [tôʿēḇâ](../../strongs/h/h8441.md) to [Yĕhovah](../../strongs/h/h3068.md): though [yad](../../strongs/h/h3027.md) join in [yad](../../strongs/h/h3027.md), he shall not be [naqah](../../strongs/h/h5352.md).

<a name="proverbs_16_6"></a>Proverbs 16:6

By [checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md) ['avon](../../strongs/h/h5771.md) is [kāp̄ar](../../strongs/h/h3722.md): and by the [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) [cuwr](../../strongs/h/h5493.md) from [ra'](../../strongs/h/h7451.md).

<a name="proverbs_16_7"></a>Proverbs 16:7

When an ['iysh](../../strongs/h/h376.md) [derek](../../strongs/h/h1870.md) [ratsah](../../strongs/h/h7521.md) [Yĕhovah](../../strongs/h/h3068.md), he maketh even his ['oyeb](../../strongs/h/h341.md) to be at [shalam](../../strongs/h/h7999.md) with him.

<a name="proverbs_16_8"></a>Proverbs 16:8

[towb](../../strongs/h/h2896.md) is a [mᵊʿaṭ](../../strongs/h/h4592.md) with [tsedaqah](../../strongs/h/h6666.md) than [rōḇ](../../strongs/h/h7230.md) [tᵊḇû'â](../../strongs/h/h8393.md) without [mishpat](../../strongs/h/h4941.md).

<a name="proverbs_16_9"></a>Proverbs 16:9

An ['āḏām](../../strongs/h/h120.md) [leb](../../strongs/h/h3820.md) [chashab](../../strongs/h/h2803.md) his [derek](../../strongs/h/h1870.md): but [Yĕhovah](../../strongs/h/h3068.md) [kuwn](../../strongs/h/h3559.md) his [ṣaʿaḏ](../../strongs/h/h6806.md).

<a name="proverbs_16_10"></a>Proverbs 16:10

A [qesem](../../strongs/h/h7081.md) is in the [saphah](../../strongs/h/h8193.md) of the [melek](../../strongs/h/h4428.md): his [peh](../../strongs/h/h6310.md) [māʿal](../../strongs/h/h4603.md) not in [mishpat](../../strongs/h/h4941.md).

<a name="proverbs_16_11"></a>Proverbs 16:11

A [mishpat](../../strongs/h/h4941.md) [peles](../../strongs/h/h6425.md) and [mō'znayim](../../strongs/h/h3976.md) are [Yĕhovah](../../strongs/h/h3068.md): all the ['eben](../../strongs/h/h68.md) of the [kîs](../../strongs/h/h3599.md) are his [ma'aseh](../../strongs/h/h4639.md).

<a name="proverbs_16_12"></a>Proverbs 16:12

It is a [tôʿēḇâ](../../strongs/h/h8441.md) to [melek](../../strongs/h/h4428.md) to ['asah](../../strongs/h/h6213.md) [resha'](../../strongs/h/h7562.md): for the [kicce'](../../strongs/h/h3678.md) is [kuwn](../../strongs/h/h3559.md) by [tsedaqah](../../strongs/h/h6666.md).

<a name="proverbs_16_13"></a>Proverbs 16:13

[tsedeq](../../strongs/h/h6664.md) [saphah](../../strongs/h/h8193.md) are the [ratsown](../../strongs/h/h7522.md) of [melek](../../strongs/h/h4428.md); and they ['ahab](../../strongs/h/h157.md) him that [dabar](../../strongs/h/h1696.md) [yashar](../../strongs/h/h3477.md).

<a name="proverbs_16_14"></a>Proverbs 16:14

The [chemah](../../strongs/h/h2534.md) of a [melek](../../strongs/h/h4428.md) is as [mal'ak](../../strongs/h/h4397.md) of [maveth](../../strongs/h/h4194.md): but a [ḥāḵām](../../strongs/h/h2450.md) ['iysh](../../strongs/h/h376.md) will [kāp̄ar](../../strongs/h/h3722.md) it.

<a name="proverbs_16_15"></a>Proverbs 16:15

In the ['owr](../../strongs/h/h216.md) of the [melek](../../strongs/h/h4428.md) [paniym](../../strongs/h/h6440.md) is [chay](../../strongs/h/h2416.md); and his [ratsown](../../strongs/h/h7522.md) is as an ['ab](../../strongs/h/h5645.md) of the latter [malqôš](../../strongs/h/h4456.md).

<a name="proverbs_16_16"></a>Proverbs 16:16

How much [towb](../../strongs/h/h2896.md) is it to [qānâ](../../strongs/h/h7069.md) [ḥāḵmâ](../../strongs/h/h2451.md) than [ḥārûṣ](../../strongs/h/h2742.md)! and to [qānâ](../../strongs/h/h7069.md) [bînâ](../../strongs/h/h998.md) rather to be [bāḥar](../../strongs/h/h977.md) than [keceph](../../strongs/h/h3701.md)!

<a name="proverbs_16_17"></a>Proverbs 16:17

The [mĕcillah](../../strongs/h/h4546.md) of the [yashar](../../strongs/h/h3477.md) is to [cuwr](../../strongs/h/h5493.md) from [ra'](../../strongs/h/h7451.md): he that [nāṣar](../../strongs/h/h5341.md) his [derek](../../strongs/h/h1870.md) [shamar](../../strongs/h/h8104.md) his [nephesh](../../strongs/h/h5315.md).

<a name="proverbs_16_18"></a>Proverbs 16:18

[gā'ôn](../../strongs/h/h1347.md) goeth [paniym](../../strongs/h/h6440.md) [šeḇar](../../strongs/h/h7667.md), and a [gobahh](../../strongs/h/h1363.md) [ruwach](../../strongs/h/h7307.md) [paniym](../../strongs/h/h6440.md) a [kiššālôn](../../strongs/h/h3783.md).

<a name="proverbs_16_19"></a>Proverbs 16:19

[towb](../../strongs/h/h2896.md) it is to be of a [šāp̄āl](../../strongs/h/h8217.md) [ruwach](../../strongs/h/h7307.md) with the ['anav](../../strongs/h/h6035.md) ['aniy](../../strongs/h/h6041.md), than to [chalaq](../../strongs/h/h2505.md) the [šālāl](../../strongs/h/h7998.md) with the [gē'ê](../../strongs/h/h1343.md).

<a name="proverbs_16_20"></a>Proverbs 16:20

He that handleth a [dabar](../../strongs/h/h1697.md) [sakal](../../strongs/h/h7919.md) shall [māṣā'](../../strongs/h/h4672.md) [towb](../../strongs/h/h2896.md): and whoso [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md), ['esher](../../strongs/h/h835.md) is he.

<a name="proverbs_16_21"></a>Proverbs 16:21

The [ḥāḵām](../../strongs/h/h2450.md) in [leb](../../strongs/h/h3820.md) shall be [qara'](../../strongs/h/h7121.md) [bîn](../../strongs/h/h995.md): and the [meṯeq](../../strongs/h/h4986.md) of the [saphah](../../strongs/h/h8193.md) [yāsap̄](../../strongs/h/h3254.md) [leqaḥ](../../strongs/h/h3948.md).

<a name="proverbs_16_22"></a>Proverbs 16:22

[śēḵel](../../strongs/h/h7922.md) is a [māqôr](../../strongs/h/h4726.md) of [chay](../../strongs/h/h2416.md) unto him that [baʿal](../../strongs/h/h1167.md) it: but the [mûsār](../../strongs/h/h4148.md) of ['ĕvîl](../../strongs/h/h191.md) is ['iûeleṯ](../../strongs/h/h200.md).

<a name="proverbs_16_23"></a>Proverbs 16:23

The [leb](../../strongs/h/h3820.md) of the [ḥāḵām](../../strongs/h/h2450.md) [sakal](../../strongs/h/h7919.md) his [peh](../../strongs/h/h6310.md), and [yāsap̄](../../strongs/h/h3254.md) [leqaḥ](../../strongs/h/h3948.md) to his [saphah](../../strongs/h/h8193.md).

<a name="proverbs_16_24"></a>Proverbs 16:24

[nōʿam](../../strongs/h/h5278.md) ['emer](../../strongs/h/h561.md) are as a [ṣûp̄](../../strongs/h/h6688.md) [dĕbash](../../strongs/h/h1706.md), [māṯôq](../../strongs/h/h4966.md) to the [nephesh](../../strongs/h/h5315.md), and [marpē'](../../strongs/h/h4832.md) to the ['etsem](../../strongs/h/h6106.md).

<a name="proverbs_16_25"></a>Proverbs 16:25

There [yēš](../../strongs/h/h3426.md) a [derek](../../strongs/h/h1870.md) that seemeth [yashar](../../strongs/h/h3477.md) [paniym](../../strongs/h/h6440.md) an ['iysh](../../strongs/h/h376.md), but the ['aḥărîṯ](../../strongs/h/h319.md) thereof are the [derek](../../strongs/h/h1870.md) of [maveth](../../strongs/h/h4194.md).

<a name="proverbs_16_26"></a>Proverbs 16:26

[nephesh](../../strongs/h/h5315.md) that [ʿāmēl](../../strongs/h/h6001.md) [ʿāmal](../../strongs/h/h5998.md) for himself; for his [peh](../../strongs/h/h6310.md) ['āḵap̄](../../strongs/h/h404.md) it of him.

<a name="proverbs_16_27"></a>Proverbs 16:27

A [beliya'al](../../strongs/h/h1100.md) ['iysh](../../strongs/h/h376.md) [karah](../../strongs/h/h3738.md) [ra'](../../strongs/h/h7451.md): and in his [saphah](../../strongs/h/h8193.md) there is as a [ṣāreḇeṯ](../../strongs/h/h6867.md) ['esh](../../strongs/h/h784.md).

<a name="proverbs_16_28"></a>Proverbs 16:28

A [tahpuḵôṯ](../../strongs/h/h8419.md) ['iysh](../../strongs/h/h376.md) [shalach](../../strongs/h/h7971.md) [māḏôn](../../strongs/h/h4066.md): and a [nirgān](../../strongs/h/h5372.md) [pāraḏ](../../strongs/h/h6504.md) ['allûp̄](../../strongs/h/h441.md).

<a name="proverbs_16_29"></a>Proverbs 16:29

A [chamac](../../strongs/h/h2555.md) ['iysh](../../strongs/h/h376.md) [pāṯâ](../../strongs/h/h6601.md) his [rea'](../../strongs/h/h7453.md), and [yālaḵ](../../strongs/h/h3212.md) him into the [derek](../../strongs/h/h1870.md) that is not [towb](../../strongs/h/h2896.md).

<a name="proverbs_16_30"></a>Proverbs 16:30

He [ʿāṣâ](../../strongs/h/h6095.md) his ['ayin](../../strongs/h/h5869.md) to [chashab](../../strongs/h/h2803.md) [tahpuḵôṯ](../../strongs/h/h8419.md): [qāraṣ](../../strongs/h/h7169.md) his [saphah](../../strongs/h/h8193.md) he [kalah](../../strongs/h/h3615.md) [ra'](../../strongs/h/h7451.md) to [kalah](../../strongs/h/h3615.md).

<a name="proverbs_16_31"></a>Proverbs 16:31

The [śêḇâ](../../strongs/h/h7872.md) is an [ʿăṭārâ](../../strongs/h/h5850.md) of [tip̄'ārâ](../../strongs/h/h8597.md), if it be [māṣā'](../../strongs/h/h4672.md) in the [derek](../../strongs/h/h1870.md) of [tsedaqah](../../strongs/h/h6666.md).

<a name="proverbs_16_32"></a>Proverbs 16:32

He that is ['ārēḵ](../../strongs/h/h750.md) to ['aph](../../strongs/h/h639.md) is [towb](../../strongs/h/h2896.md) than the [gibôr](../../strongs/h/h1368.md); and he that [mashal](../../strongs/h/h4910.md) his [ruwach](../../strongs/h/h7307.md) than he that [lāḵaḏ](../../strongs/h/h3920.md) a [ʿîr](../../strongs/h/h5892.md).

<a name="proverbs_16_33"></a>Proverbs 16:33

The [gôrāl](../../strongs/h/h1486.md) is [ṭûl](../../strongs/h/h2904.md) into the [ḥêq](../../strongs/h/h2436.md); but the whole [mishpat](../../strongs/h/h4941.md) thereof is of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 15](proverbs_15.md) - [Proverbs 17](proverbs_17.md)