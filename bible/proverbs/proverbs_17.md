# [Proverbs 17](https://www.blueletterbible.org/kjv/proverbs/17)

<a name="proverbs_17_1"></a>Proverbs 17:1

[towb](../../strongs/h/h2896.md) is a [ḥārēḇ](../../strongs/h/h2720.md) [paṯ](../../strongs/h/h6595.md), and [šalvâ](../../strongs/h/h7962.md) therewith, than a [bayith](../../strongs/h/h1004.md) [mālē'](../../strongs/h/h4392.md) of [zebach](../../strongs/h/h2077.md) with [rîḇ](../../strongs/h/h7379.md).

<a name="proverbs_17_2"></a>Proverbs 17:2

A [sakal](../../strongs/h/h7919.md) ['ebed](../../strongs/h/h5650.md) shall have [mashal](../../strongs/h/h4910.md) over a [ben](../../strongs/h/h1121.md) that causeth [buwsh](../../strongs/h/h954.md), and shall have [chalaq](../../strongs/h/h2505.md) of the [nachalah](../../strongs/h/h5159.md) [tavek](../../strongs/h/h8432.md) the ['ach](../../strongs/h/h251.md).

<a name="proverbs_17_3"></a>Proverbs 17:3

The [maṣrēp̄](../../strongs/h/h4715.md) is for [keceph](../../strongs/h/h3701.md), and the [kûr](../../strongs/h/h3564.md) for [zāhāḇ](../../strongs/h/h2091.md): but [Yĕhovah](../../strongs/h/h3068.md) [bachan](../../strongs/h/h974.md) the [libbah](../../strongs/h/h3826.md).

<a name="proverbs_17_4"></a>Proverbs 17:4

A [ra'a'](../../strongs/h/h7489.md) giveth [qashab](../../strongs/h/h7181.md) to ['aven](../../strongs/h/h205.md) [saphah](../../strongs/h/h8193.md); and a [sheqer](../../strongs/h/h8267.md) giveth ['azan](../../strongs/h/h238.md) to a [havvah](../../strongs/h/h1942.md) [lashown](../../strongs/h/h3956.md).

<a name="proverbs_17_5"></a>Proverbs 17:5

Whoso [lāʿaḡ](../../strongs/h/h3932.md) the [rûš](../../strongs/h/h7326.md) [ḥārap̄](../../strongs/h/h2778.md) his ['asah](../../strongs/h/h6213.md): and he that is [śāmēaḥ](../../strongs/h/h8056.md) at ['êḏ](../../strongs/h/h343.md) shall not be [naqah](../../strongs/h/h5352.md).

<a name="proverbs_17_6"></a>Proverbs 17:6

[ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md) are the [ʿăṭārâ](../../strongs/h/h5850.md) of [zāqēn](../../strongs/h/h2205.md); and the [tip̄'ārâ](../../strongs/h/h8597.md) of [ben](../../strongs/h/h1121.md) are their ['ab](../../strongs/h/h1.md).

<a name="proverbs_17_7"></a>Proverbs 17:7

[yeṯer](../../strongs/h/h3499.md) [saphah](../../strongs/h/h8193.md) [nā'vê](../../strongs/h/h5000.md) not a [nabal](../../strongs/h/h5036.md): much less do [sheqer](../../strongs/h/h8267.md) [saphah](../../strongs/h/h8193.md) a [nāḏîḇ](../../strongs/h/h5081.md).

<a name="proverbs_17_8"></a>Proverbs 17:8

A [shachad](../../strongs/h/h7810.md) is as a [ḥēn](../../strongs/h/h2580.md) ['eben](../../strongs/h/h68.md) in the ['ayin](../../strongs/h/h5869.md) of him that [baʿal](../../strongs/h/h1167.md) it: whithersoever it [panah](../../strongs/h/h6437.md), it [sakal](../../strongs/h/h7919.md).

<a name="proverbs_17_9"></a>Proverbs 17:9

He that [kāsâ](../../strongs/h/h3680.md) a [pesha'](../../strongs/h/h6588.md) [bāqaš](../../strongs/h/h1245.md) ['ahăḇâ](../../strongs/h/h160.md); but he that [šānâ](../../strongs/h/h8138.md) a [dabar](../../strongs/h/h1697.md) [pāraḏ](../../strongs/h/h6504.md) very ['allûp̄](../../strongs/h/h441.md).

<a name="proverbs_17_10"></a>Proverbs 17:10

A [ge'arah](../../strongs/h/h1606.md) [nāḥaṯ](../../strongs/h/h5181.md) more into a wise [bîn](../../strongs/h/h995.md) than a [mē'â](../../strongs/h/h3967.md) [nakah](../../strongs/h/h5221.md) into a [kᵊsîl](../../strongs/h/h3684.md).

<a name="proverbs_17_11"></a>Proverbs 17:11

A [ra'](../../strongs/h/h7451.md) [bāqaš](../../strongs/h/h1245.md) only [mᵊrî](../../strongs/h/h4805.md): therefore an ['aḵzārî](../../strongs/h/h394.md) [mal'ak](../../strongs/h/h4397.md) shall be [shalach](../../strongs/h/h7971.md) against him.

<a name="proverbs_17_12"></a>Proverbs 17:12

Let a [dōḇ](../../strongs/h/h1677.md) [šakûl](../../strongs/h/h7909.md) of her whelps [pāḡaš](../../strongs/h/h6298.md) an ['iysh](../../strongs/h/h376.md), rather ['al](../../strongs/h/h408.md) a [kᵊsîl](../../strongs/h/h3684.md) in his ['iûeleṯ](../../strongs/h/h200.md).

<a name="proverbs_17_13"></a>Proverbs 17:13

Whoso [shuwb](../../strongs/h/h7725.md) [ra'](../../strongs/h/h7451.md) for [towb](../../strongs/h/h2896.md), [ra'](../../strongs/h/h7451.md) shall not [mûš](../../strongs/h/h4185.md) [mûš](../../strongs/h/h4185.md) from his [bayith](../../strongs/h/h1004.md).

<a name="proverbs_17_14"></a>Proverbs 17:14

The [re'shiyth](../../strongs/h/h7225.md) of [māḏôn](../../strongs/h/h4066.md) is as when one letteth [pāṭar](../../strongs/h/h6362.md) [mayim](../../strongs/h/h4325.md): therefore leave [nāṭaš](../../strongs/h/h5203.md) [rîḇ](../../strongs/h/h7379.md), [paniym](../../strongs/h/h6440.md) it be [gālaʿ](../../strongs/h/h1566.md) with.

<a name="proverbs_17_15"></a>Proverbs 17:15

He that [ṣāḏaq](../../strongs/h/h6663.md) the [rasha'](../../strongs/h/h7563.md), and he that [rāšaʿ](../../strongs/h/h7561.md) the [tsaddiyq](../../strongs/h/h6662.md), even they [šᵊnayim](../../strongs/h/h8147.md) are [tôʿēḇâ](../../strongs/h/h8441.md) to [Yĕhovah](../../strongs/h/h3068.md).

<a name="proverbs_17_16"></a>Proverbs 17:16

Wherefore is there a [mᵊḥîr](../../strongs/h/h4242.md) in the [yad](../../strongs/h/h3027.md) of a [kᵊsîl](../../strongs/h/h3684.md) to [qānâ](../../strongs/h/h7069.md) [ḥāḵmâ](../../strongs/h/h2451.md), seeing he hath no [leb](../../strongs/h/h3820.md) to it?

<a name="proverbs_17_17"></a>Proverbs 17:17

A [rea'](../../strongs/h/h7453.md) ['ahab](../../strongs/h/h157.md) at all [ʿēṯ](../../strongs/h/h6256.md), and an ['ach](../../strongs/h/h251.md) is [yalad](../../strongs/h/h3205.md) for [tsarah](../../strongs/h/h6869.md).

<a name="proverbs_17_18"></a>Proverbs 17:18

An ['āḏām](../../strongs/h/h120.md) [ḥāsēr](../../strongs/h/h2638.md) of [leb](../../strongs/h/h3820.md) [tāqaʿ](../../strongs/h/h8628.md) [kaph](../../strongs/h/h3709.md), and [ʿāraḇ](../../strongs/h/h6148.md) [ʿărubâ](../../strongs/h/h6161.md) in the [paniym](../../strongs/h/h6440.md) of his [rea'](../../strongs/h/h7453.md).

<a name="proverbs_17_19"></a>Proverbs 17:19

He ['ahab](../../strongs/h/h157.md) [pesha'](../../strongs/h/h6588.md) that ['ahab](../../strongs/h/h157.md) [maṣṣâ](../../strongs/h/h4683.md): and he that [gāḇah](../../strongs/h/h1361.md) his [peṯaḥ](../../strongs/h/h6607.md) [bāqaš](../../strongs/h/h1245.md) [šeḇar](../../strongs/h/h7667.md).

<a name="proverbs_17_20"></a>Proverbs 17:20

He that hath an [ʿiqqēš](../../strongs/h/h6141.md) [leb](../../strongs/h/h3820.md) [māṣā'](../../strongs/h/h4672.md) no [towb](../../strongs/h/h2896.md): and he that hath a [hāp̄aḵ](../../strongs/h/h2015.md) [lashown](../../strongs/h/h3956.md) [naphal](../../strongs/h/h5307.md) into [ra'](../../strongs/h/h7451.md).

<a name="proverbs_17_21"></a>Proverbs 17:21

He that [yalad](../../strongs/h/h3205.md) a [kᵊsîl](../../strongs/h/h3684.md) doeth it to his [tûḡâ](../../strongs/h/h8424.md): and the ['ab](../../strongs/h/h1.md) of a [nabal](../../strongs/h/h5036.md) hath no [samach](../../strongs/h/h8055.md).

<a name="proverbs_17_22"></a>Proverbs 17:22

A [śāmēaḥ](../../strongs/h/h8056.md) [leb](../../strongs/h/h3820.md) doeth [yatab](../../strongs/h/h3190.md) like a [gêâ](../../strongs/h/h1456.md): but a [nāḵā'](../../strongs/h/h5218.md) [ruwach](../../strongs/h/h7307.md) [yāḇēš](../../strongs/h/h3001.md) the [gerem](../../strongs/h/h1634.md).

<a name="proverbs_17_23"></a>Proverbs 17:23

A [rasha'](../../strongs/h/h7563.md) man [laqach](../../strongs/h/h3947.md) a [shachad](../../strongs/h/h7810.md) out of the [ḥêq](../../strongs/h/h2436.md) to [natah](../../strongs/h/h5186.md) the ['orach](../../strongs/h/h734.md) of [mishpat](../../strongs/h/h4941.md).

<a name="proverbs_17_24"></a>Proverbs 17:24

[ḥāḵmâ](../../strongs/h/h2451.md) is before him that hath [paniym](../../strongs/h/h6440.md) [bîn](../../strongs/h/h995.md); but the ['ayin](../../strongs/h/h5869.md) of a [kᵊsîl](../../strongs/h/h3684.md) are in the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md).

<a name="proverbs_17_25"></a>Proverbs 17:25

A [kᵊsîl](../../strongs/h/h3684.md) [ben](../../strongs/h/h1121.md) is a [ka'ac](../../strongs/h/h3708.md) to his ['ab](../../strongs/h/h1.md), and [memer](../../strongs/h/h4470.md) to her that [yalad](../../strongs/h/h3205.md) him.

<a name="proverbs_17_26"></a>Proverbs 17:26

Also to [ʿānaš](../../strongs/h/h6064.md) the [tsaddiyq](../../strongs/h/h6662.md) is not [towb](../../strongs/h/h2896.md), nor to [nakah](../../strongs/h/h5221.md) [nāḏîḇ](../../strongs/h/h5081.md) for [yōšer](../../strongs/h/h3476.md).

<a name="proverbs_17_27"></a>Proverbs 17:27

He that [yada'](../../strongs/h/h3045.md) [da'ath](../../strongs/h/h1847.md) [ḥāśaḵ](../../strongs/h/h2820.md) his ['emer](../../strongs/h/h561.md): and an ['iysh](../../strongs/h/h376.md) of [tāḇûn](../../strongs/h/h8394.md) is of a [yāqār](../../strongs/h/h3368.md) [qar](../../strongs/h/h7119.md) [ruwach](../../strongs/h/h7307.md).

<a name="proverbs_17_28"></a>Proverbs 17:28

Even an ['ĕvîl](../../strongs/h/h191.md), when he holdeth his [ḥāraš](../../strongs/h/h2790.md), is [chashab](../../strongs/h/h2803.md) [ḥāḵām](../../strongs/h/h2450.md): and he that ['āṭam](../../strongs/h/h331.md) his [saphah](../../strongs/h/h8193.md) is esteemed a man of [bîn](../../strongs/h/h995.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 16](proverbs_16.md) - [Proverbs 18](proverbs_18.md)