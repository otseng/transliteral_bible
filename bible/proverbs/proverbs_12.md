# [Proverbs 12](https://www.blueletterbible.org/kjv/proverbs/12)

<a name="proverbs_12_1"></a>Proverbs 12:1

Whoso ['ahab](../../strongs/h/h157.md) [mûsār](../../strongs/h/h4148.md) ['ahab](../../strongs/h/h157.md) [da'ath](../../strongs/h/h1847.md): but he that [sane'](../../strongs/h/h8130.md) [tôḵēḥâ](../../strongs/h/h8433.md) is [baʿar](../../strongs/h/h1198.md).

<a name="proverbs_12_2"></a>Proverbs 12:2

A [towb](../../strongs/h/h2896.md) man [pûq](../../strongs/h/h6329.md) [ratsown](../../strongs/h/h7522.md) of [Yĕhovah](../../strongs/h/h3068.md): but an ['iysh](../../strongs/h/h376.md) of [mezimmah](../../strongs/h/h4209.md) will he [rāšaʿ](../../strongs/h/h7561.md).

<a name="proverbs_12_3"></a>Proverbs 12:3

An ['āḏām](../../strongs/h/h120.md) shall not be [kuwn](../../strongs/h/h3559.md) by [resha'](../../strongs/h/h7562.md): but the [šereš](../../strongs/h/h8328.md) of the [tsaddiyq](../../strongs/h/h6662.md) shall not be [mowt](../../strongs/h/h4131.md).

<a name="proverbs_12_4"></a>Proverbs 12:4

An [ḥayil](../../strongs/h/h2428.md) ['ishshah](../../strongs/h/h802.md) is an [ʿăṭārâ](../../strongs/h/h5850.md) to her [baʿal](../../strongs/h/h1167.md): but she that [buwsh](../../strongs/h/h954.md) is as [rāqāḇ](../../strongs/h/h7538.md) in his ['etsem](../../strongs/h/h6106.md).

<a name="proverbs_12_5"></a>Proverbs 12:5

The [maḥăšāḇâ](../../strongs/h/h4284.md) of the [tsaddiyq](../../strongs/h/h6662.md) are [mishpat](../../strongs/h/h4941.md): but the [taḥbulôṯ](../../strongs/h/h8458.md) of the [rasha'](../../strongs/h/h7563.md) are [mirmah](../../strongs/h/h4820.md).

<a name="proverbs_12_6"></a>Proverbs 12:6

The [dabar](../../strongs/h/h1697.md) of the [rasha'](../../strongs/h/h7563.md) are to lie in ['arab](../../strongs/h/h693.md) for [dam](../../strongs/h/h1818.md): but the [peh](../../strongs/h/h6310.md) of the [yashar](../../strongs/h/h3477.md) shall [natsal](../../strongs/h/h5337.md) them.

<a name="proverbs_12_7"></a>Proverbs 12:7

The [rasha'](../../strongs/h/h7563.md) are [hāp̄aḵ](../../strongs/h/h2015.md), and are not: but the [bayith](../../strongs/h/h1004.md) of the [tsaddiyq](../../strongs/h/h6662.md) shall ['amad](../../strongs/h/h5975.md).

<a name="proverbs_12_8"></a>Proverbs 12:8

An ['iysh](../../strongs/h/h376.md) shall be [halal](../../strongs/h/h1984.md) [peh](../../strongs/h/h6310.md) to his [śēḵel](../../strongs/h/h7922.md): but he that is of a [ʿāvâ](../../strongs/h/h5753.md) [leb](../../strongs/h/h3820.md) shall be [bûz](../../strongs/h/h937.md).

<a name="proverbs_12_9"></a>Proverbs 12:9

He that is [qālâ](../../strongs/h/h7034.md), and hath an ['ebed](../../strongs/h/h5650.md), is [towb](../../strongs/h/h2896.md) than he that [kabad](../../strongs/h/h3513.md) himself, and [ḥāsēr](../../strongs/h/h2638.md) [lechem](../../strongs/h/h3899.md).

<a name="proverbs_12_10"></a>Proverbs 12:10

A [tsaddiyq](../../strongs/h/h6662.md) man [yada'](../../strongs/h/h3045.md) the [nephesh](../../strongs/h/h5315.md) of his [bĕhemah](../../strongs/h/h929.md): but the [raḥam](../../strongs/h/h7356.md) of the [rasha'](../../strongs/h/h7563.md) are ['aḵzārî](../../strongs/h/h394.md).

<a name="proverbs_12_11"></a>Proverbs 12:11

He that ['abad](../../strongs/h/h5647.md) his ['ăḏāmâ](../../strongs/h/h127.md) shall be [sāׂbaʿ](../../strongs/h/h7646.md) with [lechem](../../strongs/h/h3899.md): but he that [radaph](../../strongs/h/h7291.md) [reyq](../../strongs/h/h7386.md) is [ḥāsēr](../../strongs/h/h2638.md) of [leb](../../strongs/h/h3820.md).

<a name="proverbs_12_12"></a>Proverbs 12:12

The [rasha'](../../strongs/h/h7563.md) [chamad](../../strongs/h/h2530.md) the [māṣôḏ](../../strongs/h/h4685.md) of [ra'](../../strongs/h/h7451.md) men: but the [šereš](../../strongs/h/h8328.md) of the [tsaddiyq](../../strongs/h/h6662.md) [nathan](../../strongs/h/h5414.md).

<a name="proverbs_12_13"></a>Proverbs 12:13

The [ra'](../../strongs/h/h7451.md) is [mowqesh](../../strongs/h/h4170.md) by the [pesha'](../../strongs/h/h6588.md) of his [saphah](../../strongs/h/h8193.md): but the [tsaddiyq](../../strongs/h/h6662.md) shall [yāṣā'](../../strongs/h/h3318.md) of [tsarah](../../strongs/h/h6869.md).

<a name="proverbs_12_14"></a>Proverbs 12:14

An ['iysh](../../strongs/h/h376.md) shall be [sāׂbaʿ](../../strongs/h/h7646.md) with [towb](../../strongs/h/h2896.md) by the [pĕriy](../../strongs/h/h6529.md) of his [peh](../../strongs/h/h6310.md): and the [gĕmwl](../../strongs/h/h1576.md) of an ['āḏām](../../strongs/h/h120.md) [yad](../../strongs/h/h3027.md) shall be [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) unto him.

<a name="proverbs_12_15"></a>Proverbs 12:15

The [derek](../../strongs/h/h1870.md) of an ['ĕvîl](../../strongs/h/h191.md) is [yashar](../../strongs/h/h3477.md) in his own ['ayin](../../strongs/h/h5869.md): but he that [shama'](../../strongs/h/h8085.md) unto ['etsah](../../strongs/h/h6098.md) is [ḥāḵām](../../strongs/h/h2450.md).

<a name="proverbs_12_16"></a>Proverbs 12:16

An ['ĕvîl](../../strongs/h/h191.md) [ka'ac](../../strongs/h/h3708.md) is [yowm](../../strongs/h/h3117.md) [yada'](../../strongs/h/h3045.md): but an ['aruwm](../../strongs/h/h6175.md) man [kāsâ](../../strongs/h/h3680.md) [qālôn](../../strongs/h/h7036.md).

<a name="proverbs_12_17"></a>Proverbs 12:17

He that [puwach](../../strongs/h/h6315.md) ['ĕmûnâ](../../strongs/h/h530.md) sheweth [nāḡaḏ](../../strongs/h/h5046.md) [tsedeq](../../strongs/h/h6664.md): but a [sheqer](../../strongs/h/h8267.md) ['ed](../../strongs/h/h5707.md) [mirmah](../../strongs/h/h4820.md).

<a name="proverbs_12_18"></a>Proverbs 12:18

There [yēš](../../strongs/h/h3426.md) that [bāṭā'](../../strongs/h/h981.md) like the [maḏqārâ](../../strongs/h/h4094.md) of a [chereb](../../strongs/h/h2719.md): but the [lashown](../../strongs/h/h3956.md) of the [ḥāḵām](../../strongs/h/h2450.md) is [marpē'](../../strongs/h/h4832.md).

<a name="proverbs_12_19"></a>Proverbs 12:19

The [saphah](../../strongs/h/h8193.md) of ['emeth](../../strongs/h/h571.md) shall be [kuwn](../../strongs/h/h3559.md) for [ʿaḏ](../../strongs/h/h5703.md): but a [sheqer](../../strongs/h/h8267.md) [lashown](../../strongs/h/h3956.md) is but for a [rāḡaʿ](../../strongs/h/h7280.md).

<a name="proverbs_12_20"></a>Proverbs 12:20

[mirmah](../../strongs/h/h4820.md) is in the [leb](../../strongs/h/h3820.md) of them that [ḥāraš](../../strongs/h/h2790.md) [ra'](../../strongs/h/h7451.md): but to the [ya'ats](../../strongs/h/h3289.md) of [shalowm](../../strongs/h/h7965.md) is [simchah](../../strongs/h/h8057.md).

<a name="proverbs_12_21"></a>Proverbs 12:21

There shall no ['aven](../../strongs/h/h205.md) ['ānâ](../../strongs/h/h579.md) to the [tsaddiyq](../../strongs/h/h6662.md): but the [rasha'](../../strongs/h/h7563.md) shall be [mālā'](../../strongs/h/h4390.md) with [ra'](../../strongs/h/h7451.md).

<a name="proverbs_12_22"></a>Proverbs 12:22

[sheqer](../../strongs/h/h8267.md) [saphah](../../strongs/h/h8193.md) are [tôʿēḇâ](../../strongs/h/h8441.md) to [Yĕhovah](../../strongs/h/h3068.md): but they that ['asah](../../strongs/h/h6213.md) ['ĕmûnâ](../../strongs/h/h530.md) are his [ratsown](../../strongs/h/h7522.md).

<a name="proverbs_12_23"></a>Proverbs 12:23

An ['aruwm](../../strongs/h/h6175.md) ['āḏām](../../strongs/h/h120.md) [kāsâ](../../strongs/h/h3680.md) [da'ath](../../strongs/h/h1847.md): but the [leb](../../strongs/h/h3820.md) of [kᵊsîl](../../strongs/h/h3684.md) [qara'](../../strongs/h/h7121.md) ['iûeleṯ](../../strongs/h/h200.md).

<a name="proverbs_12_24"></a>Proverbs 12:24

The [yad](../../strongs/h/h3027.md) of the [ḥārûṣ](../../strongs/h/h2742.md) shall bear [mashal](../../strongs/h/h4910.md): but the [rᵊmîyâ](../../strongs/h/h7423.md) shall be under [mas](../../strongs/h/h4522.md).

<a name="proverbs_12_25"></a>Proverbs 12:25

[dᵊ'āḡâ](../../strongs/h/h1674.md) in the [leb](../../strongs/h/h3820.md) of ['iysh](../../strongs/h/h376.md) maketh it [shachah](../../strongs/h/h7812.md): but a [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md) maketh it [samach](../../strongs/h/h8055.md).

<a name="proverbs_12_26"></a>Proverbs 12:26

The [tsaddiyq](../../strongs/h/h6662.md) is more [tûr](../../strongs/h/h8446.md) than his [rea'](../../strongs/h/h7453.md): but the [derek](../../strongs/h/h1870.md) of the [rasha'](../../strongs/h/h7563.md) [tāʿâ](../../strongs/h/h8582.md) them.

<a name="proverbs_12_27"></a>Proverbs 12:27

The [rᵊmîyâ](../../strongs/h/h7423.md) man [ḥāraḵ](../../strongs/h/h2760.md) not that which he took in [ṣayiḏ](../../strongs/h/h6718.md): but the [hôn](../../strongs/h/h1952.md) of a [ḥārûṣ](../../strongs/h/h2742.md) ['āḏām](../../strongs/h/h120.md) is [yāqār](../../strongs/h/h3368.md).

<a name="proverbs_12_28"></a>Proverbs 12:28

In the ['orach](../../strongs/h/h734.md) of [tsedaqah](../../strongs/h/h6666.md) is [chay](../../strongs/h/h2416.md); and in the [nāṯîḇ](../../strongs/h/h5410.md) [derek](../../strongs/h/h1870.md) thereof there is no [maveth](../../strongs/h/h4194.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 11](proverbs_11.md) - [Proverbs 13](proverbs_13.md)