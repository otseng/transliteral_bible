# [Proverbs 19](https://www.blueletterbible.org/kjv/proverbs/19)

<a name="proverbs_19_1"></a>Proverbs 19:1

[towb](../../strongs/h/h2896.md) is the [rûš](../../strongs/h/h7326.md) that [halak](../../strongs/h/h1980.md) in his [tom](../../strongs/h/h8537.md), than he that is [ʿiqqēš](../../strongs/h/h6141.md) in his [saphah](../../strongs/h/h8193.md), and is a [kᵊsîl](../../strongs/h/h3684.md).

<a name="proverbs_19_2"></a>Proverbs 19:2

Also, that the [nephesh](../../strongs/h/h5315.md) be without [da'ath](../../strongs/h/h1847.md), it is not [towb](../../strongs/h/h2896.md); and he that ['ûṣ](../../strongs/h/h213.md) with his [regel](../../strongs/h/h7272.md) [chata'](../../strongs/h/h2398.md).

<a name="proverbs_19_3"></a>Proverbs 19:3

The ['iûeleṯ](../../strongs/h/h200.md) of ['āḏām](../../strongs/h/h120.md) [sālap̄](../../strongs/h/h5557.md) his [derek](../../strongs/h/h1870.md): and his [leb](../../strongs/h/h3820.md) [zāʿap̄](../../strongs/h/h2196.md) against [Yĕhovah](../../strongs/h/h3068.md).

<a name="proverbs_19_4"></a>Proverbs 19:4

[hôn](../../strongs/h/h1952.md) [yāsap̄](../../strongs/h/h3254.md) [rab](../../strongs/h/h7227.md) [rea'](../../strongs/h/h7453.md); but the [dal](../../strongs/h/h1800.md) is [pāraḏ](../../strongs/h/h6504.md) from his [rea'](../../strongs/h/h7453.md).

<a name="proverbs_19_5"></a>Proverbs 19:5

A [sheqer](../../strongs/h/h8267.md) ['ed](../../strongs/h/h5707.md) shall not be [naqah](../../strongs/h/h5352.md), and he that [puwach](../../strongs/h/h6315.md) [kazab](../../strongs/h/h3577.md) shall not [mālaṭ](../../strongs/h/h4422.md).

<a name="proverbs_19_6"></a>Proverbs 19:6

[rab](../../strongs/h/h7227.md) will [ḥālâ](../../strongs/h/h2470.md) the [paniym](../../strongs/h/h6440.md) of the [nāḏîḇ](../../strongs/h/h5081.md): and every man is a [rea'](../../strongs/h/h7453.md) to ['iysh](../../strongs/h/h376.md) that giveth [matān](../../strongs/h/h4976.md).

<a name="proverbs_19_7"></a>Proverbs 19:7

All the ['ach](../../strongs/h/h251.md) of the [rûš](../../strongs/h/h7326.md) do [sane'](../../strongs/h/h8130.md) him: how much more do his [mērēaʿ](../../strongs/h/h4828.md) go [rachaq](../../strongs/h/h7368.md) from him? he [radaph](../../strongs/h/h7291.md) them with ['emer](../../strongs/h/h561.md), yet they are wanting to him.

<a name="proverbs_19_8"></a>Proverbs 19:8

He that [qānâ](../../strongs/h/h7069.md) [leb](../../strongs/h/h3820.md) ['ahab](../../strongs/h/h157.md) his own [nephesh](../../strongs/h/h5315.md): he that [shamar](../../strongs/h/h8104.md) [tāḇûn](../../strongs/h/h8394.md) shall [māṣā'](../../strongs/h/h4672.md) [towb](../../strongs/h/h2896.md).

<a name="proverbs_19_9"></a>Proverbs 19:9

A [sheqer](../../strongs/h/h8267.md) ['ed](../../strongs/h/h5707.md) shall not be [naqah](../../strongs/h/h5352.md), and he that [puwach](../../strongs/h/h6315.md) [kazab](../../strongs/h/h3577.md) shall ['abad](../../strongs/h/h6.md).

<a name="proverbs_19_10"></a>Proverbs 19:10

[taʿănûḡ](../../strongs/h/h8588.md) is not [nā'vê](../../strongs/h/h5000.md) for a [kᵊsîl](../../strongs/h/h3684.md); much less for an ['ebed](../../strongs/h/h5650.md) to have [mashal](../../strongs/h/h4910.md) over [śar](../../strongs/h/h8269.md).

<a name="proverbs_19_11"></a>Proverbs 19:11

The [śēḵel](../../strongs/h/h7922.md) of an ['āḏām](../../strongs/h/h120.md) ['arak](../../strongs/h/h748.md) his ['aph](../../strongs/h/h639.md); and it is his [tip̄'ārâ](../../strongs/h/h8597.md) to ['abar](../../strongs/h/h5674.md) a [pesha'](../../strongs/h/h6588.md).

<a name="proverbs_19_12"></a>Proverbs 19:12

The [melek](../../strongs/h/h4428.md) [zaʿap̄](../../strongs/h/h2197.md) is as the [naham](../../strongs/h/h5099.md) of a [kephiyr](../../strongs/h/h3715.md); but his [ratsown](../../strongs/h/h7522.md) is as [ṭal](../../strongs/h/h2919.md) upon the ['eseb](../../strongs/h/h6212.md).

<a name="proverbs_19_13"></a>Proverbs 19:13

A [kᵊsîl](../../strongs/h/h3684.md) [ben](../../strongs/h/h1121.md) is the [havvah](../../strongs/h/h1942.md) of his ['ab](../../strongs/h/h1.md): and the [miḏyān](../../strongs/h/h4079.md) of an ['ishshah](../../strongs/h/h802.md) are a [ṭāraḏ](../../strongs/h/h2956.md) [delep̄](../../strongs/h/h1812.md).

<a name="proverbs_19_14"></a>Proverbs 19:14

[bayith](../../strongs/h/h1004.md) and [hôn](../../strongs/h/h1952.md) are the [nachalah](../../strongs/h/h5159.md) of ['ab](../../strongs/h/h1.md): and a [sakal](../../strongs/h/h7919.md) ['ishshah](../../strongs/h/h802.md) is from [Yĕhovah](../../strongs/h/h3068.md).

<a name="proverbs_19_15"></a>Proverbs 19:15

[ʿaṣlâ](../../strongs/h/h6103.md) [naphal](../../strongs/h/h5307.md) into a [tardēmâ](../../strongs/h/h8639.md); and a [rᵊmîyâ](../../strongs/h/h7423.md) [nephesh](../../strongs/h/h5315.md) shall suffer [rāʿēḇ](../../strongs/h/h7456.md).

<a name="proverbs_19_16"></a>Proverbs 19:16

He that [shamar](../../strongs/h/h8104.md) the [mitsvah](../../strongs/h/h4687.md) [shamar](../../strongs/h/h8104.md) his own [nephesh](../../strongs/h/h5315.md); but he that [bazah](../../strongs/h/h959.md) his [derek](../../strongs/h/h1870.md) shall [muwth](../../strongs/h/h4191.md).

<a name="proverbs_19_17"></a>Proverbs 19:17

He that hath [chanan](../../strongs/h/h2603.md) upon the [dal](../../strongs/h/h1800.md) [lāvâ](../../strongs/h/h3867.md) unto [Yĕhovah](../../strongs/h/h3068.md); and that which he hath [gĕmwl](../../strongs/h/h1576.md) will he pay him [shalam](../../strongs/h/h7999.md).

<a name="proverbs_19_18"></a>Proverbs 19:18

[yacar](../../strongs/h/h3256.md) thy [ben](../../strongs/h/h1121.md) while there [yēš](../../strongs/h/h3426.md) [tiqvâ](../../strongs/h/h8615.md), and let not thy [nephesh](../../strongs/h/h5315.md) [nasa'](../../strongs/h/h5375.md) for his [muwth](../../strongs/h/h4191.md).

<a name="proverbs_19_19"></a>Proverbs 19:19

A [gadowl](../../strongs/h/h1419.md) of [gārōl](../../strongs/h/h1632.md) [chemah](../../strongs/h/h2534.md) shall [nasa'](../../strongs/h/h5375.md) [ʿōneš](../../strongs/h/h6066.md): for if thou [natsal](../../strongs/h/h5337.md) him, yet thou must do it [yāsap̄](../../strongs/h/h3254.md).

<a name="proverbs_19_20"></a>Proverbs 19:20

[shama'](../../strongs/h/h8085.md) ['etsah](../../strongs/h/h6098.md), and [qāḇal](../../strongs/h/h6901.md) [mûsār](../../strongs/h/h4148.md), that thou mayest be [ḥāḵam](../../strongs/h/h2449.md) in thy ['aḥărîṯ](../../strongs/h/h319.md).

<a name="proverbs_19_21"></a>Proverbs 19:21

There are [rab](../../strongs/h/h7227.md) [maḥăšāḇâ](../../strongs/h/h4284.md) in an ['iysh](../../strongs/h/h376.md) [leb](../../strongs/h/h3820.md); nevertheless the ['etsah](../../strongs/h/h6098.md) of [Yĕhovah](../../strongs/h/h3068.md), that shall [quwm](../../strongs/h/h6965.md).

<a name="proverbs_19_22"></a>Proverbs 19:22

The [ta'avah](../../strongs/h/h8378.md) of an ['āḏām](../../strongs/h/h120.md) is his [checed](../../strongs/h/h2617.md): and a poor [rûš](../../strongs/h/h7326.md) is [towb](../../strongs/h/h2896.md) ['iysh](../../strongs/h/h376.md) a [kazab](../../strongs/h/h3577.md).

<a name="proverbs_19_23"></a>Proverbs 19:23

The [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) tendeth to [chay](../../strongs/h/h2416.md): and he that hath it shall [lûn](../../strongs/h/h3885.md) [śāḇēaʿ](../../strongs/h/h7649.md); he shall not be [paqad](../../strongs/h/h6485.md) with [ra'](../../strongs/h/h7451.md).

<a name="proverbs_19_24"></a>Proverbs 19:24

An [ʿāṣēl](../../strongs/h/h6102.md) man [taman](../../strongs/h/h2934.md) his [yad](../../strongs/h/h3027.md) in his [ṣallaḥaṯ](../../strongs/h/h6747.md), and will not so much as [shuwb](../../strongs/h/h7725.md) it to his [peh](../../strongs/h/h6310.md) [shuwb](../../strongs/h/h7725.md).

<a name="proverbs_19_25"></a>Proverbs 19:25

[nakah](../../strongs/h/h5221.md) a [luwts](../../strongs/h/h3887.md), and the [pᵊṯî](../../strongs/h/h6612.md) will [ʿāram](../../strongs/h/h6191.md): and [yakach](../../strongs/h/h3198.md) one that hath [bîn](../../strongs/h/h995.md), and he will [bîn](../../strongs/h/h995.md) [da'ath](../../strongs/h/h1847.md).

<a name="proverbs_19_26"></a>Proverbs 19:26

He that [shadad](../../strongs/h/h7703.md) his ['ab](../../strongs/h/h1.md), and [bāraḥ](../../strongs/h/h1272.md) his ['em](../../strongs/h/h517.md), is a [ben](../../strongs/h/h1121.md) that causeth [buwsh](../../strongs/h/h954.md), and bringeth [ḥāp̄ēr](../../strongs/h/h2659.md).

<a name="proverbs_19_27"></a>Proverbs 19:27

[ḥāḏal](../../strongs/h/h2308.md), my [ben](../../strongs/h/h1121.md), to [shama'](../../strongs/h/h8085.md) the [mûsār](../../strongs/h/h4148.md) that causeth to [šāḡâ](../../strongs/h/h7686.md) from the ['emer](../../strongs/h/h561.md) of [da'ath](../../strongs/h/h1847.md).

<a name="proverbs_19_28"></a>Proverbs 19:28

A [beliya'al](../../strongs/h/h1100.md) ['ed](../../strongs/h/h5707.md) [luwts](../../strongs/h/h3887.md) [mishpat](../../strongs/h/h4941.md): and the [peh](../../strongs/h/h6310.md) of the [rasha'](../../strongs/h/h7563.md) [bālaʿ](../../strongs/h/h1104.md) ['aven](../../strongs/h/h205.md).

<a name="proverbs_19_29"></a>Proverbs 19:29

[šep̄eṭ](../../strongs/h/h8201.md) are [kuwn](../../strongs/h/h3559.md) for [luwts](../../strongs/h/h3887.md), and [mahălummâ](../../strongs/h/h4112.md) for the [gēv](../../strongs/h/h1460.md) of [kᵊsîl](../../strongs/h/h3684.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 18](proverbs_18.md) - [Proverbs 20](proverbs_20.md)