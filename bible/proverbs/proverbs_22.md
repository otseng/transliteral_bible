# [Proverbs 22](https://www.blueletterbible.org/kjv/proverbs/22)

<a name="proverbs_22_1"></a>Proverbs 22:1

A [shem](../../strongs/h/h8034.md) is rather to be [bāḥar](../../strongs/h/h977.md) than [rab](../../strongs/h/h7227.md) [ʿōšer](../../strongs/h/h6239.md), and [towb](../../strongs/h/h2896.md) [ḥēn](../../strongs/h/h2580.md) rather than [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md).

<a name="proverbs_22_2"></a>Proverbs 22:2

The [ʿāšîr](../../strongs/h/h6223.md) and [rûš](../../strongs/h/h7326.md) [pāḡaš](../../strongs/h/h6298.md): [Yĕhovah](../../strongs/h/h3068.md) is the ['asah](../../strongs/h/h6213.md) of them all.

<a name="proverbs_22_3"></a>Proverbs 22:3

An ['aruwm](../../strongs/h/h6175.md) [ra'ah](../../strongs/h/h7200.md) the [ra'](../../strongs/h/h7451.md), and [cathar](../../strongs/h/h5641.md) [cathar](../../strongs/h/h5641.md) himself: but the [pᵊṯî](../../strongs/h/h6612.md) ['abar](../../strongs/h/h5674.md), and are [ʿānaš](../../strongs/h/h6064.md).

<a name="proverbs_22_4"></a>Proverbs 22:4

[ʿēqeḇ](../../strongs/h/h6118.md) [ʿănāvâ](../../strongs/h/h6038.md) and the [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) are [ʿōšer](../../strongs/h/h6239.md), and [kabowd](../../strongs/h/h3519.md), and [chay](../../strongs/h/h2416.md).

<a name="proverbs_22_5"></a>Proverbs 22:5

[ṣēn](../../strongs/h/h6791.md) and [paḥ](../../strongs/h/h6341.md) are in the [derek](../../strongs/h/h1870.md) of the [ʿiqqēš](../../strongs/h/h6141.md): he that [shamar](../../strongs/h/h8104.md) his [nephesh](../../strongs/h/h5315.md) shall be [rachaq](../../strongs/h/h7368.md) from them.

<a name="proverbs_22_6"></a>Proverbs 22:6

[ḥānaḵ](../../strongs/h/h2596.md) a [naʿar](../../strongs/h/h5288.md) in the [derek](../../strongs/h/h1870.md) he should [peh](../../strongs/h/h6310.md): and when he is [zāqēn](../../strongs/h/h2204.md), he will not [cuwr](../../strongs/h/h5493.md) from it.

<a name="proverbs_22_7"></a>Proverbs 22:7

The [ʿāšîr](../../strongs/h/h6223.md) [mashal](../../strongs/h/h4910.md) over the [rûš](../../strongs/h/h7326.md), and the [lāvâ](../../strongs/h/h3867.md) is ['ebed](../../strongs/h/h5650.md) to the ['iysh](../../strongs/h/h376.md) [lāvâ](../../strongs/h/h3867.md).

<a name="proverbs_22_8"></a>Proverbs 22:8

He that [zāraʿ](../../strongs/h/h2232.md) ['evel](../../strongs/h/h5766.md) shall [qāṣar](../../strongs/h/h7114.md) ['aven](../../strongs/h/h205.md): and the [shebet](../../strongs/h/h7626.md) of his ['ebrah](../../strongs/h/h5678.md) shall [kalah](../../strongs/h/h3615.md).

<a name="proverbs_22_9"></a>Proverbs 22:9

He that hath a [towb](../../strongs/h/h2896.md) ['ayin](../../strongs/h/h5869.md) shall be [barak](../../strongs/h/h1288.md); for he [nathan](../../strongs/h/h5414.md) of his [lechem](../../strongs/h/h3899.md) to the [dal](../../strongs/h/h1800.md).

<a name="proverbs_22_10"></a>Proverbs 22:10

[gāraš](../../strongs/h/h1644.md) the [luwts](../../strongs/h/h3887.md), and [māḏôn](../../strongs/h/h4066.md) shall [yāṣā'](../../strongs/h/h3318.md); yea, [diyn](../../strongs/h/h1779.md) and [qālôn](../../strongs/h/h7036.md) shall [shabath](../../strongs/h/h7673.md).

<a name="proverbs_22_11"></a>Proverbs 22:11

He that ['ahab](../../strongs/h/h157.md) [ṭᵊhôr](../../strongs/h/h2890.md) [tahowr](../../strongs/h/h2889.md) of [leb](../../strongs/h/h3820.md), for the [ḥēn](../../strongs/h/h2580.md) of his [saphah](../../strongs/h/h8193.md) the [melek](../../strongs/h/h4428.md) shall be his [rea'](../../strongs/h/h7453.md).

<a name="proverbs_22_12"></a>Proverbs 22:12

The ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) [nāṣar](../../strongs/h/h5341.md) [da'ath](../../strongs/h/h1847.md), and he [sālap̄](../../strongs/h/h5557.md) the [dabar](../../strongs/h/h1697.md) of the [bāḡaḏ](../../strongs/h/h898.md).

<a name="proverbs_22_13"></a>Proverbs 22:13

The [ʿāṣēl](../../strongs/h/h6102.md) ['āmar](../../strongs/h/h559.md), There is an ['ariy](../../strongs/h/h738.md) [ḥûṣ](../../strongs/h/h2351.md), I shall be [ratsach](../../strongs/h/h7523.md) [tavek](../../strongs/h/h8432.md) the [rᵊḥōḇ](../../strongs/h/h7339.md).

<a name="proverbs_22_14"></a>Proverbs 22:14

The [peh](../../strongs/h/h6310.md) of [zûr](../../strongs/h/h2114.md) is an [ʿāmōq](../../strongs/h/h6013.md) [šûḥâ](../../strongs/h/h7745.md): he that is [za'am](../../strongs/h/h2194.md) of [Yĕhovah](../../strongs/h/h3068.md) shall [naphal](../../strongs/h/h5307.md) therein.

<a name="proverbs_22_15"></a>Proverbs 22:15

['iûeleṯ](../../strongs/h/h200.md) is [qāšar](../../strongs/h/h7194.md) in the [leb](../../strongs/h/h3820.md) of a [naʿar](../../strongs/h/h5288.md); but the [shebet](../../strongs/h/h7626.md) of [mûsār](../../strongs/h/h4148.md) shall drive it [rachaq](../../strongs/h/h7368.md) from him.

<a name="proverbs_22_16"></a>Proverbs 22:16

He that [ʿāšaq](../../strongs/h/h6231.md) the [dal](../../strongs/h/h1800.md) to [rabah](../../strongs/h/h7235.md) his riches, and he that [nathan](../../strongs/h/h5414.md) to the [ʿāšîr](../../strongs/h/h6223.md), shall surely come to [maḥsôr](../../strongs/h/h4270.md).

<a name="proverbs_22_17"></a>Proverbs 22:17

[natah](../../strongs/h/h5186.md) thine ['ozen](../../strongs/h/h241.md), and [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of the [ḥāḵām](../../strongs/h/h2450.md), and [shiyth](../../strongs/h/h7896.md) thine [leb](../../strongs/h/h3820.md) unto my [da'ath](../../strongs/h/h1847.md).

<a name="proverbs_22_18"></a>Proverbs 22:18

For it is a [na'iym](../../strongs/h/h5273.md) if thou [shamar](../../strongs/h/h8104.md) them [beten](../../strongs/h/h990.md) thee; they shall [yaḥaḏ](../../strongs/h/h3162.md) be [kuwn](../../strongs/h/h3559.md) in thy [saphah](../../strongs/h/h8193.md).

<a name="proverbs_22_19"></a>Proverbs 22:19

That thy [miḇṭāḥ](../../strongs/h/h4009.md) may be in [Yĕhovah](../../strongs/h/h3068.md), I have made [yada'](../../strongs/h/h3045.md) to thee this [yowm](../../strongs/h/h3117.md), even to thee.

<a name="proverbs_22_20"></a>Proverbs 22:20

Have not I [kāṯaḇ](../../strongs/h/h3789.md) to [šilšôm](../../strongs/h/h8032.md) [šālîš](../../strongs/h/h7991.md) in [mow'etsah](../../strongs/h/h4156.md) and [da'ath](../../strongs/h/h1847.md),

<a name="proverbs_22_21"></a>Proverbs 22:21

That I might make thee [yada'](../../strongs/h/h3045.md) the [qōšeṭ](../../strongs/h/h7189.md) of the ['emer](../../strongs/h/h561.md) of ['emeth](../../strongs/h/h571.md); that thou mightest [shuwb](../../strongs/h/h7725.md) the ['emer](../../strongs/h/h561.md) of ['emeth](../../strongs/h/h571.md) to them that [shalach](../../strongs/h/h7971.md) unto thee?

<a name="proverbs_22_22"></a>Proverbs 22:22

[gāzal](../../strongs/h/h1497.md) not the [dal](../../strongs/h/h1800.md), because he is [dal](../../strongs/h/h1800.md): neither [dāḵā'](../../strongs/h/h1792.md) the ['aniy](../../strongs/h/h6041.md) in the [sha'ar](../../strongs/h/h8179.md):

<a name="proverbs_22_23"></a>Proverbs 22:23

For [Yĕhovah](../../strongs/h/h3068.md) will [riyb](../../strongs/h/h7378.md) their [rîḇ](../../strongs/h/h7379.md), and [qāḇaʿ](../../strongs/h/h6906.md) the [nephesh](../../strongs/h/h5315.md) of those that [qāḇaʿ](../../strongs/h/h6906.md) them.

<a name="proverbs_22_24"></a>Proverbs 22:24

Make no [ra'ah](../../strongs/h/h7462.md) with an ['aph](../../strongs/h/h639.md) [baʿal](../../strongs/h/h1167.md); and with a [chemah](../../strongs/h/h2534.md) ['iysh](../../strongs/h/h376.md) thou shalt not [bow'](../../strongs/h/h935.md):

<a name="proverbs_22_25"></a>Proverbs 22:25

Lest thou ['ālap̄](../../strongs/h/h502.md) his ['orach](../../strongs/h/h734.md), and [laqach](../../strongs/h/h3947.md) a [mowqesh](../../strongs/h/h4170.md) to thy [nephesh](../../strongs/h/h5315.md).

<a name="proverbs_22_26"></a>Proverbs 22:26

Be not thou one of them that [tāqaʿ](../../strongs/h/h8628.md) [kaph](../../strongs/h/h3709.md), or of them that are [ʿāraḇ](../../strongs/h/h6148.md) for [maššā'â](../../strongs/h/h4859.md).

<a name="proverbs_22_27"></a>Proverbs 22:27

If thou hast nothing to [shalam](../../strongs/h/h7999.md), why should he take [laqach](../../strongs/h/h3947.md) thy [miškāḇ](../../strongs/h/h4904.md) from under thee?

<a name="proverbs_22_28"></a>Proverbs 22:28

[nāsaḡ](../../strongs/h/h5253.md) not the ['owlam](../../strongs/h/h5769.md) [gᵊḇûl](../../strongs/h/h1366.md), which thy ['ab](../../strongs/h/h1.md) have ['asah](../../strongs/h/h6213.md).

<a name="proverbs_22_29"></a>Proverbs 22:29

[chazah](../../strongs/h/h2372.md) thou an ['iysh](../../strongs/h/h376.md) [māhîr](../../strongs/h/h4106.md) in his [mĕla'kah](../../strongs/h/h4399.md)? he shall [yatsab](../../strongs/h/h3320.md) [paniym](../../strongs/h/h6440.md) [melek](../../strongs/h/h4428.md); he shall not [yatsab](../../strongs/h/h3320.md) [paniym](../../strongs/h/h6440.md) [ḥāšōḵ](../../strongs/h/h2823.md) men.

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 21](proverbs_21.md) - [Proverbs 23](proverbs_23.md)