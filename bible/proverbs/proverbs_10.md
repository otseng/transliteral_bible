# [Proverbs 10](https://www.blueletterbible.org/kjv/proverbs/10)

<a name="proverbs_10_1"></a>Proverbs 10:1

The [māšāl](../../strongs/h/h4912.md) of [Šᵊlōmô](../../strongs/h/h8010.md). A [ḥāḵām](../../strongs/h/h2450.md) [ben](../../strongs/h/h1121.md) maketh a [samach](../../strongs/h/h8055.md) ['ab](../../strongs/h/h1.md): but a [kᵊsîl](../../strongs/h/h3684.md) [ben](../../strongs/h/h1121.md) is the [tûḡâ](../../strongs/h/h8424.md) of his ['em](../../strongs/h/h517.md).

<a name="proverbs_10_2"></a>Proverbs 10:2

['ôṣār](../../strongs/h/h214.md) of [resha'](../../strongs/h/h7562.md) [yāʿal](../../strongs/h/h3276.md) nothing: but [tsedaqah](../../strongs/h/h6666.md) [natsal](../../strongs/h/h5337.md) from [maveth](../../strongs/h/h4194.md).

<a name="proverbs_10_3"></a>Proverbs 10:3

[Yĕhovah](../../strongs/h/h3068.md) will not [rāʿēḇ](../../strongs/h/h7456.md) the [nephesh](../../strongs/h/h5315.md) of the [tsaddiyq](../../strongs/h/h6662.md) to [rāʿēḇ](../../strongs/h/h7456.md): but he [hāḏap̄](../../strongs/h/h1920.md) the [havvah](../../strongs/h/h1942.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="proverbs_10_4"></a>Proverbs 10:4

He becometh [rûš](../../strongs/h/h7326.md) that ['asah](../../strongs/h/h6213.md) with a [rᵊmîyâ](../../strongs/h/h7423.md) [kaph](../../strongs/h/h3709.md): but the [yad](../../strongs/h/h3027.md) of the [ḥārûṣ](../../strongs/h/h2742.md) maketh [ʿāšar](../../strongs/h/h6238.md).

<a name="proverbs_10_5"></a>Proverbs 10:5

He that ['āḡar](../../strongs/h/h103.md) in [qayiṣ](../../strongs/h/h7019.md) is a [sakal](../../strongs/h/h7919.md) [ben](../../strongs/h/h1121.md): but he that [rāḏam](../../strongs/h/h7290.md) in [qāṣîr](../../strongs/h/h7105.md) is a [ben](../../strongs/h/h1121.md) that causeth [buwsh](../../strongs/h/h954.md).

<a name="proverbs_10_6"></a>Proverbs 10:6

[bĕrakah](../../strongs/h/h1293.md) are upon the [ro'sh](../../strongs/h/h7218.md) of the [tsaddiyq](../../strongs/h/h6662.md): but [chamac](../../strongs/h/h2555.md) [kāsâ](../../strongs/h/h3680.md) the [peh](../../strongs/h/h6310.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="proverbs_10_7"></a>Proverbs 10:7

The [zeker](../../strongs/h/h2143.md) of the [tsaddiyq](../../strongs/h/h6662.md) is [bĕrakah](../../strongs/h/h1293.md): but the [shem](../../strongs/h/h8034.md) of the [rasha'](../../strongs/h/h7563.md) shall [rāqaḇ](../../strongs/h/h7537.md).

<a name="proverbs_10_8"></a>Proverbs 10:8

The [ḥāḵām](../../strongs/h/h2450.md) in [leb](../../strongs/h/h3820.md) will [laqach](../../strongs/h/h3947.md) [mitsvah](../../strongs/h/h4687.md): but a [saphah](../../strongs/h/h8193.md) ['ĕvîl](../../strongs/h/h191.md) shall [lāḇaṭ](../../strongs/h/h3832.md).

<a name="proverbs_10_9"></a>Proverbs 10:9

He that [yālaḵ](../../strongs/h/h3212.md) [tom](../../strongs/h/h8537.md) [halak](../../strongs/h/h1980.md) [betach](../../strongs/h/h983.md): but he that [ʿāqaš](../../strongs/h/h6140.md) his [derek](../../strongs/h/h1870.md) shall be [yada'](../../strongs/h/h3045.md).

<a name="proverbs_10_10"></a>Proverbs 10:10

He that [qāraṣ](../../strongs/h/h7169.md) with the ['ayin](../../strongs/h/h5869.md) [nathan](../../strongs/h/h5414.md) ['atstsebeth](../../strongs/h/h6094.md): but a [saphah](../../strongs/h/h8193.md) ['ĕvîl](../../strongs/h/h191.md) shall [lāḇaṭ](../../strongs/h/h3832.md).

<a name="proverbs_10_11"></a>Proverbs 10:11

The [peh](../../strongs/h/h6310.md) of a [tsaddiyq](../../strongs/h/h6662.md) man is a [māqôr](../../strongs/h/h4726.md) of [chay](../../strongs/h/h2416.md): but [chamac](../../strongs/h/h2555.md) [kāsâ](../../strongs/h/h3680.md) the [peh](../../strongs/h/h6310.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="proverbs_10_12"></a>Proverbs 10:12

[śin'â](../../strongs/h/h8135.md) [ʿûr](../../strongs/h/h5782.md) [mᵊḏān](../../strongs/h/h4090.md): but ['ahăḇâ](../../strongs/h/h160.md) [kāsâ](../../strongs/h/h3680.md) all [pesha'](../../strongs/h/h6588.md).

<a name="proverbs_10_13"></a>Proverbs 10:13

In the [saphah](../../strongs/h/h8193.md) of him that hath [bîn](../../strongs/h/h995.md) [ḥāḵmâ](../../strongs/h/h2451.md) is [māṣā'](../../strongs/h/h4672.md): but a [shebet](../../strongs/h/h7626.md) is for the [gēv](../../strongs/h/h1460.md) of him that is [ḥāsēr](../../strongs/h/h2638.md) of [leb](../../strongs/h/h3820.md).

<a name="proverbs_10_14"></a>Proverbs 10:14

[ḥāḵām](../../strongs/h/h2450.md) men [tsaphan](../../strongs/h/h6845.md) [da'ath](../../strongs/h/h1847.md): but the [peh](../../strongs/h/h6310.md) of the ['ĕvîl](../../strongs/h/h191.md) is [qarowb](../../strongs/h/h7138.md) [mᵊḥitâ](../../strongs/h/h4288.md).

<a name="proverbs_10_15"></a>Proverbs 10:15

The [ʿāšîr](../../strongs/h/h6223.md) [hôn](../../strongs/h/h1952.md) is his ['oz](../../strongs/h/h5797.md) [qiryâ](../../strongs/h/h7151.md): the [mᵊḥitâ](../../strongs/h/h4288.md) of the [dal](../../strongs/h/h1800.md) is their [rêš](../../strongs/h/h7389.md).

<a name="proverbs_10_16"></a>Proverbs 10:16

The [pe'ullah](../../strongs/h/h6468.md) of the [tsaddiyq](../../strongs/h/h6662.md) tendeth to [chay](../../strongs/h/h2416.md): the [tᵊḇû'â](../../strongs/h/h8393.md) of the [rasha'](../../strongs/h/h7563.md) to [chatta'ath](../../strongs/h/h2403.md).

<a name="proverbs_10_17"></a>Proverbs 10:17

He is in the ['orach](../../strongs/h/h734.md) of [chay](../../strongs/h/h2416.md) that [shamar](../../strongs/h/h8104.md) [mûsār](../../strongs/h/h4148.md): but he that ['azab](../../strongs/h/h5800.md) [tôḵēḥâ](../../strongs/h/h8433.md) [tāʿâ](../../strongs/h/h8582.md).

<a name="proverbs_10_18"></a>Proverbs 10:18

He that [kāsâ](../../strongs/h/h3680.md) [śin'â](../../strongs/h/h8135.md) with [sheqer](../../strongs/h/h8267.md) [saphah](../../strongs/h/h8193.md), and he that [yāṣā'](../../strongs/h/h3318.md) a [dibâ](../../strongs/h/h1681.md), is a [kᵊsîl](../../strongs/h/h3684.md).

<a name="proverbs_10_19"></a>Proverbs 10:19

In the [rōḇ](../../strongs/h/h7230.md) of [dabar](../../strongs/h/h1697.md) there [ḥāḏal](../../strongs/h/h2308.md) not [pesha'](../../strongs/h/h6588.md): but he that [ḥāśaḵ](../../strongs/h/h2820.md) his [saphah](../../strongs/h/h8193.md) is [sakal](../../strongs/h/h7919.md).

<a name="proverbs_10_20"></a>Proverbs 10:20

The [lashown](../../strongs/h/h3956.md) of the [tsaddiyq](../../strongs/h/h6662.md) is as [bāḥar](../../strongs/h/h977.md) [keceph](../../strongs/h/h3701.md): the [leb](../../strongs/h/h3820.md) of the [rasha'](../../strongs/h/h7563.md) is [mᵊʿaṭ](../../strongs/h/h4592.md).

<a name="proverbs_10_21"></a>Proverbs 10:21

The [saphah](../../strongs/h/h8193.md) of the [tsaddiyq](../../strongs/h/h6662.md) [ra'ah](../../strongs/h/h7462.md) [rab](../../strongs/h/h7227.md): but ['ĕvîl](../../strongs/h/h191.md) [muwth](../../strongs/h/h4191.md) for [ḥāsēr](../../strongs/h/h2638.md) of [leb](../../strongs/h/h3820.md).

<a name="proverbs_10_22"></a>Proverbs 10:22

The [bĕrakah](../../strongs/h/h1293.md) of [Yĕhovah](../../strongs/h/h3068.md), it maketh [ʿāšar](../../strongs/h/h6238.md), and he [yāsap̄](../../strongs/h/h3254.md) no ['etseb](../../strongs/h/h6089.md) with it.

<a name="proverbs_10_23"></a>Proverbs 10:23

It is as [śᵊḥôq](../../strongs/h/h7814.md) to a [kᵊsîl](../../strongs/h/h3684.md) to ['asah](../../strongs/h/h6213.md) [zimmâ](../../strongs/h/h2154.md): but an ['iysh](../../strongs/h/h376.md) of [tāḇûn](../../strongs/h/h8394.md) hath [ḥāḵmâ](../../strongs/h/h2451.md).

<a name="proverbs_10_24"></a>Proverbs 10:24

The [mᵊḡôrâ](../../strongs/h/h4034.md) of the [rasha'](../../strongs/h/h7563.md), it shall [bow'](../../strongs/h/h935.md) upon him: but the [ta'avah](../../strongs/h/h8378.md) of the [tsaddiyq](../../strongs/h/h6662.md) shall be [nathan](../../strongs/h/h5414.md).

<a name="proverbs_10_25"></a>Proverbs 10:25

As the [sûp̄â](../../strongs/h/h5492.md) ['abar](../../strongs/h/h5674.md), so is the [rasha'](../../strongs/h/h7563.md) no more: but the [tsaddiyq](../../strongs/h/h6662.md) is an ['owlam](../../strongs/h/h5769.md) [yᵊsôḏ](../../strongs/h/h3247.md).

<a name="proverbs_10_26"></a>Proverbs 10:26

As [ḥōmeṣ](../../strongs/h/h2558.md) to the [šēn](../../strongs/h/h8127.md), and as ['ashan](../../strongs/h/h6227.md) to the ['ayin](../../strongs/h/h5869.md), so is the [ʿāṣēl](../../strongs/h/h6102.md) to them that [shalach](../../strongs/h/h7971.md) him.

<a name="proverbs_10_27"></a>Proverbs 10:27

The [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) [yāsap̄](../../strongs/h/h3254.md) [yowm](../../strongs/h/h3117.md): but the [šānâ](../../strongs/h/h8141.md) of the [rasha'](../../strongs/h/h7563.md) shall be [qāṣar](../../strongs/h/h7114.md).

<a name="proverbs_10_28"></a>Proverbs 10:28

The [tôḥeleṯ](../../strongs/h/h8431.md) of the [tsaddiyq](../../strongs/h/h6662.md) shall be [simchah](../../strongs/h/h8057.md): but the [tiqvâ](../../strongs/h/h8615.md) of the [rasha'](../../strongs/h/h7563.md) shall ['abad](../../strongs/h/h6.md).

<a name="proverbs_10_29"></a>Proverbs 10:29

The [derek](../../strongs/h/h1870.md) of [Yĕhovah](../../strongs/h/h3068.md) is [māʿôz](../../strongs/h/h4581.md) to the [tom](../../strongs/h/h8537.md): but [mᵊḥitâ](../../strongs/h/h4288.md) shall be to the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md).

<a name="proverbs_10_30"></a>Proverbs 10:30

The [tsaddiyq](../../strongs/h/h6662.md) shall ['owlam](../../strongs/h/h5769.md) be [mowt](../../strongs/h/h4131.md): but the [rasha'](../../strongs/h/h7563.md) shall not [shakan](../../strongs/h/h7931.md) the ['erets](../../strongs/h/h776.md).

<a name="proverbs_10_31"></a>Proverbs 10:31

The [peh](../../strongs/h/h6310.md) of the [tsaddiyq](../../strongs/h/h6662.md) bringeth [nûḇ](../../strongs/h/h5107.md) [ḥāḵmâ](../../strongs/h/h2451.md): but the [tahpuḵôṯ](../../strongs/h/h8419.md) [lashown](../../strongs/h/h3956.md) shall be [karath](../../strongs/h/h3772.md).

<a name="proverbs_10_32"></a>Proverbs 10:32

The [saphah](../../strongs/h/h8193.md) of the [tsaddiyq](../../strongs/h/h6662.md) [yada'](../../strongs/h/h3045.md) what is [ratsown](../../strongs/h/h7522.md): but the [peh](../../strongs/h/h6310.md) of the [rasha'](../../strongs/h/h7563.md) speaketh [tahpuḵôṯ](../../strongs/h/h8419.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 9](proverbs_9.md) - [Proverbs 11](proverbs_11.md)