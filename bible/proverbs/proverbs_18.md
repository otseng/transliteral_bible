# [Proverbs 18](https://www.blueletterbible.org/kjv/proverbs/18)

<a name="proverbs_18_1"></a>Proverbs 18:1

Through [ta'avah](../../strongs/h/h8378.md) a man, having [pāraḏ](../../strongs/h/h6504.md) himself, [bāqaš](../../strongs/h/h1245.md) and [gālaʿ](../../strongs/h/h1566.md) with all [tûšîyâ](../../strongs/h/h8454.md).

<a name="proverbs_18_2"></a>Proverbs 18:2

A [kᵊsîl](../../strongs/h/h3684.md) hath no [ḥāp̄ēṣ](../../strongs/h/h2654.md) in [tāḇûn](../../strongs/h/h8394.md), but that his [leb](../../strongs/h/h3820.md) may [gālâ](../../strongs/h/h1540.md) itself.

<a name="proverbs_18_3"></a>Proverbs 18:3

When the [rasha'](../../strongs/h/h7563.md) [bow'](../../strongs/h/h935.md), then [bow'](../../strongs/h/h935.md) also [bûz](../../strongs/h/h937.md), and with [qālôn](../../strongs/h/h7036.md) [cherpah](../../strongs/h/h2781.md).

<a name="proverbs_18_4"></a>Proverbs 18:4

The [dabar](../../strongs/h/h1697.md) of an ['iysh](../../strongs/h/h376.md) [peh](../../strongs/h/h6310.md) are as [ʿāmōq](../../strongs/h/h6013.md) [mayim](../../strongs/h/h4325.md), and the [māqôr](../../strongs/h/h4726.md) of [ḥāḵmâ](../../strongs/h/h2451.md) as a [nāḇaʿ](../../strongs/h/h5042.md) [nachal](../../strongs/h/h5158.md).

<a name="proverbs_18_5"></a>Proverbs 18:5

It is not [towb](../../strongs/h/h2896.md) to [nasa'](../../strongs/h/h5375.md) the [paniym](../../strongs/h/h6440.md) of the [rasha'](../../strongs/h/h7563.md), to [natah](../../strongs/h/h5186.md) the [tsaddiyq](../../strongs/h/h6662.md) in [mishpat](../../strongs/h/h4941.md).

<a name="proverbs_18_6"></a>Proverbs 18:6

A [kᵊsîl](../../strongs/h/h3684.md) [saphah](../../strongs/h/h8193.md) [bow'](../../strongs/h/h935.md) into [rîḇ](../../strongs/h/h7379.md), and his [peh](../../strongs/h/h6310.md) [qara'](../../strongs/h/h7121.md) for [mahălummâ](../../strongs/h/h4112.md).

<a name="proverbs_18_7"></a>Proverbs 18:7

A [kᵊsîl](../../strongs/h/h3684.md) [peh](../../strongs/h/h6310.md) is his [mᵊḥitâ](../../strongs/h/h4288.md), and his [saphah](../../strongs/h/h8193.md) are the [mowqesh](../../strongs/h/h4170.md) of his [nephesh](../../strongs/h/h5315.md).

<a name="proverbs_18_8"></a>Proverbs 18:8

The [dabar](../../strongs/h/h1697.md) of a [nirgān](../../strongs/h/h5372.md) are as [lāhēm](../../strongs/h/h3859.md), and they [yarad](../../strongs/h/h3381.md) into the [ḥeḏer](../../strongs/h/h2315.md) of the [beten](../../strongs/h/h990.md).

<a name="proverbs_18_9"></a>Proverbs 18:9

He also that is [rāp̄â](../../strongs/h/h7503.md) in his [mĕla'kah](../../strongs/h/h4399.md) is ['ach](../../strongs/h/h251.md) to him that is a [baʿal](../../strongs/h/h1167.md) [shachath](../../strongs/h/h7843.md).

<a name="proverbs_18_10"></a>Proverbs 18:10

The [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) is an ['oz](../../strongs/h/h5797.md) [miḡdāl](../../strongs/h/h4026.md): the [tsaddiyq](../../strongs/h/h6662.md) [rûṣ](../../strongs/h/h7323.md) into it, and is [śāḡaḇ](../../strongs/h/h7682.md).

<a name="proverbs_18_11"></a>Proverbs 18:11

The [ʿāšîr](../../strongs/h/h6223.md) [hôn](../../strongs/h/h1952.md) is his ['oz](../../strongs/h/h5797.md) [qiryâ](../../strongs/h/h7151.md), and as a [śāḡaḇ](../../strongs/h/h7682.md) [ḥômâ](../../strongs/h/h2346.md) in his own [maśkîṯ](../../strongs/h/h4906.md).

<a name="proverbs_18_12"></a>Proverbs 18:12

[paniym](../../strongs/h/h6440.md) [šeḇar](../../strongs/h/h7667.md) the [leb](../../strongs/h/h3820.md) of ['iysh](../../strongs/h/h376.md) is [gāḇah](../../strongs/h/h1361.md), and [paniym](../../strongs/h/h6440.md) [kabowd](../../strongs/h/h3519.md) is [ʿănāvâ](../../strongs/h/h6038.md).

<a name="proverbs_18_13"></a>Proverbs 18:13

He that [shuwb](../../strongs/h/h7725.md) a [dabar](../../strongs/h/h1697.md) before he [shama'](../../strongs/h/h8085.md) it, it is ['iûeleṯ](../../strongs/h/h200.md) and [kĕlimmah](../../strongs/h/h3639.md) unto him.

<a name="proverbs_18_14"></a>Proverbs 18:14

The [ruwach](../../strongs/h/h7307.md) of an ['iysh](../../strongs/h/h376.md) will [kûl](../../strongs/h/h3557.md) his [maḥălê](../../strongs/h/h4245.md); but a [nāḵā'](../../strongs/h/h5218.md) [ruwach](../../strongs/h/h7307.md) who can [nasa'](../../strongs/h/h5375.md)?

<a name="proverbs_18_15"></a>Proverbs 18:15

The [leb](../../strongs/h/h3820.md) of the [bîn](../../strongs/h/h995.md) [qānâ](../../strongs/h/h7069.md) [da'ath](../../strongs/h/h1847.md); and the ['ozen](../../strongs/h/h241.md) of the [ḥāḵām](../../strongs/h/h2450.md) [bāqaš](../../strongs/h/h1245.md) [da'ath](../../strongs/h/h1847.md).

<a name="proverbs_18_16"></a>Proverbs 18:16

An ['āḏām](../../strongs/h/h120.md) [matān](../../strongs/h/h4976.md) maketh [rāḥaḇ](../../strongs/h/h7337.md) for him, and [nachah](../../strongs/h/h5148.md) him [paniym](../../strongs/h/h6440.md) great [gadowl](../../strongs/h/h1419.md).

<a name="proverbs_18_17"></a>Proverbs 18:17

He that is [ri'šôn](../../strongs/h/h7223.md) in his own [rîḇ](../../strongs/h/h7379.md) seemeth [tsaddiyq](../../strongs/h/h6662.md); but his [rea'](../../strongs/h/h7453.md) [bow'](../../strongs/h/h935.md) [bow'](../../strongs/h/h935.md) and [chaqar](../../strongs/h/h2713.md) him.

<a name="proverbs_18_18"></a>Proverbs 18:18

The [gôrāl](../../strongs/h/h1486.md) causeth [miḏyān](../../strongs/h/h4079.md) to [shabath](../../strongs/h/h7673.md), and [pāraḏ](../../strongs/h/h6504.md) between the ['atsuwm](../../strongs/h/h6099.md).

<a name="proverbs_18_19"></a>Proverbs 18:19

An ['ach](../../strongs/h/h251.md) [pāšaʿ](../../strongs/h/h6586.md) is harder to be won than an ['oz](../../strongs/h/h5797.md) [qiryâ](../../strongs/h/h7151.md): and their [miḏyān](../../strongs/h/h4079.md) [māḏôn](../../strongs/h/h4066.md) are like the [bᵊrîaḥ](../../strongs/h/h1280.md) of an ['armôn](../../strongs/h/h759.md).

<a name="proverbs_18_20"></a>Proverbs 18:20

An ['iysh](../../strongs/h/h376.md) [beten](../../strongs/h/h990.md) shall be [sāׂbaʿ](../../strongs/h/h7646.md) with the [pĕriy](../../strongs/h/h6529.md) of his [peh](../../strongs/h/h6310.md); and with the [tᵊḇû'â](../../strongs/h/h8393.md) of his [saphah](../../strongs/h/h8193.md) shall he be [sāׂbaʿ](../../strongs/h/h7646.md).

<a name="proverbs_18_21"></a>Proverbs 18:21

[maveth](../../strongs/h/h4194.md) and [chay](../../strongs/h/h2416.md) are in the [yad](../../strongs/h/h3027.md) of the [lashown](../../strongs/h/h3956.md): and they that ['ahab](../../strongs/h/h157.md) it shall ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) thereof.

<a name="proverbs_18_22"></a>Proverbs 18:22

Whoso [māṣā'](../../strongs/h/h4672.md) an ['ishshah](../../strongs/h/h802.md) [māṣā'](../../strongs/h/h4672.md) a [towb](../../strongs/h/h2896.md) thing, and [pûq](../../strongs/h/h6329.md) [ratsown](../../strongs/h/h7522.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="proverbs_18_23"></a>Proverbs 18:23

The [rûš](../../strongs/h/h7326.md) [dabar](../../strongs/h/h1696.md) [taḥănûn](../../strongs/h/h8469.md); but the [ʿāšîr](../../strongs/h/h6223.md) ['anah](../../strongs/h/h6030.md) ['az](../../strongs/h/h5794.md).

<a name="proverbs_18_24"></a>Proverbs 18:24

An ['iysh](../../strongs/h/h376.md) that hath [rea'](../../strongs/h/h7453.md) must shew himself [ra'a'](../../strongs/h/h7489.md): and there [yēš](../../strongs/h/h3426.md) an ['ahab](../../strongs/h/h157.md) that [dāḇēq](../../strongs/h/h1695.md) than an ['ach](../../strongs/h/h251.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 17](proverbs_17.md) - [Proverbs 19](proverbs_19.md)