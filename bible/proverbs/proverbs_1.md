# [Proverbs 1](https://www.blueletterbible.org/kjv/proverbs/1)

<a name="proverbs_1_1"></a>Proverbs 1:1

The [māšāl](../../strongs/h/h4912.md) of [Šᵊlōmô](../../strongs/h/h8010.md) the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md), [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md);

<a name="proverbs_1_2"></a>Proverbs 1:2

To [yada'](../../strongs/h/h3045.md) [ḥāḵmâ](../../strongs/h/h2451.md) and [mûsār](../../strongs/h/h4148.md); to [bîn](../../strongs/h/h995.md) the ['emer](../../strongs/h/h561.md) of [bînâ](../../strongs/h/h998.md);

<a name="proverbs_1_3"></a>Proverbs 1:3

To [laqach](../../strongs/h/h3947.md) the [mûsār](../../strongs/h/h4148.md) of [sakal](../../strongs/h/h7919.md), [tsedeq](../../strongs/h/h6664.md), and [mishpat](../../strongs/h/h4941.md), and [meyshar](../../strongs/h/h4339.md);

<a name="proverbs_1_4"></a>Proverbs 1:4

To [nathan](../../strongs/h/h5414.md) [ʿārmâ](../../strongs/h/h6195.md) to the [pᵊṯî](../../strongs/h/h6612.md), to the [naʿar](../../strongs/h/h5288.md) [da'ath](../../strongs/h/h1847.md) and [mezimmah](../../strongs/h/h4209.md).

<a name="proverbs_1_5"></a>Proverbs 1:5

A [ḥāḵām](../../strongs/h/h2450.md) man will [shama'](../../strongs/h/h8085.md), and will [yāsap̄](../../strongs/h/h3254.md) [leqaḥ](../../strongs/h/h3948.md); and a man of [bîn](../../strongs/h/h995.md) shall [qānâ](../../strongs/h/h7069.md) unto [taḥbulôṯ](../../strongs/h/h8458.md):

<a name="proverbs_1_6"></a>Proverbs 1:6

To [bîn](../../strongs/h/h995.md) a [māšāl](../../strongs/h/h4912.md), and the [mᵊlîṣâ](../../strongs/h/h4426.md); the [dabar](../../strongs/h/h1697.md) of the [ḥāḵām](../../strongs/h/h2450.md), and their [ḥîḏâ](../../strongs/h/h2420.md).

<a name="proverbs_1_7"></a>Proverbs 1:7

The [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) is the [re'shiyth](../../strongs/h/h7225.md) of [da'ath](../../strongs/h/h1847.md): but ['ĕvîl](../../strongs/h/h191.md) [bûz](../../strongs/h/h936.md) [ḥāḵmâ](../../strongs/h/h2451.md) and [mûsār](../../strongs/h/h4148.md).

<a name="proverbs_1_8"></a>Proverbs 1:8

My [ben](../../strongs/h/h1121.md), [shama'](../../strongs/h/h8085.md) the [mûsār](../../strongs/h/h4148.md) of thy ['ab](../../strongs/h/h1.md), and [nāṭaš](../../strongs/h/h5203.md) not the [towrah](../../strongs/h/h8451.md) of thy ['em](../../strongs/h/h517.md):

<a name="proverbs_1_9"></a>Proverbs 1:9

For they shall be a [livyâ](../../strongs/h/h3880.md) of [ḥēn](../../strongs/h/h2580.md) unto thy [ro'sh](../../strongs/h/h7218.md), and [ʿĂnāq](../../strongs/h/h6060.md) about thy [gargᵊrôṯ](../../strongs/h/h1621.md).

<a name="proverbs_1_10"></a>Proverbs 1:10

My [ben](../../strongs/h/h1121.md), if [chatta'](../../strongs/h/h2400.md) [pāṯâ](../../strongs/h/h6601.md) thee, ['āḇâ](../../strongs/h/h14.md) thou not.

<a name="proverbs_1_11"></a>Proverbs 1:11

If they ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) with us, let us ['arab](../../strongs/h/h693.md) for [dam](../../strongs/h/h1818.md), let us [tsaphan](../../strongs/h/h6845.md) privily for the [naqiy](../../strongs/h/h5355.md) without [ḥinnām](../../strongs/h/h2600.md):

<a name="proverbs_1_12"></a>Proverbs 1:12

Let us [bālaʿ](../../strongs/h/h1104.md) them [chay](../../strongs/h/h2416.md) as the [shĕ'owl](../../strongs/h/h7585.md); and [tamiym](../../strongs/h/h8549.md), as those that [yarad](../../strongs/h/h3381.md) into the [bowr](../../strongs/h/h953.md):

<a name="proverbs_1_13"></a>Proverbs 1:13

We shall [māṣā'](../../strongs/h/h4672.md) all [yāqār](../../strongs/h/h3368.md) [hôn](../../strongs/h/h1952.md), we shall [mālā'](../../strongs/h/h4390.md) our [bayith](../../strongs/h/h1004.md) with [šālāl](../../strongs/h/h7998.md):

<a name="proverbs_1_14"></a>Proverbs 1:14

[naphal](../../strongs/h/h5307.md) in thy [gôrāl](../../strongs/h/h1486.md) [tavek](../../strongs/h/h8432.md) us; let us all have ['echad](../../strongs/h/h259.md) [kîs](../../strongs/h/h3599.md):

<a name="proverbs_1_15"></a>Proverbs 1:15

My [ben](../../strongs/h/h1121.md), [yālaḵ](../../strongs/h/h3212.md) not thou in the [derek](../../strongs/h/h1870.md) with them; [mānaʿ](../../strongs/h/h4513.md) thy [regel](../../strongs/h/h7272.md) from their [nāṯîḇ](../../strongs/h/h5410.md):

<a name="proverbs_1_16"></a>Proverbs 1:16

For their [regel](../../strongs/h/h7272.md) [rûṣ](../../strongs/h/h7323.md) to [ra'](../../strongs/h/h7451.md), and make [māhar](../../strongs/h/h4116.md) to [šāp̄aḵ](../../strongs/h/h8210.md) [dam](../../strongs/h/h1818.md).

<a name="proverbs_1_17"></a>Proverbs 1:17

Surely in [ḥinnām](../../strongs/h/h2600.md) the [rešeṯ](../../strongs/h/h7568.md) is [zārâ](../../strongs/h/h2219.md) in the ['ayin](../../strongs/h/h5869.md) of any [baʿal](../../strongs/h/h1167.md) [kanaph](../../strongs/h/h3671.md).

<a name="proverbs_1_18"></a>Proverbs 1:18

And they lay ['arab](../../strongs/h/h693.md) for their own [dam](../../strongs/h/h1818.md); they [tsaphan](../../strongs/h/h6845.md) for their own [nephesh](../../strongs/h/h5315.md).

<a name="proverbs_1_19"></a>Proverbs 1:19

So are the ['orach](../../strongs/h/h734.md) of every one that is [batsa'](../../strongs/h/h1214.md) of [beṣaʿ](../../strongs/h/h1215.md); which [laqach](../../strongs/h/h3947.md) the [nephesh](../../strongs/h/h5315.md) of the [baʿal](../../strongs/h/h1167.md) thereof.

<a name="proverbs_1_20"></a>Proverbs 1:20

[ḥāḵmôṯ](../../strongs/h/h2454.md) [ranan](../../strongs/h/h7442.md) [ḥûṣ](../../strongs/h/h2351.md); she [nathan](../../strongs/h/h5414.md) her [qowl](../../strongs/h/h6963.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md):

<a name="proverbs_1_21"></a>Proverbs 1:21

She [qara'](../../strongs/h/h7121.md) in the [ro'sh](../../strongs/h/h7218.md) of [hāmâ](../../strongs/h/h1993.md), in the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md): in the [ʿîr](../../strongs/h/h5892.md) she ['āmar](../../strongs/h/h559.md) her ['emer](../../strongs/h/h561.md), saying,

<a name="proverbs_1_22"></a>Proverbs 1:22

How long, ye [pᵊṯî](../../strongs/h/h6612.md), will ye ['ahab](../../strongs/h/h157.md) [pᵊṯî](../../strongs/h/h6612.md)? and the [luwts](../../strongs/h/h3887.md) [chamad](../../strongs/h/h2530.md) in their [lāṣôn](../../strongs/h/h3944.md), and [kᵊsîl](../../strongs/h/h3684.md) [sane'](../../strongs/h/h8130.md) [da'ath](../../strongs/h/h1847.md)?

<a name="proverbs_1_23"></a>Proverbs 1:23

[shuwb](../../strongs/h/h7725.md) you at my [tôḵēḥâ](../../strongs/h/h8433.md): behold, I will [nāḇaʿ](../../strongs/h/h5042.md) my [ruwach](../../strongs/h/h7307.md) unto you, I will [yada'](../../strongs/h/h3045.md) my [dabar](../../strongs/h/h1697.md) unto you.

<a name="proverbs_1_24"></a>Proverbs 1:24

Because I have [qara'](../../strongs/h/h7121.md), and ye [mā'ēn](../../strongs/h/h3985.md); I have [natah](../../strongs/h/h5186.md) my [yad](../../strongs/h/h3027.md), and no man [qashab](../../strongs/h/h7181.md);

<a name="proverbs_1_25"></a>Proverbs 1:25

But ye have set at [pāraʿ](../../strongs/h/h6544.md) all my ['etsah](../../strongs/h/h6098.md), and ['āḇâ](../../strongs/h/h14.md) none of my [tôḵēḥâ](../../strongs/h/h8433.md):

<a name="proverbs_1_26"></a>Proverbs 1:26

I also will [śāḥaq](../../strongs/h/h7832.md) at your ['êḏ](../../strongs/h/h343.md); I will [lāʿaḡ](../../strongs/h/h3932.md) when your [paḥaḏ](../../strongs/h/h6343.md) [bow'](../../strongs/h/h935.md);

<a name="proverbs_1_27"></a>Proverbs 1:27

When your [paḥaḏ](../../strongs/h/h6343.md) [bow'](../../strongs/h/h935.md) as [šô'](../../strongs/h/h7722.md) [ša'ăvâ](../../strongs/h/h7584.md), and your ['êḏ](../../strongs/h/h343.md) ['āṯâ](../../strongs/h/h857.md) as a [sûp̄â](../../strongs/h/h5492.md); when [tsarah](../../strongs/h/h6869.md) and [ṣôq](../../strongs/h/h6695.md) [bow'](../../strongs/h/h935.md) upon you.

<a name="proverbs_1_28"></a>Proverbs 1:28

Then shall they [qara'](../../strongs/h/h7121.md) upon me, but I will not ['anah](../../strongs/h/h6030.md); they shall [šāḥar](../../strongs/h/h7836.md) me, but they shall not [māṣā'](../../strongs/h/h4672.md) me:

<a name="proverbs_1_29"></a>Proverbs 1:29

For that they [sane'](../../strongs/h/h8130.md) [da'ath](../../strongs/h/h1847.md), and did not [bāḥar](../../strongs/h/h977.md) the [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md):

<a name="proverbs_1_30"></a>Proverbs 1:30

They ['āḇâ](../../strongs/h/h14.md) none of my ['etsah](../../strongs/h/h6098.md): they [na'ats](../../strongs/h/h5006.md) all my [tôḵēḥâ](../../strongs/h/h8433.md).

<a name="proverbs_1_31"></a>Proverbs 1:31

Therefore shall they ['akal](../../strongs/h/h398.md) of the [pĕriy](../../strongs/h/h6529.md) of their own [derek](../../strongs/h/h1870.md), and be [sāׂbaʿ](../../strongs/h/h7646.md) with their own [mow'etsah](../../strongs/h/h4156.md).

<a name="proverbs_1_32"></a>Proverbs 1:32

For the [mᵊšûḇâ](../../strongs/h/h4878.md) of the [pᵊṯî](../../strongs/h/h6612.md) shall [harag](../../strongs/h/h2026.md) them, and the [šalvâ](../../strongs/h/h7962.md) of [kᵊsîl](../../strongs/h/h3684.md) shall ['abad](../../strongs/h/h6.md) them.

<a name="proverbs_1_33"></a>Proverbs 1:33

But whoso [shama'](../../strongs/h/h8085.md) unto me shall [shakan](../../strongs/h/h7931.md) [betach](../../strongs/h/h983.md), and shall be [šā'an](../../strongs/h/h7599.md) from [paḥaḏ](../../strongs/h/h6343.md) of [ra'](../../strongs/h/h7451.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 2](proverbs_2.md)