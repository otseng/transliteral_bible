# [Proverbs 21](https://www.blueletterbible.org/kjv/proverbs/21)

<a name="proverbs_21_1"></a>Proverbs 21:1

The [melek](../../strongs/h/h4428.md) [leb](../../strongs/h/h3820.md) is in the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md), as the [peleḡ](../../strongs/h/h6388.md) of [mayim](../../strongs/h/h4325.md): he [natah](../../strongs/h/h5186.md) it whithersoever he [ḥāp̄ēṣ](../../strongs/h/h2654.md).

<a name="proverbs_21_2"></a>Proverbs 21:2

Every [derek](../../strongs/h/h1870.md) of an ['iysh](../../strongs/h/h376.md) is [yashar](../../strongs/h/h3477.md) in his own ['ayin](../../strongs/h/h5869.md): but [Yĕhovah](../../strongs/h/h3068.md) [tāḵan](../../strongs/h/h8505.md) the [libbah](../../strongs/h/h3826.md).

<a name="proverbs_21_3"></a>Proverbs 21:3

To ['asah](../../strongs/h/h6213.md) [tsedaqah](../../strongs/h/h6666.md) and [mishpat](../../strongs/h/h4941.md) is more [bāḥar](../../strongs/h/h977.md) to [Yĕhovah](../../strongs/h/h3068.md) than [zebach](../../strongs/h/h2077.md).

<a name="proverbs_21_4"></a>Proverbs 21:4

A [rûm](../../strongs/h/h7312.md) ['ayin](../../strongs/h/h5869.md), and a [rāḥāḇ](../../strongs/h/h7342.md) [leb](../../strongs/h/h3820.md), and the [nîr](../../strongs/h/h5215.md) of the [rasha'](../../strongs/h/h7563.md), is [chatta'ath](../../strongs/h/h2403.md).

<a name="proverbs_21_5"></a>Proverbs 21:5

The [maḥăšāḇâ](../../strongs/h/h4284.md) of the [ḥārûṣ](../../strongs/h/h2742.md) tend only to [môṯār](../../strongs/h/h4195.md); but of every one that is ['ûṣ](../../strongs/h/h213.md) only to [maḥsôr](../../strongs/h/h4270.md).

<a name="proverbs_21_6"></a>Proverbs 21:6

The [pōʿal](../../strongs/h/h6467.md) of ['ôṣār](../../strongs/h/h214.md) by a [sheqer](../../strongs/h/h8267.md) [lashown](../../strongs/h/h3956.md) is a [heḇel](../../strongs/h/h1892.md) [nāḏap̄](../../strongs/h/h5086.md) them that [bāqaš](../../strongs/h/h1245.md) [maveth](../../strongs/h/h4194.md).

<a name="proverbs_21_7"></a>Proverbs 21:7

The [shod](../../strongs/h/h7701.md) of the [rasha'](../../strongs/h/h7563.md) shall [gārar](../../strongs/h/h1641.md) them; because they [mā'ēn](../../strongs/h/h3985.md) to ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md).

<a name="proverbs_21_8"></a>Proverbs 21:8

The [derek](../../strongs/h/h1870.md) of ['iysh](../../strongs/h/h376.md) is [hăp̄aḵpaḵ](../../strongs/h/h2019.md) and [vāzār](../../strongs/h/h2054.md): but as for the [zāḵ](../../strongs/h/h2134.md), his [pōʿal](../../strongs/h/h6467.md) is [yashar](../../strongs/h/h3477.md).

<a name="proverbs_21_9"></a>Proverbs 21:9

It is [towb](../../strongs/h/h2896.md) to [yashab](../../strongs/h/h3427.md) in a [pinnâ](../../strongs/h/h6438.md) of the [gāḡ](../../strongs/h/h1406.md), than with a [miḏyān](../../strongs/h/h4079.md) [māḏôn](../../strongs/h/h4066.md) ['ishshah](../../strongs/h/h802.md) in a [Ḥeḇer](../../strongs/h/h2267.md) [bayith](../../strongs/h/h1004.md).

<a name="proverbs_21_10"></a>Proverbs 21:10

The [nephesh](../../strongs/h/h5315.md) of the [rasha'](../../strongs/h/h7563.md) ['āvâ](../../strongs/h/h183.md) [ra'](../../strongs/h/h7451.md): his [rea'](../../strongs/h/h7453.md) findeth no [chanan](../../strongs/h/h2603.md) in his ['ayin](../../strongs/h/h5869.md).

<a name="proverbs_21_11"></a>Proverbs 21:11

When the [luwts](../../strongs/h/h3887.md) is [ʿānaš](../../strongs/h/h6064.md), the [pᵊṯî](../../strongs/h/h6612.md) is made [ḥāḵam](../../strongs/h/h2449.md): and when the [ḥāḵām](../../strongs/h/h2450.md) is [sakal](../../strongs/h/h7919.md), he [laqach](../../strongs/h/h3947.md) [da'ath](../../strongs/h/h1847.md).

<a name="proverbs_21_12"></a>Proverbs 21:12

The [tsaddiyq](../../strongs/h/h6662.md) man [sakal](../../strongs/h/h7919.md) the [bayith](../../strongs/h/h1004.md) of the [rasha'](../../strongs/h/h7563.md): but God [sālap̄](../../strongs/h/h5557.md) the [rasha'](../../strongs/h/h7563.md) for their [ra'](../../strongs/h/h7451.md).

<a name="proverbs_21_13"></a>Proverbs 21:13

Whoso ['āṭam](../../strongs/h/h331.md) his ['ozen](../../strongs/h/h241.md) at the [zaʿaq](../../strongs/h/h2201.md) of the [dal](../../strongs/h/h1800.md), he also shall [qara'](../../strongs/h/h7121.md) himself, but shall not be ['anah](../../strongs/h/h6030.md).

<a name="proverbs_21_14"></a>Proverbs 21:14

A [matān](../../strongs/h/h4976.md) in [cether](../../strongs/h/h5643.md) [kāp̄â](../../strongs/h/h3711.md) ['aph](../../strongs/h/h639.md): and a [shachad](../../strongs/h/h7810.md) in the [ḥêq](../../strongs/h/h2436.md) ['az](../../strongs/h/h5794.md) [chemah](../../strongs/h/h2534.md).

<a name="proverbs_21_15"></a>Proverbs 21:15

It is [simchah](../../strongs/h/h8057.md) to the [tsaddiyq](../../strongs/h/h6662.md) to ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md): but [mᵊḥitâ](../../strongs/h/h4288.md) shall be to the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md).

<a name="proverbs_21_16"></a>Proverbs 21:16

The ['āḏām](../../strongs/h/h120.md) that [tāʿâ](../../strongs/h/h8582.md) out of the [derek](../../strongs/h/h1870.md) of [sakal](../../strongs/h/h7919.md) shall [nuwach](../../strongs/h/h5117.md) in the [qāhēl](../../strongs/h/h6951.md) of the [rᵊp̄ā'îm](../../strongs/h/h7496.md).

<a name="proverbs_21_17"></a>Proverbs 21:17

He that ['ahab](../../strongs/h/h157.md) [simchah](../../strongs/h/h8057.md) shall be a [maḥsôr](../../strongs/h/h4270.md) ['iysh](../../strongs/h/h376.md): he that ['ahab](../../strongs/h/h157.md) [yayin](../../strongs/h/h3196.md) and [šemen](../../strongs/h/h8081.md) shall not be [ʿāšar](../../strongs/h/h6238.md).

<a name="proverbs_21_18"></a>Proverbs 21:18

The [rasha'](../../strongs/h/h7563.md) shall be a [kōp̄er](../../strongs/h/h3724.md) for the [tsaddiyq](../../strongs/h/h6662.md), and the [bāḡaḏ](../../strongs/h/h898.md) for the [yashar](../../strongs/h/h3477.md).

<a name="proverbs_21_19"></a>Proverbs 21:19

It is [towb](../../strongs/h/h2896.md) to [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) [midbar](../../strongs/h/h4057.md), than with a [miḏyān](../../strongs/h/h4079.md) [māḏôn](../../strongs/h/h4066.md) and a [ka'ac](../../strongs/h/h3708.md) ['ishshah](../../strongs/h/h802.md).

<a name="proverbs_21_20"></a>Proverbs 21:20

There is ['ôṣār](../../strongs/h/h214.md) to be [chamad](../../strongs/h/h2530.md) and [šemen](../../strongs/h/h8081.md) in the [nāvê](../../strongs/h/h5116.md) of the [ḥāḵām](../../strongs/h/h2450.md); but a [kᵊsîl](../../strongs/h/h3684.md) ['āḏām](../../strongs/h/h120.md) spendeth it [bālaʿ](../../strongs/h/h1104.md).

<a name="proverbs_21_21"></a>Proverbs 21:21

He that [radaph](../../strongs/h/h7291.md) after [tsedaqah](../../strongs/h/h6666.md) and [checed](../../strongs/h/h2617.md) [māṣā'](../../strongs/h/h4672.md) [chay](../../strongs/h/h2416.md), [tsedaqah](../../strongs/h/h6666.md), and [kabowd](../../strongs/h/h3519.md).

<a name="proverbs_21_22"></a>Proverbs 21:22

A [ḥāḵām](../../strongs/h/h2450.md) man [ʿālâ](../../strongs/h/h5927.md) the [ʿîr](../../strongs/h/h5892.md) of the [gibôr](../../strongs/h/h1368.md), and [yarad](../../strongs/h/h3381.md) the ['oz](../../strongs/h/h5797.md) of the [miḇṭāḥ](../../strongs/h/h4009.md) thereof.

<a name="proverbs_21_23"></a>Proverbs 21:23

Whoso [shamar](../../strongs/h/h8104.md) his [peh](../../strongs/h/h6310.md) and his [lashown](../../strongs/h/h3956.md) [shamar](../../strongs/h/h8104.md) his [nephesh](../../strongs/h/h5315.md) from [tsarah](../../strongs/h/h6869.md).

<a name="proverbs_21_24"></a>Proverbs 21:24

[zed](../../strongs/h/h2086.md) and [yāhîr](../../strongs/h/h3093.md) [luwts](../../strongs/h/h3887.md) is his [shem](../../strongs/h/h8034.md), who ['asah](../../strongs/h/h6213.md) in [zāḏôn](../../strongs/h/h2087.md) ['ebrah](../../strongs/h/h5678.md).

<a name="proverbs_21_25"></a>Proverbs 21:25

The [ta'avah](../../strongs/h/h8378.md) of the [ʿāṣēl](../../strongs/h/h6102.md) [muwth](../../strongs/h/h4191.md) him; for his [yad](../../strongs/h/h3027.md) [mā'ēn](../../strongs/h/h3985.md) to ['asah](../../strongs/h/h6213.md).

<a name="proverbs_21_26"></a>Proverbs 21:26

He ['āvâ](../../strongs/h/h183.md) [ta'avah](../../strongs/h/h8378.md) all the [yowm](../../strongs/h/h3117.md) long: but the [tsaddiyq](../../strongs/h/h6662.md) [nathan](../../strongs/h/h5414.md) and [ḥāśaḵ](../../strongs/h/h2820.md) not.

<a name="proverbs_21_27"></a>Proverbs 21:27

The [zebach](../../strongs/h/h2077.md) of the [rasha'](../../strongs/h/h7563.md) is [tôʿēḇâ](../../strongs/h/h8441.md): how much more, when he [bow'](../../strongs/h/h935.md) it with a [zimmâ](../../strongs/h/h2154.md)?

<a name="proverbs_21_28"></a>Proverbs 21:28

A [kazab](../../strongs/h/h3577.md) ['ed](../../strongs/h/h5707.md) shall ['abad](../../strongs/h/h6.md): but the ['iysh](../../strongs/h/h376.md) that [shama'](../../strongs/h/h8085.md) [dabar](../../strongs/h/h1696.md) [netsach](../../strongs/h/h5331.md).

<a name="proverbs_21_29"></a>Proverbs 21:29

A [rasha'](../../strongs/h/h7563.md) ['iysh](../../strongs/h/h376.md) ['azaz](../../strongs/h/h5810.md) his [paniym](../../strongs/h/h6440.md): but as for the [yashar](../../strongs/h/h3477.md), he [bîn](../../strongs/h/h995.md) [kuwn](../../strongs/h/h3559.md) his [derek](../../strongs/h/h1870.md).

<a name="proverbs_21_30"></a>Proverbs 21:30

There is no [ḥāḵmâ](../../strongs/h/h2451.md) nor [tāḇûn](../../strongs/h/h8394.md) nor ['etsah](../../strongs/h/h6098.md) against [Yĕhovah](../../strongs/h/h3068.md).

<a name="proverbs_21_31"></a>Proverbs 21:31

The [sûs](../../strongs/h/h5483.md) is [kuwn](../../strongs/h/h3559.md) against the [yowm](../../strongs/h/h3117.md) of [milḥāmâ](../../strongs/h/h4421.md): but [tᵊšûʿâ](../../strongs/h/h8668.md) is of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 20](proverbs_20.md) - [Proverbs 22](proverbs_22.md)