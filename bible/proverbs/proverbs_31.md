# [Proverbs 31](https://www.blueletterbible.org/kjv/proverbs/31)

<a name="proverbs_31_1"></a>Proverbs 31:1

The [dabar](../../strongs/h/h1697.md) of [melek](../../strongs/h/h4428.md) [Lᵊmô'ēl](../../strongs/h/h3927.md), the [maśśā'](../../strongs/h/h4853.md) that his ['em](../../strongs/h/h517.md) [yacar](../../strongs/h/h3256.md) him.

<a name="proverbs_31_2"></a>Proverbs 31:2

What, my [bar](../../strongs/h/h1248.md)? and what, the [bar](../../strongs/h/h1248.md) of my [beten](../../strongs/h/h990.md)? and what, the [bar](../../strongs/h/h1248.md) of my [neḏer](../../strongs/h/h5088.md)?

<a name="proverbs_31_3"></a>Proverbs 31:3

[nathan](../../strongs/h/h5414.md) not thy [ḥayil](../../strongs/h/h2428.md) unto ['ishshah](../../strongs/h/h802.md), nor thy [derek](../../strongs/h/h1870.md) to that which [māḥâ](../../strongs/h/h4229.md) [melek](../../strongs/h/h4428.md).

<a name="proverbs_31_4"></a>Proverbs 31:4

It is not for [melek](../../strongs/h/h4428.md), O [Lᵊmô'ēl](../../strongs/h/h3927.md), it is not for [melek](../../strongs/h/h4428.md) to [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md); nor ['ay](../../strongs/h/h335.md) ['av](../../strongs/h/h176.md) [razan](../../strongs/h/h7336.md) strong [šēḵār](../../strongs/h/h7941.md):

<a name="proverbs_31_5"></a>Proverbs 31:5

Lest they [šāṯâ](../../strongs/h/h8354.md), and [shakach](../../strongs/h/h7911.md) the [ḥāqaq](../../strongs/h/h2710.md), and [šānâ](../../strongs/h/h8138.md) the [diyn](../../strongs/h/h1779.md) of any of the [ben](../../strongs/h/h1121.md) ['oniy](../../strongs/h/h6040.md).

<a name="proverbs_31_6"></a>Proverbs 31:6

[nathan](../../strongs/h/h5414.md) strong [šēḵār](../../strongs/h/h7941.md) unto him that is ready to ['abad](../../strongs/h/h6.md), and [yayin](../../strongs/h/h3196.md) unto those that be of [mar](../../strongs/h/h4751.md) [nephesh](../../strongs/h/h5315.md).

<a name="proverbs_31_7"></a>Proverbs 31:7

Let him [šāṯâ](../../strongs/h/h8354.md), and [shakach](../../strongs/h/h7911.md) his [rêš](../../strongs/h/h7389.md), and [zakar](../../strongs/h/h2142.md) his ['amal](../../strongs/h/h5999.md) no more.

<a name="proverbs_31_8"></a>Proverbs 31:8

[pāṯaḥ](../../strongs/h/h6605.md) thy [peh](../../strongs/h/h6310.md) for the ['illēm](../../strongs/h/h483.md) in the [diyn](../../strongs/h/h1779.md) of all such as are [ben](../../strongs/h/h1121.md) to [ḥălôp̄](../../strongs/h/h2475.md).

<a name="proverbs_31_9"></a>Proverbs 31:9

[pāṯaḥ](../../strongs/h/h6605.md) thy [peh](../../strongs/h/h6310.md), [shaphat](../../strongs/h/h8199.md) [tsedeq](../../strongs/h/h6664.md), and [diyn](../../strongs/h/h1777.md) the cause of the ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md).

<a name="proverbs_31_10"></a>Proverbs 31:10

Who can [māṣā'](../../strongs/h/h4672.md) a [ḥayil](../../strongs/h/h2428.md) ['ishshah](../../strongs/h/h802.md)? for her [meḵer](../../strongs/h/h4377.md) is [rachowq](../../strongs/h/h7350.md) above [p̄nyym](../../strongs/h/h6443.md).

<a name="proverbs_31_11"></a>Proverbs 31:11

The [leb](../../strongs/h/h3820.md) of her [baʿal](../../strongs/h/h1167.md) doth safely [batach](../../strongs/h/h982.md) in her, so that he shall have no [ḥāsēr](../../strongs/h/h2637.md) of [šālāl](../../strongs/h/h7998.md).

<a name="proverbs_31_12"></a>Proverbs 31:12

She will [gamal](../../strongs/h/h1580.md) him [towb](../../strongs/h/h2896.md) and not [ra'](../../strongs/h/h7451.md) all the [yowm](../../strongs/h/h3117.md) of her [chay](../../strongs/h/h2416.md).

<a name="proverbs_31_13"></a>Proverbs 31:13

She [darash](../../strongs/h/h1875.md) [ṣemer](../../strongs/h/h6785.md), and [pēšeṯ](../../strongs/h/h6593.md), and ['asah](../../strongs/h/h6213.md) [chephets](../../strongs/h/h2656.md) with her [kaph](../../strongs/h/h3709.md).

<a name="proverbs_31_14"></a>Proverbs 31:14

She is like the [sāḥar](../../strongs/h/h5503.md) ['ŏnîyâ](../../strongs/h/h591.md); she [bow'](../../strongs/h/h935.md) her [lechem](../../strongs/h/h3899.md) from [merḥāq](../../strongs/h/h4801.md).

<a name="proverbs_31_15"></a>Proverbs 31:15

She [quwm](../../strongs/h/h6965.md) also while it is yet [layil](../../strongs/h/h3915.md), and [nathan](../../strongs/h/h5414.md) [ṭerep̄](../../strongs/h/h2964.md) to her [bayith](../../strongs/h/h1004.md), and a [choq](../../strongs/h/h2706.md) to her [naʿărâ](../../strongs/h/h5291.md).

<a name="proverbs_31_16"></a>Proverbs 31:16

She [zāmam](../../strongs/h/h2161.md) a [sadeh](../../strongs/h/h7704.md), and [laqach](../../strongs/h/h3947.md) it: with the [pĕriy](../../strongs/h/h6529.md) of her [kaph](../../strongs/h/h3709.md) she [nāṭaʿ](../../strongs/h/h5193.md) a [kerem](../../strongs/h/h3754.md).

<a name="proverbs_31_17"></a>Proverbs 31:17

She [ḥāḡar](../../strongs/h/h2296.md) her [māṯnayim](../../strongs/h/h4975.md) with ['oz](../../strongs/h/h5797.md), and ['amats](../../strongs/h/h553.md) her [zerowa'](../../strongs/h/h2220.md).

<a name="proverbs_31_18"></a>Proverbs 31:18

She [ṭāʿam](../../strongs/h/h2938.md) that her [saḥar](../../strongs/h/h5504.md) is [towb](../../strongs/h/h2896.md): her [nîr](../../strongs/h/h5216.md) goeth not [kāḇâ](../../strongs/h/h3518.md) by [layil](../../strongs/h/h3915.md).

<a name="proverbs_31_19"></a>Proverbs 31:19

She [shalach](../../strongs/h/h7971.md) her [yad](../../strongs/h/h3027.md) to the [kîšôr](../../strongs/h/h3601.md), and her [kaph](../../strongs/h/h3709.md) [tamak](../../strongs/h/h8551.md) the [peleḵ](../../strongs/h/h6418.md).

<a name="proverbs_31_20"></a>Proverbs 31:20

She [pāraś](../../strongs/h/h6566.md) her [kaph](../../strongs/h/h3709.md) to the ['aniy](../../strongs/h/h6041.md); yea, she [shalach](../../strongs/h/h7971.md) her [yad](../../strongs/h/h3027.md) to the ['ebyown](../../strongs/h/h34.md).

<a name="proverbs_31_21"></a>Proverbs 31:21

She is not [yare'](../../strongs/h/h3372.md) of the [šeleḡ](../../strongs/h/h7950.md) for her [bayith](../../strongs/h/h1004.md): for all her [bayith](../../strongs/h/h1004.md) are [labash](../../strongs/h/h3847.md) with [šānî](../../strongs/h/h8144.md).

<a name="proverbs_31_22"></a>Proverbs 31:22

She ['asah](../../strongs/h/h6213.md) herself coverings of [marḇaḏ](../../strongs/h/h4765.md); her [lᵊḇûš](../../strongs/h/h3830.md) is [šēš](../../strongs/h/h8336.md) and ['argāmān](../../strongs/h/h713.md).

<a name="proverbs_31_23"></a>Proverbs 31:23

Her [baʿal](../../strongs/h/h1167.md) is [yada'](../../strongs/h/h3045.md) in the [sha'ar](../../strongs/h/h8179.md), when he [yashab](../../strongs/h/h3427.md) among the [zāqēn](../../strongs/h/h2205.md) of the ['erets](../../strongs/h/h776.md).

<a name="proverbs_31_24"></a>Proverbs 31:24

She ['asah](../../strongs/h/h6213.md) [sāḏîn](../../strongs/h/h5466.md), and [māḵar](../../strongs/h/h4376.md) it; and [nathan](../../strongs/h/h5414.md) [ḥăḡôr](../../strongs/h/h2289.md) unto the [Kᵊnaʿănî](../../strongs/h/h3669.md).

<a name="proverbs_31_25"></a>Proverbs 31:25

['oz](../../strongs/h/h5797.md) and [hadar](../../strongs/h/h1926.md) are her [lᵊḇûš](../../strongs/h/h3830.md); and she shall [śāḥaq](../../strongs/h/h7832.md) in [yowm](../../strongs/h/h3117.md) to ['aḥărôn](../../strongs/h/h314.md).

<a name="proverbs_31_26"></a>Proverbs 31:26

She [pāṯaḥ](../../strongs/h/h6605.md) her [peh](../../strongs/h/h6310.md) with [ḥāḵmâ](../../strongs/h/h2451.md); and in her [lashown](../../strongs/h/h3956.md) is the [towrah](../../strongs/h/h8451.md) of [checed](../../strongs/h/h2617.md).

<a name="proverbs_31_27"></a>Proverbs 31:27

She [tsaphah](../../strongs/h/h6822.md) to the [hălîḵâ](../../strongs/h/h1979.md) [hălîḵâ](../../strongs/h/h1979.md) of her [bayith](../../strongs/h/h1004.md), and ['akal](../../strongs/h/h398.md) not the [lechem](../../strongs/h/h3899.md) of [ʿaṣlûṯ](../../strongs/h/h6104.md).

<a name="proverbs_31_28"></a>Proverbs 31:28

Her [ben](../../strongs/h/h1121.md) [quwm](../../strongs/h/h6965.md), and call her ['āšar](../../strongs/h/h833.md); her [baʿal](../../strongs/h/h1167.md) also, and he [halal](../../strongs/h/h1984.md) her.

<a name="proverbs_31_29"></a>Proverbs 31:29

[rab](../../strongs/h/h7227.md) [bath](../../strongs/h/h1323.md) have ['asah](../../strongs/h/h6213.md) [ḥayil](../../strongs/h/h2428.md), but thou [ʿālâ](../../strongs/h/h5927.md) them all.

<a name="proverbs_31_30"></a>Proverbs 31:30

[ḥēn](../../strongs/h/h2580.md) is [sheqer](../../strongs/h/h8267.md), and [yᵊp̄î](../../strongs/h/h3308.md) is [heḇel](../../strongs/h/h1892.md): but an ['ishshah](../../strongs/h/h802.md) that [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md), she shall be [halal](../../strongs/h/h1984.md).

<a name="proverbs_31_31"></a>Proverbs 31:31

[nathan](../../strongs/h/h5414.md) her of the [pĕriy](../../strongs/h/h6529.md) of her [yad](../../strongs/h/h3027.md); and let her own [ma'aseh](../../strongs/h/h4639.md) [halal](../../strongs/h/h1984.md) her in the [sha'ar](../../strongs/h/h8179.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 30](proverbs_30.md)