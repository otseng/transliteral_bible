# [Proverbs 5](https://www.blueletterbible.org/kjv/proverbs/5)

<a name="proverbs_5_1"></a>Proverbs 5:1

My [ben](../../strongs/h/h1121.md), [qashab](../../strongs/h/h7181.md) unto my [ḥāḵmâ](../../strongs/h/h2451.md), and [natah](../../strongs/h/h5186.md) thine ['ozen](../../strongs/h/h241.md) to my [tāḇûn](../../strongs/h/h8394.md):

<a name="proverbs_5_2"></a>Proverbs 5:2

That thou mayest [shamar](../../strongs/h/h8104.md) [mezimmah](../../strongs/h/h4209.md), and that thy [saphah](../../strongs/h/h8193.md) may [nāṣar](../../strongs/h/h5341.md) [da'ath](../../strongs/h/h1847.md).

<a name="proverbs_5_3"></a>Proverbs 5:3

For the [saphah](../../strongs/h/h8193.md) of a [zûr](../../strongs/h/h2114.md) [nāṭap̄](../../strongs/h/h5197.md) as a [nōp̄eṯ](../../strongs/h/h5317.md), and her [ḥēḵ](../../strongs/h/h2441.md) is [ḥālāq](../../strongs/h/h2509.md) than [šemen](../../strongs/h/h8081.md):

<a name="proverbs_5_4"></a>Proverbs 5:4

But her ['aḥărîṯ](../../strongs/h/h319.md) is [mar](../../strongs/h/h4751.md) as [laʿănâ](../../strongs/h/h3939.md), [ḥaḏ](../../strongs/h/h2299.md) as a [peh](../../strongs/h/h6310.md) [chereb](../../strongs/h/h2719.md).

<a name="proverbs_5_5"></a>Proverbs 5:5

Her [regel](../../strongs/h/h7272.md) [yarad](../../strongs/h/h3381.md) to [maveth](../../strongs/h/h4194.md); her [ṣaʿaḏ](../../strongs/h/h6806.md) [tamak](../../strongs/h/h8551.md) on [shĕ'owl](../../strongs/h/h7585.md).

<a name="proverbs_5_6"></a>Proverbs 5:6

Lest thou shouldest [pālas](../../strongs/h/h6424.md) the ['orach](../../strongs/h/h734.md) of [chay](../../strongs/h/h2416.md), her [ma'gal](../../strongs/h/h4570.md) are [nûaʿ](../../strongs/h/h5128.md), that thou canst not [yada'](../../strongs/h/h3045.md) them.

<a name="proverbs_5_7"></a>Proverbs 5:7

[shama'](../../strongs/h/h8085.md) me now therefore, O ye [ben](../../strongs/h/h1121.md), and [cuwr](../../strongs/h/h5493.md) not from the ['emer](../../strongs/h/h561.md) of my [peh](../../strongs/h/h6310.md).

<a name="proverbs_5_8"></a>Proverbs 5:8

[rachaq](../../strongs/h/h7368.md) thy [derek](../../strongs/h/h1870.md) [rachaq](../../strongs/h/h7368.md) from her, and [qāraḇ](../../strongs/h/h7126.md) not the [peṯaḥ](../../strongs/h/h6607.md) of her [bayith](../../strongs/h/h1004.md):

<a name="proverbs_5_9"></a>Proverbs 5:9

Lest thou [nathan](../../strongs/h/h5414.md) thine [howd](../../strongs/h/h1935.md) unto ['aḥēr](../../strongs/h/h312.md), and thy [šānâ](../../strongs/h/h8141.md) unto the ['aḵzārî](../../strongs/h/h394.md):

<a name="proverbs_5_10"></a>Proverbs 5:10

Lest [zûr](../../strongs/h/h2114.md) be [sāׂbaʿ](../../strongs/h/h7646.md) with thy [koach](../../strongs/h/h3581.md); and thy ['etseb](../../strongs/h/h6089.md) be in the [bayith](../../strongs/h/h1004.md) of a [nāḵrî](../../strongs/h/h5237.md);

<a name="proverbs_5_11"></a>Proverbs 5:11

And thou [nāham](../../strongs/h/h5098.md) at the ['aḥărîṯ](../../strongs/h/h319.md), when thy [basar](../../strongs/h/h1320.md) and thy [šᵊ'ēr](../../strongs/h/h7607.md) are [kalah](../../strongs/h/h3615.md),

<a name="proverbs_5_12"></a>Proverbs 5:12

And ['āmar](../../strongs/h/h559.md), How have I [sane'](../../strongs/h/h8130.md) [mûsār](../../strongs/h/h4148.md), and my [leb](../../strongs/h/h3820.md) [na'ats](../../strongs/h/h5006.md) [tôḵēḥâ](../../strongs/h/h8433.md);

<a name="proverbs_5_13"></a>Proverbs 5:13

And have not [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of my [yārâ](../../strongs/h/h3384.md), nor [natah](../../strongs/h/h5186.md) mine ['ozen](../../strongs/h/h241.md) to them that [lamad](../../strongs/h/h3925.md) me!

<a name="proverbs_5_14"></a>Proverbs 5:14

I was [mᵊʿaṭ](../../strongs/h/h4592.md) in all [ra'](../../strongs/h/h7451.md) in the [tavek](../../strongs/h/h8432.md) of the [qāhēl](../../strongs/h/h6951.md) and ['edah](../../strongs/h/h5712.md).

<a name="proverbs_5_15"></a>Proverbs 5:15

[šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md) out of thine own [bowr](../../strongs/h/h953.md), and [nāzal](../../strongs/h/h5140.md) out [tavek](../../strongs/h/h8432.md) thine own [bᵊ'ēr](../../strongs/h/h875.md).

<a name="proverbs_5_16"></a>Proverbs 5:16

Let thy [maʿyān](../../strongs/h/h4599.md) be [puwts](../../strongs/h/h6327.md) [ḥûṣ](../../strongs/h/h2351.md), and [peleḡ](../../strongs/h/h6388.md) of [mayim](../../strongs/h/h4325.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md).

<a name="proverbs_5_17"></a>Proverbs 5:17

Let them be only thine own, and not [zûr](../../strongs/h/h2114.md) with thee.

<a name="proverbs_5_18"></a>Proverbs 5:18

Let thy [māqôr](../../strongs/h/h4726.md) be [barak](../../strongs/h/h1288.md): and [samach](../../strongs/h/h8055.md) with the ['ishshah](../../strongs/h/h802.md) of thy [nāʿur](../../strongs/h/h5271.md).

<a name="proverbs_5_19"></a>Proverbs 5:19

Let her be as the ['ahaḇ](../../strongs/h/h158.md) ['ayyeleṯ](../../strongs/h/h365.md) and [ḥēn](../../strongs/h/h2580.md) [yaʿălâ](../../strongs/h/h3280.md); let her [daḏ](../../strongs/h/h1717.md) [rāvâ](../../strongs/h/h7301.md) thee at all [ʿēṯ](../../strongs/h/h6256.md); and be thou [šāḡâ](../../strongs/h/h7686.md) [tāmîḏ](../../strongs/h/h8548.md) with her ['ahăḇâ](../../strongs/h/h160.md).

<a name="proverbs_5_20"></a>Proverbs 5:20

And why wilt thou, my [ben](../../strongs/h/h1121.md), be [šāḡâ](../../strongs/h/h7686.md) with a [zûr](../../strongs/h/h2114.md), and [ḥāḇaq](../../strongs/h/h2263.md) the [ḥêq](../../strongs/h/h2436.md) of a [nāḵrî](../../strongs/h/h5237.md)?

<a name="proverbs_5_21"></a>Proverbs 5:21

For the [derek](../../strongs/h/h1870.md) of ['iysh](../../strongs/h/h376.md) are [nōḵaḥ](../../strongs/h/h5227.md) the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and he [pālas](../../strongs/h/h6424.md) all his [ma'gal](../../strongs/h/h4570.md).

<a name="proverbs_5_22"></a>Proverbs 5:22

His own ['avon](../../strongs/h/h5771.md) shall [lāḵaḏ](../../strongs/h/h3920.md) the [rasha'](../../strongs/h/h7563.md) himself, and he shall be [tamak](../../strongs/h/h8551.md) with the [chebel](../../strongs/h/h2256.md) of his [chatta'ath](../../strongs/h/h2403.md).

<a name="proverbs_5_23"></a>Proverbs 5:23

He shall [muwth](../../strongs/h/h4191.md) without [mûsār](../../strongs/h/h4148.md); and in the [rōḇ](../../strongs/h/h7230.md) of his ['iûeleṯ](../../strongs/h/h200.md) he shall [šāḡâ](../../strongs/h/h7686.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 4](proverbs_4.md) - [Proverbs 6](proverbs_6.md)