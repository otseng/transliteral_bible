# [Proverbs 7](https://www.blueletterbible.org/kjv/proverbs/7)

<a name="proverbs_7_1"></a>Proverbs 7:1

My [ben](../../strongs/h/h1121.md), [shamar](../../strongs/h/h8104.md) my ['emer](../../strongs/h/h561.md), and [tsaphan](../../strongs/h/h6845.md) my [mitsvah](../../strongs/h/h4687.md) with thee.

<a name="proverbs_7_2"></a>Proverbs 7:2

[shamar](../../strongs/h/h8104.md) my [mitsvah](../../strongs/h/h4687.md), and [ḥāyâ](../../strongs/h/h2421.md); and my [towrah](../../strongs/h/h8451.md) as the ['iyshown](../../strongs/h/h380.md) of thine ['ayin](../../strongs/h/h5869.md).

<a name="proverbs_7_3"></a>Proverbs 7:3

[qāšar](../../strongs/h/h7194.md) them upon thy ['etsba'](../../strongs/h/h676.md), [kāṯaḇ](../../strongs/h/h3789.md) them upon the [lûaḥ](../../strongs/h/h3871.md) of thine [leb](../../strongs/h/h3820.md).

<a name="proverbs_7_4"></a>Proverbs 7:4

['āmar](../../strongs/h/h559.md) unto [ḥāḵmâ](../../strongs/h/h2451.md), Thou art my ['āḥôṯ](../../strongs/h/h269.md); and [qara'](../../strongs/h/h7121.md) [bînâ](../../strongs/h/h998.md) thy [môḏāʿ](../../strongs/h/h4129.md):

<a name="proverbs_7_5"></a>Proverbs 7:5

That they may [shamar](../../strongs/h/h8104.md) thee from the [zûr](../../strongs/h/h2114.md) ['ishshah](../../strongs/h/h802.md), from the [nāḵrî](../../strongs/h/h5237.md) which [chalaq](../../strongs/h/h2505.md) with her ['emer](../../strongs/h/h561.md).

<a name="proverbs_7_6"></a>Proverbs 7:6

For at the [ḥallôn](../../strongs/h/h2474.md) of my [bayith](../../strongs/h/h1004.md) I [šāqap̄](../../strongs/h/h8259.md) through my ['ešnāḇ](../../strongs/h/h822.md),

<a name="proverbs_7_7"></a>Proverbs 7:7

And [ra'ah](../../strongs/h/h7200.md) among the [pᵊṯî](../../strongs/h/h6612.md), I [bîn](../../strongs/h/h995.md) among the [ben](../../strongs/h/h1121.md), a [naʿar](../../strongs/h/h5288.md) [ḥāsēr](../../strongs/h/h2638.md) of [leb](../../strongs/h/h3820.md),

<a name="proverbs_7_8"></a>Proverbs 7:8

['abar](../../strongs/h/h5674.md) the [šûq](../../strongs/h/h7784.md) ['ēṣel](../../strongs/h/h681.md) her [pēn](../../strongs/h/h6434.md); and he [ṣāʿaḏ](../../strongs/h/h6805.md) the [derek](../../strongs/h/h1870.md) to her [bayith](../../strongs/h/h1004.md),

<a name="proverbs_7_9"></a>Proverbs 7:9

In the [nešep̄](../../strongs/h/h5399.md), in the [yowm](../../strongs/h/h3117.md) ['ereb](../../strongs/h/h6153.md), in the ['iyshown](../../strongs/h/h380.md) and ['ăp̄ēlâ](../../strongs/h/h653.md) [layil](../../strongs/h/h3915.md):

<a name="proverbs_7_10"></a>Proverbs 7:10

And, behold, there [qārā'](../../strongs/h/h7125.md) him an ['ishshah](../../strongs/h/h802.md) with the [šîṯ](../../strongs/h/h7897.md) of a [zānâ](../../strongs/h/h2181.md), and [nāṣar](../../strongs/h/h5341.md) of [leb](../../strongs/h/h3820.md).

<a name="proverbs_7_11"></a>Proverbs 7:11

(She is [hāmâ](../../strongs/h/h1993.md) and [sārar](../../strongs/h/h5637.md); her [regel](../../strongs/h/h7272.md) [shakan](../../strongs/h/h7931.md) not in her [bayith](../../strongs/h/h1004.md):

<a name="proverbs_7_12"></a>Proverbs 7:12

[pa'am](../../strongs/h/h6471.md) is she [ḥûṣ](../../strongs/h/h2351.md), [pa'am](../../strongs/h/h6471.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md), and ['arab](../../strongs/h/h693.md) ['ēṣel](../../strongs/h/h681.md) every [pinnâ](../../strongs/h/h6438.md).)

<a name="proverbs_7_13"></a>Proverbs 7:13

So she [ḥāzaq](../../strongs/h/h2388.md) him, and [nashaq](../../strongs/h/h5401.md) him, and with an ['azaz](../../strongs/h/h5810.md) [paniym](../../strongs/h/h6440.md) ['āmar](../../strongs/h/h559.md) unto him,

<a name="proverbs_7_14"></a>Proverbs 7:14

I have [šelem](../../strongs/h/h8002.md) [zebach](../../strongs/h/h2077.md) with me; this [yowm](../../strongs/h/h3117.md) have I [shalam](../../strongs/h/h7999.md) my [neḏer](../../strongs/h/h5088.md).

<a name="proverbs_7_15"></a>Proverbs 7:15

Therefore [yāṣā'](../../strongs/h/h3318.md) I to [qārā'](../../strongs/h/h7125.md) thee, diligently to [šāḥar](../../strongs/h/h7836.md) thy [paniym](../../strongs/h/h6440.md), and I have [māṣā'](../../strongs/h/h4672.md) thee.

<a name="proverbs_7_16"></a>Proverbs 7:16

I have [rāḇaḏ](../../strongs/h/h7234.md) my ['eres](../../strongs/h/h6210.md) with [marḇaḏ](../../strongs/h/h4765.md), with [ḥăṭuḇâ](../../strongs/h/h2405.md), with ['ēṭûn](../../strongs/h/h330.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="proverbs_7_17"></a>Proverbs 7:17

I have [nûp̄](../../strongs/h/h5130.md) my [miškāḇ](../../strongs/h/h4904.md) with [mōr](../../strongs/h/h4753.md), ['ăhālîm](../../strongs/h/h174.md), and [qinnāmôn](../../strongs/h/h7076.md).

<a name="proverbs_7_18"></a>Proverbs 7:18

[yālaḵ](../../strongs/h/h3212.md), let us take our [rāvâ](../../strongs/h/h7301.md) of [dôḏ](../../strongs/h/h1730.md) until the [boqer](../../strongs/h/h1242.md): let us [ʿālas](../../strongs/h/h5965.md) ourselves with ['ōhaḇ](../../strongs/h/h159.md).

<a name="proverbs_7_19"></a>Proverbs 7:19

For the ['iysh](../../strongs/h/h376.md) is not at [bayith](../../strongs/h/h1004.md), he is [halak](../../strongs/h/h1980.md) a [rachowq](../../strongs/h/h7350.md) [derek](../../strongs/h/h1870.md):

<a name="proverbs_7_20"></a>Proverbs 7:20

He hath [laqach](../../strongs/h/h3947.md) a [ṣᵊrôr](../../strongs/h/h6872.md) of [keceph](../../strongs/h/h3701.md) with [yad](../../strongs/h/h3027.md), and will [bow'](../../strongs/h/h935.md) [bayith](../../strongs/h/h1004.md) at the [yowm](../../strongs/h/h3117.md) [kese'](../../strongs/h/h3677.md).

<a name="proverbs_7_21"></a>Proverbs 7:21

With her [rōḇ](../../strongs/h/h7230.md) [leqaḥ](../../strongs/h/h3948.md) she caused him to [natah](../../strongs/h/h5186.md), with the [cheleq](../../strongs/h/h2506.md) of her [saphah](../../strongs/h/h8193.md) she [nāḏaḥ](../../strongs/h/h5080.md) him.

<a name="proverbs_7_22"></a>Proverbs 7:22

He [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) her [piṯ'ōm](../../strongs/h/h6597.md), as a [showr](../../strongs/h/h7794.md) [bow'](../../strongs/h/h935.md) to the [ṭeḇaḥ](../../strongs/h/h2874.md), or as an ['ĕvîl](../../strongs/h/h191.md) to the [mûsār](../../strongs/h/h4148.md) of the [ʿeḵes](../../strongs/h/h5914.md);

<a name="proverbs_7_23"></a>Proverbs 7:23

Till a [chets](../../strongs/h/h2671.md) [pālaḥ](../../strongs/h/h6398.md) through his [kāḇēḏ](../../strongs/h/h3516.md); as a [tsippowr](../../strongs/h/h6833.md) [māhar](../../strongs/h/h4116.md) to the [paḥ](../../strongs/h/h6341.md), and [yada'](../../strongs/h/h3045.md) not that it is for his [nephesh](../../strongs/h/h5315.md).

<a name="proverbs_7_24"></a>Proverbs 7:24

[shama'](../../strongs/h/h8085.md) unto me now therefore, O ye [ben](../../strongs/h/h1121.md), and [qashab](../../strongs/h/h7181.md) to the ['emer](../../strongs/h/h561.md) of my [peh](../../strongs/h/h6310.md).

<a name="proverbs_7_25"></a>Proverbs 7:25

Let not thine [leb](../../strongs/h/h3820.md) [śāṭâ](../../strongs/h/h7847.md) to her [derek](../../strongs/h/h1870.md), go not [tāʿâ](../../strongs/h/h8582.md) in her [nāṯîḇ](../../strongs/h/h5410.md).

<a name="proverbs_7_26"></a>Proverbs 7:26

For she hath cast [naphal](../../strongs/h/h5307.md) [rab](../../strongs/h/h7227.md) [ḥālāl](../../strongs/h/h2491.md): yea, many ['atsuwm](../../strongs/h/h6099.md) men have been [harag](../../strongs/h/h2026.md) by her.

<a name="proverbs_7_27"></a>Proverbs 7:27

Her [bayith](../../strongs/h/h1004.md) is the [derek](../../strongs/h/h1870.md) to [shĕ'owl](../../strongs/h/h7585.md), going [yarad](../../strongs/h/h3381.md) to the [ḥeḏer](../../strongs/h/h2315.md) of [maveth](../../strongs/h/h4194.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 6](proverbs_6.md) - [Proverbs 8](proverbs_8.md)