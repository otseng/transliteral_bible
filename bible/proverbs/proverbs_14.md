# [Proverbs 14](https://www.blueletterbible.org/kjv/proverbs/14)

<a name="proverbs_14_1"></a>Proverbs 14:1

Every [ḥāḵmôṯ](../../strongs/h/h2454.md) ['ishshah](../../strongs/h/h802.md) [bānâ](../../strongs/h/h1129.md) her [bayith](../../strongs/h/h1004.md): but the ['iûeleṯ](../../strongs/h/h200.md) [harac](../../strongs/h/h2040.md) it with her [yad](../../strongs/h/h3027.md).

<a name="proverbs_14_2"></a>Proverbs 14:2

He that [halak](../../strongs/h/h1980.md) in his [yōšer](../../strongs/h/h3476.md) [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md): but he that is [Lûz](../../strongs/h/h3868.md) in his [derek](../../strongs/h/h1870.md) [bazah](../../strongs/h/h959.md) him.

<a name="proverbs_14_3"></a>Proverbs 14:3

In the [peh](../../strongs/h/h6310.md) of the ['ĕvîl](../../strongs/h/h191.md) is a [ḥōṭer](../../strongs/h/h2415.md) of [ga'avah](../../strongs/h/h1346.md): but the [saphah](../../strongs/h/h8193.md) of the [ḥāḵām](../../strongs/h/h2450.md) shall [shamar](../../strongs/h/h8104.md) them.

<a name="proverbs_14_4"></a>Proverbs 14:4

Where no ['eleph](../../strongs/h/h504.md) are, the ['ēḇûs](../../strongs/h/h18.md) is [bar](../../strongs/h/h1249.md): but [rōḇ](../../strongs/h/h7230.md) [tᵊḇû'â](../../strongs/h/h8393.md) is by the [koach](../../strongs/h/h3581.md) of the [showr](../../strongs/h/h7794.md).

<a name="proverbs_14_5"></a>Proverbs 14:5

An ['ēmûn](../../strongs/h/h529.md) ['ed](../../strongs/h/h5707.md) will not [kazab](../../strongs/h/h3577.md): but a [sheqer](../../strongs/h/h8267.md) ['ed](../../strongs/h/h5707.md) will [puwach](../../strongs/h/h6315.md) [kāzaḇ](../../strongs/h/h3576.md).

<a name="proverbs_14_6"></a>Proverbs 14:6

A [luwts](../../strongs/h/h3887.md) [bāqaš](../../strongs/h/h1245.md) [ḥāḵmâ](../../strongs/h/h2451.md), and findeth it not: but [da'ath](../../strongs/h/h1847.md) is [qālal](../../strongs/h/h7043.md) unto him that [bîn](../../strongs/h/h995.md).

<a name="proverbs_14_7"></a>Proverbs 14:7

[yālaḵ](../../strongs/h/h3212.md) from the [neḡeḏ](../../strongs/h/h5048.md) of a [kᵊsîl](../../strongs/h/h3684.md) ['iysh](../../strongs/h/h376.md), when thou [yada'](../../strongs/h/h3045.md) not in him the [saphah](../../strongs/h/h8193.md) of [da'ath](../../strongs/h/h1847.md).

<a name="proverbs_14_8"></a>Proverbs 14:8

The [ḥāḵmâ](../../strongs/h/h2451.md) of the ['aruwm](../../strongs/h/h6175.md) is to [bîn](../../strongs/h/h995.md) his [derek](../../strongs/h/h1870.md): but the ['iûeleṯ](../../strongs/h/h200.md) of [kᵊsîl](../../strongs/h/h3684.md) is [mirmah](../../strongs/h/h4820.md).

<a name="proverbs_14_9"></a>Proverbs 14:9

['ĕvîl](../../strongs/h/h191.md) make a [luwts](../../strongs/h/h3887.md) at ['āšām](../../strongs/h/h817.md): but among the [yashar](../../strongs/h/h3477.md) there is [ratsown](../../strongs/h/h7522.md).

<a name="proverbs_14_10"></a>Proverbs 14:10

The [leb](../../strongs/h/h3820.md) [yada'](../../strongs/h/h3045.md) his [nephesh](../../strongs/h/h5315.md) [mārâ](../../strongs/h/h4787.md); and a [zûr](../../strongs/h/h2114.md) doth not [ʿāraḇ](../../strongs/h/h6148.md) with his [simchah](../../strongs/h/h8057.md).

<a name="proverbs_14_11"></a>Proverbs 14:11

The [bayith](../../strongs/h/h1004.md) of the [rasha'](../../strongs/h/h7563.md) shall be [šāmaḏ](../../strongs/h/h8045.md): but the ['ohel](../../strongs/h/h168.md) of the [yashar](../../strongs/h/h3477.md) shall [pāraḥ](../../strongs/h/h6524.md).

<a name="proverbs_14_12"></a>Proverbs 14:12

There [yēš](../../strongs/h/h3426.md) a [derek](../../strongs/h/h1870.md) which seemeth [yashar](../../strongs/h/h3477.md) [paniym](../../strongs/h/h6440.md) an ['iysh](../../strongs/h/h376.md), but the ['aḥărîṯ](../../strongs/h/h319.md) thereof are the [derek](../../strongs/h/h1870.md) of [maveth](../../strongs/h/h4194.md).

<a name="proverbs_14_13"></a>Proverbs 14:13

Even in [śᵊḥôq](../../strongs/h/h7814.md) the [leb](../../strongs/h/h3820.md) is [kā'aḇ](../../strongs/h/h3510.md); and the ['aḥărîṯ](../../strongs/h/h319.md) of that [simchah](../../strongs/h/h8057.md) is [tûḡâ](../../strongs/h/h8424.md).

<a name="proverbs_14_14"></a>Proverbs 14:14

The [sûḡ](../../strongs/h/h5472.md) in [leb](../../strongs/h/h3820.md) shall be [sāׂbaʿ](../../strongs/h/h7646.md) with his own [derek](../../strongs/h/h1870.md): and a [towb](../../strongs/h/h2896.md) ['iysh](../../strongs/h/h376.md) shall be satisfied from himself.

<a name="proverbs_14_15"></a>Proverbs 14:15

The [pᵊṯî](../../strongs/h/h6612.md) ['aman](../../strongs/h/h539.md) every [dabar](../../strongs/h/h1697.md): but the ['aruwm](../../strongs/h/h6175.md) man [bîn](../../strongs/h/h995.md) to his ['ashuwr](../../strongs/h/h838.md).

<a name="proverbs_14_16"></a>Proverbs 14:16

A [ḥāḵām](../../strongs/h/h2450.md) man [yārē'](../../strongs/h/h3373.md), and [cuwr](../../strongs/h/h5493.md) from [ra'](../../strongs/h/h7451.md): but the [kᵊsîl](../../strongs/h/h3684.md) ['abar](../../strongs/h/h5674.md), and is [batach](../../strongs/h/h982.md).

<a name="proverbs_14_17"></a>Proverbs 14:17

He that is [qāṣēr](../../strongs/h/h7116.md) ['aph](../../strongs/h/h639.md) ['asah](../../strongs/h/h6213.md) ['iûeleṯ](../../strongs/h/h200.md): and an ['iysh](../../strongs/h/h376.md) of wicked [mezimmah](../../strongs/h/h4209.md) is [sane'](../../strongs/h/h8130.md).

<a name="proverbs_14_18"></a>Proverbs 14:18

The [pᵊṯî](../../strongs/h/h6612.md) [nāḥal](../../strongs/h/h5157.md) ['iûeleṯ](../../strongs/h/h200.md): but the ['aruwm](../../strongs/h/h6175.md) are [kāṯar](../../strongs/h/h3803.md) with [da'ath](../../strongs/h/h1847.md).

<a name="proverbs_14_19"></a>Proverbs 14:19

The [ra'](../../strongs/h/h7451.md) [shachach](../../strongs/h/h7817.md) [paniym](../../strongs/h/h6440.md) the [towb](../../strongs/h/h2896.md); and the [rasha'](../../strongs/h/h7563.md) at the [sha'ar](../../strongs/h/h8179.md) of the [tsaddiyq](../../strongs/h/h6662.md).

<a name="proverbs_14_20"></a>Proverbs 14:20

The [rûš](../../strongs/h/h7326.md) is [sane'](../../strongs/h/h8130.md) even of his own [rea'](../../strongs/h/h7453.md): but the [ʿāšîr](../../strongs/h/h6223.md) hath [rab](../../strongs/h/h7227.md) ['ahab](../../strongs/h/h157.md).

<a name="proverbs_14_21"></a>Proverbs 14:21

He that [bûz](../../strongs/h/h936.md) his [rea'](../../strongs/h/h7453.md) [chata'](../../strongs/h/h2398.md): but he that hath [chanan](../../strongs/h/h2603.md) on the ['anav](../../strongs/h/h6035.md) ['aniy](../../strongs/h/h6041.md), ['esher](../../strongs/h/h835.md) is he.

<a name="proverbs_14_22"></a>Proverbs 14:22

Do they not [tāʿâ](../../strongs/h/h8582.md) that [ḥāraš](../../strongs/h/h2790.md) [ra'](../../strongs/h/h7451.md)? but [checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md) shall be to them that [ḥāraš](../../strongs/h/h2790.md) [towb](../../strongs/h/h2896.md).

<a name="proverbs_14_23"></a>Proverbs 14:23

In all ['etseb](../../strongs/h/h6089.md) there is [môṯār](../../strongs/h/h4195.md): but the [dabar](../../strongs/h/h1697.md) of the [saphah](../../strongs/h/h8193.md) tendeth only to [maḥsôr](../../strongs/h/h4270.md).

<a name="proverbs_14_24"></a>Proverbs 14:24

The [ʿăṭārâ](../../strongs/h/h5850.md) of the [ḥāḵām](../../strongs/h/h2450.md) is their [ʿōšer](../../strongs/h/h6239.md): but the ['iûeleṯ](../../strongs/h/h200.md) of [kᵊsîl](../../strongs/h/h3684.md) is ['iûeleṯ](../../strongs/h/h200.md).

<a name="proverbs_14_25"></a>Proverbs 14:25

An ['emeth](../../strongs/h/h571.md) ['ed](../../strongs/h/h5707.md) [natsal](../../strongs/h/h5337.md) [nephesh](../../strongs/h/h5315.md): but a [mirmah](../../strongs/h/h4820.md) witness [puwach](../../strongs/h/h6315.md) [kazab](../../strongs/h/h3577.md).

<a name="proverbs_14_26"></a>Proverbs 14:26

In the [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) is ['oz](../../strongs/h/h5797.md) [miḇṭāḥ](../../strongs/h/h4009.md): and his [ben](../../strongs/h/h1121.md) shall have a place of [machaceh](../../strongs/h/h4268.md).

<a name="proverbs_14_27"></a>Proverbs 14:27

The [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) is a [māqôr](../../strongs/h/h4726.md) of [chay](../../strongs/h/h2416.md), to [cuwr](../../strongs/h/h5493.md) from the [mowqesh](../../strongs/h/h4170.md) of [maveth](../../strongs/h/h4194.md).

<a name="proverbs_14_28"></a>Proverbs 14:28

In the [rōḇ](../../strongs/h/h7230.md) of ['am](../../strongs/h/h5971.md) is the [melek](../../strongs/h/h4428.md) [hăḏārâ](../../strongs/h/h1927.md): but in the ['ep̄es](../../strongs/h/h657.md) of [lĕom](../../strongs/h/h3816.md) is the [mᵊḥitâ](../../strongs/h/h4288.md) of the [rāzôn](../../strongs/h/h7333.md).

<a name="proverbs_14_29"></a>Proverbs 14:29

He that is ['ārēḵ](../../strongs/h/h750.md) to ['aph](../../strongs/h/h639.md) is of [rab](../../strongs/h/h7227.md) [tāḇûn](../../strongs/h/h8394.md): but he that is [qāṣēr](../../strongs/h/h7116.md) of [ruwach](../../strongs/h/h7307.md) [ruwm](../../strongs/h/h7311.md) ['iûeleṯ](../../strongs/h/h200.md).

<a name="proverbs_14_30"></a>Proverbs 14:30

A [marpē'](../../strongs/h/h4832.md) [leb](../../strongs/h/h3820.md) is the [chay](../../strongs/h/h2416.md) of the [basar](../../strongs/h/h1320.md): but [qin'â](../../strongs/h/h7068.md) the [rāqāḇ](../../strongs/h/h7538.md) of the ['etsem](../../strongs/h/h6106.md).

<a name="proverbs_14_31"></a>Proverbs 14:31

He that [ʿāšaq](../../strongs/h/h6231.md) the [dal](../../strongs/h/h1800.md) [ḥārap̄](../../strongs/h/h2778.md) his ['asah](../../strongs/h/h6213.md): but he that [kabad](../../strongs/h/h3513.md) him hath [chanan](../../strongs/h/h2603.md) on the ['ebyown](../../strongs/h/h34.md).

<a name="proverbs_14_32"></a>Proverbs 14:32

The [rasha'](../../strongs/h/h7563.md) is [dāḥâ](../../strongs/h/h1760.md) in his [ra'](../../strongs/h/h7451.md): but the [tsaddiyq](../../strongs/h/h6662.md) hath [chacah](../../strongs/h/h2620.md) in his [maveth](../../strongs/h/h4194.md).

<a name="proverbs_14_33"></a>Proverbs 14:33

[ḥāḵmâ](../../strongs/h/h2451.md) [nuwach](../../strongs/h/h5117.md) in the [leb](../../strongs/h/h3820.md) of him that hath [bîn](../../strongs/h/h995.md): but that which is in the [qereḇ](../../strongs/h/h7130.md) of [kᵊsîl](../../strongs/h/h3684.md) is made [yada'](../../strongs/h/h3045.md).

<a name="proverbs_14_34"></a>Proverbs 14:34

[tsedaqah](../../strongs/h/h6666.md) [ruwm](../../strongs/h/h7311.md) a [gowy](../../strongs/h/h1471.md): but [chatta'ath](../../strongs/h/h2403.md) is a [checed](../../strongs/h/h2617.md) to any [lĕom](../../strongs/h/h3816.md).

<a name="proverbs_14_35"></a>Proverbs 14:35

The [melek](../../strongs/h/h4428.md) [ratsown](../../strongs/h/h7522.md) is toward a [sakal](../../strongs/h/h7919.md) ['ebed](../../strongs/h/h5650.md): but his ['ebrah](../../strongs/h/h5678.md) is against him that causeth [buwsh](../../strongs/h/h954.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 13](proverbs_13.md) - [Proverbs 15](proverbs_15.md)