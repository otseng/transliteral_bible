# [Proverbs 15](https://www.blueletterbible.org/kjv/proverbs/15)

<a name="proverbs_15_1"></a>Proverbs 15:1

A [raḵ](../../strongs/h/h7390.md) [maʿănê](../../strongs/h/h4617.md) [shuwb](../../strongs/h/h7725.md) away [chemah](../../strongs/h/h2534.md): but ['etseb](../../strongs/h/h6089.md) [dabar](../../strongs/h/h1697.md) stir [ʿālâ](../../strongs/h/h5927.md) ['aph](../../strongs/h/h639.md).

<a name="proverbs_15_2"></a>Proverbs 15:2

The [lashown](../../strongs/h/h3956.md) of the [ḥāḵām](../../strongs/h/h2450.md) useth [da'ath](../../strongs/h/h1847.md) [yatab](../../strongs/h/h3190.md): but the [peh](../../strongs/h/h6310.md) of [kᵊsîl](../../strongs/h/h3684.md) [nāḇaʿ](../../strongs/h/h5042.md) ['iûeleṯ](../../strongs/h/h200.md).

<a name="proverbs_15_3"></a>Proverbs 15:3

The ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) are in every [maqowm](../../strongs/h/h4725.md), [tsaphah](../../strongs/h/h6822.md) the [ra'](../../strongs/h/h7451.md) and the [towb](../../strongs/h/h2896.md).

<a name="proverbs_15_4"></a>Proverbs 15:4

A [marpē'](../../strongs/h/h4832.md) [lashown](../../strongs/h/h3956.md) is an ['ets](../../strongs/h/h6086.md) of [chay](../../strongs/h/h2416.md): but [selep̄](../../strongs/h/h5558.md) therein is a [šeḇar](../../strongs/h/h7667.md) in the [ruwach](../../strongs/h/h7307.md).

<a name="proverbs_15_5"></a>Proverbs 15:5

An ['ĕvîl](../../strongs/h/h191.md) [na'ats](../../strongs/h/h5006.md) his ['ab](../../strongs/h/h1.md) [mûsār](../../strongs/h/h4148.md): but he that [shamar](../../strongs/h/h8104.md) [tôḵēḥâ](../../strongs/h/h8433.md) is [ʿāram](../../strongs/h/h6191.md).

<a name="proverbs_15_6"></a>Proverbs 15:6

In the [bayith](../../strongs/h/h1004.md) of the [tsaddiyq](../../strongs/h/h6662.md) is [rab](../../strongs/h/h7227.md) [ḥōsen](../../strongs/h/h2633.md): but in the [tᵊḇû'â](../../strongs/h/h8393.md) of the [rasha'](../../strongs/h/h7563.md) is [ʿāḵar](../../strongs/h/h5916.md).

<a name="proverbs_15_7"></a>Proverbs 15:7

The [saphah](../../strongs/h/h8193.md) of the [ḥāḵām](../../strongs/h/h2450.md) [zārâ](../../strongs/h/h2219.md) [da'ath](../../strongs/h/h1847.md): but the [leb](../../strongs/h/h3820.md) of the [kᵊsîl](../../strongs/h/h3684.md) doeth not so.

<a name="proverbs_15_8"></a>Proverbs 15:8

The [zebach](../../strongs/h/h2077.md) of the [rasha'](../../strongs/h/h7563.md) is a [tôʿēḇâ](../../strongs/h/h8441.md) to [Yĕhovah](../../strongs/h/h3068.md): but the [tĕphillah](../../strongs/h/h8605.md) of the [yashar](../../strongs/h/h3477.md) is his [ratsown](../../strongs/h/h7522.md).

<a name="proverbs_15_9"></a>Proverbs 15:9

The [derek](../../strongs/h/h1870.md) of the [rasha'](../../strongs/h/h7563.md) is a [tôʿēḇâ](../../strongs/h/h8441.md) unto [Yĕhovah](../../strongs/h/h3068.md): but he ['ahab](../../strongs/h/h157.md) him that [radaph](../../strongs/h/h7291.md) after [tsedaqah](../../strongs/h/h6666.md).

<a name="proverbs_15_10"></a>Proverbs 15:10

[mûsār](../../strongs/h/h4148.md) is [ra'](../../strongs/h/h7451.md) unto him that ['azab](../../strongs/h/h5800.md) the ['orach](../../strongs/h/h734.md): and he that [sane'](../../strongs/h/h8130.md) [tôḵēḥâ](../../strongs/h/h8433.md) shall [muwth](../../strongs/h/h4191.md).

<a name="proverbs_15_11"></a>Proverbs 15:11

[shĕ'owl](../../strongs/h/h7585.md) and ['Ăḇadôn](../../strongs/h/h11.md) are before [Yĕhovah](../../strongs/h/h3068.md): how much more then the [libbah](../../strongs/h/h3826.md) of the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md)?

<a name="proverbs_15_12"></a>Proverbs 15:12

A [luwts](../../strongs/h/h3887.md) ['ahab](../../strongs/h/h157.md) not one that [yakach](../../strongs/h/h3198.md) him: neither will he [yālaḵ](../../strongs/h/h3212.md) unto the [ḥāḵām](../../strongs/h/h2450.md).

<a name="proverbs_15_13"></a>Proverbs 15:13

A [śāmēaḥ](../../strongs/h/h8056.md) [leb](../../strongs/h/h3820.md) maketh a [yatab](../../strongs/h/h3190.md) [paniym](../../strongs/h/h6440.md): but by ['atstsebeth](../../strongs/h/h6094.md) of the [leb](../../strongs/h/h3820.md) the [ruwach](../../strongs/h/h7307.md) is [nāḵā'](../../strongs/h/h5218.md).

<a name="proverbs_15_14"></a>Proverbs 15:14

The [leb](../../strongs/h/h3820.md) of him that hath [bîn](../../strongs/h/h995.md) [bāqaš](../../strongs/h/h1245.md) [da'ath](../../strongs/h/h1847.md): but the [peh](../../strongs/h/h6310.md) [paniym](../../strongs/h/h6440.md) of [kᵊsîl](../../strongs/h/h3684.md) [ra'ah](../../strongs/h/h7462.md) on ['iûeleṯ](../../strongs/h/h200.md).

<a name="proverbs_15_15"></a>Proverbs 15:15

All the [yowm](../../strongs/h/h3117.md) of the ['aniy](../../strongs/h/h6041.md) are [ra'](../../strongs/h/h7451.md): but he that is of a [towb](../../strongs/h/h2896.md) [leb](../../strongs/h/h3820.md) hath a [tāmîḏ](../../strongs/h/h8548.md) [mištê](../../strongs/h/h4960.md).

<a name="proverbs_15_16"></a>Proverbs 15:16

[towb](../../strongs/h/h2896.md) is [mᵊʿaṭ](../../strongs/h/h4592.md) with the [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) than [rab](../../strongs/h/h7227.md) ['ôṣār](../../strongs/h/h214.md) and [mᵊhûmâ](../../strongs/h/h4103.md) therewith.

<a name="proverbs_15_17"></a>Proverbs 15:17

[towb](../../strongs/h/h2896.md) is an ['ăruḥâ](../../strongs/h/h737.md) of [yārāq](../../strongs/h/h3419.md) where ['ahăḇâ](../../strongs/h/h160.md) is, than an ['āḇas](../../strongs/h/h75.md) [showr](../../strongs/h/h7794.md) and [śin'â](../../strongs/h/h8135.md) therewith.

<a name="proverbs_15_18"></a>Proverbs 15:18

A [chemah](../../strongs/h/h2534.md) ['iysh](../../strongs/h/h376.md) stirreth [gārâ](../../strongs/h/h1624.md) [māḏôn](../../strongs/h/h4066.md): but he that is ['ārēḵ](../../strongs/h/h750.md) to ['aph](../../strongs/h/h639.md) [šāqaṭ](../../strongs/h/h8252.md) [rîḇ](../../strongs/h/h7379.md).

<a name="proverbs_15_19"></a>Proverbs 15:19

The [derek](../../strongs/h/h1870.md) of the [ʿāṣēl](../../strongs/h/h6102.md) man is as a [mᵊśûḵâ](../../strongs/h/h4881.md) of [ḥēḏeq](../../strongs/h/h2312.md): but the ['orach](../../strongs/h/h734.md) of the [yashar](../../strongs/h/h3477.md) is made [sālal](../../strongs/h/h5549.md).

<a name="proverbs_15_20"></a>Proverbs 15:20

A [ḥāḵām](../../strongs/h/h2450.md) [ben](../../strongs/h/h1121.md) maketh a [samach](../../strongs/h/h8055.md) ['ab](../../strongs/h/h1.md): but a [kᵊsîl](../../strongs/h/h3684.md) ['āḏām](../../strongs/h/h120.md) [bazah](../../strongs/h/h959.md) his ['em](../../strongs/h/h517.md).

<a name="proverbs_15_21"></a>Proverbs 15:21

['iûeleṯ](../../strongs/h/h200.md) is [simchah](../../strongs/h/h8057.md) to him that is [ḥāsēr](../../strongs/h/h2638.md) of [leb](../../strongs/h/h3820.md): but an ['iysh](../../strongs/h/h376.md) of [tāḇûn](../../strongs/h/h8394.md) [yālaḵ](../../strongs/h/h3212.md) [yashar](../../strongs/h/h3474.md).

<a name="proverbs_15_22"></a>Proverbs 15:22

Without [sôḏ](../../strongs/h/h5475.md) [maḥăšāḇâ](../../strongs/h/h4284.md) are [pārar](../../strongs/h/h6565.md): but in the [rōḇ](../../strongs/h/h7230.md) of [ya'ats](../../strongs/h/h3289.md) they are [quwm](../../strongs/h/h6965.md).

<a name="proverbs_15_23"></a>Proverbs 15:23

An ['iysh](../../strongs/h/h376.md) hath [simchah](../../strongs/h/h8057.md) by the [maʿănê](../../strongs/h/h4617.md) of his [peh](../../strongs/h/h6310.md): and a [dabar](../../strongs/h/h1697.md) spoken in due [ʿēṯ](../../strongs/h/h6256.md), how [towb](../../strongs/h/h2896.md) is it!

<a name="proverbs_15_24"></a>Proverbs 15:24

The ['orach](../../strongs/h/h734.md) of [chay](../../strongs/h/h2416.md) is [maʿal](../../strongs/h/h4605.md) to the [sakal](../../strongs/h/h7919.md), that he may [cuwr](../../strongs/h/h5493.md) from [shĕ'owl](../../strongs/h/h7585.md) [maṭṭâ](../../strongs/h/h4295.md).

<a name="proverbs_15_25"></a>Proverbs 15:25

[Yĕhovah](../../strongs/h/h3068.md) will [nāsaḥ](../../strongs/h/h5255.md) the [bayith](../../strongs/h/h1004.md) of the [gē'ê](../../strongs/h/h1343.md): but he will [nāṣaḇ](../../strongs/h/h5324.md) the [gᵊḇûl](../../strongs/h/h1366.md) of the ['almānâ](../../strongs/h/h490.md).

<a name="proverbs_15_26"></a>Proverbs 15:26

The [maḥăšāḇâ](../../strongs/h/h4284.md) of the [ra'](../../strongs/h/h7451.md) are a [tôʿēḇâ](../../strongs/h/h8441.md) to [Yĕhovah](../../strongs/h/h3068.md): but the words of the [tahowr](../../strongs/h/h2889.md) are [nōʿam](../../strongs/h/h5278.md) ['emer](../../strongs/h/h561.md).

<a name="proverbs_15_27"></a>Proverbs 15:27

He that is [batsa'](../../strongs/h/h1214.md) of [beṣaʿ](../../strongs/h/h1215.md) [ʿāḵar](../../strongs/h/h5916.md) his own [bayith](../../strongs/h/h1004.md); but he that [sane'](../../strongs/h/h8130.md) [matānâ](../../strongs/h/h4979.md) shall [ḥāyâ](../../strongs/h/h2421.md).

<a name="proverbs_15_28"></a>Proverbs 15:28

The [leb](../../strongs/h/h3820.md) of the [tsaddiyq](../../strongs/h/h6662.md) [hagah](../../strongs/h/h1897.md) to ['anah](../../strongs/h/h6030.md): but the [peh](../../strongs/h/h6310.md) of the [rasha'](../../strongs/h/h7563.md) [nāḇaʿ](../../strongs/h/h5042.md) out [ra'](../../strongs/h/h7451.md).

<a name="proverbs_15_29"></a>Proverbs 15:29

[Yĕhovah](../../strongs/h/h3068.md) is [rachowq](../../strongs/h/h7350.md) from the [rasha'](../../strongs/h/h7563.md): but he [shama'](../../strongs/h/h8085.md) the [tĕphillah](../../strongs/h/h8605.md) of the [tsaddiyq](../../strongs/h/h6662.md).

<a name="proverbs_15_30"></a>Proverbs 15:30

The [ma'owr](../../strongs/h/h3974.md) of the ['ayin](../../strongs/h/h5869.md) [samach](../../strongs/h/h8055.md) the [leb](../../strongs/h/h3820.md): and a [towb](../../strongs/h/h2896.md) [šᵊmûʿâ](../../strongs/h/h8052.md) [dāšēn](../../strongs/h/h1878.md) the ['etsem](../../strongs/h/h6106.md) [dāšēn](../../strongs/h/h1878.md).

<a name="proverbs_15_31"></a>Proverbs 15:31

The ['ozen](../../strongs/h/h241.md) that [shama'](../../strongs/h/h8085.md) the [tôḵēḥâ](../../strongs/h/h8433.md) of [chay](../../strongs/h/h2416.md) [lûn](../../strongs/h/h3885.md) [qereḇ](../../strongs/h/h7130.md) the [ḥāḵām](../../strongs/h/h2450.md).

<a name="proverbs_15_32"></a>Proverbs 15:32

He that [pāraʿ](../../strongs/h/h6544.md) [mûsār](../../strongs/h/h4148.md) [mā'as](../../strongs/h/h3988.md) his own [nephesh](../../strongs/h/h5315.md): but he that [shama'](../../strongs/h/h8085.md) [tôḵēḥâ](../../strongs/h/h8433.md) [qānâ](../../strongs/h/h7069.md) [leb](../../strongs/h/h3820.md).

<a name="proverbs_15_33"></a>Proverbs 15:33

The [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) is the [mûsār](../../strongs/h/h4148.md) of [ḥāḵmâ](../../strongs/h/h2451.md); and [paniym](../../strongs/h/h6440.md) [kabowd](../../strongs/h/h3519.md) is [ʿănāvâ](../../strongs/h/h6038.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 14](proverbs_14.md) - [Proverbs 16](proverbs_16.md)