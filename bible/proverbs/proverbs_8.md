# [Proverbs 8](https://www.blueletterbible.org/kjv/proverbs/8)

<a name="proverbs_8_1"></a>Proverbs 8:1

Doth not [ḥāḵmâ](../../strongs/h/h2451.md) [qara'](../../strongs/h/h7121.md)? and [tāḇûn](../../strongs/h/h8394.md) put [nathan](../../strongs/h/h5414.md) her [qowl](../../strongs/h/h6963.md)?

<a name="proverbs_8_2"></a>Proverbs 8:2

She [nāṣaḇ](../../strongs/h/h5324.md) in the [ro'sh](../../strongs/h/h7218.md) of high [marowm](../../strongs/h/h4791.md), by the [derek](../../strongs/h/h1870.md) in the [bayith](../../strongs/h/h1004.md) of the [nāṯîḇ](../../strongs/h/h5410.md).

<a name="proverbs_8_3"></a>Proverbs 8:3

She [ranan](../../strongs/h/h7442.md) [yad](../../strongs/h/h3027.md) the [sha'ar](../../strongs/h/h8179.md), at the [peh](../../strongs/h/h6310.md) of the [qereṯ](../../strongs/h/h7176.md), at the [māḇô'](../../strongs/h/h3996.md) in at the [peṯaḥ](../../strongs/h/h6607.md).

<a name="proverbs_8_4"></a>Proverbs 8:4

Unto you, O ['iysh](../../strongs/h/h376.md), I [qara'](../../strongs/h/h7121.md); and my [qowl](../../strongs/h/h6963.md) is to the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md).

<a name="proverbs_8_5"></a>Proverbs 8:5

O ye [pᵊṯî](../../strongs/h/h6612.md), [bîn](../../strongs/h/h995.md) [ʿārmâ](../../strongs/h/h6195.md): and, ye [kᵊsîl](../../strongs/h/h3684.md), be ye of a [bîn](../../strongs/h/h995.md) [leb](../../strongs/h/h3820.md).

<a name="proverbs_8_6"></a>Proverbs 8:6

[shama'](../../strongs/h/h8085.md); for I will [dabar](../../strongs/h/h1696.md) of [nāḡîḏ](../../strongs/h/h5057.md); and the [mip̄tāḥ](../../strongs/h/h4669.md) of my [saphah](../../strongs/h/h8193.md) shall be [meyshar](../../strongs/h/h4339.md).

<a name="proverbs_8_7"></a>Proverbs 8:7

For my [ḥēḵ](../../strongs/h/h2441.md) shall [hagah](../../strongs/h/h1897.md) ['emeth](../../strongs/h/h571.md); and [resha'](../../strongs/h/h7562.md) is a [tôʿēḇâ](../../strongs/h/h8441.md) to my [saphah](../../strongs/h/h8193.md).

<a name="proverbs_8_8"></a>Proverbs 8:8

All the ['emer](../../strongs/h/h561.md) of my [peh](../../strongs/h/h6310.md) are in [tsedeq](../../strongs/h/h6664.md); there is nothing [pāṯal](../../strongs/h/h6617.md) or [ʿiqqēš](../../strongs/h/h6141.md) in them.

<a name="proverbs_8_9"></a>Proverbs 8:9

They are all [nāḵōaḥ](../../strongs/h/h5228.md) to him that [bîn](../../strongs/h/h995.md), and [yashar](../../strongs/h/h3477.md) to them that [māṣā'](../../strongs/h/h4672.md) [da'ath](../../strongs/h/h1847.md).

<a name="proverbs_8_10"></a>Proverbs 8:10

[laqach](../../strongs/h/h3947.md) my [mûsār](../../strongs/h/h4148.md), and not [keceph](../../strongs/h/h3701.md); and [da'ath](../../strongs/h/h1847.md) rather than [bāḥar](../../strongs/h/h977.md) [ḥārûṣ](../../strongs/h/h2742.md).

<a name="proverbs_8_11"></a>Proverbs 8:11

For [ḥāḵmâ](../../strongs/h/h2451.md) is [towb](../../strongs/h/h2896.md) than [p̄nyym](../../strongs/h/h6443.md); and all the things that may be [chephets](../../strongs/h/h2656.md) are not to be [šāvâ](../../strongs/h/h7737.md) to it.

<a name="proverbs_8_12"></a>Proverbs 8:12

I [ḥāḵmâ](../../strongs/h/h2451.md) [shakan](../../strongs/h/h7931.md) with [ʿārmâ](../../strongs/h/h6195.md), and [māṣā'](../../strongs/h/h4672.md) [da'ath](../../strongs/h/h1847.md) of [mezimmah](../../strongs/h/h4209.md).

<a name="proverbs_8_13"></a>Proverbs 8:13

The [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) is to [sane'](../../strongs/h/h8130.md) [ra'](../../strongs/h/h7451.md): [gē'â](../../strongs/h/h1344.md), and [gā'ôn](../../strongs/h/h1347.md), and the [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md), and the [tahpuḵôṯ](../../strongs/h/h8419.md) [peh](../../strongs/h/h6310.md), do I [sane'](../../strongs/h/h8130.md).

<a name="proverbs_8_14"></a>Proverbs 8:14

['etsah](../../strongs/h/h6098.md) is mine, and [tûšîyâ](../../strongs/h/h8454.md): I am [bînâ](../../strongs/h/h998.md); I have [gᵊḇûrâ](../../strongs/h/h1369.md).

<a name="proverbs_8_15"></a>Proverbs 8:15

By me [melek](../../strongs/h/h4428.md) [mālaḵ](../../strongs/h/h4427.md), and [razan](../../strongs/h/h7336.md) [ḥāqaq](../../strongs/h/h2710.md) [tsedeq](../../strongs/h/h6664.md).

<a name="proverbs_8_16"></a>Proverbs 8:16

By me [śar](../../strongs/h/h8269.md) [śārar](../../strongs/h/h8323.md), and [nāḏîḇ](../../strongs/h/h5081.md), even all the [shaphat](../../strongs/h/h8199.md) of the ['erets](../../strongs/h/h776.md).

<a name="proverbs_8_17"></a>Proverbs 8:17

I ['ahab](../../strongs/h/h157.md) them that ['ahab](../../strongs/h/h157.md) me; and those that seek me [šāḥar](../../strongs/h/h7836.md) shall [māṣā'](../../strongs/h/h4672.md) me.

<a name="proverbs_8_18"></a>Proverbs 8:18

[ʿōšer](../../strongs/h/h6239.md) and [kabowd](../../strongs/h/h3519.md) are with me; yea, [ʿāṯēq](../../strongs/h/h6276.md) [hôn](../../strongs/h/h1952.md) and [tsedaqah](../../strongs/h/h6666.md).

<a name="proverbs_8_19"></a>Proverbs 8:19

My [pĕriy](../../strongs/h/h6529.md) is [towb](../../strongs/h/h2896.md) than [ḥārûṣ](../../strongs/h/h2742.md), yea, than [pāz](../../strongs/h/h6337.md); and my [tᵊḇû'â](../../strongs/h/h8393.md) than [bāḥar](../../strongs/h/h977.md) [keceph](../../strongs/h/h3701.md).

<a name="proverbs_8_20"></a>Proverbs 8:20

I [halak](../../strongs/h/h1980.md) in the ['orach](../../strongs/h/h734.md) of [tsedaqah](../../strongs/h/h6666.md), in the [tavek](../../strongs/h/h8432.md) of the [nāṯîḇ](../../strongs/h/h5410.md) of [mishpat](../../strongs/h/h4941.md):

<a name="proverbs_8_21"></a>Proverbs 8:21

That I may cause those that ['ahab](../../strongs/h/h157.md) me to [nāḥal](../../strongs/h/h5157.md) [yēš](../../strongs/h/h3426.md); and I will [mālā'](../../strongs/h/h4390.md) their ['ôṣār](../../strongs/h/h214.md).

<a name="proverbs_8_22"></a>Proverbs 8:22

[Yĕhovah](../../strongs/h/h3068.md) [qānâ](../../strongs/h/h7069.md) me in the [re'shiyth](../../strongs/h/h7225.md) of his [derek](../../strongs/h/h1870.md), [qeḏem](../../strongs/h/h6924.md) his [mip̄ʿāl](../../strongs/h/h4659.md) of old.

<a name="proverbs_8_23"></a>Proverbs 8:23

I was [nacak](../../strongs/h/h5258.md) from ['owlam](../../strongs/h/h5769.md), from the [ro'sh](../../strongs/h/h7218.md), or [qeḏem](../../strongs/h/h6924.md) the ['erets](../../strongs/h/h776.md) was.

<a name="proverbs_8_24"></a>Proverbs 8:24

When there were no [tĕhowm](../../strongs/h/h8415.md), I was [chuwl](../../strongs/h/h2342.md); when there were no [maʿyān](../../strongs/h/h4599.md) [kabad](../../strongs/h/h3513.md) with [mayim](../../strongs/h/h4325.md).

<a name="proverbs_8_25"></a>Proverbs 8:25

Before the [har](../../strongs/h/h2022.md) were [ṭāḇaʿ](../../strongs/h/h2883.md), [paniym](../../strongs/h/h6440.md) the [giḇʿâ](../../strongs/h/h1389.md) was I [chuwl](../../strongs/h/h2342.md):

<a name="proverbs_8_26"></a>Proverbs 8:26

While as yet he had not ['asah](../../strongs/h/h6213.md) the ['erets](../../strongs/h/h776.md), nor the [ḥûṣ](../../strongs/h/h2351.md), nor the [ro'sh](../../strongs/h/h7218.md) of the ['aphar](../../strongs/h/h6083.md) of the [tebel](../../strongs/h/h8398.md).

<a name="proverbs_8_27"></a>Proverbs 8:27

When he [kuwn](../../strongs/h/h3559.md) the [shamayim](../../strongs/h/h8064.md), I was there: when he [ḥāqaq](../../strongs/h/h2710.md) a [ḥûḡ](../../strongs/h/h2329.md) upon the [paniym](../../strongs/h/h6440.md) of the [tĕhowm](../../strongs/h/h8415.md):

<a name="proverbs_8_28"></a>Proverbs 8:28

When he ['amats](../../strongs/h/h553.md) the [shachaq](../../strongs/h/h7834.md) [maʿal](../../strongs/h/h4605.md): when he ['azaz](../../strongs/h/h5810.md) the ['ayin](../../strongs/h/h5869.md) of the [tĕhowm](../../strongs/h/h8415.md):

<a name="proverbs_8_29"></a>Proverbs 8:29

When he [śûm](../../strongs/h/h7760.md) to the [yam](../../strongs/h/h3220.md) his [choq](../../strongs/h/h2706.md), that the [mayim](../../strongs/h/h4325.md) should not ['abar](../../strongs/h/h5674.md) his [peh](../../strongs/h/h6310.md): when he [ḥāqaq](../../strongs/h/h2710.md) the [môsāḏ](../../strongs/h/h4144.md) of the ['erets](../../strongs/h/h776.md):

<a name="proverbs_8_30"></a>Proverbs 8:30

Then I was by ['ēṣel](../../strongs/h/h681.md), as one ['āmôn](../../strongs/h/h525.md) with him: and I was [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) his [šaʿăšûʿîm](../../strongs/h/h8191.md), [śāḥaq](../../strongs/h/h7832.md) [ʿēṯ](../../strongs/h/h6256.md) [paniym](../../strongs/h/h6440.md) him;

<a name="proverbs_8_31"></a>Proverbs 8:31

[śāḥaq](../../strongs/h/h7832.md) in the [tebel](../../strongs/h/h8398.md) of his ['erets](../../strongs/h/h776.md); and my [šaʿăšûʿîm](../../strongs/h/h8191.md) were with the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md).

<a name="proverbs_8_32"></a>Proverbs 8:32

Now therefore [shama'](../../strongs/h/h8085.md) unto me, O ye [ben](../../strongs/h/h1121.md): for ['esher](../../strongs/h/h835.md) are they that [shamar](../../strongs/h/h8104.md) my [derek](../../strongs/h/h1870.md).

<a name="proverbs_8_33"></a>Proverbs 8:33

[shama'](../../strongs/h/h8085.md) [mûsār](../../strongs/h/h4148.md), and be [ḥāḵam](../../strongs/h/h2449.md), and [pāraʿ](../../strongs/h/h6544.md) it not.

<a name="proverbs_8_34"></a>Proverbs 8:34

['esher](../../strongs/h/h835.md) is the ['āḏām](../../strongs/h/h120.md) that [shama'](../../strongs/h/h8085.md) me, [šāqaḏ](../../strongs/h/h8245.md) [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) at my [deleṯ](../../strongs/h/h1817.md), [shamar](../../strongs/h/h8104.md) at the [mᵊzûzâ](../../strongs/h/h4201.md) of my [peṯaḥ](../../strongs/h/h6607.md).

<a name="proverbs_8_35"></a>Proverbs 8:35

For whoso [māṣā'](../../strongs/h/h4672.md) me [māṣā'](../../strongs/h/h4672.md) [māṣā'](../../strongs/h/h4672.md) [chay](../../strongs/h/h2416.md), and shall [pûq](../../strongs/h/h6329.md) [ratsown](../../strongs/h/h7522.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="proverbs_8_36"></a>Proverbs 8:36

But he that [chata'](../../strongs/h/h2398.md) against me [ḥāmas](../../strongs/h/h2554.md) his own [nephesh](../../strongs/h/h5315.md): all they that [sane'](../../strongs/h/h8130.md) me ['ahab](../../strongs/h/h157.md) [maveth](../../strongs/h/h4194.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 7](proverbs_7.md) - [Proverbs 9](proverbs_9.md)