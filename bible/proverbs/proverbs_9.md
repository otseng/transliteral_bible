# [Proverbs 9](https://www.blueletterbible.org/kjv/proverbs/9)

<a name="proverbs_9_1"></a>Proverbs 9:1

[ḥāḵmôṯ](../../strongs/h/h2454.md) hath [bānâ](../../strongs/h/h1129.md) her [bayith](../../strongs/h/h1004.md), she hath [ḥāṣaḇ](../../strongs/h/h2672.md) her [šeḇaʿ](../../strongs/h/h7651.md) [ʿammûḏ](../../strongs/h/h5982.md):

<a name="proverbs_9_2"></a>Proverbs 9:2

She hath [ṭāḇaḥ](../../strongs/h/h2873.md) her [ṭeḇaḥ](../../strongs/h/h2874.md); she hath [māsaḵ](../../strongs/h/h4537.md) her [yayin](../../strongs/h/h3196.md); she hath also ['arak](../../strongs/h/h6186.md) her [šulḥān](../../strongs/h/h7979.md).

<a name="proverbs_9_3"></a>Proverbs 9:3

She hath [shalach](../../strongs/h/h7971.md) her [naʿărâ](../../strongs/h/h5291.md): she [qara'](../../strongs/h/h7121.md) [gap̄](../../strongs/h/h1610.md) the [marowm](../../strongs/h/h4791.md) of the [qereṯ](../../strongs/h/h7176.md),

<a name="proverbs_9_4"></a>Proverbs 9:4

Whoso is [pᵊṯî](../../strongs/h/h6612.md), let him [cuwr](../../strongs/h/h5493.md) hither: as for him that [ḥāsēr](../../strongs/h/h2638.md) [leb](../../strongs/h/h3820.md), she ['āmar](../../strongs/h/h559.md) to him,

<a name="proverbs_9_5"></a>Proverbs 9:5

[yālaḵ](../../strongs/h/h3212.md), [lāḥam](../../strongs/h/h3898.md) of my [lechem](../../strongs/h/h3899.md), and [šāṯâ](../../strongs/h/h8354.md) of the [yayin](../../strongs/h/h3196.md) which I have [māsaḵ](../../strongs/h/h4537.md).

<a name="proverbs_9_6"></a>Proverbs 9:6

['azab](../../strongs/h/h5800.md) the [pᵊṯî](../../strongs/h/h6612.md), and [ḥāyâ](../../strongs/h/h2421.md); and ['āšar](../../strongs/h/h833.md) in the [derek](../../strongs/h/h1870.md) of [bînâ](../../strongs/h/h998.md).

<a name="proverbs_9_7"></a>Proverbs 9:7

He that [yacar](../../strongs/h/h3256.md) a [luwts](../../strongs/h/h3887.md) [laqach](../../strongs/h/h3947.md) to himself [qālôn](../../strongs/h/h7036.md): and he that [yakach](../../strongs/h/h3198.md) a [rasha'](../../strongs/h/h7563.md) man getteth himself a [mᵊ'ûm](../../strongs/h/h3971.md).

<a name="proverbs_9_8"></a>Proverbs 9:8

[yakach](../../strongs/h/h3198.md) not a [luwts](../../strongs/h/h3887.md), lest he [sane'](../../strongs/h/h8130.md) thee: [yakach](../../strongs/h/h3198.md) a [ḥāḵām](../../strongs/h/h2450.md), and he will ['ahab](../../strongs/h/h157.md) thee.

<a name="proverbs_9_9"></a>Proverbs 9:9

[nathan](../../strongs/h/h5414.md) instruction to a [ḥāḵām](../../strongs/h/h2450.md), and he will be yet [ḥāḵam](../../strongs/h/h2449.md): [yada'](../../strongs/h/h3045.md) a [tsaddiyq](../../strongs/h/h6662.md) man, and he will [yāsap̄](../../strongs/h/h3254.md) in [leqaḥ](../../strongs/h/h3948.md).

<a name="proverbs_9_10"></a>Proverbs 9:10

The [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) is the [tᵊḥillâ](../../strongs/h/h8462.md) of [ḥāḵmâ](../../strongs/h/h2451.md): and the [da'ath](../../strongs/h/h1847.md) of the [qadowsh](../../strongs/h/h6918.md) is [bînâ](../../strongs/h/h998.md).

<a name="proverbs_9_11"></a>Proverbs 9:11

For by me thy [yowm](../../strongs/h/h3117.md) shall be [rabah](../../strongs/h/h7235.md), and the [šānâ](../../strongs/h/h8141.md) of thy [chay](../../strongs/h/h2416.md) shall be [yāsap̄](../../strongs/h/h3254.md).

<a name="proverbs_9_12"></a>Proverbs 9:12

If thou be [ḥāḵam](../../strongs/h/h2449.md), thou shalt be [ḥāḵam](../../strongs/h/h2449.md) for thyself: but if thou [luwts](../../strongs/h/h3887.md), thou alone shalt [nasa'](../../strongs/h/h5375.md) it.

<a name="proverbs_9_13"></a>Proverbs 9:13

A [kᵊsîlûṯ](../../strongs/h/h3687.md) ['ishshah](../../strongs/h/h802.md) is [hāmâ](../../strongs/h/h1993.md): she is [pᵊṯayyûṯ](../../strongs/h/h6615.md), [bal](../../strongs/h/h1077.md) [yada'](../../strongs/h/h3045.md) [mah](../../strongs/h/h4100.md).

<a name="proverbs_9_14"></a>Proverbs 9:14

For she [yashab](../../strongs/h/h3427.md) at the [peṯaḥ](../../strongs/h/h6607.md) of her [bayith](../../strongs/h/h1004.md), on a [kicce'](../../strongs/h/h3678.md) in the [marowm](../../strongs/h/h4791.md) of the [qereṯ](../../strongs/h/h7176.md),

<a name="proverbs_9_15"></a>Proverbs 9:15

To [qara'](../../strongs/h/h7121.md) ['abar](../../strongs/h/h5674.md) [derek](../../strongs/h/h1870.md) who [yashar](../../strongs/h/h3474.md) on their ['orach](../../strongs/h/h734.md):

<a name="proverbs_9_16"></a>Proverbs 9:16

Whoso is [pᵊṯî](../../strongs/h/h6612.md), let him [cuwr](../../strongs/h/h5493.md) hither: and as for him that [ḥāsēr](../../strongs/h/h2638.md) [leb](../../strongs/h/h3820.md), she ['āmar](../../strongs/h/h559.md) to him,

<a name="proverbs_9_17"></a>Proverbs 9:17

[ganab](../../strongs/h/h1589.md) [mayim](../../strongs/h/h4325.md) are [māṯaq](../../strongs/h/h4985.md), and [lechem](../../strongs/h/h3899.md) eaten in [cether](../../strongs/h/h5643.md) is [nāʿēm](../../strongs/h/h5276.md).

<a name="proverbs_9_18"></a>Proverbs 9:18

But he [yada'](../../strongs/h/h3045.md) not that the [rᵊp̄ā'îm](../../strongs/h/h7496.md) are there; and that her [qara'](../../strongs/h/h7121.md) are in the [ʿāmēq](../../strongs/h/h6012.md) of [shĕ'owl](../../strongs/h/h7585.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 8](proverbs_8.md) - [Proverbs 10](proverbs_10.md)