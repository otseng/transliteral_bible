# [Proverbs 24](https://www.blueletterbible.org/kjv/proverbs/24)

<a name="proverbs_24_1"></a>Proverbs 24:1

Be not thou [qānā'](../../strongs/h/h7065.md) against [ra'](../../strongs/h/h7451.md) ['enowsh](../../strongs/h/h582.md), neither ['āvâ](../../strongs/h/h183.md) to be with them.

<a name="proverbs_24_2"></a>Proverbs 24:2

For their [leb](../../strongs/h/h3820.md) [hagah](../../strongs/h/h1897.md) [shod](../../strongs/h/h7701.md), and their [saphah](../../strongs/h/h8193.md) [dabar](../../strongs/h/h1696.md) of ['amal](../../strongs/h/h5999.md).

<a name="proverbs_24_3"></a>Proverbs 24:3

Through [ḥāḵmâ](../../strongs/h/h2451.md) is a [bayith](../../strongs/h/h1004.md) [bānâ](../../strongs/h/h1129.md); and by [tāḇûn](../../strongs/h/h8394.md) it is [kuwn](../../strongs/h/h3559.md):

<a name="proverbs_24_4"></a>Proverbs 24:4

And by [da'ath](../../strongs/h/h1847.md) shall the [ḥeḏer](../../strongs/h/h2315.md) be [mālā'](../../strongs/h/h4390.md) with all [yāqār](../../strongs/h/h3368.md) and [na'iym](../../strongs/h/h5273.md) [hôn](../../strongs/h/h1952.md).

<a name="proverbs_24_5"></a>Proverbs 24:5

A [ḥāḵām](../../strongs/h/h2450.md) [geḇer](../../strongs/h/h1397.md) is ['oz](../../strongs/h/h5797.md); yea, an ['iysh](../../strongs/h/h376.md) of [da'ath](../../strongs/h/h1847.md) ['amats](../../strongs/h/h553.md) [koach](../../strongs/h/h3581.md).

<a name="proverbs_24_6"></a>Proverbs 24:6

For by [taḥbulôṯ](../../strongs/h/h8458.md) thou shalt ['asah](../../strongs/h/h6213.md) thy [milḥāmâ](../../strongs/h/h4421.md): and in [rōḇ](../../strongs/h/h7230.md) of [ya'ats](../../strongs/h/h3289.md) there is [tᵊšûʿâ](../../strongs/h/h8668.md).

<a name="proverbs_24_7"></a>Proverbs 24:7

[ḥāḵmôṯ](../../strongs/h/h2454.md) is too [ruwm](../../strongs/h/h7311.md) for an ['ĕvîl](../../strongs/h/h191.md): he [pāṯaḥ](../../strongs/h/h6605.md) not his [peh](../../strongs/h/h6310.md) in the [sha'ar](../../strongs/h/h8179.md).

<a name="proverbs_24_8"></a>Proverbs 24:8

He that [chashab](../../strongs/h/h2803.md) to do [ra'a'](../../strongs/h/h7489.md) shall be [qara'](../../strongs/h/h7121.md) a [mezimmah](../../strongs/h/h4209.md) [baʿal](../../strongs/h/h1167.md).

<a name="proverbs_24_9"></a>Proverbs 24:9

The [zimmâ](../../strongs/h/h2154.md) of ['iûeleṯ](../../strongs/h/h200.md) is [chatta'ath](../../strongs/h/h2403.md): and the [luwts](../../strongs/h/h3887.md) is a [tôʿēḇâ](../../strongs/h/h8441.md) to ['āḏām](../../strongs/h/h120.md).

<a name="proverbs_24_10"></a>Proverbs 24:10

If thou [rāp̄â](../../strongs/h/h7503.md) in the [yowm](../../strongs/h/h3117.md) of [tsarah](../../strongs/h/h6869.md), thy [koach](../../strongs/h/h3581.md) is [tsar](../../strongs/h/h6862.md).

<a name="proverbs_24_11"></a>Proverbs 24:11

If thou [ḥāśaḵ](../../strongs/h/h2820.md) to [natsal](../../strongs/h/h5337.md) them that are [laqach](../../strongs/h/h3947.md) unto [maveth](../../strongs/h/h4194.md), and those that are [mowt](../../strongs/h/h4131.md) to be [hereḡ](../../strongs/h/h2027.md);

<a name="proverbs_24_12"></a>Proverbs 24:12

If thou ['āmar](../../strongs/h/h559.md), Behold, we [yada'](../../strongs/h/h3045.md) it not; doth not he that [tāḵan](../../strongs/h/h8505.md) the [libbah](../../strongs/h/h3826.md) [bîn](../../strongs/h/h995.md) it? and he that [nāṣar](../../strongs/h/h5341.md) thy [nephesh](../../strongs/h/h5315.md), doth not he [yada'](../../strongs/h/h3045.md) it? and shall not he [shuwb](../../strongs/h/h7725.md) to every ['āḏām](../../strongs/h/h120.md) according to his [pōʿal](../../strongs/h/h6467.md)?

<a name="proverbs_24_13"></a>Proverbs 24:13

My [ben](../../strongs/h/h1121.md), ['akal](../../strongs/h/h398.md) thou [dĕbash](../../strongs/h/h1706.md), because it is [towb](../../strongs/h/h2896.md); and the [nōp̄eṯ](../../strongs/h/h5317.md), which is [māṯôq](../../strongs/h/h4966.md) to thy [ḥēḵ](../../strongs/h/h2441.md):

<a name="proverbs_24_14"></a>Proverbs 24:14

So shall the [yada'](../../strongs/h/h3045.md) of [ḥāḵmâ](../../strongs/h/h2451.md) be unto thy [nephesh](../../strongs/h/h5315.md): when thou hast [māṣā'](../../strongs/h/h4672.md) it, then there shall [yēš](../../strongs/h/h3426.md) an ['aḥărîṯ](../../strongs/h/h319.md), and thy [tiqvâ](../../strongs/h/h8615.md) shall not be [karath](../../strongs/h/h3772.md).

<a name="proverbs_24_15"></a>Proverbs 24:15

Lay not ['arab](../../strongs/h/h693.md), O [rasha'](../../strongs/h/h7563.md) man, against the [nāvê](../../strongs/h/h5116.md) of the [tsaddiyq](../../strongs/h/h6662.md); [shadad](../../strongs/h/h7703.md) not his [rēḇeṣ](../../strongs/h/h7258.md):

<a name="proverbs_24_16"></a>Proverbs 24:16

For a [tsaddiyq](../../strongs/h/h6662.md) man [naphal](../../strongs/h/h5307.md) seven [šeḇaʿ](../../strongs/h/h7651.md), and [quwm](../../strongs/h/h6965.md): but the [rasha'](../../strongs/h/h7563.md) shall [kashal](../../strongs/h/h3782.md) into [ra'](../../strongs/h/h7451.md).

<a name="proverbs_24_17"></a>Proverbs 24:17

[samach](../../strongs/h/h8055.md) not when thine ['oyeb](../../strongs/h/h341.md) [naphal](../../strongs/h/h5307.md), and let not thine [leb](../../strongs/h/h3820.md) be [giyl](../../strongs/h/h1523.md) when he [kashal](../../strongs/h/h3782.md):

<a name="proverbs_24_18"></a>Proverbs 24:18

Lest [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) it, and it [ra'a'](../../strongs/h/h7489.md) ['ayin](../../strongs/h/h5869.md) him, and he [shuwb](../../strongs/h/h7725.md) his ['aph](../../strongs/h/h639.md) from him.

<a name="proverbs_24_19"></a>Proverbs 24:19

[ḥārâ](../../strongs/h/h2734.md) not thyself because of [ra'a'](../../strongs/h/h7489.md) men, neither be thou [qānā'](../../strongs/h/h7065.md) at the [rasha'](../../strongs/h/h7563.md);

<a name="proverbs_24_20"></a>Proverbs 24:20

For there shall be no ['aḥărîṯ](../../strongs/h/h319.md) to the [ra'](../../strongs/h/h7451.md) man; the [nîr](../../strongs/h/h5216.md) of the [rasha'](../../strongs/h/h7563.md) shall be put [dāʿaḵ](../../strongs/h/h1846.md).

<a name="proverbs_24_21"></a>Proverbs 24:21

My [ben](../../strongs/h/h1121.md), [yare'](../../strongs/h/h3372.md) thou [Yĕhovah](../../strongs/h/h3068.md) and the [melek](../../strongs/h/h4428.md): and [ʿāraḇ](../../strongs/h/h6148.md) not with them that are given to [šānâ](../../strongs/h/h8138.md):

<a name="proverbs_24_22"></a>Proverbs 24:22

For their ['êḏ](../../strongs/h/h343.md) shall [quwm](../../strongs/h/h6965.md) [piṯ'ōm](../../strongs/h/h6597.md); and who [yada'](../../strongs/h/h3045.md) the [pîḏ](../../strongs/h/h6365.md) of them [šᵊnayim](../../strongs/h/h8147.md)?

<a name="proverbs_24_23"></a>Proverbs 24:23

These things also belong to the [ḥāḵām](../../strongs/h/h2450.md). It is not [towb](../../strongs/h/h2896.md) to have [nāḵar](../../strongs/h/h5234.md) of [paniym](../../strongs/h/h6440.md) in [mishpat](../../strongs/h/h4941.md).

<a name="proverbs_24_24"></a>Proverbs 24:24

He that ['āmar](../../strongs/h/h559.md) unto the [rasha'](../../strongs/h/h7563.md), Thou art [tsaddiyq](../../strongs/h/h6662.md); him shall the ['am](../../strongs/h/h5971.md) [nāqaḇ](../../strongs/h/h5344.md), [lĕom](../../strongs/h/h3816.md) shall [za'am](../../strongs/h/h2194.md) him:

<a name="proverbs_24_25"></a>Proverbs 24:25

But to them that [yakach](../../strongs/h/h3198.md) him shall be [nāʿēm](../../strongs/h/h5276.md), and a [towb](../../strongs/h/h2896.md) [bĕrakah](../../strongs/h/h1293.md) shall [bow'](../../strongs/h/h935.md) upon them.

<a name="proverbs_24_26"></a>Proverbs 24:26

Every man shall [nashaq](../../strongs/h/h5401.md) his [saphah](../../strongs/h/h8193.md) that [shuwb](../../strongs/h/h7725.md) a [nāḵōaḥ](../../strongs/h/h5228.md) [dabar](../../strongs/h/h1697.md).

<a name="proverbs_24_27"></a>Proverbs 24:27

[kuwn](../../strongs/h/h3559.md) thy [mĕla'kah](../../strongs/h/h4399.md) [ḥûṣ](../../strongs/h/h2351.md), and make it [ʿāṯaḏ](../../strongs/h/h6257.md) for thyself in the [sadeh](../../strongs/h/h7704.md); and ['aḥar](../../strongs/h/h310.md) [bānâ](../../strongs/h/h1129.md) thine [bayith](../../strongs/h/h1004.md).

<a name="proverbs_24_28"></a>Proverbs 24:28

Be not an ['ed](../../strongs/h/h5707.md) against thy [rea'](../../strongs/h/h7453.md) without [ḥinnām](../../strongs/h/h2600.md); and [pāṯâ](../../strongs/h/h6601.md) not with thy [saphah](../../strongs/h/h8193.md).

<a name="proverbs_24_29"></a>Proverbs 24:29

['āmar](../../strongs/h/h559.md) not, I will ['asah](../../strongs/h/h6213.md) so to him as he hath ['asah](../../strongs/h/h6213.md) to me: I will [shuwb](../../strongs/h/h7725.md) to the ['iysh](../../strongs/h/h376.md) according to his [pōʿal](../../strongs/h/h6467.md).

<a name="proverbs_24_30"></a>Proverbs 24:30

I ['abar](../../strongs/h/h5674.md) by the [sadeh](../../strongs/h/h7704.md) of the ['iysh](../../strongs/h/h376.md) [ʿāṣēl](../../strongs/h/h6102.md), and by the [kerem](../../strongs/h/h3754.md) of the ['āḏām](../../strongs/h/h120.md) [ḥāsēr](../../strongs/h/h2638.md) of [leb](../../strongs/h/h3820.md);

<a name="proverbs_24_31"></a>Proverbs 24:31

And, lo, it was all grown [ʿālâ](../../strongs/h/h5927.md) with [qmvש](../../strongs/h/h7063.md), and [ḥārûl](../../strongs/h/h2738.md) had [kāsâ](../../strongs/h/h3680.md) the [paniym](../../strongs/h/h6440.md) thereof, and the ['eben](../../strongs/h/h68.md) [geḏer](../../strongs/h/h1444.md) thereof was [harac](../../strongs/h/h2040.md).

<a name="proverbs_24_32"></a>Proverbs 24:32

Then I [chazah](../../strongs/h/h2372.md), and considered it [shiyth](../../strongs/h/h7896.md) [leb](../../strongs/h/h3820.md): I [ra'ah](../../strongs/h/h7200.md) upon it, and [laqach](../../strongs/h/h3947.md) [mûsār](../../strongs/h/h4148.md).

<a name="proverbs_24_33"></a>Proverbs 24:33

Yet a [mᵊʿaṭ](../../strongs/h/h4592.md) [šēnā'](../../strongs/h/h8142.md), a [mᵊʿaṭ](../../strongs/h/h4592.md) [tᵊnûmâ](../../strongs/h/h8572.md), a [mᵊʿaṭ](../../strongs/h/h4592.md) [ḥibuq](../../strongs/h/h2264.md) of the [yad](../../strongs/h/h3027.md) to [shakab](../../strongs/h/h7901.md):

<a name="proverbs_24_34"></a>Proverbs 24:34

So shall thy [rêš](../../strongs/h/h7389.md) [bow'](../../strongs/h/h935.md) as one that [halak](../../strongs/h/h1980.md); and thy [maḥsôr](../../strongs/h/h4270.md) as a [magen](../../strongs/h/h4043.md) ['iysh](../../strongs/h/h376.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 23](proverbs_23.md) - [Proverbs 25](proverbs_25.md)