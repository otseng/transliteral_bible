# [Proverbs 20](https://www.blueletterbible.org/kjv/proverbs/20)

<a name="proverbs_20_1"></a>Proverbs 20:1

[yayin](../../strongs/h/h3196.md) is a [luwts](../../strongs/h/h3887.md), [šēḵār](../../strongs/h/h7941.md) is [hāmâ](../../strongs/h/h1993.md): and whosoever is [šāḡâ](../../strongs/h/h7686.md) thereby is not [ḥāḵam](../../strongs/h/h2449.md).

<a name="proverbs_20_2"></a>Proverbs 20:2

The ['êmâ](../../strongs/h/h367.md) of a [melek](../../strongs/h/h4428.md) is as the [naham](../../strongs/h/h5099.md) of a [kephiyr](../../strongs/h/h3715.md): whoso provoketh him to ['abar](../../strongs/h/h5674.md) [chata'](../../strongs/h/h2398.md) against his own [nephesh](../../strongs/h/h5315.md).

<a name="proverbs_20_3"></a>Proverbs 20:3

It is a [kabowd](../../strongs/h/h3519.md) for an ['iysh](../../strongs/h/h376.md) to [šeḇeṯ](../../strongs/h/h7674.md) from [rîḇ](../../strongs/h/h7379.md): but every ['ĕvîl](../../strongs/h/h191.md) will be [gālaʿ](../../strongs/h/h1566.md).

<a name="proverbs_20_4"></a>Proverbs 20:4

The [ʿāṣēl](../../strongs/h/h6102.md) will not [ḥāraš](../../strongs/h/h2790.md) by reason of the [ḥōrep̄](../../strongs/h/h2779.md); therefore shall he [sha'al](../../strongs/h/h7592.md) [sha'al](../../strongs/h/h7592.md) in [qāṣîr](../../strongs/h/h7105.md), and have nothing.

<a name="proverbs_20_5"></a>Proverbs 20:5

['etsah](../../strongs/h/h6098.md) in the [leb](../../strongs/h/h3820.md) of ['iysh](../../strongs/h/h376.md) is like [ʿāmōq](../../strongs/h/h6013.md) [mayim](../../strongs/h/h4325.md); but an ['iysh](../../strongs/h/h376.md) of [tāḇûn](../../strongs/h/h8394.md) will draw it [dālâ](../../strongs/h/h1802.md).

<a name="proverbs_20_6"></a>Proverbs 20:6

[rōḇ](../../strongs/h/h7230.md) ['āḏām](../../strongs/h/h120.md) will [qara'](../../strongs/h/h7121.md) every ['iysh](../../strongs/h/h376.md) his own [checed](../../strongs/h/h2617.md): but an ['ēmûn](../../strongs/h/h529.md) ['iysh](../../strongs/h/h376.md) who can [māṣā'](../../strongs/h/h4672.md)?

<a name="proverbs_20_7"></a>Proverbs 20:7

The [tsaddiyq](../../strongs/h/h6662.md) man [halak](../../strongs/h/h1980.md) in his [tom](../../strongs/h/h8537.md): his [ben](../../strongs/h/h1121.md) are ['esher](../../strongs/h/h835.md) ['aḥar](../../strongs/h/h310.md) him.

<a name="proverbs_20_8"></a>Proverbs 20:8

A [melek](../../strongs/h/h4428.md) that [yashab](../../strongs/h/h3427.md) the [kicce'](../../strongs/h/h3678.md) of [diyn](../../strongs/h/h1779.md) [zārâ](../../strongs/h/h2219.md) all [ra'](../../strongs/h/h7451.md) with his ['ayin](../../strongs/h/h5869.md).

<a name="proverbs_20_9"></a>Proverbs 20:9

Who can ['āmar](../../strongs/h/h559.md), I have made my [leb](../../strongs/h/h3820.md) [zāḵâ](../../strongs/h/h2135.md), I am [ṭāhēr](../../strongs/h/h2891.md) from my [chatta'ath](../../strongs/h/h2403.md)?

<a name="proverbs_20_10"></a>Proverbs 20:10

['eben](../../strongs/h/h68.md) ['eben](../../strongs/h/h68.md), and ['êp̄â](../../strongs/h/h374.md) ['êp̄â](../../strongs/h/h374.md), [šᵊnayim](../../strongs/h/h8147.md) of them are [gam](../../strongs/h/h1571.md) [tôʿēḇâ](../../strongs/h/h8441.md) to [Yĕhovah](../../strongs/h/h3068.md).

<a name="proverbs_20_11"></a>Proverbs 20:11

Even a [naʿar](../../strongs/h/h5288.md) is [nāḵar](../../strongs/h/h5234.md) by his [maʿălāl](../../strongs/h/h4611.md), whether his [pōʿal](../../strongs/h/h6467.md) be [zāḵ](../../strongs/h/h2134.md), and whether it be [yashar](../../strongs/h/h3477.md).

<a name="proverbs_20_12"></a>Proverbs 20:12

The [shama'](../../strongs/h/h8085.md) ['ozen](../../strongs/h/h241.md), and the [ra'ah](../../strongs/h/h7200.md) ['ayin](../../strongs/h/h5869.md), [Yĕhovah](../../strongs/h/h3068.md) hath ['asah](../../strongs/h/h6213.md) even [šᵊnayim](../../strongs/h/h8147.md) of them.

<a name="proverbs_20_13"></a>Proverbs 20:13

['ahab](../../strongs/h/h157.md) not [šēnā'](../../strongs/h/h8142.md), lest thou come to [yarash](../../strongs/h/h3423.md); [paqach](../../strongs/h/h6491.md) thine ['ayin](../../strongs/h/h5869.md), and thou shalt be [sāׂbaʿ](../../strongs/h/h7646.md) with [lechem](../../strongs/h/h3899.md).

<a name="proverbs_20_14"></a>Proverbs 20:14

It is [ra'](../../strongs/h/h7451.md), it is [ra'](../../strongs/h/h7451.md), ['āmar](../../strongs/h/h559.md) the [qānâ](../../strongs/h/h7069.md): but when he is ['āzal](../../strongs/h/h235.md) his way, then he [halal](../../strongs/h/h1984.md).

<a name="proverbs_20_15"></a>Proverbs 20:15

There [yēš](../../strongs/h/h3426.md) [zāhāḇ](../../strongs/h/h2091.md), and a [rōḇ](../../strongs/h/h7230.md) of [p̄nyym](../../strongs/h/h6443.md): but the [saphah](../../strongs/h/h8193.md) of [da'ath](../../strongs/h/h1847.md) are a [yᵊqār](../../strongs/h/h3366.md) [kĕliy](../../strongs/h/h3627.md).

<a name="proverbs_20_16"></a>Proverbs 20:16

[laqach](../../strongs/h/h3947.md) his [beḡeḏ](../../strongs/h/h899.md) that is [ʿāraḇ](../../strongs/h/h6148.md) for a [zûr](../../strongs/h/h2114.md): and take a [chabal](../../strongs/h/h2254.md) of him for a [nāḵrî](../../strongs/h/h5237.md).

<a name="proverbs_20_17"></a>Proverbs 20:17

[lechem](../../strongs/h/h3899.md) of [sheqer](../../strongs/h/h8267.md) is [ʿārēḇ](../../strongs/h/h6156.md) to an ['iysh](../../strongs/h/h376.md); but ['aḥar](../../strongs/h/h310.md) his [peh](../../strongs/h/h6310.md) shall be [mālā'](../../strongs/h/h4390.md) with [ḥāṣāṣ](../../strongs/h/h2687.md).

<a name="proverbs_20_18"></a>Proverbs 20:18

Every [maḥăšāḇâ](../../strongs/h/h4284.md) is [kuwn](../../strongs/h/h3559.md) by ['etsah](../../strongs/h/h6098.md): and with [taḥbulôṯ](../../strongs/h/h8458.md) ['asah](../../strongs/h/h6213.md) [milḥāmâ](../../strongs/h/h4421.md).

<a name="proverbs_20_19"></a>Proverbs 20:19

He that goeth [halak](../../strongs/h/h1980.md) as a [rāḵîl](../../strongs/h/h7400.md) [gālâ](../../strongs/h/h1540.md) [sôḏ](../../strongs/h/h5475.md): therefore [ʿāraḇ](../../strongs/h/h6148.md) not with him that [pāṯâ](../../strongs/h/h6601.md) with his [saphah](../../strongs/h/h8193.md).

<a name="proverbs_20_20"></a>Proverbs 20:20

Whoso [qālal](../../strongs/h/h7043.md) his ['ab](../../strongs/h/h1.md) or his ['em](../../strongs/h/h517.md), his [nîr](../../strongs/h/h5216.md) shall be [dāʿaḵ](../../strongs/h/h1846.md) in ['iyshown](../../strongs/h/h380.md) ['iyshown](../../strongs/h/h380.md) [choshek](../../strongs/h/h2822.md).

<a name="proverbs_20_21"></a>Proverbs 20:21

A [nachalah](../../strongs/h/h5159.md) may be gotten [bahal](../../strongs/h/h926.md) [bāḥal](../../strongs/h/h973.md) at the [ri'šôn](../../strongs/h/h7223.md); but the ['aḥărîṯ](../../strongs/h/h319.md) thereof shall not be [barak](../../strongs/h/h1288.md).

<a name="proverbs_20_22"></a>Proverbs 20:22

['āmar](../../strongs/h/h559.md) not thou, I will [shalam](../../strongs/h/h7999.md) [ra'](../../strongs/h/h7451.md); but [qāvâ](../../strongs/h/h6960.md) [Yĕhovah](../../strongs/h/h3068.md), and he shall [yasha'](../../strongs/h/h3467.md) thee.

<a name="proverbs_20_23"></a>Proverbs 20:23

['eben](../../strongs/h/h68.md) ['eben](../../strongs/h/h68.md) are a [tôʿēḇâ](../../strongs/h/h8441.md) unto [Yĕhovah](../../strongs/h/h3068.md); and a [mirmah](../../strongs/h/h4820.md) [mō'znayim](../../strongs/h/h3976.md) is not [towb](../../strongs/h/h2896.md).

<a name="proverbs_20_24"></a>Proverbs 20:24

[geḇer](../../strongs/h/h1397.md) [miṣʿāḏ](../../strongs/h/h4703.md) are of [Yĕhovah](../../strongs/h/h3068.md); how can an ['āḏām](../../strongs/h/h120.md) then [bîn](../../strongs/h/h995.md) his own [derek](../../strongs/h/h1870.md)?

<a name="proverbs_20_25"></a>Proverbs 20:25

It is a [mowqesh](../../strongs/h/h4170.md) to the ['āḏām](../../strongs/h/h120.md) who [yālaʿ](../../strongs/h/h3216.md) that which is [qodesh](../../strongs/h/h6944.md), and ['aḥar](../../strongs/h/h310.md) [neḏer](../../strongs/h/h5088.md) to make [bāqar](../../strongs/h/h1239.md).

<a name="proverbs_20_26"></a>Proverbs 20:26

A [ḥāḵām](../../strongs/h/h2450.md) [melek](../../strongs/h/h4428.md) [zārâ](../../strongs/h/h2219.md) the [rasha'](../../strongs/h/h7563.md), and [shuwb](../../strongs/h/h7725.md) the ['ôp̄ān](../../strongs/h/h212.md) over them.

<a name="proverbs_20_27"></a>Proverbs 20:27

The [neshamah](../../strongs/h/h5397.md) of ['āḏām](../../strongs/h/h120.md) is the [nîr](../../strongs/h/h5216.md) of [Yĕhovah](../../strongs/h/h3068.md), [ḥāp̄aś](../../strongs/h/h2664.md) all the [ḥeḏer](../../strongs/h/h2315.md) of the [beten](../../strongs/h/h990.md).

<a name="proverbs_20_28"></a>Proverbs 20:28

[checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md) [nāṣar](../../strongs/h/h5341.md) the [melek](../../strongs/h/h4428.md): and his [kicce'](../../strongs/h/h3678.md) is [sāʿaḏ](../../strongs/h/h5582.md) by [checed](../../strongs/h/h2617.md).

<a name="proverbs_20_29"></a>Proverbs 20:29

The [tip̄'ārâ](../../strongs/h/h8597.md) of [bāḥûr](../../strongs/h/h970.md) is their [koach](../../strongs/h/h3581.md): and the [hadar](../../strongs/h/h1926.md) of [zāqēn](../../strongs/h/h2205.md) is the [śêḇâ](../../strongs/h/h7872.md).

<a name="proverbs_20_30"></a>Proverbs 20:30

The [ḥabûrâ](../../strongs/h/h2250.md) of a [peṣaʿ](../../strongs/h/h6482.md) [tamrûq](../../strongs/h/h8562.md) away [ra'](../../strongs/h/h7451.md): so do [makâ](../../strongs/h/h4347.md) the [ḥeḏer](../../strongs/h/h2315.md) of the [beten](../../strongs/h/h990.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 19](proverbs_19.md) - [Proverbs 21](proverbs_21.md)