# [Proverbs 3](https://www.blueletterbible.org/kjv/proverbs/3)

<a name="proverbs_3_1"></a>Proverbs 3:1

My [ben](../../strongs/h/h1121.md), [shakach](../../strongs/h/h7911.md) not my [towrah](../../strongs/h/h8451.md); but let thine [leb](../../strongs/h/h3820.md) [nāṣar](../../strongs/h/h5341.md) my [mitsvah](../../strongs/h/h4687.md):

<a name="proverbs_3_2"></a>Proverbs 3:2

For ['ōreḵ](../../strongs/h/h753.md) of [yowm](../../strongs/h/h3117.md), and [šānâ](../../strongs/h/h8141.md) [chay](../../strongs/h/h2416.md), and [shalowm](../../strongs/h/h7965.md), shall they [yāsap̄](../../strongs/h/h3254.md) to thee.

<a name="proverbs_3_3"></a>Proverbs 3:3

Let not [checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md) ['azab](../../strongs/h/h5800.md) thee: [qāšar](../../strongs/h/h7194.md) them about thy [gargᵊrôṯ](../../strongs/h/h1621.md); [kāṯaḇ](../../strongs/h/h3789.md) them upon the [lûaḥ](../../strongs/h/h3871.md) of thine [leb](../../strongs/h/h3820.md):

<a name="proverbs_3_4"></a>Proverbs 3:4

So shalt thou [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) and [towb](../../strongs/h/h2896.md) [śēḵel](../../strongs/h/h7922.md) in the ['ayin](../../strongs/h/h5869.md) of ['Elohiym](../../strongs/h/h430.md) and ['āḏām](../../strongs/h/h120.md).

<a name="proverbs_3_5"></a>Proverbs 3:5

[batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md) with all thine [leb](../../strongs/h/h3820.md); and [šāʿan](../../strongs/h/h8172.md) not unto thine own [bînâ](../../strongs/h/h998.md).

<a name="proverbs_3_6"></a>Proverbs 3:6

In all thy [derek](../../strongs/h/h1870.md) [yada'](../../strongs/h/h3045.md) him, and he shall [yashar](../../strongs/h/h3474.md) thy ['orach](../../strongs/h/h734.md).

<a name="proverbs_3_7"></a>Proverbs 3:7

Be not [ḥāḵām](../../strongs/h/h2450.md) in thine own ['ayin](../../strongs/h/h5869.md): [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md), and [cuwr](../../strongs/h/h5493.md) from [ra'](../../strongs/h/h7451.md).

<a name="proverbs_3_8"></a>Proverbs 3:8

It shall be [rip̄'ûṯ](../../strongs/h/h7500.md) to thy [šōr](../../strongs/h/h8270.md), and [šiqqûy](../../strongs/h/h8250.md) to thy ['etsem](../../strongs/h/h6106.md).

<a name="proverbs_3_9"></a>Proverbs 3:9

[kabad](../../strongs/h/h3513.md) [Yĕhovah](../../strongs/h/h3068.md) with thy [hôn](../../strongs/h/h1952.md), and with the [re'shiyth](../../strongs/h/h7225.md) of all thine [tᵊḇû'â](../../strongs/h/h8393.md):

<a name="proverbs_3_10"></a>Proverbs 3:10

So shall thy ['āsām](../../strongs/h/h618.md) be [mālā'](../../strongs/h/h4390.md) with [śāḇāʿ](../../strongs/h/h7647.md), and thy [yeqeḇ](../../strongs/h/h3342.md) shall [pāraṣ](../../strongs/h/h6555.md) with [tiyrowsh](../../strongs/h/h8492.md).

<a name="proverbs_3_11"></a>Proverbs 3:11

My [ben](../../strongs/h/h1121.md), [mā'as](../../strongs/h/h3988.md) not the [mûsār](../../strongs/h/h4148.md) of [Yĕhovah](../../strongs/h/h3068.md); neither be [qûṣ](../../strongs/h/h6973.md) of his [tôḵēḥâ](../../strongs/h/h8433.md):

<a name="proverbs_3_12"></a>Proverbs 3:12

For whom [Yĕhovah](../../strongs/h/h3068.md) ['ahab](../../strongs/h/h157.md) he [yakach](../../strongs/h/h3198.md); even as an ['ab](../../strongs/h/h1.md) the [ben](../../strongs/h/h1121.md) in whom he [ratsah](../../strongs/h/h7521.md).

<a name="proverbs_3_13"></a>Proverbs 3:13

['esher](../../strongs/h/h835.md) is the ['āḏām](../../strongs/h/h120.md) that [māṣā'](../../strongs/h/h4672.md) [ḥāḵmâ](../../strongs/h/h2451.md), and the ['āḏām](../../strongs/h/h120.md) that [pûq](../../strongs/h/h6329.md) [tāḇûn](../../strongs/h/h8394.md).

<a name="proverbs_3_14"></a>Proverbs 3:14

For the [saḥar](../../strongs/h/h5504.md) of it is [towb](../../strongs/h/h2896.md) than the [sāḥār](../../strongs/h/h5505.md) of [keceph](../../strongs/h/h3701.md), and the [tᵊḇû'â](../../strongs/h/h8393.md) thereof than fine [ḥārûṣ](../../strongs/h/h2742.md).

<a name="proverbs_3_15"></a>Proverbs 3:15

She is more [yāqār](../../strongs/h/h3368.md) than [p̄nyym](../../strongs/h/h6443.md): and all the things thou canst [chephets](../../strongs/h/h2656.md) are not to be [šāvâ](../../strongs/h/h7737.md) unto her.

<a name="proverbs_3_16"></a>Proverbs 3:16

['ōreḵ](../../strongs/h/h753.md) of [yowm](../../strongs/h/h3117.md) is in her [yamiyn](../../strongs/h/h3225.md); and in her [śᵊmō'l](../../strongs/h/h8040.md) [ʿōšer](../../strongs/h/h6239.md) and [kabowd](../../strongs/h/h3519.md).

<a name="proverbs_3_17"></a>Proverbs 3:17

Her [derek](../../strongs/h/h1870.md) are [derek](../../strongs/h/h1870.md) of [nōʿam](../../strongs/h/h5278.md), and all her [nāṯîḇ](../../strongs/h/h5410.md) are [shalowm](../../strongs/h/h7965.md).

<a name="proverbs_3_18"></a>Proverbs 3:18

She is an ['ets](../../strongs/h/h6086.md) of [chay](../../strongs/h/h2416.md) to them that [ḥāzaq](../../strongs/h/h2388.md) upon her: and ['āšar](../../strongs/h/h833.md) is every one that [tamak](../../strongs/h/h8551.md) her.

<a name="proverbs_3_19"></a>Proverbs 3:19

[Yĕhovah](../../strongs/h/h3068.md) by [ḥāḵmâ](../../strongs/h/h2451.md) hath [yacad](../../strongs/h/h3245.md) the ['erets](../../strongs/h/h776.md); by [tāḇûn](../../strongs/h/h8394.md) hath he [kuwn](../../strongs/h/h3559.md) the [shamayim](../../strongs/h/h8064.md).

<a name="proverbs_3_20"></a>Proverbs 3:20

By his [da'ath](../../strongs/h/h1847.md) the [tĕhowm](../../strongs/h/h8415.md) are [bāqaʿ](../../strongs/h/h1234.md), and the [shachaq](../../strongs/h/h7834.md) [rāʿap̄](../../strongs/h/h7491.md) the [ṭal](../../strongs/h/h2919.md).

<a name="proverbs_3_21"></a>Proverbs 3:21

My [ben](../../strongs/h/h1121.md), let not them [Lûz](../../strongs/h/h3868.md) from thine ['ayin](../../strongs/h/h5869.md): [nāṣar](../../strongs/h/h5341.md) [tûšîyâ](../../strongs/h/h8454.md) and [mezimmah](../../strongs/h/h4209.md):

<a name="proverbs_3_22"></a>Proverbs 3:22

So shall they be [chay](../../strongs/h/h2416.md) unto thy [nephesh](../../strongs/h/h5315.md), and [ḥēn](../../strongs/h/h2580.md) to thy [gargᵊrôṯ](../../strongs/h/h1621.md).

<a name="proverbs_3_23"></a>Proverbs 3:23

Then shalt thou [yālaḵ](../../strongs/h/h3212.md) in thy [derek](../../strongs/h/h1870.md) [betach](../../strongs/h/h983.md), and thy [regel](../../strongs/h/h7272.md) shall not [nāḡap̄](../../strongs/h/h5062.md).

<a name="proverbs_3_24"></a>Proverbs 3:24

When thou liest [shakab](../../strongs/h/h7901.md), thou shalt not be [pachad](../../strongs/h/h6342.md): yea, thou shalt [shakab](../../strongs/h/h7901.md), and thy [šēnā'](../../strongs/h/h8142.md) shall be [ʿāraḇ](../../strongs/h/h6149.md).

<a name="proverbs_3_25"></a>Proverbs 3:25

Be not [yare'](../../strongs/h/h3372.md) of [piṯ'ōm](../../strongs/h/h6597.md) [paḥaḏ](../../strongs/h/h6343.md), neither of the [šô'](../../strongs/h/h7722.md) of the [rasha'](../../strongs/h/h7563.md), when it [bow'](../../strongs/h/h935.md).

<a name="proverbs_3_26"></a>Proverbs 3:26

For [Yĕhovah](../../strongs/h/h3068.md) shall be thy [kesel](../../strongs/h/h3689.md), and shall [shamar](../../strongs/h/h8104.md) thy [regel](../../strongs/h/h7272.md) from being [leḵeḏ](../../strongs/h/h3921.md).

<a name="proverbs_3_27"></a>Proverbs 3:27

[mānaʿ](../../strongs/h/h4513.md) not [towb](../../strongs/h/h2896.md) from them to whom it is [baʿal](../../strongs/h/h1167.md), when it is in the ['el](../../strongs/h/h410.md) of thine [yad](../../strongs/h/h3027.md) to ['asah](../../strongs/h/h6213.md) it.

<a name="proverbs_3_28"></a>Proverbs 3:28

['āmar](../../strongs/h/h559.md) not unto thy [rea'](../../strongs/h/h7453.md), [yālaḵ](../../strongs/h/h3212.md), and come [shuwb](../../strongs/h/h7725.md), and to [māḥār](../../strongs/h/h4279.md) I will [nathan](../../strongs/h/h5414.md); when thou [yēš](../../strongs/h/h3426.md) it by thee.

<a name="proverbs_3_29"></a>Proverbs 3:29

[ḥāraš](../../strongs/h/h2790.md) not [ra'](../../strongs/h/h7451.md) against thy [rea'](../../strongs/h/h7453.md), seeing he [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md) by thee.

<a name="proverbs_3_30"></a>Proverbs 3:30

[riyb](../../strongs/h/h7378.md) not with an ['āḏām](../../strongs/h/h120.md) without [ḥinnām](../../strongs/h/h2600.md), if he have [gamal](../../strongs/h/h1580.md) thee no [ra'](../../strongs/h/h7451.md).

<a name="proverbs_3_31"></a>Proverbs 3:31

[qānā'](../../strongs/h/h7065.md) thou not the ['iysh](../../strongs/h/h376.md) [chamac](../../strongs/h/h2555.md), and [bāḥar](../../strongs/h/h977.md) none of his [derek](../../strongs/h/h1870.md).

<a name="proverbs_3_32"></a>Proverbs 3:32

For the [Lûz](../../strongs/h/h3868.md) is [tôʿēḇâ](../../strongs/h/h8441.md) to [Yĕhovah](../../strongs/h/h3068.md): but his [sôḏ](../../strongs/h/h5475.md) is with the [yashar](../../strongs/h/h3477.md).

<a name="proverbs_3_33"></a>Proverbs 3:33

The [mᵊ'ērâ](../../strongs/h/h3994.md) of [Yĕhovah](../../strongs/h/h3068.md) is in the [bayith](../../strongs/h/h1004.md) of the [rasha'](../../strongs/h/h7563.md): but he [barak](../../strongs/h/h1288.md) the [nāvê](../../strongs/h/h5116.md) of the [tsaddiyq](../../strongs/h/h6662.md).

<a name="proverbs_3_34"></a>Proverbs 3:34

Surely he [luwts](../../strongs/h/h3887.md) the [luwts](../../strongs/h/h3887.md): but he [nathan](../../strongs/h/h5414.md) [ḥēn](../../strongs/h/h2580.md) unto the ['anav](../../strongs/h/h6035.md) ['aniy](../../strongs/h/h6041.md).

<a name="proverbs_3_35"></a>Proverbs 3:35

The [ḥāḵām](../../strongs/h/h2450.md) shall [nāḥal](../../strongs/h/h5157.md) [kabowd](../../strongs/h/h3519.md): but [qālôn](../../strongs/h/h7036.md) shall be the [ruwm](../../strongs/h/h7311.md) of [kᵊsîl](../../strongs/h/h3684.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 2](proverbs_2.md) - [Proverbs 4](proverbs_4.md)