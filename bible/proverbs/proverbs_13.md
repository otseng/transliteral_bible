# [Proverbs 13](https://www.blueletterbible.org/kjv/proverbs/13)

<a name="proverbs_13_1"></a>Proverbs 13:1

A [ḥāḵām](../../strongs/h/h2450.md) [ben](../../strongs/h/h1121.md) heareth his ['ab](../../strongs/h/h1.md) [mûsār](../../strongs/h/h4148.md): but a [luwts](../../strongs/h/h3887.md) [shama'](../../strongs/h/h8085.md) not [ge'arah](../../strongs/h/h1606.md).

<a name="proverbs_13_2"></a>Proverbs 13:2

An ['iysh](../../strongs/h/h376.md) shall ['akal](../../strongs/h/h398.md) [towb](../../strongs/h/h2896.md) by the [pĕriy](../../strongs/h/h6529.md) of his [peh](../../strongs/h/h6310.md): but the [nephesh](../../strongs/h/h5315.md) of the [bāḡaḏ](../../strongs/h/h898.md) shall eat [chamac](../../strongs/h/h2555.md).

<a name="proverbs_13_3"></a>Proverbs 13:3

He that [nāṣar](../../strongs/h/h5341.md) his [peh](../../strongs/h/h6310.md) [shamar](../../strongs/h/h8104.md) his [nephesh](../../strongs/h/h5315.md): but he that [pāśaq](../../strongs/h/h6589.md) his [saphah](../../strongs/h/h8193.md) shall have [mᵊḥitâ](../../strongs/h/h4288.md).

<a name="proverbs_13_4"></a>Proverbs 13:4

The [nephesh](../../strongs/h/h5315.md) of the [ʿāṣēl](../../strongs/h/h6102.md) ['āvâ](../../strongs/h/h183.md), and hath nothing: but the [nephesh](../../strongs/h/h5315.md) of the [ḥārûṣ](../../strongs/h/h2742.md) shall be made [dāšēn](../../strongs/h/h1878.md).

<a name="proverbs_13_5"></a>Proverbs 13:5

A [tsaddiyq](../../strongs/h/h6662.md) man [sane'](../../strongs/h/h8130.md) [dabar](../../strongs/h/h1697.md) [sheqer](../../strongs/h/h8267.md): but a [rasha'](../../strongs/h/h7563.md) man is [bā'aš](../../strongs/h/h887.md), and cometh to [ḥāp̄ēr](../../strongs/h/h2659.md).

<a name="proverbs_13_6"></a>Proverbs 13:6

[tsedaqah](../../strongs/h/h6666.md) [nāṣar](../../strongs/h/h5341.md) him that is [tom](../../strongs/h/h8537.md) in the [derek](../../strongs/h/h1870.md): but [rišʿâ](../../strongs/h/h7564.md) [sālap̄](../../strongs/h/h5557.md) the [chatta'ath](../../strongs/h/h2403.md).

<a name="proverbs_13_7"></a>Proverbs 13:7

There [yēš](../../strongs/h/h3426.md) that maketh himself [ʿāšar](../../strongs/h/h6238.md), yet hath nothing: there is that maketh himself [rûš](../../strongs/h/h7326.md), [kōl](../../strongs/h/h3605.md) hath [rab](../../strongs/h/h7227.md) [hôn](../../strongs/h/h1952.md).

<a name="proverbs_13_8"></a>Proverbs 13:8

The [kōp̄er](../../strongs/h/h3724.md) of an ['iysh](../../strongs/h/h376.md) [nephesh](../../strongs/h/h5315.md) are his [ʿōšer](../../strongs/h/h6239.md): but the [rûš](../../strongs/h/h7326.md) [shama'](../../strongs/h/h8085.md) not [ge'arah](../../strongs/h/h1606.md).

<a name="proverbs_13_9"></a>Proverbs 13:9

The ['owr](../../strongs/h/h216.md) of the [tsaddiyq](../../strongs/h/h6662.md) [samach](../../strongs/h/h8055.md): but the [nîr](../../strongs/h/h5216.md) of the [rasha'](../../strongs/h/h7563.md) shall be [dāʿaḵ](../../strongs/h/h1846.md).

<a name="proverbs_13_10"></a>Proverbs 13:10

Only by [zāḏôn](../../strongs/h/h2087.md) [nathan](../../strongs/h/h5414.md) [maṣṣâ](../../strongs/h/h4683.md): but with the [ya'ats](../../strongs/h/h3289.md) is [ḥāḵmâ](../../strongs/h/h2451.md).

<a name="proverbs_13_11"></a>Proverbs 13:11

[hôn](../../strongs/h/h1952.md) gotten by [heḇel](../../strongs/h/h1892.md) shall be [māʿaṭ](../../strongs/h/h4591.md): but he that [qāḇaṣ](../../strongs/h/h6908.md) by [yad](../../strongs/h/h3027.md) shall [rabah](../../strongs/h/h7235.md).

<a name="proverbs_13_12"></a>Proverbs 13:12

[tôḥeleṯ](../../strongs/h/h8431.md) [mashak](../../strongs/h/h4900.md) maketh the [leb](../../strongs/h/h3820.md) [ḥālâ](../../strongs/h/h2470.md): but when the [ta'avah](../../strongs/h/h8378.md) [bow'](../../strongs/h/h935.md), it is an ['ets](../../strongs/h/h6086.md) of [chay](../../strongs/h/h2416.md).

<a name="proverbs_13_13"></a>Proverbs 13:13

Whoso [bûz](../../strongs/h/h936.md) the [dabar](../../strongs/h/h1697.md) shall be [chabal](../../strongs/h/h2254.md): but he that [yārē'](../../strongs/h/h3373.md) the [mitsvah](../../strongs/h/h4687.md) shall be [shalam](../../strongs/h/h7999.md).

<a name="proverbs_13_14"></a>Proverbs 13:14

The [towrah](../../strongs/h/h8451.md) of the [ḥāḵām](../../strongs/h/h2450.md) is a [māqôr](../../strongs/h/h4726.md) of [chay](../../strongs/h/h2416.md), to [cuwr](../../strongs/h/h5493.md) from the [mowqesh](../../strongs/h/h4170.md) of [maveth](../../strongs/h/h4194.md).

<a name="proverbs_13_15"></a>Proverbs 13:15

[towb](../../strongs/h/h2896.md) [śēḵel](../../strongs/h/h7922.md) [nathan](../../strongs/h/h5414.md) [ḥēn](../../strongs/h/h2580.md): but the [derek](../../strongs/h/h1870.md) of [bāḡaḏ](../../strongs/h/h898.md) is ['êṯān](../../strongs/h/h386.md).

<a name="proverbs_13_16"></a>Proverbs 13:16

Every ['aruwm](../../strongs/h/h6175.md) man ['asah](../../strongs/h/h6213.md) with [da'ath](../../strongs/h/h1847.md): but a [kᵊsîl](../../strongs/h/h3684.md) [pāraś](../../strongs/h/h6566.md) his ['iûeleṯ](../../strongs/h/h200.md).

<a name="proverbs_13_17"></a>Proverbs 13:17

A [rasha'](../../strongs/h/h7563.md) [mal'ak](../../strongs/h/h4397.md) [naphal](../../strongs/h/h5307.md) into [ra'](../../strongs/h/h7451.md): but an ['ēmûn](../../strongs/h/h529.md) [ṣîr](../../strongs/h/h6735.md) is [marpē'](../../strongs/h/h4832.md).

<a name="proverbs_13_18"></a>Proverbs 13:18

[rêš](../../strongs/h/h7389.md) and [qālôn](../../strongs/h/h7036.md) shall be to him that [pāraʿ](../../strongs/h/h6544.md) [mûsār](../../strongs/h/h4148.md): but he that [shamar](../../strongs/h/h8104.md) [tôḵēḥâ](../../strongs/h/h8433.md) shall be [kabad](../../strongs/h/h3513.md).

<a name="proverbs_13_19"></a>Proverbs 13:19

The [ta'avah](../../strongs/h/h8378.md) [hayah](../../strongs/h/h1961.md) is [ʿāraḇ](../../strongs/h/h6149.md) to the [nephesh](../../strongs/h/h5315.md): but it is [tôʿēḇâ](../../strongs/h/h8441.md) to [kᵊsîl](../../strongs/h/h3684.md) to [cuwr](../../strongs/h/h5493.md) from [ra'](../../strongs/h/h7451.md).

<a name="proverbs_13_20"></a>Proverbs 13:20

He that [halak](../../strongs/h/h1980.md) with [ḥāḵām](../../strongs/h/h2450.md) men shall be [ḥāḵam](../../strongs/h/h2449.md): but a [ra'ah](../../strongs/h/h7462.md) of [kᵊsîl](../../strongs/h/h3684.md) shall be [rûaʿ](../../strongs/h/h7321.md).

<a name="proverbs_13_21"></a>Proverbs 13:21

[ra'](../../strongs/h/h7451.md) [radaph](../../strongs/h/h7291.md) [chatta'](../../strongs/h/h2400.md): but to the [tsaddiyq](../../strongs/h/h6662.md) [towb](../../strongs/h/h2896.md) shall be [shalam](../../strongs/h/h7999.md).

<a name="proverbs_13_22"></a>Proverbs 13:22

A [towb](../../strongs/h/h2896.md) man leaveth a [nāḥal](../../strongs/h/h5157.md) to his [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md): and the [ḥayil](../../strongs/h/h2428.md) of the [chata'](../../strongs/h/h2398.md) is laid [tsaphan](../../strongs/h/h6845.md) for the [tsaddiyq](../../strongs/h/h6662.md).

<a name="proverbs_13_23"></a>Proverbs 13:23

[rōḇ](../../strongs/h/h7230.md) ['ōḵel](../../strongs/h/h400.md) is in the [nîr](../../strongs/h/h5215.md) of the [ro'sh](../../strongs/h/h7218.md) [rûš](../../strongs/h/h7326.md): but there [yēš](../../strongs/h/h3426.md) that is [sāp̄â](../../strongs/h/h5595.md) for [lō'](../../strongs/h/h3808.md) of [mishpat](../../strongs/h/h4941.md).

<a name="proverbs_13_24"></a>Proverbs 13:24

He that [ḥāśaḵ](../../strongs/h/h2820.md) his [shebet](../../strongs/h/h7626.md) [sane'](../../strongs/h/h8130.md) his [ben](../../strongs/h/h1121.md): but he that ['ahab](../../strongs/h/h157.md) him [mûsār](../../strongs/h/h4148.md) him [šāḥar](../../strongs/h/h7836.md).

<a name="proverbs_13_25"></a>Proverbs 13:25

The [tsaddiyq](../../strongs/h/h6662.md) ['akal](../../strongs/h/h398.md) to the [śōḇaʿ](../../strongs/h/h7648.md) of his [nephesh](../../strongs/h/h5315.md): but the [beten](../../strongs/h/h990.md) of the [rasha'](../../strongs/h/h7563.md) shall [ḥāsēr](../../strongs/h/h2637.md).

---

[Transliteral Bible](../bible.md)

[Proverbs](proverbs.md)

[Proverbs 12](proverbs_12.md) - [Proverbs 14](proverbs_14.md)