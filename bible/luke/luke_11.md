# [Luke 11](https://www.blueletterbible.org/kjv/luk/11/1/rl1/s_984001)

<a name="luke_11_1"></a>Luke 11:1

And [ginomai](../../strongs/g/g1096.md), that, as he was [proseuchomai](../../strongs/g/g4336.md) in a certain [topos](../../strongs/g/g5117.md), when he [pauō](../../strongs/g/g3973.md), one of his [mathētēs](../../strongs/g/g3101.md) [eipon](../../strongs/g/g2036.md) unto him, [kyrios](../../strongs/g/g2962.md), [didaskō](../../strongs/g/g1321.md) us to [proseuchomai](../../strongs/g/g4336.md), as [Iōannēs](../../strongs/g/g2491.md) also [didaskō](../../strongs/g/g1321.md) his [mathētēs](../../strongs/g/g3101.md).

<a name="luke_11_2"></a>Luke 11:2

And he [eipon](../../strongs/g/g2036.md) unto them, **When ye [proseuchomai](../../strongs/g/g4336.md), [legō](../../strongs/g/g3004.md), [hēmōn](../../strongs/g/g2257.md) [patēr](../../strongs/g/g3962.md) which art in [ouranos](../../strongs/g/g3772.md), [hagiazō](../../strongs/g/g37.md) thy [onoma](../../strongs/g/g3686.md). Thy [basileia](../../strongs/g/g932.md) [erchomai](../../strongs/g/g2064.md). Thy [thelēma](../../strongs/g/g2307.md) be [ginomai](../../strongs/g/g1096.md), as in [ouranos](../../strongs/g/g3772.md), so in [gē](../../strongs/g/g1093.md).** [^1]

<a name="luke_11_3"></a>Luke 11:3

**[didōmi](../../strongs/g/g1325.md) us [kata](../../strongs/g/g2596.md) [hēmera](../../strongs/g/g2250.md) our [epiousios](../../strongs/g/g1967.md) [artos](../../strongs/g/g740.md).**

<a name="luke_11_4"></a>Luke 11:4

**And [aphiēmi](../../strongs/g/g863.md) us our [hamartia](../../strongs/g/g266.md); for we also [aphiēmi](../../strongs/g/g863.md) every one that is [opheilō](../../strongs/g/g3784.md) to us. And [eispherō](../../strongs/g/g1533.md) us not into [peirasmos](../../strongs/g/g3986.md); but [rhyomai](../../strongs/g/g4506.md) us from [ponēros](../../strongs/g/g4190.md).**

<a name="luke_11_5"></a>Luke 11:5

And he [eipon](../../strongs/g/g2036.md) unto them, **Which of you shall have a [philos](../../strongs/g/g5384.md), and shall [poreuō](../../strongs/g/g4198.md) unto him at [mesonyktion](../../strongs/g/g3317.md), and [eipon](../../strongs/g/g2036.md) unto him, [philos](../../strongs/g/g5384.md), [kichrēmi](../../strongs/g/g5531.md) me three [artos](../../strongs/g/g740.md);**

<a name="luke_11_6"></a>Luke 11:6

**For a [philos](../../strongs/g/g5384.md) of mine in his [hodos](../../strongs/g/g3598.md) is [paraginomai](../../strongs/g/g3854.md) to me, and I have nothing to [paratithēmi](../../strongs/g/g3908.md) before him?**

<a name="luke_11_7"></a>Luke 11:7

**And he from [esōthen](../../strongs/g/g2081.md) shall [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), [kopos](../../strongs/g/g2873.md) [parechō](../../strongs/g/g3930.md) me not: the [thyra](../../strongs/g/g2374.md) is now [kleiō](../../strongs/g/g2808.md), and my [paidion](../../strongs/g/g3813.md) are with me in [koitē](../../strongs/g/g2845.md); I cannot [anistēmi](../../strongs/g/g450.md) and [didōmi](../../strongs/g/g1325.md) thee.**

<a name="luke_11_8"></a>Luke 11:8

**I [legō](../../strongs/g/g3004.md) unto you, Though he will not [egeirō](../../strongs/g/g1453.md) and [didōmi](../../strongs/g/g1325.md) him, because he is his [philos](../../strongs/g/g5384.md), yet because of his [anaideia](../../strongs/g/g335.md) he will [anistēmi](../../strongs/g/g450.md) and [didōmi](../../strongs/g/g1325.md) him as many as he [chrēzō](../../strongs/g/g5535.md).**

<a name="luke_11_9"></a>Luke 11:9

**And I [legō](../../strongs/g/g3004.md) unto you, [aiteō](../../strongs/g/g154.md), and it shall be [didōmi](../../strongs/g/g1325.md) you; [zēteō](../../strongs/g/g2212.md), and ye shall [heuriskō](../../strongs/g/g2147.md); [krouō](../../strongs/g/g2925.md), and it shall be [anoigō](../../strongs/g/g455.md) unto you.**

<a name="luke_11_10"></a>Luke 11:10

**For every one that [aiteō](../../strongs/g/g154.md) [lambanō](../../strongs/g/g2983.md); and he that [zēteō](../../strongs/g/g2212.md) [heuriskō](../../strongs/g/g2147.md); and to him that [krouō](../../strongs/g/g2925.md) it shall be [anoigō](../../strongs/g/g455.md).**

<a name="luke_11_11"></a>Luke 11:11

**If a [huios](../../strongs/g/g5207.md) shall [aiteō](../../strongs/g/g154.md) [artos](../../strongs/g/g740.md) of any of you that is a [patēr](../../strongs/g/g3962.md), will he [epididōmi](../../strongs/g/g1929.md) him a [lithos](../../strongs/g/g3037.md)? or if an [ichthys](../../strongs/g/g2486.md), will he for an [ichthys](../../strongs/g/g2486.md) [epididōmi](../../strongs/g/g1929.md) him a [ophis](../../strongs/g/g3789.md)?** [^2]

<a name="luke_11_12"></a>Luke 11:12

**Or if he shall [aiteō](../../strongs/g/g154.md) an [ōon](../../strongs/g/g5609.md), will he [epididōmi](../../strongs/g/g1929.md) him a [skorpios](../../strongs/g/g4651.md)?**

<a name="luke_11_13"></a>Luke 11:13

**If ye then, being [ponēros](../../strongs/g/g4190.md), [eidō](../../strongs/g/g1492.md) how to [didōmi](../../strongs/g/g1325.md) [agathos](../../strongs/g/g18.md) [doma](../../strongs/g/g1390.md) unto your [teknon](../../strongs/g/g5043.md): how much more shall [ouranos](../../strongs/g/g3772.md) [patēr](../../strongs/g/g3962.md) [didōmi](../../strongs/g/g1325.md) the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) to them that [aiteō](../../strongs/g/g154.md) him?**

<a name="luke_11_14"></a>Luke 11:14

And he was [ekballō](../../strongs/g/g1544.md) a [daimonion](../../strongs/g/g1140.md), and it was [kōphos](../../strongs/g/g2974.md). And [ginomai](../../strongs/g/g1096.md), when the [daimonion](../../strongs/g/g1140.md) was [exerchomai](../../strongs/g/g1831.md), the [kōphos](../../strongs/g/g2974.md) [laleō](../../strongs/g/g2980.md); and the [ochlos](../../strongs/g/g3793.md) [thaumazō](../../strongs/g/g2296.md).

<a name="luke_11_15"></a>Luke 11:15

But some of them [eipon](../../strongs/g/g2036.md), He [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md) through [Beelzeboul](../../strongs/g/g954.md) the [archōn](../../strongs/g/g758.md) of the [daimonion](../../strongs/g/g1140.md).

<a name="luke_11_16"></a>Luke 11:16

And others, [peirazō](../../strongs/g/g3985.md), [zēteō](../../strongs/g/g2212.md) of him a [sēmeion](../../strongs/g/g4592.md) from [ouranos](../../strongs/g/g3772.md).

<a name="luke_11_17"></a>Luke 11:17

But he, [eidō](../../strongs/g/g1492.md) their [dianoēma](../../strongs/g/g1270.md), [eipon](../../strongs/g/g2036.md) unto them, **Every [basileia](../../strongs/g/g932.md) [diamerizō](../../strongs/g/g1266.md) against itself is [erēmoō](../../strongs/g/g2049.md); and a [oikos](../../strongs/g/g3624.md) against a [oikos](../../strongs/g/g3624.md) [piptō](../../strongs/g/g4098.md).**

<a name="luke_11_18"></a>Luke 11:18

**If [Satanas](../../strongs/g/g4567.md) also be [diamerizō](../../strongs/g/g1266.md) against [heautou](../../strongs/g/g1438.md), how shall his [basileia](../../strongs/g/g932.md) [histēmi](../../strongs/g/g2476.md)? because ye [legō](../../strongs/g/g3004.md) that I [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md) through [Beelzeboul](../../strongs/g/g954.md).**

<a name="luke_11_19"></a>Luke 11:19

**And if I by [Beelzeboul](../../strongs/g/g954.md) [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md), by whom do your [huios](../../strongs/g/g5207.md) [ekballō](../../strongs/g/g1544.md)? therefore shall they be your [kritēs](../../strongs/g/g2923.md).**

<a name="luke_11_20"></a>Luke 11:20

**But if I with the [daktylos](../../strongs/g/g1147.md) of [theos](../../strongs/g/g2316.md) [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md), no doubt the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) is [phthanō](../../strongs/g/g5348.md) upon you.**

<a name="luke_11_21"></a>Luke 11:21

**When [ischyros](../../strongs/g/g2478.md) [kathoplizō](../../strongs/g/g2528.md) [phylassō](../../strongs/g/g5442.md) [aulē](../../strongs/g/g833.md) [heautou](../../strongs/g/g1438.md), his [hyparchonta](../../strongs/g/g5224.md) are in [eirēnē](../../strongs/g/g1515.md):**

<a name="luke_11_22"></a>Luke 11:22

**But when [ischyros](../../strongs/g/g2478.md) than he shall [eperchomai](../../strongs/g/g1904.md) him, and [nikaō](../../strongs/g/g3528.md) him, he [airō](../../strongs/g/g142.md) from him all his [panoplia](../../strongs/g/g3833.md) wherein he [peithō](../../strongs/g/g3982.md), and [diadidōmi](../../strongs/g/g1239.md) his [skylon](../../strongs/g/g4661.md).**

<a name="luke_11_23"></a>Luke 11:23

**He that is not with me is against me: and he that [synagō](../../strongs/g/g4863.md) not with me [skorpizō](../../strongs/g/g4650.md).**

<a name="luke_11_24"></a>Luke 11:24

**When the [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md) is [exerchomai](../../strongs/g/g1831.md) out of an [anthrōpos](../../strongs/g/g444.md), he [dierchomai](../../strongs/g/g1330.md) through [anydros](../../strongs/g/g504.md) [topos](../../strongs/g/g5117.md), [zēteō](../../strongs/g/g2212.md) [anapausis](../../strongs/g/g372.md); and [heuriskō](../../strongs/g/g2147.md) none, he [legō](../../strongs/g/g3004.md), I will [hypostrephō](../../strongs/g/g5290.md) unto my [oikos](../../strongs/g/g3624.md) whence I [exerchomai](../../strongs/g/g1831.md).**

<a name="luke_11_25"></a>Luke 11:25

**And when he [erchomai](../../strongs/g/g2064.md), he [heuriskō](../../strongs/g/g2147.md) [saroō](../../strongs/g/g4563.md) and [kosmeō](../../strongs/g/g2885.md).**

<a name="luke_11_26"></a>Luke 11:26

**Then [poreuō](../../strongs/g/g4198.md) he, and [paralambanō](../../strongs/g/g3880.md) seven other [pneuma](../../strongs/g/g4151.md) [ponēroteros](../../strongs/g/g4191.md) than himself; and they [eiserchomai](../../strongs/g/g1525.md), and [katoikeō](../../strongs/g/g2730.md) there: and the [eschatos](../../strongs/g/g2078.md) of that [anthrōpos](../../strongs/g/g444.md) is [cheirōn](../../strongs/g/g5501.md) than the first.**

<a name="luke_11_27"></a>Luke 11:27

And [ginomai](../../strongs/g/g1096.md), as he [legō](../../strongs/g/g3004.md) these things, a certain [gynē](../../strongs/g/g1135.md) of [ochlos](../../strongs/g/g3793.md) [epairō](../../strongs/g/g1869.md) her [phōnē](../../strongs/g/g5456.md), and [eipon](../../strongs/g/g2036.md) unto him, [makarios](../../strongs/g/g3107.md) the [koilia](../../strongs/g/g2836.md) that [bastazō](../../strongs/g/g941.md) thee, and the [mastos](../../strongs/g/g3149.md) which thou hast [thēlazō](../../strongs/g/g2337.md).

<a name="luke_11_28"></a>Luke 11:28

But he [eipon](../../strongs/g/g2036.md), **[menoun](../../strongs/g/g3304.md), [makarios](../../strongs/g/g3107.md) they that [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md), and [phylassō](../../strongs/g/g5442.md) it.**

<a name="luke_11_29"></a>Luke 11:29

And when the [ochlos](../../strongs/g/g3793.md) were [epathroizō](../../strongs/g/g1865.md), he [archomai](../../strongs/g/g756.md) to [legō](../../strongs/g/g3004.md), **This is a [ponēros](../../strongs/g/g4190.md) [genea](../../strongs/g/g1074.md): they [epizēteō](../../strongs/g/g1934.md) a [sēmeion](../../strongs/g/g4592.md); and there shall no [sēmeion](../../strongs/g/g4592.md) be [didōmi](../../strongs/g/g1325.md) it, but the [sēmeion](../../strongs/g/g4592.md) of [Iōnas](../../strongs/g/g2495.md) the [prophētēs](../../strongs/g/g4396.md).**

<a name="luke_11_30"></a>Luke 11:30

**For as [Iōnas](../../strongs/g/g2495.md) was a [sēmeion](../../strongs/g/g4592.md) unto the [Nineuitēs](../../strongs/g/g3536.md), so shall also the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) be to this [genea](../../strongs/g/g1074.md).**

<a name="luke_11_31"></a>Luke 11:31

**The [basilissa](../../strongs/g/g938.md) of the [notos](../../strongs/g/g3558.md) shall [egeirō](../../strongs/g/g1453.md) in the [krisis](../../strongs/g/g2920.md) with the [anēr](../../strongs/g/g435.md) of this [genea](../../strongs/g/g1074.md), and [katakrinō](../../strongs/g/g2632.md) them: for she [erchomai](../../strongs/g/g2064.md) from the [peras](../../strongs/g/g4009.md) of [gē](../../strongs/g/g1093.md) to [akouō](../../strongs/g/g191.md) the [sophia](../../strongs/g/g4678.md) of [Solomōn](../../strongs/g/g4672.md); and, [idou](../../strongs/g/g2400.md), a [pleiōn](../../strongs/g/g4119.md) than [Solomōn](../../strongs/g/g4672.md) is here.**

<a name="luke_11_32"></a>Luke 11:32

**The [anēr](../../strongs/g/g435.md) of [Nineuē](../../strongs/g/g3535.md) shall [anistēmi](../../strongs/g/g450.md) in the [krisis](../../strongs/g/g2920.md) with this [genea](../../strongs/g/g1074.md), and shall [katakrinō](../../strongs/g/g2632.md) it: for they [metanoeō](../../strongs/g/g3340.md) at the [kērygma](../../strongs/g/g2782.md) of [Iōnas](../../strongs/g/g2495.md); and, [idou](../../strongs/g/g2400.md), a [pleiōn](../../strongs/g/g4119.md) than [Iōnas](../../strongs/g/g2495.md) is here.**

<a name="luke_11_33"></a>Luke 11:33

**[oudeis](../../strongs/g/g3762.md), when he hath [haptō](../../strongs/g/g681.md) a [lychnos](../../strongs/g/g3088.md), [tithēmi](../../strongs/g/g5087.md) in a [kryptē](../../strongs/g/g2926.md), neither under a [modios](../../strongs/g/g3426.md), but on a [lychnia](../../strongs/g/g3087.md), that they which [eisporeuomai](../../strongs/g/g1531.md) may [blepō](../../strongs/g/g991.md) the [pheggos](../../strongs/g/g5338.md).**

<a name="luke_11_34"></a>Luke 11:34

**The [lychnos](../../strongs/g/g3088.md) of the [sōma](../../strongs/g/g4983.md) is the [ophthalmos](../../strongs/g/g3788.md): therefore when thine [ophthalmos](../../strongs/g/g3788.md) is [haplous](../../strongs/g/g573.md), thy [holos](../../strongs/g/g3650.md) [sōma](../../strongs/g/g4983.md) also is [phōteinos](../../strongs/g/g5460.md); but when is [ponēros](../../strongs/g/g4190.md), thy [sōma](../../strongs/g/g4983.md) also [skoteinos](../../strongs/g/g4652.md).**

<a name="luke_11_35"></a>Luke 11:35

**[skopeō](../../strongs/g/g4648.md) therefore that the [phōs](../../strongs/g/g5457.md) which is in thee be not [skotos](../../strongs/g/g4655.md).**

<a name="luke_11_36"></a>Luke 11:36

**If thy [holos](../../strongs/g/g3650.md) [sōma](../../strongs/g/g4983.md) therefore [phōteinos](../../strongs/g/g5460.md), having no [meros](../../strongs/g/g3313.md) [skoteinos](../../strongs/g/g4652.md), the [holos](../../strongs/g/g3650.md) shall be [phōteinos](../../strongs/g/g5460.md), as when the [astrapē](../../strongs/g/g796.md) of a [lychnos](../../strongs/g/g3088.md) doth [phōtizō](../../strongs/g/g5461.md) thee.**

<a name="luke_11_37"></a>Luke 11:37

And as he [laleō](../../strongs/g/g2980.md), a certain [Pharisaios](../../strongs/g/g5330.md) [erōtaō](../../strongs/g/g2065.md) him to [aristaō](../../strongs/g/g709.md) with him: and he [eiserchomai](../../strongs/g/g1525.md), and [anapiptō](../../strongs/g/g377.md).

<a name="luke_11_38"></a>Luke 11:38

And when the [Pharisaios](../../strongs/g/g5330.md) [eidō](../../strongs/g/g1492.md), [thaumazō](../../strongs/g/g2296.md) that he had not first [baptizō](../../strongs/g/g907.md) before [ariston](../../strongs/g/g712.md).

<a name="luke_11_39"></a>Luke 11:39

And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md) unto him, **Now do ye [Pharisaios](../../strongs/g/g5330.md) [katharizō](../../strongs/g/g2511.md) the [exōthen](../../strongs/g/g1855.md) of the [potērion](../../strongs/g/g4221.md) and the [pinax](../../strongs/g/g4094.md); but your [esōthen](../../strongs/g/g2081.md) is [gemō](../../strongs/g/g1073.md) of [harpagē](../../strongs/g/g724.md) and [ponēria](../../strongs/g/g4189.md).**

<a name="luke_11_40"></a>Luke 11:40

**[aphrōn](../../strongs/g/g878.md), did not he that [poieō](../../strongs/g/g4160.md) that which is [exōthen](../../strongs/g/g1855.md) [poieō](../../strongs/g/g4160.md) that which is [esōthen](../../strongs/g/g2081.md) also?**

<a name="luke_11_41"></a>Luke 11:41

**But rather [didōmi](../../strongs/g/g1325.md) [eleēmosynē](../../strongs/g/g1654.md) of such things as ye [eneimi](../../strongs/g/g1751.md); and, [idou](../../strongs/g/g2400.md), all things are [katharos](../../strongs/g/g2513.md) unto you.**

<a name="luke_11_42"></a>Luke 11:42

**But [ouai](../../strongs/g/g3759.md) unto you, [Pharisaios](../../strongs/g/g5330.md)! for ye [apodekatoō](../../strongs/g/g586.md) [hēdyosmon](../../strongs/g/g2238.md) and [pēganon](../../strongs/g/g4076.md) and all manner of [lachanon](../../strongs/g/g3001.md), and [parerchomai](../../strongs/g/g3928.md) [krisis](../../strongs/g/g2920.md) and the [agapē](../../strongs/g/g26.md) of [theos](../../strongs/g/g2316.md): these ought ye to have [poieō](../../strongs/g/g4160.md), and not to [aphiēmi](../../strongs/g/g863.md) the other.**

<a name="luke_11_43"></a>Luke 11:43

**[ouai](../../strongs/g/g3759.md) unto you, [Pharisaios](../../strongs/g/g5330.md)! for ye [agapaō](../../strongs/g/g25.md) the [prōtokathedria](../../strongs/g/g4410.md) in the [synagōgē](../../strongs/g/g4864.md), and [aspasmos](../../strongs/g/g783.md) in the [agora](../../strongs/g/g58.md).**

<a name="luke_11_44"></a>Luke 11:44

**[ouai](../../strongs/g/g3759.md) unto you, [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md), [hypokritēs](../../strongs/g/g5273.md)! for ye are as [mnēmeion](../../strongs/g/g3419.md) which [adēlos](../../strongs/g/g82.md), and the [anthrōpos](../../strongs/g/g444.md) that [peripateō](../../strongs/g/g4043.md) over are not [eidō](../../strongs/g/g1492.md).**

<a name="luke_11_45"></a>Luke 11:45

Then [apokrinomai](../../strongs/g/g611.md) one of the [nomikos](../../strongs/g/g3544.md), and [legō](../../strongs/g/g3004.md) unto him, [didaskalos](../../strongs/g/g1320.md), thus [legō](../../strongs/g/g3004.md) thou [hybrizō](../../strongs/g/g5195.md) us also.

<a name="luke_11_46"></a>Luke 11:46

And he [eipon](../../strongs/g/g2036.md), **[ouai](../../strongs/g/g3759.md) unto you also, ye [nomikos](../../strongs/g/g3544.md)! for ye [phortizō](../../strongs/g/g5412.md) [anthrōpos](../../strongs/g/g444.md) with [phortion](../../strongs/g/g5413.md) [dysbastaktos](../../strongs/g/g1419.md), and ye yourselves [prospsauō](../../strongs/g/g4379.md) not the [phortion](../../strongs/g/g5413.md) with one of your [daktylos](../../strongs/g/g1147.md).**

<a name="luke_11_47"></a>Luke 11:47

**[ouai](../../strongs/g/g3759.md) unto you! for ye [oikodomeō](../../strongs/g/g3618.md) the [mnēmeion](../../strongs/g/g3419.md) of the [prophētēs](../../strongs/g/g4396.md), and your [patēr](../../strongs/g/g3962.md) [apokteinō](../../strongs/g/g615.md) them.**

<a name="luke_11_48"></a>Luke 11:48

**[ara](../../strongs/g/g686.md) ye [martyreō](../../strongs/g/g3140.md) that ye [syneudokeō](../../strongs/g/g4909.md) the [ergon](../../strongs/g/g2041.md) of your [patēr](../../strongs/g/g3962.md): for they indeed [apokteinō](../../strongs/g/g615.md) them, and ye [oikodomeō](../../strongs/g/g3618.md) their [mnēmeion](../../strongs/g/g3419.md).**

<a name="luke_11_49"></a>Luke 11:49

**Therefore also [eipon](../../strongs/g/g2036.md) the [sophia](../../strongs/g/g4678.md) of [theos](../../strongs/g/g2316.md), I will [apostellō](../../strongs/g/g649.md) them [prophētēs](../../strongs/g/g4396.md) and [apostolos](../../strongs/g/g652.md), and of them they shall [apokteinō](../../strongs/g/g615.md) and [ekdiōkō](../../strongs/g/g1559.md):**

<a name="luke_11_50"></a>Luke 11:50

**That the [haima](../../strongs/g/g129.md) of all the [prophētēs](../../strongs/g/g4396.md), which was [ekcheō](../../strongs/g/g1632.md) from the [katabolē](../../strongs/g/g2602.md) of the [kosmos](../../strongs/g/g2889.md), may be [ekzēteō](../../strongs/g/g1567.md) of this [genea](../../strongs/g/g1074.md);**

<a name="luke_11_51"></a>Luke 11:51

**From the [haima](../../strongs/g/g129.md) of [Abel](../../strongs/g/g6.md) unto the [haima](../../strongs/g/g129.md) of [Zacharias](../../strongs/g/g2197.md) which [apollymi](../../strongs/g/g622.md) between the [thysiastērion](../../strongs/g/g2379.md) and the [oikos](../../strongs/g/g3624.md): [nai](../../strongs/g/g3483.md) I [legō](../../strongs/g/g3004.md) unto you, It shall be [ekzēteō](../../strongs/g/g1567.md) of this [genea](../../strongs/g/g1074.md).**

<a name="luke_11_52"></a>Luke 11:52

**[ouai](../../strongs/g/g3759.md) unto you, [nomikos](../../strongs/g/g3544.md)! for ye have [airō](../../strongs/g/g142.md) the [kleis](../../strongs/g/g2807.md) of [gnōsis](../../strongs/g/g1108.md): ye [eiserchomai](../../strongs/g/g1525.md) not in yourselves, and them that were [eiserchomai](../../strongs/g/g1525.md) in ye [kōlyō](../../strongs/g/g2967.md).**

<a name="luke_11_53"></a>Luke 11:53

And as he [legō](../../strongs/g/g3004.md) these things unto them, the [grammateus](../../strongs/g/g1122.md) and the [Pharisaios](../../strongs/g/g5330.md) [archomai](../../strongs/g/g756.md) to [enechō](../../strongs/g/g1758.md) [deinōs](../../strongs/g/g1171.md), and to [apostomatizō](../../strongs/g/g653.md) him to [apostomatizō](../../strongs/g/g653.md) of [pleiōn](../../strongs/g/g4119.md):

<a name="luke_11_54"></a>Luke 11:54

[enedreuō](../../strongs/g/g1748.md) him, and [zēteō](../../strongs/g/g2212.md) to [thēreuō](../../strongs/g/g2340.md) something out of his [stoma](../../strongs/g/g4750.md), that they might [katēgoreō](../../strongs/g/g2723.md) him.

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 10](luke_10.md) - [Luke 12](luke_12.md)

---

[^1]: [Luke 11:2 Commentary](../../commentary/luke/luke_11_commentary.md#luke_11_2)

[^2]: [Luke 11:11 Commentary](../../commentary/luke/luke_11_commentary.md#luke_11_11)
