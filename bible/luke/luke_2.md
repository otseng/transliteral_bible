# [Luke 2](https://www.blueletterbible.org/kjv/luk/2/1/rl1/s_975001)

<a name="luke_2_1"></a>Luke 2:1

And [ginomai](../../strongs/g/g1096.md) in those [hēmera](../../strongs/g/g2250.md), that there [exerchomai](../../strongs/g/g1831.md) a [dogma](../../strongs/g/g1378.md) from [Kaisar](../../strongs/g/g2541.md) [augoustos](../../strongs/g/g828.md) that all the [oikoumenē](../../strongs/g/g3625.md) should be [apographō](../../strongs/g/g583.md).

<a name="luke_2_2"></a>Luke 2:2

(this [apographē](../../strongs/g/g582.md) was [prōtos](../../strongs/g/g4413.md) [ginomai](../../strongs/g/g1096.md) when [Kyrēnios](../../strongs/g/g2958.md) was [hēgemoneuō](../../strongs/g/g2230.md) of [Syria](../../strongs/g/g4947.md).)

<a name="luke_2_3"></a>Luke 2:3

And all [poreuō](../../strongs/g/g4198.md) to be [apographō](../../strongs/g/g583.md), every one into his own [polis](../../strongs/g/g4172.md).

<a name="luke_2_4"></a>Luke 2:4

And [Iōsēph](../../strongs/g/g2501.md) also [anabainō](../../strongs/g/g305.md) from [Galilaia](../../strongs/g/g1056.md), out of the [polis](../../strongs/g/g4172.md) of [Nazara](../../strongs/g/g3478.md), into [Ioudaia](../../strongs/g/g2449.md), unto the [polis](../../strongs/g/g4172.md) of [Dabid](../../strongs/g/g1138.md), which is [kaleō](../../strongs/g/g2564.md) [Bēthleem](../../strongs/g/g965.md); (because he was of the [oikos](../../strongs/g/g3624.md) and [patria](../../strongs/g/g3965.md) of [Dabid](../../strongs/g/g1138.md):)

<a name="luke_2_5"></a>Luke 2:5

To be [apographō](../../strongs/g/g583.md) with [Maria](../../strongs/g/g3137.md) his [mnēsteuō](../../strongs/g/g3423.md) [gynē](../../strongs/g/g1135.md), being [egkyos](../../strongs/g/g1471.md).

<a name="luke_2_6"></a>Luke 2:6

And so it was, that, while they were there, the [hēmera](../../strongs/g/g2250.md) were [pimplēmi](../../strongs/g/g4130.md) that she should be [tiktō](../../strongs/g/g5088.md).

<a name="luke_2_7"></a>Luke 2:7

And she [tiktō](../../strongs/g/g5088.md) her [prōtotokos](../../strongs/g/g4416.md) [huios](../../strongs/g/g5207.md), and [sparganoō](../../strongs/g/g4683.md) him, and [anaklinō](../../strongs/g/g347.md) him in a [phatnē](../../strongs/g/g5336.md); because there was no [topos](../../strongs/g/g5117.md) for them in the [katalyma](../../strongs/g/g2646.md).

<a name="luke_2_8"></a>Luke 2:8

And there were in the same [chōra](../../strongs/g/g5561.md) [poimēn](../../strongs/g/g4166.md) [agrauleō](../../strongs/g/g63.md), [phylassō](../../strongs/g/g5442.md) [phylakē](../../strongs/g/g5438.md) over their [poimnē](../../strongs/g/g4167.md) by [nyx](../../strongs/g/g3571.md).

<a name="luke_2_9"></a>Luke 2:9

And, [idou](../../strongs/g/g2400.md), the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) [ephistēmi](../../strongs/g/g2186.md) them, and the [doxa](../../strongs/g/g1391.md) of the [kyrios](../../strongs/g/g2962.md) [perilampō](../../strongs/g/g4034.md) them: and they were [megas](../../strongs/g/g3173.md) [phobeō](../../strongs/g/g5399.md) [phobos](../../strongs/g/g5401.md).

<a name="luke_2_10"></a>Luke 2:10

And the [aggelos](../../strongs/g/g32.md) [eipon](../../strongs/g/g2036.md) unto them, [phobeō](../../strongs/g/g5399.md) not: for, [idou](../../strongs/g/g2400.md), I [euaggelizō](../../strongs/g/g2097.md) you of [megas](../../strongs/g/g3173.md) [chara](../../strongs/g/g5479.md), which shall be to all [laos](../../strongs/g/g2992.md).

<a name="luke_2_11"></a>Luke 2:11

For unto you is [tiktō](../../strongs/g/g5088.md) [sēmeron](../../strongs/g/g4594.md) in the [polis](../../strongs/g/g4172.md) of [Dabid](../../strongs/g/g1138.md) a [sōtēr](../../strongs/g/g4990.md), which is [Christos](../../strongs/g/g5547.md) the [kyrios](../../strongs/g/g2962.md).

<a name="luke_2_12"></a>Luke 2:12

And this be a [sēmeion](../../strongs/g/g4592.md) unto you; Ye shall [heuriskō](../../strongs/g/g2147.md) the [brephos](../../strongs/g/g1025.md) [sparganoō](../../strongs/g/g4683.md), [keimai](../../strongs/g/g2749.md) in a [phatnē](../../strongs/g/g5336.md).

<a name="luke_2_13"></a>Luke 2:13

And [exaiphnēs](../../strongs/g/g1810.md) there was with the [aggelos](../../strongs/g/g32.md) a [plēthos](../../strongs/g/g4128.md) of the [ouranios](../../strongs/g/g3770.md) [stratia](../../strongs/g/g4756.md) [aineō](../../strongs/g/g134.md) [theos](../../strongs/g/g2316.md), and [legō](../../strongs/g/g3004.md),

<a name="luke_2_14"></a>Luke 2:14

[doxa](../../strongs/g/g1391.md) to [theos](../../strongs/g/g2316.md) in the [hypsistos](../../strongs/g/g5310.md), and on [gē](../../strongs/g/g1093.md) [eirēnē](../../strongs/g/g1515.md), [eudokia](../../strongs/g/g2107.md) toward [anthrōpos](../../strongs/g/g444.md). [^1]

<a name="luke_2_15"></a>Luke 2:15

And [ginomai](../../strongs/g/g1096.md), as the [aggelos](../../strongs/g/g32.md) were [aperchomai](../../strongs/g/g565.md) from them into [ouranos](../../strongs/g/g3772.md), the [anthrōpos](../../strongs/g/g444.md) [poimēn](../../strongs/g/g4166.md) [eipon](../../strongs/g/g2036.md) to [allēlōn](../../strongs/g/g240.md), Let us now [dierchomai](../../strongs/g/g1330.md) even unto [Bēthleem](../../strongs/g/g965.md), and [eidō](../../strongs/g/g1492.md) this [rhēma](../../strongs/g/g4487.md) which is [ginomai](../../strongs/g/g1096.md), which the [kyrios](../../strongs/g/g2962.md) hath [gnōrizō](../../strongs/g/g1107.md) unto us.

<a name="luke_2_16"></a>Luke 2:16

And they [erchomai](../../strongs/g/g2064.md) with [speudō](../../strongs/g/g4692.md), and [aneuriskō](../../strongs/g/g429.md) [Maria](../../strongs/g/g3137.md), and [Iōsēph](../../strongs/g/g2501.md), and the [brephos](../../strongs/g/g1025.md) [keimai](../../strongs/g/g2749.md) in a [phatnē](../../strongs/g/g5336.md).

<a name="luke_2_17"></a>Luke 2:17

And when they had [eidō](../../strongs/g/g1492.md), they [diagnōrizō](../../strongs/g/g1232.md) the [rhēma](../../strongs/g/g4487.md) which was [laleō](../../strongs/g/g2980.md) them concerning this [paidion](../../strongs/g/g3813.md).

<a name="luke_2_18"></a>Luke 2:18

And all they that [akouō](../../strongs/g/g191.md) [thaumazō](../../strongs/g/g2296.md) at those things which were [laleō](../../strongs/g/g2980.md) them by the [poimēn](../../strongs/g/g4166.md).

<a name="luke_2_19"></a>Luke 2:19

But [Maria](../../strongs/g/g3137.md) [syntēreō](../../strongs/g/g4933.md) all these [rhēma](../../strongs/g/g4487.md), and [symballō](../../strongs/g/g4820.md) in her [kardia](../../strongs/g/g2588.md).

<a name="luke_2_20"></a>Luke 2:20

And the [poimēn](../../strongs/g/g4166.md) [epistrephō](../../strongs/g/g1994.md), [doxazō](../../strongs/g/g1392.md) and [aineō](../../strongs/g/g134.md) [theos](../../strongs/g/g2316.md) for all the things that they had [akouō](../../strongs/g/g191.md) and [eidō](../../strongs/g/g1492.md), as it was [laleō](../../strongs/g/g2980.md) unto them.

<a name="luke_2_21"></a>Luke 2:21

And when eight [hēmera](../../strongs/g/g2250.md) were [pimplēmi](../../strongs/g/g4130.md) for the [peritemnō](../../strongs/g/g4059.md) of the [paidion](../../strongs/g/g3813.md), his [onoma](../../strongs/g/g3686.md) was [kaleō](../../strongs/g/g2564.md) [Iēsous](../../strongs/g/g2424.md), which was so [kaleō](../../strongs/g/g2564.md) of the [aggelos](../../strongs/g/g32.md) before he was [syllambanō](../../strongs/g/g4815.md) in the [koilia](../../strongs/g/g2836.md).

<a name="luke_2_22"></a>Luke 2:22

And when the [hēmera](../../strongs/g/g2250.md) of her [katharismos](../../strongs/g/g2512.md) according to the [nomos](../../strongs/g/g3551.md) of [Mōÿsēs](../../strongs/g/g3475.md) were [pimplēmi](../../strongs/g/g4130.md), they [anagō](../../strongs/g/g321.md) him to [Hierosolyma](../../strongs/g/g2414.md), to [paristēmi](../../strongs/g/g3936.md) to the [kyrios](../../strongs/g/g2962.md);

<a name="luke_2_23"></a>Luke 2:23

(As it is [graphō](../../strongs/g/g1125.md) in the [nomos](../../strongs/g/g3551.md) of the [kyrios](../../strongs/g/g2962.md), Every [arrēn](../../strongs/g/g730.md) that [dianoigō](../../strongs/g/g1272.md) the [mētra](../../strongs/g/g3388.md) shall be [kaleō](../../strongs/g/g2564.md) [hagios](../../strongs/g/g40.md) to the [kyrios](../../strongs/g/g2962.md);)

<a name="luke_2_24"></a>Luke 2:24

And to [didōmi](../../strongs/g/g1325.md) a [thysia](../../strongs/g/g2378.md) according to that which is [eipon](../../strongs/g/g2046.md) in the [nomos](../../strongs/g/g3551.md) of the [kyrios](../../strongs/g/g2962.md), A pair of [trygōn](../../strongs/g/g5167.md), or two [nossos](../../strongs/g/g3502.md) [peristera](../../strongs/g/g4058.md).

<a name="luke_2_25"></a>Luke 2:25

And, [idou](../../strongs/g/g2400.md), there was an [anthrōpos](../../strongs/g/g444.md) in [Ierousalēm](../../strongs/g/g2419.md), whose [onoma](../../strongs/g/g3686.md) [Symeōn](../../strongs/g/g4826.md); and the same [anthrōpos](../../strongs/g/g444.md) was [dikaios](../../strongs/g/g1342.md) and [eulabēs](../../strongs/g/g2126.md), [prosdechomai](../../strongs/g/g4327.md) for the [paraklēsis](../../strongs/g/g3874.md) of [Israēl](../../strongs/g/g2474.md): and the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) was upon him.

<a name="luke_2_26"></a>Luke 2:26

And it was [chrēmatizō](../../strongs/g/g5537.md) unto him by the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), that he should not [eidō](../../strongs/g/g1492.md) [thanatos](../../strongs/g/g2288.md), before he had [eidō](../../strongs/g/g1492.md) the [kyrios](../../strongs/g/g2962.md) [Christos](../../strongs/g/g5547.md)ß.

<a name="luke_2_27"></a>Luke 2:27

And he [erchomai](../../strongs/g/g2064.md) by the [pneuma](../../strongs/g/g4151.md) into the [hieron](../../strongs/g/g2411.md): and when the [goneus](../../strongs/g/g1118.md) [eisagō](../../strongs/g/g1521.md) in the [paidion](../../strongs/g/g3813.md) [Iēsous](../../strongs/g/g2424.md), to [poieō](../../strongs/g/g4160.md) for him after the [ethizō](../../strongs/g/g1480.md) of the [nomos](../../strongs/g/g3551.md),

<a name="luke_2_28"></a>Luke 2:28

Then [dechomai](../../strongs/g/g1209.md) he in his [agkalē](../../strongs/g/g43.md), and [eulogeō](../../strongs/g/g2127.md) [theos](../../strongs/g/g2316.md), and [eipon](../../strongs/g/g2036.md),

<a name="luke_2_29"></a>Luke 2:29

[despotēs](../../strongs/g/g1203.md), now lettest thou thy [doulos](../../strongs/g/g1401.md) [apolyō](../../strongs/g/g630.md) in [eirēnē](../../strongs/g/g1515.md), according to thy [rhēma](../../strongs/g/g4487.md):

<a name="luke_2_30"></a>Luke 2:30

For mine [ophthalmos](../../strongs/g/g3788.md) have [eidō](../../strongs/g/g1492.md) thy [sōtērios](../../strongs/g/g4992.md),

<a name="luke_2_31"></a>Luke 2:31

Which thou hast [hetoimazō](../../strongs/g/g2090.md) before the [prosōpon](../../strongs/g/g4383.md) of all [laos](../../strongs/g/g2992.md);

<a name="luke_2_32"></a>Luke 2:32

A [phōs](../../strongs/g/g5457.md) to [apokalypsis](../../strongs/g/g602.md) the [ethnos](../../strongs/g/g1484.md), and the [doxa](../../strongs/g/g1391.md) of thy [laos](../../strongs/g/g2992.md) [Israēl](../../strongs/g/g2474.md).

<a name="luke_2_33"></a>Luke 2:33

And [Iōsēph](../../strongs/g/g2501.md) and his [mētēr](../../strongs/g/g3384.md) [thaumazō](../../strongs/g/g2296.md) at those things which were [laleō](../../strongs/g/g2980.md) of him.

<a name="luke_2_34"></a>Luke 2:34

And [Symeōn](../../strongs/g/g4826.md) [eulogeō](../../strongs/g/g2127.md) them, and [eipon](../../strongs/g/g2036.md) unto [Maria](../../strongs/g/g3137.md) his [mētēr](../../strongs/g/g3384.md), [idou](../../strongs/g/g2400.md), this is [keimai](../../strongs/g/g2749.md) for the [ptōsis](../../strongs/g/g4431.md) and [anastasis](../../strongs/g/g386.md) of [polys](../../strongs/g/g4183.md) in [Israēl](../../strongs/g/g2474.md); and for a [sēmeion](../../strongs/g/g4592.md) which shall be [antilegō](../../strongs/g/g483.md);

<a name="luke_2_35"></a>Luke 2:35

(Yea, a [rhomphaia](../../strongs/g/g4501.md) shall [dierchomai](../../strongs/g/g1330.md) thy own [psychē](../../strongs/g/g5590.md) also,) that the [dialogismos](../../strongs/g/g1261.md) of [polys](../../strongs/g/g4183.md) [kardia](../../strongs/g/g2588.md) may be [apokalyptō](../../strongs/g/g601.md).

<a name="luke_2_36"></a>Luke 2:36

And there was one [Anna](../../strongs/g/g451.md), a [prophētis](../../strongs/g/g4398.md), the [thygatēr](../../strongs/g/g2364.md) of [phanouēl](../../strongs/g/g5323.md), of the [phylē](../../strongs/g/g5443.md) of [Asēr](../../strongs/g/g768.md): she was of a [polys](../../strongs/g/g4183.md) [hēmera](../../strongs/g/g2250.md) [probainō](../../strongs/g/g4260.md), and had [zaō](../../strongs/g/g2198.md) with an [anēr](../../strongs/g/g435.md) seven [etos](../../strongs/g/g2094.md) from her [parthenia](../../strongs/g/g3932.md);

<a name="luke_2_37"></a>Luke 2:37

And she a [chēra](../../strongs/g/g5503.md) of about fourscore and four [etos](../../strongs/g/g2094.md), which [aphistēmi](../../strongs/g/g868.md) not from the [hieron](../../strongs/g/g2411.md), but [latreuō](../../strongs/g/g3000.md) with [nēsteia](../../strongs/g/g3521.md) and [deēsis](../../strongs/g/g1162.md) [nyx](../../strongs/g/g3571.md) and [hēmera](../../strongs/g/g2250.md).

<a name="luke_2_38"></a>Luke 2:38

And she [ephistēmi](../../strongs/g/g2186.md) that [hōra](../../strongs/g/g5610.md) [anthomologeomai](../../strongs/g/g437.md)  unto the [kyrios](../../strongs/g/g2962.md), and [laleō](../../strongs/g/g2980.md) of him to all them that [prosdechomai](../../strongs/g/g4327.md) [lytrōsis](../../strongs/g/g3085.md) in [Ierousalēm](../../strongs/g/g2419.md).

<a name="luke_2_39"></a>Luke 2:39

And when they had [teleō](../../strongs/g/g5055.md) all things according to the [nomos](../../strongs/g/g3551.md) of the [kyrios](../../strongs/g/g2962.md), they [hypostrephō](../../strongs/g/g5290.md) into [Galilaia](../../strongs/g/g1056.md), to their own [polis](../../strongs/g/g4172.md) [Nazara](../../strongs/g/g3478.md).

<a name="luke_2_40"></a>Luke 2:40

And the [paidion](../../strongs/g/g3813.md) [auxanō](../../strongs/g/g837.md), and [krataioō](../../strongs/g/g2901.md) in [pneuma](../../strongs/g/g4151.md), [plēroō](../../strongs/g/g4137.md) with [sophia](../../strongs/g/g4678.md): and the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) was upon him.

<a name="luke_2_41"></a>Luke 2:41

Now his [goneus](../../strongs/g/g1118.md) [poreuō](../../strongs/g/g4198.md) to [Ierousalēm](../../strongs/g/g2419.md) every [etos](../../strongs/g/g2094.md) at the [heortē](../../strongs/g/g1859.md) of the [pascha](../../strongs/g/g3957.md).

<a name="luke_2_42"></a>Luke 2:42

And when he was [dōdeka](../../strongs/g/g1427.md) [etos](../../strongs/g/g2094.md), they [anabainō](../../strongs/g/g305.md) to [Hierosolyma](../../strongs/g/g2414.md) after the [ethos](../../strongs/g/g1485.md) of the [heortē](../../strongs/g/g1859.md).

<a name="luke_2_43"></a>Luke 2:43

And when they had [teleioō](../../strongs/g/g5048.md) the [hēmera](../../strongs/g/g2250.md), as they [hypostrephō](../../strongs/g/g5290.md), the [pais](../../strongs/g/g3816.md) [Iēsous](../../strongs/g/g2424.md) [hypomenō](../../strongs/g/g5278.md) in [Ierousalēm](../../strongs/g/g2419.md); and [Iōsēph](../../strongs/g/g2501.md) and his [mētēr](../../strongs/g/g3384.md) [ginōskō](../../strongs/g/g1097.md) not.

<a name="luke_2_44"></a>Luke 2:44

But they, [nomizō](../../strongs/g/g3543.md) him to have been in the [synodia](../../strongs/g/g4923.md), [erchomai](../../strongs/g/g2064.md) a [hēmera](../../strongs/g/g2250.md) [hodos](../../strongs/g/g3598.md); and they [anazēteō](../../strongs/g/g327.md) him among [syggenēs](../../strongs/g/g4773.md) and [gnōstos](../../strongs/g/g1110.md).

<a name="luke_2_45"></a>Luke 2:45

And when they [heuriskō](../../strongs/g/g2147.md) him not, they [hypostrephō](../../strongs/g/g5290.md) to [Ierousalēm](../../strongs/g/g2419.md), [zēteō](../../strongs/g/g2212.md) him.

<a name="luke_2_46"></a>Luke 2:46

And [ginomai](../../strongs/g/g1096.md), that after three [hēmera](../../strongs/g/g2250.md) they [heuriskō](../../strongs/g/g2147.md) him in the [hieron](../../strongs/g/g2411.md), [kathezomai](../../strongs/g/g2516.md) in the [mesos](../../strongs/g/g3319.md) of the [didaskalos](../../strongs/g/g1320.md), both [akouō](../../strongs/g/g191.md) them, and [eperōtaō](../../strongs/g/g1905.md) them.

<a name="luke_2_47"></a>Luke 2:47

And all that [akouō](../../strongs/g/g191.md) him were [existēmi](../../strongs/g/g1839.md) at his [synesis](../../strongs/g/g4907.md) and [apokrisis](../../strongs/g/g612.md).

<a name="luke_2_48"></a>Luke 2:48

And when they [eidō](../../strongs/g/g1492.md) him, they were [ekplēssō](../../strongs/g/g1605.md): and his [mētēr](../../strongs/g/g3384.md) [eipon](../../strongs/g/g2036.md) unto him, [teknon](../../strongs/g/g5043.md), why hast thou thus [poieō](../../strongs/g/g4160.md) with us? [idou](../../strongs/g/g2400.md), thy [patēr](../../strongs/g/g3962.md) and I have [zēteō](../../strongs/g/g2212.md) thee [odynaō](../../strongs/g/g3600.md).

<a name="luke_2_49"></a>Luke 2:49

And he [eipon](../../strongs/g/g2036.md) unto them, **How is it that ye [zēteō](../../strongs/g/g2212.md) me? [eidō](../../strongs/g/g1492.md) ye not that I must be about my [patēr](../../strongs/g/g3962.md)?**

<a name="luke_2_50"></a>Luke 2:50

And they [syniēmi](../../strongs/g/g4920.md) not the [rhēma](../../strongs/g/g4487.md) which he [laleō](../../strongs/g/g2980.md) unto them.

<a name="luke_2_51"></a>Luke 2:51

And he [katabainō](../../strongs/g/g2597.md) with them, and [erchomai](../../strongs/g/g2064.md) to [Nazara](../../strongs/g/g3478.md), and was [hypotassō](../../strongs/g/g5293.md) unto them: but his [mētēr](../../strongs/g/g3384.md) [diatēreō](../../strongs/g/g1301.md) all these [rhēma](../../strongs/g/g4487.md) in her [kardia](../../strongs/g/g2588.md).

<a name="luke_2_52"></a>Luke 2:52

And [Iēsous](../../strongs/g/g2424.md) [prokoptō](../../strongs/g/g4298.md) in [sophia](../../strongs/g/g4678.md) and [hēlikia](../../strongs/g/g2244.md), and in [charis](../../strongs/g/g5485.md) with [theos](../../strongs/g/g2316.md) and [anthrōpos](../../strongs/g/g444.md).

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke](luke_1.md) - [Luke](luke_3.md)

---

[^1]: [Luke 2:14 Commentary](../../commentary/luke/luke_2_commentary.md#luke_2_14)
