# [Luke 8](https://www.blueletterbible.org/kjv/luk/8/1/rl1/s_981001)

<a name="luke_8_1"></a>Luke 8:1

And [ginomai](../../strongs/g/g1096.md) [kathexēs](../../strongs/g/g2517.md), that he [diodeuō](../../strongs/g/g1353.md) every [polis](../../strongs/g/g4172.md) and [kōmē](../../strongs/g/g2968.md), [kēryssō](../../strongs/g/g2784.md) and [euaggelizō](../../strongs/g/g2097.md) of the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md): and the [dōdeka](../../strongs/g/g1427.md) were with him,

<a name="luke_8_2"></a>Luke 8:2

And certain [gynē](../../strongs/g/g1135.md), which had been [therapeuō](../../strongs/g/g2323.md) of [ponēros](../../strongs/g/g4190.md) [pneuma](../../strongs/g/g4151.md) and [astheneia](../../strongs/g/g769.md), [Maria](../../strongs/g/g3137.md) [kaleō](../../strongs/g/g2564.md) [Magdalēnē](../../strongs/g/g3094.md), out of whom [exerchomai](../../strongs/g/g1831.md) seven [daimonion](../../strongs/g/g1140.md),

<a name="luke_8_3"></a>Luke 8:3

And [Iōan(n)a](../../strongs/g/g2489.md) the [gynē](../../strongs/g/g1135.md) of [Chouzas](../../strongs/g/g5529.md) [Hērōdēs](../../strongs/g/g2264.md) [epitropos](../../strongs/g/g2012.md), and [Sousanna](../../strongs/g/g4677.md), and [polys](../../strongs/g/g4183.md) others, which [diakoneō](../../strongs/g/g1247.md) unto him of their [hyparchonta](../../strongs/g/g5224.md).

<a name="luke_8_4"></a>Luke 8:4

And when [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) were [syneimi](../../strongs/g/g4896.md), and were [epiporeuomai](../../strongs/g/g1975.md) to him out of every [polis](../../strongs/g/g4172.md), he [eipon](../../strongs/g/g2036.md) by a [parabolē](../../strongs/g/g3850.md):

<a name="luke_8_5"></a>Luke 8:5

**A [speirō](../../strongs/g/g4687.md) [exerchomai](../../strongs/g/g1831.md) to [speirō](../../strongs/g/g4687.md) his [sporos](../../strongs/g/g4703.md): and as he [speirō](../../strongs/g/g4687.md), some [piptō](../../strongs/g/g4098.md) by the [hodos](../../strongs/g/g3598.md); and it was [katapateō](../../strongs/g/g2662.md), and the [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md) [katesthiō](../../strongs/g/g2719.md) it.**

<a name="luke_8_6"></a>Luke 8:6

**And some [piptō](../../strongs/g/g4098.md) upon a [petra](../../strongs/g/g4073.md); and as soon as it was [phyō](../../strongs/g/g5453.md), it [xērainō](../../strongs/g/g3583.md), because it lacked [ikmas](../../strongs/g/g2429.md).**

<a name="luke_8_7"></a>Luke 8:7

**And some [piptō](../../strongs/g/g4098.md) among [akantha](../../strongs/g/g173.md); and the [akantha](../../strongs/g/g173.md) [symphyō](../../strongs/g/g4855.md), and [apopnigō](../../strongs/g/g638.md) [epipnigō](../../strongs/g/g1970.md) it.**

<a name="luke_8_8"></a>Luke 8:8

**And other [piptō](../../strongs/g/g4098.md) on [agathos](../../strongs/g/g18.md) [gē](../../strongs/g/g1093.md), and [phyō](../../strongs/g/g5453.md), and [poieō](../../strongs/g/g4160.md) [karpos](../../strongs/g/g2590.md) an [hekatontaplasiōn](../../strongs/g/g1542.md).** And when he had [legō](../../strongs/g/g3004.md) these things, he [phōneō](../../strongs/g/g5455.md), **He that hath [ous](../../strongs/g/g3775.md) to [akouō](../../strongs/g/g191.md), let him [akouō](../../strongs/g/g191.md).**

<a name="luke_8_9"></a>Luke 8:9

And his [mathētēs](../../strongs/g/g3101.md) [eperōtaō](../../strongs/g/g1905.md) him, [legō](../../strongs/g/g3004.md), What might this [parabolē](../../strongs/g/g3850.md) be?

<a name="luke_8_10"></a>Luke 8:10

And he [eipon](../../strongs/g/g2036.md), **Unto you it is [didōmi](../../strongs/g/g1325.md) to [ginōskō](../../strongs/g/g1097.md) the [mystērion](../../strongs/g/g3466.md) of the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md): but to [loipos](../../strongs/g/g3062.md) in [parabolē](../../strongs/g/g3850.md); that [blepō](../../strongs/g/g991.md) they might not [blepō](../../strongs/g/g991.md), and [akouō](../../strongs/g/g191.md) they might not [syniēmi](../../strongs/g/g4920.md).**

<a name="luke_8_11"></a>Luke 8:11

**Now the [parabolē](../../strongs/g/g3850.md) is this: The [sporos](../../strongs/g/g4703.md) is the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md).**

<a name="luke_8_12"></a>Luke 8:12

**Those by the [hodos](../../strongs/g/g3598.md) are they that [akouō](../../strongs/g/g191.md); then [erchomai](../../strongs/g/g2064.md) the [diabolos](../../strongs/g/g1228.md), and [airō](../../strongs/g/g142.md) the [logos](../../strongs/g/g3056.md) out of their [kardia](../../strongs/g/g2588.md), lest they should [pisteuō](../../strongs/g/g4100.md) and be [sōzō](../../strongs/g/g4982.md).**

<a name="luke_8_13"></a>Luke 8:13

**They on the [petra](../../strongs/g/g4073.md), which, when they [akouō](../../strongs/g/g191.md), [dechomai](../../strongs/g/g1209.md) the [logos](../../strongs/g/g3056.md) with [chara](../../strongs/g/g5479.md); and these have no [rhiza](../../strongs/g/g4491.md), which for a while [pisteuō](../../strongs/g/g4100.md), and in [kairos](../../strongs/g/g2540.md) of [peirasmos](../../strongs/g/g3986.md) [aphistēmi](../../strongs/g/g868.md).**

<a name="luke_8_14"></a>Luke 8:14

**And that which [piptō](../../strongs/g/g4098.md) among [akantha](../../strongs/g/g173.md) are they, which, when they have [akouō](../../strongs/g/g191.md), [poreuō](../../strongs/g/g4198.md), and are [sympnigō](../../strongs/g/g4846.md) with [merimna](../../strongs/g/g3308.md) and [ploutos](../../strongs/g/g4149.md) and [hēdonē](../../strongs/g/g2237.md) of [bios](../../strongs/g/g979.md), and [telesphoreō](../../strongs/g/g5052.md) not.**

<a name="luke_8_15"></a>Luke 8:15

**But that on the [kalos](../../strongs/g/g2570.md) [gē](../../strongs/g/g1093.md) are they, which in a [kalos](../../strongs/g/g2570.md) and [agathos](../../strongs/g/g18.md) [kardia](../../strongs/g/g2588.md), having [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md), [katechō](../../strongs/g/g2722.md), and [karpophoreō](../../strongs/g/g2592.md) with [hypomonē](../../strongs/g/g5281.md).**

<a name="luke_8_16"></a>Luke 8:16

**[oudeis](../../strongs/g/g3762.md), when he hath [haptō](../../strongs/g/g681.md) a [lychnos](../../strongs/g/g3088.md), [kalyptō](../../strongs/g/g2572.md) it with a [skeuos](../../strongs/g/g4632.md), or [tithēmi](../../strongs/g/g5087.md) it [hypokatō](../../strongs/g/g5270.md) a [klinē](../../strongs/g/g2825.md); but [epitithēmi](../../strongs/g/g2007.md) it on a [lychnia](../../strongs/g/g3087.md), that they which [eisporeuomai](../../strongs/g/g1531.md) may [blepō](../../strongs/g/g991.md) the [phōs](../../strongs/g/g5457.md).**

<a name="luke_8_17"></a>Luke 8:17

**For nothing is [kryptos](../../strongs/g/g2927.md), that shall not be made [phaneros](../../strongs/g/g5318.md); neither any thing [apokryphos](../../strongs/g/g614.md), that shall not be [ginōskō](../../strongs/g/g1097.md) and [erchomai](../../strongs/g/g2064.md) [phaneros](../../strongs/g/g5318.md).**

<a name="luke_8_18"></a>Luke 8:18

**[blepō](../../strongs/g/g991.md) therefore how ye [akouō](../../strongs/g/g191.md): for whosoever hath, to him shall be [didōmi](../../strongs/g/g1325.md); and whosoever hath not, from him shall be [airō](../../strongs/g/g142.md) even that which he [dokeō](../../strongs/g/g1380.md) to have.**

<a name="luke_8_19"></a>Luke 8:19

Then [paraginomai](../../strongs/g/g3854.md) to him [mētēr](../../strongs/g/g3384.md) and his [adelphos](../../strongs/g/g80.md), and could not [syntygchanō](../../strongs/g/g4940.md) at him for the [ochlos](../../strongs/g/g3793.md).

<a name="luke_8_20"></a>Luke 8:20

And it was [apaggellō](../../strongs/g/g518.md) him which [legō](../../strongs/g/g3004.md), Thy [mētēr](../../strongs/g/g3384.md) and thy [adelphos](../../strongs/g/g80.md) [histēmi](../../strongs/g/g2476.md) without, [thelō](../../strongs/g/g2309.md) to [eidō](../../strongs/g/g1492.md) thee.

<a name="luke_8_21"></a>Luke 8:21

And he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **My [mētēr](../../strongs/g/g3384.md) and my [adelphos](../../strongs/g/g80.md) are these which [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md), and [poieō](../../strongs/g/g4160.md) it.**

<a name="luke_8_22"></a>Luke 8:22

Now [ginomai](../../strongs/g/g1096.md) on a certain [hēmera](../../strongs/g/g2250.md), that he [embainō](../../strongs/g/g1684.md) into a [ploion](../../strongs/g/g4143.md) with his [mathētēs](../../strongs/g/g3101.md): and he [eipon](../../strongs/g/g2036.md) unto them, **Let us [dierchomai](../../strongs/g/g1330.md) unto the other side of the [limnē](../../strongs/g/g3041.md).** And they [anagō](../../strongs/g/g321.md).

<a name="luke_8_23"></a>Luke 8:23

But as they [pleō](../../strongs/g/g4126.md) he [aphypnoō](../../strongs/g/g879.md): and there [katabainō](../../strongs/g/g2597.md) a [lailaps](../../strongs/g/g2978.md) of [anemos](../../strongs/g/g417.md) on the [limnē](../../strongs/g/g3041.md); and they were [symplēroō](../../strongs/g/g4845.md), and were in [kindyneuō](../../strongs/g/g2793.md).

<a name="luke_8_24"></a>Luke 8:24

And they [proserchomai](../../strongs/g/g4334.md) to him, and [diegeirō](../../strongs/g/g1326.md) him, [legō](../../strongs/g/g3004.md), [epistatēs](../../strongs/g/g1988.md), [epistatēs](../../strongs/g/g1988.md), we [apollymi](../../strongs/g/g622.md). Then he [egeirō](../../strongs/g/g1453.md), and [epitimaō](../../strongs/g/g2008.md) the [anemos](../../strongs/g/g417.md) and the [klydōn](../../strongs/g/g2830.md) of the [hydōr](../../strongs/g/g5204.md): and they [pauō](../../strongs/g/g3973.md), and there was a [galēnē](../../strongs/g/g1055.md).

<a name="luke_8_25"></a>Luke 8:25

And he [eipon](../../strongs/g/g2036.md) unto them, **Where is your [pistis](../../strongs/g/g4102.md)?** And they being [phobeō](../../strongs/g/g5399.md) [thaumazō](../../strongs/g/g2296.md), [legō](../../strongs/g/g3004.md) to [allēlōn](../../strongs/g/g240.md), [tis](../../strongs/g/g5101.md) [ara](../../strongs/g/g686.md) is this! for he [epitassō](../../strongs/g/g2004.md) even the [anemos](../../strongs/g/g417.md) and [hydōr](../../strongs/g/g5204.md), and they [hypakouō](../../strongs/g/g5219.md) him.

<a name="luke_8_26"></a>Luke 8:26

And they [katapleō](../../strongs/g/g2668.md) at the [chōra](../../strongs/g/g5561.md) of the [Gadarēnos](../../strongs/g/g1046.md), which is over [antiperan](../../strongs/g/g495.md) [Galilaia](../../strongs/g/g1056.md).

<a name="luke_8_27"></a>Luke 8:27

And when he [exerchomai](../../strongs/g/g1831.md) to [gē](../../strongs/g/g1093.md), there [hypantaō](../../strongs/g/g5221.md) him out of the [polis](../../strongs/g/g4172.md) a certain [anēr](../../strongs/g/g435.md), which had [daimonion](../../strongs/g/g1140.md) [hikanos](../../strongs/g/g2425.md) [chronos](../../strongs/g/g5550.md), and [endidyskō](../../strongs/g/g1737.md) no [himation](../../strongs/g/g2440.md), neither [menō](../../strongs/g/g3306.md) in any [oikia](../../strongs/g/g3614.md), but in the [mnēma](../../strongs/g/g3418.md).

<a name="luke_8_28"></a>Luke 8:28

When he [eidō](../../strongs/g/g1492.md) [Iēsous](../../strongs/g/g2424.md), he [anakrazō](../../strongs/g/g349.md), and [prospiptō](../../strongs/g/g4363.md) him, and with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md) [eipon](../../strongs/g/g2036.md), What have I to do with thee, [Iēsous](../../strongs/g/g2424.md), [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md) [hypsistos](../../strongs/g/g5310.md)? I [deomai](../../strongs/g/g1189.md) thee, [basanizō](../../strongs/g/g928.md) me not.

<a name="luke_8_29"></a>Luke 8:29

(For he had [paraggellō](../../strongs/g/g3853.md) the [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md) to [exerchomai](../../strongs/g/g1831.md) out of the [anthrōpos](../../strongs/g/g444.md). For [polys](../../strongs/g/g4183.md) [chronos](../../strongs/g/g5550.md) it had [synarpazō](../../strongs/g/g4884.md) him: and he was [phylassō](../../strongs/g/g5442.md) [desmeō](../../strongs/g/g1196.md) with [halysis](../../strongs/g/g254.md) and in [pedē](../../strongs/g/g3976.md); and he [diarrēssō](../../strongs/g/g1284.md) the [desmos](../../strongs/g/g1199.md), and was [elaunō](../../strongs/g/g1643.md) of the [daimōn](../../strongs/g/g1142.md) into the [erēmos](../../strongs/g/g2048.md).)

<a name="luke_8_30"></a>Luke 8:30

And [Iēsous](../../strongs/g/g2424.md) [eperōtaō](../../strongs/g/g1905.md) him, [legō](../../strongs/g/g3004.md), **What is thy [onoma](../../strongs/g/g3686.md)?** And he [eipon](../../strongs/g/g2036.md), [legiōn](../../strongs/g/g3003.md): because [polys](../../strongs/g/g4183.md) [daimonion](../../strongs/g/g1140.md) were [eiserchomai](../../strongs/g/g1525.md) into him.

<a name="luke_8_31"></a>Luke 8:31

And they [parakaleō](../../strongs/g/g3870.md) him that he would not [epitassō](../../strongs/g/g2004.md) them to [aperchomai](../../strongs/g/g565.md) into the [abyssos](../../strongs/g/g12.md).

<a name="luke_8_32"></a>Luke 8:32

And there was there an [agelē](../../strongs/g/g34.md) of [hikanos](../../strongs/g/g2425.md) [choiros](../../strongs/g/g5519.md) [boskō](../../strongs/g/g1006.md) on the [oros](../../strongs/g/g3735.md): and they [parakaleō](../../strongs/g/g3870.md) him that he would [epitrepō](../../strongs/g/g2010.md) them to [eiserchomai](../../strongs/g/g1525.md) into them. And he [epitrepō](../../strongs/g/g2010.md) them.

<a name="luke_8_33"></a>Luke 8:33

Then [exerchomai](../../strongs/g/g1831.md) the [daimonion](../../strongs/g/g1140.md) out of the [anthrōpos](../../strongs/g/g444.md), and [eiserchomai](../../strongs/g/g1525.md) into the [choiros](../../strongs/g/g5519.md): and the [agelē](../../strongs/g/g34.md) [hormaō](../../strongs/g/g3729.md) down a [krēmnos](../../strongs/g/g2911.md) into the [limnē](../../strongs/g/g3041.md), and were [apopnigō](../../strongs/g/g638.md).

<a name="luke_8_34"></a>Luke 8:34

When they that [boskō](../../strongs/g/g1006.md) [eidō](../../strongs/g/g1492.md) what was [ginomai](../../strongs/g/g1096.md), they [pheugō](../../strongs/g/g5343.md), and [aperchomai](../../strongs/g/g565.md) and [apaggellō](../../strongs/g/g518.md) in the [polis](../../strongs/g/g4172.md) and in the [agros](../../strongs/g/g68.md).

<a name="luke_8_35"></a>Luke 8:35

Then they [exerchomai](../../strongs/g/g1831.md) to [eidō](../../strongs/g/g1492.md) what was [ginomai](../../strongs/g/g1096.md); and [erchomai](../../strongs/g/g2064.md) to [Iēsous](../../strongs/g/g2424.md), and [heuriskō](../../strongs/g/g2147.md) the [anthrōpos](../../strongs/g/g444.md), out of whom the [daimonion](../../strongs/g/g1140.md) were [exerchomai](../../strongs/g/g1831.md), [kathēmai](../../strongs/g/g2521.md) at the [pous](../../strongs/g/g4228.md) of [Iēsous](../../strongs/g/g2424.md), [himatizō](../../strongs/g/g2439.md), and [sōphroneō](../../strongs/g/g4993.md): and they were [phobeō](../../strongs/g/g5399.md).

<a name="luke_8_36"></a>Luke 8:36

They also which [eidō](../../strongs/g/g1492.md) [apaggellō](../../strongs/g/g518.md) them by what means [daimonizomai](../../strongs/g/g1139.md) was [sōzō](../../strongs/g/g4982.md).

<a name="luke_8_37"></a>Luke 8:37

Then the [hapas](../../strongs/g/g537.md) [plēthos](../../strongs/g/g4128.md) of the [perichōros](../../strongs/g/g4066.md) of the [Gadarēnos](../../strongs/g/g1046.md) [erōtaō](../../strongs/g/g2065.md) him to [aperchomai](../../strongs/g/g565.md) from them; for they were [synechō](../../strongs/g/g4912.md) with [megas](../../strongs/g/g3173.md) [phobos](../../strongs/g/g5401.md): and he [embainō](../../strongs/g/g1684.md) into the [ploion](../../strongs/g/g4143.md), and [hypostrephō](../../strongs/g/g5290.md).

<a name="luke_8_38"></a>Luke 8:38

Now the [anēr](../../strongs/g/g435.md) out of whom the [daimonion](../../strongs/g/g1140.md) were [exerchomai](../../strongs/g/g1831.md) [deomai](../../strongs/g/g1189.md) him that he might be with him: but [Iēsous](../../strongs/g/g2424.md) [apolyō](../../strongs/g/g630.md) him, [legō](../../strongs/g/g3004.md),

<a name="luke_8_39"></a>Luke 8:39

**[hypostrephō](../../strongs/g/g5290.md) to thine own [oikos](../../strongs/g/g3624.md), and [diēgeomai](../../strongs/g/g1334.md) [hosos](../../strongs/g/g3745.md) [theos](../../strongs/g/g2316.md) hath [poieō](../../strongs/g/g4160.md) unto thee.** And he [aperchomai](../../strongs/g/g565.md) his way, and [kēryssō](../../strongs/g/g2784.md) throughout the [holos](../../strongs/g/g3650.md) [polis](../../strongs/g/g4172.md) [hosos](../../strongs/g/g3745.md) [Iēsous](../../strongs/g/g2424.md) had [poieō](../../strongs/g/g4160.md) unto him.

<a name="luke_8_40"></a>Luke 8:40

And [ginomai](../../strongs/g/g1096.md), when [Iēsous](../../strongs/g/g2424.md) was [hypostrephō](../../strongs/g/g5290.md), the [ochlos](../../strongs/g/g3793.md) [apodechomai](../../strongs/g/g588.md) him: for they were all [prosdokaō](../../strongs/g/g4328.md) for him.

<a name="luke_8_41"></a>Luke 8:41

And, [idou](../../strongs/g/g2400.md), there [erchomai](../../strongs/g/g2064.md) an [anēr](../../strongs/g/g435.md) [onoma](../../strongs/g/g3686.md) [Iaïros](../../strongs/g/g2383.md), and he [hyparchō](../../strongs/g/g5225.md) an [archōn](../../strongs/g/g758.md) of the [synagōgē](../../strongs/g/g4864.md): and he [piptō](../../strongs/g/g4098.md) at [Iēsous](../../strongs/g/g2424.md) [pous](../../strongs/g/g4228.md), and [parakaleō](../../strongs/g/g3870.md) him that he would [eiserchomai](../../strongs/g/g1525.md) into his [oikos](../../strongs/g/g3624.md):

<a name="luke_8_42"></a>Luke 8:42

For he had [monogenēs](../../strongs/g/g3439.md) [thygatēr](../../strongs/g/g2364.md), about twelve [etos](../../strongs/g/g2094.md), and she [apothnēskō](../../strongs/g/g599.md). But as he [hypagō](../../strongs/g/g5217.md) the [ochlos](../../strongs/g/g3793.md) [sympnigō](../../strongs/g/g4846.md) him.

<a name="luke_8_43"></a>Luke 8:43

And a [gynē](../../strongs/g/g1135.md) having a [rhysis](../../strongs/g/g4511.md) of [haima](../../strongs/g/g129.md) twelve [etos](../../strongs/g/g2094.md), which had [prosanaliskō](../../strongs/g/g4321.md) all her [bios](../../strongs/g/g979.md) upon [iatros](../../strongs/g/g2395.md), neither could be [therapeuō](../../strongs/g/g2323.md) of any,

<a name="luke_8_44"></a>Luke 8:44

[proserchomai](../../strongs/g/g4334.md) behind, and [haptomai](../../strongs/g/g680.md) the [kraspedon](../../strongs/g/g2899.md) of his [himation](../../strongs/g/g2440.md): and [parachrēma](../../strongs/g/g3916.md) her [rhysis](../../strongs/g/g4511.md) of [haima](../../strongs/g/g129.md) [histēmi](../../strongs/g/g2476.md).

<a name="luke_8_45"></a>Luke 8:45

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md), **Who [haptomai](../../strongs/g/g680.md) me?** When all [arneomai](../../strongs/g/g720.md), [Petros](../../strongs/g/g4074.md) and they that were with him [eipon](../../strongs/g/g2036.md), [epistatēs](../../strongs/g/g1988.md), the [ochlos](../../strongs/g/g3793.md) [synechō](../../strongs/g/g4912.md) thee and [apothlibō](../../strongs/g/g598.md), and [legō](../../strongs/g/g3004.md) thou, **Who [haptomai](../../strongs/g/g680.md) me?**

<a name="luke_8_46"></a>Luke 8:46

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md), **Somebody hath [haptomai](../../strongs/g/g680.md) me: for I [ginōskō](../../strongs/g/g1097.md) that [dynamis](../../strongs/g/g1411.md) is [exerchomai](../../strongs/g/g1831.md) out of me.**

<a name="luke_8_47"></a>Luke 8:47

And when the [gynē](../../strongs/g/g1135.md) [eidō](../../strongs/g/g1492.md) that she was not [lanthanō](../../strongs/g/g2990.md), she [erchomai](../../strongs/g/g2064.md) [tremō](../../strongs/g/g5141.md), and [prospiptō](../../strongs/g/g4363.md) before him, she [apaggellō](../../strongs/g/g518.md) unto him before all the [laos](../../strongs/g/g2992.md) for what [aitia](../../strongs/g/g156.md) she had [haptomai](../../strongs/g/g680.md) him, and how she was [iaomai](../../strongs/g/g2390.md) [parachrēma](../../strongs/g/g3916.md).

<a name="luke_8_48"></a>Luke 8:48

And he [eipon](../../strongs/g/g2036.md) unto her, **[thygatēr](../../strongs/g/g2364.md), [tharseō](../../strongs/g/g2293.md): thy [pistis](../../strongs/g/g4102.md) hath [sōzō](../../strongs/g/g4982.md) thee; [poreuō](../../strongs/g/g4198.md) in [eirēnē](../../strongs/g/g1515.md).**

<a name="luke_8_49"></a>Luke 8:49

While he yet [laleō](../../strongs/g/g2980.md), there [erchomai](../../strongs/g/g2064.md) one from the [archisynagōgos](../../strongs/g/g752.md), [legō](../../strongs/g/g3004.md) to him, Thy [thygatēr](../../strongs/g/g2364.md) is [thnēskō](../../strongs/g/g2348.md); [skyllō](../../strongs/g/g4660.md) not the [didaskalos](../../strongs/g/g1320.md).

<a name="luke_8_50"></a>Luke 8:50

But when [Iēsous](../../strongs/g/g2424.md) [akouō](../../strongs/g/g191.md), he [apokrinomai](../../strongs/g/g611.md) him, [legō](../../strongs/g/g3004.md), **[phobeō](../../strongs/g/g5399.md) not: [pisteuō](../../strongs/g/g4100.md) only, and [sōzō](../../strongs/g/g4982.md).**

<a name="luke_8_51"></a>Luke 8:51

And when he [eiserchomai](../../strongs/g/g1525.md) into the [oikia](../../strongs/g/g3614.md), he [aphiēmi](../../strongs/g/g863.md) [ou](../../strongs/g/g3756.md) [oudeis](../../strongs/g/g3762.md) to [eiserchomai](../../strongs/g/g1525.md), save [Petros](../../strongs/g/g4074.md), and [Iakōbos](../../strongs/g/g2385.md), and [Iōannēs](../../strongs/g/g2491.md), and the [patēr](../../strongs/g/g3962.md) and the [mētēr](../../strongs/g/g3384.md) of the [pais](../../strongs/g/g3816.md).

<a name="luke_8_52"></a>Luke 8:52

And all [klaiō](../../strongs/g/g2799.md), and [koptō](../../strongs/g/g2875.md) her: but he [eipon](../../strongs/g/g2036.md), **[klaiō](../../strongs/g/g2799.md) not; she is not [apothnēskō](../../strongs/g/g599.md), but [katheudō](../../strongs/g/g2518.md).**

<a name="luke_8_53"></a>Luke 8:53

And they [katagelaō](../../strongs/g/g2606.md) him, [eidō](../../strongs/g/g1492.md) that she was [apothnēskō](../../strongs/g/g599.md).

<a name="luke_8_54"></a>Luke 8:54

And he [ekballō](../../strongs/g/g1544.md) them all out, and [krateō](../../strongs/g/g2902.md) her by the [cheir](../../strongs/g/g5495.md), and [phōneō](../../strongs/g/g5455.md), [legō](../../strongs/g/g3004.md), **[pais](../../strongs/g/g3816.md), [egeirō](../../strongs/g/g1453.md).**

<a name="luke_8_55"></a>Luke 8:55

And her [pneuma](../../strongs/g/g4151.md) [epistrephō](../../strongs/g/g1994.md), and she [anistēmi](../../strongs/g/g450.md) [parachrēma](../../strongs/g/g3916.md): and he [diatassō](../../strongs/g/g1299.md) to [didōmi](../../strongs/g/g1325.md) her [phago](../../strongs/g/g5315.md).

<a name="luke_8_56"></a>Luke 8:56

And her [goneus](../../strongs/g/g1118.md) were [existēmi](../../strongs/g/g1839.md): but he [paraggellō](../../strongs/g/g3853.md) them that they should [eipon](../../strongs/g/g2036.md) [mēdeis](../../strongs/g/g3367.md) what was [ginomai](../../strongs/g/g1096.md).

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 7](luke_7.md) - [Luke 9](luke_9.md)