# [Luke 6](https://www.blueletterbible.org/kjv/luk/6/1/rl1/s_979001)

<a name="luke_6_1"></a>Luke 6:1

And [ginomai](../../strongs/g/g1096.md) on the second [sabbaton](../../strongs/g/g4521.md) after the first, that he [diaporeuomai](../../strongs/g/g1279.md) through the [sporimos](../../strongs/g/g4702.md); and his [mathētēs](../../strongs/g/g3101.md) [tillō](../../strongs/g/g5089.md) the [stachys](../../strongs/g/g4719.md), and did [esthiō](../../strongs/g/g2068.md), [psōchō](../../strongs/g/g5597.md) in [cheir](../../strongs/g/g5495.md).

<a name="luke_6_2"></a>Luke 6:2

And certain of the [Pharisaios](../../strongs/g/g5330.md) [eipon](../../strongs/g/g2036.md) unto them, Why [poieō](../../strongs/g/g4160.md) ye that which is not [exesti](../../strongs/g/g1832.md) to [poieō](../../strongs/g/g4160.md) on the [sabbaton](../../strongs/g/g4521.md)?

<a name="luke_6_3"></a>Luke 6:3

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) them [eipon](../../strongs/g/g2036.md), **Have ye not [anaginōskō](../../strongs/g/g314.md) so much as this, what [Dabid](../../strongs/g/g1138.md) [poieō](../../strongs/g/g4160.md), when himself was [peinaō](../../strongs/g/g3983.md), and they which were with him;**

<a name="luke_6_4"></a>Luke 6:4

**How he [eiserchomai](../../strongs/g/g1525.md) into the [oikos](../../strongs/g/g3624.md) of [theos](../../strongs/g/g2316.md), and did [lambanō](../../strongs/g/g2983.md) and [phago](../../strongs/g/g5315.md) the [artos](../../strongs/g/g740.md) [prothesis](../../strongs/g/g4286.md), and [didōmi](../../strongs/g/g1325.md) also to them that were with him; which it is not [exesti](../../strongs/g/g1832.md) to [phago](../../strongs/g/g5315.md) but for the [hiereus](../../strongs/g/g2409.md) alone?**

<a name="luke_6_5"></a>Luke 6:5

And he [legō](../../strongs/g/g3004.md) unto them, **That the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) is [kyrios](../../strongs/g/g2962.md) also of the [sabbaton](../../strongs/g/g4521.md).**

<a name="luke_6_6"></a>Luke 6:6

And [ginomai](../../strongs/g/g1096.md) also on another [sabbaton](../../strongs/g/g4521.md), that he [eiserchomai](../../strongs/g/g1525.md) into the [synagōgē](../../strongs/g/g4864.md) and [didaskō](../../strongs/g/g1321.md): and there was an [anthrōpos](../../strongs/g/g444.md) whose [dexios](../../strongs/g/g1188.md) [cheir](../../strongs/g/g5495.md) was [xēros](../../strongs/g/g3584.md).

<a name="luke_6_7"></a>Luke 6:7

And the [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md) [paratēreō](../../strongs/g/g3906.md) him, whether he would [therapeuō](../../strongs/g/g2323.md) on the [sabbaton](../../strongs/g/g4521.md); that they might [heuriskō](../../strongs/g/g2147.md) a [katēgoria](../../strongs/g/g2724.md) against him.

<a name="luke_6_8"></a>Luke 6:8

But he [eidō](../../strongs/g/g1492.md) their [dialogismos](../../strongs/g/g1261.md), and [eipon](../../strongs/g/g2036.md) to the [anthrōpos](../../strongs/g/g444.md) which had the [xēros](../../strongs/g/g3584.md) [cheir](../../strongs/g/g5495.md), **[egeirō](../../strongs/g/g1453.md), and [histēmi](../../strongs/g/g2476.md) in the midst.** And he [anistēmi](../../strongs/g/g450.md) and [histēmi](../../strongs/g/g2476.md).

<a name="luke_6_9"></a>Luke 6:9

Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) unto them, **I will [eperōtaō](../../strongs/g/g1905.md) you one thing; Is it [exesti](../../strongs/g/g1832.md) on the [sabbaton](../../strongs/g/g4521.md) to [agathopoieō](../../strongs/g/g15.md), or to [kakopoieō](../../strongs/g/g2554.md)? to [sōzō](../../strongs/g/g4982.md) [psychē](../../strongs/g/g5590.md), or to [apollymi](../../strongs/g/g622.md)?**

<a name="luke_6_10"></a>Luke 6:10

And [periblepō](../../strongs/g/g4017.md) upon them all, he [eipon](../../strongs/g/g2036.md) unto the [anthrōpos](../../strongs/g/g444.md), **[ekteinō](../../strongs/g/g1614.md) thy [cheir](../../strongs/g/g5495.md).** And he [poieō](../../strongs/g/g4160.md) so: and his [cheir](../../strongs/g/g5495.md) was [apokathistēmi](../../strongs/g/g600.md) [hygiēs](../../strongs/g/g5199.md) as the other.

<a name="luke_6_11"></a>Luke 6:11

And they were [pimplēmi](../../strongs/g/g4130.md) with [anoia](../../strongs/g/g454.md); and [dialaleō](../../strongs/g/g1255.md) with [allēlōn](../../strongs/g/g240.md) what they might [poieō](../../strongs/g/g4160.md) to [Iēsous](../../strongs/g/g2424.md).

<a name="luke_6_12"></a>Luke 6:12

And [ginomai](../../strongs/g/g1096.md) in those [hēmera](../../strongs/g/g2250.md), that he [exerchomai](../../strongs/g/g1831.md) into a [oros](../../strongs/g/g3735.md) to [proseuchomai](../../strongs/g/g4336.md), and [dianyktereuō](../../strongs/g/g1273.md) in [proseuchē](../../strongs/g/g4335.md) to [theos](../../strongs/g/g2316.md).

<a name="luke_6_13"></a>Luke 6:13

And when it was [hēmera](../../strongs/g/g2250.md), he [prosphōneō](../../strongs/g/g4377.md) his [mathētēs](../../strongs/g/g3101.md): and of them he [eklegomai](../../strongs/g/g1586.md) [dōdeka](../../strongs/g/g1427.md), whom also he [onomazō](../../strongs/g/g3687.md) [apostolos](../../strongs/g/g652.md);

<a name="luke_6_14"></a>Luke 6:14

[Simōn](../../strongs/g/g4613.md), (whom he also [onomazō](../../strongs/g/g3687.md) [Petros](../../strongs/g/g4074.md),) and [Andreas](../../strongs/g/g406.md) his [adelphos](../../strongs/g/g80.md), [Iakōbos](../../strongs/g/g2385.md) and [Iōannēs](../../strongs/g/g2491.md), [Philippos](../../strongs/g/g5376.md) and [Bartholomaios](../../strongs/g/g918.md),

<a name="luke_6_15"></a>Luke 6:15

[Maththaios](../../strongs/g/g3156.md) and [Thōmas](../../strongs/g/g2381.md), [Iakōbos](../../strongs/g/g2385.md) of [Alphaios](../../strongs/g/g256.md), and [Simōn](../../strongs/g/g4613.md) [kaleō](../../strongs/g/g2564.md) [Zēlōtēs](../../strongs/g/g2208.md),

<a name="luke_6_16"></a>Luke 6:16

And [Ioudas](../../strongs/g/g2455.md) of [Iakōbos](../../strongs/g/g2385.md), and [Ioudas](../../strongs/g/g2455.md) [Iskariōth](../../strongs/g/g2469.md), which also was the [prodotēs](../../strongs/g/g4273.md).

<a name="luke_6_17"></a>Luke 6:17

And he [katabainō](../../strongs/g/g2597.md) with them, and [histēmi](../../strongs/g/g2476.md) in the [pedinos](../../strongs/g/g3977.md) [topos](../../strongs/g/g5117.md), and the [ochlos](../../strongs/g/g3793.md) of his [mathētēs](../../strongs/g/g3101.md), and a [polys](../../strongs/g/g4183.md) [plēthos](../../strongs/g/g4128.md) of [laos](../../strongs/g/g2992.md) out of all [Ioudaia](../../strongs/g/g2449.md) and [Ierousalēm](../../strongs/g/g2419.md), and from the [paralios](../../strongs/g/g3882.md) of [Tyros](../../strongs/g/g5184.md) and [Sidōn](../../strongs/g/g4605.md), which [erchomai](../../strongs/g/g2064.md) to [akouō](../../strongs/g/g191.md) him, and to be [iaomai](../../strongs/g/g2390.md) of their [nosos](../../strongs/g/g3554.md);

<a name="luke_6_18"></a>Luke 6:18

And they that were [ochleō](../../strongs/g/g3791.md) with [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md): and they were [therapeuō](../../strongs/g/g2323.md).

<a name="luke_6_19"></a>Luke 6:19

And the [pas](../../strongs/g/g3956.md) [ochlos](../../strongs/g/g3793.md) [zēteō](../../strongs/g/g2212.md) to [haptomai](../../strongs/g/g680.md) him: for there [exerchomai](../../strongs/g/g1831.md) [dynamis](../../strongs/g/g1411.md) out of him, and [iaomai](../../strongs/g/g2390.md) all.

<a name="luke_6_20"></a>Luke 6:20

And he [epairō](../../strongs/g/g1869.md) his [ophthalmos](../../strongs/g/g3788.md) on his [mathētēs](../../strongs/g/g3101.md), and [legō](../../strongs/g/g3004.md), **[makarios](../../strongs/g/g3107.md) [ptōchos](../../strongs/g/g4434.md): for yours is the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).**

<a name="luke_6_21"></a>Luke 6:21

**[makarios](../../strongs/g/g3107.md) that [peinaō](../../strongs/g/g3983.md) now: for ye shall be [chortazō](../../strongs/g/g5526.md). [makarios](../../strongs/g/g3107.md) that [klaiō](../../strongs/g/g2799.md) now: for ye shall [gelaō](../../strongs/g/g1070.md).**

<a name="luke_6_22"></a>Luke 6:22

**[makarios](../../strongs/g/g3107.md) are ye, when [anthrōpos](../../strongs/g/g444.md) shall [miseō](../../strongs/g/g3404.md) you, and when they shall [aphorizō](../../strongs/g/g873.md) you from their company, and shall [oneidizō](../../strongs/g/g3679.md) you, and [ekballō](../../strongs/g/g1544.md) your [onoma](../../strongs/g/g3686.md) as [ponēros](../../strongs/g/g4190.md), for the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) sake.**

<a name="luke_6_23"></a>Luke 6:23

**[chairō](../../strongs/g/g5463.md) in that [hēmera](../../strongs/g/g2250.md), and [skirtaō](../../strongs/g/g4640.md): for, [idou](../../strongs/g/g2400.md), your [misthos](../../strongs/g/g3408.md) [polys](../../strongs/g/g4183.md) in [ouranos](../../strongs/g/g3772.md): for in [tauta](../../strongs/g/g5024.md) [poieō](../../strongs/g/g4160.md) their [patēr](../../strongs/g/g3962.md) unto the [prophētēs](../../strongs/g/g4396.md).**

<a name="luke_6_24"></a>Luke 6:24

**But [ouai](../../strongs/g/g3759.md) unto you that are [plousios](../../strongs/g/g4145.md)! for ye have [apechō](../../strongs/g/g568.md) your [paraklēsis](../../strongs/g/g3874.md).**

<a name="luke_6_25"></a>Luke 6:25

**[ouai](../../strongs/g/g3759.md) unto you that are [empi(m)plēmi](../../strongs/g/g1705.md)! for ye shall [peinaō](../../strongs/g/g3983.md). [ouai](../../strongs/g/g3759.md) unto you that [gelaō](../../strongs/g/g1070.md) now! for ye shall [pentheō](../../strongs/g/g3996.md) and [klaiō](../../strongs/g/g2799.md).**

<a name="luke_6_26"></a>Luke 6:26

**[ouai](../../strongs/g/g3759.md) unto you, when all [anthrōpos](../../strongs/g/g444.md) shall [eipon](../../strongs/g/g2036.md) [kalōs](../../strongs/g/g2573.md) of you! for so [poieō](../../strongs/g/g4160.md) their [patēr](../../strongs/g/g3962.md) to the [pseudoprophētēs](../../strongs/g/g5578.md).**

<a name="luke_6_27"></a>Luke 6:27

**But I [legō](../../strongs/g/g3004.md) unto you which [akouō](../../strongs/g/g191.md), [agapaō](../../strongs/g/g25.md) your [echthros](../../strongs/g/g2190.md), [poieō](../../strongs/g/g4160.md) [kalōs](../../strongs/g/g2573.md) to them which [miseō](../../strongs/g/g3404.md) you,**

<a name="luke_6_28"></a>Luke 6:28

**[eulogeō](../../strongs/g/g2127.md) them that [kataraomai](../../strongs/g/g2672.md) you, and [proseuchomai](../../strongs/g/g4336.md) for them which [epēreazō](../../strongs/g/g1908.md) you.**

<a name="luke_6_29"></a>Luke 6:29

**And unto him that [typtō](../../strongs/g/g5180.md) thee on the [siagōn](../../strongs/g/g4600.md) [parechō](../../strongs/g/g3930.md) also the other; and him that [airō](../../strongs/g/g142.md) thy [himation](../../strongs/g/g2440.md) [kōlyō](../../strongs/g/g2967.md) not to take [chitōn](../../strongs/g/g5509.md) also.**

<a name="luke_6_30"></a>Luke 6:30

**[didōmi](../../strongs/g/g1325.md) to [pas](../../strongs/g/g3956.md) that [aiteō](../../strongs/g/g154.md) of thee; and of him that [airō](../../strongs/g/g142.md) [sos](../../strongs/g/g4674.md) [apaiteō](../../strongs/g/g523.md) them not.**

<a name="luke_6_31"></a>Luke 6:31

**And as ye [thelō](../../strongs/g/g2309.md) that [anthrōpos](../../strongs/g/g444.md) should [poieō](../../strongs/g/g4160.md) to you, [poieō](../../strongs/g/g4160.md) ye also to them [homoiōs](../../strongs/g/g3668.md).**

<a name="luke_6_32"></a>Luke 6:32

**For if ye [agapaō](../../strongs/g/g25.md) them which [agapaō](../../strongs/g/g25.md) you, what [charis](../../strongs/g/g5485.md) have ye? for [hamartōlos](../../strongs/g/g268.md) also [agapaō](../../strongs/g/g25.md) those that [agapaō](../../strongs/g/g25.md) them.**

<a name="luke_6_33"></a>Luke 6:33

**And if ye [agathopoieō](../../strongs/g/g15.md) to them which [agathopoieō](../../strongs/g/g15.md) to you, what [charis](../../strongs/g/g5485.md) have ye? for [hamartōlos](../../strongs/g/g268.md) also [poieō](../../strongs/g/g4160.md) even the same.**

<a name="luke_6_34"></a>Luke 6:34

**And if ye [daneizō](../../strongs/g/g1155.md) of whom ye [elpizō](../../strongs/g/g1679.md) to [apolambanō](../../strongs/g/g618.md), what [charis](../../strongs/g/g5485.md) have ye? for [hamartōlos](../../strongs/g/g268.md) also [daneizō](../../strongs/g/g1155.md) to [hamartōlos](../../strongs/g/g268.md), to [apolambanō](../../strongs/g/g618.md) as much.**

<a name="luke_6_35"></a>Luke 6:35

**But [agapaō](../../strongs/g/g25.md) ye your [echthros](../../strongs/g/g2190.md), and [agathopoieō](../../strongs/g/g15.md), and [daneizō](../../strongs/g/g1155.md), [apelpizō](../../strongs/g/g560.md) for [mēdeis](../../strongs/g/g3367.md) again; and your [misthos](../../strongs/g/g3408.md) shall be [polys](../../strongs/g/g4183.md), and ye shall be the [huios](../../strongs/g/g5207.md) of the [hypsistos](../../strongs/g/g5310.md): for he is [chrēstos](../../strongs/g/g5543.md) unto the [acharistos](../../strongs/g/g884.md) and to the [ponēros](../../strongs/g/g4190.md).**

<a name="luke_6_36"></a>Luke 6:36

**Be ye therefore [oiktirmōn](../../strongs/g/g3629.md), as your [patēr](../../strongs/g/g3962.md) also is [oiktirmōn](../../strongs/g/g3629.md).**

<a name="luke_6_37"></a>Luke 6:37

**[krinō](../../strongs/g/g2919.md) not, and ye shall not be [krinō](../../strongs/g/g2919.md): [katadikazō](../../strongs/g/g2613.md) not, and ye shall not be [katadikazō](../../strongs/g/g2613.md): [apolyō](../../strongs/g/g630.md), and ye shall be [apolyō](../../strongs/g/g630.md):**

<a name="luke_6_38"></a>Luke 6:38

**[didōmi](../../strongs/g/g1325.md), and it shall be [didōmi](../../strongs/g/g1325.md) unto you; [kalos](../../strongs/g/g2570.md) [metron](../../strongs/g/g3358.md), [piezō](../../strongs/g/g4085.md), and [saleuō](../../strongs/g/g4531.md), and [hyperekchyn(n)ō](../../strongs/g/g5240.md), shall men [didōmi](../../strongs/g/g1325.md) into your [kolpos](../../strongs/g/g2859.md). For with the same [metron](../../strongs/g/g3358.md) that ye [metreō](../../strongs/g/g3354.md) it shall be [antimetreō](../../strongs/g/g488.md) to you.**

<a name="luke_6_39"></a>Luke 6:39

And he [eipon](../../strongs/g/g2036.md) a [parabolē](../../strongs/g/g3850.md) unto them, **Can the [typhlos](../../strongs/g/g5185.md) [hodēgeō](../../strongs/g/g3594.md) the [typhlos](../../strongs/g/g5185.md)? shall they not both [piptō](../../strongs/g/g4098.md) into the [bothynos](../../strongs/g/g999.md)?**

<a name="luke_6_40"></a>Luke 6:40

**The [mathētēs](../../strongs/g/g3101.md) is not [hyper](../../strongs/g/g5228.md) his [didaskalos](../../strongs/g/g1320.md): but every one that is [katartizō](../../strongs/g/g2675.md) shall be as his [didaskalos](../../strongs/g/g1320.md).**

<a name="luke_6_41"></a>Luke 6:41

**And why [blepō](../../strongs/g/g991.md) the [karphos](../../strongs/g/g2595.md) that is in thy [adelphos](../../strongs/g/g80.md) [ophthalmos](../../strongs/g/g3788.md), but [katanoeō](../../strongs/g/g2657.md) not the [dokos](../../strongs/g/g1385.md) that is in thine own [ophthalmos](../../strongs/g/g3788.md)?**

<a name="luke_6_42"></a>Luke 6:42

**Either how canst thou [legō](../../strongs/g/g3004.md) to thy [adelphos](../../strongs/g/g80.md), [adelphos](../../strongs/g/g80.md), [aphiēmi](../../strongs/g/g863.md) me [ekballō](../../strongs/g/g1544.md) the [karphos](../../strongs/g/g2595.md) that is in thine [ophthalmos](../../strongs/g/g3788.md), when thou thyself [blepō](../../strongs/g/g991.md) not the [dokos](../../strongs/g/g1385.md) that is in thine own [ophthalmos](../../strongs/g/g3788.md)? Thou [hypokritēs](../../strongs/g/g5273.md), [ekballō](../../strongs/g/g1544.md) first the [dokos](../../strongs/g/g1385.md) out of thine own [ophthalmos](../../strongs/g/g3788.md), and then shalt thou [diablepō](../../strongs/g/g1227.md) to [ekballō](../../strongs/g/g1544.md) the [karphos](../../strongs/g/g2595.md) that is in thy [adelphos](../../strongs/g/g80.md) [ophthalmos](../../strongs/g/g3788.md).**

<a name="luke_6_43"></a>Luke 6:43

**For a [kalos](../../strongs/g/g2570.md) [dendron](../../strongs/g/g1186.md) [poieō](../../strongs/g/g4160.md) not forth [sapros](../../strongs/g/g4550.md) [karpos](../../strongs/g/g2590.md); neither [poieō](../../strongs/g/g4160.md) a [sapros](../../strongs/g/g4550.md) [dendron](../../strongs/g/g1186.md) [poieō](../../strongs/g/g4160.md) [kalos](../../strongs/g/g2570.md) [karpos](../../strongs/g/g2590.md).**

<a name="luke_6_44"></a>Luke 6:44

**For every [dendron](../../strongs/g/g1186.md) is [ginōskō](../../strongs/g/g1097.md) by his own [karpos](../../strongs/g/g2590.md). For of [akantha](../../strongs/g/g173.md) [ou](../../strongs/g/g3756.md) [syllegō](../../strongs/g/g4816.md) [sykon](../../strongs/g/g4810.md)g942, nor of a [batos](../../strongs/g/g942.md) [trygaō](../../strongs/g/g5166.md) [staphylē](../../strongs/g/g4718.md).**

<a name="luke_6_45"></a>Luke 6:45

**An [agathos](../../strongs/g/g18.md) [anthrōpos](../../strongs/g/g444.md) out of the [agathos](../../strongs/g/g18.md) [thēsauros](../../strongs/g/g2344.md) of his [kardia](../../strongs/g/g2588.md) [propherō](../../strongs/g/g4393.md) [agathos](../../strongs/g/g18.md); and a [ponēros](../../strongs/g/g4190.md) [anthrōpos](../../strongs/g/g444.md) out of the [ponēros](../../strongs/g/g4190.md) [thēsauros](../../strongs/g/g2344.md) of his [kardia](../../strongs/g/g2588.md) [propherō](../../strongs/g/g4393.md) that which is [ponēros](../../strongs/g/g4190.md): for of the [perisseuma](../../strongs/g/g4051.md) of the [kardia](../../strongs/g/g2588.md) his [stoma](../../strongs/g/g4750.md) [laleō](../../strongs/g/g2980.md).**

<a name="luke_6_46"></a>Luke 6:46

**And why [kaleō](../../strongs/g/g2564.md) ye me, [kyrios](../../strongs/g/g2962.md), [kyrios](../../strongs/g/g2962.md), and [poieō](../../strongs/g/g4160.md) not the things which I [legō](../../strongs/g/g3004.md)?**

<a name="luke_6_47"></a>Luke 6:47

**Whosoever [erchomai](../../strongs/g/g2064.md) to me, and [akouō](../../strongs/g/g191.md) my [logos](../../strongs/g/g3056.md), and [poieō](../../strongs/g/g4160.md) them, I will [hypodeiknymi](../../strongs/g/g5263.md) you to whom he is [homoios](../../strongs/g/g3664.md):**

<a name="luke_6_48"></a>Luke 6:48

**He is [homoios](../../strongs/g/g3664.md) an [anthrōpos](../../strongs/g/g444.md) which [oikodomeō](../../strongs/g/g3618.md) an [oikia](../../strongs/g/g3614.md), and [skaptō](../../strongs/g/g4626.md) [bathynō](../../strongs/g/g900.md), and [tithēmi](../../strongs/g/g5087.md) the [themelios](../../strongs/g/g2310.md) on a [petra](../../strongs/g/g4073.md): and when the [plēmmyra](../../strongs/g/g4132.md) arose, the [potamos](../../strongs/g/g4215.md) [prosrēssō](../../strongs/g/g4366.md) upon that [oikia](../../strongs/g/g3614.md), and could not [saleuō](../../strongs/g/g4531.md) it: for it was [themelioō](../../strongs/g/g2311.md) upon a [petra](../../strongs/g/g4073.md).**

<a name="luke_6_49"></a>Luke 6:49

**But he that [akouō](../../strongs/g/g191.md), and [poieō](../../strongs/g/g4160.md) not, is [homoios](../../strongs/g/g3664.md) an [anthrōpos](../../strongs/g/g444.md) that without a [themelios](../../strongs/g/g2310.md) [oikodomeō](../../strongs/g/g3618.md) an [oikia](../../strongs/g/g3614.md) upon the [gē](../../strongs/g/g1093.md); against which the [potamos](../../strongs/g/g4215.md) [prosrēssō](../../strongs/g/g4366.md), and [eutheōs](../../strongs/g/g2112.md) it [piptō](../../strongs/g/g4098.md); and the [rhēgma](../../strongs/g/g4485.md) of that [oikia](../../strongs/g/g3614.md) was [megas](../../strongs/g/g3173.md).**

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke](luke_5.md) - [Luke](luke_7.md)