# [Luke 13](https://www.blueletterbible.org/kjv/luk/13/1/s_986001)

<a name="luke_13_1"></a>Luke 13:1

There were [pareimi](../../strongs/g/g3918.md) at that [kairos](../../strongs/g/g2540.md) some that [apaggellō](../../strongs/g/g518.md) him of the [Galilaios](../../strongs/g/g1057.md), whose [haima](../../strongs/g/g129.md) [Pilatos](../../strongs/g/g4091.md) had [mignymi](../../strongs/g/g3396.md) with their [thysia](../../strongs/g/g2378.md).

<a name="luke_13_2"></a>Luke 13:2

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto them, **[dokeō](../../strongs/g/g1380.md) ye that these [Galilaios](../../strongs/g/g1057.md) were [hamartōlos](../../strongs/g/g268.md) above all the [Galilaios](../../strongs/g/g1057.md), because they [paschō](../../strongs/g/g3958.md) such things?**

<a name="luke_13_3"></a>Luke 13:3

**I [legō](../../strongs/g/g3004.md) you, [ouchi](../../strongs/g/g3780.md): but, except ye [metanoeō](../../strongs/g/g3340.md), ye shall all [hōsautōs](../../strongs/g/g5615.md) [apollymi](../../strongs/g/g622.md).**

<a name="luke_13_4"></a>Luke 13:4

**Or those eighteen, upon whom the [pyrgos](../../strongs/g/g4444.md) in [Silōam](../../strongs/g/g4611.md) [piptō](../../strongs/g/g4098.md), and [apokteinō](../../strongs/g/g615.md) them, [dokeō](../../strongs/g/g1380.md) ye that they were [opheiletēs](../../strongs/g/g3781.md) above all [anthrōpos](../../strongs/g/g444.md) that [katoikeō](../../strongs/g/g2730.md) in [Ierousalēm](../../strongs/g/g2419.md)?**

<a name="luke_13_5"></a>Luke 13:5

**I [legō](../../strongs/g/g3004.md) you, [ouchi](../../strongs/g/g3780.md): but, except ye [metanoeō](../../strongs/g/g3340.md), ye shall all [homoiōs](../../strongs/g/g3668.md) [apollymi](../../strongs/g/g622.md).**

<a name="luke_13_6"></a>Luke 13:6

He [legō](../../strongs/g/g3004.md) also this [parabolē](../../strongs/g/g3850.md); **A certain had a [sykē](../../strongs/g/g4808.md) [phyteuō](../../strongs/g/g5452.md) in his [ampelōn](../../strongs/g/g290.md); and he [erchomai](../../strongs/g/g2064.md) and [zēteō](../../strongs/g/g2212.md) [karpos](../../strongs/g/g2590.md) thereon, and [heuriskō](../../strongs/g/g2147.md) none.**

<a name="luke_13_7"></a>Luke 13:7

**Then [eipon](../../strongs/g/g2036.md) he unto the [ampelourgos](../../strongs/g/g289.md), [idou](../../strongs/g/g2400.md), these three [etos](../../strongs/g/g2094.md) I [erchomai](../../strongs/g/g2064.md) [zēteō](../../strongs/g/g2212.md) [karpos](../../strongs/g/g2590.md) on this [sykē](../../strongs/g/g4808.md), and [heuriskō](../../strongs/g/g2147.md) none: [ekkoptō](../../strongs/g/g1581.md) it; why [katargeō](../../strongs/g/g2673.md) it the [gē](../../strongs/g/g1093.md)?**

<a name="luke_13_8"></a>Luke 13:8

**And he [apokrinomai](../../strongs/g/g611.md) [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), [aphiēmi](../../strongs/g/g863.md) it this [etos](../../strongs/g/g2094.md) also, till I shall [skaptō](../../strongs/g/g4626.md) about it, and [ballō](../../strongs/g/g906.md) [kopria](../../strongs/g/g2874.md):**

<a name="luke_13_9"></a>Luke 13:9

**And if it [poieō](../../strongs/g/g4160.md) [karpos](../../strongs/g/g2590.md): and if not, then after that thou shalt [ekkoptō](../../strongs/g/g1581.md) it.**

<a name="luke_13_10"></a>Luke 13:10

And he was [didaskō](../../strongs/g/g1321.md) in one of the [synagōgē](../../strongs/g/g4864.md) on the [sabbaton](../../strongs/g/g4521.md).

<a name="luke_13_11"></a>Luke 13:11

And, [idou](../../strongs/g/g2400.md), there was a [gynē](../../strongs/g/g1135.md) which had a [pneuma](../../strongs/g/g4151.md) of [astheneia](../../strongs/g/g769.md) eighteen [etos](../../strongs/g/g2094.md), and was [sygkyptō](../../strongs/g/g4794.md), and could in no [pantelēs](../../strongs/g/g3838.md) [anakyptō](../../strongs/g/g352.md).

<a name="luke_13_12"></a>Luke 13:12

And when [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) her, he [prosphōneō](../../strongs/g/g4377.md), and [eipon](../../strongs/g/g2036.md) unto her, **[gynē](../../strongs/g/g1135.md), thou art [apolyō](../../strongs/g/g630.md) from thine [astheneia](../../strongs/g/g769.md).**

<a name="luke_13_13"></a>Luke 13:13

And he [epitithēmi](../../strongs/g/g2007.md) [cheir](../../strongs/g/g5495.md) on her: and [parachrēma](../../strongs/g/g3916.md) she was [anorthoō](../../strongs/g/g461.md), and [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md).

<a name="luke_13_14"></a>Luke 13:14

And the [archisynagōgos](../../strongs/g/g752.md) [apokrinomai](../../strongs/g/g611.md) with [aganakteō](../../strongs/g/g23.md), because that [Iēsous](../../strongs/g/g2424.md) had [therapeuō](../../strongs/g/g2323.md) on the [sabbaton](../../strongs/g/g4521.md), and [legō](../../strongs/g/g3004.md) unto the [ochlos](../../strongs/g/g3793.md), There are six [hēmera](../../strongs/g/g2250.md) in which [dei](../../strongs/g/g1163.md) to [ergazomai](../../strongs/g/g2038.md): in them therefore [erchomai](../../strongs/g/g2064.md) and be [therapeuō](../../strongs/g/g2323.md), and not on the [sabbaton](../../strongs/g/g4521.md).

<a name="luke_13_15"></a>Luke 13:15

[kyrios](../../strongs/g/g2962.md) then [apokrinomai](../../strongs/g/g611.md) him, and [eipon](../../strongs/g/g2036.md), **[hypokritēs](../../strongs/g/g5273.md), doth not each one of you on the [sabbaton](../../strongs/g/g4521.md) [lyō](../../strongs/g/g3089.md) his [bous](../../strongs/g/g1016.md) or his [onos](../../strongs/g/g3688.md) from the [phatnē](../../strongs/g/g5336.md), and [apagō](../../strongs/g/g520.md) to [potizō](../../strongs/g/g4222.md)?**

<a name="luke_13_16"></a>Luke 13:16

**And ought not [tautē](../../strongs/g/g5026.md), being a [thygatēr](../../strongs/g/g2364.md) of [Abraam](../../strongs/g/g11.md), whom [Satanas](../../strongs/g/g4567.md) hath [deō](../../strongs/g/g1210.md), [idou](../../strongs/g/g2400.md), these eighteen [etos](../../strongs/g/g2094.md), be [lyō](../../strongs/g/g3089.md) from this [desmos](../../strongs/g/g1199.md) on the [sabbaton](../../strongs/g/g4521.md)?**

<a name="luke_13_17"></a>Luke 13:17

And when he had [legō](../../strongs/g/g3004.md) these things, all his [antikeimai](../../strongs/g/g480.md) were [kataischynō](../../strongs/g/g2617.md): and all the [ochlos](../../strongs/g/g3793.md) [chairō](../../strongs/g/g5463.md) for all the [endoxos](../../strongs/g/g1741.md) that were [ginomai](../../strongs/g/g1096.md) by him.

<a name="luke_13_18"></a>Luke 13:18

Then [legō](../../strongs/g/g3004.md) he, **Unto what is the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) [homoios](../../strongs/g/g3664.md)? and whereunto shall I [homoioō](../../strongs/g/g3666.md) it?**

<a name="luke_13_19"></a>Luke 13:19

**It is [homoios](../../strongs/g/g3664.md) a [kokkos](../../strongs/g/g2848.md) of [sinapi](../../strongs/g/g4615.md), which [anthrōpos](../../strongs/g/g444.md) [lambanō](../../strongs/g/g2983.md), and [ballō](../../strongs/g/g906.md) into his [kēpos](../../strongs/g/g2779.md); and it [auxanō](../../strongs/g/g837.md), and [ginomai](../../strongs/g/g1096.md) a [megas](../../strongs/g/g3173.md) [dendron](../../strongs/g/g1186.md); and the [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md) [kataskēnoō](../../strongs/g/g2681.md) in the [klados](../../strongs/g/g2798.md) of it.**

<a name="luke_13_20"></a>Luke 13:20

And again he [eipon](../../strongs/g/g2036.md), **Whereunto shall I [homoioō](../../strongs/g/g3666.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md)?**

<a name="luke_13_21"></a>Luke 13:21

**It is [homoios](../../strongs/g/g3664.md) [zymē](../../strongs/g/g2219.md), which a [gynē](../../strongs/g/g1135.md) [lambanō](../../strongs/g/g2983.md) and [egkryptō](../../strongs/g/g1470.md) in three [saton](../../strongs/g/g4568.md) of [aleuron](../../strongs/g/g224.md), till the [holos](../../strongs/g/g3650.md) was [zymoō](../../strongs/g/g2220.md).**

<a name="luke_13_22"></a>Luke 13:22

And he [diaporeuomai](../../strongs/g/g1279.md) through the [polis](../../strongs/g/g4172.md) and [kōmē](../../strongs/g/g2968.md), [didaskō](../../strongs/g/g1321.md), and [poieō](../../strongs/g/g4160.md) [poreia](../../strongs/g/g4197.md) toward [Ierousalēm](../../strongs/g/g2419.md).

<a name="luke_13_23"></a>Luke 13:23

Then [eipon](../../strongs/g/g2036.md) one unto him, [kyrios](../../strongs/g/g2962.md), are there [oligos](../../strongs/g/g3641.md) that be [sōzō](../../strongs/g/g4982.md)? And he [eipon](../../strongs/g/g2036.md) unto them,

<a name="luke_13_24"></a>Luke 13:24

**[agōnizomai](../../strongs/g/g75.md) to [eiserchomai](../../strongs/g/g1525.md) at the [stenos](../../strongs/g/g4728.md) [pylē](../../strongs/g/g4439.md): for [polys](../../strongs/g/g4183.md), I [legō](../../strongs/g/g3004.md) unto you, will [zēteō](../../strongs/g/g2212.md) to [eiserchomai](../../strongs/g/g1525.md), and [ischyō](../../strongs/g/g2480.md) not.**

<a name="luke_13_25"></a>Luke 13:25

**When once the [oikodespotēs](../../strongs/g/g3617.md) is [egeirō](../../strongs/g/g1453.md), and hath [apokleiō](../../strongs/g/g608.md) to the [thyra](../../strongs/g/g2374.md), and ye [archomai](../../strongs/g/g756.md) to [histēmi](../../strongs/g/g2476.md) [exō](../../strongs/g/g1854.md), and to [krouō](../../strongs/g/g2925.md) at the [thyra](../../strongs/g/g2374.md), [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), [kyrios](../../strongs/g/g2962.md), [anoigō](../../strongs/g/g455.md) unto us; and he shall [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2046.md) unto you, I [eidō](../../strongs/g/g1492.md) you not whence ye are:**

<a name="luke_13_26"></a>Luke 13:26

**Then shall ye [archomai](../../strongs/g/g756.md) to [legō](../../strongs/g/g3004.md), We have [phago](../../strongs/g/g5315.md) and [pinō](../../strongs/g/g4095.md) in thy [enōpion](../../strongs/g/g1799.md), and thou hast [didaskō](../../strongs/g/g1321.md) in our [plateia](../../strongs/g/g4113.md).**

<a name="luke_13_27"></a>Luke 13:27

**But he shall [eipon](../../strongs/g/g2046.md), I [legō](../../strongs/g/g3004.md) you, I [eidō](../../strongs/g/g1492.md) you not whence ye are; [aphistēmi](../../strongs/g/g868.md) from me, all ye [ergatēs](../../strongs/g/g2040.md) of [adikia](../../strongs/g/g93.md).**

<a name="luke_13_28"></a>Luke 13:28

**There shall be [klauthmos](../../strongs/g/g2805.md) and [brygmos](../../strongs/g/g1030.md) of [odous](../../strongs/g/g3599.md), when ye shall [optanomai](../../strongs/g/g3700.md) [Abraam](../../strongs/g/g11.md), and [Isaak](../../strongs/g/g2464.md), and [Iakōb](../../strongs/g/g2384.md), and all the [prophētēs](../../strongs/g/g4396.md), in the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md), and you yourselves [ekballō](../../strongs/g/g1544.md) out.**

<a name="luke_13_29"></a>Luke 13:29

**And they shall [hēkō](../../strongs/g/g2240.md) from the [anatolē](../../strongs/g/g395.md), and from the [dysmē](../../strongs/g/g1424.md), and from the [borras](../../strongs/g/g1005.md), and from the [notos](../../strongs/g/g3558.md), and shall [anaklinō](../../strongs/g/g347.md) in the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).**

<a name="luke_13_30"></a>Luke 13:30

**And, [idou](../../strongs/g/g2400.md), there are [eschatos](../../strongs/g/g2078.md) which shall be [prōtos](../../strongs/g/g4413.md), and there are [prōtos](../../strongs/g/g4413.md) which shall be [eschatos](../../strongs/g/g2078.md).**

<a name="luke_13_31"></a>Luke 13:31

The same [hēmera](../../strongs/g/g2250.md) there [proserchomai](../../strongs/g/g4334.md) certain of the [Pharisaios](../../strongs/g/g5330.md), [legō](../../strongs/g/g3004.md) unto him, [exerchomai](../../strongs/g/g1831.md), and [poreuō](../../strongs/g/g4198.md) hence: for [Hērōdēs](../../strongs/g/g2264.md) will [apokteinō](../../strongs/g/g615.md) thee.

<a name="luke_13_32"></a>Luke 13:32

And he [eipon](../../strongs/g/g2036.md) unto them, **[poreuō](../../strongs/g/g4198.md), and [eipon](../../strongs/g/g2036.md) that [alōpēx](../../strongs/g/g258.md), [idou](../../strongs/g/g2400.md), I [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md), and I [epiteleō](../../strongs/g/g2005.md) [iasis](../../strongs/g/g2392.md) [sēmeron](../../strongs/g/g4594.md) and [aurion](../../strongs/g/g839.md), and the third I shall [teleioō](../../strongs/g/g5048.md).**

<a name="luke_13_33"></a>Luke 13:33

**Nevertheless I must [poreuō](../../strongs/g/g4198.md) [sēmeron](../../strongs/g/g4594.md), and [aurion](../../strongs/g/g839.md), and [echō](../../strongs/g/g2192.md): for it cannot [endechomai](../../strongs/g/g1735.md) that a [prophētēs](../../strongs/g/g4396.md) [apollymi](../../strongs/g/g622.md) out of [Ierousalēm](../../strongs/g/g2419.md).**

<a name="luke_13_34"></a>Luke 13:34

**O [Ierousalēm](../../strongs/g/g2419.md), [Ierousalēm](../../strongs/g/g2419.md), which [apokteinō](../../strongs/g/g615.md) the [prophētēs](../../strongs/g/g4396.md), and [lithoboleō](../../strongs/g/g3036.md) them that are [apostellō](../../strongs/g/g649.md) unto thee; [posakis](../../strongs/g/g4212.md) would I have [episynagō](../../strongs/g/g1996.md) thy [teknon](../../strongs/g/g5043.md) together, as a [ornis](../../strongs/g/g3733.md) her [nossia](../../strongs/g/g3555.md) under her [pteryx](../../strongs/g/g4420.md), and ye would not!**

<a name="luke_13_35"></a>Luke 13:35

**[idou](../../strongs/g/g2400.md), your [oikos](../../strongs/g/g3624.md) is [aphiēmi](../../strongs/g/g863.md) unto you [erēmos](../../strongs/g/g2048.md): and [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, Ye shall not [eidō](../../strongs/g/g1492.md) me, until [hēkō](../../strongs/g/g2240.md) when ye shall [eipon](../../strongs/g/g2036.md), [eulogeō](../../strongs/g/g2127.md) is he that [erchomai](../../strongs/g/g2064.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md).**

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 12](luke_12.md) - [Luke 14](luke_14.md)