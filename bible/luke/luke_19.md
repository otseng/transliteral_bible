# [Luke 19](https://www.blueletterbible.org/kjv/luk/19/1/rl1/s_992001)

<a name="luke_19_1"></a>Luke 19:1

And [eiserchomai](../../strongs/g/g1525.md) and [dierchomai](../../strongs/g/g1330.md) [Ierichō](../../strongs/g/g2410.md).

<a name="luke_19_2"></a>Luke 19:2

And, [idou](../../strongs/g/g2400.md), an [anēr](../../strongs/g/g435.md) [onoma](../../strongs/g/g3686.md) [kaleō](../../strongs/g/g2564.md) [Zakchaios](../../strongs/g/g2195.md), which was [architelōnēs](../../strongs/g/g754.md), and he was [plousios](../../strongs/g/g4145.md).

<a name="luke_19_3"></a>Luke 19:3

And he [zēteō](../../strongs/g/g2212.md) to [eidō](../../strongs/g/g1492.md) [Iēsous](../../strongs/g/g2424.md) who he was; and could not for the [ochlos](../../strongs/g/g3793.md), because he was [mikros](../../strongs/g/g3398.md) of [hēlikia](../../strongs/g/g2244.md).

<a name="luke_19_4"></a>Luke 19:4

And he [protrechō](../../strongs/g/g4390.md) before, and [anabainō](../../strongs/g/g305.md) into a [sykomorea](../../strongs/g/g4809.md) to [eidō](../../strongs/g/g1492.md) him: for he was to [dierchomai](../../strongs/g/g1330.md) that.

<a name="luke_19_5"></a>Luke 19:5

And when [Iēsous](../../strongs/g/g2424.md) [erchomai](../../strongs/g/g2064.md) to the [topos](../../strongs/g/g5117.md), he [anablepō](../../strongs/g/g308.md), and [eidō](../../strongs/g/g1492.md) him, and [eipon](../../strongs/g/g2036.md) unto him, **[Zakchaios](../../strongs/g/g2195.md), [speudō](../../strongs/g/g4692.md), and [katabainō](../../strongs/g/g2597.md); for [sēmeron](../../strongs/g/g4594.md) I [dei](../../strongs/g/g1163.md) [menō](../../strongs/g/g3306.md) at thy [oikos](../../strongs/g/g3624.md).**

<a name="luke_19_6"></a>Luke 19:6

And he [speudō](../../strongs/g/g4692.md), and [katabainō](../../strongs/g/g2597.md), and [hypodechomai](../../strongs/g/g5264.md) him [chairō](../../strongs/g/g5463.md).

<a name="luke_19_7"></a>Luke 19:7

And when they [eidō](../../strongs/g/g1492.md), they all [diagoggyzō](../../strongs/g/g1234.md), [legō](../../strongs/g/g3004.md), That he was [eiserchomai](../../strongs/g/g1525.md) to [katalyō](../../strongs/g/g2647.md) with [anēr](../../strongs/g/g435.md) [hamartōlos](../../strongs/g/g268.md).

<a name="luke_19_8"></a>Luke 19:8

And [Zakchaios](../../strongs/g/g2195.md) [histēmi](../../strongs/g/g2476.md), and [eipon](../../strongs/g/g2036.md) unto the [kyrios](../../strongs/g/g2962.md): [idou](../../strongs/g/g2400.md), [kyrios](../../strongs/g/g2962.md), the [ēmisys](../../strongs/g/g2255.md) of my [hyparchonta](../../strongs/g/g5224.md) I [didōmi](../../strongs/g/g1325.md) to the [ptōchos](../../strongs/g/g4434.md); and if I have [sykophanteō](../../strongs/g/g4811.md) any thing from [tis](../../strongs/g/g5100.md), I [apodidōmi](../../strongs/g/g591.md) fourfold.

<a name="luke_19_9"></a>Luke 19:9

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **[sēmeron](../../strongs/g/g4594.md) is [sōtēria](../../strongs/g/g4991.md) [ginomai](../../strongs/g/g1096.md) to this [oikos](../../strongs/g/g3624.md), [kathoti](../../strongs/g/g2530.md) he also is a [huios](../../strongs/g/g5207.md) of [Abraam](../../strongs/g/g11.md).**

<a name="luke_19_10"></a>Luke 19:10

**For the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [erchomai](../../strongs/g/g2064.md) to [zēteō](../../strongs/g/g2212.md) and to [sōzō](../../strongs/g/g4982.md) [apollymi](../../strongs/g/g622.md).**

<a name="luke_19_11"></a>Luke 19:11

And as they [akouō](../../strongs/g/g191.md) these things, he [prostithēmi](../../strongs/g/g4369.md) and [eipon](../../strongs/g/g2036.md) a [parabolē](../../strongs/g/g3850.md), because he was [eggys](../../strongs/g/g1451.md) to [Ierousalēm](../../strongs/g/g2419.md), and because they [dokeō](../../strongs/g/g1380.md) that the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) should [parachrēma](../../strongs/g/g3916.md) [anaphainō](../../strongs/g/g398.md).

<a name="luke_19_12"></a>Luke 19:12

He [eipon](../../strongs/g/g2036.md) therefore, **A certain [anthrōpos](../../strongs/g/g444.md) [eugenēs](../../strongs/g/g2104.md) [poreuō](../../strongs/g/g4198.md) into a [makros](../../strongs/g/g3117.md) [chōra](../../strongs/g/g5561.md) to [lambanō](../../strongs/g/g2983.md) for himself a [basileia](../../strongs/g/g932.md), and to [hypostrephō](../../strongs/g/g5290.md).**

<a name="luke_19_13"></a>Luke 19:13

**And he [kaleō](../../strongs/g/g2564.md) his ten [doulos](../../strongs/g/g1401.md), and [didōmi](../../strongs/g/g1325.md) them ten [mna](../../strongs/g/g3414.md), and [eipon](../../strongs/g/g2036.md) unto them, [pragmateuomai](../../strongs/g/g4231.md) till I [erchomai](../../strongs/g/g2064.md).**

<a name="luke_19_14"></a>Luke 19:14

**But his [politēs](../../strongs/g/g4177.md) [miseō](../../strongs/g/g3404.md) him, and [apostellō](../../strongs/g/g649.md) a [presbeia](../../strongs/g/g4242.md) after him, [legō](../../strongs/g/g3004.md), We will not have this to [basileuō](../../strongs/g/g936.md) over us.**

<a name="luke_19_15"></a>Luke 19:15

**And [ginomai](../../strongs/g/g1096.md), that when he was [epanerchomai](../../strongs/g/g1880.md), having [lambanō](../../strongs/g/g2983.md) the [basileia](../../strongs/g/g932.md), then he [eipon](../../strongs/g/g2036.md) these [doulos](../../strongs/g/g1401.md) to be [phōneō](../../strongs/g/g5455.md) unto him, to whom he had [didōmi](../../strongs/g/g1325.md) the [argyrion](../../strongs/g/g694.md), that he might [ginōskō](../../strongs/g/g1097.md) how much every man had [diapragmateuomai](../../strongs/g/g1281.md).**

<a name="luke_19_16"></a>Luke 19:16

**Then [paraginomai](../../strongs/g/g3854.md) the first, [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), thy [mna](../../strongs/g/g3414.md) hath [prosergazomai](../../strongs/g/g4333.md) ten [mna](../../strongs/g/g3414.md).**

<a name="luke_19_17"></a>Luke 19:17

**And he [eipon](../../strongs/g/g2036.md) unto him, [eu](../../strongs/g/g2095.md), thou [agathos](../../strongs/g/g18.md) [doulos](../../strongs/g/g1401.md): because thou hast been [pistos](../../strongs/g/g4103.md) in [elachistos](../../strongs/g/g1646.md), have [isthi](../../strongs/g/g2468.md) [exousia](../../strongs/g/g1849.md) over ten [polis](../../strongs/g/g4172.md).**

<a name="luke_19_18"></a>Luke 19:18

**And the second [erchomai](../../strongs/g/g2064.md), [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), thy [mna](../../strongs/g/g3414.md) hath [poieō](../../strongs/g/g4160.md) five [mna](../../strongs/g/g3414.md).**

<a name="luke_19_19"></a>Luke 19:19

**And he [eipon](../../strongs/g/g2036.md) likewise to him, Be thou also over five [polis](../../strongs/g/g4172.md).**

<a name="luke_19_20"></a>Luke 19:20

**And another [erchomai](../../strongs/g/g2064.md), [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), [idou](../../strongs/g/g2400.md), thy [mna](../../strongs/g/g3414.md), which I have [apokeimai](../../strongs/g/g606.md) in a [soudarion](../../strongs/g/g4676.md):**

<a name="luke_19_21"></a>Luke 19:21

**For I [phobeō](../../strongs/g/g5399.md) thee, because thou art an [austēros](../../strongs/g/g840.md) [anthrōpos](../../strongs/g/g444.md): thou [airō](../../strongs/g/g142.md) that thou [tithēmi](../../strongs/g/g5087.md) not, and [therizō](../../strongs/g/g2325.md) that thou didst [speirō](../../strongs/g/g4687.md) not.**

<a name="luke_19_22"></a>Luke 19:22

**And he [legō](../../strongs/g/g3004.md) unto him, Out of thine own [stoma](../../strongs/g/g4750.md) will I [krinō](../../strongs/g/g2919.md) thee, thou [ponēros](../../strongs/g/g4190.md) [doulos](../../strongs/g/g1401.md). Thou [eidō](../../strongs/g/g1492.md) that I was an [austēros](../../strongs/g/g840.md) [anthrōpos](../../strongs/g/g444.md), [airō](../../strongs/g/g142.md) that I [tithēmi](../../strongs/g/g5087.md) not, and [therizō](../../strongs/g/g2325.md) that I did [speirō](../../strongs/g/g4687.md) not:**

<a name="luke_19_23"></a>Luke 19:23

**Wherefore then [didōmi](../../strongs/g/g1325.md) not thou my [argyrion](../../strongs/g/g694.md) into the [trapeza](../../strongs/g/g5132.md), that at my [erchomai](../../strongs/g/g2064.md) I might [prassō](../../strongs/g/g4238.md) mine own with [tokos](../../strongs/g/g5110.md)?**

<a name="luke_19_24"></a>Luke 19:24

**And he [eipon](../../strongs/g/g2036.md) unto [paristēmi](../../strongs/g/g3936.md), [airō](../../strongs/g/g142.md) from him the [mna](../../strongs/g/g3414.md), and [didōmi](../../strongs/g/g1325.md) to him that hath ten [mna](../../strongs/g/g3414.md).**

<a name="luke_19_25"></a>Luke 19:25

**(And they [eipon](../../strongs/g/g2036.md) unto him, [kyrios](../../strongs/g/g2962.md), he hath ten [mna](../../strongs/g/g3414.md).)**

<a name="luke_19_26"></a>Luke 19:26

**For I [legō](../../strongs/g/g3004.md) unto you, That unto every one which hath shall be [didōmi](../../strongs/g/g1325.md); and from him that hath not, even that he hath shall be [airō](../../strongs/g/g142.md) from him.**

<a name="luke_19_27"></a>Luke 19:27

**But those mine [echthros](../../strongs/g/g2190.md), which would not that I should [basileuō](../../strongs/g/g936.md) over them, [agō](../../strongs/g/g71.md) [hōde](../../strongs/g/g5602.md), and [katasphazō](../../strongs/g/g2695.md) before me.**

<a name="luke_19_28"></a>Luke 19:28

And when he had thus [eipon](../../strongs/g/g2036.md), he [poreuō](../../strongs/g/g4198.md) before, [anabainō](../../strongs/g/g305.md) to [Hierosolyma](../../strongs/g/g2414.md).

<a name="luke_19_29"></a>Luke 19:29

And [ginomai](../../strongs/g/g1096.md), when he was [eggizō](../../strongs/g/g1448.md) to [Bēthphagē](../../strongs/g/g967.md) and [Bēthania](../../strongs/g/g963.md), at the [oros](../../strongs/g/g3735.md) [kaleō](../../strongs/g/g2564.md) of [elaia](../../strongs/g/g1636.md), he [apostellō](../../strongs/g/g649.md) two of his [mathētēs](../../strongs/g/g3101.md),

<a name="luke_19_30"></a>Luke 19:30

[eipon](../../strongs/g/g2036.md), **[hypagō](../../strongs/g/g5217.md) ye into the [kōmē](../../strongs/g/g2968.md) [katenanti](../../strongs/g/g2713.md); in the which at your [eisporeuomai](../../strongs/g/g1531.md) ye shall [heuriskō](../../strongs/g/g2147.md) a [pōlos](../../strongs/g/g4454.md) [deō](../../strongs/g/g1210.md), whereon yet never [anthrōpos](../../strongs/g/g444.md) [kathizō](../../strongs/g/g2523.md) [pōpote](../../strongs/g/g4455.md): [lyō](../../strongs/g/g3089.md) him, and [agō](../../strongs/g/g71.md).**

<a name="luke_19_31"></a>Luke 19:31

**And if [tis](../../strongs/g/g5100.md) [erōtaō](../../strongs/g/g2065.md) you, Why do ye [lyō](../../strongs/g/g3089.md)? thus shall ye [eipon](../../strongs/g/g2046.md) unto him, Because the [kyrios](../../strongs/g/g2962.md) hath [chreia](../../strongs/g/g5532.md) of him.**

<a name="luke_19_32"></a>Luke 19:32

And [apostellō](../../strongs/g/g649.md) [aperchomai](../../strongs/g/g565.md), and [heuriskō](../../strongs/g/g2147.md) even as he had [eipon](../../strongs/g/g2036.md) unto them.

<a name="luke_19_33"></a>Luke 19:33

And as they were [lyō](../../strongs/g/g3089.md) the [pōlos](../../strongs/g/g4454.md), the [kyrios](../../strongs/g/g2962.md) thereof [eipon](../../strongs/g/g2036.md) unto them, Why [lyō](../../strongs/g/g3089.md) ye the [pōlos](../../strongs/g/g4454.md)?

<a name="luke_19_34"></a>Luke 19:34

And they [eipon](../../strongs/g/g2036.md), The [kyrios](../../strongs/g/g2962.md) hath [chreia](../../strongs/g/g5532.md) of him.

<a name="luke_19_35"></a>Luke 19:35

And they [agō](../../strongs/g/g71.md) him to [Iēsous](../../strongs/g/g2424.md): and they [epiriptō](../../strongs/g/g1977.md) their [himation](../../strongs/g/g2440.md) upon the [pōlos](../../strongs/g/g4454.md), and they [epibibazō](../../strongs/g/g1913.md) [Iēsous](../../strongs/g/g2424.md).

<a name="luke_19_36"></a>Luke 19:36

And as he [poreuō](../../strongs/g/g4198.md), they [hypostrōnnyō](../../strongs/g/g5291.md) their [himation](../../strongs/g/g2440.md) in the [hodos](../../strongs/g/g3598.md).

<a name="luke_19_37"></a>Luke 19:37

And when he was [eggizō](../../strongs/g/g1448.md), even now at the [katabasis](../../strongs/g/g2600.md) of the [oros](../../strongs/g/g3735.md) of [elaia](../../strongs/g/g1636.md), the [hapas](../../strongs/g/g537.md) [plēthos](../../strongs/g/g4128.md) of the [mathētēs](../../strongs/g/g3101.md) [archomai](../../strongs/g/g756.md) to [chairō](../../strongs/g/g5463.md) and [aineō](../../strongs/g/g134.md) [theos](../../strongs/g/g2316.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md) for all the [dynamis](../../strongs/g/g1411.md) that they had [eidō](../../strongs/g/g1492.md);

<a name="luke_19_38"></a>Luke 19:38

[legō](../../strongs/g/g3004.md), [eulogeō](../../strongs/g/g2127.md) the [basileus](../../strongs/g/g935.md) that [erchomai](../../strongs/g/g2064.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md): [eirēnē](../../strongs/g/g1515.md) in [ouranos](../../strongs/g/g3772.md), and [doxa](../../strongs/g/g1391.md) in the [hypsistos](../../strongs/g/g5310.md).

<a name="luke_19_39"></a>Luke 19:39

And some of the [Pharisaios](../../strongs/g/g5330.md) from [ochlos](../../strongs/g/g3793.md) [eipon](../../strongs/g/g2036.md) unto him, [didaskalos](../../strongs/g/g1320.md), [epitimaō](../../strongs/g/g2008.md) thy [mathētēs](../../strongs/g/g3101.md).

<a name="luke_19_40"></a>Luke 19:40

And he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **I [legō](../../strongs/g/g3004.md) you that, if these should [siōpaō](../../strongs/g/g4623.md), the [lithos](../../strongs/g/g3037.md) would [krazō](../../strongs/g/g2896.md).**

<a name="luke_19_41"></a>Luke 19:41

And when he was [eggizō](../../strongs/g/g1448.md), he [eidō](../../strongs/g/g1492.md) the [polis](../../strongs/g/g4172.md), and [klaiō](../../strongs/g/g2799.md) over it,

<a name="luke_19_42"></a>Luke 19:42

[legō](../../strongs/g/g3004.md), **If thou hadst [ginōskō](../../strongs/g/g1097.md), even thou, at [kai ge](../../strongs/g/g2534.md) in this thy [hēmera](../../strongs/g/g2250.md), the things belong unto thy [eirēnē](../../strongs/g/g1515.md)! but now they are [kryptō](../../strongs/g/g2928.md) from thine [ophthalmos](../../strongs/g/g3788.md).**

<a name="luke_19_43"></a>Luke 19:43

**For the [hēmera](../../strongs/g/g2250.md) shall [hēkō](../../strongs/g/g2240.md) upon thee, that thine [echthros](../../strongs/g/g2190.md) shall [periballō](../../strongs/g/g4016.md) a [charax](../../strongs/g/g5482.md) about thee, and [perikykloō](../../strongs/g/g4033.md) thee, and [synechō](../../strongs/g/g4912.md) thee [pantothen](../../strongs/g/g3840.md),**

<a name="luke_19_44"></a>Luke 19:44

**And shall [edaphizō](../../strongs/g/g1474.md) thee, and thy [teknon](../../strongs/g/g5043.md) within thee; and they shall not [aphiēmi](../../strongs/g/g863.md) in thee [lithos](../../strongs/g/g3037.md) upon another; because thou [ginōskō](../../strongs/g/g1097.md) not the [kairos](../../strongs/g/g2540.md) of thy [episkopē](../../strongs/g/g1984.md).**

<a name="luke_19_45"></a>Luke 19:45

And he [eiserchomai](../../strongs/g/g1525.md) into the [hieron](../../strongs/g/g2411.md), and [archomai](../../strongs/g/g756.md) to [ekballō](../../strongs/g/g1544.md) them that [pōleō](../../strongs/g/g4453.md) therein, and them that [agorazō](../../strongs/g/g59.md);

<a name="luke_19_46"></a>Luke 19:46

[legō](../../strongs/g/g3004.md) unto them, **It is [graphō](../../strongs/g/g1125.md), My [oikos](../../strongs/g/g3624.md) is the [oikos](../../strongs/g/g3624.md) of [proseuchē](../../strongs/g/g4335.md): but ye have [poieō](../../strongs/g/g4160.md) it a [spēlaion](../../strongs/g/g4693.md) of [lēstēs](../../strongs/g/g3027.md).**

<a name="luke_19_47"></a>Luke 19:47

And he [didaskō](../../strongs/g/g1321.md) [kata](../../strongs/g/g2596.md) [hēmera](../../strongs/g/g2250.md) in the [hieron](../../strongs/g/g2411.md). But the [archiereus](../../strongs/g/g749.md) and the [grammateus](../../strongs/g/g1122.md) and the [prōtos](../../strongs/g/g4413.md) of the [laos](../../strongs/g/g2992.md) [zēteō](../../strongs/g/g2212.md) to [apollymi](../../strongs/g/g622.md) him,

<a name="luke_19_48"></a>Luke 19:48

And [heuriskō](../../strongs/g/g2147.md) not what they might [poieō](../../strongs/g/g4160.md): for all the [laos](../../strongs/g/g2992.md) were [ekkremamai](../../strongs/g/g1582.md) to [akouō](../../strongs/g/g191.md) him.

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 18](luke_18.md) - [Luke 20](luke_20.md)