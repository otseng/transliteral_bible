# [Luke 10](https://www.blueletterbible.org/kjv/luk/10/1/rl1/s_983001)

<a name="luke_10_1"></a>Luke 10:1

After these things the [kyrios](../../strongs/g/g2962.md) [anadeiknymi](../../strongs/g/g322.md) other seventy also, and [apostellō](../../strongs/g/g649.md) them two and two before his [prosōpon](../../strongs/g/g4383.md) into every [polis](../../strongs/g/g4172.md) and [topos](../../strongs/g/g5117.md), whither he himself would [erchomai](../../strongs/g/g2064.md).

<a name="luke_10_2"></a>Luke 10:2

Therefore [legō](../../strongs/g/g3004.md) he unto them, **The [therismos](../../strongs/g/g2326.md) truly [polys](../../strongs/g/g4183.md), but the [ergatēs](../../strongs/g/g2040.md) [oligos](../../strongs/g/g3641.md): [deomai](../../strongs/g/g1189.md) therefore the [kyrios](../../strongs/g/g2962.md) of the [therismos](../../strongs/g/g2326.md), that he would [ekballō](../../strongs/g/g1544.md) [ergatēs](../../strongs/g/g2040.md) into his [therismos](../../strongs/g/g2326.md).**

<a name="luke_10_3"></a>Luke 10:3

**[hypagō](../../strongs/g/g5217.md): [idou](../../strongs/g/g2400.md), I [apostellō](../../strongs/g/g649.md) you as [arēn](../../strongs/g/g704.md) among [lykos](../../strongs/g/g3074.md).**

<a name="luke_10_4"></a>Luke 10:4

**[bastazō](../../strongs/g/g941.md) neither [ballantion](../../strongs/g/g905.md), nor [pēra](../../strongs/g/g4082.md), nor [hypodēma](../../strongs/g/g5266.md): and [aspazomai](../../strongs/g/g782.md) [mēdeis](../../strongs/g/g3367.md) by the [hodos](../../strongs/g/g3598.md).**

<a name="luke_10_5"></a>Luke 10:5

**And into whatsoever [oikia](../../strongs/g/g3614.md) ye [eiserchomai](../../strongs/g/g1525.md), first [legō](../../strongs/g/g3004.md), [eirēnē](../../strongs/g/g1515.md) to this [oikos](../../strongs/g/g3624.md).**

<a name="luke_10_6"></a>Luke 10:6

**And if the [huios](../../strongs/g/g5207.md) of [eirēnē](../../strongs/g/g1515.md) be there, your [eirēnē](../../strongs/g/g1515.md) shall [epanapauomai](../../strongs/g/g1879.md) upon it: if not, it shall [anakamptō](../../strongs/g/g344.md) to you.**

<a name="luke_10_7"></a>Luke 10:7

**And in the same [oikia](../../strongs/g/g3614.md) [menō](../../strongs/g/g3306.md), [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md) such things as they give: for the [ergatēs](../../strongs/g/g2040.md) is [axios](../../strongs/g/g514.md) of his [misthos](../../strongs/g/g3408.md). [metabainō](../../strongs/g/g3327.md) not from [oikia](../../strongs/g/g3614.md) to [oikia](../../strongs/g/g3614.md).**

<a name="luke_10_8"></a>Luke 10:8

**And into whatsoever [polis](../../strongs/g/g4172.md) ye [eiserchomai](../../strongs/g/g1525.md), and they [dechomai](../../strongs/g/g1209.md) you, [esthiō](../../strongs/g/g2068.md) [paratithēmi](../../strongs/g/g3908.md) you:**

<a name="luke_10_9"></a>Luke 10:9

**And [therapeuō](../../strongs/g/g2323.md) the [asthenēs](../../strongs/g/g772.md) that are therein, and [legō](../../strongs/g/g3004.md) unto them, The [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) [eggizō](../../strongs/g/g1448.md) unto you.**

<a name="luke_10_10"></a>Luke 10:10

**But into whatsoever [polis](../../strongs/g/g4172.md) ye [eiserchomai](../../strongs/g/g1525.md), and they [dechomai](../../strongs/g/g1209.md) you not, [exerchomai](../../strongs/g/g1831.md) into the [plateia](../../strongs/g/g4113.md) of the same, and [eipon](../../strongs/g/g2036.md),**

<a name="luke_10_11"></a>Luke 10:11

**Even the [koniortos](../../strongs/g/g2868.md) of your [polis](../../strongs/g/g4172.md), which [kollaō](../../strongs/g/g2853.md) on us, we do [apomassō](../../strongs/g/g631.md) against you: notwithstanding be ye [ginōskō](../../strongs/g/g1097.md) of this, that the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) [eggizō](../../strongs/g/g1448.md) unto you.**

<a name="luke_10_12"></a>Luke 10:12

**But I [legō](../../strongs/g/g3004.md) unto you, that it shall be [anektos](../../strongs/g/g414.md) in that [hēmera](../../strongs/g/g2250.md) for [Sodoma](../../strongs/g/g4670.md), than for that [polis](../../strongs/g/g4172.md).**

<a name="luke_10_13"></a>Luke 10:13

**[ouai](../../strongs/g/g3759.md) unto thee, [Chorazin](../../strongs/g/g5523.md)! [ouai](../../strongs/g/g3759.md) unto thee, [Bēthsaïda](../../strongs/g/g966.md)! for if the [dynamis](../../strongs/g/g1411.md) had been [ginomai](../../strongs/g/g1096.md) in [Tyros](../../strongs/g/g5184.md) and [Sidōn](../../strongs/g/g4605.md), which have been [ginomai](../../strongs/g/g1096.md) in you, they had [palai](../../strongs/g/g3819.md) [metanoeō](../../strongs/g/g3340.md), [kathēmai](../../strongs/g/g2521.md) in [sakkos](../../strongs/g/g4526.md) and [spodos](../../strongs/g/g4700.md).**

<a name="luke_10_14"></a>Luke 10:14

**But it shall be [anektos](../../strongs/g/g414.md) for [Tyros](../../strongs/g/g5184.md) and [Sidōn](../../strongs/g/g4605.md) at the [krisis](../../strongs/g/g2920.md), than for you.**

<a name="luke_10_15"></a>Luke 10:15

**And thou, [Kapharnaoum](../../strongs/g/g2584.md), which [hypsoō](../../strongs/g/g5312.md) to [ouranos](../../strongs/g/g3772.md), shalt be [katabibazō](../../strongs/g/g2601.md) to [hadēs](../../strongs/g/g86.md).**

<a name="luke_10_16"></a>Luke 10:16

**He that [akouō](../../strongs/g/g191.md) you [akouō](../../strongs/g/g191.md) me; and he that [atheteō](../../strongs/g/g114.md) you [atheteō](../../strongs/g/g114.md) me; and he that [atheteō](../../strongs/g/g114.md) me [atheteō](../../strongs/g/g114.md) him that [apostellō](../../strongs/g/g649.md) me.**

<a name="luke_10_17"></a>Luke 10:17

And the seventy [hypostrephō](../../strongs/g/g5290.md) with [chara](../../strongs/g/g5479.md), [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), even the [daimonion](../../strongs/g/g1140.md) are [hypotassō](../../strongs/g/g5293.md) unto us through thy [onoma](../../strongs/g/g3686.md).

<a name="luke_10_18"></a>Luke 10:18

And he [eipon](../../strongs/g/g2036.md) unto them, **I [theōreō](../../strongs/g/g2334.md) [Satanas](../../strongs/g/g4567.md) as [astrapē](../../strongs/g/g796.md) [piptō](../../strongs/g/g4098.md) from [ouranos](../../strongs/g/g3772.md).**

<a name="luke_10_19"></a>Luke 10:19

**[idou](../../strongs/g/g2400.md), I [didōmi](../../strongs/g/g1325.md) unto you [exousia](../../strongs/g/g1849.md) to [pateō](../../strongs/g/g3961.md) on [ophis](../../strongs/g/g3789.md) and [skorpios](../../strongs/g/g4651.md), and over all the [dynamis](../../strongs/g/g1411.md) of the [echthros](../../strongs/g/g2190.md): and nothing shall by any means [adikeō](../../strongs/g/g91.md) you.**

<a name="luke_10_20"></a>Luke 10:20

**[plēn](../../strongs/g/g4133.md) in this [chairō](../../strongs/g/g5463.md) not, that the [pneuma](../../strongs/g/g4151.md) are [hypotassō](../../strongs/g/g5293.md) unto you; but rather [chairō](../../strongs/g/g5463.md), because your [onoma](../../strongs/g/g3686.md) are [graphō](../../strongs/g/g1125.md) in [ouranos](../../strongs/g/g3772.md).**

<a name="luke_10_21"></a>Luke 10:21

In that [hōra](../../strongs/g/g5610.md) [Iēsous](../../strongs/g/g2424.md) [agalliaō](../../strongs/g/g21.md) in [pneuma](../../strongs/g/g4151.md), and [eipon](../../strongs/g/g2036.md), **I [exomologeō](../../strongs/g/g1843.md) thee, O [patēr](../../strongs/g/g3962.md), [kyrios](../../strongs/g/g2962.md) of [ouranos](../../strongs/g/g3772.md) and [gē](../../strongs/g/g1093.md), that thou hast [apokryptō](../../strongs/g/g613.md) these things from the [sophos](../../strongs/g/g4680.md) and [synetos](../../strongs/g/g4908.md), and hast [apokalyptō](../../strongs/g/g601.md) them unto [nēpios](../../strongs/g/g3516.md): [nai](../../strongs/g/g3483.md), [patēr](../../strongs/g/g3962.md); for so it seemed [eudokia](../../strongs/g/g2107.md) [emprosthen](../../strongs/g/g1715.md).**

<a name="luke_10_22"></a>Luke 10:22

**All things are [paradidōmi](../../strongs/g/g3860.md) to me of my [patēr](../../strongs/g/g3962.md): and [oudeis](../../strongs/g/g3762.md) [ginōskō](../../strongs/g/g1097.md) who the [huios](../../strongs/g/g5207.md) is, but the [patēr](../../strongs/g/g3962.md); and who the [patēr](../../strongs/g/g3962.md) is, but the [huios](../../strongs/g/g5207.md), and he to whom the [huios](../../strongs/g/g5207.md) [boulomai](../../strongs/g/g1014.md) [apokalyptō](../../strongs/g/g601.md) him.**

<a name="luke_10_23"></a>Luke 10:23

And he [strephō](../../strongs/g/g4762.md) him unto [mathētēs](../../strongs/g/g3101.md), and [eipon](../../strongs/g/g2036.md) [idios](../../strongs/g/g2398.md), **[makarios](../../strongs/g/g3107.md) are the [ophthalmos](../../strongs/g/g3788.md) which [blepō](../../strongs/g/g991.md) the things that ye [blepō](../../strongs/g/g991.md):**

<a name="luke_10_24"></a>Luke 10:24

**For I [legō](../../strongs/g/g3004.md) you, that [polys](../../strongs/g/g4183.md) [prophētēs](../../strongs/g/g4396.md) and [basileus](../../strongs/g/g935.md) have [thelō](../../strongs/g/g2309.md) to [eidō](../../strongs/g/g1492.md) those things which ye [blepō](../../strongs/g/g991.md), and have not [eidō](../../strongs/g/g1492.md); and to [akouō](../../strongs/g/g191.md) those things which ye [akouō](../../strongs/g/g191.md), and have not [akouō](../../strongs/g/g191.md).**

<a name="luke_10_25"></a>Luke 10:25

And, [idou](../../strongs/g/g2400.md), a certain [nomikos](../../strongs/g/g3544.md) [anistēmi](../../strongs/g/g450.md), and [ekpeirazō](../../strongs/g/g1598.md) him, [legō](../../strongs/g/g3004.md), [didaskalos](../../strongs/g/g1320.md), what shall I [poieō](../../strongs/g/g4160.md) to [klēronomeō](../../strongs/g/g2816.md) [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md)?

<a name="luke_10_26"></a>Luke 10:26

He [eipon](../../strongs/g/g2036.md) unto him, **What is [graphō](../../strongs/g/g1125.md) in the [nomos](../../strongs/g/g3551.md)? how [anaginōskō](../../strongs/g/g314.md) thou?**

<a name="luke_10_27"></a>Luke 10:27

And he [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md), Thou shalt [agapaō](../../strongs/g/g25.md) the [kyrios](../../strongs/g/g2962.md) thy [theos](../../strongs/g/g2316.md) with all thy [kardia](../../strongs/g/g2588.md), and with all thy [psychē](../../strongs/g/g5590.md), and with all thy [ischys](../../strongs/g/g2479.md), and with all thy [dianoia](../../strongs/g/g1271.md); and thy [plēsion](../../strongs/g/g4139.md) as thyself.

<a name="luke_10_28"></a>Luke 10:28

And he [eipon](../../strongs/g/g2036.md) unto him, **Thou hast [apokrinomai](../../strongs/g/g611.md) [orthōs](../../strongs/g/g3723.md): this [poieō](../../strongs/g/g4160.md), and thou shalt [zaō](../../strongs/g/g2198.md).**

<a name="luke_10_29"></a>Luke 10:29

But he, willing to [dikaioō](../../strongs/g/g1344.md) himself, [eipon](../../strongs/g/g2036.md) unto [Iēsous](../../strongs/g/g2424.md), And who is my [plēsion](../../strongs/g/g4139.md)?

<a name="luke_10_30"></a>Luke 10:30

And [Iēsous](../../strongs/g/g2424.md) [hypolambanō](../../strongs/g/g5274.md) [eipon](../../strongs/g/g2036.md), **A certain [anthrōpos](../../strongs/g/g444.md) [katabainō](../../strongs/g/g2597.md) from [Ierousalēm](../../strongs/g/g2419.md) to [Ierichō](../../strongs/g/g2410.md), and [peripiptō](../../strongs/g/g4045.md) [lēstēs](../../strongs/g/g3027.md), which [ekdyō](../../strongs/g/g1562.md) him of his [ekdyō](../../strongs/g/g1562.md), and [plēgē](../../strongs/g/g4127.md) [epitithēmi](../../strongs/g/g2007.md), and [aperchomai](../../strongs/g/g565.md), [aphiēmi](../../strongs/g/g863.md) [hēmithanēs](../../strongs/g/g2253.md) [tygchanō](../../strongs/g/g5177.md).** [^1]

<a name="luke_10_31"></a>Luke 10:31

**And by [sygkyria](../../strongs/g/g4795.md) there [katabainō](../../strongs/g/g2597.md) a certain [hiereus](../../strongs/g/g2409.md) that [hodos](../../strongs/g/g3598.md): and when he [eidō](../../strongs/g/g1492.md) him, he [antiparerchomai](../../strongs/g/g492.md).**

<a name="luke_10_32"></a>Luke 10:32

**And [homoiōs](../../strongs/g/g3668.md) a [Leuitēs](../../strongs/g/g3019.md), when he was at the [topos](../../strongs/g/g5117.md), [erchomai](../../strongs/g/g2064.md) and [eidō](../../strongs/g/g1492.md), and [antiparerchomai](../../strongs/g/g492.md).**

<a name="luke_10_33"></a>Luke 10:33

**But a certain [Samaritēs](../../strongs/g/g4541.md), as he [hodeuō](../../strongs/g/g3593.md), [erchomai](../../strongs/g/g2064.md) where he was: and when he [eidō](../../strongs/g/g1492.md) him, he had [splagchnizomai](../../strongs/g/g4697.md) on him,**

<a name="luke_10_34"></a>Luke 10:34

**And [proserchomai](../../strongs/g/g4334.md) to him, and [katadeō](../../strongs/g/g2611.md) his [trauma](../../strongs/g/g5134.md), [epicheō](../../strongs/g/g2022.md) in [elaion](../../strongs/g/g1637.md) and [oinos](../../strongs/g/g3631.md), and [epibibazō](../../strongs/g/g1913.md) him on his own [ktēnos](../../strongs/g/g2934.md), and [agō](../../strongs/g/g71.md) him to [pandocheion](../../strongs/g/g3829.md), and [epimeleomai](../../strongs/g/g1959.md) him.**

<a name="luke_10_35"></a>Luke 10:35

**And on the [aurion](../../strongs/g/g839.md) when he [exerchomai](../../strongs/g/g1831.md), he [ekballō](../../strongs/g/g1544.md) two [dēnarion](../../strongs/g/g1220.md), and [didōmi](../../strongs/g/g1325.md) to the [pandocheus](../../strongs/g/g3830.md), and [eipon](../../strongs/g/g2036.md) unto him, [epimeleomai](../../strongs/g/g1959.md) him; and whatsoever thou [prosdapanaō](../../strongs/g/g4325.md), when I [epanerchomai](../../strongs/g/g1880.md), I will [apodidōmi](../../strongs/g/g591.md) thee.**

<a name="luke_10_36"></a>Luke 10:36

**Which now of these three, [dokeō](../../strongs/g/g1380.md) thou, was [plēsion](../../strongs/g/g4139.md) unto him that [empiptō](../../strongs/g/g1706.md) among the [lēstēs](../../strongs/g/g3027.md)?**

<a name="luke_10_37"></a>Luke 10:37

And he [eipon](../../strongs/g/g2036.md), He that [poieō](../../strongs/g/g4160.md) [eleos](../../strongs/g/g1656.md) on him. Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) unto him, **[poreuō](../../strongs/g/g4198.md), and do thou [homoiōs](../../strongs/g/g3668.md).**

<a name="luke_10_38"></a>Luke 10:38

Now [ginomai](../../strongs/g/g1096.md), as they [poreuō](../../strongs/g/g4198.md), that he [eiserchomai](../../strongs/g/g1525.md) into a certain [kōmē](../../strongs/g/g2968.md): and a certain [gynē](../../strongs/g/g1135.md) [onoma](../../strongs/g/g3686.md) [Martha](../../strongs/g/g3136.md) [hypodechomai](../../strongs/g/g5264.md) him into her [oikos](../../strongs/g/g3624.md).

<a name="luke_10_39"></a>Luke 10:39

And she had an [adelphē](../../strongs/g/g79.md) [kaleō](../../strongs/g/g2564.md) [Maria](../../strongs/g/g3137.md), which also [parakathezomai](../../strongs/g/g3869.md) at [Iēsous](../../strongs/g/g2424.md) [pous](../../strongs/g/g4228.md), and [akouō](../../strongs/g/g191.md) his [logos](../../strongs/g/g3056.md).

<a name="luke_10_40"></a>Luke 10:40

But [Martha](../../strongs/g/g3136.md) was [perispaō](../../strongs/g/g4049.md) about [polys](../../strongs/g/g4183.md) [diakonia](../../strongs/g/g1248.md), and [ephistēmi](../../strongs/g/g2186.md) to him, and [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), dost thou not [melei](../../strongs/g/g3199.md) that my [adelphē](../../strongs/g/g79.md) hath [kataleipō](../../strongs/g/g2641.md) me to [diakoneō](../../strongs/g/g1247.md) alone? [eipon](../../strongs/g/g2036.md) her therefore that she [synantilambanomai](../../strongs/g/g4878.md) me.

<a name="luke_10_41"></a>Luke 10:41

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto her, **[Martha](../../strongs/g/g3136.md), [Martha](../../strongs/g/g3136.md), thou art [merimnaō](../../strongs/g/g3309.md) and [thorybazō](../../strongs/g/g5182.md) about [polys](../../strongs/g/g4183.md):**

<a name="luke_10_42"></a>Luke 10:42

**But one thing is [chreia](../../strongs/g/g5532.md): and [Maria](../../strongs/g/g3137.md) hath [eklegomai](../../strongs/g/g1586.md) that [agathos](../../strongs/g/g18.md) [meris](../../strongs/g/g3310.md), which shall not be [aphaireō](../../strongs/g/g851.md) from her.**

---

[^1]: [Luke 10:30 Commentary](../../commentary/luke/luke_10_commentary.md#luke_10_30)

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 9](luke_9.md) - [Luke 11](luke_11.md)