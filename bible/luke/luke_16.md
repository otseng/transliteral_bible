# [Luke 16](https://www.blueletterbible.org/kjv/luk/16/26/s_989001)

<a name="luke_16_1"></a>Luke 16:1

And he [legō](../../strongs/g/g3004.md) also unto his [mathētēs](../../strongs/g/g3101.md), **There was a certain [plousios](../../strongs/g/g4145.md) [anthrōpos](../../strongs/g/g444.md), which had an [oikonomos](../../strongs/g/g3623.md); and the same was [diaballō](../../strongs/g/g1225.md) unto him that he had [diaskorpizō](../../strongs/g/g1287.md) his [hyparchonta](../../strongs/g/g5224.md).**

<a name="luke_16_2"></a>Luke 16:2

**And he [phōneō](../../strongs/g/g5455.md) him, and [eipon](../../strongs/g/g2036.md) unto him, How is it that I [akouō](../../strongs/g/g191.md) this of thee? [apodidōmi](../../strongs/g/g591.md) a [logos](../../strongs/g/g3056.md) of thy [oikonomia](../../strongs/g/g3622.md); for thou mayest be no longer [oikonomeō](../../strongs/g/g3621.md).**

<a name="luke_16_3"></a>Luke 16:3

**Then the [oikonomos](../../strongs/g/g3623.md) [eipon](../../strongs/g/g2036.md) within himself, What shall I [poieō](../../strongs/g/g4160.md)? for my [kyrios](../../strongs/g/g2962.md) [aphaireō](../../strongs/g/g851.md) from me the [oikonomia](../../strongs/g/g3622.md): I cannot [skaptō](../../strongs/g/g4626.md); to [epaiteō](../../strongs/g/g1871.md) I [aischynō](../../strongs/g/g153.md).**

<a name="luke_16_4"></a>Luke 16:4

**I [ginōskō](../../strongs/g/g1097.md) what to [poieō](../../strongs/g/g4160.md), that, when I am [methistēmi](../../strongs/g/g3179.md) of the [oikonomia](../../strongs/g/g3622.md), they may [dechomai](../../strongs/g/g1209.md) me into their [oikos](../../strongs/g/g3624.md).**

<a name="luke_16_5"></a>Luke 16:5

**So he [proskaleō](../../strongs/g/g4341.md) every one of his [kyrios](../../strongs/g/g2962.md) [chreopheiletēs](../../strongs/g/g5533.md), and [legō](../../strongs/g/g3004.md) unto the first, How much [opheilō](../../strongs/g/g3784.md) thou unto my [kyrios](../../strongs/g/g2962.md)?**

<a name="luke_16_6"></a>Luke 16:6

**And he [eipon](../../strongs/g/g2036.md), An hundred [batos](../../strongs/g/g943.md) of [elaion](../../strongs/g/g1637.md). And he [eipon](../../strongs/g/g2036.md) unto him, [dechomai](../../strongs/g/g1209.md) thy [gramma](../../strongs/g/g1121.md), and [kathizō](../../strongs/g/g2523.md) [tacheōs](../../strongs/g/g5030.md), and [graphō](../../strongs/g/g1125.md) fifty.**

<a name="luke_16_7"></a>Luke 16:7

**Then [eipon](../../strongs/g/g2036.md) he to another, And how much [opheilō](../../strongs/g/g3784.md) thou? And he [eipon](../../strongs/g/g2036.md), An hundred [koros](../../strongs/g/g2884.md) of [sitos](../../strongs/g/g4621.md). And he [legō](../../strongs/g/g3004.md) unto him, [dechomai](../../strongs/g/g1209.md) thy [gramma](../../strongs/g/g1121.md), and [graphō](../../strongs/g/g1125.md) fourscore.**

<a name="luke_16_8"></a>Luke 16:8

**And the [kyrios](../../strongs/g/g2962.md) [epaineō](../../strongs/g/g1867.md) the [adikia](../../strongs/g/g93.md) [oikonomos](../../strongs/g/g3623.md), because he had [poieō](../../strongs/g/g4160.md) [phronimōs](../../strongs/g/g5430.md): for the [huios](../../strongs/g/g5207.md) of this [aiōn](../../strongs/g/g165.md) are in their [genea](../../strongs/g/g1074.md) [phronimos](../../strongs/g/g5429.md) than the [huios](../../strongs/g/g5207.md) of [phōs](../../strongs/g/g5457.md).**

<a name="luke_16_9"></a>Luke 16:9

**And I [legō](../../strongs/g/g3004.md) unto you, [poieō](../../strongs/g/g4160.md) to yourselves [philos](../../strongs/g/g5384.md) of the [mamōnas](../../strongs/g/g3126.md) of [adikia](../../strongs/g/g93.md); that, when ye [ekleipō](../../strongs/g/g1587.md), they may [dechomai](../../strongs/g/g1209.md) you into [aiōnios](../../strongs/g/g166.md) [skēnē](../../strongs/g/g4633.md).**

<a name="luke_16_10"></a>Luke 16:10

**[pistos](../../strongs/g/g4103.md) in [elachistos](../../strongs/g/g1646.md) is [pistos](../../strongs/g/g4103.md) also in [polys](../../strongs/g/g4183.md): and [adikos](../../strongs/g/g94.md) in [elachistos](../../strongs/g/g1646.md) is [adikos](../../strongs/g/g94.md) also in [polys](../../strongs/g/g4183.md).**

<a name="luke_16_11"></a>Luke 16:11

**If therefore ye have not been [pistos](../../strongs/g/g4103.md) in the [adikos](../../strongs/g/g94.md) [mamōnas](../../strongs/g/g3126.md), who will [pisteuō](../../strongs/g/g4100.md) to you the [alēthinos](../../strongs/g/g228.md)?**

<a name="luke_16_12"></a>Luke 16:12

**And if ye have not been [pistos](../../strongs/g/g4103.md) in [allotrios](../../strongs/g/g245.md), who shall [didōmi](../../strongs/g/g1325.md) you [hymeteros](../../strongs/g/g5212.md)?**

<a name="luke_16_13"></a>Luke 16:13

**No [oiketēs](../../strongs/g/g3610.md) can [douleuō](../../strongs/g/g1398.md) two [kyrios](../../strongs/g/g2962.md): for either he will [miseō](../../strongs/g/g3404.md) the one, and [agapaō](../../strongs/g/g25.md) the other; or else he will [antechō](../../strongs/g/g472.md) to the one, and [kataphroneō](../../strongs/g/g2706.md) the other. Ye cannot [douleuō](../../strongs/g/g1398.md) [theos](../../strongs/g/g2316.md) and [mamōnas](../../strongs/g/g3126.md).**

<a name="luke_16_14"></a>Luke 16:14

And the [Pharisaios](../../strongs/g/g5330.md) also, who were [philargyros](../../strongs/g/g5366.md), [akouō](../../strongs/g/g191.md) all these things: and they [ekmyktērizō](../../strongs/g/g1592.md) him.

<a name="luke_16_15"></a>Luke 16:15

And he [eipon](../../strongs/g/g2036.md) unto them, **Ye are they which [dikaioō](../../strongs/g/g1344.md) yourselves before [anthrōpos](../../strongs/g/g444.md); but [theos](../../strongs/g/g2316.md) [ginōskō](../../strongs/g/g1097.md) your [kardia](../../strongs/g/g2588.md): for [hypsēlos](../../strongs/g/g5308.md) among [anthrōpos](../../strongs/g/g444.md) is [bdelygma](../../strongs/g/g946.md) in the [enōpion](../../strongs/g/g1799.md) of [theos](../../strongs/g/g2316.md).**

<a name="luke_16_16"></a>Luke 16:16

**The [nomos](../../strongs/g/g3551.md) and the [prophētēs](../../strongs/g/g4396.md) were until [Iōannēs](../../strongs/g/g2491.md): since [tote](../../strongs/g/g5119.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) is [euaggelizō](../../strongs/g/g2097.md), and [pas](../../strongs/g/g3956.md) [biazō](../../strongs/g/g971.md) into it.**

<a name="luke_16_17"></a>Luke 16:17

**And it is [eukopos](../../strongs/g/g2123.md) for [ouranos](../../strongs/g/g3772.md) and [gē](../../strongs/g/g1093.md) to [parerchomai](../../strongs/g/g3928.md), than one [keraia](../../strongs/g/g2762.md) of the [nomos](../../strongs/g/g3551.md) to [piptō](../../strongs/g/g4098.md).**

<a name="luke_16_18"></a>Luke 16:18

**Whosoever [apolyō](../../strongs/g/g630.md) his [gynē](../../strongs/g/g1135.md), and [gameō](../../strongs/g/g1060.md) another, [moicheuō](../../strongs/g/g3431.md): and whosoever [gameō](../../strongs/g/g1060.md) [apolyō](../../strongs/g/g630.md) from [anēr](../../strongs/g/g435.md) [moicheuō](../../strongs/g/g3431.md).**

<a name="luke_16_19"></a>Luke 16:19

**There was a certain [plousios](../../strongs/g/g4145.md) [anthrōpos](../../strongs/g/g444.md), which was [endidyskō](../../strongs/g/g1737.md) in [porphyra](../../strongs/g/g4209.md) and [byssos](../../strongs/g/g1040.md), and [euphrainō](../../strongs/g/g2165.md) [lamprōs](../../strongs/g/g2988.md) every [hēmera](../../strongs/g/g2250.md):**

<a name="luke_16_20"></a>Luke 16:20

**And there was a certain [ptōchos](../../strongs/g/g4434.md) [onoma](../../strongs/g/g3686.md) [Lazaros](../../strongs/g/g2976.md), which was [ballō](../../strongs/g/g906.md) at his [pylōn](../../strongs/g/g4440.md), [helkoō](../../strongs/g/g1669.md),**

<a name="luke_16_21"></a>Luke 16:21

**And [epithymeō](../../strongs/g/g1937.md) to be [chortazō](../../strongs/g/g5526.md) with the [psichion](../../strongs/g/g5589.md) which [piptō](../../strongs/g/g4098.md) from the [plousios](../../strongs/g/g4145.md) [trapeza](../../strongs/g/g5132.md): moreover the [kyōn](../../strongs/g/g2965.md) [erchomai](../../strongs/g/g2064.md) and [apoleichō](../../strongs/g/g621.md) his [helkos](../../strongs/g/g1668.md).**

<a name="luke_16_22"></a>Luke 16:22

**And [ginomai](../../strongs/g/g1096.md), that the [ptōchos](../../strongs/g/g4434.md) [apothnēskō](../../strongs/g/g599.md), and was [apopherō](../../strongs/g/g667.md) by the [aggelos](../../strongs/g/g32.md) into [Abraam](../../strongs/g/g11.md) [kolpos](../../strongs/g/g2859.md): the [plousios](../../strongs/g/g4145.md) also [apothnēskō](../../strongs/g/g599.md), and was [thaptō](../../strongs/g/g2290.md);**

<a name="luke_16_23"></a>Luke 16:23

**And in [hadēs](../../strongs/g/g86.md) he [epairō](../../strongs/g/g1869.md) his [ophthalmos](../../strongs/g/g3788.md), being in [basanos](../../strongs/g/g931.md), and [horaō](../../strongs/g/g3708.md) [Abraam](../../strongs/g/g11.md) [makrothen](../../strongs/g/g3113.md) off, and [Lazaros](../../strongs/g/g2976.md) in his [kolpos](../../strongs/g/g2859.md).**

<a name="luke_16_24"></a>Luke 16:24

**And he [phōneō](../../strongs/g/g5455.md) and [eipon](../../strongs/g/g2036.md), [patēr](../../strongs/g/g3962.md) [Abraam](../../strongs/g/g11.md), have [eleeō](../../strongs/g/g1653.md) on me, and [pempō](../../strongs/g/g3992.md) [Lazaros](../../strongs/g/g2976.md), that he may [baptō](../../strongs/g/g911.md) the [akron](../../strongs/g/g206.md) of his [daktylos](../../strongs/g/g1147.md) in [hydōr](../../strongs/g/g5204.md), and [katapsychō](../../strongs/g/g2711.md) my [glōssa](../../strongs/g/g1100.md); for I am [odynaō](../../strongs/g/g3600.md) in this [phlox](../../strongs/g/g5395.md).**

<a name="luke_16_25"></a>Luke 16:25

**But [Abraam](../../strongs/g/g11.md) [eipon](../../strongs/g/g2036.md), [teknon](../../strongs/g/g5043.md), [mnaomai](../../strongs/g/g3415.md) that thou in thy [zōē](../../strongs/g/g2222.md) [apolambanō](../../strongs/g/g618.md) thy [agathos](../../strongs/g/g18.md), and [homoiōs](../../strongs/g/g3668.md) [Lazaros](../../strongs/g/g2976.md) [kakos](../../strongs/g/g2556.md): but now he is [parakaleō](../../strongs/g/g3870.md), and thou art [odynaō](../../strongs/g/g3600.md).**

<a name="luke_16_26"></a>Luke 16:26

**And beside all this, [metaxy](../../strongs/g/g3342.md) us and you there is a [megas](../../strongs/g/g3173.md) [chasma](../../strongs/g/g5490.md) [stērizō](../../strongs/g/g4741.md): so that they which would [diabainō](../../strongs/g/g1224.md) from hence to you cannot; neither can they [diaperaō](../../strongs/g/g1276.md) to us, [ekeithen](../../strongs/g/g1564.md).**

<a name="luke_16_27"></a>Luke 16:27

**Then he [eipon](../../strongs/g/g2036.md), I [erōtaō](../../strongs/g/g2065.md) thee therefore, [patēr](../../strongs/g/g3962.md), that thou wouldest [pempō](../../strongs/g/g3992.md) him to my [patēr](../../strongs/g/g3962.md) [oikos](../../strongs/g/g3624.md):**

<a name="luke_16_28"></a>Luke 16:28

**For I have five [adelphos](../../strongs/g/g80.md); that he may [diamartyromai](../../strongs/g/g1263.md) unto them, lest they also [erchomai](../../strongs/g/g2064.md) into this [topos](../../strongs/g/g5117.md) of [basanos](../../strongs/g/g931.md).**

<a name="luke_16_29"></a>Luke 16:29

**[Abraam](../../strongs/g/g11.md) [legō](../../strongs/g/g3004.md) unto him, They have [Mōÿsēs](../../strongs/g/g3475.md) and the [prophētēs](../../strongs/g/g4396.md); let them [akouō](../../strongs/g/g191.md) them.**

<a name="luke_16_30"></a>Luke 16:30

**And he [eipon](../../strongs/g/g2036.md), [ouchi](../../strongs/g/g3780.md), [patēr](../../strongs/g/g3962.md) [Abraam](../../strongs/g/g11.md): but if one [poreuō](../../strongs/g/g4198.md) unto them from the [nekros](../../strongs/g/g3498.md), they will [metanoeō](../../strongs/g/g3340.md).**

<a name="luke_16_31"></a>Luke 16:31

**And he [eipon](../../strongs/g/g2036.md) unto him, If they [akouō](../../strongs/g/g191.md) not [Mōÿsēs](../../strongs/g/g3475.md) and the [prophētēs](../../strongs/g/g4396.md), neither will they [peithō](../../strongs/g/g3982.md), though one [anistēmi](../../strongs/g/g450.md) from the [nekros](../../strongs/g/g3498.md).**

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 15](luke_15.md) - [Luke 17](luke_17.md)