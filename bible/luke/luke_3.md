# [Luke 3](https://www.blueletterbible.org/kjv/luk/3/1/rl1/s_976001)

<a name="luke_3_1"></a>Luke 3:1

Now in the fifteenth [etos](../../strongs/g/g2094.md) of the [hēgemonia](../../strongs/g/g2231.md) of [Tiberios](../../strongs/g/g5086.md) [Kaisar](../../strongs/g/g2541.md), [Pontios](../../strongs/g/g4194.md) [Pilatos](../../strongs/g/g4091.md) being [hēgemoneuō](../../strongs/g/g2230.md) of [Ioudaia](../../strongs/g/g2449.md), and [Hērōdēs](../../strongs/g/g2264.md) being tetrarch of [Galilaia](../../strongs/g/g1056.md), and his [adelphos](../../strongs/g/g80.md) [Philippos](../../strongs/g/g5376.md) [tetraarcheō](../../strongs/g/g5075.md) of [itouraios](../../strongs/g/g2484.md) and of the [chōra](../../strongs/g/g5561.md) of [Trachōnitis](../../strongs/g/g5139.md), and [Lysanias](../../strongs/g/g3078.md) the [tetraarcheō](../../strongs/g/g5075.md) of [Abilēnē](../../strongs/g/g9.md),

<a name="luke_3_2"></a>Luke 3:2

[Annas](../../strongs/g/g452.md) and [Kaïaphas](../../strongs/g/g2533.md) being the [archiereus](../../strongs/g/g749.md), the [rhēma](../../strongs/g/g4487.md) of [theos](../../strongs/g/g2316.md) [ginomai](../../strongs/g/g1096.md) unto [Iōannēs](../../strongs/g/g2491.md) the [huios](../../strongs/g/g5207.md) of [Zacharias](../../strongs/g/g2197.md) in the [erēmos](../../strongs/g/g2048.md).

<a name="luke_3_3"></a>Luke 3:3

And he [erchomai](../../strongs/g/g2064.md) into all the [perichōros](../../strongs/g/g4066.md) [Iordanēs](../../strongs/g/g2446.md), [kēryssō](../../strongs/g/g2784.md) the [baptisma](../../strongs/g/g908.md) of [metanoia](../../strongs/g/g3341.md) for the [aphesis](../../strongs/g/g859.md) of [hamartia](../../strongs/g/g266.md);

<a name="luke_3_4"></a>Luke 3:4

As it is [graphō](../../strongs/g/g1125.md) in the [biblos](../../strongs/g/g976.md) of the [logos](../../strongs/g/g3056.md) of [Ēsaïas](../../strongs/g/g2268.md) the [prophētēs](../../strongs/g/g4396.md), [legō](../../strongs/g/g3004.md), The [phōnē](../../strongs/g/g5456.md) of [boaō](../../strongs/g/g994.md) in the [erēmos](../../strongs/g/g2048.md), [hetoimazō](../../strongs/g/g2090.md) ye [hodos](../../strongs/g/g3598.md) of the [kyrios](../../strongs/g/g2962.md), [poieō](../../strongs/g/g4160.md) his [tribos](../../strongs/g/g5147.md) [euthys](../../strongs/g/g2117.md).

<a name="luke_3_5"></a>Luke 3:5

Every [pharagx](../../strongs/g/g5327.md) shall be [plēroō](../../strongs/g/g4137.md), and every [oros](../../strongs/g/g3735.md) and [bounos](../../strongs/g/g1015.md) shall be [tapeinoō](../../strongs/g/g5013.md); and the [skolios](../../strongs/g/g4646.md) shall be made [euthys](../../strongs/g/g2117.md), and the [trachys](../../strongs/g/g5138.md) [hodos](../../strongs/g/g3598.md) shall be made [leios](../../strongs/g/g3006.md);

<a name="luke_3_6"></a>Luke 3:6

And all [sarx](../../strongs/g/g4561.md) shall [optanomai](../../strongs/g/g3700.md) the [sōtērios](../../strongs/g/g4992.md) of [theos](../../strongs/g/g2316.md).

<a name="luke_3_7"></a>Luke 3:7

Then [legō](../../strongs/g/g3004.md) he to the [ochlos](../../strongs/g/g3793.md) that [ekporeuomai](../../strongs/g/g1607.md) to be [baptizō](../../strongs/g/g907.md) of him, [gennēma](../../strongs/g/g1081.md) of [echidna](../../strongs/g/g2191.md), who hath [hypodeiknymi](../../strongs/g/g5263.md) you to [pheugō](../../strongs/g/g5343.md) from the [orgē](../../strongs/g/g3709.md) to [mellō](../../strongs/g/g3195.md)?

<a name="luke_3_8"></a>Luke 3:8

[poieō](../../strongs/g/g4160.md) therefore [karpos](../../strongs/g/g2590.md) [axios](../../strongs/g/g514.md) of [metanoia](../../strongs/g/g3341.md), and [archomai](../../strongs/g/g756.md) not to [legō](../../strongs/g/g3004.md) within yourselves, We have [Abraam](../../strongs/g/g11.md) to our [patēr](../../strongs/g/g3962.md): for I [legō](../../strongs/g/g3004.md) unto you, That [theos](../../strongs/g/g2316.md) is able of these [lithos](../../strongs/g/g3037.md) to [egeirō](../../strongs/g/g1453.md) [teknon](../../strongs/g/g5043.md) unto [Abraam](../../strongs/g/g11.md).

<a name="luke_3_9"></a>Luke 3:9

And now also the [axinē](../../strongs/g/g513.md) is [keimai](../../strongs/g/g2749.md) unto the [rhiza](../../strongs/g/g4491.md) of the [dendron](../../strongs/g/g1186.md): every [dendron](../../strongs/g/g1186.md) therefore which [poieō](../../strongs/g/g4160.md) not [kalos](../../strongs/g/g2570.md) [karpos](../../strongs/g/g2590.md) is [ekkoptō](../../strongs/g/g1581.md), and [ballō](../../strongs/g/g906.md) into the [pyr](../../strongs/g/g4442.md).

<a name="luke_3_10"></a>Luke 3:10

And the [ochlos](../../strongs/g/g3793.md) [eperōtaō](../../strongs/g/g1905.md) him, [legō](../../strongs/g/g3004.md), What shall we [poieō](../../strongs/g/g4160.md) then?

<a name="luke_3_11"></a>Luke 3:11

He [apokrinomai](../../strongs/g/g611.md) and [legō](../../strongs/g/g3004.md) unto them, He that hath two [chitōn](../../strongs/g/g5509.md), let him [metadidōmi](../../strongs/g/g3330.md) to him that hath none; and he that hath [brōma](../../strongs/g/g1033.md), let him [poieō](../../strongs/g/g4160.md) [homoiōs](../../strongs/g/g3668.md).

<a name="luke_3_12"></a>Luke 3:12

Then [erchomai](../../strongs/g/g2064.md) also [telōnēs](../../strongs/g/g5057.md) to be [baptizō](../../strongs/g/g907.md), and [eipon](../../strongs/g/g2036.md) unto him, [didaskalos](../../strongs/g/g1320.md), what shall we [poieō](../../strongs/g/g4160.md)?

<a name="luke_3_13"></a>Luke 3:13

And he [eipon](../../strongs/g/g2036.md) unto them, [prassō](../../strongs/g/g4238.md) [mēdeis](../../strongs/g/g3367.md) more than that which is [diatassō](../../strongs/g/g1299.md) you.

<a name="luke_3_14"></a>Luke 3:14

And the [strateuō](../../strongs/g/g4754.md) likewise [eperōtaō](../../strongs/g/g1905.md) of him, [legō](../../strongs/g/g3004.md), And what shall we [poieō](../../strongs/g/g4160.md)? And he [eipon](../../strongs/g/g2036.md) unto them, [diaseiō](../../strongs/g/g1286.md) to [mēdeis](../../strongs/g/g3367.md), neither [sykophanteō](../../strongs/g/g4811.md); and be [arkeō](../../strongs/g/g714.md) with your [opsōnion](../../strongs/g/g3800.md).

<a name="luke_3_15"></a>Luke 3:15

And as the [laos](../../strongs/g/g2992.md) were in [prosdokaō](../../strongs/g/g4328.md), and [pas](../../strongs/g/g3956.md) [dialogizomai](../../strongs/g/g1260.md) in their [kardia](../../strongs/g/g2588.md) of [Iōannēs](../../strongs/g/g2491.md), whether he were the [Christos](../../strongs/g/g5547.md), or not;

<a name="luke_3_16"></a>Luke 3:16

[Iōannēs](../../strongs/g/g2491.md) [apokrinomai](../../strongs/g/g611.md), [legō](../../strongs/g/g3004.md) unto all, I indeed [baptizō](../../strongs/g/g907.md) you with [hydōr](../../strongs/g/g5204.md); but one [ischyros](../../strongs/g/g2478.md) than I [erchomai](../../strongs/g/g2064.md), the [himas](../../strongs/g/g2438.md) of whose [hypodēma](../../strongs/g/g5266.md) I am not [hikanos](../../strongs/g/g2425.md) to [lyō](../../strongs/g/g3089.md): he shall [baptizō](../../strongs/g/g907.md) you with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) and with [pyr](../../strongs/g/g4442.md):

<a name="luke_3_17"></a>Luke 3:17

Whose [ptyon](../../strongs/g/g4425.md) in his [cheir](../../strongs/g/g5495.md), and he will [diakatharizō](../../strongs/g/g1245.md) his [halōn](../../strongs/g/g257.md), and will [synagō](../../strongs/g/g4863.md) the [sitos](../../strongs/g/g4621.md) into his [apothēkē](../../strongs/g/g596.md); but the [achyron](../../strongs/g/g892.md) he will [katakaiō](../../strongs/g/g2618.md) with [pyr](../../strongs/g/g4442.md) [asbestos](../../strongs/g/g762.md).

<a name="luke_3_18"></a>Luke 3:18

And [polys](../../strongs/g/g4183.md) [heteros](../../strongs/g/g2087.md) [men](../../strongs/g/g3303.md) in his [parakaleō](../../strongs/g/g3870.md) [euaggelizō](../../strongs/g/g2097.md) he unto the [laos](../../strongs/g/g2992.md).

<a name="luke_3_19"></a>Luke 3:19

But [Hērōdēs](../../strongs/g/g2264.md) the [tetraarchēs](../../strongs/g/g5076.md), being [elegchō](../../strongs/g/g1651.md) by him for [Hērōdias](../../strongs/g/g2266.md) his [adelphos](../../strongs/g/g80.md) [Philippos](../../strongs/g/g5376.md) [gynē](../../strongs/g/g1135.md), and for all the [ponēros](../../strongs/g/g4190.md) which [Hērōdēs](../../strongs/g/g2264.md) had [poieō](../../strongs/g/g4160.md),

<a name="luke_3_20"></a>Luke 3:20

[prostithēmi](../../strongs/g/g4369.md) yet this above all, that he [katakleiō](../../strongs/g/g2623.md) [Iōannēs](../../strongs/g/g2491.md) in [phylakē](../../strongs/g/g5438.md).

<a name="luke_3_21"></a>Luke 3:21

Now when all the [laos](../../strongs/g/g2992.md) were [baptizō](../../strongs/g/g907.md), [ginomai](../../strongs/g/g1096.md), that [Iēsous](../../strongs/g/g2424.md) also being [baptizō](../../strongs/g/g907.md), and [proseuchomai](../../strongs/g/g4336.md), the [ouranos](../../strongs/g/g3772.md) was [anoigō](../../strongs/g/g455.md),

<a name="luke_3_22"></a>Luke 3:22

And the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) [katabainō](../../strongs/g/g2597.md) in a [sōmatikos](../../strongs/g/g4984.md) [eidos](../../strongs/g/g1491.md) like a [peristera](../../strongs/g/g4058.md) upon him, and a [phōnē](../../strongs/g/g5456.md) [ginomai](../../strongs/g/g1096.md) from [ouranos](../../strongs/g/g3772.md), which [legō](../../strongs/g/g3004.md), Thou art my [agapētos](../../strongs/g/g27.md) [huios](../../strongs/g/g5207.md); in thee I am [eudokeō](../../strongs/g/g2106.md).

<a name="luke_3_23"></a>Luke 3:23

And [Iēsous](../../strongs/g/g2424.md) himself [archomai](../../strongs/g/g756.md) to be about thirty [etos](../../strongs/g/g2094.md), being (as was [nomizō](../../strongs/g/g3543.md)) the [huios](../../strongs/g/g5207.md) of [Iōsēph](../../strongs/g/g2501.md), which was of [Ēli](../../strongs/g/g2242.md),

<a name="luke_3_24"></a>Luke 3:24

Which was of [Maththat](../../strongs/g/g3158.md), which was of [Leui](../../strongs/g/g3017.md), which was of [Melchi](../../strongs/g/g3197.md), which was of [Iannai](../../strongs/g/g2388.md), which was of [Iōsēph](../../strongs/g/g2501.md),

<a name="luke_3_25"></a>Luke 3:25

Which was of [Mattathias](../../strongs/g/g3161.md), which was of [Amōs](../../strongs/g/g301.md), which was of [Naoum](../../strongs/g/g3486.md), which was of [Hesli](../../strongs/g/g2069.md), which was of [Naggai](../../strongs/g/g3477.md),

<a name="luke_3_26"></a>Luke 3:26

Which was of [Maath](../../strongs/g/g3092.md), which was of [Mattathias](../../strongs/g/g3161.md), which was of [Semeïn](../../strongs/g/g4584.md), which was of [Iōsēph](../../strongs/g/g2501.md), which was of [Ioudas](../../strongs/g/g2455.md),

<a name="luke_3_27"></a>Luke 3:27

Which was of [Iōanan](../../strongs/g/g2490.md), which was of [Rhēsa](../../strongs/g/g4488.md), which was of [Zorobabel](../../strongs/g/g2216.md), which was of [Salathiēl](../../strongs/g/g4528.md), which was of [Nēri](../../strongs/g/g3518.md),

<a name="luke_3_28"></a>Luke 3:28

Which was of [Melchi](../../strongs/g/g3197.md), which was of [Addi](../../strongs/g/g78.md), which was of [Kōsam](../../strongs/g/g2973.md), which was of [Elmōdam](../../strongs/g/g1678.md), which was of [Ēr](../../strongs/g/g2262.md),

<a name="luke_3_29"></a>Luke 3:29

Which was of [Iōsē](../../strongs/g/g2499.md), which was of [Eliezer](../../strongs/g/g1663.md), which was of [Iōrim](../../strongs/g/g2497.md), which was of [Maththat](../../strongs/g/g3158.md), which was of [Leui](../../strongs/g/g3017.md),

<a name="luke_3_30"></a>Luke 3:30

Which was of [Symeōn](../../strongs/g/g4826.md), which was of [Ioudas](../../strongs/g/g2455.md), which was of [Iōsēph](../../strongs/g/g2501.md), which was of [Iōnam](../../strongs/g/g2494.md), which was of [Eliakeim](../../strongs/g/g1662.md),

<a name="luke_3_31"></a>Luke 3:31

Which was of [Melea](../../strongs/g/g3190.md), which was of [Menna](../../strongs/g/g3104.md), which was of [Mattatha](../../strongs/g/g3160.md), which was of [Natham](../../strongs/g/g3481.md), which was of [Dabid](../../strongs/g/g1138.md),

<a name="luke_3_32"></a>Luke 3:32

Which was of [Iessai](../../strongs/g/g2421.md), which was of [Iōbēd](../../strongs/g/g5601.md), which was of [Boes](../../strongs/g/g1003.md), which was of [Salmōn](../../strongs/g/g4533.md), which was of [Naassōn](../../strongs/g/g3476.md),

<a name="luke_3_33"></a>Luke 3:33

Which was of [Aminadab](../../strongs/g/g284.md), which was of [Aram](../../strongs/g/g689.md), which was of [Hesrōm](../../strongs/g/g2074.md), which was of [Phares](../../strongs/g/g5329.md), which was of [Ioudas](../../strongs/g/g2455.md),

<a name="luke_3_34"></a>Luke 3:34

Which was of [Iakōb](../../strongs/g/g2384.md), which was of [Isaak](../../strongs/g/g2464.md), which was of [Abraam](../../strongs/g/g11.md), which was of [Thara](../../strongs/g/g2291.md), which was of [Nachōr](../../strongs/g/g3493.md),

<a name="luke_3_35"></a>Luke 3:35

Which was of [Serouch](../../strongs/g/g4562.md), which was of [Rhagau](../../strongs/g/g4466.md), which was of [Phalek](../../strongs/g/g5317.md), which was of [Eber](../../strongs/g/g1443.md), which was of [Sala](../../strongs/g/g4527.md),

<a name="luke_3_36"></a>Luke 3:36

Which was of [Kaïnam](../../strongs/g/g2536.md), which was of [Arphaxad](../../strongs/g/g742.md), which was of [Sēm](../../strongs/g/g4590.md), which was of [Nōe](../../strongs/g/g3575.md), which was of [Lamech](../../strongs/g/g2984.md),

<a name="luke_3_37"></a>Luke 3:37

Which was of [Mathousala](../../strongs/g/g3103.md), which was of [Henōch](../../strongs/g/g1802.md), which was of [Iaret](../../strongs/g/g2391.md), which was of [Maleleēl](../../strongs/g/g3121.md), which was of [Kaïnam](../../strongs/g/g2536.md),

<a name="luke_3_38"></a>Luke 3:38

Which was of [Enōs](../../strongs/g/g1800.md), which was of [Sēth](../../strongs/g/g4589.md), which was of [Adam](../../strongs/g/g76.md), which was of [theos](../../strongs/g/g2316.md).

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke](luke_2.md) - [Luke](luke_4.md)