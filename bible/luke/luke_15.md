# [Luke 15](https://www.blueletterbible.org/kjv/luk/15/1/rl1/s_974001)

<a name="luke_15_1"></a>Luke 15:1

Then [eggizō](../../strongs/g/g1448.md) unto him all the [telōnēs](../../strongs/g/g5057.md) and [hamartōlos](../../strongs/g/g268.md) for to [akouō](../../strongs/g/g191.md) him.

<a name="luke_15_2"></a>Luke 15:2

And the [Pharisaios](../../strongs/g/g5330.md) and [grammateus](../../strongs/g/g1122.md) [diagoggyzō](../../strongs/g/g1234.md), [legō](../../strongs/g/g3004.md), [houtos](../../strongs/g/g3778.md) [prosdechomai](../../strongs/g/g4327.md) [hamartōlos](../../strongs/g/g268.md), and [synesthiō](../../strongs/g/g4906.md) with them.

<a name="luke_15_3"></a>Luke 15:3

And he [eipon](../../strongs/g/g2036.md) this [parabolē](../../strongs/g/g3850.md) unto them, [legō](../../strongs/g/g3004.md),

<a name="luke_15_4"></a>Luke 15:4

**What [anthrōpos](../../strongs/g/g444.md) of you, having an hundred [probaton](../../strongs/g/g4263.md), if he [apollymi](../../strongs/g/g622.md) one of them, doth not [kataleipō](../../strongs/g/g2641.md) the ninety and nine in the [erēmos](../../strongs/g/g2048.md), and [poreuō](../../strongs/g/g4198.md) after that which is [apollymi](../../strongs/g/g622.md), until he [heuriskō](../../strongs/g/g2147.md) it?**

<a name="luke_15_5"></a>Luke 15:5

**And when he hath [heuriskō](../../strongs/g/g2147.md), he [epitithēmi](../../strongs/g/g2007.md) on his [ōmos](../../strongs/g/g5606.md), [chairō](../../strongs/g/g5463.md).**

<a name="luke_15_6"></a>Luke 15:6

**And when he [erchomai](../../strongs/g/g2064.md) [oikos](../../strongs/g/g3624.md), he [sygkaleō](../../strongs/g/g4779.md) [philos](../../strongs/g/g5384.md) and [geitōn](../../strongs/g/g1069.md), [legō](../../strongs/g/g3004.md) unto them, [sygchairō](../../strongs/g/g4796.md) with me; for I have [heuriskō](../../strongs/g/g2147.md) my [probaton](../../strongs/g/g4263.md) which was [apollymi](../../strongs/g/g622.md).**

<a name="luke_15_7"></a>Luke 15:7

**I [legō](../../strongs/g/g3004.md) unto you, that likewise [chara](../../strongs/g/g5479.md) shall be in [ouranos](../../strongs/g/g3772.md) over one [hamartōlos](../../strongs/g/g268.md) that [metanoeō](../../strongs/g/g3340.md), more than over ninety and nine [dikaios](../../strongs/g/g1342.md), which [chreia](../../strongs/g/g5532.md) no [metanoia](../../strongs/g/g3341.md).**

<a name="luke_15_8"></a>Luke 15:8

**Either what [gynē](../../strongs/g/g1135.md) having ten [drachmē](../../strongs/g/g1406.md), if she [apollymi](../../strongs/g/g622.md) one [drachmē](../../strongs/g/g1406.md), doth not [haptō](../../strongs/g/g681.md) a [lychnos](../../strongs/g/g3088.md), and [saroō](../../strongs/g/g4563.md) the [oikia](../../strongs/g/g3614.md), and [zēteō](../../strongs/g/g2212.md) [epimelōs](../../strongs/g/g1960.md) till she [heuriskō](../../strongs/g/g2147.md)?**

<a name="luke_15_9"></a>Luke 15:9

**And when she hath [heuriskō](../../strongs/g/g2147.md) it, she [sygkaleō](../../strongs/g/g4779.md) her [philos](../../strongs/g/g5384.md) and her [geitōn](../../strongs/g/g1069.md), [legō](../../strongs/g/g3004.md), [sygchairō](../../strongs/g/g4796.md) with me; for I have [heuriskō](../../strongs/g/g2147.md) the [drachmē](../../strongs/g/g1406.md) which I had [apollymi](../../strongs/g/g622.md)**.

<a name="luke_15_10"></a>Luke 15:10

**Likewise, I [legō](../../strongs/g/g3004.md) unto you, there is [chara](../../strongs/g/g5479.md) in the [enōpion](../../strongs/g/g1799.md) of the [aggelos](../../strongs/g/g32.md) of [theos](../../strongs/g/g2316.md) over one [hamartōlos](../../strongs/g/g268.md) that [metanoeō](../../strongs/g/g3340.md)**.

<a name="luke_15_11"></a>Luke 15:11

And he [eipon](../../strongs/g/g2036.md), **A certain [anthrōpos](../../strongs/g/g444.md) had two [huios](../../strongs/g/g5207.md):**

<a name="luke_15_12"></a>Luke 15:12

**And the [neos](../../strongs/g/g3501.md) of them [eipon](../../strongs/g/g2036.md) to [patēr](../../strongs/g/g3962.md), [patēr](../../strongs/g/g3962.md), [didōmi](../../strongs/g/g1325.md) me the [meros](../../strongs/g/g3313.md) of [ousia](../../strongs/g/g3776.md) that [epiballō](../../strongs/g/g1911.md). And he [diaireō](../../strongs/g/g1244.md) unto them [bios](../../strongs/g/g979.md).**

<a name="luke_15_13"></a>Luke 15:13

**And not [polys](../../strongs/g/g4183.md) [hēmera](../../strongs/g/g2250.md) after the [neos](../../strongs/g/g3501.md) [huios](../../strongs/g/g5207.md) [synagō](../../strongs/g/g4863.md) [hapas](../../strongs/g/g537.md), and [apodēmeō](../../strongs/g/g589.md) into a [makros](../../strongs/g/g3117.md) [chōra](../../strongs/g/g5561.md), and there [diaskorpizō](../../strongs/g/g1287.md) his [ousia](../../strongs/g/g3776.md) with [asōtōs](../../strongs/g/g811.md) [zaō](../../strongs/g/g2198.md).**

<a name="luke_15_14"></a>Luke 15:14

**And when he had [dapanaō](../../strongs/g/g1159.md) all, there [ginomai](../../strongs/g/g1096.md) an [ischyros](../../strongs/g/g2478.md) [limos](../../strongs/g/g3042.md) in that [chōra](../../strongs/g/g5561.md); and he [archomai](../../strongs/g/g756.md) [hystereō](../../strongs/g/g5302.md).**

<a name="luke_15_15"></a>Luke 15:15

**And he [poreuō](../../strongs/g/g4198.md) and [kollaō](../../strongs/g/g2853.md) to a [politēs](../../strongs/g/g4177.md) of that [chōra](../../strongs/g/g5561.md); and he [pempō](../../strongs/g/g3992.md) him into his [agros](../../strongs/g/g68.md) to [boskō](../../strongs/g/g1006.md) [choiros](../../strongs/g/g5519.md).**

<a name="luke_15_16"></a>Luke 15:16

**And he would [epithymeō](../../strongs/g/g1937.md) have [gemizō](../../strongs/g/g1072.md) his [koilia](../../strongs/g/g2836.md) with the [keration](../../strongs/g/g2769.md) that the [choiros](../../strongs/g/g5519.md) did [esthiō](../../strongs/g/g2068.md): and [oudeis](../../strongs/g/g3762.md) [didōmi](../../strongs/g/g1325.md) unto him.**

<a name="luke_15_17"></a>Luke 15:17

**And when he [erchomai](../../strongs/g/g2064.md) to himself, he [eipon](../../strongs/g/g2036.md), How many [misthios](../../strongs/g/g3407.md) of my [patēr](../../strongs/g/g3962.md) have [artos](../../strongs/g/g740.md) [perisseuō](../../strongs/g/g4052.md), and I [apollymi](../../strongs/g/g622.md) with [limos](../../strongs/g/g3042.md)!**

<a name="luke_15_18"></a>Luke 15:18

**I will [anistēmi](../../strongs/g/g450.md) and [poreuō](../../strongs/g/g4198.md) to my [patēr](../../strongs/g/g3962.md), and will [eipon](../../strongs/g/g2046.md) unto him, [patēr](../../strongs/g/g3962.md), I have [hamartanō](../../strongs/g/g264.md) against [ouranos](../../strongs/g/g3772.md), and [enōpion](../../strongs/g/g1799.md) thee,**

<a name="luke_15_19"></a>Luke 15:19

**And am no more [axios](../../strongs/g/g514.md) to be [kaleō](../../strongs/g/g2564.md) thy [huios](../../strongs/g/g5207.md): [poieō](../../strongs/g/g4160.md) me as one of thy [misthios](../../strongs/g/g3407.md).**

<a name="luke_15_20"></a>Luke 15:20

**And he [anistēmi](../../strongs/g/g450.md), and [erchomai](../../strongs/g/g2064.md) to his [patēr](../../strongs/g/g3962.md). But when he was yet a [makran](../../strongs/g/g3112.md) off, his [patēr](../../strongs/g/g3962.md) [eidō](../../strongs/g/g1492.md) him, and had [splagchnizomai](../../strongs/g/g4697.md), and [trechō](../../strongs/g/g5143.md), and [epipiptō](../../strongs/g/g1968.md) on his [trachēlos](../../strongs/g/g5137.md), and [kataphileō](../../strongs/g/g2705.md) him.**

<a name="luke_15_21"></a>Luke 15:21

**And the [huios](../../strongs/g/g5207.md) [eipon](../../strongs/g/g2036.md) unto him, [patēr](../../strongs/g/g3962.md), I have [hamartanō](../../strongs/g/g264.md) against [ouranos](../../strongs/g/g3772.md), and in thy [enōpion](../../strongs/g/g1799.md), and am no more [axios](../../strongs/g/g514.md) to be [kaleō](../../strongs/g/g2564.md) thy [huios](../../strongs/g/g5207.md).**

<a name="luke_15_22"></a>Luke 15:22

**But the [patēr](../../strongs/g/g3962.md) [eipon](../../strongs/g/g2036.md) to his [doulos](../../strongs/g/g1401.md), [ekpherō](../../strongs/g/g1627.md) the [prōtos](../../strongs/g/g4413.md) [stolē](../../strongs/g/g4749.md), and [endyō](../../strongs/g/g1746.md) him; and [didōmi](../../strongs/g/g1325.md) a [daktylios](../../strongs/g/g1146.md) on his [cheir](../../strongs/g/g5495.md), and [hypodēma](../../strongs/g/g5266.md) on [pous](../../strongs/g/g4228.md):**

<a name="luke_15_23"></a>Luke 15:23

**And [pherō](../../strongs/g/g5342.md) the [siteutos](../../strongs/g/g4618.md) [moschos](../../strongs/g/g3448.md), and [thyō](../../strongs/g/g2380.md); and let us [phago](../../strongs/g/g5315.md), and [euphrainō](../../strongs/g/g2165.md):**

<a name="luke_15_24"></a>Luke 15:24

**For this my [huios](../../strongs/g/g5207.md) was [nekros](../../strongs/g/g3498.md), and is [anazaō](../../strongs/g/g326.md); he was [apollymi](../../strongs/g/g622.md), and is [heuriskō](../../strongs/g/g2147.md). And they [archomai](../../strongs/g/g756.md) to [euphrainō](../../strongs/g/g2165.md).**

<a name="luke_15_25"></a>Luke 15:25

**Now his [presbyteros](../../strongs/g/g4245.md) [huios](../../strongs/g/g5207.md) was in the [agros](../../strongs/g/g68.md): and as he [erchomai](../../strongs/g/g2064.md) and [eggizō](../../strongs/g/g1448.md) to the [oikia](../../strongs/g/g3614.md), he [akouō](../../strongs/g/g191.md) [symphōnia](../../strongs/g/g4858.md) and [choros](../../strongs/g/g5525.md).**

<a name="luke_15_26"></a>Luke 15:26

**And he [proskaleō](../../strongs/g/g4341.md) one of the [pais](../../strongs/g/g3816.md), and [pynthanomai](../../strongs/g/g4441.md) what these things [eiēn](../../strongs/g/g1498.md).**

<a name="luke_15_27"></a>Luke 15:27

**And he [eipon](../../strongs/g/g2036.md) unto him, Thy [adelphos](../../strongs/g/g80.md) is [hēkō](../../strongs/g/g2240.md); and thy [patēr](../../strongs/g/g3962.md) hath [thyō](../../strongs/g/g2380.md) the [siteutos](../../strongs/g/g4618.md) [moschos](../../strongs/g/g3448.md), because he hath [apolambanō](../../strongs/g/g618.md) him [hygiainō](../../strongs/g/g5198.md).**

<a name="luke_15_28"></a>Luke 15:28

**And he was [orgizō](../../strongs/g/g3710.md), and would not [eiserchomai](../../strongs/g/g1525.md): therefore [exerchomai](../../strongs/g/g1831.md) his [patēr](../../strongs/g/g3962.md), and [parakaleō](../../strongs/g/g3870.md) him.**

<a name="luke_15_29"></a>Luke 15:29

**And he [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) to [patēr](../../strongs/g/g3962.md), [idou](../../strongs/g/g2400.md), these [tosoutos](../../strongs/g/g5118.md) [etos](../../strongs/g/g2094.md) do I [douleuō](../../strongs/g/g1398.md) thee, [oudepote](../../strongs/g/g3763.md) [parerchomai](../../strongs/g/g3928.md) I thy [entolē](../../strongs/g/g1785.md): and yet thou never [didōmi](../../strongs/g/g1325.md) me an [eriphos](../../strongs/g/g2056.md), that I might [euphrainō](../../strongs/g/g2165.md) with my [philos](../../strongs/g/g5384.md):**

<a name="luke_15_30"></a>Luke 15:30

**But as soon as this thy [huios](../../strongs/g/g5207.md) was [erchomai](../../strongs/g/g2064.md), which hath [katesthiō](../../strongs/g/g2719.md) thy [bios](../../strongs/g/g979.md) with [pornē](../../strongs/g/g4204.md), thou hast [thyō](../../strongs/g/g2380.md) for him the [siteutos](../../strongs/g/g4618.md) [moschos](../../strongs/g/g3448.md).**

<a name="luke_15_31"></a>Luke 15:31

**And he [eipon](../../strongs/g/g2036.md) unto him, [teknon](../../strongs/g/g5043.md), thou art [pantote](../../strongs/g/g3842.md) with me, and all that I have is thine.**

<a name="luke_15_32"></a>Luke 15:32

**It was [dei](../../strongs/g/g1163.md) that we [euphrainō](../../strongs/g/g2165.md), and be [chairō](../../strongs/g/g5463.md): for this thy [adelphos](../../strongs/g/g80.md) was [nekros](../../strongs/g/g3498.md), and is [anazaō](../../strongs/g/g326.md); and was [apollymi](../../strongs/g/g622.md), and is [heuriskō](../../strongs/g/g2147.md).**

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 14](luke_14.md) - [Luke 16](luke_16.md)