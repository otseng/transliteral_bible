# [Luke 14](https://www.blueletterbible.org/kjv/luk/14/1/rl1/s_987001)

<a name="luke_14_1"></a>Luke 14:1

And [ginomai](../../strongs/g/g1096.md), as he [erchomai](../../strongs/g/g2064.md) into the [oikos](../../strongs/g/g3624.md) of one of the [archōn](../../strongs/g/g758.md) [Pharisaios](../../strongs/g/g5330.md) to [phago](../../strongs/g/g5315.md) [artos](../../strongs/g/g740.md) on the [sabbaton](../../strongs/g/g4521.md), that they [paratēreō](../../strongs/g/g3906.md) him.

<a name="luke_14_2"></a>Luke 14:2

And, [idou](../../strongs/g/g2400.md), there was a certain [anthrōpos](../../strongs/g/g444.md) before him [hydrōpikos](../../strongs/g/g5203.md).

<a name="luke_14_3"></a>Luke 14:3

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto the [nomikos](../../strongs/g/g3544.md) and [Pharisaios](../../strongs/g/g5330.md), [legō](../../strongs/g/g3004.md), **Is it [exesti](../../strongs/g/g1832.md) to [therapeuō](../../strongs/g/g2323.md) on the [sabbaton](../../strongs/g/g4521.md)?**

<a name="luke_14_4"></a>Luke 14:4

And they [hēsychazō](../../strongs/g/g2270.md). And he [epilambanomai](../../strongs/g/g1949.md), and [iaomai](../../strongs/g/g2390.md) him, and [apolyō](../../strongs/g/g630.md) him;

<a name="luke_14_5"></a>Luke 14:5

And [apokrinomai](../../strongs/g/g611.md) them, [eipon](../../strongs/g/g2036.md), **Which of you shall have an [onos](../../strongs/g/g3688.md) or a [bous](../../strongs/g/g1016.md) [empiptō](../../strongs/g/g1706.md) into a [phrear](../../strongs/g/g5421.md), and will not [eutheōs](../../strongs/g/g2112.md) [anaspaō](../../strongs/g/g385.md) him out on the [sabbaton](../../strongs/g/g4521.md) [hēmera](../../strongs/g/g2250.md)?**

<a name="luke_14_6"></a>Luke 14:6

And they could not [antapokrinomai](../../strongs/g/g470.md) him to [tauta](../../strongs/g/g5023.md).

<a name="luke_14_7"></a>Luke 14:7

And he [legō](../../strongs/g/g3004.md) a [parabolē](../../strongs/g/g3850.md) to [kaleō](../../strongs/g/g2564.md), when he [epechō](../../strongs/g/g1907.md) how they [eklegomai](../../strongs/g/g1586.md) the [prōtoklisia](../../strongs/g/g4411.md); [legō](../../strongs/g/g3004.md) unto them.

<a name="luke_14_8"></a>Luke 14:8

**When thou art [kaleō](../../strongs/g/g2564.md) of any to a [gamos](../../strongs/g/g1062.md), [kataklinō](../../strongs/g/g2625.md) not in the [prōtoklisia](../../strongs/g/g4411.md); lest an [entimos](../../strongs/g/g1784.md) than thou be [kaleō](../../strongs/g/g2564.md) of him;**

<a name="luke_14_9"></a>Luke 14:9

**And he that [kaleō](../../strongs/g/g2564.md) thee and him [erchomai](../../strongs/g/g2064.md) and [eipon](../../strongs/g/g2046.md) to thee, [didōmi](../../strongs/g/g1325.md) [toutō](../../strongs/g/g5129.md) [topos](../../strongs/g/g5117.md); and thou [archomai](../../strongs/g/g756.md) with [aischynē](../../strongs/g/g152.md) to [katechō](../../strongs/g/g2722.md) the [eschatos](../../strongs/g/g2078.md) [topos](../../strongs/g/g5117.md).**

<a name="luke_14_10"></a>Luke 14:10

**But when thou art [kaleō](../../strongs/g/g2564.md), [poreuō](../../strongs/g/g4198.md) and [anapiptō](../../strongs/g/g377.md) in the [eschatos](../../strongs/g/g2078.md) [topos](../../strongs/g/g5117.md); that when he that [kaleō](../../strongs/g/g2564.md) thee [erchomai](../../strongs/g/g2064.md), he may [eipon](../../strongs/g/g2036.md) unto thee, [philos](../../strongs/g/g5384.md), [prosanabainō](../../strongs/g/g4320.md) [anōteros](../../strongs/g/g511.md): then shalt thou have [doxa](../../strongs/g/g1391.md) [enōpion](../../strongs/g/g1799.md) [synanakeimai](../../strongs/g/g4873.md) with thee.**

<a name="luke_14_11"></a>Luke 14:11

**For whosoever [hypsoō](../../strongs/g/g5312.md) himself shall be [tapeinoō](../../strongs/g/g5013.md); and he that [tapeinoō](../../strongs/g/g5013.md) himself shall be [hypsoō](../../strongs/g/g5312.md).**

<a name="luke_14_12"></a>Luke 14:12

Then [legō](../../strongs/g/g3004.md) he also to him that [kaleō](../../strongs/g/g2564.md) him, **When thou [poieō](../../strongs/g/g4160.md) an [ariston](../../strongs/g/g712.md) or a [deipnon](../../strongs/g/g1173.md), [phōneō](../../strongs/g/g5455.md) not thy [philos](../../strongs/g/g5384.md), nor thy [adelphos](../../strongs/g/g80.md), neither thy [syggenēs](../../strongs/g/g4773.md), nor [plousios](../../strongs/g/g4145.md) [geitōn](../../strongs/g/g1069.md); lest they also [antikaleō](../../strongs/g/g479.md) thee, and an [antapodoma](../../strongs/g/g468.md) [ginomai](../../strongs/g/g1096.md) thee.**

<a name="luke_14_13"></a>Luke 14:13

**But when thou [poieō](../../strongs/g/g4160.md) a [dochē](../../strongs/g/g1403.md), [kaleō](../../strongs/g/g2564.md) the [ptōchos](../../strongs/g/g4434.md), the [anapēros](../../strongs/g/g376.md), the [chōlos](../../strongs/g/g5560.md), the [typhlos](../../strongs/g/g5185.md):**

<a name="luke_14_14"></a>Luke 14:14

**And thou shalt be [makarios](../../strongs/g/g3107.md); for they cannot [antapodidōmi](../../strongs/g/g467.md) thee: for thou shalt be [antapodidōmi](../../strongs/g/g467.md) at the [anastasis](../../strongs/g/g386.md) of the [dikaios](../../strongs/g/g1342.md).**

<a name="luke_14_15"></a>Luke 14:15

And when one of them [synanakeimai](../../strongs/g/g4873.md) with him [akouō](../../strongs/g/g191.md) these things, he [eipon](../../strongs/g/g2036.md) unto him, [makarios](../../strongs/g/g3107.md) he that shall [phago](../../strongs/g/g5315.md) [artos](../../strongs/g/g740.md) in the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).

<a name="luke_14_16"></a>Luke 14:16

Then [eipon](../../strongs/g/g2036.md) he unto him, **A certain [anthrōpos](../../strongs/g/g444.md) [poieō](../../strongs/g/g4160.md) a [megas](../../strongs/g/g3173.md) [deipnon](../../strongs/g/g1173.md), and [kaleō](../../strongs/g/g2564.md) [polys](../../strongs/g/g4183.md):**

<a name="luke_14_17"></a>Luke 14:17

**And [apostellō](../../strongs/g/g649.md) his [doulos](../../strongs/g/g1401.md) at [deipnon](../../strongs/g/g1173.md) [hōra](../../strongs/g/g5610.md) to [eipon](../../strongs/g/g2036.md) to [kaleō](../../strongs/g/g2564.md), [erchomai](../../strongs/g/g2064.md); for all things are now [hetoimos](../../strongs/g/g2092.md).**

<a name="luke_14_18"></a>Luke 14:18

**And they all with [mia](../../strongs/g/g3391.md) [archomai](../../strongs/g/g756.md) to [paraiteomai](../../strongs/g/g3868.md). The first [eipon](../../strongs/g/g2036.md) unto him, I have [agorazō](../../strongs/g/g59.md) an [agros](../../strongs/g/g68.md), and I [anagkē](../../strongs/g/g318.md) [exerchomai](../../strongs/g/g1831.md) and [eidō](../../strongs/g/g1492.md) it: I [erōtaō](../../strongs/g/g2065.md) thee have me [paraiteomai](../../strongs/g/g3868.md).**

<a name="luke_14_19"></a>Luke 14:19

**And another [eipon](../../strongs/g/g2036.md), I have [agorazō](../../strongs/g/g59.md) five [zeugos](../../strongs/g/g2201.md) of [bous](../../strongs/g/g1016.md), and I [poreuō](../../strongs/g/g4198.md) to [dokimazō](../../strongs/g/g1381.md) them: I [erōtaō](../../strongs/g/g2065.md) thee have me [paraiteomai](../../strongs/g/g3868.md).**

<a name="luke_14_20"></a>Luke 14:20

**And another [eipon](../../strongs/g/g2036.md), I have [gameō](../../strongs/g/g1060.md) a [gynē](../../strongs/g/g1135.md), and therefore I cannot [erchomai](../../strongs/g/g2064.md).**

<a name="luke_14_21"></a>Luke 14:21

**So that [doulos](../../strongs/g/g1401.md) [paraginomai](../../strongs/g/g3854.md), and [apaggellō](../../strongs/g/g518.md) his [kyrios](../../strongs/g/g2962.md) these things. Then the [oikodespotēs](../../strongs/g/g3617.md) being [orgizō](../../strongs/g/g3710.md) [eipon](../../strongs/g/g2036.md) to his [doulos](../../strongs/g/g1401.md), [exerchomai](../../strongs/g/g1831.md) [tacheōs](../../strongs/g/g5030.md) into the [plateia](../../strongs/g/g4113.md) and [rhymē](../../strongs/g/g4505.md) of the [polis](../../strongs/g/g4172.md), and [eisagō](../../strongs/g/g1521.md) hither the [ptōchos](../../strongs/g/g4434.md), and the [anapēros](../../strongs/g/g376.md), and the [chōlos](../../strongs/g/g5560.md), and the [typhlos](../../strongs/g/g5185.md).**

<a name="luke_14_22"></a>Luke 14:22

**And the [doulos](../../strongs/g/g1401.md) [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), it is [ginomai](../../strongs/g/g1096.md) as thou hast [epitassō](../../strongs/g/g2004.md), and yet there is [topos](../../strongs/g/g5117.md).**

<a name="luke_14_23"></a>Luke 14:23

**And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md) unto the [doulos](../../strongs/g/g1401.md), [exerchomai](../../strongs/g/g1831.md) into the [hodos](../../strongs/g/g3598.md) and [phragmos](../../strongs/g/g5418.md), and [anagkazō](../../strongs/g/g315.md) them to [eiserchomai](../../strongs/g/g1525.md), that my [oikos](../../strongs/g/g3624.md) may be [gemizō](../../strongs/g/g1072.md).**

<a name="luke_14_24"></a>Luke 14:24

**For I [legō](../../strongs/g/g3004.md) unto you, That none of those [anēr](../../strongs/g/g435.md) which were [kaleō](../../strongs/g/g2564.md) shall [geuomai](../../strongs/g/g1089.md) of my [deipnon](../../strongs/g/g1173.md).**

<a name="luke_14_25"></a>Luke 14:25

And there [symporeuomai](../../strongs/g/g4848.md) [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) with him: and he [strephō](../../strongs/g/g4762.md), and [eipon](../../strongs/g/g2036.md) unto them,

<a name="luke_14_26"></a>Luke 14:26

**If any [erchomai](../../strongs/g/g2064.md) to me, and [miseō](../../strongs/g/g3404.md) not his [patēr](../../strongs/g/g3962.md), and [mētēr](../../strongs/g/g3384.md), and [gynē](../../strongs/g/g1135.md), and [teknon](../../strongs/g/g5043.md), and [adelphos](../../strongs/g/g80.md), and [adelphē](../../strongs/g/g79.md), [eti](../../strongs/g/g2089.md), and his own [psychē](../../strongs/g/g5590.md) also, he cannot be my [mathētēs](../../strongs/g/g3101.md).**

<a name="luke_14_27"></a>Luke 14:27

**And whosoever doth not [bastazō](../../strongs/g/g941.md) his [stauros](../../strongs/g/g4716.md), and [erchomai](../../strongs/g/g2064.md) after me, cannot be my [mathētēs](../../strongs/g/g3101.md).**

<a name="luke_14_28"></a>Luke 14:28

**For which of you, [thelō](../../strongs/g/g2309.md) to [oikodomeō](../../strongs/g/g3618.md) a [pyrgos](../../strongs/g/g4444.md), [kathizō](../../strongs/g/g2523.md) not first, and [psēphizō](../../strongs/g/g5585.md) the [dapanē](../../strongs/g/g1160.md), whether he have to [apartismos](../../strongs/g/g535.md) it?**

<a name="luke_14_29"></a>Luke 14:29

**[ina mē](../../strongs/g/g3363.md) [mēpote](../../strongs/g/g3379.md), after he hath [tithēmi](../../strongs/g/g5087.md) the [themelios](../../strongs/g/g2310.md), and is not able to [ekteleō](../../strongs/g/g1615.md), all that [theōreō](../../strongs/g/g2334.md) it [archomai](../../strongs/g/g756.md) to [empaizō](../../strongs/g/g1702.md) him,**

<a name="luke_14_30"></a>Luke 14:30

**[legō](../../strongs/g/g3004.md), This [anthrōpos](../../strongs/g/g444.md) [archomai](../../strongs/g/g756.md) to [oikodomeō](../../strongs/g/g3618.md), and was not able to [ekteleō](../../strongs/g/g1615.md).**

<a name="luke_14_31"></a>Luke 14:31

**Or what [basileus](../../strongs/g/g935.md), [poreuō](../../strongs/g/g4198.md) to [symballō](../../strongs/g/g4820.md) [polemos](../../strongs/g/g4171.md) against another [basileus](../../strongs/g/g935.md), [kathizō](../../strongs/g/g2523.md) not first, and [bouleuō](../../strongs/g/g1011.md) whether he be [dynatos](../../strongs/g/g1415.md) with ten thousand to [apantaō](../../strongs/g/g528.md) him that [erchomai](../../strongs/g/g2064.md) against him with twenty thousand?**

<a name="luke_14_32"></a>Luke 14:32

**Or else, while the other is yet [porrō](../../strongs/g/g4206.md), he [apostellō](../../strongs/g/g649.md) a [presbeia](../../strongs/g/g4242.md), and [erōtaō](../../strongs/g/g2065.md) conditions of [eirēnē](../../strongs/g/g1515.md).**

<a name="luke_14_33"></a>Luke 14:33

**So likewise, whosoever he be of you that [apotassō](../../strongs/g/g657.md) not all that he [hyparchonta](../../strongs/g/g5224.md), he cannot be my [mathētēs](../../strongs/g/g3101.md).**

<a name="luke_14_34"></a>Luke 14:34

**[halas](../../strongs/g/g217.md) [kalos](../../strongs/g/g2570.md): but if the [halas](../../strongs/g/g217.md) have [mōrainō](../../strongs/g/g3471.md), wherewith shall it be [artyō](../../strongs/g/g741.md)?**

<a name="luke_14_35"></a>Luke 14:35

**It is neither [euthetos](../../strongs/g/g2111.md) for the [gē](../../strongs/g/g1093.md), nor yet for the [kopria](../../strongs/g/g2874.md); [ballō](../../strongs/g/g906.md) it. He that hath [ous](../../strongs/g/g3775.md) to [akouō](../../strongs/g/g191.md), let him [akouō](../../strongs/g/g191.md).**

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 13](luke_13.md) - [Luke 15](luke_15.md)