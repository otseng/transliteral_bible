# [Luke 18](https://www.blueletterbible.org/kjv/luk/18/1/rl1/s_991001)

<a name="luke_18_1"></a>Luke 18:1

And he [legō](../../strongs/g/g3004.md) a [parabolē](../../strongs/g/g3850.md) unto them, [dei](../../strongs/g/g1163.md) [pantote](../../strongs/g/g3842.md) to [proseuchomai](../../strongs/g/g4336.md), and not to [ekkakeō](../../strongs/g/g1573.md);

<a name="luke_18_2"></a>Luke 18:2

[legō](../../strongs/g/g3004.md), **There was in a [polis](../../strongs/g/g4172.md) a [kritēs](../../strongs/g/g2923.md), which [phobeō](../../strongs/g/g5399.md) not [theos](../../strongs/g/g2316.md), neither [entrepō](../../strongs/g/g1788.md) [anthrōpos](../../strongs/g/g444.md):**

<a name="luke_18_3"></a>Luke 18:3

**And there was a [chēra](../../strongs/g/g5503.md) in that [polis](../../strongs/g/g4172.md); and she [erchomai](../../strongs/g/g2064.md) unto him, [legō](../../strongs/g/g3004.md), [ekdikeō](../../strongs/g/g1556.md) me of mine [antidikos](../../strongs/g/g476.md).**

<a name="luke_18_4"></a>Luke 18:4

**And he would not for a [chronos](../../strongs/g/g5550.md): but afterward he [eipon](../../strongs/g/g2036.md) within himself, Though I [phobeō](../../strongs/g/g5399.md) not [theos](../../strongs/g/g2316.md), nor [entrepō](../../strongs/g/g1788.md) [anthrōpos](../../strongs/g/g444.md);**

<a name="luke_18_5"></a>Luke 18:5

**Yet because this [chēra](../../strongs/g/g5503.md) [parechō](../../strongs/g/g3930.md) [kopos](../../strongs/g/g2873.md) me, I will [ekdikeō](../../strongs/g/g1556.md) her, lest by her [telos](../../strongs/g/g5056.md) [erchomai](../../strongs/g/g2064.md) she [hypōpiazō](../../strongs/g/g5299.md) me.**

<a name="luke_18_6"></a>Luke 18:6

And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md), **[akouō](../../strongs/g/g191.md) what the [adikia](../../strongs/g/g93.md) [kritēs](../../strongs/g/g2923.md) [legō](../../strongs/g/g3004.md).**

<a name="luke_18_7"></a>Luke 18:7

**And shall not [theos](../../strongs/g/g2316.md) [poieō](../../strongs/g/g4160.md) [ekdikēsis](../../strongs/g/g1557.md) his own [eklektos](../../strongs/g/g1588.md), which [boaō](../../strongs/g/g994.md) [hēmera](../../strongs/g/g2250.md) and [nyx](../../strongs/g/g3571.md) unto him, though he [makrothymeō](../../strongs/g/g3114.md) with them?**

<a name="luke_18_8"></a>Luke 18:8

**I [legō](../../strongs/g/g3004.md) you that he [poieō](../../strongs/g/g4160.md) [ekdikēsis](../../strongs/g/g1557.md) them [tachos](../../strongs/g/g5034.md). [plēn](../../strongs/g/g4133.md) [ara](../../strongs/g/g687.md) when the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [erchomai](../../strongs/g/g2064.md), shall he [heuriskō](../../strongs/g/g2147.md) [pistis](../../strongs/g/g4102.md) on [gē](../../strongs/g/g1093.md)?**

<a name="luke_18_9"></a>Luke 18:9

And he [eipon](../../strongs/g/g2036.md) this [parabolē](../../strongs/g/g3850.md) unto certain which [peithō](../../strongs/g/g3982.md) in themselves that they were [dikaios](../../strongs/g/g1342.md), and [exoutheneō](../../strongs/g/g1848.md) [loipos](../../strongs/g/g3062.md):

<a name="luke_18_10"></a>Luke 18:10

**Two [anthrōpos](../../strongs/g/g444.md) [anabainō](../../strongs/g/g305.md) into the [hieron](../../strongs/g/g2411.md) to [proseuchomai](../../strongs/g/g4336.md); the one a [Pharisaios](../../strongs/g/g5330.md), and the other a [telōnēs](../../strongs/g/g5057.md).**

<a name="luke_18_11"></a>Luke 18:11

**The [Pharisaios](../../strongs/g/g5330.md) [histēmi](../../strongs/g/g2476.md) and [proseuchomai](../../strongs/g/g4336.md) thus with himself, [theos](../../strongs/g/g2316.md), I [eucharisteō](../../strongs/g/g2168.md) thee, that I am not as [loipos](../../strongs/g/g3062.md) [anthrōpos](../../strongs/g/g444.md), [harpax](../../strongs/g/g727.md), [adikos](../../strongs/g/g94.md), [moichos](../../strongs/g/g3432.md), or even as this [telōnēs](../../strongs/g/g5057.md).**

<a name="luke_18_12"></a>Luke 18:12

**I [nēsteuō](../../strongs/g/g3522.md) twice in the [sabbaton](../../strongs/g/g4521.md), I [apodekatoō](../../strongs/g/g586.md) of all that I [ktaomai](../../strongs/g/g2932.md).**

<a name="luke_18_13"></a>Luke 18:13

**And the [telōnēs](../../strongs/g/g5057.md), [histēmi](../../strongs/g/g2476.md) [makrothen](../../strongs/g/g3113.md), would not [epairō](../../strongs/g/g1869.md) so much as [ophthalmos](../../strongs/g/g3788.md) unto [ouranos](../../strongs/g/g3772.md), but [typtō](../../strongs/g/g5180.md) upon his [stēthos](../../strongs/g/g4738.md), [legō](../../strongs/g/g3004.md), [theos](../../strongs/g/g2316.md) be [hilaskomai](../../strongs/g/g2433.md) to me a [hamartōlos](../../strongs/g/g268.md).**

<a name="luke_18_14"></a>Luke 18:14

**I [legō](../../strongs/g/g3004.md) you, [houtos](../../strongs/g/g3778.md) [katabainō](../../strongs/g/g2597.md) to his [oikos](../../strongs/g/g3624.md) [dikaioō](../../strongs/g/g1344.md) than the other: for every one that [hypsoō](../../strongs/g/g5312.md) himself shall be [tapeinoō](../../strongs/g/g5013.md); and he that [tapeinoō](../../strongs/g/g5013.md) himself shall be [hypsoō](../../strongs/g/g5312.md).**

<a name="luke_18_15"></a>Luke 18:15

And they [prospherō](../../strongs/g/g4374.md) unto him also [brephos](../../strongs/g/g1025.md), that he would [haptomai](../../strongs/g/g680.md) them: but when [mathētēs](../../strongs/g/g3101.md) [eidō](../../strongs/g/g1492.md), they [epitimaō](../../strongs/g/g2008.md) them.

<a name="luke_18_16"></a>Luke 18:16

But [Iēsous](../../strongs/g/g2424.md) [proskaleō](../../strongs/g/g4341.md) them, and [eipon](../../strongs/g/g2036.md), **[aphiēmi](../../strongs/g/g863.md) [paidion](../../strongs/g/g3813.md) to [erchomai](../../strongs/g/g2064.md) unto me, and [kōlyō](../../strongs/g/g2967.md) them not: for of such is the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).**

<a name="luke_18_17"></a>Luke 18:17

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, Whosoever shall not [dechomai](../../strongs/g/g1209.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) as a [paidion](../../strongs/g/g3813.md) shall in no wise [eiserchomai](../../strongs/g/g1525.md) therein.**

<a name="luke_18_18"></a>Luke 18:18

And a certain [archōn](../../strongs/g/g758.md) [eperōtaō](../../strongs/g/g1905.md) him, [legō](../../strongs/g/g3004.md), [agathos](../../strongs/g/g18.md) [didaskalos](../../strongs/g/g1320.md), what shall I [poieō](../../strongs/g/g4160.md) to [klēronomeō](../../strongs/g/g2816.md) [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md)?

<a name="luke_18_19"></a>Luke 18:19

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **Why [legō](../../strongs/g/g3004.md) me [agathos](../../strongs/g/g18.md)? none is [agathos](../../strongs/g/g18.md), save one, [theos](../../strongs/g/g2316.md).**

<a name="luke_18_20"></a>Luke 18:20

**Thou [eidō](../../strongs/g/g1492.md) the [entolē](../../strongs/g/g1785.md), Do not [moicheuō](../../strongs/g/g3431.md), Do not [phoneuō](../../strongs/g/g5407.md), Do not [kleptō](../../strongs/g/g2813.md), Do not [pseudomartyreō](../../strongs/g/g5576.md), [timaō](../../strongs/g/g5091.md) thy [patēr](../../strongs/g/g3962.md) and thy [mētēr](../../strongs/g/g3384.md).**

<a name="luke_18_21"></a>Luke 18:21

And he [eipon](../../strongs/g/g2036.md), All these have I [phylassō](../../strongs/g/g5442.md) from my [neotēs](../../strongs/g/g3503.md).

<a name="luke_18_22"></a>Luke 18:22

Now when [Iēsous](../../strongs/g/g2424.md) [akouō](../../strongs/g/g191.md) these things, he [eipon](../../strongs/g/g2036.md) unto him, **Yet [leipō](../../strongs/g/g3007.md) thou one thing: [pōleō](../../strongs/g/g4453.md) all that thou hast, and [diadidōmi](../../strongs/g/g1239.md) unto the [ptōchos](../../strongs/g/g4434.md), and thou shalt have [thēsauros](../../strongs/g/g2344.md) in [ouranos](../../strongs/g/g3772.md): and [deuro](../../strongs/g/g1204.md), [akoloutheō](../../strongs/g/g190.md) me.**

<a name="luke_18_23"></a>Luke 18:23

And when he [akouō](../../strongs/g/g191.md) this, he was [perilypos](../../strongs/g/g4036.md): for he was [sphodra](../../strongs/g/g4970.md) [plousios](../../strongs/g/g4145.md).

<a name="luke_18_24"></a>Luke 18:24

And when [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) that he was [perilypos](../../strongs/g/g4036.md), he [eipon](../../strongs/g/g2036.md), **How [dyskolōs](../../strongs/g/g1423.md) shall they that have [chrēma](../../strongs/g/g5536.md) [eiserchomai](../../strongs/g/g1525.md) into the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md)!**

<a name="luke_18_25"></a>Luke 18:25

**For it is [eukopos](../../strongs/g/g2123.md) for a [kamēlos](../../strongs/g/g2574.md) to [eiserchomai](../../strongs/g/g1525.md) through a [rhaphis](../../strongs/g/g4476.md) [trymalia](../../strongs/g/g5168.md), than for a [plousios](../../strongs/g/g4145.md) to [eiserchomai](../../strongs/g/g1525.md) into the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).**

<a name="luke_18_26"></a>Luke 18:26

And they that [akouō](../../strongs/g/g191.md) [eipon](../../strongs/g/g2036.md), Who then can be [sōzō](../../strongs/g/g4982.md)?

<a name="luke_18_27"></a>Luke 18:27

And he [eipon](../../strongs/g/g2036.md), **[adynatos](../../strongs/g/g102.md) with [anthrōpos](../../strongs/g/g444.md) are [dynatos](../../strongs/g/g1415.md) with [theos](../../strongs/g/g2316.md).**

<a name="luke_18_28"></a>Luke 18:28

Then [Petros](../../strongs/g/g4074.md) [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), we have [aphiēmi](../../strongs/g/g863.md) all, and [akoloutheō](../../strongs/g/g190.md) thee.

<a name="luke_18_29"></a>Luke 18:29

And he [eipon](../../strongs/g/g2036.md) unto them, **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, There is [oudeis](../../strongs/g/g3762.md) that hath [aphiēmi](../../strongs/g/g863.md) [oikia](../../strongs/g/g3614.md), or [goneus](../../strongs/g/g1118.md), or [adelphos](../../strongs/g/g80.md), or [gynē](../../strongs/g/g1135.md), or [teknon](../../strongs/g/g5043.md), for the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) [heneka](../../strongs/g/g1752.md),**

<a name="luke_18_30"></a>Luke 18:30

**Who shall not [apolambanō](../../strongs/g/g618.md) [pollaplasiōn](../../strongs/g/g4179.md) in this [kairos](../../strongs/g/g2540.md), and in the [aiōn](../../strongs/g/g165.md) to [erchomai](../../strongs/g/g2064.md) [zōē](../../strongs/g/g2222.md) [aiōnios](../../strongs/g/g166.md).**

<a name="luke_18_31"></a>Luke 18:31

Then he [paralambanō](../../strongs/g/g3880.md) the [dōdeka](../../strongs/g/g1427.md), and [eipon](../../strongs/g/g2036.md) unto them, **[idou](../../strongs/g/g2400.md), we [anabainō](../../strongs/g/g305.md) to [Hierosolyma](../../strongs/g/g2414.md), and all things that are [graphō](../../strongs/g/g1125.md) by the [prophētēs](../../strongs/g/g4396.md) concerning the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) shall be [teleō](../../strongs/g/g5055.md).**

<a name="luke_18_32"></a>Luke 18:32

**For he shall be [paradidōmi](../../strongs/g/g3860.md) unto the [ethnos](../../strongs/g/g1484.md), and shall be [empaizō](../../strongs/g/g1702.md), and [hybrizō](../../strongs/g/g5195.md), and [emptyō](../../strongs/g/g1716.md):**

<a name="luke_18_33"></a>Luke 18:33

**And they shall [mastigoō](../../strongs/g/g3146.md), and [apokteinō](../../strongs/g/g615.md) him: and the third [hēmera](../../strongs/g/g2250.md) he shall [anistēmi](../../strongs/g/g450.md).**

<a name="luke_18_34"></a>Luke 18:34

And they [syniēmi](../../strongs/g/g4920.md) none of these things: and this [rhēma](../../strongs/g/g4487.md) was [kryptō](../../strongs/g/g2928.md) from them, neither [ginōskō](../../strongs/g/g1097.md) they the things which were [legō](../../strongs/g/g3004.md).

<a name="luke_18_35"></a>Luke 18:35

And [ginomai](../../strongs/g/g1096.md), that as he was [eggizō](../../strongs/g/g1448.md) unto [Ierichō](../../strongs/g/g2410.md), a certain [typhlos](../../strongs/g/g5185.md) [kathēmai](../../strongs/g/g2521.md) by the [hodos](../../strongs/g/g3598.md) [prosaiteō](../../strongs/g/g4319.md):

<a name="luke_18_36"></a>Luke 18:36

And [akouō](../../strongs/g/g191.md) the [ochlos](../../strongs/g/g3793.md) [diaporeuomai](../../strongs/g/g1279.md), he [pynthanomai](../../strongs/g/g4441.md) what it meant.

<a name="luke_18_37"></a>Luke 18:37

And they [apaggellō](../../strongs/g/g518.md) him, that [Iēsous](../../strongs/g/g2424.md) of [Nazōraios](../../strongs/g/g3480.md) [parerchomai](../../strongs/g/g3928.md).

<a name="luke_18_38"></a>Luke 18:38

And he [boaō](../../strongs/g/g994.md), [legō](../../strongs/g/g3004.md), [Iēsous](../../strongs/g/g2424.md), [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md), have [eleeō](../../strongs/g/g1653.md) on me.

<a name="luke_18_39"></a>Luke 18:39

And [proagō](../../strongs/g/g4254.md) [epitimaō](../../strongs/g/g2008.md) him, that he should [siōpaō](../../strongs/g/g4623.md): but he [krazō](../../strongs/g/g2896.md) so [polys](../../strongs/g/g4183.md) the [mallon](../../strongs/g/g3123.md), [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md), have [eleeō](../../strongs/g/g1653.md) on me.

<a name="luke_18_40"></a>Luke 18:40

And [Iēsous](../../strongs/g/g2424.md) [histēmi](../../strongs/g/g2476.md), and [keleuō](../../strongs/g/g2753.md) him to be [agō](../../strongs/g/g71.md) unto him: and when he was [eggizō](../../strongs/g/g1448.md), he [eperōtaō](../../strongs/g/g1905.md) him,

<a name="luke_18_41"></a>Luke 18:41

[legō](../../strongs/g/g3004.md), **What wilt thou that I shall [poieō](../../strongs/g/g4160.md) unto thee?** And he [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), that I may [anablepō](../../strongs/g/g308.md).

<a name="luke_18_42"></a>Luke 18:42

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **[anablepō](../../strongs/g/g308.md): thy [pistis](../../strongs/g/g4102.md) hath [sōzō](../../strongs/g/g4982.md) thee.**

<a name="luke_18_43"></a>Luke 18:43

And [parachrēma](../../strongs/g/g3916.md) he [anablepō](../../strongs/g/g308.md), and [akoloutheō](../../strongs/g/g190.md) him, [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md): and all the [laos](../../strongs/g/g2992.md), when they [eidō](../../strongs/g/g1492.md), [didōmi](../../strongs/g/g1325.md) [ainos](../../strongs/g/g136.md) unto [theos](../../strongs/g/g2316.md).

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 17](luke_17.md) - [Luke 19](luke_19.md)