# [Luke 20](https://www.blueletterbible.org/kjv/luk/20/1/rl1/s_993001)

<a name="luke_20_1"></a>Luke 20:1

And [ginomai](../../strongs/g/g1096.md), on one of those [hēmera](../../strongs/g/g2250.md), as he [didaskō](../../strongs/g/g1321.md) the [laos](../../strongs/g/g2992.md) in the [hieron](../../strongs/g/g2411.md), and [euaggelizō](../../strongs/g/g2097.md), the [archiereus](../../strongs/g/g749.md) and the [grammateus](../../strongs/g/g1122.md) [ephistēmi](../../strongs/g/g2186.md) with the [presbyteros](../../strongs/g/g4245.md),

<a name="luke_20_2"></a>Luke 20:2

And [eipon](../../strongs/g/g2036.md) unto him, [legō](../../strongs/g/g3004.md), [eipon](../../strongs/g/g2036.md) us, by what [exousia](../../strongs/g/g1849.md) [poieō](../../strongs/g/g4160.md) thou these things? or who is he that [didōmi](../../strongs/g/g1325.md) thee this [exousia](../../strongs/g/g1849.md)?

<a name="luke_20_3"></a>Luke 20:3

And he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **I will also [erōtaō](../../strongs/g/g2065.md) you one [logos](../../strongs/g/g3056.md); and [eipon](../../strongs/g/g2036.md) me:**

<a name="luke_20_4"></a>Luke 20:4

**The [baptisma](../../strongs/g/g908.md) of [Iōannēs](../../strongs/g/g2491.md), was it from [ouranos](../../strongs/g/g3772.md), or of [anthrōpos](../../strongs/g/g444.md)?**

<a name="luke_20_5"></a>Luke 20:5

And they [syllogizomai](../../strongs/g/g4817.md) with [heautou](../../strongs/g/g1438.md), [legō](../../strongs/g/g3004.md), If we shall [eipon](../../strongs/g/g2036.md), From [ouranos](../../strongs/g/g3772.md); he will [eipon](../../strongs/g/g2046.md), Why then [pisteuō](../../strongs/g/g4100.md) ye him not?

<a name="luke_20_6"></a>Luke 20:6

But and if we [eipon](../../strongs/g/g2036.md), Of [anthrōpos](../../strongs/g/g444.md); all the [laos](../../strongs/g/g2992.md) will [katalithazō](../../strongs/g/g2642.md) us: for they be [peithō](../../strongs/g/g3982.md) that [Iōannēs](../../strongs/g/g2491.md) was a [prophētēs](../../strongs/g/g4396.md).

<a name="luke_20_7"></a>Luke 20:7

And they [apokrinomai](../../strongs/g/g611.md), that they [eidō](../../strongs/g/g1492.md) not [pothen](../../strongs/g/g4159.md).

<a name="luke_20_8"></a>Luke 20:8

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **Neither [legō](../../strongs/g/g3004.md) I you by what [exousia](../../strongs/g/g1849.md) I [poieō](../../strongs/g/g4160.md) these things.**

<a name="luke_20_9"></a>Luke 20:9

Then [archomai](../../strongs/g/g756.md) he to [legō](../../strongs/g/g3004.md) to the [laos](../../strongs/g/g2992.md) this [parabolē](../../strongs/g/g3850.md); **A certain [anthrōpos](../../strongs/g/g444.md) [phyteuō](../../strongs/g/g5452.md) an [ampelōn](../../strongs/g/g290.md), and [ekdidōmi](../../strongs/g/g1554.md) it to [geōrgos](../../strongs/g/g1092.md), and [apodēmeō](../../strongs/g/g589.md) for a [hikanos](../../strongs/g/g2425.md) [chronos](../../strongs/g/g5550.md).**

<a name="luke_20_10"></a>Luke 20:10

**And at the [kairos](../../strongs/g/g2540.md) he [apostellō](../../strongs/g/g649.md) a [doulos](../../strongs/g/g1401.md) to the [geōrgos](../../strongs/g/g1092.md), that they should [didōmi](../../strongs/g/g1325.md) him of the [karpos](../../strongs/g/g2590.md) of the [ampelōn](../../strongs/g/g290.md): but the [geōrgos](../../strongs/g/g1092.md) [derō](../../strongs/g/g1194.md) him, and [exapostellō](../../strongs/g/g1821.md) [kenos](../../strongs/g/g2756.md).**

<a name="luke_20_11"></a>Luke 20:11

**And [prostithēmi](../../strongs/g/g4369.md) he [pempō](../../strongs/g/g3992.md) another [doulos](../../strongs/g/g1401.md): and they [derō](../../strongs/g/g1194.md) him also, and [atimazō](../../strongs/g/g818.md), and [exapostellō](../../strongs/g/g1821.md) [kenos](../../strongs/g/g2756.md).**

<a name="luke_20_12"></a>Luke 20:12

**And [prostithēmi](../../strongs/g/g4369.md) he [pempō](../../strongs/g/g3992.md) a third: and they [traumatizō](../../strongs/g/g5135.md) him also, and [ekballō](../../strongs/g/g1544.md).**

<a name="luke_20_13"></a>Luke 20:13

**Then [eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md) of the [ampelōn](../../strongs/g/g290.md), What shall I [poieō](../../strongs/g/g4160.md)? I will [pempō](../../strongs/g/g3992.md) my [agapētos](../../strongs/g/g27.md) [huios](../../strongs/g/g5207.md): it [isōs](../../strongs/g/g2481.md) they will [entrepō](../../strongs/g/g1788.md) when they [eidō](../../strongs/g/g1492.md) him.**

<a name="luke_20_14"></a>Luke 20:14

**But when the [geōrgos](../../strongs/g/g1092.md) [eidō](../../strongs/g/g1492.md) him, they [dialogizomai](../../strongs/g/g1260.md) among themselves, [legō](../../strongs/g/g3004.md), This is the [klēronomos](../../strongs/g/g2818.md): [deute](../../strongs/g/g1205.md), let us [apokteinō](../../strongs/g/g615.md) him, that the [klēronomia](../../strongs/g/g2817.md) may be ours.**

<a name="luke_20_15"></a>Luke 20:15

**So they [ekballō](../../strongs/g/g1544.md) him out of the [ampelōn](../../strongs/g/g290.md), and [apokteinō](../../strongs/g/g615.md). What therefore shall the [kyrios](../../strongs/g/g2962.md) of the [ampelōn](../../strongs/g/g290.md) [poieō](../../strongs/g/g4160.md) unto them?**

<a name="luke_20_16"></a>Luke 20:16

**He shall [erchomai](../../strongs/g/g2064.md) and [apollymi](../../strongs/g/g622.md) these [geōrgos](../../strongs/g/g1092.md), and shall [didōmi](../../strongs/g/g1325.md) the [ampelōn](../../strongs/g/g290.md) to others.** And when they [akouō](../../strongs/g/g191.md), they [eipon](../../strongs/g/g2036.md), [mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md).

<a name="luke_20_17"></a>Luke 20:17

And he [emblepō](../../strongs/g/g1689.md) them, and [eipon](../../strongs/g/g2036.md), **What is this then that is [graphō](../../strongs/g/g1125.md), The [lithos](../../strongs/g/g3037.md) which the [oikodomeō](../../strongs/g/g3618.md) [apodokimazō](../../strongs/g/g593.md), the same is become the [kephalē](../../strongs/g/g2776.md) of the [gōnia](../../strongs/g/g1137.md)?**

<a name="luke_20_18"></a>Luke 20:18

**Whosoever shall [piptō](../../strongs/g/g4098.md) upon that [lithos](../../strongs/g/g3037.md) shall be [synthlaō](../../strongs/g/g4917.md); but on whomsoever it shall [piptō](../../strongs/g/g4098.md), it will [likmaō](../../strongs/g/g3039.md) him.**

<a name="luke_20_19"></a>Luke 20:19

And the [archiereus](../../strongs/g/g749.md) and the [grammateus](../../strongs/g/g1122.md) the same [hōra](../../strongs/g/g5610.md) [zēteō](../../strongs/g/g2212.md) to [epiballō](../../strongs/g/g1911.md) [cheir](../../strongs/g/g5495.md) on him; and they [phobeō](../../strongs/g/g5399.md) the [laos](../../strongs/g/g2992.md): for they [ginōskō](../../strongs/g/g1097.md) that he had [eipon](../../strongs/g/g2036.md) this [parabolē](../../strongs/g/g3850.md) against them.

<a name="luke_20_20"></a>Luke 20:20

And they [paratēreō](../../strongs/g/g3906.md), and [apostellō](../../strongs/g/g649.md) [egkathetos](../../strongs/g/g1455.md), which should [hypokrinomai](../../strongs/g/g5271.md) [einai](../../strongs/g/g1511.md) themselves [dikaios](../../strongs/g/g1342.md), that they might [epilambanomai](../../strongs/g/g1949.md) of his [logos](../../strongs/g/g3056.md), that so they might [paradidōmi](../../strongs/g/g3860.md) him unto the [archē](../../strongs/g/g746.md) and [exousia](../../strongs/g/g1849.md) of the [hēgemōn](../../strongs/g/g2232.md).

<a name="luke_20_21"></a>Luke 20:21

And they [eperōtaō](../../strongs/g/g1905.md) him, [legō](../../strongs/g/g3004.md), [didaskalos](../../strongs/g/g1320.md), we [eidō](../../strongs/g/g1492.md) that thou [legō](../../strongs/g/g3004.md) and [didaskō](../../strongs/g/g1321.md) [orthōs](../../strongs/g/g3723.md), neither [lambanō](../../strongs/g/g2983.md) [prosōpon](../../strongs/g/g4383.md), but [didaskō](../../strongs/g/g1321.md) the [hodos](../../strongs/g/g3598.md) of [theos](../../strongs/g/g2316.md) [alētheia](../../strongs/g/g225.md):

<a name="luke_20_22"></a>Luke 20:22

Is it [exesti](../../strongs/g/g1832.md) for us to [didōmi](../../strongs/g/g1325.md) [phoros](../../strongs/g/g5411.md) unto [Kaisar](../../strongs/g/g2541.md), or no?

<a name="luke_20_23"></a>Luke 20:23

But he [katanoeō](../../strongs/g/g2657.md) their [panourgia](../../strongs/g/g3834.md), and [eipon](../../strongs/g/g2036.md) unto them, **Why [peirazō](../../strongs/g/g3985.md) me?**

<a name="luke_20_24"></a>Luke 20:24

**[epideiknymi](../../strongs/g/g1925.md) me a [dēnarion](../../strongs/g/g1220.md). Whose [eikōn](../../strongs/g/g1504.md) and [epigraphē](../../strongs/g/g1923.md) hath it?** They [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), [Kaisar](../../strongs/g/g2541.md).

<a name="luke_20_25"></a>Luke 20:25

And he [eipon](../../strongs/g/g2036.md) unto them, **[apodidōmi](../../strongs/g/g591.md) therefore unto [Kaisar](../../strongs/g/g2541.md) the things which be [Kaisar](../../strongs/g/g2541.md), and unto [theos](../../strongs/g/g2316.md) the things which be [theos](../../strongs/g/g2316.md).**

<a name="luke_20_26"></a>Luke 20:26

And they could not [epilambanomai](../../strongs/g/g1949.md) of his [rhēma](../../strongs/g/g4487.md) [enantion](../../strongs/g/g1726.md) the [laos](../../strongs/g/g2992.md): and they [thaumazō](../../strongs/g/g2296.md) at his [apokrisis](../../strongs/g/g612.md), and [sigaō](../../strongs/g/g4601.md).

<a name="luke_20_27"></a>Luke 20:27

Then [proserchomai](../../strongs/g/g4334.md) [tis](../../strongs/g/g5100.md) [Saddoukaios](../../strongs/g/g4523.md), which [antilegō](../../strongs/g/g483.md) that there is any [anastasis](../../strongs/g/g386.md); and they [eperōtaō](../../strongs/g/g1905.md) him,

<a name="luke_20_28"></a>Luke 20:28

[legō](../../strongs/g/g3004.md), [didaskalos](../../strongs/g/g1320.md), [Mōÿsēs](../../strongs/g/g3475.md) [graphō](../../strongs/g/g1125.md) unto us, If [tis](../../strongs/g/g5100.md) [adelphos](../../strongs/g/g80.md) [apothnēskō](../../strongs/g/g599.md), having a [gynē](../../strongs/g/g1135.md), and he [apothnēskō](../../strongs/g/g599.md) [ateknos](../../strongs/g/g815.md), that his [adelphos](../../strongs/g/g80.md) should [lambanō](../../strongs/g/g2983.md) his [gynē](../../strongs/g/g1135.md), and [exanistēmi](../../strongs/g/g1817.md) [sperma](../../strongs/g/g4690.md) unto his [adelphos](../../strongs/g/g80.md).

<a name="luke_20_29"></a>Luke 20:29

There were therefore seven [adelphos](../../strongs/g/g80.md): and the first [lambanō](../../strongs/g/g2983.md) a [gynē](../../strongs/g/g1135.md), and [apothnēskō](../../strongs/g/g599.md) [ateknos](../../strongs/g/g815.md).

<a name="luke_20_30"></a>Luke 20:30

And the second [lambanō](../../strongs/g/g2983.md) her to [gynē](../../strongs/g/g1135.md), and he [apothnēskō](../../strongs/g/g599.md) [ateknos](../../strongs/g/g815.md).

<a name="luke_20_31"></a>Luke 20:31

And the third [lambanō](../../strongs/g/g2983.md) her; and [hōsautōs](../../strongs/g/g5615.md) the seven also: and they [kataleipō](../../strongs/g/g2641.md) no [teknon](../../strongs/g/g5043.md), and [apothnēskō](../../strongs/g/g599.md).

<a name="luke_20_32"></a>Luke 20:32

Last of all the [gynē](../../strongs/g/g1135.md) [apothnēskō](../../strongs/g/g599.md) also.

<a name="luke_20_33"></a>Luke 20:33

Therefore in the [anastasis](../../strongs/g/g386.md) whose [gynē](../../strongs/g/g1135.md) of them is she? for seven had her to [gynē](../../strongs/g/g1135.md).

<a name="luke_20_34"></a>Luke 20:34

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto them, **The [huios](../../strongs/g/g5207.md) of this [aiōn](../../strongs/g/g165.md) [gameō](../../strongs/g/g1060.md), and are [ekgamiskō](../../strongs/g/g1548.md):**

<a name="luke_20_35"></a>Luke 20:35

**But [kataxioō](../../strongs/g/g2661.md) to [tygchanō](../../strongs/g/g5177.md) that [aiōn](../../strongs/g/g165.md), and the [anastasis](../../strongs/g/g386.md) from the [nekros](../../strongs/g/g3498.md), neither [gameō](../../strongs/g/g1060.md), nor [ekgamiskō](../../strongs/g/g1548.md):**

<a name="luke_20_36"></a>Luke 20:36

**Neither can they [apothnēskō](../../strongs/g/g599.md) any more: for they are [isaggelos](../../strongs/g/g2465.md); and are the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), being the [huios](../../strongs/g/g5207.md) of the [anastasis](../../strongs/g/g386.md).**

<a name="luke_20_37"></a>Luke 20:37

**Now that the [nekros](../../strongs/g/g3498.md) are [egeirō](../../strongs/g/g1453.md), even [Mōÿsēs](../../strongs/g/g3475.md) [mēnyō](../../strongs/g/g3377.md) at the [batos](../../strongs/g/g942.md), when he [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md) the [theos](../../strongs/g/g2316.md) of [Abraam](../../strongs/g/g11.md), and the [theos](../../strongs/g/g2316.md) of [Isaak](../../strongs/g/g2464.md), and the [theos](../../strongs/g/g2316.md) of [Iakōb](../../strongs/g/g2384.md).**

<a name="luke_20_38"></a>Luke 20:38

**For he is not a [theos](../../strongs/g/g2316.md) of the [nekros](../../strongs/g/g3498.md), but of the [zaō](../../strongs/g/g2198.md): for all [zaō](../../strongs/g/g2198.md) unto him.**

<a name="luke_20_39"></a>Luke 20:39

Then certain of the [grammateus](../../strongs/g/g1122.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md), [didaskalos](../../strongs/g/g1320.md), thou hast [kalōs](../../strongs/g/g2573.md) [eipon](../../strongs/g/g2036.md).

<a name="luke_20_40"></a>Luke 20:40

And after that they [tolmaō](../../strongs/g/g5111.md) not [eperōtaō](../../strongs/g/g1905.md) him any.

<a name="luke_20_41"></a>Luke 20:41

And he [eipon](../../strongs/g/g2036.md) unto them, **How [legō](../../strongs/g/g3004.md) they that [Christos](../../strongs/g/g5547.md) is [Dabid](../../strongs/g/g1138.md) [huios](../../strongs/g/g5207.md)?**

<a name="luke_20_42"></a>Luke 20:42

**And [Dabid](../../strongs/g/g1138.md) himself [legō](../../strongs/g/g3004.md) in the [biblos](../../strongs/g/g976.md) of [psalmos](../../strongs/g/g5568.md), The [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md) unto my [kyrios](../../strongs/g/g2962.md), [kathēmai](../../strongs/g/g2521.md) thou on my [dexios](../../strongs/g/g1188.md),**

<a name="luke_20_43"></a>Luke 20:43

**Till I [tithēmi](../../strongs/g/g5087.md) thine [echthros](../../strongs/g/g2190.md) thy [pous](../../strongs/g/g4228.md) [hypopodion](../../strongs/g/g5286.md).**

<a name="luke_20_44"></a>Luke 20:44

**[Dabid](../../strongs/g/g1138.md) therefore [kaleō](../../strongs/g/g2564.md) him [kyrios](../../strongs/g/g2962.md), how is he then his [huios](../../strongs/g/g5207.md)?**

<a name="luke_20_45"></a>Luke 20:45

Then [akouō](../../strongs/g/g191.md) of all the [laos](../../strongs/g/g2992.md) he [eipon](../../strongs/g/g2036.md) unto his [mathētēs](../../strongs/g/g3101.md),

<a name="luke_20_46"></a>Luke 20:46

**[prosechō](../../strongs/g/g4337.md) of the [grammateus](../../strongs/g/g1122.md), which [thelō](../../strongs/g/g2309.md) to [peripateō](../../strongs/g/g4043.md) in [stolē](../../strongs/g/g4749.md), and [phileō](../../strongs/g/g5368.md) [aspasmos](../../strongs/g/g783.md) in the [agora](../../strongs/g/g58.md), and the [prōtokathedria](../../strongs/g/g4410.md) in the [synagōgē](../../strongs/g/g4864.md), and the [prōtoklisia](../../strongs/g/g4411.md) at [deipnon](../../strongs/g/g1173.md);**

<a name="luke_20_47"></a>Luke 20:47

**Which [katesthiō](../../strongs/g/g2719.md) [chēra](../../strongs/g/g5503.md) [oikia](../../strongs/g/g3614.md), and [prophasis](../../strongs/g/g4392.md) [proseuchomai](../../strongs/g/g4336.md) [makros](../../strongs/g/g3117.md): the same shall [lambanō](../../strongs/g/g2983.md) [perissoteros](../../strongs/g/g4055.md) [krima](../../strongs/g/g2917.md).**

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 19](luke_19.md) - [Luke 21](luke_21.md)