# Luke

[Luke Overview](../../commentary/luke/luke_overview.md)

[Luke 1](luke_1.md)

[Luke 2](luke_2.md)

[Luke 3](luke_3.md)

[Luke 4](luke_4.md)

[Luke 5](luke_5.md)

[Luke 6](luke_6.md)

[Luke 7](luke_7.md)

[Luke 8](luke_8.md)

[Luke 9](luke_9.md)

[Luke 10](luke_10.md)

[Luke 11](luke_11.md)

[Luke 12](luke_12.md)

[Luke 13](luke_13.md)

[Luke 14](luke_14.md)

[Luke 15](luke_15.md)

[Luke 16](luke_16.md)

[Luke 17](luke_17.md)

[Luke 18](luke_18.md)

[Luke 19](luke_19.md)

[Luke 20](luke_20.md)

[Luke 21](luke_21.md)

[Luke 22](luke_22.md)

[Luke 23](luke_23.md)

[Luke 24](luke_24.md)

---

[Transliteral Bible](../index.md)