# [Luke 24](https://www.blueletterbible.org/kjv/luk/24/1/rl1/s_997001)

<a name="luke_24_1"></a>Luke 24:1

Now upon the first [sabbaton](../../strongs/g/g4521.md), [orthros](../../strongs/g/g3722.md) [bathys](../../strongs/g/g901.md), they [erchomai](../../strongs/g/g2064.md) unto the [mnēma](../../strongs/g/g3418.md), [pherō](../../strongs/g/g5342.md) the [arōma](../../strongs/g/g759.md) which they had [hetoimazō](../../strongs/g/g2090.md), and certain with them.

<a name="luke_24_2"></a>Luke 24:2

And they [heuriskō](../../strongs/g/g2147.md) the [lithos](../../strongs/g/g3037.md) [apokyliō](../../strongs/g/g617.md) from the [mnēmeion](../../strongs/g/g3419.md).

<a name="luke_24_3"></a>Luke 24:3

And they [eiserchomai](../../strongs/g/g1525.md), and [heuriskō](../../strongs/g/g2147.md) not the [sōma](../../strongs/g/g4983.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md).

<a name="luke_24_4"></a>Luke 24:4

And [ginomai](../../strongs/g/g1096.md), as they were much [diaporeō](../../strongs/g/g1280.md) thereabout, [idou](../../strongs/g/g2400.md), two [anēr](../../strongs/g/g435.md) [ephistēmi](../../strongs/g/g2186.md) them in [astraptō](../../strongs/g/g797.md) [esthēsis](../../strongs/g/g2067.md):

<a name="luke_24_5"></a>Luke 24:5

And as they were [emphobos](../../strongs/g/g1719.md), and [klinō](../../strongs/g/g2827.md) [prosōpon](../../strongs/g/g4383.md) to the [gē](../../strongs/g/g1093.md), they [eipon](../../strongs/g/g2036.md) unto them, Why [zēteō](../../strongs/g/g2212.md) the [zaō](../../strongs/g/g2198.md) among the [nekros](../../strongs/g/g3498.md)?

<a name="luke_24_6"></a>Luke 24:6

He is not [hōde](../../strongs/g/g5602.md), but [egeirō](../../strongs/g/g1453.md): [mnaomai](../../strongs/g/g3415.md) how he [laleō](../../strongs/g/g2980.md) unto you when he was yet in [Galilaia](../../strongs/g/g1056.md),

<a name="luke_24_7"></a>Luke 24:7

[legō](../../strongs/g/g3004.md), **The [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) must be [paradidōmi](../../strongs/g/g3860.md) into the [cheir](../../strongs/g/g5495.md) of [hamartōlos](../../strongs/g/g268.md) [anthrōpos](../../strongs/g/g444.md), and be [stauroō](../../strongs/g/g4717.md), and the third [hēmera](../../strongs/g/g2250.md) [anistēmi](../../strongs/g/g450.md).**

<a name="luke_24_8"></a>Luke 24:8

And they [mnaomai](../../strongs/g/g3415.md) his [rhēma](../../strongs/g/g4487.md),

<a name="luke_24_9"></a>Luke 24:9

And [hypostrephō](../../strongs/g/g5290.md) from the [mnēmeion](../../strongs/g/g3419.md), and [apaggellō](../../strongs/g/g518.md) all these things unto the [hendeka](../../strongs/g/g1733.md), and to all [loipos](../../strongs/g/g3062.md).

<a name="luke_24_10"></a>Luke 24:10

It was [Maria](../../strongs/g/g3137.md) [Magdalēnē](../../strongs/g/g3094.md) and [Iōan(n)a](../../strongs/g/g2489.md), and [Maria](../../strongs/g/g3137.md) of [Iakōbos](../../strongs/g/g2385.md), and [loipos](../../strongs/g/g3062.md) with them, which [legō](../../strongs/g/g3004.md) these things unto the [apostolos](../../strongs/g/g652.md).

<a name="luke_24_11"></a>Luke 24:11

And their [rhēma](../../strongs/g/g4487.md) [phainō](../../strongs/g/g5316.md) to them as [lēros](../../strongs/g/g3026.md), and they [apisteō](../../strongs/g/g569.md) them.

<a name="luke_24_12"></a>Luke 24:12

Then [anistēmi](../../strongs/g/g450.md) [Petros](../../strongs/g/g4074.md), and [trechō](../../strongs/g/g5143.md) unto the [mnēmeion](../../strongs/g/g3419.md); and [parakyptō](../../strongs/g/g3879.md), he [blepō](../../strongs/g/g991.md) the [othonion](../../strongs/g/g3608.md) [keimai](../../strongs/g/g2749.md) [monos](../../strongs/g/g3441.md), and [aperchomai](../../strongs/g/g565.md), [thaumazō](../../strongs/g/g2296.md) in himself [ginomai](../../strongs/g/g1096.md).

<a name="luke_24_13"></a>Luke 24:13

And, [idou](../../strongs/g/g2400.md), two of them [poreuō](../../strongs/g/g4198.md) that same [hēmera](../../strongs/g/g2250.md) to a [kōmē](../../strongs/g/g2968.md) [onoma](../../strongs/g/g3686.md) [Emmaous](../../strongs/g/g1695.md), which was from [Ierousalēm](../../strongs/g/g2419.md) threescore [stadion](../../strongs/g/g4712.md).

<a name="luke_24_14"></a>Luke 24:14

And they [homileō](../../strongs/g/g3656.md) [allēlōn](../../strongs/g/g240.md) of all these things which had [symbainō](../../strongs/g/g4819.md).

<a name="luke_24_15"></a>Luke 24:15

And [ginomai](../../strongs/g/g1096.md), while they [homileō](../../strongs/g/g3656.md) and [syzēteō](../../strongs/g/g4802.md), [Iēsous](../../strongs/g/g2424.md) himself [eggizō](../../strongs/g/g1448.md), and [symporeuomai](../../strongs/g/g4848.md) them.

<a name="luke_24_16"></a>Luke 24:16

But their [ophthalmos](../../strongs/g/g3788.md) were [krateō](../../strongs/g/g2902.md) that they should not [epiginōskō](../../strongs/g/g1921.md) him.

<a name="luke_24_17"></a>Luke 24:17

And he [eipon](../../strongs/g/g2036.md) unto them, **What [logos](../../strongs/g/g3056.md) these that ye [antiballō](../../strongs/g/g474.md) to [allēlōn](../../strongs/g/g240.md), as ye [peripateō](../../strongs/g/g4043.md), and are [skythrōpos](../../strongs/g/g4659.md)?**

<a name="luke_24_18"></a>Luke 24:18

And the one of them, whose [onoma](../../strongs/g/g3686.md) was [Kleopas](../../strongs/g/g2810.md), [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto him, Art thou [monos](../../strongs/g/g3441.md) [paroikeō](../../strongs/g/g3939.md) in [Ierousalēm](../../strongs/g/g2419.md), and hast not [ginōskō](../../strongs/g/g1097.md) [ginomai](../../strongs/g/g1096.md) there in these [hēmera](../../strongs/g/g2250.md)?

<a name="luke_24_19"></a>Luke 24:19

And he [eipon](../../strongs/g/g2036.md) unto them, **[poios](../../strongs/g/g4169.md)?** And they [eipon](../../strongs/g/g2036.md) unto him, Concerning [Iēsous](../../strongs/g/g2424.md) of [Nazōraios](../../strongs/g/g3480.md), which was a [prophētēs](../../strongs/g/g4396.md) [dynatos](../../strongs/g/g1415.md) [anēr](../../strongs/g/g435.md) in [ergon](../../strongs/g/g2041.md) and [logos](../../strongs/g/g3056.md) [enantion](../../strongs/g/g1726.md) [theos](../../strongs/g/g2316.md) and all the [laos](../../strongs/g/g2992.md):

<a name="luke_24_20"></a>Luke 24:20

And how the [archiereus](../../strongs/g/g749.md) and our [archōn](../../strongs/g/g758.md) [paradidōmi](../../strongs/g/g3860.md) him to be [krima](../../strongs/g/g2917.md) to [thanatos](../../strongs/g/g2288.md), and have [stauroō](../../strongs/g/g4717.md) him.

<a name="luke_24_21"></a>Luke 24:21

But we [elpizō](../../strongs/g/g1679.md) that it had been he which should have [lytroō](../../strongs/g/g3084.md) [Israēl](../../strongs/g/g2474.md): and beside all this, [sēmeron](../../strongs/g/g4594.md) [agō](../../strongs/g/g71.md) the third [hēmera](../../strongs/g/g2250.md) since these things [ginomai](../../strongs/g/g1096.md).

<a name="luke_24_22"></a>Luke 24:22

[alla](../../strongs/g/g235.md), and certain [gynē](../../strongs/g/g1135.md) also of our [hēmōn](../../strongs/g/g2257.md) [existēmi](../../strongs/g/g1839.md) us, which were [orthrios](../../strongs/g/g3721.md) at the [mnēmeion](../../strongs/g/g3419.md);

<a name="luke_24_23"></a>Luke 24:23

And when they [heuriskō](../../strongs/g/g2147.md) not his [sōma](../../strongs/g/g4983.md), they [erchomai](../../strongs/g/g2064.md), [legō](../../strongs/g/g3004.md), that they had also [horaō](../../strongs/g/g3708.md) a [optasia](../../strongs/g/g3701.md) of [aggelos](../../strongs/g/g32.md), which [legō](../../strongs/g/g3004.md) that he was [zaō](../../strongs/g/g2198.md).

<a name="luke_24_24"></a>Luke 24:24

And certain of them which were with us [aperchomai](../../strongs/g/g565.md) to the [mnēmeion](../../strongs/g/g3419.md), and [heuriskō](../../strongs/g/g2147.md) even so as the [gynē](../../strongs/g/g1135.md) had [eipon](../../strongs/g/g2036.md): but him they [eidō](../../strongs/g/g1492.md) not.

<a name="luke_24_25"></a>Luke 24:25

Then he [eipon](../../strongs/g/g2036.md) unto them, **[ō](../../strongs/g/g5599.md) [anoētos](../../strongs/g/g453.md), and [bradys](../../strongs/g/g1021.md) of [kardia](../../strongs/g/g2588.md) to [pisteuō](../../strongs/g/g4100.md) all that the [prophētēs](../../strongs/g/g4396.md) have [laleō](../../strongs/g/g2980.md):**

<a name="luke_24_26"></a>Luke 24:26

**Ought not [Christos](../../strongs/g/g5547.md) to have [paschō](../../strongs/g/g3958.md) these things, and to [eiserchomai](../../strongs/g/g1525.md) into his [doxa](../../strongs/g/g1391.md)?**

<a name="luke_24_27"></a>Luke 24:27

And [archomai](../../strongs/g/g756.md) at [Mōÿsēs](../../strongs/g/g3475.md) and all the [prophētēs](../../strongs/g/g4396.md), he [diermēneuō](../../strongs/g/g1329.md) unto them in all the [graphē](../../strongs/g/g1124.md) the things concerning himself.

<a name="luke_24_28"></a>Luke 24:28

And they [eggizō](../../strongs/g/g1448.md) unto the [kōmē](../../strongs/g/g2968.md), whither they [poreuō](../../strongs/g/g4198.md): and he [prospoieō](../../strongs/g/g4364.md) he would have [poreuō](../../strongs/g/g4198.md) [porrōteron](../../strongs/g/g4208.md).

<a name="luke_24_29"></a>Luke 24:29

But they [parabiazomai](../../strongs/g/g3849.md) him, [legō](../../strongs/g/g3004.md), [menō](../../strongs/g/g3306.md) with us: for it is toward [hespera](../../strongs/g/g2073.md), and the [hēmera](../../strongs/g/g2250.md) [klinō](../../strongs/g/g2827.md). And he [eiserchomai](../../strongs/g/g1525.md) in to [menō](../../strongs/g/g3306.md) with them.

<a name="luke_24_30"></a>Luke 24:30

And [ginomai](../../strongs/g/g1096.md), as he [kataklinō](../../strongs/g/g2625.md) with them, he [lambanō](../../strongs/g/g2983.md) [artos](../../strongs/g/g740.md), and [eulogeō](../../strongs/g/g2127.md), and [klaō](../../strongs/g/g2806.md), and [epididōmi](../../strongs/g/g1929.md) to them.

<a name="luke_24_31"></a>Luke 24:31

And their [ophthalmos](../../strongs/g/g3788.md) were [dianoigō](../../strongs/g/g1272.md), and they [epiginōskō](../../strongs/g/g1921.md) him; and he [ginomai](../../strongs/g/g1096.md) [aphantos](../../strongs/g/g855.md) [apo](../../strongs/g/g575.md) [autos](../../strongs/g/g846.md).

<a name="luke_24_32"></a>Luke 24:32

And they [eipon](../../strongs/g/g2036.md) to [allēlōn](../../strongs/g/g240.md), Did not our [kardia](../../strongs/g/g2588.md) [kaiō](../../strongs/g/g2545.md) within us, while he [laleō](../../strongs/g/g2980.md) with us by [hodos](../../strongs/g/g3598.md), and while he [dianoigō](../../strongs/g/g1272.md) to us the [graphē](../../strongs/g/g1124.md)?

<a name="luke_24_33"></a>Luke 24:33

And they [anistēmi](../../strongs/g/g450.md) the same [hōra](../../strongs/g/g5610.md), and [hypostrephō](../../strongs/g/g5290.md) to [Ierousalēm](../../strongs/g/g2419.md), and [heuriskō](../../strongs/g/g2147.md) the eleven [synathroizō](../../strongs/g/g4867.md), and them that were with them,

<a name="luke_24_34"></a>Luke 24:34

[legō](../../strongs/g/g3004.md), The [kyrios](../../strongs/g/g2962.md) is [egeirō](../../strongs/g/g1453.md) [ontōs](../../strongs/g/g3689.md), and hath [optanomai](../../strongs/g/g3700.md) to [Simōn](../../strongs/g/g4613.md).

<a name="luke_24_35"></a>Luke 24:35

And they [exēgeomai](../../strongs/g/g1834.md) [en](../../strongs/g/g1722.md) [hodos](../../strongs/g/g3598.md), and how he was [ginōskō](../../strongs/g/g1097.md) of them in [klasis](../../strongs/g/g2800.md) of [artos](../../strongs/g/g740.md).

<a name="luke_24_36"></a>Luke 24:36

And as they thus [laleō](../../strongs/g/g2980.md), [Iēsous](../../strongs/g/g2424.md) himself [histēmi](../../strongs/g/g2476.md) in the [mesos](../../strongs/g/g3319.md) of them, and [legō](../../strongs/g/g3004.md) unto them, **[eirēnē](../../strongs/g/g1515.md) [hymin](../../strongs/g/g5213.md).**

<a name="luke_24_37"></a>Luke 24:37

But they were [ptoeō](../../strongs/g/g4422.md) and [emphobos](../../strongs/g/g1719.md), and [dokeō](../../strongs/g/g1380.md) that they had [theōreō](../../strongs/g/g2334.md) a [pneuma](../../strongs/g/g4151.md).

<a name="luke_24_38"></a>Luke 24:38

And he [eipon](../../strongs/g/g2036.md) unto them, **Why are ye [tarassō](../../strongs/g/g5015.md)? and why do [dialogismos](../../strongs/g/g1261.md) [anabainō](../../strongs/g/g305.md) in your [kardia](../../strongs/g/g2588.md)?**

<a name="luke_24_39"></a>Luke 24:39

**[eidō](../../strongs/g/g1492.md) my [cheir](../../strongs/g/g5495.md) and my [pous](../../strongs/g/g4228.md), that it is I myself: [psēlaphaō](../../strongs/g/g5584.md) me, and [eidō](../../strongs/g/g1492.md); for a [pneuma](../../strongs/g/g4151.md) hath not [sarx](../../strongs/g/g4561.md) and [osteon](../../strongs/g/g3747.md), as ye [theōreō](../../strongs/g/g2334.md) me have.**

<a name="luke_24_40"></a>Luke 24:40

And when he had thus [eipon](../../strongs/g/g2036.md), he [epideiknymi](../../strongs/g/g1925.md) them [cheir](../../strongs/g/g5495.md) and [pous](../../strongs/g/g4228.md).

<a name="luke_24_41"></a>Luke 24:41

And while they yet [apisteō](../../strongs/g/g569.md) for [chara](../../strongs/g/g5479.md), and [thaumazō](../../strongs/g/g2296.md), he [eipon](../../strongs/g/g2036.md) unto them, **Have ye [enthade](../../strongs/g/g1759.md) any [brōsimos](../../strongs/g/g1034.md)?**

<a name="luke_24_42"></a>Luke 24:42

And they [epididōmi](../../strongs/g/g1929.md) him a [meros](../../strongs/g/g3313.md) of [optos](../../strongs/g/g3702.md) [ichthys](../../strongs/g/g2486.md), and of a [melissios](../../strongs/g/g3193.md) [kērion](../../strongs/g/g2781.md).

<a name="luke_24_43"></a>Luke 24:43

And he [lambanō](../../strongs/g/g2983.md), and did [phago](../../strongs/g/g5315.md) before them.

<a name="luke_24_44"></a>Luke 24:44

And he [eipon](../../strongs/g/g2036.md) unto them, **These the [logos](../../strongs/g/g3056.md) which I [laleō](../../strongs/g/g2980.md) unto you, while I was yet with you, that all things must be [plēroō](../../strongs/g/g4137.md), which were [graphō](../../strongs/g/g1125.md) in the [nomos](../../strongs/g/g3551.md) of [Mōÿsēs](../../strongs/g/g3475.md), and the [prophētēs](../../strongs/g/g4396.md), and the [psalmos](../../strongs/g/g5568.md), concerning me.**

<a name="luke_24_45"></a>Luke 24:45

Then [dianoigō](../../strongs/g/g1272.md) he their [nous](../../strongs/g/g3563.md), that they might [syniēmi](../../strongs/g/g4920.md) the [graphē](../../strongs/g/g1124.md),

<a name="luke_24_46"></a>Luke 24:46

And [eipon](../../strongs/g/g2036.md) unto them, **Thus it is [graphō](../../strongs/g/g1125.md), and thus [dei](../../strongs/g/g1163.md) [Christos](../../strongs/g/g5547.md) to [paschō](../../strongs/g/g3958.md), and to [anistēmi](../../strongs/g/g450.md) from the [nekros](../../strongs/g/g3498.md) the third [hēmera](../../strongs/g/g2250.md):**

<a name="luke_24_47"></a>Luke 24:47

**And that [metanoia](../../strongs/g/g3341.md) and [aphesis](../../strongs/g/g859.md) of [hamartia](../../strongs/g/g266.md) should be [kēryssō](../../strongs/g/g2784.md) in his [onoma](../../strongs/g/g3686.md) among all [ethnos](../../strongs/g/g1484.md), [archomai](../../strongs/g/g756.md) at [Ierousalēm](../../strongs/g/g2419.md).**

<a name="luke_24_48"></a>Luke 24:48

**And ye are [martys](../../strongs/g/g3144.md) of these things.**

<a name="luke_24_49"></a>Luke 24:49

**And, [idou](../../strongs/g/g2400.md), I [apostellō](../../strongs/g/g649.md) the [epaggelia](../../strongs/g/g1860.md) of my [patēr](../../strongs/g/g3962.md) upon you: but [kathizō](../../strongs/g/g2523.md) ye in the [polis](../../strongs/g/g4172.md) of [Ierousalēm](../../strongs/g/g2419.md), until ye be [endyō](../../strongs/g/g1746.md) with [dynamis](../../strongs/g/g1411.md) from [hypsos](../../strongs/g/g5311.md).**

<a name="luke_24_50"></a>Luke 24:50

And he [exagō](../../strongs/g/g1806.md) them out as far as to [Bēthania](../../strongs/g/g963.md), and he [epairō](../../strongs/g/g1869.md) his [cheir](../../strongs/g/g5495.md), and [eulogeō](../../strongs/g/g2127.md) them.

<a name="luke_24_51"></a>Luke 24:51

And [ginomai](../../strongs/g/g1096.md), while he [eulogeō](../../strongs/g/g2127.md) them, he was [diïstēmi](../../strongs/g/g1339.md) from them, and [anapherō](../../strongs/g/g399.md) into [ouranos](../../strongs/g/g3772.md).

<a name="luke_24_52"></a>Luke 24:52

And they [proskyneō](../../strongs/g/g4352.md) him, and [hypostrephō](../../strongs/g/g5290.md) to [Ierousalēm](../../strongs/g/g2419.md) with [megas](../../strongs/g/g3173.md) [chara](../../strongs/g/g5479.md):

<a name="luke_24_53"></a>Luke 24:53

And were [diapantos](../../strongs/g/g1275.md) in the [hieron](../../strongs/g/g2411.md), [aineō](../../strongs/g/g134.md) and [eulogeō](../../strongs/g/g2127.md) [theos](../../strongs/g/g2316.md). [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 23](luke_23.md)