# [Luke 12](https://www.blueletterbible.org/kjv/luk/12/1/rl1/s_985001)

<a name="luke_12_1"></a>Luke 12:1

In the mean time, when there were [episynagō](../../strongs/g/g1996.md) a [myrias](../../strongs/g/g3461.md) of [ochlos](../../strongs/g/g3793.md), insomuch that they [katapateō](../../strongs/g/g2662.md) upon [allēlōn](../../strongs/g/g240.md), he [archomai](../../strongs/g/g756.md) to [legō](../../strongs/g/g3004.md) unto his [mathētēs](../../strongs/g/g3101.md) first of all, **[prosechō](../../strongs/g/g4337.md) ye of the [zymē](../../strongs/g/g2219.md) of the [Pharisaios](../../strongs/g/g5330.md), which is [hypokrisis](../../strongs/g/g5272.md).**

<a name="luke_12_2"></a>Luke 12:2

**For there is nothing [sygkalyptō](../../strongs/g/g4780.md), that shall not be [apokalyptō](../../strongs/g/g601.md); neither [kryptos](../../strongs/g/g2927.md), that shall not be [ginōskō](../../strongs/g/g1097.md).**

<a name="luke_12_3"></a>Luke 12:3

**Therefore whatsoever ye have [eipon](../../strongs/g/g2036.md) in [skotia](../../strongs/g/g4653.md) shall be [akouō](../../strongs/g/g191.md) in the [phōs](../../strongs/g/g5457.md); and that which ye have [laleō](../../strongs/g/g2980.md) in the [ous](../../strongs/g/g3775.md) in [tameion](../../strongs/g/g5009.md) shall be [kēryssō](../../strongs/g/g2784.md) upon the [dōma](../../strongs/g/g1430.md).**

<a name="luke_12_4"></a>Luke 12:4

**And I [legō](../../strongs/g/g3004.md) unto you my [philos](../../strongs/g/g5384.md), Be not [phobeō](../../strongs/g/g5399.md) of them that [apokteinō](../../strongs/g/g615.md) the [sōma](../../strongs/g/g4983.md), and after that have no [perissoteros](../../strongs/g/g4055.md) that they can [poieō](../../strongs/g/g4160.md).**

<a name="luke_12_5"></a>Luke 12:5

**But I will [hypodeiknymi](../../strongs/g/g5263.md) you whom ye shall [phobeō](../../strongs/g/g5399.md): [phobeō](../../strongs/g/g5399.md) him, which after he hath [apokteinō](../../strongs/g/g615.md) hath [exousia](../../strongs/g/g1849.md) to [emballō](../../strongs/g/g1685.md) into [geenna](../../strongs/g/g1067.md); [nai](../../strongs/g/g3483.md), I [legō](../../strongs/g/g3004.md) unto you, [phobeō](../../strongs/g/g5399.md) him.**

<a name="luke_12_6"></a>Luke 12:6

**Are not five [strouthion](../../strongs/g/g4765.md) [pōleō](../../strongs/g/g4453.md) for two [assarion](../../strongs/g/g787.md), and not one of them is [epilanthanomai](../../strongs/g/g1950.md) before [theos](../../strongs/g/g2316.md)?**

<a name="luke_12_7"></a>Luke 12:7

**But even the very [thrix](../../strongs/g/g2359.md) of your [kephalē](../../strongs/g/g2776.md) are all [arithmeō](../../strongs/g/g705.md). [phobeō](../../strongs/g/g5399.md) not therefore: ye are [diapherō](../../strongs/g/g1308.md) than [polys](../../strongs/g/g4183.md) [strouthion](../../strongs/g/g4765.md).**

<a name="luke_12_8"></a>Luke 12:8

**Also I [legō](../../strongs/g/g3004.md) unto you, Whosoever shall [homologeō](../../strongs/g/g3670.md) me before [anthrōpos](../../strongs/g/g444.md), him shall the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) also [homologeō](../../strongs/g/g3670.md) before the [aggelos](../../strongs/g/g32.md) of [theos](../../strongs/g/g2316.md):**

<a name="luke_12_9"></a>Luke 12:9

**But he that [arneomai](../../strongs/g/g720.md) me before [anthrōpos](../../strongs/g/g444.md) shall be [aparneomai](../../strongs/g/g533.md) before the [aggelos](../../strongs/g/g32.md) of [theos](../../strongs/g/g2316.md).**

<a name="luke_12_10"></a>Luke 12:10

**And whosoever shall [eipon](../../strongs/g/g2046.md) a [logos](../../strongs/g/g3056.md) against the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md), it shall be [aphiēmi](../../strongs/g/g863.md) him: but unto him that [blasphēmeō](../../strongs/g/g987.md) against the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) it shall not be [aphiēmi](../../strongs/g/g863.md).**

<a name="luke_12_11"></a>Luke 12:11

**And when they [prospherō](../../strongs/g/g4374.md) you unto the [synagōgē](../../strongs/g/g4864.md), and [archē](../../strongs/g/g746.md), and [exousia](../../strongs/g/g1849.md), ye no [merimnaō](../../strongs/g/g3309.md) how or what thing ye shall [apologeomai](../../strongs/g/g626.md), or what ye shall [eipon](../../strongs/g/g2036.md):**

<a name="luke_12_12"></a>Luke 12:12

**For the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) shall [didaskō](../../strongs/g/g1321.md) you in the same [hōra](../../strongs/g/g5610.md) what ye ought to [eipon](../../strongs/g/g2036.md).**

<a name="luke_12_13"></a>Luke 12:13

And one of the [ochlos](../../strongs/g/g3793.md) [eipon](../../strongs/g/g2036.md) unto him, [didaskalos](../../strongs/g/g1320.md), [eipon](../../strongs/g/g2036.md) to my [adelphos](../../strongs/g/g80.md), that he [merizō](../../strongs/g/g3307.md) the [klēronomia](../../strongs/g/g2817.md) with me.

<a name="luke_12_14"></a>Luke 12:14

And he [eipon](../../strongs/g/g2036.md) unto him, **[anthrōpos](../../strongs/g/g444.md), who [kathistēmi](../../strongs/g/g2525.md) me a [dikastēs](../../strongs/g/g1348.md) or a [meristēs](../../strongs/g/g3312.md) over you?**

<a name="luke_12_15"></a>Luke 12:15

And he [eipon](../../strongs/g/g2036.md) unto them, **[horaō](../../strongs/g/g3708.md), and [phylassō](../../strongs/g/g5442.md) of [pleonexia](../../strongs/g/g4124.md): for [tis](../../strongs/g/g5100.md) [zōē](../../strongs/g/g2222.md) [esti](../../strongs/g/g2076.md) not in the [perisseuō](../../strongs/g/g4052.md) of the things which he [hyparchonta](../../strongs/g/g5224.md).**

<a name="luke_12_16"></a>Luke 12:16

And he [eipon](../../strongs/g/g2036.md) a [parabolē](../../strongs/g/g3850.md) unto them, [legō](../../strongs/g/g3004.md), **The [chōra](../../strongs/g/g5561.md) of a certain [plousios](../../strongs/g/g4145.md) [anthrōpos](../../strongs/g/g444.md) [euphoreō](../../strongs/g/g2164.md):**

<a name="luke_12_17"></a>Luke 12:17

**And he [dialogizomai](../../strongs/g/g1260.md) within himself, [legō](../../strongs/g/g3004.md), What shall I [poieō](../../strongs/g/g4160.md), because I have no [pou](../../strongs/g/g4226.md) to [synagō](../../strongs/g/g4863.md) my [karpos](../../strongs/g/g2590.md)?**

<a name="luke_12_18"></a>Luke 12:18

**And he [eipon](../../strongs/g/g2036.md), This will I [poieō](../../strongs/g/g4160.md): I will [kathaireō](../../strongs/g/g2507.md) my [apothēkē](../../strongs/g/g596.md), and [oikodomeō](../../strongs/g/g3618.md) [meizōn](../../strongs/g/g3187.md); and there will I [synagō](../../strongs/g/g4863.md) all my [gennēma](../../strongs/g/g1081.md) and my [agathos](../../strongs/g/g18.md).**

<a name="luke_12_19"></a>Luke 12:19

**And I will [eipon](../../strongs/g/g2046.md) to my [psychē](../../strongs/g/g5590.md), [psychē](../../strongs/g/g5590.md), thou hast [polys](../../strongs/g/g4183.md) [agathos](../../strongs/g/g18.md) [keimai](../../strongs/g/g2749.md) for [polys](../../strongs/g/g4183.md) [etos](../../strongs/g/g2094.md); [anapauō](../../strongs/g/g373.md), [phago](../../strongs/g/g5315.md), [pinō](../../strongs/g/g4095.md), [euphrainō](../../strongs/g/g2165.md).**

<a name="luke_12_20"></a>Luke 12:20

**But [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md) unto him, [aphrōn](../../strongs/g/g878.md), this [nyx](../../strongs/g/g3571.md) thy [psychē](../../strongs/g/g5590.md) shall be [apaiteō](../../strongs/g/g523.md) of thee: then whose shall those things be, which thou hast [hetoimazō](../../strongs/g/g2090.md)?**

<a name="luke_12_21"></a>Luke 12:21

**So he that [thēsaurizō](../../strongs/g/g2343.md) for himself, and is not [plouteō](../../strongs/g/g4147.md) toward [theos](../../strongs/g/g2316.md).**

<a name="luke_12_22"></a>Luke 12:22

And he [eipon](../../strongs/g/g2036.md) unto his [mathētēs](../../strongs/g/g3101.md), **Therefore I [legō](../../strongs/g/g3004.md) unto you, no [merimnaō](../../strongs/g/g3309.md) for your [psychē](../../strongs/g/g5590.md), what ye shall [phago](../../strongs/g/g5315.md); neither for the [sōma](../../strongs/g/g4983.md), what ye shall [endyō](../../strongs/g/g1746.md).**

<a name="luke_12_23"></a>Luke 12:23

**The [psychē](../../strongs/g/g5590.md) is more than [trophē](../../strongs/g/g5160.md), and the [sōma](../../strongs/g/g4983.md) than [endyma](../../strongs/g/g1742.md).**

<a name="luke_12_24"></a>Luke 12:24

**[katanoeō](../../strongs/g/g2657.md) the [korax](../../strongs/g/g2876.md): for they neither [speirō](../../strongs/g/g4687.md) nor [therizō](../../strongs/g/g2325.md); which neither have [tameion](../../strongs/g/g5009.md) nor [apothēkē](../../strongs/g/g596.md); and [theos](../../strongs/g/g2316.md) [trephō](../../strongs/g/g5142.md) them: how much more are ye [diapherō](../../strongs/g/g1308.md) than the [peteinon](../../strongs/g/g4071.md)?**

<a name="luke_12_25"></a>Luke 12:25

**And which of you with [merimnaō](../../strongs/g/g3309.md) can [prostithēmi](../../strongs/g/g4369.md) to his [hēlikia](../../strongs/g/g2244.md) one [pēchys](../../strongs/g/g4083.md)?**

<a name="luke_12_26"></a>Luke 12:26

**If ye then be not able to do [elachistos](../../strongs/g/g1646.md), why [merimnaō](../../strongs/g/g3309.md) for [loipos](../../strongs/g/g3062.md)?**

<a name="luke_12_27"></a>Luke 12:27

**[katanoeō](../../strongs/g/g2657.md) the [krinon](../../strongs/g/g2918.md) how they [auxanō](../../strongs/g/g837.md): they [kopiaō](../../strongs/g/g2872.md) not, they [nēthō](../../strongs/g/g3514.md) not; and yet I [legō](../../strongs/g/g3004.md) unto you, that [Solomōn](../../strongs/g/g4672.md) in all his [doxa](../../strongs/g/g1391.md) was not [periballō](../../strongs/g/g4016.md) like one of these.**

<a name="luke_12_28"></a>Luke 12:28

**If then [theos](../../strongs/g/g2316.md) so [amphiennymi](../../strongs/g/g294.md) the [chortos](../../strongs/g/g5528.md), which is [sēmeron](../../strongs/g/g4594.md) in the [agros](../../strongs/g/g68.md), and [aurion](../../strongs/g/g839.md) is [ballō](../../strongs/g/g906.md) into the [klibanos](../../strongs/g/g2823.md); how much more you, [oligopistos](../../strongs/g/g3640.md)?**

<a name="luke_12_29"></a>Luke 12:29

**And [zēteō](../../strongs/g/g2212.md) not ye what ye shall [phago](../../strongs/g/g5315.md), or what ye shall [pinō](../../strongs/g/g4095.md), neither [meteōrizomai](../../strongs/g/g3349.md).**

<a name="luke_12_30"></a>Luke 12:30

**For all these things the [ethnos](../../strongs/g/g1484.md) of the [kosmos](../../strongs/g/g2889.md) [epizēteō](../../strongs/g/g1934.md): and your [patēr](../../strongs/g/g3962.md) [eidō](../../strongs/g/g1492.md) that ye have [chrēzō](../../strongs/g/g5535.md) of these things.**

<a name="luke_12_31"></a>Luke 12:31

**But rather [zēteō](../../strongs/g/g2212.md) ye the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md); and all these things shall be [prostithēmi](../../strongs/g/g4369.md) unto you.**

<a name="luke_12_32"></a>Luke 12:32

**[phobeō](../../strongs/g/g5399.md) not, [mikros](../../strongs/g/g3398.md) [poimnion](../../strongs/g/g4168.md); for it is your [patēr](../../strongs/g/g3962.md) [eudokeō](../../strongs/g/g2106.md) to [didōmi](../../strongs/g/g1325.md) you the [basileia](../../strongs/g/g932.md).**

<a name="luke_12_33"></a>Luke 12:33

**[pōleō](../../strongs/g/g4453.md) that ye [hyparchonta](../../strongs/g/g5224.md), and [didōmi](../../strongs/g/g1325.md) [eleēmosynē](../../strongs/g/g1654.md); [poieō](../../strongs/g/g4160.md) yourselves [ballantion](../../strongs/g/g905.md) which [palaioō](../../strongs/g/g3822.md) not, a [thēsauros](../../strongs/g/g2344.md) in the [ouranos](../../strongs/g/g3772.md) [anekleiptos](../../strongs/g/g413.md), where no [kleptēs](../../strongs/g/g2812.md) [eggizō](../../strongs/g/g1448.md), neither [sēs](../../strongs/g/g4597.md) [diaphtheirō](../../strongs/g/g1311.md).**

<a name="luke_12_34"></a>Luke 12:34

**For where your [thēsauros](../../strongs/g/g2344.md) is, there will your [kardia](../../strongs/g/g2588.md) be also.**

<a name="luke_12_35"></a>Luke 12:35

**Let your [osphys](../../strongs/g/g3751.md) be [perizōnnymi](../../strongs/g/g4024.md), and [lychnos](../../strongs/g/g3088.md) [kaiō](../../strongs/g/g2545.md);**

<a name="luke_12_36"></a>Luke 12:36

**And ye yourselves [homoios](../../strongs/g/g3664.md) unto [anthrōpos](../../strongs/g/g444.md) that [prosdechomai](../../strongs/g/g4327.md) for their [kyrios](../../strongs/g/g2962.md), when he will [analyō](../../strongs/g/g360.md) from the [gamos](../../strongs/g/g1062.md); that when he [erchomai](../../strongs/g/g2064.md) and [krouō](../../strongs/g/g2925.md), they may [anoigō](../../strongs/g/g455.md) unto him [eutheōs](../../strongs/g/g2112.md)    .**

<a name="luke_12_37"></a>Luke 12:37

**[makarios](../../strongs/g/g3107.md) those [doulos](../../strongs/g/g1401.md), whom the [kyrios](../../strongs/g/g2962.md) when he [erchomai](../../strongs/g/g2064.md) shall [heuriskō](../../strongs/g/g2147.md) [grēgoreō](../../strongs/g/g1127.md): [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, that he shall [perizōnnymi](../../strongs/g/g4024.md), and make them [anaklinō](../../strongs/g/g347.md), and will [parerchomai](../../strongs/g/g3928.md) and [diakoneō](../../strongs/g/g1247.md) them.**

<a name="luke_12_38"></a>Luke 12:38

**And if he shall [erchomai](../../strongs/g/g2064.md) in the second [phylakē](../../strongs/g/g5438.md), or come in the third [phylakē](../../strongs/g/g5438.md), and [heuriskō](../../strongs/g/g2147.md) so, [makarios](../../strongs/g/g3107.md) are those [doulos](../../strongs/g/g1401.md).**

<a name="luke_12_39"></a>Luke 12:39

**And this [ginōskō](../../strongs/g/g1097.md), that if the [oikodespotēs](../../strongs/g/g3617.md) had [eidō](../../strongs/g/g1492.md) what [hōra](../../strongs/g/g5610.md) the [kleptēs](../../strongs/g/g2812.md) would [erchomai](../../strongs/g/g2064.md), he would have [grēgoreō](../../strongs/g/g1127.md), and not have [aphiēmi](../../strongs/g/g863.md) his [oikos](../../strongs/g/g3624.md) to be [dioryssō](../../strongs/g/g1358.md).**

<a name="luke_12_40"></a>Luke 12:40

**Be ye therefore [hetoimos](../../strongs/g/g2092.md) also: for the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [erchomai](../../strongs/g/g2064.md) at an [hōra](../../strongs/g/g5610.md) when ye [dokeō](../../strongs/g/g1380.md) not.**

<a name="luke_12_41"></a>Luke 12:41

Then [Petros](../../strongs/g/g4074.md) [eipon](../../strongs/g/g2036.md) unto him, [kyrios](../../strongs/g/g2962.md), [legō](../../strongs/g/g3004.md) thou this [parabolē](../../strongs/g/g3850.md) unto us, or even to all?

<a name="luke_12_42"></a>Luke 12:42

And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md), **Who then is that [pistos](../../strongs/g/g4103.md) and [phronimos](../../strongs/g/g5429.md) [oikonomos](../../strongs/g/g3623.md), whom [kyrios](../../strongs/g/g2962.md) shall [kathistēmi](../../strongs/g/g2525.md) over his [therapeia](../../strongs/g/g2322.md), to [didōmi](../../strongs/g/g1325.md) [sitometrion](../../strongs/g/g4620.md) in [kairos](../../strongs/g/g2540.md)?**

<a name="luke_12_43"></a>Luke 12:43

**[makarios](../../strongs/g/g3107.md) that [doulos](../../strongs/g/g1401.md), whom his [kyrios](../../strongs/g/g2962.md) when he [erchomai](../../strongs/g/g2064.md) shall [heuriskō](../../strongs/g/g2147.md) so [poieō](../../strongs/g/g4160.md).**

<a name="luke_12_44"></a>Luke 12:44

**[alēthōs](../../strongs/g/g230.md) I [legō](../../strongs/g/g3004.md) unto you, that he will make him [kathistēmi](../../strongs/g/g2525.md) over all that he [hyparchonta](../../strongs/g/g5224.md).**

<a name="luke_12_45"></a>Luke 12:45

**But and if that [doulos](../../strongs/g/g1401.md) [eipon](../../strongs/g/g2036.md) in his [kardia](../../strongs/g/g2588.md), My [kyrios](../../strongs/g/g2962.md) [chronizō](../../strongs/g/g5549.md) his [erchomai](../../strongs/g/g2064.md); and shall [archomai](../../strongs/g/g756.md) to [typtō](../../strongs/g/g5180.md) the [pais](../../strongs/g/g3816.md) and [paidiskē](../../strongs/g/g3814.md), and to [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md), and to be [methyskō](../../strongs/g/g3182.md);**

<a name="luke_12_46"></a>Luke 12:46

**The [kyrios](../../strongs/g/g2962.md) of that [doulos](../../strongs/g/g1401.md) will [hēkō](../../strongs/g/g2240.md) in a [hēmera](../../strongs/g/g2250.md) when he [prosdokaō](../../strongs/g/g4328.md) not for, and at an [hōra](../../strongs/g/g5610.md) when he is not [ginōskō](../../strongs/g/g1097.md), and will [dichotomeō](../../strongs/g/g1371.md) him, and will [tithēmi](../../strongs/g/g5087.md) him his [meros](../../strongs/g/g3313.md) with the [apistos](../../strongs/g/g571.md).**

<a name="luke_12_47"></a>Luke 12:47

**And that [doulos](../../strongs/g/g1401.md), which [ginōskō](../../strongs/g/g1097.md) his [kyrios](../../strongs/g/g2962.md) [thelēma](../../strongs/g/g2307.md), and [hetoimazō](../../strongs/g/g2090.md) not, neither [poieō](../../strongs/g/g4160.md) according to his [thelēma](../../strongs/g/g2307.md), shall be [derō](../../strongs/g/g1194.md) with [polys](../../strongs/g/g4183.md).**

<a name="luke_12_48"></a>Luke 12:48

**But he that [ginōskō](../../strongs/g/g1097.md) not, and did [poieō](../../strongs/g/g4160.md) things [axios](../../strongs/g/g514.md) of [plēgē](../../strongs/g/g4127.md), shall be [derō](../../strongs/g/g1194.md) with [oligos](../../strongs/g/g3641.md). For unto whomsoever [polys](../../strongs/g/g4183.md) is [didōmi](../../strongs/g/g1325.md), of him shall be [polys](../../strongs/g/g4183.md) [zēteō](../../strongs/g/g2212.md): and to [hos](../../strongs/g/g3739.md) have [paratithēmi](../../strongs/g/g3908.md) [polys](../../strongs/g/g4183.md), of him they will [aiteō](../../strongs/g/g154.md) the [perissoteros](../../strongs/g/g4055.md).**

<a name="luke_12_49"></a>Luke 12:49

**I am [erchomai](../../strongs/g/g2064.md) to [ballō](../../strongs/g/g906.md) [pyr](../../strongs/g/g4442.md) on the [gē](../../strongs/g/g1093.md); and what [thelō](../../strongs/g/g2309.md) I, if it be already [anaptō](../../strongs/g/g381.md)?**

<a name="luke_12_50"></a>Luke 12:50

**But I have a [baptisma](../../strongs/g/g908.md) to be [baptizō](../../strongs/g/g907.md) with; and how am I [synechō](../../strongs/g/g4912.md) till [teleō](../../strongs/g/g5055.md)!**

<a name="luke_12_51"></a>Luke 12:51

**[dokeō](../../strongs/g/g1380.md) ye that I am [paraginomai](../../strongs/g/g3854.md) to [didōmi](../../strongs/g/g1325.md) [eirēnē](../../strongs/g/g1515.md) on [gē](../../strongs/g/g1093.md)? I [legō](../../strongs/g/g3004.md) you, [ouchi](../../strongs/g/g3780.md); but rather [diamerismos](../../strongs/g/g1267.md):**

<a name="luke_12_52"></a>Luke 12:52

**For from henceforth there shall be five in one [oikos](../../strongs/g/g3624.md) [diamerizō](../../strongs/g/g1266.md), three against two, and two against three.**

<a name="luke_12_53"></a>Luke 12:53

**The [patēr](../../strongs/g/g3962.md) shall be [diamerizō](../../strongs/g/g1266.md) against the [huios](../../strongs/g/g5207.md), and the [huios](../../strongs/g/g5207.md) against the [patēr](../../strongs/g/g3962.md); the [mētēr](../../strongs/g/g3384.md) against the [thygatēr](../../strongs/g/g2364.md), and the [thygatēr](../../strongs/g/g2364.md) against the [mētēr](../../strongs/g/g3384.md); the [penthera](../../strongs/g/g3994.md) against her [nymphē](../../strongs/g/g3565.md), and the [nymphē](../../strongs/g/g3565.md) against her [penthera](../../strongs/g/g3994.md).**

<a name="luke_12_54"></a>Luke 12:54

And he [legō](../../strongs/g/g3004.md) also to the [ochlos](../../strongs/g/g3793.md), **When ye [eidō](../../strongs/g/g1492.md) a [nephelē](../../strongs/g/g3507.md) [anatellō](../../strongs/g/g393.md) out of the [dysmē](../../strongs/g/g1424.md), [eutheōs](../../strongs/g/g2112.md) ye [legō](../../strongs/g/g3004.md), There [erchomai](../../strongs/g/g2064.md) a [ombros](../../strongs/g/g3655.md); and so it is.**

<a name="luke_12_55"></a>Luke 12:55

**And when the [notos](../../strongs/g/g3558.md) [pneō](../../strongs/g/g4154.md), ye [legō](../../strongs/g/g3004.md), There will be [kausōn](../../strongs/g/g2742.md); and it [ginomai](../../strongs/g/g1096.md).**

<a name="luke_12_56"></a>Luke 12:56

**[hypokritēs](../../strongs/g/g5273.md), ye [eidō](../../strongs/g/g1492.md) [dokimazō](../../strongs/g/g1381.md) the [prosōpon](../../strongs/g/g4383.md) of the [ouranos](../../strongs/g/g3772.md) and of the [gē](../../strongs/g/g1093.md); but how is it that ye do not [dokimazō](../../strongs/g/g1381.md) this [kairos](../../strongs/g/g2540.md)?**

<a name="luke_12_57"></a>Luke 12:57

**[de](../../strongs/g/g1161.md), and why even of yourselves [krinō](../../strongs/g/g2919.md) ye not [dikaios](../../strongs/g/g1342.md)?**

<a name="luke_12_58"></a>Luke 12:58

**When thou [hypagō](../../strongs/g/g5217.md) with thine [antidikos](../../strongs/g/g476.md) to the [archōn](../../strongs/g/g758.md), in the [hodos](../../strongs/g/g3598.md), [didōmi](../../strongs/g/g1325.md) [ergasia](../../strongs/g/g2039.md) that thou mayest be [apallassō](../../strongs/g/g525.md) from him; lest he [katasyrō](../../strongs/g/g2694.md) thee to the [kritēs](../../strongs/g/g2923.md), and the [kritēs](../../strongs/g/g2923.md) [paradidōmi](../../strongs/g/g3860.md) thee to the [praktōr](../../strongs/g/g4233.md), and the [praktōr](../../strongs/g/g4233.md) [ballō](../../strongs/g/g906.md) thee into [phylakē](../../strongs/g/g5438.md).**

<a name="luke_12_59"></a>Luke 12:59

**I [legō](../../strongs/g/g3004.md) thee, thou shalt not [exerchomai](../../strongs/g/g1831.md) thence, till thou hast [apodidōmi](../../strongs/g/g591.md) the [eschatos](../../strongs/g/g2078.md) [lepton](../../strongs/g/g3016.md).**

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 11](luke_11.md) - [Luke 13](luke_13.md)