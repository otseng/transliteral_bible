# [Luke 4](https://www.blueletterbible.org/kjv/luk/4/1/rl1/s_977001)

<a name="luke_4_1"></a>Luke 4:1

And [Iēsous](../../strongs/g/g2424.md) being [plērēs](../../strongs/g/g4134.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) [hypostrephō](../../strongs/g/g5290.md) from [Iordanēs](../../strongs/g/g2446.md), and was [agō](../../strongs/g/g71.md) by the [pneuma](../../strongs/g/g4151.md) into the [erēmos](../../strongs/g/g2048.md),

<a name="luke_4_2"></a>Luke 4:2

Being forty [hēmera](../../strongs/g/g2250.md) [peirazō](../../strongs/g/g3985.md) of the [diabolos](../../strongs/g/g1228.md). And in those days he did [phago](../../strongs/g/g5315.md) nothing: and when they were [synteleō](../../strongs/g/g4931.md), he afterward [peinaō](../../strongs/g/g3983.md).

<a name="luke_4_3"></a>Luke 4:3

And the [diabolos](../../strongs/g/g1228.md) [eipon](../../strongs/g/g2036.md) unto him, If thou be the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), [eipon](../../strongs/g/g2036.md) this [lithos](../../strongs/g/g3037.md) that it be made [artos](../../strongs/g/g740.md).

<a name="luke_4_4"></a>Luke 4:4

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) him, [legō](../../strongs/g/g3004.md), **It is [graphō](../../strongs/g/g1125.md), That [anthrōpos](../../strongs/g/g444.md) shall not [zaō](../../strongs/g/g2198.md) by [artos](../../strongs/g/g740.md) [monos](../../strongs/g/g3441.md), but by every [rhēma](../../strongs/g/g4487.md) of [theos](../../strongs/g/g2316.md).** [^1]

<a name="luke_4_5"></a>Luke 4:5

And the [diabolos](../../strongs/g/g1228.md), [anagō](../../strongs/g/g321.md) him up into an [hypsēlos](../../strongs/g/g5308.md) [oros](../../strongs/g/g3735.md), [deiknyō](../../strongs/g/g1166.md) unto him all the [basileia](../../strongs/g/g932.md) of the [oikoumenē](../../strongs/g/g3625.md) in a [stigmē](../../strongs/g/g4743.md) of [chronos](../../strongs/g/g5550.md).

<a name="luke_4_6"></a>Luke 4:6

And the [diabolos](../../strongs/g/g1228.md) [eipon](../../strongs/g/g2036.md) unto him, All this [exousia](../../strongs/g/g1849.md) will I [didōmi](../../strongs/g/g1325.md) thee, and the [doxa](../../strongs/g/g1391.md) of them: for that is [paradidōmi](../../strongs/g/g3860.md) unto me; and to whomsoever I will I [didōmi](../../strongs/g/g1325.md) it.

<a name="luke_4_7"></a>Luke 4:7

If thou therefore wilt [proskyneō](../../strongs/g/g4352.md) me, all shall be thine.

<a name="luke_4_8"></a>Luke 4:8

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, **[hypagō](../../strongs/g/g5217.md) [opisō](../../strongs/g/g3694.md) me, [Satanas](../../strongs/g/g4567.md): for it is [graphō](../../strongs/g/g1125.md), Thou shalt [proskyneō](../../strongs/g/g4352.md) the [kyrios](../../strongs/g/g2962.md) thy [theos](../../strongs/g/g2316.md), and him only shalt thou [latreuō](../../strongs/g/g3000.md).**

<a name="luke_4_9"></a>Luke 4:9

And he [agō](../../strongs/g/g71.md) him to [Ierousalēm](../../strongs/g/g2419.md), and [histēmi](../../strongs/g/g2476.md) him on a [pterygion](../../strongs/g/g4419.md) of the [hieron](../../strongs/g/g2411.md), and [eipon](../../strongs/g/g2036.md) unto him, If thou be the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), [ballō](../../strongs/g/g906.md) thyself down from hence:

<a name="luke_4_10"></a>Luke 4:10

For it is [graphō](../../strongs/g/g1125.md), He shall [entellō](../../strongs/g/g1781.md) his [aggelos](../../strongs/g/g32.md) over thee, to [diaphylassō](../../strongs/g/g1314.md) thee:

<a name="luke_4_11"></a>Luke 4:11

And in [cheir](../../strongs/g/g5495.md) they shall [airō](../../strongs/g/g142.md) thee up, lest at any time thou [proskoptō](../../strongs/g/g4350.md) thy [pous](../../strongs/g/g4228.md) against a [lithos](../../strongs/g/g3037.md).

<a name="luke_4_12"></a>Luke 4:12

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto him, **It is [eipon](../../strongs/g/g2046.md), Thou shalt not [ekpeirazō](../../strongs/g/g1598.md) the [kyrios](../../strongs/g/g2962.md) thy [theos](../../strongs/g/g2316.md).**

<a name="luke_4_13"></a>Luke 4:13

And when the [diabolos](../../strongs/g/g1228.md) had [synteleō](../../strongs/g/g4931.md) all the [peirasmos](../../strongs/g/g3986.md), he [aphistēmi](../../strongs/g/g868.md) from him for a [kairos](../../strongs/g/g2540.md).

<a name="luke_4_14"></a>Luke 4:14

And [Iēsous](../../strongs/g/g2424.md) [hypostrephō](../../strongs/g/g5290.md) in the [dynamis](../../strongs/g/g1411.md) of the [pneuma](../../strongs/g/g4151.md) into [Galilaia](../../strongs/g/g1056.md): and there [exerchomai](../../strongs/g/g1831.md) a [phēmē](../../strongs/g/g5345.md) of him through all the [perichōros](../../strongs/g/g4066.md).

<a name="luke_4_15"></a>Luke 4:15

And he [didaskō](../../strongs/g/g1321.md) in their [synagōgē](../../strongs/g/g4864.md), being [doxazō](../../strongs/g/g1392.md) of all.

<a name="luke_4_16"></a>Luke 4:16

And he [erchomai](../../strongs/g/g2064.md) to [Nazara](../../strongs/g/g3478.md), where he had been [trephō](../../strongs/g/g5142.md): and, as his [ethō](../../strongs/g/g1486.md) was, he [eiserchomai](../../strongs/g/g1525.md) into the [synagōgē](../../strongs/g/g4864.md) on the [sabbaton](../../strongs/g/g4521.md), and [anistēmi](../../strongs/g/g450.md) for to [anaginōskō](../../strongs/g/g314.md).

<a name="luke_4_17"></a>Luke 4:17

And there was [epididōmi](../../strongs/g/g1929.md) unto him the [biblion](../../strongs/g/g975.md) of the [prophētēs](../../strongs/g/g4396.md) [Ēsaïas](../../strongs/g/g2268.md). And when he had [anaptyssō](../../strongs/g/g380.md) the [biblion](../../strongs/g/g975.md), he [heuriskō](../../strongs/g/g2147.md) the [topos](../../strongs/g/g5117.md) where it was [graphō](../../strongs/g/g1125.md),

<a name="luke_4_18"></a>Luke 4:18

**The [pneuma](../../strongs/g/g4151.md) of the [kyrios](../../strongs/g/g2962.md) is upon me, because he hath [chriō](../../strongs/g/g5548.md) me to [euaggelizō](../../strongs/g/g2097.md) to the [ptōchos](../../strongs/g/g4434.md); he hath [apostellō](../../strongs/g/g649.md) me to [iaomai](../../strongs/g/g2390.md) the [syntribō](../../strongs/g/g4937.md) [kardia](../../strongs/g/g2588.md), to [kēryssō](../../strongs/g/g2784.md) [aphesis](../../strongs/g/g859.md) to the [aichmalōtos](../../strongs/g/g164.md), and [anablepsis](../../strongs/g/g309.md) to the [typhlos](../../strongs/g/g5185.md), to set at [aphesis](../../strongs/g/g859.md) them that are [thrauō](../../strongs/g/g2352.md),**

<a name="luke_4_19"></a>Luke 4:19

**To [kēryssō](../../strongs/g/g2784.md) the [dektos](../../strongs/g/g1184.md) [eniautos](../../strongs/g/g1763.md) of the [kyrios](../../strongs/g/g2962.md).**

<a name="luke_4_20"></a>Luke 4:20

And he [ptyssō](../../strongs/g/g4428.md) the [biblion](../../strongs/g/g975.md), and he [apodidōmi](../../strongs/g/g591.md) to the [hypēretēs](../../strongs/g/g5257.md), and [kathizō](../../strongs/g/g2523.md). And the [ophthalmos](../../strongs/g/g3788.md) of all them that were in the [synagōgē](../../strongs/g/g4864.md) were [atenizō](../../strongs/g/g816.md) on him.

<a name="luke_4_21"></a>Luke 4:21

And he [archomai](../../strongs/g/g756.md) to [legō](../../strongs/g/g3004.md) unto them, **This [sēmeron](../../strongs/g/g4594.md) is this [graphē](../../strongs/g/g1124.md) [plēroō](../../strongs/g/g4137.md) in your [ous](../../strongs/g/g3775.md).**

<a name="luke_4_22"></a>Luke 4:22

And all [martyreō](../../strongs/g/g3140.md) him [martyreō](../../strongs/g/g3140.md), and [thaumazō](../../strongs/g/g2296.md) at the [charis](../../strongs/g/g5485.md) [logos](../../strongs/g/g3056.md) which [ekporeuomai](../../strongs/g/g1607.md) of his [stoma](../../strongs/g/g4750.md). And they [legō](../../strongs/g/g3004.md), Is not this [Iōsēph](../../strongs/g/g2501.md) [huios](../../strongs/g/g5207.md)?

<a name="luke_4_23"></a>Luke 4:23

And he [eipon](../../strongs/g/g2036.md) unto them, **Ye will [pantōs](../../strongs/g/g3843.md) [eipon](../../strongs/g/g2046.md) unto me this [parabolē](../../strongs/g/g3850.md), [iatros](../../strongs/g/g2395.md), [therapeuō](../../strongs/g/g2323.md) [seautou](../../strongs/g/g4572.md): whatsoever we have [akouō](../../strongs/g/g191.md) [ginomai](../../strongs/g/g1096.md) in [Kapharnaoum](../../strongs/g/g2584.md), [poieō](../../strongs/g/g4160.md) also here in thy [patris](../../strongs/g/g3968.md).**

<a name="luke_4_24"></a>Luke 4:24

And he [eipon](../../strongs/g/g2036.md), **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, No [prophētēs](../../strongs/g/g4396.md) is [dektos](../../strongs/g/g1184.md) in his own [patris](../../strongs/g/g3968.md).**

<a name="luke_4_25"></a>Luke 4:25

**But I [legō](../../strongs/g/g3004.md) you of an [alētheia](../../strongs/g/g225.md), [polys](../../strongs/g/g4183.md) [chēra](../../strongs/g/g5503.md) were in [Israēl](../../strongs/g/g2474.md) in the days of [Ēlias](../../strongs/g/g2243.md), when the [ouranos](../../strongs/g/g3772.md) was [kleiō](../../strongs/g/g2808.md) three [etos](../../strongs/g/g2094.md) and six [mēn](../../strongs/g/g3376.md), when [megas](../../strongs/g/g3173.md) [limos](../../strongs/g/g3042.md) was throughout all the [gē](../../strongs/g/g1093.md);**

<a name="luke_4_26"></a>Luke 4:26

**But unto none of them was [Ēlias](../../strongs/g/g2243.md) [pempō](../../strongs/g/g3992.md), save unto [Sarepta](../../strongs/g/g4558.md), of [Sidōn](../../strongs/g/g4605.md), unto a [gynē](../../strongs/g/g1135.md) a [chēra](../../strongs/g/g5503.md).**

<a name="luke_4_27"></a>Luke 4:27

**And [polys](../../strongs/g/g4183.md) [lepros](../../strongs/g/g3015.md) were in [Israēl](../../strongs/g/g2474.md) in the time of [Elisaios](../../strongs/g/g1666.md) the [prophētēs](../../strongs/g/g4396.md); and none of them was [katharizō](../../strongs/g/g2511.md), saving [Naiman](../../strongs/g/g3497.md) the [Syros](../../strongs/g/g4948.md).**

<a name="luke_4_28"></a>Luke 4:28

And all they in the [synagōgē](../../strongs/g/g4864.md), when they [akouō](../../strongs/g/g191.md) these things, were [pimplēmi](../../strongs/g/g4130.md) with [thymos](../../strongs/g/g2372.md),

<a name="luke_4_29"></a>Luke 4:29

And [anistēmi](../../strongs/g/g450.md), and [ekballō](../../strongs/g/g1544.md) him out of the [polis](../../strongs/g/g4172.md), and [agō](../../strongs/g/g71.md) him unto the [ophrys](../../strongs/g/g3790.md) of the [oros](../../strongs/g/g3735.md) whereon their [polis](../../strongs/g/g4172.md) was [oikodomeō](../../strongs/g/g3618.md), that they might [katakrēmnizō](../../strongs/g/g2630.md) him.

<a name="luke_4_30"></a>Luke 4:30

But he [dierchomai](../../strongs/g/g1330.md) through the [mesos](../../strongs/g/g3319.md) of them [poreuō](../../strongs/g/g4198.md),

<a name="luke_4_31"></a>Luke 4:31

And [katerchomai](../../strongs/g/g2718.md) to [Kapharnaoum](../../strongs/g/g2584.md), a [polis](../../strongs/g/g4172.md) of [Galilaia](../../strongs/g/g1056.md), and [didaskō](../../strongs/g/g1321.md) them on the [sabbaton](../../strongs/g/g4521.md).

<a name="luke_4_32"></a>Luke 4:32

And they were [ekplēssō](../../strongs/g/g1605.md) at his [didachē](../../strongs/g/g1322.md): for his [logos](../../strongs/g/g3056.md) was with [exousia](../../strongs/g/g1849.md).

<a name="luke_4_33"></a>Luke 4:33

And in the [synagōgē](../../strongs/g/g4864.md) there was an [anthrōpos](../../strongs/g/g444.md), which had a [pneuma](../../strongs/g/g4151.md) of an [akathartos](../../strongs/g/g169.md) [daimonion](../../strongs/g/g1140.md), and [anakrazō](../../strongs/g/g349.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md),

<a name="luke_4_34"></a>Luke 4:34

[legō](../../strongs/g/g3004.md), [ea](../../strongs/g/g1436.md); what have we to do with thee, thou [Iēsous](../../strongs/g/g2424.md) of [Nazarēnos](../../strongs/g/g3479.md)? art thou [erchomai](../../strongs/g/g2064.md) to [apollymi](../../strongs/g/g622.md) us? I [eidō](../../strongs/g/g1492.md) thee who thou art; the [hagios](../../strongs/g/g40.md) of [theos](../../strongs/g/g2316.md).

<a name="luke_4_35"></a>Luke 4:35

And [Iēsous](../../strongs/g/g2424.md) [epitimaō](../../strongs/g/g2008.md) him, [legō](../../strongs/g/g3004.md), **[phimoō](../../strongs/g/g5392.md), and [exerchomai](../../strongs/g/g1831.md) out of him.** And when the [daimonion](../../strongs/g/g1140.md) had [rhiptō](../../strongs/g/g4496.md) him in the [mesos](../../strongs/g/g3319.md), he [exerchomai](../../strongs/g/g1831.md) of him, and [blaptō](../../strongs/g/g984.md) him [mēdeis](../../strongs/g/g3367.md).

<a name="luke_4_36"></a>Luke 4:36

And they were all [thambos](../../strongs/g/g2285.md), and [syllaleō](../../strongs/g/g4814.md) among [allēlōn](../../strongs/g/g240.md), [legō](../../strongs/g/g3004.md), What a [logos](../../strongs/g/g3056.md) is this! for with [exousia](../../strongs/g/g1849.md) and [dynamis](../../strongs/g/g1411.md) he [epitassō](../../strongs/g/g2004.md) the [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md), and they [exerchomai](../../strongs/g/g1831.md).

<a name="luke_4_37"></a>Luke 4:37

And the [ēchos](../../strongs/g/g2279.md) of him [ekporeuomai](../../strongs/g/g1607.md) into every [topos](../../strongs/g/g5117.md) of the [perichōros](../../strongs/g/g4066.md).

<a name="luke_4_38"></a>Luke 4:38

And he [anistēmi](../../strongs/g/g450.md) out of the [synagōgē](../../strongs/g/g4864.md), and [eiserchomai](../../strongs/g/g1525.md) into [Simōn](../../strongs/g/g4613.md) [oikia](../../strongs/g/g3614.md). And [Simōn](../../strongs/g/g4613.md) [penthera](../../strongs/g/g3994.md) was [synechō](../../strongs/g/g4912.md) with a [megas](../../strongs/g/g3173.md) [pyretos](../../strongs/g/g4446.md); and they [erōtaō](../../strongs/g/g2065.md) him for her.

<a name="luke_4_39"></a>Luke 4:39

And he [ephistēmi](../../strongs/g/g2186.md) over her, and [epitimaō](../../strongs/g/g2008.md) the [pyretos](../../strongs/g/g4446.md); and it [aphiēmi](../../strongs/g/g863.md) her: and [parachrēma](../../strongs/g/g3916.md) she [anistēmi](../../strongs/g/g450.md) and [diakoneō](../../strongs/g/g1247.md) unto them.

<a name="luke_4_40"></a>Luke 4:40

Now when the [hēlios](../../strongs/g/g2246.md) was [dynō](../../strongs/g/g1416.md), all they that had any [astheneō](../../strongs/g/g770.md) with [poikilos](../../strongs/g/g4164.md) [nosos](../../strongs/g/g3554.md) [agō](../../strongs/g/g71.md) them unto him; and he [epitithēmi](../../strongs/g/g2007.md) his [cheir](../../strongs/g/g5495.md) on every one of them, and [therapeuō](../../strongs/g/g2323.md) them.

<a name="luke_4_41"></a>Luke 4:41

And [daimonion](../../strongs/g/g1140.md) also [exerchomai](../../strongs/g/g1831.md) of [polys](../../strongs/g/g4183.md), [krazō](../../strongs/g/g2896.md), and [legō](../../strongs/g/g3004.md), Thou art [Christos](../../strongs/g/g5547.md) the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md). And he [epitimaō](../../strongs/g/g2008.md) them [eaō](../../strongs/g/g1439.md) them not to [laleō](../../strongs/g/g2980.md): for they [eidō](../../strongs/g/g1492.md) that he was [Christos](../../strongs/g/g5547.md).

<a name="luke_4_42"></a>Luke 4:42

And when it was [hēmera](../../strongs/g/g2250.md), he [exerchomai](../../strongs/g/g1831.md) and [poreuō](../../strongs/g/g4198.md) into an [erēmos](../../strongs/g/g2048.md) [topos](../../strongs/g/g5117.md): and the [ochlos](../../strongs/g/g3793.md) [zēteō](../../strongs/g/g2212.md) him, and [erchomai](../../strongs/g/g2064.md) unto him, and [katechō](../../strongs/g/g2722.md) him, that he should not [poreuō](../../strongs/g/g4198.md) from them.

<a name="luke_4_43"></a>Luke 4:43

And he [eipon](../../strongs/g/g2036.md) unto them, **I must [euaggelizō](../../strongs/g/g2097.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) to other [polis](../../strongs/g/g4172.md) also: for therefore am I [apostellō](../../strongs/g/g649.md).**

<a name="luke_4_44"></a>Luke 4:44

And he [kēryssō](../../strongs/g/g2784.md) in the [synagōgē](../../strongs/g/g4864.md) of [Galilaia](../../strongs/g/g1056.md).

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke](luke_3.md) - [Luke](luke_5.md)

---

[^1]: [Luke 4:4 Commentary](../../commentary/luke/luke_4_commentary.md#luke_4_4)
