# [Luke 1](https://www.blueletterbible.org/kjv/luk/1/1/rl1/s_974001)

<a name="luke_1_1"></a>Luke 1:1

Forasmuch as [polys](../../strongs/g/g4183.md) have [epicheireō](../../strongs/g/g2021.md) to [anatassomai](../../strongs/g/g392.md) a [diēgēsis](../../strongs/g/g1335.md) of those [pragma](../../strongs/g/g4229.md) which are [plērophoreō](../../strongs/g/g4135.md) among us,

<a name="luke_1_2"></a>Luke 1:2

Even as they [paradidōmi](../../strongs/g/g3860.md) them unto us, which from the [archē](../../strongs/g/g746.md) were [autoptēs](../../strongs/g/g845.md), and [hypēretēs](../../strongs/g/g5257.md) of the [logos](../../strongs/g/g3056.md);

<a name="luke_1_3"></a>Luke 1:3

It [dokeō](../../strongs/g/g1380.md) to me also, having had [akribōs](../../strongs/g/g199.md) [parakoloutheō](../../strongs/g/g3877.md) of all things from the [anōthen](../../strongs/g/g509.md), to [graphō](../../strongs/g/g1125.md) unto thee in [kathexēs](../../strongs/g/g2517.md), [kratistos](../../strongs/g/g2903.md) [Theophilos](../../strongs/g/g2321.md),

<a name="luke_1_4"></a>Luke 1:4

That thou mightest [epiginōskō](../../strongs/g/g1921.md) the [asphaleia](../../strongs/g/g803.md) of those [logos](../../strongs/g/g3056.md), wherein thou hast been [katēcheō](../../strongs/g/g2727.md).

<a name="luke_1_5"></a>Luke 1:5

There was in the [hēmera](../../strongs/g/g2250.md) of [Hērōdēs](../../strongs/g/g2264.md), the [basileus](../../strongs/g/g935.md) of [Ioudaia](../../strongs/g/g2449.md), a [tis](../../strongs/g/g5100.md) [hiereus](../../strongs/g/g2409.md) [onoma](../../strongs/g/g3686.md) [Zacharias](../../strongs/g/g2197.md), of the [ephēmeria](../../strongs/g/g2183.md) of [Abia](../../strongs/g/g7.md): and his [gynē](../../strongs/g/g1135.md) was of the [thygatēr](../../strongs/g/g2364.md) of [Aarōn](../../strongs/g/g2.md), and her [onoma](../../strongs/g/g3686.md) was [Elisabet](../../strongs/g/g1665.md).

<a name="luke_1_6"></a>Luke 1:6

And they were [amphoteroi](../../strongs/g/g297.md) [dikaios](../../strongs/g/g1342.md) before [theos](../../strongs/g/g2316.md), [poreuō](../../strongs/g/g4198.md) in all the [entolē](../../strongs/g/g1785.md) and [dikaiōma](../../strongs/g/g1345.md) of the [kyrios](../../strongs/g/g2962.md) [amemptos](../../strongs/g/g273.md).

<a name="luke_1_7"></a>Luke 1:7

And they had no [teknon](../../strongs/g/g5043.md), because that [Elisabet](../../strongs/g/g1665.md) was [steira](../../strongs/g/g4723.md), and they [amphoteroi](../../strongs/g/g297.md) were [probainō](../../strongs/g/g4260.md) in [hēmera](../../strongs/g/g2250.md).

<a name="luke_1_8"></a>Luke 1:8

And [ginomai](../../strongs/g/g1096.md), that while he [hierateuō](../../strongs/g/g2407.md) before [theos](../../strongs/g/g2316.md) in the [taxis](../../strongs/g/g5010.md) of his [ephēmeria](../../strongs/g/g2183.md),

<a name="luke_1_9"></a>Luke 1:9

According to the [ethos](../../strongs/g/g1485.md) of the [hierateia](../../strongs/g/g2405.md), his [lagchanō](../../strongs/g/g2975.md) was to [thymiaō](../../strongs/g/g2370.md) when he [eiserchomai](../../strongs/g/g1525.md) into the [naos](../../strongs/g/g3485.md) of the [kyrios](../../strongs/g/g2962.md).

<a name="luke_1_10"></a>Luke 1:10

And the [pas](../../strongs/g/g3956.md) [plēthos](../../strongs/g/g4128.md) of the [laos](../../strongs/g/g2992.md) were [proseuchomai](../../strongs/g/g4336.md) [exō](../../strongs/g/g1854.md) at the [hōra](../../strongs/g/g5610.md) of [thymiama](../../strongs/g/g2368.md).

<a name="luke_1_11"></a>Luke 1:11

And there [optanomai](../../strongs/g/g3700.md) unto him an [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) [histēmi](../../strongs/g/g2476.md) on the [dexios](../../strongs/g/g1188.md) of the [thysiastērion](../../strongs/g/g2379.md) of [thymiama](../../strongs/g/g2368.md).

<a name="luke_1_12"></a>Luke 1:12

And when [Zacharias](../../strongs/g/g2197.md) [eidō](../../strongs/g/g1492.md) him, he was [tarassō](../../strongs/g/g5015.md), and [phobos](../../strongs/g/g5401.md) [epipiptō](../../strongs/g/g1968.md) upon him.

<a name="luke_1_13"></a>Luke 1:13

But the [aggelos](../../strongs/g/g32.md) [eipon](../../strongs/g/g2036.md) unto him, [phobeō](../../strongs/g/g5399.md) not, [Zacharias](../../strongs/g/g2197.md): for thy [deēsis](../../strongs/g/g1162.md) is [eisakouō](../../strongs/g/g1522.md); and thy [gynē](../../strongs/g/g1135.md) [Elisabet](../../strongs/g/g1665.md) shall [gennaō](../../strongs/g/g1080.md) thee a [huios](../../strongs/g/g5207.md), and thou shalt [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md) [Iōannēs](../../strongs/g/g2491.md).

<a name="luke_1_14"></a>Luke 1:14

And thou shalt have [chara](../../strongs/g/g5479.md) and [agalliasis](../../strongs/g/g20.md); and [polys](../../strongs/g/g4183.md) shall [chairō](../../strongs/g/g5463.md) at his [gennēsis](../../strongs/g/g1083.md).

<a name="luke_1_15"></a>Luke 1:15

For he shall be [megas](../../strongs/g/g3173.md) in the [enōpion](../../strongs/g/g1799.md) of the [kyrios](../../strongs/g/g2962.md), and shall [pinō](../../strongs/g/g4095.md) neither [oinos](../../strongs/g/g3631.md) nor [sikera](../../strongs/g/g4608.md); and he shall be [pimplēmi](../../strongs/g/g4130.md) with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), even from his [mētēr](../../strongs/g/g3384.md) [koilia](../../strongs/g/g2836.md).

<a name="luke_1_16"></a>Luke 1:16

And [polys](../../strongs/g/g4183.md) of the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md) shall he [epistrephō](../../strongs/g/g1994.md) to the [kyrios](../../strongs/g/g2962.md) their [theos](../../strongs/g/g2316.md).

<a name="luke_1_17"></a>Luke 1:17

And he shall [proerchomai](../../strongs/g/g4281.md) before him in the [pneuma](../../strongs/g/g4151.md) and [dynamis](../../strongs/g/g1411.md) of [Ēlias](../../strongs/g/g2243.md), to [epistrephō](../../strongs/g/g1994.md) the [kardia](../../strongs/g/g2588.md) of the [patēr](../../strongs/g/g3962.md) to the [teknon](../../strongs/g/g5043.md), and the [apeithēs](../../strongs/g/g545.md) to the [phronēsis](../../strongs/g/g5428.md) of the [dikaios](../../strongs/g/g1342.md); to [hetoimazō](../../strongs/g/g2090.md) a [laos](../../strongs/g/g2992.md) [kataskeuazō](../../strongs/g/g2680.md) for the [kyrios](../../strongs/g/g2962.md).

<a name="luke_1_18"></a>Luke 1:18

And [Zacharias](../../strongs/g/g2197.md) [eipon](../../strongs/g/g2036.md) unto the [aggelos](../../strongs/g/g32.md), Whereby shall I [ginōskō](../../strongs/g/g1097.md) this? for I am a [presbytēs](../../strongs/g/g4246.md), and my [gynē](../../strongs/g/g1135.md) [probainō](../../strongs/g/g4260.md) in [hēmera](../../strongs/g/g2250.md).

<a name="luke_1_19"></a>Luke 1:19

And the [aggelos](../../strongs/g/g32.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto him, I am [Gabriēl](../../strongs/g/g1043.md), that [paristēmi](../../strongs/g/g3936.md) in the [enōpion](../../strongs/g/g1799.md) of [theos](../../strongs/g/g2316.md); and am [apostellō](../../strongs/g/g649.md) to [laleō](../../strongs/g/g2980.md) unto thee, and to [euaggelizō](../../strongs/g/g2097.md) thee these.

<a name="luke_1_20"></a>Luke 1:20

And, [idou](../../strongs/g/g2400.md), thou shalt be [siōpaō](../../strongs/g/g4623.md), and not able to [laleō](../../strongs/g/g2980.md), until the [hēmera](../../strongs/g/g2250.md) that these things shall be [ginomai](../../strongs/g/g1096.md), because thou [pisteuō](../../strongs/g/g4100.md) not my [logos](../../strongs/g/g3056.md), which shall be [plēroō](../../strongs/g/g4137.md) in their [kairos](../../strongs/g/g2540.md).

<a name="luke_1_21"></a>Luke 1:21

And the [laos](../../strongs/g/g2992.md) [prosdokaō](../../strongs/g/g4328.md) for [Zacharias](../../strongs/g/g2197.md), and [thaumazō](../../strongs/g/g2296.md) that he [chronizō](../../strongs/g/g5549.md) in the [naos](../../strongs/g/g3485.md).

<a name="luke_1_22"></a>Luke 1:22

And when he [exerchomai](../../strongs/g/g1831.md), he could not [laleō](../../strongs/g/g2980.md) unto them: and they [epiginōskō](../../strongs/g/g1921.md) that he had [horaō](../../strongs/g/g3708.md) a [optasia](../../strongs/g/g3701.md) in the [naos](../../strongs/g/g3485.md): for he [dianeuō](../../strongs/g/g1269.md) unto them, and [diamenō](../../strongs/g/g1265.md) [kōphos](../../strongs/g/g2974.md).

<a name="luke_1_23"></a>Luke 1:23

And [ginomai](../../strongs/g/g1096.md), that, as soon as the [hēmera](../../strongs/g/g2250.md) of his [leitourgia](../../strongs/g/g3009.md) were [pimplēmi](../../strongs/g/g4130.md), he [aperchomai](../../strongs/g/g565.md) to his own [oikos](../../strongs/g/g3624.md).

<a name="luke_1_24"></a>Luke 1:24

And after those [hēmera](../../strongs/g/g2250.md) his [gynē](../../strongs/g/g1135.md) [Elisabet](../../strongs/g/g1665.md) [syllambanō](../../strongs/g/g4815.md), and [perikrybō](../../strongs/g/g4032.md) herself five [mēn](../../strongs/g/g3376.md), [legō](../../strongs/g/g3004.md),

<a name="luke_1_25"></a>Luke 1:25

Thus hath the [kyrios](../../strongs/g/g2962.md) [poieō](../../strongs/g/g4160.md) with me in the [hēmera](../../strongs/g/g2250.md) wherein he [epeidon](../../strongs/g/g1896.md) me, to [aphaireō](../../strongs/g/g851.md) my [oneidos](../../strongs/g/g3681.md) among [anthrōpos](../../strongs/g/g444.md).

<a name="luke_1_26"></a>Luke 1:26

And in the sixth [mēn](../../strongs/g/g3376.md) the [aggelos](../../strongs/g/g32.md) [Gabriēl](../../strongs/g/g1043.md) was [apostellō](../../strongs/g/g649.md) from [theos](../../strongs/g/g2316.md) unto a [polis](../../strongs/g/g4172.md) of [Galilaia](../../strongs/g/g1056.md), [onoma](../../strongs/g/g3686.md) [Nazara](../../strongs/g/g3478.md),

<a name="luke_1_27"></a>Luke 1:27

To a [parthenos](../../strongs/g/g3933.md) [mnēsteuō](../../strongs/g/g3423.md) to an [anēr](../../strongs/g/g435.md) whose [onoma](../../strongs/g/g3686.md) was [Iōsēph](../../strongs/g/g2501.md), of the [oikos](../../strongs/g/g3624.md) of [Dabid](../../strongs/g/g1138.md); and the [parthenos](../../strongs/g/g3933.md) [onoma](../../strongs/g/g3686.md) was [Maria](../../strongs/g/g3137.md).

<a name="luke_1_28"></a>Luke 1:28

And the [aggelos](../../strongs/g/g32.md) [eiserchomai](../../strongs/g/g1525.md) unto her, and [eipon](../../strongs/g/g2036.md), [chairō](../../strongs/g/g5463.md), thou [charitoō](../../strongs/g/g5487.md), the [kyrios](../../strongs/g/g2962.md) is with thee: [eulogeō](../../strongs/g/g2127.md) art thou among [gynē](../../strongs/g/g1135.md). [^1]

<a name="luke_1_29"></a>Luke 1:29

And when she [eidō](../../strongs/g/g1492.md), she was [diatarassō](../../strongs/g/g1298.md) at his [logos](../../strongs/g/g3056.md), and [dialogizomai](../../strongs/g/g1260.md) [potapos](../../strongs/g/g4217.md) of [aspasmos](../../strongs/g/g783.md) this should be.

<a name="luke_1_30"></a>Luke 1:30

And the [aggelos](../../strongs/g/g32.md) [eipon](../../strongs/g/g2036.md) unto her, [phobeō](../../strongs/g/g5399.md) not, [Maria](../../strongs/g/g3137.md): for thou hast [heuriskō](../../strongs/g/g2147.md) [charis](../../strongs/g/g5485.md) with [theos](../../strongs/g/g2316.md).

<a name="luke_1_31"></a>Luke 1:31

And, [idou](../../strongs/g/g2400.md), thou shalt [syllambanō](../../strongs/g/g4815.md) in thy [gastēr](../../strongs/g/g1064.md), and [tiktō](../../strongs/g/g5088.md) a [huios](../../strongs/g/g5207.md), and shalt [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md) [Iēsous](../../strongs/g/g2424.md).

<a name="luke_1_32"></a>Luke 1:32

He shall be [megas](../../strongs/g/g3173.md), and shall be [kaleō](../../strongs/g/g2564.md) the [huios](../../strongs/g/g5207.md) of the [hypsistos](../../strongs/g/g5310.md): and the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) shall [didōmi](../../strongs/g/g1325.md) unto him the [thronos](../../strongs/g/g2362.md) of his [patēr](../../strongs/g/g3962.md) [Dabid](../../strongs/g/g1138.md):

<a name="luke_1_33"></a>Luke 1:33

And he shall [basileuō](../../strongs/g/g936.md) over the [oikos](../../strongs/g/g3624.md) of [Iakōb](../../strongs/g/g2384.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md); and of his [basileia](../../strongs/g/g932.md) there shall be no [telos](../../strongs/g/g5056.md).

<a name="luke_1_34"></a>Luke 1:34

Then [eipon](../../strongs/g/g2036.md) [Maria](../../strongs/g/g3137.md) unto the [aggelos](../../strongs/g/g32.md), How shall this be, [epei](../../strongs/g/g1893.md) I [ginōskō](../../strongs/g/g1097.md) not an [anēr](../../strongs/g/g435.md)?

<a name="luke_1_35"></a>Luke 1:35

And the [aggelos](../../strongs/g/g32.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto her, The [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) shall [eperchomai](../../strongs/g/g1904.md) upon thee, and the [dynamis](../../strongs/g/g1411.md) of the [hypsistos](../../strongs/g/g5310.md) shall [episkiazō](../../strongs/g/g1982.md) thee: therefore also [hagios](../../strongs/g/g40.md) which shall be [gennaō](../../strongs/g/g1080.md) of thee shall be [kaleō](../../strongs/g/g2564.md) the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md).

<a name="luke_1_36"></a>Luke 1:36

And, [idou](../../strongs/g/g2400.md), thy [syggenēs](../../strongs/g/g4773.md) [Elisabet](../../strongs/g/g1665.md), she hath also [syllambanō](../../strongs/g/g4815.md) a [huios](../../strongs/g/g5207.md) in her [gēras](../../strongs/g/g1094.md): and this is the sixth [mēn](../../strongs/g/g3376.md) with her, who was [kaleō](../../strongs/g/g2564.md) [steira](../../strongs/g/g4723.md).

<a name="luke_1_37"></a>Luke 1:37

For with [theos](../../strongs/g/g2316.md) [ou](../../strongs/g/g3756.md) [pas](../../strongs/g/g3956.md) [rhēma](../../strongs/g/g4487.md) shall be [adynateō](../../strongs/g/g101.md).

<a name="luke_1_38"></a>Luke 1:38

And [Maria](../../strongs/g/g3137.md) [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md) the [doulē](../../strongs/g/g1399.md) of the [kyrios](../../strongs/g/g2962.md); be it unto me according to thy [rhēma](../../strongs/g/g4487.md). And the [aggelos](../../strongs/g/g32.md) [aperchomai](../../strongs/g/g565.md) from her.

<a name="luke_1_39"></a>Luke 1:39

And [Maria](../../strongs/g/g3137.md) [anistēmi](../../strongs/g/g450.md) in those [hēmera](../../strongs/g/g2250.md), and [poreuō](../../strongs/g/g4198.md) into the [oreinos](../../strongs/g/g3714.md) with [spoudē](../../strongs/g/g4710.md), into a [polis](../../strongs/g/g4172.md) of [Iouda](../../strongs/g/g2448.md);

<a name="luke_1_40"></a>Luke 1:40

And [eiserchomai](../../strongs/g/g1525.md) into the [oikos](../../strongs/g/g3624.md) of [Zacharias](../../strongs/g/g2197.md), and [aspazomai](../../strongs/g/g782.md) [Elisabet](../../strongs/g/g1665.md).

<a name="luke_1_41"></a>Luke 1:41

And [ginomai](../../strongs/g/g1096.md), that, when [Elisabet](../../strongs/g/g1665.md) [akouō](../../strongs/g/g191.md) the [aspasmos](../../strongs/g/g783.md) of [Maria](../../strongs/g/g3137.md), the [brephos](../../strongs/g/g1025.md) [skirtaō](../../strongs/g/g4640.md) in her [koilia](../../strongs/g/g2836.md); and [Elisabet](../../strongs/g/g1665.md) was [pimplēmi](../../strongs/g/g4130.md) with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md):

<a name="luke_1_42"></a>Luke 1:42

And she [anaphōneō](../../strongs/g/g400.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), and [eipon](../../strongs/g/g2036.md), [eulogeō](../../strongs/g/g2127.md) art thou among [gynē](../../strongs/g/g1135.md), and [eulogeō](../../strongs/g/g2127.md) is the [karpos](../../strongs/g/g2590.md) of thy [koilia](../../strongs/g/g2836.md).

<a name="luke_1_43"></a>Luke 1:43

And [pothen](../../strongs/g/g4159.md) this to me, that the [mētēr](../../strongs/g/g3384.md) of my [kyrios](../../strongs/g/g2962.md) should [erchomai](../../strongs/g/g2064.md) to me?

<a name="luke_1_44"></a>Luke 1:44

For, [idou](../../strongs/g/g2400.md), as soon as the [phōnē](../../strongs/g/g5456.md) of thy [aspasmos](../../strongs/g/g783.md) [ginomai](../../strongs/g/g1096.md) in mine [ous](../../strongs/g/g3775.md), the [brephos](../../strongs/g/g1025.md) [skirtaō](../../strongs/g/g4640.md) in my [koilia](../../strongs/g/g2836.md) for [agalliasis](../../strongs/g/g20.md).

<a name="luke_1_45"></a>Luke 1:45

And [makarios](../../strongs/g/g3107.md) [pisteuō](../../strongs/g/g4100.md): for there shall be a [teleiōsis](../../strongs/g/g5050.md) of those things which were [laleō](../../strongs/g/g2980.md) her from the [kyrios](../../strongs/g/g2962.md).

<a name="luke_1_46"></a>Luke 1:46

And [Maria](../../strongs/g/g3137.md) [eipon](../../strongs/g/g2036.md), My [psychē](../../strongs/g/g5590.md) doth [megalynō](../../strongs/g/g3170.md) the [kyrios](../../strongs/g/g2962.md),

<a name="luke_1_47"></a>Luke 1:47

And my [pneuma](../../strongs/g/g4151.md) hath [agalliaō](../../strongs/g/g21.md) in [theos](../../strongs/g/g2316.md) my [sōtēr](../../strongs/g/g4990.md).

<a name="luke_1_48"></a>Luke 1:48

For he hath [epiblepō](../../strongs/g/g1914.md) the [tapeinōsis](../../strongs/g/g5014.md) of his [doulē](../../strongs/g/g1399.md): for, [idou](../../strongs/g/g2400.md), from henceforth all [genea](../../strongs/g/g1074.md) shall [makarizō](../../strongs/g/g3106.md) me.

<a name="luke_1_49"></a>Luke 1:49

For he that is [dynatos](../../strongs/g/g1415.md) hath [poieō](../../strongs/g/g4160.md) to me [megaleios](../../strongs/g/g3167.md); and [hagios](../../strongs/g/g40.md) his [onoma](../../strongs/g/g3686.md).

<a name="luke_1_50"></a>Luke 1:50

And his [eleos](../../strongs/g/g1656.md) on them that [phobeō](../../strongs/g/g5399.md) him from [genea](../../strongs/g/g1074.md) to [genea](../../strongs/g/g1074.md).

<a name="luke_1_51"></a>Luke 1:51

He hath [poieō](../../strongs/g/g4160.md) [kratos](../../strongs/g/g2904.md) with his [brachiōn](../../strongs/g/g1023.md); he hath [diaskorpizō](../../strongs/g/g1287.md) the [hyperēphanos](../../strongs/g/g5244.md) in the [dianoia](../../strongs/g/g1271.md) of their [kardia](../../strongs/g/g2588.md).

<a name="luke_1_52"></a>Luke 1:52

He hath [kathaireō](../../strongs/g/g2507.md) the [dynastēs](../../strongs/g/g1413.md) from their [thronos](../../strongs/g/g2362.md), and [hypsoō](../../strongs/g/g5312.md) them of [tapeinos](../../strongs/g/g5011.md).

<a name="luke_1_53"></a>Luke 1:53

He hath [empi(m)plēmi](../../strongs/g/g1705.md) the [peinaō](../../strongs/g/g3983.md) with [agathos](../../strongs/g/g18.md); and the [plouteō](../../strongs/g/g4147.md) he hath [exapostellō](../../strongs/g/g1821.md) [kenos](../../strongs/g/g2756.md).

<a name="luke_1_54"></a>Luke 1:54

He hath [antilambanō](../../strongs/g/g482.md) his [pais](../../strongs/g/g3816.md) [Israēl](../../strongs/g/g2474.md), in [mnaomai](../../strongs/g/g3415.md) of his [eleos](../../strongs/g/g1656.md);

<a name="luke_1_55"></a>Luke 1:55

As he [laleō](../../strongs/g/g2980.md) to our [patēr](../../strongs/g/g3962.md), to [Abraam](../../strongs/g/g11.md), and to his [sperma](../../strongs/g/g4690.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md).

<a name="luke_1_56"></a>Luke 1:56

And [Maria](../../strongs/g/g3137.md) [menō](../../strongs/g/g3306.md) with her about three [mēn](../../strongs/g/g3376.md), and [hypostrephō](../../strongs/g/g5290.md) to her own [oikos](../../strongs/g/g3624.md).

<a name="luke_1_57"></a>Luke 1:57

Now [Elisabet](../../strongs/g/g1665.md) [pimplēmi](../../strongs/g/g4130.md) [chronos](../../strongs/g/g5550.md) that she should be [tiktō](../../strongs/g/g5088.md); and she [gennaō](../../strongs/g/g1080.md) a [huios](../../strongs/g/g5207.md).

<a name="luke_1_58"></a>Luke 1:58

And her [perioikos](../../strongs/g/g4040.md) and her [syggenēs](../../strongs/g/g4773.md) [akouō](../../strongs/g/g191.md) how the [kyrios](../../strongs/g/g2962.md) had [megalynō](../../strongs/g/g3170.md) [eleos](../../strongs/g/g1656.md) upon her; and they [sygchairō](../../strongs/g/g4796.md) with her.

<a name="luke_1_59"></a>Luke 1:59

And [ginomai](../../strongs/g/g1096.md), that on the eighth [hēmera](../../strongs/g/g2250.md) they [erchomai](../../strongs/g/g2064.md) to [peritemnō](../../strongs/g/g4059.md) the [paidion](../../strongs/g/g3813.md); and they [kaleō](../../strongs/g/g2564.md) him [Zacharias](../../strongs/g/g2197.md), after the [onoma](../../strongs/g/g3686.md) of his [patēr](../../strongs/g/g3962.md).

<a name="luke_1_60"></a>Luke 1:60

And his [mētēr](../../strongs/g/g3384.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), [ouchi](../../strongs/g/g3780.md); but he shall be [kaleō](../../strongs/g/g2564.md) [Iōannēs](../../strongs/g/g2491.md).

<a name="luke_1_61"></a>Luke 1:61

And they [eipon](../../strongs/g/g2036.md) unto her, There is [oudeis](../../strongs/g/g3762.md) of thy [syggeneia](../../strongs/g/g4772.md) that is [kaleō](../../strongs/g/g2564.md) by this [onoma](../../strongs/g/g3686.md).

<a name="luke_1_62"></a>Luke 1:62

And they made [enneuō](../../strongs/g/g1770.md) to his [patēr](../../strongs/g/g3962.md), how he would have him [kaleō](../../strongs/g/g2564.md).

<a name="luke_1_63"></a>Luke 1:63

And he [aiteō](../../strongs/g/g154.md) for a [pinakidion](../../strongs/g/g4093.md), and [graphō](../../strongs/g/g1125.md), [legō](../../strongs/g/g3004.md), His [onoma](../../strongs/g/g3686.md) is [Iōannēs](../../strongs/g/g2491.md). And they [thaumazō](../../strongs/g/g2296.md) all.

<a name="luke_1_64"></a>Luke 1:64

And his [stoma](../../strongs/g/g4750.md) was [anoigō](../../strongs/g/g455.md) [parachrēma](../../strongs/g/g3916.md), and his [glōssa](../../strongs/g/g1100.md), and he [laleō](../../strongs/g/g2980.md), and [eulogeō](../../strongs/g/g2127.md) [theos](../../strongs/g/g2316.md).

<a name="luke_1_65"></a>Luke 1:65

And [phobos](../../strongs/g/g5401.md) [ginomai](../../strongs/g/g1096.md) on all that [perioikeō](../../strongs/g/g4039.md) them: and all these [rhēma](../../strongs/g/g4487.md) were [dialaleō](../../strongs/g/g1255.md) throughout all the [oreinos](../../strongs/g/g3714.md) of [Ioudaia](../../strongs/g/g2449.md).

<a name="luke_1_66"></a>Luke 1:66

And all they that [akouō](../../strongs/g/g191.md) them [tithēmi](../../strongs/g/g5087.md) in their [kardia](../../strongs/g/g2588.md), [legō](../../strongs/g/g3004.md), What manner of [paidion](../../strongs/g/g3813.md) shall this be! And the [cheir](../../strongs/g/g5495.md) of the [kyrios](../../strongs/g/g2962.md) was with him.

<a name="luke_1_67"></a>Luke 1:67

And his [patēr](../../strongs/g/g3962.md) [Zacharias](../../strongs/g/g2197.md) was [pimplēmi](../../strongs/g/g4130.md) with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), and [prophēteuō](../../strongs/g/g4395.md), [legō](../../strongs/g/g3004.md),

<a name="luke_1_68"></a>Luke 1:68

[eulogētos](../../strongs/g/g2128.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) of [Israēl](../../strongs/g/g2474.md); for he hath [episkeptomai](../../strongs/g/g1980.md) and [lytrōsis](../../strongs/g/g3085.md) his [laos](../../strongs/g/g2992.md) [poieō](../../strongs/g/g4160.md),

<a name="luke_1_69"></a>Luke 1:69

And hath [egeirō](../../strongs/g/g1453.md) a [keras](../../strongs/g/g2768.md) of [sōtēria](../../strongs/g/g4991.md) for us in the [oikos](../../strongs/g/g3624.md) of his [pais](../../strongs/g/g3816.md) [Dabid](../../strongs/g/g1138.md);

<a name="luke_1_70"></a>Luke 1:70

As he [laleō](../../strongs/g/g2980.md) by the [stoma](../../strongs/g/g4750.md) of his [hagios](../../strongs/g/g40.md) [prophētēs](../../strongs/g/g4396.md), which have been since the [aiōn](../../strongs/g/g165.md):

<a name="luke_1_71"></a>Luke 1:71

That we should be [sōtēria](../../strongs/g/g4991.md) from our [echthros](../../strongs/g/g2190.md), and from the [cheir](../../strongs/g/g5495.md) of all that [miseō](../../strongs/g/g3404.md) us;

<a name="luke_1_72"></a>Luke 1:72

To [poieō](../../strongs/g/g4160.md) the [eleos](../../strongs/g/g1656.md) to our [patēr](../../strongs/g/g3962.md), and to [mnaomai](../../strongs/g/g3415.md) his [hagios](../../strongs/g/g40.md) [diathēkē](../../strongs/g/g1242.md);

<a name="luke_1_73"></a>Luke 1:73

The [horkos](../../strongs/g/g3727.md) which he [omnyō](../../strongs/g/g3660.md) to our [patēr](../../strongs/g/g3962.md) [Abraam](../../strongs/g/g11.md),

<a name="luke_1_74"></a>Luke 1:74

That he would [didōmi](../../strongs/g/g1325.md) unto us, that we being [rhyomai](../../strongs/g/g4506.md) out of the [cheir](../../strongs/g/g5495.md) of our [echthros](../../strongs/g/g2190.md) might [latreuō](../../strongs/g/g3000.md) him [aphobōs](../../strongs/g/g870.md),

<a name="luke_1_75"></a>Luke 1:75

In [hosiotēs](../../strongs/g/g3742.md) and [dikaiosynē](../../strongs/g/g1343.md) before him, all the [hēmera](../../strongs/g/g2250.md) of our [zōē](../../strongs/g/g2222.md).

<a name="luke_1_76"></a>Luke 1:76

And thou, [paidion](../../strongs/g/g3813.md), shalt be [kaleō](../../strongs/g/g2564.md) the [prophētēs](../../strongs/g/g4396.md) of the [hypsistos](../../strongs/g/g5310.md): for thou shalt [proporeuomai](../../strongs/g/g4313.md) before the [prosōpon](../../strongs/g/g4383.md) of the [kyrios](../../strongs/g/g2962.md) to [hetoimazō](../../strongs/g/g2090.md) his [hodos](../../strongs/g/g3598.md);

<a name="luke_1_77"></a>Luke 1:77

To [didōmi](../../strongs/g/g1325.md) [gnōsis](../../strongs/g/g1108.md) of [sōtēria](../../strongs/g/g4991.md) unto his [laos](../../strongs/g/g2992.md) by the [aphesis](../../strongs/g/g859.md) of their [hamartia](../../strongs/g/g266.md),

<a name="luke_1_78"></a>Luke 1:78

Through the [splagchnon](../../strongs/g/g4698.md) [eleos](../../strongs/g/g1656.md) of our [theos](../../strongs/g/g2316.md); whereby the [anatolē](../../strongs/g/g395.md) from [hypsos](../../strongs/g/g5311.md) hath [episkeptomai](../../strongs/g/g1980.md) us,

<a name="luke_1_79"></a>Luke 1:79

To [epiphainō](../../strongs/g/g2014.md) to them that [kathēmai](../../strongs/g/g2521.md) in [skotos](../../strongs/g/g4655.md) and in the [skia](../../strongs/g/g4639.md) of [thanatos](../../strongs/g/g2288.md), to [kateuthynō](../../strongs/g/g2720.md) our [pous](../../strongs/g/g4228.md) into the [hodos](../../strongs/g/g3598.md) of [eirēnē](../../strongs/g/g1515.md).

<a name="luke_1_80"></a>Luke 1:80

And the [paidion](../../strongs/g/g3813.md) [auxanō](../../strongs/g/g837.md), and [krataioō](../../strongs/g/g2901.md) in [pneuma](../../strongs/g/g4151.md), and was in the [erēmos](../../strongs/g/g2048.md) till the [hēmera](../../strongs/g/g2250.md) of his [anadeixis](../../strongs/g/g323.md) unto [Israēl](../../strongs/g/g2474.md).

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 2](luke_2.md)

---

[^1]: [Luke 1:28 Commentary](../../commentary/luke/luke_1_commentary.md#luke_1_28)
