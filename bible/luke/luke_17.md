# [Luke 17](https://www.blueletterbible.org/kjv/luk/17/1/rl1/s_990001)

<a name="luke_17_1"></a>Luke 17:1

Then [eipon](../../strongs/g/g2036.md) he unto the [mathētēs](../../strongs/g/g3101.md), **It is [anendektos](../../strongs/g/g418.md) but that [skandalon](../../strongs/g/g4625.md) will [erchomai](../../strongs/g/g2064.md): but [ouai](../../strongs/g/g3759.md) unto him, through whom they [erchomai](../../strongs/g/g2064.md)!**

<a name="luke_17_2"></a>Luke 17:2

**[lysiteleō](../../strongs/g/g3081.md) for him that a [mylos](../../strongs/g/g3458.md) [onikos](../../strongs/g/g3684.md) were [perikeimai](../../strongs/g/g4029.md) about his [trachēlos](../../strongs/g/g5137.md), and he [rhiptō](../../strongs/g/g4496.md) into the [thalassa](../../strongs/g/g2281.md), than that he should [skandalizō](../../strongs/g/g4624.md) one of these [mikros](../../strongs/g/g3398.md).**

<a name="luke_17_3"></a>Luke 17:3

**[prosechō](../../strongs/g/g4337.md) to yourselves: If thy [adelphos](../../strongs/g/g80.md) [hamartanō](../../strongs/g/g264.md) against thee, [epitimaō](../../strongs/g/g2008.md) him; and if he [metanoeō](../../strongs/g/g3340.md), [aphiēmi](../../strongs/g/g863.md) him.**

<a name="luke_17_4"></a>Luke 17:4

**And if he [hamartanō](../../strongs/g/g264.md) against thee seven times in a [hēmera](../../strongs/g/g2250.md), and seven times in a [hēmera](../../strongs/g/g2250.md) [epistrephō](../../strongs/g/g1994.md) to thee, [legō](../../strongs/g/g3004.md), I [metanoeō](../../strongs/g/g3340.md); thou shalt [aphiēmi](../../strongs/g/g863.md) him.**

<a name="luke_17_5"></a>Luke 17:5

And the [apostolos](../../strongs/g/g652.md) [eipon](../../strongs/g/g2036.md) unto the [kyrios](../../strongs/g/g2962.md), [prostithēmi](../../strongs/g/g4369.md) our [pistis](../../strongs/g/g4102.md).

<a name="luke_17_6"></a>Luke 17:6

And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md), **If ye had [pistis](../../strongs/g/g4102.md) as a [kokkos](../../strongs/g/g2848.md) of [sinapi](../../strongs/g/g4615.md), ye might [legō](../../strongs/g/g3004.md) unto this [sykaminos](../../strongs/g/g4807.md), Be thou [ekrizoō](../../strongs/g/g1610.md), and be thou [phyteuō](../../strongs/g/g5452.md) in the [thalassa](../../strongs/g/g2281.md); and it should [hypakouō](../../strongs/g/g5219.md) you.**

<a name="luke_17_7"></a>Luke 17:7

**But which of you, having a [doulos](../../strongs/g/g1401.md) [arotriaō](../../strongs/g/g722.md) or [poimainō](../../strongs/g/g4165.md), will [eipon](../../strongs/g/g2046.md) unto him [eutheōs](../../strongs/g/g2112.md), when he is [eiserchomai](../../strongs/g/g1525.md) from the [agros](../../strongs/g/g68.md), [parerchomai](../../strongs/g/g3928.md) [anapiptō](../../strongs/g/g377.md)?**

<a name="luke_17_8"></a>Luke 17:8

**And will not rather [eipon](../../strongs/g/g2046.md) unto him, [hetoimazō](../../strongs/g/g2090.md) wherewith I may [deipneō](../../strongs/g/g1172.md), and [perizōnnymi](../../strongs/g/g4024.md), and [diakoneō](../../strongs/g/g1247.md) me, till I have [phago](../../strongs/g/g5315.md) and [pinō](../../strongs/g/g4095.md); and afterward thou shalt [phago](../../strongs/g/g5315.md) and [pinō](../../strongs/g/g4095.md)?**

<a name="luke_17_9"></a>Luke 17:9

**Doth he [charis](../../strongs/g/g5485.md) that [doulos](../../strongs/g/g1401.md) because he [poieō](../../strongs/g/g4160.md) the things that were [diatassō](../../strongs/g/g1299.md) him? I [dokeō](../../strongs/g/g1380.md) not.**

<a name="luke_17_10"></a>Luke 17:10

**So likewise ye, when ye shall have [poieō](../../strongs/g/g4160.md) all [diatassō](../../strongs/g/g1299.md) you, [legō](../../strongs/g/g3004.md), We are [achreios](../../strongs/g/g888.md) [doulos](../../strongs/g/g1401.md): we have [poieō](../../strongs/g/g4160.md) that which was our [opheilō](../../strongs/g/g3784.md) to [poieō](../../strongs/g/g4160.md).**

<a name="luke_17_11"></a>Luke 17:11

And [ginomai](../../strongs/g/g1096.md), as he [poreuō](../../strongs/g/g4198.md) to [Ierousalēm](../../strongs/g/g2419.md), that he [dierchomai](../../strongs/g/g1330.md) through the [mesos](../../strongs/g/g3319.md) of [Samareia](../../strongs/g/g4540.md) and [Galilaia](../../strongs/g/g1056.md).

<a name="luke_17_12"></a>Luke 17:12

And as he [eiserchomai](../../strongs/g/g1525.md) into a certain [kōmē](../../strongs/g/g2968.md), there [apantaō](../../strongs/g/g528.md) him ten [anēr](../../strongs/g/g435.md) that were [lepros](../../strongs/g/g3015.md), which [histēmi](../../strongs/g/g2476.md) [porrōthen](../../strongs/g/g4207.md):

<a name="luke_17_13"></a>Luke 17:13

And they [airō](../../strongs/g/g142.md) [phōnē](../../strongs/g/g5456.md), and [legō](../../strongs/g/g3004.md), [Iēsous](../../strongs/g/g2424.md), [epistatēs](../../strongs/g/g1988.md), have [eleeō](../../strongs/g/g1653.md) on us.

<a name="luke_17_14"></a>Luke 17:14

And when he [eidō](../../strongs/g/g1492.md), he [eipon](../../strongs/g/g2036.md) unto them, **[poreuō](../../strongs/g/g4198.md) [epideiknymi](../../strongs/g/g1925.md) yourselves unto the [hiereus](../../strongs/g/g2409.md).** And [ginomai](../../strongs/g/g1096.md), as they [hypagō](../../strongs/g/g5217.md), they were [katharizō](../../strongs/g/g2511.md).

<a name="luke_17_15"></a>Luke 17:15

And one of them, when he [eidō](../../strongs/g/g1492.md) that he was [iaomai](../../strongs/g/g2390.md), [hypostrephō](../../strongs/g/g5290.md), and with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md) [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md),

<a name="luke_17_16"></a>Luke 17:16

And [piptō](../../strongs/g/g4098.md) on [prosōpon](../../strongs/g/g4383.md) at his [pous](../../strongs/g/g4228.md), [eucharisteō](../../strongs/g/g2168.md) him: and he was a [Samaritēs](../../strongs/g/g4541.md).

<a name="luke_17_17"></a>Luke 17:17

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md), **Were there not ten [katharizō](../../strongs/g/g2511.md)? but where the nine?**

<a name="luke_17_18"></a>Luke 17:18

**There are not [heuriskō](../../strongs/g/g2147.md) that [hypostrephō](../../strongs/g/g5290.md) to [didōmi](../../strongs/g/g1325.md) [doxa](../../strongs/g/g1391.md) to [theos](../../strongs/g/g2316.md), [ei mē](../../strongs/g/g1508.md) this [allogenēs](../../strongs/g/g241.md).**

<a name="luke_17_19"></a>Luke 17:19

And he [eipon](../../strongs/g/g2036.md) unto him, **[anistēmi](../../strongs/g/g450.md), [poreuō](../../strongs/g/g4198.md): thy [pistis](../../strongs/g/g4102.md) hath [sōzō](../../strongs/g/g4982.md) thee.**

<a name="luke_17_20"></a>Luke 17:20

And when he was [eperōtaō](../../strongs/g/g1905.md) of the [Pharisaios](../../strongs/g/g5330.md), when the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) should [erchomai](../../strongs/g/g2064.md), he [apokrinomai](../../strongs/g/g611.md) them and [eipon](../../strongs/g/g2036.md), **The [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) [erchomai](../../strongs/g/g2064.md) not with [paratērēsis](../../strongs/g/g3907.md):**

<a name="luke_17_21"></a>Luke 17:21

**Neither shall they [eipon](../../strongs/g/g2046.md), [idou](../../strongs/g/g2400.md) [hōde](../../strongs/g/g5602.md)! or, [idou](../../strongs/g/g2400.md) [ekei](../../strongs/g/g1563.md)! for, [idou](../../strongs/g/g2400.md), the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) is [entos](../../strongs/g/g1787.md) you.**

<a name="luke_17_22"></a>Luke 17:22

And he [eipon](../../strongs/g/g2036.md) unto the [mathētēs](../../strongs/g/g3101.md), **The [hēmera](../../strongs/g/g2250.md) will [erchomai](../../strongs/g/g2064.md), when ye shall [epithymeō](../../strongs/g/g1937.md) to [eidō](../../strongs/g/g1492.md) one of the [hēmera](../../strongs/g/g2250.md) of the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md), and ye shall [optanomai](../../strongs/g/g3700.md) not it.**

<a name="luke_17_23"></a>Luke 17:23

**And they shall [eipon](../../strongs/g/g2046.md) to you, [idou](../../strongs/g/g2400.md) [hōde](../../strongs/g/g5602.md); or, [idou](../../strongs/g/g2400.md) [ekei](../../strongs/g/g1563.md): [aperchomai](../../strongs/g/g565.md) not, nor [diōkō](../../strongs/g/g1377.md).**

<a name="luke_17_24"></a>Luke 17:24

**For as the [astrapē](../../strongs/g/g796.md), that [astraptō](../../strongs/g/g797.md) out of the one under [ouranos](../../strongs/g/g3772.md), [lampō](../../strongs/g/g2989.md) unto the other under [ouranos](../../strongs/g/g3772.md); so shall also the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) be in his [hēmera](../../strongs/g/g2250.md).**

<a name="luke_17_25"></a>Luke 17:25

**But [prōton](../../strongs/g/g4412.md) must he [paschō](../../strongs/g/g3958.md) [polys](../../strongs/g/g4183.md), and be [apodokimazō](../../strongs/g/g593.md) of this [genea](../../strongs/g/g1074.md).**

<a name="luke_17_26"></a>Luke 17:26

**And as it was in the [hēmera](../../strongs/g/g2250.md) of [Nōe](../../strongs/g/g3575.md), so shall it be also in the [hēmera](../../strongs/g/g2250.md) of the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md).**

<a name="luke_17_27"></a>Luke 17:27

**They did [esthiō](../../strongs/g/g2068.md), they [pinō](../../strongs/g/g4095.md), they [gameō](../../strongs/g/g1060.md), they were [ekgamizō](../../strongs/g/g1547.md), until the [hēmera](../../strongs/g/g2250.md) that [Nōe](../../strongs/g/g3575.md) [eiserchomai](../../strongs/g/g1525.md) into the [kibōtos](../../strongs/g/g2787.md), and the [kataklysmos](../../strongs/g/g2627.md) [erchomai](../../strongs/g/g2064.md), and [apollymi](../../strongs/g/g622.md) them all.**

<a name="luke_17_28"></a>Luke 17:28

**[homoiōs](../../strongs/g/g3668.md) also as it was in the [hēmera](../../strongs/g/g2250.md) of [Lōt](../../strongs/g/g3091.md); they did [esthiō](../../strongs/g/g2068.md), they [pinō](../../strongs/g/g4095.md), they [agorazō](../../strongs/g/g59.md), they [pōleō](../../strongs/g/g4453.md), they [phyteuō](../../strongs/g/g5452.md), they [oikodomeō](../../strongs/g/g3618.md);**

<a name="luke_17_29"></a>Luke 17:29

**But the [hēmera](../../strongs/g/g2250.md) that [Lōt](../../strongs/g/g3091.md) [exerchomai](../../strongs/g/g1831.md) of [Sodoma](../../strongs/g/g4670.md) it [brechō](../../strongs/g/g1026.md) [pyr](../../strongs/g/g4442.md) and [theion](../../strongs/g/g2303.md) from [ouranos](../../strongs/g/g3772.md), and [apollymi](../../strongs/g/g622.md) them all.**

<a name="luke_17_30"></a>Luke 17:30

**Even thus shall it be in the [hēmera](../../strongs/g/g2250.md) when the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) is [apokalyptō](../../strongs/g/g601.md).**

<a name="luke_17_31"></a>Luke 17:31

**In that [hēmera](../../strongs/g/g2250.md), he which shall be upon the [dōma](../../strongs/g/g1430.md), and his [skeuos](../../strongs/g/g4632.md) in the [oikia](../../strongs/g/g3614.md), let him not [katabainō](../../strongs/g/g2597.md) to [airō](../../strongs/g/g142.md) it: and he that is in the [agros](../../strongs/g/g68.md), let him [homoiōs](../../strongs/g/g3668.md) not [epistrephō](../../strongs/g/g1994.md) back.**

<a name="luke_17_32"></a>Luke 17:32

**[mnēmoneuō](../../strongs/g/g3421.md) [Lōt](../../strongs/g/g3091.md) [gynē](../../strongs/g/g1135.md).**

<a name="luke_17_33"></a>Luke 17:33

**Whosoever shall [zēteō](../../strongs/g/g2212.md) to [sōzō](../../strongs/g/g4982.md) his [psychē](../../strongs/g/g5590.md) shall [apollymi](../../strongs/g/g622.md) it; and whosoever shall [apollymi](../../strongs/g/g622.md) his shall [zōogoneō](../../strongs/g/g2225.md) it.**

<a name="luke_17_34"></a>Luke 17:34

**I [legō](../../strongs/g/g3004.md) you, in that [nyx](../../strongs/g/g3571.md) there shall be two [epi](../../strongs/g/g1909.md) one [klinē](../../strongs/g/g2825.md); the one shall be [paralambanō](../../strongs/g/g3880.md), and the other shall be [aphiēmi](../../strongs/g/g863.md).**

<a name="luke_17_35"></a>Luke 17:35

**Two shall be [alēthō](../../strongs/g/g229.md) together; the one shall be [paralambanō](../../strongs/g/g3880.md), and the other [aphiēmi](../../strongs/g/g863.md).**

<a name="luke_17_36"></a>Luke 17:36

**Two shall be in the [agros](../../strongs/g/g68.md); the one shall be [paralambanō](../../strongs/g/g3880.md), and the other [aphiēmi](../../strongs/g/g863.md).** [^1]

<a name="luke_17_37"></a>Luke 17:37

And they [apokrinomai](../../strongs/g/g611.md) and [legō](../../strongs/g/g3004.md) unto him, [pou](../../strongs/g/g4226.md), [kyrios](../../strongs/g/g2962.md)? And he [eipon](../../strongs/g/g2036.md) unto them, **Wheresoever the [sōma](../../strongs/g/g4983.md) is, thither will the [aetos](../../strongs/g/g105.md) be [synagō](../../strongs/g/g4863.md).**

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 16](luke_16.md) - [Luke 18](luke_18.md)

---

[^1]: [Luke 17:36 Commentary](../../commentary/luke/luke_17_commentary.md#luke_17_36)
