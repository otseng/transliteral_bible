# [Luke 21](https://www.blueletterbible.org/kjv/luk/21/1/rl1/s_994001)

<a name="luke_21_1"></a>Luke 21:1

And he [anablepō](../../strongs/g/g308.md), and [eidō](../../strongs/g/g1492.md) the [plousios](../../strongs/g/g4145.md) [ballō](../../strongs/g/g906.md) their [dōron](../../strongs/g/g1435.md) into the [gazophylakion](../../strongs/g/g1049.md).

<a name="luke_21_2"></a>Luke 21:2

And he [eidō](../../strongs/g/g1492.md) also a [tis](../../strongs/g/g5100.md) [penichros](../../strongs/g/g3998.md) [chēra](../../strongs/g/g5503.md) [ballō](../../strongs/g/g906.md) in thither two [lepton](../../strongs/g/g3016.md).

<a name="luke_21_3"></a>Luke 21:3

And he [eipon](../../strongs/g/g2036.md), **[alēthōs](../../strongs/g/g230.md) I [legō](../../strongs/g/g3004.md) unto you, that this [ptōchos](../../strongs/g/g4434.md) [chēra](../../strongs/g/g5503.md) hath [ballō](../../strongs/g/g906.md) in [pleiōn](../../strongs/g/g4119.md) they all:**

<a name="luke_21_4"></a>Luke 21:4

**For all these have of their [perisseuō](../../strongs/g/g4052.md) [ballō](../../strongs/g/g906.md) in unto the [dōron](../../strongs/g/g1435.md) of [theos](../../strongs/g/g2316.md): but she of her [hysterēma](../../strongs/g/g5303.md) hath [ballō](../../strongs/g/g906.md) all the [bios](../../strongs/g/g979.md) that she had.**

<a name="luke_21_5"></a>Luke 21:5

And as some [legō](../../strongs/g/g3004.md) of the [hieron](../../strongs/g/g2411.md), how it was [kosmeō](../../strongs/g/g2885.md) with [kalos](../../strongs/g/g2570.md) [lithos](../../strongs/g/g3037.md) and [anathēma](../../strongs/g/g334.md), he [eipon](../../strongs/g/g2036.md),

<a name="luke_21_6"></a>Luke 21:6

**these things which ye [theōreō](../../strongs/g/g2334.md), the [hēmera](../../strongs/g/g2250.md) will [erchomai](../../strongs/g/g2064.md), in the which there shall not [aphiēmi](../../strongs/g/g863.md) [lithos](../../strongs/g/g3037.md) upon another, that shall not be [katalyō](../../strongs/g/g2647.md).**

<a name="luke_21_7"></a>Luke 21:7

And they [eperōtaō](../../strongs/g/g1905.md) him, [legō](../../strongs/g/g3004.md), [didaskalos](../../strongs/g/g1320.md), but when shall these things be? and what [sēmeion](../../strongs/g/g4592.md) when these things shall [ginomai](../../strongs/g/g1096.md)?

<a name="luke_21_8"></a>Luke 21:8

And he [eipon](../../strongs/g/g2036.md), **[blepō](../../strongs/g/g991.md) that ye be not [planaō](../../strongs/g/g4105.md): for [polys](../../strongs/g/g4183.md) shall [erchomai](../../strongs/g/g2064.md) in my [onoma](../../strongs/g/g3686.md), [legō](../../strongs/g/g3004.md), I am; and the [kairos](../../strongs/g/g2540.md) [eggizō](../../strongs/g/g1448.md): [poreuō](../../strongs/g/g4198.md) ye not therefore after them.**

<a name="luke_21_9"></a>Luke 21:9

**But when ye shall [akouō](../../strongs/g/g191.md) of [polemos](../../strongs/g/g4171.md) and [akatastasia](../../strongs/g/g181.md), be not [ptoeō](../../strongs/g/g4422.md): for these things must first [ginomai](../../strongs/g/g1096.md); but the [telos](../../strongs/g/g5056.md) not [eutheōs](../../strongs/g/g2112.md).**

<a name="luke_21_10"></a>Luke 21:10

Then [legō](../../strongs/g/g3004.md) he unto them, **[ethnos](../../strongs/g/g1484.md) shall [egeirō](../../strongs/g/g1453.md) against [ethnos](../../strongs/g/g1484.md), and [basileia](../../strongs/g/g932.md) against [basileia](../../strongs/g/g932.md):**

<a name="luke_21_11"></a>Luke 21:11

**And [megas](../../strongs/g/g3173.md) [seismos](../../strongs/g/g4578.md) shall be in [topos](../../strongs/g/g5117.md), and [limos](../../strongs/g/g3042.md), and [loimos](../../strongs/g/g3061.md); and [phobētron](../../strongs/g/g5400.md) and [megas](../../strongs/g/g3173.md) [sēmeion](../../strongs/g/g4592.md) shall there be from [ouranos](../../strongs/g/g3772.md).**

<a name="luke_21_12"></a>Luke 21:12

**But before all these, they shall [epiballō](../../strongs/g/g1911.md) their [cheir](../../strongs/g/g5495.md) on you, and [diōkō](../../strongs/g/g1377.md), [paradidōmi](../../strongs/g/g3860.md) to the [synagōgē](../../strongs/g/g4864.md), and into [phylakē](../../strongs/g/g5438.md), being [agō](../../strongs/g/g71.md) before [basileus](../../strongs/g/g935.md) and [hēgemōn](../../strongs/g/g2232.md) for my [onoma](../../strongs/g/g3686.md) sake.**

<a name="luke_21_13"></a>Luke 21:13

**And it shall [apobainō](../../strongs/g/g576.md) to you for a [martyrion](../../strongs/g/g3142.md).**

<a name="luke_21_14"></a>Luke 21:14

**[tithēmi](../../strongs/g/g5087.md) therefore in your [kardia](../../strongs/g/g2588.md), not to [promeletaō](../../strongs/g/g4304.md) what ye shall [apologeomai](../../strongs/g/g626.md):**

<a name="luke_21_15"></a>Luke 21:15

**For I will [didōmi](../../strongs/g/g1325.md) you [stoma](../../strongs/g/g4750.md) and [sophia](../../strongs/g/g4678.md), which all your [antikeimai](../../strongs/g/g480.md) shall not be able to [anteipon](../../strongs/g/g471.md) nor [anthistēmi](../../strongs/g/g436.md).**

<a name="luke_21_16"></a>Luke 21:16

**And ye shall [paradidōmi](../../strongs/g/g3860.md) both by [goneus](../../strongs/g/g1118.md), and [adelphos](../../strongs/g/g80.md), and [syggenēs](../../strongs/g/g4773.md), and [philos](../../strongs/g/g5384.md); and of you shall [thanatoō](../../strongs/g/g2289.md).**

<a name="luke_21_17"></a>Luke 21:17

**And ye shall be [miseō](../../strongs/g/g3404.md) of all for my [onoma](../../strongs/g/g3686.md).**

<a name="luke_21_18"></a>Luke 21:18

**But there shall not a [thrix](../../strongs/g/g2359.md) of your [kephalē](../../strongs/g/g2776.md) [apollymi](../../strongs/g/g622.md).**

<a name="luke_21_19"></a>Luke 21:19

**In your [hypomonē](../../strongs/g/g5281.md) [ktaomai](../../strongs/g/g2932.md) ye your [psychē](../../strongs/g/g5590.md).**

<a name="luke_21_20"></a>Luke 21:20

**And when ye shall [eidō](../../strongs/g/g1492.md) [Ierousalēm](../../strongs/g/g2419.md) [kykloō](../../strongs/g/g2944.md) with [stratopedon](../../strongs/g/g4760.md), then [ginōskō](../../strongs/g/g1097.md) that the [erēmōsis](../../strongs/g/g2050.md) thereof [eggizō](../../strongs/g/g1448.md).**

<a name="luke_21_21"></a>Luke 21:21

**Then let them which are in [Ioudaia](../../strongs/g/g2449.md) [pheugō](../../strongs/g/g5343.md) to the [oros](../../strongs/g/g3735.md); and let them which are in the [mesos](../../strongs/g/g3319.md) of it [ekchōreō](../../strongs/g/g1633.md); and let not them that are in the [chōra](../../strongs/g/g5561.md) [eiserchomai](../../strongs/g/g1525.md) thereinto.**

<a name="luke_21_22"></a>Luke 21:22

**For these be the [hēmera](../../strongs/g/g2250.md) of [ekdikēsis](../../strongs/g/g1557.md), that all things which are [graphō](../../strongs/g/g1125.md) may be [plēroō](../../strongs/g/g4137.md).**

<a name="luke_21_23"></a>Luke 21:23

**But [ouai](../../strongs/g/g3759.md) unto [gastēr](../../strongs/g/g1064.md), and to [thēlazō](../../strongs/g/g2337.md), in those days! for there shall be [megas](../../strongs/g/g3173.md) [anagkē](../../strongs/g/g318.md) in [gē](../../strongs/g/g1093.md), and [orgē](../../strongs/g/g3709.md) upon this [laos](../../strongs/g/g2992.md).**

<a name="luke_21_24"></a>Luke 21:24

**And they shall [piptō](../../strongs/g/g4098.md) by the [stoma](../../strongs/g/g4750.md) of the [machaira](../../strongs/g/g3162.md), and shall be [aichmalōtizō](../../strongs/g/g163.md) into all [ethnos](../../strongs/g/g1484.md): and [Ierousalēm](../../strongs/g/g2419.md) shall be [pateō](../../strongs/g/g3961.md) of the [ethnos](../../strongs/g/g1484.md), until the [kairos](../../strongs/g/g2540.md) of the [ethnos](../../strongs/g/g1484.md) be [plēroō](../../strongs/g/g4137.md).**

<a name="luke_21_25"></a>Luke 21:25

**And there shall be [sēmeion](../../strongs/g/g4592.md) in the [hēlios](../../strongs/g/g2246.md), and in the [selēnē](../../strongs/g/g4582.md), and in the [astron](../../strongs/g/g798.md); and upon the [gē](../../strongs/g/g1093.md) [synochē](../../strongs/g/g4928.md) of [ethnos](../../strongs/g/g1484.md), with [aporia](../../strongs/g/g640.md); the [thalassa](../../strongs/g/g2281.md) and the [salos](../../strongs/g/g4535.md) [ēcheō](../../strongs/g/g2278.md);**

<a name="luke_21_26"></a>Luke 21:26

**[anthrōpos](../../strongs/g/g444.md) [apopsychō](../../strongs/g/g674.md) them for [phobos](../../strongs/g/g5401.md), and [prosdokia](../../strongs/g/g4329.md) [eperchomai](../../strongs/g/g1904.md) the [oikoumenē](../../strongs/g/g3625.md): for the [dynamis](../../strongs/g/g1411.md) of [ouranos](../../strongs/g/g3772.md) shall be [saleuō](../../strongs/g/g4531.md).**

<a name="luke_21_27"></a>Luke 21:27

**And then shall [optanomai](../../strongs/g/g3700.md) the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [erchomai](../../strongs/g/g2064.md) in a [nephelē](../../strongs/g/g3507.md) with [dynamis](../../strongs/g/g1411.md) and [polys](../../strongs/g/g4183.md) [doxa](../../strongs/g/g1391.md).**

<a name="luke_21_28"></a>Luke 21:28

**And when these things [archomai](../../strongs/g/g756.md) [ginomai](../../strongs/g/g1096.md), [anakyptō](../../strongs/g/g352.md), and [epairō](../../strongs/g/g1869.md) your [kephalē](../../strongs/g/g2776.md); for your [apolytrōsis](../../strongs/g/g629.md) [eggizō](../../strongs/g/g1448.md).**

<a name="luke_21_29"></a>Luke 21:29

And he [eipon](../../strongs/g/g2036.md) to them a [parabolē](../../strongs/g/g3850.md); **[eidō](../../strongs/g/g1492.md) the [sykē](../../strongs/g/g4808.md), and all the [dendron](../../strongs/g/g1186.md);**

<a name="luke_21_30"></a>Luke 21:30

**When they now [proballō](../../strongs/g/g4261.md), ye [blepō](../../strongs/g/g991.md) and [ginōskō](../../strongs/g/g1097.md) of [heautou](../../strongs/g/g1438.md) that [theros](../../strongs/g/g2330.md) is now [eggys](../../strongs/g/g1451.md).**

<a name="luke_21_31"></a>Luke 21:31

**So likewise ye, when ye [eidō](../../strongs/g/g1492.md) these things [ginomai](../../strongs/g/g1096.md), [ginōskō](../../strongs/g/g1097.md) ye that the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) is [eggys](../../strongs/g/g1451.md).**

<a name="luke_21_32"></a>Luke 21:32

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, This [genea](../../strongs/g/g1074.md) shall not [parerchomai](../../strongs/g/g3928.md), till all [ginomai](../../strongs/g/g1096.md).**

<a name="luke_21_33"></a>Luke 21:33

**[ouranos](../../strongs/g/g3772.md) and [gē](../../strongs/g/g1093.md) shall [parerchomai](../../strongs/g/g3928.md): but my [logos](../../strongs/g/g3056.md) shall not [parerchomai](../../strongs/g/g3928.md).**

<a name="luke_21_34"></a>Luke 21:34

**And [prosechō](../../strongs/g/g4337.md) to yourselves, [mēpote](../../strongs/g/g3379.md) your [kardia](../../strongs/g/g2588.md) [barynō](../../strongs/g/g925.md) with [kraipalē](../../strongs/g/g2897.md), and [methē](../../strongs/g/g3178.md), and [merimna](../../strongs/g/g3308.md) [biōtikos](../../strongs/g/g982.md), and so that [hēmera](../../strongs/g/g2250.md) [ephistēmi](../../strongs/g/g2186.md) you [aiphnidios](../../strongs/g/g160.md).**

<a name="luke_21_35"></a>Luke 21:35

**For as a [pagis](../../strongs/g/g3803.md) shall it [eperchomai](../../strongs/g/g1904.md) on all [kathēmai](../../strongs/g/g2521.md) on the [prosōpon](../../strongs/g/g4383.md) of [pas](../../strongs/g/g3956.md) [gē](../../strongs/g/g1093.md).**

<a name="luke_21_36"></a>Luke 21:36

**[agrypneō](../../strongs/g/g69.md) therefore, and [deomai](../../strongs/g/g1189.md) [en](../../strongs/g/g1722.md) [pas](../../strongs/g/g3956.md) [kairos](../../strongs/g/g2540.md), that ye may be [kataxioō](../../strongs/g/g2661.md) to [ekpheugō](../../strongs/g/g1628.md) all these things that shall [ginomai](../../strongs/g/g1096.md), and to [histēmi](../../strongs/g/g2476.md) before the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md).**

<a name="luke_21_37"></a>Luke 21:37

And in the [hēmera](../../strongs/g/g2250.md) he was [didaskō](../../strongs/g/g1321.md) in the [hieron](../../strongs/g/g2411.md); and at [nyx](../../strongs/g/g3571.md) he [exerchomai](../../strongs/g/g1831.md), and [aulizomai](../../strongs/g/g835.md) in the [oros](../../strongs/g/g3735.md) that is [kaleō](../../strongs/g/g2564.md) [elaia](../../strongs/g/g1636.md).

<a name="luke_21_38"></a>Luke 21:38

And all the [laos](../../strongs/g/g2992.md) [orthrizō](../../strongs/g/g3719.md) to him in the [hieron](../../strongs/g/g2411.md), for to [akouō](../../strongs/g/g191.md) him.

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 20](luke_20.md) - [Luke 22](luke_22.md)