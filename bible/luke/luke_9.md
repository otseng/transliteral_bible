# [Luke 9](https://www.blueletterbible.org/kjv/luk/9/1/rl1/s_982001)

<a name="luke_9_1"></a>Luke 9:1

Then he [sygkaleō](../../strongs/g/g4779.md) his [dōdeka](../../strongs/g/g1427.md) [mathētēs](../../strongs/g/g3101.md), and [didōmi](../../strongs/g/g1325.md) them [dynamis](../../strongs/g/g1411.md) and [exousia](../../strongs/g/g1849.md) over all [daimonion](../../strongs/g/g1140.md), and to [therapeuō](../../strongs/g/g2323.md) [nosos](../../strongs/g/g3554.md).

<a name="luke_9_2"></a>Luke 9:2

And he [apostellō](../../strongs/g/g649.md) them to [kēryssō](../../strongs/g/g2784.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md), and to [iaomai](../../strongs/g/g2390.md) the [astheneō](../../strongs/g/g770.md).

<a name="luke_9_3"></a>Luke 9:3

And he [eipon](../../strongs/g/g2036.md) unto them, **[airō](../../strongs/g/g142.md) [mēdeis](../../strongs/g/g3367.md) for [hodos](../../strongs/g/g3598.md), neither [rhabdos](../../strongs/g/g4464.md), nor [pēra](../../strongs/g/g4082.md), neither [artos](../../strongs/g/g740.md), neither [argyrion](../../strongs/g/g694.md); neither have two [chitōn](../../strongs/g/g5509.md) apiece.**

<a name="luke_9_4"></a>Luke 9:4

**And whatsoever [oikia](../../strongs/g/g3614.md) ye [eiserchomai](../../strongs/g/g1525.md) into, there [menō](../../strongs/g/g3306.md), and thence [exerchomai](../../strongs/g/g1831.md).**

<a name="luke_9_5"></a>Luke 9:5

**And whosoever will not [dechomai](../../strongs/g/g1209.md) you, when ye [exerchomai](../../strongs/g/g1831.md) out of that [polis](../../strongs/g/g4172.md), [apotinassō](../../strongs/g/g660.md) the very [koniortos](../../strongs/g/g2868.md) from your [pous](../../strongs/g/g4228.md) for a [martyrion](../../strongs/g/g3142.md) against them.**

<a name="luke_9_6"></a>Luke 9:6

And they [exerchomai](../../strongs/g/g1831.md), and [dierchomai](../../strongs/g/g1330.md) through the [kōmē](../../strongs/g/g2968.md), [euaggelizō](../../strongs/g/g2097.md), and [therapeuō](../../strongs/g/g2323.md) [pantachou](../../strongs/g/g3837.md).

<a name="luke_9_7"></a>Luke 9:7

Now [Hērōdēs](../../strongs/g/g2264.md) the [tetraarchēs](../../strongs/g/g5076.md) [akouō](../../strongs/g/g191.md) of all that was [ginomai](../../strongs/g/g1096.md) by him: and he was [diaporeō](../../strongs/g/g1280.md), because that it was [legō](../../strongs/g/g3004.md) of some, that [Iōannēs](../../strongs/g/g2491.md) was [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md);

<a name="luke_9_8"></a>Luke 9:8

And of some, that [Ēlias](../../strongs/g/g2243.md) had [phainō](../../strongs/g/g5316.md); and of others, that one of the [archaios](../../strongs/g/g744.md) [prophētēs](../../strongs/g/g4396.md) was [anistēmi](../../strongs/g/g450.md).

<a name="luke_9_9"></a>Luke 9:9

And [Hērōdēs](../../strongs/g/g2264.md) [eipon](../../strongs/g/g2036.md), [Iōannēs](../../strongs/g/g2491.md) have I [apokephalizō](../../strongs/g/g607.md): but who is this, of whom I [akouō](../../strongs/g/g191.md) such things? And he [zēteō](../../strongs/g/g2212.md) to [eidō](../../strongs/g/g1492.md) him.

<a name="luke_9_10"></a>Luke 9:10

And the [apostolos](../../strongs/g/g652.md), when they were [hypostrephō](../../strongs/g/g5290.md), [diēgeomai](../../strongs/g/g1334.md) him all that they had [poieō](../../strongs/g/g4160.md). And he [paralambanō](../../strongs/g/g3880.md) them, and [hypochōreō](../../strongs/g/g5298.md) [idios](../../strongs/g/g2398.md) into an [erēmos](../../strongs/g/g2048.md) [topos](../../strongs/g/g5117.md) belonging to the [polis](../../strongs/g/g4172.md) [kaleō](../../strongs/g/g2564.md) [Bēthsaïda](../../strongs/g/g966.md).

<a name="luke_9_11"></a>Luke 9:11

And the [ochlos](../../strongs/g/g3793.md), when they [ginōskō](../../strongs/g/g1097.md) it, [akoloutheō](../../strongs/g/g190.md) him: and he [dechomai](../../strongs/g/g1209.md) them, and [laleō](../../strongs/g/g2980.md) unto them of the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md), and [iaomai](../../strongs/g/g2390.md) them that had [chreia](../../strongs/g/g5532.md) of [therapeia](../../strongs/g/g2322.md).

<a name="luke_9_12"></a>Luke 9:12

And when the [hēmera](../../strongs/g/g2250.md) [archomai](../../strongs/g/g756.md) to [klinō](../../strongs/g/g2827.md), then [proserchomai](../../strongs/g/g4334.md) the [dōdeka](../../strongs/g/g1427.md), and [eipon](../../strongs/g/g2036.md) unto him, [apolyō](../../strongs/g/g630.md) the [ochlos](../../strongs/g/g3793.md) away, that they may [aperchomai](../../strongs/g/g565.md) into the [kōmē](../../strongs/g/g2968.md) and [agros](../../strongs/g/g68.md) [kyklō](../../strongs/g/g2945.md), and [katalyō](../../strongs/g/g2647.md), and [heuriskō](../../strongs/g/g2147.md) [episitismos](../../strongs/g/g1979.md): for we are here in an [erēmos](../../strongs/g/g2048.md) [topos](../../strongs/g/g5117.md).

<a name="luke_9_13"></a>Luke 9:13

But he [eipon](../../strongs/g/g2036.md) unto them, **[didōmi](../../strongs/g/g1325.md) ye them to [phago](../../strongs/g/g5315.md).** And they [eipon](../../strongs/g/g2036.md), We have no more but five [artos](../../strongs/g/g740.md) and two [ichthys](../../strongs/g/g2486.md); except we should [poreuō](../../strongs/g/g4198.md) and [agorazō](../../strongs/g/g59.md) [brōma](../../strongs/g/g1033.md) for all this [laos](../../strongs/g/g2992.md).

<a name="luke_9_14"></a>Luke 9:14

For they were about five thousand [anēr](../../strongs/g/g435.md). And he [eipon](../../strongs/g/g2036.md) to his [mathētēs](../../strongs/g/g3101.md), **[kataklinō](../../strongs/g/g2625.md) them by fifties in a [klisia](../../strongs/g/g2828.md).**

<a name="luke_9_15"></a>Luke 9:15

And they [poieō](../../strongs/g/g4160.md) so, and made them all [anaklinō](../../strongs/g/g347.md).

<a name="luke_9_16"></a>Luke 9:16

Then he [lambanō](../../strongs/g/g2983.md) the five [artos](../../strongs/g/g740.md) and the two [ichthys](../../strongs/g/g2486.md), and [anablepō](../../strongs/g/g308.md) to [ouranos](../../strongs/g/g3772.md), he [eulogeō](../../strongs/g/g2127.md) them, and [kataklaō](../../strongs/g/g2622.md), and [didōmi](../../strongs/g/g1325.md) to the [mathētēs](../../strongs/g/g3101.md) to [paratithēmi](../../strongs/g/g3908.md) the [ochlos](../../strongs/g/g3793.md).

<a name="luke_9_17"></a>Luke 9:17

And they did [phago](../../strongs/g/g5315.md), and were all [chortazō](../../strongs/g/g5526.md): and there [airō](../../strongs/g/g142.md) of [klasma](../../strongs/g/g2801.md) that [perisseuō](../../strongs/g/g4052.md) to them [dōdeka](../../strongs/g/g1427.md) [kophinos](../../strongs/g/g2894.md).

<a name="luke_9_18"></a>Luke 9:18

And [ginomai](../../strongs/g/g1096.md), as he was [katamonas](../../strongs/g/g2651.md) [proseuchomai](../../strongs/g/g4336.md), his [mathētēs](../../strongs/g/g3101.md) were with him: and he [eperōtaō](../../strongs/g/g1905.md) them, [legō](../../strongs/g/g3004.md), **Whom [legō](../../strongs/g/g3004.md) the [ochlos](../../strongs/g/g3793.md) that I am?**

<a name="luke_9_19"></a>Luke 9:19

They [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md), [Iōannēs](../../strongs/g/g2491.md) the [baptistēs](../../strongs/g/g910.md); but some, [Ēlias](../../strongs/g/g2243.md); and others, that one of the [archaios](../../strongs/g/g744.md) [prophētēs](../../strongs/g/g4396.md) is [anistēmi](../../strongs/g/g450.md).

<a name="luke_9_20"></a>Luke 9:20

He [eipon](../../strongs/g/g2036.md) unto them, **But whom [legō](../../strongs/g/g3004.md) ye that I am?** [Petros](../../strongs/g/g4074.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md), The [Christos](../../strongs/g/g5547.md) of [theos](../../strongs/g/g2316.md).

<a name="luke_9_21"></a>Luke 9:21

And he [epitimaō](../../strongs/g/g2008.md) them, and [paraggellō](../../strongs/g/g3853.md) them to [eipon](../../strongs/g/g2036.md) [mēdeis](../../strongs/g/g3367.md) that thing;

<a name="luke_9_22"></a>Luke 9:22

[eipon](../../strongs/g/g2036.md), **The [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) must [paschō](../../strongs/g/g3958.md) [polys](../../strongs/g/g4183.md), and be [apodokimazō](../../strongs/g/g593.md) of the [presbyteros](../../strongs/g/g4245.md) and [archiereus](../../strongs/g/g749.md) and [grammateus](../../strongs/g/g1122.md), and be [apokteinō](../../strongs/g/g615.md), and be [egeirō](../../strongs/g/g1453.md) the third [hēmera](../../strongs/g/g2250.md).**

<a name="luke_9_23"></a>Luke 9:23

And he [legō](../../strongs/g/g3004.md) to all, **If any will [erchomai](../../strongs/g/g2064.md) after me, let him [aparneomai](../../strongs/g/g533.md) himself, and [airō](../../strongs/g/g142.md) his [stauros](../../strongs/g/g4716.md) [hēmera](../../strongs/g/g2250.md), and [akoloutheō](../../strongs/g/g190.md) me.**

<a name="luke_9_24"></a>Luke 9:24

**For whosoever will [sōzō](../../strongs/g/g4982.md) his [psychē](../../strongs/g/g5590.md) shall [apollymi](../../strongs/g/g622.md) it: but whosoever will [apollymi](../../strongs/g/g622.md) his [psychē](../../strongs/g/g5590.md) for my sake, the same shall [sōzō](../../strongs/g/g4982.md) it.**

<a name="luke_9_25"></a>Luke 9:25

**For what is an [anthrōpos](../../strongs/g/g444.md) [ōpheleō](../../strongs/g/g5623.md), if he [kerdainō](../../strongs/g/g2770.md) the [holos](../../strongs/g/g3650.md) [kosmos](../../strongs/g/g2889.md), and [apollymi](../../strongs/g/g622.md) himself, or be [zēmioō](../../strongs/g/g2210.md)?**

<a name="luke_9_26"></a>Luke 9:26

**For whosoever shall be [epaischynomai](../../strongs/g/g1870.md) of me and of my [logos](../../strongs/g/g3056.md), of him shall the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) be [epaischynomai](../../strongs/g/g1870.md), when he shall [erchomai](../../strongs/g/g2064.md) in his own [doxa](../../strongs/g/g1391.md), and in his [patēr](../../strongs/g/g3962.md), and of the [hagios](../../strongs/g/g40.md) [aggelos](../../strongs/g/g32.md).**

<a name="luke_9_27"></a>Luke 9:27

**But I [legō](../../strongs/g/g3004.md) you [alēthōs](../../strongs/g/g230.md), there be some [histēmi](../../strongs/g/g2476.md) here, which shall not [geuomai](../../strongs/g/g1089.md) of [thanatos](../../strongs/g/g2288.md), till they [eidō](../../strongs/g/g1492.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).**

<a name="luke_9_28"></a>Luke 9:28

And [ginomai](../../strongs/g/g1096.md) about an eight [hēmera](../../strongs/g/g2250.md) after these [logos](../../strongs/g/g3056.md), he [paralambanō](../../strongs/g/g3880.md) [Petros](../../strongs/g/g4074.md) and [Iōannēs](../../strongs/g/g2491.md) and [Iakōbos](../../strongs/g/g2385.md), and [anabainō](../../strongs/g/g305.md) into a [oros](../../strongs/g/g3735.md) to [proseuchomai](../../strongs/g/g4336.md).

<a name="luke_9_29"></a>Luke 9:29

And as he [proseuchomai](../../strongs/g/g4336.md), the [eidos](../../strongs/g/g1491.md) of his [prosōpon](../../strongs/g/g4383.md) was [heteros](../../strongs/g/g2087.md), and his [himatismos](../../strongs/g/g2441.md) was [leukos](../../strongs/g/g3022.md) and [exastraptō](../../strongs/g/g1823.md).

<a name="luke_9_30"></a>Luke 9:30

And, [idou](../../strongs/g/g2400.md), there [syllaleō](../../strongs/g/g4814.md) with him two [anēr](../../strongs/g/g435.md), which were [Mōÿsēs](../../strongs/g/g3475.md) and [Ēlias](../../strongs/g/g2243.md):

<a name="luke_9_31"></a>Luke 9:31

Who [optanomai](../../strongs/g/g3700.md) in [doxa](../../strongs/g/g1391.md), and [legō](../../strongs/g/g3004.md) of his [exodos](../../strongs/g/g1841.md) which he should [plēroō](../../strongs/g/g4137.md) at [Ierousalēm](../../strongs/g/g2419.md).

<a name="luke_9_32"></a>Luke 9:32

But [Petros](../../strongs/g/g4074.md) and they that were with him were [bareō](../../strongs/g/g916.md) with [hypnos](../../strongs/g/g5258.md): and when they were [diagrēgoreō](../../strongs/g/g1235.md), they [eidō](../../strongs/g/g1492.md) his [doxa](../../strongs/g/g1391.md), and the two [anēr](../../strongs/g/g435.md) that [synistēmi](../../strongs/g/g4921.md) with him.

<a name="luke_9_33"></a>Luke 9:33

And [ginomai](../../strongs/g/g1096.md), as they [diachōrizō](../../strongs/g/g1316.md) from him, [Petros](../../strongs/g/g4074.md) [eipon](../../strongs/g/g2036.md) unto [Iēsous](../../strongs/g/g2424.md), [epistatēs](../../strongs/g/g1988.md), it is [kalos](../../strongs/g/g2570.md) for us to be here: and let us [poieō](../../strongs/g/g4160.md) three [skēnē](../../strongs/g/g4633.md); one for thee, and one for [Mōÿsēs](../../strongs/g/g3475.md), and one for [Ēlias](../../strongs/g/g2243.md): not [eidō](../../strongs/g/g1492.md) what he [legō](../../strongs/g/g3004.md).

<a name="luke_9_34"></a>Luke 9:34

While he thus [legō](../../strongs/g/g3004.md), there [ginomai](../../strongs/g/g1096.md) a [nephelē](../../strongs/g/g3507.md), and [episkiazō](../../strongs/g/g1982.md) them: and they [phobeō](../../strongs/g/g5399.md) as they [eiserchomai](../../strongs/g/g1525.md) into the [nephelē](../../strongs/g/g3507.md).

<a name="luke_9_35"></a>Luke 9:35

And there came a [phōnē](../../strongs/g/g5456.md) out of the [nephelē](../../strongs/g/g3507.md), [legō](../../strongs/g/g3004.md), This is my [agapētos](../../strongs/g/g27.md) [huios](../../strongs/g/g5207.md): [akouō](../../strongs/g/g191.md) him.

<a name="luke_9_36"></a>Luke 9:36

And when the [phōnē](../../strongs/g/g5456.md) was past, [Iēsous](../../strongs/g/g2424.md) was [heuriskō](../../strongs/g/g2147.md) [monos](../../strongs/g/g3441.md). And they [sigaō](../../strongs/g/g4601.md), and [apaggellō](../../strongs/g/g518.md) no man in those [hēmera](../../strongs/g/g2250.md) any of those things which they had [horaō](../../strongs/g/g3708.md).

<a name="luke_9_37"></a>Luke 9:37

And [ginomai](../../strongs/g/g1096.md), that on the [hexēs](../../strongs/g/g1836.md) [hēmera](../../strongs/g/g2250.md), when they were [katerchomai](../../strongs/g/g2718.md) from the [oros](../../strongs/g/g3735.md), [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) [synantaō](../../strongs/g/g4876.md) him.

<a name="luke_9_38"></a>Luke 9:38

And, [idou](../../strongs/g/g2400.md), an [anēr](../../strongs/g/g435.md) of the [ochlos](../../strongs/g/g3793.md) [anaboaō](../../strongs/g/g310.md), [legō](../../strongs/g/g3004.md), [didaskalos](../../strongs/g/g1320.md), I [deomai](../../strongs/g/g1189.md) thee, [epiblepō](../../strongs/g/g1914.md) upon my [huios](../../strongs/g/g5207.md): for he is mine [monogenēs](../../strongs/g/g3439.md).

<a name="luke_9_39"></a>Luke 9:39

And, [idou](../../strongs/g/g2400.md), a [pneuma](../../strongs/g/g4151.md) [lambanō](../../strongs/g/g2983.md) him, and he [exaiphnēs](../../strongs/g/g1810.md) [krazō](../../strongs/g/g2896.md); and it [sparassō](../../strongs/g/g4682.md) him that he [aphros](../../strongs/g/g876.md) again, and [syntribō](../../strongs/g/g4937.md) him [mogis](../../strongs/g/g3425.md) [apochōreō](../../strongs/g/g672.md) from him.

<a name="luke_9_40"></a>Luke 9:40

And I [deomai](../../strongs/g/g1189.md) thy [mathētēs](../../strongs/g/g3101.md) to [ekballō](../../strongs/g/g1544.md) him out; and they [dynamai](../../strongs/g/g1410.md) not.

<a name="luke_9_41"></a>Luke 9:41

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md), **[ō](../../strongs/g/g5599.md) [apistos](../../strongs/g/g571.md) and [diastrephō](../../strongs/g/g1294.md) [genea](../../strongs/g/g1074.md), how long shall I be with you, and [anechō](../../strongs/g/g430.md) you? [prosagō](../../strongs/g/g4317.md) thy [huios](../../strongs/g/g5207.md) hither.**

<a name="luke_9_42"></a>Luke 9:42

And as he was yet [proserchomai](../../strongs/g/g4334.md), the [daimonion](../../strongs/g/g1140.md) [rhēgnymi](../../strongs/g/g4486.md) him, and [sysparassō](../../strongs/g/g4952.md). And [Iēsous](../../strongs/g/g2424.md) [epitimaō](../../strongs/g/g2008.md) the [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md), and [iaomai](../../strongs/g/g2390.md) the [pais](../../strongs/g/g3816.md), and [apodidōmi](../../strongs/g/g591.md) him to his [patēr](../../strongs/g/g3962.md).

<a name="luke_9_43"></a>Luke 9:43

And they were all [ekplēssō](../../strongs/g/g1605.md) at the [megaleiotēs](../../strongs/g/g3168.md) of [theos](../../strongs/g/g2316.md). But while they [thaumazō](../../strongs/g/g2296.md) every one at all things which [Iēsous](../../strongs/g/g2424.md) [poieō](../../strongs/g/g4160.md), he [eipon](../../strongs/g/g2036.md) unto his [mathētēs](../../strongs/g/g3101.md),

<a name="luke_9_44"></a>Luke 9:44

**Let these [logos](../../strongs/g/g3056.md) [tithēmi](../../strongs/g/g5087.md) into your [ous](../../strongs/g/g3775.md): for the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) shall be [paradidōmi](../../strongs/g/g3860.md) into the [cheir](../../strongs/g/g5495.md) of [anthrōpos](../../strongs/g/g444.md).**

<a name="luke_9_45"></a>Luke 9:45

But they [agnoeō](../../strongs/g/g50.md) this [rhēma](../../strongs/g/g4487.md), and it was [parakalyptō](../../strongs/g/g3871.md) from them, that they [aisthanomai](../../strongs/g/g143.md) it not: and they [phobeō](../../strongs/g/g5399.md) to [erōtaō](../../strongs/g/g2065.md) him of that [rhēma](../../strongs/g/g4487.md).

<a name="luke_9_46"></a>Luke 9:46

Then there [eiserchomai](../../strongs/g/g1525.md) a [dialogismos](../../strongs/g/g1261.md) among them, which of them should be [meizōn](../../strongs/g/g3187.md).

<a name="luke_9_47"></a>Luke 9:47

And [Iēsous](../../strongs/g/g2424.md), [eidō](../../strongs/g/g1492.md) the [dialogismos](../../strongs/g/g1261.md) of their [kardia](../../strongs/g/g2588.md), [epilambanomai](../../strongs/g/g1949.md) a [paidion](../../strongs/g/g3813.md), and [histēmi](../../strongs/g/g2476.md) him by him,

<a name="luke_9_48"></a>Luke 9:48

And [eipon](../../strongs/g/g2036.md) unto them, **Whosoever shall [dechomai](../../strongs/g/g1209.md) this [paidion](../../strongs/g/g3813.md) in my [onoma](../../strongs/g/g3686.md) [dechomai](../../strongs/g/g1209.md) me: and whosoever shall [dechomai](../../strongs/g/g1209.md) me [dechomai](../../strongs/g/g1209.md) him that [apostellō](../../strongs/g/g649.md) me: for he that [hyparchō](../../strongs/g/g5225.md) [mikros](../../strongs/g/g3398.md) among you all, the same shall be [megas](../../strongs/g/g3173.md).**

<a name="luke_9_49"></a>Luke 9:49

And [Iōannēs](../../strongs/g/g2491.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), [epistatēs](../../strongs/g/g1988.md), we [eidō](../../strongs/g/g1492.md) one [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md) in thy [onoma](../../strongs/g/g3686.md); and we [kōlyō](../../strongs/g/g2967.md) him, because he [akoloutheō](../../strongs/g/g190.md) not with us.

<a name="luke_9_50"></a>Luke 9:50

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **[kōlyō](../../strongs/g/g2967.md) not: for he that is not against us is for us.**

<a name="luke_9_51"></a>Luke 9:51

And [ginomai](../../strongs/g/g1096.md), when the [hēmera](../../strongs/g/g2250.md) was [symplēroō](../../strongs/g/g4845.md) that he should be [analēmpsis](../../strongs/g/g354.md), he [stērizō](../../strongs/g/g4741.md) his [prosōpon](../../strongs/g/g4383.md) to [poreuō](../../strongs/g/g4198.md) to [Ierousalēm](../../strongs/g/g2419.md),

<a name="luke_9_52"></a>Luke 9:52

And [apostellō](../../strongs/g/g649.md) [aggelos](../../strongs/g/g32.md) before his [prosōpon](../../strongs/g/g4383.md): and they [poreuō](../../strongs/g/g4198.md), and [eiserchomai](../../strongs/g/g1525.md) into a [kōmē](../../strongs/g/g2968.md) of the [Samaritēs](../../strongs/g/g4541.md), to [hetoimazō](../../strongs/g/g2090.md) for him.

<a name="luke_9_53"></a>Luke 9:53

And they did not [dechomai](../../strongs/g/g1209.md) him, because his [prosōpon](../../strongs/g/g4383.md) was [poreuō](../../strongs/g/g4198.md) to [Ierousalēm](../../strongs/g/g2419.md).

<a name="luke_9_54"></a>Luke 9:54

And when his [mathētēs](../../strongs/g/g3101.md) [Iakōbos](../../strongs/g/g2385.md) and [Iōannēs](../../strongs/g/g2491.md) [eidō](../../strongs/g/g1492.md), they [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), wilt thou that we [eipon](../../strongs/g/g2036.md) [pyr](../../strongs/g/g4442.md) to [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md), and [analiskō](../../strongs/g/g355.md) them, even as [Ēlias](../../strongs/g/g2243.md) [poieō](../../strongs/g/g4160.md)?

<a name="luke_9_55"></a>Luke 9:55

But he [strephō](../../strongs/g/g4762.md), and [epitimaō](../../strongs/g/g2008.md) them, and [eipon](../../strongs/g/g2036.md), **Ye [eidō](../../strongs/g/g1492.md) not [hoios](../../strongs/g/g3634.md) of [pneuma](../../strongs/g/g4151.md) ye are of.** [^1]

<a name="luke_9_56"></a>Luke 9:56

**For the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) is not [erchomai](../../strongs/g/g2064.md) to [apollymi](../../strongs/g/g622.md) [anthrōpos](../../strongs/g/g444.md) [psychē](../../strongs/g/g5590.md), but to [sōzō](../../strongs/g/g4982.md).** And they [poreuō](../../strongs/g/g4198.md) to another [kōmē](../../strongs/g/g2968.md).

<a name="luke_9_57"></a>Luke 9:57

And [ginomai](../../strongs/g/g1096.md), that, as they [poreuō](../../strongs/g/g4198.md) in the [hodos](../../strongs/g/g3598.md), a [tis](../../strongs/g/g5100.md) [eipon](../../strongs/g/g2036.md) unto him, [kyrios](../../strongs/g/g2962.md), I will [akoloutheō](../../strongs/g/g190.md) thee whithersoever thou [aperchomai](../../strongs/g/g565.md).

<a name="luke_9_58"></a>Luke 9:58

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **[alōpēx](../../strongs/g/g258.md) have [phōleos](../../strongs/g/g5454.md), and [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md) have [kataskēnōsis](../../strongs/g/g2682.md); but the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) hath not where to [klinō](../../strongs/g/g2827.md) [kephalē](../../strongs/g/g2776.md).**

<a name="luke_9_59"></a>Luke 9:59

And he [eipon](../../strongs/g/g2036.md) unto another, **[akoloutheō](../../strongs/g/g190.md) me.** But he [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), [epitrepō](../../strongs/g/g2010.md) me first to [aperchomai](../../strongs/g/g565.md) and [thaptō](../../strongs/g/g2290.md) my [patēr](../../strongs/g/g3962.md).

<a name="luke_9_60"></a>Luke 9:60

[Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **[aphiēmi](../../strongs/g/g863.md) the [nekros](../../strongs/g/g3498.md) [thaptō](../../strongs/g/g2290.md) their [nekros](../../strongs/g/g3498.md): but [aperchomai](../../strongs/g/g565.md) thou and [diaggellō](../../strongs/g/g1229.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).**

<a name="luke_9_61"></a>Luke 9:61

And another also [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), I will [akoloutheō](../../strongs/g/g190.md) thee; but [epitrepō](../../strongs/g/g2010.md) me [prōton](../../strongs/g/g4412.md) [apotassō](../../strongs/g/g657.md), which are [eis](../../strongs/g/g1519.md) at my [oikos](../../strongs/g/g3624.md).

<a name="luke_9_62"></a>Luke 9:62

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **[oudeis](../../strongs/g/g3762.md), having [epiballō](../../strongs/g/g1911.md) his [cheir](../../strongs/g/g5495.md) to the [arotron](../../strongs/g/g723.md), and [blepō](../../strongs/g/g991.md) back, is [euthetos](../../strongs/g/g2111.md) for the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).**

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 8](luke_8.md) - [Luke 10](luke_10.md)

---

[^1]: [Luke 9:55 Commentary](../../commentary/luke/luke_9_commentary.md#luke_9_55)
