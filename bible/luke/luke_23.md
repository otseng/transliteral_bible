# [Luke 23](https://www.blueletterbible.org/kjv/luk/23/1/rl1/s_996001)

<a name="luke_23_1"></a>Luke 23:1

And the [hapas](../../strongs/g/g537.md) [plēthos](../../strongs/g/g4128.md) of them [anistēmi](../../strongs/g/g450.md), and [agō](../../strongs/g/g71.md) him unto [Pilatos](../../strongs/g/g4091.md).

<a name="luke_23_2"></a>Luke 23:2

And they [archomai](../../strongs/g/g756.md) to [katēgoreō](../../strongs/g/g2723.md) him, [legō](../../strongs/g/g3004.md), We [heuriskō](../../strongs/g/g2147.md) this [diastrephō](../../strongs/g/g1294.md) the [ethnos](../../strongs/g/g1484.md), and [kōlyō](../../strongs/g/g2967.md) to [didōmi](../../strongs/g/g1325.md) [phoros](../../strongs/g/g5411.md) to [Kaisar](../../strongs/g/g2541.md), [legō](../../strongs/g/g3004.md) that he himself is [Christos](../../strongs/g/g5547.md) a [basileus](../../strongs/g/g935.md).

<a name="luke_23_3"></a>Luke 23:3

And [Pilatos](../../strongs/g/g4091.md) [eperōtaō](../../strongs/g/g1905.md) him, [legō](../../strongs/g/g3004.md), Art thou the [basileus](../../strongs/g/g935.md) of the [Ioudaios](../../strongs/g/g2453.md)? And he [apokrinomai](../../strongs/g/g611.md) him and [phēmi](../../strongs/g/g5346.md), **Thou [legō](../../strongs/g/g3004.md).**

<a name="luke_23_4"></a>Luke 23:4

Then [eipon](../../strongs/g/g2036.md) [Pilatos](../../strongs/g/g4091.md) to the [archiereus](../../strongs/g/g749.md) and to the [ochlos](../../strongs/g/g3793.md), I [heuriskō](../../strongs/g/g2147.md) no [aition](../../strongs/g/g158.md) in this [anthrōpos](../../strongs/g/g444.md).

<a name="luke_23_5"></a>Luke 23:5

And they were [epischyō](../../strongs/g/g2001.md), [legō](../../strongs/g/g3004.md), He [anaseiō](../../strongs/g/g383.md) the [laos](../../strongs/g/g2992.md), [didaskō](../../strongs/g/g1321.md) throughout all [Ioudaia](../../strongs/g/g2449.md), [archomai](../../strongs/g/g756.md) from [Galilaia](../../strongs/g/g1056.md) to [hōde](../../strongs/g/g5602.md).

<a name="luke_23_6"></a>Luke 23:6

When [Pilatos](../../strongs/g/g4091.md) [akouō](../../strongs/g/g191.md) of [Galilaia](../../strongs/g/g1056.md), he [eperōtaō](../../strongs/g/g1905.md) whether the [anthrōpos](../../strongs/g/g444.md) were a [Galilaios](../../strongs/g/g1057.md).

<a name="luke_23_7"></a>Luke 23:7

And as soon as he [epiginōskō](../../strongs/g/g1921.md) that he belonged unto [Hērōdēs](../../strongs/g/g2264.md) [exousia](../../strongs/g/g1849.md), he [anapempō](../../strongs/g/g375.md) him to [Hērōdēs](../../strongs/g/g2264.md), who himself also was at [Hierosolyma](../../strongs/g/g2414.md) at that [hēmera](../../strongs/g/g2250.md).

<a name="luke_23_8"></a>Luke 23:8

And when [Hērōdēs](../../strongs/g/g2264.md) [eidō](../../strongs/g/g1492.md) [Iēsous](../../strongs/g/g2424.md), he was [lian](../../strongs/g/g3029.md) [chairō](../../strongs/g/g5463.md): for he was [thelō](../../strongs/g/g2309.md) to [eidō](../../strongs/g/g1492.md) him of [hikanos](../../strongs/g/g2425.md), because he had [akouō](../../strongs/g/g191.md) [polys](../../strongs/g/g4183.md) of him; and he [elpizō](../../strongs/g/g1679.md) to have [eidō](../../strongs/g/g1492.md) some [sēmeion](../../strongs/g/g4592.md) [ginomai](../../strongs/g/g1096.md) by him.

<a name="luke_23_9"></a>Luke 23:9

Then he [eperōtaō](../../strongs/g/g1905.md) with him in [hikanos](../../strongs/g/g2425.md) [logos](../../strongs/g/g3056.md); but he [apokrinomai](../../strongs/g/g611.md) him [oudeis](../../strongs/g/g3762.md).

<a name="luke_23_10"></a>Luke 23:10

And the [archiereus](../../strongs/g/g749.md) and [grammateus](../../strongs/g/g1122.md) [histēmi](../../strongs/g/g2476.md) and [eutonōs](../../strongs/g/g2159.md) [katēgoreō](../../strongs/g/g2723.md) him.

<a name="luke_23_11"></a>Luke 23:11

And [Hērōdēs](../../strongs/g/g2264.md) with his [strateuma](../../strongs/g/g4753.md) [exoutheneō](../../strongs/g/g1848.md) him, and [empaizō](../../strongs/g/g1702.md), and [periballō](../../strongs/g/g4016.md) him in a [lampros](../../strongs/g/g2986.md) [esthēs](../../strongs/g/g2066.md), and [anapempō](../../strongs/g/g375.md) him to [Pilatos](../../strongs/g/g4091.md).

<a name="luke_23_12"></a>Luke 23:12

And the same [hēmera](../../strongs/g/g2250.md) [Pilatos](../../strongs/g/g4091.md) and [Hērōdēs](../../strongs/g/g2264.md) were [ginomai](../../strongs/g/g1096.md) [philos](../../strongs/g/g5384.md) [meta](../../strongs/g/g3326.md) [allēlōn](../../strongs/g/g240.md): for [prouparchō](../../strongs/g/g4391.md) they were at [echthra](../../strongs/g/g2189.md) between themselves.

<a name="luke_23_13"></a>Luke 23:13

And [Pilatos](../../strongs/g/g4091.md), when he had [sygkaleō](../../strongs/g/g4779.md) the [archiereus](../../strongs/g/g749.md) and the [archōn](../../strongs/g/g758.md) and the [laos](../../strongs/g/g2992.md),

<a name="luke_23_14"></a>Luke 23:14

[eipon](../../strongs/g/g2036.md) unto them, Ye have [prospherō](../../strongs/g/g4374.md) this [anthrōpos](../../strongs/g/g444.md) unto me, as one that [apostrephō](../../strongs/g/g654.md) the [laos](../../strongs/g/g2992.md): and, [idou](../../strongs/g/g2400.md), I, having [anakrinō](../../strongs/g/g350.md) before you, have [heuriskō](../../strongs/g/g2147.md) no [aition](../../strongs/g/g158.md) in this [anthrōpos](../../strongs/g/g444.md) touching those things whereof ye [katēgoreō](../../strongs/g/g2723.md) him:

<a name="luke_23_15"></a>Luke 23:15

No, nor yet [Hērōdēs](../../strongs/g/g2264.md): for I [anapempō](../../strongs/g/g375.md) you to him; and, [idou](../../strongs/g/g2400.md), nothing [axios](../../strongs/g/g514.md) of [thanatos](../../strongs/g/g2288.md) is [prassō](../../strongs/g/g4238.md) unto him.

<a name="luke_23_16"></a>Luke 23:16

I will therefore [paideuō](../../strongs/g/g3811.md) him, and [apolyō](../../strongs/g/g630.md).

<a name="luke_23_17"></a>Luke 23:17

(For of [anagkē](../../strongs/g/g318.md) he must [apolyō](../../strongs/g/g630.md) one unto them at the [heortē](../../strongs/g/g1859.md).) [^1]

<a name="luke_23_18"></a>Luke 23:18

And they [anakrazō](../../strongs/g/g349.md) [pamplēthei](../../strongs/g/g3826.md), [legō](../../strongs/g/g3004.md), [airō](../../strongs/g/g142.md) [touton](../../strongs/g/g5126.md), and [apolyō](../../strongs/g/g630.md) unto us [Barabbas](../../strongs/g/g912.md):

<a name="luke_23_19"></a>Luke 23:19

(Who for a certain [stasis](../../strongs/g/g4714.md) [ginomai](../../strongs/g/g1096.md) in the [polis](../../strongs/g/g4172.md), and for [phonos](../../strongs/g/g5408.md), was [ballō](../../strongs/g/g906.md) into [phylakē](../../strongs/g/g5438.md).)

<a name="luke_23_20"></a>Luke 23:20

[Pilatos](../../strongs/g/g4091.md) therefore, willing to [apolyō](../../strongs/g/g630.md) [Iēsous](../../strongs/g/g2424.md), [prosphōneō](../../strongs/g/g4377.md) again to them.

<a name="luke_23_21"></a>Luke 23:21

But they [epiphōneō](../../strongs/g/g2019.md), [legō](../../strongs/g/g3004.md), [stauroō](../../strongs/g/g4717.md), [stauroō](../../strongs/g/g4717.md) him.

<a name="luke_23_22"></a>Luke 23:22

And he [eipon](../../strongs/g/g2036.md) unto them the third time, Why, what [kakos](../../strongs/g/g2556.md) hath he [poieō](../../strongs/g/g4160.md)? I have [heuriskō](../../strongs/g/g2147.md) no [aition](../../strongs/g/g158.md) of [thanatos](../../strongs/g/g2288.md) in him: I will therefore [paideuō](../../strongs/g/g3811.md) him, and [apolyō](../../strongs/g/g630.md).

<a name="luke_23_23"></a>Luke 23:23

And they were [epikeimai](../../strongs/g/g1945.md) with [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [aiteō](../../strongs/g/g154.md) that he might be [stauroō](../../strongs/g/g4717.md). And the [phōnē](../../strongs/g/g5456.md) of them and of the [archiereus](../../strongs/g/g749.md) [katischyō](../../strongs/g/g2729.md).

<a name="luke_23_24"></a>Luke 23:24

And [Pilatos](../../strongs/g/g4091.md) [epikrinō](../../strongs/g/g1948.md) [ginomai](../../strongs/g/g1096.md) they [aitēma](../../strongs/g/g155.md).

<a name="luke_23_25"></a>Luke 23:25

And he [apolyō](../../strongs/g/g630.md) unto them him that for [stasis](../../strongs/g/g4714.md) and [phonos](../../strongs/g/g5408.md) was [ballō](../../strongs/g/g906.md) into [phylakē](../../strongs/g/g5438.md), whom they had [aiteō](../../strongs/g/g154.md); but he [paradidōmi](../../strongs/g/g3860.md) [Iēsous](../../strongs/g/g2424.md) to their [thelēma](../../strongs/g/g2307.md).

<a name="luke_23_26"></a>Luke 23:26

And as they [apagō](../../strongs/g/g520.md) him, they [epilambanomai](../../strongs/g/g1949.md) one [Simōn](../../strongs/g/g4613.md), a [Kyrēnaios](../../strongs/g/g2956.md), [erchomai](../../strongs/g/g2064.md) out of the [agros](../../strongs/g/g68.md), and on him they [epitithēmi](../../strongs/g/g2007.md) the [stauros](../../strongs/g/g4716.md), that he might [pherō](../../strongs/g/g5342.md) it after [Iēsous](../../strongs/g/g2424.md).

<a name="luke_23_27"></a>Luke 23:27

And there [akoloutheō](../../strongs/g/g190.md) him a [polys](../../strongs/g/g4183.md) [plēthos](../../strongs/g/g4128.md) of [laos](../../strongs/g/g2992.md), and of [gynē](../../strongs/g/g1135.md), which also [koptō](../../strongs/g/g2875.md) and [thrēneō](../../strongs/g/g2354.md) him.

<a name="luke_23_28"></a>Luke 23:28

But [Iēsous](../../strongs/g/g2424.md) [strephō](../../strongs/g/g4762.md) unto them [eipon](../../strongs/g/g2036.md), **[thygatēr](../../strongs/g/g2364.md) of [Ierousalēm](../../strongs/g/g2419.md), [klaiō](../../strongs/g/g2799.md) not for me, but [klaiō](../../strongs/g/g2799.md) for yourselves, and for your [teknon](../../strongs/g/g5043.md).**

<a name="luke_23_29"></a>Luke 23:29

**For, [idou](../../strongs/g/g2400.md), the [hēmera](../../strongs/g/g2250.md) are [erchomai](../../strongs/g/g2064.md), in the which they shall [eipon](../../strongs/g/g2046.md), [makarios](../../strongs/g/g3107.md) are the [steira](../../strongs/g/g4723.md), and the [koilia](../../strongs/g/g2836.md) that never [gennaō](../../strongs/g/g1080.md), and the [mastos](../../strongs/g/g3149.md) which never [thēlazō](../../strongs/g/g2337.md).**

<a name="luke_23_30"></a>Luke 23:30

**Then shall they [archomai](../../strongs/g/g756.md) to [legō](../../strongs/g/g3004.md) to the [oros](../../strongs/g/g3735.md), [piptō](../../strongs/g/g4098.md) on us; and to the [bounos](../../strongs/g/g1015.md), [kalyptō](../../strongs/g/g2572.md) us.**

<a name="luke_23_31"></a>Luke 23:31

**For if they [poieō](../../strongs/g/g4160.md) these things in a [hygros](../../strongs/g/g5200.md) [xylon](../../strongs/g/g3586.md), what shall be [ginomai](../../strongs/g/g1096.md) in the [xēros](../../strongs/g/g3584.md)?**

<a name="luke_23_32"></a>Luke 23:32

And there were also two other, [kakourgos](../../strongs/g/g2557.md), [agō](../../strongs/g/g71.md) with him to be [anaireō](../../strongs/g/g337.md).

<a name="luke_23_33"></a>Luke 23:33

And when they were [aperchomai](../../strongs/g/g565.md) to the [topos](../../strongs/g/g5117.md), which is [kaleō](../../strongs/g/g2564.md) [kranion](../../strongs/g/g2898.md), there they [stauroō](../../strongs/g/g4717.md) him, and the [kakourgos](../../strongs/g/g2557.md), one on the [dexios](../../strongs/g/g1188.md), and the other on the [aristeros](../../strongs/g/g710.md).

<a name="luke_23_34"></a>Luke 23:34

Then [legō](../../strongs/g/g3004.md) [Iēsous](../../strongs/g/g2424.md), **[patēr](../../strongs/g/g3962.md), [aphiēmi](../../strongs/g/g863.md) them; for they [eidō](../../strongs/g/g1492.md) not what they [poieō](../../strongs/g/g4160.md).** And they [diamerizō](../../strongs/g/g1266.md) his [himation](../../strongs/g/g2440.md), and [ballō](../../strongs/g/g906.md) [klēros](../../strongs/g/g2819.md).

<a name="luke_23_35"></a>Luke 23:35

And the [laos](../../strongs/g/g2992.md) [histēmi](../../strongs/g/g2476.md) [theōreō](../../strongs/g/g2334.md). And the [archōn](../../strongs/g/g758.md) also with them [ekmyktērizō](../../strongs/g/g1592.md), [legō](../../strongs/g/g3004.md), He [sōzō](../../strongs/g/g4982.md) others; let him [sōzō](../../strongs/g/g4982.md) himself, if he be [Christos](../../strongs/g/g5547.md), the [eklektos](../../strongs/g/g1588.md) of [theos](../../strongs/g/g2316.md).

<a name="luke_23_36"></a>Luke 23:36

And the [stratiōtēs](../../strongs/g/g4757.md) also [empaizō](../../strongs/g/g1702.md) him, [proserchomai](../../strongs/g/g4334.md) to him, and [prospherō](../../strongs/g/g4374.md) him [oxos](../../strongs/g/g3690.md),

<a name="luke_23_37"></a>Luke 23:37

And [legō](../../strongs/g/g3004.md), If thou be the [basileus](../../strongs/g/g935.md) of the [Ioudaios](../../strongs/g/g2453.md), [sōzō](../../strongs/g/g4982.md) thyself.

<a name="luke_23_38"></a>Luke 23:38

And an [epigraphē](../../strongs/g/g1923.md) also was [graphō](../../strongs/g/g1125.md) over him in [gramma](../../strongs/g/g1121.md) of [Hellēnikos](../../strongs/g/g1673.md), and [Rhōmaïkos](../../strongs/g/g4513.md), and [Hebraïkos](../../strongs/g/g1444.md), [houtos](../../strongs/g/g3778.md) [esti](../../strongs/g/g2076.md) [basileus](../../strongs/g/g935.md) [Ioudaios](../../strongs/g/g2453.md).

<a name="luke_23_39"></a>Luke 23:39

And one of the [kakourgos](../../strongs/g/g2557.md) which were [kremannymi](../../strongs/g/g2910.md) [blasphēmeō](../../strongs/g/g987.md) on him, [legō](../../strongs/g/g3004.md), If thou be [Christos](../../strongs/g/g5547.md), [sōzō](../../strongs/g/g4982.md) thyself and us.

<a name="luke_23_40"></a>Luke 23:40

But the other [apokrinomai](../../strongs/g/g611.md) [epitimaō](../../strongs/g/g2008.md) him, [legō](../../strongs/g/g3004.md), Dost not thou [phobeō](../../strongs/g/g5399.md) [theos](../../strongs/g/g2316.md), [hoti](../../strongs/g/g3754.md) thou art in the same [krima](../../strongs/g/g2917.md)?

<a name="luke_23_41"></a>Luke 23:41

And we indeed [dikaiōs](../../strongs/g/g1346.md); for we [apolambanō](../../strongs/g/g618.md) the [axios](../../strongs/g/g514.md) of our [prassō](../../strongs/g/g4238.md): but [houtos](../../strongs/g/g3778.md) hath [prassō](../../strongs/g/g4238.md) nothing [atopos](../../strongs/g/g824.md).

<a name="luke_23_42"></a>Luke 23:42

And he [legō](../../strongs/g/g3004.md) unto [Iēsous](../../strongs/g/g2424.md), [kyrios](../../strongs/g/g2962.md), [mnaomai](../../strongs/g/g3415.md) me when thou [erchomai](../../strongs/g/g2064.md) into thy [basileia](../../strongs/g/g932.md).

<a name="luke_23_43"></a>Luke 23:43

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto thee, [sēmeron](../../strongs/g/g4594.md) [esomai](../../strongs/g/g2071.md) with me in [paradeisos](../../strongs/g/g3857.md).**

<a name="luke_23_44"></a>Luke 23:44

And it was about the sixth [hōra](../../strongs/g/g5610.md), and there was a [skotos](../../strongs/g/g4655.md) over [holos](../../strongs/g/g3650.md) the [gē](../../strongs/g/g1093.md) until the ninth [hōra](../../strongs/g/g5610.md).

<a name="luke_23_45"></a>Luke 23:45

And the [hēlios](../../strongs/g/g2246.md) was [skotizō](../../strongs/g/g4654.md), and the [katapetasma](../../strongs/g/g2665.md) of the [naos](../../strongs/g/g3485.md) was [schizō](../../strongs/g/g4977.md) in the [mesos](../../strongs/g/g3319.md).

<a name="luke_23_46"></a>Luke 23:46

And when [Iēsous](../../strongs/g/g2424.md) had [phōneō](../../strongs/g/g5455.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), he [eipon](../../strongs/g/g2036.md), **[patēr](../../strongs/g/g3962.md), into thy [cheir](../../strongs/g/g5495.md) I [paratithēmi](../../strongs/g/g3908.md) my [pneuma](../../strongs/g/g4151.md):** and having [eipon](../../strongs/g/g2036.md) thus, he [ekpneō](../../strongs/g/g1606.md).

<a name="luke_23_47"></a>Luke 23:47

Now when the [hekatontarchēs](../../strongs/g/g1543.md) [eidō](../../strongs/g/g1492.md) what was [ginomai](../../strongs/g/g1096.md), he [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md), [legō](../../strongs/g/g3004.md), [ontōs](../../strongs/g/g3689.md) this was a [dikaios](../../strongs/g/g1342.md) [anthrōpos](../../strongs/g/g444.md).

<a name="luke_23_48"></a>Luke 23:48

And all the [ochlos](../../strongs/g/g3793.md) that [symparaginomai](../../strongs/g/g4836.md) to that [theōria](../../strongs/g/g2335.md), [theōreō](../../strongs/g/g2334.md) [ginomai](../../strongs/g/g1096.md), [typtō](../../strongs/g/g5180.md) their [stēthos](../../strongs/g/g4738.md), and [hypostrephō](../../strongs/g/g5290.md).

<a name="luke_23_49"></a>Luke 23:49

And all his [gnōstos](../../strongs/g/g1110.md), and the [gynē](../../strongs/g/g1135.md) that [synakoloutheō](../../strongs/g/g4870.md) him from [Galilaia](../../strongs/g/g1056.md), [histēmi](../../strongs/g/g2476.md) [makrothen](../../strongs/g/g3113.md), [horaō](../../strongs/g/g3708.md) these things.

<a name="luke_23_50"></a>Luke 23:50

And, [idou](../../strongs/g/g2400.md), an [anēr](../../strongs/g/g435.md) [onoma](../../strongs/g/g3686.md) [Iōsēph](../../strongs/g/g2501.md), a [bouleutēs](../../strongs/g/g1010.md); [hyparchō](../../strongs/g/g5225.md) [agathos](../../strongs/g/g18.md) [anēr](../../strongs/g/g435.md), and [dikaios](../../strongs/g/g1342.md):

<a name="luke_23_51"></a>Luke 23:51

(The same had not [sygkatatithēmi](../../strongs/g/g4784.md) to the [boulē](../../strongs/g/g1012.md) and [praxis](../../strongs/g/g4234.md) of them;) he was of [Harimathaia](../../strongs/g/g707.md), a [polis](../../strongs/g/g4172.md) of the [Ioudaios](../../strongs/g/g2453.md): who also himself [prosdechomai](../../strongs/g/g4327.md) for the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).

<a name="luke_23_52"></a>Luke 23:52

This [proserchomai](../../strongs/g/g4334.md) unto [Pilatos](../../strongs/g/g4091.md), and [aiteō](../../strongs/g/g154.md) the [sōma](../../strongs/g/g4983.md) of [Iēsous](../../strongs/g/g2424.md).

<a name="luke_23_53"></a>Luke 23:53

And he [kathaireō](../../strongs/g/g2507.md) it, and [entylissō](../../strongs/g/g1794.md) it in [sindōn](../../strongs/g/g4616.md), and [tithēmi](../../strongs/g/g5087.md) it in a [mnēma](../../strongs/g/g3418.md) that was [laxeutos](../../strongs/g/g2991.md), wherein [ou](../../strongs/g/g3756.md) [oudepō](../../strongs/g/g3764.md) [oudeis](../../strongs/g/g3762.md) was [keimai](../../strongs/g/g2749.md). [^3]

<a name="luke_23_54"></a>Luke 23:54

And that [hēmera](../../strongs/g/g2250.md) was the [paraskeuē](../../strongs/g/g3904.md), and the [sabbaton](../../strongs/g/g4521.md) [epiphōskō](../../strongs/g/g2020.md).

<a name="luke_23_55"></a>Luke 23:55

And the [gynē](../../strongs/g/g1135.md) also, which [synerchomai](../../strongs/g/g4905.md) with him from [Galilaia](../../strongs/g/g1056.md), [katakoloutheō](../../strongs/g/g2628.md), and [theaomai](../../strongs/g/g2300.md) the [mnēmeion](../../strongs/g/g3419.md), and how his [sōma](../../strongs/g/g4983.md) was [tithēmi](../../strongs/g/g5087.md).

<a name="luke_23_56"></a>Luke 23:56

And they [hypostrephō](../../strongs/g/g5290.md), and [hetoimazō](../../strongs/g/g2090.md) [arōma](../../strongs/g/g759.md) and [myron](../../strongs/g/g3464.md); and [hēsychazō](../../strongs/g/g2270.md) the [sabbaton](../../strongs/g/g4521.md) according to the [entolē](../../strongs/g/g1785.md).

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 22](luke_22.md) - [Luke 24](luke_24.md)

---

[^1]: [Luke 23:17 Commentary](../../commentary/luke/luke_23_commentary.md#luke_23_17)

[^2]: [Luke 23:34 Commentary](../../commentary/luke/luke_23_commentary.md#luke_23_34)

[^3]: [Luke 23:53 Commentary](../../commentary/luke/luke_23_commentary.md#luke_23_53)
