# [Luke 22](https://www.blueletterbible.org/kjv/luk/22/66/s_995001)

<a name="luke_22_1"></a>Luke 22:1

Now the [heortē](../../strongs/g/g1859.md) of [azymos](../../strongs/g/g106.md) [eggizō](../../strongs/g/g1448.md), which is [legō](../../strongs/g/g3004.md) [pascha](../../strongs/g/g3957.md).

<a name="luke_22_2"></a>Luke 22:2

And the [archiereus](../../strongs/g/g749.md) and [grammateus](../../strongs/g/g1122.md) [zēteō](../../strongs/g/g2212.md) how they might [anaireō](../../strongs/g/g337.md) him; for they [phobeō](../../strongs/g/g5399.md) the [laos](../../strongs/g/g2992.md).

<a name="luke_22_3"></a>Luke 22:3

Then [eiserchomai](../../strongs/g/g1525.md) [Satanas](../../strongs/g/g4567.md) into [Ioudas](../../strongs/g/g2455.md) [epikaleō](../../strongs/g/g1941.md) [Iskariōth](../../strongs/g/g2469.md), being of the [arithmos](../../strongs/g/g706.md) of the [dōdeka](../../strongs/g/g1427.md).

<a name="luke_22_4"></a>Luke 22:4

And he went his [aperchomai](../../strongs/g/g565.md), and [syllaleō](../../strongs/g/g4814.md) with the [archiereus](../../strongs/g/g749.md) and [stratēgos](../../strongs/g/g4755.md), how he might [paradidōmi](../../strongs/g/g3860.md) him unto them.

<a name="luke_22_5"></a>Luke 22:5

And they were [chairō](../../strongs/g/g5463.md), and [syntithēmi](../../strongs/g/g4934.md) to [didōmi](../../strongs/g/g1325.md) him [argyrion](../../strongs/g/g694.md).

<a name="luke_22_6"></a>Luke 22:6

And he [exomologeō](../../strongs/g/g1843.md), and [zēteō](../../strongs/g/g2212.md) [eukairia](../../strongs/g/g2120.md) to [paradidōmi](../../strongs/g/g3860.md) him unto them [ater](../../strongs/g/g817.md) [ochlos](../../strongs/g/g3793.md).

<a name="luke_22_7"></a>Luke 22:7

Then [erchomai](../../strongs/g/g2064.md) the [hēmera](../../strongs/g/g2250.md) of [azymos](../../strongs/g/g106.md), when the [pascha](../../strongs/g/g3957.md) must be [thyō](../../strongs/g/g2380.md).

<a name="luke_22_8"></a>Luke 22:8

And he [apostellō](../../strongs/g/g649.md) [Petros](../../strongs/g/g4074.md) and [Iōannēs](../../strongs/g/g2491.md), [eipon](../../strongs/g/g2036.md), **[poreuō](../../strongs/g/g4198.md) and [hetoimazō](../../strongs/g/g2090.md) us the [pascha](../../strongs/g/g3957.md), that we may [phago](../../strongs/g/g5315.md).**

<a name="luke_22_9"></a>Luke 22:9

And they [eipon](../../strongs/g/g2036.md) unto him, Where wilt thou [hetoimazō](../../strongs/g/g2090.md)?

<a name="luke_22_10"></a>Luke 22:10

And he [eipon](../../strongs/g/g2036.md) unto them, **[idou](../../strongs/g/g2400.md), when ye [eiserchomai](../../strongs/g/g1525.md) into the [polis](../../strongs/g/g4172.md), there shall an [anthrōpos](../../strongs/g/g444.md) [synantaō](../../strongs/g/g4876.md) you, [bastazō](../../strongs/g/g941.md) a [keramion](../../strongs/g/g2765.md) of [hydōr](../../strongs/g/g5204.md); [akoloutheō](../../strongs/g/g190.md) him into the [oikia](../../strongs/g/g3614.md) where he [eisporeuomai](../../strongs/g/g1531.md).**

<a name="luke_22_11"></a>Luke 22:11

**And ye shall [eipon](../../strongs/g/g2046.md) unto the [oikodespotēs](../../strongs/g/g3617.md) of the [oikia](../../strongs/g/g3614.md), The [didaskalos](../../strongs/g/g1320.md) [legō](../../strongs/g/g3004.md) unto thee, Where is the [katalyma](../../strongs/g/g2646.md), where I shall [phago](../../strongs/g/g5315.md) the [pascha](../../strongs/g/g3957.md) with my [mathētēs](../../strongs/g/g3101.md)?**

<a name="luke_22_12"></a>Luke 22:12

**And he shall [deiknyō](../../strongs/g/g1166.md) you a [megas](../../strongs/g/g3173.md) [anōgeon](../../strongs/g/g508.md) [strōnnyō](../../strongs/g/g4766.md): there [hetoimazō](../../strongs/g/g2090.md).**

<a name="luke_22_13"></a>Luke 22:13

And they [aperchomai](../../strongs/g/g565.md), and [heuriskō](../../strongs/g/g2147.md) as he had [eipon](../../strongs/g/g2046.md) unto them: and they [hetoimazō](../../strongs/g/g2090.md) the [pascha](../../strongs/g/g3957.md).

<a name="luke_22_14"></a>Luke 22:14

And when the [hōra](../../strongs/g/g5610.md) was [ginomai](../../strongs/g/g1096.md), he [anapiptō](../../strongs/g/g377.md), and the [dōdeka](../../strongs/g/g1427.md) [apostolos](../../strongs/g/g652.md) with him.

<a name="luke_22_15"></a>Luke 22:15

And he [eipon](../../strongs/g/g2036.md) unto them, **[epithymia](../../strongs/g/g1939.md) I have [epithymeō](../../strongs/g/g1937.md) to [phago](../../strongs/g/g5315.md) this [pascha](../../strongs/g/g3957.md) with you before I [paschō](../../strongs/g/g3958.md):**

<a name="luke_22_16"></a>Luke 22:16

**For I [legō](../../strongs/g/g3004.md) unto you, I will not any more [phago](../../strongs/g/g5315.md) thereof, until [plēroō](../../strongs/g/g4137.md) in the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).**

<a name="luke_22_17"></a>Luke 22:17

And he [dechomai](../../strongs/g/g1209.md) the [potērion](../../strongs/g/g4221.md), [eucharisteō](../../strongs/g/g2168.md), and [eipon](../../strongs/g/g2036.md), **[lambanō](../../strongs/g/g2983.md) this, and [diamerizō](../../strongs/g/g1266.md) among yourselves:**

<a name="luke_22_18"></a>Luke 22:18

**For I [legō](../../strongs/g/g3004.md) unto you, I will not [pinō](../../strongs/g/g4095.md) of the [gennēma](../../strongs/g/g1081.md) of the [ampelos](../../strongs/g/g288.md), until the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) shall [erchomai](../../strongs/g/g2064.md).**

<a name="luke_22_19"></a>Luke 22:19

And he [lambanō](../../strongs/g/g2983.md) [artos](../../strongs/g/g740.md), [eucharisteō](../../strongs/g/g2168.md), and [klaō](../../strongs/g/g2806.md), and [didōmi](../../strongs/g/g1325.md) unto them, [legō](../../strongs/g/g3004.md), **This is my [sōma](../../strongs/g/g4983.md) which is [didōmi](../../strongs/g/g1325.md) for you: this [poieō](../../strongs/g/g4160.md) [anamnēsis](../../strongs/g/g364.md) of me.**

<a name="luke_22_20"></a>Luke 22:20

[hōsautōs](../../strongs/g/g5615.md) also the [potērion](../../strongs/g/g4221.md) after [deipneō](../../strongs/g/g1172.md), [legō](../../strongs/g/g3004.md), **This [potērion](../../strongs/g/g4221.md) the [kainos](../../strongs/g/g2537.md) [diathēkē](../../strongs/g/g1242.md) in my [haima](../../strongs/g/g129.md), which is [ekcheō](../../strongs/g/g1632.md) for you.**

<a name="luke_22_21"></a>Luke 22:21

**But, [idou](../../strongs/g/g2400.md), the [cheir](../../strongs/g/g5495.md) of [paradidōmi](../../strongs/g/g3860.md) me with me on the [trapeza](../../strongs/g/g5132.md).**

<a name="luke_22_22"></a>Luke 22:22

**And [men](../../strongs/g/g3303.md) the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [poreuō](../../strongs/g/g4198.md), as it was [horizō](../../strongs/g/g3724.md): but [ouai](../../strongs/g/g3759.md) unto that [anthrōpos](../../strongs/g/g444.md) by whom [paradidōmi](../../strongs/g/g3860.md)!**

<a name="luke_22_23"></a>Luke 22:23

And they [archomai](../../strongs/g/g756.md) to [syzēteō](../../strongs/g/g4802.md) among themselves, which of them it was that should [prassō](../../strongs/g/g4238.md) this thing.

<a name="luke_22_24"></a>Luke 22:24

And there was also a [philoneikia](../../strongs/g/g5379.md) among them, which of them should be [dokeō](../../strongs/g/g1380.md) the [meizōn](../../strongs/g/g3187.md).

<a name="luke_22_25"></a>Luke 22:25

And he [eipon](../../strongs/g/g2036.md) unto them, **The [basileus](../../strongs/g/g935.md) of the [ethnos](../../strongs/g/g1484.md) [kyrieuō](../../strongs/g/g2961.md) them; and [exousiazō](../../strongs/g/g1850.md) them are [kaleō](../../strongs/g/g2564.md) [euergetēs](../../strongs/g/g2110.md).**

<a name="luke_22_26"></a>Luke 22:26

**But ye not so: but [meizōn](../../strongs/g/g3187.md) among you, let him be as [neos](../../strongs/g/g3501.md); and [hēgeomai](../../strongs/g/g2233.md), as [diakoneō](../../strongs/g/g1247.md).**

<a name="luke_22_27"></a>Luke 22:27

**For whether [meizōn](../../strongs/g/g3187.md), [anakeimai](../../strongs/g/g345.md), or [diakoneō](../../strongs/g/g1247.md)? not [anakeimai](../../strongs/g/g345.md)? but I am among you as [diakoneō](../../strongs/g/g1247.md).**

<a name="luke_22_28"></a>Luke 22:28

**Ye are [diamenō](../../strongs/g/g1265.md) with me in my [peirasmos](../../strongs/g/g3986.md).**

<a name="luke_22_29"></a>Luke 22:29

**And I [diatithēmi](../../strongs/g/g1303.md) unto you a [basileia](../../strongs/g/g932.md), as my [patēr](../../strongs/g/g3962.md) hath [diatithēmi](../../strongs/g/g1303.md) unto me;**

<a name="luke_22_30"></a>Luke 22:30

**That ye may [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md) at my [trapeza](../../strongs/g/g5132.md) in my [basileia](../../strongs/g/g932.md), and [kathizō](../../strongs/g/g2523.md) on [thronos](../../strongs/g/g2362.md) [krinō](../../strongs/g/g2919.md) the twelve [phylē](../../strongs/g/g5443.md) of [Israēl](../../strongs/g/g2474.md).**

<a name="luke_22_31"></a>Luke 22:31

And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md), **[Simōn](../../strongs/g/g4613.md), [Simōn](../../strongs/g/g4613.md), [idou](../../strongs/g/g2400.md), [Satanas](../../strongs/g/g4567.md) hath [exaiteō](../../strongs/g/g1809.md) you, that he may [siniazō](../../strongs/g/g4617.md) you as [sitos](../../strongs/g/g4621.md):**

<a name="luke_22_32"></a>Luke 22:32

**But I have [deomai](../../strongs/g/g1189.md) for thee, that thy [pistis](../../strongs/g/g4102.md) [ekleipō](../../strongs/g/g1587.md) not: and [pote](../../strongs/g/g4218.md) thou [epistrephō](../../strongs/g/g1994.md), [stērizō](../../strongs/g/g4741.md) thy [adelphos](../../strongs/g/g80.md).**

<a name="luke_22_33"></a>Luke 22:33

And he [eipon](../../strongs/g/g2036.md) unto him, [kyrios](../../strongs/g/g2962.md), I am [hetoimos](../../strongs/g/g2092.md) to [poreuō](../../strongs/g/g4198.md) with thee, both into [phylakē](../../strongs/g/g5438.md), and to [thanatos](../../strongs/g/g2288.md).

<a name="luke_22_34"></a>Luke 22:34

And he [eipon](../../strongs/g/g2036.md), **I [legō](../../strongs/g/g3004.md) thee, [Petros](../../strongs/g/g4074.md), the [alektōr](../../strongs/g/g220.md) shall not [phōneō](../../strongs/g/g5455.md) this [sēmeron](../../strongs/g/g4594.md), before that thou shalt thrice [aparneomai](../../strongs/g/g533.md) that thou [eidō](../../strongs/g/g1492.md) me.**

<a name="luke_22_35"></a>Luke 22:35

And he [eipon](../../strongs/g/g2036.md) unto them, **When I [apostellō](../../strongs/g/g649.md) you without [ballantion](../../strongs/g/g905.md), and [pēra](../../strongs/g/g4082.md), and [hypodēma](../../strongs/g/g5266.md), [hystereō](../../strongs/g/g5302.md) any thing? And they [eipon](../../strongs/g/g2036.md), [oudeis](../../strongs/g/g3762.md).**

<a name="luke_22_36"></a>Luke 22:36

Then [eipon](../../strongs/g/g2036.md) he unto them, **But now, he that hath a [ballantion](../../strongs/g/g905.md), [airō](../../strongs/g/g142.md), and [homoiōs](../../strongs/g/g3668.md) [pēra](../../strongs/g/g4082.md): and he that hath no [machaira](../../strongs/g/g3162.md), let him [pōleō](../../strongs/g/g4453.md) his [himation](../../strongs/g/g2440.md), and [agorazō](../../strongs/g/g59.md).**

<a name="luke_22_37"></a>Luke 22:37

**For I [legō](../../strongs/g/g3004.md) unto you, that this [graphō](../../strongs/g/g1125.md) must yet [teleō](../../strongs/g/g5055.md) in me, And he was [logizomai](../../strongs/g/g3049.md) among the [anomos](../../strongs/g/g459.md): for [peri](../../strongs/g/g4012.md) me have [telos](../../strongs/g/g5056.md).**

<a name="luke_22_38"></a>Luke 22:38

And they [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), [idou](../../strongs/g/g2400.md), here are two [machaira](../../strongs/g/g3162.md). And he [eipon](../../strongs/g/g2036.md) unto them, **It is [hikanos](../../strongs/g/g2425.md).**

<a name="luke_22_39"></a>Luke 22:39

And he [exerchomai](../../strongs/g/g1831.md), and [poreuō](../../strongs/g/g4198.md), as he was [ethos](../../strongs/g/g1485.md), to the [oros](../../strongs/g/g3735.md) of [elaia](../../strongs/g/g1636.md); and his [mathētēs](../../strongs/g/g3101.md) also [akoloutheō](../../strongs/g/g190.md) him.

<a name="luke_22_40"></a>Luke 22:40

And when he was at [topos](../../strongs/g/g5117.md), he [eipon](../../strongs/g/g2036.md) unto them, **[proseuchomai](../../strongs/g/g4336.md) that ye [eiserchomai](../../strongs/g/g1525.md) not into [peirasmos](../../strongs/g/g3986.md).**

<a name="luke_22_41"></a>Luke 22:41

And he was [apospaō](../../strongs/g/g645.md) from them about a [lithos](../../strongs/g/g3037.md) [bolē](../../strongs/g/g1000.md), and [tithēmi](../../strongs/g/g5087.md) [gony](../../strongs/g/g1119.md), and [proseuchomai](../../strongs/g/g4336.md),

<a name="luke_22_42"></a>Luke 22:42

[legō](../../strongs/g/g3004.md), **[patēr](../../strongs/g/g3962.md), if [boulomai](../../strongs/g/g1014.md), [parapherō](../../strongs/g/g3911.md) this [potērion](../../strongs/g/g4221.md) from me: nevertheless not my [thelēma](../../strongs/g/g2307.md), but thine, be done.**

<a name="luke_22_43"></a>Luke 22:43

And there [optanomai](../../strongs/g/g3700.md) an [aggelos](../../strongs/g/g32.md) unto him from [ouranos](../../strongs/g/g3772.md), [enischyō](../../strongs/g/g1765.md) him. [^1]

<a name="luke_22_44"></a>Luke 22:44

And being in [agōnia](../../strongs/g/g74.md) he [proseuchomai](../../strongs/g/g4336.md) [ektenesteron](../../strongs/g/g1617.md): and his [hidrōs](../../strongs/g/g2402.md) was as it were [thrombos](../../strongs/g/g2361.md) of [haima](../../strongs/g/g129.md) [katabainō](../../strongs/g/g2597.md) to [gē](../../strongs/g/g1093.md).

<a name="luke_22_45"></a>Luke 22:45

And when he [anistēmi](../../strongs/g/g450.md) from [proseuchē](../../strongs/g/g4335.md), and was [erchomai](../../strongs/g/g2064.md) to his [mathētēs](../../strongs/g/g3101.md), he [heuriskō](../../strongs/g/g2147.md) them [koimaō](../../strongs/g/g2837.md) for [lypē](../../strongs/g/g3077.md),

<a name="luke_22_46"></a>Luke 22:46

And [eipon](../../strongs/g/g2036.md) unto them, **Why [katheudō](../../strongs/g/g2518.md) ye? [anistēmi](../../strongs/g/g450.md) and [proseuchomai](../../strongs/g/g4336.md), lest ye [eiserchomai](../../strongs/g/g1525.md) into [peirasmos](../../strongs/g/g3986.md).**

<a name="luke_22_47"></a>Luke 22:47

And while he yet [laleō](../../strongs/g/g2980.md), [idou](../../strongs/g/g2400.md) [ochlos](../../strongs/g/g3793.md), and he that was [legō](../../strongs/g/g3004.md) [Ioudas](../../strongs/g/g2455.md), one of the [dōdeka](../../strongs/g/g1427.md), [proerchomai](../../strongs/g/g4281.md) them, and [eggizō](../../strongs/g/g1448.md) unto [Iēsous](../../strongs/g/g2424.md) to [phileō](../../strongs/g/g5368.md) him.

<a name="luke_22_48"></a>Luke 22:48

But [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **[Ioudas](../../strongs/g/g2455.md), [paradidōmi](../../strongs/g/g3860.md) thou the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) with [philēma](../../strongs/g/g5370.md)?**

<a name="luke_22_49"></a>Luke 22:49

When they which were about him [eidō](../../strongs/g/g1492.md) [esomai](../../strongs/g/g2071.md), they [eipon](../../strongs/g/g2036.md) unto him, [kyrios](../../strongs/g/g2962.md), shall we [patassō](../../strongs/g/g3960.md) with [machaira](../../strongs/g/g3162.md)?

<a name="luke_22_50"></a>Luke 22:50

And one of them [patassō](../../strongs/g/g3960.md) the [doulos](../../strongs/g/g1401.md) of the [archiereus](../../strongs/g/g749.md), and [aphaireō](../../strongs/g/g851.md) his [dexios](../../strongs/g/g1188.md) [ous](../../strongs/g/g3775.md).

<a name="luke_22_51"></a>Luke 22:51

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), **[eaō](../../strongs/g/g1439.md) [toutou](../../strongs/g/g5127.md) [heōs](../../strongs/g/g2193.md).** And he [haptomai](../../strongs/g/g680.md) his [ōtion](../../strongs/g/g5621.md), and [iaomai](../../strongs/g/g2390.md) him.

<a name="luke_22_52"></a>Luke 22:52

Then [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto the [archiereus](../../strongs/g/g749.md), and [stratēgos](../../strongs/g/g4755.md) of the [hieron](../../strongs/g/g2411.md), and the [presbyteros](../../strongs/g/g4245.md), which were [paraginomai](../../strongs/g/g3854.md) to him, **[exerchomai](../../strongs/g/g1831.md), as against a [lēstēs](../../strongs/g/g3027.md), with [machaira](../../strongs/g/g3162.md) and [xylon](../../strongs/g/g3586.md)?**

<a name="luke_22_53"></a>Luke 22:53

**When I was [hēmera](../../strongs/g/g2250.md) with you in the [hieron](../../strongs/g/g2411.md), ye [ekteinō](../../strongs/g/g1614.md) no [cheir](../../strongs/g/g5495.md) against me: but this is your [hōra](../../strongs/g/g5610.md), and the [exousia](../../strongs/g/g1849.md) of [skotos](../../strongs/g/g4655.md).**

<a name="luke_22_54"></a>Luke 22:54

Then [syllambanō](../../strongs/g/g4815.md) him, [agō](../../strongs/g/g71.md), and [eisagō](../../strongs/g/g1521.md) him into the [archiereus](../../strongs/g/g749.md) [oikos](../../strongs/g/g3624.md). And [Petros](../../strongs/g/g4074.md) [akoloutheō](../../strongs/g/g190.md) [makrothen](../../strongs/g/g3113.md).

<a name="luke_22_55"></a>Luke 22:55

And when they had [haptō](../../strongs/g/g681.md) a [pyr](../../strongs/g/g4442.md) in the midst of the [aulē](../../strongs/g/g833.md), and were [sygkathizō](../../strongs/g/g4776.md), [Petros](../../strongs/g/g4074.md) [kathēmai](../../strongs/g/g2521.md) among them.

<a name="luke_22_56"></a>Luke 22:56

But a certain [paidiskē](../../strongs/g/g3814.md) [eidō](../../strongs/g/g1492.md) him as he [kathēmai](../../strongs/g/g2521.md) by the [phōs](../../strongs/g/g5457.md), and [atenizō](../../strongs/g/g816.md) upon him, and [eipon](../../strongs/g/g2036.md), [houtos](../../strongs/g/g3778.md) was also with him.

<a name="luke_22_57"></a>Luke 22:57

And he [arneomai](../../strongs/g/g720.md) him, [legō](../../strongs/g/g3004.md), [gynē](../../strongs/g/g1135.md), I [eidō](../../strongs/g/g1492.md) him not.

<a name="luke_22_58"></a>Luke 22:58

And after [brachys](../../strongs/g/g1024.md) another [eidō](../../strongs/g/g1492.md) him, and [phēmi](../../strongs/g/g5346.md), Thou art also of them. And [Petros](../../strongs/g/g4074.md) [eipon](../../strongs/g/g2036.md), [anthrōpos](../../strongs/g/g444.md), I am not.

<a name="luke_22_59"></a>Luke 22:59

And about [diïstēmi](../../strongs/g/g1339.md) of one [hōra](../../strongs/g/g5610.md) after another [diïschyrizomai](../../strongs/g/g1340.md), [legō](../../strongs/g/g3004.md), Of [alētheia](../../strongs/g/g225.md) this also was with him: for he is a [Galilaios](../../strongs/g/g1057.md).

<a name="luke_22_60"></a>Luke 22:60

And [Petros](../../strongs/g/g4074.md) [eipon](../../strongs/g/g2036.md), [anthrōpos](../../strongs/g/g444.md), I [eidō](../../strongs/g/g1492.md) not what thou [legō](../../strongs/g/g3004.md). And [parachrēma](../../strongs/g/g3916.md), while he yet [laleō](../../strongs/g/g2980.md), the [alektōr](../../strongs/g/g220.md) [phōneō](../../strongs/g/g5455.md).

<a name="luke_22_61"></a>Luke 22:61

And the [kyrios](../../strongs/g/g2962.md) [strephō](../../strongs/g/g4762.md), and [emblepō](../../strongs/g/g1689.md) [Petros](../../strongs/g/g4074.md). And [Petros](../../strongs/g/g4074.md) [hypomimnēskō](../../strongs/g/g5279.md) the [logos](../../strongs/g/g3056.md) of the [kyrios](../../strongs/g/g2962.md), how he had [eipon](../../strongs/g/g2036.md) unto him, **Before the [alektōr](../../strongs/g/g220.md) [phōneō](../../strongs/g/g5455.md), thou shalt [aparneomai](../../strongs/g/g533.md) me thrice.**

<a name="luke_22_62"></a>Luke 22:62

And [Petros](../../strongs/g/g4074.md) [exerchomai](../../strongs/g/g1831.md) out, and [klaiō](../../strongs/g/g2799.md) [pikrōs](../../strongs/g/g4090.md).

<a name="luke_22_63"></a>Luke 22:63

And the [anēr](../../strongs/g/g435.md) that [synechō](../../strongs/g/g4912.md) [Iēsous](../../strongs/g/g2424.md) [empaizō](../../strongs/g/g1702.md) him, and [derō](../../strongs/g/g1194.md) him.

<a name="luke_22_64"></a>Luke 22:64

And when they had [perikalyptō](../../strongs/g/g4028.md) him, they [typtō](../../strongs/g/g5180.md) him on the [prosōpon](../../strongs/g/g4383.md), and [eperōtaō](../../strongs/g/g1905.md) him, [legō](../../strongs/g/g3004.md), [prophēteuō](../../strongs/g/g4395.md), who is it that [paiō](../../strongs/g/g3817.md) thee?

<a name="luke_22_65"></a>Luke 22:65

And [polys](../../strongs/g/g4183.md) [heteros](../../strongs/g/g2087.md) [blasphēmeō](../../strongs/g/g987.md) [legō](../../strongs/g/g3004.md) they against him.

<a name="luke_22_66"></a>Luke 22:66

And as soon as it was [hēmera](../../strongs/g/g2250.md), the [presbyterion](../../strongs/g/g4244.md) of the [laos](../../strongs/g/g2992.md) and the [archiereus](../../strongs/g/g749.md) and the [grammateus](../../strongs/g/g1122.md) [synagō](../../strongs/g/g4863.md), and [anagō](../../strongs/g/g321.md) him into their [synedrion](../../strongs/g/g4892.md), [legō](../../strongs/g/g3004.md),

<a name="luke_22_67"></a>Luke 22:67

Art thou the [Christos](../../strongs/g/g5547.md)? [eipon](../../strongs/g/g2036.md) us. And he [eipon](../../strongs/g/g2036.md) unto them, **If I [eipon](../../strongs/g/g2036.md) you, ye will not [pisteuō](../../strongs/g/g4100.md):**

<a name="luke_22_68"></a>Luke 22:68

**And if I [erōtaō](../../strongs/g/g2065.md), ye will not [apokrinomai](../../strongs/g/g611.md) me, nor [apolyō](../../strongs/g/g630.md).**

<a name="luke_22_69"></a>Luke 22:69

**Hereafter shall the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [kathēmai](../../strongs/g/g2521.md) on the [dexios](../../strongs/g/g1188.md) of the [dynamis](../../strongs/g/g1411.md) of [theos](../../strongs/g/g2316.md).**

<a name="luke_22_70"></a>Luke 22:70

Then [phēmi](../../strongs/g/g5346.md) they all, Art thou then the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md)? And he [eipon](../../strongs/g/g2036.md) unto them, **Ye [legō](../../strongs/g/g3004.md) that I am.**

<a name="luke_22_71"></a>Luke 22:71

And they [eipon](../../strongs/g/g2036.md), What [chreia](../../strongs/g/g5532.md) we any further [martyria](../../strongs/g/g3141.md)? for we ourselves have [akouō](../../strongs/g/g191.md) of his own [stoma](../../strongs/g/g4750.md).

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 21](luke_21.md) - [Luke 23](luke_23.md)

---

[^1]: [Luke 22:43 Commentary](../../commentary/luke/luke_22_commentary.md#luke_22_43)
