# [Luke 7](https://www.blueletterbible.org/kjv/luk/7/1/rl1/s_980001)

<a name="luke_7_1"></a>Luke 7:1

Now when he had [plēroō](../../strongs/g/g4137.md) all his [rhēma](../../strongs/g/g4487.md) in the [akoē](../../strongs/g/g189.md) of the [laos](../../strongs/g/g2992.md), he [eiserchomai](../../strongs/g/g1525.md) into [Kapharnaoum](../../strongs/g/g2584.md).

<a name="luke_7_2"></a>Luke 7:2

And a certain [hekatontarchēs](../../strongs/g/g1543.md) [doulos](../../strongs/g/g1401.md), who was [entimos](../../strongs/g/g1784.md) unto him, was [kakōs](../../strongs/g/g2560.md), and [mellō](../../strongs/g/g3195.md) to [teleutaō](../../strongs/g/g5053.md).

<a name="luke_7_3"></a>Luke 7:3

And when he [akouō](../../strongs/g/g191.md) of [Iēsous](../../strongs/g/g2424.md), he [apostellō](../../strongs/g/g649.md) unto him the [presbyteros](../../strongs/g/g4245.md) of the [Ioudaios](../../strongs/g/g2453.md), [erōtaō](../../strongs/g/g2065.md) him that he would [erchomai](../../strongs/g/g2064.md) and [diasōzō](../../strongs/g/g1295.md) his [doulos](../../strongs/g/g1401.md).

<a name="luke_7_4"></a>Luke 7:4

And when they [paraginomai](../../strongs/g/g3854.md) to [Iēsous](../../strongs/g/g2424.md), they [parakaleō](../../strongs/g/g3870.md) him [spoudaiōs](../../strongs/g/g4709.md), [legō](../../strongs/g/g3004.md), That he was [axios](../../strongs/g/g514.md) for whom he should [parechō](../../strongs/g/g3930.md) this:

<a name="luke_7_5"></a>Luke 7:5

For he [agapaō](../../strongs/g/g25.md) our [ethnos](../../strongs/g/g1484.md), and he hath [oikodomeō](../../strongs/g/g3618.md) us a [synagōgē](../../strongs/g/g4864.md).

<a name="luke_7_6"></a>Luke 7:6

Then [Iēsous](../../strongs/g/g2424.md) [poreuō](../../strongs/g/g4198.md) with them. And when he was now not [makran](../../strongs/g/g3112.md) from the [oikia](../../strongs/g/g3614.md), the [hekatontarchēs](../../strongs/g/g1543.md) [pempō](../../strongs/g/g3992.md) [philos](../../strongs/g/g5384.md) to him, [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), [skyllō](../../strongs/g/g4660.md) not thyself: for I am not [hikanos](../../strongs/g/g2425.md) that thou shouldest [eiserchomai](../../strongs/g/g1525.md) under my [stegē](../../strongs/g/g4721.md):

<a name="luke_7_7"></a>Luke 7:7

Wherefore neither thought I myself [axioō](../../strongs/g/g515.md) to [erchomai](../../strongs/g/g2064.md) unto thee: but [eipon](../../strongs/g/g2036.md) [logos](../../strongs/g/g3056.md), and my [pais](../../strongs/g/g3816.md) shall be [iaomai](../../strongs/g/g2390.md).

<a name="luke_7_8"></a>Luke 7:8

For I also am an [anthrōpos](../../strongs/g/g444.md) [tassō](../../strongs/g/g5021.md) under [exousia](../../strongs/g/g1849.md), having under me [stratiōtēs](../../strongs/g/g4757.md), and I [legō](../../strongs/g/g3004.md) unto one, [poreuō](../../strongs/g/g4198.md), and he [poreuō](../../strongs/g/g4198.md); and to another, [erchomai](../../strongs/g/g2064.md), and he [erchomai](../../strongs/g/g2064.md); and to my [doulos](../../strongs/g/g1401.md), [poieō](../../strongs/g/g4160.md) this, and he [poieō](../../strongs/g/g4160.md).

<a name="luke_7_9"></a>Luke 7:9

When [Iēsous](../../strongs/g/g2424.md) [akouō](../../strongs/g/g191.md) these things, he [thaumazō](../../strongs/g/g2296.md) at him, and [strephō](../../strongs/g/g4762.md), and [eipon](../../strongs/g/g2036.md) unto the [ochlos](../../strongs/g/g3793.md) that [akoloutheō](../../strongs/g/g190.md) him, ***I [legō](../../strongs/g/g3004.md) unto you, I have not [heuriskō](../../strongs/g/g2147.md) [tosoutos](../../strongs/g/g5118.md) [pistis](../../strongs/g/g4102.md), [oude](../../strongs/g/g3761.md) in [Israēl](../../strongs/g/g2474.md)***.

<a name="luke_7_10"></a>Luke 7:10

And they that were [pempō](../../strongs/g/g3992.md), [hypostrephō](../../strongs/g/g5290.md) to the [oikos](../../strongs/g/g3624.md), [heuriskō](../../strongs/g/g2147.md) the [doulos](../../strongs/g/g1401.md) [hygiainō](../../strongs/g/g5198.md) that had been [astheneō](../../strongs/g/g770.md).

<a name="luke_7_11"></a>Luke 7:11

And [ginomai](../../strongs/g/g1096.md) the [hexēs](../../strongs/g/g1836.md), that he [poreuō](../../strongs/g/g4198.md) into a [polis](../../strongs/g/g4172.md) [kaleō](../../strongs/g/g2564.md) [Naïn](../../strongs/g/g3484.md); and [hikanos](../../strongs/g/g2425.md) of his [mathētēs](../../strongs/g/g3101.md) [symporeuomai](../../strongs/g/g4848.md) him, and [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md).

<a name="luke_7_12"></a>Luke 7:12

Now when he [eggizō](../../strongs/g/g1448.md) to the [pylē](../../strongs/g/g4439.md) of the [polis](../../strongs/g/g4172.md), [idou](../../strongs/g/g2400.md), there was a [thnēskō](../../strongs/g/g2348.md) [ekkomizō](../../strongs/g/g1580.md), the [monogenēs](../../strongs/g/g3439.md) [huios](../../strongs/g/g5207.md) of his [mētēr](../../strongs/g/g3384.md), and she was a [chēra](../../strongs/g/g5503.md): and [hikanos](../../strongs/g/g2425.md) [ochlos](../../strongs/g/g3793.md) of the [polis](../../strongs/g/g4172.md) was with her.

<a name="luke_7_13"></a>Luke 7:13

And when the [kyrios](../../strongs/g/g2962.md) [eidō](../../strongs/g/g1492.md) her, he had [splagchnizomai](../../strongs/g/g4697.md) on her, and [eipon](../../strongs/g/g2036.md) unto her, [klaiō](../../strongs/g/g2799.md) not.

<a name="luke_7_14"></a>Luke 7:14

And he [proserchomai](../../strongs/g/g4334.md) and [haptomai](../../strongs/g/g680.md) the [soros](../../strongs/g/g4673.md): and they that [bastazō](../../strongs/g/g941.md) him [histēmi](../../strongs/g/g2476.md). And he [eipon](../../strongs/g/g2036.md), [neaniskos](../../strongs/g/g3495.md), I [legō](../../strongs/g/g3004.md) unto thee, [egeirō](../../strongs/g/g1453.md).

<a name="luke_7_15"></a>Luke 7:15

And [nekros](../../strongs/g/g3498.md) [anakathizō](../../strongs/g/g339.md), and [archomai](../../strongs/g/g756.md) to [laleō](../../strongs/g/g2980.md). And he [didōmi](../../strongs/g/g1325.md) him to his [mētēr](../../strongs/g/g3384.md).

<a name="luke_7_16"></a>Luke 7:16

And there [lambanō](../../strongs/g/g2983.md) a [phobos](../../strongs/g/g5401.md) on all: and they [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md), [legō](../../strongs/g/g3004.md), That a [megas](../../strongs/g/g3173.md) [prophētēs](../../strongs/g/g4396.md) is [egeirō](../../strongs/g/g1453.md) among us; and, That [theos](../../strongs/g/g2316.md) hath [episkeptomai](../../strongs/g/g1980.md) his [laos](../../strongs/g/g2992.md).

<a name="luke_7_17"></a>Luke 7:17

And this [logos](../../strongs/g/g3056.md) of him [exerchomai](../../strongs/g/g1831.md) throughout all [Ioudaia](../../strongs/g/g2449.md), and throughout all the [perichōros](../../strongs/g/g4066.md).

<a name="luke_7_18"></a>Luke 7:18

And the [mathētēs](../../strongs/g/g3101.md) of [Iōannēs](../../strongs/g/g2491.md) [apaggellō](../../strongs/g/g518.md) him of all these things.

<a name="luke_7_19"></a>Luke 7:19

And [Iōannēs](../../strongs/g/g2491.md) [proskaleō](../../strongs/g/g4341.md) two of his [mathētēs](../../strongs/g/g3101.md) [pempō](../../strongs/g/g3992.md) them to [Iēsous](../../strongs/g/g2424.md), [legō](../../strongs/g/g3004.md), Art thou he that should [erchomai](../../strongs/g/g2064.md)? or [prosdokaō](../../strongs/g/g4328.md) we for another?

<a name="luke_7_20"></a>Luke 7:20

When the [anēr](../../strongs/g/g435.md) were [paraginomai](../../strongs/g/g3854.md) unto him, they [eipon](../../strongs/g/g2036.md), [Iōannēs](../../strongs/g/g2491.md) [baptistēs](../../strongs/g/g910.md) hath [apostellō](../../strongs/g/g649.md) us unto thee, [legō](../../strongs/g/g3004.md), Art thou he that should [erchomai](../../strongs/g/g2064.md)? or [prosdokaō](../../strongs/g/g4328.md) we for another?

<a name="luke_7_21"></a>Luke 7:21

And in that same [hōra](../../strongs/g/g5610.md) he [therapeuō](../../strongs/g/g2323.md) [polys](../../strongs/g/g4183.md) of their [nosos](../../strongs/g/g3554.md) and [mastix](../../strongs/g/g3148.md), and of [ponēros](../../strongs/g/g4190.md) [pneuma](../../strongs/g/g4151.md); and unto [polys](../../strongs/g/g4183.md) that were [typhlos](../../strongs/g/g5185.md) he [charizomai](../../strongs/g/g5483.md) [blepō](../../strongs/g/g991.md).

<a name="luke_7_22"></a>Luke 7:22

Then [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto them, **[poreuō](../../strongs/g/g4198.md), and [apaggellō](../../strongs/g/g518.md) [Iōannēs](../../strongs/g/g2491.md) what things ye have [eidō](../../strongs/g/g1492.md) and [akouō](../../strongs/g/g191.md); how that the [typhlos](../../strongs/g/g5185.md) [anablepō](../../strongs/g/g308.md), the [chōlos](../../strongs/g/g5560.md) [peripateō](../../strongs/g/g4043.md), the [lepros](../../strongs/g/g3015.md) [katharizō](../../strongs/g/g2511.md), the [kōphos](../../strongs/g/g2974.md) [akouō](../../strongs/g/g191.md), the [nekros](../../strongs/g/g3498.md) [egeirō](../../strongs/g/g1453.md), to the [ptōchos](../../strongs/g/g4434.md) [euaggelizō](../../strongs/g/g2097.md).**

<a name="luke_7_23"></a>Luke 7:23

**And [makarios](../../strongs/g/g3107.md) is, whosoever shall not be [skandalizō](../../strongs/g/g4624.md) in me.**

<a name="luke_7_24"></a>Luke 7:24

And when the [aggelos](../../strongs/g/g32.md) of [Iōannēs](../../strongs/g/g2491.md) were [aperchomai](../../strongs/g/g565.md), he [archomai](../../strongs/g/g756.md) to [legō](../../strongs/g/g3004.md) unto the [ochlos](../../strongs/g/g3793.md) concerning [Iōannēs](../../strongs/g/g2491.md), **What [exerchomai](../../strongs/g/g1831.md) ye into the [erēmos](../../strongs/g/g2048.md) for to [theaomai](../../strongs/g/g2300.md)?  A [kalamos](../../strongs/g/g2563.md) [saleuō](../../strongs/g/g4531.md) with the [anemos](../../strongs/g/g417.md)?**

<a name="luke_7_25"></a>Luke 7:25

**But what [exerchomai](../../strongs/g/g1831.md) ye for to [eidō](../../strongs/g/g1492.md)? an [anthrōpos](../../strongs/g/g444.md) [amphiennymi](../../strongs/g/g294.md) in [malakos](../../strongs/g/g3120.md) [himation](../../strongs/g/g2440.md)?  [idou](../../strongs/g/g2400.md), they which are [endoxos](../../strongs/g/g1741.md) [himatismos](../../strongs/g/g2441.md), and [hyparchō](../../strongs/g/g5225.md) [tryphē](../../strongs/g/g5172.md), are in [basileios](../../strongs/g/g933.md).**

<a name="luke_7_26"></a>Luke 7:26

**But what [exerchomai](../../strongs/g/g1831.md) ye for to [eidō](../../strongs/g/g1492.md)? A [prophētēs](../../strongs/g/g4396.md)? [nai](../../strongs/g/g3483.md), I [legō](../../strongs/g/g3004.md) unto you, and [perissoteros](../../strongs/g/g4055.md) than a [prophētēs](../../strongs/g/g4396.md).**

<a name="luke_7_27"></a>Luke 7:27

**This is, of whom it is [graphō](../../strongs/g/g1125.md), [idou](../../strongs/g/g2400.md), I [apostellō](../../strongs/g/g649.md) my [aggelos](../../strongs/g/g32.md) before thy [prosōpon](../../strongs/g/g4383.md), which shall [kataskeuazō](../../strongs/g/g2680.md) thy [hodos](../../strongs/g/g3598.md) before thee.**

<a name="luke_7_28"></a>Luke 7:28

**For I [legō](../../strongs/g/g3004.md) unto you, Among those [gennētos](../../strongs/g/g1084.md) of [gynē](../../strongs/g/g1135.md) there is not a [meizōn](../../strongs/g/g3187.md) [prophētēs](../../strongs/g/g4396.md) than [Iōannēs](../../strongs/g/g2491.md) the [baptistēs](../../strongs/g/g910.md): but he that is [mikros](../../strongs/g/g3398.md) in the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) is [meizōn](../../strongs/g/g3187.md) than he.**

<a name="luke_7_29"></a>Luke 7:29

And all the [laos](../../strongs/g/g2992.md) that [akouō](../../strongs/g/g191.md), and the [telōnēs](../../strongs/g/g5057.md), [dikaioō](../../strongs/g/g1344.md) [theos](../../strongs/g/g2316.md), being [baptizō](../../strongs/g/g907.md) with the [baptisma](../../strongs/g/g908.md) of [Iōannēs](../../strongs/g/g2491.md).

<a name="luke_7_30"></a>Luke 7:30

But the [Pharisaios](../../strongs/g/g5330.md) and [nomikos](../../strongs/g/g3544.md) [atheteō](../../strongs/g/g114.md) the [boulē](../../strongs/g/g1012.md) of [theos](../../strongs/g/g2316.md) against themselves, being not [baptizō](../../strongs/g/g907.md) of him.

<a name="luke_7_31"></a>Luke 7:31

And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md), **Whereunto then shall I [homoioō](../../strongs/g/g3666.md) the [anthrōpos](../../strongs/g/g444.md) of this [genea](../../strongs/g/g1074.md)? and to what are they [homoios](../../strongs/g/g3664.md)?**

<a name="luke_7_32"></a>Luke 7:32

**They are [homoios](../../strongs/g/g3664.md) unto [paidion](../../strongs/g/g3813.md) [kathēmai](../../strongs/g/g2521.md) in the [agora](../../strongs/g/g58.md), and [prosphōneō](../../strongs/g/g4377.md) to [allēlōn](../../strongs/g/g240.md), and [legō](../../strongs/g/g3004.md), We have [auleō](../../strongs/g/g832.md) unto you, and ye have not [orcheomai](../../strongs/g/g3738.md); we have [thrēneō](../../strongs/g/g2354.md) to you, and ye have not [klaiō](../../strongs/g/g2799.md).**

<a name="luke_7_33"></a>Luke 7:33

**For [Iōannēs](../../strongs/g/g2491.md) the [baptistēs](../../strongs/g/g910.md) [erchomai](../../strongs/g/g2064.md) neither [esthiō](../../strongs/g/g2068.md) [artos](../../strongs/g/g740.md) nor [pinō](../../strongs/g/g4095.md) [oinos](../../strongs/g/g3631.md); and ye [legō](../../strongs/g/g3004.md), He hath a [daimonion](../../strongs/g/g1140.md).**

<a name="luke_7_34"></a>Luke 7:34

**The [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) is [erchomai](../../strongs/g/g2064.md) [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md); and ye [legō](../../strongs/g/g3004.md), [idou](../../strongs/g/g2400.md) a [phagos](../../strongs/g/g5314.md) [anthrōpos](../../strongs/g/g444.md), and a [oinopotēs](../../strongs/g/g3630.md), a [philos](../../strongs/g/g5384.md) of [telōnēs](../../strongs/g/g5057.md) and [hamartōlos](../../strongs/g/g268.md)!**

<a name="luke_7_35"></a>Luke 7:35

**But [sophia](../../strongs/g/g4678.md) is [dikaioō](../../strongs/g/g1344.md) of all her [teknon](../../strongs/g/g5043.md).**

<a name="luke_7_36"></a>Luke 7:36

And one of the [Pharisaios](../../strongs/g/g5330.md) [erōtaō](../../strongs/g/g2065.md) him that he would [phago](../../strongs/g/g5315.md) with him. And he [eiserchomai](../../strongs/g/g1525.md) into the [Pharisaios](../../strongs/g/g5330.md) [oikia](../../strongs/g/g3614.md), and [anaklinō](../../strongs/g/g347.md).

<a name="luke_7_37"></a>Luke 7:37

And, [idou](../../strongs/g/g2400.md), a [gynē](../../strongs/g/g1135.md) in the [polis](../../strongs/g/g4172.md), which was a [hamartōlos](../../strongs/g/g268.md), when she [epiginōskō](../../strongs/g/g1921.md) that [anakeimai](../../strongs/g/g345.md) in the [Pharisaios](../../strongs/g/g5330.md) [oikia](../../strongs/g/g3614.md), [komizō](../../strongs/g/g2865.md) an [alabastron](../../strongs/g/g211.md) of [myron](../../strongs/g/g3464.md),

<a name="luke_7_38"></a>Luke 7:38

And [histēmi](../../strongs/g/g2476.md) at his [pous](../../strongs/g/g4228.md) behind [klaiō](../../strongs/g/g2799.md), and [archomai](../../strongs/g/g756.md) to [brechō](../../strongs/g/g1026.md) his [pous](../../strongs/g/g4228.md) with [dakry](../../strongs/g/g1144.md), and did [ekmassō](../../strongs/g/g1591.md) with the [thrix](../../strongs/g/g2359.md) of her [kephalē](../../strongs/g/g2776.md), and [kataphileō](../../strongs/g/g2705.md) his [pous](../../strongs/g/g4228.md), and [aleiphō](../../strongs/g/g218.md) with the [myron](../../strongs/g/g3464.md).

<a name="luke_7_39"></a>Luke 7:39

Now when the [Pharisaios](../../strongs/g/g5330.md) which had [kaleō](../../strongs/g/g2564.md) him [eidō](../../strongs/g/g1492.md), he [eipon](../../strongs/g/g2036.md) within himself, [legō](../../strongs/g/g3004.md), [houtos](../../strongs/g/g3778.md), if he were a [prophētēs](../../strongs/g/g4396.md), would have [ginōskō](../../strongs/g/g1097.md) who and [potapos](../../strongs/g/g4217.md) of [gynē](../../strongs/g/g1135.md) that [haptomai](../../strongs/g/g680.md) him: for she is a [hamartōlos](../../strongs/g/g268.md).

<a name="luke_7_40"></a>Luke 7:40

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto him, **[Simōn](../../strongs/g/g4613.md), I have somewhat to [eipon](../../strongs/g/g2036.md) unto thee.** And he [phēmi](../../strongs/g/g5346.md), [didaskalos](../../strongs/g/g1320.md), [eipon](../../strongs/g/g2036.md).

<a name="luke_7_41"></a>Luke 7:41

**There was a certain [daneistēs](../../strongs/g/g1157.md) which had two [chreopheiletēs](../../strongs/g/g5533.md): the one [opheilō](../../strongs/g/g3784.md) five hundred [dēnarion](../../strongs/g/g1220.md), and the other fifty.**

<a name="luke_7_42"></a>Luke 7:42

**And when they had [mē](../../strongs/g/g3361.md) to [apodidōmi](../../strongs/g/g591.md), he [charizomai](../../strongs/g/g5483.md) them both. [eipon](../../strongs/g/g2036.md) me therefore, which of them will [agapaō](../../strongs/g/g25.md) him [pleiōn](../../strongs/g/g4119.md)?**

<a name="luke_7_43"></a>Luke 7:43

[Simōn](../../strongs/g/g4613.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), I [hypolambanō](../../strongs/g/g5274.md) that, to whom he [charizomai](../../strongs/g/g5483.md) most. And he [eipon](../../strongs/g/g2036.md) unto him, **Thou hast [orthōs](../../strongs/g/g3723.md) [krinō](../../strongs/g/g2919.md).**

<a name="luke_7_44"></a>Luke 7:44

And he [strephō](../../strongs/g/g4762.md) to the [gynē](../../strongs/g/g1135.md), and [phēmi](../../strongs/g/g5346.md) unto [Simōn](../../strongs/g/g4613.md), **[blepō](../../strongs/g/g991.md) thou this [gynē](../../strongs/g/g1135.md)? I [eiserchomai](../../strongs/g/g1525.md) into thine [oikia](../../strongs/g/g3614.md), thou [didōmi](../../strongs/g/g1325.md) me no [hydōr](../../strongs/g/g5204.md) for my [pous](../../strongs/g/g4228.md): but she hath [brechō](../../strongs/g/g1026.md) my [pous](../../strongs/g/g4228.md) with [dakry](../../strongs/g/g1144.md), and [ekmassō](../../strongs/g/g1591.md) with the [thrix](../../strongs/g/g2359.md) of her [kephalē](../../strongs/g/g2776.md).**

<a name="luke_7_45"></a>Luke 7:45

**Thou [didōmi](../../strongs/g/g1325.md) me no [philēma](../../strongs/g/g5370.md): but [houtos](../../strongs/g/g3778.md) since [hos](../../strongs/g/g3739.md) I [eiserchomai](../../strongs/g/g1525.md) hath not [dialeipō](../../strongs/g/g1257.md) to [kataphileō](../../strongs/g/g2705.md) my [pous](../../strongs/g/g4228.md).**

<a name="luke_7_46"></a>Luke 7:46

**My [kephalē](../../strongs/g/g2776.md) with [elaion](../../strongs/g/g1637.md) thou didst not [aleiphō](../../strongs/g/g218.md): but [houtos](../../strongs/g/g3778.md) hath [aleiphō](../../strongs/g/g218.md) my [pous](../../strongs/g/g4228.md) with [myron](../../strongs/g/g3464.md).**

<a name="luke_7_47"></a>Luke 7:47

**Wherefore I [legō](../../strongs/g/g3004.md) unto thee, Her [hamartia](../../strongs/g/g266.md), which are [polys](../../strongs/g/g4183.md), are [aphiēmi](../../strongs/g/g863.md); for she [agapaō](../../strongs/g/g25.md) much: but to whom [oligos](../../strongs/g/g3641.md) is [aphiēmi](../../strongs/g/g863.md), [agapaō](../../strongs/g/g25.md) [oligos](../../strongs/g/g3641.md).**

<a name="luke_7_48"></a>Luke 7:48

And he [eipon](../../strongs/g/g2036.md) unto her, **Thy [hamartia](../../strongs/g/g266.md) are [aphiēmi](../../strongs/g/g863.md).**

<a name="luke_7_49"></a>Luke 7:49

And they that [synanakeimai](../../strongs/g/g4873.md) with him [archomai](../../strongs/g/g756.md) to [legō](../../strongs/g/g3004.md) within themselves, Who is this that [aphiēmi](../../strongs/g/g863.md) [hamartia](../../strongs/g/g266.md) also?

<a name="luke_7_50"></a>Luke 7:50

And he [eipon](../../strongs/g/g2036.md) to the [gynē](../../strongs/g/g1135.md), **Thy [pistis](../../strongs/g/g4102.md) hath [sōzō](../../strongs/g/g4982.md) thee; [poreuō](../../strongs/g/g4198.md) in [eirēnē](../../strongs/g/g1515.md).**

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke 6](luke_6.md) - [Luke 8](luke_8.md)