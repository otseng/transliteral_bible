# [Luke 5](https://www.blueletterbible.org/kjv/luk/5/1/rl1/s_978001)

<a name="luke_5_1"></a>Luke 5:1

And [ginomai](../../strongs/g/g1096.md), that, as the [ochlos](../../strongs/g/g3793.md) [epikeimai](../../strongs/g/g1945.md) him to [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md), he [histēmi](../../strongs/g/g2476.md) by the [limnē](../../strongs/g/g3041.md) of [Gennēsaret](../../strongs/g/g1082.md),

<a name="luke_5_2"></a>Luke 5:2

And [eidō](../../strongs/g/g1492.md) two [ploion](../../strongs/g/g4143.md) [histēmi](../../strongs/g/g2476.md) by the [limnē](../../strongs/g/g3041.md): but the [halieus](../../strongs/g/g231.md) were [apobainō](../../strongs/g/g576.md) out of them, and were [apoplynō](../../strongs/g/g637.md) their [diktyon](../../strongs/g/g1350.md).

<a name="luke_5_3"></a>Luke 5:3

And he [embainō](../../strongs/g/g1684.md) into one of the [ploion](../../strongs/g/g4143.md), which was [Simōn](../../strongs/g/g4613.md), and [erōtaō](../../strongs/g/g2065.md) him that he would [epanagō](../../strongs/g/g1877.md) a [oligos](../../strongs/g/g3641.md) from the [gē](../../strongs/g/g1093.md). And he [kathizō](../../strongs/g/g2523.md), and [didaskō](../../strongs/g/g1321.md) the [ochlos](../../strongs/g/g3793.md) out of the [ploion](../../strongs/g/g4143.md).

<a name="luke_5_4"></a>Luke 5:4

Now when he had [pauō](../../strongs/g/g3973.md) [laleō](../../strongs/g/g2980.md), he [eipon](../../strongs/g/g2036.md) unto [Simōn](../../strongs/g/g4613.md), **[epanagō](../../strongs/g/g1877.md) into the [bathos](../../strongs/g/g899.md), and [chalaō](../../strongs/g/g5465.md) your [diktyon](../../strongs/g/g1350.md) for an [agra](../../strongs/g/g61.md).**

<a name="luke_5_5"></a>Luke 5:5

And [Simōn](../../strongs/g/g4613.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto him, [epistatēs](../../strongs/g/g1988.md), we have [kopiaō](../../strongs/g/g2872.md) all the [nyx](../../strongs/g/g3571.md), and have [lambanō](../../strongs/g/g2983.md) nothing: nevertheless at thy [rhēma](../../strongs/g/g4487.md) I will [chalaō](../../strongs/g/g5465.md) the [diktyon](../../strongs/g/g1350.md).

<a name="luke_5_6"></a>Luke 5:6

And when they [poieō](../../strongs/g/g4160.md) this, they [sygkleiō](../../strongs/g/g4788.md) a [polys](../../strongs/g/g4183.md) [plēthos](../../strongs/g/g4128.md) of [ichthys](../../strongs/g/g2486.md): and their [diktyon](../../strongs/g/g1350.md) [diarrēssō](../../strongs/g/g1284.md).

<a name="luke_5_7"></a>Luke 5:7

And they [kataneuō](../../strongs/g/g2656.md) unto [metochos](../../strongs/g/g3353.md), which were in the other [ploion](../../strongs/g/g4143.md), that they should [erchomai](../../strongs/g/g2064.md) and [syllambanō](../../strongs/g/g4815.md) them. And they [erchomai](../../strongs/g/g2064.md), and [pimplēmi](../../strongs/g/g4130.md) both the [ploion](../../strongs/g/g4143.md), so that they be [bythizō](../../strongs/g/g1036.md).

<a name="luke_5_8"></a>Luke 5:8

When [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) [eidō](../../strongs/g/g1492.md), he [prospiptō](../../strongs/g/g4363.md) at [Iēsous](../../strongs/g/g2424.md) [gony](../../strongs/g/g1119.md), [legō](../../strongs/g/g3004.md), [exerchomai](../../strongs/g/g1831.md) from me; for I am a [hamartōlos](../../strongs/g/g268.md) [anēr](../../strongs/g/g435.md), [kyrios](../../strongs/g/g2962.md).

<a name="luke_5_9"></a>Luke 5:9

For he was [periechō](../../strongs/g/g4023.md) [thambos](../../strongs/g/g2285.md), and all that were with him, at the [agra](../../strongs/g/g61.md) of the [ichthys](../../strongs/g/g2486.md) which they had [syllambanō](../../strongs/g/g4815.md):

<a name="luke_5_10"></a>Luke 5:10

And [homoiōs](../../strongs/g/g3668.md) also [Iakōbos](../../strongs/g/g2385.md), and [Iōannēs](../../strongs/g/g2491.md), the [huios](../../strongs/g/g5207.md) of [Zebedaios](../../strongs/g/g2199.md), which were [koinōnos](../../strongs/g/g2844.md) with [Simōn](../../strongs/g/g4613.md). And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto [Simōn](../../strongs/g/g4613.md), **[phobeō](../../strongs/g/g5399.md) not; from henceforth thou shalt [zōgreō](../../strongs/g/g2221.md) [anthrōpos](../../strongs/g/g444.md).**

<a name="luke_5_11"></a>Luke 5:11

And when they had [katagō](../../strongs/g/g2609.md) their [ploion](../../strongs/g/g4143.md) to [gē](../../strongs/g/g1093.md), they [aphiēmi](../../strongs/g/g863.md) all, and [akoloutheō](../../strongs/g/g190.md) him.

<a name="luke_5_12"></a>Luke 5:12

And [ginomai](../../strongs/g/g1096.md), when he was in a certain [polis](../../strongs/g/g4172.md), [idou](../../strongs/g/g2400.md) an [anēr](../../strongs/g/g435.md) [plērēs](../../strongs/g/g4134.md) of [lepra](../../strongs/g/g3014.md): who [eidō](../../strongs/g/g1492.md) [Iēsous](../../strongs/g/g2424.md) [piptō](../../strongs/g/g4098.md) on [prosōpon](../../strongs/g/g4383.md), and [deomai](../../strongs/g/g1189.md) him, [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), if thou wilt, thou canst [katharizō](../../strongs/g/g2511.md) me.

<a name="luke_5_13"></a>Luke 5:13

And he [ekteinō](../../strongs/g/g1614.md) his [cheir](../../strongs/g/g5495.md), and [haptomai](../../strongs/g/g680.md) him, [eipon](../../strongs/g/g2036.md), **I [thelō](../../strongs/g/g2309.md): be thou [katharizō](../../strongs/g/g2511.md).** And [eutheōs](../../strongs/g/g2112.md) the [lepra](../../strongs/g/g3014.md) [aperchomai](../../strongs/g/g565.md) from him.

<a name="luke_5_14"></a>Luke 5:14

And he [paraggellō](../../strongs/g/g3853.md) him to [eipon](../../strongs/g/g2036.md) [mēdeis](../../strongs/g/g3367.md): **but [aperchomai](../../strongs/g/g565.md), and [deiknyō](../../strongs/g/g1166.md) thyself to the [hiereus](../../strongs/g/g2409.md), and [prospherō](../../strongs/g/g4374.md) for thy [katharismos](../../strongs/g/g2512.md), according as [Mōÿsēs](../../strongs/g/g3475.md) [prostassō](../../strongs/g/g4367.md), for a [martyrion](../../strongs/g/g3142.md) unto them.**

<a name="luke_5_15"></a>Luke 5:15

But so much the [mallon](../../strongs/g/g3123.md) [dierchomai](../../strongs/g/g1330.md) there a [logos](../../strongs/g/g3056.md) of him: and [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) [synerchomai](../../strongs/g/g4905.md) to [akouō](../../strongs/g/g191.md), and to be [therapeuō](../../strongs/g/g2323.md) by him of their [astheneia](../../strongs/g/g769.md).

<a name="luke_5_16"></a>Luke 5:16

And he [hypochōreō](../../strongs/g/g5298.md) himself into the [erēmos](../../strongs/g/g2048.md), and [proseuchomai](../../strongs/g/g4336.md).

<a name="luke_5_17"></a>Luke 5:17

And [ginomai](../../strongs/g/g1096.md) on a certain [hēmera](../../strongs/g/g2250.md), as he was [didaskō](../../strongs/g/g1321.md), that there were [Pharisaios](../../strongs/g/g5330.md) and [nomodidaskalos](../../strongs/g/g3547.md) [kathēmai](../../strongs/g/g2521.md) by, which were [erchomai](../../strongs/g/g2064.md) out of every [kōmē](../../strongs/g/g2968.md) of [Galilaia](../../strongs/g/g1056.md), and [Ioudaia](../../strongs/g/g2449.md), and [Ierousalēm](../../strongs/g/g2419.md): and the [dynamis](../../strongs/g/g1411.md) of the [kyrios](../../strongs/g/g2962.md) was present to [iaomai](../../strongs/g/g2390.md) them.

<a name="luke_5_18"></a>Luke 5:18

And, [idou](../../strongs/g/g2400.md), [anēr](../../strongs/g/g435.md) [pherō](../../strongs/g/g5342.md) in a [klinē](../../strongs/g/g2825.md) an [anthrōpos](../../strongs/g/g444.md) which was [paralyō](../../strongs/g/g3886.md): and they [zēteō](../../strongs/g/g2212.md) means to [eispherō](../../strongs/g/g1533.md) him in, and to [tithēmi](../../strongs/g/g5087.md) him before him.

<a name="luke_5_19"></a>Luke 5:19

And when they could not [heuriskō](../../strongs/g/g2147.md) by what way they might [eispherō](../../strongs/g/g1533.md) him in because of the [ochlos](../../strongs/g/g3793.md), they [anabainō](../../strongs/g/g305.md) upon the [dōma](../../strongs/g/g1430.md), and [kathiēmi](../../strongs/g/g2524.md) him through the [keramos](../../strongs/g/g2766.md) with his [klinidion](../../strongs/g/g2826.md) into the midst before [Iēsous](../../strongs/g/g2424.md).

<a name="luke_5_20"></a>Luke 5:20

And when he [eidō](../../strongs/g/g1492.md) their [pistis](../../strongs/g/g4102.md), he [eipon](../../strongs/g/g2036.md) unto him, **[anthrōpos](../../strongs/g/g444.md), thy [hamartia](../../strongs/g/g266.md) are [aphiēmi](../../strongs/g/g863.md) thee.**

<a name="luke_5_21"></a>Luke 5:21

And the [grammateus](../../strongs/g/g1122.md) and the [Pharisaios](../../strongs/g/g5330.md) [archomai](../../strongs/g/g756.md) to [dialogizomai](../../strongs/g/g1260.md), [legō](../../strongs/g/g3004.md), Who is this which [laleō](../../strongs/g/g2980.md) [blasphēmia](../../strongs/g/g988.md)? Who can [aphiēmi](../../strongs/g/g863.md) [hamartia](../../strongs/g/g266.md), but [theos](../../strongs/g/g2316.md) [monos](../../strongs/g/g3441.md)?

<a name="luke_5_22"></a>Luke 5:22

But when [Iēsous](../../strongs/g/g2424.md) [epiginōskō](../../strongs/g/g1921.md) their [dialogismos](../../strongs/g/g1261.md), he [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto them, **What [dialogizomai](../../strongs/g/g1260.md) ye in your [kardia](../../strongs/g/g2588.md)?**

<a name="luke_5_23"></a>Luke 5:23

**Whether is [eukopos](../../strongs/g/g2123.md), to [eipon](../../strongs/g/g2036.md), Thy [hamartia](../../strongs/g/g266.md) be [aphiēmi](../../strongs/g/g863.md) thee; or to [eipon](../../strongs/g/g2036.md), [egeirō](../../strongs/g/g1453.md) and [peripateō](../../strongs/g/g4043.md)?**

<a name="luke_5_24"></a>Luke 5:24

**But that ye may [eidō](../../strongs/g/g1492.md) that the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) hath [exousia](../../strongs/g/g1849.md) upon [gē](../../strongs/g/g1093.md) to [aphiēmi](../../strongs/g/g863.md) [hamartia](../../strongs/g/g266.md),** (he [eipon](../../strongs/g/g2036.md) unto the [paralyō](../../strongs/g/g3886.md),) **I [legō](../../strongs/g/g3004.md) unto thee, [egeirō](../../strongs/g/g1453.md), and [airō](../../strongs/g/g142.md) thy [klinidion](../../strongs/g/g2826.md), and [poreuō](../../strongs/g/g4198.md) into thine [oikos](../../strongs/g/g3624.md).**

<a name="luke_5_25"></a>Luke 5:25

And [parachrēma](../../strongs/g/g3916.md) he [anistēmi](../../strongs/g/g450.md) before them, and [airō](../../strongs/g/g142.md) that whereon he [katakeimai](../../strongs/g/g2621.md), and [aperchomai](../../strongs/g/g565.md) to his own [oikos](../../strongs/g/g3624.md), [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md).

<a name="luke_5_26"></a>Luke 5:26

And they [lambanō](../../strongs/g/g2983.md) all [ekstasis](../../strongs/g/g1611.md), and they [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md), and were [pimplēmi](../../strongs/g/g4130.md) with [phobos](../../strongs/g/g5401.md), [legō](../../strongs/g/g3004.md), We have [eidō](../../strongs/g/g1492.md) [paradoxos](../../strongs/g/g3861.md) [sēmeron](../../strongs/g/g4594.md).

<a name="luke_5_27"></a>Luke 5:27

And after these things he [exerchomai](../../strongs/g/g1831.md), and [theaomai](../../strongs/g/g2300.md) a [telōnēs](../../strongs/g/g5057.md), [onoma](../../strongs/g/g3686.md) [Leuis](../../strongs/g/g3018.md), [kathēmai](../../strongs/g/g2521.md) at the [telōnion](../../strongs/g/g5058.md): and he [eipon](../../strongs/g/g2036.md) unto him, **[akoloutheō](../../strongs/g/g190.md) me.**

<a name="luke_5_28"></a>Luke 5:28

And he [kataleipō](../../strongs/g/g2641.md) all, [anistēmi](../../strongs/g/g450.md), and [akoloutheō](../../strongs/g/g190.md) him.

<a name="luke_5_29"></a>Luke 5:29

And [Leuis](../../strongs/g/g3018.md) [poieō](../../strongs/g/g4160.md) him a [megas](../../strongs/g/g3173.md) [dochē](../../strongs/g/g1403.md) in his own [oikia](../../strongs/g/g3614.md): and there was a [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) of [telōnēs](../../strongs/g/g5057.md) and of others that [katakeimai](../../strongs/g/g2621.md) with them.

<a name="luke_5_30"></a>Luke 5:30

But their [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md) [goggyzō](../../strongs/g/g1111.md) against his [mathētēs](../../strongs/g/g3101.md), [legō](../../strongs/g/g3004.md), Why do ye [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md) with [telōnēs](../../strongs/g/g5057.md) and [hamartōlos](../../strongs/g/g268.md)?

<a name="luke_5_31"></a>Luke 5:31

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto them, **[hygiainō](../../strongs/g/g5198.md) [chreia](../../strongs/g/g5532.md) not an [iatros](../../strongs/g/g2395.md); but they that are [kakōs](../../strongs/g/g2560.md).**

<a name="luke_5_32"></a>Luke 5:32

**I [erchomai](../../strongs/g/g2064.md) not to [kaleō](../../strongs/g/g2564.md) the [dikaios](../../strongs/g/g1342.md), but [hamartōlos](../../strongs/g/g268.md) to [metanoia](../../strongs/g/g3341.md).**

<a name="luke_5_33"></a>Luke 5:33

And they [eipon](../../strongs/g/g2036.md) unto him, Why [nēsteuō](../../strongs/g/g3522.md) the [mathētēs](../../strongs/g/g3101.md) of [Iōannēs](../../strongs/g/g2491.md) [pyknos](../../strongs/g/g4437.md), and [poieō](../../strongs/g/g4160.md) [deēsis](../../strongs/g/g1162.md), and [homoiōs](../../strongs/g/g3668.md) the [ho](../../strongs/g/g3588.md) of the [Pharisaios](../../strongs/g/g5330.md); but thine [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md)?

<a name="luke_5_34"></a>Luke 5:34

And he [eipon](../../strongs/g/g2036.md) unto them, **Can ye [poieō](../../strongs/g/g4160.md) the [huios](../../strongs/g/g5207.md) of the [nymphōn](../../strongs/g/g3567.md) [nēsteuō](../../strongs/g/g3522.md), while the [nymphios](../../strongs/g/g3566.md) is with them?**

<a name="luke_5_35"></a>Luke 5:35

**But the [hēmera](../../strongs/g/g2250.md) will [erchomai](../../strongs/g/g2064.md), when the [nymphios](../../strongs/g/g3566.md) shall be [apairō](../../strongs/g/g522.md) from them, and then shall they [nēsteuō](../../strongs/g/g3522.md) in those [hēmera](../../strongs/g/g2250.md).**

<a name="luke_5_36"></a>Luke 5:36

And he [legō](../../strongs/g/g3004.md) also a [parabolē](../../strongs/g/g3850.md) unto them; **[oudeis](../../strongs/g/g3762.md) [epiballō](../../strongs/g/g1911.md) an [epiblēma](../../strongs/g/g1915.md) of a [kainos](../../strongs/g/g2537.md) [himation](../../strongs/g/g2440.md) upon a [palaios](../../strongs/g/g3820.md); if otherwise, then both the [kainos](../../strongs/g/g2537.md) [schizō](../../strongs/g/g4977.md), and the [epiblēma](../../strongs/g/g1915.md) that was out of the [kainos](../../strongs/g/g2537.md) [symphōneō](../../strongs/g/g4856.md) not with the [palaios](../../strongs/g/g3820.md).**

<a name="luke_5_37"></a>Luke 5:37

**And [oudeis](../../strongs/g/g3762.md) [ballō](../../strongs/g/g906.md) [neos](../../strongs/g/g3501.md) [oinos](../../strongs/g/g3631.md) into [palaios](../../strongs/g/g3820.md) [askos](../../strongs/g/g779.md); else the [neos](../../strongs/g/g3501.md) [oinos](../../strongs/g/g3631.md) will [rhēgnymi](../../strongs/g/g4486.md) the [askos](../../strongs/g/g779.md), and be [ekcheō](../../strongs/g/g1632.md), and the [askos](../../strongs/g/g779.md) shall [apollymi](../../strongs/g/g622.md).**

<a name="luke_5_38"></a>Luke 5:38

**But [neos](../../strongs/g/g3501.md) [oinos](../../strongs/g/g3631.md) must be [blēteos](../../strongs/g/g992.md) into [kainos](../../strongs/g/g2537.md) [askos](../../strongs/g/g779.md); and both are [syntēreō](../../strongs/g/g4933.md).**

<a name="luke_5_39"></a>Luke 5:39

**[oudeis](../../strongs/g/g3762.md) also having [pinō](../../strongs/g/g4095.md) [palaios](../../strongs/g/g3820.md) [eutheōs](../../strongs/g/g2112.md) [thelō](../../strongs/g/g2309.md) [neos](../../strongs/g/g3501.md): for he [legō](../../strongs/g/g3004.md), The [palaios](../../strongs/g/g3820.md) is [chrēstos](../../strongs/g/g5543.md).**

---

[Transliteral Bible](../bible.md)

[Luke](luke.md)

[Luke](luke_4.md) - [Luke](luke_6.md)