# [2 John 1](https://www.blueletterbible.org/kjv/2jo/1/1/s_1165001)

<a name="2john_1_1"></a>2 John 1:1

The [presbyteros](../../strongs/g/g4245.md) unto the [eklektos](../../strongs/g/g1588.md) [kyria](../../strongs/g/g2959.md) and her [teknon](../../strongs/g/g5043.md), whom I [agapaō](../../strongs/g/g25.md) in the [alētheia](../../strongs/g/g225.md); and not I only, but also all they that have [ginōskō](../../strongs/g/g1097.md) the [alētheia](../../strongs/g/g225.md);

<a name="2john_1_2"></a>2 John 1:2

For the [alētheia](../../strongs/g/g225.md), which [menō](../../strongs/g/g3306.md) in us, and shall be with us [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md).

<a name="2john_1_3"></a>2 John 1:3

[charis](../../strongs/g/g5485.md) be with you, [eleos](../../strongs/g/g1656.md), and [eirēnē](../../strongs/g/g1515.md), from [theos](../../strongs/g/g2316.md) the [patēr](../../strongs/g/g3962.md), and from the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), the [huios](../../strongs/g/g5207.md) of the [patēr](../../strongs/g/g3962.md), in [alētheia](../../strongs/g/g225.md) and [agapē](../../strongs/g/g26.md).

<a name="2john_1_4"></a>2 John 1:4

I [chairō](../../strongs/g/g5463.md) [lian](../../strongs/g/g3029.md) that I [heuriskō](../../strongs/g/g2147.md) of thy [teknon](../../strongs/g/g5043.md) [peripateō](../../strongs/g/g4043.md) in [alētheia](../../strongs/g/g225.md), as we have [lambanō](../../strongs/g/g2983.md) an [entolē](../../strongs/g/g1785.md) from the [patēr](../../strongs/g/g3962.md).

<a name="2john_1_5"></a>2 John 1:5

And now I [erōtaō](../../strongs/g/g2065.md) thee, [kyria](../../strongs/g/g2959.md), not as though I [graphō](../../strongs/g/g1125.md) a [kainos](../../strongs/g/g2537.md) [entolē](../../strongs/g/g1785.md) unto thee, but that which we had from the [archē](../../strongs/g/g746.md), that we [agapaō](../../strongs/g/g25.md) [allēlōn](../../strongs/g/g240.md).

<a name="2john_1_6"></a>2 John 1:6

And this is [agapē](../../strongs/g/g26.md), that we [peripateō](../../strongs/g/g4043.md) after his [entolē](../../strongs/g/g1785.md). This is the [entolē](../../strongs/g/g1785.md), That, as ye have [akouō](../../strongs/g/g191.md) from the [archē](../../strongs/g/g746.md), ye should [peripateō](../../strongs/g/g4043.md) in it.

<a name="2john_1_7"></a>2 John 1:7

For [polys](../../strongs/g/g4183.md) [planos](../../strongs/g/g4108.md) are [eiserchomai](../../strongs/g/g1525.md) into the [kosmos](../../strongs/g/g2889.md), who [homologeō](../../strongs/g/g3670.md) not that [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) is [erchomai](../../strongs/g/g2064.md) in the [sarx](../../strongs/g/g4561.md). This is a [planos](../../strongs/g/g4108.md) and an [antichristos](../../strongs/g/g500.md).

<a name="2john_1_8"></a>2 John 1:8

[blepō](../../strongs/g/g991.md) to yourselves, that we [apollymi](../../strongs/g/g622.md) not those things which we have [ergazomai](../../strongs/g/g2038.md), but that we [apolambanō](../../strongs/g/g618.md) a [plērēs](../../strongs/g/g4134.md) [misthos](../../strongs/g/g3408.md).

<a name="2john_1_9"></a>2 John 1:9

Whosoever [parabainō](../../strongs/g/g3845.md), and [menō](../../strongs/g/g3306.md) not in the [didachē](../../strongs/g/g1322.md) of [Christos](../../strongs/g/g5547.md), hath not [theos](../../strongs/g/g2316.md). He that [menō](../../strongs/g/g3306.md) in the [didachē](../../strongs/g/g1322.md) of [Christos](../../strongs/g/g5547.md), he hath both the [patēr](../../strongs/g/g3962.md) and the [huios](../../strongs/g/g5207.md).

<a name="2john_1_10"></a>2 John 1:10

If there [erchomai](../../strongs/g/g2064.md) any unto you, and [pherō](../../strongs/g/g5342.md) not this [didachē](../../strongs/g/g1322.md), [lambanō](../../strongs/g/g2983.md) him not into your [oikia](../../strongs/g/g3614.md), neither [legō](../../strongs/g/g3004.md) him [chairō](../../strongs/g/g5463.md):

<a name="2john_1_11"></a>2 John 1:11

For he that [legō](../../strongs/g/g3004.md) him [chairō](../../strongs/g/g5463.md) is [koinōneō](../../strongs/g/g2841.md) of his [ponēros](../../strongs/g/g4190.md) [ergon](../../strongs/g/g2041.md).

<a name="2john_1_12"></a>2 John 1:12

Having [polys](../../strongs/g/g4183.md) to [graphō](../../strongs/g/g1125.md) unto you, I [boulomai](../../strongs/g/g1014.md) not with [chartēs](../../strongs/g/g5489.md) and [melan](../../strongs/g/g3188.md): but I [elpizō](../../strongs/g/g1679.md) to [erchomai](../../strongs/g/g2064.md) unto you, and [laleō](../../strongs/g/g2980.md) [stoma](../../strongs/g/g4750.md) to [stoma](../../strongs/g/g4750.md), that our [chara](../../strongs/g/g5479.md) may be [plēroō](../../strongs/g/g4137.md).

<a name="2john_1_13"></a>2 John 1:13

The [teknon](../../strongs/g/g5043.md) of thy [eklektos](../../strongs/g/g1588.md) [adelphē](../../strongs/g/g79.md) [aspazomai](../../strongs/g/g782.md) thee. [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[2 John](2john.md)