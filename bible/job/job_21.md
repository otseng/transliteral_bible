# [Job 21](https://www.blueletterbible.org/kjv/job/21)

<a name="job_21_1"></a>Job 21:1

But ['Îyôḇ](../../strongs/h/h347.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md),

<a name="job_21_2"></a>Job 21:2

[shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) my [millâ](../../strongs/h/h4405.md), and let this be your [tanḥûmôṯ](../../strongs/h/h8575.md).

<a name="job_21_3"></a>Job 21:3

[nasa'](../../strongs/h/h5375.md) me that I may [dabar](../../strongs/h/h1696.md); and ['aḥar](../../strongs/h/h310.md) that I have [dabar](../../strongs/h/h1696.md), mock [lāʿaḡ](../../strongs/h/h3932.md).

<a name="job_21_4"></a>Job 21:4

As for me, is my [śîaḥ](../../strongs/h/h7879.md) to ['āḏām](../../strongs/h/h120.md)? and if it were so, why should not my [ruwach](../../strongs/h/h7307.md) be [qāṣar](../../strongs/h/h7114.md)?

<a name="job_21_5"></a>Job 21:5

[panah](../../strongs/h/h6437.md) me, and be [šāmēm](../../strongs/h/h8074.md), and [śûm](../../strongs/h/h7760.md) your [yad](../../strongs/h/h3027.md) upon your [peh](../../strongs/h/h6310.md).

<a name="job_21_6"></a>Job 21:6

Even when I [zakar](../../strongs/h/h2142.md) I am [bahal](../../strongs/h/h926.md), and [pallāṣûṯ](../../strongs/h/h6427.md) taketh hold ['āḥaz](../../strongs/h/h270.md) my [basar](../../strongs/h/h1320.md).

<a name="job_21_7"></a>Job 21:7

Wherefore do the [rasha'](../../strongs/h/h7563.md) [ḥāyâ](../../strongs/h/h2421.md), become ['athaq](../../strongs/h/h6275.md), yea, are [gabar](../../strongs/h/h1396.md) in [ḥayil](../../strongs/h/h2428.md)?

<a name="job_21_8"></a>Job 21:8

Their [zera'](../../strongs/h/h2233.md) is [kuwn](../../strongs/h/h3559.md) in their [paniym](../../strongs/h/h6440.md) with them, and their [ṣe'ĕṣā'îm](../../strongs/h/h6631.md) before their ['ayin](../../strongs/h/h5869.md).

<a name="job_21_9"></a>Job 21:9

Their [bayith](../../strongs/h/h1004.md) are [shalowm](../../strongs/h/h7965.md) from [paḥaḏ](../../strongs/h/h6343.md), neither is the [shebet](../../strongs/h/h7626.md) of ['ĕlvôha](../../strongs/h/h433.md) upon them.

<a name="job_21_10"></a>Job 21:10

Their [showr](../../strongs/h/h7794.md) ['abar](../../strongs/h/h5674.md), and [gāʿal](../../strongs/h/h1602.md) not; their [pārâ](../../strongs/h/h6510.md) [palat](../../strongs/h/h6403.md), and casteth not her [šāḵōl](../../strongs/h/h7921.md).

<a name="job_21_11"></a>Job 21:11

They [shalach](../../strongs/h/h7971.md) their [ʿăvîl](../../strongs/h/h5759.md) like a [tso'n](../../strongs/h/h6629.md), and their [yeleḏ](../../strongs/h/h3206.md) [rāqaḏ](../../strongs/h/h7540.md).

<a name="job_21_12"></a>Job 21:12

They [nasa'](../../strongs/h/h5375.md) the [tōp̄](../../strongs/h/h8596.md) and [kinnôr](../../strongs/h/h3658.md), and [samach](../../strongs/h/h8055.md) at the [qowl](../../strongs/h/h6963.md) of the [ʿûḡāḇ](../../strongs/h/h5748.md).

<a name="job_21_13"></a>Job 21:13

They [kalah](../../strongs/h/h3615.md) [bālâ](../../strongs/h/h1086.md) their [yowm](../../strongs/h/h3117.md) in [towb](../../strongs/h/h2896.md), and in a [reḡaʿ](../../strongs/h/h7281.md) go [ḥāṯaṯ](../../strongs/h/h2865.md) [nāḥaṯ](../../strongs/h/h5181.md) to the [shĕ'owl](../../strongs/h/h7585.md).

<a name="job_21_14"></a>Job 21:14

Therefore they ['āmar](../../strongs/h/h559.md) unto ['el](../../strongs/h/h410.md), [cuwr](../../strongs/h/h5493.md) from us; for we [ḥāp̄ēṣ](../../strongs/h/h2654.md) not the [da'ath](../../strongs/h/h1847.md) of thy [derek](../../strongs/h/h1870.md).

<a name="job_21_15"></a>Job 21:15

What is the [Šaday](../../strongs/h/h7706.md), that we should ['abad](../../strongs/h/h5647.md) him? and what [yāʿal](../../strongs/h/h3276.md) should we have, if we [pāḡaʿ](../../strongs/h/h6293.md) unto him?

<a name="job_21_16"></a>Job 21:16

Lo, their [ṭûḇ](../../strongs/h/h2898.md) is not in their [yad](../../strongs/h/h3027.md): the ['etsah](../../strongs/h/h6098.md) of the [rasha'](../../strongs/h/h7563.md) is [rachaq](../../strongs/h/h7368.md) from me.

<a name="job_21_17"></a>Job 21:17

How oft is the [nîr](../../strongs/h/h5216.md) of the [rasha'](../../strongs/h/h7563.md) put [dāʿaḵ](../../strongs/h/h1846.md)! and how oft [bow'](../../strongs/h/h935.md) their ['êḏ](../../strongs/h/h343.md) upon them! [chalaq](../../strongs/h/h2505.md) [chebel](../../strongs/h/h2256.md) in his ['aph](../../strongs/h/h639.md).

<a name="job_21_18"></a>Job 21:18

They are as [teḇen](../../strongs/h/h8401.md) [paniym](../../strongs/h/h6440.md) the [ruwach](../../strongs/h/h7307.md), and as [mots](../../strongs/h/h4671.md) that the [sûp̄â](../../strongs/h/h5492.md) [ganab](../../strongs/h/h1589.md).

<a name="job_21_19"></a>Job 21:19

['ĕlvôha](../../strongs/h/h433.md) [tsaphan](../../strongs/h/h6845.md) his ['aven](../../strongs/h/h205.md) for his [ben](../../strongs/h/h1121.md): he [shalam](../../strongs/h/h7999.md) him, and he shall [yada'](../../strongs/h/h3045.md) it.

<a name="job_21_20"></a>Job 21:20

His ['ayin](../../strongs/h/h5869.md) shall [ra'ah](../../strongs/h/h7200.md) his [kîḏ](../../strongs/h/h3589.md), and he shall [šāṯâ](../../strongs/h/h8354.md) of the [chemah](../../strongs/h/h2534.md) of the [Šaday](../../strongs/h/h7706.md).

<a name="job_21_21"></a>Job 21:21

For what [chephets](../../strongs/h/h2656.md) hath he in his [bayith](../../strongs/h/h1004.md) ['aḥar](../../strongs/h/h310.md) him, when the [mispār](../../strongs/h/h4557.md) of his [ḥōḏeš](../../strongs/h/h2320.md) is cut off in the [ḥāṣaṣ](../../strongs/h/h2686.md)?

<a name="job_21_22"></a>Job 21:22

Shall any [lamad](../../strongs/h/h3925.md) ['el](../../strongs/h/h410.md) [da'ath](../../strongs/h/h1847.md)? seeing he [shaphat](../../strongs/h/h8199.md) those that are [ruwm](../../strongs/h/h7311.md).

<a name="job_21_23"></a>Job 21:23

One [muwth](../../strongs/h/h4191.md) in his [tom](../../strongs/h/h8537.md) ['etsem](../../strongs/h/h6106.md), being wholly at [šal'ănān](../../strongs/h/h7946.md) and [šālēv](../../strongs/h/h7961.md).

<a name="job_21_24"></a>Job 21:24

His [ʿăṭîn](../../strongs/h/h5845.md) are [mālā'](../../strongs/h/h4390.md) of [chalab](../../strongs/h/h2461.md), and his ['etsem](../../strongs/h/h6106.md) are [šāqâ](../../strongs/h/h8248.md) with [mōaḥ](../../strongs/h/h4221.md).

<a name="job_21_25"></a>Job 21:25

And another [muwth](../../strongs/h/h4191.md) in the [mar](../../strongs/h/h4751.md) of his [nephesh](../../strongs/h/h5315.md), and never ['akal](../../strongs/h/h398.md) with [towb](../../strongs/h/h2896.md).

<a name="job_21_26"></a>Job 21:26

They shall [shakab](../../strongs/h/h7901.md) [yaḥaḏ](../../strongs/h/h3162.md) in the ['aphar](../../strongs/h/h6083.md), and the [rimmâ](../../strongs/h/h7415.md) shall [kāsâ](../../strongs/h/h3680.md) them.

<a name="job_21_27"></a>Job 21:27

Behold, I [yada'](../../strongs/h/h3045.md) your [maḥăšāḇâ](../../strongs/h/h4284.md), and the [mezimmah](../../strongs/h/h4209.md) which ye wrongfully [ḥāmas](../../strongs/h/h2554.md) against me.

<a name="job_21_28"></a>Job 21:28

For ye ['āmar](../../strongs/h/h559.md), Where is the [bayith](../../strongs/h/h1004.md) of the [nāḏîḇ](../../strongs/h/h5081.md)? and where are the [miškān](../../strongs/h/h4908.md) ['ohel](../../strongs/h/h168.md) of the [rasha'](../../strongs/h/h7563.md)?

<a name="job_21_29"></a>Job 21:29

Have ye not [sha'al](../../strongs/h/h7592.md) them that ['abar](../../strongs/h/h5674.md) by the [derek](../../strongs/h/h1870.md)? and do ye not [nāḵar](../../strongs/h/h5234.md) their ['ôṯ](../../strongs/h/h226.md),

<a name="job_21_30"></a>Job 21:30

That the [ra'](../../strongs/h/h7451.md) is [ḥāśaḵ](../../strongs/h/h2820.md) to the [yowm](../../strongs/h/h3117.md) of ['êḏ](../../strongs/h/h343.md)? they shall be brought [yāḇal](../../strongs/h/h2986.md) to the [yowm](../../strongs/h/h3117.md) of ['ebrah](../../strongs/h/h5678.md).

<a name="job_21_31"></a>Job 21:31

Who shall [nāḡaḏ](../../strongs/h/h5046.md) his [derek](../../strongs/h/h1870.md) to his [paniym](../../strongs/h/h6440.md)? and who shall [shalam](../../strongs/h/h7999.md) him what he hath ['asah](../../strongs/h/h6213.md)?

<a name="job_21_32"></a>Job 21:32

Yet shall he be [yāḇal](../../strongs/h/h2986.md) to the [qeber](../../strongs/h/h6913.md), and shall [šāqaḏ](../../strongs/h/h8245.md) in the [gāḏîš](../../strongs/h/h1430.md).

<a name="job_21_33"></a>Job 21:33

The [reḡeḇ](../../strongs/h/h7263.md) of the [nachal](../../strongs/h/h5158.md) shall be [māṯaq](../../strongs/h/h4985.md) unto him, and every ['āḏām](../../strongs/h/h120.md) shall [mashak](../../strongs/h/h4900.md) ['aḥar](../../strongs/h/h310.md) him, as there are [mispār](../../strongs/h/h4557.md) [paniym](../../strongs/h/h6440.md) him.

<a name="job_21_34"></a>Job 21:34

How then [nacham](../../strongs/h/h5162.md) ye me in [heḇel](../../strongs/h/h1892.md), seeing in your [tᵊšûḇâ](../../strongs/h/h8666.md) there [šā'ar](../../strongs/h/h7604.md) [maʿal](../../strongs/h/h4604.md)?

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 20](job_20.md) - [Job 22](job_22.md)