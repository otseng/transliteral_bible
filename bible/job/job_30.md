# [Job 30](https://www.blueletterbible.org/kjv/job/30)

<a name="job_30_1"></a>Job 30:1

But now they that are [ṣāʿîr](../../strongs/h/h6810.md) [yowm](../../strongs/h/h3117.md) than I have me in [śāḥaq](../../strongs/h/h7832.md), whose ['ab](../../strongs/h/h1.md) I would have [mā'as](../../strongs/h/h3988.md) to have [shiyth](../../strongs/h/h7896.md) with the [keleḇ](../../strongs/h/h3611.md) of my [tso'n](../../strongs/h/h6629.md).

<a name="job_30_2"></a>Job 30:2

Yea, whereto might the [koach](../../strongs/h/h3581.md) of their [yad](../../strongs/h/h3027.md) profit me, in whom [kelaḥ](../../strongs/h/h3624.md) was ['abad](../../strongs/h/h6.md)?

<a name="job_30_3"></a>Job 30:3

For [ḥeser](../../strongs/h/h2639.md) and [kāp̄ān](../../strongs/h/h3720.md) they were [galmûḏ](../../strongs/h/h1565.md); [ʿāraq](../../strongs/h/h6207.md) into the [ṣîyâ](../../strongs/h/h6723.md) in former ['emeš](../../strongs/h/h570.md) [šô'](../../strongs/h/h7722.md) and [mᵊšô'â](../../strongs/h/h4875.md).

<a name="job_30_4"></a>Job 30:4

Who [qāṭap̄](../../strongs/h/h6998.md) [mallûaḥ](../../strongs/h/h4408.md) by the [śîaḥ](../../strongs/h/h7880.md), and [rōṯem](../../strongs/h/h7574.md) [šereš](../../strongs/h/h8328.md) for their [lechem](../../strongs/h/h3899.md).

<a name="job_30_5"></a>Job 30:5

They were [gāraš](../../strongs/h/h1644.md) from [gēv](../../strongs/h/h1460.md) men, (they [rûaʿ](../../strongs/h/h7321.md) after them as after a [gannāḇ](../../strongs/h/h1590.md);)

<a name="job_30_6"></a>Job 30:6

To [shakan](../../strongs/h/h7931.md) in the [ʿārûṣ](../../strongs/h/h6178.md) of the [nachal](../../strongs/h/h5158.md), in [ḥôr](../../strongs/h/h2356.md) of the ['aphar](../../strongs/h/h6083.md), and in the [kᵊp̄](../../strongs/h/h3710.md).

<a name="job_30_7"></a>Job 30:7

Among the [śîaḥ](../../strongs/h/h7880.md) they [nāhaq](../../strongs/h/h5101.md); under the [ḥārûl](../../strongs/h/h2738.md) they were [sāp̄aḥ](../../strongs/h/h5596.md).

<a name="job_30_8"></a>Job 30:8

They were [ben](../../strongs/h/h1121.md) of [nabal](../../strongs/h/h5036.md), yea, [ben](../../strongs/h/h1121.md) of [shem](../../strongs/h/h8034.md): they were [nāḵā'](../../strongs/h/h5217.md) than the ['erets](../../strongs/h/h776.md).

<a name="job_30_9"></a>Job 30:9

And now am I their [Nᵊḡînâ](../../strongs/h/h5058.md), yea, I am their [millâ](../../strongs/h/h4405.md).

<a name="job_30_10"></a>Job 30:10

They [ta'ab](../../strongs/h/h8581.md) me, they [rachaq](../../strongs/h/h7368.md) from me, and [ḥāśaḵ](../../strongs/h/h2820.md) not to [rōq](../../strongs/h/h7536.md) in my [paniym](../../strongs/h/h6440.md).

<a name="job_30_11"></a>Job 30:11

Because he hath [pāṯaḥ](../../strongs/h/h6605.md) my [yeṯer](../../strongs/h/h3499.md), and [ʿānâ](../../strongs/h/h6031.md) me, they have also let [shalach](../../strongs/h/h7971.md) the [resen](../../strongs/h/h7448.md) [paniym](../../strongs/h/h6440.md) me.

<a name="job_30_12"></a>Job 30:12

Upon my [yamiyn](../../strongs/h/h3225.md) hand [quwm](../../strongs/h/h6965.md) the [pirḥāḥ](../../strongs/h/h6526.md); they push [shalach](../../strongs/h/h7971.md) my [regel](../../strongs/h/h7272.md), and they [sālal](../../strongs/h/h5549.md) against me the ['orach](../../strongs/h/h734.md) of their ['êḏ](../../strongs/h/h343.md).

<a name="job_30_13"></a>Job 30:13

They [nāṯas](../../strongs/h/h5420.md) my [nāṯîḇ](../../strongs/h/h5410.md), they [yāʿal](../../strongs/h/h3276.md) my [havvah](../../strongs/h/h1942.md) [hayyâ](../../strongs/h/h1962.md), they have no [ʿāzar](../../strongs/h/h5826.md).

<a name="job_30_14"></a>Job 30:14

They ['āṯâ](../../strongs/h/h857.md) upon me as a [rāḥāḇ](../../strongs/h/h7342.md) [pereṣ](../../strongs/h/h6556.md): in the [šô'](../../strongs/h/h7722.md) they [gālal](../../strongs/h/h1556.md) themselves upon me.

<a name="job_30_15"></a>Job 30:15

[ballāhâ](../../strongs/h/h1091.md) are [hāp̄aḵ](../../strongs/h/h2015.md) upon me: they [radaph](../../strongs/h/h7291.md) my [nᵊḏîḇâ](../../strongs/h/h5082.md) as the [ruwach](../../strongs/h/h7307.md): and my [yĕshuw'ah](../../strongs/h/h3444.md) ['abar](../../strongs/h/h5674.md) as an ['ab](../../strongs/h/h5645.md).

<a name="job_30_16"></a>Job 30:16

And now my [nephesh](../../strongs/h/h5315.md) is [šāp̄aḵ](../../strongs/h/h8210.md) upon me; the [yowm](../../strongs/h/h3117.md) of ['oniy](../../strongs/h/h6040.md) have ['āḥaz](../../strongs/h/h270.md) upon me.

<a name="job_30_17"></a>Job 30:17

My ['etsem](../../strongs/h/h6106.md) are [nāqar](../../strongs/h/h5365.md) in me in the [layil](../../strongs/h/h3915.md): and my [ʿāraq](../../strongs/h/h6207.md) take no [shakab](../../strongs/h/h7901.md).

<a name="job_30_18"></a>Job 30:18

By the [rōḇ](../../strongs/h/h7230.md) [koach](../../strongs/h/h3581.md) of my disease is my [lᵊḇûš](../../strongs/h/h3830.md) [ḥāp̄aś](../../strongs/h/h2664.md): it ['āzar](../../strongs/h/h247.md) me as the [peh](../../strongs/h/h6310.md) of my [kĕthoneth](../../strongs/h/h3801.md).

<a name="job_30_19"></a>Job 30:19

He hath [yārâ](../../strongs/h/h3384.md) me into the [ḥōmer](../../strongs/h/h2563.md), and I am become [māšal](../../strongs/h/h4911.md) ['aphar](../../strongs/h/h6083.md) and ['ēp̄er](../../strongs/h/h665.md).

<a name="job_30_20"></a>Job 30:20

I [šāvaʿ](../../strongs/h/h7768.md) unto thee, and thou dost not ['anah](../../strongs/h/h6030.md) me: I ['amad](../../strongs/h/h5975.md), and thou [bîn](../../strongs/h/h995.md) me not.

<a name="job_30_21"></a>Job 30:21

Thou art [hāp̄aḵ](../../strongs/h/h2015.md) ['aḵzār](../../strongs/h/h393.md) to me: with thy [ʿōṣem](../../strongs/h/h6108.md) [yad](../../strongs/h/h3027.md) thou [śāṭam](../../strongs/h/h7852.md) thyself against me.

<a name="job_30_22"></a>Job 30:22

Thou [nasa'](../../strongs/h/h5375.md) me to the [ruwach](../../strongs/h/h7307.md); thou causest me to [rāḵaḇ](../../strongs/h/h7392.md) upon it, and [mûḡ](../../strongs/h/h4127.md) my [tûšîyâ](../../strongs/h/h8454.md) H7738.

<a name="job_30_23"></a>Job 30:23

For I [yada'](../../strongs/h/h3045.md) that thou wilt [shuwb](../../strongs/h/h7725.md) me to [maveth](../../strongs/h/h4194.md), and to the [bayith](../../strongs/h/h1004.md) [môʿēḏ](../../strongs/h/h4150.md) for all [chay](../../strongs/h/h2416.md).

<a name="job_30_24"></a>Job 30:24

Howbeit he will not [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) to the [bᵊʿî](../../strongs/h/h1164.md), though they [šôaʿ](../../strongs/h/h7769.md) in his [pîḏ](../../strongs/h/h6365.md).

<a name="job_30_25"></a>Job 30:25

Did not I [bāḵâ](../../strongs/h/h1058.md) for him that was in [qāšê](../../strongs/h/h7186.md) [yowm](../../strongs/h/h3117.md)? was not my [nephesh](../../strongs/h/h5315.md) [ʿāḡam](../../strongs/h/h5701.md) for the ['ebyown](../../strongs/h/h34.md)?

<a name="job_30_26"></a>Job 30:26

When I [qāvâ](../../strongs/h/h6960.md) for [towb](../../strongs/h/h2896.md), then [ra'](../../strongs/h/h7451.md) [bow'](../../strongs/h/h935.md) unto me: and when I [yāḥal](../../strongs/h/h3176.md) for ['owr](../../strongs/h/h216.md), there [bow'](../../strongs/h/h935.md) ['ōp̄el](../../strongs/h/h652.md).

<a name="job_30_27"></a>Job 30:27

My [me'ah](../../strongs/h/h4578.md) [rāṯaḥ](../../strongs/h/h7570.md), and [damam](../../strongs/h/h1826.md) not: the [yowm](../../strongs/h/h3117.md) of ['oniy](../../strongs/h/h6040.md) [qadam](../../strongs/h/h6923.md) me.

<a name="job_30_28"></a>Job 30:28

I [halak](../../strongs/h/h1980.md) [qāḏar](../../strongs/h/h6937.md) without the [ḥammâ](../../strongs/h/h2535.md): I [quwm](../../strongs/h/h6965.md), and I [šāvaʿ](../../strongs/h/h7768.md) in the [qāhēl](../../strongs/h/h6951.md).

<a name="job_30_29"></a>Job 30:29

I am an ['ach](../../strongs/h/h251.md) to [tannîn](../../strongs/h/h8577.md), and a [rea'](../../strongs/h/h7453.md) to [bath](../../strongs/h/h1323.md) [yaʿănâ](../../strongs/h/h3284.md).

<a name="job_30_30"></a>Job 30:30

My ['owr](../../strongs/h/h5785.md) is [šāḥar](../../strongs/h/h7835.md) upon me, and my ['etsem](../../strongs/h/h6106.md) are [ḥārar](../../strongs/h/h2787.md) with [ḥōreḇ](../../strongs/h/h2721.md).

<a name="job_30_31"></a>Job 30:31

My [kinnôr](../../strongs/h/h3658.md) also is turned to ['ēḇel](../../strongs/h/h60.md), and my [ʿûḡāḇ](../../strongs/h/h5748.md) into the [qowl](../../strongs/h/h6963.md) of them that [bāḵâ](../../strongs/h/h1058.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 29](job_29.md) - [Job 31](job_31.md)