# [Job 36](https://www.blueletterbible.org/kjv/job/36)

<a name="job_36_1"></a>Job 36:1

['Ĕlîhû](../../strongs/h/h453.md) also [yāsap̄](../../strongs/h/h3254.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_36_2"></a>Job 36:2

[kāṯar](../../strongs/h/h3803.md) me a [zᵊʿêr](../../strongs/h/h2191.md), and I will [ḥāvâ](../../strongs/h/h2331.md) thee that I have yet to [millâ](../../strongs/h/h4405.md) on ['ĕlvôha](../../strongs/h/h433.md) behalf.

<a name="job_36_3"></a>Job 36:3

I will [nasa'](../../strongs/h/h5375.md) my [dēaʿ](../../strongs/h/h1843.md) from [rachowq](../../strongs/h/h7350.md), and will [nathan](../../strongs/h/h5414.md) [tsedeq](../../strongs/h/h6664.md) to my [pa'al](../../strongs/h/h6466.md).

<a name="job_36_4"></a>Job 36:4

For ['āmnām](../../strongs/h/h551.md) my [millâ](../../strongs/h/h4405.md) shall not be [sheqer](../../strongs/h/h8267.md): he that is [tamiym](../../strongs/h/h8549.md) in [dēʿâ](../../strongs/h/h1844.md) is with thee.

<a name="job_36_5"></a>Job 36:5

Behold, ['el](../../strongs/h/h410.md) is [kabîr](../../strongs/h/h3524.md), and [mā'as](../../strongs/h/h3988.md) not any: he is [kabîr](../../strongs/h/h3524.md) in [koach](../../strongs/h/h3581.md) and [leb](../../strongs/h/h3820.md).

<a name="job_36_6"></a>Job 36:6

He preserveth not the [ḥāyâ](../../strongs/h/h2421.md) of the [rasha'](../../strongs/h/h7563.md): but [nathan](../../strongs/h/h5414.md) [mishpat](../../strongs/h/h4941.md) to the ['aniy](../../strongs/h/h6041.md).

<a name="job_36_7"></a>Job 36:7

He [gāraʿ](../../strongs/h/h1639.md) not his ['ayin](../../strongs/h/h5869.md) from the [tsaddiyq](../../strongs/h/h6662.md): but with [melek](../../strongs/h/h4428.md) are they on the [kicce'](../../strongs/h/h3678.md); yea, he doth [yashab](../../strongs/h/h3427.md) them for [netsach](../../strongs/h/h5331.md), and they are [gāḇah](../../strongs/h/h1361.md).

<a name="job_36_8"></a>Job 36:8

And if they be ['āsar](../../strongs/h/h631.md) in [zîqôṯ](../../strongs/h/h2131.md), and be [lāḵaḏ](../../strongs/h/h3920.md) in [chebel](../../strongs/h/h2256.md) of ['oniy](../../strongs/h/h6040.md);

<a name="job_36_9"></a>Job 36:9

Then he [nāḡaḏ](../../strongs/h/h5046.md) them their [pōʿal](../../strongs/h/h6467.md), and their [pesha'](../../strongs/h/h6588.md) that they have [gabar](../../strongs/h/h1396.md).

<a name="job_36_10"></a>Job 36:10

He [gālâ](../../strongs/h/h1540.md) also their ['ozen](../../strongs/h/h241.md) to [mûsār](../../strongs/h/h4148.md), and ['āmar](../../strongs/h/h559.md) that they [shuwb](../../strongs/h/h7725.md) from ['aven](../../strongs/h/h205.md).

<a name="job_36_11"></a>Job 36:11

If they [shama'](../../strongs/h/h8085.md) and ['abad](../../strongs/h/h5647.md) him, they shall [kalah](../../strongs/h/h3615.md) their [yowm](../../strongs/h/h3117.md) in [towb](../../strongs/h/h2896.md), and their [šānâ](../../strongs/h/h8141.md) in [na'iym](../../strongs/h/h5273.md).

<a name="job_36_12"></a>Job 36:12

But if they [shama'](../../strongs/h/h8085.md) not, they shall ['abar](../../strongs/h/h5674.md) by the [Šelaḥ](../../strongs/h/h7973.md), and they shall [gāvaʿ](../../strongs/h/h1478.md) without [da'ath](../../strongs/h/h1847.md).

<a name="job_36_13"></a>Job 36:13

But the [ḥānēp̄](../../strongs/h/h2611.md) in [leb](../../strongs/h/h3820.md) [śûm](../../strongs/h/h7760.md) ['aph](../../strongs/h/h639.md): they [šāvaʿ](../../strongs/h/h7768.md) not when he ['āsar](../../strongs/h/h631.md) them.

<a name="job_36_14"></a>Job 36:14

[nephesh](../../strongs/h/h5315.md) [muwth](../../strongs/h/h4191.md) in [nōʿar](../../strongs/h/h5290.md), and their [chay](../../strongs/h/h2416.md) is among the [qāḏēš](../../strongs/h/h6945.md).

<a name="job_36_15"></a>Job 36:15

He [chalats](../../strongs/h/h2502.md) the ['aniy](../../strongs/h/h6041.md) in his ['oniy](../../strongs/h/h6040.md), and [gālâ](../../strongs/h/h1540.md) their ['ozen](../../strongs/h/h241.md) in [laḥaṣ](../../strongs/h/h3906.md).

<a name="job_36_16"></a>Job 36:16

Even so would he have [sûṯ](../../strongs/h/h5496.md) thee out of the [peh](../../strongs/h/h6310.md) [tsar](../../strongs/h/h6862.md) into a [raḥaḇ](../../strongs/h/h7338.md), where there is no [mûṣaq](../../strongs/h/h4164.md); and that which should be [naḥaṯ](../../strongs/h/h5183.md) on thy [šulḥān](../../strongs/h/h7979.md) should be [mālā'](../../strongs/h/h4390.md) of [dešen](../../strongs/h/h1880.md).

<a name="job_36_17"></a>Job 36:17

But thou hast [mālā'](../../strongs/h/h4390.md) the [diyn](../../strongs/h/h1779.md) of the [rasha'](../../strongs/h/h7563.md): [diyn](../../strongs/h/h1779.md) and [mishpat](../../strongs/h/h4941.md) take [tamak](../../strongs/h/h8551.md) on thee.

<a name="job_36_18"></a>Job 36:18

Because there is [chemah](../../strongs/h/h2534.md), beware lest he take thee [sûṯ](../../strongs/h/h5496.md) with his [sēp̄eq](../../strongs/h/h5607.md): then a [rōḇ](../../strongs/h/h7230.md) [kōp̄er](../../strongs/h/h3724.md) cannot [natah](../../strongs/h/h5186.md) thee.

<a name="job_36_19"></a>Job 36:19

Will he ['arak](../../strongs/h/h6186.md) thy [šôaʿ](../../strongs/h/h7769.md)? no, not [bᵊṣar](../../strongs/h/h1222.md), nor all the [ma'ămāṣ](../../strongs/h/h3981.md) of [koach](../../strongs/h/h3581.md).

<a name="job_36_20"></a>Job 36:20

[šā'ap̄](../../strongs/h/h7602.md) not the [layil](../../strongs/h/h3915.md), when ['am](../../strongs/h/h5971.md) are [ʿālâ](../../strongs/h/h5927.md) in their place.

<a name="job_36_21"></a>Job 36:21

[shamar](../../strongs/h/h8104.md), [panah](../../strongs/h/h6437.md) not ['aven](../../strongs/h/h205.md): for this hast thou [bāḥar](../../strongs/h/h977.md) rather than ['oniy](../../strongs/h/h6040.md).

<a name="job_36_22"></a>Job 36:22

Behold, ['el](../../strongs/h/h410.md) [śāḡaḇ](../../strongs/h/h7682.md) by his [koach](../../strongs/h/h3581.md): who [yārâ](../../strongs/h/h3384.md) like him?

<a name="job_36_23"></a>Job 36:23

Who hath [paqad](../../strongs/h/h6485.md) him his [derek](../../strongs/h/h1870.md)? or who can ['āmar](../../strongs/h/h559.md), Thou hast [pa'al](../../strongs/h/h6466.md) ['evel](../../strongs/h/h5766.md)?

<a name="job_36_24"></a>Job 36:24

[zakar](../../strongs/h/h2142.md) that thou [śāḡā'](../../strongs/h/h7679.md) his [pōʿal](../../strongs/h/h6467.md), which ['enowsh](../../strongs/h/h582.md) [shiyr](../../strongs/h/h7891.md).

<a name="job_36_25"></a>Job 36:25

Every ['āḏām](../../strongs/h/h120.md) may [chazah](../../strongs/h/h2372.md) it; ['enowsh](../../strongs/h/h582.md) may [nabat](../../strongs/h/h5027.md) it afar [rachowq](../../strongs/h/h7350.md).

<a name="job_36_26"></a>Job 36:26

Behold, ['el](../../strongs/h/h410.md) is [śagî'](../../strongs/h/h7689.md), and we [yada'](../../strongs/h/h3045.md) him not, neither can the [mispār](../../strongs/h/h4557.md) of his [šānâ](../../strongs/h/h8141.md) be [ḥēqer](../../strongs/h/h2714.md).

<a name="job_36_27"></a>Job 36:27

For he maketh [gāraʿ](../../strongs/h/h1639.md) the [nāṭāp̄](../../strongs/h/h5198.md) of [mayim](../../strongs/h/h4325.md): they [zaqaq](../../strongs/h/h2212.md) [māṭār](../../strongs/h/h4306.md) according to the ['ēḏ](../../strongs/h/h108.md) thereof:

<a name="job_36_28"></a>Job 36:28

Which the [shachaq](../../strongs/h/h7834.md) do [nāzal](../../strongs/h/h5140.md) and [rāʿap̄](../../strongs/h/h7491.md) upon ['āḏām](../../strongs/h/h120.md) [rab](../../strongs/h/h7227.md).

<a name="job_36_29"></a>Job 36:29

Also can any [bîn](../../strongs/h/h995.md) the [mip̄rāś](../../strongs/h/h4666.md) of the ['ab](../../strongs/h/h5645.md), or the [tᵊšu'â](../../strongs/h/h8663.md) of his [cukkah](../../strongs/h/h5521.md)?

<a name="job_36_30"></a>Job 36:30

Behold, he [pāraś](../../strongs/h/h6566.md) his ['owr](../../strongs/h/h216.md) upon it, and [kāsâ](../../strongs/h/h3680.md) the [šereš](../../strongs/h/h8328.md) of the [yam](../../strongs/h/h3220.md).

<a name="job_36_31"></a>Job 36:31

For by them [diyn](../../strongs/h/h1777.md) he the ['am](../../strongs/h/h5971.md); he [nathan](../../strongs/h/h5414.md) ['ōḵel](../../strongs/h/h400.md) in [maḵbîr](../../strongs/h/h4342.md).

<a name="job_36_32"></a>Job 36:32

With [kaph](../../strongs/h/h3709.md) he [kāsâ](../../strongs/h/h3680.md) the ['owr](../../strongs/h/h216.md); and [tsavah](../../strongs/h/h6680.md) it not to shine by the cloud that cometh [pāḡaʿ](../../strongs/h/h6293.md).

<a name="job_36_33"></a>Job 36:33

The [rēaʿ](../../strongs/h/h7452.md) thereof [nāḡaḏ](../../strongs/h/h5046.md) concerning it, the [miqnê](../../strongs/h/h4735.md) also concerning the [ʿālâ](../../strongs/h/h5927.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 35](job_35.md) - [Job 37](job_37.md)