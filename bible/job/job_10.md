# [Job 10](https://www.blueletterbible.org/kjv/job/10)

<a name="job_10_1"></a>Job 10:1

My [nephesh](../../strongs/h/h5315.md) is [nāqaṭ](../../strongs/h/h5354.md) of my [chay](../../strongs/h/h2416.md); I will ['azab](../../strongs/h/h5800.md) my [śîaḥ](../../strongs/h/h7879.md) upon myself; I will [dabar](../../strongs/h/h1696.md) in the [mar](../../strongs/h/h4751.md) of my [nephesh](../../strongs/h/h5315.md).

<a name="job_10_2"></a>Job 10:2

I will ['āmar](../../strongs/h/h559.md) unto ['ĕlvôha](../../strongs/h/h433.md), Do not [rāšaʿ](../../strongs/h/h7561.md) me; [yada'](../../strongs/h/h3045.md) me wherefore thou [riyb](../../strongs/h/h7378.md) with me.

<a name="job_10_3"></a>Job 10:3

Is it [ṭôḇ](../../strongs/h/h2895.md) unto thee that thou shouldest [ʿāšaq](../../strongs/h/h6231.md), that thou shouldest [mā'as](../../strongs/h/h3988.md) the [yᵊḡîaʿ](../../strongs/h/h3018.md) of thine [kaph](../../strongs/h/h3709.md), and [yāp̄aʿ](../../strongs/h/h3313.md) upon the ['etsah](../../strongs/h/h6098.md) of the [rasha'](../../strongs/h/h7563.md)?

<a name="job_10_4"></a>Job 10:4

Hast thou ['ayin](../../strongs/h/h5869.md) of [basar](../../strongs/h/h1320.md)? or [ra'ah](../../strongs/h/h7200.md) thou as ['enowsh](../../strongs/h/h582.md) [ra'ah](../../strongs/h/h7200.md)?

<a name="job_10_5"></a>Job 10:5

Are thy [yowm](../../strongs/h/h3117.md) as the [yowm](../../strongs/h/h3117.md) of ['enowsh](../../strongs/h/h582.md)? are thy [šānâ](../../strongs/h/h8141.md) as [geḇer](../../strongs/h/h1397.md) [yowm](../../strongs/h/h3117.md),

<a name="job_10_6"></a>Job 10:6

That thou [bāqaš](../../strongs/h/h1245.md) after mine ['avon](../../strongs/h/h5771.md), and [darash](../../strongs/h/h1875.md) after my [chatta'ath](../../strongs/h/h2403.md)?

<a name="job_10_7"></a>Job 10:7

Thou [da'ath](../../strongs/h/h1847.md) that I am not [rāšaʿ](../../strongs/h/h7561.md); and there is none that can [natsal](../../strongs/h/h5337.md) out of thine [yad](../../strongs/h/h3027.md).

<a name="job_10_8"></a>Job 10:8

Thine [yad](../../strongs/h/h3027.md) have [ʿāṣaḇ](../../strongs/h/h6087.md) me and ['asah](../../strongs/h/h6213.md) me [yaḥaḏ](../../strongs/h/h3162.md) [cabiyb](../../strongs/h/h5439.md); yet thou dost [bālaʿ](../../strongs/h/h1104.md) me.

<a name="job_10_9"></a>Job 10:9

[zakar](../../strongs/h/h2142.md), I beseech thee, that thou hast ['asah](../../strongs/h/h6213.md) me as the [ḥōmer](../../strongs/h/h2563.md); and wilt thou [shuwb](../../strongs/h/h7725.md) me into ['aphar](../../strongs/h/h6083.md) [shuwb](../../strongs/h/h7725.md)?

<a name="job_10_10"></a>Job 10:10

Hast thou not [nāṯaḵ](../../strongs/h/h5413.md) me as [chalab](../../strongs/h/h2461.md), and [qāp̄ā'](../../strongs/h/h7087.md) me like [gᵊḇînâ](../../strongs/h/h1385.md)?

<a name="job_10_11"></a>Job 10:11

Thou hast [labash](../../strongs/h/h3847.md) me with ['owr](../../strongs/h/h5785.md) and [basar](../../strongs/h/h1320.md), and hast [śûḵ](../../strongs/h/h7753.md) me with ['etsem](../../strongs/h/h6106.md) and [gîḏ](../../strongs/h/h1517.md).

<a name="job_10_12"></a>Job 10:12

Thou hast ['asah](../../strongs/h/h6213.md) me [chay](../../strongs/h/h2416.md) and [checed](../../strongs/h/h2617.md), and thy [pᵊqudâ](../../strongs/h/h6486.md) hath [shamar](../../strongs/h/h8104.md) my [ruwach](../../strongs/h/h7307.md).

<a name="job_10_13"></a>Job 10:13

And these things hast thou [tsaphan](../../strongs/h/h6845.md) in thine [lebab](../../strongs/h/h3824.md): I [yada'](../../strongs/h/h3045.md) that this is with thee.

<a name="job_10_14"></a>Job 10:14

If I [chata'](../../strongs/h/h2398.md), then thou [shamar](../../strongs/h/h8104.md) me, and thou wilt not [naqah](../../strongs/h/h5352.md) me from mine ['avon](../../strongs/h/h5771.md).

<a name="job_10_15"></a>Job 10:15

If I be [rāšaʿ](../../strongs/h/h7561.md), ['allay](../../strongs/h/h480.md) unto me; and if I be [ṣāḏaq](../../strongs/h/h6663.md), yet will I not [nasa'](../../strongs/h/h5375.md) my [ro'sh](../../strongs/h/h7218.md). I am [śāḇēaʿ](../../strongs/h/h7649.md) of [qālôn](../../strongs/h/h7036.md); therefore [rā'ê](../../strongs/h/h7202.md) [ra'ah](../../strongs/h/h7200.md) thou mine ['oniy](../../strongs/h/h6040.md);

<a name="job_10_16"></a>Job 10:16

For it [gā'â](../../strongs/h/h1342.md). Thou [ṣûḏ](../../strongs/h/h6679.md) me as a [šāḥal](../../strongs/h/h7826.md): and [shuwb](../../strongs/h/h7725.md) thou shewest thyself [pala'](../../strongs/h/h6381.md) upon me.

<a name="job_10_17"></a>Job 10:17

Thou [ḥādaš](../../strongs/h/h2318.md) thy ['ed](../../strongs/h/h5707.md) against me, and [rabah](../../strongs/h/h7235.md) thine [ka'ac](../../strongs/h/h3708.md) upon me; [ḥălîp̄â](../../strongs/h/h2487.md) and [tsaba'](../../strongs/h/h6635.md) are against me.

<a name="job_10_18"></a>Job 10:18

Wherefore then hast thou [yāṣā'](../../strongs/h/h3318.md) me out of the [reḥem](../../strongs/h/h7358.md)? Oh that I had given up the [gāvaʿ](../../strongs/h/h1478.md), and no ['ayin](../../strongs/h/h5869.md) had [ra'ah](../../strongs/h/h7200.md) me!

<a name="job_10_19"></a>Job 10:19

I should have been as though I had not been; I should have been [yāḇal](../../strongs/h/h2986.md) from the [beten](../../strongs/h/h990.md) to the [qeber](../../strongs/h/h6913.md).

<a name="job_10_20"></a>Job 10:20

Are not my [yowm](../../strongs/h/h3117.md) [mᵊʿaṭ](../../strongs/h/h4592.md)? [ḥāḏal](../../strongs/h/h2308.md) [ḥāḏal](../../strongs/h/h2308.md) then, and let me [shiyth](../../strongs/h/h7896.md) [shiyth](../../strongs/h/h7896.md), that I may take [bālaḡ](../../strongs/h/h1082.md) a [mᵊʿaṭ](../../strongs/h/h4592.md),

<a name="job_10_21"></a>Job 10:21

Before I [yālaḵ](../../strongs/h/h3212.md) whence I shall not [shuwb](../../strongs/h/h7725.md), even to the ['erets](../../strongs/h/h776.md) of [choshek](../../strongs/h/h2822.md) and the [ṣalmāveṯ](../../strongs/h/h6757.md);

<a name="job_10_22"></a>Job 10:22

An ['erets](../../strongs/h/h776.md) of [ʿêp̄â](../../strongs/h/h5890.md), as ['ōp̄el](../../strongs/h/h652.md) itself; and of the [ṣalmāveṯ](../../strongs/h/h6757.md), without any [seḏer](../../strongs/h/h5468.md), and where the [yāp̄aʿ](../../strongs/h/h3313.md) is as ['ōp̄el](../../strongs/h/h652.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 9](job_9.md) - [Job 11](job_11.md)