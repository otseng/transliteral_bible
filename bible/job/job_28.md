# [Job 28](https://www.blueletterbible.org/kjv/job/28)

<a name="job_28_1"></a>Job 28:1

[yēš](../../strongs/h/h3426.md) there is a [môṣā'](../../strongs/h/h4161.md) for the [keceph](../../strongs/h/h3701.md), and a [maqowm](../../strongs/h/h4725.md) for [zāhāḇ](../../strongs/h/h2091.md) where they [zaqaq](../../strongs/h/h2212.md) it.

<a name="job_28_2"></a>Job 28:2

[barzel](../../strongs/h/h1270.md) is [laqach](../../strongs/h/h3947.md) out of the ['aphar](../../strongs/h/h6083.md), and [nᵊḥûšâ](../../strongs/h/h5154.md) is [ṣûq](../../strongs/h/h6694.md) out of the ['eben](../../strongs/h/h68.md).

<a name="job_28_3"></a>Job 28:3

He [śûm](../../strongs/h/h7760.md) a [qēṣ](../../strongs/h/h7093.md) to [choshek](../../strongs/h/h2822.md), and [chaqar](../../strongs/h/h2713.md) all [taḵlîṯ](../../strongs/h/h8503.md): the ['eben](../../strongs/h/h68.md) of ['ōp̄el](../../strongs/h/h652.md), and the [ṣalmāveṯ](../../strongs/h/h6757.md).

<a name="job_28_4"></a>Job 28:4

The [nachal](../../strongs/h/h5158.md) [pāraṣ](../../strongs/h/h6555.md) from the [guwr](../../strongs/h/h1481.md); even the waters [shakach](../../strongs/h/h7911.md) of the [regel](../../strongs/h/h7272.md): they are dried [dālal](../../strongs/h/h1809.md), they are gone [nûaʿ](../../strongs/h/h5128.md) from ['enowsh](../../strongs/h/h582.md).

<a name="job_28_5"></a>Job 28:5

As for the ['erets](../../strongs/h/h776.md), out of it [yāṣā'](../../strongs/h/h3318.md) [lechem](../../strongs/h/h3899.md): and under it is [hāp̄aḵ](../../strongs/h/h2015.md) as it were ['esh](../../strongs/h/h784.md).

<a name="job_28_6"></a>Job 28:6

The ['eben](../../strongs/h/h68.md) of it are the [maqowm](../../strongs/h/h4725.md) of [sapîr](../../strongs/h/h5601.md): and it hath ['aphar](../../strongs/h/h6083.md) of [zāhāḇ](../../strongs/h/h2091.md).

<a name="job_28_7"></a>Job 28:7

There is a [nāṯîḇ](../../strongs/h/h5410.md) which no [ʿayiṭ](../../strongs/h/h5861.md) [yada'](../../strongs/h/h3045.md), and which the ['ayyâ](../../strongs/h/h344.md) ['ayin](../../strongs/h/h5869.md) hath not [šāzap̄](../../strongs/h/h7805.md):

<a name="job_28_8"></a>Job 28:8

The [šaḥaṣ](../../strongs/h/h7830.md) [ben](../../strongs/h/h1121.md) have not [dāraḵ](../../strongs/h/h1869.md) it, nor the [šāḥal](../../strongs/h/h7826.md) [ʿāḏâ](../../strongs/h/h5710.md) by it.

<a name="job_28_9"></a>Job 28:9

He [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) upon the [ḥallāmîš](../../strongs/h/h2496.md); he [hāp̄aḵ](../../strongs/h/h2015.md) the [har](../../strongs/h/h2022.md) by the [šereš](../../strongs/h/h8328.md).

<a name="job_28_10"></a>Job 28:10

He [bāqaʿ](../../strongs/h/h1234.md) [yᵊ'ōr](../../strongs/h/h2975.md) among the [tsuwr](../../strongs/h/h6697.md); and his ['ayin](../../strongs/h/h5869.md) [ra'ah](../../strongs/h/h7200.md) every [yᵊqār](../../strongs/h/h3366.md).

<a name="job_28_11"></a>Job 28:11

He [ḥāḇaš](../../strongs/h/h2280.md) the [nāhār](../../strongs/h/h5104.md) from [bĕkiy](../../strongs/h/h1065.md); and the thing that is [taʿălumâ](../../strongs/h/h8587.md) [yāṣā'](../../strongs/h/h3318.md) he to ['owr](../../strongs/h/h216.md).

<a name="job_28_12"></a>Job 28:12

But where shall [ḥāḵmâ](../../strongs/h/h2451.md) be [māṣā'](../../strongs/h/h4672.md)? and where is the [maqowm](../../strongs/h/h4725.md) of [bînâ](../../strongs/h/h998.md)?

<a name="job_28_13"></a>Job 28:13

['enowsh](../../strongs/h/h582.md) [yada'](../../strongs/h/h3045.md) not the [ʿēreḵ](../../strongs/h/h6187.md) thereof; neither is it [māṣā'](../../strongs/h/h4672.md) in the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md).

<a name="job_28_14"></a>Job 28:14

The [tĕhowm](../../strongs/h/h8415.md) ['āmar](../../strongs/h/h559.md), It is not in me: and the [yam](../../strongs/h/h3220.md) ['āmar](../../strongs/h/h559.md), It is not with me.

<a name="job_28_15"></a>Job 28:15

It cannot be [nathan](../../strongs/h/h5414.md) for [sᵊḡôr](../../strongs/h/h5458.md), neither shall [keceph](../../strongs/h/h3701.md) be [šāqal](../../strongs/h/h8254.md) for the [mᵊḥîr](../../strongs/h/h4242.md) thereof.

<a name="job_28_16"></a>Job 28:16

It cannot be [sālâ](../../strongs/h/h5541.md) with the [keṯem](../../strongs/h/h3800.md) of ['Ôp̄îr](../../strongs/h/h211.md), with the [yāqār](../../strongs/h/h3368.md) [šōham](../../strongs/h/h7718.md), or the [sapîr](../../strongs/h/h5601.md).

<a name="job_28_17"></a>Job 28:17

The [zāhāḇ](../../strongs/h/h2091.md) and the [zᵊḵûḵîṯ](../../strongs/h/h2137.md) cannot ['arak](../../strongs/h/h6186.md) it: and the [tᵊmûrâ](../../strongs/h/h8545.md) of it shall not be for [kĕliy](../../strongs/h/h3627.md) of fine [pāz](../../strongs/h/h6337.md).

<a name="job_28_18"></a>Job 28:18

No [zakar](../../strongs/h/h2142.md) shall be made of [rā'mâ](../../strongs/h/h7215.md), or of [gāḇîš](../../strongs/h/h1378.md): for the [Mešeḵ](../../strongs/h/h4901.md) of [ḥāḵmâ](../../strongs/h/h2451.md) is above [p̄nyym](../../strongs/h/h6443.md).

<a name="job_28_19"></a>Job 28:19

The [piṭḏâ](../../strongs/h/h6357.md) of [Kûš](../../strongs/h/h3568.md) shall not ['arak](../../strongs/h/h6186.md) it, neither shall it be [sālâ](../../strongs/h/h5541.md) with [tahowr](../../strongs/h/h2889.md) [keṯem](../../strongs/h/h3800.md).

<a name="job_28_20"></a>Job 28:20

Whence then [bow'](../../strongs/h/h935.md) [ḥāḵmâ](../../strongs/h/h2451.md)? and where is the [maqowm](../../strongs/h/h4725.md) of [bînâ](../../strongs/h/h998.md)?

<a name="job_28_21"></a>Job 28:21

Seeing it is ['alam](../../strongs/h/h5956.md) from the ['ayin](../../strongs/h/h5869.md) of all [chay](../../strongs/h/h2416.md), and [cathar](../../strongs/h/h5641.md) from the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md).

<a name="job_28_22"></a>Job 28:22

['Ăḇadôn](../../strongs/h/h11.md) and [maveth](../../strongs/h/h4194.md) ['āmar](../../strongs/h/h559.md), We have [shama'](../../strongs/h/h8085.md) the [šēmaʿ](../../strongs/h/h8088.md) thereof with our ['ozen](../../strongs/h/h241.md).

<a name="job_28_23"></a>Job 28:23

['Elohiym](../../strongs/h/h430.md) [bîn](../../strongs/h/h995.md) the [derek](../../strongs/h/h1870.md) thereof, and he [yada'](../../strongs/h/h3045.md) the [maqowm](../../strongs/h/h4725.md) thereof.

<a name="job_28_24"></a>Job 28:24

For he [nabat](../../strongs/h/h5027.md) to the [qāṣâ](../../strongs/h/h7098.md) of the ['erets](../../strongs/h/h776.md), and [ra'ah](../../strongs/h/h7200.md) under the whole [shamayim](../../strongs/h/h8064.md);

<a name="job_28_25"></a>Job 28:25

To ['asah](../../strongs/h/h6213.md) the [mišqāl](../../strongs/h/h4948.md) for the [ruwach](../../strongs/h/h7307.md); and he [tāḵan](../../strongs/h/h8505.md) the [mayim](../../strongs/h/h4325.md) by [midâ](../../strongs/h/h4060.md).

<a name="job_28_26"></a>Job 28:26

When he ['asah](../../strongs/h/h6213.md) a [choq](../../strongs/h/h2706.md) for the [māṭār](../../strongs/h/h4306.md), and a [derek](../../strongs/h/h1870.md) for the [ḥāzîz](../../strongs/h/h2385.md) of the [qowl](../../strongs/h/h6963.md):

<a name="job_28_27"></a>Job 28:27

Then did he [ra'ah](../../strongs/h/h7200.md) it, and [sāp̄ar](../../strongs/h/h5608.md) it; he [kuwn](../../strongs/h/h3559.md) it, yea, and searched it [chaqar](../../strongs/h/h2713.md).

<a name="job_28_28"></a>Job 28:28

And unto ['āḏām](../../strongs/h/h120.md) he ['āmar](../../strongs/h/h559.md), Behold, the [yir'ah](../../strongs/h/h3374.md) of the ['adonay](../../strongs/h/h136.md), that is [ḥāḵmâ](../../strongs/h/h2451.md); and to [cuwr](../../strongs/h/h5493.md) from [ra'](../../strongs/h/h7451.md) is [bînâ](../../strongs/h/h998.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 27](job_27.md) - [Job 29](job_29.md)