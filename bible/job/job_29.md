# [Job 29](https://www.blueletterbible.org/kjv/job/29)

<a name="job_29_1"></a>Job 29:1

Moreover ['Îyôḇ](../../strongs/h/h347.md) [yāsap̄](../../strongs/h/h3254.md) [nasa'](../../strongs/h/h5375.md) his [māšāl](../../strongs/h/h4912.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_29_2"></a>Job 29:2

Oh [nathan](../../strongs/h/h5414.md) I were as in [yeraḥ](../../strongs/h/h3391.md) [qeḏem](../../strongs/h/h6924.md), as in the [yowm](../../strongs/h/h3117.md) when ['ĕlvôha](../../strongs/h/h433.md) [shamar](../../strongs/h/h8104.md) me;

<a name="job_29_3"></a>Job 29:3

When his [nîr](../../strongs/h/h5216.md) [halal](../../strongs/h/h1984.md) upon my [ro'sh](../../strongs/h/h7218.md), and when by his ['owr](../../strongs/h/h216.md) I [yālaḵ](../../strongs/h/h3212.md) through [choshek](../../strongs/h/h2822.md);

<a name="job_29_4"></a>Job 29:4

As I was in the [yowm](../../strongs/h/h3117.md) of my [ḥōrep̄](../../strongs/h/h2779.md), when the [sôḏ](../../strongs/h/h5475.md) of ['ĕlvôha](../../strongs/h/h433.md) was upon my ['ohel](../../strongs/h/h168.md);

<a name="job_29_5"></a>Job 29:5

When the [Šaday](../../strongs/h/h7706.md) was yet with me, when my [naʿar](../../strongs/h/h5288.md) were [cabiyb](../../strongs/h/h5439.md) me;

<a name="job_29_6"></a>Job 29:6

When I [rāḥaṣ](../../strongs/h/h7364.md) my [hālîḵ](../../strongs/h/h1978.md) with [ḥem'â](../../strongs/h/h2529.md), and the [tsuwr](../../strongs/h/h6697.md) [ṣûq](../../strongs/h/h6694.md) me [peleḡ](../../strongs/h/h6388.md) of [šemen](../../strongs/h/h8081.md);

<a name="job_29_7"></a>Job 29:7

When I [yāṣā'](../../strongs/h/h3318.md) to the [sha'ar](../../strongs/h/h8179.md) through the [qereṯ](../../strongs/h/h7176.md), when I [kuwn](../../strongs/h/h3559.md) my [môšāḇ](../../strongs/h/h4186.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md)!

<a name="job_29_8"></a>Job 29:8

The [naʿar](../../strongs/h/h5288.md) [ra'ah](../../strongs/h/h7200.md) me, and [chaba'](../../strongs/h/h2244.md) themselves: and the [yāšîš](../../strongs/h/h3453.md) [quwm](../../strongs/h/h6965.md), and stood ['amad](../../strongs/h/h5975.md).

<a name="job_29_9"></a>Job 29:9

The [śar](../../strongs/h/h8269.md) [ʿāṣar](../../strongs/h/h6113.md) [millâ](../../strongs/h/h4405.md), and [śûm](../../strongs/h/h7760.md) their [kaph](../../strongs/h/h3709.md) on their [peh](../../strongs/h/h6310.md).

<a name="job_29_10"></a>Job 29:10

The [nāḡîḏ](../../strongs/h/h5057.md) [chaba'](../../strongs/h/h2244.md) their [qowl](../../strongs/h/h6963.md), and their [lashown](../../strongs/h/h3956.md) [dāḇaq](../../strongs/h/h1692.md) to the [ḥēḵ](../../strongs/h/h2441.md).

<a name="job_29_11"></a>Job 29:11

When the ['ozen](../../strongs/h/h241.md) [shama'](../../strongs/h/h8085.md) me, then it ['āšar](../../strongs/h/h833.md) me; and when the ['ayin](../../strongs/h/h5869.md) [ra'ah](../../strongs/h/h7200.md) me, it gave [ʿûḏ](../../strongs/h/h5749.md) to me:

<a name="job_29_12"></a>Job 29:12

Because I [mālaṭ](../../strongs/h/h4422.md) the ['aniy](../../strongs/h/h6041.md) that [šāvaʿ](../../strongs/h/h7768.md), and the [yathowm](../../strongs/h/h3490.md), and him that had none to [ʿāzar](../../strongs/h/h5826.md) him.

<a name="job_29_13"></a>Job 29:13

The [bĕrakah](../../strongs/h/h1293.md) of him that was ready to ['abad](../../strongs/h/h6.md) [bow'](../../strongs/h/h935.md) upon me: and I caused the ['almānâ](../../strongs/h/h490.md) [leb](../../strongs/h/h3820.md) to [ranan](../../strongs/h/h7442.md).

<a name="job_29_14"></a>Job 29:14

I [labash](../../strongs/h/h3847.md) on [tsedeq](../../strongs/h/h6664.md), and it [labash](../../strongs/h/h3847.md) me: my [mishpat](../../strongs/h/h4941.md) was as a [mᵊʿîl](../../strongs/h/h4598.md) and a [ṣānîp̄](../../strongs/h/h6797.md).

<a name="job_29_15"></a>Job 29:15

I was ['ayin](../../strongs/h/h5869.md) to the [ʿiûēr](../../strongs/h/h5787.md), and [regel](../../strongs/h/h7272.md) was I to the [pissēaḥ](../../strongs/h/h6455.md).

<a name="job_29_16"></a>Job 29:16

I was an ['ab](../../strongs/h/h1.md) to the ['ebyown](../../strongs/h/h34.md): and the [rîḇ](../../strongs/h/h7379.md) which I [yada'](../../strongs/h/h3045.md) not I [chaqar](../../strongs/h/h2713.md).

<a name="job_29_17"></a>Job 29:17

And I [shabar](../../strongs/h/h7665.md) the [mᵊṯallᵊʿâ](../../strongs/h/h4973.md) of the [ʿaûāl](../../strongs/h/h5767.md), and [shalak](../../strongs/h/h7993.md) the [ṭerep̄](../../strongs/h/h2964.md) out of his [šēn](../../strongs/h/h8127.md).

<a name="job_29_18"></a>Job 29:18

Then I ['āmar](../../strongs/h/h559.md), I shall [gāvaʿ](../../strongs/h/h1478.md) in my [qēn](../../strongs/h/h7064.md), and I shall [rabah](../../strongs/h/h7235.md) my [yowm](../../strongs/h/h3117.md) as the [ḥôl](../../strongs/h/h2344.md).

<a name="job_29_19"></a>Job 29:19

My [šereš](../../strongs/h/h8328.md) was [pāṯaḥ](../../strongs/h/h6605.md) by the [mayim](../../strongs/h/h4325.md), and the [ṭal](../../strongs/h/h2919.md) lay all [lûn](../../strongs/h/h3885.md) upon my [qāṣîr](../../strongs/h/h7105.md).

<a name="job_29_20"></a>Job 29:20

My [kabowd](../../strongs/h/h3519.md) was [ḥāḏāš](../../strongs/h/h2319.md) in me, and my [qesheth](../../strongs/h/h7198.md) was [ḥālap̄](../../strongs/h/h2498.md) in my [yad](../../strongs/h/h3027.md).

<a name="job_29_21"></a>Job 29:21

Unto me men [shama'](../../strongs/h/h8085.md), and [yāḥal](../../strongs/h/h3176.md), and kept [damam](../../strongs/h/h1826.md) [lᵊmô](../../strongs/h/h3926.md) my ['etsah](../../strongs/h/h6098.md).

<a name="job_29_22"></a>Job 29:22

['aḥar](../../strongs/h/h310.md) my [dabar](../../strongs/h/h1697.md) they spake not [šānâ](../../strongs/h/h8138.md); and my [millâ](../../strongs/h/h4405.md) [nāṭap̄](../../strongs/h/h5197.md) upon them.

<a name="job_29_23"></a>Job 29:23

And they [yāḥal](../../strongs/h/h3176.md) for me as for the [māṭār](../../strongs/h/h4306.md); and they [pāʿar](../../strongs/h/h6473.md) their [peh](../../strongs/h/h6310.md) wide as for the latter [malqôš](../../strongs/h/h4456.md).

<a name="job_29_24"></a>Job 29:24

If I [śāḥaq](../../strongs/h/h7832.md) on them, they ['aman](../../strongs/h/h539.md) it not; and the ['owr](../../strongs/h/h216.md) of my [paniym](../../strongs/h/h6440.md) they cast not [naphal](../../strongs/h/h5307.md).

<a name="job_29_25"></a>Job 29:25

I [bāḥar](../../strongs/h/h977.md) out their [derek](../../strongs/h/h1870.md), and [yashab](../../strongs/h/h3427.md) [ro'sh](../../strongs/h/h7218.md), and [shakan](../../strongs/h/h7931.md) as a [melek](../../strongs/h/h4428.md) in the [gᵊḏûḏ](../../strongs/h/h1416.md), as one that [nacham](../../strongs/h/h5162.md) the ['āḇēl](../../strongs/h/h57.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 28](job_28.md) - [Job 30](job_30.md)