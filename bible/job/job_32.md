# [Job 32](https://www.blueletterbible.org/kjv/job/32)

<a name="job_32_1"></a>Job 32:1

So these three ['enowsh](../../strongs/h/h582.md) [shabath](../../strongs/h/h7673.md) to ['anah](../../strongs/h/h6030.md) ['Îyôḇ](../../strongs/h/h347.md), because he was [tsaddiyq](../../strongs/h/h6662.md) in his own ['ayin](../../strongs/h/h5869.md).

<a name="job_32_2"></a>Job 32:2

Then was [ḥārâ](../../strongs/h/h2734.md) the ['aph](../../strongs/h/h639.md) of ['Ĕlîhû](../../strongs/h/h453.md) the [ben](../../strongs/h/h1121.md) of [Bāraḵ'Ēl](../../strongs/h/h1292.md) the [bûzî](../../strongs/h/h940.md), of the [mišpāḥâ](../../strongs/h/h4940.md) of [Rām](../../strongs/h/h7410.md): against ['Îyôḇ](../../strongs/h/h347.md) was his ['aph](../../strongs/h/h639.md) [ḥārâ](../../strongs/h/h2734.md), because he [ṣāḏaq](../../strongs/h/h6663.md) [nephesh](../../strongs/h/h5315.md) rather than ['Elohiym](../../strongs/h/h430.md).

<a name="job_32_3"></a>Job 32:3

Also against his three [rea'](../../strongs/h/h7453.md) was his ['aph](../../strongs/h/h639.md) [ḥārâ](../../strongs/h/h2734.md), because they had [māṣā'](../../strongs/h/h4672.md) no [maʿănê](../../strongs/h/h4617.md), and yet had [rāšaʿ](../../strongs/h/h7561.md) ['Îyôḇ](../../strongs/h/h347.md).

<a name="job_32_4"></a>Job 32:4

Now ['Ĕlîhû](../../strongs/h/h453.md) had [ḥāḵâ](../../strongs/h/h2442.md) till ['Îyôḇ](../../strongs/h/h347.md) had [dabar](../../strongs/h/h1697.md), because they were [zāqēn](../../strongs/h/h2205.md) [yowm](../../strongs/h/h3117.md) than he.

<a name="job_32_5"></a>Job 32:5

When ['Ĕlîhû](../../strongs/h/h453.md) [ra'ah](../../strongs/h/h7200.md) that there was no [maʿănê](../../strongs/h/h4617.md) in the [peh](../../strongs/h/h6310.md) of these three ['enowsh](../../strongs/h/h582.md), then his ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md).

<a name="job_32_6"></a>Job 32:6

And ['Ĕlîhû](../../strongs/h/h453.md) the [ben](../../strongs/h/h1121.md) of [Bāraḵ'Ēl](../../strongs/h/h1292.md) the [bûzî](../../strongs/h/h940.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), I am [ṣāʿîr](../../strongs/h/h6810.md) [yowm](../../strongs/h/h3117.md), and ye are very [yāšîš](../../strongs/h/h3453.md); wherefore I was [zāḥal](../../strongs/h/h2119.md), and [yare'](../../strongs/h/h3372.md) not [ḥāvâ](../../strongs/h/h2331.md) you mine [dēaʿ](../../strongs/h/h1843.md).

<a name="job_32_7"></a>Job 32:7

I ['āmar](../../strongs/h/h559.md), [yowm](../../strongs/h/h3117.md) should [dabar](../../strongs/h/h1696.md), and [rōḇ](../../strongs/h/h7230.md) of [šānâ](../../strongs/h/h8141.md) should [yada'](../../strongs/h/h3045.md) [ḥāḵmâ](../../strongs/h/h2451.md).

<a name="job_32_8"></a>Job 32:8

['āḵēn](../../strongs/h/h403.md) there is a [ruwach](../../strongs/h/h7307.md) in ['enowsh](../../strongs/h/h582.md): and the [neshamah](../../strongs/h/h5397.md) of the [Šaday](../../strongs/h/h7706.md) giveth them [bîn](../../strongs/h/h995.md).

<a name="job_32_9"></a>Job 32:9

[rab](../../strongs/h/h7227.md) are not always [ḥāḵam](../../strongs/h/h2449.md): neither do the [zāqēn](../../strongs/h/h2205.md) [bîn](../../strongs/h/h995.md) [mishpat](../../strongs/h/h4941.md).

<a name="job_32_10"></a>Job 32:10

Therefore I ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) to me; I also will [ḥāvâ](../../strongs/h/h2331.md) mine [dēaʿ](../../strongs/h/h1843.md).

<a name="job_32_11"></a>Job 32:11

Behold, I [yāḥal](../../strongs/h/h3176.md) for your [dabar](../../strongs/h/h1697.md); I ['azan](../../strongs/h/h238.md) to your [tāḇûn](../../strongs/h/h8394.md), whilst ye [chaqar](../../strongs/h/h2713.md) what to [millâ](../../strongs/h/h4405.md).

<a name="job_32_12"></a>Job 32:12

Yea, I [bîn](../../strongs/h/h995.md) unto you, and, behold, there was none of you that [yakach](../../strongs/h/h3198.md) ['Îyôḇ](../../strongs/h/h347.md), or that ['anah](../../strongs/h/h6030.md) his ['emer](../../strongs/h/h561.md):

<a name="job_32_13"></a>Job 32:13

Lest ye should ['āmar](../../strongs/h/h559.md), We have [māṣā'](../../strongs/h/h4672.md) [ḥāḵmâ](../../strongs/h/h2451.md): ['el](../../strongs/h/h410.md) thrusteth him [nāḏap̄](../../strongs/h/h5086.md), not ['iysh](../../strongs/h/h376.md).

<a name="job_32_14"></a>Job 32:14

Now he hath not ['arak](../../strongs/h/h6186.md) his [millâ](../../strongs/h/h4405.md) against me: neither will I [shuwb](../../strongs/h/h7725.md) him with your ['emer](../../strongs/h/h561.md).

<a name="job_32_15"></a>Job 32:15

They were [ḥāṯaṯ](../../strongs/h/h2865.md), they ['anah](../../strongs/h/h6030.md) no more: they left ['athaq](../../strongs/h/h6275.md) [millâ](../../strongs/h/h4405.md).

<a name="job_32_16"></a>Job 32:16

When I had [yāḥal](../../strongs/h/h3176.md), (for they [dabar](../../strongs/h/h1696.md) not, but ['amad](../../strongs/h/h5975.md), and ['anah](../../strongs/h/h6030.md) no more;)

<a name="job_32_17"></a>Job 32:17

I said, I will ['anah](../../strongs/h/h6030.md) also my [cheleq](../../strongs/h/h2506.md), I also will [ḥāvâ](../../strongs/h/h2331.md) mine [dēaʿ](../../strongs/h/h1843.md).

<a name="job_32_18"></a>Job 32:18

For I am [mālā'](../../strongs/h/h4390.md) of [millâ](../../strongs/h/h4405.md), the [ruwach](../../strongs/h/h7307.md) [beten](../../strongs/h/h990.md) me [ṣûq](../../strongs/h/h6693.md) me.

<a name="job_32_19"></a>Job 32:19

Behold, my [beten](../../strongs/h/h990.md) is as [yayin](../../strongs/h/h3196.md) which hath no [pāṯaḥ](../../strongs/h/h6605.md); it is ready to [bāqaʿ](../../strongs/h/h1234.md) like [ḥāḏāš](../../strongs/h/h2319.md) ['ôḇ](../../strongs/h/h178.md).

<a name="job_32_20"></a>Job 32:20

I will [dabar](../../strongs/h/h1696.md), that I may be [rāvaḥ](../../strongs/h/h7304.md): I will [pāṯaḥ](../../strongs/h/h6605.md) my [saphah](../../strongs/h/h8193.md) and ['anah](../../strongs/h/h6030.md).

<a name="job_32_21"></a>Job 32:21

Let me not, I pray you, [nasa'](../../strongs/h/h5375.md) any ['iysh](../../strongs/h/h376.md) [paniym](../../strongs/h/h6440.md), neither let me give [kānâ](../../strongs/h/h3655.md) unto ['āḏām](../../strongs/h/h120.md).

<a name="job_32_22"></a>Job 32:22

For I [yada'](../../strongs/h/h3045.md) not to give [kānâ](../../strongs/h/h3655.md); in so doing my ['asah](../../strongs/h/h6213.md) would [mᵊʿaṭ](../../strongs/h/h4592.md) take me [nasa'](../../strongs/h/h5375.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 31](job_31.md) - [Job 33](job_33.md)