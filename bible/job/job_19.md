# [Job 19](https://www.blueletterbible.org/kjv/job/19)

<a name="job_19_1"></a>Job 19:1

Then ['Îyôḇ](../../strongs/h/h347.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md),

<a name="job_19_2"></a>Job 19:2

How long will ye [yāḡâ](../../strongs/h/h3013.md) my [nephesh](../../strongs/h/h5315.md), and [dāḵā'](../../strongs/h/h1792.md) me in pieces with [millâ](../../strongs/h/h4405.md)?

<a name="job_19_3"></a>Job 19:3

These ten times have ye [kālam](../../strongs/h/h3637.md) me: ye are not [buwsh](../../strongs/h/h954.md) that ye make yourselves [hāḵar](../../strongs/h/h1970.md) to me.

<a name="job_19_4"></a>Job 19:4

And be it ['āmnām](../../strongs/h/h551.md) that I have [šāḡâ](../../strongs/h/h7686.md), mine [mᵊšûḡâ](../../strongs/h/h4879.md) [lûn](../../strongs/h/h3885.md) with myself.

<a name="job_19_5"></a>Job 19:5

If ['āmnām](../../strongs/h/h551.md) ye will [gāḏal](../../strongs/h/h1431.md) yourselves against me, and [yakach](../../strongs/h/h3198.md) against me my [cherpah](../../strongs/h/h2781.md):

<a name="job_19_6"></a>Job 19:6

[yada'](../../strongs/h/h3045.md) now that ['ĕlvôha](../../strongs/h/h433.md) hath [ʿāvaṯ](../../strongs/h/h5791.md) me, and hath [naqaph](../../strongs/h/h5362.md) me with his [matsuwd](../../strongs/h/h4686.md).

<a name="job_19_7"></a>Job 19:7

Behold, I cry [ṣāʿaq](../../strongs/h/h6817.md) of [chamac](../../strongs/h/h2555.md), but I am not ['anah](../../strongs/h/h6030.md): I cry [šāvaʿ](../../strongs/h/h7768.md), but there is no [mishpat](../../strongs/h/h4941.md).

<a name="job_19_8"></a>Job 19:8

He hath [gāḏar](../../strongs/h/h1443.md) my ['orach](../../strongs/h/h734.md) that I cannot ['abar](../../strongs/h/h5674.md), and he hath [śûm](../../strongs/h/h7760.md) [choshek](../../strongs/h/h2822.md) in my [nāṯîḇ](../../strongs/h/h5410.md).

<a name="job_19_9"></a>Job 19:9

He hath [pāšaṭ](../../strongs/h/h6584.md) me of my [kabowd](../../strongs/h/h3519.md), and [cuwr](../../strongs/h/h5493.md) the [ʿăṭārâ](../../strongs/h/h5850.md) from my [ro'sh](../../strongs/h/h7218.md).

<a name="job_19_10"></a>Job 19:10

He hath [nāṯaṣ](../../strongs/h/h5422.md) me [cabiyb](../../strongs/h/h5439.md), and I am [yālaḵ](../../strongs/h/h3212.md): and mine [tiqvâ](../../strongs/h/h8615.md) hath he [nāsaʿ](../../strongs/h/h5265.md) like an ['ets](../../strongs/h/h6086.md).

<a name="job_19_11"></a>Job 19:11

He hath also [ḥārâ](../../strongs/h/h2734.md) his ['aph](../../strongs/h/h639.md) against me, and he [chashab](../../strongs/h/h2803.md) me unto him as one of his [tsar](../../strongs/h/h6862.md).

<a name="job_19_12"></a>Job 19:12

His [gᵊḏûḏ](../../strongs/h/h1416.md) [bow'](../../strongs/h/h935.md) [yaḥaḏ](../../strongs/h/h3162.md), and raise [sālal](../../strongs/h/h5549.md) their [derek](../../strongs/h/h1870.md) against me, and [ḥānâ](../../strongs/h/h2583.md) [cabiyb](../../strongs/h/h5439.md) my ['ohel](../../strongs/h/h168.md).

<a name="job_19_13"></a>Job 19:13

He hath [rachaq](../../strongs/h/h7368.md) my ['ach](../../strongs/h/h251.md) [rachaq](../../strongs/h/h7368.md) from me, and mine [yada'](../../strongs/h/h3045.md) are verily [zûr](../../strongs/h/h2114.md) from me.

<a name="job_19_14"></a>Job 19:14

My [qarowb](../../strongs/h/h7138.md) have [ḥāḏal](../../strongs/h/h2308.md), and my familiar [yada'](../../strongs/h/h3045.md) have [shakach](../../strongs/h/h7911.md) me.

<a name="job_19_15"></a>Job 19:15

They that [guwr](../../strongs/h/h1481.md) in mine [bayith](../../strongs/h/h1004.md), and my ['amah](../../strongs/h/h519.md), [chashab](../../strongs/h/h2803.md) me for a [zûr](../../strongs/h/h2114.md): I am a [nāḵrî](../../strongs/h/h5237.md) in their ['ayin](../../strongs/h/h5869.md).

<a name="job_19_16"></a>Job 19:16

I [qara'](../../strongs/h/h7121.md) my ['ebed](../../strongs/h/h5650.md), and he gave me no ['anah](../../strongs/h/h6030.md); I [chanan](../../strongs/h/h2603.md) him [bᵊmô](../../strongs/h/h1119.md) my [peh](../../strongs/h/h6310.md).

<a name="job_19_17"></a>Job 19:17

My [ruwach](../../strongs/h/h7307.md) is [zûr](../../strongs/h/h2114.md) to my ['ishshah](../../strongs/h/h802.md), though I [ḥānnôṯ](../../strongs/h/h2589.md) for the [ben](../../strongs/h/h1121.md) sake of mine own [beten](../../strongs/h/h990.md).

<a name="job_19_18"></a>Job 19:18

Yea, [ʿăvîl](../../strongs/h/h5759.md) [mā'as](../../strongs/h/h3988.md) me; I [quwm](../../strongs/h/h6965.md), and they [dabar](../../strongs/h/h1696.md) against me.

<a name="job_19_19"></a>Job 19:19

All my [sôḏ](../../strongs/h/h5475.md) [math](../../strongs/h/h4962.md) [ta'ab](../../strongs/h/h8581.md) me: and they whom I ['ahab](../../strongs/h/h157.md) are [hāp̄aḵ](../../strongs/h/h2015.md) against me.

<a name="job_19_20"></a>Job 19:20

My ['etsem](../../strongs/h/h6106.md) [dāḇaq](../../strongs/h/h1692.md) to my ['owr](../../strongs/h/h5785.md) and to my [basar](../../strongs/h/h1320.md), and I am [mālaṭ](../../strongs/h/h4422.md) with the ['owr](../../strongs/h/h5785.md) of my [šēn](../../strongs/h/h8127.md).

<a name="job_19_21"></a>Job 19:21

Have [chanan](../../strongs/h/h2603.md) upon me, have [chanan](../../strongs/h/h2603.md) upon me, O ye my [rea'](../../strongs/h/h7453.md); for the [yad](../../strongs/h/h3027.md) of ['ĕlvôha](../../strongs/h/h433.md) hath [naga'](../../strongs/h/h5060.md) me.

<a name="job_19_22"></a>Job 19:22

Why do ye [radaph](../../strongs/h/h7291.md) me as ['el](../../strongs/h/h410.md), and are not [sāׂbaʿ](../../strongs/h/h7646.md) with my [basar](../../strongs/h/h1320.md)?

<a name="job_19_23"></a>Job 19:23

Oh [nathan](../../strongs/h/h5414.md) my [millâ](../../strongs/h/h4405.md) were now [kāṯaḇ](../../strongs/h/h3789.md)! oh [nathan](../../strongs/h/h5414.md) they were [ḥāqaq](../../strongs/h/h2710.md) in a [sēp̄er](../../strongs/h/h5612.md)!

<a name="job_19_24"></a>Job 19:24

That they were [ḥāṣaḇ](../../strongs/h/h2672.md) with a [barzel](../../strongs/h/h1270.md) [ʿēṭ](../../strongs/h/h5842.md) and [ʿōp̄ereṯ](../../strongs/h/h5777.md) in the [tsuwr](../../strongs/h/h6697.md) for [ʿaḏ](../../strongs/h/h5703.md)!

<a name="job_19_25"></a>Job 19:25

For I [yada'](../../strongs/h/h3045.md) that my [gā'al](../../strongs/h/h1350.md) [chay](../../strongs/h/h2416.md), and that he shall [quwm](../../strongs/h/h6965.md) at the ['aḥărôn](../../strongs/h/h314.md) day upon the ['aphar](../../strongs/h/h6083.md):

<a name="job_19_26"></a>Job 19:26

And though ['aḥar](../../strongs/h/h310.md) my ['owr](../../strongs/h/h5785.md) worms [naqaph](../../strongs/h/h5362.md) this body, yet in my [basar](../../strongs/h/h1320.md) shall I [chazah](../../strongs/h/h2372.md) ['ĕlvôha](../../strongs/h/h433.md):

<a name="job_19_27"></a>Job 19:27

Whom I shall [chazah](../../strongs/h/h2372.md) for myself, and mine ['ayin](../../strongs/h/h5869.md) shall [ra'ah](../../strongs/h/h7200.md), and not [zûr](../../strongs/h/h2114.md); though my [kilyah](../../strongs/h/h3629.md) be [kalah](../../strongs/h/h3615.md) [ḥêq](../../strongs/h/h2436.md) me.

<a name="job_19_28"></a>Job 19:28

But ye should ['āmar](../../strongs/h/h559.md), Why [radaph](../../strongs/h/h7291.md) we him, seeing the [šereš](../../strongs/h/h8328.md) of the [dabar](../../strongs/h/h1697.md) is [māṣā'](../../strongs/h/h4672.md) in me?

<a name="job_19_29"></a>Job 19:29

Be ye [guwr](../../strongs/h/h1481.md) of the [chereb](../../strongs/h/h2719.md): for [chemah](../../strongs/h/h2534.md) bringeth the ['avon](../../strongs/h/h5771.md) [paniym](../../strongs/h/h6440.md) the [chereb](../../strongs/h/h2719.md), that ye may [yada'](../../strongs/h/h3045.md) there is a [diyn](../../strongs/h/h1779.md) [diyn](../../strongs/h/h1779.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 18](job_18.md) - [Job 20](job_20.md)