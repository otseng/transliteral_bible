# [Job 14](https://www.blueletterbible.org/kjv/job/14)

<a name="job_14_1"></a>Job 14:1

['āḏām](../../strongs/h/h120.md) that is [yalad](../../strongs/h/h3205.md) of an ['ishshah](../../strongs/h/h802.md) is of [qāṣēr](../../strongs/h/h7116.md) [yowm](../../strongs/h/h3117.md), and [śāḇēaʿ](../../strongs/h/h7649.md) of [rōḡez](../../strongs/h/h7267.md).

<a name="job_14_2"></a>Job 14:2

He [yāṣā'](../../strongs/h/h3318.md) like a [tsiyts](../../strongs/h/h6731.md), and is [namal](../../strongs/h/h5243.md): he [bāraḥ](../../strongs/h/h1272.md) also as a [ṣēl](../../strongs/h/h6738.md), and ['amad](../../strongs/h/h5975.md) not.

<a name="job_14_3"></a>Job 14:3

And dost thou [paqach](../../strongs/h/h6491.md) thine ['ayin](../../strongs/h/h5869.md) upon such an one, and [bow'](../../strongs/h/h935.md) me into [mishpat](../../strongs/h/h4941.md) with thee?

<a name="job_14_4"></a>Job 14:4

Who can [nathan](../../strongs/h/h5414.md) a [tahowr](../../strongs/h/h2889.md) thing out of a [tame'](../../strongs/h/h2931.md)? not one.

<a name="job_14_5"></a>Job 14:5

Seeing his [yowm](../../strongs/h/h3117.md) are [ḥāraṣ](../../strongs/h/h2782.md), the [mispār](../../strongs/h/h4557.md) of his [ḥōḏeš](../../strongs/h/h2320.md) are with thee, thou hast ['asah](../../strongs/h/h6213.md) his [choq](../../strongs/h/h2706.md) that he cannot ['abar](../../strongs/h/h5674.md);

<a name="job_14_6"></a>Job 14:6

[šāʿâ](../../strongs/h/h8159.md) from him, that he may [ḥāḏal](../../strongs/h/h2308.md), till he shall [ratsah](../../strongs/h/h7521.md), as an [śāḵîr](../../strongs/h/h7916.md), his [yowm](../../strongs/h/h3117.md).

<a name="job_14_7"></a>Job 14:7

For there is [tiqvâ](../../strongs/h/h8615.md) of an ['ets](../../strongs/h/h6086.md), if it be [karath](../../strongs/h/h3772.md), that it will [ḥālap̄](../../strongs/h/h2498.md), and that the [yôneqeṯ](../../strongs/h/h3127.md) thereof will not [ḥāḏal](../../strongs/h/h2308.md).

<a name="job_14_8"></a>Job 14:8

Though the [šereš](../../strongs/h/h8328.md) thereof [zāqēn](../../strongs/h/h2204.md) in the ['erets](../../strongs/h/h776.md), and the [gezaʿ](../../strongs/h/h1503.md) thereof [muwth](../../strongs/h/h4191.md) in the ['aphar](../../strongs/h/h6083.md);

<a name="job_14_9"></a>Job 14:9

Yet through the [rêaḥ](../../strongs/h/h7381.md) of [mayim](../../strongs/h/h4325.md) it will [pāraḥ](../../strongs/h/h6524.md), and ['asah](../../strongs/h/h6213.md) [qāṣîr](../../strongs/h/h7105.md) like a [neṭaʿ](../../strongs/h/h5194.md).

<a name="job_14_10"></a>Job 14:10

But [geḇer](../../strongs/h/h1397.md) [muwth](../../strongs/h/h4191.md), and [ḥālaš](../../strongs/h/h2522.md): yea, ['āḏām](../../strongs/h/h120.md) giveth up the [gāvaʿ](../../strongs/h/h1478.md), and where is he?

<a name="job_14_11"></a>Job 14:11

As the [mayim](../../strongs/h/h4325.md) ['āzal](../../strongs/h/h235.md) from the [yam](../../strongs/h/h3220.md), and the [nāhār](../../strongs/h/h5104.md) [ḥāraḇ](../../strongs/h/h2717.md) and [yāḇēš](../../strongs/h/h3001.md):

<a name="job_14_12"></a>Job 14:12

So ['iysh](../../strongs/h/h376.md) [shakab](../../strongs/h/h7901.md), and [quwm](../../strongs/h/h6965.md) not: till the [shamayim](../../strongs/h/h8064.md) be no more, they shall not [quwts](../../strongs/h/h6974.md), nor be [ʿûr](../../strongs/h/h5782.md) of their [šēnā'](../../strongs/h/h8142.md).

<a name="job_14_13"></a>Job 14:13

O [nathan](../../strongs/h/h5414.md) thou wouldest [tsaphan](../../strongs/h/h6845.md) me in the [shĕ'owl](../../strongs/h/h7585.md), that thou wouldest keep me [cathar](../../strongs/h/h5641.md), until thy ['aph](../../strongs/h/h639.md) be [shuwb](../../strongs/h/h7725.md), that thou wouldest [shiyth](../../strongs/h/h7896.md) me a set [choq](../../strongs/h/h2706.md), and [zakar](../../strongs/h/h2142.md) me!

<a name="job_14_14"></a>Job 14:14

If a [geḇer](../../strongs/h/h1397.md) [muwth](../../strongs/h/h4191.md), shall he [ḥāyâ](../../strongs/h/h2421.md) again? all the [yowm](../../strongs/h/h3117.md) of my appointed [tsaba'](../../strongs/h/h6635.md) will I [yāḥal](../../strongs/h/h3176.md), till my [ḥălîp̄â](../../strongs/h/h2487.md) [bow'](../../strongs/h/h935.md).

<a name="job_14_15"></a>Job 14:15

Thou shalt [qara'](../../strongs/h/h7121.md), and I will ['anah](../../strongs/h/h6030.md) thee: thou wilt have a [kacaph](../../strongs/h/h3700.md) to the [ma'aseh](../../strongs/h/h4639.md) of thine [yad](../../strongs/h/h3027.md).

<a name="job_14_16"></a>Job 14:16

For now thou [sāp̄ar](../../strongs/h/h5608.md) my [ṣaʿaḏ](../../strongs/h/h6806.md): dost thou not [shamar](../../strongs/h/h8104.md) over my [chatta'ath](../../strongs/h/h2403.md)?

<a name="job_14_17"></a>Job 14:17

My [pesha'](../../strongs/h/h6588.md) is [ḥāṯam](../../strongs/h/h2856.md) in a [ṣᵊrôr](../../strongs/h/h6872.md), and thou sewest [ṭāp̄al](../../strongs/h/h2950.md) mine ['avon](../../strongs/h/h5771.md).

<a name="job_14_18"></a>Job 14:18

And surely the [har](../../strongs/h/h2022.md) [naphal](../../strongs/h/h5307.md) cometh to [nabel](../../strongs/h/h5034.md), and the [tsuwr](../../strongs/h/h6697.md) is ['athaq](../../strongs/h/h6275.md) out of his [maqowm](../../strongs/h/h4725.md).

<a name="job_14_19"></a>Job 14:19

The [mayim](../../strongs/h/h4325.md) [šāḥaq](../../strongs/h/h7833.md) the ['eben](../../strongs/h/h68.md): thou [šāṭap̄](../../strongs/h/h7857.md) the things which [sāp̄îaḥ](../../strongs/h/h5599.md) out of the ['aphar](../../strongs/h/h6083.md) of the ['erets](../../strongs/h/h776.md); and thou ['abad](../../strongs/h/h6.md) the [tiqvâ](../../strongs/h/h8615.md) of ['enowsh](../../strongs/h/h582.md).

<a name="job_14_20"></a>Job 14:20

Thou [tāqap̄](../../strongs/h/h8630.md) for [netsach](../../strongs/h/h5331.md) against him, and he [halak](../../strongs/h/h1980.md): thou [šānâ](../../strongs/h/h8138.md) his [paniym](../../strongs/h/h6440.md), and sendest him [shalach](../../strongs/h/h7971.md).

<a name="job_14_21"></a>Job 14:21

His [ben](../../strongs/h/h1121.md) come to [kabad](../../strongs/h/h3513.md), and he [yada'](../../strongs/h/h3045.md) it not; and they are brought [ṣāʿar](../../strongs/h/h6819.md), but he [bîn](../../strongs/h/h995.md) it not of them.

<a name="job_14_22"></a>Job 14:22

But his [basar](../../strongs/h/h1320.md) upon him shall have [kā'aḇ](../../strongs/h/h3510.md), and his [nephesh](../../strongs/h/h5315.md) within him shall ['āḇal](../../strongs/h/h56.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 13](job_13.md) - [Job 15](job_15.md)