# [Job 9](https://www.blueletterbible.org/kjv/job/9)

<a name="job_9_1"></a>Job 9:1

Then ['Îyôḇ](../../strongs/h/h347.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md),

<a name="job_9_2"></a>Job 9:2

I [yada'](../../strongs/h/h3045.md) it is so of an ['āmnām](../../strongs/h/h551.md): but how should ['enowsh](../../strongs/h/h582.md) be [ṣāḏaq](../../strongs/h/h6663.md) with ['el](../../strongs/h/h410.md)?

<a name="job_9_3"></a>Job 9:3

If he [ḥāp̄ēṣ](../../strongs/h/h2654.md) [riyb](../../strongs/h/h7378.md) with him, he cannot ['anah](../../strongs/h/h6030.md) him one of a thousand.

<a name="job_9_4"></a>Job 9:4

He is [ḥāḵām](../../strongs/h/h2450.md) in [lebab](../../strongs/h/h3824.md), and ['ammîṣ](../../strongs/h/h533.md) in [koach](../../strongs/h/h3581.md): who hath [qāšâ](../../strongs/h/h7185.md) himself against him, and hath [shalam](../../strongs/h/h7999.md)?

<a name="job_9_5"></a>Job 9:5

Which ['athaq](../../strongs/h/h6275.md) the [har](../../strongs/h/h2022.md), and they [yada'](../../strongs/h/h3045.md) not: which [hāp̄aḵ](../../strongs/h/h2015.md) them in his ['aph](../../strongs/h/h639.md).

<a name="job_9_6"></a>Job 9:6

Which [ragaz](../../strongs/h/h7264.md) the ['erets](../../strongs/h/h776.md) out of her [maqowm](../../strongs/h/h4725.md), and the [ʿammûḏ](../../strongs/h/h5982.md) thereof [pālaṣ](../../strongs/h/h6426.md).

<a name="job_9_7"></a>Job 9:7

Which ['āmar](../../strongs/h/h559.md) the [ḥeres](../../strongs/h/h2775.md), and it [zāraḥ](../../strongs/h/h2224.md) not; and [ḥāṯam](../../strongs/h/h2856.md) the [kowkab](../../strongs/h/h3556.md).

<a name="job_9_8"></a>Job 9:8

Which alone [natah](../../strongs/h/h5186.md) the [shamayim](../../strongs/h/h8064.md), and [dāraḵ](../../strongs/h/h1869.md) upon the [bāmâ](../../strongs/h/h1116.md) of the [yam](../../strongs/h/h3220.md).

<a name="job_9_9"></a>Job 9:9

Which ['asah](../../strongs/h/h6213.md) [ʿayiš](../../strongs/h/h5906.md), [kᵊsîl](../../strongs/h/h3685.md), and [kîmâ](../../strongs/h/h3598.md), and the [ḥeḏer](../../strongs/h/h2315.md) of the [têmān](../../strongs/h/h8486.md).

<a name="job_9_10"></a>Job 9:10

Which ['asah](../../strongs/h/h6213.md) [gadowl](../../strongs/h/h1419.md) past [ḥēqer](../../strongs/h/h2714.md); yea, and [pala'](../../strongs/h/h6381.md) without [mispār](../../strongs/h/h4557.md).

<a name="job_9_11"></a>Job 9:11

Lo, he ['abar](../../strongs/h/h5674.md) by me, and I [ra'ah](../../strongs/h/h7200.md) him not: he [ḥālap̄](../../strongs/h/h2498.md) also, but I [bîn](../../strongs/h/h995.md) him not.

<a name="job_9_12"></a>Job 9:12

Behold, he [ḥāṯap̄](../../strongs/h/h2862.md), who can [shuwb](../../strongs/h/h7725.md) him? who will ['āmar](../../strongs/h/h559.md) unto him, What ['asah](../../strongs/h/h6213.md) thou?

<a name="job_9_13"></a>Job 9:13

If ['ĕlvôha](../../strongs/h/h433.md) will not [shuwb](../../strongs/h/h7725.md) his ['aph](../../strongs/h/h639.md), the [rahaḇ](../../strongs/h/h7293.md) [ʿāzar](../../strongs/h/h5826.md) do [shachach](../../strongs/h/h7817.md) under him.

<a name="job_9_14"></a>Job 9:14

How much less shall I ['anah](../../strongs/h/h6030.md) him, and [bāḥar](../../strongs/h/h977.md) my [dabar](../../strongs/h/h1697.md) to reason with him?

<a name="job_9_15"></a>Job 9:15

Whom, though I were [ṣāḏaq](../../strongs/h/h6663.md), yet would I not ['anah](../../strongs/h/h6030.md), but I would make [chanan](../../strongs/h/h2603.md) to my [shaphat](../../strongs/h/h8199.md).

<a name="job_9_16"></a>Job 9:16

If I had [qara'](../../strongs/h/h7121.md), and he had ['anah](../../strongs/h/h6030.md) me; yet would I not ['aman](../../strongs/h/h539.md) that he had ['azan](../../strongs/h/h238.md) unto my [qowl](../../strongs/h/h6963.md).

<a name="job_9_17"></a>Job 9:17

For he [shuwph](../../strongs/h/h7779.md) me with a [śᵊʿārâ](../../strongs/h/h8183.md), and [rabah](../../strongs/h/h7235.md) my [peṣaʿ](../../strongs/h/h6482.md) without [ḥinnām](../../strongs/h/h2600.md).

<a name="job_9_18"></a>Job 9:18

He will not [nathan](../../strongs/h/h5414.md) me to [shuwb](../../strongs/h/h7725.md) my [ruwach](../../strongs/h/h7307.md), but [sāׂbaʿ](../../strongs/h/h7646.md) me with [mammᵊrōrîm](../../strongs/h/h4472.md).

<a name="job_9_19"></a>Job 9:19

If I speak of [koach](../../strongs/h/h3581.md), lo, he is ['ammîṣ](../../strongs/h/h533.md): and if of [mishpat](../../strongs/h/h4941.md), who shall [yāʿaḏ](../../strongs/h/h3259.md) me a time to plead?

<a name="job_9_20"></a>Job 9:20

If I [ṣāḏaq](../../strongs/h/h6663.md) myself, mine own [peh](../../strongs/h/h6310.md) shall [rāšaʿ](../../strongs/h/h7561.md) me: if I say, I am [tām](../../strongs/h/h8535.md), it shall also prove me [ʿāqaš](../../strongs/h/h6140.md).

<a name="job_9_21"></a>Job 9:21

Though I were [tām](../../strongs/h/h8535.md), yet would I not [yada'](../../strongs/h/h3045.md) my [nephesh](../../strongs/h/h5315.md): I would [mā'as](../../strongs/h/h3988.md) my [chay](../../strongs/h/h2416.md).

<a name="job_9_22"></a>Job 9:22

This is one thing, therefore I ['āmar](../../strongs/h/h559.md) it, He [kalah](../../strongs/h/h3615.md) the [tām](../../strongs/h/h8535.md) and the [rasha'](../../strongs/h/h7563.md).

<a name="job_9_23"></a>Job 9:23

If the [šôṭ](../../strongs/h/h7752.md) [muwth](../../strongs/h/h4191.md) [piṯ'ōm](../../strongs/h/h6597.md), he will [lāʿaḡ](../../strongs/h/h3932.md) at the [massâ](../../strongs/h/h4531.md) of the [naqiy](../../strongs/h/h5355.md).

<a name="job_9_24"></a>Job 9:24

The ['erets](../../strongs/h/h776.md) is [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [rasha'](../../strongs/h/h7563.md): he [kāsâ](../../strongs/h/h3680.md) the [paniym](../../strongs/h/h6440.md) of the [shaphat](../../strongs/h/h8199.md) thereof; if not, where, and who is he?

<a name="job_9_25"></a>Job 9:25

Now my [yowm](../../strongs/h/h3117.md) are [qālal](../../strongs/h/h7043.md) than a [rûṣ](../../strongs/h/h7323.md): they [bāraḥ](../../strongs/h/h1272.md), they [ra'ah](../../strongs/h/h7200.md) no [towb](../../strongs/h/h2896.md).

<a name="job_9_26"></a>Job 9:26

They are [ḥālap̄](../../strongs/h/h2498.md) as the ['ēḇê](../../strongs/h/h16.md) ['ŏnîyâ](../../strongs/h/h591.md): as the [nesheׁr](../../strongs/h/h5404.md) that [ṭûś](../../strongs/h/h2907.md) to the ['ōḵel](../../strongs/h/h400.md).

<a name="job_9_27"></a>Job 9:27

If I ['āmar](../../strongs/h/h559.md), I will [shakach](../../strongs/h/h7911.md) my [śîaḥ](../../strongs/h/h7879.md), I will ['azab](../../strongs/h/h5800.md) my [paniym](../../strongs/h/h6440.md), and [bālaḡ](../../strongs/h/h1082.md) myself:

<a name="job_9_28"></a>Job 9:28

I am [yāḡōr](../../strongs/h/h3025.md) of all my ['atstsebeth](../../strongs/h/h6094.md), I [yada'](../../strongs/h/h3045.md) that thou wilt not hold me [naqah](../../strongs/h/h5352.md).

<a name="job_9_29"></a>Job 9:29

If I be [rāšaʿ](../../strongs/h/h7561.md), why then [yaga'](../../strongs/h/h3021.md) I in [heḇel](../../strongs/h/h1892.md)?

<a name="job_9_30"></a>Job 9:30

If I [rāḥaṣ](../../strongs/h/h7364.md) myself [šeleḡ](../../strongs/h/h7950.md) [mayim](../../strongs/h/h4325.md) [bᵊmô](../../strongs/h/h1119.md), and make my [kaph](../../strongs/h/h3709.md) [bōr](../../strongs/h/h1252.md) [bōr](../../strongs/h/h1253.md) so [zāḵaḵ](../../strongs/h/h2141.md);

<a name="job_9_31"></a>Job 9:31

Yet shalt thou [ṭāḇal](../../strongs/h/h2881.md) me in the [shachath](../../strongs/h/h7845.md), and mine own [śalmâ](../../strongs/h/h8008.md) shall [ta'ab](../../strongs/h/h8581.md) me.

<a name="job_9_32"></a>Job 9:32

For he is not an ['iysh](../../strongs/h/h376.md), as I am, that I should ['anah](../../strongs/h/h6030.md) him, and we should [bow'](../../strongs/h/h935.md) [yaḥaḏ](../../strongs/h/h3162.md) in [mishpat](../../strongs/h/h4941.md).

<a name="job_9_33"></a>Job 9:33

Neither is there any [yakach](../../strongs/h/h3198.md) betwixt us, that might [shiyth](../../strongs/h/h7896.md) his [yad](../../strongs/h/h3027.md) upon us both.

<a name="job_9_34"></a>Job 9:34

Let him [cuwr](../../strongs/h/h5493.md) his [shebet](../../strongs/h/h7626.md) [cuwr](../../strongs/h/h5493.md) from me, and let not his ['êmâ](../../strongs/h/h367.md) [ba'ath](../../strongs/h/h1204.md) me:

<a name="job_9_35"></a>Job 9:35

Then would I [dabar](../../strongs/h/h1696.md), and not [yare'](../../strongs/h/h3372.md) him; but it is not so with me.

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 8](job_8.md) - [Job 10](job_10.md)