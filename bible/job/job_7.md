# [Job 7](https://www.blueletterbible.org/kjv/job/7)

<a name="job_7_1"></a>Job 7:1

Is there not a [tsaba'](../../strongs/h/h6635.md) to ['enowsh](../../strongs/h/h582.md) upon ['erets](../../strongs/h/h776.md)? are not his [yowm](../../strongs/h/h3117.md) also like the [yowm](../../strongs/h/h3117.md) of an [śāḵîr](../../strongs/h/h7916.md)?

<a name="job_7_2"></a>Job 7:2

As an ['ebed](../../strongs/h/h5650.md) [šā'ap̄](../../strongs/h/h7602.md) the [ṣēl](../../strongs/h/h6738.md), and as an [śāḵîr](../../strongs/h/h7916.md) [qāvâ](../../strongs/h/h6960.md) for the reward of his [pōʿal](../../strongs/h/h6467.md):

<a name="job_7_3"></a>Job 7:3

So am I made to [nāḥal](../../strongs/h/h5157.md) [yeraḥ](../../strongs/h/h3391.md) of [shav'](../../strongs/h/h7723.md), and ['amal](../../strongs/h/h5999.md) [layil](../../strongs/h/h3915.md) are [mānâ](../../strongs/h/h4487.md) to me.

<a name="job_7_4"></a>Job 7:4

When I [shakab](../../strongs/h/h7901.md), I ['āmar](../../strongs/h/h559.md), When shall I [quwm](../../strongs/h/h6965.md), and the ['ereb](../../strongs/h/h6153.md) be [midaḏ](../../strongs/h/h4059.md)? and I am [sāׂbaʿ](../../strongs/h/h7646.md) of [nᵊḏuḏîm](../../strongs/h/h5076.md) unto the [nešep̄](../../strongs/h/h5399.md).

<a name="job_7_5"></a>Job 7:5

My [basar](../../strongs/h/h1320.md) is [labash](../../strongs/h/h3847.md) with [rimmâ](../../strongs/h/h7415.md) and [gûš](../../strongs/h/h1487.md) of ['aphar](../../strongs/h/h6083.md); my ['owr](../../strongs/h/h5785.md) is [rāḡaʿ](../../strongs/h/h7280.md), and become [mā'as](../../strongs/h/h3988.md).

<a name="job_7_6"></a>Job 7:6

My [yowm](../../strongs/h/h3117.md) are [qālal](../../strongs/h/h7043.md) than an ['ereḡ](../../strongs/h/h708.md), and are [kalah](../../strongs/h/h3615.md) ['ep̄es](../../strongs/h/h657.md) [tiqvâ](../../strongs/h/h8615.md).

<a name="job_7_7"></a>Job 7:7

O [zakar](../../strongs/h/h2142.md) that my [chay](../../strongs/h/h2416.md) is [ruwach](../../strongs/h/h7307.md): mine ['ayin](../../strongs/h/h5869.md) shall no [shuwb](../../strongs/h/h7725.md) [ra'ah](../../strongs/h/h7200.md) [towb](../../strongs/h/h2896.md).

<a name="job_7_8"></a>Job 7:8

The ['ayin](../../strongs/h/h5869.md) of him that hath [rŏ'î](../../strongs/h/h7210.md) me shall [šûr](../../strongs/h/h7789.md) me no more: thine ['ayin](../../strongs/h/h5869.md) are upon me, and I am not.

<a name="job_7_9"></a>Job 7:9

As the [ʿānān](../../strongs/h/h6051.md) is [kalah](../../strongs/h/h3615.md) and [yālaḵ](../../strongs/h/h3212.md): so he that goeth [yarad](../../strongs/h/h3381.md) to the [shĕ'owl](../../strongs/h/h7585.md) shall [ʿālâ](../../strongs/h/h5927.md) no more.

<a name="job_7_10"></a>Job 7:10

He shall [shuwb](../../strongs/h/h7725.md) no more to his [bayith](../../strongs/h/h1004.md), neither shall his [maqowm](../../strongs/h/h4725.md) [nāḵar](../../strongs/h/h5234.md) him any more.

<a name="job_7_11"></a>Job 7:11

Therefore I will not [ḥāśaḵ](../../strongs/h/h2820.md) my [peh](../../strongs/h/h6310.md); I will [dabar](../../strongs/h/h1696.md) in the [tsar](../../strongs/h/h6862.md) of my [ruwach](../../strongs/h/h7307.md); I will [śîaḥ](../../strongs/h/h7878.md) in the [mar](../../strongs/h/h4751.md) of my [nephesh](../../strongs/h/h5315.md).

<a name="job_7_12"></a>Job 7:12

Am I a [yam](../../strongs/h/h3220.md), or a [tannîn](../../strongs/h/h8577.md), that thou [śûm](../../strongs/h/h7760.md) a [mišmār](../../strongs/h/h4929.md) over me?

<a name="job_7_13"></a>Job 7:13

When I ['āmar](../../strongs/h/h559.md), My ['eres](../../strongs/h/h6210.md) shall [nacham](../../strongs/h/h5162.md) me, my [miškāḇ](../../strongs/h/h4904.md) shall [nasa'](../../strongs/h/h5375.md) my [śîaḥ](../../strongs/h/h7879.md);

<a name="job_7_14"></a>Job 7:14

Then thou [ḥāṯaṯ](../../strongs/h/h2865.md) me with [ḥălôm](../../strongs/h/h2472.md), and [ba'ath](../../strongs/h/h1204.md) me through [ḥizzāyôn](../../strongs/h/h2384.md):

<a name="job_7_15"></a>Job 7:15

So that my [nephesh](../../strongs/h/h5315.md) [bāḥar](../../strongs/h/h977.md) [maḥănāq](../../strongs/h/h4267.md), and [maveth](../../strongs/h/h4194.md) rather than my ['etsem](../../strongs/h/h6106.md).

<a name="job_7_16"></a>Job 7:16

I [mā'as](../../strongs/h/h3988.md) it; I would not [ḥāyâ](../../strongs/h/h2421.md) ['owlam](../../strongs/h/h5769.md): let me [ḥāḏal](../../strongs/h/h2308.md); for my [yowm](../../strongs/h/h3117.md) are [heḇel](../../strongs/h/h1892.md).

<a name="job_7_17"></a>Job 7:17

What is ['enowsh](../../strongs/h/h582.md), that thou shouldest [gāḏal](../../strongs/h/h1431.md) him? and that thou shouldest [shiyth](../../strongs/h/h7896.md) thine [leb](../../strongs/h/h3820.md) upon him?

<a name="job_7_18"></a>Job 7:18

And that thou shouldest [paqad](../../strongs/h/h6485.md) him every [boqer](../../strongs/h/h1242.md), and [bachan](../../strongs/h/h974.md) him every [reḡaʿ](../../strongs/h/h7281.md)?

<a name="job_7_19"></a>Job 7:19

How long wilt thou not [šāʿâ](../../strongs/h/h8159.md) from me, nor let me [rāp̄â](../../strongs/h/h7503.md) till I [bālaʿ](../../strongs/h/h1104.md) my [rōq](../../strongs/h/h7536.md)?

<a name="job_7_20"></a>Job 7:20

I have [chata'](../../strongs/h/h2398.md); what shall I [pa'al](../../strongs/h/h6466.md) unto thee, O thou [nāṣar](../../strongs/h/h5341.md) of ['āḏām](../../strongs/h/h120.md)? why hast thou [śûm](../../strongs/h/h7760.md) me as a [mip̄gāʿ](../../strongs/h/h4645.md) against thee, so that I am a [maśśā'](../../strongs/h/h4853.md) to myself?

<a name="job_7_21"></a>Job 7:21

And why dost thou not [nasa'](../../strongs/h/h5375.md) my [pesha'](../../strongs/h/h6588.md), and take ['abar](../../strongs/h/h5674.md) mine ['avon](../../strongs/h/h5771.md)? for now shall I [shakab](../../strongs/h/h7901.md) in the ['aphar](../../strongs/h/h6083.md); and thou shalt seek me in the [šāḥar](../../strongs/h/h7836.md), but I shall not be.

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 6](job_6.md) - [Job 8](job_8.md)