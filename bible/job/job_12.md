# [Job 12](https://www.blueletterbible.org/kjv/job/12)

<a name="job_12_1"></a>Job 12:1

And ['Îyôḇ](../../strongs/h/h347.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md),

<a name="job_12_2"></a>Job 12:2

No ['āmnām](../../strongs/h/h551.md) but ye are the ['am](../../strongs/h/h5971.md), and [ḥāḵmâ](../../strongs/h/h2451.md) shall [muwth](../../strongs/h/h4191.md) with you.

<a name="job_12_3"></a>Job 12:3

But I have [lebab](../../strongs/h/h3824.md) as well as you; I am not [naphal](../../strongs/h/h5307.md) to you: yea, who knoweth not such things as these?

<a name="job_12_4"></a>Job 12:4

I am as one [śᵊḥôq](../../strongs/h/h7814.md) of his [rea'](../../strongs/h/h7453.md), who [qara'](../../strongs/h/h7121.md) upon ['ĕlvôha](../../strongs/h/h433.md), and he ['anah](../../strongs/h/h6030.md) him: the [tsaddiyq](../../strongs/h/h6662.md) [tamiym](../../strongs/h/h8549.md) is [śᵊḥôq](../../strongs/h/h7814.md).

<a name="job_12_5"></a>Job 12:5

He that is [kuwn](../../strongs/h/h3559.md) to [māʿaḏ](../../strongs/h/h4571.md) with his [regel](../../strongs/h/h7272.md) is as a [lapîḏ](../../strongs/h/h3940.md) [bûz](../../strongs/h/h937.md) in the [ʿaštûṯ](../../strongs/h/h6248.md) of him that is at [ša'ănān](../../strongs/h/h7600.md).

<a name="job_12_6"></a>Job 12:6

The ['ohel](../../strongs/h/h168.md) of [shadad](../../strongs/h/h7703.md) [šālâ](../../strongs/h/h7951.md), and they that [ragaz](../../strongs/h/h7264.md) ['el](../../strongs/h/h410.md) are [baṭuḥôṯ](../../strongs/h/h987.md); into whose [yad](../../strongs/h/h3027.md) ['ĕlvôha](../../strongs/h/h433.md) [bow'](../../strongs/h/h935.md) abundantly.

<a name="job_12_7"></a>Job 12:7

But [sha'al](../../strongs/h/h7592.md) now the [bĕhemah](../../strongs/h/h929.md), and they shall [yārâ](../../strongs/h/h3384.md) thee; and the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and they shall [nāḡaḏ](../../strongs/h/h5046.md) thee:

<a name="job_12_8"></a>Job 12:8

Or [śîaḥ](../../strongs/h/h7878.md) to the ['erets](../../strongs/h/h776.md), and it shall [yārâ](../../strongs/h/h3384.md) thee: and the [dag](../../strongs/h/h1709.md) of the [yam](../../strongs/h/h3220.md) shall [sāp̄ar](../../strongs/h/h5608.md) unto thee.

<a name="job_12_9"></a>Job 12:9

Who [yada'](../../strongs/h/h3045.md) not in all these that the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) hath ['asah](../../strongs/h/h6213.md) this?

<a name="job_12_10"></a>Job 12:10

In whose [yad](../../strongs/h/h3027.md) is the [nephesh](../../strongs/h/h5315.md) of every [chay](../../strongs/h/h2416.md), and the [ruwach](../../strongs/h/h7307.md) of all ['iysh](../../strongs/h/h376.md) [basar](../../strongs/h/h1320.md).

<a name="job_12_11"></a>Job 12:11

Doth not the ['ozen](../../strongs/h/h241.md) [bachan](../../strongs/h/h974.md) [millâ](../../strongs/h/h4405.md)? and the [ḥēḵ](../../strongs/h/h2441.md) [ṭāʿam](../../strongs/h/h2938.md) his ['ōḵel](../../strongs/h/h400.md)?

<a name="job_12_12"></a>Job 12:12

With the [yāšîš](../../strongs/h/h3453.md) is [ḥāḵmâ](../../strongs/h/h2451.md); and in ['ōreḵ](../../strongs/h/h753.md) of [yowm](../../strongs/h/h3117.md) [tāḇûn](../../strongs/h/h8394.md).

<a name="job_12_13"></a>Job 12:13

With him is [ḥāḵmâ](../../strongs/h/h2451.md) and [gᵊḇûrâ](../../strongs/h/h1369.md), he hath ['etsah](../../strongs/h/h6098.md) and [tāḇûn](../../strongs/h/h8394.md).

<a name="job_12_14"></a>Job 12:14

Behold, he [harac](../../strongs/h/h2040.md), and it cannot be built [bānâ](../../strongs/h/h1129.md): he [cagar](../../strongs/h/h5462.md) an ['iysh](../../strongs/h/h376.md), and there can be no [pāṯaḥ](../../strongs/h/h6605.md).

<a name="job_12_15"></a>Job 12:15

Behold, he [ʿāṣar](../../strongs/h/h6113.md) the [mayim](../../strongs/h/h4325.md), and they [yāḇēš](../../strongs/h/h3001.md): also he [shalach](../../strongs/h/h7971.md) them, and they [hāp̄aḵ](../../strongs/h/h2015.md) the ['erets](../../strongs/h/h776.md).

<a name="job_12_16"></a>Job 12:16

With him is ['oz](../../strongs/h/h5797.md) and [tûšîyâ](../../strongs/h/h8454.md): the [šāḡaḡ](../../strongs/h/h7683.md) and the [šāḡâ](../../strongs/h/h7686.md) are his.

<a name="job_12_17"></a>Job 12:17

He [yālaḵ](../../strongs/h/h3212.md) [ya'ats](../../strongs/h/h3289.md) away [šôlāl](../../strongs/h/h7758.md), and maketh the [shaphat](../../strongs/h/h8199.md) [halal](../../strongs/h/h1984.md).

<a name="job_12_18"></a>Job 12:18

He [pāṯaḥ](../../strongs/h/h6605.md) the [mûsār](../../strongs/h/h4148.md) of [melek](../../strongs/h/h4428.md), and ['āsar](../../strongs/h/h631.md) their [māṯnayim](../../strongs/h/h4975.md) with an ['ēzôr](../../strongs/h/h232.md).

<a name="job_12_19"></a>Job 12:19

He [yālaḵ](../../strongs/h/h3212.md) [kōhēn](../../strongs/h/h3548.md) away [šôlāl](../../strongs/h/h7758.md), and [sālap̄](../../strongs/h/h5557.md) the ['êṯān](../../strongs/h/h386.md).

<a name="job_12_20"></a>Job 12:20

He [cuwr](../../strongs/h/h5493.md) the [saphah](../../strongs/h/h8193.md) of the ['aman](../../strongs/h/h539.md), and [laqach](../../strongs/h/h3947.md) the [ṭaʿam](../../strongs/h/h2940.md) of the [zāqēn](../../strongs/h/h2205.md).

<a name="job_12_21"></a>Job 12:21

He [šāp̄aḵ](../../strongs/h/h8210.md) [bûz](../../strongs/h/h937.md) upon [nāḏîḇ](../../strongs/h/h5081.md), and [rāp̄â](../../strongs/h/h7503.md) the [mᵊzîaḥ](../../strongs/h/h4206.md) of the ['āp̄îq](../../strongs/h/h650.md).

<a name="job_12_22"></a>Job 12:22

He [gālâ](../../strongs/h/h1540.md) [ʿāmōq](../../strongs/h/h6013.md) out of [choshek](../../strongs/h/h2822.md), and [yāṣā'](../../strongs/h/h3318.md) to ['owr](../../strongs/h/h216.md) the [ṣalmāveṯ](../../strongs/h/h6757.md).

<a name="job_12_23"></a>Job 12:23

He [śāḡā'](../../strongs/h/h7679.md) the [gowy](../../strongs/h/h1471.md), and ['abad](../../strongs/h/h6.md) them: he [šāṭaḥ](../../strongs/h/h7849.md) the [gowy](../../strongs/h/h1471.md), and [nachah](../../strongs/h/h5148.md) them again.

<a name="job_12_24"></a>Job 12:24

He [cuwr](../../strongs/h/h5493.md) the [leb](../../strongs/h/h3820.md) of the [ro'sh](../../strongs/h/h7218.md) of the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), and causeth them to [tāʿâ](../../strongs/h/h8582.md) in a [tohuw](../../strongs/h/h8414.md) where there is no [derek](../../strongs/h/h1870.md).

<a name="job_12_25"></a>Job 12:25

They [māšaš](../../strongs/h/h4959.md) in the [choshek](../../strongs/h/h2822.md) without ['owr](../../strongs/h/h216.md), and he maketh them to [tāʿâ](../../strongs/h/h8582.md) like a [šikôr](../../strongs/h/h7910.md) man.

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 11](job_11.md) - [Job 13](job_13.md)