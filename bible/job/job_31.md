# [Job 31](https://www.blueletterbible.org/kjv/job/31)

<a name="job_31_1"></a>Job 31:1

I [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with mine ['ayin](../../strongs/h/h5869.md); why then should I [bîn](../../strongs/h/h995.md) upon a [bᵊṯûlâ](../../strongs/h/h1330.md)?

<a name="job_31_2"></a>Job 31:2

For what [cheleq](../../strongs/h/h2506.md) of ['ĕlvôha](../../strongs/h/h433.md) is there from [maʿal](../../strongs/h/h4605.md)? and what [nachalah](../../strongs/h/h5159.md) of the [Šaday](../../strongs/h/h7706.md) from on [marowm](../../strongs/h/h4791.md)?

<a name="job_31_3"></a>Job 31:3

Is not ['êḏ](../../strongs/h/h343.md) to the [ʿaûāl](../../strongs/h/h5767.md)? and a [neḵer](../../strongs/h/h5235.md) punishment to the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md)?

<a name="job_31_4"></a>Job 31:4

Doth not he [ra'ah](../../strongs/h/h7200.md) my [derek](../../strongs/h/h1870.md), and [sāp̄ar](../../strongs/h/h5608.md) all my [ṣaʿaḏ](../../strongs/h/h6806.md)?

<a name="job_31_5"></a>Job 31:5

If I have [halak](../../strongs/h/h1980.md) with [shav'](../../strongs/h/h7723.md), or if my [regel](../../strongs/h/h7272.md) hath [ḥûš](../../strongs/h/h2363.md) to [mirmah](../../strongs/h/h4820.md);

<a name="job_31_6"></a>Job 31:6

Let me be [šāqal](../../strongs/h/h8254.md) in a [tsedeq](../../strongs/h/h6664.md) [mō'znayim](../../strongs/h/h3976.md), that ['ĕlvôha](../../strongs/h/h433.md) may [yada'](../../strongs/h/h3045.md) mine [tummâ](../../strongs/h/h8538.md).

<a name="job_31_7"></a>Job 31:7

If my ['ashuwr](../../strongs/h/h838.md) hath [natah](../../strongs/h/h5186.md) of the [derek](../../strongs/h/h1870.md), and mine [leb](../../strongs/h/h3820.md) [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) mine ['ayin](../../strongs/h/h5869.md), and if any [mᵊ'ûm](../../strongs/h/h3971.md) hath [dāḇaq](../../strongs/h/h1692.md) to mine [kaph](../../strongs/h/h3709.md);

<a name="job_31_8"></a>Job 31:8

Then let me [zāraʿ](../../strongs/h/h2232.md), and let ['aḥēr](../../strongs/h/h312.md) ['akal](../../strongs/h/h398.md); yea, let my [ṣe'ĕṣā'îm](../../strongs/h/h6631.md) be rooted [šērēš](../../strongs/h/h8327.md).

<a name="job_31_9"></a>Job 31:9

If mine [leb](../../strongs/h/h3820.md) have been [pāṯâ](../../strongs/h/h6601.md) by an ['ishshah](../../strongs/h/h802.md), or if I have ['arab](../../strongs/h/h693.md) at my [rea'](../../strongs/h/h7453.md) [peṯaḥ](../../strongs/h/h6607.md);

<a name="job_31_10"></a>Job 31:10

Then let my ['ishshah](../../strongs/h/h802.md) [ṭāḥan](../../strongs/h/h2912.md) unto ['aḥēr](../../strongs/h/h312.md), and let ['aḥēr](../../strongs/h/h312.md) [kara'](../../strongs/h/h3766.md) upon her.

<a name="job_31_11"></a>Job 31:11

For this is a [zimmâ](../../strongs/h/h2154.md); yea, it is an ['avon](../../strongs/h/h5771.md) to be punished by the [pālîl](../../strongs/h/h6414.md).

<a name="job_31_12"></a>Job 31:12

For it is an ['esh](../../strongs/h/h784.md) that ['akal](../../strongs/h/h398.md) to ['Ăḇadôn](../../strongs/h/h11.md), and would root [šērēš](../../strongs/h/h8327.md) all mine [tᵊḇû'â](../../strongs/h/h8393.md).

<a name="job_31_13"></a>Job 31:13

If I did [mā'as](../../strongs/h/h3988.md) the [mishpat](../../strongs/h/h4941.md) of my ['ebed](../../strongs/h/h5650.md) or of my ['amah](../../strongs/h/h519.md), when they [rîḇ](../../strongs/h/h7379.md) with me;

<a name="job_31_14"></a>Job 31:14

What then shall I ['asah](../../strongs/h/h6213.md) when ['el](../../strongs/h/h410.md) [quwm](../../strongs/h/h6965.md)? and when he [paqad](../../strongs/h/h6485.md), what shall I [shuwb](../../strongs/h/h7725.md) him?

<a name="job_31_15"></a>Job 31:15

Did not he that ['asah](../../strongs/h/h6213.md) me in the [beten](../../strongs/h/h990.md) ['asah](../../strongs/h/h6213.md) him? and did not one [kuwn](../../strongs/h/h3559.md) us in the [reḥem](../../strongs/h/h7358.md)?

<a name="job_31_16"></a>Job 31:16

If I have [mānaʿ](../../strongs/h/h4513.md) the [dal](../../strongs/h/h1800.md) from their [chephets](../../strongs/h/h2656.md), or have caused the ['ayin](../../strongs/h/h5869.md) of the ['almānâ](../../strongs/h/h490.md) to [kalah](../../strongs/h/h3615.md);

<a name="job_31_17"></a>Job 31:17

Or have ['akal](../../strongs/h/h398.md) my [paṯ](../../strongs/h/h6595.md) myself alone, and the [yathowm](../../strongs/h/h3490.md) hath not ['akal](../../strongs/h/h398.md) thereof;

<a name="job_31_18"></a>Job 31:18

(For from my [nāʿur](../../strongs/h/h5271.md) he was [gāḏal](../../strongs/h/h1431.md) with me, as with an ['ab](../../strongs/h/h1.md), and I have [nachah](../../strongs/h/h5148.md) her from my ['em](../../strongs/h/h517.md) [beten](../../strongs/h/h990.md);)

<a name="job_31_19"></a>Job 31:19

If I have [ra'ah](../../strongs/h/h7200.md) any ['abad](../../strongs/h/h6.md) for want of [lᵊḇûš](../../strongs/h/h3830.md), or any ['ebyown](../../strongs/h/h34.md) without [kᵊsûṯ](../../strongs/h/h3682.md);

<a name="job_31_20"></a>Job 31:20

If his [ḥālāṣ](../../strongs/h/h2504.md) have not [barak](../../strongs/h/h1288.md) me, and if he were not [ḥāmam](../../strongs/h/h2552.md) with the [gēz](../../strongs/h/h1488.md) of my [keḇeś](../../strongs/h/h3532.md);

<a name="job_31_21"></a>Job 31:21

If I have [nûp̄](../../strongs/h/h5130.md) my [yad](../../strongs/h/h3027.md) against the [yathowm](../../strongs/h/h3490.md), when I [ra'ah](../../strongs/h/h7200.md) my [ʿezrâ](../../strongs/h/h5833.md) in the [sha'ar](../../strongs/h/h8179.md):

<a name="job_31_22"></a>Job 31:22

Then let mine [kāṯēp̄](../../strongs/h/h3802.md) [naphal](../../strongs/h/h5307.md) from my [šᵊḵem](../../strongs/h/h7929.md), and mine ['ezrôaʿ](../../strongs/h/h248.md) be [shabar](../../strongs/h/h7665.md) from the [qānê](../../strongs/h/h7070.md).

<a name="job_31_23"></a>Job 31:23

For ['êḏ](../../strongs/h/h343.md) from ['el](../../strongs/h/h410.md) was a [paḥaḏ](../../strongs/h/h6343.md) to me, and by reason of his [śᵊ'ēṯ](../../strongs/h/h7613.md) I [yakol](../../strongs/h/h3201.md) not endure.

<a name="job_31_24"></a>Job 31:24

If I have [śûm](../../strongs/h/h7760.md) [zāhāḇ](../../strongs/h/h2091.md) my [kesel](../../strongs/h/h3689.md), or have ['āmar](../../strongs/h/h559.md) to the [keṯem](../../strongs/h/h3800.md), Thou art my [miḇṭāḥ](../../strongs/h/h4009.md);

<a name="job_31_25"></a>Job 31:25

If I [samach](../../strongs/h/h8055.md) because my [ḥayil](../../strongs/h/h2428.md) was [rab](../../strongs/h/h7227.md), and because mine [yad](../../strongs/h/h3027.md) had [māṣā'](../../strongs/h/h4672.md) [kabîr](../../strongs/h/h3524.md);

<a name="job_31_26"></a>Job 31:26

If I [ra'ah](../../strongs/h/h7200.md) the ['owr](../../strongs/h/h216.md) when it [halal](../../strongs/h/h1984.md), or the [yareach](../../strongs/h/h3394.md) [halak](../../strongs/h/h1980.md) in [yāqār](../../strongs/h/h3368.md);

<a name="job_31_27"></a>Job 31:27

And my [leb](../../strongs/h/h3820.md) hath been [cether](../../strongs/h/h5643.md) [pāṯâ](../../strongs/h/h6601.md), or my [peh](../../strongs/h/h6310.md) hath [nashaq](../../strongs/h/h5401.md) my [yad](../../strongs/h/h3027.md):

<a name="job_31_28"></a>Job 31:28

This also were an ['avon](../../strongs/h/h5771.md) to be punished by the [pᵊlîlî](../../strongs/h/h6416.md): for I should have [kāḥaš](../../strongs/h/h3584.md) the ['el](../../strongs/h/h410.md) that is [maʿal](../../strongs/h/h4605.md).

<a name="job_31_29"></a>Job 31:29

If I [samach](../../strongs/h/h8055.md) at the [pîḏ](../../strongs/h/h6365.md) of him that [sane'](../../strongs/h/h8130.md) me, or lifted [ʿûr](../../strongs/h/h5782.md) myself when [ra'](../../strongs/h/h7451.md) [māṣā'](../../strongs/h/h4672.md) him:

<a name="job_31_30"></a>Job 31:30

Neither have I [nathan](../../strongs/h/h5414.md) my [ḥēḵ](../../strongs/h/h2441.md) to [chata'](../../strongs/h/h2398.md) by [sha'al](../../strongs/h/h7592.md) an ['alah](../../strongs/h/h423.md) to his [nephesh](../../strongs/h/h5315.md).

<a name="job_31_31"></a>Job 31:31

If the [math](../../strongs/h/h4962.md) of my ['ohel](../../strongs/h/h168.md) ['āmar](../../strongs/h/h559.md) not, Oh that we [nathan](../../strongs/h/h5414.md) of his [basar](../../strongs/h/h1320.md)! we cannot be [sāׂbaʿ](../../strongs/h/h7646.md).

<a name="job_31_32"></a>Job 31:32

The [ger](../../strongs/h/h1616.md) did not [lûn](../../strongs/h/h3885.md) in the [ḥûṣ](../../strongs/h/h2351.md): but I [pāṯaḥ](../../strongs/h/h6605.md) my [deleṯ](../../strongs/h/h1817.md) to the ['orach](../../strongs/h/h734.md).

<a name="job_31_33"></a>Job 31:33

If I [kāsâ](../../strongs/h/h3680.md) my [pesha'](../../strongs/h/h6588.md) as ['Āḏām](../../strongs/h/h121.md), by [taman](../../strongs/h/h2934.md) mine ['avon](../../strongs/h/h5771.md) in my [ḥōḇ](../../strongs/h/h2243.md):

<a name="job_31_34"></a>Job 31:34

Did I [ʿāraṣ](../../strongs/h/h6206.md) a [rab](../../strongs/h/h7227.md) [hāmôn](../../strongs/h/h1995.md), or did the [bûz](../../strongs/h/h937.md) of [mišpāḥâ](../../strongs/h/h4940.md) [ḥāṯaṯ](../../strongs/h/h2865.md) me, that I kept [damam](../../strongs/h/h1826.md), and went not [yāṣā'](../../strongs/h/h3318.md) of the [peṯaḥ](../../strongs/h/h6607.md)?

<a name="job_31_35"></a>Job 31:35

Oh [nathan](../../strongs/h/h5414.md) one would [shama'](../../strongs/h/h8085.md) me! behold, my [tāv](../../strongs/h/h8420.md) is, that the [Šaday](../../strongs/h/h7706.md) would ['anah](../../strongs/h/h6030.md) me, and that mine ['iysh](../../strongs/h/h376.md) [rîḇ](../../strongs/h/h7379.md) had [kāṯaḇ](../../strongs/h/h3789.md) a [sēp̄er](../../strongs/h/h5612.md).

<a name="job_31_36"></a>Job 31:36

Surely I would [nasa'](../../strongs/h/h5375.md) it upon my [šᵊḵem](../../strongs/h/h7926.md), and [ʿānaḏ](../../strongs/h/h6029.md) it as a [ʿăṭārâ](../../strongs/h/h5850.md) to me.

<a name="job_31_37"></a>Job 31:37

I would [nāḡaḏ](../../strongs/h/h5046.md) unto him the [mispār](../../strongs/h/h4557.md) of my [ṣaʿaḏ](../../strongs/h/h6806.md); as a [nāḡîḏ](../../strongs/h/h5057.md) would I go [qāraḇ](../../strongs/h/h7126.md) unto him.

<a name="job_31_38"></a>Job 31:38

If my ['ăḏāmâ](../../strongs/h/h127.md) [zāʿaq](../../strongs/h/h2199.md) against me, or that the [telem](../../strongs/h/h8525.md) [yaḥaḏ](../../strongs/h/h3162.md) thereof [bāḵâ](../../strongs/h/h1058.md);

<a name="job_31_39"></a>Job 31:39

If I have ['akal](../../strongs/h/h398.md) the [koach](../../strongs/h/h3581.md) thereof without [keceph](../../strongs/h/h3701.md), or have caused the [baʿal](../../strongs/h/h1167.md) thereof to [nāp̄aḥ](../../strongs/h/h5301.md) their [nephesh](../../strongs/h/h5315.md):

<a name="job_31_40"></a>Job 31:40

Let [ḥôaḥ](../../strongs/h/h2336.md) [yāṣā'](../../strongs/h/h3318.md) instead of [ḥiṭṭâ](../../strongs/h/h2406.md), and [bā'šâ](../../strongs/h/h890.md) instead of [śᵊʿōrâ](../../strongs/h/h8184.md). The [dabar](../../strongs/h/h1697.md) of ['Îyôḇ](../../strongs/h/h347.md) are [tamam](../../strongs/h/h8552.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 30](job_30.md) - [Job 32](job_32.md)