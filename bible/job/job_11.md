# [Job 11](https://www.blueletterbible.org/kjv/job/11)

<a name="job_11_1"></a>Job 11:1

Then ['anah](../../strongs/h/h6030.md) [Ṣôp̄Ar](../../strongs/h/h6691.md) the [naʿămāṯî](../../strongs/h/h5284.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_11_2"></a>Job 11:2

Should not the [rōḇ](../../strongs/h/h7230.md) of [dabar](../../strongs/h/h1697.md) be ['anah](../../strongs/h/h6030.md)? and should an ['iysh](../../strongs/h/h376.md) full of [saphah](../../strongs/h/h8193.md) be [ṣāḏaq](../../strongs/h/h6663.md)?

<a name="job_11_3"></a>Job 11:3

Should thy [baḏ](../../strongs/h/h907.md) [ḥāraš](../../strongs/h/h2790.md) [math](../../strongs/h/h4962.md) hold their [ḥāraš](../../strongs/h/h2790.md)? and when thou [lāʿaḡ](../../strongs/h/h3932.md), shall no man make thee [kālam](../../strongs/h/h3637.md)?

<a name="job_11_4"></a>Job 11:4

For thou hast ['āmar](../../strongs/h/h559.md), My [leqaḥ](../../strongs/h/h3948.md) is [zāḵ](../../strongs/h/h2134.md), and I am [bar](../../strongs/h/h1249.md) in thine ['ayin](../../strongs/h/h5869.md).

<a name="job_11_5"></a>Job 11:5

But oh [nathan](../../strongs/h/h5414.md) ['ĕlvôha](../../strongs/h/h433.md) would [dabar](../../strongs/h/h1696.md), and [pāṯaḥ](../../strongs/h/h6605.md) his [saphah](../../strongs/h/h8193.md) against thee;

<a name="job_11_6"></a>Job 11:6

And that he would [nāḡaḏ](../../strongs/h/h5046.md) thee the [taʿălumâ](../../strongs/h/h8587.md) of [ḥāḵmâ](../../strongs/h/h2451.md), that they are [kep̄el](../../strongs/h/h3718.md) to that which [tûšîyâ](../../strongs/h/h8454.md)! [yada'](../../strongs/h/h3045.md) therefore that ['ĕlvôha](../../strongs/h/h433.md) [nāšâ](../../strongs/h/h5382.md) of thee less than thine ['avon](../../strongs/h/h5771.md) deserveth.

<a name="job_11_7"></a>Job 11:7

Canst thou by [ḥēqer](../../strongs/h/h2714.md) [māṣā'](../../strongs/h/h4672.md) ['ĕlvôha](../../strongs/h/h433.md)? canst thou [māṣā'](../../strongs/h/h4672.md) out the [Šaday](../../strongs/h/h7706.md) unto [taḵlîṯ](../../strongs/h/h8503.md)?

<a name="job_11_8"></a>Job 11:8

It is as [gobahh](../../strongs/h/h1363.md) as [shamayim](../../strongs/h/h8064.md); what canst thou [pa'al](../../strongs/h/h6466.md)? [ʿāmōq](../../strongs/h/h6013.md) than [shĕ'owl](../../strongs/h/h7585.md); what canst thou [yada'](../../strongs/h/h3045.md)?

<a name="job_11_9"></a>Job 11:9

The [maḏ](../../strongs/h/h4055.md) thereof is ['ārēḵ](../../strongs/h/h752.md) than the ['erets](../../strongs/h/h776.md), and [rāḥāḇ](../../strongs/h/h7342.md) than the [yam](../../strongs/h/h3220.md).

<a name="job_11_10"></a>Job 11:10

If he [ḥālap̄](../../strongs/h/h2498.md), and [cagar](../../strongs/h/h5462.md), or [qāhal](../../strongs/h/h6950.md), then who can [shuwb](../../strongs/h/h7725.md) him?

<a name="job_11_11"></a>Job 11:11

For he [yada'](../../strongs/h/h3045.md) [shav'](../../strongs/h/h7723.md) [math](../../strongs/h/h4962.md): he [ra'ah](../../strongs/h/h7200.md) ['aven](../../strongs/h/h205.md) also; will he not then [bîn](../../strongs/h/h995.md) it?

<a name="job_11_12"></a>Job 11:12

For [nāḇaḇ](../../strongs/h/h5014.md) ['iysh](../../strongs/h/h376.md) would be [lāḇaḇ](../../strongs/h/h3823.md), though ['āḏām](../../strongs/h/h120.md) be [yalad](../../strongs/h/h3205.md) like a [pere'](../../strongs/h/h6501.md) [ʿayir](../../strongs/h/h5895.md).

<a name="job_11_13"></a>Job 11:13

If thou [kuwn](../../strongs/h/h3559.md) thine [leb](../../strongs/h/h3820.md), and [pāraś](../../strongs/h/h6566.md) out thine [kaph](../../strongs/h/h3709.md) toward him;

<a name="job_11_14"></a>Job 11:14

If ['aven](../../strongs/h/h205.md) be in thine [yad](../../strongs/h/h3027.md), put it far [rachaq](../../strongs/h/h7368.md), and let not ['evel](../../strongs/h/h5766.md) [shakan](../../strongs/h/h7931.md) in thy ['ohel](../../strongs/h/h168.md).

<a name="job_11_15"></a>Job 11:15

For then shalt thou [nasa'](../../strongs/h/h5375.md) thy [paniym](../../strongs/h/h6440.md) without [mᵊ'ûm](../../strongs/h/h3971.md); yea, thou shalt be [yāṣaq](../../strongs/h/h3332.md), and shalt not [yare'](../../strongs/h/h3372.md):

<a name="job_11_16"></a>Job 11:16

Because thou shalt [shakach](../../strongs/h/h7911.md) thy ['amal](../../strongs/h/h5999.md), and [zakar](../../strongs/h/h2142.md) it as [mayim](../../strongs/h/h4325.md) that ['abar](../../strongs/h/h5674.md):

<a name="job_11_17"></a>Job 11:17

And thine [cheled](../../strongs/h/h2465.md) shall be [quwm](../../strongs/h/h6965.md) than the [ṣōhar](../../strongs/h/h6672.md); thou shalt shine ['uwph](../../strongs/h/h5774.md), thou shalt be as the [boqer](../../strongs/h/h1242.md).

<a name="job_11_18"></a>Job 11:18

And thou shalt be [batach](../../strongs/h/h982.md), because there is [tiqvâ](../../strongs/h/h8615.md); yea, thou shalt [chaphar](../../strongs/h/h2658.md) about thee, and thou shalt take thy [shakab](../../strongs/h/h7901.md) in [betach](../../strongs/h/h983.md).

<a name="job_11_19"></a>Job 11:19

Also thou shalt lie [rāḇaṣ](../../strongs/h/h7257.md), and none shall make thee [ḥārēḏ](../../strongs/h/h2729.md); yea, [rab](../../strongs/h/h7227.md) shall make [ḥālâ](../../strongs/h/h2470.md) unto [paniym](../../strongs/h/h6440.md).

<a name="job_11_20"></a>Job 11:20

But the ['ayin](../../strongs/h/h5869.md) of the [rasha'](../../strongs/h/h7563.md) shall [kalah](../../strongs/h/h3615.md), and they shall not ['abad](../../strongs/h/h6.md) [mānôs](../../strongs/h/h4498.md), and their [tiqvâ](../../strongs/h/h8615.md) shall be as the [mapāḥ](../../strongs/h/h4646.md) of the [nephesh](../../strongs/h/h5315.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 10](job_10.md) - [Job 12](job_12.md)