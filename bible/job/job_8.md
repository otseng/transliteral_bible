# [Job 8](https://www.blueletterbible.org/kjv/job/8)

<a name="job_8_1"></a>Job 8:1

Then ['anah](../../strongs/h/h6030.md) [Bildaḏ](../../strongs/h/h1085.md) the [šûḥî](../../strongs/h/h7747.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_8_2"></a>Job 8:2

How long wilt thou [mālal](../../strongs/h/h4448.md) these things? and how long shall the ['emer](../../strongs/h/h561.md) of thy [peh](../../strongs/h/h6310.md) be like a [kabîr](../../strongs/h/h3524.md) [ruwach](../../strongs/h/h7307.md)?

<a name="job_8_3"></a>Job 8:3

Doth ['el](../../strongs/h/h410.md) [ʿāvaṯ](../../strongs/h/h5791.md) [mishpat](../../strongs/h/h4941.md)? or doth the [Šaday](../../strongs/h/h7706.md) [ʿāvaṯ](../../strongs/h/h5791.md) [tsedeq](../../strongs/h/h6664.md)?

<a name="job_8_4"></a>Job 8:4

If thy [ben](../../strongs/h/h1121.md) have [chata'](../../strongs/h/h2398.md) against him, and he have [shalach](../../strongs/h/h7971.md) [yad](../../strongs/h/h3027.md) their [pesha'](../../strongs/h/h6588.md);

<a name="job_8_5"></a>Job 8:5

If thou wouldest [šāḥar](../../strongs/h/h7836.md) unto ['el](../../strongs/h/h410.md) [šāḥar](../../strongs/h/h7836.md), and make thy [chanan](../../strongs/h/h2603.md) to the [Šaday](../../strongs/h/h7706.md);

<a name="job_8_6"></a>Job 8:6

If thou wert [zāḵ](../../strongs/h/h2134.md) and [yashar](../../strongs/h/h3477.md); surely now he would [ʿûr](../../strongs/h/h5782.md) for thee, and make the [nāvê](../../strongs/h/h5116.md) of thy [tsedeq](../../strongs/h/h6664.md) [shalam](../../strongs/h/h7999.md).

<a name="job_8_7"></a>Job 8:7

Though thy [re'shiyth](../../strongs/h/h7225.md) was [miṣʿār](../../strongs/h/h4705.md), yet thy ['aḥărîṯ](../../strongs/h/h319.md) should [me'od](../../strongs/h/h3966.md) [śāḡâ](../../strongs/h/h7685.md).

<a name="job_8_8"></a>Job 8:8

For [sha'al](../../strongs/h/h7592.md), I pray thee, of the [ri'šôn](../../strongs/h/h7223.md) [ri'šôn](../../strongs/h/h7223.md) [dôr](../../strongs/h/h1755.md), and [kuwn](../../strongs/h/h3559.md) thyself to the [ḥēqer](../../strongs/h/h2714.md) of their ['ab](../../strongs/h/h1.md):

<a name="job_8_9"></a>Job 8:9

(For we are but of [tᵊmôl](../../strongs/h/h8543.md), and [yada'](../../strongs/h/h3045.md) nothing, because our [yowm](../../strongs/h/h3117.md) upon ['erets](../../strongs/h/h776.md) are a [ṣēl](../../strongs/h/h6738.md):)

<a name="job_8_10"></a>Job 8:10

Shall not they [yārâ](../../strongs/h/h3384.md) thee, and ['āmar](../../strongs/h/h559.md) thee, and [yāṣā'](../../strongs/h/h3318.md) [millâ](../../strongs/h/h4405.md) out of their [leb](../../strongs/h/h3820.md)?

<a name="job_8_11"></a>Job 8:11

Can the [gōme'](../../strongs/h/h1573.md) [gā'â](../../strongs/h/h1342.md) without [biṣṣâ](../../strongs/h/h1207.md)? [śāḡâ](../../strongs/h/h7685.md) the ['āḥû](../../strongs/h/h260.md) [śāḡâ](../../strongs/h/h7685.md) without [mayim](../../strongs/h/h4325.md)?

<a name="job_8_12"></a>Job 8:12

Whilst it is yet in his ['ēḇ](../../strongs/h/h3.md), and not [qāṭap̄](../../strongs/h/h6998.md), it [yāḇēš](../../strongs/h/h3001.md) [paniym](../../strongs/h/h6440.md) any other [chatsiyr](../../strongs/h/h2682.md).

<a name="job_8_13"></a>Job 8:13

So are the ['orach](../../strongs/h/h734.md) of all that [shakach](../../strongs/h/h7911.md) ['el](../../strongs/h/h410.md); and the [ḥānēp̄](../../strongs/h/h2611.md) [tiqvâ](../../strongs/h/h8615.md) shall ['abad](../../strongs/h/h6.md):

<a name="job_8_14"></a>Job 8:14

Whose [kesel](../../strongs/h/h3689.md) shall be [qāṭaṭ](../../strongs/h/h6990.md), and whose [miḇṭāḥ](../../strongs/h/h4009.md) shall be a [ʿakāḇîš](../../strongs/h/h5908.md) [bayith](../../strongs/h/h1004.md).

<a name="job_8_15"></a>Job 8:15

He shall [šāʿan](../../strongs/h/h8172.md) upon his [bayith](../../strongs/h/h1004.md), but it shall not ['amad](../../strongs/h/h5975.md): he shall [ḥāzaq](../../strongs/h/h2388.md) it fast, but it shall not [quwm](../../strongs/h/h6965.md).

<a name="job_8_16"></a>Job 8:16

He is [rāṭōḇ](../../strongs/h/h7373.md) [paniym](../../strongs/h/h6440.md) the [šemeš](../../strongs/h/h8121.md), and his [yôneqeṯ](../../strongs/h/h3127.md) [yāṣā'](../../strongs/h/h3318.md) in his [gannâ](../../strongs/h/h1593.md).

<a name="job_8_17"></a>Job 8:17

His [šereš](../../strongs/h/h8328.md) are [sāḇaḵ](../../strongs/h/h5440.md) the [gal](../../strongs/h/h1530.md), and [chazah](../../strongs/h/h2372.md) the [bayith](../../strongs/h/h1004.md) of ['eben](../../strongs/h/h68.md).

<a name="job_8_18"></a>Job 8:18

If he [bālaʿ](../../strongs/h/h1104.md) him from his [maqowm](../../strongs/h/h4725.md), then it shall [kāḥaš](../../strongs/h/h3584.md) him, saying, I have not [ra'ah](../../strongs/h/h7200.md) thee.

<a name="job_8_19"></a>Job 8:19

Behold, this is the [māśôś](../../strongs/h/h4885.md) of his [derek](../../strongs/h/h1870.md), and out of the ['aphar](../../strongs/h/h6083.md) shall ['aḥēr](../../strongs/h/h312.md) [ṣāmaḥ](../../strongs/h/h6779.md).

<a name="job_8_20"></a>Job 8:20

Behold, ['el](../../strongs/h/h410.md) will not [mā'as](../../strongs/h/h3988.md) a [tām](../../strongs/h/h8535.md) man, neither will he [ḥāzaq](../../strongs/h/h2388.md) [yad](../../strongs/h/h3027.md) the [ra'a'](../../strongs/h/h7489.md):

<a name="job_8_21"></a>Job 8:21

Till he [mālā'](../../strongs/h/h4390.md) thy [peh](../../strongs/h/h6310.md) with [śᵊḥôq](../../strongs/h/h7814.md), and thy [saphah](../../strongs/h/h8193.md) with [tᵊrûʿâ](../../strongs/h/h8643.md).

<a name="job_8_22"></a>Job 8:22

They that [sane'](../../strongs/h/h8130.md) thee shall be [labash](../../strongs/h/h3847.md) with [bšeṯ](../../strongs/h/h1322.md); and the ['ohel](../../strongs/h/h168.md) place of the [rasha'](../../strongs/h/h7563.md) shall come to nought.

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 7](job_7.md) - [Job 9](job_9.md)