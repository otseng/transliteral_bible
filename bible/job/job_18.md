# [Job 18](https://www.blueletterbible.org/kjv/job/18)

<a name="job_18_1"></a>Job 18:1

Then ['anah](../../strongs/h/h6030.md) [Bildaḏ](../../strongs/h/h1085.md) the [šûḥî](../../strongs/h/h7747.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_18_2"></a>Job 18:2

How long will it be ere ye [śûm](../../strongs/h/h7760.md) a [qnṣ](../../strongs/h/h7078.md) of [millâ](../../strongs/h/h4405.md)? [bîn](../../strongs/h/h995.md), and ['aḥar](../../strongs/h/h310.md) we will [dabar](../../strongs/h/h1696.md).

<a name="job_18_3"></a>Job 18:3

Wherefore are we [chashab](../../strongs/h/h2803.md) as [bĕhemah](../../strongs/h/h929.md), and [ṭāmâ](../../strongs/h/h2933.md) in your ['ayin](../../strongs/h/h5869.md)?

<a name="job_18_4"></a>Job 18:4

He [taraph](../../strongs/h/h2963.md) [nephesh](../../strongs/h/h5315.md) in his ['aph](../../strongs/h/h639.md): shall the ['erets](../../strongs/h/h776.md) be ['azab](../../strongs/h/h5800.md) for thee? and shall the [tsuwr](../../strongs/h/h6697.md) be ['athaq](../../strongs/h/h6275.md) out of his [maqowm](../../strongs/h/h4725.md)?

<a name="job_18_5"></a>Job 18:5

Yea, the ['owr](../../strongs/h/h216.md) of the [rasha'](../../strongs/h/h7563.md) shall be put [dāʿaḵ](../../strongs/h/h1846.md), and the [šāḇîḇ](../../strongs/h/h7632.md) of his ['esh](../../strongs/h/h784.md) shall not [nāḡahh](../../strongs/h/h5050.md).

<a name="job_18_6"></a>Job 18:6

The ['owr](../../strongs/h/h216.md) shall be [ḥāšaḵ](../../strongs/h/h2821.md) in his ['ohel](../../strongs/h/h168.md), and his [nîr](../../strongs/h/h5216.md) shall be put [dāʿaḵ](../../strongs/h/h1846.md) with him.

<a name="job_18_7"></a>Job 18:7

The [ṣaʿaḏ](../../strongs/h/h6806.md) of his ['ôn](../../strongs/h/h202.md) shall be [yāṣar](../../strongs/h/h3334.md), and his own ['etsah](../../strongs/h/h6098.md) shall [shalak](../../strongs/h/h7993.md) him.

<a name="job_18_8"></a>Job 18:8

For he is [shalach](../../strongs/h/h7971.md) into a [rešeṯ](../../strongs/h/h7568.md) by his own [regel](../../strongs/h/h7272.md), and he [halak](../../strongs/h/h1980.md) upon a [śᵊḇāḵâ](../../strongs/h/h7639.md).

<a name="job_18_9"></a>Job 18:9

The [paḥ](../../strongs/h/h6341.md) shall ['āḥaz](../../strongs/h/h270.md) him by the ['aqeb](../../strongs/h/h6119.md), and the [ṣāmmîm](../../strongs/h/h6782.md) shall [ḥāzaq](../../strongs/h/h2388.md) against him.

<a name="job_18_10"></a>Job 18:10

The [chebel](../../strongs/h/h2256.md) is [taman](../../strongs/h/h2934.md) for him in the ['erets](../../strongs/h/h776.md), and a [malkōḏeṯ](../../strongs/h/h4434.md) for him in the [nāṯîḇ](../../strongs/h/h5410.md).

<a name="job_18_11"></a>Job 18:11

[ballāhâ](../../strongs/h/h1091.md) shall make him [ba'ath](../../strongs/h/h1204.md) on every [cabiyb](../../strongs/h/h5439.md), and shall [puwts](../../strongs/h/h6327.md) him to his [regel](../../strongs/h/h7272.md).

<a name="job_18_12"></a>Job 18:12

His ['ôn](../../strongs/h/h202.md) shall be [rāʿēḇ](../../strongs/h/h7457.md), and ['êḏ](../../strongs/h/h343.md) shall be [kuwn](../../strongs/h/h3559.md) at his [tsela'](../../strongs/h/h6763.md).

<a name="job_18_13"></a>Job 18:13

It shall ['akal](../../strongs/h/h398.md) the [baḏ](../../strongs/h/h905.md) of his ['owr](../../strongs/h/h5785.md): even the [bᵊḵôr](../../strongs/h/h1060.md) of [maveth](../../strongs/h/h4194.md) shall ['akal](../../strongs/h/h398.md) his [baḏ](../../strongs/h/h905.md).

<a name="job_18_14"></a>Job 18:14

His [miḇṭāḥ](../../strongs/h/h4009.md) shall be [nathaq](../../strongs/h/h5423.md) of his ['ohel](../../strongs/h/h168.md), and it shall [ṣāʿaḏ](../../strongs/h/h6805.md) him to the [melek](../../strongs/h/h4428.md) of [ballāhâ](../../strongs/h/h1091.md).

<a name="job_18_15"></a>Job 18:15

It shall [shakan](../../strongs/h/h7931.md) in his ['ohel](../../strongs/h/h168.md), because it is none of his: [gophriyth](../../strongs/h/h1614.md) shall be [zārâ](../../strongs/h/h2219.md) upon his [nāvê](../../strongs/h/h5116.md).

<a name="job_18_16"></a>Job 18:16

His [šereš](../../strongs/h/h8328.md) shall be [yāḇēš](../../strongs/h/h3001.md) beneath, and [maʿal](../../strongs/h/h4605.md) shall his [qāṣîr](../../strongs/h/h7105.md) be [namal](../../strongs/h/h5243.md).

<a name="job_18_17"></a>Job 18:17

His [zeker](../../strongs/h/h2143.md) shall ['abad](../../strongs/h/h6.md) from the ['erets](../../strongs/h/h776.md), and he shall have no [shem](../../strongs/h/h8034.md) in the [paniym](../../strongs/h/h6440.md) [ḥûṣ](../../strongs/h/h2351.md).

<a name="job_18_18"></a>Job 18:18

He shall be [hāḏap̄](../../strongs/h/h1920.md) from ['owr](../../strongs/h/h216.md) into [choshek](../../strongs/h/h2822.md), and [nāḏaḏ](../../strongs/h/h5074.md) out of the [tebel](../../strongs/h/h8398.md).

<a name="job_18_19"></a>Job 18:19

He shall neither have [nîn](../../strongs/h/h5209.md) nor [neḵeḏ](../../strongs/h/h5220.md) among his ['am](../../strongs/h/h5971.md), nor any [śārîḏ](../../strongs/h/h8300.md) in his [māḡûr](../../strongs/h/h4033.md).

<a name="job_18_20"></a>Job 18:20

They that ['aḥărôn](../../strongs/h/h314.md) him shall be [šāmēm](../../strongs/h/h8074.md) at his [yowm](../../strongs/h/h3117.md), as they that [qaḏmōnî](../../strongs/h/h6931.md) were ['āḥaz](../../strongs/h/h270.md) [śaʿar](../../strongs/h/h8178.md).

<a name="job_18_21"></a>Job 18:21

Surely such are the [miškān](../../strongs/h/h4908.md) of the [ʿaûāl](../../strongs/h/h5767.md), and this is the [maqowm](../../strongs/h/h4725.md) of him that [yada'](../../strongs/h/h3045.md) not ['el](../../strongs/h/h410.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 17](job_17.md) - [Job 19](job_19.md)