# [Job 15](https://www.blueletterbible.org/kjv/job/15)

<a name="job_15_1"></a>Job 15:1

Then ['anah](../../strongs/h/h6030.md) ['Ĕlîp̄az](../../strongs/h/h464.md) the [têmānî](../../strongs/h/h8489.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_15_2"></a>Job 15:2

Should a [ḥāḵām](../../strongs/h/h2450.md) ['anah](../../strongs/h/h6030.md) [ruwach](../../strongs/h/h7307.md) [da'ath](../../strongs/h/h1847.md), and [mālā'](../../strongs/h/h4390.md) his [beten](../../strongs/h/h990.md) with the [qāḏîm](../../strongs/h/h6921.md)?

<a name="job_15_3"></a>Job 15:3

Should he [yakach](../../strongs/h/h3198.md) with [sāḵan](../../strongs/h/h5532.md) [dabar](../../strongs/h/h1697.md)? or with [millâ](../../strongs/h/h4405.md) wherewith he can do no [yāʿal](../../strongs/h/h3276.md)?

<a name="job_15_4"></a>Job 15:4

Yea, thou [pārar](../../strongs/h/h6565.md) [yir'ah](../../strongs/h/h3374.md), and [gāraʿ](../../strongs/h/h1639.md) [śîḥâ](../../strongs/h/h7881.md) [paniym](../../strongs/h/h6440.md) ['el](../../strongs/h/h410.md).

<a name="job_15_5"></a>Job 15:5

For thy [peh](../../strongs/h/h6310.md) ['ālap̄](../../strongs/h/h502.md) thine ['avon](../../strongs/h/h5771.md), and thou [bāḥar](../../strongs/h/h977.md) the [lashown](../../strongs/h/h3956.md) of the ['aruwm](../../strongs/h/h6175.md).

<a name="job_15_6"></a>Job 15:6

Thine own [peh](../../strongs/h/h6310.md) [rāšaʿ](../../strongs/h/h7561.md) thee, and not I: yea, thine own [saphah](../../strongs/h/h8193.md) ['anah](../../strongs/h/h6030.md) against thee.

<a name="job_15_7"></a>Job 15:7

Art thou the [ri'šôn](../../strongs/h/h7223.md) [ri'šôn](../../strongs/h/h7223.md) ['āḏām](../../strongs/h/h120.md) that was [yalad](../../strongs/h/h3205.md)? or wast thou [chuwl](../../strongs/h/h2342.md) [paniym](../../strongs/h/h6440.md) the [giḇʿâ](../../strongs/h/h1389.md)?

<a name="job_15_8"></a>Job 15:8

Hast thou [shama'](../../strongs/h/h8085.md) the [sôḏ](../../strongs/h/h5475.md) of ['ĕlvôha](../../strongs/h/h433.md)? and dost thou [gāraʿ](../../strongs/h/h1639.md) [ḥāḵmâ](../../strongs/h/h2451.md) to thyself?

<a name="job_15_9"></a>Job 15:9

What [yada'](../../strongs/h/h3045.md) thou, that we [yada'](../../strongs/h/h3045.md) not? what [bîn](../../strongs/h/h995.md) thou, which is not in us?

<a name="job_15_10"></a>Job 15:10

With us are both the [śîḇ](../../strongs/h/h7867.md) and [yāšîš](../../strongs/h/h3453.md), [kabîr](../../strongs/h/h3524.md) [yowm](../../strongs/h/h3117.md) than thy ['ab](../../strongs/h/h1.md).

<a name="job_15_11"></a>Job 15:11

Are the [tanḥûmôṯ](../../strongs/h/h8575.md) of ['el](../../strongs/h/h410.md) [mᵊʿaṭ](../../strongs/h/h4592.md) with thee? is there any ['aṭ](../../strongs/h/h328.md) [dabar](../../strongs/h/h1697.md) with thee?

<a name="job_15_12"></a>Job 15:12

Why doth thine [leb](../../strongs/h/h3820.md) carry thee [laqach](../../strongs/h/h3947.md)? and what do thy ['ayin](../../strongs/h/h5869.md) wink [rāzam](../../strongs/h/h7335.md),

<a name="job_15_13"></a>Job 15:13

That thou [shuwb](../../strongs/h/h7725.md) thy [ruwach](../../strongs/h/h7307.md) against ['el](../../strongs/h/h410.md), and lettest such [millâ](../../strongs/h/h4405.md) [yāṣā'](../../strongs/h/h3318.md) of thy [peh](../../strongs/h/h6310.md)?

<a name="job_15_14"></a>Job 15:14

What is ['enowsh](../../strongs/h/h582.md), that he should be [zāḵâ](../../strongs/h/h2135.md)? and he which is [yalad](../../strongs/h/h3205.md) of an ['ishshah](../../strongs/h/h802.md), that he should be [ṣāḏaq](../../strongs/h/h6663.md)?

<a name="job_15_15"></a>Job 15:15

Behold, he putteth no ['aman](../../strongs/h/h539.md) in his [qadowsh](../../strongs/h/h6918.md); yea, the [shamayim](../../strongs/h/h8064.md) are not [zāḵaḵ](../../strongs/h/h2141.md) in his ['ayin](../../strongs/h/h5869.md).

<a name="job_15_16"></a>Job 15:16

How much more [ta'ab](../../strongs/h/h8581.md) and ['alach](../../strongs/h/h444.md) is ['iysh](../../strongs/h/h376.md), which [šāṯâ](../../strongs/h/h8354.md) ['evel](../../strongs/h/h5766.md) ['evel](../../strongs/h/h5766.md) like [mayim](../../strongs/h/h4325.md)?

<a name="job_15_17"></a>Job 15:17

I will [ḥāvâ](../../strongs/h/h2331.md) thee, [shama'](../../strongs/h/h8085.md) me; and that which I have [chazah](../../strongs/h/h2372.md) I will [sāp̄ar](../../strongs/h/h5608.md);

<a name="job_15_18"></a>Job 15:18

Which [ḥāḵām](../../strongs/h/h2450.md)have [nāḡaḏ](../../strongs/h/h5046.md) from their ['ab](../../strongs/h/h1.md), and have not [kāḥaḏ](../../strongs/h/h3582.md) it:

<a name="job_15_19"></a>Job 15:19

Unto whom alone the ['erets](../../strongs/h/h776.md) was [nathan](../../strongs/h/h5414.md), and no [zûr](../../strongs/h/h2114.md) ['abar](../../strongs/h/h5674.md) among them.

<a name="job_15_20"></a>Job 15:20

The [rasha'](../../strongs/h/h7563.md) [chuwl](../../strongs/h/h2342.md) all his [yowm](../../strongs/h/h3117.md), and the [mispār](../../strongs/h/h4557.md) of [šānâ](../../strongs/h/h8141.md) is [tsaphan](../../strongs/h/h6845.md) to the [ʿārîṣ](../../strongs/h/h6184.md).

<a name="job_15_21"></a>Job 15:21

A [paḥaḏ](../../strongs/h/h6343.md) [qowl](../../strongs/h/h6963.md) is in his ['ozen](../../strongs/h/h241.md): in [shalowm](../../strongs/h/h7965.md) the [shadad](../../strongs/h/h7703.md) shall [bow'](../../strongs/h/h935.md) upon him.

<a name="job_15_22"></a>Job 15:22

He ['aman](../../strongs/h/h539.md) not that he shall [shuwb](../../strongs/h/h7725.md) out of [choshek](../../strongs/h/h2822.md), and he is [tsaphah](../../strongs/h/h6822.md) for of the [chereb](../../strongs/h/h2719.md).

<a name="job_15_23"></a>Job 15:23

He [nāḏaḏ](../../strongs/h/h5074.md) for [lechem](../../strongs/h/h3899.md), saying, Where is it? he [yada'](../../strongs/h/h3045.md) that the [yowm](../../strongs/h/h3117.md) of [choshek](../../strongs/h/h2822.md) is [kuwn](../../strongs/h/h3559.md) at his [yad](../../strongs/h/h3027.md).

<a name="job_15_24"></a>Job 15:24

[tsar](../../strongs/h/h6862.md) and [mᵊṣûqâ](../../strongs/h/h4691.md) shall make him [ba'ath](../../strongs/h/h1204.md); they shall [tāqap̄](../../strongs/h/h8630.md) against him, as a [melek](../../strongs/h/h4428.md) [ʿāṯîḏ](../../strongs/h/h6264.md) to the [kîḏôr](../../strongs/h/h3593.md).

<a name="job_15_25"></a>Job 15:25

For he [natah](../../strongs/h/h5186.md) his [yad](../../strongs/h/h3027.md) against ['el](../../strongs/h/h410.md), and [gabar](../../strongs/h/h1396.md) himself against the [Šaday](../../strongs/h/h7706.md).

<a name="job_15_26"></a>Job 15:26

He [rûṣ](../../strongs/h/h7323.md) upon him, even on his [ṣaûā'r](../../strongs/h/h6677.md), upon the [ʿăḇî](../../strongs/h/h5672.md) [gaḇ](../../strongs/h/h1354.md) of his [magen](../../strongs/h/h4043.md):

<a name="job_15_27"></a>Job 15:27

Because he [kāsâ](../../strongs/h/h3680.md) his [paniym](../../strongs/h/h6440.md) with his [cheleb](../../strongs/h/h2459.md), and ['asah](../../strongs/h/h6213.md) [pîmâ](../../strongs/h/h6371.md) on his [kesel](../../strongs/h/h3689.md).

<a name="job_15_28"></a>Job 15:28

And he [shakan](../../strongs/h/h7931.md) in [kāḥaḏ](../../strongs/h/h3582.md) [ʿîr](../../strongs/h/h5892.md), and in [bayith](../../strongs/h/h1004.md) which no man [yashab](../../strongs/h/h3427.md), which are [ʿāṯaḏ](../../strongs/h/h6257.md) to become [gal](../../strongs/h/h1530.md).

<a name="job_15_29"></a>Job 15:29

He shall not be [ʿāšar](../../strongs/h/h6238.md), neither shall his [ḥayil](../../strongs/h/h2428.md) [quwm](../../strongs/h/h6965.md), neither shall he [natah](../../strongs/h/h5186.md) the [minlê](../../strongs/h/h4512.md) thereof upon the ['erets](../../strongs/h/h776.md).

<a name="job_15_30"></a>Job 15:30

He shall not [cuwr](../../strongs/h/h5493.md) out of [choshek](../../strongs/h/h2822.md); the [šalheḇeṯ](../../strongs/h/h7957.md) shall [yāḇēš](../../strongs/h/h3001.md) his [yôneqeṯ](../../strongs/h/h3127.md), and by the [ruwach](../../strongs/h/h7307.md) of his [peh](../../strongs/h/h6310.md) shall he go [cuwr](../../strongs/h/h5493.md).

<a name="job_15_31"></a>Job 15:31

Let not him that is [tāʿâ](../../strongs/h/h8582.md) ['aman](../../strongs/h/h539.md) in [shav'](../../strongs/h/h7723.md): for [shav'](../../strongs/h/h7723.md) shall be his [tᵊmûrâ](../../strongs/h/h8545.md).

<a name="job_15_32"></a>Job 15:32

It shall be [mālā'](../../strongs/h/h4390.md) before his [yowm](../../strongs/h/h3117.md), and his [kipâ](../../strongs/h/h3712.md) shall not be [raʿănān](../../strongs/h/h7488.md).

<a name="job_15_33"></a>Job 15:33

He shall [ḥāmas](../../strongs/h/h2554.md) his [bēser](../../strongs/h/h1154.md) as the [gep̄en](../../strongs/h/h1612.md), and shall [shalak](../../strongs/h/h7993.md) his [niṣṣâ](../../strongs/h/h5328.md) as the [zayiṯ](../../strongs/h/h2132.md).

<a name="job_15_34"></a>Job 15:34

For the ['edah](../../strongs/h/h5712.md) of [ḥānēp̄](../../strongs/h/h2611.md) shall be [galmûḏ](../../strongs/h/h1565.md), and ['esh](../../strongs/h/h784.md) shall ['akal](../../strongs/h/h398.md) the ['ohel](../../strongs/h/h168.md) of [shachad](../../strongs/h/h7810.md).

<a name="job_15_35"></a>Job 15:35

They [harah](../../strongs/h/h2029.md) ['amal](../../strongs/h/h5999.md), and [yalad](../../strongs/h/h3205.md) ['aven](../../strongs/h/h205.md), and their [beten](../../strongs/h/h990.md) [kuwn](../../strongs/h/h3559.md) [mirmah](../../strongs/h/h4820.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 14](job_14.md) - [Job 16](job_16.md)