# [Job 27](https://www.blueletterbible.org/kjv/job/27)

<a name="job_27_1"></a>Job 27:1

Moreover ['Îyôḇ](../../strongs/h/h347.md) [yāsap̄](../../strongs/h/h3254.md) [nasa'](../../strongs/h/h5375.md) his [māšāl](../../strongs/h/h4912.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_27_2"></a>Job 27:2

As ['el](../../strongs/h/h410.md) [chay](../../strongs/h/h2416.md), who hath [cuwr](../../strongs/h/h5493.md) my [mishpat](../../strongs/h/h4941.md); and the [Šaday](../../strongs/h/h7706.md), who hath [mārar](../../strongs/h/h4843.md) my [nephesh](../../strongs/h/h5315.md);

<a name="job_27_3"></a>Job 27:3

All the while my [neshamah](../../strongs/h/h5397.md) is in me, and the [ruwach](../../strongs/h/h7307.md) of ['ĕlvôha](../../strongs/h/h433.md) is in my ['aph](../../strongs/h/h639.md);

<a name="job_27_4"></a>Job 27:4

My [saphah](../../strongs/h/h8193.md) shall not [dabar](../../strongs/h/h1696.md) ['evel](../../strongs/h/h5766.md), nor my [lashown](../../strongs/h/h3956.md) [hagah](../../strongs/h/h1897.md) [rᵊmîyâ](../../strongs/h/h7423.md).

<a name="job_27_5"></a>Job 27:5

[ḥālîlâ](../../strongs/h/h2486.md) that I should [ṣāḏaq](../../strongs/h/h6663.md) you: till I [gāvaʿ](../../strongs/h/h1478.md) I will not [cuwr](../../strongs/h/h5493.md) mine [tummâ](../../strongs/h/h8538.md) from me.

<a name="job_27_6"></a>Job 27:6

My [tsedaqah](../../strongs/h/h6666.md) I [ḥāzaq](../../strongs/h/h2388.md) fast, and will not let it [rāp̄â](../../strongs/h/h7503.md): my [lebab](../../strongs/h/h3824.md) shall not [ḥārap̄](../../strongs/h/h2778.md) me so long as I [yowm](../../strongs/h/h3117.md).

<a name="job_27_7"></a>Job 27:7

Let mine ['oyeb](../../strongs/h/h341.md) be as the [rasha'](../../strongs/h/h7563.md), and he that [quwm](../../strongs/h/h6965.md) against me as the [ʿaûāl](../../strongs/h/h5767.md).

<a name="job_27_8"></a>Job 27:8

For what is the [tiqvâ](../../strongs/h/h8615.md) of the [ḥānēp̄](../../strongs/h/h2611.md), though he hath [batsa'](../../strongs/h/h1214.md), when ['ĕlvôha](../../strongs/h/h433.md) [šālâ](../../strongs/h/h7953.md) his [nephesh](../../strongs/h/h5315.md)?

<a name="job_27_9"></a>Job 27:9

Will ['el](../../strongs/h/h410.md) [shama'](../../strongs/h/h8085.md) his [tsa'aqah](../../strongs/h/h6818.md) when [tsarah](../../strongs/h/h6869.md) [bow'](../../strongs/h/h935.md) upon him?

<a name="job_27_10"></a>Job 27:10

Will he [ʿānaḡ](../../strongs/h/h6026.md) himself in the [Šaday](../../strongs/h/h7706.md)? will he [ʿēṯ](../../strongs/h/h6256.md) [qara'](../../strongs/h/h7121.md) upon ['ĕlvôha](../../strongs/h/h433.md)?

<a name="job_27_11"></a>Job 27:11

I will [yārâ](../../strongs/h/h3384.md) you by the [yad](../../strongs/h/h3027.md) of ['el](../../strongs/h/h410.md): that which is with the [Šaday](../../strongs/h/h7706.md) will I not [kāḥaḏ](../../strongs/h/h3582.md).

<a name="job_27_12"></a>Job 27:12

Behold, all ye yourselves have [chazah](../../strongs/h/h2372.md) it; why then are ye thus [heḇel](../../strongs/h/h1892.md) [hāḇal](../../strongs/h/h1891.md)?

<a name="job_27_13"></a>Job 27:13

This is the [cheleq](../../strongs/h/h2506.md) of a [rasha'](../../strongs/h/h7563.md) ['āḏām](../../strongs/h/h120.md) with ['el](../../strongs/h/h410.md), and the [nachalah](../../strongs/h/h5159.md) of [ʿārîṣ](../../strongs/h/h6184.md), which they shall [laqach](../../strongs/h/h3947.md) of the [Šaday](../../strongs/h/h7706.md).

<a name="job_27_14"></a>Job 27:14

If his [ben](../../strongs/h/h1121.md) be [rabah](../../strongs/h/h7235.md), it is [lᵊmô](../../strongs/h/h3926.md) the [chereb](../../strongs/h/h2719.md): and his [ṣe'ĕṣā'îm](../../strongs/h/h6631.md) shall not be [sāׂbaʿ](../../strongs/h/h7646.md) with [lechem](../../strongs/h/h3899.md).

<a name="job_27_15"></a>Job 27:15

Those that [śārîḏ](../../strongs/h/h8300.md) of him shall be [qāḇar](../../strongs/h/h6912.md) in [maveth](../../strongs/h/h4194.md): and his ['almānâ](../../strongs/h/h490.md) shall not [bāḵâ](../../strongs/h/h1058.md).

<a name="job_27_16"></a>Job 27:16

Though he [ṣāḇar](../../strongs/h/h6651.md) [keceph](../../strongs/h/h3701.md) as the ['aphar](../../strongs/h/h6083.md), and [kuwn](../../strongs/h/h3559.md) [malbûš](../../strongs/h/h4403.md) as the [ḥōmer](../../strongs/h/h2563.md);

<a name="job_27_17"></a>Job 27:17

He may [kuwn](../../strongs/h/h3559.md) it, but the [tsaddiyq](../../strongs/h/h6662.md) shall put it [labash](../../strongs/h/h3847.md), and the [naqiy](../../strongs/h/h5355.md) shall [chalaq](../../strongs/h/h2505.md) the [keceph](../../strongs/h/h3701.md).

<a name="job_27_18"></a>Job 27:18

He [bānâ](../../strongs/h/h1129.md) his [bayith](../../strongs/h/h1004.md) as a [ʿāš](../../strongs/h/h6211.md), and as a [cukkah](../../strongs/h/h5521.md) that the [nāṣar](../../strongs/h/h5341.md) ['asah](../../strongs/h/h6213.md).

<a name="job_27_19"></a>Job 27:19

The [ʿāšîr](../../strongs/h/h6223.md) shall [shakab](../../strongs/h/h7901.md), but he shall not be ['āsap̄](../../strongs/h/h622.md): he [paqach](../../strongs/h/h6491.md) his ['ayin](../../strongs/h/h5869.md), and he is not.

<a name="job_27_20"></a>Job 27:20

[ballāhâ](../../strongs/h/h1091.md) take [nāśaḡ](../../strongs/h/h5381.md) on him as [mayim](../../strongs/h/h4325.md), a [sûp̄â](../../strongs/h/h5492.md) [ganab](../../strongs/h/h1589.md) him in the [layil](../../strongs/h/h3915.md).

<a name="job_27_21"></a>Job 27:21

The [qāḏîm](../../strongs/h/h6921.md) [nasa'](../../strongs/h/h5375.md) him, and he [yālaḵ](../../strongs/h/h3212.md): and as a [śāʿar](../../strongs/h/h8175.md) him out of his [maqowm](../../strongs/h/h4725.md).

<a name="job_27_22"></a>Job 27:22

For shall [shalak](../../strongs/h/h7993.md) upon him, and not [ḥāmal](../../strongs/h/h2550.md): he would [bāraḥ](../../strongs/h/h1272.md) [bāraḥ](../../strongs/h/h1272.md) out of his [yad](../../strongs/h/h3027.md).

<a name="job_27_23"></a>Job 27:23

Shall [sāp̄aq](../../strongs/h/h5606.md) their [kaph](../../strongs/h/h3709.md) at him, and shall [šāraq](../../strongs/h/h8319.md) him out of his [maqowm](../../strongs/h/h4725.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 26](job_26.md) - [Job 28](job_28.md)