# [Job 20](https://www.blueletterbible.org/kjv/job/20)

<a name="job_20_1"></a>Job 20:1

Then ['anah](../../strongs/h/h6030.md) [Ṣôp̄Ar](../../strongs/h/h6691.md) the [naʿămāṯî](../../strongs/h/h5284.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_20_2"></a>Job 20:2

Therefore do my [sᵊʿipâ](../../strongs/h/h5587.md) cause me to [shuwb](../../strongs/h/h7725.md), and for this I make [ḥûš](../../strongs/h/h2363.md).

<a name="job_20_3"></a>Job 20:3

I have [shama'](../../strongs/h/h8085.md) the [mûsār](../../strongs/h/h4148.md) of my [kĕlimmah](../../strongs/h/h3639.md), and the [ruwach](../../strongs/h/h7307.md) of my [bînâ](../../strongs/h/h998.md) causeth me to ['anah](../../strongs/h/h6030.md).

<a name="job_20_4"></a>Job 20:4

[yada'](../../strongs/h/h3045.md) thou not this of [ʿaḏ](../../strongs/h/h5703.md), since ['āḏām](../../strongs/h/h120.md) was [śûm](../../strongs/h/h7760.md) upon ['erets](../../strongs/h/h776.md),

<a name="job_20_5"></a>Job 20:5

That the [rᵊnānâ](../../strongs/h/h7445.md) of the [rasha'](../../strongs/h/h7563.md) is [qarowb](../../strongs/h/h7138.md), and the [simchah](../../strongs/h/h8057.md) of the [ḥānēp̄](../../strongs/h/h2611.md) but for a [reḡaʿ](../../strongs/h/h7281.md)?

<a name="job_20_6"></a>Job 20:6

Though his [śî'](../../strongs/h/h7863.md) [ʿālâ](../../strongs/h/h5927.md) to the [shamayim](../../strongs/h/h8064.md), and his [ro'sh](../../strongs/h/h7218.md) [naga'](../../strongs/h/h5060.md) unto the ['ab](../../strongs/h/h5645.md);

<a name="job_20_7"></a>Job 20:7

Yet he shall ['abad](../../strongs/h/h6.md) [netsach](../../strongs/h/h5331.md) like his own [gēlel](../../strongs/h/h1561.md): they which have [ra'ah](../../strongs/h/h7200.md) him shall ['āmar](../../strongs/h/h559.md), Where is he?

<a name="job_20_8"></a>Job 20:8

He shall ['uwph](../../strongs/h/h5774.md) as a [ḥălôm](../../strongs/h/h2472.md), and shall not be [māṣā'](../../strongs/h/h4672.md): yea, he shall be [nāḏaḏ](../../strongs/h/h5074.md) as a [ḥizzāyôn](../../strongs/h/h2384.md) of the [layil](../../strongs/h/h3915.md).

<a name="job_20_9"></a>Job 20:9

The ['ayin](../../strongs/h/h5869.md) also which [šāzap̄](../../strongs/h/h7805.md) him shall see him no more; neither shall his [maqowm](../../strongs/h/h4725.md) any more [šûr](../../strongs/h/h7789.md) him.

<a name="job_20_10"></a>Job 20:10

His [ben](../../strongs/h/h1121.md) shall seek to [ratsah](../../strongs/h/h7521.md) the [dal](../../strongs/h/h1800.md), and his [yad](../../strongs/h/h3027.md) shall [shuwb](../../strongs/h/h7725.md) their ['ôn](../../strongs/h/h202.md).

<a name="job_20_11"></a>Job 20:11

His ['etsem](../../strongs/h/h6106.md) are [mālā'](../../strongs/h/h4390.md) of the sin of his [ʿălûmîm](../../strongs/h/h5934.md), which shall [shakab](../../strongs/h/h7901.md) with him in the ['aphar](../../strongs/h/h6083.md).

<a name="job_20_12"></a>Job 20:12

Though [ra'](../../strongs/h/h7451.md) be [māṯaq](../../strongs/h/h4985.md) in his [peh](../../strongs/h/h6310.md), though he [kāḥaḏ](../../strongs/h/h3582.md) it under his [lashown](../../strongs/h/h3956.md);

<a name="job_20_13"></a>Job 20:13

Though he [ḥāmal](../../strongs/h/h2550.md) it, and ['azab](../../strongs/h/h5800.md) it not; but keep it [mānaʿ](../../strongs/h/h4513.md) within his [ḥēḵ](../../strongs/h/h2441.md):

<a name="job_20_14"></a>Job 20:14

Yet his [lechem](../../strongs/h/h3899.md) in his [me'ah](../../strongs/h/h4578.md) is [hāp̄aḵ](../../strongs/h/h2015.md), it is the [mᵊrōrâ](../../strongs/h/h4846.md) of [peṯen](../../strongs/h/h6620.md) [qereḇ](../../strongs/h/h7130.md) him.

<a name="job_20_15"></a>Job 20:15

He hath swallowed [bālaʿ](../../strongs/h/h1104.md) [ḥayil](../../strongs/h/h2428.md), and he shall vomit them up [qî'](../../strongs/h/h6958.md): ['el](../../strongs/h/h410.md) shall cast them [yarash](../../strongs/h/h3423.md) of his [beten](../../strongs/h/h990.md).

<a name="job_20_16"></a>Job 20:16

He shall [yānaq](../../strongs/h/h3243.md) the [rō'š](../../strongs/h/h7219.md) of [peṯen](../../strongs/h/h6620.md): the ['ep̄ʿê](../../strongs/h/h660.md) [lashown](../../strongs/h/h3956.md) shall [harag](../../strongs/h/h2026.md) him.

<a name="job_20_17"></a>Job 20:17

He shall not [ra'ah](../../strongs/h/h7200.md) the [pᵊlagâ](../../strongs/h/h6390.md), the [nāhār](../../strongs/h/h5104.md), the [nachal](../../strongs/h/h5158.md) of [dĕbash](../../strongs/h/h1706.md) and [ḥem'â](../../strongs/h/h2529.md).

<a name="job_20_18"></a>Job 20:18

That which he [yāḡāʿ](../../strongs/h/h3022.md) for shall he [shuwb](../../strongs/h/h7725.md), and shall not swallow it [bālaʿ](../../strongs/h/h1104.md): according to his [ḥayil](../../strongs/h/h2428.md) shall the [tᵊmûrâ](../../strongs/h/h8545.md) be, and he shall not [ʿālas](../../strongs/h/h5965.md) therein.

<a name="job_20_19"></a>Job 20:19

Because he hath [rāṣaṣ](../../strongs/h/h7533.md) and hath ['azab](../../strongs/h/h5800.md) the [dal](../../strongs/h/h1800.md); because he hath violently taken [gāzal](../../strongs/h/h1497.md) a [bayith](../../strongs/h/h1004.md) which he [bānâ](../../strongs/h/h1129.md) not;

<a name="job_20_20"></a>Job 20:20

Surely he shall not [yada'](../../strongs/h/h3045.md) [šālēv](../../strongs/h/h7961.md) in his [beten](../../strongs/h/h990.md), he shall not [mālaṭ](../../strongs/h/h4422.md) of that which he [chamad](../../strongs/h/h2530.md).

<a name="job_20_21"></a>Job 20:21

There shall none of his ['ōḵel](../../strongs/h/h400.md) be [śārîḏ](../../strongs/h/h8300.md); therefore shall no man [chuwl](../../strongs/h/h2342.md) for his [ṭûḇ](../../strongs/h/h2898.md).

<a name="job_20_22"></a>Job 20:22

In the [mālā'](../../strongs/h/h4390.md) [mālā'](../../strongs/h/h4390.md) of his [sēp̄eq](../../strongs/h/h5607.md) he shall be in [yāṣar](../../strongs/h/h3334.md): every [yad](../../strongs/h/h3027.md) of the [ʿāmēl](../../strongs/h/h6001.md) shall [bow'](../../strongs/h/h935.md) upon him.

<a name="job_20_23"></a>Job 20:23

When he is about to [mālā'](../../strongs/h/h4390.md) his [beten](../../strongs/h/h990.md), shall [shalach](../../strongs/h/h7971.md) the [charown](../../strongs/h/h2740.md) of his ['aph](../../strongs/h/h639.md) upon him, and shall [matar](../../strongs/h/h4305.md) it upon him while he is [lāḥûm](../../strongs/h/h3894.md).

<a name="job_20_24"></a>Job 20:24

He shall [bāraḥ](../../strongs/h/h1272.md) from the [barzel](../../strongs/h/h1270.md) [nešeq](../../strongs/h/h5402.md), and the [qesheth](../../strongs/h/h7198.md) of [nᵊḥûšâ](../../strongs/h/h5154.md) shall [ḥālap̄](../../strongs/h/h2498.md) him.

<a name="job_20_25"></a>Job 20:25

It is [šālap̄](../../strongs/h/h8025.md), and [yāṣā'](../../strongs/h/h3318.md) of the [gēvâ](../../strongs/h/h1465.md); yea, the [baraq](../../strongs/h/h1300.md) [halak](../../strongs/h/h1980.md) of his [mᵊrōrâ](../../strongs/h/h4846.md): ['êmâ](../../strongs/h/h367.md) are upon him.

<a name="job_20_26"></a>Job 20:26

All [choshek](../../strongs/h/h2822.md) shall be [taman](../../strongs/h/h2934.md) in his [tsaphan](../../strongs/h/h6845.md): an ['esh](../../strongs/h/h784.md) not [nāp̄aḥ](../../strongs/h/h5301.md) shall ['akal](../../strongs/h/h398.md) him; it shall go [yāraʿ](../../strongs/h/h3415.md) with him that is [śārîḏ](../../strongs/h/h8300.md) in his ['ohel](../../strongs/h/h168.md).

<a name="job_20_27"></a>Job 20:27

The [shamayim](../../strongs/h/h8064.md) shall [gālâ](../../strongs/h/h1540.md) his ['avon](../../strongs/h/h5771.md); and the ['erets](../../strongs/h/h776.md) shall [quwm](../../strongs/h/h6965.md) against him.

<a name="job_20_28"></a>Job 20:28

The [yᵊḇûl](../../strongs/h/h2981.md) of his [bayith](../../strongs/h/h1004.md) shall [gālâ](../../strongs/h/h1540.md), and his goods shall [nāḡar](../../strongs/h/h5064.md) in the [yowm](../../strongs/h/h3117.md) of his ['aph](../../strongs/h/h639.md).

<a name="job_20_29"></a>Job 20:29

This is the [cheleq](../../strongs/h/h2506.md) of a [rasha'](../../strongs/h/h7563.md) ['āḏām](../../strongs/h/h120.md) from ['Elohiym](../../strongs/h/h430.md), and the [nachalah](../../strongs/h/h5159.md) ['emer](../../strongs/h/h561.md) unto him by ['el](../../strongs/h/h410.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 19](job_19.md) - [Job 21](job_21.md)