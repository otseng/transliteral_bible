# [Job 16](https://www.blueletterbible.org/kjv/job/16)

<a name="job_16_1"></a>Job 16:1

Then ['Îyôḇ](../../strongs/h/h347.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md),

<a name="job_16_2"></a>Job 16:2

I have [shama'](../../strongs/h/h8085.md) many such [rab](../../strongs/h/h7227.md): ['amal](../../strongs/h/h5999.md) [nacham](../../strongs/h/h5162.md) are ye all.

<a name="job_16_3"></a>Job 16:3

Shall [ruwach](../../strongs/h/h7307.md) [dabar](../../strongs/h/h1697.md) have a [qēṣ](../../strongs/h/h7093.md)? or what [māraṣ](../../strongs/h/h4834.md) thee that thou ['anah](../../strongs/h/h6030.md)?

<a name="job_16_4"></a>Job 16:4

I also could [dabar](../../strongs/h/h1696.md) as ye do: if your [nephesh](../../strongs/h/h5315.md) were in my [nephesh](../../strongs/h/h5315.md) stead, I could [ḥāḇar](../../strongs/h/h2266.md) [millâ](../../strongs/h/h4405.md) against you, and [nûaʿ](../../strongs/h/h5128.md) mine [ro'sh](../../strongs/h/h7218.md) [bᵊmô](../../strongs/h/h1119.md) you.

<a name="job_16_5"></a>Job 16:5

But I would ['amats](../../strongs/h/h553.md) you [bᵊmô](../../strongs/h/h1119.md) my [peh](../../strongs/h/h6310.md), and the [nîḏ](../../strongs/h/h5205.md) of my [saphah](../../strongs/h/h8193.md) should [ḥāśaḵ](../../strongs/h/h2820.md) your grief.

<a name="job_16_6"></a>Job 16:6

Though I [dabar](../../strongs/h/h1696.md), my [kᵊ'ēḇ](../../strongs/h/h3511.md) is not [ḥāśaḵ](../../strongs/h/h2820.md): and though I [ḥāḏal](../../strongs/h/h2308.md), what am I [halak](../../strongs/h/h1980.md)?

<a name="job_16_7"></a>Job 16:7

But now he hath made me [lā'â](../../strongs/h/h3811.md): thou hast made [šāmēm](../../strongs/h/h8074.md) all my ['edah](../../strongs/h/h5712.md).

<a name="job_16_8"></a>Job 16:8

And thou hast filled me with [qāmaṭ](../../strongs/h/h7059.md), which is an ['ed](../../strongs/h/h5707.md) against me: and my [kaḥaš](../../strongs/h/h3585.md) [quwm](../../strongs/h/h6965.md) in me ['anah](../../strongs/h/h6030.md) to my [paniym](../../strongs/h/h6440.md).

<a name="job_16_9"></a>Job 16:9

He [taraph](../../strongs/h/h2963.md) me in his ['aph](../../strongs/h/h639.md), who [śāṭam](../../strongs/h/h7852.md) me: he [ḥāraq](../../strongs/h/h2786.md) upon me with his [šēn](../../strongs/h/h8127.md); mine [tsar](../../strongs/h/h6862.md) [lāṭaš](../../strongs/h/h3913.md) his ['ayin](../../strongs/h/h5869.md) upon me.

<a name="job_16_10"></a>Job 16:10

They have [pāʿar](../../strongs/h/h6473.md) upon me with their [peh](../../strongs/h/h6310.md); they have [nakah](../../strongs/h/h5221.md) me upon the [lᵊḥî](../../strongs/h/h3895.md) [cherpah](../../strongs/h/h2781.md); they have [mālā'](../../strongs/h/h4390.md) themselves [yaḥaḏ](../../strongs/h/h3162.md) against me.

<a name="job_16_11"></a>Job 16:11

['el](../../strongs/h/h410.md) hath [cagar](../../strongs/h/h5462.md) me to the [ʿăvîl](../../strongs/h/h5760.md), and [yāraṭ](../../strongs/h/h3399.md) me into the [yad](../../strongs/h/h3027.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="job_16_12"></a>Job 16:12

I was at [šālēv](../../strongs/h/h7961.md), but he hath [pārar](../../strongs/h/h6565.md) me: he hath also ['āḥaz](../../strongs/h/h270.md) me by my [ʿōrep̄](../../strongs/h/h6203.md), and [puwts](../../strongs/h/h6327.md) me, and [quwm](../../strongs/h/h6965.md) me for his [maṭṭārâ](../../strongs/h/h4307.md).

<a name="job_16_13"></a>Job 16:13

His [raḇ](../../strongs/h/h7228.md) [cabab](../../strongs/h/h5437.md) me, he [pālaḥ](../../strongs/h/h6398.md) my [kilyah](../../strongs/h/h3629.md) [pālaḥ](../../strongs/h/h6398.md), and doth not [ḥāmal](../../strongs/h/h2550.md); he [šāp̄aḵ](../../strongs/h/h8210.md) my [mᵊrērâ](../../strongs/h/h4845.md) upon the ['erets](../../strongs/h/h776.md).

<a name="job_16_14"></a>Job 16:14

He [pāraṣ](../../strongs/h/h6555.md) me with [pereṣ](../../strongs/h/h6556.md) [paniym](../../strongs/h/h6440.md) [pereṣ](../../strongs/h/h6556.md), he [rûṣ](../../strongs/h/h7323.md) upon me like a [gibôr](../../strongs/h/h1368.md).

<a name="job_16_15"></a>Job 16:15

I have [taphar](../../strongs/h/h8609.md) [śaq](../../strongs/h/h8242.md) upon my [geleḏ](../../strongs/h/h1539.md), and [ʿālal](../../strongs/h/h5953.md) my [qeren](../../strongs/h/h7161.md) in the ['aphar](../../strongs/h/h6083.md).

<a name="job_16_16"></a>Job 16:16

My [paniym](../../strongs/h/h6440.md) is [ḥāmar](../../strongs/h/h2560.md) with [bĕkiy](../../strongs/h/h1065.md), and on my ['aph'aph](../../strongs/h/h6079.md) is the [ṣalmāveṯ](../../strongs/h/h6757.md);

<a name="job_16_17"></a>Job 16:17

Not for any [chamac](../../strongs/h/h2555.md) in mine [kaph](../../strongs/h/h3709.md): also my [tĕphillah](../../strongs/h/h8605.md) is [zāḵ](../../strongs/h/h2134.md).

<a name="job_16_18"></a>Job 16:18

O ['erets](../../strongs/h/h776.md), [kāsâ](../../strongs/h/h3680.md) not thou my [dam](../../strongs/h/h1818.md), and let my [zaʿaq](../../strongs/h/h2201.md) have no [maqowm](../../strongs/h/h4725.md).

<a name="job_16_19"></a>Job 16:19

Also now, behold, my ['ed](../../strongs/h/h5707.md) is in [shamayim](../../strongs/h/h8064.md), and my [śāhēḏ](../../strongs/h/h7717.md) is on [marowm](../../strongs/h/h4791.md).

<a name="job_16_20"></a>Job 16:20

My [rea'](../../strongs/h/h7453.md) [luwts](../../strongs/h/h3887.md) me: but mine ['ayin](../../strongs/h/h5869.md) [dālap̄](../../strongs/h/h1811.md) unto ['ĕlvôha](../../strongs/h/h433.md).

<a name="job_16_21"></a>Job 16:21

O that one might [yakach](../../strongs/h/h3198.md) for a [geḇer](../../strongs/h/h1397.md) with ['ĕlvôha](../../strongs/h/h433.md), as an ['āḏām](../../strongs/h/h120.md) pleadeth for his [ben](../../strongs/h/h1121.md) [rea'](../../strongs/h/h7453.md)!

<a name="job_16_22"></a>Job 16:22

When a [mispār](../../strongs/h/h4557.md) [šānâ](../../strongs/h/h8141.md) are ['āṯâ](../../strongs/h/h857.md), then I shall [halak](../../strongs/h/h1980.md) the ['orach](../../strongs/h/h734.md) whence I shall not [shuwb](../../strongs/h/h7725.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 15](job_15.md) - [Job 17](job_17.md)