# [Job 1](https://www.blueletterbible.org/kjv/job/1)

<a name="job_1_1"></a>Job 1:1

There was an ['iysh](../../strongs/h/h376.md) in the ['erets](../../strongs/h/h776.md) of [ʿÛṣ](../../strongs/h/h5780.md), whose [shem](../../strongs/h/h8034.md) was ['Îyôḇ](../../strongs/h/h347.md); and that ['iysh](../../strongs/h/h376.md) was [tām](../../strongs/h/h8535.md) and [yashar](../../strongs/h/h3477.md), and one that [yārē'](../../strongs/h/h3373.md) ['Elohiym](../../strongs/h/h430.md), and [cuwr](../../strongs/h/h5493.md) [ra'](../../strongs/h/h7451.md).

<a name="job_1_2"></a>Job 1:2

And there were [yalad](../../strongs/h/h3205.md) unto him seven [ben](../../strongs/h/h1121.md) and three [bath](../../strongs/h/h1323.md).

<a name="job_1_3"></a>Job 1:3

His [miqnê](../../strongs/h/h4735.md) also was seven thousand [tso'n](../../strongs/h/h6629.md), and three thousand [gāmāl](../../strongs/h/h1581.md), and five hundred [ṣemeḏ](../../strongs/h/h6776.md) of [bāqār](../../strongs/h/h1241.md), and five hundred she ['āṯôn](../../strongs/h/h860.md), and a [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md) [ʿăḇudâ](../../strongs/h/h5657.md); so that this ['iysh](../../strongs/h/h376.md) was the [gadowl](../../strongs/h/h1419.md) of all the [ben](../../strongs/h/h1121.md) of the [qeḏem](../../strongs/h/h6924.md).

<a name="job_1_4"></a>Job 1:4

And his [ben](../../strongs/h/h1121.md) [halak](../../strongs/h/h1980.md) and [mištê](../../strongs/h/h4960.md) ['asah](../../strongs/h/h6213.md) in their [bayith](../../strongs/h/h1004.md), every ['iysh](../../strongs/h/h376.md) his [yowm](../../strongs/h/h3117.md); and [shalach](../../strongs/h/h7971.md) and [qara'](../../strongs/h/h7121.md) for their three ['āḥôṯ](../../strongs/h/h269.md) to ['akal](../../strongs/h/h398.md) and to [šāṯâ](../../strongs/h/h8354.md) with them.

<a name="job_1_5"></a>Job 1:5

And it was so, when the [yowm](../../strongs/h/h3117.md) of their [mištê](../../strongs/h/h4960.md) were [naqaph](../../strongs/h/h5362.md), that ['Îyôḇ](../../strongs/h/h347.md) [shalach](../../strongs/h/h7971.md) and [qadash](../../strongs/h/h6942.md) them, and [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) according to the [mispār](../../strongs/h/h4557.md) of them all: for ['Îyôḇ](../../strongs/h/h347.md) ['āmar](../../strongs/h/h559.md), It may be that my [ben](../../strongs/h/h1121.md) have [chata'](../../strongs/h/h2398.md), and [barak](../../strongs/h/h1288.md) ['Elohiym](../../strongs/h/h430.md) in their [lebab](../../strongs/h/h3824.md). Thus ['asah](../../strongs/h/h6213.md) ['Îyôḇ](../../strongs/h/h347.md) [yowm](../../strongs/h/h3117.md).

<a name="job_1_6"></a>Job 1:6

Now there was a [yowm](../../strongs/h/h3117.md) when the [ben](../../strongs/h/h1121.md) of ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) to [yatsab](../../strongs/h/h3320.md) themselves before [Yĕhovah](../../strongs/h/h3068.md), and [satan](../../strongs/h/h7854.md) [bow'](../../strongs/h/h935.md) also among them.

<a name="job_1_7"></a>Job 1:7

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [satan](../../strongs/h/h7854.md), Whence [bow'](../../strongs/h/h935.md) thou? Then [satan](../../strongs/h/h7854.md) ['anah](../../strongs/h/h6030.md) [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), [šûṭ](../../strongs/h/h7751.md) in the ['erets](../../strongs/h/h776.md), and [halak](../../strongs/h/h1980.md) in it.

<a name="job_1_8"></a>Job 1:8

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [satan](../../strongs/h/h7854.md), Hast thou [śûm](../../strongs/h/h7760.md) [leb](../../strongs/h/h3820.md) my ['ebed](../../strongs/h/h5650.md) ['Îyôḇ](../../strongs/h/h347.md), that there is none like him in the ['erets](../../strongs/h/h776.md), a [tām](../../strongs/h/h8535.md) and a [yashar](../../strongs/h/h3477.md) ['iysh](../../strongs/h/h376.md), one that [yārē'](../../strongs/h/h3373.md) ['Elohiym](../../strongs/h/h430.md), and [cuwr](../../strongs/h/h5493.md) [ra'](../../strongs/h/h7451.md)?

<a name="job_1_9"></a>Job 1:9

Then [satan](../../strongs/h/h7854.md) ['anah](../../strongs/h/h6030.md) [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), Doth ['Îyôḇ](../../strongs/h/h347.md) [yare'](../../strongs/h/h3372.md) ['Elohiym](../../strongs/h/h430.md) for [ḥinnām](../../strongs/h/h2600.md)?

<a name="job_1_10"></a>Job 1:10

Hast not thou made an [śûḵ](../../strongs/h/h7753.md) about him, and about his [bayith](../../strongs/h/h1004.md), and about all that he hath on every [cabiyb](../../strongs/h/h5439.md)? thou hast [barak](../../strongs/h/h1288.md) the [ma'aseh](../../strongs/h/h4639.md) of his [yad](../../strongs/h/h3027.md), and his [miqnê](../../strongs/h/h4735.md) is [pāraṣ](../../strongs/h/h6555.md) in the ['erets](../../strongs/h/h776.md).

<a name="job_1_11"></a>Job 1:11

But [shalach](../../strongs/h/h7971.md) thine [yad](../../strongs/h/h3027.md) now, and [naga'](../../strongs/h/h5060.md) all that he hath, and he will [barak](../../strongs/h/h1288.md) [lō'](../../strongs/h/h3808.md) thee to thy [paniym](../../strongs/h/h6440.md).

<a name="job_1_12"></a>Job 1:12

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [satan](../../strongs/h/h7854.md), Behold, all that he hath is in thy [yad](../../strongs/h/h3027.md); only upon himself put not [shalach](../../strongs/h/h7971.md) thine [yad](../../strongs/h/h3027.md). So [satan](../../strongs/h/h7854.md) [yāṣā'](../../strongs/h/h3318.md) from the [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="job_1_13"></a>Job 1:13

And there was a [yowm](../../strongs/h/h3117.md) when his [ben](../../strongs/h/h1121.md) and his [bath](../../strongs/h/h1323.md) were ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md) in their [bᵊḵôr](../../strongs/h/h1060.md) ['ach](../../strongs/h/h251.md) [bayith](../../strongs/h/h1004.md):

<a name="job_1_14"></a>Job 1:14

And there [bow'](../../strongs/h/h935.md) a [mal'ak](../../strongs/h/h4397.md) unto ['Îyôḇ](../../strongs/h/h347.md), and ['āmar](../../strongs/h/h559.md), The [bāqār](../../strongs/h/h1241.md) were [ḥāraš](../../strongs/h/h2790.md), and the ['āṯôn](../../strongs/h/h860.md) [ra'ah](../../strongs/h/h7462.md) [yad](../../strongs/h/h3027.md) them:

<a name="job_1_15"></a>Job 1:15

And the [Šᵊḇā'](../../strongs/h/h7614.md) [naphal](../../strongs/h/h5307.md) upon them, and [laqach](../../strongs/h/h3947.md) them; yea, they have [nakah](../../strongs/h/h5221.md) the [naʿar](../../strongs/h/h5288.md) with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md); and I only am [mālaṭ](../../strongs/h/h4422.md) alone to [nāḡaḏ](../../strongs/h/h5046.md) thee.

<a name="job_1_16"></a>Job 1:16

While he was yet [dabar](../../strongs/h/h1696.md), there [bow'](../../strongs/h/h935.md) also another, and ['āmar](../../strongs/h/h559.md), The ['esh](../../strongs/h/h784.md) of ['Elohiym](../../strongs/h/h430.md) is [naphal](../../strongs/h/h5307.md) from [shamayim](../../strongs/h/h8064.md), and hath [bāʿar](../../strongs/h/h1197.md) the [tso'n](../../strongs/h/h6629.md), and the [naʿar](../../strongs/h/h5288.md), and ['akal](../../strongs/h/h398.md) them; and I only am [mālaṭ](../../strongs/h/h4422.md) alone to [nāḡaḏ](../../strongs/h/h5046.md) thee.

<a name="job_1_17"></a>Job 1:17

While he was yet [dabar](../../strongs/h/h1696.md), there [bow'](../../strongs/h/h935.md) also another, and ['āmar](../../strongs/h/h559.md), The [Kaśdîmâ](../../strongs/h/h3778.md) made [śûm](../../strongs/h/h7760.md) three [ro'sh](../../strongs/h/h7218.md), and [pāšaṭ](../../strongs/h/h6584.md) upon the [gāmāl](../../strongs/h/h1581.md), and have [laqach](../../strongs/h/h3947.md) them, yea, and [nakah](../../strongs/h/h5221.md) the [naʿar](../../strongs/h/h5288.md) with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md); and I only am [mālaṭ](../../strongs/h/h4422.md) alone to [nāḡaḏ](../../strongs/h/h5046.md) thee.

<a name="job_1_18"></a>Job 1:18

While he was yet [dabar](../../strongs/h/h1696.md), there [bow'](../../strongs/h/h935.md) also another, and ['āmar](../../strongs/h/h559.md), Thy [ben](../../strongs/h/h1121.md) and thy [bath](../../strongs/h/h1323.md) were ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md) in their [bᵊḵôr](../../strongs/h/h1060.md) ['ach](../../strongs/h/h251.md) [bayith](../../strongs/h/h1004.md):

<a name="job_1_19"></a>Job 1:19

And, behold, there [bow'](../../strongs/h/h935.md) a [gadowl](../../strongs/h/h1419.md) [ruwach](../../strongs/h/h7307.md) [ʿēḇer](../../strongs/h/h5676.md) the [midbar](../../strongs/h/h4057.md), and [naga'](../../strongs/h/h5060.md) the four [pinnâ](../../strongs/h/h6438.md) of the [bayith](../../strongs/h/h1004.md), and it [naphal](../../strongs/h/h5307.md) upon the [naʿar](../../strongs/h/h5288.md), and they are [muwth](../../strongs/h/h4191.md); and I only am [mālaṭ](../../strongs/h/h4422.md) alone to [nāḡaḏ](../../strongs/h/h5046.md) thee.

<a name="job_1_20"></a>Job 1:20

Then ['Îyôḇ](../../strongs/h/h347.md) [quwm](../../strongs/h/h6965.md), and [qāraʿ](../../strongs/h/h7167.md) his [mᵊʿîl](../../strongs/h/h4598.md), and [gāzaz](../../strongs/h/h1494.md) his [ro'sh](../../strongs/h/h7218.md), and [naphal](../../strongs/h/h5307.md) upon the ['erets](../../strongs/h/h776.md), and [shachah](../../strongs/h/h7812.md),

<a name="job_1_21"></a>Job 1:21

And ['āmar](../../strongs/h/h559.md), ['arowm](../../strongs/h/h6174.md) came I [yāṣā'](../../strongs/h/h3318.md) of my ['em](../../strongs/h/h517.md) [beten](../../strongs/h/h990.md), and ['arowm](../../strongs/h/h6174.md) shall I [shuwb](../../strongs/h/h7725.md) thither: [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md), and [Yĕhovah](../../strongs/h/h3068.md) hath [laqach](../../strongs/h/h3947.md); [barak](../../strongs/h/h1288.md) be the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="job_1_22"></a>Job 1:22

In all this ['Îyôḇ](../../strongs/h/h347.md) [chata'](../../strongs/h/h2398.md) not, nor [nathan](../../strongs/h/h5414.md) ['Elohiym](../../strongs/h/h430.md) [tip̄lâ](../../strongs/h/h8604.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 2](job_2.md)