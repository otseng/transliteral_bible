# [Job 40](https://www.blueletterbible.org/kjv/job/40)

<a name="job_40_1"></a>Job 40:1

Moreover [Yĕhovah](../../strongs/h/h3068.md) ['anah](../../strongs/h/h6030.md) ['Îyôḇ](../../strongs/h/h347.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_40_2"></a>Job 40:2

Shall he that [riyb](../../strongs/h/h7378.md) with the [Šaday](../../strongs/h/h7706.md) [yissôr](../../strongs/h/h3250.md) him? he that [yakach](../../strongs/h/h3198.md) ['ĕlvôha](../../strongs/h/h433.md), let him ['anah](../../strongs/h/h6030.md) it.

<a name="job_40_3"></a>Job 40:3

Then ['Îyôḇ](../../strongs/h/h347.md) ['anah](../../strongs/h/h6030.md) [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_40_4"></a>Job 40:4

Behold, I am [qālal](../../strongs/h/h7043.md); what shall I [shuwb](../../strongs/h/h7725.md) thee? I will [śûm](../../strongs/h/h7760.md) mine [yad](../../strongs/h/h3027.md) [lᵊmô](../../strongs/h/h3926.md) my [peh](../../strongs/h/h6310.md).

<a name="job_40_5"></a>Job 40:5

Once have I [dabar](../../strongs/h/h1696.md); but I will not ['anah](../../strongs/h/h6030.md): yea, twice; but I will proceed no further.

<a name="job_40_6"></a>Job 40:6

Then ['anah](../../strongs/h/h6030.md) [Yĕhovah](../../strongs/h/h3068.md) unto ['Îyôḇ](../../strongs/h/h347.md) out of the [saʿar](../../strongs/h/h5591.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_40_7"></a>Job 40:7

['āzar](../../strongs/h/h247.md) thy [ḥālāṣ](../../strongs/h/h2504.md) now like a [geḇer](../../strongs/h/h1397.md): I will [sha'al](../../strongs/h/h7592.md) of thee, and [yada'](../../strongs/h/h3045.md) thou unto me.

<a name="job_40_8"></a>Job 40:8

Wilt thou also [pārar](../../strongs/h/h6565.md) my [mishpat](../../strongs/h/h4941.md)? wilt thou [rāšaʿ](../../strongs/h/h7561.md) me, that thou mayest be [ṣāḏaq](../../strongs/h/h6663.md)?

<a name="job_40_9"></a>Job 40:9

Hast thou a [zerowa'](../../strongs/h/h2220.md) like ['el](../../strongs/h/h410.md)? or canst thou [ra'am](../../strongs/h/h7481.md) with a [qowl](../../strongs/h/h6963.md) like him?

<a name="job_40_10"></a>Job 40:10

[ʿāḏâ](../../strongs/h/h5710.md) thyself now with [gā'ôn](../../strongs/h/h1347.md) and [gobahh](../../strongs/h/h1363.md); and [labash](../../strongs/h/h3847.md) thyself with [howd](../../strongs/h/h1935.md) and [hadar](../../strongs/h/h1926.md).

<a name="job_40_11"></a>Job 40:11

[puwts](../../strongs/h/h6327.md) the ['ebrah](../../strongs/h/h5678.md) of thy ['aph](../../strongs/h/h639.md): and [ra'ah](../../strongs/h/h7200.md) every one that is [gē'ê](../../strongs/h/h1343.md), and [šāp̄ēl](../../strongs/h/h8213.md) him.

<a name="job_40_12"></a>Job 40:12

[ra'ah](../../strongs/h/h7200.md) on every one that is [gē'ê](../../strongs/h/h1343.md), and bring him [kānaʿ](../../strongs/h/h3665.md); and tread [hāḏaḵ](../../strongs/h/h1915.md) the [rasha'](../../strongs/h/h7563.md) in their place.

<a name="job_40_13"></a>Job 40:13

[taman](../../strongs/h/h2934.md) them in the ['aphar](../../strongs/h/h6083.md) [yaḥaḏ](../../strongs/h/h3162.md); and [ḥāḇaš](../../strongs/h/h2280.md) their [paniym](../../strongs/h/h6440.md) in [taman](../../strongs/h/h2934.md).

<a name="job_40_14"></a>Job 40:14

Then will I also [yadah](../../strongs/h/h3034.md) unto thee that thine own [yamiyn](../../strongs/h/h3225.md) can [yasha'](../../strongs/h/h3467.md) thee.

<a name="job_40_15"></a>Job 40:15

Behold now [bahămôṯ](../../strongs/h/h930.md), which I ['asah](../../strongs/h/h6213.md) with thee; he ['akal](../../strongs/h/h398.md) [chatsiyr](../../strongs/h/h2682.md) as a [bāqār](../../strongs/h/h1241.md).

<a name="job_40_16"></a>Job 40:16

Lo now, his [koach](../../strongs/h/h3581.md) is in his [māṯnayim](../../strongs/h/h4975.md), and his ['ôn](../../strongs/h/h202.md) is in the [šārîr](../../strongs/h/h8306.md) of his [beten](../../strongs/h/h990.md).

<a name="job_40_17"></a>Job 40:17

He [ḥāp̄ēṣ](../../strongs/h/h2654.md) his [zānāḇ](../../strongs/h/h2180.md) like an ['erez](../../strongs/h/h730.md): the [gîḏ](../../strongs/h/h1517.md) of his [p̄ḥḏ](../../strongs/h/h6344.md) are [śāraḡ](../../strongs/h/h8276.md).

<a name="job_40_18"></a>Job 40:18

His ['etsem](../../strongs/h/h6106.md) are as ['āp̄îq](../../strongs/h/h650.md) pieces of [nᵊḥûšâ](../../strongs/h/h5154.md); his [gerem](../../strongs/h/h1634.md) are like [mᵊṭîl](../../strongs/h/h4300.md) of [barzel](../../strongs/h/h1270.md).

<a name="job_40_19"></a>Job 40:19

He is the [re'shiyth](../../strongs/h/h7225.md) of the [derek](../../strongs/h/h1870.md) of ['el](../../strongs/h/h410.md): he that ['asah](../../strongs/h/h6213.md) him can [nāḡaš](../../strongs/h/h5066.md) his [chereb](../../strongs/h/h2719.md) to [nāḡaš](../../strongs/h/h5066.md) unto him.

<a name="job_40_20"></a>Job 40:20

Surely the [har](../../strongs/h/h2022.md) [nasa'](../../strongs/h/h5375.md) him [bûl](../../strongs/h/h944.md), where all the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) [śāḥaq](../../strongs/h/h7832.md).

<a name="job_40_21"></a>Job 40:21

He [shakab](../../strongs/h/h7901.md) under the [ṣe'ĕlîm](../../strongs/h/h6628.md), in the [cether](../../strongs/h/h5643.md) of the [qānê](../../strongs/h/h7070.md), and [biṣṣâ](../../strongs/h/h1207.md).

<a name="job_40_22"></a>Job 40:22

The [ṣe'ĕlîm](../../strongs/h/h6628.md) [cakak](../../strongs/h/h5526.md) him with their [ṣl](../../strongs/h/h6752.md); the [ʿărāḇâ](../../strongs/h/h6155.md) of the [nachal](../../strongs/h/h5158.md) compass him [cabab](../../strongs/h/h5437.md).

<a name="job_40_23"></a>Job 40:23

Behold, he [ʿāšaq](../../strongs/h/h6231.md) a [nāhār](../../strongs/h/h5104.md), and [ḥāp̄az](../../strongs/h/h2648.md) not: he [batach](../../strongs/h/h982.md) that he can [gîaḥ](../../strongs/h/h1518.md) [Yardēn](../../strongs/h/h3383.md) into his [peh](../../strongs/h/h6310.md).

<a name="job_40_24"></a>Job 40:24

He [laqach](../../strongs/h/h3947.md) it with his ['ayin](../../strongs/h/h5869.md): his ['aph](../../strongs/h/h639.md) [nāqaḇ](../../strongs/h/h5344.md) [mowqesh](../../strongs/h/h4170.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 39](job_39.md) - [Job 41](job_41.md)