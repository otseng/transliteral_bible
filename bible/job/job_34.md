# [Job 34](https://www.blueletterbible.org/kjv/job/34)

<a name="job_34_1"></a>Job 34:1

Furthermore ['Ĕlîhû](../../strongs/h/h453.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md),

<a name="job_34_2"></a>Job 34:2

[shama'](../../strongs/h/h8085.md) my [millâ](../../strongs/h/h4405.md), O ye [ḥāḵām](../../strongs/h/h2450.md) men; and ['azan](../../strongs/h/h238.md) unto me, ye that have [yada'](../../strongs/h/h3045.md).

<a name="job_34_3"></a>Job 34:3

For the ['ozen](../../strongs/h/h241.md) [bachan](../../strongs/h/h974.md) [millâ](../../strongs/h/h4405.md), as the [ḥēḵ](../../strongs/h/h2441.md) [ṭāʿam](../../strongs/h/h2938.md) ['akal](../../strongs/h/h398.md).

<a name="job_34_4"></a>Job 34:4

Let us [bāḥar](../../strongs/h/h977.md) to us [mishpat](../../strongs/h/h4941.md): let us [yada'](../../strongs/h/h3045.md) among ourselves what is [towb](../../strongs/h/h2896.md).

<a name="job_34_5"></a>Job 34:5

For ['Îyôḇ](../../strongs/h/h347.md) hath ['āmar](../../strongs/h/h559.md), I am [ṣāḏaq](../../strongs/h/h6663.md): and ['el](../../strongs/h/h410.md) hath [cuwr](../../strongs/h/h5493.md) my [mishpat](../../strongs/h/h4941.md).

<a name="job_34_6"></a>Job 34:6

Should I [kāzaḇ](../../strongs/h/h3576.md) against my [mishpat](../../strongs/h/h4941.md)? my [chets](../../strongs/h/h2671.md) is ['anash](../../strongs/h/h605.md) without [pesha'](../../strongs/h/h6588.md).

<a name="job_34_7"></a>Job 34:7

What [geḇer](../../strongs/h/h1397.md) is like ['Îyôḇ](../../strongs/h/h347.md), who [šāṯâ](../../strongs/h/h8354.md) [laʿaḡ](../../strongs/h/h3933.md) like [mayim](../../strongs/h/h4325.md)?

<a name="job_34_8"></a>Job 34:8

Which ['āraḥ](../../strongs/h/h732.md) in [ḥeḇrâ](../../strongs/h/h2274.md) with the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md), and [yālaḵ](../../strongs/h/h3212.md) with [resha'](../../strongs/h/h7562.md) ['enowsh](../../strongs/h/h582.md).

<a name="job_34_9"></a>Job 34:9

For he hath ['āmar](../../strongs/h/h559.md), It [sāḵan](../../strongs/h/h5532.md) a [geḇer](../../strongs/h/h1397.md) nothing that he should [ratsah](../../strongs/h/h7521.md) himself with ['Elohiym](../../strongs/h/h430.md).

<a name="job_34_10"></a>Job 34:10

Therefore [shama'](../../strongs/h/h8085.md) unto me, ye ['enowsh](../../strongs/h/h582.md) of [lebab](../../strongs/h/h3824.md): far be [ḥālîlâ](../../strongs/h/h2486.md) from ['el](../../strongs/h/h410.md), that he should do [resha'](../../strongs/h/h7562.md); and from the [Šaday](../../strongs/h/h7706.md), that he should commit ['evel](../../strongs/h/h5766.md).

<a name="job_34_11"></a>Job 34:11

For the [pōʿal](../../strongs/h/h6467.md) of an ['āḏām](../../strongs/h/h120.md) shall he [shalam](../../strongs/h/h7999.md) unto him, and cause every ['iysh](../../strongs/h/h376.md) to [māṣā'](../../strongs/h/h4672.md) according to his ['orach](../../strongs/h/h734.md).

<a name="job_34_12"></a>Job 34:12

Yea, ['āmnām](../../strongs/h/h551.md) ['el](../../strongs/h/h410.md) will not do [rāšaʿ](../../strongs/h/h7561.md), neither will the [Šaday](../../strongs/h/h7706.md) [ʿāvaṯ](../../strongs/h/h5791.md) [mishpat](../../strongs/h/h4941.md).

<a name="job_34_13"></a>Job 34:13

Who hath given him a [paqad](../../strongs/h/h6485.md) over the ['erets](../../strongs/h/h776.md)? or who hath [śûm](../../strongs/h/h7760.md) the whole [tebel](../../strongs/h/h8398.md)?

<a name="job_34_14"></a>Job 34:14

If he [śûm](../../strongs/h/h7760.md) his [leb](../../strongs/h/h3820.md) upon man, if he ['āsap̄](../../strongs/h/h622.md) unto himself his [ruwach](../../strongs/h/h7307.md) and his [neshamah](../../strongs/h/h5397.md);

<a name="job_34_15"></a>Job 34:15

All [basar](../../strongs/h/h1320.md) shall [gāvaʿ](../../strongs/h/h1478.md) [yaḥaḏ](../../strongs/h/h3162.md), and ['āḏām](../../strongs/h/h120.md) shall [shuwb](../../strongs/h/h7725.md) unto ['aphar](../../strongs/h/h6083.md).

<a name="job_34_16"></a>Job 34:16

If now thou hast [bînâ](../../strongs/h/h998.md), [shama'](../../strongs/h/h8085.md) this: ['azan](../../strongs/h/h238.md) to the [qowl](../../strongs/h/h6963.md) of my [millâ](../../strongs/h/h4405.md).

<a name="job_34_17"></a>Job 34:17

Shall even he that [sane'](../../strongs/h/h8130.md) [mishpat](../../strongs/h/h4941.md) [ḥāḇaš](../../strongs/h/h2280.md)? and wilt thou [rāšaʿ](../../strongs/h/h7561.md) him that is [kabîr](../../strongs/h/h3524.md) [tsaddiyq](../../strongs/h/h6662.md)?

<a name="job_34_18"></a>Job 34:18

Is it fit to ['āmar](../../strongs/h/h559.md) to a [melek](../../strongs/h/h4428.md), Thou art [beliya'al](../../strongs/h/h1100.md)? and to [nāḏîḇ](../../strongs/h/h5081.md), Ye are [rasha'](../../strongs/h/h7563.md)?

<a name="job_34_19"></a>Job 34:19

How much less to him that [nasa'](../../strongs/h/h5375.md) not the [paniym](../../strongs/h/h6440.md) of [śar](../../strongs/h/h8269.md), nor [nāḵar](../../strongs/h/h5234.md) the [šôaʿ](../../strongs/h/h7771.md) more [paniym](../../strongs/h/h6440.md) the [dal](../../strongs/h/h1800.md)? for they all are the [ma'aseh](../../strongs/h/h4639.md) of his [yad](../../strongs/h/h3027.md).

<a name="job_34_20"></a>Job 34:20

In a [reḡaʿ](../../strongs/h/h7281.md) shall they [muwth](../../strongs/h/h4191.md), and the ['am](../../strongs/h/h5971.md) shall be [gāʿaš](../../strongs/h/h1607.md) at [ḥāṣôṯ](../../strongs/h/h2676.md) [layil](../../strongs/h/h3915.md), and ['abar](../../strongs/h/h5674.md): and the ['abîr](../../strongs/h/h47.md) shall be [cuwr](../../strongs/h/h5493.md) without [yad](../../strongs/h/h3027.md).

<a name="job_34_21"></a>Job 34:21

For his ['ayin](../../strongs/h/h5869.md) are upon the [derek](../../strongs/h/h1870.md) of ['iysh](../../strongs/h/h376.md), and he [ra'ah](../../strongs/h/h7200.md) all his [ṣaʿaḏ](../../strongs/h/h6806.md).

<a name="job_34_22"></a>Job 34:22

There is no [choshek](../../strongs/h/h2822.md), nor [ṣalmāveṯ](../../strongs/h/h6757.md), where the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md) may [cathar](../../strongs/h/h5641.md) themselves.

<a name="job_34_23"></a>Job 34:23

For he will not [śûm](../../strongs/h/h7760.md) upon ['iysh](../../strongs/h/h376.md) more than right; that he should [halak](../../strongs/h/h1980.md) into [mishpat](../../strongs/h/h4941.md) with ['el](../../strongs/h/h410.md).

<a name="job_34_24"></a>Job 34:24

He shall [ra'a'](../../strongs/h/h7489.md) [kabîr](../../strongs/h/h3524.md) without [ḥēqer](../../strongs/h/h2714.md), and ['amad](../../strongs/h/h5975.md) ['aḥēr](../../strongs/h/h312.md) in their stead.

<a name="job_34_25"></a>Job 34:25

Therefore he [nāḵar](../../strongs/h/h5234.md) their [maʿbāḏ](../../strongs/h/h4566.md), and he [hāp̄aḵ](../../strongs/h/h2015.md) them in the [layil](../../strongs/h/h3915.md), so that they are [dāḵā'](../../strongs/h/h1792.md).

<a name="job_34_26"></a>Job 34:26

He [sāp̄aq](../../strongs/h/h5606.md) them as [rasha'](../../strongs/h/h7563.md) in the [maqowm](../../strongs/h/h4725.md) [ra'ah](../../strongs/h/h7200.md) of others;

<a name="job_34_27"></a>Job 34:27

Because they [cuwr](../../strongs/h/h5493.md) from ['aḥar](../../strongs/h/h310.md), and would not [sakal](../../strongs/h/h7919.md) any of his [derek](../../strongs/h/h1870.md):

<a name="job_34_28"></a>Job 34:28

So that they cause the [tsa'aqah](../../strongs/h/h6818.md) of the [dal](../../strongs/h/h1800.md) to [bow'](../../strongs/h/h935.md) unto him, and he [shama'](../../strongs/h/h8085.md) the [tsa'aqah](../../strongs/h/h6818.md) of the ['aniy](../../strongs/h/h6041.md).

<a name="job_34_29"></a>Job 34:29

When he giveth [šāqaṭ](../../strongs/h/h8252.md), who then can make [rāšaʿ](../../strongs/h/h7561.md)? and when he [cathar](../../strongs/h/h5641.md) his [paniym](../../strongs/h/h6440.md), who then can [šûr](../../strongs/h/h7789.md) him? whether it be done against a [gowy](../../strongs/h/h1471.md), or against an ['āḏām](../../strongs/h/h120.md) [yaḥaḏ](../../strongs/h/h3162.md):

<a name="job_34_30"></a>Job 34:30

That the ['āḏām](../../strongs/h/h120.md) [ḥānēp̄](../../strongs/h/h2611.md) [mālaḵ](../../strongs/h/h4427.md) not, lest the ['am](../../strongs/h/h5971.md) be [mowqesh](../../strongs/h/h4170.md).

<a name="job_34_31"></a>Job 34:31

Surely it is meet to be ['āmar](../../strongs/h/h559.md) unto ['el](../../strongs/h/h410.md), I have [nasa'](../../strongs/h/h5375.md) chastisement, I will not [chabal](../../strongs/h/h2254.md) any more:

<a name="job_34_32"></a>Job 34:32

That which I [chazah](../../strongs/h/h2372.md) [bilʿăḏê](../../strongs/h/h1107.md) [yārâ](../../strongs/h/h3384.md) thou me: if I have [pa'al](../../strongs/h/h6466.md) ['evel](../../strongs/h/h5766.md), I will do no more.

<a name="job_34_33"></a>Job 34:33

Should it be according to thy mind? he will [shalam](../../strongs/h/h7999.md) it, whether thou refuse [mā'as](../../strongs/h/h3988.md), or whether thou [bāḥar](../../strongs/h/h977.md); and not I: therefore [dabar](../../strongs/h/h1696.md) what thou [yada'](../../strongs/h/h3045.md).

<a name="job_34_34"></a>Job 34:34

Let ['enowsh](../../strongs/h/h582.md) of [lebab](../../strongs/h/h3824.md) ['āmar](../../strongs/h/h559.md) me, and let a [ḥāḵām](../../strongs/h/h2450.md) [geḇer](../../strongs/h/h1397.md) [shama'](../../strongs/h/h8085.md) unto me.

<a name="job_34_35"></a>Job 34:35

['Îyôḇ](../../strongs/h/h347.md) hath [dabar](../../strongs/h/h1696.md) without [da'ath](../../strongs/h/h1847.md), and his [dabar](../../strongs/h/h1697.md) were without [sakal](../../strongs/h/h7919.md).

<a name="job_34_36"></a>Job 34:36

My ['āḇâ](../../strongs/h/h15.md) ['ab](../../strongs/h/h1.md) is that ['Îyôḇ](../../strongs/h/h347.md) may be [bachan](../../strongs/h/h974.md) unto the [netsach](../../strongs/h/h5331.md) because of his [tᵊšûḇâ](../../strongs/h/h8666.md) for ['aven](../../strongs/h/h205.md) ['enowsh](../../strongs/h/h582.md).

<a name="job_34_37"></a>Job 34:37

For he addeth [pesha'](../../strongs/h/h6588.md) unto his [chatta'ath](../../strongs/h/h2403.md), he [sāp̄aq](../../strongs/h/h5606.md) his hands among us, and [rabah](../../strongs/h/h7235.md) his ['emer](../../strongs/h/h561.md) against ['el](../../strongs/h/h410.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 33](job_33.md) - [Job 35](job_35.md)