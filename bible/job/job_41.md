# [Job 41](https://www.blueletterbible.org/kjv/job/41)

<a name="job_41_1"></a>Job 41:1

Canst thou [mashak](../../strongs/h/h4900.md) [livyāṯān](../../strongs/h/h3882.md) with an [ḥakâ](../../strongs/h/h2443.md)? or his [lashown](../../strongs/h/h3956.md) with a [chebel](../../strongs/h/h2256.md) which thou lettest [šāqaʿ](../../strongs/h/h8257.md)?

<a name="job_41_2"></a>Job 41:2

Canst thou [śûm](../../strongs/h/h7760.md) an ['aḡmôn](../../strongs/h/h100.md) into his ['aph](../../strongs/h/h639.md)? or [nāqaḇ](../../strongs/h/h5344.md) his [lᵊḥî](../../strongs/h/h3895.md) through with a [ḥôaḥ](../../strongs/h/h2336.md)?

<a name="job_41_3"></a>Job 41:3

Will he make [rabah](../../strongs/h/h7235.md) [taḥănûn](../../strongs/h/h8469.md) unto thee? will he [dabar](../../strongs/h/h1696.md) [raḵ](../../strongs/h/h7390.md) unto thee?

<a name="job_41_4"></a>Job 41:4

Will he [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with thee? wilt thou [laqach](../../strongs/h/h3947.md) him for an ['ebed](../../strongs/h/h5650.md) ['owlam](../../strongs/h/h5769.md)?

<a name="job_41_5"></a>Job 41:5

Wilt thou [śāḥaq](../../strongs/h/h7832.md) with him as with a [tsippowr](../../strongs/h/h6833.md)? or wilt thou [qāšar](../../strongs/h/h7194.md) him for thy [naʿărâ](../../strongs/h/h5291.md)?

<a name="job_41_6"></a>Job 41:6

Shall the [ḥabār](../../strongs/h/h2271.md) make a [kārâ](../../strongs/h/h3739.md) of him? shall they [ḥāṣâ](../../strongs/h/h2673.md) him among the [Kᵊnaʿănî](../../strongs/h/h3669.md)?

<a name="job_41_7"></a>Job 41:7

Canst thou [mālā'](../../strongs/h/h4390.md) his ['owr](../../strongs/h/h5785.md) with [śukâ](../../strongs/h/h7905.md)? or his [ro'sh](../../strongs/h/h7218.md) with [dag](../../strongs/h/h1709.md) [ṣᵊlāṣal](../../strongs/h/h6767.md)?

<a name="job_41_8"></a>Job 41:8

[śûm](../../strongs/h/h7760.md) thine [kaph](../../strongs/h/h3709.md) upon him, [zakar](../../strongs/h/h2142.md) the [milḥāmâ](../../strongs/h/h4421.md), do no more.

<a name="job_41_9"></a>Job 41:9

Behold, the [tôḥeleṯ](../../strongs/h/h8431.md) of him is in [kāzaḇ](../../strongs/h/h3576.md): shall not one be [ṭûl](../../strongs/h/h2904.md) even at the [mar'ê](../../strongs/h/h4758.md) of him?

<a name="job_41_10"></a>Job 41:10

None is so ['aḵzār](../../strongs/h/h393.md) that dare stir him [ʿûr](../../strongs/h/h5782.md) [ʿûr](../../strongs/h/h5782.md): who then is able to [yatsab](../../strongs/h/h3320.md) [paniym](../../strongs/h/h6440.md) me?

<a name="job_41_11"></a>Job 41:11

Who hath [qadam](../../strongs/h/h6923.md) me, that I should [shalam](../../strongs/h/h7999.md) him? whatsoever is under the [shamayim](../../strongs/h/h8064.md) is mine.

<a name="job_41_12"></a>Job 41:12

I will not [ḥāraš](../../strongs/h/h2790.md) his [baḏ](../../strongs/h/h907.md), nor his [gᵊḇûrâ](../../strongs/h/h1369.md) [dabar](../../strongs/h/h1697.md), nor his [ḥîn](../../strongs/h/h2433.md) [ʿēreḵ](../../strongs/h/h6187.md).

<a name="job_41_13"></a>Job 41:13

Who can [gālâ](../../strongs/h/h1540.md) the [paniym](../../strongs/h/h6440.md) of his [lᵊḇûš](../../strongs/h/h3830.md)? or who can [bow'](../../strongs/h/h935.md) to him with his [kep̄el](../../strongs/h/h3718.md) [resen](../../strongs/h/h7448.md)?

<a name="job_41_14"></a>Job 41:14

Who can [pāṯaḥ](../../strongs/h/h6605.md) the [deleṯ](../../strongs/h/h1817.md) of his [paniym](../../strongs/h/h6440.md)? his [šēn](../../strongs/h/h8127.md) are ['êmâ](../../strongs/h/h367.md) [cabiyb](../../strongs/h/h5439.md).

<a name="job_41_15"></a>Job 41:15

His [magen](../../strongs/h/h4043.md) ['āp̄îq](../../strongs/h/h650.md) are his [ga'avah](../../strongs/h/h1346.md), [cagar](../../strongs/h/h5462.md) as with a [tsar](../../strongs/h/h6862.md) [ḥôṯām](../../strongs/h/h2368.md).

<a name="job_41_16"></a>Job 41:16

One is so [nāḡaš](../../strongs/h/h5066.md) to another, that no [ruwach](../../strongs/h/h7307.md) can [bow'](../../strongs/h/h935.md) between them.

<a name="job_41_17"></a>Job 41:17

They are [dāḇaq](../../strongs/h/h1692.md) ['iysh](../../strongs/h/h376.md) to ['ach](../../strongs/h/h251.md), they [lāḵaḏ](../../strongs/h/h3920.md), that they cannot be [pāraḏ](../../strongs/h/h6504.md).

<a name="job_41_18"></a>Job 41:18

By his [ʿăṭîšâ](../../strongs/h/h5846.md) a ['owr](../../strongs/h/h216.md) doth [halal](../../strongs/h/h1984.md), and his ['ayin](../../strongs/h/h5869.md) are like the ['aph'aph](../../strongs/h/h6079.md) of the [šaḥar](../../strongs/h/h7837.md).

<a name="job_41_19"></a>Job 41:19

Out of his [peh](../../strongs/h/h6310.md) [halak](../../strongs/h/h1980.md) [lapîḏ](../../strongs/h/h3940.md), and [kîḏôḏ](../../strongs/h/h3590.md) of ['esh](../../strongs/h/h784.md) leap [mālaṭ](../../strongs/h/h4422.md).

<a name="job_41_20"></a>Job 41:20

Out of his [nᵊḥîr](../../strongs/h/h5156.md) [yāṣā'](../../strongs/h/h3318.md) ['ashan](../../strongs/h/h6227.md), as out of a [nāp̄aḥ](../../strongs/h/h5301.md) [dûḏ](../../strongs/h/h1731.md) or ['aḡmôn](../../strongs/h/h100.md).

<a name="job_41_21"></a>Job 41:21

His [nephesh](../../strongs/h/h5315.md) [lāhaṭ](../../strongs/h/h3857.md) [gechel](../../strongs/h/h1513.md), and a [lahaḇ](../../strongs/h/h3851.md) [yāṣā'](../../strongs/h/h3318.md) of his [peh](../../strongs/h/h6310.md).

<a name="job_41_22"></a>Job 41:22

In his [ṣaûā'r](../../strongs/h/h6677.md) [lûn](../../strongs/h/h3885.md) ['oz](../../strongs/h/h5797.md), and [dᵊ'āḇâ](../../strongs/h/h1670.md) is turned into [dûṣ](../../strongs/h/h1750.md) [paniym](../../strongs/h/h6440.md) him.

<a name="job_41_23"></a>Job 41:23

The [mapāl](../../strongs/h/h4651.md) of his [basar](../../strongs/h/h1320.md) are [dāḇaq](../../strongs/h/h1692.md): they are [yāṣaq](../../strongs/h/h3332.md) in themselves; they cannot be [mowt](../../strongs/h/h4131.md).

<a name="job_41_24"></a>Job 41:24

His [leb](../../strongs/h/h3820.md) is as [yāṣaq](../../strongs/h/h3332.md) as an ['eben](../../strongs/h/h68.md); yea, as [yāṣaq](../../strongs/h/h3332.md) as a [pelaḥ](../../strongs/h/h6400.md) of the [taḥtî](../../strongs/h/h8482.md) millstone.

<a name="job_41_25"></a>Job 41:25

When he [śᵊ'ēṯ](../../strongs/h/h7613.md) himself, the ['ayil](../../strongs/h/h352.md) are [guwr](../../strongs/h/h1481.md): by [šeḇar](../../strongs/h/h7667.md) they [chata'](../../strongs/h/h2398.md) themselves.

<a name="job_41_26"></a>Job 41:26

The [chereb](../../strongs/h/h2719.md) of him that [nāśaḡ](../../strongs/h/h5381.md) at him cannot [quwm](../../strongs/h/h6965.md): the [ḥănîṯ](../../strongs/h/h2595.md), the [massāʿ](../../strongs/h/h4551.md), nor the [širyôn](../../strongs/h/h8302.md).

<a name="job_41_27"></a>Job 41:27

He [chashab](../../strongs/h/h2803.md) [barzel](../../strongs/h/h1270.md) as [teḇen](../../strongs/h/h8401.md), and [nᵊḥûšâ](../../strongs/h/h5154.md) as [riqqāḇôn](../../strongs/h/h7539.md) ['ets](../../strongs/h/h6086.md).

<a name="job_41_28"></a>Job 41:28

The [ben](../../strongs/h/h1121.md) [qesheth](../../strongs/h/h7198.md) cannot make him [bāraḥ](../../strongs/h/h1272.md): ['eben](../../strongs/h/h68.md) [qelaʿ](../../strongs/h/h7050.md) are [hāp̄aḵ](../../strongs/h/h2015.md) with him into [qaš](../../strongs/h/h7179.md).

<a name="job_41_29"></a>Job 41:29

[tôṯāḥ](../../strongs/h/h8455.md) are [chashab](../../strongs/h/h2803.md) as [qaš](../../strongs/h/h7179.md): he [śāḥaq](../../strongs/h/h7832.md) at the [raʿaš](../../strongs/h/h7494.md) of a [kîḏôn](../../strongs/h/h3591.md).

<a name="job_41_30"></a>Job 41:30

[ḥadûḏ](../../strongs/h/h2303.md) [ḥereś](../../strongs/h/h2789.md) are under him: he [rāp̄aḏ](../../strongs/h/h7502.md) [ḥārûṣ](../../strongs/h/h2742.md) upon the [ṭîṭ](../../strongs/h/h2916.md).

<a name="job_41_31"></a>Job 41:31

He maketh the [mᵊṣôlâ](../../strongs/h/h4688.md) to [rāṯaḥ](../../strongs/h/h7570.md) like a [sîr](../../strongs/h/h5518.md): he [śûm](../../strongs/h/h7760.md) the [yam](../../strongs/h/h3220.md) like a pot of [merqāḥâ](../../strongs/h/h4841.md).

<a name="job_41_32"></a>Job 41:32

He maketh a [nāṯîḇ](../../strongs/h/h5410.md) to ['owr](../../strongs/h/h215.md) ['aḥar](../../strongs/h/h310.md) him; one would [chashab](../../strongs/h/h2803.md) the [tĕhowm](../../strongs/h/h8415.md) to be [śêḇâ](../../strongs/h/h7872.md).

<a name="job_41_33"></a>Job 41:33

Upon ['aphar](../../strongs/h/h6083.md) there is not his [mōšel](../../strongs/h/h4915.md), who is ['asah](../../strongs/h/h6213.md) without [ḥaṯ](../../strongs/h/h2844.md).

<a name="job_41_34"></a>Job 41:34

He [ra'ah](../../strongs/h/h7200.md) all [gāḇōha](../../strongs/h/h1364.md) things: he is a [melek](../../strongs/h/h4428.md) over all the [ben](../../strongs/h/h1121.md) of [šaḥaṣ](../../strongs/h/h7830.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 40](job_40.md) - [Job 42](job_42.md)