# [Job 25](https://www.blueletterbible.org/kjv/job/25)

<a name="job_25_1"></a>Job 25:1

Then ['anah](../../strongs/h/h6030.md) [Bildaḏ](../../strongs/h/h1085.md) the [šûḥî](../../strongs/h/h7747.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_25_2"></a>Job 25:2

[mashal](../../strongs/h/h4910.md) and [paḥaḏ](../../strongs/h/h6343.md) are with him, he ['asah](../../strongs/h/h6213.md) [shalowm](../../strongs/h/h7965.md) in his [marowm](../../strongs/h/h4791.md).

<a name="job_25_3"></a>Job 25:3

Is there any [mispār](../../strongs/h/h4557.md) of his [gᵊḏûḏ](../../strongs/h/h1416.md)? and upon whom doth not his ['owr](../../strongs/h/h216.md) [quwm](../../strongs/h/h6965.md)?

<a name="job_25_4"></a>Job 25:4

How then can ['enowsh](../../strongs/h/h582.md) be [ṣāḏaq](../../strongs/h/h6663.md) with ['el](../../strongs/h/h410.md)? or how can he be [zāḵâ](../../strongs/h/h2135.md) that is [yalad](../../strongs/h/h3205.md) of an ['ishshah](../../strongs/h/h802.md)?

<a name="job_25_5"></a>Job 25:5

Behold even to the [yareach](../../strongs/h/h3394.md), and it ['āhal](../../strongs/h/h166.md) not; yea, the [kowkab](../../strongs/h/h3556.md) are not [zāḵaḵ](../../strongs/h/h2141.md) in his ['ayin](../../strongs/h/h5869.md).

<a name="job_25_6"></a>Job 25:6

How much less ['enowsh](../../strongs/h/h582.md), that is a [rimmâ](../../strongs/h/h7415.md)? and the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), which is a [tôlāʿ](../../strongs/h/h8438.md)?

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 24](job_24.md) - [Job 26](job_26.md)