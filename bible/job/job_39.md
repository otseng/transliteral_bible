# [Job 39](https://www.blueletterbible.org/kjv/job/39)

<a name="job_39_1"></a>Job 39:1

[yada'](../../strongs/h/h3045.md) thou the [ʿēṯ](../../strongs/h/h6256.md) when the [yāʿēl](../../strongs/h/h3277.md) of the [cela'](../../strongs/h/h5553.md) [yalad](../../strongs/h/h3205.md)? or canst thou [shamar](../../strongs/h/h8104.md) when the ['ayyālâ](../../strongs/h/h355.md) do [chuwl](../../strongs/h/h2342.md)?

<a name="job_39_2"></a>Job 39:2

Canst thou [sāp̄ar](../../strongs/h/h5608.md) the [yeraḥ](../../strongs/h/h3391.md) that they [mālā'](../../strongs/h/h4390.md)? or [yada'](../../strongs/h/h3045.md) thou the [ʿēṯ](../../strongs/h/h6256.md) when they [yalad](../../strongs/h/h3205.md)?

<a name="job_39_3"></a>Job 39:3

They [kara'](../../strongs/h/h3766.md) themselves, they [pālaḥ](../../strongs/h/h6398.md) their [yeleḏ](../../strongs/h/h3206.md), they [shalach](../../strongs/h/h7971.md) their [chebel](../../strongs/h/h2256.md).

<a name="job_39_4"></a>Job 39:4

Their [ben](../../strongs/h/h1121.md) are in [ḥālam](../../strongs/h/h2492.md), they [rabah](../../strongs/h/h7235.md) with [bar](../../strongs/h/h1250.md); they [yāṣā'](../../strongs/h/h3318.md), and [shuwb](../../strongs/h/h7725.md) not unto them.

<a name="job_39_5"></a>Job 39:5

Who hath [shalach](../../strongs/h/h7971.md) the [pere'](../../strongs/h/h6501.md) [ḥāp̄šî](../../strongs/h/h2670.md)? or who hath [pāṯaḥ](../../strongs/h/h6605.md) the [mowcer](../../strongs/h/h4147.md) of the [ʿārôḏ](../../strongs/h/h6171.md)?

<a name="job_39_6"></a>Job 39:6

Whose [bayith](../../strongs/h/h1004.md) I have [śûm](../../strongs/h/h7760.md) the ['arabah](../../strongs/h/h6160.md), and the [mᵊlēḥâ](../../strongs/h/h4420.md) land his [miškān](../../strongs/h/h4908.md).

<a name="job_39_7"></a>Job 39:7

He [śāḥaq](../../strongs/h/h7832.md) the [hāmôn](../../strongs/h/h1995.md) of the [qiryâ](../../strongs/h/h7151.md), neither [shama'](../../strongs/h/h8085.md) he the [tᵊšu'â](../../strongs/h/h8663.md) of the [nāḡaś](../../strongs/h/h5065.md).

<a name="job_39_8"></a>Job 39:8

The [yᵊṯûr](../../strongs/h/h3491.md) of the [har](../../strongs/h/h2022.md) is his [mirʿê](../../strongs/h/h4829.md), and he [darash](../../strongs/h/h1875.md) ['aḥar](../../strongs/h/h310.md) every [yārôq](../../strongs/h/h3387.md).

<a name="job_39_9"></a>Job 39:9

Will the [rᵊ'ēm](../../strongs/h/h7214.md) be ['āḇâ](../../strongs/h/h14.md) to ['abad](../../strongs/h/h5647.md) thee, or [lûn](../../strongs/h/h3885.md) by thy ['ēḇûs](../../strongs/h/h18.md)?

<a name="job_39_10"></a>Job 39:10

Canst thou [qāšar](../../strongs/h/h7194.md) the [rᵊ'ēm](../../strongs/h/h7214.md) with his [ʿăḇōṯ](../../strongs/h/h5688.md) in the [telem](../../strongs/h/h8525.md)? or will he [śāḏaḏ](../../strongs/h/h7702.md) the [ʿēmeq](../../strongs/h/h6010.md) ['aḥar](../../strongs/h/h310.md) thee?

<a name="job_39_11"></a>Job 39:11

Wilt thou [batach](../../strongs/h/h982.md) him, because his [koach](../../strongs/h/h3581.md) is [rab](../../strongs/h/h7227.md)? or wilt thou ['azab](../../strongs/h/h5800.md) thy [yᵊḡîaʿ](../../strongs/h/h3018.md) to him?

<a name="job_39_12"></a>Job 39:12

Wilt thou ['aman](../../strongs/h/h539.md) him, that he will [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) thy [zera'](../../strongs/h/h2233.md), and ['āsap̄](../../strongs/h/h622.md) it into thy [gōren](../../strongs/h/h1637.md)?

<a name="job_39_13"></a>Job 39:13

Gavest thou the [rᵊnānîm](../../strongs/h/h7443.md) [kanaph](../../strongs/h/h3671.md) unto the [ʿālas](../../strongs/h/h5965.md)? or ['ēḇrâ](../../strongs/h/h84.md) and [ḥăsîḏâ](../../strongs/h/h2624.md) unto the [nôṣâ](../../strongs/h/h5133.md)?

<a name="job_39_14"></a>Job 39:14

Which ['azab](../../strongs/h/h5800.md) her [bêṣâ](../../strongs/h/h1000.md) in the ['erets](../../strongs/h/h776.md), and [ḥāmam](../../strongs/h/h2552.md) them in ['aphar](../../strongs/h/h6083.md),

<a name="job_39_15"></a>Job 39:15

And [shakach](../../strongs/h/h7911.md) that the [regel](../../strongs/h/h7272.md) may [zûr](../../strongs/h/h2115.md) them, or that the [sadeh](../../strongs/h/h7704.md) [chay](../../strongs/h/h2416.md) may [dûš](../../strongs/h/h1758.md) them.

<a name="job_39_16"></a>Job 39:16

She is [qāšaḥ](../../strongs/h/h7188.md) against her [ben](../../strongs/h/h1121.md), as though they were not hers: her [yᵊḡîaʿ](../../strongs/h/h3018.md) is in [riyq](../../strongs/h/h7385.md) without [paḥaḏ](../../strongs/h/h6343.md);

<a name="job_39_17"></a>Job 39:17

Because ['ĕlvôha](../../strongs/h/h433.md) hath [nāšâ](../../strongs/h/h5382.md) her of [ḥāḵmâ](../../strongs/h/h2451.md), neither hath he [chalaq](../../strongs/h/h2505.md) to her [bînâ](../../strongs/h/h998.md).

<a name="job_39_18"></a>Job 39:18

What [ʿēṯ](../../strongs/h/h6256.md) she [mārā'](../../strongs/h/h4754.md) herself on [marowm](../../strongs/h/h4791.md), she [śāḥaq](../../strongs/h/h7832.md) the [sûs](../../strongs/h/h5483.md) and his [rāḵaḇ](../../strongs/h/h7392.md).

<a name="job_39_19"></a>Job 39:19

Hast thou [nathan](../../strongs/h/h5414.md) the [sûs](../../strongs/h/h5483.md) [gᵊḇûrâ](../../strongs/h/h1369.md)? hast thou [labash](../../strongs/h/h3847.md) his [ṣaûā'r](../../strongs/h/h6677.md) with [Raʿmâ](../../strongs/h/h7483.md)?

<a name="job_39_20"></a>Job 39:20

Canst thou make him [rāʿaš](../../strongs/h/h7493.md) as an ['arbê](../../strongs/h/h697.md)? the [howd](../../strongs/h/h1935.md) of his [naḥar](../../strongs/h/h5170.md) is ['êmâ](../../strongs/h/h367.md).

<a name="job_39_21"></a>Job 39:21

He [chaphar](../../strongs/h/h2658.md) in the [ʿēmeq](../../strongs/h/h6010.md), and [śûś](../../strongs/h/h7797.md) in his [koach](../../strongs/h/h3581.md): he [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) the [nešeq](../../strongs/h/h5402.md).

<a name="job_39_22"></a>Job 39:22

He [śāḥaq](../../strongs/h/h7832.md) at [paḥaḏ](../../strongs/h/h6343.md), and is not [ḥāṯaṯ](../../strongs/h/h2865.md); neither he [shuwb](../../strongs/h/h7725.md) [paniym](../../strongs/h/h6440.md) the [chereb](../../strongs/h/h2719.md).

<a name="job_39_23"></a>Job 39:23

The ['ašpâ](../../strongs/h/h827.md) [rānâ](../../strongs/h/h7439.md) against him, the [lahaḇ](../../strongs/h/h3851.md) [ḥănîṯ](../../strongs/h/h2595.md) and the [kîḏôn](../../strongs/h/h3591.md).

<a name="job_39_24"></a>Job 39:24

He [gāmā'](../../strongs/h/h1572.md) the ['erets](../../strongs/h/h776.md) with [raʿaš](../../strongs/h/h7494.md) and [rōḡez](../../strongs/h/h7267.md): neither ['aman](../../strongs/h/h539.md) he that it is the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md).

<a name="job_39_25"></a>Job 39:25

He ['āmar](../../strongs/h/h559.md) [day](../../strongs/h/h1767.md) the [šôp̄ār](../../strongs/h/h7782.md), [he'āḥ](../../strongs/h/h1889.md), [he'āḥ](../../strongs/h/h1889.md); and he [rîaḥ](../../strongs/h/h7306.md) the [milḥāmâ](../../strongs/h/h4421.md) afar [rachowq](../../strongs/h/h7350.md), the [raʿam](../../strongs/h/h7482.md) of the [śar](../../strongs/h/h8269.md), and the [tᵊrûʿâ](../../strongs/h/h8643.md).

<a name="job_39_26"></a>Job 39:26

Doth the [nēṣ](../../strongs/h/h5322.md) ['āḇar](../../strongs/h/h82.md) by thy [bînâ](../../strongs/h/h998.md), and [pāraś](../../strongs/h/h6566.md) her [kanaph](../../strongs/h/h3671.md) toward the [têmān](../../strongs/h/h8486.md)?

<a name="job_39_27"></a>Job 39:27

Doth the [nesheׁr](../../strongs/h/h5404.md) [gāḇah](../../strongs/h/h1361.md) at thy [peh](../../strongs/h/h6310.md), and [ruwm](../../strongs/h/h7311.md) her [qēn](../../strongs/h/h7064.md) on [ruwm](../../strongs/h/h7311.md)?

<a name="job_39_28"></a>Job 39:28

She [shakan](../../strongs/h/h7931.md) and [lûn](../../strongs/h/h3885.md) on the [cela'](../../strongs/h/h5553.md), upon the [šēn](../../strongs/h/h8127.md) of the [cela'](../../strongs/h/h5553.md), and the [matsuwd](../../strongs/h/h4686.md).

<a name="job_39_29"></a>Job 39:29

From thence she [chaphar](../../strongs/h/h2658.md) the ['ōḵel](../../strongs/h/h400.md), and her ['ayin](../../strongs/h/h5869.md) [nabat](../../strongs/h/h5027.md) afar [rachowq](../../strongs/h/h7350.md).

<a name="job_39_30"></a>Job 39:30

Her ['ep̄rōaḥ](../../strongs/h/h667.md) also [ʿālaʿ](../../strongs/h/h5966.md) [dam](../../strongs/h/h1818.md): and where the [ḥālāl](../../strongs/h/h2491.md) are, there is she.

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 38](job_38.md) - [Job 40](job_40.md)