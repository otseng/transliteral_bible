# [Job 13](https://www.blueletterbible.org/kjv/job/13)

<a name="job_13_1"></a>Job 13:1

Lo, mine ['ayin](../../strongs/h/h5869.md) hath [ra'ah](../../strongs/h/h7200.md) all this, mine ['ozen](../../strongs/h/h241.md) hath [shama'](../../strongs/h/h8085.md) and [bîn](../../strongs/h/h995.md) it.

<a name="job_13_2"></a>Job 13:2

What ye [da'ath](../../strongs/h/h1847.md), the same do I [yada'](../../strongs/h/h3045.md) also: I am not [naphal](../../strongs/h/h5307.md) unto you.

<a name="job_13_3"></a>Job 13:3

Surely I would [dabar](../../strongs/h/h1696.md) to the [Šaday](../../strongs/h/h7706.md), and I [ḥāp̄ēṣ](../../strongs/h/h2654.md) to [yakach](../../strongs/h/h3198.md) with ['el](../../strongs/h/h410.md).

<a name="job_13_4"></a>Job 13:4

But ye are [ṭāp̄al](../../strongs/h/h2950.md) of [sheqer](../../strongs/h/h8267.md), ye are all [rapha'](../../strongs/h/h7495.md) of no ['ĕlîl](../../strongs/h/h457.md).

<a name="job_13_5"></a>Job 13:5

O [nathan](../../strongs/h/h5414.md) ye would [ḥāraš](../../strongs/h/h2790.md) hold your [ḥāraš](../../strongs/h/h2790.md)! and it should be your [ḥāḵmâ](../../strongs/h/h2451.md).

<a name="job_13_6"></a>Job 13:6

[shama'](../../strongs/h/h8085.md) now my [tôḵēḥâ](../../strongs/h/h8433.md), and [qashab](../../strongs/h/h7181.md) to the [rîḇ](../../strongs/h/h7379.md) of my [saphah](../../strongs/h/h8193.md).

<a name="job_13_7"></a>Job 13:7

Will ye [dabar](../../strongs/h/h1696.md) ['evel](../../strongs/h/h5766.md) for ['el](../../strongs/h/h410.md)? and [dabar](../../strongs/h/h1696.md) [rᵊmîyâ](../../strongs/h/h7423.md) for him?

<a name="job_13_8"></a>Job 13:8

Will ye [nasa'](../../strongs/h/h5375.md) his [paniym](../../strongs/h/h6440.md)? will ye [riyb](../../strongs/h/h7378.md) for ['el](../../strongs/h/h410.md)?

<a name="job_13_9"></a>Job 13:9

Is it [ṭôḇ](../../strongs/h/h2895.md) that he should search you [chaqar](../../strongs/h/h2713.md)? or as one ['enowsh](../../strongs/h/h582.md) [hāṯal](../../strongs/h/h2048.md) another, do ye so [hāṯal](../../strongs/h/h2048.md) him?

<a name="job_13_10"></a>Job 13:10

He will [yakach](../../strongs/h/h3198.md) [yakach](../../strongs/h/h3198.md) you, if ye do [cether](../../strongs/h/h5643.md) [nasa'](../../strongs/h/h5375.md) [paniym](../../strongs/h/h6440.md).

<a name="job_13_11"></a>Job 13:11

Shall not his [śᵊ'ēṯ](../../strongs/h/h7613.md) make you [ba'ath](../../strongs/h/h1204.md)? and his [paḥaḏ](../../strongs/h/h6343.md) [naphal](../../strongs/h/h5307.md) upon you?

<a name="job_13_12"></a>Job 13:12

Your [zikārôn](../../strongs/h/h2146.md) are [māšāl](../../strongs/h/h4912.md) unto ['ēp̄er](../../strongs/h/h665.md), your [gaḇ](../../strongs/h/h1354.md) to [gaḇ](../../strongs/h/h1354.md) of [ḥōmer](../../strongs/h/h2563.md).

<a name="job_13_13"></a>Job 13:13

Hold your [ḥāraš](../../strongs/h/h2790.md), let me alone, that I may [dabar](../../strongs/h/h1696.md), and let ['abar](../../strongs/h/h5674.md) on me what will.

<a name="job_13_14"></a>Job 13:14

Wherefore do I [nasa'](../../strongs/h/h5375.md) my [basar](../../strongs/h/h1320.md) in my [šēn](../../strongs/h/h8127.md), and [śûm](../../strongs/h/h7760.md) my [nephesh](../../strongs/h/h5315.md) in mine [kaph](../../strongs/h/h3709.md)?

<a name="job_13_15"></a>Job 13:15

[hen](../../strongs/h/h2005.md) he [qāṭal](../../strongs/h/h6991.md) me, yet will I [yāḥal](../../strongs/h/h3176.md) in him: but I will [yakach](../../strongs/h/h3198.md) mine own [derek](../../strongs/h/h1870.md) [paniym](../../strongs/h/h6440.md) him.

<a name="job_13_16"></a>Job 13:16

He also shall be my [yĕshuw'ah](../../strongs/h/h3444.md): for an [ḥānēp̄](../../strongs/h/h2611.md) shall not [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) him.

<a name="job_13_17"></a>Job 13:17

[shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) my [millâ](../../strongs/h/h4405.md), and my ['aḥvâ](../../strongs/h/h262.md) with your ['ozen](../../strongs/h/h241.md).

<a name="job_13_18"></a>Job 13:18

Behold now, I have ['arak](../../strongs/h/h6186.md) my [mishpat](../../strongs/h/h4941.md); I [yada'](../../strongs/h/h3045.md) that I shall be [ṣāḏaq](../../strongs/h/h6663.md).

<a name="job_13_19"></a>Job 13:19

Who is he that will [riyb](../../strongs/h/h7378.md) with me? for now, if I hold my [ḥāraš](../../strongs/h/h2790.md), I shall give up the [gāvaʿ](../../strongs/h/h1478.md).

<a name="job_13_20"></a>Job 13:20

Only ['asah](../../strongs/h/h6213.md) not two things unto me: then will I not [cathar](../../strongs/h/h5641.md) myself from [paniym](../../strongs/h/h6440.md).

<a name="job_13_21"></a>Job 13:21

[rachaq](../../strongs/h/h7368.md) thine [kaph](../../strongs/h/h3709.md) [rachaq](../../strongs/h/h7368.md) from me: and let not thy ['êmâ](../../strongs/h/h367.md) make me [ba'ath](../../strongs/h/h1204.md).

<a name="job_13_22"></a>Job 13:22

Then [qara'](../../strongs/h/h7121.md) thou, and I will ['anah](../../strongs/h/h6030.md): or let me [dabar](../../strongs/h/h1696.md), and [shuwb](../../strongs/h/h7725.md) thou me.

<a name="job_13_23"></a>Job 13:23

How many are mine ['avon](../../strongs/h/h5771.md) and [chatta'ath](../../strongs/h/h2403.md)? make me to [yada'](../../strongs/h/h3045.md) my [pesha'](../../strongs/h/h6588.md) and my [chatta'ath](../../strongs/h/h2403.md).

<a name="job_13_24"></a>Job 13:24

Wherefore [cathar](../../strongs/h/h5641.md) thou thy [paniym](../../strongs/h/h6440.md), and [chashab](../../strongs/h/h2803.md) me for thine ['oyeb](../../strongs/h/h341.md)?

<a name="job_13_25"></a>Job 13:25

Wilt thou [ʿāraṣ](../../strongs/h/h6206.md) an ['aleh](../../strongs/h/h5929.md) driven to and [nāḏap̄](../../strongs/h/h5086.md)? and wilt thou [radaph](../../strongs/h/h7291.md) the [yāḇēš](../../strongs/h/h3002.md) [qaš](../../strongs/h/h7179.md)?

<a name="job_13_26"></a>Job 13:26

For thou [kāṯaḇ](../../strongs/h/h3789.md) [mᵊrōrâ](../../strongs/h/h4846.md) against me, and makest me to [yarash](../../strongs/h/h3423.md) the ['avon](../../strongs/h/h5771.md) of my [nāʿur](../../strongs/h/h5271.md).

<a name="job_13_27"></a>Job 13:27

Thou [śûm](../../strongs/h/h7760.md) my [regel](../../strongs/h/h7272.md) also in the [saḏ](../../strongs/h/h5465.md), and lookest [shamar](../../strongs/h/h8104.md) unto all my ['orach](../../strongs/h/h734.md); thou settest a [ḥāqâ](../../strongs/h/h2707.md) upon the [šereš](../../strongs/h/h8328.md) of my [regel](../../strongs/h/h7272.md).

<a name="job_13_28"></a>Job 13:28

And he, as a [rāqāḇ](../../strongs/h/h7538.md), [bālâ](../../strongs/h/h1086.md), as a [beḡeḏ](../../strongs/h/h899.md) that is [ʿāš](../../strongs/h/h6211.md) ['akal](../../strongs/h/h398.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 12](job_12.md) - [Job 14](job_14.md)