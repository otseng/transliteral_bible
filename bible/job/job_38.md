# [Job 38](https://www.blueletterbible.org/kjv/job/38)

<a name="job_38_1"></a>Job 38:1

Then [Yĕhovah](../../strongs/h/h3068.md) ['anah](../../strongs/h/h6030.md) ['Îyôḇ](../../strongs/h/h347.md) out of the [saʿar](../../strongs/h/h5591.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_38_2"></a>Job 38:2

Who is this that [ḥāšaḵ](../../strongs/h/h2821.md) ['etsah](../../strongs/h/h6098.md) by [millâ](../../strongs/h/h4405.md) without [da'ath](../../strongs/h/h1847.md)?

<a name="job_38_3"></a>Job 38:3

['āzar](../../strongs/h/h247.md) now thy [ḥālāṣ](../../strongs/h/h2504.md) like a [geḇer](../../strongs/h/h1397.md); for I will [sha'al](../../strongs/h/h7592.md) of thee, and [yada'](../../strongs/h/h3045.md) thou me.

<a name="job_38_4"></a>Job 38:4

['êp̄ô](../../strongs/h/h375.md) wast thou when I [yacad](../../strongs/h/h3245.md) the ['erets](../../strongs/h/h776.md)? [nāḡaḏ](../../strongs/h/h5046.md), if thou [yada'](../../strongs/h/h3045.md) [bînâ](../../strongs/h/h998.md).

<a name="job_38_5"></a>Job 38:5

Who hath [śûm](../../strongs/h/h7760.md) the [mēmaḏ](../../strongs/h/h4461.md) thereof, if thou [yada'](../../strongs/h/h3045.md)? or who hath [natah](../../strongs/h/h5186.md) the [qāv](../../strongs/h/h6957.md) upon it?

<a name="job_38_6"></a>Job 38:6

Whereupon are the ['eḏen](../../strongs/h/h134.md) thereof [ṭāḇaʿ](../../strongs/h/h2883.md)? or who [yārâ](../../strongs/h/h3384.md) the [pinnâ](../../strongs/h/h6438.md) ['eben](../../strongs/h/h68.md) thereof;

<a name="job_38_7"></a>Job 38:7

When the [boqer](../../strongs/h/h1242.md) [kowkab](../../strongs/h/h3556.md) [ranan](../../strongs/h/h7442.md) [yaḥaḏ](../../strongs/h/h3162.md), and all the [ben](../../strongs/h/h1121.md) of ['Elohiym](../../strongs/h/h430.md) [rûaʿ](../../strongs/h/h7321.md) for joy?

<a name="job_38_8"></a>Job 38:8

Or who [cakak](../../strongs/h/h5526.md) the [yam](../../strongs/h/h3220.md) with [deleṯ](../../strongs/h/h1817.md), when it [gîaḥ](../../strongs/h/h1518.md), as if it had [yāṣā'](../../strongs/h/h3318.md) of the [reḥem](../../strongs/h/h7358.md)?

<a name="job_38_9"></a>Job 38:9

When I [śûm](../../strongs/h/h7760.md) the [ʿānān](../../strongs/h/h6051.md) the [lᵊḇûš](../../strongs/h/h3830.md) thereof, and ['araphel](../../strongs/h/h6205.md) a [ḥăṯullâ](../../strongs/h/h2854.md) for it,

<a name="job_38_10"></a>Job 38:10

And [shabar](../../strongs/h/h7665.md) for it my [choq](../../strongs/h/h2706.md), and [śûm](../../strongs/h/h7760.md) [bᵊrîaḥ](../../strongs/h/h1280.md) and [deleṯ](../../strongs/h/h1817.md),

<a name="job_38_11"></a>Job 38:11

And ['āmar](../../strongs/h/h559.md), Hitherto  shalt thou [bow'](../../strongs/h/h935.md), but no further: and here shall thy [gā'ôn](../../strongs/h/h1347.md) [gal](../../strongs/h/h1530.md) be [shiyth](../../strongs/h/h7896.md)?

<a name="job_38_12"></a>Job 38:12

Hast thou [tsavah](../../strongs/h/h6680.md) the [boqer](../../strongs/h/h1242.md) since thy [yowm](../../strongs/h/h3117.md); and caused the [šaḥar](../../strongs/h/h7837.md) to [yada'](../../strongs/h/h3045.md) his [maqowm](../../strongs/h/h4725.md);

<a name="job_38_13"></a>Job 38:13

That it might take ['āḥaz](../../strongs/h/h270.md) of the [kanaph](../../strongs/h/h3671.md) of the ['erets](../../strongs/h/h776.md), that the [rasha'](../../strongs/h/h7563.md) might be [nāʿar](../../strongs/h/h5287.md) of it?

<a name="job_38_14"></a>Job 38:14

It is [hāp̄aḵ](../../strongs/h/h2015.md) as [ḥōmer](../../strongs/h/h2563.md) to the [ḥôṯām](../../strongs/h/h2368.md); and they [yatsab](../../strongs/h/h3320.md) as a [lᵊḇûš](../../strongs/h/h3830.md).

<a name="job_38_15"></a>Job 38:15

And from the [rasha'](../../strongs/h/h7563.md) their ['owr](../../strongs/h/h216.md) is [mānaʿ](../../strongs/h/h4513.md), and the [ruwm](../../strongs/h/h7311.md) [zerowa'](../../strongs/h/h2220.md) shall be [shabar](../../strongs/h/h7665.md).

<a name="job_38_16"></a>Job 38:16

Hast thou [bow'](../../strongs/h/h935.md) into the [nēḇeḵ](../../strongs/h/h5033.md) of the [yam](../../strongs/h/h3220.md)? or hast thou [halak](../../strongs/h/h1980.md) in the [ḥēqer](../../strongs/h/h2714.md) of the [tĕhowm](../../strongs/h/h8415.md)?

<a name="job_38_17"></a>Job 38:17

Have the [sha'ar](../../strongs/h/h8179.md) of [maveth](../../strongs/h/h4194.md) been [gālâ](../../strongs/h/h1540.md) unto thee? or hast thou [ra'ah](../../strongs/h/h7200.md) the [sha'ar](../../strongs/h/h8179.md) of the [ṣalmāveṯ](../../strongs/h/h6757.md)?

<a name="job_38_18"></a>Job 38:18

Hast thou [bîn](../../strongs/h/h995.md) the [raḥaḇ](../../strongs/h/h7338.md) of the ['erets](../../strongs/h/h776.md)? [nāḡaḏ](../../strongs/h/h5046.md) if thou [yada'](../../strongs/h/h3045.md) it all.

<a name="job_38_19"></a>Job 38:19

Where is the [derek](../../strongs/h/h1870.md) where ['owr](../../strongs/h/h216.md) [shakan](../../strongs/h/h7931.md)? and as for [choshek](../../strongs/h/h2822.md), where is the [maqowm](../../strongs/h/h4725.md) thereof,

<a name="job_38_20"></a>Job 38:20

That thou shouldest [laqach](../../strongs/h/h3947.md) it to the [gᵊḇûl](../../strongs/h/h1366.md) thereof, and that thou shouldest [bîn](../../strongs/h/h995.md) the [nāṯîḇ](../../strongs/h/h5410.md) to the [bayith](../../strongs/h/h1004.md) thereof?

<a name="job_38_21"></a>Job 38:21

[yada'](../../strongs/h/h3045.md) thou it, because thou wast then [yalad](../../strongs/h/h3205.md)? or because the [mispār](../../strongs/h/h4557.md) of thy [yowm](../../strongs/h/h3117.md) is [rab](../../strongs/h/h7227.md)?

<a name="job_38_22"></a>Job 38:22

Hast thou [bow'](../../strongs/h/h935.md) into the ['ôṣār](../../strongs/h/h214.md) of the [šeleḡ](../../strongs/h/h7950.md)? or hast thou [ra'ah](../../strongs/h/h7200.md) the ['ôṣār](../../strongs/h/h214.md) of the [barad](../../strongs/h/h1259.md),

<a name="job_38_23"></a>Job 38:23

Which I have [ḥāśaḵ](../../strongs/h/h2820.md) against the [ʿēṯ](../../strongs/h/h6256.md) of [tsar](../../strongs/h/h6862.md), against the [yowm](../../strongs/h/h3117.md) of [qᵊrāḇ](../../strongs/h/h7128.md) and [milḥāmâ](../../strongs/h/h4421.md)?

<a name="job_38_24"></a>Job 38:24

By what [derek](../../strongs/h/h1870.md) is the ['owr](../../strongs/h/h216.md) [chalaq](../../strongs/h/h2505.md), which [puwts](../../strongs/h/h6327.md) the [qāḏîm](../../strongs/h/h6921.md) upon the ['erets](../../strongs/h/h776.md)?

<a name="job_38_25"></a>Job 38:25

Who hath [pālaḡ](../../strongs/h/h6385.md) a [tᵊʿālâ](../../strongs/h/h8585.md) for the [šeṭep̄](../../strongs/h/h7858.md), or a [derek](../../strongs/h/h1870.md) for the [ḥāzîz](../../strongs/h/h2385.md) of [qowl](../../strongs/h/h6963.md);

<a name="job_38_26"></a>Job 38:26

To cause it to [matar](../../strongs/h/h4305.md) on the ['erets](../../strongs/h/h776.md), where no ['iysh](../../strongs/h/h376.md) is; on the [midbar](../../strongs/h/h4057.md), wherein there is no ['āḏām](../../strongs/h/h120.md);

<a name="job_38_27"></a>Job 38:27

To [sāׂbaʿ](../../strongs/h/h7646.md) the [šô'](../../strongs/h/h7722.md) and [mᵊšô'â](../../strongs/h/h4875.md); and to cause the [môṣā'](../../strongs/h/h4161.md) of the [deše'](../../strongs/h/h1877.md) to [ṣāmaḥ](../../strongs/h/h6779.md)?

<a name="job_38_28"></a>Job 38:28

Hath the [māṭār](../../strongs/h/h4306.md) an ['ab](../../strongs/h/h1.md)? or who hath [yalad](../../strongs/h/h3205.md) the ['ēḡel](../../strongs/h/h96.md) of [ṭal](../../strongs/h/h2919.md)?

<a name="job_38_29"></a>Job 38:29

Out of whose [beten](../../strongs/h/h990.md) [yāṣā'](../../strongs/h/h3318.md) the [qeraḥ](../../strongs/h/h7140.md)? and the [kᵊp̄ôr](../../strongs/h/h3713.md) of [shamayim](../../strongs/h/h8064.md), who hath [yalad](../../strongs/h/h3205.md) it?

<a name="job_38_30"></a>Job 38:30

The [mayim](../../strongs/h/h4325.md) are [chaba'](../../strongs/h/h2244.md) as with an ['eben](../../strongs/h/h68.md), and the [paniym](../../strongs/h/h6440.md) of the [tĕhowm](../../strongs/h/h8415.md) is [lāḵaḏ](../../strongs/h/h3920.md).

<a name="job_38_31"></a>Job 38:31

Canst thou [qāšar](../../strongs/h/h7194.md) the [maʿăḏannâ](../../strongs/h/h4575.md) of [kîmâ](../../strongs/h/h3598.md), or [pāṯaḥ](../../strongs/h/h6605.md) the [môšḵôṯ](../../strongs/h/h4189.md) of [kᵊsîl](../../strongs/h/h3685.md)?

<a name="job_38_32"></a>Job 38:32

Canst thou [yāṣā'](../../strongs/h/h3318.md) [mazzārâ](../../strongs/h/h4216.md) in his [ʿēṯ](../../strongs/h/h6256.md)? or canst thou [nachah](../../strongs/h/h5148.md) [ʿayiš](../../strongs/h/h5906.md) with his [ben](../../strongs/h/h1121.md)?

<a name="job_38_33"></a>Job 38:33

[yada'](../../strongs/h/h3045.md) thou the [chuqqah](../../strongs/h/h2708.md) of [shamayim](../../strongs/h/h8064.md)? canst thou [śûm](../../strongs/h/h7760.md) the [mišṭār](../../strongs/h/h4896.md) thereof in the ['erets](../../strongs/h/h776.md)?

<a name="job_38_34"></a>Job 38:34

Canst thou [ruwm](../../strongs/h/h7311.md) thy [qowl](../../strongs/h/h6963.md) to the ['ab](../../strongs/h/h5645.md), that [šip̄ʿâ](../../strongs/h/h8229.md) of [mayim](../../strongs/h/h4325.md) may [kāsâ](../../strongs/h/h3680.md) thee?

<a name="job_38_35"></a>Job 38:35

Canst thou [shalach](../../strongs/h/h7971.md) [baraq](../../strongs/h/h1300.md), that they may [yālaḵ](../../strongs/h/h3212.md), and ['āmar](../../strongs/h/h559.md) unto thee, Here we are?

<a name="job_38_36"></a>Job 38:36

Who hath [shiyth](../../strongs/h/h7896.md) [ḥāḵmâ](../../strongs/h/h2451.md) in the inward [ṭuḥâ](../../strongs/h/h2910.md)? or who hath [nathan](../../strongs/h/h5414.md) [bînâ](../../strongs/h/h998.md) to the [śeḵvî](../../strongs/h/h7907.md)?

<a name="job_38_37"></a>Job 38:37

Who can [sāp̄ar](../../strongs/h/h5608.md) the [shachaq](../../strongs/h/h7834.md) in [ḥāḵmâ](../../strongs/h/h2451.md)? or who can [shakab](../../strongs/h/h7901.md) the [neḇel](../../strongs/h/h5035.md) of [shamayim](../../strongs/h/h8064.md),

<a name="job_38_38"></a>Job 38:38

When the ['aphar](../../strongs/h/h6083.md) [yāṣaq](../../strongs/h/h3332.md) into [mûṣāq](../../strongs/h/h4165.md), and the [reḡeḇ](../../strongs/h/h7263.md) [dāḇaq](../../strongs/h/h1692.md)?

<a name="job_38_39"></a>Job 38:39

Wilt thou [ṣûḏ](../../strongs/h/h6679.md) the [ṭerep̄](../../strongs/h/h2964.md) for the [lāḇî'](../../strongs/h/h3833.md)? or [mālā'](../../strongs/h/h4390.md) the [chay](../../strongs/h/h2416.md) of the [kephiyr](../../strongs/h/h3715.md),

<a name="job_38_40"></a>Job 38:40

When they [shachach](../../strongs/h/h7817.md) in their [mᵊʿônâ](../../strongs/h/h4585.md), and [yashab](../../strongs/h/h3427.md) in the [cukkah](../../strongs/h/h5521.md) [lᵊmô](../../strongs/h/h3926.md) lie in ['ereḇ](../../strongs/h/h695.md)?

<a name="job_38_41"></a>Job 38:41

Who [kuwn](../../strongs/h/h3559.md) for the [ʿōrēḇ](../../strongs/h/h6158.md) his [ṣayiḏ](../../strongs/h/h6718.md)? when his [yeleḏ](../../strongs/h/h3206.md) [šāvaʿ](../../strongs/h/h7768.md) unto ['el](../../strongs/h/h410.md), they [tāʿâ](../../strongs/h/h8582.md) for lack of ['ōḵel](../../strongs/h/h400.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 37](job_37.md) - [Job 39](job_39.md)