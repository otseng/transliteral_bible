# [Job 42](https://www.blueletterbible.org/kjv/job/42)

<a name="job_42_1"></a>Job 42:1

Then ['Îyôḇ](../../strongs/h/h347.md) ['anah](../../strongs/h/h6030.md) [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_42_2"></a>Job 42:2

I [yada'](../../strongs/h/h3045.md) that thou canst [yakol](../../strongs/h/h3201.md) every thing, and that no [mezimmah](../../strongs/h/h4209.md) can be [bāṣar](../../strongs/h/h1219.md) from thee.

<a name="job_42_3"></a>Job 42:3

Who is he that ['alam](../../strongs/h/h5956.md) ['etsah](../../strongs/h/h6098.md) without [da'ath](../../strongs/h/h1847.md)? therefore have I [nāḡaḏ](../../strongs/h/h5046.md) that I [bîn](../../strongs/h/h995.md) not; things too [pala'](../../strongs/h/h6381.md) for me, which I [yada'](../../strongs/h/h3045.md) not.

<a name="job_42_4"></a>Job 42:4

[shama'](../../strongs/h/h8085.md), I beseech thee, and I will [dabar](../../strongs/h/h1696.md): I will [sha'al](../../strongs/h/h7592.md) of thee, and [yada'](../../strongs/h/h3045.md) thou unto me.

<a name="job_42_5"></a>Job 42:5

I have [shama'](../../strongs/h/h8085.md) of thee by the [šēmaʿ](../../strongs/h/h8088.md) of the ['ozen](../../strongs/h/h241.md): but now mine ['ayin](../../strongs/h/h5869.md) [ra'ah](../../strongs/h/h7200.md) thee.

<a name="job_42_6"></a>Job 42:6

Wherefore I [mā'as](../../strongs/h/h3988.md) myself, and [nacham](../../strongs/h/h5162.md) in ['aphar](../../strongs/h/h6083.md) and ['ēp̄er](../../strongs/h/h665.md).

<a name="job_42_7"></a>Job 42:7

And it was so, that ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md) these [dabar](../../strongs/h/h1697.md) unto ['Îyôḇ](../../strongs/h/h347.md), [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) to ['Ĕlîp̄az](../../strongs/h/h464.md) the [têmānî](../../strongs/h/h8489.md), My ['aph](../../strongs/h/h639.md) is [ḥārâ](../../strongs/h/h2734.md) against thee, and against thy two [rea'](../../strongs/h/h7453.md): for ye have not [dabar](../../strongs/h/h1696.md) of me the thing that is [kuwn](../../strongs/h/h3559.md), as my ['ebed](../../strongs/h/h5650.md) ['Îyôḇ](../../strongs/h/h347.md) hath.

<a name="job_42_8"></a>Job 42:8

Therefore [laqach](../../strongs/h/h3947.md) unto you now seven [par](../../strongs/h/h6499.md) and seven ['ayil](../../strongs/h/h352.md), and [yālaḵ](../../strongs/h/h3212.md) to my ['ebed](../../strongs/h/h5650.md) ['Îyôḇ](../../strongs/h/h347.md), and [ʿālâ](../../strongs/h/h5927.md) for yourselves a [ʿōlâ](../../strongs/h/h5930.md); and my ['ebed](../../strongs/h/h5650.md) ['Îyôḇ](../../strongs/h/h347.md) shall [palal](../../strongs/h/h6419.md) for you: for [paniym](../../strongs/h/h6440.md) will I [nasa'](../../strongs/h/h5375.md): lest I ['asah](../../strongs/h/h6213.md) with you after your [nᵊḇālâ](../../strongs/h/h5039.md), in that ye have not [dabar](../../strongs/h/h1696.md) of me the thing which is [kuwn](../../strongs/h/h3559.md), like my ['ebed](../../strongs/h/h5650.md) ['Îyôḇ](../../strongs/h/h347.md).

<a name="job_42_9"></a>Job 42:9

So ['Ĕlîp̄az](../../strongs/h/h464.md) the [têmānî](../../strongs/h/h8489.md) and [Bildaḏ](../../strongs/h/h1085.md) the [šûḥî](../../strongs/h/h7747.md) and [Ṣôp̄Ar](../../strongs/h/h6691.md) the [naʿămāṯî](../../strongs/h/h5284.md) [yālaḵ](../../strongs/h/h3212.md), and ['asah](../../strongs/h/h6213.md) according as [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) them: [Yĕhovah](../../strongs/h/h3068.md) also [nasa'](../../strongs/h/h5375.md) [paniym](../../strongs/h/h6440.md) ['Îyôḇ](../../strongs/h/h347.md).

<a name="job_42_10"></a>Job 42:10

And [Yĕhovah](../../strongs/h/h3068.md) [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) [shebuwth](../../strongs/h/h7622.md) of ['Îyôḇ](../../strongs/h/h347.md), when he [palal](../../strongs/h/h6419.md) for his [rea'](../../strongs/h/h7453.md): also [Yĕhovah](../../strongs/h/h3068.md) gave ['Îyôḇ](../../strongs/h/h347.md) twice as [mišnê](../../strongs/h/h4932.md) as he had before.

<a name="job_42_11"></a>Job 42:11

Then [bow'](../../strongs/h/h935.md) there unto him all his ['ach](../../strongs/h/h251.md), and all his ['āḥôṯ](../../strongs/h/h269.md), and all they that had been of his [yada'](../../strongs/h/h3045.md) [paniym](../../strongs/h/h6440.md), and did ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) with him in his [bayith](../../strongs/h/h1004.md): and they [nuwd](../../strongs/h/h5110.md) him, and [nacham](../../strongs/h/h5162.md) him over all the [ra'](../../strongs/h/h7451.md) that [Yĕhovah](../../strongs/h/h3068.md) had [bow'](../../strongs/h/h935.md) upon him: every ['iysh](../../strongs/h/h376.md) also [nathan](../../strongs/h/h5414.md) him a piece of [qᵊśîṭâ](../../strongs/h/h7192.md), and every ['iysh](../../strongs/h/h376.md) a [nezem](../../strongs/h/h5141.md) of [zāhāḇ](../../strongs/h/h2091.md).

<a name="job_42_12"></a>Job 42:12

So [Yĕhovah](../../strongs/h/h3068.md) [barak](../../strongs/h/h1288.md) the ['aḥărîṯ](../../strongs/h/h319.md) of ['Îyôḇ](../../strongs/h/h347.md) more than his [re'shiyth](../../strongs/h/h7225.md): for he had fourteen  thousand [tso'n](../../strongs/h/h6629.md), and six thousand [gāmāl](../../strongs/h/h1581.md), and a thousand [ṣemeḏ](../../strongs/h/h6776.md) of [bāqār](../../strongs/h/h1241.md), and a thousand she ['āṯôn](../../strongs/h/h860.md).

<a name="job_42_13"></a>Job 42:13

He had also seven [ben](../../strongs/h/h1121.md) and three [bath](../../strongs/h/h1323.md).

<a name="job_42_14"></a>Job 42:14

And he [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of the first, [Yᵊmîmâ](../../strongs/h/h3224.md); and the [shem](../../strongs/h/h8034.md) of the second, [QᵊṣîʿÂ](../../strongs/h/h7103.md); and the [shem](../../strongs/h/h8034.md) of the third, [Qeren Hapûḵ](../../strongs/h/h7163.md).

<a name="job_42_15"></a>Job 42:15

And in all the ['erets](../../strongs/h/h776.md) were no ['ishshah](../../strongs/h/h802.md) [māṣā'](../../strongs/h/h4672.md) so [yāp̄ê](../../strongs/h/h3303.md) as the [bath](../../strongs/h/h1323.md) of ['Îyôḇ](../../strongs/h/h347.md): and their ['ab](../../strongs/h/h1.md) [nathan](../../strongs/h/h5414.md) them [nachalah](../../strongs/h/h5159.md) among their ['ach](../../strongs/h/h251.md).

<a name="job_42_16"></a>Job 42:16

['aḥar](../../strongs/h/h310.md) this [ḥāyâ](../../strongs/h/h2421.md) ['Îyôḇ](../../strongs/h/h347.md) an hundred and forty [šānâ](../../strongs/h/h8141.md), and [ra'ah](../../strongs/h/h7200.md) his [ben](../../strongs/h/h1121.md), and his [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md), even four [dôr](../../strongs/h/h1755.md).

<a name="job_42_17"></a>Job 42:17

So ['Îyôḇ](../../strongs/h/h347.md) [muwth](../../strongs/h/h4191.md), being [zāqēn](../../strongs/h/h2205.md) and [śāḇēaʿ](../../strongs/h/h7649.md) of [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 41](job_41.md)