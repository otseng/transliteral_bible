# [Job 5](https://www.blueletterbible.org/kjv/job/5)

<a name="job_5_1"></a>Job 5:1

[qara'](../../strongs/h/h7121.md) now, if there be any that will ['anah](../../strongs/h/h6030.md) thee; and to which of the [qadowsh](../../strongs/h/h6918.md) wilt thou [panah](../../strongs/h/h6437.md)?

<a name="job_5_2"></a>Job 5:2

For [ka'ac](../../strongs/h/h3708.md) [harag](../../strongs/h/h2026.md) the ['ĕvîl](../../strongs/h/h191.md), and [qin'â](../../strongs/h/h7068.md) [muwth](../../strongs/h/h4191.md) the [pāṯâ](../../strongs/h/h6601.md).

<a name="job_5_3"></a>Job 5:3

I have [ra'ah](../../strongs/h/h7200.md) the ['ĕvîl](../../strongs/h/h191.md) taking [šērēš](../../strongs/h/h8327.md): but [piṯ'ōm](../../strongs/h/h6597.md) I [nāqaḇ](../../strongs/h/h5344.md) his [nāvê](../../strongs/h/h5116.md).

<a name="job_5_4"></a>Job 5:4

His [ben](../../strongs/h/h1121.md) are [rachaq](../../strongs/h/h7368.md) from [yesha'](../../strongs/h/h3468.md), and they are [dāḵā'](../../strongs/h/h1792.md) in the [sha'ar](../../strongs/h/h8179.md), neither is there any to [natsal](../../strongs/h/h5337.md) them.

<a name="job_5_5"></a>Job 5:5

Whose [qāṣîr](../../strongs/h/h7105.md) the [rāʿēḇ](../../strongs/h/h7457.md) eateth ['akal](../../strongs/h/h398.md), and [laqach](../../strongs/h/h3947.md) it even out of the [ṣēn](../../strongs/h/h6791.md), and the [ṣāmmîm](../../strongs/h/h6782.md) [šā'ap̄](../../strongs/h/h7602.md) their [ḥayil](../../strongs/h/h2428.md).

<a name="job_5_6"></a>Job 5:6

Although ['aven](../../strongs/h/h205.md) [yāṣā'](../../strongs/h/h3318.md) not of the ['aphar](../../strongs/h/h6083.md), neither doth ['amal](../../strongs/h/h5999.md) spring [ṣāmaḥ](../../strongs/h/h6779.md) of the ['ăḏāmâ](../../strongs/h/h127.md);

<a name="job_5_7"></a>Job 5:7

Yet ['āḏām](../../strongs/h/h120.md) is [yalad](../../strongs/h/h3205.md) unto ['amal](../../strongs/h/h5999.md), as the [ben](../../strongs/h/h1121.md) [rešep̄](../../strongs/h/h7565.md) ['uwph](../../strongs/h/h5774.md) [gāḇah](../../strongs/h/h1361.md).

<a name="job_5_8"></a>Job 5:8

I would [darash](../../strongs/h/h1875.md) unto ['el](../../strongs/h/h410.md), and unto ['Elohiym](../../strongs/h/h430.md) would I [śûm](../../strongs/h/h7760.md) my [diḇrâ](../../strongs/h/h1700.md):

<a name="job_5_9"></a>Job 5:9

Which ['asah](../../strongs/h/h6213.md) [gadowl](../../strongs/h/h1419.md) and['în](../../strongs/h/h369.md) [ḥēqer](../../strongs/h/h2714.md); [pala'](../../strongs/h/h6381.md) without [mispār](../../strongs/h/h4557.md):

<a name="job_5_10"></a>Job 5:10

Who [nathan](../../strongs/h/h5414.md) [māṭār](../../strongs/h/h4306.md) [paniym](../../strongs/h/h6440.md) the ['erets](../../strongs/h/h776.md), and [shalach](../../strongs/h/h7971.md) [mayim](../../strongs/h/h4325.md) [paniym](../../strongs/h/h6440.md) the [ḥûṣ](../../strongs/h/h2351.md):

<a name="job_5_11"></a>Job 5:11

To [śûm](../../strongs/h/h7760.md) on [marowm](../../strongs/h/h4791.md) those that be [šāp̄āl](../../strongs/h/h8217.md); that those which [qāḏar](../../strongs/h/h6937.md) may be [śāḡaḇ](../../strongs/h/h7682.md) to [yesha'](../../strongs/h/h3468.md).

<a name="job_5_12"></a>Job 5:12

He [pārar](../../strongs/h/h6565.md) the [maḥăšāḇâ](../../strongs/h/h4284.md) of the ['aruwm](../../strongs/h/h6175.md), so that their [yad](../../strongs/h/h3027.md) cannot ['asah](../../strongs/h/h6213.md) their [tûšîyâ](../../strongs/h/h8454.md).

<a name="job_5_13"></a>Job 5:13

He [lāḵaḏ](../../strongs/h/h3920.md) the [ḥāḵām](../../strongs/h/h2450.md) in their own [ʿōrem](../../strongs/h/h6193.md): and the ['etsah](../../strongs/h/h6098.md) of the [pāṯal](../../strongs/h/h6617.md) is [māhar](../../strongs/h/h4116.md).

<a name="job_5_14"></a>Job 5:14

They [pāḡaš](../../strongs/h/h6298.md) with [choshek](../../strongs/h/h2822.md) in the [yômām](../../strongs/h/h3119.md), and [māšaš](../../strongs/h/h4959.md) in the [ṣōhar](../../strongs/h/h6672.md) as in the [layil](../../strongs/h/h3915.md).

<a name="job_5_15"></a>Job 5:15

But he [yasha'](../../strongs/h/h3467.md) the ['ebyown](../../strongs/h/h34.md) from the [chereb](../../strongs/h/h2719.md), from their [peh](../../strongs/h/h6310.md), and from the [yad](../../strongs/h/h3027.md) of the [ḥāzāq](../../strongs/h/h2389.md).

<a name="job_5_16"></a>Job 5:16

So the [dal](../../strongs/h/h1800.md) hath [tiqvâ](../../strongs/h/h8615.md), and ['evel](../../strongs/h/h5766.md) [qāp̄aṣ](../../strongs/h/h7092.md) her [peh](../../strongs/h/h6310.md).

<a name="job_5_17"></a>Job 5:17

Behold, ['esher](../../strongs/h/h835.md) is the ['enowsh](../../strongs/h/h582.md) whom ['ĕlvôha](../../strongs/h/h433.md) [yakach](../../strongs/h/h3198.md): therefore [mā'as](../../strongs/h/h3988.md) not thou the [mûsār](../../strongs/h/h4148.md) of the [Šaday](../../strongs/h/h7706.md):

<a name="job_5_18"></a>Job 5:18

For he [kā'aḇ](../../strongs/h/h3510.md), and [ḥāḇaš](../../strongs/h/h2280.md): he [māḥaṣ](../../strongs/h/h4272.md), and his [yad](../../strongs/h/h3027.md) make [rapha'](../../strongs/h/h7495.md).

<a name="job_5_19"></a>Job 5:19

He shall [natsal](../../strongs/h/h5337.md) thee in six [tsarah](../../strongs/h/h6869.md): yea, in seven there shall no [ra'](../../strongs/h/h7451.md) [naga'](../../strongs/h/h5060.md) thee.

<a name="job_5_20"></a>Job 5:20

In [rāʿāḇ](../../strongs/h/h7458.md) he shall [pāḏâ](../../strongs/h/h6299.md) thee from [maveth](../../strongs/h/h4194.md): and in [milḥāmâ](../../strongs/h/h4421.md) from the [yad](../../strongs/h/h3027.md) of the [chereb](../../strongs/h/h2719.md).

<a name="job_5_21"></a>Job 5:21

Thou shalt be [chaba'](../../strongs/h/h2244.md) from the [šôṭ](../../strongs/h/h7752.md) of the [lashown](../../strongs/h/h3956.md): neither shalt thou be [yare'](../../strongs/h/h3372.md) of [shod](../../strongs/h/h7701.md) when it [bow'](../../strongs/h/h935.md).

<a name="job_5_22"></a>Job 5:22

At [shod](../../strongs/h/h7701.md) and [kāp̄ān](../../strongs/h/h3720.md) thou shalt [śāḥaq](../../strongs/h/h7832.md): neither shalt thou be [yare'](../../strongs/h/h3372.md) of the [chay](../../strongs/h/h2416.md) of the ['erets](../../strongs/h/h776.md).

<a name="job_5_23"></a>Job 5:23

For thou shalt be in [bĕriyth](../../strongs/h/h1285.md) with the ['eben](../../strongs/h/h68.md) of the [sadeh](../../strongs/h/h7704.md): and the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) shall be at [shalam](../../strongs/h/h7999.md) with thee.

<a name="job_5_24"></a>Job 5:24

And thou shalt [yada'](../../strongs/h/h3045.md) that thy ['ohel](../../strongs/h/h168.md) shall be in [shalowm](../../strongs/h/h7965.md); and thou shalt [paqad](../../strongs/h/h6485.md) thy [nāvê](../../strongs/h/h5116.md), and shalt not [chata'](../../strongs/h/h2398.md).

<a name="job_5_25"></a>Job 5:25

Thou shalt [yada'](../../strongs/h/h3045.md) also that thy [zera'](../../strongs/h/h2233.md) shall be [rab](../../strongs/h/h7227.md), and thine [ṣe'ĕṣā'îm](../../strongs/h/h6631.md) as the ['eseb](../../strongs/h/h6212.md) of the ['erets](../../strongs/h/h776.md).

<a name="job_5_26"></a>Job 5:26

Thou shalt [bow'](../../strongs/h/h935.md) to thy [qeber](../../strongs/h/h6913.md) in a [kelaḥ](../../strongs/h/h3624.md), like as a [gāḏîš](../../strongs/h/h1430.md) [ʿālâ](../../strongs/h/h5927.md) in his [ʿēṯ](../../strongs/h/h6256.md).

<a name="job_5_27"></a>Job 5:27

Lo this, we have [chaqar](../../strongs/h/h2713.md) it, so it is; [shama'](../../strongs/h/h8085.md) it, and [yada'](../../strongs/h/h3045.md) thou it for thy good.

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 4](job_4.md) - [Job 6](job_6.md)