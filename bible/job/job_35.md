# [Job 35](https://www.blueletterbible.org/kjv/job/35)

<a name="job_35_1"></a>Job 35:1

['Ĕlîhû](../../strongs/h/h453.md) ['anah](../../strongs/h/h6030.md) moreover, and ['āmar](../../strongs/h/h559.md),

<a name="job_35_2"></a>Job 35:2

[chashab](../../strongs/h/h2803.md) thou this to be [mishpat](../../strongs/h/h4941.md), that thou ['āmar](../../strongs/h/h559.md), My [tsedeq](../../strongs/h/h6664.md) is more than ['el](../../strongs/h/h410.md)?

<a name="job_35_3"></a>Job 35:3

For thou ['āmar](../../strongs/h/h559.md), What [sāḵan](../../strongs/h/h5532.md) will it be unto thee? and, What [yāʿal](../../strongs/h/h3276.md) shall I have, from my [chatta'ath](../../strongs/h/h2403.md)?

<a name="job_35_4"></a>Job 35:4

I will [shuwb](../../strongs/h/h7725.md) [millâ](../../strongs/h/h4405.md) thee, and thy [rea'](../../strongs/h/h7453.md) with thee.

<a name="job_35_5"></a>Job 35:5

[nabat](../../strongs/h/h5027.md) unto the [shamayim](../../strongs/h/h8064.md), and [ra'ah](../../strongs/h/h7200.md); and [šûr](../../strongs/h/h7789.md) the [shachaq](../../strongs/h/h7834.md) are [gāḇah](../../strongs/h/h1361.md) than thou.

<a name="job_35_6"></a>Job 35:6

If thou [chata'](../../strongs/h/h2398.md), what [pa'al](../../strongs/h/h6466.md) thou against him? or if thy [pesha'](../../strongs/h/h6588.md) be [rabab](../../strongs/h/h7231.md), what ['asah](../../strongs/h/h6213.md) thou unto him?

<a name="job_35_7"></a>Job 35:7

If thou be [ṣāḏaq](../../strongs/h/h6663.md), what [nathan](../../strongs/h/h5414.md) thou him? or what [laqach](../../strongs/h/h3947.md) he of thine [yad](../../strongs/h/h3027.md)?

<a name="job_35_8"></a>Job 35:8

Thy [resha'](../../strongs/h/h7562.md) an ['iysh](../../strongs/h/h376.md) as thou art; and thy [tsedaqah](../../strongs/h/h6666.md) the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md).

<a name="job_35_9"></a>Job 35:9

By reason of the [rōḇ](../../strongs/h/h7230.md) of [ʿăšûqîm](../../strongs/h/h6217.md) they make to [zāʿaq](../../strongs/h/h2199.md): they [šāvaʿ](../../strongs/h/h7768.md) by reason of the [zerowa'](../../strongs/h/h2220.md) of the [rab](../../strongs/h/h7227.md).

<a name="job_35_10"></a>Job 35:10

But none ['āmar](../../strongs/h/h559.md), Where is ['ĕlvôha](../../strongs/h/h433.md) my ['asah](../../strongs/h/h6213.md), who [nathan](../../strongs/h/h5414.md) [zᵊmîr](../../strongs/h/h2158.md) in the [layil](../../strongs/h/h3915.md);

<a name="job_35_11"></a>Job 35:11

Who ['ālap̄](../../strongs/h/h502.md) us more than the [bĕhemah](../../strongs/h/h929.md) of the ['erets](../../strongs/h/h776.md), and maketh us [ḥāḵam](../../strongs/h/h2449.md) than the [ʿôp̄](../../strongs/h/h5775.md) of [shamayim](../../strongs/h/h8064.md)?

<a name="job_35_12"></a>Job 35:12

There they [ṣāʿaq](../../strongs/h/h6817.md), but none giveth ['anah](../../strongs/h/h6030.md), [paniym](../../strongs/h/h6440.md) of the [gā'ôn](../../strongs/h/h1347.md) of [ra'](../../strongs/h/h7451.md).

<a name="job_35_13"></a>Job 35:13

Surely ['el](../../strongs/h/h410.md) will not [shama'](../../strongs/h/h8085.md) [shav'](../../strongs/h/h7723.md), neither will the [Šaday](../../strongs/h/h7706.md) [šûr](../../strongs/h/h7789.md) it.

<a name="job_35_14"></a>Job 35:14

Although thou ['āmar](../../strongs/h/h559.md) thou shalt not [šûr](../../strongs/h/h7789.md) him, yet [diyn](../../strongs/h/h1779.md) is [paniym](../../strongs/h/h6440.md) him; therefore [chuwl](../../strongs/h/h2342.md) thou in him.

<a name="job_35_15"></a>Job 35:15

But now, because it is not so, he hath [paqad](../../strongs/h/h6485.md) in his ['aph](../../strongs/h/h639.md); yet he [yada'](../../strongs/h/h3045.md) it not in [me'od](../../strongs/h/h3966.md) [paš](../../strongs/h/h6580.md):

<a name="job_35_16"></a>Job 35:16

Therefore doth ['Îyôḇ](../../strongs/h/h347.md) [pāṣâ](../../strongs/h/h6475.md) his [peh](../../strongs/h/h6310.md) in [heḇel](../../strongs/h/h1892.md); he [kāḇar](../../strongs/h/h3527.md) [millâ](../../strongs/h/h4405.md) without [da'ath](../../strongs/h/h1847.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 34](job_34.md) - [Job 36](job_36.md)