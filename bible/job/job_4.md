# [Job 4](https://www.blueletterbible.org/kjv/job/4)

<a name="job_4_1"></a>Job 4:1

Then ['Ĕlîp̄az](../../strongs/h/h464.md) the [têmānî](../../strongs/h/h8489.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md),

<a name="job_4_2"></a>Job 4:2

If we [nāsâ](../../strongs/h/h5254.md) to [dabar](../../strongs/h/h1697.md) with thee, wilt thou be [lā'â](../../strongs/h/h3811.md)? but who [yakol](../../strongs/h/h3201.md) [ʿāṣar](../../strongs/h/h6113.md) himself from [millâ](../../strongs/h/h4405.md)?

<a name="job_4_3"></a>Job 4:3

Behold, thou hast [yacar](../../strongs/h/h3256.md) [rab](../../strongs/h/h7227.md), and thou hast [ḥāzaq](../../strongs/h/h2388.md) the [rāp̄ê](../../strongs/h/h7504.md) [yad](../../strongs/h/h3027.md).

<a name="job_4_4"></a>Job 4:4

Thy [millâ](../../strongs/h/h4405.md) have [quwm](../../strongs/h/h6965.md) him that was [kashal](../../strongs/h/h3782.md), and thou hast ['amats](../../strongs/h/h553.md) the [kara'](../../strongs/h/h3766.md) [bereḵ](../../strongs/h/h1290.md).

<a name="job_4_5"></a>Job 4:5

But now it is [bow'](../../strongs/h/h935.md) upon thee, and thou [lā'â](../../strongs/h/h3811.md); it [naga'](../../strongs/h/h5060.md) thee, and thou art [bahal](../../strongs/h/h926.md).

<a name="job_4_6"></a>Job 4:6

Is not this thy [yir'ah](../../strongs/h/h3374.md), thy [kislâ](../../strongs/h/h3690.md), thy [tiqvâ](../../strongs/h/h8615.md), and the [tom](../../strongs/h/h8537.md) of thy [derek](../../strongs/h/h1870.md)?

<a name="job_4_7"></a>Job 4:7

[zakar](../../strongs/h/h2142.md), I pray thee, who ever ['abad](../../strongs/h/h6.md), being [naqiy](../../strongs/h/h5355.md)? or ['êp̄ô](../../strongs/h/h375.md) were the [yashar](../../strongs/h/h3477.md) cut [kāḥaḏ](../../strongs/h/h3582.md)?

<a name="job_4_8"></a>Job 4:8

Even as I have [ra'ah](../../strongs/h/h7200.md), they that [ḥāraš](../../strongs/h/h2790.md) ['aven](../../strongs/h/h205.md), and [zāraʿ](../../strongs/h/h2232.md) ['amal](../../strongs/h/h5999.md), [qāṣar](../../strongs/h/h7114.md) the same.

<a name="job_4_9"></a>Job 4:9

By the [neshamah](../../strongs/h/h5397.md) of ['ĕlvôha](../../strongs/h/h433.md) they ['abad](../../strongs/h/h6.md), and by the [ruwach](../../strongs/h/h7307.md) of his ['aph](../../strongs/h/h639.md) are they [kalah](../../strongs/h/h3615.md).

<a name="job_4_10"></a>Job 4:10

The [šᵊ'āḡâ](../../strongs/h/h7581.md) of the ['ariy](../../strongs/h/h738.md), and the [qowl](../../strongs/h/h6963.md) of the [šāḥal](../../strongs/h/h7826.md), and the [šēn](../../strongs/h/h8127.md) of the [kephiyr](../../strongs/h/h3715.md), are [nāṯaʿ](../../strongs/h/h5421.md).

<a name="job_4_11"></a>Job 4:11

The [layiš](../../strongs/h/h3918.md) ['abad](../../strongs/h/h6.md) for lack of [ṭerep̄](../../strongs/h/h2964.md), and the [lāḇî'](../../strongs/h/h3833.md) [ben](../../strongs/h/h1121.md) are [pāraḏ](../../strongs/h/h6504.md).

<a name="job_4_12"></a>Job 4:12

Now a [dabar](../../strongs/h/h1697.md) was [ganab](../../strongs/h/h1589.md) to me, and mine ['ozen](../../strongs/h/h241.md) [laqach](../../strongs/h/h3947.md) a [šemeṣ](../../strongs/h/h8102.md) thereof.

<a name="job_4_13"></a>Job 4:13

In [sᵊʿipâ](../../strongs/h/h5587.md) from the [ḥizzāyôn](../../strongs/h/h2384.md) of the [layil](../../strongs/h/h3915.md), when deep [tardēmâ](../../strongs/h/h8639.md) [naphal](../../strongs/h/h5307.md) on ['enowsh](../../strongs/h/h582.md),

<a name="job_4_14"></a>Job 4:14

[paḥaḏ](../../strongs/h/h6343.md) [qārā'](../../strongs/h/h7122.md) upon me, and [ra'ad](../../strongs/h/h7461.md), which made [rōḇ](../../strongs/h/h7230.md) my ['etsem](../../strongs/h/h6106.md) to [pachad](../../strongs/h/h6342.md).

<a name="job_4_15"></a>Job 4:15

Then a [ruwach](../../strongs/h/h7307.md) [ḥālap̄](../../strongs/h/h2498.md) before my [paniym](../../strongs/h/h6440.md); the [śaʿărâ](../../strongs/h/h8185.md) of my [basar](../../strongs/h/h1320.md) stood [sāmar](../../strongs/h/h5568.md):

<a name="job_4_16"></a>Job 4:16

It ['amad](../../strongs/h/h5975.md), but I could not [nāḵar](../../strongs/h/h5234.md) the [mar'ê](../../strongs/h/h4758.md) thereof: a [tĕmuwnah](../../strongs/h/h8544.md) was before mine ['ayin](../../strongs/h/h5869.md), there was [dᵊmāmâ](../../strongs/h/h1827.md), and I [shama'](../../strongs/h/h8085.md) a [qowl](../../strongs/h/h6963.md), saying,

<a name="job_4_17"></a>Job 4:17

Shall ['enowsh](../../strongs/h/h582.md) be more [ṣāḏaq](../../strongs/h/h6663.md) than ['ĕlvôha](../../strongs/h/h433.md)? shall a [geḇer](../../strongs/h/h1397.md) be more [ṭāhēr](../../strongs/h/h2891.md) than his ['asah](../../strongs/h/h6213.md)?

<a name="job_4_18"></a>Job 4:18

Behold, he put no ['aman](../../strongs/h/h539.md) in his ['ebed](../../strongs/h/h5650.md); and his [mal'ak](../../strongs/h/h4397.md) he [śûm](../../strongs/h/h7760.md) with [tāhŏlâ](../../strongs/h/h8417.md):

<a name="job_4_19"></a>Job 4:19

How much less in them that [shakan](../../strongs/h/h7931.md) in [bayith](../../strongs/h/h1004.md) of [ḥōmer](../../strongs/h/h2563.md), whose [yᵊsôḏ](../../strongs/h/h3247.md) is in the ['aphar](../../strongs/h/h6083.md), which are [dāḵā'](../../strongs/h/h1792.md) [paniym](../../strongs/h/h6440.md) the [ʿāš](../../strongs/h/h6211.md)?

<a name="job_4_20"></a>Job 4:20

They are [kāṯaṯ](../../strongs/h/h3807.md) from [boqer](../../strongs/h/h1242.md) to ['ereb](../../strongs/h/h6153.md): they ['abad](../../strongs/h/h6.md) for [netsach](../../strongs/h/h5331.md) without any [śûm](../../strongs/h/h7760.md) it.

<a name="job_4_21"></a>Job 4:21

Doth not their [yeṯer](../../strongs/h/h3499.md) which is in them [nāsaʿ](../../strongs/h/h5265.md)? they [muwth](../../strongs/h/h4191.md), even without [ḥāḵmâ](../../strongs/h/h2451.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 3](job_3.md) - [Job 5](job_5.md)