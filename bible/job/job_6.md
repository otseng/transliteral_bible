# [Job 6](https://www.blueletterbible.org/kjv/job/6)

<a name="job_6_1"></a>Job 6:1

But ['Îyôḇ](../../strongs/h/h347.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md),

<a name="job_6_2"></a>Job 6:2

Oh that my [ka'ac](../../strongs/h/h3708.md) were [šāqal](../../strongs/h/h8254.md) [šāqal](../../strongs/h/h8254.md), and my [havvah](../../strongs/h/h1942.md) [hayyâ](../../strongs/h/h1962.md) [nasa'](../../strongs/h/h5375.md) in the [mō'znayim](../../strongs/h/h3976.md) [yaḥaḏ](../../strongs/h/h3162.md)!

<a name="job_6_3"></a>Job 6:3

For now it would be [kabad](../../strongs/h/h3513.md) than the [ḥôl](../../strongs/h/h2344.md) of the [yam](../../strongs/h/h3220.md): therefore my [dabar](../../strongs/h/h1697.md) are [lûaʿ](../../strongs/h/h3886.md).

<a name="job_6_4"></a>Job 6:4

For the [chets](../../strongs/h/h2671.md) of the [Šaday](../../strongs/h/h7706.md) are within me, the [chemah](../../strongs/h/h2534.md) whereof [šāṯâ](../../strongs/h/h8354.md) my [ruwach](../../strongs/h/h7307.md): the [biʿûṯîm](../../strongs/h/h1161.md) of ['ĕlvôha](../../strongs/h/h433.md) do set themselves in ['arak](../../strongs/h/h6186.md) against me.

<a name="job_6_5"></a>Job 6:5

Doth the [pere'](../../strongs/h/h6501.md) [nāhaq](../../strongs/h/h5101.md) when he hath [deše'](../../strongs/h/h1877.md)? or [gāʿâ](../../strongs/h/h1600.md) the [showr](../../strongs/h/h7794.md) over his [bᵊlîl](../../strongs/h/h1098.md)?

<a name="job_6_6"></a>Job 6:6

Can that which is [tāp̄ēl](../../strongs/h/h8602.md) be ['akal](../../strongs/h/h398.md) without [melaḥ](../../strongs/h/h4417.md)? or is there any [ṭaʿam](../../strongs/h/h2940.md) in the [rîr](../../strongs/h/h7388.md) of an [ḥallāmûṯ](../../strongs/h/h2495.md)?

<a name="job_6_7"></a>Job 6:7

The things that my [nephesh](../../strongs/h/h5315.md) [mā'ēn](../../strongs/h/h3985.md) to [naga'](../../strongs/h/h5060.md) are as my [dᵊvay](../../strongs/h/h1741.md) [lechem](../../strongs/h/h3899.md).

<a name="job_6_8"></a>Job 6:8

Oh that I might [bow'](../../strongs/h/h935.md) my [šᵊ'ēlâ](../../strongs/h/h7596.md); and that ['ĕlvôha](../../strongs/h/h433.md) would [nathan](../../strongs/h/h5414.md) me the thing that I [tiqvâ](../../strongs/h/h8615.md)!

<a name="job_6_9"></a>Job 6:9

Even that it would [yā'al](../../strongs/h/h2974.md) ['ĕlvôha](../../strongs/h/h433.md) to [dāḵā'](../../strongs/h/h1792.md) me; that he would let [nāṯar](../../strongs/h/h5425.md) his [yad](../../strongs/h/h3027.md), and [batsa'](../../strongs/h/h1214.md) me!

<a name="job_6_10"></a>Job 6:10

Then should I yet have [neḥāmâ](../../strongs/h/h5165.md); yea, I would [sālaḏ](../../strongs/h/h5539.md) myself in [ḥîl](../../strongs/h/h2427.md): let him not [ḥāmal](../../strongs/h/h2550.md); for I have not [kāḥaḏ](../../strongs/h/h3582.md) the ['emer](../../strongs/h/h561.md) of the [qadowsh](../../strongs/h/h6918.md).

<a name="job_6_11"></a>Job 6:11

What is my [koach](../../strongs/h/h3581.md), that I should [yāḥal](../../strongs/h/h3176.md)? and what is mine [qēṣ](../../strongs/h/h7093.md), that I should ['arak](../../strongs/h/h748.md) my [nephesh](../../strongs/h/h5315.md)?

<a name="job_6_12"></a>Job 6:12

Is my [koach](../../strongs/h/h3581.md) the [koach](../../strongs/h/h3581.md) of ['eben](../../strongs/h/h68.md)? or is my [basar](../../strongs/h/h1320.md) of [nāḥûš](../../strongs/h/h5153.md)?

<a name="job_6_13"></a>Job 6:13

Is not my [ʿezrâ](../../strongs/h/h5833.md) in me? and is [tûšîyâ](../../strongs/h/h8454.md) [nāḏaḥ](../../strongs/h/h5080.md) from me?

<a name="job_6_14"></a>Job 6:14

To him that is [mās](../../strongs/h/h4523.md) [checed](../../strongs/h/h2617.md) should be shewed from his [rea'](../../strongs/h/h7453.md); but he ['azab](../../strongs/h/h5800.md) the [yir'ah](../../strongs/h/h3374.md) of the [Šaday](../../strongs/h/h7706.md).

<a name="job_6_15"></a>Job 6:15

My ['ach](../../strongs/h/h251.md) have [bāḡaḏ](../../strongs/h/h898.md) as a [nachal](../../strongs/h/h5158.md), and as the ['āp̄îq](../../strongs/h/h650.md) of [nachal](../../strongs/h/h5158.md) they ['abar](../../strongs/h/h5674.md);

<a name="job_6_16"></a>Job 6:16

Which are [qāḏar](../../strongs/h/h6937.md) by reason of the [qeraḥ](../../strongs/h/h7140.md), and wherein the [šeleḡ](../../strongs/h/h7950.md) is ['alam](../../strongs/h/h5956.md):

<a name="job_6_17"></a>Job 6:17

What [ʿēṯ](../../strongs/h/h6256.md) they [zāraḇ](../../strongs/h/h2215.md), they [ṣāmaṯ](../../strongs/h/h6789.md): when it is [ḥōm](../../strongs/h/h2527.md), they are consumed [dāʿaḵ](../../strongs/h/h1846.md) of their [maqowm](../../strongs/h/h4725.md).

<a name="job_6_18"></a>Job 6:18

The ['orach](../../strongs/h/h734.md) of their [derek](../../strongs/h/h1870.md) are [lāp̄aṯ](../../strongs/h/h3943.md); they [ʿālâ](../../strongs/h/h5927.md) to [tohuw](../../strongs/h/h8414.md), and ['abad](../../strongs/h/h6.md).

<a name="job_6_19"></a>Job 6:19

The ['orach](../../strongs/h/h734.md) of [Têmā'](../../strongs/h/h8485.md) [nabat](../../strongs/h/h5027.md), the [hălîḵâ](../../strongs/h/h1979.md) of [Šᵊḇā'](../../strongs/h/h7614.md) [qāvâ](../../strongs/h/h6960.md) for them.

<a name="job_6_20"></a>Job 6:20

They were [buwsh](../../strongs/h/h954.md) because they had [batach](../../strongs/h/h982.md); they [bow'](../../strongs/h/h935.md) thither, and were [ḥāp̄ēr](../../strongs/h/h2659.md).

<a name="job_6_21"></a>Job 6:21

For now ye are nothing; ye [ra'ah](../../strongs/h/h7200.md) my [ḥăṯaṯ](../../strongs/h/h2866.md), and are [yare'](../../strongs/h/h3372.md).

<a name="job_6_22"></a>Job 6:22

Did I ['āmar](../../strongs/h/h559.md), [yāhaḇ](../../strongs/h/h3051.md) unto me? or, [šāḥaḏ](../../strongs/h/h7809.md) me of your [koach](../../strongs/h/h3581.md)?

<a name="job_6_23"></a>Job 6:23

Or, [mālaṭ](../../strongs/h/h4422.md) me from the [tsar](../../strongs/h/h6862.md) [yad](../../strongs/h/h3027.md)? or, [pāḏâ](../../strongs/h/h6299.md) me from the [yad](../../strongs/h/h3027.md) of the [ʿārîṣ](../../strongs/h/h6184.md)?

<a name="job_6_24"></a>Job 6:24

[yārâ](../../strongs/h/h3384.md) me, and I will [ḥāraš](../../strongs/h/h2790.md): and cause me to [bîn](../../strongs/h/h995.md) wherein I have [šāḡâ](../../strongs/h/h7686.md).

<a name="job_6_25"></a>Job 6:25

How [māraṣ](../../strongs/h/h4834.md) are [yōšer](../../strongs/h/h3476.md) ['emer](../../strongs/h/h561.md)! but what doth your [yakach](../../strongs/h/h3198.md) [yakach](../../strongs/h/h3198.md)?

<a name="job_6_26"></a>Job 6:26

Do ye [chashab](../../strongs/h/h2803.md) to [yakach](../../strongs/h/h3198.md) [millâ](../../strongs/h/h4405.md), and the ['emer](../../strongs/h/h561.md) of one that is [yā'aš](../../strongs/h/h2976.md), which are as [ruwach](../../strongs/h/h7307.md)?

<a name="job_6_27"></a>Job 6:27

Yea, ye [naphal](../../strongs/h/h5307.md) the [yathowm](../../strongs/h/h3490.md), and ye [karah](../../strongs/h/h3738.md) a pit for your [rea'](../../strongs/h/h7453.md).

<a name="job_6_28"></a>Job 6:28

Now therefore be [yā'al](../../strongs/h/h2974.md), [panah](../../strongs/h/h6437.md) upon me; for it is [paniym](../../strongs/h/h6440.md) unto you if I [kāzaḇ](../../strongs/h/h3576.md).

<a name="job_6_29"></a>Job 6:29

[shuwb](../../strongs/h/h7725.md), I pray you, let it not be ['evel](../../strongs/h/h5766.md); yea, [shuwb](../../strongs/h/h7725.md), my [tsedeq](../../strongs/h/h6664.md) is in it.

<a name="job_6_30"></a>Job 6:30

Is there ['evel](../../strongs/h/h5766.md) in my [lashown](../../strongs/h/h3956.md)? cannot my [ḥēḵ](../../strongs/h/h2441.md) [bîn](../../strongs/h/h995.md) [havvah](../../strongs/h/h1942.md)?

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 5](job_5.md) - [Job 7](job_7.md)