# [Job 24](https://www.blueletterbible.org/kjv/job/24)

<a name="job_24_1"></a>Job 24:1

Why, seeing [ʿēṯ](../../strongs/h/h6256.md) are not [tsaphan](../../strongs/h/h6845.md) from the [Šaday](../../strongs/h/h7706.md), do they that [yada'](../../strongs/h/h3045.md) him not [chazah](../../strongs/h/h2372.md) his [yowm](../../strongs/h/h3117.md)?

<a name="job_24_2"></a>Job 24:2

Some [nāśaḡ](../../strongs/h/h5381.md) the [gᵊḇûlâ](../../strongs/h/h1367.md); they violently take [gāzal](../../strongs/h/h1497.md) [ʿēḏer](../../strongs/h/h5739.md), and [ra'ah](../../strongs/h/h7462.md) thereof.

<a name="job_24_3"></a>Job 24:3

They drive [nāhaḡ](../../strongs/h/h5090.md) the [chamowr](../../strongs/h/h2543.md) of the [yathowm](../../strongs/h/h3490.md), they [chabal](../../strongs/h/h2254.md) the ['almānâ](../../strongs/h/h490.md) [showr](../../strongs/h/h7794.md) for a [chabal](../../strongs/h/h2254.md).

<a name="job_24_4"></a>Job 24:4

They [natah](../../strongs/h/h5186.md) the ['ebyown](../../strongs/h/h34.md) out of the [derek](../../strongs/h/h1870.md): the ['aniy](../../strongs/h/h6041.md) ['anav](../../strongs/h/h6035.md) of the ['erets](../../strongs/h/h776.md) [chaba'](../../strongs/h/h2244.md) themselves [yaḥaḏ](../../strongs/h/h3162.md).

<a name="job_24_5"></a>Job 24:5

Behold, as [pere'](../../strongs/h/h6501.md) in the [midbar](../../strongs/h/h4057.md), go they [yāṣā'](../../strongs/h/h3318.md) to their [pōʿal](../../strongs/h/h6467.md); rising [šāḥar](../../strongs/h/h7836.md) for a [ṭerep̄](../../strongs/h/h2964.md): the ['arabah](../../strongs/h/h6160.md) yieldeth [lechem](../../strongs/h/h3899.md) for them and for their [naʿar](../../strongs/h/h5288.md).

<a name="job_24_6"></a>Job 24:6

They [qāṣar](../../strongs/h/h7114.md) [qāṣar](../../strongs/h/h7114.md) every one his [bᵊlîl](../../strongs/h/h1098.md) in the [sadeh](../../strongs/h/h7704.md): and they [lāqaš](../../strongs/h/h3953.md) the [kerem](../../strongs/h/h3754.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="job_24_7"></a>Job 24:7

They cause the ['arowm](../../strongs/h/h6174.md) to [lûn](../../strongs/h/h3885.md) without [lᵊḇûš](../../strongs/h/h3830.md), that they have no [kᵊsûṯ](../../strongs/h/h3682.md) in the [qārâ](../../strongs/h/h7135.md).

<a name="job_24_8"></a>Job 24:8

They are [rāṭaḇ](../../strongs/h/h7372.md) with the [zerem](../../strongs/h/h2230.md) of the [har](../../strongs/h/h2022.md), and [ḥāḇaq](../../strongs/h/h2263.md) the [tsuwr](../../strongs/h/h6697.md) for want of a [machaceh](../../strongs/h/h4268.md).

<a name="job_24_9"></a>Job 24:9

They [gāzal](../../strongs/h/h1497.md) the [yathowm](../../strongs/h/h3490.md) from the [šaḏ](../../strongs/h/h7699.md), and take a [chabal](../../strongs/h/h2254.md) of the ['aniy](../../strongs/h/h6041.md).

<a name="job_24_10"></a>Job 24:10

They cause him to [halak](../../strongs/h/h1980.md) ['arowm](../../strongs/h/h6174.md) without [lᵊḇûš](../../strongs/h/h3830.md), and they take [nasa'](../../strongs/h/h5375.md) the [ʿōmer](../../strongs/h/h6016.md) from the [rāʿēḇ](../../strongs/h/h7457.md);

<a name="job_24_11"></a>Job 24:11

Which make [ṣāhar](../../strongs/h/h6671.md) within their [šûrâ](../../strongs/h/h7791.md), and [dāraḵ](../../strongs/h/h1869.md) their [yeqeḇ](../../strongs/h/h3342.md), and suffer [ṣāmē'](../../strongs/h/h6770.md).

<a name="job_24_12"></a>Job 24:12

[math](../../strongs/h/h4962.md) [nā'aq](../../strongs/h/h5008.md) from out of the [ʿîr](../../strongs/h/h5892.md), and the [nephesh](../../strongs/h/h5315.md) of the [ḥālāl](../../strongs/h/h2491.md) crieth [šāvaʿ](../../strongs/h/h7768.md): yet ['ĕlvôha](../../strongs/h/h433.md) [śûm](../../strongs/h/h7760.md) not [tip̄lâ](../../strongs/h/h8604.md) to them.

<a name="job_24_13"></a>Job 24:13

They are of those that [māraḏ](../../strongs/h/h4775.md) against the ['owr](../../strongs/h/h216.md); they [nāḵar](../../strongs/h/h5234.md) not the [derek](../../strongs/h/h1870.md) thereof, nor [yashab](../../strongs/h/h3427.md) in the [nāṯîḇ](../../strongs/h/h5410.md) thereof.

<a name="job_24_14"></a>Job 24:14

The [ratsach](../../strongs/h/h7523.md) [quwm](../../strongs/h/h6965.md) with the ['owr](../../strongs/h/h216.md) [qāṭal](../../strongs/h/h6991.md) the ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md), and in the [layil](../../strongs/h/h3915.md) is as a [gannāḇ](../../strongs/h/h1590.md).

<a name="job_24_15"></a>Job 24:15

The ['ayin](../../strongs/h/h5869.md) also of the [na'aph](../../strongs/h/h5003.md) [shamar](../../strongs/h/h8104.md) for the [nešep̄](../../strongs/h/h5399.md), ['āmar](../../strongs/h/h559.md), No ['ayin](../../strongs/h/h5869.md) shall [šûr](../../strongs/h/h7789.md) me: and [cether](../../strongs/h/h5643.md) [śûm](../../strongs/h/h7760.md) his [paniym](../../strongs/h/h6440.md).

<a name="job_24_16"></a>Job 24:16

In the [choshek](../../strongs/h/h2822.md) they dig [ḥāṯar](../../strongs/h/h2864.md) [bayith](../../strongs/h/h1004.md), which they had [ḥāṯam](../../strongs/h/h2856.md) for themselves in the [yômām](../../strongs/h/h3119.md): they [yada'](../../strongs/h/h3045.md) not the ['owr](../../strongs/h/h216.md).

<a name="job_24_17"></a>Job 24:17

For the [boqer](../../strongs/h/h1242.md) is to them even [yaḥaḏ](../../strongs/h/h3162.md) the [ṣalmāveṯ](../../strongs/h/h6757.md): if one [nāḵar](../../strongs/h/h5234.md) them, they are in the [ballāhâ](../../strongs/h/h1091.md) of the [ṣalmāveṯ](../../strongs/h/h6757.md).

<a name="job_24_18"></a>Job 24:18

He is [qal](../../strongs/h/h7031.md) as the [paniym](../../strongs/h/h6440.md) [mayim](../../strongs/h/h4325.md); their [ḥelqâ](../../strongs/h/h2513.md) is [qālal](../../strongs/h/h7043.md) in the ['erets](../../strongs/h/h776.md): he [panah](../../strongs/h/h6437.md) not the [derek](../../strongs/h/h1870.md) of the [kerem](../../strongs/h/h3754.md).

<a name="job_24_19"></a>Job 24:19

[ṣîyâ](../../strongs/h/h6723.md) and [ḥōm](../../strongs/h/h2527.md) [gāzal](../../strongs/h/h1497.md) the [šeleḡ](../../strongs/h/h7950.md) [mayim](../../strongs/h/h4325.md): so doth the [shĕ'owl](../../strongs/h/h7585.md) those which have [chata'](../../strongs/h/h2398.md).

<a name="job_24_20"></a>Job 24:20

The [reḥem](../../strongs/h/h7358.md) shall [shakach](../../strongs/h/h7911.md) him; the [rimmâ](../../strongs/h/h7415.md) shall feed [māṯāq](../../strongs/h/h4988.md) on him; he shall be no more [zakar](../../strongs/h/h2142.md); and ['evel](../../strongs/h/h5766.md) shall be [shabar](../../strongs/h/h7665.md) as an ['ets](../../strongs/h/h6086.md).

<a name="job_24_21"></a>Job 24:21

He evil [ra'ah](../../strongs/h/h7462.md) the [ʿāqār](../../strongs/h/h6135.md) that [yalad](../../strongs/h/h3205.md) not: and doeth not [yatab](../../strongs/h/h3190.md) to the ['almānâ](../../strongs/h/h490.md).

<a name="job_24_22"></a>Job 24:22

He [mashak](../../strongs/h/h4900.md) also the ['abîr](../../strongs/h/h47.md) with his [koach](../../strongs/h/h3581.md): he riseth [quwm](../../strongs/h/h6965.md), and no man is ['aman](../../strongs/h/h539.md) of [chay](../../strongs/h/h2416.md).

<a name="job_24_23"></a>Job 24:23

Though it be [nathan](../../strongs/h/h5414.md) him to be in [betach](../../strongs/h/h983.md), whereon he [šāʿan](../../strongs/h/h8172.md); yet his ['ayin](../../strongs/h/h5869.md) are upon their [derek](../../strongs/h/h1870.md).

<a name="job_24_24"></a>Job 24:24

They are [rāmam](../../strongs/h/h7426.md) for a [mᵊʿaṭ](../../strongs/h/h4592.md), but are gone and brought [māḵaḵ](../../strongs/h/h4355.md); they are taken [qāp̄aṣ](../../strongs/h/h7092.md) of the [derek](../../strongs/h/h1870.md) as all other, and [namal](../../strongs/h/h5243.md) as the [ro'sh](../../strongs/h/h7218.md) of the ears of [šibōleṯ](../../strongs/h/h7641.md).

<a name="job_24_25"></a>Job 24:25

And if it be not so now, who will [śûm](../../strongs/h/h7760.md) me a [kāzaḇ](../../strongs/h/h3576.md), and [śûm](../../strongs/h/h7760.md) my [millâ](../../strongs/h/h4405.md) nothing worth?

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 23](job_23.md) - [Job 25](job_25.md)