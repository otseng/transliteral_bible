# [Job 2](https://www.blueletterbible.org/kjv/job/2)

<a name="job_2_1"></a>Job 2:1

Again there was a [yowm](../../strongs/h/h3117.md) when the [ben](../../strongs/h/h1121.md) of ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) to [yatsab](../../strongs/h/h3320.md) themselves before [Yĕhovah](../../strongs/h/h3068.md), and [satan](../../strongs/h/h7854.md) [bow'](../../strongs/h/h935.md) also among them to [yatsab](../../strongs/h/h3320.md) himself before [Yĕhovah](../../strongs/h/h3068.md).

<a name="job_2_2"></a>Job 2:2

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [satan](../../strongs/h/h7854.md), From whence [bow'](../../strongs/h/h935.md) thou? And [satan](../../strongs/h/h7854.md) ['anah](../../strongs/h/h6030.md) [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), [šûṭ](../../strongs/h/h7751.md) in the ['erets](../../strongs/h/h776.md), and [halak](../../strongs/h/h1980.md) in it.

<a name="job_2_3"></a>Job 2:3

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [satan](../../strongs/h/h7854.md), Hast [śûm](../../strongs/h/h7760.md) [leb](../../strongs/h/h3820.md) my ['ebed](../../strongs/h/h5650.md) ['Îyôḇ](../../strongs/h/h347.md), that there is none like him in the ['erets](../../strongs/h/h776.md), a [tām](../../strongs/h/h8535.md) and a [yashar](../../strongs/h/h3477.md) ['iysh](../../strongs/h/h376.md), one that [yārē'](../../strongs/h/h3373.md) ['Elohiym](../../strongs/h/h430.md), and [cuwr](../../strongs/h/h5493.md) [ra'](../../strongs/h/h7451.md)? and still he [ḥāzaq](../../strongs/h/h2388.md) his [tummâ](../../strongs/h/h8538.md), although thou [sûṯ](../../strongs/h/h5496.md) me against him, to [bālaʿ](../../strongs/h/h1104.md) him without [ḥinnām](../../strongs/h/h2600.md).

<a name="job_2_4"></a>Job 2:4

And [satan](../../strongs/h/h7854.md) ['anah](../../strongs/h/h6030.md) [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), ['owr](../../strongs/h/h5785.md) for ['owr](../../strongs/h/h5785.md), yea, all that an ['iysh](../../strongs/h/h376.md) hath will he [nathan](../../strongs/h/h5414.md) for his [nephesh](../../strongs/h/h5315.md).

<a name="job_2_5"></a>Job 2:5

But [shalach](../../strongs/h/h7971.md) thine [yad](../../strongs/h/h3027.md) now, and [naga'](../../strongs/h/h5060.md) his ['etsem](../../strongs/h/h6106.md) and his [basar](../../strongs/h/h1320.md), and he will [barak](../../strongs/h/h1288.md) thee to thy [paniym](../../strongs/h/h6440.md).

<a name="job_2_6"></a>Job 2:6

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [satan](../../strongs/h/h7854.md), Behold, he is in thine [yad](../../strongs/h/h3027.md); but [shamar](../../strongs/h/h8104.md) his [nephesh](../../strongs/h/h5315.md).

<a name="job_2_7"></a>Job 2:7

So [yāṣā'](../../strongs/h/h3318.md) [satan](../../strongs/h/h7854.md) [yāṣā'](../../strongs/h/h3318.md) from the [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md), and [nakah](../../strongs/h/h5221.md) ['Îyôḇ](../../strongs/h/h347.md) with [ra'](../../strongs/h/h7451.md) [šiḥîn](../../strongs/h/h7822.md) from the [kaph](../../strongs/h/h3709.md) of his [regel](../../strongs/h/h7272.md) unto his [qodqod](../../strongs/h/h6936.md).

<a name="job_2_8"></a>Job 2:8

And he [laqach](../../strongs/h/h3947.md) him a [ḥereś](../../strongs/h/h2789.md) to [gāraḏ](../../strongs/h/h1623.md) himself withal; and he [yashab](../../strongs/h/h3427.md) among the ['ēp̄er](../../strongs/h/h665.md).

<a name="job_2_9"></a>Job 2:9

Then ['āmar](../../strongs/h/h559.md) his ['ishshah](../../strongs/h/h802.md) unto him, Dost thou still [ḥāzaq](../../strongs/h/h2388.md) thine [tummâ](../../strongs/h/h8538.md)? [barak](../../strongs/h/h1288.md) ['Elohiym](../../strongs/h/h430.md), and [muwth](../../strongs/h/h4191.md).

<a name="job_2_10"></a>Job 2:10

But he ['āmar](../../strongs/h/h559.md) unto her, Thou [dabar](../../strongs/h/h1696.md) as one of the [nabal](../../strongs/h/h5036.md) [dabar](../../strongs/h/h1696.md). What? shall we [qāḇal](../../strongs/h/h6901.md) [towb](../../strongs/h/h2896.md) at the hand of ['Elohiym](../../strongs/h/h430.md), and shall we not [qāḇal](../../strongs/h/h6901.md) [ra'](../../strongs/h/h7451.md)? In all this did not ['Îyôḇ](../../strongs/h/h347.md) [chata'](../../strongs/h/h2398.md) with his [saphah](../../strongs/h/h8193.md).

<a name="job_2_11"></a>Job 2:11

Now when ['Îyôḇ](../../strongs/h/h347.md) three [rea'](../../strongs/h/h7453.md) [shama'](../../strongs/h/h8085.md) of all this [ra'](../../strongs/h/h7451.md) that was [bow'](../../strongs/h/h935.md) upon him, they [bow'](../../strongs/h/h935.md) every ['iysh](../../strongs/h/h376.md) from his own [maqowm](../../strongs/h/h4725.md); ['Ĕlîp̄az](../../strongs/h/h464.md) the [têmānî](../../strongs/h/h8489.md), and [Bildaḏ](../../strongs/h/h1085.md) the [šûḥî](../../strongs/h/h7747.md), and [Ṣôp̄Ar](../../strongs/h/h6691.md) the [naʿămāṯî](../../strongs/h/h5284.md): for they had made a [yāʿaḏ](../../strongs/h/h3259.md) [yaḥaḏ](../../strongs/h/h3162.md) to [bow'](../../strongs/h/h935.md) to [nuwd](../../strongs/h/h5110.md) with him and to [nacham](../../strongs/h/h5162.md) him.

<a name="job_2_12"></a>Job 2:12

And when they [nasa'](../../strongs/h/h5375.md) their ['ayin](../../strongs/h/h5869.md) afar [rachowq](../../strongs/h/h7350.md), and [nāḵar](../../strongs/h/h5234.md) him not, they [nasa'](../../strongs/h/h5375.md) their [qowl](../../strongs/h/h6963.md), and [bāḵâ](../../strongs/h/h1058.md); and they [qāraʿ](../../strongs/h/h7167.md) every ['iysh](../../strongs/h/h376.md) his [mᵊʿîl](../../strongs/h/h4598.md), and [zāraq](../../strongs/h/h2236.md) ['aphar](../../strongs/h/h6083.md) upon their [ro'sh](../../strongs/h/h7218.md) toward [shamayim](../../strongs/h/h8064.md).

<a name="job_2_13"></a>Job 2:13

So they [yashab](../../strongs/h/h3427.md) with him upon the ['erets](../../strongs/h/h776.md) seven [yowm](../../strongs/h/h3117.md) and seven [layil](../../strongs/h/h3915.md), and none [dabar](../../strongs/h/h1696.md) a [dabar](../../strongs/h/h1697.md) unto him: for they [ra'ah](../../strongs/h/h7200.md) that his [kᵊ'ēḇ](../../strongs/h/h3511.md) was [me'od](../../strongs/h/h3966.md) [gāḏal](../../strongs/h/h1431.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 1](job_1.md) - [Job 3](job_3.md)