# [Job 26](https://www.blueletterbible.org/kjv/job/26)

<a name="job_26_1"></a>Job 26:1

But ['Îyôḇ](../../strongs/h/h347.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md),

<a name="job_26_2"></a>Job 26:2

How hast thou [ʿāzar](../../strongs/h/h5826.md) him that is without [koach](../../strongs/h/h3581.md)? how [yasha'](../../strongs/h/h3467.md) thou the [zerowa'](../../strongs/h/h2220.md) that hath no ['oz](../../strongs/h/h5797.md)?

<a name="job_26_3"></a>Job 26:3

How hast thou [ya'ats](../../strongs/h/h3289.md) him that hath no [ḥāḵmâ](../../strongs/h/h2451.md)? and how hast thou [rōḇ](../../strongs/h/h7230.md) [yada'](../../strongs/h/h3045.md) the thing as it [tûšîyâ](../../strongs/h/h8454.md)?

<a name="job_26_4"></a>Job 26:4

To whom hast thou [nāḡaḏ](../../strongs/h/h5046.md) [millâ](../../strongs/h/h4405.md)? and whose [neshamah](../../strongs/h/h5397.md) [yāṣā'](../../strongs/h/h3318.md) from thee?

<a name="job_26_5"></a>Job 26:5

[rᵊp̄ā'îm](../../strongs/h/h7496.md) things are [chuwl](../../strongs/h/h2342.md) from under the [mayim](../../strongs/h/h4325.md), and the [shakan](../../strongs/h/h7931.md) thereof.

<a name="job_26_6"></a>Job 26:6

[shĕ'owl](../../strongs/h/h7585.md) is ['arowm](../../strongs/h/h6174.md) before him, and ['Ăḇadôn](../../strongs/h/h11.md) hath no [kᵊsûṯ](../../strongs/h/h3682.md).

<a name="job_26_7"></a>Job 26:7

He [natah](../../strongs/h/h5186.md) the [ṣāp̄ôn](../../strongs/h/h6828.md) over the [tohuw](../../strongs/h/h8414.md), and [tālâ](../../strongs/h/h8518.md) the ['erets](../../strongs/h/h776.md) upon [bᵊlîmâ](../../strongs/h/h1099.md).

<a name="job_26_8"></a>Job 26:8

He [tsarar](../../strongs/h/h6887.md) the [mayim](../../strongs/h/h4325.md) in his ['ab](../../strongs/h/h5645.md); and the [ʿānān](../../strongs/h/h6051.md) is not [bāqaʿ](../../strongs/h/h1234.md) under them.

<a name="job_26_9"></a>Job 26:9

He ['āḥaz](../../strongs/h/h270.md) the [paniym](../../strongs/h/h6440.md) of his [kicce'](../../strongs/h/h3678.md), and [paršēz](../../strongs/h/h6576.md) his [ʿānān](../../strongs/h/h6051.md) upon it.

<a name="job_26_10"></a>Job 26:10

He hath [ḥûḡ](../../strongs/h/h2328.md) the [paniym](../../strongs/h/h6440.md) [mayim](../../strongs/h/h4325.md) with [choq](../../strongs/h/h2706.md), until the ['owr](../../strongs/h/h216.md) and [choshek](../../strongs/h/h2822.md) come to a [taḵlîṯ](../../strongs/h/h8503.md).

<a name="job_26_11"></a>Job 26:11

The [ʿammûḏ](../../strongs/h/h5982.md) of [shamayim](../../strongs/h/h8064.md) [rûp̄](../../strongs/h/h7322.md) and are [tāmah](../../strongs/h/h8539.md) at his [ge'arah](../../strongs/h/h1606.md).

<a name="job_26_12"></a>Job 26:12

He [rāḡaʿ](../../strongs/h/h7280.md) the [yam](../../strongs/h/h3220.md) with his [koach](../../strongs/h/h3581.md), and by his [tāḇûn](../../strongs/h/h8394.md) [tāḇûn](../../strongs/h/h8394.md) he [māḥaṣ](../../strongs/h/h4272.md) through the [rahaḇ](../../strongs/h/h7293.md).

<a name="job_26_13"></a>Job 26:13

By his [ruwach](../../strongs/h/h7307.md) he hath [šip̄râ](../../strongs/h/h8235.md) the [shamayim](../../strongs/h/h8064.md); his [yad](../../strongs/h/h3027.md) hath [chuwl](../../strongs/h/h2342.md) the [bāriaḥ](../../strongs/h/h1281.md) [nachash](../../strongs/h/h5175.md).

<a name="job_26_14"></a>Job 26:14

Lo, these are [qāṣâ](../../strongs/h/h7098.md) of his [derek](../../strongs/h/h1870.md): but how [šemeṣ](../../strongs/h/h8102.md) a [dabar](../../strongs/h/h1697.md) is [shama'](../../strongs/h/h8085.md) of him? but the [raʿam](../../strongs/h/h7482.md) of his [gᵊḇûrâ](../../strongs/h/h1369.md) who can [bîn](../../strongs/h/h995.md)?

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 25](job_25.md) - [Job 27](job_27.md)