# [Job 33](https://www.blueletterbible.org/kjv/job/33)

<a name="job_33_1"></a>Job 33:1

Wherefore, ['Îyôḇ](../../strongs/h/h347.md), I pray thee, [shama'](../../strongs/h/h8085.md) my [millâ](../../strongs/h/h4405.md), and ['azan](../../strongs/h/h238.md) to all my [dabar](../../strongs/h/h1697.md).

<a name="job_33_2"></a>Job 33:2

Behold, now I have [pāṯaḥ](../../strongs/h/h6605.md) my [peh](../../strongs/h/h6310.md), my [lashown](../../strongs/h/h3956.md) hath [dabar](../../strongs/h/h1696.md) in my [ḥēḵ](../../strongs/h/h2441.md).

<a name="job_33_3"></a>Job 33:3

My ['emer](../../strongs/h/h561.md) shall be of the [yōšer](../../strongs/h/h3476.md) of my [leb](../../strongs/h/h3820.md): and my [saphah](../../strongs/h/h8193.md) shall [mālal](../../strongs/h/h4448.md) [da'ath](../../strongs/h/h1847.md) [bārar](../../strongs/h/h1305.md).

<a name="job_33_4"></a>Job 33:4

The [ruwach](../../strongs/h/h7307.md) of ['el](../../strongs/h/h410.md) hath ['asah](../../strongs/h/h6213.md) me, and the [neshamah](../../strongs/h/h5397.md) of the [Šaday](../../strongs/h/h7706.md) hath [ḥāyâ](../../strongs/h/h2421.md) me.

<a name="job_33_5"></a>Job 33:5

If thou [yakol](../../strongs/h/h3201.md) [shuwb](../../strongs/h/h7725.md) me, set thy words in ['arak](../../strongs/h/h6186.md) [paniym](../../strongs/h/h6440.md) me, [yatsab](../../strongs/h/h3320.md).

<a name="job_33_6"></a>Job 33:6

Behold, I am according to thy [peh](../../strongs/h/h6310.md) in ['el](../../strongs/h/h410.md) stead: I also am [qāraṣ](../../strongs/h/h7169.md) out of the [ḥōmer](../../strongs/h/h2563.md).

<a name="job_33_7"></a>Job 33:7

Behold, my ['êmâ](../../strongs/h/h367.md) shall not make thee [ba'ath](../../strongs/h/h1204.md), neither shall my ['eḵep̄](../../strongs/h/h405.md) be [kabad](../../strongs/h/h3513.md) upon thee.

<a name="job_33_8"></a>Job 33:8

Surely thou hast ['āmar](../../strongs/h/h559.md) in mine ['ozen](../../strongs/h/h241.md), and I have [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of thy [millâ](../../strongs/h/h4405.md), saying,

<a name="job_33_9"></a>Job 33:9

I am [zāḵ](../../strongs/h/h2134.md) without [pesha'](../../strongs/h/h6588.md), I am [ḥap̄](../../strongs/h/h2643.md); neither is there ['avon](../../strongs/h/h5771.md) in me.

<a name="job_33_10"></a>Job 33:10

Behold, he [māṣā'](../../strongs/h/h4672.md) [tᵊnû'â](../../strongs/h/h8569.md) against me, he [chashab](../../strongs/h/h2803.md) me for his ['oyeb](../../strongs/h/h341.md),

<a name="job_33_11"></a>Job 33:11

He [śûm](../../strongs/h/h7760.md) my [regel](../../strongs/h/h7272.md) in the [saḏ](../../strongs/h/h5465.md), he [shamar](../../strongs/h/h8104.md) all my ['orach](../../strongs/h/h734.md).

<a name="job_33_12"></a>Job 33:12

Behold, in this thou art not [ṣāḏaq](../../strongs/h/h6663.md): I will ['anah](../../strongs/h/h6030.md) thee, that ['ĕlvôha](../../strongs/h/h433.md) is [rabah](../../strongs/h/h7235.md) than ['enowsh](../../strongs/h/h582.md).

<a name="job_33_13"></a>Job 33:13

Why dost thou [riyb](../../strongs/h/h7378.md) against him? for he giveth not ['anah](../../strongs/h/h6030.md) of any of his [dabar](../../strongs/h/h1697.md).

<a name="job_33_14"></a>Job 33:14

For ['el](../../strongs/h/h410.md) [dabar](../../strongs/h/h1696.md) once, yea twice, yet man [šûr](../../strongs/h/h7789.md) it not.

<a name="job_33_15"></a>Job 33:15

In a [ḥălôm](../../strongs/h/h2472.md), in a [ḥizzāyôn](../../strongs/h/h2384.md) of the [layil](../../strongs/h/h3915.md), when [tardēmâ](../../strongs/h/h8639.md) [naphal](../../strongs/h/h5307.md) upon ['enowsh](../../strongs/h/h582.md), in [tᵊnûmâ](../../strongs/h/h8572.md) upon the [miškāḇ](../../strongs/h/h4904.md);

<a name="job_33_16"></a>Job 33:16

Then he [gālâ](../../strongs/h/h1540.md) the ['ozen](../../strongs/h/h241.md) of ['enowsh](../../strongs/h/h582.md), and [ḥāṯam](../../strongs/h/h2856.md) their [mōsār](../../strongs/h/h4561.md),

<a name="job_33_17"></a>Job 33:17

That he may [cuwr](../../strongs/h/h5493.md) ['āḏām](../../strongs/h/h120.md) from his [ma'aseh](../../strongs/h/h4639.md), and [kāsâ](../../strongs/h/h3680.md) [gēvâ](../../strongs/h/h1466.md) from [geḇer](../../strongs/h/h1397.md).

<a name="job_33_18"></a>Job 33:18

He [ḥāśaḵ](../../strongs/h/h2820.md) his [nephesh](../../strongs/h/h5315.md) from the [shachath](../../strongs/h/h7845.md), and his [chay](../../strongs/h/h2416.md) from ['abar](../../strongs/h/h5674.md) by the [Šelaḥ](../../strongs/h/h7973.md).

<a name="job_33_19"></a>Job 33:19

He is [yakach](../../strongs/h/h3198.md) also with [maḵ'ōḇ](../../strongs/h/h4341.md) upon his [miškāḇ](../../strongs/h/h4904.md), and the [rōḇ](../../strongs/h/h7230.md) [rîḇ](../../strongs/h/h7379.md) of his ['etsem](../../strongs/h/h6106.md) with ['êṯān](../../strongs/h/h386.md) pain:

<a name="job_33_20"></a>Job 33:20

So that his [chay](../../strongs/h/h2416.md) [zāham](../../strongs/h/h2092.md) [lechem](../../strongs/h/h3899.md), and his [nephesh](../../strongs/h/h5315.md) [ta'avah](../../strongs/h/h8378.md) [ma'akal](../../strongs/h/h3978.md).

<a name="job_33_21"></a>Job 33:21

His [basar](../../strongs/h/h1320.md) is [kalah](../../strongs/h/h3615.md), that it cannot be [rŏ'î](../../strongs/h/h7210.md); and his ['etsem](../../strongs/h/h6106.md) that were not [ra'ah](../../strongs/h/h7200.md) stick [šāp̄â](../../strongs/h/h8192.md) [šᵊp̄î](../../strongs/h/h8205.md).

<a name="job_33_22"></a>Job 33:22

Yea, his [nephesh](../../strongs/h/h5315.md) [qāraḇ](../../strongs/h/h7126.md) unto the [shachath](../../strongs/h/h7845.md), and his [chay](../../strongs/h/h2416.md) to the [muwth](../../strongs/h/h4191.md).

<a name="job_33_23"></a>Job 33:23

If there be a [mal'ak](../../strongs/h/h4397.md) with him, a [luwts](../../strongs/h/h3887.md), one among a thousand, to [nāḡaḏ](../../strongs/h/h5046.md) unto ['āḏām](../../strongs/h/h120.md) his [yōšer](../../strongs/h/h3476.md):

<a name="job_33_24"></a>Job 33:24

Then he is [chanan](../../strongs/h/h2603.md) unto him, and ['āmar](../../strongs/h/h559.md), [pāḏaʿ](../../strongs/h/h6308.md) him from [yarad](../../strongs/h/h3381.md) to the [shachath](../../strongs/h/h7845.md): I have [māṣā'](../../strongs/h/h4672.md) a [kōp̄er](../../strongs/h/h3724.md).

<a name="job_33_25"></a>Job 33:25

His [basar](../../strongs/h/h1320.md) shall be [ruṭăp̄aš](../../strongs/h/h7375.md) than a [nōʿar](../../strongs/h/h5290.md): he shall [shuwb](../../strongs/h/h7725.md) to the [yowm](../../strongs/h/h3117.md) of his [ʿălûmîm](../../strongs/h/h5934.md):

<a name="job_33_26"></a>Job 33:26

He shall [ʿāṯar](../../strongs/h/h6279.md) unto ['ĕlvôha](../../strongs/h/h433.md), and he will be [ratsah](../../strongs/h/h7521.md) unto him: and he shall [ra'ah](../../strongs/h/h7200.md) his [paniym](../../strongs/h/h6440.md) with [tᵊrûʿâ](../../strongs/h/h8643.md): for he will [shuwb](../../strongs/h/h7725.md) unto ['enowsh](../../strongs/h/h582.md) his [tsedaqah](../../strongs/h/h6666.md).

<a name="job_33_27"></a>Job 33:27

He [šûr](../../strongs/h/h7789.md) upon ['enowsh](../../strongs/h/h582.md), and if any ['āmar](../../strongs/h/h559.md), I have [chata'](../../strongs/h/h2398.md), and [ʿāvâ](../../strongs/h/h5753.md) that which was [yashar](../../strongs/h/h3477.md), and it [šāvâ](../../strongs/h/h7737.md) me not;

<a name="job_33_28"></a>Job 33:28

He will [pāḏâ](../../strongs/h/h6299.md) his [nephesh](../../strongs/h/h5315.md) from ['abar](../../strongs/h/h5674.md) into the [shachath](../../strongs/h/h7845.md), and his [chay](../../strongs/h/h2416.md) shall [ra'ah](../../strongs/h/h7200.md) the ['owr](../../strongs/h/h216.md).

<a name="job_33_29"></a>Job 33:29

Lo, all these things [pa'al](../../strongs/h/h6466.md) ['el](../../strongs/h/h410.md) oftentimes  with [geḇer](../../strongs/h/h1397.md),

<a name="job_33_30"></a>Job 33:30

To [shuwb](../../strongs/h/h7725.md) his [nephesh](../../strongs/h/h5315.md) from the [shachath](../../strongs/h/h7845.md), to be ['owr](../../strongs/h/h215.md) with the ['owr](../../strongs/h/h216.md) of the [chay](../../strongs/h/h2416.md).

<a name="job_33_31"></a>Job 33:31

[qashab](../../strongs/h/h7181.md), O ['Îyôḇ](../../strongs/h/h347.md), [shama'](../../strongs/h/h8085.md) unto me: hold thy [ḥāraš](../../strongs/h/h2790.md), and I will [dabar](../../strongs/h/h1696.md).

<a name="job_33_32"></a>Job 33:32

If thou hast any thing to [millâ](../../strongs/h/h4405.md), [shuwb](../../strongs/h/h7725.md) me: [dabar](../../strongs/h/h1696.md), for I [ḥāp̄ēṣ](../../strongs/h/h2654.md) to [ṣāḏaq](../../strongs/h/h6663.md) thee.

<a name="job_33_33"></a>Job 33:33

If not, [shama'](../../strongs/h/h8085.md) unto me: hold thy [ḥāraš](../../strongs/h/h2790.md), and I shall ['ālap̄](../../strongs/h/h502.md) thee [ḥāḵmâ](../../strongs/h/h2451.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 32](job_32.md) - [Job 34](job_34.md)