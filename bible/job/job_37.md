# [Job 37](https://www.blueletterbible.org/kjv/job/37)

<a name="job_37_1"></a>Job 37:1

At this also my [leb](../../strongs/h/h3820.md) [ḥārēḏ](../../strongs/h/h2729.md), and is [nāṯar](../../strongs/h/h5425.md) of his [maqowm](../../strongs/h/h4725.md).

<a name="job_37_2"></a>Job 37:2

[shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) the [rōḡez](../../strongs/h/h7267.md) of his [qowl](../../strongs/h/h6963.md), and the [heḡê](../../strongs/h/h1899.md) that [yāṣā'](../../strongs/h/h3318.md) of his [peh](../../strongs/h/h6310.md).

<a name="job_37_3"></a>Job 37:3

He [yashar](../../strongs/h/h3474.md) [šārâ](../../strongs/h/h8281.md) under the [shamayim](../../strongs/h/h8064.md), and his ['owr](../../strongs/h/h216.md) unto the [kanaph](../../strongs/h/h3671.md) of the ['erets](../../strongs/h/h776.md).

<a name="job_37_4"></a>Job 37:4

['aḥar](../../strongs/h/h310.md) it a [qowl](../../strongs/h/h6963.md) [šā'aḡ](../../strongs/h/h7580.md): he [ra'am](../../strongs/h/h7481.md) with the [qowl](../../strongs/h/h6963.md) of his [gā'ôn](../../strongs/h/h1347.md); and he will not [ʿāqaḇ](../../strongs/h/h6117.md) them when his [qowl](../../strongs/h/h6963.md) is [shama'](../../strongs/h/h8085.md).

<a name="job_37_5"></a>Job 37:5

['el](../../strongs/h/h410.md) [ra'am](../../strongs/h/h7481.md) [pala'](../../strongs/h/h6381.md) with his [qowl](../../strongs/h/h6963.md); [gadowl](../../strongs/h/h1419.md) ['asah](../../strongs/h/h6213.md) he, which we cannot [yada'](../../strongs/h/h3045.md).

<a name="job_37_6"></a>Job 37:6

For he ['āmar](../../strongs/h/h559.md) to the [šeleḡ](../../strongs/h/h7950.md), Be thou on the ['erets](../../strongs/h/h776.md); likewise to the [māṭār](../../strongs/h/h4306.md) [gešem](../../strongs/h/h1653.md), and to the [māṭār](../../strongs/h/h4306.md) [gešem](../../strongs/h/h1653.md) of his ['oz](../../strongs/h/h5797.md).

<a name="job_37_7"></a>Job 37:7

He [ḥāṯam](../../strongs/h/h2856.md) the [yad](../../strongs/h/h3027.md) of every ['āḏām](../../strongs/h/h120.md); that all ['enowsh](../../strongs/h/h582.md) may [yada'](../../strongs/h/h3045.md) his [ma'aseh](../../strongs/h/h4639.md).

<a name="job_37_8"></a>Job 37:8

Then the [chay](../../strongs/h/h2416.md) [bow'](../../strongs/h/h935.md) [bᵊmô](../../strongs/h/h1119.md) ['ereḇ](../../strongs/h/h695.md), and [shakan](../../strongs/h/h7931.md) in their [mᵊʿônâ](../../strongs/h/h4585.md).

<a name="job_37_9"></a>Job 37:9

Out of the [ḥeḏer](../../strongs/h/h2315.md) [bow'](../../strongs/h/h935.md) the [sûp̄â](../../strongs/h/h5492.md): and [qārâ](../../strongs/h/h7135.md) out of the [mᵊzārîm](../../strongs/h/h4215.md).

<a name="job_37_10"></a>Job 37:10

By the [neshamah](../../strongs/h/h5397.md) of ['el](../../strongs/h/h410.md) [qeraḥ](../../strongs/h/h7140.md) is [nathan](../../strongs/h/h5414.md): and the [rōḥaḇ](../../strongs/h/h7341.md) of the [mayim](../../strongs/h/h4325.md) is [mûṣaq](../../strongs/h/h4164.md).

<a name="job_37_11"></a>Job 37:11

Also by [rî](../../strongs/h/h7377.md) he [ṭāraḥ](../../strongs/h/h2959.md) the ['ab](../../strongs/h/h5645.md): he [puwts](../../strongs/h/h6327.md) his ['owr](../../strongs/h/h216.md) [ʿānān](../../strongs/h/h6051.md):

<a name="job_37_12"></a>Job 37:12

And it is [hāp̄aḵ](../../strongs/h/h2015.md) [mēsaḇ](../../strongs/h/h4524.md) by his [taḥbulôṯ](../../strongs/h/h8458.md): that they may [pōʿal](../../strongs/h/h6467.md) whatsoever he [tsavah](../../strongs/h/h6680.md) them upon the [paniym](../../strongs/h/h6440.md) of the [tebel](../../strongs/h/h8398.md) in the ['erets](../../strongs/h/h776.md).

<a name="job_37_13"></a>Job 37:13

He causeth it to [māṣā'](../../strongs/h/h4672.md), whether for [shebet](../../strongs/h/h7626.md), or for his ['erets](../../strongs/h/h776.md), or for [checed](../../strongs/h/h2617.md).

<a name="job_37_14"></a>Job 37:14

['azan](../../strongs/h/h238.md) unto this, O ['Îyôḇ](../../strongs/h/h347.md): ['amad](../../strongs/h/h5975.md), and [bîn](../../strongs/h/h995.md) the [pala'](../../strongs/h/h6381.md) works of ['el](../../strongs/h/h410.md).

<a name="job_37_15"></a>Job 37:15

Dost thou [yada'](../../strongs/h/h3045.md) when ['ĕlvôha](../../strongs/h/h433.md) [śûm](../../strongs/h/h7760.md) them, and caused the ['owr](../../strongs/h/h216.md) of his [ʿānān](../../strongs/h/h6051.md) to [yāp̄aʿ](../../strongs/h/h3313.md)?

<a name="job_37_16"></a>Job 37:16

Dost thou [yada'](../../strongs/h/h3045.md) the [mip̄lāś](../../strongs/h/h4657.md) of the ['ab](../../strongs/h/h5645.md), the [mip̄lā'â](../../strongs/h/h4652.md) of him which is [tamiym](../../strongs/h/h8549.md) in [dēaʿ](../../strongs/h/h1843.md)?

<a name="job_37_17"></a>Job 37:17

How thy [beḡeḏ](../../strongs/h/h899.md) are [ḥām](../../strongs/h/h2525.md), when he [šāqaṭ](../../strongs/h/h8252.md) the ['erets](../../strongs/h/h776.md) by the [dārôm](../../strongs/h/h1864.md)?

<a name="job_37_18"></a>Job 37:18

Hast thou with him [rāqaʿ](../../strongs/h/h7554.md) the [shachaq](../../strongs/h/h7834.md), which is [ḥāzāq](../../strongs/h/h2389.md), and as a [yāṣaq](../../strongs/h/h3332.md) looking [rᵊ'î](../../strongs/h/h7209.md)?

<a name="job_37_19"></a>Job 37:19

[yada'](../../strongs/h/h3045.md) us what we shall ['āmar](../../strongs/h/h559.md) unto him; for we cannot ['arak](../../strongs/h/h6186.md) our speech by [paniym](../../strongs/h/h6440.md) of [choshek](../../strongs/h/h2822.md).

<a name="job_37_20"></a>Job 37:20

Shall it be [sāp̄ar](../../strongs/h/h5608.md) him that I [dabar](../../strongs/h/h1696.md)? if an ['iysh](../../strongs/h/h376.md) ['āmar](../../strongs/h/h559.md), surely he shall be [bālaʿ](../../strongs/h/h1104.md).

<a name="job_37_21"></a>Job 37:21

And now men [ra'ah](../../strongs/h/h7200.md) not the [bāhîr](../../strongs/h/h925.md) ['owr](../../strongs/h/h216.md) which is in the [shachaq](../../strongs/h/h7834.md): but the [ruwach](../../strongs/h/h7307.md) ['abar](../../strongs/h/h5674.md), and [ṭāhēr](../../strongs/h/h2891.md) them.

<a name="job_37_22"></a>Job 37:22

Fair [zāhāḇ](../../strongs/h/h2091.md) ['āṯâ](../../strongs/h/h857.md) out of the [ṣāp̄ôn](../../strongs/h/h6828.md): with ['ĕlvôha](../../strongs/h/h433.md) is [yare'](../../strongs/h/h3372.md) [howd](../../strongs/h/h1935.md).

<a name="job_37_23"></a>Job 37:23

Touching the [Šaday](../../strongs/h/h7706.md), we cannot [māṣā'](../../strongs/h/h4672.md) him: he is [śagî'](../../strongs/h/h7689.md) in [koach](../../strongs/h/h3581.md), and in [mishpat](../../strongs/h/h4941.md), and in [rōḇ](../../strongs/h/h7230.md) of [tsedaqah](../../strongs/h/h6666.md): he will not [ʿānâ](../../strongs/h/h6031.md).

<a name="job_37_24"></a>Job 37:24

['enowsh](../../strongs/h/h582.md) do therefore [yare'](../../strongs/h/h3372.md) him: he [ra'ah](../../strongs/h/h7200.md) not any that are [ḥāḵām](../../strongs/h/h2450.md) of [leb](../../strongs/h/h3820.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 36](job_36.md) - [Job 38](job_38.md)