# [Job 17](https://www.blueletterbible.org/kjv/job/17)

<a name="job_17_1"></a>Job 17:1

My [ruwach](../../strongs/h/h7307.md) is [chabal](../../strongs/h/h2254.md), my [yowm](../../strongs/h/h3117.md) are [zāʿaḵ](../../strongs/h/h2193.md), the [qeber](../../strongs/h/h6913.md) are ready for me.

<a name="job_17_2"></a>Job 17:2

Are there not [hăṯullîm](../../strongs/h/h2049.md) with me? and doth not mine ['ayin](../../strongs/h/h5869.md) [lûn](../../strongs/h/h3885.md) in their [marah](../../strongs/h/h4784.md)?

<a name="job_17_3"></a>Job 17:3

[śûm](../../strongs/h/h7760.md) now, put me in a [ʿāraḇ](../../strongs/h/h6148.md) with thee; who is he that will [tāqaʿ](../../strongs/h/h8628.md) [yad](../../strongs/h/h3027.md) with me?

<a name="job_17_4"></a>Job 17:4

For thou hast [tsaphan](../../strongs/h/h6845.md) their [leb](../../strongs/h/h3820.md) from [śēḵel](../../strongs/h/h7922.md): therefore shalt thou not [ruwm](../../strongs/h/h7311.md) them.

<a name="job_17_5"></a>Job 17:5

He that [nāḡaḏ](../../strongs/h/h5046.md) [cheleq](../../strongs/h/h2506.md) to his [rea'](../../strongs/h/h7453.md), even the ['ayin](../../strongs/h/h5869.md) of his [ben](../../strongs/h/h1121.md) shall [kalah](../../strongs/h/h3615.md).

<a name="job_17_6"></a>Job 17:6

He hath [yāṣaḡ](../../strongs/h/h3322.md) me also a [mᵊšōl](../../strongs/h/h4914.md) of the ['am](../../strongs/h/h5971.md); and [paniym](../../strongs/h/h6440.md) I was as a [tōp̄eṯ](../../strongs/h/h8611.md).

<a name="job_17_7"></a>Job 17:7

Mine ['ayin](../../strongs/h/h5869.md) also is [kāhâ](../../strongs/h/h3543.md) by reason of [ka'ac](../../strongs/h/h3708.md), and all my [yᵊṣurîm](../../strongs/h/h3338.md) are as a [ṣēl](../../strongs/h/h6738.md).

<a name="job_17_8"></a>Job 17:8

[yashar](../../strongs/h/h3477.md) men shall be [šāmēm](../../strongs/h/h8074.md) at this, and the [naqiy](../../strongs/h/h5355.md) shall stir [ʿûr](../../strongs/h/h5782.md) himself against the [ḥānēp̄](../../strongs/h/h2611.md).

<a name="job_17_9"></a>Job 17:9

The [tsaddiyq](../../strongs/h/h6662.md) also shall ['āḥaz](../../strongs/h/h270.md) on his [derek](../../strongs/h/h1870.md), and he that hath [ṭᵊhôr](../../strongs/h/h2890.md) [yad](../../strongs/h/h3027.md) shall be stronger and ['ōmeṣ](../../strongs/h/h555.md).

<a name="job_17_10"></a>Job 17:10

But as for you all, do ye [shuwb](../../strongs/h/h7725.md), and [bow'](../../strongs/h/h935.md): for I cannot [māṣā'](../../strongs/h/h4672.md) one [ḥāḵām](../../strongs/h/h2450.md) man among you.

<a name="job_17_11"></a>Job 17:11

My [yowm](../../strongs/h/h3117.md) are ['abar](../../strongs/h/h5674.md), my [zimmâ](../../strongs/h/h2154.md) are [nathaq](../../strongs/h/h5423.md), even the [môrāš](../../strongs/h/h4180.md) of my [lebab](../../strongs/h/h3824.md).

<a name="job_17_12"></a>Job 17:12

They [śûm](../../strongs/h/h7760.md) the [layil](../../strongs/h/h3915.md) into [yowm](../../strongs/h/h3117.md): the ['owr](../../strongs/h/h216.md) is [qarowb](../../strongs/h/h7138.md) [paniym](../../strongs/h/h6440.md) of [choshek](../../strongs/h/h2822.md).

<a name="job_17_13"></a>Job 17:13

If I [qāvâ](../../strongs/h/h6960.md), the [shĕ'owl](../../strongs/h/h7585.md) is mine [bayith](../../strongs/h/h1004.md): I have [rāp̄aḏ](../../strongs/h/h7502.md) my [yāṣûaʿ](../../strongs/h/h3326.md) in the [choshek](../../strongs/h/h2822.md).

<a name="job_17_14"></a>Job 17:14

I have [qara'](../../strongs/h/h7121.md) to [shachath](../../strongs/h/h7845.md), Thou art my ['ab](../../strongs/h/h1.md): to the [rimmâ](../../strongs/h/h7415.md), Thou art my ['em](../../strongs/h/h517.md), and my ['āḥôṯ](../../strongs/h/h269.md).

<a name="job_17_15"></a>Job 17:15

And where is now my [tiqvâ](../../strongs/h/h8615.md)? as for my [tiqvâ](../../strongs/h/h8615.md), who shall [šûr](../../strongs/h/h7789.md) it?

<a name="job_17_16"></a>Job 17:16

They shall [yarad](../../strongs/h/h3381.md) to the [baḏ](../../strongs/h/h905.md) of the [shĕ'owl](../../strongs/h/h7585.md), when our [naḥaṯ](../../strongs/h/h5183.md) [yaḥaḏ](../../strongs/h/h3162.md) is in the ['aphar](../../strongs/h/h6083.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 16](job_16.md) - [Job 18](job_18.md)