# [Job 23](https://www.blueletterbible.org/kjv/job/23)

<a name="job_23_1"></a>Job 23:1

Then ['Îyôḇ](../../strongs/h/h347.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md),

<a name="job_23_2"></a>Job 23:2

Even to [yowm](../../strongs/h/h3117.md) is my [śîaḥ](../../strongs/h/h7879.md) [mᵊrî](../../strongs/h/h4805.md): my [yad](../../strongs/h/h3027.md) is [kabad](../../strongs/h/h3513.md) than my ['anachah](../../strongs/h/h585.md).

<a name="job_23_3"></a>Job 23:3

Oh [nathan](../../strongs/h/h5414.md) I [yada'](../../strongs/h/h3045.md) where I might [māṣā'](../../strongs/h/h4672.md) him! that I might [bow'](../../strongs/h/h935.md) even to his [tᵊḵûnâ](../../strongs/h/h8499.md)!

<a name="job_23_4"></a>Job 23:4

I would ['arak](../../strongs/h/h6186.md) my [mishpat](../../strongs/h/h4941.md) [paniym](../../strongs/h/h6440.md) him, and [mālā'](../../strongs/h/h4390.md) my [peh](../../strongs/h/h6310.md) with [tôḵēḥâ](../../strongs/h/h8433.md).

<a name="job_23_5"></a>Job 23:5

I would [yada'](../../strongs/h/h3045.md) the [millâ](../../strongs/h/h4405.md) which he would ['anah](../../strongs/h/h6030.md) me, and [bîn](../../strongs/h/h995.md) what he would ['āmar](../../strongs/h/h559.md) unto me.

<a name="job_23_6"></a>Job 23:6

Will he [riyb](../../strongs/h/h7378.md) against me with his [rōḇ](../../strongs/h/h7230.md) [koach](../../strongs/h/h3581.md)? No; but he would [śûm](../../strongs/h/h7760.md) strength in me.

<a name="job_23_7"></a>Job 23:7

There the [yashar](../../strongs/h/h3477.md) might [yakach](../../strongs/h/h3198.md) with him; so should I be [palat](../../strongs/h/h6403.md) for [netsach](../../strongs/h/h5331.md) from my [shaphat](../../strongs/h/h8199.md).

<a name="job_23_8"></a>Job 23:8

Behold, I [halak](../../strongs/h/h1980.md) [qeḏem](../../strongs/h/h6924.md), but he is not there; and ['āḥôr](../../strongs/h/h268.md), but I cannot [bîn](../../strongs/h/h995.md) him:

<a name="job_23_9"></a>Job 23:9

On the [śᵊmō'l](../../strongs/h/h8040.md), where he doth ['asah](../../strongs/h/h6213.md), but I cannot [chazah](../../strongs/h/h2372.md) him: he [ʿāṭap̄](../../strongs/h/h5848.md) himself on the [yamiyn](../../strongs/h/h3225.md), that I cannot [ra'ah](../../strongs/h/h7200.md) him:

<a name="job_23_10"></a>Job 23:10

But he [yada'](../../strongs/h/h3045.md) the [derek](../../strongs/h/h1870.md) that I take: when he hath [bachan](../../strongs/h/h974.md) me, I shall [yāṣā'](../../strongs/h/h3318.md) as [zāhāḇ](../../strongs/h/h2091.md).

<a name="job_23_11"></a>Job 23:11

My [regel](../../strongs/h/h7272.md) hath ['āḥaz](../../strongs/h/h270.md) his ['ashuwr](../../strongs/h/h838.md), his [derek](../../strongs/h/h1870.md) have I [shamar](../../strongs/h/h8104.md), and not [natah](../../strongs/h/h5186.md).

<a name="job_23_12"></a>Job 23:12

Neither have I gone [mûš](../../strongs/h/h4185.md) from the [mitsvah](../../strongs/h/h4687.md) of his [saphah](../../strongs/h/h8193.md); I have [tsaphan](../../strongs/h/h6845.md) the ['emer](../../strongs/h/h561.md) of his [peh](../../strongs/h/h6310.md) more than my [choq](../../strongs/h/h2706.md) food.

<a name="job_23_13"></a>Job 23:13

But he is in one mind, and who can [shuwb](../../strongs/h/h7725.md) him? and what his [nephesh](../../strongs/h/h5315.md) ['āvâ](../../strongs/h/h183.md), even that he ['asah](../../strongs/h/h6213.md).

<a name="job_23_14"></a>Job 23:14

For he [shalam](../../strongs/h/h7999.md) the thing that is [choq](../../strongs/h/h2706.md) for me: and [rab](../../strongs/h/h7227.md) such things are with him.

<a name="job_23_15"></a>Job 23:15

Therefore am I [bahal](../../strongs/h/h926.md) at his [paniym](../../strongs/h/h6440.md): when I [bîn](../../strongs/h/h995.md), I am [pachad](../../strongs/h/h6342.md) of him.

<a name="job_23_16"></a>Job 23:16

For ['el](../../strongs/h/h410.md) maketh my [leb](../../strongs/h/h3820.md) [rāḵaḵ](../../strongs/h/h7401.md), and the [Šaday](../../strongs/h/h7706.md) [bahal](../../strongs/h/h926.md) me:

<a name="job_23_17"></a>Job 23:17

Because I was not [ṣāmaṯ](../../strongs/h/h6789.md) [paniym](../../strongs/h/h6440.md) the [choshek](../../strongs/h/h2822.md), neither hath he [kāsâ](../../strongs/h/h3680.md) the ['ōp̄el](../../strongs/h/h652.md) from my [paniym](../../strongs/h/h6440.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 22](job_22.md) - [Job 24](job_24.md)