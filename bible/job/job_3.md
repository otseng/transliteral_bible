# [Job 3](https://www.blueletterbible.org/kjv/job/3)

<a name="job_3_1"></a>Job 3:1

['aḥar](../../strongs/h/h310.md) this [pāṯaḥ](../../strongs/h/h6605.md) ['Îyôḇ](../../strongs/h/h347.md) his [peh](../../strongs/h/h6310.md), and [qālal](../../strongs/h/h7043.md) his [yowm](../../strongs/h/h3117.md).

<a name="job_3_2"></a>Job 3:2

And ['Îyôḇ](../../strongs/h/h347.md) ['anah](../../strongs/h/h6030.md), and ['āmar](../../strongs/h/h559.md),

<a name="job_3_3"></a>Job 3:3

Let the [yowm](../../strongs/h/h3117.md) ['abad](../../strongs/h/h6.md) wherein I was [yalad](../../strongs/h/h3205.md), and the [layil](../../strongs/h/h3915.md) in which it was ['āmar](../../strongs/h/h559.md), There is a [geḇer](../../strongs/h/h1397.md) [harah](../../strongs/h/h2029.md).

<a name="job_3_4"></a>Job 3:4

Let that [yowm](../../strongs/h/h3117.md) be [choshek](../../strongs/h/h2822.md); let not ['ĕlvôha](../../strongs/h/h433.md) [darash](../../strongs/h/h1875.md) it from [maʿal](../../strongs/h/h4605.md), neither let the [nᵊhārâ](../../strongs/h/h5105.md) [yāp̄aʿ](../../strongs/h/h3313.md) upon it.

<a name="job_3_5"></a>Job 3:5

Let [choshek](../../strongs/h/h2822.md) and the [ṣalmāveṯ](../../strongs/h/h6757.md) [gā'al](../../strongs/h/h1350.md) it; let a [ʿănānâ](../../strongs/h/h6053.md) [shakan](../../strongs/h/h7931.md) upon it; let the [kimrîr](../../strongs/h/h3650.md) of the [yowm](../../strongs/h/h3117.md) [ba'ath](../../strongs/h/h1204.md) it.

<a name="job_3_6"></a>Job 3:6

As for that [layil](../../strongs/h/h3915.md), let ['ōp̄el](../../strongs/h/h652.md) [laqach](../../strongs/h/h3947.md) upon it; let it not be [ḥāḏâ](../../strongs/h/h2302.md) unto the [yowm](../../strongs/h/h3117.md) of the [šānâ](../../strongs/h/h8141.md), let it not [bow'](../../strongs/h/h935.md) into the [mispār](../../strongs/h/h4557.md) of the [yeraḥ](../../strongs/h/h3391.md).

<a name="job_3_7"></a>Job 3:7

Lo, let that [layil](../../strongs/h/h3915.md) be [galmûḏ](../../strongs/h/h1565.md), let no [rᵊnānâ](../../strongs/h/h7445.md) [bow'](../../strongs/h/h935.md) therein.

<a name="job_3_8"></a>Job 3:8

Let them [nāqaḇ](../../strongs/h/h5344.md) it that ['arar](../../strongs/h/h779.md) the [yowm](../../strongs/h/h3117.md), who are [ʿāṯîḏ](../../strongs/h/h6264.md) to [ʿûr](../../strongs/h/h5782.md) their [livyāṯān](../../strongs/h/h3882.md).

<a name="job_3_9"></a>Job 3:9

Let the [kowkab](../../strongs/h/h3556.md) of the [nešep̄](../../strongs/h/h5399.md) thereof be [ḥāšaḵ](../../strongs/h/h2821.md); let it [qāvâ](../../strongs/h/h6960.md) for ['owr](../../strongs/h/h216.md), but have none; neither let it [ra'ah](../../strongs/h/h7200.md) the ['aph'aph](../../strongs/h/h6079.md) of the [šaḥar](../../strongs/h/h7837.md):

<a name="job_3_10"></a>Job 3:10

Because it [cagar](../../strongs/h/h5462.md) not the [deleṯ](../../strongs/h/h1817.md) of my mother's [beten](../../strongs/h/h990.md), nor [cathar](../../strongs/h/h5641.md) ['amal](../../strongs/h/h5999.md) from mine ['ayin](../../strongs/h/h5869.md).

<a name="job_3_11"></a>Job 3:11

Why [muwth](../../strongs/h/h4191.md) I not from the [reḥem](../../strongs/h/h7358.md)? why did I not give up the [gāvaʿ](../../strongs/h/h1478.md) when I [yāṣā'](../../strongs/h/h3318.md) of the [beten](../../strongs/h/h990.md)?

<a name="job_3_12"></a>Job 3:12

Why did the [bereḵ](../../strongs/h/h1290.md) [qadam](../../strongs/h/h6923.md) me? or why the [šaḏ](../../strongs/h/h7699.md) that I should [yānaq](../../strongs/h/h3243.md)?

<a name="job_3_13"></a>Job 3:13

For now should I have [shakab](../../strongs/h/h7901.md) and been [šāqaṭ](../../strongs/h/h8252.md), I should have [yashen](../../strongs/h/h3462.md): then had I been at [nuwach](../../strongs/h/h5117.md),

<a name="job_3_14"></a>Job 3:14

With [melek](../../strongs/h/h4428.md) and [ya'ats](../../strongs/h/h3289.md) of the ['erets](../../strongs/h/h776.md), which [bānâ](../../strongs/h/h1129.md) [chorbah](../../strongs/h/h2723.md) for themselves;

<a name="job_3_15"></a>Job 3:15

Or with [śar](../../strongs/h/h8269.md) that had [zāhāḇ](../../strongs/h/h2091.md), who [mālā'](../../strongs/h/h4390.md) their [bayith](../../strongs/h/h1004.md) with [keceph](../../strongs/h/h3701.md):

<a name="job_3_16"></a>Job 3:16

Or as a [taman](../../strongs/h/h2934.md) untimely [nep̄el](../../strongs/h/h5309.md) I had not been; as ['owlel](../../strongs/h/h5768.md) which never [ra'ah](../../strongs/h/h7200.md) ['owr](../../strongs/h/h216.md).

<a name="job_3_17"></a>Job 3:17

There the [rasha'](../../strongs/h/h7563.md) [ḥāḏal](../../strongs/h/h2308.md) from [rōḡez](../../strongs/h/h7267.md); and there the [yāḡîaʿ](../../strongs/h/h3019.md) [koach](../../strongs/h/h3581.md) be at [nuwach](../../strongs/h/h5117.md).

<a name="job_3_18"></a>Job 3:18

There the ['āsîr](../../strongs/h/h615.md) [šā'an](../../strongs/h/h7599.md) [yaḥaḏ](../../strongs/h/h3162.md); they [shama'](../../strongs/h/h8085.md) not the [qowl](../../strongs/h/h6963.md) of the [nāḡaś](../../strongs/h/h5065.md).

<a name="job_3_19"></a>Job 3:19

The [qāṭān](../../strongs/h/h6996.md) and [gadowl](../../strongs/h/h1419.md) are there; and the ['ebed](../../strongs/h/h5650.md) is [ḥāp̄šî](../../strongs/h/h2670.md) from his ['adown](../../strongs/h/h113.md).

<a name="job_3_20"></a>Job 3:20

Wherefore is ['owr](../../strongs/h/h216.md) [nathan](../../strongs/h/h5414.md) to him that is in [ʿāmēl](../../strongs/h/h6001.md), and [chay](../../strongs/h/h2416.md) unto the [mar](../../strongs/h/h4751.md) in [nephesh](../../strongs/h/h5315.md);

<a name="job_3_21"></a>Job 3:21

Which [ḥāḵâ](../../strongs/h/h2442.md) for [maveth](../../strongs/h/h4194.md), but it cometh not; and [chaphar](../../strongs/h/h2658.md) for it more than for hid [maṭmôn](../../strongs/h/h4301.md);

<a name="job_3_22"></a>Job 3:22

Which [śāmēaḥ](../../strongs/h/h8056.md) [gîl](../../strongs/h/h1524.md), and are [śûś](../../strongs/h/h7797.md), when they can [māṣā'](../../strongs/h/h4672.md) the [qeber](../../strongs/h/h6913.md)?

<a name="job_3_23"></a>Job 3:23

Why is light given to a [geḇer](../../strongs/h/h1397.md) whose [derek](../../strongs/h/h1870.md) is [cathar](../../strongs/h/h5641.md), and whom ['ĕlvôha](../../strongs/h/h433.md) hath hedged [cakak](../../strongs/h/h5526.md)?

<a name="job_3_24"></a>Job 3:24

For my ['anachah](../../strongs/h/h585.md) [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) I [lechem](../../strongs/h/h3899.md), and my [šᵊ'āḡâ](../../strongs/h/h7581.md) are [nāṯaḵ](../../strongs/h/h5413.md) like the [mayim](../../strongs/h/h4325.md).

<a name="job_3_25"></a>Job 3:25

For the thing which I [paḥaḏ](../../strongs/h/h6343.md) [pachad](../../strongs/h/h6342.md) is ['āṯâ](../../strongs/h/h857.md) upon me, and that which I was [yāḡōr](../../strongs/h/h3025.md) of is [bow'](../../strongs/h/h935.md) unto me.

<a name="job_3_26"></a>Job 3:26

I was not in [šālâ](../../strongs/h/h7951.md), neither had I [šāqaṭ](../../strongs/h/h8252.md), neither was I [nuwach](../../strongs/h/h5117.md); yet [rōḡez](../../strongs/h/h7267.md) [bow'](../../strongs/h/h935.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 2](job_2.md) - [Job 4](job_4.md)