# [Job 22](https://www.blueletterbible.org/kjv/job/22)

<a name="job_22_1"></a>Job 22:1

Then ['Ĕlîp̄az](../../strongs/h/h464.md) the [têmānî](../../strongs/h/h8489.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md),

<a name="job_22_2"></a>Job 22:2

Can a [geḇer](../../strongs/h/h1397.md) be [sāḵan](../../strongs/h/h5532.md) unto ['el](../../strongs/h/h410.md), as he that is [sakal](../../strongs/h/h7919.md) may be [sāḵan](../../strongs/h/h5532.md) unto himself?

<a name="job_22_3"></a>Job 22:3

Is it any [chephets](../../strongs/h/h2656.md) to the [Šaday](../../strongs/h/h7706.md), that thou art [ṣāḏaq](../../strongs/h/h6663.md)? or is it [beṣaʿ](../../strongs/h/h1215.md) to him, that thou makest thy [derek](../../strongs/h/h1870.md) [tamam](../../strongs/h/h8552.md)?

<a name="job_22_4"></a>Job 22:4

Will he [yakach](../../strongs/h/h3198.md) thee for [yir'ah](../../strongs/h/h3374.md) of thee? will he [bow'](../../strongs/h/h935.md) with thee into [mishpat](../../strongs/h/h4941.md)?

<a name="job_22_5"></a>Job 22:5

Is not thy [ra'](../../strongs/h/h7451.md) [rab](../../strongs/h/h7227.md)? and thine ['avon](../../strongs/h/h5771.md) [qēṣ](../../strongs/h/h7093.md)?

<a name="job_22_6"></a>Job 22:6

For thou hast taken a [chabal](../../strongs/h/h2254.md) from thy ['ach](../../strongs/h/h251.md) for [ḥinnām](../../strongs/h/h2600.md), and [pāšaṭ](../../strongs/h/h6584.md) the ['arowm](../../strongs/h/h6174.md) of their [beḡeḏ](../../strongs/h/h899.md).

<a name="job_22_7"></a>Job 22:7

Thou hast not given [mayim](../../strongs/h/h4325.md) to the [ʿāyēp̄](../../strongs/h/h5889.md) to [šāqâ](../../strongs/h/h8248.md), and thou hast [mānaʿ](../../strongs/h/h4513.md) [lechem](../../strongs/h/h3899.md) from the [rāʿēḇ](../../strongs/h/h7457.md).

<a name="job_22_8"></a>Job 22:8

But as for the [zerowa'](../../strongs/h/h2220.md) ['iysh](../../strongs/h/h376.md), he had the ['erets](../../strongs/h/h776.md); and the [nasa'](../../strongs/h/h5375.md) [paniym](../../strongs/h/h6440.md) [yashab](../../strongs/h/h3427.md) in it.

<a name="job_22_9"></a>Job 22:9

Thou hast [shalach](../../strongs/h/h7971.md) ['almānâ](../../strongs/h/h490.md) away [rêqām](../../strongs/h/h7387.md), and the [zerowa'](../../strongs/h/h2220.md) of the [yathowm](../../strongs/h/h3490.md) have been [dāḵā'](../../strongs/h/h1792.md).

<a name="job_22_10"></a>Job 22:10

Therefore [paḥ](../../strongs/h/h6341.md) are [cabiyb](../../strongs/h/h5439.md) thee, and [piṯ'ōm](../../strongs/h/h6597.md) [paḥaḏ](../../strongs/h/h6343.md) [bahal](../../strongs/h/h926.md) thee;

<a name="job_22_11"></a>Job 22:11

Or [choshek](../../strongs/h/h2822.md), that thou canst not [ra'ah](../../strongs/h/h7200.md); and [šip̄ʿâ](../../strongs/h/h8229.md) of [mayim](../../strongs/h/h4325.md) [kāsâ](../../strongs/h/h3680.md) thee.

<a name="job_22_12"></a>Job 22:12

Is not ['ĕlvôha](../../strongs/h/h433.md) in the [gobahh](../../strongs/h/h1363.md) of [shamayim](../../strongs/h/h8064.md)? and [ra'ah](../../strongs/h/h7200.md) the [ro'sh](../../strongs/h/h7218.md) of the [kowkab](../../strongs/h/h3556.md), how [ruwm](../../strongs/h/h7311.md) they are!

<a name="job_22_13"></a>Job 22:13

And thou ['āmar](../../strongs/h/h559.md), How doth ['el](../../strongs/h/h410.md) [yada'](../../strongs/h/h3045.md)? can he [shaphat](../../strongs/h/h8199.md) through the ['araphel](../../strongs/h/h6205.md)?

<a name="job_22_14"></a>Job 22:14

['ab](../../strongs/h/h5645.md) are a [cether](../../strongs/h/h5643.md) to him, that he [ra'ah](../../strongs/h/h7200.md) not; and he [halak](../../strongs/h/h1980.md) in the [ḥûḡ](../../strongs/h/h2329.md) of [shamayim](../../strongs/h/h8064.md).

<a name="job_22_15"></a>Job 22:15

Hast thou [shamar](../../strongs/h/h8104.md) the ['owlam](../../strongs/h/h5769.md) ['orach](../../strongs/h/h734.md) which ['aven](../../strongs/h/h205.md) [math](../../strongs/h/h4962.md) have [dāraḵ](../../strongs/h/h1869.md)?

<a name="job_22_16"></a>Job 22:16

Which were [qāmaṭ](../../strongs/h/h7059.md) out of [ʿēṯ](../../strongs/h/h6256.md), whose [yᵊsôḏ](../../strongs/h/h3247.md) was [yāṣaq](../../strongs/h/h3332.md) with a [nāhār](../../strongs/h/h5104.md):

<a name="job_22_17"></a>Job 22:17

Which ['āmar](../../strongs/h/h559.md) unto ['el](../../strongs/h/h410.md), [cuwr](../../strongs/h/h5493.md) from us: and what can the [Šaday](../../strongs/h/h7706.md) [pa'al](../../strongs/h/h6466.md) for them?

<a name="job_22_18"></a>Job 22:18

Yet he [mālā'](../../strongs/h/h4390.md) their [bayith](../../strongs/h/h1004.md) with [towb](../../strongs/h/h2896.md) things: but the ['etsah](../../strongs/h/h6098.md) of the [rasha'](../../strongs/h/h7563.md) is [rachaq](../../strongs/h/h7368.md) from me.

<a name="job_22_19"></a>Job 22:19

The [tsaddiyq](../../strongs/h/h6662.md) [ra'ah](../../strongs/h/h7200.md) it, and are [samach](../../strongs/h/h8055.md): and the [naqiy](../../strongs/h/h5355.md) [lāʿaḡ](../../strongs/h/h3932.md) them.

<a name="job_22_20"></a>Job 22:20

Whereas our [qîm](../../strongs/h/h7009.md) is not [kāḥaḏ](../../strongs/h/h3582.md), but the [yeṯer](../../strongs/h/h3499.md) of them the ['esh](../../strongs/h/h784.md) ['akal](../../strongs/h/h398.md).

<a name="job_22_21"></a>Job 22:21

[sāḵan](../../strongs/h/h5532.md) now thyself with him, and be at [shalam](../../strongs/h/h7999.md): thereby [towb](../../strongs/h/h2896.md) shall [bow'](../../strongs/h/h935.md) unto thee.

<a name="job_22_22"></a>Job 22:22

[laqach](../../strongs/h/h3947.md), I pray thee, the [towrah](../../strongs/h/h8451.md) from his [peh](../../strongs/h/h6310.md), and [śûm](../../strongs/h/h7760.md) his ['emer](../../strongs/h/h561.md) in thine [lebab](../../strongs/h/h3824.md).

<a name="job_22_23"></a>Job 22:23

If thou [shuwb](../../strongs/h/h7725.md) to the [Šaday](../../strongs/h/h7706.md), thou shalt be built [bānâ](../../strongs/h/h1129.md), thou shalt put [rachaq](../../strongs/h/h7368.md) ['evel](../../strongs/h/h5766.md) [rachaq](../../strongs/h/h7368.md) from thy ['ohel](../../strongs/h/h168.md).

<a name="job_22_24"></a>Job 22:24

Then shalt thou [shiyth](../../strongs/h/h7896.md) [beṣer](../../strongs/h/h1220.md) as ['aphar](../../strongs/h/h6083.md), and the ['Ôp̄îr](../../strongs/h/h211.md) as the [tsuwr](../../strongs/h/h6697.md) of the [nachal](../../strongs/h/h5158.md).

<a name="job_22_25"></a>Job 22:25

Yea, the [Šaday](../../strongs/h/h7706.md) shall be thy [beṣer](../../strongs/h/h1220.md), and thou shalt have [tôʿāp̄ôṯ](../../strongs/h/h8443.md) of [keceph](../../strongs/h/h3701.md).

<a name="job_22_26"></a>Job 22:26

For then shalt thou have thy [ʿānaḡ](../../strongs/h/h6026.md) in the [Šaday](../../strongs/h/h7706.md), and shalt [nasa'](../../strongs/h/h5375.md) thy [paniym](../../strongs/h/h6440.md) unto ['ĕlvôha](../../strongs/h/h433.md).

<a name="job_22_27"></a>Job 22:27

Thou shalt make thy [ʿāṯar](../../strongs/h/h6279.md) unto him, and he shall [shama'](../../strongs/h/h8085.md) thee, and thou shalt [shalam](../../strongs/h/h7999.md) thy [neḏer](../../strongs/h/h5088.md).

<a name="job_22_28"></a>Job 22:28

Thou shalt also [gāzar](../../strongs/h/h1504.md) a ['omer](../../strongs/h/h562.md), and it shall be [quwm](../../strongs/h/h6965.md) unto thee: and the ['owr](../../strongs/h/h216.md) shall [nāḡahh](../../strongs/h/h5050.md) upon thy [derek](../../strongs/h/h1870.md).

<a name="job_22_29"></a>Job 22:29

When men are [šāp̄ēl](../../strongs/h/h8213.md), then thou shalt ['āmar](../../strongs/h/h559.md), There is [gēvâ](../../strongs/h/h1466.md); and he shall [yasha'](../../strongs/h/h3467.md) the [šaḥ](../../strongs/h/h7807.md) ['ayin](../../strongs/h/h5869.md).

<a name="job_22_30"></a>Job 22:30

He shall [mālaṭ](../../strongs/h/h4422.md) the ['î](../../strongs/h/h336.md) of the [naqiy](../../strongs/h/h5355.md): and it is [mālaṭ](../../strongs/h/h4422.md) by the [bōr](../../strongs/h/h1252.md) of thine [kaph](../../strongs/h/h3709.md).

---

[Transliteral Bible](../bible.md)

[Job](job.md)

[Job 21](job_21.md) - [Job 23](job_23.md)