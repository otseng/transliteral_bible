# [Obadiah 1](https://www.blueletterbible.org/kjv/obadiah/1)

<a name="obadiah_1_1"></a>Obadiah 1:1

The [ḥāzôn](../../strongs/h/h2377.md) of [ʿŌḇaḏyâ](../../strongs/h/h5662.md). Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) concerning ['Ĕḏōm](../../strongs/h/h123.md); We have [shama'](../../strongs/h/h8085.md) a [šᵊmûʿâ](../../strongs/h/h8052.md) from [Yĕhovah](../../strongs/h/h3068.md), and an [ṣîr](../../strongs/h/h6735.md) is [shalach](../../strongs/h/h7971.md) among the [gowy](../../strongs/h/h1471.md), [quwm](../../strongs/h/h6965.md) ye, and let us [quwm](../../strongs/h/h6965.md) against her in [milḥāmâ](../../strongs/h/h4421.md).

<a name="obadiah_1_2"></a>Obadiah 1:2

Behold, I have [nathan](../../strongs/h/h5414.md) thee [qāṭān](../../strongs/h/h6996.md) among the [gowy](../../strongs/h/h1471.md): thou art [me'od](../../strongs/h/h3966.md) [bazah](../../strongs/h/h959.md).

<a name="obadiah_1_3"></a>Obadiah 1:3

The [zāḏôn](../../strongs/h/h2087.md) of thine [leb](../../strongs/h/h3820.md) hath [nasha'](../../strongs/h/h5377.md) thee, thou that [shakan](../../strongs/h/h7931.md) in the [ḥăḡāv](../../strongs/h/h2288.md) of the [cela'](../../strongs/h/h5553.md), whose [yashab](../../strongs/h/h3427.md) is [marowm](../../strongs/h/h4791.md); that ['āmar](../../strongs/h/h559.md) in his [leb](../../strongs/h/h3820.md), Who shall [yarad](../../strongs/h/h3381.md) me to the ['erets](../../strongs/h/h776.md)?

<a name="obadiah_1_4"></a>Obadiah 1:4

Though thou [gāḇah](../../strongs/h/h1361.md) thyself as the [nesheׁr](../../strongs/h/h5404.md), and though thou [śûm](../../strongs/h/h7760.md) thy [qēn](../../strongs/h/h7064.md) among the [kowkab](../../strongs/h/h3556.md), thence will I [yarad](../../strongs/h/h3381.md) thee, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="obadiah_1_5"></a>Obadiah 1:5

If [gannāḇ](../../strongs/h/h1590.md) [bow'](../../strongs/h/h935.md) to thee, if [shadad](../../strongs/h/h7703.md) by [layil](../../strongs/h/h3915.md), (how art thou [damah](../../strongs/h/h1820.md)!) would they not have [ganab](../../strongs/h/h1589.md) till they had [day](../../strongs/h/h1767.md)? if the [bāṣar](../../strongs/h/h1219.md) [bow'](../../strongs/h/h935.md) to thee, would they not [šā'ar](../../strongs/h/h7604.md) [ʿōlēlôṯ](../../strongs/h/h5955.md)?

<a name="obadiah_1_6"></a>Obadiah 1:6

How are the things of [ʿĒśāv](../../strongs/h/h6215.md) [ḥāp̄aś](../../strongs/h/h2664.md)! how are his [maṣpunîm](../../strongs/h/h4710.md) [bāʿâ](../../strongs/h/h1158.md)!

<a name="obadiah_1_7"></a>Obadiah 1:7

All the ['enowsh](../../strongs/h/h582.md) of thy [bĕriyth](../../strongs/h/h1285.md) have [shalach](../../strongs/h/h7971.md) thee even to the [gᵊḇûl](../../strongs/h/h1366.md): the ['enowsh](../../strongs/h/h582.md) that were at [shalowm](../../strongs/h/h7965.md) with thee have [nasha'](../../strongs/h/h5377.md) thee, and [yakol](../../strongs/h/h3201.md) against thee; thy [lechem](../../strongs/h/h3899.md) have [śûm](../../strongs/h/h7760.md) a [māzôr](../../strongs/h/h4204.md) under thee: there is none [tāḇûn](../../strongs/h/h8394.md) in him.

<a name="obadiah_1_8"></a>Obadiah 1:8

Shall I not in that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), even ['abad](../../strongs/h/h6.md) the [ḥāḵām](../../strongs/h/h2450.md) men out of ['Ĕḏōm](../../strongs/h/h123.md), and [tāḇûn](../../strongs/h/h8394.md) out of the [har](../../strongs/h/h2022.md) of [ʿĒśāv](../../strongs/h/h6215.md)?

<a name="obadiah_1_9"></a>Obadiah 1:9

And thy [gibôr](../../strongs/h/h1368.md) men, O [Têmān](../../strongs/h/h8487.md), shall be [ḥāṯaṯ](../../strongs/h/h2865.md), to the end that every ['iysh](../../strongs/h/h376.md) of the [har](../../strongs/h/h2022.md) of [ʿĒśāv](../../strongs/h/h6215.md) may be [karath](../../strongs/h/h3772.md) by [qeṭel](../../strongs/h/h6993.md) .

<a name="obadiah_1_10"></a>Obadiah 1:10

For thy [chamac](../../strongs/h/h2555.md) against thy ['ach](../../strongs/h/h251.md) [Ya'aqob](../../strongs/h/h3290.md) [bûšâ](../../strongs/h/h955.md) shall [kāsâ](../../strongs/h/h3680.md) thee, and thou shalt be [karath](../../strongs/h/h3772.md) ['owlam](../../strongs/h/h5769.md).

<a name="obadiah_1_11"></a>Obadiah 1:11

In the [yowm](../../strongs/h/h3117.md) that thou ['amad](../../strongs/h/h5975.md) on the other side, in the [yowm](../../strongs/h/h3117.md) that the [zûr](../../strongs/h/h2114.md) [šāḇâ](../../strongs/h/h7617.md) his [ḥayil](../../strongs/h/h2428.md), and [nāḵrî](../../strongs/h/h5237.md) [bow'](../../strongs/h/h935.md) into his [sha'ar](../../strongs/h/h8179.md), and [yāḏaḏ](../../strongs/h/h3032.md) [gôrāl](../../strongs/h/h1486.md) upon [Yĕruwshalaim](../../strongs/h/h3389.md), even thou wast as one of them.

<a name="obadiah_1_12"></a>Obadiah 1:12

But thou shouldest not have [ra'ah](../../strongs/h/h7200.md) on the [yowm](../../strongs/h/h3117.md) of thy ['ach](../../strongs/h/h251.md) in the [yowm](../../strongs/h/h3117.md) that he became a [neḵer](../../strongs/h/h5235.md); neither shouldest thou have [samach](../../strongs/h/h8055.md) over the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) in the [yowm](../../strongs/h/h3117.md) of their ['abad](../../strongs/h/h6.md); neither shouldest thou have [peh](../../strongs/h/h6310.md) [gāḏal](../../strongs/h/h1431.md) in the [yowm](../../strongs/h/h3117.md) of [tsarah](../../strongs/h/h6869.md).

<a name="obadiah_1_13"></a>Obadiah 1:13

Thou shouldest not have [bow'](../../strongs/h/h935.md) into the [sha'ar](../../strongs/h/h8179.md) of my ['am](../../strongs/h/h5971.md) in the [yowm](../../strongs/h/h3117.md) of their ['êḏ](../../strongs/h/h343.md); yea, thou shouldest not have [ra'ah](../../strongs/h/h7200.md) on their [ra'](../../strongs/h/h7451.md) in the [yowm](../../strongs/h/h3117.md) of their ['êḏ](../../strongs/h/h343.md), nor have [shalach](../../strongs/h/h7971.md) hands on their [ḥayil](../../strongs/h/h2428.md) in the [yowm](../../strongs/h/h3117.md) of their ['êḏ](../../strongs/h/h343.md);

<a name="obadiah_1_14"></a>Obadiah 1:14

Neither shouldest thou have ['amad](../../strongs/h/h5975.md) in the [pereq](../../strongs/h/h6563.md) , to [karath](../../strongs/h/h3772.md) those of his that did [pālîṭ](../../strongs/h/h6412.md); neither shouldest thou have [cagar](../../strongs/h/h5462.md) those of his that did [śārîḏ](../../strongs/h/h8300.md) in the [yowm](../../strongs/h/h3117.md) of [tsarah](../../strongs/h/h6869.md).

<a name="obadiah_1_15"></a>Obadiah 1:15

For the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) is [qarowb](../../strongs/h/h7138.md) upon all the [gowy](../../strongs/h/h1471.md): as thou hast ['asah](../../strongs/h/h6213.md), it shall be ['asah](../../strongs/h/h6213.md) unto thee: thy [gĕmwl](../../strongs/h/h1576.md) shall [shuwb](../../strongs/h/h7725.md) upon thine own [ro'sh](../../strongs/h/h7218.md).

<a name="obadiah_1_16"></a>Obadiah 1:16

For as ye have [šāṯâ](../../strongs/h/h8354.md) upon my [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md), so shall all the [gowy](../../strongs/h/h1471.md) [šāṯâ](../../strongs/h/h8354.md) [tāmîḏ](../../strongs/h/h8548.md), yea, they shall [šāṯâ](../../strongs/h/h8354.md), and they shall [lûaʿ](../../strongs/h/h3886.md), and they shall be as though they had not been.

<a name="obadiah_1_17"></a>Obadiah 1:17

But upon [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md) shall be [pᵊlêṭâ](../../strongs/h/h6413.md), and there shall be [qodesh](../../strongs/h/h6944.md); and the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md) shall [yarash](../../strongs/h/h3423.md) their [môrāš](../../strongs/h/h4180.md).

<a name="obadiah_1_18"></a>Obadiah 1:18

And the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md) shall be an ['esh](../../strongs/h/h784.md), and the [bayith](../../strongs/h/h1004.md) of [Yôsēp̄](../../strongs/h/h3130.md) a [lehāḇâ](../../strongs/h/h3852.md), and the [bayith](../../strongs/h/h1004.md) of [ʿĒśāv](../../strongs/h/h6215.md) for [qaš](../../strongs/h/h7179.md), and they shall [dalaq](../../strongs/h/h1814.md) in them, and ['akal](../../strongs/h/h398.md) them; and there shall not be any [śārîḏ](../../strongs/h/h8300.md) of the [bayith](../../strongs/h/h1004.md) of [ʿĒśāv](../../strongs/h/h6215.md); for [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) it.

<a name="obadiah_1_19"></a>Obadiah 1:19

And they of the [neḡeḇ](../../strongs/h/h5045.md) shall [yarash](../../strongs/h/h3423.md) the [har](../../strongs/h/h2022.md) of [ʿĒśāv](../../strongs/h/h6215.md); and they of the [šᵊp̄ēlâ](../../strongs/h/h8219.md) the [Pᵊlištî](../../strongs/h/h6430.md): and they shall [yarash](../../strongs/h/h3423.md) the [sadeh](../../strongs/h/h7704.md) of ['Ep̄rayim](../../strongs/h/h669.md), and the [sadeh](../../strongs/h/h7704.md) of [Šōmrôn](../../strongs/h/h8111.md): and [Binyāmîn](../../strongs/h/h1144.md) [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="obadiah_1_20"></a>Obadiah 1:20

And the [gālûṯ](../../strongs/h/h1546.md) of this [cheyl](../../strongs/h/h2426.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) that of the [Kᵊnaʿănî](../../strongs/h/h3669.md), even unto [Ṣārp̄Aṯ](../../strongs/h/h6886.md); and the [gālûṯ](../../strongs/h/h1546.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), which is in [Sᵊp̄Āraḏ](../../strongs/h/h5614.md) , shall [yarash](../../strongs/h/h3423.md) the [ʿîr](../../strongs/h/h5892.md) of the [neḡeḇ](../../strongs/h/h5045.md).

<a name="obadiah_1_21"></a>Obadiah 1:21

And [yasha'](../../strongs/h/h3467.md) shall [ʿālâ](../../strongs/h/h5927.md) on [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md) to [shaphat](../../strongs/h/h8199.md) the [har](../../strongs/h/h2022.md) of [ʿĒśāv](../../strongs/h/h6215.md); and the [mᵊlûḵâ](../../strongs/h/h4410.md) shall be [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Obadiah](obadiah.md)
