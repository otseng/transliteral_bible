# [Ruth 4](https://www.blueletterbible.org/kjv/ruth/4)

<a name="ruth_4_1"></a>Ruth 4:1

Then [BōʿAz](../../strongs/h/h1162.md) [ʿālâ](../../strongs/h/h5927.md) to the [sha'ar](../../strongs/h/h8179.md), and [yashab](../../strongs/h/h3427.md) him there: and, behold, the [gā'al](../../strongs/h/h1350.md) of whom [BōʿAz](../../strongs/h/h1162.md) [dabar](../../strongs/h/h1696.md) ['abar](../../strongs/h/h5674.md) by; unto whom he ['āmar](../../strongs/h/h559.md), [hôy](../../strongs/h/h1945.md), [palōnî](../../strongs/h/h6423.md) a ['almōnî](../../strongs/h/h492.md)! [cuwr](../../strongs/h/h5493.md), [yashab](../../strongs/h/h3427.md) here. And he [cuwr](../../strongs/h/h5493.md), and [yashab](../../strongs/h/h3427.md).

<a name="ruth_4_2"></a>Ruth 4:2

And he [laqach](../../strongs/h/h3947.md) [ʿeśer](../../strongs/h/h6235.md) ['enowsh](../../strongs/h/h582.md) of the [zāqēn](../../strongs/h/h2205.md) of the [ʿîr](../../strongs/h/h5892.md), and ['āmar](../../strongs/h/h559.md), [yashab](../../strongs/h/h3427.md) ye here. And they [yashab](../../strongs/h/h3427.md).

<a name="ruth_4_3"></a>Ruth 4:3

And he ['āmar](../../strongs/h/h559.md) unto the [gā'al](../../strongs/h/h1350.md), [NāʿŎmî](../../strongs/h/h5281.md), that is [shuwb](../../strongs/h/h7725.md) out of the [sadeh](../../strongs/h/h7704.md) of [Mô'āḇ](../../strongs/h/h4124.md), [māḵar](../../strongs/h/h4376.md) a [ḥelqâ](../../strongs/h/h2513.md) of [sadeh](../../strongs/h/h7704.md), which was our ['ach](../../strongs/h/h251.md) ['Ĕlîmeleḵ](../../strongs/h/h458.md):

<a name="ruth_4_4"></a>Ruth 4:4

And I ['āmar](../../strongs/h/h559.md) to [gālâ](../../strongs/h/h1540.md) ['ozen](../../strongs/h/h241.md) thee, ['āmar](../../strongs/h/h559.md), [qānâ](../../strongs/h/h7069.md) it before the [yashab](../../strongs/h/h3427.md), and before the [zāqēn](../../strongs/h/h2205.md) of my ['am](../../strongs/h/h5971.md). If thou wilt [gā'al](../../strongs/h/h1350.md) it, [gā'al](../../strongs/h/h1350.md) it: but if thou wilt not [gā'al](../../strongs/h/h1350.md) it, then [nāḡaḏ](../../strongs/h/h5046.md) me, that I may [yada'](../../strongs/h/h3045.md): for there is none to [gā'al](../../strongs/h/h1350.md) it [zûlâ](../../strongs/h/h2108.md) thee; and I am ['aḥar](../../strongs/h/h310.md) thee. And he ['āmar](../../strongs/h/h559.md), I will [gā'al](../../strongs/h/h1350.md) it.

<a name="ruth_4_5"></a>Ruth 4:5

Then ['āmar](../../strongs/h/h559.md) [BōʿAz](../../strongs/h/h1162.md), What [yowm](../../strongs/h/h3117.md) thou [qānâ](../../strongs/h/h7069.md) the [sadeh](../../strongs/h/h7704.md) of the [yad](../../strongs/h/h3027.md) of [NāʿŎmî](../../strongs/h/h5281.md), thou must [qānâ](../../strongs/h/h7069.md) it also of [Rûṯ](../../strongs/h/h7327.md) the [Mô'āḇî](../../strongs/h/h4125.md), the ['ishshah](../../strongs/h/h802.md) of the [muwth](../../strongs/h/h4191.md), to [quwm](../../strongs/h/h6965.md) the [shem](../../strongs/h/h8034.md) of the [muwth](../../strongs/h/h4191.md) upon his [nachalah](../../strongs/h/h5159.md).

<a name="ruth_4_6"></a>Ruth 4:6

And the [gā'al](../../strongs/h/h1350.md) ['āmar](../../strongs/h/h559.md), I [yakol](../../strongs/h/h3201.md) [gā'al](../../strongs/h/h1350.md) it for myself, lest I [shachath](../../strongs/h/h7843.md) mine own [nachalah](../../strongs/h/h5159.md): [gā'al](../../strongs/h/h1350.md) thou my [gᵊ'ullâ](../../strongs/h/h1353.md) to thyself; for I [yakol](../../strongs/h/h3201.md) [gā'al](../../strongs/h/h1350.md) it.

<a name="ruth_4_7"></a>Ruth 4:7

Now this was the manner in former [paniym](../../strongs/h/h6440.md) in [Yisra'el](../../strongs/h/h3478.md) concerning [gᵊ'ullâ](../../strongs/h/h1353.md) and concerning [tᵊmûrâ](../../strongs/h/h8545.md), for to [quwm](../../strongs/h/h6965.md) all [dabar](../../strongs/h/h1697.md); an ['iysh](../../strongs/h/h376.md) plucked [šālap̄](../../strongs/h/h8025.md) his [naʿal](../../strongs/h/h5275.md), and [nathan](../../strongs/h/h5414.md) it to his [rea'](../../strongs/h/h7453.md): and this was a [tᵊʿûḏâ](../../strongs/h/h8584.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="ruth_4_8"></a>Ruth 4:8

Therefore the [gā'al](../../strongs/h/h1350.md) ['āmar](../../strongs/h/h559.md) unto [BōʿAz](../../strongs/h/h1162.md), [qānâ](../../strongs/h/h7069.md) it for thee. So he drew [šālap̄](../../strongs/h/h8025.md) his [naʿal](../../strongs/h/h5275.md).

<a name="ruth_4_9"></a>Ruth 4:9

And [BōʿAz](../../strongs/h/h1162.md) ['āmar](../../strongs/h/h559.md) unto the [zāqēn](../../strongs/h/h2205.md), and unto all the ['am](../../strongs/h/h5971.md), Ye are ['ed](../../strongs/h/h5707.md) this [yowm](../../strongs/h/h3117.md), that I have [qānâ](../../strongs/h/h7069.md) all that was ['Ĕlîmeleḵ](../../strongs/h/h458.md), and all that was [Kilyôn](../../strongs/h/h3630.md) and [Maḥlôn](../../strongs/h/h4248.md), of the [yad](../../strongs/h/h3027.md) of [NāʿŎmî](../../strongs/h/h5281.md).

<a name="ruth_4_10"></a>Ruth 4:10

Moreover [Rûṯ](../../strongs/h/h7327.md) the [Mô'āḇî](../../strongs/h/h4125.md), the ['ishshah](../../strongs/h/h802.md) of [Maḥlôn](../../strongs/h/h4248.md), have I [qānâ](../../strongs/h/h7069.md) to be my ['ishshah](../../strongs/h/h802.md), to [quwm](../../strongs/h/h6965.md) the [shem](../../strongs/h/h8034.md) of the [muwth](../../strongs/h/h4191.md) upon his [nachalah](../../strongs/h/h5159.md), that the [shem](../../strongs/h/h8034.md) of the [muwth](../../strongs/h/h4191.md) be not [karath](../../strongs/h/h3772.md) from [ʿim](../../strongs/h/h5973.md) his ['ach](../../strongs/h/h251.md), and from the [sha'ar](../../strongs/h/h8179.md) of his [maqowm](../../strongs/h/h4725.md): ye are ['ed](../../strongs/h/h5707.md) this [yowm](../../strongs/h/h3117.md).

<a name="ruth_4_11"></a>Ruth 4:11

And all the ['am](../../strongs/h/h5971.md) that were in the [sha'ar](../../strongs/h/h8179.md), and the [zāqēn](../../strongs/h/h2205.md), ['āmar](../../strongs/h/h559.md), We are ['ed](../../strongs/h/h5707.md). [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) the ['ishshah](../../strongs/h/h802.md) that is [bow'](../../strongs/h/h935.md) into thine [bayith](../../strongs/h/h1004.md) like [Rāḥēl](../../strongs/h/h7354.md) and like [Lē'â](../../strongs/h/h3812.md), which [šᵊnayim](../../strongs/h/h8147.md) did [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md): and ['asah](../../strongs/h/h6213.md) thou [ḥayil](../../strongs/h/h2428.md) in ['Ep̄rāṯ](../../strongs/h/h672.md), and be [qara'](../../strongs/h/h7121.md) [shem](../../strongs/h/h8034.md) in [Bêṯ leḥem](../../strongs/h/h1035.md):

<a name="ruth_4_12"></a>Ruth 4:12

And let thy [bayith](../../strongs/h/h1004.md) be like the [bayith](../../strongs/h/h1004.md) of [Pereṣ](../../strongs/h/h6557.md), whom [Tāmār](../../strongs/h/h8559.md) [yalad](../../strongs/h/h3205.md) unto [Yehuwdah](../../strongs/h/h3063.md), of the [zera'](../../strongs/h/h2233.md) which [Yĕhovah](../../strongs/h/h3068.md) shall [nathan](../../strongs/h/h5414.md) thee of this [naʿărâ](../../strongs/h/h5291.md).

<a name="ruth_4_13"></a>Ruth 4:13

So [BōʿAz](../../strongs/h/h1162.md) [laqach](../../strongs/h/h3947.md) [Rûṯ](../../strongs/h/h7327.md), and she was his ['ishshah](../../strongs/h/h802.md): and when he [bow'](../../strongs/h/h935.md) unto her, [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) her [herown](../../strongs/h/h2032.md), and she [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md).

<a name="ruth_4_14"></a>Ruth 4:14

And the ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md) unto [NāʿŎmî](../../strongs/h/h5281.md), [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md), which hath not [shabath](../../strongs/h/h7673.md) thee this [yowm](../../strongs/h/h3117.md) without a [gā'al](../../strongs/h/h1350.md), that his [shem](../../strongs/h/h8034.md) may be [qara'](../../strongs/h/h7121.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="ruth_4_15"></a>Ruth 4:15

And he shall be unto thee a [shuwb](../../strongs/h/h7725.md) of thy [nephesh](../../strongs/h/h5315.md), and a [kûl](../../strongs/h/h3557.md) of thine old [śêḇâ](../../strongs/h/h7872.md): for thy [kallâ](../../strongs/h/h3618.md), which ['ahab](../../strongs/h/h157.md) thee, which is [towb](../../strongs/h/h2896.md) to thee than [šeḇaʿ](../../strongs/h/h7651.md) [ben](../../strongs/h/h1121.md), hath [yalad](../../strongs/h/h3205.md) him.

<a name="ruth_4_16"></a>Ruth 4:16

And [NāʿŎmî](../../strongs/h/h5281.md) [laqach](../../strongs/h/h3947.md) the [yeleḏ](../../strongs/h/h3206.md), and [shiyth](../../strongs/h/h7896.md) it in her [ḥêq](../../strongs/h/h2436.md), and became ['aman](../../strongs/h/h539.md) unto it.

<a name="ruth_4_17"></a>Ruth 4:17

And the [šāḵēn](../../strongs/h/h7934.md) [qara'](../../strongs/h/h7121.md) it a [shem](../../strongs/h/h8034.md), ['āmar](../../strongs/h/h559.md), There is a [ben](../../strongs/h/h1121.md) [yalad](../../strongs/h/h3205.md) to [NāʿŎmî](../../strongs/h/h5281.md); and they [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [ʿÔḇēḏ](../../strongs/h/h5744.md): he is the ['ab](../../strongs/h/h1.md) of [Yišay](../../strongs/h/h3448.md), the ['ab](../../strongs/h/h1.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="ruth_4_18"></a>Ruth 4:18

Now these are the [towlĕdah](../../strongs/h/h8435.md) of [Pereṣ](../../strongs/h/h6557.md): [Pereṣ](../../strongs/h/h6557.md) [yalad](../../strongs/h/h3205.md) [Ḥeṣrôn](../../strongs/h/h2696.md),

<a name="ruth_4_19"></a>Ruth 4:19

And [Ḥeṣrôn](../../strongs/h/h2696.md) [yalad](../../strongs/h/h3205.md) [Rām](../../strongs/h/h7410.md), and [Rām](../../strongs/h/h7410.md) [yalad](../../strongs/h/h3205.md) [ʿAmmînāḏāḇ](../../strongs/h/h5992.md),

<a name="ruth_4_20"></a>Ruth 4:20

And [ʿAmmînāḏāḇ](../../strongs/h/h5992.md) [yalad](../../strongs/h/h3205.md) [Naḥšôn](../../strongs/h/h5177.md), and [Naḥšôn](../../strongs/h/h5177.md) [yalad](../../strongs/h/h3205.md) Salmon H8009,

<a name="ruth_4_21"></a>Ruth 4:21

And [Śalmôn](../../strongs/h/h8012.md) [yalad](../../strongs/h/h3205.md) [BōʿAz](../../strongs/h/h1162.md), and [BōʿAz](../../strongs/h/h1162.md) [yalad](../../strongs/h/h3205.md) [ʿÔḇēḏ](../../strongs/h/h5744.md),

<a name="ruth_4_22"></a>Ruth 4:22

And [ʿÔḇēḏ](../../strongs/h/h5744.md) [yalad](../../strongs/h/h3205.md) [Yišay](../../strongs/h/h3448.md), and [Yišay](../../strongs/h/h3448.md) [yalad](../../strongs/h/h3205.md) [Dāviḏ](../../strongs/h/h1732.md).

---

[Transliteral Bible](../bible.md)

[Ruth](ruth.md)

[Ruth 3](ruth_3.md)