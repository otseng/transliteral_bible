# [Ruth 1](https://www.blueletterbible.org/kjv/ruth/1)

<a name="ruth_1_1"></a>Ruth 1:1

Now it came to pass in the [yowm](../../strongs/h/h3117.md) when the [shaphat](../../strongs/h/h8199.md) [shaphat](../../strongs/h/h8199.md), that there was a [rāʿāḇ](../../strongs/h/h7458.md) in the ['erets](../../strongs/h/h776.md). And a certain ['iysh](../../strongs/h/h376.md) of [Bêṯ leḥem](../../strongs/h/h1035.md) [Yehuwdah](../../strongs/h/h3063.md) [yālaḵ](../../strongs/h/h3212.md) to [guwr](../../strongs/h/h1481.md) in the [sadeh](../../strongs/h/h7704.md) of [Mô'āḇ](../../strongs/h/h4124.md), he, and his ['ishshah](../../strongs/h/h802.md), and his [šᵊnayim](../../strongs/h/h8147.md) [ben](../../strongs/h/h1121.md).

<a name="ruth_1_2"></a>Ruth 1:2

And the [shem](../../strongs/h/h8034.md) of the ['iysh](../../strongs/h/h376.md) was ['Ĕlîmeleḵ](../../strongs/h/h458.md), and the [shem](../../strongs/h/h8034.md) of his ['ishshah](../../strongs/h/h802.md) [NāʿŎmî](../../strongs/h/h5281.md), and the [shem](../../strongs/h/h8034.md) of his [šᵊnayim](../../strongs/h/h8147.md) [ben](../../strongs/h/h1121.md) [Maḥlôn](../../strongs/h/h4248.md) and [Kilyôn](../../strongs/h/h3630.md), ['ep̄rāṯî](../../strongs/h/h673.md) of [Bêṯ leḥem](../../strongs/h/h1035.md) [Yehuwdah](../../strongs/h/h3063.md). And they [bow'](../../strongs/h/h935.md) into the [sadeh](../../strongs/h/h7704.md) of [Mô'āḇ](../../strongs/h/h4124.md), and continued there.

<a name="ruth_1_3"></a>Ruth 1:3

And ['Ĕlîmeleḵ](../../strongs/h/h458.md) [NāʿŎmî](../../strongs/h/h5281.md) ['iysh](../../strongs/h/h376.md) [muwth](../../strongs/h/h4191.md); and she was [šā'ar](../../strongs/h/h7604.md), and her [šᵊnayim](../../strongs/h/h8147.md) [ben](../../strongs/h/h1121.md).

<a name="ruth_1_4"></a>Ruth 1:4

And they [nasa'](../../strongs/h/h5375.md) them ['ishshah](../../strongs/h/h802.md) of the ['ishshah](../../strongs/h/h802.md) of [Mô'āḇî](../../strongs/h/h4125.md); the [shem](../../strongs/h/h8034.md) of the ['echad](../../strongs/h/h259.md) was [ʿĀrpâ](../../strongs/h/h6204.md), and the [shem](../../strongs/h/h8034.md) of the [šēnî](../../strongs/h/h8145.md) [Rûṯ](../../strongs/h/h7327.md): and they [yashab](../../strongs/h/h3427.md) there about [ʿeśer](../../strongs/h/h6235.md) [šānâ](../../strongs/h/h8141.md).

<a name="ruth_1_5"></a>Ruth 1:5

And [Maḥlôn](../../strongs/h/h4248.md) and [Kilyôn](../../strongs/h/h3630.md) [muwth](../../strongs/h/h4191.md) also [šᵊnayim](../../strongs/h/h8147.md) of them; and the ['ishshah](../../strongs/h/h802.md) was [šā'ar](../../strongs/h/h7604.md) of her [šᵊnayim](../../strongs/h/h8147.md) [yeleḏ](../../strongs/h/h3206.md) and her ['iysh](../../strongs/h/h376.md).

<a name="ruth_1_6"></a>Ruth 1:6

Then she [quwm](../../strongs/h/h6965.md) with her [kallâ](../../strongs/h/h3618.md), that she might [shuwb](../../strongs/h/h7725.md) from the [sadeh](../../strongs/h/h7704.md) of [Mô'āḇ](../../strongs/h/h4124.md): for she had [shama'](../../strongs/h/h8085.md) in the [sadeh](../../strongs/h/h7704.md) of [Mô'āḇ](../../strongs/h/h4124.md) how that [Yĕhovah](../../strongs/h/h3068.md) had [paqad](../../strongs/h/h6485.md) his ['am](../../strongs/h/h5971.md) in [nathan](../../strongs/h/h5414.md) them [lechem](../../strongs/h/h3899.md).

<a name="ruth_1_7"></a>Ruth 1:7

Wherefore she [yāṣā'](../../strongs/h/h3318.md) out of the [maqowm](../../strongs/h/h4725.md) where she was, and her [šᵊnayim](../../strongs/h/h8147.md) [kallâ](../../strongs/h/h3618.md) with her; and they [yālaḵ](../../strongs/h/h3212.md) on the [derek](../../strongs/h/h1870.md) to [shuwb](../../strongs/h/h7725.md) unto the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="ruth_1_8"></a>Ruth 1:8

And [NāʿŎmî](../../strongs/h/h5281.md) ['āmar](../../strongs/h/h559.md) unto her [šᵊnayim](../../strongs/h/h8147.md) [kallâ](../../strongs/h/h3618.md), [yālaḵ](../../strongs/h/h3212.md), [shuwb](../../strongs/h/h7725.md) ['ishshah](../../strongs/h/h802.md) to her ['em](../../strongs/h/h517.md) [bayith](../../strongs/h/h1004.md): [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) with you, as ye have ['asah](../../strongs/h/h6213.md) with the [muwth](../../strongs/h/h4191.md), and with me.

<a name="ruth_1_9"></a>Ruth 1:9

[Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) you that ye may [māṣā'](../../strongs/h/h4672.md) [mᵊnûḥâ](../../strongs/h/h4496.md), ['ishshah](../../strongs/h/h802.md) of you in the [bayith](../../strongs/h/h1004.md) of her ['iysh](../../strongs/h/h376.md). Then she [nashaq](../../strongs/h/h5401.md) them; and they [nasa'](../../strongs/h/h5375.md) their [qowl](../../strongs/h/h6963.md), and [bāḵâ](../../strongs/h/h1058.md).

<a name="ruth_1_10"></a>Ruth 1:10

And they ['āmar](../../strongs/h/h559.md) unto her, Surely we will [shuwb](../../strongs/h/h7725.md) with thee unto thy ['am](../../strongs/h/h5971.md).

<a name="ruth_1_11"></a>Ruth 1:11

And [NāʿŎmî](../../strongs/h/h5281.md) ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md), my [bath](../../strongs/h/h1323.md): why will ye [yālaḵ](../../strongs/h/h3212.md) with me? are there yet any more [ben](../../strongs/h/h1121.md) in my [me'ah](../../strongs/h/h4578.md), that they may be your ['enowsh](../../strongs/h/h582.md)?

<a name="ruth_1_12"></a>Ruth 1:12

[shuwb](../../strongs/h/h7725.md), my [bath](../../strongs/h/h1323.md), [yālaḵ](../../strongs/h/h3212.md) your way; for I am too [zāqēn](../../strongs/h/h2204.md) to have an ['iysh](../../strongs/h/h376.md). If I should ['āmar](../../strongs/h/h559.md), I [yēš](../../strongs/h/h3426.md) [tiqvâ](../../strongs/h/h8615.md), if I should have an ['iysh](../../strongs/h/h376.md) also to [layil](../../strongs/h/h3915.md), and should also [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md);

<a name="ruth_1_13"></a>Ruth 1:13

Would ye [śāḇar](../../strongs/h/h7663.md) for [lāhēn](../../strongs/h/h3860.md) till they were [gāḏal](../../strongs/h/h1431.md)? would ye [ʿāḡan](../../strongs/h/h5702.md) for [lāhēn](../../strongs/h/h3860.md) from having ['iysh](../../strongs/h/h376.md)? nay, my [bath](../../strongs/h/h1323.md); for it [mārar](../../strongs/h/h4843.md) me [me'od](../../strongs/h/h3966.md) for your sakes that the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) is [yāṣā'](../../strongs/h/h3318.md) against me.

<a name="ruth_1_14"></a>Ruth 1:14

And they [nasa'](../../strongs/h/h5375.md) their [qowl](../../strongs/h/h6963.md), and [bāḵâ](../../strongs/h/h1058.md): and [ʿĀrpâ](../../strongs/h/h6204.md) [nashaq](../../strongs/h/h5401.md) her [ḥāmôṯ](../../strongs/h/h2545.md); but [Rûṯ](../../strongs/h/h7327.md) [dāḇaq](../../strongs/h/h1692.md) unto her.

<a name="ruth_1_15"></a>Ruth 1:15

And she ['āmar](../../strongs/h/h559.md), Behold, thy [yᵊḇēmeṯ](../../strongs/h/h2994.md) is [shuwb](../../strongs/h/h7725.md) unto her ['am](../../strongs/h/h5971.md), and unto her ['Elohiym](../../strongs/h/h430.md): [shuwb](../../strongs/h/h7725.md) thou ['aḥar](../../strongs/h/h310.md) thy [yᵊḇēmeṯ](../../strongs/h/h2994.md).

<a name="ruth_1_16"></a>Ruth 1:16

And [Rûṯ](../../strongs/h/h7327.md) ['āmar](../../strongs/h/h559.md), [pāḡaʿ](../../strongs/h/h6293.md) me not to ['azab](../../strongs/h/h5800.md) thee, or to [shuwb](../../strongs/h/h7725.md) from following ['aḥar](../../strongs/h/h310.md) thee: for whither thou [yālaḵ](../../strongs/h/h3212.md), I will [yālaḵ](../../strongs/h/h3212.md); and where thou [lûn](../../strongs/h/h3885.md), I will [lûn](../../strongs/h/h3885.md): thy ['am](../../strongs/h/h5971.md) shall be my ['am](../../strongs/h/h5971.md), and thy ['Elohiym](../../strongs/h/h430.md) my ['Elohiym](../../strongs/h/h430.md):

<a name="ruth_1_17"></a>Ruth 1:17

Where thou [muwth](../../strongs/h/h4191.md), will I [muwth](../../strongs/h/h4191.md), and there will I be [qāḇar](../../strongs/h/h6912.md): [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) so to me, and [yāsap̄](../../strongs/h/h3254.md) also, if ought but [maveth](../../strongs/h/h4194.md) [pāraḏ](../../strongs/h/h6504.md) thee and me.

<a name="ruth_1_18"></a>Ruth 1:18

When she [ra'ah](../../strongs/h/h7200.md) that she was ['amats](../../strongs/h/h553.md) to [yālaḵ](../../strongs/h/h3212.md) with her, then she [ḥāḏal](../../strongs/h/h2308.md) [dabar](../../strongs/h/h1696.md) unto her.

<a name="ruth_1_19"></a>Ruth 1:19

So they [šᵊnayim](../../strongs/h/h8147.md) [yālaḵ](../../strongs/h/h3212.md) until they [bow'](../../strongs/h/h935.md) to [Bêṯ leḥem](../../strongs/h/h1035.md). And it came to pass, when they were [bow'](../../strongs/h/h935.md) to [Bêṯ leḥem](../../strongs/h/h1035.md), that all the [ʿîr](../../strongs/h/h5892.md) was [huwm](../../strongs/h/h1949.md) about them, and they ['āmar](../../strongs/h/h559.md), Is this [NāʿŎmî](../../strongs/h/h5281.md)?

<a name="ruth_1_20"></a>Ruth 1:20

And she ['āmar](../../strongs/h/h559.md) unto them, [qara'](../../strongs/h/h7121.md) me not [NāʿŎmî](../../strongs/h/h5281.md), [qara'](../../strongs/h/h7121.md) me [Mārā'](../../strongs/h/h4755.md): for the [Šaday](../../strongs/h/h7706.md) hath dealt [me'od](../../strongs/h/h3966.md) [mārar](../../strongs/h/h4843.md) with me.

<a name="ruth_1_21"></a>Ruth 1:21

I [halak](../../strongs/h/h1980.md) [mālē'](../../strongs/h/h4392.md), and [Yĕhovah](../../strongs/h/h3068.md) hath [shuwb](../../strongs/h/h7725.md) me [rêqām](../../strongs/h/h7387.md): why then [qara'](../../strongs/h/h7121.md) ye me [NāʿŎmî](../../strongs/h/h5281.md), seeing [Yĕhovah](../../strongs/h/h3068.md) hath ['anah](../../strongs/h/h6030.md) against me, and the [Šaday](../../strongs/h/h7706.md) hath [ra'a'](../../strongs/h/h7489.md) me?

<a name="ruth_1_22"></a>Ruth 1:22

So [NāʿŎmî](../../strongs/h/h5281.md) [shuwb](../../strongs/h/h7725.md), and [Rûṯ](../../strongs/h/h7327.md) the [Mô'āḇî](../../strongs/h/h4125.md), her [kallâ](../../strongs/h/h3618.md), with her, which [shuwb](../../strongs/h/h7725.md) out of the [sadeh](../../strongs/h/h7704.md) of [Mô'āḇ](../../strongs/h/h4124.md): and they [bow'](../../strongs/h/h935.md) to [Bêṯ leḥem](../../strongs/h/h1035.md) in the [tᵊḥillâ](../../strongs/h/h8462.md) of [śᵊʿōrâ](../../strongs/h/h8184.md) [qāṣîr](../../strongs/h/h7105.md).

---

[Transliteral Bible](../bible.md)

[Ruth](ruth.md)

[Ruth 2](ruth_2.md)