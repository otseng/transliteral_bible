# [Ruth 3](https://www.blueletterbible.org/kjv/ruth/3)

<a name="ruth_3_1"></a>Ruth 3:1

Then [NāʿŎmî](../../strongs/h/h5281.md) her [ḥāmôṯ](../../strongs/h/h2545.md) ['āmar](../../strongs/h/h559.md) unto her, My [bath](../../strongs/h/h1323.md), shall I not [bāqaš](../../strongs/h/h1245.md) [mānôaḥ](../../strongs/h/h4494.md) for thee, that it may be [yatab](../../strongs/h/h3190.md) with thee?

<a name="ruth_3_2"></a>Ruth 3:2

And now is not [BōʿAz](../../strongs/h/h1162.md) of our [môḏaʿaṯ](../../strongs/h/h4130.md), with whose [naʿărâ](../../strongs/h/h5291.md) thou wast? Behold, he [zārâ](../../strongs/h/h2219.md) [śᵊʿōrâ](../../strongs/h/h8184.md) to [layil](../../strongs/h/h3915.md) in the [gōren](../../strongs/h/h1637.md).

<a name="ruth_3_3"></a>Ruth 3:3

[rāḥaṣ](../../strongs/h/h7364.md) thyself therefore, and [sûḵ](../../strongs/h/h5480.md) thee, and [śûm](../../strongs/h/h7760.md) thy [śimlâ](../../strongs/h/h8071.md) upon thee, and get thee [yarad](../../strongs/h/h3381.md) to the [gōren](../../strongs/h/h1637.md): but make not thyself [yada'](../../strongs/h/h3045.md) unto the ['iysh](../../strongs/h/h376.md), until he shall have [kalah](../../strongs/h/h3615.md) ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md).

<a name="ruth_3_4"></a>Ruth 3:4

And it shall be, when he [shakab](../../strongs/h/h7901.md), that thou shalt [yada'](../../strongs/h/h3045.md) the [maqowm](../../strongs/h/h4725.md) where he shall [shakab](../../strongs/h/h7901.md), and thou shalt [bow'](../../strongs/h/h935.md), and [gālâ](../../strongs/h/h1540.md) his [margᵊlôṯ](../../strongs/h/h4772.md), and lay thee [shakab](../../strongs/h/h7901.md); and he will [nāḡaḏ](../../strongs/h/h5046.md) thee what thou shalt ['asah](../../strongs/h/h6213.md).

<a name="ruth_3_5"></a>Ruth 3:5

And she ['āmar](../../strongs/h/h559.md) unto her, All that thou ['āmar](../../strongs/h/h559.md) unto me I will ['asah](../../strongs/h/h6213.md).

<a name="ruth_3_6"></a>Ruth 3:6

And she [yarad](../../strongs/h/h3381.md) unto the [gōren](../../strongs/h/h1637.md), and ['asah](../../strongs/h/h6213.md) according to all that her [ḥāmôṯ](../../strongs/h/h2545.md) [tsavah](../../strongs/h/h6680.md) her.

<a name="ruth_3_7"></a>Ruth 3:7

And when [BōʿAz](../../strongs/h/h1162.md) had ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and his [leb](../../strongs/h/h3820.md) was [yatab](../../strongs/h/h3190.md), he [bow'](../../strongs/h/h935.md) to [shakab](../../strongs/h/h7901.md) at the [qāṣê](../../strongs/h/h7097.md) of the heap of [ʿărēmâ](../../strongs/h/h6194.md): and she [bow'](../../strongs/h/h935.md) [lāṭ](../../strongs/h/h3909.md), and [gālâ](../../strongs/h/h1540.md) his [margᵊlôṯ](../../strongs/h/h4772.md), and [shakab](../../strongs/h/h7901.md) her.

<a name="ruth_3_8"></a>Ruth 3:8

And it came to pass at [ḥēṣî](../../strongs/h/h2677.md) [layil](../../strongs/h/h3915.md), that the ['iysh](../../strongs/h/h376.md) was [ḥārēḏ](../../strongs/h/h2729.md), and [lāp̄aṯ](../../strongs/h/h3943.md) himself: and, behold, a ['ishshah](../../strongs/h/h802.md) [shakab](../../strongs/h/h7901.md) at his [margᵊlôṯ](../../strongs/h/h4772.md).

<a name="ruth_3_9"></a>Ruth 3:9

And he ['āmar](../../strongs/h/h559.md), Who art thou? And she ['āmar](../../strongs/h/h559.md), I am [Rûṯ](../../strongs/h/h7327.md) thine ['amah](../../strongs/h/h519.md): [pāraś](../../strongs/h/h6566.md) therefore thy [kanaph](../../strongs/h/h3671.md) over thine ['amah](../../strongs/h/h519.md); for thou art a [gā'al](../../strongs/h/h1350.md).

<a name="ruth_3_10"></a>Ruth 3:10

And he ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be thou of [Yĕhovah](../../strongs/h/h3068.md), my [bath](../../strongs/h/h1323.md): for thou hast [yatab](../../strongs/h/h3190.md) more [checed](../../strongs/h/h2617.md) in the ['aḥărôn](../../strongs/h/h314.md) than at the [ri'šôn](../../strongs/h/h7223.md), inasmuch as thou [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) [biltî](../../strongs/h/h1115.md) [bāḥûr](../../strongs/h/h970.md), whether [dal](../../strongs/h/h1800.md) or [ʿāšîr](../../strongs/h/h6223.md).

<a name="ruth_3_11"></a>Ruth 3:11

And now, my [bath](../../strongs/h/h1323.md), [yare'](../../strongs/h/h3372.md) not; I will ['asah](../../strongs/h/h6213.md) to thee all that thou ['āmar](../../strongs/h/h559.md): for all the [sha'ar](../../strongs/h/h8179.md) of my ['am](../../strongs/h/h5971.md) doth [yada'](../../strongs/h/h3045.md) that thou art a [ḥayil](../../strongs/h/h2428.md) ['ishshah](../../strongs/h/h802.md).

<a name="ruth_3_12"></a>Ruth 3:12

And now it is ['āmnām](../../strongs/h/h551.md) that I am thy [gā'al](../../strongs/h/h1350.md): howbeit there [yēš](../../strongs/h/h3426.md) a [gā'al](../../strongs/h/h1350.md) [qarowb](../../strongs/h/h7138.md) than I.

<a name="ruth_3_13"></a>Ruth 3:13

[lûn](../../strongs/h/h3885.md) this [layil](../../strongs/h/h3915.md), and it shall be in the [boqer](../../strongs/h/h1242.md), that if he will perform unto thee the part of a [gā'al](../../strongs/h/h1350.md), [towb](../../strongs/h/h2896.md); let him do the [gā'al](../../strongs/h/h1350.md): but if he [ḥāp̄ēṣ](../../strongs/h/h2654.md) not do the part of a [gā'al](../../strongs/h/h1350.md) to thee, then will I do the part of a [gā'al](../../strongs/h/h1350.md) to thee, as [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md): [shakab](../../strongs/h/h7901.md) until the [boqer](../../strongs/h/h1242.md).

<a name="ruth_3_14"></a>Ruth 3:14

And she [shakab](../../strongs/h/h7901.md) at his [margᵊlôṯ](../../strongs/h/h4772.md) until the [boqer](../../strongs/h/h1242.md): and she [quwm](../../strongs/h/h6965.md) [ṭᵊrôm](../../strongs/h/h2958.md) ['iysh](../../strongs/h/h376.md) could [nāḵar](../../strongs/h/h5234.md) [rea'](../../strongs/h/h7453.md). And he ['āmar](../../strongs/h/h559.md), Let it not be [yada'](../../strongs/h/h3045.md) that a ['ishshah](../../strongs/h/h802.md) [bow'](../../strongs/h/h935.md) into the [gōren](../../strongs/h/h1637.md).

<a name="ruth_3_15"></a>Ruth 3:15

Also he ['āmar](../../strongs/h/h559.md), [yāhaḇ](../../strongs/h/h3051.md) the [miṭpaḥaṯ](../../strongs/h/h4304.md) that thou hast upon thee, and ['āḥaz](../../strongs/h/h270.md) it. And when she ['āḥaz](../../strongs/h/h270.md) it, he [māḏaḏ](../../strongs/h/h4058.md) [šēš](../../strongs/h/h8337.md) measures of [śᵊʿōrâ](../../strongs/h/h8184.md), and [shiyth](../../strongs/h/h7896.md) it on her: and she [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md).

<a name="ruth_3_16"></a>Ruth 3:16

And when she [bow'](../../strongs/h/h935.md) to her [ḥāmôṯ](../../strongs/h/h2545.md), she ['āmar](../../strongs/h/h559.md), Who art thou, my [bath](../../strongs/h/h1323.md)? And she [nāḡaḏ](../../strongs/h/h5046.md) her all that the ['iysh](../../strongs/h/h376.md) had ['asah](../../strongs/h/h6213.md) to her.

<a name="ruth_3_17"></a>Ruth 3:17

And she ['āmar](../../strongs/h/h559.md), These [šēš](../../strongs/h/h8337.md) measures of [śᵊʿōrâ](../../strongs/h/h8184.md) [nathan](../../strongs/h/h5414.md) he me; for he ['āmar](../../strongs/h/h559.md) to me, [bow'](../../strongs/h/h935.md) not [rêqām](../../strongs/h/h7387.md) unto thy [ḥāmôṯ](../../strongs/h/h2545.md).

<a name="ruth_3_18"></a>Ruth 3:18

Then ['āmar](../../strongs/h/h559.md) she, [yashab](../../strongs/h/h3427.md), my [bath](../../strongs/h/h1323.md), until thou [yada'](../../strongs/h/h3045.md) how the [dabar](../../strongs/h/h1697.md) will [naphal](../../strongs/h/h5307.md): for the ['iysh](../../strongs/h/h376.md) will not be in [šāqaṭ](../../strongs/h/h8252.md), ['im](../../strongs/h/h518.md) he have [kalah](../../strongs/h/h3615.md) the [dabar](../../strongs/h/h1697.md) this [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Ruth](ruth.md)

[Ruth 2](ruth_2.md) - [Ruth 4](ruth_4.md)