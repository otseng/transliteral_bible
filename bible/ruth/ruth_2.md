# [Ruth 2](https://www.blueletterbible.org/kjv/ruth/2)

<a name="ruth_2_1"></a>Ruth 2:1

And [NāʿŎmî](../../strongs/h/h5281.md) had a [môḏāʿ](../../strongs/h/h4129.md) [yada'](../../strongs/h/h3045.md) of her ['iysh](../../strongs/h/h376.md), a [gibôr](../../strongs/h/h1368.md) ['iysh](../../strongs/h/h376.md) of [ḥayil](../../strongs/h/h2428.md), of the [mišpāḥâ](../../strongs/h/h4940.md) of ['Ĕlîmeleḵ](../../strongs/h/h458.md); and his [shem](../../strongs/h/h8034.md) was [BōʿAz](../../strongs/h/h1162.md).

<a name="ruth_2_2"></a>Ruth 2:2

And [Rûṯ](../../strongs/h/h7327.md) the [Mô'āḇî](../../strongs/h/h4125.md) ['āmar](../../strongs/h/h559.md) unto [NāʿŎmî](../../strongs/h/h5281.md), Let me now [yālaḵ](../../strongs/h/h3212.md) to the [sadeh](../../strongs/h/h7704.md), and [lāqaṭ](../../strongs/h/h3950.md) ears of [šibōleṯ](../../strongs/h/h7641.md) ['aḥar](../../strongs/h/h310.md) him in whose ['ayin](../../strongs/h/h5869.md) I shall [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md). And she ['āmar](../../strongs/h/h559.md) unto her, [yālaḵ](../../strongs/h/h3212.md), my [bath](../../strongs/h/h1323.md).

<a name="ruth_2_3"></a>Ruth 2:3

And she [yālaḵ](../../strongs/h/h3212.md), and [bow'](../../strongs/h/h935.md), and [lāqaṭ](../../strongs/h/h3950.md) in the [sadeh](../../strongs/h/h7704.md) ['aḥar](../../strongs/h/h310.md) the [qāṣar](../../strongs/h/h7114.md): and her [miqrê](../../strongs/h/h4745.md) was to [qārâ](../../strongs/h/h7136.md) on a [ḥelqâ](../../strongs/h/h2513.md) of the [sadeh](../../strongs/h/h7704.md) belonging unto [BōʿAz](../../strongs/h/h1162.md), who was of the [mišpāḥâ](../../strongs/h/h4940.md) of ['Ĕlîmeleḵ](../../strongs/h/h458.md).

<a name="ruth_2_4"></a>Ruth 2:4

And, behold, [BōʿAz](../../strongs/h/h1162.md) [bow'](../../strongs/h/h935.md) from [Bêṯ leḥem](../../strongs/h/h1035.md), and ['āmar](../../strongs/h/h559.md) unto the [qāṣar](../../strongs/h/h7114.md), [Yĕhovah](../../strongs/h/h3068.md) be with you. And they ['āmar](../../strongs/h/h559.md) him, [Yĕhovah](../../strongs/h/h3068.md) [barak](../../strongs/h/h1288.md) thee.

<a name="ruth_2_5"></a>Ruth 2:5

Then ['āmar](../../strongs/h/h559.md) [BōʿAz](../../strongs/h/h1162.md) unto his [naʿar](../../strongs/h/h5288.md) that was [nāṣaḇ](../../strongs/h/h5324.md) over the [qāṣar](../../strongs/h/h7114.md), Whose [naʿărâ](../../strongs/h/h5291.md) is this?

<a name="ruth_2_6"></a>Ruth 2:6

And the [naʿar](../../strongs/h/h5288.md) that was [nāṣaḇ](../../strongs/h/h5324.md) over the [qāṣar](../../strongs/h/h7114.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), It is the [Mô'āḇî](../../strongs/h/h4125.md) [naʿărâ](../../strongs/h/h5291.md) that [shuwb](../../strongs/h/h7725.md) with [NāʿŎmî](../../strongs/h/h5281.md) out of the [sadeh](../../strongs/h/h7704.md) of [Mô'āḇ](../../strongs/h/h4124.md):

<a name="ruth_2_7"></a>Ruth 2:7

And she ['āmar](../../strongs/h/h559.md), I pray you, let me [lāqaṭ](../../strongs/h/h3950.md) and ['āsap̄](../../strongs/h/h622.md) ['aḥar](../../strongs/h/h310.md) the [qāṣar](../../strongs/h/h7114.md) among the [ʿōmer](../../strongs/h/h6016.md): so she [bow'](../../strongs/h/h935.md), and hath ['amad](../../strongs/h/h5975.md) ['āz](../../strongs/h/h227.md) from the [boqer](../../strongs/h/h1242.md) until now, that she [yashab](../../strongs/h/h3427.md) a [mᵊʿaṭ](../../strongs/h/h4592.md) in the [bayith](../../strongs/h/h1004.md).

<a name="ruth_2_8"></a>Ruth 2:8

Then ['āmar](../../strongs/h/h559.md) [BōʿAz](../../strongs/h/h1162.md) unto [Rûṯ](../../strongs/h/h7327.md), [shama'](../../strongs/h/h8085.md) thou not, my [bath](../../strongs/h/h1323.md)? [yālaḵ](../../strongs/h/h3212.md) not to [lāqaṭ](../../strongs/h/h3950.md) in ['aḥēr](../../strongs/h/h312.md) [sadeh](../../strongs/h/h7704.md), neither ['abar](../../strongs/h/h5674.md) from hence, but [dāḇaq](../../strongs/h/h1692.md) here fast by my [naʿărâ](../../strongs/h/h5291.md):

<a name="ruth_2_9"></a>Ruth 2:9

Let thine ['ayin](../../strongs/h/h5869.md) be on the [sadeh](../../strongs/h/h7704.md) that they do [qāṣar](../../strongs/h/h7114.md), and [halak](../../strongs/h/h1980.md) thou ['aḥar](../../strongs/h/h310.md) them: have I not [tsavah](../../strongs/h/h6680.md) the [naʿar](../../strongs/h/h5288.md) that they shall not [naga'](../../strongs/h/h5060.md) thee? and when thou art [ṣāmē'](../../strongs/h/h6770.md), [halak](../../strongs/h/h1980.md) unto the [kĕliy](../../strongs/h/h3627.md), and [šāṯâ](../../strongs/h/h8354.md) of that which the [naʿar](../../strongs/h/h5288.md) have [šā'aḇ](../../strongs/h/h7579.md).

<a name="ruth_2_10"></a>Ruth 2:10

Then she [naphal](../../strongs/h/h5307.md) on her [paniym](../../strongs/h/h6440.md), and [shachah](../../strongs/h/h7812.md) herself to the ['erets](../../strongs/h/h776.md), and ['āmar](../../strongs/h/h559.md) unto him, Why have I [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thine ['ayin](../../strongs/h/h5869.md), that thou shouldest take [nāḵar](../../strongs/h/h5234.md) of me, seeing I am a [nāḵrî](../../strongs/h/h5237.md)?

<a name="ruth_2_11"></a>Ruth 2:11

And [BōʿAz](../../strongs/h/h1162.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto her, It hath [nāḡaḏ](../../strongs/h/h5046.md) been [nāḡaḏ](../../strongs/h/h5046.md) me, all that thou hast ['asah](../../strongs/h/h6213.md) unto thy [ḥāmôṯ](../../strongs/h/h2545.md) ['aḥar](../../strongs/h/h310.md) the [maveth](../../strongs/h/h4194.md) of thine ['iysh](../../strongs/h/h376.md): and how thou hast ['azab](../../strongs/h/h5800.md) thy ['ab](../../strongs/h/h1.md) and thy ['em](../../strongs/h/h517.md), and the ['erets](../../strongs/h/h776.md) of thy [môleḏeṯ](../../strongs/h/h4138.md), and art [yālaḵ](../../strongs/h/h3212.md) unto a ['am](../../strongs/h/h5971.md) which thou [yada'](../../strongs/h/h3045.md) not [tᵊmôl](../../strongs/h/h8543.md) [šilšôm](../../strongs/h/h8032.md).

<a name="ruth_2_12"></a>Ruth 2:12

[Yĕhovah](../../strongs/h/h3068.md) [shalam](../../strongs/h/h7999.md) thy [pōʿal](../../strongs/h/h6467.md), and a [šālēm](../../strongs/h/h8003.md) [maśkōreṯ](../../strongs/h/h4909.md) be given thee of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), under whose [kanaph](../../strongs/h/h3671.md) thou art [bow'](../../strongs/h/h935.md) to [chacah](../../strongs/h/h2620.md).

<a name="ruth_2_13"></a>Ruth 2:13

Then she ['āmar](../../strongs/h/h559.md), Let me [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md), my ['adown](../../strongs/h/h113.md); for that thou hast [nacham](../../strongs/h/h5162.md) me, and for that thou hast [dabar](../../strongs/h/h1696.md) [leb](../../strongs/h/h3820.md) unto thine [šip̄ḥâ](../../strongs/h/h8198.md), though I be not like unto ['echad](../../strongs/h/h259.md) of thine [šip̄ḥâ](../../strongs/h/h8198.md).

<a name="ruth_2_14"></a>Ruth 2:14

And [BōʿAz](../../strongs/h/h1162.md) ['āmar](../../strongs/h/h559.md) unto her, At [ʿēṯ](../../strongs/h/h6256.md) ['ōḵel](../../strongs/h/h400.md) [nāḡaš](../../strongs/h/h5066.md) thou [hălōm](../../strongs/h/h1988.md), and ['akal](../../strongs/h/h398.md) of the [lechem](../../strongs/h/h3899.md), and [ṭāḇal](../../strongs/h/h2881.md) thy [paṯ](../../strongs/h/h6595.md) in the [ḥōmeṣ](../../strongs/h/h2558.md). And she [yashab](../../strongs/h/h3427.md) [ṣaḏ](../../strongs/h/h6654.md) the [qāṣar](../../strongs/h/h7114.md): and he [ṣāḇaṭ](../../strongs/h/h6642.md) her [qālî](../../strongs/h/h7039.md) corn, and she did ['akal](../../strongs/h/h398.md), and was [sāׂbaʿ](../../strongs/h/h7646.md), and [yāṯar](../../strongs/h/h3498.md).

<a name="ruth_2_15"></a>Ruth 2:15

And when she was [quwm](../../strongs/h/h6965.md) to [lāqaṭ](../../strongs/h/h3950.md), [BōʿAz](../../strongs/h/h1162.md) [tsavah](../../strongs/h/h6680.md) his [naʿar](../../strongs/h/h5288.md), ['āmar](../../strongs/h/h559.md), Let her [lāqaṭ](../../strongs/h/h3950.md) even among the [ʿōmer](../../strongs/h/h6016.md), and [kālam](../../strongs/h/h3637.md) her not:

<a name="ruth_2_16"></a>Ruth 2:16

And let [šālal](../../strongs/h/h7997.md) also some of the [ṣeḇeṯ](../../strongs/h/h6653.md) of [šālal](../../strongs/h/h7997.md) for her, and ['azab](../../strongs/h/h5800.md) them, that she may [lāqaṭ](../../strongs/h/h3950.md) them, and [gāʿar](../../strongs/h/h1605.md) her not.

<a name="ruth_2_17"></a>Ruth 2:17

So she [lāqaṭ](../../strongs/h/h3950.md) in the [sadeh](../../strongs/h/h7704.md) until ['ereb](../../strongs/h/h6153.md), and [ḥāḇaṭ](../../strongs/h/h2251.md) that she had [lāqaṭ](../../strongs/h/h3950.md): and it was about an ['êp̄â](../../strongs/h/h374.md) of [śᵊʿōrâ](../../strongs/h/h8184.md).

<a name="ruth_2_18"></a>Ruth 2:18

And she [nasa'](../../strongs/h/h5375.md) it, and [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md): and her [ḥāmôṯ](../../strongs/h/h2545.md) [ra'ah](../../strongs/h/h7200.md) what she had [lāqaṭ](../../strongs/h/h3950.md): and she [yāṣā'](../../strongs/h/h3318.md), and [nathan](../../strongs/h/h5414.md) to her that she had [yāṯar](../../strongs/h/h3498.md) after she was [śōḇaʿ](../../strongs/h/h7648.md).

<a name="ruth_2_19"></a>Ruth 2:19

And her [ḥāmôṯ](../../strongs/h/h2545.md) ['āmar](../../strongs/h/h559.md) unto her, Where hast thou [lāqaṭ](../../strongs/h/h3950.md) to [yowm](../../strongs/h/h3117.md)? and ['êp̄ô](../../strongs/h/h375.md) ['asah](../../strongs/h/h6213.md) thou? [barak](../../strongs/h/h1288.md) be he that did take [nāḵar](../../strongs/h/h5234.md) of thee. And she [nāḡaḏ](../../strongs/h/h5046.md) her [ḥāmôṯ](../../strongs/h/h2545.md) with whom she had ['asah](../../strongs/h/h6213.md), and ['āmar](../../strongs/h/h559.md), The ['iysh](../../strongs/h/h376.md) [shem](../../strongs/h/h8034.md) with whom I ['asah](../../strongs/h/h6213.md) to [yowm](../../strongs/h/h3117.md) is [BōʿAz](../../strongs/h/h1162.md).

<a name="ruth_2_20"></a>Ruth 2:20

And [NāʿŎmî](../../strongs/h/h5281.md) ['āmar](../../strongs/h/h559.md) unto her [kallâ](../../strongs/h/h3618.md), [barak](../../strongs/h/h1288.md) be he of [Yĕhovah](../../strongs/h/h3068.md), who hath not ['azab](../../strongs/h/h5800.md) his [checed](../../strongs/h/h2617.md) to the [chay](../../strongs/h/h2416.md) and to the [muwth](../../strongs/h/h4191.md). And [NāʿŎmî](../../strongs/h/h5281.md) ['āmar](../../strongs/h/h559.md) unto her, The ['iysh](../../strongs/h/h376.md) is near of [qarowb](../../strongs/h/h7138.md) unto us, one of our next [gā'al](../../strongs/h/h1350.md).

<a name="ruth_2_21"></a>Ruth 2:21

And [Rûṯ](../../strongs/h/h7327.md) the [Mô'āḇî](../../strongs/h/h4125.md) ['āmar](../../strongs/h/h559.md), He ['āmar](../../strongs/h/h559.md) unto me also, Thou shalt keep [dāḇaq](../../strongs/h/h1692.md) by my [naʿar](../../strongs/h/h5288.md), until they have [kalah](../../strongs/h/h3615.md) all my [qāṣîr](../../strongs/h/h7105.md).

<a name="ruth_2_22"></a>Ruth 2:22

And [NāʿŎmî](../../strongs/h/h5281.md) ['āmar](../../strongs/h/h559.md) unto [Rûṯ](../../strongs/h/h7327.md) her [kallâ](../../strongs/h/h3618.md), It is [towb](../../strongs/h/h2896.md), my [bath](../../strongs/h/h1323.md), that thou [yāṣā'](../../strongs/h/h3318.md) with his [naʿărâ](../../strongs/h/h5291.md), that they [pāḡaʿ](../../strongs/h/h6293.md) thee not in any ['aḥēr](../../strongs/h/h312.md) [sadeh](../../strongs/h/h7704.md).

<a name="ruth_2_23"></a>Ruth 2:23

So she kept [dāḇaq](../../strongs/h/h1692.md) by the [naʿărâ](../../strongs/h/h5291.md) of [BōʿAz](../../strongs/h/h1162.md) to [lāqaṭ](../../strongs/h/h3950.md) unto the [kalah](../../strongs/h/h3615.md) of [śᵊʿōrâ](../../strongs/h/h8184.md) [qāṣîr](../../strongs/h/h7105.md) and of [ḥiṭṭâ](../../strongs/h/h2406.md) [qāṣîr](../../strongs/h/h7105.md); and [yashab](../../strongs/h/h3427.md) with her [ḥāmôṯ](../../strongs/h/h2545.md).

---

[Transliteral Bible](../bible.md)

[Ruth](ruth.md)

[Ruth 1](ruth_1.md) - [Ruth 3](ruth_3.md)