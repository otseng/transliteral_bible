# [James 5](https://www.blueletterbible.org/kjv/jas/5/1/s_1151001)

<a name="james_5_1"></a>James 5:1

[age](../../strongs/g/g33.md) now, [plousios](../../strongs/g/g4145.md), [klaiō](../../strongs/g/g2799.md) and [ololyzō](../../strongs/g/g3649.md) for your [talaipōria](../../strongs/g/g5004.md) that shall [eperchomai](../../strongs/g/g1904.md).

<a name="james_5_2"></a>James 5:2

Your [ploutos](../../strongs/g/g4149.md) are [sēpō](../../strongs/g/g4595.md), and your [himation](../../strongs/g/g2440.md) are [sētobrōtos](../../strongs/g/g4598.md).

<a name="james_5_3"></a>James 5:3

Your [chrysos](../../strongs/g/g5557.md) and [argyros](../../strongs/g/g696.md) is [katioō](../../strongs/g/g2728.md); and the [ios](../../strongs/g/g2447.md) of them shall be a [martyrion](../../strongs/g/g3142.md) against you, and shall [phago](../../strongs/g/g5315.md) your [sarx](../../strongs/g/g4561.md) as it were [pyr](../../strongs/g/g4442.md). Ye have [thēsaurizō](../../strongs/g/g2343.md) for the [eschatos](../../strongs/g/g2078.md) [hēmera](../../strongs/g/g2250.md).

<a name="james_5_4"></a>James 5:4

[idou](../../strongs/g/g2400.md), the [misthos](../../strongs/g/g3408.md) of the [ergatēs](../../strongs/g/g2040.md) who have [amaō](../../strongs/g/g270.md) your [chōra](../../strongs/g/g5561.md), which is of you [apostereō](../../strongs/g/g650.md), [krazō](../../strongs/g/g2896.md): and the [boē](../../strongs/g/g995.md) of them which have [therizō](../../strongs/g/g2325.md) are [eiserchomai](../../strongs/g/g1525.md) into the [ous](../../strongs/g/g3775.md) of the [kyrios](../../strongs/g/g2962.md) of [Sabaōth](../../strongs/g/g4519.md).

<a name="james_5_5"></a>James 5:5

Ye have [tryphaō](../../strongs/g/g5171.md) on [gē](../../strongs/g/g1093.md), and been [spatalaō](../../strongs/g/g4684.md); ye have [trephō](../../strongs/g/g5142.md) your [kardia](../../strongs/g/g2588.md), as in a day of [sphagē](../../strongs/g/g4967.md).

<a name="james_5_6"></a>James 5:6

Ye have [katadikazō](../../strongs/g/g2613.md) [phoneuō](../../strongs/g/g5407.md) the [dikaios](../../strongs/g/g1342.md); he doth not [antitassō](../../strongs/g/g498.md) you.

<a name="james_5_7"></a>James 5:7

Be [makrothymeō](../../strongs/g/g3114.md) therefore, [adelphos](../../strongs/g/g80.md), unto the [parousia](../../strongs/g/g3952.md) of the [kyrios](../../strongs/g/g2962.md). [idou](../../strongs/g/g2400.md), the [geōrgos](../../strongs/g/g1092.md) [ekdechomai](../../strongs/g/g1551.md) for the [timios](../../strongs/g/g5093.md) [karpos](../../strongs/g/g2590.md) of [gē](../../strongs/g/g1093.md), and hath [makrothymeō](../../strongs/g/g3114.md) for it, until he [lambanō](../../strongs/g/g2983.md) the [proimos](../../strongs/g/g4406.md) and [opsimos](../../strongs/g/g3797.md) [hyetos](../../strongs/g/g5205.md).

<a name="james_5_8"></a>James 5:8

Be ye also [makrothymeō](../../strongs/g/g3114.md); [stērizō](../../strongs/g/g4741.md) your [kardia](../../strongs/g/g2588.md): for the [parousia](../../strongs/g/g3952.md) of the [kyrios](../../strongs/g/g2962.md) [eggizō](../../strongs/g/g1448.md).

<a name="james_5_9"></a>James 5:9

[stenazō](../../strongs/g/g4727.md) not against [allēlōn](../../strongs/g/g240.md), [adelphos](../../strongs/g/g80.md), lest ye be [katakrinō](../../strongs/g/g2632.md): [idou](../../strongs/g/g2400.md), the [kritēs](../../strongs/g/g2923.md) [histēmi](../../strongs/g/g2476.md) before the [thyra](../../strongs/g/g2374.md).

<a name="james_5_10"></a>James 5:10

[lambanō](../../strongs/g/g2983.md), my [adelphos](../../strongs/g/g80.md), the [prophētēs](../../strongs/g/g4396.md), who have [laleō](../../strongs/g/g2980.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md), for an [hypodeigma](../../strongs/g/g5262.md) of [kakopatheia](../../strongs/g/g2552.md), and of [makrothymia](../../strongs/g/g3115.md).

<a name="james_5_11"></a>James 5:11

[idou](../../strongs/g/g2400.md), we [makarizō](../../strongs/g/g3106.md) which [hypomenō](../../strongs/g/g5278.md). Ye have [akouō](../../strongs/g/g191.md) of the [hypomonē](../../strongs/g/g5281.md) of [Iōb](../../strongs/g/g2492.md), and have [eidō](../../strongs/g/g1492.md) the [telos](../../strongs/g/g5056.md) of the [kyrios](../../strongs/g/g2962.md); that the [kyrios](../../strongs/g/g2962.md) is [polysplagchnos](../../strongs/g/g4184.md), and of [oiktirmōn](../../strongs/g/g3629.md).

<a name="james_5_12"></a>James 5:12

But above all things, my [adelphos](../../strongs/g/g80.md), [omnyō](../../strongs/g/g3660.md) not, neither by [ouranos](../../strongs/g/g3772.md), neither by [gē](../../strongs/g/g1093.md), neither by any other [horkos](../../strongs/g/g3727.md): but let your [nai](../../strongs/g/g3483.md) be [nai](../../strongs/g/g3483.md); and [ou](../../strongs/g/g3756.md), [ou](../../strongs/g/g3756.md); lest ye [piptō](../../strongs/g/g4098.md) into [hypokrisis](../../strongs/g/g5272.md).

<a name="james_5_13"></a>James 5:13

Is any among you [kakopatheō](../../strongs/g/g2553.md)? let him [proseuchomai](../../strongs/g/g4336.md). Is any [euthymeō](../../strongs/g/g2114.md)? let him [psallō](../../strongs/g/g5567.md).

<a name="james_5_14"></a>James 5:14

Is any [astheneō](../../strongs/g/g770.md) among you? let him [proskaleō](../../strongs/g/g4341.md) for the [presbyteros](../../strongs/g/g4245.md) of the [ekklēsia](../../strongs/g/g1577.md); and let them [proseuchomai](../../strongs/g/g4336.md) over him, [aleiphō](../../strongs/g/g218.md) him with [elaion](../../strongs/g/g1637.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md):

<a name="james_5_15"></a>James 5:15

And the [euchē](../../strongs/g/g2171.md) of [pistis](../../strongs/g/g4102.md) shall [sōzō](../../strongs/g/g4982.md) the [kamnō](../../strongs/g/g2577.md), and the [kyrios](../../strongs/g/g2962.md) shall [egeirō](../../strongs/g/g1453.md) him up; and if he have [poieō](../../strongs/g/g4160.md) [hamartia](../../strongs/g/g266.md), they shall be [aphiēmi](../../strongs/g/g863.md) him.

<a name="james_5_16"></a>James 5:16

[exomologeō](../../strongs/g/g1843.md) [paraptōma](../../strongs/g/g3900.md) to [allēlōn](../../strongs/g/g240.md), and [euchomai](../../strongs/g/g2172.md) for [allēlōn](../../strongs/g/g240.md), that ye may be [iaomai](../../strongs/g/g2390.md). The [energeō](../../strongs/g/g1754.md) [deēsis](../../strongs/g/g1162.md) of a [dikaios](../../strongs/g/g1342.md) [ischyō](../../strongs/g/g2480.md) [polys](../../strongs/g/g4183.md).

<a name="james_5_17"></a>James 5:17

[Ēlias](../../strongs/g/g2243.md) was an [anthrōpos](../../strongs/g/g444.md) [homoiopathēs](../../strongs/g/g3663.md) as we are, and he [proseuchomai](../../strongs/g/g4336.md) [proseuchē](../../strongs/g/g4335.md) that it might not [brechō](../../strongs/g/g1026.md): and it [brechō](../../strongs/g/g1026.md) not on [gē](../../strongs/g/g1093.md) by the space of three [eniautos](../../strongs/g/g1763.md) and six [mēn](../../strongs/g/g3376.md).

<a name="james_5_18"></a>James 5:18

And he [proseuchomai](../../strongs/g/g4336.md) again, and the [ouranos](../../strongs/g/g3772.md) [didōmi](../../strongs/g/g1325.md) [hyetos](../../strongs/g/g5205.md), and [gē](../../strongs/g/g1093.md) [blastanō](../../strongs/g/g985.md) her [karpos](../../strongs/g/g2590.md).

<a name="james_5_19"></a>James 5:19

[adelphos](../../strongs/g/g80.md), if any of you [planaō](../../strongs/g/g4105.md) from the [alētheia](../../strongs/g/g225.md), and one [epistrephō](../../strongs/g/g1994.md) him;

<a name="james_5_20"></a>James 5:20

Let him [ginōskō](../../strongs/g/g1097.md), that he which [epistrephō](../../strongs/g/g1994.md) the [hamartōlos](../../strongs/g/g268.md) from the [planē](../../strongs/g/g4106.md) of his [hodos](../../strongs/g/g3598.md) shall [sōzō](../../strongs/g/g4982.md) a [psychē](../../strongs/g/g5590.md) from [thanatos](../../strongs/g/g2288.md), and shall [kalyptō](../../strongs/g/g2572.md) a [plēthos](../../strongs/g/g4128.md) of [hamartia](../../strongs/g/g266.md).

---

[Transliteral Bible](../bible.md)

[James](james.md)

[James 4](james_4.md)