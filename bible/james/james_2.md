# [James 2](https://www.blueletterbible.org/kjv/jas/2/1/s_1148001)

<a name="james_2_1"></a>James 2:1

My [adelphos](../../strongs/g/g80.md), have not the [pistis](../../strongs/g/g4102.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), of [doxa](../../strongs/g/g1391.md), with [prosōpolēmpsia](../../strongs/g/g4382.md).

<a name="james_2_2"></a>James 2:2

For if there [eiserchomai](../../strongs/g/g1525.md) unto your [synagōgē](../../strongs/g/g4864.md) an [anēr](../../strongs/g/g435.md) [chrysodaktylios](../../strongs/g/g5554.md), in [lampros](../../strongs/g/g2986.md) [esthēs](../../strongs/g/g2066.md), and there [eiserchomai](../../strongs/g/g1525.md) also a [ptōchos](../../strongs/g/g4434.md) in [rhyparos](../../strongs/g/g4508.md) [esthēs](../../strongs/g/g2066.md);

<a name="james_2_3"></a>James 2:3

And ye have [epiblepō](../../strongs/g/g1914.md) to him that [phoreō](../../strongs/g/g5409.md) the [lampros](../../strongs/g/g2986.md) [esthēs](../../strongs/g/g2066.md), and [eipon](../../strongs/g/g2036.md) unto him, [kathēmai](../../strongs/g/g2521.md) thou here [kalōs](../../strongs/g/g2573.md); and [eipon](../../strongs/g/g2036.md) to the [ptōchos](../../strongs/g/g4434.md), [histēmi](../../strongs/g/g2476.md) thou there, or [kathēmai](../../strongs/g/g2521.md) here under my [hypopodion](../../strongs/g/g5286.md):

<a name="james_2_4"></a>James 2:4

Are ye not then [diakrinō](../../strongs/g/g1252.md) in yourselves, and are become [kritēs](../../strongs/g/g2923.md) of [ponēros](../../strongs/g/g4190.md) [dialogismos](../../strongs/g/g1261.md)?

<a name="james_2_5"></a>James 2:5

[akouō](../../strongs/g/g191.md), my [agapētos](../../strongs/g/g27.md) [adelphos](../../strongs/g/g80.md), Hath not [theos](../../strongs/g/g2316.md) [eklegomai](../../strongs/g/g1586.md) the [ptōchos](../../strongs/g/g4434.md) of this [kosmos](../../strongs/g/g2889.md) [plousios](../../strongs/g/g4145.md) in [pistis](../../strongs/g/g4102.md), and [klēronomos](../../strongs/g/g2818.md) of the [basileia](../../strongs/g/g932.md) which he hath [epaggellomai](../../strongs/g/g1861.md) to them that [agapaō](../../strongs/g/g25.md) him?

<a name="james_2_6"></a>James 2:6

But ye have [atimazō](../../strongs/g/g818.md) the [ptōchos](../../strongs/g/g4434.md). [katadynasteuō](../../strongs/g/g2616.md) not [plousios](../../strongs/g/g4145.md) [katadynasteuō](../../strongs/g/g2616.md) you, and [helkō](../../strongs/g/g1670.md) you before the [kritērion](../../strongs/g/g2922.md)?

<a name="james_2_7"></a>James 2:7

Do not they [blasphēmeō](../../strongs/g/g987.md) that [kalos](../../strongs/g/g2570.md) [onoma](../../strongs/g/g3686.md) by the which ye are [epikaleō](../../strongs/g/g1941.md)?

<a name="james_2_8"></a>James 2:8

If ye [teleō](../../strongs/g/g5055.md) the [basilikos](../../strongs/g/g937.md) [nomos](../../strongs/g/g3551.md) according to the [graphē](../../strongs/g/g1124.md), Thou shalt [agapaō](../../strongs/g/g25.md) thy [plēsion](../../strongs/g/g4139.md) as thyself, ye [poieō](../../strongs/g/g4160.md) [kalōs](../../strongs/g/g2573.md):

<a name="james_2_9"></a>James 2:9

But if ye [prosōpolēmpteō](../../strongs/g/g4380.md), ye [ergazomai](../../strongs/g/g2038.md) [hamartia](../../strongs/g/g266.md), and are [elegchō](../../strongs/g/g1651.md) of the [nomos](../../strongs/g/g3551.md) as [parabatēs](../../strongs/g/g3848.md).

<a name="james_2_10"></a>James 2:10

For whosoever shall [tēreō](../../strongs/g/g5083.md) the [holos](../../strongs/g/g3650.md) [nomos](../../strongs/g/g3551.md), and yet [ptaiō](../../strongs/g/g4417.md) in [heis](../../strongs/g/g1520.md), he is [enochos](../../strongs/g/g1777.md) of [pas](../../strongs/g/g3956.md).

<a name="james_2_11"></a>James 2:11

For he that [eipon](../../strongs/g/g2036.md), Do not [moicheuō](../../strongs/g/g3431.md), [eipon](../../strongs/g/g2036.md) also, Do not [phoneuō](../../strongs/g/g5407.md). Now if thou [moicheuō](../../strongs/g/g3431.md) no, yet if thou [phoneuō](../../strongs/g/g5407.md), thou art become a [parabatēs](../../strongs/g/g3848.md) of the [nomos](../../strongs/g/g3551.md).

<a name="james_2_12"></a>James 2:12

So [laleō](../../strongs/g/g2980.md) ye, and so [poieō](../../strongs/g/g4160.md), as they that shall be [krinō](../../strongs/g/g2919.md) by the [nomos](../../strongs/g/g3551.md) of [eleutheria](../../strongs/g/g1657.md).

<a name="james_2_13"></a>James 2:13

For he shall have [krisis](../../strongs/g/g2920.md) [anileōs](../../strongs/g/g448.md), that hath [poieō](../../strongs/g/g4160.md) no [eleos](../../strongs/g/g1656.md); and [eleos](../../strongs/g/g1656.md) [katakauchaomai](../../strongs/g/g2620.md) [krisis](../../strongs/g/g2920.md).

<a name="james_2_14"></a>James 2:14

What [ophelos](../../strongs/g/g3786.md), my [adelphos](../../strongs/g/g80.md), though [tis](../../strongs/g/g5100.md) [legō](../../strongs/g/g3004.md) he hath [pistis](../../strongs/g/g4102.md), and have not [ergon](../../strongs/g/g2041.md)? can [pistis](../../strongs/g/g4102.md) [sōzō](../../strongs/g/g4982.md) him?

<a name="james_2_15"></a>James 2:15

If an [adelphos](../../strongs/g/g80.md) or [adelphē](../../strongs/g/g79.md) be [gymnos](../../strongs/g/g1131.md), and [leipō](../../strongs/g/g3007.md) of [ephēmeros](../../strongs/g/g2184.md) [trophē](../../strongs/g/g5160.md),

<a name="james_2_16"></a>James 2:16

And [tis](../../strongs/g/g5100.md) of you [eipon](../../strongs/g/g2036.md) unto them, [hypagō](../../strongs/g/g5217.md) in [eirēnē](../../strongs/g/g1515.md), be ye [thermainō](../../strongs/g/g2328.md) and [chortazō](../../strongs/g/g5526.md); notwithstanding ye [didōmi](../../strongs/g/g1325.md) them not those [epitēdeios](../../strongs/g/g2006.md) to the [sōma](../../strongs/g/g4983.md); what [ophelos](../../strongs/g/g3786.md)?

<a name="james_2_17"></a>James 2:17

Even so [pistis](../../strongs/g/g4102.md), if it hath not [ergon](../../strongs/g/g2041.md), is [nekros](../../strongs/g/g3498.md), being alone.

<a name="james_2_18"></a>James 2:18

[alla](../../strongs/g/g235.md), a [tis](../../strongs/g/g5100.md) may [eipon](../../strongs/g/g2046.md), Thou hast [pistis](../../strongs/g/g4102.md), and I have [ergon](../../strongs/g/g2041.md): [deiknyō](../../strongs/g/g1166.md) me thy [pistis](../../strongs/g/g4102.md) without thy [ergon](../../strongs/g/g2041.md), and I will [deiknyō](../../strongs/g/g1166.md) thee my [pistis](../../strongs/g/g4102.md) by my [ergon](../../strongs/g/g2041.md).

<a name="james_2_19"></a>James 2:19

Thou [pisteuō](../../strongs/g/g4100.md) that there is one [theos](../../strongs/g/g2316.md); thou [poieō](../../strongs/g/g4160.md) [kalōs](../../strongs/g/g2573.md): the [daimonion](../../strongs/g/g1140.md) also [pisteuō](../../strongs/g/g4100.md), and [phrissō](../../strongs/g/g5425.md).

<a name="james_2_20"></a>James 2:20

But wilt thou [ginōskō](../../strongs/g/g1097.md), [ō](../../strongs/g/g5599.md) [kenos](../../strongs/g/g2756.md) [anthrōpos](../../strongs/g/g444.md), that [pistis](../../strongs/g/g4102.md) without [ergon](../../strongs/g/g2041.md) is [nekros](../../strongs/g/g3498.md)?

<a name="james_2_21"></a>James 2:21

Was not [Abraam](../../strongs/g/g11.md) our [patēr](../../strongs/g/g3962.md) [dikaioō](../../strongs/g/g1344.md) by [ergon](../../strongs/g/g2041.md), when he had [anapherō](../../strongs/g/g399.md) [Isaak](../../strongs/g/g2464.md) his [huios](../../strongs/g/g5207.md) upon the [thysiastērion](../../strongs/g/g2379.md)?

<a name="james_2_22"></a>James 2:22

[blepō](../../strongs/g/g991.md) thou how [pistis](../../strongs/g/g4102.md) [synergeō](../../strongs/g/g4903.md) with his [ergon](../../strongs/g/g2041.md), and by [ergon](../../strongs/g/g2041.md) was [pistis](../../strongs/g/g4102.md) [teleioō](../../strongs/g/g5048.md)?

<a name="james_2_23"></a>James 2:23

And the [graphē](../../strongs/g/g1124.md) was [plēroō](../../strongs/g/g4137.md) which [legō](../../strongs/g/g3004.md), [Abraam](../../strongs/g/g11.md) [pisteuō](../../strongs/g/g4100.md) [theos](../../strongs/g/g2316.md), and it was [logizomai](../../strongs/g/g3049.md) unto him for [dikaiosynē](../../strongs/g/g1343.md): and he was [kaleō](../../strongs/g/g2564.md) the [philos](../../strongs/g/g5384.md) of [theos](../../strongs/g/g2316.md).

<a name="james_2_24"></a>James 2:24

Ye [horaō](../../strongs/g/g3708.md) then how that by [ergon](../../strongs/g/g2041.md) an [anthrōpos](../../strongs/g/g444.md) is [dikaioō](../../strongs/g/g1344.md), and not by [pistis](../../strongs/g/g4102.md) only.

<a name="james_2_25"></a>James 2:25

[homoiōs](../../strongs/g/g3668.md) also was not [rhaab](../../strongs/g/g4460.md) the [pornē](../../strongs/g/g4204.md) [dikaioō](../../strongs/g/g1344.md) by [ergon](../../strongs/g/g2041.md), when she had [hypodechomai](../../strongs/g/g5264.md) the [aggelos](../../strongs/g/g32.md), and had [ekballō](../../strongs/g/g1544.md) another [hodos](../../strongs/g/g3598.md)?

<a name="james_2_26"></a>James 2:26

For as the [sōma](../../strongs/g/g4983.md) without the [pneuma](../../strongs/g/g4151.md) is [nekros](../../strongs/g/g3498.md), so [pistis](../../strongs/g/g4102.md) without [ergon](../../strongs/g/g2041.md) is [nekros](../../strongs/g/g3498.md) also.

---

[Transliteral Bible](../bible.md)

[James](james.md)

[James 1](james_1.md) - [James 3](james_3.md)