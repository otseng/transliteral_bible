# [James 3](https://www.blueletterbible.org/kjv/jas/3/1/s_1149001)

<a name="james_3_1"></a>James 3:1

My [adelphos](../../strongs/g/g80.md), be not [polys](../../strongs/g/g4183.md) [didaskalos](../../strongs/g/g1320.md), [eidō](../../strongs/g/g1492.md) that we shall [lambanō](../../strongs/g/g2983.md) the [meizōn](../../strongs/g/g3187.md) [krima](../../strongs/g/g2917.md).

<a name="james_3_2"></a>James 3:2

For in [polys](../../strongs/g/g4183.md) we [ptaiō](../../strongs/g/g4417.md) all. [ei tis](../../strongs/g/g1536.md) [ptaiō](../../strongs/g/g4417.md) not in [logos](../../strongs/g/g3056.md), the same a [teleios](../../strongs/g/g5046.md) [anēr](../../strongs/g/g435.md), [dynatos](../../strongs/g/g1415.md) also to [chalinagōgeō](../../strongs/g/g5468.md) the [holos](../../strongs/g/g3650.md) [sōma](../../strongs/g/g4983.md).

<a name="james_3_3"></a>James 3:3

[idou](../../strongs/g/g2400.md), we [ballō](../../strongs/g/g906.md) [chalinos](../../strongs/g/g5469.md) in the [hippos](../../strongs/g/g2462.md) [stoma](../../strongs/g/g4750.md), that they may [peithō](../../strongs/g/g3982.md) us; and we [metagō](../../strongs/g/g3329.md) their [holos](../../strongs/g/g3650.md) [sōma](../../strongs/g/g4983.md).

<a name="james_3_4"></a>James 3:4

[idou](../../strongs/g/g2400.md) also the [ploion](../../strongs/g/g4143.md), which though [tēlikoutos](../../strongs/g/g5082.md), and are [elaunō](../../strongs/g/g1643.md) of [sklēros](../../strongs/g/g4642.md) [anemos](../../strongs/g/g417.md), yet they [metagō](../../strongs/g/g3329.md) with an [elachistos](../../strongs/g/g1646.md) [pēdalion](../../strongs/g/g4079.md), whithersoever the [euthynō](../../strongs/g/g2116.md) [hormē](../../strongs/g/g3730.md) [boulomai](../../strongs/g/g1014.md).

<a name="james_3_5"></a>James 3:5

Even so the [glōssa](../../strongs/g/g1100.md) is a [mikros](../../strongs/g/g3398.md) [melos](../../strongs/g/g3196.md), and [aucheō](../../strongs/g/g3166.md). [idou](../../strongs/g/g2400.md), [hēlikos](../../strongs/g/g2245.md) a [hylē](../../strongs/g/g5208.md) a [oligos](../../strongs/g/g3641.md) [pyr](../../strongs/g/g4442.md) [anaptō](../../strongs/g/g381.md)!

<a name="james_3_6"></a>James 3:6

And the [glōssa](../../strongs/g/g1100.md) a [pyr](../../strongs/g/g4442.md), a [kosmos](../../strongs/g/g2889.md) of [adikia](../../strongs/g/g93.md): so [kathistēmi](../../strongs/g/g2525.md) the [glōssa](../../strongs/g/g1100.md) among our [melos](../../strongs/g/g3196.md), that it [spiloō](../../strongs/g/g4695.md) the whole [sōma](../../strongs/g/g4983.md), and [phlogizō](../../strongs/g/g5394.md) the [trochos](../../strongs/g/g5164.md) of [genesis](../../strongs/g/g1078.md); and it is [phlogizō](../../strongs/g/g5394.md) of [geenna](../../strongs/g/g1067.md).

<a name="james_3_7"></a>James 3:7

For every [physis](../../strongs/g/g5449.md) of [thērion](../../strongs/g/g2342.md), and of [peteinon](../../strongs/g/g4071.md), and of [herpeton](../../strongs/g/g2062.md), and of [enalios](../../strongs/g/g1724.md), is [damazō](../../strongs/g/g1150.md), and hath been [damazō](../../strongs/g/g1150.md) of [physis](../../strongs/g/g5449.md) [anthrōpinos](../../strongs/g/g442.md):

<a name="james_3_8"></a>James 3:8

But the [glōssa](../../strongs/g/g1100.md) can no [anthrōpos](../../strongs/g/g444.md) [damazō](../../strongs/g/g1150.md); an [akataschetos](../../strongs/g/g183.md) [kakos](../../strongs/g/g2556.md), [mestos](../../strongs/g/g3324.md) of [thanatēphoros](../../strongs/g/g2287.md) [ios](../../strongs/g/g2447.md).

<a name="james_3_9"></a>James 3:9

Therewith [eulogeō](../../strongs/g/g2127.md) we [theos](../../strongs/g/g2316.md), even the [patēr](../../strongs/g/g3962.md); and therewith [kataraomai](../../strongs/g/g2672.md) we [anthrōpos](../../strongs/g/g444.md), which are [ginomai](../../strongs/g/g1096.md) after the [homoiōsis](../../strongs/g/g3669.md) of [theos](../../strongs/g/g2316.md).

<a name="james_3_10"></a>James 3:10

Out of the same [stoma](../../strongs/g/g4750.md) [exerchomai](../../strongs/g/g1831.md) [eulogia](../../strongs/g/g2129.md) and [katara](../../strongs/g/g2671.md). My [adelphos](../../strongs/g/g80.md), these things [chrē](../../strongs/g/g5534.md) not so [ginomai](../../strongs/g/g1096.md).

<a name="james_3_11"></a>James 3:11

Doth a [pēgē](../../strongs/g/g4077.md) [bryō](../../strongs/g/g1032.md) at the same [opē](../../strongs/g/g3692.md) [glykys](../../strongs/g/g1099.md) and [pikros](../../strongs/g/g4089.md)?

<a name="james_3_12"></a>James 3:12

Can the [sykē](../../strongs/g/g4808.md), my [adelphos](../../strongs/g/g80.md), [poieō](../../strongs/g/g4160.md) [elaia](../../strongs/g/g1636.md)? either an [ampelos](../../strongs/g/g288.md), [sykon](../../strongs/g/g4810.md)? so no [pēgē](../../strongs/g/g4077.md) both yield [halykos](../../strongs/g/g252.md) [hydōr](../../strongs/g/g5204.md) and [glykys](../../strongs/g/g1099.md).

<a name="james_3_13"></a>James 3:13

Who [sophos](../../strongs/g/g4680.md) and [epistēmōn](../../strongs/g/g1990.md) among you? let him [deiknyō](../../strongs/g/g1166.md) out of a [kalos](../../strongs/g/g2570.md) [anastrophē](../../strongs/g/g391.md) his [ergon](../../strongs/g/g2041.md) with [praÿtēs](../../strongs/g/g4240.md) of [sophia](../../strongs/g/g4678.md).

<a name="james_3_14"></a>James 3:14

But if ye have [pikros](../../strongs/g/g4089.md) [zēlos](../../strongs/g/g2205.md) and [eritheia](../../strongs/g/g2052.md) in your [kardia](../../strongs/g/g2588.md), [katakauchaomai](../../strongs/g/g2620.md) not, and [pseudomai](../../strongs/g/g5574.md) not against the [alētheia](../../strongs/g/g225.md).

<a name="james_3_15"></a>James 3:15

This [sophia](../../strongs/g/g4678.md) [katerchomai](../../strongs/g/g2718.md) not from [anōthen](../../strongs/g/g509.md), but [epigeios](../../strongs/g/g1919.md), [psychikos](../../strongs/g/g5591.md), [daimoniōdēs](../../strongs/g/g1141.md).

<a name="james_3_16"></a>James 3:16

For where [zēlos](../../strongs/g/g2205.md) and [eritheia](../../strongs/g/g2052.md), there [akatastasia](../../strongs/g/g181.md) and every [phaulos](../../strongs/g/g5337.md) [pragma](../../strongs/g/g4229.md).

<a name="james_3_17"></a>James 3:17

But the [sophia](../../strongs/g/g4678.md) that is from [anōthen](../../strongs/g/g509.md) is [prōton](../../strongs/g/g4412.md) [men](../../strongs/g/g3303.md) [hagnos](../../strongs/g/g53.md), then [eirēnikos](../../strongs/g/g1516.md), [epieikēs](../../strongs/g/g1933.md), [eupeithēs](../../strongs/g/g2138.md), [mestos](../../strongs/g/g3324.md) of [eleos](../../strongs/g/g1656.md) and [agathos](../../strongs/g/g18.md) [karpos](../../strongs/g/g2590.md), [adiakritos](../../strongs/g/g87.md), and [anypokritos](../../strongs/g/g505.md).

<a name="james_3_18"></a>James 3:18

And the [karpos](../../strongs/g/g2590.md) of [dikaiosynē](../../strongs/g/g1343.md) is [speirō](../../strongs/g/g4687.md) in [eirēnē](../../strongs/g/g1515.md) of them that [poieō](../../strongs/g/g4160.md) [eirēnē](../../strongs/g/g1515.md).

---

[Transliteral Bible](../bible.md)

[James](james.md)

[James 2](james_2.md) - [James 4](james_4.md)