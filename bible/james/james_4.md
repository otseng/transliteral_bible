# [James 4](https://www.blueletterbible.org/kjv/jas/4/1/s_1150001)

<a name="james_4_1"></a>James 4:1

From whence [polemos](../../strongs/g/g4171.md) and [machē](../../strongs/g/g3163.md) among you? not hence, of your [hēdonē](../../strongs/g/g2237.md) that [strateuō](../../strongs/g/g4754.md) in your [melos](../../strongs/g/g3196.md)?

<a name="james_4_2"></a>James 4:2

Ye [epithymeō](../../strongs/g/g1937.md), and have not: ye [phoneuō](../../strongs/g/g5407.md), and [zēloō](../../strongs/g/g2206.md), and cannot [epitygchanō](../../strongs/g/g2013.md): ye [machomai](../../strongs/g/g3164.md) and [polemeō](../../strongs/g/g4170.md), yet ye have not, because ye [aiteō](../../strongs/g/g154.md) not.

<a name="james_4_3"></a>James 4:3

Ye [aiteō](../../strongs/g/g154.md), and [lambanō](../../strongs/g/g2983.md) not, because ye [aiteō](../../strongs/g/g154.md) [kakōs](../../strongs/g/g2560.md), that ye may [dapanaō](../../strongs/g/g1159.md) upon your [hēdonē](../../strongs/g/g2237.md).

<a name="james_4_4"></a>James 4:4

Ye [moichos](../../strongs/g/g3432.md) and [moichalis](../../strongs/g/g3428.md), [eidō](../../strongs/g/g1492.md) ye not that the [philia](../../strongs/g/g5373.md) of the world is [echthra](../../strongs/g/g2189.md) with [theos](../../strongs/g/g2316.md)? whosoever therefore [boulomai](../../strongs/g/g1014.md) a [philos](../../strongs/g/g5384.md) of the [kosmos](../../strongs/g/g2889.md) [kathistēmi](../../strongs/g/g2525.md) the [echthros](../../strongs/g/g2190.md) of [theos](../../strongs/g/g2316.md).

<a name="james_4_5"></a>James 4:5

Do ye [dokeō](../../strongs/g/g1380.md) that the [graphē](../../strongs/g/g1124.md) [legō](../../strongs/g/g3004.md) in [kenōs](../../strongs/g/g2761.md), The [pneuma](../../strongs/g/g4151.md) that [katoikeō](../../strongs/g/g2730.md) in us [epipotheo](../../strongs/g/g1971.md) to [phthonos](../../strongs/g/g5355.md)?

<a name="james_4_6"></a>James 4:6

But he [didōmi](../../strongs/g/g1325.md) [meizōn](../../strongs/g/g3187.md) [charis](../../strongs/g/g5485.md). Wherefore he [legō](../../strongs/g/g3004.md), [theos](../../strongs/g/g2316.md) [antitassō](../../strongs/g/g498.md) the [hyperēphanos](../../strongs/g/g5244.md), but [didōmi](../../strongs/g/g1325.md) [charis](../../strongs/g/g5485.md) unto the [tapeinos](../../strongs/g/g5011.md).

<a name="james_4_7"></a>James 4:7

[hypotassō](../../strongs/g/g5293.md) therefore to [theos](../../strongs/g/g2316.md). [anthistēmi](../../strongs/g/g436.md) the [diabolos](../../strongs/g/g1228.md), and he will [pheugō](../../strongs/g/g5343.md) from you.

<a name="james_4_8"></a>James 4:8

[eggizō](../../strongs/g/g1448.md) to [theos](../../strongs/g/g2316.md), and he will [eggizō](../../strongs/g/g1448.md) to you. [katharizō](../../strongs/g/g2511.md) [cheir](../../strongs/g/g5495.md), ye [hamartōlos](../../strongs/g/g268.md); and [hagnizō](../../strongs/g/g48.md) [kardia](../../strongs/g/g2588.md), [dipsychos](../../strongs/g/g1374.md).

<a name="james_4_9"></a>James 4:9

[talaipōreō](../../strongs/g/g5003.md), and [pentheō](../../strongs/g/g3996.md), and [klaiō](../../strongs/g/g2799.md): let your [gelōs](../../strongs/g/g1071.md) be [metastrephō](../../strongs/g/g3344.md) to [penthos](../../strongs/g/g3997.md), and [chara](../../strongs/g/g5479.md) to [katēpheia](../../strongs/g/g2726.md).

<a name="james_4_10"></a>James 4:10

[tapeinoō](../../strongs/g/g5013.md) in the [enōpion](../../strongs/g/g1799.md) of the [kyrios](../../strongs/g/g2962.md), and he shall [hypsoō](../../strongs/g/g5312.md) you up.

<a name="james_4_11"></a>James 4:11

[katalaleō](../../strongs/g/g2635.md) not of [allēlōn](../../strongs/g/g240.md), [adelphos](../../strongs/g/g80.md). He that [katalaleō](../../strongs/g/g2635.md) of [adelphos](../../strongs/g/g80.md), and [krinō](../../strongs/g/g2919.md) his [adelphos](../../strongs/g/g80.md), [katalaleō](../../strongs/g/g2635.md) of the [nomos](../../strongs/g/g3551.md), and [krinō](../../strongs/g/g2919.md) the [nomos](../../strongs/g/g3551.md): but if thou [krinō](../../strongs/g/g2919.md) the [nomos](../../strongs/g/g3551.md), thou art not a [poiētēs](../../strongs/g/g4163.md) of the [nomos](../../strongs/g/g3551.md), but a [kritēs](../../strongs/g/g2923.md).

<a name="james_4_12"></a>James 4:12

There is one [nomothetēs](../../strongs/g/g3550.md), who is able to [sōzō](../../strongs/g/g4982.md) and to [apollymi](../../strongs/g/g622.md): who art thou that [krinō](../../strongs/g/g2919.md) another?

<a name="james_4_13"></a>James 4:13

[age](../../strongs/g/g33.md) now, ye that [legō](../../strongs/g/g3004.md), [sēmeron](../../strongs/g/g4594.md) or [aurion](../../strongs/g/g839.md) we will [poreuō](../../strongs/g/g4198.md) into such a [polis](../../strongs/g/g4172.md), and [poieō](../../strongs/g/g4160.md) there an [eniautos](../../strongs/g/g1763.md), and [emporeuomai](../../strongs/g/g1710.md), and [kerdainō](../../strongs/g/g2770.md):

<a name="james_4_14"></a>James 4:14

Whereas ye [epistamai](../../strongs/g/g1987.md) not what on [aurion](../../strongs/g/g839.md). For [poios](../../strongs/g/g4169.md) your [zōē](../../strongs/g/g2222.md)? It is even an [atmis](../../strongs/g/g822.md), that [phainō](../../strongs/g/g5316.md) for [oligos](../../strongs/g/g3641.md), and then [aphanizō](../../strongs/g/g853.md).

<a name="james_4_15"></a>James 4:15

For that ye to [legō](../../strongs/g/g3004.md), If the [kyrios](../../strongs/g/g2962.md) [thelō](../../strongs/g/g2309.md), we shall [zaō](../../strongs/g/g2198.md), and [poieō](../../strongs/g/g4160.md) this, or that.

<a name="james_4_16"></a>James 4:16

But now ye [kauchaomai](../../strongs/g/g2744.md) in your [alazoneia](../../strongs/g/g212.md): all such [kauchēsis](../../strongs/g/g2746.md) is [ponēros](../../strongs/g/g4190.md).

<a name="james_4_17"></a>James 4:17

Therefore to him that [eidō](../../strongs/g/g1492.md) to do [kalos](../../strongs/g/g2570.md), and [poieō](../../strongs/g/g4160.md) not, to him it is [hamartia](../../strongs/g/g266.md).

---

[Transliteral Bible](../bible.md)

[James](james.md)

[James](james_3.md) - [James](james_5.md)