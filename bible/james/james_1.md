# [James 1](https://www.blueletterbible.org/kjv/jas/1/1/s_1147001)

<a name="james_1_1"></a>James 1:1

[Iakōbos](../../strongs/g/g2385.md), a [doulos](../../strongs/g/g1401.md) of [theos](../../strongs/g/g2316.md) and of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), to the twelve [phylē](../../strongs/g/g5443.md) which are [diaspora](../../strongs/g/g1290.md), [chairō](../../strongs/g/g5463.md).

<a name="james_1_2"></a>James 1:2

My [adelphos](../../strongs/g/g80.md), [hēgeomai](../../strongs/g/g2233.md) it all [chara](../../strongs/g/g5479.md) when ye [peripiptō](../../strongs/g/g4045.md) into [poikilos](../../strongs/g/g4164.md) [peirasmos](../../strongs/g/g3986.md);

<a name="james_1_3"></a>James 1:3

[ginōskō](../../strongs/g/g1097.md), that the [dokimion](../../strongs/g/g1383.md) of your [pistis](../../strongs/g/g4102.md) [katergazomai](../../strongs/g/g2716.md) [hypomonē](../../strongs/g/g5281.md).

<a name="james_1_4"></a>James 1:4

But let [hypomonē](../../strongs/g/g5281.md) have [teleios](../../strongs/g/g5046.md) [ergon](../../strongs/g/g2041.md), that ye may be [teleios](../../strongs/g/g5046.md) and [holoklēros](../../strongs/g/g3648.md), [leipō](../../strongs/g/g3007.md) [mēdeis](../../strongs/g/g3367.md).

<a name="james_1_5"></a>James 1:5

If any of you [leipō](../../strongs/g/g3007.md) [sophia](../../strongs/g/g4678.md), let him [aiteō](../../strongs/g/g154.md) of [theos](../../strongs/g/g2316.md), that [didōmi](../../strongs/g/g1325.md) to all [haplōs](../../strongs/g/g574.md), and [oneidizō](../../strongs/g/g3679.md) not; and it shall be [didōmi](../../strongs/g/g1325.md) him.

<a name="james_1_6"></a>James 1:6

But let him [aiteō](../../strongs/g/g154.md) in [pistis](../../strongs/g/g4102.md), [mēdeis](../../strongs/g/g3367.md) [diakrinō](../../strongs/g/g1252.md). For he that [diakrinō](../../strongs/g/g1252.md) is [eikō](../../strongs/g/g1503.md) a [klydōn](../../strongs/g/g2830.md) of the [thalassa](../../strongs/g/g2281.md) driven with the [anemizō](../../strongs/g/g416.md) and [rhipizō](../../strongs/g/g4494.md).

<a name="james_1_7"></a>James 1:7

For let not that [anthrōpos](../../strongs/g/g444.md) [oiomai](../../strongs/g/g3633.md) that he shall [lambanō](../../strongs/g/g2983.md) any thing of the [kyrios](../../strongs/g/g2962.md).

<a name="james_1_8"></a>James 1:8

A [dipsychos](../../strongs/g/g1374.md) [anēr](../../strongs/g/g435.md) [akatastatos](../../strongs/g/g182.md) in all his [hodos](../../strongs/g/g3598.md).

<a name="james_1_9"></a>James 1:9

Let the [adelphos](../../strongs/g/g80.md) of [tapeinos](../../strongs/g/g5011.md) [kauchaomai](../../strongs/g/g2744.md) in that he is [hypsos](../../strongs/g/g5311.md):

<a name="james_1_10"></a>James 1:10

But the [plousios](../../strongs/g/g4145.md), in that he is [tapeinōsis](../../strongs/g/g5014.md): because as the [anthos](../../strongs/g/g438.md) of the [chortos](../../strongs/g/g5528.md) he shall [parerchomai](../../strongs/g/g3928.md).

<a name="james_1_11"></a>James 1:11

For the [hēlios](../../strongs/g/g2246.md) is [anatellō](../../strongs/g/g393.md) with a [kausōn](../../strongs/g/g2742.md), but it [xērainō](../../strongs/g/g3583.md) the [chortos](../../strongs/g/g5528.md), and the [anthos](../../strongs/g/g438.md) thereof [ekpiptō](../../strongs/g/g1601.md), and the [euprepeia](../../strongs/g/g2143.md) of the [prosōpon](../../strongs/g/g4383.md) of it [apollymi](../../strongs/g/g622.md): so also shall the [plousios](../../strongs/g/g4145.md) [marainō](../../strongs/g/g3133.md) in his [poreia](../../strongs/g/g4197.md).

<a name="james_1_12"></a>James 1:12

[makarios](../../strongs/g/g3107.md) the [anēr](../../strongs/g/g435.md) that [hypomenō](../../strongs/g/g5278.md) [peirasmos](../../strongs/g/g3986.md): for when he is [dokimos](../../strongs/g/g1384.md), he shall [lambanō](../../strongs/g/g2983.md) the [stephanos](../../strongs/g/g4735.md) of [zōē](../../strongs/g/g2222.md), which the [kyrios](../../strongs/g/g2962.md) hath [epaggellomai](../../strongs/g/g1861.md) to them that [agapaō](../../strongs/g/g25.md) him.

<a name="james_1_13"></a>James 1:13

Let [mēdeis](../../strongs/g/g3367.md) [legō](../../strongs/g/g3004.md) when he is [peirazō](../../strongs/g/g3985.md), I am [peirazō](../../strongs/g/g3985.md) of [theos](../../strongs/g/g2316.md): for [theos](../../strongs/g/g2316.md) cannot be [apeirastos](../../strongs/g/g551.md) with [kakos](../../strongs/g/g2556.md), neither [peirazō](../../strongs/g/g3985.md) he [oudeis](../../strongs/g/g3762.md):

<a name="james_1_14"></a>James 1:14

But [hekastos](../../strongs/g/g1538.md) is [peirazō](../../strongs/g/g3985.md), when he is [exelkō](../../strongs/g/g1828.md) of his own [epithymia](../../strongs/g/g1939.md), and [deleazō](../../strongs/g/g1185.md).

<a name="james_1_15"></a>James 1:15

Then when [epithymia](../../strongs/g/g1939.md) hath [syllambanō](../../strongs/g/g4815.md), it [tiktō](../../strongs/g/g5088.md) [hamartia](../../strongs/g/g266.md): and [hamartia](../../strongs/g/g266.md), when it is [apoteleō](../../strongs/g/g658.md), [apokyeō](../../strongs/g/g616.md) [thanatos](../../strongs/g/g2288.md).

<a name="james_1_16"></a>James 1:16

Do not [planaō](../../strongs/g/g4105.md), my [agapētos](../../strongs/g/g27.md) [adelphos](../../strongs/g/g80.md).

<a name="james_1_17"></a>James 1:17

Every [agathos](../../strongs/g/g18.md) [dosis](../../strongs/g/g1394.md) and every [teleios](../../strongs/g/g5046.md) [dōrēma](../../strongs/g/g1434.md) is from [anōthen](../../strongs/g/g509.md), and [katabainō](../../strongs/g/g2597.md) from the [patēr](../../strongs/g/g3962.md) of [phōs](../../strongs/g/g5457.md), with whom is no [parallagē](../../strongs/g/g3883.md), neither [aposkiasma](../../strongs/g/g644.md) of [tropē](../../strongs/g/g5157.md).

<a name="james_1_18"></a>James 1:18

Of his own [boulomai](../../strongs/g/g1014.md) [apokyeō](../../strongs/g/g616.md) he us with the [logos](../../strongs/g/g3056.md) of [alētheia](../../strongs/g/g225.md), that we should be a kind of [aparchē](../../strongs/g/g536.md) of his [ktisma](../../strongs/g/g2938.md).

<a name="james_1_19"></a>James 1:19

Wherefore, my [agapētos](../../strongs/g/g27.md) [adelphos](../../strongs/g/g80.md), let every [anthrōpos](../../strongs/g/g444.md) be [tachys](../../strongs/g/g5036.md) to [akouō](../../strongs/g/g191.md), [bradys](../../strongs/g/g1021.md) to [laleō](../../strongs/g/g2980.md), [bradys](../../strongs/g/g1021.md) to [orgē](../../strongs/g/g3709.md):

<a name="james_1_20"></a>James 1:20

For the [orgē](../../strongs/g/g3709.md) of [anēr](../../strongs/g/g435.md) [katergazomai](../../strongs/g/g2716.md) not the [dikaiosynē](../../strongs/g/g1343.md) of [theos](../../strongs/g/g2316.md).

<a name="james_1_21"></a>James 1:21

Wherefore [apotithēmi](../../strongs/g/g659.md) all [rhyparia](../../strongs/g/g4507.md) and [perisseia](../../strongs/g/g4050.md) of [kakia](../../strongs/g/g2549.md), and [dechomai](../../strongs/g/g1209.md) with [praÿtēs](../../strongs/g/g4240.md) the [emphytos](../../strongs/g/g1721.md) [logos](../../strongs/g/g3056.md), which is able to [sōzō](../../strongs/g/g4982.md) your [psychē](../../strongs/g/g5590.md).

<a name="james_1_22"></a>James 1:22

But be ye [poiētēs](../../strongs/g/g4163.md) of the [logos](../../strongs/g/g3056.md), and not [akroatēs](../../strongs/g/g202.md) only, [paralogizomai](../../strongs/g/g3884.md) your own selves.

<a name="james_1_23"></a>James 1:23

For if any be an [akroatēs](../../strongs/g/g202.md) of the [logos](../../strongs/g/g3056.md), and not a [poiētēs](../../strongs/g/g4163.md), he is [eikō](../../strongs/g/g1503.md) unto an [anēr](../../strongs/g/g435.md) [katanoeō](../../strongs/g/g2657.md) his [genesis](../../strongs/g/g1078.md) [prosōpon](../../strongs/g/g4383.md) in an [esoptron](../../strongs/g/g2072.md):

<a name="james_1_24"></a>James 1:24

For he [katanoeō](../../strongs/g/g2657.md) himself, and [aperchomai](../../strongs/g/g565.md), and [eutheōs](../../strongs/g/g2112.md) [epilanthanomai](../../strongs/g/g1950.md) [hopoios](../../strongs/g/g3697.md) he was.

<a name="james_1_25"></a>James 1:25

But whoso [parakyptō](../../strongs/g/g3879.md) into the [teleios](../../strongs/g/g5046.md) [nomos](../../strongs/g/g3551.md) of [eleutheria](../../strongs/g/g1657.md), and [paramenō](../../strongs/g/g3887.md), he being not an [epilēsmonē](../../strongs/g/g1953.md) [akroatēs](../../strongs/g/g202.md), but a [poiētēs](../../strongs/g/g4163.md) of the [ergon](../../strongs/g/g2041.md), [houtos](../../strongs/g/g3778.md) shall be [makarios](../../strongs/g/g3107.md) in his [poiēsis](../../strongs/g/g4162.md).

<a name="james_1_26"></a>James 1:26

[ei tis](../../strongs/g/g1536.md) among you [dokeō](../../strongs/g/g1380.md) to be [thrēskos](../../strongs/g/g2357.md), and [chalinagōgeō](../../strongs/g/g5468.md) not his [glōssa](../../strongs/g/g1100.md), but [apataō](../../strongs/g/g538.md) his own [kardia](../../strongs/g/g2588.md), [toutou](../../strongs/g/g5127.md) [thrēskeia](../../strongs/g/g2356.md) [mataios](../../strongs/g/g3152.md).

<a name="james_1_27"></a>James 1:27

[katharos](../../strongs/g/g2513.md) [thrēskeia](../../strongs/g/g2356.md) and [amiantos](../../strongs/g/g283.md) before [theos](../../strongs/g/g2316.md) and the [patēr](../../strongs/g/g3962.md) is this, To [episkeptomai](../../strongs/g/g1980.md) the [orphanos](../../strongs/g/g3737.md) and [chēra](../../strongs/g/g5503.md) in their [thlipsis](../../strongs/g/g2347.md), to [tēreō](../../strongs/g/g5083.md) himself [aspilos](../../strongs/g/g784.md) from the [kosmos](../../strongs/g/g2889.md).

---

[Transliteral Bible](../bible.md)

[James](james.md)

[James 2](james_2.md)