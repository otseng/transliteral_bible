# [Galatians 3](https://www.blueletterbible.org/kjv/gal/3/1/s_1094001)

<a name="galatians_3_1"></a>Galatians 3:1

[ō](../../strongs/g/g5599.md) [anoētos](../../strongs/g/g453.md) [galatēs](../../strongs/g/g1052.md), who hath [baskainō](../../strongs/g/g940.md) you, that ye should not [peithō](../../strongs/g/g3982.md) the [alētheia](../../strongs/g/g225.md), before whose [ophthalmos](../../strongs/g/g3788.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) hath been [prographō](../../strongs/g/g4270.md), [stauroō](../../strongs/g/g4717.md) among you? [^1]

<a name="galatians_3_2"></a>Galatians 3:2

This only would I [manthanō](../../strongs/g/g3129.md) of you, [lambanō](../../strongs/g/g2983.md) ye the [pneuma](../../strongs/g/g4151.md) by the [ergon](../../strongs/g/g2041.md) of the [nomos](../../strongs/g/g3551.md), or by the [akoē](../../strongs/g/g189.md) of [pistis](../../strongs/g/g4102.md)?

<a name="galatians_3_3"></a>Galatians 3:3

Are ye so [anoētos](../../strongs/g/g453.md)? having [enarchomai](../../strongs/g/g1728.md) in the [pneuma](../../strongs/g/g4151.md), are ye now [epiteleō](../../strongs/g/g2005.md) by the [sarx](../../strongs/g/g4561.md)?

<a name="galatians_3_4"></a>Galatians 3:4

Have ye [paschō](../../strongs/g/g3958.md) [tosoutos](../../strongs/g/g5118.md) in [eikē](../../strongs/g/g1500.md)? if it be yet in [eikē](../../strongs/g/g1500.md).

<a name="galatians_3_5"></a>Galatians 3:5

He therefore that [epichorēgeō](../../strongs/g/g2023.md) to you the [pneuma](../../strongs/g/g4151.md), and [energeō](../../strongs/g/g1754.md) [dynamis](../../strongs/g/g1411.md) among you, by the [ergon](../../strongs/g/g2041.md) of the [nomos](../../strongs/g/g3551.md), or by the [akoē](../../strongs/g/g189.md) of [pistis](../../strongs/g/g4102.md)?

<a name="galatians_3_6"></a>Galatians 3:6

Even as [Abraam](../../strongs/g/g11.md) [pisteuō](../../strongs/g/g4100.md) [theos](../../strongs/g/g2316.md), and it was [logizomai](../../strongs/g/g3049.md) to him for [dikaiosynē](../../strongs/g/g1343.md).

<a name="galatians_3_7"></a>Galatians 3:7

[ginōskō](../../strongs/g/g1097.md) ye therefore that they which are of [pistis](../../strongs/g/g4102.md), the same are the [huios](../../strongs/g/g5207.md) of [Abraam](../../strongs/g/g11.md).

<a name="galatians_3_8"></a>Galatians 3:8

And the [graphē](../../strongs/g/g1124.md), [prooraō](../../strongs/g/g4275.md) that [theos](../../strongs/g/g2316.md) would [dikaioō](../../strongs/g/g1344.md) the [ethnos](../../strongs/g/g1484.md) through [pistis](../../strongs/g/g4102.md), [proeuangelizomai](../../strongs/g/g4283.md) unto [Abraam](../../strongs/g/g11.md), saying, In thee shall all [ethnos](../../strongs/g/g1484.md) be [eneulogeō](../../strongs/g/g1757.md).

<a name="galatians_3_9"></a>Galatians 3:9

So then they which be of [pistis](../../strongs/g/g4102.md) are [eulogeō](../../strongs/g/g2127.md) with [pistos](../../strongs/g/g4103.md) [Abraam](../../strongs/g/g11.md).

<a name="galatians_3_10"></a>Galatians 3:10

For as many as are of the [ergon](../../strongs/g/g2041.md) of the [nomos](../../strongs/g/g3551.md) are under the [katara](../../strongs/g/g2671.md): for it is [graphō](../../strongs/g/g1125.md), [epikataratos](../../strongs/g/g1944.md) is every one that [emmenō](../../strongs/g/g1696.md) not in all things which are [graphō](../../strongs/g/g1125.md) in the [biblion](../../strongs/g/g975.md) of the [nomos](../../strongs/g/g3551.md) to [poieō](../../strongs/g/g4160.md) them.

<a name="galatians_3_11"></a>Galatians 3:11

But that no man is [dikaioō](../../strongs/g/g1344.md) by the [nomos](../../strongs/g/g3551.md) in the [para](../../strongs/g/g3844.md) [theos](../../strongs/g/g2316.md), [dēlos](../../strongs/g/g1212.md): for, The [dikaios](../../strongs/g/g1342.md) shall [zaō](../../strongs/g/g2198.md) by [pistis](../../strongs/g/g4102.md).

<a name="galatians_3_12"></a>Galatians 3:12

And the [nomos](../../strongs/g/g3551.md) is not of [pistis](../../strongs/g/g4102.md): but, The [anthrōpos](../../strongs/g/g444.md) that [poieō](../../strongs/g/g4160.md) them shall [zaō](../../strongs/g/g2198.md) in them.

<a name="galatians_3_13"></a>Galatians 3:13

[Christos](../../strongs/g/g5547.md) hath [exagorazō](../../strongs/g/g1805.md) us from the [katara](../../strongs/g/g2671.md) of the [nomos](../../strongs/g/g3551.md), being [ginomai](../../strongs/g/g1096.md) a [katara](../../strongs/g/g2671.md) for us: for it is [graphō](../../strongs/g/g1125.md), [epikataratos](../../strongs/g/g1944.md) is every one that [kremannymi](../../strongs/g/g2910.md) on a [xylon](../../strongs/g/g3586.md):

<a name="galatians_3_14"></a>Galatians 3:14

That the [eulogia](../../strongs/g/g2129.md) of [Abraam](../../strongs/g/g11.md) might [ginomai](../../strongs/g/g1096.md) on the [ethnos](../../strongs/g/g1484.md) through [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md); that we might [lambanō](../../strongs/g/g2983.md) the [epaggelia](../../strongs/g/g1860.md) of the [pneuma](../../strongs/g/g4151.md) through [pistis](../../strongs/g/g4102.md).

<a name="galatians_3_15"></a>Galatians 3:15

[adelphos](../../strongs/g/g80.md), I [legō](../../strongs/g/g3004.md) after the manner of [anthrōpos](../../strongs/g/g444.md); Though it be but an [anthrōpos](../../strongs/g/g444.md) [diathēkē](../../strongs/g/g1242.md), yet if it be [kyroō](../../strongs/g/g2964.md), [oudeis](../../strongs/g/g3762.md) [atheteō](../../strongs/g/g114.md), or [epidiatassomai](../../strongs/g/g1928.md).

<a name="galatians_3_16"></a>Galatians 3:16

Now to [Abraam](../../strongs/g/g11.md) and his [sperma](../../strongs/g/g4690.md) were the [epaggelia](../../strongs/g/g1860.md) [rheō](../../strongs/g/g4483.md). He [legō](../../strongs/g/g3004.md) not, And to [sperma](../../strongs/g/g4690.md), as of [polys](../../strongs/g/g4183.md); but as of one, And to thy [sperma](../../strongs/g/g4690.md), which is [Christos](../../strongs/g/g5547.md).

<a name="galatians_3_17"></a>Galatians 3:17

And this I [legō](../../strongs/g/g3004.md), that the [diathēkē](../../strongs/g/g1242.md), that was [prokyroō](../../strongs/g/g4300.md) of [theos](../../strongs/g/g2316.md) in [Christos](../../strongs/g/g5547.md), the [nomos](../../strongs/g/g3551.md), which was four hundred and thirty [etos](../../strongs/g/g2094.md) after, cannot [akyroō](../../strongs/g/g208.md), that it should [katargeō](../../strongs/g/g2673.md) the [epaggelia](../../strongs/g/g1860.md).

<a name="galatians_3_18"></a>Galatians 3:18

For if the [klēronomia](../../strongs/g/g2817.md) be of the [nomos](../../strongs/g/g3551.md), it is no more of [epaggelia](../../strongs/g/g1860.md): but [theos](../../strongs/g/g2316.md) [charizomai](../../strongs/g/g5483.md) it to [Abraam](../../strongs/g/g11.md) by [epaggelia](../../strongs/g/g1860.md).

<a name="galatians_3_19"></a>Galatians 3:19

Wherefore then the [nomos](../../strongs/g/g3551.md)? It was [prostithēmi](../../strongs/g/g4369.md) because of [parabasis](../../strongs/g/g3847.md), till the [sperma](../../strongs/g/g4690.md) should [erchomai](../../strongs/g/g2064.md) to whom the [epaggellomai](../../strongs/g/g1861.md); [diatassō](../../strongs/g/g1299.md) by [aggelos](../../strongs/g/g32.md) in the [cheir](../../strongs/g/g5495.md) of a [mesitēs](../../strongs/g/g3316.md).

<a name="galatians_3_20"></a>Galatians 3:20

Now a [mesitēs](../../strongs/g/g3316.md) is not of one, but [theos](../../strongs/g/g2316.md) is one.

<a name="galatians_3_21"></a>Galatians 3:21

the [nomos](../../strongs/g/g3551.md) then against the [epaggelia](../../strongs/g/g1860.md) of [theos](../../strongs/g/g2316.md)? [mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md): for if there had been a [nomos](../../strongs/g/g3551.md) [didōmi](../../strongs/g/g1325.md) which could have [zōopoieō](../../strongs/g/g2227.md), [ontōs](../../strongs/g/g3689.md) [dikaiosynē](../../strongs/g/g1343.md) should have been by the [nomos](../../strongs/g/g3551.md).

<a name="galatians_3_22"></a>Galatians 3:22

But the [graphē](../../strongs/g/g1124.md) hath [sygkleiō](../../strongs/g/g4788.md) all under [hamartia](../../strongs/g/g266.md), that the [epaggelia](../../strongs/g/g1860.md) by [pistis](../../strongs/g/g4102.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) might be [didōmi](../../strongs/g/g1325.md) to [pisteuō](../../strongs/g/g4100.md).

<a name="galatians_3_23"></a>Galatians 3:23

But before [pistis](../../strongs/g/g4102.md) [erchomai](../../strongs/g/g2064.md), we were [phroureō](../../strongs/g/g5432.md) under the [nomos](../../strongs/g/g3551.md), [sygkleiō](../../strongs/g/g4788.md) unto the [pistis](../../strongs/g/g4102.md) which should afterwards be [apokalyptō](../../strongs/g/g601.md).

<a name="galatians_3_24"></a>Galatians 3:24

Wherefore the [nomos](../../strongs/g/g3551.md) was our [paidagōgos](../../strongs/g/g3807.md) to unto [Christos](../../strongs/g/g5547.md), that we might be [dikaioō](../../strongs/g/g1344.md) by [pistis](../../strongs/g/g4102.md).

<a name="galatians_3_25"></a>Galatians 3:25

But after that [pistis](../../strongs/g/g4102.md) is [erchomai](../../strongs/g/g2064.md), we are no longer under a [paidagōgos](../../strongs/g/g3807.md).

<a name="galatians_3_26"></a>Galatians 3:26

For ye are all the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md) by [pistis](../../strongs/g/g4102.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="galatians_3_27"></a>Galatians 3:27

For as many of you as have been [baptizō](../../strongs/g/g907.md) into [Christos](../../strongs/g/g5547.md) have [endyō](../../strongs/g/g1746.md) [Christos](../../strongs/g/g5547.md).

<a name="galatians_3_28"></a>Galatians 3:28

There is neither [Ioudaios](../../strongs/g/g2453.md) nor [Hellēn](../../strongs/g/g1672.md), there is neither [doulos](../../strongs/g/g1401.md) nor [eleutheros](../../strongs/g/g1658.md), there is neither [arrēn](../../strongs/g/g730.md) nor [thēlys](../../strongs/g/g2338.md): for ye are all one in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="galatians_3_29"></a>Galatians 3:29

And if ye [Christos](../../strongs/g/g5547.md), then are ye [Abraam](../../strongs/g/g11.md) [sperma](../../strongs/g/g4690.md), and [klēronomos](../../strongs/g/g2818.md) according to the [epaggelia](../../strongs/g/g1860.md).

---

[Transliteral Bible](../bible.md)

[Galatians](galatians.md)

[Galatians 2](galatians_2.md) - [Galatians 4](galatians_4.md)

---

[^1]: [Galatians 3:1 Commentary](../../commentary/galatians/galatians_3_commentary.md#galatians_3_1)
