# [Galatians 1](https://www.blueletterbible.org/kjv/gal/1/1/s_1092001)

<a name="galatians_1_1"></a>Galatians 1:1

[Paulos](../../strongs/g/g3972.md), [apostolos](../../strongs/g/g652.md), (not of [anthrōpos](../../strongs/g/g444.md), neither by [anthrōpos](../../strongs/g/g444.md), but by [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), and [theos](../../strongs/g/g2316.md) the [patēr](../../strongs/g/g3962.md), who [egeirō](../../strongs/g/g1453.md) him from the [nekros](../../strongs/g/g3498.md);)

<a name="galatians_1_2"></a>Galatians 1:2

And all the [adelphos](../../strongs/g/g80.md) which are with me, unto the [ekklēsia](../../strongs/g/g1577.md) of [galatia](../../strongs/g/g1053.md):

<a name="galatians_1_3"></a>Galatians 1:3

[charis](../../strongs/g/g5485.md) to you and [eirēnē](../../strongs/g/g1515.md) from [theos](../../strongs/g/g2316.md) the [patēr](../../strongs/g/g3962.md), and from our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md),

<a name="galatians_1_4"></a>Galatians 1:4

Who [didōmi](../../strongs/g/g1325.md) himself for our [hamartia](../../strongs/g/g266.md), that he might [exaireō](../../strongs/g/g1807.md) us from this [enistēmi](../../strongs/g/g1764.md) [ponēros](../../strongs/g/g4190.md) [aiōn](../../strongs/g/g165.md), according to the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md) and our [patēr](../../strongs/g/g3962.md):

<a name="galatians_1_5"></a>Galatians 1:5

To whom [doxa](../../strongs/g/g1391.md) for [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

<a name="galatians_1_6"></a>Galatians 1:6

I [thaumazō](../../strongs/g/g2296.md) that ye are so [tacheōs](../../strongs/g/g5030.md) [metatithēmi](../../strongs/g/g3346.md) from him that [kaleō](../../strongs/g/g2564.md) you into the [charis](../../strongs/g/g5485.md) of [Christos](../../strongs/g/g5547.md) unto another [euaggelion](../../strongs/g/g2098.md):

<a name="galatians_1_7"></a>Galatians 1:7

Which is not another; but there be some that [tarassō](../../strongs/g/g5015.md) you, and would [metastrephō](../../strongs/g/g3344.md) the [euaggelion](../../strongs/g/g2098.md) of [Christos](../../strongs/g/g5547.md).

<a name="galatians_1_8"></a>Galatians 1:8

But though we, or an [aggelos](../../strongs/g/g32.md) from [ouranos](../../strongs/g/g3772.md), [euaggelizō](../../strongs/g/g2097.md) unto you than that which we have [euaggelizō](../../strongs/g/g2097.md) unto you, let him be [anathema](../../strongs/g/g331.md).

<a name="galatians_1_9"></a>Galatians 1:9

As we [proereō](../../strongs/g/g4280.md), so [legō](../../strongs/g/g3004.md) I now again, if any man [euaggelizō](../../strongs/g/g2097.md) any other unto you than that ye have [paralambanō](../../strongs/g/g3880.md), let him be [anathema](../../strongs/g/g331.md).

<a name="galatians_1_10"></a>Galatians 1:10

For do I now [peithō](../../strongs/g/g3982.md) [anthrōpos](../../strongs/g/g444.md), or [theos](../../strongs/g/g2316.md)? or do I [zēteō](../../strongs/g/g2212.md) to [areskō](../../strongs/g/g700.md) [anthrōpos](../../strongs/g/g444.md)? for if I yet [areskō](../../strongs/g/g700.md) [anthrōpos](../../strongs/g/g444.md), I should not be the [doulos](../../strongs/g/g1401.md) of [Christos](../../strongs/g/g5547.md).

<a name="galatians_1_11"></a>Galatians 1:11

But I [gnōrizō](../../strongs/g/g1107.md) you, [adelphos](../../strongs/g/g80.md), that the [euaggelion](../../strongs/g/g2098.md) which was [euaggelizō](../../strongs/g/g2097.md) of me is not after [anthrōpos](../../strongs/g/g444.md).

<a name="galatians_1_12"></a>Galatians 1:12

For I neither [paralambanō](../../strongs/g/g3880.md) it of [anthrōpos](../../strongs/g/g444.md), neither was I [didaskō](../../strongs/g/g1321.md) it, but by the [apokalypsis](../../strongs/g/g602.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="galatians_1_13"></a>Galatians 1:13

For ye have [akouō](../../strongs/g/g191.md) of my [anastrophē](../../strongs/g/g391.md) in time past in [ioudaismos](../../strongs/g/g2454.md), how that [hyperbolē](../../strongs/g/g5236.md) I [diōkō](../../strongs/g/g1377.md) the [ekklēsia](../../strongs/g/g1577.md) of [theos](../../strongs/g/g2316.md), and [portheō](../../strongs/g/g4199.md) it:

<a name="galatians_1_14"></a>Galatians 1:14

And [prokoptō](../../strongs/g/g4298.md) in [ioudaismos](../../strongs/g/g2454.md) above [polys](../../strongs/g/g4183.md) [synēlikiōtēs](../../strongs/g/g4915.md) in mine own [genos](../../strongs/g/g1085.md), being more [perissoterōs](../../strongs/g/g4056.md) [zēlōtēs](../../strongs/g/g2207.md) of the [paradosis](../../strongs/g/g3862.md) of my [patrikos](../../strongs/g/g3967.md).

<a name="galatians_1_15"></a>Galatians 1:15

But when it [eudokeō](../../strongs/g/g2106.md) [theos](../../strongs/g/g2316.md), who [aphorizō](../../strongs/g/g873.md) me from my [mētēr](../../strongs/g/g3384.md) [koilia](../../strongs/g/g2836.md), and [kaleō](../../strongs/g/g2564.md) me by his [charis](../../strongs/g/g5485.md),

<a name="galatians_1_16"></a>Galatians 1:16

To [apokalyptō](../../strongs/g/g601.md) his [huios](../../strongs/g/g5207.md) in me, that I might [euaggelizō](../../strongs/g/g2097.md) him among the [ethnos](../../strongs/g/g1484.md); [eutheōs](../../strongs/g/g2112.md) I [prosanatithēmi](../../strongs/g/g4323.md) not with [sarx](../../strongs/g/g4561.md) and [haima](../../strongs/g/g129.md):

<a name="galatians_1_17"></a>Galatians 1:17

Neither [anerchomai](../../strongs/g/g424.md) I to [Hierosolyma](../../strongs/g/g2414.md) to them which were [apostolos](../../strongs/g/g652.md) before me; but I [aperchomai](../../strongs/g/g565.md) into [arabia](../../strongs/g/g688.md), and [hypostrephō](../../strongs/g/g5290.md) again unto [Damaskos](../../strongs/g/g1154.md).

<a name="galatians_1_18"></a>Galatians 1:18

Then after three [etos](../../strongs/g/g2094.md) I [anerchomai](../../strongs/g/g424.md) to [Hierosolyma](../../strongs/g/g2414.md) to [historeō](../../strongs/g/g2477.md) [Petros](../../strongs/g/g4074.md), and [epimenō](../../strongs/g/g1961.md) with him fifteen [hēmera](../../strongs/g/g2250.md).

<a name="galatians_1_19"></a>Galatians 1:19

But other of the [apostolos](../../strongs/g/g652.md) [eidō](../../strongs/g/g1492.md) I none, save [Iakōbos](../../strongs/g/g2385.md) the [kyrios](../../strongs/g/g2962.md) [adelphos](../../strongs/g/g80.md).

<a name="galatians_1_20"></a>Galatians 1:20

Now the things which I [graphō](../../strongs/g/g1125.md) unto you, [idou](../../strongs/g/g2400.md), before [theos](../../strongs/g/g2316.md), I [pseudomai](../../strongs/g/g5574.md) not.

<a name="galatians_1_21"></a>Galatians 1:21

Afterwards I [erchomai](../../strongs/g/g2064.md) into the [klima](../../strongs/g/g2824.md) of [Syria](../../strongs/g/g4947.md) and [Kilikia](../../strongs/g/g2791.md);

<a name="galatians_1_22"></a>Galatians 1:22

And was [agnoeō](../../strongs/g/g50.md) by [prosōpon](../../strongs/g/g4383.md) unto the [ekklēsia](../../strongs/g/g1577.md) of [Ioudaia](../../strongs/g/g2449.md) which were in [Christos](../../strongs/g/g5547.md):

<a name="galatians_1_23"></a>Galatians 1:23

But they had [akouō](../../strongs/g/g191.md) only, That he which [diōkō](../../strongs/g/g1377.md) us in times past now [euaggelizō](../../strongs/g/g2097.md) the [pistis](../../strongs/g/g4102.md) which once he [portheō](../../strongs/g/g4199.md).

<a name="galatians_1_24"></a>Galatians 1:24

And they [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md) in me.

---

[Transliteral Bible](../bible.md)

[Galatians](galatians.md)

[Galatians 0](galatians_0.md) - [Galatians 2](galatians_2.md)