# [Galatians 5](https://www.blueletterbible.org/kjv/gal/5/16/s_1096016)

<a name="galatians_5_1"></a>Galatians 5:1

[stēkō](../../strongs/g/g4739.md) therefore in the [eleutheria](../../strongs/g/g1657.md) wherewith [Christos](../../strongs/g/g5547.md) hath [eleutheroō](../../strongs/g/g1659.md) us, and be not [enechō](../../strongs/g/g1758.md) again with the [zygos](../../strongs/g/g2218.md) of [douleia](../../strongs/g/g1397.md).

<a name="galatians_5_2"></a>Galatians 5:2

[ide](../../strongs/g/g2396.md), I [Paulos](../../strongs/g/g3972.md) [legō](../../strongs/g/g3004.md) unto you, that if ye be [peritemnō](../../strongs/g/g4059.md), [Christos](../../strongs/g/g5547.md) shall [ōpheleō](../../strongs/g/g5623.md) you nothing.

<a name="galatians_5_3"></a>Galatians 5:3

For I [martyromai](../../strongs/g/g3143.md) again to every [anthrōpos](../../strongs/g/g444.md) that is [peritemnō](../../strongs/g/g4059.md), that he is a [opheiletēs](../../strongs/g/g3781.md) to [poieō](../../strongs/g/g4160.md) the [holos](../../strongs/g/g3650.md) [nomos](../../strongs/g/g3551.md).

<a name="galatians_5_4"></a>Galatians 5:4

[Christos](../../strongs/g/g5547.md) [apo](../../strongs/g/g575.md) [katargeō](../../strongs/g/g2673.md) unto you, whosoever of you are [dikaioō](../../strongs/g/g1344.md) by the [nomos](../../strongs/g/g3551.md); ye are [ekpiptō](../../strongs/g/g1601.md) from [charis](../../strongs/g/g5485.md).

<a name="galatians_5_5"></a>Galatians 5:5

For we through the [pneuma](../../strongs/g/g4151.md) [apekdechomai](../../strongs/g/g553.md) for the [elpis](../../strongs/g/g1680.md) of [dikaiosynē](../../strongs/g/g1343.md) by [pistis](../../strongs/g/g4102.md).

<a name="galatians_5_6"></a>Galatians 5:6

For in [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) neither [peritomē](../../strongs/g/g4061.md) availeth any thing, nor [akrobystia](../../strongs/g/g203.md); but [pistis](../../strongs/g/g4102.md) which [energeō](../../strongs/g/g1754.md) by [agapē](../../strongs/g/g26.md).

<a name="galatians_5_7"></a>Galatians 5:7

Ye did [trechō](../../strongs/g/g5143.md) [kalōs](../../strongs/g/g2573.md); who did [anakoptō](../../strongs/g/g348.md) [egkoptō](../../strongs/g/g1465.md) you that ye should not [peithō](../../strongs/g/g3982.md) the [alētheia](../../strongs/g/g225.md)?

<a name="galatians_5_8"></a>Galatians 5:8

This [peismonē](../../strongs/g/g3988.md) not of him that [kaleō](../../strongs/g/g2564.md) you.

<a name="galatians_5_9"></a>Galatians 5:9

A [mikros](../../strongs/g/g3398.md) [zymē](../../strongs/g/g2219.md) [zymoō](../../strongs/g/g2220.md) the [holos](../../strongs/g/g3650.md) [phyrama](../../strongs/g/g5445.md).

<a name="galatians_5_10"></a>Galatians 5:10

I have [peithō](../../strongs/g/g3982.md) in you through the [kyrios](../../strongs/g/g2962.md), that ye will be none otherwise [phroneō](../../strongs/g/g5426.md): but he that [tarassō](../../strongs/g/g5015.md) you shall [bastazō](../../strongs/g/g941.md) his [krima](../../strongs/g/g2917.md), whosoever he be.

<a name="galatians_5_11"></a>Galatians 5:11

And I, [adelphos](../../strongs/g/g80.md), if I yet [kēryssō](../../strongs/g/g2784.md) [peritomē](../../strongs/g/g4061.md), why do I yet [diōkō](../../strongs/g/g1377.md)? then is the [skandalon](../../strongs/g/g4625.md) of the [stauros](../../strongs/g/g4716.md) [katargeō](../../strongs/g/g2673.md).

<a name="galatians_5_12"></a>Galatians 5:12

I [ophelon](../../strongs/g/g3785.md) they were even [apokoptō](../../strongs/g/g609.md) which [anastatoō](../../strongs/g/g387.md) you.

<a name="galatians_5_13"></a>Galatians 5:13

For, [adelphos](../../strongs/g/g80.md), ye have been [kaleō](../../strongs/g/g2564.md) unto [eleutheria](../../strongs/g/g1657.md); only use not [eleutheria](../../strongs/g/g1657.md) for an [aphormē](../../strongs/g/g874.md) to the [sarx](../../strongs/g/g4561.md), but by [agapē](../../strongs/g/g26.md) [douleuō](../../strongs/g/g1398.md) [allēlōn](../../strongs/g/g240.md).

<a name="galatians_5_14"></a>Galatians 5:14

For all the [nomos](../../strongs/g/g3551.md) is [plēroō](../../strongs/g/g4137.md) in [heis](../../strongs/g/g1520.md) [logos](../../strongs/g/g3056.md), in this; Thou shalt [agapaō](../../strongs/g/g25.md) thy [plēsion](../../strongs/g/g4139.md) as thyself.

<a name="galatians_5_15"></a>Galatians 5:15

But if ye [daknō](../../strongs/g/g1143.md) and [katesthiō](../../strongs/g/g2719.md) [allēlōn](../../strongs/g/g240.md), [blepō](../../strongs/g/g991.md) that ye [anaideia](../../strongs/g/g335.md) not [analiskō](../../strongs/g/g355.md) one of [allēlōn](../../strongs/g/g240.md).

<a name="galatians_5_16"></a>Galatians 5:16

This I [legō](../../strongs/g/g3004.md) then, [peripateō](../../strongs/g/g4043.md) in the [pneuma](../../strongs/g/g4151.md), and ye shall not [teleō](../../strongs/g/g5055.md) the [epithymia](../../strongs/g/g1939.md) of the [sarx](../../strongs/g/g4561.md).

<a name="galatians_5_17"></a>Galatians 5:17

For the [sarx](../../strongs/g/g4561.md) [epithymeō](../../strongs/g/g1937.md) against the [pneuma](../../strongs/g/g4151.md), and the [pneuma](../../strongs/g/g4151.md) against the [sarx](../../strongs/g/g4561.md): and these are [antikeimai](../../strongs/g/g480.md) the one to the [allēlōn](../../strongs/g/g240.md): so that ye cannot [poieō](../../strongs/g/g4160.md) the things that ye [thelō](../../strongs/g/g2309.md).

<a name="galatians_5_18"></a>Galatians 5:18

But if ye be [agō](../../strongs/g/g71.md) of the [pneuma](../../strongs/g/g4151.md), ye are not under the [nomos](../../strongs/g/g3551.md).

<a name="galatians_5_19"></a>Galatians 5:19

Now the [ergon](../../strongs/g/g2041.md) of the [sarx](../../strongs/g/g4561.md) are [phaneros](../../strongs/g/g5318.md), which are these; [moicheia](../../strongs/g/g3430.md), [porneia](../../strongs/g/g4202.md), [akatharsia](../../strongs/g/g167.md), [aselgeia](../../strongs/g/g766.md),

<a name="galatians_5_20"></a>Galatians 5:20

[eidōlolatria](../../strongs/g/g1495.md), [pharmakeia](../../strongs/g/g5331.md), [echthra](../../strongs/g/g2189.md), [eris](../../strongs/g/g2054.md), [zēlos](../../strongs/g/g2205.md), [thymos](../../strongs/g/g2372.md), [eritheia](../../strongs/g/g2052.md), [dichostasia](../../strongs/g/g1370.md), [hairesis](../../strongs/g/g139.md),

<a name="galatians_5_21"></a>Galatians 5:21

[phthonos](../../strongs/g/g5355.md), [phonos](../../strongs/g/g5408.md), [methē](../../strongs/g/g3178.md), [kōmos](../../strongs/g/g2970.md), and such [homoios](../../strongs/g/g3664.md): of the which I [prolegō](../../strongs/g/g4302.md) you, as I have also [proepō](../../strongs/g/g4277.md) in time past, that they which [prassō](../../strongs/g/g4238.md) such things shall not [klēronomeō](../../strongs/g/g2816.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).

<a name="galatians_5_22"></a>Galatians 5:22

But the [karpos](../../strongs/g/g2590.md) of the [pneuma](../../strongs/g/g4151.md) is [agapē](../../strongs/g/g26.md), [chara](../../strongs/g/g5479.md), [eirēnē](../../strongs/g/g1515.md), [makrothymia](../../strongs/g/g3115.md), [chrēstotēs](../../strongs/g/g5544.md), [agathōsynē](../../strongs/g/g19.md), [pistis](../../strongs/g/g4102.md),

<a name="galatians_5_23"></a>Galatians 5:23

[praotēs](../../strongs/g/g4236.md), [egkrateia](../../strongs/g/g1466.md): against such there is no [nomos](../../strongs/g/g3551.md).

<a name="galatians_5_24"></a>Galatians 5:24

And they that are [Christos](../../strongs/g/g5547.md) have [stauroō](../../strongs/g/g4717.md) the [sarx](../../strongs/g/g4561.md) with the [pathēma](../../strongs/g/g3804.md) and [epithymia](../../strongs/g/g1939.md).

<a name="galatians_5_25"></a>Galatians 5:25

If we [zaō](../../strongs/g/g2198.md) in the [pneuma](../../strongs/g/g4151.md), let us also [stoicheō](../../strongs/g/g4748.md) in the [pneuma](../../strongs/g/g4151.md).

<a name="galatians_5_26"></a>Galatians 5:26

Let us not be [kenodoxos](../../strongs/g/g2755.md), [prokaleō](../../strongs/g/g4292.md) [allēlōn](../../strongs/g/g240.md), [phthoneō](../../strongs/g/g5354.md) [allēlōn](../../strongs/g/g240.md).

---

[Transliteral Bible](../bible.md)

[Galatians](galatians.md)

[Galatians 4](galatians_4.md) - [Galatians 6](galatians_6.md)