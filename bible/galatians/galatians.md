# Galatians

[Galatians Overview](../../commentary/galatians/galatians_overview.md)

[Galatians 1](galatians_1.md)

[Galatians 2](galatians_2.md)

[Galatians 3](galatians_3.md)

[Galatians 4](galatians_4.md)

[Galatians 5](galatians_5.md)

[Galatians 6](galatians_6.md)

---

[Transliteral Bible](../index.md)
