# [Galatians 2](https://www.blueletterbible.org/kjv/gal/2/1/s_1093001)

<a name="galatians_2_1"></a>Galatians 2:1

Then fourteen [etos](../../strongs/g/g2094.md) after I [anabainō](../../strongs/g/g305.md) again to [Hierosolyma](../../strongs/g/g2414.md) with [Barnabas](../../strongs/g/g921.md), and [symparalambanō](../../strongs/g/g4838.md) [titos](../../strongs/g/g5103.md) with me also.

<a name="galatians_2_2"></a>Galatians 2:2

And I [anabainō](../../strongs/g/g305.md) by [apokalypsis](../../strongs/g/g602.md), and [anatithēmi](../../strongs/g/g394.md) unto them that [euaggelion](../../strongs/g/g2098.md) which I [kēryssō](../../strongs/g/g2784.md) among the [ethnos](../../strongs/g/g1484.md), but [idios](../../strongs/g/g2398.md) to them which were of [dokeō](../../strongs/g/g1380.md), lest by any means I should [trechō](../../strongs/g/g5143.md), or had [trechō](../../strongs/g/g5143.md), in [kenos](../../strongs/g/g2756.md).

<a name="galatians_2_3"></a>Galatians 2:3

But neither [titos](../../strongs/g/g5103.md), who was with me, being a [Hellēn](../../strongs/g/g1672.md), was [anagkazō](../../strongs/g/g315.md) to be [peritemnō](../../strongs/g/g4059.md):

<a name="galatians_2_4"></a>Galatians 2:4

And that because of [pseudadelphos](../../strongs/g/g5569.md) [pareisaktos](../../strongs/g/g3920.md), who [pareiserchomai](../../strongs/g/g3922.md) to [kataskopeō](../../strongs/g/g2684.md) our [eleutheria](../../strongs/g/g1657.md) which we have in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md), that they might bring us into [katadouloō](../../strongs/g/g2615.md):

<a name="galatians_2_5"></a>Galatians 2:5

To whom we [eikō](../../strongs/g/g1502.md) by [hypotagē](../../strongs/g/g5292.md), no, not for an [hōra](../../strongs/g/g5610.md); that the [alētheia](../../strongs/g/g225.md) of the [euaggelion](../../strongs/g/g2098.md) might [diamenō](../../strongs/g/g1265.md) with you.

<a name="galatians_2_6"></a>Galatians 2:6

But of these who [dokeō](../../strongs/g/g1380.md) to be somewhat, (whatsoever they were, it no [diapherō](../../strongs/g/g1308.md) to me: [theos](../../strongs/g/g2316.md) [lambanō](../../strongs/g/g2983.md) no [anthrōpos](../../strongs/g/g444.md) [prosōpon](../../strongs/g/g4383.md):) for they who [dokeō](../../strongs/g/g1380.md) [prosanatithēmi](../../strongs/g/g4323.md) nothing to me:

<a name="galatians_2_7"></a>Galatians 2:7

But [tounantion](../../strongs/g/g5121.md), when they [eidō](../../strongs/g/g1492.md) that the [euaggelion](../../strongs/g/g2098.md) of the [akrobystia](../../strongs/g/g203.md) was [pisteuō](../../strongs/g/g4100.md) unto me, as the [peritomē](../../strongs/g/g4061.md) was unto [Petros](../../strongs/g/g4074.md);

<a name="galatians_2_8"></a>Galatians 2:8

(For [energeō](../../strongs/g/g1754.md) in [Petros](../../strongs/g/g4074.md) to the [apostolē](../../strongs/g/g651.md) of the [peritomē](../../strongs/g/g4061.md), the same was [energeō](../../strongs/g/g1754.md) in me toward the [ethnos](../../strongs/g/g1484.md):)

<a name="galatians_2_9"></a>Galatians 2:9

And when [Iakōbos](../../strongs/g/g2385.md), [Kēphas](../../strongs/g/g2786.md), and [Iōannēs](../../strongs/g/g2491.md), who [dokeō](../../strongs/g/g1380.md) to be [stylos](../../strongs/g/g4769.md), [ginōskō](../../strongs/g/g1097.md) the [charis](../../strongs/g/g5485.md) that was [didōmi](../../strongs/g/g1325.md) unto me, they [didōmi](../../strongs/g/g1325.md) to me and [Barnabas](../../strongs/g/g921.md) the [dexios](../../strongs/g/g1188.md) of [koinōnia](../../strongs/g/g2842.md); that we unto the [ethnos](../../strongs/g/g1484.md), and they unto the [peritomē](../../strongs/g/g4061.md).

<a name="galatians_2_10"></a>Galatians 2:10

Only they would that we should [mnēmoneuō](../../strongs/g/g3421.md) the [ptōchos](../../strongs/g/g4434.md); the same which I also was [spoudazō](../../strongs/g/g4704.md) to [poieō](../../strongs/g/g4160.md).

<a name="galatians_2_11"></a>Galatians 2:11

But when [Petros](../../strongs/g/g4074.md) was [erchomai](../../strongs/g/g2064.md) to [Antiocheia](../../strongs/g/g490.md), I [anthistēmi](../../strongs/g/g436.md) him to the [prosōpon](../../strongs/g/g4383.md), because he was to be [kataginōskō](../../strongs/g/g2607.md).

<a name="galatians_2_12"></a>Galatians 2:12

For before that certain came from [Iakōbos](../../strongs/g/g2385.md), he did [synesthiō](../../strongs/g/g4906.md) with the [ethnos](../../strongs/g/g1484.md): but when they were [erchomai](../../strongs/g/g2064.md), he [hypostellō](../../strongs/g/g5288.md) and [aphorizō](../../strongs/g/g873.md) himself, [phobeō](../../strongs/g/g5399.md) them which were of the [peritomē](../../strongs/g/g4061.md).

<a name="galatians_2_13"></a>Galatians 2:13

And the [loipos](../../strongs/g/g3062.md) [Ioudaios](../../strongs/g/g2453.md) [synypokrinomai](../../strongs/g/g4942.md) likewise with him; insomuch that [Barnabas](../../strongs/g/g921.md) also was [synapagō](../../strongs/g/g4879.md) with their [hypokrisis](../../strongs/g/g5272.md).

<a name="galatians_2_14"></a>Galatians 2:14

But when I [eidō](../../strongs/g/g1492.md) that they [orthopodeō](../../strongs/g/g3716.md) not according to the [alētheia](../../strongs/g/g225.md) of the [euaggelion](../../strongs/g/g2098.md), I [eipon](../../strongs/g/g2036.md) unto [Petros](../../strongs/g/g4074.md) before them all, If thou, being an [Ioudaios](../../strongs/g/g2453.md), [zaō](../../strongs/g/g2198.md) [ethnikōs](../../strongs/g/g1483.md), and not as do the [Ioudaikōs](../../strongs/g/g2452.md), why [anagkazō](../../strongs/g/g315.md) thou the [ethnos](../../strongs/g/g1484.md) to [ioudaizō](../../strongs/g/g2450.md)?

<a name="galatians_2_15"></a>Galatians 2:15

We [Ioudaios](../../strongs/g/g2453.md) by [physis](../../strongs/g/g5449.md), and not [hamartōlos](../../strongs/g/g268.md) of the [ethnos](../../strongs/g/g1484.md),

<a name="galatians_2_16"></a>Galatians 2:16

[eidō](../../strongs/g/g1492.md) that [anthrōpos](../../strongs/g/g444.md) is not [dikaioō](../../strongs/g/g1344.md) by the [ergon](../../strongs/g/g2041.md) of the [nomos](../../strongs/g/g3551.md), but by the [pistis](../../strongs/g/g4102.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), even we have [pisteuō](../../strongs/g/g4100.md) in [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), that we might be [dikaioō](../../strongs/g/g1344.md) by the [pistis](../../strongs/g/g4102.md) of [Christos](../../strongs/g/g5547.md), and not by the [ergon](../../strongs/g/g2041.md) of the [nomos](../../strongs/g/g3551.md): for by the [ergon](../../strongs/g/g2041.md) of the [nomos](../../strongs/g/g3551.md) shall no [sarx](../../strongs/g/g4561.md) be [dikaioō](../../strongs/g/g1344.md).

<a name="galatians_2_17"></a>Galatians 2:17

But if, while we [zēteō](../../strongs/g/g2212.md) to be [dikaioō](../../strongs/g/g1344.md) by [Christos](../../strongs/g/g5547.md), we ourselves also are [heuriskō](../../strongs/g/g2147.md) [hamartōlos](../../strongs/g/g268.md), [ara](../../strongs/g/g687.md) [Christos](../../strongs/g/g5547.md) the [diakonos](../../strongs/g/g1249.md) of [hamartia](../../strongs/g/g266.md)? [mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md).

<a name="galatians_2_18"></a>Galatians 2:18

For if I [oikodomeō](../../strongs/g/g3618.md) again the things which I [katalyō](../../strongs/g/g2647.md), I [synistēmi](../../strongs/g/g4921.md) myself a [parabatēs](../../strongs/g/g3848.md).

<a name="galatians_2_19"></a>Galatians 2:19

For I through the [nomos](../../strongs/g/g3551.md) am [apothnēskō](../../strongs/g/g599.md) to the [nomos](../../strongs/g/g3551.md), that I might [zaō](../../strongs/g/g2198.md) unto [theos](../../strongs/g/g2316.md).

<a name="galatians_2_20"></a>Galatians 2:20

I am [systauroō](../../strongs/g/g4957.md) with [Christos](../../strongs/g/g5547.md): neverthless I [zaō](../../strongs/g/g2198.md); yet not I, but [Christos](../../strongs/g/g5547.md) [zaō](../../strongs/g/g2198.md) in me: and [hos](../../strongs/g/g3739.md) which I now [zaō](../../strongs/g/g2198.md) in the [sarx](../../strongs/g/g4561.md) I [zaō](../../strongs/g/g2198.md) by the [pistis](../../strongs/g/g4102.md) of the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), who [agapaō](../../strongs/g/g25.md) me, and [paradidōmi](../../strongs/g/g3860.md) himself for me.

<a name="galatians_2_21"></a>Galatians 2:21

I do not [atheteō](../../strongs/g/g114.md) the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md): for if [dikaiosynē](../../strongs/g/g1343.md) by the [nomos](../../strongs/g/g3551.md), then [Christos](../../strongs/g/g5547.md) is [apothnēskō](../../strongs/g/g599.md) [dōrean](../../strongs/g/g1432.md).

---

[Transliteral Bible](../bible.md)

[Galatians](galatians.md)

[Galatians 1](galatians_1.md) - [Galatians 3](galatians_3.md)