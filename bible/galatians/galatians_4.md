# [Galatians 4](https://www.blueletterbible.org/kjv/gal/4/6/p0/s_1095001)

<a name="galatians_4_1"></a>Galatians 4:1

Now I [legō](../../strongs/g/g3004.md), the [klēronomos](../../strongs/g/g2818.md), [epi](../../strongs/g/g1909.md) [hosos](../../strongs/g/g3745.md) [chronos](../../strongs/g/g5550.md) as he is a [nēpios](../../strongs/g/g3516.md), [diapherō](../../strongs/g/g1308.md) [oudeis](../../strongs/g/g3762.md) from a [doulos](../../strongs/g/g1401.md), though he be [kyrios](../../strongs/g/g2962.md) of all;

<a name="galatians_4_2"></a>Galatians 4:2

But is under [epitropos](../../strongs/g/g2012.md) and [oikonomos](../../strongs/g/g3623.md) until the [prothesmia](../../strongs/g/g4287.md) of the [patēr](../../strongs/g/g3962.md).

<a name="galatians_4_3"></a>Galatians 4:3

Even so we, when we were [nēpios](../../strongs/g/g3516.md), were in [douloō](../../strongs/g/g1402.md) under the [stoicheion](../../strongs/g/g4747.md) of the [kosmos](../../strongs/g/g2889.md):

<a name="galatians_4_4"></a>Galatians 4:4

But when the [plērōma](../../strongs/g/g4138.md) of the [chronos](../../strongs/g/g5550.md) was [erchomai](../../strongs/g/g2064.md), [theos](../../strongs/g/g2316.md) [exapostellō](../../strongs/g/g1821.md) his [huios](../../strongs/g/g5207.md), [ginomai](../../strongs/g/g1096.md) of a [gynē](../../strongs/g/g1135.md), [ginomai](../../strongs/g/g1096.md) under the [nomos](../../strongs/g/g3551.md),

<a name="galatians_4_5"></a>Galatians 4:5

To [exagorazō](../../strongs/g/g1805.md) them that were under the [nomos](../../strongs/g/g3551.md), that we might [apolambanō](../../strongs/g/g618.md) the [huiothesia](../../strongs/g/g5206.md).

<a name="galatians_4_6"></a>Galatians 4:6

And because ye are [huios](../../strongs/g/g5207.md), [theos](../../strongs/g/g2316.md) hath [exapostellō](../../strongs/g/g1821.md) the [pneuma](../../strongs/g/g4151.md) of his [huios](../../strongs/g/g5207.md) into your [kardia](../../strongs/g/g2588.md), [krazō](../../strongs/g/g2896.md), [abba](../../strongs/g/g5.md), [patēr](../../strongs/g/g3962.md).

<a name="galatians_4_7"></a>Galatians 4:7

Wherefore thou art no more a [doulos](../../strongs/g/g1401.md), but a [huios](../../strongs/g/g5207.md); and if a [huios](../../strongs/g/g5207.md), then a [klēronomos](../../strongs/g/g2818.md) of [theos](../../strongs/g/g2316.md) through [Christos](../../strongs/g/g5547.md).

<a name="galatians_4_8"></a>Galatians 4:8

Howbeit then, when ye [eidō](../../strongs/g/g1492.md) not [theos](../../strongs/g/g2316.md), ye did [douleuō](../../strongs/g/g1398.md) unto them which by [physis](../../strongs/g/g5449.md) are no [theos](../../strongs/g/g2316.md).

<a name="galatians_4_9"></a>Galatians 4:9

But now, after that ye have [ginōskō](../../strongs/g/g1097.md) [theos](../../strongs/g/g2316.md), or rather are [ginōskō](../../strongs/g/g1097.md) of [theos](../../strongs/g/g2316.md), how [epistrephō](../../strongs/g/g1994.md) ye [anōthen](../../strongs/g/g509.md) to the [asthenēs](../../strongs/g/g772.md) and [ptōchos](../../strongs/g/g4434.md) [stoicheion](../../strongs/g/g4747.md), whereunto ye [thelō](../../strongs/g/g2309.md) again to be [douleuō](../../strongs/g/g1398.md)?

<a name="galatians_4_10"></a>Galatians 4:10

Ye [paratēreō](../../strongs/g/g3906.md) [hēmera](../../strongs/g/g2250.md), and [mēn](../../strongs/g/g3376.md), and [kairos](../../strongs/g/g2540.md), and [eniautos](../../strongs/g/g1763.md).

<a name="galatians_4_11"></a>Galatians 4:11

I am [phobeō](../../strongs/g/g5399.md) of you, lest I have [kopiaō](../../strongs/g/g2872.md) upon you [eikē](../../strongs/g/g1500.md).

<a name="galatians_4_12"></a>Galatians 4:12

[adelphos](../../strongs/g/g80.md), I [deomai](../../strongs/g/g1189.md) you, be as I am; for I am as ye are: ye have not [adikeō](../../strongs/g/g91.md) me at all.

<a name="galatians_4_13"></a>Galatians 4:13

Ye [eidō](../../strongs/g/g1492.md) how through [astheneia](../../strongs/g/g769.md) of the [sarx](../../strongs/g/g4561.md) [euaggelizō](../../strongs/g/g2097.md) unto you at the first.

<a name="galatians_4_14"></a>Galatians 4:14

And my [peirasmos](../../strongs/g/g3986.md) which was in my [sarx](../../strongs/g/g4561.md) ye [exoutheneō](../../strongs/g/g1848.md) not, nor [ekptyō](../../strongs/g/g1609.md); but [dechomai](../../strongs/g/g1209.md) me as an [aggelos](../../strongs/g/g32.md) of [theos](../../strongs/g/g2316.md), even as [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="galatians_4_15"></a>Galatians 4:15

Where is then the [makarismos](../../strongs/g/g3108.md) ye spake of? for I [martyreō](../../strongs/g/g3140.md) you, that, if [dynatos](../../strongs/g/g1415.md), ye would have [exoryssō](../../strongs/g/g1846.md) your own [ophthalmos](../../strongs/g/g3788.md), and have [didōmi](../../strongs/g/g1325.md) them to me.

<a name="galatians_4_16"></a>Galatians 4:16

Am I therefore become your [echthros](../../strongs/g/g2190.md), because I [alētheuō](../../strongs/g/g226.md) you?

<a name="galatians_4_17"></a>Galatians 4:17

They [zēloō](../../strongs/g/g2206.md) you, but not [kalōs](../../strongs/g/g2573.md); yea, they would [ekkleiō](../../strongs/g/g1576.md) you, that ye might [zēloō](../../strongs/g/g2206.md) them.

<a name="galatians_4_18"></a>Galatians 4:18

But [kalos](../../strongs/g/g2570.md) to be [zēloō](../../strongs/g/g2206.md) [pantote](../../strongs/g/g3842.md) in a [kalos](../../strongs/g/g2570.md), and not only when I am [pareimi](../../strongs/g/g3918.md) with you.

<a name="galatians_4_19"></a>Galatians 4:19

My [teknion](../../strongs/g/g5040.md), of whom I [ōdinō](../../strongs/g/g5605.md) again until [Christos](../../strongs/g/g5547.md) be [morphoō](../../strongs/g/g3445.md) in you,

<a name="galatians_4_20"></a>Galatians 4:20

I [thelō](../../strongs/g/g2309.md) to be [pareimi](../../strongs/g/g3918.md) with you now, and to [allassō](../../strongs/g/g236.md) my [phōnē](../../strongs/g/g5456.md); for I [aporeō](../../strongs/g/g639.md) of you.

<a name="galatians_4_21"></a>Galatians 4:21

[legō](../../strongs/g/g3004.md) me, ye that [thelō](../../strongs/g/g2309.md) to be under the [nomos](../../strongs/g/g3551.md), do ye not [akouō](../../strongs/g/g191.md) the [nomos](../../strongs/g/g3551.md)?

<a name="galatians_4_22"></a>Galatians 4:22

For it is [graphō](../../strongs/g/g1125.md), that [Abraam](../../strongs/g/g11.md) had two [huios](../../strongs/g/g5207.md), the one by a [paidiskē](../../strongs/g/g3814.md), the other by an [eleutheros](../../strongs/g/g1658.md).

<a name="galatians_4_23"></a>Galatians 4:23

But he who was of the [paidiskē](../../strongs/g/g3814.md) was [gennaō](../../strongs/g/g1080.md) after the [sarx](../../strongs/g/g4561.md); but he of the [eleutheros](../../strongs/g/g1658.md) was by [epaggelia](../../strongs/g/g1860.md).

<a name="galatians_4_24"></a>Galatians 4:24

Which things are an [allēgoreō](../../strongs/g/g238.md): for these are the two [diathēkē](../../strongs/g/g1242.md); the one from the [oros](../../strongs/g/g3735.md) [Sina](../../strongs/g/g4614.md), which [gennaō](../../strongs/g/g1080.md) to [douleia](../../strongs/g/g1397.md), which is [Agar](../../strongs/g/g28.md).

<a name="galatians_4_25"></a>Galatians 4:25

For this [Agar](../../strongs/g/g28.md) is [oros](../../strongs/g/g3735.md) [Sina](../../strongs/g/g4614.md) in [arabia](../../strongs/g/g688.md), and [systoicheō](../../strongs/g/g4960.md) to [Ierousalēm](../../strongs/g/g2419.md) which now is, and is in [douleuō](../../strongs/g/g1398.md) with her [teknon](../../strongs/g/g5043.md).

<a name="galatians_4_26"></a>Galatians 4:26

But [Ierousalēm](../../strongs/g/g2419.md) which is [anō](../../strongs/g/g507.md) is [eleutheros](../../strongs/g/g1658.md), which is the [mētēr](../../strongs/g/g3384.md) of us all.

<a name="galatians_4_27"></a>Galatians 4:27

For it is [graphō](../../strongs/g/g1125.md), [euphrainō](../../strongs/g/g2165.md), [steira](../../strongs/g/g4723.md) that [tiktō](../../strongs/g/g5088.md) not; [rhēgnymi](../../strongs/g/g4486.md) and [boaō](../../strongs/g/g994.md), thou that [ōdinō](../../strongs/g/g5605.md) not: for the [erēmos](../../strongs/g/g2048.md) hath [polys](../../strongs/g/g4183.md)  [teknon](../../strongs/g/g5043.md) than she which hath an [anēr](../../strongs/g/g435.md).

<a name="galatians_4_28"></a>Galatians 4:28

Now we, [adelphos](../../strongs/g/g80.md), as [Isaak](../../strongs/g/g2464.md) was, are the [teknon](../../strongs/g/g5043.md) of [epaggelia](../../strongs/g/g1860.md).

<a name="galatians_4_29"></a>Galatians 4:29

But as then he that was [gennaō](../../strongs/g/g1080.md) after the [sarx](../../strongs/g/g4561.md) [diōkō](../../strongs/g/g1377.md) him after the [pneuma](../../strongs/g/g4151.md), even so now.

<a name="galatians_4_30"></a>Galatians 4:30

Nevertheless what [legō](../../strongs/g/g3004.md) the [graphē](../../strongs/g/g1124.md)? [ekballō](../../strongs/g/g1544.md) the [paidiskē](../../strongs/g/g3814.md) and her [huios](../../strongs/g/g5207.md): for the [huios](../../strongs/g/g5207.md) of the [paidiskē](../../strongs/g/g3814.md) shall not [klēronomeō](../../strongs/g/g2816.md) with the [huios](../../strongs/g/g5207.md) of the [eleutheros](../../strongs/g/g1658.md).

<a name="galatians_4_31"></a>Galatians 4:31

So then, [adelphos](../../strongs/g/g80.md), we are not [teknon](../../strongs/g/g5043.md) of the [paidiskē](../../strongs/g/g3814.md), but of the [eleutheros](../../strongs/g/g1658.md).

---

[Transliteral Bible](../bible.md)

[Galatians](galatians.md)

[Galatians 3](galatians_3.md) - [Galatians 5](galatians_5.md)