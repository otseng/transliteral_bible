# [Galatians 6](https://www.blueletterbible.org/kjv/gal/6/1/s_1097001)

<a name="galatians_6_1"></a>Galatians 6:1

[adelphos](../../strongs/g/g80.md), if an [anthrōpos](../../strongs/g/g444.md) be [prolambanō](../../strongs/g/g4301.md) in a [paraptōma](../../strongs/g/g3900.md), ye which are [pneumatikos](../../strongs/g/g4152.md), [katartizō](../../strongs/g/g2675.md) such an one in the [pneuma](../../strongs/g/g4151.md) of [praotēs](../../strongs/g/g4236.md); [skopeō](../../strongs/g/g4648.md) thyself, lest thou also be [peirazō](../../strongs/g/g3985.md).

<a name="galatians_6_2"></a>Galatians 6:2

[bastazō](../../strongs/g/g941.md) [allēlōn](../../strongs/g/g240.md) [baros](../../strongs/g/g922.md), and so [anaplēroō](../../strongs/g/g378.md) the [nomos](../../strongs/g/g3551.md) of [Christos](../../strongs/g/g5547.md).

<a name="galatians_6_3"></a>Galatians 6:3

For [ei tis](../../strongs/g/g1536.md) [dokeō](../../strongs/g/g1380.md) himself to be [tis](../../strongs/g/g5100.md), when he is [mēdeis](../../strongs/g/g3367.md), he [phrenapataō](../../strongs/g/g5422.md) himself.

<a name="galatians_6_4"></a>Galatians 6:4

But let [hekastos](../../strongs/g/g1538.md) [dokimazō](../../strongs/g/g1381.md) his own [ergon](../../strongs/g/g2041.md), and then shall he have [kauchēma](../../strongs/g/g2745.md) in himself alone, and not in another.

<a name="galatians_6_5"></a>Galatians 6:5

For [hekastos](../../strongs/g/g1538.md) shall [bastazō](../../strongs/g/g941.md) his own [phortion](../../strongs/g/g5413.md).

<a name="galatians_6_6"></a>Galatians 6:6

Let him that is [katēcheō](../../strongs/g/g2727.md) in the [logos](../../strongs/g/g3056.md) [koinōneō](../../strongs/g/g2841.md) unto him that [katēcheō](../../strongs/g/g2727.md) in all [agathos](../../strongs/g/g18.md).

<a name="galatians_6_7"></a>Galatians 6:7

Be not [planaō](../../strongs/g/g4105.md); [theos](../../strongs/g/g2316.md) is not [myktērizō](../../strongs/g/g3456.md): for whatsoever an [anthrōpos](../../strongs/g/g444.md) [speirō](../../strongs/g/g4687.md), that shall he also [therizō](../../strongs/g/g2325.md).

<a name="galatians_6_8"></a>Galatians 6:8

For he that [speirō](../../strongs/g/g4687.md) to his [sarx](../../strongs/g/g4561.md) shall of the [sarx](../../strongs/g/g4561.md) [therizō](../../strongs/g/g2325.md) [phthora](../../strongs/g/g5356.md); but he that [speirō](../../strongs/g/g4687.md) to the [pneuma](../../strongs/g/g4151.md) shall of the [pneuma](../../strongs/g/g4151.md) [therizō](../../strongs/g/g2325.md) [zōē](../../strongs/g/g2222.md) [aiōnios](../../strongs/g/g166.md).

<a name="galatians_6_9"></a>Galatians 6:9

And let us not be [ekkakeō](../../strongs/g/g1573.md) in [kalos](../../strongs/g/g2570.md) [poieō](../../strongs/g/g4160.md): for in due [kairos](../../strongs/g/g2540.md) we shall [therizō](../../strongs/g/g2325.md), if we [eklyō](../../strongs/g/g1590.md) not.

<a name="galatians_6_10"></a>Galatians 6:10

As we have therefore [kairos](../../strongs/g/g2540.md), let us [ergazomai](../../strongs/g/g2038.md) [agathos](../../strongs/g/g18.md) unto all, [malista](../../strongs/g/g3122.md) unto [oikeios](../../strongs/g/g3609.md) of [pistis](../../strongs/g/g4102.md).

<a name="galatians_6_11"></a>Galatians 6:11

Ye [eidō](../../strongs/g/g1492.md) how [pēlikos](../../strongs/g/g4080.md) a [gramma](../../strongs/g/g1121.md) I have [graphō](../../strongs/g/g1125.md) unto you with mine own [cheir](../../strongs/g/g5495.md).

<a name="galatians_6_12"></a>Galatians 6:12

As many as [thelō](../../strongs/g/g2309.md) to [euprosōpeō](../../strongs/g/g2146.md) in the [sarx](../../strongs/g/g4561.md), they [anagkazō](../../strongs/g/g315.md) you to be [peritemnō](../../strongs/g/g4059.md); only lest they [diōkō](../../strongs/g/g1377.md) for the [stauros](../../strongs/g/g4716.md) of [Christos](../../strongs/g/g5547.md).

<a name="galatians_6_13"></a>Galatians 6:13

For neither they themselves who are [peritemnō](../../strongs/g/g4059.md) [phylassō](../../strongs/g/g5442.md) the [nomos](../../strongs/g/g3551.md); but [thelō](../../strongs/g/g2309.md) to have you [peritemnō](../../strongs/g/g4059.md), that they may [kauchaomai](../../strongs/g/g2744.md) in your [sarx](../../strongs/g/g4561.md).

<a name="galatians_6_14"></a>Galatians 6:14

But [mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md) that I should [kauchaomai](../../strongs/g/g2744.md), save in the [stauros](../../strongs/g/g4716.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), by whom the [kosmos](../../strongs/g/g2889.md) is [stauroō](../../strongs/g/g4717.md) unto me, and I unto the [kosmos](../../strongs/g/g2889.md).

<a name="galatians_6_15"></a>Galatians 6:15

For in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) neither [peritomē](../../strongs/g/g4061.md) availeth any thing, nor [akrobystia](../../strongs/g/g203.md), but a [kainos](../../strongs/g/g2537.md) [ktisis](../../strongs/g/g2937.md).

<a name="galatians_6_16"></a>Galatians 6:16

And as many as [stoicheō](../../strongs/g/g4748.md) according to this [kanōn](../../strongs/g/g2583.md), [eirēnē](../../strongs/g/g1515.md) be on them, and [eleos](../../strongs/g/g1656.md), and upon the [Israēl](../../strongs/g/g2474.md) of [theos](../../strongs/g/g2316.md).

<a name="galatians_6_17"></a>Galatians 6:17

[loipou](../../strongs/g/g3064.md) let [mēdeis](../../strongs/g/g3367.md) [parechō](../../strongs/g/g3930.md) [kopos](../../strongs/g/g2873.md) me: for I [bastazō](../../strongs/g/g941.md) in my [sōma](../../strongs/g/g4983.md) the [stigma](../../strongs/g/g4742.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md).

<a name="galatians_6_18"></a>Galatians 6:18

[adelphos](../../strongs/g/g80.md), the [charis](../../strongs/g/g5485.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) be with your [pneuma](../../strongs/g/g4151.md). [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Galatians](galatians.md)

[Galatians 5](galatians_5.md)