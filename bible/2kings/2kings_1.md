# [2Kings 1](https://www.blueletterbible.org/kjv/2kings/1)

<a name="2kings_1_1"></a>2Kings 1:1

Then [Mô'āḇ](../../strongs/h/h4124.md) [pāšaʿ](../../strongs/h/h6586.md) against [Yisra'el](../../strongs/h/h3478.md) ['aḥar](../../strongs/h/h310.md) the [maveth](../../strongs/h/h4194.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md).

<a name="2kings_1_2"></a>2Kings 1:2

And ['Ăḥazyâ](../../strongs/h/h274.md) [naphal](../../strongs/h/h5307.md) through a [śᵊḇāḵâ](../../strongs/h/h7639.md) in his [ʿălîyâ](../../strongs/h/h5944.md) that was in [Šōmrôn](../../strongs/h/h8111.md), and was [ḥālâ](../../strongs/h/h2470.md): and he [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md), and ['āmar](../../strongs/h/h559.md) unto them, [yālaḵ](../../strongs/h/h3212.md), [darash](../../strongs/h/h1875.md) of [BaʿAl Zᵊḇûḇ](../../strongs/h/h1176.md) the ['Elohiym](../../strongs/h/h430.md) of [ʿEqrôn](../../strongs/h/h6138.md) whether I shall [ḥāyâ](../../strongs/h/h2421.md) of this [ḥŏlî](../../strongs/h/h2483.md).

<a name="2kings_1_3"></a>2Kings 1:3

But the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) to ['Ēlîyâ](../../strongs/h/h452.md) the [Tišbî](../../strongs/h/h8664.md), [quwm](../../strongs/h/h6965.md), [ʿālâ](../../strongs/h/h5927.md) to [qārā'](../../strongs/h/h7125.md) the [mal'ak](../../strongs/h/h4397.md) of the [melek](../../strongs/h/h4428.md) of [Šōmrôn](../../strongs/h/h8111.md), and [dabar](../../strongs/h/h1696.md) unto them, Is it not because there is not an ['Elohiym](../../strongs/h/h430.md) in [Yisra'el](../../strongs/h/h3478.md), that ye [halak](../../strongs/h/h1980.md) to [darash](../../strongs/h/h1875.md) of [BaʿAl Zᵊḇûḇ](../../strongs/h/h1176.md) the ['Elohiym](../../strongs/h/h430.md) of [ʿEqrôn](../../strongs/h/h6138.md)?

<a name="2kings_1_4"></a>2Kings 1:4

Now therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Thou shalt not [yarad](../../strongs/h/h3381.md) from that [mittah](../../strongs/h/h4296.md) on which thou art [ʿālâ](../../strongs/h/h5927.md), but shalt [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md). And ['Ēlîyâ](../../strongs/h/h452.md) [yālaḵ](../../strongs/h/h3212.md).

<a name="2kings_1_5"></a>2Kings 1:5

And when the [mal'ak](../../strongs/h/h4397.md) [shuwb](../../strongs/h/h7725.md) unto him, he ['āmar](../../strongs/h/h559.md) unto them, Why are ye now [shuwb](../../strongs/h/h7725.md)?

<a name="2kings_1_6"></a>2Kings 1:6

And they ['āmar](../../strongs/h/h559.md) unto him, There [ʿālâ](../../strongs/h/h5927.md) an ['iysh](../../strongs/h/h376.md) [ʿālâ](../../strongs/h/h5927.md) to [qārā'](../../strongs/h/h7125.md) us, and ['āmar](../../strongs/h/h559.md) unto us, [yālaḵ](../../strongs/h/h3212.md), [shuwb](../../strongs/h/h7725.md) unto the [melek](../../strongs/h/h4428.md) that [shalach](../../strongs/h/h7971.md) you, and [dabar](../../strongs/h/h1696.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Is it not because there is not an ['Elohiym](../../strongs/h/h430.md) in [Yisra'el](../../strongs/h/h3478.md), that thou [shalach](../../strongs/h/h7971.md) to [darash](../../strongs/h/h1875.md) of [BaʿAl Zᵊḇûḇ](../../strongs/h/h1176.md) the ['Elohiym](../../strongs/h/h430.md) of [ʿEqrôn](../../strongs/h/h6138.md)? therefore thou shalt not [yarad](../../strongs/h/h3381.md) from that [mittah](../../strongs/h/h4296.md) on which thou art [ʿālâ](../../strongs/h/h5927.md), but shalt [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="2kings_1_7"></a>2Kings 1:7

And he [dabar](../../strongs/h/h1696.md) unto them, What [mishpat](../../strongs/h/h4941.md) of ['iysh](../../strongs/h/h376.md) was he which [ʿālâ](../../strongs/h/h5927.md) to [qārā'](../../strongs/h/h7125.md) you, and [dabar](../../strongs/h/h1696.md) you these [dabar](../../strongs/h/h1697.md)?

<a name="2kings_1_8"></a>2Kings 1:8

And they ['āmar](../../strongs/h/h559.md) him, He was an [śēʿār](../../strongs/h/h8181.md) [baʿal](../../strongs/h/h1167.md) ['iysh](../../strongs/h/h376.md), and ['āzar](../../strongs/h/h247.md) with an ['ēzôr](../../strongs/h/h232.md) of ['owr](../../strongs/h/h5785.md) about his [māṯnayim](../../strongs/h/h4975.md). And he ['āmar](../../strongs/h/h559.md), It is ['Ēlîyâ](../../strongs/h/h452.md) the [Tišbî](../../strongs/h/h8664.md).

<a name="2kings_1_9"></a>2Kings 1:9

Then the [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md) unto him a [śar](../../strongs/h/h8269.md) of fifty with his fifty. And he [ʿālâ](../../strongs/h/h5927.md) to him: and, behold, he [yashab](../../strongs/h/h3427.md) on the [ro'sh](../../strongs/h/h7218.md) of an [har](../../strongs/h/h2022.md). And he [dabar](../../strongs/h/h1696.md) unto him, Thou ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), the [melek](../../strongs/h/h4428.md) hath [dabar](../../strongs/h/h1696.md), [yarad](../../strongs/h/h3381.md).

<a name="2kings_1_10"></a>2Kings 1:10

And ['Ēlîyâ](../../strongs/h/h452.md) ['anah](../../strongs/h/h6030.md) and [dabar](../../strongs/h/h1696.md) to the [śar](../../strongs/h/h8269.md) of fifty, If I be an ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), then let ['esh](../../strongs/h/h784.md) [yarad](../../strongs/h/h3381.md) from [shamayim](../../strongs/h/h8064.md), and ['akal](../../strongs/h/h398.md) thee and thy fifty. And there [yarad](../../strongs/h/h3381.md) ['esh](../../strongs/h/h784.md) from [shamayim](../../strongs/h/h8064.md), and ['akal](../../strongs/h/h398.md) him and his fifty.

<a name="2kings_1_11"></a>2Kings 1:11

[shuwb](../../strongs/h/h7725.md) also he [shalach](../../strongs/h/h7971.md) unto him ['aḥēr](../../strongs/h/h312.md) [śar](../../strongs/h/h8269.md) of fifty with his fifty. And he ['anah](../../strongs/h/h6030.md) and [dabar](../../strongs/h/h1696.md) unto him, O ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), thus hath the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), [yarad](../../strongs/h/h3381.md) [mᵊhērâ](../../strongs/h/h4120.md).

<a name="2kings_1_12"></a>2Kings 1:12

And ['Ēlîyâ](../../strongs/h/h452.md) ['anah](../../strongs/h/h6030.md) and [dabar](../../strongs/h/h1696.md) unto them, If I be an ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), let ['esh](../../strongs/h/h784.md) [yarad](../../strongs/h/h3381.md) from [shamayim](../../strongs/h/h8064.md), and ['akal](../../strongs/h/h398.md) thee and thy fifty. And the ['esh](../../strongs/h/h784.md) of ['Elohiym](../../strongs/h/h430.md) [yarad](../../strongs/h/h3381.md) from [shamayim](../../strongs/h/h8064.md), and ['akal](../../strongs/h/h398.md) him and his fifty.

<a name="2kings_1_13"></a>2Kings 1:13

And he [shalach](../../strongs/h/h7971.md) [shuwb](../../strongs/h/h7725.md) a [śar](../../strongs/h/h8269.md) of the third fifty with his fifty. And the third [śar](../../strongs/h/h8269.md) of fifty [ʿālâ](../../strongs/h/h5927.md), and [bow'](../../strongs/h/h935.md) and [kara'](../../strongs/h/h3766.md) on his [bereḵ](../../strongs/h/h1290.md) [neḡeḏ](../../strongs/h/h5048.md) ['Ēlîyâ](../../strongs/h/h452.md), and [chanan](../../strongs/h/h2603.md) him, and [dabar](../../strongs/h/h1696.md) unto him, O ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), I pray thee, let my [nephesh](../../strongs/h/h5315.md), and the [nephesh](../../strongs/h/h5315.md) of these fifty thy ['ebed](../../strongs/h/h5650.md), be [yāqar](../../strongs/h/h3365.md) in thy ['ayin](../../strongs/h/h5869.md).

<a name="2kings_1_14"></a>2Kings 1:14

Behold, there [yarad](../../strongs/h/h3381.md) ['esh](../../strongs/h/h784.md) [yarad](../../strongs/h/h3381.md) from [shamayim](../../strongs/h/h8064.md), and burnt ['akal](../../strongs/h/h398.md) the two [śar](../../strongs/h/h8269.md) of the [ri'šôn](../../strongs/h/h7223.md) fifties with their fifties: therefore let my [nephesh](../../strongs/h/h5315.md) now be [yāqar](../../strongs/h/h3365.md) in thy ['ayin](../../strongs/h/h5869.md).

<a name="2kings_1_15"></a>2Kings 1:15

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto ['Ēlîyâ](../../strongs/h/h452.md), [yarad](../../strongs/h/h3381.md) with him: be not [yare'](../../strongs/h/h3372.md) of [paniym](../../strongs/h/h6440.md). And he [quwm](../../strongs/h/h6965.md), and [yarad](../../strongs/h/h3381.md) with him unto the [melek](../../strongs/h/h4428.md).

<a name="2kings_1_16"></a>2Kings 1:16

And he [dabar](../../strongs/h/h1696.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Forasmuch as thou hast [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to [darash](../../strongs/h/h1875.md) of [BaʿAl Zᵊḇûḇ](../../strongs/h/h1176.md) the ['Elohiym](../../strongs/h/h430.md) of [ʿEqrôn](../../strongs/h/h6138.md), is it not because there is no ['Elohiym](../../strongs/h/h430.md) in [Yisra'el](../../strongs/h/h3478.md) to [darash](../../strongs/h/h1875.md) of his [dabar](../../strongs/h/h1697.md)? therefore thou shalt not [yarad](../../strongs/h/h3381.md) off that [mittah](../../strongs/h/h4296.md) on which thou art [ʿālâ](../../strongs/h/h5927.md), but shalt [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="2kings_1_17"></a>2Kings 1:17

So he [muwth](../../strongs/h/h4191.md) according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) which ['Ēlîyâ](../../strongs/h/h452.md) had [dabar](../../strongs/h/h1696.md). And [Yᵊhôrām](../../strongs/h/h3088.md) [mālaḵ](../../strongs/h/h4427.md) in his stead in the second [šānâ](../../strongs/h/h8141.md) of [Yᵊhôrām](../../strongs/h/h3088.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md); because he had no [ben](../../strongs/h/h1121.md).

<a name="2kings_1_18"></a>2Kings 1:18

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of ['Ăḥazyâ](../../strongs/h/h274.md) which he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 2](2kings_2.md)