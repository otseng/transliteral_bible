# [2Kings 17](https://www.blueletterbible.org/kjv/2kings/17)

<a name="2kings_17_1"></a>2Kings 17:1

In the twelfth [šānâ](../../strongs/h/h8141.md) of ['Āḥāz](../../strongs/h/h271.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) began [Hôšēaʿ](../../strongs/h/h1954.md) the [ben](../../strongs/h/h1121.md) of ['Ēlâ](../../strongs/h/h425.md) to [mālaḵ](../../strongs/h/h4427.md) in [Šōmrôn](../../strongs/h/h8111.md) over [Yisra'el](../../strongs/h/h3478.md) nine [šānâ](../../strongs/h/h8141.md).

<a name="2kings_17_2"></a>2Kings 17:2

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), but not as the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) that were [paniym](../../strongs/h/h6440.md) him.

<a name="2kings_17_3"></a>2Kings 17:3

Against him [ʿālâ](../../strongs/h/h5927.md) [Šalman'Eser](../../strongs/h/h8022.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md); and [Hôšēaʿ](../../strongs/h/h1954.md) became his ['ebed](../../strongs/h/h5650.md), and [shuwb](../../strongs/h/h7725.md) him [minchah](../../strongs/h/h4503.md).

<a name="2kings_17_4"></a>2Kings 17:4

And the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [māṣā'](../../strongs/h/h4672.md) [qešer](../../strongs/h/h7195.md) in [Hôšēaʿ](../../strongs/h/h1954.md): for he had [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to [Sô'](../../strongs/h/h5471.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), and [ʿālâ](../../strongs/h/h5927.md) no [minchah](../../strongs/h/h4503.md) to the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), as he had [šānâ](../../strongs/h/h8141.md) by [šānâ](../../strongs/h/h8141.md): therefore the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [ʿāṣar](../../strongs/h/h6113.md) him, and ['āsar](../../strongs/h/h631.md) him in [bayith](../../strongs/h/h1004.md) [kele'](../../strongs/h/h3608.md).

<a name="2kings_17_5"></a>2Kings 17:5

Then the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [ʿālâ](../../strongs/h/h5927.md) throughout all the ['erets](../../strongs/h/h776.md), and [ʿālâ](../../strongs/h/h5927.md) to [Šōmrôn](../../strongs/h/h8111.md), and [ṣûr](../../strongs/h/h6696.md) it three [šānâ](../../strongs/h/h8141.md).

<a name="2kings_17_6"></a>2Kings 17:6

In the ninth [šānâ](../../strongs/h/h8141.md) of [Hôšēaʿ](../../strongs/h/h1954.md) the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [lāḵaḏ](../../strongs/h/h3920.md) [Šōmrôn](../../strongs/h/h8111.md), and [gālâ](../../strongs/h/h1540.md) [Yisra'el](../../strongs/h/h3478.md) [gālâ](../../strongs/h/h1540.md) into ['Aššûr](../../strongs/h/h804.md), and [yashab](../../strongs/h/h3427.md) them in [Ḥălaḥ](../../strongs/h/h2477.md) and in [Ḥāḇôr](../../strongs/h/h2249.md) by the [nāhār](../../strongs/h/h5104.md) of [Gôzān](../../strongs/h/h1470.md), and in the [ʿîr](../../strongs/h/h5892.md) of the [Māḏay](../../strongs/h/h4074.md).

<a name="2kings_17_7"></a>2Kings 17:7

For so it was, that the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) had [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), which had [ʿālâ](../../strongs/h/h5927.md) them out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), from under the [yad](../../strongs/h/h3027.md) of [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), and had [yare'](../../strongs/h/h3372.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md),

<a name="2kings_17_8"></a>2Kings 17:8

And [yālaḵ](../../strongs/h/h3212.md) in the [chuqqah](../../strongs/h/h2708.md) of the [gowy](../../strongs/h/h1471.md), whom [Yĕhovah](../../strongs/h/h3068.md) [yarash](../../strongs/h/h3423.md) from [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), which they had ['asah](../../strongs/h/h6213.md).

<a name="2kings_17_9"></a>2Kings 17:9

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) did [ḥāp̄ā'](../../strongs/h/h2644.md) those [dabar](../../strongs/h/h1697.md) that were not right against [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), and they [bānâ](../../strongs/h/h1129.md) them [bāmâ](../../strongs/h/h1116.md) in all their [ʿîr](../../strongs/h/h5892.md), from the [miḡdāl](../../strongs/h/h4026.md) of the [nāṣar](../../strongs/h/h5341.md) to the [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md).

<a name="2kings_17_10"></a>2Kings 17:10

And they set them [nāṣaḇ](../../strongs/h/h5324.md) [maṣṣēḇâ](../../strongs/h/h4676.md) and ['ăšērâ](../../strongs/h/h842.md) in every [gāḇōha](../../strongs/h/h1364.md) [giḇʿâ](../../strongs/h/h1389.md), and under every [raʿănān](../../strongs/h/h7488.md) ['ets](../../strongs/h/h6086.md):

<a name="2kings_17_11"></a>2Kings 17:11

And there they [qāṭar](../../strongs/h/h6999.md) in all the [bāmâ](../../strongs/h/h1116.md), as did the [gowy](../../strongs/h/h1471.md) whom [Yĕhovah](../../strongs/h/h3068.md) [gālâ](../../strongs/h/h1540.md) [paniym](../../strongs/h/h6440.md) them; and ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) [dabar](../../strongs/h/h1697.md) to [Yĕhovah](../../strongs/h/h3068.md) to [kāʿas](../../strongs/h/h3707.md):

<a name="2kings_17_12"></a>2Kings 17:12

For they ['abad](../../strongs/h/h5647.md) [gillûl](../../strongs/h/h1544.md), whereof [Yĕhovah](../../strongs/h/h3068.md) had ['āmar](../../strongs/h/h559.md) unto them, Ye shall not ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md).

<a name="2kings_17_13"></a>2Kings 17:13

Yet [Yĕhovah](../../strongs/h/h3068.md) [ʿûḏ](../../strongs/h/h5749.md) against [Yisra'el](../../strongs/h/h3478.md), and against [Yehuwdah](../../strongs/h/h3063.md), [yad](../../strongs/h/h3027.md) all the [nāḇî'](../../strongs/h/h5030.md), and by all the [ḥōzê](../../strongs/h/h2374.md), ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md) ye from your [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md), and [shamar](../../strongs/h/h8104.md) my [mitsvah](../../strongs/h/h4687.md) and my [chuqqah](../../strongs/h/h2708.md), according to all the [towrah](../../strongs/h/h8451.md) which I [tsavah](../../strongs/h/h6680.md) your ['ab](../../strongs/h/h1.md), and which I [shalach](../../strongs/h/h7971.md) to you [yad](../../strongs/h/h3027.md) my ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md).

<a name="2kings_17_14"></a>2Kings 17:14

Notwithstanding they would not [shama'](../../strongs/h/h8085.md), but [qāšâ](../../strongs/h/h7185.md) their [ʿōrep̄](../../strongs/h/h6203.md), like to the [ʿōrep̄](../../strongs/h/h6203.md) of their ['ab](../../strongs/h/h1.md), that did not ['aman](../../strongs/h/h539.md) in [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md).

<a name="2kings_17_15"></a>2Kings 17:15

And they [mā'as](../../strongs/h/h3988.md) his [choq](../../strongs/h/h2706.md), and his [bĕriyth](../../strongs/h/h1285.md) that he [karath](../../strongs/h/h3772.md) with their ['ab](../../strongs/h/h1.md), and his [ʿēḏûṯ](../../strongs/h/h5715.md) which he [ʿûḏ](../../strongs/h/h5749.md) against them; and they [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) [heḇel](../../strongs/h/h1892.md), and became [hāḇal](../../strongs/h/h1891.md), and went ['aḥar](../../strongs/h/h310.md) the [gowy](../../strongs/h/h1471.md) that were [cabiyb](../../strongs/h/h5439.md) them, concerning whom [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md) them, that they should not ['asah](../../strongs/h/h6213.md) like them.

<a name="2kings_17_16"></a>2Kings 17:16

And they ['azab](../../strongs/h/h5800.md) all the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), and ['asah](../../strongs/h/h6213.md) them [massēḵâ](../../strongs/h/h4541.md), even two [ʿēḡel](../../strongs/h/h5695.md), and ['asah](../../strongs/h/h6213.md) a ['ăšērâ](../../strongs/h/h842.md), and [shachah](../../strongs/h/h7812.md) all the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md), and ['abad](../../strongs/h/h5647.md) [BaʿAl](../../strongs/h/h1168.md).

<a name="2kings_17_17"></a>2Kings 17:17

And they caused their [ben](../../strongs/h/h1121.md) and their [bath](../../strongs/h/h1323.md) to ['abar](../../strongs/h/h5674.md) through the ['esh](../../strongs/h/h784.md), and [qāsam](../../strongs/h/h7080.md) [qesem](../../strongs/h/h7081.md) and [nāḥaš](../../strongs/h/h5172.md), and [māḵar](../../strongs/h/h4376.md) themselves to ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), to [kāʿas](../../strongs/h/h3707.md) him.

<a name="2kings_17_18"></a>2Kings 17:18

Therefore [Yĕhovah](../../strongs/h/h3068.md) was [me'od](../../strongs/h/h3966.md) ['anaph](../../strongs/h/h599.md) with [Yisra'el](../../strongs/h/h3478.md), and [cuwr](../../strongs/h/h5493.md) them out of his [paniym](../../strongs/h/h6440.md): there was none [šā'ar](../../strongs/h/h7604.md) but the [shebet](../../strongs/h/h7626.md) of [Yehuwdah](../../strongs/h/h3063.md) only.

<a name="2kings_17_19"></a>2Kings 17:19

Also [Yehuwdah](../../strongs/h/h3063.md) [shamar](../../strongs/h/h8104.md) not the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), but [yālaḵ](../../strongs/h/h3212.md) in the [chuqqah](../../strongs/h/h2708.md) of [Yisra'el](../../strongs/h/h3478.md) which they ['asah](../../strongs/h/h6213.md).

<a name="2kings_17_20"></a>2Kings 17:20

And [Yĕhovah](../../strongs/h/h3068.md) [mā'as](../../strongs/h/h3988.md) all the [zera'](../../strongs/h/h2233.md) of [Yisra'el](../../strongs/h/h3478.md), and [ʿānâ](../../strongs/h/h6031.md) them, and [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of [šāsâ](../../strongs/h/h8154.md), until he had [shalak](../../strongs/h/h7993.md) them out of his [paniym](../../strongs/h/h6440.md).

<a name="2kings_17_21"></a>2Kings 17:21

For he [qāraʿ](../../strongs/h/h7167.md) [Yisra'el](../../strongs/h/h3478.md) from the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md); and they made [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md) [mālaḵ](../../strongs/h/h4427.md): and [YārāḇʿĀm](../../strongs/h/h3379.md) [nāḏaḥ](../../strongs/h/h5080.md) [nāḏâ](../../strongs/h/h5077.md) [Yisra'el](../../strongs/h/h3478.md) from ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md), and made them [chata'](../../strongs/h/h2398.md) a [gadowl](../../strongs/h/h1419.md) [ḥăṭā'â](../../strongs/h/h2401.md).

<a name="2kings_17_22"></a>2Kings 17:22

For the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yālaḵ](../../strongs/h/h3212.md) in all the [chatta'ath](../../strongs/h/h2403.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) which he ['asah](../../strongs/h/h6213.md); they [cuwr](../../strongs/h/h5493.md) not from them;

<a name="2kings_17_23"></a>2Kings 17:23

Until [Yĕhovah](../../strongs/h/h3068.md) [cuwr](../../strongs/h/h5493.md) [Yisra'el](../../strongs/h/h3478.md) out of his [paniym](../../strongs/h/h6440.md), as he had [dabar](../../strongs/h/h1696.md) [yad](../../strongs/h/h3027.md) all his ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md). So was [Yisra'el](../../strongs/h/h3478.md) [gālâ](../../strongs/h/h1540.md) out of their own ['ăḏāmâ](../../strongs/h/h127.md) to ['Aššûr](../../strongs/h/h804.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="2kings_17_24"></a>2Kings 17:24

And the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [bow'](../../strongs/h/h935.md) men from [Bāḇel](../../strongs/h/h894.md), and from [Kûṯ](../../strongs/h/h3575.md), and from [ʿIûâ](../../strongs/h/h5755.md), and from [Ḥămāṯ](../../strongs/h/h2574.md), and from [Sᵊp̄arvayim](../../strongs/h/h5617.md), and [yashab](../../strongs/h/h3427.md) them in the [ʿîr](../../strongs/h/h5892.md) of [Šōmrôn](../../strongs/h/h8111.md) instead of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): and they [yarash](../../strongs/h/h3423.md) [Šōmrôn](../../strongs/h/h8111.md), and [yashab](../../strongs/h/h3427.md) in the [ʿîr](../../strongs/h/h5892.md) thereof.

<a name="2kings_17_25"></a>2Kings 17:25

And so it was at the [tᵊḥillâ](../../strongs/h/h8462.md) of their [yashab](../../strongs/h/h3427.md) there, that they [yare'](../../strongs/h/h3372.md) not [Yĕhovah](../../strongs/h/h3068.md): therefore [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) ['ariy](../../strongs/h/h738.md) among them, which [harag](../../strongs/h/h2026.md) some of them.

<a name="2kings_17_26"></a>2Kings 17:26

Wherefore they ['āmar](../../strongs/h/h559.md) to the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), ['āmar](../../strongs/h/h559.md), The [gowy](../../strongs/h/h1471.md) which thou hast [gālâ](../../strongs/h/h1540.md), and [yashab](../../strongs/h/h3427.md) in the [ʿîr](../../strongs/h/h5892.md) of [Šōmrôn](../../strongs/h/h8111.md), [yada'](../../strongs/h/h3045.md) not the [mishpat](../../strongs/h/h4941.md) of the ['Elohiym](../../strongs/h/h430.md) of the ['erets](../../strongs/h/h776.md): therefore he hath [shalach](../../strongs/h/h7971.md) ['ariy](../../strongs/h/h738.md) among them, and, behold, they [muwth](../../strongs/h/h4191.md) them, because they [yada'](../../strongs/h/h3045.md) not the [mishpat](../../strongs/h/h4941.md) of the ['Elohiym](../../strongs/h/h430.md) of the ['erets](../../strongs/h/h776.md).

<a name="2kings_17_27"></a>2Kings 17:27

Then the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [tsavah](../../strongs/h/h6680.md), ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) thither one of the [kōhēn](../../strongs/h/h3548.md) whom ye [gālâ](../../strongs/h/h1540.md) from thence; and let them [yālaḵ](../../strongs/h/h3212.md) and [yashab](../../strongs/h/h3427.md) there, and let him [yārâ](../../strongs/h/h3384.md) them the [mishpat](../../strongs/h/h4941.md) of the ['Elohiym](../../strongs/h/h430.md) of the ['erets](../../strongs/h/h776.md).

<a name="2kings_17_28"></a>2Kings 17:28

Then one of the [kōhēn](../../strongs/h/h3548.md) whom they had [gālâ](../../strongs/h/h1540.md) from [Šōmrôn](../../strongs/h/h8111.md) [bow'](../../strongs/h/h935.md) and [yashab](../../strongs/h/h3427.md) in [Bêṯ-'ēl](../../strongs/h/h1008.md), and [yārâ](../../strongs/h/h3384.md) them how they should [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_17_29"></a>2Kings 17:29

Howbeit every [gowy](../../strongs/h/h1471.md) ['asah](../../strongs/h/h6213.md) ['Elohiym](../../strongs/h/h430.md) of their own, and [yānaḥ](../../strongs/h/h3240.md) them in the [bayith](../../strongs/h/h1004.md) of the [bāmâ](../../strongs/h/h1116.md) which the [Šōmrōnî](../../strongs/h/h8118.md) had ['asah](../../strongs/h/h6213.md), every [gowy](../../strongs/h/h1471.md) in their [ʿîr](../../strongs/h/h5892.md) wherein they [yashab](../../strongs/h/h3427.md).

<a name="2kings_17_30"></a>2Kings 17:30

And the ['enowsh](../../strongs/h/h582.md) of [Bāḇel](../../strongs/h/h894.md) ['asah](../../strongs/h/h6213.md) [Sukôṯ bᵊnôṯ](../../strongs/h/h5524.md), and the ['enowsh](../../strongs/h/h582.md) of [Kûṯ](../../strongs/h/h3575.md) ['asah](../../strongs/h/h6213.md) [Nērḡal](../../strongs/h/h5370.md), and the ['enowsh](../../strongs/h/h582.md) of [Ḥămāṯ](../../strongs/h/h2574.md) ['asah](../../strongs/h/h6213.md) ['Ăšîmā'](../../strongs/h/h807.md),

<a name="2kings_17_31"></a>2Kings 17:31

And the [ʿAûî](../../strongs/h/h5757.md) ['asah](../../strongs/h/h6213.md) [Niḇḥaz](../../strongs/h/h5026.md) and [Tartāq](../../strongs/h/h8662.md), and the [Sᵊp̄arvî](../../strongs/h/h5616.md) [śārap̄](../../strongs/h/h8313.md) their [ben](../../strongs/h/h1121.md) in ['esh](../../strongs/h/h784.md) to ['Aḏrammeleḵ](../../strongs/h/h152.md) and [ʿĂnammeleḵ](../../strongs/h/h6048.md), the ['Elohiym](../../strongs/h/h430.md) of [Sᵊp̄arvayim](../../strongs/h/h5617.md).

<a name="2kings_17_32"></a>2Kings 17:32

So they [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md), and ['asah](../../strongs/h/h6213.md) unto themselves of the [qāṣâ](../../strongs/h/h7098.md) of them [kōhēn](../../strongs/h/h3548.md) of the [bāmâ](../../strongs/h/h1116.md), which ['asah](../../strongs/h/h6213.md) for them in the [bayith](../../strongs/h/h1004.md) of the [bāmâ](../../strongs/h/h1116.md).

<a name="2kings_17_33"></a>2Kings 17:33

They [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md), and ['abad](../../strongs/h/h5647.md) their own ['Elohiym](../../strongs/h/h430.md), after the [mishpat](../../strongs/h/h4941.md) of the [gowy](../../strongs/h/h1471.md) whom they [gālâ](../../strongs/h/h1540.md) from thence.

<a name="2kings_17_34"></a>2Kings 17:34

Unto this [yowm](../../strongs/h/h3117.md) they ['asah](../../strongs/h/h6213.md) after the [ri'šôn](../../strongs/h/h7223.md) [mishpat](../../strongs/h/h4941.md): they [yārē'](../../strongs/h/h3373.md) not [Yĕhovah](../../strongs/h/h3068.md), neither ['asah](../../strongs/h/h6213.md) they after their [chuqqah](../../strongs/h/h2708.md), or after their [mishpat](../../strongs/h/h4941.md), or after the [towrah](../../strongs/h/h8451.md) and [mitsvah](../../strongs/h/h4687.md) which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) the [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md), whom he [śûm](../../strongs/h/h7760.md) [shem](../../strongs/h/h8034.md) [Yisra'el](../../strongs/h/h3478.md);

<a name="2kings_17_35"></a>2Kings 17:35

With whom [Yĕhovah](../../strongs/h/h3068.md) had [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md), and [tsavah](../../strongs/h/h6680.md) them, ['āmar](../../strongs/h/h559.md), Ye shall not [yare'](../../strongs/h/h3372.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), nor [shachah](../../strongs/h/h7812.md) yourselves to them, nor ['abad](../../strongs/h/h5647.md) them, nor [zabach](../../strongs/h/h2076.md) to them:

<a name="2kings_17_36"></a>2Kings 17:36

But [Yĕhovah](../../strongs/h/h3068.md), who you [ʿālâ](../../strongs/h/h5927.md) you out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) with [gadowl](../../strongs/h/h1419.md) [koach](../../strongs/h/h3581.md) and a [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md), him shall ye [yare'](../../strongs/h/h3372.md), and him shall ye [shachah](../../strongs/h/h7812.md), and to him shall ye do [zabach](../../strongs/h/h2076.md).

<a name="2kings_17_37"></a>2Kings 17:37

And the [choq](../../strongs/h/h2706.md), and the [mishpat](../../strongs/h/h4941.md), and the [towrah](../../strongs/h/h8451.md), and the [mitsvah](../../strongs/h/h4687.md), which he [kāṯaḇ](../../strongs/h/h3789.md) for you, ye shall [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) for [yowm](../../strongs/h/h3117.md); and ye shall not [yare'](../../strongs/h/h3372.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md).

<a name="2kings_17_38"></a>2Kings 17:38

And the [bĕriyth](../../strongs/h/h1285.md) that I have [karath](../../strongs/h/h3772.md) with you ye shall not [shakach](../../strongs/h/h7911.md); neither shall ye [yare'](../../strongs/h/h3372.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md).

<a name="2kings_17_39"></a>2Kings 17:39

But [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) ye shall [yare'](../../strongs/h/h3372.md); and he shall [natsal](../../strongs/h/h5337.md) you out of the [yad](../../strongs/h/h3027.md) of all your ['oyeb](../../strongs/h/h341.md).

<a name="2kings_17_40"></a>2Kings 17:40

Howbeit they did not [shama'](../../strongs/h/h8085.md), but they ['asah](../../strongs/h/h6213.md) after their [ri'šôn](../../strongs/h/h7223.md) [mishpat](../../strongs/h/h4941.md).

<a name="2kings_17_41"></a>2Kings 17:41

So these [gowy](../../strongs/h/h1471.md) [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md), and ['abad](../../strongs/h/h5647.md) their [pāsîl](../../strongs/h/h6456.md), both their [ben](../../strongs/h/h1121.md), and their [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md): as ['asah](../../strongs/h/h6213.md) their ['ab](../../strongs/h/h1.md), so ['asah](../../strongs/h/h6213.md) they unto this [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 16](2kings_16.md) - [2Kings 18](2kings_18.md)