# [2Kings 9](https://www.blueletterbible.org/kjv/2kings/9)

<a name="2kings_9_1"></a>2Kings 9:1

And ['Ĕlîšāʿ](../../strongs/h/h477.md) the [nāḇî'](../../strongs/h/h5030.md) [qara'](../../strongs/h/h7121.md) one of the [ben](../../strongs/h/h1121.md) of the [nāḇî'](../../strongs/h/h5030.md), and ['āmar](../../strongs/h/h559.md) unto him, [ḥāḡar](../../strongs/h/h2296.md) thy [māṯnayim](../../strongs/h/h4975.md), and [laqach](../../strongs/h/h3947.md) this [paḵ](../../strongs/h/h6378.md) of [šemen](../../strongs/h/h8081.md) in thine [yad](../../strongs/h/h3027.md), and [yālaḵ](../../strongs/h/h3212.md) to [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md):

<a name="2kings_9_2"></a>2Kings 9:2

And when thou [bow'](../../strongs/h/h935.md) thither, look [ra'ah](../../strongs/h/h7200.md) there [Yêû'](../../strongs/h/h3058.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [ben](../../strongs/h/h1121.md) of [Nimšî](../../strongs/h/h5250.md), and [bow'](../../strongs/h/h935.md), and make him [quwm](../../strongs/h/h6965.md) from among his ['ach](../../strongs/h/h251.md), and [bow'](../../strongs/h/h935.md) him to an [ḥeḏer](../../strongs/h/h2315.md) [ḥeḏer](../../strongs/h/h2315.md);

<a name="2kings_9_3"></a>2Kings 9:3

Then [laqach](../../strongs/h/h3947.md) the [paḵ](../../strongs/h/h6378.md) of [šemen](../../strongs/h/h8081.md), and [yāṣaq](../../strongs/h/h3332.md) it on his [ro'sh](../../strongs/h/h7218.md), and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), I have [māšaḥ](../../strongs/h/h4886.md) thee [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md). Then [pāṯaḥ](../../strongs/h/h6605.md) the [deleṯ](../../strongs/h/h1817.md), and [nûs](../../strongs/h/h5127.md), and [ḥāḵâ](../../strongs/h/h2442.md) not.

<a name="2kings_9_4"></a>2Kings 9:4

So the [naʿar](../../strongs/h/h5288.md), even the [naʿar](../../strongs/h/h5288.md) the [nāḇî'](../../strongs/h/h5030.md), [yālaḵ](../../strongs/h/h3212.md) to [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="2kings_9_5"></a>2Kings 9:5

And when he [bow'](../../strongs/h/h935.md), behold, the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md) were [yashab](../../strongs/h/h3427.md); and he ['āmar](../../strongs/h/h559.md), I have a [dabar](../../strongs/h/h1697.md) to thee, O [śar](../../strongs/h/h8269.md). And [Yêû'](../../strongs/h/h3058.md) ['āmar](../../strongs/h/h559.md), Unto which of all us? And he ['āmar](../../strongs/h/h559.md), To thee, O [śar](../../strongs/h/h8269.md).

<a name="2kings_9_6"></a>2Kings 9:6

And he [quwm](../../strongs/h/h6965.md), and [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md); and he [yāṣaq](../../strongs/h/h3332.md) the [šemen](../../strongs/h/h8081.md) on his [ro'sh](../../strongs/h/h7218.md), and ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), I have [māšaḥ](../../strongs/h/h4886.md) thee [melek](../../strongs/h/h4428.md) over the ['am](../../strongs/h/h5971.md) of [Yĕhovah](../../strongs/h/h3068.md), even over [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_9_7"></a>2Kings 9:7

And thou shalt [nakah](../../strongs/h/h5221.md) the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) thy ['adown](../../strongs/h/h113.md), that I may [naqam](../../strongs/h/h5358.md) the [dam](../../strongs/h/h1818.md) of my ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md), and the [dam](../../strongs/h/h1818.md) of all the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md), at the [yad](../../strongs/h/h3027.md) of ['Îzeḇel](../../strongs/h/h348.md).

<a name="2kings_9_8"></a>2Kings 9:8

For the whole [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) shall ['abad](../../strongs/h/h6.md): and I will [karath](../../strongs/h/h3772.md) from ['Aḥ'Āḇ](../../strongs/h/h256.md) him that [šāṯan](../../strongs/h/h8366.md) against the [qîr](../../strongs/h/h7023.md), and him that is [ʿāṣar](../../strongs/h/h6113.md) and ['azab](../../strongs/h/h5800.md) in [Yisra'el](../../strongs/h/h3478.md):

<a name="2kings_9_9"></a>2Kings 9:9

And I will [nathan](../../strongs/h/h5414.md) the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) like the [bayith](../../strongs/h/h1004.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), and like the [bayith](../../strongs/h/h1004.md) of [BaʿŠā'](../../strongs/h/h1201.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîyâ](../../strongs/h/h281.md):

<a name="2kings_9_10"></a>2Kings 9:10

And the [keleḇ](../../strongs/h/h3611.md) shall ['akal](../../strongs/h/h398.md) ['Îzeḇel](../../strongs/h/h348.md) in the [cheleq](../../strongs/h/h2506.md) of [YizrᵊʿE'L](../../strongs/h/h3157.md), and there shall be none to [qāḇar](../../strongs/h/h6912.md) her. And he [pāṯaḥ](../../strongs/h/h6605.md) the [deleṯ](../../strongs/h/h1817.md), and [nûs](../../strongs/h/h5127.md).

<a name="2kings_9_11"></a>2Kings 9:11

Then [Yêû'](../../strongs/h/h3058.md) [yāṣā'](../../strongs/h/h3318.md) to the ['ebed](../../strongs/h/h5650.md) of his ['adown](../../strongs/h/h113.md): and one ['āmar](../../strongs/h/h559.md) unto him, Is all [shalowm](../../strongs/h/h7965.md)? wherefore [bow'](../../strongs/h/h935.md) this [šāḡaʿ](../../strongs/h/h7696.md) to thee? And he ['āmar](../../strongs/h/h559.md) unto them, Ye [yada'](../../strongs/h/h3045.md) the ['iysh](../../strongs/h/h376.md), and his [śîaḥ](../../strongs/h/h7879.md).

<a name="2kings_9_12"></a>2Kings 9:12

And they ['āmar](../../strongs/h/h559.md), It is [sheqer](../../strongs/h/h8267.md); [nāḡaḏ](../../strongs/h/h5046.md) us now. And he ['āmar](../../strongs/h/h559.md), Thus and thus ['āmar](../../strongs/h/h559.md) he to me, ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), I have [māšaḥ](../../strongs/h/h4886.md) thee [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_9_13"></a>2Kings 9:13

Then they [māhar](../../strongs/h/h4116.md), and [laqach](../../strongs/h/h3947.md) every ['iysh](../../strongs/h/h376.md) his [beḡeḏ](../../strongs/h/h899.md), and [śûm](../../strongs/h/h7760.md) it under him on the [gerem](../../strongs/h/h1634.md) of the [maʿălâ](../../strongs/h/h4609.md), and [tāqaʿ](../../strongs/h/h8628.md) with [šôp̄ār](../../strongs/h/h7782.md), ['āmar](../../strongs/h/h559.md), [Yêû'](../../strongs/h/h3058.md) is [mālaḵ](../../strongs/h/h4427.md).

<a name="2kings_9_14"></a>2Kings 9:14

So [Yêû'](../../strongs/h/h3058.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [ben](../../strongs/h/h1121.md) of [Nimšî](../../strongs/h/h5250.md) [qāšar](../../strongs/h/h7194.md) against [Yôrām](../../strongs/h/h3141.md). (Now [Yôrām](../../strongs/h/h3141.md) had [shamar](../../strongs/h/h8104.md) [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md), he and all [Yisra'el](../../strongs/h/h3478.md), [paniym](../../strongs/h/h6440.md) of [Ḥăzā'Ēl](../../strongs/h/h2371.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md).

<a name="2kings_9_15"></a>2Kings 9:15

But [melek](../../strongs/h/h4428.md) [Yᵊhôrām](../../strongs/h/h3088.md) was [shuwb](../../strongs/h/h7725.md) to be [rapha'](../../strongs/h/h7495.md) in [YizrᵊʿE'L](../../strongs/h/h3157.md) of the [makâ](../../strongs/h/h4347.md) which the ['Ărammy](../../strongs/h/h761.md) had [nakah](../../strongs/h/h5221.md) him, when he [lāḥam](../../strongs/h/h3898.md) with [Ḥăzā'Ēl](../../strongs/h/h2371.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md).) And [Yêû'](../../strongs/h/h3058.md) ['āmar](../../strongs/h/h559.md), If it be your [nephesh](../../strongs/h/h5315.md), then let none [yāṣā'](../../strongs/h/h3318.md) nor [pālîṭ](../../strongs/h/h6412.md) out of the [ʿîr](../../strongs/h/h5892.md) to [yālaḵ](../../strongs/h/h3212.md) to [nāḡaḏ](../../strongs/h/h5046.md) it in [YizrᵊʿE'L](../../strongs/h/h3157.md).

<a name="2kings_9_16"></a>2Kings 9:16

So [Yêû'](../../strongs/h/h3058.md) rode in a [rāḵaḇ](../../strongs/h/h7392.md), and [yālaḵ](../../strongs/h/h3212.md) to [YizrᵊʿE'L](../../strongs/h/h3157.md); for [Yôrām](../../strongs/h/h3141.md) [shakab](../../strongs/h/h7901.md) there. And ['Ăḥazyâ](../../strongs/h/h274.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) was [yarad](../../strongs/h/h3381.md) to [ra'ah](../../strongs/h/h7200.md) [Yôrām](../../strongs/h/h3141.md).

<a name="2kings_9_17"></a>2Kings 9:17

And there ['amad](../../strongs/h/h5975.md) a [tsaphah](../../strongs/h/h6822.md) on the [miḡdāl](../../strongs/h/h4026.md) in [YizrᵊʿE'L](../../strongs/h/h3157.md), and he [ra'ah](../../strongs/h/h7200.md) the [šip̄ʿâ](../../strongs/h/h8229.md) of [Yêû'](../../strongs/h/h3058.md) as he [bow'](../../strongs/h/h935.md), and ['āmar](../../strongs/h/h559.md), I [ra'ah](../../strongs/h/h7200.md) a [šip̄ʿâ](../../strongs/h/h8229.md). And [Yᵊhôrām](../../strongs/h/h3088.md) ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) a [rakāḇ](../../strongs/h/h7395.md), and [shalach](../../strongs/h/h7971.md) to [qārā'](../../strongs/h/h7125.md) them, and let him ['āmar](../../strongs/h/h559.md), Is it [shalowm](../../strongs/h/h7965.md)?

<a name="2kings_9_18"></a>2Kings 9:18

So there [yālaḵ](../../strongs/h/h3212.md) one on [rāḵaḇ](../../strongs/h/h7392.md) [sûs](../../strongs/h/h5483.md) to [qārā'](../../strongs/h/h7125.md) him, and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md), Is it [shalowm](../../strongs/h/h7965.md)? And [Yêû'](../../strongs/h/h3058.md) ['āmar](../../strongs/h/h559.md), What hast thou to do with [shalowm](../../strongs/h/h7965.md)? [cabab](../../strongs/h/h5437.md) thee ['aḥar](../../strongs/h/h310.md) me. And the [tsaphah](../../strongs/h/h6822.md) [nāḡaḏ](../../strongs/h/h5046.md), ['āmar](../../strongs/h/h559.md), The [mal'ak](../../strongs/h/h4397.md) [bow'](../../strongs/h/h935.md) to them, but he [shuwb](../../strongs/h/h7725.md) not.

<a name="2kings_9_19"></a>2Kings 9:19

Then he [shalach](../../strongs/h/h7971.md) a second on [rāḵaḇ](../../strongs/h/h7392.md) [sûs](../../strongs/h/h5483.md), which [bow'](../../strongs/h/h935.md) to them, and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md), Is it [shalowm](../../strongs/h/h7965.md)? And [Yêû'](../../strongs/h/h3058.md) ['āmar](../../strongs/h/h559.md), What hast thou to do with [shalowm](../../strongs/h/h7965.md)? [cabab](../../strongs/h/h5437.md) thee ['aḥar](../../strongs/h/h310.md) me.

<a name="2kings_9_20"></a>2Kings 9:20

And the [tsaphah](../../strongs/h/h6822.md) [nāḡaḏ](../../strongs/h/h5046.md), ['āmar](../../strongs/h/h559.md), He [bow'](../../strongs/h/h935.md) even unto them, and cometh not [shuwb](../../strongs/h/h7725.md): and the [minhāḡ](../../strongs/h/h4491.md) is like the [minhāḡ](../../strongs/h/h4491.md) of [Yêû'](../../strongs/h/h3058.md) the [ben](../../strongs/h/h1121.md) of [Nimšî](../../strongs/h/h5250.md); for he [nāhaḡ](../../strongs/h/h5090.md) [šigāʿôn](../../strongs/h/h7697.md).

<a name="2kings_9_21"></a>2Kings 9:21

And [Yᵊhôrām](../../strongs/h/h3088.md) ['āmar](../../strongs/h/h559.md), ['āsar](../../strongs/h/h631.md). And his [reḵeḇ](../../strongs/h/h7393.md) was made ['āsar](../../strongs/h/h631.md). And [Yᵊhôrām](../../strongs/h/h3088.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) and ['Ăḥazyâ](../../strongs/h/h274.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [yāṣā'](../../strongs/h/h3318.md), ['iysh](../../strongs/h/h376.md) in his [reḵeḇ](../../strongs/h/h7393.md), and they [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) [Yêû'](../../strongs/h/h3058.md), and [māṣā'](../../strongs/h/h4672.md) him in the [ḥelqâ](../../strongs/h/h2513.md) of [Nāḇôṯ](../../strongs/h/h5022.md) the [Yizrᵊʿē'lî](../../strongs/h/h3158.md).

<a name="2kings_9_22"></a>2Kings 9:22

And it came to pass, when [Yᵊhôrām](../../strongs/h/h3088.md) [ra'ah](../../strongs/h/h7200.md) [Yêû'](../../strongs/h/h3058.md), that he ['āmar](../../strongs/h/h559.md), Is it [shalowm](../../strongs/h/h7965.md), [Yêû'](../../strongs/h/h3058.md)? And he ['āmar](../../strongs/h/h559.md), What [shalowm](../../strongs/h/h7965.md), so long as the [zᵊnûnîm](../../strongs/h/h2183.md) of thy ['em](../../strongs/h/h517.md) ['Îzeḇel](../../strongs/h/h348.md) and her [kešep̄](../../strongs/h/h3785.md) are so [rab](../../strongs/h/h7227.md)?

<a name="2kings_9_23"></a>2Kings 9:23

And [Yᵊhôrām](../../strongs/h/h3088.md) [hāp̄aḵ](../../strongs/h/h2015.md) his [yad](../../strongs/h/h3027.md), and [nûs](../../strongs/h/h5127.md), and ['āmar](../../strongs/h/h559.md) to ['Ăḥazyâ](../../strongs/h/h274.md), There is [mirmah](../../strongs/h/h4820.md), O ['Ăḥazyâ](../../strongs/h/h274.md).

<a name="2kings_9_24"></a>2Kings 9:24

And [Yêû'](../../strongs/h/h3058.md) drew a [qesheth](../../strongs/h/h7198.md) with his [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md), and [nakah](../../strongs/h/h5221.md) [Yᵊhôrām](../../strongs/h/h3088.md) between his [zerowa'](../../strongs/h/h2220.md), and the [ḥēṣî](../../strongs/h/h2678.md) [yāṣā'](../../strongs/h/h3318.md) at his [leb](../../strongs/h/h3820.md), and he [kara'](../../strongs/h/h3766.md) in his [reḵeḇ](../../strongs/h/h7393.md).

<a name="2kings_9_25"></a>2Kings 9:25

Then ['āmar](../../strongs/h/h559.md) to [Biḏqar](../../strongs/h/h920.md) his [šālîš](../../strongs/h/h7991.md), [nasa'](../../strongs/h/h5375.md), and [shalak](../../strongs/h/h7993.md) him in the [ḥelqâ](../../strongs/h/h2513.md) of the [sadeh](../../strongs/h/h7704.md) of [Nāḇôṯ](../../strongs/h/h5022.md) the [Yizrᵊʿē'lî](../../strongs/h/h3158.md): for [zakar](../../strongs/h/h2142.md) how that, when I and thou [rāḵaḇ](../../strongs/h/h7392.md) [ṣemeḏ](../../strongs/h/h6776.md) ['aḥar](../../strongs/h/h310.md) ['Aḥ'Āḇ](../../strongs/h/h256.md) his ['ab](../../strongs/h/h1.md), [Yĕhovah](../../strongs/h/h3068.md) [nasa'](../../strongs/h/h5375.md) this [maśśā'](../../strongs/h/h4853.md) upon him;

<a name="2kings_9_26"></a>2Kings 9:26

Surely I have [ra'ah](../../strongs/h/h7200.md) ['emeš](../../strongs/h/h570.md) the [dam](../../strongs/h/h1818.md) of [Nāḇôṯ](../../strongs/h/h5022.md), and the [dam](../../strongs/h/h1818.md) of his [ben](../../strongs/h/h1121.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); and I will [shalam](../../strongs/h/h7999.md) thee in this [ḥelqâ](../../strongs/h/h2513.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md). Now therefore [nasa'](../../strongs/h/h5375.md) and [shalak](../../strongs/h/h7993.md) him into the [ḥelqâ](../../strongs/h/h2513.md), according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_9_27"></a>2Kings 9:27

But when ['Ăḥazyâ](../../strongs/h/h274.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [ra'ah](../../strongs/h/h7200.md) this, he [nûs](../../strongs/h/h5127.md) by the [derek](../../strongs/h/h1870.md) of the [gan](../../strongs/h/h1588.md) [bayith](../../strongs/h/h1004.md). And [Yêû'](../../strongs/h/h3058.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) him, and ['āmar](../../strongs/h/h559.md), [nakah](../../strongs/h/h5221.md) him also in the [merkāḇâ](../../strongs/h/h4818.md). And they did so at the [maʿălê](../../strongs/h/h4608.md) to [Gûr](../../strongs/h/h1483.md), which is by [YiḇlᵊʿĀm](../../strongs/h/h2991.md). And he [nûs](../../strongs/h/h5127.md) to [Mᵊḡidôn](../../strongs/h/h4023.md), and [muwth](../../strongs/h/h4191.md) there.

<a name="2kings_9_28"></a>2Kings 9:28

And his ['ebed](../../strongs/h/h5650.md) [rāḵaḇ](../../strongs/h/h7392.md) him in a chariot to [Yĕruwshalaim](../../strongs/h/h3389.md), and [qāḇar](../../strongs/h/h6912.md) him in his [qᵊḇûrâ](../../strongs/h/h6900.md) with his ['ab](../../strongs/h/h1.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="2kings_9_29"></a>2Kings 9:29

And in the eleventh [šānâ](../../strongs/h/h8141.md) [šānâ](../../strongs/h/h8141.md) of [Yôrām](../../strongs/h/h3141.md) the [ben](../../strongs/h/h1121.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) began ['Ăḥazyâ](../../strongs/h/h274.md) to [mālaḵ](../../strongs/h/h4427.md) over [Yehuwdah](../../strongs/h/h3063.md).

<a name="2kings_9_30"></a>2Kings 9:30

And when [Yêû'](../../strongs/h/h3058.md) was [bow'](../../strongs/h/h935.md) to [YizrᵊʿE'L](../../strongs/h/h3157.md), ['Îzeḇel](../../strongs/h/h348.md) [shama'](../../strongs/h/h8085.md) of it; and she [śûm](../../strongs/h/h7760.md) [pûḵ](../../strongs/h/h6320.md) her ['ayin](../../strongs/h/h5869.md), and [yatab](../../strongs/h/h3190.md) her [ro'sh](../../strongs/h/h7218.md), and [šāqap̄](../../strongs/h/h8259.md) at a [ḥallôn](../../strongs/h/h2474.md).

<a name="2kings_9_31"></a>2Kings 9:31

And as [Yêû'](../../strongs/h/h3058.md) entered [bow'](../../strongs/h/h935.md) at the [sha'ar](../../strongs/h/h8179.md), she ['āmar](../../strongs/h/h559.md), Had [Zimrî](../../strongs/h/h2174.md) [shalowm](../../strongs/h/h7965.md), who [harag](../../strongs/h/h2026.md) his ['adown](../../strongs/h/h113.md)?

<a name="2kings_9_32"></a>2Kings 9:32

And he [nasa'](../../strongs/h/h5375.md) his [paniym](../../strongs/h/h6440.md) to the [ḥallôn](../../strongs/h/h2474.md), and ['āmar](../../strongs/h/h559.md), Who is on my side? who? And there [šāqap̄](../../strongs/h/h8259.md) to him two or three [sārîs](../../strongs/h/h5631.md).

<a name="2kings_9_33"></a>2Kings 9:33

And he ['āmar](../../strongs/h/h559.md), [šāmaṭ](../../strongs/h/h8058.md) her. So they [šāmaṭ](../../strongs/h/h8058.md) her: and some of her [dam](../../strongs/h/h1818.md) was [nāzâ](../../strongs/h/h5137.md) on the [qîr](../../strongs/h/h7023.md), and on the [sûs](../../strongs/h/h5483.md): and he [rāmas](../../strongs/h/h7429.md) her.

<a name="2kings_9_34"></a>2Kings 9:34

And when he was [bow'](../../strongs/h/h935.md), he did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and ['āmar](../../strongs/h/h559.md), [paqad](../../strongs/h/h6485.md), see now this ['arar](../../strongs/h/h779.md), and [qāḇar](../../strongs/h/h6912.md) her: for she is a [melek](../../strongs/h/h4428.md) [bath](../../strongs/h/h1323.md).

<a name="2kings_9_35"></a>2Kings 9:35

And they [yālaḵ](../../strongs/h/h3212.md) to [qāḇar](../../strongs/h/h6912.md) her: but they [māṣā'](../../strongs/h/h4672.md) no more of her than the [gulgōleṯ](../../strongs/h/h1538.md), and the [regel](../../strongs/h/h7272.md), and the [kaph](../../strongs/h/h3709.md) of her [yad](../../strongs/h/h3027.md).

<a name="2kings_9_36"></a>2Kings 9:36

Wherefore they [shuwb](../../strongs/h/h7725.md), and [nāḡaḏ](../../strongs/h/h5046.md) him. And he ['āmar](../../strongs/h/h559.md), This is the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [dabar](../../strongs/h/h1696.md) [yad](../../strongs/h/h3027.md) his ['ebed](../../strongs/h/h5650.md) ['Ēlîyâ](../../strongs/h/h452.md) the [Tišbî](../../strongs/h/h8664.md), ['āmar](../../strongs/h/h559.md), In the [cheleq](../../strongs/h/h2506.md) of [YizrᵊʿE'L](../../strongs/h/h3157.md) shall [keleḇ](../../strongs/h/h3611.md) ['akal](../../strongs/h/h398.md) the [basar](../../strongs/h/h1320.md) of ['Îzeḇel](../../strongs/h/h348.md):

<a name="2kings_9_37"></a>2Kings 9:37

And the [nᵊḇēlâ](../../strongs/h/h5038.md) of ['Îzeḇel](../../strongs/h/h348.md) shall be as [dōmen](../../strongs/h/h1828.md) upon the [paniym](../../strongs/h/h6440.md) of the [sadeh](../../strongs/h/h7704.md) in the [cheleq](../../strongs/h/h2506.md) of [YizrᵊʿE'L](../../strongs/h/h3157.md); so that they shall not ['āmar](../../strongs/h/h559.md), This is ['Îzeḇel](../../strongs/h/h348.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 8](2kings_8.md) - [2Kings 10](2kings_10.md)