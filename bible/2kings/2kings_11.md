# [2Kings 11](https://www.blueletterbible.org/kjv/2kings/11)

<a name="2kings_11_1"></a>2Kings 11:1

And when [ʿĂṯalyâ](../../strongs/h/h6271.md) the ['em](../../strongs/h/h517.md) of ['Ăḥazyâ](../../strongs/h/h274.md) [ra'ah](../../strongs/h/h7200.md) that her [ben](../../strongs/h/h1121.md) was [muwth](../../strongs/h/h4191.md), she [quwm](../../strongs/h/h6965.md) and ['abad](../../strongs/h/h6.md) all the [zera'](../../strongs/h/h2233.md) [mamlāḵâ](../../strongs/h/h4467.md).

<a name="2kings_11_2"></a>2Kings 11:2

But [Yᵊhôšeḇaʿ](../../strongs/h/h3089.md), the [bath](../../strongs/h/h1323.md) of [melek](../../strongs/h/h4428.md) [Yôrām](../../strongs/h/h3141.md), ['āḥôṯ](../../strongs/h/h269.md) of ['Ăḥazyâ](../../strongs/h/h274.md), [laqach](../../strongs/h/h3947.md) [Yô'Āš](../../strongs/h/h3101.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥazyâ](../../strongs/h/h274.md), and [ganab](../../strongs/h/h1589.md) him from among the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md) which were [muwth](../../strongs/h/h4191.md); and they [cathar](../../strongs/h/h5641.md) him, even him and his [yānaq](../../strongs/h/h3243.md), in the [ḥeḏer](../../strongs/h/h2315.md) [mittah](../../strongs/h/h4296.md) [paniym](../../strongs/h/h6440.md) [ʿĂṯalyâ](../../strongs/h/h6271.md), so that he was not [muwth](../../strongs/h/h4191.md).

<a name="2kings_11_3"></a>2Kings 11:3

And he was with her [chaba'](../../strongs/h/h2244.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) six [šānâ](../../strongs/h/h8141.md). And [ʿĂṯalyâ](../../strongs/h/h6271.md) did [mālaḵ](../../strongs/h/h4427.md) over the ['erets](../../strongs/h/h776.md).

<a name="2kings_11_4"></a>2Kings 11:4

And the seventh [šānâ](../../strongs/h/h8141.md) [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) [shalach](../../strongs/h/h7971.md) and [laqach](../../strongs/h/h3947.md) the [śar](../../strongs/h/h8269.md) over hundreds, with the [kārî](../../strongs/h/h3746.md) and the [rûṣ](../../strongs/h/h7323.md), and [bow'](../../strongs/h/h935.md) them to him into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with them, and took a [shaba'](../../strongs/h/h7650.md) of them in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and [ra'ah](../../strongs/h/h7200.md) them the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md).

<a name="2kings_11_5"></a>2Kings 11:5

And he [tsavah](../../strongs/h/h6680.md) them, ['āmar](../../strongs/h/h559.md), This is the [dabar](../../strongs/h/h1697.md) that ye shall ['asah](../../strongs/h/h6213.md); A third part of you that [bow'](../../strongs/h/h935.md) on the [shabbath](../../strongs/h/h7676.md) shall even be [shamar](../../strongs/h/h8104.md) of the [mišmereṯ](../../strongs/h/h4931.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md);

<a name="2kings_11_6"></a>2Kings 11:6

And a third part shall be at the [sha'ar](../../strongs/h/h8179.md) of [sûr](../../strongs/h/h5495.md); and a third part at the [sha'ar](../../strongs/h/h8179.md) ['aḥar](../../strongs/h/h310.md) the [rûṣ](../../strongs/h/h7323.md): so shall ye [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of the [bayith](../../strongs/h/h1004.md), that it be not [massāḥ](../../strongs/h/h4535.md).

<a name="2kings_11_7"></a>2Kings 11:7

And two [yad](../../strongs/h/h3027.md) of all you that [yāṣā'](../../strongs/h/h3318.md) on the [shabbath](../../strongs/h/h7676.md), even they shall [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) about the [melek](../../strongs/h/h4428.md).

<a name="2kings_11_8"></a>2Kings 11:8

And ye shall [naqaph](../../strongs/h/h5362.md) the [melek](../../strongs/h/h4428.md) [cabiyb](../../strongs/h/h5439.md), every ['iysh](../../strongs/h/h376.md) with his [kĕliy](../../strongs/h/h3627.md) in his [yad](../../strongs/h/h3027.md): and he that [bow'](../../strongs/h/h935.md) within the [śᵊḏērâ](../../strongs/h/h7713.md), let him be [muwth](../../strongs/h/h4191.md): and be ye with the [melek](../../strongs/h/h4428.md) as he [yāṣā'](../../strongs/h/h3318.md) and as he [bow'](../../strongs/h/h935.md).

<a name="2kings_11_9"></a>2Kings 11:9

And the [śar](../../strongs/h/h8269.md) over the hundreds ['asah](../../strongs/h/h6213.md) according to all things that [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md) [tsavah](../../strongs/h/h6680.md): and they [laqach](../../strongs/h/h3947.md) every ['iysh](../../strongs/h/h376.md) his ['enowsh](../../strongs/h/h582.md) that were to [bow'](../../strongs/h/h935.md) on the [shabbath](../../strongs/h/h7676.md), with them that should [yāṣā'](../../strongs/h/h3318.md) on the [shabbath](../../strongs/h/h7676.md), and [bow'](../../strongs/h/h935.md) to [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md).

<a name="2kings_11_10"></a>2Kings 11:10

And to the [śar](../../strongs/h/h8269.md) over hundreds did the [kōhēn](../../strongs/h/h3548.md) [nathan](../../strongs/h/h5414.md) [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) [ḥănîṯ](../../strongs/h/h2595.md) and [šeleṭ](../../strongs/h/h7982.md), that were in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_11_11"></a>2Kings 11:11

And the [rûṣ](../../strongs/h/h7323.md) ['amad](../../strongs/h/h5975.md), every ['iysh](../../strongs/h/h376.md) with his [kĕliy](../../strongs/h/h3627.md) in his [yad](../../strongs/h/h3027.md), [cabiyb](../../strongs/h/h5439.md) the [melek](../../strongs/h/h4428.md), from the [yᵊmānî](../../strongs/h/h3233.md) [kāṯēp̄](../../strongs/h/h3802.md) of the [bayith](../../strongs/h/h1004.md) to the [śᵊmā'lî](../../strongs/h/h8042.md) [kāṯēp̄](../../strongs/h/h3802.md) of the [bayith](../../strongs/h/h1004.md), along by the [mizbeach](../../strongs/h/h4196.md) and the [bayith](../../strongs/h/h1004.md).

<a name="2kings_11_12"></a>2Kings 11:12

And he [yāṣā'](../../strongs/h/h3318.md) the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md), and [nathan](../../strongs/h/h5414.md) the [nēzer](../../strongs/h/h5145.md) upon him, and gave him the [ʿēḏûṯ](../../strongs/h/h5715.md); and they made him [mālaḵ](../../strongs/h/h4427.md), and [māšaḥ](../../strongs/h/h4886.md) him; and they [nakah](../../strongs/h/h5221.md) their [kaph](../../strongs/h/h3709.md), and ['āmar](../../strongs/h/h559.md), [ḥāyâ](../../strongs/h/h2421.md) the [melek](../../strongs/h/h4428.md).

<a name="2kings_11_13"></a>2Kings 11:13

And when [ʿĂṯalyâ](../../strongs/h/h6271.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [rûṣ](../../strongs/h/h7323.md) and of the ['am](../../strongs/h/h5971.md), she [bow'](../../strongs/h/h935.md) to the ['am](../../strongs/h/h5971.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_11_14"></a>2Kings 11:14

And when she [ra'ah](../../strongs/h/h7200.md), behold, the [melek](../../strongs/h/h4428.md) ['amad](../../strongs/h/h5975.md) by a [ʿammûḏ](../../strongs/h/h5982.md), as the [mishpat](../../strongs/h/h4941.md) was, and the [śar](../../strongs/h/h8269.md) and the [ḥăṣōṣrâ](../../strongs/h/h2689.md) by the [melek](../../strongs/h/h4428.md), and all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) [śāmēaḥ](../../strongs/h/h8056.md), and [tāqaʿ](../../strongs/h/h8628.md) with [ḥăṣōṣrâ](../../strongs/h/h2689.md): and [ʿĂṯalyâ](../../strongs/h/h6271.md) [qāraʿ](../../strongs/h/h7167.md) her [beḡeḏ](../../strongs/h/h899.md), and [qara'](../../strongs/h/h7121.md), [qešer](../../strongs/h/h7195.md), [qešer](../../strongs/h/h7195.md).

<a name="2kings_11_15"></a>2Kings 11:15

But [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md) [tsavah](../../strongs/h/h6680.md) the [śar](../../strongs/h/h8269.md) of the hundreds, the [paqad](../../strongs/h/h6485.md) of the [ḥayil](../../strongs/h/h2428.md), and ['āmar](../../strongs/h/h559.md) unto them, Have her [yāṣā'](../../strongs/h/h3318.md) [bayith](../../strongs/h/h1004.md) the [śᵊḏērâ](../../strongs/h/h7713.md): and him that [bow'](../../strongs/h/h935.md) ['aḥar](../../strongs/h/h310.md) her [muwth](../../strongs/h/h4191.md) with the [chereb](../../strongs/h/h2719.md). For the [kōhēn](../../strongs/h/h3548.md) had ['āmar](../../strongs/h/h559.md), Let her not be [muwth](../../strongs/h/h4191.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_11_16"></a>2Kings 11:16

And they [śûm](../../strongs/h/h7760.md) [yad](../../strongs/h/h3027.md) on her; and she [bow'](../../strongs/h/h935.md) by the [derek](../../strongs/h/h1870.md) by the which the [sûs](../../strongs/h/h5483.md) [māḇô'](../../strongs/h/h3996.md) into the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md): and there was she [muwth](../../strongs/h/h4191.md).

<a name="2kings_11_17"></a>2Kings 11:17

And [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) between [Yĕhovah](../../strongs/h/h3068.md) and the [melek](../../strongs/h/h4428.md) and the ['am](../../strongs/h/h5971.md), that they should be [Yĕhovah](../../strongs/h/h3068.md) ['am](../../strongs/h/h5971.md); between the [melek](../../strongs/h/h4428.md) also and the ['am](../../strongs/h/h5971.md).

<a name="2kings_11_18"></a>2Kings 11:18

And all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [BaʿAl](../../strongs/h/h1168.md), and [nāṯaṣ](../../strongs/h/h5422.md) it; his [mizbeach](../../strongs/h/h4196.md) and his [tselem](../../strongs/h/h6754.md) brake they in [shabar](../../strongs/h/h7665.md) [yatab](../../strongs/h/h3190.md), and [harag](../../strongs/h/h2026.md) [Matān](../../strongs/h/h4977.md) the [kōhēn](../../strongs/h/h3548.md) of [BaʿAl](../../strongs/h/h1168.md) [paniym](../../strongs/h/h6440.md) the [mizbeach](../../strongs/h/h4196.md). And the [kōhēn](../../strongs/h/h3548.md) [śûm](../../strongs/h/h7760.md) [paqad](../../strongs/h/h6485.md) [pᵊqudâ](../../strongs/h/h6486.md) over the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_11_19"></a>2Kings 11:19

And he [laqach](../../strongs/h/h3947.md) the [śar](../../strongs/h/h8269.md) over hundreds, and the [kārî](../../strongs/h/h3746.md), and the [rûṣ](../../strongs/h/h7323.md), and all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md); and they [yarad](../../strongs/h/h3381.md) the [melek](../../strongs/h/h4428.md) from the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and [bow'](../../strongs/h/h935.md) by the [derek](../../strongs/h/h1870.md) of the [sha'ar](../../strongs/h/h8179.md) of the [rûṣ](../../strongs/h/h7323.md) to the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md). And he [yashab](../../strongs/h/h3427.md) on the [kicce'](../../strongs/h/h3678.md) of the [melek](../../strongs/h/h4428.md).

<a name="2kings_11_20"></a>2Kings 11:20

And all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) [samach](../../strongs/h/h8055.md), and the [ʿîr](../../strongs/h/h5892.md) was in [šāqaṭ](../../strongs/h/h8252.md): and they [muwth](../../strongs/h/h4191.md) [ʿĂṯalyâ](../../strongs/h/h6271.md) with the [chereb](../../strongs/h/h2719.md) beside the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md).

<a name="2kings_11_21"></a>2Kings 11:21

Seven [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) was [Yᵊhô'Āš](../../strongs/h/h3060.md) when he began to [mālaḵ](../../strongs/h/h4427.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 10](2kings_10.md) - [2Kings 12](2kings_12.md)