# [2Kings 25](https://www.blueletterbible.org/kjv/2kings/25)

<a name="2kings_25_1"></a>2Kings 25:1

And it came to pass in the ninth [šānâ](../../strongs/h/h8141.md) of his [mālaḵ](../../strongs/h/h4427.md), in the tenth [ḥōḏeš](../../strongs/h/h2320.md), in the tenth day of the [ḥōḏeš](../../strongs/h/h2320.md), that [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [bow'](../../strongs/h/h935.md), he, and all his [ḥayil](../../strongs/h/h2428.md), against [Yĕruwshalaim](../../strongs/h/h3389.md), and [ḥānâ](../../strongs/h/h2583.md) against it; and they [bānâ](../../strongs/h/h1129.md) [dāyēq](../../strongs/h/h1785.md) against it [cabiyb](../../strongs/h/h5439.md).

<a name="2kings_25_2"></a>2Kings 25:2

And the [ʿîr](../../strongs/h/h5892.md) was [bow'](../../strongs/h/h935.md) [māṣôr](../../strongs/h/h4692.md) unto the eleventh [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [Ṣḏqyh](../../strongs/h/h6667.md).

<a name="2kings_25_3"></a>2Kings 25:3

And on the ninth day of the fourth [ḥōḏeš](../../strongs/h/h2320.md) the [rāʿāḇ](../../strongs/h/h7458.md) [ḥāzaq](../../strongs/h/h2388.md) in the [ʿîr](../../strongs/h/h5892.md), and there was no [lechem](../../strongs/h/h3899.md) for the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md).

<a name="2kings_25_4"></a>2Kings 25:4

And the [ʿîr](../../strongs/h/h5892.md) was [bāqaʿ](../../strongs/h/h1234.md), and all the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md) by [layil](../../strongs/h/h3915.md) by the [derek](../../strongs/h/h1870.md) of the [sha'ar](../../strongs/h/h8179.md) between two [ḥômâ](../../strongs/h/h2346.md), which is by the [melek](../../strongs/h/h4428.md) [gan](../../strongs/h/h1588.md): (now the [Kaśdîmâ](../../strongs/h/h3778.md) were against the [ʿîr](../../strongs/h/h5892.md) [cabiyb](../../strongs/h/h5439.md):) and [yālaḵ](../../strongs/h/h3212.md) the [derek](../../strongs/h/h1870.md) toward the ['arabah](../../strongs/h/h6160.md).

<a name="2kings_25_5"></a>2Kings 25:5

And the [ḥayil](../../strongs/h/h2428.md) of the [Kaśdîmâ](../../strongs/h/h3778.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) the [melek](../../strongs/h/h4428.md), and [nāśaḡ](../../strongs/h/h5381.md) him in the ['arabah](../../strongs/h/h6160.md) of [Yᵊrēḥô](../../strongs/h/h3405.md): and all his [ḥayil](../../strongs/h/h2428.md) were [puwts](../../strongs/h/h6327.md) from him.

<a name="2kings_25_6"></a>2Kings 25:6

So they [tāp̄aś](../../strongs/h/h8610.md) the [melek](../../strongs/h/h4428.md), and [ʿālâ](../../strongs/h/h5927.md) him to the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) to [Riḇlâ](../../strongs/h/h7247.md); and they [dabar](../../strongs/h/h1696.md) [mishpat](../../strongs/h/h4941.md) upon him.

<a name="2kings_25_7"></a>2Kings 25:7

And they [šāḥaṭ](../../strongs/h/h7819.md) the [ben](../../strongs/h/h1121.md) of [Ṣḏqyh](../../strongs/h/h6667.md) before his ['ayin](../../strongs/h/h5869.md), and put [ʿāvar](../../strongs/h/h5786.md) the ['ayin](../../strongs/h/h5869.md) of [Ṣḏqyh](../../strongs/h/h6667.md), and ['āsar](../../strongs/h/h631.md) him with [nᵊḥšeṯ](../../strongs/h/h5178.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md), and [bow'](../../strongs/h/h935.md) him to [Bāḇel](../../strongs/h/h894.md).

<a name="2kings_25_8"></a>2Kings 25:8

And in the fifth [ḥōḏeš](../../strongs/h/h2320.md), on the seventh day of the [ḥōḏeš](../../strongs/h/h2320.md), which is the nineteenth [šānâ](../../strongs/h/h8141.md) [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), [bow'](../../strongs/h/h935.md) [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md), [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md), an ['ebed](../../strongs/h/h5650.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), unto [Yĕruwshalaim](../../strongs/h/h3389.md):

<a name="2kings_25_9"></a>2Kings 25:9

And he [śārap̄](../../strongs/h/h8313.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and all the [bayith](../../strongs/h/h1004.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and every [gadowl](../../strongs/h/h1419.md) [bayith](../../strongs/h/h1004.md) [śārap̄](../../strongs/h/h8313.md) he with ['esh](../../strongs/h/h784.md).

<a name="2kings_25_10"></a>2Kings 25:10

And all the [ḥayil](../../strongs/h/h2428.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), that were with the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md), [nāṯaṣ](../../strongs/h/h5422.md) the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) [cabiyb](../../strongs/h/h5439.md).

<a name="2kings_25_11"></a>2Kings 25:11

Now the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md) that were [šā'ar](../../strongs/h/h7604.md) in the [ʿîr](../../strongs/h/h5892.md), and the [naphal](../../strongs/h/h5307.md) that [naphal](../../strongs/h/h5307.md) to the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), with the [yeṯer](../../strongs/h/h3499.md) of the [hāmôn](../../strongs/h/h1995.md), did [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md) the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [gālâ](../../strongs/h/h1540.md).

<a name="2kings_25_12"></a>2Kings 25:12

But the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [šā'ar](../../strongs/h/h7604.md) of the [dallâ](../../strongs/h/h1803.md) of the ['erets](../../strongs/h/h776.md) to be [kōrēm](../../strongs/h/h3755.md) and [yāḡaḇ](../../strongs/h/h3009.md) [gûḇ](../../strongs/h/h1461.md) .

<a name="2kings_25_13"></a>2Kings 25:13

And the [ʿammûḏ](../../strongs/h/h5982.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) that were in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [mᵊḵônâ](../../strongs/h/h4350.md), and the [nᵊḥšeṯ](../../strongs/h/h5178.md) [yam](../../strongs/h/h3220.md) that was in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), did the [Kaśdîmâ](../../strongs/h/h3778.md) [shabar](../../strongs/h/h7665.md), and [nasa'](../../strongs/h/h5375.md) the [nᵊḥšeṯ](../../strongs/h/h5178.md) of them to [Bāḇel](../../strongs/h/h894.md).

<a name="2kings_25_14"></a>2Kings 25:14

And the [sîr](../../strongs/h/h5518.md), and the [yāʿ](../../strongs/h/h3257.md), and the [mᵊzammerê](../../strongs/h/h4212.md), and the [kaph](../../strongs/h/h3709.md), and all the [kĕliy](../../strongs/h/h3627.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) wherewith they [sharath](../../strongs/h/h8334.md), they [laqach](../../strongs/h/h3947.md).

<a name="2kings_25_15"></a>2Kings 25:15

And the [maḥtâ](../../strongs/h/h4289.md), and the [mizrāq](../../strongs/h/h4219.md), and such things as were of [zāhāḇ](../../strongs/h/h2091.md), in [zāhāḇ](../../strongs/h/h2091.md), and of [keceph](../../strongs/h/h3701.md), in [keceph](../../strongs/h/h3701.md), the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [laqach](../../strongs/h/h3947.md).

<a name="2kings_25_16"></a>2Kings 25:16

The two [ʿammûḏ](../../strongs/h/h5982.md), one [yam](../../strongs/h/h3220.md), and the [mᵊḵônâ](../../strongs/h/h4350.md) which [Šᵊlōmô](../../strongs/h/h8010.md) had ['asah](../../strongs/h/h6213.md) for the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md); the [nᵊḥšeṯ](../../strongs/h/h5178.md) of all these [kĕliy](../../strongs/h/h3627.md) was without [mišqāl](../../strongs/h/h4948.md).

<a name="2kings_25_17"></a>2Kings 25:17

The [qômâ](../../strongs/h/h6967.md) of the one [ʿammûḏ](../../strongs/h/h5982.md) was eighteen ['ammâ](../../strongs/h/h520.md), and the [kōṯereṯ](../../strongs/h/h3805.md) upon it was [nᵊḥšeṯ](../../strongs/h/h5178.md): and the [qômâ](../../strongs/h/h6967.md) of the [kōṯereṯ](../../strongs/h/h3805.md) three ['ammâ](../../strongs/h/h520.md); and the [śᵊḇāḵâ](../../strongs/h/h7639.md), and [rimmôn](../../strongs/h/h7416.md) upon the [kōṯereṯ](../../strongs/h/h3805.md) [cabiyb](../../strongs/h/h5439.md), all of [nᵊḥšeṯ](../../strongs/h/h5178.md): and like unto these had the second [ʿammûḏ](../../strongs/h/h5982.md) with [śᵊḇāḵâ](../../strongs/h/h7639.md).

<a name="2kings_25_18"></a>2Kings 25:18

And the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [laqach](../../strongs/h/h3947.md) [Śᵊrāyâ](../../strongs/h/h8304.md) the [ro'sh](../../strongs/h/h7218.md) [kōhēn](../../strongs/h/h3548.md), and [Ṣᵊp̄Anyâ](../../strongs/h/h6846.md) the [mišnê](../../strongs/h/h4932.md) [kōhēn](../../strongs/h/h3548.md), and the three [shamar](../../strongs/h/h8104.md) of the [caph](../../strongs/h/h5592.md):

<a name="2kings_25_19"></a>2Kings 25:19

And out of the [ʿîr](../../strongs/h/h5892.md) he [laqach](../../strongs/h/h3947.md) a [sārîs](../../strongs/h/h5631.md) that was [pāqîḏ](../../strongs/h/h6496.md) over the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md), and five ['enowsh](../../strongs/h/h582.md) of them that [ra'ah](../../strongs/h/h7200.md) in the [melek](../../strongs/h/h4428.md) [paniym](../../strongs/h/h6440.md), which were [māṣā'](../../strongs/h/h4672.md) in the [ʿîr](../../strongs/h/h5892.md), and the [śar](../../strongs/h/h8269.md) [sāp̄ar](../../strongs/h/h5608.md) of the [tsaba'](../../strongs/h/h6635.md), which [ṣᵊḇā'](../../strongs/h/h6633.md) the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), and threescore ['iysh](../../strongs/h/h376.md) of the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) that were [māṣā'](../../strongs/h/h4672.md) in the [ʿîr](../../strongs/h/h5892.md):

<a name="2kings_25_20"></a>2Kings 25:20

And [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md) [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [laqach](../../strongs/h/h3947.md) these, and [yālaḵ](../../strongs/h/h3212.md) them to the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) to [Riḇlâ](../../strongs/h/h7247.md):

<a name="2kings_25_21"></a>2Kings 25:21

And the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [nakah](../../strongs/h/h5221.md) them, and [muwth](../../strongs/h/h4191.md) them at [Riḇlâ](../../strongs/h/h7247.md) in the ['erets](../../strongs/h/h776.md) of [Ḥămāṯ](../../strongs/h/h2574.md). So [Yehuwdah](../../strongs/h/h3063.md) was [gālâ](../../strongs/h/h1540.md) out of their ['ăḏāmâ](../../strongs/h/h127.md).

<a name="2kings_25_22"></a>2Kings 25:22

And as for the ['am](../../strongs/h/h5971.md) that [šā'ar](../../strongs/h/h7604.md) in the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md), whom [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) had [šā'ar](../../strongs/h/h7604.md), even over them he made [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md), the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md), [paqad](../../strongs/h/h6485.md).

<a name="2kings_25_23"></a>2Kings 25:23

And when all the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md), they and their ['enowsh](../../strongs/h/h582.md), [shama'](../../strongs/h/h8085.md) that the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) had made [Gᵊḏalyâ](../../strongs/h/h1436.md) [paqad](../../strongs/h/h6485.md), there [bow'](../../strongs/h/h935.md) to [Gᵊḏalyâ](../../strongs/h/h1436.md) to [Miṣpâ](../../strongs/h/h4709.md), even [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md), and [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md), and [Śᵊrāyâ](../../strongs/h/h8304.md) the [ben](../../strongs/h/h1121.md) of [Tanḥumeṯ](../../strongs/h/h8576.md) the [Nᵊṭōp̄āṯî](../../strongs/h/h5200.md), and [Ya'Ăzanyâ](../../strongs/h/h2970.md) the [ben](../../strongs/h/h1121.md) of a [Maʿăḵāṯî](../../strongs/h/h4602.md), they and their ['enowsh](../../strongs/h/h582.md).

<a name="2kings_25_24"></a>2Kings 25:24

And [Gᵊḏalyâ](../../strongs/h/h1436.md) [shaba'](../../strongs/h/h7650.md) to them, and to their ['enowsh](../../strongs/h/h582.md), and ['āmar](../../strongs/h/h559.md) unto them, [yare'](../../strongs/h/h3372.md) not to be the ['ebed](../../strongs/h/h5650.md) of the [Kaśdîmâ](../../strongs/h/h3778.md): [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md), and ['abad](../../strongs/h/h5647.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md); and it shall be [yatab](../../strongs/h/h3190.md) with you.

<a name="2kings_25_25"></a>2Kings 25:25

But it came to pass in the seventh [ḥōḏeš](../../strongs/h/h2320.md), that [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md), the [ben](../../strongs/h/h1121.md) of ['Ĕlîšāmāʿ](../../strongs/h/h476.md), of the [zera'](../../strongs/h/h2233.md) [mᵊlûḵâ](../../strongs/h/h4410.md), [bow'](../../strongs/h/h935.md), and ten ['enowsh](../../strongs/h/h582.md) with him, and [nakah](../../strongs/h/h5221.md) [Gᵊḏalyâ](../../strongs/h/h1436.md), that he [muwth](../../strongs/h/h4191.md), and the [Yᵊhûḏî](../../strongs/h/h3064.md) and the [Kaśdîmâ](../../strongs/h/h3778.md) that were with him at [Miṣpâ](../../strongs/h/h4709.md).

<a name="2kings_25_26"></a>2Kings 25:26

And all the ['am](../../strongs/h/h5971.md), both [qāṭān](../../strongs/h/h6996.md) and [gadowl](../../strongs/h/h1419.md), and the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md), [quwm](../../strongs/h/h6965.md), and [bow'](../../strongs/h/h935.md) to [Mitsrayim](../../strongs/h/h4714.md): for they were [yare'](../../strongs/h/h3372.md) [paniym](../../strongs/h/h6440.md) the [Kaśdîmâ](../../strongs/h/h3778.md).

<a name="2kings_25_27"></a>2Kings 25:27

And it came to pass in the seven and thirtieth [šānâ](../../strongs/h/h8141.md) of the [gālûṯ](../../strongs/h/h1546.md) of [Yᵊhôyāḵîn](../../strongs/h/h3078.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), in the twelfth [ḥōḏeš](../../strongs/h/h2320.md), on the seven and twentieth day of the [ḥōḏeš](../../strongs/h/h2320.md), that ['Ĕvîl Mᵊrōḏaḵ](../../strongs/h/h192.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) in the [šānâ](../../strongs/h/h8141.md) that he began to [mālaḵ](../../strongs/h/h4427.md) did [nasa'](../../strongs/h/h5375.md) the [ro'sh](../../strongs/h/h7218.md) of [Yᵊhôyāḵîn](../../strongs/h/h3078.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) out of [bayith](../../strongs/h/h1004.md) [kele'](../../strongs/h/h3608.md);

<a name="2kings_25_28"></a>2Kings 25:28

And he [dabar](../../strongs/h/h1696.md) [towb](../../strongs/h/h2896.md) to him, and [nathan](../../strongs/h/h5414.md) his [kicce'](../../strongs/h/h3678.md) above the [kicce'](../../strongs/h/h3678.md) of the [melek](../../strongs/h/h4428.md) that were with him in [Bāḇel](../../strongs/h/h894.md);

<a name="2kings_25_29"></a>2Kings 25:29

And [šānā'](../../strongs/h/h8132.md) his [kele'](../../strongs/h/h3608.md) [beḡeḏ](../../strongs/h/h899.md): and he did ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) [tāmîḏ](../../strongs/h/h8548.md) [paniym](../../strongs/h/h6440.md) him all the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md).

<a name="2kings_25_30"></a>2Kings 25:30

And his ['ăruḥâ](../../strongs/h/h737.md) was a [tāmîḏ](../../strongs/h/h8548.md) ['ăruḥâ](../../strongs/h/h737.md) [nathan](../../strongs/h/h5414.md) him of the [melek](../../strongs/h/h4428.md), a [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) [dabar](../../strongs/h/h1697.md) for every [yowm](../../strongs/h/h3117.md), all the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 24](2kings_24.md)