# [2Kings 8](https://www.blueletterbible.org/kjv/2kings/8)

<a name="2kings_8_1"></a>2Kings 8:1

Then [dabar](../../strongs/h/h1696.md) ['Ĕlîšāʿ](../../strongs/h/h477.md) unto the ['ishshah](../../strongs/h/h802.md), whose [ben](../../strongs/h/h1121.md) he had [ḥāyâ](../../strongs/h/h2421.md), ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) thou and thine [bayith](../../strongs/h/h1004.md), and [guwr](../../strongs/h/h1481.md) wheresoever thou canst [guwr](../../strongs/h/h1481.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [qara'](../../strongs/h/h7121.md) for a [rāʿāḇ](../../strongs/h/h7458.md); and it shall also [bow'](../../strongs/h/h935.md) upon the ['erets](../../strongs/h/h776.md) seven [šānâ](../../strongs/h/h8141.md).

<a name="2kings_8_2"></a>2Kings 8:2

And the ['ishshah](../../strongs/h/h802.md) [quwm](../../strongs/h/h6965.md), and ['asah](../../strongs/h/h6213.md) after the [dabar](../../strongs/h/h1697.md) of the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md): and she [yālaḵ](../../strongs/h/h3212.md) with her [bayith](../../strongs/h/h1004.md), and [guwr](../../strongs/h/h1481.md) in the ['erets](../../strongs/h/h776.md) of the [Pᵊlištî](../../strongs/h/h6430.md) seven [šānâ](../../strongs/h/h8141.md).

<a name="2kings_8_3"></a>2Kings 8:3

And it came to pass at the seven [šānâ](../../strongs/h/h8141.md) [qāṣê](../../strongs/h/h7097.md), that the ['ishshah](../../strongs/h/h802.md) [shuwb](../../strongs/h/h7725.md) out of the ['erets](../../strongs/h/h776.md) of the [Pᵊlištî](../../strongs/h/h6430.md): and she [yāṣā'](../../strongs/h/h3318.md) to [ṣāʿaq](../../strongs/h/h6817.md) unto the [melek](../../strongs/h/h4428.md) for her [bayith](../../strongs/h/h1004.md) and for her [sadeh](../../strongs/h/h7704.md).

<a name="2kings_8_4"></a>2Kings 8:4

And the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1696.md) with [Gêḥăzî](../../strongs/h/h1522.md) the [naʿar](../../strongs/h/h5288.md) of the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), ['āmar](../../strongs/h/h559.md), [sāp̄ar](../../strongs/h/h5608.md) me, I pray thee, all the [gadowl](../../strongs/h/h1419.md) that ['Ĕlîšāʿ](../../strongs/h/h477.md) hath ['asah](../../strongs/h/h6213.md).

<a name="2kings_8_5"></a>2Kings 8:5

And it came to pass, as he was [sāp̄ar](../../strongs/h/h5608.md) the [melek](../../strongs/h/h4428.md) how he had [ḥāyâ](../../strongs/h/h2421.md) a [muwth](../../strongs/h/h4191.md) to [ḥāyâ](../../strongs/h/h2421.md), that, behold, the ['ishshah](../../strongs/h/h802.md), whose [ben](../../strongs/h/h1121.md) he had restored to [ḥāyâ](../../strongs/h/h2421.md), [ṣāʿaq](../../strongs/h/h6817.md) to the [melek](../../strongs/h/h4428.md) for her [bayith](../../strongs/h/h1004.md) and for her [sadeh](../../strongs/h/h7704.md). And [Gêḥăzî](../../strongs/h/h1522.md) ['āmar](../../strongs/h/h559.md), My ['adown](../../strongs/h/h113.md), O [melek](../../strongs/h/h4428.md), this is the ['ishshah](../../strongs/h/h802.md), and this is her [ben](../../strongs/h/h1121.md), whom ['Ĕlîšāʿ](../../strongs/h/h477.md) restored to [ḥāyâ](../../strongs/h/h2421.md).

<a name="2kings_8_6"></a>2Kings 8:6

And when the [melek](../../strongs/h/h4428.md) [sha'al](../../strongs/h/h7592.md) the ['ishshah](../../strongs/h/h802.md), she [sāp̄ar](../../strongs/h/h5608.md) him. So the [melek](../../strongs/h/h4428.md) [nathan](../../strongs/h/h5414.md) unto her a certain [sārîs](../../strongs/h/h5631.md), ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md) all that was hers, and all the [tᵊḇû'â](../../strongs/h/h8393.md) of the [sadeh](../../strongs/h/h7704.md) since the [yowm](../../strongs/h/h3117.md) that she ['azab](../../strongs/h/h5800.md) the ['erets](../../strongs/h/h776.md), even until now.

<a name="2kings_8_7"></a>2Kings 8:7

And ['Ĕlîšāʿ](../../strongs/h/h477.md) [bow'](../../strongs/h/h935.md) to [Dammeśeq](../../strongs/h/h1834.md); and [Ben-Hăḏaḏ](../../strongs/h/h1130.md) the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) was [ḥālâ](../../strongs/h/h2470.md); and it was [nāḡaḏ](../../strongs/h/h5046.md) him, ['āmar](../../strongs/h/h559.md), The ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) is [bow'](../../strongs/h/h935.md) hither.

<a name="2kings_8_8"></a>2Kings 8:8

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto [Ḥăzā'Ēl](../../strongs/h/h2371.md), [laqach](../../strongs/h/h3947.md) a [minchah](../../strongs/h/h4503.md) in thine [yad](../../strongs/h/h3027.md), and [yālaḵ](../../strongs/h/h3212.md), [qārā'](../../strongs/h/h7125.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), and [darash](../../strongs/h/h1875.md) of [Yĕhovah](../../strongs/h/h3068.md) by him, ['āmar](../../strongs/h/h559.md), Shall I [ḥāyâ](../../strongs/h/h2421.md) of this [ḥŏlî](../../strongs/h/h2483.md)?

<a name="2kings_8_9"></a>2Kings 8:9

So [Ḥăzā'Ēl](../../strongs/h/h2371.md) [yālaḵ](../../strongs/h/h3212.md) to [qārā'](../../strongs/h/h7125.md) him, and [laqach](../../strongs/h/h3947.md) a [minchah](../../strongs/h/h4503.md) with [yad](../../strongs/h/h3027.md), even of every [ṭûḇ](../../strongs/h/h2898.md) of [Dammeśeq](../../strongs/h/h1834.md), forty [gāmāl](../../strongs/h/h1581.md) [maśśā'](../../strongs/h/h4853.md), and [bow'](../../strongs/h/h935.md) and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) him, and ['āmar](../../strongs/h/h559.md), Thy [ben](../../strongs/h/h1121.md) [Ben-Hăḏaḏ](../../strongs/h/h1130.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) hath [shalach](../../strongs/h/h7971.md) me to thee, ['āmar](../../strongs/h/h559.md), Shall I [ḥāyâ](../../strongs/h/h2421.md) of this [ḥŏlî](../../strongs/h/h2483.md)?

<a name="2kings_8_10"></a>2Kings 8:10

And ['Ĕlîšāʿ](../../strongs/h/h477.md) ['āmar](../../strongs/h/h559.md) unto him, [yālaḵ](../../strongs/h/h3212.md), ['āmar](../../strongs/h/h559.md) unto him, Thou mayest [ḥāyâ](../../strongs/h/h2421.md) [ḥāyâ](../../strongs/h/h2421.md): howbeit [Yĕhovah](../../strongs/h/h3068.md) hath [ra'ah](../../strongs/h/h7200.md) me that he shall [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="2kings_8_11"></a>2Kings 8:11

And he ['amad](../../strongs/h/h5975.md) his [paniym](../../strongs/h/h6440.md) [śûm](../../strongs/h/h7760.md), until he was [buwsh](../../strongs/h/h954.md): and the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) [bāḵâ](../../strongs/h/h1058.md).

<a name="2kings_8_12"></a>2Kings 8:12

And [Ḥăzā'Ēl](../../strongs/h/h2371.md) ['āmar](../../strongs/h/h559.md), Why [bāḵâ](../../strongs/h/h1058.md) my ['adown](../../strongs/h/h113.md)? And he ['āmar](../../strongs/h/h559.md), Because I [yada'](../../strongs/h/h3045.md) the [ra'](../../strongs/h/h7451.md) that thou wilt ['asah](../../strongs/h/h6213.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): their [miḇṣār](../../strongs/h/h4013.md) wilt thou [shalach](../../strongs/h/h7971.md) on ['esh](../../strongs/h/h784.md), and their [bāḥûr](../../strongs/h/h970.md) wilt thou [harag](../../strongs/h/h2026.md) with the [chereb](../../strongs/h/h2719.md), and wilt [rāṭaš](../../strongs/h/h7376.md) their ['owlel](../../strongs/h/h5768.md), and rip [bāqaʿ](../../strongs/h/h1234.md) their [hārê](../../strongs/h/h2030.md).

<a name="2kings_8_13"></a>2Kings 8:13

And [Ḥăzā'Ēl](../../strongs/h/h2371.md) ['āmar](../../strongs/h/h559.md), But what, is thy ['ebed](../../strongs/h/h5650.md) a [keleḇ](../../strongs/h/h3611.md), that he should ['asah](../../strongs/h/h6213.md) this [gadowl](../../strongs/h/h1419.md) [dabar](../../strongs/h/h1697.md)? And ['Ĕlîšāʿ](../../strongs/h/h477.md) ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) hath [ra'ah](../../strongs/h/h7200.md) me that thou shalt be [melek](../../strongs/h/h4428.md) over ['Ărām](../../strongs/h/h758.md).

<a name="2kings_8_14"></a>2Kings 8:14

So he [yālaḵ](../../strongs/h/h3212.md) from ['Ĕlîšāʿ](../../strongs/h/h477.md), and [bow'](../../strongs/h/h935.md) to his ['adown](../../strongs/h/h113.md); who ['āmar](../../strongs/h/h559.md) to him, What ['āmar](../../strongs/h/h559.md) ['Ĕlîšāʿ](../../strongs/h/h477.md) to thee? And he ['āmar](../../strongs/h/h559.md), He ['āmar](../../strongs/h/h559.md) me that thou shouldest [ḥāyâ](../../strongs/h/h2421.md) [ḥāyâ](../../strongs/h/h2421.md).

<a name="2kings_8_15"></a>2Kings 8:15

And it came to pass on the [māḥŏrāṯ](../../strongs/h/h4283.md), that he [laqach](../../strongs/h/h3947.md) a [miḵbār](../../strongs/h/h4346.md), and [ṭāḇal](../../strongs/h/h2881.md) it in [mayim](../../strongs/h/h4325.md), and [pāraś](../../strongs/h/h6566.md) it on his [paniym](../../strongs/h/h6440.md), so that he [muwth](../../strongs/h/h4191.md): and [Ḥăzā'Ēl](../../strongs/h/h2371.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2kings_8_16"></a>2Kings 8:16

And in the fifth [šānâ](../../strongs/h/h8141.md) of [Yôrām](../../strongs/h/h3141.md) the [ben](../../strongs/h/h1121.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) being then [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), [Yᵊhôrām](../../strongs/h/h3088.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) began to [mālaḵ](../../strongs/h/h4427.md).

<a name="2kings_8_17"></a>2Kings 8:17

Thirty and two [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) was he when he began to [mālaḵ](../../strongs/h/h4427.md); and he [mālaḵ](../../strongs/h/h4427.md) eight [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2kings_8_18"></a>2Kings 8:18

And he [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), as ['asah](../../strongs/h/h6213.md) the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md): for the [bath](../../strongs/h/h1323.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) was his ['ishshah](../../strongs/h/h802.md): and he ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_8_19"></a>2Kings 8:19

Yet [Yĕhovah](../../strongs/h/h3068.md) ['āḇâ](../../strongs/h/h14.md) not [shachath](../../strongs/h/h7843.md) [Yehuwdah](../../strongs/h/h3063.md) for [Dāviḏ](../../strongs/h/h1732.md) his ['ebed](../../strongs/h/h5650.md) sake, as he ['āmar](../../strongs/h/h559.md) him to [nathan](../../strongs/h/h5414.md) him [yowm](../../strongs/h/h3117.md) a [nîr](../../strongs/h/h5216.md), and to his [ben](../../strongs/h/h1121.md).

<a name="2kings_8_20"></a>2Kings 8:20

In his [yowm](../../strongs/h/h3117.md) ['Ĕḏōm](../../strongs/h/h123.md) [pāšaʿ](../../strongs/h/h6586.md) from under the [yad](../../strongs/h/h3027.md) of [Yehuwdah](../../strongs/h/h3063.md), and [mālaḵ](../../strongs/h/h4427.md) a [melek](../../strongs/h/h4428.md) over themselves.

<a name="2kings_8_21"></a>2Kings 8:21

So [Yôrām](../../strongs/h/h3141.md) ['abar](../../strongs/h/h5674.md) to [ṢāʿÎr](../../strongs/h/h6811.md), and all the [reḵeḇ](../../strongs/h/h7393.md) with him: and he [quwm](../../strongs/h/h6965.md) by [layil](../../strongs/h/h3915.md), and [nakah](../../strongs/h/h5221.md) the ['Ĕḏōm](../../strongs/h/h123.md) which [cabab](../../strongs/h/h5437.md) him, and the [śar](../../strongs/h/h8269.md) of the [reḵeḇ](../../strongs/h/h7393.md): and the ['am](../../strongs/h/h5971.md) [nûs](../../strongs/h/h5127.md) into their ['ohel](../../strongs/h/h168.md).

<a name="2kings_8_22"></a>2Kings 8:22

Yet ['Ĕḏōm](../../strongs/h/h123.md) [pāšaʿ](../../strongs/h/h6586.md) from under the [yad](../../strongs/h/h3027.md) of [Yehuwdah](../../strongs/h/h3063.md) unto this [yowm](../../strongs/h/h3117.md). Then [Liḇnâ](../../strongs/h/h3841.md) [pāšaʿ](../../strongs/h/h6586.md) at the same [ʿēṯ](../../strongs/h/h6256.md).

<a name="2kings_8_23"></a>2Kings 8:23

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yôrām](../../strongs/h/h3141.md), and all that he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)?

<a name="2kings_8_24"></a>2Kings 8:24

And [Yôrām](../../strongs/h/h3141.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) with his ['ab](../../strongs/h/h1.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md): and ['Ăḥazyâ](../../strongs/h/h274.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2kings_8_25"></a>2Kings 8:25

In the twelfth [šānâ](../../strongs/h/h8141.md) [šānâ](../../strongs/h/h8141.md) of [Yôrām](../../strongs/h/h3141.md) the [ben](../../strongs/h/h1121.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) did ['Ăḥazyâ](../../strongs/h/h274.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôrām](../../strongs/h/h3088.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) begin to [mālaḵ](../../strongs/h/h4427.md).

<a name="2kings_8_26"></a>2Kings 8:26

Two and twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) was ['Ăḥazyâ](../../strongs/h/h274.md) when he began to [mālaḵ](../../strongs/h/h4427.md); and he [mālaḵ](../../strongs/h/h4427.md) one [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [ʿĂṯalyâ](../../strongs/h/h6271.md), the [bath](../../strongs/h/h1323.md) of [ʿĀmrî](../../strongs/h/h6018.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_8_27"></a>2Kings 8:27

And he [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md), and ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), as did the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md): for he was the [ḥāṯān](../../strongs/h/h2860.md) of the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md).

<a name="2kings_8_28"></a>2Kings 8:28

And he [yālaḵ](../../strongs/h/h3212.md) with [Yôrām](../../strongs/h/h3141.md) the [ben](../../strongs/h/h1121.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) to the [milḥāmâ](../../strongs/h/h4421.md) against [Ḥăzā'Ēl](../../strongs/h/h2371.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) in [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md); and the ['Ărammy](../../strongs/h/h761.md) [nakah](../../strongs/h/h5221.md) [Yôrām](../../strongs/h/h3141.md).

<a name="2kings_8_29"></a>2Kings 8:29

And [melek](../../strongs/h/h4428.md) [Yôrām](../../strongs/h/h3141.md) went [shuwb](../../strongs/h/h7725.md) to be [rapha'](../../strongs/h/h7495.md) in [YizrᵊʿE'L](../../strongs/h/h3157.md) of the [makâ](../../strongs/h/h4347.md) which the ['Ărammy](../../strongs/h/h761.md) had [nakah](../../strongs/h/h5221.md) him at [rāmâ](../../strongs/h/h7414.md), when he [lāḥam](../../strongs/h/h3898.md) against [Ḥăzā'Ēl](../../strongs/h/h2371.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md). And ['Ăḥazyâ](../../strongs/h/h274.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôrām](../../strongs/h/h3088.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [yarad](../../strongs/h/h3381.md) to [ra'ah](../../strongs/h/h7200.md) [Yôrām](../../strongs/h/h3141.md) the [ben](../../strongs/h/h1121.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) in [YizrᵊʿE'L](../../strongs/h/h3157.md), because he was [ḥālâ](../../strongs/h/h2470.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 7](2kings_7.md) - [2Kings 9](2kings_9.md)