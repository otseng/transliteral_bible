# [2Kings 4](https://www.blueletterbible.org/kjv/2kings/4)

<a name="2kings_4_1"></a>2Kings 4:1

Now there [ṣāʿaq](../../strongs/h/h6817.md) a certain ['ishshah](../../strongs/h/h802.md) of the ['ishshah](../../strongs/h/h802.md) of the [ben](../../strongs/h/h1121.md) of the [nāḇî'](../../strongs/h/h5030.md) unto ['Ĕlîšāʿ](../../strongs/h/h477.md), ['āmar](../../strongs/h/h559.md), Thy ['ebed](../../strongs/h/h5650.md) my ['iysh](../../strongs/h/h376.md) is [muwth](../../strongs/h/h4191.md); and thou [yada'](../../strongs/h/h3045.md) that thy ['ebed](../../strongs/h/h5650.md) did [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md): and the [nāšâ](../../strongs/h/h5383.md) is [bow'](../../strongs/h/h935.md) to [laqach](../../strongs/h/h3947.md) unto him my two [yeleḏ](../../strongs/h/h3206.md) to be ['ebed](../../strongs/h/h5650.md).

<a name="2kings_4_2"></a>2Kings 4:2

And ['Ĕlîšāʿ](../../strongs/h/h477.md) ['āmar](../../strongs/h/h559.md) unto her, What shall I ['asah](../../strongs/h/h6213.md) for thee? [nāḡaḏ](../../strongs/h/h5046.md) me, what hast thou in the [bayith](../../strongs/h/h1004.md)? And she ['āmar](../../strongs/h/h559.md), Thine [šip̄ḥâ](../../strongs/h/h8198.md) hath not any thing in the [bayith](../../strongs/h/h1004.md), save an ['āsûḵ](../../strongs/h/h610.md) of [šemen](../../strongs/h/h8081.md).

<a name="2kings_4_3"></a>2Kings 4:3

Then he ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), [sha'al](../../strongs/h/h7592.md) thee [kĕliy](../../strongs/h/h3627.md) [ḥûṣ](../../strongs/h/h2351.md) of all thy [šāḵēn](../../strongs/h/h7934.md), even [reyq](../../strongs/h/h7386.md) [kĕliy](../../strongs/h/h3627.md); not a [māʿaṭ](../../strongs/h/h4591.md).

<a name="2kings_4_4"></a>2Kings 4:4

And when thou art [bow'](../../strongs/h/h935.md), thou shalt [cagar](../../strongs/h/h5462.md) the [deleṯ](../../strongs/h/h1817.md) upon thee and upon thy [ben](../../strongs/h/h1121.md), and shalt [yāṣaq](../../strongs/h/h3332.md) into all those [kĕliy](../../strongs/h/h3627.md), and thou shalt set [nāsaʿ](../../strongs/h/h5265.md) that which is [mālē'](../../strongs/h/h4392.md).

<a name="2kings_4_5"></a>2Kings 4:5

So she [yālaḵ](../../strongs/h/h3212.md) from him, and [cagar](../../strongs/h/h5462.md) the [deleṯ](../../strongs/h/h1817.md) upon her and upon her [ben](../../strongs/h/h1121.md), who [nāḡaš](../../strongs/h/h5066.md) to her; and she [yāṣaq](../../strongs/h/h3332.md).

<a name="2kings_4_6"></a>2Kings 4:6

And it came to pass, when the [kĕliy](../../strongs/h/h3627.md) were [mālā'](../../strongs/h/h4390.md), that she ['āmar](../../strongs/h/h559.md) unto her [ben](../../strongs/h/h1121.md), [nāḡaš](../../strongs/h/h5066.md) me yet a [kĕliy](../../strongs/h/h3627.md). And he ['āmar](../../strongs/h/h559.md) unto her, There is not a [kĕliy](../../strongs/h/h3627.md) more. And the [šemen](../../strongs/h/h8081.md) ['amad](../../strongs/h/h5975.md).

<a name="2kings_4_7"></a>2Kings 4:7

Then she [bow'](../../strongs/h/h935.md) and [nāḡaḏ](../../strongs/h/h5046.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md). And he ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), [māḵar](../../strongs/h/h4376.md) the [šemen](../../strongs/h/h8081.md), and [shalam](../../strongs/h/h7999.md) thy [nᵊšî](../../strongs/h/h5386.md), and [ḥāyâ](../../strongs/h/h2421.md) thou and thy [ben](../../strongs/h/h1121.md) of the [yāṯar](../../strongs/h/h3498.md).

<a name="2kings_4_8"></a>2Kings 4:8

And it fell on a [yowm](../../strongs/h/h3117.md), that ['Ĕlîšāʿ](../../strongs/h/h477.md) ['abar](../../strongs/h/h5674.md) to [Šûnēm](../../strongs/h/h7766.md), where was a [gadowl](../../strongs/h/h1419.md) ['ishshah](../../strongs/h/h802.md); and she [ḥāzaq](../../strongs/h/h2388.md) him to ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md). And so it was, that as [day](../../strongs/h/h1767.md) as he ['abar](../../strongs/h/h5674.md), he [cuwr](../../strongs/h/h5493.md) in thither to ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md).

<a name="2kings_4_9"></a>2Kings 4:9

And she ['āmar](../../strongs/h/h559.md) unto her ['iysh](../../strongs/h/h376.md), Behold now, I [yada'](../../strongs/h/h3045.md) that this is a [qadowsh](../../strongs/h/h6918.md) ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), which ['abar](../../strongs/h/h5674.md) by us [tāmîḏ](../../strongs/h/h8548.md).

<a name="2kings_4_10"></a>2Kings 4:10

Let us ['asah](../../strongs/h/h6213.md) a [qāṭān](../../strongs/h/h6996.md) [ʿălîyâ](../../strongs/h/h5944.md), I pray thee, on the [qîr](../../strongs/h/h7023.md); and let us [śûm](../../strongs/h/h7760.md) for him there a [mittah](../../strongs/h/h4296.md), and a [šulḥān](../../strongs/h/h7979.md), and a [kicce'](../../strongs/h/h3678.md), and a [mᵊnôrâ](../../strongs/h/h4501.md): and it shall be, when he [bow'](../../strongs/h/h935.md) to us, that he shall [cuwr](../../strongs/h/h5493.md) in thither.

<a name="2kings_4_11"></a>2Kings 4:11

And it fell on a [yowm](../../strongs/h/h3117.md), that he [bow'](../../strongs/h/h935.md) thither, and he [cuwr](../../strongs/h/h5493.md) into the [ʿălîyâ](../../strongs/h/h5944.md), and [shakab](../../strongs/h/h7901.md) there.

<a name="2kings_4_12"></a>2Kings 4:12

And he ['āmar](../../strongs/h/h559.md) to [Gêḥăzî](../../strongs/h/h1522.md) his [naʿar](../../strongs/h/h5288.md), [qara'](../../strongs/h/h7121.md) this [Šûnammîṯ](../../strongs/h/h7767.md). And when he had [qara'](../../strongs/h/h7121.md) her, she ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) him.

<a name="2kings_4_13"></a>2Kings 4:13

And he ['āmar](../../strongs/h/h559.md) unto him, ['āmar](../../strongs/h/h559.md) now unto her, Behold, thou hast been [ḥārēḏ](../../strongs/h/h2729.md) for us with all this [ḥărāḏâ](../../strongs/h/h2731.md); what is to be ['asah](../../strongs/h/h6213.md) for thee? wouldest thou be [dabar](../../strongs/h/h1696.md) for to the [melek](../../strongs/h/h4428.md), or to the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md)? And she ['āmar](../../strongs/h/h559.md), I [yashab](../../strongs/h/h3427.md) among mine own ['am](../../strongs/h/h5971.md).

<a name="2kings_4_14"></a>2Kings 4:14

And he ['āmar](../../strongs/h/h559.md), What then is to be ['asah](../../strongs/h/h6213.md) for her? And [Gêḥăzî](../../strongs/h/h1522.md) ['āmar](../../strongs/h/h559.md), ['ăḇāl](../../strongs/h/h61.md) she hath no [ben](../../strongs/h/h1121.md), and her ['iysh](../../strongs/h/h376.md) is [zāqēn](../../strongs/h/h2204.md).

<a name="2kings_4_15"></a>2Kings 4:15

And he ['āmar](../../strongs/h/h559.md), [qara'](../../strongs/h/h7121.md) her. And when he had [qara'](../../strongs/h/h7121.md) her, she ['amad](../../strongs/h/h5975.md) in the [peṯaḥ](../../strongs/h/h6607.md).

<a name="2kings_4_16"></a>2Kings 4:16

And he ['āmar](../../strongs/h/h559.md), About this [môʿēḏ](../../strongs/h/h4150.md), according to the [ʿēṯ](../../strongs/h/h6256.md) of [chay](../../strongs/h/h2416.md), thou shalt [ḥāḇaq](../../strongs/h/h2263.md) a [ben](../../strongs/h/h1121.md). And she ['āmar](../../strongs/h/h559.md), Nay, my ['adown](../../strongs/h/h113.md), thou ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), do not [kāzaḇ](../../strongs/h/h3576.md) unto thine [šip̄ḥâ](../../strongs/h/h8198.md).

<a name="2kings_4_17"></a>2Kings 4:17

And the ['ishshah](../../strongs/h/h802.md) [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md) at that [môʿēḏ](../../strongs/h/h4150.md) that ['Ĕlîšāʿ](../../strongs/h/h477.md) had [dabar](../../strongs/h/h1696.md) unto her, according to the [ʿēṯ](../../strongs/h/h6256.md) of [chay](../../strongs/h/h2416.md).

<a name="2kings_4_18"></a>2Kings 4:18

And when the [yeleḏ](../../strongs/h/h3206.md) was [gāḏal](../../strongs/h/h1431.md), it fell on a [yowm](../../strongs/h/h3117.md), that he [yāṣā'](../../strongs/h/h3318.md) to his ['ab](../../strongs/h/h1.md) to the [qāṣar](../../strongs/h/h7114.md).

<a name="2kings_4_19"></a>2Kings 4:19

And he ['āmar](../../strongs/h/h559.md) unto his ['ab](../../strongs/h/h1.md), My [ro'sh](../../strongs/h/h7218.md), my [ro'sh](../../strongs/h/h7218.md). And he ['āmar](../../strongs/h/h559.md) to a [naʿar](../../strongs/h/h5288.md), [nasa'](../../strongs/h/h5375.md) him to his ['em](../../strongs/h/h517.md).

<a name="2kings_4_20"></a>2Kings 4:20

And when he had [nasa'](../../strongs/h/h5375.md) him, and [bow'](../../strongs/h/h935.md) him to his ['em](../../strongs/h/h517.md), he [yashab](../../strongs/h/h3427.md) on her [bereḵ](../../strongs/h/h1290.md) till [ṣōhar](../../strongs/h/h6672.md), and then [muwth](../../strongs/h/h4191.md).

<a name="2kings_4_21"></a>2Kings 4:21

And she [ʿālâ](../../strongs/h/h5927.md), and [shakab](../../strongs/h/h7901.md) him on the [mittah](../../strongs/h/h4296.md) of the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), and [cagar](../../strongs/h/h5462.md) upon him, and [yāṣā'](../../strongs/h/h3318.md).

<a name="2kings_4_22"></a>2Kings 4:22

And she [qara'](../../strongs/h/h7121.md) unto her ['iysh](../../strongs/h/h376.md), and ['āmar](../../strongs/h/h559.md), [shalach](../../strongs/h/h7971.md) me, I pray thee, one of the [naʿar](../../strongs/h/h5288.md), and one of the ['āṯôn](../../strongs/h/h860.md), that I may [rûṣ](../../strongs/h/h7323.md) to the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), and [shuwb](../../strongs/h/h7725.md).

<a name="2kings_4_23"></a>2Kings 4:23

And he ['āmar](../../strongs/h/h559.md), Wherefore wilt thou [halak](../../strongs/h/h1980.md) to him to [yowm](../../strongs/h/h3117.md)? it is neither [ḥōḏeš](../../strongs/h/h2320.md), nor [shabbath](../../strongs/h/h7676.md). And she ['āmar](../../strongs/h/h559.md), It shall be [shalowm](../../strongs/h/h7965.md).

<a name="2kings_4_24"></a>2Kings 4:24

Then she [ḥāḇaš](../../strongs/h/h2280.md) an ['āṯôn](../../strongs/h/h860.md), and ['āmar](../../strongs/h/h559.md) to her [naʿar](../../strongs/h/h5288.md), [nāhaḡ](../../strongs/h/h5090.md), and go [yālaḵ](../../strongs/h/h3212.md); [ʿāṣar](../../strongs/h/h6113.md) not thy [rāḵaḇ](../../strongs/h/h7392.md) for me, except I ['āmar](../../strongs/h/h559.md) thee.

<a name="2kings_4_25"></a>2Kings 4:25

So she [yālaḵ](../../strongs/h/h3212.md) and [bow'](../../strongs/h/h935.md) unto the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) to [har](../../strongs/h/h2022.md) [Karmel](../../strongs/h/h3760.md). And it came to pass, when the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) her, that he ['āmar](../../strongs/h/h559.md) to [Gêḥăzî](../../strongs/h/h1522.md) his [naʿar](../../strongs/h/h5288.md), Behold, yonder is [hallāz](../../strongs/h/h1975.md) [Šûnammîṯ](../../strongs/h/h7767.md):

<a name="2kings_4_26"></a>2Kings 4:26

[rûṣ](../../strongs/h/h7323.md) now, I pray thee, to [qārā'](../../strongs/h/h7125.md) her, and ['āmar](../../strongs/h/h559.md) unto her, Is it [shalowm](../../strongs/h/h7965.md) with thee? is it [shalowm](../../strongs/h/h7965.md) with thy ['iysh](../../strongs/h/h376.md)? is it [shalowm](../../strongs/h/h7965.md) with the [yeleḏ](../../strongs/h/h3206.md)? And she ['āmar](../../strongs/h/h559.md), It is [shalowm](../../strongs/h/h7965.md).

<a name="2kings_4_27"></a>2Kings 4:27

And when she [bow'](../../strongs/h/h935.md) to the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) to the [har](../../strongs/h/h2022.md), she [ḥāzaq](../../strongs/h/h2388.md) him by the [regel](../../strongs/h/h7272.md): but [Gêḥăzî](../../strongs/h/h1522.md) [nāḡaš](../../strongs/h/h5066.md) to [hāḏap̄](../../strongs/h/h1920.md) her. And the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), Let her [rāp̄â](../../strongs/h/h7503.md); for her [nephesh](../../strongs/h/h5315.md) is [mārar](../../strongs/h/h4843.md) within her: and [Yĕhovah](../../strongs/h/h3068.md) hath ['alam](../../strongs/h/h5956.md) it from me, and hath not [nāḡaḏ](../../strongs/h/h5046.md) me.

<a name="2kings_4_28"></a>2Kings 4:28

Then she ['āmar](../../strongs/h/h559.md), Did I [sha'al](../../strongs/h/h7592.md) a [ben](../../strongs/h/h1121.md) of my ['adown](../../strongs/h/h113.md)? did I not ['āmar](../../strongs/h/h559.md), Do not [šālâ](../../strongs/h/h7952.md) me?

<a name="2kings_4_29"></a>2Kings 4:29

Then he ['āmar](../../strongs/h/h559.md) to [Gêḥăzî](../../strongs/h/h1522.md), [ḥāḡar](../../strongs/h/h2296.md) thy [māṯnayim](../../strongs/h/h4975.md), and [laqach](../../strongs/h/h3947.md) my [mašʿēnâ](../../strongs/h/h4938.md) in thine [yad](../../strongs/h/h3027.md), and go thy [yālaḵ](../../strongs/h/h3212.md): if thou [māṣā'](../../strongs/h/h4672.md) any ['iysh](../../strongs/h/h376.md), [barak](../../strongs/h/h1288.md) him not; and if ['iysh](../../strongs/h/h376.md) [barak](../../strongs/h/h1288.md) thee, ['anah](../../strongs/h/h6030.md) him not: and [śûm](../../strongs/h/h7760.md) my [mašʿēnâ](../../strongs/h/h4938.md) upon the [paniym](../../strongs/h/h6440.md) of the [naʿar](../../strongs/h/h5288.md).

<a name="2kings_4_30"></a>2Kings 4:30

And the ['em](../../strongs/h/h517.md) of the [naʿar](../../strongs/h/h5288.md) ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), and as thy [nephesh](../../strongs/h/h5315.md) [chay](../../strongs/h/h2416.md), I will not ['azab](../../strongs/h/h5800.md) thee. And he [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) her.

<a name="2kings_4_31"></a>2Kings 4:31

And [Gêḥăzî](../../strongs/h/h1522.md) ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) them, and [śûm](../../strongs/h/h7760.md) the [mašʿēnâ](../../strongs/h/h4938.md) upon the [paniym](../../strongs/h/h6440.md) of the [naʿar](../../strongs/h/h5288.md); but there was neither [qowl](../../strongs/h/h6963.md), nor [qešeḇ](../../strongs/h/h7182.md). Wherefore he went [shuwb](../../strongs/h/h7725.md) to [qārā'](../../strongs/h/h7125.md) him, and [nāḡaḏ](../../strongs/h/h5046.md) him, ['āmar](../../strongs/h/h559.md), The [naʿar](../../strongs/h/h5288.md) is not [quwts](../../strongs/h/h6974.md).

<a name="2kings_4_32"></a>2Kings 4:32

And when ['Ĕlîšāʿ](../../strongs/h/h477.md) was [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md), behold, the [naʿar](../../strongs/h/h5288.md) was [muwth](../../strongs/h/h4191.md), and [shakab](../../strongs/h/h7901.md) upon his [mittah](../../strongs/h/h4296.md).

<a name="2kings_4_33"></a>2Kings 4:33

He [bow'](../../strongs/h/h935.md) therefore, and [cagar](../../strongs/h/h5462.md) the [deleṯ](../../strongs/h/h1817.md) upon them twain, and [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_4_34"></a>2Kings 4:34

And he [ʿālâ](../../strongs/h/h5927.md), and [shakab](../../strongs/h/h7901.md) upon the [yeleḏ](../../strongs/h/h3206.md), and [śûm](../../strongs/h/h7760.md) his [peh](../../strongs/h/h6310.md) upon his [peh](../../strongs/h/h6310.md), and his ['ayin](../../strongs/h/h5869.md) upon his ['ayin](../../strongs/h/h5869.md), and his [kaph](../../strongs/h/h3709.md) upon his [kaph](../../strongs/h/h3709.md): and he [gāhar](../../strongs/h/h1457.md) himself upon the child; and the [basar](../../strongs/h/h1320.md) of the [yeleḏ](../../strongs/h/h3206.md) [ḥāmam](../../strongs/h/h2552.md).

<a name="2kings_4_35"></a>2Kings 4:35

Then he [shuwb](../../strongs/h/h7725.md), and [yālaḵ](../../strongs/h/h3212.md) in the [bayith](../../strongs/h/h1004.md) to and fro ; and [ʿālâ](../../strongs/h/h5927.md), and [gāhar](../../strongs/h/h1457.md) himself upon him: and the [naʿar](../../strongs/h/h5288.md) [zārar](../../strongs/h/h2237.md) seven [pa'am](../../strongs/h/h6471.md), and the [naʿar](../../strongs/h/h5288.md) [paqach](../../strongs/h/h6491.md) his ['ayin](../../strongs/h/h5869.md).

<a name="2kings_4_36"></a>2Kings 4:36

And he [qara'](../../strongs/h/h7121.md) [Gêḥăzî](../../strongs/h/h1522.md), and ['āmar](../../strongs/h/h559.md), [qara'](../../strongs/h/h7121.md) this [Šûnammîṯ](../../strongs/h/h7767.md). So he [qara'](../../strongs/h/h7121.md) her. And when she was [bow'](../../strongs/h/h935.md) unto him, he ['āmar](../../strongs/h/h559.md), [nasa'](../../strongs/h/h5375.md) thy [ben](../../strongs/h/h1121.md).

<a name="2kings_4_37"></a>2Kings 4:37

Then she [bow'](../../strongs/h/h935.md), and [naphal](../../strongs/h/h5307.md) at his [regel](../../strongs/h/h7272.md), and [shachah](../../strongs/h/h7812.md) herself to the ['erets](../../strongs/h/h776.md), and [nasa'](../../strongs/h/h5375.md) her [ben](../../strongs/h/h1121.md), and [yāṣā'](../../strongs/h/h3318.md).

<a name="2kings_4_38"></a>2Kings 4:38

And ['Ĕlîšāʿ](../../strongs/h/h477.md) [shuwb](../../strongs/h/h7725.md) to [Gilgāl](../../strongs/h/h1537.md): and there was a [rāʿāḇ](../../strongs/h/h7458.md) in the ['erets](../../strongs/h/h776.md); and the [ben](../../strongs/h/h1121.md) of the [nāḇî'](../../strongs/h/h5030.md) were [yashab](../../strongs/h/h3427.md) [paniym](../../strongs/h/h6440.md) him: and he ['āmar](../../strongs/h/h559.md) unto his [naʿar](../../strongs/h/h5288.md), [šāp̄aṯ](../../strongs/h/h8239.md) on the [gadowl](../../strongs/h/h1419.md) [sîr](../../strongs/h/h5518.md), and [bāšal](../../strongs/h/h1310.md) [nāzîḏ](../../strongs/h/h5138.md) for the [ben](../../strongs/h/h1121.md) of the [nāḇî'](../../strongs/h/h5030.md).

<a name="2kings_4_39"></a>2Kings 4:39

And one [yāṣā'](../../strongs/h/h3318.md) into the [sadeh](../../strongs/h/h7704.md) to [lāqaṭ](../../strongs/h/h3950.md) ['ôrâ](../../strongs/h/h219.md), and [māṣā'](../../strongs/h/h4672.md) a [sadeh](../../strongs/h/h7704.md) [gep̄en](../../strongs/h/h1612.md), and [lāqaṭ](../../strongs/h/h3950.md) thereof [sadeh](../../strongs/h/h7704.md) [paqquʿōṯ](../../strongs/h/h6498.md) his [beḡeḏ](../../strongs/h/h899.md) [mᵊlō'](../../strongs/h/h4393.md), and [bow'](../../strongs/h/h935.md) and [pālaḥ](../../strongs/h/h6398.md) them into the [sîr](../../strongs/h/h5518.md) of [nāzîḏ](../../strongs/h/h5138.md): for they [yada'](../../strongs/h/h3045.md) them not.

<a name="2kings_4_40"></a>2Kings 4:40

So they [yāṣaq](../../strongs/h/h3332.md) for the ['enowsh](../../strongs/h/h582.md) to ['akal](../../strongs/h/h398.md). And it came to pass, as they were ['akal](../../strongs/h/h398.md) of the [nāzîḏ](../../strongs/h/h5138.md), that they [ṣāʿaq](../../strongs/h/h6817.md), and ['āmar](../../strongs/h/h559.md), O thou ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), there is [maveth](../../strongs/h/h4194.md) in the [sîr](../../strongs/h/h5518.md). And they [yakol](../../strongs/h/h3201.md) not ['akal](../../strongs/h/h398.md) thereof.

<a name="2kings_4_41"></a>2Kings 4:41

But he ['āmar](../../strongs/h/h559.md), Then [laqach](../../strongs/h/h3947.md) [qemaḥ](../../strongs/h/h7058.md). And he [shalak](../../strongs/h/h7993.md) it into the [sîr](../../strongs/h/h5518.md); and he ['āmar](../../strongs/h/h559.md), [yāṣaq](../../strongs/h/h3332.md) for the ['am](../../strongs/h/h5971.md), that they may ['akal](../../strongs/h/h398.md). And there was no [dabar](../../strongs/h/h1697.md) [ra'](../../strongs/h/h7451.md) in the [sîr](../../strongs/h/h5518.md).

<a name="2kings_4_42"></a>2Kings 4:42

And there [bow'](../../strongs/h/h935.md) an ['iysh](../../strongs/h/h376.md) from [BaʿAl ŠāliשÂ](../../strongs/h/h1190.md), and [bow'](../../strongs/h/h935.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) [lechem](../../strongs/h/h3899.md) of the [bikûr](../../strongs/h/h1061.md), twenty [lechem](../../strongs/h/h3899.md) of [śᵊʿōrâ](../../strongs/h/h8184.md), and full ears of [karmel](../../strongs/h/h3759.md) in the [ṣiqlôn](../../strongs/h/h6861.md) thereof. And he ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) unto the ['am](../../strongs/h/h5971.md), that they may ['akal](../../strongs/h/h398.md).

<a name="2kings_4_43"></a>2Kings 4:43

And his [sharath](../../strongs/h/h8334.md) ['āmar](../../strongs/h/h559.md), What, should I [nathan](../../strongs/h/h5414.md) this [paniym](../../strongs/h/h6440.md) an hundred ['iysh](../../strongs/h/h376.md)? He said ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) the ['am](../../strongs/h/h5971.md), that they may ['akal](../../strongs/h/h398.md): for thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), They shall ['akal](../../strongs/h/h398.md), and shall [yāṯar](../../strongs/h/h3498.md) thereof.

<a name="2kings_4_44"></a>2Kings 4:44

So he [nathan](../../strongs/h/h5414.md) it [paniym](../../strongs/h/h6440.md) them, and they did ['akal](../../strongs/h/h398.md), and [yāṯar](../../strongs/h/h3498.md) thereof, according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 3](2kings_3.md) - [2Kings 5](2kings_5.md)