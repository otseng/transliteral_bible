# [2Kings 20](https://www.blueletterbible.org/kjv/2kings/20)

<a name="2kings_20_1"></a>2Kings 20:1

In those [yowm](../../strongs/h/h3117.md) was [Ḥizqîyâ](../../strongs/h/h2396.md) [ḥālâ](../../strongs/h/h2470.md) unto [muwth](../../strongs/h/h4191.md). And the [nāḇî'](../../strongs/h/h5030.md) [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [ben](../../strongs/h/h1121.md) of ['Āmôṣ](../../strongs/h/h531.md) [bow'](../../strongs/h/h935.md) to him, and ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Set thine [bayith](../../strongs/h/h1004.md) in [tsavah](../../strongs/h/h6680.md); for thou shalt [muwth](../../strongs/h/h4191.md), and not [ḥāyâ](../../strongs/h/h2421.md).

<a name="2kings_20_2"></a>2Kings 20:2

Then he [cabab](../../strongs/h/h5437.md) his [paniym](../../strongs/h/h6440.md) to the [qîr](../../strongs/h/h7023.md), and [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="2kings_20_3"></a>2Kings 20:3

I ['ānnā'](../../strongs/h/h577.md) thee, [Yĕhovah](../../strongs/h/h3068.md), [zakar](../../strongs/h/h2142.md) now how I have [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) thee in ['emeth](../../strongs/h/h571.md) and with a [šālēm](../../strongs/h/h8003.md) [lebab](../../strongs/h/h3824.md), and have ['asah](../../strongs/h/h6213.md) that which is [towb](../../strongs/h/h2896.md) in thy ['ayin](../../strongs/h/h5869.md). And [Ḥizqîyâ](../../strongs/h/h2396.md) [bāḵâ](../../strongs/h/h1058.md) [bĕkiy](../../strongs/h/h1065.md) [gadowl](../../strongs/h/h1419.md).

<a name="2kings_20_4"></a>2Kings 20:4

And it came to pass, afore [Yᵊšaʿyâ](../../strongs/h/h3470.md) was [yāṣā'](../../strongs/h/h3318.md) into the [tîḵôn](../../strongs/h/h8484.md) [ḥāṣēr](../../strongs/h/h2691.md) [ʿîr](../../strongs/h/h5892.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to him, ['āmar](../../strongs/h/h559.md),

<a name="2kings_20_5"></a>2Kings 20:5

[shuwb](../../strongs/h/h7725.md), and ['āmar](../../strongs/h/h559.md) [Ḥizqîyâ](../../strongs/h/h2396.md) the [nāḡîḏ](../../strongs/h/h5057.md) of my ['am](../../strongs/h/h5971.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Dāviḏ](../../strongs/h/h1732.md) thy ['ab](../../strongs/h/h1.md), I have [shama'](../../strongs/h/h8085.md) thy [tĕphillah](../../strongs/h/h8605.md), I have [ra'ah](../../strongs/h/h7200.md) thy [dim'ah](../../strongs/h/h1832.md): behold, I will [rapha'](../../strongs/h/h7495.md) thee: on the third [yowm](../../strongs/h/h3117.md) thou shalt [ʿālâ](../../strongs/h/h5927.md) unto the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_20_6"></a>2Kings 20:6

And I will add unto thy [yowm](../../strongs/h/h3117.md) fifteen [šānâ](../../strongs/h/h8141.md); and I will [natsal](../../strongs/h/h5337.md) thee and this [ʿîr](../../strongs/h/h5892.md) out of the [kaph](../../strongs/h/h3709.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md); and I will [gānan](../../strongs/h/h1598.md) this [ʿîr](../../strongs/h/h5892.md) for mine own sake, and for my ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) sake.

<a name="2kings_20_7"></a>2Kings 20:7

And [Yᵊšaʿyâ](../../strongs/h/h3470.md) ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) a [dᵊḇēlâ](../../strongs/h/h1690.md) of [tĕ'en](../../strongs/h/h8384.md). And they [laqach](../../strongs/h/h3947.md) and [śûm](../../strongs/h/h7760.md) it on the [šiḥîn](../../strongs/h/h7822.md), and he [ḥāyâ](../../strongs/h/h2421.md).

<a name="2kings_20_8"></a>2Kings 20:8

And [Ḥizqîyâ](../../strongs/h/h2396.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊšaʿyâ](../../strongs/h/h3470.md), What shall be the ['ôṯ](../../strongs/h/h226.md) that [Yĕhovah](../../strongs/h/h3068.md) will [rapha'](../../strongs/h/h7495.md) me, and that I shall [ʿālâ](../../strongs/h/h5927.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) the third [yowm](../../strongs/h/h3117.md)?

<a name="2kings_20_9"></a>2Kings 20:9

And [Yᵊšaʿyâ](../../strongs/h/h3470.md) ['āmar](../../strongs/h/h559.md), This ['ôṯ](../../strongs/h/h226.md) shalt thou have of [Yĕhovah](../../strongs/h/h3068.md), that [Yĕhovah](../../strongs/h/h3068.md) will ['asah](../../strongs/h/h6213.md) the [dabar](../../strongs/h/h1697.md) that he hath [dabar](../../strongs/h/h1696.md): shall the [ṣēl](../../strongs/h/h6738.md) go [halak](../../strongs/h/h1980.md) ten [maʿălâ](../../strongs/h/h4609.md), or [shuwb](../../strongs/h/h7725.md) ten [maʿălâ](../../strongs/h/h4609.md)?

<a name="2kings_20_10"></a>2Kings 20:10

And [Yᵊḥizqîyâ](../../strongs/h/h3169.md) ['āmar](../../strongs/h/h559.md), It is a [qālal](../../strongs/h/h7043.md) for the [ṣēl](../../strongs/h/h6738.md) to go [natah](../../strongs/h/h5186.md) ten [maʿălâ](../../strongs/h/h4609.md): nay, but let the [ṣēl](../../strongs/h/h6738.md) [shuwb](../../strongs/h/h7725.md) ['ăḥōrannîṯ](../../strongs/h/h322.md) ten [maʿălâ](../../strongs/h/h4609.md).

<a name="2kings_20_11"></a>2Kings 20:11

And [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [nāḇî'](../../strongs/h/h5030.md) [qara'](../../strongs/h/h7121.md) unto [Yĕhovah](../../strongs/h/h3068.md): and he [shuwb](../../strongs/h/h7725.md) the [ṣēl](../../strongs/h/h6738.md) ten [maʿălâ](../../strongs/h/h4609.md) ['ăḥōrannîṯ](../../strongs/h/h322.md), [maʿălâ](../../strongs/h/h4609.md) which it had [yarad](../../strongs/h/h3381.md) in the [maʿălâ](../../strongs/h/h4609.md) of ['Āḥāz](../../strongs/h/h271.md).

<a name="2kings_20_12"></a>2Kings 20:12

At that [ʿēṯ](../../strongs/h/h6256.md) [Bᵊrō'Ḏaḵ-Bal'Ăḏān](../../strongs/h/h1255.md), the [ben](../../strongs/h/h1121.md) of [Bal'Ăḏān](../../strongs/h/h1081.md), [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), [shalach](../../strongs/h/h7971.md) [sēp̄er](../../strongs/h/h5612.md) and a [minchah](../../strongs/h/h4503.md) unto [Ḥizqîyâ](../../strongs/h/h2396.md): for he had [shama'](../../strongs/h/h8085.md) that [Ḥizqîyâ](../../strongs/h/h2396.md) had been [ḥālâ](../../strongs/h/h2470.md).

<a name="2kings_20_13"></a>2Kings 20:13

And [Ḥizqîyâ](../../strongs/h/h2396.md) [shama'](../../strongs/h/h8085.md) unto them, and [ra'ah](../../strongs/h/h7200.md) them all the [bayith](../../strongs/h/h1004.md) of his [nᵊḵōṯ](../../strongs/h/h5238.md), the [keceph](../../strongs/h/h3701.md), and the [zāhāḇ](../../strongs/h/h2091.md), and the [beśem](../../strongs/h/h1314.md), and the [towb](../../strongs/h/h2896.md) [šemen](../../strongs/h/h8081.md), and all the [bayith](../../strongs/h/h1004.md) of his [kĕliy](../../strongs/h/h3627.md), and all that was [māṣā'](../../strongs/h/h4672.md) in his ['ôṣār](../../strongs/h/h214.md): there was [dabar](../../strongs/h/h1697.md) in his [bayith](../../strongs/h/h1004.md), nor in all his [memshalah](../../strongs/h/h4475.md), that [Ḥizqîyâ](../../strongs/h/h2396.md) [ra'ah](../../strongs/h/h7200.md) them not.

<a name="2kings_20_14"></a>2Kings 20:14

Then [bow'](../../strongs/h/h935.md) [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [nāḇî'](../../strongs/h/h5030.md) unto [melek](../../strongs/h/h4428.md) [Ḥizqîyâ](../../strongs/h/h2396.md), and ['āmar](../../strongs/h/h559.md) unto him, What ['āmar](../../strongs/h/h559.md) these ['enowsh](../../strongs/h/h582.md)? and from whence [bow'](../../strongs/h/h935.md) they unto thee? And [Ḥizqîyâ](../../strongs/h/h2396.md) ['āmar](../../strongs/h/h559.md), They are [bow'](../../strongs/h/h935.md) from a [rachowq](../../strongs/h/h7350.md) ['erets](../../strongs/h/h776.md), even from [Bāḇel](../../strongs/h/h894.md).

<a name="2kings_20_15"></a>2Kings 20:15

And he ['āmar](../../strongs/h/h559.md), What have they [ra'ah](../../strongs/h/h7200.md) in thine [bayith](../../strongs/h/h1004.md)? And [Ḥizqîyâ](../../strongs/h/h2396.md) ['āmar](../../strongs/h/h559.md), All the things that are in mine [bayith](../../strongs/h/h1004.md) have they [ra'ah](../../strongs/h/h7200.md): there is [dabar](../../strongs/h/h1697.md) among my ['ôṣār](../../strongs/h/h214.md) that I have not [ra'ah](../../strongs/h/h7200.md) them.

<a name="2kings_20_16"></a>2Kings 20:16

And [Yᵊšaʿyâ](../../strongs/h/h3470.md) ['āmar](../../strongs/h/h559.md) unto [Ḥizqîyâ](../../strongs/h/h2396.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_20_17"></a>2Kings 20:17

Behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), that all that is in thine [bayith](../../strongs/h/h1004.md), and that which thy ['ab](../../strongs/h/h1.md) have ['āṣar](../../strongs/h/h686.md) unto this [yowm](../../strongs/h/h3117.md), shall be [nasa'](../../strongs/h/h5375.md) into [Bāḇel](../../strongs/h/h894.md): [dabar](../../strongs/h/h1697.md) shall be [yāṯar](../../strongs/h/h3498.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_20_18"></a>2Kings 20:18

And of thy [ben](../../strongs/h/h1121.md) that shall [yāṣā'](../../strongs/h/h3318.md) from thee, which thou shalt [yalad](../../strongs/h/h3205.md), shall they [laqach](../../strongs/h/h3947.md); and they shall be [sārîs](../../strongs/h/h5631.md) in the [heykal](../../strongs/h/h1964.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md).

<a name="2kings_20_19"></a>2Kings 20:19

Then ['āmar](../../strongs/h/h559.md) [Ḥizqîyâ](../../strongs/h/h2396.md) unto [Yᵊšaʿyâ](../../strongs/h/h3470.md), [towb](../../strongs/h/h2896.md) is the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) which thou hast [dabar](../../strongs/h/h1696.md). And he ['āmar](../../strongs/h/h559.md), Is it not good, if [shalowm](../../strongs/h/h7965.md) and ['emeth](../../strongs/h/h571.md) be in my [yowm](../../strongs/h/h3117.md)?

<a name="2kings_20_20"></a>2Kings 20:20

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Ḥizqîyâ](../../strongs/h/h2396.md), and all his [gᵊḇûrâ](../../strongs/h/h1369.md), and how he ['asah](../../strongs/h/h6213.md) a [bᵊrēḵâ](../../strongs/h/h1295.md), and a [tᵊʿālâ](../../strongs/h/h8585.md), and [bow'](../../strongs/h/h935.md) [mayim](../../strongs/h/h4325.md) into the [ʿîr](../../strongs/h/h5892.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)?

<a name="2kings_20_21"></a>2Kings 20:21

And [Ḥizqîyâ](../../strongs/h/h2396.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md): and [Mᵊnaššê](../../strongs/h/h4519.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 19](2kings_19.md) - [2Kings 21](2kings_21.md)