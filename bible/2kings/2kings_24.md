# [2Kings 24](https://www.blueletterbible.org/kjv/2kings/24)

<a name="2kings_24_1"></a>2Kings 24:1

In his [yowm](../../strongs/h/h3117.md) [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [ʿālâ](../../strongs/h/h5927.md), and [Yᵊhôyāqîm](../../strongs/h/h3079.md) became his ['ebed](../../strongs/h/h5650.md) three [šānâ](../../strongs/h/h8141.md): then he [shuwb](../../strongs/h/h7725.md) and [māraḏ](../../strongs/h/h4775.md) against him.

<a name="2kings_24_2"></a>2Kings 24:2

And [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) against him [gᵊḏûḏ](../../strongs/h/h1416.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), and [gᵊḏûḏ](../../strongs/h/h1416.md) of the ['Ărām](../../strongs/h/h758.md), and [gᵊḏûḏ](../../strongs/h/h1416.md) of the [Mô'āḇ](../../strongs/h/h4124.md), and [gᵊḏûḏ](../../strongs/h/h1416.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and [shalach](../../strongs/h/h7971.md) them against [Yehuwdah](../../strongs/h/h3063.md) to ['abad](../../strongs/h/h6.md) it, according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [dabar](../../strongs/h/h1696.md) [yad](../../strongs/h/h3027.md) his ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md).

<a name="2kings_24_3"></a>2Kings 24:3

Surely at the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) came this upon [Yehuwdah](../../strongs/h/h3063.md), to [cuwr](../../strongs/h/h5493.md) them out of his [paniym](../../strongs/h/h6440.md), for the [chatta'ath](../../strongs/h/h2403.md) of [Mᵊnaššê](../../strongs/h/h4519.md), according to all that he ['asah](../../strongs/h/h6213.md);

<a name="2kings_24_4"></a>2Kings 24:4

And also for the [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md) that he [šāp̄aḵ](../../strongs/h/h8210.md): for he [mālā'](../../strongs/h/h4390.md) [Yĕruwshalaim](../../strongs/h/h3389.md) with [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md); which [Yĕhovah](../../strongs/h/h3068.md) ['āḇâ](../../strongs/h/h14.md) not [sālaḥ](../../strongs/h/h5545.md).

<a name="2kings_24_5"></a>2Kings 24:5

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md), and all that he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)?

<a name="2kings_24_6"></a>2Kings 24:6

So [Yᵊhôyāqîm](../../strongs/h/h3079.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md): and [Yᵊhôyāḵîn](../../strongs/h/h3078.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2kings_24_7"></a>2Kings 24:7

And the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) [yāṣā'](../../strongs/h/h3318.md) not again any more out of his ['erets](../../strongs/h/h776.md): for the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) had [laqach](../../strongs/h/h3947.md) from the [nachal](../../strongs/h/h5158.md) of [Mitsrayim](../../strongs/h/h4714.md) unto the [nāhār](../../strongs/h/h5104.md) [Pᵊrāṯ](../../strongs/h/h6578.md) all that pertained to the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="2kings_24_8"></a>2Kings 24:8

[Yᵊhôyāḵîn](../../strongs/h/h3078.md) was eighteen [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) three [ḥōḏeš](../../strongs/h/h2320.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [Nᵊḥuštā'](../../strongs/h/h5179.md), the [bath](../../strongs/h/h1323.md) of ['Elnāṯān](../../strongs/h/h494.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2kings_24_9"></a>2Kings 24:9

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), according to all that his ['ab](../../strongs/h/h1.md) had ['asah](../../strongs/h/h6213.md).

<a name="2kings_24_10"></a>2Kings 24:10

At that [ʿēṯ](../../strongs/h/h6256.md) the ['ebed](../../strongs/h/h5650.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [ʿālâ](../../strongs/h/h5927.md) against [Yĕruwshalaim](../../strongs/h/h3389.md), and the [ʿîr](../../strongs/h/h5892.md) was [bow'](../../strongs/h/h935.md) [māṣôr](../../strongs/h/h4692.md).

<a name="2kings_24_11"></a>2Kings 24:11

And [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [bow'](../../strongs/h/h935.md) against the [ʿîr](../../strongs/h/h5892.md), and his ['ebed](../../strongs/h/h5650.md) did [ṣûr](../../strongs/h/h6696.md) it.

<a name="2kings_24_12"></a>2Kings 24:12

And [Yᵊhôyāḵîn](../../strongs/h/h3078.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [yāṣā'](../../strongs/h/h3318.md) to the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), he, and his ['em](../../strongs/h/h517.md), and his ['ebed](../../strongs/h/h5650.md), and his [śar](../../strongs/h/h8269.md), and his [sārîs](../../strongs/h/h5631.md): and the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [laqach](../../strongs/h/h3947.md) him in the eighth [šānâ](../../strongs/h/h8141.md) of his [mālaḵ](../../strongs/h/h4427.md).

<a name="2kings_24_13"></a>2Kings 24:13

And he [yāṣā'](../../strongs/h/h3318.md) thence all the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the ['ôṣār](../../strongs/h/h214.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and [qāṣaṣ](../../strongs/h/h7112.md) all the [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md) which [Šᵊlōmô](../../strongs/h/h8010.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) had ['asah](../../strongs/h/h6213.md) in the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md), as [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md).

<a name="2kings_24_14"></a>2Kings 24:14

And he [gālâ](../../strongs/h/h1540.md) all [Yĕruwshalaim](../../strongs/h/h3389.md), and all the [śar](../../strongs/h/h8269.md), and all the [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), even ten thousand [gālâ](../../strongs/h/h1540.md), and all the [ḥārāš](../../strongs/h/h2796.md) and [masgēr](../../strongs/h/h4525.md): none [šā'ar](../../strongs/h/h7604.md), [zûlâ](../../strongs/h/h2108.md) the [dallâ](../../strongs/h/h1803.md) of the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md).

<a name="2kings_24_15"></a>2Kings 24:15

And he [gālâ](../../strongs/h/h1540.md) [Yᵊhôyāḵîn](../../strongs/h/h3078.md) to [Bāḇel](../../strongs/h/h894.md), and the [melek](../../strongs/h/h4428.md) ['em](../../strongs/h/h517.md), and the [melek](../../strongs/h/h4428.md) ['ishshah](../../strongs/h/h802.md), and his [sārîs](../../strongs/h/h5631.md), and the ['ayil](../../strongs/h/h352.md) ['ûl](../../strongs/h/h193.md) of the ['erets](../../strongs/h/h776.md), those [yālaḵ](../../strongs/h/h3212.md) he into [gôlâ](../../strongs/h/h1473.md) from [Yĕruwshalaim](../../strongs/h/h3389.md) to [Bāḇel](../../strongs/h/h894.md).

<a name="2kings_24_16"></a>2Kings 24:16

And all the ['enowsh](../../strongs/h/h582.md) of [ḥayil](../../strongs/h/h2428.md), even seven thousand, and [ḥārāš](../../strongs/h/h2796.md) and [masgēr](../../strongs/h/h4525.md) a thousand, all that were [gibôr](../../strongs/h/h1368.md) and ['asah](../../strongs/h/h6213.md) for [milḥāmâ](../../strongs/h/h4421.md), even them the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [bow'](../../strongs/h/h935.md) [gôlâ](../../strongs/h/h1473.md) to [Bāḇel](../../strongs/h/h894.md).

<a name="2kings_24_17"></a>2Kings 24:17

And the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) made [Matanyâ](../../strongs/h/h4983.md) his [dôḏ](../../strongs/h/h1730.md) [mālaḵ](../../strongs/h/h4427.md) in his stead, and [cabab](../../strongs/h/h5437.md) his [shem](../../strongs/h/h8034.md) to [Ṣḏqyh](../../strongs/h/h6667.md).

<a name="2kings_24_18"></a>2Kings 24:18

[Ṣḏqyh](../../strongs/h/h6667.md) was twenty and one [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) eleven [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [Ḥămûṭal](../../strongs/h/h2537.md), the [bath](../../strongs/h/h1323.md) of [Yirmᵊyâ](../../strongs/h/h3414.md) of [Liḇnâ](../../strongs/h/h3841.md).

<a name="2kings_24_19"></a>2Kings 24:19

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), according to all that [Yᵊhôyāqîm](../../strongs/h/h3079.md) had ['asah](../../strongs/h/h6213.md).

<a name="2kings_24_20"></a>2Kings 24:20

For through the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) it came to pass in [Yĕruwshalaim](../../strongs/h/h3389.md) and [Yehuwdah](../../strongs/h/h3063.md), until he had [shalak](../../strongs/h/h7993.md) them from his [paniym](../../strongs/h/h6440.md), that [Ṣḏqyh](../../strongs/h/h6667.md) [māraḏ](../../strongs/h/h4775.md) against the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 23](2kings_23.md) - [2Kings 25](2kings_25.md)