# [2Kings 12](https://www.blueletterbible.org/kjv/2kings/12)

<a name="2kings_12_1"></a>2Kings 12:1

In the seventh [šānâ](../../strongs/h/h8141.md) of [Yêû'](../../strongs/h/h3058.md) [Yᵊhô'Āš](../../strongs/h/h3060.md) began to [mālaḵ](../../strongs/h/h4427.md); and forty [šānâ](../../strongs/h/h8141.md) [mālaḵ](../../strongs/h/h4427.md) he in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [Ṣiḇyâ](../../strongs/h/h6645.md) of [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md).

<a name="2kings_12_2"></a>2Kings 12:2

And [Yᵊhô'Āš](../../strongs/h/h3060.md) ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) all his [yowm](../../strongs/h/h3117.md) wherein [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md) [yārâ](../../strongs/h/h3384.md) him.

<a name="2kings_12_3"></a>2Kings 12:3

But the [bāmâ](../../strongs/h/h1116.md) were not [cuwr](../../strongs/h/h5493.md): the ['am](../../strongs/h/h5971.md) still [zabach](../../strongs/h/h2076.md) and [qāṭar](../../strongs/h/h6999.md) in the [bāmâ](../../strongs/h/h1116.md).

<a name="2kings_12_4"></a>2Kings 12:4

And [Yᵊhô'Āš](../../strongs/h/h3060.md) ['āmar](../../strongs/h/h559.md) to the [kōhēn](../../strongs/h/h3548.md), All the [keceph](../../strongs/h/h3701.md) of the [qodesh](../../strongs/h/h6944.md) that is [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), even the [keceph](../../strongs/h/h3701.md) of every ['iysh](../../strongs/h/h376.md) that ['abar](../../strongs/h/h5674.md) the account, the [keceph](../../strongs/h/h3701.md) that every [nephesh](../../strongs/h/h5315.md) is set [ʿēreḵ](../../strongs/h/h6187.md), and all the [keceph](../../strongs/h/h3701.md) that [ʿālâ](../../strongs/h/h5927.md) into any ['iysh](../../strongs/h/h376.md) [leb](../../strongs/h/h3820.md) to [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md),

<a name="2kings_12_5"></a>2Kings 12:5

Let the [kōhēn](../../strongs/h/h3548.md) [laqach](../../strongs/h/h3947.md) it to them, every ['iysh](../../strongs/h/h376.md) of his [makār](../../strongs/h/h4378.md): and let them [ḥāzaq](../../strongs/h/h2388.md) the [beḏeq](../../strongs/h/h919.md) of the [bayith](../../strongs/h/h1004.md), wheresoever any [beḏeq](../../strongs/h/h919.md) shall be [māṣā'](../../strongs/h/h4672.md).

<a name="2kings_12_6"></a>2Kings 12:6

But it was so, that in the three [šānâ](../../strongs/h/h8141.md) and twentieth [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [Yᵊhô'Āš](../../strongs/h/h3060.md) the [kōhēn](../../strongs/h/h3548.md) had not [ḥāzaq](../../strongs/h/h2388.md) the [beḏeq](../../strongs/h/h919.md) of the [bayith](../../strongs/h/h1004.md).

<a name="2kings_12_7"></a>2Kings 12:7

Then [melek](../../strongs/h/h4428.md) [Yᵊhô'Āš](../../strongs/h/h3060.md) [qara'](../../strongs/h/h7121.md) for [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md), and the other [kōhēn](../../strongs/h/h3548.md), and ['āmar](../../strongs/h/h559.md) unto them, Why [ḥāzaq](../../strongs/h/h2388.md) ye not the [beḏeq](../../strongs/h/h919.md) of the [bayith](../../strongs/h/h1004.md)? now therefore [laqach](../../strongs/h/h3947.md) no more [keceph](../../strongs/h/h3701.md) of your [makār](../../strongs/h/h4378.md), but [nathan](../../strongs/h/h5414.md) it for the [beḏeq](../../strongs/h/h919.md) of the [bayith](../../strongs/h/h1004.md).

<a name="2kings_12_8"></a>2Kings 12:8

And the [kōhēn](../../strongs/h/h3548.md) ['ôṯ](../../strongs/h/h225.md) to [laqach](../../strongs/h/h3947.md) no more [keceph](../../strongs/h/h3701.md) of the ['am](../../strongs/h/h5971.md), neither to [ḥāzaq](../../strongs/h/h2388.md) the [beḏeq](../../strongs/h/h919.md) of the [bayith](../../strongs/h/h1004.md).

<a name="2kings_12_9"></a>2Kings 12:9

But [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md) [laqach](../../strongs/h/h3947.md) an ['ārôn](../../strongs/h/h727.md), and [nāqaḇ](../../strongs/h/h5344.md) a [ḥôr](../../strongs/h/h2356.md) in the [deleṯ](../../strongs/h/h1817.md) of it, and [nathan](../../strongs/h/h5414.md) it beside the [mizbeach](../../strongs/h/h4196.md), on the [yamiyn](../../strongs/h/h3225.md) as ['iysh](../../strongs/h/h376.md) [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md): and the [kōhēn](../../strongs/h/h3548.md) that [shamar](../../strongs/h/h8104.md) the [caph](../../strongs/h/h5592.md) [nathan](../../strongs/h/h5414.md) therein all the [keceph](../../strongs/h/h3701.md) that was [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_12_10"></a>2Kings 12:10

And it was so, when they [ra'ah](../../strongs/h/h7200.md) that there was [rab](../../strongs/h/h7227.md) [keceph](../../strongs/h/h3701.md) in the ['ārôn](../../strongs/h/h727.md), that the [melek](../../strongs/h/h4428.md) [sāp̄ar](../../strongs/h/h5608.md) and the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md) [ʿālâ](../../strongs/h/h5927.md), and they put up in [ṣûr](../../strongs/h/h6696.md), and [mānâ](../../strongs/h/h4487.md) the [keceph](../../strongs/h/h3701.md) that was [māṣā'](../../strongs/h/h4672.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_12_11"></a>2Kings 12:11

And they [nathan](../../strongs/h/h5414.md) the [keceph](../../strongs/h/h3701.md), being [tāḵan](../../strongs/h/h8505.md), into the [yad](../../strongs/h/h3027.md) of them that ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md), that had the [paqad](../../strongs/h/h6485.md) [paqad](../../strongs/h/h6485.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md): and they laid it [yāṣā'](../../strongs/h/h3318.md) to the [ḥārāš](../../strongs/h/h2796.md) ['ets](../../strongs/h/h6086.md) and [bānâ](../../strongs/h/h1129.md), that ['asah](../../strongs/h/h6213.md) upon the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md),

<a name="2kings_12_12"></a>2Kings 12:12

And to [gāḏar](../../strongs/h/h1443.md), and [ḥāṣaḇ](../../strongs/h/h2672.md) of ['eben](../../strongs/h/h68.md), and to [qānâ](../../strongs/h/h7069.md) ['ets](../../strongs/h/h6086.md) and [maḥṣēḇ](../../strongs/h/h4274.md) ['eben](../../strongs/h/h68.md) to [ḥāzaq](../../strongs/h/h2388.md) the [beḏeq](../../strongs/h/h919.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and for all that was laid [yāṣā'](../../strongs/h/h3318.md) for the [bayith](../../strongs/h/h1004.md) to [ḥāzqâ](../../strongs/h/h2394.md) it.

<a name="2kings_12_13"></a>2Kings 12:13

Howbeit there were not ['asah](../../strongs/h/h6213.md) for the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) [caph](../../strongs/h/h5592.md) of [keceph](../../strongs/h/h3701.md), [mᵊzammerê](../../strongs/h/h4212.md), [mizrāq](../../strongs/h/h4219.md), [ḥăṣōṣrâ](../../strongs/h/h2689.md), any [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md), or [kĕliy](../../strongs/h/h3627.md) of [keceph](../../strongs/h/h3701.md), of the [keceph](../../strongs/h/h3701.md) that was [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md):

<a name="2kings_12_14"></a>2Kings 12:14

But they [nathan](../../strongs/h/h5414.md) that to the [mĕla'kah](../../strongs/h/h4399.md) ['asah](../../strongs/h/h6213.md), and [ḥāzaq](../../strongs/h/h2388.md) therewith the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_12_15"></a>2Kings 12:15

Moreover they [chashab](../../strongs/h/h2803.md) not with the ['enowsh](../../strongs/h/h582.md), into whose [yad](../../strongs/h/h3027.md) they [nathan](../../strongs/h/h5414.md) the [keceph](../../strongs/h/h3701.md) to be [nathan](../../strongs/h/h5414.md) on [mĕla'kah](../../strongs/h/h4399.md): for they ['asah](../../strongs/h/h6213.md) ['ĕmûnâ](../../strongs/h/h530.md).

<a name="2kings_12_16"></a>2Kings 12:16

The ['āšām](../../strongs/h/h817.md) [keceph](../../strongs/h/h3701.md) and [chatta'ath](../../strongs/h/h2403.md) [keceph](../../strongs/h/h3701.md) was not [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md): it was the [kōhēn](../../strongs/h/h3548.md).

<a name="2kings_12_17"></a>2Kings 12:17

Then [Ḥăzā'Ēl](../../strongs/h/h2371.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) [ʿālâ](../../strongs/h/h5927.md), and [lāḥam](../../strongs/h/h3898.md) against [Gaṯ](../../strongs/h/h1661.md), and [lāḵaḏ](../../strongs/h/h3920.md) it: and [Ḥăzā'Ēl](../../strongs/h/h2371.md) [śûm](../../strongs/h/h7760.md) his [paniym](../../strongs/h/h6440.md) to [ʿālâ](../../strongs/h/h5927.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2kings_12_18"></a>2Kings 12:18

And [Yᵊhô'Āš](../../strongs/h/h3060.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [laqach](../../strongs/h/h3947.md) all the [qodesh](../../strongs/h/h6944.md) that [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), and [Yᵊhôrām](../../strongs/h/h3088.md), and ['Ăḥazyâ](../../strongs/h/h274.md), his ['ab](../../strongs/h/h1.md), [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), had [qadash](../../strongs/h/h6942.md), and his own [qodesh](../../strongs/h/h6944.md), and all the [zāhāḇ](../../strongs/h/h2091.md) that was [māṣā'](../../strongs/h/h4672.md) in the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and in the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and [shalach](../../strongs/h/h7971.md) it to [Ḥăzā'Ēl](../../strongs/h/h2371.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md): and he [ʿālâ](../../strongs/h/h5927.md) from [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2kings_12_19"></a>2Kings 12:19

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yô'Āš](../../strongs/h/h3101.md), and all that he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)?

<a name="2kings_12_20"></a>2Kings 12:20

And his ['ebed](../../strongs/h/h5650.md) [quwm](../../strongs/h/h6965.md), and [qāšar](../../strongs/h/h7194.md) a [qešer](../../strongs/h/h7195.md), and [nakah](../../strongs/h/h5221.md) [Yô'Āš](../../strongs/h/h3101.md) in the [bayith](../../strongs/h/h1004.md) of [Millô'](../../strongs/h/h4407.md) [Bêṯ Millô'](../../strongs/h/h1037.md), which [yarad](../../strongs/h/h3381.md) to [sillā'](../../strongs/h/h5538.md).

<a name="2kings_12_21"></a>2Kings 12:21

For [Yôzāḵār](../../strongs/h/h3108.md) the [ben](../../strongs/h/h1121.md) of [ŠimʿĀṯ](../../strongs/h/h8100.md), and [Yᵊhôzāḇāḏ](../../strongs/h/h3075.md) the [ben](../../strongs/h/h1121.md) of [Šōmēr](../../strongs/h/h7763.md), his ['ebed](../../strongs/h/h5650.md), [nakah](../../strongs/h/h5221.md) him, and he [muwth](../../strongs/h/h4191.md); and they [qāḇar](../../strongs/h/h6912.md) him with his ['ab](../../strongs/h/h1.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md): and ['Ămaṣyâ](../../strongs/h/h558.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 11](2kings_11.md) - [2Kings 13](2kings_13.md)