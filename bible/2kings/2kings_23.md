# [2Kings 23](https://www.blueletterbible.org/kjv/2kings/23)

<a name="2kings_23_1"></a>2Kings 23:1

And the [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md), and they ['āsap̄](../../strongs/h/h622.md) unto him all the [zāqēn](../../strongs/h/h2205.md) of [Yehuwdah](../../strongs/h/h3063.md) and of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2kings_23_2"></a>2Kings 23:2

And the [melek](../../strongs/h/h4428.md) [ʿālâ](../../strongs/h/h5927.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and all the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) and all the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) with him, and the [kōhēn](../../strongs/h/h3548.md), and the [nāḇî'](../../strongs/h/h5030.md), and all the ['am](../../strongs/h/h5971.md), both [qāṭān](../../strongs/h/h6996.md) and [gadowl](../../strongs/h/h1419.md): and he [qara'](../../strongs/h/h7121.md) in their ['ozen](../../strongs/h/h241.md) all the [dabar](../../strongs/h/h1697.md) of the [sēp̄er](../../strongs/h/h5612.md) of the [bĕriyth](../../strongs/h/h1285.md) which was [māṣā'](../../strongs/h/h4672.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_23_3"></a>2Kings 23:3

And the [melek](../../strongs/h/h4428.md) ['amad](../../strongs/h/h5975.md) by a [ʿammûḏ](../../strongs/h/h5982.md), and [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), to [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md), and to [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md) and his [ʿēḏûṯ](../../strongs/h/h5715.md) and his [chuqqah](../../strongs/h/h2708.md) with all their [leb](../../strongs/h/h3820.md) and all their [nephesh](../../strongs/h/h5315.md), to [quwm](../../strongs/h/h6965.md) the [dabar](../../strongs/h/h1697.md) of this [bĕriyth](../../strongs/h/h1285.md) that were [kāṯaḇ](../../strongs/h/h3789.md) in this [sēp̄er](../../strongs/h/h5612.md). And all the ['am](../../strongs/h/h5971.md) ['amad](../../strongs/h/h5975.md) to the [bĕriyth](../../strongs/h/h1285.md).

<a name="2kings_23_4"></a>2Kings 23:4

And the [melek](../../strongs/h/h4428.md) [tsavah](../../strongs/h/h6680.md) [Ḥilqîyâ](../../strongs/h/h2518.md) the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md), and the [kōhēn](../../strongs/h/h3548.md) of the second [mišnê](../../strongs/h/h4932.md), and the [shamar](../../strongs/h/h8104.md) of the [caph](../../strongs/h/h5592.md), to [yāṣā'](../../strongs/h/h3318.md) out of the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md) all the [kĕliy](../../strongs/h/h3627.md) that were ['asah](../../strongs/h/h6213.md) for [BaʿAl](../../strongs/h/h1168.md), and for the ['ăšērâ](../../strongs/h/h842.md), and for all the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md): and he [śārap̄](../../strongs/h/h8313.md) them [ḥûṣ](../../strongs/h/h2351.md) [Yĕruwshalaim](../../strongs/h/h3389.md) in the [šᵊḏēmâ](../../strongs/h/h7709.md) of [Qiḏrôn](../../strongs/h/h6939.md), and [nasa'](../../strongs/h/h5375.md) the ['aphar](../../strongs/h/h6083.md) of them unto [Bêṯ-'ēl](../../strongs/h/h1008.md).

<a name="2kings_23_5"></a>2Kings 23:5

And he put [shabath](../../strongs/h/h7673.md) the [kōmer](../../strongs/h/h3649.md), whom the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) had [nathan](../../strongs/h/h5414.md) to [qāṭar](../../strongs/h/h6999.md) in the [bāmâ](../../strongs/h/h1116.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and in the [mēsaḇ](../../strongs/h/h4524.md) [Yĕruwshalaim](../../strongs/h/h3389.md); them also that [qāṭar](../../strongs/h/h6999.md) unto [BaʿAl](../../strongs/h/h1168.md), to the [šemeš](../../strongs/h/h8121.md), and to the [yareach](../../strongs/h/h3394.md), and to the [mazzālôṯ](../../strongs/h/h4208.md), and to all the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md).

<a name="2kings_23_6"></a>2Kings 23:6

And he [yāṣā'](../../strongs/h/h3318.md) the ['ăšērâ](../../strongs/h/h842.md) from the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), [ḥûṣ](../../strongs/h/h2351.md) [Yĕruwshalaim](../../strongs/h/h3389.md), unto the [nachal](../../strongs/h/h5158.md) [Qiḏrôn](../../strongs/h/h6939.md), and [śārap̄](../../strongs/h/h8313.md) it at the [nachal](../../strongs/h/h5158.md) [Qiḏrôn](../../strongs/h/h6939.md), and [dāqaq](../../strongs/h/h1854.md) it to ['aphar](../../strongs/h/h6083.md), and [shalak](../../strongs/h/h7993.md) the ['aphar](../../strongs/h/h6083.md) thereof upon the [qeber](../../strongs/h/h6913.md) of the [ben](../../strongs/h/h1121.md) of the ['am](../../strongs/h/h5971.md).

<a name="2kings_23_7"></a>2Kings 23:7

And he [nāṯaṣ](../../strongs/h/h5422.md) the [bayith](../../strongs/h/h1004.md) of the [qāḏēš](../../strongs/h/h6945.md), that were by the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), where the ['ishshah](../../strongs/h/h802.md) ['āraḡ](../../strongs/h/h707.md) [bayith](../../strongs/h/h1004.md) for the ['ăšērâ](../../strongs/h/h842.md).

<a name="2kings_23_8"></a>2Kings 23:8

And he [bow'](../../strongs/h/h935.md) all the [kōhēn](../../strongs/h/h3548.md) out of the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and [ṭāmē'](../../strongs/h/h2930.md) the [bāmâ](../../strongs/h/h1116.md) where the [kōhēn](../../strongs/h/h3548.md) had [qāṭar](../../strongs/h/h6999.md), from [Geḇaʿ](../../strongs/h/h1387.md) to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md), and brake [nāṯaṣ](../../strongs/h/h5422.md) the [bāmâ](../../strongs/h/h1116.md) of the [sha'ar](../../strongs/h/h8179.md) that were in the entering [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md) of [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [śar](../../strongs/h/h8269.md) of the [ʿîr](../../strongs/h/h5892.md), which were on an ['iysh](../../strongs/h/h376.md) [śᵊmō'l](../../strongs/h/h8040.md) at the [sha'ar](../../strongs/h/h8179.md) of the [ʿîr](../../strongs/h/h5892.md).

<a name="2kings_23_9"></a>2Kings 23:9

Nevertheless the [kōhēn](../../strongs/h/h3548.md) of the [bāmâ](../../strongs/h/h1116.md) [ʿālâ](../../strongs/h/h5927.md) not to the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), but they did ['akal](../../strongs/h/h398.md) of the [maṣṣâ](../../strongs/h/h4682.md) [tavek](../../strongs/h/h8432.md) their ['ach](../../strongs/h/h251.md).

<a name="2kings_23_10"></a>2Kings 23:10

And he [ṭāmē'](../../strongs/h/h2930.md) [Tōp̄Eṯ](../../strongs/h/h8612.md), which is in the [gay'](../../strongs/h/h1516.md) of the [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md) of [Hinnōm](../../strongs/h/h2011.md), that no ['iysh](../../strongs/h/h376.md) might make his [ben](../../strongs/h/h1121.md) or his [bath](../../strongs/h/h1323.md) to ['abar](../../strongs/h/h5674.md) the ['esh](../../strongs/h/h784.md) to [mōleḵ](../../strongs/h/h4432.md).

<a name="2kings_23_11"></a>2Kings 23:11

And he [shabath](../../strongs/h/h7673.md) the [sûs](../../strongs/h/h5483.md) that the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) had [nathan](../../strongs/h/h5414.md) to the [šemeš](../../strongs/h/h8121.md), at the [bow'](../../strongs/h/h935.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), by the [liškâ](../../strongs/h/h3957.md) of [Nᵊṯan-Meleḵ](../../strongs/h/h5419.md) the [sārîs](../../strongs/h/h5631.md), which was in the [Parbār](../../strongs/h/h6503.md), and [śārap̄](../../strongs/h/h8313.md) the [merkāḇâ](../../strongs/h/h4818.md) of the [šemeš](../../strongs/h/h8121.md) with ['esh](../../strongs/h/h784.md).

<a name="2kings_23_12"></a>2Kings 23:12

And the [mizbeach](../../strongs/h/h4196.md) that were on the [gāḡ](../../strongs/h/h1406.md) of the [ʿălîyâ](../../strongs/h/h5944.md) of ['Āḥāz](../../strongs/h/h271.md), which the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) had ['asah](../../strongs/h/h6213.md), and the [mizbeach](../../strongs/h/h4196.md) which [Mᵊnaššê](../../strongs/h/h4519.md) had ['asah](../../strongs/h/h6213.md) in the two [ḥāṣēr](../../strongs/h/h2691.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), did the [melek](../../strongs/h/h4428.md) [nāṯaṣ](../../strongs/h/h5422.md), and [rûṣ](../../strongs/h/h7323.md) them from thence, and [shalak](../../strongs/h/h7993.md) the ['aphar](../../strongs/h/h6083.md) of them into the [nachal](../../strongs/h/h5158.md) [Qiḏrôn](../../strongs/h/h6939.md).

<a name="2kings_23_13"></a>2Kings 23:13

And the [bāmâ](../../strongs/h/h1116.md) that were [paniym](../../strongs/h/h6440.md) [Yĕruwshalaim](../../strongs/h/h3389.md), which were on the [yamiyn](../../strongs/h/h3225.md) of the [har](../../strongs/h/h2022.md) of [mašḥîṯ](../../strongs/h/h4889.md), which [Šᵊlōmô](../../strongs/h/h8010.md) the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) had [bānâ](../../strongs/h/h1129.md) for [ʿAštōreṯ](../../strongs/h/h6253.md) the [šiqqûṣ](../../strongs/h/h8251.md) of the [Ṣîḏōnî](../../strongs/h/h6722.md), and for [kᵊmôš](../../strongs/h/h3645.md) the [šiqqûṣ](../../strongs/h/h8251.md) of the [Mô'āḇ](../../strongs/h/h4124.md), and for [Malkām](../../strongs/h/h4445.md) the [tôʿēḇâ](../../strongs/h/h8441.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), did the [melek](../../strongs/h/h4428.md) [ṭāmē'](../../strongs/h/h2930.md).

<a name="2kings_23_14"></a>2Kings 23:14

And he [shabar](../../strongs/h/h7665.md) the [maṣṣēḇâ](../../strongs/h/h4676.md), and [karath](../../strongs/h/h3772.md) the ['ăšērâ](../../strongs/h/h842.md), and [mālā'](../../strongs/h/h4390.md) their [maqowm](../../strongs/h/h4725.md) with the ['etsem](../../strongs/h/h6106.md) of ['āḏām](../../strongs/h/h120.md).

<a name="2kings_23_15"></a>2Kings 23:15

Moreover the [mizbeach](../../strongs/h/h4196.md) that was at [Bêṯ-'ēl](../../strongs/h/h1008.md), and the [bāmâ](../../strongs/h/h1116.md) which [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), who made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md), had ['asah](../../strongs/h/h6213.md), both that [mizbeach](../../strongs/h/h4196.md) and the [bāmâ](../../strongs/h/h1116.md) he [nāṯaṣ](../../strongs/h/h5422.md), and [śārap̄](../../strongs/h/h8313.md) the [bāmâ](../../strongs/h/h1116.md), and [dāqaq](../../strongs/h/h1854.md) it to ['aphar](../../strongs/h/h6083.md), and [śārap̄](../../strongs/h/h8313.md) the ['ăšērâ](../../strongs/h/h842.md).

<a name="2kings_23_16"></a>2Kings 23:16

And as [Yō'Šîyâ](../../strongs/h/h2977.md) [panah](../../strongs/h/h6437.md) himself, he [ra'ah](../../strongs/h/h7200.md) the [qeber](../../strongs/h/h6913.md) that were there in the [har](../../strongs/h/h2022.md), and [shalach](../../strongs/h/h7971.md), and [laqach](../../strongs/h/h3947.md) the ['etsem](../../strongs/h/h6106.md) out of the [qeber](../../strongs/h/h6913.md), and [śārap̄](../../strongs/h/h8313.md) them upon the [mizbeach](../../strongs/h/h4196.md), and [ṭāmē'](../../strongs/h/h2930.md) it, according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) which the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) [qara'](../../strongs/h/h7121.md), who [qara'](../../strongs/h/h7121.md) these [dabar](../../strongs/h/h1697.md).

<a name="2kings_23_17"></a>2Kings 23:17

Then he ['āmar](../../strongs/h/h559.md), What [ṣîyûn](../../strongs/h/h6725.md) is [hallāz](../../strongs/h/h1975.md) that I [ra'ah](../../strongs/h/h7200.md)? And the ['enowsh](../../strongs/h/h582.md) of the [ʿîr](../../strongs/h/h5892.md) ['āmar](../../strongs/h/h559.md) him, It is the [qeber](../../strongs/h/h6913.md) of the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), which [bow'](../../strongs/h/h935.md) from [Yehuwdah](../../strongs/h/h3063.md), and [qara'](../../strongs/h/h7121.md) these [dabar](../../strongs/h/h1697.md) that thou hast ['asah](../../strongs/h/h6213.md) against the [mizbeach](../../strongs/h/h4196.md) of [Bêṯ-'ēl](../../strongs/h/h1008.md).

<a name="2kings_23_18"></a>2Kings 23:18

And he ['āmar](../../strongs/h/h559.md), Let him [yānaḥ](../../strongs/h/h3240.md); let no ['iysh](../../strongs/h/h376.md) [nûaʿ](../../strongs/h/h5128.md) his ['etsem](../../strongs/h/h6106.md). So they let his ['etsem](../../strongs/h/h6106.md) [mālaṭ](../../strongs/h/h4422.md), with the ['etsem](../../strongs/h/h6106.md) of the [nāḇî'](../../strongs/h/h5030.md) that [bow'](../../strongs/h/h935.md) of [Šōmrôn](../../strongs/h/h8111.md).

<a name="2kings_23_19"></a>2Kings 23:19

And all the [bayith](../../strongs/h/h1004.md) also of the [bāmâ](../../strongs/h/h1116.md) that were in the [ʿîr](../../strongs/h/h5892.md) of [Šōmrôn](../../strongs/h/h8111.md), which the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) had ['asah](../../strongs/h/h6213.md) to [kāʿas](../../strongs/h/h3707.md), [Yō'Šîyâ](../../strongs/h/h2977.md) [cuwr](../../strongs/h/h5493.md), and ['asah](../../strongs/h/h6213.md) to them according to all the [ma'aseh](../../strongs/h/h4639.md) that he had ['asah](../../strongs/h/h6213.md) in [Bêṯ-'ēl](../../strongs/h/h1008.md).

<a name="2kings_23_20"></a>2Kings 23:20

And he [zabach](../../strongs/h/h2076.md) all the [kōhēn](../../strongs/h/h3548.md) of the [bāmâ](../../strongs/h/h1116.md) that were there upon the [mizbeach](../../strongs/h/h4196.md), and [śārap̄](../../strongs/h/h8313.md) ['āḏām](../../strongs/h/h120.md) ['etsem](../../strongs/h/h6106.md) upon them, and [shuwb](../../strongs/h/h7725.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2kings_23_21"></a>2Kings 23:21

And the [melek](../../strongs/h/h4428.md) [tsavah](../../strongs/h/h6680.md) all the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) unto [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), as it is [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of this [bĕriyth](../../strongs/h/h1285.md).

<a name="2kings_23_22"></a>2Kings 23:22

Surely there was not ['asah](../../strongs/h/h6213.md) such a [pecach](../../strongs/h/h6453.md) from the [yowm](../../strongs/h/h3117.md) of the [shaphat](../../strongs/h/h8199.md) that [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md), nor in all the [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), nor of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md);

<a name="2kings_23_23"></a>2Kings 23:23

But in the eighteenth [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [Yō'Šîyâ](../../strongs/h/h2977.md), wherein this [pecach](../../strongs/h/h6453.md) was ['asah](../../strongs/h/h6213.md) to [Yĕhovah](../../strongs/h/h3068.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2kings_23_24"></a>2Kings 23:24

Moreover the ['ôḇ](../../strongs/h/h178.md), and the [yiḏʿōnî](../../strongs/h/h3049.md), and the [tᵊrāp̄îm](../../strongs/h/h8655.md), and the [gillûl](../../strongs/h/h1544.md), and all the [šiqqûṣ](../../strongs/h/h8251.md) that were [ra'ah](../../strongs/h/h7200.md) in the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md) and in [Yĕruwshalaim](../../strongs/h/h3389.md), did [Yō'Šîyâ](../../strongs/h/h2977.md) put [bāʿar](../../strongs/h/h1197.md), that he might [quwm](../../strongs/h/h6965.md) the [dabar](../../strongs/h/h1697.md) of the [towrah](../../strongs/h/h8451.md) which were [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) that [Ḥilqîyâ](../../strongs/h/h2518.md) the [kōhēn](../../strongs/h/h3548.md) [māṣā'](../../strongs/h/h4672.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_23_25"></a>2Kings 23:25

And like unto him was there no [melek](../../strongs/h/h4428.md) [paniym](../../strongs/h/h6440.md) him, that [shuwb](../../strongs/h/h7725.md) to [Yĕhovah](../../strongs/h/h3068.md) with all his [lebab](../../strongs/h/h3824.md), and with all his [nephesh](../../strongs/h/h5315.md), and with all his [me'od](../../strongs/h/h3966.md), according to all the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md); neither ['aḥar](../../strongs/h/h310.md) him [quwm](../../strongs/h/h6965.md) there any like him.

<a name="2kings_23_26"></a>2Kings 23:26

Notwithstanding [Yĕhovah](../../strongs/h/h3068.md) [shuwb](../../strongs/h/h7725.md) not from the [charown](../../strongs/h/h2740.md) of his [gadowl](../../strongs/h/h1419.md) ['aph](../../strongs/h/h639.md), wherewith his ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md) against [Yehuwdah](../../strongs/h/h3063.md), because of all the [ka'ac](../../strongs/h/h3708.md) that [Mᵊnaššê](../../strongs/h/h4519.md) had [kāʿas](../../strongs/h/h3707.md) him withal.

<a name="2kings_23_27"></a>2Kings 23:27

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), I will [cuwr](../../strongs/h/h5493.md) [Yehuwdah](../../strongs/h/h3063.md) also out of my [paniym](../../strongs/h/h6440.md), as I have [cuwr](../../strongs/h/h5493.md) [Yisra'el](../../strongs/h/h3478.md), and will [mā'as](../../strongs/h/h3988.md) this [ʿîr](../../strongs/h/h5892.md) [Yĕruwshalaim](../../strongs/h/h3389.md) which I have [bāḥar](../../strongs/h/h977.md), and the [bayith](../../strongs/h/h1004.md) of which I ['āmar](../../strongs/h/h559.md), My [shem](../../strongs/h/h8034.md) shall be there.

<a name="2kings_23_28"></a>2Kings 23:28

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yō'Šîyâ](../../strongs/h/h2977.md), and all that he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)?

<a name="2kings_23_29"></a>2Kings 23:29

In his [yowm](../../strongs/h/h3117.md) [ParʿÔ Nᵊḵô](../../strongs/h/h6549.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) [ʿālâ](../../strongs/h/h5927.md) against the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) to the [nāhār](../../strongs/h/h5104.md) [Pᵊrāṯ](../../strongs/h/h6578.md): and [melek](../../strongs/h/h4428.md) [Yō'Šîyâ](../../strongs/h/h2977.md) [yālaḵ](../../strongs/h/h3212.md) [qārā'](../../strongs/h/h7125.md) him; and he [muwth](../../strongs/h/h4191.md) him at [Mᵊḡidôn](../../strongs/h/h4023.md), when he had [ra'ah](../../strongs/h/h7200.md) him.

<a name="2kings_23_30"></a>2Kings 23:30

And his ['ebed](../../strongs/h/h5650.md) [rāḵaḇ](../../strongs/h/h7392.md) him [muwth](../../strongs/h/h4191.md) from [Mᵊḡidôn](../../strongs/h/h4023.md), and [bow'](../../strongs/h/h935.md) him to [Yĕruwshalaim](../../strongs/h/h3389.md), and [qāḇar](../../strongs/h/h6912.md) him in his own [qᵊḇûrâ](../../strongs/h/h6900.md). And the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) [laqach](../../strongs/h/h3947.md) [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md), and [māšaḥ](../../strongs/h/h4886.md) him, and made him [mālaḵ](../../strongs/h/h4427.md) in his ['ab](../../strongs/h/h1.md) stead.

<a name="2kings_23_31"></a>2Kings 23:31

[Yᵊhô'Āḥāz](../../strongs/h/h3059.md) was twenty and three [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md); and he [mālaḵ](../../strongs/h/h4427.md) three [ḥōḏeš](../../strongs/h/h2320.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [Ḥămûṭal](../../strongs/h/h2537.md), the [bath](../../strongs/h/h1323.md) of [Yirmᵊyâ](../../strongs/h/h3414.md) of [Liḇnâ](../../strongs/h/h3841.md).

<a name="2kings_23_32"></a>2Kings 23:32

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), according to all that his ['ab](../../strongs/h/h1.md) had ['asah](../../strongs/h/h6213.md).

<a name="2kings_23_33"></a>2Kings 23:33

And [ParʿÔ Nᵊḵô](../../strongs/h/h6549.md) put him in ['āsar](../../strongs/h/h631.md) at [Riḇlâ](../../strongs/h/h7247.md) in the ['erets](../../strongs/h/h776.md) of [Ḥămāṯ](../../strongs/h/h2574.md), that he might not [mālaḵ](../../strongs/h/h4427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md); and [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) to a [ʿōneš](../../strongs/h/h6066.md) of an hundred [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md), and a [kikār](../../strongs/h/h3603.md) of [zāhāḇ](../../strongs/h/h2091.md).

<a name="2kings_23_34"></a>2Kings 23:34

And [ParʿÔ Nᵊḵô](../../strongs/h/h6549.md) made ['Elyāqîm](../../strongs/h/h471.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [mālaḵ](../../strongs/h/h4427.md) in the room of [Yō'Šîyâ](../../strongs/h/h2977.md) his ['ab](../../strongs/h/h1.md), and [cabab](../../strongs/h/h5437.md) his [shem](../../strongs/h/h8034.md) to [Yᵊhôyāqîm](../../strongs/h/h3079.md), and [laqach](../../strongs/h/h3947.md) [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) [laqach](../../strongs/h/h3947.md): and he [bow'](../../strongs/h/h935.md) to [Mitsrayim](../../strongs/h/h4714.md), and [muwth](../../strongs/h/h4191.md) there.

<a name="2kings_23_35"></a>2Kings 23:35

And [Yᵊhôyāqîm](../../strongs/h/h3079.md) [nathan](../../strongs/h/h5414.md) the [keceph](../../strongs/h/h3701.md) and the [zāhāḇ](../../strongs/h/h2091.md) to [Parʿô](../../strongs/h/h6547.md); but he ['arak](../../strongs/h/h6186.md) the ['erets](../../strongs/h/h776.md) to [nathan](../../strongs/h/h5414.md) the [keceph](../../strongs/h/h3701.md) according to the [peh](../../strongs/h/h6310.md) of [Parʿô](../../strongs/h/h6547.md): he [nāḡaś](../../strongs/h/h5065.md) the [keceph](../../strongs/h/h3701.md) and the [zāhāḇ](../../strongs/h/h2091.md) of the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), of every ['iysh](../../strongs/h/h376.md) according to his [ʿēreḵ](../../strongs/h/h6187.md), to [nathan](../../strongs/h/h5414.md) it unto [ParʿÔ Nᵊḵô](../../strongs/h/h6549.md).

<a name="2kings_23_36"></a>2Kings 23:36

[Yᵊhôyāqîm](../../strongs/h/h3079.md) was twenty and five [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md); and he [mālaḵ](../../strongs/h/h4427.md) eleven [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [Zᵊḇîḏâ](../../strongs/h/h2080.md), the [bath](../../strongs/h/h1323.md) of [Pᵊḏāyâ](../../strongs/h/h6305.md) of [Rûmâ](../../strongs/h/h7316.md).

<a name="2kings_23_37"></a>2Kings 23:37

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), according to all that his ['ab](../../strongs/h/h1.md) had ['asah](../../strongs/h/h6213.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 22](2kings_22.md) - [2Kings 24](2kings_24.md)