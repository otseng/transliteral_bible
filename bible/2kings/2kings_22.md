# [2Kings 22](https://www.blueletterbible.org/kjv/2kings/22)

<a name="2kings_22_1"></a>2Kings 22:1

[Yō'Šîyâ](../../strongs/h/h2977.md) was eight [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) thirty and one [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [Yᵊḏîḏâ](../../strongs/h/h3040.md), the [bath](../../strongs/h/h1323.md) of [ʿĂḏāyâ](../../strongs/h/h5718.md) of [Bāṣqaṯ](../../strongs/h/h1218.md).

<a name="2kings_22_2"></a>2Kings 22:2

And he ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and [yālaḵ](../../strongs/h/h3212.md) in all the [derek](../../strongs/h/h1870.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md), and [cuwr](../../strongs/h/h5493.md) not to the [yamiyn](../../strongs/h/h3225.md) or to the [śᵊmō'l](../../strongs/h/h8040.md).

<a name="2kings_22_3"></a>2Kings 22:3

And it came to pass in the eighteenth [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [Yō'Šîyâ](../../strongs/h/h2977.md), that the [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md) [Šāp̄ān](../../strongs/h/h8227.md) the [ben](../../strongs/h/h1121.md) of ['Ăṣalyâû](../../strongs/h/h683.md), the [ben](../../strongs/h/h1121.md) of [Mᵊšullām](../../strongs/h/h4918.md), the [sāp̄ar](../../strongs/h/h5608.md), to the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="2kings_22_4"></a>2Kings 22:4

[ʿālâ](../../strongs/h/h5927.md) to [Ḥilqîyâ](../../strongs/h/h2518.md) the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md), that he may [tamam](../../strongs/h/h8552.md) the [keceph](../../strongs/h/h3701.md) which is [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), which the [shamar](../../strongs/h/h8104.md) of the [caph](../../strongs/h/h5592.md) have ['āsap̄](../../strongs/h/h622.md) of the ['am](../../strongs/h/h5971.md):

<a name="2kings_22_5"></a>2Kings 22:5

And let them [nathan](../../strongs/h/h5414.md) it into the [yad](../../strongs/h/h3027.md) of the ['asah](../../strongs/h/h6213.md) of the [mĕla'kah](../../strongs/h/h4399.md), that have the [paqad](../../strongs/h/h6485.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md): and let them [nathan](../../strongs/h/h5414.md) it to the ['asah](../../strongs/h/h6213.md) of the [mĕla'kah](../../strongs/h/h4399.md) which is in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), to [ḥāzaq](../../strongs/h/h2388.md) the [beḏeq](../../strongs/h/h919.md) of the [bayith](../../strongs/h/h1004.md),

<a name="2kings_22_6"></a>2Kings 22:6

Unto [ḥārāš](../../strongs/h/h2796.md), and [bānâ](../../strongs/h/h1129.md), and [gāḏar](../../strongs/h/h1443.md), and to [qānâ](../../strongs/h/h7069.md) ['ets](../../strongs/h/h6086.md) and [maḥṣēḇ](../../strongs/h/h4274.md) ['eben](../../strongs/h/h68.md) to [ḥāzaq](../../strongs/h/h2388.md) the [bayith](../../strongs/h/h1004.md).

<a name="2kings_22_7"></a>2Kings 22:7

Howbeit there was no [chashab](../../strongs/h/h2803.md) with them of the [keceph](../../strongs/h/h3701.md) that was [nathan](../../strongs/h/h5414.md) into their [yad](../../strongs/h/h3027.md), because they ['asah](../../strongs/h/h6213.md) ['ĕmûnâ](../../strongs/h/h530.md).

<a name="2kings_22_8"></a>2Kings 22:8

And [Ḥilqîyâ](../../strongs/h/h2518.md) the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md) ['āmar](../../strongs/h/h559.md) unto [Šāp̄ān](../../strongs/h/h8227.md) the [sāp̄ar](../../strongs/h/h5608.md), I have [māṣā'](../../strongs/h/h4672.md) the [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md). And [Ḥilqîyâ](../../strongs/h/h2518.md) [nathan](../../strongs/h/h5414.md) the [sēp̄er](../../strongs/h/h5612.md) to [Šāp̄ān](../../strongs/h/h8227.md), and he [qara'](../../strongs/h/h7121.md) it.

<a name="2kings_22_9"></a>2Kings 22:9

And [Šāp̄ān](../../strongs/h/h8227.md) the [sāp̄ar](../../strongs/h/h5608.md) [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md), and [shuwb](../../strongs/h/h7725.md) the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) [shuwb](../../strongs/h/h7725.md), and ['āmar](../../strongs/h/h559.md), Thy ['ebed](../../strongs/h/h5650.md) have [nāṯaḵ](../../strongs/h/h5413.md) the [keceph](../../strongs/h/h3701.md) that was [māṣā'](../../strongs/h/h4672.md) in the [bayith](../../strongs/h/h1004.md), and have [nathan](../../strongs/h/h5414.md) it into the [yad](../../strongs/h/h3027.md) of them that ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md), that have the [paqad](../../strongs/h/h6485.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_22_10"></a>2Kings 22:10

And [Šāp̄ān](../../strongs/h/h8227.md) the [sāp̄ar](../../strongs/h/h5608.md) [nāḡaḏ](../../strongs/h/h5046.md) the [melek](../../strongs/h/h4428.md), ['āmar](../../strongs/h/h559.md), [Ḥilqîyâ](../../strongs/h/h2518.md) the [kōhēn](../../strongs/h/h3548.md) hath [nathan](../../strongs/h/h5414.md) me a [sēp̄er](../../strongs/h/h5612.md). And [Šāp̄ān](../../strongs/h/h8227.md) [qara'](../../strongs/h/h7121.md) it [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

<a name="2kings_22_11"></a>2Kings 22:11

And it came to pass, when the [melek](../../strongs/h/h4428.md) had [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of the [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md), that he [qāraʿ](../../strongs/h/h7167.md) his [beḡeḏ](../../strongs/h/h899.md).

<a name="2kings_22_12"></a>2Kings 22:12

And the [melek](../../strongs/h/h4428.md) [tsavah](../../strongs/h/h6680.md) [Ḥilqîyâ](../../strongs/h/h2518.md) the [kōhēn](../../strongs/h/h3548.md), and ['Ăḥîqām](../../strongs/h/h296.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md), and [ʿAḵbôr](../../strongs/h/h5907.md) the [ben](../../strongs/h/h1121.md) of [Mîḵāyâ](../../strongs/h/h4320.md), and [Šāp̄ān](../../strongs/h/h8227.md) the [sāp̄ar](../../strongs/h/h5608.md), and [ʿĂśāyâ](../../strongs/h/h6222.md) an ['ebed](../../strongs/h/h5650.md) of the [melek](../../strongs/h/h4428.md), ['āmar](../../strongs/h/h559.md),

<a name="2kings_22_13"></a>2Kings 22:13

[yālaḵ](../../strongs/h/h3212.md) ye, [darash](../../strongs/h/h1875.md) of [Yĕhovah](../../strongs/h/h3068.md) for me, and for the ['am](../../strongs/h/h5971.md), and for all [Yehuwdah](../../strongs/h/h3063.md), concerning the [dabar](../../strongs/h/h1697.md) of this [sēp̄er](../../strongs/h/h5612.md) that is [māṣā'](../../strongs/h/h4672.md): for [gadowl](../../strongs/h/h1419.md) is the [chemah](../../strongs/h/h2534.md) of [Yĕhovah](../../strongs/h/h3068.md) that is [yāṣaṯ](../../strongs/h/h3341.md) against us, because our ['ab](../../strongs/h/h1.md) have not [shama'](../../strongs/h/h8085.md) unto the [dabar](../../strongs/h/h1697.md) of this [sēp̄er](../../strongs/h/h5612.md), to ['asah](../../strongs/h/h6213.md) according unto all that which is [kāṯaḇ](../../strongs/h/h3789.md) concerning us.

<a name="2kings_22_14"></a>2Kings 22:14

So [Ḥilqîyâ](../../strongs/h/h2518.md) the [kōhēn](../../strongs/h/h3548.md), and ['Ăḥîqām](../../strongs/h/h296.md), and [ʿAḵbôr](../../strongs/h/h5907.md), and [Šāp̄ān](../../strongs/h/h8227.md), and [ʿĂśāyâ](../../strongs/h/h6222.md), [yālaḵ](../../strongs/h/h3212.md) unto [Ḥuldâ](../../strongs/h/h2468.md) the [nᵊḇî'â](../../strongs/h/h5031.md), the ['ishshah](../../strongs/h/h802.md) of [Šallûm](../../strongs/h/h7967.md) the [ben](../../strongs/h/h1121.md) of [Tiqvâ](../../strongs/h/h8616.md), the [ben](../../strongs/h/h1121.md) of [Ḥarḥas](../../strongs/h/h2745.md), [shamar](../../strongs/h/h8104.md) of the [beḡeḏ](../../strongs/h/h899.md); (now she [yashab](../../strongs/h/h3427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) in the [mišnê](../../strongs/h/h4932.md);) and they [dabar](../../strongs/h/h1696.md) with her.

<a name="2kings_22_15"></a>2Kings 22:15

And she ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md) the ['iysh](../../strongs/h/h376.md) that [shalach](../../strongs/h/h7971.md) you to me,

<a name="2kings_22_16"></a>2Kings 22:16

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Behold, I will [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md) upon this [maqowm](../../strongs/h/h4725.md), and upon the [yashab](../../strongs/h/h3427.md) thereof, even all the [dabar](../../strongs/h/h1697.md) of the [sēp̄er](../../strongs/h/h5612.md) which the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) hath [qara'](../../strongs/h/h7121.md):

<a name="2kings_22_17"></a>2Kings 22:17

Because they have ['azab](../../strongs/h/h5800.md) me, and have [qāṭar](../../strongs/h/h6999.md) unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), that they might [kāʿas](../../strongs/h/h3707.md) me with all the [ma'aseh](../../strongs/h/h4639.md) of their [yad](../../strongs/h/h3027.md); therefore my [chemah](../../strongs/h/h2534.md) shall be [yāṣaṯ](../../strongs/h/h3341.md) against this [maqowm](../../strongs/h/h4725.md), and shall not be [kāḇâ](../../strongs/h/h3518.md).

<a name="2kings_22_18"></a>2Kings 22:18

But to the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) which [shalach](../../strongs/h/h7971.md) you to [darash](../../strongs/h/h1875.md) of [Yĕhovah](../../strongs/h/h3068.md), thus shall ye ['āmar](../../strongs/h/h559.md) to him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), As the [dabar](../../strongs/h/h1697.md) which thou hast [shama'](../../strongs/h/h8085.md);

<a name="2kings_22_19"></a>2Kings 22:19

Because thine [lebab](../../strongs/h/h3824.md) was [rāḵaḵ](../../strongs/h/h7401.md), and thou hast [kānaʿ](../../strongs/h/h3665.md) thyself [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), when thou [shama'](../../strongs/h/h8085.md) what I [dabar](../../strongs/h/h1696.md) against this [maqowm](../../strongs/h/h4725.md), and against the [yashab](../../strongs/h/h3427.md) thereof, that they should become a [šammâ](../../strongs/h/h8047.md) and a [qᵊlālâ](../../strongs/h/h7045.md), and hast [qāraʿ](../../strongs/h/h7167.md) thy [beḡeḏ](../../strongs/h/h899.md), and [bāḵâ](../../strongs/h/h1058.md) [paniym](../../strongs/h/h6440.md) me; I also have [shama'](../../strongs/h/h8085.md) thee, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_22_20"></a>2Kings 22:20

Behold therefore, I will ['āsap̄](../../strongs/h/h622.md) thee unto thy ['ab](../../strongs/h/h1.md), and thou shalt be ['āsap̄](../../strongs/h/h622.md) into thy [qeber](../../strongs/h/h6913.md) in [shalowm](../../strongs/h/h7965.md); and thine ['ayin](../../strongs/h/h5869.md) shall not [ra'ah](../../strongs/h/h7200.md) all the [ra'](../../strongs/h/h7451.md) which I will [bow'](../../strongs/h/h935.md) upon this [maqowm](../../strongs/h/h4725.md). And they [shuwb](../../strongs/h/h7725.md) the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) [shuwb](../../strongs/h/h7725.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 21](2kings_21.md) - [2Kings 23](2kings_23.md)