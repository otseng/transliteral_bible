# [2Kings 18](https://www.blueletterbible.org/kjv/2kings/18)

<a name="2kings_18_1"></a>2Kings 18:1

Now it came to pass in the third [šānâ](../../strongs/h/h8141.md) of [Hôšēaʿ](../../strongs/h/h1954.md) [ben](../../strongs/h/h1121.md) of ['Ēlâ](../../strongs/h/h425.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), that [Ḥizqîyâ](../../strongs/h/h2396.md) the [ben](../../strongs/h/h1121.md) of ['Āḥāz](../../strongs/h/h271.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) began to [mālaḵ](../../strongs/h/h4427.md).

<a name="2kings_18_2"></a>2Kings 18:2

Twenty and five [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) was he when he began to [mālaḵ](../../strongs/h/h4427.md); and he [mālaḵ](../../strongs/h/h4427.md) twenty and nine [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). His ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) also was ['Ăḇî](../../strongs/h/h21.md), the [bath](../../strongs/h/h1323.md) of [Zᵊḵaryâ](../../strongs/h/h2148.md).

<a name="2kings_18_3"></a>2Kings 18:3

And he ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), according to all that [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md) ['asah](../../strongs/h/h6213.md).

<a name="2kings_18_4"></a>2Kings 18:4

He [cuwr](../../strongs/h/h5493.md) the [bāmâ](../../strongs/h/h1116.md), and [shabar](../../strongs/h/h7665.md) the [maṣṣēḇâ](../../strongs/h/h4676.md), and [karath](../../strongs/h/h3772.md) the ['ăšērâ](../../strongs/h/h842.md), and [kāṯaṯ](../../strongs/h/h3807.md) the [nᵊḥšeṯ](../../strongs/h/h5178.md) [nachash](../../strongs/h/h5175.md) that [Mōshe](../../strongs/h/h4872.md) had ['asah](../../strongs/h/h6213.md): for unto those [yowm](../../strongs/h/h3117.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) did [qāṭar](../../strongs/h/h6999.md) to it: and he [qara'](../../strongs/h/h7121.md) it [nᵊḥuštān](../../strongs/h/h5180.md).

<a name="2kings_18_5"></a>2Kings 18:5

He [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); so that ['aḥar](../../strongs/h/h310.md) him was none like him among all the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), nor any that were [paniym](../../strongs/h/h6440.md) him.

<a name="2kings_18_6"></a>2Kings 18:6

For he [dāḇaq](../../strongs/h/h1692.md) to [Yĕhovah](../../strongs/h/h3068.md), and [cuwr](../../strongs/h/h5493.md) not from ['aḥar](../../strongs/h/h310.md) him, but [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md), which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="2kings_18_7"></a>2Kings 18:7

And [Yĕhovah](../../strongs/h/h3068.md) was with him; and he [sakal](../../strongs/h/h7919.md) whithersoever he [yāṣā'](../../strongs/h/h3318.md): and he [māraḏ](../../strongs/h/h4775.md) against the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), and ['abad](../../strongs/h/h5647.md) him not.

<a name="2kings_18_8"></a>2Kings 18:8

He [nakah](../../strongs/h/h5221.md) the [Pᵊlištî](../../strongs/h/h6430.md), even unto [ʿAzzâ](../../strongs/h/h5804.md), and the [gᵊḇûl](../../strongs/h/h1366.md) thereof, from the [miḡdāl](../../strongs/h/h4026.md) of the [nāṣar](../../strongs/h/h5341.md) to the [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md).

<a name="2kings_18_9"></a>2Kings 18:9

And it came to pass in the fourth [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [Ḥizqîyâ](../../strongs/h/h2396.md), which was the seventh [šānâ](../../strongs/h/h8141.md) of [Hôšēaʿ](../../strongs/h/h1954.md) [ben](../../strongs/h/h1121.md) of ['Ēlâ](../../strongs/h/h425.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), that [Šalman'Eser](../../strongs/h/h8022.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [ʿālâ](../../strongs/h/h5927.md) against [Šōmrôn](../../strongs/h/h8111.md), and [ṣûr](../../strongs/h/h6696.md) it.

<a name="2kings_18_10"></a>2Kings 18:10

And at the [qāṣê](../../strongs/h/h7097.md) of three [šānâ](../../strongs/h/h8141.md) they [lāḵaḏ](../../strongs/h/h3920.md) it: even in the sixth [šānâ](../../strongs/h/h8141.md) of [Ḥizqîyâ](../../strongs/h/h2396.md), that is the ninth [šānâ](../../strongs/h/h8141.md) of [Hôšēaʿ](../../strongs/h/h1954.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), [Šōmrôn](../../strongs/h/h8111.md) was [lāḵaḏ](../../strongs/h/h3920.md).

<a name="2kings_18_11"></a>2Kings 18:11

And the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) did [gālâ](../../strongs/h/h1540.md) [Yisra'el](../../strongs/h/h3478.md) unto ['Aššûr](../../strongs/h/h804.md), and [nachah](../../strongs/h/h5148.md) them in [Ḥălaḥ](../../strongs/h/h2477.md) and in [Ḥāḇôr](../../strongs/h/h2249.md) by the [nāhār](../../strongs/h/h5104.md) of [Gôzān](../../strongs/h/h1470.md), and in the [ʿîr](../../strongs/h/h5892.md) of the [Māḏay](../../strongs/h/h4074.md):

<a name="2kings_18_12"></a>2Kings 18:12

Because they [shama'](../../strongs/h/h8085.md) not the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), but ['abar](../../strongs/h/h5674.md) his [bĕriyth](../../strongs/h/h1285.md), and all that [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md), and would not [shama'](../../strongs/h/h8085.md) them, nor ['asah](../../strongs/h/h6213.md) them.

<a name="2kings_18_13"></a>2Kings 18:13

Now in the fourteenth [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [Ḥizqîyâ](../../strongs/h/h2396.md) did [Sanḥērîḇ](../../strongs/h/h5576.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [ʿālâ](../../strongs/h/h5927.md) against all the [bāṣar](../../strongs/h/h1219.md) [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and [tāp̄aś](../../strongs/h/h8610.md) them.

<a name="2kings_18_14"></a>2Kings 18:14

And [Ḥizqîyâ](../../strongs/h/h2396.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [shalach](../../strongs/h/h7971.md) to the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) to [Lāḵîš](../../strongs/h/h3923.md), ['āmar](../../strongs/h/h559.md), I have [chata'](../../strongs/h/h2398.md); [shuwb](../../strongs/h/h7725.md) from me: that which thou [nathan](../../strongs/h/h5414.md) on me will I [nasa'](../../strongs/h/h5375.md). And the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [śûm](../../strongs/h/h7760.md) unto [Ḥizqîyâ](../../strongs/h/h2396.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) three hundred [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md) and thirty [kikār](../../strongs/h/h3603.md) of [zāhāḇ](../../strongs/h/h2091.md).

<a name="2kings_18_15"></a>2Kings 18:15

And [Ḥizqîyâ](../../strongs/h/h2396.md) [nathan](../../strongs/h/h5414.md) him all the [keceph](../../strongs/h/h3701.md) that was [māṣā'](../../strongs/h/h4672.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and in the ['ôṣār](../../strongs/h/h214.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md).

<a name="2kings_18_16"></a>2Kings 18:16

At that [ʿēṯ](../../strongs/h/h6256.md) did [Ḥizqîyâ](../../strongs/h/h2396.md) [qāṣaṣ](../../strongs/h/h7112.md) from the [deleṯ](../../strongs/h/h1817.md) of the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md), and from the ['ōmnâ](../../strongs/h/h547.md) which [Ḥizqîyâ](../../strongs/h/h2396.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) had [ṣāp̄â](../../strongs/h/h6823.md), and [nathan](../../strongs/h/h5414.md) it to the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md).

<a name="2kings_18_17"></a>2Kings 18:17

And the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [shalach](../../strongs/h/h7971.md) [tartān](../../strongs/h/h8661.md) and [Rḇ-Srys](../../strongs/h/h7249.md) and [Raḇ-šāqê](../../strongs/h/h7262.md) from [Lāḵîš](../../strongs/h/h3923.md) to [melek](../../strongs/h/h4428.md) [Ḥizqîyâ](../../strongs/h/h2396.md) with a [kāḇēḏ](../../strongs/h/h3515.md) [cheyl](../../strongs/h/h2426.md) against [Yĕruwshalaim](../../strongs/h/h3389.md). And they [ʿālâ](../../strongs/h/h5927.md) and [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md). And when they were [ʿālâ](../../strongs/h/h5927.md), they [bow'](../../strongs/h/h935.md) and ['amad](../../strongs/h/h5975.md) by the [tᵊʿālâ](../../strongs/h/h8585.md) of the ['elyown](../../strongs/h/h5945.md) [bᵊrēḵâ](../../strongs/h/h1295.md), which is in the [mĕcillah](../../strongs/h/h4546.md) of the [kāḇas](../../strongs/h/h3526.md) [sadeh](../../strongs/h/h7704.md).

<a name="2kings_18_18"></a>2Kings 18:18

And when they had [qara'](../../strongs/h/h7121.md) to the [melek](../../strongs/h/h4428.md), there [yāṣā'](../../strongs/h/h3318.md) to them ['Elyāqîm](../../strongs/h/h471.md) the [ben](../../strongs/h/h1121.md) of [Ḥilqîyâ](../../strongs/h/h2518.md), which was over the [bayith](../../strongs/h/h1004.md), and [šeḇnā'](../../strongs/h/h7644.md) the [sāp̄ar](../../strongs/h/h5608.md), and [Yô'āḥ](../../strongs/h/h3098.md) the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md) the [zakar](../../strongs/h/h2142.md).

<a name="2kings_18_19"></a>2Kings 18:19

And [Raḇ-šāqê](../../strongs/h/h7262.md) ['āmar](../../strongs/h/h559.md) unto them, ['āmar](../../strongs/h/h559.md) ye now to [Ḥizqîyâ](../../strongs/h/h2396.md), Thus ['āmar](../../strongs/h/h559.md) the [gadowl](../../strongs/h/h1419.md) [melek](../../strongs/h/h4428.md), the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), What [biṭṭāḥôn](../../strongs/h/h986.md) is this wherein thou [batach](../../strongs/h/h982.md)?

<a name="2kings_18_20"></a>2Kings 18:20

Thou ['āmar](../../strongs/h/h559.md), (but they are but [saphah](../../strongs/h/h8193.md) [dabar](../../strongs/h/h1697.md),) I have ['etsah](../../strongs/h/h6098.md) and [gᵊḇûrâ](../../strongs/h/h1369.md) for the [milḥāmâ](../../strongs/h/h4421.md). Now on whom dost thou [batach](../../strongs/h/h982.md), that thou [māraḏ](../../strongs/h/h4775.md) against me?

<a name="2kings_18_21"></a>2Kings 18:21

Now, behold, thou [batach](../../strongs/h/h982.md) upon the [mašʿēnâ](../../strongs/h/h4938.md) of this [rāṣaṣ](../../strongs/h/h7533.md) [qānê](../../strongs/h/h7070.md), even upon [Mitsrayim](../../strongs/h/h4714.md), on which if an ['iysh](../../strongs/h/h376.md) [camak](../../strongs/h/h5564.md), it will [bow'](../../strongs/h/h935.md) into his [kaph](../../strongs/h/h3709.md), and [nāqaḇ](../../strongs/h/h5344.md) it: so is [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) unto all that [batach](../../strongs/h/h982.md) on him.

<a name="2kings_18_22"></a>2Kings 18:22

But if ye ['āmar](../../strongs/h/h559.md) unto me, We [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md): is not that he, whose [bāmâ](../../strongs/h/h1116.md) and whose [mizbeach](../../strongs/h/h4196.md) [Ḥizqîyâ](../../strongs/h/h2396.md) hath [cuwr](../../strongs/h/h5493.md), and hath ['āmar](../../strongs/h/h559.md) to [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md), Ye shall [shachah](../../strongs/h/h7812.md) [paniym](../../strongs/h/h6440.md) this [mizbeach](../../strongs/h/h4196.md) in [Yĕruwshalaim](../../strongs/h/h3389.md)?

<a name="2kings_18_23"></a>2Kings 18:23

Now therefore, I pray thee, [ʿāraḇ](../../strongs/h/h6148.md) to my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), and I will [nathan](../../strongs/h/h5414.md) thee two thousand [sûs](../../strongs/h/h5483.md), if thou be [yakol](../../strongs/h/h3201.md) on thy part to [nathan](../../strongs/h/h5414.md) [rāḵaḇ](../../strongs/h/h7392.md) upon them.

<a name="2kings_18_24"></a>2Kings 18:24

How then wilt thou [shuwb](../../strongs/h/h7725.md) the [paniym](../../strongs/h/h6440.md) of one [peḥâ](../../strongs/h/h6346.md) of the [qāṭān](../../strongs/h/h6996.md) of my ['adown](../../strongs/h/h113.md) ['ebed](../../strongs/h/h5650.md), and put thy [batach](../../strongs/h/h982.md) on [Mitsrayim](../../strongs/h/h4714.md) for [reḵeḇ](../../strongs/h/h7393.md) and for [pārāš](../../strongs/h/h6571.md)?

<a name="2kings_18_25"></a>2Kings 18:25

Am I now [ʿālâ](../../strongs/h/h5927.md) [bilʿăḏê](../../strongs/h/h1107.md) [Yĕhovah](../../strongs/h/h3068.md) against this [maqowm](../../strongs/h/h4725.md) to [shachath](../../strongs/h/h7843.md) it? [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) to me, [ʿālâ](../../strongs/h/h5927.md) against this ['erets](../../strongs/h/h776.md), and [shachath](../../strongs/h/h7843.md) it.

<a name="2kings_18_26"></a>2Kings 18:26

Then ['āmar](../../strongs/h/h559.md) ['Elyāqîm](../../strongs/h/h471.md) the [ben](../../strongs/h/h1121.md) of [Ḥilqîyâ](../../strongs/h/h2518.md), and [šeḇnā'](../../strongs/h/h7644.md), and [Yô'āḥ](../../strongs/h/h3098.md), unto [Raḇ-šāqê](../../strongs/h/h7262.md), [dabar](../../strongs/h/h1696.md), I pray thee, to thy ['ebed](../../strongs/h/h5650.md) in the ['ărāmy](../../strongs/h/h762.md); for we [shama'](../../strongs/h/h8085.md) it: and [dabar](../../strongs/h/h1696.md) not with us in the [yᵊhûḏîṯ](../../strongs/h/h3066.md) in the ['ozen](../../strongs/h/h241.md) of the ['am](../../strongs/h/h5971.md) that are on the [ḥômâ](../../strongs/h/h2346.md).

<a name="2kings_18_27"></a>2Kings 18:27

But [Raḇ-šāqê](../../strongs/h/h7262.md) ['āmar](../../strongs/h/h559.md) unto them, Hath my ['adown](../../strongs/h/h113.md) [shalach](../../strongs/h/h7971.md) me to thy ['adown](../../strongs/h/h113.md), and to thee, to [dabar](../../strongs/h/h1696.md) these [dabar](../../strongs/h/h1697.md)? hath he not sent me to the ['enowsh](../../strongs/h/h582.md) which [yashab](../../strongs/h/h3427.md) on the [ḥômâ](../../strongs/h/h2346.md), that they may ['akal](../../strongs/h/h398.md) their own [ṣ'h](../../strongs/h/h6675.md) [ḥărê yônîm](../../strongs/h/h2755.md), and [šāṯâ](../../strongs/h/h8354.md) their own [mayim](../../strongs/h/h4325.md) [regel](../../strongs/h/h7272.md) [šayin](../../strongs/h/h7890.md) with you?

<a name="2kings_18_28"></a>2Kings 18:28

Then [Raḇ-šāqê](../../strongs/h/h7262.md) ['amad](../../strongs/h/h5975.md) and [qara'](../../strongs/h/h7121.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md) in the [yᵊhûḏîṯ](../../strongs/h/h3066.md), and [dabar](../../strongs/h/h1696.md), ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of the [gadowl](../../strongs/h/h1419.md) [melek](../../strongs/h/h4428.md), the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md):

<a name="2kings_18_29"></a>2Kings 18:29

Thus ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md), Let not [Ḥizqîyâ](../../strongs/h/h2396.md) [nasha'](../../strongs/h/h5377.md) you: for he shall not be [yakol](../../strongs/h/h3201.md) to [natsal](../../strongs/h/h5337.md) you out of his [yad](../../strongs/h/h3027.md):

<a name="2kings_18_30"></a>2Kings 18:30

Neither let [Ḥizqîyâ](../../strongs/h/h2396.md) make you [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) will [natsal](../../strongs/h/h5337.md) [natsal](../../strongs/h/h5337.md) us, and this [ʿîr](../../strongs/h/h5892.md) shall not be [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md).

<a name="2kings_18_31"></a>2Kings 18:31

[shama'](../../strongs/h/h8085.md) not to [Ḥizqîyâ](../../strongs/h/h2396.md): for thus ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), ['asah](../../strongs/h/h6213.md) an agreement with me by a [bĕrakah](../../strongs/h/h1293.md), and [yāṣā'](../../strongs/h/h3318.md) to me, and then ['akal](../../strongs/h/h398.md) ye every ['iysh](../../strongs/h/h376.md) of his own [gep̄en](../../strongs/h/h1612.md), and every ['iysh](../../strongs/h/h376.md) of his [tĕ'en](../../strongs/h/h8384.md), and [šāṯâ](../../strongs/h/h8354.md) ye every ['iysh](../../strongs/h/h376.md) the [mayim](../../strongs/h/h4325.md) of his [bowr](../../strongs/h/h953.md):

<a name="2kings_18_32"></a>2Kings 18:32

Until I [bow'](../../strongs/h/h935.md) and [laqach](../../strongs/h/h3947.md) you to an ['erets](../../strongs/h/h776.md) like your own ['erets](../../strongs/h/h776.md), an ['erets](../../strongs/h/h776.md) of [dagan](../../strongs/h/h1715.md) and [tiyrowsh](../../strongs/h/h8492.md), an ['erets](../../strongs/h/h776.md) of [lechem](../../strongs/h/h3899.md) and [kerem](../../strongs/h/h3754.md), an ['erets](../../strongs/h/h776.md) of [yiṣhār](../../strongs/h/h3323.md) [zayiṯ](../../strongs/h/h2132.md) and of [dĕbash](../../strongs/h/h1706.md), that ye may [ḥāyâ](../../strongs/h/h2421.md), and not [muwth](../../strongs/h/h4191.md): and [shama'](../../strongs/h/h8085.md) not unto [Ḥizqîyâ](../../strongs/h/h2396.md), when he [sûṯ](../../strongs/h/h5496.md) you, ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) will [natsal](../../strongs/h/h5337.md) us.

<a name="2kings_18_33"></a>2Kings 18:33

Hath ['iysh](../../strongs/h/h376.md) of the ['Elohiym](../../strongs/h/h430.md) of the [gowy](../../strongs/h/h1471.md) [natsal](../../strongs/h/h5337.md) at [natsal](../../strongs/h/h5337.md) his ['erets](../../strongs/h/h776.md) out of the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md)?

<a name="2kings_18_34"></a>2Kings 18:34

Where are the ['Elohiym](../../strongs/h/h430.md) of [Ḥămāṯ](../../strongs/h/h2574.md), and of ['Arpāḏ](../../strongs/h/h774.md)? where are the ['Elohiym](../../strongs/h/h430.md) of [Sᵊp̄arvayim](../../strongs/h/h5617.md), [Hēnaʿ](../../strongs/h/h2012.md), and [ʿIûâ](../../strongs/h/h5755.md)? have they [natsal](../../strongs/h/h5337.md) [Šōmrôn](../../strongs/h/h8111.md) out of mine [yad](../../strongs/h/h3027.md)?

<a name="2kings_18_35"></a>2Kings 18:35

Who are they among all the ['Elohiym](../../strongs/h/h430.md) of the ['erets](../../strongs/h/h776.md), that have [natsal](../../strongs/h/h5337.md) their ['erets](../../strongs/h/h776.md) out of mine [yad](../../strongs/h/h3027.md), that [Yĕhovah](../../strongs/h/h3068.md) should [natsal](../../strongs/h/h5337.md) [Yĕruwshalaim](../../strongs/h/h3389.md) out of mine [yad](../../strongs/h/h3027.md)?

<a name="2kings_18_36"></a>2Kings 18:36

But the ['am](../../strongs/h/h5971.md) [ḥāraš](../../strongs/h/h2790.md), and ['anah](../../strongs/h/h6030.md) him not a [dabar](../../strongs/h/h1697.md): for the [melek](../../strongs/h/h4428.md) [mitsvah](../../strongs/h/h4687.md) was, ['āmar](../../strongs/h/h559.md), ['anah](../../strongs/h/h6030.md) him not.

<a name="2kings_18_37"></a>2Kings 18:37

Then [bow'](../../strongs/h/h935.md) ['Elyāqîm](../../strongs/h/h471.md) the [ben](../../strongs/h/h1121.md) of [Ḥilqîyâ](../../strongs/h/h2518.md), which was over the [bayith](../../strongs/h/h1004.md), and [šeḇnā'](../../strongs/h/h7644.md) the [sāp̄ar](../../strongs/h/h5608.md), and [Yô'āḥ](../../strongs/h/h3098.md) the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md) the [zakar](../../strongs/h/h2142.md), to [Ḥizqîyâ](../../strongs/h/h2396.md) with their [beḡeḏ](../../strongs/h/h899.md) [qāraʿ](../../strongs/h/h7167.md), and [nāḡaḏ](../../strongs/h/h5046.md) him the [dabar](../../strongs/h/h1697.md) of [Raḇ-šāqê](../../strongs/h/h7262.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 17](2kings_17.md) - [2Kings 19](2kings_19.md)