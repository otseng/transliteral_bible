# [2Kings 21](https://www.blueletterbible.org/kjv/2kings/21)

<a name="2kings_21_1"></a>2Kings 21:1

[Mᵊnaššê](../../strongs/h/h4519.md) was twelve [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and [mālaḵ](../../strongs/h/h4427.md) fifty and five [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [ḥep̄ṣî-ḇâ](../../strongs/h/h2657.md).

<a name="2kings_21_2"></a>2Kings 21:2

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), after the [tôʿēḇâ](../../strongs/h/h8441.md) of the [gowy](../../strongs/h/h1471.md), whom [Yĕhovah](../../strongs/h/h3068.md) [yarash](../../strongs/h/h3423.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_21_3"></a>2Kings 21:3

For he [bānâ](../../strongs/h/h1129.md) [shuwb](../../strongs/h/h7725.md) the [bāmâ](../../strongs/h/h1116.md) which [Ḥizqîyâ](../../strongs/h/h2396.md) his ['ab](../../strongs/h/h1.md) had ['abad](../../strongs/h/h6.md); and he [quwm](../../strongs/h/h6965.md) [mizbeach](../../strongs/h/h4196.md) for [BaʿAl](../../strongs/h/h1168.md), and ['asah](../../strongs/h/h6213.md) a ['ăšērâ](../../strongs/h/h842.md), as ['asah](../../strongs/h/h6213.md) ['Aḥ'Āḇ](../../strongs/h/h256.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md); and [shachah](../../strongs/h/h7812.md) all the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md), and ['abad](../../strongs/h/h5647.md) them.

<a name="2kings_21_4"></a>2Kings 21:4

And he [bānâ](../../strongs/h/h1129.md) [mizbeach](../../strongs/h/h4196.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), of which [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), In [Yĕruwshalaim](../../strongs/h/h3389.md) will I [śûm](../../strongs/h/h7760.md) my [shem](../../strongs/h/h8034.md).

<a name="2kings_21_5"></a>2Kings 21:5

And he [bānâ](../../strongs/h/h1129.md) [mizbeach](../../strongs/h/h4196.md) for all the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md) in the two [ḥāṣēr](../../strongs/h/h2691.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_21_6"></a>2Kings 21:6

And he made his [ben](../../strongs/h/h1121.md) ['abar](../../strongs/h/h5674.md) through the ['esh](../../strongs/h/h784.md), and [ʿānan](../../strongs/h/h6049.md), and used [nāḥaš](../../strongs/h/h5172.md), and ['asah](../../strongs/h/h6213.md) with ['ôḇ](../../strongs/h/h178.md) and [yiḏʿōnî](../../strongs/h/h3049.md): he ['asah](../../strongs/h/h6213.md) [rabah](../../strongs/h/h7235.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), to [kāʿas](../../strongs/h/h3707.md) him.

<a name="2kings_21_7"></a>2Kings 21:7

And he [śûm](../../strongs/h/h7760.md) a [pecel](../../strongs/h/h6459.md) of the ['ăšērâ](../../strongs/h/h842.md) that he had ['asah](../../strongs/h/h6213.md) in the [bayith](../../strongs/h/h1004.md), of which [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), and to [Šᵊlōmô](../../strongs/h/h8010.md) his [ben](../../strongs/h/h1121.md), In this [bayith](../../strongs/h/h1004.md), and in [Yĕruwshalaim](../../strongs/h/h3389.md), which I have [bāḥar](../../strongs/h/h977.md) out of all [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), will I [śûm](../../strongs/h/h7760.md) my [shem](../../strongs/h/h8034.md) ['owlam](../../strongs/h/h5769.md):

<a name="2kings_21_8"></a>2Kings 21:8

Neither will I make the [regel](../../strongs/h/h7272.md) of [Yisra'el](../../strongs/h/h3478.md) [nuwd](../../strongs/h/h5110.md) any more out of the ['ăḏāmâ](../../strongs/h/h127.md) which I [nathan](../../strongs/h/h5414.md) their ['ab](../../strongs/h/h1.md); only if they will [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) according to all that I have [tsavah](../../strongs/h/h6680.md) them, and according to all the [towrah](../../strongs/h/h8451.md) that my ['ebed](../../strongs/h/h5650.md) [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md) them.

<a name="2kings_21_9"></a>2Kings 21:9

But they [shama'](../../strongs/h/h8085.md) not: and [Mᵊnaššê](../../strongs/h/h4519.md) [tāʿâ](../../strongs/h/h8582.md) them to ['asah](../../strongs/h/h6213.md) more [ra'](../../strongs/h/h7451.md) than did the [gowy](../../strongs/h/h1471.md) whom [Yĕhovah](../../strongs/h/h3068.md) [šāmaḏ](../../strongs/h/h8045.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_21_10"></a>2Kings 21:10

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) [yad](../../strongs/h/h3027.md) his ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md), ['āmar](../../strongs/h/h559.md),

<a name="2kings_21_11"></a>2Kings 21:11

Because [Mᵊnaššê](../../strongs/h/h4519.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) hath ['asah](../../strongs/h/h6213.md) these [tôʿēḇâ](../../strongs/h/h8441.md), and hath done [ra'a'](../../strongs/h/h7489.md) above all that the ['Ĕmōrî](../../strongs/h/h567.md) ['asah](../../strongs/h/h6213.md), which were [paniym](../../strongs/h/h6440.md) him, and hath made [Yehuwdah](../../strongs/h/h3063.md) also to [chata'](../../strongs/h/h2398.md) with his [gillûl](../../strongs/h/h1544.md):

<a name="2kings_21_12"></a>2Kings 21:12

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), Behold, I am [bow'](../../strongs/h/h935.md) such [ra'](../../strongs/h/h7451.md) upon [Yĕruwshalaim](../../strongs/h/h3389.md) and [Yehuwdah](../../strongs/h/h3063.md), that whosoever [shama'](../../strongs/h/h8085.md) of it, both his ['ozen](../../strongs/h/h241.md) shall [ṣālal](../../strongs/h/h6750.md).

<a name="2kings_21_13"></a>2Kings 21:13

And I will [natah](../../strongs/h/h5186.md) over [Yĕruwshalaim](../../strongs/h/h3389.md) the [qāv](../../strongs/h/h6957.md) of [Šōmrôn](../../strongs/h/h8111.md), and the [mišqeleṯ](../../strongs/h/h4949.md) of the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md): and I will [māḥâ](../../strongs/h/h4229.md) [Yĕruwshalaim](../../strongs/h/h3389.md) as [māḥâ](../../strongs/h/h4229.md) a [ṣallaḥaṯ](../../strongs/h/h6747.md), [māḥâ](../../strongs/h/h4229.md) it, and [hāp̄aḵ](../../strongs/h/h2015.md) it [paniym](../../strongs/h/h6440.md).

<a name="2kings_21_14"></a>2Kings 21:14

And I will [nāṭaš](../../strongs/h/h5203.md) the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of mine [nachalah](../../strongs/h/h5159.md), and [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of their ['oyeb](../../strongs/h/h341.md); and they shall become a [baz](../../strongs/h/h957.md) and a [mᵊšissâ](../../strongs/h/h4933.md) to all their ['oyeb](../../strongs/h/h341.md);

<a name="2kings_21_15"></a>2Kings 21:15

Because they have ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in my ['ayin](../../strongs/h/h5869.md), and have [kāʿas](../../strongs/h/h3707.md) me, since the [yowm](../../strongs/h/h3117.md) their ['ab](../../strongs/h/h1.md) [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md), even unto this [yowm](../../strongs/h/h3117.md).

<a name="2kings_21_16"></a>2Kings 21:16

Moreover [Mᵊnaššê](../../strongs/h/h4519.md) [šāp̄aḵ](../../strongs/h/h8210.md) [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md) [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md), till he had [mālā'](../../strongs/h/h4390.md) [Yĕruwshalaim](../../strongs/h/h3389.md) from one [peh](../../strongs/h/h6310.md) to [peh](../../strongs/h/h6310.md); beside his [chatta'ath](../../strongs/h/h2403.md) wherewith he made [Yehuwdah](../../strongs/h/h3063.md) to [chata'](../../strongs/h/h2398.md), in ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_21_17"></a>2Kings 21:17

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Mᵊnaššê](../../strongs/h/h4519.md), and all that he ['asah](../../strongs/h/h6213.md), and his [chatta'ath](../../strongs/h/h2403.md) that he [chata'](../../strongs/h/h2398.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)?

<a name="2kings_21_18"></a>2Kings 21:18

And [Mᵊnaššê](../../strongs/h/h4519.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) in the [gan](../../strongs/h/h1588.md) of his own [bayith](../../strongs/h/h1004.md), in the [gan](../../strongs/h/h1588.md) of [ʿUzzā'](../../strongs/h/h5798.md): and ['Āmôn](../../strongs/h/h526.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2kings_21_19"></a>2Kings 21:19

['Āmôn](../../strongs/h/h526.md) was twenty and two [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) two [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [Mᵊšullemeṯ](../../strongs/h/h4922.md), the [bath](../../strongs/h/h1323.md) of [Ḥārûṣ](../../strongs/h/h2743.md) of [Yāṭḇâ](../../strongs/h/h3192.md).

<a name="2kings_21_20"></a>2Kings 21:20

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), as his ['ab](../../strongs/h/h1.md) [Mᵊnaššê](../../strongs/h/h4519.md) ['asah](../../strongs/h/h6213.md).

<a name="2kings_21_21"></a>2Kings 21:21

And he [yālaḵ](../../strongs/h/h3212.md) in all the [derek](../../strongs/h/h1870.md) that his ['ab](../../strongs/h/h1.md) [halak](../../strongs/h/h1980.md), and ['abad](../../strongs/h/h5647.md) the [gillûl](../../strongs/h/h1544.md) that his ['ab](../../strongs/h/h1.md) ['abad](../../strongs/h/h5647.md), and [shachah](../../strongs/h/h7812.md) them:

<a name="2kings_21_22"></a>2Kings 21:22

And he ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of his ['ab](../../strongs/h/h1.md), and [halak](../../strongs/h/h1980.md) not in the [derek](../../strongs/h/h1870.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_21_23"></a>2Kings 21:23

And the ['ebed](../../strongs/h/h5650.md) of ['Āmôn](../../strongs/h/h526.md) [qāšar](../../strongs/h/h7194.md) against him, and [muwth](../../strongs/h/h4191.md) the [melek](../../strongs/h/h4428.md) in his own [bayith](../../strongs/h/h1004.md).

<a name="2kings_21_24"></a>2Kings 21:24

And the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) [nakah](../../strongs/h/h5221.md) all them that had [qāšar](../../strongs/h/h7194.md) against [melek](../../strongs/h/h4428.md) ['Āmôn](../../strongs/h/h526.md); and the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) made [Yō'Šîyâ](../../strongs/h/h2977.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2kings_21_25"></a>2Kings 21:25

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of ['Āmôn](../../strongs/h/h526.md) which he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)?

<a name="2kings_21_26"></a>2Kings 21:26

And he was [qāḇar](../../strongs/h/h6912.md) in his [qᵊḇûrâ](../../strongs/h/h6900.md) in the [gan](../../strongs/h/h1588.md) of [ʿUzzā'](../../strongs/h/h5798.md): and [Yō'Šîyâ](../../strongs/h/h2977.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 20](2kings_20.md) - [2Kings 22](2kings_22.md)