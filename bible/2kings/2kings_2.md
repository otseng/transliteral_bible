# [2Kings 2](https://www.blueletterbible.org/kjv/2kings/2)

<a name="2kings_2_1"></a>2Kings 2:1

And it came to pass, when [Yĕhovah](../../strongs/h/h3068.md) would [ʿālâ](../../strongs/h/h5927.md) ['Ēlîyâ](../../strongs/h/h452.md) into [shamayim](../../strongs/h/h8064.md) by a [saʿar](../../strongs/h/h5591.md), that ['Ēlîyâ](../../strongs/h/h452.md) [yālaḵ](../../strongs/h/h3212.md) with ['Ĕlîšāʿ](../../strongs/h/h477.md) from [Gilgāl](../../strongs/h/h1537.md).

<a name="2kings_2_2"></a>2Kings 2:2

And ['Ēlîyâ](../../strongs/h/h452.md) ['āmar](../../strongs/h/h559.md) unto ['Ĕlîšāʿ](../../strongs/h/h477.md), [yashab](../../strongs/h/h3427.md) here, I pray thee; for [Yĕhovah](../../strongs/h/h3068.md) hath [shalach](../../strongs/h/h7971.md) me to [Bêṯ-'ēl](../../strongs/h/h1008.md). And ['Ĕlîšāʿ](../../strongs/h/h477.md) ['āmar](../../strongs/h/h559.md) unto him, As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), and as thy [nephesh](../../strongs/h/h5315.md) [chay](../../strongs/h/h2416.md), I will not ['azab](../../strongs/h/h5800.md) thee. So they [yarad](../../strongs/h/h3381.md) to [Bêṯ-'ēl](../../strongs/h/h1008.md).

<a name="2kings_2_3"></a>2Kings 2:3

And the [ben](../../strongs/h/h1121.md) of the [nāḇî'](../../strongs/h/h5030.md) that were at [Bêṯ-'ēl](../../strongs/h/h1008.md) [yāṣā'](../../strongs/h/h3318.md) to ['Ĕlîšāʿ](../../strongs/h/h477.md), and ['āmar](../../strongs/h/h559.md) unto him, [yada'](../../strongs/h/h3045.md) thou that [Yĕhovah](../../strongs/h/h3068.md) will [laqach](../../strongs/h/h3947.md) thy ['adown](../../strongs/h/h113.md) from thy [ro'sh](../../strongs/h/h7218.md) to [yowm](../../strongs/h/h3117.md)? And he ['āmar](../../strongs/h/h559.md), Yea, I [yada'](../../strongs/h/h3045.md) it; [ḥāšâ](../../strongs/h/h2814.md) ye.

<a name="2kings_2_4"></a>2Kings 2:4

And ['Ēlîyâ](../../strongs/h/h452.md) ['āmar](../../strongs/h/h559.md) unto him, ['Ĕlîšāʿ](../../strongs/h/h477.md), [yashab](../../strongs/h/h3427.md) here, I pray thee; for [Yĕhovah](../../strongs/h/h3068.md) hath [shalach](../../strongs/h/h7971.md) me to [Yᵊrēḥô](../../strongs/h/h3405.md). And he ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), and as thy [nephesh](../../strongs/h/h5315.md) [chay](../../strongs/h/h2416.md), I will not ['azab](../../strongs/h/h5800.md) thee. So they [bow'](../../strongs/h/h935.md) to [Yᵊrēḥô](../../strongs/h/h3405.md).

<a name="2kings_2_5"></a>2Kings 2:5

And the [ben](../../strongs/h/h1121.md) of the [nāḇî'](../../strongs/h/h5030.md) that were at [Yᵊrēḥô](../../strongs/h/h3405.md) [nāḡaš](../../strongs/h/h5066.md) to ['Ĕlîšāʿ](../../strongs/h/h477.md), and ['āmar](../../strongs/h/h559.md) unto him, [yada'](../../strongs/h/h3045.md) thou that [Yĕhovah](../../strongs/h/h3068.md) will [laqach](../../strongs/h/h3947.md) thy ['adown](../../strongs/h/h113.md) from thy [ro'sh](../../strongs/h/h7218.md) to [yowm](../../strongs/h/h3117.md)? And he ['āmar](../../strongs/h/h559.md), Yea, I [yada'](../../strongs/h/h3045.md) it; [ḥāšâ](../../strongs/h/h2814.md) ye.

<a name="2kings_2_6"></a>2Kings 2:6

And ['Ēlîyâ](../../strongs/h/h452.md) ['āmar](../../strongs/h/h559.md) unto him, [yashab](../../strongs/h/h3427.md), I pray thee, here; for [Yĕhovah](../../strongs/h/h3068.md) hath [shalach](../../strongs/h/h7971.md) me to [Yardēn](../../strongs/h/h3383.md). And he ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), and as thy [nephesh](../../strongs/h/h5315.md) [chay](../../strongs/h/h2416.md), I will not ['azab](../../strongs/h/h5800.md) thee. And they two [yālaḵ](../../strongs/h/h3212.md).

<a name="2kings_2_7"></a>2Kings 2:7

And fifty ['iysh](../../strongs/h/h376.md) of the [ben](../../strongs/h/h1121.md) of the [nāḇî'](../../strongs/h/h5030.md) [halak](../../strongs/h/h1980.md), and ['amad](../../strongs/h/h5975.md) to [neḡeḏ](../../strongs/h/h5048.md) afar [rachowq](../../strongs/h/h7350.md): and they two ['amad](../../strongs/h/h5975.md) by [Yardēn](../../strongs/h/h3383.md).

<a name="2kings_2_8"></a>2Kings 2:8

And ['Ēlîyâ](../../strongs/h/h452.md) [laqach](../../strongs/h/h3947.md) his ['adereṯ](../../strongs/h/h155.md), and [gālam](../../strongs/h/h1563.md) it, and [nakah](../../strongs/h/h5221.md) the [mayim](../../strongs/h/h4325.md), and they were [ḥāṣâ](../../strongs/h/h2673.md) hither and thither, so that they two ['abar](../../strongs/h/h5674.md) on [ḥārāḇâ](../../strongs/h/h2724.md).

<a name="2kings_2_9"></a>2Kings 2:9

And it came to pass, when they were ['abar](../../strongs/h/h5674.md), that ['Ēlîyâ](../../strongs/h/h452.md) ['āmar](../../strongs/h/h559.md) unto ['Ĕlîšāʿ](../../strongs/h/h477.md), [sha'al](../../strongs/h/h7592.md) what I shall ['asah](../../strongs/h/h6213.md) for thee, before I be [laqach](../../strongs/h/h3947.md) from thee. And ['Ĕlîšāʿ](../../strongs/h/h477.md) ['āmar](../../strongs/h/h559.md), I pray thee, let a double [peh](../../strongs/h/h6310.md) of thy [ruwach](../../strongs/h/h7307.md) be upon me.

<a name="2kings_2_10"></a>2Kings 2:10

And he ['āmar](../../strongs/h/h559.md), Thou hast [sha'al](../../strongs/h/h7592.md) a [qāšâ](../../strongs/h/h7185.md): nevertheless, if thou [ra'ah](../../strongs/h/h7200.md) me when I am [laqach](../../strongs/h/h3947.md) from thee, it shall be so unto thee; but if not, it shall not be so.

<a name="2kings_2_11"></a>2Kings 2:11

And it came to pass, as they [halak](../../strongs/h/h1980.md) [halak](../../strongs/h/h1980.md), and [dabar](../../strongs/h/h1696.md), that, behold, there appeared a [reḵeḇ](../../strongs/h/h7393.md) of ['esh](../../strongs/h/h784.md), and [sûs](../../strongs/h/h5483.md) of ['esh](../../strongs/h/h784.md), and [pāraḏ](../../strongs/h/h6504.md) them both [bayin](../../strongs/h/h996.md); and ['Ēlîyâ](../../strongs/h/h452.md) [ʿālâ](../../strongs/h/h5927.md) by a [saʿar](../../strongs/h/h5591.md) into [shamayim](../../strongs/h/h8064.md).

<a name="2kings_2_12"></a>2Kings 2:12

And ['Ĕlîšāʿ](../../strongs/h/h477.md) [ra'ah](../../strongs/h/h7200.md) it, and he [ṣāʿaq](../../strongs/h/h6817.md), My ['ab](../../strongs/h/h1.md), my ['ab](../../strongs/h/h1.md), the [reḵeḇ](../../strongs/h/h7393.md) of [Yisra'el](../../strongs/h/h3478.md), and the [pārāš](../../strongs/h/h6571.md) thereof. And he [ra'ah](../../strongs/h/h7200.md) him no more: and he [ḥāzaq](../../strongs/h/h2388.md) of his own [beḡeḏ](../../strongs/h/h899.md), and [qāraʿ](../../strongs/h/h7167.md) them in two [qᵊrāʿîm](../../strongs/h/h7168.md).

<a name="2kings_2_13"></a>2Kings 2:13

He [ruwm](../../strongs/h/h7311.md) also the ['adereṯ](../../strongs/h/h155.md) of ['Ēlîyâ](../../strongs/h/h452.md) that [naphal](../../strongs/h/h5307.md) from him, and went [shuwb](../../strongs/h/h7725.md), and ['amad](../../strongs/h/h5975.md) by the [saphah](../../strongs/h/h8193.md) of [Yardēn](../../strongs/h/h3383.md);

<a name="2kings_2_14"></a>2Kings 2:14

And he [laqach](../../strongs/h/h3947.md) the ['adereṯ](../../strongs/h/h155.md) of ['Ēlîyâ](../../strongs/h/h452.md) that [naphal](../../strongs/h/h5307.md) from him, and [nakah](../../strongs/h/h5221.md) the [mayim](../../strongs/h/h4325.md), and ['āmar](../../strongs/h/h559.md), Where is [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of ['Ēlîyâ](../../strongs/h/h452.md)? and when he also had [nakah](../../strongs/h/h5221.md) the [mayim](../../strongs/h/h4325.md), they [ḥāṣâ](../../strongs/h/h2673.md) hither and thither: and ['Ĕlîšāʿ](../../strongs/h/h477.md) ['abar](../../strongs/h/h5674.md).

<a name="2kings_2_15"></a>2Kings 2:15

And when the [ben](../../strongs/h/h1121.md) of the [nāḇî'](../../strongs/h/h5030.md) which were to view at [Yᵊrēḥô](../../strongs/h/h3405.md) [ra'ah](../../strongs/h/h7200.md) him, they ['āmar](../../strongs/h/h559.md), The [ruwach](../../strongs/h/h7307.md) of ['Ēlîyâ](../../strongs/h/h452.md) doth [nuwach](../../strongs/h/h5117.md) on ['Ĕlîšāʿ](../../strongs/h/h477.md). And they [bow'](../../strongs/h/h935.md) to [qārā'](../../strongs/h/h7125.md) him, and [shachah](../../strongs/h/h7812.md) themselves to the ['erets](../../strongs/h/h776.md) before him.

<a name="2kings_2_16"></a>2Kings 2:16

And they ['āmar](../../strongs/h/h559.md) unto him, Behold now, there be with thy ['ebed](../../strongs/h/h5650.md) fifty [ḥayil](../../strongs/h/h2428.md) [ben](../../strongs/h/h1121.md) ['enowsh](../../strongs/h/h582.md); let them [yālaḵ](../../strongs/h/h3212.md), we pray thee, and [bāqaš](../../strongs/h/h1245.md) thy ['adown](../../strongs/h/h113.md): lest peradventure the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) hath [nasa'](../../strongs/h/h5375.md) him, and [shalak](../../strongs/h/h7993.md) him upon some [har](../../strongs/h/h2022.md), or into some [gay'](../../strongs/h/h1516.md). And he ['āmar](../../strongs/h/h559.md), Ye shall not [shalach](../../strongs/h/h7971.md).

<a name="2kings_2_17"></a>2Kings 2:17

And when they [pāṣar](../../strongs/h/h6484.md) him till he was [buwsh](../../strongs/h/h954.md), he ['āmar](../../strongs/h/h559.md), [shalach](../../strongs/h/h7971.md). They [shalach](../../strongs/h/h7971.md) therefore fifty ['iysh](../../strongs/h/h376.md); and they [bāqaš](../../strongs/h/h1245.md) three [yowm](../../strongs/h/h3117.md), but [māṣā'](../../strongs/h/h4672.md) him not.

<a name="2kings_2_18"></a>2Kings 2:18

And when they [shuwb](../../strongs/h/h7725.md) to him, (for he [yashab](../../strongs/h/h3427.md) at [Yᵊrēḥô](../../strongs/h/h3405.md),) he ['āmar](../../strongs/h/h559.md) unto them, Did I not ['āmar](../../strongs/h/h559.md) unto you, [yālaḵ](../../strongs/h/h3212.md) not?

<a name="2kings_2_19"></a>2Kings 2:19

And the ['enowsh](../../strongs/h/h582.md) of the [ʿîr](../../strongs/h/h5892.md) ['āmar](../../strongs/h/h559.md) unto ['Ĕlîšāʿ](../../strongs/h/h477.md), Behold, I pray thee, the [môšāḇ](../../strongs/h/h4186.md) of this [ʿîr](../../strongs/h/h5892.md) is [towb](../../strongs/h/h2896.md), as my ['adown](../../strongs/h/h113.md) [ra'ah](../../strongs/h/h7200.md): but the [mayim](../../strongs/h/h4325.md) is [ra'](../../strongs/h/h7451.md), and the ['erets](../../strongs/h/h776.md) [šāḵōl](../../strongs/h/h7921.md).

<a name="2kings_2_20"></a>2Kings 2:20

And he ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) me a [ḥāḏāš](../../strongs/h/h2319.md) [ṣᵊlōḥîṯ](../../strongs/h/h6746.md), and [śûm](../../strongs/h/h7760.md) [melaḥ](../../strongs/h/h4417.md) therein. And they [laqach](../../strongs/h/h3947.md) it to him.

<a name="2kings_2_21"></a>2Kings 2:21

And he [yāṣā'](../../strongs/h/h3318.md) unto the [môṣā'](../../strongs/h/h4161.md) of the [mayim](../../strongs/h/h4325.md), and [shalak](../../strongs/h/h7993.md) the [melaḥ](../../strongs/h/h4417.md) in there, and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), I have [rapha'](../../strongs/h/h7495.md) these [mayim](../../strongs/h/h4325.md); there shall not be from thence any more [maveth](../../strongs/h/h4194.md) or [šāḵōl](../../strongs/h/h7921.md) land.

<a name="2kings_2_22"></a>2Kings 2:22

So the [mayim](../../strongs/h/h4325.md) were [rapha'](../../strongs/h/h7495.md) unto this [yowm](../../strongs/h/h3117.md), according to the [dabar](../../strongs/h/h1697.md) of ['Ĕlîšāʿ](../../strongs/h/h477.md) which he [dabar](../../strongs/h/h1696.md).

<a name="2kings_2_23"></a>2Kings 2:23

And he [ʿālâ](../../strongs/h/h5927.md) from thence unto [Bêṯ-'ēl](../../strongs/h/h1008.md): and as he was going [ʿālâ](../../strongs/h/h5927.md) by the [derek](../../strongs/h/h1870.md), there [yāṣā'](../../strongs/h/h3318.md) [qāṭān](../../strongs/h/h6996.md) [naʿar](../../strongs/h/h5288.md) out of the [ʿîr](../../strongs/h/h5892.md), and [qālas](../../strongs/h/h7046.md) him, and ['āmar](../../strongs/h/h559.md) unto him, [ʿālâ](../../strongs/h/h5927.md), thou [qērēaḥ](../../strongs/h/h7142.md); [ʿālâ](../../strongs/h/h5927.md), thou [qērēaḥ](../../strongs/h/h7142.md).

<a name="2kings_2_24"></a>2Kings 2:24

And he [panah](../../strongs/h/h6437.md) ['aḥar](../../strongs/h/h310.md), and [ra'ah](../../strongs/h/h7200.md) on them, and [qālal](../../strongs/h/h7043.md) them in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md). And there [yāṣā'](../../strongs/h/h3318.md) two [dōḇ](../../strongs/h/h1677.md) out of the [yaʿar](../../strongs/h/h3293.md), and [bāqaʿ](../../strongs/h/h1234.md) forty and two [yeleḏ](../../strongs/h/h3206.md) of them.

<a name="2kings_2_25"></a>2Kings 2:25

And he [yālaḵ](../../strongs/h/h3212.md) from thence to [har](../../strongs/h/h2022.md) [Karmel](../../strongs/h/h3760.md), and from thence he [shuwb](../../strongs/h/h7725.md) to [Šōmrôn](../../strongs/h/h8111.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 1](2kings_1.md) - [2Kings 3](2kings_3.md)