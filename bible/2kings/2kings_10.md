# [2Kings 10](https://www.blueletterbible.org/kjv/2kings/10)

<a name="2kings_10_1"></a>2Kings 10:1

And ['Aḥ'Āḇ](../../strongs/h/h256.md) had seventy [ben](../../strongs/h/h1121.md) in [Šōmrôn](../../strongs/h/h8111.md). And [Yêû'](../../strongs/h/h3058.md) [kāṯaḇ](../../strongs/h/h3789.md) [sēp̄er](../../strongs/h/h5612.md), and [shalach](../../strongs/h/h7971.md) to [Šōmrôn](../../strongs/h/h8111.md), unto the [śar](../../strongs/h/h8269.md) of [YizrᵊʿE'L](../../strongs/h/h3157.md), to the [zāqēn](../../strongs/h/h2205.md), and to them that ['aman](../../strongs/h/h539.md) ['Aḥ'Āḇ](../../strongs/h/h256.md), ['āmar](../../strongs/h/h559.md),

<a name="2kings_10_2"></a>2Kings 10:2

Now as soon as this [sēp̄er](../../strongs/h/h5612.md) [bow'](../../strongs/h/h935.md) to you, seeing your ['adown](../../strongs/h/h113.md) [ben](../../strongs/h/h1121.md) are with you, and there are with you [reḵeḇ](../../strongs/h/h7393.md) and [sûs](../../strongs/h/h5483.md), a [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md) also, and [nešeq](../../strongs/h/h5402.md);

<a name="2kings_10_3"></a>2Kings 10:3

Look even [ra'ah](../../strongs/h/h7200.md) the [towb](../../strongs/h/h2896.md) and [yashar](../../strongs/h/h3477.md) of your ['adown](../../strongs/h/h113.md) [ben](../../strongs/h/h1121.md), and [śûm](../../strongs/h/h7760.md) him on his ['ab](../../strongs/h/h1.md) [kicce'](../../strongs/h/h3678.md), and [lāḥam](../../strongs/h/h3898.md) for your ['adown](../../strongs/h/h113.md) [bayith](../../strongs/h/h1004.md).

<a name="2kings_10_4"></a>2Kings 10:4

But they were [me'od](../../strongs/h/h3966.md) [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md), and ['āmar](../../strongs/h/h559.md), Behold, two [melek](../../strongs/h/h4428.md) ['amad](../../strongs/h/h5975.md) not [paniym](../../strongs/h/h6440.md) him: how then shall we ['amad](../../strongs/h/h5975.md)?

<a name="2kings_10_5"></a>2Kings 10:5

And he that was over the [bayith](../../strongs/h/h1004.md), and he that was over the [ʿîr](../../strongs/h/h5892.md), the [zāqēn](../../strongs/h/h2205.md) also, and the ['aman](../../strongs/h/h539.md), [shalach](../../strongs/h/h7971.md) to [Yêû'](../../strongs/h/h3058.md), ['āmar](../../strongs/h/h559.md), We are thy ['ebed](../../strongs/h/h5650.md), and will ['asah](../../strongs/h/h6213.md) all that thou shalt ['āmar](../../strongs/h/h559.md) us; we will not make ['iysh](../../strongs/h/h376.md) [mālaḵ](../../strongs/h/h4427.md): ['asah](../../strongs/h/h6213.md) thou that which is [towb](../../strongs/h/h2896.md) in thine ['ayin](../../strongs/h/h5869.md).

<a name="2kings_10_6"></a>2Kings 10:6

Then he [kāṯaḇ](../../strongs/h/h3789.md) a [sēp̄er](../../strongs/h/h5612.md) the [šēnî](../../strongs/h/h8145.md) time to them, ['āmar](../../strongs/h/h559.md), If ye be mine, and if ye will [shama'](../../strongs/h/h8085.md) unto my [qowl](../../strongs/h/h6963.md), [laqach](../../strongs/h/h3947.md) ye the [ro'sh](../../strongs/h/h7218.md) of the ['enowsh](../../strongs/h/h582.md) your ['adown](../../strongs/h/h113.md) [ben](../../strongs/h/h1121.md), and [bow'](../../strongs/h/h935.md) to me to [YizrᵊʿE'L](../../strongs/h/h3157.md) by [māḥār](../../strongs/h/h4279.md) this [ʿēṯ](../../strongs/h/h6256.md). Now the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md), being seventy ['iysh](../../strongs/h/h376.md), were with the [gadowl](../../strongs/h/h1419.md) of the [ʿîr](../../strongs/h/h5892.md), which brought them [gāḏal](../../strongs/h/h1431.md).

<a name="2kings_10_7"></a>2Kings 10:7

And it came to pass, when the [sēp̄er](../../strongs/h/h5612.md) [bow'](../../strongs/h/h935.md) to them, that they [laqach](../../strongs/h/h3947.md) the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md), and [šāḥaṭ](../../strongs/h/h7819.md) seventy ['iysh](../../strongs/h/h376.md), and [śûm](../../strongs/h/h7760.md) their [ro'sh](../../strongs/h/h7218.md) in [dûḏ](../../strongs/h/h1731.md), and [shalach](../../strongs/h/h7971.md) him them to [YizrᵊʿE'L](../../strongs/h/h3157.md).

<a name="2kings_10_8"></a>2Kings 10:8

And there [bow'](../../strongs/h/h935.md) a [mal'ak](../../strongs/h/h4397.md), and [nāḡaḏ](../../strongs/h/h5046.md) him, ['āmar](../../strongs/h/h559.md), They have [bow'](../../strongs/h/h935.md) the [ro'sh](../../strongs/h/h7218.md) of the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md). And he ['āmar](../../strongs/h/h559.md), [śûm](../../strongs/h/h7760.md) ye them in two [ṣāḇar](../../strongs/h/h6652.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md) until the [boqer](../../strongs/h/h1242.md).

<a name="2kings_10_9"></a>2Kings 10:9

And it came to pass in the [boqer](../../strongs/h/h1242.md), that he [yāṣā'](../../strongs/h/h3318.md), and ['amad](../../strongs/h/h5975.md), and ['āmar](../../strongs/h/h559.md) to all the ['am](../../strongs/h/h5971.md), Ye be [tsaddiyq](../../strongs/h/h6662.md): behold, I [qāšar](../../strongs/h/h7194.md) against my ['adown](../../strongs/h/h113.md), and [harag](../../strongs/h/h2026.md) him: but who [nakah](../../strongs/h/h5221.md) all these?

<a name="2kings_10_10"></a>2Kings 10:10

[yada'](../../strongs/h/h3045.md) now that there shall [naphal](../../strongs/h/h5307.md) unto the ['erets](../../strongs/h/h776.md) nothing of the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) concerning the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md): for [Yĕhovah](../../strongs/h/h3068.md) hath ['asah](../../strongs/h/h6213.md) that which he [dabar](../../strongs/h/h1696.md) [yad](../../strongs/h/h3027.md) his ['ebed](../../strongs/h/h5650.md) ['Ēlîyâ](../../strongs/h/h452.md).

<a name="2kings_10_11"></a>2Kings 10:11

So [Yêû'](../../strongs/h/h3058.md) [nakah](../../strongs/h/h5221.md) all that [šā'ar](../../strongs/h/h7604.md) of the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) in [YizrᵊʿE'L](../../strongs/h/h3157.md), and all his [gadowl](../../strongs/h/h1419.md), and his [yada'](../../strongs/h/h3045.md), and his [kōhēn](../../strongs/h/h3548.md), until he [šā'ar](../../strongs/h/h7604.md) him none [śārîḏ](../../strongs/h/h8300.md).

<a name="2kings_10_12"></a>2Kings 10:12

And he [quwm](../../strongs/h/h6965.md) and [bow'](../../strongs/h/h935.md), and [yālaḵ](../../strongs/h/h3212.md) to [Šōmrôn](../../strongs/h/h8111.md). And as he was at the [ra'ah](../../strongs/h/h7462.md) [bayith](../../strongs/h/h1004.md) [Bêṯ-ʿĒqeḏ](../../strongs/h/h1044.md) in the [derek](../../strongs/h/h1870.md),

<a name="2kings_10_13"></a>2Kings 10:13

[Yêû'](../../strongs/h/h3058.md) [māṣā'](../../strongs/h/h4672.md) with the ['ach](../../strongs/h/h251.md) of ['Ăḥazyâ](../../strongs/h/h274.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and ['āmar](../../strongs/h/h559.md), Who are ye? And they ['āmar](../../strongs/h/h559.md), We are the ['ach](../../strongs/h/h251.md) of ['Ăḥazyâ](../../strongs/h/h274.md); and we [yarad](../../strongs/h/h3381.md) to [shalowm](../../strongs/h/h7965.md) the [ben](../../strongs/h/h1121.md) of the [melek](../../strongs/h/h4428.md) and the [ben](../../strongs/h/h1121.md) of the [ḡᵊḇîrâ](../../strongs/h/h1377.md).

<a name="2kings_10_14"></a>2Kings 10:14

And he ['āmar](../../strongs/h/h559.md), [tāp̄aś](../../strongs/h/h8610.md) them [chay](../../strongs/h/h2416.md). And they [tāp̄aś](../../strongs/h/h8610.md) them [chay](../../strongs/h/h2416.md), and [šāḥaṭ](../../strongs/h/h7819.md) them at the [bowr](../../strongs/h/h953.md) of the shearing [Bêṯ-ʿĒqeḏ](../../strongs/h/h1044.md), even two and forty ['iysh](../../strongs/h/h376.md); neither [šā'ar](../../strongs/h/h7604.md) he ['iysh](../../strongs/h/h376.md) of them.

<a name="2kings_10_15"></a>2Kings 10:15

And when he was [yālaḵ](../../strongs/h/h3212.md) thence, he [māṣā'](../../strongs/h/h4672.md) on [Yᵊhônāḏāḇ](../../strongs/h/h3082.md) the [ben](../../strongs/h/h1121.md) of [Rēḵāḇ](../../strongs/h/h7394.md) coming to [qārā'](../../strongs/h/h7125.md) him: and he [barak](../../strongs/h/h1288.md) him, and ['āmar](../../strongs/h/h559.md) to him, Is thine [lebab](../../strongs/h/h3824.md) [yashar](../../strongs/h/h3477.md), as my [lebab](../../strongs/h/h3824.md) is with thy [lebab](../../strongs/h/h3824.md)? And [Yᵊhônāḏāḇ](../../strongs/h/h3082.md) ['āmar](../../strongs/h/h559.md), It is. If it be, [nathan](../../strongs/h/h5414.md) me thine [yad](../../strongs/h/h3027.md). And he [nathan](../../strongs/h/h5414.md) him his [yad](../../strongs/h/h3027.md); and he [ʿālâ](../../strongs/h/h5927.md) him to him into the [merkāḇâ](../../strongs/h/h4818.md).

<a name="2kings_10_16"></a>2Kings 10:16

And he ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) with me, and [ra'ah](../../strongs/h/h7200.md) my [qin'â](../../strongs/h/h7068.md) for [Yĕhovah](../../strongs/h/h3068.md). So they made him [rāḵaḇ](../../strongs/h/h7392.md) in his [reḵeḇ](../../strongs/h/h7393.md).

<a name="2kings_10_17"></a>2Kings 10:17

And when he [bow'](../../strongs/h/h935.md) to [Šōmrôn](../../strongs/h/h8111.md), he [nakah](../../strongs/h/h5221.md) all that [šā'ar](../../strongs/h/h7604.md) unto ['Aḥ'Āḇ](../../strongs/h/h256.md) in [Šōmrôn](../../strongs/h/h8111.md), till he had [šāmaḏ](../../strongs/h/h8045.md) him, according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [dabar](../../strongs/h/h1696.md) to ['Ēlîyâ](../../strongs/h/h452.md).

<a name="2kings_10_18"></a>2Kings 10:18

And [Yêû'](../../strongs/h/h3058.md) [qāḇaṣ](../../strongs/h/h6908.md) all the ['am](../../strongs/h/h5971.md) [qāḇaṣ](../../strongs/h/h6908.md), and ['āmar](../../strongs/h/h559.md) unto them, ['Aḥ'Āḇ](../../strongs/h/h256.md) ['abad](../../strongs/h/h5647.md) [BaʿAl](../../strongs/h/h1168.md) a [mᵊʿaṭ](../../strongs/h/h4592.md); but [Yêû'](../../strongs/h/h3058.md) shall ['abad](../../strongs/h/h5647.md) him [rabah](../../strongs/h/h7235.md).

<a name="2kings_10_19"></a>2Kings 10:19

Now therefore [qara'](../../strongs/h/h7121.md) unto me all the [nāḇî'](../../strongs/h/h5030.md) of [BaʿAl](../../strongs/h/h1168.md), all his ['abad](../../strongs/h/h5647.md), and all his [kōhēn](../../strongs/h/h3548.md); let ['iysh](../../strongs/h/h376.md) be [paqad](../../strongs/h/h6485.md): for I have a [gadowl](../../strongs/h/h1419.md) [zebach](../../strongs/h/h2077.md) to do to [BaʿAl](../../strongs/h/h1168.md); whosoever shall be [paqad](../../strongs/h/h6485.md), he shall not [ḥāyâ](../../strongs/h/h2421.md). But [Yêû'](../../strongs/h/h3058.md) ['asah](../../strongs/h/h6213.md) it in [ʿāqḇâ](../../strongs/h/h6122.md), to the intent that he might ['abad](../../strongs/h/h6.md) the ['abad](../../strongs/h/h5647.md) of [BaʿAl](../../strongs/h/h1168.md).

<a name="2kings_10_20"></a>2Kings 10:20

And [Yêû'](../../strongs/h/h3058.md) ['āmar](../../strongs/h/h559.md), [qadash](../../strongs/h/h6942.md) a [ʿăṣārâ](../../strongs/h/h6116.md) for [BaʿAl](../../strongs/h/h1168.md). And they [qara'](../../strongs/h/h7121.md) it.

<a name="2kings_10_21"></a>2Kings 10:21

And [Yêû'](../../strongs/h/h3058.md) [shalach](../../strongs/h/h7971.md) through all [Yisra'el](../../strongs/h/h3478.md): and all the ['abad](../../strongs/h/h5647.md) of [BaʿAl](../../strongs/h/h1168.md) [bow'](../../strongs/h/h935.md), so that there was not an ['iysh](../../strongs/h/h376.md) [šā'ar](../../strongs/h/h7604.md) that [bow'](../../strongs/h/h935.md) not. And they [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [BaʿAl](../../strongs/h/h1168.md); and the [bayith](../../strongs/h/h1004.md) of [BaʿAl](../../strongs/h/h1168.md) was [mālā'](../../strongs/h/h4390.md) from one [peh](../../strongs/h/h6310.md) to [peh](../../strongs/h/h6310.md).

<a name="2kings_10_22"></a>2Kings 10:22

And he ['āmar](../../strongs/h/h559.md) unto him that was over the [meltāḥâ](../../strongs/h/h4458.md), [yāṣā'](../../strongs/h/h3318.md) [lᵊḇûš](../../strongs/h/h3830.md) for all the ['abad](../../strongs/h/h5647.md) of [BaʿAl](../../strongs/h/h1168.md). And he [yāṣā'](../../strongs/h/h3318.md) them [malbûš](../../strongs/h/h4403.md).

<a name="2kings_10_23"></a>2Kings 10:23

And [Yêû'](../../strongs/h/h3058.md) [bow'](../../strongs/h/h935.md), and [Yᵊhônāḏāḇ](../../strongs/h/h3082.md) the [ben](../../strongs/h/h1121.md) of [Rēḵāḇ](../../strongs/h/h7394.md), into the [bayith](../../strongs/h/h1004.md) of [BaʿAl](../../strongs/h/h1168.md), and ['āmar](../../strongs/h/h559.md) unto the ['abad](../../strongs/h/h5647.md) of [BaʿAl](../../strongs/h/h1168.md), [ḥāp̄aś](../../strongs/h/h2664.md), and [ra'ah](../../strongs/h/h7200.md) that there be here with you none of the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md), but the ['abad](../../strongs/h/h5647.md) of [BaʿAl](../../strongs/h/h1168.md) only.

<a name="2kings_10_24"></a>2Kings 10:24

And when they [bow'](../../strongs/h/h935.md) to ['asah](../../strongs/h/h6213.md) [zebach](../../strongs/h/h2077.md) and [ʿōlâ](../../strongs/h/h5930.md), [Yêû'](../../strongs/h/h3058.md) [śûm](../../strongs/h/h7760.md) fourscore ['iysh](../../strongs/h/h376.md) [ḥûṣ](../../strongs/h/h2351.md), and ['āmar](../../strongs/h/h559.md), If ['iysh](../../strongs/h/h376.md) of the ['enowsh](../../strongs/h/h582.md) whom I have [bow'](../../strongs/h/h935.md) into your [yad](../../strongs/h/h3027.md) [mālaṭ](../../strongs/h/h4422.md), he that letteth him go, his [nephesh](../../strongs/h/h5315.md) shall be for the [nephesh](../../strongs/h/h5315.md) of him.

<a name="2kings_10_25"></a>2Kings 10:25

And it came to pass, as soon as he had made a [kalah](../../strongs/h/h3615.md) of ['asah](../../strongs/h/h6213.md) the [ʿōlâ](../../strongs/h/h5930.md), that [Yêû'](../../strongs/h/h3058.md) ['āmar](../../strongs/h/h559.md) to the [rûṣ](../../strongs/h/h7323.md) and to the [šālîš](../../strongs/h/h7991.md), [bow'](../../strongs/h/h935.md), and [nakah](../../strongs/h/h5221.md) them; let ['iysh](../../strongs/h/h376.md) [yāṣā'](../../strongs/h/h3318.md). And they [nakah](../../strongs/h/h5221.md) them with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md); and the [rûṣ](../../strongs/h/h7323.md) and the [šālîš](../../strongs/h/h7991.md) [shalak](../../strongs/h/h7993.md) them, and [yālaḵ](../../strongs/h/h3212.md) to the [ʿîr](../../strongs/h/h5892.md) of the [bayith](../../strongs/h/h1004.md) of [BaʿAl](../../strongs/h/h1168.md).

<a name="2kings_10_26"></a>2Kings 10:26

And they [yāṣā'](../../strongs/h/h3318.md) the [maṣṣēḇâ](../../strongs/h/h4676.md) out of the [bayith](../../strongs/h/h1004.md) of [BaʿAl](../../strongs/h/h1168.md), and [śārap̄](../../strongs/h/h8313.md) them.

<a name="2kings_10_27"></a>2Kings 10:27

And they brake [nāṯaṣ](../../strongs/h/h5422.md) the [maṣṣēḇâ](../../strongs/h/h4676.md) of [BaʿAl](../../strongs/h/h1168.md), and brake [nāṯaṣ](../../strongs/h/h5422.md) the [bayith](../../strongs/h/h1004.md) of [BaʿAl](../../strongs/h/h1168.md), and [śûm](../../strongs/h/h7760.md) it a draught [môṣā'â](../../strongs/h/h4163.md) [maḥărā'â](../../strongs/h/h4280.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="2kings_10_28"></a>2Kings 10:28

Thus [Yêû'](../../strongs/h/h3058.md) [šāmaḏ](../../strongs/h/h8045.md) [BaʿAl](../../strongs/h/h1168.md) out of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_10_29"></a>2Kings 10:29

Howbeit from the [ḥēṭĕ'](../../strongs/h/h2399.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), who made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md), [Yêû'](../../strongs/h/h3058.md) [cuwr](../../strongs/h/h5493.md) not from ['aḥar](../../strongs/h/h310.md) them, to wit, the [zāhāḇ](../../strongs/h/h2091.md) [ʿēḡel](../../strongs/h/h5695.md) that were in [Bêṯ-'ēl](../../strongs/h/h1008.md), and that were in [Dān](../../strongs/h/h1835.md).

<a name="2kings_10_30"></a>2Kings 10:30

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Yêû'](../../strongs/h/h3058.md), Because thou hast done [ṭôḇ](../../strongs/h/h2895.md) in ['asah](../../strongs/h/h6213.md) that which is [yashar](../../strongs/h/h3477.md) in mine ['ayin](../../strongs/h/h5869.md), and hast ['asah](../../strongs/h/h6213.md) unto the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) according to all that was in mine [lebab](../../strongs/h/h3824.md), thy [ben](../../strongs/h/h1121.md) of the fourth generation shall [yashab](../../strongs/h/h3427.md) on the [kicce'](../../strongs/h/h3678.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_10_31"></a>2Kings 10:31

But [Yêû'](../../strongs/h/h3058.md) took no [shamar](../../strongs/h/h8104.md) to [yālaḵ](../../strongs/h/h3212.md) in the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) with all his [lebab](../../strongs/h/h3824.md): for he [cuwr](../../strongs/h/h5493.md) not from the [chatta'ath](../../strongs/h/h2403.md) of [YārāḇʿĀm](../../strongs/h/h3379.md), which made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md).

<a name="2kings_10_32"></a>2Kings 10:32

In those [yowm](../../strongs/h/h3117.md) [Yĕhovah](../../strongs/h/h3068.md) [ḥālal](../../strongs/h/h2490.md) to [qāṣâ](../../strongs/h/h7096.md) [Yisra'el](../../strongs/h/h3478.md) [qāṣâ](../../strongs/h/h7096.md): and [Ḥăzā'Ēl](../../strongs/h/h2371.md) [nakah](../../strongs/h/h5221.md) them in all the [gᵊḇûl](../../strongs/h/h1366.md) of [Yisra'el](../../strongs/h/h3478.md);

<a name="2kings_10_33"></a>2Kings 10:33

From [Yardēn](../../strongs/h/h3383.md) [mizrach](../../strongs/h/h4217.md) [šemeš](../../strongs/h/h8121.md), all the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md), the [Gāḏî](../../strongs/h/h1425.md), and the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md), and the [Mᵊnaššê](../../strongs/h/h4520.md), from [ʿĂrôʿēr](../../strongs/h/h6177.md), which is by the [nachal](../../strongs/h/h5158.md) ['arnôn](../../strongs/h/h769.md), even [Gilʿāḏ](../../strongs/h/h1568.md) and [Bāšān](../../strongs/h/h1316.md).

<a name="2kings_10_34"></a>2Kings 10:34

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yêû'](../../strongs/h/h3058.md), and all that he ['asah](../../strongs/h/h6213.md), and all his [gᵊḇûrâ](../../strongs/h/h1369.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="2kings_10_35"></a>2Kings 10:35

And [Yêû'](../../strongs/h/h3058.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md): and they [qāḇar](../../strongs/h/h6912.md) him in [Šōmrôn](../../strongs/h/h8111.md). And [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2kings_10_36"></a>2Kings 10:36

And the [yowm](../../strongs/h/h3117.md) that [Yêû'](../../strongs/h/h3058.md) [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) in [Šōmrôn](../../strongs/h/h8111.md) was twenty and eight [šānâ](../../strongs/h/h8141.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 9](2kings_9.md) - [2Kings 11](2kings_11.md)