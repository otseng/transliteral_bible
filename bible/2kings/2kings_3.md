# [2Kings 3](https://www.blueletterbible.org/kjv/2kings/3)

<a name="2kings_3_1"></a>2Kings 3:1

Now [Yᵊhôrām](../../strongs/h/h3088.md) the [ben](../../strongs/h/h1121.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) began to [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) in [Šōmrôn](../../strongs/h/h8111.md) the eighteenth [šānâ](../../strongs/h/h8141.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and [mālaḵ](../../strongs/h/h4427.md) twelve [šānâ](../../strongs/h/h8141.md).

<a name="2kings_3_2"></a>2Kings 3:2

And he ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md); but not like his ['ab](../../strongs/h/h1.md), and like his ['em](../../strongs/h/h517.md): for he put [cuwr](../../strongs/h/h5493.md) the [maṣṣēḇâ](../../strongs/h/h4676.md) of [BaʿAl](../../strongs/h/h1168.md) that his ['ab](../../strongs/h/h1.md) had ['asah](../../strongs/h/h6213.md).

<a name="2kings_3_3"></a>2Kings 3:3

Nevertheless he [dāḇaq](../../strongs/h/h1692.md) unto the [chatta'ath](../../strongs/h/h2403.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), which made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md); he [cuwr](../../strongs/h/h5493.md) not therefrom.

<a name="2kings_3_4"></a>2Kings 3:4

And [Mêšaʿ](../../strongs/h/h4338.md) [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md) was a [nōqēḏ](../../strongs/h/h5349.md), and [shuwb](../../strongs/h/h7725.md) unto the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) an hundred thousand [kar](../../strongs/h/h3733.md), and an hundred thousand ['ayil](../../strongs/h/h352.md), with the [ṣemer](../../strongs/h/h6785.md).

<a name="2kings_3_5"></a>2Kings 3:5

But it came to pass, when ['Aḥ'Āḇ](../../strongs/h/h256.md) was [maveth](../../strongs/h/h4194.md), that the [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md) [pāšaʿ](../../strongs/h/h6586.md) against the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_3_6"></a>2Kings 3:6

And [melek](../../strongs/h/h4428.md) [Yᵊhôrām](../../strongs/h/h3088.md) [yāṣā'](../../strongs/h/h3318.md) of [Šōmrôn](../../strongs/h/h8111.md) the same [yowm](../../strongs/h/h3117.md), and [paqad](../../strongs/h/h6485.md) all [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_3_7"></a>2Kings 3:7

And he [yālaḵ](../../strongs/h/h3212.md) and [shalach](../../strongs/h/h7971.md) to [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), ['āmar](../../strongs/h/h559.md), The [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md) hath [pāšaʿ](../../strongs/h/h6586.md) against me: wilt thou [yālaḵ](../../strongs/h/h3212.md) with me against [Mô'āḇ](../../strongs/h/h4124.md) to [milḥāmâ](../../strongs/h/h4421.md)? And he ['āmar](../../strongs/h/h559.md), I will [ʿālâ](../../strongs/h/h5927.md): I am as thou art, my ['am](../../strongs/h/h5971.md) as thy ['am](../../strongs/h/h5971.md), and my [sûs](../../strongs/h/h5483.md) as thy [sûs](../../strongs/h/h5483.md).

<a name="2kings_3_8"></a>2Kings 3:8

And he ['āmar](../../strongs/h/h559.md), Which [derek](../../strongs/h/h1870.md) shall we [ʿālâ](../../strongs/h/h5927.md)? And he ['āmar](../../strongs/h/h559.md), The [derek](../../strongs/h/h1870.md) through the [midbar](../../strongs/h/h4057.md) of ['Ĕḏōm](../../strongs/h/h123.md).

<a name="2kings_3_9"></a>2Kings 3:9

So the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [yālaḵ](../../strongs/h/h3212.md), and the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and the [melek](../../strongs/h/h4428.md) of ['Ĕḏōm](../../strongs/h/h123.md): and they fetched a [cabab](../../strongs/h/h5437.md) of seven [yowm](../../strongs/h/h3117.md) [derek](../../strongs/h/h1870.md): and there was no [mayim](../../strongs/h/h4325.md) for the [maḥănê](../../strongs/h/h4264.md), and for the [bĕhemah](../../strongs/h/h929.md) that [regel](../../strongs/h/h7272.md) them.

<a name="2kings_3_10"></a>2Kings 3:10

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md), ['ăhâ](../../strongs/h/h162.md)! that [Yĕhovah](../../strongs/h/h3068.md) hath [qara'](../../strongs/h/h7121.md) these three [melek](../../strongs/h/h4428.md) [qara'](../../strongs/h/h7121.md), to [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of [Mô'āḇ](../../strongs/h/h4124.md)!

<a name="2kings_3_11"></a>2Kings 3:11

But [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['āmar](../../strongs/h/h559.md), Is there not here a [nāḇî'](../../strongs/h/h5030.md) of [Yĕhovah](../../strongs/h/h3068.md), that we may [darash](../../strongs/h/h1875.md) of [Yĕhovah](../../strongs/h/h3068.md) by him? And one of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['ebed](../../strongs/h/h5650.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), Here is ['Ĕlîšāʿ](../../strongs/h/h477.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄āṭ](../../strongs/h/h8202.md), which [yāṣaq](../../strongs/h/h3332.md) [mayim](../../strongs/h/h4325.md) on the [yad](../../strongs/h/h3027.md) of ['Ēlîyâ](../../strongs/h/h452.md).

<a name="2kings_3_12"></a>2Kings 3:12

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['āmar](../../strongs/h/h559.md), The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) is with him. So the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) and [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) and the [melek](../../strongs/h/h4428.md) of ['Ĕḏōm](../../strongs/h/h123.md) [yarad](../../strongs/h/h3381.md) to him.

<a name="2kings_3_13"></a>2Kings 3:13

And ['Ĕlîšāʿ](../../strongs/h/h477.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), What have I to do with thee? [yālaḵ](../../strongs/h/h3212.md) thee to the [nāḇî'](../../strongs/h/h5030.md) of thy ['ab](../../strongs/h/h1.md), and to the [nāḇî'](../../strongs/h/h5030.md) of thy ['em](../../strongs/h/h517.md). And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto him, Nay: for [Yĕhovah](../../strongs/h/h3068.md) hath [qara'](../../strongs/h/h7121.md) these three [melek](../../strongs/h/h4428.md) [qara'](../../strongs/h/h7121.md), to [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of [Mô'āḇ](../../strongs/h/h4124.md).

<a name="2kings_3_14"></a>2Kings 3:14

And ['Ĕlîšāʿ](../../strongs/h/h477.md) ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) [chay](../../strongs/h/h2416.md), [paniym](../../strongs/h/h6440.md) whom I ['amad](../../strongs/h/h5975.md), surely, were it not that I [nasa'](../../strongs/h/h5375.md) the [paniym](../../strongs/h/h6440.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), I would not [nabat](../../strongs/h/h5027.md) toward thee, nor [ra'ah](../../strongs/h/h7200.md) thee.

<a name="2kings_3_15"></a>2Kings 3:15

But now [laqach](../../strongs/h/h3947.md) me a [nāḡan](../../strongs/h/h5059.md). And it came to pass, when the [nāḡan](../../strongs/h/h5059.md) [nāḡan](../../strongs/h/h5059.md), that the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) came upon him.

<a name="2kings_3_16"></a>2Kings 3:16

And he ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), ['asah](../../strongs/h/h6213.md) this [nachal](../../strongs/h/h5158.md) full of [gēḇ](../../strongs/h/h1356.md).

<a name="2kings_3_17"></a>2Kings 3:17

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Ye shall not [ra'ah](../../strongs/h/h7200.md) [ruwach](../../strongs/h/h7307.md), neither shall ye [ra'ah](../../strongs/h/h7200.md) [gešem](../../strongs/h/h1653.md); yet that [nachal](../../strongs/h/h5158.md) shall be [mālā'](../../strongs/h/h4390.md) with [mayim](../../strongs/h/h4325.md), that ye may [šāṯâ](../../strongs/h/h8354.md), both ye, and your [miqnê](../../strongs/h/h4735.md), and your [bĕhemah](../../strongs/h/h929.md).

<a name="2kings_3_18"></a>2Kings 3:18

And this is but a [qālal](../../strongs/h/h7043.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md): he will [nathan](../../strongs/h/h5414.md) the [Mô'āḇ](../../strongs/h/h4124.md) also into your [yad](../../strongs/h/h3027.md).

<a name="2kings_3_19"></a>2Kings 3:19

And ye shall [nakah](../../strongs/h/h5221.md) every [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md), and every [miḇḥôr](../../strongs/h/h4004.md) [ʿîr](../../strongs/h/h5892.md), and shall [naphal](../../strongs/h/h5307.md) every [towb](../../strongs/h/h2896.md) ['ets](../../strongs/h/h6086.md), and [sāṯam](../../strongs/h/h5640.md) all [maʿyān](../../strongs/h/h4599.md) of [mayim](../../strongs/h/h4325.md), and [kā'aḇ](../../strongs/h/h3510.md) every [towb](../../strongs/h/h2896.md) [ḥelqâ](../../strongs/h/h2513.md) of land with ['eben](../../strongs/h/h68.md).

<a name="2kings_3_20"></a>2Kings 3:20

And it came to pass in the [boqer](../../strongs/h/h1242.md), when the [minchah](../../strongs/h/h4503.md) was [ʿālâ](../../strongs/h/h5927.md), that, behold, there [bow'](../../strongs/h/h935.md) [mayim](../../strongs/h/h4325.md) by the [derek](../../strongs/h/h1870.md) of ['Ĕḏōm](../../strongs/h/h123.md), and the ['erets](../../strongs/h/h776.md) was [mālā'](../../strongs/h/h4390.md) with [mayim](../../strongs/h/h4325.md).

<a name="2kings_3_21"></a>2Kings 3:21

And when all the [Mô'āḇ](../../strongs/h/h4124.md) [shama'](../../strongs/h/h8085.md) that the [melek](../../strongs/h/h4428.md) were [ʿālâ](../../strongs/h/h5927.md) to [lāḥam](../../strongs/h/h3898.md) against them, they [ṣāʿaq](../../strongs/h/h6817.md) all that were able to [ḥāḡar](../../strongs/h/h2296.md) on [chagowr](../../strongs/h/h2290.md), and [maʿal](../../strongs/h/h4605.md), and ['amad](../../strongs/h/h5975.md) in the [gᵊḇûl](../../strongs/h/h1366.md).

<a name="2kings_3_22"></a>2Kings 3:22

And they [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and the [šemeš](../../strongs/h/h8121.md) [zāraḥ](../../strongs/h/h2224.md) upon the [mayim](../../strongs/h/h4325.md), and the [Mô'āḇ](../../strongs/h/h4124.md) [ra'ah](../../strongs/h/h7200.md) the [mayim](../../strongs/h/h4325.md) on the other [neḡeḏ](../../strongs/h/h5048.md) as ['āḏōm](../../strongs/h/h122.md) as [dam](../../strongs/h/h1818.md):

<a name="2kings_3_23"></a>2Kings 3:23

And they ['āmar](../../strongs/h/h559.md), This is [dam](../../strongs/h/h1818.md): the [melek](../../strongs/h/h4428.md) are [ḥāraḇ](../../strongs/h/h2717.md) [ḥāraḇ](../../strongs/h/h2717.md), and they have [nakah](../../strongs/h/h5221.md) ['iysh](../../strongs/h/h376.md) [rea'](../../strongs/h/h7453.md): now therefore, [Mô'āḇ](../../strongs/h/h4124.md), to the [šālāl](../../strongs/h/h7998.md).

<a name="2kings_3_24"></a>2Kings 3:24

And when they [bow'](../../strongs/h/h935.md) to the [maḥănê](../../strongs/h/h4264.md) of [Yisra'el](../../strongs/h/h3478.md), the [Yisra'el](../../strongs/h/h3478.md) [quwm](../../strongs/h/h6965.md) and [nakah](../../strongs/h/h5221.md) the [Mô'āḇ](../../strongs/h/h4124.md), so that they [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) them: but they went [nakah](../../strongs/h/h5221.md) [nakah](../../strongs/h/h5221.md) the [Mô'āḇ](../../strongs/h/h4124.md), even in their country.

<a name="2kings_3_25"></a>2Kings 3:25

And they [harac](../../strongs/h/h2040.md) the [ʿîr](../../strongs/h/h5892.md), and on every [towb](../../strongs/h/h2896.md) [ḥelqâ](../../strongs/h/h2513.md) [shalak](../../strongs/h/h7993.md) every ['iysh](../../strongs/h/h376.md) his ['eben](../../strongs/h/h68.md), and [mālā'](../../strongs/h/h4390.md) it; and they [sāṯam](../../strongs/h/h5640.md) all the [maʿyān](../../strongs/h/h4599.md) of [mayim](../../strongs/h/h4325.md), and [naphal](../../strongs/h/h5307.md) all the [towb](../../strongs/h/h2896.md) ['ets](../../strongs/h/h6086.md): only in [Qîr-ḥereś](../../strongs/h/h7025.md) [šā'ar](../../strongs/h/h7604.md) they the ['eben](../../strongs/h/h68.md) thereof; howbeit the [qallāʿ](../../strongs/h/h7051.md) went [cabab](../../strongs/h/h5437.md) it, and [nakah](../../strongs/h/h5221.md) it.

<a name="2kings_3_26"></a>2Kings 3:26

And when the [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md) [ra'ah](../../strongs/h/h7200.md) that the [milḥāmâ](../../strongs/h/h4421.md) was too [ḥāzaq](../../strongs/h/h2388.md) for him, he [laqach](../../strongs/h/h3947.md) with him seven hundred ['iysh](../../strongs/h/h376.md) that [šālap̄](../../strongs/h/h8025.md) [chereb](../../strongs/h/h2719.md), to break [bāqaʿ](../../strongs/h/h1234.md) even unto the [melek](../../strongs/h/h4428.md) of ['Ĕḏōm](../../strongs/h/h123.md): but they [yakol](../../strongs/h/h3201.md) not.

<a name="2kings_3_27"></a>2Kings 3:27

Then he [laqach](../../strongs/h/h3947.md) his [bᵊḵôr](../../strongs/h/h1060.md) [ben](../../strongs/h/h1121.md) that should have [mālaḵ](../../strongs/h/h4427.md) in his stead, and [ʿālâ](../../strongs/h/h5927.md) him for a [ʿōlâ](../../strongs/h/h5930.md) upon the [ḥômâ](../../strongs/h/h2346.md). And there was [gadowl](../../strongs/h/h1419.md) [qeṣep̄](../../strongs/h/h7110.md) against [Yisra'el](../../strongs/h/h3478.md): and they [nāsaʿ](../../strongs/h/h5265.md) from him, and [shuwb](../../strongs/h/h7725.md) to their own ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 2](2kings_2.md) - [2Kings 4](2kings_4.md)