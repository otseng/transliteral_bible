# [2Kings 16](https://www.blueletterbible.org/kjv/2kings/16)

<a name="2kings_16_1"></a>2Kings 16:1

In the seventeenth [šānâ](../../strongs/h/h8141.md) [šānâ](../../strongs/h/h8141.md) of [Peqaḥ](../../strongs/h/h6492.md) the [ben](../../strongs/h/h1121.md) of [Rᵊmalyâû](../../strongs/h/h7425.md) ['Āḥāz](../../strongs/h/h271.md) the [ben](../../strongs/h/h1121.md) of [Yôṯām](../../strongs/h/h3147.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) began to [mālaḵ](../../strongs/h/h4427.md).

<a name="2kings_16_2"></a>2Kings 16:2

Twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) was ['Āḥāz](../../strongs/h/h271.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and [mālaḵ](../../strongs/h/h4427.md) sixteen [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), and ['asah](../../strongs/h/h6213.md) not that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md), like [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md).

<a name="2kings_16_3"></a>2Kings 16:3

But he [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), yea, and made his [ben](../../strongs/h/h1121.md) to ['abar](../../strongs/h/h5674.md) the ['esh](../../strongs/h/h784.md), according to the [tôʿēḇâ](../../strongs/h/h8441.md) of the [gowy](../../strongs/h/h1471.md), whom [Yĕhovah](../../strongs/h/h3068.md) cast [yarash](../../strongs/h/h3423.md) from [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_16_4"></a>2Kings 16:4

And he [zabach](../../strongs/h/h2076.md) and [qāṭar](../../strongs/h/h6999.md) in the [bāmâ](../../strongs/h/h1116.md), and on the [giḇʿâ](../../strongs/h/h1389.md), and under every [raʿănān](../../strongs/h/h7488.md) ['ets](../../strongs/h/h6086.md).

<a name="2kings_16_5"></a>2Kings 16:5

Then [Rᵊṣîn](../../strongs/h/h7526.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) and [Peqaḥ](../../strongs/h/h6492.md) [ben](../../strongs/h/h1121.md) of [Rᵊmalyâû](../../strongs/h/h7425.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) to [milḥāmâ](../../strongs/h/h4421.md): and they [ṣûr](../../strongs/h/h6696.md) ['Āḥāz](../../strongs/h/h271.md), but [yakol](../../strongs/h/h3201.md) not [lāḥam](../../strongs/h/h3898.md) him.

<a name="2kings_16_6"></a>2Kings 16:6

At that [ʿēṯ](../../strongs/h/h6256.md) [Rᵊṣîn](../../strongs/h/h7526.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) [shuwb](../../strongs/h/h7725.md) ['Êlôṯ](../../strongs/h/h359.md) to ['Ărām](../../strongs/h/h758.md) ['Ăḏōmî](../../strongs/h/h130.md), and [nāšal](../../strongs/h/h5394.md) the [Yᵊhûḏî](../../strongs/h/h3064.md) from ['Êlôṯ](../../strongs/h/h359.md): and the ['rvmy](../../strongs/h/h726.md) [bow'](../../strongs/h/h935.md) to ['Êlôṯ](../../strongs/h/h359.md), and [yashab](../../strongs/h/h3427.md) there unto this [yowm](../../strongs/h/h3117.md).

<a name="2kings_16_7"></a>2Kings 16:7

So ['Āḥāz](../../strongs/h/h271.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to [Tiḡlaṯ Pil'Eser](../../strongs/h/h8407.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), ['āmar](../../strongs/h/h559.md), I am thy ['ebed](../../strongs/h/h5650.md) and thy [ben](../../strongs/h/h1121.md): [ʿālâ](../../strongs/h/h5927.md), and [yasha'](../../strongs/h/h3467.md) me out of the [kaph](../../strongs/h/h3709.md) of the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md), and out of the [kaph](../../strongs/h/h3709.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), which [quwm](../../strongs/h/h6965.md) against me.

<a name="2kings_16_8"></a>2Kings 16:8

And ['Āḥāz](../../strongs/h/h271.md) [laqach](../../strongs/h/h3947.md) the [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md) that was [māṣā'](../../strongs/h/h4672.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and in the ['ôṣār](../../strongs/h/h214.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and [shalach](../../strongs/h/h7971.md) it for a [shachad](../../strongs/h/h7810.md) to the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md).

<a name="2kings_16_9"></a>2Kings 16:9

And the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [shama'](../../strongs/h/h8085.md) unto him: for the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [ʿālâ](../../strongs/h/h5927.md) against [Dammeśeq](../../strongs/h/h1834.md), and [tāp̄aś](../../strongs/h/h8610.md) it, and [gālâ](../../strongs/h/h1540.md) to [qîr](../../strongs/h/h7024.md), and [muwth](../../strongs/h/h4191.md) [Rᵊṣîn](../../strongs/h/h7526.md).

<a name="2kings_16_10"></a>2Kings 16:10

And [melek](../../strongs/h/h4428.md) ['Āḥāz](../../strongs/h/h271.md) [yālaḵ](../../strongs/h/h3212.md) to [Dammeśeq](../../strongs/h/h1834.md) to [qārā'](../../strongs/h/h7125.md) [Tiḡlaṯ Pil'Eser](../../strongs/h/h8407.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), and [ra'ah](../../strongs/h/h7200.md) a [mizbeach](../../strongs/h/h4196.md) that was at [Dammeśeq](../../strongs/h/h1834.md): and [melek](../../strongs/h/h4428.md) ['Āḥāz](../../strongs/h/h271.md) [shalach](../../strongs/h/h7971.md) to ['Ûrîyâ](../../strongs/h/h223.md) the [kōhēn](../../strongs/h/h3548.md) the [dĕmuwth](../../strongs/h/h1823.md) of the [mizbeach](../../strongs/h/h4196.md), and the [taḇnîṯ](../../strongs/h/h8403.md) of it, according to all the [ma'aseh](../../strongs/h/h4639.md) thereof.

<a name="2kings_16_11"></a>2Kings 16:11

And ['Ûrîyâ](../../strongs/h/h223.md) the [kōhēn](../../strongs/h/h3548.md) [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) according to all that [melek](../../strongs/h/h4428.md) ['Āḥāz](../../strongs/h/h271.md) had [shalach](../../strongs/h/h7971.md) from [Dammeśeq](../../strongs/h/h1834.md): so ['Ûrîyâ](../../strongs/h/h223.md) the [kōhēn](../../strongs/h/h3548.md) ['asah](../../strongs/h/h6213.md) it against [melek](../../strongs/h/h4428.md) ['Āḥāz](../../strongs/h/h271.md) [bow'](../../strongs/h/h935.md) from [Dammeśeq](../../strongs/h/h1834.md).

<a name="2kings_16_12"></a>2Kings 16:12

And when the [melek](../../strongs/h/h4428.md) was [bow'](../../strongs/h/h935.md) from [Dammeśeq](../../strongs/h/h1834.md), the [melek](../../strongs/h/h4428.md) [ra'ah](../../strongs/h/h7200.md) the [mizbeach](../../strongs/h/h4196.md): and the [melek](../../strongs/h/h4428.md) [qāraḇ](../../strongs/h/h7126.md) to the [mizbeach](../../strongs/h/h4196.md), and [ʿālâ](../../strongs/h/h5927.md) thereon.

<a name="2kings_16_13"></a>2Kings 16:13

And he [qāṭar](../../strongs/h/h6999.md) his [ʿōlâ](../../strongs/h/h5930.md) and his [minchah](../../strongs/h/h4503.md), and [nacak](../../strongs/h/h5258.md) his [necek](../../strongs/h/h5262.md), and [zāraq](../../strongs/h/h2236.md) the [dam](../../strongs/h/h1818.md) of his [šelem](../../strongs/h/h8002.md), upon the [mizbeach](../../strongs/h/h4196.md).

<a name="2kings_16_14"></a>2Kings 16:14

And he [qāraḇ](../../strongs/h/h7126.md) also the [nᵊḥšeṯ](../../strongs/h/h5178.md) [mizbeach](../../strongs/h/h4196.md), which was [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), from the [paniym](../../strongs/h/h6440.md) of the [bayith](../../strongs/h/h1004.md), from between the [mizbeach](../../strongs/h/h4196.md) and the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and [nathan](../../strongs/h/h5414.md) it on the [ṣāp̄ôn](../../strongs/h/h6828.md) [yārēḵ](../../strongs/h/h3409.md) of the [mizbeach](../../strongs/h/h4196.md).

<a name="2kings_16_15"></a>2Kings 16:15

And [melek](../../strongs/h/h4428.md) ['Āḥāz](../../strongs/h/h271.md) [tsavah](../../strongs/h/h6680.md) ['Ûrîyâ](../../strongs/h/h223.md) the [kōhēn](../../strongs/h/h3548.md), ['āmar](../../strongs/h/h559.md), Upon the [gadowl](../../strongs/h/h1419.md) [mizbeach](../../strongs/h/h4196.md) [qāṭar](../../strongs/h/h6999.md) the [boqer](../../strongs/h/h1242.md) [ʿōlâ](../../strongs/h/h5930.md), and the ['ereb](../../strongs/h/h6153.md) [minchah](../../strongs/h/h4503.md), and the [melek](../../strongs/h/h4428.md) [ʿōlâ](../../strongs/h/h5930.md), and his [minchah](../../strongs/h/h4503.md), with the [ʿōlâ](../../strongs/h/h5930.md) of all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), and their [minchah](../../strongs/h/h4503.md), and their [necek](../../strongs/h/h5262.md); and [zāraq](../../strongs/h/h2236.md) upon it all the [dam](../../strongs/h/h1818.md) of the [ʿōlâ](../../strongs/h/h5930.md), and all the [dam](../../strongs/h/h1818.md) of the [zebach](../../strongs/h/h2077.md): and the [nᵊḥšeṯ](../../strongs/h/h5178.md) [mizbeach](../../strongs/h/h4196.md) shall be for me to [bāqar](../../strongs/h/h1239.md) by.

<a name="2kings_16_16"></a>2Kings 16:16

Thus ['asah](../../strongs/h/h6213.md) ['Ûrîyâ](../../strongs/h/h223.md) the [kōhēn](../../strongs/h/h3548.md), according to all that [melek](../../strongs/h/h4428.md) ['Āḥāz](../../strongs/h/h271.md) [tsavah](../../strongs/h/h6680.md).

<a name="2kings_16_17"></a>2Kings 16:17

And [melek](../../strongs/h/h4428.md) ['Āḥāz](../../strongs/h/h271.md) [qāṣaṣ](../../strongs/h/h7112.md) the [misgereṯ](../../strongs/h/h4526.md) of the [mᵊḵônâ](../../strongs/h/h4350.md), and [cuwr](../../strongs/h/h5493.md) the [kîyôr](../../strongs/h/h3595.md) from off them; and took [yarad](../../strongs/h/h3381.md) the [yam](../../strongs/h/h3220.md) from off the [nᵊḥšeṯ](../../strongs/h/h5178.md) [bāqār](../../strongs/h/h1241.md) that were under it, and [nathan](../../strongs/h/h5414.md) it upon a [marṣep̄eṯ](../../strongs/h/h4837.md) of ['eben](../../strongs/h/h68.md).

<a name="2kings_16_18"></a>2Kings 16:18

And the [mêsaḵ](../../strongs/h/h4329.md) [mêsaḵ](../../strongs/h/h4329.md) for the [shabbath](../../strongs/h/h7676.md) that they had [bānâ](../../strongs/h/h1129.md) in the [bayith](../../strongs/h/h1004.md), and the [melek](../../strongs/h/h4428.md) [māḇô'](../../strongs/h/h3996.md) [ḥîṣôn](../../strongs/h/h2435.md), [cabab](../../strongs/h/h5437.md) he from the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md).

<a name="2kings_16_19"></a>2Kings 16:19

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of ['Āḥāz](../../strongs/h/h271.md) which he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)?

<a name="2kings_16_20"></a>2Kings 16:20

And ['Āḥāz](../../strongs/h/h271.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) with his ['ab](../../strongs/h/h1.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md): and [Ḥizqîyâ](../../strongs/h/h2396.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 15](2kings_15.md) - [2Kings 17](2kings_17.md)