# [2Kings 5](https://www.blueletterbible.org/kjv/2kings/5)

<a name="2kings_5_1"></a>2Kings 5:1

Now [Naʿămān](../../strongs/h/h5283.md), [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) of the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md), was a [gadowl](../../strongs/h/h1419.md) ['iysh](../../strongs/h/h376.md) [paniym](../../strongs/h/h6440.md) his ['adown](../../strongs/h/h113.md), and [nasa'](../../strongs/h/h5375.md), because by him [Yĕhovah](../../strongs/h/h3068.md) had [nathan](../../strongs/h/h5414.md) [tᵊšûʿâ](../../strongs/h/h8668.md) unto ['Ărām](../../strongs/h/h758.md): he was also a [gibôr](../../strongs/h/h1368.md) ['iysh](../../strongs/h/h376.md) in [ḥayil](../../strongs/h/h2428.md), but he was a [ṣāraʿ](../../strongs/h/h6879.md).

<a name="2kings_5_2"></a>2Kings 5:2

And the ['Ărām](../../strongs/h/h758.md) had [yāṣā'](../../strongs/h/h3318.md) by [gᵊḏûḏ](../../strongs/h/h1416.md), and had [šāḇâ](../../strongs/h/h7617.md) out of the ['erets](../../strongs/h/h776.md) of [Yisra'el](../../strongs/h/h3478.md) a [qāṭān](../../strongs/h/h6996.md) [naʿărâ](../../strongs/h/h5291.md); and she waited [paniym](../../strongs/h/h6440.md) [Naʿămān](../../strongs/h/h5283.md) ['ishshah](../../strongs/h/h802.md).

<a name="2kings_5_3"></a>2Kings 5:3

And she ['āmar](../../strongs/h/h559.md) unto her [gᵊḇereṯ](../../strongs/h/h1404.md), ['aḥălay](../../strongs/h/h305.md) my ['adown](../../strongs/h/h113.md) were [paniym](../../strongs/h/h6440.md) the [nāḇî'](../../strongs/h/h5030.md) that is in [Šōmrôn](../../strongs/h/h8111.md)! for he would ['āsap̄](../../strongs/h/h622.md) him of his [ṣāraʿaṯ](../../strongs/h/h6883.md).

<a name="2kings_5_4"></a>2Kings 5:4

And one [bow'](../../strongs/h/h935.md), and [nāḡaḏ](../../strongs/h/h5046.md) his ['adown](../../strongs/h/h113.md), ['āmar](../../strongs/h/h559.md), Thus and thus [dabar](../../strongs/h/h1696.md) the [naʿărâ](../../strongs/h/h5291.md) that is of the ['erets](../../strongs/h/h776.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_5_5"></a>2Kings 5:5

And the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), [bow'](../../strongs/h/h935.md), and I will [shalach](../../strongs/h/h7971.md) a [sēp̄er](../../strongs/h/h5612.md) unto the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md). And he [yālaḵ](../../strongs/h/h3212.md), and [laqach](../../strongs/h/h3947.md) with [yad](../../strongs/h/h3027.md) ten [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md), and six thousand pieces of [zāhāḇ](../../strongs/h/h2091.md), and ten [ḥălîp̄â](../../strongs/h/h2487.md) of [beḡeḏ](../../strongs/h/h899.md).

<a name="2kings_5_6"></a>2Kings 5:6

And he [bow'](../../strongs/h/h935.md) the [sēp̄er](../../strongs/h/h5612.md) to the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), Now when this [sēp̄er](../../strongs/h/h5612.md) is [bow'](../../strongs/h/h935.md) unto thee, behold, I have therewith [shalach](../../strongs/h/h7971.md) [Naʿămān](../../strongs/h/h5283.md) my ['ebed](../../strongs/h/h5650.md) to thee, that thou mayest ['āsap̄](../../strongs/h/h622.md) him of his [ṣāraʿaṯ](../../strongs/h/h6883.md).

<a name="2kings_5_7"></a>2Kings 5:7

And it came to pass, when the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) had [qara'](../../strongs/h/h7121.md) the [sēp̄er](../../strongs/h/h5612.md), that he [qāraʿ](../../strongs/h/h7167.md) his [beḡeḏ](../../strongs/h/h899.md), and ['āmar](../../strongs/h/h559.md), Am I ['Elohiym](../../strongs/h/h430.md), to [muwth](../../strongs/h/h4191.md) and to make [ḥāyâ](../../strongs/h/h2421.md), that this man doth [shalach](../../strongs/h/h7971.md) unto me to ['āsap̄](../../strongs/h/h622.md) an ['iysh](../../strongs/h/h376.md) of his [ṣāraʿaṯ](../../strongs/h/h6883.md)? wherefore [yada'](../../strongs/h/h3045.md), I pray you, and [ra'ah](../../strongs/h/h7200.md) how he seeketh an ['ānâ](../../strongs/h/h579.md) against me.

<a name="2kings_5_8"></a>2Kings 5:8

And it was so, when ['Ĕlîšāʿ](../../strongs/h/h477.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) had [shama'](../../strongs/h/h8085.md) that the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) had [qāraʿ](../../strongs/h/h7167.md) his [beḡeḏ](../../strongs/h/h899.md), that he [shalach](../../strongs/h/h7971.md) to the [melek](../../strongs/h/h4428.md), ['āmar](../../strongs/h/h559.md), Wherefore hast thou [qāraʿ](../../strongs/h/h7167.md) thy [beḡeḏ](../../strongs/h/h899.md)? let him [bow'](../../strongs/h/h935.md) now to me, and he shall [yada'](../../strongs/h/h3045.md) that there is a [nāḇî'](../../strongs/h/h5030.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_5_9"></a>2Kings 5:9

So [Naʿămān](../../strongs/h/h5283.md) [bow'](../../strongs/h/h935.md) with his [sûs](../../strongs/h/h5483.md) and with his [reḵeḇ](../../strongs/h/h7393.md), and ['amad](../../strongs/h/h5975.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the [bayith](../../strongs/h/h1004.md) of ['Ĕlîšāʿ](../../strongs/h/h477.md).

<a name="2kings_5_10"></a>2Kings 5:10

And ['Ĕlîšāʿ](../../strongs/h/h477.md) [shalach](../../strongs/h/h7971.md) a [mal'ak](../../strongs/h/h4397.md) unto him, ['āmar](../../strongs/h/h559.md), [halak](../../strongs/h/h1980.md) and [rāḥaṣ](../../strongs/h/h7364.md) in [Yardēn](../../strongs/h/h3383.md) seven [pa'am](../../strongs/h/h6471.md), and thy [basar](../../strongs/h/h1320.md) shall [shuwb](../../strongs/h/h7725.md) to thee, and thou shalt be [ṭāhēr](../../strongs/h/h2891.md).

<a name="2kings_5_11"></a>2Kings 5:11

But [Naʿămān](../../strongs/h/h5283.md) was [qāṣap̄](../../strongs/h/h7107.md), and [yālaḵ](../../strongs/h/h3212.md), and ['āmar](../../strongs/h/h559.md), Behold, I ['āmar](../../strongs/h/h559.md), He will [yāṣā'](../../strongs/h/h3318.md) [yāṣā'](../../strongs/h/h3318.md) to me, and ['amad](../../strongs/h/h5975.md), and [qara'](../../strongs/h/h7121.md) on the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md), and [nûp̄](../../strongs/h/h5130.md) his [yad](../../strongs/h/h3027.md) over the [maqowm](../../strongs/h/h4725.md), and ['āsap̄](../../strongs/h/h622.md) the [ṣāraʿ](../../strongs/h/h6879.md).

<a name="2kings_5_12"></a>2Kings 5:12

Are not ['ăḇānâ](../../strongs/h/h71.md) ['Ămānâ](../../strongs/h/h549.md) and [parpar](../../strongs/h/h6554.md), [nāhār](../../strongs/h/h5104.md) of [Dammeśeq](../../strongs/h/h1834.md), [towb](../../strongs/h/h2896.md) than all the [mayim](../../strongs/h/h4325.md) of [Yisra'el](../../strongs/h/h3478.md)? may I not [rāḥaṣ](../../strongs/h/h7364.md) in them, and be [ṭāhēr](../../strongs/h/h2891.md)? So he [panah](../../strongs/h/h6437.md) and [yālaḵ](../../strongs/h/h3212.md) in a [chemah](../../strongs/h/h2534.md).

<a name="2kings_5_13"></a>2Kings 5:13

And his ['ebed](../../strongs/h/h5650.md) [nāḡaš](../../strongs/h/h5066.md), and [dabar](../../strongs/h/h1696.md) unto him, and ['āmar](../../strongs/h/h559.md), My ['ab](../../strongs/h/h1.md), if the [nāḇî'](../../strongs/h/h5030.md) had [dabar](../../strongs/h/h1696.md) thee do some [gadowl](../../strongs/h/h1419.md) [dabar](../../strongs/h/h1697.md), wouldest thou not have ['asah](../../strongs/h/h6213.md) it? how much rather then, when he ['āmar](../../strongs/h/h559.md) to thee, [rāḥaṣ](../../strongs/h/h7364.md), and be [ṭāhēr](../../strongs/h/h2891.md)?

<a name="2kings_5_14"></a>2Kings 5:14

Then went he [yarad](../../strongs/h/h3381.md), and [ṭāḇal](../../strongs/h/h2881.md) himself seven [pa'am](../../strongs/h/h6471.md) in [Yardēn](../../strongs/h/h3383.md), according to the [dabar](../../strongs/h/h1697.md) of the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md): and his [basar](../../strongs/h/h1320.md) [shuwb](../../strongs/h/h7725.md) like unto the [basar](../../strongs/h/h1320.md) of a [qāṭān](../../strongs/h/h6996.md) [naʿar](../../strongs/h/h5288.md), and he was [ṭāhēr](../../strongs/h/h2891.md).

<a name="2kings_5_15"></a>2Kings 5:15

And he [shuwb](../../strongs/h/h7725.md) to the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), he and all his [maḥănê](../../strongs/h/h4264.md), and [bow'](../../strongs/h/h935.md), and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) him: and he ['āmar](../../strongs/h/h559.md), Behold, now I [yada'](../../strongs/h/h3045.md) that there is no ['Elohiym](../../strongs/h/h430.md) in all the ['erets](../../strongs/h/h776.md), but in [Yisra'el](../../strongs/h/h3478.md): now therefore, I pray thee, [laqach](../../strongs/h/h3947.md) a [bĕrakah](../../strongs/h/h1293.md) of thy ['ebed](../../strongs/h/h5650.md).

<a name="2kings_5_16"></a>2Kings 5:16

But he ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), [paniym](../../strongs/h/h6440.md) whom I ['amad](../../strongs/h/h5975.md), I will [laqach](../../strongs/h/h3947.md) none. And he [pāṣar](../../strongs/h/h6484.md) him to [laqach](../../strongs/h/h3947.md) it; but he [mā'ēn](../../strongs/h/h3985.md).

<a name="2kings_5_17"></a>2Kings 5:17

And [Naʿămān](../../strongs/h/h5283.md) ['āmar](../../strongs/h/h559.md), Shall there not then, I pray thee, be [nathan](../../strongs/h/h5414.md) to thy ['ebed](../../strongs/h/h5650.md) [ṣemeḏ](../../strongs/h/h6776.md) [pereḏ](../../strongs/h/h6505.md) [maśśā'](../../strongs/h/h4853.md) of ['ăḏāmâ](../../strongs/h/h127.md)? for thy ['ebed](../../strongs/h/h5650.md) will henceforth ['asah](../../strongs/h/h6213.md) neither [ʿōlâ](../../strongs/h/h5930.md) nor [zebach](../../strongs/h/h2077.md) unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), but unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_5_18"></a>2Kings 5:18

In this [dabar](../../strongs/h/h1697.md) [Yĕhovah](../../strongs/h/h3068.md) [sālaḥ](../../strongs/h/h5545.md) thy ['ebed](../../strongs/h/h5650.md), that when my ['adown](../../strongs/h/h113.md) [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Rimmôn](../../strongs/h/h7417.md) to [shachah](../../strongs/h/h7812.md) there, and he [šāʿan](../../strongs/h/h8172.md) on my [yad](../../strongs/h/h3027.md), and I [shachah](../../strongs/h/h7812.md) myself in the [bayith](../../strongs/h/h1004.md) of [Rimmôn](../../strongs/h/h7417.md): when I [shachah](../../strongs/h/h7812.md) myself in the [bayith](../../strongs/h/h1004.md) of [Rimmôn](../../strongs/h/h7417.md), [Yĕhovah](../../strongs/h/h3068.md) [sālaḥ](../../strongs/h/h5545.md) thy ['ebed](../../strongs/h/h5650.md) in this [dabar](../../strongs/h/h1697.md).

<a name="2kings_5_19"></a>2Kings 5:19

And he ['āmar](../../strongs/h/h559.md) unto him, [yālaḵ](../../strongs/h/h3212.md) in [shalowm](../../strongs/h/h7965.md). So he [yālaḵ](../../strongs/h/h3212.md) from him a [kiḇrâ](../../strongs/h/h3530.md) ['erets](../../strongs/h/h776.md).

<a name="2kings_5_20"></a>2Kings 5:20

But [Gêḥăzî](../../strongs/h/h1522.md), the [naʿar](../../strongs/h/h5288.md) of ['Ĕlîšāʿ](../../strongs/h/h477.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), ['āmar](../../strongs/h/h559.md), Behold, my ['adown](../../strongs/h/h113.md) hath [ḥāśaḵ](../../strongs/h/h2820.md) [Naʿămān](../../strongs/h/h5283.md) this ['Ărammy](../../strongs/h/h761.md), in not [laqach](../../strongs/h/h3947.md) at his [yad](../../strongs/h/h3027.md) that which he [bow'](../../strongs/h/h935.md): but, as [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), I will [rûṣ](../../strongs/h/h7323.md) ['aḥar](../../strongs/h/h310.md) him, and [laqach](../../strongs/h/h3947.md) [mᵊ'ûmâ](../../strongs/h/h3972.md) of him.

<a name="2kings_5_21"></a>2Kings 5:21

So [Gêḥăzî](../../strongs/h/h1522.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) [Naʿămān](../../strongs/h/h5283.md). And when [Naʿămān](../../strongs/h/h5283.md) [ra'ah](../../strongs/h/h7200.md) him [rûṣ](../../strongs/h/h7323.md) ['aḥar](../../strongs/h/h310.md) him, he [naphal](../../strongs/h/h5307.md) from the [merkāḇâ](../../strongs/h/h4818.md) to [qārā'](../../strongs/h/h7125.md) him, and ['āmar](../../strongs/h/h559.md), Is all [shalowm](../../strongs/h/h7965.md)?

<a name="2kings_5_22"></a>2Kings 5:22

And he ['āmar](../../strongs/h/h559.md), All is [shalowm](../../strongs/h/h7965.md). My ['adown](../../strongs/h/h113.md) hath [shalach](../../strongs/h/h7971.md) me, ['āmar](../../strongs/h/h559.md), Behold, even now there be [bow'](../../strongs/h/h935.md) to me from [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md) two [naʿar](../../strongs/h/h5288.md) of the [ben](../../strongs/h/h1121.md) of the [nāḇî'](../../strongs/h/h5030.md): [nathan](../../strongs/h/h5414.md) them, I pray thee, a [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md), and two [ḥălîp̄â](../../strongs/h/h2487.md) of [beḡeḏ](../../strongs/h/h899.md).

<a name="2kings_5_23"></a>2Kings 5:23

And [Naʿămān](../../strongs/h/h5283.md) ['āmar](../../strongs/h/h559.md), Be [yā'al](../../strongs/h/h2974.md), [laqach](../../strongs/h/h3947.md) two [kikār](../../strongs/h/h3603.md). And he [pāraṣ](../../strongs/h/h6555.md) him, and [ṣûr](../../strongs/h/h6696.md) two [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md) in two [ḥārîṭ](../../strongs/h/h2754.md), with two [ḥălîp̄â](../../strongs/h/h2487.md) of [beḡeḏ](../../strongs/h/h899.md), and [nathan](../../strongs/h/h5414.md) them upon two of his [naʿar](../../strongs/h/h5288.md); and they [nasa'](../../strongs/h/h5375.md) them [paniym](../../strongs/h/h6440.md) him.

<a name="2kings_5_24"></a>2Kings 5:24

And when he [bow'](../../strongs/h/h935.md) to the [ʿōp̄el](../../strongs/h/h6076.md), he [laqach](../../strongs/h/h3947.md) them from their [yad](../../strongs/h/h3027.md), and [paqad](../../strongs/h/h6485.md) them in the [bayith](../../strongs/h/h1004.md): and he let the ['enowsh](../../strongs/h/h582.md) [shalach](../../strongs/h/h7971.md), and they [yālaḵ](../../strongs/h/h3212.md).

<a name="2kings_5_25"></a>2Kings 5:25

But he [bow'](../../strongs/h/h935.md), and ['amad](../../strongs/h/h5975.md) before his ['adown](../../strongs/h/h113.md). And ['Ĕlîšāʿ](../../strongs/h/h477.md) ['āmar](../../strongs/h/h559.md) unto him, Whence comest thou, [Gêḥăzî](../../strongs/h/h1522.md)? And he ['āmar](../../strongs/h/h559.md), Thy ['ebed](../../strongs/h/h5650.md) [halak](../../strongs/h/h1980.md) no whither.

<a name="2kings_5_26"></a>2Kings 5:26

And he ['āmar](../../strongs/h/h559.md) unto him, [halak](../../strongs/h/h1980.md) not mine [leb](../../strongs/h/h3820.md) with thee, when the ['iysh](../../strongs/h/h376.md) [hāp̄aḵ](../../strongs/h/h2015.md) again from his [merkāḇâ](../../strongs/h/h4818.md) to [qārā'](../../strongs/h/h7125.md) thee? Is it a [ʿēṯ](../../strongs/h/h6256.md) to [laqach](../../strongs/h/h3947.md) [keceph](../../strongs/h/h3701.md), and to [laqach](../../strongs/h/h3947.md) [beḡeḏ](../../strongs/h/h899.md), and [zayiṯ](../../strongs/h/h2132.md), and [kerem](../../strongs/h/h3754.md), and [tso'n](../../strongs/h/h6629.md), and [bāqār](../../strongs/h/h1241.md), and ['ebed](../../strongs/h/h5650.md), and [šip̄ḥâ](../../strongs/h/h8198.md)?

<a name="2kings_5_27"></a>2Kings 5:27

The [ṣāraʿaṯ](../../strongs/h/h6883.md) therefore of [Naʿămān](../../strongs/h/h5283.md) shall [dāḇaq](../../strongs/h/h1692.md) unto thee, and unto thy [zera'](../../strongs/h/h2233.md) ['owlam](../../strongs/h/h5769.md). And he [yāṣā'](../../strongs/h/h3318.md) from his [paniym](../../strongs/h/h6440.md) a [ṣāraʿ](../../strongs/h/h6879.md) as [šeleḡ](../../strongs/h/h7950.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 4](2kings_4.md) - [2Kings 6](2kings_6.md)