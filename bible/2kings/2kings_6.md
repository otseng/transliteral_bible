# [2Kings 6](https://www.blueletterbible.org/kjv/2kings/6)

<a name="2kings_6_1"></a>2Kings 6:1

And the [ben](../../strongs/h/h1121.md) of the [nāḇî'](../../strongs/h/h5030.md) ['āmar](../../strongs/h/h559.md) [paniym](../../strongs/h/h6440.md) ['Ĕlîšāʿ](../../strongs/h/h477.md), Behold now, the [maqowm](../../strongs/h/h4725.md) where we [yashab](../../strongs/h/h3427.md) with thee is too [tsar](../../strongs/h/h6862.md) for us.

<a name="2kings_6_2"></a>2Kings 6:2

Let us [yālaḵ](../../strongs/h/h3212.md), we pray thee, unto [Yardēn](../../strongs/h/h3383.md), and [laqach](../../strongs/h/h3947.md) thence every ['iysh](../../strongs/h/h376.md) a [qôrâ](../../strongs/h/h6982.md), and let us ['asah](../../strongs/h/h6213.md) us a [maqowm](../../strongs/h/h4725.md) there, where we may [yashab](../../strongs/h/h3427.md). And he ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) ye.

<a name="2kings_6_3"></a>2Kings 6:3

And one ['āmar](../../strongs/h/h559.md), Be [yā'al](../../strongs/h/h2974.md), I pray thee, and [yālaḵ](../../strongs/h/h3212.md) with thy ['ebed](../../strongs/h/h5650.md). And he ['āmar](../../strongs/h/h559.md), I will [yālaḵ](../../strongs/h/h3212.md).

<a name="2kings_6_4"></a>2Kings 6:4

So he [yālaḵ](../../strongs/h/h3212.md) with them. And when they [bow'](../../strongs/h/h935.md) to [Yardēn](../../strongs/h/h3383.md), they [gāzar](../../strongs/h/h1504.md) ['ets](../../strongs/h/h6086.md).

<a name="2kings_6_5"></a>2Kings 6:5

But as one was [naphal](../../strongs/h/h5307.md) a [qôrâ](../../strongs/h/h6982.md), the [barzel](../../strongs/h/h1270.md) [naphal](../../strongs/h/h5307.md) into the [mayim](../../strongs/h/h4325.md): and he [ṣāʿaq](../../strongs/h/h6817.md), and ['āmar](../../strongs/h/h559.md), ['ăhâ](../../strongs/h/h162.md), ['adown](../../strongs/h/h113.md)! for it was [sha'al](../../strongs/h/h7592.md).

<a name="2kings_6_6"></a>2Kings 6:6

And the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), Where [naphal](../../strongs/h/h5307.md) it? And he [ra'ah](../../strongs/h/h7200.md) him the [maqowm](../../strongs/h/h4725.md). And he [qāṣaḇ](../../strongs/h/h7094.md) an ['ets](../../strongs/h/h6086.md), and [shalak](../../strongs/h/h7993.md) it in thither; and the [barzel](../../strongs/h/h1270.md) did [ṣûp̄](../../strongs/h/h6687.md).

<a name="2kings_6_7"></a>2Kings 6:7

Therefore ['āmar](../../strongs/h/h559.md) he, [ruwm](../../strongs/h/h7311.md) to thee. And he [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md), and [laqach](../../strongs/h/h3947.md) it.

<a name="2kings_6_8"></a>2Kings 6:8

Then the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) [lāḥam](../../strongs/h/h3898.md) against [Yisra'el](../../strongs/h/h3478.md), and took [ya'ats](../../strongs/h/h3289.md) with his ['ebed](../../strongs/h/h5650.md), ['āmar](../../strongs/h/h559.md), In [palōnî](../../strongs/h/h6423.md) and ['almōnî](../../strongs/h/h492.md) a [maqowm](../../strongs/h/h4725.md) shall be my [taḥănâ](../../strongs/h/h8466.md).

<a name="2kings_6_9"></a>2Kings 6:9

And the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) [shalach](../../strongs/h/h7971.md) unto the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), [shamar](../../strongs/h/h8104.md) that thou ['abar](../../strongs/h/h5674.md) not such a [maqowm](../../strongs/h/h4725.md); for thither the ['Ărām](../../strongs/h/h758.md) are [nāḥēṯ](../../strongs/h/h5185.md).

<a name="2kings_6_10"></a>2Kings 6:10

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md) to the [maqowm](../../strongs/h/h4725.md) which the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) him and [zāhar](../../strongs/h/h2094.md) him of, and [shamar](../../strongs/h/h8104.md) himself there, not once nor twice.

<a name="2kings_6_11"></a>2Kings 6:11

Therefore the [leb](../../strongs/h/h3820.md) of the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) was [sāʿar](../../strongs/h/h5590.md) for this [dabar](../../strongs/h/h1697.md); and he [qara'](../../strongs/h/h7121.md) his ['ebed](../../strongs/h/h5650.md), and ['āmar](../../strongs/h/h559.md) unto them, Will ye not [nāḡaḏ](../../strongs/h/h5046.md) me which of us is for the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="2kings_6_12"></a>2Kings 6:12

And one of his ['ebed](../../strongs/h/h5650.md) ['āmar](../../strongs/h/h559.md), None, my ['adown](../../strongs/h/h113.md), O [melek](../../strongs/h/h4428.md): but ['Ĕlîšāʿ](../../strongs/h/h477.md), the [nāḇî'](../../strongs/h/h5030.md) that is in [Yisra'el](../../strongs/h/h3478.md), [nāḡaḏ](../../strongs/h/h5046.md) the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) the [dabar](../../strongs/h/h1697.md) that thou [dabar](../../strongs/h/h1696.md) in thy [ḥeḏer](../../strongs/h/h2315.md) [miškāḇ](../../strongs/h/h4904.md).

<a name="2kings_6_13"></a>2Kings 6:13

And he ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) and [ra'ah](../../strongs/h/h7200.md) ['êḵô](../../strongs/h/h351.md) he is, that I may [shalach](../../strongs/h/h7971.md) and [laqach](../../strongs/h/h3947.md) him. And it was [nāḡaḏ](../../strongs/h/h5046.md) him, ['āmar](../../strongs/h/h559.md), Behold, he is in [Dōṯān](../../strongs/h/h1886.md).

<a name="2kings_6_14"></a>2Kings 6:14

Therefore [shalach](../../strongs/h/h7971.md) he thither [sûs](../../strongs/h/h5483.md), and [reḵeḇ](../../strongs/h/h7393.md), and a [kāḇēḏ](../../strongs/h/h3515.md) [ḥayil](../../strongs/h/h2428.md): and they [bow'](../../strongs/h/h935.md) by [layil](../../strongs/h/h3915.md), and [naqaph](../../strongs/h/h5362.md) the [ʿîr](../../strongs/h/h5892.md) [naqaph](../../strongs/h/h5362.md).

<a name="2kings_6_15"></a>2Kings 6:15

And when the [sharath](../../strongs/h/h8334.md) of the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) was [quwm](../../strongs/h/h6965.md) [šāḵam](../../strongs/h/h7925.md), and [yāṣā'](../../strongs/h/h3318.md), behold, an [ḥayil](../../strongs/h/h2428.md) [cabab](../../strongs/h/h5437.md) the [ʿîr](../../strongs/h/h5892.md) both with [sûs](../../strongs/h/h5483.md) and [reḵeḇ](../../strongs/h/h7393.md). And his [naʿar](../../strongs/h/h5288.md) ['āmar](../../strongs/h/h559.md) unto him, ['ăhâ](../../strongs/h/h162.md), my ['adown](../../strongs/h/h113.md)! how shall we ['asah](../../strongs/h/h6213.md)?

<a name="2kings_6_16"></a>2Kings 6:16

And he ['āmar](../../strongs/h/h559.md), [yare'](../../strongs/h/h3372.md) not: for they that be with us are [rab](../../strongs/h/h7227.md) than they that be with them.

<a name="2kings_6_17"></a>2Kings 6:17

And ['Ĕlîšāʿ](../../strongs/h/h477.md) [palal](../../strongs/h/h6419.md), and ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md), I pray thee, [paqach](../../strongs/h/h6491.md) his ['ayin](../../strongs/h/h5869.md), that he may [ra'ah](../../strongs/h/h7200.md). And [Yĕhovah](../../strongs/h/h3068.md) [paqach](../../strongs/h/h6491.md) the ['ayin](../../strongs/h/h5869.md) of the [naʿar](../../strongs/h/h5288.md); and he [ra'ah](../../strongs/h/h7200.md): and, behold, the [har](../../strongs/h/h2022.md) was [mālā'](../../strongs/h/h4390.md) of [sûs](../../strongs/h/h5483.md) and [reḵeḇ](../../strongs/h/h7393.md) of ['esh](../../strongs/h/h784.md) [cabiyb](../../strongs/h/h5439.md) ['Ĕlîšāʿ](../../strongs/h/h477.md).

<a name="2kings_6_18"></a>2Kings 6:18

And when they [yarad](../../strongs/h/h3381.md) to him, ['Ĕlîšāʿ](../../strongs/h/h477.md) [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), [nakah](../../strongs/h/h5221.md) this [gowy](../../strongs/h/h1471.md), I pray thee, with [sanvērîm](../../strongs/h/h5575.md). And he [nakah](../../strongs/h/h5221.md) them with [sanvērîm](../../strongs/h/h5575.md) according to the [dabar](../../strongs/h/h1697.md) of ['Ĕlîšāʿ](../../strongs/h/h477.md).

<a name="2kings_6_19"></a>2Kings 6:19

And ['Ĕlîšāʿ](../../strongs/h/h477.md) ['āmar](../../strongs/h/h559.md) unto them, This is not the [derek](../../strongs/h/h1870.md), neither is [zô](../../strongs/h/h2090.md) the [ʿîr](../../strongs/h/h5892.md): [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) me, and I will [yālaḵ](../../strongs/h/h3212.md) you to the ['iysh](../../strongs/h/h376.md) whom ye [bāqaš](../../strongs/h/h1245.md). But he [yālaḵ](../../strongs/h/h3212.md) them to [Šōmrôn](../../strongs/h/h8111.md).

<a name="2kings_6_20"></a>2Kings 6:20

And it came to pass, when they were [bow'](../../strongs/h/h935.md) into [Šōmrôn](../../strongs/h/h8111.md), that ['Ĕlîšāʿ](../../strongs/h/h477.md) ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md), [paqach](../../strongs/h/h6491.md) the ['ayin](../../strongs/h/h5869.md) of these men, that they may [ra'ah](../../strongs/h/h7200.md). And [Yĕhovah](../../strongs/h/h3068.md) [paqach](../../strongs/h/h6491.md) their ['ayin](../../strongs/h/h5869.md), and they [ra'ah](../../strongs/h/h7200.md); and, behold, they were in the midst of [Šōmrôn](../../strongs/h/h8111.md).

<a name="2kings_6_21"></a>2Kings 6:21

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto ['Ĕlîšāʿ](../../strongs/h/h477.md), when he [ra'ah](../../strongs/h/h7200.md) them, My ['ab](../../strongs/h/h1.md), shall I [nakah](../../strongs/h/h5221.md) them? shall I [nakah](../../strongs/h/h5221.md) them?

<a name="2kings_6_22"></a>2Kings 6:22

And he ['āmar](../../strongs/h/h559.md), Thou shalt not [nakah](../../strongs/h/h5221.md) them: wouldest thou [nakah](../../strongs/h/h5221.md) those whom thou hast taken [šāḇâ](../../strongs/h/h7617.md) with thy [chereb](../../strongs/h/h2719.md) and with thy [qesheth](../../strongs/h/h7198.md)? [śûm](../../strongs/h/h7760.md) [lechem](../../strongs/h/h3899.md) and [mayim](../../strongs/h/h4325.md) [paniym](../../strongs/h/h6440.md) them, that they may ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and [yālaḵ](../../strongs/h/h3212.md) to their ['adown](../../strongs/h/h113.md).

<a name="2kings_6_23"></a>2Kings 6:23

And he [kārâ](../../strongs/h/h3739.md) [gadowl](../../strongs/h/h1419.md) [kērâ](../../strongs/h/h3740.md) for them: and when they had ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), he sent them [shalach](../../strongs/h/h7971.md), and they [yālaḵ](../../strongs/h/h3212.md) to their ['adown](../../strongs/h/h113.md). So the [gᵊḏûḏ](../../strongs/h/h1416.md) of ['Ărām](../../strongs/h/h758.md) [bow'](../../strongs/h/h935.md) no more into the ['erets](../../strongs/h/h776.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_6_24"></a>2Kings 6:24

And it came to pass ['aḥar](../../strongs/h/h310.md), that [Ben-Hăḏaḏ](../../strongs/h/h1130.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) [qāḇaṣ](../../strongs/h/h6908.md) all his [maḥănê](../../strongs/h/h4264.md), and [ʿālâ](../../strongs/h/h5927.md), and [ṣûr](../../strongs/h/h6696.md) [Šōmrôn](../../strongs/h/h8111.md).

<a name="2kings_6_25"></a>2Kings 6:25

And there was a [gadowl](../../strongs/h/h1419.md) [rāʿāḇ](../../strongs/h/h7458.md) in [Šōmrôn](../../strongs/h/h8111.md): and, behold, they [ṣûr](../../strongs/h/h6696.md) it, until a [chamowr](../../strongs/h/h2543.md) [ro'sh](../../strongs/h/h7218.md) was sold for fourscore pieces of [keceph](../../strongs/h/h3701.md), and the fourth [rōḇaʿ](../../strongs/h/h7255.md) of a [qaḇ](../../strongs/h/h6894.md) of dove's [diḇyônîm](../../strongs/h/h1686.md) [yônâ](../../strongs/h/h3123.md) [ḥărê yônîm](../../strongs/h/h2755.md) for five pieces of [keceph](../../strongs/h/h3701.md).

<a name="2kings_6_26"></a>2Kings 6:26

And as the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) was ['abar](../../strongs/h/h5674.md) upon the [ḥômâ](../../strongs/h/h2346.md), there [ṣāʿaq](../../strongs/h/h6817.md) an ['ishshah](../../strongs/h/h802.md) unto him, ['āmar](../../strongs/h/h559.md), [yasha'](../../strongs/h/h3467.md), my ['adown](../../strongs/h/h113.md), O [melek](../../strongs/h/h4428.md).

<a name="2kings_6_27"></a>2Kings 6:27

And he ['āmar](../../strongs/h/h559.md), If [Yĕhovah](../../strongs/h/h3068.md) do not [yasha'](../../strongs/h/h3467.md) thee, whence shall I [yasha'](../../strongs/h/h3467.md) thee? out of the [gōren](../../strongs/h/h1637.md), or out of the [yeqeḇ](../../strongs/h/h3342.md)?

<a name="2kings_6_28"></a>2Kings 6:28

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto her, What aileth thee? And she ['āmar](../../strongs/h/h559.md), This ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md) unto me, [nathan](../../strongs/h/h5414.md) thy [ben](../../strongs/h/h1121.md), that we may ['akal](../../strongs/h/h398.md) him to [yowm](../../strongs/h/h3117.md), and we will ['akal](../../strongs/h/h398.md) my [ben](../../strongs/h/h1121.md) [māḥār](../../strongs/h/h4279.md).

<a name="2kings_6_29"></a>2Kings 6:29

So we [bāšal](../../strongs/h/h1310.md) my [ben](../../strongs/h/h1121.md), and did ['akal](../../strongs/h/h398.md) him: and I ['āmar](../../strongs/h/h559.md) unto her on the ['aḥēr](../../strongs/h/h312.md) [yowm](../../strongs/h/h3117.md), [nathan](../../strongs/h/h5414.md) thy [ben](../../strongs/h/h1121.md), that we may ['akal](../../strongs/h/h398.md) him: and she hath [chaba'](../../strongs/h/h2244.md) her [ben](../../strongs/h/h1121.md).

<a name="2kings_6_30"></a>2Kings 6:30

And it came to pass, when the [melek](../../strongs/h/h4428.md) [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of the ['ishshah](../../strongs/h/h802.md), that he [qāraʿ](../../strongs/h/h7167.md) his [beḡeḏ](../../strongs/h/h899.md); and he ['abar](../../strongs/h/h5674.md) upon the [ḥômâ](../../strongs/h/h2346.md), and the ['am](../../strongs/h/h5971.md) [ra'ah](../../strongs/h/h7200.md), and, behold, he had [śaq](../../strongs/h/h8242.md) [bayith](../../strongs/h/h1004.md) upon his [basar](../../strongs/h/h1320.md).

<a name="2kings_6_31"></a>2Kings 6:31

Then he ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) so and more also to me, if the [ro'sh](../../strongs/h/h7218.md) of ['Ĕlîšāʿ](../../strongs/h/h477.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄āṭ](../../strongs/h/h8202.md) shall ['amad](../../strongs/h/h5975.md) on him this [yowm](../../strongs/h/h3117.md).

<a name="2kings_6_32"></a>2Kings 6:32

But ['Ĕlîšāʿ](../../strongs/h/h477.md) [yashab](../../strongs/h/h3427.md) in his [bayith](../../strongs/h/h1004.md), and the [zāqēn](../../strongs/h/h2205.md) [yashab](../../strongs/h/h3427.md) with him; and [shalach](../../strongs/h/h7971.md) an ['iysh](../../strongs/h/h376.md) from [paniym](../../strongs/h/h6440.md) him: but ere the [mal'ak](../../strongs/h/h4397.md) [bow'](../../strongs/h/h935.md) to him, he ['āmar](../../strongs/h/h559.md) to the [zāqēn](../../strongs/h/h2205.md), [ra'ah](../../strongs/h/h7200.md) ye how this [ben](../../strongs/h/h1121.md) of a [ratsach](../../strongs/h/h7523.md) hath [shalach](../../strongs/h/h7971.md) to [cuwr](../../strongs/h/h5493.md) mine [ro'sh](../../strongs/h/h7218.md)? [ra'ah](../../strongs/h/h7200.md), when the [mal'ak](../../strongs/h/h4397.md) [bow'](../../strongs/h/h935.md), [cagar](../../strongs/h/h5462.md) the [deleṯ](../../strongs/h/h1817.md), and hold him [lāḥaṣ](../../strongs/h/h3905.md) at the [deleṯ](../../strongs/h/h1817.md): is not the [qowl](../../strongs/h/h6963.md) of his ['adown](../../strongs/h/h113.md) [regel](../../strongs/h/h7272.md) ['aḥar](../../strongs/h/h310.md) him?

<a name="2kings_6_33"></a>2Kings 6:33

And while he yet [dabar](../../strongs/h/h1696.md) with them, behold, the [mal'ak](../../strongs/h/h4397.md) [yarad](../../strongs/h/h3381.md) unto him: and he ['āmar](../../strongs/h/h559.md), Behold, this [ra'](../../strongs/h/h7451.md) is of [Yĕhovah](../../strongs/h/h3068.md); what should I [yāḥal](../../strongs/h/h3176.md) for [Yĕhovah](../../strongs/h/h3068.md) any longer?

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 5](2kings_5.md) - [2Kings 7](2kings_7.md)