# [2Kings 14](https://www.blueletterbible.org/kjv/2kings/14)

<a name="2kings_14_1"></a>2Kings 14:1

In the second [šānâ](../../strongs/h/h8141.md) of [Yô'Āš](../../strongs/h/h3101.md) [ben](../../strongs/h/h1121.md) of [Yô'Āḥāz](../../strongs/h/h3099.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [mālaḵ](../../strongs/h/h4427.md) ['Ămaṣyâ](../../strongs/h/h558.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="2kings_14_2"></a>2Kings 14:2

He was twenty and five [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and [mālaḵ](../../strongs/h/h4427.md) twenty and nine [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [YᵊhôʿĀḏîn](../../strongs/h/h3086.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2kings_14_3"></a>2Kings 14:3

And he ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), yet not like [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md): he ['asah](../../strongs/h/h6213.md) according to all things as [Yô'Āš](../../strongs/h/h3101.md) his ['ab](../../strongs/h/h1.md) ['asah](../../strongs/h/h6213.md).

<a name="2kings_14_4"></a>2Kings 14:4

Howbeit the [bāmâ](../../strongs/h/h1116.md) were not [cuwr](../../strongs/h/h5493.md): as yet the ['am](../../strongs/h/h5971.md) did [zabach](../../strongs/h/h2076.md) and [qāṭar](../../strongs/h/h6999.md) on the [bāmâ](../../strongs/h/h1116.md).

<a name="2kings_14_5"></a>2Kings 14:5

And it came to pass, as soon as the [mamlāḵâ](../../strongs/h/h4467.md) was [ḥāzaq](../../strongs/h/h2388.md) in his [yad](../../strongs/h/h3027.md), that he [nakah](../../strongs/h/h5221.md) his ['ebed](../../strongs/h/h5650.md) which had [nakah](../../strongs/h/h5221.md) the [melek](../../strongs/h/h4428.md) his ['ab](../../strongs/h/h1.md).

<a name="2kings_14_6"></a>2Kings 14:6

But the [ben](../../strongs/h/h1121.md) of the [nakah](../../strongs/h/h5221.md) he [muwth](../../strongs/h/h4191.md) not: according unto that which is [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md), wherein [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md), ['āmar](../../strongs/h/h559.md), The ['ab](../../strongs/h/h1.md) shall not be put to [muwth](../../strongs/h/h4191.md) for the [ben](../../strongs/h/h1121.md), nor the [ben](../../strongs/h/h1121.md) be put to [muwth](../../strongs/h/h4191.md) for the ['ab](../../strongs/h/h1.md); but every ['iysh](../../strongs/h/h376.md) shall be put to [muwth](../../strongs/h/h4191.md) for his own [ḥēṭĕ'](../../strongs/h/h2399.md).

<a name="2kings_14_7"></a>2Kings 14:7

He [nakah](../../strongs/h/h5221.md) of ['Ĕḏōm](../../strongs/h/h123.md) in the [gay'](../../strongs/h/h1516.md) of [melaḥ](../../strongs/h/h4417.md) ten thousand, and [tāp̄aś](../../strongs/h/h8610.md) [selaʿ](../../strongs/h/h5554.md) by [milḥāmâ](../../strongs/h/h4421.md), and [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of it [Yāqṯᵊ'Ēl](../../strongs/h/h3371.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="2kings_14_8"></a>2Kings 14:8

Then ['Ămaṣyâ](../../strongs/h/h558.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to [Yᵊhô'Āš](../../strongs/h/h3060.md), the [ben](../../strongs/h/h1121.md) of [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) [ben](../../strongs/h/h1121.md) of [Yêû'](../../strongs/h/h3058.md), [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), let us [ra'ah](../../strongs/h/h7200.md) one another in the [paniym](../../strongs/h/h6440.md).

<a name="2kings_14_9"></a>2Kings 14:9

And [Yᵊhô'Āš](../../strongs/h/h3060.md) the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md) to ['Ămaṣyâ](../../strongs/h/h558.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), ['āmar](../../strongs/h/h559.md), The [ḥôaḥ](../../strongs/h/h2336.md) that was in [Lᵊḇānôn](../../strongs/h/h3844.md) [shalach](../../strongs/h/h7971.md) to the ['erez](../../strongs/h/h730.md) that was in [Lᵊḇānôn](../../strongs/h/h3844.md), ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) thy [bath](../../strongs/h/h1323.md) to my [ben](../../strongs/h/h1121.md) to ['ishshah](../../strongs/h/h802.md): and there ['abar](../../strongs/h/h5674.md) by a [sadeh](../../strongs/h/h7704.md) [chay](../../strongs/h/h2416.md) that was in [Lᵊḇānôn](../../strongs/h/h3844.md), and trode [rāmas](../../strongs/h/h7429.md) the [ḥôaḥ](../../strongs/h/h2336.md).

<a name="2kings_14_10"></a>2Kings 14:10

Thou hast [nakah](../../strongs/h/h5221.md) [nakah](../../strongs/h/h5221.md) ['Ĕḏōm](../../strongs/h/h123.md), and thine [leb](../../strongs/h/h3820.md) hath [nasa'](../../strongs/h/h5375.md) thee: [kabad](../../strongs/h/h3513.md) of this, and [yashab](../../strongs/h/h3427.md) at [bayith](../../strongs/h/h1004.md): for why shouldest thou [gārâ](../../strongs/h/h1624.md) to thy [ra'](../../strongs/h/h7451.md), that thou shouldest [naphal](../../strongs/h/h5307.md), even thou, and [Yehuwdah](../../strongs/h/h3063.md) with thee?

<a name="2kings_14_11"></a>2Kings 14:11

But ['Ămaṣyâ](../../strongs/h/h558.md) would not [shama'](../../strongs/h/h8085.md). Therefore [Yᵊhô'Āš](../../strongs/h/h3060.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md); and he and ['Ămaṣyâ](../../strongs/h/h558.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [ra'ah](../../strongs/h/h7200.md) one another in the [paniym](../../strongs/h/h6440.md) at [Bêṯ Šemeš](../../strongs/h/h1053.md), which belongeth to [Yehuwdah](../../strongs/h/h3063.md).

<a name="2kings_14_12"></a>2Kings 14:12

And [Yehuwdah](../../strongs/h/h3063.md) was put to the [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) [Yisra'el](../../strongs/h/h3478.md); and they [nûs](../../strongs/h/h5127.md) every ['iysh](../../strongs/h/h376.md) to their ['ohel](../../strongs/h/h168.md).

<a name="2kings_14_13"></a>2Kings 14:13

And [Yᵊhô'Āš](../../strongs/h/h3060.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [tāp̄aś](../../strongs/h/h8610.md) ['Ămaṣyâ](../../strongs/h/h558.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), the [ben](../../strongs/h/h1121.md) of [Yᵊhô'Āš](../../strongs/h/h3060.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥazyâ](../../strongs/h/h274.md), at [Bêṯ Šemeš](../../strongs/h/h1053.md), and [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), and [pāraṣ](../../strongs/h/h6555.md) the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) from the [sha'ar](../../strongs/h/h8179.md) of ['Ep̄rayim](../../strongs/h/h669.md) unto the [pinnâ](../../strongs/h/h6438.md) [sha'ar](../../strongs/h/h8179.md), four hundred ['ammâ](../../strongs/h/h520.md).

<a name="2kings_14_14"></a>2Kings 14:14

And he [laqach](../../strongs/h/h3947.md) all the [zāhāḇ](../../strongs/h/h2091.md) and [keceph](../../strongs/h/h3701.md), and all the [kĕliy](../../strongs/h/h3627.md) that were [māṣā'](../../strongs/h/h4672.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and in the ['ôṣār](../../strongs/h/h214.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and [ben](../../strongs/h/h1121.md) [taʿărûḇâ](../../strongs/h/h8594.md) , and [shuwb](../../strongs/h/h7725.md) to [Šōmrôn](../../strongs/h/h8111.md).

<a name="2kings_14_15"></a>2Kings 14:15

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yᵊhô'Āš](../../strongs/h/h3060.md) which he ['asah](../../strongs/h/h6213.md), and his [gᵊḇûrâ](../../strongs/h/h1369.md), and how he [lāḥam](../../strongs/h/h3898.md) with ['Ămaṣyâ](../../strongs/h/h558.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="2kings_14_16"></a>2Kings 14:16

And [Yᵊhô'Āš](../../strongs/h/h3060.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) in [Šōmrôn](../../strongs/h/h8111.md) with the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md); and [YārāḇʿĀm](../../strongs/h/h3379.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2kings_14_17"></a>2Kings 14:17

And ['Ămaṣyâ](../../strongs/h/h558.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) the [maveth](../../strongs/h/h4194.md) of [Yᵊhô'Āš](../../strongs/h/h3060.md) [ben](../../strongs/h/h1121.md) of [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) fifteen [šānâ](../../strongs/h/h8141.md).

<a name="2kings_14_18"></a>2Kings 14:18

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of ['Ămaṣyâ](../../strongs/h/h558.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)?

<a name="2kings_14_19"></a>2Kings 14:19

Now they [qāšar](../../strongs/h/h7194.md) a [qešer](../../strongs/h/h7195.md) against him in [Yĕruwshalaim](../../strongs/h/h3389.md): and he [nûs](../../strongs/h/h5127.md) to [Lāḵîš](../../strongs/h/h3923.md); but they [shalach](../../strongs/h/h7971.md) ['aḥar](../../strongs/h/h310.md) him to [Lāḵîš](../../strongs/h/h3923.md), and [muwth](../../strongs/h/h4191.md) him there.

<a name="2kings_14_20"></a>2Kings 14:20

And they [nasa'](../../strongs/h/h5375.md) him on [sûs](../../strongs/h/h5483.md): and he was [qāḇar](../../strongs/h/h6912.md) at [Yĕruwshalaim](../../strongs/h/h3389.md) with his ['ab](../../strongs/h/h1.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="2kings_14_21"></a>2Kings 14:21

And all the ['am](../../strongs/h/h5971.md) of [Yehuwdah](../../strongs/h/h3063.md) [laqach](../../strongs/h/h3947.md) [ʿĂzaryâ](../../strongs/h/h5838.md), which was sixteen [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), and made him [mālaḵ](../../strongs/h/h4427.md) instead of his ['ab](../../strongs/h/h1.md) ['Ămaṣyâ](../../strongs/h/h558.md).

<a name="2kings_14_22"></a>2Kings 14:22

He [bānâ](../../strongs/h/h1129.md) ['Êlôṯ](../../strongs/h/h359.md), and [shuwb](../../strongs/h/h7725.md) it to [Yehuwdah](../../strongs/h/h3063.md), ['aḥar](../../strongs/h/h310.md) that the [melek](../../strongs/h/h4428.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md).

<a name="2kings_14_23"></a>2Kings 14:23

In the fifteenth [šānâ](../../strongs/h/h8141.md) [šānâ](../../strongs/h/h8141.md) of ['Ămaṣyâ](../../strongs/h/h558.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) began to [mālaḵ](../../strongs/h/h4427.md) in [Šōmrôn](../../strongs/h/h8111.md), and reigned forty and one [šānâ](../../strongs/h/h8141.md).

<a name="2kings_14_24"></a>2Kings 14:24

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md): he [cuwr](../../strongs/h/h5493.md) not from all the [chatta'ath](../../strongs/h/h2403.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), who made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md).

<a name="2kings_14_25"></a>2Kings 14:25

He [shuwb](../../strongs/h/h7725.md) the [gᵊḇûl](../../strongs/h/h1366.md) of [Yisra'el](../../strongs/h/h3478.md) from the [bow'](../../strongs/h/h935.md) of [Ḥămāṯ](../../strongs/h/h2574.md) unto the [yam](../../strongs/h/h3220.md) of the ['arabah](../../strongs/h/h6160.md), according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), which he [dabar](../../strongs/h/h1696.md) by the [yad](../../strongs/h/h3027.md) of his ['ebed](../../strongs/h/h5650.md) [Yonah](../../strongs/h/h3124.md), the [ben](../../strongs/h/h1121.md) of ['Ămitay](../../strongs/h/h573.md), the [nāḇî'](../../strongs/h/h5030.md), which was of [Ḡṯ Ḥp̄R](../../strongs/h/h1662.md).

<a name="2kings_14_26"></a>2Kings 14:26

For [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) the ['oniy](../../strongs/h/h6040.md) of [Yisra'el](../../strongs/h/h3478.md), that it was [me'od](../../strongs/h/h3966.md) [marah](../../strongs/h/h4784.md): for there was ['ep̄es](../../strongs/h/h657.md) any [ʿāṣar](../../strongs/h/h6113.md), nor any ['azab](../../strongs/h/h5800.md), nor any [ʿāzar](../../strongs/h/h5826.md) for [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_14_27"></a>2Kings 14:27

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) not that he would blot [māḥâ](../../strongs/h/h4229.md) the [shem](../../strongs/h/h8034.md) of [Yisra'el](../../strongs/h/h3478.md) from under [shamayim](../../strongs/h/h8064.md): but he [yasha'](../../strongs/h/h3467.md) them by the [yad](../../strongs/h/h3027.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md).

<a name="2kings_14_28"></a>2Kings 14:28

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [YārāḇʿĀm](../../strongs/h/h3379.md), and all that he ['asah](../../strongs/h/h6213.md), and his [gᵊḇûrâ](../../strongs/h/h1369.md), how he [lāḥam](../../strongs/h/h3898.md), and how he [shuwb](../../strongs/h/h7725.md) [Dammeśeq](../../strongs/h/h1834.md), and [Ḥămāṯ](../../strongs/h/h2574.md), which belonged to [Yehuwdah](../../strongs/h/h3063.md), for [Yisra'el](../../strongs/h/h3478.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="2kings_14_29"></a>2Kings 14:29

And [YārāḇʿĀm](../../strongs/h/h3379.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), even with the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md); and [Zᵊḵaryâ](../../strongs/h/h2148.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 13](2kings_13.md) - [2Kings 15](2kings_15.md)