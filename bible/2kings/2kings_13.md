# [2Kings 13](https://www.blueletterbible.org/kjv/2kings/13)

<a name="2kings_13_1"></a>2Kings 13:1

In the three [šānâ](../../strongs/h/h8141.md) and twentieth [šānâ](../../strongs/h/h8141.md) of [Yô'Āš](../../strongs/h/h3101.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥazyâ](../../strongs/h/h274.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) the [ben](../../strongs/h/h1121.md) of [Yêû'](../../strongs/h/h3058.md) began to [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) in [Šōmrôn](../../strongs/h/h8111.md), and reigned seventeen [šānâ](../../strongs/h/h8141.md).

<a name="2kings_13_2"></a>2Kings 13:2

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) the [chatta'ath](../../strongs/h/h2403.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), which made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md); he [cuwr](../../strongs/h/h5493.md) not therefrom.

<a name="2kings_13_3"></a>2Kings 13:3

And the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against [Yisra'el](../../strongs/h/h3478.md), and he [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of [Ḥăzā'Ēl](../../strongs/h/h2371.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md), and into the [yad](../../strongs/h/h3027.md) of [Ben-Hăḏaḏ](../../strongs/h/h1130.md) the [ben](../../strongs/h/h1121.md) of [Ḥăzā'Ēl](../../strongs/h/h2371.md), all their [yowm](../../strongs/h/h3117.md).

<a name="2kings_13_4"></a>2Kings 13:4

And [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) [ḥālâ](../../strongs/h/h2470.md) [Yĕhovah](../../strongs/h/h3068.md), and [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) [paniym](../../strongs/h/h6440.md) him: for he [ra'ah](../../strongs/h/h7200.md) the [laḥaṣ](../../strongs/h/h3906.md) of [Yisra'el](../../strongs/h/h3478.md), because the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) [lāḥaṣ](../../strongs/h/h3905.md) them.

<a name="2kings_13_5"></a>2Kings 13:5

(And [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) [Yisra'el](../../strongs/h/h3478.md) a [yasha'](../../strongs/h/h3467.md), so that they [yāṣā'](../../strongs/h/h3318.md) from under the [yad](../../strongs/h/h3027.md) of the ['Ărām](../../strongs/h/h758.md): and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yashab](../../strongs/h/h3427.md) in their ['ohel](../../strongs/h/h168.md), as [šilšôm](../../strongs/h/h8032.md) [tᵊmôl](../../strongs/h/h8543.md).

<a name="2kings_13_6"></a>2Kings 13:6

Nevertheless they [cuwr](../../strongs/h/h5493.md) not from the [chatta'ath](../../strongs/h/h2403.md) of the [bayith](../../strongs/h/h1004.md) of [YārāḇʿĀm](../../strongs/h/h3379.md), who made [Yisra'el](../../strongs/h/h3478.md) [chata'](../../strongs/h/h2398.md), but [halak](../../strongs/h/h1980.md) therein: and there ['amad](../../strongs/h/h5975.md) the ['ăšērâ](../../strongs/h/h842.md) also in [Šōmrôn](../../strongs/h/h8111.md).)

<a name="2kings_13_7"></a>2Kings 13:7

Neither did he [šā'ar](../../strongs/h/h7604.md) of the ['am](../../strongs/h/h5971.md) to [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) but fifty [pārāš](../../strongs/h/h6571.md), and ten [reḵeḇ](../../strongs/h/h7393.md), and ten thousand [raḡlî](../../strongs/h/h7273.md); for the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) had ['abad](../../strongs/h/h6.md) them, and had [śûm](../../strongs/h/h7760.md) them like the ['aphar](../../strongs/h/h6083.md) by [dûš](../../strongs/h/h1758.md).

<a name="2kings_13_8"></a>2Kings 13:8

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yᵊhô'Āḥāz](../../strongs/h/h3059.md), and all that he ['asah](../../strongs/h/h6213.md), and his [gᵊḇûrâ](../../strongs/h/h1369.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="2kings_13_9"></a>2Kings 13:9

And [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md); and they [qāḇar](../../strongs/h/h6912.md) him in [Šōmrôn](../../strongs/h/h8111.md): and [Yô'Āš](../../strongs/h/h3101.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2kings_13_10"></a>2Kings 13:10

In the thirty [šānâ](../../strongs/h/h8141.md) and seventh [šānâ](../../strongs/h/h8141.md) of [Yô'Āš](../../strongs/h/h3101.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) began [Yᵊhô'Āš](../../strongs/h/h3060.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) to [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) in [Šōmrôn](../../strongs/h/h8111.md), and reigned sixteen [šānâ](../../strongs/h/h8141.md).

<a name="2kings_13_11"></a>2Kings 13:11

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md); he [cuwr](../../strongs/h/h5493.md) not from all the [chatta'ath](../../strongs/h/h2403.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), who made [Yisra'el](../../strongs/h/h3478.md) [chata'](../../strongs/h/h2398.md): but he [halak](../../strongs/h/h1980.md) therein.

<a name="2kings_13_12"></a>2Kings 13:12

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yô'Āš](../../strongs/h/h3101.md), and all that he ['asah](../../strongs/h/h6213.md), and his [gᵊḇûrâ](../../strongs/h/h1369.md) wherewith he [lāḥam](../../strongs/h/h3898.md) against ['Ămaṣyâ](../../strongs/h/h558.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="2kings_13_13"></a>2Kings 13:13

And [Yô'Āš](../../strongs/h/h3101.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md); and [YārāḇʿĀm](../../strongs/h/h3379.md) [yashab](../../strongs/h/h3427.md) upon his [kicce'](../../strongs/h/h3678.md): and [Yô'Āš](../../strongs/h/h3101.md) was [qāḇar](../../strongs/h/h6912.md) in [Šōmrôn](../../strongs/h/h8111.md) with the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_13_14"></a>2Kings 13:14

Now ['Ĕlîšāʿ](../../strongs/h/h477.md) was [ḥālâ](../../strongs/h/h2470.md) of his [ḥŏlî](../../strongs/h/h2483.md) whereof he [muwth](../../strongs/h/h4191.md). And [Yô'Āš](../../strongs/h/h3101.md) the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [yarad](../../strongs/h/h3381.md) unto him, and [bāḵâ](../../strongs/h/h1058.md) over his [paniym](../../strongs/h/h6440.md), and ['āmar](../../strongs/h/h559.md), O my ['ab](../../strongs/h/h1.md), my ['ab](../../strongs/h/h1.md), the [reḵeḇ](../../strongs/h/h7393.md) of [Yisra'el](../../strongs/h/h3478.md), and the [pārāš](../../strongs/h/h6571.md) thereof.

<a name="2kings_13_15"></a>2Kings 13:15

And ['Ĕlîšāʿ](../../strongs/h/h477.md) ['āmar](../../strongs/h/h559.md) unto him, [laqach](../../strongs/h/h3947.md) [qesheth](../../strongs/h/h7198.md) and [chets](../../strongs/h/h2671.md). And he [laqach](../../strongs/h/h3947.md) unto him [qesheth](../../strongs/h/h7198.md) and [chets](../../strongs/h/h2671.md).

<a name="2kings_13_16"></a>2Kings 13:16

And he ['āmar](../../strongs/h/h559.md) to the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), [rāḵaḇ](../../strongs/h/h7392.md) thine [yad](../../strongs/h/h3027.md) upon the [qesheth](../../strongs/h/h7198.md). And he [rāḵaḇ](../../strongs/h/h7392.md) his [yad](../../strongs/h/h3027.md) upon it: and ['Ĕlîšāʿ](../../strongs/h/h477.md) [śûm](../../strongs/h/h7760.md) his [yad](../../strongs/h/h3027.md) upon the [melek](../../strongs/h/h4428.md) [yad](../../strongs/h/h3027.md).

<a name="2kings_13_17"></a>2Kings 13:17

And he ['āmar](../../strongs/h/h559.md), [pāṯaḥ](../../strongs/h/h6605.md) the [ḥallôn](../../strongs/h/h2474.md) [qeḏem](../../strongs/h/h6924.md). And he [pāṯaḥ](../../strongs/h/h6605.md) it. Then ['Ĕlîšāʿ](../../strongs/h/h477.md) ['āmar](../../strongs/h/h559.md), [yārâ](../../strongs/h/h3384.md). And he [yārâ](../../strongs/h/h3384.md). And he ['āmar](../../strongs/h/h559.md), The [chets](../../strongs/h/h2671.md) of [Yĕhovah](../../strongs/h/h3068.md) [tᵊšûʿâ](../../strongs/h/h8668.md), and the [chets](../../strongs/h/h2671.md) of [tᵊšûʿâ](../../strongs/h/h8668.md) from ['Ărām](../../strongs/h/h758.md): for thou shalt [nakah](../../strongs/h/h5221.md) the ['Ărām](../../strongs/h/h758.md) in ['Ăp̄Ēq](../../strongs/h/h663.md), till thou have [kalah](../../strongs/h/h3615.md) them.

<a name="2kings_13_18"></a>2Kings 13:18

And he ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) the [chets](../../strongs/h/h2671.md). And he [laqach](../../strongs/h/h3947.md) them. And he ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), [nakah](../../strongs/h/h5221.md) upon the ['erets](../../strongs/h/h776.md). And he [nakah](../../strongs/h/h5221.md) thrice [pa'am](../../strongs/h/h6471.md), and ['amad](../../strongs/h/h5975.md).

<a name="2kings_13_19"></a>2Kings 13:19

And the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) was [qāṣap̄](../../strongs/h/h7107.md) with him, and ['āmar](../../strongs/h/h559.md), Thou shouldest have [nakah](../../strongs/h/h5221.md) five or six [pa'am](../../strongs/h/h6471.md); then hadst thou [nakah](../../strongs/h/h5221.md) ['Ărām](../../strongs/h/h758.md) till thou hadst [kalah](../../strongs/h/h3615.md) it: whereas now thou shalt [nakah](../../strongs/h/h5221.md) ['Ărām](../../strongs/h/h758.md) but thrice [pa'am](../../strongs/h/h6471.md).

<a name="2kings_13_20"></a>2Kings 13:20

And ['Ĕlîšāʿ](../../strongs/h/h477.md) [muwth](../../strongs/h/h4191.md), and they [qāḇar](../../strongs/h/h6912.md) him. And the [gᵊḏûḏ](../../strongs/h/h1416.md) of the [Mô'āḇ](../../strongs/h/h4124.md) [bow'](../../strongs/h/h935.md) the ['erets](../../strongs/h/h776.md) at the [bow'](../../strongs/h/h935.md) of the [šānâ](../../strongs/h/h8141.md).

<a name="2kings_13_21"></a>2Kings 13:21

And it came to pass, as they were [qāḇar](../../strongs/h/h6912.md) an ['iysh](../../strongs/h/h376.md), that, behold, they [ra'ah](../../strongs/h/h7200.md) a [gᵊḏûḏ](../../strongs/h/h1416.md) of men; and they [shalak](../../strongs/h/h7993.md) the ['iysh](../../strongs/h/h376.md) into the [qeber](../../strongs/h/h6913.md) of ['Ĕlîšāʿ](../../strongs/h/h477.md): and when the ['iysh](../../strongs/h/h376.md) was let [yālaḵ](../../strongs/h/h3212.md), and [naga'](../../strongs/h/h5060.md) the ['etsem](../../strongs/h/h6106.md) of ['Ĕlîšāʿ](../../strongs/h/h477.md), he [ḥāyâ](../../strongs/h/h2421.md), and [quwm](../../strongs/h/h6965.md) on his [regel](../../strongs/h/h7272.md).

<a name="2kings_13_22"></a>2Kings 13:22

But [Ḥăzā'Ēl](../../strongs/h/h2371.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) [lāḥaṣ](../../strongs/h/h3905.md) [Yisra'el](../../strongs/h/h3478.md) all the [yowm](../../strongs/h/h3117.md) of [Yᵊhô'Āḥāz](../../strongs/h/h3059.md).

<a name="2kings_13_23"></a>2Kings 13:23

And [Yĕhovah](../../strongs/h/h3068.md) was [chanan](../../strongs/h/h2603.md) unto them, and had [racham](../../strongs/h/h7355.md) on them, and had [panah](../../strongs/h/h6437.md) unto them, because of his [bĕriyth](../../strongs/h/h1285.md) with ['Abraham](../../strongs/h/h85.md), [Yiṣḥāq](../../strongs/h/h3327.md), and [Ya'aqob](../../strongs/h/h3290.md), and ['āḇâ](../../strongs/h/h14.md) not [shachath](../../strongs/h/h7843.md) them, neither [shalak](../../strongs/h/h7993.md) he them from his [paniym](../../strongs/h/h6440.md) as yet.

<a name="2kings_13_24"></a>2Kings 13:24

So [Ḥăzā'Ēl](../../strongs/h/h2371.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) [muwth](../../strongs/h/h4191.md); and [Ben-Hăḏaḏ](../../strongs/h/h1130.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2kings_13_25"></a>2Kings 13:25

And [Yᵊhô'Āš](../../strongs/h/h3060.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) [laqach](../../strongs/h/h3947.md) [shuwb](../../strongs/h/h7725.md) out of the [yad](../../strongs/h/h3027.md) of [Ben-Hăḏaḏ](../../strongs/h/h1130.md) the [ben](../../strongs/h/h1121.md) of [Ḥăzā'Ēl](../../strongs/h/h2371.md) the [ʿîr](../../strongs/h/h5892.md), which he had [laqach](../../strongs/h/h3947.md) out of the [yad](../../strongs/h/h3027.md) of [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) his ['ab](../../strongs/h/h1.md) by [milḥāmâ](../../strongs/h/h4421.md). Three [pa'am](../../strongs/h/h6471.md) did [Yô'Āš](../../strongs/h/h3101.md) [nakah](../../strongs/h/h5221.md) him, and [shuwb](../../strongs/h/h7725.md) the [ʿîr](../../strongs/h/h5892.md) of [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 12](2kings_12.md) - [2Kings 14](2kings_14.md)