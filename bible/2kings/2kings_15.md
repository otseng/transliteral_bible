# [2Kings 15](https://www.blueletterbible.org/kjv/2kings/15)

<a name="2kings_15_1"></a>2Kings 15:1

In the twenty [šānâ](../../strongs/h/h8141.md) and seventh [šānâ](../../strongs/h/h8141.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) began [ʿĂzaryâ](../../strongs/h/h5838.md) [ben](../../strongs/h/h1121.md) of ['Ămaṣyâ](../../strongs/h/h558.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) to [mālaḵ](../../strongs/h/h4427.md).

<a name="2kings_15_2"></a>2Kings 15:2

Sixteen [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) was he when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) two and fifty [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [Yᵊḵālyâ](../../strongs/h/h3203.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2kings_15_3"></a>2Kings 15:3

And he ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), according to all that his ['ab](../../strongs/h/h1.md) ['Ămaṣyâ](../../strongs/h/h558.md) had ['asah](../../strongs/h/h6213.md);

<a name="2kings_15_4"></a>2Kings 15:4

Save that the [bāmâ](../../strongs/h/h1116.md) were not [cuwr](../../strongs/h/h5493.md): the ['am](../../strongs/h/h5971.md) [zabach](../../strongs/h/h2076.md) and [qāṭar](../../strongs/h/h6999.md) still on the [bāmâ](../../strongs/h/h1116.md).

<a name="2kings_15_5"></a>2Kings 15:5

And [Yĕhovah](../../strongs/h/h3068.md) [naga'](../../strongs/h/h5060.md) the [melek](../../strongs/h/h4428.md), so that he was a [ṣāraʿ](../../strongs/h/h6879.md) unto the [yowm](../../strongs/h/h3117.md) of his [maveth](../../strongs/h/h4194.md), and [yashab](../../strongs/h/h3427.md) in a [ḥāp̄šûṯ](../../strongs/h/h2669.md) [bayith](../../strongs/h/h1004.md). And [Yôṯām](../../strongs/h/h3147.md) the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md) was over the [bayith](../../strongs/h/h1004.md), [shaphat](../../strongs/h/h8199.md) the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md).

<a name="2kings_15_6"></a>2Kings 15:6

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [ʿĂzaryâ](../../strongs/h/h5838.md), and all that he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)?

<a name="2kings_15_7"></a>2Kings 15:7

So [ʿĂzaryâ](../../strongs/h/h5838.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md); and they [qāḇar](../../strongs/h/h6912.md) him with his ['ab](../../strongs/h/h1.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md): and [Yôṯām](../../strongs/h/h3147.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2kings_15_8"></a>2Kings 15:8

In the thirty [šānâ](../../strongs/h/h8141.md) and eighth [šānâ](../../strongs/h/h8141.md) of [ʿĂzaryâ](../../strongs/h/h5838.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) did [Zᵊḵaryâ](../../strongs/h/h2148.md) the [ben](../../strongs/h/h1121.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) in [Šōmrôn](../../strongs/h/h8111.md) six [ḥōḏeš](../../strongs/h/h2320.md).

<a name="2kings_15_9"></a>2Kings 15:9

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), as his ['ab](../../strongs/h/h1.md) had ['asah](../../strongs/h/h6213.md): he [cuwr](../../strongs/h/h5493.md) not from the [chatta'ath](../../strongs/h/h2403.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), who made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md).

<a name="2kings_15_10"></a>2Kings 15:10

And [Šallûm](../../strongs/h/h7967.md) the [ben](../../strongs/h/h1121.md) of [Yāḇēš](../../strongs/h/h3003.md) [qāšar](../../strongs/h/h7194.md) against him, and [nakah](../../strongs/h/h5221.md) him [qŏḇāl](../../strongs/h/h6905.md) the ['am](../../strongs/h/h5971.md), and [muwth](../../strongs/h/h4191.md) him, and [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2kings_15_11"></a>2Kings 15:11

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Zᵊḵaryâ](../../strongs/h/h2148.md), behold, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_15_12"></a>2Kings 15:12

This was the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) which he [dabar](../../strongs/h/h1696.md) unto [Yêû'](../../strongs/h/h3058.md), ['āmar](../../strongs/h/h559.md), Thy [ben](../../strongs/h/h1121.md) shall [yashab](../../strongs/h/h3427.md) on the [kicce'](../../strongs/h/h3678.md) of [Yisra'el](../../strongs/h/h3478.md) unto the fourth generation. And so it came to pass.

<a name="2kings_15_13"></a>2Kings 15:13

[Šallûm](../../strongs/h/h7967.md) the [ben](../../strongs/h/h1121.md) of [Yāḇēš](../../strongs/h/h3003.md) began to [mālaḵ](../../strongs/h/h4427.md) in the nine and thirtieth [šānâ](../../strongs/h/h8141.md) of ['Uzziyah](../../strongs/h/h5818.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md); and he [mālaḵ](../../strongs/h/h4427.md) a [yowm](../../strongs/h/h3117.md) [yeraḥ](../../strongs/h/h3391.md) in [Šōmrôn](../../strongs/h/h8111.md).

<a name="2kings_15_14"></a>2Kings 15:14

For [Mᵊnaḥēm](../../strongs/h/h4505.md) the [ben](../../strongs/h/h1121.md) of [Gāḏî](../../strongs/h/h1424.md) [ʿālâ](../../strongs/h/h5927.md) from [Tirṣâ](../../strongs/h/h8656.md), and [bow'](../../strongs/h/h935.md) to [Šōmrôn](../../strongs/h/h8111.md), and [nakah](../../strongs/h/h5221.md) [Šallûm](../../strongs/h/h7967.md) the [ben](../../strongs/h/h1121.md) of [Yāḇēš](../../strongs/h/h3003.md) in [Šōmrôn](../../strongs/h/h8111.md), and [muwth](../../strongs/h/h4191.md) him, and [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2kings_15_15"></a>2Kings 15:15

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Šallûm](../../strongs/h/h7967.md), and his [qešer](../../strongs/h/h7195.md) which he [qāšar](../../strongs/h/h7194.md), behold, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_15_16"></a>2Kings 15:16

Then [Mᵊnaḥēm](../../strongs/h/h4505.md) [nakah](../../strongs/h/h5221.md) [Tip̄Saḥ](../../strongs/h/h8607.md), and all that were therein, and the [gᵊḇûl](../../strongs/h/h1366.md) thereof from [Tirṣâ](../../strongs/h/h8656.md): because they [pāṯaḥ](../../strongs/h/h6605.md) not to him, therefore he [nakah](../../strongs/h/h5221.md) it; and all the [hārê](../../strongs/h/h2030.md) he [bāqaʿ](../../strongs/h/h1234.md).

<a name="2kings_15_17"></a>2Kings 15:17

In the nine [šānâ](../../strongs/h/h8141.md) and thirtieth [šānâ](../../strongs/h/h8141.md) of [ʿĂzaryâ](../../strongs/h/h5838.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) began [Mᵊnaḥēm](../../strongs/h/h4505.md) the [ben](../../strongs/h/h1121.md) of [Gāḏî](../../strongs/h/h1424.md) to [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md), and reigned ten [šānâ](../../strongs/h/h8141.md) in [Šōmrôn](../../strongs/h/h8111.md).

<a name="2kings_15_18"></a>2Kings 15:18

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md): he [cuwr](../../strongs/h/h5493.md) not all his [yowm](../../strongs/h/h3117.md) from the [chatta'ath](../../strongs/h/h2403.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), who made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md).

<a name="2kings_15_19"></a>2Kings 15:19

And [Pûl](../../strongs/h/h6322.md) the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [bow'](../../strongs/h/h935.md) against the ['erets](../../strongs/h/h776.md): and [Mᵊnaḥēm](../../strongs/h/h4505.md) [nathan](../../strongs/h/h5414.md) [Pûl](../../strongs/h/h6322.md) a thousand [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md), that his [yad](../../strongs/h/h3027.md) might be with him to [ḥāzaq](../../strongs/h/h2388.md) the [mamlāḵâ](../../strongs/h/h4467.md) in his [yad](../../strongs/h/h3027.md).

<a name="2kings_15_20"></a>2Kings 15:20

And [Mᵊnaḥēm](../../strongs/h/h4505.md) [yāṣā'](../../strongs/h/h3318.md) the [keceph](../../strongs/h/h3701.md) of [Yisra'el](../../strongs/h/h3478.md), even of all the [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), of each ['iysh](../../strongs/h/h376.md) fifty [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md), to [nathan](../../strongs/h/h5414.md) to the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md). So the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [shuwb](../../strongs/h/h7725.md), and ['amad](../../strongs/h/h5975.md) not there in the ['erets](../../strongs/h/h776.md).

<a name="2kings_15_21"></a>2Kings 15:21

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Mᵊnaḥēm](../../strongs/h/h4505.md), and all that he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="2kings_15_22"></a>2Kings 15:22

And [Mᵊnaḥēm](../../strongs/h/h4505.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md); and [Pᵊqaḥyâ](../../strongs/h/h6494.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2kings_15_23"></a>2Kings 15:23

In the fiftieth [šānâ](../../strongs/h/h8141.md) [šānâ](../../strongs/h/h8141.md) of [ʿĂzaryâ](../../strongs/h/h5838.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [Pᵊqaḥyâ](../../strongs/h/h6494.md) the [ben](../../strongs/h/h1121.md) of [Mᵊnaḥēm](../../strongs/h/h4505.md) began to [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) in [Šōmrôn](../../strongs/h/h8111.md), and reigned two [šānâ](../../strongs/h/h8141.md).

<a name="2kings_15_24"></a>2Kings 15:24

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md): he [cuwr](../../strongs/h/h5493.md) not from the [chatta'ath](../../strongs/h/h2403.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), who made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md).

<a name="2kings_15_25"></a>2Kings 15:25

But [Peqaḥ](../../strongs/h/h6492.md) the [ben](../../strongs/h/h1121.md) of [Rᵊmalyâû](../../strongs/h/h7425.md), a [šālîš](../../strongs/h/h7991.md) of his, [qāšar](../../strongs/h/h7194.md) against him, and [nakah](../../strongs/h/h5221.md) him in [Šōmrôn](../../strongs/h/h8111.md), in the ['armôn](../../strongs/h/h759.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), with ['Argōḇ](../../strongs/h/h709.md) and ['Aryê](../../strongs/h/h745.md), and with him fifty ['iysh](../../strongs/h/h376.md) of the [ben](../../strongs/h/h1121.md) [Gilʿāḏî](../../strongs/h/h1569.md): and he [muwth](../../strongs/h/h4191.md) him, and [mālaḵ](../../strongs/h/h4427.md) in his room.

<a name="2kings_15_26"></a>2Kings 15:26

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Pᵊqaḥyâ](../../strongs/h/h6494.md), and all that he ['asah](../../strongs/h/h6213.md), behold, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_15_27"></a>2Kings 15:27

In the two [šānâ](../../strongs/h/h8141.md) and fiftieth [šānâ](../../strongs/h/h8141.md) of [ʿĂzaryâ](../../strongs/h/h5838.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [Peqaḥ](../../strongs/h/h6492.md) the [ben](../../strongs/h/h1121.md) of [Rᵊmalyâû](../../strongs/h/h7425.md) began to [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) in [Šōmrôn](../../strongs/h/h8111.md), and reigned twenty [šānâ](../../strongs/h/h8141.md).

<a name="2kings_15_28"></a>2Kings 15:28

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md): he [cuwr](../../strongs/h/h5493.md) not from the [chatta'ath](../../strongs/h/h2403.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), who made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md).

<a name="2kings_15_29"></a>2Kings 15:29

In the [yowm](../../strongs/h/h3117.md) of [Peqaḥ](../../strongs/h/h6492.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) [Tiḡlaṯ Pil'Eser](../../strongs/h/h8407.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), and [laqach](../../strongs/h/h3947.md) [ʿÎyôn](../../strongs/h/h5859.md), and ['Āḇēl Bêṯ-MaʿĂḵâ](../../strongs/h/h62.md), and [Yānôaḥ](../../strongs/h/h3239.md), and [Qeḏeš](../../strongs/h/h6943.md), and [Ḥāṣôr](../../strongs/h/h2674.md), and [Gilʿāḏ](../../strongs/h/h1568.md), and [Gālîl](../../strongs/h/h1551.md), all the ['erets](../../strongs/h/h776.md) of [Nap̄tālî](../../strongs/h/h5321.md), and carried them [gālâ](../../strongs/h/h1540.md) to ['Aššûr](../../strongs/h/h804.md).

<a name="2kings_15_30"></a>2Kings 15:30

And [Hôšēaʿ](../../strongs/h/h1954.md) the [ben](../../strongs/h/h1121.md) of ['Ēlâ](../../strongs/h/h425.md) [qāšar](../../strongs/h/h7194.md) a [qešer](../../strongs/h/h7195.md) against [Peqaḥ](../../strongs/h/h6492.md) the [ben](../../strongs/h/h1121.md) of [Rᵊmalyâû](../../strongs/h/h7425.md), and [nakah](../../strongs/h/h5221.md) him, and [muwth](../../strongs/h/h4191.md) him, and [mālaḵ](../../strongs/h/h4427.md) in his stead, in the twentieth [šānâ](../../strongs/h/h8141.md) of [Yôṯām](../../strongs/h/h3147.md) the [ben](../../strongs/h/h1121.md) of ['Uzziyah](../../strongs/h/h5818.md).

<a name="2kings_15_31"></a>2Kings 15:31

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Peqaḥ](../../strongs/h/h6492.md), and all that he ['asah](../../strongs/h/h6213.md), behold, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2kings_15_32"></a>2Kings 15:32

In the second [šānâ](../../strongs/h/h8141.md) of [Peqaḥ](../../strongs/h/h6492.md) the [ben](../../strongs/h/h1121.md) of [Rᵊmalyâû](../../strongs/h/h7425.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) began [Yôṯām](../../strongs/h/h3147.md) the [ben](../../strongs/h/h1121.md) of ['Uzziyah](../../strongs/h/h5818.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) to [mālaḵ](../../strongs/h/h4427.md).

<a name="2kings_15_33"></a>2Kings 15:33

Five and twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) was he when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) sixteen [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [Yᵊrûšā'](../../strongs/h/h3388.md), the [bath](../../strongs/h/h1323.md) of [Ṣāḏôq](../../strongs/h/h6659.md).

<a name="2kings_15_34"></a>2Kings 15:34

And he ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md): he ['asah](../../strongs/h/h6213.md) according to all that his ['ab](../../strongs/h/h1.md) ['Uzziyah](../../strongs/h/h5818.md) had ['asah](../../strongs/h/h6213.md).

<a name="2kings_15_35"></a>2Kings 15:35

Howbeit the [bāmâ](../../strongs/h/h1116.md) were not [cuwr](../../strongs/h/h5493.md): the ['am](../../strongs/h/h5971.md) [zabach](../../strongs/h/h2076.md) and [qāṭar](../../strongs/h/h6999.md) still in the [bāmâ](../../strongs/h/h1116.md). He [bānâ](../../strongs/h/h1129.md) the ['elyown](../../strongs/h/h5945.md) [sha'ar](../../strongs/h/h8179.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_15_36"></a>2Kings 15:36

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yôṯām](../../strongs/h/h3147.md), and all that he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)?

<a name="2kings_15_37"></a>2Kings 15:37

In those [yowm](../../strongs/h/h3117.md) [Yĕhovah](../../strongs/h/h3068.md) [ḥālal](../../strongs/h/h2490.md) to [shalach](../../strongs/h/h7971.md) against [Yehuwdah](../../strongs/h/h3063.md) [Rᵊṣîn](../../strongs/h/h7526.md) the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md), and [Peqaḥ](../../strongs/h/h6492.md) the [ben](../../strongs/h/h1121.md) of [Rᵊmalyâû](../../strongs/h/h7425.md).

<a name="2kings_15_38"></a>2Kings 15:38

And [Yôṯām](../../strongs/h/h3147.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) with his ['ab](../../strongs/h/h1.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md): and ['Āḥāz](../../strongs/h/h271.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 14](2kings_14.md) - [2Kings 16](2kings_16.md)