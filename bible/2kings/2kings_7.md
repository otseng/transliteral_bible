# [2Kings 7](https://www.blueletterbible.org/kjv/2kings/7)

<a name="2kings_7_1"></a>2Kings 7:1

Then ['Ĕlîšāʿ](../../strongs/h/h477.md) ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) ye the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md); Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), [māḥār](../../strongs/h/h4279.md) about this [ʿēṯ](../../strongs/h/h6256.md) shall a [sᵊ'â](../../strongs/h/h5429.md) of [sōleṯ](../../strongs/h/h5560.md) be sold for a [šeqel](../../strongs/h/h8255.md), and two [sᵊ'â](../../strongs/h/h5429.md) of [śᵊʿōrâ](../../strongs/h/h8184.md) for a [šeqel](../../strongs/h/h8255.md), in the [sha'ar](../../strongs/h/h8179.md) of [Šōmrôn](../../strongs/h/h8111.md).

<a name="2kings_7_2"></a>2Kings 7:2

Then a [šālîš](../../strongs/h/h7991.md) on whose [yad](../../strongs/h/h3027.md) the [melek](../../strongs/h/h4428.md) [šāʿan](../../strongs/h/h8172.md) ['anah](../../strongs/h/h6030.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), and ['āmar](../../strongs/h/h559.md), Behold, if [Yĕhovah](../../strongs/h/h3068.md) would ['asah](../../strongs/h/h6213.md) ['ărubâ](../../strongs/h/h699.md) in [shamayim](../../strongs/h/h8064.md), might this [dabar](../../strongs/h/h1697.md) be? And he ['āmar](../../strongs/h/h559.md), Behold, thou shalt [ra'ah](../../strongs/h/h7200.md) it with thine ['ayin](../../strongs/h/h5869.md), but shalt not ['akal](../../strongs/h/h398.md) thereof.

<a name="2kings_7_3"></a>2Kings 7:3

And there were four [ṣāraʿ](../../strongs/h/h6879.md) ['enowsh](../../strongs/h/h582.md) at the entering [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md): and they ['āmar](../../strongs/h/h559.md) ['iysh](../../strongs/h/h376.md) to [rea'](../../strongs/h/h7453.md), Why [yashab](../../strongs/h/h3427.md) we here until we [muwth](../../strongs/h/h4191.md)?

<a name="2kings_7_4"></a>2Kings 7:4

If we ['āmar](../../strongs/h/h559.md), We will [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md), then the [rāʿāḇ](../../strongs/h/h7458.md) is in the [ʿîr](../../strongs/h/h5892.md), and we shall [muwth](../../strongs/h/h4191.md) there: and if we [yashab](../../strongs/h/h3427.md) here, we [muwth](../../strongs/h/h4191.md) also. Now therefore [yālaḵ](../../strongs/h/h3212.md), and let us [naphal](../../strongs/h/h5307.md) unto the [maḥănê](../../strongs/h/h4264.md) of the ['Ărām](../../strongs/h/h758.md): if they [ḥāyâ](../../strongs/h/h2421.md) us, we shall [ḥāyâ](../../strongs/h/h2421.md); and if they [muwth](../../strongs/h/h4191.md) us, we shall but [muwth](../../strongs/h/h4191.md).

<a name="2kings_7_5"></a>2Kings 7:5

And they [quwm](../../strongs/h/h6965.md) in the [nešep̄](../../strongs/h/h5399.md), to [bow'](../../strongs/h/h935.md) unto the [maḥănê](../../strongs/h/h4264.md) of the ['Ărām](../../strongs/h/h758.md): and when they were [bow'](../../strongs/h/h935.md) to the [qāṣê](../../strongs/h/h7097.md) of the [maḥănê](../../strongs/h/h4264.md) of ['Ărām](../../strongs/h/h758.md), behold, there was no ['iysh](../../strongs/h/h376.md) there.

<a name="2kings_7_6"></a>2Kings 7:6

For the ['adonay](../../strongs/h/h136.md) had made the [maḥănê](../../strongs/h/h4264.md) of the ['Ărām](../../strongs/h/h758.md) to [shama'](../../strongs/h/h8085.md) a [qowl](../../strongs/h/h6963.md) of [reḵeḇ](../../strongs/h/h7393.md), and a [qowl](../../strongs/h/h6963.md) of [sûs](../../strongs/h/h5483.md), even the [qowl](../../strongs/h/h6963.md) of a [gadowl](../../strongs/h/h1419.md) [ḥayil](../../strongs/h/h2428.md): and they ['āmar](../../strongs/h/h559.md) ['iysh](../../strongs/h/h376.md) to ['ach](../../strongs/h/h251.md), Lo, the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) hath [śāḵar](../../strongs/h/h7936.md) against us the [melek](../../strongs/h/h4428.md) of the [Ḥitî](../../strongs/h/h2850.md), and the [melek](../../strongs/h/h4428.md) of the [Mitsrayim](../../strongs/h/h4714.md), to [bow'](../../strongs/h/h935.md) upon us.

<a name="2kings_7_7"></a>2Kings 7:7

Wherefore they [quwm](../../strongs/h/h6965.md) and [nûs](../../strongs/h/h5127.md) in the [nešep̄](../../strongs/h/h5399.md), and ['azab](../../strongs/h/h5800.md) their ['ohel](../../strongs/h/h168.md), and their [sûs](../../strongs/h/h5483.md), and their [chamowr](../../strongs/h/h2543.md), even the [maḥănê](../../strongs/h/h4264.md) as it was, and [nûs](../../strongs/h/h5127.md) for their [nephesh](../../strongs/h/h5315.md).

<a name="2kings_7_8"></a>2Kings 7:8

And when these [ṣāraʿ](../../strongs/h/h6879.md) [bow'](../../strongs/h/h935.md) to the [qāṣê](../../strongs/h/h7097.md) of the [maḥănê](../../strongs/h/h4264.md), they [bow'](../../strongs/h/h935.md) into one ['ohel](../../strongs/h/h168.md), and did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and [nasa'](../../strongs/h/h5375.md) thence [keceph](../../strongs/h/h3701.md), and [zāhāḇ](../../strongs/h/h2091.md), and [beḡeḏ](../../strongs/h/h899.md), and [yālaḵ](../../strongs/h/h3212.md) and [taman](../../strongs/h/h2934.md) it; and [shuwb](../../strongs/h/h7725.md), and [bow'](../../strongs/h/h935.md) into ['aḥēr](../../strongs/h/h312.md) ['ohel](../../strongs/h/h168.md), and [nasa'](../../strongs/h/h5375.md) thence also, and [yālaḵ](../../strongs/h/h3212.md) and [taman](../../strongs/h/h2934.md) it.

<a name="2kings_7_9"></a>2Kings 7:9

Then they ['āmar](../../strongs/h/h559.md) ['iysh](../../strongs/h/h376.md) to [rea'](../../strongs/h/h7453.md), We ['asah](../../strongs/h/h6213.md) not well: this [yowm](../../strongs/h/h3117.md) is a [yowm](../../strongs/h/h3117.md) of [bᵊśôrâ](../../strongs/h/h1309.md), and we [ḥāšâ](../../strongs/h/h2814.md): if we [ḥāḵâ](../../strongs/h/h2442.md) till the [boqer](../../strongs/h/h1242.md) ['owr](../../strongs/h/h216.md), some ['avon](../../strongs/h/h5771.md) will [māṣā'](../../strongs/h/h4672.md) upon us: now therefore [yālaḵ](../../strongs/h/h3212.md), that we may [bow'](../../strongs/h/h935.md) and [nāḡaḏ](../../strongs/h/h5046.md) the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md).

<a name="2kings_7_10"></a>2Kings 7:10

So they [bow'](../../strongs/h/h935.md) and [qara'](../../strongs/h/h7121.md) unto the [šôʿēr](../../strongs/h/h7778.md) of the [ʿîr](../../strongs/h/h5892.md): and they [nāḡaḏ](../../strongs/h/h5046.md) them, ['āmar](../../strongs/h/h559.md), We [bow'](../../strongs/h/h935.md) to the [maḥănê](../../strongs/h/h4264.md) of the ['Ărām](../../strongs/h/h758.md), and, behold, there was no ['iysh](../../strongs/h/h376.md) there, neither [qowl](../../strongs/h/h6963.md) of ['āḏām](../../strongs/h/h120.md), but [sûs](../../strongs/h/h5483.md) ['āsar](../../strongs/h/h631.md), and [chamowr](../../strongs/h/h2543.md) ['āsar](../../strongs/h/h631.md), and the ['ohel](../../strongs/h/h168.md) as they were.

<a name="2kings_7_11"></a>2Kings 7:11

And he [qara'](../../strongs/h/h7121.md) the [šôʿēr](../../strongs/h/h7778.md); and they [nāḡaḏ](../../strongs/h/h5046.md) it to the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md) [pᵊnîmâ](../../strongs/h/h6441.md).

<a name="2kings_7_12"></a>2Kings 7:12

And the [melek](../../strongs/h/h4428.md) [quwm](../../strongs/h/h6965.md) in the [layil](../../strongs/h/h3915.md), and ['āmar](../../strongs/h/h559.md) unto his ['ebed](../../strongs/h/h5650.md), I will now [nāḡaḏ](../../strongs/h/h5046.md) you what the ['Ărām](../../strongs/h/h758.md) have ['asah](../../strongs/h/h6213.md) to us. They [yada'](../../strongs/h/h3045.md) that we be [rāʿēḇ](../../strongs/h/h7457.md); therefore are they [yāṣā'](../../strongs/h/h3318.md) of the [maḥănê](../../strongs/h/h4264.md) to [ḥāḇâ](../../strongs/h/h2247.md) themselves in the [sadeh](../../strongs/h/h7704.md), ['āmar](../../strongs/h/h559.md), When they [yāṣā'](../../strongs/h/h3318.md) of the [ʿîr](../../strongs/h/h5892.md), we shall [tāp̄aś](../../strongs/h/h8610.md) them [chay](../../strongs/h/h2416.md), and [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md).

<a name="2kings_7_13"></a>2Kings 7:13

And one of his ['ebed](../../strongs/h/h5650.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), Let some [laqach](../../strongs/h/h3947.md), I pray thee, five of the [sûs](../../strongs/h/h5483.md) that [šā'ar](../../strongs/h/h7604.md), which are [šā'ar](../../strongs/h/h7604.md) in the city, (behold, they are as all the [hāmôn](../../strongs/h/h1995.md) of [Yisra'el](../../strongs/h/h3478.md) that are [šā'ar](../../strongs/h/h7604.md) in it: behold, I say, they are even as all the [hāmôn](../../strongs/h/h1995.md) of the [Yisra'el](../../strongs/h/h3478.md) that are [tamam](../../strongs/h/h8552.md):) and let us [shalach](../../strongs/h/h7971.md) and [ra'ah](../../strongs/h/h7200.md).

<a name="2kings_7_14"></a>2Kings 7:14

They [laqach](../../strongs/h/h3947.md) therefore two [reḵeḇ](../../strongs/h/h7393.md) [sûs](../../strongs/h/h5483.md); and the [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md) ['aḥar](../../strongs/h/h310.md) the [maḥănê](../../strongs/h/h4264.md) of the ['Ărām](../../strongs/h/h758.md), ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) and [ra'ah](../../strongs/h/h7200.md).

<a name="2kings_7_15"></a>2Kings 7:15

And they [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) them unto [Yardēn](../../strongs/h/h3383.md): and, lo, all the [derek](../../strongs/h/h1870.md) was [mālē'](../../strongs/h/h4392.md) of [beḡeḏ](../../strongs/h/h899.md) and [kĕliy](../../strongs/h/h3627.md), which the ['Ărām](../../strongs/h/h758.md) had [shalak](../../strongs/h/h7993.md) in their [ḥāp̄az](../../strongs/h/h2648.md). And the [mal'ak](../../strongs/h/h4397.md) [shuwb](../../strongs/h/h7725.md), and [nāḡaḏ](../../strongs/h/h5046.md) the [melek](../../strongs/h/h4428.md).

<a name="2kings_7_16"></a>2Kings 7:16

And the ['am](../../strongs/h/h5971.md) [yāṣā'](../../strongs/h/h3318.md), and [bāzaz](../../strongs/h/h962.md) the [maḥănê](../../strongs/h/h4264.md) of the ['Ărām](../../strongs/h/h758.md). So a [sᵊ'â](../../strongs/h/h5429.md) of fine [sōleṯ](../../strongs/h/h5560.md) was sold for a [šeqel](../../strongs/h/h8255.md), and two [sᵊ'â](../../strongs/h/h5429.md) of [śᵊʿōrâ](../../strongs/h/h8184.md) for a [šeqel](../../strongs/h/h8255.md), according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2kings_7_17"></a>2Kings 7:17

And the [melek](../../strongs/h/h4428.md) [paqad](../../strongs/h/h6485.md) the [šālîš](../../strongs/h/h7991.md) on whose [yad](../../strongs/h/h3027.md) he [šāʿan](../../strongs/h/h8172.md) to have the charge of the [sha'ar](../../strongs/h/h8179.md): and the ['am](../../strongs/h/h5971.md) [rāmas](../../strongs/h/h7429.md) upon him in the [sha'ar](../../strongs/h/h8179.md), and he [muwth](../../strongs/h/h4191.md), as the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) had [dabar](../../strongs/h/h1696.md), who [dabar](../../strongs/h/h1696.md) when the [melek](../../strongs/h/h4428.md) [yarad](../../strongs/h/h3381.md) to him.

<a name="2kings_7_18"></a>2Kings 7:18

And it came to pass as the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) had [dabar](../../strongs/h/h1696.md) to the [melek](../../strongs/h/h4428.md), ['āmar](../../strongs/h/h559.md), Two [sᵊ'â](../../strongs/h/h5429.md) of [śᵊʿōrâ](../../strongs/h/h8184.md) for a [šeqel](../../strongs/h/h8255.md), and a [sᵊ'â](../../strongs/h/h5429.md) of [sōleṯ](../../strongs/h/h5560.md) for a [šeqel](../../strongs/h/h8255.md), shall be [māḥār](../../strongs/h/h4279.md) about this [ʿēṯ](../../strongs/h/h6256.md) in the [sha'ar](../../strongs/h/h8179.md) of [Šōmrôn](../../strongs/h/h8111.md):

<a name="2kings_7_19"></a>2Kings 7:19

And that [šālîš](../../strongs/h/h7991.md) ['anah](../../strongs/h/h6030.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), and ['āmar](../../strongs/h/h559.md), Now, behold, if [Yĕhovah](../../strongs/h/h3068.md) should ['asah](../../strongs/h/h6213.md) ['ărubâ](../../strongs/h/h699.md) in [shamayim](../../strongs/h/h8064.md), might such a [dabar](../../strongs/h/h1697.md) be? And he ['āmar](../../strongs/h/h559.md), Behold, thou shalt [ra'ah](../../strongs/h/h7200.md) it with thine ['ayin](../../strongs/h/h5869.md), but shalt not ['akal](../../strongs/h/h398.md) thereof.

<a name="2kings_7_20"></a>2Kings 7:20

And so it fell out unto him: for the ['am](../../strongs/h/h5971.md) [rāmas](../../strongs/h/h7429.md) upon him in the [sha'ar](../../strongs/h/h8179.md), and he [muwth](../../strongs/h/h4191.md).

---

[Transliteral Bible](../bible.md)

[2Kings](2kings.md)

[2Kings 6](2kings_6.md) - [2Kings 8](2kings_8.md)