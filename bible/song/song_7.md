# [Song 7](https://www.blueletterbible.org/kjv/song/7)

<a name="song_7_1"></a>Song 7:1

How [yāp̄â](../../strongs/h/h3302.md) are thy [pa'am](../../strongs/h/h6471.md) with [naʿal](../../strongs/h/h5275.md), O [nāḏîḇ](../../strongs/h/h5081.md) [bath](../../strongs/h/h1323.md)! the [ḥammûq](../../strongs/h/h2542.md) of thy [yārēḵ](../../strongs/h/h3409.md) are like [ḥălî](../../strongs/h/h2481.md), the [ma'aseh](../../strongs/h/h4639.md) of the [yad](../../strongs/h/h3027.md) of an ['āmān](../../strongs/h/h542.md).

<a name="song_7_2"></a>Song 7:2

Thy [šōrer](../../strongs/h/h8326.md) is like a [sahar](../../strongs/h/h5469.md) ['agān](../../strongs/h/h101.md), which [ḥāsēr](../../strongs/h/h2637.md) not [mezeḡ](../../strongs/h/h4197.md): thy [beten](../../strongs/h/h990.md) is like an [ʿărēmâ](../../strongs/h/h6194.md) of [ḥiṭṭâ](../../strongs/h/h2406.md) set [sûḡ](../../strongs/h/h5473.md) with [šûšan](../../strongs/h/h7799.md).

<a name="song_7_3"></a>Song 7:3

Thy two [šaḏ](../../strongs/h/h7699.md) are like two [ʿōp̄er](../../strongs/h/h6082.md) [ṣᵊḇîyâ](../../strongs/h/h6646.md) that are [tᵊ'ômîm](../../strongs/h/h8380.md).

<a name="song_7_4"></a>Song 7:4

Thy [ṣaûā'r](../../strongs/h/h6677.md) is as a [miḡdāl](../../strongs/h/h4026.md) of [šēn](../../strongs/h/h8127.md); thine ['ayin](../../strongs/h/h5869.md) like the [bᵊrēḵâ](../../strongs/h/h1295.md) in [Hešbôn](../../strongs/h/h2809.md), by the [sha'ar](../../strongs/h/h8179.md) of [Baṯ-Rabîm](../../strongs/h/h1337.md): thy ['aph](../../strongs/h/h639.md) is as the [miḡdāl](../../strongs/h/h4026.md) of [Lᵊḇānôn](../../strongs/h/h3844.md) which [tsaphah](../../strongs/h/h6822.md) [paniym](../../strongs/h/h6440.md) [Dammeśeq](../../strongs/h/h1834.md).

<a name="song_7_5"></a>Song 7:5

Thine [ro'sh](../../strongs/h/h7218.md) upon thee is like [Karmel](../../strongs/h/h3760.md), and the [dallâ](../../strongs/h/h1803.md) of thine [ro'sh](../../strongs/h/h7218.md) like ['argāmān](../../strongs/h/h713.md); the [melek](../../strongs/h/h4428.md) is ['āsar](../../strongs/h/h631.md) in the [rahaṭ](../../strongs/h/h7298.md).

<a name="song_7_6"></a>Song 7:6

How [yāp̄â](../../strongs/h/h3302.md) and how [nāʿēm](../../strongs/h/h5276.md) art thou, O ['ahăḇâ](../../strongs/h/h160.md), for [taʿănûḡ](../../strongs/h/h8588.md)!

<a name="song_7_7"></a>Song 7:7

This thy [qômâ](../../strongs/h/h6967.md) is [dāmâ](../../strongs/h/h1819.md) to a palm [tāmār](../../strongs/h/h8558.md), and thy [šaḏ](../../strongs/h/h7699.md) to ['eškōôl](../../strongs/h/h811.md).

<a name="song_7_8"></a>Song 7:8

I ['āmar](../../strongs/h/h559.md), I will [ʿālâ](../../strongs/h/h5927.md) to the [tāmār](../../strongs/h/h8558.md), I will ['āḥaz](../../strongs/h/h270.md) of the [sansînnim](../../strongs/h/h5577.md) thereof: now also thy [šaḏ](../../strongs/h/h7699.md) shall be as ['eškōôl](../../strongs/h/h811.md) of the [gep̄en](../../strongs/h/h1612.md), and the [rêaḥ](../../strongs/h/h7381.md) of thy ['aph](../../strongs/h/h639.md) like [tapûaḥ](../../strongs/h/h8598.md);

<a name="song_7_9"></a>Song 7:9

And the [ḥēḵ](../../strongs/h/h2441.md) like the [towb](../../strongs/h/h2896.md) [yayin](../../strongs/h/h3196.md) for my [dôḏ](../../strongs/h/h1730.md), that [halak](../../strongs/h/h1980.md) down [meyshar](../../strongs/h/h4339.md), causing the [saphah](../../strongs/h/h8193.md) of those that are [yāšēn](../../strongs/h/h3463.md) to [dāḇaḇ](../../strongs/h/h1680.md).

<a name="song_7_10"></a>Song 7:10

I am my [dôḏ](../../strongs/h/h1730.md), and his [tᵊšûqâ](../../strongs/h/h8669.md) is toward me.

<a name="song_7_11"></a>Song 7:11

[yālaḵ](../../strongs/h/h3212.md), my [dôḏ](../../strongs/h/h1730.md), let us [yāṣā'](../../strongs/h/h3318.md) into the [sadeh](../../strongs/h/h7704.md); let us [lûn](../../strongs/h/h3885.md) in the [kāp̄ār](../../strongs/h/h3723.md).

<a name="song_7_12"></a>Song 7:12

Let us [šāḵam](../../strongs/h/h7925.md) to the [kerem](../../strongs/h/h3754.md); let us [ra'ah](../../strongs/h/h7200.md) if the [gep̄en](../../strongs/h/h1612.md) [pāraḥ](../../strongs/h/h6524.md), whether the [sᵊmāḏar](../../strongs/h/h5563.md) [pāṯaḥ](../../strongs/h/h6605.md), and the [rimmôn](../../strongs/h/h7416.md) bud [nûṣ](../../strongs/h/h5132.md): there will I [nathan](../../strongs/h/h5414.md) thee my [dôḏ](../../strongs/h/h1730.md).

<a name="song_7_13"></a>Song 7:13

The [dûḏay](../../strongs/h/h1736.md) [nathan](../../strongs/h/h5414.md) a [rêaḥ](../../strongs/h/h7381.md), and at our [peṯaḥ](../../strongs/h/h6607.md) are all manner of [meḡeḏ](../../strongs/h/h4022.md), [ḥāḏāš](../../strongs/h/h2319.md) and [yāšān](../../strongs/h/h3465.md), which I have [tsaphan](../../strongs/h/h6845.md) for thee, O my [dôḏ](../../strongs/h/h1730.md).

---

[Transliteral Bible](../bible.md)

[Song](song.md)

[Song 6](song_6.md) - [Song 8](song_8.md)