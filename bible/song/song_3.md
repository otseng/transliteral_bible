# [Song 3](https://www.blueletterbible.org/kjv/song/3)

<a name="song_3_1"></a>Song 3:1

By [layil](../../strongs/h/h3915.md) on my [miškāḇ](../../strongs/h/h4904.md) I [bāqaš](../../strongs/h/h1245.md) him whom my [nephesh](../../strongs/h/h5315.md) ['ahab](../../strongs/h/h157.md): I [bāqaš](../../strongs/h/h1245.md) him, but I [māṣā'](../../strongs/h/h4672.md) him not.

<a name="song_3_2"></a>Song 3:2

I will [quwm](../../strongs/h/h6965.md) now, and [cabab](../../strongs/h/h5437.md) the [ʿîr](../../strongs/h/h5892.md) in the [šûq](../../strongs/h/h7784.md), and in the [rᵊḥōḇ](../../strongs/h/h7339.md) I will [bāqaš](../../strongs/h/h1245.md) him whom my [nephesh](../../strongs/h/h5315.md) ['ahab](../../strongs/h/h157.md): I [bāqaš](../../strongs/h/h1245.md) him, but I [māṣā'](../../strongs/h/h4672.md) him not.

<a name="song_3_3"></a>Song 3:3

The [shamar](../../strongs/h/h8104.md) that [cabab](../../strongs/h/h5437.md) the [ʿîr](../../strongs/h/h5892.md) [māṣā'](../../strongs/h/h4672.md) me: to whom I said, [ra'ah](../../strongs/h/h7200.md) ye him whom my [nephesh](../../strongs/h/h5315.md) ['ahab](../../strongs/h/h157.md)?

<a name="song_3_4"></a>Song 3:4

It was but a [mᵊʿaṭ](../../strongs/h/h4592.md) that I ['abar](../../strongs/h/h5674.md) from them, but I [māṣā'](../../strongs/h/h4672.md) him whom my [nephesh](../../strongs/h/h5315.md) ['ahab](../../strongs/h/h157.md): I ['āḥaz](../../strongs/h/h270.md) him, and would not let him [rāp̄â](../../strongs/h/h7503.md), until I had [bow'](../../strongs/h/h935.md) him into my ['em](../../strongs/h/h517.md) [bayith](../../strongs/h/h1004.md), and into the [ḥeḏer](../../strongs/h/h2315.md) of her that [harah](../../strongs/h/h2029.md) me.

<a name="song_3_5"></a>Song 3:5

I [shaba'](../../strongs/h/h7650.md) you, O ye [bath](../../strongs/h/h1323.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), by the [ṣᵊḇî](../../strongs/h/h6643.md), and by the ['ayyālâ](../../strongs/h/h355.md) of the [sadeh](../../strongs/h/h7704.md), that ye [ʿûr](../../strongs/h/h5782.md) not, nor [ʿûr](../../strongs/h/h5782.md) my ['ahăḇâ](../../strongs/h/h160.md), till he [ḥāp̄ēṣ](../../strongs/h/h2654.md).

<a name="song_3_6"></a>Song 3:6

Who is this that [ʿālâ](../../strongs/h/h5927.md) out of the [midbar](../../strongs/h/h4057.md) like [tîmārâ](../../strongs/h/h8490.md) of ['ashan](../../strongs/h/h6227.md), [qāṭar](../../strongs/h/h6999.md) with [mōr](../../strongs/h/h4753.md) and [lᵊḇônâ](../../strongs/h/h3828.md), with all ['ăḇāqâ](../../strongs/h/h81.md) of the [rāḵal](../../strongs/h/h7402.md)?

<a name="song_3_7"></a>Song 3:7

Behold his [mittah](../../strongs/h/h4296.md), which is [Šᵊlōmô](../../strongs/h/h8010.md); threescore [gibôr](../../strongs/h/h1368.md) men are [cabiyb](../../strongs/h/h5439.md) it, of the [gibôr](../../strongs/h/h1368.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="song_3_8"></a>Song 3:8

They all ['āḥaz](../../strongs/h/h270.md) [chereb](../../strongs/h/h2719.md), being [lamad](../../strongs/h/h3925.md) in [milḥāmâ](../../strongs/h/h4421.md): every ['iysh](../../strongs/h/h376.md) hath his [chereb](../../strongs/h/h2719.md) upon his [yārēḵ](../../strongs/h/h3409.md) because of [paḥaḏ](../../strongs/h/h6343.md) in the [layil](../../strongs/h/h3915.md).

<a name="song_3_9"></a>Song 3:9

[melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) himself an ['apiryôn](../../strongs/h/h668.md) of the ['ets](../../strongs/h/h6086.md) of [Lᵊḇānôn](../../strongs/h/h3844.md).

<a name="song_3_10"></a>Song 3:10

He ['asah](../../strongs/h/h6213.md) the [ʿammûḏ](../../strongs/h/h5982.md) thereof of [keceph](../../strongs/h/h3701.md), the [rᵊp̄îḏâ](../../strongs/h/h7507.md) thereof of [zāhāḇ](../../strongs/h/h2091.md), the [merkāḇ](../../strongs/h/h4817.md) of it of ['argāmān](../../strongs/h/h713.md), the midst thereof being [rāṣap̄](../../strongs/h/h7528.md) with ['ahăḇâ](../../strongs/h/h160.md), for the [bath](../../strongs/h/h1323.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="song_3_11"></a>Song 3:11

[yāṣā'](../../strongs/h/h3318.md), O ye [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md), and [ra'ah](../../strongs/h/h7200.md) [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) with the [ʿăṭārâ](../../strongs/h/h5850.md) wherewith his ['em](../../strongs/h/h517.md) ['atar](../../strongs/h/h5849.md) him in the [yowm](../../strongs/h/h3117.md) of his [ḥăṯunnâ](../../strongs/h/h2861.md), and in the [yowm](../../strongs/h/h3117.md) of the [simchah](../../strongs/h/h8057.md) of his [leb](../../strongs/h/h3820.md).

---

[Transliteral Bible](../bible.md)

[Song](song.md)

[Song 2](song_2.md) - [Song 4](song_4.md)