# [Song 8](https://www.blueletterbible.org/kjv/song/8)

<a name="song_8_1"></a>Song 8:1

O [nathan](../../strongs/h/h5414.md) thou wert as my ['ach](../../strongs/h/h251.md), that [yānaq](../../strongs/h/h3243.md) the [šaḏ](../../strongs/h/h7699.md) of my ['em](../../strongs/h/h517.md)! when I should [māṣā'](../../strongs/h/h4672.md) thee [ḥûṣ](../../strongs/h/h2351.md), I would [nashaq](../../strongs/h/h5401.md) thee; yea, I should not be [bûz](../../strongs/h/h936.md).

<a name="song_8_2"></a>Song 8:2

I would [nāhaḡ](../../strongs/h/h5090.md) thee, and [bow'](../../strongs/h/h935.md) thee into my ['em](../../strongs/h/h517.md) [bayith](../../strongs/h/h1004.md), who would [lamad](../../strongs/h/h3925.md) me: I would cause thee to [šāqâ](../../strongs/h/h8248.md) of [reqaḥ](../../strongs/h/h7544.md) [yayin](../../strongs/h/h3196.md) of the [ʿāsîs](../../strongs/h/h6071.md) of my [rimmôn](../../strongs/h/h7416.md).

<a name="song_8_3"></a>Song 8:3

His [śᵊmō'l](../../strongs/h/h8040.md) should be under my [ro'sh](../../strongs/h/h7218.md), and his [yamiyn](../../strongs/h/h3225.md) should [ḥāḇaq](../../strongs/h/h2263.md) me.

<a name="song_8_4"></a>Song 8:4

I [shaba'](../../strongs/h/h7650.md) you, O [bath](../../strongs/h/h1323.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), that ye stir not [ʿûr](../../strongs/h/h5782.md), nor [ʿûr](../../strongs/h/h5782.md) my ['ahăḇâ](../../strongs/h/h160.md), until he [ḥāp̄ēṣ](../../strongs/h/h2654.md).

<a name="song_8_5"></a>Song 8:5

Who is this that [ʿālâ](../../strongs/h/h5927.md) from the [midbar](../../strongs/h/h4057.md), [rāp̄aq](../../strongs/h/h7514.md) upon her [dôḏ](../../strongs/h/h1730.md)? I [ʿûr](../../strongs/h/h5782.md) thee up under the apple [tapûaḥ](../../strongs/h/h8598.md): there thy ['em](../../strongs/h/h517.md) brought thee [chabal](../../strongs/h/h2254.md): there she brought thee [chabal](../../strongs/h/h2254.md) that [yalad](../../strongs/h/h3205.md) thee.

<a name="song_8_6"></a>Song 8:6

[śûm](../../strongs/h/h7760.md) me as a [ḥôṯām](../../strongs/h/h2368.md) upon thine [leb](../../strongs/h/h3820.md), as a [ḥôṯām](../../strongs/h/h2368.md) upon thine [zerowa'](../../strongs/h/h2220.md): for ['ahăḇâ](../../strongs/h/h160.md) is ['az](../../strongs/h/h5794.md) as [maveth](../../strongs/h/h4194.md); [qin'â](../../strongs/h/h7068.md) is [qāšê](../../strongs/h/h7186.md) as the [shĕ'owl](../../strongs/h/h7585.md): the [rešep̄](../../strongs/h/h7565.md) thereof are [rešep̄](../../strongs/h/h7565.md) of ['esh](../../strongs/h/h784.md), which hath a [šalheḇeṯ](../../strongs/h/h7957.md).

<a name="song_8_7"></a>Song 8:7

[rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md) [yakol](../../strongs/h/h3201.md) [kāḇâ](../../strongs/h/h3518.md) ['ahăḇâ](../../strongs/h/h160.md), neither can the [nāhār](../../strongs/h/h5104.md) [šāṭap̄](../../strongs/h/h7857.md) it: if an ['iysh](../../strongs/h/h376.md) would [nathan](../../strongs/h/h5414.md) all the [hôn](../../strongs/h/h1952.md) of his [bayith](../../strongs/h/h1004.md) for ['ahăḇâ](../../strongs/h/h160.md), it would [bûz](../../strongs/h/h936.md) be [bûz](../../strongs/h/h936.md).

<a name="song_8_8"></a>Song 8:8

We have a [qāṭān](../../strongs/h/h6996.md) ['āḥôṯ](../../strongs/h/h269.md), and she hath no [šaḏ](../../strongs/h/h7699.md): what shall we ['asah](../../strongs/h/h6213.md) for our ['āḥôṯ](../../strongs/h/h269.md) in the [yowm](../../strongs/h/h3117.md) when she shall be [dabar](../../strongs/h/h1696.md)?

<a name="song_8_9"></a>Song 8:9

If she be a [ḥômâ](../../strongs/h/h2346.md), we will [bānâ](../../strongs/h/h1129.md) upon her a [ṭîrâ](../../strongs/h/h2918.md) of [keceph](../../strongs/h/h3701.md): and if she be a [deleṯ](../../strongs/h/h1817.md), we will [ṣûr](../../strongs/h/h6696.md) her with [lûaḥ](../../strongs/h/h3871.md) of ['erez](../../strongs/h/h730.md).

<a name="song_8_10"></a>Song 8:10

I am a [ḥômâ](../../strongs/h/h2346.md), and my [šaḏ](../../strongs/h/h7699.md) like [miḡdāl](../../strongs/h/h4026.md): then was I in his ['ayin](../../strongs/h/h5869.md) as one that [māṣā'](../../strongs/h/h4672.md) [shalowm](../../strongs/h/h7965.md).

<a name="song_8_11"></a>Song 8:11

[Šᵊlōmô](../../strongs/h/h8010.md) had a [kerem](../../strongs/h/h3754.md) at [BaʿAl Hāmôn](../../strongs/h/h1174.md) ; he let [nathan](../../strongs/h/h5414.md) the [kerem](../../strongs/h/h3754.md) unto [nāṭar](../../strongs/h/h5201.md); every ['iysh](../../strongs/h/h376.md) for the [pĕriy](../../strongs/h/h6529.md) thereof was to [bow'](../../strongs/h/h935.md) a thousand pieces of [keceph](../../strongs/h/h3701.md).

<a name="song_8_12"></a>Song 8:12

My [kerem](../../strongs/h/h3754.md), which is mine, is [paniym](../../strongs/h/h6440.md) me: thou, O [Šᵊlōmô](../../strongs/h/h8010.md), must have a thousand, and those that [nāṭar](../../strongs/h/h5201.md) the [pĕriy](../../strongs/h/h6529.md) thereof two hundred.

<a name="song_8_13"></a>Song 8:13

Thou that [yashab](../../strongs/h/h3427.md) in the [gan](../../strongs/h/h1588.md), the [ḥāḇēr](../../strongs/h/h2270.md) [qashab](../../strongs/h/h7181.md) to thy [qowl](../../strongs/h/h6963.md): cause me to [shama'](../../strongs/h/h8085.md) it.

<a name="song_8_14"></a>Song 8:14

Make [bāraḥ](../../strongs/h/h1272.md), my [dôḏ](../../strongs/h/h1730.md), and be thou [dāmâ](../../strongs/h/h1819.md) to a [ṣᵊḇî](../../strongs/h/h6643.md) or to a [ʿōp̄er](../../strongs/h/h6082.md) ['ayyāl](../../strongs/h/h354.md) upon the [har](../../strongs/h/h2022.md) of [beśem](../../strongs/h/h1314.md).

---

[Transliteral Bible](../bible.md)

[Song](song.md)

[Song 7](song_7.md) - [Song 9](song_9.md)