# [Song 2](https://www.blueletterbible.org/kjv/song/2)

<a name="song_2_1"></a>Song 2:1

I am the [ḥăḇaṣṣeleṯ](../../strongs/h/h2261.md) of [Šārôn](../../strongs/h/h8289.md), and the [šûšan](../../strongs/h/h7799.md) of the [ʿēmeq](../../strongs/h/h6010.md).

<a name="song_2_2"></a>Song 2:2

As the [šûšan](../../strongs/h/h7799.md) among [ḥôaḥ](../../strongs/h/h2336.md), so is my [raʿyâ](../../strongs/h/h7474.md) among the [bath](../../strongs/h/h1323.md).

<a name="song_2_3"></a>Song 2:3

As the apple [tapûaḥ](../../strongs/h/h8598.md) among the ['ets](../../strongs/h/h6086.md) of the [yaʿar](../../strongs/h/h3293.md), so is my [dôḏ](../../strongs/h/h1730.md) among the [ben](../../strongs/h/h1121.md). I [yashab](../../strongs/h/h3427.md) down under his [ṣēl](../../strongs/h/h6738.md) with [chamad](../../strongs/h/h2530.md), and his [pĕriy](../../strongs/h/h6529.md) was [māṯôq](../../strongs/h/h4966.md) to my [ḥēḵ](../../strongs/h/h2441.md).

<a name="song_2_4"></a>Song 2:4

He [bow'](../../strongs/h/h935.md) me to the [yayin](../../strongs/h/h3196.md) [bayith](../../strongs/h/h1004.md), and his [deḡel](../../strongs/h/h1714.md) over me was ['ahăḇâ](../../strongs/h/h160.md).

<a name="song_2_5"></a>Song 2:5

[camak](../../strongs/h/h5564.md) me with ['ăšîšâ](../../strongs/h/h809.md), [rāp̄aḏ](../../strongs/h/h7502.md) me with [tapûaḥ](../../strongs/h/h8598.md): for I am [ḥālâ](../../strongs/h/h2470.md) of ['ahăḇâ](../../strongs/h/h160.md).

<a name="song_2_6"></a>Song 2:6

His [śᵊmō'l](../../strongs/h/h8040.md) is under my [ro'sh](../../strongs/h/h7218.md), and his [yamiyn](../../strongs/h/h3225.md) doth [ḥāḇaq](../../strongs/h/h2263.md) me.

<a name="song_2_7"></a>Song 2:7

I [shaba'](../../strongs/h/h7650.md) you, O ye [bath](../../strongs/h/h1323.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), by the [ṣᵊḇî](../../strongs/h/h6643.md), and by the ['ayyālâ](../../strongs/h/h355.md) of the [sadeh](../../strongs/h/h7704.md), that ye stir not [ʿûr](../../strongs/h/h5782.md), nor [ʿûr](../../strongs/h/h5782.md) my ['ahăḇâ](../../strongs/h/h160.md), till he [ḥāp̄ēṣ](../../strongs/h/h2654.md).

<a name="song_2_8"></a>Song 2:8

The [qowl](../../strongs/h/h6963.md) of my [dôḏ](../../strongs/h/h1730.md)! behold, he [bow'](../../strongs/h/h935.md) [dālaḡ](../../strongs/h/h1801.md) upon the [har](../../strongs/h/h2022.md), [qāp̄aṣ](../../strongs/h/h7092.md) upon the [giḇʿâ](../../strongs/h/h1389.md).

<a name="song_2_9"></a>Song 2:9

My [dôḏ](../../strongs/h/h1730.md) is [dāmâ](../../strongs/h/h1819.md) a [ṣᵊḇî](../../strongs/h/h6643.md) or a [ʿōp̄er](../../strongs/h/h6082.md) ['ayyāl](../../strongs/h/h354.md): behold, he ['amad](../../strongs/h/h5975.md) ['aḥar](../../strongs/h/h310.md) our [kōṯel](../../strongs/h/h3796.md), he [šāḡaḥ](../../strongs/h/h7688.md) at the [ḥallôn](../../strongs/h/h2474.md), [tsuwts](../../strongs/h/h6692.md) himself through the [ḥărakîm](../../strongs/h/h2762.md).

<a name="song_2_10"></a>Song 2:10

My [dôḏ](../../strongs/h/h1730.md) ['anah](../../strongs/h/h6030.md), and ['āmar](../../strongs/h/h559.md) unto me, [quwm](../../strongs/h/h6965.md), my [raʿyâ](../../strongs/h/h7474.md), my [yāp̄ê](../../strongs/h/h3303.md), and [yālaḵ](../../strongs/h/h3212.md).

<a name="song_2_11"></a>Song 2:11

For, lo, the [sᵊṯāv](../../strongs/h/h5638.md) is ['abar](../../strongs/h/h5674.md), the [gešem](../../strongs/h/h1653.md) is [ḥālap̄](../../strongs/h/h2498.md) and [halak](../../strongs/h/h1980.md);

<a name="song_2_12"></a>Song 2:12

The [niṣṣān](../../strongs/h/h5339.md) [ra'ah](../../strongs/h/h7200.md) on the ['erets](../../strongs/h/h776.md); the [ʿēṯ](../../strongs/h/h6256.md) of the [zᵊmîr](../../strongs/h/h2158.md) of birds is [naga'](../../strongs/h/h5060.md), and the [qowl](../../strongs/h/h6963.md) of the [tôr](../../strongs/h/h8449.md) is [shama'](../../strongs/h/h8085.md) in our ['erets](../../strongs/h/h776.md);

<a name="song_2_13"></a>Song 2:13

The [tĕ'en](../../strongs/h/h8384.md) putteth [ḥānaṭ](../../strongs/h/h2590.md) her green [paḡ](../../strongs/h/h6291.md), and the [gep̄en](../../strongs/h/h1612.md) with the tender [sᵊmāḏar](../../strongs/h/h5563.md) [nathan](../../strongs/h/h5414.md) a [rêaḥ](../../strongs/h/h7381.md). [quwm](../../strongs/h/h6965.md), my [raʿyâ](../../strongs/h/h7474.md), my [yāp̄ê](../../strongs/h/h3303.md), and come [yālaḵ](../../strongs/h/h3212.md).

<a name="song_2_14"></a>Song 2:14

O my [yônâ](../../strongs/h/h3123.md), that art in the [ḥăḡāv](../../strongs/h/h2288.md) of the [cela'](../../strongs/h/h5553.md), in the [cether](../../strongs/h/h5643.md) places of the [maḏrēḡâ](../../strongs/h/h4095.md), let me [ra'ah](../../strongs/h/h7200.md) thy [mar'ê](../../strongs/h/h4758.md), let me [shama'](../../strongs/h/h8085.md) thy [qowl](../../strongs/h/h6963.md); for [ʿārēḇ](../../strongs/h/h6156.md) is thy [qowl](../../strongs/h/h6963.md), and thy [mar'ê](../../strongs/h/h4758.md) is [nā'vê](../../strongs/h/h5000.md).

<a name="song_2_15"></a>Song 2:15

['āḥaz](../../strongs/h/h270.md) us the [šûʿāl](../../strongs/h/h7776.md), the [qāṭān](../../strongs/h/h6996.md) [šûʿāl](../../strongs/h/h7776.md), that [chabal](../../strongs/h/h2254.md) the [kerem](../../strongs/h/h3754.md): for our [kerem](../../strongs/h/h3754.md) have [sᵊmāḏar](../../strongs/h/h5563.md).

<a name="song_2_16"></a>Song 2:16

My [dôḏ](../../strongs/h/h1730.md) is mine, and I am his: he [ra'ah](../../strongs/h/h7462.md) among the [šûšan](../../strongs/h/h7799.md).

<a name="song_2_17"></a>Song 2:17

Until the [yowm](../../strongs/h/h3117.md) [puwach](../../strongs/h/h6315.md), and the [ṣl](../../strongs/h/h6752.md) [nûs](../../strongs/h/h5127.md), [cabab](../../strongs/h/h5437.md), my [dôḏ](../../strongs/h/h1730.md), and be thou [dāmâ](../../strongs/h/h1819.md) a [ṣᵊḇî](../../strongs/h/h6643.md) or a [ʿōp̄er](../../strongs/h/h6082.md) ['ayyāl](../../strongs/h/h354.md) upon the [har](../../strongs/h/h2022.md) of [Beṯer](../../strongs/h/h1336.md).

---

[Transliteral Bible](../bible.md)

[Song](song.md)

[Song 1](song_1.md) - [Song 3](song_3.md)