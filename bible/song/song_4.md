# [Song 4](https://www.blueletterbible.org/kjv/song/4)

<a name="song_4_1"></a>Song 4:1

Behold, thou art [yāp̄ê](../../strongs/h/h3303.md), my [raʿyâ](../../strongs/h/h7474.md); behold, thou art [yāp̄ê](../../strongs/h/h3303.md); thou hast [yônâ](../../strongs/h/h3123.md) ['ayin](../../strongs/h/h5869.md) within thy [ṣammâ](../../strongs/h/h6777.md): thy [śēʿār](../../strongs/h/h8181.md) is as a [ʿēḏer](../../strongs/h/h5739.md) of [ʿēz](../../strongs/h/h5795.md), that [gālaš](../../strongs/h/h1570.md) from [har](../../strongs/h/h2022.md) [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="song_4_2"></a>Song 4:2

Thy [šēn](../../strongs/h/h8127.md) are like a [ʿēḏer](../../strongs/h/h5739.md) [qāṣaḇ](../../strongs/h/h7094.md), which [ʿālâ](../../strongs/h/h5927.md) from the [raḥṣâ](../../strongs/h/h7367.md); whereof every one [tā'am](../../strongs/h/h8382.md), and none is [šakûl](../../strongs/h/h7909.md) among them.

<a name="song_4_3"></a>Song 4:3

Thy [saphah](../../strongs/h/h8193.md) are like a [ḥûṭ](../../strongs/h/h2339.md) of [šānî](../../strongs/h/h8144.md), and thy [midbar](../../strongs/h/h4057.md) is [nā'vê](../../strongs/h/h5000.md): thy [raqqâ](../../strongs/h/h7541.md) are like a [pelaḥ](../../strongs/h/h6400.md) of a [rimmôn](../../strongs/h/h7416.md) within thy [ṣammâ](../../strongs/h/h6777.md).

<a name="song_4_4"></a>Song 4:4

Thy [ṣaûā'r](../../strongs/h/h6677.md) is like the [miḡdāl](../../strongs/h/h4026.md) of [Dāviḏ](../../strongs/h/h1732.md) [bānâ](../../strongs/h/h1129.md) for a [talpîyôṯ](../../strongs/h/h8530.md), whereon there [tālâ](../../strongs/h/h8518.md) a thousand [magen](../../strongs/h/h4043.md), all [šeleṭ](../../strongs/h/h7982.md) of [gibôr](../../strongs/h/h1368.md).

<a name="song_4_5"></a>Song 4:5

Thy two [šaḏ](../../strongs/h/h7699.md) are like two [ʿōp̄er](../../strongs/h/h6082.md) [ṣᵊḇîyâ](../../strongs/h/h6646.md) that are [tᵊ'ômîm](../../strongs/h/h8380.md), which [ra'ah](../../strongs/h/h7462.md) among the [šûšan](../../strongs/h/h7799.md).

<a name="song_4_6"></a>Song 4:6

Until the [yowm](../../strongs/h/h3117.md) [puwach](../../strongs/h/h6315.md), and the [ṣl](../../strongs/h/h6752.md) flee [nûs](../../strongs/h/h5127.md), I will [yālaḵ](../../strongs/h/h3212.md) me to the [har](../../strongs/h/h2022.md) of [mōr](../../strongs/h/h4753.md), and to the [giḇʿâ](../../strongs/h/h1389.md) of [lᵊḇônâ](../../strongs/h/h3828.md).

<a name="song_4_7"></a>Song 4:7

Thou art all [yāp̄ê](../../strongs/h/h3303.md), my [raʿyâ](../../strongs/h/h7474.md); there is no [mᵊ'ûm](../../strongs/h/h3971.md) in thee.

<a name="song_4_8"></a>Song 4:8

[bow'](../../strongs/h/h935.md) with me from [Lᵊḇānôn](../../strongs/h/h3844.md), my [kallâ](../../strongs/h/h3618.md), with me from [Lᵊḇānôn](../../strongs/h/h3844.md): [šûr](../../strongs/h/h7789.md) from the [ro'sh](../../strongs/h/h7218.md) of ['Ămānâ](../../strongs/h/h549.md), from the [ro'sh](../../strongs/h/h7218.md) of [Śᵊnîr](../../strongs/h/h8149.md) and [Ḥermôn](../../strongs/h/h2768.md), from the ['ariy](../../strongs/h/h738.md) [mᵊʿônâ](../../strongs/h/h4585.md), from the [har](../../strongs/h/h2042.md) of the [nāmēr](../../strongs/h/h5246.md).

<a name="song_4_9"></a>Song 4:9

Thou hast [lāḇaḇ](../../strongs/h/h3823.md), my ['āḥôṯ](../../strongs/h/h269.md), my [kallâ](../../strongs/h/h3618.md); thou hast [lāḇaḇ](../../strongs/h/h3823.md) with one of thine ['ayin](../../strongs/h/h5869.md), with one [ʿĂnāq](../../strongs/h/h6060.md) of thy [ṣaûā'r](../../strongs/h/h6677.md).

<a name="song_4_10"></a>Song 4:10

How [yāp̄â](../../strongs/h/h3302.md) is thy [dôḏ](../../strongs/h/h1730.md), my ['āḥôṯ](../../strongs/h/h269.md), my [kallâ](../../strongs/h/h3618.md)! how much [ṭôḇ](../../strongs/h/h2895.md) is thy [dôḏ](../../strongs/h/h1730.md) than [yayin](../../strongs/h/h3196.md)! and the [rêaḥ](../../strongs/h/h7381.md) of thine [šemen](../../strongs/h/h8081.md) than all [beśem](../../strongs/h/h1314.md)!

<a name="song_4_11"></a>Song 4:11

Thy [saphah](../../strongs/h/h8193.md), O my [kallâ](../../strongs/h/h3618.md), [nāṭap̄](../../strongs/h/h5197.md) as the [nōp̄eṯ](../../strongs/h/h5317.md): [dĕbash](../../strongs/h/h1706.md) and [chalab](../../strongs/h/h2461.md) are under thy [lashown](../../strongs/h/h3956.md); and the [rêaḥ](../../strongs/h/h7381.md) of thy [śalmâ](../../strongs/h/h8008.md) is like the [rêaḥ](../../strongs/h/h7381.md) of [Lᵊḇānôn](../../strongs/h/h3844.md).

<a name="song_4_12"></a>Song 4:12

A [gan](../../strongs/h/h1588.md) [nāʿal](../../strongs/h/h5274.md) is my ['āḥôṯ](../../strongs/h/h269.md), my [kallâ](../../strongs/h/h3618.md); a [gal](../../strongs/h/h1530.md) shut [nāʿal](../../strongs/h/h5274.md), a [maʿyān](../../strongs/h/h4599.md) [ḥāṯam](../../strongs/h/h2856.md).

<a name="song_4_13"></a>Song 4:13

Thy [Šelaḥ](../../strongs/h/h7973.md) are a [pardēs](../../strongs/h/h6508.md) of [rimmôn](../../strongs/h/h7416.md), with [meḡeḏ](../../strongs/h/h4022.md) [pĕriy](../../strongs/h/h6529.md); [kōp̄er](../../strongs/h/h3724.md), with [nērdᵊ](../../strongs/h/h5373.md),

<a name="song_4_14"></a>Song 4:14

[nērdᵊ](../../strongs/h/h5373.md) and [karkōm](../../strongs/h/h3750.md); [qānê](../../strongs/h/h7070.md) and [qinnāmôn](../../strongs/h/h7076.md), with all ['ets](../../strongs/h/h6086.md) of [lᵊḇônâ](../../strongs/h/h3828.md); [mōr](../../strongs/h/h4753.md) and ['ăhālîm](../../strongs/h/h174.md), with all the [ro'sh](../../strongs/h/h7218.md) [beśem](../../strongs/h/h1314.md):

<a name="song_4_15"></a>Song 4:15

A [maʿyān](../../strongs/h/h4599.md) of [gan](../../strongs/h/h1588.md), a [bᵊ'ēr](../../strongs/h/h875.md) of [chay](../../strongs/h/h2416.md) [mayim](../../strongs/h/h4325.md), and [nāzal](../../strongs/h/h5140.md) from [Lᵊḇānôn](../../strongs/h/h3844.md).

<a name="song_4_16"></a>Song 4:16

[ʿûr](../../strongs/h/h5782.md), O [ṣāp̄ôn](../../strongs/h/h6828.md); and [bow'](../../strongs/h/h935.md), thou [têmān](../../strongs/h/h8486.md); [puwach](../../strongs/h/h6315.md) upon my [gan](../../strongs/h/h1588.md), that the [beśem](../../strongs/h/h1314.md) thereof may [nāzal](../../strongs/h/h5140.md). Let my [dôḏ](../../strongs/h/h1730.md) [bow'](../../strongs/h/h935.md) into his [gan](../../strongs/h/h1588.md), and ['akal](../../strongs/h/h398.md) his [meḡeḏ](../../strongs/h/h4022.md) [pĕriy](../../strongs/h/h6529.md).

---

[Transliteral Bible](../bible.md)

[Song](song.md)

[Song 3](song_3.md) - [Song 5](song_5.md)