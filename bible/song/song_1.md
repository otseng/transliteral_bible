# [Song 1](https://www.blueletterbible.org/kjv/song/1)

<a name="song_1_1"></a>Song 1:1

The [šîr](../../strongs/h/h7892.md) of [šîr](../../strongs/h/h7892.md), which is [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="song_1_2"></a>Song 1:2

Let him [nashaq](../../strongs/h/h5401.md) me with the [nᵊšîqâ](../../strongs/h/h5390.md) of his [peh](../../strongs/h/h6310.md): for thy [dôḏ](../../strongs/h/h1730.md) is [towb](../../strongs/h/h2896.md) than [yayin](../../strongs/h/h3196.md).

<a name="song_1_3"></a>Song 1:3

Because of the [rêaḥ](../../strongs/h/h7381.md) of thy [towb](../../strongs/h/h2896.md) [šemen](../../strongs/h/h8081.md) thy [shem](../../strongs/h/h8034.md) is as [šemen](../../strongs/h/h8081.md) [rîq](../../strongs/h/h7324.md), therefore do the [ʿalmâ](../../strongs/h/h5959.md) ['ahab](../../strongs/h/h157.md) thee.

<a name="song_1_4"></a>Song 1:4

[mashak](../../strongs/h/h4900.md) me, we will [rûṣ](../../strongs/h/h7323.md) ['aḥar](../../strongs/h/h310.md) thee: the [melek](../../strongs/h/h4428.md) hath [bow'](../../strongs/h/h935.md) me into his [ḥeḏer](../../strongs/h/h2315.md): we will be [giyl](../../strongs/h/h1523.md) and [samach](../../strongs/h/h8055.md) in thee, we will [zakar](../../strongs/h/h2142.md) thy [dôḏ](../../strongs/h/h1730.md) more than [yayin](../../strongs/h/h3196.md): the [meyshar](../../strongs/h/h4339.md) ['ahab](../../strongs/h/h157.md) thee.

<a name="song_1_5"></a>Song 1:5

I am [šāḥōr](../../strongs/h/h7838.md), but [nā'vê](../../strongs/h/h5000.md), O ye [bath](../../strongs/h/h1323.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), as the ['ohel](../../strongs/h/h168.md) of [Qēḏār](../../strongs/h/h6938.md), as the [yᵊrîʿâ](../../strongs/h/h3407.md) of [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="song_1_6"></a>Song 1:6

[ra'ah](../../strongs/h/h7200.md) not upon me, because I am [šᵊḥarḥōr](../../strongs/h/h7840.md), because the [šemeš](../../strongs/h/h8121.md) hath [šāzap̄](../../strongs/h/h7805.md) upon me: my ['em](../../strongs/h/h517.md) [ben](../../strongs/h/h1121.md) were [ḥārar](../../strongs/h/h2787.md) with me; they [śûm](../../strongs/h/h7760.md) me the [nāṭar](../../strongs/h/h5201.md) of the [kerem](../../strongs/h/h3754.md); but mine own [kerem](../../strongs/h/h3754.md) have I not [nāṭar](../../strongs/h/h5201.md).

<a name="song_1_7"></a>Song 1:7

[nāḡaḏ](../../strongs/h/h5046.md) me, O thou whom my [nephesh](../../strongs/h/h5315.md) ['ahab](../../strongs/h/h157.md), where thou [ra'ah](../../strongs/h/h7462.md), where thou [rāḇaṣ](../../strongs/h/h7257.md) at [ṣōhar](../../strongs/h/h6672.md): for why should I be as one that [ʿāṭâ](../../strongs/h/h5844.md) by the [ʿēḏer](../../strongs/h/h5739.md) of thy [ḥāḇēr](../../strongs/h/h2270.md)?

<a name="song_1_8"></a>Song 1:8

If thou [yada'](../../strongs/h/h3045.md) not, O thou [yāp̄ê](../../strongs/h/h3303.md) among ['ishshah](../../strongs/h/h802.md), [yāṣā'](../../strongs/h/h3318.md) by the ['aqeb](../../strongs/h/h6119.md) of the [tso'n](../../strongs/h/h6629.md), and [ra'ah](../../strongs/h/h7462.md) thy [gᵊḏîyâ](../../strongs/h/h1429.md) beside the [ra'ah](../../strongs/h/h7462.md) [miškān](../../strongs/h/h4908.md).

<a name="song_1_9"></a>Song 1:9

I have [dāmâ](../../strongs/h/h1819.md) thee, O my [raʿyâ](../../strongs/h/h7474.md), to [sûsâ](../../strongs/h/h5484.md) in [Parʿô](../../strongs/h/h6547.md) [reḵeḇ](../../strongs/h/h7393.md).

<a name="song_1_10"></a>Song 1:10

Thy [lᵊḥî](../../strongs/h/h3895.md) are [nā'â](../../strongs/h/h4998.md) with [tôr](../../strongs/h/h8447.md), thy [ṣaûā'r](../../strongs/h/h6677.md) with [ḥărûzîm](../../strongs/h/h2737.md).

<a name="song_1_11"></a>Song 1:11

We will ['asah](../../strongs/h/h6213.md) thee [tôr](../../strongs/h/h8447.md) of [zāhāḇ](../../strongs/h/h2091.md) with [nᵊqudâ](../../strongs/h/h5351.md) of [keceph](../../strongs/h/h3701.md).

<a name="song_1_12"></a>Song 1:12

While the [melek](../../strongs/h/h4428.md) [mēsaḇ](../../strongs/h/h4524.md), my [nērdᵊ](../../strongs/h/h5373.md) [nathan](../../strongs/h/h5414.md) the [rêaḥ](../../strongs/h/h7381.md) thereof.

<a name="song_1_13"></a>Song 1:13

A [ṣᵊrôr](../../strongs/h/h6872.md) of [mōr](../../strongs/h/h4753.md) is my [dôḏ](../../strongs/h/h1730.md) unto me; he shall [lûn](../../strongs/h/h3885.md) my [šaḏ](../../strongs/h/h7699.md).

<a name="song_1_14"></a>Song 1:14

My [dôḏ](../../strongs/h/h1730.md) is unto me as an ['eškōôl](../../strongs/h/h811.md) of [kōp̄er](../../strongs/h/h3724.md) in the [kerem](../../strongs/h/h3754.md) of [ʿÊn Ḡeḏî](../../strongs/h/h5872.md).

<a name="song_1_15"></a>Song 1:15

Behold, thou art [yāp̄ê](../../strongs/h/h3303.md), my [raʿyâ](../../strongs/h/h7474.md); behold, thou art [yāp̄ê](../../strongs/h/h3303.md); thou hast [yônâ](../../strongs/h/h3123.md) ['ayin](../../strongs/h/h5869.md).

<a name="song_1_16"></a>Song 1:16

Behold, thou art [yāp̄ê](../../strongs/h/h3303.md), my [dôḏ](../../strongs/h/h1730.md), yea, [na'iym](../../strongs/h/h5273.md): also our ['eres](../../strongs/h/h6210.md) is [raʿănān](../../strongs/h/h7488.md).

<a name="song_1_17"></a>Song 1:17

The [qôrâ](../../strongs/h/h6982.md) of our [bayith](../../strongs/h/h1004.md) are ['erez](../../strongs/h/h730.md), and our [rāḥîṭ](../../strongs/h/h7351.md) [rāḥîṭ](../../strongs/h/h7351.md) of [bᵊrôṯ](../../strongs/h/h1266.md).

---

[Transliteral Bible](../bible.md)

[Song](song.md)

[Song 2](song_2.md)