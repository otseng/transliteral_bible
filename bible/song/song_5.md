# [Song 5](https://www.blueletterbible.org/kjv/song/5)

<a name="song_5_1"></a>Song 5:1

I am [bow'](../../strongs/h/h935.md) into my [gan](../../strongs/h/h1588.md), my ['āḥôṯ](../../strongs/h/h269.md), my [kallâ](../../strongs/h/h3618.md): I have ['ārâ](../../strongs/h/h717.md) my [mōr](../../strongs/h/h4753.md) with my [bāśām](../../strongs/h/h1313.md); I have ['akal](../../strongs/h/h398.md) my [yaʿar](../../strongs/h/h3293.md) with my [dĕbash](../../strongs/h/h1706.md); I have [šāṯâ](../../strongs/h/h8354.md) my [yayin](../../strongs/h/h3196.md) with my [chalab](../../strongs/h/h2461.md): ['akal](../../strongs/h/h398.md), O [rea'](../../strongs/h/h7453.md); [šāṯâ](../../strongs/h/h8354.md), yea, [šāḵar](../../strongs/h/h7937.md), O [dôḏ](../../strongs/h/h1730.md).

<a name="song_5_2"></a>Song 5:2

I [yāšēn](../../strongs/h/h3463.md), but my [leb](../../strongs/h/h3820.md) [ʿûr](../../strongs/h/h5782.md): it is the [qowl](../../strongs/h/h6963.md) of my [dôḏ](../../strongs/h/h1730.md) that [dāp̄aq](../../strongs/h/h1849.md), [pāṯaḥ](../../strongs/h/h6605.md) to me, my ['āḥôṯ](../../strongs/h/h269.md), my [raʿyâ](../../strongs/h/h7474.md), my [yônâ](../../strongs/h/h3123.md), my [tām](../../strongs/h/h8535.md): for my [ro'sh](../../strongs/h/h7218.md) is [mālā'](../../strongs/h/h4390.md) with [ṭal](../../strongs/h/h2919.md), and my [qᵊvuṣṣôṯ](../../strongs/h/h6977.md) with the [rāsîs](../../strongs/h/h7447.md) of the [layil](../../strongs/h/h3915.md).

<a name="song_5_3"></a>Song 5:3

I have put [pāšaṭ](../../strongs/h/h6584.md) my [kĕthoneth](../../strongs/h/h3801.md); how shall I put it [labash](../../strongs/h/h3847.md)? I have [rāḥaṣ](../../strongs/h/h7364.md) my [regel](../../strongs/h/h7272.md); how shall I [ṭānap̄](../../strongs/h/h2936.md) them?

<a name="song_5_4"></a>Song 5:4

My [dôḏ](../../strongs/h/h1730.md) [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) by the [ḥôr](../../strongs/h/h2356.md), and my [me'ah](../../strongs/h/h4578.md) were [hāmâ](../../strongs/h/h1993.md) for him.

<a name="song_5_5"></a>Song 5:5

I [quwm](../../strongs/h/h6965.md) to [pāṯaḥ](../../strongs/h/h6605.md) to my [dôḏ](../../strongs/h/h1730.md); and my [yad](../../strongs/h/h3027.md) [nāṭap̄](../../strongs/h/h5197.md) with [mōr](../../strongs/h/h4753.md), and my ['etsba'](../../strongs/h/h676.md) with ['abar](../../strongs/h/h5674.md) [mōr](../../strongs/h/h4753.md), upon the [kaph](../../strongs/h/h3709.md) of the [manʿûl](../../strongs/h/h4514.md).

<a name="song_5_6"></a>Song 5:6

I [pāṯaḥ](../../strongs/h/h6605.md) to my [dôḏ](../../strongs/h/h1730.md); but my [dôḏ](../../strongs/h/h1730.md) had [ḥāmaq](../../strongs/h/h2559.md) himself, and was ['abar](../../strongs/h/h5674.md): my [nephesh](../../strongs/h/h5315.md) [yāṣā'](../../strongs/h/h3318.md) when he [dabar](../../strongs/h/h1696.md): I [bāqaš](../../strongs/h/h1245.md) him, but I could not [māṣā'](../../strongs/h/h4672.md) him; I [qara'](../../strongs/h/h7121.md) him, but he gave me no ['anah](../../strongs/h/h6030.md).

<a name="song_5_7"></a>Song 5:7

The [shamar](../../strongs/h/h8104.md) that went [cabab](../../strongs/h/h5437.md) the [ʿîr](../../strongs/h/h5892.md) [māṣā'](../../strongs/h/h4672.md) me, they [nakah](../../strongs/h/h5221.md) me, they [pāṣaʿ](../../strongs/h/h6481.md) me; the [shamar](../../strongs/h/h8104.md) of the [ḥômâ](../../strongs/h/h2346.md) [nasa'](../../strongs/h/h5375.md) my [rāḏîḏ](../../strongs/h/h7289.md) from me.

<a name="song_5_8"></a>Song 5:8

I [shaba'](../../strongs/h/h7650.md) you, O [bath](../../strongs/h/h1323.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), if ye [māṣā'](../../strongs/h/h4672.md) my [dôḏ](../../strongs/h/h1730.md), that ye [nāḡaḏ](../../strongs/h/h5046.md) him, that I am [ḥālâ](../../strongs/h/h2470.md) of ['ahăḇâ](../../strongs/h/h160.md).

<a name="song_5_9"></a>Song 5:9

What is thy [dôḏ](../../strongs/h/h1730.md) more than [dôḏ](../../strongs/h/h1730.md), O thou [yāp̄ê](../../strongs/h/h3303.md) among ['ishshah](../../strongs/h/h802.md)? what is thy [dôḏ](../../strongs/h/h1730.md) more than another [dôḏ](../../strongs/h/h1730.md), that thou dost so [shaba'](../../strongs/h/h7650.md) us?

<a name="song_5_10"></a>Song 5:10

My [dôḏ](../../strongs/h/h1730.md) is [ṣaḥ](../../strongs/h/h6703.md) and ['āḏōm](../../strongs/h/h122.md), the [dāḡal](../../strongs/h/h1713.md) among ten thousand.

<a name="song_5_11"></a>Song 5:11

His [ro'sh](../../strongs/h/h7218.md) is as the [keṯem](../../strongs/h/h3800.md) [pāz](../../strongs/h/h6337.md), his [qᵊvuṣṣôṯ](../../strongs/h/h6977.md) are [taltal](../../strongs/h/h8534.md), and [šāḥōr](../../strongs/h/h7838.md) as a [ʿōrēḇ](../../strongs/h/h6158.md).

<a name="song_5_12"></a>Song 5:12

His ['ayin](../../strongs/h/h5869.md) are as the [yônâ](../../strongs/h/h3123.md) by the ['āp̄îq](../../strongs/h/h650.md) of [mayim](../../strongs/h/h4325.md), [rāḥaṣ](../../strongs/h/h7364.md) with [chalab](../../strongs/h/h2461.md), and fitly [yashab](../../strongs/h/h3427.md) [millē'ṯ](../../strongs/h/h4402.md) .

<a name="song_5_13"></a>Song 5:13

His [lᵊḥî](../../strongs/h/h3895.md) are as a [ʿărûḡâ](../../strongs/h/h6170.md) of [beśem](../../strongs/h/h1314.md), as [merqāḥ](../../strongs/h/h4840.md) [miḡdāl](../../strongs/h/h4026.md): his [saphah](../../strongs/h/h8193.md) like [šûšan](../../strongs/h/h7799.md), [nāṭap̄](../../strongs/h/h5197.md) ['abar](../../strongs/h/h5674.md) [mōr](../../strongs/h/h4753.md).

<a name="song_5_14"></a>Song 5:14

His [yad](../../strongs/h/h3027.md) are as [zāhāḇ](../../strongs/h/h2091.md) [Gālîl](../../strongs/h/h1550.md) [mālā'](../../strongs/h/h4390.md) with the [taršîš](../../strongs/h/h8658.md): his [me'ah](../../strongs/h/h4578.md) is as [ʿešeṯ](../../strongs/h/h6247.md) [šēn](../../strongs/h/h8127.md) [ʿālap̄](../../strongs/h/h5968.md) with [sapîr](../../strongs/h/h5601.md).

<a name="song_5_15"></a>Song 5:15

His [šôq](../../strongs/h/h7785.md) are as [ʿammûḏ](../../strongs/h/h5982.md) of [šēš](../../strongs/h/h8336.md), [yacad](../../strongs/h/h3245.md) upon ['eḏen](../../strongs/h/h134.md) of [pāz](../../strongs/h/h6337.md): his [mar'ê](../../strongs/h/h4758.md) is as [Lᵊḇānôn](../../strongs/h/h3844.md), [bāḥar](../../strongs/h/h977.md) as the ['erez](../../strongs/h/h730.md).

<a name="song_5_16"></a>Song 5:16

His [ḥēḵ](../../strongs/h/h2441.md) is most [mamtaqqîm](../../strongs/h/h4477.md): yea, he is altogether [maḥmāḏ](../../strongs/h/h4261.md). This is my [dôḏ](../../strongs/h/h1730.md), and this is my [rea'](../../strongs/h/h7453.md), O [bath](../../strongs/h/h1323.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

---

[Transliteral Bible](../bible.md)

[Song](song.md)

[Song 4](song_4.md) - [Song 6](song_6.md)