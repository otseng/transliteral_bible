# [Song 6](https://www.blueletterbible.org/kjv/song/6)

<a name="song_6_1"></a>Song 6:1

Whither is thy [dôḏ](../../strongs/h/h1730.md) [halak](../../strongs/h/h1980.md), O thou [yāp̄ê](../../strongs/h/h3303.md) among ['ishshah](../../strongs/h/h802.md)? whither is thy [dôḏ](../../strongs/h/h1730.md) [panah](../../strongs/h/h6437.md)? that we may [bāqaš](../../strongs/h/h1245.md) him with thee.

<a name="song_6_2"></a>Song 6:2

My [dôḏ](../../strongs/h/h1730.md) is [yarad](../../strongs/h/h3381.md) into his [gan](../../strongs/h/h1588.md), to the [ʿărûḡâ](../../strongs/h/h6170.md) of [beśem](../../strongs/h/h1314.md), to [ra'ah](../../strongs/h/h7462.md) in the [gan](../../strongs/h/h1588.md), and to [lāqaṭ](../../strongs/h/h3950.md) [šûšan](../../strongs/h/h7799.md).

<a name="song_6_3"></a>Song 6:3

I am my [dôḏ](../../strongs/h/h1730.md), and my [dôḏ](../../strongs/h/h1730.md) is mine: he [ra'ah](../../strongs/h/h7462.md) among the [šûšan](../../strongs/h/h7799.md).

<a name="song_6_4"></a>Song 6:4

Thou art [yāp̄ê](../../strongs/h/h3303.md), O my [raʿyâ](../../strongs/h/h7474.md), as [Tirṣâ](../../strongs/h/h8656.md), [nā'vê](../../strongs/h/h5000.md) as [Yĕruwshalaim](../../strongs/h/h3389.md), ['āyōm](../../strongs/h/h366.md) as a [dāḡal](../../strongs/h/h1713.md).

<a name="song_6_5"></a>Song 6:5

[cabab](../../strongs/h/h5437.md) thine ['ayin](../../strongs/h/h5869.md) from me, for they have [rāhaḇ](../../strongs/h/h7292.md) me: thy [śēʿār](../../strongs/h/h8181.md) is as a [ʿēḏer](../../strongs/h/h5739.md) of [ʿēz](../../strongs/h/h5795.md) that [gālaš](../../strongs/h/h1570.md) from [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="song_6_6"></a>Song 6:6

Thy [šēn](../../strongs/h/h8127.md) are as a [ʿēḏer](../../strongs/h/h5739.md) of [rāḥēl](../../strongs/h/h7353.md) which [ʿālâ](../../strongs/h/h5927.md) from the [raḥṣâ](../../strongs/h/h7367.md), whereof every one beareth [tā'am](../../strongs/h/h8382.md), and there is not one [šakûl](../../strongs/h/h7909.md) among them.

<a name="song_6_7"></a>Song 6:7

As a [pelaḥ](../../strongs/h/h6400.md) of a [rimmôn](../../strongs/h/h7416.md) are thy [raqqâ](../../strongs/h/h7541.md) within thy [ṣammâ](../../strongs/h/h6777.md).

<a name="song_6_8"></a>Song 6:8

There are threescore [malkâ](../../strongs/h/h4436.md), and fourscore [pîleḡeš](../../strongs/h/h6370.md), and [ʿalmâ](../../strongs/h/h5959.md) without [mispār](../../strongs/h/h4557.md).

<a name="song_6_9"></a>Song 6:9

My [yônâ](../../strongs/h/h3123.md), my [tām](../../strongs/h/h8535.md) is but one; she is the only one of her ['em](../../strongs/h/h517.md), she is the [bar](../../strongs/h/h1249.md) one of her that [yalad](../../strongs/h/h3205.md) her. The [bath](../../strongs/h/h1323.md) [ra'ah](../../strongs/h/h7200.md) her, and ['āšar](../../strongs/h/h833.md) her; yea, the [malkâ](../../strongs/h/h4436.md) and the [pîleḡeš](../../strongs/h/h6370.md), and they [halal](../../strongs/h/h1984.md) her.

<a name="song_6_10"></a>Song 6:10

Who is she that [šāqap̄](../../strongs/h/h8259.md) as the [šaḥar](../../strongs/h/h7837.md), [yāp̄ê](../../strongs/h/h3303.md) as the [lᵊḇānâ](../../strongs/h/h3842.md), [bar](../../strongs/h/h1249.md) as the [ḥammâ](../../strongs/h/h2535.md), and ['āyōm](../../strongs/h/h366.md) as a [dāḡal](../../strongs/h/h1713.md)?

<a name="song_6_11"></a>Song 6:11

I [yarad](../../strongs/h/h3381.md) into the [ginnâ](../../strongs/h/h1594.md) of ['ĕḡôz](../../strongs/h/h93.md) to [ra'ah](../../strongs/h/h7200.md) the ['ēḇ](../../strongs/h/h3.md) of the [nachal](../../strongs/h/h5158.md), and to [ra'ah](../../strongs/h/h7200.md) whether the [gep̄en](../../strongs/h/h1612.md) [pāraḥ](../../strongs/h/h6524.md), and the [rimmôn](../../strongs/h/h7416.md) [nûṣ](../../strongs/h/h5132.md).

<a name="song_6_12"></a>Song 6:12

Or ever I was [yada'](../../strongs/h/h3045.md), my [nephesh](../../strongs/h/h5315.md) [śûm](../../strongs/h/h7760.md) me like the [merkāḇâ](../../strongs/h/h4818.md) of [ʿAmmî Nāḏîḇ](../../strongs/h/h5993.md).

<a name="song_6_13"></a>Song 6:13

[shuwb](../../strongs/h/h7725.md), [shuwb](../../strongs/h/h7725.md), O [šûlammîṯ](../../strongs/h/h7759.md); [shuwb](../../strongs/h/h7725.md), [shuwb](../../strongs/h/h7725.md), that we may [chazah](../../strongs/h/h2372.md) upon thee. What will ye [chazah](../../strongs/h/h2372.md) in the [šûlammîṯ](../../strongs/h/h7759.md)? As it were the [mᵊḥōlâ](../../strongs/h/h4246.md) of two [maḥănê](../../strongs/h/h4264.md).

---

[Transliteral Bible](../bible.md)

[Song](song.md)

[Song 5](song_5.md) - [Song 7](song_7.md)