# [1 John 3](https://www.blueletterbible.org/kjv/1jo/3/1/s_1162001)

<a name="1john_3_1"></a>1 John 3:1

[eidō](../../strongs/g/g1492.md), [potapos](../../strongs/g/g4217.md) of [agapē](../../strongs/g/g26.md) the [patēr](../../strongs/g/g3962.md) hath [didōmi](../../strongs/g/g1325.md) upon us, that we should be [kaleō](../../strongs/g/g2564.md) the [teknon](../../strongs/g/g5043.md) of [theos](../../strongs/g/g2316.md): therefore the [kosmos](../../strongs/g/g2889.md) [ginōskō](../../strongs/g/g1097.md) us not, because it [ginōskō](../../strongs/g/g1097.md) him not.

<a name="1john_3_2"></a>1 John 3:2

[agapētos](../../strongs/g/g27.md), now are we the [teknon](../../strongs/g/g5043.md) of [theos](../../strongs/g/g2316.md), and it doth not yet [phaneroō](../../strongs/g/g5319.md) what we shall be: but we [eidō](../../strongs/g/g1492.md) that, when he shall [phaneroō](../../strongs/g/g5319.md), we shall be [homoios](../../strongs/g/g3664.md) him; for we shall [optanomai](../../strongs/g/g3700.md) him as he is.

<a name="1john_3_3"></a>1 John 3:3

And [pas](../../strongs/g/g3956.md) that hath this [elpis](../../strongs/g/g1680.md) in him [hagnizō](../../strongs/g/g48.md) himself, even as he is [hagnos](../../strongs/g/g53.md).

<a name="1john_3_4"></a>1 John 3:4

Whosoever [poieō](../../strongs/g/g4160.md) [hamartia](../../strongs/g/g266.md) [poieō](../../strongs/g/g4160.md) [anomia](../../strongs/g/g458.md) also: for [hamartia](../../strongs/g/g266.md) is [anomia](../../strongs/g/g458.md).

<a name="1john_3_5"></a>1 John 3:5

And ye [eidō](../../strongs/g/g1492.md) that he was [phaneroō](../../strongs/g/g5319.md) to [airō](../../strongs/g/g142.md) our [hamartia](../../strongs/g/g266.md); and in him is no [hamartia](../../strongs/g/g266.md).

<a name="1john_3_6"></a>1 John 3:6

Whosoever [menō](../../strongs/g/g3306.md) in him [hamartanō](../../strongs/g/g264.md) not: whosoever [hamartanō](../../strongs/g/g264.md) hath not [horaō](../../strongs/g/g3708.md) him, neither [ginōskō](../../strongs/g/g1097.md) him.

<a name="1john_3_7"></a>1 John 3:7

[teknion](../../strongs/g/g5040.md), let [mēdeis](../../strongs/g/g3367.md) [planaō](../../strongs/g/g4105.md) you: he that [poieō](../../strongs/g/g4160.md) [dikaiosynē](../../strongs/g/g1343.md) is [dikaios](../../strongs/g/g1342.md), even as he is [dikaios](../../strongs/g/g1342.md).

<a name="1john_3_8"></a>1 John 3:8

He that [poieō](../../strongs/g/g4160.md) [hamartia](../../strongs/g/g266.md) is of the [diabolos](../../strongs/g/g1228.md); for the [diabolos](../../strongs/g/g1228.md) [hamartanō](../../strongs/g/g264.md) from the [archē](../../strongs/g/g746.md). For this purpose the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md) was [phaneroō](../../strongs/g/g5319.md), that he might [lyō](../../strongs/g/g3089.md) the [ergon](../../strongs/g/g2041.md) of the [diabolos](../../strongs/g/g1228.md).

<a name="1john_3_9"></a>1 John 3:9

Whosoever is [gennaō](../../strongs/g/g1080.md) of [theos](../../strongs/g/g2316.md) [poieō](../../strongs/g/g4160.md) not [hamartia](../../strongs/g/g266.md); for his [sperma](../../strongs/g/g4690.md) [menō](../../strongs/g/g3306.md) in him: and he cannot [hamartanō](../../strongs/g/g264.md), because he is [gennaō](../../strongs/g/g1080.md) of [theos](../../strongs/g/g2316.md).

<a name="1john_3_10"></a>1 John 3:10

In this the [teknon](../../strongs/g/g5043.md) of [theos](../../strongs/g/g2316.md) are [phaneros](../../strongs/g/g5318.md), and the [teknon](../../strongs/g/g5043.md) of the [diabolos](../../strongs/g/g1228.md): whosoever [poieō](../../strongs/g/g4160.md) not [dikaiosynē](../../strongs/g/g1343.md) is not of [theos](../../strongs/g/g2316.md), neither he that [agapaō](../../strongs/g/g25.md) not his [adelphos](../../strongs/g/g80.md).

<a name="1john_3_11"></a>1 John 3:11

For this is the [angelia](../../strongs/g/g31.md) that ye [akouō](../../strongs/g/g191.md) from the [archē](../../strongs/g/g746.md), that we should [agapaō](../../strongs/g/g25.md) [allēlōn](../../strongs/g/g240.md).

<a name="1john_3_12"></a>1 John 3:12

Not as [kain](../../strongs/g/g2535.md), was of [ponēros](../../strongs/g/g4190.md), and [sphazō](../../strongs/g/g4969.md) his [adelphos](../../strongs/g/g80.md). And wherefore [sphazō](../../strongs/g/g4969.md) he him? Because his own [ergon](../../strongs/g/g2041.md) were [ponēros](../../strongs/g/g4190.md), and his [adelphos](../../strongs/g/g80.md) [dikaios](../../strongs/g/g1342.md).

<a name="1john_3_13"></a>1 John 3:13

[thaumazō](../../strongs/g/g2296.md) not, my [adelphos](../../strongs/g/g80.md), if the [kosmos](../../strongs/g/g2889.md) [miseō](../../strongs/g/g3404.md) you.

<a name="1john_3_14"></a>1 John 3:14

We [eidō](../../strongs/g/g1492.md) that we have [metabainō](../../strongs/g/g3327.md) from [thanatos](../../strongs/g/g2288.md) unto [zōē](../../strongs/g/g2222.md), because we [agapaō](../../strongs/g/g25.md) the [adelphos](../../strongs/g/g80.md). He that [agapaō](../../strongs/g/g25.md) not [adelphos](../../strongs/g/g80.md) [menō](../../strongs/g/g3306.md) in [thanatos](../../strongs/g/g2288.md).

<a name="1john_3_15"></a>1 John 3:15

Whosoever [miseō](../../strongs/g/g3404.md) his [adelphos](../../strongs/g/g80.md) is an [anthrōpoktonos](../../strongs/g/g443.md): and ye [eidō](../../strongs/g/g1492.md) that no [anthrōpoktonos](../../strongs/g/g443.md) hath [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md) [menō](../../strongs/g/g3306.md) in him.

<a name="1john_3_16"></a>1 John 3:16

Hereby [ginōskō](../../strongs/g/g1097.md) we the [agapē](../../strongs/g/g26.md), because he [tithēmi](../../strongs/g/g5087.md) his [psychē](../../strongs/g/g5590.md) for us: and we [opheilō](../../strongs/g/g3784.md) to [tithēmi](../../strongs/g/g5087.md) [psychē](../../strongs/g/g5590.md) for the [adelphos](../../strongs/g/g80.md).

<a name="1john_3_17"></a>1 John 3:17

But whoso hath this [kosmos](../../strongs/g/g2889.md) [bios](../../strongs/g/g979.md), and [theōreō](../../strongs/g/g2334.md) his [adelphos](../../strongs/g/g80.md) have [chreia](../../strongs/g/g5532.md), and [kleiō](../../strongs/g/g2808.md) his [splagchnon](../../strongs/g/g4698.md) from him, how [menō](../../strongs/g/g3306.md) the [agapē](../../strongs/g/g26.md) of [theos](../../strongs/g/g2316.md) in him?

<a name="1john_3_18"></a>1 John 3:18

My [teknion](../../strongs/g/g5040.md), let us not [agapaō](../../strongs/g/g25.md) in [logos](../../strongs/g/g3056.md), neither in [glōssa](../../strongs/g/g1100.md); but in [ergon](../../strongs/g/g2041.md) and in [alētheia](../../strongs/g/g225.md).

<a name="1john_3_19"></a>1 John 3:19

And hereby we [ginōskō](../../strongs/g/g1097.md) that we are of the [alētheia](../../strongs/g/g225.md), and shall [peithō](../../strongs/g/g3982.md) our [kardia](../../strongs/g/g2588.md) before him.

<a name="1john_3_20"></a>1 John 3:20

For if our [kardia](../../strongs/g/g2588.md) [kataginōskō](../../strongs/g/g2607.md) us, [theos](../../strongs/g/g2316.md) is [meizōn](../../strongs/g/g3187.md) than our [kardia](../../strongs/g/g2588.md), and [ginōskō](../../strongs/g/g1097.md) all things.

<a name="1john_3_21"></a>1 John 3:21

[agapētos](../../strongs/g/g27.md), if our [kardia](../../strongs/g/g2588.md) [kataginōskō](../../strongs/g/g2607.md) us not, have we [parrēsia](../../strongs/g/g3954.md) toward [theos](../../strongs/g/g2316.md).

<a name="1john_3_22"></a>1 John 3:22

And whatsoever we [aiteō](../../strongs/g/g154.md), we [lambanō](../../strongs/g/g2983.md) of him, because we [tēreō](../../strongs/g/g5083.md) his [entolē](../../strongs/g/g1785.md), and [poieō](../../strongs/g/g4160.md) [arestos](../../strongs/g/g701.md) in his [enōpion](../../strongs/g/g1799.md).

<a name="1john_3_23"></a>1 John 3:23

And this is his [entolē](../../strongs/g/g1785.md), That we should [pisteuō](../../strongs/g/g4100.md) on the [onoma](../../strongs/g/g3686.md) of his [huios](../../strongs/g/g5207.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), and [agapaō](../../strongs/g/g25.md) [allēlōn](../../strongs/g/g240.md), as he [didōmi](../../strongs/g/g1325.md) us [entolē](../../strongs/g/g1785.md).

<a name="1john_3_24"></a>1 John 3:24

And he that [tēreō](../../strongs/g/g5083.md) his [entolē](../../strongs/g/g1785.md) [menō](../../strongs/g/g3306.md) in him, and he in him. And hereby we [ginōskō](../../strongs/g/g1097.md) that he [menō](../../strongs/g/g3306.md) in us, by the [pneuma](../../strongs/g/g4151.md) which he hath [didōmi](../../strongs/g/g1325.md) us.

---

[Transliteral Bible](../bible.md)

[1 John](1john.md)

[1 John 2](1john_2.md) - [1 John 4](1john_4.md)