# [1 John 2](https://www.blueletterbible.org/kjv/1jo/2/1/s_1161001)

<a name="1john_2_1"></a>1 John 2:1

My [teknion](../../strongs/g/g5040.md), these things [graphō](../../strongs/g/g1125.md) I unto you, that ye [hamartanō](../../strongs/g/g264.md) not. And if [tis](../../strongs/g/g5100.md) [hamartanō](../../strongs/g/g264.md), we have a [paraklētos](../../strongs/g/g3875.md) with the [patēr](../../strongs/g/g3962.md), [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) the [dikaios](../../strongs/g/g1342.md):

<a name="1john_2_2"></a>1 John 2:2

And he is the [hilasmos](../../strongs/g/g2434.md) for our [hamartia](../../strongs/g/g266.md): and not for our's only, but also for the [holos](../../strongs/g/g3650.md) [kosmos](../../strongs/g/g2889.md).

<a name="1john_2_3"></a>1 John 2:3

And hereby we do [ginōskō](../../strongs/g/g1097.md) that we [ginōskō](../../strongs/g/g1097.md) him, if we [tēreō](../../strongs/g/g5083.md) his [entolē](../../strongs/g/g1785.md).

<a name="1john_2_4"></a>1 John 2:4

He that [legō](../../strongs/g/g3004.md), I [ginōskō](../../strongs/g/g1097.md) him, and [tēreō](../../strongs/g/g5083.md) not his [entolē](../../strongs/g/g1785.md), is a [pseustēs](../../strongs/g/g5583.md), and the [alētheia](../../strongs/g/g225.md) is not in him.

<a name="1john_2_5"></a>1 John 2:5

But whoso [tēreō](../../strongs/g/g5083.md) his [logos](../../strongs/g/g3056.md), in him [alēthōs](../../strongs/g/g230.md) is the [agapē](../../strongs/g/g26.md) of [theos](../../strongs/g/g2316.md) [teleioō](../../strongs/g/g5048.md): hereby [ginōskō](../../strongs/g/g1097.md) we that we are in him.

<a name="1john_2_6"></a>1 John 2:6

He that [legō](../../strongs/g/g3004.md) he [menō](../../strongs/g/g3306.md) in him [opheilō](../../strongs/g/g3784.md) himself also so to [peripateō](../../strongs/g/g4043.md), even as he [peripateō](../../strongs/g/g4043.md).

<a name="1john_2_7"></a>1 John 2:7

[adelphos](../../strongs/g/g80.md), I [graphō](../../strongs/g/g1125.md) no [kainos](../../strongs/g/g2537.md) [entolē](../../strongs/g/g1785.md) unto you, but a [palaios](../../strongs/g/g3820.md) [entolē](../../strongs/g/g1785.md) which ye had from the [archē](../../strongs/g/g746.md). The [palaios](../../strongs/g/g3820.md) [entolē](../../strongs/g/g1785.md) is the [logos](../../strongs/g/g3056.md) which ye have [akouō](../../strongs/g/g191.md) from the [archē](../../strongs/g/g746.md).

<a name="1john_2_8"></a>1 John 2:8

Again, a [kainos](../../strongs/g/g2537.md) [entolē](../../strongs/g/g1785.md) I [graphō](../../strongs/g/g1125.md) unto you, which thing is [alēthēs](../../strongs/g/g227.md) in him and in you: because the [skotia](../../strongs/g/g4653.md) is [paragō](../../strongs/g/g3855.md), and the [alēthinos](../../strongs/g/g228.md) [phōs](../../strongs/g/g5457.md) now [phainō](../../strongs/g/g5316.md).

<a name="1john_2_9"></a>1 John 2:9

He that [legō](../../strongs/g/g3004.md) he is in the [phōs](../../strongs/g/g5457.md), and [miseō](../../strongs/g/g3404.md) his [adelphos](../../strongs/g/g80.md), is in [skotia](../../strongs/g/g4653.md) even until now.

<a name="1john_2_10"></a>1 John 2:10

He that [agapaō](../../strongs/g/g25.md) his [adelphos](../../strongs/g/g80.md) [menō](../../strongs/g/g3306.md) in the [phōs](../../strongs/g/g5457.md), and there is no [skandalon](../../strongs/g/g4625.md) in him.

<a name="1john_2_11"></a>1 John 2:11

But he that [miseō](../../strongs/g/g3404.md) his [adelphos](../../strongs/g/g80.md) is in [skotia](../../strongs/g/g4653.md), and [peripateō](../../strongs/g/g4043.md) in [skotia](../../strongs/g/g4653.md), and [eidō](../../strongs/g/g1492.md) not whither he [hypagō](../../strongs/g/g5217.md), because that [skotia](../../strongs/g/g4653.md) hath [typhloō](../../strongs/g/g5186.md) his [ophthalmos](../../strongs/g/g3788.md).

<a name="1john_2_12"></a>1 John 2:12

I [graphō](../../strongs/g/g1125.md) unto you, [teknion](../../strongs/g/g5040.md), because your [hamartia](../../strongs/g/g266.md) are [aphiēmi](../../strongs/g/g863.md) you for his [onoma](../../strongs/g/g3686.md) sake.

<a name="1john_2_13"></a>1 John 2:13

I [graphō](../../strongs/g/g1125.md) unto you, [patēr](../../strongs/g/g3962.md), because ye have [ginōskō](../../strongs/g/g1097.md) him from the [archē](../../strongs/g/g746.md). I [graphō](../../strongs/g/g1125.md) unto you, [neaniskos](../../strongs/g/g3495.md), because ye have [nikaō](../../strongs/g/g3528.md) the [ponēros](../../strongs/g/g4190.md). I [graphō](../../strongs/g/g1125.md) unto you, [paidion](../../strongs/g/g3813.md), because ye have [ginōskō](../../strongs/g/g1097.md) the [patēr](../../strongs/g/g3962.md).

<a name="1john_2_14"></a>1 John 2:14

I have [graphō](../../strongs/g/g1125.md) unto you, [patēr](../../strongs/g/g3962.md), because ye have [ginōskō](../../strongs/g/g1097.md) him from the [archē](../../strongs/g/g746.md). I have [graphō](../../strongs/g/g1125.md) unto you, [neaniskos](../../strongs/g/g3495.md), because ye are [ischyros](../../strongs/g/g2478.md), and the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) [menō](../../strongs/g/g3306.md) in you, and ye have [nikaō](../../strongs/g/g3528.md) the [ponēros](../../strongs/g/g4190.md).

<a name="1john_2_15"></a>1 John 2:15

[agapaō](../../strongs/g/g25.md) not the [kosmos](../../strongs/g/g2889.md), neither the things in the [kosmos](../../strongs/g/g2889.md). If any man [agapaō](../../strongs/g/g25.md) the [kosmos](../../strongs/g/g2889.md), the [agapē](../../strongs/g/g26.md) of the [patēr](../../strongs/g/g3962.md) is not in him.

<a name="1john_2_16"></a>1 John 2:16

For all that in the [kosmos](../../strongs/g/g2889.md), the [epithymia](../../strongs/g/g1939.md) of the [sarx](../../strongs/g/g4561.md), and the [epithymia](../../strongs/g/g1939.md) of the [ophthalmos](../../strongs/g/g3788.md), and the [alazoneia](../../strongs/g/g212.md) of [bios](../../strongs/g/g979.md), is not of the [patēr](../../strongs/g/g3962.md), but is of the [kosmos](../../strongs/g/g2889.md).

<a name="1john_2_17"></a>1 John 2:17

And the [kosmos](../../strongs/g/g2889.md) [paragō](../../strongs/g/g3855.md), and the [epithymia](../../strongs/g/g1939.md) thereof: but he that [poieō](../../strongs/g/g4160.md) the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md) [menō](../../strongs/g/g3306.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md).

<a name="1john_2_18"></a>1 John 2:18

[paidion](../../strongs/g/g3813.md), it is the [eschatos](../../strongs/g/g2078.md) [hōra](../../strongs/g/g5610.md): and as ye have [akouō](../../strongs/g/g191.md) that [antichristos](../../strongs/g/g500.md) shall [erchomai](../../strongs/g/g2064.md), even now are there [polys](../../strongs/g/g4183.md) [antichristos](../../strongs/g/g500.md); whereby we [ginōskō](../../strongs/g/g1097.md) that it is the [eschatos](../../strongs/g/g2078.md) [hōra](../../strongs/g/g5610.md).

<a name="1john_2_19"></a>1 John 2:19

They [exerchomai](../../strongs/g/g1831.md) from us, but they were not of us; for if they had been of us, they would have [menō](../../strongs/g/g3306.md) with us: but, that they might be [phaneroō](../../strongs/g/g5319.md) that they were not all of us.

<a name="1john_2_20"></a>1 John 2:20

But ye have a [chrisma](../../strongs/g/g5545.md) from the [hagios](../../strongs/g/g40.md), and ye [eidō](../../strongs/g/g1492.md) all things.

<a name="1john_2_21"></a>1 John 2:21

I have not [graphō](../../strongs/g/g1125.md) unto you because ye [eidō](../../strongs/g/g1492.md) not the [alētheia](../../strongs/g/g225.md), but because ye [eidō](../../strongs/g/g1492.md) it, and that no [pseudos](../../strongs/g/g5579.md) is of the [alētheia](../../strongs/g/g225.md).

<a name="1john_2_22"></a>1 John 2:22

Who is a [pseustēs](../../strongs/g/g5583.md) but he that [arneomai](../../strongs/g/g720.md) that [Iēsous](../../strongs/g/g2424.md) is the [Christos](../../strongs/g/g5547.md)? He is [antichristos](../../strongs/g/g500.md), that [arneomai](../../strongs/g/g720.md) the [patēr](../../strongs/g/g3962.md) and the [huios](../../strongs/g/g5207.md).

<a name="1john_2_23"></a>1 John 2:23

Whosoever [arneomai](../../strongs/g/g720.md) the [huios](../../strongs/g/g5207.md), the same hath not the [patēr](../../strongs/g/g3962.md): (but) he that [homologeō](../../strongs/g/g3670.md) the [huios](../../strongs/g/g5207.md) hath the [patēr](../../strongs/g/g3962.md) also.

<a name="1john_2_24"></a>1 John 2:24

Let that therefore [menō](../../strongs/g/g3306.md) in you, which ye have [akouō](../../strongs/g/g191.md) from the [archē](../../strongs/g/g746.md). If that which ye have [akouō](../../strongs/g/g191.md) from the [archē](../../strongs/g/g746.md) shall [menō](../../strongs/g/g3306.md) in you, ye also shall [menō](../../strongs/g/g3306.md) in the [huios](../../strongs/g/g5207.md), and in the [patēr](../../strongs/g/g3962.md).

<a name="1john_2_25"></a>1 John 2:25

And this is the [epaggelia](../../strongs/g/g1860.md) that he hath [epaggellomai](../../strongs/g/g1861.md) us, [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md).

<a name="1john_2_26"></a>1 John 2:26

These have I [graphō](../../strongs/g/g1125.md) unto you concerning them that [planaō](../../strongs/g/g4105.md) you.

<a name="1john_2_27"></a>1 John 2:27

But the [chrisma](../../strongs/g/g5545.md) which ye have [lambanō](../../strongs/g/g2983.md) of him [menō](../../strongs/g/g3306.md) in you, and ye [chreia](../../strongs/g/g5532.md) not that any [tis](../../strongs/g/g5100.md) [didaskō](../../strongs/g/g1321.md) you: but as the same [chrisma](../../strongs/g/g5545.md) [didaskō](../../strongs/g/g1321.md) you of all things, and is [alēthēs](../../strongs/g/g227.md), and is no [pseudos](../../strongs/g/g5579.md), and even as it hath [didaskō](../../strongs/g/g1321.md) you, ye shall [menō](../../strongs/g/g3306.md) in him.

<a name="1john_2_28"></a>1 John 2:28

And now, [teknion](../../strongs/g/g5040.md), [menō](../../strongs/g/g3306.md) in him; that, when he shall [phaneroō](../../strongs/g/g5319.md), we may have [parrēsia](../../strongs/g/g3954.md), and not [aischynō](../../strongs/g/g153.md) before him at his [parousia](../../strongs/g/g3952.md).

<a name="1john_2_29"></a>1 John 2:29

If ye [eidō](../../strongs/g/g1492.md) that he is [dikaios](../../strongs/g/g1342.md), ye [ginōskō](../../strongs/g/g1097.md) that every one that [poieō](../../strongs/g/g4160.md) [dikaiosynē](../../strongs/g/g1343.md) is [gennaō](../../strongs/g/g1080.md) of him.

---

[Transliteral Bible](../bible.md)

[1 John](1john.md)

[1 John 1](1john_1.md) - [1 John 3](1john_3.md)