# [1 John 5](https://www.blueletterbible.org/kjv/1jo/5/1/s_1164001)

<a name="1john_5_1"></a>1 John 5:1

Whosoever [pisteuō](../../strongs/g/g4100.md) that [Iēsous](../../strongs/g/g2424.md) is the [Christos](../../strongs/g/g5547.md) is [gennaō](../../strongs/g/g1080.md) of [theos](../../strongs/g/g2316.md): and every one that [agapaō](../../strongs/g/g25.md) him that [gennaō](../../strongs/g/g1080.md) [agapaō](../../strongs/g/g25.md) him also that is [gennaō](../../strongs/g/g1080.md) of him.

<a name="1john_5_2"></a>1 John 5:2

By this we [ginōskō](../../strongs/g/g1097.md) that we [agapaō](../../strongs/g/g25.md) the [teknon](../../strongs/g/g5043.md) of [theos](../../strongs/g/g2316.md), when we [agapaō](../../strongs/g/g25.md) [theos](../../strongs/g/g2316.md), and [tēreō](../../strongs/g/g5083.md) his [entolē](../../strongs/g/g1785.md).

<a name="1john_5_3"></a>1 John 5:3

For this is the [agapē](../../strongs/g/g26.md) of [theos](../../strongs/g/g2316.md), that we [tēreō](../../strongs/g/g5083.md) his [entolē](../../strongs/g/g1785.md): and his [entolē](../../strongs/g/g1785.md) are not [barys](../../strongs/g/g926.md).

<a name="1john_5_4"></a>1 John 5:4

For whatsoever is [gennaō](../../strongs/g/g1080.md) of [theos](../../strongs/g/g2316.md) [nikaō](../../strongs/g/g3528.md) the [kosmos](../../strongs/g/g2889.md): and this is the [nikē](../../strongs/g/g3529.md) that [nikaō](../../strongs/g/g3528.md) the [kosmos](../../strongs/g/g2889.md), our [pistis](../../strongs/g/g4102.md).

<a name="1john_5_5"></a>1 John 5:5

Who is he that [nikaō](../../strongs/g/g3528.md) the [kosmos](../../strongs/g/g2889.md), but he that [pisteuō](../../strongs/g/g4100.md) that [Iēsous](../../strongs/g/g2424.md) is the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md)?

<a name="1john_5_6"></a>1 John 5:6

This is he that [erchomai](../../strongs/g/g2064.md) by [hydōr](../../strongs/g/g5204.md) and [haima](../../strongs/g/g129.md), [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md); not by [hydōr](../../strongs/g/g5204.md) only, but by [hydōr](../../strongs/g/g5204.md) and [haima](../../strongs/g/g129.md). And it is the [pneuma](../../strongs/g/g4151.md) that [martyreō](../../strongs/g/g3140.md), because the [pneuma](../../strongs/g/g4151.md) is [alētheia](../../strongs/g/g225.md).

<a name="1john_5_7"></a>1 John 5:7

For there are three that [martyreō](../../strongs/g/g3140.md) in [ouranos](../../strongs/g/g3772.md), the [patēr](../../strongs/g/g3962.md), the [logos](../../strongs/g/g3056.md), and the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md): and these three are one. [^1]

<a name="1john_5_8"></a>1 John 5:8

And there are three that [martyreō](../../strongs/g/g3140.md) in [gē](../../strongs/g/g1093.md), the [pneuma](../../strongs/g/g4151.md), and the [hydōr](../../strongs/g/g5204.md), and the [haima](../../strongs/g/g129.md): and these three [eisi](../../strongs/g/g1526.md) in one.

<a name="1john_5_9"></a>1 John 5:9

If we [lambanō](../../strongs/g/g2983.md) the [martyria](../../strongs/g/g3141.md) of [anthrōpos](../../strongs/g/g444.md), the [martyria](../../strongs/g/g3141.md) of [theos](../../strongs/g/g2316.md) is [meizōn](../../strongs/g/g3187.md): for this is the [martyria](../../strongs/g/g3141.md) of [theos](../../strongs/g/g2316.md) which he hath [martyreō](../../strongs/g/g3140.md) of his [huios](../../strongs/g/g5207.md).

<a name="1john_5_10"></a>1 John 5:10

He that [pisteuō](../../strongs/g/g4100.md) on the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md) hath the [martyria](../../strongs/g/g3141.md) in himself: he that [pisteuō](../../strongs/g/g4100.md) not [theos](../../strongs/g/g2316.md) hath [poieō](../../strongs/g/g4160.md) him a [pseustēs](../../strongs/g/g5583.md); because he [pisteuō](../../strongs/g/g4100.md) not the [martyria](../../strongs/g/g3141.md) that [theos](../../strongs/g/g2316.md) [martyreō](../../strongs/g/g3140.md) of his [huios](../../strongs/g/g5207.md).

<a name="1john_5_11"></a>1 John 5:11

And this is the [martyria](../../strongs/g/g3141.md), that [theos](../../strongs/g/g2316.md) hath [didōmi](../../strongs/g/g1325.md) to us [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md), and this [zōē](../../strongs/g/g2222.md) is in his [huios](../../strongs/g/g5207.md).

<a name="1john_5_12"></a>1 John 5:12

He that hath the [huios](../../strongs/g/g5207.md) hath [zōē](../../strongs/g/g2222.md); he that hath not the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md) hath not [zōē](../../strongs/g/g2222.md).

<a name="1john_5_13"></a>1 John 5:13

These things have I [graphō](../../strongs/g/g1125.md) unto you that [pisteuō](../../strongs/g/g4100.md) on the [onoma](../../strongs/g/g3686.md) of the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md); that ye may [eidō](../../strongs/g/g1492.md) that ye have [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md), and that ye may [pisteuō](../../strongs/g/g4100.md) on the [onoma](../../strongs/g/g3686.md) of the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md). [^2]

<a name="1john_5_14"></a>1 John 5:14

And this is the [parrēsia](../../strongs/g/g3954.md) that we have in him, that, if we [aiteō](../../strongs/g/g154.md) any thing according to his [thelēma](../../strongs/g/g2307.md), he [akouō](../../strongs/g/g191.md) us:

<a name="1john_5_15"></a>1 John 5:15

And if we [eidō](../../strongs/g/g1492.md) that he [akouō](../../strongs/g/g191.md) us, whatsoever we [aiteō](../../strongs/g/g154.md), we [eidō](../../strongs/g/g1492.md) that we have the [aitēma](../../strongs/g/g155.md) that we [aiteō](../../strongs/g/g154.md) of him.

<a name="1john_5_16"></a>1 John 5:16

If [tis](../../strongs/g/g5100.md) [eidō](../../strongs/g/g1492.md) his [adelphos](../../strongs/g/g80.md) [hamartanō](../../strongs/g/g264.md) a [hamartia](../../strongs/g/g266.md) is not unto [thanatos](../../strongs/g/g2288.md), he shall [aiteō](../../strongs/g/g154.md), and he shall [didōmi](../../strongs/g/g1325.md) him [zōē](../../strongs/g/g2222.md) for them that [hamartanō](../../strongs/g/g264.md) not unto [thanatos](../../strongs/g/g2288.md). There is a [hamartia](../../strongs/g/g266.md) unto [thanatos](../../strongs/g/g2288.md): I do not [legō](../../strongs/g/g3004.md) that he shall [erōtaō](../../strongs/g/g2065.md) for it.

<a name="1john_5_17"></a>1 John 5:17

All [adikia](../../strongs/g/g93.md) is [hamartia](../../strongs/g/g266.md): and there is a [hamartia](../../strongs/g/g266.md) not unto [thanatos](../../strongs/g/g2288.md).

<a name="1john_5_18"></a>1 John 5:18

We [eidō](../../strongs/g/g1492.md) that whosoever is [gennaō](../../strongs/g/g1080.md) of [theos](../../strongs/g/g2316.md) [hamartanō](../../strongs/g/g264.md) not; but he that is [gennaō](../../strongs/g/g1080.md) of [theos](../../strongs/g/g2316.md) [tēreō](../../strongs/g/g5083.md) himself, and that [ponēros](../../strongs/g/g4190.md) [haptomai](../../strongs/g/g680.md) him not.

<a name="1john_5_19"></a>1 John 5:19

we [eidō](../../strongs/g/g1492.md) that we are of [theos](../../strongs/g/g2316.md), and the [holos](../../strongs/g/g3650.md) [kosmos](../../strongs/g/g2889.md) [keimai](../../strongs/g/g2749.md) in [ponēros](../../strongs/g/g4190.md).

<a name="1john_5_20"></a>1 John 5:20

And we [eidō](../../strongs/g/g1492.md) that the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md) [hēkō](../../strongs/g/g2240.md), and hath [didōmi](../../strongs/g/g1325.md) us a [dianoia](../../strongs/g/g1271.md), that we may [ginōskō](../../strongs/g/g1097.md) him that is [alēthinos](../../strongs/g/g228.md), and we are in him that is [alēthinos](../../strongs/g/g228.md), even in his [huios](../../strongs/g/g5207.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md). This is the true [theos](../../strongs/g/g2316.md), and [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md).

<a name="1john_5_21"></a>1 John 5:21

[teknion](../../strongs/g/g5040.md), [phylassō](../../strongs/g/g5442.md) yourselves from [eidōlon](../../strongs/g/g1497.md). [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[1 John](1john.md)

[1 John 4](1john_4.md)

---

[^1]: [1 John 5:7 Commentary](../../commentary/1john/1john_5_commentary.md#1john_5_7)

[^2]: [1 John 5:13 Commentary](../../commentary/1john/1john_5_commentary.md#1john_5_13)
