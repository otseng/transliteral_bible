# [1 John 4](https://www.blueletterbible.org/kjv/1jo/4/1/s_1163001)

<a name="1john_4_1"></a>1 John 4:1

[agapētos](../../strongs/g/g27.md), [pisteuō](../../strongs/g/g4100.md) not every [pneuma](../../strongs/g/g4151.md), but [dokimazō](../../strongs/g/g1381.md) the [pneuma](../../strongs/g/g4151.md) whether they are of [theos](../../strongs/g/g2316.md): because [polys](../../strongs/g/g4183.md) [pseudoprophētēs](../../strongs/g/g5578.md) are [exerchomai](../../strongs/g/g1831.md) into the [kosmos](../../strongs/g/g2889.md).

<a name="1john_4_2"></a>1 John 4:2

Hereby [ginōskō](../../strongs/g/g1097.md) ye the [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md): Every [pneuma](../../strongs/g/g4151.md) that [homologeō](../../strongs/g/g3670.md) that [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) is [erchomai](../../strongs/g/g2064.md) in the [sarx](../../strongs/g/g4561.md) is of [theos](../../strongs/g/g2316.md):

<a name="1john_4_3"></a>1 John 4:3

And every [pneuma](../../strongs/g/g4151.md) that [homologeō](../../strongs/g/g3670.md) not that [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) is [erchomai](../../strongs/g/g2064.md) in the [sarx](../../strongs/g/g4561.md) is not of [theos](../../strongs/g/g2316.md): and this is that of [antichristos](../../strongs/g/g500.md), whereof ye have [akouō](../../strongs/g/g191.md) that it should [erchomai](../../strongs/g/g2064.md); and even now already is it in the [kosmos](../../strongs/g/g2889.md). [^1]

<a name="1john_4_4"></a>1 John 4:4

Ye are of [theos](../../strongs/g/g2316.md), [teknion](../../strongs/g/g5040.md), and have [nikaō](../../strongs/g/g3528.md) them: because [meizōn](../../strongs/g/g3187.md) is he that is in you, than he that is in the [kosmos](../../strongs/g/g2889.md).

<a name="1john_4_5"></a>1 John 4:5

They are of the [kosmos](../../strongs/g/g2889.md): therefore [laleō](../../strongs/g/g2980.md) they of the [kosmos](../../strongs/g/g2889.md), and the [kosmos](../../strongs/g/g2889.md) [akouō](../../strongs/g/g191.md) them.

<a name="1john_4_6"></a>1 John 4:6

We are of [theos](../../strongs/g/g2316.md): he that [ginōskō](../../strongs/g/g1097.md) [theos](../../strongs/g/g2316.md) [akouō](../../strongs/g/g191.md) us; he that is not of [theos](../../strongs/g/g2316.md) [akouō](../../strongs/g/g191.md) not us. Hereby [ginōskō](../../strongs/g/g1097.md) we the [pneuma](../../strongs/g/g4151.md) of [alētheia](../../strongs/g/g225.md), and the [pneuma](../../strongs/g/g4151.md) of [planē](../../strongs/g/g4106.md).

<a name="1john_4_7"></a>1 John 4:7

[agapētos](../../strongs/g/g27.md), let us [agapaō](../../strongs/g/g25.md) [allēlōn](../../strongs/g/g240.md): for [agapē](../../strongs/g/g26.md) is of [theos](../../strongs/g/g2316.md); and every one that [agapaō](../../strongs/g/g25.md) is [gennaō](../../strongs/g/g1080.md) of [theos](../../strongs/g/g2316.md), and [ginōskō](../../strongs/g/g1097.md) [theos](../../strongs/g/g2316.md).

<a name="1john_4_8"></a>1 John 4:8

He that [agapaō](../../strongs/g/g25.md) not [ginōskō](../../strongs/g/g1097.md) not [theos](../../strongs/g/g2316.md); for [theos](../../strongs/g/g2316.md) is [agapē](../../strongs/g/g26.md).

<a name="1john_4_9"></a>1 John 4:9

In this was [phaneroō](../../strongs/g/g5319.md) the [agapē](../../strongs/g/g26.md) of [theos](../../strongs/g/g2316.md) toward us, because that [theos](../../strongs/g/g2316.md) [apostellō](../../strongs/g/g649.md) his [monogenēs](../../strongs/g/g3439.md) [huios](../../strongs/g/g5207.md) into the [kosmos](../../strongs/g/g2889.md), that we might [zaō](../../strongs/g/g2198.md) through him.

<a name="1john_4_10"></a>1 John 4:10

Herein is [agapē](../../strongs/g/g26.md), not that we [agapaō](../../strongs/g/g25.md) [theos](../../strongs/g/g2316.md), but that he [agapaō](../../strongs/g/g25.md) us, and [apostellō](../../strongs/g/g649.md) his [huios](../../strongs/g/g5207.md) to be the [hilasmos](../../strongs/g/g2434.md) for our [hamartia](../../strongs/g/g266.md).

<a name="1john_4_11"></a>1 John 4:11

[agapētos](../../strongs/g/g27.md), if [theos](../../strongs/g/g2316.md) so [agapaō](../../strongs/g/g25.md) us, we [opheilō](../../strongs/g/g3784.md) also to [agapaō](../../strongs/g/g25.md) [allēlōn](../../strongs/g/g240.md).

<a name="1john_4_12"></a>1 John 4:12

[oudeis](../../strongs/g/g3762.md) hath [theaomai](../../strongs/g/g2300.md) [theos](../../strongs/g/g2316.md) at any [pōpote](../../strongs/g/g4455.md). If we [agapaō](../../strongs/g/g25.md) [allēlōn](../../strongs/g/g240.md), [theos](../../strongs/g/g2316.md) [menō](../../strongs/g/g3306.md) in us, and his [agapē](../../strongs/g/g26.md) is [teleioō](../../strongs/g/g5048.md) in us.

<a name="1john_4_13"></a>1 John 4:13

Hereby [ginōskō](../../strongs/g/g1097.md) we that we [menō](../../strongs/g/g3306.md) in him, and he in us, because he hath [didōmi](../../strongs/g/g1325.md) us of his [pneuma](../../strongs/g/g4151.md).

<a name="1john_4_14"></a>1 John 4:14

And we have [theaomai](../../strongs/g/g2300.md) and do [martyreō](../../strongs/g/g3140.md) that the [patēr](../../strongs/g/g3962.md)  [apostellō](../../strongs/g/g649.md) the [huios](../../strongs/g/g5207.md) to be the [sōtēr](../../strongs/g/g4990.md) of the [kosmos](../../strongs/g/g2889.md).

<a name="1john_4_15"></a>1 John 4:15

Whosoever shall [homologeō](../../strongs/g/g3670.md) that [Iēsous](../../strongs/g/g2424.md) is the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), [theos](../../strongs/g/g2316.md) [menō](../../strongs/g/g3306.md) in him, and he in [theos](../../strongs/g/g2316.md).

<a name="1john_4_16"></a>1 John 4:16

And we have [ginōskō](../../strongs/g/g1097.md) and [pisteuō](../../strongs/g/g4100.md) the [agapē](../../strongs/g/g26.md) that [theos](../../strongs/g/g2316.md) hath to us. [theos](../../strongs/g/g2316.md) is [agapē](../../strongs/g/g26.md); and he that [menō](../../strongs/g/g3306.md) in [agapē](../../strongs/g/g26.md) [menō](../../strongs/g/g3306.md) in [theos](../../strongs/g/g2316.md), and [theos](../../strongs/g/g2316.md) in him.

<a name="1john_4_17"></a>1 John 4:17

Herein is our [agapē](../../strongs/g/g26.md) [teleioō](../../strongs/g/g5048.md), that we may have [parrēsia](../../strongs/g/g3954.md) in the day of [krisis](../../strongs/g/g2920.md): because as he is, so are we in this [kosmos](../../strongs/g/g2889.md).

<a name="1john_4_18"></a>1 John 4:18

There is no [phobos](../../strongs/g/g5401.md) in [agapē](../../strongs/g/g26.md); but [teleios](../../strongs/g/g5046.md) [agapē](../../strongs/g/g26.md) [ballō](../../strongs/g/g906.md) out [phobos](../../strongs/g/g5401.md): because [phobos](../../strongs/g/g5401.md) hath [kolasis](../../strongs/g/g2851.md). He that [phobeō](../../strongs/g/g5399.md) is not [teleioō](../../strongs/g/g5048.md) in [agapē](../../strongs/g/g26.md).

<a name="1john_4_19"></a>1 John 4:19

We [agapaō](../../strongs/g/g25.md) him, because he first [agapaō](../../strongs/g/g25.md) us.

<a name="1john_4_20"></a>1 John 4:20

If [tis](../../strongs/g/g5100.md) [eipon](../../strongs/g/g2036.md), I [agapaō](../../strongs/g/g25.md) [theos](../../strongs/g/g2316.md), and [miseō](../../strongs/g/g3404.md) his [adelphos](../../strongs/g/g80.md), he is a [pseustēs](../../strongs/g/g5583.md): for he that [agapaō](../../strongs/g/g25.md) not his [adelphos](../../strongs/g/g80.md) whom he hath [horaō](../../strongs/g/g3708.md), how can he [agapaō](../../strongs/g/g25.md) [theos](../../strongs/g/g2316.md) whom he hath not [horaō](../../strongs/g/g3708.md)?

<a name="1john_4_21"></a>1 John 4:21

And this [entolē](../../strongs/g/g1785.md) have we from him, That he who [agapaō](../../strongs/g/g25.md) [theos](../../strongs/g/g2316.md) [agapaō](../../strongs/g/g25.md) his [adelphos](../../strongs/g/g80.md) also.

---

[Transliteral Bible](../bible.md)

[1 John](1john.md)

[1 John 3](1john_3.md) - [1 John 5](1john_5.md)

---

[^1]: [1 John 4:3 Commentary](../../commentary/1john/1john_4_commentary.md#1john_4_3)
