# [1 John 1](https://www.blueletterbible.org/kjv/1jo/1/1/s_1160001)

<a name="1john_1_1"></a>1 John 1:1

That which was from the [archē](../../strongs/g/g746.md), which we have [akouō](../../strongs/g/g191.md), which we have [horaō](../../strongs/g/g3708.md) with our [ophthalmos](../../strongs/g/g3788.md), which we have [theaomai](../../strongs/g/g2300.md), and our [cheir](../../strongs/g/g5495.md) have [psēlaphaō](../../strongs/g/g5584.md), of the [logos](../../strongs/g/g3056.md) of [zōē](../../strongs/g/g2222.md);

<a name="1john_1_2"></a>1 John 1:2

(For the [zōē](../../strongs/g/g2222.md) was [phaneroō](../../strongs/g/g5319.md), and we have [horaō](../../strongs/g/g3708.md), and [martyreō](../../strongs/g/g3140.md), and [apaggellō](../../strongs/g/g518.md) unto you that [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md), which was with the [patēr](../../strongs/g/g3962.md), and was [phaneroō](../../strongs/g/g5319.md) unto us;)

<a name="1john_1_3"></a>1 John 1:3

That which we have [horaō](../../strongs/g/g3708.md) and [akouō](../../strongs/g/g191.md) [apaggellō](../../strongs/g/g518.md) we unto you, that ye also may have [koinōnia](../../strongs/g/g2842.md) with us: and truly our [koinōnia](../../strongs/g/g2842.md) with the [patēr](../../strongs/g/g3962.md), and with his [huios](../../strongs/g/g5207.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="1john_1_4"></a>1 John 1:4

And these things [graphō](../../strongs/g/g1125.md) we unto you, that your [chara](../../strongs/g/g5479.md) may be [plēroō](../../strongs/g/g4137.md).

<a name="1john_1_5"></a>1 John 1:5

This then is the [epaggelia](../../strongs/g/g1860.md) which we have [akouō](../../strongs/g/g191.md) of him, and [anaggellō](../../strongs/g/g312.md) unto you, that [theos](../../strongs/g/g2316.md) is [phōs](../../strongs/g/g5457.md), and in him is no [skotia](../../strongs/g/g4653.md) at all.

<a name="1john_1_6"></a>1 John 1:6

If we [eipon](../../strongs/g/g2036.md) that we have [koinōnia](../../strongs/g/g2842.md) with him, and [peripateō](../../strongs/g/g4043.md) in [skotos](../../strongs/g/g4655.md), we [pseudomai](../../strongs/g/g5574.md), and [poieō](../../strongs/g/g4160.md) not the [alētheia](../../strongs/g/g225.md):

<a name="1john_1_7"></a>1 John 1:7

But if we [peripateō](../../strongs/g/g4043.md) in the [phōs](../../strongs/g/g5457.md), as he is in the [phōs](../../strongs/g/g5457.md), we have [koinōnia](../../strongs/g/g2842.md) with [allēlōn](../../strongs/g/g240.md), and the [haima](../../strongs/g/g129.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) his [huios](../../strongs/g/g5207.md) [katharizō](../../strongs/g/g2511.md) us from all [hamartia](../../strongs/g/g266.md).

<a name="1john_1_8"></a>1 John 1:8

If we [eipon](../../strongs/g/g2036.md) that we have no [hamartia](../../strongs/g/g266.md), we [planaō](../../strongs/g/g4105.md) ourselves, and the [alētheia](../../strongs/g/g225.md) is not in us.

<a name="1john_1_9"></a>1 John 1:9

If we [homologeō](../../strongs/g/g3670.md) our [hamartia](../../strongs/g/g266.md), he is [pistos](../../strongs/g/g4103.md) and [dikaios](../../strongs/g/g1342.md) to [aphiēmi](../../strongs/g/g863.md) us [hamartia](../../strongs/g/g266.md), and to [katharizō](../../strongs/g/g2511.md) us from all [adikia](../../strongs/g/g93.md).

<a name="1john_1_10"></a>1 John 1:10

If we [eipon](../../strongs/g/g2036.md) that we have not [hamartanō](../../strongs/g/g264.md), we [poieō](../../strongs/g/g4160.md) him a [pseustēs](../../strongs/g/g5583.md), and his [logos](../../strongs/g/g3056.md) is not in us.

---

[Transliteral Bible](../bible.md)

[1 John](1john.md)

[1 John 2](1john_2.md)