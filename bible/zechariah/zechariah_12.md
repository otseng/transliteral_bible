# [Zechariah 12](https://www.blueletterbible.org/kjv/zechariah/12)

<a name="zechariah_12_1"></a>Zechariah 12:1

The [maśśā'](../../strongs/h/h4853.md) of the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) for [Yisra'el](../../strongs/h/h3478.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), which [natah](../../strongs/h/h5186.md) the [shamayim](../../strongs/h/h8064.md), and [yacad](../../strongs/h/h3245.md) the ['erets](../../strongs/h/h776.md), and [yāṣar](../../strongs/h/h3335.md) the [ruwach](../../strongs/h/h7307.md) of ['āḏām](../../strongs/h/h120.md) [qereḇ](../../strongs/h/h7130.md) him.

<a name="zechariah_12_2"></a>Zechariah 12:2

Behold, I will [śûm](../../strongs/h/h7760.md) [Yĕruwshalaim](../../strongs/h/h3389.md) a [caph](../../strongs/h/h5592.md) of [raʿal](../../strongs/h/h7478.md) unto all the ['am](../../strongs/h/h5971.md) [cabiyb](../../strongs/h/h5439.md), when they shall be in the [māṣôr](../../strongs/h/h4692.md) both against [Yehuwdah](../../strongs/h/h3063.md) and against [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="zechariah_12_3"></a>Zechariah 12:3

And in that [yowm](../../strongs/h/h3117.md) will I [śûm](../../strongs/h/h7760.md) [Yĕruwshalaim](../../strongs/h/h3389.md) a [maʿămāsâ](../../strongs/h/h4614.md) ['eben](../../strongs/h/h68.md) for all ['am](../../strongs/h/h5971.md): all that [ʿāmas](../../strongs/h/h6006.md) themselves with it shall be [śāraṭ](../../strongs/h/h8295.md) [śāraṭ](../../strongs/h/h8295.md), though all the [gowy](../../strongs/h/h1471.md) of the ['erets](../../strongs/h/h776.md) be ['āsap̄](../../strongs/h/h622.md) against it.

<a name="zechariah_12_4"></a>Zechariah 12:4

In that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), I will [nakah](../../strongs/h/h5221.md) every [sûs](../../strongs/h/h5483.md) with [timâôn](../../strongs/h/h8541.md), and his [rāḵaḇ](../../strongs/h/h7392.md) with [šigāʿôn](../../strongs/h/h7697.md): and I will [paqach](../../strongs/h/h6491.md) mine ['ayin](../../strongs/h/h5869.md) upon the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md), and will [nakah](../../strongs/h/h5221.md) every [sûs](../../strongs/h/h5483.md) of the ['am](../../strongs/h/h5971.md) with [ʿiûārôn](../../strongs/h/h5788.md).

<a name="zechariah_12_5"></a>Zechariah 12:5

And the ['allûp̄](../../strongs/h/h441.md) of [Yehuwdah](../../strongs/h/h3063.md) shall ['āmar](../../strongs/h/h559.md) in their [leb](../../strongs/h/h3820.md), The [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) shall be my ['amṣâ](../../strongs/h/h556.md) in [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) their ['Elohiym](../../strongs/h/h430.md).

<a name="zechariah_12_6"></a>Zechariah 12:6

In that [yowm](../../strongs/h/h3117.md) will I [śûm](../../strongs/h/h7760.md) the ['allûp̄](../../strongs/h/h441.md) of [Yehuwdah](../../strongs/h/h3063.md) like a [kîyôr](../../strongs/h/h3595.md) of ['esh](../../strongs/h/h784.md) among the ['ets](../../strongs/h/h6086.md), and like a [lapîḏ](../../strongs/h/h3940.md) of ['esh](../../strongs/h/h784.md) in a [ʿāmîr](../../strongs/h/h5995.md); and they shall ['akal](../../strongs/h/h398.md) all the ['am](../../strongs/h/h5971.md) [cabiyb](../../strongs/h/h5439.md), on the [yamiyn](../../strongs/h/h3225.md) and on the [śᵊmō'l](../../strongs/h/h8040.md): and [Yĕruwshalaim](../../strongs/h/h3389.md) shall be [yashab](../../strongs/h/h3427.md) again in her own place, even in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="zechariah_12_7"></a>Zechariah 12:7

[Yĕhovah](../../strongs/h/h3068.md) also shall [yasha'](../../strongs/h/h3467.md) the ['ohel](../../strongs/h/h168.md) of [Yehuwdah](../../strongs/h/h3063.md) [ri'šôn](../../strongs/h/h7223.md), that the [tip̄'ārâ](../../strongs/h/h8597.md) of the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md) and the [tip̄'ārâ](../../strongs/h/h8597.md) of the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) do not [gāḏal](../../strongs/h/h1431.md) themselves against [Yehuwdah](../../strongs/h/h3063.md).

<a name="zechariah_12_8"></a>Zechariah 12:8

In that [yowm](../../strongs/h/h3117.md) shall [Yĕhovah](../../strongs/h/h3068.md) [gānan](../../strongs/h/h1598.md) the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md); and he that is [kashal](../../strongs/h/h3782.md) among them at that [yowm](../../strongs/h/h3117.md) shall be as [Dāviḏ](../../strongs/h/h1732.md); and the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md) shall be as ['Elohiym](../../strongs/h/h430.md), as the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [paniym](../../strongs/h/h6440.md) them.

<a name="zechariah_12_9"></a>Zechariah 12:9

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that I will [bāqaš](../../strongs/h/h1245.md) to [šāmaḏ](../../strongs/h/h8045.md) all the [gowy](../../strongs/h/h1471.md) that [bow'](../../strongs/h/h935.md) against [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="zechariah_12_10"></a>Zechariah 12:10

And I will [šāp̄aḵ](../../strongs/h/h8210.md) upon the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md), and upon the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), the [ruwach](../../strongs/h/h7307.md) of [ḥēn](../../strongs/h/h2580.md) and of [taḥănûn](../../strongs/h/h8469.md): and they shall [nabat](../../strongs/h/h5027.md) upon me whom they have [dāqar](../../strongs/h/h1856.md), and they shall [mispēḏ](../../strongs/h/h4553.md) for him, as one [sāp̄aḏ](../../strongs/h/h5594.md) for his [yāḥîḏ](../../strongs/h/h3173.md) son, and shall be in [mārar](../../strongs/h/h4843.md) for him, as one that is in [mārar](../../strongs/h/h4843.md) for his [bᵊḵôr](../../strongs/h/h1060.md).

<a name="zechariah_12_11"></a>Zechariah 12:11

In that [yowm](../../strongs/h/h3117.md) shall there be a [gāḏal](../../strongs/h/h1431.md) [mispēḏ](../../strongs/h/h4553.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), as the [mispēḏ](../../strongs/h/h4553.md) of [Hăḏaḏrimmôn](../../strongs/h/h1910.md) in the [biqʿâ](../../strongs/h/h1237.md) of [Mᵊḡidôn](../../strongs/h/h4023.md).

<a name="zechariah_12_12"></a>Zechariah 12:12

And the ['erets](../../strongs/h/h776.md) shall [sāp̄aḏ](../../strongs/h/h5594.md), [mišpāḥâ](../../strongs/h/h4940.md) [mišpāḥâ](../../strongs/h/h4940.md) [baḏ](../../strongs/h/h905.md); the [mišpāḥâ](../../strongs/h/h4940.md) of the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md) apart, and their ['ishshah](../../strongs/h/h802.md) apart; the [mišpāḥâ](../../strongs/h/h4940.md) of the [bayith](../../strongs/h/h1004.md) of [Nāṯān](../../strongs/h/h5416.md) apart, and their ['ishshah](../../strongs/h/h802.md) apart;

<a name="zechariah_12_13"></a>Zechariah 12:13

The [mišpāḥâ](../../strongs/h/h4940.md) of the [bayith](../../strongs/h/h1004.md) of [Lēvî](../../strongs/h/h3878.md) apart, and their ['ishshah](../../strongs/h/h802.md) apart; the [mišpāḥâ](../../strongs/h/h4940.md) of [Šimʿî](../../strongs/h/h8097.md) apart, and their ['ishshah](../../strongs/h/h802.md) apart;

<a name="zechariah_12_14"></a>Zechariah 12:14

All the [mišpāḥâ](../../strongs/h/h4940.md) that [šā'ar](../../strongs/h/h7604.md), every [mišpāḥâ](../../strongs/h/h4940.md) apart, and their ['ishshah](../../strongs/h/h802.md) apart.

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 11](zechariah_11.md) - [Zechariah 13](zechariah_13.md)