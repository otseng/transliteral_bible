# [Zechariah 6](https://www.blueletterbible.org/kjv/zechariah/6)

<a name="zechariah_6_1"></a>Zechariah 6:1

And I [shuwb](../../strongs/h/h7725.md), and [nasa'](../../strongs/h/h5375.md) mine ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md), and, behold, there [yāṣā'](../../strongs/h/h3318.md) four [merkāḇâ](../../strongs/h/h4818.md) [yāṣā'](../../strongs/h/h3318.md) from between two [har](../../strongs/h/h2022.md); and the [har](../../strongs/h/h2022.md) were [har](../../strongs/h/h2022.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="zechariah_6_2"></a>Zechariah 6:2

In the [ri'šôn](../../strongs/h/h7223.md) [merkāḇâ](../../strongs/h/h4818.md) were ['āḏōm](../../strongs/h/h122.md) [sûs](../../strongs/h/h5483.md); and in the second [merkāḇâ](../../strongs/h/h4818.md) [šāḥōr](../../strongs/h/h7838.md) [sûs](../../strongs/h/h5483.md);

<a name="zechariah_6_3"></a>Zechariah 6:3

And in the third [merkāḇâ](../../strongs/h/h4818.md) [lāḇān](../../strongs/h/h3836.md) [sûs](../../strongs/h/h5483.md); and in the fourth [merkāḇâ](../../strongs/h/h4818.md) [bārōḏ](../../strongs/h/h1261.md) and ['āmōṣ](../../strongs/h/h554.md) [sûs](../../strongs/h/h5483.md).

<a name="zechariah_6_4"></a>Zechariah 6:4

Then I ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto the [mal'ak](../../strongs/h/h4397.md) that [dabar](../../strongs/h/h1696.md) with me, What are these, my ['adown](../../strongs/h/h113.md)?

<a name="zechariah_6_5"></a>Zechariah 6:5

And the [mal'ak](../../strongs/h/h4397.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto me, These are the four [ruwach](../../strongs/h/h7307.md) of the [shamayim](../../strongs/h/h8064.md), which [yāṣā'](../../strongs/h/h3318.md) from [yatsab](../../strongs/h/h3320.md) before the ['adown](../../strongs/h/h113.md) of all the ['erets](../../strongs/h/h776.md).

<a name="zechariah_6_6"></a>Zechariah 6:6

The [šāḥōr](../../strongs/h/h7838.md) [sûs](../../strongs/h/h5483.md) which are therein [yāṣā'](../../strongs/h/h3318.md) into the [ṣāp̄ôn](../../strongs/h/h6828.md) ['erets](../../strongs/h/h776.md); and the [lāḇān](../../strongs/h/h3836.md) [yāṣā'](../../strongs/h/h3318.md) ['aḥar](../../strongs/h/h310.md) them; and the [bārōḏ](../../strongs/h/h1261.md) [yāṣā'](../../strongs/h/h3318.md) toward the [têmān](../../strongs/h/h8486.md) ['erets](../../strongs/h/h776.md).

<a name="zechariah_6_7"></a>Zechariah 6:7

And the ['āmōṣ](../../strongs/h/h554.md) [yāṣā'](../../strongs/h/h3318.md), and [bāqaš](../../strongs/h/h1245.md) to [yālaḵ](../../strongs/h/h3212.md) that they might [halak](../../strongs/h/h1980.md) through the ['erets](../../strongs/h/h776.md): and he ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), [halak](../../strongs/h/h1980.md) through the ['erets](../../strongs/h/h776.md). So they [halak](../../strongs/h/h1980.md) through the ['erets](../../strongs/h/h776.md).

<a name="zechariah_6_8"></a>Zechariah 6:8

Then [zāʿaq](../../strongs/h/h2199.md) he upon me, and [dabar](../../strongs/h/h1696.md) unto me, ['āmar](../../strongs/h/h559.md), [ra'ah](../../strongs/h/h7200.md), these that [yāṣā'](../../strongs/h/h3318.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md) ['erets](../../strongs/h/h776.md) have [nuwach](../../strongs/h/h5117.md) my [ruwach](../../strongs/h/h7307.md) in the [ṣāp̄ôn](../../strongs/h/h6828.md) ['erets](../../strongs/h/h776.md).

<a name="zechariah_6_9"></a>Zechariah 6:9

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="zechariah_6_10"></a>Zechariah 6:10

[laqach](../../strongs/h/h3947.md) of them of the [gôlâ](../../strongs/h/h1473.md), even of [Ḥelday](../../strongs/h/h2469.md), of [Ṭôḇîyâ](../../strongs/h/h2900.md), and of [YᵊḏaʿYâ](../../strongs/h/h3048.md), which are [bow'](../../strongs/h/h935.md) from [Bāḇel](../../strongs/h/h894.md), and [bow'](../../strongs/h/h935.md) thou the same [yowm](../../strongs/h/h3117.md), and [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊp̄Anyâ](../../strongs/h/h6846.md);

<a name="zechariah_6_11"></a>Zechariah 6:11

Then [laqach](../../strongs/h/h3947.md) [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md), and ['asah](../../strongs/h/h6213.md) [ʿăṭārâ](../../strongs/h/h5850.md), and [śûm](../../strongs/h/h7760.md) them upon the [ro'sh](../../strongs/h/h7218.md) of [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôṣāḏāq](../../strongs/h/h3087.md), the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md);

<a name="zechariah_6_12"></a>Zechariah 6:12

And ['āmar](../../strongs/h/h559.md) unto him, ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), ['āmar](../../strongs/h/h559.md), Behold the ['iysh](../../strongs/h/h376.md) whose [shem](../../strongs/h/h8034.md) is the [ṣemaḥ](../../strongs/h/h6780.md); and he shall grow [ṣāmaḥ](../../strongs/h/h6779.md) out of his place, and he shall [bānâ](../../strongs/h/h1129.md) the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md):

<a name="zechariah_6_13"></a>Zechariah 6:13

Even he shall [bānâ](../../strongs/h/h1129.md) the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md); and he shall [nasa'](../../strongs/h/h5375.md) the [howd](../../strongs/h/h1935.md), and shall [yashab](../../strongs/h/h3427.md) and [mashal](../../strongs/h/h4910.md) upon his [kicce'](../../strongs/h/h3678.md); and he shall be a [kōhēn](../../strongs/h/h3548.md) upon his [kicce'](../../strongs/h/h3678.md): and the ['etsah](../../strongs/h/h6098.md) of [shalowm](../../strongs/h/h7965.md) shall be between them both.

<a name="zechariah_6_14"></a>Zechariah 6:14

And the [ʿăṭārâ](../../strongs/h/h5850.md) shall be to [Ḥēlem](../../strongs/h/h2494.md), and to [Ṭôḇîyâ](../../strongs/h/h2900.md), and to [YᵊḏaʿYâ](../../strongs/h/h3048.md), and to [Ḥēn](../../strongs/h/h2581.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊp̄Anyâ](../../strongs/h/h6846.md), for a [zikārôn](../../strongs/h/h2146.md) in the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="zechariah_6_15"></a>Zechariah 6:15

And they that are far [rachowq](../../strongs/h/h7350.md) shall [bow'](../../strongs/h/h935.md) and [bānâ](../../strongs/h/h1129.md) in the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md), and ye shall [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) hath [shalach](../../strongs/h/h7971.md) me unto you. And this shall come to pass, if ye will [shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 5](zechariah_5.md) - [Zechariah 7](zechariah_7.md)