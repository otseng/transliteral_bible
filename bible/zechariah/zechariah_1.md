# [Zechariah 1](https://www.blueletterbible.org/kjv/zechariah/1)

<a name="zechariah_1_1"></a>Zechariah 1:1

In the eighth [ḥōḏeš](../../strongs/h/h2320.md), in the second [šānâ](../../strongs/h/h8141.md) of [Daryāveš](../../strongs/h/h1867.md), came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto [Zᵊḵaryâ](../../strongs/h/h2148.md), the [ben](../../strongs/h/h1121.md) of [Bereḵyâ](../../strongs/h/h1296.md), the [ben](../../strongs/h/h1121.md) of [ʿIdô](../../strongs/h/h5714.md) the [nāḇî'](../../strongs/h/h5030.md), ['āmar](../../strongs/h/h559.md),

<a name="zechariah_1_2"></a>Zechariah 1:2

[Yĕhovah](../../strongs/h/h3068.md) hath been [qeṣep̄](../../strongs/h/h7110.md) [qāṣap̄](../../strongs/h/h7107.md) with your ['ab](../../strongs/h/h1.md).

<a name="zechariah_1_3"></a>Zechariah 1:3

Therefore ['āmar](../../strongs/h/h559.md) thou unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); [shuwb](../../strongs/h/h7725.md) ye unto me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), and I will [shuwb](../../strongs/h/h7725.md) unto you, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="zechariah_1_4"></a>Zechariah 1:4

Be ye not as your ['ab](../../strongs/h/h1.md), unto whom the [ri'šôn](../../strongs/h/h7223.md) [nāḇî'](../../strongs/h/h5030.md) have [qara'](../../strongs/h/h7121.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); [shuwb](../../strongs/h/h7725.md) ye now from your [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md), and from your [ra'](../../strongs/h/h7451.md) [maʿălāl](../../strongs/h/h4611.md): but they did not [shama'](../../strongs/h/h8085.md), nor [qashab](../../strongs/h/h7181.md) unto me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="zechariah_1_5"></a>Zechariah 1:5

Your ['ab](../../strongs/h/h1.md), where are they? and the [nāḇî'](../../strongs/h/h5030.md), do they [ḥāyâ](../../strongs/h/h2421.md) ['owlam](../../strongs/h/h5769.md)?

<a name="zechariah_1_6"></a>Zechariah 1:6

But my [dabar](../../strongs/h/h1697.md) and my [choq](../../strongs/h/h2706.md), which I [tsavah](../../strongs/h/h6680.md) my ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md), did they not take [nāśaḡ](../../strongs/h/h5381.md) of your ['ab](../../strongs/h/h1.md)? and they [shuwb](../../strongs/h/h7725.md) and ['āmar](../../strongs/h/h559.md), Like as [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) [zāmam](../../strongs/h/h2161.md) to ['asah](../../strongs/h/h6213.md) unto us, according to our [derek](../../strongs/h/h1870.md), and according to our [maʿălāl](../../strongs/h/h4611.md), so hath he ['asah](../../strongs/h/h6213.md) with us.

<a name="zechariah_1_7"></a>Zechariah 1:7

Upon the four and twentieth [yowm](../../strongs/h/h3117.md) of the eleventh [ḥōḏeš](../../strongs/h/h2320.md), which is the [ḥōḏeš](../../strongs/h/h2320.md) [šᵊḇāṭ](../../strongs/h/h7627.md), in the second [šānâ](../../strongs/h/h8141.md) of [Daryāveš](../../strongs/h/h1867.md), came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto [Zᵊḵaryâ](../../strongs/h/h2148.md), the [ben](../../strongs/h/h1121.md) of [Bereḵyâ](../../strongs/h/h1296.md), the [ben](../../strongs/h/h1121.md) of [ʿIdô](../../strongs/h/h5714.md) the [nāḇî'](../../strongs/h/h5030.md), ['āmar](../../strongs/h/h559.md),

<a name="zechariah_1_8"></a>Zechariah 1:8

I [ra'ah](../../strongs/h/h7200.md) by [layil](../../strongs/h/h3915.md), and behold an ['iysh](../../strongs/h/h376.md) [rāḵaḇ](../../strongs/h/h7392.md) upon an ['āḏōm](../../strongs/h/h122.md) [sûs](../../strongs/h/h5483.md), and he ['amad](../../strongs/h/h5975.md) among the [hăḏas](../../strongs/h/h1918.md) that were in the [mᵊṣullâ](../../strongs/h/h4699.md); and ['aḥar](../../strongs/h/h310.md) him were there ['āḏōm](../../strongs/h/h122.md) [sûs](../../strongs/h/h5483.md), [śārōq](../../strongs/h/h8320.md), and [lāḇān](../../strongs/h/h3836.md).

<a name="zechariah_1_9"></a>Zechariah 1:9

Then ['āmar](../../strongs/h/h559.md) I, O my ['adown](../../strongs/h/h113.md), what are these? And the [mal'ak](../../strongs/h/h4397.md) that [dabar](../../strongs/h/h1696.md) with me ['āmar](../../strongs/h/h559.md) unto me, I will [ra'ah](../../strongs/h/h7200.md) thee what these be.

<a name="zechariah_1_10"></a>Zechariah 1:10

And the ['iysh](../../strongs/h/h376.md) that ['amad](../../strongs/h/h5975.md) among the [hăḏas](../../strongs/h/h1918.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), These are they whom [Yĕhovah](../../strongs/h/h3068.md) hath [shalach](../../strongs/h/h7971.md) to [halak](../../strongs/h/h1980.md) to and fro through the ['erets](../../strongs/h/h776.md).

<a name="zechariah_1_11"></a>Zechariah 1:11

And they ['anah](../../strongs/h/h6030.md) the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) that ['amad](../../strongs/h/h5975.md) among the [hăḏas](../../strongs/h/h1918.md), and ['āmar](../../strongs/h/h559.md), We have walked to and [halak](../../strongs/h/h1980.md) through the ['erets](../../strongs/h/h776.md), and, behold, all the ['erets](../../strongs/h/h776.md) sitteth [yashab](../../strongs/h/h3427.md), and is at [šāqaṭ](../../strongs/h/h8252.md).

<a name="zechariah_1_12"></a>Zechariah 1:12

Then the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), how long wilt thou not have [racham](../../strongs/h/h7355.md) on [Yĕruwshalaim](../../strongs/h/h3389.md) and on the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), against which thou hast had [za'am](../../strongs/h/h2194.md) these threescore and ten [šānâ](../../strongs/h/h8141.md)?

<a name="zechariah_1_13"></a>Zechariah 1:13

And [Yĕhovah](../../strongs/h/h3068.md) ['anah](../../strongs/h/h6030.md) the [mal'ak](../../strongs/h/h4397.md) that [dabar](../../strongs/h/h1696.md) with me with [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md) and [niḥum](../../strongs/h/h5150.md) [dabar](../../strongs/h/h1697.md).

<a name="zechariah_1_14"></a>Zechariah 1:14

So the [mal'ak](../../strongs/h/h4397.md) that [dabar](../../strongs/h/h1696.md) with me ['āmar](../../strongs/h/h559.md) unto me, [qara'](../../strongs/h/h7121.md) thou, ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); I am [qānā'](../../strongs/h/h7065.md) for [Yĕruwshalaim](../../strongs/h/h3389.md) and for [Tsiyown](../../strongs/h/h6726.md) with a [gadowl](../../strongs/h/h1419.md) [qin'â](../../strongs/h/h7068.md).

<a name="zechariah_1_15"></a>Zechariah 1:15

And I am [gadowl](../../strongs/h/h1419.md) [qeṣep̄](../../strongs/h/h7110.md) [qāṣap̄](../../strongs/h/h7107.md) with the [gowy](../../strongs/h/h1471.md) that are at [ša'ănān](../../strongs/h/h7600.md): for I was but a [mᵊʿaṭ](../../strongs/h/h4592.md) [qāṣap̄](../../strongs/h/h7107.md), and they [ʿāzar](../../strongs/h/h5826.md) forward the [ra'](../../strongs/h/h7451.md).

<a name="zechariah_1_16"></a>Zechariah 1:16

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); I am [shuwb](../../strongs/h/h7725.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) with [raḥam](../../strongs/h/h7356.md): my [bayith](../../strongs/h/h1004.md) shall be [bānâ](../../strongs/h/h1129.md) in it, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), and a [qāv](../../strongs/h/h6957.md) [qevê](../../strongs/h/h6961.md) shall be [natah](../../strongs/h/h5186.md) upon [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="zechariah_1_17"></a>Zechariah 1:17

[qara'](../../strongs/h/h7121.md) yet, ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); My [ʿîr](../../strongs/h/h5892.md) through [towb](../../strongs/h/h2896.md) shall yet be [puwts](../../strongs/h/h6327.md); and [Yĕhovah](../../strongs/h/h3068.md) shall yet [nacham](../../strongs/h/h5162.md) [Tsiyown](../../strongs/h/h6726.md), and shall yet [bāḥar](../../strongs/h/h977.md) [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="zechariah_1_18"></a>Zechariah 1:18

Then I [nasa'](../../strongs/h/h5375.md) mine ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md), and behold four [qeren](../../strongs/h/h7161.md).

<a name="zechariah_1_19"></a>Zechariah 1:19

And I ['āmar](../../strongs/h/h559.md) unto the [mal'ak](../../strongs/h/h4397.md) that [dabar](../../strongs/h/h1696.md) with me, What be these? And he ['āmar](../../strongs/h/h559.md) me, These are the [qeren](../../strongs/h/h7161.md) which have [zārâ](../../strongs/h/h2219.md) [Yehuwdah](../../strongs/h/h3063.md), [Yisra'el](../../strongs/h/h3478.md), and [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="zechariah_1_20"></a>Zechariah 1:20

And [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) me four [ḥārāš](../../strongs/h/h2796.md).

<a name="zechariah_1_21"></a>Zechariah 1:21

Then ['āmar](../../strongs/h/h559.md) I, What [bow'](../../strongs/h/h935.md) these to ['asah](../../strongs/h/h6213.md)? And he ['āmar](../../strongs/h/h559.md), ['āmar](../../strongs/h/h559.md), These are the [qeren](../../strongs/h/h7161.md) which have [zārâ](../../strongs/h/h2219.md) [Yehuwdah](../../strongs/h/h3063.md), so [peh](../../strongs/h/h6310.md) no ['iysh](../../strongs/h/h376.md) did [nasa'](../../strongs/h/h5375.md) his [ro'sh](../../strongs/h/h7218.md): but these are [bow'](../../strongs/h/h935.md) to [ḥārēḏ](../../strongs/h/h2729.md) them, to [yadah](../../strongs/h/h3034.md) the [qeren](../../strongs/h/h7161.md) of the [gowy](../../strongs/h/h1471.md), which [nasa'](../../strongs/h/h5375.md) their [qeren](../../strongs/h/h7161.md) over the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md) to [zārâ](../../strongs/h/h2219.md) it.

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 2](zechariah_2.md)