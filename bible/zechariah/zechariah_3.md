# [Zechariah 3](https://www.blueletterbible.org/kjv/zechariah/3)

<a name="zechariah_3_1"></a>Zechariah 3:1

And he [ra'ah](../../strongs/h/h7200.md) me [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md) ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md), and [satan](../../strongs/h/h7854.md) ['amad](../../strongs/h/h5975.md) at his [yamiyn](../../strongs/h/h3225.md) to [śāṭan](../../strongs/h/h7853.md) him.

<a name="zechariah_3_2"></a>Zechariah 3:2

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [satan](../../strongs/h/h7854.md), [Yĕhovah](../../strongs/h/h3068.md) [gāʿar](../../strongs/h/h1605.md) thee, O [satan](../../strongs/h/h7854.md); even [Yĕhovah](../../strongs/h/h3068.md) that hath [bāḥar](../../strongs/h/h977.md) [Yĕruwshalaim](../../strongs/h/h3389.md) [gāʿar](../../strongs/h/h1605.md) thee: is not this a ['ûḏ](../../strongs/h/h181.md) [natsal](../../strongs/h/h5337.md) out of the ['esh](../../strongs/h/h784.md)?

<a name="zechariah_3_3"></a>Zechariah 3:3

Now [Yᵊhôšûaʿ](../../strongs/h/h3091.md) was [labash](../../strongs/h/h3847.md) with [ṣ'y](../../strongs/h/h6674.md) [beḡeḏ](../../strongs/h/h899.md), and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the [mal'ak](../../strongs/h/h4397.md).

<a name="zechariah_3_4"></a>Zechariah 3:4

And he ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto those that ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) him, ['āmar](../../strongs/h/h559.md), [cuwr](../../strongs/h/h5493.md) the [ṣ'y](../../strongs/h/h6674.md) [beḡeḏ](../../strongs/h/h899.md) from him. And unto him he ['āmar](../../strongs/h/h559.md), [ra'ah](../../strongs/h/h7200.md), I have caused thine ['avon](../../strongs/h/h5771.md) to ['abar](../../strongs/h/h5674.md) from thee, and I will [labash](../../strongs/h/h3847.md) thee with change of [maḥălāṣôṯ](../../strongs/h/h4254.md).

<a name="zechariah_3_5"></a>Zechariah 3:5

And I ['āmar](../../strongs/h/h559.md), Let them [śûm](../../strongs/h/h7760.md) a [tahowr](../../strongs/h/h2889.md) [ṣānîp̄](../../strongs/h/h6797.md) upon his [ro'sh](../../strongs/h/h7218.md). So they [śûm](../../strongs/h/h7760.md) a [tahowr](../../strongs/h/h2889.md) [ṣānîp̄](../../strongs/h/h6797.md) upon his [ro'sh](../../strongs/h/h7218.md), and [labash](../../strongs/h/h3847.md) him with [beḡeḏ](../../strongs/h/h899.md). And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['amad](../../strongs/h/h5975.md).

<a name="zechariah_3_6"></a>Zechariah 3:6

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [ʿûḏ](../../strongs/h/h5749.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), ['āmar](../../strongs/h/h559.md),

<a name="zechariah_3_7"></a>Zechariah 3:7

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); If thou wilt [yālaḵ](../../strongs/h/h3212.md) in my [derek](../../strongs/h/h1870.md), and if thou wilt [shamar](../../strongs/h/h8104.md) my [mišmereṯ](../../strongs/h/h4931.md), then thou shalt also [diyn](../../strongs/h/h1777.md) my [bayith](../../strongs/h/h1004.md), and shalt also [shamar](../../strongs/h/h8104.md) my [ḥāṣēr](../../strongs/h/h2691.md), and I will [nathan](../../strongs/h/h5414.md) thee places to [mahălāḵ](../../strongs/h/h4108.md) among these that ['amad](../../strongs/h/h5975.md).

<a name="zechariah_3_8"></a>Zechariah 3:8

[shama'](../../strongs/h/h8085.md) now, O [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md), thou, and thy [rea'](../../strongs/h/h7453.md) that [yashab](../../strongs/h/h3427.md) [paniym](../../strongs/h/h6440.md) thee: for they are ['enowsh](../../strongs/h/h582.md) wondered [môp̄ēṯ](../../strongs/h/h4159.md): for, behold, I will bring [bow'](../../strongs/h/h935.md) my ['ebed](../../strongs/h/h5650.md) the [ṣemaḥ](../../strongs/h/h6780.md).

<a name="zechariah_3_9"></a>Zechariah 3:9

For behold the ['eben](../../strongs/h/h68.md) that I have [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md); upon one ['eben](../../strongs/h/h68.md) shall be seven ['ayin](../../strongs/h/h5869.md): behold, I will [pāṯaḥ](../../strongs/h/h6605.md) the [pitûaḥ](../../strongs/h/h6603.md) thereof, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), and I will [mûš](../../strongs/h/h4185.md) the ['avon](../../strongs/h/h5771.md) of that ['erets](../../strongs/h/h776.md) in one [yowm](../../strongs/h/h3117.md).

<a name="zechariah_3_10"></a>Zechariah 3:10R

In that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), shall ye [qara'](../../strongs/h/h7121.md) every ['iysh](../../strongs/h/h376.md) his [rea'](../../strongs/h/h7453.md) under the [gep̄en](../../strongs/h/h1612.md) and under the [tĕ'en](../../strongs/h/h8384.md).

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 2](zechariah_2.md) - [Zechariah 4](zechariah_4.md)