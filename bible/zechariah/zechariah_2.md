# [Zechariah 2](https://www.blueletterbible.org/kjv/zechariah/2)

<a name="zechariah_2_1"></a>Zechariah 2:1

I [nasa'](../../strongs/h/h5375.md) mine ['ayin](../../strongs/h/h5869.md) again, and [ra'ah](../../strongs/h/h7200.md), and behold an ['iysh](../../strongs/h/h376.md) with a [midâ](../../strongs/h/h4060.md) [chebel](../../strongs/h/h2256.md) in his [yad](../../strongs/h/h3027.md).

<a name="zechariah_2_2"></a>Zechariah 2:2

Then ['āmar](../../strongs/h/h559.md) I, Whither [halak](../../strongs/h/h1980.md) thou? And he ['āmar](../../strongs/h/h559.md) unto me, To [māḏaḏ](../../strongs/h/h4058.md) [Yĕruwshalaim](../../strongs/h/h3389.md), to [ra'ah](../../strongs/h/h7200.md) what is the [rōḥaḇ](../../strongs/h/h7341.md) thereof, and what is the ['ōreḵ](../../strongs/h/h753.md) thereof.

<a name="zechariah_2_3"></a>Zechariah 2:3

And, behold, the [mal'ak](../../strongs/h/h4397.md) that [dabar](../../strongs/h/h1696.md) with me [yāṣā'](../../strongs/h/h3318.md), and ['aḥēr](../../strongs/h/h312.md) [mal'ak](../../strongs/h/h4397.md) [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) him,

<a name="zechariah_2_4"></a>Zechariah 2:4

And ['āmar](../../strongs/h/h559.md) unto him, [rûṣ](../../strongs/h/h7323.md), [dabar](../../strongs/h/h1696.md) to [hallāz](../../strongs/h/h1975.md) [naʿar](../../strongs/h/h5288.md), ['āmar](../../strongs/h/h559.md), [Yĕruwshalaim](../../strongs/h/h3389.md) shall be [yashab](../../strongs/h/h3427.md) as towns without [p̄rvzym](../../strongs/h/h6519.md) for the [rōḇ](../../strongs/h/h7230.md) of ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md) [tavek](../../strongs/h/h8432.md):

<a name="zechariah_2_5"></a>Zechariah 2:5

For I, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), will be unto her a [ḥômâ](../../strongs/h/h2346.md) of ['esh](../../strongs/h/h784.md) [cabiyb](../../strongs/h/h5439.md), and will be the [kabowd](../../strongs/h/h3519.md) in the [tavek](../../strongs/h/h8432.md) of her.

<a name="zechariah_2_6"></a>Zechariah 2:6

[hôy](../../strongs/h/h1945.md), [hôy](../../strongs/h/h1945.md), come forth, and [nûs](../../strongs/h/h5127.md) from the ['erets](../../strongs/h/h776.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): for I have spread you [pāraś](../../strongs/h/h6566.md) as the four [ruwach](../../strongs/h/h7307.md) of the [shamayim](../../strongs/h/h8064.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="zechariah_2_7"></a>Zechariah 2:7

[mālaṭ](../../strongs/h/h4422.md) thyself, [hôy](../../strongs/h/h1945.md) [Tsiyown](../../strongs/h/h6726.md), that [yashab](../../strongs/h/h3427.md) with the [bath](../../strongs/h/h1323.md) of [Bāḇel](../../strongs/h/h894.md).

<a name="zechariah_2_8"></a>Zechariah 2:8

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); ['aḥar](../../strongs/h/h310.md) the [kabowd](../../strongs/h/h3519.md) hath he [shalach](../../strongs/h/h7971.md) me unto the [gowy](../../strongs/h/h1471.md) which [šālal](../../strongs/h/h7997.md) you: for he that [naga'](../../strongs/h/h5060.md) you [naga'](../../strongs/h/h5060.md) the [bāḇâ](../../strongs/h/h892.md) of his ['ayin](../../strongs/h/h5869.md).

<a name="zechariah_2_9"></a>Zechariah 2:9

For, behold, I will [nûp̄](../../strongs/h/h5130.md) mine [yad](../../strongs/h/h3027.md) upon them, and they shall be a [šālāl](../../strongs/h/h7998.md) to their ['abad](../../strongs/h/h5647.md): and ye shall [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) hath [shalach](../../strongs/h/h7971.md) me.

<a name="zechariah_2_10"></a>Zechariah 2:10

[ranan](../../strongs/h/h7442.md) and [samach](../../strongs/h/h8055.md), O [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md): for, lo, I [bow'](../../strongs/h/h935.md), and I will [shakan](../../strongs/h/h7931.md) in the [tavek](../../strongs/h/h8432.md) of thee, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="zechariah_2_11"></a>Zechariah 2:11

And [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md) shall be [lāvâ](../../strongs/h/h3867.md) to [Yĕhovah](../../strongs/h/h3068.md) in that [yowm](../../strongs/h/h3117.md), and shall be my ['am](../../strongs/h/h5971.md): and I will [shakan](../../strongs/h/h7931.md) in the [tavek](../../strongs/h/h8432.md) of thee, and thou shalt [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) hath [shalach](../../strongs/h/h7971.md) me unto thee.

<a name="zechariah_2_12"></a>Zechariah 2:12

And [Yĕhovah](../../strongs/h/h3068.md) shall [nāḥal](../../strongs/h/h5157.md) [Yehuwdah](../../strongs/h/h3063.md) his [cheleq](../../strongs/h/h2506.md) in the [qodesh](../../strongs/h/h6944.md) ['ăḏāmâ](../../strongs/h/h127.md), and shall [bāḥar](../../strongs/h/h977.md) [Yĕruwshalaim](../../strongs/h/h3389.md) again.

<a name="zechariah_2_13"></a>Zechariah 2:13

Be [hāsâ](../../strongs/h/h2013.md), O all [basar](../../strongs/h/h1320.md), [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): for he is raised [ʿûr](../../strongs/h/h5782.md) out of his [qodesh](../../strongs/h/h6944.md) [māʿôn](../../strongs/h/h4583.md).

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 1](zechariah_1.md) - [Zechariah 3](zechariah_3.md)