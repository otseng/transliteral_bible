# [Zechariah 7](https://www.blueletterbible.org/kjv/zechariah/7)

<a name="zechariah_7_1"></a>Zechariah 7:1

And it came to pass in the fourth [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [Daryāveš](../../strongs/h/h1867.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto [Zᵊḵaryâ](../../strongs/h/h2148.md) in the fourth day of the ninth [ḥōḏeš](../../strongs/h/h2320.md), even in [Kislēv](../../strongs/h/h3691.md);

<a name="zechariah_7_2"></a>Zechariah 7:2

When they had [shalach](../../strongs/h/h7971.md) unto the [bayith](../../strongs/h/h1004.md) of ['el](../../strongs/h/h410.md) [Šar'eṣer](../../strongs/h/h8272.md) and [Reḡem Meleḵ](../../strongs/h/h7278.md), and their ['enowsh](../../strongs/h/h582.md), to [ḥālâ](../../strongs/h/h2470.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md),

<a name="zechariah_7_3"></a>Zechariah 7:3

And to ['āmar](../../strongs/h/h559.md) unto the [kōhēn](../../strongs/h/h3548.md) which were in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), and to the [nāḇî'](../../strongs/h/h5030.md), ['āmar](../../strongs/h/h559.md), Should I [bāḵâ](../../strongs/h/h1058.md) in the fifth [ḥōḏeš](../../strongs/h/h2320.md), [nāzar](../../strongs/h/h5144.md) myself, as I have ['asah](../../strongs/h/h6213.md) these so many [šānâ](../../strongs/h/h8141.md)?

<a name="zechariah_7_4"></a>Zechariah 7:4

Then came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) unto me, ['āmar](../../strongs/h/h559.md),

<a name="zechariah_7_5"></a>Zechariah 7:5

['āmar](../../strongs/h/h559.md) unto all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), and to the [kōhēn](../../strongs/h/h3548.md), ['āmar](../../strongs/h/h559.md), When ye [ṣûm](../../strongs/h/h6684.md) and [sāp̄aḏ](../../strongs/h/h5594.md) in the fifth and seventh month, even those seventy [šānâ](../../strongs/h/h8141.md), did ye at [ṣûm](../../strongs/h/h6684.md) [ṣûm](../../strongs/h/h6684.md) unto me, even to me?

<a name="zechariah_7_6"></a>Zechariah 7:6

And when ye did ['akal](../../strongs/h/h398.md), and when ye did [šāṯâ](../../strongs/h/h8354.md), did not ye ['akal](../../strongs/h/h398.md) for yourselves, and [šāṯâ](../../strongs/h/h8354.md) for yourselves?

<a name="zechariah_7_7"></a>Zechariah 7:7

Should ye not hear the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [qara'](../../strongs/h/h7121.md) [yad](../../strongs/h/h3027.md) the [ri'šôn](../../strongs/h/h7223.md) [nāḇî'](../../strongs/h/h5030.md), when [Yĕruwshalaim](../../strongs/h/h3389.md) was [yashab](../../strongs/h/h3427.md) and in [šālēv](../../strongs/h/h7961.md), and the [ʿîr](../../strongs/h/h5892.md) thereof round about [cabiyb](../../strongs/h/h5439.md), when [yashab](../../strongs/h/h3427.md) the [neḡeḇ](../../strongs/h/h5045.md) and the [šᵊp̄ēlâ](../../strongs/h/h8219.md)?

<a name="zechariah_7_8"></a>Zechariah 7:8

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto [Zᵊḵaryâ](../../strongs/h/h2148.md), ['āmar](../../strongs/h/h559.md),

<a name="zechariah_7_9"></a>Zechariah 7:9

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), ['āmar](../../strongs/h/h559.md), [shaphat](../../strongs/h/h8199.md) ['emeth](../../strongs/h/h571.md) [mishpat](../../strongs/h/h4941.md), and ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) and [raḥam](../../strongs/h/h7356.md) every ['iysh](../../strongs/h/h376.md) to his ['ach](../../strongs/h/h251.md):

<a name="zechariah_7_10"></a>Zechariah 7:10

And [ʿāšaq](../../strongs/h/h6231.md) not the ['almānâ](../../strongs/h/h490.md), nor the [yathowm](../../strongs/h/h3490.md), the [ger](../../strongs/h/h1616.md), nor the ['aniy](../../strongs/h/h6041.md); and let none of you [chashab](../../strongs/h/h2803.md) [ra'](../../strongs/h/h7451.md) ['iysh](../../strongs/h/h376.md) his ['ach](../../strongs/h/h251.md) in your [lebab](../../strongs/h/h3824.md).

<a name="zechariah_7_11"></a>Zechariah 7:11

But they [mā'ēn](../../strongs/h/h3985.md) to [qashab](../../strongs/h/h7181.md), and pulled [nathan](../../strongs/h/h5414.md) [sārar](../../strongs/h/h5637.md) the [kāṯēp̄](../../strongs/h/h3802.md), and [kabad](../../strongs/h/h3513.md) their ['ozen](../../strongs/h/h241.md), that they should not [shama'](../../strongs/h/h8085.md).

<a name="zechariah_7_12"></a>Zechariah 7:12

Yea, they [śûm](../../strongs/h/h7760.md) their [leb](../../strongs/h/h3820.md) as a [šāmîr](../../strongs/h/h8068.md), lest they should [shama'](../../strongs/h/h8085.md) the [towrah](../../strongs/h/h8451.md), and the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) hath [shalach](../../strongs/h/h7971.md) in his [ruwach](../../strongs/h/h7307.md) [yad](../../strongs/h/h3027.md) the [ri'šôn](../../strongs/h/h7223.md) [nāḇî'](../../strongs/h/h5030.md): therefore came a [gadowl](../../strongs/h/h1419.md) [qeṣep̄](../../strongs/h/h7110.md) from [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="zechariah_7_13"></a>Zechariah 7:13

Therefore it is come to pass, that as he [qara'](../../strongs/h/h7121.md), and they would not [shama'](../../strongs/h/h8085.md); so they [qara'](../../strongs/h/h7121.md), and I would not [shama'](../../strongs/h/h8085.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md):

<a name="zechariah_7_14"></a>Zechariah 7:14

But I scattered them with a [sāʿar](../../strongs/h/h5590.md) among all the [gowy](../../strongs/h/h1471.md) whom they [yada'](../../strongs/h/h3045.md) not. Thus the ['erets](../../strongs/h/h776.md) was [šāmēm](../../strongs/h/h8074.md) ['aḥar](../../strongs/h/h310.md) them, that no man ['abar](../../strongs/h/h5674.md) nor [shuwb](../../strongs/h/h7725.md): for they [śûm](../../strongs/h/h7760.md) the [ḥemdâ](../../strongs/h/h2532.md) ['erets](../../strongs/h/h776.md) [šammâ](../../strongs/h/h8047.md).

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 6](zechariah_6.md) - [Zechariah 8](zechariah_8.md)