# [Zechariah 13](https://www.blueletterbible.org/kjv/zechariah/13)

<a name="zechariah_13_1"></a>Zechariah 13:1

In that [yowm](../../strongs/h/h3117.md) there shall be a [māqôr](../../strongs/h/h4726.md) [pāṯaḥ](../../strongs/h/h6605.md) to the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md) and to the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) for [chatta'ath](../../strongs/h/h2403.md) and for [nidâ](../../strongs/h/h5079.md).

<a name="zechariah_13_2"></a>Zechariah 13:2

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), that I will [karath](../../strongs/h/h3772.md) the [shem](../../strongs/h/h8034.md) of the [ʿāṣāḇ](../../strongs/h/h6091.md) out of the ['erets](../../strongs/h/h776.md), and they shall no more be [zakar](../../strongs/h/h2142.md): and also I will cause the [nāḇî'](../../strongs/h/h5030.md) and the [ṭām'â](../../strongs/h/h2932.md) [ruwach](../../strongs/h/h7307.md) to ['abar](../../strongs/h/h5674.md) out of the ['erets](../../strongs/h/h776.md).

<a name="zechariah_13_3"></a>Zechariah 13:3

And it shall come to pass, that when ['iysh](../../strongs/h/h376.md) shall yet [nāḇā'](../../strongs/h/h5012.md), then his ['ab](../../strongs/h/h1.md) and his ['em](../../strongs/h/h517.md) that [yalad](../../strongs/h/h3205.md) him shall ['āmar](../../strongs/h/h559.md) unto him, Thou shalt not [ḥāyâ](../../strongs/h/h2421.md); for thou [dabar](../../strongs/h/h1696.md) [sheqer](../../strongs/h/h8267.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md): and his ['ab](../../strongs/h/h1.md) and his ['em](../../strongs/h/h517.md) that [yalad](../../strongs/h/h3205.md) him shall [dāqar](../../strongs/h/h1856.md) him through when he [nāḇā'](../../strongs/h/h5012.md).

<a name="zechariah_13_4"></a>Zechariah 13:4

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that the [nāḇî'](../../strongs/h/h5030.md) shall be [buwsh](../../strongs/h/h954.md) every ['iysh](../../strongs/h/h376.md) of his [ḥizzāyôn](../../strongs/h/h2384.md), when he hath [nāḇā'](../../strongs/h/h5012.md); neither shall they [labash](../../strongs/h/h3847.md) a [śēʿār](../../strongs/h/h8181.md) ['adereṯ](../../strongs/h/h155.md) to [kāḥaš](../../strongs/h/h3584.md):

<a name="zechariah_13_5"></a>Zechariah 13:5

But he shall ['āmar](../../strongs/h/h559.md), I am no [nāḇî'](../../strongs/h/h5030.md), I am an ['iysh](../../strongs/h/h376.md) ['abad](../../strongs/h/h5647.md) ['ăḏāmâ](../../strongs/h/h127.md); for ['āḏām](../../strongs/h/h120.md) [qānâ](../../strongs/h/h7069.md) from my [nāʿur](../../strongs/h/h5271.md).

<a name="zechariah_13_6"></a>Zechariah 13:6

And one shall ['āmar](../../strongs/h/h559.md) unto him, What are these [makâ](../../strongs/h/h4347.md) in thine [yad](../../strongs/h/h3027.md)? Then he shall ['āmar](../../strongs/h/h559.md), Those with which I was [nakah](../../strongs/h/h5221.md) in the [bayith](../../strongs/h/h1004.md) of my ['ahab](../../strongs/h/h157.md).

<a name="zechariah_13_7"></a>Zechariah 13:7

[ʿûr](../../strongs/h/h5782.md), O [chereb](../../strongs/h/h2719.md), against my [ra'ah](../../strongs/h/h7462.md), and against the [geḇer](../../strongs/h/h1397.md) that is my [ʿāmîṯ](../../strongs/h/h5997.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md): [nakah](../../strongs/h/h5221.md) the [ra'ah](../../strongs/h/h7462.md), and the [tso'n](../../strongs/h/h6629.md) shall be [puwts](../../strongs/h/h6327.md): and I will [shuwb](../../strongs/h/h7725.md) mine [yad](../../strongs/h/h3027.md) upon the [ṣāʿar](../../strongs/h/h6819.md).

<a name="zechariah_13_8"></a>Zechariah 13:8

And it shall come to pass, that in all the ['erets](../../strongs/h/h776.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), two [peh](../../strongs/h/h6310.md) therein shall be [karath](../../strongs/h/h3772.md) and [gāvaʿ](../../strongs/h/h1478.md); but the third shall be [yāṯar](../../strongs/h/h3498.md) therein.

<a name="zechariah_13_9"></a>Zechariah 13:9

And I will [bow'](../../strongs/h/h935.md) the third part through the ['esh](../../strongs/h/h784.md), and will [tsaraph](../../strongs/h/h6884.md) them as [keceph](../../strongs/h/h3701.md) is [tsaraph](../../strongs/h/h6884.md), and will [bachan](../../strongs/h/h974.md) them as [zāhāḇ](../../strongs/h/h2091.md) is [bachan](../../strongs/h/h974.md): they shall [qara'](../../strongs/h/h7121.md) on my [shem](../../strongs/h/h8034.md), and I will ['anah](../../strongs/h/h6030.md) them: I will ['āmar](../../strongs/h/h559.md), It is my ['am](../../strongs/h/h5971.md): and they shall ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) is my ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 12](zechariah_12.md) - [Zechariah 14](zechariah_14.md)