# [Zechariah 11](https://www.blueletterbible.org/kjv/zechariah/11)

<a name="zechariah_11_1"></a>Zechariah 11:1

[pāṯaḥ](../../strongs/h/h6605.md) thy [deleṯ](../../strongs/h/h1817.md), O [Lᵊḇānôn](../../strongs/h/h3844.md), that the ['esh](../../strongs/h/h784.md) may ['akal](../../strongs/h/h398.md) thy ['erez](../../strongs/h/h730.md).

<a name="zechariah_11_2"></a>Zechariah 11:2

[yālal](../../strongs/h/h3213.md), [bᵊrôš](../../strongs/h/h1265.md); for the ['erez](../../strongs/h/h730.md) is [naphal](../../strongs/h/h5307.md); because the ['addiyr](../../strongs/h/h117.md) are [shadad](../../strongs/h/h7703.md): [yālal](../../strongs/h/h3213.md), O ye ['allôn](../../strongs/h/h437.md) of [Bāšān](../../strongs/h/h1316.md); for the [yaʿar](../../strongs/h/h3293.md) of the [bāṣivr](../../strongs/h/h1208.md) [bāṣar](../../strongs/h/h1219.md) is [yarad](../../strongs/h/h3381.md).

<a name="zechariah_11_3"></a>Zechariah 11:3

There is a [qowl](../../strongs/h/h6963.md) of the [yᵊlālâ](../../strongs/h/h3215.md) of the [ra'ah](../../strongs/h/h7462.md); for their ['adereṯ](../../strongs/h/h155.md) is [shadad](../../strongs/h/h7703.md): a [qowl](../../strongs/h/h6963.md) of the [šᵊ'āḡâ](../../strongs/h/h7581.md) of [kephiyr](../../strongs/h/h3715.md); for the [gā'ôn](../../strongs/h/h1347.md) of [Yardēn](../../strongs/h/h3383.md) is [shadad](../../strongs/h/h7703.md).

<a name="zechariah_11_4"></a>Zechariah 11:4

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md); [ra'ah](../../strongs/h/h7462.md) the [tso'n](../../strongs/h/h6629.md) of the [hărēḡâ](../../strongs/h/h2028.md);

<a name="zechariah_11_5"></a>Zechariah 11:5

Whose [qānâ](../../strongs/h/h7069.md) [harag](../../strongs/h/h2026.md) them, and hold themselves not ['asham](../../strongs/h/h816.md): and they that [māḵar](../../strongs/h/h4376.md) them ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md); for I am [ʿāšar](../../strongs/h/h6238.md): and their own [ra'ah](../../strongs/h/h7462.md) [ḥāmal](../../strongs/h/h2550.md) them not.

<a name="zechariah_11_6"></a>Zechariah 11:6

For I will no more [ḥāmal](../../strongs/h/h2550.md) the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): but, lo, I will [māṣā'](../../strongs/h/h4672.md) the ['āḏām](../../strongs/h/h120.md) every ['iysh](../../strongs/h/h376.md) into his [rea'](../../strongs/h/h7453.md) [yad](../../strongs/h/h3027.md), and into the [yad](../../strongs/h/h3027.md) of his [melek](../../strongs/h/h4428.md): and they shall [kāṯaṯ](../../strongs/h/h3807.md) the ['erets](../../strongs/h/h776.md), and out of their [yad](../../strongs/h/h3027.md) I will not [natsal](../../strongs/h/h5337.md) them.

<a name="zechariah_11_7"></a>Zechariah 11:7

And I will [ra'ah](../../strongs/h/h7462.md) the [tso'n](../../strongs/h/h6629.md) of [hărēḡâ](../../strongs/h/h2028.md), even you, O ['aniy](../../strongs/h/h6041.md) of the [tso'n](../../strongs/h/h6629.md). And I [laqach](../../strongs/h/h3947.md) unto me two [maqqēl](../../strongs/h/h4731.md); the one I [qara'](../../strongs/h/h7121.md) [nōʿam](../../strongs/h/h5278.md), and the other I [qara'](../../strongs/h/h7121.md) [chabal](../../strongs/h/h2254.md); and I [ra'ah](../../strongs/h/h7462.md) the [tso'n](../../strongs/h/h6629.md).

<a name="zechariah_11_8"></a>Zechariah 11:8

Three [ra'ah](../../strongs/h/h7462.md) also I [kāḥaḏ](../../strongs/h/h3582.md) in one [yeraḥ](../../strongs/h/h3391.md); and my [nephesh](../../strongs/h/h5315.md) [qāṣar](../../strongs/h/h7114.md) them, and their [nephesh](../../strongs/h/h5315.md) also [bāḥal](../../strongs/h/h973.md) me.

<a name="zechariah_11_9"></a>Zechariah 11:9

Then ['āmar](../../strongs/h/h559.md) I, I will not [ra'ah](../../strongs/h/h7462.md) you: that that [muwth](../../strongs/h/h4191.md), let it [muwth](../../strongs/h/h4191.md); and that that is to be [kāḥaḏ](../../strongs/h/h3582.md), let it be [kāḥaḏ](../../strongs/h/h3582.md); and let the [šā'ar](../../strongs/h/h7604.md) ['akal](../../strongs/h/h398.md) every ['ishshah](../../strongs/h/h802.md) the [basar](../../strongs/h/h1320.md) of [rᵊʿûṯ](../../strongs/h/h7468.md).

<a name="zechariah_11_10"></a>Zechariah 11:10

And I [laqach](../../strongs/h/h3947.md) my [maqqēl](../../strongs/h/h4731.md), even [nōʿam](../../strongs/h/h5278.md), and [gāḏaʿ](../../strongs/h/h1438.md) it, that I might [pārar](../../strongs/h/h6565.md) my [bĕriyth](../../strongs/h/h1285.md) which I had [karath](../../strongs/h/h3772.md) with all the ['am](../../strongs/h/h5971.md).

<a name="zechariah_11_11"></a>Zechariah 11:11

And it was [pārar](../../strongs/h/h6565.md) in that [yowm](../../strongs/h/h3117.md): and so the ['aniy](../../strongs/h/h6041.md) of the [tso'n](../../strongs/h/h6629.md) that [shamar](../../strongs/h/h8104.md) upon me [yada'](../../strongs/h/h3045.md) that it was the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="zechariah_11_12"></a>Zechariah 11:12

And I ['āmar](../../strongs/h/h559.md) unto them, If ye ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md), [yāhaḇ](../../strongs/h/h3051.md) me my [śāḵār](../../strongs/h/h7939.md); and if not, [ḥāḏal](../../strongs/h/h2308.md). So they [šāqal](../../strongs/h/h8254.md) for my [śāḵār](../../strongs/h/h7939.md) thirty pieces of [keceph](../../strongs/h/h3701.md).

<a name="zechariah_11_13"></a>Zechariah 11:13

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [shalak](../../strongs/h/h7993.md) it unto the [yāṣar](../../strongs/h/h3335.md): an ['eḏer](../../strongs/h/h145.md) [yᵊqār](../../strongs/h/h3366.md) that I was [yāqar](../../strongs/h/h3365.md) of them. And I [laqach](../../strongs/h/h3947.md) the thirty pieces of [keceph](../../strongs/h/h3701.md), and [shalak](../../strongs/h/h7993.md) them to the [yāṣar](../../strongs/h/h3335.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="zechariah_11_14"></a>Zechariah 11:14

Then I [gāḏaʿ](../../strongs/h/h1438.md) mine other [maqqēl](../../strongs/h/h4731.md), even [chabal](../../strongs/h/h2254.md), that I might [pārar](../../strongs/h/h6565.md) the ['aḥăvâ](../../strongs/h/h264.md) between [Yehuwdah](../../strongs/h/h3063.md) and [Yisra'el](../../strongs/h/h3478.md).

<a name="zechariah_11_15"></a>Zechariah 11:15

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [laqach](../../strongs/h/h3947.md) unto thee yet the [kĕliy](../../strongs/h/h3627.md) of an ['ĕvilî](../../strongs/h/h196.md) [ra'ah](../../strongs/h/h7462.md).

<a name="zechariah_11_16"></a>Zechariah 11:16

For, lo, I will [quwm](../../strongs/h/h6965.md) a [ra'ah](../../strongs/h/h7462.md) in the ['erets](../../strongs/h/h776.md), which shall not [paqad](../../strongs/h/h6485.md) those that be cut [kāḥaḏ](../../strongs/h/h3582.md), neither shall [bāqaš](../../strongs/h/h1245.md) the [naʿar](../../strongs/h/h5289.md), nor [rapha'](../../strongs/h/h7495.md) that that is [shabar](../../strongs/h/h7665.md), nor [kûl](../../strongs/h/h3557.md) that that [nāṣaḇ](../../strongs/h/h5324.md) still: but he shall ['akal](../../strongs/h/h398.md) the [basar](../../strongs/h/h1320.md) of the [bārî'](../../strongs/h/h1277.md), and [paraq](../../strongs/h/h6561.md) their [parsâ](../../strongs/h/h6541.md) in [paraq](../../strongs/h/h6561.md).

<a name="zechariah_11_17"></a>Zechariah 11:17

[hôy](../../strongs/h/h1945.md) to the ['ĕlîl](../../strongs/h/h457.md) [rōʿê](../../strongs/h/h7473.md) that ['azab](../../strongs/h/h5800.md) the [tso'n](../../strongs/h/h6629.md)! the [chereb](../../strongs/h/h2719.md) shall be upon his [zerowa'](../../strongs/h/h2220.md), and upon his [yamiyn](../../strongs/h/h3225.md) ['ayin](../../strongs/h/h5869.md): his [zerowa'](../../strongs/h/h2220.md) shall be [yāḇēš](../../strongs/h/h3001.md) dried [yāḇēš](../../strongs/h/h3001.md), and his [yamiyn](../../strongs/h/h3225.md) ['ayin](../../strongs/h/h5869.md) shall be [kāhâ](../../strongs/h/h3543.md) [kāhâ](../../strongs/h/h3543.md).

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 10](zechariah_10.md) - [Zechariah 12](zechariah_12.md)