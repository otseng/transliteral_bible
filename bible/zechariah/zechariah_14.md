# [Zechariah 14](https://www.blueletterbible.org/kjv/zechariah/14)

<a name="zechariah_14_1"></a>Zechariah 14:1

Behold, the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md), and thy [šālāl](../../strongs/h/h7998.md) shall be [chalaq](../../strongs/h/h2505.md) in the [qereḇ](../../strongs/h/h7130.md) of thee.

<a name="zechariah_14_2"></a>Zechariah 14:2

For I will ['āsap̄](../../strongs/h/h622.md) all [gowy](../../strongs/h/h1471.md) against [Yĕruwshalaim](../../strongs/h/h3389.md) to [milḥāmâ](../../strongs/h/h4421.md); and the [ʿîr](../../strongs/h/h5892.md) shall be [lāḵaḏ](../../strongs/h/h3920.md), and the [bayith](../../strongs/h/h1004.md) [šāsas](../../strongs/h/h8155.md), and the ['ishshah](../../strongs/h/h802.md) [shakab](../../strongs/h/h7901.md) [šāḡal](../../strongs/h/h7693.md); and [ḥēṣî](../../strongs/h/h2677.md) of the [ʿîr](../../strongs/h/h5892.md) shall [yāṣā'](../../strongs/h/h3318.md) into [gôlâ](../../strongs/h/h1473.md), and the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md) shall not be [karath](../../strongs/h/h3772.md) from the [ʿîr](../../strongs/h/h5892.md).

<a name="zechariah_14_3"></a>Zechariah 14:3

Then shall [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md), and [lāḥam](../../strongs/h/h3898.md) against those [gowy](../../strongs/h/h1471.md), as [yowm](../../strongs/h/h3117.md) he [lāḥam](../../strongs/h/h3898.md) in the [yowm](../../strongs/h/h3117.md) of [qᵊrāḇ](../../strongs/h/h7128.md).

<a name="zechariah_14_4"></a>Zechariah 14:4

And his [regel](../../strongs/h/h7272.md) shall ['amad](../../strongs/h/h5975.md) in that [yowm](../../strongs/h/h3117.md) upon the [har](../../strongs/h/h2022.md) of [zayiṯ](../../strongs/h/h2132.md), which is [paniym](../../strongs/h/h6440.md) [Yĕruwshalaim](../../strongs/h/h3389.md) on the [qeḏem](../../strongs/h/h6924.md), and the [har](../../strongs/h/h2022.md) of [zayiṯ](../../strongs/h/h2132.md) shall [bāqaʿ](../../strongs/h/h1234.md) in the [ḥēṣî](../../strongs/h/h2677.md) thereof toward the [mizrach](../../strongs/h/h4217.md) and toward the [yam](../../strongs/h/h3220.md), and there shall be a [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) [gay'](../../strongs/h/h1516.md); and [ḥēṣî](../../strongs/h/h2677.md) of the [har](../../strongs/h/h2022.md) shall [mûš](../../strongs/h/h4185.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md), and [ḥēṣî](../../strongs/h/h2677.md) of it toward the [neḡeḇ](../../strongs/h/h5045.md).

<a name="zechariah_14_5"></a>Zechariah 14:5

And ye shall [nûs](../../strongs/h/h5127.md) to the [gay'](../../strongs/h/h1516.md) of the [har](../../strongs/h/h2022.md); for the [gay'](../../strongs/h/h1516.md) of the [har](../../strongs/h/h2022.md) shall [naga'](../../strongs/h/h5060.md) unto ['Āṣēl](../../strongs/h/h682.md): yea, ye shall [nûs](../../strongs/h/h5127.md), like as ye [nûs](../../strongs/h/h5127.md) from [paniym](../../strongs/h/h6440.md) the [raʿaš](../../strongs/h/h7494.md) in the [yowm](../../strongs/h/h3117.md) of ['Uzziyah](../../strongs/h/h5818.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md): and [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md) shall [bow'](../../strongs/h/h935.md), and all the [qadowsh](../../strongs/h/h6918.md) with thee.

<a name="zechariah_14_6"></a>Zechariah 14:6

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that the ['owr](../../strongs/h/h216.md) shall not be [yāqār](../../strongs/h/h3368.md), nor [qāp̄ā'](../../strongs/h/h7087.md) [qāp̄ā'](../../strongs/h/h7087.md):

<a name="zechariah_14_7"></a>Zechariah 14:7

But it shall be one [yowm](../../strongs/h/h3117.md) which shall be [yada'](../../strongs/h/h3045.md) to [Yĕhovah](../../strongs/h/h3068.md), not [yowm](../../strongs/h/h3117.md), nor [layil](../../strongs/h/h3915.md): but it shall come to pass, that at ['ereb](../../strongs/h/h6153.md) [ʿēṯ](../../strongs/h/h6256.md) it shall be ['owr](../../strongs/h/h216.md).

<a name="zechariah_14_8"></a>Zechariah 14:8

And it shall be in that [yowm](../../strongs/h/h3117.md), that [chay](../../strongs/h/h2416.md) [mayim](../../strongs/h/h4325.md) shall [yāṣā'](../../strongs/h/h3318.md) from [Yĕruwshalaim](../../strongs/h/h3389.md); [ḥēṣî](../../strongs/h/h2677.md) of them toward the [qaḏmōnî](../../strongs/h/h6931.md) [yam](../../strongs/h/h3220.md), and [ḥēṣî](../../strongs/h/h2677.md) of them toward the ['aḥărôn](../../strongs/h/h314.md) [yam](../../strongs/h/h3220.md): in [qayiṣ](../../strongs/h/h7019.md) and in [ḥōrep̄](../../strongs/h/h2779.md) shall it be.

<a name="zechariah_14_9"></a>Zechariah 14:9

And [Yĕhovah](../../strongs/h/h3068.md) shall be [melek](../../strongs/h/h4428.md) over all the ['erets](../../strongs/h/h776.md): in that [yowm](../../strongs/h/h3117.md) shall there be one [Yĕhovah](../../strongs/h/h3068.md), and his [shem](../../strongs/h/h8034.md) one.

<a name="zechariah_14_10"></a>Zechariah 14:10

All the ['erets](../../strongs/h/h776.md) shall be [cabab](../../strongs/h/h5437.md) as an ['arabah](../../strongs/h/h6160.md) from [Geḇaʿ](../../strongs/h/h1387.md) to [Rimmôn](../../strongs/h/h7417.md) [neḡeḇ](../../strongs/h/h5045.md) of [Yĕruwshalaim](../../strongs/h/h3389.md): and it shall be [rā'am](../../strongs/h/h7213.md) , and [yashab](../../strongs/h/h3427.md) in her place, from [Binyāmîn](../../strongs/h/h1144.md) [sha'ar](../../strongs/h/h8179.md) unto the [maqowm](../../strongs/h/h4725.md) of the [ri'šôn](../../strongs/h/h7223.md) [sha'ar](../../strongs/h/h8179.md), unto the [pēn](../../strongs/h/h6434.md) [sha'ar](../../strongs/h/h8179.md), and from the [miḡdāl](../../strongs/h/h4026.md) of [Ḥănan'Ēl](../../strongs/h/h2606.md) unto the [melek](../../strongs/h/h4428.md) [yeqeḇ](../../strongs/h/h3342.md).

<a name="zechariah_14_11"></a>Zechariah 14:11

And men shall [yashab](../../strongs/h/h3427.md) in it, and there shall be no more [ḥērem](../../strongs/h/h2764.md); but [Yĕruwshalaim](../../strongs/h/h3389.md) shall be [betach](../../strongs/h/h983.md) [yashab](../../strongs/h/h3427.md).

<a name="zechariah_14_12"></a>Zechariah 14:12

And this shall be the [magēp̄â](../../strongs/h/h4046.md) wherewith [Yĕhovah](../../strongs/h/h3068.md) will [nāḡap̄](../../strongs/h/h5062.md) all the ['am](../../strongs/h/h5971.md) that have [ṣᵊḇā'](../../strongs/h/h6633.md) against [Yĕruwshalaim](../../strongs/h/h3389.md); Their [basar](../../strongs/h/h1320.md) shall [māqaq](../../strongs/h/h4743.md) while they ['amad](../../strongs/h/h5975.md) upon their [regel](../../strongs/h/h7272.md), and their ['ayin](../../strongs/h/h5869.md) shall [māqaq](../../strongs/h/h4743.md) in their [ḥôr](../../strongs/h/h2356.md), and their [lashown](../../strongs/h/h3956.md) shall [māqaq](../../strongs/h/h4743.md) in their [peh](../../strongs/h/h6310.md).

<a name="zechariah_14_13"></a>Zechariah 14:13

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that a [rab](../../strongs/h/h7227.md) [mᵊhûmâ](../../strongs/h/h4103.md) from [Yĕhovah](../../strongs/h/h3068.md) shall be among them; and they shall [ḥāzaq](../../strongs/h/h2388.md) every ['iysh](../../strongs/h/h376.md) on the [yad](../../strongs/h/h3027.md) of his [rea'](../../strongs/h/h7453.md), and his [yad](../../strongs/h/h3027.md) shall [ʿālâ](../../strongs/h/h5927.md) against the [yad](../../strongs/h/h3027.md) of his [rea'](../../strongs/h/h7453.md).

<a name="zechariah_14_14"></a>Zechariah 14:14

And [Yehuwdah](../../strongs/h/h3063.md) also shall [lāḥam](../../strongs/h/h3898.md) at [Yĕruwshalaim](../../strongs/h/h3389.md); and the [ḥayil](../../strongs/h/h2428.md) of all the [gowy](../../strongs/h/h1471.md) [cabiyb](../../strongs/h/h5439.md) shall be ['āsap̄](../../strongs/h/h622.md), [zāhāḇ](../../strongs/h/h2091.md), and [keceph](../../strongs/h/h3701.md), and [beḡeḏ](../../strongs/h/h899.md), in [me'od](../../strongs/h/h3966.md) [rōḇ](../../strongs/h/h7230.md).

<a name="zechariah_14_15"></a>Zechariah 14:15

And so shall be the [magēp̄â](../../strongs/h/h4046.md) of the [sûs](../../strongs/h/h5483.md), of the [pereḏ](../../strongs/h/h6505.md), of the [gāmāl](../../strongs/h/h1581.md), and of the [chamowr](../../strongs/h/h2543.md), and of all the [bĕhemah](../../strongs/h/h929.md) that shall be in these [maḥănê](../../strongs/h/h4264.md), as this [magēp̄â](../../strongs/h/h4046.md).

<a name="zechariah_14_16"></a>Zechariah 14:16

And it shall come to pass, that every one that is [yāṯar](../../strongs/h/h3498.md) of all the [gowy](../../strongs/h/h1471.md) which [bow'](../../strongs/h/h935.md) against [Yĕruwshalaim](../../strongs/h/h3389.md) shall even [ʿālâ](../../strongs/h/h5927.md) [day](../../strongs/h/h1767.md) [šānâ](../../strongs/h/h8141.md) to [šānâ](../../strongs/h/h8141.md) to [shachah](../../strongs/h/h7812.md) the [melek](../../strongs/h/h4428.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), and to [ḥāḡaḡ](../../strongs/h/h2287.md) the [ḥāḡ](../../strongs/h/h2282.md) of [cukkah](../../strongs/h/h5521.md).

<a name="zechariah_14_17"></a>Zechariah 14:17

And it shall be, that whoso will not [ʿālâ](../../strongs/h/h5927.md) of all the [mišpāḥâ](../../strongs/h/h4940.md) of the ['erets](../../strongs/h/h776.md) unto [Yĕruwshalaim](../../strongs/h/h3389.md) to [shachah](../../strongs/h/h7812.md) the [melek](../../strongs/h/h4428.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), even upon them shall be no [gešem](../../strongs/h/h1653.md).

<a name="zechariah_14_18"></a>Zechariah 14:18

And if the [mišpāḥâ](../../strongs/h/h4940.md) of [Mitsrayim](../../strongs/h/h4714.md) [ʿālâ](../../strongs/h/h5927.md) not, and [bow'](../../strongs/h/h935.md) not, that have no; there shall be the [magēp̄â](../../strongs/h/h4046.md), wherewith [Yĕhovah](../../strongs/h/h3068.md) will [nāḡap̄](../../strongs/h/h5062.md) the [gowy](../../strongs/h/h1471.md) that come not [ʿālâ](../../strongs/h/h5927.md) to [ḥāḡaḡ](../../strongs/h/h2287.md) the [ḥāḡ](../../strongs/h/h2282.md) of [cukkah](../../strongs/h/h5521.md).

<a name="zechariah_14_19"></a>Zechariah 14:19

This shall be the [chatta'ath](../../strongs/h/h2403.md) of [Mitsrayim](../../strongs/h/h4714.md), and the [chatta'ath](../../strongs/h/h2403.md) of all [gowy](../../strongs/h/h1471.md) that come not [ʿālâ](../../strongs/h/h5927.md) to [ḥāḡaḡ](../../strongs/h/h2287.md) the [ḥāḡ](../../strongs/h/h2282.md) of [cukkah](../../strongs/h/h5521.md).

<a name="zechariah_14_20"></a>Zechariah 14:20

In that [yowm](../../strongs/h/h3117.md) shall there be upon the [mᵊṣillâ](../../strongs/h/h4698.md) of the [sûs](../../strongs/h/h5483.md), [qodesh](../../strongs/h/h6944.md) UNTO THE [Yĕhovah](../../strongs/h/h3068.md); and the [sîr](../../strongs/h/h5518.md) in [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md) shall be like the [mizrāq](../../strongs/h/h4219.md) [paniym](../../strongs/h/h6440.md) the [mizbeach](../../strongs/h/h4196.md).

<a name="zechariah_14_21"></a>Zechariah 14:21

Yea, every [sîr](../../strongs/h/h5518.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) and in [Yehuwdah](../../strongs/h/h3063.md) shall be [qodesh](../../strongs/h/h6944.md) unto [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md): and all they that [zabach](../../strongs/h/h2076.md) shall [bow'](../../strongs/h/h935.md) and [laqach](../../strongs/h/h3947.md) of them, and [bāšal](../../strongs/h/h1310.md) therein: and in that [yowm](../../strongs/h/h3117.md) there shall be no more the [Kᵊnaʿănî](../../strongs/h/h3669.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 13](zechariah_13.md)