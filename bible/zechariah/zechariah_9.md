# [Zechariah 9](https://www.blueletterbible.org/kjv/zechariah/9)

<a name="zechariah_9_1"></a>Zechariah 9:1

The [maśśā'](../../strongs/h/h4853.md) of the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) in the ['erets](../../strongs/h/h776.md) of [Ḥaḏrāḵ](../../strongs/h/h2317.md), and [Dammeśeq](../../strongs/h/h1834.md) shall be the [mᵊnûḥâ](../../strongs/h/h4496.md) thereof: when the ['ayin](../../strongs/h/h5869.md) of ['āḏām](../../strongs/h/h120.md), as of all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), shall be toward [Yĕhovah](../../strongs/h/h3068.md).

<a name="zechariah_9_2"></a>Zechariah 9:2

And [Ḥămāṯ](../../strongs/h/h2574.md) also shall [gāḇal](../../strongs/h/h1379.md) thereby; [Ṣōr](../../strongs/h/h6865.md), and [Ṣîḏôn](../../strongs/h/h6721.md), though it be [me'od](../../strongs/h/h3966.md) [ḥāḵam](../../strongs/h/h2449.md).

<a name="zechariah_9_3"></a>Zechariah 9:3

And [Ṣōr](../../strongs/h/h6865.md) did [bānâ](../../strongs/h/h1129.md) herself a strong [māṣôr](../../strongs/h/h4692.md), and heaped [ṣāḇar](../../strongs/h/h6651.md) [keceph](../../strongs/h/h3701.md) as the ['aphar](../../strongs/h/h6083.md), and [ḥārûṣ](../../strongs/h/h2742.md) as the [ṭîṭ](../../strongs/h/h2916.md) of the [ḥûṣ](../../strongs/h/h2351.md).

<a name="zechariah_9_4"></a>Zechariah 9:4

Behold, the ['adonay](../../strongs/h/h136.md) will [yarash](../../strongs/h/h3423.md) her, and he will [nakah](../../strongs/h/h5221.md) her [ḥayil](../../strongs/h/h2428.md) in the [yam](../../strongs/h/h3220.md); and she shall be ['akal](../../strongs/h/h398.md) with ['esh](../../strongs/h/h784.md).

<a name="zechariah_9_5"></a>Zechariah 9:5

['Ašqᵊlôn](../../strongs/h/h831.md) shall [ra'ah](../../strongs/h/h7200.md) it, and [yare'](../../strongs/h/h3372.md); [ʿAzzâ](../../strongs/h/h5804.md) also, and be [me'od](../../strongs/h/h3966.md) [chuwl](../../strongs/h/h2342.md), and [ʿEqrôn](../../strongs/h/h6138.md); for her [mabāṭ](../../strongs/h/h4007.md) shall be [yāḇēš](../../strongs/h/h3001.md); and the [melek](../../strongs/h/h4428.md) shall ['abad](../../strongs/h/h6.md) from [ʿAzzâ](../../strongs/h/h5804.md), and ['Ašqᵊlôn](../../strongs/h/h831.md) shall not be [yashab](../../strongs/h/h3427.md).

<a name="zechariah_9_6"></a>Zechariah 9:6

And a [mamzēr](../../strongs/h/h4464.md) shall [yashab](../../strongs/h/h3427.md) in ['Ašdôḏ](../../strongs/h/h795.md), and I will [karath](../../strongs/h/h3772.md) the [gā'ôn](../../strongs/h/h1347.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="zechariah_9_7"></a>Zechariah 9:7

And I will [cuwr](../../strongs/h/h5493.md) his [dam](../../strongs/h/h1818.md) out of his [peh](../../strongs/h/h6310.md), and his [šiqqûṣ](../../strongs/h/h8251.md) from between his [šēn](../../strongs/h/h8127.md): but he that [šā'ar](../../strongs/h/h7604.md), even he, shall be for our ['Elohiym](../../strongs/h/h430.md), and he shall be as an ['allûp̄](../../strongs/h/h441.md) in [Yehuwdah](../../strongs/h/h3063.md), and [ʿEqrôn](../../strongs/h/h6138.md) as a [Yᵊḇûsî](../../strongs/h/h2983.md).

<a name="zechariah_9_8"></a>Zechariah 9:8

And I will [ḥānâ](../../strongs/h/h2583.md) about mine [bayith](../../strongs/h/h1004.md) because of the [maṣṣāḇâ](../../strongs/h/h4675.md), because of him that ['abar](../../strongs/h/h5674.md), and because of him that [shuwb](../../strongs/h/h7725.md): and no [nāḡaś](../../strongs/h/h5065.md) shall ['abar](../../strongs/h/h5674.md) them any more: for now have I [ra'ah](../../strongs/h/h7200.md) with mine ['ayin](../../strongs/h/h5869.md).

<a name="zechariah_9_9"></a>Zechariah 9:9

[giyl](../../strongs/h/h1523.md) [me'od](../../strongs/h/h3966.md), O [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md); [rûaʿ](../../strongs/h/h7321.md), O [bath](../../strongs/h/h1323.md) of [Yĕruwshalaim](../../strongs/h/h3389.md): behold, thy [melek](../../strongs/h/h4428.md) [bow'](../../strongs/h/h935.md) unto thee: he is [tsaddiyq](../../strongs/h/h6662.md), and having [yasha'](../../strongs/h/h3467.md); ['aniy](../../strongs/h/h6041.md), and [rāḵaḇ](../../strongs/h/h7392.md) upon a [chamowr](../../strongs/h/h2543.md), and upon a [ʿayir](../../strongs/h/h5895.md) the [ben](../../strongs/h/h1121.md) of an ['āṯôn](../../strongs/h/h860.md).

<a name="zechariah_9_10"></a>Zechariah 9:10

And I will [karath](../../strongs/h/h3772.md) the [reḵeḇ](../../strongs/h/h7393.md) from ['Ep̄rayim](../../strongs/h/h669.md), and the [sûs](../../strongs/h/h5483.md) from [Yĕruwshalaim](../../strongs/h/h3389.md), and the [milḥāmâ](../../strongs/h/h4421.md) [qesheth](../../strongs/h/h7198.md) shall be [karath](../../strongs/h/h3772.md): and he shall [dabar](../../strongs/h/h1696.md) [shalowm](../../strongs/h/h7965.md) unto the [gowy](../../strongs/h/h1471.md): and his [mōšel](../../strongs/h/h4915.md) shall be from [yam](../../strongs/h/h3220.md) even to [yam](../../strongs/h/h3220.md), and from the [nāhār](../../strongs/h/h5104.md) even to the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md).

<a name="zechariah_9_11"></a>Zechariah 9:11

As for thee also, by the [dam](../../strongs/h/h1818.md) of thy [bĕriyth](../../strongs/h/h1285.md) I have [shalach](../../strongs/h/h7971.md) thy ['āsîr](../../strongs/h/h615.md) out of the [bowr](../../strongs/h/h953.md) wherein is no [mayim](../../strongs/h/h4325.md).

<a name="zechariah_9_12"></a>Zechariah 9:12

[shuwb](../../strongs/h/h7725.md) you to the [biṣṣārôn](../../strongs/h/h1225.md), ye ['āsîr](../../strongs/h/h615.md) of [tiqvâ](../../strongs/h/h8615.md): even to [yowm](../../strongs/h/h3117.md) do I [nāḡaḏ](../../strongs/h/h5046.md) that I will [shuwb](../../strongs/h/h7725.md) [mišnê](../../strongs/h/h4932.md) unto thee;

<a name="zechariah_9_13"></a>Zechariah 9:13

When I have [dāraḵ](../../strongs/h/h1869.md) [Yehuwdah](../../strongs/h/h3063.md) for me, [mālā'](../../strongs/h/h4390.md) the [qesheth](../../strongs/h/h7198.md) with ['Ep̄rayim](../../strongs/h/h669.md), and [ʿûr](../../strongs/h/h5782.md) thy [ben](../../strongs/h/h1121.md), [Tsiyown](../../strongs/h/h6726.md), against thy [ben](../../strongs/h/h1121.md), O [Yāvān](../../strongs/h/h3120.md), and [śûm](../../strongs/h/h7760.md) thee as the [chereb](../../strongs/h/h2719.md) of a [gibôr](../../strongs/h/h1368.md).

<a name="zechariah_9_14"></a>Zechariah 9:14

And [Yĕhovah](../../strongs/h/h3068.md) shall be [ra'ah](../../strongs/h/h7200.md) over them, and his [chets](../../strongs/h/h2671.md) shall [yāṣā'](../../strongs/h/h3318.md) as the [baraq](../../strongs/h/h1300.md): and the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) shall [tāqaʿ](../../strongs/h/h8628.md) the [šôp̄ār](../../strongs/h/h7782.md), and shall [halak](../../strongs/h/h1980.md) with [saʿar](../../strongs/h/h5591.md) of the [têmān](../../strongs/h/h8486.md).

<a name="zechariah_9_15"></a>Zechariah 9:15

[Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) shall [gānan](../../strongs/h/h1598.md) them; and they shall ['akal](../../strongs/h/h398.md), and [kāḇaš](../../strongs/h/h3533.md) with [qelaʿ](../../strongs/h/h7050.md) ['eben](../../strongs/h/h68.md); and they shall [šāṯâ](../../strongs/h/h8354.md), and make a [hāmâ](../../strongs/h/h1993.md) as through [yayin](../../strongs/h/h3196.md); and they shall be [mālā'](../../strongs/h/h4390.md) like [mizrāq](../../strongs/h/h4219.md), and as the [zāvîṯ](../../strongs/h/h2106.md) of the [mizbeach](../../strongs/h/h4196.md).

<a name="zechariah_9_16"></a>Zechariah 9:16

And [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md) shall [yasha'](../../strongs/h/h3467.md) them in that [yowm](../../strongs/h/h3117.md) as the [tso'n](../../strongs/h/h6629.md) of his ['am](../../strongs/h/h5971.md): for they shall be as the ['eben](../../strongs/h/h68.md) of a [nēzer](../../strongs/h/h5145.md), [nāsas](../../strongs/h/h5264.md) upon his ['ăḏāmâ](../../strongs/h/h127.md).

<a name="zechariah_9_17"></a>Zechariah 9:17

For how great is his [ṭûḇ](../../strongs/h/h2898.md), and how great is his [yᵊp̄î](../../strongs/h/h3308.md)! [dagan](../../strongs/h/h1715.md) shall make the [bāḥûr](../../strongs/h/h970.md) [nûḇ](../../strongs/h/h5107.md), and [tiyrowsh](../../strongs/h/h8492.md) the [bᵊṯûlâ](../../strongs/h/h1330.md).

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 8](zechariah_8.md) - [Zechariah 10](zechariah_10.md)