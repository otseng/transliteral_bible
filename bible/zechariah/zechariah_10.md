# [Zechariah 10](https://www.blueletterbible.org/kjv/zechariah/10)

<a name="zechariah_10_1"></a>Zechariah 10:1

[sha'al](../../strongs/h/h7592.md) ye of [Yĕhovah](../../strongs/h/h3068.md) [māṭār](../../strongs/h/h4306.md) in the [ʿēṯ](../../strongs/h/h6256.md) of the [malqôš](../../strongs/h/h4456.md); so [Yĕhovah](../../strongs/h/h3068.md) shall ['asah](../../strongs/h/h6213.md) bright [ḥāzîz](../../strongs/h/h2385.md), and [nathan](../../strongs/h/h5414.md) them [gešem](../../strongs/h/h1653.md) of [māṭār](../../strongs/h/h4306.md), to every ['iysh](../../strongs/h/h376.md) ['eseb](../../strongs/h/h6212.md) in the [sadeh](../../strongs/h/h7704.md).

<a name="zechariah_10_2"></a>Zechariah 10:2

For the [tᵊrāp̄îm](../../strongs/h/h8655.md) have [dabar](../../strongs/h/h1696.md) ['aven](../../strongs/h/h205.md), and the [qāsam](../../strongs/h/h7080.md) have [chazah](../../strongs/h/h2372.md) a [sheqer](../../strongs/h/h8267.md), and have [dabar](../../strongs/h/h1696.md) [shav'](../../strongs/h/h7723.md) [ḥălôm](../../strongs/h/h2472.md); they [nacham](../../strongs/h/h5162.md) in [heḇel](../../strongs/h/h1892.md): therefore they [nāsaʿ](../../strongs/h/h5265.md) their way as a [tso'n](../../strongs/h/h6629.md), they were [ʿānâ](../../strongs/h/h6031.md), because there was no [ra'ah](../../strongs/h/h7462.md).

<a name="zechariah_10_3"></a>Zechariah 10:3

Mine ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md) against the [ra'ah](../../strongs/h/h7462.md), and I [paqad](../../strongs/h/h6485.md) the [ʿatûḏ](../../strongs/h/h6260.md): for [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) hath [paqad](../../strongs/h/h6485.md) his [ʿēḏer](../../strongs/h/h5739.md) the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md), and hath [śûm](../../strongs/h/h7760.md) them as his [howd](../../strongs/h/h1935.md) [sûs](../../strongs/h/h5483.md) in the [milḥāmâ](../../strongs/h/h4421.md).

<a name="zechariah_10_4"></a>Zechariah 10:4

Out of him [yāṣā'](../../strongs/h/h3318.md) the [pinnâ](../../strongs/h/h6438.md), out of him the [yāṯēḏ](../../strongs/h/h3489.md), out of him the [milḥāmâ](../../strongs/h/h4421.md) [qesheth](../../strongs/h/h7198.md), out of him every [nāḡaś](../../strongs/h/h5065.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="zechariah_10_5"></a>Zechariah 10:5

And they shall be as [gibôr](../../strongs/h/h1368.md) men, which tread [bûs](../../strongs/h/h947.md) their enemies in the [ṭîṭ](../../strongs/h/h2916.md) of the [ḥûṣ](../../strongs/h/h2351.md) in the [milḥāmâ](../../strongs/h/h4421.md): and they shall [lāḥam](../../strongs/h/h3898.md), because [Yĕhovah](../../strongs/h/h3068.md) is with them, and the [rāḵaḇ](../../strongs/h/h7392.md) on [sûs](../../strongs/h/h5483.md) shall be [yāḇēš](../../strongs/h/h3001.md).

<a name="zechariah_10_6"></a>Zechariah 10:6

And I will [gabar](../../strongs/h/h1396.md) the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md), and I will [yasha'](../../strongs/h/h3467.md) the [bayith](../../strongs/h/h1004.md) of [Yôsēp̄](../../strongs/h/h3130.md), and I will bring them again to [yashab](../../strongs/h/h3427.md) them; for I have [racham](../../strongs/h/h7355.md) upon them: and they shall be as though I had not [zānaḥ](../../strongs/h/h2186.md) them: for I am [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), and will ['anah](../../strongs/h/h6030.md) them.

<a name="zechariah_10_7"></a>Zechariah 10:7

And they of ['Ep̄rayim](../../strongs/h/h669.md) shall be like a [gibôr](../../strongs/h/h1368.md) man, and their [leb](../../strongs/h/h3820.md) shall [samach](../../strongs/h/h8055.md) as through [yayin](../../strongs/h/h3196.md): yea, their [ben](../../strongs/h/h1121.md) shall [ra'ah](../../strongs/h/h7200.md) it, and be [samach](../../strongs/h/h8055.md); their [leb](../../strongs/h/h3820.md) shall [giyl](../../strongs/h/h1523.md) in [Yĕhovah](../../strongs/h/h3068.md).

<a name="zechariah_10_8"></a>Zechariah 10:8

I will [šāraq](../../strongs/h/h8319.md) for them, and [qāḇaṣ](../../strongs/h/h6908.md) them; for I have [pāḏâ](../../strongs/h/h6299.md) them: and they shall [rabah](../../strongs/h/h7235.md) as they have [rabah](../../strongs/h/h7235.md).

<a name="zechariah_10_9"></a>Zechariah 10:9

And I will [zāraʿ](../../strongs/h/h2232.md) them among the ['am](../../strongs/h/h5971.md): and they shall [zakar](../../strongs/h/h2142.md) me in [merḥāq](../../strongs/h/h4801.md); and they shall [ḥāyâ](../../strongs/h/h2421.md) with their [ben](../../strongs/h/h1121.md), and [shuwb](../../strongs/h/h7725.md).

<a name="zechariah_10_10"></a>Zechariah 10:10

I will [shuwb](../../strongs/h/h7725.md) them also out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and [qāḇaṣ](../../strongs/h/h6908.md) them out of ['Aššûr](../../strongs/h/h804.md); and I will [bow'](../../strongs/h/h935.md) them into the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md) and [Lᵊḇānôn](../../strongs/h/h3844.md); and place shall not be [māṣā'](../../strongs/h/h4672.md) for them.

<a name="zechariah_10_11"></a>Zechariah 10:11

And he shall ['abar](../../strongs/h/h5674.md) the [yam](../../strongs/h/h3220.md) with [tsarah](../../strongs/h/h6869.md), and shall [nakah](../../strongs/h/h5221.md) the [gal](../../strongs/h/h1530.md) in the [yam](../../strongs/h/h3220.md), and all the [mᵊṣôlâ](../../strongs/h/h4688.md) of the [yᵊ'ōr](../../strongs/h/h2975.md) shall [yāḇēš](../../strongs/h/h3001.md): and the [gā'ôn](../../strongs/h/h1347.md) of ['Aššûr](../../strongs/h/h804.md) shall be [yarad](../../strongs/h/h3381.md), and the [shebet](../../strongs/h/h7626.md) of [Mitsrayim](../../strongs/h/h4714.md) shall [cuwr](../../strongs/h/h5493.md).

<a name="zechariah_10_12"></a>Zechariah 10:12

And I will [gabar](../../strongs/h/h1396.md) them in [Yĕhovah](../../strongs/h/h3068.md); and they shall [halak](../../strongs/h/h1980.md) in his [shem](../../strongs/h/h8034.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 9](zechariah_9.md) - [Zechariah 11](zechariah_11.md)