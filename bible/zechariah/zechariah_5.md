# [Zechariah 5](https://www.blueletterbible.org/kjv/zechariah/5)

<a name="zechariah_5_1"></a>Zechariah 5:1

Then I [shuwb](../../strongs/h/h7725.md), and [nasa'](../../strongs/h/h5375.md) mine ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md), and behold a ['uwph](../../strongs/h/h5774.md) [mᵊḡillâ](../../strongs/h/h4039.md).

<a name="zechariah_5_2"></a>Zechariah 5:2

And he ['āmar](../../strongs/h/h559.md) unto me, What [ra'ah](../../strongs/h/h7200.md) thou? And I ['āmar](../../strongs/h/h559.md), I [ra'ah](../../strongs/h/h7200.md) a ['uwph](../../strongs/h/h5774.md) [mᵊḡillâ](../../strongs/h/h4039.md); the ['ōreḵ](../../strongs/h/h753.md) thereof is twenty ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) thereof ten ['ammâ](../../strongs/h/h520.md).

<a name="zechariah_5_3"></a>Zechariah 5:3

Then ['āmar](../../strongs/h/h559.md) he unto me, This is the ['alah](../../strongs/h/h423.md) that [yāṣā'](../../strongs/h/h3318.md) over the [paniym](../../strongs/h/h6440.md) of the ['erets](../../strongs/h/h776.md): for every one that [ganab](../../strongs/h/h1589.md) shall be cut [naqah](../../strongs/h/h5352.md) as on this side according to it; and every one that [shaba'](../../strongs/h/h7650.md) shall be [naqah](../../strongs/h/h5352.md) as on that side according to it.

<a name="zechariah_5_4"></a>Zechariah 5:4

I will [yāṣā'](../../strongs/h/h3318.md) it, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), and it shall [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of the [gannāḇ](../../strongs/h/h1590.md), and into the [bayith](../../strongs/h/h1004.md) of him that [shaba'](../../strongs/h/h7650.md) [sheqer](../../strongs/h/h8267.md) by my [shem](../../strongs/h/h8034.md): and it shall [lûn](../../strongs/h/h3885.md) in the [tavek](../../strongs/h/h8432.md) of his [bayith](../../strongs/h/h1004.md), and shall [kalah](../../strongs/h/h3615.md) it with the ['ets](../../strongs/h/h6086.md) thereof and the ['eben](../../strongs/h/h68.md) thereof.

<a name="zechariah_5_5"></a>Zechariah 5:5

Then the [mal'ak](../../strongs/h/h4397.md) that [dabar](../../strongs/h/h1696.md) with me [yāṣā'](../../strongs/h/h3318.md), and ['āmar](../../strongs/h/h559.md) unto me, [nasa'](../../strongs/h/h5375.md) now thine ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md) what is this that [yāṣā'](../../strongs/h/h3318.md).

<a name="zechariah_5_6"></a>Zechariah 5:6

And I ['āmar](../../strongs/h/h559.md), What is it? And he ['āmar](../../strongs/h/h559.md), This is an ['êp̄â](../../strongs/h/h374.md) that [yāṣā'](../../strongs/h/h3318.md). He ['āmar](../../strongs/h/h559.md) moreover, This is their ['ayin](../../strongs/h/h5869.md) through all the ['erets](../../strongs/h/h776.md).

<a name="zechariah_5_7"></a>Zechariah 5:7

And, behold, there was [nasa'](../../strongs/h/h5375.md) a [kikār](../../strongs/h/h3603.md) of [ʿōp̄ereṯ](../../strongs/h/h5777.md): and this is an ['ishshah](../../strongs/h/h802.md) that [yashab](../../strongs/h/h3427.md) in the [tavek](../../strongs/h/h8432.md) of the ['êp̄â](../../strongs/h/h374.md).

<a name="zechariah_5_8"></a>Zechariah 5:8

And he ['āmar](../../strongs/h/h559.md), This is [rišʿâ](../../strongs/h/h7564.md). And he [shalak](../../strongs/h/h7993.md) it into the [tavek](../../strongs/h/h8432.md) of the ['êp̄â](../../strongs/h/h374.md); and he [shalak](../../strongs/h/h7993.md) the ['eben](../../strongs/h/h68.md) of [ʿōp̄ereṯ](../../strongs/h/h5777.md) upon the [peh](../../strongs/h/h6310.md) thereof.

<a name="zechariah_5_9"></a>Zechariah 5:9

Then I [nasa'](../../strongs/h/h5375.md) mine ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md), and, behold, there [yāṣā'](../../strongs/h/h3318.md) two ['ishshah](../../strongs/h/h802.md), and the [ruwach](../../strongs/h/h7307.md) was in their [kanaph](../../strongs/h/h3671.md); for they had [kanaph](../../strongs/h/h3671.md) like the [kanaph](../../strongs/h/h3671.md) of a [ḥăsîḏâ](../../strongs/h/h2624.md): and they [nasa'](../../strongs/h/h5375.md) the ['êp̄â](../../strongs/h/h374.md) between the ['erets](../../strongs/h/h776.md) and the [shamayim](../../strongs/h/h8064.md).

<a name="zechariah_5_10"></a>Zechariah 5:10

Then ['āmar](../../strongs/h/h559.md) I to the [mal'ak](../../strongs/h/h4397.md) that [dabar](../../strongs/h/h1696.md) with me, Whither do these [yālaḵ](../../strongs/h/h3212.md) the ['êp̄â](../../strongs/h/h374.md)?

<a name="zechariah_5_11"></a>Zechariah 5:11

And he ['āmar](../../strongs/h/h559.md) unto me, To [bānâ](../../strongs/h/h1129.md) it a [bayith](../../strongs/h/h1004.md) in the ['erets](../../strongs/h/h776.md) of [Šinʿār](../../strongs/h/h8152.md): and it shall be [kuwn](../../strongs/h/h3559.md), and [yānaḥ](../../strongs/h/h3240.md) there upon her own [mᵊḵunâ](../../strongs/h/h4369.md).

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 4](zechariah_4.md) - [Zechariah 6](zechariah_6.md)