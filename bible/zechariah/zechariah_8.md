# [Zechariah 8](https://www.blueletterbible.org/kjv/zechariah/8)

<a name="zechariah_8_1"></a>Zechariah 8:1

Again the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) came to me, ['āmar](../../strongs/h/h559.md),

<a name="zechariah_8_2"></a>Zechariah 8:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); I was [qānā'](../../strongs/h/h7065.md) for [Tsiyown](../../strongs/h/h6726.md) with [gadowl](../../strongs/h/h1419.md) [qin'â](../../strongs/h/h7068.md), and I was [qānā'](../../strongs/h/h7065.md) for her with [gadowl](../../strongs/h/h1419.md) [chemah](../../strongs/h/h2534.md).

<a name="zechariah_8_3"></a>Zechariah 8:3

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); I am [shuwb](../../strongs/h/h7725.md) unto [Tsiyown](../../strongs/h/h6726.md), and will [shakan](../../strongs/h/h7931.md) in the [tavek](../../strongs/h/h8432.md) of [Yĕruwshalaim](../../strongs/h/h3389.md): and [Yĕruwshalaim](../../strongs/h/h3389.md) shall be [qara'](../../strongs/h/h7121.md) a [ʿîr](../../strongs/h/h5892.md) of ['emeth](../../strongs/h/h571.md); and the [har](../../strongs/h/h2022.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) the [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md).

<a name="zechariah_8_4"></a>Zechariah 8:4

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); There shall yet [zāqēn](../../strongs/h/h2205.md) and [zāqēn](../../strongs/h/h2205.md) [yashab](../../strongs/h/h3427.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and every ['iysh](../../strongs/h/h376.md) with his [mašʿēnâ](../../strongs/h/h4938.md) in his [yad](../../strongs/h/h3027.md) for [rōḇ](../../strongs/h/h7230.md) [yowm](../../strongs/h/h3117.md).

<a name="zechariah_8_5"></a>Zechariah 8:5

And the [rᵊḥōḇ](../../strongs/h/h7339.md) of the [ʿîr](../../strongs/h/h5892.md) shall be [mālā'](../../strongs/h/h4390.md) of [yeleḏ](../../strongs/h/h3206.md) and [yaldâ](../../strongs/h/h3207.md) [śāḥaq](../../strongs/h/h7832.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md) thereof.

<a name="zechariah_8_6"></a>Zechariah 8:6

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); If it be [pala'](../../strongs/h/h6381.md) in the ['ayin](../../strongs/h/h5869.md) of the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of this ['am](../../strongs/h/h5971.md) in these [yowm](../../strongs/h/h3117.md), should it also be [pala'](../../strongs/h/h6381.md) in mine ['ayin](../../strongs/h/h5869.md)? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="zechariah_8_7"></a>Zechariah 8:7

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); Behold, I will [yasha'](../../strongs/h/h3467.md) my ['am](../../strongs/h/h5971.md) from the [mizrach](../../strongs/h/h4217.md) ['erets](../../strongs/h/h776.md), and from the [māḇô'](../../strongs/h/h3996.md) [šemeš](../../strongs/h/h8121.md) ['erets](../../strongs/h/h776.md);

<a name="zechariah_8_8"></a>Zechariah 8:8

And I will [bow'](../../strongs/h/h935.md) them, and they shall [shakan](../../strongs/h/h7931.md) in the [tavek](../../strongs/h/h8432.md) of [Yĕruwshalaim](../../strongs/h/h3389.md): and they shall be my ['am](../../strongs/h/h5971.md), and I will be their ['Elohiym](../../strongs/h/h430.md), in ['emeth](../../strongs/h/h571.md) and in [tsedaqah](../../strongs/h/h6666.md).

<a name="zechariah_8_9"></a>Zechariah 8:9

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); Let your [yad](../../strongs/h/h3027.md) be [ḥāzaq](../../strongs/h/h2388.md), ye that [shama'](../../strongs/h/h8085.md) in these [yowm](../../strongs/h/h3117.md) these [dabar](../../strongs/h/h1697.md) by the [peh](../../strongs/h/h6310.md) of the [nāḇî'](../../strongs/h/h5030.md), which were in the [yowm](../../strongs/h/h3117.md) that the [yacad](../../strongs/h/h3245.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) was [yacad](../../strongs/h/h3245.md), that the [heykal](../../strongs/h/h1964.md) might be [bānâ](../../strongs/h/h1129.md).

<a name="zechariah_8_10"></a>Zechariah 8:10

For [paniym](../../strongs/h/h6440.md) these [yowm](../../strongs/h/h3117.md) there [hayah](../../strongs/h/h1961.md) no [śāḵār](../../strongs/h/h7939.md) for ['āḏām](../../strongs/h/h120.md), nor any [śāḵār](../../strongs/h/h7939.md) for [bĕhemah](../../strongs/h/h929.md); neither was there any [shalowm](../../strongs/h/h7965.md) to him that [yāṣā'](../../strongs/h/h3318.md) or [bow'](../../strongs/h/h935.md) because of the [tsar](../../strongs/h/h6862.md): for I [shalach](../../strongs/h/h7971.md) all ['āḏām](../../strongs/h/h120.md) every ['iysh](../../strongs/h/h376.md) against his [rea'](../../strongs/h/h7453.md).

<a name="zechariah_8_11"></a>Zechariah 8:11

But now I will not be unto the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of this ['am](../../strongs/h/h5971.md) as in the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="zechariah_8_12"></a>Zechariah 8:12

For the [zera'](../../strongs/h/h2233.md) shall be [shalowm](../../strongs/h/h7965.md); the [gep̄en](../../strongs/h/h1612.md) shall [nathan](../../strongs/h/h5414.md) her [pĕriy](../../strongs/h/h6529.md), and the ['erets](../../strongs/h/h776.md) shall [nathan](../../strongs/h/h5414.md) her [yᵊḇûl](../../strongs/h/h2981.md), and the [shamayim](../../strongs/h/h8064.md) shall [nathan](../../strongs/h/h5414.md) their [ṭal](../../strongs/h/h2919.md); and I will cause the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of this ['am](../../strongs/h/h5971.md) to [nāḥal](../../strongs/h/h5157.md) all these things.

<a name="zechariah_8_13"></a>Zechariah 8:13

And it shall come to pass, that as ye were a [qᵊlālâ](../../strongs/h/h7045.md) among the [gowy](../../strongs/h/h1471.md), O [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md), and [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md); so will I [yasha'](../../strongs/h/h3467.md) you, and ye shall be a [bĕrakah](../../strongs/h/h1293.md): [yare'](../../strongs/h/h3372.md) not, but let your [yad](../../strongs/h/h3027.md) be [ḥāzaq](../../strongs/h/h2388.md).

<a name="zechariah_8_14"></a>Zechariah 8:14

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); As I [zāmam](../../strongs/h/h2161.md) to [ra'a'](../../strongs/h/h7489.md) you, when your ['ab](../../strongs/h/h1.md) provoked me to [qāṣap̄](../../strongs/h/h7107.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), and I [nacham](../../strongs/h/h5162.md) not:

<a name="zechariah_8_15"></a>Zechariah 8:15

So [shuwb](../../strongs/h/h7725.md) have I [zāmam](../../strongs/h/h2161.md) in these [yowm](../../strongs/h/h3117.md) to do [yatab](../../strongs/h/h3190.md) unto [Yĕruwshalaim](../../strongs/h/h3389.md) and to the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md): [yare'](../../strongs/h/h3372.md) ye not.

<a name="zechariah_8_16"></a>Zechariah 8:16

These are the [dabar](../../strongs/h/h1697.md) that ye shall ['asah](../../strongs/h/h6213.md); [dabar](../../strongs/h/h1696.md) ye every ['iysh](../../strongs/h/h376.md) the ['emeth](../../strongs/h/h571.md) to his [rea'](../../strongs/h/h7453.md); [shaphat](../../strongs/h/h8199.md) the [mishpat](../../strongs/h/h4941.md) of ['emeth](../../strongs/h/h571.md) and [shalowm](../../strongs/h/h7965.md) in your [sha'ar](../../strongs/h/h8179.md):

<a name="zechariah_8_17"></a>Zechariah 8:17

And let ['iysh](../../strongs/h/h376.md) of you [chashab](../../strongs/h/h2803.md) [ra'](../../strongs/h/h7451.md) in your [lebab](../../strongs/h/h3824.md) against his [rea'](../../strongs/h/h7453.md); and ['ahab](../../strongs/h/h157.md) no [sheqer](../../strongs/h/h8267.md) [šᵊḇûʿâ](../../strongs/h/h7621.md): for all these are things that I [sane'](../../strongs/h/h8130.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="zechariah_8_18"></a>Zechariah 8:18

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="zechariah_8_19"></a>Zechariah 8:19

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); The [ṣôm](../../strongs/h/h6685.md) of the fourth month, and the [ṣôm](../../strongs/h/h6685.md) of the fifth, and the [ṣôm](../../strongs/h/h6685.md) of the seventh, and the [ṣôm](../../strongs/h/h6685.md) of the tenth, shall be to the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) [śāśôn](../../strongs/h/h8342.md) and [simchah](../../strongs/h/h8057.md), and [towb](../../strongs/h/h2896.md) [môʿēḏ](../../strongs/h/h4150.md); therefore ['ahab](../../strongs/h/h157.md) the ['emeth](../../strongs/h/h571.md) and [shalowm](../../strongs/h/h7965.md).

<a name="zechariah_8_20"></a>Zechariah 8:20

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); It shall yet come to pass, that there shall [bow'](../../strongs/h/h935.md) ['am](../../strongs/h/h5971.md), and the [yashab](../../strongs/h/h3427.md) of [rab](../../strongs/h/h7227.md) [ʿîr](../../strongs/h/h5892.md):

<a name="zechariah_8_21"></a>Zechariah 8:21

And the [yashab](../../strongs/h/h3427.md) of one shall [halak](../../strongs/h/h1980.md) to another, ['āmar](../../strongs/h/h559.md), Let us [yālaḵ](../../strongs/h/h3212.md) [halak](../../strongs/h/h1980.md) to [ḥālâ](../../strongs/h/h2470.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and to [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md): I will [yālaḵ](../../strongs/h/h3212.md) also.

<a name="zechariah_8_22"></a>Zechariah 8:22

Yea, [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) and ['atsuwm](../../strongs/h/h6099.md) [gowy](../../strongs/h/h1471.md) shall [bow'](../../strongs/h/h935.md) to [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), and to [ḥālâ](../../strongs/h/h2470.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="zechariah_8_23"></a>Zechariah 8:23

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); In those [yowm](../../strongs/h/h3117.md) it shall come to pass, that ten ['enowsh](../../strongs/h/h582.md) shall take [ḥāzaq](../../strongs/h/h2388.md) out of all [lashown](../../strongs/h/h3956.md) of the [gowy](../../strongs/h/h1471.md), even shall take [ḥāzaq](../../strongs/h/h2388.md) of the [kanaph](../../strongs/h/h3671.md) of him that is an ['iysh](../../strongs/h/h376.md) [Yᵊhûḏî](../../strongs/h/h3064.md), ['āmar](../../strongs/h/h559.md), We will [yālaḵ](../../strongs/h/h3212.md) with you: for we have [shama'](../../strongs/h/h8085.md) that ['Elohiym](../../strongs/h/h430.md) is with you.

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 7](zechariah_7.md) - [Zechariah 9](zechariah_9.md)