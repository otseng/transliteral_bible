# [Zechariah 4](https://www.blueletterbible.org/kjv/zechariah/4)

<a name="zechariah_4_1"></a>Zechariah 4:1

And the [mal'ak](../../strongs/h/h4397.md) that [dabar](../../strongs/h/h1696.md) with me [shuwb](../../strongs/h/h7725.md), and [ʿûr](../../strongs/h/h5782.md) me, as an ['iysh](../../strongs/h/h376.md) that is [ʿûr](../../strongs/h/h5782.md) out of his [šēnā'](../../strongs/h/h8142.md),

<a name="zechariah_4_2"></a>Zechariah 4:2

And ['āmar](../../strongs/h/h559.md) unto me, What [ra'ah](../../strongs/h/h7200.md) thou? And I ['āmar](../../strongs/h/h559.md), I have [ra'ah](../../strongs/h/h7200.md), and behold a [mᵊnôrâ](../../strongs/h/h4501.md) all of [zāhāḇ](../../strongs/h/h2091.md), with a [gōl](../../strongs/h/h1531.md) upon the [ro'sh](../../strongs/h/h7218.md) of it, and his seven [nîr](../../strongs/h/h5216.md) thereon, and seven [mûṣāqâ](../../strongs/h/h4166.md) to the seven [nîr](../../strongs/h/h5216.md), which are upon the [ro'sh](../../strongs/h/h7218.md) thereof:

<a name="zechariah_4_3"></a>Zechariah 4:3

And two [zayiṯ](../../strongs/h/h2132.md) by it, one upon the [yamiyn](../../strongs/h/h3225.md) of the [gullâ](../../strongs/h/h1543.md), and the other upon the [śᵊmō'l](../../strongs/h/h8040.md) thereof.

<a name="zechariah_4_4"></a>Zechariah 4:4

So I ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) to the [mal'ak](../../strongs/h/h4397.md) that [dabar](../../strongs/h/h1696.md) with me, ['āmar](../../strongs/h/h559.md), What are these, my ['adown](../../strongs/h/h113.md)?

<a name="zechariah_4_5"></a>Zechariah 4:5

Then the [mal'ak](../../strongs/h/h4397.md) that [dabar](../../strongs/h/h1696.md) with me ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto me, [yada'](../../strongs/h/h3045.md) thou not what these be? And I ['āmar](../../strongs/h/h559.md), No, my ['adown](../../strongs/h/h113.md).

<a name="zechariah_4_6"></a>Zechariah 4:6

Then he ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto me, ['āmar](../../strongs/h/h559.md), This is the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto [Zᵊrubāḇel](../../strongs/h/h2216.md), ['āmar](../../strongs/h/h559.md), Not by [ḥayil](../../strongs/h/h2428.md), nor by [koach](../../strongs/h/h3581.md), but by my [ruwach](../../strongs/h/h7307.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="zechariah_4_7"></a>Zechariah 4:7

Who art thou, O [gadowl](../../strongs/h/h1419.md) [har](../../strongs/h/h2022.md)? [paniym](../../strongs/h/h6440.md) [Zᵊrubāḇel](../../strongs/h/h2216.md) thou shalt become a [mîšôr](../../strongs/h/h4334.md): and he shall [yāṣā'](../../strongs/h/h3318.md) the ['eben](../../strongs/h/h68.md) [rō'šâ](../../strongs/h/h7222.md) thereof with [tᵊšu'â](../../strongs/h/h8663.md), [ḥēn](../../strongs/h/h2580.md), [ḥēn](../../strongs/h/h2580.md) unto it.

<a name="zechariah_4_8"></a>Zechariah 4:8

Moreover the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="zechariah_4_9"></a>Zechariah 4:9

The [yad](../../strongs/h/h3027.md) of [Zᵊrubāḇel](../../strongs/h/h2216.md) have laid the [yacad](../../strongs/h/h3245.md) of this [bayith](../../strongs/h/h1004.md); his [yad](../../strongs/h/h3027.md) shall also [batsa'](../../strongs/h/h1214.md) it; and thou shalt [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) hath [shalach](../../strongs/h/h7971.md) me unto you.

<a name="zechariah_4_10"></a>Zechariah 4:10

For who hath [bûz](../../strongs/h/h936.md) the [yowm](../../strongs/h/h3117.md) of small [qāṭān](../../strongs/h/h6996.md)? for they shall [samach](../../strongs/h/h8055.md), and shall [ra'ah](../../strongs/h/h7200.md) the ['eben](../../strongs/h/h68.md) [bᵊḏîl](../../strongs/h/h913.md) in the [yad](../../strongs/h/h3027.md) of [Zᵊrubāḇel](../../strongs/h/h2216.md) with those seven; they are the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), which [šûṭ](../../strongs/h/h7751.md) through the ['erets](../../strongs/h/h776.md).

<a name="zechariah_4_11"></a>Zechariah 4:11

Then ['anah](../../strongs/h/h6030.md) I, and ['āmar](../../strongs/h/h559.md) unto him, What are these two [zayiṯ](../../strongs/h/h2132.md) upon the [yamiyn](../../strongs/h/h3225.md) side of the [mᵊnôrâ](../../strongs/h/h4501.md) and upon the [śᵊmō'l](../../strongs/h/h8040.md) side thereof?

<a name="zechariah_4_12"></a>Zechariah 4:12

And I ['anah](../../strongs/h/h6030.md) again, and ['āmar](../../strongs/h/h559.md) unto him, What be these two [zayiṯ](../../strongs/h/h2132.md) [šibōleṯ](../../strongs/h/h7641.md) which [yad](../../strongs/h/h3027.md) the two [zāhāḇ](../../strongs/h/h2091.md) [ṣantᵊrôṯ](../../strongs/h/h6804.md) [rîq](../../strongs/h/h7324.md) the [zāhāḇ](../../strongs/h/h2091.md) oil out of themselves?

<a name="zechariah_4_13"></a>Zechariah 4:13

And he ['āmar](../../strongs/h/h559.md) me and ['āmar](../../strongs/h/h559.md), [yada'](../../strongs/h/h3045.md) thou not what these be? And I ['āmar](../../strongs/h/h559.md), No, my ['adown](../../strongs/h/h113.md).

<a name="zechariah_4_14"></a>Zechariah 4:14

Then ['āmar](../../strongs/h/h559.md) he, These are the two [yiṣhār](../../strongs/h/h3323.md) [ben](../../strongs/h/h1121.md), that ['amad](../../strongs/h/h5975.md) by the ['adown](../../strongs/h/h113.md) of the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Zechariah](zechariah.md)

[Zechariah 3](zechariah_3.md) - [Zechariah 5](zechariah_5.md)