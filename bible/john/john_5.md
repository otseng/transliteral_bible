# [John 5](https://www.blueletterbible.org/kjv/jhn/5/1/rl1/s_1002001)

<a name="john_5_1"></a>John 5:1

After this there was a [heortē](../../strongs/g/g1859.md) of the [Ioudaios](../../strongs/g/g2453.md); and [Iēsous](../../strongs/g/g2424.md) [anabainō](../../strongs/g/g305.md) to [Hierosolyma](../../strongs/g/g2414.md).

<a name="john_5_2"></a>John 5:2

Now there is at [Hierosolyma](../../strongs/g/g2414.md) by the [probatikos](../../strongs/g/g4262.md) a [kolymbēthra](../../strongs/g/g2861.md), which is [epilegō](../../strongs/g/g1951.md) in the [Hebraïsti](../../strongs/g/g1447.md) [Bēthesda](../../strongs/g/g964.md), having five [stoa](../../strongs/g/g4745.md).

<a name="john_5_3"></a>John 5:3

In these [katakeimai](../../strongs/g/g2621.md) a [polys](../../strongs/g/g4183.md) [plēthos](../../strongs/g/g4128.md) of [astheneō](../../strongs/g/g770.md), of [typhlos](../../strongs/g/g5185.md), [chōlos](../../strongs/g/g5560.md), [xēros](../../strongs/g/g3584.md), [ekdechomai](../../strongs/g/g1551.md) for the [kinēsis](../../strongs/g/g2796.md) of the [hydōr](../../strongs/g/g5204.md). [^1]

<a name="john_5_4"></a>John 5:4

For an [aggelos](../../strongs/g/g32.md) [katabainō](../../strongs/g/g2597.md) at a [kairos](../../strongs/g/g2540.md) into the [kolymbēthra](../../strongs/g/g2861.md), and [tarassō](../../strongs/g/g5015.md) the [hydōr](../../strongs/g/g5204.md): whosoever then first after the [tarachē](../../strongs/g/g5016.md) of the [hydōr](../../strongs/g/g5204.md) [embainō](../../strongs/g/g1684.md) was [ginomai](../../strongs/g/g1096.md) [hygiēs](../../strongs/g/g5199.md) of [dēpote](../../strongs/g/g1221.md) [nosēma](../../strongs/g/g3553.md) he [katechō](../../strongs/g/g2722.md).

<a name="john_5_5"></a>John 5:5

And a [tis](../../strongs/g/g5100.md) [anthrōpos](../../strongs/g/g444.md) was there, which had an [astheneia](../../strongs/g/g769.md) thirty and eight [etos](../../strongs/g/g2094.md).

<a name="john_5_6"></a>John 5:6

When [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) him [katakeimai](../../strongs/g/g2621.md), and [ginōskō](../../strongs/g/g1097.md) that he had been now a [polys](../../strongs/g/g4183.md) [chronos](../../strongs/g/g5550.md), he [legō](../../strongs/g/g3004.md) unto him, **Wilt thou be [ginomai](../../strongs/g/g1096.md) [hygiēs](../../strongs/g/g5199.md)?**

<a name="john_5_7"></a>John 5:7

The [astheneō](../../strongs/g/g770.md) [apokrinomai](../../strongs/g/g611.md) him, [kyrios](../../strongs/g/g2962.md), I have [ou](../../strongs/g/g3756.md) [anthrōpos](../../strongs/g/g444.md), when the [hydōr](../../strongs/g/g5204.md) is [tarassō](../../strongs/g/g5015.md), to [ballō](../../strongs/g/g906.md) me into the [kolymbēthra](../../strongs/g/g2861.md): but while I am [erchomai](../../strongs/g/g2064.md), another [katabainō](../../strongs/g/g2597.md) before me.

<a name="john_5_8"></a>John 5:8

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, **[egeirō](../../strongs/g/g1453.md), [airō](../../strongs/g/g142.md) thy [krabattos](../../strongs/g/g2895.md), and [peripateō](../../strongs/g/g4043.md).**

<a name="john_5_9"></a>John 5:9

And [eutheōs](../../strongs/g/g2112.md) the [anthrōpos](../../strongs/g/g444.md) was [ginomai](../../strongs/g/g1096.md) [hygiēs](../../strongs/g/g5199.md), and [airō](../../strongs/g/g142.md) his [krabattos](../../strongs/g/g2895.md), and [peripateō](../../strongs/g/g4043.md): and on the same [hēmera](../../strongs/g/g2250.md) was the [sabbaton](../../strongs/g/g4521.md).

<a name="john_5_10"></a>John 5:10

The [Ioudaios](../../strongs/g/g2453.md) therefore [legō](../../strongs/g/g3004.md) unto [therapeuō](../../strongs/g/g2323.md), It is the [sabbaton](../../strongs/g/g4521.md): it is not [exesti](../../strongs/g/g1832.md) for thee to [airō](../../strongs/g/g142.md) [krabattos](../../strongs/g/g2895.md).

<a name="john_5_11"></a>John 5:11

He [apokrinomai](../../strongs/g/g611.md) them, He that [poieō](../../strongs/g/g4160.md) me [hygiēs](../../strongs/g/g5199.md), the same [eipon](../../strongs/g/g2036.md) unto me, **[airō](../../strongs/g/g142.md) thy [krabattos](../../strongs/g/g2895.md), and [peripateō](../../strongs/g/g4043.md).**

<a name="john_5_12"></a>John 5:12

Then [erōtaō](../../strongs/g/g2065.md) they him, What [anthrōpos](../../strongs/g/g444.md) is that which [eipon](../../strongs/g/g2036.md) unto thee, **[airō](../../strongs/g/g142.md) thy [krabattos](../../strongs/g/g2895.md), and [peripateō](../../strongs/g/g4043.md)?**

<a name="john_5_13"></a>John 5:13

And he that was [iaomai](../../strongs/g/g2390.md) [eidō](../../strongs/g/g1492.md) not who it was: for [Iēsous](../../strongs/g/g2424.md) had [ekneuō](../../strongs/g/g1593.md) himself away, an [ochlos](../../strongs/g/g3793.md) being in [topos](../../strongs/g/g5117.md).

<a name="john_5_14"></a>John 5:14

Afterward [Iēsous](../../strongs/g/g2424.md) [heuriskō](../../strongs/g/g2147.md) him in the [hieron](../../strongs/g/g2411.md), and [eipon](../../strongs/g/g2036.md) unto him, **[ide](../../strongs/g/g2396.md), thou art [ginomai](../../strongs/g/g1096.md) [hygiēs](../../strongs/g/g5199.md): [hamartanō](../../strongs/g/g264.md) [mēketi](../../strongs/g/g3371.md), lest a [cheirōn](../../strongs/g/g5501.md) [ginomai](../../strongs/g/g1096.md) unto thee.**

<a name="john_5_15"></a>John 5:15

The [anthrōpos](../../strongs/g/g444.md) [aperchomai](../../strongs/g/g565.md), and [anaggellō](../../strongs/g/g312.md) the [Ioudaios](../../strongs/g/g2453.md) that it was [Iēsous](../../strongs/g/g2424.md), which had [poieō](../../strongs/g/g4160.md) him [hygiēs](../../strongs/g/g5199.md).

<a name="john_5_16"></a>John 5:16

And therefore did the [Ioudaios](../../strongs/g/g2453.md) [diōkō](../../strongs/g/g1377.md) [Iēsous](../../strongs/g/g2424.md), and [zēteō](../../strongs/g/g2212.md) to [apokteinō](../../strongs/g/g615.md) him, because he had [poieō](../../strongs/g/g4160.md) these things on the [sabbaton](../../strongs/g/g4521.md).

<a name="john_5_17"></a>John 5:17

But [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) them, **My [patēr](../../strongs/g/g3962.md) [ergazomai](../../strongs/g/g2038.md) hitherto, and I [ergazomai](../../strongs/g/g2038.md).**

<a name="john_5_18"></a>John 5:18

Therefore the [Ioudaios](../../strongs/g/g2453.md) [zēteō](../../strongs/g/g2212.md) the more to [apokteinō](../../strongs/g/g615.md) him, because he not only had [lyō](../../strongs/g/g3089.md) the [sabbaton](../../strongs/g/g4521.md), but [legō](../../strongs/g/g3004.md) also that [theos](../../strongs/g/g2316.md) was his [patēr](../../strongs/g/g3962.md), [poieō](../../strongs/g/g4160.md) himself [isos](../../strongs/g/g2470.md) with [theos](../../strongs/g/g2316.md).

<a name="john_5_19"></a>John 5:19

Then [apokrinomai](../../strongs/g/g611.md) [Iēsous](../../strongs/g/g2424.md) and [eipon](../../strongs/g/g2036.md) unto them, **[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, The [huios](../../strongs/g/g5207.md) can do [oudeis](../../strongs/g/g3762.md) of himself, but what he [blepō](../../strongs/g/g991.md) the [patēr](../../strongs/g/g3962.md) [poieō](../../strongs/g/g4160.md): for what things soever he [poieō](../../strongs/g/g4160.md), these also [poieō](../../strongs/g/g4160.md) the [huios](../../strongs/g/g5207.md) [homoiōs](../../strongs/g/g3668.md).**

<a name="john_5_20"></a>John 5:20

**For the [patēr](../../strongs/g/g3962.md) [phileō](../../strongs/g/g5368.md) the [huios](../../strongs/g/g5207.md), and [deiknyō](../../strongs/g/g1166.md) him all things that himself [poieō](../../strongs/g/g4160.md): and he will [deiknyō](../../strongs/g/g1166.md) him [meizōn](../../strongs/g/g3187.md) [ergon](../../strongs/g/g2041.md) than these, that ye may [thaumazō](../../strongs/g/g2296.md).**

<a name="john_5_21"></a>John 5:21

**For as the [patēr](../../strongs/g/g3962.md) [egeirō](../../strongs/g/g1453.md) the [nekros](../../strongs/g/g3498.md), and [zōopoieō](../../strongs/g/g2227.md) them; even so the [huios](../../strongs/g/g5207.md) [zōopoieō](../../strongs/g/g2227.md) whom he will.**

<a name="john_5_22"></a>John 5:22

**For the [patēr](../../strongs/g/g3962.md) [krinō](../../strongs/g/g2919.md) [oudeis](../../strongs/g/g3762.md), but hath [didōmi](../../strongs/g/g1325.md) all [krisis](../../strongs/g/g2920.md) unto the [huios](../../strongs/g/g5207.md):**

<a name="john_5_23"></a>John 5:23

**That all should [timaō](../../strongs/g/g5091.md) the [huios](../../strongs/g/g5207.md), even as they [timaō](../../strongs/g/g5091.md) the [patēr](../../strongs/g/g3962.md). He that [timaō](../../strongs/g/g5091.md) not the [huios](../../strongs/g/g5207.md) [timaō](../../strongs/g/g5091.md) not the [patēr](../../strongs/g/g3962.md) which hath [pempō](../../strongs/g/g3992.md) him.**

<a name="john_5_24"></a>John 5:24

**[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, He that [akouō](../../strongs/g/g191.md) my [logos](../../strongs/g/g3056.md), and [pisteuō](../../strongs/g/g4100.md) on him that [pempō](../../strongs/g/g3992.md) me, hath [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md), and shall not [erchomai](../../strongs/g/g2064.md) into [krisis](../../strongs/g/g2920.md); but is [metabainō](../../strongs/g/g3327.md) from [thanatos](../../strongs/g/g2288.md) unto [zōē](../../strongs/g/g2222.md).**

<a name="john_5_25"></a>John 5:25

**[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, The [hōra](../../strongs/g/g5610.md) is [erchomai](../../strongs/g/g2064.md), and now is, when the [nekros](../../strongs/g/g3498.md) shall [akouō](../../strongs/g/g191.md) the [phōnē](../../strongs/g/g5456.md) of the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md): and they that [akouō](../../strongs/g/g191.md) shall [zaō](../../strongs/g/g2198.md).**

<a name="john_5_26"></a>John 5:26

**For as the [patēr](../../strongs/g/g3962.md) hath [zōē](../../strongs/g/g2222.md) in himself; so hath he [didōmi](../../strongs/g/g1325.md) to the [huios](../../strongs/g/g5207.md) to have [zōē](../../strongs/g/g2222.md) in himself;**

<a name="john_5_27"></a>John 5:27

**And hath [didōmi](../../strongs/g/g1325.md) him [exousia](../../strongs/g/g1849.md) to [poieō](../../strongs/g/g4160.md) [krisis](../../strongs/g/g2920.md) also, because he is the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md).**

<a name="john_5_28"></a>John 5:28

**[thaumazō](../../strongs/g/g2296.md) not at this: for the [hōra](../../strongs/g/g5610.md) is [erchomai](../../strongs/g/g2064.md), in the which all that are in the [mnēmeion](../../strongs/g/g3419.md) shall [akouō](../../strongs/g/g191.md) his [phōnē](../../strongs/g/g5456.md),**

<a name="john_5_29"></a>John 5:29

**And shall [ekporeuomai](../../strongs/g/g1607.md); they that have [poieō](../../strongs/g/g4160.md) [agathos](../../strongs/g/g18.md), unto the [anastasis](../../strongs/g/g386.md) of [zōē](../../strongs/g/g2222.md); and they that have [prassō](../../strongs/g/g4238.md) [phaulos](../../strongs/g/g5337.md), unto the [anastasis](../../strongs/g/g386.md) of [krisis](../../strongs/g/g2920.md).**

<a name="john_5_30"></a>John 5:30

**I can [emautou](../../strongs/g/g1683.md) [poieō](../../strongs/g/g4160.md) [ou](../../strongs/g/g3756.md) [oudeis](../../strongs/g/g3762.md): as [akouō](../../strongs/g/g191.md), I [krinō](../../strongs/g/g2919.md): and my [krisis](../../strongs/g/g2920.md) is [dikaios](../../strongs/g/g1342.md); because I [zēteō](../../strongs/g/g2212.md) not mine own [thelēma](../../strongs/g/g2307.md), but the [thelēma](../../strongs/g/g2307.md) of the [patēr](../../strongs/g/g3962.md) which hath [pempō](../../strongs/g/g3992.md) me.**

<a name="john_5_31"></a>John 5:31

**If I [martyreō](../../strongs/g/g3140.md) of myself, my [martyria](../../strongs/g/g3141.md) is not [alēthēs](../../strongs/g/g227.md).**

<a name="john_5_32"></a>John 5:32

**There is another that [martyreō](../../strongs/g/g3140.md) of me; and I [eidō](../../strongs/g/g1492.md) that the [martyria](../../strongs/g/g3141.md) which he [martyreō](../../strongs/g/g3140.md) of me is [alēthēs](../../strongs/g/g227.md).**

<a name="john_5_33"></a>John 5:33

**Ye [apostellō](../../strongs/g/g649.md) unto [Iōannēs](../../strongs/g/g2491.md), and he [martyreō](../../strongs/g/g3140.md) unto the [alētheia](../../strongs/g/g225.md).**

<a name="john_5_34"></a>John 5:34

**But I [lambanō](../../strongs/g/g2983.md) not [martyria](../../strongs/g/g3141.md) from [anthrōpos](../../strongs/g/g444.md): but these things I [legō](../../strongs/g/g3004.md), that ye might be [sōzō](../../strongs/g/g4982.md).**

<a name="john_5_35"></a>John 5:35

**He was a [kaiō](../../strongs/g/g2545.md) and a [phainō](../../strongs/g/g5316.md) [lychnos](../../strongs/g/g3088.md): and ye were [thelō](../../strongs/g/g2309.md) for an [hōra](../../strongs/g/g5610.md) to [agalliaō](../../strongs/g/g21.md) in his [phōs](../../strongs/g/g5457.md).**

<a name="john_5_36"></a>John 5:36

**But I have [meizōn](../../strongs/g/g3187.md) [martyria](../../strongs/g/g3141.md) than of [Iōannēs](../../strongs/g/g2491.md): for the [ergon](../../strongs/g/g2041.md) which the [patēr](../../strongs/g/g3962.md) hath [didōmi](../../strongs/g/g1325.md) me to [teleioō](../../strongs/g/g5048.md), the same [ergon](../../strongs/g/g2041.md) that I [poieō](../../strongs/g/g4160.md), [martyreō](../../strongs/g/g3140.md) of me, that the [patēr](../../strongs/g/g3962.md) hath [apostellō](../../strongs/g/g649.md) me.**

<a name="john_5_37"></a>John 5:37

**And the [patēr](../../strongs/g/g3962.md) himself, which hath [pempō](../../strongs/g/g3992.md) me, hath [martyreō](../../strongs/g/g3140.md) of me. Ye have neither [akouō](../../strongs/g/g191.md) his [phōnē](../../strongs/g/g5456.md) [pōpote](../../strongs/g/g4455.md), nor [horaō](../../strongs/g/g3708.md) his [eidos](../../strongs/g/g1491.md).**

<a name="john_5_38"></a>John 5:38

**And ye have not his [logos](../../strongs/g/g3056.md) [menō](../../strongs/g/g3306.md) in you: for whom he hath [apostellō](../../strongs/g/g649.md), him ye [pisteuō](../../strongs/g/g4100.md) not.**

<a name="john_5_39"></a>John 5:39

**[eraunaō](../../strongs/g/g2045.md) the [graphē](../../strongs/g/g1124.md); for in them ye [dokeō](../../strongs/g/g1380.md) ye have [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md): and they are they which [martyreō](../../strongs/g/g3140.md) of me.**

<a name="john_5_40"></a>John 5:40

**And ye will not [erchomai](../../strongs/g/g2064.md) to me, that ye might have [zōē](../../strongs/g/g2222.md).**

<a name="john_5_41"></a>John 5:41

**I [lambanō](../../strongs/g/g2983.md) not [doxa](../../strongs/g/g1391.md) from [anthrōpos](../../strongs/g/g444.md).**

<a name="john_5_42"></a>John 5:42

**But I [ginōskō](../../strongs/g/g1097.md) you, that ye have not the [agapē](../../strongs/g/g26.md) of [theos](../../strongs/g/g2316.md) in you.**

<a name="john_5_43"></a>John 5:43

**I am [erchomai](../../strongs/g/g2064.md) in my [patēr](../../strongs/g/g3962.md) [onoma](../../strongs/g/g3686.md), and ye [lambanō](../../strongs/g/g2983.md) me not: if another shall [erchomai](../../strongs/g/g2064.md) in his own [onoma](../../strongs/g/g3686.md), him ye will [lambanō](../../strongs/g/g2983.md).**

<a name="john_5_44"></a>John 5:44

**How can ye [pisteuō](../../strongs/g/g4100.md), which [lambanō](../../strongs/g/g2983.md) [doxa](../../strongs/g/g1391.md) of [allēlōn](../../strongs/g/g240.md), and [zēteō](../../strongs/g/g2212.md) not the [doxa](../../strongs/g/g1391.md) that from [theos](../../strongs/g/g2316.md) [monos](../../strongs/g/g3441.md)?**

<a name="john_5_45"></a>John 5:45

**Do not [dokeō](../../strongs/g/g1380.md) that I will [katēgoreō](../../strongs/g/g2723.md) you to the [patēr](../../strongs/g/g3962.md): there is one that [katēgoreō](../../strongs/g/g2723.md) you, even [Mōÿsēs](../../strongs/g/g3475.md), in whom ye [elpizō](../../strongs/g/g1679.md).**

<a name="john_5_46"></a>John 5:46

**For had ye [pisteuō](../../strongs/g/g4100.md) [Mōÿsēs](../../strongs/g/g3475.md), ye would have [pisteuō](../../strongs/g/g4100.md) me; for he [graphō](../../strongs/g/g1125.md) of me.**

<a name="john_5_47"></a>John 5:47

**But if ye [pisteuō](../../strongs/g/g4100.md) not his [gramma](../../strongs/g/g1121.md), how shall ye [pisteuō](../../strongs/g/g4100.md) my [rhēma](../../strongs/g/g4487.md)?**

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 4](john_4.md) - [John 6](john_6.md)

---

[^1]: [John 5:3 Commentary](../../commentary/john/john_5_commentary.md#john_5_3)
