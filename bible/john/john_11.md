# [John 11](https://www.blueletterbible.org/kjv/jhn/11/1/p0/rl1/s_1005001)

<a name="john_11_1"></a>John 11:1

Now a certain was [astheneō](../../strongs/g/g770.md), [Lazaros](../../strongs/g/g2976.md), of [Bēthania](../../strongs/g/g963.md), the [kōmē](../../strongs/g/g2968.md) of [Maria](../../strongs/g/g3137.md) and her [adelphē](../../strongs/g/g79.md) [Martha](../../strongs/g/g3136.md).

<a name="john_11_2"></a>John 11:2

(It was [Maria](../../strongs/g/g3137.md) which [aleiphō](../../strongs/g/g218.md) the [kyrios](../../strongs/g/g2962.md) with [myron](../../strongs/g/g3464.md), and [ekmassō](../../strongs/g/g1591.md) his [pous](../../strongs/g/g4228.md) with her [thrix](../../strongs/g/g2359.md), whose [adelphos](../../strongs/g/g80.md) [Lazaros](../../strongs/g/g2976.md) was [astheneō](../../strongs/g/g770.md).)

<a name="john_11_3"></a>John 11:3

Therefore his [adelphē](../../strongs/g/g79.md) [apostellō](../../strongs/g/g649.md) unto him, [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), [ide](../../strongs/g/g2396.md), he whom thou [phileō](../../strongs/g/g5368.md) is [astheneō](../../strongs/g/g770.md).

<a name="john_11_4"></a>John 11:4

When [Iēsous](../../strongs/g/g2424.md) [akouō](../../strongs/g/g191.md), he [eipon](../../strongs/g/g2036.md), **This [astheneia](../../strongs/g/g769.md) is not unto [thanatos](../../strongs/g/g2288.md), but for the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md), that the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md) might be [doxazō](../../strongs/g/g1392.md) thereby.**

<a name="john_11_5"></a>John 11:5

Now [Iēsous](../../strongs/g/g2424.md) [agapaō](../../strongs/g/g25.md) [Martha](../../strongs/g/g3136.md), and her [adelphē](../../strongs/g/g79.md), and [Lazaros](../../strongs/g/g2976.md).

<a name="john_11_6"></a>John 11:6

When he had [akouō](../../strongs/g/g191.md) therefore that he was [astheneō](../../strongs/g/g770.md), he [menō](../../strongs/g/g3306.md) two [hēmera](../../strongs/g/g2250.md) still in the [topos](../../strongs/g/g5117.md) where he was.

<a name="john_11_7"></a>John 11:7

Then after that [legō](../../strongs/g/g3004.md) he to [mathētēs](../../strongs/g/g3101.md), **Let us [agō](../../strongs/g/g71.md) into [Ioudaia](../../strongs/g/g2449.md) again.**

<a name="john_11_8"></a>John 11:8

[mathētēs](../../strongs/g/g3101.md) [legō](../../strongs/g/g3004.md) unto him, [rhabbi](../../strongs/g/g4461.md), the [Ioudaios](../../strongs/g/g2453.md) of late [zēteō](../../strongs/g/g2212.md) to [lithazō](../../strongs/g/g3034.md) thee; and [hypagō](../../strongs/g/g5217.md) thou thither again?

<a name="john_11_9"></a>John 11:9

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md), **Are there not twelve [hōra](../../strongs/g/g5610.md) in the [hēmera](../../strongs/g/g2250.md)? If any man [peripateō](../../strongs/g/g4043.md) in the [hēmera](../../strongs/g/g2250.md), he [proskoptō](../../strongs/g/g4350.md) not, because he [blepō](../../strongs/g/g991.md) the [phōs](../../strongs/g/g5457.md) of this [kosmos](../../strongs/g/g2889.md).**

<a name="john_11_10"></a>John 11:10

**But if [tis](../../strongs/g/g5100.md) [peripateō](../../strongs/g/g4043.md) in the [nyx](../../strongs/g/g3571.md), he [proskoptō](../../strongs/g/g4350.md), because there is no [phōs](../../strongs/g/g5457.md) in him.**

<a name="john_11_11"></a>John 11:11

These things [eipon](../../strongs/g/g2036.md) he: and after that he [legō](../../strongs/g/g3004.md) unto them, **Our [philos](../../strongs/g/g5384.md) [Lazaros](../../strongs/g/g2976.md) [koimaō](../../strongs/g/g2837.md); but I [poreuō](../../strongs/g/g4198.md), that I may [exypnizō](../../strongs/g/g1852.md) him.**

<a name="john_11_12"></a>John 11:12

Then [eipon](../../strongs/g/g2036.md) his [mathētēs](../../strongs/g/g3101.md), [kyrios](../../strongs/g/g2962.md), if he [koimaō](../../strongs/g/g2837.md), he shall [sōzō](../../strongs/g/g4982.md).

<a name="john_11_13"></a>John 11:13

Howbeit [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2046.md) of his [thanatos](../../strongs/g/g2288.md): but they [dokeō](../../strongs/g/g1380.md) that he had [legō](../../strongs/g/g3004.md) of [koimēsis](../../strongs/g/g2838.md) in [hypnos](../../strongs/g/g5258.md).

<a name="john_11_14"></a>John 11:14

Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) unto them [parrēsia](../../strongs/g/g3954.md), **[Lazaros](../../strongs/g/g2976.md) is [apothnēskō](../../strongs/g/g599.md).**

<a name="john_11_15"></a>John 11:15

**And I am [chairō](../../strongs/g/g5463.md) for your sakes that I was not there, to the intent ye may [pisteuō](../../strongs/g/g4100.md); nevertheless let us [agō](../../strongs/g/g71.md) unto him.**

<a name="john_11_16"></a>John 11:16

Then [eipon](../../strongs/g/g2036.md) [Thōmas](../../strongs/g/g2381.md), which is [legō](../../strongs/g/g3004.md) [didymos](../../strongs/g/g1324.md), unto his [symmathētēs](../../strongs/g/g4827.md), Let us also [agō](../../strongs/g/g71.md), that we may [apothnēskō](../../strongs/g/g599.md) with him.

<a name="john_11_17"></a>John 11:17

Then when [Iēsous](../../strongs/g/g2424.md) [erchomai](../../strongs/g/g2064.md), he [heuriskō](../../strongs/g/g2147.md) that he had in the [mnēmeion](../../strongs/g/g3419.md) four [hēmera](../../strongs/g/g2250.md) already.

<a name="john_11_18"></a>John 11:18

Now [Bēthania](../../strongs/g/g963.md) was [eggys](../../strongs/g/g1451.md) unto [Hierosolyma](../../strongs/g/g2414.md), about fifteen [stadion](../../strongs/g/g4712.md) off:

<a name="john_11_19"></a>John 11:19

And [polys](../../strongs/g/g4183.md) of the [Ioudaios](../../strongs/g/g2453.md) [erchomai](../../strongs/g/g2064.md) to [Martha](../../strongs/g/g3136.md) and [Maria](../../strongs/g/g3137.md), to [paramytheomai](../../strongs/g/g3888.md) them concerning their [adelphos](../../strongs/g/g80.md).

<a name="john_11_20"></a>John 11:20

Then [Martha](../../strongs/g/g3136.md), as soon as she [akouō](../../strongs/g/g191.md) that [Iēsous](../../strongs/g/g2424.md) was [erchomai](../../strongs/g/g2064.md), [hypantaō](../../strongs/g/g5221.md) him: but [Maria](../../strongs/g/g3137.md) [kathezomai](../../strongs/g/g2516.md) in the [oikos](../../strongs/g/g3624.md).

<a name="john_11_21"></a>John 11:21

Then [eipon](../../strongs/g/g2036.md) [Martha](../../strongs/g/g3136.md) unto [Iēsous](../../strongs/g/g2424.md), [kyrios](../../strongs/g/g2962.md), if thou hadst been here, my [adelphos](../../strongs/g/g80.md) had not [thnēskō](../../strongs/g/g2348.md).

<a name="john_11_22"></a>John 11:22

But I [eidō](../../strongs/g/g1492.md), that even now, whatsoever thou wilt [aiteō](../../strongs/g/g154.md) of [theos](../../strongs/g/g2316.md), [theos](../../strongs/g/g2316.md) will [didōmi](../../strongs/g/g1325.md) thee.

<a name="john_11_23"></a>John 11:23

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto her, **Thy [adelphos](../../strongs/g/g80.md) shall [anistēmi](../../strongs/g/g450.md).**

<a name="john_11_24"></a>John 11:24

[Martha](../../strongs/g/g3136.md) [legō](../../strongs/g/g3004.md) unto him, I [eidō](../../strongs/g/g1492.md) that he shall [anistēmi](../../strongs/g/g450.md) in the [anastasis](../../strongs/g/g386.md) at the [eschatos](../../strongs/g/g2078.md) [hēmera](../../strongs/g/g2250.md).

<a name="john_11_25"></a>John 11:25

[Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto her, **I am the [anastasis](../../strongs/g/g386.md), and the [zōē](../../strongs/g/g2222.md): he that [pisteuō](../../strongs/g/g4100.md) in me, though he were [apothnēskō](../../strongs/g/g599.md), yet shall he [zaō](../../strongs/g/g2198.md):**

<a name="john_11_26"></a>John 11:26

**And whosoever [zaō](../../strongs/g/g2198.md) and [pisteuō](../../strongs/g/g4100.md) in me shall [ou mē](../../strongs/g/g3364.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) [apothnēskō](../../strongs/g/g599.md). [pisteuō](../../strongs/g/g4100.md) thou this?**

<a name="john_11_27"></a>John 11:27

She [legō](../../strongs/g/g3004.md) unto him, [nai](../../strongs/g/g3483.md), [kyrios](../../strongs/g/g2962.md): I [pisteuō](../../strongs/g/g4100.md) that thou art the [Christos](../../strongs/g/g5547.md), the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), which should [erchomai](../../strongs/g/g2064.md) into the [kosmos](../../strongs/g/g2889.md).

<a name="john_11_28"></a>John 11:28

And when she had so [eipon](../../strongs/g/g2036.md), she [aperchomai](../../strongs/g/g565.md), and [phōneō](../../strongs/g/g5455.md) [Maria](../../strongs/g/g3137.md) her [adelphē](../../strongs/g/g79.md) [lathra](../../strongs/g/g2977.md), [eipon](../../strongs/g/g2036.md), [didaskalos](../../strongs/g/g1320.md) is [pareimi](../../strongs/g/g3918.md), and [phōneō](../../strongs/g/g5455.md) for thee.

<a name="john_11_29"></a>John 11:29

As soon as she [akouō](../../strongs/g/g191.md), she [egeirō](../../strongs/g/g1453.md) [tachy](../../strongs/g/g5035.md), and [erchomai](../../strongs/g/g2064.md) unto him.

<a name="john_11_30"></a>John 11:30

Now [Iēsous](../../strongs/g/g2424.md) was not yet [erchomai](../../strongs/g/g2064.md) into the [kōmē](../../strongs/g/g2968.md), but was in that [topos](../../strongs/g/g5117.md) where [Martha](../../strongs/g/g3136.md) [hypantaō](../../strongs/g/g5221.md) him.

<a name="john_11_31"></a>John 11:31

The [Ioudaios](../../strongs/g/g2453.md) then which were with her in the [oikia](../../strongs/g/g3614.md), and [paramytheomai](../../strongs/g/g3888.md) her, when they [eidō](../../strongs/g/g1492.md) [Maria](../../strongs/g/g3137.md), that she [anistēmi](../../strongs/g/g450.md) [tacheōs](../../strongs/g/g5030.md) and [exerchomai](../../strongs/g/g1831.md), [akoloutheō](../../strongs/g/g190.md) her, [legō](../../strongs/g/g3004.md), She [hypagō](../../strongs/g/g5217.md) unto the [mnēmeion](../../strongs/g/g3419.md) to [klaiō](../../strongs/g/g2799.md) there.

<a name="john_11_32"></a>John 11:32

Then when [Maria](../../strongs/g/g3137.md) was [erchomai](../../strongs/g/g2064.md) where [Iēsous](../../strongs/g/g2424.md) was, and [eidō](../../strongs/g/g1492.md) him, she [piptō](../../strongs/g/g4098.md) at his [pous](../../strongs/g/g4228.md), [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), if thou hadst been here, my [adelphos](../../strongs/g/g80.md) had not [apothnēskō](../../strongs/g/g599.md).

<a name="john_11_33"></a>John 11:33

When [Iēsous](../../strongs/g/g2424.md) therefore [eidō](../../strongs/g/g1492.md) her [klaiō](../../strongs/g/g2799.md), and the [Ioudaios](../../strongs/g/g2453.md) also [klaiō](../../strongs/g/g2799.md) which [synerchomai](../../strongs/g/g4905.md) with her, he [embrimaomai](../../strongs/g/g1690.md) in the [pneuma](../../strongs/g/g4151.md), and was [tarassō](../../strongs/g/g5015.md).

<a name="john_11_34"></a>John 11:34

And [eipon](../../strongs/g/g2036.md), **Where have ye [tithēmi](../../strongs/g/g5087.md) him? They [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), [erchomai](../../strongs/g/g2064.md) and [eidō](../../strongs/g/g1492.md).**

<a name="john_11_35"></a>John 11:35

[Iēsous](../../strongs/g/g2424.md) [dakryō](../../strongs/g/g1145.md).

<a name="john_11_36"></a>John 11:36

Then [legō](../../strongs/g/g3004.md) the [Ioudaios](../../strongs/g/g2453.md), [ide](../../strongs/g/g2396.md) how he [phileō](../../strongs/g/g5368.md) him!

<a name="john_11_37"></a>John 11:37

And some of them [eipon](../../strongs/g/g2036.md), Could not [houtos](../../strongs/g/g3778.md), which [anoigō](../../strongs/g/g455.md) the [ophthalmos](../../strongs/g/g3788.md) of the [typhlos](../../strongs/g/g5185.md), have [poieō](../../strongs/g/g4160.md) that even [houtos](../../strongs/g/g3778.md) should not have [apothnēskō](../../strongs/g/g599.md)?

<a name="john_11_38"></a>John 11:38

[Iēsous](../../strongs/g/g2424.md) therefore again [embrimaomai](../../strongs/g/g1690.md) in himself [erchomai](../../strongs/g/g2064.md) to the [mnēmeion](../../strongs/g/g3419.md). It was a [spēlaion](../../strongs/g/g4693.md), and a [lithos](../../strongs/g/g3037.md) [epikeimai](../../strongs/g/g1945.md) upon it.

<a name="john_11_39"></a>John 11:39

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md), **[airō](../../strongs/g/g142.md) the [lithos](../../strongs/g/g3037.md).** [Martha](../../strongs/g/g3136.md), the [adelphē](../../strongs/g/g79.md) of him that was [thnēskō](../../strongs/g/g2348.md), [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), by this time he [ozō](../../strongs/g/g3605.md): for he hath been [tetartaios](../../strongs/g/g5066.md).

<a name="john_11_40"></a>John 11:40

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto her, **[eipon](../../strongs/g/g2036.md) I not unto thee, that, if thou wouldest [pisteuō](../../strongs/g/g4100.md), thou shouldest [optanomai](../../strongs/g/g3700.md) the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md)?**

<a name="john_11_41"></a>John 11:41

Then they [airō](../../strongs/g/g142.md) the [lithos](../../strongs/g/g3037.md) where the [thnēskō](../../strongs/g/g2348.md) was [keimai](../../strongs/g/g2749.md). And [Iēsous](../../strongs/g/g2424.md) [airō](../../strongs/g/g142.md) [anō](../../strongs/g/g507.md) [ophthalmos](../../strongs/g/g3788.md), and [eipon](../../strongs/g/g2036.md), **[patēr](../../strongs/g/g3962.md), I [eucharisteō](../../strongs/g/g2168.md) thee that thou hast [akouō](../../strongs/g/g191.md) me.**

<a name="john_11_42"></a>John 11:42

**And I [eidō](../../strongs/g/g1492.md) that thou [akouō](../../strongs/g/g191.md) me [pantote](../../strongs/g/g3842.md): but because of the [ochlos](../../strongs/g/g3793.md) which [periistēmi](../../strongs/g/g4026.md) I [eipon](../../strongs/g/g2036.md) it, that they may [pisteuō](../../strongs/g/g4100.md) that thou hast [apostellō](../../strongs/g/g649.md) me.**

<a name="john_11_43"></a>John 11:43

And when he thus had [eipon](../../strongs/g/g2036.md), he [kraugazō](../../strongs/g/g2905.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), **[Lazaros](../../strongs/g/g2976.md), [deuro](../../strongs/g/g1204.md) [exō](../../strongs/g/g1854.md).**

<a name="john_11_44"></a>John 11:44

And he that was [thnēskō](../../strongs/g/g2348.md) [exerchomai](../../strongs/g/g1831.md), [deō](../../strongs/g/g1210.md) [cheir](../../strongs/g/g5495.md) and [pous](../../strongs/g/g4228.md) with [keiria](../../strongs/g/g2750.md): and his [opsis](../../strongs/g/g3799.md) was [perideō](../../strongs/g/g4019.md) with a [soudarion](../../strongs/g/g4676.md). [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, **[lyō](../../strongs/g/g3089.md) him, and [aphiēmi](../../strongs/g/g863.md) him [hypagō](../../strongs/g/g5217.md).**

<a name="john_11_45"></a>John 11:45

Then [polys](../../strongs/g/g4183.md) of the [Ioudaios](../../strongs/g/g2453.md) which [erchomai](../../strongs/g/g2064.md) to [Maria](../../strongs/g/g3137.md), and had [theaomai](../../strongs/g/g2300.md) the things which [Iēsous](../../strongs/g/g2424.md) [poieō](../../strongs/g/g4160.md), [pisteuō](../../strongs/g/g4100.md) on him.

<a name="john_11_46"></a>John 11:46

But some of them [aperchomai](../../strongs/g/g565.md) to the [Pharisaios](../../strongs/g/g5330.md), and [eipon](../../strongs/g/g2036.md) them what things [Iēsous](../../strongs/g/g2424.md) had [poieō](../../strongs/g/g4160.md).

<a name="john_11_47"></a>John 11:47

Then [synagō](../../strongs/g/g4863.md) the [archiereus](../../strongs/g/g749.md) and the [Pharisaios](../../strongs/g/g5330.md) a [synedrion](../../strongs/g/g4892.md), and [legō](../../strongs/g/g3004.md), What [poieō](../../strongs/g/g4160.md) we? for this [anthrōpos](../../strongs/g/g444.md) [poieō](../../strongs/g/g4160.md) [polys](../../strongs/g/g4183.md) [sēmeion](../../strongs/g/g4592.md).

<a name="john_11_48"></a>John 11:48

If we [aphiēmi](../../strongs/g/g863.md) him thus, all will [pisteuō](../../strongs/g/g4100.md) on him: and the [Rhōmaios](../../strongs/g/g4514.md) shall [erchomai](../../strongs/g/g2064.md) and [airō](../../strongs/g/g142.md) both our [topos](../../strongs/g/g5117.md) and [ethnos](../../strongs/g/g1484.md).

<a name="john_11_49"></a>John 11:49

And one of them, [Kaïaphas](../../strongs/g/g2533.md), being the [archiereus](../../strongs/g/g749.md) that same [eniautos](../../strongs/g/g1763.md), [eipon](../../strongs/g/g2036.md) unto them, Ye [eidō](../../strongs/g/g1492.md) nothing at all,

<a name="john_11_50"></a>John 11:50

Nor [dialogizomai](../../strongs/g/g1260.md) that it is [sympherō](../../strongs/g/g4851.md) for us, that one [anthrōpos](../../strongs/g/g444.md) should [apothnēskō](../../strongs/g/g599.md) for the [laos](../../strongs/g/g2992.md), and that the [holos](../../strongs/g/g3650.md) [ethnos](../../strongs/g/g1484.md) [apollymi](../../strongs/g/g622.md) not.

<a name="john_11_51"></a>John 11:51

And this [eipon](../../strongs/g/g2036.md) he not of himself: but being [archiereus](../../strongs/g/g749.md) that [eniautos](../../strongs/g/g1763.md), he [prophēteuō](../../strongs/g/g4395.md) that [Iēsous](../../strongs/g/g2424.md) should [apothnēskō](../../strongs/g/g599.md) for that [ethnos](../../strongs/g/g1484.md);

<a name="john_11_52"></a>John 11:52

And not for that [ethnos](../../strongs/g/g1484.md) only, but that also he should [synagō](../../strongs/g/g4863.md) in one the [teknon](../../strongs/g/g5043.md) of [theos](../../strongs/g/g2316.md) that were [diaskorpizō](../../strongs/g/g1287.md).

<a name="john_11_53"></a>John 11:53

Then from that [hēmera](../../strongs/g/g2250.md) forth they [symbouleuō](../../strongs/g/g4823.md) for to [apokteinō](../../strongs/g/g615.md) him.

<a name="john_11_54"></a>John 11:54

[Iēsous](../../strongs/g/g2424.md) therefore [peripateō](../../strongs/g/g4043.md) no more [parrēsia](../../strongs/g/g3954.md) among the [Ioudaios](../../strongs/g/g2453.md); but [aperchomai](../../strongs/g/g565.md) thence unto a [chōra](../../strongs/g/g5561.md) [eggys](../../strongs/g/g1451.md) to the [erēmos](../../strongs/g/g2048.md), into a [polis](../../strongs/g/g4172.md) [legō](../../strongs/g/g3004.md) [Ephraim](../../strongs/g/g2187.md), and there [diatribō](../../strongs/g/g1304.md) with his [mathētēs](../../strongs/g/g3101.md).

<a name="john_11_55"></a>John 11:55

And the [Ioudaios](../../strongs/g/g2453.md)' [pascha](../../strongs/g/g3957.md) was [eggys](../../strongs/g/g1451.md): and [polys](../../strongs/g/g4183.md) [anabainō](../../strongs/g/g305.md) out of the [chōra](../../strongs/g/g5561.md) up to [Hierosolyma](../../strongs/g/g2414.md) before the [pascha](../../strongs/g/g3957.md), to [hagnizō](../../strongs/g/g48.md) themselves.

<a name="john_11_56"></a>John 11:56

Then [zēteō](../../strongs/g/g2212.md) they for [Iēsous](../../strongs/g/g2424.md), and [legō](../../strongs/g/g3004.md) among [allēlōn](../../strongs/g/g240.md), as they [histēmi](../../strongs/g/g2476.md) in the [hieron](../../strongs/g/g2411.md), What [dokeō](../../strongs/g/g1380.md) ye, that he will not [erchomai](../../strongs/g/g2064.md) to the [heortē](../../strongs/g/g1859.md)?

<a name="john_11_57"></a>John 11:57

Now both the [archiereus](../../strongs/g/g749.md) and the [Pharisaios](../../strongs/g/g5330.md) had [didōmi](../../strongs/g/g1325.md) an [entolē](../../strongs/g/g1785.md), that, if [tis](../../strongs/g/g5100.md) [ginōskō](../../strongs/g/g1097.md) where he were, he should [mēnyō](../../strongs/g/g3377.md) it, that they might [piazō](../../strongs/g/g4084.md) him.

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 10](john_10.md) - [John 12](john_12.md)