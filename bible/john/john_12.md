# [John 12](https://www.blueletterbible.org/kjv/jhn/12/1/p0/rl1/s_1005001)

<a name="john_12_1"></a>John 12:1

Then [Iēsous](../../strongs/g/g2424.md) six [hēmera](../../strongs/g/g2250.md) before the [pascha](../../strongs/g/g3957.md) [erchomai](../../strongs/g/g2064.md) to [Bēthania](../../strongs/g/g963.md), where [Lazaros](../../strongs/g/g2976.md) was, which had been [thnēskō](../../strongs/g/g2348.md), whom he [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md).

<a name="john_12_2"></a>John 12:2

There they [poieō](../../strongs/g/g4160.md) him a [deipnon](../../strongs/g/g1173.md); and [Martha](../../strongs/g/g3136.md) [diakoneō](../../strongs/g/g1247.md): but [Lazaros](../../strongs/g/g2976.md) was one of them that [synanakeimai](../../strongs/g/g4873.md) with him.

<a name="john_12_3"></a>John 12:3

Then [lambanō](../../strongs/g/g2983.md) [Maria](../../strongs/g/g3137.md) a [litra](../../strongs/g/g3046.md) of [myron](../../strongs/g/g3464.md) of [nardos](../../strongs/g/g3487.md) [pistikos](../../strongs/g/g4101.md), [polytimos](../../strongs/g/g4186.md), and [aleiphō](../../strongs/g/g218.md) the [pous](../../strongs/g/g4228.md) of [Iēsous](../../strongs/g/g2424.md), and [ekmassō](../../strongs/g/g1591.md) his [pous](../../strongs/g/g4228.md) with her [thrix](../../strongs/g/g2359.md): and the [oikia](../../strongs/g/g3614.md) was [plēroō](../../strongs/g/g4137.md) with the [osmē](../../strongs/g/g3744.md) of the [myron](../../strongs/g/g3464.md).

<a name="john_12_4"></a>John 12:4

Then [legō](../../strongs/g/g3004.md) one of his [mathētēs](../../strongs/g/g3101.md), [Ioudas](../../strongs/g/g2455.md) [Iskariōth](../../strongs/g/g2469.md), [Simōn](../../strongs/g/g4613.md), which should [paradidōmi](../../strongs/g/g3860.md) him,

<a name="john_12_5"></a>John 12:5

Why was not this [myron](../../strongs/g/g3464.md) [pipraskō](../../strongs/g/g4097.md) for three hundred [dēnarion](../../strongs/g/g1220.md), and [didōmi](../../strongs/g/g1325.md) to the [ptōchos](../../strongs/g/g4434.md)?

<a name="john_12_6"></a>John 12:6

This he [eipon](../../strongs/g/g2036.md), not that he [melei](../../strongs/g/g3199.md) for the [ptōchos](../../strongs/g/g4434.md); but because he was a [kleptēs](../../strongs/g/g2812.md), and had the [glōssokomon](../../strongs/g/g1101.md), and [bastazō](../../strongs/g/g941.md) what was [ballō](../../strongs/g/g906.md).

<a name="john_12_7"></a>John 12:7

Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md), **[aphiēmi](../../strongs/g/g863.md) her: against the [hēmera](../../strongs/g/g2250.md) of my [entaphiasmos](../../strongs/g/g1780.md) hath she [tēreō](../../strongs/g/g5083.md) this.**

<a name="john_12_8"></a>John 12:8

**For the [ptōchos](../../strongs/g/g4434.md) [pantote](../../strongs/g/g3842.md) ye have with you; but me ye have not [pantote](../../strongs/g/g3842.md).**

<a name="john_12_9"></a>John 12:9

[polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) of the [Ioudaios](../../strongs/g/g2453.md) therefore [ginōskō](../../strongs/g/g1097.md) that he was there: and they [erchomai](../../strongs/g/g2064.md) not for [Iēsous](../../strongs/g/g2424.md)' sake only, but that they might [eidō](../../strongs/g/g1492.md) [Lazaros](../../strongs/g/g2976.md) also, whom he had [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md).

<a name="john_12_10"></a>John 12:10

But the [archiereus](../../strongs/g/g749.md) [bouleuō](../../strongs/g/g1011.md) that they might [apokteinō](../../strongs/g/g615.md) [Lazaros](../../strongs/g/g2976.md) also;

<a name="john_12_11"></a>John 12:11

Because that by reason of him [polys](../../strongs/g/g4183.md) of the [Ioudaios](../../strongs/g/g2453.md) [hypagō](../../strongs/g/g5217.md), and [pisteuō](../../strongs/g/g4100.md) on [Iēsous](../../strongs/g/g2424.md).

<a name="john_12_12"></a>John 12:12

On the [epaurion](../../strongs/g/g1887.md) [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) that were [erchomai](../../strongs/g/g2064.md) to the [heortē](../../strongs/g/g1859.md), when they [akouō](../../strongs/g/g191.md) that [Iēsous](../../strongs/g/g2424.md) was [erchomai](../../strongs/g/g2064.md) to [Hierosolyma](../../strongs/g/g2414.md),

<a name="john_12_13"></a>John 12:13

[lambanō](../../strongs/g/g2983.md) [baion](../../strongs/g/g902.md) of [phoinix/phoinix](../../strongs/g/g5404.md), and [exerchomai](../../strongs/g/g1831.md) to [hypantēsis](../../strongs/g/g5222.md) him, and [krazō](../../strongs/g/g2896.md), [hōsanna](../../strongs/g/g5614.md): [eulogeō](../../strongs/g/g2127.md) is the [basileus](../../strongs/g/g935.md) of [Israēl](../../strongs/g/g2474.md) that [erchomai](../../strongs/g/g2064.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md).

<a name="john_12_14"></a>John 12:14

And [Iēsous](../../strongs/g/g2424.md), when he had [heuriskō](../../strongs/g/g2147.md) a [onarion](../../strongs/g/g3678.md), [kathizō](../../strongs/g/g2523.md) thereon; as it is [graphō](../../strongs/g/g1125.md),

<a name="john_12_15"></a>John 12:15

[phobeō](../../strongs/g/g5399.md) not, [thygatēr](../../strongs/g/g2364.md) of [Siōn](../../strongs/g/g4622.md): [idou](../../strongs/g/g2400.md), thy [basileus](../../strongs/g/g935.md) [erchomai](../../strongs/g/g2064.md), [kathēmai](../../strongs/g/g2521.md) on an [onos](../../strongs/g/g3688.md) [pōlos](../../strongs/g/g4454.md).

<a name="john_12_16"></a>John 12:16

These things [ginōskō](../../strongs/g/g1097.md) not his [mathētēs](../../strongs/g/g3101.md) at the first: but when [Iēsous](../../strongs/g/g2424.md) was [doxazō](../../strongs/g/g1392.md), then [mnaomai](../../strongs/g/g3415.md) they that these things were [graphō](../../strongs/g/g1125.md) of him, and that they had [poieō](../../strongs/g/g4160.md) these things unto him.

<a name="john_12_17"></a>John 12:17

The [ochlos](../../strongs/g/g3793.md) therefore that was with him when he [phōneō](../../strongs/g/g5455.md) [Lazaros](../../strongs/g/g2976.md) out of his [mnēmeion](../../strongs/g/g3419.md), and [egeirō](../../strongs/g/g1453.md) him from the [nekros](../../strongs/g/g3498.md), [martyreō](../../strongs/g/g3140.md).

<a name="john_12_18"></a>John 12:18

For this cause the [ochlos](../../strongs/g/g3793.md) also [hypantaō](../../strongs/g/g5221.md) him, for that they [akouō](../../strongs/g/g191.md) that he had [poieō](../../strongs/g/g4160.md) this [sēmeion](../../strongs/g/g4592.md).

<a name="john_12_19"></a>John 12:19

The [Pharisaios](../../strongs/g/g5330.md) therefore [eipon](../../strongs/g/g2036.md) among themselves, [theōreō](../../strongs/g/g2334.md) ye how ye [ōpheleō](../../strongs/g/g5623.md) nothing? [ide](../../strongs/g/g2396.md), the [kosmos](../../strongs/g/g2889.md) is [aperchomai](../../strongs/g/g565.md) after him.

<a name="john_12_20"></a>John 12:20

And there were certain [Hellēn](../../strongs/g/g1672.md) among them that [anabainō](../../strongs/g/g305.md) to [proskyneō](../../strongs/g/g4352.md) at the [heortē](../../strongs/g/g1859.md):

<a name="john_12_21"></a>John 12:21

The same [proserchomai](../../strongs/g/g4334.md) therefore to [Philippos](../../strongs/g/g5376.md), which was of [Bēthsaïda](../../strongs/g/g966.md) of [Galilaia](../../strongs/g/g1056.md), and [erōtaō](../../strongs/g/g2065.md) him, [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), we [thelō](../../strongs/g/g2309.md) [eidō](../../strongs/g/g1492.md) [Iēsous](../../strongs/g/g2424.md).

<a name="john_12_22"></a>John 12:22

[Philippos](../../strongs/g/g5376.md) [erchomai](../../strongs/g/g2064.md) and [legō](../../strongs/g/g3004.md) [Andreas](../../strongs/g/g406.md): and again [Andreas](../../strongs/g/g406.md) and [Philippos](../../strongs/g/g5376.md) [legō](../../strongs/g/g3004.md) [Iēsous](../../strongs/g/g2424.md).

<a name="john_12_23"></a>John 12:23

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) them, [legō](../../strongs/g/g3004.md), **The [hōra](../../strongs/g/g5610.md) is [erchomai](../../strongs/g/g2064.md), that the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) should be [doxazō](../../strongs/g/g1392.md).**

<a name="john_12_24"></a>John 12:24

**[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, Except a [kokkos](../../strongs/g/g2848.md) of [sitos](../../strongs/g/g4621.md) [piptō](../../strongs/g/g4098.md) into the [gē](../../strongs/g/g1093.md) and [apothnēskō](../../strongs/g/g599.md), it [menō](../../strongs/g/g3306.md) alone: but if it [apothnēskō](../../strongs/g/g599.md), it [pherō](../../strongs/g/g5342.md) [polys](../../strongs/g/g4183.md) [karpos](../../strongs/g/g2590.md).**

<a name="john_12_25"></a>John 12:25

**He that [phileō](../../strongs/g/g5368.md) his [psychē](../../strongs/g/g5590.md) shall [apollymi](../../strongs/g/g622.md) it; and he that [miseō](../../strongs/g/g3404.md) his [psychē](../../strongs/g/g5590.md) in this [kosmos](../../strongs/g/g2889.md) shall [phylassō](../../strongs/g/g5442.md) it unto [zōē](../../strongs/g/g2222.md) [aiōnios](../../strongs/g/g166.md).**

<a name="john_12_26"></a>John 12:26

**If any man [diakoneō](../../strongs/g/g1247.md) me, let him [akoloutheō](../../strongs/g/g190.md) me; and where I am, there shall also my [diakonos](../../strongs/g/g1249.md) be: if [tis](../../strongs/g/g5100.md) [diakoneō](../../strongs/g/g1247.md) me, him will my [patēr](../../strongs/g/g3962.md) [timaō](../../strongs/g/g5091.md).**

<a name="john_12_27"></a>John 12:27

**Now is my [psychē](../../strongs/g/g5590.md) [tarassō](../../strongs/g/g5015.md); and what shall I [eipon](../../strongs/g/g2036.md)? [patēr](../../strongs/g/g3962.md), [sōzō](../../strongs/g/g4982.md) me from this [hōra](../../strongs/g/g5610.md): but for this cause [erchomai](../../strongs/g/g2064.md) I unto this [hōra](../../strongs/g/g5610.md).**

<a name="john_12_28"></a>John 12:28

**[patēr](../../strongs/g/g3962.md), [doxazō](../../strongs/g/g1392.md) thy [onoma](../../strongs/g/g3686.md).** Then [erchomai](../../strongs/g/g2064.md) there a [phōnē](../../strongs/g/g5456.md) from [ouranos](../../strongs/g/g3772.md), saying, I have both [doxazō](../../strongs/g/g1392.md) it, and will [doxazō](../../strongs/g/g1392.md) it again.

<a name="john_12_29"></a>John 12:29

The [ochlos](../../strongs/g/g3793.md) therefore, that [histēmi](../../strongs/g/g2476.md) by, and [akouō](../../strongs/g/g191.md) it, [legō](../../strongs/g/g3004.md) that it [ginomai](../../strongs/g/g1096.md) [brontē](../../strongs/g/g1027.md): others [legō](../../strongs/g/g3004.md), An [aggelos](../../strongs/g/g32.md) [laleō](../../strongs/g/g2980.md) to him.

<a name="john_12_30"></a>John 12:30

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), **This [phōnē](../../strongs/g/g5456.md) [ginomai](../../strongs/g/g1096.md) not because of me, but for your sakes.**

<a name="john_12_31"></a>John 12:31

**Now is the [krisis](../../strongs/g/g2920.md) of this [kosmos](../../strongs/g/g2889.md): now shall the [archōn](../../strongs/g/g758.md) of this [kosmos](../../strongs/g/g2889.md) be [ekballō](../../strongs/g/g1544.md) out.**

<a name="john_12_32"></a>John 12:32

**And I, if I be [hypsoō](../../strongs/g/g5312.md) from [gē](../../strongs/g/g1093.md), will [helkō](../../strongs/g/g1670.md) [pas](../../strongs/g/g3956.md) unto me.**

<a name="john_12_33"></a>John 12:33

This he [legō](../../strongs/g/g3004.md), [sēmainō](../../strongs/g/g4591.md) what [thanatos](../../strongs/g/g2288.md) he should [apothnēskō](../../strongs/g/g599.md).

<a name="john_12_34"></a>John 12:34

The [ochlos](../../strongs/g/g3793.md) [apokrinomai](../../strongs/g/g611.md) him, We have [akouō](../../strongs/g/g191.md) out of the [nomos](../../strongs/g/g3551.md) that [Christos](../../strongs/g/g5547.md) [menō](../../strongs/g/g3306.md) for [aiōn](../../strongs/g/g165.md): and how [legō](../../strongs/g/g3004.md) thou, The [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) must be [hypsoō](../../strongs/g/g5312.md)? who is this [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md)?

<a name="john_12_35"></a>John 12:35

Then [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **Yet a [mikros](../../strongs/g/g3398.md) [chronos](../../strongs/g/g5550.md) is the [phōs](../../strongs/g/g5457.md) with you. [peripateō](../../strongs/g/g4043.md) while ye have the [phōs](../../strongs/g/g5457.md), lest [skotia](../../strongs/g/g4653.md) [katalambanō](../../strongs/g/g2638.md) you: for he that [peripateō](../../strongs/g/g4043.md) in [skotia](../../strongs/g/g4653.md) [eidō](../../strongs/g/g1492.md) not whither he [hypagō](../../strongs/g/g5217.md).**

<a name="john_12_36"></a>John 12:36

**While ye have [phōs](../../strongs/g/g5457.md), [pisteuō](../../strongs/g/g4100.md) in the [phōs](../../strongs/g/g5457.md), that ye may be the [huios](../../strongs/g/g5207.md) of [phōs](../../strongs/g/g5457.md).** These things [laleō](../../strongs/g/g2980.md) [Iēsous](../../strongs/g/g2424.md), and [aperchomai](../../strongs/g/g565.md), and did [kryptō](../../strongs/g/g2928.md) from them.

<a name="john_12_37"></a>John 12:37

But though he had [poieō](../../strongs/g/g4160.md) [tosoutos](../../strongs/g/g5118.md) [sēmeion](../../strongs/g/g4592.md) before them, yet they [pisteuō](../../strongs/g/g4100.md) not on him:

<a name="john_12_38"></a>John 12:38

That the [logos](../../strongs/g/g3056.md) of [Ēsaïas](../../strongs/g/g2268.md) the [prophētēs](../../strongs/g/g4396.md) might be [plēroō](../../strongs/g/g4137.md), which he [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), who hath [pisteuō](../../strongs/g/g4100.md) our [akoē](../../strongs/g/g189.md)? and to whom hath the [brachiōn](../../strongs/g/g1023.md) of the [kyrios](../../strongs/g/g2962.md) been [apokalyptō](../../strongs/g/g601.md)?

<a name="john_12_39"></a>John 12:39

Therefore they could not [pisteuō](../../strongs/g/g4100.md), because that [Ēsaïas](../../strongs/g/g2268.md) [eipon](../../strongs/g/g2036.md) again,

<a name="john_12_40"></a>John 12:40

He hath [typhloō](../../strongs/g/g5186.md) their [ophthalmos](../../strongs/g/g3788.md), and [pōroō](../../strongs/g/g4456.md) their [kardia](../../strongs/g/g2588.md); that they should not [eidō](../../strongs/g/g1492.md) with their [ophthalmos](../../strongs/g/g3788.md), nor [noeō](../../strongs/g/g3539.md) with their [kardia](../../strongs/g/g2588.md), and be [epistrephō](../../strongs/g/g1994.md), and I should [iaomai](../../strongs/g/g2390.md) them.

<a name="john_12_41"></a>John 12:41

These things [eipon](../../strongs/g/g2036.md) [Ēsaïas](../../strongs/g/g2268.md), when he [eidō](../../strongs/g/g1492.md) his [doxa](../../strongs/g/g1391.md), and [laleō](../../strongs/g/g2980.md) of him.

<a name="john_12_42"></a>John 12:42

Nevertheless among the [archōn](../../strongs/g/g758.md) also [polys](../../strongs/g/g4183.md) [pisteuō](../../strongs/g/g4100.md) on him; but because of the [Pharisaios](../../strongs/g/g5330.md) they did not [homologeō](../../strongs/g/g3670.md) him, lest they should be [aposynagōgos](../../strongs/g/g656.md):

<a name="john_12_43"></a>John 12:43

For they [agapaō](../../strongs/g/g25.md) the [doxa](../../strongs/g/g1391.md) of [anthrōpos](../../strongs/g/g444.md) more than the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md).

<a name="john_12_44"></a>John 12:44

[Iēsous](../../strongs/g/g2424.md) [krazō](../../strongs/g/g2896.md) and [eipon](../../strongs/g/g2036.md), **He that [pisteuō](../../strongs/g/g4100.md) on me, [pisteuō](../../strongs/g/g4100.md) not on me, but on him that [pempō](../../strongs/g/g3992.md) me.**

<a name="john_12_45"></a>John 12:45

**And he that [theōreō](../../strongs/g/g2334.md) me [theōreō](../../strongs/g/g2334.md) him that [pempō](../../strongs/g/g3992.md) me.**

<a name="john_12_46"></a>John 12:46

**I am [erchomai](../../strongs/g/g2064.md) a [phōs](../../strongs/g/g5457.md) into the [kosmos](../../strongs/g/g2889.md), that whosoever [pisteuō](../../strongs/g/g4100.md) on me should not [menō](../../strongs/g/g3306.md) in [skotia](../../strongs/g/g4653.md).**

<a name="john_12_47"></a>John 12:47

**And if [tis](../../strongs/g/g5100.md) [akouō](../../strongs/g/g191.md) my [rhēma](../../strongs/g/g4487.md), and [pisteuō](../../strongs/g/g4100.md) not, I [krinō](../../strongs/g/g2919.md) him not: for I [erchomai](../../strongs/g/g2064.md) not to [krinō](../../strongs/g/g2919.md) the [kosmos](../../strongs/g/g2889.md), but to [sōzō](../../strongs/g/g4982.md) the [kosmos](../../strongs/g/g2889.md).**

<a name="john_12_48"></a>John 12:48

**He that [atheteō](../../strongs/g/g114.md) me, and [lambanō](../../strongs/g/g2983.md) not my [rhēma](../../strongs/g/g4487.md), hath one that [krinō](../../strongs/g/g2919.md) him: the [logos](../../strongs/g/g3056.md) that I have [laleō](../../strongs/g/g2980.md), the same shall [krinō](../../strongs/g/g2919.md) him in the [eschatos](../../strongs/g/g2078.md) [hēmera](../../strongs/g/g2250.md).**

<a name="john_12_49"></a>John 12:49

**For I have not [laleō](../../strongs/g/g2980.md) of myself; but the [patēr](../../strongs/g/g3962.md) which [pempō](../../strongs/g/g3992.md) me, he [didōmi](../../strongs/g/g1325.md) me an [entolē](../../strongs/g/g1785.md), what I should [eipon](../../strongs/g/g2036.md), and what I should [laleō](../../strongs/g/g2980.md).**

<a name="john_12_50"></a>John 12:50

**And I [eidō](../../strongs/g/g1492.md) that his [entolē](../../strongs/g/g1785.md) is [zōē](../../strongs/g/g2222.md) [aiōnios](../../strongs/g/g166.md): whatsoever I [laleō](../../strongs/g/g2980.md) therefore, even as the [patēr](../../strongs/g/g3962.md) [eipon](../../strongs/g/g2046.md) unto me, so I [laleō](../../strongs/g/g2980.md).**

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 11](john_11.md) - [John 13](john_13.md)