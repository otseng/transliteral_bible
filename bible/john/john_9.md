# [John 9](https://www.blueletterbible.org/kjv/jhn/9/1/p0/rl1/s_1005001)

<a name="john_9_1"></a>John 9:1

And [paragō](../../strongs/g/g3855.md), he [eidō](../../strongs/g/g1492.md) an [anthrōpos](../../strongs/g/g444.md) which was [typhlos](../../strongs/g/g5185.md) from [genetē](../../strongs/g/g1079.md).

<a name="john_9_2"></a>John 9:2

And his [mathētēs](../../strongs/g/g3101.md) [erōtaō](../../strongs/g/g2065.md) him, [legō](../../strongs/g/g3004.md), [rhabbi](../../strongs/g/g4461.md), who did [hamartanō](../../strongs/g/g264.md), [houtos](../../strongs/g/g3778.md), or his [goneus](../../strongs/g/g1118.md), that he was [gennaō](../../strongs/g/g1080.md) [typhlos](../../strongs/g/g5185.md)?

<a name="john_9_3"></a>John 9:3

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md), **Neither hath [houtos](../../strongs/g/g3778.md) [hamartanō](../../strongs/g/g264.md), nor his [goneus](../../strongs/g/g1118.md): but that the [ergon](../../strongs/g/g2041.md) of [theos](../../strongs/g/g2316.md) should be [phaneroō](../../strongs/g/g5319.md) in him.**

<a name="john_9_4"></a>John 9:4

**I must [ergazomai](../../strongs/g/g2038.md) the [ergon](../../strongs/g/g2041.md) of him that [pempō](../../strongs/g/g3992.md) me, while it is [hēmera](../../strongs/g/g2250.md): the [nyx](../../strongs/g/g3571.md) [erchomai](../../strongs/g/g2064.md), when [oudeis](../../strongs/g/g3762.md) can [ergazomai](../../strongs/g/g2038.md).**

<a name="john_9_5"></a>John 9:5

**As long as I am in the [kosmos](../../strongs/g/g2889.md), I am the [phōs](../../strongs/g/g5457.md) of the [kosmos](../../strongs/g/g2889.md).**

<a name="john_9_6"></a>John 9:6

When he had thus [eipon](../../strongs/g/g2036.md), he [ptyō](../../strongs/g/g4429.md) [chamai](../../strongs/g/g5476.md), and [poieō](../../strongs/g/g4160.md) [pēlos](../../strongs/g/g4081.md) of the [ptysma](../../strongs/g/g4427.md), and he [epichriō](../../strongs/g/g2025.md) the [ophthalmos](../../strongs/g/g3788.md) of the [typhlos](../../strongs/g/g5185.md) with the [pēlos](../../strongs/g/g4081.md),

<a name="john_9_7"></a>John 9:7

And [eipon](../../strongs/g/g2036.md) unto him, **[hypagō](../../strongs/g/g5217.md), [niptō](../../strongs/g/g3538.md) in the [kolymbēthra](../../strongs/g/g2861.md) of [Silōam](../../strongs/g/g4611.md)**, (which is [hermēneuō](../../strongs/g/g2059.md), [apostellō](../../strongs/g/g649.md).) He [aperchomai](../../strongs/g/g565.md) therefore, and [niptō](../../strongs/g/g3538.md), and [erchomai](../../strongs/g/g2064.md) [blepō](../../strongs/g/g991.md).

<a name="john_9_8"></a>John 9:8

The [geitōn](../../strongs/g/g1069.md) therefore, and they which [proteros](../../strongs/g/g4386.md) had [theōreō](../../strongs/g/g2334.md) him that he was [typhlos](../../strongs/g/g5185.md), [legō](../../strongs/g/g3004.md), Is not this he that [kathēmai](../../strongs/g/g2521.md) and [prosaiteō](../../strongs/g/g4319.md)?

<a name="john_9_9"></a>John 9:9

Some [legō](../../strongs/g/g3004.md), This is he: others, He is [homoios](../../strongs/g/g3664.md) him: but he [legō](../../strongs/g/g3004.md), I am he.

<a name="john_9_10"></a>John 9:10

Therefore [legō](../../strongs/g/g3004.md) they unto him, How were thine [ophthalmos](../../strongs/g/g3788.md) [anoigō](../../strongs/g/g455.md)?

<a name="john_9_11"></a>John 9:11

He [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), an [anthrōpos](../../strongs/g/g444.md) that is [legō](../../strongs/g/g3004.md) [Iēsous](../../strongs/g/g2424.md) [poieō](../../strongs/g/g4160.md) [pēlos](../../strongs/g/g4081.md), and [epichriō](../../strongs/g/g2025.md) mine [ophthalmos](../../strongs/g/g3788.md), and [eipon](../../strongs/g/g2036.md) unto me, **[hypagō](../../strongs/g/g5217.md) to the [kolymbēthra](../../strongs/g/g2861.md) of [Silōam](../../strongs/g/g4611.md), and [niptō](../../strongs/g/g3538.md)**: and I [aperchomai](../../strongs/g/g565.md) and [niptō](../../strongs/g/g3538.md), and I [anablepō](../../strongs/g/g308.md).

<a name="john_9_12"></a>John 9:12

Then [eipon](../../strongs/g/g2036.md) they unto him, Where is he? He [legō](../../strongs/g/g3004.md), I [eidō](../../strongs/g/g1492.md) not.

<a name="john_9_13"></a>John 9:13

They [agō](../../strongs/g/g71.md) to the [Pharisaios](../../strongs/g/g5330.md) him that [pote](../../strongs/g/g4218.md) was [typhlos](../../strongs/g/g5185.md).

<a name="john_9_14"></a>John 9:14

And it was [sabbaton](../../strongs/g/g4521.md) when [Iēsous](../../strongs/g/g2424.md) [poieō](../../strongs/g/g4160.md) the [pēlos](../../strongs/g/g4081.md), and [anoigō](../../strongs/g/g455.md) his [ophthalmos](../../strongs/g/g3788.md).

<a name="john_9_15"></a>John 9:15

Then again the [Pharisaios](../../strongs/g/g5330.md) also [erōtaō](../../strongs/g/g2065.md) him how he had [anablepō](../../strongs/g/g308.md). He [eipon](../../strongs/g/g2036.md) unto them, He [epitithēmi](../../strongs/g/g2007.md) [pēlos](../../strongs/g/g4081.md) upon mine [ophthalmos](../../strongs/g/g3788.md), and I [niptō](../../strongs/g/g3538.md), and [blepō](../../strongs/g/g991.md).

<a name="john_9_16"></a>John 9:16

Therefore [legō](../../strongs/g/g3004.md) some of the [Pharisaios](../../strongs/g/g5330.md), This [anthrōpos](../../strongs/g/g444.md) is not of [theos](../../strongs/g/g2316.md), because he [tēreō](../../strongs/g/g5083.md) not [sabbaton](../../strongs/g/g4521.md). Others [legō](../../strongs/g/g3004.md), How can an [anthrōpos](../../strongs/g/g444.md) that is [hamartōlos](../../strongs/g/g268.md) [poieō](../../strongs/g/g4160.md) such [sēmeion](../../strongs/g/g4592.md)? And there was a [schisma](../../strongs/g/g4978.md) among them.

<a name="john_9_17"></a>John 9:17

They [legō](../../strongs/g/g3004.md) unto the [typhlos](../../strongs/g/g5185.md) again, What [legō](../../strongs/g/g3004.md) thou of him, that he hath [anoigō](../../strongs/g/g455.md) thine [ophthalmos](../../strongs/g/g3788.md)? He [eipon](../../strongs/g/g2036.md), He is a [prophētēs](../../strongs/g/g4396.md).

<a name="john_9_18"></a>John 9:18

But the [Ioudaios](../../strongs/g/g2453.md) did not [pisteuō](../../strongs/g/g4100.md) concerning him, that he had been [typhlos](../../strongs/g/g5185.md), and [anablepō](../../strongs/g/g308.md), until they [phōneō](../../strongs/g/g5455.md) the [goneus](../../strongs/g/g1118.md) of him that [anablepō](../../strongs/g/g308.md).

<a name="john_9_19"></a>John 9:19

And they [erōtaō](../../strongs/g/g2065.md) them, [legō](../../strongs/g/g3004.md), Is this your [huios](../../strongs/g/g5207.md)\, who ye [legō](../../strongs/g/g3004.md) was [gennaō](../../strongs/g/g1080.md) [typhlos](../../strongs/g/g5185.md)? how then doth he now [blepō](../../strongs/g/g991.md)?

<a name="john_9_20"></a>John 9:20

His [goneus](../../strongs/g/g1118.md) [apokrinomai](../../strongs/g/g611.md) them and [eipon](../../strongs/g/g2036.md), We [eidō](../../strongs/g/g1492.md) that this is our [huios](../../strongs/g/g5207.md), and that he was [gennaō](../../strongs/g/g1080.md) [typhlos](../../strongs/g/g5185.md):

<a name="john_9_21"></a>John 9:21

But by what means he now [blepō](../../strongs/g/g991.md), we [eidō](../../strongs/g/g1492.md) not; or who hath [anoigō](../../strongs/g/g455.md) his [ophthalmos](../../strongs/g/g3788.md), we [eidō](../../strongs/g/g1492.md) not: he is [hēlikia](../../strongs/g/g2244.md); [erōtaō](../../strongs/g/g2065.md) him: he shall [laleō](../../strongs/g/g2980.md) for himself.

<a name="john_9_22"></a>John 9:22

These [eipon](../../strongs/g/g2036.md) his [goneus](../../strongs/g/g1118.md), because they [phobeō](../../strongs/g/g5399.md) the [Ioudaios](../../strongs/g/g2453.md): for the [Ioudaios](../../strongs/g/g2453.md) had [syntithēmi](../../strongs/g/g4934.md) already, that if [tis](../../strongs/g/g5100.md) did [homologeō](../../strongs/g/g3670.md) that he was [Christos](../../strongs/g/g5547.md), he should be [aposynagōgos](../../strongs/g/g656.md).

<a name="john_9_23"></a>John 9:23

Therefore [eipon](../../strongs/g/g2036.md) his [goneus](../../strongs/g/g1118.md), He is [hēlikia](../../strongs/g/g2244.md); [erōtaō](../../strongs/g/g2065.md) him.

<a name="john_9_24"></a>John 9:24

Then again [phōneō](../../strongs/g/g5455.md) they the [anthrōpos](../../strongs/g/g444.md) that was [typhlos](../../strongs/g/g5185.md), and [eipon](../../strongs/g/g2036.md) unto him, [didōmi](../../strongs/g/g1325.md) [theos](../../strongs/g/g2316.md) [doxa](../../strongs/g/g1391.md): we [eidō](../../strongs/g/g1492.md) that this [anthrōpos](../../strongs/g/g444.md) is [hamartōlos](../../strongs/g/g268.md).

<a name="john_9_25"></a>John 9:25

He [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), Whether he be [hamartōlos](../../strongs/g/g268.md), I [eidō](../../strongs/g/g1492.md) not: one thing I [eidō](../../strongs/g/g1492.md), that, whereas I was [typhlos](../../strongs/g/g5185.md), now I [blepō](../../strongs/g/g991.md).

<a name="john_9_26"></a>John 9:26

Then [eipon](../../strongs/g/g2036.md) they to him again, What [poieō](../../strongs/g/g4160.md) he to thee? how [anoigō](../../strongs/g/g455.md) he thine [ophthalmos](../../strongs/g/g3788.md)?

<a name="john_9_27"></a>John 9:27

He [apokrinomai](../../strongs/g/g611.md) them, I have [eipon](../../strongs/g/g2036.md) you already, and ye did not [akouō](../../strongs/g/g191.md): wherefore [thelō](../../strongs/g/g2309.md) ye [akouō](../../strongs/g/g191.md) it again? [thelō](../../strongs/g/g2309.md) ye also be his [mathētēs](../../strongs/g/g3101.md)?

<a name="john_9_28"></a>John 9:28

Then they [loidoreō](../../strongs/g/g3058.md) him, and [eipon](../../strongs/g/g2036.md), Thou art his [mathētēs](../../strongs/g/g3101.md); but we are [Mōÿsēs](../../strongs/g/g3475.md) [mathētēs](../../strongs/g/g3101.md).

<a name="john_9_29"></a>John 9:29

We [eidō](../../strongs/g/g1492.md) that [theos](../../strongs/g/g2316.md) [laleō](../../strongs/g/g2980.md) unto [Mōÿsēs](../../strongs/g/g3475.md): as for this, we [eidō](../../strongs/g/g1492.md) not from whence he is.

<a name="john_9_30"></a>John 9:30

The [anthrōpos](../../strongs/g/g444.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, Why herein is a [thaumastos](../../strongs/g/g2298.md), that ye [eidō](../../strongs/g/g1492.md) not from whence he is, and yet he hath [anoigō](../../strongs/g/g455.md) mine [ophthalmos](../../strongs/g/g3788.md).

<a name="john_9_31"></a>John 9:31

Now we [eidō](../../strongs/g/g1492.md) that [theos](../../strongs/g/g2316.md) heareth not [hamartōlos](../../strongs/g/g268.md): but if any man be [theosebēs](../../strongs/g/g2318.md), and [poieō](../../strongs/g/g4160.md) his [thelēma](../../strongs/g/g2307.md), him he [akouō](../../strongs/g/g191.md).

<a name="john_9_32"></a>John 9:32

Since [aiōn](../../strongs/g/g165.md) was it not [akouō](../../strongs/g/g191.md) that [tis](../../strongs/g/g5100.md) [anoigō](../../strongs/g/g455.md) the [ophthalmos](../../strongs/g/g3788.md) of one that was [gennaō](../../strongs/g/g1080.md) [typhlos](../../strongs/g/g5185.md).

<a name="john_9_33"></a>John 9:33

If [houtos](../../strongs/g/g3778.md) were not of [theos](../../strongs/g/g2316.md), he could [poieō](../../strongs/g/g4160.md) [oudeis](../../strongs/g/g3762.md).

<a name="john_9_34"></a>John 9:34

They [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, Thou wast [holos](../../strongs/g/g3650.md) [gennaō](../../strongs/g/g1080.md) in [hamartia](../../strongs/g/g266.md), and dost thou [didaskō](../../strongs/g/g1321.md) us? And they [ekballō](../../strongs/g/g1544.md) him out.

<a name="john_9_35"></a>John 9:35

[Iēsous](../../strongs/g/g2424.md) [akouō](../../strongs/g/g191.md) that they had [ekballō](../../strongs/g/g1544.md) him out; and when he had [heuriskō](../../strongs/g/g2147.md) him, he [eipon](../../strongs/g/g2036.md) unto him, **Dost thou [pisteuō](../../strongs/g/g4100.md) on the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md)?**

<a name="john_9_36"></a>John 9:36

He [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), Who is he, [kyrios](../../strongs/g/g2962.md), that I might [pisteuō](../../strongs/g/g4100.md) on him?

<a name="john_9_37"></a>John 9:37

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **Thou hast both [horaō](../../strongs/g/g3708.md) him, and it is he that [laleō](../../strongs/g/g2980.md) with thee.**

<a name="john_9_38"></a>John 9:38

And he [phēmi](../../strongs/g/g5346.md), [kyrios](../../strongs/g/g2962.md), I [pisteuō](../../strongs/g/g4100.md). And he [proskyneō](../../strongs/g/g4352.md) him.

<a name="john_9_39"></a>John 9:39

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md), **For [krima](../../strongs/g/g2917.md) I am [erchomai](../../strongs/g/g2064.md) into this [kosmos](../../strongs/g/g2889.md), that they which [blepō](../../strongs/g/g991.md) not might [blepō](../../strongs/g/g991.md); and that they which [blepō](../../strongs/g/g991.md) might be made [typhlos](../../strongs/g/g5185.md).**

<a name="john_9_40"></a>John 9:40

And some of the [Pharisaios](../../strongs/g/g5330.md) which were with him [akouō](../../strongs/g/g191.md) [tauta](../../strongs/g/g5023.md), and [eipon](../../strongs/g/g2036.md) unto him, Are we [typhlos](../../strongs/g/g5185.md) also?

<a name="john_9_41"></a>John 9:41

[Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **If ye were [typhlos](../../strongs/g/g5185.md), ye should have no [hamartia](../../strongs/g/g266.md): but now ye [legō](../../strongs/g/g3004.md), We [blepō](../../strongs/g/g991.md); therefore your [hamartia](../../strongs/g/g266.md) [menō](../../strongs/g/g3306.md).**

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 8](john_8.md) - [John 10](john_10.md)