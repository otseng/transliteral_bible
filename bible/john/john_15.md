# [John 15](https://www.blueletterbible.org/kjv/jhn/15/1/s_1012001)

<a name="John_15_1"></a>John 15:1

**I am the [alēthinos](../../strongs/g/g228.md) [ampelos](../../strongs/g/g288.md), and my [patēr](../../strongs/g/g3962.md) is the [geōrgos](../../strongs/g/g1092.md).**

<a name="John_15_2"></a>John 15:2

**Every [klēma](../../strongs/g/g2814.md) in me that [pherō](../../strongs/g/g5342.md) not [karpos](../../strongs/g/g2590.md) he [airō](../../strongs/g/g142.md): and every that [pherō](../../strongs/g/g5342.md) [karpos](../../strongs/g/g2590.md), he [kathairō](../../strongs/g/g2508.md) it, that it may [pherō](../../strongs/g/g5342.md) [pleiōn](../../strongs/g/g4119.md) [karpos](../../strongs/g/g2590.md).**

<a name="John_15_3"></a>John 15:3

**Now ye are [katharos](../../strongs/g/g2513.md) through the [logos](../../strongs/g/g3056.md) which I have [laleō](../../strongs/g/g2980.md) unto you.**

<a name="John_15_4"></a>John 15:4

**[menō](../../strongs/g/g3306.md) in me, and I in you. As the [klēma](../../strongs/g/g2814.md) cannot [pherō](../../strongs/g/g5342.md) [karpos](../../strongs/g/g2590.md) of itself, except it [menō](../../strongs/g/g3306.md) in the [ampelos](../../strongs/g/g288.md); no more can ye, except ye [menō](../../strongs/g/g3306.md) in me.**

<a name="John_15_5"></a>John 15:5

**I am the [ampelos](../../strongs/g/g288.md), ye the [klēma](../../strongs/g/g2814.md): He that [menō](../../strongs/g/g3306.md) in me, and I in him, the same [pherō](../../strongs/g/g5342.md) [polys](../../strongs/g/g4183.md) [karpos](../../strongs/g/g2590.md): for without me ye can [poieō](../../strongs/g/g4160.md) [ou](../../strongs/g/g3756.md) [oudeis](../../strongs/g/g3762.md).**

<a name="John_15_6"></a>John 15:6

**If a man [menō](../../strongs/g/g3306.md) not in me, he is [ballō](../../strongs/g/g906.md) forth as a [klēma](../../strongs/g/g2814.md), and is [xērainō](../../strongs/g/g3583.md); and [synagō](../../strongs/g/g4863.md) them, and [ballō](../../strongs/g/g906.md) into the [pyr](../../strongs/g/g4442.md), and they are [kaiō](../../strongs/g/g2545.md).**

<a name="John_15_7"></a>John 15:7

**If ye [menō](../../strongs/g/g3306.md) in me, and my [rhēma](../../strongs/g/g4487.md) [menō](../../strongs/g/g3306.md) in you, ye shall [aiteō](../../strongs/g/g154.md) what ye [thelō](../../strongs/g/g2309.md), and it shall be [ginomai](../../strongs/g/g1096.md) unto you.**

<a name="John_15_8"></a>John 15:8

**Herein is my [patēr](../../strongs/g/g3962.md) [doxazō](../../strongs/g/g1392.md), that ye [pherō](../../strongs/g/g5342.md) [polys](../../strongs/g/g4183.md) [karpos](../../strongs/g/g2590.md); so shall ye be my [mathētēs](../../strongs/g/g3101.md).**

<a name="John_15_9"></a>John 15:9

**As the [patēr](../../strongs/g/g3962.md) hath [agapaō](../../strongs/g/g25.md) me, so have I [agapaō](../../strongs/g/g25.md) you: [menō](../../strongs/g/g3306.md) in my [agapē](../../strongs/g/g26.md).**

<a name="John_15_10"></a>John 15:10

**If ye [tēreō](../../strongs/g/g5083.md) my [entolē](../../strongs/g/g1785.md), ye shall [menō](../../strongs/g/g3306.md) in my [agapē](../../strongs/g/g26.md); even as I have [tēreō](../../strongs/g/g5083.md) my [patēr](../../strongs/g/g3962.md) [entolē](../../strongs/g/g1785.md), and [menō](../../strongs/g/g3306.md) in his [agapē](../../strongs/g/g26.md).**

<a name="John_15_11"></a>John 15:11

**These things have I [laleō](../../strongs/g/g2980.md) unto you, that my [chara](../../strongs/g/g5479.md) might [menō](../../strongs/g/g3306.md) in you, and your [chara](../../strongs/g/g5479.md) might be [plēroō](../../strongs/g/g4137.md).**

<a name="John_15_12"></a>John 15:12

**This is my [entolē](../../strongs/g/g1785.md), That ye [agapaō](../../strongs/g/g25.md) [allēlōn](../../strongs/g/g240.md), as I have [agapaō](../../strongs/g/g25.md) you.**

<a name="John_15_13"></a>John 15:13

**[meizōn](../../strongs/g/g3187.md) [agapē](../../strongs/g/g26.md) hath [oudeis](../../strongs/g/g3762.md) than this, that [tis](../../strongs/g/g5100.md) [tithēmi](../../strongs/g/g5087.md) his [psychē](../../strongs/g/g5590.md) for his [philos](../../strongs/g/g5384.md).**

<a name="John_15_14"></a>John 15:14

**Ye are my [philos](../../strongs/g/g5384.md), if ye [poieō](../../strongs/g/g4160.md) whatsoever I [entellō](../../strongs/g/g1781.md) you.**

<a name="John_15_15"></a>John 15:15

**Henceforth I [legō](../../strongs/g/g3004.md) you not [doulos](../../strongs/g/g1401.md); for the [doulos](../../strongs/g/g1401.md) [eidō](../../strongs/g/g1492.md) not what his [kyrios](../../strongs/g/g2962.md) [poieō](../../strongs/g/g4160.md): but I have [eipon](../../strongs/g/g2046.md) you [philos](../../strongs/g/g5384.md); for all things that I have [akouō](../../strongs/g/g191.md) of my [patēr](../../strongs/g/g3962.md) I have [gnōrizō](../../strongs/g/g1107.md) unto you.**

<a name="John_15_16"></a>John 15:16

**Ye have not [eklegomai](../../strongs/g/g1586.md) me, but I have [eklegomai](../../strongs/g/g1586.md) you, and [tithēmi](../../strongs/g/g5087.md) you, that ye should [hypagō](../../strongs/g/g5217.md) and [pherō](../../strongs/g/g5342.md) [karpos](../../strongs/g/g2590.md), and your [karpos](../../strongs/g/g2590.md) should [menō](../../strongs/g/g3306.md): that whatsoever ye shall [aiteō](../../strongs/g/g154.md) of the [patēr](../../strongs/g/g3962.md) in my [onoma](../../strongs/g/g3686.md), he may [didōmi](../../strongs/g/g1325.md) it you.**

<a name="John_15_17"></a>John 15:17

**These things I [entellō](../../strongs/g/g1781.md) you, that ye [agapaō](../../strongs/g/g25.md) [allēlōn](../../strongs/g/g240.md).**

<a name="John_15_18"></a>John 15:18

**If the [kosmos](../../strongs/g/g2889.md) [miseō](../../strongs/g/g3404.md) you, ye [ginōskō](../../strongs/g/g1097.md) that it [miseō](../../strongs/g/g3404.md) me before you.**

<a name="John_15_19"></a>John 15:19

**If ye were of the [kosmos](../../strongs/g/g2889.md), the [kosmos](../../strongs/g/g2889.md) would [phileō](../../strongs/g/g5368.md) his own: but because ye are not of the [kosmos](../../strongs/g/g2889.md), but I have [eklegomai](../../strongs/g/g1586.md) you out of the [kosmos](../../strongs/g/g2889.md), therefore the [kosmos](../../strongs/g/g2889.md) [miseō](../../strongs/g/g3404.md) you.**

<a name="John_15_20"></a>John 15:20

**[mnēmoneuō](../../strongs/g/g3421.md) the [logos](../../strongs/g/g3056.md) that I [eipon](../../strongs/g/g2036.md) unto you, The [doulos](../../strongs/g/g1401.md) is not [meizōn](../../strongs/g/g3187.md) than his [kyrios](../../strongs/g/g2962.md). If they have [diōkō](../../strongs/g/g1377.md) me, they will also [diōkō](../../strongs/g/g1377.md) you; if they have [tēreō](../../strongs/g/g5083.md) my [logos](../../strongs/g/g3056.md), they will [tēreō](../../strongs/g/g5083.md) yours also.**

<a name="John_15_21"></a>John 15:21

**But all these things will they [poieō](../../strongs/g/g4160.md) unto you for my [onoma](../../strongs/g/g3686.md) sake, because they [eidō](../../strongs/g/g1492.md) not him that [pempō](../../strongs/g/g3992.md) me.**

<a name="John_15_22"></a>John 15:22

**If I had not [erchomai](../../strongs/g/g2064.md) and [laleō](../../strongs/g/g2980.md) unto them, they had not had [hamartia](../../strongs/g/g266.md): but now they have no [prophasis](../../strongs/g/g4392.md) for their [hamartia](../../strongs/g/g266.md).**

<a name="John_15_23"></a>John 15:23

**He that [miseō](../../strongs/g/g3404.md) me [miseō](../../strongs/g/g3404.md) my [patēr](../../strongs/g/g3962.md) also.**

<a name="John_15_24"></a>John 15:24

**If I had not [poieō](../../strongs/g/g4160.md) among them the [ergon](../../strongs/g/g2041.md) which none [allos](../../strongs/g/g243.md) [poieō](../../strongs/g/g4160.md), they had not had [hamartia](../../strongs/g/g266.md): but now have they both [horaō](../../strongs/g/g3708.md) and [miseō](../../strongs/g/g3404.md) both me and my [patēr](../../strongs/g/g3962.md).**

<a name="John_15_25"></a>John 15:25

**But, that the [logos](../../strongs/g/g3056.md) might be [plēroō](../../strongs/g/g4137.md) that is [graphō](../../strongs/g/g1125.md) in their [nomos](../../strongs/g/g3551.md), They [miseō](../../strongs/g/g3404.md) me [dōrean](../../strongs/g/g1432.md).**

<a name="John_15_26"></a>John 15:26

**But when the [paraklētos](../../strongs/g/g3875.md) is [erchomai](../../strongs/g/g2064.md), whom I will [pempō](../../strongs/g/g3992.md) unto you from the [patēr](../../strongs/g/g3962.md), the [pneuma](../../strongs/g/g4151.md) of [alētheia](../../strongs/g/g225.md), which [ekporeuomai](../../strongs/g/g1607.md) from the [patēr](../../strongs/g/g3962.md), he shall [martyreō](../../strongs/g/g3140.md) of me:**

<a name="John_15_27"></a>John 15:27

**And ye also shall [martyreō](../../strongs/g/g3140.md), because ye have been with me from the [archē](../../strongs/g/g746.md).**

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 14](john_14.md) - [John 16](john_16.md)