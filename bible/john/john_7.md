# [John 7](https://www.blueletterbible.org/kjv/jhn/7/1/p0/rl1/s_1003001)

<a name="john_7_1"></a>John 7:1

After these things [Iēsous](../../strongs/g/g2424.md) [peripateō](../../strongs/g/g4043.md) in [Galilaia](../../strongs/g/g1056.md): for he [thelō](../../strongs/g/g2309.md) not [peripateō](../../strongs/g/g4043.md) in [Ioudaia](../../strongs/g/g2449.md), because the [Ioudaios](../../strongs/g/g2453.md) [zēteō](../../strongs/g/g2212.md) to [apokteinō](../../strongs/g/g615.md) him.

<a name="john_7_2"></a>John 7:2

Now the [Ioudaios](../../strongs/g/g2453.md) [heortē](../../strongs/g/g1859.md) of [skēnopēgia](../../strongs/g/g4634.md) was [eggys](../../strongs/g/g1451.md).

<a name="john_7_3"></a>John 7:3

His [adelphos](../../strongs/g/g80.md) therefore [eipon](../../strongs/g/g2036.md) unto him, [metabainō](../../strongs/g/g3327.md) hence, and [hypagō](../../strongs/g/g5217.md) into [Ioudaia](../../strongs/g/g2449.md), that thy [mathētēs](../../strongs/g/g3101.md) also may [theōreō](../../strongs/g/g2334.md) the [ergon](../../strongs/g/g2041.md) that thou [poieō](../../strongs/g/g4160.md).

<a name="john_7_4"></a>John 7:4

For there is [oudeis](../../strongs/g/g3762.md) that [poieō](../../strongs/g/g4160.md) any thing in [kryptos](../../strongs/g/g2927.md), and he himself [zēteō](../../strongs/g/g2212.md) to be [parrēsia](../../strongs/g/g3954.md). If thou [poieō](../../strongs/g/g4160.md) these things, [phaneroō](../../strongs/g/g5319.md) thyself to the [kosmos](../../strongs/g/g2889.md).

<a name="john_7_5"></a>John 7:5

For neither did his [adelphos](../../strongs/g/g80.md) [pisteuō](../../strongs/g/g4100.md) in him.

<a name="john_7_6"></a>John 7:6

Then [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, **My [kairos](../../strongs/g/g2540.md) is not yet [pareimi](../../strongs/g/g3918.md): but your [kairos](../../strongs/g/g2540.md) is [pantote](../../strongs/g/g3842.md) [hetoimos](../../strongs/g/g2092.md).**

<a name="john_7_7"></a>John 7:7

**The [kosmos](../../strongs/g/g2889.md) cannot [miseō](../../strongs/g/g3404.md) you; but me it [miseō](../../strongs/g/g3404.md), because I [martyreō](../../strongs/g/g3140.md) of it, that the [ergon](../../strongs/g/g2041.md) thereof are [ponēros](../../strongs/g/g4190.md).**

<a name="john_7_8"></a>John 7:8

**[anabainō](../../strongs/g/g305.md) ye unto this [heortē](../../strongs/g/g1859.md): I [anabainō](../../strongs/g/g305.md) not yet unto this [heortē](../../strongs/g/g1859.md): for my [kairos](../../strongs/g/g2540.md) is not yet [plēroō](../../strongs/g/g4137.md).**

<a name="john_7_9"></a>John 7:9

When he had [eipon](../../strongs/g/g2036.md) [tauta](../../strongs/g/g5023.md) unto them, he [menō](../../strongs/g/g3306.md) still in [Galilaia](../../strongs/g/g1056.md).

<a name="john_7_10"></a>John 7:10

But when his [adelphos](../../strongs/g/g80.md) were [anabainō](../../strongs/g/g305.md), then [anabainō](../../strongs/g/g305.md) he also unto the [heortē](../../strongs/g/g1859.md), not [phanerōs](../../strongs/g/g5320.md), but as it were in [kryptos](../../strongs/g/g2927.md).

<a name="john_7_11"></a>John 7:11

Then the [Ioudaios](../../strongs/g/g2453.md) [zēteō](../../strongs/g/g2212.md) him at the [heortē](../../strongs/g/g1859.md), and [legō](../../strongs/g/g3004.md), Where is he?

<a name="john_7_12"></a>John 7:12

And there was [polys](../../strongs/g/g4183.md) [goggysmos](../../strongs/g/g1112.md) among the [ochlos](../../strongs/g/g3793.md) concerning him: for some [legō](../../strongs/g/g3004.md), He is an [agathos](../../strongs/g/g18.md): others [legō](../../strongs/g/g3004.md), [ou](../../strongs/g/g3756.md); but he [planaō](../../strongs/g/g4105.md) the [ochlos](../../strongs/g/g3793.md).

<a name="john_7_13"></a>John 7:13

Howbeit [oudeis](../../strongs/g/g3762.md) [laleō](../../strongs/g/g2980.md) [parrēsia](../../strongs/g/g3954.md) of him for [phobos](../../strongs/g/g5401.md) of the [Ioudaios](../../strongs/g/g2453.md).

<a name="john_7_14"></a>John 7:14

Now about the [mesoō](../../strongs/g/g3322.md) of the [heortē](../../strongs/g/g1859.md) [Iēsous](../../strongs/g/g2424.md) [anabainō](../../strongs/g/g305.md) into the [hieron](../../strongs/g/g2411.md), and [didaskō](../../strongs/g/g1321.md).

<a name="john_7_15"></a>John 7:15

And the [Ioudaios](../../strongs/g/g2453.md) [thaumazō](../../strongs/g/g2296.md), [legō](../../strongs/g/g3004.md), How [eidō](../../strongs/g/g1492.md) [houtos](../../strongs/g/g3778.md) [gramma](../../strongs/g/g1121.md), having never [manthanō](../../strongs/g/g3129.md)?

<a name="john_7_16"></a>John 7:16

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) them, and [eipon](../../strongs/g/g2036.md), **My [didachē](../../strongs/g/g1322.md) is not mine, but his that [pempō](../../strongs/g/g3992.md) me.**

<a name="john_7_17"></a>John 7:17

**If any [tis](../../strongs/g/g5100.md) [thelō](../../strongs/g/g2309.md) [poieō](../../strongs/g/g4160.md) his [thelēma](../../strongs/g/g2307.md), he shall [ginōskō](../../strongs/g/g1097.md) of the [didachē](../../strongs/g/g1322.md), [poteron](../../strongs/g/g4220.md) it be of [theos](../../strongs/g/g2316.md), or I [laleō](../../strongs/g/g2980.md) of myself.**

<a name="john_7_18"></a>John 7:18

**He that [laleō](../../strongs/g/g2980.md) of himself [zēteō](../../strongs/g/g2212.md) his own [doxa](../../strongs/g/g1391.md): but he that [zēteō](../../strongs/g/g2212.md) his [doxa](../../strongs/g/g1391.md) that [pempō](../../strongs/g/g3992.md) him, the same is [alēthēs](../../strongs/g/g227.md), and no [adikia](../../strongs/g/g93.md) is in him.**

<a name="john_7_19"></a>John 7:19

**Did not [Mōÿsēs](../../strongs/g/g3475.md) [didōmi](../../strongs/g/g1325.md) you the [nomos](../../strongs/g/g3551.md), and yet none of you [poieō](../../strongs/g/g4160.md) the [nomos](../../strongs/g/g3551.md)?  Why [zēteō](../../strongs/g/g2212.md) ye to [apokteinō](../../strongs/g/g615.md) me?**

<a name="john_7_20"></a>John 7:20

The [ochlos](../../strongs/g/g3793.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), Thou hast a [daimonion](../../strongs/g/g1140.md): who [zēteō](../../strongs/g/g2212.md) to [apokteinō](../../strongs/g/g615.md) thee?

<a name="john_7_21"></a>John 7:21

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **I have [poieō](../../strongs/g/g4160.md) one [ergon](../../strongs/g/g2041.md), and ye all [thaumazō](../../strongs/g/g2296.md).**

<a name="john_7_22"></a>John 7:22

**[Mōÿsēs](../../strongs/g/g3475.md) therefore [didōmi](../../strongs/g/g1325.md) unto you [peritomē](../../strongs/g/g4061.md); (not because it is of [Mōÿsēs](../../strongs/g/g3475.md), but of the [patēr](../../strongs/g/g3962.md);) and ye on the [sabbaton](../../strongs/g/g4521.md) [peritemnō](../../strongs/g/g4059.md) an [anthrōpos](../../strongs/g/g444.md).**

<a name="john_7_23"></a>John 7:23

**If an [anthrōpos](../../strongs/g/g444.md) on the [sabbaton](../../strongs/g/g4521.md) [lambanō](../../strongs/g/g2983.md) [peritomē](../../strongs/g/g4061.md), that the [nomos](../../strongs/g/g3551.md) of [Mōÿsēs](../../strongs/g/g3475.md) should not be [lyō](../../strongs/g/g3089.md); are ye [cholaō](../../strongs/g/g5520.md) at me, because I have [poieō](../../strongs/g/g4160.md) an [anthrōpos](../../strongs/g/g444.md) [holos](../../strongs/g/g3650.md) [hygiēs](../../strongs/g/g5199.md) on the [sabbaton](../../strongs/g/g4521.md)?**

<a name="john_7_24"></a>John 7:24

**[krinō](../../strongs/g/g2919.md) not according to the [opsis](../../strongs/g/g3799.md), but [krinō](../../strongs/g/g2919.md) [dikaios](../../strongs/g/g1342.md) [krisis](../../strongs/g/g2920.md).**

<a name="john_7_25"></a>John 7:25

Then [legō](../../strongs/g/g3004.md) some of them of [Ierosolymitēs](../../strongs/g/g2415.md), Is not this he, whom they [zēteō](../../strongs/g/g2212.md) to [apokteinō](../../strongs/g/g615.md)?

<a name="john_7_26"></a>John 7:26

But, [ide](../../strongs/g/g2396.md), he [laleō](../../strongs/g/g2980.md) [parrēsia](../../strongs/g/g3954.md), and they [legō](../../strongs/g/g3004.md) nothing unto him. Do the [archōn](../../strongs/g/g758.md) [ginōskō](../../strongs/g/g1097.md) [mēpote](../../strongs/g/g3379.md) [alēthōs](../../strongs/g/g230.md) that this is the [alēthōs](../../strongs/g/g230.md) [Christos](../../strongs/g/g5547.md)?

<a name="john_7_27"></a>John 7:27

Howbeit we [eidō](../../strongs/g/g1492.md) [touton](../../strongs/g/g5126.md) whence he is: but when [Christos](../../strongs/g/g5547.md) [erchomai](../../strongs/g/g2064.md), [oudeis](../../strongs/g/g3762.md) [ginōskō](../../strongs/g/g1097.md) whence he is.

<a name="john_7_28"></a>John 7:28

Then [krazō](../../strongs/g/g2896.md) [Iēsous](../../strongs/g/g2424.md) in the [hieron](../../strongs/g/g2411.md) as he [didaskō](../../strongs/g/g1321.md), [legō](../../strongs/g/g3004.md),**Ye both [eidō](../../strongs/g/g1492.md) me, and ye [eidō](../../strongs/g/g1492.md) whence I am: and I am not [erchomai](../../strongs/g/g2064.md) of myself, but he that [pempō](../../strongs/g/g3992.md) me is [alēthinos](../../strongs/g/g228.md), whom ye [eidō](../../strongs/g/g1492.md) not.**

<a name="john_7_29"></a>John 7:29

**But I [eidō](../../strongs/g/g1492.md) him: for I am from him, and he hath [apostellō](../../strongs/g/g649.md) me.**

<a name="john_7_30"></a>John 7:30

Then they [zēteō](../../strongs/g/g2212.md) to [piazō](../../strongs/g/g4084.md) him: but [oudeis](../../strongs/g/g3762.md) [epiballō](../../strongs/g/g1911.md) [cheir](../../strongs/g/g5495.md) on him, because his [hōra](../../strongs/g/g5610.md) was not yet [erchomai](../../strongs/g/g2064.md).

<a name="john_7_31"></a>John 7:31

And [polys](../../strongs/g/g4183.md) of the [ochlos](../../strongs/g/g3793.md) [pisteuō](../../strongs/g/g4100.md) on him, and [legō](../../strongs/g/g3004.md), When [Christos](../../strongs/g/g5547.md) [erchomai](../../strongs/g/g2064.md), will he do more [sēmeion](../../strongs/g/g4592.md) than these which this hath [poieō](../../strongs/g/g4160.md)?

<a name="john_7_32"></a>John 7:32

The [Pharisaios](../../strongs/g/g5330.md) [akouō](../../strongs/g/g191.md) that the [ochlos](../../strongs/g/g3793.md) [goggyzō](../../strongs/g/g1111.md) such things concerning him; and the [Pharisaios](../../strongs/g/g5330.md) and the [archiereus](../../strongs/g/g749.md) [apostellō](../../strongs/g/g649.md) [hypēretēs](../../strongs/g/g5257.md) to [piazō](../../strongs/g/g4084.md) him.

<a name="john_7_33"></a>John 7:33

Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) unto them, **Yet a [mikros](../../strongs/g/g3398.md) [chronos](../../strongs/g/g5550.md) am I with you, and then I [hypagō](../../strongs/g/g5217.md) unto him that [pempō](../../strongs/g/g3992.md) me.**

<a name="john_7_34"></a>John 7:34

**Ye shall [zēteō](../../strongs/g/g2212.md) me, and shall not [heuriskō](../../strongs/g/g2147.md) me: and where I am, thither ye cannot [erchomai](../../strongs/g/g2064.md).**

<a name="john_7_35"></a>John 7:35

Then [eipon](../../strongs/g/g2036.md) the [Ioudaios](../../strongs/g/g2453.md) among themselves, Whither will he [poreuō](../../strongs/g/g4198.md), that we shall not [heuriskō](../../strongs/g/g2147.md) him? will he [poreuō](../../strongs/g/g4198.md) unto the [diaspora](../../strongs/g/g1290.md) among the [Hellēn](../../strongs/g/g1672.md), and [didaskō](../../strongs/g/g1321.md) the [Hellēn](../../strongs/g/g1672.md)?

<a name="john_7_36"></a>John 7:36

What [logos](../../strongs/g/g3056.md) is this that he [eipon](../../strongs/g/g2036.md), **Ye shall [zēteō](../../strongs/g/g2212.md) me, and shall not [heuriskō](../../strongs/g/g2147.md) me: and where I am, thither ye cannot [erchomai](../../strongs/g/g2064.md)?**

<a name="john_7_37"></a>John 7:37

In the [eschatos](../../strongs/g/g2078.md) [hēmera](../../strongs/g/g2250.md), that [megas](../../strongs/g/g3173.md) of the [heortē](../../strongs/g/g1859.md), [Iēsous](../../strongs/g/g2424.md) [histēmi](../../strongs/g/g2476.md) and [krazō](../../strongs/g/g2896.md), [legō](../../strongs/g/g3004.md), **If [tis](../../strongs/g/g5100.md) [dipsaō](../../strongs/g/g1372.md), let him [erchomai](../../strongs/g/g2064.md) unto me, and [pinō](../../strongs/g/g4095.md).**

<a name="john_7_38"></a>John 7:38

**He that [pisteuō](../../strongs/g/g4100.md) on me, as the [graphē](../../strongs/g/g1124.md) hath [eipon](../../strongs/g/g2036.md), out of his [koilia](../../strongs/g/g2836.md) shall [reō](../../strongs/g/g4482.md) [potamos](../../strongs/g/g4215.md) of [zaō](../../strongs/g/g2198.md) [hydōr](../../strongs/g/g5204.md).**

<a name="john_7_39"></a>John 7:39

(But this [eipon](../../strongs/g/g2036.md) he of the [pneuma](../../strongs/g/g4151.md), which they that [pisteuō](../../strongs/g/g4100.md) on him should [lambanō](../../strongs/g/g2983.md): for the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) was [oupō](../../strongs/g/g3768.md); because that [Iēsous](../../strongs/g/g2424.md) was [oudepō](../../strongs/g/g3764.md) [doxazō](../../strongs/g/g1392.md).)

<a name="john_7_40"></a>John 7:40

[polys](../../strongs/g/g4183.md) of the [ochlos](../../strongs/g/g3793.md) therefore, when they [akouō](../../strongs/g/g191.md) this [logos](../../strongs/g/g3056.md), [legō](../../strongs/g/g3004.md), [alēthōs](../../strongs/g/g230.md) this is the [prophētēs](../../strongs/g/g4396.md).

<a name="john_7_41"></a>John 7:41

Others [legō](../../strongs/g/g3004.md), This is the [Christos](../../strongs/g/g5547.md). But some [legō](../../strongs/g/g3004.md), Shall [Christos](../../strongs/g/g5547.md) [erchomai](../../strongs/g/g2064.md) out of [Galilaia](../../strongs/g/g1056.md)?

<a name="john_7_42"></a>John 7:42

Hath not the [graphē](../../strongs/g/g1124.md) [eipon](../../strongs/g/g2036.md), That [Christos](../../strongs/g/g5547.md) [erchomai](../../strongs/g/g2064.md) of the [sperma](../../strongs/g/g4690.md) of [Dabid](../../strongs/g/g1138.md), and out of the [kōmē](../../strongs/g/g2968.md) of [Bēthleem](../../strongs/g/g965.md), where [Dabid](../../strongs/g/g1138.md) was?

<a name="john_7_43"></a>John 7:43

So there was a [schisma](../../strongs/g/g4978.md) among the [ochlos](../../strongs/g/g3793.md) because of him.

<a name="john_7_44"></a>John 7:44

And some of them [thelō](../../strongs/g/g2309.md) have [piazō](../../strongs/g/g4084.md) him; but [oudeis](../../strongs/g/g3762.md) [epiballō](../../strongs/g/g1911.md) [cheir](../../strongs/g/g5495.md) on him.

<a name="john_7_45"></a>John 7:45

Then [erchomai](../../strongs/g/g2064.md) the [hypēretēs](../../strongs/g/g5257.md) to the [archiereus](../../strongs/g/g749.md) and [Pharisaios](../../strongs/g/g5330.md); and they [eipon](../../strongs/g/g2036.md) unto them, Why have ye not [agō](../../strongs/g/g71.md) him?

<a name="john_7_46"></a>John 7:46

The [hypēretēs](../../strongs/g/g5257.md) [apokrinomai](../../strongs/g/g611.md), Never [anthrōpos](../../strongs/g/g444.md) [laleō](../../strongs/g/g2980.md) like this [anthrōpos](../../strongs/g/g444.md).

<a name="john_7_47"></a>John 7:47

Then [apokrinomai](../../strongs/g/g611.md) them the [Pharisaios](../../strongs/g/g5330.md), Are ye also [planaō](../../strongs/g/g4105.md)?

<a name="john_7_48"></a>John 7:48

[mē tis](../../strongs/g/g3387.md) of the [archōn](../../strongs/g/g758.md) or of the [Pharisaios](../../strongs/g/g5330.md) [pisteuō](../../strongs/g/g4100.md) on him?

<a name="john_7_49"></a>John 7:49

But this [ochlos](../../strongs/g/g3793.md) who [ginōskō](../../strongs/g/g1097.md) not the [nomos](../../strongs/g/g3551.md) are [epikataratos](../../strongs/g/g1944.md).

<a name="john_7_50"></a>John 7:50

[Nikodēmos](../../strongs/g/g3530.md) [legō](../../strongs/g/g3004.md) unto them, (he that [erchomai](../../strongs/g/g2064.md) to [autos](../../strongs/g/g846.md) by [nyx](../../strongs/g/g3571.md), being one of them,)

<a name="john_7_51"></a>John 7:51

Doth our [nomos](../../strongs/g/g3551.md) [krinō](../../strongs/g/g2919.md) any [anthrōpos](../../strongs/g/g444.md), [proteros](../../strongs/g/g4386.md) it [akouō](../../strongs/g/g191.md) him, and [ginōskō](../../strongs/g/g1097.md) what he [poieō](../../strongs/g/g4160.md)?

<a name="john_7_52"></a>John 7:52

They [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, Art thou also of [Galilaia](../../strongs/g/g1056.md)?  [eraunaō](../../strongs/g/g2045.md), and [eidō](../../strongs/g/g1492.md): for out of [Galilaia](../../strongs/g/g1056.md) [egeirō](../../strongs/g/g1453.md) no [prophētēs](../../strongs/g/g4396.md).

<a name="john_7_53"></a>John 7:53

And [hekastos](../../strongs/g/g1538.md) [poreuō](../../strongs/g/g4198.md) unto his own [oikos](../../strongs/g/g3624.md).

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 6](john_6.md) - [John 8](john_8.md)

---

[^1]: [John 7:53 Commentary](../../commentary/john/john_7_commentary.md#john_7_53)
