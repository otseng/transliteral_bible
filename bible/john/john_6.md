# [John 6](https://www.blueletterbible.org/kjv/jhn/6/1/p0/rl1/s_1003001)

<a name="john_6_1"></a>John 6:1

After these things [Iēsous](../../strongs/g/g2424.md) [aperchomai](../../strongs/g/g565.md) over the [thalassa](../../strongs/g/g2281.md) of [Galilaia](../../strongs/g/g1056.md), which is of [Tiberias](../../strongs/g/g5085.md).

<a name="john_6_2"></a>John 6:2

And a [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) [akoloutheō](../../strongs/g/g190.md) him, because they [horaō](../../strongs/g/g3708.md) his [sēmeion](../../strongs/g/g4592.md) which he [poieō](../../strongs/g/g4160.md) on them that were [astheneō](../../strongs/g/g770.md).

<a name="john_6_3"></a>John 6:3

And [Iēsous](../../strongs/g/g2424.md) [anerchomai](../../strongs/g/g424.md) into a [oros](../../strongs/g/g3735.md), and there he [kathēmai](../../strongs/g/g2521.md) with his [mathētēs](../../strongs/g/g3101.md).

<a name="john_6_4"></a>John 6:4

And the [pascha](../../strongs/g/g3957.md), a [heortē](../../strongs/g/g1859.md) of the [Ioudaios](../../strongs/g/g2453.md), was [eggys](../../strongs/g/g1451.md).

<a name="john_6_5"></a>John 6:5

When [Iēsous](../../strongs/g/g2424.md) then [epairō](../../strongs/g/g1869.md) [ophthalmos](../../strongs/g/g3788.md), and [theaomai](../../strongs/g/g2300.md) a [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) [erchomai](../../strongs/g/g2064.md) unto him, he [legō](../../strongs/g/g3004.md) unto [Philippos](../../strongs/g/g5376.md), **Whence shall we [agorazō](../../strongs/g/g59.md) [artos](../../strongs/g/g740.md), that these may [phago](../../strongs/g/g5315.md)?**

<a name="john_6_6"></a>John 6:6

And this he [legō](../../strongs/g/g3004.md) to [peirazō](../../strongs/g/g3985.md) him: for he himself [eidō](../../strongs/g/g1492.md) what he would [poieō](../../strongs/g/g4160.md).

<a name="john_6_7"></a>John 6:7

[Philippos](../../strongs/g/g5376.md) [apokrinomai](../../strongs/g/g611.md) him, Two hundred [dēnarion](../../strongs/g/g1220.md) of [artos](../../strongs/g/g740.md) is not [arkeō](../../strongs/g/g714.md) for them, that every one of them may [lambanō](../../strongs/g/g2983.md) a [brachys](../../strongs/g/g1024.md).

<a name="john_6_8"></a>John 6:8

One of his [mathētēs](../../strongs/g/g3101.md), [Andreas](../../strongs/g/g406.md), [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) [adelphos](../../strongs/g/g80.md), [legō](../../strongs/g/g3004.md) unto him,

<a name="john_6_9"></a>John 6:9

There is a [paidarion](../../strongs/g/g3808.md) here, which hath five [krithinos](../../strongs/g/g2916.md) [artos](../../strongs/g/g740.md), and two [opsarion](../../strongs/g/g3795.md): but what are they among [tosoutos](../../strongs/g/g5118.md)?

<a name="john_6_10"></a>John 6:10

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md), **[poieō](../../strongs/g/g4160.md) the [anthrōpos](../../strongs/g/g444.md) [anapiptō](../../strongs/g/g377.md).** Now there was [polys](../../strongs/g/g4183.md) [chortos](../../strongs/g/g5528.md) in the [topos](../../strongs/g/g5117.md). So the [anēr](../../strongs/g/g435.md) [anapiptō](../../strongs/g/g377.md), in [arithmos](../../strongs/g/g706.md) about five thousand.

<a name="john_6_11"></a>John 6:11

And [Iēsous](../../strongs/g/g2424.md) [lambanō](../../strongs/g/g2983.md) the [artos](../../strongs/g/g740.md); and when he had [eucharisteō](../../strongs/g/g2168.md), he [diadidōmi](../../strongs/g/g1239.md) to the [mathētēs](../../strongs/g/g3101.md), and the [mathētēs](../../strongs/g/g3101.md) to them that were [anakeimai](../../strongs/g/g345.md); and [homoiōs](../../strongs/g/g3668.md) of the [opsarion](../../strongs/g/g3795.md) as much as they would.

<a name="john_6_12"></a>John 6:12

When they were [empi(m)plēmi](../../strongs/g/g1705.md), he [legō](../../strongs/g/g3004.md) unto his [mathētēs](../../strongs/g/g3101.md), **[synagō](../../strongs/g/g4863.md) the [klasma](../../strongs/g/g2801.md) that [perisseuō](../../strongs/g/g4052.md), that nothing [apollymi](../../strongs/g/g622.md).**

<a name="john_6_13"></a>John 6:13

Therefore they [synagō](../../strongs/g/g4863.md), and [gemizō](../../strongs/g/g1072.md) [dōdeka](../../strongs/g/g1427.md) [kophinos](../../strongs/g/g2894.md) with the [klasma](../../strongs/g/g2801.md) of the five [krithinos](../../strongs/g/g2916.md) [artos](../../strongs/g/g740.md), which remained [perisseuō](../../strongs/g/g4052.md) unto them that had [bibrōskō](../../strongs/g/g977.md).

<a name="john_6_14"></a>John 6:14

Then those [anthrōpos](../../strongs/g/g444.md), when they had [eidō](../../strongs/g/g1492.md) the [sēmeion](../../strongs/g/g4592.md) that [Iēsous](../../strongs/g/g2424.md) [poieō](../../strongs/g/g4160.md), [legō](../../strongs/g/g3004.md), This is [alēthōs](../../strongs/g/g230.md) that [prophētēs](../../strongs/g/g4396.md) that should [erchomai](../../strongs/g/g2064.md) into the [kosmos](../../strongs/g/g2889.md).

<a name="john_6_15"></a>John 6:15

When [Iēsous](../../strongs/g/g2424.md) therefore [ginōskō](../../strongs/g/g1097.md) that they would [erchomai](../../strongs/g/g2064.md) and [harpazō](../../strongs/g/g726.md) him, to [poieō](../../strongs/g/g4160.md) him a [basileus](../../strongs/g/g935.md), he [anachōreō](../../strongs/g/g402.md) again into a [oros](../../strongs/g/g3735.md) himself alone.

<a name="john_6_16"></a>John 6:16

And when [opsios](../../strongs/g/g3798.md) was [ginomai](../../strongs/g/g1096.md), his [mathētēs](../../strongs/g/g3101.md) went[katabainō](../../strongs/g/g2597.md) unto the [thalassa](../../strongs/g/g2281.md),

<a name="john_6_17"></a>John 6:17

And [embainō](../../strongs/g/g1684.md) into a [ploion](../../strongs/g/g4143.md), and [erchomai](../../strongs/g/g2064.md) over the [thalassa](../../strongs/g/g2281.md) toward [Kapharnaoum](../../strongs/g/g2584.md). And it was now [skotia](../../strongs/g/g4653.md), and [Iēsous](../../strongs/g/g2424.md) was not [erchomai](../../strongs/g/g2064.md) to them.

<a name="john_6_18"></a>John 6:18

And the [thalassa](../../strongs/g/g2281.md) [diegeirō](../../strongs/g/g1326.md) a [megas](../../strongs/g/g3173.md) [anemos](../../strongs/g/g417.md) that [pneō](../../strongs/g/g4154.md).

<a name="john_6_19"></a>John 6:19

So when they had [elaunō](../../strongs/g/g1643.md) about five and twenty or thirty [stadion](../../strongs/g/g4712.md), they [theōreō](../../strongs/g/g2334.md) [Iēsous](../../strongs/g/g2424.md) [peripateō](../../strongs/g/g4043.md) on the [thalassa](../../strongs/g/g2281.md), and [ginomai](../../strongs/g/g1096.md) [eggys](../../strongs/g/g1451.md) unto the [ploion](../../strongs/g/g4143.md): and they were [phobeō](../../strongs/g/g5399.md).

<a name="john_6_20"></a>John 6:20

But he [legō](../../strongs/g/g3004.md) unto them, **It is I; be not [phobeō](../../strongs/g/g5399.md).**

<a name="john_6_21"></a>John 6:21

Then they [thelō](../../strongs/g/g2309.md) [lambanō](../../strongs/g/g2983.md) him into the [ploion](../../strongs/g/g4143.md): and [eutheōs](../../strongs/g/g2112.md) the [ploion](../../strongs/g/g4143.md) was at the [gē](../../strongs/g/g1093.md) whither they [hypagō](../../strongs/g/g5217.md).

<a name="john_6_22"></a>John 6:22

The [epaurion](../../strongs/g/g1887.md), when the [ochlos](../../strongs/g/g3793.md) which [histēmi](../../strongs/g/g2476.md) on the other side of the [thalassa](../../strongs/g/g2281.md) [eidō](../../strongs/g/g1492.md) that there was none other [ploiarion](../../strongs/g/g4142.md) there, save that one whereinto his [mathētēs](../../strongs/g/g3101.md) were [embainō](../../strongs/g/g1684.md), and that [Iēsous](../../strongs/g/g2424.md) [syneiserchomai](../../strongs/g/g4897.md) not with his [mathētēs](../../strongs/g/g3101.md) into the [ploiarion](../../strongs/g/g4142.md), but that his [mathētēs](../../strongs/g/g3101.md) were [aperchomai](../../strongs/g/g565.md) [monos](../../strongs/g/g3441.md);

<a name="john_6_23"></a>John 6:23

(Howbeit there [erchomai](../../strongs/g/g2064.md) other [ploiarion](../../strongs/g/g4142.md) from [Tiberias](../../strongs/g/g5085.md) [eggys](../../strongs/g/g1451.md) unto the [topos](../../strongs/g/g5117.md) where they did [phago](../../strongs/g/g5315.md) [artos](../../strongs/g/g740.md), after that the [kyrios](../../strongs/g/g2962.md) had [eucharisteō](../../strongs/g/g2168.md):)

<a name="john_6_24"></a>John 6:24

When the [ochlos](../../strongs/g/g3793.md) therefore [eidō](../../strongs/g/g1492.md) that [Iēsous](../../strongs/g/g2424.md) was not there, neither his [mathētēs](../../strongs/g/g3101.md), they also [embainō](../../strongs/g/g1684.md) [ploion](../../strongs/g/g4143.md), and [erchomai](../../strongs/g/g2064.md) to [Kapharnaoum](../../strongs/g/g2584.md), [zēteō](../../strongs/g/g2212.md) for [Iēsous](../../strongs/g/g2424.md).

<a name="john_6_25"></a>John 6:25

And when they had [heuriskō](../../strongs/g/g2147.md) him on the other side of the [thalassa](../../strongs/g/g2281.md), they [eipon](../../strongs/g/g2036.md) unto him, [rhabbi](../../strongs/g/g4461.md), when [ginomai](../../strongs/g/g1096.md) thou hither?

<a name="john_6_26"></a>John 6:26

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) them and [eipon](../../strongs/g/g2036.md), **[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, Ye [zēteō](../../strongs/g/g2212.md) me, not because ye [eidō](../../strongs/g/g1492.md) the [sēmeion](../../strongs/g/g4592.md), but because ye did [phago](../../strongs/g/g5315.md) of the [artos](../../strongs/g/g740.md), and were [chortazō](../../strongs/g/g5526.md).**

<a name="john_6_27"></a>John 6:27

**[ergazomai](../../strongs/g/g2038.md) not for the [brōsis](../../strongs/g/g1035.md) which [apollymi](../../strongs/g/g622.md), but for that [brōsis](../../strongs/g/g1035.md) which [menō](../../strongs/g/g3306.md) unto [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md), which the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) shall [didōmi](../../strongs/g/g1325.md) unto you: for him [theos](../../strongs/g/g2316.md) the [patēr](../../strongs/g/g3962.md) hath [sphragizō](../../strongs/g/g4972.md).**

<a name="john_6_28"></a>John 6:28

Then [eipon](../../strongs/g/g2036.md) they unto him, What shall we [poieō](../../strongs/g/g4160.md), that we might [ergazomai](../../strongs/g/g2038.md) the [ergon](../../strongs/g/g2041.md) of [theos](../../strongs/g/g2316.md)?

<a name="john_6_29"></a>John 6:29

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **This is the [ergon](../../strongs/g/g2041.md) of [theos](../../strongs/g/g2316.md), that ye [pisteuō](../../strongs/g/g4100.md) on him whom he hath [apostellō](../../strongs/g/g649.md).**

<a name="john_6_30"></a>John 6:30

They [eipon](../../strongs/g/g2036.md) therefore unto him, What [sēmeion](../../strongs/g/g4592.md) [poieō](../../strongs/g/g4160.md) thou then, that we may [eidō](../../strongs/g/g1492.md), and [pisteuō](../../strongs/g/g4100.md) thee? what dost thou [ergazomai](../../strongs/g/g2038.md)?

<a name="john_6_31"></a>John 6:31

Our [patēr](../../strongs/g/g3962.md) did [phago](../../strongs/g/g5315.md) [manna](../../strongs/g/g3131.md) in the [erēmos](../../strongs/g/g2048.md); as it is [graphō](../../strongs/g/g1125.md), He [didōmi](../../strongs/g/g1325.md) them [artos](../../strongs/g/g740.md) from [ouranos](../../strongs/g/g3772.md) to [phago](../../strongs/g/g5315.md).

<a name="john_6_32"></a>John 6:32

Then [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, [Mōÿsēs](../../strongs/g/g3475.md) [didōmi](../../strongs/g/g1325.md) you not that [artos](../../strongs/g/g740.md) from [ouranos](../../strongs/g/g3772.md); but my [patēr](../../strongs/g/g3962.md) [didōmi](../../strongs/g/g1325.md) you the [alēthinos](../../strongs/g/g228.md) [artos](../../strongs/g/g740.md) from [ouranos](../../strongs/g/g3772.md).**

<a name="john_6_33"></a>John 6:33

**For the [artos](../../strongs/g/g740.md) of [theos](../../strongs/g/g2316.md) is he which [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md), and [didōmi](../../strongs/g/g1325.md) [zōē](../../strongs/g/g2222.md) unto the [kosmos](../../strongs/g/g2889.md).**

<a name="john_6_34"></a>John 6:34

Then [eipon](../../strongs/g/g2036.md) they unto him, [kyrios](../../strongs/g/g2962.md), [pantote](../../strongs/g/g3842.md) [didōmi](../../strongs/g/g1325.md) us this [artos](../../strongs/g/g740.md).

<a name="john_6_35"></a>John 6:35

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **I am the [artos](../../strongs/g/g740.md) of [zōē](../../strongs/g/g2222.md): he that [erchomai](../../strongs/g/g2064.md) to me shall [ou mē](../../strongs/g/g3364.md) [peinaō](../../strongs/g/g3983.md); and he that [pisteuō](../../strongs/g/g4100.md) on me shall [ou mē](../../strongs/g/g3364.md) [pōpote](../../strongs/g/g4455.md) [dipsaō](../../strongs/g/g1372.md).**

<a name="john_6_36"></a>John 6:36

**But I [eipon](../../strongs/g/g2036.md) unto you, That ye also have [horaō](../../strongs/g/g3708.md) me, and [pisteuō](../../strongs/g/g4100.md) not.**

<a name="john_6_37"></a>John 6:37

**All that the [patēr](../../strongs/g/g3962.md) [didōmi](../../strongs/g/g1325.md) me shall [hēkō](../../strongs/g/g2240.md) to me; and him that [erchomai](../../strongs/g/g2064.md) to me I will in no wise [ekballō](../../strongs/g/g1544.md).**

<a name="john_6_38"></a>John 6:38

**For I [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md), not to [poieō](../../strongs/g/g4160.md) mine own [thelēma](../../strongs/g/g2307.md), but the [thelēma](../../strongs/g/g2307.md) of him that [pempō](../../strongs/g/g3992.md) me.**

<a name="john_6_39"></a>John 6:39

**And this is the [patēr](../../strongs/g/g3962.md) [thelēma](../../strongs/g/g2307.md) which hath [pempō](../../strongs/g/g3992.md) me, that of all which he hath [didōmi](../../strongs/g/g1325.md) me I should [apollymi](../../strongs/g/g622.md) [mē](../../strongs/g/g3361.md) [ek](../../strongs/g/g1537.md) [autos](../../strongs/g/g846.md), but should [anistēmi](../../strongs/g/g450.md) it at the [eschatos](../../strongs/g/g2078.md) [hēmera](../../strongs/g/g2250.md).**

<a name="john_6_40"></a>John 6:40

**And this is the [thelēma](../../strongs/g/g2307.md) of him that [pempō](../../strongs/g/g3992.md) me, that every one which [theōreō](../../strongs/g/g2334.md) the [huios](../../strongs/g/g5207.md), and [pisteuō](../../strongs/g/g4100.md) on him, may have [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md): and I will [anistēmi](../../strongs/g/g450.md) him at the [eschatos](../../strongs/g/g2078.md) [hēmera](../../strongs/g/g2250.md).**

<a name="john_6_41"></a>John 6:41

The [Ioudaios](../../strongs/g/g2453.md) then [goggyzō](../../strongs/g/g1111.md) at him, because he [eipon](../../strongs/g/g2036.md), **I am the [artos](../../strongs/g/g740.md) which [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md).**

<a name="john_6_42"></a>John 6:42

And they [legō](../../strongs/g/g3004.md), Is not this [Iēsous](../../strongs/g/g2424.md), the [huios](../../strongs/g/g5207.md) of [Iōsēph](../../strongs/g/g2501.md), whose [patēr](../../strongs/g/g3962.md) and [mētēr](../../strongs/g/g3384.md) we [eidō](../../strongs/g/g1492.md)? how is it then that he [legō](../../strongs/g/g3004.md), **I [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md)?**

<a name="john_6_43"></a>John 6:43

[Iēsous](../../strongs/g/g2424.md) therefore [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **[goggyzō](../../strongs/g/g1111.md) not among [allēlōn](../../strongs/g/g240.md).**

<a name="john_6_44"></a>John 6:44

**[oudeis](../../strongs/g/g3762.md) can [erchomai](../../strongs/g/g2064.md) to me, except the [patēr](../../strongs/g/g3962.md) which hath [pempō](../../strongs/g/g3992.md) me [helkō](../../strongs/g/g1670.md) him: and I will [anistēmi](../../strongs/g/g450.md) him at the [eschatos](../../strongs/g/g2078.md) day.**

<a name="john_6_45"></a>John 6:45

**It is [graphō](../../strongs/g/g1125.md) in the [prophētēs](../../strongs/g/g4396.md), And they shall be all [didaktos](../../strongs/g/g1318.md) of [theos](../../strongs/g/g2316.md). [pas](../../strongs/g/g3956.md) therefore that hath [akouō](../../strongs/g/g191.md), and hath [manthanō](../../strongs/g/g3129.md) of the [patēr](../../strongs/g/g3962.md), [erchomai](../../strongs/g/g2064.md) unto me.**

<a name="john_6_46"></a>John 6:46

**Not that [tis](../../strongs/g/g5100.md) hath [horaō](../../strongs/g/g3708.md) the [patēr](../../strongs/g/g3962.md), save he which is of [theos](../../strongs/g/g2316.md), he hath [horaō](../../strongs/g/g3708.md) the [patēr](../../strongs/g/g3962.md).**

<a name="john_6_47"></a>John 6:47

**[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, He that [pisteuō](../../strongs/g/g4100.md) on me hath [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md)**

<a name="john_6_48"></a>John 6:48

**I am that [artos](../../strongs/g/g740.md) [zōē](../../strongs/g/g2222.md).**

<a name="john_6_49"></a>John 6:49

**Your [patēr](../../strongs/g/g3962.md) did [phago](../../strongs/g/g5315.md) [manna](../../strongs/g/g3131.md) in the [erēmos](../../strongs/g/g2048.md), and are [apothnēskō](../../strongs/g/g599.md).**

<a name="john_6_50"></a>John 6:50

**This is the [artos](../../strongs/g/g740.md) which [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md), that a man may [phago](../../strongs/g/g5315.md) thereof, and not [apothnēskō](../../strongs/g/g599.md).**

<a name="john_6_51"></a>John 6:51

**I am the [zaō](../../strongs/g/g2198.md) [artos](../../strongs/g/g740.md) which [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md): if any man [phago](../../strongs/g/g5315.md) of this [artos](../../strongs/g/g740.md), he shall [zaō](../../strongs/g/g2198.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md): and the [artos](../../strongs/g/g740.md) that I will [didōmi](../../strongs/g/g1325.md) is my [sarx](../../strongs/g/g4561.md), which I will [didōmi](../../strongs/g/g1325.md) for the [zōē](../../strongs/g/g2222.md) of the [kosmos](../../strongs/g/g2889.md).**

<a name="john_6_52"></a>John 6:52

The [Ioudaios](../../strongs/g/g2453.md) therefore [machomai](../../strongs/g/g3164.md) among [allēlōn](../../strongs/g/g240.md), [legō](../../strongs/g/g3004.md), How can [houtos](../../strongs/g/g3778.md) [didōmi](../../strongs/g/g1325.md) us his [sarx](../../strongs/g/g4561.md) to [phago](../../strongs/g/g5315.md)?

<a name="john_6_53"></a>John 6:53

Then [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, Except ye [phago](../../strongs/g/g5315.md) the [sarx](../../strongs/g/g4561.md) of the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md), and [pinō](../../strongs/g/g4095.md) his [haima](../../strongs/g/g129.md), ye have no [zōē](../../strongs/g/g2222.md) in you.**

<a name="john_6_54"></a>John 6:54

**Whoso [trōgō](../../strongs/g/g5176.md) my [sarx](../../strongs/g/g4561.md), and [pinō](../../strongs/g/g4095.md) my [haima](../../strongs/g/g129.md), hath [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md); and I will [anistēmi](../../strongs/g/g450.md) him at the [eschatos](../../strongs/g/g2078.md) [hēmera](../../strongs/g/g2250.md).**

<a name="john_6_55"></a>John 6:55

**For my [sarx](../../strongs/g/g4561.md) is [brōsis](../../strongs/g/g1035.md) [alēthōs](../../strongs/g/g230.md), and my [haima](../../strongs/g/g129.md) is [posis](../../strongs/g/g4213.md) [alēthōs](../../strongs/g/g230.md).**

<a name="john_6_56"></a>John 6:56

**He that [trōgō](../../strongs/g/g5176.md) my [sarx](../../strongs/g/g4561.md), and [pinō](../../strongs/g/g4095.md) my [haima](../../strongs/g/g129.md), [menō](../../strongs/g/g3306.md) in me, and I in him.**

<a name="john_6_57"></a>John 6:57

**As the [zaō](../../strongs/g/g2198.md) [patēr](../../strongs/g/g3962.md) hath [apostellō](../../strongs/g/g649.md) me, and I [zaō](../../strongs/g/g2198.md) by the [patēr](../../strongs/g/g3962.md): so he that [trōgō](../../strongs/g/g5176.md) me, even he shall [zaō](../../strongs/g/g2198.md) by me.**

<a name="john_6_58"></a>John 6:58

**This is that [artos](../../strongs/g/g740.md) which [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md): not as your [patēr](../../strongs/g/g3962.md) did [phago](../../strongs/g/g5315.md) [manna](../../strongs/g/g3131.md), and are [apothnēskō](../../strongs/g/g599.md): he that [trōgō](../../strongs/g/g5176.md) of this [artos](../../strongs/g/g740.md) shall [zaō](../../strongs/g/g2198.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md).**

<a name="john_6_59"></a>John 6:59

These things [eipon](../../strongs/g/g2036.md) he in the [synagōgē](../../strongs/g/g4864.md), as he [didaskō](../../strongs/g/g1321.md) in [Kapharnaoum](../../strongs/g/g2584.md).

<a name="john_6_60"></a>John 6:60

[polys](../../strongs/g/g4183.md) therefore of his [mathētēs](../../strongs/g/g3101.md), when they had [akouō](../../strongs/g/g191.md) this, [eipon](../../strongs/g/g2036.md), This is [sklēros](../../strongs/g/g4642.md) [logos](../../strongs/g/g3056.md); who can [akouō](../../strongs/g/g191.md) it?

<a name="john_6_61"></a>John 6:61

When [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) in himself that his [mathētēs](../../strongs/g/g3101.md) [goggyzō](../../strongs/g/g1111.md) at it, he [eipon](../../strongs/g/g2036.md) unto them, **Doth this [skandalizō](../../strongs/g/g4624.md) you?**

<a name="john_6_62"></a>John 6:62

**What and if ye shall [theōreō](../../strongs/g/g2334.md) the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [anabainō](../../strongs/g/g305.md) where he was [proteros](../../strongs/g/g4386.md)?**

<a name="john_6_63"></a>John 6:63

**It is the [pneuma](../../strongs/g/g4151.md) that [zōopoieō](../../strongs/g/g2227.md); the [sarx](../../strongs/g/g4561.md) [ōpheleō](../../strongs/g/g5623.md) nothing: the [rhēma](../../strongs/g/g4487.md) that I [laleō](../../strongs/g/g2980.md) unto you, they are [pneuma](../../strongs/g/g4151.md), and they are [zōē](../../strongs/g/g2222.md).**

<a name="john_6_64"></a>John 6:64

But there are some of you that [pisteuō](../../strongs/g/g4100.md) not. For [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) from the [archē](../../strongs/g/g746.md) who they were that [pisteuō](../../strongs/g/g4100.md) not, and who should [paradidōmi](../../strongs/g/g3860.md) him.

<a name="john_6_65"></a>John 6:65

And he [legō](../../strongs/g/g3004.md), **Therefore [eipon](../../strongs/g/g2046.md) I unto you, that [oudeis](../../strongs/g/g3762.md) [dynamai](../../strongs/g/g1410.md) [erchomai](../../strongs/g/g2064.md) unto me, except it were [didōmi](../../strongs/g/g1325.md) unto him of my [patēr](../../strongs/g/g3962.md).**

<a name="john_6_66"></a>John 6:66

From that [polys](../../strongs/g/g4183.md) of his [mathētēs](../../strongs/g/g3101.md) [aperchomai](../../strongs/g/g565.md) back, and [peripateō](../../strongs/g/g4043.md) no more with him.

<a name="john_6_67"></a>John 6:67

Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) unto the [dōdeka](../../strongs/g/g1427.md), **[thelō](../../strongs/g/g2309.md) ye also [hypagō](../../strongs/g/g5217.md)?**

<a name="john_6_68"></a>John 6:68

Then [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) [apokrinomai](../../strongs/g/g611.md) him, [kyrios](../../strongs/g/g2962.md), to whom shall we [aperchomai](../../strongs/g/g565.md)? thou hast the [rhēma](../../strongs/g/g4487.md) of [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md).

<a name="john_6_69"></a>John 6:69

And we [pisteuō](../../strongs/g/g4100.md) and [ginōskō](../../strongs/g/g1097.md) that thou art that [Christos](../../strongs/g/g5547.md), the [huios](../../strongs/g/g5207.md) of the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md). [^1]

<a name="john_6_70"></a>John 6:70

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) them, **Have not I [eklegomai](../../strongs/g/g1586.md) you [dōdeka](../../strongs/g/g1427.md), and one of you is a [diabolos](../../strongs/g/g1228.md)?**

<a name="john_6_71"></a>John 6:71

He [legō](../../strongs/g/g3004.md) of [Ioudas](../../strongs/g/g2455.md) [Iskariōth](../../strongs/g/g2469.md) of [Simōn](../../strongs/g/g4613.md): for he it was that should [paradidōmi](../../strongs/g/g3860.md) him, being one of the [dōdeka](../../strongs/g/g1427.md).

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 5](john_5.md) - [John 7](john_7.md)

---

[^1]: [John 6:69 Commentary](../../commentary/john/john_6_commentary.md#john_6_69)
