# [John 16](https://www.blueletterbible.org/kjv/jhn/16/1/s_1012001)

<a name="john_16_1"></a>John 16:1

**These things have I [laleō](../../strongs/g/g2980.md) unto you, that ye should not be [skandalizō](../../strongs/g/g4624.md).**

<a name="john_16_2"></a>John 16:2

**They shall [poieō](../../strongs/g/g4160.md) you [aposynagōgos](../../strongs/g/g656.md): [alla](../../strongs/g/g235.md), the [hōra](../../strongs/g/g5610.md) [erchomai](../../strongs/g/g2064.md), that whosoever [apokteinō](../../strongs/g/g615.md) you will [dokeō](../../strongs/g/g1380.md) that he [prospherō](../../strongs/g/g4374.md) [theos](../../strongs/g/g2316.md) [latreia](../../strongs/g/g2999.md).**

<a name="john_16_3"></a>John 16:3

**And these things will they [poieō](../../strongs/g/g4160.md) unto you, because they have not [ginōskō](../../strongs/g/g1097.md) the [patēr](../../strongs/g/g3962.md), nor me.**

<a name="john_16_4"></a>John 16:4

**But these things have I [laleō](../../strongs/g/g2980.md) you, that when the [hōra](../../strongs/g/g5610.md) shall [erchomai](../../strongs/g/g2064.md), ye may [mnēmoneuō](../../strongs/g/g3421.md) that I [eipon](../../strongs/g/g2036.md) you of them. And these things I [eipon](../../strongs/g/g2036.md) not unto you at the [archē](../../strongs/g/g746.md), because I was with you.**

<a name="john_16_5"></a>John 16:5

**But now I [hypagō](../../strongs/g/g5217.md) to him that [pempō](../../strongs/g/g3992.md) me; and none of you [erōtaō](../../strongs/g/g2065.md) me, Whither [hypagō](../../strongs/g/g5217.md) thou?**

<a name="john_16_6"></a>John 16:6

**But because I have [laleō](../../strongs/g/g2980.md) these things unto you, [lypē](../../strongs/g/g3077.md) hath [plēroō](../../strongs/g/g4137.md) your [kardia](../../strongs/g/g2588.md).**

<a name="john_16_7"></a>John 16:7

**[alla](../../strongs/g/g235.md) I [legō](../../strongs/g/g3004.md) you the [alētheia](../../strongs/g/g225.md); It is [sympherō](../../strongs/g/g4851.md) for you that I [aperchomai](../../strongs/g/g565.md): for if I [aperchomai](../../strongs/g/g565.md) not, the [paraklētos](../../strongs/g/g3875.md) will not [erchomai](../../strongs/g/g2064.md) unto you; but if I [poreuō](../../strongs/g/g4198.md), I will [pempō](../../strongs/g/g3992.md) him unto you.**

<a name="john_16_8"></a>John 16:8

**And when he is [erchomai](../../strongs/g/g2064.md), he will [elegchō](../../strongs/g/g1651.md) the [kosmos](../../strongs/g/g2889.md) of [hamartia](../../strongs/g/g266.md), and of [dikaiosynē](../../strongs/g/g1343.md), and of [krisis](../../strongs/g/g2920.md)**

<a name="john_16_9"></a>John 16:9

**Of [hamartia](../../strongs/g/g266.md), because they [pisteuō](../../strongs/g/g4100.md) not on me;**

<a name="john_16_10"></a>John 16:10

**Of [dikaiosynē](../../strongs/g/g1343.md), because I [hypagō](../../strongs/g/g5217.md) to my [patēr](../../strongs/g/g3962.md), and ye [theōreō](../../strongs/g/g2334.md) me no more;**

<a name="john_16_11"></a>John 16:11

**Of [krisis](../../strongs/g/g2920.md), because the [archōn](../../strongs/g/g758.md) of this [kosmos](../../strongs/g/g2889.md) is [krinō](../../strongs/g/g2919.md).**

<a name="john_16_12"></a>John 16:12

**I have yet [polys](../../strongs/g/g4183.md) to [legō](../../strongs/g/g3004.md) unto you, but ye cannot [bastazō](../../strongs/g/g941.md) them now.**

<a name="john_16_13"></a>John 16:13

**Howbeit when he, the [pneuma](../../strongs/g/g4151.md) of [alētheia](../../strongs/g/g225.md), is [erchomai](../../strongs/g/g2064.md), he will [hodēgeō](../../strongs/g/g3594.md) you into all [alētheia](../../strongs/g/g225.md): for he shall not [laleō](../../strongs/g/g2980.md) of himself; but whatsoever he shall [akouō](../../strongs/g/g191.md), shall he [laleō](../../strongs/g/g2980.md): and he will [anaggellō](../../strongs/g/g312.md) you things to [erchomai](../../strongs/g/g2064.md).**

<a name="john_16_14"></a>John 16:14

**He shall [doxazō](../../strongs/g/g1392.md) me: for he shall [lambanō](../../strongs/g/g2983.md) of mine, and shall [anaggellō](../../strongs/g/g312.md) unto you.**

<a name="john_16_15"></a>John 16:15

**All things that the [patēr](../../strongs/g/g3962.md) hath are mine: therefore [eipon](../../strongs/g/g2036.md) I, that he shall [lambanō](../../strongs/g/g2983.md) of mine, and shall [anaggellō](../../strongs/g/g312.md) it unto you.**

<a name="john_16_16"></a>John 16:16

**A [mikron](../../strongs/g/g3397.md), and ye shall not [theōreō](../../strongs/g/g2334.md) me: and again, a [mikron](../../strongs/g/g3397.md), and ye shall [optanomai](../../strongs/g/g3700.md) me, because I [hypagō](../../strongs/g/g5217.md) to the [patēr](../../strongs/g/g3962.md).**

<a name="john_16_17"></a>John 16:17

Then [eipon](../../strongs/g/g2036.md) of his [mathētēs](../../strongs/g/g3101.md) among [allēlōn](../../strongs/g/g240.md), What is this that he [legō](../../strongs/g/g3004.md) unto us, **A [mikron](../../strongs/g/g3397.md), and ye shall not [theōreō](../../strongs/g/g2334.md) me: and again, a [mikron](../../strongs/g/g3397.md), and ye shall [optanomai](../../strongs/g/g3700.md) me: and, Because I [hypagō](../../strongs/g/g5217.md) to the [patēr](../../strongs/g/g3962.md)?**

<a name="john_16_18"></a>John 16:18

They [legō](../../strongs/g/g3004.md) therefore, What is this that he [legō](../../strongs/g/g3004.md), **A [mikron](../../strongs/g/g3397.md)?** we cannot [eidō](../../strongs/g/g1492.md) what he [laleō](../../strongs/g/g2980.md).

<a name="john_16_19"></a>John 16:19

Now [Iēsous](../../strongs/g/g2424.md) [ginōskō](../../strongs/g/g1097.md) that they were [thelō](../../strongs/g/g2309.md) to [erōtaō](../../strongs/g/g2065.md) him, and [eipon](../../strongs/g/g2036.md) unto them, **Do ye [zēteō](../../strongs/g/g2212.md) among [allēlōn](../../strongs/g/g240.md) of that I [eipon](../../strongs/g/g2036.md), A [mikron](../../strongs/g/g3397.md), and ye shall not [theōreō](../../strongs/g/g2334.md) me: and again, a [mikron](../../strongs/g/g3397.md), and ye shall [optanomai](../../strongs/g/g3700.md) me?**

<a name="john_16_20"></a>John 16:20

**[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, That ye shall [klaiō](../../strongs/g/g2799.md) and [thrēneō](../../strongs/g/g2354.md), but the [kosmos](../../strongs/g/g2889.md) shall [chairō](../../strongs/g/g5463.md): and ye shall be [lypeō](../../strongs/g/g3076.md), but your [lypē](../../strongs/g/g3077.md) shall be [ginomai](../../strongs/g/g1096.md) into [chara](../../strongs/g/g5479.md).**

<a name="john_16_21"></a>John 16:21

**A [gynē](../../strongs/g/g1135.md) when she is in [tiktō](../../strongs/g/g5088.md) hath [lypē](../../strongs/g/g3077.md), because her [hōra](../../strongs/g/g5610.md) is [erchomai](../../strongs/g/g2064.md): but as soon as she is [gennaō](../../strongs/g/g1080.md) of the [paidion](../../strongs/g/g3813.md), she [mnēmoneuō](../../strongs/g/g3421.md) no more the [thlipsis](../../strongs/g/g2347.md), for [chara](../../strongs/g/g5479.md) that an [anthrōpos](../../strongs/g/g444.md) is [gennaō](../../strongs/g/g1080.md) into the [kosmos](../../strongs/g/g2889.md).**

<a name="john_16_22"></a>John 16:22

**And ye now therefore have [lypē](../../strongs/g/g3077.md): but I will [optanomai](../../strongs/g/g3700.md) you again, and your [kardia](../../strongs/g/g2588.md) shall [chairō](../../strongs/g/g5463.md), and your [chara](../../strongs/g/g5479.md) [oudeis](../../strongs/g/g3762.md) [airō](../../strongs/g/g142.md) from you.**

<a name="john_16_23"></a>John 16:23

**And in that [hēmera](../../strongs/g/g2250.md) ye shall [erōtaō](../../strongs/g/g2065.md) me nothing. [amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, Whatsoever ye shall [aiteō](../../strongs/g/g154.md) the [patēr](../../strongs/g/g3962.md) in my [onoma](../../strongs/g/g3686.md), he will [didōmi](../../strongs/g/g1325.md) you.**

<a name="john_16_24"></a>John 16:24

**Hitherto have ye [aiteō](../../strongs/g/g154.md) nothing in my [onoma](../../strongs/g/g3686.md): [aiteō](../../strongs/g/g154.md), and ye shall [lambanō](../../strongs/g/g2983.md), that your [chara](../../strongs/g/g5479.md) may be [plēroō](../../strongs/g/g4137.md).**

<a name="john_16_25"></a>John 16:25

**These things have I [laleō](../../strongs/g/g2980.md) unto you in [paroimia](../../strongs/g/g3942.md): but the [hōra](../../strongs/g/g5610.md) [erchomai](../../strongs/g/g2064.md), when I shall no more [laleō](../../strongs/g/g2980.md) unto you in [paroimia](../../strongs/g/g3942.md), but I shall [anaggellō](../../strongs/g/g312.md) you [parrēsia](../../strongs/g/g3954.md) of the [patēr](../../strongs/g/g3962.md).**

<a name="john_16_26"></a>John 16:26

**At that [hēmera](../../strongs/g/g2250.md) ye shall [aiteō](../../strongs/g/g154.md) in my [onoma](../../strongs/g/g3686.md): and I [legō](../../strongs/g/g3004.md) not unto you, that I will [erōtaō](../../strongs/g/g2065.md) the [patēr](../../strongs/g/g3962.md) for you:**

<a name="john_16_27"></a>John 16:27

**For the [patēr](../../strongs/g/g3962.md) himself [phileō](../../strongs/g/g5368.md) you, because ye have [phileō](../../strongs/g/g5368.md) me, and have [pisteuō](../../strongs/g/g4100.md) that I [exerchomai](../../strongs/g/g1831.md) from [theos](../../strongs/g/g2316.md).**

<a name="john_16_28"></a>John 16:28

**I [exerchomai](../../strongs/g/g1831.md) from the [patēr](../../strongs/g/g3962.md), and am [erchomai](../../strongs/g/g2064.md) into the [kosmos](../../strongs/g/g2889.md): again, I [aphiēmi](../../strongs/g/g863.md) the [kosmos](../../strongs/g/g2889.md), and [poreuō](../../strongs/g/g4198.md) to the [patēr](../../strongs/g/g3962.md).**

<a name="john_16_29"></a>John 16:29

His [mathētēs](../../strongs/g/g3101.md) [legō](../../strongs/g/g3004.md) unto him, [ide](../../strongs/g/g2396.md), now [laleō](../../strongs/g/g2980.md) thou [parrēsia](../../strongs/g/g3954.md), and [legō](../../strongs/g/g3004.md) no [paroimia](../../strongs/g/g3942.md).

<a name="john_16_30"></a>John 16:30

Now are we sure that thou [eidō](../../strongs/g/g1492.md) all things, and [chreia](../../strongs/g/g5532.md) not that [tis](../../strongs/g/g5100.md) should [erōtaō](../../strongs/g/g2065.md) thee: by this we [pisteuō](../../strongs/g/g4100.md) that thou [exerchomai](../../strongs/g/g1831.md) from [theos](../../strongs/g/g2316.md).

<a name="john_16_31"></a>John 16:31

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) them, **Do ye now [pisteuō](../../strongs/g/g4100.md)?**

<a name="john_16_32"></a>John 16:32

**[idou](../../strongs/g/g2400.md), the [hōra](../../strongs/g/g5610.md) [erchomai](../../strongs/g/g2064.md), yea, is now [erchomai](../../strongs/g/g2064.md), that ye shall be [skorpizō](../../strongs/g/g4650.md), every man to his own, and shall [aphiēmi](../../strongs/g/g863.md) me [monos](../../strongs/g/g3441.md): and yet I am not [monos](../../strongs/g/g3441.md), because the [patēr](../../strongs/g/g3962.md) is with me.**

<a name="john_16_33"></a>John 16:33

**These things I have [laleō](../../strongs/g/g2980.md) unto you, that in me ye might have [eirēnē](../../strongs/g/g1515.md). In the [kosmos](../../strongs/g/g2889.md) ye shall have [thlipsis](../../strongs/g/g2347.md): but [tharseō](../../strongs/g/g2293.md); I have [nikaō](../../strongs/g/g3528.md) the [kosmos](../../strongs/g/g2889.md).**

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 15](john_15.md) - [John 17](john_17.md)