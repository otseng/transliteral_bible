# [John 10](https://www.blueletterbible.org/kjv/jhn/10/1/p0/rl1/s_1005001)

<a name="john_10_1"></a>John 10:1

**[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, He that [eiserchomai](../../strongs/g/g1525.md) not by the [thyra](../../strongs/g/g2374.md) into the [probaton](../../strongs/g/g4263.md) [aulē](../../strongs/g/g833.md), but [anabainō](../../strongs/g/g305.md) [allachothen](../../strongs/g/g237.md), [ekeinos](../../strongs/g/g1565.md) is a [kleptēs](../../strongs/g/g2812.md) and a [lēstēs](../../strongs/g/g3027.md).**

<a name="john_10_2"></a>John 10:2

**But he that [eiserchomai](../../strongs/g/g1525.md) by the [thyra](../../strongs/g/g2374.md) is the [poimēn](../../strongs/g/g4166.md) of the [probaton](../../strongs/g/g4263.md).**

<a name="john_10_3"></a>John 10:3

**To him the [thyrōros](../../strongs/g/g2377.md) [anoigō](../../strongs/g/g455.md); and the [probaton](../../strongs/g/g4263.md) [akouō](../../strongs/g/g191.md) his [phōnē](../../strongs/g/g5456.md): and he [kaleō](../../strongs/g/g2564.md) his own [probaton](../../strongs/g/g4263.md) by [onoma](../../strongs/g/g3686.md), and [exagō](../../strongs/g/g1806.md) them.**

<a name="john_10_4"></a>John 10:4

**And when he [ekballō](../../strongs/g/g1544.md) his own [probaton](../../strongs/g/g4263.md), he [poreuō](../../strongs/g/g4198.md) before them, and the [probaton](../../strongs/g/g4263.md) [akoloutheō](../../strongs/g/g190.md) him: for they [eidō](../../strongs/g/g1492.md) his [phōnē](../../strongs/g/g5456.md).**

<a name="john_10_5"></a>John 10:5

**And an [allotrios](../../strongs/g/g245.md) will they not [akoloutheō](../../strongs/g/g190.md), but will [pheugō](../../strongs/g/g5343.md) from him: for they [eidō](../../strongs/g/g1492.md) not the [phōnē](../../strongs/g/g5456.md) of [allotrios](../../strongs/g/g245.md).**

<a name="john_10_6"></a>John 10:6

This [paroimia](../../strongs/g/g3942.md) [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) unto them: but they [ginōskō](../../strongs/g/g1097.md) not what things they were which he [laleō](../../strongs/g/g2980.md) unto them.

<a name="john_10_7"></a>John 10:7

Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) unto them again, **[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, I am the [thyra](../../strongs/g/g2374.md) of the [probaton](../../strongs/g/g4263.md).**

<a name="john_10_8"></a>John 10:8

**All that ever [erchomai](../../strongs/g/g2064.md) before me are [kleptēs](../../strongs/g/g2812.md) and [lēstēs](../../strongs/g/g3027.md): but the [probaton](../../strongs/g/g4263.md) did not [akouō](../../strongs/g/g191.md) them.**

<a name="john_10_9"></a>John 10:9

**I am the [thyra](../../strongs/g/g2374.md): by me if [tis](../../strongs/g/g5100.md) [eiserchomai](../../strongs/g/g1525.md) in, he shall be [sōzō](../../strongs/g/g4982.md), and shall [eiserchomai](../../strongs/g/g1525.md) and [exerchomai](../../strongs/g/g1831.md), and [heuriskō](../../strongs/g/g2147.md) [nomē](../../strongs/g/g3542.md).**

<a name="john_10_10"></a>John 10:10

**The [kleptēs](../../strongs/g/g2812.md) [erchomai](../../strongs/g/g2064.md) not, but for [kleptō](../../strongs/g/g2813.md), and [thyō](../../strongs/g/g2380.md), and [apollymi](../../strongs/g/g622.md): I am [erchomai](../../strongs/g/g2064.md) that they might have [zōē](../../strongs/g/g2222.md), and that they might have [perissos](../../strongs/g/g4053.md).**

<a name="john_10_11"></a>John 10:11

**I am the [kalos](../../strongs/g/g2570.md) [poimēn](../../strongs/g/g4166.md): the [kalos](../../strongs/g/g2570.md) [poimēn](../../strongs/g/g4166.md) [tithēmi](../../strongs/g/g5087.md) his [psychē](../../strongs/g/g5590.md) for the [probaton](../../strongs/g/g4263.md).**

<a name="john_10_12"></a>John 10:12

**But he that is a [misthōtos](../../strongs/g/g3411.md), and not the [poimēn](../../strongs/g/g4166.md), whose own the [probaton](../../strongs/g/g4263.md) are not, [theōreō](../../strongs/g/g2334.md) the [lykos](../../strongs/g/g3074.md) [erchomai](../../strongs/g/g2064.md), and [aphiēmi](../../strongs/g/g863.md) the [probaton](../../strongs/g/g4263.md), and [pheugō](../../strongs/g/g5343.md): and the [lykos](../../strongs/g/g3074.md) [harpazō](../../strongs/g/g726.md) them, and [skorpizō](../../strongs/g/g4650.md) the [probaton](../../strongs/g/g4263.md).**

<a name="john_10_13"></a>John 10:13

**The [misthōtos](../../strongs/g/g3411.md) [pheugō](../../strongs/g/g5343.md), because he is a [misthōtos](../../strongs/g/g3411.md), and [melei](../../strongs/g/g3199.md) not for the [probaton](../../strongs/g/g4263.md).**

<a name="john_10_14"></a>John 10:14

**I am the [kalos](../../strongs/g/g2570.md) [poimēn](../../strongs/g/g4166.md), and [ginōskō](../../strongs/g/g1097.md) mine, and am [ginōskō](../../strongs/g/g1097.md) of mine.**

<a name="john_10_15"></a>John 10:15

**As the [patēr](../../strongs/g/g3962.md) [ginōskō](../../strongs/g/g1097.md) me, even so [ginōskō](../../strongs/g/g1097.md) I the [patēr](../../strongs/g/g3962.md): and I [tithēmi](../../strongs/g/g5087.md) my [psychē](../../strongs/g/g5590.md) for the [probaton](../../strongs/g/g4263.md).**

<a name="john_10_16"></a>John 10:16

**And other [probaton](../../strongs/g/g4263.md) I have, which are not of this [aulē](../../strongs/g/g833.md): them also I must [agō](../../strongs/g/g71.md), and they shall [akouō](../../strongs/g/g191.md) my [phōnē](../../strongs/g/g5456.md); and there shall be one [poimnē](../../strongs/g/g4167.md), and one [poimēn](../../strongs/g/g4166.md).**

<a name="john_10_17"></a>John 10:17

**Therefore doth my [patēr](../../strongs/g/g3962.md) [agapaō](../../strongs/g/g25.md) me, because I [tithēmi](../../strongs/g/g5087.md) my [psychē](../../strongs/g/g5590.md), that I might [lambanō](../../strongs/g/g2983.md) it again.**

<a name="john_10_18"></a>John 10:18

**[oudeis](../../strongs/g/g3762.md) [airō](../../strongs/g/g142.md) it from me, but I [tithēmi](../../strongs/g/g5087.md) it of myself. I have [exousia](../../strongs/g/g1849.md) to [tithēmi](../../strongs/g/g5087.md) it, and I have [exousia](../../strongs/g/g1849.md) to [lambanō](../../strongs/g/g2983.md) it again. This [entolē](../../strongs/g/g1785.md) have I [lambanō](../../strongs/g/g2983.md) of my [patēr](../../strongs/g/g3962.md).**

<a name="john_10_19"></a>John 10:19

There was a [schisma](../../strongs/g/g4978.md) therefore again among the [Ioudaios](../../strongs/g/g2453.md) for these [logos](../../strongs/g/g3056.md).

<a name="john_10_20"></a>John 10:20

And [polys](../../strongs/g/g4183.md) of them [legō](../../strongs/g/g3004.md), He hath a [daimonion](../../strongs/g/g1140.md), and is [mainomai](../../strongs/g/g3105.md); why [akouō](../../strongs/g/g191.md) ye him?

<a name="john_10_21"></a>John 10:21

Others [legō](../../strongs/g/g3004.md), These are not the [rhēma](../../strongs/g/g4487.md) of him that [daimonizomai](../../strongs/g/g1139.md). Can a [daimonion](../../strongs/g/g1140.md) [anoigō](../../strongs/g/g455.md) the [ophthalmos](../../strongs/g/g3788.md) of the [typhlos](../../strongs/g/g5185.md)?

<a name="john_10_22"></a>John 10:22

And it was at [Hierosolyma](../../strongs/g/g2414.md) the [egkainia](../../strongs/g/g1456.md), and it was [cheimōn](../../strongs/g/g5494.md).

<a name="john_10_23"></a>John 10:23

And [Iēsous](../../strongs/g/g2424.md) [peripateō](../../strongs/g/g4043.md) in the [hieron](../../strongs/g/g2411.md) in [Solomōn](../../strongs/g/g4672.md) [stoa](../../strongs/g/g4745.md).

<a name="john_10_24"></a>John 10:24

Then [kykloō](../../strongs/g/g2944.md) the [Ioudaios](../../strongs/g/g2453.md) him, and [legō](../../strongs/g/g3004.md) unto him, How long dost thou [airō](../../strongs/g/g142.md) us to [psychē](../../strongs/g/g5590.md)? If thou be the [Christos](../../strongs/g/g5547.md), [eipon](../../strongs/g/g2036.md) us [parrēsia](../../strongs/g/g3954.md).

<a name="john_10_25"></a>John 10:25

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) them, **I [eipon](../../strongs/g/g2036.md) you, and ye [pisteuō](../../strongs/g/g4100.md) not: the [ergon](../../strongs/g/g2041.md) that I [poieō](../../strongs/g/g4160.md) in my [patēr](../../strongs/g/g3962.md) [onoma](../../strongs/g/g3686.md), they [martyreō](../../strongs/g/g3140.md) of me.**

<a name="john_10_26"></a>John 10:26

**But ye [pisteuō](../../strongs/g/g4100.md) not, because ye are not of my [probaton](../../strongs/g/g4263.md), as I [eipon](../../strongs/g/g2036.md) unto you.**

<a name="john_10_27"></a>John 10:27

**My [probaton](../../strongs/g/g4263.md) [akouō](../../strongs/g/g191.md) my [phōnē](../../strongs/g/g5456.md), and I [ginōskō](../../strongs/g/g1097.md) them, and they [akoloutheō](../../strongs/g/g190.md) me:**

<a name="john_10_28"></a>John 10:28

**And I [didōmi](../../strongs/g/g1325.md) unto them [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md); and they shall [ou mē](../../strongs/g/g3364.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) [apollymi](../../strongs/g/g622.md), neither shall [tis](../../strongs/g/g5100.md) [harpazō](../../strongs/g/g726.md) them out of my [cheir](../../strongs/g/g5495.md).**

<a name="john_10_29"></a>John 10:29

**My [patēr](../../strongs/g/g3962.md), which [didōmi](../../strongs/g/g1325.md) me, is [meizōn](../../strongs/g/g3187.md) than all; and [oudeis](../../strongs/g/g3762.md) is able to [harpazō](../../strongs/g/g726.md) them out of my [patēr](../../strongs/g/g3962.md) [cheir](../../strongs/g/g5495.md).**

<a name="john_10_30"></a>John 10:30

**I and [patēr](../../strongs/g/g3962.md) are [heis](../../strongs/g/g1520.md).**

<a name="john_10_31"></a>John 10:31

Then the [Ioudaios](../../strongs/g/g2453.md) [bastazō](../../strongs/g/g941.md) [lithos](../../strongs/g/g3037.md) again to [lithazō](../../strongs/g/g3034.md) him.

<a name="john_10_32"></a>John 10:32

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) them, **[polys](../../strongs/g/g4183.md) [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md) have I [deiknyō](../../strongs/g/g1166.md) you from my [patēr](../../strongs/g/g3962.md); for which of those [ergon](../../strongs/g/g2041.md) do ye [lithazō](../../strongs/g/g3034.md) me?**

<a name="john_10_33"></a>John 10:33

The [Ioudaios](../../strongs/g/g2453.md) [apokrinomai](../../strongs/g/g611.md) him, [legō](../../strongs/g/g3004.md), For a [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md) we [lithazō](../../strongs/g/g3034.md) thee not; but for [blasphēmia](../../strongs/g/g988.md); and because that thou, being an [anthrōpos](../../strongs/g/g444.md), [poieō](../../strongs/g/g4160.md) thyself [theos](../../strongs/g/g2316.md).

<a name="john_10_34"></a>John 10:34

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) them, **Is it not [graphō](../../strongs/g/g1125.md) in your [nomos](../../strongs/g/g3551.md), I [eipon](../../strongs/g/g2036.md), Ye are [theos](../../strongs/g/g2316.md)?**

<a name="john_10_35"></a>John 10:35

**If he [eipon](../../strongs/g/g2036.md) them [theos](../../strongs/g/g2316.md), unto whom the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) [ginomai](../../strongs/g/g1096.md), and the [graphē](../../strongs/g/g1124.md) cannot be [lyō](../../strongs/g/g3089.md);**

<a name="john_10_36"></a>John 10:36

**[legō](../../strongs/g/g3004.md) ye of him, whom the [patēr](../../strongs/g/g3962.md) hath [hagiazō](../../strongs/g/g37.md), and [apostellō](../../strongs/g/g649.md) into the [kosmos](../../strongs/g/g2889.md), Thou [blasphēmeō](../../strongs/g/g987.md); because I [eipon](../../strongs/g/g2036.md), I am the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md)?**

<a name="john_10_37"></a>John 10:37

**If I [poieō](../../strongs/g/g4160.md) not the [ergon](../../strongs/g/g2041.md) of my [patēr](../../strongs/g/g3962.md), [pisteuō](../../strongs/g/g4100.md) me not.**

<a name="john_10_38"></a>John 10:38

**But if I [poieō](../../strongs/g/g4160.md), though ye [pisteuō](../../strongs/g/g4100.md) not me, [pisteuō](../../strongs/g/g4100.md) the [ergon](../../strongs/g/g2041.md): that ye may [ginōskō](../../strongs/g/g1097.md), and [pisteuō](../../strongs/g/g4100.md), that the [patēr](../../strongs/g/g3962.md) is in me, and I in him.**

<a name="john_10_39"></a>John 10:39

Therefore they [zēteō](../../strongs/g/g2212.md) again to [piazō](../../strongs/g/g4084.md) him: but he [exerchomai](../../strongs/g/g1831.md) out of their [cheir](../../strongs/g/g5495.md),

<a name="john_10_40"></a>John 10:40

And [aperchomai](../../strongs/g/g565.md) again beyond [Iordanēs](../../strongs/g/g2446.md) into the [topos](../../strongs/g/g5117.md) where [Iōannēs](../../strongs/g/g2491.md) at first [baptizō](../../strongs/g/g907.md); and there he [menō](../../strongs/g/g3306.md).

<a name="john_10_41"></a>John 10:41

And [polys](../../strongs/g/g4183.md) [erchomai](../../strongs/g/g2064.md) unto him, and [legō](../../strongs/g/g3004.md), [Iōannēs](../../strongs/g/g2491.md) [poieō](../../strongs/g/g4160.md) no [sēmeion](../../strongs/g/g4592.md): but all things that [Iōannēs](../../strongs/g/g2491.md) [eipon](../../strongs/g/g2036.md) of [toutou](../../strongs/g/g5127.md) were [alēthēs](../../strongs/g/g227.md).

<a name="john_10_42"></a>John 10:42

And [polys](../../strongs/g/g4183.md) [pisteuō](../../strongs/g/g4100.md) on him there.

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 9](john_9.md) - [John 11](john_11.md)