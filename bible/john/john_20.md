# [John 20](https://www.blueletterbible.org/kjv/jhn/20/1/rl1/s_1016001)

<a name="john_20_1"></a>John 20:1

The [mia](../../strongs/g/g3391.md) of the [sabbaton](../../strongs/g/g4521.md) [erchomai](../../strongs/g/g2064.md) [Maria](../../strongs/g/g3137.md) [Magdalēnē](../../strongs/g/g3094.md) [prōï](../../strongs/g/g4404.md), when it was yet [skotia](../../strongs/g/g4653.md), unto the [mnēmeion](../../strongs/g/g3419.md), and [blepō](../../strongs/g/g991.md) the [lithos](../../strongs/g/g3037.md) [airō](../../strongs/g/g142.md) from the [mnēmeion](../../strongs/g/g3419.md).

<a name="john_20_2"></a>John 20:2

Then she [trechō](../../strongs/g/g5143.md), and [erchomai](../../strongs/g/g2064.md) to [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md), and to the other [mathētēs](../../strongs/g/g3101.md), whom [Iēsous](../../strongs/g/g2424.md) [phileō](../../strongs/g/g5368.md), and [legō](../../strongs/g/g3004.md) unto them, They have [airō](../../strongs/g/g142.md) the [kyrios](../../strongs/g/g2962.md) out of the [mnēmeion](../../strongs/g/g3419.md), and we [eidō](../../strongs/g/g1492.md) not where they have [tithēmi](../../strongs/g/g5087.md) him.

<a name="john_20_3"></a>John 20:3

[Petros](../../strongs/g/g4074.md) therefore [exerchomai](../../strongs/g/g1831.md), and that other [mathētēs](../../strongs/g/g3101.md), and [erchomai](../../strongs/g/g2064.md) to the [mnēmeion](../../strongs/g/g3419.md).

<a name="john_20_4"></a>John 20:4

So they [trechō](../../strongs/g/g5143.md) both [homou](../../strongs/g/g3674.md): and the other [mathētēs](../../strongs/g/g3101.md) did [protrechō](../../strongs/g/g4390.md) [tachion](../../strongs/g/g5032.md) [Petros](../../strongs/g/g4074.md), and [erchomai](../../strongs/g/g2064.md) [prōtos](../../strongs/g/g4413.md) to the [mnēmeion](../../strongs/g/g3419.md).

<a name="john_20_5"></a>John 20:5

And he [parakyptō](../../strongs/g/g3879.md), [blepō](../../strongs/g/g991.md) the [othonion](../../strongs/g/g3608.md) [keimai](../../strongs/g/g2749.md); yet [eiserchomai](../../strongs/g/g1525.md) he not in.

<a name="john_20_6"></a>John 20:6

Then [erchomai](../../strongs/g/g2064.md) [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) [akoloutheō](../../strongs/g/g190.md) him, and [eiserchomai](../../strongs/g/g1525.md) into the [mnēmeion](../../strongs/g/g3419.md), and [theōreō](../../strongs/g/g2334.md) the [othonion](../../strongs/g/g3608.md) [keimai](../../strongs/g/g2749.md),

<a name="john_20_7"></a>John 20:7

And the [soudarion](../../strongs/g/g4676.md), that was about his [kephalē](../../strongs/g/g2776.md), not [keimai](../../strongs/g/g2749.md) with the [othonion](../../strongs/g/g3608.md), but [entylissō](../../strongs/g/g1794.md) in a [topos](../../strongs/g/g5117.md) [heis](../../strongs/g/g1520.md) [chōris](../../strongs/g/g5565.md).

<a name="john_20_8"></a>John 20:8

Then [eiserchomai](../../strongs/g/g1525.md) also that other [mathētēs](../../strongs/g/g3101.md), which [erchomai](../../strongs/g/g2064.md) [prōtos](../../strongs/g/g4413.md) to the [mnēmeion](../../strongs/g/g3419.md), and he [eidō](../../strongs/g/g1492.md), and [pisteuō](../../strongs/g/g4100.md).

<a name="john_20_9"></a>John 20:9

For as yet they [eidō](../../strongs/g/g1492.md) not the [graphē](../../strongs/g/g1124.md), that he must [anistēmi](../../strongs/g/g450.md) from the [nekros](../../strongs/g/g3498.md).

<a name="john_20_10"></a>John 20:10

Then the [mathētēs](../../strongs/g/g3101.md)s [aperchomai](../../strongs/g/g565.md) [palin](../../strongs/g/g3825.md) unto [heautou](../../strongs/g/g1438.md).

<a name="john_20_11"></a>John 20:11

But [Maria](../../strongs/g/g3137.md) [histēmi](../../strongs/g/g2476.md) without at the [mnēmeion](../../strongs/g/g3419.md) [klaiō](../../strongs/g/g2799.md): and as she [klaiō](../../strongs/g/g2799.md), she [parakyptō](../../strongs/g/g3879.md), into the [mnēmeion](../../strongs/g/g3419.md),

<a name="john_20_12"></a>John 20:12

And [theōreō](../../strongs/g/g2334.md) two [aggelos](../../strongs/g/g32.md) in [leukos](../../strongs/g/g3022.md) [kathezomai](../../strongs/g/g2516.md), the one at the [kephalē](../../strongs/g/g2776.md), and the other at the [pous](../../strongs/g/g4228.md), where the [sōma](../../strongs/g/g4983.md) of [Iēsous](../../strongs/g/g2424.md) had [keimai](../../strongs/g/g2749.md).

<a name="john_20_13"></a>John 20:13

And they [legō](../../strongs/g/g3004.md) unto her, [gynē](../../strongs/g/g1135.md), why [klaiō](../../strongs/g/g2799.md) thou? She [legō](../../strongs/g/g3004.md) unto them, Because they have [airō](../../strongs/g/g142.md) my [kyrios](../../strongs/g/g2962.md), and I [eidō](../../strongs/g/g1492.md) not where they have [tithēmi](../../strongs/g/g5087.md) him.

<a name="john_20_14"></a>John 20:14

And when she had thus [eipon](../../strongs/g/g2036.md), she [strephō](../../strongs/g/g4762.md) herself back, and [theōreō](../../strongs/g/g2334.md) [Iēsous](../../strongs/g/g2424.md) [histēmi](../../strongs/g/g2476.md), and [eidō](../../strongs/g/g1492.md) not that it was [Iēsous](../../strongs/g/g2424.md).

<a name="john_20_15"></a>John 20:15

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto her, **[gynē](../../strongs/g/g1135.md), why [klaiō](../../strongs/g/g2799.md)? whom [zēteō](../../strongs/g/g2212.md) thou?** She, [dokeō](../../strongs/g/g1380.md) him to be the [kēpouros](../../strongs/g/g2780.md), [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), if thou have [bastazō](../../strongs/g/g941.md) him hence, [eipon](../../strongs/g/g2036.md) me where thou hast [tithēmi](../../strongs/g/g5087.md) him, and I will [airō](../../strongs/g/g142.md) him.

<a name="john_20_16"></a>John 20:16

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto her, **[Maria](../../strongs/g/g3137.md)**. She [strephō](../../strongs/g/g4762.md) herself, and [legō](../../strongs/g/g3004.md) unto him, [rhabbouni](../../strongs/g/g4462.md); which is to [legō](../../strongs/g/g3004.md), [didaskalos](../../strongs/g/g1320.md).

<a name="john_20_17"></a>John 20:17

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto her, **[haptomai](../../strongs/g/g680.md) me not; for I am not yet [anabainō](../../strongs/g/g305.md) to my [patēr](../../strongs/g/g3962.md): but [poreuō](../../strongs/g/g4198.md) to my [adelphos](../../strongs/g/g80.md), and [eipon](../../strongs/g/g2036.md) unto them, I [anabainō](../../strongs/g/g305.md) unto my [patēr](../../strongs/g/g3962.md), and your [patēr](../../strongs/g/g3962.md); and my [theos](../../strongs/g/g2316.md), and your [theos](../../strongs/g/g2316.md).**

<a name="john_20_18"></a>John 20:18

[Maria](../../strongs/g/g3137.md) [Magdalēnē](../../strongs/g/g3094.md) [erchomai](../../strongs/g/g2064.md) and [apaggellō](../../strongs/g/g518.md) the [mathētēs](../../strongs/g/g3101.md) that she had [horaō](../../strongs/g/g3708.md) the [kyrios](../../strongs/g/g2962.md), and he had [eipon](../../strongs/g/g2036.md) these things unto her.

<a name="john_20_19"></a>John 20:19

Then the same [hēmera](../../strongs/g/g2250.md) at [opsios](../../strongs/g/g3798.md), being the first of the [sabbaton](../../strongs/g/g4521.md), when the [thyra](../../strongs/g/g2374.md) were [kleiō](../../strongs/g/g2808.md) where the [mathētēs](../../strongs/g/g3101.md)s were [synagō](../../strongs/g/g4863.md) for [phobos](../../strongs/g/g5401.md) of the [Ioudaios](../../strongs/g/g2453.md), [erchomai](../../strongs/g/g2064.md) [Iēsous](../../strongs/g/g2424.md) and [histēmi](../../strongs/g/g2476.md) in the [mesos](../../strongs/g/g3319.md), and [legō](../../strongs/g/g3004.md) unto them, **[eirēnē](../../strongs/g/g1515.md) unto you.**

<a name="john_20_20"></a>John 20:20

And when he had so [eipon](../../strongs/g/g2036.md), he [deiknyō](../../strongs/g/g1166.md) unto them his [cheir](../../strongs/g/g5495.md) and his [pleura](../../strongs/g/g4125.md). Then were the [mathētēs](../../strongs/g/g3101.md) [chairō](../../strongs/g/g5463.md), when they [eidō](../../strongs/g/g1492.md) the [kyrios](../../strongs/g/g2962.md).

<a name="john_20_21"></a>John 20:21

Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) to them again, **[eirēnē](../../strongs/g/g1515.md) unto you: as [patēr](../../strongs/g/g3962.md) hath [apostellō](../../strongs/g/g649.md) me, even so [pempō](../../strongs/g/g3992.md) I you.**

<a name="john_20_22"></a>John 20:22

And when he had [eipon](../../strongs/g/g2036.md) this, he [emphysaō](../../strongs/g/g1720.md), and [legō](../../strongs/g/g3004.md) unto them, **[lambanō](../../strongs/g/g2983.md) ye the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md):** [^1]

<a name="john_20_23"></a>John 20:23

**Whose soever [hamartia](../../strongs/g/g266.md) ye [aphiēmi](../../strongs/g/g863.md), they are [aphiēmi](../../strongs/g/g863.md) unto them; whose soever ye [krateō](../../strongs/g/g2902.md), they are [krateō](../../strongs/g/g2902.md).**

<a name="john_20_24"></a>John 20:24

But [Thōmas](../../strongs/g/g2381.md), one of the [dōdeka](../../strongs/g/g1427.md), [legō](../../strongs/g/g3004.md) [didymos](../../strongs/g/g1324.md), was not with them when [Iēsous](../../strongs/g/g2424.md) [erchomai](../../strongs/g/g2064.md).

<a name="john_20_25"></a>John 20:25

The other [mathētēs](../../strongs/g/g3101.md)s therefore [legō](../../strongs/g/g3004.md) unto him, We have [horaō](../../strongs/g/g3708.md) the [kyrios](../../strongs/g/g2962.md). But he [eipon](../../strongs/g/g2036.md) unto them, Except I shall [eidō](../../strongs/g/g1492.md) in his [cheir](../../strongs/g/g5495.md) the [typos](../../strongs/g/g5179.md) of the [hēlos](../../strongs/g/g2247.md), and [ballō](../../strongs/g/g906.md) my [daktylos](../../strongs/g/g1147.md) into the [typos](../../strongs/g/g5179.md) of the [hēlos](../../strongs/g/g2247.md), and [ballō](../../strongs/g/g906.md) my [cheir](../../strongs/g/g5495.md) into his [pleura](../../strongs/g/g4125.md), I will not [pisteuō](../../strongs/g/g4100.md).

<a name="john_20_26"></a>John 20:26

And after eight [hēmera](../../strongs/g/g2250.md) again his [mathētēs](../../strongs/g/g3101.md) were within, and [Thōmas](../../strongs/g/g2381.md) with them: [erchomai](../../strongs/g/g2064.md) [Iēsous](../../strongs/g/g2424.md), the [thyra](../../strongs/g/g2374.md) being [kleiō](../../strongs/g/g2808.md), and [histēmi](../../strongs/g/g2476.md) in the [mesos](../../strongs/g/g3319.md), and [eipon](../../strongs/g/g2036.md), **[eirēnē](../../strongs/g/g1515.md) be unto you.**

<a name="john_20_27"></a>John 20:27

Then [legō](../../strongs/g/g3004.md) he to [Thōmas](../../strongs/g/g2381.md), **[pherō](../../strongs/g/g5342.md) hither thy [daktylos](../../strongs/g/g1147.md), and [eidō](../../strongs/g/g1492.md) my [cheir](../../strongs/g/g5495.md); and [pherō](../../strongs/g/g5342.md) thy [cheir](../../strongs/g/g5495.md), and [ballō](../../strongs/g/g906.md) it into my [pleura](../../strongs/g/g4125.md): and be not [apistos](../../strongs/g/g571.md), but [pistos](../../strongs/g/g4103.md).**

<a name="john_20_28"></a>John 20:28

And [Thōmas](../../strongs/g/g2381.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, My [kyrios](../../strongs/g/g2962.md) and my [theos](../../strongs/g/g2316.md).

<a name="john_20_29"></a>John 20:29

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, **[Thōmas](../../strongs/g/g2381.md), because thou hast [horaō](../../strongs/g/g3708.md) me, thou hast [pisteuō](../../strongs/g/g4100.md): [makarios](../../strongs/g/g3107.md) are they that have not [eidō](../../strongs/g/g1492.md), and yet have [pisteuō](../../strongs/g/g4100.md).**

<a name="john_20_30"></a>John 20:30

And [polys](../../strongs/g/g4183.md) other [sēmeion](../../strongs/g/g4592.md) [men](../../strongs/g/g3303.md) [poieō](../../strongs/g/g4160.md) [Iēsous](../../strongs/g/g2424.md) [enōpion](../../strongs/g/g1799.md) of his [mathētēs](../../strongs/g/g3101.md), which are not [graphō](../../strongs/g/g1125.md) in this [biblion](../../strongs/g/g975.md):

<a name="john_20_31"></a>John 20:31

But these are [graphō](../../strongs/g/g1125.md), that ye might [pisteuō](../../strongs/g/g4100.md) that [Iēsous](../../strongs/g/g2424.md) is [Christos](../../strongs/g/g5547.md) [huios](../../strongs/g/g5207.md) [theos](../../strongs/g/g2316.md); and that [pisteuō](../../strongs/g/g4100.md) ye might have [zōē](../../strongs/g/g2222.md) through his [onoma](../../strongs/g/g3686.md).

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 19](john_19.md) - [John 21](john_21.md)

---

[^1]: [John 20:22 Commentary](../../commentary/john/john_20_commentary.md#john_20_22)
