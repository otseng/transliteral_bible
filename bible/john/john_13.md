# [John 13](https://www.blueletterbible.org/kjv/jhn/13/1/p0/rl1/s_1005001)

<a name="john_13_1"></a>John 13:1

Now before the [heortē](../../strongs/g/g1859.md) of the [pascha](../../strongs/g/g3957.md), when [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) that his [hōra](../../strongs/g/g5610.md) was [erchomai](../../strongs/g/g2064.md) that he should [metabainō](../../strongs/g/g3327.md) out of this [kosmos](../../strongs/g/g2889.md) unto the [patēr](../../strongs/g/g3962.md), having [agapaō](../../strongs/g/g25.md) his own which were in the [kosmos](../../strongs/g/g2889.md), he [agapaō](../../strongs/g/g25.md) them unto the [telos](../../strongs/g/g5056.md).

<a name="john_13_2"></a>John 13:2

And [deipnon](../../strongs/g/g1173.md) being [ginomai](../../strongs/g/g1096.md), the [diabolos](../../strongs/g/g1228.md) having now [ballō](../../strongs/g/g906.md) into the [kardia](../../strongs/g/g2588.md) of [Ioudas](../../strongs/g/g2455.md) [Iskariōth](../../strongs/g/g2469.md), [Simōn](../../strongs/g/g4613.md), to [paradidōmi](../../strongs/g/g3860.md) him;

<a name="john_13_3"></a>John 13:3

[Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) that the [patēr](../../strongs/g/g3962.md) had [didōmi](../../strongs/g/g1325.md) all things into his [cheir](../../strongs/g/g5495.md), and that he was [exerchomai](../../strongs/g/g1831.md) from [theos](../../strongs/g/g2316.md), and [hypagō](../../strongs/g/g5217.md) to [theos](../../strongs/g/g2316.md);

<a name="john_13_4"></a>John 13:4

He [egeirō](../../strongs/g/g1453.md) from [deipnon](../../strongs/g/g1173.md), and [tithēmi](../../strongs/g/g5087.md) his [himation](../../strongs/g/g2440.md); and [lambanō](../../strongs/g/g2983.md) a [lention](../../strongs/g/g3012.md), and [diazōnnymi](../../strongs/g/g1241.md) himself.

<a name="john_13_5"></a>John 13:5

After that he [ballō](../../strongs/g/g906.md) [hydōr](../../strongs/g/g5204.md) into a [niptēr](../../strongs/g/g3537.md), and [archomai](../../strongs/g/g756.md) to [niptō](../../strongs/g/g3538.md) the [mathētēs](../../strongs/g/g3101.md) [pous](../../strongs/g/g4228.md), and to [ekmassō](../../strongs/g/g1591.md) with the [lention](../../strongs/g/g3012.md) wherewith he was [diazōnnymi](../../strongs/g/g1241.md).

<a name="john_13_6"></a>John 13:6

Then [erchomai](../../strongs/g/g2064.md) he to [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md): and [ekeinos](../../strongs/g/g1565.md) [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), dost thou [niptō](../../strongs/g/g3538.md) my [pous](../../strongs/g/g4228.md)?

<a name="john_13_7"></a>John 13:7

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, **What I [poieō](../../strongs/g/g4160.md) thou [eidō](../../strongs/g/g1492.md) not now; but thou shalt [ginōskō](../../strongs/g/g1097.md) hereafter.**

<a name="john_13_8"></a>John 13:8

[Petros](../../strongs/g/g4074.md) [legō](../../strongs/g/g3004.md) unto him, **Thou shalt [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) [ou mē](../../strongs/g/g3364.md) [niptō](../../strongs/g/g3538.md) my [pous](../../strongs/g/g4228.md). [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) him, If I [niptō](../../strongs/g/g3538.md) thee not, thou hast no [meros](../../strongs/g/g3313.md) with me.**

<a name="john_13_9"></a>John 13:9

[Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), not my [pous](../../strongs/g/g4228.md) only, but also [cheir](../../strongs/g/g5495.md) and [kephalē](../../strongs/g/g2776.md).

<a name="john_13_10"></a>John 13:10

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) to him, **He that is [louō](../../strongs/g/g3068.md) [chreia](../../strongs/g/g5532.md) not save to [niptō](../../strongs/g/g3538.md) [pous](../../strongs/g/g4228.md), but is [katharos](../../strongs/g/g2513.md) [holos](../../strongs/g/g3650.md): and ye are [katharos](../../strongs/g/g2513.md), but not all.**

<a name="john_13_11"></a>John 13:11

For he [eidō](../../strongs/g/g1492.md) who should [paradidōmi](../../strongs/g/g3860.md) him; therefore [eipon](../../strongs/g/g2036.md) he, **Ye are not all [katharos](../../strongs/g/g2513.md).**

<a name="john_13_12"></a>John 13:12

So after he had [niptō](../../strongs/g/g3538.md) their [pous](../../strongs/g/g4228.md), and had [lambanō](../../strongs/g/g2983.md) his [himation](../../strongs/g/g2440.md), and was [anapiptō](../../strongs/g/g377.md) again, he [eipon](../../strongs/g/g2036.md) unto them, **[ginōskō](../../strongs/g/g1097.md) ye what I have [poieō](../../strongs/g/g4160.md) to you?**

<a name="john_13_13"></a>John 13:13

**Ye [phōneō](../../strongs/g/g5455.md) me [didaskalos](../../strongs/g/g1320.md) and [kyrios](../../strongs/g/g2962.md): and ye [legō](../../strongs/g/g3004.md) [kalōs](../../strongs/g/g2573.md); for so I am.**

<a name="john_13_14"></a>John 13:14

**If I then, [kyrios](../../strongs/g/g2962.md) and [didaskalos](../../strongs/g/g1320.md), have [niptō](../../strongs/g/g3538.md) your [pous](../../strongs/g/g4228.md); ye also [opheilō](../../strongs/g/g3784.md) to [niptō](../../strongs/g/g3538.md) [allēlōn](../../strongs/g/g240.md) [pous](../../strongs/g/g4228.md).**

<a name="john_13_15"></a>John 13:15

**For I have [didōmi](../../strongs/g/g1325.md) you an [hypodeigma](../../strongs/g/g5262.md), that ye should [poieō](../../strongs/g/g4160.md) as I have [poieō](../../strongs/g/g4160.md) to you.**

<a name="john_13_16"></a>John 13:16

**[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, The [doulos](../../strongs/g/g1401.md) is not [meizōn](../../strongs/g/g3187.md) than his [kyrios](../../strongs/g/g2962.md); neither [apostolos](../../strongs/g/g652.md) [meizōn](../../strongs/g/g3187.md) than he that [pempō](../../strongs/g/g3992.md) him.**

<a name="john_13_17"></a>John 13:17

**If ye [eidō](../../strongs/g/g1492.md) these things, [makarios](../../strongs/g/g3107.md) are ye if ye [poieō](../../strongs/g/g4160.md) them.**

<a name="john_13_18"></a>John 13:18

**I [legō](../../strongs/g/g3004.md) not of you all: I [eidō](../../strongs/g/g1492.md) whom I have [eklegomai](../../strongs/g/g1586.md): but that the [graphē](../../strongs/g/g1124.md) may be [plēroō](../../strongs/g/g4137.md), He that [trōgō](../../strongs/g/g5176.md) [artos](../../strongs/g/g740.md) with me hath [epairō](../../strongs/g/g1869.md) his [pterna](../../strongs/g/g4418.md) against me.**

<a name="john_13_19"></a>John 13:19

**Now I [legō](../../strongs/g/g3004.md) you before it [ginomai](../../strongs/g/g1096.md), that, when it is [ginomai](../../strongs/g/g1096.md), ye may [pisteuō](../../strongs/g/g4100.md) that I am.**

<a name="john_13_20"></a>John 13:20

**[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, He that [lambanō](../../strongs/g/g2983.md) whomsoever I [pempō](../../strongs/g/g3992.md) [lambanō](../../strongs/g/g2983.md) me; and he that [lambanō](../../strongs/g/g2983.md) me [lambanō](../../strongs/g/g2983.md) him that [pempō](../../strongs/g/g3992.md) me.**

<a name="john_13_21"></a>John 13:21

When [Iēsous](../../strongs/g/g2424.md) had thus [eipon](../../strongs/g/g2036.md), he was [tarassō](../../strongs/g/g5015.md) in [pneuma](../../strongs/g/g4151.md), and [martyreō](../../strongs/g/g3140.md), and [eipon](../../strongs/g/g2036.md), **[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, that one of you shall [paradidōmi](../../strongs/g/g3860.md) me.**

<a name="john_13_22"></a>John 13:22

Then the [mathētēs](../../strongs/g/g3101.md) [blepō](../../strongs/g/g991.md) on [allēlōn](../../strongs/g/g240.md), [aporeō](../../strongs/g/g639.md) of whom he [legō](../../strongs/g/g3004.md).

<a name="john_13_23"></a>John 13:23

Now there was [anakeimai](../../strongs/g/g345.md) on [Iēsous](../../strongs/g/g2424.md)' [kolpos](../../strongs/g/g2859.md) one of his [mathētēs](../../strongs/g/g3101.md), whom [Iēsous](../../strongs/g/g2424.md) [agapaō](../../strongs/g/g25.md).

<a name="john_13_24"></a>John 13:24

[Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) therefore [neuō](../../strongs/g/g3506.md) to him, that he should [pynthanomai](../../strongs/g/g4441.md) who it should be of whom he [legō](../../strongs/g/g3004.md).

<a name="john_13_25"></a>John 13:25

He then [epipiptō](../../strongs/g/g1968.md) on [Iēsous](../../strongs/g/g2424.md)' [stēthos](../../strongs/g/g4738.md) [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), who is it?

<a name="john_13_26"></a>John 13:26

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md), **He it is, to whom I shall [epididōmi](../../strongs/g/g1929.md) a [psōmion](../../strongs/g/g5596.md), when I have [baptō](../../strongs/g/g911.md) it.** And when he had [embaptō](../../strongs/g/g1686.md) the [psōmion](../../strongs/g/g5596.md), he [didōmi](../../strongs/g/g1325.md) it to [Ioudas](../../strongs/g/g2455.md) [Iskariōth](../../strongs/g/g2469.md), of [Simōn](../../strongs/g/g4613.md).

<a name="john_13_27"></a>John 13:27

And after the [psōmion](../../strongs/g/g5596.md) [tote](../../strongs/g/g5119.md) [Satanas](../../strongs/g/g4567.md) [eiserchomai](../../strongs/g/g1525.md) into him. Then [legō](../../strongs/g/g3004.md) [Iēsous](../../strongs/g/g2424.md) unto him, **That thou [poieō](../../strongs/g/g4160.md), [poieō](../../strongs/g/g4160.md) [tachion](../../strongs/g/g5032.md).**

<a name="john_13_28"></a>John 13:28

Now [oudeis](../../strongs/g/g3762.md) at the [anakeimai](../../strongs/g/g345.md) [ginōskō](../../strongs/g/g1097.md) for what intent he [eipon](../../strongs/g/g2036.md) this unto him.

<a name="john_13_29"></a>John 13:29

For some [dokeō](../../strongs/g/g1380.md), because [Ioudas](../../strongs/g/g2455.md) had the [glōssokomon](../../strongs/g/g1101.md), that [Iēsous](../../strongs/g/g2424.md) had [legō](../../strongs/g/g3004.md) unto him, [agorazō](../../strongs/g/g59.md) that we have [chreia](../../strongs/g/g5532.md) against the [heortē](../../strongs/g/g1859.md); or, that he should [didōmi](../../strongs/g/g1325.md) something to the [ptōchos](../../strongs/g/g4434.md).

<a name="john_13_30"></a>John 13:30

He then having [lambanō](../../strongs/g/g2983.md) the [psōmion](../../strongs/g/g5596.md) [exerchomai](../../strongs/g/g1831.md) [eutheōs](../../strongs/g/g2112.md): and it was [nyx](../../strongs/g/g3571.md).

<a name="john_13_31"></a>John 13:31

Therefore, when he was [exerchomai](../../strongs/g/g1831.md), [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md), **Now is the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [doxazō](../../strongs/g/g1392.md), and [theos](../../strongs/g/g2316.md) is [doxazō](../../strongs/g/g1392.md) in him.**

<a name="john_13_32"></a>John 13:32

**If [theos](../../strongs/g/g2316.md) be [doxazō](../../strongs/g/g1392.md) in him, [theos](../../strongs/g/g2316.md) shall also [doxazō](../../strongs/g/g1392.md) him in himself, and shall [euthys](../../strongs/g/g2117.md) [doxazō](../../strongs/g/g1392.md) him.**

<a name="john_13_33"></a>John 13:33

**[teknion](../../strongs/g/g5040.md), yet a [mikron](../../strongs/g/g3397.md) I am with you. Ye shall [zēteō](../../strongs/g/g2212.md) me: and as I [eipon](../../strongs/g/g2036.md) unto the [Ioudaios](../../strongs/g/g2453.md), Whither I [hypagō](../../strongs/g/g5217.md), ye cannot [erchomai](../../strongs/g/g2064.md); so now I [legō](../../strongs/g/g3004.md) to you.**

<a name="john_13_34"></a>John 13:34

**A [kainos](../../strongs/g/g2537.md) [entolē](../../strongs/g/g1785.md) I [didōmi](../../strongs/g/g1325.md) unto you, That ye [agapaō](../../strongs/g/g25.md) [allēlōn](../../strongs/g/g240.md); as I have [agapaō](../../strongs/g/g25.md) you, that ye also [agapaō](../../strongs/g/g25.md) [allēlōn](../../strongs/g/g240.md).**

<a name="john_13_35"></a>John 13:35

**By [toutō](../../strongs/g/g5129.md) shall [pas](../../strongs/g/g3956.md) [ginōskō](../../strongs/g/g1097.md) that ye are my [mathētēs](../../strongs/g/g3101.md), if ye have [agapē](../../strongs/g/g26.md) [en](../../strongs/g/g1722.md) [allēlōn](../../strongs/g/g240.md).**

<a name="john_13_36"></a>John 13:36

[Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), whither [hypagō](../../strongs/g/g5217.md) thou? [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) him, **Whither I [hypagō](../../strongs/g/g5217.md), thou canst not [akoloutheō](../../strongs/g/g190.md) me now; but thou shalt [akoloutheō](../../strongs/g/g190.md) me [hysteron](../../strongs/g/g5305.md).**

<a name="john_13_37"></a>John 13:37

[Petros](../../strongs/g/g4074.md) [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), why cannot I [akoloutheō](../../strongs/g/g190.md) thee now? I will [tithēmi](../../strongs/g/g5087.md) my [psychē](../../strongs/g/g5590.md) for thy sake.

<a name="john_13_38"></a>John 13:38

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) him, **Wilt thou [tithēmi](../../strongs/g/g5087.md) thy [psychē](../../strongs/g/g5590.md) for my sake? [amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto thee, The [alektōr](../../strongs/g/g220.md) shall not [phōneō](../../strongs/g/g5455.md), till thou hast [aparneomai](../../strongs/g/g533.md) me thrice.**

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 12](john_12.md) - [John 14](john_14.md)