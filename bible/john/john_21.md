# [John 21](https://www.blueletterbible.org/kjv/jhn/21/1/rl1/s_1018001)

<a name="john_21_1"></a>John 21:1

After these things [Iēsous](../../strongs/g/g2424.md) [phaneroō](../../strongs/g/g5319.md) himself again to the [mathētēs](../../strongs/g/g3101.md) at the [thalassa](../../strongs/g/g2281.md) of [Tiberias](../../strongs/g/g5085.md); and [houtō(s)](../../strongs/g/g3779.md) [phaneroō](../../strongs/g/g5319.md) he.

<a name="john_21_2"></a>John 21:2

There were [homou](../../strongs/g/g3674.md) [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md), and [Thōmas](../../strongs/g/g2381.md) [legō](../../strongs/g/g3004.md) [didymos](../../strongs/g/g1324.md), and [Nathanaēl](../../strongs/g/g3482.md) of [Kana](../../strongs/g/g2580.md) in [Galilaia](../../strongs/g/g1056.md), and the of [Zebedaios](../../strongs/g/g2199.md), and two other of his [mathētēs](../../strongs/g/g3101.md).

<a name="john_21_3"></a>John 21:3

[Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) [legō](../../strongs/g/g3004.md) unto them, I [hypagō](../../strongs/g/g5217.md) [halieuō](../../strongs/g/g232.md). They [legō](../../strongs/g/g3004.md) unto him, We also [erchomai](../../strongs/g/g2064.md) with thee. They [exerchomai](../../strongs/g/g1831.md), and [anabainō](../../strongs/g/g305.md) into a [ploion](../../strongs/g/g4143.md) [euthys](../../strongs/g/g2117.md); and that [nyx](../../strongs/g/g3571.md) they [piazō](../../strongs/g/g4084.md) [oudeis](../../strongs/g/g3762.md).

<a name="john_21_4"></a>John 21:4

But when the [prōïa](../../strongs/g/g4405.md) was now [ginomai](../../strongs/g/g1096.md), [Iēsous](../../strongs/g/g2424.md) [histēmi](../../strongs/g/g2476.md) on the [aigialos](../../strongs/g/g123.md): but the [mathētēs](../../strongs/g/g3101.md) [eidō](../../strongs/g/g1492.md) not that it was [Iēsous](../../strongs/g/g2424.md).

<a name="john_21_5"></a>John 21:5

Then [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, **[paidion](../../strongs/g/g3813.md), have ye [mē tis](../../strongs/g/g3387.md) [prosphagion](../../strongs/g/g4371.md)?** They [apokrinomai](../../strongs/g/g611.md) him, No.

<a name="john_21_6"></a>John 21:6

And he [eipon](../../strongs/g/g2036.md) unto them, **[ballō](../../strongs/g/g906.md) the [diktyon](../../strongs/g/g1350.md) on the [dexios](../../strongs/g/g1188.md) [meros](../../strongs/g/g3313.md) of the [ploion](../../strongs/g/g4143.md), and ye shall [heuriskō](../../strongs/g/g2147.md).** They [ballō](../../strongs/g/g906.md) therefore, and now they were not able to [helkō](../../strongs/g/g1670.md) it for the [plēthos](../../strongs/g/g4128.md) of [ichthys](../../strongs/g/g2486.md).

<a name="john_21_7"></a>John 21:7

Therefore that [mathētēs](../../strongs/g/g3101.md) whom [Iēsous](../../strongs/g/g2424.md) [agapaō](../../strongs/g/g25.md) [legō](../../strongs/g/g3004.md) unto [Petros](../../strongs/g/g4074.md), It is the [kyrios](../../strongs/g/g2962.md). Now when [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) [akouō](../../strongs/g/g191.md) that it was the [kyrios](../../strongs/g/g2962.md), he [diazōnnymi](../../strongs/g/g1241.md) [ependytēs](../../strongs/g/g1903.md), (for he was [gymnos](../../strongs/g/g1131.md),) and did [ballō](../../strongs/g/g906.md) himself into the [thalassa](../../strongs/g/g2281.md).

<a name="john_21_8"></a>John 21:8

And the other [mathētēs](../../strongs/g/g3101.md) [erchomai](../../strongs/g/g2064.md) in a [ploiarion](../../strongs/g/g4142.md); (for they were not [makran](../../strongs/g/g3112.md) from [gē](../../strongs/g/g1093.md), but as it were two hundred [pēchys](../../strongs/g/g4083.md),) [syrō](../../strongs/g/g4951.md) the [diktyon](../../strongs/g/g1350.md) with [ichthys](../../strongs/g/g2486.md).

<a name="john_21_9"></a>John 21:9

As soon then as they were [apobainō](../../strongs/g/g576.md) to [gē](../../strongs/g/g1093.md), they [blepō](../../strongs/g/g991.md) an [anthrakia](../../strongs/g/g439.md) there, and [opsarion](../../strongs/g/g3795.md) [keimai](../../strongs/g/g2749.md) [epikeimai](../../strongs/g/g1945.md), and [artos](../../strongs/g/g740.md).

<a name="john_21_10"></a>John 21:10

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, **[pherō](../../strongs/g/g5342.md) of the [opsarion](../../strongs/g/g3795.md) which ye have now [piazō](../../strongs/g/g4084.md).**

<a name="john_21_11"></a>John 21:11

[Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) [anabainō](../../strongs/g/g305.md), and [helkō](../../strongs/g/g1670.md) the [diktyon](../../strongs/g/g1350.md) to [gē](../../strongs/g/g1093.md) [mestos](../../strongs/g/g3324.md) of [megas](../../strongs/g/g3173.md) [ichthys](../../strongs/g/g2486.md), an hundred and fifty and three: and for all there were so [tosoutos](../../strongs/g/g5118.md), yet was not the [diktyon](../../strongs/g/g1350.md) [schizō](../../strongs/g/g4977.md).

<a name="john_21_12"></a>John 21:12

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, **[deute](../../strongs/g/g1205.md) [aristaō](../../strongs/g/g709.md)**. And none of the [mathētēs](../../strongs/g/g3101.md) [tolmaō](../../strongs/g/g5111.md) [exetazō](../../strongs/g/g1833.md) him, Who art thou? [eidō](../../strongs/g/g1492.md) that it was the [kyrios](../../strongs/g/g2962.md).

<a name="john_21_13"></a>John 21:13

[Iēsous](../../strongs/g/g2424.md) then [erchomai](../../strongs/g/g2064.md), and [lambanō](../../strongs/g/g2983.md) [artos](../../strongs/g/g740.md), and [didōmi](../../strongs/g/g1325.md) them, and [opsarion](../../strongs/g/g3795.md) [homoiōs](../../strongs/g/g3668.md).

<a name="john_21_14"></a>John 21:14

This is now the third time that [Iēsous](../../strongs/g/g2424.md) [phaneroō](../../strongs/g/g5319.md) himself to his [mathētēs](../../strongs/g/g3101.md), after that he was [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md).

<a name="john_21_15"></a>John 21:15

So when they had [aristaō](../../strongs/g/g709.md), [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) to [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md), **[Simōn](../../strongs/g/g4613.md), of [Iōnas](../../strongs/g/g2495.md), [agapaō](../../strongs/g/g25.md) thou me more than these?** He [legō](../../strongs/g/g3004.md) unto him, [nai](../../strongs/g/g3483.md), [kyrios](../../strongs/g/g2962.md); thou [eidō](../../strongs/g/g1492.md) that I [phileō](../../strongs/g/g5368.md) thee. He [legō](../../strongs/g/g3004.md) unto him, **[boskō](../../strongs/g/g1006.md) my [arnion](../../strongs/g/g721.md).**

<a name="john_21_16"></a>John 21:16

He [legō](../../strongs/g/g3004.md) to him again the second time, **[Simōn](../../strongs/g/g4613.md), of [Iōnas](../../strongs/g/g2495.md), [agapaō](../../strongs/g/g25.md) thou me?** He [legō](../../strongs/g/g3004.md) unto him, [nai](../../strongs/g/g3483.md), [kyrios](../../strongs/g/g2962.md); thou [eidō](../../strongs/g/g1492.md) that I [phileō](../../strongs/g/g5368.md) thee. He [legō](../../strongs/g/g3004.md) unto him, **[poimainō](../../strongs/g/g4165.md) my [probaton](../../strongs/g/g4263.md).**

<a name="john_21_17"></a>John 21:17

He [legō](../../strongs/g/g3004.md) unto him the third time, **[Simōn](../../strongs/g/g4613.md), of [Iōnas](../../strongs/g/g2495.md), [phileō](../../strongs/g/g5368.md) thou me?** [Petros](../../strongs/g/g4074.md) was [lypeō](../../strongs/g/g3076.md) because he [eipon](../../strongs/g/g2036.md) unto him the third time, **[phileō](../../strongs/g/g5368.md) thou me?** And he [eipon](../../strongs/g/g2036.md) unto him, [kyrios](../../strongs/g/g2962.md), thou [eidō](../../strongs/g/g1492.md) all things; thou [ginōskō](../../strongs/g/g1097.md) that I [phileō](../../strongs/g/g5368.md) thee. [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, **[boskō](../../strongs/g/g1006.md) my [probaton](../../strongs/g/g4263.md).**

<a name="john_21_18"></a>John 21:18

**[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto thee, When thou wast [neos](../../strongs/g/g3501.md), thou [zōnnymi](../../strongs/g/g2224.md) thyself, and [peripateō](../../strongs/g/g4043.md) whither thou [thelō](../../strongs/g/g2309.md): but when thou shalt be [gēraskō](../../strongs/g/g1095.md), thou shalt [ekteinō](../../strongs/g/g1614.md) thy [cheir](../../strongs/g/g5495.md), and another shall [zōnnymi](../../strongs/g/g2224.md) thee, and [pherō](../../strongs/g/g5342.md) thee whither thou [thelō](../../strongs/g/g2309.md) not.**

<a name="john_21_19"></a>John 21:19

This [eipon](../../strongs/g/g2036.md) he, [sēmainō](../../strongs/g/g4591.md) by what [thanatos](../../strongs/g/g2288.md) he should [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md). And when he had [eipon](../../strongs/g/g2036.md) this, he [legō](../../strongs/g/g3004.md) unto him, **[akoloutheō](../../strongs/g/g190.md) me.**

<a name="john_21_20"></a>John 21:20

Then [Petros](../../strongs/g/g4074.md), [epistrephō](../../strongs/g/g1994.md), [blepō](../../strongs/g/g991.md) the [mathētēs](../../strongs/g/g3101.md) whom [Iēsous](../../strongs/g/g2424.md) [agapaō](../../strongs/g/g25.md) [akoloutheō](../../strongs/g/g190.md); which also [anapiptō](../../strongs/g/g377.md) on his [stēthos](../../strongs/g/g4738.md) at [deipnon](../../strongs/g/g1173.md), and [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), which is he that [paradidōmi](../../strongs/g/g3860.md) thee?

<a name="john_21_21"></a>John 21:21

[Petros](../../strongs/g/g4074.md) [eidō](../../strongs/g/g1492.md) him [legō](../../strongs/g/g3004.md) to [Iēsous](../../strongs/g/g2424.md), [kyrios](../../strongs/g/g2962.md), and [tis](../../strongs/g/g5101.md) [houtos](../../strongs/g/g3778.md)?

<a name="john_21_22"></a>John 21:22

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, **If I [thelō](../../strongs/g/g2309.md) that he [menō](../../strongs/g/g3306.md) till I [erchomai](../../strongs/g/g2064.md), what to thee? [akoloutheō](../../strongs/g/g190.md) thou me.**

<a name="john_21_23"></a>John 21:23

Then went this [logos](../../strongs/g/g3056.md) [exerchomai](../../strongs/g/g1831.md) among the [adelphos](../../strongs/g/g80.md), that that [mathētēs](../../strongs/g/g3101.md) should not [apothnēskō](../../strongs/g/g599.md): yet [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) not unto him, He shall not [apothnēskō](../../strongs/g/g599.md); but, **If I [thelō](../../strongs/g/g2309.md) that he [menō](../../strongs/g/g3306.md) till I [erchomai](../../strongs/g/g2064.md), what to thee?**

<a name="john_21_24"></a>John 21:24

This is the [mathētēs](../../strongs/g/g3101.md) which [martyreō](../../strongs/g/g3140.md) of these things, and [graphō](../../strongs/g/g1125.md) these things: and we [eidō](../../strongs/g/g1492.md) that his [martyria](../../strongs/g/g3141.md) is [alēthēs](../../strongs/g/g227.md).

<a name="john_21_25"></a>John 21:25

And there are also [polys](../../strongs/g/g4183.md) other things which [Iēsous](../../strongs/g/g2424.md) [poieō](../../strongs/g/g4160.md), the which, if they should be [graphō](../../strongs/g/g1125.md) every one, I [oiomai](../../strongs/g/g3633.md) that even the [kosmos](../../strongs/g/g2889.md) itself could not [chōreō](../../strongs/g/g5562.md) the [biblion](../../strongs/g/g975.md) that should be [graphō](../../strongs/g/g1125.md). [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 20](john_20.md) - [Acts 1](acts_1.md)