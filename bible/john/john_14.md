# [John 14](https://www.blueletterbible.org/kjv/jhn/14/1/p0/rl1/s_1005001)

<a name="john_14_1"></a>John 14:1

**Let not your [kardia](../../strongs/g/g2588.md) be [tarassō](../../strongs/g/g5015.md): ye [pisteuō](../../strongs/g/g4100.md) in [theos](../../strongs/g/g2316.md), [pisteuō](../../strongs/g/g4100.md) also in me.**

<a name="john_14_2"></a>John 14:2

**In my [patēr](../../strongs/g/g3962.md) [oikia](../../strongs/g/g3614.md) are [polys](../../strongs/g/g4183.md) [monē](../../strongs/g/g3438.md): if it were not so, I would have [eipon](../../strongs/g/g2036.md) you. I [poreuō](../../strongs/g/g4198.md) to [hetoimazō](../../strongs/g/g2090.md) a [topos](../../strongs/g/g5117.md) for you.**

<a name="john_14_3"></a>John 14:3

**And if I [poreuō](../../strongs/g/g4198.md) and [hetoimazō](../../strongs/g/g2090.md) a [topos](../../strongs/g/g5117.md) for you, I will [erchomai](../../strongs/g/g2064.md) again, and [paralambanō](../../strongs/g/g3880.md) you unto myself; that where [egō](../../strongs/g/g1473.md) [eimi](../../strongs/g/g1510.md), ye may be also.**

<a name="john_14_4"></a>John 14:4

**And whither I [hypagō](../../strongs/g/g5217.md) ye [eidō](../../strongs/g/g1492.md), and the [hodos](../../strongs/g/g3598.md) ye [eidō](../../strongs/g/g1492.md).**

<a name="john_14_5"></a>John 14:5

[Thōmas](../../strongs/g/g2381.md) [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), we [eidō](../../strongs/g/g1492.md) not whither thou [hypagō](../../strongs/g/g5217.md); and how can we [eidō](../../strongs/g/g1492.md) the [hodos](../../strongs/g/g3598.md)?

<a name="john_14_6"></a>John 14:6

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, **I am the [hodos](../../strongs/g/g3598.md), the [alētheia](../../strongs/g/g225.md), and the [zōē](../../strongs/g/g2222.md): [oudeis](../../strongs/g/g3762.md) [erchomai](../../strongs/g/g2064.md) unto the [patēr](../../strongs/g/g3962.md), but by me.**

<a name="john_14_7"></a>John 14:7

**If ye had [ginōskō](../../strongs/g/g1097.md) me, ye should have [ginōskō](../../strongs/g/g1097.md) my [patēr](../../strongs/g/g3962.md) also: and from henceforth ye [ginōskō](../../strongs/g/g1097.md) him, and have [horaō](../../strongs/g/g3708.md) him.**

<a name="john_14_8"></a>John 14:8

[Philippos](../../strongs/g/g5376.md) [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), [deiknyō](../../strongs/g/g1166.md) us the [patēr](../../strongs/g/g3962.md), and it [arkeō](../../strongs/g/g714.md) us.

<a name="john_14_9"></a>John 14:9

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, **Have I been [tosoutos](../../strongs/g/g5118.md) [chronos](../../strongs/g/g5550.md) with you, and yet hast thou not [ginōskō](../../strongs/g/g1097.md) me, [Philippos](../../strongs/g/g5376.md)? he that hath [horaō](../../strongs/g/g3708.md) me hath [horaō](../../strongs/g/g3708.md) the [patēr](../../strongs/g/g3962.md); and how [legō](../../strongs/g/g3004.md) thou then, [deiknyō](../../strongs/g/g1166.md) us the [patēr](../../strongs/g/g3962.md)?**

<a name="john_14_10"></a>John 14:10

**[pisteuō](../../strongs/g/g4100.md) thou not that I am in the [patēr](../../strongs/g/g3962.md), and the [patēr](../../strongs/g/g3962.md) in me?  the [rhēma](../../strongs/g/g4487.md) that I [laleō](../../strongs/g/g2980.md) unto you I [laleō](../../strongs/g/g2980.md) not of myself: but the [patēr](../../strongs/g/g3962.md) that [menō](../../strongs/g/g3306.md) in me, he [poieō](../../strongs/g/g4160.md) the [ergon](../../strongs/g/g2041.md).**

<a name="john_14_11"></a>John 14:11

**[pisteuō](../../strongs/g/g4100.md) me that I am in the [patēr](../../strongs/g/g3962.md), and the [patēr](../../strongs/g/g3962.md) in me: or else [pisteuō](../../strongs/g/g4100.md) me for the very [ergon](../../strongs/g/g2041.md) sake.**

<a name="john_14_12"></a>John 14:12

**[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, He that [pisteuō](../../strongs/g/g4100.md) on me, the [ergon](../../strongs/g/g2041.md) that I [poieō](../../strongs/g/g4160.md) shall he [poieō](../../strongs/g/g4160.md) also; and [meizōn](../../strongs/g/g3187.md) than these shall he [poieō](../../strongs/g/g4160.md); because I [poreuō](../../strongs/g/g4198.md) unto my [patēr](../../strongs/g/g3962.md).**

<a name="john_14_13"></a>John 14:13

**And whatsoever ye shall [aiteō](../../strongs/g/g154.md) in my [onoma](../../strongs/g/g3686.md), that will I [poieō](../../strongs/g/g4160.md), that the [patēr](../../strongs/g/g3962.md) may be [doxazō](../../strongs/g/g1392.md) in the [huios](../../strongs/g/g5207.md).**

<a name="john_14_14"></a>John 14:14

**If ye shall [aiteō](../../strongs/g/g154.md) any thing in my [onoma](../../strongs/g/g3686.md), I will [poieō](../../strongs/g/g4160.md) it.**

<a name="john_14_15"></a>John 14:15

**If ye [agapaō](../../strongs/g/g25.md) me, [tēreō](../../strongs/g/g5083.md) my [entolē](../../strongs/g/g1785.md).**

<a name="john_14_16"></a>John 14:16

**And I will [erōtaō](../../strongs/g/g2065.md) the [patēr](../../strongs/g/g3962.md), and he shall [didōmi](../../strongs/g/g1325.md) you [allos](../../strongs/g/g243.md) [paraklētos](../../strongs/g/g3875.md), that he may [menō](../../strongs/g/g3306.md) with you for [aiōn](../../strongs/g/g165.md);**

<a name="john_14_17"></a>John 14:17

**the [pneuma](../../strongs/g/g4151.md) of [alētheia](../../strongs/g/g225.md); whom the [kosmos](../../strongs/g/g2889.md) cannot [lambanō](../../strongs/g/g2983.md), because it [theōreō](../../strongs/g/g2334.md) him not, neither [ginōskō](../../strongs/g/g1097.md) him: but ye [ginōskō](../../strongs/g/g1097.md) him; for he [menō](../../strongs/g/g3306.md) with you, and shall be in you.**

<a name="john_14_18"></a>John 14:18

**I will not [aphiēmi](../../strongs/g/g863.md) you [orphanos](../../strongs/g/g3737.md): I will [erchomai](../../strongs/g/g2064.md) to you.**

<a name="john_14_19"></a>John 14:19

**Yet a [mikron](../../strongs/g/g3397.md), and the [kosmos](../../strongs/g/g2889.md) [theōreō](../../strongs/g/g2334.md) me no more; but ye [theōreō](../../strongs/g/g2334.md) me: because I [zaō](../../strongs/g/g2198.md), ye shall [zaō](../../strongs/g/g2198.md) also.**

<a name="john_14_20"></a>John 14:20

**At that [hēmera](../../strongs/g/g2250.md) ye shall [ginōskō](../../strongs/g/g1097.md) that I in my [patēr](../../strongs/g/g3962.md), and ye in me, and I in you.**

<a name="john_14_21"></a>John 14:21

**He that hath my [entolē](../../strongs/g/g1785.md), and [tēreō](../../strongs/g/g5083.md) them, he it is that [agapaō](../../strongs/g/g25.md) me: and he that [agapaō](../../strongs/g/g25.md) me shall be [agapaō](../../strongs/g/g25.md) of my [patēr](../../strongs/g/g3962.md), and I will [agapaō](../../strongs/g/g25.md) him, and will [emphanizō](../../strongs/g/g1718.md) myself to him.**

<a name="john_14_22"></a>John 14:22

[Ioudas](../../strongs/g/g2455.md) [legō](../../strongs/g/g3004.md) unto him, not [Iskariōth](../../strongs/g/g2469.md), [kyrios](../../strongs/g/g2962.md), how is it that thou wilt [emphanizō](../../strongs/g/g1718.md) thyself unto us, and not unto the [kosmos](../../strongs/g/g2889.md)?

<a name="john_14_23"></a>John 14:23

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, **If a man [agapaō](../../strongs/g/g25.md) me, he will [tēreō](../../strongs/g/g5083.md) my [logos](../../strongs/g/g3056.md): and my [patēr](../../strongs/g/g3962.md) will [agapaō](../../strongs/g/g25.md) him, and we will [erchomai](../../strongs/g/g2064.md) unto him, and [poieō](../../strongs/g/g4160.md) our [monē](../../strongs/g/g3438.md) with him.**

<a name="john_14_24"></a>John 14:24

**He that [agapaō](../../strongs/g/g25.md) me not [tēreō](../../strongs/g/g5083.md) not my [logos](../../strongs/g/g3056.md): and the [logos](../../strongs/g/g3056.md) which ye [akouō](../../strongs/g/g191.md) is not mine, but the [patēr](../../strongs/g/g3962.md) which [pempō](../../strongs/g/g3992.md) me.**

<a name="john_14_25"></a>John 14:25

**These things have I [laleō](../../strongs/g/g2980.md) unto you, being [menō](../../strongs/g/g3306.md) with you.**

<a name="john_14_26"></a>John 14:26

**But the [paraklētos](../../strongs/g/g3875.md), the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), whom the [patēr](../../strongs/g/g3962.md) will [pempō](../../strongs/g/g3992.md) in my [onoma](../../strongs/g/g3686.md), he shall [didaskō](../../strongs/g/g1321.md) you all things, and [hypomimnēskō](../../strongs/g/g5279.md) all things, whatsoever I have [eipon](../../strongs/g/g2036.md) unto you.**

<a name="john_14_27"></a>John 14:27

**[eirēnē](../../strongs/g/g1515.md) I [aphiēmi](../../strongs/g/g863.md) with you, my [eirēnē](../../strongs/g/g1515.md) I [didōmi](../../strongs/g/g1325.md) unto you: not as the [kosmos](../../strongs/g/g2889.md) [didōmi](../../strongs/g/g1325.md), [didōmi](../../strongs/g/g1325.md) I unto you. Let not your [kardia](../../strongs/g/g2588.md) be [tarassō](../../strongs/g/g5015.md), neither let it be [deiliaō](../../strongs/g/g1168.md).**

<a name="john_14_28"></a>John 14:28

**Ye have [akouō](../../strongs/g/g191.md) how I [eipon](../../strongs/g/g2036.md) unto you, I [hypagō](../../strongs/g/g5217.md), and [erchomai](../../strongs/g/g2064.md) unto you. If ye [agapaō](../../strongs/g/g25.md) me, ye would [chairō](../../strongs/g/g5463.md), because I [eipon](../../strongs/g/g2036.md), I [poreuō](../../strongs/g/g4198.md) unto the [patēr](../../strongs/g/g3962.md): for my [patēr](../../strongs/g/g3962.md) is [meizōn](../../strongs/g/g3187.md) than I.**

<a name="john_14_29"></a>John 14:29

**And now I have [eipon](../../strongs/g/g2046.md) you before it [ginomai](../../strongs/g/g1096.md), that, when it is [ginomai](../../strongs/g/g1096.md), ye might [pisteuō](../../strongs/g/g4100.md).**

<a name="john_14_30"></a>John 14:30

**Hereafter I will not [laleō](../../strongs/g/g2980.md) [polys](../../strongs/g/g4183.md) with you: for the [archōn](../../strongs/g/g758.md) of this [kosmos](../../strongs/g/g2889.md) [erchomai](../../strongs/g/g2064.md), and hath [oudeis](../../strongs/g/g3762.md) in me.**

<a name="john_14_31"></a>John 14:31

**But that the [kosmos](../../strongs/g/g2889.md) may [ginōskō](../../strongs/g/g1097.md) that I [agapaō](../../strongs/g/g25.md) the [patēr](../../strongs/g/g3962.md); and as the [patēr](../../strongs/g/g3962.md) [entellō](../../strongs/g/g1781.md) me, even so I [poieō](../../strongs/g/g4160.md). [egeirō](../../strongs/g/g1453.md), let us [agō](../../strongs/g/g71.md) hence.**

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 13](john_13.md) - [John 15](john_15.md)