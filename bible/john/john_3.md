# [John 3](https://www.blueletterbible.org/kjv/jhn/3/1/rl1/s_1002001)

<a name="john_3_1"></a>John 3:1

There was an [anthrōpos](../../strongs/g/g444.md) of the [Pharisaios](../../strongs/g/g5330.md), [onoma](../../strongs/g/g3686.md) [Nikodēmos](../../strongs/g/g3530.md), an [archōn](../../strongs/g/g758.md) of the [Ioudaios](../../strongs/g/g2453.md): [^1]

<a name="john_3_2"></a>John 3:2

The same [erchomai](../../strongs/g/g2064.md) to [Iēsous](../../strongs/g/g2424.md) by [nyx](../../strongs/g/g3571.md), and [eipon](../../strongs/g/g2036.md) unto him, [rhabbi](../../strongs/g/g4461.md), we [eidō](../../strongs/g/g1492.md) that thou [erchomai](../../strongs/g/g2064.md) a [didaskalos](../../strongs/g/g1320.md) from [theos](../../strongs/g/g2316.md): for [oudeis](../../strongs/g/g3762.md) can [poieō](../../strongs/g/g4160.md) these [sēmeion](../../strongs/g/g4592.md) that thou [poieō](../../strongs/g/g4160.md), except [theos](../../strongs/g/g2316.md) be with him.

<a name="john_3_3"></a>John 3:3

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, **[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto thee, Except a [tis](../../strongs/g/g5100.md) be [gennaō](../../strongs/g/g1080.md) [anōthen](../../strongs/g/g509.md), he cannot [eidō](../../strongs/g/g1492.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md)**.

<a name="john_3_4"></a>John 3:4

[Nikodēmos](../../strongs/g/g3530.md) [legō](../../strongs/g/g3004.md) unto him, How can an [anthrōpos](../../strongs/g/g444.md) be [gennaō](../../strongs/g/g1080.md) when he is [gerōn](../../strongs/g/g1088.md)? can he [eiserchomai](../../strongs/g/g1525.md) the [deuteros](../../strongs/g/g1208.md) into his [mētēr](../../strongs/g/g3384.md) [koilia](../../strongs/g/g2836.md), and be [gennaō](../../strongs/g/g1080.md)?

<a name="john_3_5"></a>John 3:5

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md), **[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto thee, Except [tis](../../strongs/g/g5100.md) be [gennaō](../../strongs/g/g1080.md) of [hydōr](../../strongs/g/g5204.md) and of the [pneuma](../../strongs/g/g4151.md), he cannot [eiserchomai](../../strongs/g/g1525.md) into the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md)**.

<a name="john_3_6"></a>John 3:6

**That which is [gennaō](../../strongs/g/g1080.md) of the [sarx](../../strongs/g/g4561.md) is [sarx](../../strongs/g/g4561.md); and that which is [gennaō](../../strongs/g/g1080.md) of the [pneuma](../../strongs/g/g4151.md) is [pneuma](../../strongs/g/g4151.md)**.

<a name="john_3_7"></a>John 3:7

**[thaumazō](../../strongs/g/g2296.md) not that I [eipon](../../strongs/g/g2036.md) unto thee, Ye must be [gennaō](../../strongs/g/g1080.md) [anōthen](../../strongs/g/g509.md)**.

<a name="john_3_8"></a>John 3:8

**The [pneuma](../../strongs/g/g4151.md) [pneō](../../strongs/g/g4154.md) where it [thelō](../../strongs/g/g2309.md), and thou [akouō](../../strongs/g/g191.md) the [phōnē](../../strongs/g/g5456.md) thereof, but canst not [eidō](../../strongs/g/g1492.md) whence it [erchomai](../../strongs/g/g2064.md), and whither it [hypagō](../../strongs/g/g5217.md): so is every one that is [gennaō](../../strongs/g/g1080.md) of the [pneuma](../../strongs/g/g4151.md)**.

<a name="john_3_9"></a>John 3:9

[Nikodēmos](../../strongs/g/g3530.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, How can these things [ginomai](../../strongs/g/g1096.md)?

<a name="john_3_10"></a>John 3:10

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, **Art thou a [didaskalos](../../strongs/g/g1320.md) of [Israēl](../../strongs/g/g2474.md), and [ginōskō](../../strongs/g/g1097.md) not these things?**

<a name="john_3_11"></a>John 3:11

**[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto thee, We [laleō](../../strongs/g/g2980.md) that we do [eidō](../../strongs/g/g1492.md), and [martyreō](../../strongs/g/g3140.md) that we have [horaō](../../strongs/g/g3708.md); and ye [lambanō](../../strongs/g/g2983.md) not our [martyria](../../strongs/g/g3141.md)**.

<a name="john_3_12"></a>John 3:12

**If I have [eipon](../../strongs/g/g2036.md) you [epigeios](../../strongs/g/g1919.md), and ye [pisteuō](../../strongs/g/g4100.md) not, how shall ye [pisteuō](../../strongs/g/g4100.md), if I [eipon](../../strongs/g/g2036.md) you [epouranios](../../strongs/g/g2032.md)?**

<a name="john_3_13"></a>John 3:13

**And [oudeis](../../strongs/g/g3762.md) hath [anabainō](../../strongs/g/g305.md) to [ouranos](../../strongs/g/g3772.md), but he that [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md), the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) which is in [ouranos](../../strongs/g/g3772.md).**

<a name="john_3_14"></a>John 3:14

**And as [Mōÿsēs](../../strongs/g/g3475.md) [hypsoō](../../strongs/g/g5312.md) up the [ophis](../../strongs/g/g3789.md) in the [erēmos](../../strongs/g/g2048.md), even so must the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) be [hypsoō](../../strongs/g/g5312.md):**

<a name="john_3_15"></a>John 3:15

**That whosoever [pisteuō](../../strongs/g/g4100.md) in him should not [apollymi](../../strongs/g/g622.md), but have [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md).**

<a name="john_3_16"></a>John 3:16

**For [theos](../../strongs/g/g2316.md) [houtō(s)](../../strongs/g/g3779.md) [agapaō](../../strongs/g/g25.md) the [kosmos](../../strongs/g/g2889.md), that he [didōmi](../../strongs/g/g1325.md) his [monogenēs](../../strongs/g/g3439.md) [huios](../../strongs/g/g5207.md), that whosoever [pisteuō](../../strongs/g/g4100.md) in him should not [apollymi](../../strongs/g/g622.md), but have [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md).**

<a name="john_3_17"></a>John 3:17

**For [theos](../../strongs/g/g2316.md) [apostellō](../../strongs/g/g649.md) not his [huios](../../strongs/g/g5207.md) into the [kosmos](../../strongs/g/g2889.md) to [krinō](../../strongs/g/g2919.md) the [kosmos](../../strongs/g/g2889.md); but that the [kosmos](../../strongs/g/g2889.md) through him might be [sōzō](../../strongs/g/g4982.md).**

<a name="john_3_18"></a>John 3:18

**He that [pisteuō](../../strongs/g/g4100.md) on him is not [krinō](../../strongs/g/g2919.md): but he that [pisteuō](../../strongs/g/g4100.md) not is [krinō](../../strongs/g/g2919.md) already, because he hath not [pisteuō](../../strongs/g/g4100.md) in the [onoma](../../strongs/g/g3686.md) of the [monogenēs](../../strongs/g/g3439.md) [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md).**

<a name="john_3_19"></a>John 3:19

**And this is the [krisis](../../strongs/g/g2920.md), that [phōs](../../strongs/g/g5457.md) is [erchomai](../../strongs/g/g2064.md) into the [kosmos](../../strongs/g/g2889.md), and [anthrōpos](../../strongs/g/g444.md) [agapaō](../../strongs/g/g25.md) [skotos](../../strongs/g/g4655.md) rather than [phōs](../../strongs/g/g5457.md), because their [ergon](../../strongs/g/g2041.md) were [ponēros](../../strongs/g/g4190.md).**

<a name="john_3_20"></a>John 3:20

**For [pas](../../strongs/g/g3956.md) that [prassō](../../strongs/g/g4238.md) [phaulos](../../strongs/g/g5337.md) [miseō](../../strongs/g/g3404.md) the [phōs](../../strongs/g/g5457.md), neither [erchomai](../../strongs/g/g2064.md) to the [phōs](../../strongs/g/g5457.md), lest his [ergon](../../strongs/g/g2041.md) should be [elegchō](../../strongs/g/g1651.md).**

<a name="john_3_21"></a>John 3:21

**But he that [poieō](../../strongs/g/g4160.md) [alētheia](../../strongs/g/g225.md) [erchomai](../../strongs/g/g2064.md) to the [phōs](../../strongs/g/g5457.md), that his [ergon](../../strongs/g/g2041.md) [phaneroō](../../strongs/g/g5319.md), that they are [ergazomai](../../strongs/g/g2038.md) in [theos](../../strongs/g/g2316.md).**

<a name="john_3_22"></a>John 3:22

After these things [erchomai](../../strongs/g/g2064.md) [Iēsous](../../strongs/g/g2424.md) and his [mathētēs](../../strongs/g/g3101.md) into the [gē](../../strongs/g/g1093.md) of [Ioudaia](../../strongs/g/g2449.md); and there he [diatribō](../../strongs/g/g1304.md) with them, and [baptizō](../../strongs/g/g907.md).

<a name="john_3_23"></a>John 3:23

And [Iōannēs](../../strongs/g/g2491.md) also was [baptizō](../../strongs/g/g907.md) in [Ainōn](../../strongs/g/g137.md) [eggys](../../strongs/g/g1451.md) to [Saleim](../../strongs/g/g4530.md), because there was [polys](../../strongs/g/g4183.md) [hydōr](../../strongs/g/g5204.md) there: and they [paraginomai](../../strongs/g/g3854.md), and were [baptizō](../../strongs/g/g907.md).

<a name="john_3_24"></a>John 3:24

For [Iōannēs](../../strongs/g/g2491.md) was not yet [ballō](../../strongs/g/g906.md) into [phylakē](../../strongs/g/g5438.md).

<a name="john_3_25"></a>John 3:25

Then there [ginomai](../../strongs/g/g1096.md) a [zētēsis](../../strongs/g/g2214.md) between of [Iōannēs](../../strongs/g/g2491.md) [mathētēs](../../strongs/g/g3101.md) and the [Ioudaios](../../strongs/g/g2453.md) about [katharismos](../../strongs/g/g2512.md).

<a name="john_3_26"></a>John 3:26

And they [erchomai](../../strongs/g/g2064.md) unto [Iōannēs](../../strongs/g/g2491.md), and [eipon](../../strongs/g/g2036.md) unto him, [rhabbi](../../strongs/g/g4461.md), he that was with thee beyond [Iordanēs](../../strongs/g/g2446.md), to whom thou [martyreō](../../strongs/g/g3140.md), [ide](../../strongs/g/g2396.md), the same [baptizō](../../strongs/g/g907.md), and all [erchomai](../../strongs/g/g2064.md) to him.

<a name="john_3_27"></a>John 3:27

[Iōannēs](../../strongs/g/g2491.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), An [anthrōpos](../../strongs/g/g444.md) can [lambanō](../../strongs/g/g2983.md) [oudeis](../../strongs/g/g3762.md), except it be [didōmi](../../strongs/g/g1325.md) him from [ouranos](../../strongs/g/g3772.md).

<a name="john_3_28"></a>John 3:28

Ye yourselves [martyreō](../../strongs/g/g3140.md), that I [eipon](../../strongs/g/g2036.md), I am not the [Christos](../../strongs/g/g5547.md), but that I am [apostellō](../../strongs/g/g649.md) before him.

<a name="john_3_29"></a>John 3:29

He that hath the [nymphē](../../strongs/g/g3565.md) is the [nymphios](../../strongs/g/g3566.md): but the [philos](../../strongs/g/g5384.md) of the [nymphios](../../strongs/g/g3566.md), which [histēmi](../../strongs/g/g2476.md) and [akouō](../../strongs/g/g191.md) him, [chairō](../../strongs/g/g5463.md) greatly because of the [nymphios](../../strongs/g/g3566.md) [phōnē](../../strongs/g/g5456.md): this my [chara](../../strongs/g/g5479.md) therefore is [plēroō](../../strongs/g/g4137.md).

<a name="john_3_30"></a>John 3:30

He must [auxanō](../../strongs/g/g837.md), but I [elattoō](../../strongs/g/g1642.md).

<a name="john_3_31"></a>John 3:31

He that [erchomai](../../strongs/g/g2064.md) from [anōthen](../../strongs/g/g509.md) is above all: he that is of [gē](../../strongs/g/g1093.md) is of [gē](../../strongs/g/g1093.md), and [laleō](../../strongs/g/g2980.md) of [gē](../../strongs/g/g1093.md): he that [erchomai](../../strongs/g/g2064.md) from [ouranos](../../strongs/g/g3772.md) is [epanō](../../strongs/g/g1883.md) all.

<a name="john_3_32"></a>John 3:32

And what he hath [horaō](../../strongs/g/g3708.md) and [akouō](../../strongs/g/g191.md), that [martyreō](../../strongs/g/g3140.md); and [oudeis](../../strongs/g/g3762.md) [lambanō](../../strongs/g/g2983.md) his [martyria](../../strongs/g/g3141.md).

<a name="john_3_33"></a>John 3:33

He that hath [lambanō](../../strongs/g/g2983.md) his [martyria](../../strongs/g/g3141.md) hath [sphragizō](../../strongs/g/g4972.md) that [theos](../../strongs/g/g2316.md) is [alēthēs](../../strongs/g/g227.md).

<a name="john_3_34"></a>John 3:34

For he whom [theos](../../strongs/g/g2316.md) hath [apostellō](../../strongs/g/g649.md) [laleō](../../strongs/g/g2980.md) the [rhēma](../../strongs/g/g4487.md) of [theos](../../strongs/g/g2316.md): for [theos](../../strongs/g/g2316.md) [didōmi](../../strongs/g/g1325.md) not the [pneuma](../../strongs/g/g4151.md) by [metron](../../strongs/g/g3358.md) unto him.

<a name="john_3_35"></a>John 3:35

[patēr](../../strongs/g/g3962.md) [agapaō](../../strongs/g/g25.md) the [huios](../../strongs/g/g5207.md), and hath [didōmi](../../strongs/g/g1325.md) [pas](../../strongs/g/g3956.md) into his [cheir](../../strongs/g/g5495.md).

<a name="john_3_36"></a>John 3:36

He that [pisteuō](../../strongs/g/g4100.md) on the [huios](../../strongs/g/g5207.md) hath [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md): and he that [apeitheō](../../strongs/g/g544.md) the [huios](../../strongs/g/g5207.md) [optanomai](../../strongs/g/g3700.md) not [zōē](../../strongs/g/g2222.md); but the [orgē](../../strongs/g/g3709.md) of [theos](../../strongs/g/g2316.md) [menō](../../strongs/g/g3306.md) on him.

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 2](john_2.md) - [John 4](john_4.md)

---

[^1]: [1 John 3:1 Commentary](../../commentary/1john/1john_3_commentary.md#1john_3_1)
