# [John 2](https://www.blueletterbible.org/kjv/jhn/2/1/rl1/s_1002001)

<a name="john_2_1"></a>John 2:1

And the third [hēmera](../../strongs/g/g2250.md) there was a [gamos](../../strongs/g/g1062.md) in [Kana](../../strongs/g/g2580.md) of [Galilaia](../../strongs/g/g1056.md); and the [mētēr](../../strongs/g/g3384.md) of [Iēsous](../../strongs/g/g2424.md) was there:

<a name="john_2_2"></a>John 2:2

And both [Iēsous](../../strongs/g/g2424.md) was [kaleō](../../strongs/g/g2564.md), and his [mathētēs](../../strongs/g/g3101.md), to the [gamos](../../strongs/g/g1062.md).

<a name="john_2_3"></a>John 2:3

And when they [hystereō](../../strongs/g/g5302.md) [oinos](../../strongs/g/g3631.md), the [mētēr](../../strongs/g/g3384.md) of [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, They have no [oinos](../../strongs/g/g3631.md).

<a name="john_2_4"></a>John 2:4

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto her, **[gynē](../../strongs/g/g1135.md), [tis](../../strongs/g/g5101.md) [emoi](../../strongs/g/g1698.md) [kai](../../strongs/g/g2532.md) [soi](../../strongs/g/g4671.md)? mine [hōra](../../strongs/g/g5610.md) is not yet [hēkō](../../strongs/g/g2240.md).**

<a name="john_2_5"></a>John 2:5

His [mētēr](../../strongs/g/g3384.md) [legō](../../strongs/g/g3004.md) unto the [diakonos](../../strongs/g/g1249.md), Whatsoever he [legō](../../strongs/g/g3004.md) unto you, [poieō](../../strongs/g/g4160.md).

<a name="john_2_6"></a>John 2:6

And there were [keimai](../../strongs/g/g2749.md) there six [hydria](../../strongs/g/g5201.md) of [lithinos](../../strongs/g/g3035.md), after the [katharismos](../../strongs/g/g2512.md) of the [Ioudaios](../../strongs/g/g2453.md), [chōreō](../../strongs/g/g5562.md) two or three [metrētēs](../../strongs/g/g3355.md) [ana](../../strongs/g/g303.md).

<a name="john_2_7"></a>John 2:7

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, [gemizō](../../strongs/g/g1072.md) the [hydria](../../strongs/g/g5201.md) with [hydōr](../../strongs/g/g5204.md). And they [gemizō](../../strongs/g/g1072.md) them up to the [anō](../../strongs/g/g507.md).

<a name="john_2_8"></a>John 2:8

And he [legō](../../strongs/g/g3004.md) unto them, [antleō](../../strongs/g/g501.md) now, and [pherō](../../strongs/g/g5342.md) unto the [architriklinos](../../strongs/g/g755.md). And they [pherō](../../strongs/g/g5342.md).

<a name="john_2_9"></a>John 2:9

When the [architriklinos](../../strongs/g/g755.md) had [geuomai](../../strongs/g/g1089.md) the [hydōr](../../strongs/g/g5204.md) that was [ginomai](../../strongs/g/g1096.md) [oinos](../../strongs/g/g3631.md), and [eidō](../../strongs/g/g1492.md) not whence it was: (but the [diakonos](../../strongs/g/g1249.md) which [antleō](../../strongs/g/g501.md) the [hydōr](../../strongs/g/g5204.md) [eidō](../../strongs/g/g1492.md);) the [architriklinos](../../strongs/g/g755.md) [phōneō](../../strongs/g/g5455.md) the [nymphios](../../strongs/g/g3566.md),

<a name="john_2_10"></a>John 2:10

And [legō](../../strongs/g/g3004.md) unto him, Every [anthrōpos](../../strongs/g/g444.md) at the [prōton](../../strongs/g/g4412.md) [tithēmi](../../strongs/g/g5087.md) [kalos](../../strongs/g/g2570.md) [oinos](../../strongs/g/g3631.md); and when have [methyō](../../strongs/g/g3184.md), then that which is [elassōn](../../strongs/g/g1640.md): thou hast [tēreō](../../strongs/g/g5083.md) the [kalos](../../strongs/g/g2570.md) [oinos](../../strongs/g/g3631.md) until [arti](../../strongs/g/g737.md).

<a name="john_2_11"></a>John 2:11

This [archē](../../strongs/g/g746.md) of [sēmeion](../../strongs/g/g4592.md) [poieō](../../strongs/g/g4160.md) [Iēsous](../../strongs/g/g2424.md) in [Kana](../../strongs/g/g2580.md) of [Galilaia](../../strongs/g/g1056.md), and [phaneroō](../../strongs/g/g5319.md) his [doxa](../../strongs/g/g1391.md); and his [mathētēs](../../strongs/g/g3101.md) [pisteuō](../../strongs/g/g4100.md) on him.

<a name="john_2_12"></a>John 2:12

After this he went[katabainō](../../strongs/g/g2597.md) to [Kapharnaoum](../../strongs/g/g2584.md), he, and his [mētēr](../../strongs/g/g3384.md), and his [adelphos](../../strongs/g/g80.md), and his [mathētēs](../../strongs/g/g3101.md): and [menō](../../strongs/g/g3306.md) there not [polys](../../strongs/g/g4183.md) [hēmera](../../strongs/g/g2250.md).

<a name="john_2_13"></a>John 2:13

And the [Ioudaios](../../strongs/g/g2453.md)' [pascha](../../strongs/g/g3957.md) was [eggys](../../strongs/g/g1451.md), and [Iēsous](../../strongs/g/g2424.md) [anabainō](../../strongs/g/g305.md) to [Hierosolyma](../../strongs/g/g2414.md).

<a name="john_2_14"></a>John 2:14

And [heuriskō](../../strongs/g/g2147.md) in the [hieron](../../strongs/g/g2411.md) those that [pōleō](../../strongs/g/g4453.md) [bous](../../strongs/g/g1016.md) and [probaton](../../strongs/g/g4263.md) and [peristera](../../strongs/g/g4058.md), and the [kermatistēs](../../strongs/g/g2773.md) [kathēmai](../../strongs/g/g2521.md):

<a name="john_2_15"></a>John 2:15

And when he had [poieō](../../strongs/g/g4160.md) a [phragellion](../../strongs/g/g5416.md) of [schoinion](../../strongs/g/g4979.md), he [ekballō](../../strongs/g/g1544.md) them all out of the [hieron](../../strongs/g/g2411.md), and the [probaton](../../strongs/g/g4263.md), and the [bous](../../strongs/g/g1016.md); and [ekcheō](../../strongs/g/g1632.md) the [kollybistēs](../../strongs/g/g2855.md) [kerma](../../strongs/g/g2772.md), and [anastrephō](../../strongs/g/g390.md) the [trapeza](../../strongs/g/g5132.md);

<a name="john_2_16"></a>John 2:16

And [eipon](../../strongs/g/g2036.md) unto them that [pōleō](../../strongs/g/g4453.md) [peristera](../../strongs/g/g4058.md), **[airō](../../strongs/g/g142.md) these things hence; [poieō](../../strongs/g/g4160.md) not my [patēr](../../strongs/g/g3962.md) [oikos](../../strongs/g/g3624.md) an [oikos](../../strongs/g/g3624.md) of [emporion](../../strongs/g/g1712.md).**

<a name="john_2_17"></a>John 2:17

And his [mathētēs](../../strongs/g/g3101.md) [mnaomai](../../strongs/g/g3415.md) that it was [graphō](../../strongs/g/g1125.md), The [zēlos](../../strongs/g/g2205.md) of thine [oikos](../../strongs/g/g3624.md) hath [katesthiō](../../strongs/g/g2719.md).

<a name="john_2_18"></a>John 2:18

Then [apokrinomai](../../strongs/g/g611.md) the [Ioudaios](../../strongs/g/g2453.md) and [eipon](../../strongs/g/g2036.md) unto him, What [sēmeion](../../strongs/g/g4592.md) [deiknyō](../../strongs/g/g1166.md) thou unto us, seeing that thou [poieō](../../strongs/g/g4160.md) these things?

<a name="john_2_19"></a>John 2:19

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **[lyō](../../strongs/g/g3089.md) this [naos](../../strongs/g/g3485.md), and in three [hēmera](../../strongs/g/g2250.md) I will [egeirō](../../strongs/g/g1453.md).**

<a name="john_2_20"></a>John 2:20

Then [eipon](../../strongs/g/g2036.md) the [Ioudaios](../../strongs/g/g2453.md), Forty and six [etos](../../strongs/g/g2094.md) was this [naos](../../strongs/g/g3485.md) in [oikodomeō](../../strongs/g/g3618.md), and wilt thou [egeirō](../../strongs/g/g1453.md) it in three [hēmera](../../strongs/g/g2250.md)?

<a name="john_2_21"></a>John 2:21

But he [legō](../../strongs/g/g3004.md) of the [naos](../../strongs/g/g3485.md) of his [sōma](../../strongs/g/g4983.md).

<a name="john_2_22"></a>John 2:22

When therefore he was [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md), his [mathētēs](../../strongs/g/g3101.md) [mnaomai](../../strongs/g/g3415.md) that he had [legō](../../strongs/g/g3004.md) this unto them; and they [pisteuō](../../strongs/g/g4100.md) the [graphē](../../strongs/g/g1124.md), and the [logos](../../strongs/g/g3056.md) which [Iēsous](../../strongs/g/g2424.md) had [eipon](../../strongs/g/g2036.md).

<a name="john_2_23"></a>John 2:23

Now when he was in [Hierosolyma](../../strongs/g/g2414.md) at the [pascha](../../strongs/g/g3957.md), in the [heortē](../../strongs/g/g1859.md), [polys](../../strongs/g/g4183.md) [pisteuō](../../strongs/g/g4100.md) in his [onoma](../../strongs/g/g3686.md), when they [theōreō](../../strongs/g/g2334.md) the [sēmeion](../../strongs/g/g4592.md) which he [poieō](../../strongs/g/g4160.md).

<a name="john_2_24"></a>John 2:24

But [Iēsous](../../strongs/g/g2424.md) not [pisteuō](../../strongs/g/g4100.md) himself unto them, because he [ginōskō](../../strongs/g/g1097.md) all,

<a name="john_2_25"></a>John 2:25

And [chreia](../../strongs/g/g5532.md) not that any should [martyreō](../../strongs/g/g3140.md) of [anthrōpos](../../strongs/g/g444.md): for he [ginōskō](../../strongs/g/g1097.md) what was in [anthrōpos](../../strongs/g/g444.md).

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 1](john_1.md) - [John 3](john_3.md)
