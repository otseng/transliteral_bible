# [John 18](https://www.blueletterbible.org/kjv/jhn/18/1/s_1012001)

<a name="john_18_1"></a>John 18:1

When [Iēsous](../../strongs/g/g2424.md) had [eipon](../../strongs/g/g2036.md) [tauta](../../strongs/g/g5023.md), he [exerchomai](../../strongs/g/g1831.md) with his [mathētēs](../../strongs/g/g3101.md) over the [cheimarros](../../strongs/g/g5493.md) [kedrōn](../../strongs/g/g2748.md), where was a [kēpos](../../strongs/g/g2779.md), into the which he [eiserchomai](../../strongs/g/g1525.md), and his [mathētēs](../../strongs/g/g3101.md).

<a name="john_18_2"></a>John 18:2

And [Ioudas](../../strongs/g/g2455.md) also, which [paradidōmi](../../strongs/g/g3860.md) him, [eidō](../../strongs/g/g1492.md) the [topos](../../strongs/g/g5117.md): for [Iēsous](../../strongs/g/g2424.md) ofttimes [synagō](../../strongs/g/g4863.md) thither with his [mathētēs](../../strongs/g/g3101.md).

<a name="john_18_3"></a>John 18:3

[Ioudas](../../strongs/g/g2455.md) then, having [lambanō](../../strongs/g/g2983.md) a [speira](../../strongs/g/g4686.md) and [hypēretēs](../../strongs/g/g5257.md) from the [archiereus](../../strongs/g/g749.md) and [Pharisaios](../../strongs/g/g5330.md), [erchomai](../../strongs/g/g2064.md) thither with [phanos](../../strongs/g/g5322.md) and [lampas](../../strongs/g/g2985.md) and [hoplon](../../strongs/g/g3696.md).

<a name="john_18_4"></a>John 18:4

[Iēsous](../../strongs/g/g2424.md) therefore, [eidō](../../strongs/g/g1492.md) all things that should [erchomai](../../strongs/g/g2064.md) upon him, [exerchomai](../../strongs/g/g1831.md), and [eipon](../../strongs/g/g2036.md) unto them, **Whom [zēteō](../../strongs/g/g2212.md) ye?**

<a name="john_18_5"></a>John 18:5

They [apokrinomai](../../strongs/g/g611.md) him, [Iēsous](../../strongs/g/g2424.md) of [Nazōraios](../../strongs/g/g3480.md). [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, **[egō](../../strongs/g/g1473.md) [eimi](../../strongs/g/g1510.md)**. And [Ioudas](../../strongs/g/g2455.md) also, which [paradidōmi](../../strongs/g/g3860.md) him, [histēmi](../../strongs/g/g2476.md) with them.

<a name="john_18_6"></a>John 18:6

As soon then as he had [eipon](../../strongs/g/g2036.md) unto them, **[egō](../../strongs/g/g1473.md) [eimi](../../strongs/g/g1510.md)**, they [aperchomai](../../strongs/g/g565.md) [eis](../../strongs/g/g1519.md) [opisō](../../strongs/g/g3694.md), and [piptō](../../strongs/g/g4098.md) to the [chamai](../../strongs/g/g5476.md).

<a name="john_18_7"></a>John 18:7

Then [eperōtaō](../../strongs/g/g1905.md) he them again, **Whom [zēteō](../../strongs/g/g2212.md) ye**? And they [eipon](../../strongs/g/g2036.md), [Iēsous](../../strongs/g/g2424.md) of [Nazōraios](../../strongs/g/g3480.md).

<a name="john_18_8"></a>John 18:8

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md), **I have [eipon](../../strongs/g/g2036.md) you that [egō](../../strongs/g/g1473.md) [eimi](../../strongs/g/g1510.md): if therefore ye [zēteō](../../strongs/g/g2212.md) me, [aphiēmi](../../strongs/g/g863.md) these [hypagō](../../strongs/g/g5217.md):**

<a name="john_18_9"></a>John 18:9

That the [logos](../../strongs/g/g3056.md) might be [plēroō](../../strongs/g/g4137.md), which he [eipon](../../strongs/g/g2036.md), **Of them which thou [didōmi](../../strongs/g/g1325.md) me have I [apollymi](../../strongs/g/g622.md) none.**

<a name="john_18_10"></a>John 18:10

Then [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) having a [machaira](../../strongs/g/g3162.md) [helkō](../../strongs/g/g1670.md) it, and [paiō](../../strongs/g/g3817.md) the [archiereus](../../strongs/g/g749.md) [doulos](../../strongs/g/g1401.md), and [apokoptō](../../strongs/g/g609.md) his [dexios](../../strongs/g/g1188.md) [ōtion](../../strongs/g/g5621.md). The [doulos](../../strongs/g/g1401.md) [onoma](../../strongs/g/g3686.md) was [Malchos](../../strongs/g/g3124.md).

<a name="john_18_11"></a>John 18:11

Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) unto [Petros](../../strongs/g/g4074.md), **[ballō](../../strongs/g/g906.md) thy [machaira](../../strongs/g/g3162.md) into the [thēkē](../../strongs/g/g2336.md): the [potērion](../../strongs/g/g4221.md) which my [patēr](../../strongs/g/g3962.md) hath [didōmi](../../strongs/g/g1325.md) me, shall I not [pinō](../../strongs/g/g4095.md) it?**

<a name="john_18_12"></a>John 18:12

Then the [speira](../../strongs/g/g4686.md) and the [chiliarchos](../../strongs/g/g5506.md) and [hypēretēs](../../strongs/g/g5257.md) of the [Ioudaios](../../strongs/g/g2453.md) [syllambanō](../../strongs/g/g4815.md) [Iēsous](../../strongs/g/g2424.md), and [deō](../../strongs/g/g1210.md) him,

<a name="john_18_13"></a>John 18:13

And [apagō](../../strongs/g/g520.md) him to [Annas](../../strongs/g/g452.md) first; for he was [pentheros](../../strongs/g/g3995.md) to [Kaïaphas](../../strongs/g/g2533.md), which was the [archiereus](../../strongs/g/g749.md) that same [eniautos](../../strongs/g/g1763.md).

<a name="john_18_14"></a>John 18:14

Now [Kaïaphas](../../strongs/g/g2533.md) was he, which [symbouleuō](../../strongs/g/g4823.md) to the [Ioudaios](../../strongs/g/g2453.md), that it was [sympherō](../../strongs/g/g4851.md) that one [anthrōpos](../../strongs/g/g444.md) should [apollymi](../../strongs/g/g622.md) for the [laos](../../strongs/g/g2992.md).

<a name="john_18_15"></a>John 18:15

And [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) [akoloutheō](../../strongs/g/g190.md) [Iēsous](../../strongs/g/g2424.md), and another [mathētēs](../../strongs/g/g3101.md): that [mathētēs](../../strongs/g/g3101.md) was [gnōstos](../../strongs/g/g1110.md) unto the [archiereus](../../strongs/g/g749.md), and [syneiserchomai](../../strongs/g/g4897.md) [Iēsous](../../strongs/g/g2424.md) into the [aulē](../../strongs/g/g833.md) of the [archiereus](../../strongs/g/g749.md).

<a name="john_18_16"></a>John 18:16

But [Petros](../../strongs/g/g4074.md) [histēmi](../../strongs/g/g2476.md) at the [thyra](../../strongs/g/g2374.md) without. Then [exerchomai](../../strongs/g/g1831.md) that other [mathētēs](../../strongs/g/g3101.md), which was [gnōstos](../../strongs/g/g1110.md) unto the [archiereus](../../strongs/g/g749.md), and [eipon](../../strongs/g/g2036.md) unto her that [thyrōros](../../strongs/g/g2377.md), and [eisagō](../../strongs/g/g1521.md) [Petros](../../strongs/g/g4074.md).

<a name="john_18_17"></a>John 18:17

Then [legō](../../strongs/g/g3004.md) the [paidiskē](../../strongs/g/g3814.md) [thyrōros](../../strongs/g/g2377.md) unto [Petros](../../strongs/g/g4074.md), Art not thou also of this [anthrōpos](../../strongs/g/g444.md) [mathētēs](../../strongs/g/g3101.md)? He [legō](../../strongs/g/g3004.md), I am not.

<a name="john_18_18"></a>John 18:18

And the [doulos](../../strongs/g/g1401.md) and [hypēretēs](../../strongs/g/g5257.md) [histēmi](../../strongs/g/g2476.md) there, who had [poieō](../../strongs/g/g4160.md) an [anthrakia](../../strongs/g/g439.md); for it was [psychos](../../strongs/g/g5592.md): and they [thermainō](../../strongs/g/g2328.md) themselves: and [Petros](../../strongs/g/g4074.md) [histēmi](../../strongs/g/g2476.md) with them, and [thermainō](../../strongs/g/g2328.md) himself.

<a name="john_18_19"></a>John 18:19

The [archiereus](../../strongs/g/g749.md) then [erōtaō](../../strongs/g/g2065.md) [Iēsous](../../strongs/g/g2424.md) of his [mathētēs](../../strongs/g/g3101.md), and of his [didachē](../../strongs/g/g1322.md).

<a name="john_18_20"></a>John 18:20

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) him, **I [laleō](../../strongs/g/g2980.md) [parrēsia](../../strongs/g/g3954.md) to the [kosmos](../../strongs/g/g2889.md); I [pantote](../../strongs/g/g3842.md) [didaskō](../../strongs/g/g1321.md) in the [synagōgē](../../strongs/g/g4864.md), and in the [hieron](../../strongs/g/g2411.md), whither the [Ioudaios](../../strongs/g/g2453.md) [pantote](../../strongs/g/g3842.md) [synerchomai](../../strongs/g/g4905.md); and in [kryptos](../../strongs/g/g2927.md) have I [laleō](../../strongs/g/g2980.md) nothing.**

<a name="john_18_21"></a>John 18:21

**Why [eperōtaō](../../strongs/g/g1905.md) thou me? [eperōtaō](../../strongs/g/g1905.md) them which [akouō](../../strongs/g/g191.md) me, what I have [laleō](../../strongs/g/g2980.md) unto them: [ide](../../strongs/g/g2396.md), they [eidō](../../strongs/g/g1492.md) what I [eipon](../../strongs/g/g2036.md).**

<a name="john_18_22"></a>John 18:22

And when he had thus [eipon](../../strongs/g/g2036.md), one of the [hypēretēs](../../strongs/g/g5257.md) which [paristēmi](../../strongs/g/g3936.md) [didōmi](../../strongs/g/g1325.md) [rhapisma](../../strongs/g/g4475.md) [Iēsous](../../strongs/g/g2424.md), [eipon](../../strongs/g/g2036.md), [apokrinomai](../../strongs/g/g611.md) thou the [archiereus](../../strongs/g/g749.md) so?

<a name="john_18_23"></a>John 18:23

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) him, **If I have [laleō](../../strongs/g/g2980.md) [kakōs](../../strongs/g/g2560.md), [martyreō](../../strongs/g/g3140.md) of the [kakos](../../strongs/g/g2556.md): but if [kalōs](../../strongs/g/g2573.md), why [derō](../../strongs/g/g1194.md) thou me?**

<a name="john_18_24"></a>John 18:24

Now [Annas](../../strongs/g/g452.md) had [apostellō](../../strongs/g/g649.md) him [deō](../../strongs/g/g1210.md) unto [Kaïaphas](../../strongs/g/g2533.md) the [archiereus](../../strongs/g/g749.md).

<a name="john_18_25"></a>John 18:25

And [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) [histēmi](../../strongs/g/g2476.md) and [thermainō](../../strongs/g/g2328.md) himself. They [eipon](../../strongs/g/g2036.md) therefore unto him, Art not thou also one of his [mathētēs](../../strongs/g/g3101.md)? He [arneomai](../../strongs/g/g720.md), and [eipon](../../strongs/g/g2036.md), I am not.

<a name="john_18_26"></a>John 18:26

One of the [doulos](../../strongs/g/g1401.md) of the [archiereus](../../strongs/g/g749.md), being his [syggenēs](../../strongs/g/g4773.md) whose [ōtion](../../strongs/g/g5621.md) [Petros](../../strongs/g/g4074.md) [apokoptō](../../strongs/g/g609.md), [legō](../../strongs/g/g3004.md), Did not I [eidō](../../strongs/g/g1492.md) thee in the [kēpos](../../strongs/g/g2779.md) with him?

<a name="john_18_27"></a>John 18:27

[Petros](../../strongs/g/g4074.md) then [arneomai](../../strongs/g/g720.md) again: and [eutheōs](../../strongs/g/g2112.md) the [alektōr](../../strongs/g/g220.md) [phōneō](../../strongs/g/g5455.md).

<a name="john_18_28"></a>John 18:28

Then [agō](../../strongs/g/g71.md) they [Iēsous](../../strongs/g/g2424.md) from [Kaïaphas](../../strongs/g/g2533.md) unto the [praitōrion](../../strongs/g/g4232.md): and it was [prōïa](../../strongs/g/g4405.md); and they themselves [eiserchomai](../../strongs/g/g1525.md) not into the [praitōrion](../../strongs/g/g4232.md), lest they should be [miainō](../../strongs/g/g3392.md); but that they might [phago](../../strongs/g/g5315.md) the [pascha](../../strongs/g/g3957.md).

<a name="john_18_29"></a>John 18:29

[Pilatos](../../strongs/g/g4091.md) then [exerchomai](../../strongs/g/g1831.md) unto them, and [eipon](../../strongs/g/g2036.md), What [katēgoria](../../strongs/g/g2724.md) [pherō](../../strongs/g/g5342.md) ye against this [anthrōpos](../../strongs/g/g444.md)?

<a name="john_18_30"></a>John 18:30

They [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, If he were not a [kakopoios](../../strongs/g/g2555.md), we would not have [paradidōmi](../../strongs/g/g3860.md) him unto thee.

<a name="john_18_31"></a>John 18:31

Then [eipon](../../strongs/g/g2036.md) [Pilatos](../../strongs/g/g4091.md) unto them, [lambanō](../../strongs/g/g2983.md) ye him, and [krinō](../../strongs/g/g2919.md) him according to your [nomos](../../strongs/g/g3551.md). The [Ioudaios](../../strongs/g/g2453.md) therefore [eipon](../../strongs/g/g2036.md) unto him, It is not [exesti](../../strongs/g/g1832.md) for us to [apokteinō](../../strongs/g/g615.md) [oudeis](../../strongs/g/g3762.md):

<a name="john_18_32"></a>John 18:32

That the [logos](../../strongs/g/g3056.md) of [Iēsous](../../strongs/g/g2424.md) might be [plēroō](../../strongs/g/g4137.md), which he [eipon](../../strongs/g/g2036.md), [sēmainō](../../strongs/g/g4591.md) what [thanatos](../../strongs/g/g2288.md) he should [apothnēskō](../../strongs/g/g599.md).

<a name="john_18_33"></a>John 18:33

Then [Pilatos](../../strongs/g/g4091.md) [eiserchomai](../../strongs/g/g1525.md) into the [praitōrion](../../strongs/g/g4232.md) again, and [phōneō](../../strongs/g/g5455.md) [Iēsous](../../strongs/g/g2424.md), and [eipon](../../strongs/g/g2036.md) unto him, Art thou the [basileus](../../strongs/g/g935.md) of the [Ioudaios](../../strongs/g/g2453.md)?

<a name="john_18_34"></a>John 18:34

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) him, **[legō](../../strongs/g/g3004.md) thou this thing of thyself, or did others [eipon](../../strongs/g/g2036.md) it thee of me?**

<a name="john_18_35"></a>John 18:35

[Pilatos](../../strongs/g/g4091.md) [apokrinomai](../../strongs/g/g611.md), Am I an [Ioudaios](../../strongs/g/g2453.md)? Thine own [ethnos](../../strongs/g/g1484.md) and the [archiereus](../../strongs/g/g749.md) have [paradidōmi](../../strongs/g/g3860.md) thee unto me: what hast thou [poieō](../../strongs/g/g4160.md)?

<a name="john_18_36"></a>John 18:36

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md), **My [basileia](../../strongs/g/g932.md) is not of this [kosmos](../../strongs/g/g2889.md): if my [basileia](../../strongs/g/g932.md) were of this [kosmos](../../strongs/g/g2889.md), then would my [hypēretēs](../../strongs/g/g5257.md) [agōnizomai](../../strongs/g/g75.md), that I should not be [paradidōmi](../../strongs/g/g3860.md) to the [Ioudaios](../../strongs/g/g2453.md): but now is my [basileia](../../strongs/g/g932.md) not from hence.**

<a name="john_18_37"></a>John 18:37

[Pilatos](../../strongs/g/g4091.md) therefore [eipon](../../strongs/g/g2036.md) unto him, Art thou a [basileus](../../strongs/g/g935.md) then? [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md), **Thou [legō](../../strongs/g/g3004.md) that I am a [basileus](../../strongs/g/g935.md). To this was I [gennaō](../../strongs/g/g1080.md), and for this [erchomai](../../strongs/g/g2064.md) I into the [kosmos](../../strongs/g/g2889.md), that I should [martyreō](../../strongs/g/g3140.md) unto the [alētheia](../../strongs/g/g225.md). Every one that is of the [alētheia](../../strongs/g/g225.md) [akouō](../../strongs/g/g191.md) my [phōnē](../../strongs/g/g5456.md).**

<a name="john_18_38"></a>John 18:38

[Pilatos](../../strongs/g/g4091.md) [legō](../../strongs/g/g3004.md) unto him, What is [alētheia](../../strongs/g/g225.md)? And when he had [eipon](../../strongs/g/g2036.md) this, he [exerchomai](../../strongs/g/g1831.md) again unto the [Ioudaios](../../strongs/g/g2453.md), and [legō](../../strongs/g/g3004.md) unto them, I [heuriskō](../../strongs/g/g2147.md) in him no [aitia](../../strongs/g/g156.md).

<a name="john_18_39"></a>John 18:39

But ye have a [synētheia](../../strongs/g/g4914.md), that I should [apolyō](../../strongs/g/g630.md) unto you one at the [pascha](../../strongs/g/g3957.md): [boulomai](../../strongs/g/g1014.md) ye therefore that I [apolyō](../../strongs/g/g630.md) unto you the [basileus](../../strongs/g/g935.md) of the [Ioudaios](../../strongs/g/g2453.md)?

<a name="john_18_40"></a>John 18:40

Then [kraugazō](../../strongs/g/g2905.md) they all again, [legō](../../strongs/g/g3004.md), Not [touton](../../strongs/g/g5126.md), but [Barabbas](../../strongs/g/g912.md). Now [Barabbas](../../strongs/g/g912.md) was a [lēstēs](../../strongs/g/g3027.md).

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 17](john_17.md) - [John 19](john_19.md)