# [John 1](https://www.blueletterbible.org/kjv/jhn/1/1/rl1/s_1002001)

<a name="john_1_1"></a>John 1:1

In the [archē](../../strongs/g/g746.md) was the [logos](../../strongs/g/g3056.md), and the [logos](../../strongs/g/g3056.md) was with [theos](../../strongs/g/g2316.md), and the [logos](../../strongs/g/g3056.md) was [theos](../../strongs/g/g2316.md).

<a name="john_1_2"></a>John 1:2

[houtos](../../strongs/g/g3778.md) was in the [archē](../../strongs/g/g746.md) with [theos](../../strongs/g/g2316.md).

<a name="john_1_3"></a>John 1:3

[pas](../../strongs/g/g3956.md) were [ginomai](../../strongs/g/g1096.md) by him; and without him was not any thing [ginomai](../../strongs/g/g1096.md) that was [ginomai](../../strongs/g/g1096.md).

<a name="john_1_4"></a>John 1:4

In him was [zōē](../../strongs/g/g2222.md); and the [zōē](../../strongs/g/g2222.md) was the [phōs](../../strongs/g/g5457.md) of [anthrōpos](../../strongs/g/g444.md).

<a name="john_1_5"></a>John 1:5

And the [phōs](../../strongs/g/g5457.md) [phainō](../../strongs/g/g5316.md) in [skotia](../../strongs/g/g4653.md); and the [skotia](../../strongs/g/g4653.md) [katalambanō](../../strongs/g/g2638.md) it not.

<a name="john_1_6"></a>John 1:6

There was an [anthrōpos](../../strongs/g/g444.md) [apostellō](../../strongs/g/g649.md) from [theos](../../strongs/g/g2316.md), whose [onoma](../../strongs/g/g3686.md) [Iōannēs](../../strongs/g/g2491.md).

<a name="john_1_7"></a>John 1:7

The same [erchomai](../../strongs/g/g2064.md) for a [martyria](../../strongs/g/g3141.md), to [martyreō](../../strongs/g/g3140.md) of the [phōs](../../strongs/g/g5457.md), that all through him might [pisteuō](../../strongs/g/g4100.md).

<a name="john_1_8"></a>John 1:8

He was not that [phōs](../../strongs/g/g5457.md), but to [martyreō](../../strongs/g/g3140.md) of that [phōs](../../strongs/g/g5457.md).

<a name="john_1_9"></a>John 1:9

was the [alēthinos](../../strongs/g/g228.md) [phōs](../../strongs/g/g5457.md), which [phōtizō](../../strongs/g/g5461.md) every [anthrōpos](../../strongs/g/g444.md) that [erchomai](../../strongs/g/g2064.md) into the [kosmos](../../strongs/g/g2889.md).

<a name="john_1_10"></a>John 1:10

He was in the [kosmos](../../strongs/g/g2889.md), and the [kosmos](../../strongs/g/g2889.md) was [ginomai](../../strongs/g/g1096.md) by him, and the [kosmos](../../strongs/g/g2889.md) [ginōskō](../../strongs/g/g1097.md) him not.

<a name="john_1_11"></a>John 1:11

He [erchomai](../../strongs/g/g2064.md) unto [idios](../../strongs/g/g2398.md), and his own [paralambanō](../../strongs/g/g3880.md) him not.

<a name="john_1_12"></a>John 1:12

But as many as [lambanō](../../strongs/g/g2983.md) him, to them [didōmi](../../strongs/g/g1325.md) he [exousia](../../strongs/g/g1849.md) to become the [teknon](../../strongs/g/g5043.md) of [theos](../../strongs/g/g2316.md), to them that [pisteuō](../../strongs/g/g4100.md) on his [onoma](../../strongs/g/g3686.md):

<a name="john_1_13"></a>John 1:13

Which were [gennaō](../../strongs/g/g1080.md), not of [haima](../../strongs/g/g129.md), nor of the [thelēma](../../strongs/g/g2307.md) of the [sarx](../../strongs/g/g4561.md), nor of the [thelēma](../../strongs/g/g2307.md) of [anēr](../../strongs/g/g435.md), but of [theos](../../strongs/g/g2316.md).

<a name="john_1_14"></a>John 1:14

And the [logos](../../strongs/g/g3056.md) was [ginomai](../../strongs/g/g1096.md) [sarx](../../strongs/g/g4561.md), and [skēnoō](../../strongs/g/g4637.md) among us, (and we [theaomai](../../strongs/g/g2300.md) his [doxa](../../strongs/g/g1391.md), the [doxa](../../strongs/g/g1391.md) as [monogenēs](../../strongs/g/g3439.md) of the [patēr](../../strongs/g/g3962.md),) [plērēs](../../strongs/g/g4134.md) of [charis](../../strongs/g/g5485.md) and [alētheia](../../strongs/g/g225.md).

<a name="john_1_15"></a>John 1:15

[Iōannēs](../../strongs/g/g2491.md) [martyreō](../../strongs/g/g3140.md) of him, and [krazō](../../strongs/g/g2896.md), [legō](../../strongs/g/g3004.md), This was he of whom I [eipon](../../strongs/g/g2036.md), He that [erchomai](../../strongs/g/g2064.md) after me is [ginomai](../../strongs/g/g1096.md) [emprosthen](../../strongs/g/g1715.md) me: for he was [prōtos](../../strongs/g/g4413.md) me.

<a name="john_1_16"></a>John 1:16

And of his [plērōma](../../strongs/g/g4138.md) have all we [lambanō](../../strongs/g/g2983.md), and [charis](../../strongs/g/g5485.md) for [charis](../../strongs/g/g5485.md).

<a name="john_1_17"></a>John 1:17

For the [nomos](../../strongs/g/g3551.md) was [didōmi](../../strongs/g/g1325.md) by [Mōÿsēs](../../strongs/g/g3475.md), but [charis](../../strongs/g/g5485.md) and [alētheia](../../strongs/g/g225.md) [ginomai](../../strongs/g/g1096.md) by [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="john_1_18"></a>John 1:18

[oudeis](../../strongs/g/g3762.md) hath [horaō](../../strongs/g/g3708.md) [theos](../../strongs/g/g2316.md) [pōpote](../../strongs/g/g4455.md), the [monogenēs](../../strongs/g/g3439.md) [huios](../../strongs/g/g5207.md), which is in the [kolpos](../../strongs/g/g2859.md) of the [patēr](../../strongs/g/g3962.md), he hath [exēgeomai](../../strongs/g/g1834.md). [^1]

<a name="john_1_19"></a>John 1:19

And this is the [martyria](../../strongs/g/g3141.md) of [Iōannēs](../../strongs/g/g2491.md), when the [Ioudaios](../../strongs/g/g2453.md) [apostellō](../../strongs/g/g649.md) [hiereus](../../strongs/g/g2409.md) and [Leuitēs](../../strongs/g/g3019.md) from [Hierosolyma](../../strongs/g/g2414.md) to [erōtaō](../../strongs/g/g2065.md) him, Who art thou?

<a name="john_1_20"></a>John 1:20

And he [homologeō](../../strongs/g/g3670.md), and [arneomai](../../strongs/g/g720.md) not; but [homologeō](../../strongs/g/g3670.md), I am not the [Christos](../../strongs/g/g5547.md).

<a name="john_1_21"></a>John 1:21

And they [erōtaō](../../strongs/g/g2065.md) him, What then? Art thou [Ēlias](../../strongs/g/g2243.md)? And he [legō](../../strongs/g/g3004.md), I am not. Art thou that [prophētēs](../../strongs/g/g4396.md)? And he [apokrinomai](../../strongs/g/g611.md), No.

<a name="john_1_22"></a>John 1:22

Then [eipon](../../strongs/g/g2036.md) they unto him, Who art thou? that we may [didōmi](../../strongs/g/g1325.md) an [apokrisis](../../strongs/g/g612.md) to them that [pempō](../../strongs/g/g3992.md) us. What [legō](../../strongs/g/g3004.md) thou of thyself?

<a name="john_1_23"></a>John 1:23

He [phēmi](../../strongs/g/g5346.md), I the [phōnē](../../strongs/g/g5456.md) of one [boaō](../../strongs/g/g994.md) in the [erēmos](../../strongs/g/g2048.md), Make [euthynō](../../strongs/g/g2116.md) [hodos](../../strongs/g/g3598.md) of the [kyrios](../../strongs/g/g2962.md), as [eipon](../../strongs/g/g2036.md) the [prophētēs](../../strongs/g/g4396.md) [Ēsaïas](../../strongs/g/g2268.md).

<a name="john_1_24"></a>John 1:24

And they which were [apostellō](../../strongs/g/g649.md) were of the [Pharisaios](../../strongs/g/g5330.md).

<a name="john_1_25"></a>John 1:25

And they [erōtaō](../../strongs/g/g2065.md) him, and [eipon](../../strongs/g/g2036.md) unto him, Why [baptizō](../../strongs/g/g907.md) thou then, if thou be not that [Christos](../../strongs/g/g5547.md), nor [Ēlias](../../strongs/g/g2243.md), neither that [prophētēs](../../strongs/g/g4396.md)?

<a name="john_1_26"></a>John 1:26

[Iōannēs](../../strongs/g/g2491.md) [apokrinomai](../../strongs/g/g611.md) them, [legō](../../strongs/g/g3004.md), I [baptizō](../../strongs/g/g907.md) with [hydōr](../../strongs/g/g5204.md): but there [histēmi](../../strongs/g/g2476.md) one among you, whom ye [eidō](../../strongs/g/g1492.md) not;

<a name="john_1_27"></a>John 1:27

He it is, who [erchomai](../../strongs/g/g2064.md) after me is [ginomai](../../strongs/g/g1096.md) [emprosthen](../../strongs/g/g1715.md) me, whose [hypodēma](../../strongs/g/g5266.md) [himas](../../strongs/g/g2438.md) I am not [axios](../../strongs/g/g514.md) to [lyō](../../strongs/g/g3089.md).

<a name="john_1_28"></a>John 1:28

These things were [ginomai](../../strongs/g/g1096.md) in [Bēthabara](../../strongs/g/g962.md) beyond [Iordanēs](../../strongs/g/g2446.md), where [Iōannēs](../../strongs/g/g2491.md) was [baptizō](../../strongs/g/g907.md).

<a name="john_1_29"></a>John 1:29

The [epaurion](../../strongs/g/g1887.md) [Iōannēs](../../strongs/g/g2491.md) [blepō](../../strongs/g/g991.md) [Iēsous](../../strongs/g/g2424.md) [erchomai](../../strongs/g/g2064.md) unto him, and [legō](../../strongs/g/g3004.md), [ide](../../strongs/g/g2396.md) the [amnos](../../strongs/g/g286.md) of [theos](../../strongs/g/g2316.md), which [airō](../../strongs/g/g142.md) the [hamartia](../../strongs/g/g266.md) of the [kosmos](../../strongs/g/g2889.md).

<a name="john_1_30"></a>John 1:30

This is he of whom I [eipon](../../strongs/g/g2036.md), After me [erchomai](../../strongs/g/g2064.md) an [anēr](../../strongs/g/g435.md) which is [ginomai](../../strongs/g/g1096.md) [emprosthen](../../strongs/g/g1715.md) me: for he was [prōtos](../../strongs/g/g4413.md) me.

<a name="john_1_31"></a>John 1:31

And I [eidō](../../strongs/g/g1492.md) him not: but that [phaneroō](../../strongs/g/g5319.md) to [Israēl](../../strongs/g/g2474.md), therefore am I [erchomai](../../strongs/g/g2064.md) [baptizō](../../strongs/g/g907.md) with [hydōr](../../strongs/g/g5204.md).

<a name="john_1_32"></a>John 1:32

And [Iōannēs](../../strongs/g/g2491.md) [martyreō](../../strongs/g/g3140.md), [legō](../../strongs/g/g3004.md), I [theaomai](../../strongs/g/g2300.md) the [pneuma](../../strongs/g/g4151.md) [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md) like a [peristera](../../strongs/g/g4058.md), and it [menō](../../strongs/g/g3306.md) upon him.

<a name="john_1_33"></a>John 1:33

And I [eidō](../../strongs/g/g1492.md) him not: but he that [pempō](../../strongs/g/g3992.md) me to [baptizō](../../strongs/g/g907.md) with [hydōr](../../strongs/g/g5204.md), the same [eipon](../../strongs/g/g2036.md) unto me, Upon whom [eidō](../../strongs/g/g1492.md) the [pneuma](../../strongs/g/g4151.md) [katabainō](../../strongs/g/g2597.md), and [menō](../../strongs/g/g3306.md) on him, the same is he which [baptizō](../../strongs/g/g907.md) with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="john_1_34"></a>John 1:34

And I [horaō](../../strongs/g/g3708.md), and [martyreō](../../strongs/g/g3140.md) that this is the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md).

<a name="john_1_35"></a>John 1:35

Again the [epaurion](../../strongs/g/g1887.md) [Iōannēs](../../strongs/g/g2491.md) [histēmi](../../strongs/g/g2476.md), and two of his [mathētēs](../../strongs/g/g3101.md);

<a name="john_1_36"></a>John 1:36

And [emblepō](../../strongs/g/g1689.md) [Iēsous](../../strongs/g/g2424.md) as he [peripateō](../../strongs/g/g4043.md), he [legō](../../strongs/g/g3004.md), [ide](../../strongs/g/g2396.md) the [amnos](../../strongs/g/g286.md) of [theos](../../strongs/g/g2316.md)!

<a name="john_1_37"></a>John 1:37

And the two [mathētēs](../../strongs/g/g3101.md) [akouō](../../strongs/g/g191.md) him [laleō](../../strongs/g/g2980.md), and they [akoloutheō](../../strongs/g/g190.md) [Iēsous](../../strongs/g/g2424.md).

<a name="john_1_38"></a>John 1:38

Then [Iēsous](../../strongs/g/g2424.md) [strephō](../../strongs/g/g4762.md), and [theaomai](../../strongs/g/g2300.md) them [akoloutheō](../../strongs/g/g190.md), and [legō](../../strongs/g/g3004.md) unto them, **What [zēteō](../../strongs/g/g2212.md) ye?** They [eipon](../../strongs/g/g2036.md) unto him, [rhabbi](../../strongs/g/g4461.md), (which is to say, being [hermēneuō](../../strongs/g/g2059.md), [didaskalos](../../strongs/g/g1320.md),) where [menō](../../strongs/g/g3306.md)?

<a name="john_1_39"></a>John 1:39

He [legō](../../strongs/g/g3004.md) unto them, **[erchomai](../../strongs/g/g2064.md) and [eidō](../../strongs/g/g1492.md)**. They [erchomai](../../strongs/g/g2064.md) and [eidō](../../strongs/g/g1492.md) where he [menō](../../strongs/g/g3306.md), and [menō](../../strongs/g/g3306.md) with him that [hēmera](../../strongs/g/g2250.md): for it was about the tenth [hōra](../../strongs/g/g5610.md).

<a name="john_1_40"></a>John 1:40

One of the two which [akouō](../../strongs/g/g191.md) [Iōannēs](../../strongs/g/g2491.md), and [akoloutheō](../../strongs/g/g190.md) him, was [Andreas](../../strongs/g/g406.md), [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) [adelphos](../../strongs/g/g80.md).

<a name="john_1_41"></a>John 1:41

He first [heuriskō](../../strongs/g/g2147.md) his own [adelphos](../../strongs/g/g80.md) [Simōn](../../strongs/g/g4613.md), and [legō](../../strongs/g/g3004.md) unto him, We have [heuriskō](../../strongs/g/g2147.md) the [Messias](../../strongs/g/g3323.md), which is, being [methermēneuō](../../strongs/g/g3177.md), the [Christos](../../strongs/g/g5547.md).

<a name="john_1_42"></a>John 1:42

And he [agō](../../strongs/g/g71.md) him to [Iēsous](../../strongs/g/g2424.md). And when [Iēsous](../../strongs/g/g2424.md) [emblepō](../../strongs/g/g1689.md) him, he [eipon](../../strongs/g/g2036.md), **Thou art [Simōn](../../strongs/g/g4613.md) the [huios](../../strongs/g/g5207.md) of [Iōnas](../../strongs/g/g2495.md): thou shalt be [kaleō](../../strongs/g/g2564.md) [Kēphas](../../strongs/g/g2786.md)**, which is by [hermēneuō](../../strongs/g/g2059.md), A [Petros](../../strongs/g/g4074.md).

<a name="john_1_43"></a>John 1:43

The [epaurion](../../strongs/g/g1887.md) [Iēsous](../../strongs/g/g2424.md) would [exerchomai](../../strongs/g/g1831.md) into [Galilaia](../../strongs/g/g1056.md), and [heuriskō](../../strongs/g/g2147.md) [Philippos](../../strongs/g/g5376.md), and [legō](../../strongs/g/g3004.md) unto him, **[akoloutheō](../../strongs/g/g190.md) me**.

<a name="john_1_44"></a>John 1:44

Now [Philippos](../../strongs/g/g5376.md) was of [Bēthsaïda](../../strongs/g/g966.md), the [polis](../../strongs/g/g4172.md) of [Andreas](../../strongs/g/g406.md) and [Petros](../../strongs/g/g4074.md).

<a name="john_1_45"></a>John 1:45

[Philippos](../../strongs/g/g5376.md) [heuriskō](../../strongs/g/g2147.md) [Nathanaēl](../../strongs/g/g3482.md), and [legō](../../strongs/g/g3004.md) unto him, We have [heuriskō](../../strongs/g/g2147.md) him, of whom [Mōÿsēs](../../strongs/g/g3475.md) in the [nomos](../../strongs/g/g3551.md), and the [prophētēs](../../strongs/g/g4396.md), did [graphō](../../strongs/g/g1125.md), [Iēsous](../../strongs/g/g2424.md) of [Nazara](../../strongs/g/g3478.md), the [huios](../../strongs/g/g5207.md) of [Iōsēph](../../strongs/g/g2501.md).

<a name="john_1_46"></a>John 1:46

And [Nathanaēl](../../strongs/g/g3482.md) [eipon](../../strongs/g/g2036.md) unto him, Can there any [agathos](../../strongs/g/g18.md) [einai](../../strongs/g/g1511.md) out of [Nazara](../../strongs/g/g3478.md)? [Philippos](../../strongs/g/g5376.md) [legō](../../strongs/g/g3004.md) unto him, [erchomai](../../strongs/g/g2064.md) and [eidō](../../strongs/g/g1492.md).

<a name="john_1_47"></a>John 1:47

[Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) [Nathanaēl](../../strongs/g/g3482.md) [erchomai](../../strongs/g/g2064.md) to him, and [legō](../../strongs/g/g3004.md) of him, **[ide](../../strongs/g/g2396.md) an [Israēlitēs](../../strongs/g/g2475.md) [alēthōs](../../strongs/g/g230.md), in whom is no [dolos](../../strongs/g/g1388.md)**!

<a name="john_1_48"></a>John 1:48

[Nathanaēl](../../strongs/g/g3482.md) [legō](../../strongs/g/g3004.md) unto him, Whence [ginōskō](../../strongs/g/g1097.md) thou me? [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, **Before that [Philippos](../../strongs/g/g5376.md) [phōneō](../../strongs/g/g5455.md) thee, when thou wast under the [sykē](../../strongs/g/g4808.md), I [eidō](../../strongs/g/g1492.md) thee**.

<a name="john_1_49"></a>John 1:49

[Nathanaēl](../../strongs/g/g3482.md) [apokrinomai](../../strongs/g/g611.md) and [legō](../../strongs/g/g3004.md) unto him, [rhabbi](../../strongs/g/g4461.md), thou art the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md); thou art the [basileus](../../strongs/g/g935.md) of [Israēl](../../strongs/g/g2474.md).

<a name="john_1_50"></a>John 1:50

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, **Because I [eipon](../../strongs/g/g2036.md) unto thee, I [eidō](../../strongs/g/g1492.md) thee [hypokatō](../../strongs/g/g5270.md) the [sykē](../../strongs/g/g4808.md), [pisteuō](../../strongs/g/g4100.md) thou? thou shalt [optanomai](../../strongs/g/g3700.md) [meizōn](../../strongs/g/g3187.md) than these**.

<a name="john_1_51"></a>John 1:51

And he [legō](../../strongs/g/g3004.md) unto him, **[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, Hereafter ye shall [optanomai](../../strongs/g/g3700.md) [ouranos](../../strongs/g/g3772.md) [anoigō](../../strongs/g/g455.md), and the [aggelos](../../strongs/g/g32.md) of [theos](../../strongs/g/g2316.md) [anabainō](../../strongs/g/g305.md) and [katabainō](../../strongs/g/g2597.md) upon the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md)**.

---

[Transliteral Bible](../bible.md)

[John](john.md)

[Luke 24](luke_24.md) - [John 2](john_2.md)

---

[^1]: [John 1:18 Commentary](../../commentary/john/john_1_commentary.md#john_1_18)
