# [John 17](https://www.blueletterbible.org/kjv/jhn/17/1/s_1012001)

<a name="john_17_1"></a>John 17:1

[tauta](../../strongs/g/g5023.md) [laleō](../../strongs/g/g2980.md) [Iēsous](../../strongs/g/g2424.md), and [epairō](../../strongs/g/g1869.md) his [ophthalmos](../../strongs/g/g3788.md) to [ouranos](../../strongs/g/g3772.md), and [eipon](../../strongs/g/g2036.md), **[patēr](../../strongs/g/g3962.md), the [hōra](../../strongs/g/g5610.md) is [erchomai](../../strongs/g/g2064.md); [doxazō](../../strongs/g/g1392.md) thy [huios](../../strongs/g/g5207.md), that thy [huios](../../strongs/g/g5207.md) also may [doxazō](../../strongs/g/g1392.md) thee:**

<a name="john_17_2"></a>John 17:2

**As thou hast [didōmi](../../strongs/g/g1325.md) him [exousia](../../strongs/g/g1849.md) over all [sarx](../../strongs/g/g4561.md), that he should [didōmi](../../strongs/g/g1325.md) [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md) to as many as thou hast [didōmi](../../strongs/g/g1325.md) him.**

<a name="john_17_3"></a>John 17:3

**And this is [zōē](../../strongs/g/g2222.md) [aiōnios](../../strongs/g/g166.md), that they might [ginōskō](../../strongs/g/g1097.md) thee the only [alēthinos](../../strongs/g/g228.md) [theos](../../strongs/g/g2316.md), and [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), whom [apostellō](../../strongs/g/g649.md).**

<a name="john_17_4"></a>John 17:4

**I have [doxazō](../../strongs/g/g1392.md) thee on [gē](../../strongs/g/g1093.md): I have [teleioō](../../strongs/g/g5048.md) the [ergon](../../strongs/g/g2041.md) which thou [didōmi](../../strongs/g/g1325.md) me to [poieō](../../strongs/g/g4160.md).**

<a name="john_17_5"></a>John 17:5

**And now, [patēr](../../strongs/g/g3962.md), [doxazō](../../strongs/g/g1392.md) thou me with thine own self with the [doxa](../../strongs/g/g1391.md) which I had with thee before the [kosmos](../../strongs/g/g2889.md) was.**

<a name="john_17_6"></a>John 17:6

**I have [phaneroō](../../strongs/g/g5319.md) thy [onoma](../../strongs/g/g3686.md) unto the [anthrōpos](../../strongs/g/g444.md) which thou [didōmi](../../strongs/g/g1325.md) me out of the [kosmos](../../strongs/g/g2889.md): thine they were, and thou [didōmi](../../strongs/g/g1325.md) them me; and they have [tēreō](../../strongs/g/g5083.md) thy [logos](../../strongs/g/g3056.md).**

<a name="john_17_7"></a>John 17:7

**Now they have [ginōskō](../../strongs/g/g1097.md) that all things whatsoever thou hast [didōmi](../../strongs/g/g1325.md) me are of thee.**

<a name="john_17_8"></a>John 17:8

**For I have [didōmi](../../strongs/g/g1325.md) unto them the [rhēma](../../strongs/g/g4487.md) which thou [didōmi](../../strongs/g/g1325.md) me; and they have [lambanō](../../strongs/g/g2983.md), and have [ginōskō](../../strongs/g/g1097.md) [alēthōs](../../strongs/g/g230.md) that I [exerchomai](../../strongs/g/g1831.md) from thee, and they have [pisteuō](../../strongs/g/g4100.md) that thou [apostellō](../../strongs/g/g649.md) me.**

<a name="john_17_9"></a>John 17:9

**I [erōtaō](../../strongs/g/g2065.md) for them: I [erōtaō](../../strongs/g/g2065.md) not for the [kosmos](../../strongs/g/g2889.md), but for them which thou hast [didōmi](../../strongs/g/g1325.md) me; for they are thine.**

<a name="john_17_10"></a>John 17:10

**And all mine are thine, and thine are mine; and I am [doxazō](../../strongs/g/g1392.md) in them.**

<a name="john_17_11"></a>John 17:11

**And now I am no more in the [kosmos](../../strongs/g/g2889.md), but these are in the [kosmos](../../strongs/g/g2889.md), and I [erchomai](../../strongs/g/g2064.md) to thee. [hagios](../../strongs/g/g40.md) [patēr](../../strongs/g/g3962.md), [tēreō](../../strongs/g/g5083.md) through thine own [onoma](../../strongs/g/g3686.md) those whom thou hast [didōmi](../../strongs/g/g1325.md) me, that they may be [heis](../../strongs/g/g1520.md), as we.**

<a name="john_17_12"></a>John 17:12

**While I was with them in the [kosmos](../../strongs/g/g2889.md), I [tēreō](../../strongs/g/g5083.md) them in thy [onoma](../../strongs/g/g3686.md): those that thou [didōmi](../../strongs/g/g1325.md) me I have [phylassō](../../strongs/g/g5442.md), and none of them [apollymi](../../strongs/g/g622.md), but the [huios](../../strongs/g/g5207.md) of [apōleia](../../strongs/g/g684.md); that the [graphē](../../strongs/g/g1124.md) might be [plēroō](../../strongs/g/g4137.md).**

<a name="john_17_13"></a>John 17:13

**And now [erchomai](../../strongs/g/g2064.md) I to thee; and these things I [laleō](../../strongs/g/g2980.md) in the [kosmos](../../strongs/g/g2889.md), that they might have my [chara](../../strongs/g/g5479.md) [plēroō](../../strongs/g/g4137.md) in themselves.**

<a name="john_17_14"></a>John 17:14

**I have [didōmi](../../strongs/g/g1325.md) them thy [logos](../../strongs/g/g3056.md); and the [kosmos](../../strongs/g/g2889.md) hath [miseō](../../strongs/g/g3404.md) them, because they are not of the [kosmos](../../strongs/g/g2889.md), even as I am not of the [kosmos](../../strongs/g/g2889.md).**

<a name="john_17_15"></a>John 17:15

**I [erōtaō](../../strongs/g/g2065.md) not that thou shouldest [airō](../../strongs/g/g142.md) them out of the [kosmos](../../strongs/g/g2889.md), but that thou shouldest [tēreō](../../strongs/g/g5083.md) them from [ponēros](../../strongs/g/g4190.md).**

<a name="john_17_16"></a>John 17:16

**They are not of the [kosmos](../../strongs/g/g2889.md), even as I am not of the [kosmos](../../strongs/g/g2889.md).**

<a name="john_17_17"></a>John 17:17

**[hagiazō](../../strongs/g/g37.md) them through thy [alētheia](../../strongs/g/g225.md): thy [logos](../../strongs/g/g3056.md) is [alētheia](../../strongs/g/g225.md).**

<a name="john_17_18"></a>John 17:18

**As thou hast [apostellō](../../strongs/g/g649.md) me into the [kosmos](../../strongs/g/g2889.md), even so have I also [apostellō](../../strongs/g/g649.md) them into the [kosmos](../../strongs/g/g2889.md).**

<a name="john_17_19"></a>John 17:19

**And for their sakes I [hagiazō](../../strongs/g/g37.md) myself, that they also might be [hagiazō](../../strongs/g/g37.md) through the [alētheia](../../strongs/g/g225.md).**

<a name="john_17_20"></a>John 17:20

**Neither [erōtaō](../../strongs/g/g2065.md) for these alone, but for them also which shall [pisteuō](../../strongs/g/g4100.md) on me through their [logos](../../strongs/g/g3056.md);**

<a name="john_17_21"></a>John 17:21

**That they all may be [heis](../../strongs/g/g1520.md); as thou, [patēr](../../strongs/g/g3962.md), in me, and I in thee, that they also may be [heis](../../strongs/g/g1520.md) in us: that the [kosmos](../../strongs/g/g2889.md) may [pisteuō](../../strongs/g/g4100.md) that thou hast [apostellō](../../strongs/g/g649.md) me.**

<a name="john_17_22"></a>John 17:22

**And the [doxa](../../strongs/g/g1391.md) which thou [didōmi](../../strongs/g/g1325.md) me I have [didōmi](../../strongs/g/g1325.md) them; that they may be [heis](../../strongs/g/g1520.md), even as we are [heis](../../strongs/g/g1520.md):**

<a name="john_17_23"></a>John 17:23

**I in them, and thou in me, that they may be [teleioō](../../strongs/g/g5048.md) in [heis](../../strongs/g/g1520.md); and that the [kosmos](../../strongs/g/g2889.md) may [ginōskō](../../strongs/g/g1097.md) that thou hast [apostellō](../../strongs/g/g649.md) me, and hast [agapaō](../../strongs/g/g25.md) them, as thou hast [agapaō](../../strongs/g/g25.md) me.**

<a name="john_17_24"></a>John 17:24

**[patēr](../../strongs/g/g3962.md), I [thelō](../../strongs/g/g2309.md) that they also, whom thou hast [didōmi](../../strongs/g/g1325.md) me, be with me where I am; that they may [theōreō](../../strongs/g/g2334.md) my [doxa](../../strongs/g/g1391.md), which thou hast [didōmi](../../strongs/g/g1325.md) me: for thou [agapaō](../../strongs/g/g25.md) me before the [katabolē](../../strongs/g/g2602.md) of the [kosmos](../../strongs/g/g2889.md).**

<a name="john_17_25"></a>John 17:25

**O [dikaios](../../strongs/g/g1342.md) [patēr](../../strongs/g/g3962.md), the [kosmos](../../strongs/g/g2889.md) hath not [ginōskō](../../strongs/g/g1097.md) thee: but I have [ginōskō](../../strongs/g/g1097.md) thee, and these have [ginōskō](../../strongs/g/g1097.md) that thou hast [apostellō](../../strongs/g/g649.md) me.**

<a name="john_17_26"></a>John 17:26

**And I have [gnōrizō](../../strongs/g/g1107.md) unto them thy [onoma](../../strongs/g/g3686.md), and will [gnōrizō](../../strongs/g/g1107.md): that the [agapē](../../strongs/g/g26.md) wherewith thou hast [agapaō](../../strongs/g/g25.md) me may be in them, and I in them.**

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 16](john_16.md) - [John 18](john_18.md)