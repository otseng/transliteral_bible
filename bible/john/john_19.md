# [John 19](https://www.blueletterbible.org/kjv/jhn/19/1/rl1/s_1016001)

<a name="john_19_1"></a>John 19:1

Then [Pilatos](../../strongs/g/g4091.md) therefore [lambanō](../../strongs/g/g2983.md) [Iēsous](../../strongs/g/g2424.md), and [mastigoō](../../strongs/g/g3146.md) him.

<a name="john_19_2"></a>John 19:2

And the [stratiōtēs](../../strongs/g/g4757.md) [plekō](../../strongs/g/g4120.md) a [stephanos](../../strongs/g/g4735.md) of [akantha](../../strongs/g/g173.md), and [epitithēmi](../../strongs/g/g2007.md) his [kephalē](../../strongs/g/g2776.md), and they [periballō](../../strongs/g/g4016.md) him a [porphyrous](../../strongs/g/g4210.md) [himation](../../strongs/g/g2440.md),

<a name="john_19_3"></a>John 19:3

And [legō](../../strongs/g/g3004.md), [chairō](../../strongs/g/g5463.md), [basileus](../../strongs/g/g935.md) of the [Ioudaios](../../strongs/g/g2453.md)! and they [didōmi](../../strongs/g/g1325.md) him with [rhapisma](../../strongs/g/g4475.md).

<a name="john_19_4"></a>John 19:4

[Pilatos](../../strongs/g/g4091.md) therefore [exerchomai](../../strongs/g/g1831.md) forth again, and [legō](../../strongs/g/g3004.md) unto them, [ide](../../strongs/g/g2396.md), I [agō](../../strongs/g/g71.md) him forth to you, that ye may [ginōskō](../../strongs/g/g1097.md) that I [heuriskō](../../strongs/g/g2147.md) no [aitia](../../strongs/g/g156.md) in him.

<a name="john_19_5"></a>John 19:5

Then [exerchomai](../../strongs/g/g1831.md) [Iēsous](../../strongs/g/g2424.md) forth, [phoreō](../../strongs/g/g5409.md) the [stephanos](../../strongs/g/g4735.md) of [akanthinos](../../strongs/g/g174.md), and the [porphyrous](../../strongs/g/g4210.md) [himation](../../strongs/g/g2440.md). And [legō](../../strongs/g/g3004.md) unto them, [ide](../../strongs/g/g2396.md) the [anthrōpos](../../strongs/g/g444.md)!

<a name="john_19_6"></a>John 19:6

When the [archiereus](../../strongs/g/g749.md) therefore and [hypēretēs](../../strongs/g/g5257.md) [eidō](../../strongs/g/g1492.md) him, they [kraugazō](../../strongs/g/g2905.md), [legō](../../strongs/g/g3004.md), [stauroō](../../strongs/g/g4717.md), [stauroō](../../strongs/g/g4717.md). [Pilatos](../../strongs/g/g4091.md) [legō](../../strongs/g/g3004.md) unto them, [lambanō](../../strongs/g/g2983.md) ye him, and [stauroō](../../strongs/g/g4717.md): for I [heuriskō](../../strongs/g/g2147.md) no [aitia](../../strongs/g/g156.md) in him.

<a name="john_19_7"></a>John 19:7

The [Ioudaios](../../strongs/g/g2453.md) [apokrinomai](../../strongs/g/g611.md) him, We have a [nomos](../../strongs/g/g3551.md), and by our [nomos](../../strongs/g/g3551.md) he [opheilō](../../strongs/g/g3784.md) to [apothnēskō](../../strongs/g/g599.md), because he [poieō](../../strongs/g/g4160.md) himself the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md).

<a name="john_19_8"></a>John 19:8

When [Pilatos](../../strongs/g/g4091.md) therefore [akouō](../../strongs/g/g191.md) that [logos](../../strongs/g/g3056.md), he was the more [phobeō](../../strongs/g/g5399.md);

<a name="john_19_9"></a>John 19:9

And [eiserchomai](../../strongs/g/g1525.md) again into the [praitōrion](../../strongs/g/g4232.md), and [legō](../../strongs/g/g3004.md) unto [Iēsous](../../strongs/g/g2424.md), Whence art thou? But [Iēsous](../../strongs/g/g2424.md) [didōmi](../../strongs/g/g1325.md) him no [apokrisis](../../strongs/g/g612.md).

<a name="john_19_10"></a>John 19:10

Then [legō](../../strongs/g/g3004.md) [Pilatos](../../strongs/g/g4091.md) unto him, [laleō](../../strongs/g/g2980.md) thou not unto me? [eidō](../../strongs/g/g1492.md) thou not that I have [exousia](../../strongs/g/g1849.md) to [stauroō](../../strongs/g/g4717.md) thee, and have [exousia](../../strongs/g/g1849.md) to [apolyō](../../strongs/g/g630.md) thee?

<a name="john_19_11"></a>John 19:11

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md), **Thou couldest have no [exousia](../../strongs/g/g1849.md) against me, except it were [didōmi](../../strongs/g/g1325.md) thee from [anōthen](../../strongs/g/g509.md): therefore he that [paradidōmi](../../strongs/g/g3860.md) me unto thee hath the [meizōn](../../strongs/g/g3187.md) [hamartia](../../strongs/g/g266.md).**

<a name="john_19_12"></a>John 19:12

And from thenceforth [Pilatos](../../strongs/g/g4091.md) [zēteō](../../strongs/g/g2212.md) to [apolyō](../../strongs/g/g630.md) him: but the [Ioudaios](../../strongs/g/g2453.md) [krazō](../../strongs/g/g2896.md), [legō](../../strongs/g/g3004.md), If thou [apolyō](../../strongs/g/g630.md) [touton](../../strongs/g/g5126.md), thou art not [Kaisar](../../strongs/g/g2541.md) [philos](../../strongs/g/g5384.md): whosoever [poieō](../../strongs/g/g4160.md) himself a [basileus](../../strongs/g/g935.md) [antilegō](../../strongs/g/g483.md) [Kaisar](../../strongs/g/g2541.md).

<a name="john_19_13"></a>John 19:13

When [Pilatos](../../strongs/g/g4091.md) therefore [akouō](../../strongs/g/g191.md) that [logos](../../strongs/g/g3056.md), he [agō](../../strongs/g/g71.md) [Iēsous](../../strongs/g/g2424.md) forth, and [kathizō](../../strongs/g/g2523.md) in the [bēma](../../strongs/g/g968.md) in a [topos](../../strongs/g/g5117.md) that is [legō](../../strongs/g/g3004.md) the [lithostrōtos](../../strongs/g/g3038.md), but in the [Hebraïsti](../../strongs/g/g1447.md), [gabbatha](../../strongs/g/g1042.md).

<a name="john_19_14"></a>John 19:14

And it was the [paraskeuē](../../strongs/g/g3904.md) of the [pascha](../../strongs/g/g3957.md), and about the sixth [hōra](../../strongs/g/g5610.md): and he [legō](../../strongs/g/g3004.md) unto the [Ioudaios](../../strongs/g/g2453.md), [ide](../../strongs/g/g2396.md) your [basileus](../../strongs/g/g935.md)!

<a name="john_19_15"></a>John 19:15

But they [kraugazō](../../strongs/g/g2905.md), [airō](../../strongs/g/g142.md), [airō](../../strongs/g/g142.md), [stauroō](../../strongs/g/g4717.md) him. [Pilatos](../../strongs/g/g4091.md) [legō](../../strongs/g/g3004.md) unto them, Shall I [stauroō](../../strongs/g/g4717.md) your [basileus](../../strongs/g/g935.md)? The [archiereus](../../strongs/g/g749.md) [apokrinomai](../../strongs/g/g611.md), We have no [basileus](../../strongs/g/g935.md) but [Kaisar](../../strongs/g/g2541.md).

<a name="john_19_16"></a>John 19:16

Then [paradidōmi](../../strongs/g/g3860.md) he him therefore unto them to be [stauroō](../../strongs/g/g4717.md). And they [paralambanō](../../strongs/g/g3880.md) [Iēsous](../../strongs/g/g2424.md), and [apagō](../../strongs/g/g520.md).

<a name="john_19_17"></a>John 19:17

And he [bastazō](../../strongs/g/g941.md) his [stauros](../../strongs/g/g4716.md) [exerchomai](../../strongs/g/g1831.md) into a [topos](../../strongs/g/g5117.md) [legō](../../strongs/g/g3004.md) [kranion](../../strongs/g/g2898.md), which is [legō](../../strongs/g/g3004.md) in the [Hebraïsti](../../strongs/g/g1447.md) [Golgotha](../../strongs/g/g1115.md):

<a name="john_19_18"></a>John 19:18

Where they [stauroō](../../strongs/g/g4717.md) him, and two other with him, on either side one, and [Iēsous](../../strongs/g/g2424.md) in the [mesos](../../strongs/g/g3319.md).

<a name="john_19_19"></a>John 19:19

And [Pilatos](../../strongs/g/g4091.md) [graphō](../../strongs/g/g1125.md) a [titlos](../../strongs/g/g5102.md), and [tithēmi](../../strongs/g/g5087.md) on the [stauros](../../strongs/g/g4716.md). And the [graphō](../../strongs/g/g1125.md) was [Iēsous](../../strongs/g/g2424.md) [Nazōraios](../../strongs/g/g3480.md) [basileus](../../strongs/g/g935.md) [Ioudaios](../../strongs/g/g2453.md).

<a name="john_19_20"></a>John 19:20

This [titlos](../../strongs/g/g5102.md) then [anaginōskō](../../strongs/g/g314.md) [polys](../../strongs/g/g4183.md) of the [Ioudaios](../../strongs/g/g2453.md): for the [topos](../../strongs/g/g5117.md) where [Iēsous](../../strongs/g/g2424.md) was [stauroō](../../strongs/g/g4717.md) was [eggys](../../strongs/g/g1451.md) to the [polis](../../strongs/g/g4172.md): and it was [graphō](../../strongs/g/g1125.md) in [Hebraïsti](../../strongs/g/g1447.md), [Hellēnisti](../../strongs/g/g1676.md), [Rhōmaïsti](../../strongs/g/g4515.md).

<a name="john_19_21"></a>John 19:21

Then [legō](../../strongs/g/g3004.md) the [archiereus](../../strongs/g/g749.md) of the [Ioudaios](../../strongs/g/g2453.md) to [Pilatos](../../strongs/g/g4091.md), [graphō](../../strongs/g/g1125.md) not, The [basileus](../../strongs/g/g935.md) [Ioudaios](../../strongs/g/g2453.md); but that he [eipon](../../strongs/g/g2036.md), I am [basileus](../../strongs/g/g935.md) [Ioudaios](../../strongs/g/g2453.md).

<a name="john_19_22"></a>John 19:22

[Pilatos](../../strongs/g/g4091.md) [apokrinomai](../../strongs/g/g611.md), What I have [graphō](../../strongs/g/g1125.md) I have [graphō](../../strongs/g/g1125.md).

<a name="john_19_23"></a>John 19:23

Then the [stratiōtēs](../../strongs/g/g4757.md), when they had [stauroō](../../strongs/g/g4717.md) [Iēsous](../../strongs/g/g2424.md), [lambanō](../../strongs/g/g2983.md) his [himation](../../strongs/g/g2440.md), and [poieō](../../strongs/g/g4160.md) four [meros](../../strongs/g/g3313.md), to every [stratiōtēs](../../strongs/g/g4757.md) a part; and also [chitōn](../../strongs/g/g5509.md): now the [chitōn](../../strongs/g/g5509.md) was [araphos](../../strongs/g/g729.md), [hyphantos](../../strongs/g/g5307.md) from the [anōthen](../../strongs/g/g509.md) throughout.

<a name="john_19_24"></a>John 19:24

They [eipon](../../strongs/g/g2036.md) therefore among [allēlōn](../../strongs/g/g240.md), Let us not [schizō](../../strongs/g/g4977.md) it, but [lagchanō](../../strongs/g/g2975.md) for it, whose it shall be: that the [graphē](../../strongs/g/g1124.md) might be [plēroō](../../strongs/g/g4137.md), which [legō](../../strongs/g/g3004.md), They [diamerizō](../../strongs/g/g1266.md) my [himation](../../strongs/g/g2440.md) among them, and for my [himatismos](../../strongs/g/g2441.md) they did [ballō](../../strongs/g/g906.md) [klēros](../../strongs/g/g2819.md). These things therefore the [stratiōtēs](../../strongs/g/g4757.md) [poieō](../../strongs/g/g4160.md).

<a name="john_19_25"></a>John 19:25

Now there [histēmi](../../strongs/g/g2476.md) by the [stauros](../../strongs/g/g4716.md) of [Iēsous](../../strongs/g/g2424.md) his [mētēr](../../strongs/g/g3384.md), and his [mētēr](../../strongs/g/g3384.md) [adelphē](../../strongs/g/g79.md), [Maria](../../strongs/g/g3137.md) of [Klōpas](../../strongs/g/g2832.md), and [Maria](../../strongs/g/g3137.md) [Magdalēnē](../../strongs/g/g3094.md).

<a name="john_19_26"></a>John 19:26

When [Iēsous](../../strongs/g/g2424.md) therefore [eidō](../../strongs/g/g1492.md) his [mētēr](../../strongs/g/g3384.md), and the [mathētēs](../../strongs/g/g3101.md) [paristēmi](../../strongs/g/g3936.md), whom he [agapaō](../../strongs/g/g25.md), he [legō](../../strongs/g/g3004.md) unto his [mētēr](../../strongs/g/g3384.md), **[gynē](../../strongs/g/g1135.md), [idou](../../strongs/g/g2400.md) thy [huios](../../strongs/g/g5207.md)!**

<a name="john_19_27"></a>John 19:27

Then [legō](../../strongs/g/g3004.md) he to the [mathētēs](../../strongs/g/g3101.md), **[idou](../../strongs/g/g2400.md) thy [mētēr](../../strongs/g/g3384.md)!** And from that [hōra](../../strongs/g/g5610.md) that [mathētēs](../../strongs/g/g3101.md) [lambanō](../../strongs/g/g2983.md) her unto his [idios](../../strongs/g/g2398.md).

<a name="john_19_28"></a>John 19:28

After this, [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) that all things were now [teleō](../../strongs/g/g5055.md), that the [graphē](../../strongs/g/g1124.md) might be [teleioō](../../strongs/g/g5048.md), [legō](../../strongs/g/g3004.md), **[dipsaō](../../strongs/g/g1372.md)**.

<a name="john_19_29"></a>John 19:29

Now there was [keimai](../../strongs/g/g2749.md) a [skeuos](../../strongs/g/g4632.md) [mestos](../../strongs/g/g3324.md) of [oxos](../../strongs/g/g3690.md): and they [pimplēmi](../../strongs/g/g4130.md) a [spoggos](../../strongs/g/g4699.md) with [oxos](../../strongs/g/g3690.md), and [peritithēmi](../../strongs/g/g4060.md) it upon [hyssōpos](../../strongs/g/g5301.md), and [prospherō](../../strongs/g/g4374.md) it to his [stoma](../../strongs/g/g4750.md).

<a name="john_19_30"></a>John 19:30

When [Iēsous](../../strongs/g/g2424.md) therefore had [lambanō](../../strongs/g/g2983.md) the [oxos](../../strongs/g/g3690.md), he [eipon](../../strongs/g/g2036.md), **[teleō](../../strongs/g/g5055.md)**: and he [klinō](../../strongs/g/g2827.md) his [kephalē](../../strongs/g/g2776.md), and [paradidōmi](../../strongs/g/g3860.md) the [pneuma](../../strongs/g/g4151.md).

<a name="john_19_31"></a>John 19:31

The [Ioudaios](../../strongs/g/g2453.md) therefore, because it was the [paraskeuē](../../strongs/g/g3904.md), that the [sōma](../../strongs/g/g4983.md) should not [menō](../../strongs/g/g3306.md) upon the [stauros](../../strongs/g/g4716.md) on the [sabbaton](../../strongs/g/g4521.md), (for that [sabbaton](../../strongs/g/g4521.md) was a [megas](../../strongs/g/g3173.md) [hēmera](../../strongs/g/g2250.md),) [erōtaō](../../strongs/g/g2065.md) [Pilatos](../../strongs/g/g4091.md) that their [skelos](../../strongs/g/g4628.md) might be [katagnymi](../../strongs/g/g2608.md), and that they might be [airō](../../strongs/g/g142.md).

<a name="john_19_32"></a>John 19:32

Then [erchomai](../../strongs/g/g2064.md) the [stratiōtēs](../../strongs/g/g4757.md), and [katagnymi](../../strongs/g/g2608.md) the [skelos](../../strongs/g/g4628.md) of the first, and of the other which was [systauroō](../../strongs/g/g4957.md) him.

<a name="john_19_33"></a>John 19:33

But when they [erchomai](../../strongs/g/g2064.md) to [Iēsous](../../strongs/g/g2424.md), and [eidō](../../strongs/g/g1492.md) that he was [thnēskō](../../strongs/g/g2348.md) already, they [katagnymi](../../strongs/g/g2608.md) not his [skelos](../../strongs/g/g4628.md):

<a name="john_19_34"></a>John 19:34

But one of the [stratiōtēs](../../strongs/g/g4757.md) with a [logchē](../../strongs/g/g3057.md) [nyssō](../../strongs/g/g3572.md) his [pleura](../../strongs/g/g4125.md), and [euthys](../../strongs/g/g2117.md) [exerchomai](../../strongs/g/g1831.md) there [haima](../../strongs/g/g129.md) and [hydōr](../../strongs/g/g5204.md).

<a name="john_19_35"></a>John 19:35

And he that [horaō](../../strongs/g/g3708.md) [martyreō](../../strongs/g/g3140.md), and his [martyria](../../strongs/g/g3141.md) is [alēthinos](../../strongs/g/g228.md): and he [eidō](../../strongs/g/g1492.md) that he [legō](../../strongs/g/g3004.md) [alēthēs](../../strongs/g/g227.md), that ye might [pisteuō](../../strongs/g/g4100.md).

<a name="john_19_36"></a>John 19:36

For these things were [ginomai](../../strongs/g/g1096.md), that the [graphē](../../strongs/g/g1124.md) should be [plēroō](../../strongs/g/g4137.md), A [osteon](../../strongs/g/g3747.md) of him shall not be [syntribō](../../strongs/g/g4937.md).

<a name="john_19_37"></a>John 19:37

And again another [graphē](../../strongs/g/g1124.md) [legō](../../strongs/g/g3004.md), They shall [optanomai](../../strongs/g/g3700.md) on him whom they [ekkenteō](../../strongs/g/g1574.md).

<a name="john_19_38"></a>John 19:38

And after this [Iōsēph](../../strongs/g/g2501.md) of [Harimathaia](../../strongs/g/g707.md), being a [mathētēs](../../strongs/g/g3101.md) of [Iēsous](../../strongs/g/g2424.md), but [kryptō](../../strongs/g/g2928.md) for [phobos](../../strongs/g/g5401.md) of the [Ioudaios](../../strongs/g/g2453.md), [erōtaō](../../strongs/g/g2065.md) [Pilatos](../../strongs/g/g4091.md) that he might [airō](../../strongs/g/g142.md) the [sōma](../../strongs/g/g4983.md) of [Iēsous](../../strongs/g/g2424.md): and [Pilatos](../../strongs/g/g4091.md) [epitrepō](../../strongs/g/g2010.md). He [erchomai](../../strongs/g/g2064.md) therefore, and [airō](../../strongs/g/g142.md) the [sōma](../../strongs/g/g4983.md) of [Iēsous](../../strongs/g/g2424.md).

<a name="john_19_39"></a>John 19:39

And there [erchomai](../../strongs/g/g2064.md) also [Nikodēmos](../../strongs/g/g3530.md), which at the first [erchomai](../../strongs/g/g2064.md) to [Iēsous](../../strongs/g/g2424.md) by [nyx](../../strongs/g/g3571.md), and [pherō](../../strongs/g/g5342.md) a [migma](../../strongs/g/g3395.md) of [smyrna](../../strongs/g/g4666.md) and [aloē](../../strongs/g/g250.md), about a [hekaton](../../strongs/g/g1540.md) [litra](../../strongs/g/g3046.md).

<a name="john_19_40"></a>John 19:40

Then [lambanō](../../strongs/g/g2983.md) they the [sōma](../../strongs/g/g4983.md) of [Iēsous](../../strongs/g/g2424.md), and [deō](../../strongs/g/g1210.md) it in [othonion](../../strongs/g/g3608.md) with the [arōma](../../strongs/g/g759.md), as the [ethos](../../strongs/g/g1485.md) of the [Ioudaios](../../strongs/g/g2453.md) is to [entaphiazō](../../strongs/g/g1779.md).

<a name="john_19_41"></a>John 19:41

Now in the [topos](../../strongs/g/g5117.md) where he was [stauroō](../../strongs/g/g4717.md) there was a [kēpos](../../strongs/g/g2779.md); and in the [kēpos](../../strongs/g/g2779.md) a [kainos](../../strongs/g/g2537.md) [mnēmeion](../../strongs/g/g3419.md), wherein was [oudepō](../../strongs/g/g3764.md) [oudeis](../../strongs/g/g3762.md) yet [tithēmi](../../strongs/g/g5087.md).

<a name="john_19_42"></a>John 19:42

There [tithēmi](../../strongs/g/g5087.md) they [Iēsous](../../strongs/g/g2424.md) therefore because of the [Ioudaios](../../strongs/g/g2453.md) [paraskeuē](../../strongs/g/g3904.md); for the [mnēmeion](../../strongs/g/g3419.md) was [eggys](../../strongs/g/g1451.md).

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 18](john_18.md) - [John 20](john_20.md)