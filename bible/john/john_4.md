# [John 4](https://www.blueletterbible.org/kjv/jhn/4/1/rl1/s_1002001)

<a name="john_4_1"></a>John 4:1

When therefore the [kyrios](../../strongs/g/g2962.md) [ginōskō](../../strongs/g/g1097.md) how the [Pharisaios](../../strongs/g/g5330.md) had [akouō](../../strongs/g/g191.md) that [Iēsous](../../strongs/g/g2424.md) [poieō](../../strongs/g/g4160.md) and [baptizō](../../strongs/g/g907.md) more [mathētēs](../../strongs/g/g3101.md) than [Iōannēs](../../strongs/g/g2491.md),

<a name="john_4_2"></a>John 4:2

([kaitoige](../../strongs/g/g2544.md) [Iēsous](../../strongs/g/g2424.md) himself [baptizō](../../strongs/g/g907.md) not, but his [mathētēs](../../strongs/g/g3101.md),)

<a name="john_4_3"></a>John 4:3

He [aphiēmi](../../strongs/g/g863.md) [Ioudaia](../../strongs/g/g2449.md), and [aperchomai](../../strongs/g/g565.md) again into [Galilaia](../../strongs/g/g1056.md).

<a name="john_4_4"></a>John 4:4

And he must needs [dierchomai](../../strongs/g/g1330.md) through [Samareia](../../strongs/g/g4540.md).

<a name="john_4_5"></a>John 4:5

Then [erchomai](../../strongs/g/g2064.md) he to a [polis](../../strongs/g/g4172.md) of [Samareia](../../strongs/g/g4540.md), which is [legō](../../strongs/g/g3004.md) [Sychar](../../strongs/g/g4965.md), [plēsion](../../strongs/g/g4139.md) to the [chōrion](../../strongs/g/g5564.md) that [Iakōb](../../strongs/g/g2384.md) [didōmi](../../strongs/g/g1325.md) to his [huios](../../strongs/g/g5207.md) [Iōsēph](../../strongs/g/g2501.md).

<a name="john_4_6"></a>John 4:6

Now [Iakōb](../../strongs/g/g2384.md) [pēgē](../../strongs/g/g4077.md) was there. [Iēsous](../../strongs/g/g2424.md) therefore, being [kopiaō](../../strongs/g/g2872.md) with his [hodoiporia](../../strongs/g/g3597.md), [kathezomai](../../strongs/g/g2516.md) thus on the [pēgē](../../strongs/g/g4077.md): and it was about the sixth [hōra](../../strongs/g/g5610.md).

<a name="john_4_7"></a>John 4:7

There [erchomai](../../strongs/g/g2064.md) a [gynē](../../strongs/g/g1135.md) of [Samareia](../../strongs/g/g4540.md) to [antleō](../../strongs/g/g501.md) [hydōr](../../strongs/g/g5204.md): [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto her, **[didōmi](../../strongs/g/g1325.md) me to [pinō](../../strongs/g/g4095.md).**

<a name="john_4_8"></a>John 4:8

(For his [mathētēs](../../strongs/g/g3101.md) were [aperchomai](../../strongs/g/g565.md) unto the [polis](../../strongs/g/g4172.md) to [agorazō](../../strongs/g/g59.md) [trophē](../../strongs/g/g5160.md).)

<a name="john_4_9"></a>John 4:9

Then [legō](../../strongs/g/g3004.md) the [gynē](../../strongs/g/g1135.md) of [Samaritis](../../strongs/g/g4542.md) unto him, How is it that thou, being an [Ioudaios](../../strongs/g/g2453.md), [aiteō](../../strongs/g/g154.md) [pinō](../../strongs/g/g4095.md) of me, which am a [gynē](../../strongs/g/g1135.md) of [Samaritis](../../strongs/g/g4542.md)? for the [Ioudaios](../../strongs/g/g2453.md) have no [sygchraomai](../../strongs/g/g4798.md) with the [Samaritēs](../../strongs/g/g4541.md).

<a name="john_4_10"></a>John 4:10

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto her, **If thou [eidō](../../strongs/g/g1492.md) the [dōrea](../../strongs/g/g1431.md) of [theos](../../strongs/g/g2316.md), and who it is that [legō](../../strongs/g/g3004.md) to thee, [didōmi](../../strongs/g/g1325.md) me to [pinō](../../strongs/g/g4095.md); thou wouldest have [aiteō](../../strongs/g/g154.md) of him, and he would have [didōmi](../../strongs/g/g1325.md) thee [zaō](../../strongs/g/g2198.md) [hydōr](../../strongs/g/g5204.md).**

<a name="john_4_11"></a>John 4:11

The [gynē](../../strongs/g/g1135.md) [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), thou hast [oute](../../strongs/g/g3777.md) [antlēma](../../strongs/g/g502.md), and the [phrear](../../strongs/g/g5421.md) is [bathys](../../strongs/g/g901.md): from whence then hast thou that [zaō](../../strongs/g/g2198.md) [hydōr](../../strongs/g/g5204.md)?

<a name="john_4_12"></a>John 4:12

Art thou [meizōn](../../strongs/g/g3187.md) than our [patēr](../../strongs/g/g3962.md) [Iakōb](../../strongs/g/g2384.md), which [didōmi](../../strongs/g/g1325.md) us the [phrear](../../strongs/g/g5421.md), and [pinō](../../strongs/g/g4095.md) thereof himself, and his [huios](../../strongs/g/g5207.md), and his [thremma](../../strongs/g/g2353.md)?

<a name="john_4_13"></a>John 4:13

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto her, **Whosoever [pinō](../../strongs/g/g4095.md) of this [hydōr](../../strongs/g/g5204.md) shall [dipsaō](../../strongs/g/g1372.md) again:**

<a name="john_4_14"></a>John 4:14

**But whosoever [pinō](../../strongs/g/g4095.md) of the [hydōr](../../strongs/g/g5204.md) that I shall [didōmi](../../strongs/g/g1325.md) him shall [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) [ou mē](../../strongs/g/g3364.md) [dipsaō](../../strongs/g/g1372.md); but the [hydōr](../../strongs/g/g5204.md) that I shall [didōmi](../../strongs/g/g1325.md) him shall be in him a [pēgē](../../strongs/g/g4077.md) of [hydōr](../../strongs/g/g5204.md) [hallomai](../../strongs/g/g242.md) into [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md).**

<a name="john_4_15"></a>John 4:15

The [gynē](../../strongs/g/g1135.md) [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), [didōmi](../../strongs/g/g1325.md) me this [hydōr](../../strongs/g/g5204.md), that I [dipsaō](../../strongs/g/g1372.md) not, neither [erchomai](../../strongs/g/g2064.md) [enthade](../../strongs/g/g1759.md) to [antleō](../../strongs/g/g501.md).

<a name="john_4_16"></a>John 4:16

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto her, **[hypagō](../../strongs/g/g5217.md), [phōneō](../../strongs/g/g5455.md) thy [anēr](../../strongs/g/g435.md), and [erchomai](../../strongs/g/g2064.md) [enthade](../../strongs/g/g1759.md).**

<a name="john_4_17"></a>John 4:17

The [gynē](../../strongs/g/g1135.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), I have no [anēr](../../strongs/g/g435.md). [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto her, **Thou hast [kalōs](../../strongs/g/g2573.md) [eipon](../../strongs/g/g2036.md), I have no [anēr](../../strongs/g/g435.md):**

<a name="john_4_18"></a>John 4:18

**For thou hast had five [anēr](../../strongs/g/g435.md); and he whom thou now hast is not thy [anēr](../../strongs/g/g435.md): in that [eipon](../../strongs/g/g2046.md) [alēthēs](../../strongs/g/g227.md).**

<a name="john_4_19"></a>John 4:19

The [gynē](../../strongs/g/g1135.md) [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), I [theōreō](../../strongs/g/g2334.md) that thou art a [prophētēs](../../strongs/g/g4396.md).

<a name="john_4_20"></a>John 4:20

Our [patēr](../../strongs/g/g3962.md) [proskyneō](../../strongs/g/g4352.md) in this [oros](../../strongs/g/g3735.md); and ye [legō](../../strongs/g/g3004.md), that in [Hierosolyma](../../strongs/g/g2414.md) is the [topos](../../strongs/g/g5117.md) where [dei](../../strongs/g/g1163.md) to [proskyneō](../../strongs/g/g4352.md).

<a name="john_4_21"></a>John 4:21

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto her, **[gynē](../../strongs/g/g1135.md), [pisteuō](../../strongs/g/g4100.md) me, the [hōra](../../strongs/g/g5610.md) [erchomai](../../strongs/g/g2064.md), when ye shall neither in this [oros](../../strongs/g/g3735.md), nor yet at [Hierosolyma](../../strongs/g/g2414.md), [proskyneō](../../strongs/g/g4352.md) the [patēr](../../strongs/g/g3962.md).**

<a name="john_4_22"></a>John 4:22

**Ye [proskyneō](../../strongs/g/g4352.md) ye [eidō](../../strongs/g/g1492.md) not what: we [eidō](../../strongs/g/g1492.md) what we [proskyneō](../../strongs/g/g4352.md): for [sōtēria](../../strongs/g/g4991.md) is of the [Ioudaios](../../strongs/g/g2453.md).**

<a name="john_4_23"></a>John 4:23

**But the [hōra](../../strongs/g/g5610.md) [erchomai](../../strongs/g/g2064.md), and now is, when the [alēthinos](../../strongs/g/g228.md) [proskynētēs](../../strongs/g/g4353.md) shall [proskyneō](../../strongs/g/g4352.md) the [patēr](../../strongs/g/g3962.md) in [pneuma](../../strongs/g/g4151.md) and in [alētheia](../../strongs/g/g225.md): for the [patēr](../../strongs/g/g3962.md) [zēteō](../../strongs/g/g2212.md) such to [proskyneō](../../strongs/g/g4352.md) him.**

<a name="john_4_24"></a>John 4:24

**[theos](../../strongs/g/g2316.md) is [pneuma](../../strongs/g/g4151.md): and they that [proskyneō](../../strongs/g/g4352.md) him must [proskyneō](../../strongs/g/g4352.md) him in [pneuma](../../strongs/g/g4151.md) and in [alētheia](../../strongs/g/g225.md).**

<a name="john_4_25"></a>John 4:25

The [gynē](../../strongs/g/g1135.md) [legō](../../strongs/g/g3004.md) unto him, I [eidō](../../strongs/g/g1492.md) that [Messias](../../strongs/g/g3323.md) [erchomai](../../strongs/g/g2064.md), which is [legō](../../strongs/g/g3004.md) [Christos](../../strongs/g/g5547.md): when he is [erchomai](../../strongs/g/g2064.md), he will [anaggellō](../../strongs/g/g312.md) us all things.

<a name="john_4_26"></a>John 4:26

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto her, **I that [laleō](../../strongs/g/g2980.md) unto thee am he.**

<a name="john_4_27"></a>John 4:27

And upon this [erchomai](../../strongs/g/g2064.md) his [mathētēs](../../strongs/g/g3101.md), and [thaumazō](../../strongs/g/g2296.md) that he [laleō](../../strongs/g/g2980.md) with the [gynē](../../strongs/g/g1135.md): yet [oudeis](../../strongs/g/g3762.md) [eipon](../../strongs/g/g2036.md), What [zēteō](../../strongs/g/g2212.md) thou? or, Why [laleō](../../strongs/g/g2980.md) thou with her?

<a name="john_4_28"></a>John 4:28

The [gynē](../../strongs/g/g1135.md) then [aphiēmi](../../strongs/g/g863.md) her [hydria](../../strongs/g/g5201.md), and [aperchomai](../../strongs/g/g565.md) into the [polis](../../strongs/g/g4172.md), and [legō](../../strongs/g/g3004.md) to the [anthrōpos](../../strongs/g/g444.md),

<a name="john_4_29"></a>John 4:29

[deute](../../strongs/g/g1205.md), [eidō](../../strongs/g/g1492.md) an [anthrōpos](../../strongs/g/g444.md), which [eipon](../../strongs/g/g2036.md) me all things that ever I [poieō](../../strongs/g/g4160.md): is not this the [Christos](../../strongs/g/g5547.md)?

<a name="john_4_30"></a>John 4:30

Then they [exerchomai](../../strongs/g/g1831.md) of the [polis](../../strongs/g/g4172.md), and [erchomai](../../strongs/g/g2064.md) unto him.

<a name="john_4_31"></a>John 4:31

In the mean while his [mathētēs](../../strongs/g/g3101.md) [erōtaō](../../strongs/g/g2065.md) him, [legō](../../strongs/g/g3004.md), [rhabbi](../../strongs/g/g4461.md), [phago](../../strongs/g/g5315.md).

<a name="john_4_32"></a>John 4:32

But he [eipon](../../strongs/g/g2036.md) unto them, **I have [brōsis](../../strongs/g/g1035.md) to [phago](../../strongs/g/g5315.md) that ye [eidō](../../strongs/g/g1492.md) not of.**

<a name="john_4_33"></a>John 4:33

Therefore [legō](../../strongs/g/g3004.md) the [mathētēs](../../strongs/g/g3101.md) to [allēlōn](../../strongs/g/g240.md), Hath [mē tis](../../strongs/g/g3387.md) [pherō](../../strongs/g/g5342.md) him to [phago](../../strongs/g/g5315.md)?

<a name="john_4_34"></a>John 4:34

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, **My [brōma](../../strongs/g/g1033.md) is to [poieō](../../strongs/g/g4160.md) the [thelēma](../../strongs/g/g2307.md) of him that [pempō](../../strongs/g/g3992.md) me, and to [teleioō](../../strongs/g/g5048.md) his [ergon](../../strongs/g/g2041.md).**

<a name="john_4_35"></a>John 4:35

**[legō](../../strongs/g/g3004.md) not ye, There are yet [tetramēnos](../../strongs/g/g5072.md), and then [erchomai](../../strongs/g/g2064.md) [therismos](../../strongs/g/g2326.md)? [idou](../../strongs/g/g2400.md), I [legō](../../strongs/g/g3004.md) unto you, [epairō](../../strongs/g/g1869.md) your [ophthalmos](../../strongs/g/g3788.md), and [theaomai](../../strongs/g/g2300.md) the [chōra](../../strongs/g/g5561.md); for they are [leukos](../../strongs/g/g3022.md) already to [therismos](../../strongs/g/g2326.md).**

<a name="john_4_36"></a>John 4:36

**And he that [therizō](../../strongs/g/g2325.md) [lambanō](../../strongs/g/g2983.md) [misthos](../../strongs/g/g3408.md), and [synagō](../../strongs/g/g4863.md) [karpos](../../strongs/g/g2590.md) unto [zōē](../../strongs/g/g2222.md) [aiōnios](../../strongs/g/g166.md): that both he that [speirō](../../strongs/g/g4687.md) and he that [therizō](../../strongs/g/g2325.md) may [chairō](../../strongs/g/g5463.md) [homou](../../strongs/g/g3674.md).**

<a name="john_4_37"></a>John 4:37

**And herein is that [logos](../../strongs/g/g3056.md) [alēthinos](../../strongs/g/g228.md), One [speirō](../../strongs/g/g4687.md), and another [therizō](../../strongs/g/g2325.md).**

<a name="john_4_38"></a>John 4:38

**I [apostellō](../../strongs/g/g649.md) you to [therizō](../../strongs/g/g2325.md) that whereon ye no [kopiaō](../../strongs/g/g2872.md): [allos](../../strongs/g/g243.md) [kopiaō](../../strongs/g/g2872.md), and ye are [eiserchomai](../../strongs/g/g1525.md) into their [kopos](../../strongs/g/g2873.md).**

<a name="john_4_39"></a>John 4:39

And [polys](../../strongs/g/g4183.md) of the [Samaritēs](../../strongs/g/g4541.md) of that [polis](../../strongs/g/g4172.md) [pisteuō](../../strongs/g/g4100.md) on him for the [logos](../../strongs/g/g3056.md) of the [gynē](../../strongs/g/g1135.md), which [martyreō](../../strongs/g/g3140.md), He [eipon](../../strongs/g/g2036.md) me all that ever I [poieō](../../strongs/g/g4160.md).

<a name="john_4_40"></a>John 4:40

So when the [Samaritēs](../../strongs/g/g4541.md) were [erchomai](../../strongs/g/g2064.md) unto him, they [erōtaō](../../strongs/g/g2065.md) him that he would [menō](../../strongs/g/g3306.md) with them: and he [menō](../../strongs/g/g3306.md) there two [hēmera](../../strongs/g/g2250.md).

<a name="john_4_41"></a>John 4:41

And [polys](../../strongs/g/g4183.md) more [pisteuō](../../strongs/g/g4100.md) because of his own [logos](../../strongs/g/g3056.md);

<a name="john_4_42"></a>John 4:42

And [legō](../../strongs/g/g3004.md) unto the [gynē](../../strongs/g/g1135.md), Now we [pisteuō](../../strongs/g/g4100.md), not because of thy [lalia](../../strongs/g/g2981.md): for we have [akouō](../../strongs/g/g191.md) ourselves, and [eidō](../../strongs/g/g1492.md) that this is [alēthōs](../../strongs/g/g230.md) the [Christos](../../strongs/g/g5547.md), the [sōtēr](../../strongs/g/g4990.md) of the [kosmos](../../strongs/g/g2889.md).

<a name="john_4_43"></a>John 4:43

Now after two [hēmera](../../strongs/g/g2250.md) he [aperchomai](../../strongs/g/g565.md) [exerchomai](../../strongs/g/g1831.md) thence, and went into [Galilaia](../../strongs/g/g1056.md).

<a name="john_4_44"></a>John 4:44

For [Iēsous](../../strongs/g/g2424.md) himself [martyreō](../../strongs/g/g3140.md), that a [prophētēs](../../strongs/g/g4396.md) hath no [timē](../../strongs/g/g5092.md) in his own [patris](../../strongs/g/g3968.md).

<a name="john_4_45"></a>John 4:45

Then when he was [erchomai](../../strongs/g/g2064.md) into [Galilaia](../../strongs/g/g1056.md), the [Galilaios](../../strongs/g/g1057.md) [dechomai](../../strongs/g/g1209.md) him, having [horaō](../../strongs/g/g3708.md) all the things that he [poieō](../../strongs/g/g4160.md) at [Hierosolyma](../../strongs/g/g2414.md) at the [heortē](../../strongs/g/g1859.md): for they also [erchomai](../../strongs/g/g2064.md) unto the [heortē](../../strongs/g/g1859.md).

<a name="john_4_46"></a>John 4:46

So [Iēsous](../../strongs/g/g2424.md) [erchomai](../../strongs/g/g2064.md) again into [Kana](../../strongs/g/g2580.md) of [Galilaia](../../strongs/g/g1056.md), where he [poieō](../../strongs/g/g4160.md) the [hydōr](../../strongs/g/g5204.md) [oinos](../../strongs/g/g3631.md). And there was a certain [basilikos](../../strongs/g/g937.md), whose [huios](../../strongs/g/g5207.md) was [astheneō](../../strongs/g/g770.md) at [Kapharnaoum](../../strongs/g/g2584.md).

<a name="john_4_47"></a>John 4:47

When he [akouō](../../strongs/g/g191.md) that [Iēsous](../../strongs/g/g2424.md) was [hēkō](../../strongs/g/g2240.md) out of [Ioudaia](../../strongs/g/g2449.md) into [Galilaia](../../strongs/g/g1056.md), he [aperchomai](../../strongs/g/g565.md) unto him, and [erōtaō](../../strongs/g/g2065.md) him that he would [katabainō](../../strongs/g/g2597.md), and [iaomai](../../strongs/g/g2390.md) his [huios](../../strongs/g/g5207.md): for he was [mellō](../../strongs/g/g3195.md) of [apothnēskō](../../strongs/g/g599.md).

<a name="john_4_48"></a>John 4:48

Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) unto him, **Except ye [eidō](../../strongs/g/g1492.md) [sēmeion](../../strongs/g/g4592.md) and [teras](../../strongs/g/g5059.md), ye will not [pisteuō](../../strongs/g/g4100.md).**

<a name="john_4_49"></a>John 4:49

The [basilikos](../../strongs/g/g937.md) [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), [katabainō](../../strongs/g/g2597.md) ere my [paidion](../../strongs/g/g3813.md) [apothnēskō](../../strongs/g/g599.md).

<a name="john_4_50"></a>John 4:50

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, **[poreuō](../../strongs/g/g4198.md); thy [huios](../../strongs/g/g5207.md) [zaō](../../strongs/g/g2198.md).** And the [anthrōpos](../../strongs/g/g444.md) [pisteuō](../../strongs/g/g4100.md) the [logos](../../strongs/g/g3056.md) that [Iēsous](../../strongs/g/g2424.md) had [eipon](../../strongs/g/g2036.md) unto him, and he [poreuō](../../strongs/g/g4198.md).

<a name="john_4_51"></a>John 4:51

And as he was now [katabainō](../../strongs/g/g2597.md), his [doulos](../../strongs/g/g1401.md) [apantaō](../../strongs/g/g528.md) him, and [apaggellō](../../strongs/g/g518.md), [legō](../../strongs/g/g3004.md), Thy [pais](../../strongs/g/g3816.md) [zaō](../../strongs/g/g2198.md).

<a name="john_4_52"></a>John 4:52

Then [pynthanomai](../../strongs/g/g4441.md) he of them the [hōra](../../strongs/g/g5610.md) when he began to [kompsoteron](../../strongs/g/g2866.md). And they [eipon](../../strongs/g/g2036.md) unto him, [echthes](../../strongs/g/g5504.md) at the seventh [hōra](../../strongs/g/g5610.md) the [pyretos](../../strongs/g/g4446.md) [aphiēmi](../../strongs/g/g863.md) him.

<a name="john_4_53"></a>John 4:53

So the [patēr](../../strongs/g/g3962.md) [ginōskō](../../strongs/g/g1097.md) that was at the same [hōra](../../strongs/g/g5610.md), in the which [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **Thy [huios](../../strongs/g/g5207.md) [zaō](../../strongs/g/g2198.md)**: and himself [pisteuō](../../strongs/g/g4100.md), and his [holos](../../strongs/g/g3650.md) [oikia](../../strongs/g/g3614.md).

<a name="john_4_54"></a>John 4:54

This again the second [sēmeion](../../strongs/g/g4592.md) [Iēsous](../../strongs/g/g2424.md) [poieō](../../strongs/g/g4160.md), when he was [erchomai](../../strongs/g/g2064.md) of [Ioudaia](../../strongs/g/g2449.md) into [Galilaia](../../strongs/g/g1056.md).

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 3](john_3.md) - [John 5](john_5.md)