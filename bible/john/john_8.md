# [John 8](https://www.blueletterbible.org/kjv/jhn/8/1/p0/rl1/s_1005001)

<a name="john_8_1"></a>John 8:1

[Iēsous](../../strongs/g/g2424.md) [poreuō](../../strongs/g/g4198.md) unto the [oros](../../strongs/g/g3735.md) of [elaia](../../strongs/g/g1636.md). [^1]

<a name="john_8_2"></a>John 8:2

And [orthros](../../strongs/g/g3722.md) he [paraginomai](../../strongs/g/g3854.md) [palin](../../strongs/g/g3825.md) into the [hieron](../../strongs/g/g2411.md), and all the [laos](../../strongs/g/g2992.md) [erchomai](../../strongs/g/g2064.md) unto him; and he [kathizō](../../strongs/g/g2523.md), and [didaskō](../../strongs/g/g1321.md) them.

<a name="john_8_3"></a>John 8:3

And the [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md) [agō](../../strongs/g/g71.md) unto him a [gynē](../../strongs/g/g1135.md) [katalambanō](../../strongs/g/g2638.md) in [moicheia](../../strongs/g/g3430.md); and when they had [histēmi](../../strongs/g/g2476.md) her in the [mesos](../../strongs/g/g3319.md),

<a name="john_8_4"></a>John 8:4

They [legō](../../strongs/g/g3004.md) unto him, [didaskalos](../../strongs/g/g1320.md), this [gynē](../../strongs/g/g1135.md) was [katalambanō](../../strongs/g/g2638.md) in [moicheuō](../../strongs/g/g3431.md), in [autophōros](../../strongs/g/g1888.md).

<a name="john_8_5"></a>John 8:5

Now [Mōÿsēs](../../strongs/g/g3475.md) in the [nomos](../../strongs/g/g3551.md) [entellō](../../strongs/g/g1781.md) us, that such should be [lithoboleō](../../strongs/g/g3036.md): but what [legō](../../strongs/g/g3004.md) thou?

<a name="john_8_6"></a>John 8:6

This they [legō](../../strongs/g/g3004.md), [peirazō](../../strongs/g/g3985.md) him, that they might have to [katēgoreō](../../strongs/g/g2723.md) him. But [Iēsous](../../strongs/g/g2424.md) [kyptō](../../strongs/g/g2955.md) down, and with his [daktylos](../../strongs/g/g1147.md) [graphō](../../strongs/g/g1125.md) on the [gē](../../strongs/g/g1093.md), as though he [prospoieō](../../strongs/g/g4364.md) them not.

<a name="john_8_7"></a>John 8:7

So when they [epimenō](../../strongs/g/g1961.md) [erōtaō](../../strongs/g/g2065.md) him, he [anakyptō](../../strongs/g/g352.md), and [eipon](../../strongs/g/g2036.md) unto them, **[anamartētos](../../strongs/g/g361.md) among you, let him first [ballō](../../strongs/g/g906.md) a [lithos](../../strongs/g/g3037.md) at her.**

<a name="john_8_8"></a>John 8:8

And again he [kyptō](../../strongs/g/g2955.md) down, and [graphō](../../strongs/g/g1125.md) on the [gē](../../strongs/g/g1093.md).

<a name="john_8_9"></a>John 8:9

And they which [akouō](../../strongs/g/g191.md) it, being [elegchō](../../strongs/g/g1651.md) by [syneidēsis](../../strongs/g/g4893.md), [exerchomai](../../strongs/g/g1831.md) [heis kath' heis](../../strongs/g/g1527.md), [archomai](../../strongs/g/g756.md) at the [presbyteros](../../strongs/g/g4245.md), even unto the [eschatos](../../strongs/g/g2078.md): and [Iēsous](../../strongs/g/g2424.md) was [kataleipō](../../strongs/g/g2641.md) alone, and the [gynē](../../strongs/g/g1135.md) [histēmi](../../strongs/g/g2476.md) in the [mesos](../../strongs/g/g3319.md).

<a name="john_8_10"></a>John 8:10

When [Iēsous](../../strongs/g/g2424.md) had [anakyptō](../../strongs/g/g352.md), and [theaomai](../../strongs/g/g2300.md) [mēdeis](../../strongs/g/g3367.md) but the [gynē](../../strongs/g/g1135.md), he [eipon](../../strongs/g/g2036.md) unto her, **[gynē](../../strongs/g/g1135.md), where are those thine [katēgoros](../../strongs/g/g2725.md)? hath no man [katakrinō](../../strongs/g/g2632.md) thee?**

<a name="john_8_11"></a>John 8:11

She [eipon](../../strongs/g/g2036.md), [oudeis](../../strongs/g/g3762.md), [kyrios](../../strongs/g/g2962.md). And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto her, **Neither do I [katakrinō](../../strongs/g/g2632.md) thee: [poreuō](../../strongs/g/g4198.md), and [hamartanō](../../strongs/g/g264.md) [mēketi](../../strongs/g/g3371.md).**

<a name="john_8_12"></a>John 8:12

Then [laleō](../../strongs/g/g2980.md) [Iēsous](../../strongs/g/g2424.md) again unto them, [legō](../../strongs/g/g3004.md), **I am the [phōs](../../strongs/g/g5457.md) of the [kosmos](../../strongs/g/g2889.md): he that [akoloutheō](../../strongs/g/g190.md) me shall not [peripateō](../../strongs/g/g4043.md) in [skotia](../../strongs/g/g4653.md), but shall have the [phōs](../../strongs/g/g5457.md) of [zōē](../../strongs/g/g2222.md).**

<a name="john_8_13"></a>John 8:13

The [Pharisaios](../../strongs/g/g5330.md) therefore [eipon](../../strongs/g/g2036.md) unto him, Thou [martyreō](../../strongs/g/g3140.md) of thyself; thy [martyria](../../strongs/g/g3141.md) is not [alēthēs](../../strongs/g/g227.md).

<a name="john_8_14"></a>John 8:14

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **Though I [martyreō](../../strongs/g/g3140.md) of myself, yet my [martyria](../../strongs/g/g3141.md) is [alēthēs](../../strongs/g/g227.md): for I [eidō](../../strongs/g/g1492.md) whence I [erchomai](../../strongs/g/g2064.md), and whither I [hypagō](../../strongs/g/g5217.md); but ye cannot [eidō](../../strongs/g/g1492.md) whence I [erchomai](../../strongs/g/g2064.md), and whither I [hypagō](../../strongs/g/g5217.md).**

<a name="john_8_15"></a>John 8:15

**Ye [krinō](../../strongs/g/g2919.md) after the [sarx](../../strongs/g/g4561.md); I [krinō](../../strongs/g/g2919.md) [ou](../../strongs/g/g3756.md) [oudeis](../../strongs/g/g3762.md).**

<a name="john_8_16"></a>John 8:16

**And yet if I [krinō](../../strongs/g/g2919.md), my [krisis](../../strongs/g/g2920.md) is [alēthēs](../../strongs/g/g227.md): for I am not [monos](../../strongs/g/g3441.md), but I and the [patēr](../../strongs/g/g3962.md) that [pempō](../../strongs/g/g3992.md) me.**

<a name="john_8_17"></a>John 8:17

**It is also [graphō](../../strongs/g/g1125.md) in your [nomos](../../strongs/g/g3551.md), that the [martyria](../../strongs/g/g3141.md) of two [anthrōpos](../../strongs/g/g444.md) is [alēthēs](../../strongs/g/g227.md).**

<a name="john_8_18"></a>John 8:18

**I am one that [martyreō](../../strongs/g/g3140.md) of myself, and the [patēr](../../strongs/g/g3962.md) that [pempō](../../strongs/g/g3992.md) me [martyreō](../../strongs/g/g3140.md) of me.**

<a name="john_8_19"></a>John 8:19

Then [legō](../../strongs/g/g3004.md) they unto him, Where is thy [patēr](../../strongs/g/g3962.md)? [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md), **Ye neither [eidō](../../strongs/g/g1492.md) me, nor my [patēr](../../strongs/g/g3962.md): if ye had [eidō](../../strongs/g/g1492.md) me, ye should have [eidō](../../strongs/g/g1492.md) my [patēr](../../strongs/g/g3962.md) also.**

<a name="john_8_20"></a>John 8:20

These [rhēma](../../strongs/g/g4487.md) [laleō](../../strongs/g/g2980.md) [Iēsous](../../strongs/g/g2424.md) in the [gazophylakion](../../strongs/g/g1049.md), as he [didaskō](../../strongs/g/g1321.md) in the [hieron](../../strongs/g/g2411.md): and no man [piazō](../../strongs/g/g4084.md) him; for his [hōra](../../strongs/g/g5610.md) was not yet [erchomai](../../strongs/g/g2064.md).

<a name="john_8_21"></a>John 8:21

Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) again unto them, **I [hypagō](../../strongs/g/g5217.md), and ye shall [zēteō](../../strongs/g/g2212.md) me, and shall [apothnēskō](../../strongs/g/g599.md) in your [hamartia](../../strongs/g/g266.md): whither I [hypagō](../../strongs/g/g5217.md), ye cannot [erchomai](../../strongs/g/g2064.md).**

<a name="john_8_22"></a>John 8:22

Then [legō](../../strongs/g/g3004.md) the [Ioudaios](../../strongs/g/g2453.md), Will he [apokteinō](../../strongs/g/g615.md) himself? because he [legō](../../strongs/g/g3004.md), **Whither I [hypagō](../../strongs/g/g5217.md), ye cannot [erchomai](../../strongs/g/g2064.md).**

<a name="john_8_23"></a>John 8:23

And he [eipon](../../strongs/g/g2036.md) unto them, **Ye are from [katō](../../strongs/g/g2736.md); I am from [anō](../../strongs/g/g507.md): ye are of this [kosmos](../../strongs/g/g2889.md); I am not of this [kosmos](../../strongs/g/g2889.md).**

<a name="john_8_24"></a>John 8:24

**I [eipon](../../strongs/g/g2036.md) therefore unto you, that ye shall [apothnēskō](../../strongs/g/g599.md) in your [hamartia](../../strongs/g/g266.md): for if ye [pisteuō](../../strongs/g/g4100.md) not that I am he, ye shall [apothnēskō](../../strongs/g/g599.md) in your [hamartia](../../strongs/g/g266.md).**

<a name="john_8_25"></a>John 8:25

Then [legō](../../strongs/g/g3004.md) they unto him, Who art thou? And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **Even the same that I [laleō](../../strongs/g/g2980.md) unto you from the [archē](../../strongs/g/g746.md).**

<a name="john_8_26"></a>John 8:26

**I have [polys](../../strongs/g/g4183.md) to [laleō](../../strongs/g/g2980.md) and to [krinō](../../strongs/g/g2919.md) of you: but he that [pempō](../../strongs/g/g3992.md) me is [alēthēs](../../strongs/g/g227.md); and I [legō](../../strongs/g/g3004.md) to the [kosmos](../../strongs/g/g2889.md) those things which I have [akouō](../../strongs/g/g191.md) of him.**

<a name="john_8_27"></a>John 8:27

They [ginōskō](../../strongs/g/g1097.md) not that he [legō](../../strongs/g/g3004.md) to them of the [patēr](../../strongs/g/g3962.md).

<a name="john_8_28"></a>John 8:28

Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) unto them, **When ye have [hypsoō](../../strongs/g/g5312.md) the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md), then shall ye [ginōskō](../../strongs/g/g1097.md) that I am he, and that I [poieō](../../strongs/g/g4160.md) [oudeis](../../strongs/g/g3762.md) of myself; but as my [patēr](../../strongs/g/g3962.md) hath [didaskō](../../strongs/g/g1321.md) me, I [laleō](../../strongs/g/g2980.md) these things.**

<a name="john_8_29"></a>John 8:29

**And he that [pempō](../../strongs/g/g3992.md) me is with me: the [patēr](../../strongs/g/g3962.md) hath not [aphiēmi](../../strongs/g/g863.md) me [monos](../../strongs/g/g3441.md); for I [poieō](../../strongs/g/g4160.md) [pantote](../../strongs/g/g3842.md) [arestos](../../strongs/g/g701.md) him.**

<a name="john_8_30"></a>John 8:30

As he [laleō](../../strongs/g/g2980.md) [tauta](../../strongs/g/g5023.md), [polys](../../strongs/g/g4183.md) [pisteuō](../../strongs/g/g4100.md) on him.

<a name="john_8_31"></a>John 8:31

Then [legō](../../strongs/g/g3004.md) [Iēsous](../../strongs/g/g2424.md) to those [Ioudaios](../../strongs/g/g2453.md) which [pisteuō](../../strongs/g/g4100.md) on him, **If ye [menō](../../strongs/g/g3306.md) in my [logos](../../strongs/g/g3056.md), then are ye my [mathētēs](../../strongs/g/g3101.md) [alēthōs](../../strongs/g/g230.md);**

<a name="john_8_32"></a>John 8:32

**And ye shall [ginōskō](../../strongs/g/g1097.md) the [alētheia](../../strongs/g/g225.md), and the [alētheia](../../strongs/g/g225.md) shall [eleutheroō](../../strongs/g/g1659.md) you.**

<a name="john_8_33"></a>John 8:33

They [apokrinomai](../../strongs/g/g611.md) him, We be [Abraam](../../strongs/g/g11.md) [sperma](../../strongs/g/g4690.md), and were [pōpote](../../strongs/g/g4455.md) in [douleuō](../../strongs/g/g1398.md) to [oudeis](../../strongs/g/g3762.md): how [legō](../../strongs/g/g3004.md) thou, **Ye shall [ginomai](../../strongs/g/g1096.md) [eleutheros](../../strongs/g/g1658.md)?**

<a name="john_8_34"></a>John 8:34

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) them, **[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, Whosoever [poieō](../../strongs/g/g4160.md) [hamartia](../../strongs/g/g266.md) is the [doulos](../../strongs/g/g1401.md) of [hamartia](../../strongs/g/g266.md).**

<a name="john_8_35"></a>John 8:35

And the [doulos](../../strongs/g/g1401.md) [menō](../../strongs/g/g3306.md) not in the [oikia](../../strongs/g/g3614.md) for [aiōn](../../strongs/g/g165.md): but the [huios](../../strongs/g/g5207.md) [menō](../../strongs/g/g3306.md) [aiōn](../../strongs/g/g165.md).

<a name="john_8_36"></a>John 8:36

If the [huios](../../strongs/g/g5207.md) therefore shall make you [eleutheroō](../../strongs/g/g1659.md), ye shall be [eleutheros](../../strongs/g/g1658.md) [ontōs](../../strongs/g/g3689.md).

<a name="john_8_37"></a>John 8:37

I [eidō](../../strongs/g/g1492.md) that ye are [Abraam](../../strongs/g/g11.md) [sperma](../../strongs/g/g4690.md); but ye [zēteō](../../strongs/g/g2212.md) to [apokteinō](../../strongs/g/g615.md) me, because my [logos](../../strongs/g/g3056.md) hath no [chōreō](../../strongs/g/g5562.md) in you.

<a name="john_8_38"></a>John 8:38

I [laleō](../../strongs/g/g2980.md) that which I have [horaō](../../strongs/g/g3708.md) with my [patēr](../../strongs/g/g3962.md): and ye [poieō](../../strongs/g/g4160.md) that which ye have [horaō](../../strongs/g/g3708.md) with your [patēr](../../strongs/g/g3962.md).

<a name="john_8_39"></a>John 8:39

They [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, [Abraam](../../strongs/g/g11.md) is our [patēr](../../strongs/g/g3962.md). [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, If ye were [Abraam](../../strongs/g/g11.md) [teknon](../../strongs/g/g5043.md), ye would [poieō](../../strongs/g/g4160.md) the [ergon](../../strongs/g/g2041.md) of [Abraam](../../strongs/g/g11.md).

<a name="john_8_40"></a>John 8:40

But now ye [zēteō](../../strongs/g/g2212.md) to [apokteinō](../../strongs/g/g615.md) me, an [anthrōpos](../../strongs/g/g444.md) that hath [laleō](../../strongs/g/g2980.md) you the [alētheia](../../strongs/g/g225.md), which I have [akouō](../../strongs/g/g191.md) of [theos](../../strongs/g/g2316.md): this [poieō](../../strongs/g/g4160.md) not [Abraam](../../strongs/g/g11.md).

<a name="john_8_41"></a>John 8:41

Ye [poieō](../../strongs/g/g4160.md) the [ergon](../../strongs/g/g2041.md) of your [patēr](../../strongs/g/g3962.md). Then [eipon](../../strongs/g/g2036.md) they to him, We be not [gennaō](../../strongs/g/g1080.md) of [porneia](../../strongs/g/g4202.md); we have one [patēr](../../strongs/g/g3962.md), even [theos](../../strongs/g/g2316.md).

<a name="john_8_42"></a>John 8:42

[Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, If [theos](../../strongs/g/g2316.md) were your [patēr](../../strongs/g/g3962.md), ye would [agapaō](../../strongs/g/g25.md) me: for I [exerchomai](../../strongs/g/g1831.md) and [hēkō](../../strongs/g/g2240.md) from [theos](../../strongs/g/g2316.md); neither [erchomai](../../strongs/g/g2064.md) I of myself, but he [apostellō](../../strongs/g/g649.md) me.

<a name="john_8_43"></a>John 8:43

Why do ye not [ginōskō](../../strongs/g/g1097.md) my [lalia](../../strongs/g/g2981.md)? even because ye cannot [akouō](../../strongs/g/g191.md) my [logos](../../strongs/g/g3056.md).

<a name="john_8_44"></a>John 8:44

Ye are of [patēr](../../strongs/g/g3962.md) the [diabolos](../../strongs/g/g1228.md), and the [epithymia](../../strongs/g/g1939.md) of your [patēr](../../strongs/g/g3962.md) ye [thelō](../../strongs/g/g2309.md) [poieō](../../strongs/g/g4160.md). He was an [anthrōpoktonos](../../strongs/g/g443.md) from the [archē](../../strongs/g/g746.md), and [histēmi](../../strongs/g/g2476.md) not in the [alētheia](../../strongs/g/g225.md), because there is no [alētheia](../../strongs/g/g225.md) in him. When he [laleō](../../strongs/g/g2980.md) a [pseudos](../../strongs/g/g5579.md), he [laleō](../../strongs/g/g2980.md) of his own: for he is a [pseustēs](../../strongs/g/g5583.md), and the [patēr](../../strongs/g/g3962.md) of it.

<a name="john_8_45"></a>John 8:45

And because I [legō](../../strongs/g/g3004.md) you the [alētheia](../../strongs/g/g225.md), ye [pisteuō](../../strongs/g/g4100.md) me not.

<a name="john_8_46"></a>John 8:46

Which of you [elegchō](../../strongs/g/g1651.md) me of [hamartia](../../strongs/g/g266.md)? And if I [legō](../../strongs/g/g3004.md) the [alētheia](../../strongs/g/g225.md), why do ye not [pisteuō](../../strongs/g/g4100.md) me?

<a name="john_8_47"></a>John 8:47

He that is of [theos](../../strongs/g/g2316.md) heareth [theos](../../strongs/g/g2316.md) [rhēma](../../strongs/g/g4487.md): ye therefore [akouō](../../strongs/g/g191.md) them not, because ye are not of [theos](../../strongs/g/g2316.md).

<a name="john_8_48"></a>John 8:48

Then [apokrinomai](../../strongs/g/g611.md) the [Ioudaios](../../strongs/g/g2453.md), and [eipon](../../strongs/g/g2036.md) unto him, [legō](../../strongs/g/g3004.md) we not [kalōs](../../strongs/g/g2573.md) that thou art a [Samaritēs](../../strongs/g/g4541.md), and hast a [daimonion](../../strongs/g/g1140.md)?

<a name="john_8_49"></a>John 8:49

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md), I have not a [daimonion](../../strongs/g/g1140.md); but I [timaō](../../strongs/g/g5091.md) my [patēr](../../strongs/g/g3962.md), and ye [atimazō](../../strongs/g/g818.md) me.

<a name="john_8_50"></a>John 8:50

And I [zēteō](../../strongs/g/g2212.md) not mine own [doxa](../../strongs/g/g1391.md): there is one that [zēteō](../../strongs/g/g2212.md) and [krinō](../../strongs/g/g2919.md).

<a name="john_8_51"></a>John 8:51

[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, If [tis](../../strongs/g/g5100.md) [tēreō](../../strongs/g/g5083.md) my [logos](../../strongs/g/g3056.md), he shall [ou mē](../../strongs/g/g3364.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) [theōreō](../../strongs/g/g2334.md) [thanatos](../../strongs/g/g2288.md).

<a name="john_8_52"></a>John 8:52

Then [eipon](../../strongs/g/g2036.md) the [Ioudaios](../../strongs/g/g2453.md) unto him, Now we [ginōskō](../../strongs/g/g1097.md) that thou hast a [daimonion](../../strongs/g/g1140.md). [Abraam](../../strongs/g/g11.md) is [apothnēskō](../../strongs/g/g599.md), and the [prophētēs](../../strongs/g/g4396.md); and thou [legō](../../strongs/g/g3004.md), If [tis](../../strongs/g/g5100.md) [tēreō](../../strongs/g/g5083.md) my [logos](../../strongs/g/g3056.md), he shall [ou mē](../../strongs/g/g3364.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) [geuomai](../../strongs/g/g1089.md) of [thanatos](../../strongs/g/g2288.md).

<a name="john_8_53"></a>John 8:53

Art thou [meizōn](../../strongs/g/g3187.md) than our [patēr](../../strongs/g/g3962.md) [Abraam](../../strongs/g/g11.md), which is [apothnēskō](../../strongs/g/g599.md)? and the [prophētēs](../../strongs/g/g4396.md) are [apothnēskō](../../strongs/g/g599.md): whom [poieō](../../strongs/g/g4160.md) thou thyself?

<a name="john_8_54"></a>John 8:54

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md), If I [doxazō](../../strongs/g/g1392.md) myself, my [doxa](../../strongs/g/g1391.md) is nothing: it is my [patēr](../../strongs/g/g3962.md) that [doxazō](../../strongs/g/g1392.md) me; of whom ye [legō](../../strongs/g/g3004.md), that he is your [theos](../../strongs/g/g2316.md):

<a name="john_8_55"></a>John 8:55

Yet ye have not [ginōskō](../../strongs/g/g1097.md) him; but I [eidō](../../strongs/g/g1492.md) him: and if I should [eipon](../../strongs/g/g2036.md), I [eidō](../../strongs/g/g1492.md) him not, I shall be a [pseustēs](../../strongs/g/g5583.md) [homoios](../../strongs/g/g3664.md) unto you: but I [eidō](../../strongs/g/g1492.md) him, and [tēreō](../../strongs/g/g5083.md) his [logos](../../strongs/g/g3056.md).

<a name="john_8_56"></a>John 8:56

Your [patēr](../../strongs/g/g3962.md) [Abraam](../../strongs/g/g11.md) [agalliaō](../../strongs/g/g21.md) to [eidō](../../strongs/g/g1492.md) my [hēmera](../../strongs/g/g2250.md): and he [eidō](../../strongs/g/g1492.md) it, and was [chairō](../../strongs/g/g5463.md).

<a name="john_8_57"></a>John 8:57

Then [eipon](../../strongs/g/g2036.md) the [Ioudaios](../../strongs/g/g2453.md) unto him, Thou art not yet fifty [etos](../../strongs/g/g2094.md), and hast thou [horaō](../../strongs/g/g3708.md) [Abraam](../../strongs/g/g11.md)?

<a name="john_8_58"></a>John 8:58

[Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, [amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md), I [legō](../../strongs/g/g3004.md) unto you, Before [Abraam](../../strongs/g/g11.md) was, [egō](../../strongs/g/g1473.md) [eimi](../../strongs/g/g1510.md).

<a name="john_8_59"></a>John 8:59

Then they [airō](../../strongs/g/g142.md) [lithos](../../strongs/g/g3037.md) to [ballō](../../strongs/g/g906.md) at him: but [Iēsous](../../strongs/g/g2424.md) [kryptō](../../strongs/g/g2928.md) himself, and [exerchomai](../../strongs/g/g1831.md) of the [hieron](../../strongs/g/g2411.md), [dierchomai](../../strongs/g/g1330.md) through the [mesos](../../strongs/g/g3319.md) of them, and so [paragō](../../strongs/g/g3855.md). [^2]

---

[Transliteral Bible](../bible.md)

[John](john.md)

[John 7](john_7.md) - [John 9](john_9.md)

---

[^1]: [John 7:53 Commentary](../../commentary/john/john_7_commentary.md#john_7_53)

[^2]: [John 8:59 Commentary](../../commentary/john/john_8_commentary.md#john_8_59)
