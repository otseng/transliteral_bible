# Transliteral Bible

## New Testament

[Matthew](matthew/matthew.md)

[Mark](mark/mark.md)

[Luke](luke/luke.md)

[John](john/john.md)

[Acts](acts/acts.md)

[Romans](romans/romans.md)

[1 Corinthians](1corinthians/1corinthians.md)

[2 Corinthians](2corinthians/2corinthians.md)

[Galatians](galatians/galatians.md)

[Ephesians](ephesians/ephesians.md)

[Philippians](philippians/philippians.md)

[Colossians](colossians/colossians.md)

[1 Thessalonians](1thessalonians/1thessalonians.md)

[2 Thessalonians](2thessalonians/2thessalonians.md)

[1 Timothy](1timothy/1timothy.md)

[2 Timothy](2timothy/2timothy.md)

[Titus](titus/titus.md)

[Philemon](philemon/philemon.md)

[Hebrews](hebrews/hebrews.md)

[James](james/james.md)

[1 Peter](1peter/1peter.md)

[2 Peter](2peter/2peter.md)

[1 John](1john/1john.md)

[2 John](2john/2john.md)

[3 John](3john/3john.md)

[Jude](jude/jude.md)

[Revelation](revelation/revelation.md)

## Old Testament

[Genesis](genesis/genesis.md) - [Genesis LXX](genesis/genesis_lxx.md)

[Psalms](psalms/psalms.md) - [Psalms LXX](psalms/psalms_lxx.md)

---

[Home](../README.md)