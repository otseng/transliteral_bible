# [Matthew 4](https://www.blueletterbible.org/kjv/mat/4/1/rl1/s_930001)

<a name="matthew_4_1"></a>Matthew 4:1

Then was [Iēsous](../../strongs/g/g2424.md) [anagō](../../strongs/g/g321.md) of the [pneuma](../../strongs/g/g4151.md) into the [erēmos](../../strongs/g/g2048.md) to be [peirazō](../../strongs/g/g3985.md) of the [diabolos](../../strongs/g/g1228.md).

<a name="matthew_4_2"></a>Matthew 4:2

And when he had [nēsteuō](../../strongs/g/g3522.md) forty [hēmera](../../strongs/g/g2250.md) and forty [nyx](../../strongs/g/g3571.md), he was afterward [peinaō](../../strongs/g/g3983.md). [^1]

<a name="matthew_4_3"></a>Matthew 4:3

And when the [peirazō](../../strongs/g/g3985.md) [proserchomai](../../strongs/g/g4334.md) to him, he [eipon](../../strongs/g/g2036.md), If thou be the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), [eipon](../../strongs/g/g2036.md) that these [lithos](../../strongs/g/g3037.md) be made [artos](../../strongs/g/g740.md).

<a name="matthew_4_4"></a>Matthew 4:4

But he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), **It is [graphō](../../strongs/g/g1125.md), [anthrōpos](../../strongs/g/g444.md) shall not [zaō](../../strongs/g/g2198.md) by [artos](../../strongs/g/g740.md) [monos](../../strongs/g/g3441.md), but by every [rhēma](../../strongs/g/g4487.md) that [ekporeuomai](../../strongs/g/g1607.md) out of the [stoma](../../strongs/g/g4750.md) of [theos](../../strongs/g/g2316.md).** [^2]

<a name="matthew_4_5"></a>Matthew 4:5

Then the [diabolos](../../strongs/g/g1228.md) [paralambanō](../../strongs/g/g3880.md) him into the [hagios](../../strongs/g/g40.md) [polis](../../strongs/g/g4172.md), and [histēmi](../../strongs/g/g2476.md) him on a [pterygion](../../strongs/g/g4419.md) of the [hieron](../../strongs/g/g2411.md),

<a name="matthew_4_6"></a>Matthew 4:6

And [legō](../../strongs/g/g3004.md) unto him, If thou be the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), [ballō](../../strongs/g/g906.md) thyself down: for it is [graphō](../../strongs/g/g1125.md), He shall [entellō](../../strongs/g/g1781.md) his [aggelos](../../strongs/g/g32.md) concerning thee: and in [cheir](../../strongs/g/g5495.md) they shall [airō](../../strongs/g/g142.md) thee, lest at any time thou [proskoptō](../../strongs/g/g4350.md) thy [pous](../../strongs/g/g4228.md) against a [lithos](../../strongs/g/g3037.md). [^3]

<a name="matthew_4_7"></a>Matthew 4:7

[Iēsous](../../strongs/g/g2424.md) [phēmi](../../strongs/g/g5346.md) unto him, **It is [graphō](../../strongs/g/g1125.md) again, Thou shalt not [ekpeirazō](../../strongs/g/g1598.md) the [kyrios](../../strongs/g/g2962.md) thy [theos](../../strongs/g/g2316.md).**

<a name="matthew_4_8"></a>Matthew 4:8

Again, the [diabolos](../../strongs/g/g1228.md) [paralambanō](../../strongs/g/g3880.md) him into [lian](../../strongs/g/g3029.md) [hypsēlos](../../strongs/g/g5308.md) [oros](../../strongs/g/g3735.md), and [deiknyō](../../strongs/g/g1166.md) him all the [basileia](../../strongs/g/g932.md) of the [kosmos](../../strongs/g/g2889.md), and the [doxa](../../strongs/g/g1391.md) of them;

<a name="matthew_4_9"></a>Matthew 4:9

And [legō](../../strongs/g/g3004.md) unto him, [pas](../../strongs/g/g3956.md) [tauta](../../strongs/g/g5023.md) will I [didōmi](../../strongs/g/g1325.md) thee, if thou wilt [piptō](../../strongs/g/g4098.md) and [proskyneō](../../strongs/g/g4352.md) me.

<a name="matthew_4_10"></a>Matthew 4:10

Then [legō](../../strongs/g/g3004.md) [Iēsous](../../strongs/g/g2424.md) unto him, **[hypagō](../../strongs/g/g5217.md), [Satanas](../../strongs/g/g4567.md): for it is [graphō](../../strongs/g/g1125.md), Thou shalt [proskyneō](../../strongs/g/g4352.md) the [kyrios](../../strongs/g/g2962.md) thy [theos](../../strongs/g/g2316.md), and him only shalt thou [latreuō](../../strongs/g/g3000.md).** [^4]

<a name="matthew_4_11"></a>Matthew 4:11

Then the [diabolos](../../strongs/g/g1228.md) [aphiēmi](../../strongs/g/g863.md) him, and, [idou](../../strongs/g/g2400.md), [aggelos](../../strongs/g/g32.md) [proserchomai](../../strongs/g/g4334.md) and [diakoneō](../../strongs/g/g1247.md) unto him.

<a name="matthew_4_12"></a>Matthew 4:12

Now when [Iēsous](../../strongs/g/g2424.md) had [akouō](../../strongs/g/g191.md) that [Iōannēs](../../strongs/g/g2491.md) was [paradidōmi](../../strongs/g/g3860.md), he [anachōreō](../../strongs/g/g402.md) into [Galilaia](../../strongs/g/g1056.md); [^5]

<a name="matthew_4_13"></a>Matthew 4:13

And [kataleipō](../../strongs/g/g2641.md) [Nazara](../../strongs/g/g3478.md), he [erchomai](../../strongs/g/g2064.md) and [katoikeō](../../strongs/g/g2730.md) in [Kapharnaoum](../../strongs/g/g2584.md), which is upon the [parathalassios](../../strongs/g/g3864.md), in the [horion](../../strongs/g/g3725.md) of [Zaboulōn](../../strongs/g/g2194.md) and [Nephthalim](../../strongs/g/g3508.md):

<a name="matthew_4_14"></a>Matthew 4:14

That it might be [plēroō](../../strongs/g/g4137.md) which was [rheō](../../strongs/g/g4483.md) by [Ēsaïas](../../strongs/g/g2268.md) the [prophētēs](../../strongs/g/g4396.md), [legō](../../strongs/g/g3004.md),

<a name="matthew_4_15"></a>Matthew 4:15

The [gē](../../strongs/g/g1093.md) of [Zaboulōn](../../strongs/g/g2194.md), and the [gē](../../strongs/g/g1093.md) of [Nephthalim](../../strongs/g/g3508.md), the [hodos](../../strongs/g/g3598.md) of the [thalassa](../../strongs/g/g2281.md), beyond [Iordanēs](../../strongs/g/g2446.md), [Galilaia](../../strongs/g/g1056.md) of the [ethnos](../../strongs/g/g1484.md);

<a name="matthew_4_16"></a>Matthew 4:16

The [laos](../../strongs/g/g2992.md) which [kathēmai](../../strongs/g/g2521.md) in [skotos](../../strongs/g/g4655.md) [eidō](../../strongs/g/g1492.md) [megas](../../strongs/g/g3173.md) [phōs](../../strongs/g/g5457.md); and to them which [kathēmai](../../strongs/g/g2521.md) in the [chōra](../../strongs/g/g5561.md) and [skia](../../strongs/g/g4639.md) of [thanatos](../../strongs/g/g2288.md) [phōs](../../strongs/g/g5457.md) is [anatellō](../../strongs/g/g393.md).

<a name="matthew_4_17"></a>Matthew 4:17

From that time [Iēsous](../../strongs/g/g2424.md) [archomai](../../strongs/g/g756.md) to [kēryssō](../../strongs/g/g2784.md), and to [legō](../../strongs/g/g3004.md), **[metanoeō](../../strongs/g/g3340.md): for the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) [eggizō](../../strongs/g/g1448.md).** [^6]

<a name="matthew_4_18"></a>Matthew 4:18

And [Iēsous](../../strongs/g/g2424.md), [peripateō](../../strongs/g/g4043.md) by the [thalassa](../../strongs/g/g2281.md) of [Galilaia](../../strongs/g/g1056.md), [eidō](../../strongs/g/g1492.md) two [adelphos](../../strongs/g/g80.md), [Simōn](../../strongs/g/g4613.md) [legō](../../strongs/g/g3004.md) [Petros](../../strongs/g/g4074.md), and [Andreas](../../strongs/g/g406.md) his [adelphos](../../strongs/g/g80.md), [ballō](../../strongs/g/g906.md) an [amphiblēstron](../../strongs/g/g293.md) into the [thalassa](../../strongs/g/g2281.md): for they were [halieus](../../strongs/g/g231.md). [^7]

<a name="matthew_4_19"></a>Matthew 4:19

And he [legō](../../strongs/g/g3004.md) unto them, **[deute](../../strongs/g/g1205.md) [opisō](../../strongs/g/g3694.md) me, and I will [poieō](../../strongs/g/g4160.md) you [halieus](../../strongs/g/g231.md) of [anthrōpos](../../strongs/g/g444.md).**

<a name="matthew_4_20"></a>Matthew 4:20

And they [eutheōs](../../strongs/g/g2112.md) [aphiēmi](../../strongs/g/g863.md) their [diktyon](../../strongs/g/g1350.md), and [akoloutheō](../../strongs/g/g190.md) him.

<a name="matthew_4_21"></a>Matthew 4:21

And [probainō](../../strongs/g/g4260.md) from thence, he [eidō](../../strongs/g/g1492.md) other two [adelphos](../../strongs/g/g80.md), [Iakōbos](../../strongs/g/g2385.md) the son of [Zebedaios](../../strongs/g/g2199.md), and [Iōannēs](../../strongs/g/g2491.md) his [adelphos](../../strongs/g/g80.md), in a [ploion](../../strongs/g/g4143.md) with [Zebedaios](../../strongs/g/g2199.md) their [patēr](../../strongs/g/g3962.md), [katartizō](../../strongs/g/g2675.md) their [diktyon](../../strongs/g/g1350.md); and he [kaleō](../../strongs/g/g2564.md) them. [^8]

<a name="matthew_4_22"></a>Matthew 4:22

And they [eutheōs](../../strongs/g/g2112.md) [aphiēmi](../../strongs/g/g863.md) the [ploion](../../strongs/g/g4143.md) and their [patēr](../../strongs/g/g3962.md), and [akoloutheō](../../strongs/g/g190.md) him.

<a name="matthew_4_23"></a>Matthew 4:23

And [Iēsous](../../strongs/g/g2424.md) [periagō](../../strongs/g/g4013.md) all [Galilaia](../../strongs/g/g1056.md), [didaskō](../../strongs/g/g1321.md) in their [synagōgē](../../strongs/g/g4864.md), and [kēryssō](../../strongs/g/g2784.md) the [euaggelion](../../strongs/g/g2098.md) of the [basileia](../../strongs/g/g932.md), and [therapeuō](../../strongs/g/g2323.md) all [nosos](../../strongs/g/g3554.md) and all [malakia](../../strongs/g/g3119.md) among the [laos](../../strongs/g/g2992.md). [^9]

<a name="matthew_4_24"></a>Matthew 4:24

And his [akoē](../../strongs/g/g189.md) [aperchomai](../../strongs/g/g565.md) throughout all [Syria](../../strongs/g/g4947.md): and they [prospherō](../../strongs/g/g4374.md) unto him all [kakōs](../../strongs/g/g2560.md) [echō](../../strongs/g/g2192.md) that were [synechō](../../strongs/g/g4912.md) with [poikilos](../../strongs/g/g4164.md) [nosos](../../strongs/g/g3554.md) and [basanos](../../strongs/g/g931.md), and [daimonizomai](../../strongs/g/g1139.md), and [selēniazomai](../../strongs/g/g4583.md), and [paralytikos](../../strongs/g/g3885.md); and he [therapeuō](../../strongs/g/g2323.md) them. [^10]

<a name="matthew_4_25"></a>Matthew 4:25

And there [akoloutheō](../../strongs/g/g190.md) him [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) from [Galilaia](../../strongs/g/g1056.md), and from [Dekapolis](../../strongs/g/g1179.md), and from [Hierosolyma](../../strongs/g/g2414.md), and from [Ioudaia](../../strongs/g/g2449.md), and from beyond [Iordanēs](../../strongs/g/g2446.md).

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 3](matthew_3.md) - [Matthew 5](matthew_5.md)

---

[^1]: [Matthew 4:2 Commentary](../../commentary/matthew/matthew_4_commentary.md#matthew_4_2)

[^2]: [Matthew 4:4 Commentary](../../commentary/matthew/matthew_4_commentary.md#matthew_4_4)

[^3]: [Matthew 4:6 Commentary](../../commentary/matthew/matthew_4_commentary.md#matthew_4_6)

[^4]: [Matthew 4:10 Commentary](../../commentary/matthew/matthew_4_commentary.md#matthew_4_10)

[^5]: [Matthew 4:12 Commentary](../../commentary/matthew/matthew_4_commentary.md#matthew_4_12)

[^6]: [Matthew 4:17 Commentary](../../commentary/matthew/matthew_4_commentary.md#matthew_4_17)

[^7]: [Matthew 4:18 Commentary](../../commentary/matthew/matthew_4_commentary.md#matthew_4_18)

[^8]: [Matthew 4:21 Commentary](../../commentary/matthew/matthew_4_commentary.md#matthew_4_21)

[^9]: [Matthew 4:23 Commentary](../../commentary/matthew/matthew_4_commentary.md#matthew_4_23)

[^10]: [Matthew 4:24 Commentary](../../commentary/matthew/matthew_4_commentary.md#matthew_4_24)
