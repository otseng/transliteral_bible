# [Matthew 23](https://www.blueletterbible.org/kjv/mat/23/1/rl1/s_952001)

<a name="matthew_23_1"></a>Matthew 23:1

Then [laleō](../../strongs/g/g2980.md) [Iēsous](../../strongs/g/g2424.md) to the [ochlos](../../strongs/g/g3793.md), and to his [mathētēs](../../strongs/g/g3101.md),

<a name="matthew_23_2"></a>Matthew 23:2

[legō](../../strongs/g/g3004.md) **The [grammateus](../../strongs/g/g1122.md) and the [Pharisaios](../../strongs/g/g5330.md) [kathizō](../../strongs/g/g2523.md) in [Mōÿsēs](../../strongs/g/g3475.md) [kathedra](../../strongs/g/g2515.md):**

<a name="matthew_23_3"></a>Matthew 23:3

**All therefore whatsoever they [eipon](../../strongs/g/g2036.md) you [tēreō](../../strongs/g/g5083.md), that [tēreō](../../strongs/g/g5083.md) and [poieō](../../strongs/g/g4160.md); but [poieō](../../strongs/g/g4160.md) not ye after their [ergon](../../strongs/g/g2041.md): for they [legō](../../strongs/g/g3004.md), and [poieō](../../strongs/g/g4160.md) not.**

<a name="matthew_23_4"></a>Matthew 23:4

**For they [desmeuō](../../strongs/g/g1195.md) [barys](../../strongs/g/g926.md) [phortion](../../strongs/g/g5413.md) and [dysbastaktos](../../strongs/g/g1419.md), and [epitithēmi](../../strongs/g/g2007.md) them on [anthrōpos](../../strongs/g/g444.md) [ōmos](../../strongs/g/g5606.md); but they themselves will not [kineō](../../strongs/g/g2795.md) them with one of their [daktylos](../../strongs/g/g1147.md).**

<a name="matthew_23_5"></a>Matthew 23:5

**But all their [ergon](../../strongs/g/g2041.md) they [poieō](../../strongs/g/g4160.md) for [theaomai](../../strongs/g/g2300.md) of [anthrōpos](../../strongs/g/g444.md): they [platynō](../../strongs/g/g4115.md) their [phylaktērion](../../strongs/g/g5440.md), and [megalynō](../../strongs/g/g3170.md) the [kraspedon](../../strongs/g/g2899.md) of their [himation](../../strongs/g/g2440.md),**

<a name="matthew_23_6"></a>Matthew 23:6

**And [phileō](../../strongs/g/g5368.md) the [prōtoklisia](../../strongs/g/g4411.md) at [deipnon](../../strongs/g/g1173.md), and the [prōtokathedria](../../strongs/g/g4410.md) in the [synagōgē](../../strongs/g/g4864.md),**

<a name="matthew_23_7"></a>Matthew 23:7

**And [aspasmos](../../strongs/g/g783.md) in the [agora](../../strongs/g/g58.md), and to be [kaleō](../../strongs/g/g2564.md) of [anthrōpos](../../strongs/g/g444.md), [rhabbi](../../strongs/g/g4461.md), [rhabbi](../../strongs/g/g4461.md).**

<a name="matthew_23_8"></a>Matthew 23:8

**But be not ye [kaleō](../../strongs/g/g2564.md) [rhabbi](../../strongs/g/g4461.md): for one is your [kathēgētēs](../../strongs/g/g2519.md), even [Christos](../../strongs/g/g5547.md); and all ye are [adelphos](../../strongs/g/g80.md).**

<a name="matthew_23_9"></a>Matthew 23:9

**And [kaleō](../../strongs/g/g2564.md) no your [patēr](../../strongs/g/g3962.md) upon the [gē](../../strongs/g/g1093.md): for one is your [patēr](../../strongs/g/g3962.md), which is in [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_23_10"></a>Matthew 23:10

**Neither be ye [kaleō](../../strongs/g/g2564.md) [kathēgētēs](../../strongs/g/g2519.md): for one is your [kathēgētēs](../../strongs/g/g2519.md), even [Christos](../../strongs/g/g5547.md).**

<a name="matthew_23_11"></a>Matthew 23:11

**But he that is [meizōn](../../strongs/g/g3187.md) among you shall be your [diakonos](../../strongs/g/g1249.md).**

<a name="matthew_23_12"></a>Matthew 23:12

**And whosoever shall [hypsoō](../../strongs/g/g5312.md) himself shall be [tapeinoō](../../strongs/g/g5013.md); and he that shall [tapeinoō](../../strongs/g/g5013.md) himself shall be [hypsoō](../../strongs/g/g5312.md).**

<a name="matthew_23_13"></a>Matthew 23:13

**But [ouai](../../strongs/g/g3759.md) unto you, [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md), [hypokritēs](../../strongs/g/g5273.md)! for ye [kleiō](../../strongs/g/g2808.md) the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) against [anthrōpos](../../strongs/g/g444.md): for ye neither [eiserchomai](../../strongs/g/g1525.md), neither [aphiēmi](../../strongs/g/g863.md) them that are [eiserchomai](../../strongs/g/g1525.md).**

<a name="matthew_23_14"></a>Matthew 23:14

**[ouai](../../strongs/g/g3759.md) unto you, [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md), [hypokritēs](../../strongs/g/g5273.md)! for ye [katesthiō](../../strongs/g/g2719.md) [chēra](../../strongs/g/g5503.md) [oikia](../../strongs/g/g3614.md), and for a [prophasis](../../strongs/g/g4392.md) [proseuchomai](../../strongs/g/g4336.md) [makros](../../strongs/g/g3117.md): therefore ye shall [lambanō](../../strongs/g/g2983.md) the [perissoteros](../../strongs/g/g4055.md) [krima](../../strongs/g/g2917.md).** [^1]

<a name="matthew_23_15"></a>Matthew 23:15

**[ouai](../../strongs/g/g3759.md) unto you, [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md), [hypokritēs](../../strongs/g/g5273.md)! for ye [periagō](../../strongs/g/g4013.md) [thalassa](../../strongs/g/g2281.md) and [xēros](../../strongs/g/g3584.md) to [poieō](../../strongs/g/g4160.md) one [prosēlytos](../../strongs/g/g4339.md), and when he is made, ye [poieō](../../strongs/g/g4160.md) him twofold more the [huios](../../strongs/g/g5207.md) of [geenna](../../strongs/g/g1067.md) than yourselves.**

<a name="matthew_23_16"></a>Matthew 23:16

**[ouai](../../strongs/g/g3759.md) unto you, ye [typhlos](../../strongs/g/g5185.md) [hodēgos](../../strongs/g/g3595.md), which [legō](../../strongs/g/g3004.md), Whosoever shall [omnyō](../../strongs/g/g3660.md) by the [naos](../../strongs/g/g3485.md), it is nothing; but whosoever shall [omnyō](../../strongs/g/g3660.md) by the [chrysos](../../strongs/g/g5557.md) of the [naos](../../strongs/g/g3485.md), he is [opheilō](../../strongs/g/g3784.md)!**

<a name="matthew_23_17"></a>Matthew 23:17

**[mōros](../../strongs/g/g3474.md) and [typhlos](../../strongs/g/g5185.md): for whether is [meizōn](../../strongs/g/g3187.md), the [chrysos](../../strongs/g/g5557.md), or the [naos](../../strongs/g/g3485.md) that [hagiazō](../../strongs/g/g37.md) the [chrysos](../../strongs/g/g5557.md)?**

<a name="matthew_23_18"></a>Matthew 23:18

**And, Whosoever shall [omnyō](../../strongs/g/g3660.md) by the [thysiastērion](../../strongs/g/g2379.md), it is [oudeis](../../strongs/g/g3762.md); but whosoever [omnyō](../../strongs/g/g3660.md) by the [dōron](../../strongs/g/g1435.md) that is upon it, he is [opheilō](../../strongs/g/g3784.md).**

<a name="matthew_23_19"></a>Matthew 23:19

**Ye [mōros](../../strongs/g/g3474.md) and [typhlos](../../strongs/g/g5185.md): for whether [meizōn](../../strongs/g/g3187.md), the [dōron](../../strongs/g/g1435.md), or the [thysiastērion](../../strongs/g/g2379.md) that [hagiazō](../../strongs/g/g37.md) the [dōron](../../strongs/g/g1435.md)?**

<a name="matthew_23_20"></a>Matthew 23:20

**Whoso therefore shall [omnyō](../../strongs/g/g3660.md) by the [thysiastērion](../../strongs/g/g2379.md), [omnyō](../../strongs/g/g3660.md) by it, and by all things thereon.**

<a name="matthew_23_21"></a>Matthew 23:21

**And whoso shall [omnyō](../../strongs/g/g3660.md) by the [naos](../../strongs/g/g3485.md), [omnyō](../../strongs/g/g3660.md) by it, and by him that [katoikeō](../../strongs/g/g2730.md) therein.**

<a name="matthew_23_22"></a>Matthew 23:22

**And he that shall [omnyō](../../strongs/g/g3660.md) by [ouranos](../../strongs/g/g3772.md), [omnyō](../../strongs/g/g3660.md) by the [thronos](../../strongs/g/g2362.md) of [theos](../../strongs/g/g2316.md), and by him that [kathēmai](../../strongs/g/g2521.md) thereon.**

<a name="matthew_23_23"></a>Matthew 23:23

**[ouai](../../strongs/g/g3759.md) unto you, [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md), [hypokritēs](../../strongs/g/g5273.md)! for ye [apodekatoō](../../strongs/g/g586.md) of [hēdyosmon](../../strongs/g/g2238.md) and [anēthon](../../strongs/g/g432.md) and [kyminon](../../strongs/g/g2951.md), and have [aphiēmi](../../strongs/g/g863.md) the [barys](../../strongs/g/g926.md) of the [nomos](../../strongs/g/g3551.md), [krisis](../../strongs/g/g2920.md), [eleos](../../strongs/g/g1656.md), and [pistis](../../strongs/g/g4102.md): these ought ye to have [poieō](../../strongs/g/g4160.md), and not to [aphiēmi](../../strongs/g/g863.md) the other [aphiēmi](../../strongs/g/g863.md).**

<a name="matthew_23_24"></a>Matthew 23:24

**[typhlos](../../strongs/g/g5185.md) [hodēgos](../../strongs/g/g3595.md), which [diÿlizō](../../strongs/g/g1368.md) at a [kōnōps](../../strongs/g/g2971.md), and [katapinō](../../strongs/g/g2666.md) a [kamēlos](../../strongs/g/g2574.md).**

<a name="matthew_23_25"></a>Matthew 23:25

**[ouai](../../strongs/g/g3759.md) unto you, [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md), [hypokritēs](../../strongs/g/g5273.md)! for ye [katharizō](../../strongs/g/g2511.md) the [exōthen](../../strongs/g/g1855.md) of the [potērion](../../strongs/g/g4221.md) and of the [paropsis](../../strongs/g/g3953.md), but [esōthen](../../strongs/g/g2081.md) they are [gemō](../../strongs/g/g1073.md) of [harpagē](../../strongs/g/g724.md) and [akrasia](../../strongs/g/g192.md).**

<a name="matthew_23_26"></a>Matthew 23:26

**[typhlos](../../strongs/g/g5185.md) [Pharisaios](../../strongs/g/g5330.md), [katharizō](../../strongs/g/g2511.md) first that is [entos](../../strongs/g/g1787.md) the [potērion](../../strongs/g/g4221.md) and [paropsis](../../strongs/g/g3953.md), that the [ektos](../../strongs/g/g1622.md) of them may be [katharos](../../strongs/g/g2513.md) also.** [^2]

<a name="matthew_23_27"></a>Matthew 23:27

**[ouai](../../strongs/g/g3759.md) unto you, [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md), [hypokritēs](../../strongs/g/g5273.md)! for ye are [paromoiazō](../../strongs/g/g3945.md) unto [koniaō](../../strongs/g/g2867.md) [taphos](../../strongs/g/g5028.md), which [men](../../strongs/g/g3303.md) [phainō](../../strongs/g/g5316.md) [hōraios](../../strongs/g/g5611.md) [exōthen](../../strongs/g/g1855.md), but are [esōthen](../../strongs/g/g2081.md) [gemō](../../strongs/g/g1073.md) of [nekros](../../strongs/g/g3498.md) [osteon](../../strongs/g/g3747.md), and of all [akatharsia](../../strongs/g/g167.md).**

<a name="matthew_23_28"></a>Matthew 23:28

**Even so ye also [exōthen](../../strongs/g/g1855.md) [phainō](../../strongs/g/g5316.md) [dikaios](../../strongs/g/g1342.md) unto [anthrōpos](../../strongs/g/g444.md), but [esōthen](../../strongs/g/g2081.md) ye are [mestos](../../strongs/g/g3324.md) of [hypokrisis](../../strongs/g/g5272.md) and [anomia](../../strongs/g/g458.md).**

<a name="matthew_23_29"></a>Matthew 23:29

**[ouai](../../strongs/g/g3759.md) unto you, [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md), [hypokritēs](../../strongs/g/g5273.md)! because ye [oikodomeō](../../strongs/g/g3618.md) the [taphos](../../strongs/g/g5028.md) of the [prophētēs](../../strongs/g/g4396.md), and [kosmeō](../../strongs/g/g2885.md) the [mnēmeion](../../strongs/g/g3419.md) of the [dikaios](../../strongs/g/g1342.md),**

<a name="matthew_23_30"></a>Matthew 23:30

**And [legō](../../strongs/g/g3004.md), If we had been in the [hēmera](../../strongs/g/g2250.md) of our [patēr](../../strongs/g/g3962.md), we would not have been [koinōnos](../../strongs/g/g2844.md) with them in the [haima](../../strongs/g/g129.md) of the [prophētēs](../../strongs/g/g4396.md).**

<a name="matthew_23_31"></a>Matthew 23:31

**Wherefore ye be [martyreō](../../strongs/g/g3140.md) unto yourselves, that ye are the [huios](../../strongs/g/g5207.md) of them which [phoneuō](../../strongs/g/g5407.md) the [prophētēs](../../strongs/g/g4396.md).**

<a name="matthew_23_32"></a>Matthew 23:32

**[plēroō](../../strongs/g/g4137.md) ye then the [metron](../../strongs/g/g3358.md) of your [patēr](../../strongs/g/g3962.md).**

<a name="matthew_23_33"></a>Matthew 23:33

**[ophis](../../strongs/g/g3789.md), [gennēma](../../strongs/g/g1081.md) of [echidna](../../strongs/g/g2191.md), how can [pheugō](../../strongs/g/g5343.md) [apo](../../strongs/g/g575.md) the [krisis](../../strongs/g/g2920.md) of [geenna](../../strongs/g/g1067.md)?**

<a name="matthew_23_34"></a>Matthew 23:34

**Wherefore, [idou](../../strongs/g/g2400.md), I [apostellō](../../strongs/g/g649.md) unto you [prophētēs](../../strongs/g/g4396.md), and [sophos](../../strongs/g/g4680.md), and [grammateus](../../strongs/g/g1122.md): and of them ye shall [apokteinō](../../strongs/g/g615.md) and [stauroō](../../strongs/g/g4717.md); and of them shall ye [mastigoō](../../strongs/g/g3146.md) in your [synagōgē](../../strongs/g/g4864.md), and [diōkō](../../strongs/g/g1377.md) from [polis](../../strongs/g/g4172.md) to [polis](../../strongs/g/g4172.md):**

<a name="matthew_23_35"></a>Matthew 23:35

**That upon you may [erchomai](../../strongs/g/g2064.md) all the [dikaios](../../strongs/g/g1342.md) [haima](../../strongs/g/g129.md) [ekcheō](../../strongs/g/g1632.md) upon [gē](../../strongs/g/g1093.md), from the [haima](../../strongs/g/g129.md) of [dikaios](../../strongs/g/g1342.md) [Abel](../../strongs/g/g6.md) unto the [haima](../../strongs/g/g129.md) of [Zacharias](../../strongs/g/g2197.md) [huios](../../strongs/g/g5207.md) of [Barachias](../../strongs/g/g914.md), whom ye [phoneuō](../../strongs/g/g5407.md) between the [naos](../../strongs/g/g3485.md) and the [thysiastērion](../../strongs/g/g2379.md).**

<a name="matthew_23_36"></a>Matthew 23:36

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, All these things shall [hēkō](../../strongs/g/g2240.md) upon this [genea](../../strongs/g/g1074.md).**

<a name="matthew_23_37"></a>Matthew 23:37

**O [Ierousalēm](../../strongs/g/g2419.md), [Ierousalēm](../../strongs/g/g2419.md), that [apokteinō](../../strongs/g/g615.md) the [prophētēs](../../strongs/g/g4396.md), and [lithoboleō](../../strongs/g/g3036.md) them which are [apostellō](../../strongs/g/g649.md) unto thee, [posakis](../../strongs/g/g4212.md) would I have [episynagō](../../strongs/g/g1996.md) thy [teknon](../../strongs/g/g5043.md), even as [ornis](../../strongs/g/g3733.md) [episynagō](../../strongs/g/g1996.md) her [nossion](../../strongs/g/g3556.md) under [pteryx](../../strongs/g/g4420.md), and ye would not!**

<a name="matthew_23_38"></a>Matthew 23:38

**[idou](../../strongs/g/g2400.md), your [oikos](../../strongs/g/g3624.md) is [aphiēmi](../../strongs/g/g863.md) unto you [erēmos](../../strongs/g/g2048.md).** [^3]

<a name="matthew_23_39"></a>Matthew 23:39

**For I [legō](../../strongs/g/g3004.md) unto you, Ye shall not [eidō](../../strongs/g/g1492.md) me henceforth, till ye shall [eipon](../../strongs/g/g2036.md), [eulogeō](../../strongs/g/g2127.md) he that [erchomai](../../strongs/g/g2064.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md).**

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 22](matthew_22.md) - [Matthew 24](matthew_24.md)

---

[^1]: [Matthew 23:14 Commentary](../../commentary/matthew/matthew_23_commentary.md#matthew_23_14)

[^2]: [Matthew 23:26 Commentary](../../commentary/matthew/matthew_23_commentary.md#matthew_23_26)

[^3]: [Matthew 23:38 Commentary](../../commentary/matthew/matthew_23_commentary.md#matthew_23_38)
