# [Matthew 28](https://www.blueletterbible.org/kjv/mat/28/1/rl1/s_957001)

<a name="matthew_28_1"></a>Matthew 28:1

In the [opse](../../strongs/g/g3796.md) of the [sabbaton](../../strongs/g/g4521.md), as it [epiphōskō](../../strongs/g/g2020.md) toward the first [sabbaton](../../strongs/g/g4521.md), [erchomai](../../strongs/g/g2064.md) [Maria](../../strongs/g/g3137.md) [Magdalēnē](../../strongs/g/g3094.md) and the other [Maria](../../strongs/g/g3137.md) to [theōreō](../../strongs/g/g2334.md) the [taphos](../../strongs/g/g5028.md).

<a name="matthew_28_2"></a>Matthew 28:2

And, [idou](../../strongs/g/g2400.md), there was a [megas](../../strongs/g/g3173.md) [seismos](../../strongs/g/g4578.md): for the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md), and [proserchomai](../../strongs/g/g4334.md) and [apokyliō](../../strongs/g/g617.md) the [lithos](../../strongs/g/g3037.md) from the [thyra](../../strongs/g/g2374.md), and [kathēmai](../../strongs/g/g2521.md) upon it.

<a name="matthew_28_3"></a>Matthew 28:3

His [idea](../../strongs/g/g2397.md) was like [astrapē](../../strongs/g/g796.md), and his [endyma](../../strongs/g/g1742.md) [leukos](../../strongs/g/g3022.md) as [chiōn](../../strongs/g/g5510.md):

<a name="matthew_28_4"></a>Matthew 28:4

And for [phobos](../../strongs/g/g5401.md) of him the [tēreō](../../strongs/g/g5083.md) did [seiō](../../strongs/g/g4579.md), and became as [nekros](../../strongs/g/g3498.md).

<a name="matthew_28_5"></a>Matthew 28:5

And the [aggelos](../../strongs/g/g32.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto the [gynē](../../strongs/g/g1135.md), [phobeō](../../strongs/g/g5399.md) not ye: for I [eidō](../../strongs/g/g1492.md) that ye [zēteō](../../strongs/g/g2212.md) [Iēsous](../../strongs/g/g2424.md), which was [stauroō](../../strongs/g/g4717.md).

<a name="matthew_28_6"></a>Matthew 28:6

He is not [hōde](../../strongs/g/g5602.md): for he is [egeirō](../../strongs/g/g1453.md), as he [eipon](../../strongs/g/g2036.md). [deute](../../strongs/g/g1205.md), [eidō](../../strongs/g/g1492.md) the [topos](../../strongs/g/g5117.md) where the [kyrios](../../strongs/g/g2962.md) [keimai](../../strongs/g/g2749.md).

<a name="matthew_28_7"></a>Matthew 28:7

And [poreuō](../../strongs/g/g4198.md) [tachy](../../strongs/g/g5035.md), and [eipon](../../strongs/g/g2036.md) his [mathētēs](../../strongs/g/g3101.md) that he is [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md); and, [idou](../../strongs/g/g2400.md), he [proagō](../../strongs/g/g4254.md) you into [Galilaia](../../strongs/g/g1056.md); there shall ye [optanomai](../../strongs/g/g3700.md) him: [idou](../../strongs/g/g2400.md), I have [eipon](../../strongs/g/g2036.md) you.

<a name="matthew_28_8"></a>Matthew 28:8

And they [exerchomai](../../strongs/g/g1831.md) [tachy](../../strongs/g/g5035.md) from the [mnēmeion](../../strongs/g/g3419.md) with [phobos](../../strongs/g/g5401.md) and [megas](../../strongs/g/g3173.md) [chara](../../strongs/g/g5479.md); and did [trechō](../../strongs/g/g5143.md) to [apaggellō](../../strongs/g/g518.md) his [mathētēs](../../strongs/g/g3101.md).

<a name="matthew_28_9"></a>Matthew 28:9

And as they [poreuō](../../strongs/g/g4198.md) to [apaggellō](../../strongs/g/g518.md) his [mathētēs](../../strongs/g/g3101.md), [idou](../../strongs/g/g2400.md), [Iēsous](../../strongs/g/g2424.md) [apantaō](../../strongs/g/g528.md) them, [legō](../../strongs/g/g3004.md), **[chairō](../../strongs/g/g5463.md)**. And they [proserchomai](../../strongs/g/g4334.md) and [krateō](../../strongs/g/g2902.md) him by the [pous](../../strongs/g/g4228.md), and [proskyneō](../../strongs/g/g4352.md) him.

<a name="matthew_28_10"></a>Matthew 28:10

Then [legō](../../strongs/g/g3004.md) [Iēsous](../../strongs/g/g2424.md) unto them, **Be not [phobeō](../../strongs/g/g5399.md): [aperchomai](../../strongs/g/g565.md) [apaggellō](../../strongs/g/g518.md) my [adelphos](../../strongs/g/g80.md) that they [hypagō](../../strongs/g/g5217.md) into [Galilaia](../../strongs/g/g1056.md), and there shall they [optanomai](../../strongs/g/g3700.md) me.**

<a name="matthew_28_11"></a>Matthew 28:11

Now when they were [poreuō](../../strongs/g/g4198.md), [idou](../../strongs/g/g2400.md), some of the [koustōdia](../../strongs/g/g2892.md) [erchomai](../../strongs/g/g2064.md) into the [polis](../../strongs/g/g4172.md), and [apaggellō](../../strongs/g/g518.md) unto the [archiereus](../../strongs/g/g749.md) all the things that were [ginomai](../../strongs/g/g1096.md).

<a name="matthew_28_12"></a>Matthew 28:12

And when they were [synagō](../../strongs/g/g4863.md) with the [presbyteros](../../strongs/g/g4245.md), and had [lambanō](../../strongs/g/g2983.md) [symboulion](../../strongs/g/g4824.md), they [didōmi](../../strongs/g/g1325.md) [hikanos](../../strongs/g/g2425.md) [argyrion](../../strongs/g/g694.md) unto the [stratiōtēs](../../strongs/g/g4757.md),

<a name="matthew_28_13"></a>Matthew 28:13

[legō](../../strongs/g/g3004.md), [eipon](../../strongs/g/g2036.md) ye, His [mathētēs](../../strongs/g/g3101.md) [erchomai](../../strongs/g/g2064.md) by [nyx](../../strongs/g/g3571.md), and [kleptō](../../strongs/g/g2813.md) him away while we [koimaō](../../strongs/g/g2837.md).

<a name="matthew_28_14"></a>Matthew 28:14

And if this [akouō](../../strongs/g/g191.md) to the [hēgemōn](../../strongs/g/g2232.md), we will [peithō](../../strongs/g/g3982.md) him, and [amerimnos](../../strongs/g/g275.md) [poieō](../../strongs/g/g4160.md) you.

<a name="matthew_28_15"></a>Matthew 28:15

So they [lambanō](../../strongs/g/g2983.md) the [argyrion](../../strongs/g/g694.md), and [poieō](../../strongs/g/g4160.md) as they were [didaskō](../../strongs/g/g1321.md): and this [logos](../../strongs/g/g3056.md) is [diaphēmizō](../../strongs/g/g1310.md) among the [Ioudaios](../../strongs/g/g2453.md) until [sēmeron](../../strongs/g/g4594.md).

<a name="matthew_28_16"></a>Matthew 28:16

Then the eleven [mathētēs](../../strongs/g/g3101.md) [poreuō](../../strongs/g/g4198.md) into [Galilaia](../../strongs/g/g1056.md), into a [oros](../../strongs/g/g3735.md) where [Iēsous](../../strongs/g/g2424.md) had [tassō](../../strongs/g/g5021.md) them.

<a name="matthew_28_17"></a>Matthew 28:17

And when they [eidō](../../strongs/g/g1492.md) him, they [proskyneō](../../strongs/g/g4352.md) him: but [distazō](../../strongs/g/g1365.md). [^1]

<a name="matthew_28_18"></a>Matthew 28:18

And [Iēsous](../../strongs/g/g2424.md) [proserchomai](../../strongs/g/g4334.md) and [laleō](../../strongs/g/g2980.md) unto them, [legō](../../strongs/g/g3004.md), **All [exousia](../../strongs/g/g1849.md) is [didōmi](../../strongs/g/g1325.md) unto me in [ouranos](../../strongs/g/g3772.md) and in [gē](../../strongs/g/g1093.md).**

<a name="matthew_28_19"></a>Matthew 28:19

**[poreuō](../../strongs/g/g4198.md) therefore, and [mathēteuō](../../strongs/g/g3100.md) all [ethnos](../../strongs/g/g1484.md), [baptizō](../../strongs/g/g907.md) them in the [onoma](../../strongs/g/g3686.md) of the [patēr](../../strongs/g/g3962.md), and of the [huios](../../strongs/g/g5207.md), and of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md):** [^2]

<a name="matthew_28_20"></a>Matthew 28:20

**[didaskō](../../strongs/g/g1321.md) them to [tēreō](../../strongs/g/g5083.md) all things whatsoever I have [entellō](../../strongs/g/g1781.md) you: and, [idou](../../strongs/g/g2400.md), I am with you [pas](../../strongs/g/g3956.md) [hēmera](../../strongs/g/g2250.md), even unto the [synteleia](../../strongs/g/g4930.md) of the [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).**

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 27](matthew_27.md)

---

[^1]: [Matthew 28:27 Commentary](../../commentary/matthew/matthew_28_commentary.md#matthew_28_27)

[^2]: [Matthew 28:19 Commentary](../../commentary/matthew/matthew_28_commentary.md#matthew_28_19)
