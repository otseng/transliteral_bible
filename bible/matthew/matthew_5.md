# [Matthew 5](https://www.blueletterbible.org/kjv/mat/5/1/rl1/s_930001)

<a name="matthew_5_1"></a>Matthew 5:1

And [eidō](../../strongs/g/g1492.md) the [ochlos](../../strongs/g/g3793.md), he [anabainō](../../strongs/g/g305.md) into a [oros](../../strongs/g/g3735.md): and when he [kathizō](../../strongs/g/g2523.md), his [mathētēs](../../strongs/g/g3101.md) [proserchomai](../../strongs/g/g4334.md) unto him:

<a name="matthew_5_2"></a>Matthew 5:2

And he [anoigō](../../strongs/g/g455.md) his [stoma](../../strongs/g/g4750.md), and [didaskō](../../strongs/g/g1321.md) them, [legō](../../strongs/g/g3004.md),

<a name="matthew_5_3"></a>Matthew 5:3

**[makarios](../../strongs/g/g3107.md) the [ptōchos](../../strongs/g/g4434.md) in [pneuma](../../strongs/g/g4151.md): for theirs is the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_5_4"></a>Matthew 5:4

**[makarios](../../strongs/g/g3107.md) [pentheō](../../strongs/g/g3996.md): for they shall be [parakaleō](../../strongs/g/g3870.md).** [^1]

<a name="matthew_5_5"></a>Matthew 5:5

**[makarios](../../strongs/g/g3107.md) the [praÿs](../../strongs/g/g4239.md): for they shall [klēronomeō](../../strongs/g/g2816.md) [gē](../../strongs/g/g1093.md).**

<a name="matthew_5_6"></a>Matthew 5:6

**[makarios](../../strongs/g/g3107.md) they which [peinaō](../../strongs/g/g3983.md) and [dipsaō](../../strongs/g/g1372.md) [dikaiosynē](../../strongs/g/g1343.md): for they shall be [chortazō](../../strongs/g/g5526.md).**

<a name="matthew_5_7"></a>Matthew 5:7

**[makarios](../../strongs/g/g3107.md) [eleēmōn](../../strongs/g/g1655.md): for they shall [eleeō](../../strongs/g/g1653.md).**

<a name="matthew_5_8"></a>Matthew 5:8

**[makarios](../../strongs/g/g3107.md) [katharos](../../strongs/g/g2513.md) [kardia](../../strongs/g/g2588.md): for they shall [optanomai](../../strongs/g/g3700.md) [theos](../../strongs/g/g2316.md).**

<a name="matthew_5_9"></a>Matthew 5:9

**[makarios](../../strongs/g/g3107.md) [eirēnopoios](../../strongs/g/g1518.md): for they shall be [kaleō](../../strongs/g/g2564.md) the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md).**

<a name="matthew_5_10"></a>Matthew 5:10

**[makarios](../../strongs/g/g3107.md) [diōkō](../../strongs/g/g1377.md) for [dikaiosynē](../../strongs/g/g1343.md): for theirs is the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_5_11"></a>Matthew 5:11

**[makarios](../../strongs/g/g3107.md) are ye, when shall [oneidizō](../../strongs/g/g3679.md) you, and [diōkō](../../strongs/g/g1377.md), and shall [eipon](../../strongs/g/g2036.md) all manner of [ponēros](../../strongs/g/g4190.md) [rhēma](../../strongs/g/g4487.md) against you [pseudomai](../../strongs/g/g5574.md), for my sake.** [^2]

<a name="matthew_5_12"></a>Matthew 5:12

**[chairō](../../strongs/g/g5463.md), and [agalliaō](../../strongs/g/g21.md): for [polys](../../strongs/g/g4183.md) your [misthos](../../strongs/g/g3408.md) in [ouranos](../../strongs/g/g3772.md): for so [diōkō](../../strongs/g/g1377.md) the [prophētēs](../../strongs/g/g4396.md) which were before you.** [^3]

<a name="matthew_5_13"></a>Matthew 5:13

**Ye are the [halas](../../strongs/g/g217.md) of the [gē](../../strongs/g/g1093.md): but if the [halas](../../strongs/g/g217.md) have [mōrainō](../../strongs/g/g3471.md), wherewith shall [halizō](../../strongs/g/g233.md)? it is thenceforth [ischyō](../../strongs/g/g2480.md) for [oudeis](../../strongs/g/g3762.md), but to be [ballō](../../strongs/g/g906.md) out, and [katapateō](../../strongs/g/g2662.md) of [anthrōpos](../../strongs/g/g444.md).**

<a name="matthew_5_14"></a>Matthew 5:14

**Ye are the [phōs](../../strongs/g/g5457.md) of the [kosmos](../../strongs/g/g2889.md). A [polis](../../strongs/g/g4172.md) that [keimai](../../strongs/g/g2749.md) on [oros](../../strongs/g/g3735.md) cannot be [kryptō](../../strongs/g/g2928.md).**

<a name="matthew_5_15"></a>Matthew 5:15

**Neither [kaiō](../../strongs/g/g2545.md) a [lychnos](../../strongs/g/g3088.md), and [tithēmi](../../strongs/g/g5087.md) it under a [modios](../../strongs/g/g3426.md), but on a [lychnia](../../strongs/g/g3087.md); and it [lampō](../../strongs/g/g2989.md) unto all that are in the [oikia](../../strongs/g/g3614.md).**

<a name="matthew_5_16"></a>Matthew 5:16

**Let your [phōs](../../strongs/g/g5457.md) so [lampō](../../strongs/g/g2989.md) before [anthrōpos](../../strongs/g/g444.md), that they may [eidō](../../strongs/g/g1492.md) your [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md), and [doxazō](../../strongs/g/g1392.md) your [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_5_17"></a>Matthew 5:17

**[nomizō](../../strongs/g/g3543.md) not that I am [erchomai](../../strongs/g/g2064.md) to [katalyō](../../strongs/g/g2647.md) the [nomos](../../strongs/g/g3551.md), or the [prophētēs](../../strongs/g/g4396.md): I am not [erchomai](../../strongs/g/g2064.md) to [katalyō](../../strongs/g/g2647.md), but [plēroō](../../strongs/g/g4137.md).**

<a name="matthew_5_18"></a>Matthew 5:18

**For [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, Till [ouranos](../../strongs/g/g3772.md) and [gē](../../strongs/g/g1093.md) [parerchomai](../../strongs/g/g3928.md), one [iōta](../../strongs/g/g2503.md) or one [keraia](../../strongs/g/g2762.md) shall in no wise [parerchomai](../../strongs/g/g3928.md) from the [nomos](../../strongs/g/g3551.md), till all [ginomai](../../strongs/g/g1096.md).** [^4]

<a name="matthew_5_19"></a>Matthew 5:19

**Whosoever therefore shall [lyō](../../strongs/g/g3089.md) one of these least [entolē](../../strongs/g/g1785.md), and shall [didaskō](../../strongs/g/g1321.md) [anthrōpos](../../strongs/g/g444.md) so, he shall be [kaleō](../../strongs/g/g2564.md) [elachistos](../../strongs/g/g1646.md) in the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md): but whosoever shall [poieō](../../strongs/g/g4160.md) and [didaskō](../../strongs/g/g1321.md), the same shall be called [megas](../../strongs/g/g3173.md) in the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md).** [^5]

<a name="matthew_5_20"></a>Matthew 5:20

**For I [legō](../../strongs/g/g3004.md) unto you, That except your [dikaiosynē](../../strongs/g/g1343.md) shall [perisseuō](../../strongs/g/g4052.md) [pleiōn](../../strongs/g/g4119.md) of the [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md), ye shall in no case [eiserchomai](../../strongs/g/g1525.md) into the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md).** [^6]

<a name="matthew_5_21"></a>Matthew 5:21

**Ye have [akouō](../../strongs/g/g191.md) that it was [rheō](../../strongs/g/g4483.md) [archaios](../../strongs/g/g744.md), Thou shalt not [phoneuō](../../strongs/g/g5407.md); and whosoever shall [phoneuō](../../strongs/g/g5407.md) shall be [enochos](../../strongs/g/g1777.md) of the [krisis](../../strongs/g/g2920.md):**

<a name="matthew_5_22"></a>Matthew 5:22

**But I [legō](../../strongs/g/g3004.md) unto you, That whosoever is [orgizō](../../strongs/g/g3710.md) with his [adelphos](../../strongs/g/g80.md) [eikē](../../strongs/g/g1500.md) shall be [enochos](../../strongs/g/g1777.md) of the [krisis](../../strongs/g/g2920.md): and whosoever shall [eipon](../../strongs/g/g2036.md) to his [adelphos](../../strongs/g/g80.md), [rhaka](../../strongs/g/g4469.md), shall be [enochos](../../strongs/g/g1777.md) of the [synedrion](../../strongs/g/g4892.md): but whosoever shall [eipon](../../strongs/g/g2036.md), [mōros](../../strongs/g/g3474.md), shall be [enochos](../../strongs/g/g1777.md) of [geenna](../../strongs/g/g1067.md) [pyr](../../strongs/g/g4442.md).** [^7]

<a name="matthew_5_23"></a>Matthew 5:23

**Therefore if thou [prospherō](../../strongs/g/g4374.md) thy [dōron](../../strongs/g/g1435.md) to the [thysiastērion](../../strongs/g/g2379.md), and there [mnaomai](../../strongs/g/g3415.md) that thy [adelphos](../../strongs/g/g80.md) hath [tis](../../strongs/g/g5100.md) against thee;**

<a name="matthew_5_24"></a>Matthew 5:24

**[aphiēmi](../../strongs/g/g863.md) there thy [dōron](../../strongs/g/g1435.md) before the [thysiastērion](../../strongs/g/g2379.md), and [hypagō](../../strongs/g/g5217.md); [prōton](../../strongs/g/g4412.md) [diallassō](../../strongs/g/g1259.md) to thy [adelphos](../../strongs/g/g80.md), and then [erchomai](../../strongs/g/g2064.md) and [prospherō](../../strongs/g/g4374.md) thy [dōron](../../strongs/g/g1435.md).**

<a name="matthew_5_25"></a>Matthew 5:25

**[isthi](../../strongs/g/g2468.md) [eunoeō](../../strongs/g/g2132.md) with thine [antidikos](../../strongs/g/g476.md) [tachy](../../strongs/g/g5035.md), whiles thou art in [hodos](../../strongs/g/g3598.md) with him; lest at any time the [antidikos](../../strongs/g/g476.md) [paradidōmi](../../strongs/g/g3860.md) thee to the [kritēs](../../strongs/g/g2923.md), and the [kritēs](../../strongs/g/g2923.md) [paradidōmi](../../strongs/g/g3860.md) thee to the [hypēretēs](../../strongs/g/g5257.md), and thou be [ballō](../../strongs/g/g906.md) into [phylakē](../../strongs/g/g5438.md).** [^8]

<a name="matthew_5_26"></a>Matthew 5:26

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto thee, Thou shalt by no means [exerchomai](../../strongs/g/g1831.md) out thence, till thou hast [apodidōmi](../../strongs/g/g591.md) the [eschatos](../../strongs/g/g2078.md) [kodrantēs](../../strongs/g/g2835.md).**

<a name="matthew_5_27"></a>Matthew 5:27

**Ye have [akouō](../../strongs/g/g191.md) that it was [rheō](../../strongs/g/g4483.md) [archaios](../../strongs/g/g744.md), Thou shalt not [moicheuō](../../strongs/g/g3431.md):** [^9]

<a name="matthew_5_28"></a>Matthew 5:28

**But I [legō](../../strongs/g/g3004.md) unto you, That whosoever [blepō](../../strongs/g/g991.md) [gynē](../../strongs/g/g1135.md) to [epithymeō](../../strongs/g/g1937.md) her hath [moicheuō](../../strongs/g/g3431.md) with her already in his [kardia](../../strongs/g/g2588.md).**

<a name="matthew_5_29"></a>Matthew 5:29

**And if thy [dexios](../../strongs/g/g1188.md) [ophthalmos](../../strongs/g/g3788.md) [skandalizō](../../strongs/g/g4624.md) thee, [exaireō](../../strongs/g/g1807.md) it, and [ballō](../../strongs/g/g906.md) from thee: for it is [sympherō](../../strongs/g/g4851.md) for thee that one of thy [melos](../../strongs/g/g3196.md) should [apollymi](../../strongs/g/g622.md), and not thy [holos](../../strongs/g/g3650.md) [sōma](../../strongs/g/g4983.md) should be [ballō](../../strongs/g/g906.md) into [geenna](../../strongs/g/g1067.md).** [^10]

<a name="matthew_5_30"></a>Matthew 5:30

**And if thy [dexios](../../strongs/g/g1188.md) [cheir](../../strongs/g/g5495.md) [skandalizō](../../strongs/g/g4624.md) thee, [ekkoptō](../../strongs/g/g1581.md) it, and [ballō](../../strongs/g/g906.md) from thee: for it is [sympherō](../../strongs/g/g4851.md) for thee that one of thy [melos](../../strongs/g/g3196.md) should [apollymi](../../strongs/g/g622.md), and not thy [holos](../../strongs/g/g3650.md) [sōma](../../strongs/g/g4983.md) should be [ballō](../../strongs/g/g906.md) into [geenna](../../strongs/g/g1067.md).** [^11]

<a name="matthew_5_31"></a>Matthew 5:31

**It hath been [rheō](../../strongs/g/g4483.md), Whosoever shall [apolyō](../../strongs/g/g630.md) his [gynē](../../strongs/g/g1135.md), let him [didōmi](../../strongs/g/g1325.md) her [apostasion](../../strongs/g/g647.md):**

<a name="matthew_5_32"></a>Matthew 5:32

**But I [legō](../../strongs/g/g3004.md) unto you, That whosoever shall [apolyō](../../strongs/g/g630.md) his [gynē](../../strongs/g/g1135.md), [parektos](../../strongs/g/g3924.md) for the [logos](../../strongs/g/g3056.md) of [porneia](../../strongs/g/g4202.md), [poieō](../../strongs/g/g4160.md) her to [moichaō](../../strongs/g/g3429.md): and whosoever shall [gameō](../../strongs/g/g1060.md) [apolyō](../../strongs/g/g630.md) [moichaō](../../strongs/g/g3429.md).** [^12]

<a name="matthew_5_33"></a>Matthew 5:33

**[palin](../../strongs/g/g3825.md), ye have [akouō](../../strongs/g/g191.md) that it hath been [rheō](../../strongs/g/g4483.md) [archaios](../../strongs/g/g744.md), Thou shalt not [epiorkeō](../../strongs/g/g1964.md), but shalt [apodidōmi](../../strongs/g/g591.md) unto the [kyrios](../../strongs/g/g2962.md) thine [horkos](../../strongs/g/g3727.md):** [^13]

<a name="matthew_5_34"></a>Matthew 5:34

**But I [legō](../../strongs/g/g3004.md) unto you, [omnyō](../../strongs/g/g3660.md) not at all; neither by [ouranos](../../strongs/g/g3772.md); for it is [theos](../../strongs/g/g2316.md) [thronos](../../strongs/g/g2362.md):**

<a name="matthew_5_35"></a>Matthew 5:35

**Nor by [gē](../../strongs/g/g1093.md); for it is his [pous](../../strongs/g/g4228.md) [hypopodion](../../strongs/g/g5286.md): neither by [Hierosolyma](../../strongs/g/g2414.md); for it is the [polis](../../strongs/g/g4172.md) of the [megas](../../strongs/g/g3173.md) [basileus](../../strongs/g/g935.md).**

<a name="matthew_5_36"></a>Matthew 5:36

**Neither shalt thou [omnyō](../../strongs/g/g3660.md) by thy [kephalē](../../strongs/g/g2776.md), because thou canst not [poieō](../../strongs/g/g4160.md) [mia](../../strongs/g/g3391.md) [thrix](../../strongs/g/g2359.md) [leukos](../../strongs/g/g3022.md) or [melas](../../strongs/g/g3189.md).**

<a name="matthew_5_37"></a>Matthew 5:37

**But let your [logos](../../strongs/g/g3056.md) be, [nai](../../strongs/g/g3483.md), [nai](../../strongs/g/g3483.md); [ou](../../strongs/g/g3756.md), [ou](../../strongs/g/g3756.md): for whatsoever is [perissos](../../strongs/g/g4053.md) than these cometh of [ponēros](../../strongs/g/g4190.md).**

<a name="matthew_5_38"></a>Matthew 5:38

**Ye have [akouō](../../strongs/g/g191.md) that it hath been [rheō](../../strongs/g/g4483.md), An [ophthalmos](../../strongs/g/g3788.md) for an [ophthalmos](../../strongs/g/g3788.md), and a [odous](../../strongs/g/g3599.md) for a [odous](../../strongs/g/g3599.md):**

<a name="matthew_5_39"></a>Matthew 5:39

**But I [legō](../../strongs/g/g3004.md) unto you, That ye [anthistēmi](../../strongs/g/g436.md) not [ponēros](../../strongs/g/g4190.md): but whosoever shall [rhapizō](../../strongs/g/g4474.md) thee on thy [dexios](../../strongs/g/g1188.md) [siagōn](../../strongs/g/g4600.md), [strephō](../../strongs/g/g4762.md) to him the other also.** [^14]

<a name="matthew_5_40"></a>Matthew 5:40

**And [thelō](../../strongs/g/g2309.md) [krinō](../../strongs/g/g2919.md) thee, and [lambanō](../../strongs/g/g2983.md) thy [chitōn](../../strongs/g/g5509.md), [aphiēmi](../../strongs/g/g863.md) him [himation](../../strongs/g/g2440.md) also.**

<a name="matthew_5_41"></a>Matthew 5:41

**And whosoever shall [aggareuō](../../strongs/g/g29.md) thee to [aggareuō](../../strongs/g/g29.md) a [milion](../../strongs/g/g3400.md), [hypagō](../../strongs/g/g5217.md) with him [dyo](../../strongs/g/g1417.md).**

<a name="matthew_5_42"></a>Matthew 5:42

**[didōmi](../../strongs/g/g1325.md) to him that [aiteō](../../strongs/g/g154.md) thee, and from him that would [daneizō](../../strongs/g/g1155.md) of thee [apostrephō](../../strongs/g/g654.md) not.**

<a name="matthew_5_43"></a>Matthew 5:43

**Ye have [akouō](../../strongs/g/g191.md) that it hath been [rheō](../../strongs/g/g4483.md), Thou shalt [agapaō](../../strongs/g/g25.md) thy [plēsion](../../strongs/g/g4139.md), and [miseō](../../strongs/g/g3404.md) thine [echthros](../../strongs/g/g2190.md).**

<a name="matthew_5_44"></a>Matthew 5:44

**But I [legō](../../strongs/g/g3004.md) unto you, [agapaō](../../strongs/g/g25.md) your [echthros](../../strongs/g/g2190.md), [eulogeō](../../strongs/g/g2127.md) them that [kataraomai](../../strongs/g/g2672.md) you, [poieō](../../strongs/g/g4160.md) [kalōs](../../strongs/g/g2573.md) to them that [miseō](../../strongs/g/g3404.md) you, and [proseuchomai](../../strongs/g/g4336.md) for them which [epēreazō](../../strongs/g/g1908.md) you, and [diōkō](../../strongs/g/g1377.md) you;** [^15] [^16]

<a name="matthew_5_45"></a>Matthew 5:45

**That ye may be the [huios](../../strongs/g/g5207.md) of your [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md): for he [anatellō](../../strongs/g/g393.md) his [hēlios](../../strongs/g/g2246.md) on the [ponēros](../../strongs/g/g4190.md) and on the [agathos](../../strongs/g/g18.md), and [brechō](../../strongs/g/g1026.md) on the [dikaios](../../strongs/g/g1342.md) and on the [adikos](../../strongs/g/g94.md).**

<a name="matthew_5_46"></a>Matthew 5:46

**For if ye [agapaō](../../strongs/g/g25.md) them which [agapaō](../../strongs/g/g25.md) you, what [misthos](../../strongs/g/g3408.md) have ye? [poieō](../../strongs/g/g4160.md) not even the [telōnēs](../../strongs/g/g5057.md) the same?**

<a name="matthew_5_47"></a>Matthew 5:47

**And if ye [aspazomai](../../strongs/g/g782.md) your [adelphos](../../strongs/g/g80.md) only, what [poieō](../../strongs/g/g4160.md) ye [perissos](../../strongs/g/g4053.md)? [poieō](../../strongs/g/g4160.md) not even the [telōnēs](../../strongs/g/g5057.md) so?** [^17]

<a name="matthew_5_48"></a>Matthew 5:48

**Be ye therefore [teleios](../../strongs/g/g5046.md), even as your [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md) is [teleios](../../strongs/g/g5046.md).**

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 4](matthew_4.md) - [Matthew 6](matthew_6.md)

---

[^1]: [Matthew 5:4 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_4)

[^2]: [Matthew 5:11 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_11)

[^3]: [Matthew 5:12 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_12)

[^4]: [Matthew 5:18 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_18)

[^5]: [Matthew 5:19 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_19)

[^6]: [Matthew 5:20 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_20)

[^7]: [Matthew 5:22 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_22)

[^8]: [Matthew 5:25 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_25)

[^9]: [Matthew 5:27 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_27)

[^10]: [Matthew 5:29 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_29)

[^11]: [Matthew 5:30 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_30)

[^12]: [Matthew 5:32 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_32)

[^13]: [Matthew 5:33 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_33)

[^14]: [Matthew 5:39 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_39)

[^15]: [Matthew 5:44 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_44)

[^16]: [Matthew 5:44 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_44)

[^17]: [Matthew 5:47 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_47)
