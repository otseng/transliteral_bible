# [Matthew 24](https://www.blueletterbible.org/kjv/mat/24/1/rl1/s_953001)

<a name="matthew_24_1"></a>Matthew 24:1

And [Iēsous](../../strongs/g/g2424.md) [exerchomai](../../strongs/g/g1831.md), and [poreuō](../../strongs/g/g4198.md) from the [hieron](../../strongs/g/g2411.md): and his [mathētēs](../../strongs/g/g3101.md) [proserchomai](../../strongs/g/g4334.md) to for to [epideiknymi](../../strongs/g/g1925.md) him the [oikodomē](../../strongs/g/g3619.md) of the [hieron](../../strongs/g/g2411.md).

<a name="matthew_24_2"></a>Matthew 24:2

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **[blepō](../../strongs/g/g991.md) ye not all these things? [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, There shall not be [aphiēmi](../../strongs/g/g863.md) here [lithos](../../strongs/g/g3037.md) upon another, that shall not be [katalyō](../../strongs/g/g2647.md).**

<a name="matthew_24_3"></a>Matthew 24:3

And as he [kathēmai](../../strongs/g/g2521.md) upon the [oros](../../strongs/g/g3735.md) of [elaia](../../strongs/g/g1636.md), the [mathētēs](../../strongs/g/g3101.md) [proserchomai](../../strongs/g/g4334.md) unto him [idios](../../strongs/g/g2398.md), [legō](../../strongs/g/g3004.md), [eipon](../../strongs/g/g2036.md) us, when shall these things be? and what shall be the [sēmeion](../../strongs/g/g4592.md) of thy [parousia](../../strongs/g/g3952.md), and of the [synteleia](../../strongs/g/g4930.md) of the [aiōn](../../strongs/g/g165.md)?

<a name="matthew_24_4"></a>Matthew 24:4

**And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, [blepō](../../strongs/g/g991.md) that no man [planaō](../../strongs/g/g4105.md) you.**

<a name="matthew_24_5"></a>Matthew 24:5

**For [polys](../../strongs/g/g4183.md) shall [erchomai](../../strongs/g/g2064.md) in my [onoma](../../strongs/g/g3686.md), [legō](../../strongs/g/g3004.md), I am [Christos](../../strongs/g/g5547.md); and shall [planaō](../../strongs/g/g4105.md) [polys](../../strongs/g/g4183.md).**

<a name="matthew_24_6"></a>Matthew 24:6

**And ye shall [akouō](../../strongs/g/g191.md) of [polemos](../../strongs/g/g4171.md) and [akoē](../../strongs/g/g189.md) of [polemos](../../strongs/g/g4171.md): [horaō](../../strongs/g/g3708.md) that ye be not [throeō](../../strongs/g/g2360.md): for all must [ginomai](../../strongs/g/g1096.md), but the [telos](../../strongs/g/g5056.md) is not yet.**

<a name="matthew_24_7"></a>Matthew 24:7

**For [ethnos](../../strongs/g/g1484.md) shall [egeirō](../../strongs/g/g1453.md) against [ethnos](../../strongs/g/g1484.md), and [basileia](../../strongs/g/g932.md) against [basileia](../../strongs/g/g932.md): and there shall be [limos](../../strongs/g/g3042.md), and [loimos](../../strongs/g/g3061.md), and [seismos](../../strongs/g/g4578.md), [kata](../../strongs/g/g2596.md) [topos](../../strongs/g/g5117.md).**

<a name="matthew_24_8"></a>Matthew 24:8

**All these are the [archē](../../strongs/g/g746.md) of [ōdin](../../strongs/g/g5604.md).**

<a name="matthew_24_9"></a>Matthew 24:9

**Then shall they [paradidōmi](../../strongs/g/g3860.md) you up to be [thlipsis](../../strongs/g/g2347.md), and shall [apokteinō](../../strongs/g/g615.md) you: and ye shall be [miseō](../../strongs/g/g3404.md) of all [ethnos](../../strongs/g/g1484.md) for my [onoma](../../strongs/g/g3686.md).**

<a name="matthew_24_10"></a>Matthew 24:10

**And then shall [polys](../../strongs/g/g4183.md) be [skandalizō](../../strongs/g/g4624.md), and shall [paradidōmi](../../strongs/g/g3860.md) one another, and shall [miseō](../../strongs/g/g3404.md) [allēlōn](../../strongs/g/g240.md).**

<a name="matthew_24_11"></a>Matthew 24:11

**And [polys](../../strongs/g/g4183.md) [pseudoprophētēs](../../strongs/g/g5578.md) shall [egeirō](../../strongs/g/g1453.md), and shall [planaō](../../strongs/g/g4105.md) [polys](../../strongs/g/g4183.md).**

<a name="matthew_24_12"></a>Matthew 24:12

**And because [anomia](../../strongs/g/g458.md) shall [plēthynō](../../strongs/g/g4129.md), the [agapē](../../strongs/g/g26.md) of [polys](../../strongs/g/g4183.md) shall [psychō](../../strongs/g/g5594.md).**

<a name="matthew_24_13"></a>Matthew 24:13

**But he that shall [hypomenō](../../strongs/g/g5278.md) unto the [telos](../../strongs/g/g5056.md), the same shall be [sōzō](../../strongs/g/g4982.md).**

<a name="matthew_24_14"></a>Matthew 24:14

**And this [euaggelion](../../strongs/g/g2098.md) of the [basileia](../../strongs/g/g932.md) shall be [kēryssō](../../strongs/g/g2784.md) in all the [oikoumenē](../../strongs/g/g3625.md) for a [martyrion](../../strongs/g/g3142.md) unto all [ethnos](../../strongs/g/g1484.md); and then shall the [telos](../../strongs/g/g5056.md) [hēkō](../../strongs/g/g2240.md).**

<a name="matthew_24_15"></a>Matthew 24:15

**When ye therefore shall [eidō](../../strongs/g/g1492.md) the [bdelygma](../../strongs/g/g946.md) of [erēmōsis](../../strongs/g/g2050.md), [rheō](../../strongs/g/g4483.md) of by [Daniēl](../../strongs/g/g1158.md) the [prophētēs](../../strongs/g/g4396.md), [histēmi](../../strongs/g/g2476.md) in the [hagios](../../strongs/g/g40.md) [topos](../../strongs/g/g5117.md),** (whoso [anaginōskō](../../strongs/g/g314.md), let him [noeō](../../strongs/g/g3539.md):)

<a name="matthew_24_16"></a>Matthew 24:16

**Then let them which be in [Ioudaia](../../strongs/g/g2449.md) [pheugō](../../strongs/g/g5343.md) into the [oros](../../strongs/g/g3735.md):**

<a name="matthew_24_17"></a>Matthew 24:17

**Let him which is on the [dōma](../../strongs/g/g1430.md) not [katabainō](../../strongs/g/g2597.md) to [airō](../../strongs/g/g142.md) any thing out of his [oikia](../../strongs/g/g3614.md):**

<a name="matthew_24_18"></a>Matthew 24:18

**Neither let him which is in the [agros](../../strongs/g/g68.md) [epistrephō](../../strongs/g/g1994.md) back to [airō](../../strongs/g/g142.md) his [himation](../../strongs/g/g2440.md).**

<a name="matthew_24_19"></a>Matthew 24:19

**And [ouai](../../strongs/g/g3759.md) unto them that are with [gastēr](../../strongs/g/g1064.md), and to [thēlazō](../../strongs/g/g2337.md) in those [hēmera](../../strongs/g/g2250.md)!**

<a name="matthew_24_20"></a>Matthew 24:20

**But [proseuchomai](../../strongs/g/g4336.md) ye that your [phygē](../../strongs/g/g5437.md) be not in the [cheimōn](../../strongs/g/g5494.md), neither on the [sabbaton](../../strongs/g/g4521.md):**

<a name="matthew_24_21"></a>Matthew 24:21

**For then shall be [megas](../../strongs/g/g3173.md) [thlipsis](../../strongs/g/g2347.md), such as was not since the [archē](../../strongs/g/g746.md) of the [kosmos](../../strongs/g/g2889.md) to [nyn](../../strongs/g/g3568.md), no, nor ever shall be.**

<a name="matthew_24_22"></a>Matthew 24:22

**And except those [hēmera](../../strongs/g/g2250.md) should be [koloboō](../../strongs/g/g2856.md), there should no [sarx](../../strongs/g/g4561.md) be [sōzō](../../strongs/g/g4982.md): but for the [eklektos](../../strongs/g/g1588.md) those [hēmera](../../strongs/g/g2250.md) shall be [koloboō](../../strongs/g/g2856.md).**

<a name="matthew_24_23"></a>Matthew 24:23

**Then if any man shall [eipon](../../strongs/g/g2036.md) unto you, [idou](../../strongs/g/g2400.md), here [Christos](../../strongs/g/g5547.md), or there; [pisteuō](../../strongs/g/g4100.md) not.**

<a name="matthew_24_24"></a>Matthew 24:24

**For there shall [egeirō](../../strongs/g/g1453.md) [pseudochristos](../../strongs/g/g5580.md), and [pseudoprophētēs](../../strongs/g/g5578.md), and shall [didōmi](../../strongs/g/g1325.md) [megas](../../strongs/g/g3173.md) [sēmeion](../../strongs/g/g4592.md) and [teras](../../strongs/g/g5059.md); insomuch that, if [dynatos](../../strongs/g/g1415.md), they shall [planaō](../../strongs/g/g4105.md) the very [eklektos](../../strongs/g/g1588.md).**

<a name="matthew_24_25"></a>Matthew 24:25

**[idou](../../strongs/g/g2400.md), I have [proereō](../../strongs/g/g4280.md) you.**

<a name="matthew_24_26"></a>Matthew 24:26

**Wherefore if they shall [eipon](../../strongs/g/g2036.md) unto you, [idou](../../strongs/g/g2400.md), he is in the [erēmos](../../strongs/g/g2048.md); [exerchomai](../../strongs/g/g1831.md) not: [idou](../../strongs/g/g2400.md), in the [tameion](../../strongs/g/g5009.md); [pisteuō](../../strongs/g/g4100.md) not.**

<a name="matthew_24_27"></a>Matthew 24:27

**For as the [astrapē](../../strongs/g/g796.md) [exerchomai](../../strongs/g/g1831.md) out of the [anatolē](../../strongs/g/g395.md), and [phainō](../../strongs/g/g5316.md) even unto the [dysmē](../../strongs/g/g1424.md); so shall also the [parousia](../../strongs/g/g3952.md) of the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) be.**

<a name="matthew_24_28"></a>Matthew 24:28

**For wheresoever the [ptōma](../../strongs/g/g4430.md) is, there will the [aetos](../../strongs/g/g105.md) [synagō](../../strongs/g/g4863.md).**

<a name="matthew_24_29"></a>Matthew 24:29

**[eutheōs](../../strongs/g/g2112.md) after the [thlipsis](../../strongs/g/g2347.md) of those [hēmera](../../strongs/g/g2250.md) shall the [hēlios](../../strongs/g/g2246.md) be [skotizō](../../strongs/g/g4654.md), and the [selēnē](../../strongs/g/g4582.md) shall not [didōmi](../../strongs/g/g1325.md) her [pheggos](../../strongs/g/g5338.md), and the [astēr](../../strongs/g/g792.md) shall [piptō](../../strongs/g/g4098.md) from [ouranos](../../strongs/g/g3772.md), and the [dynamis](../../strongs/g/g1411.md) of the [ouranos](../../strongs/g/g3772.md) shall be [saleuō](../../strongs/g/g4531.md):**

<a name="matthew_24_30"></a>Matthew 24:30

**And then shall [phainō](../../strongs/g/g5316.md) the [sēmeion](../../strongs/g/g4592.md) of the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) in [ouranos](../../strongs/g/g3772.md): and then shall all the [phylē](../../strongs/g/g5443.md) of [gē](../../strongs/g/g1093.md) [koptō](../../strongs/g/g2875.md), and they shall [optanomai](../../strongs/g/g3700.md) the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [erchomai](../../strongs/g/g2064.md) in the [nephelē](../../strongs/g/g3507.md) of [ouranos](../../strongs/g/g3772.md) with [dynamis](../../strongs/g/g1411.md) and [polys](../../strongs/g/g4183.md) [doxa](../../strongs/g/g1391.md).**

<a name="matthew_24_31"></a>Matthew 24:31

**And he shall [apostellō](../../strongs/g/g649.md) his [aggelos](../../strongs/g/g32.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md) of a [salpigx](../../strongs/g/g4536.md), and they shall [episynagō](../../strongs/g/g1996.md) his [eklektos](../../strongs/g/g1588.md) from the four [anemos](../../strongs/g/g417.md), from one [akron](../../strongs/g/g206.md) of [ouranos](../../strongs/g/g3772.md) to the [akron](../../strongs/g/g206.md) [autos](../../strongs/g/g846.md).**

<a name="matthew_24_32"></a>Matthew 24:32

**Now [manthanō](../../strongs/g/g3129.md) a [parabolē](../../strongs/g/g3850.md) of the [sykē](../../strongs/g/g4808.md); When his [klados](../../strongs/g/g2798.md) is yet [hapalos](../../strongs/g/g527.md), and [ekphyō](../../strongs/g/g1631.md) [phyllon](../../strongs/g/g5444.md), ye [ginōskō](../../strongs/g/g1097.md) that [theros](../../strongs/g/g2330.md) [eggys](../../strongs/g/g1451.md):**

<a name="matthew_24_33"></a>Matthew 24:33

**So likewise ye, when ye shall [eidō](../../strongs/g/g1492.md) all these things, [ginōskō](../../strongs/g/g1097.md) that it is [eggys](../../strongs/g/g1451.md), at the [thyra](../../strongs/g/g2374.md).**

<a name="matthew_24_34"></a>Matthew 24:34

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, This [genea](../../strongs/g/g1074.md) shall not [parerchomai](../../strongs/g/g3928.md), till all these things [ginomai](../../strongs/g/g1096.md).**

<a name="matthew_24_35"></a>Matthew 24:35

**[ouranos](../../strongs/g/g3772.md) and [gē](../../strongs/g/g1093.md) shall [parerchomai](../../strongs/g/g3928.md), but my [logos](../../strongs/g/g3056.md) shall not [parerchomai](../../strongs/g/g3928.md).**

<a name="matthew_24_36"></a>Matthew 24:36

**But of that [hēmera](../../strongs/g/g2250.md) and [hōra](../../strongs/g/g5610.md) [eidō](../../strongs/g/g1492.md) [oudeis](../../strongs/g/g3762.md), no, not the [aggelos](../../strongs/g/g32.md) of [ouranos](../../strongs/g/g3772.md), but my [patēr](../../strongs/g/g3962.md) only.** [^1]

<a name="matthew_24_37"></a>Matthew 24:37

**But as the [hēmera](../../strongs/g/g2250.md) of [Nōe](../../strongs/g/g3575.md) were, so shall also the [parousia](../../strongs/g/g3952.md) of the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) be.**

<a name="matthew_24_38"></a>Matthew 24:38

**For as in the [hēmera](../../strongs/g/g2250.md) that were before the [kataklysmos](../../strongs/g/g2627.md) they were [trōgō](../../strongs/g/g5176.md) and [pinō](../../strongs/g/g4095.md), [gameō](../../strongs/g/g1060.md) and [ekgamizō](../../strongs/g/g1547.md), until the [hēmera](../../strongs/g/g2250.md) that [Nōe](../../strongs/g/g3575.md) [eiserchomai](../../strongs/g/g1525.md) into the [kibōtos](../../strongs/g/g2787.md),**

<a name="matthew_24_39"></a>Matthew 24:39

**And [ginōskō](../../strongs/g/g1097.md) not until the [kataklysmos](../../strongs/g/g2627.md) [erchomai](../../strongs/g/g2064.md), and [airō](../../strongs/g/g142.md) them all away; so shall also the [parousia](../../strongs/g/g3952.md) of the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) be.**

<a name="matthew_24_40"></a>Matthew 24:40

**Then shall two be in the [agros](../../strongs/g/g68.md); the one shall be [paralambanō](../../strongs/g/g3880.md), and the other [aphiēmi](../../strongs/g/g863.md).**

<a name="matthew_24_41"></a>Matthew 24:41

**Two [alēthō](../../strongs/g/g229.md) at the [mylōn](../../strongs/g/g3459.md); the one shall be [paralambanō](../../strongs/g/g3880.md), and the other [aphiēmi](../../strongs/g/g863.md).**

<a name="matthew_24_42"></a>Matthew 24:42

**[grēgoreō](../../strongs/g/g1127.md) therefore: for ye [eidō](../../strongs/g/g1492.md) not what [hōra](../../strongs/g/g5610.md) your [kyrios](../../strongs/g/g2962.md) doth [erchomai](../../strongs/g/g2064.md).**

<a name="matthew_24_43"></a>Matthew 24:43

**But [ginōskō](../../strongs/g/g1097.md) this, that if the [oikodespotēs](../../strongs/g/g3617.md) had [eidō](../../strongs/g/g1492.md) in what [phylakē](../../strongs/g/g5438.md) the [kleptēs](../../strongs/g/g2812.md) would [erchomai](../../strongs/g/g2064.md), he would have [grēgoreō](../../strongs/g/g1127.md), and would not have [eaō](../../strongs/g/g1439.md) his [oikia](../../strongs/g/g3614.md) to be [dioryssō](../../strongs/g/g1358.md).**

<a name="matthew_24_44"></a>Matthew 24:44

**Therefore be ye also [hetoimos](../../strongs/g/g2092.md): for in such an [hōra](../../strongs/g/g5610.md) as ye [dokeō](../../strongs/g/g1380.md) not the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [erchomai](../../strongs/g/g2064.md).**

<a name="matthew_24_45"></a>Matthew 24:45

**Who then is a [pistos](../../strongs/g/g4103.md) and [phronimos](../../strongs/g/g5429.md) [doulos](../../strongs/g/g1401.md), whom his [kyrios](../../strongs/g/g2962.md) hath [kathistēmi](../../strongs/g/g2525.md) over his [therapeia](../../strongs/g/g2322.md), to [didōmi](../../strongs/g/g1325.md) them [trophē](../../strongs/g/g5160.md) in [kairos](../../strongs/g/g2540.md)?**

<a name="matthew_24_46"></a>Matthew 24:46

**[makarios](../../strongs/g/g3107.md) that [doulos](../../strongs/g/g1401.md), whom his [kyrios](../../strongs/g/g2962.md) when he [erchomai](../../strongs/g/g2064.md) shall [heuriskō](../../strongs/g/g2147.md) so [poieō](../../strongs/g/g4160.md).**

<a name="matthew_24_47"></a>Matthew 24:47

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, That he shall [kathistēmi](../../strongs/g/g2525.md) him over all his [hyparchonta](../../strongs/g/g5224.md).**

<a name="matthew_24_48"></a>Matthew 24:48

**But and if that [kakos](../../strongs/g/g2556.md) [doulos](../../strongs/g/g1401.md) shall [eipon](../../strongs/g/g2036.md) in his [kardia](../../strongs/g/g2588.md), My [kyrios](../../strongs/g/g2962.md) [chronizō](../../strongs/g/g5549.md) his [erchomai](../../strongs/g/g2064.md);**

<a name="matthew_24_49"></a>Matthew 24:49

**And shall [archomai](../../strongs/g/g756.md) to [typtō](../../strongs/g/g5180.md) [syndoulos](../../strongs/g/g4889.md), and to [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md) with the [methyō](../../strongs/g/g3184.md);**

<a name="matthew_24_50"></a>Matthew 24:50

**The [kyrios](../../strongs/g/g2962.md) of that [doulos](../../strongs/g/g1401.md) shall [hēkō](../../strongs/g/g2240.md) in a [hēmera](../../strongs/g/g2250.md) when he [prosdokaō](../../strongs/g/g4328.md) not, and in an [hōra](../../strongs/g/g5610.md) that he is not [ginōskō](../../strongs/g/g1097.md),**

<a name="matthew_24_51"></a>Matthew 24:51

**And shall [dichotomeō](../../strongs/g/g1371.md) him, and [tithēmi](../../strongs/g/g5087.md) his [meros](../../strongs/g/g3313.md) with the [hypokritēs](../../strongs/g/g5273.md): there shall be [klauthmos](../../strongs/g/g2805.md) and [brygmos](../../strongs/g/g1030.md) of [odous](../../strongs/g/g3599.md).**

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 23](matthew_23.md) - [Matthew 25](matthew_25.md)

---

[^1]: [Matthew 24:36 Commentary](../../commentary/matthew/matthew_24_commentary.md#matthew_24_36)
