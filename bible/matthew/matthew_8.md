# [Matthew 8](https://www.blueletterbible.org/kjv/mat/8/1/rl1/s_937001)

<a name="matthew_8_1"></a>Matthew 8:1

When he was [katabainō](../../strongs/g/g2597.md) from the [oros](../../strongs/g/g3735.md), [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) [akoloutheō](../../strongs/g/g190.md) him.

<a name="matthew_8_2"></a>Matthew 8:2

And, [idou](../../strongs/g/g2400.md), there [erchomai](../../strongs/g/g2064.md) a [lepros](../../strongs/g/g3015.md) and [proskyneō](../../strongs/g/g4352.md) him, [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), if thou wilt, thou canst [katharizō](../../strongs/g/g2511.md) me.

<a name="matthew_8_3"></a>Matthew 8:3

And [Iēsous](../../strongs/g/g2424.md) [ekteinō](../../strongs/g/g1614.md) [cheir](../../strongs/g/g5495.md), and [haptomai](../../strongs/g/g680.md) him, [legō](../../strongs/g/g3004.md), **I [thelō](../../strongs/g/g2309.md); be thou [katharizō](../../strongs/g/g2511.md).** And [eutheōs](../../strongs/g/g2112.md) his [lepra](../../strongs/g/g3014.md) was [katharizō](../../strongs/g/g2511.md). [^1]

<a name="matthew_8_4"></a>Matthew 8:4

And [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, **[horaō](../../strongs/g/g3708.md) thou [eipon](../../strongs/g/g2036.md) [mēdeis](../../strongs/g/g3367.md); but [hypagō](../../strongs/g/g5217.md), [deiknyō](../../strongs/g/g1166.md) thyself to the [hiereus](../../strongs/g/g2409.md), and [prospherō](../../strongs/g/g4374.md) the [dōron](../../strongs/g/g1435.md) that [Mōÿsēs](../../strongs/g/g3475.md) [prostassō](../../strongs/g/g4367.md), for a [martyrion](../../strongs/g/g3142.md) unto them.**

<a name="matthew_8_5"></a>Matthew 8:5

And when [Iēsous](../../strongs/g/g2424.md) was [eiserchomai](../../strongs/g/g1525.md) into [Kapharnaoum](../../strongs/g/g2584.md), there [proserchomai](../../strongs/g/g4334.md) unto him a [hekatontarchēs](../../strongs/g/g1543.md), [parakaleō](../../strongs/g/g3870.md) him, [^2]

<a name="matthew_8_6"></a>Matthew 8:6

And [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), my [pais](../../strongs/g/g3816.md) [ballō](../../strongs/g/g906.md) at [oikia](../../strongs/g/g3614.md) [paralytikos](../../strongs/g/g3885.md), [deinōs](../../strongs/g/g1171.md) [basanizō](../../strongs/g/g928.md). [^3]

<a name="matthew_8_7"></a>Matthew 8:7

And [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, **I will [erchomai](../../strongs/g/g2064.md) and [therapeuō](../../strongs/g/g2323.md) him.** [^4]

<a name="matthew_8_8"></a>Matthew 8:8

The [hekatontarchēs](../../strongs/g/g1543.md) [apokrinomai](../../strongs/g/g611.md) and [phēmi](../../strongs/g/g5346.md), [kyrios](../../strongs/g/g2962.md), I am not [hikanos](../../strongs/g/g2425.md) that thou [eiserchomai](../../strongs/g/g1525.md) under my [stegē](../../strongs/g/g4721.md): but [eipon](../../strongs/g/g2036.md) the [logos](../../strongs/g/g3056.md) only, and my [pais](../../strongs/g/g3816.md) shall be [iaomai](../../strongs/g/g2390.md). [^5]

<a name="matthew_8_9"></a>Matthew 8:9

For I am an [anthrōpos](../../strongs/g/g444.md) under [exousia](../../strongs/g/g1849.md), having [stratiōtēs](../../strongs/g/g4757.md) under me: and I [legō](../../strongs/g/g3004.md) to this, [poreuō](../../strongs/g/g4198.md), and he [poreuō](../../strongs/g/g4198.md); and to another, [erchomai](../../strongs/g/g2064.md), and he [erchomai](../../strongs/g/g2064.md); and to my [doulos](../../strongs/g/g1401.md), [poieō](../../strongs/g/g4160.md) this, and he [poieō](../../strongs/g/g4160.md). [^6]

<a name="matthew_8_10"></a>Matthew 8:10

When [Iēsous](../../strongs/g/g2424.md) [akouō](../../strongs/g/g191.md), he [thaumazō](../../strongs/g/g2296.md), and [eipon](../../strongs/g/g2036.md) to [akoloutheō](../../strongs/g/g190.md), **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, I have not [heuriskō](../../strongs/g/g2147.md) [tosoutos](../../strongs/g/g5118.md) [pistis](../../strongs/g/g4102.md), no, not in [Israēl](../../strongs/g/g2474.md).** [^7]

<a name="matthew_8_11"></a>Matthew 8:11

**And I [legō](../../strongs/g/g3004.md) unto you, That [polys](../../strongs/g/g4183.md) shall [hēkō](../../strongs/g/g2240.md) from the [anatolē](../../strongs/g/g395.md) and [dysmē](../../strongs/g/g1424.md), and shall [anaklinō](../../strongs/g/g347.md) with [Abraam](../../strongs/g/g11.md), and [Isaak](../../strongs/g/g2464.md), and [Iakōb](../../strongs/g/g2384.md), in the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md).** [^8]

<a name="matthew_8_12"></a>Matthew 8:12

**But the [huios](../../strongs/g/g5207.md) of the [basileia](../../strongs/g/g932.md) shall be [ekballō](../../strongs/g/g1544.md) into [exōteros](../../strongs/g/g1857.md) [skotos](../../strongs/g/g4655.md): there shall be [klauthmos](../../strongs/g/g2805.md) and [brygmos](../../strongs/g/g1030.md) of [odous](../../strongs/g/g3599.md).** [^9]

<a name="matthew_8_13"></a>Matthew 8:13

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto the [hekatontarchēs](../../strongs/g/g1543.md), **[hypagō](../../strongs/g/g5217.md); and as thou hast [pisteuō](../../strongs/g/g4100.md), [ginomai](../../strongs/g/g1096.md) unto thee.** And his [pais](../../strongs/g/g3816.md) was [iaomai](../../strongs/g/g2390.md) in the [ekeinos](../../strongs/g/g1565.md) [hōra](../../strongs/g/g5610.md). [^10]

<a name="matthew_8_14"></a>Matthew 8:14

And when [Iēsous](../../strongs/g/g2424.md) was [erchomai](../../strongs/g/g2064.md) into [Petros](../../strongs/g/g4074.md) [oikia](../../strongs/g/g3614.md), he [eidō](../../strongs/g/g1492.md) his [penthera](../../strongs/g/g3994.md) [ballō](../../strongs/g/g906.md), and [pyressō](../../strongs/g/g4445.md).

<a name="matthew_8_15"></a>Matthew 8:15

And he [haptomai](../../strongs/g/g680.md) her [cheir](../../strongs/g/g5495.md), and the [pyretos](../../strongs/g/g4446.md) [aphiēmi](../../strongs/g/g863.md) her: and she [egeirō](../../strongs/g/g1453.md), and [diakoneō](../../strongs/g/g1247.md) unto them. [^11]

<a name="matthew_8_16"></a>Matthew 8:16

When the [opsios](../../strongs/g/g3798.md) was [ginomai](../../strongs/g/g1096.md), they [prospherō](../../strongs/g/g4374.md) unto him [polys](../../strongs/g/g4183.md) [daimonizomai](../../strongs/g/g1139.md): and he [ekballō](../../strongs/g/g1544.md) the [pneuma](../../strongs/g/g4151.md) with his [logos](../../strongs/g/g3056.md), and [therapeuō](../../strongs/g/g2323.md) all that were [kakōs](../../strongs/g/g2560.md):

<a name="matthew_8_17"></a>Matthew 8:17

That it might be [plēroō](../../strongs/g/g4137.md) which was [rheō](../../strongs/g/g4483.md) by [Ēsaïas](../../strongs/g/g2268.md) the [prophētēs](../../strongs/g/g4396.md), [legō](../../strongs/g/g3004.md), Himself [lambanō](../../strongs/g/g2983.md) our [astheneia](../../strongs/g/g769.md), and [bastazō](../../strongs/g/g941.md) [nosos](../../strongs/g/g3554.md).

<a name="matthew_8_18"></a>Matthew 8:18

Now when [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) about him, he [keleuō](../../strongs/g/g2753.md) to [aperchomai](../../strongs/g/g565.md) unto the [peran](../../strongs/g/g4008.md). [^12]

<a name="matthew_8_19"></a>Matthew 8:19

And a certain [grammateus](../../strongs/g/g1122.md) [proserchomai](../../strongs/g/g4334.md), and [eipon](../../strongs/g/g2036.md) unto him, [didaskalos](../../strongs/g/g1320.md), I will [akoloutheō](../../strongs/g/g190.md) thee whithersoever thou [aperchomai](../../strongs/g/g565.md).

<a name="matthew_8_20"></a>Matthew 8:20

And [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, **The [alōpēx](../../strongs/g/g258.md) have [phōleos](../../strongs/g/g5454.md), and the [peteinon](../../strongs/g/g4071.md) of [ouranos](../../strongs/g/g3772.md) have [kataskēnōsis](../../strongs/g/g2682.md); but the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) hath not where to [klinō](../../strongs/g/g2827.md) his [kephalē](../../strongs/g/g2776.md).**

<a name="matthew_8_21"></a>Matthew 8:21

And another of his [mathētēs](../../strongs/g/g3101.md) [eipon](../../strongs/g/g2036.md) unto him, [kyrios](../../strongs/g/g2962.md), [epitrepō](../../strongs/g/g2010.md) me [prōton](../../strongs/g/g4412.md) to [aperchomai](../../strongs/g/g565.md) and [thaptō](../../strongs/g/g2290.md) my [patēr](../../strongs/g/g3962.md). [^13]

<a name="matthew_8_22"></a>Matthew 8:22

But [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **[akoloutheō](../../strongs/g/g190.md) me; and [aphiēmi](../../strongs/g/g863.md) the [nekros](../../strongs/g/g3498.md) [thaptō](../../strongs/g/g2290.md) their [nekros](../../strongs/g/g3498.md).** [^14]

<a name="matthew_8_23"></a>Matthew 8:23

And when he was [embainō](../../strongs/g/g1684.md) into a [ploion](../../strongs/g/g4143.md), his [mathētēs](../../strongs/g/g3101.md) [akoloutheō](../../strongs/g/g190.md) him.

<a name="matthew_8_24"></a>Matthew 8:24

And, [idou](../../strongs/g/g2400.md), there [ginomai](../../strongs/g/g1096.md) a [megas](../../strongs/g/g3173.md) [seismos](../../strongs/g/g4578.md) in the [thalassa](../../strongs/g/g2281.md), insomuch that the [ploion](../../strongs/g/g4143.md) was [kalyptō](../../strongs/g/g2572.md) with the [kyma](../../strongs/g/g2949.md): but he was [katheudō](../../strongs/g/g2518.md).

<a name="matthew_8_25"></a>Matthew 8:25

And his [mathētēs](../../strongs/g/g3101.md) [proserchomai](../../strongs/g/g4334.md) [egeirō](../../strongs/g/g1453.md) him, [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), [sōzō](../../strongs/g/g4982.md) us: we [apollymi](../../strongs/g/g622.md). [^15]

<a name="matthew_8_26"></a>Matthew 8:26

And he [legō](../../strongs/g/g3004.md) unto them, **Why are ye [deilos](../../strongs/g/g1169.md), [oligopistos](../../strongs/g/g3640.md)?** Then he [egeirō](../../strongs/g/g1453.md), and [epitimaō](../../strongs/g/g2008.md) the [anemos](../../strongs/g/g417.md) and the [thalassa](../../strongs/g/g2281.md); and there was a [megas](../../strongs/g/g3173.md) [galēnē](../../strongs/g/g1055.md). [^16]

<a name="matthew_8_27"></a>Matthew 8:27

But the [anthrōpos](../../strongs/g/g444.md) [thaumazō](../../strongs/g/g2296.md), [legō](../../strongs/g/g3004.md), [potapos](../../strongs/g/g4217.md) is this, that even the [anemos](../../strongs/g/g417.md) and the [thalassa](../../strongs/g/g2281.md) [hypakouō](../../strongs/g/g5219.md) him!

<a name="matthew_8_28"></a>Matthew 8:28

And when he was [erchomai](../../strongs/g/g2064.md) to the [peran](../../strongs/g/g4008.md) into the [chōra](../../strongs/g/g5561.md) of the [Gerasēnos](../../strongs/g/g1086.md), there [hypantaō](../../strongs/g/g5221.md) him two [daimonizomai](../../strongs/g/g1139.md), [exerchomai](../../strongs/g/g1831.md) of the [mnēmeion](../../strongs/g/g3419.md), [lian](../../strongs/g/g3029.md) [chalepos](../../strongs/g/g5467.md), so that no [tis](../../strongs/g/g5100.md) [ischyō](../../strongs/g/g2480.md) [parerchomai](../../strongs/g/g3928.md) by that [hodos](../../strongs/g/g3598.md). [^17]

<a name="matthew_8_29"></a>Matthew 8:29

And, [idou](../../strongs/g/g2400.md), they [krazō](../../strongs/g/g2896.md), [legō](../../strongs/g/g3004.md), What have we to do with thee, [Iēsous](../../strongs/g/g2424.md), thou [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md)? [erchomai](../../strongs/g/g2064.md) hither to [basanizō](../../strongs/g/g928.md) us before [kairos](../../strongs/g/g2540.md)? [^18]

<a name="matthew_8_30"></a>Matthew 8:30

And there was [makran](../../strongs/g/g3112.md) from them [agelē](../../strongs/g/g34.md) of [polys](../../strongs/g/g4183.md) [choiros](../../strongs/g/g5519.md) [boskō](../../strongs/g/g1006.md). [^19]

<a name="matthew_8_31"></a>Matthew 8:31

So the [daimōn](../../strongs/g/g1142.md) [parakaleō](../../strongs/g/g3870.md) him, [legō](../../strongs/g/g3004.md), If thou [ekballō](../../strongs/g/g1544.md) us, [epitrepō](../../strongs/g/g2010.md) us [aperchomai](../../strongs/g/g565.md) into the [agelē](../../strongs/g/g34.md) of [choiros](../../strongs/g/g5519.md). [^20]

<a name="matthew_8_32"></a>Matthew 8:32

And he [eipon](../../strongs/g/g2036.md) unto them, **[hypagō](../../strongs/g/g5217.md).** And when they were [exerchomai](../../strongs/g/g1831.md), they [aperchomai](../../strongs/g/g565.md) into the [agelē](../../strongs/g/g34.md) of [choiros](../../strongs/g/g5519.md): and, [idou](../../strongs/g/g2400.md), the whole [agelē](../../strongs/g/g34.md) of [choiros](../../strongs/g/g5519.md) [hormaō](../../strongs/g/g3729.md) down a [krēmnos](../../strongs/g/g2911.md) into the [thalassa](../../strongs/g/g2281.md), and [apothnēskō](../../strongs/g/g599.md) in the [hydōr](../../strongs/g/g5204.md). [^21]

<a name="matthew_8_33"></a>Matthew 8:33

And they that [boskō](../../strongs/g/g1006.md) them [pheugō](../../strongs/g/g5343.md), and [aperchomai](../../strongs/g/g565.md) into the [polis](../../strongs/g/g4172.md), and [apaggellō](../../strongs/g/g518.md) every thing, and [daimonizomai](../../strongs/g/g1139.md).

<a name="matthew_8_34"></a>Matthew 8:34

And, [idou](../../strongs/g/g2400.md), the [pas](../../strongs/g/g3956.md) [polis](../../strongs/g/g4172.md) [exerchomai](../../strongs/g/g1831.md) to [synantēsis](../../strongs/g/g4877.md) [Iēsous](../../strongs/g/g2424.md): and when they [eidō](../../strongs/g/g1492.md) him, they [parakaleō](../../strongs/g/g3870.md) him that he would [metabainō](../../strongs/g/g3327.md) out of their [horion](../../strongs/g/g3725.md).

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 7](matthew_7.md) - [Matthew 9](matthew_9.md)

---

[^1]: [Matthew 8:3 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_3)

[^2]: [Matthew 8:5 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_5)

[^3]: [Matthew 8:6 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_6)

[^4]: [Matthew 8:7 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_7)

[^5]: [Matthew 8:8 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_8)

[^6]: [Matthew 8:9 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_9)

[^7]: [Matthew 8:10 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_10)

[^8]: [Matthew 8:11 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_11)

[^9]: [Matthew 8:12 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_12)

[^10]: [Matthew 8:13 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_13)

[^11]: [Matthew 8:15 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_15)

[^12]: [Matthew 8:18 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_18)

[^13]: [Matthew 8:21 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_21)

[^14]: [Matthew 8:22 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_22)

[^15]: [Matthew 8:25 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_25)

[^16]: [Matthew 8:26 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_26)

[^17]: [Matthew 8:28 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_28)

[^18]: [Matthew 8:29 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_29)

[^19]: [Matthew 8:30 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_30)

[^20]: [Matthew 8:31 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_31)

[^21]: [Matthew 8:32 Commentary](../../commentary/matthew/matthew_8_commentary.md#matthew_8_32)
