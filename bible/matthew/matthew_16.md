# [Matthew 16](https://www.blueletterbible.org/kjv/mat/16/1/rl1/s_945001)

<a name="matthew_16_1"></a>Matthew 16:1

The [Pharisaios](../../strongs/g/g5330.md) also with the [Saddoukaios](../../strongs/g/g4523.md) [proserchomai](../../strongs/g/g4334.md), and [peirazō](../../strongs/g/g3985.md) [eperōtaō](../../strongs/g/g1905.md) him that he would [epideiknymi](../../strongs/g/g1925.md) them a [sēmeion](../../strongs/g/g4592.md) from [ouranos](../../strongs/g/g3772.md).

<a name="matthew_16_2"></a>Matthew 16:2

He [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **When it is [opsios](../../strongs/g/g3798.md), ye [legō](../../strongs/g/g3004.md), be [eudia](../../strongs/g/g2105.md): for the [ouranos](../../strongs/g/g3772.md) is [pyrrazō](../../strongs/g/g4449.md).** [^1]

<a name="matthew_16_3"></a>Matthew 16:3

**And [prōï](../../strongs/g/g4404.md), [cheimōn](../../strongs/g/g5494.md) [sēmeron](../../strongs/g/g4594.md): for the [ouranos](../../strongs/g/g3772.md) is [pyrrazō](../../strongs/g/g4449.md) and [stygnazō](../../strongs/g/g4768.md). O ye [hypokritēs](../../strongs/g/g5273.md), ye [ginōskō](../../strongs/g/g1097.md) [diakrinō](../../strongs/g/g1252.md) [men](../../strongs/g/g3303.md) the [prosōpon](../../strongs/g/g4383.md) of the [ouranos](../../strongs/g/g3772.md); but [dynamai](../../strongs/g/g1410.md) ye not the [sēmeion](../../strongs/g/g4592.md) of the [kairos](../../strongs/g/g2540.md)?**

<a name="matthew_16_4"></a>Matthew 16:4

**A [ponēros](../../strongs/g/g4190.md) and [moichalis](../../strongs/g/g3428.md) [genea](../../strongs/g/g1074.md) [epizēteō](../../strongs/g/g1934.md) a [sēmeion](../../strongs/g/g4592.md); and there shall no [sēmeion](../../strongs/g/g4592.md) be [didōmi](../../strongs/g/g1325.md) unto it, but the [sēmeion](../../strongs/g/g4592.md) of the [prophētēs](../../strongs/g/g4396.md) [Iōnas](../../strongs/g/g2495.md).** And he [kataleipō](../../strongs/g/g2641.md) them, and [aperchomai](../../strongs/g/g565.md).

<a name="matthew_16_5"></a>Matthew 16:5

And when his [mathētēs](../../strongs/g/g3101.md) were [erchomai](../../strongs/g/g2064.md) to the [peran](../../strongs/g/g4008.md), they had [epilanthanomai](../../strongs/g/g1950.md) to [lambanō](../../strongs/g/g2983.md) [artos](../../strongs/g/g740.md).

<a name="matthew_16_6"></a>Matthew 16:6

Then [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **[horaō](../../strongs/g/g3708.md) and [prosechō](../../strongs/g/g4337.md) of the [zymē](../../strongs/g/g2219.md) of the [Pharisaios](../../strongs/g/g5330.md) and of the [Saddoukaios](../../strongs/g/g4523.md).**

<a name="matthew_16_7"></a>Matthew 16:7

And they [dialogizomai](../../strongs/g/g1260.md) among themselves, [legō](../../strongs/g/g3004.md), because we have [lambanō](../../strongs/g/g2983.md) no [artos](../../strongs/g/g740.md).

<a name="matthew_16_8"></a>Matthew 16:8

when [Iēsous](../../strongs/g/g2424.md) [ginōskō](../../strongs/g/g1097.md), he [eipon](../../strongs/g/g2036.md) unto them, **[oligopistos](../../strongs/g/g3640.md), why [dialogizomai](../../strongs/g/g1260.md) ye among yourselves, because ye have [lambanō](../../strongs/g/g2983.md) no [artos](../../strongs/g/g740.md)?**

<a name="matthew_16_9"></a>Matthew 16:9

**Do ye not yet [noeō](../../strongs/g/g3539.md), neither [mnēmoneuō](../../strongs/g/g3421.md) the five [artos](../../strongs/g/g740.md) of the five thousand, and how many [kophinos](../../strongs/g/g2894.md) ye [lambanō](../../strongs/g/g2983.md)?**

<a name="matthew_16_10"></a>Matthew 16:10

**Neither the seven [artos](../../strongs/g/g740.md) of the four thousand, and how many [spyris](../../strongs/g/g4711.md) ye [lambanō](../../strongs/g/g2983.md)?**

<a name="matthew_16_11"></a>Matthew 16:11

**How is it that ye do not [noeō](../../strongs/g/g3539.md) that I [eipon](../../strongs/g/g2036.md) it not to you concerning [artos](../../strongs/g/g740.md), that ye should [prosechō](../../strongs/g/g4337.md) of the [zymē](../../strongs/g/g2219.md) of the [Pharisaios](../../strongs/g/g5330.md) and of the [Saddoukaios](../../strongs/g/g4523.md)?**

<a name="matthew_16_12"></a>Matthew 16:12

Then [syniēmi](../../strongs/g/g4920.md) they how that he [eipon](../../strongs/g/g2036.md) not [prosechō](../../strongs/g/g4337.md) of the [zymē](../../strongs/g/g2219.md) of [artos](../../strongs/g/g740.md), but of the [didachē](../../strongs/g/g1322.md) of the [Pharisaios](../../strongs/g/g5330.md) and of the [Saddoukaios](../../strongs/g/g4523.md). [^2]

<a name="matthew_16_13"></a>Matthew 16:13

When [Iēsous](../../strongs/g/g2424.md) [erchomai](../../strongs/g/g2064.md) into the [meros](../../strongs/g/g3313.md) of [Kaisareia](../../strongs/g/g2542.md) [Philippos](../../strongs/g/g5376.md), he [erōtaō](../../strongs/g/g2065.md) his [mathētēs](../../strongs/g/g3101.md), [legō](../../strongs/g/g3004.md), **Whom do [anthrōpos](../../strongs/g/g444.md) [legō](../../strongs/g/g3004.md) that I the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [einai](../../strongs/g/g1511.md)?**

<a name="matthew_16_14"></a>Matthew 16:14

And they [eipon](../../strongs/g/g2036.md), Some [Iōannēs](../../strongs/g/g2491.md) the [baptistēs](../../strongs/g/g910.md): some, [Ēlias](../../strongs/g/g2243.md); and others, [Ieremias](../../strongs/g/g2408.md), or one of the [prophētēs](../../strongs/g/g4396.md).

<a name="matthew_16_15"></a>Matthew 16:15

He [legō](../../strongs/g/g3004.md) unto them, But whom [legō](../../strongs/g/g3004.md) ye that I [einai](../../strongs/g/g1511.md)?

<a name="matthew_16_16"></a>Matthew 16:16

And [Simōn](../../strongs/g/g4613.md) [Petros](../../strongs/g/g4074.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), Thou art the [Christos](../../strongs/g/g5547.md), the [huios](../../strongs/g/g5207.md) of the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md).

<a name="matthew_16_17"></a>Matthew 16:17

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, **[makarios](../../strongs/g/g3107.md) art thou, [Simōn](../../strongs/g/g4613.md) [Bariōna](../../strongs/g/g920.md): for [sarx](../../strongs/g/g4561.md) and [haima](../../strongs/g/g129.md) hath not [apokalyptō](../../strongs/g/g601.md) it unto thee, but my [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_16_18"></a>Matthew 16:18

**And I [legō](../../strongs/g/g3004.md) also unto thee, That thou art [Petros](../../strongs/g/g4074.md), and upon this [petra](../../strongs/g/g4073.md) I will [oikodomeō](../../strongs/g/g3618.md) my [ekklēsia](../../strongs/g/g1577.md); and the [pylē](../../strongs/g/g4439.md) of [hadēs](../../strongs/g/g86.md) shall not [katischyō](../../strongs/g/g2729.md) it.**

<a name="matthew_16_19"></a>Matthew 16:19

**And I will [didōmi](../../strongs/g/g1325.md) unto thee the [kleis](../../strongs/g/g2807.md) of the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md): and whatsoever thou shalt [deō](../../strongs/g/g1210.md) on [gē](../../strongs/g/g1093.md) shall be [deō](../../strongs/g/g1210.md) in [ouranos](../../strongs/g/g3772.md): and whatsoever thou shalt [lyō](../../strongs/g/g3089.md) on [gē](../../strongs/g/g1093.md) shall be [lyō](../../strongs/g/g3089.md) in [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_16_20"></a>Matthew 16:20

Then [diastellō](../../strongs/g/g1291.md) he his [mathētēs](../../strongs/g/g3101.md) that they should [eipon](../../strongs/g/g2036.md) [mēdeis](../../strongs/g/g3367.md) that he was [Iēsous](../../strongs/g/g2424.md) the [Christos](../../strongs/g/g5547.md).

<a name="matthew_16_21"></a>Matthew 16:21

From that time forth [archomai](../../strongs/g/g756.md) [Iēsous](../../strongs/g/g2424.md) to [deiknyō](../../strongs/g/g1166.md) unto his [mathētēs](../../strongs/g/g3101.md), how that he must [aperchomai](../../strongs/g/g565.md) unto [Hierosolyma](../../strongs/g/g2414.md), and [paschō](../../strongs/g/g3958.md) [polys](../../strongs/g/g4183.md) of the [presbyteros](../../strongs/g/g4245.md) and [archiereus](../../strongs/g/g749.md) and [grammateus](../../strongs/g/g1122.md), and be [apokteinō](../../strongs/g/g615.md), and be [egeirō](../../strongs/g/g1453.md) the third [hēmera](../../strongs/g/g2250.md).

<a name="matthew_16_22"></a>Matthew 16:22

Then [Petros](../../strongs/g/g4074.md) [proslambanō](../../strongs/g/g4355.md) him, and [archomai](../../strongs/g/g756.md) to [epitimaō](../../strongs/g/g2008.md) him, [legō](../../strongs/g/g3004.md), [hileōs](../../strongs/g/g2436.md) from thee, [kyrios](../../strongs/g/g2962.md): this [esomai](../../strongs/g/g2071.md) not unto thee.

<a name="matthew_16_23"></a>Matthew 16:23

But he [strephō](../../strongs/g/g4762.md), and [eipon](../../strongs/g/g2036.md) unto [Petros](../../strongs/g/g4074.md), **[hypagō](../../strongs/g/g5217.md) [opisō](../../strongs/g/g3694.md) me, [Satanas](../../strongs/g/g4567.md): thou art [skandalon](../../strongs/g/g4625.md) unto me: for thou [phroneō](../../strongs/g/g5426.md) not the things that be of [theos](../../strongs/g/g2316.md), but [anthrōpos](../../strongs/g/g444.md).**

<a name="matthew_16_24"></a>Matthew 16:24

Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) unto his [mathētēs](../../strongs/g/g3101.md), **If any will [erchomai](../../strongs/g/g2064.md) after me, let him [aparneomai](../../strongs/g/g533.md) himself, and [airō](../../strongs/g/g142.md) his [stauros](../../strongs/g/g4716.md), and [akoloutheō](../../strongs/g/g190.md) me.**

<a name="matthew_16_25"></a>Matthew 16:25

**For whosoever will [sōzō](../../strongs/g/g4982.md) his [psychē](../../strongs/g/g5590.md) shall [apollymi](../../strongs/g/g622.md) it: and whosoever will [apollymi](../../strongs/g/g622.md) his [psychē](../../strongs/g/g5590.md) for my sake shall [heuriskō](../../strongs/g/g2147.md) it.**

<a name="matthew_16_26"></a>Matthew 16:26

**For what is an [anthrōpos](../../strongs/g/g444.md) [ōpheleō](../../strongs/g/g5623.md), if he shall [kerdainō](../../strongs/g/g2770.md) the [holos](../../strongs/g/g3650.md) [kosmos](../../strongs/g/g2889.md), and [zēmioō](../../strongs/g/g2210.md) his own [psychē](../../strongs/g/g5590.md)? or what shall an [anthrōpos](../../strongs/g/g444.md) [didōmi](../../strongs/g/g1325.md) in [antallagma](../../strongs/g/g465.md) for his [psychē](../../strongs/g/g5590.md)?**

<a name="matthew_16_27"></a>Matthew 16:27

**For the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) shall [erchomai](../../strongs/g/g2064.md) in the [doxa](../../strongs/g/g1391.md) of his [patēr](../../strongs/g/g3962.md) with his [aggelos](../../strongs/g/g32.md); and then he shall [apodidōmi](../../strongs/g/g591.md) [hekastos](../../strongs/g/g1538.md) according to his [praxis](../../strongs/g/g4234.md).**

<a name="matthew_16_28"></a>Matthew 16:28

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, There be some [histēmi](../../strongs/g/g2476.md) [hōde](../../strongs/g/g5602.md), which shall not [geuomai](../../strongs/g/g1089.md) of [thanatos](../../strongs/g/g2288.md), till they [eidō](../../strongs/g/g1492.md) the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [erchomai](../../strongs/g/g2064.md) in his [basileia](../../strongs/g/g932.md).**

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 15](matthew_15.md) - [Matthew 17](matthew_17.md)

---

[^1]: [Matthew 16:2 Commentary](../../commentary/matthew/matthew_16_commentary.md#matthew_16_2)

[^2]: [Matthew 16:12 Commentary](../../commentary/matthew/matthew_16_commentary.md#matthew_16_12)
