# [Matthew 21](https://www.blueletterbible.org/kjv/mat/21/1/rl1/s_950001)

<a name="matthew_21_1"></a>Matthew 21:1

And when they [eggizō](../../strongs/g/g1448.md) unto [Hierosolyma](../../strongs/g/g2414.md), and were [erchomai](../../strongs/g/g2064.md) to [Bēthphagē](../../strongs/g/g967.md), unto the [oros](../../strongs/g/g3735.md) of [elaia](../../strongs/g/g1636.md), then [apostellō](../../strongs/g/g649.md) [Iēsous](../../strongs/g/g2424.md) two [mathētēs](../../strongs/g/g3101.md),

<a name="matthew_21_2"></a>Matthew 21:2

[legō](../../strongs/g/g3004.md) unto them, **[poreuō](../../strongs/g/g4198.md) into the [kōmē](../../strongs/g/g2968.md) over [apenanti](../../strongs/g/g561.md) you, and [eutheōs](../../strongs/g/g2112.md) ye shall [heuriskō](../../strongs/g/g2147.md) an [onos](../../strongs/g/g3688.md) [deō](../../strongs/g/g1210.md), and a [pōlos](../../strongs/g/g4454.md) with her: [lyō](../../strongs/g/g3089.md), and [agō](../../strongs/g/g71.md) unto me.**

<a name="matthew_21_3"></a>Matthew 21:3

**And if any [eipon](../../strongs/g/g2036.md) ought unto you, ye shall [eipon](../../strongs/g/g2046.md), [kyrios](../../strongs/g/g2962.md) hath [chreia](../../strongs/g/g5532.md) of them; and [eutheōs](../../strongs/g/g2112.md) he will [apostellō](../../strongs/g/g649.md) them.**

<a name="matthew_21_4"></a>Matthew 21:4

All this was [ginomai](../../strongs/g/g1096.md), that it might be [plēroō](../../strongs/g/g4137.md) which was [rheō](../../strongs/g/g4483.md) by the [prophētēs](../../strongs/g/g4396.md), [legō](../../strongs/g/g3004.md),

<a name="matthew_21_5"></a>Matthew 21:5

[eipon](../../strongs/g/g2036.md) ye the [thygatēr](../../strongs/g/g2364.md) of [Siōn](../../strongs/g/g4622.md), [idou](../../strongs/g/g2400.md), thy [basileus](../../strongs/g/g935.md) [erchomai](../../strongs/g/g2064.md) unto thee, [praÿs](../../strongs/g/g4239.md), and [epibainō](../../strongs/g/g1910.md) upon an [onos](../../strongs/g/g3688.md), and a [pōlos](../../strongs/g/g4454.md) the [huios](../../strongs/g/g5207.md) of [hypozygion](../../strongs/g/g5268.md).

<a name="matthew_21_6"></a>Matthew 21:6

And the [mathētēs](../../strongs/g/g3101.md) [poreuō](../../strongs/g/g4198.md), and [poieō](../../strongs/g/g4160.md) as [Iēsous](../../strongs/g/g2424.md) [prostassō](../../strongs/g/g4367.md) them,

<a name="matthew_21_7"></a>Matthew 21:7

And [agō](../../strongs/g/g71.md) the [onos](../../strongs/g/g3688.md), and the [pōlos](../../strongs/g/g4454.md), and [epitithēmi](../../strongs/g/g2007.md) on them their [himation](../../strongs/g/g2440.md), and they [epikathizō](../../strongs/g/g1940.md) thereon.

<a name="matthew_21_8"></a>Matthew 21:8

And [pleistos](../../strongs/g/g4118.md) [ochlos](../../strongs/g/g3793.md) [strōnnyō](../../strongs/g/g4766.md) their [himation](../../strongs/g/g2440.md) in [hodos](../../strongs/g/g3598.md); others [koptō](../../strongs/g/g2875.md) [klados](../../strongs/g/g2798.md) from the [dendron](../../strongs/g/g1186.md), and [strōnnyō](../../strongs/g/g4766.md) in [hodos](../../strongs/g/g3598.md).

<a name="matthew_21_9"></a>Matthew 21:9

And the [ochlos](../../strongs/g/g3793.md) that [proagō](../../strongs/g/g4254.md), and that [akoloutheō](../../strongs/g/g190.md), [krazō](../../strongs/g/g2896.md), [legō](../../strongs/g/g3004.md), [hōsanna](../../strongs/g/g5614.md) to the [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md): [eulogeō](../../strongs/g/g2127.md) is he that [erchomai](../../strongs/g/g2064.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md); [hōsanna](../../strongs/g/g5614.md) in the [hypsistos](../../strongs/g/g5310.md).

<a name="matthew_21_10"></a>Matthew 21:10

And when he was [eiserchomai](../../strongs/g/g1525.md) into [Hierosolyma](../../strongs/g/g2414.md), all the [polis](../../strongs/g/g4172.md) was [seiō](../../strongs/g/g4579.md), [legō](../../strongs/g/g3004.md), Who is this?

<a name="matthew_21_11"></a>Matthew 21:11

And the [ochlos](../../strongs/g/g3793.md) [legō](../../strongs/g/g3004.md), This is [Iēsous](../../strongs/g/g2424.md) the [prophētēs](../../strongs/g/g4396.md) of [Nazara](../../strongs/g/g3478.md) of [Galilaia](../../strongs/g/g1056.md).

<a name="matthew_21_12"></a>Matthew 21:12

And [Iēsous](../../strongs/g/g2424.md) [eiserchomai](../../strongs/g/g1525.md) into the [hieron](../../strongs/g/g2411.md) of [theos](../../strongs/g/g2316.md), and [ekballō](../../strongs/g/g1544.md) all them that [pōleō](../../strongs/g/g4453.md) and [agorazō](../../strongs/g/g59.md) in the [hieron](../../strongs/g/g2411.md), and [katastrephō](../../strongs/g/g2690.md) the [trapeza](../../strongs/g/g5132.md) of the [kollybistēs](../../strongs/g/g2855.md), and the [kathedra](../../strongs/g/g2515.md) of them that [pōleō](../../strongs/g/g4453.md) [peristera](../../strongs/g/g4058.md),

<a name="matthew_21_13"></a>Matthew 21:13

And [legō](../../strongs/g/g3004.md) unto them, **It is [graphō](../../strongs/g/g1125.md), My [oikos](../../strongs/g/g3624.md) shall be [kaleō](../../strongs/g/g2564.md) the [oikos](../../strongs/g/g3624.md) of [proseuchē](../../strongs/g/g4335.md); but ye have [poieō](../../strongs/g/g4160.md) it a [spēlaion](../../strongs/g/g4693.md) of [lēstēs](../../strongs/g/g3027.md).**

<a name="matthew_21_14"></a>Matthew 21:14

And the [typhlos](../../strongs/g/g5185.md) and the [chōlos](../../strongs/g/g5560.md) [proserchomai](../../strongs/g/g4334.md) to him in the [hieron](../../strongs/g/g2411.md); and he [therapeuō](../../strongs/g/g2323.md) them.

<a name="matthew_21_15"></a>Matthew 21:15

And when the [archiereus](../../strongs/g/g749.md) and [grammateus](../../strongs/g/g1122.md) [eidō](../../strongs/g/g1492.md) the [thaumasios](../../strongs/g/g2297.md) that he [poieō](../../strongs/g/g4160.md), and the [pais](../../strongs/g/g3816.md) [krazō](../../strongs/g/g2896.md) in the [hieron](../../strongs/g/g2411.md), and [legō](../../strongs/g/g3004.md), [hōsanna](../../strongs/g/g5614.md) to the [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md); they were [aganakteō](../../strongs/g/g23.md),

<a name="matthew_21_16"></a>Matthew 21:16

And [eipon](../../strongs/g/g2036.md) unto him, [akouō](../../strongs/g/g191.md) thou what these [legō](../../strongs/g/g3004.md)? And [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, **[nai](../../strongs/g/g3483.md); have ye never [anaginōskō](../../strongs/g/g314.md), Out of the [stoma](../../strongs/g/g4750.md) of [nēpios](../../strongs/g/g3516.md) and [thēlazō](../../strongs/g/g2337.md) thou hast [katartizō](../../strongs/g/g2675.md) [ainos](../../strongs/g/g136.md)?**

<a name="matthew_21_17"></a>Matthew 21:17

And he [kataleipō](../../strongs/g/g2641.md) them, and [exerchomai](../../strongs/g/g1831.md) of the [polis](../../strongs/g/g4172.md) into [Bēthania](../../strongs/g/g963.md); and he [aulizomai](../../strongs/g/g835.md) there.

<a name="matthew_21_18"></a>Matthew 21:18

Now in the [prōïa](../../strongs/g/g4405.md) as he [epanagō](../../strongs/g/g1877.md) into the [polis](../../strongs/g/g4172.md), he [peinaō](../../strongs/g/g3983.md).

<a name="matthew_21_19"></a>Matthew 21:19

And when he [eidō](../../strongs/g/g1492.md) a [sykē](../../strongs/g/g4808.md) in [hodos](../../strongs/g/g3598.md), he [erchomai](../../strongs/g/g2064.md) to it, and [heuriskō](../../strongs/g/g2147.md) nothing thereon, but [phyllon](../../strongs/g/g5444.md) only, and [legō](../../strongs/g/g3004.md) unto it, **Let no [karpos](../../strongs/g/g2590.md) [ginomai](../../strongs/g/g1096.md) on thee [mēketi](../../strongs/g/g3371.md) for [aiōn](../../strongs/g/g165.md).** And [parachrēma](../../strongs/g/g3916.md) the [sykē](../../strongs/g/g4808.md) [xērainō](../../strongs/g/g3583.md).

<a name="matthew_21_20"></a>Matthew 21:20

And when the [mathētēs](../../strongs/g/g3101.md) [eidō](../../strongs/g/g1492.md), they [thaumazō](../../strongs/g/g2296.md), [legō](../../strongs/g/g3004.md), How [parachrēma](../../strongs/g/g3916.md) is the [sykē](../../strongs/g/g4808.md) [xērainō](../../strongs/g/g3583.md)!

<a name="matthew_21_21"></a>Matthew 21:21

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, If ye have [pistis](../../strongs/g/g4102.md), and [diakrinō](../../strongs/g/g1252.md) not, ye shall not only [poieō](../../strongs/g/g4160.md) this to the [sykē](../../strongs/g/g4808.md), but also if ye shall [eipon](../../strongs/g/g2036.md) unto this [oros](../../strongs/g/g3735.md), [airō](../../strongs/g/g142.md), and be thou [ballō](../../strongs/g/g906.md) into the [thalassa](../../strongs/g/g2281.md); it shall be [ginomai](../../strongs/g/g1096.md).**

<a name="matthew_21_22"></a>Matthew 21:22

**And all things, whatsoever ye shall [aiteō](../../strongs/g/g154.md) in [proseuchē](../../strongs/g/g4335.md), [pisteuō](../../strongs/g/g4100.md), ye shall [lambanō](../../strongs/g/g2983.md).**

<a name="matthew_21_23"></a>Matthew 21:23

And when he was [erchomai](../../strongs/g/g2064.md) into the [hieron](../../strongs/g/g2411.md), the [archiereus](../../strongs/g/g749.md) and the [presbyteros](../../strongs/g/g4245.md) of the [laos](../../strongs/g/g2992.md) [proserchomai](../../strongs/g/g4334.md) unto him as he was [didaskō](../../strongs/g/g1321.md), and [legō](../../strongs/g/g3004.md), By what [exousia](../../strongs/g/g1849.md) [poieō](../../strongs/g/g4160.md) thou these things? and who [didōmi](../../strongs/g/g1325.md) thee this [exousia](../../strongs/g/g1849.md)? [^1]

<a name="matthew_21_24"></a>Matthew 21:24

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **I also will [erōtaō](../../strongs/g/g2065.md) you one [logos](../../strongs/g/g3056.md), which if ye [eipon](../../strongs/g/g2036.md) me, I in like wise will [eipon](../../strongs/g/g2046.md) you by what [exousia](../../strongs/g/g1849.md) I [poieō](../../strongs/g/g4160.md) these things.**

<a name="matthew_21_25"></a>Matthew 21:25

**The [baptisma](../../strongs/g/g908.md) of [Iōannēs](../../strongs/g/g2491.md), whence was it? from [ouranos](../../strongs/g/g3772.md), or of [anthrōpos](../../strongs/g/g444.md)? And they [dialogizomai](../../strongs/g/g1260.md) with themselves, [legō](../../strongs/g/g3004.md), If we shall [eipon](../../strongs/g/g2036.md), From [ouranos](../../strongs/g/g3772.md); he will [eipon](../../strongs/g/g2046.md) unto us, Why did ye not then [pisteuō](../../strongs/g/g4100.md) him?**

<a name="matthew_21_26"></a>Matthew 21:26

But if we shall [eipon](../../strongs/g/g2036.md), Of [anthrōpos](../../strongs/g/g444.md); we [phobeō](../../strongs/g/g5399.md) the [ochlos](../../strongs/g/g3793.md); for all hold [Iōannēs](../../strongs/g/g2491.md) as a [prophētēs](../../strongs/g/g4396.md).

<a name="matthew_21_27"></a>Matthew 21:27

And they [apokrinomai](../../strongs/g/g611.md) [Iēsous](../../strongs/g/g2424.md), and [eipon](../../strongs/g/g2036.md), We cannot [eidō](../../strongs/g/g1492.md). And he [phēmi](../../strongs/g/g5346.md) unto them, **Neither [legō](../../strongs/g/g3004.md) I you by what [exousia](../../strongs/g/g1849.md) I [poieō](../../strongs/g/g4160.md) these things.**

<a name="matthew_21_28"></a>Matthew 21:28

**But what [dokeō](../../strongs/g/g1380.md) ye? An [anthrōpos](../../strongs/g/g444.md) had two [teknon](../../strongs/g/g5043.md); and he [proserchomai](../../strongs/g/g4334.md) to the first, and [eipon](../../strongs/g/g2036.md), [teknon](../../strongs/g/g5043.md), [hypagō](../../strongs/g/g5217.md) [ergazomai](../../strongs/g/g2038.md) [sēmeron](../../strongs/g/g4594.md) in my [ampelōn](../../strongs/g/g290.md).**

<a name="matthew_21_29"></a>Matthew 21:29

**He [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), I will not: but afterward he [metamelomai](../../strongs/g/g3338.md), and [aperchomai](../../strongs/g/g565.md).**

<a name="matthew_21_30"></a>Matthew 21:30

**And he [proserchomai](../../strongs/g/g4334.md) to the second, and [eipon](../../strongs/g/g2036.md) [hōsautōs](../../strongs/g/g5615.md). And he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), [egō](../../strongs/g/g1473.md) [kyrios](../../strongs/g/g2962.md): and [aperchomai](../../strongs/g/g565.md) not.**

<a name="matthew_21_31"></a>Matthew 21:31

**Whether of them twain [poieō](../../strongs/g/g4160.md) the [thelēma](../../strongs/g/g2307.md) of [patēr](../../strongs/g/g3962.md)?** They [legō](../../strongs/g/g3004.md) unto him, The first. [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, **That the [telōnēs](../../strongs/g/g5057.md) and the [pornē](../../strongs/g/g4204.md) [proagō](../../strongs/g/g4254.md) into the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) before you.** [^2]

<a name="matthew_21_32"></a>Matthew 21:32

**For [Iōannēs](../../strongs/g/g2491.md) [erchomai](../../strongs/g/g2064.md) unto you in the [hodos](../../strongs/g/g3598.md) of [dikaiosynē](../../strongs/g/g1343.md), and ye [pisteuō](../../strongs/g/g4100.md) him not: but the [telōnēs](../../strongs/g/g5057.md) and the [pornē](../../strongs/g/g4204.md) [pisteuō](../../strongs/g/g4100.md) him: and ye, when ye had [eidō](../../strongs/g/g1492.md) it, [metamelomai](../../strongs/g/g3338.md) not afterward, that ye might [pisteuō](../../strongs/g/g4100.md) him.**

<a name="matthew_21_33"></a>Matthew 21:33

**[akouō](../../strongs/g/g191.md) another [parabolē](../../strongs/g/g3850.md): There was an [anthrōpos](../../strongs/g/g444.md) [oikodespotēs](../../strongs/g/g3617.md), which [phyteuō](../../strongs/g/g5452.md) an [ampelōn](../../strongs/g/g290.md), and [phragmos](../../strongs/g/g5418.md) it [peritithēmi](../../strongs/g/g4060.md), and [oryssō](../../strongs/g/g3736.md) a [lēnos](../../strongs/g/g3025.md) in it, and [oikodomeō](../../strongs/g/g3618.md) a [pyrgos](../../strongs/g/g4444.md), and [ekdidōmi](../../strongs/g/g1554.md) it to [geōrgos](../../strongs/g/g1092.md), and [apodēmeō](../../strongs/g/g589.md):**

<a name="matthew_21_34"></a>Matthew 21:34

**And when the [kairos](../../strongs/g/g2540.md) of the [karpos](../../strongs/g/g2590.md) [eggizō](../../strongs/g/g1448.md), he [apostellō](../../strongs/g/g649.md) his [doulos](../../strongs/g/g1401.md) to the [geōrgos](../../strongs/g/g1092.md), that they might [lambanō](../../strongs/g/g2983.md) the [karpos](../../strongs/g/g2590.md) of it.**

<a name="matthew_21_35"></a>Matthew 21:35

**And the [geōrgos](../../strongs/g/g1092.md) [lambanō](../../strongs/g/g2983.md) his [doulos](../../strongs/g/g1401.md), and [derō](../../strongs/g/g1194.md) one, and [apokteinō](../../strongs/g/g615.md) another, and [lithoboleō](../../strongs/g/g3036.md) another.**

<a name="matthew_21_36"></a>Matthew 21:36

**Again, he [apostellō](../../strongs/g/g649.md) other [doulos](../../strongs/g/g1401.md) more than the first: and they [poieō](../../strongs/g/g4160.md) unto them [hōsautōs](../../strongs/g/g5615.md).**

<a name="matthew_21_37"></a>Matthew 21:37

**But last of all he [apostellō](../../strongs/g/g649.md) unto them his [huios](../../strongs/g/g5207.md), [legō](../../strongs/g/g3004.md), They will [entrepō](../../strongs/g/g1788.md) my [huios](../../strongs/g/g5207.md).**

<a name="matthew_21_38"></a>Matthew 21:38

**But when the [geōrgos](../../strongs/g/g1092.md) [eidō](../../strongs/g/g1492.md) the [huios](../../strongs/g/g5207.md), they [eipon](../../strongs/g/g2036.md) among themselves, This is the [klēronomos](../../strongs/g/g2818.md); [deute](../../strongs/g/g1205.md), let us [apokteinō](../../strongs/g/g615.md) him, and let us [katechō](../../strongs/g/g2722.md) on his [klēronomia](../../strongs/g/g2817.md).**

<a name="matthew_21_39"></a>Matthew 21:39

**And they [lambanō](../../strongs/g/g2983.md) him, and [ekballō](../../strongs/g/g1544.md) out of the [ampelōn](../../strongs/g/g290.md), and [apokteinō](../../strongs/g/g615.md).**

<a name="matthew_21_40"></a>Matthew 21:40

**When the [kyrios](../../strongs/g/g2962.md) therefore of the [ampelōn](../../strongs/g/g290.md) [erchomai](../../strongs/g/g2064.md), what will he [poieō](../../strongs/g/g4160.md) unto those [geōrgos](../../strongs/g/g1092.md)?**

<a name="matthew_21_41"></a>Matthew 21:41

They [legō](../../strongs/g/g3004.md) unto him, He will [kakōs](../../strongs/g/g2560.md) [apollymi](../../strongs/g/g622.md) those [kakos](../../strongs/g/g2556.md), and will [ekdidōmi](../../strongs/g/g1554.md) [ampelōn](../../strongs/g/g290.md) unto other [geōrgos](../../strongs/g/g1092.md), which shall [apodidōmi](../../strongs/g/g591.md) him the [karpos](../../strongs/g/g2590.md) in their [kairos](../../strongs/g/g2540.md).

<a name="matthew_21_42"></a>Matthew 21:42

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, **Did ye never [anaginōskō](../../strongs/g/g314.md) in the [graphē](../../strongs/g/g1124.md), The [lithos](../../strongs/g/g3037.md) which the [oikodomeō](../../strongs/g/g3618.md) [apodokimazō](../../strongs/g/g593.md), the same is become the [kephalē](../../strongs/g/g2776.md) of the [gōnia](../../strongs/g/g1137.md): this is the [kyrios](../../strongs/g/g2962.md) [ginomai](../../strongs/g/g1096.md), and it is [thaumastos](../../strongs/g/g2298.md) in our [ophthalmos](../../strongs/g/g3788.md)?**

<a name="matthew_21_43"></a>Matthew 21:43

**Therefore [legō](../../strongs/g/g3004.md) I unto you, The [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) shall be [airō](../../strongs/g/g142.md) from you, and [didōmi](../../strongs/g/g1325.md) to [ethnos](../../strongs/g/g1484.md) [poieō](../../strongs/g/g4160.md) the [karpos](../../strongs/g/g2590.md) thereof.**

<a name="matthew_21_44"></a>Matthew 21:44

**And whosoever shall [piptō](../../strongs/g/g4098.md) on this [lithos](../../strongs/g/g3037.md) shall be [synthlaō](../../strongs/g/g4917.md): but on whomsoever it shall [piptō](../../strongs/g/g4098.md), it will [likmaō](../../strongs/g/g3039.md) him.** [^3]

<a name="matthew_21_45"></a>Matthew 21:45

And when the [archiereus](../../strongs/g/g749.md) and [Pharisaios](../../strongs/g/g5330.md) had [akouō](../../strongs/g/g191.md) his [parabolē](../../strongs/g/g3850.md), they [ginōskō](../../strongs/g/g1097.md) that he [legō](../../strongs/g/g3004.md) of them.

<a name="matthew_21_46"></a>Matthew 21:46

But when they [zēteō](../../strongs/g/g2212.md) to [krateō](../../strongs/g/g2902.md) on him, they [phobeō](../../strongs/g/g5399.md) the [ochlos](../../strongs/g/g3793.md), because they [echō](../../strongs/g/g2192.md) him for a [prophētēs](../../strongs/g/g4396.md).

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 20](matthew_20.md) - [Matthew 22](matthew_22.md)

---

[^1]: [Matthew 21:23 Commentary](../../commentary/matthew/matthew_21_commentary.md#matthew_21_23)

[^2]: [Matthew 21:31 Commentary](../../commentary/matthew/matthew_21_commentary.md#matthew_21_31)

[^3]: [Matthew 21:44 Commentary](../../commentary/matthew/matthew_21_commentary.md#matthew_21_44)
