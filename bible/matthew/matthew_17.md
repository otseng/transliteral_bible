# [Matthew 17]()

<a name="matthew_17_1"></a>Matthew 17:1

And after six [hēmera](../../strongs/g/g2250.md) [Iēsous](../../strongs/g/g2424.md) [paralambanō](../../strongs/g/g3880.md) [Petros](../../strongs/g/g4074.md), [Iakōbos](../../strongs/g/g2385.md), and [Iōannēs](../../strongs/g/g2491.md) his [adelphos](../../strongs/g/g80.md), and [anapherō](../../strongs/g/g399.md) them into an [hypsēlos](../../strongs/g/g5308.md) [oros](../../strongs/g/g3735.md) apart,

<a name="matthew_17_2"></a>Matthew 17:2

And was [metamorphoō](../../strongs/g/g3339.md) before them: and his [prosōpon](../../strongs/g/g4383.md) did [lampō](../../strongs/g/g2989.md) as the [hēlios](../../strongs/g/g2246.md), and his [himation](../../strongs/g/g2440.md) was [leukos](../../strongs/g/g3022.md) as the [phōs](../../strongs/g/g5457.md).

<a name="matthew_17_3"></a>Matthew 17:3

And, [idou](../../strongs/g/g2400.md), there [optanomai](../../strongs/g/g3700.md) unto them [Mōÿsēs](../../strongs/g/g3475.md) and [Ēlias](../../strongs/g/g2243.md) [syllaleō](../../strongs/g/g4814.md) with him.

<a name="matthew_17_4"></a>Matthew 17:4

Then [apokrinomai](../../strongs/g/g611.md) [Petros](../../strongs/g/g4074.md), and [eipon](../../strongs/g/g2036.md) unto [Iēsous](../../strongs/g/g2424.md), [kyrios](../../strongs/g/g2962.md), it is [kalos](../../strongs/g/g2570.md) for us to be here: if thou wilt, let us [poieō](../../strongs/g/g4160.md) here three [skēnē](../../strongs/g/g4633.md); one for thee, and one for [Mōÿsēs](../../strongs/g/g3475.md), and one for [Ēlias](../../strongs/g/g2243.md).

<a name="matthew_17_5"></a>Matthew 17:5

While he yet [laleō](../../strongs/g/g2980.md), [idou](../../strongs/g/g2400.md), a [phōteinos](../../strongs/g/g5460.md) [nephelē](../../strongs/g/g3507.md) [episkiazō](../../strongs/g/g1982.md) them: and [idou](../../strongs/g/g2400.md) a [phōnē](../../strongs/g/g5456.md) out of the [nephelē](../../strongs/g/g3507.md), which [legō](../../strongs/g/g3004.md), This is my [agapētos](../../strongs/g/g27.md) [huios](../../strongs/g/g5207.md), in whom I am [eudokeō](../../strongs/g/g2106.md); [akouō](../../strongs/g/g191.md) ye him.

<a name="matthew_17_6"></a>Matthew 17:6

And when the [mathētēs](../../strongs/g/g3101.md) [akouō](../../strongs/g/g191.md), they [piptō](../../strongs/g/g4098.md) on their [prosōpon](../../strongs/g/g4383.md), and were [sphodra](../../strongs/g/g4970.md) [phobeō](../../strongs/g/g5399.md).

<a name="matthew_17_7"></a>Matthew 17:7

And [Iēsous](../../strongs/g/g2424.md) [proserchomai](../../strongs/g/g4334.md) and [haptomai](../../strongs/g/g680.md) them, and [eipon](../../strongs/g/g2036.md), **[egeirō](../../strongs/g/g1453.md), and be not [phobeō](../../strongs/g/g5399.md).**

<a name="matthew_17_8"></a>Matthew 17:8

And when they had [epairō](../../strongs/g/g1869.md) their [ophthalmos](../../strongs/g/g3788.md), they [eidō](../../strongs/g/g1492.md) [oudeis](../../strongs/g/g3762.md), save [Iēsous](../../strongs/g/g2424.md) only.

<a name="matthew_17_9"></a>Matthew 17:9

And as they [katabainō](../../strongs/g/g2597.md) from the [oros](../../strongs/g/g3735.md), [Iēsous](../../strongs/g/g2424.md) [entellō](../../strongs/g/g1781.md) them, [legō](../../strongs/g/g3004.md), **[eipon](../../strongs/g/g2036.md) the [horama](../../strongs/g/g3705.md) to [mēdeis](../../strongs/g/g3367.md), until the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) be [anistēmi](../../strongs/g/g450.md) from the [nekros](../../strongs/g/g3498.md).**

<a name="matthew_17_10"></a>Matthew 17:10

And his [mathētēs](../../strongs/g/g3101.md) [eperōtaō](../../strongs/g/g1905.md) him, [legō](../../strongs/g/g3004.md), Why then [legō](../../strongs/g/g3004.md) the [grammateus](../../strongs/g/g1122.md) that [Ēlias](../../strongs/g/g2243.md) must first [erchomai](../../strongs/g/g2064.md)?

<a name="matthew_17_11"></a>Matthew 17:11

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **[Ēlias](../../strongs/g/g2243.md) truly shall first [erchomai](../../strongs/g/g2064.md), and [apokathistēmi](../../strongs/g/g600.md) all things.**

<a name="matthew_17_12"></a>Matthew 17:12

**But I [legō](../../strongs/g/g3004.md) unto you, That [Ēlias](../../strongs/g/g2243.md) is [erchomai](../../strongs/g/g2064.md) already, and they [epiginōskō](../../strongs/g/g1921.md) him not, but have [poieō](../../strongs/g/g4160.md) unto him whatsoever they [thelō](../../strongs/g/g2309.md). Likewise shall also the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [paschō](../../strongs/g/g3958.md) of them.**

<a name="matthew_17_13"></a>Matthew 17:13

Then the [mathētēs](../../strongs/g/g3101.md) [syniēmi](../../strongs/g/g4920.md) that he [eipon](../../strongs/g/g2036.md) unto them of [Iōannēs](../../strongs/g/g2491.md) the [baptistēs](../../strongs/g/g910.md).

<a name="matthew_17_14"></a>Matthew 17:14

And when they were [erchomai](../../strongs/g/g2064.md) to the [ochlos](../../strongs/g/g3793.md), there [proserchomai](../../strongs/g/g4334.md) to him [anthrōpos](../../strongs/g/g444.md), [gonypeteō](../../strongs/g/g1120.md) to him, and [legō](../../strongs/g/g3004.md),

<a name="matthew_17_15"></a>Matthew 17:15

[kyrios](../../strongs/g/g2962.md), have [eleeō](../../strongs/g/g1653.md) on my [huios](../../strongs/g/g5207.md): for he [selēniazomai](../../strongs/g/g4583.md), and [kakōs](../../strongs/g/g2560.md) [paschō](../../strongs/g/g3958.md): for ofttimes he [piptō](../../strongs/g/g4098.md) into the [pyr](../../strongs/g/g4442.md), and oft into the [hydōr](../../strongs/g/g5204.md).

<a name="matthew_17_16"></a>Matthew 17:16

And I [prospherō](../../strongs/g/g4374.md) him to thy [mathētēs](../../strongs/g/g3101.md), and they could not [therapeuō](../../strongs/g/g2323.md) him.

<a name="matthew_17_17"></a>Matthew 17:17

Then [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), **[ō](../../strongs/g/g5599.md) [apistos](../../strongs/g/g571.md) and [diastrephō](../../strongs/g/g1294.md) [genea](../../strongs/g/g1074.md), how long shall I be with you? how long shall I [anechō](../../strongs/g/g430.md) you? [pherō](../../strongs/g/g5342.md) him hither to me.**

<a name="matthew_17_18"></a>Matthew 17:18

And [Iēsous](../../strongs/g/g2424.md) [epitimaō](../../strongs/g/g2008.md) the [daimonion](../../strongs/g/g1140.md); and he [exerchomai](../../strongs/g/g1831.md) out of him: and the [pais](../../strongs/g/g3816.md) was [therapeuō](../../strongs/g/g2323.md) from that very [hōra](../../strongs/g/g5610.md).

<a name="matthew_17_19"></a>Matthew 17:19

Then [proserchomai](../../strongs/g/g4334.md) the [mathētēs](../../strongs/g/g3101.md) to [Iēsous](../../strongs/g/g2424.md) apart, and [eipon](../../strongs/g/g2036.md), Why could not we [ekballō](../../strongs/g/g1544.md) him?

<a name="matthew_17_20"></a>Matthew 17:20

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **Because of your [apistia](../../strongs/g/g570.md): for [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, If ye have [pistis](../../strongs/g/g4102.md) as a [kokkos](../../strongs/g/g2848.md) of [sinapi](../../strongs/g/g4615.md), ye shall [eipon](../../strongs/g/g2046.md) unto this [oros](../../strongs/g/g3735.md), [metabainō](../../strongs/g/g3327.md) hence to [ekei](../../strongs/g/g1563.md); and it shall [metabainō](../../strongs/g/g3327.md); and [oudeis](../../strongs/g/g3762.md) shall [adynateō](../../strongs/g/g101.md) unto you.**

<a name="matthew_17_21"></a>Matthew 17:21

**Howbeit this [genos](../../strongs/g/g1085.md) [ekporeuomai](../../strongs/g/g1607.md) not but by [proseuchē](../../strongs/g/g4335.md) and [nēsteia](../../strongs/g/g3521.md).** [^1]

<a name="matthew_17_22"></a>Matthew 17:22

And while they [anastrephō](../../strongs/g/g390.md) in [Galilaia](../../strongs/g/g1056.md), [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **The [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) shall be [paradidōmi](../../strongs/g/g3860.md) into the [cheir](../../strongs/g/g5495.md) of [anthrōpos](../../strongs/g/g444.md):**

<a name="matthew_17_23"></a>Matthew 17:23

**And they shall [apokteinō](../../strongs/g/g615.md) him, and the third [hēmera](../../strongs/g/g2250.md) he shall be [egeirō](../../strongs/g/g1453.md).** And they were [sphodra](../../strongs/g/g4970.md) [lypeō](../../strongs/g/g3076.md).

<a name="matthew_17_24"></a>Matthew 17:24

And when they were [erchomai](../../strongs/g/g2064.md) to [Kapharnaoum](../../strongs/g/g2584.md), they that [lambanō](../../strongs/g/g2983.md) [didrachmon](../../strongs/g/g1323.md) [proserchomai](../../strongs/g/g4334.md) to [Petros](../../strongs/g/g4074.md), and [eipon](../../strongs/g/g2036.md), Doth not your [didaskalos](../../strongs/g/g1320.md) [teleō](../../strongs/g/g5055.md) [didrachmon](../../strongs/g/g1323.md?

<a name="matthew_17_25"></a>Matthew 17:25

He [legō](../../strongs/g/g3004.md), [nai](../../strongs/g/g3483.md). And when he was [eiserchomai](../../strongs/g/g1525.md) into the [oikia](../../strongs/g/g3614.md), [Iēsous](../../strongs/g/g2424.md) [prophthanō](../../strongs/g/g4399.md) him, saying, **What [dokeō](../../strongs/g/g1380.md) thou, [Simōn](../../strongs/g/g4613.md)? of whom do the [basileus](../../strongs/g/g935.md) of [gē](../../strongs/g/g1093.md) [lambanō](../../strongs/g/g2983.md) [telos](../../strongs/g/g5056.md) or [kēnsos](../../strongs/g/g2778.md)? of their own [huios](../../strongs/g/g5207.md), or of [allotrios](../../strongs/g/g245.md)?**

<a name="matthew_17_26"></a>Matthew 17:26

[Petros](../../strongs/g/g4074.md) [legō](../../strongs/g/g3004.md) unto him, Of [allotrios](../../strongs/g/g245.md). [Iēsous](../../strongs/g/g2424.md) [phēmi](../../strongs/g/g5346.md) unto him, **Then are the [huios](../../strongs/g/g5207.md) [eleutheros](../../strongs/g/g1658.md).**

<a name="matthew_17_27"></a>Matthew 17:27

**Notwithstanding, lest we should [skandalizō](../../strongs/g/g4624.md) them, [poreuō](../../strongs/g/g4198.md) thou to the [thalassa](../../strongs/g/g2281.md), and [ballō](../../strongs/g/g906.md) an [agkistron](../../strongs/g/g44.md), and [airō](../../strongs/g/g142.md) the [ichthys](../../strongs/g/g2486.md) that first [anabainō](../../strongs/g/g305.md); and when thou hast [anoigō](../../strongs/g/g455.md) his [stoma](../../strongs/g/g4750.md), thou shalt [heuriskō](../../strongs/g/g2147.md) a [statēr](../../strongs/g/g4715.md): that [lambanō](../../strongs/g/g2983.md), and [didōmi](../../strongs/g/g1325.md) unto them for me and thee.**

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 16](matthew_16.md) - [Matthew 18](matthew_18.md)

---

[^1]: [Matthew 17:21 Commentary](../../commentary/matthew/matthew_17_commentary.md#matthew_17_21)
