# [Matthew 15](https://www.blueletterbible.org/kjv/mat/15/1/rl1/s_944001)

<a name="matthew_15_1"></a>Matthew 15:1

Then [proserchomai](../../strongs/g/g4334.md) to [Iēsous](../../strongs/g/g2424.md) [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md), which were of [Hierosolyma](../../strongs/g/g2414.md), [legō](../../strongs/g/g3004.md),

<a name="matthew_15_2"></a>Matthew 15:2

Why do thy [mathētēs](../../strongs/g/g3101.md) [parabainō](../../strongs/g/g3845.md) the [paradosis](../../strongs/g/g3862.md) of the [presbyteros](../../strongs/g/g4245.md)? for they [niptō](../../strongs/g/g3538.md) not their [cheir](../../strongs/g/g5495.md) when they [esthiō](../../strongs/g/g2068.md) [artos](../../strongs/g/g740.md).

<a name="matthew_15_3"></a>Matthew 15:3

But he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **Why do ye also [parabainō](../../strongs/g/g3845.md) the [entolē](../../strongs/g/g1785.md) of [theos](../../strongs/g/g2316.md) by your [paradosis](../../strongs/g/g3862.md)?**

<a name="matthew_15_4"></a>Matthew 15:4

**For [theos](../../strongs/g/g2316.md) [entellō](../../strongs/g/g1781.md), [legō](../../strongs/g/g3004.md), [timaō](../../strongs/g/g5091.md) thy [patēr](../../strongs/g/g3962.md) and [mētēr](../../strongs/g/g3384.md): and, He that [kakologeō](../../strongs/g/g2551.md) [patēr](../../strongs/g/g3962.md) or [mētēr](../../strongs/g/g3384.md), let him [teleutaō](../../strongs/g/g5053.md) the [thanatos](../../strongs/g/g2288.md).**

<a name="matthew_15_5"></a>Matthew 15:5

**But ye [legō](../../strongs/g/g3004.md), Whosoever shall [eipon](../../strongs/g/g2036.md) to his [patēr](../../strongs/g/g3962.md) or his [mētēr](../../strongs/g/g3384.md), [dōron](../../strongs/g/g1435.md), by whatsoever thou mightest be [ōpheleō](../../strongs/g/g5623.md) by me;**

<a name="matthew_15_6"></a>Matthew 15:6

**And [timaō](../../strongs/g/g5091.md) not his [patēr](../../strongs/g/g3962.md) or his [mētēr](../../strongs/g/g3384.md). Thus have ye made the [entolē](../../strongs/g/g1785.md) of [theos](../../strongs/g/g2316.md) [akyroō](../../strongs/g/g208.md) by your [paradosis](../../strongs/g/g3862.md).** [^1]

<a name="matthew_15_7"></a>Matthew 15:7

**Ye [hypokritēs](../../strongs/g/g5273.md), [kalōs](../../strongs/g/g2573.md) did [Ēsaïas](../../strongs/g/g2268.md) [prophēteuō](../../strongs/g/g4395.md) of you, [legō](../../strongs/g/g3004.md),**

<a name="matthew_15_8"></a>Matthew 15:8

**This [laos](../../strongs/g/g2992.md) [eggizō](../../strongs/g/g1448.md) unto me with their [stoma](../../strongs/g/g4750.md), and [timaō](../../strongs/g/g5091.md) me with [cheilos](../../strongs/g/g5491.md); but their [kardia](../../strongs/g/g2588.md) is [porrō](../../strongs/g/g4206.md) from me.**

<a name="matthew_15_9"></a>Matthew 15:9

**But in [matēn](../../strongs/g/g3155.md) they do [sebō](../../strongs/g/g4576.md) me, [didaskō](../../strongs/g/g1321.md) [didaskalia](../../strongs/g/g1319.md) the [entalma](../../strongs/g/g1778.md) of [anthrōpos](../../strongs/g/g444.md).**

<a name="matthew_15_10"></a>Matthew 15:10

And he [proskaleō](../../strongs/g/g4341.md) the [ochlos](../../strongs/g/g3793.md), and [eipon](../../strongs/g/g2036.md) unto them, **[akouō](../../strongs/g/g191.md), and [syniēmi](../../strongs/g/g4920.md):**

<a name="matthew_15_11"></a>Matthew 15:11

**Not that which [eiserchomai](../../strongs/g/g1525.md) into the [stoma](../../strongs/g/g4750.md) [koinoō](../../strongs/g/g2840.md) an [anthrōpos](../../strongs/g/g444.md); but that which [ekporeuomai](../../strongs/g/g1607.md) of the [stoma](../../strongs/g/g4750.md), this [koinoō](../../strongs/g/g2840.md) an [anthrōpos](../../strongs/g/g444.md).**

<a name="matthew_15_12"></a>Matthew 15:12

Then [proserchomai](../../strongs/g/g4334.md) his [mathētēs](../../strongs/g/g3101.md), and [eipon](../../strongs/g/g2036.md) unto him, [eidō](../../strongs/g/g1492.md) that the [Pharisaios](../../strongs/g/g5330.md) were [skandalizō](../../strongs/g/g4624.md), after they [akouō](../../strongs/g/g191.md) this [logos](../../strongs/g/g3056.md)?

<a name="matthew_15_13"></a>Matthew 15:13

But he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), **Every [phyteia](../../strongs/g/g5451.md), which my [ouranios](../../strongs/g/g3770.md) [patēr](../../strongs/g/g3962.md) hath not [phyteuō](../../strongs/g/g5452.md), shall be [ekrizoō](../../strongs/g/g1610.md).**

<a name="matthew_15_14"></a>Matthew 15:14

**[aphiēmi](../../strongs/g/g863.md) them: they be [typhlos](../../strongs/g/g5185.md) [hodēgos](../../strongs/g/g3595.md) of the [typhlos](../../strongs/g/g5185.md). And if the [typhlos](../../strongs/g/g5185.md) [hodēgeō](../../strongs/g/g3594.md) the [typhlos](../../strongs/g/g5185.md), both shall [piptō](../../strongs/g/g4098.md) into the [bothynos](../../strongs/g/g999.md).**

<a name="matthew_15_15"></a>Matthew 15:15

Then [apokrinomai](../../strongs/g/g611.md) [Petros](../../strongs/g/g4074.md) and [eipon](../../strongs/g/g2036.md) unto him, [phrazō](../../strongs/g/g5419.md) unto us this [parabolē](../../strongs/g/g3850.md). [^2]

<a name="matthew_15_16"></a>Matthew 15:16

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md), **Are ye also yet [asynetos](../../strongs/g/g801.md)?**

<a name="matthew_15_17"></a>Matthew 15:17

**Do not ye yet [noeō](../../strongs/g/g3539.md), that whatsoever [eisporeuomai](../../strongs/g/g1531.md) at the [stoma](../../strongs/g/g4750.md) [chōreō](../../strongs/g/g5562.md) into the [koilia](../../strongs/g/g2836.md), and is [ekballō](../../strongs/g/g1544.md) into the [aphedrōn](../../strongs/g/g856.md)?**

<a name="matthew_15_18"></a>Matthew 15:18

**But those things which [ekporeuomai](../../strongs/g/g1607.md) of the [stoma](../../strongs/g/g4750.md) [exerchomai](../../strongs/g/g1831.md) from the [kardia](../../strongs/g/g2588.md); and they [koinoō](../../strongs/g/g2840.md) the [anthrōpos](../../strongs/g/g444.md).**

<a name="matthew_15_19"></a>Matthew 15:19

**For out of the [kardia](../../strongs/g/g2588.md) [exerchomai](../../strongs/g/g1831.md) [ponēros](../../strongs/g/g4190.md) [dialogismos](../../strongs/g/g1261.md), [phonos](../../strongs/g/g5408.md), [moicheia](../../strongs/g/g3430.md), [porneia](../../strongs/g/g4202.md), [klopē](../../strongs/g/g2829.md), [pseudomartyria](../../strongs/g/g5577.md), [blasphēmia](../../strongs/g/g988.md):**

<a name="matthew_15_20"></a>Matthew 15:20

**These are which [koinoō](../../strongs/g/g2840.md) an [anthrōpos](../../strongs/g/g444.md): but to [phago](../../strongs/g/g5315.md) with [aniptos](../../strongs/g/g449.md) [cheir](../../strongs/g/g5495.md) [koinoō](../../strongs/g/g2840.md) not an [anthrōpos](../../strongs/g/g444.md).**

<a name="matthew_15_21"></a>Matthew 15:21

Then [Iēsous](../../strongs/g/g2424.md) [exerchomai](../../strongs/g/g1831.md) thence, and [anachōreō](../../strongs/g/g402.md) into the [meros](../../strongs/g/g3313.md) of [Tyros](../../strongs/g/g5184.md) and [Sidōn](../../strongs/g/g4605.md).

<a name="matthew_15_22"></a>Matthew 15:22

And, [idou](../../strongs/g/g2400.md), a [gynē](../../strongs/g/g1135.md) of [Chananaios](../../strongs/g/g5478.md) [exerchomai](../../strongs/g/g1831.md) of the same [horion](../../strongs/g/g3725.md), and [kraugazō](../../strongs/g/g2905.md) unto him, [legō](../../strongs/g/g3004.md), Have [eleeō](../../strongs/g/g1653.md) on me, [kyrios](../../strongs/g/g2962.md), [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md); my [thygatēr](../../strongs/g/g2364.md) is [kakōs](../../strongs/g/g2560.md) [daimonizomai](../../strongs/g/g1139.md).

<a name="matthew_15_23"></a>Matthew 15:23

But he [apokrinomai](../../strongs/g/g611.md) her not a [logos](../../strongs/g/g3056.md). And his [mathētēs](../../strongs/g/g3101.md) [proserchomai](../../strongs/g/g4334.md) and [erōtaō](../../strongs/g/g2065.md) him, [legō](../../strongs/g/g3004.md), [apolyō](../../strongs/g/g630.md) her; for she [krazō](../../strongs/g/g2896.md) after us.

<a name="matthew_15_24"></a>Matthew 15:24

But he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), **I am not [apostellō](../../strongs/g/g649.md) but unto the [apollymi](../../strongs/g/g622.md) [probaton](../../strongs/g/g4263.md) of the [oikos](../../strongs/g/g3624.md) of [Israēl](../../strongs/g/g2474.md).**

<a name="matthew_15_25"></a>Matthew 15:25

Then she [erchomai](../../strongs/g/g2064.md) and [proskyneō](../../strongs/g/g4352.md) him, [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), [boētheō](../../strongs/g/g997.md) me.

<a name="matthew_15_26"></a>Matthew 15:26

But he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), **It is not [kalos](../../strongs/g/g2570.md) to [lambanō](../../strongs/g/g2983.md) the [teknon](../../strongs/g/g5043.md) [artos](../../strongs/g/g740.md), and to [ballō](../../strongs/g/g906.md) to [kynarion](../../strongs/g/g2952.md).**

<a name="matthew_15_27"></a>Matthew 15:27

And she [eipon](../../strongs/g/g2036.md), [nai](../../strongs/g/g3483.md), [kyrios](../../strongs/g/g2962.md): yet the [kynarion](../../strongs/g/g2952.md) [esthiō](../../strongs/g/g2068.md) of the [psichion](../../strongs/g/g5589.md) which [piptō](../../strongs/g/g4098.md) from their [kyrios](../../strongs/g/g2962.md) [trapeza](../../strongs/g/g5132.md).

<a name="matthew_15_28"></a>Matthew 15:28

Then [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto her, **[ō](../../strongs/g/g5599.md) [gynē](../../strongs/g/g1135.md), [megas](../../strongs/g/g3173.md) thy [pistis](../../strongs/g/g4102.md): be it unto thee even as thou wilt.** And her [thygatēr](../../strongs/g/g2364.md) was [iaomai](../../strongs/g/g2390.md) from that very [hōra](../../strongs/g/g5610.md).

<a name="matthew_15_29"></a>Matthew 15:29

And [Iēsous](../../strongs/g/g2424.md) [metabainō](../../strongs/g/g3327.md) from thence, and [erchomai](../../strongs/g/g2064.md) nigh unto the [thalassa](../../strongs/g/g2281.md) of [Galilaia](../../strongs/g/g1056.md); and [anabainō](../../strongs/g/g305.md) into a [oros](../../strongs/g/g3735.md), and [kathēmai](../../strongs/g/g2521.md) there.

<a name="matthew_15_30"></a>Matthew 15:30

And [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) [proserchomai](../../strongs/g/g4334.md) unto him, having with them [chōlos](../../strongs/g/g5560.md), [typhlos](../../strongs/g/g5185.md), [kōphos](../../strongs/g/g2974.md), [kyllos](../../strongs/g/g2948.md), and many others, and [rhiptō](../../strongs/g/g4496.md) them at [Iēsous](../../strongs/g/g2424.md) [pous](../../strongs/g/g4228.md); and he [therapeuō](../../strongs/g/g2323.md) them:

<a name="matthew_15_31"></a>Matthew 15:31

Insomuch that the [ochlos](../../strongs/g/g3793.md) [thaumazō](../../strongs/g/g2296.md), when they [blepō](../../strongs/g/g991.md) the [kōphos](../../strongs/g/g2974.md) to [laleō](../../strongs/g/g2980.md), the [kyllos](../../strongs/g/g2948.md) to be [hygiēs](../../strongs/g/g5199.md), the [chōlos](../../strongs/g/g5560.md) to [peripateō](../../strongs/g/g4043.md), and the [typhlos](../../strongs/g/g5185.md) to [blepō](../../strongs/g/g991.md): and they [doxazō](../../strongs/g/g1392.md) the [theos](../../strongs/g/g2316.md) of [Israēl](../../strongs/g/g2474.md). [^3]

<a name="matthew_15_32"></a>Matthew 15:32

Then [Iēsous](../../strongs/g/g2424.md) [proskaleō](../../strongs/g/g4341.md) his [mathētēs](../../strongs/g/g3101.md), and [eipon](../../strongs/g/g2036.md), **I have [splagchnizomai](../../strongs/g/g4697.md) on the [ochlos](../../strongs/g/g3793.md), because they [prosmenō](../../strongs/g/g4357.md) with me now three [hēmera](../../strongs/g/g2250.md), and have nothing to [phago](../../strongs/g/g5315.md): and I will not [apolyō](../../strongs/g/g630.md) them [nēstis](../../strongs/g/g3523.md), lest they [eklyō](../../strongs/g/g1590.md) in the [hodos](../../strongs/g/g3598.md).**

<a name="matthew_15_33"></a>Matthew 15:33

And his [mathētēs](../../strongs/g/g3101.md) [legō](../../strongs/g/g3004.md) unto him, Whence should we have so much [artos](../../strongs/g/g740.md) in the [erēmia](../../strongs/g/g2047.md), as to [chortazō](../../strongs/g/g5526.md) [tosoutos](../../strongs/g/g5118.md) an [ochlos](../../strongs/g/g3793.md)?

<a name="matthew_15_34"></a>Matthew 15:34

And [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, **How many [artos](../../strongs/g/g740.md) have ye?** And they [eipon](../../strongs/g/g2036.md), Seven, and an [oligos](../../strongs/g/g3641.md) [ichthydion](../../strongs/g/g2485.md).

<a name="matthew_15_35"></a>Matthew 15:35

And he [keleuō](../../strongs/g/g2753.md) the [ochlos](../../strongs/g/g3793.md) to [anapiptō](../../strongs/g/g377.md) on the [gē](../../strongs/g/g1093.md).

<a name="matthew_15_36"></a>Matthew 15:36

And he [lambanō](../../strongs/g/g2983.md) the seven [artos](../../strongs/g/g740.md) and the [ichthys](../../strongs/g/g2486.md), and [eucharisteō](../../strongs/g/g2168.md), and [klaō](../../strongs/g/g2806.md) them, and [didōmi](../../strongs/g/g1325.md) to his [mathētēs](../../strongs/g/g3101.md), and the [mathētēs](../../strongs/g/g3101.md) to the [ochlos](../../strongs/g/g3793.md).

<a name="matthew_15_37"></a>Matthew 15:37

And they did all [phago](../../strongs/g/g5315.md), and were [chortazō](../../strongs/g/g5526.md): and they [airō](../../strongs/g/g142.md) of the [klasma](../../strongs/g/g2801.md) [perisseuō](../../strongs/g/g4052.md) seven [spyris](../../strongs/g/g4711.md) [plērēs](../../strongs/g/g4134.md).

<a name="matthew_15_38"></a>Matthew 15:38

And they that did [esthiō](../../strongs/g/g2068.md) were four thousand [anēr](../../strongs/g/g435.md), beside [gynē](../../strongs/g/g1135.md) and [paidion](../../strongs/g/g3813.md).

<a name="matthew_15_39"></a>Matthew 15:39

And he [apolyō](../../strongs/g/g630.md) the [ochlos](../../strongs/g/g3793.md), and [embainō](../../strongs/g/g1684.md) [ploion](../../strongs/g/g4143.md), and [erchomai](../../strongs/g/g2064.md) into the [horion](../../strongs/g/g3725.md) of [Magadan](../../strongs/g/g3093.md).

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 14](matthew_14.md) - [Matthew 16](matthew_16.md)
---


[^1]: [Matthew 15:6 Commentary](../../commentary/matthew/matthew_15_commentary.md#matthew_15_6)

[^2]: [Matthew 15:15 Commentary](../../commentary/matthew/matthew_15_commentary.md#matthew_15_15)

[^3]: [Matthew 15:31 Commentary](../../commentary/matthew/matthew_15_commentary.md#matthew_15_31)
