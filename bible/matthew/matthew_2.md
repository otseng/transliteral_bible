# [Matthew 2](https://www.blueletterbible.org/kjv/mat/2/1/rl1/s_930001)

<a name="matthew_2_1"></a>Matthew 2:1

Now when [Iēsous](../../strongs/g/g2424.md) was [gennaō](../../strongs/g/g1080.md) in [Bēthleem](../../strongs/g/g965.md) of [Ioudaia](../../strongs/g/g2449.md) in the [hēmera](../../strongs/g/g2250.md) of [Hērōdēs](../../strongs/g/g2264.md) the [basileus](../../strongs/g/g935.md), [idou](../../strongs/g/g2400.md), there [paraginomai](../../strongs/g/g3854.md) [magos](../../strongs/g/g3097.md) from the [anatolē](../../strongs/g/g395.md) to [Hierosolyma](../../strongs/g/g2414.md),

<a name="matthew_2_2"></a>Matthew 2:2

[legō](../../strongs/g/g3004.md), Where is [tiktō](../../strongs/g/g5088.md) [basileus](../../strongs/g/g935.md) of the [Ioudaios](../../strongs/g/g2453.md)? for we have [eidō](../../strongs/g/g1492.md) his [astēr](../../strongs/g/g792.md) in the [anatolē](../../strongs/g/g395.md), and are [erchomai](../../strongs/g/g2064.md) to [proskyneō](../../strongs/g/g4352.md) him.

<a name="matthew_2_3"></a>Matthew 2:3

When [Hērōdēs](../../strongs/g/g2264.md) the [basileus](../../strongs/g/g935.md) had [akouō](../../strongs/g/g191.md), he was [tarassō](../../strongs/g/g5015.md), and all [Hierosolyma](../../strongs/g/g2414.md) with him. [^1]

<a name="matthew_2_4"></a>Matthew 2:4

And when he had [synagō](../../strongs/g/g4863.md) all the [archiereus](../../strongs/g/g749.md) and [grammateus](../../strongs/g/g1122.md) of the [laos](../../strongs/g/g2992.md), he [pynthanomai](../../strongs/g/g4441.md) of them where [Christos](../../strongs/g/g5547.md) should be [gennaō](../../strongs/g/g1080.md). [^2]

<a name="matthew_2_5"></a>Matthew 2:5

And they [eipon](../../strongs/g/g2036.md) unto him, In [Bēthleem](../../strongs/g/g965.md) of [Ioudaia](../../strongs/g/g2449.md): for thus it is [graphō](../../strongs/g/g1125.md) by the [prophētēs](../../strongs/g/g4396.md), [^3]

<a name="matthew_2_6"></a>Matthew 2:6

And thou [Bēthleem](../../strongs/g/g965.md), the [gē](../../strongs/g/g1093.md) of [Iouda](../../strongs/g/g2448.md), art [oudamōs](../../strongs/g/g3760.md) the [elachistos](../../strongs/g/g1646.md) among the [hēgemōn](../../strongs/g/g2232.md) of [Iouda](../../strongs/g/g2448.md): for out of thee shall [exerchomai](../../strongs/g/g1831.md) a [hēgeomai](../../strongs/g/g2233.md), that shall [poimainō](../../strongs/g/g4165.md) my [laos](../../strongs/g/g2992.md) [Israēl](../../strongs/g/g2474.md).

<a name="matthew_2_7"></a>Matthew 2:7

Then [Hērōdēs](../../strongs/g/g2264.md), [lathra](../../strongs/g/g2977.md) [kaleō](../../strongs/g/g2564.md) the [magos](../../strongs/g/g3097.md), [akriboō](../../strongs/g/g198.md) of them what [chronos](../../strongs/g/g5550.md) the [astēr](../../strongs/g/g792.md) [phainō](../../strongs/g/g5316.md).

<a name="matthew_2_8"></a>Matthew 2:8

And he [pempō](../../strongs/g/g3992.md) them to [Bēthleem](../../strongs/g/g965.md), and [eipon](../../strongs/g/g2036.md), [poreuō](../../strongs/g/g4198.md) and [exetazō](../../strongs/g/g1833.md) [akribōs](../../strongs/g/g199.md) for [paidion](../../strongs/g/g3813.md); and when ye have [heuriskō](../../strongs/g/g2147.md), [apaggellō](../../strongs/g/g518.md) me, that I may [erchomai](../../strongs/g/g2064.md) and [proskyneō](../../strongs/g/g4352.md) him also.

<a name="matthew_2_9"></a>Matthew 2:9

When they had [akouō](../../strongs/g/g191.md) the [basileus](../../strongs/g/g935.md), they [poreuō](../../strongs/g/g4198.md); and, [idou](../../strongs/g/g2400.md), the [astēr](../../strongs/g/g792.md), which they [eidō](../../strongs/g/g1492.md) in the [anatolē](../../strongs/g/g395.md), [proagō](../../strongs/g/g4254.md) them, till it [erchomai](../../strongs/g/g2064.md) and [histēmi](../../strongs/g/g2476.md) over where the [paidion](../../strongs/g/g3813.md) was. [^4]

<a name="matthew_2_10"></a>Matthew 2:10

When they [eidō](../../strongs/g/g1492.md) the [astēr](../../strongs/g/g792.md), they [chairō](../../strongs/g/g5463.md) with [sphodra](../../strongs/g/g4970.md) [megas](../../strongs/g/g3173.md) [chara](../../strongs/g/g5479.md).

<a name="matthew_2_11"></a>Matthew 2:11

And when they were [erchomai](../../strongs/g/g2064.md) into the [oikia](../../strongs/g/g3614.md), they [heuriskō](../../strongs/g/g2147.md) [eidō](../../strongs/g/g1492.md) the [paidion](../../strongs/g/g3813.md) with [Maria](../../strongs/g/g3137.md) his [mētēr](../../strongs/g/g3384.md), and [piptō](../../strongs/g/g4098.md), and [proskyneō](../../strongs/g/g4352.md) him: and when they had [anoigō](../../strongs/g/g455.md) their [thēsauros](../../strongs/g/g2344.md), they [prospherō](../../strongs/g/g4374.md) unto him [dōron](../../strongs/g/g1435.md); [chrysos](../../strongs/g/g5557.md), and [libanos](../../strongs/g/g3030.md) and [smyrna](../../strongs/g/g4666.md). [^5]

<a name="matthew_2_12"></a>Matthew 2:12

And [chrēmatizō](../../strongs/g/g5537.md) in an [onar](../../strongs/g/g3677.md) that they should not [anakamptō](../../strongs/g/g344.md) to [Hērōdēs](../../strongs/g/g2264.md), they [anachōreō](../../strongs/g/g402.md) into their own [chōra](../../strongs/g/g5561.md) another [hodos](../../strongs/g/g3598.md).

<a name="matthew_2_13"></a>Matthew 2:13

And when they were [anachōreō](../../strongs/g/g402.md), [idou](../../strongs/g/g2400.md), the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) [phainō](../../strongs/g/g5316.md) to [Iōsēph](../../strongs/g/g2501.md) in an [onar](../../strongs/g/g3677.md), [legō](../../strongs/g/g3004.md), [egeirō](../../strongs/g/g1453.md), and [paralambanō](../../strongs/g/g3880.md) the [paidion](../../strongs/g/g3813.md) and his [mētēr](../../strongs/g/g3384.md), and [pheugō](../../strongs/g/g5343.md) into [Aigyptos](../../strongs/g/g125.md), and be thou there until I [eipon](../../strongs/g/g2036.md) thee: for [Hērōdēs](../../strongs/g/g2264.md) will [zēteō](../../strongs/g/g2212.md) the [paidion](../../strongs/g/g3813.md) to [apollymi](../../strongs/g/g622.md) him.

<a name="matthew_2_14"></a>Matthew 2:14

When he [egeirō](../../strongs/g/g1453.md), he [paralambanō](../../strongs/g/g3880.md) the [paidion](../../strongs/g/g3813.md) and his [mētēr](../../strongs/g/g3384.md) by [nyx](../../strongs/g/g3571.md), and [anachōreō](../../strongs/g/g402.md) into [Aigyptos](../../strongs/g/g125.md):

<a name="matthew_2_15"></a>Matthew 2:15

And was there until the [teleutē](../../strongs/g/g5054.md) of [Hērōdēs](../../strongs/g/g2264.md): that it might be [plēroō](../../strongs/g/g4137.md) which was [rheō](../../strongs/g/g4483.md) of the [kyrios](../../strongs/g/g2962.md) by the [prophētēs](../../strongs/g/g4396.md), [legō](../../strongs/g/g3004.md), Out of [Aigyptos](../../strongs/g/g125.md) have I [kaleō](../../strongs/g/g2564.md) my [huios](../../strongs/g/g5207.md). [^6]

<a name="matthew_2_16"></a>Matthew 2:16

Then [Hērōdēs](../../strongs/g/g2264.md), when he [eidō](../../strongs/g/g1492.md) that he was [empaizō](../../strongs/g/g1702.md) of the [magos](../../strongs/g/g3097.md), was [lian](../../strongs/g/g3029.md) [thymoō](../../strongs/g/g2373.md), and [apostellō](../../strongs/g/g649.md), and [anaireō](../../strongs/g/g337.md) all the [pais](../../strongs/g/g3816.md) that were in [Bēthleem](../../strongs/g/g965.md), and in all the [horion](../../strongs/g/g3725.md) thereof, from two years old and under, according to the [chronos](../../strongs/g/g5550.md) which he had [akriboō](../../strongs/g/g198.md) of the [magos](../../strongs/g/g3097.md).

<a name="matthew_2_17"></a>Matthew 2:17

Then was [plēroō](../../strongs/g/g4137.md) that which was [rheō](../../strongs/g/g4483.md) by [Ieremias](../../strongs/g/g2408.md) the [prophētēs](../../strongs/g/g4396.md), [legō](../../strongs/g/g3004.md), [^7]

<a name="matthew_2_18"></a>Matthew 2:18

In [Rhama](../../strongs/g/g4471.md) was there a [phōnē](../../strongs/g/g5456.md) [akouō](../../strongs/g/g191.md), [thrēnos](../../strongs/g/g2355.md), and [klauthmos](../../strongs/g/g2805.md), and [polys](../../strongs/g/g4183.md) [odyrmos](../../strongs/g/g3602.md), [Rhachēl](../../strongs/g/g4478.md) [klaiō](../../strongs/g/g2799.md) her [teknon](../../strongs/g/g5043.md), and would not [parakaleō](../../strongs/g/g3870.md), because they are not. [^8]

<a name="matthew_2_19"></a>Matthew 2:19

But when [Hērōdēs](../../strongs/g/g2264.md) was [teleutaō](../../strongs/g/g5053.md), [idou](../../strongs/g/g2400.md), an [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) [phainō](../../strongs/g/g5316.md) in a [onar](../../strongs/g/g3677.md) to [Iōsēph](../../strongs/g/g2501.md) in [Aigyptos](../../strongs/g/g125.md),

<a name="matthew_2_20"></a>Matthew 2:20

[Legō](../../strongs/g/g3004.md), [egeirō](../../strongs/g/g1453.md), and [paralambanō](../../strongs/g/g3880.md) the [paidion](../../strongs/g/g3813.md) and his [mētēr](../../strongs/g/g3384.md), and [poreuō](../../strongs/g/g4198.md) into the [gē](../../strongs/g/g1093.md) of [Israēl](../../strongs/g/g2474.md): for they [thnēskō](../../strongs/g/g2348.md) which [zēteō](../../strongs/g/g2212.md) the [paidion](../../strongs/g/g3813.md) [psychē](../../strongs/g/g5590.md).

<a name="matthew_2_21"></a>Matthew 2:21

And he [egeirō](../../strongs/g/g1453.md), and [paralambanō](../../strongs/g/g3880.md) the [paidion](../../strongs/g/g3813.md) and his [mētēr](../../strongs/g/g3384.md), and [erchomai](../../strongs/g/g2064.md) into the [gē](../../strongs/g/g1093.md) of [Israēl](../../strongs/g/g2474.md). [^9]

<a name="matthew_2_22"></a>Matthew 2:22

But when he [akouō](../../strongs/g/g191.md) that [Archelaos](../../strongs/g/g745.md) did [basileuō](../../strongs/g/g936.md) in [Ioudaia](../../strongs/g/g2449.md) in the room of his [patēr](../../strongs/g/g3962.md) [Hērōdēs](../../strongs/g/g2264.md), he was [phobeō](../../strongs/g/g5399.md) to [aperchomai](../../strongs/g/g565.md) thither: notwithstanding, being [chrēmatizō](../../strongs/g/g5537.md) in a [onar](../../strongs/g/g3677.md), he [anachōreō](../../strongs/g/g402.md) into the [meros](../../strongs/g/g3313.md) of [Galilaia](../../strongs/g/g1056.md):

<a name="matthew_2_23"></a>Matthew 2:23

And he [erchomai](../../strongs/g/g2064.md) and [katoikeō](../../strongs/g/g2730.md) in a [polis](../../strongs/g/g4172.md) [legō](../../strongs/g/g3004.md) [Nazara](../../strongs/g/g3478.md): that it might be [plēroō](../../strongs/g/g4137.md) which was [rheō](../../strongs/g/g4483.md) by the [prophētēs](../../strongs/g/g4396.md), He shall be [kaleō](../../strongs/g/g2564.md) a [Nazōraios](../../strongs/g/g3480.md).

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 1](matthew_1.md) - [Matthew 3](matthew_3.md)

---

[^1]: [Matthew 2:3 Commentary](../../commentary/matthew/matthew_2_commentary.md#matthew_2_3)

[^2]: [Matthew 2:4 Commentary](../../commentary/matthew/matthew_2_commentary.md#matthew_2_4)

[^3]: [Matthew 2:5 Commentary](../../commentary/matthew/matthew_2_commentary.md#matthew_2_5)

[^4]: [Matthew 2:9 Commentary](../../commentary/matthew/matthew_2_commentary.md#matthew_2_9)

[^5]: [Matthew 2:11 Commentary](../../commentary/matthew/matthew_2_commentary.md#matthew_2_11)

[^6]: [Matthew 2:15 Commentary](../../commentary/matthew/matthew_2_commentary.md#matthew_2_15)

[^7]: [Matthew 2:17 Commentary](../../commentary/matthew/matthew_2_commentary.md#matthew_2_17)

[^8]: [Matthew 2:18 Commentary](../../commentary/matthew/matthew_2_commentary.md#matthew_2_18)

[^9]: [Matthew 2:21 Commentary](../../commentary/matthew/matthew_2_commentary.md#matthew_2_21)
