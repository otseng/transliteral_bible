# [Matthew 18](https://www.blueletterbible.org/kjv/mat/18/1/rl1/s_947001)

<a name="matthew_18_1"></a>Matthew 18:1

At the same [hōra](../../strongs/g/g5610.md) [proserchomai](../../strongs/g/g4334.md) the [mathētēs](../../strongs/g/g3101.md) unto [Iēsous](../../strongs/g/g2424.md), [legō](../../strongs/g/g3004.md), Who is the [meizōn](../../strongs/g/g3187.md) in the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md)?

<a name="matthew_18_2"></a>Matthew 18:2

And [Iēsous](../../strongs/g/g2424.md) [proskaleō](../../strongs/g/g4341.md) a [paidion](../../strongs/g/g3813.md) unto him, and [histēmi](../../strongs/g/g2476.md) him in the [mesos](../../strongs/g/g3319.md) of them,

<a name="matthew_18_3"></a>Matthew 18:3

And [eipon](../../strongs/g/g2036.md), **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, Except ye be [strephō](../../strongs/g/g4762.md), and become as [paidion](../../strongs/g/g3813.md), ye shall not [eiserchomai](../../strongs/g/g1525.md) into the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_18_4"></a>Matthew 18:4

**Whosoever therefore shall [tapeinoō](../../strongs/g/g5013.md) himself as this [paidion](../../strongs/g/g3813.md), the same is [meizōn](../../strongs/g/g3187.md) in the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_18_5"></a>Matthew 18:5

**And whoso shall [dechomai](../../strongs/g/g1209.md) one such [paidion](../../strongs/g/g3813.md) in my [onoma](../../strongs/g/g3686.md) [dechomai](../../strongs/g/g1209.md) me.**

<a name="matthew_18_6"></a>Matthew 18:6

**But whoso shall [skandalizō](../../strongs/g/g4624.md) one of these [mikros](../../strongs/g/g3398.md) which [pisteuō](../../strongs/g/g4100.md) in me, it were [sympherō](../../strongs/g/g4851.md) for him that a [mylos](../../strongs/g/g3458.md) [onikos](../../strongs/g/g3684.md) were [kremannymi](../../strongs/g/g2910.md) about his [trachēlos](../../strongs/g/g5137.md), and he were [katapontizō](../../strongs/g/g2670.md) in the [pelagos](../../strongs/g/g3989.md) of the [thalassa](../../strongs/g/g2281.md).**

<a name="matthew_18_7"></a>Matthew 18:7

**[ouai](../../strongs/g/g3759.md) unto the [kosmos](../../strongs/g/g2889.md) because of [skandalon](../../strongs/g/g4625.md)! for it [anagkē](../../strongs/g/g318.md) be that [skandalon](../../strongs/g/g4625.md) [erchomai](../../strongs/g/g2064.md); but [ouai](../../strongs/g/g3759.md) to that [anthrōpos](../../strongs/g/g444.md) by whom the [skandalon](../../strongs/g/g4625.md) [erchomai](../../strongs/g/g2064.md)!**

<a name="matthew_18_8"></a>Matthew 18:8

**Wherefore if thy [cheir](../../strongs/g/g5495.md) or thy [pous](../../strongs/g/g4228.md) [skandalizō](../../strongs/g/g4624.md) thee, [ekkoptō](../../strongs/g/g1581.md) them, and [ballō](../../strongs/g/g906.md) from thee: it is [kalos](../../strongs/g/g2570.md) for thee to [eiserchomai](../../strongs/g/g1525.md) into [zōē](../../strongs/g/g2222.md) [chōlos](../../strongs/g/g5560.md) or [kyllos](../../strongs/g/g2948.md), rather than having two [cheir](../../strongs/g/g5495.md) or two [pous](../../strongs/g/g4228.md) to be [ballō](../../strongs/g/g906.md) into [aiōnios](../../strongs/g/g166.md) [pyr](../../strongs/g/g4442.md).**

<a name="matthew_18_9"></a>Matthew 18:9

**And if thine [ophthalmos](../../strongs/g/g3788.md) [skandalizō](../../strongs/g/g4624.md) thee, [exaireō](../../strongs/g/g1807.md) it, and [ballō](../../strongs/g/g906.md) from thee: it is [kalos](../../strongs/g/g2570.md) for thee to [eiserchomai](../../strongs/g/g1525.md) into [zōē](../../strongs/g/g2222.md) with [monophthalmos](../../strongs/g/g3442.md), rather than having two [ophthalmos](../../strongs/g/g3788.md) to be [ballō](../../strongs/g/g906.md) into [geenna](../../strongs/g/g1067.md) [pyr](../../strongs/g/g4442.md).**

<a name="matthew_18_10"></a>Matthew 18:10

**[horaō](../../strongs/g/g3708.md) that ye [kataphroneō](../../strongs/g/g2706.md) not one of these [mikros](../../strongs/g/g3398.md); for I [legō](../../strongs/g/g3004.md) unto you, That in [ouranos](../../strongs/g/g3772.md) their [aggelos](../../strongs/g/g32.md) do always [blepō](../../strongs/g/g991.md) the [prosōpon](../../strongs/g/g4383.md) of my [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_18_11"></a>Matthew 18:11

**For the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) is [erchomai](../../strongs/g/g2064.md) to [sōzō](../../strongs/g/g4982.md) that which was [apollymi](../../strongs/g/g622.md).** [^1]

<a name="matthew_18_12"></a>Matthew 18:12

**How [dokeō](../../strongs/g/g1380.md) ye? if an [anthrōpos](../../strongs/g/g444.md) have an hundred [probaton](../../strongs/g/g4263.md), and one of them [planaō](../../strongs/g/g4105.md), doth he not [aphiēmi](../../strongs/g/g863.md) the ninety and nine, and [poreuō](../../strongs/g/g4198.md) into the [oros](../../strongs/g/g3735.md), and [zēteō](../../strongs/g/g2212.md) that which [planaō](../../strongs/g/g4105.md)?**

<a name="matthew_18_13"></a>Matthew 18:13

**And if so be that he [heuriskō](../../strongs/g/g2147.md) it, [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, he [chairō](../../strongs/g/g5463.md) more of that, than of the ninety and nine which [planaō](../../strongs/g/g4105.md) not.**

<a name="matthew_18_14"></a>Matthew 18:14

**Even so it is not the [thelēma](../../strongs/g/g2307.md) of your [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md), that one of these [mikros](../../strongs/g/g3398.md) should [apollymi](../../strongs/g/g622.md).**

<a name="matthew_18_15"></a>Matthew 18:15

**Moreover if thy [adelphos](../../strongs/g/g80.md) shall [hamartanō](../../strongs/g/g264.md) against thee, [hypagō](../../strongs/g/g5217.md) and [elegchō](../../strongs/g/g1651.md) him between thee and him alone: if he shall [akouō](../../strongs/g/g191.md) thee, thou hast [kerdainō](../../strongs/g/g2770.md) thy [adelphos](../../strongs/g/g80.md).**

<a name="matthew_18_16"></a>Matthew 18:16

**But if he will not [akouō](../../strongs/g/g191.md), [paralambanō](../../strongs/g/g3880.md) with thee one or two more, that in the [stoma](../../strongs/g/g4750.md) of two or three [martys](../../strongs/g/g3144.md) every [rhēma](../../strongs/g/g4487.md) may be [histēmi](../../strongs/g/g2476.md).**

<a name="matthew_18_17"></a>Matthew 18:17

**And if he shall [parakouō](../../strongs/g/g3878.md) them, [eipon](../../strongs/g/g2036.md) unto the [ekklēsia](../../strongs/g/g1577.md): but if he [parakouō](../../strongs/g/g3878.md) the [ekklēsia](../../strongs/g/g1577.md), let him be unto thee as [ethnikos](../../strongs/g/g1482.md) and a [telōnēs](../../strongs/g/g5057.md).**

<a name="matthew_18_18"></a>Matthew 18:18

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, Whatsoever ye shall [deō](../../strongs/g/g1210.md) on [gē](../../strongs/g/g1093.md) shall be [deō](../../strongs/g/g1210.md) in [ouranos](../../strongs/g/g3772.md): and whatsoever ye shall [lyō](../../strongs/g/g3089.md) on [gē](../../strongs/g/g1093.md) shall be [lyō](../../strongs/g/g3089.md) in [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_18_19"></a>Matthew 18:19

**Again I [legō](../../strongs/g/g3004.md) unto you, That if two of you shall [symphōneō](../../strongs/g/g4856.md) on [gē](../../strongs/g/g1093.md) [peri](../../strongs/g/g4012.md) any [pragma](../../strongs/g/g4229.md) that they shall [aiteō](../../strongs/g/g154.md), it shall be [ginomai](../../strongs/g/g1096.md) for them of my [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_18_20"></a>Matthew 18:20

**For where two or three are [synagō](../../strongs/g/g4863.md) in my [onoma](../../strongs/g/g3686.md), there am I in the [mesos](../../strongs/g/g3319.md) of them.**

<a name="matthew_18_21"></a>Matthew 18:21

Then [proserchomai](../../strongs/g/g4334.md) [Petros](../../strongs/g/g4074.md) to him, and [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), [posakis](../../strongs/g/g4212.md) shall my [adelphos](../../strongs/g/g80.md) [hamartanō](../../strongs/g/g264.md) against me, and I [aphiēmi](../../strongs/g/g863.md) him? till [heptakis](../../strongs/g/g2034.md)?

<a name="matthew_18_22"></a>Matthew 18:22

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, **I [legō](../../strongs/g/g3004.md) not unto thee, Until [heptakis](../../strongs/g/g2034.md): but, Until [hebdomēkontakis](../../strongs/g/g1441.md) [hepta](../../strongs/g/g2033.md).**

<a name="matthew_18_23"></a>Matthew 18:23

**Therefore is the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) [homoioō](../../strongs/g/g3666.md) unto an [anthrōpos](../../strongs/g/g444.md) [basileus](../../strongs/g/g935.md), which would [synairō](../../strongs/g/g4868.md) [logos](../../strongs/g/g3056.md) of his [doulos](../../strongs/g/g1401.md).**

<a name="matthew_18_24"></a>Matthew 18:24

**And when he had [archomai](../../strongs/g/g756.md) to [synairō](../../strongs/g/g4868.md), one was [prospherō](../../strongs/g/g4374.md) unto him, which [opheiletēs](../../strongs/g/g3781.md) him [myrios](../../strongs/g/g3463.md) [talanton](../../strongs/g/g5007.md).**

<a name="matthew_18_25"></a>Matthew 18:25

**But forasmuch as he had not [apodidōmi](../../strongs/g/g591.md), his [kyrios](../../strongs/g/g2962.md) [keleuō](../../strongs/g/g2753.md) him to be [pipraskō](../../strongs/g/g4097.md), and his [gynē](../../strongs/g/g1135.md), and [teknon](../../strongs/g/g5043.md), and all that he had, and [apodidōmi](../../strongs/g/g591.md).**

<a name="matthew_18_26"></a>Matthew 18:26

**The [doulos](../../strongs/g/g1401.md) therefore [piptō](../../strongs/g/g4098.md), and [proskyneō](../../strongs/g/g4352.md) him, [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), have [makrothymeō](../../strongs/g/g3114.md) with me, and I will [apodidōmi](../../strongs/g/g591.md) thee all.**

<a name="matthew_18_27"></a>Matthew 18:27

**Then the [kyrios](../../strongs/g/g2962.md) of that [doulos](../../strongs/g/g1401.md) was [splagchnizomai](../../strongs/g/g4697.md), and [apolyō](../../strongs/g/g630.md) him, and [aphiēmi](../../strongs/g/g863.md) him the [dan(e)ion](../../strongs/g/g1156.md).**

<a name="matthew_18_28"></a>Matthew 18:28

**But the same [doulos](../../strongs/g/g1401.md) [exerchomai](../../strongs/g/g1831.md), and [heuriskō](../../strongs/g/g2147.md) one of his [syndoulos](../../strongs/g/g4889.md), which [opheilō](../../strongs/g/g3784.md) him an hundred [dēnarion](../../strongs/g/g1220.md): and he [krateō](../../strongs/g/g2902.md) on him, and [pnigō](../../strongs/g/g4155.md), [legō](../../strongs/g/g3004.md), [apodidōmi](../../strongs/g/g591.md) me that thou [opheilō](../../strongs/g/g3784.md).**

<a name="matthew_18_29"></a>Matthew 18:29

**And his [syndoulos](../../strongs/g/g4889.md) [piptō](../../strongs/g/g4098.md) at his [pous](../../strongs/g/g4228.md), and [parakaleō](../../strongs/g/g3870.md) him, [legō](../../strongs/g/g3004.md), Have [makrothymeō](../../strongs/g/g3114.md) with me, and I will [apodidōmi](../../strongs/g/g591.md) thee all.**

<a name="matthew_18_30"></a>Matthew 18:30

**And he would not: but [aperchomai](../../strongs/g/g565.md) and [ballō](../../strongs/g/g906.md) him into [phylakē](../../strongs/g/g5438.md), till he should [apodidōmi](../../strongs/g/g591.md) the [opheilō](../../strongs/g/g3784.md).**

<a name="matthew_18_31"></a>Matthew 18:31

**So when his [syndoulos](../../strongs/g/g4889.md) [eidō](../../strongs/g/g1492.md) what was [ginomai](../../strongs/g/g1096.md), they were [sphodra](../../strongs/g/g4970.md) [lypeō](../../strongs/g/g3076.md), and [erchomai](../../strongs/g/g2064.md) and [diasapheō](../../strongs/g/g1285.md) unto their [kyrios](../../strongs/g/g2962.md) all that was [ginomai](../../strongs/g/g1096.md).**

<a name="matthew_18_32"></a>Matthew 18:32

**Then his [kyrios](../../strongs/g/g2962.md), [proskaleō](../../strongs/g/g4341.md) him, [legō](../../strongs/g/g3004.md) unto him, O thou [ponēros](../../strongs/g/g4190.md) [doulos](../../strongs/g/g1401.md), I [aphiēmi](../../strongs/g/g863.md) thee all that [opheilē](../../strongs/g/g3782.md), because thou [parakaleō](../../strongs/g/g3870.md) me:**

<a name="matthew_18_33"></a>Matthew 18:33

**Shouldest not thou also [eleeō](../../strongs/g/g1653.md) on thy [syndoulos](../../strongs/g/g4889.md), even as I had [eleeō](../../strongs/g/g1653.md) on thee?**

<a name="matthew_18_34"></a>Matthew 18:34

**And his [kyrios](../../strongs/g/g2962.md) was [orgizō](../../strongs/g/g3710.md), and [paradidōmi](../../strongs/g/g3860.md) him to the [basanistēs](../../strongs/g/g930.md), till he should [apodidōmi](../../strongs/g/g591.md) all that was [opheilō](../../strongs/g/g3784.md) unto him.**

<a name="matthew_18_35"></a>Matthew 18:35

**So likewise shall my [epouranios](../../strongs/g/g2032.md) [patēr](../../strongs/g/g3962.md) [poieō](../../strongs/g/g4160.md) also unto you, if ye from your [kardia](../../strongs/g/g2588.md) [aphiēmi](../../strongs/g/g863.md) not every one his [adelphos](../../strongs/g/g80.md) their [paraptōma](../../strongs/g/g3900.md).**

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 17](matthew_17.md) - [Matthew 19](matthew_19.md)

---

[^1]: [Matthew 18:11 Commentary](../../commentary/matthew/matthew_18_commentary.md#matthew_18_11)
