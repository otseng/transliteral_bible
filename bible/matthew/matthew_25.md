# [Matthew 25](https://www.blueletterbible.org/kjv/mat/25/1/rl1/s_954001)

<a name="matthew_25_1"></a>Matthew 25:1

**Then shall the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) [homoioō](../../strongs/g/g3666.md) unto ten [parthenos](../../strongs/g/g3933.md), which [lambanō](../../strongs/g/g2983.md) their [lampas](../../strongs/g/g2985.md), and [exerchomai](../../strongs/g/g1831.md) to [apantēsis](../../strongs/g/g529.md) the [nymphios](../../strongs/g/g3566.md).**

<a name="matthew_25_2"></a>Matthew 25:2

**And five of them were [phronimos](../../strongs/g/g5429.md), and five [mōros](../../strongs/g/g3474.md).**

<a name="matthew_25_3"></a>Matthew 25:3

**They that [mōros](../../strongs/g/g3474.md) [lambanō](../../strongs/g/g2983.md) their [lampas](../../strongs/g/g2985.md), and [lambanō](../../strongs/g/g2983.md) no [elaion](../../strongs/g/g1637.md) with them:**

<a name="matthew_25_4"></a>Matthew 25:4

**But the [phronimos](../../strongs/g/g5429.md) [lambanō](../../strongs/g/g2983.md) [elaion](../../strongs/g/g1637.md) in their [aggeion](../../strongs/g/g30.md) with their [lampas](../../strongs/g/g2985.md).**

<a name="matthew_25_5"></a>Matthew 25:5

**While the [nymphios](../../strongs/g/g3566.md) [chronizō](../../strongs/g/g5549.md), they all [nystazō](../../strongs/g/g3573.md) and [katheudō](../../strongs/g/g2518.md).**

<a name="matthew_25_6"></a>Matthew 25:6

**And at [mesos](../../strongs/g/g3319.md) [nyx](../../strongs/g/g3571.md) there was a [kraugē](../../strongs/g/g2906.md) [ginomai](../../strongs/g/g1096.md), [idou](../../strongs/g/g2400.md), the [nymphios](../../strongs/g/g3566.md) [erchomai](../../strongs/g/g2064.md); [exerchomai](../../strongs/g/g1831.md) ye to [apantēsis](../../strongs/g/g529.md) him.**

<a name="matthew_25_7"></a>Matthew 25:7

**Then all those [parthenos](../../strongs/g/g3933.md) [egeirō](../../strongs/g/g1453.md), and [kosmeō](../../strongs/g/g2885.md) their [lampas](../../strongs/g/g2985.md).**

<a name="matthew_25_8"></a>Matthew 25:8

**And the [mōros](../../strongs/g/g3474.md) [eipon](../../strongs/g/g2036.md) unto the [phronimos](../../strongs/g/g5429.md), [didōmi](../../strongs/g/g1325.md) us of your [elaion](../../strongs/g/g1637.md); for our [lampas](../../strongs/g/g2985.md) are [sbennymi](../../strongs/g/g4570.md).**

<a name="matthew_25_9"></a>Matthew 25:9

**But the [phronimos](../../strongs/g/g5429.md) [apokrinomai](../../strongs/g/g611.md), [legō](../../strongs/g/g3004.md), lest there be not [arkeō](../../strongs/g/g714.md) for us and you: but [poreuō](../../strongs/g/g4198.md) ye rather to them that [pōleō](../../strongs/g/g4453.md), and [agorazō](../../strongs/g/g59.md) for yourselves.**

<a name="matthew_25_10"></a>Matthew 25:10

**And while they [aperchomai](../../strongs/g/g565.md) to [agorazō](../../strongs/g/g59.md), the [nymphios](../../strongs/g/g3566.md) [erchomai](../../strongs/g/g2064.md); and [hetoimos](../../strongs/g/g2092.md) [eiserchomai](../../strongs/g/g1525.md) with him to the [gamos](../../strongs/g/g1062.md): and the [thyra](../../strongs/g/g2374.md) was [kleiō](../../strongs/g/g2808.md).**

<a name="matthew_25_11"></a>Matthew 25:11

**Afterward [erchomai](../../strongs/g/g2064.md) also the [loipos](../../strongs/g/g3062.md) [parthenos](../../strongs/g/g3933.md), [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), [kyrios](../../strongs/g/g2962.md), [anoigō](../../strongs/g/g455.md) to us.**

<a name="matthew_25_12"></a>Matthew 25:12

**But he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, I [eidō](../../strongs/g/g1492.md) you not.**

<a name="matthew_25_13"></a>Matthew 25:13

**[grēgoreō](../../strongs/g/g1127.md) therefore, for ye [eidō](../../strongs/g/g1492.md) neither the [hēmera](../../strongs/g/g2250.md) nor the [hōra](../../strongs/g/g5610.md) wherein the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [erchomai](../../strongs/g/g2064.md).**

<a name="matthew_25_14"></a>Matthew 25:14

**For is as an [anthrōpos](../../strongs/g/g444.md) [apodēmeō](../../strongs/g/g589.md), who [kaleō](../../strongs/g/g2564.md) his own [doulos](../../strongs/g/g1401.md), and [paradidōmi](../../strongs/g/g3860.md) unto them his [hyparchonta](../../strongs/g/g5224.md).**

<a name="matthew_25_15"></a>Matthew 25:15

**And unto one he [didōmi](../../strongs/g/g1325.md) five [talanton](../../strongs/g/g5007.md), to another two, and to another one; to [hekastos](../../strongs/g/g1538.md) according to his [idios](../../strongs/g/g2398.md) [dynamis](../../strongs/g/g1411.md); and [eutheōs](../../strongs/g/g2112.md) [apodēmeō](../../strongs/g/g589.md).**

<a name="matthew_25_16"></a>Matthew 25:16

**Then he that had [lambanō](../../strongs/g/g2983.md) the five [talanton](../../strongs/g/g5007.md) [poreuō](../../strongs/g/g4198.md) and [ergazomai](../../strongs/g/g2038.md) with the same, and [poieō](../../strongs/g/g4160.md) them other five [talanton](../../strongs/g/g5007.md).**

<a name="matthew_25_17"></a>Matthew 25:17

**And [hōsautōs](../../strongs/g/g5615.md) he that [ho](../../strongs/g/g3588.md) two, he also [kerdainō](../../strongs/g/g2770.md) other two.** [^1]

<a name="matthew_25_18"></a>Matthew 25:18

**But he that had [lambanō](../../strongs/g/g2983.md) one [aperchomai](../../strongs/g/g565.md) and [oryssō](../../strongs/g/g3736.md) in [gē](../../strongs/g/g1093.md), and [apokryptō](../../strongs/g/g613.md) his [kyrios](../../strongs/g/g2962.md) [argyrion](../../strongs/g/g694.md).**

<a name="matthew_25_19"></a>Matthew 25:19

**After a [polys](../../strongs/g/g4183.md) [chronos](../../strongs/g/g5550.md) the [kyrios](../../strongs/g/g2962.md) of those [doulos](../../strongs/g/g1401.md) [erchomai](../../strongs/g/g2064.md), and [synairō](../../strongs/g/g4868.md) [logos](../../strongs/g/g3056.md) with them.**

<a name="matthew_25_20"></a>Matthew 25:20

**And so he that had [lambanō](../../strongs/g/g2983.md) five [talanton](../../strongs/g/g5007.md) [proserchomai](../../strongs/g/g4334.md) and [prospherō](../../strongs/g/g4374.md) other five [talanton](../../strongs/g/g5007.md), [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), thou [paradidōmi](../../strongs/g/g3860.md) unto me five [talanton](../../strongs/g/g5007.md): [ide](../../strongs/g/g2396.md), I have [kerdainō](../../strongs/g/g2770.md) beside them five [talanton](../../strongs/g/g5007.md) more.**

<a name="matthew_25_21"></a>Matthew 25:21

**His [kyrios](../../strongs/g/g2962.md) [phēmi](../../strongs/g/g5346.md) unto him, [eu](../../strongs/g/g2095.md), [agathos](../../strongs/g/g18.md) and [pistos](../../strongs/g/g4103.md) [doulos](../../strongs/g/g1401.md): thou hast been [pistos](../../strongs/g/g4103.md) over an [oligos](../../strongs/g/g3641.md), I will [kathistēmi](../../strongs/g/g2525.md) thee over [polys](../../strongs/g/g4183.md): [eiserchomai](../../strongs/g/g1525.md) thou into the [chara](../../strongs/g/g5479.md) of thy [kyrios](../../strongs/g/g2962.md).**

<a name="matthew_25_22"></a>Matthew 25:22

**He also that had [lambanō](../../strongs/g/g2983.md) two [talanton](../../strongs/g/g5007.md) [proserchomai](../../strongs/g/g4334.md) and [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), thou [paradidōmi](../../strongs/g/g3860.md) unto me two [talanton](../../strongs/g/g5007.md): [ide](../../strongs/g/g2396.md), I have [kerdainō](../../strongs/g/g2770.md) two other [talanton](../../strongs/g/g5007.md) beside them.**

<a name="matthew_25_23"></a>Matthew 25:23

**His [kyrios](../../strongs/g/g2962.md) [phēmi](../../strongs/g/g5346.md) unto him, [eu](../../strongs/g/g2095.md), [agathos](../../strongs/g/g18.md) and [pistos](../../strongs/g/g4103.md) [doulos](../../strongs/g/g1401.md); thou hast been [pistos](../../strongs/g/g4103.md) over an [oligos](../../strongs/g/g3641.md), I will [kathistēmi](../../strongs/g/g2525.md) thee over [polys](../../strongs/g/g4183.md): [eiserchomai](../../strongs/g/g1525.md) thou into the [chara](../../strongs/g/g5479.md) of thy [kyrios](../../strongs/g/g2962.md).**

<a name="matthew_25_24"></a>Matthew 25:24

**Then he which had [lambanō](../../strongs/g/g2983.md) the one [talanton](../../strongs/g/g5007.md) [proserchomai](../../strongs/g/g4334.md) and [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), I [ginōskō](../../strongs/g/g1097.md) thee that thou art [sklēros](../../strongs/g/g4642.md) [anthrōpos](../../strongs/g/g444.md), [therizō](../../strongs/g/g2325.md) where thou hast not [speirō](../../strongs/g/g4687.md), and [synagō](../../strongs/g/g4863.md) where thou hast not [diaskorpizō](../../strongs/g/g1287.md):**

<a name="matthew_25_25"></a>Matthew 25:25

**And I was [phobeō](../../strongs/g/g5399.md), and [aperchomai](../../strongs/g/g565.md) and [kryptō](../../strongs/g/g2928.md) thy [talanton](../../strongs/g/g5007.md) in the [gē](../../strongs/g/g1093.md): [ide](../../strongs/g/g2396.md), thou hast thine.**

<a name="matthew_25_26"></a>Matthew 25:26

**His [kyrios](../../strongs/g/g2962.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, [ponēros](../../strongs/g/g4190.md) and [oknēros](../../strongs/g/g3636.md) [doulos](../../strongs/g/g1401.md), thou [eidō](../../strongs/g/g1492.md) that I [therizō](../../strongs/g/g2325.md) where I [speirō](../../strongs/g/g4687.md) not, and [synagō](../../strongs/g/g4863.md) where I have not [diaskorpizō](../../strongs/g/g1287.md):**

<a name="matthew_25_27"></a>Matthew 25:27

**Thou oughtest therefore to have [ballō](../../strongs/g/g906.md) my [argyrion](../../strongs/g/g694.md) to the [trapezitēs](../../strongs/g/g5133.md), and at my [erchomai](../../strongs/g/g2064.md) I should have [komizō](../../strongs/g/g2865.md) mine own with [tokos](../../strongs/g/g5110.md).**

<a name="matthew_25_28"></a>Matthew 25:28

**[airō](../../strongs/g/g142.md) therefore the [talanton](../../strongs/g/g5007.md) from him, and [didōmi](../../strongs/g/g1325.md) unto him which hath ten [talanton](../../strongs/g/g5007.md).**

<a name="matthew_25_29"></a>Matthew 25:29

**For unto every one that hath shall be [didōmi](../../strongs/g/g1325.md), and he shall [perisseuō](../../strongs/g/g4052.md): but from him that hath not shall be [airō](../../strongs/g/g142.md) even that which he hath.**

<a name="matthew_25_30"></a>Matthew 25:30

**And [ekballō](../../strongs/g/g1544.md) ye the [achreios](../../strongs/g/g888.md) [doulos](../../strongs/g/g1401.md) into [exōteros](../../strongs/g/g1857.md) [skotos](../../strongs/g/g4655.md): there shall be [klauthmos](../../strongs/g/g2805.md) and [brygmos](../../strongs/g/g1030.md) of [odous](../../strongs/g/g3599.md).**

<a name="matthew_25_31"></a>Matthew 25:31

**When the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) shall [erchomai](../../strongs/g/g2064.md) in his [doxa](../../strongs/g/g1391.md), and all the [hagios](../../strongs/g/g40.md) [aggelos](../../strongs/g/g32.md) with him, then shall he [kathizō](../../strongs/g/g2523.md) upon the [thronos](../../strongs/g/g2362.md) of his [doxa](../../strongs/g/g1391.md):**

<a name="matthew_25_32"></a>Matthew 25:32

**And before him shall be [synagō](../../strongs/g/g4863.md) all [ethnos](../../strongs/g/g1484.md): and he shall [aphorizō](../../strongs/g/g873.md) them from [allēlōn](../../strongs/g/g240.md), as a [poimēn](../../strongs/g/g4166.md) [aphorizō](../../strongs/g/g873.md) [probaton](../../strongs/g/g4263.md) from the [eriphos](../../strongs/g/g2056.md):**

<a name="matthew_25_33"></a>Matthew 25:33

**And he shall [histēmi](../../strongs/g/g2476.md) the [probaton](../../strongs/g/g4263.md) on his [dexios](../../strongs/g/g1188.md), but the [eriphion](../../strongs/g/g2055.md) on the [euōnymos](../../strongs/g/g2176.md).**

<a name="matthew_25_34"></a>Matthew 25:34

**Then shall the [basileus](../../strongs/g/g935.md) [eipon](../../strongs/g/g2046.md) unto them on his [dexios](../../strongs/g/g1188.md), [deute](../../strongs/g/g1205.md), ye [eulogeō](../../strongs/g/g2127.md) of my [patēr](../../strongs/g/g3962.md), [klēronomeō](../../strongs/g/g2816.md) the [basileia](../../strongs/g/g932.md) [hetoimazō](../../strongs/g/g2090.md) for you from the [katabolē](../../strongs/g/g2602.md) of the [kosmos](../../strongs/g/g2889.md):**

<a name="matthew_25_35"></a>Matthew 25:35

**For I was a [peinaō](../../strongs/g/g3983.md), and ye [didōmi](../../strongs/g/g1325.md) me [phago](../../strongs/g/g5315.md): I was [dipsaō](../../strongs/g/g1372.md), and ye [potizō](../../strongs/g/g4222.md) me: I was a [xenos](../../strongs/g/g3581.md), and ye [synagō](../../strongs/g/g4863.md) me:**

<a name="matthew_25_36"></a>Matthew 25:36

**[gymnos](../../strongs/g/g1131.md), and ye [periballō](../../strongs/g/g4016.md) me: I was [astheneō](../../strongs/g/g770.md), and ye [episkeptomai](../../strongs/g/g1980.md) me: I was in [phylakē](../../strongs/g/g5438.md), and ye [erchomai](../../strongs/g/g2064.md) unto me.**

<a name="matthew_25_37"></a>Matthew 25:37

**Then shall the [dikaios](../../strongs/g/g1342.md) [apokrinomai](../../strongs/g/g611.md) him, [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), when [eidō](../../strongs/g/g1492.md) we thee [peinaō](../../strongs/g/g3983.md), and [trephō](../../strongs/g/g5142.md)? or [dipsaō](../../strongs/g/g1372.md), and [potizō](../../strongs/g/g4222.md)?**

<a name="matthew_25_38"></a>Matthew 25:38

**When [eidō](../../strongs/g/g1492.md) we thee a [xenos](../../strongs/g/g3581.md), and [synagō](../../strongs/g/g4863.md)? or [gymnos](../../strongs/g/g1131.md), and [periballō](../../strongs/g/g4016.md)?**

<a name="matthew_25_39"></a>Matthew 25:39

**Or when [eidō](../../strongs/g/g1492.md) we thee [asthenēs](../../strongs/g/g772.md), or in [phylakē](../../strongs/g/g5438.md), and [erchomai](../../strongs/g/g2064.md) unto thee?**

<a name="matthew_25_40"></a>Matthew 25:40

**And the [basileus](../../strongs/g/g935.md) shall [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2046.md) unto them, [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, Inasmuch as ye have [poieō](../../strongs/g/g4160.md) unto one of the [elachistos](../../strongs/g/g1646.md) of these my [adelphos](../../strongs/g/g80.md), ye have [poieō](../../strongs/g/g4160.md) unto me.**

<a name="matthew_25_41"></a>Matthew 25:41

**Then shall he [eipon](../../strongs/g/g2046.md) also unto them on the [euōnymos](../../strongs/g/g2176.md), [poreuō](../../strongs/g/g4198.md) from me, ye [kataraomai](../../strongs/g/g2672.md), into [aiōnios](../../strongs/g/g166.md) [pyr](../../strongs/g/g4442.md), [hetoimazō](../../strongs/g/g2090.md) for the [diabolos](../../strongs/g/g1228.md) and his [aggelos](../../strongs/g/g32.md):**

<a name="matthew_25_42"></a>Matthew 25:42

**For I was [peinaō](../../strongs/g/g3983.md), and ye [didōmi](../../strongs/g/g1325.md) me no [phago](../../strongs/g/g5315.md): I was [dipsaō](../../strongs/g/g1372.md), and ye [potizō](../../strongs/g/g4222.md) me not:**

<a name="matthew_25_43"></a>Matthew 25:43

**I was a [xenos](../../strongs/g/g3581.md), and ye [synagō](../../strongs/g/g4863.md) me not: [gymnos](../../strongs/g/g1131.md), and ye [periballō](../../strongs/g/g4016.md) me not: [asthenēs](../../strongs/g/g772.md), and in [phylakē](../../strongs/g/g5438.md), and ye [episkeptomai](../../strongs/g/g1980.md) me not.**

<a name="matthew_25_44"></a>Matthew 25:44

**Then shall they also [apokrinomai](../../strongs/g/g611.md) him, [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), when [eidō](../../strongs/g/g1492.md) we thee [peinaō](../../strongs/g/g3983.md), or [dipsaō](../../strongs/g/g1372.md), or a [xenos](../../strongs/g/g3581.md), or [gymnos](../../strongs/g/g1131.md), or [asthenēs](../../strongs/g/g772.md), or in [phylakē](../../strongs/g/g5438.md), and [diakoneō](../../strongs/g/g1247.md) not unto thee?**

<a name="matthew_25_45"></a>Matthew 25:45

**Then shall he [apokrinomai](../../strongs/g/g611.md) them, [legō](../../strongs/g/g3004.md), [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, Inasmuch as ye [poieō](../../strongs/g/g4160.md) it not to one of the [elachistos](../../strongs/g/g1646.md) of these, ye [poieō](../../strongs/g/g4160.md) not to me.**

<a name="matthew_25_46"></a>Matthew 25:46

**And these shall [aperchomai](../../strongs/g/g565.md) into [aiōnios](../../strongs/g/g166.md) [kolasis](../../strongs/g/g2851.md): but the [dikaios](../../strongs/g/g1342.md) into [zōē](../../strongs/g/g2222.md) [aiōnios](../../strongs/g/g166.md).**

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 24](matthew_24.md) - [Matthew 26](matthew_26.md)

---

[^1]: [Matthew 25:17 Commentary](../../commentary/matthew/matthew_25_commentary.md#matthew_25_17)
