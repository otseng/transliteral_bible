# [Matthew 27](https://www.blueletterbible.org/kjv/mat/27/1/rl1/s_956001)

<a name="matthew_27_1"></a>Matthew 27:1

When the [prōïa](../../strongs/g/g4405.md) was [ginomai](../../strongs/g/g1096.md), all the [archiereus](../../strongs/g/g749.md) and [presbyteros](../../strongs/g/g4245.md) of the [laos](../../strongs/g/g2992.md) [lambanō](../../strongs/g/g2983.md) [symboulion](../../strongs/g/g4824.md) against [Iēsous](../../strongs/g/g2424.md) to [thanatoō](../../strongs/g/g2289.md) him:

<a name="matthew_27_2"></a>Matthew 27:2

And when they had [deō](../../strongs/g/g1210.md) him, they [apagō](../../strongs/g/g520.md), and [paradidōmi](../../strongs/g/g3860.md) him to [Pontios](../../strongs/g/g4194.md) [Pilatos](../../strongs/g/g4091.md) the [hēgemōn](../../strongs/g/g2232.md).

<a name="matthew_27_3"></a>Matthew 27:3

Then [Ioudas](../../strongs/g/g2455.md), which had [paradidōmi](../../strongs/g/g3860.md) him, when he [eidō](../../strongs/g/g1492.md) that he was [katakrinō](../../strongs/g/g2632.md), [metamelomai](../../strongs/g/g3338.md), and [apostrephō](../../strongs/g/g654.md) the thirty [argyrion](../../strongs/g/g694.md) to the [archiereus](../../strongs/g/g749.md) and [presbyteros](../../strongs/g/g4245.md),

<a name="matthew_27_4"></a>Matthew 27:4

[legō](../../strongs/g/g3004.md), I have [hamartanō](../../strongs/g/g264.md) in that I have [paradidōmi](../../strongs/g/g3860.md) the [athōos](../../strongs/g/g121.md) [haima](../../strongs/g/g129.md). And they [eipon](../../strongs/g/g2036.md), What to us? [optanomai](../../strongs/g/g3700.md) thou. [^1]

<a name="matthew_27_5"></a>Matthew 27:5

And he [rhiptō](../../strongs/g/g4496.md) the [argyrion](../../strongs/g/g694.md) in the [naos](../../strongs/g/g3485.md), and [anachōreō](../../strongs/g/g402.md), and [aperchomai](../../strongs/g/g565.md) and [apagchō](../../strongs/g/g519.md).

<a name="matthew_27_6"></a>Matthew 27:6

And the [archiereus](../../strongs/g/g749.md) [lambanō](../../strongs/g/g2983.md) the [argyrion](../../strongs/g/g694.md), and [eipon](../../strongs/g/g2036.md), It is not [exesti](../../strongs/g/g1832.md) for to [ballō](../../strongs/g/g906.md) them into the [korban](../../strongs/g/g2878.md), because it is the [timē](../../strongs/g/g5092.md) of [haima](../../strongs/g/g129.md).

<a name="matthew_27_7"></a>Matthew 27:7

And they [lambanō](../../strongs/g/g2983.md) [symboulion](../../strongs/g/g4824.md), and [agorazō](../../strongs/g/g59.md) with them the [kerameus](../../strongs/g/g2763.md) [agros](../../strongs/g/g68.md), to [taphē](../../strongs/g/g5027.md) [xenos](../../strongs/g/g3581.md) in.

<a name="matthew_27_8"></a>Matthew 27:8

Wherefore that [agros](../../strongs/g/g68.md) was [kaleō](../../strongs/g/g2564.md), The [agros](../../strongs/g/g68.md) of [haima](../../strongs/g/g129.md), unto [sēmeron](../../strongs/g/g4594.md).

<a name="matthew_27_9"></a>Matthew 27:9

Then was [plēroō](../../strongs/g/g4137.md) that which was [rheō](../../strongs/g/g4483.md) by [Ieremias](../../strongs/g/g2408.md) the [prophētēs](../../strongs/g/g4396.md), [legō](../../strongs/g/g3004.md), And they [lambanō](../../strongs/g/g2983.md) the thirty [argyrion](../../strongs/g/g694.md), the [timē](../../strongs/g/g5092.md) [timaō](../../strongs/g/g5091.md), whom they of the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md) [timaō](../../strongs/g/g5091.md); [^2]

<a name="matthew_27_10"></a>Matthew 27:10

And [didōmi](../../strongs/g/g1325.md) them for the [kerameus](../../strongs/g/g2763.md) [agros](../../strongs/g/g68.md), as the [kyrios](../../strongs/g/g2962.md) [syntassō](../../strongs/g/g4929.md) me.

<a name="matthew_27_11"></a>Matthew 27:11

And [Iēsous](../../strongs/g/g2424.md) [histēmi](../../strongs/g/g2476.md) before the [hēgemōn](../../strongs/g/g2232.md): and the [hēgemōn](../../strongs/g/g2232.md) [eperōtaō](../../strongs/g/g1905.md) him, [legō](../../strongs/g/g3004.md), Art thou the [basileus](../../strongs/g/g935.md) of the [Ioudaios](../../strongs/g/g2453.md)? And [Iēsous](../../strongs/g/g2424.md) [phēmi](../../strongs/g/g5346.md) unto him, **Thou [legō](../../strongs/g/g3004.md).**

<a name="matthew_27_12"></a>Matthew 27:12

And when he was [katēgoreō](../../strongs/g/g2723.md) of the [archiereus](../../strongs/g/g749.md) and [presbyteros](../../strongs/g/g4245.md), he [apokrinomai](../../strongs/g/g611.md) [oudeis](../../strongs/g/g3762.md).

<a name="matthew_27_13"></a>Matthew 27:13

Then [legō](../../strongs/g/g3004.md) [Pilatos](../../strongs/g/g4091.md) unto him, [akouō](../../strongs/g/g191.md) thou not how many things they [katamartyreō](../../strongs/g/g2649.md) thee?

<a name="matthew_27_14"></a>Matthew 27:14

And he [ou](../../strongs/g/g3756.md) [apokrinomai](../../strongs/g/g611.md) him to [oude](../../strongs/g/g3761.md) a [rhēma](../../strongs/g/g4487.md); insomuch that the [hēgemōn](../../strongs/g/g2232.md) [thaumazō](../../strongs/g/g2296.md) [lian](../../strongs/g/g3029.md).

<a name="matthew_27_15"></a>Matthew 27:15

Now at [heortē](../../strongs/g/g1859.md) the [hēgemōn](../../strongs/g/g2232.md) was [ethō](../../strongs/g/g1486.md) to [apolyō](../../strongs/g/g630.md) unto the [ochlos](../../strongs/g/g3793.md) a [desmios](../../strongs/g/g1198.md), whom they would.

<a name="matthew_27_16"></a>Matthew 27:16

And they had then an [episēmos](../../strongs/g/g1978.md) [desmios](../../strongs/g/g1198.md), [legō](../../strongs/g/g3004.md) [Barabbas](../../strongs/g/g912.md). [^3]

<a name="matthew_27_17"></a>Matthew 27:17

Therefore when they were [synagō](../../strongs/g/g4863.md), [Pilatos](../../strongs/g/g4091.md) [eipon](../../strongs/g/g2036.md) unto them, Whom will ye that I [apolyō](../../strongs/g/g630.md) unto you? [Barabbas](../../strongs/g/g912.md), or [Iēsous](../../strongs/g/g2424.md) which is [legō](../../strongs/g/g3004.md) [Christos](../../strongs/g/g5547.md)?

<a name="matthew_27_18"></a>Matthew 27:18

For he [eidō](../../strongs/g/g1492.md) that for [phthonos](../../strongs/g/g5355.md) they had [paradidōmi](../../strongs/g/g3860.md) him.

<a name="matthew_27_19"></a>Matthew 27:19

When he was [kathēmai](../../strongs/g/g2521.md) on [bēma](../../strongs/g/g968.md), his [gynē](../../strongs/g/g1135.md) [apostellō](../../strongs/g/g649.md) unto him, [legō](../../strongs/g/g3004.md), Have thou [mēdeis](../../strongs/g/g3367.md) to do with that [dikaios](../../strongs/g/g1342.md): for I have [paschō](../../strongs/g/g3958.md) [polys](../../strongs/g/g4183.md) [sēmeron](../../strongs/g/g4594.md) in a [onar](../../strongs/g/g3677.md) because of him.

<a name="matthew_27_20"></a>Matthew 27:20

But the [archiereus](../../strongs/g/g749.md) and [presbyteros](../../strongs/g/g4245.md) [peithō](../../strongs/g/g3982.md) the [ochlos](../../strongs/g/g3793.md) that they should [aiteō](../../strongs/g/g154.md) [Barabbas](../../strongs/g/g912.md), and [apollymi](../../strongs/g/g622.md) [Iēsous](../../strongs/g/g2424.md).

<a name="matthew_27_21"></a>Matthew 27:21

The [hēgemōn](../../strongs/g/g2232.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, Whether of the twain will ye that I [apolyō](../../strongs/g/g630.md) unto you? They [eipon](../../strongs/g/g2036.md), [Barabbas](../../strongs/g/g912.md).

<a name="matthew_27_22"></a>Matthew 27:22

[Pilatos](../../strongs/g/g4091.md) [legō](../../strongs/g/g3004.md) unto them, What shall I [poieō](../../strongs/g/g4160.md) then with [Iēsous](../../strongs/g/g2424.md) which is [legō](../../strongs/g/g3004.md) [Christos](../../strongs/g/g5547.md)? all [legō](../../strongs/g/g3004.md) unto him, Let him be [stauroō](../../strongs/g/g4717.md).

<a name="matthew_27_23"></a>Matthew 27:23

And the [hēgemōn](../../strongs/g/g2232.md) [phēmi](../../strongs/g/g5346.md), Why, what [kakos](../../strongs/g/g2556.md) hath he [poieō](../../strongs/g/g4160.md)? But they [krazō](../../strongs/g/g2896.md) the [perissōs](../../strongs/g/g4057.md), [legō](../../strongs/g/g3004.md), Let him be [stauroō](../../strongs/g/g4717.md).

<a name="matthew_27_24"></a>Matthew 27:24

When [Pilatos](../../strongs/g/g4091.md) [eidō](../../strongs/g/g1492.md) that he could [ōpheleō](../../strongs/g/g5623.md) nothing, but rather a [thorybos](../../strongs/g/g2351.md) was made, he [lambanō](../../strongs/g/g2983.md) [hydōr](../../strongs/g/g5204.md), and [aponiptō](../../strongs/g/g633.md) [cheir](../../strongs/g/g5495.md) [apenanti](../../strongs/g/g561.md) the [ochlos](../../strongs/g/g3793.md), [legō](../../strongs/g/g3004.md), I am [athōos](../../strongs/g/g121.md) of the [haima](../../strongs/g/g129.md) of this [dikaios](../../strongs/g/g1342.md): [optanomai](../../strongs/g/g3700.md) ye.

<a name="matthew_27_25"></a>Matthew 27:25

Then [apokrinomai](../../strongs/g/g611.md) all the [laos](../../strongs/g/g2992.md), and [eipon](../../strongs/g/g2036.md), His [haima](../../strongs/g/g129.md) on us, and on our [teknon](../../strongs/g/g5043.md).

<a name="matthew_27_26"></a>Matthew 27:26

Then [apolyō](../../strongs/g/g630.md) he [Barabbas](../../strongs/g/g912.md) unto them: and when he had [phragelloō](../../strongs/g/g5417.md) [Iēsous](../../strongs/g/g2424.md), he [paradidōmi](../../strongs/g/g3860.md) to be [stauroō](../../strongs/g/g4717.md).

<a name="matthew_27_27"></a>Matthew 27:27

Then the [stratiōtēs](../../strongs/g/g4757.md) of the [hēgemōn](../../strongs/g/g2232.md) [paralambanō](../../strongs/g/g3880.md) [Iēsous](../../strongs/g/g2424.md) into the [praitōrion](../../strongs/g/g4232.md), and [synagō](../../strongs/g/g4863.md) unto him the [holos](../../strongs/g/g3650.md) [speira](../../strongs/g/g4686.md).

<a name="matthew_27_28"></a>Matthew 27:28

And they [ekdyō](../../strongs/g/g1562.md) him, and [peritithēmi](../../strongs/g/g4060.md) him a [kokkinos](../../strongs/g/g2847.md) [chlamys](../../strongs/g/g5511.md).

<a name="matthew_27_29"></a>Matthew 27:29

And when they had [plekō](../../strongs/g/g4120.md) a [stephanos](../../strongs/g/g4735.md) of [akantha](../../strongs/g/g173.md), they [epitithēmi](../../strongs/g/g2007.md) upon his [kephalē](../../strongs/g/g2776.md), and a [kalamos](../../strongs/g/g2563.md) in his [dexios](../../strongs/g/g1188.md): and they [gonypeteō](../../strongs/g/g1120.md) before him, and [empaizō](../../strongs/g/g1702.md) him, [legō](../../strongs/g/g3004.md), [chairō](../../strongs/g/g5463.md), [basileus](../../strongs/g/g935.md) of the [Ioudaios](../../strongs/g/g2453.md)!

<a name="matthew_27_30"></a>Matthew 27:30

And they [emptyō](../../strongs/g/g1716.md) upon him, and [lambanō](../../strongs/g/g2983.md) the [kalamos](../../strongs/g/g2563.md), and [typtō](../../strongs/g/g5180.md) him on the [kephalē](../../strongs/g/g2776.md).

<a name="matthew_27_31"></a>Matthew 27:31

And after that they had [empaizō](../../strongs/g/g1702.md) him, they [ekdyō](../../strongs/g/g1562.md) the [chlamys](../../strongs/g/g5511.md) from him, and [endyō](../../strongs/g/g1746.md) his own [himation](../../strongs/g/g2440.md) on him, and [apagō](../../strongs/g/g520.md)him to [stauroō](../../strongs/g/g4717.md).

<a name="matthew_27_32"></a>Matthew 27:32

And as they [exerchomai](../../strongs/g/g1831.md), they [heuriskō](../../strongs/g/g2147.md) an [anthrōpos](../../strongs/g/g444.md) of [Kyrēnaios](../../strongs/g/g2956.md), [Simōn](../../strongs/g/g4613.md) by [onoma](../../strongs/g/g3686.md): him they [aggareuō](../../strongs/g/g29.md) to [airō](../../strongs/g/g142.md) his [stauros](../../strongs/g/g4716.md).

<a name="matthew_27_33"></a>Matthew 27:33

And when they were [erchomai](../../strongs/g/g2064.md) unto a [topos](../../strongs/g/g5117.md) [legō](../../strongs/g/g3004.md) [Golgotha](../../strongs/g/g1115.md), that is to [legō](../../strongs/g/g3004.md), a [topos](../../strongs/g/g5117.md) of a [kranion](../../strongs/g/g2898.md),

<a name="matthew_27_34"></a>Matthew 27:34

They [didōmi](../../strongs/g/g1325.md) him [oxos](../../strongs/g/g3690.md) to [pinō](../../strongs/g/g4095.md) [mignymi](../../strongs/g/g3396.md) with [cholē](../../strongs/g/g5521.md) : and when he had [geuomai](../../strongs/g/g1089.md), he would not [pinō](../../strongs/g/g4095.md).

<a name="matthew_27_35"></a>Matthew 27:35

And they [stauroō](../../strongs/g/g4717.md) him, and [diamerizō](../../strongs/g/g1266.md) his [himation](../../strongs/g/g2440.md), [ballō](../../strongs/g/g906.md) [klēros](../../strongs/g/g2819.md): that it might be [plēroō](../../strongs/g/g4137.md) which was [rheō](../../strongs/g/g4483.md) by the [prophētēs](../../strongs/g/g4396.md), They parted my [himation](../../strongs/g/g2440.md) among them, and upon my [himatismos](../../strongs/g/g2441.md) did they cast [klēros](../../strongs/g/g2819.md). [^4]

<a name="matthew_27_36"></a>Matthew 27:36

And [kathēmai](../../strongs/g/g2521.md) they [tēreō](../../strongs/g/g5083.md) him there;

<a name="matthew_27_37"></a>Matthew 27:37

And [epitithēmi](../../strongs/g/g2007.md) over his [kephalē](../../strongs/g/g2776.md) his [aitia](../../strongs/g/g156.md) [graphō](../../strongs/g/g1125.md), [houtos](../../strongs/g/g3778.md) [esti](../../strongs/g/g2076.md) [Iēsous](../../strongs/g/g2424.md) [basileus](../../strongs/g/g935.md) [Ioudaios](../../strongs/g/g2453.md).

<a name="matthew_27_38"></a>Matthew 27:38

Then were there two [lēstēs](../../strongs/g/g3027.md) [stauroō](../../strongs/g/g4717.md) with him, one on the [dexios](../../strongs/g/g1188.md), and another on the [euōnymos](../../strongs/g/g2176.md).

<a name="matthew_27_39"></a>Matthew 27:39

And [paraporeuomai](../../strongs/g/g3899.md) [blasphēmeō](../../strongs/g/g987.md) him, [kineō](../../strongs/g/g2795.md) their [kephalē](../../strongs/g/g2776.md),

<a name="matthew_27_40"></a>Matthew 27:40

And [legō](../../strongs/g/g3004.md), Thou that [katalyō](../../strongs/g/g2647.md) the [naos](../../strongs/g/g3485.md), and [oikodomeō](../../strongs/g/g3618.md) it in three [hēmera](../../strongs/g/g2250.md), [sōzō](../../strongs/g/g4982.md) thyself. If thou be the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), [katabainō](../../strongs/g/g2597.md) from the [stauros](../../strongs/g/g4716.md).

<a name="matthew_27_41"></a>Matthew 27:41

[homoiōs](../../strongs/g/g3668.md) also the [archiereus](../../strongs/g/g749.md) [empaizō](../../strongs/g/g1702.md), with the [grammateus](../../strongs/g/g1122.md) and [presbyteros](../../strongs/g/g4245.md), [legō](../../strongs/g/g3004.md),

<a name="matthew_27_42"></a>Matthew 27:42

He [sōzō](../../strongs/g/g4982.md) others; himself he cannot [sōzō](../../strongs/g/g4982.md). If he be the [basileus](../../strongs/g/g935.md) of [Israēl](../../strongs/g/g2474.md), let him now [katabainō](../../strongs/g/g2597.md) from the [stauros](../../strongs/g/g4716.md), and we will [pisteuō](../../strongs/g/g4100.md) him.

<a name="matthew_27_43"></a>Matthew 27:43

He [peithō](../../strongs/g/g3982.md) in [theos](../../strongs/g/g2316.md); let him [rhyomai](../../strongs/g/g4506.md) him now, if he will have him: for he [eipon](../../strongs/g/g2036.md), I am the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md).

<a name="matthew_27_44"></a>Matthew 27:44

The [lēstēs](../../strongs/g/g3027.md) also, which were [systauroō](../../strongs/g/g4957.md) him, [oneidizō](../../strongs/g/g3679.md) the same in his.

<a name="matthew_27_45"></a>Matthew 27:45

Now from the sixth [hōra](../../strongs/g/g5610.md) there was [skotos](../../strongs/g/g4655.md) over all the [gē](../../strongs/g/g1093.md) unto the ninth [hōra](../../strongs/g/g5610.md).

<a name="matthew_27_46"></a>Matthew 27:46

And about the ninth [hōra](../../strongs/g/g5610.md) [Iēsous](../../strongs/g/g2424.md) [anaboaō](../../strongs/g/g310.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [legō](../../strongs/g/g3004.md), **[ēli](../../strongs/g/g2241.md), [ēli](../../strongs/g/g2241.md), [lema](../../strongs/g/g2982.md) [sabachthani](../../strongs/g/g4518.md)?** that is to say, **My [theos](../../strongs/g/g2316.md), my [theos](../../strongs/g/g2316.md), why hast thou [egkataleipō](../../strongs/g/g1459.md) me?**

<a name="matthew_27_47"></a>Matthew 27:47

Some of them that [histēmi](../../strongs/g/g2476.md) there, when they [akouō](../../strongs/g/g191.md), [legō](../../strongs/g/g3004.md), This [phōneō](../../strongs/g/g5455.md) for [Ēlias](../../strongs/g/g2243.md).

<a name="matthew_27_48"></a>Matthew 27:48

And [eutheōs](../../strongs/g/g2112.md) one of them [trechō](../../strongs/g/g5143.md), and [lambanō](../../strongs/g/g2983.md) a [spoggos](../../strongs/g/g4699.md), and [pimplēmi](../../strongs/g/g4130.md) with [oxos](../../strongs/g/g3690.md), and [peritithēmi](../../strongs/g/g4060.md) a [kalamos](../../strongs/g/g2563.md), and [potizō](../../strongs/g/g4222.md) him.

<a name="matthew_27_49"></a>Matthew 27:49

The [loipos](../../strongs/g/g3062.md) [legō](../../strongs/g/g3004.md), [aphiēmi](../../strongs/g/g863.md), let us [eidō](../../strongs/g/g1492.md) whether [Ēlias](../../strongs/g/g2243.md) will [erchomai](../../strongs/g/g2064.md) to [sōzō](../../strongs/g/g4982.md) him. [^5]

<a name="matthew_27_50"></a>Matthew 27:50

[Iēsous](../../strongs/g/g2424.md), when he had [krazō](../../strongs/g/g2896.md) again with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [aphiēmi](../../strongs/g/g863.md) the [pneuma](../../strongs/g/g4151.md).

<a name="matthew_27_51"></a>Matthew 27:51

And, [idou](../../strongs/g/g2400.md), the [katapetasma](../../strongs/g/g2665.md) of the [naos](../../strongs/g/g3485.md) was [schizō](../../strongs/g/g4977.md) in [dyo](../../strongs/g/g1417.md) from the [anōthen](../../strongs/g/g509.md) to the [katō](../../strongs/g/g2736.md); and the [gē](../../strongs/g/g1093.md) did [seiō](../../strongs/g/g4579.md), and the [petra](../../strongs/g/g4073.md) [schizō](../../strongs/g/g4977.md);

<a name="matthew_27_52"></a>Matthew 27:52

And the [mnēmeion](../../strongs/g/g3419.md) were [anoigō](../../strongs/g/g455.md); and [polys](../../strongs/g/g4183.md) [sōma](../../strongs/g/g4983.md) of the [hagios](../../strongs/g/g40.md) which [koimaō](../../strongs/g/g2837.md) [egeirō](../../strongs/g/g1453.md),

<a name="matthew_27_53"></a>Matthew 27:53

And [exerchomai](../../strongs/g/g1831.md) of the [mnēmeion](../../strongs/g/g3419.md) after his [egersis](../../strongs/g/g1454.md), and [eiserchomai](../../strongs/g/g1525.md) into the [hagios](../../strongs/g/g40.md) [polis](../../strongs/g/g4172.md), and [emphanizō](../../strongs/g/g1718.md) unto [polys](../../strongs/g/g4183.md).

<a name="matthew_27_54"></a>Matthew 27:54

Now when the [hekatontarchēs](../../strongs/g/g1543.md), and they that were with him, [tēreō](../../strongs/g/g5083.md) [Iēsous](../../strongs/g/g2424.md), [eidō](../../strongs/g/g1492.md) the [seismos](../../strongs/g/g4578.md), and those things that were [ginomai](../../strongs/g/g1096.md), they [phobeō](../../strongs/g/g5399.md) [sphodra](../../strongs/g/g4970.md), [legō](../../strongs/g/g3004.md), [alēthōs](../../strongs/g/g230.md) this was the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md).

<a name="matthew_27_55"></a>Matthew 27:55

And [polys](../../strongs/g/g4183.md) [gynē](../../strongs/g/g1135.md) were there [theōreō](../../strongs/g/g2334.md) [apo](../../strongs/g/g575.md) [makrothen](../../strongs/g/g3113.md), which [akoloutheō](../../strongs/g/g190.md) [Iēsous](../../strongs/g/g2424.md) from [Galilaia](../../strongs/g/g1056.md), [diakoneō](../../strongs/g/g1247.md) unto him:

<a name="matthew_27_56"></a>Matthew 27:56

Among which was [Maria](../../strongs/g/g3137.md) [Magdalēnē](../../strongs/g/g3094.md), and [Maria](../../strongs/g/g3137.md) the [mētēr](../../strongs/g/g3384.md) of [Iakōbos](../../strongs/g/g2385.md) and [Iōsēs](../../strongs/g/g2500.md), and the [mētēr](../../strongs/g/g3384.md) of [Zebedaios](../../strongs/g/g2199.md) [huios](../../strongs/g/g5207.md).

<a name="matthew_27_57"></a>Matthew 27:57

When the [opsios](../../strongs/g/g3798.md) was [ginomai](../../strongs/g/g1096.md), there [erchomai](../../strongs/g/g2064.md) a [plousios](../../strongs/g/g4145.md) [anthrōpos](../../strongs/g/g444.md) of [Harimathaia](../../strongs/g/g707.md), [tounoma](../../strongs/g/g5122.md) [Iōsēph](../../strongs/g/g2501.md), who also himself was [Iēsous](../../strongs/g/g2424.md) [mathēteuō](../../strongs/g/g3100.md):

<a name="matthew_27_58"></a>Matthew 27:58

He [proserchomai](../../strongs/g/g4334.md) to [Pilatos](../../strongs/g/g4091.md), and [aiteō](../../strongs/g/g154.md) the [sōma](../../strongs/g/g4983.md) of [Iēsous](../../strongs/g/g2424.md). Then [Pilatos](../../strongs/g/g4091.md) [keleuō](../../strongs/g/g2753.md) the [sōma](../../strongs/g/g4983.md) to be [apodidōmi](../../strongs/g/g591.md).

<a name="matthew_27_59"></a>Matthew 27:59

And when [Iōsēph](../../strongs/g/g2501.md) had [lambanō](../../strongs/g/g2983.md) the [sōma](../../strongs/g/g4983.md), he [entylissō](../../strongs/g/g1794.md) it in a [katharos](../../strongs/g/g2513.md) [sindōn](../../strongs/g/g4616.md),

<a name="matthew_27_60"></a>Matthew 27:60

And [tithēmi](../../strongs/g/g5087.md) it in his own [kainos](../../strongs/g/g2537.md) [mnēmeion](../../strongs/g/g3419.md), which he had [latomeō](../../strongs/g/g2998.md) in the [petra](../../strongs/g/g4073.md): and he [proskyliō](../../strongs/g/g4351.md) a [megas](../../strongs/g/g3173.md) [lithos](../../strongs/g/g3037.md) to the [thyra](../../strongs/g/g2374.md) of the [mnēmeion](../../strongs/g/g3419.md), and [aperchomai](../../strongs/g/g565.md).

<a name="matthew_27_61"></a>Matthew 27:61

And there was [Maria](../../strongs/g/g3137.md) [Magdalēnē](../../strongs/g/g3094.md), and the other [Maria](../../strongs/g/g3137.md), [kathēmai](../../strongs/g/g2521.md) [apenanti](../../strongs/g/g561.md) the [taphos](../../strongs/g/g5028.md).

<a name="matthew_27_62"></a>Matthew 27:62

Now the [epaurion](../../strongs/g/g1887.md), that [meta](../../strongs/g/g3326.md) [paraskeuē](../../strongs/g/g3904.md), the [archiereus](../../strongs/g/g749.md) and [Pharisaios](../../strongs/g/g5330.md) [synagō](../../strongs/g/g4863.md) unto [Pilatos](../../strongs/g/g4091.md),

<a name="matthew_27_63"></a>Matthew 27:63

[legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), we [mnaomai](../../strongs/g/g3415.md) that that [planos](../../strongs/g/g4108.md) [eipon](../../strongs/g/g2036.md), while he was yet [zaō](../../strongs/g/g2198.md), **After three [hēmera](../../strongs/g/g2250.md) I will [egeirō](../../strongs/g/g1453.md).**

<a name="matthew_27_64"></a>Matthew 27:64

[keleuō](../../strongs/g/g2753.md) therefore that the [taphos](../../strongs/g/g5028.md) be [asphalizō](../../strongs/g/g805.md) until the third [hēmera](../../strongs/g/g2250.md), lest his [mathētēs](../../strongs/g/g3101.md) [erchomai](../../strongs/g/g2064.md) by [nyx](../../strongs/g/g3571.md), and [kleptō](../../strongs/g/g2813.md) him, and [eipon](../../strongs/g/g2036.md) unto the [laos](../../strongs/g/g2992.md), He is [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md): so the [eschatos](../../strongs/g/g2078.md) [planē](../../strongs/g/g4106.md) shall be [cheirōn](../../strongs/g/g5501.md) than the [prōtos](../../strongs/g/g4413.md).

<a name="matthew_27_65"></a>Matthew 27:65

[Pilatos](../../strongs/g/g4091.md) [phēmi](../../strongs/g/g5346.md) unto them, Ye have a [koustōdia](../../strongs/g/g2892.md): [hypagō](../../strongs/g/g5217.md), [asphalizō](../../strongs/g/g805.md) as ye [eidō](../../strongs/g/g1492.md).

<a name="matthew_27_66"></a>Matthew 27:66

So they [poreuō](../../strongs/g/g4198.md), and [asphalizō](../../strongs/g/g805.md) the [taphos](../../strongs/g/g5028.md), [sphragizō](../../strongs/g/g4972.md) the [lithos](../../strongs/g/g3037.md), and setting a [koustōdia](../../strongs/g/g2892.md).

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 26](matthew_26.md) - [Matthew 28](matthew_28.md)

---

[^1]: [Matthew 27:4 Commentary](../../commentary/matthew/matthew_27_commentary.md#matthew_27_4)

[^2]: [Matthew 27:9 Commentary](../../commentary/matthew/matthew_27_commentary.md#matthew_27_9)

[^3]: [Matthew 27:16 Commentary](../../commentary/matthew/matthew_27_commentary.md#matthew_27_16)

[^4]: [Psalms 22:18](../psalms/psalms_22.md#psalms_22_18)

[^5]: [Matthew 27:49 Commentary](../../commentary/matthew/matthew_27_commentary.md#matthew_27_49)

[^6]: [Matthew 27:35 Commentary](../../commentary/matthew/matthew_27_commentary.md#matthew_27_35)
