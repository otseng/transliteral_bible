# [Matthew 6](https://www.blueletterbible.org/kjv/mat/6/1/rl1/s_930001)

<a name="matthew_6_1"></a>Matthew 6:1

**[prosechō](../../strongs/g/g4337.md) that ye [poieō](../../strongs/g/g4160.md) not your [eleēmosynē](../../strongs/g/g1654.md) before [anthrōpos](../../strongs/g/g444.md), to be [theaomai](../../strongs/g/g2300.md) of them: otherwise ye have no [misthos](../../strongs/g/g3408.md) of your [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md).** [^1]

<a name="matthew_6_2"></a>Matthew 6:2

**Therefore when thou [poieō](../../strongs/g/g4160.md) [eleēmosynē](../../strongs/g/g1654.md), do not [salpizō](../../strongs/g/g4537.md) before thee, as the [hypokritēs](../../strongs/g/g5273.md) do in the [synagōgē](../../strongs/g/g4864.md) and in the [rhymē](../../strongs/g/g4505.md), that they may have [doxazō](../../strongs/g/g1392.md) of [anthrōpos](../../strongs/g/g444.md). [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, They have their [misthos](../../strongs/g/g3408.md).**

<a name="matthew_6_3"></a>Matthew 6:3

**But when thou [poieō](../../strongs/g/g4160.md) [eleēmosynē](../../strongs/g/g1654.md), let not thy [aristeros](../../strongs/g/g710.md) [ginōskō](../../strongs/g/g1097.md) what thy [dexios](../../strongs/g/g1188.md) [poieō](../../strongs/g/g4160.md):**

<a name="matthew_6_4"></a>Matthew 6:4

**That thine [eleēmosynē](../../strongs/g/g1654.md) may be in [kryptos](../../strongs/g/g2927.md): and thy [patēr](../../strongs/g/g3962.md) which [blepō](../../strongs/g/g991.md) in [kryptos](../../strongs/g/g2927.md) himself shall [apodidōmi](../../strongs/g/g591.md) thee [phaneros](../../strongs/g/g5318.md).** [^2]

<a name="matthew_6_5"></a>Matthew 6:5

**And when thou [proseuchomai](../../strongs/g/g4336.md), thou shalt not be as the [hypokritēs](../../strongs/g/g5273.md): for they [phileō](../../strongs/g/g5368.md) to [proseuchomai](../../strongs/g/g4336.md) [histēmi](../../strongs/g/g2476.md) in the [synagōgē](../../strongs/g/g4864.md) and in the [gōnia](../../strongs/g/g1137.md) of the [plateia](../../strongs/g/g4113.md), that they may be [phainō](../../strongs/g/g5316.md) of [anthrōpos](../../strongs/g/g444.md). [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, They have their [misthos](../../strongs/g/g3408.md).** [^3]

<a name="matthew_6_6"></a>Matthew 6:6

**But thou, when thou [proseuchomai](../../strongs/g/g4336.md), [eiserchomai](../../strongs/g/g1525.md) into thy [tameion](../../strongs/g/g5009.md), and when thou hast [kleiō](../../strongs/g/g2808.md) thy [thyra](../../strongs/g/g2374.md), [proseuchomai](../../strongs/g/g4336.md) to thy [patēr](../../strongs/g/g3962.md) which is in [kryptos](../../strongs/g/g2927.md); and thy [patēr](../../strongs/g/g3962.md) which [blepō](../../strongs/g/g991.md) in [kryptos](../../strongs/g/g2927.md) shall [apodidōmi](../../strongs/g/g591.md) thee [en](../../strongs/g/g1722.md) [phaneros](../../strongs/g/g5318.md).** [^4]

<a name="matthew_6_7"></a>Matthew 6:7

**But when ye [proseuchomai](../../strongs/g/g4336.md), [battalogeō](../../strongs/g/g945.md) not, as the [ethnikos](../../strongs/g/g1482.md): for they [dokeō](../../strongs/g/g1380.md) that they shall be [eisakouō](../../strongs/g/g1522.md) for their [polylogia](../../strongs/g/g4180.md).** [^5]

<a name="matthew_6_8"></a>Matthew 6:8

**[homoioō](../../strongs/g/g3666.md) not therefore unto them: for your [patēr](../../strongs/g/g3962.md) [eidō](../../strongs/g/g1492.md) what things ye have [chreia](../../strongs/g/g5532.md), before ye [aiteō](../../strongs/g/g154.md) him.** [^6]

<a name="matthew_6_9"></a>Matthew 6:9

**[houtō(s)](../../strongs/g/g3779.md) therefore [proseuchomai](../../strongs/g/g4336.md) ye: Our [patēr](../../strongs/g/g3962.md) which art in [ouranos](../../strongs/g/g3772.md), [hagiazō](../../strongs/g/g37.md) thy [onoma](../../strongs/g/g3686.md).** [^7]

<a name="matthew_6_10"></a>Matthew 6:10

**Thy [basileia](../../strongs/g/g932.md) [erchomai](../../strongs/g/g2064.md), Thy [thelēma](../../strongs/g/g2307.md) [ginomai](../../strongs/g/g1096.md) in [gē](../../strongs/g/g1093.md), as in [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_6_11"></a>Matthew 6:11

**[didōmi](../../strongs/g/g1325.md) us [sēmeron](../../strongs/g/g4594.md) our [epiousios](../../strongs/g/g1967.md) [artos](../../strongs/g/g740.md).** [^8]

<a name="matthew_6_12"></a>Matthew 6:12

**And [aphiēmi](../../strongs/g/g863.md) us our [opheilēma](../../strongs/g/g3783.md), as we [aphiēmi](../../strongs/g/g863.md) our [opheiletēs](../../strongs/g/g3781.md).** [^9]

<a name="matthew_6_13"></a>Matthew 6:13

**And [eispherō](../../strongs/g/g1533.md) us not into [peirasmos](../../strongs/g/g3986.md), but [rhyomai](../../strongs/g/g4506.md) us from [ponēros](../../strongs/g/g4190.md): For thine is the [basileia](../../strongs/g/g932.md), and the [dynamis](../../strongs/g/g1411.md), and the [doxa](../../strongs/g/g1391.md), [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).** [^10]

<a name="matthew_6_14"></a>Matthew 6:14

**For if ye [aphiēmi](../../strongs/g/g863.md) [anthrōpos](../../strongs/g/g444.md) their [paraptōma](../../strongs/g/g3900.md), your [ouranios](../../strongs/g/g3770.md) [patēr](../../strongs/g/g3962.md) will also [aphiēmi](../../strongs/g/g863.md) you:** [^11]

<a name="matthew_6_15"></a>Matthew 6:15

**But if ye [aphiēmi](../../strongs/g/g863.md) not [anthrōpos](../../strongs/g/g444.md) their [paraptōma](../../strongs/g/g3900.md), neither will your [patēr](../../strongs/g/g3962.md) [aphiēmi](../../strongs/g/g863.md) your [paraptōma](../../strongs/g/g3900.md).** [^12]

<a name="matthew_6_16"></a>Matthew 6:16

**Moreover when ye [nēsteuō](../../strongs/g/g3522.md), [ginomai](../../strongs/g/g1096.md) not, as the [hypokritēs](../../strongs/g/g5273.md), [skythrōpos](../../strongs/g/g4659.md): for they [aphanizō](../../strongs/g/g853.md) their [prosōpon](../../strongs/g/g4383.md), that they may [phainō](../../strongs/g/g5316.md) unto [anthrōpos](../../strongs/g/g444.md) to [nēsteuō](../../strongs/g/g3522.md). [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, They have their [misthos](../../strongs/g/g3408.md).**

<a name="matthew_6_17"></a>Matthew 6:17

**But thou, when thou [nēsteuō](../../strongs/g/g3522.md), [aleiphō](../../strongs/g/g218.md) thine [kephalē](../../strongs/g/g2776.md), and [niptō](../../strongs/g/g3538.md) thy [prosōpon](../../strongs/g/g4383.md);**

<a name="matthew_6_18"></a>Matthew 6:18

**That thou [phainō](../../strongs/g/g5316.md) not unto [anthrōpos](../../strongs/g/g444.md) to [nēsteuō](../../strongs/g/g3522.md), but unto thy [patēr](../../strongs/g/g3962.md) which is in [kryptos](../../strongs/g/g2927.md): and thy [patēr](../../strongs/g/g3962.md), which [blepō](../../strongs/g/g991.md) in [kryptos](../../strongs/g/g2927.md), shall [apodidōmi](../../strongs/g/g591.md) thee [en](../../strongs/g/g1722.md) [phaneros](../../strongs/g/g5318.md).** [^13]

<a name="matthew_6_19"></a>Matthew 6:19

**[thēsaurizō](../../strongs/g/g2343.md) not for yourselves [thēsauros](../../strongs/g/g2344.md) upon [gē](../../strongs/g/g1093.md), where [sēs](../../strongs/g/g4597.md) and [brōsis](../../strongs/g/g1035.md) [aphanizō](../../strongs/g/g853.md), and where [kleptēs](../../strongs/g/g2812.md) [dioryssō](../../strongs/g/g1358.md) and [kleptō](../../strongs/g/g2813.md):**

<a name="matthew_6_20"></a>Matthew 6:20

**But [thēsaurizō](../../strongs/g/g2343.md) for yourselves [thēsauros](../../strongs/g/g2344.md) in [ouranos](../../strongs/g/g3772.md), where neither [sēs](../../strongs/g/g4597.md) nor [brōsis](../../strongs/g/g1035.md) [aphanizō](../../strongs/g/g853.md), and where [kleptēs](../../strongs/g/g2812.md) do not [dioryssō](../../strongs/g/g1358.md) nor [kleptō](../../strongs/g/g2813.md):** [^14]

<a name="matthew_6_21"></a>Matthew 6:21

**For where your [thēsauros](../../strongs/g/g2344.md) is, there [esomai](../../strongs/g/g2071.md) your [kardia](../../strongs/g/g2588.md) also.**

<a name="matthew_6_22"></a>Matthew 6:22

**The [lychnos](../../strongs/g/g3088.md) of the [sōma](../../strongs/g/g4983.md) is the [ophthalmos](../../strongs/g/g3788.md): if therefore thine [ophthalmos](../../strongs/g/g3788.md) be [haplous](../../strongs/g/g573.md), thy [holos](../../strongs/g/g3650.md) [sōma](../../strongs/g/g4983.md) shall be [phōteinos](../../strongs/g/g5460.md).**

<a name="matthew_6_23"></a>Matthew 6:23

**But if thine [ophthalmos](../../strongs/g/g3788.md) be [ponēros](../../strongs/g/g4190.md), thy [holos](../../strongs/g/g3650.md) [sōma](../../strongs/g/g4983.md) shall be [skoteinos](../../strongs/g/g4652.md). If therefore the [phōs](../../strongs/g/g5457.md) that is in thee be [skotos](../../strongs/g/g4655.md), [posos](../../strongs/g/g4214.md) is that [skotos](../../strongs/g/g4655.md)!**

<a name="matthew_6_24"></a>Matthew 6:24

**[oudeis](../../strongs/g/g3762.md) can [douleuō](../../strongs/g/g1398.md) two [kyrios](../../strongs/g/g2962.md): for either he will [miseō](../../strongs/g/g3404.md) the one, and [agapaō](../../strongs/g/g25.md) the other; or else he will [antechō](../../strongs/g/g472.md) to the one, and [kataphroneō](../../strongs/g/g2706.md) the other. Ye cannot [douleuō](../../strongs/g/g1398.md) [theos](../../strongs/g/g2316.md) and [mamōnas](../../strongs/g/g3126.md).** [^15]

<a name="matthew_6_25"></a>Matthew 6:25

**Therefore I [legō](../../strongs/g/g3004.md) unto you, Take no [merimnaō](../../strongs/g/g3309.md) for your [psychē](../../strongs/g/g5590.md), what ye shall [phago](../../strongs/g/g5315.md), or what ye shall [pinō](../../strongs/g/g4095.md); nor yet for your [sōma](../../strongs/g/g4983.md), what ye shall [endyō](../../strongs/g/g1746.md). Is not the [psychē](../../strongs/g/g5590.md) more than [trophē](../../strongs/g/g5160.md), and the [sōma](../../strongs/g/g4983.md) than [endyma](../../strongs/g/g1742.md)?** [^16]

<a name="matthew_6_26"></a>Matthew 6:26

**[emblepō](../../strongs/g/g1689.md) the [peteinon](../../strongs/g/g4071.md) of [ouranos](../../strongs/g/g3772.md): for they [speirō](../../strongs/g/g4687.md) not, neither do they [therizō](../../strongs/g/g2325.md), nor [synagō](../../strongs/g/g4863.md) into [apothēkē](../../strongs/g/g596.md); yet your [ouranios](../../strongs/g/g3770.md) [patēr](../../strongs/g/g3962.md) [trephō](../../strongs/g/g5142.md) them. Are ye not much [diapherō](../../strongs/g/g1308.md) than they?**

<a name="matthew_6_27"></a>Matthew 6:27

**Which of you by [merimnaō](../../strongs/g/g3309.md) can [prostithēmi](../../strongs/g/g4369.md) one [pēchys](../../strongs/g/g4083.md) unto his [hēlikia](../../strongs/g/g2244.md)?** [^17]

<a name="matthew_6_28"></a>Matthew 6:28

**And why [merimnaō](../../strongs/g/g3309.md) for [endyma](../../strongs/g/g1742.md)? [katamanthanō](../../strongs/g/g2648.md) the [krinon](../../strongs/g/g2918.md) of the [agros](../../strongs/g/g68.md), how they [auxanō](../../strongs/g/g837.md); they [kopiaō](../../strongs/g/g2872.md) not, neither do they [nēthō](../../strongs/g/g3514.md):** [^18]

<a name="matthew_6_29"></a>Matthew 6:29

**And yet I [legō](../../strongs/g/g3004.md) unto you, That even [Solomōn](../../strongs/g/g4672.md) in all his [doxa](../../strongs/g/g1391.md) was not [periballō](../../strongs/g/g4016.md) like one of these.**

<a name="matthew_6_30"></a>Matthew 6:30

**Wherefore, if [theos](../../strongs/g/g2316.md) so [amphiennymi](../../strongs/g/g294.md) the [chortos](../../strongs/g/g5528.md) of the [agros](../../strongs/g/g68.md), which [sēmeron](../../strongs/g/g4594.md) is, and [aurion](../../strongs/g/g839.md) is [ballō](../../strongs/g/g906.md) into the [klibanos](../../strongs/g/g2823.md), not [polys](../../strongs/g/g4183.md) more you, [oligopistos](../../strongs/g/g3640.md)?**

<a name="matthew_6_31"></a>Matthew 6:31

**Therefore take no [merimnaō](../../strongs/g/g3309.md), [legō](../../strongs/g/g3004.md), What shall we [phago](../../strongs/g/g5315.md)? or, What shall we [pinō](../../strongs/g/g4095.md)? or, Wherewithal shall we be [periballō](../../strongs/g/g4016.md)?**

<a name="matthew_6_32"></a>Matthew 6:32

**(For after all these things the [ethnos](../../strongs/g/g1484.md) [epizēteō](../../strongs/g/g1934.md)) for your [ouranios](../../strongs/g/g3770.md) [patēr](../../strongs/g/g3962.md) [eidō](../../strongs/g/g1492.md) that ye have [chrēzō](../../strongs/g/g5535.md) of all [toutōn](../../strongs/g/g5130.md).**

<a name="matthew_6_33"></a>Matthew 6:33

**But [zēteō](../../strongs/g/g2212.md) ye [prōton](../../strongs/g/g4412.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md), and his [dikaiosynē](../../strongs/g/g1343.md); and all [tauta](../../strongs/g/g5023.md) shall be [prostithēmi](../../strongs/g/g4369.md) unto you.** [^19]

<a name="matthew_6_34"></a>Matthew 6:34

**[merimnaō](../../strongs/g/g3309.md) therefore no [merimnaō](../../strongs/g/g3309.md) for [aurion](../../strongs/g/g839.md): for [aurion](../../strongs/g/g839.md) shall [merimnaō](../../strongs/g/g3309.md) for the things [heautou](../../strongs/g/g1438.md). [arketos](../../strongs/g/g713.md) unto [hēmera](../../strongs/g/g2250.md) the [kakia](../../strongs/g/g2549.md) thereof.**

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 5](matthew_5.md) - [Matthew 7](matthew_7.md)

---

[^1]: [Matthew 6:1 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_1)

[^2]: [Matthew 6:4 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_4)

[^3]: [Matthew 6:5 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_5)

[^4]: [Matthew 6:6 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_6)

[^5]: [Matthew 6:7 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_7)

[^6]: [Matthew 6:8 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_8)

[^7]: [Matthew 6:9 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_9)

[^8]: [Matthew 6:11 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_11)

[^9]: [Matthew 6:12 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_12)

[^10]: [Matthew 6:13 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6:13)

[^11]: [Matthew 6:14 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_14)

[^12]: [Matthew 6:15 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_15)

[^13]: [Matthew 6:18 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_18)

[^14]: [Matthew 6:20 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_20)

[^15]: [Matthew 6:24 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_24)

[^16]: [Matthew 6:25 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_25)

[^17]: [Matthew 6:27 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_27)

[^18]: [Matthew 6:28 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_28)

[^19]: [Matthew 6:33 Commentary](../../commentary/matthew/matthew_6_commentary.md#matthew_6_33)
