# [Matthew 7](https://www.blueletterbible.org/kjv/mat/7/1/rl1/s_930001)

<a name="matthew_7_1"></a>Matthew 7:1

**[krinō](../../strongs/g/g2919.md) not, that ye be not [krinō](../../strongs/g/g2919.md).**

<a name="matthew_7_2"></a>Matthew 7:2

**For with what [krima](../../strongs/g/g2917.md) ye [krinō](../../strongs/g/g2919.md), ye shall be [krinō](../../strongs/g/g2919.md): and with what [metron](../../strongs/g/g3358.md) ye [metreō](../../strongs/g/g3354.md), it shall be [antimetreō](../../strongs/g/g488.md) to you.**

<a name="matthew_7_3"></a>Matthew 7:3

**And why [blepō](../../strongs/g/g991.md) thou the [karphos](../../strongs/g/g2595.md) that is in thy [adelphos](../../strongs/g/g80.md) [ophthalmos](../../strongs/g/g3788.md), but [katanoeō](../../strongs/g/g2657.md) not the [dokos](../../strongs/g/g1385.md) that is in thine own [ophthalmos](../../strongs/g/g3788.md)?**

<a name="matthew_7_4"></a>Matthew 7:4

**Or how wilt thou [eipon](../../strongs/g/g2046.md) to thy [adelphos](../../strongs/g/g80.md), [aphiēmi](../../strongs/g/g863.md) me [ekballō](../../strongs/g/g1544.md) the [karphos](../../strongs/g/g2595.md) out of thine [ophthalmos](../../strongs/g/g3788.md); and, [idou](../../strongs/g/g2400.md), a [dokos](../../strongs/g/g1385.md) is in thine own [ophthalmos](../../strongs/g/g3788.md)?**

<a name="matthew_7_5"></a>Matthew 7:5

**Thou [hypokritēs](../../strongs/g/g5273.md), first [ekballō](../../strongs/g/g1544.md) the [dokos](../../strongs/g/g1385.md) out of thine own [ophthalmos](../../strongs/g/g3788.md); and then shalt thou [diablepō](../../strongs/g/g1227.md) to [ekballō](../../strongs/g/g1544.md) the [karphos](../../strongs/g/g2595.md) out of thy [adelphos](../../strongs/g/g80.md) [ophthalmos](../../strongs/g/g3788.md).**

<a name="matthew_7_6"></a>Matthew 7:6

**[didōmi](../../strongs/g/g1325.md) not that which is [hagios](../../strongs/g/g40.md) unto the [kyōn](../../strongs/g/g2965.md), neither [ballō](../../strongs/g/g906.md) ye your [margaritēs](../../strongs/g/g3135.md) before [choiros](../../strongs/g/g5519.md), lest they [katapateō](../../strongs/g/g2662.md) them under their [pous](../../strongs/g/g4228.md), and [strephō](../../strongs/g/g4762.md) and [rhēgnymi](../../strongs/g/g4486.md) you.**

<a name="matthew_7_7"></a>Matthew 7:7

**[aiteō](../../strongs/g/g154.md), and it shall be [didōmi](../../strongs/g/g1325.md) you; [zēteō](../../strongs/g/g2212.md), and ye shall [heuriskō](../../strongs/g/g2147.md); [krouō](../../strongs/g/g2925.md), and it shall be [anoigō](../../strongs/g/g455.md) unto you:**

<a name="matthew_7_8"></a>Matthew 7:8

**For every one that [aiteō](../../strongs/g/g154.md) [lambanō](../../strongs/g/g2983.md); and he that [zēteō](../../strongs/g/g2212.md) [heuriskō](../../strongs/g/g2147.md); and to him that [krouō](../../strongs/g/g2925.md) it shall be [anoigō](../../strongs/g/g455.md).**

<a name="matthew_7_9"></a>Matthew 7:9

**Or what [anthrōpos](../../strongs/g/g444.md) is there of you, whom if his [huios](../../strongs/g/g5207.md) [aiteō](../../strongs/g/g154.md) [artos](../../strongs/g/g740.md), will he [epididōmi](../../strongs/g/g1929.md) him a [lithos](../../strongs/g/g3037.md)?**

<a name="matthew_7_10"></a>Matthew 7:10

**Or if he [aiteō](../../strongs/g/g154.md) an [ichthys](../../strongs/g/g2486.md), will he [epididōmi](../../strongs/g/g1929.md) him a [ophis](../../strongs/g/g3789.md)?**

<a name="matthew_7_11"></a>Matthew 7:11

**If ye then, being [ponēros](../../strongs/g/g4190.md), [eidō](../../strongs/g/g1492.md) how to [didōmi](../../strongs/g/g1325.md) [agathos](../../strongs/g/g18.md) [doma](../../strongs/g/g1390.md) unto your [teknon](../../strongs/g/g5043.md), how much more shall your [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md) [didōmi](../../strongs/g/g1325.md) [agathos](../../strongs/g/g18.md) to them that [aiteō](../../strongs/g/g154.md) him?**

<a name="matthew_7_12"></a>Matthew 7:12

**Therefore all things whatsoever ye would that [anthrōpos](../../strongs/g/g444.md) [poieō](../../strongs/g/g4160.md) to you, [poieō](../../strongs/g/g4160.md) ye even so to them: for this is the [nomos](../../strongs/g/g3551.md) and the [prophētēs](../../strongs/g/g4396.md).**

<a name="matthew_7_13"></a>Matthew 7:13

**[eiserchomai](../../strongs/g/g1525.md) ye in at the [stenos](../../strongs/g/g4728.md) [pylē](../../strongs/g/g4439.md): for [platys](../../strongs/g/g4116.md) is the [pylē](../../strongs/g/g4439.md), and [eurychōros](../../strongs/g/g2149.md) is [hodos](../../strongs/g/g3598.md), that [apagō](../../strongs/g/g520.md) to [apōleia](../../strongs/g/g684.md), and [polys](../../strongs/g/g4183.md) there be which [eiserchomai](../../strongs/g/g1525.md) thereat:** [^1]

<a name="matthew_7_14"></a>Matthew 7:14

**Because [stenos](../../strongs/g/g4728.md) is the [pylē](../../strongs/g/g4439.md), and [thlibō](../../strongs/g/g2346.md) is [hodos](../../strongs/g/g3598.md), which [apagō](../../strongs/g/g520.md) unto [zōē](../../strongs/g/g2222.md), and [oligos](../../strongs/g/g3641.md) there be [heuriskō](../../strongs/g/g2147.md) it.** [^2]

<a name="matthew_7_15"></a>Matthew 7:15

**[prosechō](../../strongs/g/g4337.md) of [pseudoprophētēs](../../strongs/g/g5578.md), which [erchomai](../../strongs/g/g2064.md) to you in [probaton](../../strongs/g/g4263.md) [endyma](../../strongs/g/g1742.md), but [esōthen](../../strongs/g/g2081.md) they are [harpax](../../strongs/g/g727.md) [lykos](../../strongs/g/g3074.md).**

<a name="matthew_7_16"></a>Matthew 7:16

**Ye shall [epiginōskō](../../strongs/g/g1921.md) them by their [karpos](../../strongs/g/g2590.md). Do [syllegō](../../strongs/g/g4816.md) [staphylē](../../strongs/g/g4718.md) of [akantha](../../strongs/g/g173.md), or [sykon](../../strongs/g/g4810.md) of [tribolos](../../strongs/g/g5146.md)?**

<a name="matthew_7_17"></a>Matthew 7:17

**Even so every [agathos](../../strongs/g/g18.md) [dendron](../../strongs/g/g1186.md) [poieō](../../strongs/g/g4160.md) [kalos](../../strongs/g/g2570.md) [karpos](../../strongs/g/g2590.md); but a [sapros](../../strongs/g/g4550.md) [dendron](../../strongs/g/g1186.md) [poieō](../../strongs/g/g4160.md) [ponēros](../../strongs/g/g4190.md) [karpos](../../strongs/g/g2590.md).**

<a name="matthew_7_18"></a>Matthew 7:18

**An [agathos](../../strongs/g/g18.md) [dendron](../../strongs/g/g1186.md) cannot [poieō](../../strongs/g/g4160.md) [ponēros](../../strongs/g/g4190.md) [karpos](../../strongs/g/g2590.md), neither can a [sapros](../../strongs/g/g4550.md) [dendron](../../strongs/g/g1186.md) bring forth [kalos](../../strongs/g/g2570.md) [karpos](../../strongs/g/g2590.md).**

<a name="matthew_7_19"></a>Matthew 7:19

**Every [dendron](../../strongs/g/g1186.md) that [poieō](../../strongs/g/g4160.md) not [kalos](../../strongs/g/g2570.md) [karpos](../../strongs/g/g2590.md) is [ekkoptō](../../strongs/g/g1581.md), and [ballō](../../strongs/g/g906.md) into the [pyr](../../strongs/g/g4442.md).**

<a name="matthew_7_20"></a>Matthew 7:20

**Wherefore by their [karpos](../../strongs/g/g2590.md) ye shall [epiginōskō](../../strongs/g/g1921.md) them.**

<a name="matthew_7_21"></a>Matthew 7:21

**Not every one that [legō](../../strongs/g/g3004.md) unto me, [kyrios](../../strongs/g/g2962.md), [kyrios](../../strongs/g/g2962.md), shall [eiserchomai](../../strongs/g/g1525.md) into the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md); but he that [poieō](../../strongs/g/g4160.md) the [thelēma](../../strongs/g/g2307.md) of my [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md).** [^3]

<a name="matthew_7_22"></a>Matthew 7:22

**Many will [eipon](../../strongs/g/g2046.md) to me in that [hēmera](../../strongs/g/g2250.md), [kyrios](../../strongs/g/g2962.md), [kyrios](../../strongs/g/g2962.md), have we not [prophēteuō](../../strongs/g/g4395.md) in thy [onoma](../../strongs/g/g3686.md)? and in thy [onoma](../../strongs/g/g3686.md) have [ekballō](../../strongs/g/g1544.md) out [daimonion](../../strongs/g/g1140.md)? and in thy [onoma](../../strongs/g/g3686.md) [poieō](../../strongs/g/g4160.md) [polys](../../strongs/g/g4183.md) [dynamis](../../strongs/g/g1411.md)?** [^4]

<a name="matthew_7_23"></a>Matthew 7:23

**And then will I [homologeō](../../strongs/g/g3670.md) unto them, I never [ginōskō](../../strongs/g/g1097.md) you: [apochōreō](../../strongs/g/g672.md) from me, ye that [ergazomai](../../strongs/g/g2038.md) [anomia](../../strongs/g/g458.md).** [^5]

<a name="matthew_7_24"></a>Matthew 7:24

**Therefore whosoever [akouō](../../strongs/g/g191.md) these [logos](../../strongs/g/g3056.md) of mine, and [poieō](../../strongs/g/g4160.md) them, I will [homoioō](../../strongs/g/g3666.md) him unto a [phronimos](../../strongs/g/g5429.md) [anēr](../../strongs/g/g435.md), which [oikodomeō](../../strongs/g/g3618.md) his [oikia](../../strongs/g/g3614.md) upon a [petra](../../strongs/g/g4073.md):** [^6]

<a name="matthew_7_25"></a>Matthew 7:25

**And the [brochē](../../strongs/g/g1028.md) [katabainō](../../strongs/g/g2597.md), and the [potamos](../../strongs/g/g4215.md) [erchomai](../../strongs/g/g2064.md), and the [anemos](../../strongs/g/g417.md) [pneō](../../strongs/g/g4154.md), and [prospiptō](../../strongs/g/g4363.md) that [oikia](../../strongs/g/g3614.md); and it [piptō](../../strongs/g/g4098.md) not: for it was [themelioō](../../strongs/g/g2311.md) upon a [petra](../../strongs/g/g4073.md).**

<a name="matthew_7_26"></a>Matthew 7:26

**And every one that [akouō](../../strongs/g/g191.md) these [logos](../../strongs/g/g3056.md) of mine, and [poieō](../../strongs/g/g4160.md) them not, shall be [homoioō](../../strongs/g/g3666.md) unto a [mōros](../../strongs/g/g3474.md) [anēr](../../strongs/g/g435.md), which [oikodomeō](../../strongs/g/g3618.md) his [oikia](../../strongs/g/g3614.md) upon the [ammos](../../strongs/g/g285.md):**

<a name="matthew_7_27"></a>Matthew 7:27

**And the [brochē](../../strongs/g/g1028.md) [katabainō](../../strongs/g/g2597.md), and the [potamos](../../strongs/g/g4215.md) [erchomai](../../strongs/g/g2064.md), and the [anemos](../../strongs/g/g417.md) [pneō](../../strongs/g/g4154.md), and [proskoptō](../../strongs/g/g4350.md) that [oikia](../../strongs/g/g3614.md); and it [piptō](../../strongs/g/g4098.md): and [megas](../../strongs/g/g3173.md) was the [ptōsis](../../strongs/g/g4431.md) of it.** [^7]

<a name="matthew_7_28"></a>Matthew 7:28

And [ginomai](../../strongs/g/g1096.md), when [Iēsous](../../strongs/g/g2424.md) had [synteleō](../../strongs/g/g4931.md) these [logos](../../strongs/g/g3056.md), the [ochlos](../../strongs/g/g3793.md) were [ekplēssō](../../strongs/g/g1605.md) at his [didachē](../../strongs/g/g1322.md): [^8]

<a name="matthew_7_29"></a>Matthew 7:29

For he [didaskō](../../strongs/g/g1321.md) them as one having [exousia](../../strongs/g/g1849.md), and not as the [grammateus](../../strongs/g/g1122.md). [^9]

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew](matthew_6.md) - [Matthew](matthew_8.md)

---

[^1]: [Matthew 7:13 Commentary](../../commentary/matthew/matthew_7_commentary.md#matthew_7_13)

[^2]: [Matthew 7:14 Commentary](../../commentary/matthew/matthew_7_commentary.md#matthew_7_14)

[^3]: [Matthew 7:21 Commentary](../../commentary/matthew/matthew_7_commentary.md#matthew_7_21)

[^4]: [Matthew 7:22 Commentary](../../commentary/matthew/matthew_7_commentary.md#matthew_7_22)

[^5]: [Matthew 7:23 Commentary](../../commentary/matthew/matthew_7_commentary.md#matthew_7_23)

[^6]: [Matthew 7:24 Commentary](../../commentary/matthew/matthew_7_commentary.md#matthew_7_24)

[^7]: [Matthew 7:27 Commentary](../../commentary/matthew/matthew_7_commentary.md#matthew_7_27)

[^8]: [Matthew 7:28 Commentary](../../commentary/matthew/matthew_7_commentary.md#matthew_7_28)

[^9]: [Matthew 7:29 Commentary](../../commentary/matthew/matthew_7_commentary.md#matthew_7_29)
