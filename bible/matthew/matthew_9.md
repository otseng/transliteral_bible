# [Matthew 9](https://www.blueletterbible.org/kjv/mat/9/1/rl1/s_938001)

<a name="matthew_9_1"></a>Matthew 9:1

And he [embainō](../../strongs/g/g1684.md) into a [ploion](../../strongs/g/g4143.md), and [diaperaō](../../strongs/g/g1276.md), and [erchomai](../../strongs/g/g2064.md) into his own [polis](../../strongs/g/g4172.md). [^1]

<a name="matthew_9_2"></a>Matthew 9:2

And, [idou](../../strongs/g/g2400.md), they [prospherō](../../strongs/g/g4374.md) to him a [paralytikos](../../strongs/g/g3885.md), [ballō](../../strongs/g/g906.md) on a [klinē](../../strongs/g/g2825.md): and [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) their [pistis](../../strongs/g/g4102.md) [eipon](../../strongs/g/g2036.md) unto the [paralytikos](../../strongs/g/g3885.md); **[teknon](../../strongs/g/g5043.md), [tharseō](../../strongs/g/g2293.md); thy [hamartia](../../strongs/g/g266.md) be [aphiēmi](../../strongs/g/g863.md) thee.** [^2]

<a name="matthew_9_3"></a>Matthew 9:3

And, [idou](../../strongs/g/g2400.md), certain of the [grammateus](../../strongs/g/g1122.md) [eipon](../../strongs/g/g2036.md) within themselves, This [blasphēmeō](../../strongs/g/g987.md).

<a name="matthew_9_4"></a>Matthew 9:4

And [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) their [enthymēsis](../../strongs/g/g1761.md) [eipon](../../strongs/g/g2036.md), **Wherefore [enthymeomai](../../strongs/g/g1760.md) ye [ponēros](../../strongs/g/g4190.md) in your [kardia](../../strongs/g/g2588.md)?** [^3]

<a name="matthew_9_5"></a>Matthew 9:5

**For whether is [eukopos](../../strongs/g/g2123.md), to [eipon](../../strongs/g/g2036.md), [hamartia](../../strongs/g/g266.md) be [aphiēmi](../../strongs/g/g863.md) thee; or to [eipon](../../strongs/g/g2036.md), [egeirō](../../strongs/g/g1453.md), and [peripateō](../../strongs/g/g4043.md)?**

<a name="matthew_9_6"></a>Matthew 9:6

**But that ye may [eidō](../../strongs/g/g1492.md) that the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) hath [exousia](../../strongs/g/g1849.md) on [gē](../../strongs/g/g1093.md) to [aphiēmi](../../strongs/g/g863.md) [hamartia](../../strongs/g/g266.md),** (then [legō](../../strongs/g/g3004.md) he to [paralytikos](../../strongs/g/g3885.md),) **[egeirō](../../strongs/g/g1453.md), [airō](../../strongs/g/g142.md) thy [klinē](../../strongs/g/g2825.md), and [hypagō](../../strongs/g/g5217.md) unto thine [oikos](../../strongs/g/g3624.md).**

<a name="matthew_9_7"></a>Matthew 9:7

And he [egeirō](../../strongs/g/g1453.md), and [aperchomai](../../strongs/g/g565.md) to his [oikos](../../strongs/g/g3624.md).

<a name="matthew_9_8"></a>Matthew 9:8

But when the [ochlos](../../strongs/g/g3793.md) [eidō](../../strongs/g/g1492.md), they [thaumazō](../../strongs/g/g2296.md), and [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md), which had [didōmi](../../strongs/g/g1325.md) such [exousia](../../strongs/g/g1849.md) unto [anthrōpos](../../strongs/g/g444.md). [^4]

<a name="matthew_9_9"></a>Matthew 9:9

And as [Iēsous](../../strongs/g/g2424.md) [paragō](../../strongs/g/g3855.md) from thence, he [eidō](../../strongs/g/g1492.md) an [anthrōpos](../../strongs/g/g444.md), named [Maththaios](../../strongs/g/g3156.md), [kathēmai](../../strongs/g/g2521.md) at the [telōnion](../../strongs/g/g5058.md): and he [legō](../../strongs/g/g3004.md) unto him, **[akoloutheō](../../strongs/g/g190.md) me.** And he [anistēmi](../../strongs/g/g450.md), and [akoloutheō](../../strongs/g/g190.md) him. [^5]

<a name="matthew_9_10"></a>Matthew 9:10

And [ginomai](../../strongs/g/g1096.md), as [Iēsous](../../strongs/g/g2424.md) [anakeimai](../../strongs/g/g345.md) in the [oikia](../../strongs/g/g3614.md), [idou](../../strongs/g/g2400.md), [polys](../../strongs/g/g4183.md) [telōnēs](../../strongs/g/g5057.md) and [hamartōlos](../../strongs/g/g268.md) [erchomai](../../strongs/g/g2064.md) and [synanakeimai](../../strongs/g/g4873.md) with him and his [mathētēs](../../strongs/g/g3101.md).

<a name="matthew_9_11"></a>Matthew 9:11

And when the [Pharisaios](../../strongs/g/g5330.md) [eidō](../../strongs/g/g1492.md), they [eipon](../../strongs/g/g2036.md) unto his [mathētēs](../../strongs/g/g3101.md), Why [esthiō](../../strongs/g/g2068.md) your [didaskalos](../../strongs/g/g1320.md) with [telōnēs](../../strongs/g/g5057.md) and [hamartōlos](../../strongs/g/g268.md)? [^6]

<a name="matthew_9_12"></a>Matthew 9:12

But when [Iēsous](../../strongs/g/g2424.md) [akouō](../../strongs/g/g191.md), he [eipon](../../strongs/g/g2036.md) unto them, **They that be [ischyō](../../strongs/g/g2480.md) [chreia](../../strongs/g/g5532.md) not an [iatros](../../strongs/g/g2395.md), but they that are [kakōs](../../strongs/g/g2560.md).** [^7]

<a name="matthew_9_13"></a>Matthew 9:13

**But [poreuō](../../strongs/g/g4198.md) and [manthanō](../../strongs/g/g3129.md) what meaneth, I will have [eleos](../../strongs/g/g1656.md), and not [thysia](../../strongs/g/g2378.md): for I am not [erchomai](../../strongs/g/g2064.md) to [kaleō](../../strongs/g/g2564.md) the [dikaios](../../strongs/g/g1342.md), but [hamartōlos](../../strongs/g/g268.md) to [metanoia](../../strongs/g/g3341.md).** [^8]

<a name="matthew_9_14"></a>Matthew 9:14

Then [proserchomai](../../strongs/g/g4334.md) to him the [mathētēs](../../strongs/g/g3101.md) of [Iōannēs](../../strongs/g/g2491.md), [legō](../../strongs/g/g3004.md), Why do we and the [Pharisaios](../../strongs/g/g5330.md) [nēsteuō](../../strongs/g/g3522.md) [polys](../../strongs/g/g4183.md), but thy [mathētēs](../../strongs/g/g3101.md) [nēsteuō](../../strongs/g/g3522.md) not? [^9]

<a name="matthew_9_15"></a>Matthew 9:15

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **Can the [huios](../../strongs/g/g5207.md) of [nymphōn](../../strongs/g/g3567.md) [pentheō](../../strongs/g/g3996.md), as long as the [nymphios](../../strongs/g/g3566.md) is with them? but the [hēmera](../../strongs/g/g2250.md) will [erchomai](../../strongs/g/g2064.md), when the [nymphios](../../strongs/g/g3566.md) shall be [apairō](../../strongs/g/g522.md) from them, and then shall they [nēsteuō](../../strongs/g/g3522.md).** [^10]

<a name="matthew_9_16"></a>Matthew 9:16

**[oudeis](../../strongs/g/g3762.md) [epiballō](../../strongs/g/g1911.md) [epiblēma](../../strongs/g/g1915.md) of [agnaphos](../../strongs/g/g46.md) [rhakos](../../strongs/g/g4470.md) unto a [palaios](../../strongs/g/g3820.md) [himation](../../strongs/g/g2440.md), for [plērōma](../../strongs/g/g4138.md) [airō](../../strongs/g/g142.md) from the [himation](../../strongs/g/g2440.md), and the [schisma](../../strongs/g/g4978.md) is [ginomai](../../strongs/g/g1096.md) [cheirōn](../../strongs/g/g5501.md).**

<a name="matthew_9_17"></a>Matthew 9:17

**[oude](../../strongs/g/g3761.md) [ballō](../../strongs/g/g906.md) [neos](../../strongs/g/g3501.md) [oinos](../../strongs/g/g3631.md) into [palaios](../../strongs/g/g3820.md) [askos](../../strongs/g/g779.md): else the [askos](../../strongs/g/g779.md) [rhēgnymi](../../strongs/g/g4486.md), and the [oinos](../../strongs/g/g3631.md) [ekcheō](../../strongs/g/g1632.md), and the [askos](../../strongs/g/g779.md) [apollymi](../../strongs/g/g622.md): but they put [neos](../../strongs/g/g3501.md) [oinos](../../strongs/g/g3631.md) into [kainos](../../strongs/g/g2537.md) [askos](../../strongs/g/g779.md), and [amphoteroi](../../strongs/g/g297.md) are [syntēreō](../../strongs/g/g4933.md).**

<a name="matthew_9_18"></a>Matthew 9:18

While he [laleō](../../strongs/g/g2980.md) these things unto them, [idou](../../strongs/g/g2400.md), there came a certain [archōn](../../strongs/g/g758.md), and [proskyneō](../../strongs/g/g4352.md) him, [legō](../../strongs/g/g3004.md), My [thygatēr](../../strongs/g/g2364.md) is even now [teleutaō](../../strongs/g/g5053.md): but [erchomai](../../strongs/g/g2064.md) and [epitithēmi](../../strongs/g/g2007.md) thy [cheir](../../strongs/g/g5495.md) upon her, and she shall [zaō](../../strongs/g/g2198.md). [^11]

<a name="matthew_9_19"></a>Matthew 9:19

And [Iēsous](../../strongs/g/g2424.md) [egeirō](../../strongs/g/g1453.md), and [akoloutheō](../../strongs/g/g190.md) him, and so did his [mathētēs](../../strongs/g/g3101.md). [^12]

<a name="matthew_9_20"></a>Matthew 9:20

And, [idou](../../strongs/g/g2400.md), a [gynē](../../strongs/g/g1135.md), [haimorroeō](../../strongs/g/g131.md) twelve [etos](../../strongs/g/g2094.md), [proserchomai](../../strongs/g/g4334.md) [opisthen](../../strongs/g/g3693.md), and [haptomai](../../strongs/g/g680.md) the [kraspedon](../../strongs/g/g2899.md) of his [himation](../../strongs/g/g2440.md):

<a name="matthew_9_21"></a>Matthew 9:21

For she [legō](../../strongs/g/g3004.md) within herself, If I may but [haptomai](../../strongs/g/g680.md) his [himation](../../strongs/g/g2440.md), I shall be [sōzō](../../strongs/g/g4982.md). [^13]

<a name="matthew_9_22"></a>Matthew 9:22

But [Iēsous](../../strongs/g/g2424.md) [epistrephō](../../strongs/g/g1994.md), and when he [eidō](../../strongs/g/g1492.md) her, he [eipon](../../strongs/g/g2036.md), **[thygatēr](../../strongs/g/g2364.md), [tharseō](../../strongs/g/g2293.md); thy [pistis](../../strongs/g/g4102.md) hath [sōzō](../../strongs/g/g4982.md) thee.** And the [gynē](../../strongs/g/g1135.md) was [sōzō](../../strongs/g/g4982.md) from that [hōra](../../strongs/g/g5610.md). [^14]

<a name="matthew_9_23"></a>Matthew 9:23

And when [Iēsous](../../strongs/g/g2424.md) [erchomai](../../strongs/g/g2064.md) into the [archōn](../../strongs/g/g758.md) [oikia](../../strongs/g/g3614.md), and [eidō](../../strongs/g/g1492.md) the [aulētēs](../../strongs/g/g834.md) and the [ochlos](../../strongs/g/g3793.md) [thorybeō](../../strongs/g/g2350.md),

<a name="matthew_9_24"></a>Matthew 9:24

He [legō](../../strongs/g/g3004.md) unto them, **[anachōreō](../../strongs/g/g402.md): for the [korasion](../../strongs/g/g2877.md) [apothnēskō](../../strongs/g/g599.md) not, but [katheudō](../../strongs/g/g2518.md).** And they [katagelaō](../../strongs/g/g2606.md) him. [^15]

<a name="matthew_9_25"></a>Matthew 9:25

But when the [ochlos](../../strongs/g/g3793.md) [ekballō](../../strongs/g/g1544.md), he [eiserchomai](../../strongs/g/g1525.md), and [krateō](../../strongs/g/g2902.md) her by the [cheir](../../strongs/g/g5495.md), and the [korasion](../../strongs/g/g2877.md) [egeirō](../../strongs/g/g1453.md). [^16]

<a name="matthew_9_26"></a>Matthew 9:26

And the [phēmē](../../strongs/g/g5345.md) hereof [exerchomai](../../strongs/g/g1831.md) into all that [gē](../../strongs/g/g1093.md). [^17]

<a name="matthew_9_27"></a>Matthew 9:27

And when [Iēsous](../../strongs/g/g2424.md) [paragō](../../strongs/g/g3855.md) thence, two [typhlos](../../strongs/g/g5185.md) [akoloutheō](../../strongs/g/g190.md) him, [krazō](../../strongs/g/g2896.md), and [legō](../../strongs/g/g3004.md), [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md), [eleeō](../../strongs/g/g1653.md) on us.

<a name="matthew_9_28"></a>Matthew 9:28

And when he was [erchomai](../../strongs/g/g2064.md) into the [oikia](../../strongs/g/g3614.md), the [typhlos](../../strongs/g/g5185.md) [proserchomai](../../strongs/g/g4334.md) to him: and [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, **[pisteuō](../../strongs/g/g4100.md) ye that I am able [poieō](../../strongs/g/g4160.md) this?** They [legō](../../strongs/g/g3004.md) unto him, [nai](../../strongs/g/g3483.md), [kyrios](../../strongs/g/g2962.md). [^18]

<a name="matthew_9_29"></a>Matthew 9:29

Then [haptomai](../../strongs/g/g680.md) their [ophthalmos](../../strongs/g/g3788.md), [legō](../../strongs/g/g3004.md), **According to your [pistis](../../strongs/g/g4102.md) be it unto you.**

<a name="matthew_9_30"></a>Matthew 9:30

And their [ophthalmos](../../strongs/g/g3788.md) were [anoigō](../../strongs/g/g455.md); and [Iēsous](../../strongs/g/g2424.md) [embrimaomai](../../strongs/g/g1690.md) them, [legō](../../strongs/g/g3004.md), **[horaō](../../strongs/g/g3708.md) that [mēdeis](../../strongs/g/g3367.md) [ginōskō](../../strongs/g/g1097.md) it.**

<a name="matthew_9_31"></a>Matthew 9:31

But they, [exerchomai](../../strongs/g/g1831.md), [diaphēmizō](../../strongs/g/g1310.md) in all that [gē](../../strongs/g/g1093.md).

<a name="matthew_9_32"></a>Matthew 9:32

As they [exerchomai](../../strongs/g/g1831.md), [idou](../../strongs/g/g2400.md), they [prospherō](../../strongs/g/g4374.md) to him a [kōphos](../../strongs/g/g2974.md) [anthrōpos](../../strongs/g/g444.md) [daimonizomai](../../strongs/g/g1139.md). [^19]

<a name="matthew_9_33"></a>Matthew 9:33

And when the [daimonion](../../strongs/g/g1140.md) was [ekballō](../../strongs/g/g1544.md), the [kōphos](../../strongs/g/g2974.md) [laleō](../../strongs/g/g2980.md): and the [ochlos](../../strongs/g/g3793.md) [thaumazō](../../strongs/g/g2296.md), [legō](../../strongs/g/g3004.md), It was never so [phainō](../../strongs/g/g5316.md) in [Israēl](../../strongs/g/g2474.md).

<a name="matthew_9_34"></a>Matthew 9:34

But the [Pharisaios](../../strongs/g/g5330.md) [legō](../../strongs/g/g3004.md), He [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md) through the [archōn](../../strongs/g/g758.md) of the [daimonion](../../strongs/g/g1140.md). [^20]

<a name="matthew_9_35"></a>Matthew 9:35

And [Iēsous](../../strongs/g/g2424.md) [periagō](../../strongs/g/g4013.md) all the [polis](../../strongs/g/g4172.md) and [kōmē](../../strongs/g/g2968.md), [didaskō](../../strongs/g/g1321.md) in their [synagōgē](../../strongs/g/g4864.md), and [kēryssō](../../strongs/g/g2784.md) the [euaggelion](../../strongs/g/g2098.md) of the [basileia](../../strongs/g/g932.md), and [therapeuō](../../strongs/g/g2323.md) every [nosos](../../strongs/g/g3554.md) and every [malakia](../../strongs/g/g3119.md) among the [laos](../../strongs/g/g2992.md). [^21]

<a name="matthew_9_36"></a>Matthew 9:36

But when he [eidō](../../strongs/g/g1492.md) the [ochlos](../../strongs/g/g3793.md), he was [splagchnizomai](../../strongs/g/g4697.md) on them, because they [eklyō](../../strongs/g/g1590.md), and were [rhiptō](../../strongs/g/g4496.md), as [probaton](../../strongs/g/g4263.md) having no [poimēn](../../strongs/g/g4166.md). [^22]

<a name="matthew_9_37"></a>Matthew 9:37

Then [legō](../../strongs/g/g3004.md) he unto his [mathētēs](../../strongs/g/g3101.md), **The [therismos](../../strongs/g/g2326.md) [men](../../strongs/g/g3303.md) [polys](../../strongs/g/g4183.md), but the [ergatēs](../../strongs/g/g2040.md) [oligos](../../strongs/g/g3641.md);**

<a name="matthew_9_38"></a>Matthew 9:38

**[deomai](../../strongs/g/g1189.md) therefore the [kyrios](../../strongs/g/g2962.md) of the [therismos](../../strongs/g/g2326.md), that he will [ekballō](../../strongs/g/g1544.md) [ergatēs](../../strongs/g/g2040.md) into his [therismos](../../strongs/g/g2326.md).**

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 8](matthew_8.md) - [Matthew 10](matthew_10.md)

---

[^1]: [Matthew 9:1 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_1)

[^2]: [Matthew 9:2 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_2)

[^3]: [Matthew 9:4 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_4)

[^4]: [Matthew 9:8 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_8)

[^5]: [Matthew 9:9 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_9)

[^6]: [Matthew 9:11 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_11)

[^7]: [Matthew 9:12 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_12)

[^8]: [Matthew 9:13 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_13)

[^9]: [Matthew 9:14 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_14)

[^10]: [Matthew 9:15 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_15)

[^11]: [Matthew 9:18 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_18)

[^12]: [Matthew 9:19 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_19)

[^13]: [Matthew 9:21 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_21)

[^14]: [Matthew 9:22 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_22)

[^15]: [Matthew 9:24 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_24)

[^16]: [Matthew 9:25 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_25)

[^17]: [Matthew 9:26 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_26)

[^18]: [Matthew 9:28 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_28)

[^19]: [Matthew 9:32 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_32)

[^20]: [Matthew 9:34 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_34)

[^21]: [Matthew 9:35 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_35)

[^22]: [Matthew 9:36 Commentary](../../commentary/matthew/matthew_9_commentary.md#matthew_9_36)
