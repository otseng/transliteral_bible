# [Matthew 13](https://www.blueletterbible.org/kjv/mat/13/1/rl1/s_942001)

<a name="matthew_13_1"></a>Matthew 13:1

The same [hēmera](../../strongs/g/g2250.md) [exerchomai](../../strongs/g/g1831.md) [Iēsous](../../strongs/g/g2424.md) out of the [oikia](../../strongs/g/g3614.md), and [kathēmai](../../strongs/g/g2521.md) by the [thalassa](../../strongs/g/g2281.md). [^1]

<a name="matthew_13_2"></a>Matthew 13:2

And [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) were [synagō](../../strongs/g/g4863.md) unto him, so that he [embainō](../../strongs/g/g1684.md) into a [ploion](../../strongs/g/g4143.md), and [kathēmai](../../strongs/g/g2521.md); and the [pas](../../strongs/g/g3956.md) [ochlos](../../strongs/g/g3793.md) [histēmi](../../strongs/g/g2476.md) on the [aigialos](../../strongs/g/g123.md).

<a name="matthew_13_3"></a>Matthew 13:3

And he [laleō](../../strongs/g/g2980.md) [polys](../../strongs/g/g4183.md) unto them in [parabolē](../../strongs/g/g3850.md), [legō](../../strongs/g/g3004.md), **[idou](../../strongs/g/g2400.md), a [speirō](../../strongs/g/g4687.md) [exerchomai](../../strongs/g/g1831.md) to [speirō](../../strongs/g/g4687.md);**

<a name="matthew_13_4"></a>Matthew 13:4

**And when he [speirō](../../strongs/g/g4687.md), some [piptō](../../strongs/g/g4098.md) by the [hodos](../../strongs/g/g3598.md), and the [peteinon](../../strongs/g/g4071.md) [erchomai](../../strongs/g/g2064.md) and [katesthiō](../../strongs/g/g2719.md) them:** [^2]

<a name="matthew_13_5"></a>Matthew 13:5

**Some [piptō](../../strongs/g/g4098.md) upon [petrōdēs](../../strongs/g/g4075.md), where they had not [polys](../../strongs/g/g4183.md) [gē](../../strongs/g/g1093.md): and [eutheōs](../../strongs/g/g2112.md) they [exanatellō](../../strongs/g/g1816.md), because they had no [bathos](../../strongs/g/g899.md) of [gē](../../strongs/g/g1093.md):**

<a name="matthew_13_6"></a>Matthew 13:6

**And when the [hēlios](../../strongs/g/g2246.md) was [anatellō](../../strongs/g/g393.md), they were [kaumatizō](../../strongs/g/g2739.md); and because they had no [rhiza](../../strongs/g/g4491.md), they [xērainō](../../strongs/g/g3583.md).** [^3]

<a name="matthew_13_7"></a>Matthew 13:7

**And some [piptō](../../strongs/g/g4098.md) among [akantha](../../strongs/g/g173.md); and the [akantha](../../strongs/g/g173.md) [anabainō](../../strongs/g/g305.md), and [apopnigō](../../strongs/g/g638.md) them:**

<a name="matthew_13_8"></a>Matthew 13:8

**But other [piptō](../../strongs/g/g4098.md) into [kalos](../../strongs/g/g2570.md) [gē](../../strongs/g/g1093.md), and [didōmi](../../strongs/g/g1325.md) [karpos](../../strongs/g/g2590.md), some an hundredfold, some sixtyfold, some thirtyfold.**

<a name="matthew_13_9"></a>Matthew 13:9

**Who hath [ous](../../strongs/g/g3775.md) to [akouō](../../strongs/g/g191.md), let him [akouō](../../strongs/g/g191.md).** [^4]

<a name="matthew_13_10"></a>Matthew 13:10

And the [mathētēs](../../strongs/g/g3101.md) [proserchomai](../../strongs/g/g4334.md), and [eipon](../../strongs/g/g2036.md) unto him, Why [laleō](../../strongs/g/g2980.md) thou unto them in [parabolē](../../strongs/g/g3850.md)?

<a name="matthew_13_11"></a>Matthew 13:11

He [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **Because it is [didōmi](../../strongs/g/g1325.md) unto you to [ginōskō](../../strongs/g/g1097.md) the [mystērion](../../strongs/g/g3466.md) of the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md), but to them it is not [didōmi](../../strongs/g/g1325.md).** [^5]

<a name="matthew_13_12"></a>Matthew 13:12

**For whosoever hath, to him shall be [didōmi](../../strongs/g/g1325.md), and he shall [perisseuō](../../strongs/g/g4052.md): but whosoever hath not, from him shall be [airō](../../strongs/g/g142.md) even that he hath.**

<a name="matthew_13_13"></a>Matthew 13:13

**Therefore [laleō](../../strongs/g/g2980.md) I to them in [parabolē](../../strongs/g/g3850.md): because they [blepō](../../strongs/g/g991.md) [blepō](../../strongs/g/g991.md) not; and [akouō](../../strongs/g/g191.md) they [akouō](../../strongs/g/g191.md) not, neither do they [syniēmi](../../strongs/g/g4920.md).** [^6]

<a name="matthew_13_14"></a>Matthew 13:14

**And in them is [anaplēroō](../../strongs/g/g378.md) the [prophēteia](../../strongs/g/g4394.md) of [Ēsaïas](../../strongs/g/g2268.md), which [legō](../../strongs/g/g3004.md), By [akoē](../../strongs/g/g189.md) ye shall [akouō](../../strongs/g/g191.md), and shall not [syniēmi](../../strongs/g/g4920.md); and [blepō](../../strongs/g/g991.md) ye shall [blepō](../../strongs/g/g991.md), and shall not [eidō](../../strongs/g/g1492.md):** [^7]

<a name="matthew_13_15"></a>Matthew 13:15

**For this [laos](../../strongs/g/g2992.md) [kardia](../../strongs/g/g2588.md) is [pachynō](../../strongs/g/g3975.md), and [ous](../../strongs/g/g3775.md) are [bareōs](../../strongs/g/g917.md) of [akouō](../../strongs/g/g191.md), and their [ophthalmos](../../strongs/g/g3788.md) they have [kammyō](../../strongs/g/g2576.md); lest at any time they should [eidō](../../strongs/g/g1492.md) with [ophthalmos](../../strongs/g/g3788.md) and [akouō](../../strongs/g/g191.md) with [ous](../../strongs/g/g3775.md), and should [syniēmi](../../strongs/g/g4920.md) with [kardia](../../strongs/g/g2588.md), and should be [epistrephō](../../strongs/g/g1994.md), and I should [iaomai](../../strongs/g/g2390.md) them.**

<a name="matthew_13_16"></a>Matthew 13:16

**But [makarios](../../strongs/g/g3107.md) your [ophthalmos](../../strongs/g/g3788.md), for they [blepō](../../strongs/g/g991.md): and your [ous](../../strongs/g/g3775.md), for they [akouō](../../strongs/g/g191.md).**

<a name="matthew_13_17"></a>Matthew 13:17

**For [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, That [polys](../../strongs/g/g4183.md) [prophētēs](../../strongs/g/g4396.md) and [dikaios](../../strongs/g/g1342.md) have [epithymeō](../../strongs/g/g1937.md) to [eidō](../../strongs/g/g1492.md) which ye [blepō](../../strongs/g/g991.md), and have not [eidō](../../strongs/g/g1492.md); and to [akouō](../../strongs/g/g191.md) which ye [akouō](../../strongs/g/g191.md), and have not [akouō](../../strongs/g/g191.md).** [^8]

<a name="matthew_13_18"></a>Matthew 13:18

**[akouō](../../strongs/g/g191.md) ye therefore the [parabolē](../../strongs/g/g3850.md) of the [speirō](../../strongs/g/g4687.md).**

<a name="matthew_13_19"></a>Matthew 13:19

**When any one [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md) of the [basileia](../../strongs/g/g932.md), and [syniēmi](../../strongs/g/g4920.md) not, then [erchomai](../../strongs/g/g2064.md) the [ponēros](../../strongs/g/g4190.md), and [harpazō](../../strongs/g/g726.md) that which was [speirō](../../strongs/g/g4687.md) in his [kardia](../../strongs/g/g2588.md). This is he [speirō](../../strongs/g/g4687.md) by [hodos](../../strongs/g/g3598.md).** [^9]

<a name="matthew_13_20"></a>Matthew 13:20

**But he that [speirō](../../strongs/g/g4687.md) into [petrōdēs](../../strongs/g/g4075.md), the same is he that [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md), and [euthys](../../strongs/g/g2117.md) with [chara](../../strongs/g/g5479.md) [lambanō](../../strongs/g/g2983.md) it;**

<a name="matthew_13_21"></a>Matthew 13:21

**Yet hath he not [rhiza](../../strongs/g/g4491.md) in himself, but dureth [proskairos](../../strongs/g/g4340.md): for when [thlipsis](../../strongs/g/g2347.md) or [diōgmos](../../strongs/g/g1375.md) ariseth because of the [logos](../../strongs/g/g3056.md), [euthys](../../strongs/g/g2117.md) he is [skandalizō](../../strongs/g/g4624.md).** [^10]

<a name="matthew_13_22"></a>Matthew 13:22

**He also that [speirō](../../strongs/g/g4687.md) among the [akantha](../../strongs/g/g173.md) is he that [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md); and the [merimna](../../strongs/g/g3308.md) of this [aiōn](../../strongs/g/g165.md), and the [apatē](../../strongs/g/g539.md) of [ploutos](../../strongs/g/g4149.md), [sympnigō](../../strongs/g/g4846.md) the [logos](../../strongs/g/g3056.md), and he becometh [akarpos](../../strongs/g/g175.md).** [^11]

<a name="matthew_13_23"></a>Matthew 13:23

**But he that [speirō](../../strongs/g/g4687.md) into the [kalos](../../strongs/g/g2570.md) [gē](../../strongs/g/g1093.md) is he that [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md), and [syniēmi](../../strongs/g/g4920.md); which also [karpophoreō](../../strongs/g/g2592.md), and [poieō](../../strongs/g/g4160.md), some an hundredfold, some sixty, some thirty.** [^12]

<a name="matthew_13_24"></a>Matthew 13:24

Another [parabolē](../../strongs/g/g3850.md) he [paratithēmi](../../strongs/g/g3908.md) unto them, [legō](../../strongs/g/g3004.md), **The [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) [homoioō](../../strongs/g/g3666.md) unto an [anthrōpos](../../strongs/g/g444.md) which [speirō](../../strongs/g/g4687.md) [kalos](../../strongs/g/g2570.md) [sperma](../../strongs/g/g4690.md) in his [agros](../../strongs/g/g68.md):**

<a name="matthew_13_25"></a>Matthew 13:25

**But while [anthrōpos](../../strongs/g/g444.md) [katheudō](../../strongs/g/g2518.md), his [echthros](../../strongs/g/g2190.md) [erchomai](../../strongs/g/g2064.md) and [speirō](../../strongs/g/g4687.md) [zizanion](../../strongs/g/g2215.md) [ana](../../strongs/g/g303.md) [mesos](../../strongs/g/g3319.md) the [sitos](../../strongs/g/g4621.md), and [aperchomai](../../strongs/g/g565.md).**

<a name="matthew_13_26"></a>Matthew 13:26

**But when the [chortos](../../strongs/g/g5528.md) was [blastanō](../../strongs/g/g985.md), and [poieō](../../strongs/g/g4160.md) [karpos](../../strongs/g/g2590.md), then [phainō](../../strongs/g/g5316.md) the [zizanion](../../strongs/g/g2215.md) also.**

<a name="matthew_13_27"></a>Matthew 13:27

**So the [doulos](../../strongs/g/g1401.md) of the [oikodespotēs](../../strongs/g/g3617.md) [proserchomai](../../strongs/g/g4334.md) and [eipon](../../strongs/g/g2036.md) unto him, [kyrios](../../strongs/g/g2962.md), didst not thou [speirō](../../strongs/g/g4687.md) [kalos](../../strongs/g/g2570.md) [sperma](../../strongs/g/g4690.md) in thy [agros](../../strongs/g/g68.md)? from whence then hath it [zizanion](../../strongs/g/g2215.md)?**

<a name="matthew_13_28"></a>Matthew 13:28

**He [phēmi](../../strongs/g/g5346.md) unto them, An [echthros](../../strongs/g/g2190.md) [anthrōpos](../../strongs/g/g444.md) hath [poieō](../../strongs/g/g4160.md) this. The [doulos](../../strongs/g/g1401.md) [eipon](../../strongs/g/g2036.md) unto him, Wilt thou then that we [aperchomai](../../strongs/g/g565.md) and [syllegō](../../strongs/g/g4816.md) them?** [^13]

<a name="matthew_13_29"></a>Matthew 13:29

**But he [phēmi](../../strongs/g/g5346.md), [ou](../../strongs/g/g3756.md); lest while ye [syllegō](../../strongs/g/g4816.md) the [zizanion](../../strongs/g/g2215.md), ye [ekrizoō](../../strongs/g/g1610.md) also the [sitos](../../strongs/g/g4621.md) with them.**

<a name="matthew_13_30"></a>Matthew 13:30

**[aphiēmi](../../strongs/g/g863.md) [amphoteroi](../../strongs/g/g297.md) [synauxanō](../../strongs/g/g4885.md) until the [therismos](../../strongs/g/g2326.md): and in the [kairos](../../strongs/g/g2540.md) of [therismos](../../strongs/g/g2326.md) I will [eipon](../../strongs/g/g2046.md) to the [theristēs](../../strongs/g/g2327.md), [syllegō](../../strongs/g/g4816.md) first the [zizanion](../../strongs/g/g2215.md), and [deō](../../strongs/g/g1210.md) them in [desmē](../../strongs/g/g1197.md) to [katakaiō](../../strongs/g/g2618.md) them: but [synagō](../../strongs/g/g4863.md) the [sitos](../../strongs/g/g4621.md) into my [apothēkē](../../strongs/g/g596.md).**

<a name="matthew_13_31"></a>Matthew 13:31

Another [parabolē](../../strongs/g/g3850.md) he [paratithēmi](../../strongs/g/g3908.md) unto them, [legō](../../strongs/g/g3004.md), **The [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) is [homoios](../../strongs/g/g3664.md) to a [kokkos](../../strongs/g/g2848.md) of [sinapi](../../strongs/g/g4615.md), which an [anthrōpos](../../strongs/g/g444.md) [lambanō](../../strongs/g/g2983.md), and [speirō](../../strongs/g/g4687.md) in his [agros](../../strongs/g/g68.md):** [^14]

<a name="matthew_13_32"></a>Matthew 13:32

**Which indeed is the [mikros](../../strongs/g/g3398.md) of all [sperma](../../strongs/g/g4690.md): but when it [auxanō](../../strongs/g/g837.md), it is the [meizōn](../../strongs/g/g3187.md) [lachanon](../../strongs/g/g3001.md), and becometh a [dendron](../../strongs/g/g1186.md), so that the [peteinon](../../strongs/g/g4071.md) of [ouranos](../../strongs/g/g3772.md) [erchomai](../../strongs/g/g2064.md) and [kataskēnoō](../../strongs/g/g2681.md) in the [klados](../../strongs/g/g2798.md) thereof.** [^15]

<a name="matthew_13_33"></a>Matthew 13:33

Another [parabolē](../../strongs/g/g3850.md) [laleō](../../strongs/g/g2980.md) he unto them; **The [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) is [homoios](../../strongs/g/g3664.md) unto [zymē](../../strongs/g/g2219.md), which a [gynē](../../strongs/g/g1135.md) [lambanō](../../strongs/g/g2983.md), and [egkryptō](../../strongs/g/g1470.md) in three [saton](../../strongs/g/g4568.md) of [aleuron](../../strongs/g/g224.md), till the [holos](../../strongs/g/g3650.md) was [zymoō](../../strongs/g/g2220.md).** [^16]

<a name="matthew_13_34"></a>Matthew 13:34

All these things [laleō](../../strongs/g/g2980.md) [Iēsous](../../strongs/g/g2424.md) unto the [ochlos](../../strongs/g/g3793.md) in [parabolē](../../strongs/g/g3850.md); and without a [parabolē](../../strongs/g/g3850.md) [laleō](../../strongs/g/g2980.md) he not unto them:

<a name="matthew_13_35"></a>Matthew 13:35

That it might be [plēroō](../../strongs/g/g4137.md) which was [rheō](../../strongs/g/g4483.md) by the [prophētēs](../../strongs/g/g4396.md), [legō](../../strongs/g/g3004.md), I will [anoigō](../../strongs/g/g455.md) my [stoma](../../strongs/g/g4750.md) in [parabolē](../../strongs/g/g3850.md); I will [ereugomai](../../strongs/g/g2044.md) [kryptō](../../strongs/g/g2928.md) from the [katabolē](../../strongs/g/g2602.md) of the [kosmos](../../strongs/g/g2889.md). [^17]

<a name="matthew_13_36"></a>Matthew 13:36

Then [Iēsous](../../strongs/g/g2424.md) [aphiēmi](../../strongs/g/g863.md) the [ochlos](../../strongs/g/g3793.md), and [erchomai](../../strongs/g/g2064.md) into the [oikia](../../strongs/g/g3614.md): and his [mathētēs](../../strongs/g/g3101.md) [proserchomai](../../strongs/g/g4334.md) unto him, [legō](../../strongs/g/g3004.md), [phrazō](../../strongs/g/g5419.md) unto us the [parabolē](../../strongs/g/g3850.md) of the [zizanion](../../strongs/g/g2215.md) of the [agros](../../strongs/g/g68.md). [^18]

<a name="matthew_13_37"></a>Matthew 13:37

He [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **He that [speirō](../../strongs/g/g4687.md) the [kalos](../../strongs/g/g2570.md) [sperma](../../strongs/g/g4690.md) is the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md);**

<a name="matthew_13_38"></a>Matthew 13:38

**The [agros](../../strongs/g/g68.md) is the [kosmos](../../strongs/g/g2889.md); the [kalos](../../strongs/g/g2570.md) [sperma](../../strongs/g/g4690.md) are the [huios](../../strongs/g/g5207.md) of the [basileia](../../strongs/g/g932.md); but the [zizanion](../../strongs/g/g2215.md) are the [huios](../../strongs/g/g5207.md) of the [ponēros](../../strongs/g/g4190.md);**

<a name="matthew_13_39"></a>Matthew 13:39

**The [echthros](../../strongs/g/g2190.md) that [speirō](../../strongs/g/g4687.md) them is the [diabolos](../../strongs/g/g1228.md); the [therismos](../../strongs/g/g2326.md) is the [synteleia](../../strongs/g/g4930.md) of the [aiōn](../../strongs/g/g165.md); and the [theristēs](../../strongs/g/g2327.md) are the [aggelos](../../strongs/g/g32.md).**

<a name="matthew_13_40"></a>Matthew 13:40

**As therefore the [zizanion](../../strongs/g/g2215.md) are [syllegō](../../strongs/g/g4816.md) and [katakaiō](../../strongs/g/g2618.md) in the [pyr](../../strongs/g/g4442.md); so shall it be in the [synteleia](../../strongs/g/g4930.md) of this [aiōn](../../strongs/g/g165.md).** [^19]

<a name="matthew_13_41"></a>Matthew 13:41

**The [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) shall [apostellō](../../strongs/g/g649.md) his [aggelos](../../strongs/g/g32.md), and they shall [syllegō](../../strongs/g/g4816.md) out of his [basileia](../../strongs/g/g932.md) all things [skandalon](../../strongs/g/g4625.md), and them which [poieō](../../strongs/g/g4160.md) [anomia](../../strongs/g/g458.md);**

<a name="matthew_13_42"></a>Matthew 13:42

**And shall [ballō](../../strongs/g/g906.md) them into a [kaminos](../../strongs/g/g2575.md) of [pyr](../../strongs/g/g4442.md): there shall be [klauthmos](../../strongs/g/g2805.md) and [brygmos](../../strongs/g/g1030.md) of [odous](../../strongs/g/g3599.md).**

<a name="matthew_13_43"></a>Matthew 13:43

**Then shall the [dikaios](../../strongs/g/g1342.md) [eklampō](../../strongs/g/g1584.md) as the [hēlios](../../strongs/g/g2246.md) in the [basileia](../../strongs/g/g932.md) of their [patēr](../../strongs/g/g3962.md). Who hath [ous](../../strongs/g/g3775.md) to [akouō](../../strongs/g/g191.md), let him [akouō](../../strongs/g/g191.md).** [^20]

<a name="matthew_13_44"></a>Matthew 13:44

**Again, the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) is [homoios](../../strongs/g/g3664.md) unto [thēsauros](../../strongs/g/g2344.md) [kryptō](../../strongs/g/g2928.md) in an [agros](../../strongs/g/g68.md); the which when an [anthrōpos](../../strongs/g/g444.md) hath [heuriskō](../../strongs/g/g2147.md), he [kryptō](../../strongs/g/g2928.md), and for [chara](../../strongs/g/g5479.md) thereof [hypagō](../../strongs/g/g5217.md) and [pōleō](../../strongs/g/g4453.md) all that he hath, and [agorazō](../../strongs/g/g59.md) that [agros](../../strongs/g/g68.md).** [^21]

<a name="matthew_13_45"></a>Matthew 13:45

**Again, the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) is [homoios](../../strongs/g/g3664.md) unto an [emporos](../../strongs/g/g1713.md) [anthrōpos](../../strongs/g/g444.md), [zēteō](../../strongs/g/g2212.md) [kalos](../../strongs/g/g2570.md) [margaritēs](../../strongs/g/g3135.md):** [^22]

<a name="matthew_13_46"></a>Matthew 13:46

**Who, when he had [heuriskō](../../strongs/g/g2147.md) one [margaritēs](../../strongs/g/g3135.md) of [polytimos](../../strongs/g/g4186.md), [aperchomai](../../strongs/g/g565.md) and [pipraskō](../../strongs/g/g4097.md) all that he had, and [agorazō](../../strongs/g/g59.md) it.** [^23]

<a name="matthew_13_47"></a>Matthew 13:47

**Again, the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) is [homoios](../../strongs/g/g3664.md) unto a [sagēnē](../../strongs/g/g4522.md), that was [ballō](../../strongs/g/g906.md) into the [thalassa](../../strongs/g/g2281.md), and [synagō](../../strongs/g/g4863.md) of every [genos](../../strongs/g/g1085.md):**

<a name="matthew_13_48"></a>Matthew 13:48

**Which, when it was [plēroō](../../strongs/g/g4137.md), they [anabibazō](../../strongs/g/g307.md) to [aigialos](../../strongs/g/g123.md), and [kathizō](../../strongs/g/g2523.md), and [syllegō](../../strongs/g/g4816.md) the [kalos](../../strongs/g/g2570.md) into [aggeion](../../strongs/g/g30.md), but [ballō](../../strongs/g/g906.md) the [sapros](../../strongs/g/g4550.md) away.**

<a name="matthew_13_49"></a>Matthew 13:49

**So shall it be at the [synteleia](../../strongs/g/g4930.md) of the [aiōn](../../strongs/g/g165.md): the [aggelos](../../strongs/g/g32.md) shall [exerchomai](../../strongs/g/g1831.md), and [aphorizō](../../strongs/g/g873.md) the [ponēros](../../strongs/g/g4190.md) from [mesos](../../strongs/g/g3319.md) the [dikaios](../../strongs/g/g1342.md),**

<a name="matthew_13_50"></a>Matthew 13:50

**And shall [ballō](../../strongs/g/g906.md) them into the [kaminos](../../strongs/g/g2575.md) of [pyr](../../strongs/g/g4442.md): there shall be [klauthmos](../../strongs/g/g2805.md) and [brygmos](../../strongs/g/g1030.md) of [odous](../../strongs/g/g3599.md).**

<a name="matthew_13_51"></a>Matthew 13:51

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, **Have ye [syniēmi](../../strongs/g/g4920.md) all these things?** They [legō](../../strongs/g/g3004.md) unto him, [nai](../../strongs/g/g3483.md), [kyrios](../../strongs/g/g2962.md). [^24]

<a name="matthew_13_52"></a>Matthew 13:52

Then [eipon](../../strongs/g/g2036.md) he unto them, **Therefore every [grammateus](../../strongs/g/g1122.md) [mathēteuō](../../strongs/g/g3100.md) unto the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) is [homoios](../../strongs/g/g3664.md) unto an [anthrōpos](../../strongs/g/g444.md) [oikodespotēs](../../strongs/g/g3617.md), which [ekballō](../../strongs/g/g1544.md) out of his [thēsauros](../../strongs/g/g2344.md) [kainos](../../strongs/g/g2537.md) and [palaios](../../strongs/g/g3820.md).**

<a name="matthew_13_53"></a>Matthew 13:53

And [ginomai](../../strongs/g/g1096.md), when [Iēsous](../../strongs/g/g2424.md) had [teleō](../../strongs/g/g5055.md) these [parabolē](../../strongs/g/g3850.md), he [metairō](../../strongs/g/g3332.md) thence.

<a name="matthew_13_54"></a>Matthew 13:54

And when he was [erchomai](../../strongs/g/g2064.md) into his own [patris](../../strongs/g/g3968.md), he [didaskō](../../strongs/g/g1321.md) them in their [synagōgē](../../strongs/g/g4864.md), insomuch that they were [ekplēssō](../../strongs/g/g1605.md), and [legō](../../strongs/g/g3004.md), Whence hath this this [sophia](../../strongs/g/g4678.md), and [dynamis](../../strongs/g/g1411.md)? [^25]

<a name="matthew_13_55"></a>Matthew 13:55

Is not this the [tektōn](../../strongs/g/g5045.md) [huios](../../strongs/g/g5207.md)? is not his [mētēr](../../strongs/g/g3384.md) [legō](../../strongs/g/g3004.md) [Maria](../../strongs/g/g3137.md)? and his [adelphos](../../strongs/g/g80.md), [Iakōbos](../../strongs/g/g2385.md), and [Iōsēs](../../strongs/g/g2500.md), and [Simōn](../../strongs/g/g4613.md), and [Ioudas](../../strongs/g/g2455.md)? [^26]

<a name="matthew_13_56"></a>Matthew 13:56

And his [adelphē](../../strongs/g/g79.md), are they not all with us? Whence then hath this all these things?

<a name="matthew_13_57"></a>Matthew 13:57

And they were [skandalizō](../../strongs/g/g4624.md) in him. But [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **A [prophētēs](../../strongs/g/g4396.md) is not [atimos](../../strongs/g/g820.md), save in his own [patris](../../strongs/g/g3968.md), and in his own [oikia](../../strongs/g/g3614.md).** [^27]

<a name="matthew_13_58"></a>Matthew 13:58

And he [poieō](../../strongs/g/g4160.md) not [polys](../../strongs/g/g4183.md) [dynamis](../../strongs/g/g1411.md) there because of their [apistia](../../strongs/g/g570.md).

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 12](matthew_12.md) - [Matthew 14](matthew_14.md)

---

[^1]: [Matthew 13:1 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_1)

[^2]: [Matthew 13:4 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_4)

[^3]: [Matthew 13:6 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_6)

[^4]: [Matthew 13:9 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_9)

[^5]: [Matthew 13:11 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_11)

[^6]: [Matthew 13:13 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_13)

[^7]: [Matthew 13:14 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_14)

[^8]: [Matthew 13:17 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_17)

[^9]: [Matthew 13:19 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_19)

[^10]: [Matthew 13:21 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_21)

[^11]: [Matthew 13:22 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_22)

[^12]: [Matthew 13:23 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_23)

[^13]: [Matthew 13:28 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_28)

[^14]: [Matthew 13:31 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_31)

[^15]: [Matthew 13:32 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_32)

[^16]: [Matthew 13:33 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_33)

[^17]: [Matthew 13:35 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_35)

[^18]: [Matthew 13:36 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_36)

[^19]: [Matthew 13:40 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_40)

[^20]: [Matthew 13:43 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_43)

[^21]: [Matthew 13:44 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_44)

[^22]: [Matthew 13:45 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_45)

[^23]: [Matthew 13:46 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_46)

[^24]: [Matthew 13:51 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_51)

[^25]: [Matthew 13:54 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_54)

[^26]: [Matthew 13:55 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_55)

[^27]: [Matthew 13:57 Commentary](../../commentary/matthew/matthew_13_commentary.md#matthew_13_57)
