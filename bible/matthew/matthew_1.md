# [Matthew 1](https://www.blueletterbible.org/kjv/mat/1/1/rl1/s_930001)

<a name="matthew_1_1"></a>Matthew 1:1

The [biblos](../../strongs/g/g976.md) of the [genesis](../../strongs/g/g1078.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), the [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md), the [huios](../../strongs/g/g5207.md) of [Abraam](../../strongs/g/g11.md).

<a name="matthew_1_2"></a>Matthew 1:2

[Abraam](../../strongs/g/g11.md) [gennaō](../../strongs/g/g1080.md) [Isaak](../../strongs/g/g2464.md); and [Isaak](../../strongs/g/g2464.md) [gennaō](../../strongs/g/g1080.md) [Iakōb](../../strongs/g/g2384.md); and [Iakōb](../../strongs/g/g2384.md) [gennaō](../../strongs/g/g1080.md) [Ioudas](../../strongs/g/g2455.md) and his [adelphos](../../strongs/g/g80.md);

<a name="matthew_1_3"></a>Matthew 1:3

And [Ioudas](../../strongs/g/g2455.md) [gennaō](../../strongs/g/g1080.md) [Phares](../../strongs/g/g5329.md) and [Zara](../../strongs/g/g2196.md) of [Thamar](../../strongs/g/g2283.md); and [Phares](../../strongs/g/g5329.md) [gennaō](../../strongs/g/g1080.md) [Hesrōm](../../strongs/g/g2074.md); and [Hesrōm](../../strongs/g/g2074.md) [gennaō](../../strongs/g/g1080.md) [Aram](../../strongs/g/g689.md); [^1]

<a name="matthew_1_4"></a>Matthew 1:4

And [Aram](../../strongs/g/g689.md) [gennaō](../../strongs/g/g1080.md) [Aminadab](../../strongs/g/g284.md); and [Aminadab](../../strongs/g/g284.md) [gennaō](../../strongs/g/g1080.md) [Naassōn](../../strongs/g/g3476.md); and [Naassōn](../../strongs/g/g3476.md) [gennaō](../../strongs/g/g1080.md) [Salmōn](../../strongs/g/g4533.md);

<a name="matthew_1_5"></a>Matthew 1:5

And [Salmōn](../../strongs/g/g4533.md) [gennaō](../../strongs/g/g1080.md) [Boes](../../strongs/g/g1003.md) of [Rhachab](../../strongs/g/g4477.md); and [Boes](../../strongs/g/g1003.md) [gennaō](../../strongs/g/g1080.md) [Iōbēd](../../strongs/g/g5601.md) of [Rhouth](../../strongs/g/g4503.md); and [Iōbēd](../../strongs/g/g5601.md) [gennaō](../../strongs/g/g1080.md) [Iessai](../../strongs/g/g2421.md);

<a name="matthew_1_6"></a>Matthew 1:6

And [Iessai](../../strongs/g/g2421.md) [gennaō](../../strongs/g/g1080.md) [Dabid](../../strongs/g/g1138.md) the [basileus](../../strongs/g/g935.md); and [Dabid](../../strongs/g/g1138.md) the [basileus](../../strongs/g/g935.md) [gennaō](../../strongs/g/g1080.md) [Solomōn](../../strongs/g/g4672.md) of her of [ourias](../../strongs/g/g3774.md); [^2]

<a name="matthew_1_7"></a>Matthew 1:7

And [Solomōn](../../strongs/g/g4672.md) [gennaō](../../strongs/g/g1080.md) [Rhoboam](../../strongs/g/g4497.md); and [Rhoboam](../../strongs/g/g4497.md) [gennaō](../../strongs/g/g1080.md) [Abia](../../strongs/g/g7.md); and [Abia](../../strongs/g/g7.md) [gennaō](../../strongs/g/g1080.md) [Asa](../../strongs/g/g760.md);

<a name="matthew_1_8"></a>Matthew 1:8

And [Asa](../../strongs/g/g760.md) [gennaō](../../strongs/g/g1080.md) [Iōsaphat](../../strongs/g/g2498.md); and [Iōsaphat](../../strongs/g/g2498.md) [gennaō](../../strongs/g/g1080.md) [Iōram](../../strongs/g/g2496.md); and [Iōram](../../strongs/g/g2496.md) [gennaō](../../strongs/g/g1080.md) [Ozias](../../strongs/g/g3604.md);

<a name="matthew_1_9"></a>Matthew 1:9

And [Ozias](../../strongs/g/g3604.md) [gennaō](../../strongs/g/g1080.md) [Iōatham](../../strongs/g/g2488.md); and [Iōatham](../../strongs/g/g2488.md) [gennaō](../../strongs/g/g1080.md) [Achaz](../../strongs/g/g881.md); and [Achaz](../../strongs/g/g881.md) [gennaō](../../strongs/g/g1080.md) [Hezekias](../../strongs/g/g1478.md);

<a name="matthew_1_10"></a>Matthew 1:10

And [Hezekias](../../strongs/g/g1478.md) [gennaō](../../strongs/g/g1080.md) [Manassēs](../../strongs/g/g3128.md); and [Manassēs](../../strongs/g/g3128.md) [gennaō](../../strongs/g/g1080.md) [Amōn](../../strongs/g/g300.md); and [Amōn](../../strongs/g/g300.md) [gennaō](../../strongs/g/g1080.md) [Iōsias](../../strongs/g/g2502.md);

<a name="matthew_1_11"></a>Matthew 1:11

And [Iōsias](../../strongs/g/g2502.md) [gennaō](../../strongs/g/g1080.md) [Iechonias](../../strongs/g/g2423.md) and his [adelphos](../../strongs/g/g80.md), about the time they were [metoikesia](../../strongs/g/g3350.md) to [Babylōn](../../strongs/g/g897.md):

<a name="matthew_1_12"></a>Matthew 1:12

And after they were [metoikesia](../../strongs/g/g3350.md) to [Babylōn](../../strongs/g/g897.md), [Iechonias](../../strongs/g/g2423.md) [gennaō](../../strongs/g/g1080.md) [Salathiēl](../../strongs/g/g4528.md); and [Salathiēl](../../strongs/g/g4528.md) [gennaō](../../strongs/g/g1080.md) [Zorobabel](../../strongs/g/g2216.md);

<a name="matthew_1_13"></a>Matthew 1:13

And [Zorobabel](../../strongs/g/g2216.md) [gennaō](../../strongs/g/g1080.md) [Abioud](../../strongs/g/g10.md); and [Abioud](../../strongs/g/g10.md) [gennaō](../../strongs/g/g1080.md) [Eliakeim](../../strongs/g/g1662.md); and [Eliakeim](../../strongs/g/g1662.md) [gennaō](../../strongs/g/g1080.md) [Azōr](../../strongs/g/g107.md);

<a name="matthew_1_14"></a>Matthew 1:14

And [Azōr](../../strongs/g/g107.md) [gennaō](../../strongs/g/g1080.md) [Sadōk](../../strongs/g/g4524.md); and [Sadōk](../../strongs/g/g4524.md) [gennaō](../../strongs/g/g1080.md) [Acheim](../../strongs/g/g885.md); and [Acheim](../../strongs/g/g885.md) [gennaō](../../strongs/g/g1080.md) [Elioud](../../strongs/g/g1664.md);

<a name="matthew_1_15"></a>Matthew 1:15

And [Elioud](../../strongs/g/g1664.md) [gennaō](../../strongs/g/g1080.md) [Eleazar](../../strongs/g/g1648.md); and [Eleazar](../../strongs/g/g1648.md) [gennaō](../../strongs/g/g1080.md) [Matthan](../../strongs/g/g3157.md); and [Matthan](../../strongs/g/g3157.md) [gennaō](../../strongs/g/g1080.md) [Iakōb](../../strongs/g/g2384.md);

<a name="matthew_1_16"></a>Matthew 1:16

And [Iakōb](../../strongs/g/g2384.md) [gennaō](../../strongs/g/g1080.md) [Iōsēph](../../strongs/g/g2501.md) the [anēr](../../strongs/g/g435.md) of [Maria](../../strongs/g/g3137.md), of whom was [gennaō](../../strongs/g/g1080.md) [Iēsous](../../strongs/g/g2424.md), who is [legō](../../strongs/g/g3004.md) [Christos](../../strongs/g/g5547.md). [^3]

<a name="matthew_1_17"></a>Matthew 1:17

So all the [genea](../../strongs/g/g1074.md) from [Abraam](../../strongs/g/g11.md) to [Dabid](../../strongs/g/g1138.md) are fourteen [genea](../../strongs/g/g1074.md); and from [Dabid](../../strongs/g/g1138.md) until the [metoikesia](../../strongs/g/g3350.md) into [Babylōn](../../strongs/g/g897.md) are fourteen [genea](../../strongs/g/g1074.md); and from the [metoikesia](../../strongs/g/g3350.md) into [Babylōn](../../strongs/g/g897.md) unto [Christos](../../strongs/g/g5547.md) are fourteen [genea](../../strongs/g/g1074.md).

<a name="matthew_1_18"></a>Matthew 1:18

Now the [gennēsis](../../strongs/g/g1083.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) was [houtō(s)](../../strongs/g/g3779.md): When as his [mētēr](../../strongs/g/g3384.md)  [Maria](../../strongs/g/g3137.md) was [mnēsteuō](../../strongs/g/g3423.md) to [Iōsēph](../../strongs/g/g2501.md), before they [synerchomai](../../strongs/g/g4905.md), she was [heuriskō](../../strongs/g/g2147.md) [en](../../strongs/g/g1722.md) [gastēr](../../strongs/g/g1064.md) [echō](../../strongs/g/g2192.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md). [^4]

<a name="matthew_1_19"></a>Matthew 1:19

Then [Iōsēph](../../strongs/g/g2501.md) her [anēr](../../strongs/g/g435.md), being a [dikaios](../../strongs/g/g1342.md), and not willing to [paradeigmatizō](../../strongs/g/g3856.md) her, was [boulomai](../../strongs/g/g1014.md) to [apolyō](../../strongs/g/g630.md) her [lathra](../../strongs/g/g2977.md).

<a name="matthew_1_20"></a>Matthew 1:20

But while he [enthymeomai](../../strongs/g/g1760.md) these things, [idou](../../strongs/g/g2400.md), the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) [phainō](../../strongs/g/g5316.md) unto him in a [onar](../../strongs/g/g3677.md), [legō](../../strongs/g/g3004.md), [Iōsēph](../../strongs/g/g2501.md), thou [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md), [phobeō](../../strongs/g/g5399.md) not to [paralambanō](../../strongs/g/g3880.md) unto thee [Maria](../../strongs/g/g3137.md) thy [gynē](../../strongs/g/g1135.md): for that which is [gennaō](../../strongs/g/g1080.md) in her is of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="matthew_1_21"></a>Matthew 1:21

And she shall [tiktō](../../strongs/g/g5088.md) a [huios](../../strongs/g/g5207.md), and thou shalt [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md) [Iēsous](../../strongs/g/g2424.md): for he shall [sōzō](../../strongs/g/g4982.md) his [laos](../../strongs/g/g2992.md) from their [hamartia](../../strongs/g/g266.md).

<a name="matthew_1_22"></a>Matthew 1:22

Now all this was [ginomai](../../strongs/g/g1096.md), that it might [plēroō](../../strongs/g/g4137.md) which was [rheō](../../strongs/g/g4483.md) of the [kyrios](../../strongs/g/g2962.md) by the [prophētēs](../../strongs/g/g4396.md), [legō](../../strongs/g/g3004.md),

<a name="matthew_1_23"></a>Matthew 1:23

[idou](../../strongs/g/g2400.md), a [parthenos](../../strongs/g/g3933.md) [en](../../strongs/g/g1722.md) [gastēr](../../strongs/g/g1064.md) [echō](../../strongs/g/g2192.md), and shall [tiktō](../../strongs/g/g5088.md) a [huios](../../strongs/g/g5207.md), and they shall [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md) [emmanouēl](../../strongs/g/g1694.md), which [methermēneuō](../../strongs/g/g3177.md) is, [theos](../../strongs/g/g2316.md) with us. [^7]

<a name="matthew_1_24"></a>Matthew 1:24

Then [Iōsēph](../../strongs/g/g2501.md) [diegeirō](../../strongs/g/g1326.md) from [hypnos](../../strongs/g/g5258.md) [poieō](../../strongs/g/g4160.md) as the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) had [prostassō](../../strongs/g/g4367.md) him, and [paralambanō](../../strongs/g/g3880.md) unto him his [gynē](../../strongs/g/g1135.md): [^8]

<a name="matthew_1_25"></a>Matthew 1:25

And [ginōskō](../../strongs/g/g1097.md) her not till she had [tiktō](../../strongs/g/g5088.md) her [prōtotokos](../../strongs/g/g4416.md) [huios](../../strongs/g/g5207.md): and he [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md) [Iēsous](../../strongs/g/g2424.md). [^9]

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 2](matthew_2.md)

---

[^1]: [Matthew 1:3 Commentary](../../commentary/matthew/matthew_1_commentary.md#matthew_1_3)

[^2]: [Matthew 1:6 Commentary](../../commentary/matthew/matthew_1_commentary.md#matthew_1_6)

[^3]: [Matthew 1:16 Commentary](../../commentary/matthew/matthew_1_commentary.md#matthew_1_16)

[^4]: [Matthew 1:18 Commentary](../../commentary/matthew/matthew_1_commentary.md#matthew_1_18)

[^5]: [Matthew 1:21 Commentary](../../commentary/matthew/matthew_1_commentary.md#matthew_1_21)

[^6]: [Matthew 1:22 Commentary](../../commentary/matthew/matthew_1_commentary.md#matthew_1_22)

[^7]: [Matthew 1:23 Commentary](../../commentary/matthew/matthew_1_commentary.md#matthew_1_23)

[^8]: [Matthew 1:24 Commentary](../../commentary/matthew/matthew_1_commentary.md#matthew_1_24)

[^9]: [Matthew 1:25 Commentary](../../commentary/matthew/matthew_1_commentary.md#matthew_1_25)
