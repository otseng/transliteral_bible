# [Matthew](https://www.blueletterbible.org/kjv/mat/20/1/rl1/s_949001)

<a name="matthew_20_1"></a>Matthew 20:1

**For the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) is [homoios](../../strongs/g/g3664.md) unto an [anthrōpos](../../strongs/g/g444.md) [oikodespotēs](../../strongs/g/g3617.md), which [exerchomai](../../strongs/g/g1831.md) [hama](../../strongs/g/g260.md) [prōï](../../strongs/g/g4404.md) to [misthoō](../../strongs/g/g3409.md) [ergatēs](../../strongs/g/g2040.md) into his [ampelōn](../../strongs/g/g290.md).**

<a name="matthew_20_2"></a>Matthew 20:2

**And when he had [symphōneō](../../strongs/g/g4856.md) with the [ergatēs](../../strongs/g/g2040.md) for a [dēnarion](../../strongs/g/g1220.md) a [hēmera](../../strongs/g/g2250.md), he [apostellō](../../strongs/g/g649.md) them into his [ampelōn](../../strongs/g/g290.md).**

<a name="matthew_20_3"></a>Matthew 20:3

**And he [exerchomai](../../strongs/g/g1831.md) about the third [hōra](../../strongs/g/g5610.md), and [eidō](../../strongs/g/g1492.md) others [histēmi](../../strongs/g/g2476.md) [argos](../../strongs/g/g692.md) in the [agora](../../strongs/g/g58.md),**

<a name="matthew_20_4"></a>Matthew 20:4

**And [eipon](../../strongs/g/g2036.md) unto them; [hypagō](../../strongs/g/g5217.md) ye also into the [ampelōn](../../strongs/g/g290.md), and whatsoever is [dikaios](../../strongs/g/g1342.md) I will [didōmi](../../strongs/g/g1325.md) you. And they [aperchomai](../../strongs/g/g565.md).**

<a name="matthew_20_5"></a>Matthew 20:5

**Again he [exerchomai](../../strongs/g/g1831.md) about the sixth and ninth [hōra](../../strongs/g/g5610.md), and [poieō](../../strongs/g/g4160.md) [hōsautōs](../../strongs/g/g5615.md).**

<a name="matthew_20_6"></a>Matthew 20:6

**And about the eleventh [hōra](../../strongs/g/g5610.md) he [exerchomai](../../strongs/g/g1831.md), and [heuriskō](../../strongs/g/g2147.md) others [histēmi](../../strongs/g/g2476.md) [argos](../../strongs/g/g692.md), and [legō](../../strongs/g/g3004.md) unto them, Why [histēmi](../../strongs/g/g2476.md) ye here all the [hēmera](../../strongs/g/g2250.md) [argos](../../strongs/g/g692.md)?**

<a name="matthew_20_7"></a>Matthew 20:7

**They [legō](../../strongs/g/g3004.md) unto him, Because [oudeis](../../strongs/g/g3762.md) hath [misthoō](../../strongs/g/g3409.md) us. He [legō](../../strongs/g/g3004.md) unto them, [hypagō](../../strongs/g/g5217.md) ye also into the [ampelōn](../../strongs/g/g290.md); and whatsoever is [dikaios](../../strongs/g/g1342.md), shall ye [lambanō](../../strongs/g/g2983.md).**

<a name="matthew_20_8"></a>Matthew 20:8

**So when [opsios](../../strongs/g/g3798.md) was [ginomai](../../strongs/g/g1096.md), the [kyrios](../../strongs/g/g2962.md) of the [ampelōn](../../strongs/g/g290.md) [legō](../../strongs/g/g3004.md) unto his [epitropos](../../strongs/g/g2012.md), [kaleō](../../strongs/g/g2564.md) the [ergatēs](../../strongs/g/g2040.md), and [apodidōmi](../../strongs/g/g591.md) them [misthos](../../strongs/g/g3408.md), [archomai](../../strongs/g/g756.md) from the [eschatos](../../strongs/g/g2078.md) unto the [prōtos](../../strongs/g/g4413.md).**

<a name="matthew_20_9"></a>Matthew 20:9

**And when they [erchomai](../../strongs/g/g2064.md) that about the eleventh [hōra](../../strongs/g/g5610.md), they [lambanō](../../strongs/g/g2983.md) [ana](../../strongs/g/g303.md) a [dēnarion](../../strongs/g/g1220.md).**

<a name="matthew_20_10"></a>Matthew 20:10

**But when the [prōtos](../../strongs/g/g4413.md) [erchomai](../../strongs/g/g2064.md), they [nomizō](../../strongs/g/g3543.md) that they should have [lambanō](../../strongs/g/g2983.md) more; and they likewise [lambanō](../../strongs/g/g2983.md) [ana](../../strongs/g/g303.md) a [dēnarion](../../strongs/g/g1220.md).**

<a name="matthew_20_11"></a>Matthew 20:11

**And when they had [lambanō](../../strongs/g/g2983.md), they [goggyzō](../../strongs/g/g1111.md) against the [oikodespotēs](../../strongs/g/g3617.md),**

<a name="matthew_20_12"></a>Matthew 20:12

**[legō](../../strongs/g/g3004.md), These [eschatos](../../strongs/g/g2078.md) have [poieō](../../strongs/g/g4160.md) one [hōra](../../strongs/g/g5610.md), and thou hast made them [isos](../../strongs/g/g2470.md) unto us, which have [bastazō](../../strongs/g/g941.md) the [baros](../../strongs/g/g922.md) and [kausōn](../../strongs/g/g2742.md) of the [hēmera](../../strongs/g/g2250.md).**

<a name="matthew_20_13"></a>Matthew 20:13

**But he [apokrinomai](../../strongs/g/g611.md) one of them, and [eipon](../../strongs/g/g2036.md), [hetairos](../../strongs/g/g2083.md), I do thee no [adikeō](../../strongs/g/g91.md): didst not thou [symphōneō](../../strongs/g/g4856.md) with me for a [dēnarion](../../strongs/g/g1220.md)?**

<a name="matthew_20_14"></a>Matthew 20:14

**[airō](../../strongs/g/g142.md) thine, and [hypagō](../../strongs/g/g5217.md): I will [didōmi](../../strongs/g/g1325.md) unto this [eschatos](../../strongs/g/g2078.md), even as unto thee.**

<a name="matthew_20_15"></a>Matthew 20:15

**Is it not [exesti](../../strongs/g/g1832.md) for me to [poieō](../../strongs/g/g4160.md) what I will with mine own? Is thine [ophthalmos](../../strongs/g/g3788.md) [ponēros](../../strongs/g/g4190.md), because I am [agathos](../../strongs/g/g18.md)?**

<a name="matthew_20_16"></a>Matthew 20:16

**So the [eschatos](../../strongs/g/g2078.md) shall be [prōtos](../../strongs/g/g4413.md), and the [prōtos](../../strongs/g/g4413.md) [eschatos](../../strongs/g/g2078.md): for [polys](../../strongs/g/g4183.md) be [klētos](../../strongs/g/g2822.md), but [oligos](../../strongs/g/g3641.md) [eklektos](../../strongs/g/g1588.md).** [^1]

<a name="matthew_20_17"></a>Matthew 20:17

And [Iēsous](../../strongs/g/g2424.md) [anabainō](../../strongs/g/g305.md) to [Hierosolyma](../../strongs/g/g2414.md) [paralambanō](../../strongs/g/g3880.md) the [dōdeka](../../strongs/g/g1427.md) [mathētēs](../../strongs/g/g3101.md) apart in [hodos](../../strongs/g/g3598.md), and [eipon](../../strongs/g/g2036.md) unto them,

<a name="matthew_20_18"></a>Matthew 20:18

**[idou](../../strongs/g/g2400.md), we [anabainō](../../strongs/g/g305.md) to [Hierosolyma](../../strongs/g/g2414.md); and the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) shall be [paradidōmi](../../strongs/g/g3860.md) unto the [archiereus](../../strongs/g/g749.md) and unto the [grammateus](../../strongs/g/g1122.md), and they shall [katakrinō](../../strongs/g/g2632.md) him to [thanatos](../../strongs/g/g2288.md),**

<a name="matthew_20_19"></a>Matthew 20:19

**And shall [paradidōmi](../../strongs/g/g3860.md) him to the [ethnos](../../strongs/g/g1484.md) to [empaizō](../../strongs/g/g1702.md), and to [mastigoō](../../strongs/g/g3146.md), and to [stauroō](../../strongs/g/g4717.md): and the third [hēmera](../../strongs/g/g2250.md) he shall [anistēmi](../../strongs/g/g450.md).**

<a name="matthew_20_20"></a>Matthew 20:20

Then [proserchomai](../../strongs/g/g4334.md) to him the [mētēr](../../strongs/g/g3384.md) of [Zebedaios](../../strongs/g/g2199.md) [huios](../../strongs/g/g5207.md) with her [huios](../../strongs/g/g5207.md), [proskyneō](../../strongs/g/g4352.md), and [aiteō](../../strongs/g/g154.md) a certain thing of him.

<a name="matthew_20_21"></a>Matthew 20:21

And he [eipon](../../strongs/g/g2036.md) unto her, **What [thelō](../../strongs/g/g2309.md)?** She [legō](../../strongs/g/g3004.md) unto him, [eipon](../../strongs/g/g2036.md) that these my two [huios](../../strongs/g/g5207.md) may [kathizō](../../strongs/g/g2523.md), the one on thy [dexios](../../strongs/g/g1188.md), and the other on the [euōnymos](../../strongs/g/g2176.md), in thy [basileia](../../strongs/g/g932.md).

<a name="matthew_20_22"></a>Matthew 20:22

But [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), **Ye [eidō](../../strongs/g/g1492.md) not what ye [aiteō](../../strongs/g/g154.md). Are ye able to [pinō](../../strongs/g/g4095.md) of the [potērion](../../strongs/g/g4221.md) that I shall [pinō](../../strongs/g/g4095.md) of, and to be [baptizō](../../strongs/g/g907.md) with the [baptisma](../../strongs/g/g908.md) that I am [baptizō](../../strongs/g/g907.md) with?** They [legō](../../strongs/g/g3004.md) unto him, We [dynamai](../../strongs/g/g1410.md). [^2]

<a name="matthew_20_23"></a>Matthew 20:23

And he [legō](../../strongs/g/g3004.md) unto them, **Ye shall [pinō](../../strongs/g/g4095.md) indeed of my [potērion](../../strongs/g/g4221.md), and be [baptizō](../../strongs/g/g907.md) with the [baptisma](../../strongs/g/g908.md) that I am [baptizō](../../strongs/g/g907.md) with: but to [kathizō](../../strongs/g/g2523.md) on my [dexios](../../strongs/g/g1188.md), and on my [euōnymos](../../strongs/g/g2176.md), is not mine to [didōmi](../../strongs/g/g1325.md), but for whom it is [hetoimazō](../../strongs/g/g2090.md) of my [patēr](../../strongs/g/g3962.md).**

<a name="matthew_20_24"></a>Matthew 20:24

And when the ten [akouō](../../strongs/g/g191.md), they were [aganakteō](../../strongs/g/g23.md) against the two [adelphos](../../strongs/g/g80.md).

<a name="matthew_20_25"></a>Matthew 20:25

But [Iēsous](../../strongs/g/g2424.md) [proskaleō](../../strongs/g/g4341.md) them, and [eipon](../../strongs/g/g2036.md), **Ye [eidō](../../strongs/g/g1492.md) that the [archōn](../../strongs/g/g758.md) of the [ethnos](../../strongs/g/g1484.md) [katakyrieuō](../../strongs/g/g2634.md) them, and they that are [megas](../../strongs/g/g3173.md) [katexousiazō](../../strongs/g/g2715.md) them.**

<a name="matthew_20_26"></a>Matthew 20:26

**But it shall not be so among you: but whosoever will be [megas](../../strongs/g/g3173.md) among you, let him be your [diakonos](../../strongs/g/g1249.md);**

<a name="matthew_20_27"></a>Matthew 20:27

**And whosoever will be [prōtos](../../strongs/g/g4413.md) among you, let him be your [doulos](../../strongs/g/g1401.md):**

<a name="matthew_20_28"></a>Matthew 20:28

**Even as the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [erchomai](../../strongs/g/g2064.md) not to [diakoneō](../../strongs/g/g1247.md), but to [diakoneō](../../strongs/g/g1247.md), and to [didōmi](../../strongs/g/g1325.md) his [psychē](../../strongs/g/g5590.md) a [lytron](../../strongs/g/g3083.md) for [polys](../../strongs/g/g4183.md).** [^3]

<a name="matthew_20_29"></a>Matthew 20:29

And as they [ekporeuomai](../../strongs/g/g1607.md) from [Ierichō](../../strongs/g/g2410.md), a [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) [akoloutheō](../../strongs/g/g190.md) him.

<a name="matthew_20_30"></a>Matthew 20:30

And, [idou](../../strongs/g/g2400.md), two [typhlos](../../strongs/g/g5185.md) [kathēmai](../../strongs/g/g2521.md) by [hodos](../../strongs/g/g3598.md) side, when they [akouō](../../strongs/g/g191.md) that [Iēsous](../../strongs/g/g2424.md) [paragō](../../strongs/g/g3855.md), [krazō](../../strongs/g/g2896.md), [legō](../../strongs/g/g3004.md), Have [eleeō](../../strongs/g/g1653.md) on us, [kyrios](../../strongs/g/g2962.md), [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md).

<a name="matthew_20_31"></a>Matthew 20:31

And the [ochlos](../../strongs/g/g3793.md) [epitimaō](../../strongs/g/g2008.md) them, because they should [siōpaō](../../strongs/g/g4623.md): but they [krazō](../../strongs/g/g2896.md) [meizōn](../../strongs/g/g3185.md), [legō](../../strongs/g/g3004.md), Have [eleeō](../../strongs/g/g1653.md) on us, [kyrios](../../strongs/g/g2962.md), [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md).

<a name="matthew_20_32"></a>Matthew 20:32

And [Iēsous](../../strongs/g/g2424.md) [histēmi](../../strongs/g/g2476.md), and [phōneō](../../strongs/g/g5455.md) them, and [eipon](../../strongs/g/g2036.md), **What will ye that I shall [poieō](../../strongs/g/g4160.md) unto you?**

<a name="matthew_20_33"></a>Matthew 20:33

They [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), that our [ophthalmos](../../strongs/g/g3788.md) may be [anoigō](../../strongs/g/g455.md).

<a name="matthew_20_34"></a>Matthew 20:34

So [Iēsous](../../strongs/g/g2424.md) had [splagchnizomai](../../strongs/g/g4697.md), and [haptomai](../../strongs/g/g680.md) their [ophthalmos](../../strongs/g/g3788.md): and [eutheōs](../../strongs/g/g2112.md) their [ophthalmos](../../strongs/g/g3788.md) [anablepō](../../strongs/g/g308.md), and they [akoloutheō](../../strongs/g/g190.md) him.

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 19](matthew_19.md) - [Matthew 21](matthew_21.md)

---

[^1]: [Matthew 20:16 Commentary](../../commentary/matthew/matthew_20_commentary.md#matthew_20_16)

[^2]: [Matthew 20:22 Commentary](../../commentary/matthew/matthew_20_commentary.md#matthew_20_22)

[^3]: [Matthew 20:28 Commentary](../../commentary/matthew/matthew_20_commentary.md#matthew_20_28)
