# [Matthew 19](https://www.blueletterbible.org/kjv/mat/19/1/rl1/s_948001)

<a name="matthew_19_1"></a>Matthew 19:1

And [ginomai](../../strongs/g/g1096.md), when [Iēsous](../../strongs/g/g2424.md) had [teleō](../../strongs/g/g5055.md) these [logos](../../strongs/g/g3056.md), he [metairō](../../strongs/g/g3332.md) from [Galilaia](../../strongs/g/g1056.md), and [erchomai](../../strongs/g/g2064.md) into the [horion](../../strongs/g/g3725.md) of [Ioudaia](../../strongs/g/g2449.md) beyond [Iordanēs](../../strongs/g/g2446.md);

<a name="matthew_19_2"></a>Matthew 19:2

And [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) [akoloutheō](../../strongs/g/g190.md) him; and he [therapeuō](../../strongs/g/g2323.md) them there.

<a name="matthew_19_3"></a>Matthew 19:3

The [Pharisaios](../../strongs/g/g5330.md) also [proserchomai](../../strongs/g/g4334.md) unto him, [peirazō](../../strongs/g/g3985.md) him, and [legō](../../strongs/g/g3004.md) unto him, Is it [exesti](../../strongs/g/g1832.md) for an [anthrōpos](../../strongs/g/g444.md) to [apolyō](../../strongs/g/g630.md) his [gynē](../../strongs/g/g1135.md) for every [aitia](../../strongs/g/g156.md)?

<a name="matthew_19_4"></a>Matthew 19:4

And he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **Have ye not [anaginōskō](../../strongs/g/g314.md), that he which [poieō](../../strongs/g/g4160.md) at the [archē](../../strongs/g/g746.md) [poieō](../../strongs/g/g4160.md) them [arrēn](../../strongs/g/g730.md) and [thēlys](../../strongs/g/g2338.md),**

<a name="matthew_19_5"></a>Matthew 19:5

**And [eipon](../../strongs/g/g2036.md), For this [heneka](../../strongs/g/g1752.md) shall [anthrōpos](../../strongs/g/g444.md) [kataleipō](../../strongs/g/g2641.md) [patēr](../../strongs/g/g3962.md) and [mētēr](../../strongs/g/g3384.md), and shall [proskollaō](../../strongs/g/g4347.md) to his [gynē](../../strongs/g/g1135.md): and they [dyo](../../strongs/g/g1417.md) shall be one [sarx](../../strongs/g/g4561.md)?**

<a name="matthew_19_6"></a>Matthew 19:6

**Wherefore they are no more [dyo](../../strongs/g/g1417.md), but one [sarx](../../strongs/g/g4561.md). What therefore [theos](../../strongs/g/g2316.md) hath [syzeugnymi](../../strongs/g/g4801.md), let not [anthrōpos](../../strongs/g/g444.md) [chōrizō](../../strongs/g/g5563.md).**

<a name="matthew_19_7"></a>Matthew 19:7

They [legō](../../strongs/g/g3004.md) unto him, Why did [Mōÿsēs](../../strongs/g/g3475.md) then [entellō](../../strongs/g/g1781.md) to [didōmi](../../strongs/g/g1325.md) a [biblion](../../strongs/g/g975.md) of [apostasion](../../strongs/g/g647.md), and to [apolyō](../../strongs/g/g630.md) her?

<a name="matthew_19_8"></a>Matthew 19:8

He [legō](../../strongs/g/g3004.md) unto them, **[Mōÿsēs](../../strongs/g/g3475.md) because of the [sklērokardia](../../strongs/g/g4641.md) [epitrepō](../../strongs/g/g2010.md) you to [apolyō](../../strongs/g/g630.md) your [gynē](../../strongs/g/g1135.md): but from the [archē](../../strongs/g/g746.md) it was not so.**

<a name="matthew_19_9"></a>Matthew 19:9

**And I [legō](../../strongs/g/g3004.md) unto you, Whosoever shall [apolyō](../../strongs/g/g630.md) his [gynē](../../strongs/g/g1135.md), except for [porneia](../../strongs/g/g4202.md), and shall [gameō](../../strongs/g/g1060.md) another, [moichaō](../../strongs/g/g3429.md): and whoso [gameō](../../strongs/g/g1060.md) her which is put away doth [moichaō](../../strongs/g/g3429.md).**

<a name="matthew_19_10"></a>Matthew 19:10

His [mathētēs](../../strongs/g/g3101.md) [legō](../../strongs/g/g3004.md) unto him, If the case of the [anthrōpos](../../strongs/g/g444.md) be so with [gynē](../../strongs/g/g1135.md), it is not [sympherō](../../strongs/g/g4851.md) to [gameō](../../strongs/g/g1060.md).

<a name="matthew_19_11"></a>Matthew 19:11

But he [eipon](../../strongs/g/g2036.md) unto them, **All cannot [chōreō](../../strongs/g/g5562.md) this [logos](../../strongs/g/g3056.md), save to whom it is [didōmi](../../strongs/g/g1325.md).**

<a name="matthew_19_12"></a>Matthew 19:12

**For there are some [eunouchos](../../strongs/g/g2135.md), which were so [gennaō](../../strongs/g/g1080.md) from their [mētēr](../../strongs/g/g3384.md) [koilia](../../strongs/g/g2836.md): and there are some [eunouchos](../../strongs/g/g2135.md), which were [eunouchizō](../../strongs/g/g2134.md) of [anthrōpos](../../strongs/g/g444.md): and there be [eunouchos](../../strongs/g/g2135.md), which have [eunouchizō](../../strongs/g/g2134.md) themselves for the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md). He that is able to [chōreō](../../strongs/g/g5562.md), let him [chōreō](../../strongs/g/g5562.md).**

<a name="matthew_19_13"></a>Matthew 19:13

Then were there [prospherō](../../strongs/g/g4374.md) unto him [paidion](../../strongs/g/g3813.md), that he should [epitithēmi](../../strongs/g/g2007.md) [cheir](../../strongs/g/g5495.md) on them, and [proseuchomai](../../strongs/g/g4336.md): and the [mathētēs](../../strongs/g/g3101.md) [epitimaō](../../strongs/g/g2008.md) them.

<a name="matthew_19_14"></a>Matthew 19:14

But [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md), **[aphiēmi](../../strongs/g/g863.md) [paidion](../../strongs/g/g3813.md), and [kōlyō](../../strongs/g/g2967.md) them not, to [erchomai](../../strongs/g/g2064.md) unto me: for [toioutos](../../strongs/g/g5108.md) is the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_19_15"></a>Matthew 19:15

And he [epitithēmi](../../strongs/g/g2007.md) [cheir](../../strongs/g/g5495.md) on them, and [poreuō](../../strongs/g/g4198.md) thence.

<a name="matthew_19_16"></a>Matthew 19:16

And, [idou](../../strongs/g/g2400.md), one [proserchomai](../../strongs/g/g4334.md) and [eipon](../../strongs/g/g2036.md) unto him, [agathos](../../strongs/g/g18.md) [didaskalos](../../strongs/g/g1320.md), what [agathos](../../strongs/g/g18.md) shall I [poieō](../../strongs/g/g4160.md), that I may have [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md)?

<a name="matthew_19_17"></a>Matthew 19:17

And he [eipon](../../strongs/g/g2036.md) unto him, **Why [legō](../../strongs/g/g3004.md) thou me [agathos](../../strongs/g/g18.md)? there is none [agathos](../../strongs/g/g18.md) but one, [theos](../../strongs/g/g2316.md): but if thou wilt [eiserchomai](../../strongs/g/g1525.md) into [zōē](../../strongs/g/g2222.md), [tēreō](../../strongs/g/g5083.md) the [entolē](../../strongs/g/g1785.md).**

<a name="matthew_19_18"></a>Matthew 19:18

He [legō](../../strongs/g/g3004.md) unto him, [poios](../../strongs/g/g4169.md) [de](../../strongs/g/g1161.md)? [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md), **Thou shalt do no [phoneuō](../../strongs/g/g5407.md), Thou shalt not [moicheuō](../../strongs/g/g3431.md), Thou shalt not [kleptō](../../strongs/g/g2813.md), Thou shalt not [pseudomartyreō](../../strongs/g/g5576.md),**

<a name="matthew_19_19"></a>Matthew 19:19

**[timaō](../../strongs/g/g5091.md) thy [patēr](../../strongs/g/g3962.md) and [mētēr](../../strongs/g/g3384.md): and, Thou shalt [agapaō](../../strongs/g/g25.md) thy [plēsion](../../strongs/g/g4139.md) as thyself.**

<a name="matthew_19_20"></a>Matthew 19:20

The [neaniskos](../../strongs/g/g3495.md) [legō](../../strongs/g/g3004.md) unto him, All these things have I [phylassō](../../strongs/g/g5442.md) from my [neotēs](../../strongs/g/g3503.md): what [hystereō](../../strongs/g/g5302.md) I yet?

<a name="matthew_19_21"></a>Matthew 19:21

[Iēsous](../../strongs/g/g2424.md) [phēmi](../../strongs/g/g5346.md) unto him, **If thou wilt be [teleios](../../strongs/g/g5046.md), [hypagō](../../strongs/g/g5217.md) [pōleō](../../strongs/g/g4453.md) that thou [hyparchonta](../../strongs/g/g5224.md), and [didōmi](../../strongs/g/g1325.md) to the [ptōchos](../../strongs/g/g4434.md), and thou shalt have [thēsauros](../../strongs/g/g2344.md) in [ouranos](../../strongs/g/g3772.md): and [deuro](../../strongs/g/g1204.md) [akoloutheō](../../strongs/g/g190.md) me.**

<a name="matthew_19_22"></a>Matthew 19:22

But when the [neaniskos](../../strongs/g/g3495.md) [akouō](../../strongs/g/g191.md) that [logos](../../strongs/g/g3056.md), he [aperchomai](../../strongs/g/g565.md) [lypeō](../../strongs/g/g3076.md): for he had [polys](../../strongs/g/g4183.md) [ktēma](../../strongs/g/g2933.md).

<a name="matthew_19_23"></a>Matthew 19:23

Then [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) unto his [mathētēs](../../strongs/g/g3101.md), **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, That a [plousios](../../strongs/g/g4145.md) shall [dyskolōs](../../strongs/g/g1423.md) [eiserchomai](../../strongs/g/g1525.md) into the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_19_24"></a>Matthew 19:24

**And again I [legō](../../strongs/g/g3004.md) unto you, It is [eukopos](../../strongs/g/g2123.md) for a [kamēlos](../../strongs/g/g2574.md) to [dierchomai](../../strongs/g/g1330.md) through the [trypēma](../../strongs/g/g5169.md) of a [rhaphis](../../strongs/g/g4476.md), than for a [plousios](../../strongs/g/g4145.md) to [eiserchomai](../../strongs/g/g1525.md) into the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).**

<a name="matthew_19_25"></a>Matthew 19:25

When his [mathētēs](../../strongs/g/g3101.md) [akouō](../../strongs/g/g191.md), they were [sphodra](../../strongs/g/g4970.md) [ekplēssō](../../strongs/g/g1605.md), [legō](../../strongs/g/g3004.md), Who then can be [sōzō](../../strongs/g/g4982.md)?

<a name="matthew_19_26"></a>Matthew 19:26

But [Iēsous](../../strongs/g/g2424.md) [emblepō](../../strongs/g/g1689.md), and [eipon](../../strongs/g/g2036.md) unto them, **With [anthrōpos](../../strongs/g/g444.md) this is [adynatos](../../strongs/g/g102.md); but with [theos](../../strongs/g/g2316.md) all things are [dynatos](../../strongs/g/g1415.md).**

<a name="matthew_19_27"></a>Matthew 19:27

Then [apokrinomai](../../strongs/g/g611.md) [Petros](../../strongs/g/g4074.md) and [eipon](../../strongs/g/g2036.md) unto him, [idou](../../strongs/g/g2400.md), we have [aphiēmi](../../strongs/g/g863.md) all, and [akoloutheō](../../strongs/g/g190.md) thee; what shall we have therefore?

<a name="matthew_19_28"></a>Matthew 19:28

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, That ye which have [akoloutheō](../../strongs/g/g190.md) me, in the [paliggenesia](../../strongs/g/g3824.md) when the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) shall [kathizō](../../strongs/g/g2523.md) in the [thronos](../../strongs/g/g2362.md) of his [doxa](../../strongs/g/g1391.md), ye also shall [kathizō](../../strongs/g/g2523.md) upon twelve [thronos](../../strongs/g/g2362.md), [krinō](../../strongs/g/g2919.md) the twelve [phylē](../../strongs/g/g5443.md) of [Israēl](../../strongs/g/g2474.md).**

<a name="matthew_19_29"></a>Matthew 19:29

**And every one that hath [aphiēmi](../../strongs/g/g863.md) [oikia](../../strongs/g/g3614.md), or [adelphos](../../strongs/g/g80.md), or [adelphē](../../strongs/g/g79.md), or [patēr](../../strongs/g/g3962.md), or [mētēr](../../strongs/g/g3384.md), or [gynē](../../strongs/g/g1135.md), or [teknon](../../strongs/g/g5043.md), or [agros](../../strongs/g/g68.md), for my [onoma](../../strongs/g/g3686.md) sake, shall [lambanō](../../strongs/g/g2983.md) an hundredfold, and shall [klēronomeō](../../strongs/g/g2816.md) [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md).** [^1]

<a name="matthew_19_30"></a>Matthew 19:30

**But [polys](../../strongs/g/g4183.md) [prōtos](../../strongs/g/g4413.md) shall be [eschatos](../../strongs/g/g2078.md); and the [eschatos](../../strongs/g/g2078.md) [prōtos](../../strongs/g/g4413.md).**

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 18](matthew_18.md) - [Matthew 20](matthew_20.md)

---

[^1]: [Matthew 19:29 Commentary](../../commentary/matthew/matthew_19_commentary.md#matthew_19_29)
