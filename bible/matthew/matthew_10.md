# [Matthew 10](https://www.blueletterbible.org/kjv/mat/10/1/rl1/s_939001)

<a name="matthew_10_1"></a>Matthew 10:1

And when he had [proskaleō](../../strongs/g/g4341.md) unto his twelve [mathētēs](../../strongs/g/g3101.md), he [didōmi](../../strongs/g/g1325.md) them [exousia](../../strongs/g/g1849.md) [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md), to [ekballō](../../strongs/g/g1544.md) them, and to [therapeuō](../../strongs/g/g2323.md) all manner of [nosos](../../strongs/g/g3554.md) and all manner of [malakia](../../strongs/g/g3119.md). [^1]

<a name="matthew_10_2"></a>Matthew 10:2

Now the [onoma](../../strongs/g/g3686.md) of the [dōdeka](../../strongs/g/g1427.md) [apostolos](../../strongs/g/g652.md) are these; The first, [Simōn](../../strongs/g/g4613.md), who is [legō](../../strongs/g/g3004.md) [Petros](../../strongs/g/g4074.md), and [Andreas](../../strongs/g/g406.md) his [adelphos](../../strongs/g/g80.md); [Iakōbos](../../strongs/g/g2385.md) of [Zebedaios](../../strongs/g/g2199.md), and [Iōannēs](../../strongs/g/g2491.md) his [adelphos](../../strongs/g/g80.md);

<a name="matthew_10_3"></a>Matthew 10:3

[Philippos](../../strongs/g/g5376.md), and [Bartholomaios](../../strongs/g/g918.md); [Thōmas](../../strongs/g/g2381.md), and [Maththaios](../../strongs/g/g3156.md) the [telōnēs](../../strongs/g/g5057.md); [Iakōbos](../../strongs/g/g2385.md) of [Alphaios](../../strongs/g/g256.md), and [Lebbaios](../../strongs/g/g3002.md), [epikaleō](../../strongs/g/g1941.md) [Thaddaios](../../strongs/g/g2280.md);

<a name="matthew_10_4"></a>Matthew 10:4

[Simōn](../../strongs/g/g4613.md) the [Kananaios](../../strongs/g/g2581.md), and [Ioudas](../../strongs/g/g2455.md) [Iskariōth](../../strongs/g/g2469.md), who also [paradidōmi](../../strongs/g/g3860.md) him.

<a name="matthew_10_5"></a>Matthew 10:5

These twelve [Iēsous](../../strongs/g/g2424.md) [apostellō](../../strongs/g/g649.md), and [paraggellō](../../strongs/g/g3853.md) them, [legō](../../strongs/g/g3004.md), **[aperchomai](../../strongs/g/g565.md) not into the [hodos](../../strongs/g/g3598.md) of the [ethnos](../../strongs/g/g1484.md), and into [polis](../../strongs/g/g4172.md) of the [Samaritēs](../../strongs/g/g4541.md) [eiserchomai](../../strongs/g/g1525.md) ye not:**

<a name="matthew_10_6"></a>Matthew 10:6

**But [poreuō](../../strongs/g/g4198.md) rather to the [apollymi](../../strongs/g/g622.md) [probaton](../../strongs/g/g4263.md) of the [oikos](../../strongs/g/g3624.md) of [Israēl](../../strongs/g/g2474.md).**

<a name="matthew_10_7"></a>Matthew 10:7

**And as ye [poreuō](../../strongs/g/g4198.md), [kēryssō](../../strongs/g/g2784.md), [legō](../../strongs/g/g3004.md), The [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) [eggizō](../../strongs/g/g1448.md).**

<a name="matthew_10_8"></a>Matthew 10:8

**[therapeuō](../../strongs/g/g2323.md) the [astheneō](../../strongs/g/g770.md), [katharizō](../../strongs/g/g2511.md) the [lepros](../../strongs/g/g3015.md), [egeirō](../../strongs/g/g1453.md) the [nekros](../../strongs/g/g3498.md), [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md): [dōrean](../../strongs/g/g1432.md) ye have [lambanō](../../strongs/g/g2983.md), [dōrean](../../strongs/g/g1432.md) [didōmi](../../strongs/g/g1325.md).** [^2]

<a name="matthew_10_9"></a>Matthew 10:9

**[ktaomai](../../strongs/g/g2932.md) neither [chrysos](../../strongs/g/g5557.md), nor [argyros](../../strongs/g/g696.md), nor [chalkos](../../strongs/g/g5475.md) in your [zōnē](../../strongs/g/g2223.md),**

<a name="matthew_10_10"></a>Matthew 10:10

**Nor [pēra](../../strongs/g/g4082.md) for [hodos](../../strongs/g/g3598.md), neither two [chitōn](../../strongs/g/g5509.md), neither [hypodēma](../../strongs/g/g5266.md), nor yet [rhabdos](../../strongs/g/g4464.md): for the [ergatēs](../../strongs/g/g2040.md) is [axios](../../strongs/g/g514.md) of his [trophē](../../strongs/g/g5160.md).**

<a name="matthew_10_11"></a>Matthew 10:11

**And into whatsoever [polis](../../strongs/g/g4172.md) or [kōmē](../../strongs/g/g2968.md) ye shall [eiserchomai](../../strongs/g/g1525.md), [exetazō](../../strongs/g/g1833.md) who in it is [axios](../../strongs/g/g514.md); and there [menō](../../strongs/g/g3306.md) till ye [exerchomai](../../strongs/g/g1831.md).**

<a name="matthew_10_12"></a>Matthew 10:12

**And when ye [eiserchomai](../../strongs/g/g1525.md) into [oikia](../../strongs/g/g3614.md), [aspazomai](../../strongs/g/g782.md) it.**

<a name="matthew_10_13"></a>Matthew 10:13

**And if the [oikia](../../strongs/g/g3614.md) be [axios](../../strongs/g/g514.md), let your [eirēnē](../../strongs/g/g1515.md) [erchomai](../../strongs/g/g2064.md) upon it: but if it be not [axios](../../strongs/g/g514.md), let your [eirēnē](../../strongs/g/g1515.md) [epistrephō](../../strongs/g/g1994.md) to you.** [^3]

<a name="matthew_10_14"></a>Matthew 10:14

**And whosoever shall not [dechomai](../../strongs/g/g1209.md) you, nor [akouō](../../strongs/g/g191.md) your [logos](../../strongs/g/g3056.md), when ye [exerchomai](../../strongs/g/g1831.md) of that [oikia](../../strongs/g/g3614.md) or [polis](../../strongs/g/g4172.md), [ektinassō](../../strongs/g/g1621.md) the [koniortos](../../strongs/g/g2868.md) of your [pous](../../strongs/g/g4228.md).** [^4]

<a name="matthew_10_15"></a>Matthew 10:15

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, It shall be [anektos](../../strongs/g/g414.md) for the [gē](../../strongs/g/g1093.md) of [Sodoma](../../strongs/g/g4670.md) and [Gomorra](../../strongs/g/g1116.md) in the [hēmera](../../strongs/g/g2250.md) of [krisis](../../strongs/g/g2920.md), than for that [polis](../../strongs/g/g4172.md).**

<a name="matthew_10_16"></a>Matthew 10:16

**[idou](../../strongs/g/g2400.md), I [apostellō](../../strongs/g/g649.md) you as [probaton](../../strongs/g/g4263.md) in the [mesos](../../strongs/g/g3319.md) of [lykos](../../strongs/g/g3074.md): be ye therefore [phronimos](../../strongs/g/g5429.md) as [ophis](../../strongs/g/g3789.md), and [akeraios](../../strongs/g/g185.md) as [peristera](../../strongs/g/g4058.md).** [^5]

<a name="matthew_10_17"></a>Matthew 10:17

**But [prosechō](../../strongs/g/g4337.md) of [anthrōpos](../../strongs/g/g444.md): for they will [paradidōmi](../../strongs/g/g3860.md) you to the [synedrion](../../strongs/g/g4892.md), and they will [mastigoō](../../strongs/g/g3146.md) you in their [synagōgē](../../strongs/g/g4864.md);**

<a name="matthew_10_18"></a>Matthew 10:18

**And ye shall be [agō](../../strongs/g/g71.md) before [hēgemōn](../../strongs/g/g2232.md) and [basileus](../../strongs/g/g935.md) for my sake, for a [martyrion](../../strongs/g/g3142.md) against them and the [ethnos](../../strongs/g/g1484.md).** [^6]

<a name="matthew_10_19"></a>Matthew 10:19

**But when they [paradidōmi](../../strongs/g/g3860.md) you, [merimnaō](../../strongs/g/g3309.md) not how or what ye shall [laleō](../../strongs/g/g2980.md): for it shall be [didōmi](../../strongs/g/g1325.md) you in [ekeinos](../../strongs/g/g1565.md) [hōra](../../strongs/g/g5610.md) what ye shall [laleō](../../strongs/g/g2980.md).** [^7]

<a name="matthew_10_20"></a>Matthew 10:20

**For it is not ye that [laleō](../../strongs/g/g2980.md), but the [pneuma](../../strongs/g/g4151.md) of your [patēr](../../strongs/g/g3962.md) which [laleō](../../strongs/g/g2980.md) in you.**

<a name="matthew_10_21"></a>Matthew 10:21

**And the [adelphos](../../strongs/g/g80.md) shall [paradidōmi](../../strongs/g/g3860.md) the [adelphos](../../strongs/g/g80.md) to [thanatos](../../strongs/g/g2288.md), and the [patēr](../../strongs/g/g3962.md) the [teknon](../../strongs/g/g5043.md): and the [teknon](../../strongs/g/g5043.md) shall [epanistēmi](../../strongs/g/g1881.md) against their [goneus](../../strongs/g/g1118.md), and [thanatoō](../../strongs/g/g2289.md) them.**

<a name="matthew_10_22"></a>Matthew 10:22

**And ye shall be [miseō](../../strongs/g/g3404.md) of all for my [onoma](../../strongs/g/g3686.md): but he that [hypomenō](../../strongs/g/g5278.md) to the [telos](../../strongs/g/g5056.md) shall be [sōzō](../../strongs/g/g4982.md).**

<a name="matthew_10_23"></a>Matthew 10:23

**But when they [diōkō](../../strongs/g/g1377.md) you in this [polis](../../strongs/g/g4172.md), [pheugō](../../strongs/g/g5343.md) ye into another: for [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, Ye shall not have [teleō](../../strongs/g/g5055.md) the [polis](../../strongs/g/g4172.md) of [Israēl](../../strongs/g/g2474.md), till the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) be [erchomai](../../strongs/g/g2064.md).** [^8]

<a name="matthew_10_24"></a>Matthew 10:24

**The [mathētēs](../../strongs/g/g3101.md) is not above [didaskalos](../../strongs/g/g1320.md), nor the [doulos](../../strongs/g/g1401.md) above his [kyrios](../../strongs/g/g2962.md) .**

<a name="matthew_10_25"></a>Matthew 10:25

**It is [arketos](../../strongs/g/g713.md) for the [mathētēs](../../strongs/g/g3101.md) that he be as his [didaskalos](../../strongs/g/g1320.md), and the [doulos](../../strongs/g/g1401.md) as his [kyrios](../../strongs/g/g2962.md). If they have [kaleō](../../strongs/g/g2564.md) the [oikodespotēs](../../strongs/g/g3617.md) [Beelzeboul](../../strongs/g/g954.md), how much more them of his [oikiakos](../../strongs/g/g3615.md)?** [^9]

<a name="matthew_10_26"></a>Matthew 10:26

**[phobeō](../../strongs/g/g5399.md) them not therefore: for there is [oudeis](../../strongs/g/g3762.md) [kalyptō](../../strongs/g/g2572.md), that shall not be [apokalyptō](../../strongs/g/g601.md); and [kryptos](../../strongs/g/g2927.md), that shall not be [ginōskō](../../strongs/g/g1097.md).**

<a name="matthew_10_27"></a>Matthew 10:27

**What I [legō](../../strongs/g/g3004.md) you in [skotia](../../strongs/g/g4653.md), [eipon](../../strongs/g/g2036.md) ye in [phōs](../../strongs/g/g5457.md): and what ye [akouō](../../strongs/g/g191.md) in the [ous](../../strongs/g/g3775.md), [kēryssō](../../strongs/g/g2784.md) ye upon the [dōma](../../strongs/g/g1430.md).**

<a name="matthew_10_28"></a>Matthew 10:28

**And [phobeō](../../strongs/g/g5399.md) not them which [apokteinō](../../strongs/g/g615.md) the [sōma](../../strongs/g/g4983.md), but are not able to [apokteinō](../../strongs/g/g615.md) the [psychē](../../strongs/g/g5590.md): but rather [phobeō](../../strongs/g/g5399.md) him which is able to [apollymi](../../strongs/g/g622.md) both [psychē](../../strongs/g/g5590.md) and [sōma](../../strongs/g/g4983.md) in [geenna](../../strongs/g/g1067.md).**

<a name="matthew_10_29"></a>Matthew 10:29

**Are not two [strouthion](../../strongs/g/g4765.md) [pōleō](../../strongs/g/g4453.md) for an [assarion](../../strongs/g/g787.md)? and one of them shall not [piptō](../../strongs/g/g4098.md) on the [gē](../../strongs/g/g1093.md) without your [patēr](../../strongs/g/g3962.md).**

<a name="matthew_10_30"></a>Matthew 10:30

**But the very [thrix](../../strongs/g/g2359.md) of your [kephalē](../../strongs/g/g2776.md) are all [arithmeō](../../strongs/g/g705.md).**

<a name="matthew_10_31"></a>Matthew 10:31

**[phobeō](../../strongs/g/g5399.md) ye not therefore, ye [diapherō](../../strongs/g/g1308.md) than [polys](../../strongs/g/g4183.md) [strouthion](../../strongs/g/g4765.md).**

<a name="matthew_10_32"></a>Matthew 10:32

**Whosoever therefore shall [homologeō](../../strongs/g/g3670.md) me before [anthrōpos](../../strongs/g/g444.md), him will I [homologeō](../../strongs/g/g3670.md) also before my [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_10_33"></a>Matthew 10:33

**But whosoever shall [arneomai](../../strongs/g/g720.md) me before [anthrōpos](../../strongs/g/g444.md), him will I also [arneomai](../../strongs/g/g720.md) before my [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_10_34"></a>Matthew 10:34

**[nomizō](../../strongs/g/g3543.md) not that I am [erchomai](../../strongs/g/g2064.md) to [ballō](../../strongs/g/g906.md) [eirēnē](../../strongs/g/g1515.md) on [gē](../../strongs/g/g1093.md): I came not to [ballō](../../strongs/g/g906.md) [eirēnē](../../strongs/g/g1515.md), but a [machaira](../../strongs/g/g3162.md).**

<a name="matthew_10_35"></a>Matthew 10:35

**For I am [erchomai](../../strongs/g/g2064.md) to [dichazō](../../strongs/g/g1369.md) an [anthrōpos](../../strongs/g/g444.md) against his [patēr](../../strongs/g/g3962.md), and the [thygatēr](../../strongs/g/g2364.md) against her [mētēr](../../strongs/g/g3384.md), and the [nymphē](../../strongs/g/g3565.md) against her [penthera](../../strongs/g/g3994.md).** [^10]

<a name="matthew_10_36"></a>Matthew 10:36

**And an [anthrōpos](../../strongs/g/g444.md) [echthros](../../strongs/g/g2190.md) be they of his own [oikiakos](../../strongs/g/g3615.md).**

<a name="matthew_10_37"></a>Matthew 10:37

**He that [phileō](../../strongs/g/g5368.md) [patēr](../../strongs/g/g3962.md) or [mētēr](../../strongs/g/g3384.md) more than me is not [axios](../../strongs/g/g514.md) of me: and he that [phileō](../../strongs/g/g5368.md) [huios](../../strongs/g/g5207.md) or [thygatēr](../../strongs/g/g2364.md) more than me is not [axios](../../strongs/g/g514.md) of me.** [^11]

<a name="matthew_10_38"></a>Matthew 10:38

**And he that [lambanō](../../strongs/g/g2983.md) not his [stauros](../../strongs/g/g4716.md), and [akoloutheō](../../strongs/g/g190.md) after me, is not [axios](../../strongs/g/g514.md) of me.** [^12]

<a name="matthew_10_39"></a>Matthew 10:39

**He that [heuriskō](../../strongs/g/g2147.md) his [psychē](../../strongs/g/g5590.md) shall [apollymi](../../strongs/g/g622.md) it: and he that [apollymi](../../strongs/g/g622.md) his [psychē](../../strongs/g/g5590.md) for my [heneka](../../strongs/g/g1752.md) shall [heuriskō](../../strongs/g/g2147.md) it.** [^13]

<a name="matthew_10_40"></a>Matthew 10:40

**He that [dechomai](../../strongs/g/g1209.md) you [dechomai](../../strongs/g/g1209.md) me, and he that [dechomai](../../strongs/g/g1209.md) me [dechomai](../../strongs/g/g1209.md) him that [apostellō](../../strongs/g/g649.md) me.**

<a name="matthew_10_41"></a>Matthew 10:41

**He that [dechomai](../../strongs/g/g1209.md) a [prophētēs](../../strongs/g/g4396.md) in the [onoma](../../strongs/g/g3686.md) of a [prophētēs](../../strongs/g/g4396.md) shall [lambanō](../../strongs/g/g2983.md) a [prophētēs](../../strongs/g/g4396.md) [misthos](../../strongs/g/g3408.md); and he that [lambanō](../../strongs/g/g2983.md) a [dikaios](../../strongs/g/g1342.md) in the [onoma](../../strongs/g/g3686.md) of a [dikaios](../../strongs/g/g1342.md) shall [dechomai](../../strongs/g/g1209.md) a [dikaios](../../strongs/g/g1342.md) [misthos](../../strongs/g/g3408.md).** [^14]

<a name="matthew_10_42"></a>Matthew 10:42

**And whosoever shall [potizō](../../strongs/g/g4222.md) unto one of these [mikros](../../strongs/g/g3398.md) a [potērion](../../strongs/g/g4221.md) of [psychros](../../strongs/g/g5593.md) [monon](../../strongs/g/g3440.md) in the [onoma](../../strongs/g/g3686.md) of a [mathētēs](../../strongs/g/g3101.md), [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, he shall in no wise [apollymi](../../strongs/g/g622.md) his [misthos](../../strongs/g/g3408.md).** [^15]

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 9](matthew_9.md) - [Matthew 11](matthew_11.md)

---

[^1]: [Matthew 10:1 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_1)

[^2]: [Matthew 10:8 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_8)

[^3]: [Matthew 10:13 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_13)

[^4]: [Matthew 10:14 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_14)

[^5]: [Matthew 10:16 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_16)

[^6]: [Matthew 10:18 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_18)

[^7]: [Matthew 10:19 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_19)

[^8]: [Matthew 10:23 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_23)

[^9]: [Matthew 10:25 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_25)

[^10]: [Matthew 10:35 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_35)

[^11]: [Matthew 10:37 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_37)

[^12]: [Matthew 10:38 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_38)

[^13]: [Matthew 10:39 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_39)

[^14]: [Matthew 10:41 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_41)

[^15]: [Matthew 10:42 Commentary](../../commentary/matthew/matthew_10_commentary.md#matthew_10_42)
