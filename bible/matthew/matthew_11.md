# [Matthew 11](https://www.blueletterbible.org/kjv/mat/11/1/rl1/s_940001)

<a name="matthew_11_1"></a>Matthew 11:1

And [ginomai](../../strongs/g/g1096.md), when [Iēsous](../../strongs/g/g2424.md) [teleō](../../strongs/g/g5055.md) of [diatassō](../../strongs/g/g1299.md) his [dōdeka](../../strongs/g/g1427.md) [mathētēs](../../strongs/g/g3101.md), he [metabainō](../../strongs/g/g3327.md) thence to [didaskō](../../strongs/g/g1321.md) and to [kēryssō](../../strongs/g/g2784.md) in their [polis](../../strongs/g/g4172.md). [^1]

<a name="matthew_11_2"></a>Matthew 11:2

Now when [Iōannēs](../../strongs/g/g2491.md) had [akouō](../../strongs/g/g191.md) in the [desmōtērion](../../strongs/g/g1201.md) the [ergon](../../strongs/g/g2041.md) of [Christos](../../strongs/g/g5547.md), he [pempō](../../strongs/g/g3992.md) two of his [mathētēs](../../strongs/g/g3101.md), [^2]

<a name="matthew_11_3"></a>Matthew 11:3

And [eipon](../../strongs/g/g2036.md) unto him, Art thou he that should [erchomai](../../strongs/g/g2064.md), or do we [prosdokaō](../../strongs/g/g4328.md) for another? [^3]

<a name="matthew_11_4"></a>Matthew 11:4

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **[poreuō](../../strongs/g/g4198.md) and [apaggellō](../../strongs/g/g518.md) [Iōannēs](../../strongs/g/g2491.md) those things which ye do [akouō](../../strongs/g/g191.md) and [blepō](../../strongs/g/g991.md):**

<a name="matthew_11_5"></a>Matthew 11:5

**The [typhlos](../../strongs/g/g5185.md) [anablepō](../../strongs/g/g308.md), and the [chōlos](../../strongs/g/g5560.md) [peripateō](../../strongs/g/g4043.md), the [lepros](../../strongs/g/g3015.md) are [katharizō](../../strongs/g/g2511.md), and the [kōphos](../../strongs/g/g2974.md) [akouō](../../strongs/g/g191.md), the [nekros](../../strongs/g/g3498.md) [egeirō](../../strongs/g/g1453.md), and the [ptōchos](../../strongs/g/g4434.md) [euaggelizō](../../strongs/g/g2097.md).** [^4]

<a name="matthew_11_6"></a>Matthew 11:6

**And [makarios](../../strongs/g/g3107.md) is, whosoever shall not be [skandalizō](../../strongs/g/g4624.md) in me.**

<a name="matthew_11_7"></a>Matthew 11:7

And as they [poreuō](../../strongs/g/g4198.md), [Iēsous](../../strongs/g/g2424.md) [archomai](../../strongs/g/g756.md) to [legō](../../strongs/g/g3004.md) unto the [ochlos](../../strongs/g/g3793.md) concerning [Iōannēs](../../strongs/g/g2491.md), **What [exerchomai](../../strongs/g/g1831.md) ye into the [erēmos](../../strongs/g/g2048.md) to [theaomai](../../strongs/g/g2300.md)? A [kalamos](../../strongs/g/g2563.md) [saleuō](../../strongs/g/g4531.md) with the [anemos](../../strongs/g/g417.md)?** [^5]

<a name="matthew_11_8"></a>Matthew 11:8

**But what [exerchomai](../../strongs/g/g1831.md) for to [eidō](../../strongs/g/g1492.md)? An [anthrōpos](../../strongs/g/g444.md) [amphiennymi](../../strongs/g/g294.md) in [malakos](../../strongs/g/g3120.md) [himation](../../strongs/g/g2440.md)?  [idou](../../strongs/g/g2400.md), they that [phoreō](../../strongs/g/g5409.md) [malakos](../../strongs/g/g3120.md) are in [basileus](../../strongs/g/g935.md) [oikos](../../strongs/g/g3624.md).** [^6]

<a name="matthew_11_9"></a>Matthew 11:9

**But what [exerchomai](../../strongs/g/g1831.md) for to [eidō](../../strongs/g/g1492.md)? A [prophētēs](../../strongs/g/g4396.md)? [nai](../../strongs/g/g3483.md), I [legō](../../strongs/g/g3004.md) unto you, and [perissoteros](../../strongs/g/g4055.md) than a [prophētēs](../../strongs/g/g4396.md).** [^7]

<a name="matthew_11_10"></a>Matthew 11:10

**For this is, of whom it is [graphō](../../strongs/g/g1125.md), [idou](../../strongs/g/g2400.md), I [apostellō](../../strongs/g/g649.md) my [aggelos](../../strongs/g/g32.md) before thy [prosōpon](../../strongs/g/g4383.md), which shall [kataskeuazō](../../strongs/g/g2680.md) thy [hodos](../../strongs/g/g3598.md) before thee.**

<a name="matthew_11_11"></a>Matthew 11:11

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, Among [gennētos](../../strongs/g/g1084.md) of [gynē](../../strongs/g/g1135.md) there hath not [egeirō](../../strongs/g/g1453.md) a greater than [Iōannēs](../../strongs/g/g2491.md) the [baptistēs](../../strongs/g/g910.md): notwithstanding he that is [mikros](../../strongs/g/g3398.md) in the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) is [meizōn](../../strongs/g/g3187.md) he.**

<a name="matthew_11_12"></a>Matthew 11:12

**And from the [hēmera](../../strongs/g/g2250.md) of [Iōannēs](../../strongs/g/g2491.md) the [baptistēs](../../strongs/g/g910.md) until now the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) [biazō](../../strongs/g/g971.md), and the [biastēs](../../strongs/g/g973.md) [harpazō](../../strongs/g/g726.md) it.**

<a name="matthew_11_13"></a>Matthew 11:13

**For all the [prophētēs](../../strongs/g/g4396.md) and the [nomos](../../strongs/g/g3551.md) [prophēteuō](../../strongs/g/g4395.md) until [Iōannēs](../../strongs/g/g2491.md).** [^8]

<a name="matthew_11_14"></a>Matthew 11:14

**And if ye will [dechomai](../../strongs/g/g1209.md), this is [Ēlias](../../strongs/g/g2243.md), which was for to [erchomai](../../strongs/g/g2064.md).**

<a name="matthew_11_15"></a>Matthew 11:15

**He that hath [ous](../../strongs/g/g3775.md) to [akouō](../../strongs/g/g191.md), let him [akouō](../../strongs/g/g191.md).** [^9]

<a name="matthew_11_16"></a>Matthew 11:16

**But whereunto shall I [homoioō](../../strongs/g/g3666.md) this [genea](../../strongs/g/g1074.md)? It is [homoios](../../strongs/g/g3664.md) [paidarion](../../strongs/g/g3808.md) [kathēmai](../../strongs/g/g2521.md) in the [agora](../../strongs/g/g58.md), and [prosphōneō](../../strongs/g/g4377.md) unto their [hetairos](../../strongs/g/g2083.md),** [^10]

<a name="matthew_11_17"></a>Matthew 11:17

**And [legō](../../strongs/g/g3004.md), We have [auleō](../../strongs/g/g832.md) unto you, and ye have not [orcheomai](../../strongs/g/g3738.md); we have [thrēneō](../../strongs/g/g2354.md) unto you, and ye have not [koptō](../../strongs/g/g2875.md).** [^11]

<a name="matthew_11_18"></a>Matthew 11:18

**For [Iōannēs](../../strongs/g/g2491.md) [erchomai](../../strongs/g/g2064.md) neither [esthiō](../../strongs/g/g2068.md) nor [pinō](../../strongs/g/g4095.md), and they [legō](../../strongs/g/g3004.md), He hath a [daimonion](../../strongs/g/g1140.md).** [^12]

<a name="matthew_11_19"></a>Matthew 11:19

**The [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [erchomai](../../strongs/g/g2064.md) [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md), and they [legō](../../strongs/g/g3004.md), [idou](../../strongs/g/g2400.md) a man [phagos](../../strongs/g/g5314.md), and a [oinopotēs](../../strongs/g/g3630.md), a [philos](../../strongs/g/g5384.md) of [telōnēs](../../strongs/g/g5057.md) and [hamartōlos](../../strongs/g/g268.md). But [sophia](../../strongs/g/g4678.md) is [dikaioō](../../strongs/g/g1344.md) of her [teknon](../../strongs/g/g5043.md).** [^13]

<a name="matthew_11_20"></a>Matthew 11:20

Then [archomai](../../strongs/g/g756.md) he to [oneidizō](../../strongs/g/g3679.md) the [polis](../../strongs/g/g4172.md) wherein [pleistos](../../strongs/g/g4118.md) of his [dynamis](../../strongs/g/g1411.md) were done, because they [metanoeō](../../strongs/g/g3340.md) not: [^14]

<a name="matthew_11_21"></a>Matthew 11:21

**[ouai](../../strongs/g/g3759.md) unto thee, [Chorazin](../../strongs/g/g5523.md)! [ouai](../../strongs/g/g3759.md) unto thee, [Bēthsaïda](../../strongs/g/g966.md)! for if [dynamis](../../strongs/g/g1411.md), which were done in you, had been done in [Tyros](../../strongs/g/g5184.md) and [Sidōn](../../strongs/g/g4605.md), they would have [metanoeō](../../strongs/g/g3340.md) [palai](../../strongs/g/g3819.md) in [sakkos](../../strongs/g/g4526.md) and [spodos](../../strongs/g/g4700.md).** [^15]

<a name="matthew_11_22"></a>Matthew 11:22

**But I [legō](../../strongs/g/g3004.md) unto you, It shall be [anektos](../../strongs/g/g414.md) for [Tyros](../../strongs/g/g5184.md) and [Sidōn](../../strongs/g/g4605.md) at the [hēmera](../../strongs/g/g2250.md) of [krisis](../../strongs/g/g2920.md), than for you.**

<a name="matthew_11_23"></a>Matthew 11:23

**And thou, [Kapharnaoum](../../strongs/g/g2584.md), which art [hypsoō](../../strongs/g/g5312.md) unto [ouranos](../../strongs/g/g3772.md), shalt be [katabibazō](../../strongs/g/g2601.md) to [hadēs](../../strongs/g/g86.md): for if the [dynamis](../../strongs/g/g1411.md), which have been done in thee, had been done in [Sodoma](../../strongs/g/g4670.md), it would have [menō](../../strongs/g/g3306.md) until [sēmeron](../../strongs/g/g4594.md).** [^16]

<a name="matthew_11_24"></a>Matthew 11:24

**But I [legō](../../strongs/g/g3004.md) unto you, That it shall be [anektos](../../strongs/g/g414.md) for the [gē](../../strongs/g/g1093.md) of [Sodoma](../../strongs/g/g4670.md) in the [hēmera](../../strongs/g/g2250.md) of [krisis](../../strongs/g/g2920.md), than for thee.**

<a name="matthew_11_25"></a>Matthew 11:25

At that [kairos](../../strongs/g/g2540.md) [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), **I [exomologeō](../../strongs/g/g1843.md) thee, O [patēr](../../strongs/g/g3962.md), [kyrios](../../strongs/g/g2962.md) of [ouranos](../../strongs/g/g3772.md) and [gē](../../strongs/g/g1093.md), because thou hast [apokryptō](../../strongs/g/g613.md) these things from the [sophos](../../strongs/g/g4680.md) and [synetos](../../strongs/g/g4908.md), and hast [apokalyptō](../../strongs/g/g601.md) them unto [nēpios](../../strongs/g/g3516.md).** [^17]

<a name="matthew_11_26"></a>Matthew 11:26

**[nai](../../strongs/g/g3483.md), [patēr](../../strongs/g/g3962.md): for so it seemed [eudokia](../../strongs/g/g2107.md) in thy [emprosthen](../../strongs/g/g1715.md).**

<a name="matthew_11_27"></a>Matthew 11:27

**All things are [paradidōmi](../../strongs/g/g3860.md) unto me of my [patēr](../../strongs/g/g3962.md): and [oudeis](../../strongs/g/g3762.md) [epiginōskō](../../strongs/g/g1921.md) the [huios](../../strongs/g/g5207.md), but the [patēr](../../strongs/g/g3962.md); neither [epiginōskō](../../strongs/g/g1921.md) any [tis](../../strongs/g/g5100.md) the [patēr](../../strongs/g/g3962.md), [ei mē](../../strongs/g/g1508.md) the [huios](../../strongs/g/g5207.md), and to whomsoever the [huios](../../strongs/g/g5207.md) [boulomai](../../strongs/g/g1014.md) [apokalyptō](../../strongs/g/g601.md).** [^18]

<a name="matthew_11_28"></a>Matthew 11:28

**[deute](../../strongs/g/g1205.md) unto me, all ye that [kopiaō](../../strongs/g/g2872.md) and are [phortizō](../../strongs/g/g5412.md), and I will [anapauō](../../strongs/g/g373.md) you.**

<a name="matthew_11_29"></a>Matthew 11:29

**[airō](../../strongs/g/g142.md) my [zygos](../../strongs/g/g2218.md) upon you, and [manthanō](../../strongs/g/g3129.md) of me; for I am [praos](../../strongs/g/g4235.md) and [tapeinos](../../strongs/g/g5011.md) in [kardia](../../strongs/g/g2588.md): and ye shall [heuriskō](../../strongs/g/g2147.md) [anapausis](../../strongs/g/g372.md) unto your [psychē](../../strongs/g/g5590.md).** [^19]

<a name="matthew_11_30"></a>Matthew 11:30

**For my [zygos](../../strongs/g/g2218.md) [chrēstos](../../strongs/g/g5543.md), and my [phortion](../../strongs/g/g5413.md) is [elaphros](../../strongs/g/g1645.md).**

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 10](matthew_10.md) - [Matthew 12](matthew_12.md)

---

[^1]: [Matthew 11:1 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_1)

[^2]: [Matthew 11:2 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_2)

[^3]: [Matthew 11:3 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_3)

[^4]: [Matthew 11:5 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_5)

[^5]: [Matthew 11:7 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_7)

[^6]: [Matthew 11:8 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_8)

[^7]: [Matthew 11:9 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_9)

[^8]: [Matthew 11:13 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_13)

[^9]: [Matthew 11:15 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_15)

[^10]: [Matthew 11:16 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_16)

[^11]: [Matthew 11:17 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_17)

[^12]: [Matthew 11:18 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_18)

[^13]: [Matthew 11:19 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_19)

[^14]: [Matthew 11:20 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_20)

[^15]: [Matthew 11:21 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_21)

[^16]: [Matthew 11:23 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_23)

[^17]: [Matthew 11:25 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_25)

[^18]: [Matthew 11:27 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_27)

[^19]: [Matthew 11:29 Commentary](../../commentary/matthew/matthew_11_commentary.md#matthew_11_29)
