# [Matthew 12](https://www.blueletterbible.org/kjv/mat/12/1/rl1/s_941001)

<a name="matthew_12_1"></a>Matthew 12:1

At that [kairos](../../strongs/g/g2540.md) [Iēsous](../../strongs/g/g2424.md) [poreuō](../../strongs/g/g4198.md) on the [sabbaton](../../strongs/g/g4521.md) through the [sporimos](../../strongs/g/g4702.md); and his [mathētēs](../../strongs/g/g3101.md) were [peinaō](../../strongs/g/g3983.md), and [archomai](../../strongs/g/g756.md) to [tillō](../../strongs/g/g5089.md) the [stachys](../../strongs/g/g4719.md) and [esthiō](../../strongs/g/g2068.md). [^1]

<a name="matthew_12_2"></a>Matthew 12:2

But when the [Pharisaios](../../strongs/g/g5330.md) [eidō](../../strongs/g/g1492.md), they [eipon](../../strongs/g/g2036.md) unto him, [idou](../../strongs/g/g2400.md), thy [mathētēs](../../strongs/g/g3101.md) [poieō](../../strongs/g/g4160.md) that which is not [exesti](../../strongs/g/g1832.md) to [poieō](../../strongs/g/g4160.md) upon the [sabbaton](../../strongs/g/g4521.md). [^2]

<a name="matthew_12_3"></a>Matthew 12:3

But he [eipon](../../strongs/g/g2036.md) unto them, **Have ye not [anaginōskō](../../strongs/g/g314.md) what [Dabid](../../strongs/g/g1138.md) [poieō](../../strongs/g/g4160.md), when he was [peinaō](../../strongs/g/g3983.md), and they that were with him;**

<a name="matthew_12_4"></a>Matthew 12:4

**How he [eiserchomai](../../strongs/g/g1525.md) into the [oikos](../../strongs/g/g3624.md) of [theos](../../strongs/g/g2316.md), and did [phago](../../strongs/g/g5315.md) the [artos](../../strongs/g/g740.md) [prothesis](../../strongs/g/g4286.md), which was not [exesti](../../strongs/g/g1832.md) for him to [phago](../../strongs/g/g5315.md), neither for them which were with him, but only for the [hiereus](../../strongs/g/g2409.md)?** [^3]

<a name="matthew_12_5"></a>Matthew 12:5

**Or have ye not [anaginōskō](../../strongs/g/g314.md) in the [nomos](../../strongs/g/g3551.md), how that on the [sabbaton](../../strongs/g/g4521.md) the [hiereus](../../strongs/g/g2409.md) in the [hieron](../../strongs/g/g2411.md) [bebēloō](../../strongs/g/g953.md) the [sabbaton](../../strongs/g/g4521.md), and are [anaitios](../../strongs/g/g338.md)?**

<a name="matthew_12_6"></a>Matthew 12:6

**But I [legō](../../strongs/g/g3004.md) unto you, That in [hōde](../../strongs/g/g5602.md) is [meizōn](../../strongs/g/g3187.md) the [hieron](../../strongs/g/g2411.md).**

<a name="matthew_12_7"></a>Matthew 12:7

**But if ye had [ginōskō](../../strongs/g/g1097.md) what meaneth, I will have [eleos](../../strongs/g/g1656.md), and not [thysia](../../strongs/g/g2378.md), ye would not have [katadikazō](../../strongs/g/g2613.md) the [anaitios](../../strongs/g/g338.md).**

<a name="matthew_12_8"></a>Matthew 12:8

**For the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) is [kyrios](../../strongs/g/g2962.md) even of the [sabbaton](../../strongs/g/g4521.md).**

<a name="matthew_12_9"></a>Matthew 12:9

And when he was [metabainō](../../strongs/g/g3327.md), he [erchomai](../../strongs/g/g2064.md) into their [synagōgē](../../strongs/g/g4864.md): [^4]

<a name="matthew_12_10"></a>Matthew 12:10

And, [idou](../../strongs/g/g2400.md), there was an [anthrōpos](../../strongs/g/g444.md) which had [cheir](../../strongs/g/g5495.md) [xēros](../../strongs/g/g3584.md). And they [eperōtaō](../../strongs/g/g1905.md) him, [legō](../../strongs/g/g3004.md), Is it [exesti](../../strongs/g/g1832.md) to [therapeuō](../../strongs/g/g2323.md) on the [sabbaton](../../strongs/g/g4521.md)? that they might [katēgoreō](../../strongs/g/g2723.md) him. [^5]

<a name="matthew_12_11"></a>Matthew 12:11

And he [eipon](../../strongs/g/g2036.md) unto them, **What [anthrōpos](../../strongs/g/g444.md) shall there be among you, that shall have one [probaton](../../strongs/g/g4263.md), and if it [empiptō](../../strongs/g/g1706.md) into a [bothynos](../../strongs/g/g999.md) on the [sabbaton](../../strongs/g/g4521.md), will he not [krateō](../../strongs/g/g2902.md) on it, and [egeirō](../../strongs/g/g1453.md)?**

<a name="matthew_12_12"></a>Matthew 12:12

**How much then is an [anthrōpos](../../strongs/g/g444.md) [diapherō](../../strongs/g/g1308.md) a [probaton](../../strongs/g/g4263.md)? Wherefore it is [exesti](../../strongs/g/g1832.md) to [poieō](../../strongs/g/g4160.md) [kalōs](../../strongs/g/g2573.md) on the [sabbaton](../../strongs/g/g4521.md).** [^6]

<a name="matthew_12_13"></a>Matthew 12:13

Then [legō](../../strongs/g/g3004.md) he to the [anthrōpos](../../strongs/g/g444.md), **[ekteinō](../../strongs/g/g1614.md) thine [cheir](../../strongs/g/g5495.md).** And he [ekteinō](../../strongs/g/g1614.md); and it was [apokathistēmi](../../strongs/g/g600.md) [hygiēs](../../strongs/g/g5199.md), like as the other. [^7]

<a name="matthew_12_14"></a>Matthew 12:14

Then the [Pharisaios](../../strongs/g/g5330.md) [exerchomai](../../strongs/g/g1831.md), and [lambanō](../../strongs/g/g2983.md) a [symboulion](../../strongs/g/g4824.md) against him, how they might [apollymi](../../strongs/g/g622.md) him.

<a name="matthew_12_15"></a>Matthew 12:15

But when [Iēsous](../../strongs/g/g2424.md) [ginōskō](../../strongs/g/g1097.md), he [anachōreō](../../strongs/g/g402.md) from thence: and [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) [akoloutheō](../../strongs/g/g190.md) him, and he [therapeuō](../../strongs/g/g2323.md) them all; [^8]

<a name="matthew_12_16"></a>Matthew 12:16

And [epitimaō](../../strongs/g/g2008.md) them that they should not [poieō](../../strongs/g/g4160.md) him [phaneros](../../strongs/g/g5318.md): [^9]

<a name="matthew_12_17"></a>Matthew 12:17

That it might be [plēroō](../../strongs/g/g4137.md) which was [rheō](../../strongs/g/g4483.md) by [Ēsaïas](../../strongs/g/g2268.md) the [prophētēs](../../strongs/g/g4396.md), [legō](../../strongs/g/g3004.md),

<a name="matthew_12_18"></a>Matthew 12:18

[idou](../../strongs/g/g2400.md) my [pais](../../strongs/g/g3816.md), whom I have [hairetizō](../../strongs/g/g140.md); my [agapētos](../../strongs/g/g27.md), in whom my [psychē](../../strongs/g/g5590.md) is [eudokeō](../../strongs/g/g2106.md): I will [tithēmi](../../strongs/g/g5087.md) my [pneuma](../../strongs/g/g4151.md) upon him, and he shall [apaggellō](../../strongs/g/g518.md) [krisis](../../strongs/g/g2920.md) to the [ethnos](../../strongs/g/g1484.md).

<a name="matthew_12_19"></a>Matthew 12:19

He shall not [erizō](../../strongs/g/g2051.md), nor [kraugazō](../../strongs/g/g2905.md); neither shall any [tis](../../strongs/g/g5100.md) [akouō](../../strongs/g/g191.md) his [phōnē](../../strongs/g/g5456.md) in the [plateia](../../strongs/g/g4113.md).

<a name="matthew_12_20"></a>Matthew 12:20

A [syntribō](../../strongs/g/g4937.md) [kalamos](../../strongs/g/g2563.md) shall he not [katagnymi](../../strongs/g/g2608.md), and [typhō](../../strongs/g/g5188.md) [linon](../../strongs/g/g3043.md) shall he not [sbennymi](../../strongs/g/g4570.md), till he [ekballō](../../strongs/g/g1544.md) [krisis](../../strongs/g/g2920.md) unto [nikos](../../strongs/g/g3534.md).

<a name="matthew_12_21"></a>Matthew 12:21

And in his [onoma](../../strongs/g/g3686.md) shall the [ethnos](../../strongs/g/g1484.md) [elpizō](../../strongs/g/g1679.md).

<a name="matthew_12_22"></a>Matthew 12:22

Then was [prospherō](../../strongs/g/g4374.md) unto him [daimonizomai](../../strongs/g/g1139.md), [typhlos](../../strongs/g/g5185.md), and [kōphos](../../strongs/g/g2974.md): and he [therapeuō](../../strongs/g/g2323.md) him, insomuch that the [typhlos](../../strongs/g/g5185.md) and [kōphos](../../strongs/g/g2974.md) both [laleō](../../strongs/g/g2980.md) and [blepō](../../strongs/g/g991.md). [^10]

<a name="matthew_12_23"></a>Matthew 12:23

And all the [ochlos](../../strongs/g/g3793.md) were [existēmi](../../strongs/g/g1839.md), and [legō](../../strongs/g/g3004.md), Is not this the [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md)?

<a name="matthew_12_24"></a>Matthew 12:24

But when the [Pharisaios](../../strongs/g/g5330.md) [akouō](../../strongs/g/g191.md), they [eipon](../../strongs/g/g2036.md), This doth not [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md), but by [Beelzeboul](../../strongs/g/g954.md) the [archōn](../../strongs/g/g758.md) of the [daimonion](../../strongs/g/g1140.md). [^11]

<a name="matthew_12_25"></a>Matthew 12:25

And [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) their [enthymēsis](../../strongs/g/g1761.md), and [eipon](../../strongs/g/g2036.md) unto them, **Every [basileia](../../strongs/g/g932.md) [merizō](../../strongs/g/g3307.md) against itself is [erēmoō](../../strongs/g/g2049.md); and every [polis](../../strongs/g/g4172.md) or [oikia](../../strongs/g/g3614.md) [merizō](../../strongs/g/g3307.md) against itself shall not [histēmi](../../strongs/g/g2476.md):** [^12]

<a name="matthew_12_26"></a>Matthew 12:26

**And if [Satanas](../../strongs/g/g4567.md) [ekballō](../../strongs/g/g1544.md) [Satanas](../../strongs/g/g4567.md), he is [merizō](../../strongs/g/g3307.md) against himself; how shall then his [basileia](../../strongs/g/g932.md) [histēmi](../../strongs/g/g2476.md)?**

<a name="matthew_12_27"></a>Matthew 12:27

**And if I by [Beelzeboul](../../strongs/g/g954.md) [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md), by whom do your [huios](../../strongs/g/g5207.md) [ekballō](../../strongs/g/g1544.md)? therefore they shall be your [kritēs](../../strongs/g/g2923.md).**

<a name="matthew_12_28"></a>Matthew 12:28

**But if I [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md) by the [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md), then the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) is [phthanō](../../strongs/g/g5348.md) unto you.**

<a name="matthew_12_29"></a>Matthew 12:29

**Or else how can one [eiserchomai](../../strongs/g/g1525.md) into [ischyros](../../strongs/g/g2478.md) [oikia](../../strongs/g/g3614.md), and [diarpazō](../../strongs/g/g1283.md) his [skeuos](../../strongs/g/g4632.md), except he first [deō](../../strongs/g/g1210.md) the [ischyros](../../strongs/g/g2478.md)? and then he will [diarpazō](../../strongs/g/g1283.md) his [oikia](../../strongs/g/g3614.md).** [^13]

<a name="matthew_12_30"></a>Matthew 12:30

**He that is not with me is against me; and he that [synagō](../../strongs/g/g4863.md) not with me [skorpizō](../../strongs/g/g4650.md).** [^14]

<a name="matthew_12_31"></a>Matthew 12:31

**Wherefore I [legō](../../strongs/g/g3004.md) unto you, All manner of [hamartia](../../strongs/g/g266.md) and [blasphēmia](../../strongs/g/g988.md) shall be [aphiēmi](../../strongs/g/g863.md) unto [anthrōpos](../../strongs/g/g444.md): but the [blasphēmia](../../strongs/g/g988.md) the [pneuma](../../strongs/g/g4151.md) shall not be [aphiēmi](../../strongs/g/g863.md) unto [anthrōpos](../../strongs/g/g444.md).** [^15]

<a name="matthew_12_32"></a>Matthew 12:32

**And whosoever [eipon](../../strongs/g/g2036.md) a [logos](../../strongs/g/g3056.md) against the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md), it shall be [aphiēmi](../../strongs/g/g863.md) him: but whosoever [eipon](../../strongs/g/g2036.md) against the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), it shall not be [aphiēmi](../../strongs/g/g863.md) him, neither in this [aiōn](../../strongs/g/g165.md), neither in [mellō](../../strongs/g/g3195.md).** [^16]

<a name="matthew_12_33"></a>Matthew 12:33

**Either [poieō](../../strongs/g/g4160.md) the [dendron](../../strongs/g/g1186.md) [kalos](../../strongs/g/g2570.md), and his [karpos](../../strongs/g/g2590.md) [kalos](../../strongs/g/g2570.md); or else [poieō](../../strongs/g/g4160.md) the [dendron](../../strongs/g/g1186.md) [sapros](../../strongs/g/g4550.md), and his [karpos](../../strongs/g/g2590.md) [sapros](../../strongs/g/g4550.md): for the [dendron](../../strongs/g/g1186.md) is [ginōskō](../../strongs/g/g1097.md) by [karpos](../../strongs/g/g2590.md).**

<a name="matthew_12_34"></a>Matthew 12:34

**O [gennēma](../../strongs/g/g1081.md) of [echidna](../../strongs/g/g2191.md), how can ye, being [ponēros](../../strongs/g/g4190.md), speak [agathos](../../strongs/g/g18.md)?  for out of the [perisseuma](../../strongs/g/g4051.md) of the [kardia](../../strongs/g/g2588.md) the [stoma](../../strongs/g/g4750.md) [laleō](../../strongs/g/g2980.md).** [^17]

<a name="matthew_12_35"></a>Matthew 12:35

**An [agathos](../../strongs/g/g18.md) [anthrōpos](../../strongs/g/g444.md) out of the [agathos](../../strongs/g/g18.md) [thēsauros](../../strongs/g/g2344.md) of the [kardia](../../strongs/g/g2588.md) [ekballō](../../strongs/g/g1544.md) [agathos](../../strongs/g/g18.md): and a [ponēros](../../strongs/g/g4190.md) [anthrōpos](../../strongs/g/g444.md) out of the [ponēros](../../strongs/g/g4190.md) [thēsauros](../../strongs/g/g2344.md) [ekballō](../../strongs/g/g1544.md) [ponēros](../../strongs/g/g4190.md).** [^18]

<a name="matthew_12_36"></a>Matthew 12:36

**But I [legō](../../strongs/g/g3004.md) unto you, That every [argos](../../strongs/g/g692.md) [rhēma](../../strongs/g/g4487.md) that [anthrōpos](../../strongs/g/g444.md) shall [laleō](../../strongs/g/g2980.md), they shall [apodidōmi](../../strongs/g/g591.md) [logos](../../strongs/g/g3056.md) thereof in the [hēmera](../../strongs/g/g2250.md) of [krisis](../../strongs/g/g2920.md).**

<a name="matthew_12_37"></a>Matthew 12:37

**For by thy [logos](../../strongs/g/g3056.md) thou shalt be [dikaioō](../../strongs/g/g1344.md), and by thy [logos](../../strongs/g/g3056.md) thou shalt be [katadikazō](../../strongs/g/g2613.md).**

<a name="matthew_12_38"></a>Matthew 12:38

Then certain of the [grammateus](../../strongs/g/g1122.md) and of the [Pharisaios](../../strongs/g/g5330.md) [apokrinomai](../../strongs/g/g611.md), [legō](../../strongs/g/g3004.md), [didaskalos](../../strongs/g/g1320.md), we would [eidō](../../strongs/g/g1492.md) a [sēmeion](../../strongs/g/g4592.md) from thee. [^19]

<a name="matthew_12_39"></a>Matthew 12:39

But he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **A [ponēros](../../strongs/g/g4190.md) and [moichalis](../../strongs/g/g3428.md) [genea](../../strongs/g/g1074.md) [epizēteō](../../strongs/g/g1934.md) a [sēmeion](../../strongs/g/g4592.md); and there shall no [sēmeion](../../strongs/g/g4592.md) be [didōmi](../../strongs/g/g1325.md) to it, but the [sēmeion](../../strongs/g/g4592.md) of the [prophētēs](../../strongs/g/g4396.md) [Iōnas](../../strongs/g/g2495.md):**

<a name="matthew_12_40"></a>Matthew 12:40

**For as [Iōnas](../../strongs/g/g2495.md) was three [hēmera](../../strongs/g/g2250.md) and three [nyx](../../strongs/g/g3571.md) in the [kētos](../../strongs/g/g2785.md) [koilia](../../strongs/g/g2836.md); so shall the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) be three [hēmera](../../strongs/g/g2250.md) and three [nyx](../../strongs/g/g3571.md) in the [kardia](../../strongs/g/g2588.md) of [gē](../../strongs/g/g1093.md).** [^20]

<a name="matthew_12_41"></a>Matthew 12:41

**The [anēr](../../strongs/g/g435.md) of [Nineuitēs](../../strongs/g/g3536.md) shall [anistēmi](../../strongs/g/g450.md) in [krisis](../../strongs/g/g2920.md) with this [genea](../../strongs/g/g1074.md), and shall [katakrinō](../../strongs/g/g2632.md) it: because they [metanoeō](../../strongs/g/g3340.md) at the [kērygma](../../strongs/g/g2782.md) of [Iōnas](../../strongs/g/g2495.md); and, [idou](../../strongs/g/g2400.md), a [pleiōn](../../strongs/g/g4119.md) than [Iōnas](../../strongs/g/g2495.md) is here.**

<a name="matthew_12_42"></a>Matthew 12:42

**The [basilissa](../../strongs/g/g938.md) of the [notos](../../strongs/g/g3558.md) shall [egeirō](../../strongs/g/g1453.md) in the [krisis](../../strongs/g/g2920.md) with this [genea](../../strongs/g/g1074.md), and shall [katakrinō](../../strongs/g/g2632.md) it: for she [erchomai](../../strongs/g/g2064.md) from [peras](../../strongs/g/g4009.md) of [gē](../../strongs/g/g1093.md) to [akouō](../../strongs/g/g191.md) the [sophia](../../strongs/g/g4678.md) of [Solomōn](../../strongs/g/g4672.md); and, [idou](../../strongs/g/g2400.md), a [pleiōn](../../strongs/g/g4119.md) than [Solomōn](../../strongs/g/g4672.md) is here.**

<a name="matthew_12_43"></a>Matthew 12:43

**When the [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md) is [exerchomai](../../strongs/g/g1831.md) out of an [anthrōpos](../../strongs/g/g444.md), he [dierchomai](../../strongs/g/g1330.md) through [anydros](../../strongs/g/g504.md) [topos](../../strongs/g/g5117.md), [zēteō](../../strongs/g/g2212.md) [anapausis](../../strongs/g/g372.md), and [heuriskō](../../strongs/g/g2147.md) none.**

<a name="matthew_12_44"></a>Matthew 12:44

**Then he [legō](../../strongs/g/g3004.md), I will [epistrephō](../../strongs/g/g1994.md) into my [oikos](../../strongs/g/g3624.md) from whence I [exerchomai](../../strongs/g/g1831.md); and when he [erchomai](../../strongs/g/g2064.md), he [heuriskō](../../strongs/g/g2147.md) it [scholazō](../../strongs/g/g4980.md), [saroō](../../strongs/g/g4563.md), and [kosmeō](../../strongs/g/g2885.md).** [^21]

<a name="matthew_12_45"></a>Matthew 12:45

**Then [poreuō](../../strongs/g/g4198.md), and [paralambanō](../../strongs/g/g3880.md) with himself seven other [pneuma](../../strongs/g/g4151.md) [ponēroteros](../../strongs/g/g4191.md) than himself, and they [eiserchomai](../../strongs/g/g1525.md) and [katoikeō](../../strongs/g/g2730.md) there: and the [eschatos](../../strongs/g/g2078.md) [ekeinos](../../strongs/g/g1565.md) [anthrōpos](../../strongs/g/g444.md) [ginomai](../../strongs/g/g1096.md) [cheirōn](../../strongs/g/g5501.md) the [prōtos](../../strongs/g/g4413.md). Even so shall it be also unto this [ponēros](../../strongs/g/g4190.md) [genea](../../strongs/g/g1074.md).**

<a name="matthew_12_46"></a>Matthew 12:46

While he yet [laleō](../../strongs/g/g2980.md) to the [ochlos](../../strongs/g/g3793.md), [idou](../../strongs/g/g2400.md), [mētēr](../../strongs/g/g3384.md) and his [adelphos](../../strongs/g/g80.md) [histēmi](../../strongs/g/g2476.md) [exō](../../strongs/g/g1854.md), [zēteō](../../strongs/g/g2212.md) to [laleō](../../strongs/g/g2980.md) with him. [^22]

<a name="matthew_12_47"></a>Matthew 12:47

Then one [eipon](../../strongs/g/g2036.md) unto him, [idou](../../strongs/g/g2400.md), thy [mētēr](../../strongs/g/g3384.md) and thy [adelphos](../../strongs/g/g80.md) [histēmi](../../strongs/g/g2476.md) [exō](../../strongs/g/g1854.md), [zēteō](../../strongs/g/g2212.md) to [laleō](../../strongs/g/g2980.md) with thee. [^23]

<a name="matthew_12_48"></a>Matthew 12:48

But he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him that told him, **Who is my [mētēr](../../strongs/g/g3384.md)?  and who are my [adelphos](../../strongs/g/g80.md)?**

<a name="matthew_12_49"></a>Matthew 12:49

And he [ekteinō](../../strongs/g/g1614.md) his [cheir](../../strongs/g/g5495.md) toward his [mathētēs](../../strongs/g/g3101.md), and [eipon](../../strongs/g/g2036.md), **[idou](../../strongs/g/g2400.md) my [mētēr](../../strongs/g/g3384.md) and my [adelphos](../../strongs/g/g80.md)!**

<a name="matthew_12_50"></a>Matthew 12:50

**For whosoever shall [poieō](../../strongs/g/g4160.md) the [thelēma](../../strongs/g/g2307.md) of my [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md), the same is my [adelphos](../../strongs/g/g80.md), and [adelphē](../../strongs/g/g79.md), and [mētēr](../../strongs/g/g3384.md).**

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 11](matthew_11.md) - [Matthew 13](matthew_13.md)

---

[^1]: [Matthew 12:1 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_1)

[^2]: [Matthew 12:2 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_2)

[^3]: [Matthew 12:4 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_4)

[^4]: [Matthew 12:9 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_9)

[^5]: [Matthew 12:10 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_10)

[^6]: [Matthew 12:12 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_12)

[^7]: [Matthew 12:13 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_13)

[^8]: [Matthew 12:15 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_15)

[^9]: [Matthew 12:16 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_16)

[^10]: [Matthew 12:22 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_22)

[^11]: [Matthew 12:24 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_24)

[^12]: [Matthew 12:25 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_25)

[^13]: [Matthew 12:29 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_29)

[^14]: [Matthew 12:30 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_30)

[^15]: [Matthew 12:31 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_31)

[^16]: [Matthew 12:32 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_32)

[^17]: [Matthew 12:34 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_34)

[^18]: [Matthew 12:35 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_35)

[^19]: [Matthew 12:38 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_38)

[^20]: [Jonah 1](../jonah/jonah_1.md#jonah_1_17), [Jonah 1 LXX](../jonah/jonah_lxx_1.md#jonah_1_17)

[^21]: [Matthew 12:44 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_44)

[^22]: [Matthew 12:46 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_46)

[^23]: [Matthew 12:47 Commentary](../../commentary/matthew/matthew_12_commentary.md#matthew_12_47)
