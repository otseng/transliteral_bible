# [Matthew 14](https://www.blueletterbible.org/kjv/mat/14/1/rl1/s_943001)

<a name="matthew_14_1"></a>Matthew 14:1

At that [kairos](../../strongs/g/g2540.md) [Hērōdēs](../../strongs/g/g2264.md) the [tetraarchēs](../../strongs/g/g5076.md) [akouō](../../strongs/g/g191.md) of the [akoē](../../strongs/g/g189.md) of [Iēsous](../../strongs/g/g2424.md),

<a name="matthew_14_2"></a>Matthew 14:2

And [eipon](../../strongs/g/g2036.md) unto his [pais](../../strongs/g/g3816.md), This is [Iōannēs](../../strongs/g/g2491.md) the [baptistēs](../../strongs/g/g910.md); he is [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md); and therefore [dynamis](../../strongs/g/g1411.md) [energeō](../../strongs/g/g1754.md) in him.

<a name="matthew_14_3"></a>Matthew 14:3

For [Hērōdēs](../../strongs/g/g2264.md) had [krateō](../../strongs/g/g2902.md) on [Iōannēs](../../strongs/g/g2491.md), and [deō](../../strongs/g/g1210.md) him, and [tithēmi](../../strongs/g/g5087.md) in [phylakē](../../strongs/g/g5438.md) for [Hērōdias](../../strongs/g/g2266.md) sake, his [adelphos](../../strongs/g/g80.md) [Philippos](../../strongs/g/g5376.md) [gynē](../../strongs/g/g1135.md). [^1]

<a name="matthew_14_4"></a>Matthew 14:4

For [Iōannēs](../../strongs/g/g2491.md) [legō](../../strongs/g/g3004.md) unto him, It is not [exesti](../../strongs/g/g1832.md) for thee to have her.

<a name="matthew_14_5"></a>Matthew 14:5

And when he would [apokteinō](../../strongs/g/g615.md) him, he [phobeō](../../strongs/g/g5399.md) the [ochlos](../../strongs/g/g3793.md), because they counted him as a [prophētēs](../../strongs/g/g4396.md).

<a name="matthew_14_6"></a>Matthew 14:6

But when [Hērōdēs](../../strongs/g/g2264.md) [genesia](../../strongs/g/g1077.md) was [agō](../../strongs/g/g71.md), the [thygatēr](../../strongs/g/g2364.md) of [Hērōdias](../../strongs/g/g2266.md) [orcheomai](../../strongs/g/g3738.md) before them, and [areskō](../../strongs/g/g700.md) [Hērōdēs](../../strongs/g/g2264.md). [^2]

<a name="matthew_14_7"></a>Matthew 14:7

Whereupon he [homologeō](../../strongs/g/g3670.md) with an [horkos](../../strongs/g/g3727.md) to [didōmi](../../strongs/g/g1325.md) her whatsoever she would [aiteō](../../strongs/g/g154.md).

<a name="matthew_14_8"></a>Matthew 14:8

And she, [probibazō](../../strongs/g/g4264.md) of her [mētēr](../../strongs/g/g3384.md), [phēmi](../../strongs/g/g5346.md), [didōmi](../../strongs/g/g1325.md) me here [Iōannēs](../../strongs/g/g2491.md) [baptistēs](../../strongs/g/g910.md) [kephalē](../../strongs/g/g2776.md) in a [pinax](../../strongs/g/g4094.md).

<a name="matthew_14_9"></a>Matthew 14:9

And the [basileus](../../strongs/g/g935.md) was [lypeō](../../strongs/g/g3076.md): nevertheless for the [horkos](../../strongs/g/g3727.md), and [synanakeimai](../../strongs/g/g4873.md), he [keleuō](../../strongs/g/g2753.md) it to be [didōmi](../../strongs/g/g1325.md).

<a name="matthew_14_10"></a>Matthew 14:10

And he [pempō](../../strongs/g/g3992.md), and [apokephalizō](../../strongs/g/g607.md) [Iōannēs](../../strongs/g/g2491.md) in the [phylakē](../../strongs/g/g5438.md).

<a name="matthew_14_11"></a>Matthew 14:11

And his [kephalē](../../strongs/g/g2776.md) was [pherō](../../strongs/g/g5342.md) in a [pinax](../../strongs/g/g4094.md), and [didōmi](../../strongs/g/g1325.md) to the [korasion](../../strongs/g/g2877.md): and she [pherō](../../strongs/g/g5342.md) to her [mētēr](../../strongs/g/g3384.md).

<a name="matthew_14_12"></a>Matthew 14:12

And his [mathētēs](../../strongs/g/g3101.md) [proserchomai](../../strongs/g/g4334.md), and [airō](../../strongs/g/g142.md) the [sōma](../../strongs/g/g4983.md), and [thaptō](../../strongs/g/g2290.md) it, and [erchomai](../../strongs/g/g2064.md) and [apaggellō](../../strongs/g/g518.md) [Iēsous](../../strongs/g/g2424.md). [^3]

<a name="matthew_14_13"></a>Matthew 14:13

When [Iēsous](../../strongs/g/g2424.md) [akouō](../../strongs/g/g191.md), he [anachōreō](../../strongs/g/g402.md) thence by [ploion](../../strongs/g/g4143.md) into an [erēmos](../../strongs/g/g2048.md) [topos](../../strongs/g/g5117.md) apart: and when the [ochlos](../../strongs/g/g3793.md) had [akouō](../../strongs/g/g191.md), they [akoloutheō](../../strongs/g/g190.md) him [pezē](../../strongs/g/g3979.md) out of the [polis](../../strongs/g/g4172.md).

<a name="matthew_14_14"></a>Matthew 14:14

And [Iēsous](../../strongs/g/g2424.md) [exerchomai](../../strongs/g/g1831.md), and [eidō](../../strongs/g/g1492.md) a [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md), and was [splagchnizomai](../../strongs/g/g4697.md) toward them, and he [therapeuō](../../strongs/g/g2323.md) their [arrōstos](../../strongs/g/g732.md). [^4]

<a name="matthew_14_15"></a>Matthew 14:15

And when it was [opsios](../../strongs/g/g3798.md), his [mathētēs](../../strongs/g/g3101.md) [proserchomai](../../strongs/g/g4334.md) to him, [legō](../../strongs/g/g3004.md), This is an [erēmos](../../strongs/g/g2048.md) [topos](../../strongs/g/g5117.md), and the [hōra](../../strongs/g/g5610.md) is now [parerchomai](../../strongs/g/g3928.md); [apolyō](../../strongs/g/g630.md) the [ochlos](../../strongs/g/g3793.md) away, that they may [aperchomai](../../strongs/g/g565.md) into the [kōmē](../../strongs/g/g2968.md), and [agorazō](../../strongs/g/g59.md) themselves [brōma](../../strongs/g/g1033.md).

<a name="matthew_14_16"></a>Matthew 14:16

But [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **They [chreia](../../strongs/g/g5532.md) not [aperchomai](../../strongs/g/g565.md); [didōmi](../../strongs/g/g1325.md) ye them to [phago](../../strongs/g/g5315.md).** [^5]

<a name="matthew_14_17"></a>Matthew 14:17

And they [legō](../../strongs/g/g3004.md) unto him, We have here but five [artos](../../strongs/g/g740.md), and two [ichthys](../../strongs/g/g2486.md).

<a name="matthew_14_18"></a>Matthew 14:18

He [eipon](../../strongs/g/g2036.md), **[pherō](../../strongs/g/g5342.md) them [hōde](../../strongs/g/g5602.md) to me.**

<a name="matthew_14_19"></a>Matthew 14:19

And he [keleuō](../../strongs/g/g2753.md) the [ochlos](../../strongs/g/g3793.md) to [anaklinō](../../strongs/g/g347.md) on the [chortos](../../strongs/g/g5528.md), and [lambanō](../../strongs/g/g2983.md) the five [artos](../../strongs/g/g740.md), and the two [ichthys](../../strongs/g/g2486.md), and [anablepō](../../strongs/g/g308.md) to [ouranos](../../strongs/g/g3772.md), he [eulogeō](../../strongs/g/g2127.md), and [klaō](../../strongs/g/g2806.md), and [didōmi](../../strongs/g/g1325.md) the [artos](../../strongs/g/g740.md) to [mathētēs](../../strongs/g/g3101.md), and the [mathētēs](../../strongs/g/g3101.md) to the [ochlos](../../strongs/g/g3793.md).

<a name="matthew_14_20"></a>Matthew 14:20

And they did all [phago](../../strongs/g/g5315.md), and were [chortazō](../../strongs/g/g5526.md): and they [airō](../../strongs/g/g142.md) of the [klasma](../../strongs/g/g2801.md) that [perisseuō](../../strongs/g/g4052.md) twelve [kophinos](../../strongs/g/g2894.md) [plērēs](../../strongs/g/g4134.md).

<a name="matthew_14_21"></a>Matthew 14:21

And [esthiō](../../strongs/g/g2068.md) were about five thousand [anēr](../../strongs/g/g435.md), beside [gynē](../../strongs/g/g1135.md) and [paidion](../../strongs/g/g3813.md).

<a name="matthew_14_22"></a>Matthew 14:22

And [eutheōs](../../strongs/g/g2112.md) [Iēsous](../../strongs/g/g2424.md) [anagkazō](../../strongs/g/g315.md) his [mathētēs](../../strongs/g/g3101.md) to [embainō](../../strongs/g/g1684.md) into a [ploion](../../strongs/g/g4143.md), and to [proagō](../../strongs/g/g4254.md) him unto the other side, while he [apolyō](../../strongs/g/g630.md) the [ochlos](../../strongs/g/g3793.md) away. [^6]

<a name="matthew_14_23"></a>Matthew 14:23

And when he had [apolyō](../../strongs/g/g630.md) the [ochlos](../../strongs/g/g3793.md), he [anabainō](../../strongs/g/g305.md) into a [oros](../../strongs/g/g3735.md) [kata](../../strongs/g/g2596.md) [idios](../../strongs/g/g2398.md) to [proseuchomai](../../strongs/g/g4336.md): and when the [opsios](../../strongs/g/g3798.md) was [ginomai](../../strongs/g/g1096.md), he was there [monos](../../strongs/g/g3441.md).

<a name="matthew_14_24"></a>Matthew 14:24

But the [ploion](../../strongs/g/g4143.md) was now in the [mesos](../../strongs/g/g3319.md) of the [thalassa](../../strongs/g/g2281.md), [basanizō](../../strongs/g/g928.md) with [kyma](../../strongs/g/g2949.md): for the [anemos](../../strongs/g/g417.md) was [enantios](../../strongs/g/g1727.md). [^7]

<a name="matthew_14_25"></a>Matthew 14:25

And in the fourth [phylakē](../../strongs/g/g5438.md) of the [nyx](../../strongs/g/g3571.md) [Iēsous](../../strongs/g/g2424.md) [aperchomai](../../strongs/g/g565.md) unto them, [peripateō](../../strongs/g/g4043.md) on the [thalassa](../../strongs/g/g2281.md).

<a name="matthew_14_26"></a>Matthew 14:26

And when the [mathētēs](../../strongs/g/g3101.md) [eidō](../../strongs/g/g1492.md) him [peripateō](../../strongs/g/g4043.md) on the [thalassa](../../strongs/g/g2281.md), they were [tarassō](../../strongs/g/g5015.md), [legō](../../strongs/g/g3004.md), It is a [phantasma](../../strongs/g/g5326.md); and they [krazō](../../strongs/g/g2896.md) for [phobos](../../strongs/g/g5401.md). [^8]

<a name="matthew_14_27"></a>Matthew 14:27

But [eutheōs](../../strongs/g/g2112.md) [Iēsous](../../strongs/g/g2424.md) [laleō](../../strongs/g/g2980.md) unto them, [legō](../../strongs/g/g3004.md), **[tharseō](../../strongs/g/g2293.md); it is I; be not [phobeō](../../strongs/g/g5399.md).** [^9]

<a name="matthew_14_28"></a>Matthew 14:28

And [Petros](../../strongs/g/g4074.md) [apokrinomai](../../strongs/g/g611.md) him and [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), if it be thou, [keleuō](../../strongs/g/g2753.md) me [erchomai](../../strongs/g/g2064.md) unto thee on the [hydōr](../../strongs/g/g5204.md).

<a name="matthew_14_29"></a>Matthew 14:29

And he [eipon](../../strongs/g/g2036.md), **[erchomai](../../strongs/g/g2064.md).** And when [Petros](../../strongs/g/g4074.md) was [katabainō](../../strongs/g/g2597.md) out of the [ploion](../../strongs/g/g4143.md), he [peripateō](../../strongs/g/g4043.md) on the [hydōr](../../strongs/g/g5204.md), to [erchomai](../../strongs/g/g2064.md) to [Iēsous](../../strongs/g/g2424.md).

<a name="matthew_14_30"></a>Matthew 14:30

But when he [blepō](../../strongs/g/g991.md) the [anemos](../../strongs/g/g417.md) [ischyros](../../strongs/g/g2478.md), he was [phobeō](../../strongs/g/g5399.md); and [archomai](../../strongs/g/g756.md) to [katapontizō](../../strongs/g/g2670.md), he [krazō](../../strongs/g/g2896.md), [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), [sōzō](../../strongs/g/g4982.md) me. [^10]

<a name="matthew_14_31"></a>Matthew 14:31

And [eutheōs](../../strongs/g/g2112.md) [Iēsous](../../strongs/g/g2424.md) [ekteinō](../../strongs/g/g1614.md) [cheir](../../strongs/g/g5495.md), and [epilambanomai](../../strongs/g/g1949.md) him, and [legō](../../strongs/g/g3004.md) unto him, **[oligopistos](../../strongs/g/g3640.md), wherefore didst thou [distazō](../../strongs/g/g1365.md)?**

<a name="matthew_14_32"></a>Matthew 14:32

And when they [embainō](../../strongs/g/g1684.md) into the [ploion](../../strongs/g/g4143.md), the [anemos](../../strongs/g/g417.md) [kopazō](../../strongs/g/g2869.md). [^11]

<a name="matthew_14_33"></a>Matthew 14:33

Then they that were in the [ploion](../../strongs/g/g4143.md) [erchomai](../../strongs/g/g2064.md) and [proskyneō](../../strongs/g/g4352.md) him, [legō](../../strongs/g/g3004.md), [alēthōs](../../strongs/g/g230.md) thou art the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md). [^12]

<a name="matthew_14_34"></a>Matthew 14:34

And when they were [diaperaō](../../strongs/g/g1276.md), they [erchomai](../../strongs/g/g2064.md) into the [gē](../../strongs/g/g1093.md) of [Gennēsaret](../../strongs/g/g1082.md).

<a name="matthew_14_35"></a>Matthew 14:35

And when the [anēr](../../strongs/g/g435.md) of that [topos](../../strongs/g/g5117.md) [epiginōskō](../../strongs/g/g1921.md) of him, they [apostellō](../../strongs/g/g649.md) into all that [perichōros](../../strongs/g/g4066.md), and [prospherō](../../strongs/g/g4374.md) unto him all that were [kakōs](../../strongs/g/g2560.md);

<a name="matthew_14_36"></a>Matthew 14:36

And [parakaleō](../../strongs/g/g3870.md) him that they might only [haptomai](../../strongs/g/g680.md) the [kraspedon](../../strongs/g/g2899.md) of his [himation](../../strongs/g/g2440.md): and as many as [haptomai](../../strongs/g/g680.md) were [diasōzō](../../strongs/g/g1295.md).

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 13](matthew_13.md) - [Matthew 15](matthew_15.md)

---

[^1]: [Matthew 14:3 Commentary](../../commentary/matthew/matthew_14_commentary.md#matthew_14_3)

[^2]: [Matthew 14:6 Commentary](../../commentary/matthew/matthew_14_commentary.md#matthew_14_6)

[^3]: [Matthew 14:12 Commentary](../../commentary/matthew/matthew_14_commentary.md#matthew_14_12)

[^4]: [Matthew 14:14 Commentary](../../commentary/matthew/matthew_14_commentary.md#matthew_14_14)

[^5]: [Matthew 14:16 Commentary](../../commentary/matthew/matthew_14_commentary.md#matthew_14_16)

[^6]: [Matthew 14:22 Commentary](../../commentary/matthew/matthew_14_commentary.md#matthew_14_22)

[^7]: [Matthew 14:24 Commentary](../../commentary/matthew/matthew_14_commentary.md#matthew_14_24)

[^8]: [Matthew 14:26 Commentary](../../commentary/matthew/matthew_14_commentary.md#matthew_14_26)

[^9]: [Matthew 14:27 Commentary](../../commentary/matthew/matthew_14_commentary.md#matthew_14_27)

[^10]: [Matthew 14:30 Commentary](../../commentary/matthew/matthew_14_commentary.md#matthew_14_30)

[^11]: [Matthew 14:32 Commentary](../../commentary/matthew/matthew_14_commentary.md#matthew_14_32)

[^12]: [Matthew 14:33 Commentary](../../commentary/matthew/matthew_14_commentary.md#matthew_14_33)
