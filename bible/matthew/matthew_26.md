# [Matthew 26](https://www.blueletterbible.org/kjv/mat/26/1/rl1/s_955001)

<a name="matthew_26_1"></a>Matthew 26:1

And [ginomai](../../strongs/g/g1096.md), when [Iēsous](../../strongs/g/g2424.md) had [teleō](../../strongs/g/g5055.md) all these [logos](../../strongs/g/g3056.md), he [eipon](../../strongs/g/g2036.md) unto his [mathētēs](../../strongs/g/g3101.md),

<a name="matthew_26_2"></a>Matthew 26:2

**Ye [eidō](../../strongs/g/g1492.md) that after two [hēmera](../../strongs/g/g2250.md) is the [pascha](../../strongs/g/g3957.md), and the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) is [paradidōmi](../../strongs/g/g3860.md) to be [stauroō](../../strongs/g/g4717.md).**

<a name="matthew_26_3"></a>Matthew 26:3

Then [synagō](../../strongs/g/g4863.md) the [archiereus](../../strongs/g/g749.md), and the [grammateus](../../strongs/g/g1122.md), and the [presbyteros](../../strongs/g/g4245.md) of the [laos](../../strongs/g/g2992.md), unto the [aulē](../../strongs/g/g833.md) of the [archiereus](../../strongs/g/g749.md), who was [legō](../../strongs/g/g3004.md) [Kaïaphas](../../strongs/g/g2533.md),

<a name="matthew_26_4"></a>Matthew 26:4

And [symbouleuō](../../strongs/g/g4823.md) that they might [krateō](../../strongs/g/g2902.md) [Iēsous](../../strongs/g/g2424.md) by [dolos](../../strongs/g/g1388.md), and [apokteinō](../../strongs/g/g615.md).

<a name="matthew_26_5"></a>Matthew 26:5

But they [legō](../../strongs/g/g3004.md), Not on the [heortē](../../strongs/g/g1859.md), lest there be a [thorybos](../../strongs/g/g2351.md) among the [laos](../../strongs/g/g2992.md).

<a name="matthew_26_6"></a>Matthew 26:6

Now when [Iēsous](../../strongs/g/g2424.md) was in [Bēthania](../../strongs/g/g963.md), in the [oikia](../../strongs/g/g3614.md) of [Simōn](../../strongs/g/g4613.md) the [lepros](../../strongs/g/g3015.md),

<a name="matthew_26_7"></a>Matthew 26:7

There [proserchomai](../../strongs/g/g4334.md) unto him a [gynē](../../strongs/g/g1135.md) having [alabastron](../../strongs/g/g211.md) of [barytimos](../../strongs/g/g927.md) [myron](../../strongs/g/g3464.md), and [katacheō](../../strongs/g/g2708.md) on his [kephalē](../../strongs/g/g2776.md), as he [anakeimai](../../strongs/g/g345.md).

<a name="matthew_26_8"></a>Matthew 26:8

But when his [mathētēs](../../strongs/g/g3101.md) [eidō](../../strongs/g/g1492.md), they had [aganakteō](../../strongs/g/g23.md), [legō](../../strongs/g/g3004.md), To what purpose this [apōleia](../../strongs/g/g684.md)?

<a name="matthew_26_9"></a>Matthew 26:9

For this [myron](../../strongs/g/g3464.md) might have been [pipraskō](../../strongs/g/g4097.md) for [polys](../../strongs/g/g4183.md), and [didōmi](../../strongs/g/g1325.md) to the [ptōchos](../../strongs/g/g4434.md).

<a name="matthew_26_10"></a>Matthew 26:10

When [Iēsous](../../strongs/g/g2424.md) [ginōskō](../../strongs/g/g1097.md), he [eipon](../../strongs/g/g2036.md) unto them, **Why [kopos](../../strongs/g/g2873.md) [parechō](../../strongs/g/g3930.md) the [gynē](../../strongs/g/g1135.md)? for she hath [ergazomai](../../strongs/g/g2038.md) a [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md) upon me.**

<a name="matthew_26_11"></a>Matthew 26:11

**For ye have the [ptōchos](../../strongs/g/g4434.md) [pantote](../../strongs/g/g3842.md) with you; but me ye have not [pantote](../../strongs/g/g3842.md).**

<a name="matthew_26_12"></a>Matthew 26:12

**For in that she hath [ballō](../../strongs/g/g906.md) this [myron](../../strongs/g/g3464.md) on my [sōma](../../strongs/g/g4983.md), she [poieō](../../strongs/g/g4160.md) for my [entaphiazō](../../strongs/g/g1779.md).**

<a name="matthew_26_13"></a>Matthew 26:13

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, Wheresoever this [euaggelion](../../strongs/g/g2098.md) shall be [kēryssō](../../strongs/g/g2784.md) in the [holos](../../strongs/g/g3650.md) [kosmos](../../strongs/g/g2889.md), there shall also this, that this woman hath [poieō](../../strongs/g/g4160.md), be [laleō](../../strongs/g/g2980.md) for a [mnēmosynon](../../strongs/g/g3422.md) of her.**

<a name="matthew_26_14"></a>Matthew 26:14

Then one of the [dōdeka](../../strongs/g/g1427.md), [legō](../../strongs/g/g3004.md) [Ioudas](../../strongs/g/g2455.md) [Iskariōth](../../strongs/g/g2469.md), [poreuō](../../strongs/g/g4198.md) unto the [archiereus](../../strongs/g/g749.md),

<a name="matthew_26_15"></a>Matthew 26:15

And [eipon](../../strongs/g/g2036.md), What will ye [didōmi](../../strongs/g/g1325.md) me, and I will [paradidōmi](../../strongs/g/g3860.md) him unto you? And they [histēmi](../../strongs/g/g2476.md) with him for thirty [argyrion](../../strongs/g/g694.md).

<a name="matthew_26_16"></a>Matthew 26:16

And from that time he [zēteō](../../strongs/g/g2212.md) [eukairia](../../strongs/g/g2120.md) to [paradidōmi](../../strongs/g/g3860.md) him.

<a name="matthew_26_17"></a>Matthew 26:17

Now the first of the [azymos](../../strongs/g/g106.md) the [mathētēs](../../strongs/g/g3101.md) [proserchomai](../../strongs/g/g4334.md) to [Iēsous](../../strongs/g/g2424.md), [legō](../../strongs/g/g3004.md) unto him, Where wilt thou that we [hetoimazō](../../strongs/g/g2090.md) for thee to [phago](../../strongs/g/g5315.md) the [pascha](../../strongs/g/g3957.md)?

<a name="matthew_26_18"></a>Matthew 26:18

And he [eipon](../../strongs/g/g2036.md), **[hypagō](../../strongs/g/g5217.md) into the [polis](../../strongs/g/g4172.md) to [deina](../../strongs/g/g1170.md), and [eipon](../../strongs/g/g2036.md) unto him, The [didaskalos](../../strongs/g/g1320.md) [legō](../../strongs/g/g3004.md), My [kairos](../../strongs/g/g2540.md) is [eggys](../../strongs/g/g1451.md); I will [poieō](../../strongs/g/g4160.md) the [pascha](../../strongs/g/g3957.md) at [se](../../strongs/g/g4571.md) with my [mathētēs](../../strongs/g/g3101.md).**

<a name="matthew_26_19"></a>Matthew 26:19

And the [mathētēs](../../strongs/g/g3101.md) [poieō](../../strongs/g/g4160.md) as [Iēsous](../../strongs/g/g2424.md) had [syntassō](../../strongs/g/g4929.md) them; and they [hetoimazō](../../strongs/g/g2090.md) the [pascha](../../strongs/g/g3957.md).

<a name="matthew_26_20"></a>Matthew 26:20

Now when the [opsios](../../strongs/g/g3798.md) was [ginomai](../../strongs/g/g1096.md), he [anakeimai](../../strongs/g/g345.md) with the [dōdeka](../../strongs/g/g1427.md).

<a name="matthew_26_21"></a>Matthew 26:21

And as they did [esthiō](../../strongs/g/g2068.md), he [eipon](../../strongs/g/g2036.md), **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, that one of you shall [paradidōmi](../../strongs/g/g3860.md) me.**

<a name="matthew_26_22"></a>Matthew 26:22

And they were [sphodra](../../strongs/g/g4970.md) [lypeō](../../strongs/g/g3076.md), and [archomai](../../strongs/g/g756.md) [hekastos](../../strongs/g/g1538.md) of them to [legō](../../strongs/g/g3004.md) unto him, [kyrios](../../strongs/g/g2962.md), is it I?

<a name="matthew_26_23"></a>Matthew 26:23

And he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), **He that [embaptō](../../strongs/g/g1686.md) [cheir](../../strongs/g/g5495.md) with me in the [tryblion](../../strongs/g/g5165.md), the same shall [paradidōmi](../../strongs/g/g3860.md) me.**

<a name="matthew_26_24"></a>Matthew 26:24

**The [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [hypagō](../../strongs/g/g5217.md) as it is [graphō](../../strongs/g/g1125.md) of him: but [ouai](../../strongs/g/g3759.md) unto that [anthrōpos](../../strongs/g/g444.md) by whom the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) is [paradidōmi](../../strongs/g/g3860.md)! it had been [kalos](../../strongs/g/g2570.md) for that [autos](../../strongs/g/g846.md) if he had not been [gennaō](../../strongs/g/g1080.md).**

<a name="matthew_26_25"></a>Matthew 26:25

Then [Ioudas](../../strongs/g/g2455.md), which [paradidōmi](../../strongs/g/g3860.md) him, [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), [rhabbi](../../strongs/g/g4461.md), is it I?  He [legō](../../strongs/g/g3004.md) unto him, **Thou hast [eipon](../../strongs/g/g2036.md).**

<a name="matthew_26_26"></a>Matthew 26:26

And as they were [esthiō](../../strongs/g/g2068.md), [Iēsous](../../strongs/g/g2424.md) [lambanō](../../strongs/g/g2983.md) [artos](../../strongs/g/g740.md), and [eulogeō](../../strongs/g/g2127.md), and [klaō](../../strongs/g/g2806.md), and [didōmi](../../strongs/g/g1325.md) to the [mathētēs](../../strongs/g/g3101.md), and [eipon](../../strongs/g/g2036.md), **[lambanō](../../strongs/g/g2983.md), [phago](../../strongs/g/g5315.md); this is my [sōma](../../strongs/g/g4983.md).**

<a name="matthew_26_27"></a>Matthew 26:27

And he [lambanō](../../strongs/g/g2983.md) the [potērion](../../strongs/g/g4221.md), and [eucharisteō](../../strongs/g/g2168.md), and [didōmi](../../strongs/g/g1325.md) to them, [legō](../../strongs/g/g3004.md), **[pinō](../../strongs/g/g4095.md) ye all of it;**

<a name="matthew_26_28"></a>Matthew 26:28

**For this is my [haima](../../strongs/g/g129.md) of the [kainos](../../strongs/g/g2537.md) [diathēkē](../../strongs/g/g1242.md), which is [ekcheō](../../strongs/g/g1632.md) for [polys](../../strongs/g/g4183.md) for the [aphesis](../../strongs/g/g859.md) of [hamartia](../../strongs/g/g266.md).** [^1]

<a name="matthew_26_29"></a>Matthew 26:29

**But I [legō](../../strongs/g/g3004.md) unto you, I will not [pinō](../../strongs/g/g4095.md) henceforth of this [gennēma](../../strongs/g/g1081.md) of the [ampelos](../../strongs/g/g288.md), until that [hēmera](../../strongs/g/g2250.md) when I [pinō](../../strongs/g/g4095.md) it [kainos](../../strongs/g/g2537.md) with you in my [patēr](../../strongs/g/g3962.md) [basileia](../../strongs/g/g932.md).**

<a name="matthew_26_30"></a>Matthew 26:30

And [hymneō](../../strongs/g/g5214.md), they [exerchomai](../../strongs/g/g1831.md) into the [oros](../../strongs/g/g3735.md) of [elaia](../../strongs/g/g1636.md).

<a name="matthew_26_31"></a>Matthew 26:31

Then [legō](../../strongs/g/g3004.md) [Iēsous](../../strongs/g/g2424.md) unto them, **All ye shall be [skandalizō](../../strongs/g/g4624.md) because of me this [nyx](../../strongs/g/g3571.md): for it is [graphō](../../strongs/g/g1125.md), I will [patassō](../../strongs/g/g3960.md) the [poimēn](../../strongs/g/g4166.md), and the [probaton](../../strongs/g/g4263.md) of the [poimnē](../../strongs/g/g4167.md) shall be [diaskorpizō](../../strongs/g/g1287.md).**

<a name="matthew_26_32"></a>Matthew 26:32

**But after I am [egeirō](../../strongs/g/g1453.md), I will [proagō](../../strongs/g/g4254.md) you into [Galilaia](../../strongs/g/g1056.md).**

<a name="matthew_26_33"></a>Matthew 26:33

[Petros](../../strongs/g/g4074.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, Though all shall be [skandalizō](../../strongs/g/g4624.md) because of thee, will I never be [skandalizō](../../strongs/g/g4624.md).

<a name="matthew_26_34"></a>Matthew 26:34

[Iēsous](../../strongs/g/g2424.md) [phēmi](../../strongs/g/g5346.md) unto him, **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto thee, That this [nyx](../../strongs/g/g3571.md), before the [alektōr](../../strongs/g/g220.md) [phōneō](../../strongs/g/g5455.md), thou shalt [aparneomai](../../strongs/g/g533.md) me thrice.**

<a name="matthew_26_35"></a>Matthew 26:35

[Petros](../../strongs/g/g4074.md) [legō](../../strongs/g/g3004.md) unto him, Though I should [apothnēskō](../../strongs/g/g599.md) with thee, yet will I not [aparneomai](../../strongs/g/g533.md) thee. [homoiōs](../../strongs/g/g3668.md) also [eipon](../../strongs/g/g2036.md) all the [mathētēs](../../strongs/g/g3101.md).

<a name="matthew_26_36"></a>Matthew 26:36

Then [erchomai](../../strongs/g/g2064.md) [Iēsous](../../strongs/g/g2424.md) with them unto a [chōrion](../../strongs/g/g5564.md) [legō](../../strongs/g/g3004.md) [Gethsēmani](../../strongs/g/g1068.md), and [legō](../../strongs/g/g3004.md) unto the [mathētēs](../../strongs/g/g3101.md), **[kathizō](../../strongs/g/g2523.md) here, while I [aperchomai](../../strongs/g/g565.md) and [proseuchomai](../../strongs/g/g4336.md) yonder.**

<a name="matthew_26_37"></a>Matthew 26:37

And he [paralambanō](../../strongs/g/g3880.md) with him [Petros](../../strongs/g/g4074.md) and the two [huios](../../strongs/g/g5207.md) of [Zebedaios](../../strongs/g/g2199.md), and [archomai](../../strongs/g/g756.md) to be [lypeō](../../strongs/g/g3076.md) and [adēmoneō](../../strongs/g/g85.md).

<a name="matthew_26_38"></a>Matthew 26:38

Then [legō](../../strongs/g/g3004.md) he unto them, **My [psychē](../../strongs/g/g5590.md) is [perilypos](../../strongs/g/g4036.md), even unto [thanatos](../../strongs/g/g2288.md): [menō](../../strongs/g/g3306.md) here, and [grēgoreō](../../strongs/g/g1127.md) with me.**

<a name="matthew_26_39"></a>Matthew 26:39

And he [proerchomai](../../strongs/g/g4281.md) a [mikron](../../strongs/g/g3397.md), and [piptō](../../strongs/g/g4098.md) on his [prosōpon](../../strongs/g/g4383.md), and [proseuchomai](../../strongs/g/g4336.md), [legō](../../strongs/g/g3004.md), **O my [patēr](../../strongs/g/g3962.md), if it be [dynatos](../../strongs/g/g1415.md), let this [potērion](../../strongs/g/g4221.md) [parerchomai](../../strongs/g/g3928.md) from me: nevertheless not as I [thelō](../../strongs/g/g2309.md), but as thou.**

<a name="matthew_26_40"></a>Matthew 26:40

And he [erchomai](../../strongs/g/g2064.md) unto the [mathētēs](../../strongs/g/g3101.md), and [heuriskō](../../strongs/g/g2147.md) them [katheudō](../../strongs/g/g2518.md), and [legō](../../strongs/g/g3004.md) unto [Petros](../../strongs/g/g4074.md), **[houtō(s)](../../strongs/g/g3779.md), could ye not [grēgoreō](../../strongs/g/g1127.md) with me one [hōra](../../strongs/g/g5610.md)?**

<a name="matthew_26_41"></a>Matthew 26:41

**[grēgoreō](../../strongs/g/g1127.md) and [proseuchomai](../../strongs/g/g4336.md), that ye [eiserchomai](../../strongs/g/g1525.md) not into [peirasmos](../../strongs/g/g3986.md): the [pneuma](../../strongs/g/g4151.md) indeed is [prothymos](../../strongs/g/g4289.md), but the [sarx](../../strongs/g/g4561.md) is [asthenēs](../../strongs/g/g772.md).**

<a name="matthew_26_42"></a>Matthew 26:42

He [aperchomai](../../strongs/g/g565.md) again the second time, and [proseuchomai](../../strongs/g/g4336.md), [legō](../../strongs/g/g3004.md), **O my [patēr](../../strongs/g/g3962.md), if this [potērion](../../strongs/g/g4221.md) may not [parerchomai](../../strongs/g/g3928.md) from me, except I [pinō](../../strongs/g/g4095.md) it, thy [thelēma](../../strongs/g/g2307.md) be [ginomai](../../strongs/g/g1096.md).**

<a name="matthew_26_43"></a>Matthew 26:43

And he [erchomai](../../strongs/g/g2064.md) and [heuriskō](../../strongs/g/g2147.md) them [katheudō](../../strongs/g/g2518.md) again: for their [ophthalmos](../../strongs/g/g3788.md) were [bareō](../../strongs/g/g916.md).

<a name="matthew_26_44"></a>Matthew 26:44

And he [aphiēmi](../../strongs/g/g863.md) them, and [aperchomai](../../strongs/g/g565.md) again, and [proseuchomai](../../strongs/g/g4336.md) the third time, [eipon](../../strongs/g/g2036.md) the same [logos](../../strongs/g/g3056.md).

<a name="matthew_26_45"></a>Matthew 26:45

Then [erchomai](../../strongs/g/g2064.md) he to his [mathētēs](../../strongs/g/g3101.md), and [legō](../../strongs/g/g3004.md) unto them, **[katheudō](../../strongs/g/g2518.md) now, and [anapauō](../../strongs/g/g373.md): [idou](../../strongs/g/g2400.md), the [hōra](../../strongs/g/g5610.md) is [eggizō](../../strongs/g/g1448.md), and the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) is [paradidōmi](../../strongs/g/g3860.md) into the [cheir](../../strongs/g/g5495.md) of [hamartōlos](../../strongs/g/g268.md).**

<a name="matthew_26_46"></a>Matthew 26:46

**[egeirō](../../strongs/g/g1453.md), [agō](../../strongs/g/g71.md): [idou](../../strongs/g/g2400.md), he is [eggizō](../../strongs/g/g1448.md) that doth [paradidōmi](../../strongs/g/g3860.md) me.**

<a name="matthew_26_47"></a>Matthew 26:47

And while he yet [laleō](../../strongs/g/g2980.md), [idou](../../strongs/g/g2400.md), [Ioudas](../../strongs/g/g2455.md), one of the [dōdeka](../../strongs/g/g1427.md), [erchomai](../../strongs/g/g2064.md), and with him a [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) with [machaira](../../strongs/g/g3162.md) and [xylon](../../strongs/g/g3586.md), from the [archiereus](../../strongs/g/g749.md) and [presbyteros](../../strongs/g/g4245.md) of the [laos](../../strongs/g/g2992.md).

<a name="matthew_26_48"></a>Matthew 26:48

Now he that [paradidōmi](../../strongs/g/g3860.md) him [didōmi](../../strongs/g/g1325.md) them a [sēmeion](../../strongs/g/g4592.md), [legō](../../strongs/g/g3004.md), Whomsoever I shall [phileō](../../strongs/g/g5368.md), that same is he: [krateō](../../strongs/g/g2902.md) him.

<a name="matthew_26_49"></a>Matthew 26:49

And [eutheōs](../../strongs/g/g2112.md) he [proserchomai](../../strongs/g/g4334.md) to [Iēsous](../../strongs/g/g2424.md), and [eipon](../../strongs/g/g2036.md), [chairō](../../strongs/g/g5463.md), [rhabbi](../../strongs/g/g4461.md); and [kataphileō](../../strongs/g/g2705.md) him.

<a name="matthew_26_50"></a>Matthew 26:50

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **[hetairos](../../strongs/g/g2083.md), wherefore art thou [pareimi](../../strongs/g/g3918.md)?** Then [proserchomai](../../strongs/g/g4334.md) they, and [epiballō](../../strongs/g/g1911.md) [cheir](../../strongs/g/g5495.md) on [Iēsous](../../strongs/g/g2424.md) and [krateō](../../strongs/g/g2902.md) him.

<a name="matthew_26_51"></a>Matthew 26:51

And, [idou](../../strongs/g/g2400.md), one of them which were with [Iēsous](../../strongs/g/g2424.md) [ekteinō](../../strongs/g/g1614.md) his [cheir](../../strongs/g/g5495.md), and [apospaō](../../strongs/g/g645.md) his [machaira](../../strongs/g/g3162.md), and [patassō](../../strongs/g/g3960.md) a [doulos](../../strongs/g/g1401.md) of the [archiereus](../../strongs/g/g749.md), and [aphaireō](../../strongs/g/g851.md) his [ōtion](../../strongs/g/g5621.md).

<a name="matthew_26_52"></a>Matthew 26:52

Then [legō](../../strongs/g/g3004.md) [Iēsous](../../strongs/g/g2424.md) unto him, **[apostrephō](../../strongs/g/g654.md) thy [machaira](../../strongs/g/g3162.md) into his [topos](../../strongs/g/g5117.md): for all they that [lambanō](../../strongs/g/g2983.md) the [machaira](../../strongs/g/g3162.md) shall [apollymi](../../strongs/g/g622.md) with the [machaira](../../strongs/g/g3162.md).**

<a name="matthew_26_53"></a>Matthew 26:53

**[dokeō](../../strongs/g/g1380.md) thou that I cannot now [parakaleō](../../strongs/g/g3870.md) to my [patēr](../../strongs/g/g3962.md), and he shall [paristēmi](../../strongs/g/g3936.md) me more than [dōdeka](../../strongs/g/g1427.md) [legiōn](../../strongs/g/g3003.md) of [aggelos](../../strongs/g/g32.md)?**

<a name="matthew_26_54"></a>Matthew 26:54

**But how then shall the [graphē](../../strongs/g/g1124.md) be [plēroō](../../strongs/g/g4137.md), that thus it must [ginomai](../../strongs/g/g1096.md)?**

<a name="matthew_26_55"></a>Matthew 26:55

In that [ekeinos](../../strongs/g/g1565.md) [hōra](../../strongs/g/g5610.md) [eipon](../../strongs/g/g2036.md) [Iēsous](../../strongs/g/g2424.md) to the [ochlos](../../strongs/g/g3793.md), **Are ye [exerchomai](../../strongs/g/g1831.md) as against a [lēstēs](../../strongs/g/g3027.md) with [machaira](../../strongs/g/g3162.md) and [xylon](../../strongs/g/g3586.md) for to [syllambanō](../../strongs/g/g4815.md) me? I [kathezomai](../../strongs/g/g2516.md) [hēmera](../../strongs/g/g2250.md) with you [didaskō](../../strongs/g/g1321.md) in the [hieron](../../strongs/g/g2411.md), and ye [krateō](../../strongs/g/g2902.md) no on me.**

<a name="matthew_26_56"></a>Matthew 26:56

**But all this was [ginomai](../../strongs/g/g1096.md), that the [graphē](../../strongs/g/g1124.md) of the [prophētēs](../../strongs/g/g4396.md) might be [plēroō](../../strongs/g/g4137.md).** Then all the [mathētēs](../../strongs/g/g3101.md) [aphiēmi](../../strongs/g/g863.md) him, and [pheugō](../../strongs/g/g5343.md).

<a name="matthew_26_57"></a>Matthew 26:57

And [krateō](../../strongs/g/g2902.md) [Iēsous](../../strongs/g/g2424.md) [apagō](../../strongs/g/g520.md) to [Kaïaphas](../../strongs/g/g2533.md) the [archiereus](../../strongs/g/g749.md), where the [grammateus](../../strongs/g/g1122.md) and the [presbyteros](../../strongs/g/g4245.md) were [synagō](../../strongs/g/g4863.md).

<a name="matthew_26_58"></a>Matthew 26:58

But [Petros](../../strongs/g/g4074.md) [akoloutheō](../../strongs/g/g190.md) him [apo](../../strongs/g/g575.md) [makrothen](../../strongs/g/g3113.md) unto the [archiereus](../../strongs/g/g749.md) [aulē](../../strongs/g/g833.md), and [eiserchomai](../../strongs/g/g1525.md) in, and [kathēmai](../../strongs/g/g2521.md) with the [hypēretēs](../../strongs/g/g5257.md), to [eidō](../../strongs/g/g1492.md) the [telos](../../strongs/g/g5056.md).

<a name="matthew_26_59"></a>Matthew 26:59

Now the [archiereus](../../strongs/g/g749.md), and [presbyteros](../../strongs/g/g4245.md), and all the [synedrion](../../strongs/g/g4892.md), [zēteō](../../strongs/g/g2212.md) [pseudomartyria](../../strongs/g/g5577.md) against [Iēsous](../../strongs/g/g2424.md), to [thanatoō](../../strongs/g/g2289.md) him;

<a name="matthew_26_60"></a>Matthew 26:60

But [heuriskō](../../strongs/g/g2147.md) none: [kai](../../strongs/g/g2532.md), though [polys](../../strongs/g/g4183.md) [pseudomartys](../../strongs/g/g5575.md) [proserchomai](../../strongs/g/g4334.md), yet [heuriskō](../../strongs/g/g2147.md) they none. At the [hysteron](../../strongs/g/g5305.md) [proserchomai](../../strongs/g/g4334.md) two [pseudomartys](../../strongs/g/g5575.md),

<a name="matthew_26_61"></a>Matthew 26:61

And [eipon](../../strongs/g/g2036.md), This [phēmi](../../strongs/g/g5346.md), I am able to [katalyō](../../strongs/g/g2647.md) the [naos](../../strongs/g/g3485.md) of [theos](../../strongs/g/g2316.md), and to [oikodomeō](../../strongs/g/g3618.md) it in three [hēmera](../../strongs/g/g2250.md).

<a name="matthew_26_62"></a>Matthew 26:62

And the [archiereus](../../strongs/g/g749.md) [anistēmi](../../strongs/g/g450.md), and [eipon](../../strongs/g/g2036.md) unto him, [apokrinomai](../../strongs/g/g611.md) thou nothing? what is it which these [katamartyreō](../../strongs/g/g2649.md) thee?

<a name="matthew_26_63"></a>Matthew 26:63

But [Iēsous](../../strongs/g/g2424.md) [siōpaō](../../strongs/g/g4623.md), And the [archiereus](../../strongs/g/g749.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, I [exorkizō](../../strongs/g/g1844.md) thee by the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md), that thou [eipon](../../strongs/g/g2036.md) us whether thou be the [Christos](../../strongs/g/g5547.md), the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md).

<a name="matthew_26_64"></a>Matthew 26:64

[Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, Thou hast [eipon](../../strongs/g/g2036.md): **nevertheless I [legō](../../strongs/g/g3004.md) unto you, Hereafter shall ye [optanomai](../../strongs/g/g3700.md) the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [kathēmai](../../strongs/g/g2521.md) on the [dexios](../../strongs/g/g1188.md) of [dynamis](../../strongs/g/g1411.md), and [erchomai](../../strongs/g/g2064.md) in the [nephelē](../../strongs/g/g3507.md) of [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_26_65"></a>Matthew 26:65

Then the [archiereus](../../strongs/g/g749.md) [diarrēssō](../../strongs/g/g1284.md) his [himation](../../strongs/g/g2440.md), [legō](../../strongs/g/g3004.md), He hath [blasphēmeō](../../strongs/g/g987.md); what further [chreia](../../strongs/g/g5532.md) have we of [martys](../../strongs/g/g3144.md)? [ide](../../strongs/g/g2396.md), now ye have [akouō](../../strongs/g/g191.md) his [blasphēmia](../../strongs/g/g988.md).

<a name="matthew_26_66"></a>Matthew 26:66

What [dokeō](../../strongs/g/g1380.md) ye? They [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), He is [enochos](../../strongs/g/g1777.md) of [thanatos](../../strongs/g/g2288.md).

<a name="matthew_26_67"></a>Matthew 26:67

Then did they [emptyō](../../strongs/g/g1716.md) in his [prosōpon](../../strongs/g/g4383.md), and [kolaphizō](../../strongs/g/g2852.md) him; and [rhapizō](../../strongs/g/g4474.md),

<a name="matthew_26_68"></a>Matthew 26:68

[legō](../../strongs/g/g3004.md), [prophēteuō](../../strongs/g/g4395.md) unto us, thou [Christos](../../strongs/g/g5547.md), Who is he that [paiō](../../strongs/g/g3817.md) thee?

<a name="matthew_26_69"></a>Matthew 26:69

Now [Petros](../../strongs/g/g4074.md) [kathēmai](../../strongs/g/g2521.md) without in the [aulē](../../strongs/g/g833.md): and a [paidiskē](../../strongs/g/g3814.md) [proserchomai](../../strongs/g/g4334.md) unto him, [legō](../../strongs/g/g3004.md), Thou also wast with [Iēsous](../../strongs/g/g2424.md) of [Galilaios](../../strongs/g/g1057.md).

<a name="matthew_26_70"></a>Matthew 26:70

But he [arneomai](../../strongs/g/g720.md) [emprosthen](../../strongs/g/g1715.md) all, [legō](../../strongs/g/g3004.md), I [eidō](../../strongs/g/g1492.md) not what thou [legō](../../strongs/g/g3004.md).

<a name="matthew_26_71"></a>Matthew 26:71

And when he was [exerchomai](../../strongs/g/g1831.md) into the [pylōn](../../strongs/g/g4440.md), another [eidō](../../strongs/g/g1492.md) him, and [legō](../../strongs/g/g3004.md) unto them that were there, This was also with [Iēsous](../../strongs/g/g2424.md) of [Nazōraios](../../strongs/g/g3480.md).

<a name="matthew_26_72"></a>Matthew 26:72

And again he [arneomai](../../strongs/g/g720.md) with [horkos](../../strongs/g/g3727.md), I do not [eidō](../../strongs/g/g1492.md) the [anthrōpos](../../strongs/g/g444.md).

<a name="matthew_26_73"></a>Matthew 26:73

And after a [mikron](../../strongs/g/g3397.md) [proserchomai](../../strongs/g/g4334.md) unto they that [histēmi](../../strongs/g/g2476.md) by, and [eipon](../../strongs/g/g2036.md) to [Petros](../../strongs/g/g4074.md), [alēthōs](../../strongs/g/g230.md) thou also art of them; for thy [lalia](../../strongs/g/g2981.md) [dēlos](../../strongs/g/g1212.md) [poieō](../../strongs/g/g4160.md) thee.

<a name="matthew_26_74"></a>Matthew 26:74

Then [archomai](../../strongs/g/g756.md) he to [katathematizō](../../strongs/g/g2653.md) and [omnyō](../../strongs/g/g3660.md), I [eidō](../../strongs/g/g1492.md) not the [anthrōpos](../../strongs/g/g444.md). And [eutheōs](../../strongs/g/g2112.md) the [alektōr](../../strongs/g/g220.md) [phōneō](../../strongs/g/g5455.md).

<a name="matthew_26_75"></a>Matthew 26:75

And [Petros](../../strongs/g/g4074.md) [mnaomai](../../strongs/g/g3415.md) the [rhēma](../../strongs/g/g4487.md) of [Iēsous](../../strongs/g/g2424.md), which [eipon](../../strongs/g/g2046.md) unto him, **Before the [alektōr](../../strongs/g/g220.md) [phōneō](../../strongs/g/g5455.md), thou shalt [aparneomai](../../strongs/g/g533.md) me thrice.** And he [exō](../../strongs/g/g1854.md) [exerchomai](../../strongs/g/g1831.md) [klaiō](../../strongs/g/g2799.md) [pikrōs](../../strongs/g/g4090.md).

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 25](matthew_25.md) - [Matthew 27](matthew_27.md)

---

[^1]: [Matthew 26:28 Commentary](../../commentary/matthew/matthew_26_commentary.md#matthew_26_28)
