# [Matthew 22](https://www.blueletterbible.org/kjv/mat/22/1/rl1/s_951001)

<a name="matthew_22_1"></a>Matthew 22:1

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them again by [parabolē](../../strongs/g/g3850.md), and [legō](../../strongs/g/g3004.md),

<a name="matthew_22_2"></a>Matthew 22:2

**The [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) is [homoioō](../../strongs/g/g3666.md) unto an [anthrōpos](../../strongs/g/g444.md) [basileus](../../strongs/g/g935.md), which [poieō](../../strongs/g/g4160.md) a [gamos](../../strongs/g/g1062.md) for his [huios](../../strongs/g/g5207.md),**

<a name="matthew_22_3"></a>Matthew 22:3

**And [apostellō](../../strongs/g/g649.md) his [doulos](../../strongs/g/g1401.md) to [kaleō](../../strongs/g/g2564.md) them that were [kaleō](../../strongs/g/g2564.md) to the [gamos](../../strongs/g/g1062.md): and they would not [erchomai](../../strongs/g/g2064.md).**

<a name="matthew_22_4"></a>Matthew 22:4

**Again, he [apostellō](../../strongs/g/g649.md) other [doulos](../../strongs/g/g1401.md), [legō](../../strongs/g/g3004.md), [eipon](../../strongs/g/g2036.md) them which are [kaleō](../../strongs/g/g2564.md), [idou](../../strongs/g/g2400.md), I have [hetoimazō](../../strongs/g/g2090.md) my [ariston](../../strongs/g/g712.md): my [tauros](../../strongs/g/g5022.md) and [sitistos](../../strongs/g/g4619.md) are [thyō](../../strongs/g/g2380.md), and all things are [hetoimos](../../strongs/g/g2092.md): [deute](../../strongs/g/g1205.md) unto the [gamos](../../strongs/g/g1062.md).**

<a name="matthew_22_5"></a>Matthew 22:5

**But they [ameleō](../../strongs/g/g272.md), and [aperchomai](../../strongs/g/g565.md), one to his [agros](../../strongs/g/g68.md), another to his [emporia](../../strongs/g/g1711.md):**

<a name="matthew_22_6"></a>Matthew 22:6

**And the [loipos](../../strongs/g/g3062.md) [krateō](../../strongs/g/g2902.md) his [doulos](../../strongs/g/g1401.md), and [hybrizō](../../strongs/g/g5195.md), and [apokteinō](../../strongs/g/g615.md) them.**

<a name="matthew_22_7"></a>Matthew 22:7

**But when the [basileus](../../strongs/g/g935.md) [akouō](../../strongs/g/g191.md), he was [orgizō](../../strongs/g/g3710.md): and he [pempō](../../strongs/g/g3992.md) his [strateuma](../../strongs/g/g4753.md), and [apollymi](../../strongs/g/g622.md) those [phoneus](../../strongs/g/g5406.md), and [empi(m)prēmi](../../strongs/g/g1714.md) their [polis](../../strongs/g/g4172.md).**

<a name="matthew_22_8"></a>Matthew 22:8

**Then [legō](../../strongs/g/g3004.md) he to his [doulos](../../strongs/g/g1401.md), The [gamos](../../strongs/g/g1062.md) is [hetoimos](../../strongs/g/g2092.md), but [kaleō](../../strongs/g/g2564.md) were not [axios](../../strongs/g/g514.md).**

<a name="matthew_22_9"></a>Matthew 22:9

**[poreuō](../../strongs/g/g4198.md) ye therefore into the [diexodos](../../strongs/g/g1327.md) [hodos](../../strongs/g/g3598.md), and as many as ye shall [heuriskō](../../strongs/g/g2147.md), [kaleō](../../strongs/g/g2564.md) to the [gamos](../../strongs/g/g1062.md).**

<a name="matthew_22_10"></a>Matthew 22:10

**So those [doulos](../../strongs/g/g1401.md) [exerchomai](../../strongs/g/g1831.md) into the [hodos](../../strongs/g/g3598.md), and [synagō](../../strongs/g/g4863.md) all as many as they [heuriskō](../../strongs/g/g2147.md), both [ponēros](../../strongs/g/g4190.md) and [agathos](../../strongs/g/g18.md): and the [gamos](../../strongs/g/g1062.md) was [pimplēmi](../../strongs/g/g4130.md) with [anakeimai](../../strongs/g/g345.md).** [^1]

<a name="matthew_22_11"></a>Matthew 22:11

**And when the [basileus](../../strongs/g/g935.md) [eiserchomai](../../strongs/g/g1525.md) to [theaomai](../../strongs/g/g2300.md) the [anakeimai](../../strongs/g/g345.md), he [eidō](../../strongs/g/g1492.md) there an [anthrōpos](../../strongs/g/g444.md) which [endyō](../../strongs/g/g1746.md) not a [gamos](../../strongs/g/g1062.md) [endyma](../../strongs/g/g1742.md):**

<a name="matthew_22_12"></a>Matthew 22:12

**And he [legō](../../strongs/g/g3004.md) unto him, [hetairos](../../strongs/g/g2083.md), how [eiserchomai](../../strongs/g/g1525.md) thou in hither not having a [gamos](../../strongs/g/g1062.md) [endyma](../../strongs/g/g1742.md)? And he was [phimoō](../../strongs/g/g5392.md).**

<a name="matthew_22_13"></a>Matthew 22:13

**Then [eipon](../../strongs/g/g2036.md) the [basileus](../../strongs/g/g935.md) to the [diakonos](../../strongs/g/g1249.md), [deō](../../strongs/g/g1210.md) him [cheir](../../strongs/g/g5495.md) and [pous](../../strongs/g/g4228.md), and [airō](../../strongs/g/g142.md) him, and [ekballō](../../strongs/g/g1544.md) into [exōteros](../../strongs/g/g1857.md) [skotos](../../strongs/g/g4655.md), there shall be [klauthmos](../../strongs/g/g2805.md) and [brygmos](../../strongs/g/g1030.md) of [odous](../../strongs/g/g3599.md).**

<a name="matthew_22_14"></a>Matthew 22:14

**For [polys](../../strongs/g/g4183.md) are [klētos](../../strongs/g/g2822.md), but [oligos](../../strongs/g/g3641.md) [eklektos](../../strongs/g/g1588.md).**

<a name="matthew_22_15"></a>Matthew 22:15

Then [poreuō](../../strongs/g/g4198.md) the [Pharisaios](../../strongs/g/g5330.md), and [lambanō](../../strongs/g/g2983.md) [symboulion](../../strongs/g/g4824.md) how they might [pagideuō](../../strongs/g/g3802.md) him in [logos](../../strongs/g/g3056.md).

<a name="matthew_22_16"></a>Matthew 22:16

And they [apostellō](../../strongs/g/g649.md) unto him their [mathētēs](../../strongs/g/g3101.md) with the [Hērōdianoi](../../strongs/g/g2265.md), [legō](../../strongs/g/g3004.md), [didaskalos](../../strongs/g/g1320.md), we [eidō](../../strongs/g/g1492.md) that thou art [alēthēs](../../strongs/g/g227.md), and [didaskō](../../strongs/g/g1321.md) [hodos](../../strongs/g/g3598.md) of [theos](../../strongs/g/g2316.md) in [alētheia](../../strongs/g/g225.md), neither [melei](../../strongs/g/g3199.md) thou for any: for thou [blepō](../../strongs/g/g991.md) not the [prosōpon](../../strongs/g/g4383.md) of [anthrōpos](../../strongs/g/g444.md).

<a name="matthew_22_17"></a>Matthew 22:17

[eipon](../../strongs/g/g2036.md) us therefore, What [dokeō](../../strongs/g/g1380.md) thou? Is it [exesti](../../strongs/g/g1832.md) to [didōmi](../../strongs/g/g1325.md) [kēnsos](../../strongs/g/g2778.md) unto [Kaisar](../../strongs/g/g2541.md), or not?

<a name="matthew_22_18"></a>Matthew 22:18

But [Iēsous](../../strongs/g/g2424.md) [ginōskō](../../strongs/g/g1097.md) their [ponēria](../../strongs/g/g4189.md), and [eipon](../../strongs/g/g2036.md), **Why [peirazō](../../strongs/g/g3985.md) me, ye [hypokritēs](../../strongs/g/g5273.md)?**

<a name="matthew_22_19"></a>Matthew 22:19

**[epideiknymi](../../strongs/g/g1925.md) me the [kēnsos](../../strongs/g/g2778.md) [nomisma](../../strongs/g/g3546.md).** And they [prospherō](../../strongs/g/g4374.md) unto him a [dēnarion](../../strongs/g/g1220.md).

<a name="matthew_22_20"></a>Matthew 22:20

And he [legō](../../strongs/g/g3004.md) unto them, **Whose this [eikōn](../../strongs/g/g1504.md) and [epigraphē](../../strongs/g/g1923.md)?**

<a name="matthew_22_21"></a>Matthew 22:21

They [legō](../../strongs/g/g3004.md) unto him, [Kaisar](../../strongs/g/g2541.md). Then [legō](../../strongs/g/g3004.md) he unto them, **[apodidōmi](../../strongs/g/g591.md) therefore unto [Kaisar](../../strongs/g/g2541.md) the things which are [Kaisar](../../strongs/g/g2541.md); and unto [theos](../../strongs/g/g2316.md) the things that are [theos](../../strongs/g/g2316.md).**

<a name="matthew_22_22"></a>Matthew 22:22

When they had [akouō](../../strongs/g/g191.md), they [thaumazō](../../strongs/g/g2296.md), and [aphiēmi](../../strongs/g/g863.md) him, and [aperchomai](../../strongs/g/g565.md).

<a name="matthew_22_23"></a>Matthew 22:23

The same [hēmera](../../strongs/g/g2250.md) [proserchomai](../../strongs/g/g4334.md) to him the [Saddoukaios](../../strongs/g/g4523.md), which [legō](../../strongs/g/g3004.md) that there is no [anastasis](../../strongs/g/g386.md), and [eperōtaō](../../strongs/g/g1905.md) him,

<a name="matthew_22_24"></a>Matthew 22:24

[legō](../../strongs/g/g3004.md), [didaskalos](../../strongs/g/g1320.md), [Mōÿsēs](../../strongs/g/g3475.md) [eipon](../../strongs/g/g2036.md), If a man [apothnēskō](../../strongs/g/g599.md), having no [teknon](../../strongs/g/g5043.md), his [adelphos](../../strongs/g/g80.md) shall [epigambreuō](../../strongs/g/g1918.md) his [gynē](../../strongs/g/g1135.md), and [anistēmi](../../strongs/g/g450.md) [sperma](../../strongs/g/g4690.md) unto his [adelphos](../../strongs/g/g80.md).

<a name="matthew_22_25"></a>Matthew 22:25

Now there were with us seven [adelphos](../../strongs/g/g80.md): and the first, when he had [gameō](../../strongs/g/g1060.md), [teleutaō](../../strongs/g/g5053.md), and, having no [sperma](../../strongs/g/g4690.md), [aphiēmi](../../strongs/g/g863.md) his [gynē](../../strongs/g/g1135.md) unto his [adelphos](../../strongs/g/g80.md):

<a name="matthew_22_26"></a>Matthew 22:26

[homoiōs](../../strongs/g/g3668.md) the [deuteros](../../strongs/g/g1208.md) also, and the [tritos](../../strongs/g/g5154.md), unto the [hepta](../../strongs/g/g2033.md).

<a name="matthew_22_27"></a>Matthew 22:27

And [hysteron](../../strongs/g/g5305.md) of all the [gynē](../../strongs/g/g1135.md) [apothnēskō](../../strongs/g/g599.md) also.

<a name="matthew_22_28"></a>Matthew 22:28

Therefore in the [anastasis](../../strongs/g/g386.md) whose [gynē](../../strongs/g/g1135.md) shall she be of the seven?  for they all had her.

<a name="matthew_22_29"></a>Matthew 22:29

[Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **Ye do [planaō](../../strongs/g/g4105.md), not [eidō](../../strongs/g/g1492.md) the [graphē](../../strongs/g/g1124.md), nor the [dynamis](../../strongs/g/g1411.md) of [theos](../../strongs/g/g2316.md).**

<a name="matthew_22_30"></a>Matthew 22:30

**For in the [anastasis](../../strongs/g/g386.md) they neither [gameō](../../strongs/g/g1060.md), nor [ekgamizō](../../strongs/g/g1547.md), but are as the [aggelos](../../strongs/g/g32.md) of [theos](../../strongs/g/g2316.md) in [ouranos](../../strongs/g/g3772.md).**

<a name="matthew_22_31"></a>Matthew 22:31

**But as [peri](../../strongs/g/g4012.md) the [anastasis](../../strongs/g/g386.md) of the [nekros](../../strongs/g/g3498.md), have ye not [anaginōskō](../../strongs/g/g314.md) that which was [rheō](../../strongs/g/g4483.md) unto you by [theos](../../strongs/g/g2316.md), [legō](../../strongs/g/g3004.md),**

<a name="matthew_22_32"></a>Matthew 22:32

**I am the [theos](../../strongs/g/g2316.md) of [Abraam](../../strongs/g/g11.md), and the [theos](../../strongs/g/g2316.md) of [Isaak](../../strongs/g/g2464.md), and the [theos](../../strongs/g/g2316.md) of [Iakōb](../../strongs/g/g2384.md)?  [theos](../../strongs/g/g2316.md) is not the [theos](../../strongs/g/g2316.md) of the [nekros](../../strongs/g/g3498.md), but of the [zaō](../../strongs/g/g2198.md).**

<a name="matthew_22_33"></a>Matthew 22:33

And when the [ochlos](../../strongs/g/g3793.md) [akouō](../../strongs/g/g191.md), they were [ekplēssō](../../strongs/g/g1605.md) at his [didachē](../../strongs/g/g1322.md).

<a name="matthew_22_34"></a>Matthew 22:34

But when the [Pharisaios](../../strongs/g/g5330.md) had [akouō](../../strongs/g/g191.md) that he had [phimoō](../../strongs/g/g5392.md) the [Saddoukaios](../../strongs/g/g4523.md), they were [synagō](../../strongs/g/g4863.md).

<a name="matthew_22_35"></a>Matthew 22:35

Then one of them, a [nomikos](../../strongs/g/g3544.md), [eperōtaō](../../strongs/g/g1905.md), [peirazō](../../strongs/g/g3985.md) him, and [legō](../../strongs/g/g3004.md),

<a name="matthew_22_36"></a>Matthew 22:36

[didaskalos](../../strongs/g/g1320.md), which is the [megas](../../strongs/g/g3173.md) [entolē](../../strongs/g/g1785.md) in the [nomos](../../strongs/g/g3551.md)?

<a name="matthew_22_37"></a>Matthew 22:37

[Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **Thou shalt [agapaō](../../strongs/g/g25.md) the [kyrios](../../strongs/g/g2962.md) thy [theos](../../strongs/g/g2316.md) with [holos](../g/g3650.md) thy [kardia](../../strongs/g/g2588.md), and with [holos](../g/g3650.md) thy [psychē](../../strongs/g/g5590.md), and with [holos](../g/g3650.md) thy [dianoia](../../strongs/g/g1271.md).**

<a name="matthew_22_38"></a>Matthew 22:38

**This is the [prōtos](../../strongs/g/g4413.md) and [megas](../../strongs/g/g3173.md) [entolē](../../strongs/g/g1785.md).**

<a name="matthew_22_39"></a>Matthew 22:39

**And the [deuteros](../../strongs/g/g1208.md) [homoios](../../strongs/g/g3664.md) unto it, Thou shalt [agapaō](../../strongs/g/g25.md) thy [plēsion](../../strongs/g/g4139.md) as [seautou](../../strongs/g/g4572.md).**

<a name="matthew_22_40"></a>Matthew 22:40

**On these two [entolē](../../strongs/g/g1785.md) [kremannymi](../../strongs/g/g2910.md) all the [nomos](../../strongs/g/g3551.md) and the [prophētēs](../../strongs/g/g4396.md).**

<a name="matthew_22_41"></a>Matthew 22:41

While the [Pharisaios](../../strongs/g/g5330.md) were [synagō](../../strongs/g/g4863.md), [Iēsous](../../strongs/g/g2424.md) [eperōtaō](../../strongs/g/g1905.md) them,

<a name="matthew_22_42"></a>Matthew 22:42

[legō](../../strongs/g/g3004.md), **What [dokeō](../../strongs/g/g1380.md) ye of [Christos](../../strongs/g/g5547.md)? whose [huios](../../strongs/g/g5207.md) is he?** They [legō](../../strongs/g/g3004.md) unto him, of [Dabid](../../strongs/g/g1138.md).

<a name="matthew_22_43"></a>Matthew 22:43

He [legō](../../strongs/g/g3004.md) unto them, **How then doth [Dabid](../../strongs/g/g1138.md) in [pneuma](../../strongs/g/g4151.md) [kaleō](../../strongs/g/g2564.md) him [kyrios](../../strongs/g/g2962.md), [legō](../../strongs/g/g3004.md),**

<a name="matthew_22_44"></a>Matthew 22:44

**The [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md) unto my [kyrios](../../strongs/g/g2962.md), [kathēmai](../../strongs/g/g2521.md) on my [dexios](../../strongs/g/g1188.md), till I [tithēmi](../../strongs/g/g5087.md) thine [echthros](../../strongs/g/g2190.md) thy [hypopodion](../../strongs/g/g5286.md) [pous](../../strongs/g/g4228.md)?**

<a name="matthew_22_45"></a>Matthew 22:45

**If [Dabid](../../strongs/g/g1138.md) then [kaleō](../../strongs/g/g2564.md) him [kyrios](../../strongs/g/g2962.md), how is he his [huios](../../strongs/g/g5207.md)?**

<a name="matthew_22_46"></a>Matthew 22:46

And [oudeis](../../strongs/g/g3762.md) was able to [apokrinomai](../../strongs/g/g611.md) him a [logos](../../strongs/g/g3056.md), neither [tolmaō](../../strongs/g/g5111.md) any from that [hēmera](../../strongs/g/g2250.md) forth [eperōtaō](../../strongs/g/g1905.md) him any more.

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 21](matthew_21.md) - [Matthew 23](matthew_23.md)

---

[^1]: [Matthew 22:10 Commentary](../../commentary/matthew/matthew_22_commentary.md#matthew_22_10)
