# [Matthew 3](https://www.blueletterbible.org/kjv/mat/3/1/rl1/s_930001)

<a name="matthew_3_1"></a>Matthew 3:1

In those [hēmera](../../strongs/g/g2250.md) [paraginomai](../../strongs/g/g3854.md) [Iōannēs](../../strongs/g/g2491.md) the [baptistēs](../../strongs/g/g910.md), [kēryssō](../../strongs/g/g2784.md) in the [erēmos](../../strongs/g/g2048.md) of [Ioudaia](../../strongs/g/g2449.md),

<a name="matthew_3_2"></a>Matthew 3:2

And [legō](../../strongs/g/g3004.md), [metanoeō](../../strongs/g/g3340.md) ye: for the [basileia](../../strongs/g/g932.md) of [ouranos](../../strongs/g/g3772.md) [eggizō](../../strongs/g/g1448.md).

<a name="matthew_3_3"></a>Matthew 3:3

For this is he that was [rheō](../../strongs/g/g4483.md) of by the [prophētēs](../../strongs/g/g4396.md) [Ēsaïas](../../strongs/g/g2268.md), [legō](../../strongs/g/g3004.md), The [phōnē](../../strongs/g/g5456.md) [boaō](../../strongs/g/g994.md) in the [erēmos](../../strongs/g/g2048.md), [hetoimazō](../../strongs/g/g2090.md) ye [hodos](../../strongs/g/g3598.md) of the [kyrios](../../strongs/g/g2962.md), [poieō](../../strongs/g/g4160.md) his [tribos](../../strongs/g/g5147.md) [euthys](../../strongs/g/g2117.md). [^1]

<a name="matthew_3_4"></a>Matthew 3:4

And the same [Iōannēs](../../strongs/g/g2491.md) had his [endyma](../../strongs/g/g1742.md) of [kamēlos](../../strongs/g/g2574.md) [thrix](../../strongs/g/g2359.md), and a [dermatinos](../../strongs/g/g1193.md) [zōnē](../../strongs/g/g2223.md) about his [osphys](../../strongs/g/g3751.md); and his [trophē](../../strongs/g/g5160.md) was [akris](../../strongs/g/g200.md) and [agrios](../../strongs/g/g66.md) [meli](../../strongs/g/g3192.md).

<a name="matthew_3_5"></a>Matthew 3:5

Then [ekporeuomai](../../strongs/g/g1607.md) to him [Hierosolyma](../../strongs/g/g2414.md), and all [Ioudaia](../../strongs/g/g2449.md), and all [perichōros](../../strongs/g/g4066.md) [Iordanēs](../../strongs/g/g2446.md), [^2]

<a name="matthew_3_6"></a>Matthew 3:6

And were [baptizō](../../strongs/g/g907.md) of him in [Iordanēs](../../strongs/g/g2446.md), [exomologeō](../../strongs/g/g1843.md) their [hamartia](../../strongs/g/g266.md). [^3]

<a name="matthew_3_7"></a>Matthew 3:7

But when he [eidō](../../strongs/g/g1492.md) [polys](../../strongs/g/g4183.md) of the [Pharisaios](../../strongs/g/g5330.md) and [Saddoukaios](../../strongs/g/g4523.md) [erchomai](../../strongs/g/g2064.md) to his [baptisma](../../strongs/g/g908.md), he [eipon](../../strongs/g/g2036.md) unto them, [gennēma](../../strongs/g/g1081.md) of [echidna](../../strongs/g/g2191.md), who hath [hypodeiknymi](../../strongs/g/g5263.md) you to [pheugō](../../strongs/g/g5343.md) from the [orgē](../../strongs/g/g3709.md) to [mellō](../../strongs/g/g3195.md)?

<a name="matthew_3_8"></a>Matthew 3:8

[poieō](../../strongs/g/g4160.md) therefore [karpos](../../strongs/g/g2590.md) [axios](../../strongs/g/g514.md) for [metanoia](../../strongs/g/g3341.md):

<a name="matthew_3_9"></a>Matthew 3:9

And [dokeō](../../strongs/g/g1380.md) not to [legō](../../strongs/g/g3004.md) within yourselves, We have [Abraam](../../strongs/g/g11.md) [patēr](../../strongs/g/g3962.md): for I [legō](../../strongs/g/g3004.md) unto you, that [theos](../../strongs/g/g2316.md) is able of these [lithos](../../strongs/g/g3037.md) to [egeirō](../../strongs/g/g1453.md) [teknon](../../strongs/g/g5043.md) unto [Abraam](../../strongs/g/g11.md). [^4]

<a name="matthew_3_10"></a>Matthew 3:10

And now also the [axinē](../../strongs/g/g513.md) [keimai](../../strongs/g/g2749.md) unto the [rhiza](../../strongs/g/g4491.md) of the [dendron](../../strongs/g/g1186.md): therefore every [dendron](../../strongs/g/g1186.md) which [poieō](../../strongs/g/g4160.md) not [kalos](../../strongs/g/g2570.md) [karpos](../../strongs/g/g2590.md) [ekkoptō](../../strongs/g/g1581.md), and [ballō](../../strongs/g/g906.md) into the [pyr](../../strongs/g/g4442.md).

<a name="matthew_3_11"></a>Matthew 3:11

I [men](../../strongs/g/g3303.md) [baptizō](../../strongs/g/g907.md) you with [hydōr](../../strongs/g/g5204.md) unto [metanoia](../../strongs/g/g3341.md). but he that [erchomai](../../strongs/g/g2064.md) after me is [ischyros](../../strongs/g/g2478.md) I, whose [hypodēma](../../strongs/g/g5266.md) I am not [hikanos](../../strongs/g/g2425.md) to [bastazō](../../strongs/g/g941.md): he shall [baptizō](../../strongs/g/g907.md) you with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), and [pyr](../../strongs/g/g4442.md): [^5]

<a name="matthew_3_12"></a>Matthew 3:12

Whose [ptyon](../../strongs/g/g4425.md) in his [cheir](../../strongs/g/g5495.md), and [diakatharizō](../../strongs/g/g1245.md) his [halōn](../../strongs/g/g257.md), and [synagō](../../strongs/g/g4863.md) his [sitos](../../strongs/g/g4621.md) into the [apothēkē](../../strongs/g/g596.md); but he will [katakaiō](../../strongs/g/g2618.md) the [achyron](../../strongs/g/g892.md) with [asbestos](../../strongs/g/g762.md) [pyr](../../strongs/g/g4442.md).

<a name="matthew_3_13"></a>Matthew 3:13

Then [paraginomai](../../strongs/g/g3854.md) [Iēsous](../../strongs/g/g2424.md) from [Galilaia](../../strongs/g/g1056.md) to [Iordanēs](../../strongs/g/g2446.md) unto [Iōannēs](../../strongs/g/g2491.md), to be [baptizō](../../strongs/g/g907.md) of him.

<a name="matthew_3_14"></a>Matthew 3:14

But [Iōannēs](../../strongs/g/g2491.md) [diakōlyō](../../strongs/g/g1254.md) him, [legō](../../strongs/g/g3004.md), I have [chreia](../../strongs/g/g5532.md) to be [baptizō](../../strongs/g/g907.md) of thee, and [erchomai](../../strongs/g/g2064.md) thou to me? [^6]

<a name="matthew_3_15"></a>Matthew 3:15

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto him, **[aphiēmi](../../strongs/g/g863.md) now: for thus it becometh us to [plēroō](../../strongs/g/g4137.md) all [dikaiosynē](../../strongs/g/g1343.md). Then he [aphiēmi](../../strongs/g/g863.md) him.** [^7]

<a name="matthew_3_16"></a>Matthew 3:16

And [Iēsous](../../strongs/g/g2424.md), when he was [baptizō](../../strongs/g/g907.md), [anabainō](../../strongs/g/g305.md) [euthys](../../strongs/g/g2117.md) out of the [hydōr](../../strongs/g/g5204.md): and, [idou](../../strongs/g/g2400.md), the [ouranos](../../strongs/g/g3772.md) were [anoigō](../../strongs/g/g455.md) unto him, and he [eidō](../../strongs/g/g1492.md) the [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md) [katabainō](../../strongs/g/g2597.md) like a [peristera](../../strongs/g/g4058.md), and [erchomai](../../strongs/g/g2064.md) upon him:

<a name="matthew_3_17"></a>Matthew 3:17

And [idou](../../strongs/g/g2400.md) a [phōnē](../../strongs/g/g5456.md) from [ouranos](../../strongs/g/g3772.md), [legō](../../strongs/g/g3004.md), This is my [agapētos](../../strongs/g/g27.md) [huios](../../strongs/g/g5207.md), in whom [eudokeō](../../strongs/g/g2106.md). [^8]

---

[Transliteral Bible](../bible.md)

[Matthew](matthew.md)

[Matthew 2](matthew_2.md) - [Matthew 4](matthew_4.md)

---

[^1]: [Matthew 3:3 Commentary](../../commentary/matthew/matthew_3_commentary.md#matthew_3_3)

[^2]: [Matthew 3:5 Commentary](../../commentary/matthew/matthew_3_commentary.md#matthew_3_5)

[^3]: [Matthew 3:6 Commentary](../../commentary/matthew/matthew_3_commentary.md#matthew_3_6)

[^4]: [Matthew 3:9 Commentary](../../commentary/matthew/matthew_3_commentary.md#matthew_3_9)

[^5]: [Matthew 3:11 Commentary](../../commentary/matthew/matthew_3_commentary.md#matthew_3_11)

[^6]: [Matthew 3:14 Commentary](../../commentary/matthew/matthew_3_commentary.md#matthew_3_14)

[^7]: [Matthew 3:15 Commentary](../../commentary/matthew/matthew_3_commentary.md#matthew_3_15)

[^8]: [Matthew 3:17 Commentary](../../commentary/matthew/matthew_3_commentary.md#matthew_3_17)
