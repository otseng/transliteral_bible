# [Esther 6](https://www.blueletterbible.org/kjv/esther/6)

<a name="esther_6_1"></a>Esther 6:1

On that [layil](../../strongs/h/h3915.md) could [nāḏaḏ](../../strongs/h/h5074.md) the [melek](../../strongs/h/h4428.md) [šēnā'](../../strongs/h/h8142.md), and he ['āmar](../../strongs/h/h559.md) to [bow'](../../strongs/h/h935.md) the [sēp̄er](../../strongs/h/h5612.md) of [zikārôn](../../strongs/h/h2146.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md); and they were [qara'](../../strongs/h/h7121.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

<a name="esther_6_2"></a>Esther 6:2

And it was [māṣā'](../../strongs/h/h4672.md) [kāṯaḇ](../../strongs/h/h3789.md), that [Mārdᵊḵay](../../strongs/h/h4782.md) had [nāḡaḏ](../../strongs/h/h5046.md) of [Biḡṯān](../../strongs/h/h904.md) and [Tereš](../../strongs/h/h8657.md), two of the [melek](../../strongs/h/h4428.md) [sārîs](../../strongs/h/h5631.md), the [shamar](../../strongs/h/h8104.md) of the [caph](../../strongs/h/h5592.md), who [bāqaš](../../strongs/h/h1245.md) to [shalach](../../strongs/h/h7971.md) [yad](../../strongs/h/h3027.md) on the [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md).

<a name="esther_6_3"></a>Esther 6:3

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), What [yᵊqār](../../strongs/h/h3366.md) and [gᵊḏûlâ](../../strongs/h/h1420.md) hath been ['asah](../../strongs/h/h6213.md) to [Mārdᵊḵay](../../strongs/h/h4782.md) for this? Then ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md) [naʿar](../../strongs/h/h5288.md) that [sharath](../../strongs/h/h8334.md) unto him, There is [dabar](../../strongs/h/h1697.md) ['asah](../../strongs/h/h6213.md) for him.

<a name="esther_6_4"></a>Esther 6:4

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), Who is in the [ḥāṣēr](../../strongs/h/h2691.md)? Now [Hāmān](../../strongs/h/h2001.md) was [bow'](../../strongs/h/h935.md) into the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), to ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md) to [tālâ](../../strongs/h/h8518.md) [Mārdᵊḵay](../../strongs/h/h4782.md) on the ['ets](../../strongs/h/h6086.md) that he had [kuwn](../../strongs/h/h3559.md) for him.

<a name="esther_6_5"></a>Esther 6:5

And the [melek](../../strongs/h/h4428.md) [naʿar](../../strongs/h/h5288.md) ['āmar](../../strongs/h/h559.md) unto him, Behold, [Hāmān](../../strongs/h/h2001.md) ['amad](../../strongs/h/h5975.md) in the [ḥāṣēr](../../strongs/h/h2691.md). And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), Let him [bow'](../../strongs/h/h935.md).

<a name="esther_6_6"></a>Esther 6:6

So [Hāmān](../../strongs/h/h2001.md) [bow'](../../strongs/h/h935.md). And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto him, What shall be ['asah](../../strongs/h/h6213.md) unto the ['iysh](../../strongs/h/h376.md) whom the [melek](../../strongs/h/h4428.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) to [yᵊqār](../../strongs/h/h3366.md)? Now [Hāmān](../../strongs/h/h2001.md) ['āmar](../../strongs/h/h559.md) in his [leb](../../strongs/h/h3820.md), To whom would the [melek](../../strongs/h/h4428.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) to ['asah](../../strongs/h/h6213.md) [yᵊqār](../../strongs/h/h3366.md) [yôṯēr](../../strongs/h/h3148.md) than to myself?

<a name="esther_6_7"></a>Esther 6:7

And [Hāmān](../../strongs/h/h2001.md) ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md), For the ['iysh](../../strongs/h/h376.md) whom the [melek](../../strongs/h/h4428.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) to [yᵊqār](../../strongs/h/h3366.md),

<a name="esther_6_8"></a>Esther 6:8

Let the [malkuwth](../../strongs/h/h4438.md) [lᵊḇûš](../../strongs/h/h3830.md) be [bow'](../../strongs/h/h935.md) which the [melek](../../strongs/h/h4428.md) useth to [labash](../../strongs/h/h3847.md), and the [sûs](../../strongs/h/h5483.md) that the [melek](../../strongs/h/h4428.md) [rāḵaḇ](../../strongs/h/h7392.md) upon, and the [keṯer](../../strongs/h/h3804.md) [malkuwth](../../strongs/h/h4438.md) which is [nathan](../../strongs/h/h5414.md) upon his [ro'sh](../../strongs/h/h7218.md):

<a name="esther_6_9"></a>Esther 6:9

And let this [lᵊḇûš](../../strongs/h/h3830.md) and [sûs](../../strongs/h/h5483.md) be [nathan](../../strongs/h/h5414.md) to the [yad](../../strongs/h/h3027.md) of ['iysh](../../strongs/h/h376.md) of the [melek](../../strongs/h/h4428.md) most [partᵊmîm](../../strongs/h/h6579.md) [śar](../../strongs/h/h8269.md), that they may [labash](../../strongs/h/h3847.md) the ['iysh](../../strongs/h/h376.md) withal whom the [melek](../../strongs/h/h4428.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) to [yᵊqār](../../strongs/h/h3366.md), and [rāḵaḇ](../../strongs/h/h7392.md) him on [sûs](../../strongs/h/h5483.md) through the [rᵊḥōḇ](../../strongs/h/h7339.md) of the [ʿîr](../../strongs/h/h5892.md), and [qara'](../../strongs/h/h7121.md) [paniym](../../strongs/h/h6440.md) him, Thus shall it be ['asah](../../strongs/h/h6213.md) to the ['iysh](../../strongs/h/h376.md) whom the [melek](../../strongs/h/h4428.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) to [yᵊqār](../../strongs/h/h3366.md).

<a name="esther_6_10"></a>Esther 6:10

Then the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) to [Hāmān](../../strongs/h/h2001.md), [māhar](../../strongs/h/h4116.md), and [laqach](../../strongs/h/h3947.md) the [lᵊḇûš](../../strongs/h/h3830.md) and the [sûs](../../strongs/h/h5483.md), as thou hast [dabar](../../strongs/h/h1696.md), and ['asah](../../strongs/h/h6213.md) even so to [Mārdᵊḵay](../../strongs/h/h4782.md) the [Yᵊhûḏî](../../strongs/h/h3064.md), that [yashab](../../strongs/h/h3427.md) at the [melek](../../strongs/h/h4428.md) [sha'ar](../../strongs/h/h8179.md): let [dabar](../../strongs/h/h1697.md) [naphal](../../strongs/h/h5307.md) of all that thou hast [dabar](../../strongs/h/h1696.md).

<a name="esther_6_11"></a>Esther 6:11

Then [laqach](../../strongs/h/h3947.md) [Hāmān](../../strongs/h/h2001.md) the [lᵊḇûš](../../strongs/h/h3830.md) and the [sûs](../../strongs/h/h5483.md), and [labash](../../strongs/h/h3847.md) [Mārdᵊḵay](../../strongs/h/h4782.md), and [rāḵaḇ](../../strongs/h/h7392.md) him through the [rᵊḥōḇ](../../strongs/h/h7339.md) of the [ʿîr](../../strongs/h/h5892.md), and [qara'](../../strongs/h/h7121.md) [paniym](../../strongs/h/h6440.md) him, Thus shall it be ['asah](../../strongs/h/h6213.md) unto the ['iysh](../../strongs/h/h376.md) whom the [melek](../../strongs/h/h4428.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) to [yᵊqār](../../strongs/h/h3366.md).

<a name="esther_6_12"></a>Esther 6:12

And [Mārdᵊḵay](../../strongs/h/h4782.md) [shuwb](../../strongs/h/h7725.md) to the [melek](../../strongs/h/h4428.md) [sha'ar](../../strongs/h/h8179.md). But [Hāmān](../../strongs/h/h2001.md) [dāḥap̄](../../strongs/h/h1765.md) to his [bayith](../../strongs/h/h1004.md) ['āḇēl](../../strongs/h/h57.md), and having his [ro'sh](../../strongs/h/h7218.md) [ḥāp̄â](../../strongs/h/h2645.md).

<a name="esther_6_13"></a>Esther 6:13

And [Hāmān](../../strongs/h/h2001.md) [sāp̄ar](../../strongs/h/h5608.md) [Zereš](../../strongs/h/h2238.md) his ['ishshah](../../strongs/h/h802.md) and all his ['ahab](../../strongs/h/h157.md) every thing that had [qārâ](../../strongs/h/h7136.md) him. Then ['āmar](../../strongs/h/h559.md) his [ḥāḵām](../../strongs/h/h2450.md) and [Zereš](../../strongs/h/h2238.md) his ['ishshah](../../strongs/h/h802.md) unto him, If [Mārdᵊḵay](../../strongs/h/h4782.md) be of the [zera'](../../strongs/h/h2233.md) of the [Yᵊhûḏî](../../strongs/h/h3064.md), [paniym](../../strongs/h/h6440.md) whom thou hast [ḥālal](../../strongs/h/h2490.md) to [naphal](../../strongs/h/h5307.md), thou shalt not [yakol](../../strongs/h/h3201.md) against him, but shalt [naphal](../../strongs/h/h5307.md) [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) him.

<a name="esther_6_14"></a>Esther 6:14

And while they were yet [dabar](../../strongs/h/h1696.md) with him, [naga'](../../strongs/h/h5060.md) the [melek](../../strongs/h/h4428.md) [sārîs](../../strongs/h/h5631.md), and [bahal](../../strongs/h/h926.md) to [bow'](../../strongs/h/h935.md) [Hāmān](../../strongs/h/h2001.md) unto the [mištê](../../strongs/h/h4960.md) that ['Estēr](../../strongs/h/h635.md) had ['asah](../../strongs/h/h6213.md).

---

[Transliteral Bible](../bible.md)

[Esther](esther.md)

[Esther 5](esther_5.md) - [Esther 7](esther_7.md)