# [Esther 5](https://www.blueletterbible.org/kjv/esther/5)

<a name="esther_5_1"></a>Esther 5:1

Now it came to pass on the third [yowm](../../strongs/h/h3117.md), that ['Estēr](../../strongs/h/h635.md) [labash](../../strongs/h/h3847.md) her [malkuwth](../../strongs/h/h4438.md), and ['amad](../../strongs/h/h5975.md) in the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), over against the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md): and the [melek](../../strongs/h/h4428.md) [yashab](../../strongs/h/h3427.md) upon his [malkuwth](../../strongs/h/h4438.md) [kicce'](../../strongs/h/h3678.md) in the [malkuwth](../../strongs/h/h4438.md) [bayith](../../strongs/h/h1004.md), over against the [peṯaḥ](../../strongs/h/h6607.md) of the [bayith](../../strongs/h/h1004.md).

<a name="esther_5_2"></a>Esther 5:2

And it was so, when the [melek](../../strongs/h/h4428.md) [ra'ah](../../strongs/h/h7200.md) ['Estēr](../../strongs/h/h635.md) the [malkâ](../../strongs/h/h4436.md) ['amad](../../strongs/h/h5975.md) in the [ḥāṣēr](../../strongs/h/h2691.md), that she [nasa'](../../strongs/h/h5375.md) [ḥēn](../../strongs/h/h2580.md) in his ['ayin](../../strongs/h/h5869.md): and the [melek](../../strongs/h/h4428.md) held [yāšaṭ](../../strongs/h/h3447.md) to ['Estēr](../../strongs/h/h635.md) the [zāhāḇ](../../strongs/h/h2091.md) [šarbîṭ](../../strongs/h/h8275.md) that was in his [yad](../../strongs/h/h3027.md). So ['Estēr](../../strongs/h/h635.md) [qāraḇ](../../strongs/h/h7126.md), and [naga'](../../strongs/h/h5060.md) the [ro'sh](../../strongs/h/h7218.md) of the [šarbîṭ](../../strongs/h/h8275.md).

<a name="esther_5_3"></a>Esther 5:3

Then ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md) unto her, What wilt thou, [malkâ](../../strongs/h/h4436.md) ['Estēr](../../strongs/h/h635.md)? and what is thy [baqqāšâ](../../strongs/h/h1246.md)? it shall be even [nathan](../../strongs/h/h5414.md) thee to the [ḥēṣî](../../strongs/h/h2677.md) of the [malkuwth](../../strongs/h/h4438.md).

<a name="esther_5_4"></a>Esther 5:4

And ['Estēr](../../strongs/h/h635.md) ['āmar](../../strongs/h/h559.md), If it seem [ṭôḇ](../../strongs/h/h2895.md) unto the [melek](../../strongs/h/h4428.md), let the [melek](../../strongs/h/h4428.md) and [Hāmān](../../strongs/h/h2001.md) [bow'](../../strongs/h/h935.md) this [yowm](../../strongs/h/h3117.md) unto the [mištê](../../strongs/h/h4960.md) that I have ['asah](../../strongs/h/h6213.md) for him.

<a name="esther_5_5"></a>Esther 5:5

Then the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), Cause [Hāmān](../../strongs/h/h2001.md) to make [māhar](../../strongs/h/h4116.md), that he may ['asah](../../strongs/h/h6213.md) as ['Estēr](../../strongs/h/h635.md) hath [dabar](../../strongs/h/h1697.md). So the [melek](../../strongs/h/h4428.md) and [Hāmān](../../strongs/h/h2001.md) [bow'](../../strongs/h/h935.md) to the [mištê](../../strongs/h/h4960.md) that ['Estēr](../../strongs/h/h635.md) had ['asah](../../strongs/h/h6213.md).

<a name="esther_5_6"></a>Esther 5:6

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto ['Estēr](../../strongs/h/h635.md) at the [mištê](../../strongs/h/h4960.md) of [yayin](../../strongs/h/h3196.md), What is thy [šᵊ'ēlâ](../../strongs/h/h7596.md)? and it shall be [nathan](../../strongs/h/h5414.md) thee: and what is thy [baqqāšâ](../../strongs/h/h1246.md)? even to the [ḥēṣî](../../strongs/h/h2677.md) of the [malkuwth](../../strongs/h/h4438.md) it shall be ['asah](../../strongs/h/h6213.md).

<a name="esther_5_7"></a>Esther 5:7

Then ['anah](../../strongs/h/h6030.md) ['Estēr](../../strongs/h/h635.md), and ['āmar](../../strongs/h/h559.md), My [šᵊ'ēlâ](../../strongs/h/h7596.md) and my [baqqāšâ](../../strongs/h/h1246.md) is;

<a name="esther_5_8"></a>Esther 5:8

If I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in the ['ayin](../../strongs/h/h5869.md) of the [melek](../../strongs/h/h4428.md), and if it [ṭôḇ](../../strongs/h/h2895.md) the [melek](../../strongs/h/h4428.md) to [nathan](../../strongs/h/h5414.md) my [šᵊ'ēlâ](../../strongs/h/h7596.md), and to ['asah](../../strongs/h/h6213.md) my [baqqāšâ](../../strongs/h/h1246.md), let the [melek](../../strongs/h/h4428.md) and [Hāmān](../../strongs/h/h2001.md) [bow'](../../strongs/h/h935.md) to the [mištê](../../strongs/h/h4960.md) that I shall ['asah](../../strongs/h/h6213.md) for them, and I will ['asah](../../strongs/h/h6213.md) [māḥār](../../strongs/h/h4279.md) as the [melek](../../strongs/h/h4428.md) hath [dabar](../../strongs/h/h1697.md).

<a name="esther_5_9"></a>Esther 5:9

Then [yāṣā'](../../strongs/h/h3318.md) [Hāmān](../../strongs/h/h2001.md) [yāṣā'](../../strongs/h/h3318.md) that [yowm](../../strongs/h/h3117.md) [śāmēaḥ](../../strongs/h/h8056.md) and with a [towb](../../strongs/h/h2896.md) [leb](../../strongs/h/h3820.md): but when [Hāmān](../../strongs/h/h2001.md) [ra'ah](../../strongs/h/h7200.md) [Mārdᵊḵay](../../strongs/h/h4782.md) in the [melek](../../strongs/h/h4428.md) [sha'ar](../../strongs/h/h8179.md), that he stood not [quwm](../../strongs/h/h6965.md), nor [zûaʿ](../../strongs/h/h2111.md) for him, [Hāmān](../../strongs/h/h2001.md) was [mālā'](../../strongs/h/h4390.md) of [chemah](../../strongs/h/h2534.md) against [Mārdᵊḵay](../../strongs/h/h4782.md).

<a name="esther_5_10"></a>Esther 5:10

Nevertheless [Hāmān](../../strongs/h/h2001.md) ['āp̄aq](../../strongs/h/h662.md) himself: and when he [bow'](../../strongs/h/h935.md) [bayith](../../strongs/h/h1004.md), he [shalach](../../strongs/h/h7971.md) and [bow'](../../strongs/h/h935.md) for his ['ahab](../../strongs/h/h157.md), and [Zereš](../../strongs/h/h2238.md) his ['ishshah](../../strongs/h/h802.md).

<a name="esther_5_11"></a>Esther 5:11

And [Hāmān](../../strongs/h/h2001.md) [sāp̄ar](../../strongs/h/h5608.md) them of the [kabowd](../../strongs/h/h3519.md) of his [ʿōšer](../../strongs/h/h6239.md), and the [rōḇ](../../strongs/h/h7230.md) of his [ben](../../strongs/h/h1121.md), and all the things wherein the [melek](../../strongs/h/h4428.md) had [gāḏal](../../strongs/h/h1431.md) him, and how he had [nasa'](../../strongs/h/h5375.md) him above the [śar](../../strongs/h/h8269.md) and ['ebed](../../strongs/h/h5650.md) of the [melek](../../strongs/h/h4428.md).

<a name="esther_5_12"></a>Esther 5:12

[Hāmān](../../strongs/h/h2001.md) ['āmar](../../strongs/h/h559.md) moreover, Yea, ['Estēr](../../strongs/h/h635.md) the [malkâ](../../strongs/h/h4436.md) did let no man [bow'](../../strongs/h/h935.md) with the [melek](../../strongs/h/h4428.md) unto the [mištê](../../strongs/h/h4960.md) that she had ['asah](../../strongs/h/h6213.md) but myself; and [māḥār](../../strongs/h/h4279.md) am I [qara'](../../strongs/h/h7121.md) unto her also with the [melek](../../strongs/h/h4428.md).

<a name="esther_5_13"></a>Esther 5:13

Yet all this [šāvâ](../../strongs/h/h7737.md) me nothing, so long [ʿēṯ](../../strongs/h/h6256.md) I [ra'ah](../../strongs/h/h7200.md) [Mārdᵊḵay](../../strongs/h/h4782.md) the [Yᵊhûḏî](../../strongs/h/h3064.md) [yashab](../../strongs/h/h3427.md) at the [melek](../../strongs/h/h4428.md) [sha'ar](../../strongs/h/h8179.md).

<a name="esther_5_14"></a>Esther 5:14

Then ['āmar](../../strongs/h/h559.md) [Zereš](../../strongs/h/h2238.md) his ['ishshah](../../strongs/h/h802.md) and all his ['ahab](../../strongs/h/h157.md) unto him, Let an ['ets](../../strongs/h/h6086.md) be ['asah](../../strongs/h/h6213.md) of fifty ['ammâ](../../strongs/h/h520.md) [gāḇōha](../../strongs/h/h1364.md), and to [boqer](../../strongs/h/h1242.md) ['āmar](../../strongs/h/h559.md) thou unto the [melek](../../strongs/h/h4428.md) that [Mārdᵊḵay](../../strongs/h/h4782.md) may be [tālâ](../../strongs/h/h8518.md) thereon: then [bow'](../../strongs/h/h935.md) thou in [śāmēaḥ](../../strongs/h/h8056.md) with the [melek](../../strongs/h/h4428.md) unto the [mištê](../../strongs/h/h4960.md). And the [dabar](../../strongs/h/h1697.md) [yatab](../../strongs/h/h3190.md) [paniym](../../strongs/h/h6440.md) [Hāmān](../../strongs/h/h2001.md); and he ['asah](../../strongs/h/h6213.md) the ['ets](../../strongs/h/h6086.md) to be ['asah](../../strongs/h/h6213.md).

---

[Transliteral Bible](../bible.md)

[Esther](esther.md)

[Esther 4](esther_4.md) - [Esther 6](esther_6.md)