# [Esther 10](https://www.blueletterbible.org/kjv/esther/10)

<a name="esther_10_1"></a>Esther 10:1

And the [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md) [śûm](../../strongs/h/h7760.md) a [mas](../../strongs/h/h4522.md) upon the ['erets](../../strongs/h/h776.md), and upon the ['î](../../strongs/h/h339.md) of the [yam](../../strongs/h/h3220.md).

<a name="esther_10_2"></a>Esther 10:2

And all the [ma'aseh](../../strongs/h/h4639.md) of his [tōqep̄](../../strongs/h/h8633.md) and of his [gᵊḇûrâ](../../strongs/h/h1369.md), and the [pārāšâ](../../strongs/h/h6575.md) of the [gᵊḏûlâ](../../strongs/h/h1420.md) of [Mārdᵊḵay](../../strongs/h/h4782.md), whereunto the [melek](../../strongs/h/h4428.md) [gāḏal](../../strongs/h/h1431.md) him, are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Māḏay](../../strongs/h/h4074.md) and [Pāras](../../strongs/h/h6539.md)?

<a name="esther_10_3"></a>Esther 10:3

For [Mārdᵊḵay](../../strongs/h/h4782.md) the [Yᵊhûḏî](../../strongs/h/h3064.md) was [mišnê](../../strongs/h/h4932.md) unto [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md), and [gadowl](../../strongs/h/h1419.md) among the [Yᵊhûḏî](../../strongs/h/h3064.md), and [ratsah](../../strongs/h/h7521.md) of the [rōḇ](../../strongs/h/h7230.md) of his ['ach](../../strongs/h/h251.md), [darash](../../strongs/h/h1875.md) the [towb](../../strongs/h/h2896.md) of his ['am](../../strongs/h/h5971.md), and [dabar](../../strongs/h/h1696.md) [shalowm](../../strongs/h/h7965.md) to all his [zera'](../../strongs/h/h2233.md).

---

[Transliteral Bible](../bible.md)

[Esther](esther.md)

[Esther 9](esther_9.md)