# [Esther 7](https://www.blueletterbible.org/kjv/esther/7)

<a name="esther_7_1"></a>Esther 7:1

So the [melek](../../strongs/h/h4428.md) and [Hāmān](../../strongs/h/h2001.md) [bow'](../../strongs/h/h935.md) to [šāṯâ](../../strongs/h/h8354.md) with ['Estēr](../../strongs/h/h635.md) the [malkâ](../../strongs/h/h4436.md).

<a name="esther_7_2"></a>Esther 7:2

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) again unto ['Estēr](../../strongs/h/h635.md) on the second [yowm](../../strongs/h/h3117.md) at the [mištê](../../strongs/h/h4960.md) of [yayin](../../strongs/h/h3196.md), What is thy [šᵊ'ēlâ](../../strongs/h/h7596.md), [malkâ](../../strongs/h/h4436.md) ['Estēr](../../strongs/h/h635.md)? and it shall be [nathan](../../strongs/h/h5414.md) thee: and what is thy [baqqāšâ](../../strongs/h/h1246.md)? and it shall be ['asah](../../strongs/h/h6213.md), even to the [ḥēṣî](../../strongs/h/h2677.md) of the [malkuwth](../../strongs/h/h4438.md).

<a name="esther_7_3"></a>Esther 7:3

Then ['Estēr](../../strongs/h/h635.md) the [malkâ](../../strongs/h/h4436.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), If I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md), O [melek](../../strongs/h/h4428.md), and if it [ṭôḇ](../../strongs/h/h2895.md) the [melek](../../strongs/h/h4428.md), let my [nephesh](../../strongs/h/h5315.md) be [nathan](../../strongs/h/h5414.md) me at my [šᵊ'ēlâ](../../strongs/h/h7596.md), and my ['am](../../strongs/h/h5971.md) at my [baqqāšâ](../../strongs/h/h1246.md):

<a name="esther_7_4"></a>Esther 7:4

For we are [māḵar](../../strongs/h/h4376.md), I and my ['am](../../strongs/h/h5971.md), to be [šāmaḏ](../../strongs/h/h8045.md), to be [harag](../../strongs/h/h2026.md), and to ['abad](../../strongs/h/h6.md). But ['illû](../../strongs/h/h432.md) we had been [māḵar](../../strongs/h/h4376.md) for ['ebed](../../strongs/h/h5650.md) and [šip̄ḥâ](../../strongs/h/h8198.md), I had held my [ḥāraš](../../strongs/h/h2790.md), although the [tsar](../../strongs/h/h6862.md) could not [šāvâ](../../strongs/h/h7737.md) the [melek](../../strongs/h/h4428.md) [nezeq](../../strongs/h/h5143.md).

<a name="esther_7_5"></a>Esther 7:5

Then the [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md) ['āmar](../../strongs/h/h559.md) and ['āmar](../../strongs/h/h559.md) unto ['Estēr](../../strongs/h/h635.md) the [malkâ](../../strongs/h/h4436.md), Who is he, and where is he, that durst [mālā'](../../strongs/h/h4390.md) in his [leb](../../strongs/h/h3820.md) to ['asah](../../strongs/h/h6213.md) so?

<a name="esther_7_6"></a>Esther 7:6

And ['Estēr](../../strongs/h/h635.md) ['āmar](../../strongs/h/h559.md), The ['iysh](../../strongs/h/h376.md) [tsar](../../strongs/h/h6862.md) and ['oyeb](../../strongs/h/h341.md) is this [ra'](../../strongs/h/h7451.md) [Hāmān](../../strongs/h/h2001.md). Then [Hāmān](../../strongs/h/h2001.md) was [ba'ath](../../strongs/h/h1204.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) and the [malkâ](../../strongs/h/h4436.md).

<a name="esther_7_7"></a>Esther 7:7

And the [melek](../../strongs/h/h4428.md) [quwm](../../strongs/h/h6965.md) from the [mištê](../../strongs/h/h4960.md) of [yayin](../../strongs/h/h3196.md) in his [chemah](../../strongs/h/h2534.md) went into the [bîṯān](../../strongs/h/h1055.md) [ginnâ](../../strongs/h/h1594.md): and [Hāmān](../../strongs/h/h2001.md) ['amad](../../strongs/h/h5975.md) to make [bāqaš](../../strongs/h/h1245.md) for his [nephesh](../../strongs/h/h5315.md) to ['Estēr](../../strongs/h/h635.md) the [malkâ](../../strongs/h/h4436.md); for he [ra'ah](../../strongs/h/h7200.md) that there was [ra'](../../strongs/h/h7451.md) [kalah](../../strongs/h/h3615.md) against him by the [melek](../../strongs/h/h4428.md).

<a name="esther_7_8"></a>Esther 7:8

Then the [melek](../../strongs/h/h4428.md) [shuwb](../../strongs/h/h7725.md) out of the [bîṯān](../../strongs/h/h1055.md) [ginnâ](../../strongs/h/h1594.md) into the [bayith](../../strongs/h/h1004.md) of the [mištê](../../strongs/h/h4960.md) of [yayin](../../strongs/h/h3196.md); and [Hāmān](../../strongs/h/h2001.md) was [naphal](../../strongs/h/h5307.md) upon the [mittah](../../strongs/h/h4296.md) whereon ['Estēr](../../strongs/h/h635.md) was. Then ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md), Will he [kāḇaš](../../strongs/h/h3533.md) the [malkâ](../../strongs/h/h4436.md) also before me in the [bayith](../../strongs/h/h1004.md)? As the [dabar](../../strongs/h/h1697.md) [yāṣā'](../../strongs/h/h3318.md) of the [melek](../../strongs/h/h4428.md) [peh](../../strongs/h/h6310.md), they [ḥāp̄â](../../strongs/h/h2645.md) [Hāmān](../../strongs/h/h2001.md) [paniym](../../strongs/h/h6440.md).

<a name="esther_7_9"></a>Esther 7:9

And [Ḥarḇônā'](../../strongs/h/h2726.md), one of the [sārîs](../../strongs/h/h5631.md), ['āmar](../../strongs/h/h559.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md), Behold also, the ['ets](../../strongs/h/h6086.md) fifty ['ammâ](../../strongs/h/h520.md) [gāḇōha](../../strongs/h/h1364.md), which [Hāmān](../../strongs/h/h2001.md) had ['asah](../../strongs/h/h6213.md) for [Mārdᵊḵay](../../strongs/h/h4782.md), who had [dabar](../../strongs/h/h1696.md) [towb](../../strongs/h/h2896.md) for the [melek](../../strongs/h/h4428.md), ['amad](../../strongs/h/h5975.md) in the [bayith](../../strongs/h/h1004.md) of [Hāmān](../../strongs/h/h2001.md). Then the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), [tālâ](../../strongs/h/h8518.md) him thereon.

<a name="esther_7_10"></a>Esther 7:10

So they [tālâ](../../strongs/h/h8518.md) [Hāmān](../../strongs/h/h2001.md) on the ['ets](../../strongs/h/h6086.md) that he had [kuwn](../../strongs/h/h3559.md) for [Mārdᵊḵay](../../strongs/h/h4782.md). Then was the [melek](../../strongs/h/h4428.md) [chemah](../../strongs/h/h2534.md) [šāḵaḵ](../../strongs/h/h7918.md).

---

[Transliteral Bible](../bible.md)

[Esther](esther.md)

[Esther 6](esther_6.md) - [Esther 8](esther_8.md)