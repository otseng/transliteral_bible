# [Esther 3](https://www.blueletterbible.org/kjv/esther/3)

<a name="esther_3_1"></a>Esther 3:1

['aḥar](../../strongs/h/h310.md) these [dabar](../../strongs/h/h1697.md) did [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md) [gāḏal](../../strongs/h/h1431.md) [Hāmān](../../strongs/h/h2001.md) the [ben](../../strongs/h/h1121.md) of [Mᵊḏāṯā'](../../strongs/h/h4099.md) the ['Ăḡāḡay](../../strongs/h/h91.md), and [nasa'](../../strongs/h/h5375.md) him, and [śûm](../../strongs/h/h7760.md) his [kicce'](../../strongs/h/h3678.md) above all the [śar](../../strongs/h/h8269.md) that were with him.

<a name="esther_3_2"></a>Esther 3:2

And all the [melek](../../strongs/h/h4428.md) ['ebed](../../strongs/h/h5650.md), that were in the [melek](../../strongs/h/h4428.md) [sha'ar](../../strongs/h/h8179.md), [kara'](../../strongs/h/h3766.md), and [shachah](../../strongs/h/h7812.md) [Hāmān](../../strongs/h/h2001.md): for the [melek](../../strongs/h/h4428.md) had so [tsavah](../../strongs/h/h6680.md) concerning him. But [Mārdᵊḵay](../../strongs/h/h4782.md) [kara'](../../strongs/h/h3766.md) not, nor did him [shachah](../../strongs/h/h7812.md).

<a name="esther_3_3"></a>Esther 3:3

Then the [melek](../../strongs/h/h4428.md) ['ebed](../../strongs/h/h5650.md), which were in the [melek](../../strongs/h/h4428.md) [sha'ar](../../strongs/h/h8179.md), ['āmar](../../strongs/h/h559.md) unto [Mārdᵊḵay](../../strongs/h/h4782.md), Why ['abar](../../strongs/h/h5674.md) thou the [melek](../../strongs/h/h4428.md) [mitsvah](../../strongs/h/h4687.md)?

<a name="esther_3_4"></a>Esther 3:4

Now it came to pass, when they ['āmar](../../strongs/h/h559.md) [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) unto him, and he [shama'](../../strongs/h/h8085.md) not unto them, that they [nāḡaḏ](../../strongs/h/h5046.md) [Hāmān](../../strongs/h/h2001.md), to [ra'ah](../../strongs/h/h7200.md) whether [Mārdᵊḵay](../../strongs/h/h4782.md) [dabar](../../strongs/h/h1697.md) would ['amad](../../strongs/h/h5975.md): for he had [nāḡaḏ](../../strongs/h/h5046.md) them that he was a [Yᵊhûḏî](../../strongs/h/h3064.md).

<a name="esther_3_5"></a>Esther 3:5

And when [Hāmān](../../strongs/h/h2001.md) [ra'ah](../../strongs/h/h7200.md) that [Mārdᵊḵay](../../strongs/h/h4782.md) [kara'](../../strongs/h/h3766.md) not, nor did him [shachah](../../strongs/h/h7812.md), then was [Hāmān](../../strongs/h/h2001.md) [mālā'](../../strongs/h/h4390.md) of [chemah](../../strongs/h/h2534.md).

<a name="esther_3_6"></a>Esther 3:6

And he ['ayin](../../strongs/h/h5869.md) [bazah](../../strongs/h/h959.md) to [shalach](../../strongs/h/h7971.md) [yad](../../strongs/h/h3027.md) on [Mārdᵊḵay](../../strongs/h/h4782.md) alone; for they had [nāḡaḏ](../../strongs/h/h5046.md) him the ['am](../../strongs/h/h5971.md) of [Mārdᵊḵay](../../strongs/h/h4782.md): wherefore [Hāmān](../../strongs/h/h2001.md) [bāqaš](../../strongs/h/h1245.md) to [šāmaḏ](../../strongs/h/h8045.md) all the [Yᵊhûḏî](../../strongs/h/h3064.md) that were throughout the whole [malkuwth](../../strongs/h/h4438.md) of ['Ăḥašvērôš](../../strongs/h/h325.md), even the ['am](../../strongs/h/h5971.md) of [Mārdᵊḵay](../../strongs/h/h4782.md).

<a name="esther_3_7"></a>Esther 3:7

In the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md), that is, the [ḥōḏeš](../../strongs/h/h2320.md) [nîsān](../../strongs/h/h5212.md), in the twelfth [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md), they [naphal](../../strongs/h/h5307.md) [pûr](../../strongs/h/h6332.md), that is, the [gôrāl](../../strongs/h/h1486.md), [paniym](../../strongs/h/h6440.md) [Hāmān](../../strongs/h/h2001.md) from [yowm](../../strongs/h/h3117.md) to [yowm](../../strongs/h/h3117.md), and from [ḥōḏeš](../../strongs/h/h2320.md) to [ḥōḏeš](../../strongs/h/h2320.md), to the twelfth month, that is, the [ḥōḏeš](../../strongs/h/h2320.md) ['ăḏār](../../strongs/h/h143.md). [^1]

<a name="esther_3_8"></a>Esther 3:8

And [Hāmān](../../strongs/h/h2001.md) ['āmar](../../strongs/h/h559.md) unto [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md), There is a certain ['am](../../strongs/h/h5971.md) [p̄zr](../../strongs/h/h6340.md) and [pāraḏ](../../strongs/h/h6504.md) among the ['am](../../strongs/h/h5971.md) in all the [mᵊḏînâ](../../strongs/h/h4082.md) of thy [malkuwth](../../strongs/h/h4438.md); and their [dāṯ](../../strongs/h/h1881.md) are [šānâ](../../strongs/h/h8138.md) from all ['am](../../strongs/h/h5971.md); neither ['asah](../../strongs/h/h6213.md) they the [melek](../../strongs/h/h4428.md) [dāṯ](../../strongs/h/h1881.md): therefore it is not for the [melek](../../strongs/h/h4428.md) [šāvâ](../../strongs/h/h7737.md) to [yānaḥ](../../strongs/h/h3240.md) them.

<a name="esther_3_9"></a>Esther 3:9

If it [ṭôḇ](../../strongs/h/h2895.md) the [melek](../../strongs/h/h4428.md), let it be [kāṯaḇ](../../strongs/h/h3789.md) that they may be ['abad](../../strongs/h/h6.md): and I will [šāqal](../../strongs/h/h8254.md) ten thousand [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md) to the [yad](../../strongs/h/h3027.md) of those that have the ['asah](../../strongs/h/h6213.md) of the [mĕla'kah](../../strongs/h/h4399.md), to [bow'](../../strongs/h/h935.md) it into the [melek](../../strongs/h/h4428.md) [gᵊnāzîm](../../strongs/h/h1595.md).

<a name="esther_3_10"></a>Esther 3:10

And the [melek](../../strongs/h/h4428.md) [cuwr](../../strongs/h/h5493.md) his [ṭabaʿaṯ](../../strongs/h/h2885.md) from his [yad](../../strongs/h/h3027.md), and [nathan](../../strongs/h/h5414.md) it unto [Hāmān](../../strongs/h/h2001.md) the [ben](../../strongs/h/h1121.md) of [Mᵊḏāṯā'](../../strongs/h/h4099.md) the ['Ăḡāḡay](../../strongs/h/h91.md), the [Yᵊhûḏî](../../strongs/h/h3064.md) [tsarar](../../strongs/h/h6887.md).

<a name="esther_3_11"></a>Esther 3:11

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto [Hāmān](../../strongs/h/h2001.md), The [keceph](../../strongs/h/h3701.md) is [nathan](../../strongs/h/h5414.md) to thee, the ['am](../../strongs/h/h5971.md) also, to ['asah](../../strongs/h/h6213.md) with them as it ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md) to thee.

<a name="esther_3_12"></a>Esther 3:12

Then were the [melek](../../strongs/h/h4428.md) [sāp̄ar](../../strongs/h/h5608.md) [qara'](../../strongs/h/h7121.md) on the thirteenth [yowm](../../strongs/h/h3117.md) of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md), and there was [kāṯaḇ](../../strongs/h/h3789.md) according to all that [Hāmān](../../strongs/h/h2001.md) had [tsavah](../../strongs/h/h6680.md) unto the [melek](../../strongs/h/h4428.md) ['ăḥašdarpᵊnîm](../../strongs/h/h323.md), and to the [peḥâ](../../strongs/h/h6346.md) that were over every [mᵊḏînâ](../../strongs/h/h4082.md), and to the [śar](../../strongs/h/h8269.md) of every ['am](../../strongs/h/h5971.md) of every [mᵊḏînâ](../../strongs/h/h4082.md) according to the [kᵊṯāḇ](../../strongs/h/h3791.md) thereof, and to every ['am](../../strongs/h/h5971.md) after their [lashown](../../strongs/h/h3956.md); in the [shem](../../strongs/h/h8034.md) of [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md) was it [kāṯaḇ](../../strongs/h/h3789.md), and [ḥāṯam](../../strongs/h/h2856.md) with the [melek](../../strongs/h/h4428.md) [ṭabaʿaṯ](../../strongs/h/h2885.md).

<a name="esther_3_13"></a>Esther 3:13

And the [sēp̄er](../../strongs/h/h5612.md) were [shalach](../../strongs/h/h7971.md) [yad](../../strongs/h/h3027.md) [rûṣ](../../strongs/h/h7323.md) into all the [melek](../../strongs/h/h4428.md) [mᵊḏînâ](../../strongs/h/h4082.md), to [šāmaḏ](../../strongs/h/h8045.md), to [harag](../../strongs/h/h2026.md), and to cause to ['abad](../../strongs/h/h6.md), all [Yᵊhûḏî](../../strongs/h/h3064.md), both [naʿar](../../strongs/h/h5288.md) and [zāqēn](../../strongs/h/h2205.md), [ṭap̄](../../strongs/h/h2945.md) and ['ishshah](../../strongs/h/h802.md), in one [yowm](../../strongs/h/h3117.md), even upon the thirteenth day of the twelfth [ḥōḏeš](../../strongs/h/h2320.md), which is the [ḥōḏeš](../../strongs/h/h2320.md) ['ăḏār](../../strongs/h/h143.md), and to take the [šālāl](../../strongs/h/h7998.md) of them for a [bāzaz](../../strongs/h/h962.md).

<a name="esther_3_14"></a>Esther 3:14

The [paršeḡen](../../strongs/h/h6572.md) of the [kᵊṯāḇ](../../strongs/h/h3791.md) for a [dāṯ](../../strongs/h/h1881.md) to be [nathan](../../strongs/h/h5414.md) in every [mᵊḏînâ](../../strongs/h/h4082.md) was [gālâ](../../strongs/h/h1540.md) unto all ['am](../../strongs/h/h5971.md), that they should be [ʿāṯîḏ](../../strongs/h/h6264.md) against that [yowm](../../strongs/h/h3117.md).

<a name="esther_3_15"></a>Esther 3:15

The [rûṣ](../../strongs/h/h7323.md) [yāṣā'](../../strongs/h/h3318.md), being [dāḥap̄](../../strongs/h/h1765.md) by the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md), and the [dāṯ](../../strongs/h/h1881.md) was [nathan](../../strongs/h/h5414.md) in [Šûšan](../../strongs/h/h7800.md) the [bîrâ](../../strongs/h/h1002.md). And the [melek](../../strongs/h/h4428.md) and [Hāmān](../../strongs/h/h2001.md) [yashab](../../strongs/h/h3427.md) to [šāṯâ](../../strongs/h/h8354.md); but the [ʿîr](../../strongs/h/h5892.md) [Šûšan](../../strongs/h/h7800.md) was [bûḵ](../../strongs/h/h943.md).

---

[Transliteral Bible](../bible.md)

[Esther](esther.md)

[Esther 2](esther_2.md) - [Esther 4](esther_4.md)

---

[^1]: [Esther 3:7 Commentary](../../commentary/esther/esther_3_commentary.md#esther_3_7)
