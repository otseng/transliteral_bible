# [Esther 2](https://www.blueletterbible.org/kjv/esther/2)

<a name="esther_2_1"></a>Esther 2:1

['aḥar](../../strongs/h/h310.md) these [dabar](../../strongs/h/h1697.md), when the [chemah](../../strongs/h/h2534.md) of [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md) was [šāḵaḵ](../../strongs/h/h7918.md), he [zakar](../../strongs/h/h2142.md) [Vaštî](../../strongs/h/h2060.md), and what she had ['asah](../../strongs/h/h6213.md), and what was [gāzar](../../strongs/h/h1504.md) against her.

<a name="esther_2_2"></a>Esther 2:2

Then ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md) [naʿar](../../strongs/h/h5288.md) that [sharath](../../strongs/h/h8334.md) unto him, Let there be [towb](../../strongs/h/h2896.md) [mar'ê](../../strongs/h/h4758.md) [naʿărâ](../../strongs/h/h5291.md) [bᵊṯûlâ](../../strongs/h/h1330.md) [bāqaš](../../strongs/h/h1245.md) for the [melek](../../strongs/h/h4428.md):

<a name="esther_2_3"></a>Esther 2:3

And let the [melek](../../strongs/h/h4428.md) [paqad](../../strongs/h/h6485.md) [pāqîḏ](../../strongs/h/h6496.md) in all the [mᵊḏînâ](../../strongs/h/h4082.md) of his [malkuwth](../../strongs/h/h4438.md), that they may [qāḇaṣ](../../strongs/h/h6908.md) all the [towb](../../strongs/h/h2896.md) [mar'ê](../../strongs/h/h4758.md) [naʿărâ](../../strongs/h/h5291.md) [bᵊṯûlâ](../../strongs/h/h1330.md) unto [Šûšan](../../strongs/h/h7800.md) the [bîrâ](../../strongs/h/h1002.md), to the [bayith](../../strongs/h/h1004.md) of the ['ishshah](../../strongs/h/h802.md), unto the [yad](../../strongs/h/h3027.md) of [Hēḡē'](../../strongs/h/h1896.md) the [melek](../../strongs/h/h4428.md) [sārîs](../../strongs/h/h5631.md), [shamar](../../strongs/h/h8104.md) of the ['ishshah](../../strongs/h/h802.md); and let their things for [tamrûq](../../strongs/h/h8562.md) be [nathan](../../strongs/h/h5414.md) them:

<a name="esther_2_4"></a>Esther 2:4

And let the [naʿărâ](../../strongs/h/h5291.md) which [yatab](../../strongs/h/h3190.md) ['ayin](../../strongs/h/h5869.md) the [melek](../../strongs/h/h4428.md) be [mālaḵ](../../strongs/h/h4427.md) instead of [Vaštî](../../strongs/h/h2060.md). And the [dabar](../../strongs/h/h1697.md) [yatab](../../strongs/h/h3190.md) ['ayin](../../strongs/h/h5869.md) the [melek](../../strongs/h/h4428.md); and he did ['asah](../../strongs/h/h6213.md).

<a name="esther_2_5"></a>Esther 2:5

Now in [Šûšan](../../strongs/h/h7800.md) the [bîrâ](../../strongs/h/h1002.md) there was an ['iysh](../../strongs/h/h376.md) [Yᵊhûḏî](../../strongs/h/h3064.md), whose [shem](../../strongs/h/h8034.md) was [Mārdᵊḵay](../../strongs/h/h4782.md), the [ben](../../strongs/h/h1121.md) of [Yā'Îr](../../strongs/h/h2971.md), the [ben](../../strongs/h/h1121.md) of [Šimʿî](../../strongs/h/h8096.md), the [ben](../../strongs/h/h1121.md) of [Qîš](../../strongs/h/h7027.md), a [Ben-yᵊmînî](../../strongs/h/h1145.md);

<a name="esther_2_6"></a>Esther 2:6

Who had been [gālâ](../../strongs/h/h1540.md) from [Yĕruwshalaim](../../strongs/h/h3389.md) with the [gôlâ](../../strongs/h/h1473.md) which had been [gālâ](../../strongs/h/h1540.md) with [Yᵊḵānyâ](../../strongs/h/h3204.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), whom [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) had [gālâ](../../strongs/h/h1540.md).

<a name="esther_2_7"></a>Esther 2:7

And he ['aman](../../strongs/h/h539.md) [Hăḏasâ](../../strongs/h/h1919.md), that is, ['Estēr](../../strongs/h/h635.md), his [dôḏ](../../strongs/h/h1730.md) [bath](../../strongs/h/h1323.md): for she had neither ['ab](../../strongs/h/h1.md) nor ['em](../../strongs/h/h517.md), and the [naʿărâ](../../strongs/h/h5291.md) was [yāp̄ê](../../strongs/h/h3303.md) [tō'ar](../../strongs/h/h8389.md) and [towb](../../strongs/h/h2896.md) [mar'ê](../../strongs/h/h4758.md); whom [Mārdᵊḵay](../../strongs/h/h4782.md), when her ['ab](../../strongs/h/h1.md) and ['em](../../strongs/h/h517.md) were [maveth](../../strongs/h/h4194.md), [laqach](../../strongs/h/h3947.md) for his own [bath](../../strongs/h/h1323.md).

<a name="esther_2_8"></a>Esther 2:8

So it came to pass, when the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) and his [dāṯ](../../strongs/h/h1881.md) was [shama'](../../strongs/h/h8085.md), and when [rab](../../strongs/h/h7227.md) [naʿărâ](../../strongs/h/h5291.md) were [qāḇaṣ](../../strongs/h/h6908.md) unto [Šûšan](../../strongs/h/h7800.md) the [bîrâ](../../strongs/h/h1002.md), to the [yad](../../strongs/h/h3027.md) of [Hēḡē'](../../strongs/h/h1896.md), that ['Estēr](../../strongs/h/h635.md) was [laqach](../../strongs/h/h3947.md) also unto the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), to the [yad](../../strongs/h/h3027.md) of [Hēḡē'](../../strongs/h/h1896.md), [shamar](../../strongs/h/h8104.md) of the ['ishshah](../../strongs/h/h802.md).

<a name="esther_2_9"></a>Esther 2:9

And the [naʿărâ](../../strongs/h/h5291.md) [yatab](../../strongs/h/h3190.md) ['ayin](../../strongs/h/h5869.md) him, and she [nasa'](../../strongs/h/h5375.md) [checed](../../strongs/h/h2617.md) of [paniym](../../strongs/h/h6440.md); and he [bahal](../../strongs/h/h926.md) [nathan](../../strongs/h/h5414.md) her her things for [tamrûq](../../strongs/h/h8562.md), with such things as [mānâ](../../strongs/h/h4490.md) to her, and seven [naʿărâ](../../strongs/h/h5291.md), which were [ra'ah](../../strongs/h/h7200.md) to be [nathan](../../strongs/h/h5414.md) her, out of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md): and he [šānâ](../../strongs/h/h8138.md) her and her [naʿărâ](../../strongs/h/h5291.md) unto the [towb](../../strongs/h/h2896.md) place of the [bayith](../../strongs/h/h1004.md) of the ['ishshah](../../strongs/h/h802.md).

<a name="esther_2_10"></a>Esther 2:10

['Estēr](../../strongs/h/h635.md) had not [nāḡaḏ](../../strongs/h/h5046.md) her ['am](../../strongs/h/h5971.md) nor her [môleḏeṯ](../../strongs/h/h4138.md): for [Mārdᵊḵay](../../strongs/h/h4782.md) had [tsavah](../../strongs/h/h6680.md) her that she should not [nāḡaḏ](../../strongs/h/h5046.md) it.

<a name="esther_2_11"></a>Esther 2:11

And [Mārdᵊḵay](../../strongs/h/h4782.md) [halak](../../strongs/h/h1980.md) every [yowm](../../strongs/h/h3117.md) [paniym](../../strongs/h/h6440.md) the [ḥāṣēr](../../strongs/h/h2691.md) of the ['ishshah](../../strongs/h/h802.md) [bayith](../../strongs/h/h1004.md), to [yada'](../../strongs/h/h3045.md) how ['Estēr](../../strongs/h/h635.md) [shalowm](../../strongs/h/h7965.md), and what should ['asah](../../strongs/h/h6213.md) of her.

<a name="esther_2_12"></a>Esther 2:12

Now when every [naʿărâ](../../strongs/h/h5291.md) [tôr](../../strongs/h/h8447.md) was [naga'](../../strongs/h/h5060.md) to [bow'](../../strongs/h/h935.md) to [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md), [qēṣ](../../strongs/h/h7093.md) that she had been twelve [ḥōḏeš](../../strongs/h/h2320.md), according to the [dāṯ](../../strongs/h/h1881.md) of the ['ishshah](../../strongs/h/h802.md), (for so were the [yowm](../../strongs/h/h3117.md) of their [mᵊrûqîm](../../strongs/h/h4795.md) [mālā'](../../strongs/h/h4390.md), to wit, six [ḥōḏeš](../../strongs/h/h2320.md) with [šemen](../../strongs/h/h8081.md) of [mōr](../../strongs/h/h4753.md), and six [ḥōḏeš](../../strongs/h/h2320.md) with [beśem](../../strongs/h/h1314.md), and with other things for the [tamrûq](../../strongs/h/h8562.md) of the ['ishshah](../../strongs/h/h802.md);)

<a name="esther_2_13"></a>Esther 2:13

Then thus [bow'](../../strongs/h/h935.md) every [naʿărâ](../../strongs/h/h5291.md) unto the [melek](../../strongs/h/h4428.md); whatsoever she ['āmar](../../strongs/h/h559.md) was [nathan](../../strongs/h/h5414.md) her to [bow'](../../strongs/h/h935.md) with her out of the [bayith](../../strongs/h/h1004.md) of the ['ishshah](../../strongs/h/h802.md) unto the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md).

<a name="esther_2_14"></a>Esther 2:14

In the ['ereb](../../strongs/h/h6153.md) she [bow'](../../strongs/h/h935.md), and on the [boqer](../../strongs/h/h1242.md) she [shuwb](../../strongs/h/h7725.md) into the second [bayith](../../strongs/h/h1004.md) of the ['ishshah](../../strongs/h/h802.md), to the [yad](../../strongs/h/h3027.md) of [ŠaʿAšgaz](../../strongs/h/h8190.md), the [melek](../../strongs/h/h4428.md) [sārîs](../../strongs/h/h5631.md), which [shamar](../../strongs/h/h8104.md) the [pîleḡeš](../../strongs/h/h6370.md): she [bow'](../../strongs/h/h935.md) unto the [melek](../../strongs/h/h4428.md) no more, except the [melek](../../strongs/h/h4428.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) in her, and that she were [qara'](../../strongs/h/h7121.md) by [shem](../../strongs/h/h8034.md).

<a name="esther_2_15"></a>Esther 2:15

Now when the [tôr](../../strongs/h/h8447.md) of ['Estēr](../../strongs/h/h635.md), the [bath](../../strongs/h/h1323.md) of ['Ăḇîhayil](../../strongs/h/h32.md) the [dôḏ](../../strongs/h/h1730.md) of [Mārdᵊḵay](../../strongs/h/h4782.md), who had [laqach](../../strongs/h/h3947.md) her for his [bath](../../strongs/h/h1323.md), was [naga'](../../strongs/h/h5060.md) to [bow'](../../strongs/h/h935.md) unto the [melek](../../strongs/h/h4428.md), she [bāqaš](../../strongs/h/h1245.md) [dabar](../../strongs/h/h1697.md) but what [Hēḡē'](../../strongs/h/h1896.md) the [melek](../../strongs/h/h4428.md) [sārîs](../../strongs/h/h5631.md), the [shamar](../../strongs/h/h8104.md) of the ['ishshah](../../strongs/h/h802.md), ['āmar](../../strongs/h/h559.md). And ['Estēr](../../strongs/h/h635.md) [nasa'](../../strongs/h/h5375.md) [ḥēn](../../strongs/h/h2580.md) in the ['ayin](../../strongs/h/h5869.md) of all them that [ra'ah](../../strongs/h/h7200.md) upon her.

<a name="esther_2_16"></a>Esther 2:16

So ['Estēr](../../strongs/h/h635.md) was [laqach](../../strongs/h/h3947.md) unto [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md) into his [bayith](../../strongs/h/h1004.md) [malkuwth](../../strongs/h/h4438.md) in the tenth [ḥōḏeš](../../strongs/h/h2320.md), which is the [ḥōḏeš](../../strongs/h/h2320.md) [ṭēḇēṯ](../../strongs/h/h2887.md), in the seventh [šānâ](../../strongs/h/h8141.md) of his [malkuwth](../../strongs/h/h4438.md).

<a name="esther_2_17"></a>Esther 2:17

And the [melek](../../strongs/h/h4428.md) ['ahab](../../strongs/h/h157.md) ['Estēr](../../strongs/h/h635.md) above all the ['ishshah](../../strongs/h/h802.md), and she [nasa'](../../strongs/h/h5375.md) [ḥēn](../../strongs/h/h2580.md) and [checed](../../strongs/h/h2617.md) in his [paniym](../../strongs/h/h6440.md) more than all the [bᵊṯûlâ](../../strongs/h/h1330.md); so that he [śûm](../../strongs/h/h7760.md) the [malkuwth](../../strongs/h/h4438.md) [keṯer](../../strongs/h/h3804.md) upon her [ro'sh](../../strongs/h/h7218.md), and made her [mālaḵ](../../strongs/h/h4427.md) instead of [Vaštî](../../strongs/h/h2060.md).

<a name="esther_2_18"></a>Esther 2:18

Then the [melek](../../strongs/h/h4428.md) ['asah](../../strongs/h/h6213.md) a [gadowl](../../strongs/h/h1419.md) [mištê](../../strongs/h/h4960.md) unto all his [śar](../../strongs/h/h8269.md) and his ['ebed](../../strongs/h/h5650.md), even ['Estēr](../../strongs/h/h635.md) [mištê](../../strongs/h/h4960.md); and he ['asah](../../strongs/h/h6213.md) a [hănāḥâ](../../strongs/h/h2010.md) to the [mᵊḏînâ](../../strongs/h/h4082.md), and [nathan](../../strongs/h/h5414.md) [maśśᵊ'ēṯ](../../strongs/h/h4864.md), according to the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md).

<a name="esther_2_19"></a>Esther 2:19

And when the [bᵊṯûlâ](../../strongs/h/h1330.md) were [qāḇaṣ](../../strongs/h/h6908.md) the second time, then [Mārdᵊḵay](../../strongs/h/h4782.md) [yashab](../../strongs/h/h3427.md) in the [melek](../../strongs/h/h4428.md) [sha'ar](../../strongs/h/h8179.md).

<a name="esther_2_20"></a>Esther 2:20

['Estēr](../../strongs/h/h635.md) had not yet [nāḡaḏ](../../strongs/h/h5046.md) her [môleḏeṯ](../../strongs/h/h4138.md) nor her ['am](../../strongs/h/h5971.md); as [Mārdᵊḵay](../../strongs/h/h4782.md) had [tsavah](../../strongs/h/h6680.md) her: for ['Estēr](../../strongs/h/h635.md) ['asah](../../strongs/h/h6213.md) the [ma'ămār](../../strongs/h/h3982.md) of [Mārdᵊḵay](../../strongs/h/h4782.md), like as when she was ['āmnâ](../../strongs/h/h545.md) with him.

<a name="esther_2_21"></a>Esther 2:21

In those [yowm](../../strongs/h/h3117.md), while [Mārdᵊḵay](../../strongs/h/h4782.md) [yashab](../../strongs/h/h3427.md) in the [melek](../../strongs/h/h4428.md) [sha'ar](../../strongs/h/h8179.md), two of the [melek](../../strongs/h/h4428.md) [sārîs](../../strongs/h/h5631.md), [Biḡṯān](../../strongs/h/h904.md) and [Tereš](../../strongs/h/h8657.md), of those which [shamar](../../strongs/h/h8104.md) the [caph](../../strongs/h/h5592.md), were [qāṣap̄](../../strongs/h/h7107.md), and [bāqaš](../../strongs/h/h1245.md) to [shalach](../../strongs/h/h7971.md) [yad](../../strongs/h/h3027.md) on the [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md).

<a name="esther_2_22"></a>Esther 2:22

And the [dabar](../../strongs/h/h1697.md) was [yada'](../../strongs/h/h3045.md) to [Mārdᵊḵay](../../strongs/h/h4782.md), who [nāḡaḏ](../../strongs/h/h5046.md) it unto ['Estēr](../../strongs/h/h635.md) the [malkâ](../../strongs/h/h4436.md); and ['Estēr](../../strongs/h/h635.md) ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md) thereof in [Mārdᵊḵay](../../strongs/h/h4782.md) [shem](../../strongs/h/h8034.md).

<a name="esther_2_23"></a>Esther 2:23

And when [bāqaš](../../strongs/h/h1245.md) was made of the [dabar](../../strongs/h/h1697.md), it was [māṣā'](../../strongs/h/h4672.md); therefore they were both [tālâ](../../strongs/h/h8518.md) on an ['ets](../../strongs/h/h6086.md): and it was [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

---

[Transliteral Bible](../bible.md)

[Esther](esther.md)

[Esther 1](esther_1.md) - [Esther 3](esther_3.md)