# [Esther 8](https://www.blueletterbible.org/kjv/esther/8)

<a name="esther_8_1"></a>Esther 8:1

On that [yowm](../../strongs/h/h3117.md) did the [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md) [nathan](../../strongs/h/h5414.md) the [bayith](../../strongs/h/h1004.md) of [Hāmān](../../strongs/h/h2001.md) the [Yᵊhûḏî](../../strongs/h/h3064.md) [tsarar](../../strongs/h/h6887.md) unto ['Estēr](../../strongs/h/h635.md) the [malkâ](../../strongs/h/h4436.md). And [Mārdᵊḵay](../../strongs/h/h4782.md) [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md); for ['Estēr](../../strongs/h/h635.md) had [nāḡaḏ](../../strongs/h/h5046.md) what he was unto her.

<a name="esther_8_2"></a>Esther 8:2

And the [melek](../../strongs/h/h4428.md) [cuwr](../../strongs/h/h5493.md) his [ṭabaʿaṯ](../../strongs/h/h2885.md), which he had ['abar](../../strongs/h/h5674.md) from [Hāmān](../../strongs/h/h2001.md), and [nathan](../../strongs/h/h5414.md) it unto [Mārdᵊḵay](../../strongs/h/h4782.md). And ['Estēr](../../strongs/h/h635.md) [śûm](../../strongs/h/h7760.md) [Mārdᵊḵay](../../strongs/h/h4782.md) over the [bayith](../../strongs/h/h1004.md) of [Hāmān](../../strongs/h/h2001.md).

<a name="esther_8_3"></a>Esther 8:3

And ['Estēr](../../strongs/h/h635.md) [dabar](../../strongs/h/h1696.md) yet again [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md), and [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) his [regel](../../strongs/h/h7272.md), and [chanan](../../strongs/h/h2603.md) him with [bāḵâ](../../strongs/h/h1058.md) to ['abar](../../strongs/h/h5674.md) the [ra'](../../strongs/h/h7451.md) of [Hāmān](../../strongs/h/h2001.md) the ['Ăḡāḡay](../../strongs/h/h91.md), and his [maḥăšāḇâ](../../strongs/h/h4284.md) that he had [chashab](../../strongs/h/h2803.md) against the [Yᵊhûḏî](../../strongs/h/h3064.md).

<a name="esther_8_4"></a>Esther 8:4

Then the [melek](../../strongs/h/h4428.md) held [yāšaṭ](../../strongs/h/h3447.md) the [zāhāḇ](../../strongs/h/h2091.md) [šarbîṭ](../../strongs/h/h8275.md) toward ['Estēr](../../strongs/h/h635.md). So ['Estēr](../../strongs/h/h635.md) [quwm](../../strongs/h/h6965.md), and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md),

<a name="esther_8_5"></a>Esther 8:5

And ['āmar](../../strongs/h/h559.md), If it [towb](../../strongs/h/h2896.md) the [melek](../../strongs/h/h4428.md), and if I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in his [paniym](../../strongs/h/h6440.md), and the [dabar](../../strongs/h/h1697.md) seem [kāšēr](../../strongs/h/h3787.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md), and I be [towb](../../strongs/h/h2896.md) in his ['ayin](../../strongs/h/h5869.md), let it be [kāṯaḇ](../../strongs/h/h3789.md) to [shuwb](../../strongs/h/h7725.md) the [sēp̄er](../../strongs/h/h5612.md) [maḥăšāḇâ](../../strongs/h/h4284.md) by [Hāmān](../../strongs/h/h2001.md) the [ben](../../strongs/h/h1121.md) of [Mᵊḏāṯā'](../../strongs/h/h4099.md) the ['Ăḡāḡay](../../strongs/h/h91.md), which he [kāṯaḇ](../../strongs/h/h3789.md) to ['abad](../../strongs/h/h6.md) the [Yᵊhûḏî](../../strongs/h/h3064.md) which are in all the [melek](../../strongs/h/h4428.md) [mᵊḏînâ](../../strongs/h/h4082.md):

<a name="esther_8_6"></a>Esther 8:6

For how can I [yakol](../../strongs/h/h3201.md) to [ra'ah](../../strongs/h/h7200.md) the [ra'](../../strongs/h/h7451.md) that shall [māṣā'](../../strongs/h/h4672.md) unto my ['am](../../strongs/h/h5971.md)? or how can I [yakol](../../strongs/h/h3201.md) to [ra'ah](../../strongs/h/h7200.md) the ['āḇḏān](../../strongs/h/h13.md) of my [môleḏeṯ](../../strongs/h/h4138.md)?

<a name="esther_8_7"></a>Esther 8:7

Then the [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md) ['āmar](../../strongs/h/h559.md) unto ['Estēr](../../strongs/h/h635.md) the [malkâ](../../strongs/h/h4436.md) and to [Mārdᵊḵay](../../strongs/h/h4782.md) the [Yᵊhûḏî](../../strongs/h/h3064.md), Behold, I have [nathan](../../strongs/h/h5414.md) ['Estēr](../../strongs/h/h635.md) the [bayith](../../strongs/h/h1004.md) of [Hāmān](../../strongs/h/h2001.md), and him they have [tālâ](../../strongs/h/h8518.md) upon the ['ets](../../strongs/h/h6086.md), because he [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) upon the [Yᵊhûḏî](../../strongs/h/h3064.md).

<a name="esther_8_8"></a>Esther 8:8

[kāṯaḇ](../../strongs/h/h3789.md) ye also for the [Yᵊhûḏî](../../strongs/h/h3064.md), as it [towb](../../strongs/h/h2896.md) ['ayin](../../strongs/h/h5869.md), in the [melek](../../strongs/h/h4428.md) [shem](../../strongs/h/h8034.md), and [ḥāṯam](../../strongs/h/h2856.md) it with the [melek](../../strongs/h/h4428.md) [ṭabaʿaṯ](../../strongs/h/h2885.md): for the [kᵊṯāḇ](../../strongs/h/h3791.md) which is [kāṯaḇ](../../strongs/h/h3789.md) in the [melek](../../strongs/h/h4428.md) [shem](../../strongs/h/h8034.md), and [ḥāṯam](../../strongs/h/h2856.md) with the [melek](../../strongs/h/h4428.md) [ṭabaʿaṯ](../../strongs/h/h2885.md), may no man [shuwb](../../strongs/h/h7725.md).

<a name="esther_8_9"></a>Esther 8:9

Then were the [melek](../../strongs/h/h4428.md) [sāp̄ar](../../strongs/h/h5608.md) [qara'](../../strongs/h/h7121.md) at that [ʿēṯ](../../strongs/h/h6256.md) in the third [ḥōḏeš](../../strongs/h/h2320.md), that is, the [ḥōḏeš](../../strongs/h/h2320.md) [sîvān](../../strongs/h/h5510.md), on the three and twentieth day thereof; and it was [kāṯaḇ](../../strongs/h/h3789.md) according to all that [Mārdᵊḵay](../../strongs/h/h4782.md) [tsavah](../../strongs/h/h6680.md) unto the [Yᵊhûḏî](../../strongs/h/h3064.md), and to the ['ăḥašdarpᵊnîm](../../strongs/h/h323.md), and the [peḥâ](../../strongs/h/h6346.md) and [śar](../../strongs/h/h8269.md) of the [mᵊḏînâ](../../strongs/h/h4082.md) which are from [Hōdû](../../strongs/h/h1912.md) unto [Kûš](../../strongs/h/h3568.md), an hundred twenty and seven [mᵊḏînâ](../../strongs/h/h4082.md), unto every [mᵊḏînâ](../../strongs/h/h4082.md) according to the [kᵊṯāḇ](../../strongs/h/h3791.md) thereof, and unto every ['am](../../strongs/h/h5971.md) after their [lashown](../../strongs/h/h3956.md), and to the [Yᵊhûḏî](../../strongs/h/h3064.md) according to their [kᵊṯāḇ](../../strongs/h/h3791.md), and according to their [lashown](../../strongs/h/h3956.md).

<a name="esther_8_10"></a>Esther 8:10

And he [kāṯaḇ](../../strongs/h/h3789.md) in the [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md) [shem](../../strongs/h/h8034.md), and [ḥāṯam](../../strongs/h/h2856.md) it with the [melek](../../strongs/h/h4428.md) [ṭabaʿaṯ](../../strongs/h/h2885.md), and [shalach](../../strongs/h/h7971.md) [sēp̄er](../../strongs/h/h5612.md) [yad](../../strongs/h/h3027.md) [rûṣ](../../strongs/h/h7323.md) on [sûs](../../strongs/h/h5483.md), and [rāḵaḇ](../../strongs/h/h7392.md) on [reḵeš](../../strongs/h/h7409.md), ['ăḥaštᵊrān](../../strongs/h/h327.md), and [ben](../../strongs/h/h1121.md) [rmḵh](../../strongs/h/h7424.md):

<a name="esther_8_11"></a>Esther 8:11

Wherein the [melek](../../strongs/h/h4428.md) [nathan](../../strongs/h/h5414.md) the [Yᵊhûḏî](../../strongs/h/h3064.md) which were in every [ʿîr](../../strongs/h/h5892.md) to gather themselves [qāhal](../../strongs/h/h6950.md), and to ['amad](../../strongs/h/h5975.md) for their [nephesh](../../strongs/h/h5315.md), to [šāmaḏ](../../strongs/h/h8045.md), to [harag](../../strongs/h/h2026.md), and to cause to ['abad](../../strongs/h/h6.md), all the [ḥayil](../../strongs/h/h2428.md) of the ['am](../../strongs/h/h5971.md) and [mᵊḏînâ](../../strongs/h/h4082.md) that would [ṣûr](../../strongs/h/h6696.md) them, both [ṭap̄](../../strongs/h/h2945.md) and ['ishshah](../../strongs/h/h802.md), and to take the [šālāl](../../strongs/h/h7998.md) of them for a [bāzaz](../../strongs/h/h962.md),

<a name="esther_8_12"></a>Esther 8:12

Upon one [yowm](../../strongs/h/h3117.md) in all the [mᵊḏînâ](../../strongs/h/h4082.md) of [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md), namely, upon the thirteenth day of the twelfth [ḥōḏeš](../../strongs/h/h2320.md), which is the [ḥōḏeš](../../strongs/h/h2320.md) ['ăḏār](../../strongs/h/h143.md).

<a name="esther_8_13"></a>Esther 8:13

The [paršeḡen](../../strongs/h/h6572.md) of the [kᵊṯāḇ](../../strongs/h/h3791.md) for a [dāṯ](../../strongs/h/h1881.md) to be [nathan](../../strongs/h/h5414.md) in every [mᵊḏînâ](../../strongs/h/h4082.md) was [gālâ](../../strongs/h/h1540.md) unto all ['am](../../strongs/h/h5971.md), and that the [Yᵊhûḏî](../../strongs/h/h3064.md) should be [ʿāṯîḏ](../../strongs/h/h6264.md) [ʿāṯûḏ](../../strongs/h/h6259.md) against that [yowm](../../strongs/h/h3117.md) to [naqam](../../strongs/h/h5358.md) themselves on their ['oyeb](../../strongs/h/h341.md).

<a name="esther_8_14"></a>Esther 8:14

So the [rûṣ](../../strongs/h/h7323.md) that [rāḵaḇ](../../strongs/h/h7392.md) upon [reḵeš](../../strongs/h/h7409.md) and ['ăḥaštᵊrān](../../strongs/h/h327.md) [yāṣā'](../../strongs/h/h3318.md), being [bahal](../../strongs/h/h926.md) and [dāḥap̄](../../strongs/h/h1765.md) by the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md). And the [dāṯ](../../strongs/h/h1881.md) was [nathan](../../strongs/h/h5414.md) at [Šûšan](../../strongs/h/h7800.md) the [bîrâ](../../strongs/h/h1002.md).

<a name="esther_8_15"></a>Esther 8:15

And [Mārdᵊḵay](../../strongs/h/h4782.md) [yāṣā'](../../strongs/h/h3318.md) from the [paniym](../../strongs/h/h6440.md) of the [melek](../../strongs/h/h4428.md) in [malkuwth](../../strongs/h/h4438.md) [lᵊḇûš](../../strongs/h/h3830.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md) and [ḥûr](../../strongs/h/h2353.md), and with a [gadowl](../../strongs/h/h1419.md) [ʿăṭārâ](../../strongs/h/h5850.md) of [zāhāḇ](../../strongs/h/h2091.md), and with a [taḵrîḵ](../../strongs/h/h8509.md) of [bûṣ](../../strongs/h/h948.md) and ['argāmān](../../strongs/h/h713.md): and the [ʿîr](../../strongs/h/h5892.md) of [Šûšan](../../strongs/h/h7800.md) [ṣāhal](../../strongs/h/h6670.md) and was [śāmēaḥ](../../strongs/h/h8056.md).

<a name="esther_8_16"></a>Esther 8:16

The [Yᵊhûḏî](../../strongs/h/h3064.md) had ['ôrâ](../../strongs/h/h219.md), and [simchah](../../strongs/h/h8057.md), and [śāśôn](../../strongs/h/h8342.md), and [yᵊqār](../../strongs/h/h3366.md).

<a name="esther_8_17"></a>Esther 8:17

And in every [mᵊḏînâ](../../strongs/h/h4082.md), and in every [ʿîr](../../strongs/h/h5892.md), [maqowm](../../strongs/h/h4725.md) the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) and his [dāṯ](../../strongs/h/h1881.md) [naga'](../../strongs/h/h5060.md), the [Yᵊhûḏî](../../strongs/h/h3064.md) had [simchah](../../strongs/h/h8057.md) and [śāśôn](../../strongs/h/h8342.md), a [mištê](../../strongs/h/h4960.md) and a [towb](../../strongs/h/h2896.md) [yowm](../../strongs/h/h3117.md). And [rab](../../strongs/h/h7227.md) of the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) became [yāhaḏ](../../strongs/h/h3054.md); for the [paḥaḏ](../../strongs/h/h6343.md) of the [Yᵊhûḏî](../../strongs/h/h3064.md) [naphal](../../strongs/h/h5307.md) upon them.

---

[Transliteral Bible](../bible.md)

[Esther](esther.md)

[Esther 7](esther_7.md) - [Esther 9](esther_9.md)