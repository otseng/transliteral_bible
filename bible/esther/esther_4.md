# [Esther 4](https://www.blueletterbible.org/kjv/esther/4)

<a name="esther_4_1"></a>Esther 4:1

When [Mārdᵊḵay](../../strongs/h/h4782.md) [yada'](../../strongs/h/h3045.md) all that was ['asah](../../strongs/h/h6213.md), [Mārdᵊḵay](../../strongs/h/h4782.md) [qāraʿ](../../strongs/h/h7167.md) his [beḡeḏ](../../strongs/h/h899.md), and [labash](../../strongs/h/h3847.md) [śaq](../../strongs/h/h8242.md) with ['ēp̄er](../../strongs/h/h665.md), and [yāṣā'](../../strongs/h/h3318.md) into the [tavek](../../strongs/h/h8432.md) of the [ʿîr](../../strongs/h/h5892.md), and [zāʿaq](../../strongs/h/h2199.md) with a [gadowl](../../strongs/h/h1419.md) and a [mar](../../strongs/h/h4751.md) [zaʿaq](../../strongs/h/h2201.md);

<a name="esther_4_2"></a>Esther 4:2

And [bow'](../../strongs/h/h935.md) even [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) [sha'ar](../../strongs/h/h8179.md): for none might [bow'](../../strongs/h/h935.md) into the [melek](../../strongs/h/h4428.md) [sha'ar](../../strongs/h/h8179.md) [lᵊḇûš](../../strongs/h/h3830.md) with [śaq](../../strongs/h/h8242.md).

<a name="esther_4_3"></a>Esther 4:3

And in every [mᵊḏînâ](../../strongs/h/h4082.md), [maqowm](../../strongs/h/h4725.md) the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) and his [dāṯ](../../strongs/h/h1881.md) [naga'](../../strongs/h/h5060.md), there was [gadowl](../../strongs/h/h1419.md) ['ēḇel](../../strongs/h/h60.md) among the [Yᵊhûḏî](../../strongs/h/h3064.md), and [ṣôm](../../strongs/h/h6685.md), and [bĕkiy](../../strongs/h/h1065.md), and [mispēḏ](../../strongs/h/h4553.md); and [rab](../../strongs/h/h7227.md) [yāṣaʿ](../../strongs/h/h3331.md) in [śaq](../../strongs/h/h8242.md) and ['ēp̄er](../../strongs/h/h665.md).

<a name="esther_4_4"></a>Esther 4:4

So ['Estēr](../../strongs/h/h635.md) [naʿărâ](../../strongs/h/h5291.md) and her [sārîs](../../strongs/h/h5631.md) [bow'](../../strongs/h/h935.md) and [nāḡaḏ](../../strongs/h/h5046.md) it her. Then was the [malkâ](../../strongs/h/h4436.md) [me'od](../../strongs/h/h3966.md) [chuwl](../../strongs/h/h2342.md); and she [shalach](../../strongs/h/h7971.md) [beḡeḏ](../../strongs/h/h899.md) to [labash](../../strongs/h/h3847.md) [Mārdᵊḵay](../../strongs/h/h4782.md), and to [cuwr](../../strongs/h/h5493.md) his [śaq](../../strongs/h/h8242.md) from him: but he [qāḇal](../../strongs/h/h6901.md) it not.

<a name="esther_4_5"></a>Esther 4:5

Then [qara'](../../strongs/h/h7121.md) ['Estēr](../../strongs/h/h635.md) for [Hăṯāḵ](../../strongs/h/h2047.md), one of the [melek](../../strongs/h/h4428.md) [sārîs](../../strongs/h/h5631.md), whom he had ['amad](../../strongs/h/h5975.md) to [paniym](../../strongs/h/h6440.md) upon her, and gave him a [tsavah](../../strongs/h/h6680.md) to [Mārdᵊḵay](../../strongs/h/h4782.md), to [yada'](../../strongs/h/h3045.md) what it was, and why it was.

<a name="esther_4_6"></a>Esther 4:6

So [Hăṯāḵ](../../strongs/h/h2047.md) [yāṣā'](../../strongs/h/h3318.md) to [Mārdᵊḵay](../../strongs/h/h4782.md) unto the [rᵊḥōḇ](../../strongs/h/h7339.md) of the [ʿîr](../../strongs/h/h5892.md), which was [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) [sha'ar](../../strongs/h/h8179.md).

<a name="esther_4_7"></a>Esther 4:7

And [Mārdᵊḵay](../../strongs/h/h4782.md) [nāḡaḏ](../../strongs/h/h5046.md) him of all that had [qārâ](../../strongs/h/h7136.md) unto him, and of the [pārāšâ](../../strongs/h/h6575.md) of the [keceph](../../strongs/h/h3701.md) that [Hāmān](../../strongs/h/h2001.md) had ['āmar](../../strongs/h/h559.md) to [šāqal](../../strongs/h/h8254.md) to the [melek](../../strongs/h/h4428.md) [gᵊnāzîm](../../strongs/h/h1595.md) for the [Yᵊhûḏî](../../strongs/h/h3064.md), to ['abad](../../strongs/h/h6.md) them.

<a name="esther_4_8"></a>Esther 4:8

Also he [nathan](../../strongs/h/h5414.md) him the [paršeḡen](../../strongs/h/h6572.md) of the [kᵊṯāḇ](../../strongs/h/h3791.md) of the [dāṯ](../../strongs/h/h1881.md) that was [nathan](../../strongs/h/h5414.md) at [Šûšan](../../strongs/h/h7800.md) to [šāmaḏ](../../strongs/h/h8045.md) them, to [ra'ah](../../strongs/h/h7200.md) it unto ['Estēr](../../strongs/h/h635.md), and to [nāḡaḏ](../../strongs/h/h5046.md) it unto her, and to [tsavah](../../strongs/h/h6680.md) her that she should [bow'](../../strongs/h/h935.md) unto the [melek](../../strongs/h/h4428.md), to make [chanan](../../strongs/h/h2603.md) unto him, and to make [bāqaš](../../strongs/h/h1245.md) [paniym](../../strongs/h/h6440.md) him for her ['am](../../strongs/h/h5971.md).

<a name="esther_4_9"></a>Esther 4:9

And [Hăṯāḵ](../../strongs/h/h2047.md) [bow'](../../strongs/h/h935.md) and [nāḡaḏ](../../strongs/h/h5046.md) ['Estēr](../../strongs/h/h635.md) the [dabar](../../strongs/h/h1697.md) of [Mārdᵊḵay](../../strongs/h/h4782.md).

<a name="esther_4_10"></a>Esther 4:10

Again ['Estēr](../../strongs/h/h635.md) ['āmar](../../strongs/h/h559.md) unto [Hăṯāḵ](../../strongs/h/h2047.md), and gave him [tsavah](../../strongs/h/h6680.md) unto [Mārdᵊḵay](../../strongs/h/h4782.md);

<a name="esther_4_11"></a>Esther 4:11

All the [melek](../../strongs/h/h4428.md) ['ebed](../../strongs/h/h5650.md), and the ['am](../../strongs/h/h5971.md) of the [melek](../../strongs/h/h4428.md) [mᵊḏînâ](../../strongs/h/h4082.md), do [yada'](../../strongs/h/h3045.md), that whosoever, whether ['iysh](../../strongs/h/h376.md) or ['ishshah](../../strongs/h/h802.md), shall [bow'](../../strongs/h/h935.md) unto the [melek](../../strongs/h/h4428.md) into the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md), who is not [qara'](../../strongs/h/h7121.md), there is one [dāṯ](../../strongs/h/h1881.md) of his to put him to [muwth](../../strongs/h/h4191.md), [baḏ](../../strongs/h/h905.md) such to whom the [melek](../../strongs/h/h4428.md) shall hold [yāšaṭ](../../strongs/h/h3447.md) the [zāhāḇ](../../strongs/h/h2091.md) [šarbîṭ](../../strongs/h/h8275.md), that he may [ḥāyâ](../../strongs/h/h2421.md): but I have not been [qara'](../../strongs/h/h7121.md) to [bow'](../../strongs/h/h935.md) unto the [melek](../../strongs/h/h4428.md) these thirty [yowm](../../strongs/h/h3117.md).

<a name="esther_4_12"></a>Esther 4:12

And they [nāḡaḏ](../../strongs/h/h5046.md) to [Mārdᵊḵay](../../strongs/h/h4782.md) ['Estēr](../../strongs/h/h635.md) [dabar](../../strongs/h/h1697.md).

<a name="esther_4_13"></a>Esther 4:13

Then [Mārdᵊḵay](../../strongs/h/h4782.md) ['āmar](../../strongs/h/h559.md) to [shuwb](../../strongs/h/h7725.md) ['Estēr](../../strongs/h/h635.md), [dāmâ](../../strongs/h/h1819.md) not with [nephesh](../../strongs/h/h5315.md) that thou shalt [mālaṭ](../../strongs/h/h4422.md) in the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), more than all the [Yᵊhûḏî](../../strongs/h/h3064.md).

<a name="esther_4_14"></a>Esther 4:14

For if thou [ḥāraš](../../strongs/h/h2790.md) holdest thy [ḥāraš](../../strongs/h/h2790.md) at this [ʿēṯ](../../strongs/h/h6256.md), then shall there [revaḥ](../../strongs/h/h7305.md) and [haṣṣālâ](../../strongs/h/h2020.md) ['amad](../../strongs/h/h5975.md) to the [Yᵊhûḏî](../../strongs/h/h3064.md) from ['aḥēr](../../strongs/h/h312.md) [maqowm](../../strongs/h/h4725.md); but thou and thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) shall be ['abad](../../strongs/h/h6.md): and who [yada'](../../strongs/h/h3045.md) whether thou art [naga'](../../strongs/h/h5060.md) to the [malkuwth](../../strongs/h/h4438.md) for such a [ʿēṯ](../../strongs/h/h6256.md) as this?

<a name="esther_4_15"></a>Esther 4:15

Then ['Estēr](../../strongs/h/h635.md) ['āmar](../../strongs/h/h559.md) them [shuwb](../../strongs/h/h7725.md) [Mārdᵊḵay](../../strongs/h/h4782.md) this answer,

<a name="esther_4_16"></a>Esther 4:16

[yālaḵ](../../strongs/h/h3212.md), [kānas](../../strongs/h/h3664.md) all the [Yᵊhûḏî](../../strongs/h/h3064.md) that are [māṣā'](../../strongs/h/h4672.md) in [Šûšan](../../strongs/h/h7800.md), and [ṣûm](../../strongs/h/h6684.md) ye for me, and neither ['akal](../../strongs/h/h398.md) nor [šāṯâ](../../strongs/h/h8354.md) three [yowm](../../strongs/h/h3117.md), [layil](../../strongs/h/h3915.md) or [yowm](../../strongs/h/h3117.md): I also and my [naʿărâ](../../strongs/h/h5291.md) will [ṣûm](../../strongs/h/h6684.md) likewise; and so will I [bow'](../../strongs/h/h935.md) in unto the [melek](../../strongs/h/h4428.md), which is not according to the [dāṯ](../../strongs/h/h1881.md): and if I ['abad](../../strongs/h/h6.md), I ['abad](../../strongs/h/h6.md).

<a name="esther_4_17"></a>Esther 4:17

So [Mārdᵊḵay](../../strongs/h/h4782.md) went his ['abar](../../strongs/h/h5674.md), and ['asah](../../strongs/h/h6213.md) according to all that ['Estēr](../../strongs/h/h635.md) had [tsavah](../../strongs/h/h6680.md) him.

---

[Transliteral Bible](../bible.md)

[Esther](esther.md)

[Esther 3](esther_3.md) - [Esther 5](esther_5.md)