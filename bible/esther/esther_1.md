# [Esther 1](https://www.blueletterbible.org/kjv/esther/1)

<a name="esther_1_1"></a>Esther 1:1

Now it came to pass in the [yowm](../../strongs/h/h3117.md) of ['Ăḥašvērôš](../../strongs/h/h325.md), (this is ['Ăḥašvērôš](../../strongs/h/h325.md) which [mālaḵ](../../strongs/h/h4427.md), from [Hōdû](../../strongs/h/h1912.md) even unto [Kûš](../../strongs/h/h3568.md), over an hundred and seven and twenty [mᵊḏînâ](../../strongs/h/h4082.md):)

<a name="esther_1_2"></a>Esther 1:2

That in those [yowm](../../strongs/h/h3117.md), when the [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md) [yashab](../../strongs/h/h3427.md) on the [kicce'](../../strongs/h/h3678.md) of his [malkuwth](../../strongs/h/h4438.md), which was in [Šûšan](../../strongs/h/h7800.md) the [bîrâ](../../strongs/h/h1002.md),

<a name="esther_1_3"></a>Esther 1:3

In the third [šānâ](../../strongs/h/h8141.md) of his [mālaḵ](../../strongs/h/h4427.md), he ['asah](../../strongs/h/h6213.md) a [mištê](../../strongs/h/h4960.md) unto all his [śar](../../strongs/h/h8269.md) and his ['ebed](../../strongs/h/h5650.md); the [ḥayil](../../strongs/h/h2428.md) of [Pāras](../../strongs/h/h6539.md) and [Māḏay](../../strongs/h/h4074.md), the [partᵊmîm](../../strongs/h/h6579.md) and [śar](../../strongs/h/h8269.md) of the [mᵊḏînâ](../../strongs/h/h4082.md), being [paniym](../../strongs/h/h6440.md) him:

<a name="esther_1_4"></a>Esther 1:4

When he [ra'ah](../../strongs/h/h7200.md) the [ʿōšer](../../strongs/h/h6239.md) of his [kabowd](../../strongs/h/h3519.md) [malkuwth](../../strongs/h/h4438.md) and the [yᵊqār](../../strongs/h/h3366.md) of his [tip̄'ārâ](../../strongs/h/h8597.md) [gᵊḏûlâ](../../strongs/h/h1420.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md), even an hundred and fourscore [yowm](../../strongs/h/h3117.md).

<a name="esther_1_5"></a>Esther 1:5

And when these [yowm](../../strongs/h/h3117.md) were [mālā'](../../strongs/h/h4390.md), the [melek](../../strongs/h/h4428.md) ['asah](../../strongs/h/h6213.md) a [mištê](../../strongs/h/h4960.md) unto all the ['am](../../strongs/h/h5971.md) that were [māṣā'](../../strongs/h/h4672.md) in [Šûšan](../../strongs/h/h7800.md) the [bîrâ](../../strongs/h/h1002.md), both unto [gadowl](../../strongs/h/h1419.md) and [qāṭān](../../strongs/h/h6996.md), seven [yowm](../../strongs/h/h3117.md), in the [ḥāṣēr](../../strongs/h/h2691.md) of the [ginnâ](../../strongs/h/h1594.md) of the [melek](../../strongs/h/h4428.md) [bîṯān](../../strongs/h/h1055.md);

<a name="esther_1_6"></a>Esther 1:6

Where were [ḥûr](../../strongs/h/h2353.md), [karpas](../../strongs/h/h3768.md), and [tᵊḵēleṯ](../../strongs/h/h8504.md), ['āḥaz](../../strongs/h/h270.md) with [chebel](../../strongs/h/h2256.md) of fine [bûṣ](../../strongs/h/h948.md) and ['argāmān](../../strongs/h/h713.md) to [keceph](../../strongs/h/h3701.md) [Gālîl](../../strongs/h/h1550.md) and [ʿammûḏ](../../strongs/h/h5982.md) of [šēš](../../strongs/h/h8336.md): the [mittah](../../strongs/h/h4296.md) were of [zāhāḇ](../../strongs/h/h2091.md) and [keceph](../../strongs/h/h3701.md), upon a [ritspah](../../strongs/h/h7531.md) of [bahaṭ](../../strongs/h/h923.md), and [tᵊḵēleṯ](../../strongs/h/h8504.md), and [dar](../../strongs/h/h1858.md), and [sōḥereṯ](../../strongs/h/h5508.md), [šēš](../../strongs/h/h8336.md).

<a name="esther_1_7"></a>Esther 1:7

And they gave them [šāqâ](../../strongs/h/h8248.md) in [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md), (the [kĕliy](../../strongs/h/h3627.md) being [šānâ](../../strongs/h/h8138.md) one from [kĕliy](../../strongs/h/h3627.md),) and [malkuwth](../../strongs/h/h4438.md) [yayin](../../strongs/h/h3196.md) in [rab](../../strongs/h/h7227.md), according to the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md).

<a name="esther_1_8"></a>Esther 1:8

And the [šᵊṯîyâ](../../strongs/h/h8360.md) was according to the [dāṯ](../../strongs/h/h1881.md); none did ['ānas](../../strongs/h/h597.md): for so the [melek](../../strongs/h/h4428.md) had [yacad](../../strongs/h/h3245.md) to all the [rab](../../strongs/h/h7227.md) of his [bayith](../../strongs/h/h1004.md), that they should ['asah](../../strongs/h/h6213.md) according to every ['iysh](../../strongs/h/h376.md) [ratsown](../../strongs/h/h7522.md).

<a name="esther_1_9"></a>Esther 1:9

Also [Vaštî](../../strongs/h/h2060.md) the [malkâ](../../strongs/h/h4436.md) ['asah](../../strongs/h/h6213.md) a [mištê](../../strongs/h/h4960.md) for the ['ishshah](../../strongs/h/h802.md) in the [malkuwth](../../strongs/h/h4438.md) [bayith](../../strongs/h/h1004.md) which belonged to [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md).

<a name="esther_1_10"></a>Esther 1:10

On the seventh [yowm](../../strongs/h/h3117.md), when the [leb](../../strongs/h/h3820.md) of the [melek](../../strongs/h/h4428.md) was [towb](../../strongs/h/h2896.md) with [yayin](../../strongs/h/h3196.md), he ['āmar](../../strongs/h/h559.md) [Mᵊhûmān](../../strongs/h/h4104.md), [Bizṯā'](../../strongs/h/h968.md), [Ḥarḇônā'](../../strongs/h/h2726.md), [Biḡṯā'](../../strongs/h/h903.md), and ['Ăḇaḡṯā'](../../strongs/h/h5.md), [Zēṯar](../../strongs/h/h2242.md), and [Karkas](../../strongs/h/h3752.md), the seven [sārîs](../../strongs/h/h5631.md) that [sharath](../../strongs/h/h8334.md) in the [paniym](../../strongs/h/h6440.md) of ['Ăḥašvērôš](../../strongs/h/h325.md) the [melek](../../strongs/h/h4428.md),

<a name="esther_1_11"></a>Esther 1:11

To [bow'](../../strongs/h/h935.md) [Vaštî](../../strongs/h/h2060.md) the [malkâ](../../strongs/h/h4436.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) with the [keṯer](../../strongs/h/h3804.md) [malkuwth](../../strongs/h/h4438.md), to [ra'ah](../../strongs/h/h7200.md) the ['am](../../strongs/h/h5971.md) and the [śar](../../strongs/h/h8269.md) her [yᵊp̄î](../../strongs/h/h3308.md): for she was [towb](../../strongs/h/h2896.md) to [mar'ê](../../strongs/h/h4758.md).

<a name="esther_1_12"></a>Esther 1:12

But the [malkâ](../../strongs/h/h4436.md) [Vaštî](../../strongs/h/h2060.md) [mā'ēn](../../strongs/h/h3985.md) to [bow'](../../strongs/h/h935.md) at the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) [yad](../../strongs/h/h3027.md) his [sārîs](../../strongs/h/h5631.md): therefore was the [melek](../../strongs/h/h4428.md) [me'od](../../strongs/h/h3966.md) [qāṣap̄](../../strongs/h/h7107.md), and his [chemah](../../strongs/h/h2534.md) [bāʿar](../../strongs/h/h1197.md) in him.

<a name="esther_1_13"></a>Esther 1:13

Then the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) to the [ḥāḵām](../../strongs/h/h2450.md), which [yada'](../../strongs/h/h3045.md) the [ʿēṯ](../../strongs/h/h6256.md), (for so was the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) [paniym](../../strongs/h/h6440.md) all that [yada'](../../strongs/h/h3045.md) [dāṯ](../../strongs/h/h1881.md) and [diyn](../../strongs/h/h1779.md):

<a name="esther_1_14"></a>Esther 1:14

And the [qarowb](../../strongs/h/h7138.md) unto him was [Karšᵊnā'](../../strongs/h/h3771.md), [Šēṯār](../../strongs/h/h8369.md), ['Aḏmāṯā'](../../strongs/h/h133.md), [Taršîš](../../strongs/h/h8659.md), [Meres](../../strongs/h/h4825.md), [Marsᵊnā'](../../strongs/h/h4826.md), and [Mᵊmûḵān](../../strongs/h/h4462.md), the seven [śar](../../strongs/h/h8269.md) of [Pāras](../../strongs/h/h6539.md) and [Māḏay](../../strongs/h/h4074.md), which [ra'ah](../../strongs/h/h7200.md) the [melek](../../strongs/h/h4428.md) [paniym](../../strongs/h/h6440.md), and which [yashab](../../strongs/h/h3427.md) the [ri'šôn](../../strongs/h/h7223.md) in the [malkuwth](../../strongs/h/h4438.md);)

<a name="esther_1_15"></a>Esther 1:15

What shall we ['asah](../../strongs/h/h6213.md) unto the [malkâ](../../strongs/h/h4436.md) [Vaštî](../../strongs/h/h2060.md) according to [dāṯ](../../strongs/h/h1881.md), because she hath not ['asah](../../strongs/h/h6213.md) the [ma'ămār](../../strongs/h/h3982.md) of the [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md) [yad](../../strongs/h/h3027.md) the [sārîs](../../strongs/h/h5631.md)?

<a name="esther_1_16"></a>Esther 1:16

And [Mᵊmûḵān](../../strongs/h/h4462.md) ['āmar](../../strongs/h/h559.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) and the [śar](../../strongs/h/h8269.md), [Vaštî](../../strongs/h/h2060.md) the [malkâ](../../strongs/h/h4436.md) hath not done [ʿāvâ](../../strongs/h/h5753.md) to the [melek](../../strongs/h/h4428.md) only, but also to all the [śar](../../strongs/h/h8269.md), and to all the ['am](../../strongs/h/h5971.md) that are in all the [mᵊḏînâ](../../strongs/h/h4082.md) of the [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md).

<a name="esther_1_17"></a>Esther 1:17

For this [dabar](../../strongs/h/h1697.md) of the [malkâ](../../strongs/h/h4436.md) shall [yāṣā'](../../strongs/h/h3318.md) unto all ['ishshah](../../strongs/h/h802.md), so that they shall [bazah](../../strongs/h/h959.md) their [baʿal](../../strongs/h/h1167.md) in their ['ayin](../../strongs/h/h5869.md), when it shall be ['āmar](../../strongs/h/h559.md), The [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md) ['āmar](../../strongs/h/h559.md) [Vaštî](../../strongs/h/h2060.md) the [malkâ](../../strongs/h/h4436.md) to be [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) him, but she [bow'](../../strongs/h/h935.md) not.

<a name="esther_1_18"></a>Esther 1:18

Likewise shall the [śārâ](../../strongs/h/h8282.md) of [Pāras](../../strongs/h/h6539.md) and [Māḏay](../../strongs/h/h4074.md) ['āmar](../../strongs/h/h559.md) this [yowm](../../strongs/h/h3117.md) unto all the [melek](../../strongs/h/h4428.md) [śar](../../strongs/h/h8269.md), which have [shama'](../../strongs/h/h8085.md) of the [dabar](../../strongs/h/h1697.md) of the [malkâ](../../strongs/h/h4436.md). Thus shall there arise too [day](../../strongs/h/h1767.md) [bizzāyôn](../../strongs/h/h963.md) and [qeṣep̄](../../strongs/h/h7110.md).

<a name="esther_1_19"></a>Esther 1:19

If it [ṭôḇ](../../strongs/h/h2895.md) the [melek](../../strongs/h/h4428.md), let there [yāṣā'](../../strongs/h/h3318.md) a [malkuwth](../../strongs/h/h4438.md) [dabar](../../strongs/h/h1697.md) from [paniym](../../strongs/h/h6440.md), and let it be [kāṯaḇ](../../strongs/h/h3789.md) among the [dāṯ](../../strongs/h/h1881.md) of the [Pāras](../../strongs/h/h6539.md) and the [Māḏay](../../strongs/h/h4074.md), that it be not ['abar](../../strongs/h/h5674.md), That [Vaštî](../../strongs/h/h2060.md) [bow'](../../strongs/h/h935.md) no more [paniym](../../strongs/h/h6440.md) [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md); and let the [melek](../../strongs/h/h4428.md) [nathan](../../strongs/h/h5414.md) her royal [malkuwth](../../strongs/h/h4438.md) unto [rᵊʿûṯ](../../strongs/h/h7468.md) that is [towb](../../strongs/h/h2896.md) than she.

<a name="esther_1_20"></a>Esther 1:20

And when the [melek](../../strongs/h/h4428.md) [piṯgām](../../strongs/h/h6599.md) which he shall ['asah](../../strongs/h/h6213.md) shall be [shama'](../../strongs/h/h8085.md) throughout all his [malkuwth](../../strongs/h/h4438.md), (for it is [rab](../../strongs/h/h7227.md),) all the ['ishshah](../../strongs/h/h802.md) shall [nathan](../../strongs/h/h5414.md) to their [baʿal](../../strongs/h/h1167.md) [yᵊqār](../../strongs/h/h3366.md), both to [gadowl](../../strongs/h/h1419.md) and [qāṭān](../../strongs/h/h6996.md).

<a name="esther_1_21"></a>Esther 1:21

And the [dabar](../../strongs/h/h1697.md) [yatab](../../strongs/h/h3190.md) ['ayin](../../strongs/h/h5869.md) the [melek](../../strongs/h/h4428.md) and the [śar](../../strongs/h/h8269.md); and the [melek](../../strongs/h/h4428.md) ['asah](../../strongs/h/h6213.md) according to the [dabar](../../strongs/h/h1697.md) of [Mᵊmûḵān](../../strongs/h/h4462.md):

<a name="esther_1_22"></a>Esther 1:22

For he [shalach](../../strongs/h/h7971.md) [sēp̄er](../../strongs/h/h5612.md) into all the [melek](../../strongs/h/h4428.md) [mᵊḏînâ](../../strongs/h/h4082.md), into every [mᵊḏînâ](../../strongs/h/h4082.md) according to the [kᵊṯāḇ](../../strongs/h/h3791.md) thereof, and to every ['am](../../strongs/h/h5971.md) after their [lashown](../../strongs/h/h3956.md), that every ['iysh](../../strongs/h/h376.md) should bear [śārar](../../strongs/h/h8323.md) in his own [bayith](../../strongs/h/h1004.md), and that it should be [dabar](../../strongs/h/h1696.md) according to the [lashown](../../strongs/h/h3956.md) of every ['am](../../strongs/h/h5971.md).

---

[Transliteral Bible](../bible.md)

[Esther](esther.md)

[Esther 2](esther_2.md)