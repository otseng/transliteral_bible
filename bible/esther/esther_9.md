# [Esther 9](https://www.blueletterbible.org/kjv/esther/9)

<a name="esther_9_1"></a>Esther 9:1

Now in the twelfth [ḥōḏeš](../../strongs/h/h2320.md), that is, the [ḥōḏeš](../../strongs/h/h2320.md) ['ăḏār](../../strongs/h/h143.md), on the thirteenth [yowm](../../strongs/h/h3117.md) of the same, when the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) and his [dāṯ](../../strongs/h/h1881.md) [naga'](../../strongs/h/h5060.md) to be put in ['asah](../../strongs/h/h6213.md), in the [yowm](../../strongs/h/h3117.md) that the ['oyeb](../../strongs/h/h341.md) of the [Yᵊhûḏî](../../strongs/h/h3064.md) [śāḇar](../../strongs/h/h7663.md) to have [šālaṭ](../../strongs/h/h7980.md) over them, (though it was [hāp̄aḵ](../../strongs/h/h2015.md) to the contrary, that the [Yᵊhûḏî](../../strongs/h/h3064.md) had [šālaṭ](../../strongs/h/h7980.md) over them that [sane'](../../strongs/h/h8130.md) them;)

<a name="esther_9_2"></a>Esther 9:2

The [Yᵊhûḏî](../../strongs/h/h3064.md) [qāhal](../../strongs/h/h6950.md) themselves in their [ʿîr](../../strongs/h/h5892.md) throughout all the [mᵊḏînâ](../../strongs/h/h4082.md) of the [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md), to [shalach](../../strongs/h/h7971.md) [yad](../../strongs/h/h3027.md) on such as [bāqaš](../../strongs/h/h1245.md) their [ra'](../../strongs/h/h7451.md): and no ['iysh](../../strongs/h/h376.md) could ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md); for the [paḥaḏ](../../strongs/h/h6343.md) of them [naphal](../../strongs/h/h5307.md) upon all ['am](../../strongs/h/h5971.md).

<a name="esther_9_3"></a>Esther 9:3

And all the [śar](../../strongs/h/h8269.md) of the [mᵊḏînâ](../../strongs/h/h4082.md), and the ['ăḥašdarpᵊnîm](../../strongs/h/h323.md), and the [peḥâ](../../strongs/h/h6346.md), and ['asah](../../strongs/h/h6213.md) [mĕla'kah](../../strongs/h/h4399.md) of the [melek](../../strongs/h/h4428.md), [nasa'](../../strongs/h/h5375.md) the [Yᵊhûḏî](../../strongs/h/h3064.md); because the [paḥaḏ](../../strongs/h/h6343.md) of [Mārdᵊḵay](../../strongs/h/h4782.md) [naphal](../../strongs/h/h5307.md) upon them.

<a name="esther_9_4"></a>Esther 9:4

For [Mārdᵊḵay](../../strongs/h/h4782.md) was [gadowl](../../strongs/h/h1419.md) in the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and his [šōmaʿ](../../strongs/h/h8089.md) [halak](../../strongs/h/h1980.md) throughout all the [mᵊḏînâ](../../strongs/h/h4082.md): for this ['iysh](../../strongs/h/h376.md) [Mārdᵊḵay](../../strongs/h/h4782.md) [halak](../../strongs/h/h1980.md) [gadowl](../../strongs/h/h1419.md).

<a name="esther_9_5"></a>Esther 9:5

Thus the [Yᵊhûḏî](../../strongs/h/h3064.md) [nakah](../../strongs/h/h5221.md) all their ['oyeb](../../strongs/h/h341.md) with the [makâ](../../strongs/h/h4347.md) of the [chereb](../../strongs/h/h2719.md), and [hereḡ](../../strongs/h/h2027.md), and ['aḇḏān](../../strongs/h/h12.md), and ['asah](../../strongs/h/h6213.md) what they [ratsown](../../strongs/h/h7522.md) unto those that [sane'](../../strongs/h/h8130.md) them.

<a name="esther_9_6"></a>Esther 9:6

And in [Šûšan](../../strongs/h/h7800.md) the [bîrâ](../../strongs/h/h1002.md) the [Yᵊhûḏî](../../strongs/h/h3064.md) [harag](../../strongs/h/h2026.md) and ['abad](../../strongs/h/h6.md) five hundred ['iysh](../../strongs/h/h376.md).

<a name="esther_9_7"></a>Esther 9:7

And [Paršandāṯā'](../../strongs/h/h6577.md), and [Dalp̄Ôn](../../strongs/h/h1813.md), and ['Aspāṯā'](../../strongs/h/h630.md),

<a name="esther_9_8"></a>Esther 9:8

And [Pôrāṯā'](../../strongs/h/h6334.md), and ['Ăḏalyā'](../../strongs/h/h118.md), and ['Ărîḏāṯā'](../../strongs/h/h743.md),

<a name="esther_9_9"></a>Esther 9:9

And [Parmaštā'](../../strongs/h/h6534.md), and ['Ărîsay](../../strongs/h/h747.md), and ['Ărîḏay](../../strongs/h/h742.md), and [Vayzāṯā'](../../strongs/h/h2055.md),

<a name="esther_9_10"></a>Esther 9:10

The ten [ben](../../strongs/h/h1121.md) of [Hāmān](../../strongs/h/h2001.md) the [ben](../../strongs/h/h1121.md) of [Mᵊḏāṯā'](../../strongs/h/h4099.md), the [tsarar](../../strongs/h/h6887.md) of the [Yᵊhûḏî](../../strongs/h/h3064.md), [harag](../../strongs/h/h2026.md) they; but on the [bizzâ](../../strongs/h/h961.md) [shalach](../../strongs/h/h7971.md) they not their [yad](../../strongs/h/h3027.md).

<a name="esther_9_11"></a>Esther 9:11

On that [yowm](../../strongs/h/h3117.md) the [mispār](../../strongs/h/h4557.md) of those that were [harag](../../strongs/h/h2026.md) in [Šûšan](../../strongs/h/h7800.md) the [bîrâ](../../strongs/h/h1002.md) was [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

<a name="esther_9_12"></a>Esther 9:12

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto ['Estēr](../../strongs/h/h635.md) the [malkâ](../../strongs/h/h4436.md), The [Yᵊhûḏî](../../strongs/h/h3064.md) have [harag](../../strongs/h/h2026.md) and ['abad](../../strongs/h/h6.md) five hundred ['iysh](../../strongs/h/h376.md) in [Šûšan](../../strongs/h/h7800.md) the [bîrâ](../../strongs/h/h1002.md), and the ten [ben](../../strongs/h/h1121.md) of [Hāmān](../../strongs/h/h2001.md); what have they ['asah](../../strongs/h/h6213.md) in the [šᵊ'ār](../../strongs/h/h7605.md) of the [melek](../../strongs/h/h4428.md) [mᵊḏînâ](../../strongs/h/h4082.md)? now what is thy [šᵊ'ēlâ](../../strongs/h/h7596.md)? and it shall be [nathan](../../strongs/h/h5414.md) thee: or what is thy [baqqāšâ](../../strongs/h/h1246.md) further? and it shall be ['asah](../../strongs/h/h6213.md).

<a name="esther_9_13"></a>Esther 9:13

Then ['āmar](../../strongs/h/h559.md) ['Estēr](../../strongs/h/h635.md), If it [towb](../../strongs/h/h2896.md) the [melek](../../strongs/h/h4428.md), let it be [nathan](../../strongs/h/h5414.md) to the [Yᵊhûḏî](../../strongs/h/h3064.md) which are in [Šûšan](../../strongs/h/h7800.md) to ['asah](../../strongs/h/h6213.md) [māḥār](../../strongs/h/h4279.md) also according unto this [yowm](../../strongs/h/h3117.md) [dāṯ](../../strongs/h/h1881.md), and let [Hāmān](../../strongs/h/h2001.md) ten [ben](../../strongs/h/h1121.md) be [tālâ](../../strongs/h/h8518.md) upon the ['ets](../../strongs/h/h6086.md).

<a name="esther_9_14"></a>Esther 9:14

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) it so to be ['asah](../../strongs/h/h6213.md): and the [dāṯ](../../strongs/h/h1881.md) was [nathan](../../strongs/h/h5414.md) at [Šûšan](../../strongs/h/h7800.md); and they [tālâ](../../strongs/h/h8518.md) [Hāmān](../../strongs/h/h2001.md) ten [ben](../../strongs/h/h1121.md).

<a name="esther_9_15"></a>Esther 9:15

For the [Yᵊhûḏî](../../strongs/h/h3064.md) that were in [Šûšan](../../strongs/h/h7800.md) gathered themselves [qāhal](../../strongs/h/h6950.md) on the fourteenth [yowm](../../strongs/h/h3117.md) also of the [ḥōḏeš](../../strongs/h/h2320.md) ['ăḏār](../../strongs/h/h143.md), and [harag](../../strongs/h/h2026.md) three hundred ['iysh](../../strongs/h/h376.md) at [Šûšan](../../strongs/h/h7800.md); but on the [bizzâ](../../strongs/h/h961.md) they [shalach](../../strongs/h/h7971.md) not their [yad](../../strongs/h/h3027.md).

<a name="esther_9_16"></a>Esther 9:16

But the [šᵊ'ār](../../strongs/h/h7605.md) [Yᵊhûḏî](../../strongs/h/h3064.md) that were in the [melek](../../strongs/h/h4428.md) [mᵊḏînâ](../../strongs/h/h4082.md) gathered themselves [qāhal](../../strongs/h/h6950.md), and ['amad](../../strongs/h/h5975.md) for their [nephesh](../../strongs/h/h5315.md), and had [nûaḥ](../../strongs/h/h5118.md) from their ['oyeb](../../strongs/h/h341.md), and [harag](../../strongs/h/h2026.md) of their [sane'](../../strongs/h/h8130.md) seventy and five thousand, but they [shalach](../../strongs/h/h7971.md) not their [yad](../../strongs/h/h3027.md) on the [bizzâ](../../strongs/h/h961.md),

<a name="esther_9_17"></a>Esther 9:17

On the thirteenth [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md) ['ăḏār](../../strongs/h/h143.md); and on the fourteenth day of the same [nûaḥ](../../strongs/h/h5118.md) they, and ['asah](../../strongs/h/h6213.md) it a [yowm](../../strongs/h/h3117.md) of [mištê](../../strongs/h/h4960.md) and [simchah](../../strongs/h/h8057.md).

<a name="esther_9_18"></a>Esther 9:18

But the [Yᵊhûḏî](../../strongs/h/h3064.md) that were at [Šûšan](../../strongs/h/h7800.md) [qāhal](../../strongs/h/h6950.md) on the thirteenth day thereof, and on the fourteenth thereof; and on the fifteenth day of the same they [nûaḥ](../../strongs/h/h5118.md), and ['asah](../../strongs/h/h6213.md) it a [yowm](../../strongs/h/h3117.md) of [mištê](../../strongs/h/h4960.md) and [simchah](../../strongs/h/h8057.md).

<a name="esther_9_19"></a>Esther 9:19

Therefore the [Yᵊhûḏî](../../strongs/h/h3064.md) of the [pᵊrāzî](../../strongs/h/h6521.md), that [yashab](../../strongs/h/h3427.md) in the [p̄rvzym](../../strongs/h/h6519.md) [ʿîr](../../strongs/h/h5892.md), ['asah](../../strongs/h/h6213.md) the fourteenth [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md) ['ăḏār](../../strongs/h/h143.md) a day of [simchah](../../strongs/h/h8057.md) and [mištê](../../strongs/h/h4960.md), and a [towb](../../strongs/h/h2896.md) [yowm](../../strongs/h/h3117.md), and of [mišlôaḥ](../../strongs/h/h4916.md) [mānâ](../../strongs/h/h4490.md) ['iysh](../../strongs/h/h376.md) to [rea'](../../strongs/h/h7453.md).

<a name="esther_9_20"></a>Esther 9:20

And [Mārdᵊḵay](../../strongs/h/h4782.md) [kāṯaḇ](../../strongs/h/h3789.md) these [dabar](../../strongs/h/h1697.md), and [shalach](../../strongs/h/h7971.md) [sēp̄er](../../strongs/h/h5612.md) unto all the [Yᵊhûḏî](../../strongs/h/h3064.md) that were in all the [mᵊḏînâ](../../strongs/h/h4082.md) of the [melek](../../strongs/h/h4428.md) ['Ăḥašvērôš](../../strongs/h/h325.md), both [qarowb](../../strongs/h/h7138.md) and [rachowq](../../strongs/h/h7350.md),

<a name="esther_9_21"></a>Esther 9:21

To [quwm](../../strongs/h/h6965.md) this among them, that they should ['asah](../../strongs/h/h6213.md) the fourteenth [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md) ['ăḏār](../../strongs/h/h143.md), and the fifteenth [yowm](../../strongs/h/h3117.md) of the same, [šānâ](../../strongs/h/h8141.md) [šānâ](../../strongs/h/h8141.md),

<a name="esther_9_22"></a>Esther 9:22

As the [yowm](../../strongs/h/h3117.md) wherein the [Yᵊhûḏî](../../strongs/h/h3064.md) [nuwach](../../strongs/h/h5117.md) from their ['oyeb](../../strongs/h/h341.md), and the [ḥōḏeš](../../strongs/h/h2320.md) which was [hāp̄aḵ](../../strongs/h/h2015.md) unto them from [yagown](../../strongs/h/h3015.md) to [simchah](../../strongs/h/h8057.md), and from ['ēḇel](../../strongs/h/h60.md) into a [towb](../../strongs/h/h2896.md) [yowm](../../strongs/h/h3117.md): that they should ['asah](../../strongs/h/h6213.md) them [yowm](../../strongs/h/h3117.md) of [mištê](../../strongs/h/h4960.md) and [simchah](../../strongs/h/h8057.md), and of [mišlôaḥ](../../strongs/h/h4916.md) [mānâ](../../strongs/h/h4490.md) ['iysh](../../strongs/h/h376.md) to [rea'](../../strongs/h/h7453.md), and [matānâ](../../strongs/h/h4979.md) to the ['ebyown](../../strongs/h/h34.md).

<a name="esther_9_23"></a>Esther 9:23

And the [Yᵊhûḏî](../../strongs/h/h3064.md) [qāḇal](../../strongs/h/h6901.md) to ['asah](../../strongs/h/h6213.md) as they had [ḥālal](../../strongs/h/h2490.md), and as [Mārdᵊḵay](../../strongs/h/h4782.md) had [kāṯaḇ](../../strongs/h/h3789.md) unto them;

<a name="esther_9_24"></a>Esther 9:24

Because [Hāmān](../../strongs/h/h2001.md) the [ben](../../strongs/h/h1121.md) of [Mᵊḏāṯā'](../../strongs/h/h4099.md), the ['Ăḡāḡay](../../strongs/h/h91.md), the [tsarar](../../strongs/h/h6887.md) of all the [Yᵊhûḏî](../../strongs/h/h3064.md), had [chashab](../../strongs/h/h2803.md) against the [Yᵊhûḏî](../../strongs/h/h3064.md) to ['abad](../../strongs/h/h6.md) them, and had [naphal](../../strongs/h/h5307.md) [pûr](../../strongs/h/h6332.md), that is, the [gôrāl](../../strongs/h/h1486.md), to [hāmam](../../strongs/h/h2000.md) them, and to ['abad](../../strongs/h/h6.md) them;

<a name="esther_9_25"></a>Esther 9:25

But when [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md), he ['āmar](../../strongs/h/h559.md) by [sēp̄er](../../strongs/h/h5612.md) that his [ra'](../../strongs/h/h7451.md) [maḥăšāḇâ](../../strongs/h/h4284.md), which he [chashab](../../strongs/h/h2803.md) against the [Yᵊhûḏî](../../strongs/h/h3064.md), should [shuwb](../../strongs/h/h7725.md) upon his own [ro'sh](../../strongs/h/h7218.md), and that he and his [ben](../../strongs/h/h1121.md) should be [tālâ](../../strongs/h/h8518.md) on the ['ets](../../strongs/h/h6086.md).

<a name="esther_9_26"></a>Esther 9:26

Wherefore they [qara'](../../strongs/h/h7121.md) these [yowm](../../strongs/h/h3117.md) [pûr](../../strongs/h/h6332.md) after the [shem](../../strongs/h/h8034.md) of [pûr](../../strongs/h/h6332.md). Therefore for all the [dabar](../../strongs/h/h1697.md) of this ['agereṯ](../../strongs/h/h107.md), and of that which they had [ra'ah](../../strongs/h/h7200.md) concerning this matter, and which had [naga'](../../strongs/h/h5060.md) unto them,

<a name="esther_9_27"></a>Esther 9:27

The [Yᵊhûḏî](../../strongs/h/h3064.md) [quwm](../../strongs/h/h6965.md), and [qāḇal](../../strongs/h/h6901.md) upon them, and upon their [zera'](../../strongs/h/h2233.md), and upon all such as [lāvâ](../../strongs/h/h3867.md) themselves unto them, so as it should not ['abar](../../strongs/h/h5674.md), that they would ['asah](../../strongs/h/h6213.md) these two [yowm](../../strongs/h/h3117.md) according to their [kᵊṯāḇ](../../strongs/h/h3791.md), and according to their [zᵊmān](../../strongs/h/h2165.md) every [šānâ](../../strongs/h/h8141.md) [šānâ](../../strongs/h/h8141.md);

<a name="esther_9_28"></a>Esther 9:28

And that these [yowm](../../strongs/h/h3117.md) should be [zakar](../../strongs/h/h2142.md) and ['asah](../../strongs/h/h6213.md) [dôr](../../strongs/h/h1755.md) every [dôr](../../strongs/h/h1755.md), every [mišpāḥâ](../../strongs/h/h4940.md), every [mᵊḏînâ](../../strongs/h/h4082.md), and every [ʿîr](../../strongs/h/h5892.md); and that these [yowm](../../strongs/h/h3117.md) of [pûr](../../strongs/h/h6332.md) should not ['abar](../../strongs/h/h5674.md) from [tavek](../../strongs/h/h8432.md) the [Yᵊhûḏî](../../strongs/h/h3064.md), nor the [zeker](../../strongs/h/h2143.md) of them [sûp̄](../../strongs/h/h5486.md) from their [zera'](../../strongs/h/h2233.md).

<a name="esther_9_29"></a>Esther 9:29

Then ['Estēr](../../strongs/h/h635.md) the [malkâ](../../strongs/h/h4436.md), the [bath](../../strongs/h/h1323.md) of ['Ăḇîhayil](../../strongs/h/h32.md), and [Mārdᵊḵay](../../strongs/h/h4782.md) the [Yᵊhûḏî](../../strongs/h/h3064.md), [kāṯaḇ](../../strongs/h/h3789.md) with all [tōqep̄](../../strongs/h/h8633.md), to [quwm](../../strongs/h/h6965.md) this second ['agereṯ](../../strongs/h/h107.md) of [pûr](../../strongs/h/h6332.md).

<a name="esther_9_30"></a>Esther 9:30

And he [shalach](../../strongs/h/h7971.md) the [sēp̄er](../../strongs/h/h5612.md) unto all the [Yᵊhûḏî](../../strongs/h/h3064.md), to the hundred twenty and seven [mᵊḏînâ](../../strongs/h/h4082.md) of the [malkuwth](../../strongs/h/h4438.md) of ['Ăḥašvērôš](../../strongs/h/h325.md), with [dabar](../../strongs/h/h1697.md) of [shalowm](../../strongs/h/h7965.md) and ['emeth](../../strongs/h/h571.md),

<a name="esther_9_31"></a>Esther 9:31

To [quwm](../../strongs/h/h6965.md) these [yowm](../../strongs/h/h3117.md) of [pûr](../../strongs/h/h6332.md) in their [zᵊmān](../../strongs/h/h2165.md) appointed, according as [Mārdᵊḵay](../../strongs/h/h4782.md) the [Yᵊhûḏî](../../strongs/h/h3064.md) and ['Estēr](../../strongs/h/h635.md) the [malkâ](../../strongs/h/h4436.md) had [quwm](../../strongs/h/h6965.md) them, and as they had [quwm](../../strongs/h/h6965.md) for [nephesh](../../strongs/h/h5315.md) and for their [zera'](../../strongs/h/h2233.md), the [dabar](../../strongs/h/h1697.md) of the [ṣôm](../../strongs/h/h6685.md) and their [zaʿaq](../../strongs/h/h2201.md).

<a name="esther_9_32"></a>Esther 9:32

And the [ma'ămār](../../strongs/h/h3982.md) of ['Estēr](../../strongs/h/h635.md) [quwm](../../strongs/h/h6965.md) these [dabar](../../strongs/h/h1697.md) of [pûr](../../strongs/h/h6332.md); and it was [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md).

---

[Transliteral Bible](../bible.md)

[Esther](esther.md)

[Esther 8](esther_8.md) - [Esther 10](esther_10.md)