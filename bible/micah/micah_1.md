# [Micah 1](https://www.blueletterbible.org/kjv/micah/1)

<a name="micah_1_1"></a>Micah 1:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) that came to [Mîḵâ](../../strongs/h/h4318.md) the [môraštî](../../strongs/h/h4183.md) in the [yowm](../../strongs/h/h3117.md) of [Yôṯām](../../strongs/h/h3147.md), ['Āḥāz](../../strongs/h/h271.md), and [Yᵊḥizqîyâ](../../strongs/h/h3169.md), [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), which he [chazah](../../strongs/h/h2372.md) concerning [Šōmrôn](../../strongs/h/h8111.md) and [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="micah_1_2"></a>Micah 1:2

[shama'](../../strongs/h/h8085.md), all ye ['am](../../strongs/h/h5971.md); [qashab](../../strongs/h/h7181.md), O ['erets](../../strongs/h/h776.md), and all that [mᵊlō'](../../strongs/h/h4393.md) is: and let the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) be ['ed](../../strongs/h/h5707.md) against you, the ['adonay](../../strongs/h/h136.md) from his [qodesh](../../strongs/h/h6944.md) [heykal](../../strongs/h/h1964.md).

<a name="micah_1_3"></a>Micah 1:3

For, behold, [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) out of his [maqowm](../../strongs/h/h4725.md), and will [yarad](../../strongs/h/h3381.md), and [dāraḵ](../../strongs/h/h1869.md) upon the [bāmâ](../../strongs/h/h1116.md) of the ['erets](../../strongs/h/h776.md).

<a name="micah_1_4"></a>Micah 1:4

And the [har](../../strongs/h/h2022.md) shall be [māsas](../../strongs/h/h4549.md) under him, and the [ʿēmeq](../../strongs/h/h6010.md) shall be [bāqaʿ](../../strongs/h/h1234.md), as [dônāḡ](../../strongs/h/h1749.md) [paniym](../../strongs/h/h6440.md) the ['esh](../../strongs/h/h784.md), and as the [mayim](../../strongs/h/h4325.md) that are [nāḡar](../../strongs/h/h5064.md) down a [môrāḏ](../../strongs/h/h4174.md).

<a name="micah_1_5"></a>Micah 1:5

For the [pesha'](../../strongs/h/h6588.md) of [Ya'aqob](../../strongs/h/h3290.md) is all this, and for the [chatta'ath](../../strongs/h/h2403.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md). What is the [pesha'](../../strongs/h/h6588.md) of [Ya'aqob](../../strongs/h/h3290.md)? is it not [Šōmrôn](../../strongs/h/h8111.md)? and what are the [bāmâ](../../strongs/h/h1116.md) of [Yehuwdah](../../strongs/h/h3063.md)? are they not [Yĕruwshalaim](../../strongs/h/h3389.md)?

<a name="micah_1_6"></a>Micah 1:6

Therefore I will [śûm](../../strongs/h/h7760.md) [Šōmrôn](../../strongs/h/h8111.md) as an [ʿî](../../strongs/h/h5856.md) of the [sadeh](../../strongs/h/h7704.md), and as [maṭṭāʿ](../../strongs/h/h4302.md) of a [kerem](../../strongs/h/h3754.md): and I will [nāḡar](../../strongs/h/h5064.md) the ['eben](../../strongs/h/h68.md) thereof into the [gay'](../../strongs/h/h1516.md), and I will [gālâ](../../strongs/h/h1540.md) the [yᵊsôḏ](../../strongs/h/h3247.md) thereof.

<a name="micah_1_7"></a>Micah 1:7

And all the [pāsîl](../../strongs/h/h6456.md) thereof shall be [kāṯaṯ](../../strongs/h/h3807.md), and all the ['eṯnan](../../strongs/h/h868.md) thereof shall be [śārap̄](../../strongs/h/h8313.md) with the ['esh](../../strongs/h/h784.md), and all the [ʿāṣāḇ](../../strongs/h/h6091.md) thereof will I [śûm](../../strongs/h/h7760.md) [šᵊmāmâ](../../strongs/h/h8077.md): for she [qāḇaṣ](../../strongs/h/h6908.md) it of the ['eṯnan](../../strongs/h/h868.md) of a [zānâ](../../strongs/h/h2181.md), and they shall [shuwb](../../strongs/h/h7725.md) to the ['eṯnan](../../strongs/h/h868.md) of a [zānâ](../../strongs/h/h2181.md).

<a name="micah_1_8"></a>Micah 1:8

Therefore I will [sāp̄aḏ](../../strongs/h/h5594.md) and [yālal](../../strongs/h/h3213.md), I will [yālaḵ](../../strongs/h/h3212.md) [šôlāl](../../strongs/h/h7758.md) [šôlāl](../../strongs/h/h7758.md) and ['arowm](../../strongs/h/h6174.md): I will ['asah](../../strongs/h/h6213.md) a [mispēḏ](../../strongs/h/h4553.md) like the [tannîn](../../strongs/h/h8577.md), and ['ēḇel](../../strongs/h/h60.md) as the [bath](../../strongs/h/h1323.md) [yaʿănâ](../../strongs/h/h3284.md).

<a name="micah_1_9"></a>Micah 1:9

For her [makâ](../../strongs/h/h4347.md) is ['anash](../../strongs/h/h605.md); for it is [bow'](../../strongs/h/h935.md) unto [Yehuwdah](../../strongs/h/h3063.md); he is [naga'](../../strongs/h/h5060.md) unto the [sha'ar](../../strongs/h/h8179.md) of my ['am](../../strongs/h/h5971.md), even to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="micah_1_10"></a>Micah 1:10

[nāḡaḏ](../../strongs/h/h5046.md) ye it not at [Gaṯ](../../strongs/h/h1661.md), [bāḵâ](../../strongs/h/h1058.md) ye not at [bāḵâ](../../strongs/h/h1058.md): in the house of [Bêṯ LᵊʿAp̄Râ](../../strongs/h/h1036.md) [pālaš](../../strongs/h/h6428.md) [pālaš](../../strongs/h/h6428.md) thyself in the ['aphar](../../strongs/h/h6083.md).

<a name="micah_1_11"></a>Micah 1:11

Pass ye ['abar](../../strongs/h/h5674.md), thou [yashab](../../strongs/h/h3427.md) of [Šāp̄Îr](../../strongs/h/h8208.md), having thy [bšeṯ](../../strongs/h/h1322.md) [ʿeryâ](../../strongs/h/h6181.md): the [yashab](../../strongs/h/h3427.md) of [Ṣa'Ănān](../../strongs/h/h6630.md) came not [yāṣā'](../../strongs/h/h3318.md) in the [mispēḏ](../../strongs/h/h4553.md) of [Bêṯ Hā'Ēṣel](../../strongs/h/h1018.md); he shall [laqach](../../strongs/h/h3947.md) of you his [ʿemdâ](../../strongs/h/h5979.md).

<a name="micah_1_12"></a>Micah 1:12

For the [yashab](../../strongs/h/h3427.md) of [mārôṯ](../../strongs/h/h4796.md) waited [chuwl](../../strongs/h/h2342.md) for [towb](../../strongs/h/h2896.md): but [ra'](../../strongs/h/h7451.md) [yarad](../../strongs/h/h3381.md) from [Yĕhovah](../../strongs/h/h3068.md) unto the [sha'ar](../../strongs/h/h8179.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="micah_1_13"></a>Micah 1:13

O thou [yashab](../../strongs/h/h3427.md) of [Lāḵîš](../../strongs/h/h3923.md), [rāṯam](../../strongs/h/h7573.md) the [merkāḇâ](../../strongs/h/h4818.md) to the [reḵeš](../../strongs/h/h7409.md): she is the [re'shiyth](../../strongs/h/h7225.md) of the [chatta'ath](../../strongs/h/h2403.md) to the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md): for the [pesha'](../../strongs/h/h6588.md) of [Yisra'el](../../strongs/h/h3478.md) were [māṣā'](../../strongs/h/h4672.md) in thee.

<a name="micah_1_14"></a>Micah 1:14

Therefore shalt thou [nathan](../../strongs/h/h5414.md) [šillûḥîm](../../strongs/h/h7964.md) to [Môrešeṯ Gaṯ](../../strongs/h/h4182.md): the [bayith](../../strongs/h/h1004.md) of ['Aḵzîḇ](../../strongs/h/h392.md) shall be an ['aḵzāḇ](../../strongs/h/h391.md) to the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="micah_1_15"></a>Micah 1:15

Yet will I [bow'](../../strongs/h/h935.md) a [yarash](../../strongs/h/h3423.md) unto thee, O [yashab](../../strongs/h/h3427.md) of [Mārē'Šâ](../../strongs/h/h4762.md): he shall [bow'](../../strongs/h/h935.md) unto [ʿĂḏullām](../../strongs/h/h5725.md) the [kabowd](../../strongs/h/h3519.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="micah_1_16"></a>Micah 1:16

Make thee [qāraḥ](../../strongs/h/h7139.md), and [gāzaz](../../strongs/h/h1494.md) thee for thy [taʿănûḡ](../../strongs/h/h8588.md) [ben](../../strongs/h/h1121.md); [rāḥaḇ](../../strongs/h/h7337.md) thy [qrḥ](../../strongs/h/h7144.md) as the [nesheׁr](../../strongs/h/h5404.md); for they are gone into [gālâ](../../strongs/h/h1540.md) from thee.

---

[Transliteral Bible](../bible.md)

[Micah](micah.md)

[Micah 2](micah_2.md)