# [Micah 7](https://www.blueletterbible.org/kjv/micah/7)

<a name="micah_7_1"></a>Micah 7:1

['allay](../../strongs/h/h480.md) is me! for I am as when they have ['ōsep̄](../../strongs/h/h625.md) the [qayiṣ](../../strongs/h/h7019.md), as the [ʿōlēlôṯ](../../strongs/h/h5955.md) of the [bāṣîr](../../strongs/h/h1210.md): there is no ['eškōôl](../../strongs/h/h811.md) to ['akal](../../strongs/h/h398.md): my [nephesh](../../strongs/h/h5315.md) ['āvâ](../../strongs/h/h183.md) the [bikûrâ](../../strongs/h/h1063.md).

<a name="micah_7_2"></a>Micah 7:2

The [chaciyd](../../strongs/h/h2623.md) is ['abad](../../strongs/h/h6.md) out of the ['erets](../../strongs/h/h776.md): and there is none [yashar](../../strongs/h/h3477.md) among ['āḏām](../../strongs/h/h120.md): they all ['arab](../../strongs/h/h693.md) for [dam](../../strongs/h/h1818.md); they [ṣûḏ](../../strongs/h/h6679.md) every ['iysh](../../strongs/h/h376.md) his ['ach](../../strongs/h/h251.md) with a [ḥērem](../../strongs/h/h2764.md).

<a name="micah_7_3"></a>Micah 7:3

That they may do [ra'](../../strongs/h/h7451.md) with both [kaph](../../strongs/h/h3709.md) [yatab](../../strongs/h/h3190.md), the [śar](../../strongs/h/h8269.md) [sha'al](../../strongs/h/h7592.md), and the [shaphat](../../strongs/h/h8199.md) asketh for a [šillûm](../../strongs/h/h7966.md); and the [gadowl](../../strongs/h/h1419.md), he [dabar](../../strongs/h/h1696.md) his [havvah](../../strongs/h/h1942.md) [nephesh](../../strongs/h/h5315.md): so they [ʿāḇaṯ](../../strongs/h/h5686.md) it.

<a name="micah_7_4"></a>Micah 7:4

The [towb](../../strongs/h/h2896.md) of them is as a [ḥēḏeq](../../strongs/h/h2312.md): the [yashar](../../strongs/h/h3477.md) than a [mᵊsûḵâ](../../strongs/h/h4534.md): the [yowm](../../strongs/h/h3117.md) of thy [tsaphah](../../strongs/h/h6822.md) and thy [pᵊqudâ](../../strongs/h/h6486.md) [bow'](../../strongs/h/h935.md); now shall be their [mᵊḇûḵâ](../../strongs/h/h3998.md).

<a name="micah_7_5"></a>Micah 7:5

['aman](../../strongs/h/h539.md) ye not in a [rea'](../../strongs/h/h7453.md), put ye not [batach](../../strongs/h/h982.md) in an ['allûp̄](../../strongs/h/h441.md): [shamar](../../strongs/h/h8104.md) the [peṯaḥ](../../strongs/h/h6607.md) of thy [peh](../../strongs/h/h6310.md) from her that [shakab](../../strongs/h/h7901.md) in thy [ḥêq](../../strongs/h/h2436.md).

<a name="micah_7_6"></a>Micah 7:6

For the [ben](../../strongs/h/h1121.md) [nabel](../../strongs/h/h5034.md) the ['ab](../../strongs/h/h1.md), the [bath](../../strongs/h/h1323.md) [quwm](../../strongs/h/h6965.md) against her ['em](../../strongs/h/h517.md), the [kallâ](../../strongs/h/h3618.md) against her [ḥāmôṯ](../../strongs/h/h2545.md); an ['iysh](../../strongs/h/h376.md) ['oyeb](../../strongs/h/h341.md) are the ['enowsh](../../strongs/h/h582.md) of his own [bayith](../../strongs/h/h1004.md).

<a name="micah_7_7"></a>Micah 7:7

Therefore I will [tsaphah](../../strongs/h/h6822.md) unto [Yĕhovah](../../strongs/h/h3068.md); I will [yāḥal](../../strongs/h/h3176.md) for the ['Elohiym](../../strongs/h/h430.md) of my [yesha'](../../strongs/h/h3468.md): my ['Elohiym](../../strongs/h/h430.md) will [shama'](../../strongs/h/h8085.md) me.

<a name="micah_7_8"></a>Micah 7:8

[samach](../../strongs/h/h8055.md) not against me, O mine ['oyeb](../../strongs/h/h341.md): when I [naphal](../../strongs/h/h5307.md), I shall [quwm](../../strongs/h/h6965.md); when I [yashab](../../strongs/h/h3427.md) in [choshek](../../strongs/h/h2822.md), [Yĕhovah](../../strongs/h/h3068.md) shall be a ['owr](../../strongs/h/h216.md) unto me.

<a name="micah_7_9"></a>Micah 7:9

I will [nasa'](../../strongs/h/h5375.md) the [zaʿap̄](../../strongs/h/h2197.md) of [Yĕhovah](../../strongs/h/h3068.md), because I have [chata'](../../strongs/h/h2398.md) against him, until he [riyb](../../strongs/h/h7378.md) my [rîḇ](../../strongs/h/h7379.md), and ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) for me: he will bring me [yāṣā'](../../strongs/h/h3318.md) to the ['owr](../../strongs/h/h216.md), and I shall [ra'ah](../../strongs/h/h7200.md) his [tsedaqah](../../strongs/h/h6666.md).

<a name="micah_7_10"></a>Micah 7:10

Then she that is mine ['oyeb](../../strongs/h/h341.md) shall [ra'ah](../../strongs/h/h7200.md) it, and [bûšâ](../../strongs/h/h955.md) shall [kāsâ](../../strongs/h/h3680.md) her which ['āmar](../../strongs/h/h559.md) unto me, Where is [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md)? mine ['ayin](../../strongs/h/h5869.md) shall [ra'ah](../../strongs/h/h7200.md) her: now shall she be trodden [mirmās](../../strongs/h/h4823.md) as the [ṭîṭ](../../strongs/h/h2916.md) of the [ḥûṣ](../../strongs/h/h2351.md).

<a name="micah_7_11"></a>Micah 7:11

In the [yowm](../../strongs/h/h3117.md) that thy [gāḏēr](../../strongs/h/h1447.md) are to be [bānâ](../../strongs/h/h1129.md), in that [yowm](../../strongs/h/h3117.md) shall the [choq](../../strongs/h/h2706.md) be far [rachaq](../../strongs/h/h7368.md).

<a name="micah_7_12"></a>Micah 7:12

In that [yowm](../../strongs/h/h3117.md) also he shall [bow'](../../strongs/h/h935.md) even to thee from ['Aššûr](../../strongs/h/h804.md), and from the [māṣôr](../../strongs/h/h4693.md) [ʿîr](../../strongs/h/h5892.md), and from the [māṣôr](../../strongs/h/h4693.md) even to the [nāhār](../../strongs/h/h5104.md), and from [yam](../../strongs/h/h3220.md) to [yam](../../strongs/h/h3220.md), and from [har](../../strongs/h/h2022.md) to [har](../../strongs/h/h2022.md).

<a name="micah_7_13"></a>Micah 7:13

Notwithstanding the ['erets](../../strongs/h/h776.md) shall be [šᵊmāmâ](../../strongs/h/h8077.md) because of them that [yashab](../../strongs/h/h3427.md) therein, for the [pĕriy](../../strongs/h/h6529.md) of their [maʿălāl](../../strongs/h/h4611.md).

<a name="micah_7_14"></a>Micah 7:14

[ra'ah](../../strongs/h/h7462.md) thy ['am](../../strongs/h/h5971.md) with thy [shebet](../../strongs/h/h7626.md), the [tso'n](../../strongs/h/h6629.md) of thine [nachalah](../../strongs/h/h5159.md), which [shakan](../../strongs/h/h7931.md) [bāḏāḏ](../../strongs/h/h910.md) in the [yaʿar](../../strongs/h/h3293.md), in the [tavek](../../strongs/h/h8432.md) of [Karmel](../../strongs/h/h3760.md): let them [ra'ah](../../strongs/h/h7462.md) in [Bāšān](../../strongs/h/h1316.md) and [Gilʿāḏ](../../strongs/h/h1568.md), as in the [yowm](../../strongs/h/h3117.md) of ['owlam](../../strongs/h/h5769.md).

<a name="micah_7_15"></a>Micah 7:15

According to the [yowm](../../strongs/h/h3117.md) of thy [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) will I [ra'ah](../../strongs/h/h7200.md) unto him [pala'](../../strongs/h/h6381.md).

<a name="micah_7_16"></a>Micah 7:16

The [gowy](../../strongs/h/h1471.md) shall [ra'ah](../../strongs/h/h7200.md) and be [buwsh](../../strongs/h/h954.md) at all their [gᵊḇûrâ](../../strongs/h/h1369.md): they shall [śûm](../../strongs/h/h7760.md) their [yad](../../strongs/h/h3027.md) upon their [peh](../../strongs/h/h6310.md), their ['ozen](../../strongs/h/h241.md) shall be [ḥāraš](../../strongs/h/h2790.md).

<a name="micah_7_17"></a>Micah 7:17

They shall [lāḥaḵ](../../strongs/h/h3897.md) the ['aphar](../../strongs/h/h6083.md) like a [nachash](../../strongs/h/h5175.md), they shall [ragaz](../../strongs/h/h7264.md) out of their [misgereṯ](../../strongs/h/h4526.md) like [zāḥal](../../strongs/h/h2119.md) of the ['erets](../../strongs/h/h776.md): they shall be [pachad](../../strongs/h/h6342.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), and shall [yare'](../../strongs/h/h3372.md) because of thee.

<a name="micah_7_18"></a>Micah 7:18

Who is an ['el](../../strongs/h/h410.md) like unto thee, that [nasa'](../../strongs/h/h5375.md) ['avon](../../strongs/h/h5771.md), and ['abar](../../strongs/h/h5674.md) the [pesha'](../../strongs/h/h6588.md) of the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of his [nachalah](../../strongs/h/h5159.md)? he [ḥāzaq](../../strongs/h/h2388.md) not his ['aph](../../strongs/h/h639.md) for [ʿaḏ](../../strongs/h/h5703.md), because he [ḥāp̄ēṣ](../../strongs/h/h2654.md) in [checed](../../strongs/h/h2617.md).

<a name="micah_7_19"></a>Micah 7:19

He will [shuwb](../../strongs/h/h7725.md), he will have [racham](../../strongs/h/h7355.md) upon us; he will [kāḇaš](../../strongs/h/h3533.md) our ['avon](../../strongs/h/h5771.md); and thou wilt [shalak](../../strongs/h/h7993.md) all their [chatta'ath](../../strongs/h/h2403.md) into the [mᵊṣôlâ](../../strongs/h/h4688.md) of the [yam](../../strongs/h/h3220.md).

<a name="micah_7_20"></a>Micah 7:20

Thou wilt [nathan](../../strongs/h/h5414.md) the ['emeth](../../strongs/h/h571.md) to [Ya'aqob](../../strongs/h/h3290.md), and the [checed](../../strongs/h/h2617.md) to ['Abraham](../../strongs/h/h85.md), which thou hast [shaba'](../../strongs/h/h7650.md) unto our ['ab](../../strongs/h/h1.md) from the [yowm](../../strongs/h/h3117.md) of [qeḏem](../../strongs/h/h6924.md).

---

[Transliteral Bible](../bible.md)

[Micah](micah.md)

[Micah 6](micah_6.md)