# [Micah 2](https://www.blueletterbible.org/kjv/micah/2)

<a name="micah_2_1"></a>Micah 2:1

[hôy](../../strongs/h/h1945.md) to them that [chashab](../../strongs/h/h2803.md) ['aven](../../strongs/h/h205.md), and [pa'al](../../strongs/h/h6466.md) [ra'](../../strongs/h/h7451.md) upon their [miškāḇ](../../strongs/h/h4904.md)! when the [boqer](../../strongs/h/h1242.md) is ['owr](../../strongs/h/h216.md), they ['asah](../../strongs/h/h6213.md) it, because it is in the ['el](../../strongs/h/h410.md) of their [yad](../../strongs/h/h3027.md).

<a name="micah_2_2"></a>Micah 2:2

And they [chamad](../../strongs/h/h2530.md) [sadeh](../../strongs/h/h7704.md), and take them by [gāzal](../../strongs/h/h1497.md); and [bayith](../../strongs/h/h1004.md), and take them [nasa'](../../strongs/h/h5375.md): so they [ʿāšaq](../../strongs/h/h6231.md) a [geḇer](../../strongs/h/h1397.md) and his [bayith](../../strongs/h/h1004.md), even an ['iysh](../../strongs/h/h376.md) and his [nachalah](../../strongs/h/h5159.md).

<a name="micah_2_3"></a>Micah 2:3

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, against this [mišpāḥâ](../../strongs/h/h4940.md) do I [chashab](../../strongs/h/h2803.md) a [ra'](../../strongs/h/h7451.md), from which ye shall not [mûš](../../strongs/h/h4185.md) your [ṣaûā'r](../../strongs/h/h6677.md); neither shall ye [yālaḵ](../../strongs/h/h3212.md) [rômâ](../../strongs/h/h7317.md): for this [ʿēṯ](../../strongs/h/h6256.md) is [ra'](../../strongs/h/h7451.md).

<a name="micah_2_4"></a>Micah 2:4

In that [yowm](../../strongs/h/h3117.md) shall one [nasa'](../../strongs/h/h5375.md) a [māšāl](../../strongs/h/h4912.md) against you, and [nāhâ](../../strongs/h/h5091.md) with a [nihyhā](../../strongs/h/h5093.md) [nᵊhî](../../strongs/h/h5092.md), and ['āmar](../../strongs/h/h559.md), We be [shadad](../../strongs/h/h7703.md) [shadad](../../strongs/h/h7703.md): he hath [mûr](../../strongs/h/h4171.md) the [cheleq](../../strongs/h/h2506.md) of my ['am](../../strongs/h/h5971.md): how hath he [mûš](../../strongs/h/h4185.md) it from me! [shuwb](../../strongs/h/h7725.md) he hath [chalaq](../../strongs/h/h2505.md) our [sadeh](../../strongs/h/h7704.md).

<a name="micah_2_5"></a>Micah 2:5

Therefore thou shalt have none that shall [shalak](../../strongs/h/h7993.md) a [chebel](../../strongs/h/h2256.md) by [gôrāl](../../strongs/h/h1486.md) in the [qāhēl](../../strongs/h/h6951.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="micah_2_6"></a>Micah 2:6

[nāṭap̄](../../strongs/h/h5197.md) ye not, say they to them that [nāṭap̄](../../strongs/h/h5197.md): they shall not [nāṭap̄](../../strongs/h/h5197.md) to them, that they shall not [nāsaḡ](../../strongs/h/h5253.md) [kĕlimmah](../../strongs/h/h3639.md).

<a name="micah_2_7"></a>Micah 2:7

O thou that art ['āmar](../../strongs/h/h559.md) the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), is the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) [qāṣar](../../strongs/h/h7114.md)? are these his [maʿălāl](../../strongs/h/h4611.md)? do not my [dabar](../../strongs/h/h1697.md) do [yatab](../../strongs/h/h3190.md) to him that [halak](../../strongs/h/h1980.md) [yashar](../../strongs/h/h3477.md)?

<a name="micah_2_8"></a>Micah 2:8

Even of ['eṯmôl](../../strongs/h/h865.md) my ['am](../../strongs/h/h5971.md) is [quwm](../../strongs/h/h6965.md) as an ['oyeb](../../strongs/h/h341.md): ye pull [pāšaṭ](../../strongs/h/h6584.md) the ['eḏer](../../strongs/h/h145.md) [môl](../../strongs/h/h4136.md) the [śalmâ](../../strongs/h/h8008.md) from them that ['abar](../../strongs/h/h5674.md) by [betach](../../strongs/h/h983.md) as men [shuwb](../../strongs/h/h7725.md) from [milḥāmâ](../../strongs/h/h4421.md).

<a name="micah_2_9"></a>Micah 2:9

The ['ishshah](../../strongs/h/h802.md) of my ['am](../../strongs/h/h5971.md) have ye [gāraš](../../strongs/h/h1644.md) from their [taʿănûḡ](../../strongs/h/h8588.md) [bayith](../../strongs/h/h1004.md); from their ['owlel](../../strongs/h/h5768.md) have ye [laqach](../../strongs/h/h3947.md) my [hadar](../../strongs/h/h1926.md) ['owlam](../../strongs/h/h5769.md).

<a name="micah_2_10"></a>Micah 2:10

[quwm](../../strongs/h/h6965.md) ye, and [yālaḵ](../../strongs/h/h3212.md); for this is not your [mᵊnûḥâ](../../strongs/h/h4496.md): because it is [ṭāmē'](../../strongs/h/h2930.md), it shall [chabal](../../strongs/h/h2254.md) you, even with a [māraṣ](../../strongs/h/h4834.md) [chebel](../../strongs/h/h2256.md).

<a name="micah_2_11"></a>Micah 2:11

If an ['iysh](../../strongs/h/h376.md) [halak](../../strongs/h/h1980.md) in the [ruwach](../../strongs/h/h7307.md) and [sheqer](../../strongs/h/h8267.md) do [kāzaḇ](../../strongs/h/h3576.md), saying, I will [nāṭap̄](../../strongs/h/h5197.md) unto thee of [yayin](../../strongs/h/h3196.md) and of [šēḵār](../../strongs/h/h7941.md); he shall even be the [nāṭap̄](../../strongs/h/h5197.md) of this ['am](../../strongs/h/h5971.md).

<a name="micah_2_12"></a>Micah 2:12

I will ['āsap̄](../../strongs/h/h622.md) ['āsap̄](../../strongs/h/h622.md), O [Ya'aqob](../../strongs/h/h3290.md), all of thee; I will [qāḇaṣ](../../strongs/h/h6908.md) [qāḇaṣ](../../strongs/h/h6908.md) the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yisra'el](../../strongs/h/h3478.md); I will [śûm](../../strongs/h/h7760.md) them [yaḥaḏ](../../strongs/h/h3162.md) as the [tso'n](../../strongs/h/h6629.md) of [Bāṣrâ](../../strongs/h/h1223.md) [Bāṣrâ](../../strongs/h/h1224.md), as the [ʿēḏer](../../strongs/h/h5739.md) in the [tavek](../../strongs/h/h8432.md) of their [dibēr](../../strongs/h/h1699.md): they shall make [huwm](../../strongs/h/h1949.md) of ['āḏām](../../strongs/h/h120.md).

<a name="micah_2_13"></a>Micah 2:13

The [pāraṣ](../../strongs/h/h6555.md) is [ʿālâ](../../strongs/h/h5927.md) [paniym](../../strongs/h/h6440.md) them: they have [pāraṣ](../../strongs/h/h6555.md), and have ['abar](../../strongs/h/h5674.md) the [sha'ar](../../strongs/h/h8179.md), and are [yāṣā'](../../strongs/h/h3318.md) by it: and their [melek](../../strongs/h/h4428.md) shall ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) them, and [Yĕhovah](../../strongs/h/h3068.md) on the [ro'sh](../../strongs/h/h7218.md) of them.

---

[Transliteral Bible](../bible.md)

[Micah](micah.md)

[Micah 1](micah_1.md) - [Micah 3](micah_3.md)