# [Micah 4](https://www.blueletterbible.org/kjv/micah/4)

<a name="micah_4_1"></a>Micah 4:1

But in the ['aḥărîṯ](../../strongs/h/h319.md) [yowm](../../strongs/h/h3117.md) it shall come to pass, that the [har](../../strongs/h/h2022.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) shall be [kuwn](../../strongs/h/h3559.md) in the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md), and it shall be [nasa'](../../strongs/h/h5375.md) above the [giḇʿâ](../../strongs/h/h1389.md); and ['am](../../strongs/h/h5971.md) shall [nāhar](../../strongs/h/h5102.md) unto it.

<a name="micah_4_2"></a>Micah 4:2

And [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md) shall [halak](../../strongs/h/h1980.md), and ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), and let us [ʿālâ](../../strongs/h/h5927.md) to the [har](../../strongs/h/h2022.md) of [Yĕhovah](../../strongs/h/h3068.md), and to the [bayith](../../strongs/h/h1004.md) of the ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md); and he will [yārâ](../../strongs/h/h3384.md) us of his [derek](../../strongs/h/h1870.md), and we will [yālaḵ](../../strongs/h/h3212.md) in his ['orach](../../strongs/h/h734.md): for the [towrah](../../strongs/h/h8451.md) shall [yāṣā'](../../strongs/h/h3318.md) of [Tsiyown](../../strongs/h/h6726.md), and the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) from [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="micah_4_3"></a>Micah 4:3

And he shall [shaphat](../../strongs/h/h8199.md) among [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md), and [yakach](../../strongs/h/h3198.md) ['atsuwm](../../strongs/h/h6099.md) [gowy](../../strongs/h/h1471.md) afar [rachowq](../../strongs/h/h7350.md); and they shall [kāṯaṯ](../../strongs/h/h3807.md) their [chereb](../../strongs/h/h2719.md) into ['ēṯ](../../strongs/h/h855.md), and their [ḥănîṯ](../../strongs/h/h2595.md) into [mazmērâ](../../strongs/h/h4211.md): [gowy](../../strongs/h/h1471.md) shall not [nasa'](../../strongs/h/h5375.md) a [chereb](../../strongs/h/h2719.md) against [gowy](../../strongs/h/h1471.md), neither shall they [lamad](../../strongs/h/h3925.md) [milḥāmâ](../../strongs/h/h4421.md) any more.

<a name="micah_4_4"></a>Micah 4:4

But they shall [yashab](../../strongs/h/h3427.md) every ['iysh](../../strongs/h/h376.md) under his [gep̄en](../../strongs/h/h1612.md) and under his [tĕ'en](../../strongs/h/h8384.md); and none shall make them [ḥārēḏ](../../strongs/h/h2729.md): for the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) hath [dabar](../../strongs/h/h1696.md) it.

<a name="micah_4_5"></a>Micah 4:5

For all ['am](../../strongs/h/h5971.md) will [yālaḵ](../../strongs/h/h3212.md) every ['iysh](../../strongs/h/h376.md) in the [shem](../../strongs/h/h8034.md) of his ['Elohiym](../../strongs/h/h430.md), and we will [yālaḵ](../../strongs/h/h3212.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) ['owlam](../../strongs/h/h5769.md) and [ʿaḏ](../../strongs/h/h5703.md).

<a name="micah_4_6"></a>Micah 4:6

In that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), will I ['āsap̄](../../strongs/h/h622.md) her that [ṣālaʿ](../../strongs/h/h6760.md), and I will [qāḇaṣ](../../strongs/h/h6908.md) her that is [nāḏaḥ](../../strongs/h/h5080.md), and her that I have [ra'a'](../../strongs/h/h7489.md);

<a name="micah_4_7"></a>Micah 4:7

And I will [śûm](../../strongs/h/h7760.md) her that [ṣālaʿ](../../strongs/h/h6760.md) a [šᵊ'ērîṯ](../../strongs/h/h7611.md), and her that was [hālā'](../../strongs/h/h1972.md) an ['atsuwm](../../strongs/h/h6099.md) [gowy](../../strongs/h/h1471.md): and [Yĕhovah](../../strongs/h/h3068.md) shall [mālaḵ](../../strongs/h/h4427.md) over them in [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md) from henceforth, even ['owlam](../../strongs/h/h5769.md).

<a name="micah_4_8"></a>Micah 4:8

And thou, O [miḡdāl](../../strongs/h/h4026.md) of the [ʿēḏer](../../strongs/h/h5739.md) [miḡdal-ʿēḏer](../../strongs/h/h4029.md), the [ʿōp̄el](../../strongs/h/h6076.md) of the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md), unto thee shall it ['āṯâ](../../strongs/h/h857.md), even the [ri'šôn](../../strongs/h/h7223.md) [memshalah](../../strongs/h/h4475.md); the [mamlāḵâ](../../strongs/h/h4467.md) shall [bow'](../../strongs/h/h935.md) to the [bath](../../strongs/h/h1323.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="micah_4_9"></a>Micah 4:9

Now why dost thou [rûaʿ](../../strongs/h/h7321.md) out [rēaʿ](../../strongs/h/h7452.md)? is there no [melek](../../strongs/h/h4428.md) in thee? is thy [ya'ats](../../strongs/h/h3289.md) ['abad](../../strongs/h/h6.md)? for [ḥîl](../../strongs/h/h2427.md) have [ḥāzaq](../../strongs/h/h2388.md) thee as a [yalad](../../strongs/h/h3205.md).

<a name="micah_4_10"></a>Micah 4:10

Be in [chuwl](../../strongs/h/h2342.md), and labour to [gîaḥ](../../strongs/h/h1518.md), O [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md), like a [yalad](../../strongs/h/h3205.md): for now shalt thou [yāṣā'](../../strongs/h/h3318.md) out of the [qiryâ](../../strongs/h/h7151.md), and thou shalt [shakan](../../strongs/h/h7931.md) in the [sadeh](../../strongs/h/h7704.md), and thou shalt [bow'](../../strongs/h/h935.md) even to [Bāḇel](../../strongs/h/h894.md); there shalt thou be [natsal](../../strongs/h/h5337.md); there [Yĕhovah](../../strongs/h/h3068.md) shall [gā'al](../../strongs/h/h1350.md) thee from the [kaph](../../strongs/h/h3709.md) of thine ['oyeb](../../strongs/h/h341.md).

<a name="micah_4_11"></a>Micah 4:11

Now also [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md) are ['āsap̄](../../strongs/h/h622.md) against thee, that ['āmar](../../strongs/h/h559.md), Let her be [ḥānēp̄](../../strongs/h/h2610.md), and let our ['ayin](../../strongs/h/h5869.md) [chazah](../../strongs/h/h2372.md) upon [Tsiyown](../../strongs/h/h6726.md).

<a name="micah_4_12"></a>Micah 4:12

But they [yada'](../../strongs/h/h3045.md) not the [maḥăšāḇâ](../../strongs/h/h4284.md) of [Yĕhovah](../../strongs/h/h3068.md), neither [bîn](../../strongs/h/h995.md) they his ['etsah](../../strongs/h/h6098.md): for he shall [qāḇaṣ](../../strongs/h/h6908.md) them as the [ʿāmîr](../../strongs/h/h5995.md) into the [gōren](../../strongs/h/h1637.md).

<a name="micah_4_13"></a>Micah 4:13

[quwm](../../strongs/h/h6965.md) and [dûš](../../strongs/h/h1758.md), O [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md): for I will [śûm](../../strongs/h/h7760.md) thine [qeren](../../strongs/h/h7161.md) [barzel](../../strongs/h/h1270.md), and I will [śûm](../../strongs/h/h7760.md) thy [parsâ](../../strongs/h/h6541.md) [nᵊḥûšâ](../../strongs/h/h5154.md): and thou shalt beat in [dāqaq](../../strongs/h/h1854.md) [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md): and I will [ḥāram](../../strongs/h/h2763.md) their [beṣaʿ](../../strongs/h/h1215.md) unto [Yĕhovah](../../strongs/h/h3068.md), and their [ḥayil](../../strongs/h/h2428.md) unto the ['adown](../../strongs/h/h113.md) of the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Micah](micah.md)

[Micah 3](micah_3.md) - [Micah 5](micah_5.md)