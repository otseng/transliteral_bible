# Micah

[Micah Overview](../../commentary/micah/micah_overview.md)

[Micah 1](micah_1.md)

[Micah 2](micah_2.md)

[Micah 3](micah_3.md)

[Micah 4](micah_4.md)

[Micah 5](micah_5.md)

[Micah 6](micah_6.md)

[Micah 7](micah_7.md)

---

[Transliteral Bible](../index.md)