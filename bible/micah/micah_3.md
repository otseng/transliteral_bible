# [Micah 3](https://www.blueletterbible.org/kjv/micah/3)

<a name="micah_3_1"></a>Micah 3:1

And I ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md), I pray you, O [ro'sh](../../strongs/h/h7218.md) of [Ya'aqob](../../strongs/h/h3290.md), and ye [qāṣîn](../../strongs/h/h7101.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md); Is it not for you to [yada'](../../strongs/h/h3045.md) [mishpat](../../strongs/h/h4941.md)?

<a name="micah_3_2"></a>Micah 3:2

Who [sane'](../../strongs/h/h8130.md) the [towb](../../strongs/h/h2896.md), and ['ahab](../../strongs/h/h157.md) the [ra'](../../strongs/h/h7451.md); who [gāzal](../../strongs/h/h1497.md) their ['owr](../../strongs/h/h5785.md) from off them, and their [šᵊ'ēr](../../strongs/h/h7607.md) from off their ['etsem](../../strongs/h/h6106.md);

<a name="micah_3_3"></a>Micah 3:3

Who also ['akal](../../strongs/h/h398.md) the [šᵊ'ēr](../../strongs/h/h7607.md) of my ['am](../../strongs/h/h5971.md), and [pāšaṭ](../../strongs/h/h6584.md) their ['owr](../../strongs/h/h5785.md) from off them; and they [pāṣaḥ](../../strongs/h/h6476.md) their ['etsem](../../strongs/h/h6106.md), and [pāraś](../../strongs/h/h6566.md) them, as for the [sîr](../../strongs/h/h5518.md), and as [basar](../../strongs/h/h1320.md) [tavek](../../strongs/h/h8432.md) the [qallaḥaṯ](../../strongs/h/h7037.md).

<a name="micah_3_4"></a>Micah 3:4

Then shall they [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md), but he will not ['anah](../../strongs/h/h6030.md) them: he will even [cathar](../../strongs/h/h5641.md) his [paniym](../../strongs/h/h6440.md) from them at that [ʿēṯ](../../strongs/h/h6256.md), as they have behaved themselves [ra'a'](../../strongs/h/h7489.md) in their [maʿălāl](../../strongs/h/h4611.md).

<a name="micah_3_5"></a>Micah 3:5

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) concerning the [nāḇî'](../../strongs/h/h5030.md) that make my ['am](../../strongs/h/h5971.md) [tāʿâ](../../strongs/h/h8582.md), that [nāšaḵ](../../strongs/h/h5391.md) with their [šēn](../../strongs/h/h8127.md), and [qara'](../../strongs/h/h7121.md), [shalowm](../../strongs/h/h7965.md); and he that [nathan](../../strongs/h/h5414.md) not into their [peh](../../strongs/h/h6310.md), they even [qadash](../../strongs/h/h6942.md) [milḥāmâ](../../strongs/h/h4421.md) against him.

<a name="micah_3_6"></a>Micah 3:6

Therefore [layil](../../strongs/h/h3915.md) shall be unto you, that ye shall not have a [ḥāzôn](../../strongs/h/h2377.md); and it shall be [ḥāšaḵ](../../strongs/h/h2821.md) [ḥāšaḵ](../../strongs/h/h2821.md) unto you, that ye shall not [qāsam](../../strongs/h/h7080.md); and the [šemeš](../../strongs/h/h8121.md) shall [bow'](../../strongs/h/h935.md) over the [nāḇî'](../../strongs/h/h5030.md), and the [yowm](../../strongs/h/h3117.md) shall be [qāḏar](../../strongs/h/h6937.md) over them.

<a name="micah_3_7"></a>Micah 3:7

Then shall the [ḥōzê](../../strongs/h/h2374.md) be [buwsh](../../strongs/h/h954.md), and the [qāsam](../../strongs/h/h7080.md) [ḥāp̄ēr](../../strongs/h/h2659.md): yea, they shall all [ʿāṭâ](../../strongs/h/h5844.md) their [śāp̄ām](../../strongs/h/h8222.md); for there is no [maʿănê](../../strongs/h/h4617.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="micah_3_8"></a>Micah 3:8

But truly I am [mālā'](../../strongs/h/h4390.md) of [koach](../../strongs/h/h3581.md) by the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md), and of [mishpat](../../strongs/h/h4941.md), and of [gᵊḇûrâ](../../strongs/h/h1369.md), to [nāḡaḏ](../../strongs/h/h5046.md) unto [Ya'aqob](../../strongs/h/h3290.md) his [pesha'](../../strongs/h/h6588.md), and to [Yisra'el](../../strongs/h/h3478.md) his [chatta'ath](../../strongs/h/h2403.md).

<a name="micah_3_9"></a>Micah 3:9

[shama'](../../strongs/h/h8085.md) this, I pray you, ye [ro'sh](../../strongs/h/h7218.md) of the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), and [qāṣîn](../../strongs/h/h7101.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), that [ta'ab](../../strongs/h/h8581.md) [mishpat](../../strongs/h/h4941.md), and [ʿāqaš](../../strongs/h/h6140.md) all [yashar](../../strongs/h/h3477.md).

<a name="micah_3_10"></a>Micah 3:10

They [bānâ](../../strongs/h/h1129.md) [Tsiyown](../../strongs/h/h6726.md) with [dam](../../strongs/h/h1818.md), and [Yĕruwshalaim](../../strongs/h/h3389.md) with ['evel](../../strongs/h/h5766.md).

<a name="micah_3_11"></a>Micah 3:11

The [ro'sh](../../strongs/h/h7218.md) thereof [shaphat](../../strongs/h/h8199.md) for [shachad](../../strongs/h/h7810.md), and the [kōhēn](../../strongs/h/h3548.md) thereof [yārâ](../../strongs/h/h3384.md) for [mᵊḥîr](../../strongs/h/h4242.md), and the [nāḇî'](../../strongs/h/h5030.md) thereof [qāsam](../../strongs/h/h7080.md) for [keceph](../../strongs/h/h3701.md): yet will they [šāʿan](../../strongs/h/h8172.md) upon [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), Is not [Yĕhovah](../../strongs/h/h3068.md) [qereḇ](../../strongs/h/h7130.md) us? none [ra'](../../strongs/h/h7451.md) can [bow'](../../strongs/h/h935.md) upon us.

<a name="micah_3_12"></a>Micah 3:12

Therefore shall [Tsiyown](../../strongs/h/h6726.md) for your sake be [ḥāraš](../../strongs/h/h2790.md) as a [sadeh](../../strongs/h/h7704.md), and [Yĕruwshalaim](../../strongs/h/h3389.md) shall become [ʿî](../../strongs/h/h5856.md), and the [har](../../strongs/h/h2022.md) of the [bayith](../../strongs/h/h1004.md) as the [bāmâ](../../strongs/h/h1116.md) of the [yaʿar](../../strongs/h/h3293.md).

---

[Transliteral Bible](../bible.md)

[Micah](micah.md)

[Micah 2](micah_2.md) - [Micah 4](micah_4.md)