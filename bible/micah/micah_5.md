# [Micah 5](https://www.blueletterbible.org/kjv/micah/5)

<a name="micah_5_1"></a>Micah 5:1

Now [gāḏaḏ](../../strongs/h/h1413.md) thyself in [gᵊḏûḏ](../../strongs/h/h1416.md), O [bath](../../strongs/h/h1323.md) of [gᵊḏûḏ](../../strongs/h/h1416.md): he hath [śûm](../../strongs/h/h7760.md) [māṣôr](../../strongs/h/h4692.md) against us: they shall [nakah](../../strongs/h/h5221.md) the [shaphat](../../strongs/h/h8199.md) of [Yisra'el](../../strongs/h/h3478.md) with a [shebet](../../strongs/h/h7626.md) upon the [lᵊḥî](../../strongs/h/h3895.md).

<a name="micah_5_2"></a>Micah 5:2

But thou, [Bêṯ leḥem](../../strongs/h/h1035.md) ['ep̄rāṯ](../../strongs/h/h672.md), though thou be [ṣāʿîr](../../strongs/h/h6810.md) among the thousands of [Yehuwdah](../../strongs/h/h3063.md), yet out of thee shall he [yāṣā'](../../strongs/h/h3318.md) unto me that is to be [mashal](../../strongs/h/h4910.md) in [Yisra'el](../../strongs/h/h3478.md); whose [môṣā'â](../../strongs/h/h4163.md) have been from of [qeḏem](../../strongs/h/h6924.md), from [yowm](../../strongs/h/h3117.md) ['owlam](../../strongs/h/h5769.md).

<a name="micah_5_3"></a>Micah 5:3

Therefore will he [nathan](../../strongs/h/h5414.md) them, until the [ʿēṯ](../../strongs/h/h6256.md) that she which [yalad](../../strongs/h/h3205.md) hath [yalad](../../strongs/h/h3205.md): then the [yeṯer](../../strongs/h/h3499.md) of his ['ach](../../strongs/h/h251.md) shall [shuwb](../../strongs/h/h7725.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="micah_5_4"></a>Micah 5:4

And he shall ['amad](../../strongs/h/h5975.md) and [ra'ah](../../strongs/h/h7462.md) in the ['oz](../../strongs/h/h5797.md) of [Yĕhovah](../../strongs/h/h3068.md), in the [gā'ôn](../../strongs/h/h1347.md) of the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md); and they shall [yashab](../../strongs/h/h3427.md): for now shall he be [gāḏal](../../strongs/h/h1431.md) unto the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md).

<a name="micah_5_5"></a>Micah 5:5

And this man shall be the [shalowm](../../strongs/h/h7965.md), when the ['Aššûr](../../strongs/h/h804.md) shall [bow'](../../strongs/h/h935.md) into our ['erets](../../strongs/h/h776.md): and when he shall [dāraḵ](../../strongs/h/h1869.md) in our ['armôn](../../strongs/h/h759.md), then shall we [quwm](../../strongs/h/h6965.md) against him seven [ra'ah](../../strongs/h/h7462.md), and eight [nāsîḵ](../../strongs/h/h5257.md) ['āḏām](../../strongs/h/h120.md).

<a name="micah_5_6"></a>Micah 5:6

And they shall [ra'ah](../../strongs/h/h7462.md) the ['erets](../../strongs/h/h776.md) of ['Aššûr](../../strongs/h/h804.md) with the [chereb](../../strongs/h/h2719.md), and the ['erets](../../strongs/h/h776.md) of [Nimrōḏ](../../strongs/h/h5248.md) in the [peṯaḥ](../../strongs/h/h6607.md) thereof: thus shall he [natsal](../../strongs/h/h5337.md) us from the ['Aššûr](../../strongs/h/h804.md), when he [bow'](../../strongs/h/h935.md) into our ['erets](../../strongs/h/h776.md), and when he [dāraḵ](../../strongs/h/h1869.md) within our [gᵊḇûl](../../strongs/h/h1366.md).

<a name="micah_5_7"></a>Micah 5:7

And the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Ya'aqob](../../strongs/h/h3290.md) shall be in the [qereḇ](../../strongs/h/h7130.md) of [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) as a [ṭal](../../strongs/h/h2919.md) from [Yĕhovah](../../strongs/h/h3068.md), as the [rᵊḇîḇîm](../../strongs/h/h7241.md) upon the ['eseb](../../strongs/h/h6212.md), that [qāvâ](../../strongs/h/h6960.md) not for ['iysh](../../strongs/h/h376.md), nor [yāḥal](../../strongs/h/h3176.md) for the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md).

<a name="micah_5_8"></a>Micah 5:8

And the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Ya'aqob](../../strongs/h/h3290.md) shall be among the [gowy](../../strongs/h/h1471.md) in the [qereḇ](../../strongs/h/h7130.md) of [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) as an ['ariy](../../strongs/h/h738.md) among the [bĕhemah](../../strongs/h/h929.md) of the [yaʿar](../../strongs/h/h3293.md), as a [kephiyr](../../strongs/h/h3715.md) among the [ʿēḏer](../../strongs/h/h5739.md) of [tso'n](../../strongs/h/h6629.md): who, if he ['abar](../../strongs/h/h5674.md), both [rāmas](../../strongs/h/h7429.md), and [taraph](../../strongs/h/h2963.md), and none can [natsal](../../strongs/h/h5337.md).

<a name="micah_5_9"></a>Micah 5:9

Thine [yad](../../strongs/h/h3027.md) shall be [ruwm](../../strongs/h/h7311.md) upon thine [tsar](../../strongs/h/h6862.md), and all thine ['oyeb](../../strongs/h/h341.md) shall be [karath](../../strongs/h/h3772.md).

<a name="micah_5_10"></a>Micah 5:10

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that I will [karath](../../strongs/h/h3772.md) thy [sûs](../../strongs/h/h5483.md) out of the [qereḇ](../../strongs/h/h7130.md) of thee, and I will ['abad](../../strongs/h/h6.md) thy [merkāḇâ](../../strongs/h/h4818.md):

<a name="micah_5_11"></a>Micah 5:11

And I will [karath](../../strongs/h/h3772.md) the [ʿĀr](../../strongs/h/h6145.md) [ʿîr](../../strongs/h/h5892.md) of thy ['erets](../../strongs/h/h776.md), and [harac](../../strongs/h/h2040.md) all thy [miḇṣār](../../strongs/h/h4013.md):

<a name="micah_5_12"></a>Micah 5:12

And I will [karath](../../strongs/h/h3772.md) [kešep̄](../../strongs/h/h3785.md) out of thine [yad](../../strongs/h/h3027.md); and thou shalt have no more [ʿānan](../../strongs/h/h6049.md):

<a name="micah_5_13"></a>Micah 5:13

Thy [pāsîl](../../strongs/h/h6456.md) also will I [karath](../../strongs/h/h3772.md), and thy standing [maṣṣēḇâ](../../strongs/h/h4676.md) out of the [qereḇ](../../strongs/h/h7130.md) of thee; and thou shalt no more [shachah](../../strongs/h/h7812.md) the [ma'aseh](../../strongs/h/h4639.md) of thine [yad](../../strongs/h/h3027.md).

<a name="micah_5_14"></a>Micah 5:14

And I will [nathash](../../strongs/h/h5428.md) thy ['ăšērâ](../../strongs/h/h842.md) out of the [qereḇ](../../strongs/h/h7130.md) of thee: so will I [šāmaḏ](../../strongs/h/h8045.md) thy [ʿĀr](../../strongs/h/h6145.md) [ʿîr](../../strongs/h/h5892.md).

<a name="micah_5_15"></a>Micah 5:15

And I will ['asah](../../strongs/h/h6213.md) [nāqām](../../strongs/h/h5359.md) in ['aph](../../strongs/h/h639.md) and [chemah](../../strongs/h/h2534.md) upon the [gowy](../../strongs/h/h1471.md), such as they have not [shama'](../../strongs/h/h8085.md).

---

[Transliteral Bible](../bible.md)

[Micah](micah.md)

[Micah 4](micah_4.md) - [Micah 6](micah_6.md)