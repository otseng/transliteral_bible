# [Micah 6](https://www.blueletterbible.org/kjv/micah/6)

<a name="micah_6_1"></a>Micah 6:1

[shama'](../../strongs/h/h8085.md) ye now what [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md); [quwm](../../strongs/h/h6965.md), [riyb](../../strongs/h/h7378.md) thou before the [har](../../strongs/h/h2022.md), and let the [giḇʿâ](../../strongs/h/h1389.md) [shama'](../../strongs/h/h8085.md) thy [qowl](../../strongs/h/h6963.md).

<a name="micah_6_2"></a>Micah 6:2

[shama'](../../strongs/h/h8085.md) ye, O [har](../../strongs/h/h2022.md), [Yĕhovah](../../strongs/h/h3068.md) [rîḇ](../../strongs/h/h7379.md), and ye ['êṯān](../../strongs/h/h386.md) [mowcadah](../../strongs/h/h4146.md) of the ['erets](../../strongs/h/h776.md): for [Yĕhovah](../../strongs/h/h3068.md) hath a [rîḇ](../../strongs/h/h7379.md) with his ['am](../../strongs/h/h5971.md), and he will [yakach](../../strongs/h/h3198.md) with [Yisra'el](../../strongs/h/h3478.md).

<a name="micah_6_3"></a>Micah 6:3

O my ['am](../../strongs/h/h5971.md), what have I ['asah](../../strongs/h/h6213.md) unto thee? and wherein have I [lā'â](../../strongs/h/h3811.md) thee? ['anah](../../strongs/h/h6030.md) against me.

<a name="micah_6_4"></a>Micah 6:4

For I [ʿālâ](../../strongs/h/h5927.md) thee out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and [pāḏâ](../../strongs/h/h6299.md) thee out of the [bayith](../../strongs/h/h1004.md) of ['ebed](../../strongs/h/h5650.md); and I [shalach](../../strongs/h/h7971.md) [paniym](../../strongs/h/h6440.md) thee [Mōshe](../../strongs/h/h4872.md), ['Ahărôn](../../strongs/h/h175.md), and [Miryām](../../strongs/h/h4813.md).

<a name="micah_6_5"></a>Micah 6:5

O my ['am](../../strongs/h/h5971.md), [zakar](../../strongs/h/h2142.md) now what [Bālāq](../../strongs/h/h1111.md) [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md) [ya'ats](../../strongs/h/h3289.md), and what [Bilʿām](../../strongs/h/h1109.md) the [ben](../../strongs/h/h1121.md) of [bᵊʿôr](../../strongs/h/h1160.md) ['anah](../../strongs/h/h6030.md) him from [Šiṭṭāym](../../strongs/h/h7851.md) unto [Gilgāl](../../strongs/h/h1537.md); that ye may [yada'](../../strongs/h/h3045.md) the [tsedaqah](../../strongs/h/h6666.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="micah_6_6"></a>Micah 6:6

Wherewith shall I [qadam](../../strongs/h/h6923.md) [Yĕhovah](../../strongs/h/h3068.md), and [kāp̄ap̄](../../strongs/h/h3721.md) myself before the [marowm](../../strongs/h/h4791.md) ['Elohiym](../../strongs/h/h430.md)? shall I [qadam](../../strongs/h/h6923.md) him with [ʿōlâ](../../strongs/h/h5930.md), with [ʿēḡel](../../strongs/h/h5695.md) of a [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md)?

<a name="micah_6_7"></a>Micah 6:7

Will [Yĕhovah](../../strongs/h/h3068.md) be [ratsah](../../strongs/h/h7521.md) with thousands of ['ayil](../../strongs/h/h352.md), or with ten thousands of [nachal](../../strongs/h/h5158.md) of [šemen](../../strongs/h/h8081.md)? shall I [nathan](../../strongs/h/h5414.md) my [bᵊḵôr](../../strongs/h/h1060.md) for my [pesha'](../../strongs/h/h6588.md), the [pĕriy](../../strongs/h/h6529.md) of my [beten](../../strongs/h/h990.md) for the [chatta'ath](../../strongs/h/h2403.md) of my [nephesh](../../strongs/h/h5315.md)?

<a name="micah_6_8"></a>Micah 6:8

He hath [nāḡaḏ](../../strongs/h/h5046.md) thee, O ['āḏām](../../strongs/h/h120.md), what is [towb](../../strongs/h/h2896.md); and what doth [Yĕhovah](../../strongs/h/h3068.md) [darash](../../strongs/h/h1875.md) of thee, but to ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md), and to ['ahăḇâ](../../strongs/h/h160.md) [checed](../../strongs/h/h2617.md), and to [yālaḵ](../../strongs/h/h3212.md) [ṣānaʿ](../../strongs/h/h6800.md) with thy ['Elohiym](../../strongs/h/h430.md)?

<a name="micah_6_9"></a>Micah 6:9

[Yĕhovah](../../strongs/h/h3068.md) [qowl](../../strongs/h/h6963.md) [qara'](../../strongs/h/h7121.md) unto the [ʿîr](../../strongs/h/h5892.md), and the man of [tûšîyâ](../../strongs/h/h8454.md) shall [ra'ah](../../strongs/h/h7200.md) thy [shem](../../strongs/h/h8034.md): [shama'](../../strongs/h/h8085.md) ye the [maṭṭê](../../strongs/h/h4294.md), and who hath [yāʿaḏ](../../strongs/h/h3259.md) it.

<a name="micah_6_10"></a>Micah 6:10

Are ['iš](../../strongs/h/h786.md) yet the ['ôṣār](../../strongs/h/h214.md) of [resha'](../../strongs/h/h7562.md) ['iysh](../../strongs/h/h376.md) in the [bayith](../../strongs/h/h1004.md) of the [rasha'](../../strongs/h/h7563.md), and the [rāzôn](../../strongs/h/h7332.md) ['êp̄â](../../strongs/h/h374.md) that is [za'am](../../strongs/h/h2194.md)?

<a name="micah_6_11"></a>Micah 6:11

Shall I count them [zāḵâ](../../strongs/h/h2135.md) with the [resha'](../../strongs/h/h7562.md) [mō'znayim](../../strongs/h/h3976.md), and with the [kîs](../../strongs/h/h3599.md) of [mirmah](../../strongs/h/h4820.md) ['eben](../../strongs/h/h68.md)?

<a name="micah_6_12"></a>Micah 6:12

For the [ʿāšîr](../../strongs/h/h6223.md) thereof are [mālā'](../../strongs/h/h4390.md) of [chamac](../../strongs/h/h2555.md), and the [yashab](../../strongs/h/h3427.md) thereof have [dabar](../../strongs/h/h1696.md) [sheqer](../../strongs/h/h8267.md), and their [lashown](../../strongs/h/h3956.md) is [rᵊmîyâ](../../strongs/h/h7423.md) in their [peh](../../strongs/h/h6310.md).

<a name="micah_6_13"></a>Micah 6:13

Therefore also will I make thee [ḥālâ](../../strongs/h/h2470.md) in [nakah](../../strongs/h/h5221.md) thee, in making thee [šāmēm](../../strongs/h/h8074.md) because of thy [chatta'ath](../../strongs/h/h2403.md).

<a name="micah_6_14"></a>Micah 6:14

Thou shalt ['akal](../../strongs/h/h398.md), but not be [sāׂbaʿ](../../strongs/h/h7646.md); and thy [yešaḥ](../../strongs/h/h3445.md) shall be in the [qereḇ](../../strongs/h/h7130.md) of thee; and thou shalt take [nāsaḡ](../../strongs/h/h5253.md), but shalt not [palat](../../strongs/h/h6403.md); and that which thou [palat](../../strongs/h/h6403.md) will I give [nathan](../../strongs/h/h5414.md) to the [chereb](../../strongs/h/h2719.md).

<a name="micah_6_15"></a>Micah 6:15

Thou shalt [zāraʿ](../../strongs/h/h2232.md), but thou shalt not [qāṣar](../../strongs/h/h7114.md); thou shalt [dāraḵ](../../strongs/h/h1869.md) the [zayiṯ](../../strongs/h/h2132.md), but thou shalt not [sûḵ](../../strongs/h/h5480.md) thee with [šemen](../../strongs/h/h8081.md); and [tiyrowsh](../../strongs/h/h8492.md), but shalt not [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md).

<a name="micah_6_16"></a>Micah 6:16

For the [chuqqah](../../strongs/h/h2708.md) of [ʿĀmrî](../../strongs/h/h6018.md) are [shamar](../../strongs/h/h8104.md), and all the [ma'aseh](../../strongs/h/h4639.md) of the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md), and ye [yālaḵ](../../strongs/h/h3212.md) in their [mow'etsah](../../strongs/h/h4156.md); that I should [nathan](../../strongs/h/h5414.md) thee a [šammâ](../../strongs/h/h8047.md), and the [yashab](../../strongs/h/h3427.md) thereof a [šᵊrēqâ](../../strongs/h/h8322.md): therefore ye shall [nasa'](../../strongs/h/h5375.md) the [cherpah](../../strongs/h/h2781.md) of my ['am](../../strongs/h/h5971.md).

---

[Transliteral Bible](../bible.md)

[Micah](micah.md)

[Micah 5](micah_5.md) - [Micah 7](micah_7.md)