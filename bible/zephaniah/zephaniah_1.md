# [Zephaniah 1](https://www.blueletterbible.org/kjv/zephaniah/1)

<a name="zephaniah_1_1"></a>Zephaniah 1:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) which came unto [Ṣᵊp̄Anyâ](../../strongs/h/h6846.md) the [ben](../../strongs/h/h1121.md) of [Kûšî](../../strongs/h/h3570.md), the [ben](../../strongs/h/h1121.md) of [Gᵊḏalyâ](../../strongs/h/h1436.md), the [ben](../../strongs/h/h1121.md) of ['Ămaryâ](../../strongs/h/h568.md), the [ben](../../strongs/h/h1121.md) of [Ḥizqîyâ](../../strongs/h/h2396.md), in the [yowm](../../strongs/h/h3117.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) the [ben](../../strongs/h/h1121.md) of ['Āmôn](../../strongs/h/h526.md), [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="zephaniah_1_2"></a>Zephaniah 1:2

I will ['āsap̄](../../strongs/h/h622.md) [sûp̄](../../strongs/h/h5486.md) all things from off the [paniym](../../strongs/h/h6440.md) ['ăḏāmâ](../../strongs/h/h127.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="zephaniah_1_3"></a>Zephaniah 1:3

I will [sûp̄](../../strongs/h/h5486.md) ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md); I will [sûp̄](../../strongs/h/h5486.md) the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and the [dag](../../strongs/h/h1709.md) of the [yam](../../strongs/h/h3220.md), and the [maḵšēlâ](../../strongs/h/h4384.md) with the [rasha'](../../strongs/h/h7563.md); and I will [karath](../../strongs/h/h3772.md) ['āḏām](../../strongs/h/h120.md) from [paniym](../../strongs/h/h6440.md) the ['ăḏāmâ](../../strongs/h/h127.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="zephaniah_1_4"></a>Zephaniah 1:4

I will also [natah](../../strongs/h/h5186.md) mine [yad](../../strongs/h/h3027.md) upon [Yehuwdah](../../strongs/h/h3063.md), and upon all the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md); and I will [karath](../../strongs/h/h3772.md) the [šᵊ'ār](../../strongs/h/h7605.md) of [BaʿAl](../../strongs/h/h1168.md) from this [maqowm](../../strongs/h/h4725.md), and the [shem](../../strongs/h/h8034.md) of the [kōmer](../../strongs/h/h3649.md) with the [kōhēn](../../strongs/h/h3548.md);

<a name="zephaniah_1_5"></a>Zephaniah 1:5

And them that [shachah](../../strongs/h/h7812.md) the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md) upon the [gāḡ](../../strongs/h/h1406.md); and them that [shachah](../../strongs/h/h7812.md) and that [shaba'](../../strongs/h/h7650.md) by [Yĕhovah](../../strongs/h/h3068.md), and that [shaba'](../../strongs/h/h7650.md) by [melek](../../strongs/h/h4428.md);

<a name="zephaniah_1_6"></a>Zephaniah 1:6

And them that are [sûḡ](../../strongs/h/h5472.md) ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md); and those that have not [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md), nor [darash](../../strongs/h/h1875.md) for him.

<a name="zephaniah_1_7"></a>Zephaniah 1:7

[hāsâ](../../strongs/h/h2013.md) at the [paniym](../../strongs/h/h6440.md) of the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md): for the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) is at [qarowb](../../strongs/h/h7138.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [kuwn](../../strongs/h/h3559.md) a [zebach](../../strongs/h/h2077.md), he hath [qadash](../../strongs/h/h6942.md) his [qara'](../../strongs/h/h7121.md).

<a name="zephaniah_1_8"></a>Zephaniah 1:8

And it shall come to pass in the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) [zebach](../../strongs/h/h2077.md), that I will [paqad](../../strongs/h/h6485.md) the [śar](../../strongs/h/h8269.md), and the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md), and all such as are [labash](../../strongs/h/h3847.md) with [nāḵrî](../../strongs/h/h5237.md) [malbûš](../../strongs/h/h4403.md).

<a name="zephaniah_1_9"></a>Zephaniah 1:9

In the same [yowm](../../strongs/h/h3117.md) also will I [paqad](../../strongs/h/h6485.md) all those that [dālaḡ](../../strongs/h/h1801.md) on the [mip̄tān](../../strongs/h/h4670.md), which [mālā'](../../strongs/h/h4390.md) their ['adown](../../strongs/h/h113.md) [bayith](../../strongs/h/h1004.md) with [chamac](../../strongs/h/h2555.md) and [mirmah](../../strongs/h/h4820.md).

<a name="zephaniah_1_10"></a>Zephaniah 1:10

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that there shall be the [qowl](../../strongs/h/h6963.md) of a [tsa'aqah](../../strongs/h/h6818.md) from the [dag](../../strongs/h/h1709.md) [sha'ar](../../strongs/h/h8179.md), and a [yᵊlālâ](../../strongs/h/h3215.md) from the [mišnê](../../strongs/h/h4932.md), and a [gadowl](../../strongs/h/h1419.md) [šeḇar](../../strongs/h/h7667.md) from the [giḇʿâ](../../strongs/h/h1389.md).

<a name="zephaniah_1_11"></a>Zephaniah 1:11

[yālal](../../strongs/h/h3213.md), ye [yashab](../../strongs/h/h3427.md) of [maḵtēš](../../strongs/h/h4389.md), for all the [Kĕna'an](../../strongs/h/h3667.md) ['am](../../strongs/h/h5971.md) are [damah](../../strongs/h/h1820.md); all they that [nāṭîl](../../strongs/h/h5187.md) [keceph](../../strongs/h/h3701.md) are [karath](../../strongs/h/h3772.md).

<a name="zephaniah_1_12"></a>Zephaniah 1:12

And it shall come to pass at that [ʿēṯ](../../strongs/h/h6256.md), that I will [ḥāp̄aś](../../strongs/h/h2664.md) [Yĕruwshalaim](../../strongs/h/h3389.md) with [nîr](../../strongs/h/h5216.md), and [paqad](../../strongs/h/h6485.md) the ['enowsh](../../strongs/h/h582.md) that are [qāp̄ā'](../../strongs/h/h7087.md) on their [šemer](../../strongs/h/h8105.md): that ['āmar](../../strongs/h/h559.md) in their [lebab](../../strongs/h/h3824.md), [Yĕhovah](../../strongs/h/h3068.md) will not do [yatab](../../strongs/h/h3190.md), neither will he do [ra'a'](../../strongs/h/h7489.md).

<a name="zephaniah_1_13"></a>Zephaniah 1:13

Therefore their [ḥayil](../../strongs/h/h2428.md) shall become a [mᵊšissâ](../../strongs/h/h4933.md), and their [bayith](../../strongs/h/h1004.md) a [šᵊmāmâ](../../strongs/h/h8077.md): they shall also [bānâ](../../strongs/h/h1129.md) [bayith](../../strongs/h/h1004.md), but not [yashab](../../strongs/h/h3427.md) them; and they shall [nāṭaʿ](../../strongs/h/h5193.md) [kerem](../../strongs/h/h3754.md), but not [šāṯâ](../../strongs/h/h8354.md) the [yayin](../../strongs/h/h3196.md) thereof.

<a name="zephaniah_1_14"></a>Zephaniah 1:14

The [gadowl](../../strongs/h/h1419.md) [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) is [qarowb](../../strongs/h/h7138.md), it is [qarowb](../../strongs/h/h7138.md), and [mahēr](../../strongs/h/h4118.md) [me'od](../../strongs/h/h3966.md), even the [qowl](../../strongs/h/h6963.md) of the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md): the [gibôr](../../strongs/h/h1368.md) shall [ṣāraḥ](../../strongs/h/h6873.md) there [mar](../../strongs/h/h4751.md).

<a name="zephaniah_1_15"></a>Zephaniah 1:15

That [yowm](../../strongs/h/h3117.md) is a [yowm](../../strongs/h/h3117.md) of ['ebrah](../../strongs/h/h5678.md), a [yowm](../../strongs/h/h3117.md) of [tsarah](../../strongs/h/h6869.md) and [mᵊṣûqâ](../../strongs/h/h4691.md), a [yowm](../../strongs/h/h3117.md) of [šô'](../../strongs/h/h7722.md) and [mᵊšô'â](../../strongs/h/h4875.md), a [yowm](../../strongs/h/h3117.md) of [choshek](../../strongs/h/h2822.md) and ['ăp̄ēlâ](../../strongs/h/h653.md), a [yowm](../../strongs/h/h3117.md) of [ʿānān](../../strongs/h/h6051.md) and ['araphel](../../strongs/h/h6205.md),

<a name="zephaniah_1_16"></a>Zephaniah 1:16

A [yowm](../../strongs/h/h3117.md) of the [šôp̄ār](../../strongs/h/h7782.md) and [tᵊrûʿâ](../../strongs/h/h8643.md) against the [bāṣar](../../strongs/h/h1219.md) [ʿîr](../../strongs/h/h5892.md), and against the [gāḇōha](../../strongs/h/h1364.md) [pinnâ](../../strongs/h/h6438.md).

<a name="zephaniah_1_17"></a>Zephaniah 1:17

And I will bring [tsarar](../../strongs/h/h6887.md) upon ['āḏām](../../strongs/h/h120.md), that they shall [halak](../../strongs/h/h1980.md) like [ʿiûēr](../../strongs/h/h5787.md), because they have [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md): and their [dam](../../strongs/h/h1818.md) shall be [šāp̄aḵ](../../strongs/h/h8210.md) as ['aphar](../../strongs/h/h6083.md), and their [lāḥûm](../../strongs/h/h3894.md) as the [gēlel](../../strongs/h/h1561.md).

<a name="zephaniah_1_18"></a>Zephaniah 1:18

Neither their [keceph](../../strongs/h/h3701.md) nor their [zāhāḇ](../../strongs/h/h2091.md) shall be [yakol](../../strongs/h/h3201.md) to [natsal](../../strongs/h/h5337.md) them in the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) ['ebrah](../../strongs/h/h5678.md); but the ['erets](../../strongs/h/h776.md) shall be ['akal](../../strongs/h/h398.md) by the ['esh](../../strongs/h/h784.md) of his [qin'â](../../strongs/h/h7068.md): for he shall ['asah](../../strongs/h/h6213.md) even a [bahal](../../strongs/h/h926.md) [kālâ](../../strongs/h/h3617.md) of all them that [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Zephaniah](zephaniah.md)

[Zephaniah 2](zephaniah_2.md)