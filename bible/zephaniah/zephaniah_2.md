# [Zephaniah 2](https://www.blueletterbible.org/kjv/zephaniah/2)

<a name="zephaniah_2_1"></a>Zephaniah 2:1

[qāšaš](../../strongs/h/h7197.md) yourselves, yea, [qāšaš](../../strongs/h/h7197.md), O [gowy](../../strongs/h/h1471.md) not [kacaph](../../strongs/h/h3700.md);

<a name="zephaniah_2_2"></a>Zephaniah 2:2

Before the [choq](../../strongs/h/h2706.md) [yalad](../../strongs/h/h3205.md), before the [yowm](../../strongs/h/h3117.md) ['abar](../../strongs/h/h5674.md) as the [mots](../../strongs/h/h4671.md), before the [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md) upon you, before the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) ['aph](../../strongs/h/h639.md) [bow'](../../strongs/h/h935.md) upon you.

<a name="zephaniah_2_3"></a>Zephaniah 2:3

[bāqaš](../../strongs/h/h1245.md) ye [Yĕhovah](../../strongs/h/h3068.md), all ye ['anav](../../strongs/h/h6035.md) of the ['erets](../../strongs/h/h776.md), which have [pa'al](../../strongs/h/h6466.md) his [mishpat](../../strongs/h/h4941.md); [bāqaš](../../strongs/h/h1245.md) [tsedeq](../../strongs/h/h6664.md), [bāqaš](../../strongs/h/h1245.md) [ʿănāvâ](../../strongs/h/h6038.md): it may be ye shall be [cathar](../../strongs/h/h5641.md) in the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) ['aph](../../strongs/h/h639.md).

<a name="zephaniah_2_4"></a>Zephaniah 2:4

For [ʿAzzâ](../../strongs/h/h5804.md) shall be ['azab](../../strongs/h/h5800.md), and ['Ašqᵊlôn](../../strongs/h/h831.md) a [šᵊmāmâ](../../strongs/h/h8077.md): they shall [gāraš](../../strongs/h/h1644.md) ['Ašdôḏ](../../strongs/h/h795.md) at the [ṣōhar](../../strongs/h/h6672.md), and [ʿEqrôn](../../strongs/h/h6138.md) shall be [ʿāqar](../../strongs/h/h6131.md).

<a name="zephaniah_2_5"></a>Zephaniah 2:5

[hôy](../../strongs/h/h1945.md) unto the [yashab](../../strongs/h/h3427.md) of the [yam](../../strongs/h/h3220.md) [chebel](../../strongs/h/h2256.md), the [gowy](../../strongs/h/h1471.md) of the [kᵊrēṯî](../../strongs/h/h3774.md)! the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) is against you; O [Kĕna'an](../../strongs/h/h3667.md), the ['erets](../../strongs/h/h776.md) of the [Pᵊlištî](../../strongs/h/h6430.md), I will even ['abad](../../strongs/h/h6.md) thee, that there shall be no [yashab](../../strongs/h/h3427.md).

<a name="zephaniah_2_6"></a>Zephaniah 2:6

And the [yam](../../strongs/h/h3220.md) [chebel](../../strongs/h/h2256.md) shall be [nāvê](../../strongs/h/h5116.md) and [kārâ](../../strongs/h/h3741.md) for [ra'ah](../../strongs/h/h7462.md), and [gᵊḏērâ](../../strongs/h/h1448.md) for [tso'n](../../strongs/h/h6629.md).

<a name="zephaniah_2_7"></a>Zephaniah 2:7

And the [chebel](../../strongs/h/h2256.md) shall be for the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md); they shall [ra'ah](../../strongs/h/h7462.md) thereupon: in the [bayith](../../strongs/h/h1004.md) of ['Ašqᵊlôn](../../strongs/h/h831.md) shall they [rāḇaṣ](../../strongs/h/h7257.md) in the ['ereb](../../strongs/h/h6153.md): for [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md) shall [paqad](../../strongs/h/h6485.md) them, and [shuwb](../../strongs/h/h7725.md) their [shebuwth](../../strongs/h/h7622.md) [shebuwth](../../strongs/h/h7622.md).

<a name="zephaniah_2_8"></a>Zephaniah 2:8

I have [shama'](../../strongs/h/h8085.md) the [cherpah](../../strongs/h/h2781.md) of [Mô'āḇ](../../strongs/h/h4124.md), and the [gidûp̄](../../strongs/h/h1421.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), whereby they have [ḥārap̄](../../strongs/h/h2778.md) my ['am](../../strongs/h/h5971.md), and [gāḏal](../../strongs/h/h1431.md) themselves against their [gᵊḇûl](../../strongs/h/h1366.md).

<a name="zephaniah_2_9"></a>Zephaniah 2:9

Therefore as I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), Surely [Mô'āḇ](../../strongs/h/h4124.md) shall be as [Sᵊḏōm](../../strongs/h/h5467.md), and the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) as [ʿĂmōrâ](../../strongs/h/h6017.md), even the [mimšāq](../../strongs/h/h4476.md) of [ḥārûl](../../strongs/h/h2738.md), and [miḵrê](../../strongs/h/h4379.md) [melaḥ](../../strongs/h/h4417.md), and a ['owlam](../../strongs/h/h5769.md) [šᵊmāmâ](../../strongs/h/h8077.md): the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of my ['am](../../strongs/h/h5971.md) shall [bāzaz](../../strongs/h/h962.md) them, and the [yeṯer](../../strongs/h/h3499.md) of my [gowy](../../strongs/h/h1471.md) shall [nāḥal](../../strongs/h/h5157.md) them.

<a name="zephaniah_2_10"></a>Zephaniah 2:10

This shall they have for their [gā'ôn](../../strongs/h/h1347.md), because they have [ḥārap̄](../../strongs/h/h2778.md) and [gāḏal](../../strongs/h/h1431.md) themselves against the ['am](../../strongs/h/h5971.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="zephaniah_2_11"></a>Zephaniah 2:11

[Yĕhovah](../../strongs/h/h3068.md) will be [yare'](../../strongs/h/h3372.md) unto them: for he will [rāzâ](../../strongs/h/h7329.md) all the ['Elohiym](../../strongs/h/h430.md) of the ['erets](../../strongs/h/h776.md); and men shall [shachah](../../strongs/h/h7812.md) him, every ['iysh](../../strongs/h/h376.md) from his [maqowm](../../strongs/h/h4725.md), even all the ['î](../../strongs/h/h339.md) of the [gowy](../../strongs/h/h1471.md).

<a name="zephaniah_2_12"></a>Zephaniah 2:12

Ye [Kûšî](../../strongs/h/h3569.md) also, ye shall be [ḥālāl](../../strongs/h/h2491.md) by my [chereb](../../strongs/h/h2719.md).

<a name="zephaniah_2_13"></a>Zephaniah 2:13

And he will [natah](../../strongs/h/h5186.md) his [yad](../../strongs/h/h3027.md) against the [ṣāp̄ôn](../../strongs/h/h6828.md), and ['abad](../../strongs/h/h6.md) ['Aššûr](../../strongs/h/h804.md); and will [śûm](../../strongs/h/h7760.md) [Nînvê](../../strongs/h/h5210.md) a [šᵊmāmâ](../../strongs/h/h8077.md), and [ṣîyâ](../../strongs/h/h6723.md) like a [midbar](../../strongs/h/h4057.md).

<a name="zephaniah_2_14"></a>Zephaniah 2:14

And [ʿēḏer](../../strongs/h/h5739.md) shall [rāḇaṣ](../../strongs/h/h7257.md) in the [tavek](../../strongs/h/h8432.md) of her, all the [chay](../../strongs/h/h2416.md) of the [gowy](../../strongs/h/h1471.md): both the [qā'aṯ](../../strongs/h/h6893.md) and the [qipōḏ](../../strongs/h/h7090.md) shall [lûn](../../strongs/h/h3885.md) in the [kap̄tôr](../../strongs/h/h3730.md) of it; their [qowl](../../strongs/h/h6963.md) shall [shiyr](../../strongs/h/h7891.md) in the [ḥallôn](../../strongs/h/h2474.md); [ḥōreḇ](../../strongs/h/h2721.md) shall be in the [caph](../../strongs/h/h5592.md): for he shall [ʿārâ](../../strongs/h/h6168.md) the ['arzâ](../../strongs/h/h731.md).

<a name="zephaniah_2_15"></a>Zephaniah 2:15

This is the [ʿallîz](../../strongs/h/h5947.md) [ʿîr](../../strongs/h/h5892.md) that [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md), that ['āmar](../../strongs/h/h559.md) in her [lebab](../../strongs/h/h3824.md), I am, and there is none ['ep̄es](../../strongs/h/h657.md) me: how is she become a [šammâ](../../strongs/h/h8047.md), a place for [chay](../../strongs/h/h2416.md) to [marbēṣ](../../strongs/h/h4769.md)! every one that ['abar](../../strongs/h/h5674.md) her shall [šāraq](../../strongs/h/h8319.md), and [nûaʿ](../../strongs/h/h5128.md) his [yad](../../strongs/h/h3027.md).

---

[Transliteral Bible](../bible.md)

[Zephaniah](zephaniah.md)

[Zephaniah 1](zephaniah_1.md) - [Zephaniah 3](zephaniah_3.md)