# [Zephaniah 3](https://www.blueletterbible.org/kjv/zephaniah/3)

<a name="zephaniah_3_1"></a>Zephaniah 3:1

[hôy](../../strongs/h/h1945.md) to her that is [mārā'](../../strongs/h/h4754.md) and [gā'al](../../strongs/h/h1351.md), to the [yānâ](../../strongs/h/h3238.md) [ʿîr](../../strongs/h/h5892.md)!

<a name="zephaniah_3_2"></a>Zephaniah 3:2

She [shama'](../../strongs/h/h8085.md) not the [qowl](../../strongs/h/h6963.md); she [laqach](../../strongs/h/h3947.md) not [mûsār](../../strongs/h/h4148.md); she [batach](../../strongs/h/h982.md) not in [Yĕhovah](../../strongs/h/h3068.md); she drew not [qāraḇ](../../strongs/h/h7126.md) to her ['Elohiym](../../strongs/h/h430.md).

<a name="zephaniah_3_3"></a>Zephaniah 3:3

Her [śar](../../strongs/h/h8269.md) [qereḇ](../../strongs/h/h7130.md) her are [šā'aḡ](../../strongs/h/h7580.md) ['ariy](../../strongs/h/h738.md); her [shaphat](../../strongs/h/h8199.md) are ['ereb](../../strongs/h/h6153.md) [zᵊ'ēḇ](../../strongs/h/h2061.md); they [gāram](../../strongs/h/h1633.md) not till the [boqer](../../strongs/h/h1242.md).

<a name="zephaniah_3_4"></a>Zephaniah 3:4

Her [nāḇî'](../../strongs/h/h5030.md) are [pāḥaz](../../strongs/h/h6348.md) and [bōḡḏôṯ](../../strongs/h/h900.md) ['enowsh](../../strongs/h/h582.md): her [kōhēn](../../strongs/h/h3548.md) have [ḥālal](../../strongs/h/h2490.md) the [qodesh](../../strongs/h/h6944.md), they have done [ḥāmas](../../strongs/h/h2554.md) to the [towrah](../../strongs/h/h8451.md).

<a name="zephaniah_3_5"></a>Zephaniah 3:5

The [tsaddiyq](../../strongs/h/h6662.md) [Yĕhovah](../../strongs/h/h3068.md) is in the [qereḇ](../../strongs/h/h7130.md) thereof; he will not ['asah](../../strongs/h/h6213.md) ['evel](../../strongs/h/h5766.md): [boqer](../../strongs/h/h1242.md) [boqer](../../strongs/h/h1242.md) doth he [nathan](../../strongs/h/h5414.md) his [mishpat](../../strongs/h/h4941.md) to ['owr](../../strongs/h/h216.md), he [ʿāḏar](../../strongs/h/h5737.md) not; but the [ʿaûāl](../../strongs/h/h5767.md) [yada'](../../strongs/h/h3045.md) no [bšeṯ](../../strongs/h/h1322.md).

<a name="zephaniah_3_6"></a>Zephaniah 3:6

I have [karath](../../strongs/h/h3772.md) the [gowy](../../strongs/h/h1471.md): their [pinnâ](../../strongs/h/h6438.md) are [šāmēm](../../strongs/h/h8074.md); I made their [ḥûṣ](../../strongs/h/h2351.md) [ḥāraḇ](../../strongs/h/h2717.md), that none ['abar](../../strongs/h/h5674.md): their [ʿîr](../../strongs/h/h5892.md) are [ṣāḏâ](../../strongs/h/h6658.md), so that there is no ['iysh](../../strongs/h/h376.md), that there is none [yashab](../../strongs/h/h3427.md).

<a name="zephaniah_3_7"></a>Zephaniah 3:7

I ['āmar](../../strongs/h/h559.md), Surely thou wilt [yare'](../../strongs/h/h3372.md) me, thou wilt [laqach](../../strongs/h/h3947.md) [mûsār](../../strongs/h/h4148.md); so their [māʿôn](../../strongs/h/h4583.md) should not be [karath](../../strongs/h/h3772.md), howsoever I [paqad](../../strongs/h/h6485.md) them: ['āḵēn](../../strongs/h/h403.md) they [šāḵam](../../strongs/h/h7925.md), and [shachath](../../strongs/h/h7843.md) all their ['aliylah](../../strongs/h/h5949.md).

<a name="zephaniah_3_8"></a>Zephaniah 3:8

Therefore [ḥāḵâ](../../strongs/h/h2442.md) ye upon me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), until the [yowm](../../strongs/h/h3117.md) that I [quwm](../../strongs/h/h6965.md) to the [ʿaḏ](../../strongs/h/h5706.md): for my [mishpat](../../strongs/h/h4941.md) is to ['āsap̄](../../strongs/h/h622.md) the [gowy](../../strongs/h/h1471.md), that I may [qāḇaṣ](../../strongs/h/h6908.md) the [mamlāḵâ](../../strongs/h/h4467.md), to [šāp̄aḵ](../../strongs/h/h8210.md) upon them mine [zaʿam](../../strongs/h/h2195.md), even all my [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md): for all the ['erets](../../strongs/h/h776.md) shall be ['akal](../../strongs/h/h398.md) with the ['esh](../../strongs/h/h784.md) of my [qin'â](../../strongs/h/h7068.md). [^1]

<a name="zephaniah_3_9"></a>Zephaniah 3:9

For then will I [hāp̄aḵ](../../strongs/h/h2015.md) to the ['am](../../strongs/h/h5971.md) a [bārar](../../strongs/h/h1305.md) [saphah](../../strongs/h/h8193.md), that they may all [qara'](../../strongs/h/h7121.md) upon the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), to ['abad](../../strongs/h/h5647.md) him with one [šᵊḵem](../../strongs/h/h7926.md).

<a name="zephaniah_3_10"></a>Zephaniah 3:10

From [ʿēḇer](../../strongs/h/h5676.md) the [nāhār](../../strongs/h/h5104.md) of [Kûš](../../strongs/h/h3568.md) my [ʿāṯār](../../strongs/h/h6282.md), even the [bath](../../strongs/h/h1323.md) of my [puwts](../../strongs/h/h6327.md), shall [yāḇal](../../strongs/h/h2986.md) mine [minchah](../../strongs/h/h4503.md).

<a name="zephaniah_3_11"></a>Zephaniah 3:11

In that [yowm](../../strongs/h/h3117.md) shalt thou not be [buwsh](../../strongs/h/h954.md) for all thy ['aliylah](../../strongs/h/h5949.md), wherein thou hast [pāšaʿ](../../strongs/h/h6586.md) against me: for then I will [cuwr](../../strongs/h/h5493.md) out of the [qereḇ](../../strongs/h/h7130.md) of thee them that [ʿallîz](../../strongs/h/h5947.md) in thy [ga'avah](../../strongs/h/h1346.md), and thou shalt no more be [gāḇah](../../strongs/h/h1361.md) because of my [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md).

<a name="zephaniah_3_12"></a>Zephaniah 3:12

I will also [šā'ar](../../strongs/h/h7604.md) in the [qereḇ](../../strongs/h/h7130.md) of thee an ['aniy](../../strongs/h/h6041.md) and [dal](../../strongs/h/h1800.md) ['am](../../strongs/h/h5971.md), and they shall [chacah](../../strongs/h/h2620.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="zephaniah_3_13"></a>Zephaniah 3:13

The [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yisra'el](../../strongs/h/h3478.md) shall not ['asah](../../strongs/h/h6213.md) ['evel](../../strongs/h/h5766.md), nor [dabar](../../strongs/h/h1696.md) [kazab](../../strongs/h/h3577.md); neither shall a [tārmâ](../../strongs/h/h8649.md) [lashown](../../strongs/h/h3956.md) be [māṣā'](../../strongs/h/h4672.md) in their [peh](../../strongs/h/h6310.md): for they shall [ra'ah](../../strongs/h/h7462.md) and [rāḇaṣ](../../strongs/h/h7257.md), and none shall make them [ḥārēḏ](../../strongs/h/h2729.md).

<a name="zephaniah_3_14"></a>Zephaniah 3:14

[ranan](../../strongs/h/h7442.md), O [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md); [rûaʿ](../../strongs/h/h7321.md), O [Yisra'el](../../strongs/h/h3478.md); be [samach](../../strongs/h/h8055.md) and [ʿālaz](../../strongs/h/h5937.md) with all the [leb](../../strongs/h/h3820.md), O [bath](../../strongs/h/h1323.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="zephaniah_3_15"></a>Zephaniah 3:15

[Yĕhovah](../../strongs/h/h3068.md) hath [cuwr](../../strongs/h/h5493.md) thy [mishpat](../../strongs/h/h4941.md), he hath [panah](../../strongs/h/h6437.md) thine ['oyeb](../../strongs/h/h341.md): the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), even [Yĕhovah](../../strongs/h/h3068.md), is in the [qereḇ](../../strongs/h/h7130.md) of thee: thou shalt not [ra'ah](../../strongs/h/h7200.md) [ra'](../../strongs/h/h7451.md) any more.

<a name="zephaniah_3_16"></a>Zephaniah 3:16

In that [yowm](../../strongs/h/h3117.md) it shall be ['āmar](../../strongs/h/h559.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), [yare'](../../strongs/h/h3372.md) thou not: and to [Tsiyown](../../strongs/h/h6726.md), Let not thine [yad](../../strongs/h/h3027.md) be [rāp̄â](../../strongs/h/h7503.md).

<a name="zephaniah_3_17"></a>Zephaniah 3:17

[Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) in the [qereḇ](../../strongs/h/h7130.md) of thee is [gibôr](../../strongs/h/h1368.md); he will [yasha'](../../strongs/h/h3467.md), he will [śûś](../../strongs/h/h7797.md) over thee with [simchah](../../strongs/h/h8057.md); he will [ḥāraš](../../strongs/h/h2790.md) in his ['ahăḇâ](../../strongs/h/h160.md), he will [giyl](../../strongs/h/h1523.md) over thee with [rinnah](../../strongs/h/h7440.md).

<a name="zephaniah_3_18"></a>Zephaniah 3:18

I will ['āsap̄](../../strongs/h/h622.md) them that are [yāḡâ](../../strongs/h/h3013.md) for the [môʿēḏ](../../strongs/h/h4150.md), who are of thee, to whom the [cherpah](../../strongs/h/h2781.md) of it was a [maśśᵊ'ēṯ](../../strongs/h/h4864.md).

<a name="zephaniah_3_19"></a>Zephaniah 3:19

Behold, at that [ʿēṯ](../../strongs/h/h6256.md) I will ['asah](../../strongs/h/h6213.md) all that [ʿānâ](../../strongs/h/h6031.md) thee: and I will [yasha'](../../strongs/h/h3467.md) her that [ṣālaʿ](../../strongs/h/h6760.md), and [qāḇaṣ](../../strongs/h/h6908.md) her that was [nāḏaḥ](../../strongs/h/h5080.md); and I will [śûm](../../strongs/h/h7760.md) them [tehillah](../../strongs/h/h8416.md) and [shem](../../strongs/h/h8034.md) in every ['erets](../../strongs/h/h776.md) where they have been put to [bšeṯ](../../strongs/h/h1322.md).

<a name="zephaniah_3_20"></a>Zephaniah 3:20

At that [ʿēṯ](../../strongs/h/h6256.md) will I [bow'](../../strongs/h/h935.md) you again, even in the [ʿēṯ](../../strongs/h/h6256.md) that I [qāḇaṣ](../../strongs/h/h6908.md) you: for I will [nathan](../../strongs/h/h5414.md) you a [shem](../../strongs/h/h8034.md) and a [tehillah](../../strongs/h/h8416.md) among all ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), when I [shuwb](../../strongs/h/h7725.md) your [shebuwth](../../strongs/h/h7622.md) before your ['ayin](../../strongs/h/h5869.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Zephaniah](zephaniah.md)

[Zephaniah 2](zephaniah_2.md)

---

[^1]: [Zephaniah 3:8 Commentary](../../commentary/zephaniah/zephaniah_3_commentary.md#zephaniah_3_8)