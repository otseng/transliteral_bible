# [Ezekiel 33](https://www.blueletterbible.org/kjv/ezekiel/33)

<a name="ezekiel_33_1"></a>Ezekiel 33:1

Again the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_33_2"></a>Ezekiel 33:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [dabar](../../strongs/h/h1696.md) to the [ben](../../strongs/h/h1121.md) of thy ['am](../../strongs/h/h5971.md), and ['āmar](../../strongs/h/h559.md) unto them, When I [bow'](../../strongs/h/h935.md) the [chereb](../../strongs/h/h2719.md) upon an ['erets](../../strongs/h/h776.md), if the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) [laqach](../../strongs/h/h3947.md) an ['iysh](../../strongs/h/h376.md) of their [qāṣê](../../strongs/h/h7097.md), and [nathan](../../strongs/h/h5414.md) him for their [tsaphah](../../strongs/h/h6822.md):

<a name="ezekiel_33_3"></a>Ezekiel 33:3

If when he [ra'ah](../../strongs/h/h7200.md) the [chereb](../../strongs/h/h2719.md) [bow'](../../strongs/h/h935.md) upon the ['erets](../../strongs/h/h776.md), he [tāqaʿ](../../strongs/h/h8628.md) the [šôp̄ār](../../strongs/h/h7782.md), and [zāhar](../../strongs/h/h2094.md) the ['am](../../strongs/h/h5971.md);

<a name="ezekiel_33_4"></a>Ezekiel 33:4

Then [shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md), and taketh not [zāhar](../../strongs/h/h2094.md); if the [chereb](../../strongs/h/h2719.md) [bow'](../../strongs/h/h935.md), and take him [laqach](../../strongs/h/h3947.md), his [dam](../../strongs/h/h1818.md) shall be upon his own [ro'sh](../../strongs/h/h7218.md).

<a name="ezekiel_33_5"></a>Ezekiel 33:5

He [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md), and [zāhar](../../strongs/h/h2094.md) not; his [dam](../../strongs/h/h1818.md) shall be upon him. But he that [zāhar](../../strongs/h/h2094.md) shall [mālaṭ](../../strongs/h/h4422.md) his [nephesh](../../strongs/h/h5315.md).

<a name="ezekiel_33_6"></a>Ezekiel 33:6

But if the [tsaphah](../../strongs/h/h6822.md) [ra'ah](../../strongs/h/h7200.md) the [chereb](../../strongs/h/h2719.md) [bow'](../../strongs/h/h935.md), and [tāqaʿ](../../strongs/h/h8628.md) not the [šôp̄ār](../../strongs/h/h7782.md), and the ['am](../../strongs/h/h5971.md) be not [zāhar](../../strongs/h/h2094.md); if the [chereb](../../strongs/h/h2719.md) [bow'](../../strongs/h/h935.md), and [laqach](../../strongs/h/h3947.md) any [nephesh](../../strongs/h/h5315.md) from among them, he is [laqach](../../strongs/h/h3947.md) in his ['avon](../../strongs/h/h5771.md); but his [dam](../../strongs/h/h1818.md) will I [darash](../../strongs/h/h1875.md) at the [tsaphah](../../strongs/h/h6822.md) [yad](../../strongs/h/h3027.md).

<a name="ezekiel_33_7"></a>Ezekiel 33:7

So thou, O [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), I have [nathan](../../strongs/h/h5414.md) thee a [tsaphah](../../strongs/h/h6822.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md); therefore thou shalt [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) at my [peh](../../strongs/h/h6310.md), and [zāhar](../../strongs/h/h2094.md) them from me.

<a name="ezekiel_33_8"></a>Ezekiel 33:8

When I ['āmar](../../strongs/h/h559.md) unto the [rasha'](../../strongs/h/h7563.md), O [rasha'](../../strongs/h/h7563.md) man, thou shalt [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md); if thou dost not [dabar](../../strongs/h/h1696.md) to [zāhar](../../strongs/h/h2094.md) the [rasha'](../../strongs/h/h7563.md) from his [derek](../../strongs/h/h1870.md), that [rasha'](../../strongs/h/h7563.md) man shall [muwth](../../strongs/h/h4191.md) in his ['avon](../../strongs/h/h5771.md); but his [dam](../../strongs/h/h1818.md) will I [bāqaš](../../strongs/h/h1245.md) at thine [yad](../../strongs/h/h3027.md).

<a name="ezekiel_33_9"></a>Ezekiel 33:9

Nevertheless, if thou [zāhar](../../strongs/h/h2094.md) the [rasha'](../../strongs/h/h7563.md) of his [derek](../../strongs/h/h1870.md) to [shuwb](../../strongs/h/h7725.md) from it; if he do not [shuwb](../../strongs/h/h7725.md) from his [derek](../../strongs/h/h1870.md), he shall [muwth](../../strongs/h/h4191.md) in his ['avon](../../strongs/h/h5771.md); but thou hast [natsal](../../strongs/h/h5337.md) thy [nephesh](../../strongs/h/h5315.md).

<a name="ezekiel_33_10"></a>Ezekiel 33:10

Therefore, O thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), ['āmar](../../strongs/h/h559.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md); Thus ye ['āmar](../../strongs/h/h559.md), ['āmar](../../strongs/h/h559.md), If our [pesha'](../../strongs/h/h6588.md) and our [chatta'ath](../../strongs/h/h2403.md) be upon us, and we [māqaq](../../strongs/h/h4743.md) in them, how should we then [ḥāyâ](../../strongs/h/h2421.md)?

<a name="ezekiel_33_11"></a>Ezekiel 33:11

['āmar](../../strongs/h/h559.md) unto them, As I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), I have no [ḥāp̄ēṣ](../../strongs/h/h2654.md) in the [maveth](../../strongs/h/h4194.md) of the [rasha'](../../strongs/h/h7563.md); but that the [rasha'](../../strongs/h/h7563.md) [shuwb](../../strongs/h/h7725.md) from his [derek](../../strongs/h/h1870.md) and [ḥāyâ](../../strongs/h/h2421.md): [shuwb](../../strongs/h/h7725.md) ye, [shuwb](../../strongs/h/h7725.md) ye from your [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md); for why will ye [muwth](../../strongs/h/h4191.md), O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="ezekiel_33_12"></a>Ezekiel 33:12

Therefore, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of thy ['am](../../strongs/h/h5971.md), The [tsedaqah](../../strongs/h/h6666.md) of the [tsaddiyq](../../strongs/h/h6662.md) shall not [natsal](../../strongs/h/h5337.md) him in the [yowm](../../strongs/h/h3117.md) of his [pesha'](../../strongs/h/h6588.md): as for the [rišʿâ](../../strongs/h/h7564.md) of the [rasha'](../../strongs/h/h7563.md), he shall not [kashal](../../strongs/h/h3782.md) thereby in the [yowm](../../strongs/h/h3117.md) that he [shuwb](../../strongs/h/h7725.md) from his [resha'](../../strongs/h/h7562.md); neither shall the [tsaddiyq](../../strongs/h/h6662.md) be [yakol](../../strongs/h/h3201.md) to [ḥāyâ](../../strongs/h/h2421.md) for his in the [yowm](../../strongs/h/h3117.md) that he [chata'](../../strongs/h/h2398.md).

<a name="ezekiel_33_13"></a>Ezekiel 33:13

When I shall ['āmar](../../strongs/h/h559.md) to the [tsaddiyq](../../strongs/h/h6662.md), that he shall [ḥāyâ](../../strongs/h/h2421.md) [ḥāyâ](../../strongs/h/h2421.md); if he [batach](../../strongs/h/h982.md) to his own [tsedaqah](../../strongs/h/h6666.md), and ['asah](../../strongs/h/h6213.md) ['evel](../../strongs/h/h5766.md), all his [tsedaqah](../../strongs/h/h6666.md) shall not be [zakar](../../strongs/h/h2142.md); but for his ['evel](../../strongs/h/h5766.md) that he hath ['asah](../../strongs/h/h6213.md), he shall [muwth](../../strongs/h/h4191.md) for it.

<a name="ezekiel_33_14"></a>Ezekiel 33:14

Again, when I ['āmar](../../strongs/h/h559.md) unto the [rasha'](../../strongs/h/h7563.md), Thou shalt [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md); if he [shuwb](../../strongs/h/h7725.md) from his [chatta'ath](../../strongs/h/h2403.md), and ['asah](../../strongs/h/h6213.md) that which is [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md);

<a name="ezekiel_33_15"></a>Ezekiel 33:15

If the [rasha'](../../strongs/h/h7563.md) [shuwb](../../strongs/h/h7725.md) the [ḥăḇōl](../../strongs/h/h2258.md), [shalam](../../strongs/h/h7999.md) that he had [gᵊzēlâ](../../strongs/h/h1500.md), [halak](../../strongs/h/h1980.md) in the [chuqqah](../../strongs/h/h2708.md) of [chay](../../strongs/h/h2416.md), without ['asah](../../strongs/h/h6213.md) ['evel](../../strongs/h/h5766.md); he shall [ḥāyâ](../../strongs/h/h2421.md) [ḥāyâ](../../strongs/h/h2421.md), he shall not [muwth](../../strongs/h/h4191.md).

<a name="ezekiel_33_16"></a>Ezekiel 33:16

None of his [chatta'ath](../../strongs/h/h2403.md) that he hath [chata'](../../strongs/h/h2398.md) shall be [zakar](../../strongs/h/h2142.md) unto him: he hath ['asah](../../strongs/h/h6213.md) that which is [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md); he shall [ḥāyâ](../../strongs/h/h2421.md) [ḥāyâ](../../strongs/h/h2421.md).

<a name="ezekiel_33_17"></a>Ezekiel 33:17

Yet the [ben](../../strongs/h/h1121.md) of thy ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md), The [derek](../../strongs/h/h1870.md) of the ['adonay](../../strongs/h/h136.md) is not [tāḵan](../../strongs/h/h8505.md): but as for them, their [derek](../../strongs/h/h1870.md) is not [tāḵan](../../strongs/h/h8505.md).

<a name="ezekiel_33_18"></a>Ezekiel 33:18

When the [tsaddiyq](../../strongs/h/h6662.md) [shuwb](../../strongs/h/h7725.md) from his [tsedaqah](../../strongs/h/h6666.md), and ['asah](../../strongs/h/h6213.md) ['evel](../../strongs/h/h5766.md), he shall even [muwth](../../strongs/h/h4191.md) thereby.

<a name="ezekiel_33_19"></a>Ezekiel 33:19

But if the [rasha'](../../strongs/h/h7563.md) [shuwb](../../strongs/h/h7725.md) from his [rišʿâ](../../strongs/h/h7564.md), and ['asah](../../strongs/h/h6213.md) that which is [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md), he shall [ḥāyâ](../../strongs/h/h2421.md) thereby.

<a name="ezekiel_33_20"></a>Ezekiel 33:20

Yet ye ['āmar](../../strongs/h/h559.md), The [derek](../../strongs/h/h1870.md) of the ['adonay](../../strongs/h/h136.md) is not [tāḵan](../../strongs/h/h8505.md). O ye [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), I will [shaphat](../../strongs/h/h8199.md) you every ['iysh](../../strongs/h/h376.md) after his [derek](../../strongs/h/h1870.md).

<a name="ezekiel_33_21"></a>Ezekiel 33:21

And it came to pass in the twelfth [šānâ](../../strongs/h/h8141.md) of our [gālûṯ](../../strongs/h/h1546.md), in the tenth month, in the fifth day of the [ḥōḏeš](../../strongs/h/h2320.md), that one that had [pālîṭ](../../strongs/h/h6412.md) out of [Yĕruwshalaim](../../strongs/h/h3389.md) [bow'](../../strongs/h/h935.md) unto me, ['āmar](../../strongs/h/h559.md), The [ʿîr](../../strongs/h/h5892.md) is [nakah](../../strongs/h/h5221.md).

<a name="ezekiel_33_22"></a>Ezekiel 33:22

Now the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) was upon me in the ['ereb](../../strongs/h/h6153.md), [paniym](../../strongs/h/h6440.md) he that was [pālîṭ](../../strongs/h/h6412.md) [bow'](../../strongs/h/h935.md); and had [pāṯaḥ](../../strongs/h/h6605.md) my [peh](../../strongs/h/h6310.md), until he [bow'](../../strongs/h/h935.md) to me in the [boqer](../../strongs/h/h1242.md); and my [peh](../../strongs/h/h6310.md) was [pāṯaḥ](../../strongs/h/h6605.md), and I was no more ['ālam](../../strongs/h/h481.md).

<a name="ezekiel_33_23"></a>Ezekiel 33:23

Then the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_33_24"></a>Ezekiel 33:24

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), they that [yashab](../../strongs/h/h3427.md) those [chorbah](../../strongs/h/h2723.md) of the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md), ['āmar](../../strongs/h/h559.md), ['Abraham](../../strongs/h/h85.md) was one, and he [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md): but we are [rab](../../strongs/h/h7227.md); the ['erets](../../strongs/h/h776.md) is [nathan](../../strongs/h/h5414.md) us for [môrāšâ](../../strongs/h/h4181.md).

<a name="ezekiel_33_25"></a>Ezekiel 33:25

Wherefore ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Ye ['akal](../../strongs/h/h398.md) with the [dam](../../strongs/h/h1818.md), and [nasa'](../../strongs/h/h5375.md) your ['ayin](../../strongs/h/h5869.md) toward your [gillûl](../../strongs/h/h1544.md), and [šāp̄aḵ](../../strongs/h/h8210.md) [dam](../../strongs/h/h1818.md): and shall ye [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md)?

<a name="ezekiel_33_26"></a>Ezekiel 33:26

Ye ['amad](../../strongs/h/h5975.md) upon your [chereb](../../strongs/h/h2719.md), ye ['asah](../../strongs/h/h6213.md) [tôʿēḇâ](../../strongs/h/h8441.md), and ye [ṭāmē'](../../strongs/h/h2930.md) every ['iysh](../../strongs/h/h376.md) his [rea'](../../strongs/h/h7453.md) ['ishshah](../../strongs/h/h802.md): and shall ye [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md)?

<a name="ezekiel_33_27"></a>Ezekiel 33:27

['āmar](../../strongs/h/h559.md) thou thus unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); As I [chay](../../strongs/h/h2416.md), surely they that are in the [chorbah](../../strongs/h/h2723.md) shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md), and him that is in the [paniym](../../strongs/h/h6440.md) [sadeh](../../strongs/h/h7704.md) will I [nathan](../../strongs/h/h5414.md) to the [chay](../../strongs/h/h2416.md) to be ['akal](../../strongs/h/h398.md), and they that be in the [mᵊṣāḏ](../../strongs/h/h4679.md) and in the [mᵊʿārâ](../../strongs/h/h4631.md) shall [muwth](../../strongs/h/h4191.md) of the [deḇer](../../strongs/h/h1698.md).

<a name="ezekiel_33_28"></a>Ezekiel 33:28

For I will [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) [mᵊšammâ](../../strongs/h/h4923.md) [šᵊmāmâ](../../strongs/h/h8077.md), and the [gā'ôn](../../strongs/h/h1347.md) of her ['oz](../../strongs/h/h5797.md) shall [shabath](../../strongs/h/h7673.md); and the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md) shall be [šāmēm](../../strongs/h/h8074.md), that none shall ['abar](../../strongs/h/h5674.md).

<a name="ezekiel_33_29"></a>Ezekiel 33:29

Then shall they [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when I have [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) [mᵊšammâ](../../strongs/h/h4923.md) [šᵊmāmâ](../../strongs/h/h8077.md) because of all their [tôʿēḇâ](../../strongs/h/h8441.md) which they have ['asah](../../strongs/h/h6213.md).

<a name="ezekiel_33_30"></a>Ezekiel 33:30

Also, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), the [ben](../../strongs/h/h1121.md) of thy ['am](../../strongs/h/h5971.md) still are [dabar](../../strongs/h/h1696.md) against thee by the [qîr](../../strongs/h/h7023.md) and in the [peṯaḥ](../../strongs/h/h6607.md) of the [bayith](../../strongs/h/h1004.md), and [dabar](../../strongs/h/h1696.md) [ḥaḏ](../../strongs/h/h2297.md) to another, every ['iysh](../../strongs/h/h376.md) to his ['ach](../../strongs/h/h251.md), ['āmar](../../strongs/h/h559.md), [bow'](../../strongs/h/h935.md), I pray you, and [shama'](../../strongs/h/h8085.md) what is the [dabar](../../strongs/h/h1697.md) that [yāṣā'](../../strongs/h/h3318.md) from [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_33_31"></a>Ezekiel 33:31

And they [bow'](../../strongs/h/h935.md) unto thee as the ['am](../../strongs/h/h5971.md) [māḇô'](../../strongs/h/h3996.md), and they [yashab](../../strongs/h/h3427.md) [paniym](../../strongs/h/h6440.md) thee as my ['am](../../strongs/h/h5971.md), and they [shama'](../../strongs/h/h8085.md) thy [dabar](../../strongs/h/h1697.md), but they will not ['asah](../../strongs/h/h6213.md) them: for with their [peh](../../strongs/h/h6310.md) they ['asah](../../strongs/h/h6213.md) much [ʿăḡāḇîm](../../strongs/h/h5690.md), but their [leb](../../strongs/h/h3820.md) [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) their [beṣaʿ](../../strongs/h/h1215.md).

<a name="ezekiel_33_32"></a>Ezekiel 33:32

And, lo, thou art unto them as a very [ʿăḡāḇîm](../../strongs/h/h5690.md) [šîr](../../strongs/h/h7892.md) of one that hath a [yāp̄ê](../../strongs/h/h3303.md) [qowl](../../strongs/h/h6963.md), and can [ṭôḇ](../../strongs/h/h2895.md) on a [nāḡan](../../strongs/h/h5059.md): for they [shama'](../../strongs/h/h8085.md) thy [dabar](../../strongs/h/h1697.md), but they ['asah](../../strongs/h/h6213.md) them not.

<a name="ezekiel_33_33"></a>Ezekiel 33:33

And when this cometh to [bow'](../../strongs/h/h935.md), (lo, it will [bow'](../../strongs/h/h935.md),) then shall they [yada'](../../strongs/h/h3045.md) that a [nāḇî'](../../strongs/h/h5030.md) hath been [tavek](../../strongs/h/h8432.md) them.

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 32](ezekiel_32.md) - [Ezekiel 34](ezekiel_34.md)