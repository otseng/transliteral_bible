# [Ezekiel 13](https://www.blueletterbible.org/kjv/ezekiel/13)

<a name="ezekiel_13_1"></a>Ezekiel 13:1

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_13_2"></a>Ezekiel 13:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nāḇā'](../../strongs/h/h5012.md) against the [nāḇî'](../../strongs/h/h5030.md) of [Yisra'el](../../strongs/h/h3478.md) that [nāḇā'](../../strongs/h/h5012.md), and ['āmar](../../strongs/h/h559.md) thou unto them that [nāḇî'](../../strongs/h/h5030.md) out of their own [leb](../../strongs/h/h3820.md), [shama'](../../strongs/h/h8085.md) ye the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md);

<a name="ezekiel_13_3"></a>Ezekiel 13:3

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); [hôy](../../strongs/h/h1945.md) unto the [nabal](../../strongs/h/h5036.md) [nāḇî'](../../strongs/h/h5030.md), that [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) their own [ruwach](../../strongs/h/h7307.md), and have [ra'ah](../../strongs/h/h7200.md) nothing!

<a name="ezekiel_13_4"></a>Ezekiel 13:4

O [Yisra'el](../../strongs/h/h3478.md), thy [nāḇî'](../../strongs/h/h5030.md) are like the [šûʿāl](../../strongs/h/h7776.md) in the [chorbah](../../strongs/h/h2723.md).

<a name="ezekiel_13_5"></a>Ezekiel 13:5

Ye have not [ʿālâ](../../strongs/h/h5927.md) into the [pereṣ](../../strongs/h/h6556.md), neither made [gāḏar](../../strongs/h/h1443.md) the [gāḏēr](../../strongs/h/h1447.md) for the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) to ['amad](../../strongs/h/h5975.md) in the [milḥāmâ](../../strongs/h/h4421.md) in the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_13_6"></a>Ezekiel 13:6

They have [chazah](../../strongs/h/h2372.md) [shav'](../../strongs/h/h7723.md) and [kazab](../../strongs/h/h3577.md) [qesem](../../strongs/h/h7081.md), ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [nᵊ'um](../../strongs/h/h5002.md): and [Yĕhovah](../../strongs/h/h3068.md) hath not [shalach](../../strongs/h/h7971.md) them: and they have made others to [yāḥal](../../strongs/h/h3176.md) that they would [quwm](../../strongs/h/h6965.md) the [dabar](../../strongs/h/h1697.md).

<a name="ezekiel_13_7"></a>Ezekiel 13:7

Have ye not [chazah](../../strongs/h/h2372.md) a [shav'](../../strongs/h/h7723.md) [maḥăzê](../../strongs/h/h4236.md), and have ye not [dabar](../../strongs/h/h1696.md) a [kazab](../../strongs/h/h3577.md) [miqsām](../../strongs/h/h4738.md), whereas ye ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [nᵊ'um](../../strongs/h/h5002.md) it; albeit I have not ['āmar](../../strongs/h/h559.md)?

<a name="ezekiel_13_8"></a>Ezekiel 13:8

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because ye have [dabar](../../strongs/h/h1696.md) [shav'](../../strongs/h/h7723.md), and [chazah](../../strongs/h/h2372.md) [kazab](../../strongs/h/h3577.md), therefore, behold, I am against you, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_13_9"></a>Ezekiel 13:9

And mine [yad](../../strongs/h/h3027.md) shall be upon the [nāḇî'](../../strongs/h/h5030.md) that [ḥōzê](../../strongs/h/h2374.md) [shav'](../../strongs/h/h7723.md), and that [qāsam](../../strongs/h/h7080.md) [kazab](../../strongs/h/h3577.md): they shall not be in the [sôḏ](../../strongs/h/h5475.md) of my ['am](../../strongs/h/h5971.md), neither shall they be [kāṯaḇ](../../strongs/h/h3789.md) in the [kᵊṯāḇ](../../strongs/h/h3791.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), neither shall they [bow'](../../strongs/h/h935.md) into the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md); and ye shall [yada'](../../strongs/h/h3045.md) that I am the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_13_10"></a>Ezekiel 13:10

Because, even because they have [ṭāʿâ](../../strongs/h/h2937.md) my ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), [shalowm](../../strongs/h/h7965.md); and there was no [shalowm](../../strongs/h/h7965.md); and one built [bānâ](../../strongs/h/h1129.md) a [ḥayiṣ](../../strongs/h/h2434.md), and, [hinneh](../../strongs/h/h2009.md), others [ṭûaḥ](../../strongs/h/h2902.md) it with [tāp̄ēl](../../strongs/h/h8602.md) morter:

<a name="ezekiel_13_11"></a>Ezekiel 13:11

['āmar](../../strongs/h/h559.md) unto them which [ṭûaḥ](../../strongs/h/h2902.md) it with [tāp̄ēl](../../strongs/h/h8602.md), that it shall [naphal](../../strongs/h/h5307.md): there shall be a [šāṭap̄](../../strongs/h/h7857.md) [gešem](../../strongs/h/h1653.md); and ye, ['elgāḇîš](../../strongs/h/h417.md) ['eben](../../strongs/h/h68.md), shall [naphal](../../strongs/h/h5307.md); and a [saʿar](../../strongs/h/h5591.md) [ruwach](../../strongs/h/h7307.md) shall [bāqaʿ](../../strongs/h/h1234.md) it.

<a name="ezekiel_13_12"></a>Ezekiel 13:12

Lo, when the [qîr](../../strongs/h/h7023.md) is [naphal](../../strongs/h/h5307.md), shall it not be ['āmar](../../strongs/h/h559.md) unto you, Where is the [ṭîaḥ](../../strongs/h/h2915.md) wherewith ye have [ṭûaḥ](../../strongs/h/h2902.md) it?

<a name="ezekiel_13_13"></a>Ezekiel 13:13

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); I will even [bāqaʿ](../../strongs/h/h1234.md) it with a [saʿar](../../strongs/h/h5591.md) [ruwach](../../strongs/h/h7307.md) in my [chemah](../../strongs/h/h2534.md); and there shall be a [šāṭap̄](../../strongs/h/h7857.md) [gešem](../../strongs/h/h1653.md) in mine ['aph](../../strongs/h/h639.md), and ['elgāḇîš](../../strongs/h/h417.md) ['eben](../../strongs/h/h68.md) in my [chemah](../../strongs/h/h2534.md) to [kālâ](../../strongs/h/h3617.md) it.

<a name="ezekiel_13_14"></a>Ezekiel 13:14

So will I [harac](../../strongs/h/h2040.md) the [qîr](../../strongs/h/h7023.md) that ye have [ṭûaḥ](../../strongs/h/h2902.md) with [tāp̄ēl](../../strongs/h/h8602.md), and [naga'](../../strongs/h/h5060.md) it down to the ['erets](../../strongs/h/h776.md), so that the [yᵊsôḏ](../../strongs/h/h3247.md) thereof shall be [gālâ](../../strongs/h/h1540.md), and it shall [naphal](../../strongs/h/h5307.md), and ye shall be [kalah](../../strongs/h/h3615.md) in the midst thereof: and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_13_15"></a>Ezekiel 13:15

Thus will I [kalah](../../strongs/h/h3615.md) my [chemah](../../strongs/h/h2534.md) upon the [qîr](../../strongs/h/h7023.md), and upon them that have [ṭûaḥ](../../strongs/h/h2902.md) it with [tāp̄ēl](../../strongs/h/h8602.md), and will ['āmar](../../strongs/h/h559.md) unto you, The [qîr](../../strongs/h/h7023.md) is no more, neither they that [ṭûaḥ](../../strongs/h/h2902.md) it;

<a name="ezekiel_13_16"></a>Ezekiel 13:16

the [nāḇî'](../../strongs/h/h5030.md) of [Yisra'el](../../strongs/h/h3478.md) which [nāḇā'](../../strongs/h/h5012.md) concerning [Yĕruwshalaim](../../strongs/h/h3389.md), and which [ḥōzê](../../strongs/h/h2374.md) [ḥāzôn](../../strongs/h/h2377.md) of [shalowm](../../strongs/h/h7965.md) for her, and there is no [shalowm](../../strongs/h/h7965.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_13_17"></a>Ezekiel 13:17

Likewise, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [śûm](../../strongs/h/h7760.md) thy [paniym](../../strongs/h/h6440.md) against the [bath](../../strongs/h/h1323.md) of thy ['am](../../strongs/h/h5971.md), which [nāḇā'](../../strongs/h/h5012.md) out of their own [leb](../../strongs/h/h3820.md); and [nāḇā'](../../strongs/h/h5012.md) thou against them,

<a name="ezekiel_13_18"></a>Ezekiel 13:18

And ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); [hôy](../../strongs/h/h1945.md) to that [taphar](../../strongs/h/h8609.md) [keseṯ](../../strongs/h/h3704.md) to all ['aṣṣîl](../../strongs/h/h679.md) [yad](../../strongs/h/h3027.md), and ['asah](../../strongs/h/h6213.md) [mispāḥâ](../../strongs/h/h4555.md) upon the [ro'sh](../../strongs/h/h7218.md) of every [qômâ](../../strongs/h/h6967.md) to [ṣûḏ](../../strongs/h/h6679.md) [nephesh](../../strongs/h/h5315.md)! Will ye [ṣûḏ](../../strongs/h/h6679.md) the [nephesh](../../strongs/h/h5315.md) of my ['am](../../strongs/h/h5971.md), and will ye [ḥāyâ](../../strongs/h/h2421.md) the [nephesh](../../strongs/h/h5315.md) [ḥāyâ](../../strongs/h/h2421.md) unto you?

<a name="ezekiel_13_19"></a>Ezekiel 13:19

And will ye [ḥālal](../../strongs/h/h2490.md) me among my ['am](../../strongs/h/h5971.md) for [šōʿal](../../strongs/h/h8168.md) of [śᵊʿōrâ](../../strongs/h/h8184.md) and for [paṯ](../../strongs/h/h6595.md) of [lechem](../../strongs/h/h3899.md), to [muwth](../../strongs/h/h4191.md) the [nephesh](../../strongs/h/h5315.md) that should not [muwth](../../strongs/h/h4191.md), and to [ḥāyâ](../../strongs/h/h2421.md) the [nephesh](../../strongs/h/h5315.md) [ḥāyâ](../../strongs/h/h2421.md) that should not [ḥāyâ](../../strongs/h/h2421.md), by your [kāzaḇ](../../strongs/h/h3576.md) to my ['am](../../strongs/h/h5971.md) that [shama'](../../strongs/h/h8085.md) your [kazab](../../strongs/h/h3577.md)?

<a name="ezekiel_13_20"></a>Ezekiel 13:20

Wherefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I am against your [keseṯ](../../strongs/h/h3704.md), wherewith ye there [ṣûḏ](../../strongs/h/h6679.md) the [nephesh](../../strongs/h/h5315.md) to make them [pāraḥ](../../strongs/h/h6524.md), and I will [qāraʿ](../../strongs/h/h7167.md) them from your [zerowa'](../../strongs/h/h2220.md), and will let the [nephesh](../../strongs/h/h5315.md) [shalach](../../strongs/h/h7971.md), even the [nephesh](../../strongs/h/h5315.md) that ye [ṣûḏ](../../strongs/h/h6679.md) to make them [pāraḥ](../../strongs/h/h6524.md).

<a name="ezekiel_13_21"></a>Ezekiel 13:21

Your [mispāḥâ](../../strongs/h/h4555.md) also will I [qāraʿ](../../strongs/h/h7167.md), and [natsal](../../strongs/h/h5337.md) my ['am](../../strongs/h/h5971.md) out of your [yad](../../strongs/h/h3027.md), and they shall be no more in your [yad](../../strongs/h/h3027.md) to be [matsuwd](../../strongs/h/h4686.md); and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_13_22"></a>Ezekiel 13:22

Because with [sheqer](../../strongs/h/h8267.md) ye have made the [leb](../../strongs/h/h3820.md) of the [tsaddiyq](../../strongs/h/h6662.md) [kā'â](../../strongs/h/h3512.md), whom I have not made [kā'aḇ](../../strongs/h/h3510.md); and [ḥāzaq](../../strongs/h/h2388.md) the [yad](../../strongs/h/h3027.md) of the [ra'](../../strongs/h/h7451.md), that he should not [shuwb](../../strongs/h/h7725.md) from his [rasha'](../../strongs/h/h7563.md) [derek](../../strongs/h/h1870.md), by promising him [ḥāyâ](../../strongs/h/h2421.md):

<a name="ezekiel_13_23"></a>Ezekiel 13:23

Therefore ye shall [chazah](../../strongs/h/h2372.md) no more [shav'](../../strongs/h/h7723.md), nor [qāsam](../../strongs/h/h7080.md) [qesem](../../strongs/h/h7081.md): for I will [natsal](../../strongs/h/h5337.md) my ['am](../../strongs/h/h5971.md) out of your [yad](../../strongs/h/h3027.md): and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 12](ezekiel_12.md) - [Ezekiel 14](ezekiel_14.md)