# [Ezekiel 6](https://www.blueletterbible.org/kjv/ezekiel/6)

<a name="ezekiel_6_1"></a>Ezekiel 6:1

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_6_2"></a>Ezekiel 6:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [śûm](../../strongs/h/h7760.md) thy [paniym](../../strongs/h/h6440.md) toward the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md), and [nāḇā'](../../strongs/h/h5012.md) against them,

<a name="ezekiel_6_3"></a>Ezekiel 6:3

And ['āmar](../../strongs/h/h559.md), Ye [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) to the [har](../../strongs/h/h2022.md), and to the [giḇʿâ](../../strongs/h/h1389.md), to the ['āp̄îq](../../strongs/h/h650.md), and to the [gay'](../../strongs/h/h1516.md); Behold, I, even I, will [bow'](../../strongs/h/h935.md) a [chereb](../../strongs/h/h2719.md) upon you, and I will ['abad](../../strongs/h/h6.md) your [bāmâ](../../strongs/h/h1116.md).

<a name="ezekiel_6_4"></a>Ezekiel 6:4

And your [mizbeach](../../strongs/h/h4196.md) shall be [šāmēm](../../strongs/h/h8074.md), and your [ḥammān](../../strongs/h/h2553.md) shall be [shabar](../../strongs/h/h7665.md): and I will [naphal](../../strongs/h/h5307.md) your [ḥālāl](../../strongs/h/h2491.md) [paniym](../../strongs/h/h6440.md) your [gillûl](../../strongs/h/h1544.md).

<a name="ezekiel_6_5"></a>Ezekiel 6:5

And I will [nathan](../../strongs/h/h5414.md) the [peḡer](../../strongs/h/h6297.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [paniym](../../strongs/h/h6440.md) their [gillûl](../../strongs/h/h1544.md); and I will [zārâ](../../strongs/h/h2219.md) your ['etsem](../../strongs/h/h6106.md) [cabiyb](../../strongs/h/h5439.md) your [mizbeach](../../strongs/h/h4196.md).

<a name="ezekiel_6_6"></a>Ezekiel 6:6

In all your [môšāḇ](../../strongs/h/h4186.md) the [ʿîr](../../strongs/h/h5892.md) shall be [ḥāraḇ](../../strongs/h/h2717.md), and the [bāmâ](../../strongs/h/h1116.md) shall be [yāšam](../../strongs/h/h3456.md); that your [mizbeach](../../strongs/h/h4196.md) may be laid [ḥāraḇ](../../strongs/h/h2717.md) and made ['asham](../../strongs/h/h816.md), and your [gillûl](../../strongs/h/h1544.md) may be [shabar](../../strongs/h/h7665.md) and [shabath](../../strongs/h/h7673.md), and your [ḥammān](../../strongs/h/h2553.md) may be [gāḏaʿ](../../strongs/h/h1438.md), and your [ma'aseh](../../strongs/h/h4639.md) may be [māḥâ](../../strongs/h/h4229.md).

<a name="ezekiel_6_7"></a>Ezekiel 6:7

And the [ḥālāl](../../strongs/h/h2491.md) shall [naphal](../../strongs/h/h5307.md) in the midst of you, and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_6_8"></a>Ezekiel 6:8

Yet will I leave a [yāṯar](../../strongs/h/h3498.md), that ye may have some that shall [pālîṭ](../../strongs/h/h6412.md) the [chereb](../../strongs/h/h2719.md) among the [gowy](../../strongs/h/h1471.md), when ye shall be [zārâ](../../strongs/h/h2219.md) through the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_6_9"></a>Ezekiel 6:9

And they that [pālîṭ](../../strongs/h/h6412.md) of you shall [zakar](../../strongs/h/h2142.md) me among the [gowy](../../strongs/h/h1471.md) whither they shall be [šāḇâ](../../strongs/h/h7617.md), because I am [shabar](../../strongs/h/h7665.md) with their [zānâ](../../strongs/h/h2181.md) [leb](../../strongs/h/h3820.md), which hath [cuwr](../../strongs/h/h5493.md) from me, and with their ['ayin](../../strongs/h/h5869.md), which go a [zānâ](../../strongs/h/h2181.md) ['aḥar](../../strongs/h/h310.md) their [gillûl](../../strongs/h/h1544.md): and they shall [qûṭ](../../strongs/h/h6962.md) [paniym](../../strongs/h/h6440.md) for the [ra'](../../strongs/h/h7451.md) which they have ['asah](../../strongs/h/h6213.md) in all their [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="ezekiel_6_10"></a>Ezekiel 6:10

And they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), and that I have not [dabar](../../strongs/h/h1696.md) in [ḥinnām](../../strongs/h/h2600.md) that I would ['asah](../../strongs/h/h6213.md) this [ra'](../../strongs/h/h7451.md) unto them.

<a name="ezekiel_6_11"></a>Ezekiel 6:11

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); [nakah](../../strongs/h/h5221.md) with thine [kaph](../../strongs/h/h3709.md), and [rāqaʿ](../../strongs/h/h7554.md) with thy [regel](../../strongs/h/h7272.md), and ['āmar](../../strongs/h/h559.md), ['āḥ](../../strongs/h/h253.md) for all the [ra'](../../strongs/h/h7451.md) [tôʿēḇâ](../../strongs/h/h8441.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md)! for they shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md), by the [rāʿāḇ](../../strongs/h/h7458.md), and by the [deḇer](../../strongs/h/h1698.md).

<a name="ezekiel_6_12"></a>Ezekiel 6:12

He that is far [rachowq](../../strongs/h/h7350.md) shall [muwth](../../strongs/h/h4191.md) of the [deḇer](../../strongs/h/h1698.md); and he that is [qarowb](../../strongs/h/h7138.md) shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md); and he that [šā'ar](../../strongs/h/h7604.md) and is [nāṣar](../../strongs/h/h5341.md) shall [muwth](../../strongs/h/h4191.md) by the [rāʿāḇ](../../strongs/h/h7458.md): thus will I [kalah](../../strongs/h/h3615.md) my [chemah](../../strongs/h/h2534.md) upon them.

<a name="ezekiel_6_13"></a>Ezekiel 6:13

Then shall ye [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when their [ḥālāl](../../strongs/h/h2491.md) men shall be among their [gillûl](../../strongs/h/h1544.md) [cabiyb](../../strongs/h/h5439.md) their [mizbeach](../../strongs/h/h4196.md), upon every [ruwm](../../strongs/h/h7311.md) [giḇʿâ](../../strongs/h/h1389.md), in all the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md), and under every [raʿănān](../../strongs/h/h7488.md) ['ets](../../strongs/h/h6086.md), and under every [ʿāḇōṯ](../../strongs/h/h5687.md) ['ēlâ](../../strongs/h/h424.md), the [maqowm](../../strongs/h/h4725.md) where they did [nathan](../../strongs/h/h5414.md) [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) to all their [gillûl](../../strongs/h/h1544.md).

<a name="ezekiel_6_14"></a>Ezekiel 6:14

So will I [natah](../../strongs/h/h5186.md) my [yad](../../strongs/h/h3027.md) upon them, and [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) [šᵊmāmâ](../../strongs/h/h8077.md), yea, more [mᵊšammâ](../../strongs/h/h4923.md) than the [midbar](../../strongs/h/h4057.md) toward [Diḇlâ](../../strongs/h/h1689.md), in all their [môšāḇ](../../strongs/h/h4186.md): and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 5](ezekiel_5.md) - [Ezekiel 7](ezekiel_7.md)