# [Ezekiel 38](https://www.blueletterbible.org/kjv/ezekiel/38)

<a name="ezekiel_38_1"></a>Ezekiel 38:1

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_38_2"></a>Ezekiel 38:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [śûm](../../strongs/h/h7760.md) thy [paniym](../../strongs/h/h6440.md) against [Gôḡ](../../strongs/h/h1463.md), the ['erets](../../strongs/h/h776.md) of [Māḡôḡ](../../strongs/h/h4031.md), the [ro'sh](../../strongs/h/h7218.md) [nāśî'](../../strongs/h/h5387.md) of [Mešeḵ](../../strongs/h/h4902.md) and [Tuḇal](../../strongs/h/h8422.md), and [nāḇā'](../../strongs/h/h5012.md) against him,

<a name="ezekiel_38_3"></a>Ezekiel 38:3

And ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I am against thee, O [Gôḡ](../../strongs/h/h1463.md), the [ro'sh](../../strongs/h/h7218.md) [nāśî'](../../strongs/h/h5387.md) of [Mešeḵ](../../strongs/h/h4902.md) and [Tuḇal](../../strongs/h/h8422.md):

<a name="ezekiel_38_4"></a>Ezekiel 38:4

And I will [shuwb](../../strongs/h/h7725.md) thee, and [nathan](../../strongs/h/h5414.md) [ḥāḥ](../../strongs/h/h2397.md) into thy [lᵊḥî](../../strongs/h/h3895.md), and I will [yāṣā'](../../strongs/h/h3318.md) thee, and all thine [ḥayil](../../strongs/h/h2428.md), [sûs](../../strongs/h/h5483.md) and [pārāš](../../strongs/h/h6571.md), all of them [labash](../../strongs/h/h3847.md) with all [miḵlôl](../../strongs/h/h4358.md), even a [rab](../../strongs/h/h7227.md) [qāhēl](../../strongs/h/h6951.md) with [tsinnah](../../strongs/h/h6793.md) and [magen](../../strongs/h/h4043.md), all of them [tāp̄aś](../../strongs/h/h8610.md) [chereb](../../strongs/h/h2719.md):

<a name="ezekiel_38_5"></a>Ezekiel 38:5

[Pāras](../../strongs/h/h6539.md), [Kûš](../../strongs/h/h3568.md), and [Pûṭ](../../strongs/h/h6316.md) with them; all of them with [magen](../../strongs/h/h4043.md) and [kôḇaʿ](../../strongs/h/h3553.md):

<a name="ezekiel_38_6"></a>Ezekiel 38:6

[Gōmer](../../strongs/h/h1586.md), and all his ['ăḡap̄](../../strongs/h/h102.md); the [bayith](../../strongs/h/h1004.md) of [Tōḡarmâ](../../strongs/h/h8425.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md) [yᵊrēḵâ](../../strongs/h/h3411.md), and all his ['ăḡap̄](../../strongs/h/h102.md): and [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) with thee.

<a name="ezekiel_38_7"></a>Ezekiel 38:7

Be thou [kuwn](../../strongs/h/h3559.md), and [kuwn](../../strongs/h/h3559.md) for thyself, thou, and all thy [qāhēl](../../strongs/h/h6951.md) that are [qāhal](../../strongs/h/h6950.md) unto thee, and be thou a [mišmār](../../strongs/h/h4929.md) unto them.

<a name="ezekiel_38_8"></a>Ezekiel 38:8

After [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md) thou shalt be [paqad](../../strongs/h/h6485.md): in the ['aḥărîṯ](../../strongs/h/h319.md) [šānâ](../../strongs/h/h8141.md) thou shalt [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) that is [shuwb](../../strongs/h/h7725.md) from the [chereb](../../strongs/h/h2719.md), and is [qāḇaṣ](../../strongs/h/h6908.md) out of [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md), against the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md), which have been [tāmîḏ](../../strongs/h/h8548.md) [chorbah](../../strongs/h/h2723.md): but it is [yāṣā'](../../strongs/h/h3318.md) out of the ['am](../../strongs/h/h5971.md), and they shall [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md) all of them.

<a name="ezekiel_38_9"></a>Ezekiel 38:9

Thou shalt [ʿālâ](../../strongs/h/h5927.md) and [bow'](../../strongs/h/h935.md) like a [šô'](../../strongs/h/h7722.md), thou shalt be like a [ʿānān](../../strongs/h/h6051.md) to [kāsâ](../../strongs/h/h3680.md) the ['erets](../../strongs/h/h776.md), thou, and all thy ['ăḡap̄](../../strongs/h/h102.md), and [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) with thee.

<a name="ezekiel_38_10"></a>Ezekiel 38:10

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); It shall also come to pass, that at the same [yowm](../../strongs/h/h3117.md) shall [dabar](../../strongs/h/h1697.md) [ʿālâ](../../strongs/h/h5927.md) into thy [lebab](../../strongs/h/h3824.md), and thou shalt [chashab](../../strongs/h/h2803.md) a [ra'](../../strongs/h/h7451.md) [maḥăšāḇâ](../../strongs/h/h4284.md):

<a name="ezekiel_38_11"></a>Ezekiel 38:11

And thou shalt ['āmar](../../strongs/h/h559.md), I will [ʿālâ](../../strongs/h/h5927.md) to the ['erets](../../strongs/h/h776.md) of [p̄rvzym](../../strongs/h/h6519.md); I will [bow'](../../strongs/h/h935.md) to them that are at [šāqaṭ](../../strongs/h/h8252.md), that [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md), all of them [yashab](../../strongs/h/h3427.md) without [ḥômâ](../../strongs/h/h2346.md), and having neither [bᵊrîaḥ](../../strongs/h/h1280.md) nor [deleṯ](../../strongs/h/h1817.md),

<a name="ezekiel_38_12"></a>Ezekiel 38:12

To [šālal](../../strongs/h/h7997.md) a [šālāl](../../strongs/h/h7998.md), and to [bāzaz](../../strongs/h/h962.md) a [baz](../../strongs/h/h957.md); to [shuwb](../../strongs/h/h7725.md) thine [yad](../../strongs/h/h3027.md) upon the [chorbah](../../strongs/h/h2723.md) that are now [yashab](../../strongs/h/h3427.md), and upon the ['am](../../strongs/h/h5971.md) that are ['āsap̄](../../strongs/h/h622.md) out of the [gowy](../../strongs/h/h1471.md), which have ['asah](../../strongs/h/h6213.md) [miqnê](../../strongs/h/h4735.md) and [qinyān](../../strongs/h/h7075.md), that [yashab](../../strongs/h/h3427.md) in the [ṭabûr](../../strongs/h/h2872.md) of the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_38_13"></a>Ezekiel 38:13

[Šᵊḇā'](../../strongs/h/h7614.md), and [Dᵊḏān](../../strongs/h/h1719.md), and the [sāḥar](../../strongs/h/h5503.md) of [Taršîš](../../strongs/h/h8659.md), with all the [kephiyr](../../strongs/h/h3715.md) thereof, shall ['āmar](../../strongs/h/h559.md) unto thee, Art thou [bow'](../../strongs/h/h935.md) to [šālal](../../strongs/h/h7997.md) a [šālāl](../../strongs/h/h7998.md)? hast thou [qāhal](../../strongs/h/h6950.md) thy [qāhēl](../../strongs/h/h6951.md) to [bāzaz](../../strongs/h/h962.md) a [baz](../../strongs/h/h957.md)? to carry [nasa'](../../strongs/h/h5375.md) [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md), to [laqach](../../strongs/h/h3947.md) [miqnê](../../strongs/h/h4735.md) and [qinyān](../../strongs/h/h7075.md), to [šālal](../../strongs/h/h7997.md) a [gadowl](../../strongs/h/h1419.md) [šālāl](../../strongs/h/h7998.md)?

<a name="ezekiel_38_14"></a>Ezekiel 38:14

Therefore, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nāḇā'](../../strongs/h/h5012.md) and ['āmar](../../strongs/h/h559.md) unto [Gôḡ](../../strongs/h/h1463.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); In that [yowm](../../strongs/h/h3117.md) when my ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md) [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md), shalt thou not [yada'](../../strongs/h/h3045.md) it?

<a name="ezekiel_38_15"></a>Ezekiel 38:15

And thou shalt [bow'](../../strongs/h/h935.md) from thy [maqowm](../../strongs/h/h4725.md) out of the [ṣāp̄ôn](../../strongs/h/h6828.md) [yᵊrēḵâ](../../strongs/h/h3411.md), thou, and [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) with thee, all of them [rāḵaḇ](../../strongs/h/h7392.md) upon [sûs](../../strongs/h/h5483.md), a [gadowl](../../strongs/h/h1419.md) [qāhēl](../../strongs/h/h6951.md), and a [rab](../../strongs/h/h7227.md) [ḥayil](../../strongs/h/h2428.md):

<a name="ezekiel_38_16"></a>Ezekiel 38:16

And thou shalt [ʿālâ](../../strongs/h/h5927.md) against my ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md), as a [ʿānān](../../strongs/h/h6051.md) to [kāsâ](../../strongs/h/h3680.md) the ['erets](../../strongs/h/h776.md); it shall be in the ['aḥărîṯ](../../strongs/h/h319.md) [yowm](../../strongs/h/h3117.md), and I will [bow'](../../strongs/h/h935.md) thee against my ['erets](../../strongs/h/h776.md), that the [gowy](../../strongs/h/h1471.md) may [yada'](../../strongs/h/h3045.md) me, when I shall be [qadash](../../strongs/h/h6942.md) in thee, O [Gôḡ](../../strongs/h/h1463.md), before their ['ayin](../../strongs/h/h5869.md).

<a name="ezekiel_38_17"></a>Ezekiel 38:17

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Art thou he of whom I have [dabar](../../strongs/h/h1696.md) in [qaḏmōnî](../../strongs/h/h6931.md) [yowm](../../strongs/h/h3117.md) [yad](../../strongs/h/h3027.md) my ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md) of [Yisra'el](../../strongs/h/h3478.md), which [nāḇā'](../../strongs/h/h5012.md) in those [yowm](../../strongs/h/h3117.md) many [šānâ](../../strongs/h/h8141.md) that I would [bow'](../../strongs/h/h935.md) thee against them?

<a name="ezekiel_38_18"></a>Ezekiel 38:18

And it shall come to pass at the same [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) [Gôḡ](../../strongs/h/h1463.md) shall [bow'](../../strongs/h/h935.md) against the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), that my [chemah](../../strongs/h/h2534.md) shall [ʿālâ](../../strongs/h/h5927.md) in my ['aph](../../strongs/h/h639.md).

<a name="ezekiel_38_19"></a>Ezekiel 38:19

For in my [qin'â](../../strongs/h/h7068.md) and in the ['esh](../../strongs/h/h784.md) of my ['ebrah](../../strongs/h/h5678.md) have I [dabar](../../strongs/h/h1696.md), Surely in that [yowm](../../strongs/h/h3117.md) there shall be a [gadowl](../../strongs/h/h1419.md) [raʿaš](../../strongs/h/h7494.md) in the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md);

<a name="ezekiel_38_20"></a>Ezekiel 38:20

So that the [dag](../../strongs/h/h1709.md) of the [yam](../../strongs/h/h3220.md), and the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md), and all [remeś](../../strongs/h/h7431.md) that [rāmaś](../../strongs/h/h7430.md) upon the ['ăḏāmâ](../../strongs/h/h127.md), and all the ['āḏām](../../strongs/h/h120.md) that are upon the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md), shall [rāʿaš](../../strongs/h/h7493.md) at my [paniym](../../strongs/h/h6440.md), and the [har](../../strongs/h/h2022.md) shall be [harac](../../strongs/h/h2040.md), and the [maḏrēḡâ](../../strongs/h/h4095.md) shall [naphal](../../strongs/h/h5307.md), and every [ḥômâ](../../strongs/h/h2346.md) shall [naphal](../../strongs/h/h5307.md) to the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_38_21"></a>Ezekiel 38:21

And I will [qara'](../../strongs/h/h7121.md) for a [chereb](../../strongs/h/h2719.md) against him throughout all my [har](../../strongs/h/h2022.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md): every ['iysh](../../strongs/h/h376.md) [chereb](../../strongs/h/h2719.md) shall be against his ['ach](../../strongs/h/h251.md).

<a name="ezekiel_38_22"></a>Ezekiel 38:22

And I will [shaphat](../../strongs/h/h8199.md) against him with [deḇer](../../strongs/h/h1698.md) and with [dam](../../strongs/h/h1818.md); and I will [matar](../../strongs/h/h4305.md) upon him, and upon his ['ăḡap̄](../../strongs/h/h102.md), and upon the [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) that are with him, a [šāṭap̄](../../strongs/h/h7857.md) [gešem](../../strongs/h/h1653.md), and ['elgāḇîš](../../strongs/h/h417.md) ['eben](../../strongs/h/h68.md), ['esh](../../strongs/h/h784.md), and [gophriyth](../../strongs/h/h1614.md).

<a name="ezekiel_38_23"></a>Ezekiel 38:23

Thus will I [gāḏal](../../strongs/h/h1431.md) myself, and [qadash](../../strongs/h/h6942.md) myself; and I will be [yada'](../../strongs/h/h3045.md) in the ['ayin](../../strongs/h/h5869.md) of [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md), and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 37](ezekiel_37.md) - [Ezekiel 39](ezekiel_39.md)