# [Ezekiel 26](https://www.blueletterbible.org/kjv/ezekiel/26)

<a name="ezekiel_26_1"></a>Ezekiel 26:1

And it came to pass in the eleventh [šānâ](../../strongs/h/h8141.md), in the first day of the [ḥōḏeš](../../strongs/h/h2320.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_26_2"></a>Ezekiel 26:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), because that [Ṣōr](../../strongs/h/h6865.md) hath ['āmar](../../strongs/h/h559.md) against [Yĕruwshalaim](../../strongs/h/h3389.md), [he'āḥ](../../strongs/h/h1889.md), she is [shabar](../../strongs/h/h7665.md) that was the [deleṯ](../../strongs/h/h1817.md) of the ['am](../../strongs/h/h5971.md): she is [cabab](../../strongs/h/h5437.md) unto me: I shall be [mālā'](../../strongs/h/h4390.md), she is [ḥāraḇ](../../strongs/h/h2717.md):

<a name="ezekiel_26_3"></a>Ezekiel 26:3

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I am against thee, O [Ṣōr](../../strongs/h/h6865.md), and will cause [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md) to [ʿālâ](../../strongs/h/h5927.md) against thee, as the [yam](../../strongs/h/h3220.md) causeth his [gal](../../strongs/h/h1530.md) to [ʿālâ](../../strongs/h/h5927.md).

<a name="ezekiel_26_4"></a>Ezekiel 26:4

And they shall [shachath](../../strongs/h/h7843.md) the [ḥômâ](../../strongs/h/h2346.md) of [Ṣōr](../../strongs/h/h6865.md), and [harac](../../strongs/h/h2040.md) her [miḡdāl](../../strongs/h/h4026.md): I will also [sāḥâ](../../strongs/h/h5500.md) her ['aphar](../../strongs/h/h6083.md) from her, and [nathan](../../strongs/h/h5414.md) her like the [ṣᵊḥîaḥ](../../strongs/h/h6706.md) of a [cela'](../../strongs/h/h5553.md).

<a name="ezekiel_26_5"></a>Ezekiel 26:5

It shall be for the [mišṭôaḥ](../../strongs/h/h4894.md) of [ḥērem](../../strongs/h/h2764.md) in the midst of the [yam](../../strongs/h/h3220.md): for I have [dabar](../../strongs/h/h1696.md) it, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md): and it shall become a [baz](../../strongs/h/h957.md) to the [gowy](../../strongs/h/h1471.md).

<a name="ezekiel_26_6"></a>Ezekiel 26:6

And her [bath](../../strongs/h/h1323.md) which are in the [sadeh](../../strongs/h/h7704.md) shall be [harag](../../strongs/h/h2026.md) by the [chereb](../../strongs/h/h2719.md); and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_26_7"></a>Ezekiel 26:7

For thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I will [bow'](../../strongs/h/h935.md) upon [Ṣōr](../../strongs/h/h6865.md) [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), a [melek](../../strongs/h/h4428.md) of [melek](../../strongs/h/h4428.md), from the [ṣāp̄ôn](../../strongs/h/h6828.md), with [sûs](../../strongs/h/h5483.md), and with [reḵeḇ](../../strongs/h/h7393.md), and with [pārāš](../../strongs/h/h6571.md), and [qāhēl](../../strongs/h/h6951.md), and [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md).

<a name="ezekiel_26_8"></a>Ezekiel 26:8

He shall [harag](../../strongs/h/h2026.md) with the [chereb](../../strongs/h/h2719.md) thy [bath](../../strongs/h/h1323.md) in the [sadeh](../../strongs/h/h7704.md): and he shall [nathan](../../strongs/h/h5414.md) a [dāyēq](../../strongs/h/h1785.md) against thee, and [šāp̄aḵ](../../strongs/h/h8210.md) a [sōllâ](../../strongs/h/h5550.md) against thee, and [quwm](../../strongs/h/h6965.md) the [tsinnah](../../strongs/h/h6793.md) against thee.

<a name="ezekiel_26_9"></a>Ezekiel 26:9

And he shall [nathan](../../strongs/h/h5414.md) [mᵊḥî](../../strongs/h/h4239.md) of [qōḇel](../../strongs/h/h6904.md) against thy [ḥômâ](../../strongs/h/h2346.md), and with his [chereb](../../strongs/h/h2719.md) he shall [nāṯaṣ](../../strongs/h/h5422.md) thy [miḡdāl](../../strongs/h/h4026.md).

<a name="ezekiel_26_10"></a>Ezekiel 26:10

By reason of the [šip̄ʿâ](../../strongs/h/h8229.md) of his [sûs](../../strongs/h/h5483.md) their ['āḇāq](../../strongs/h/h80.md) shall [kāsâ](../../strongs/h/h3680.md) thee: thy [ḥômâ](../../strongs/h/h2346.md) shall [rāʿaš](../../strongs/h/h7493.md) at the [qowl](../../strongs/h/h6963.md) of the [pārāš](../../strongs/h/h6571.md), and of the [galgal](../../strongs/h/h1534.md), and of the [reḵeḇ](../../strongs/h/h7393.md), when he shall [bow'](../../strongs/h/h935.md) into thy [sha'ar](../../strongs/h/h8179.md), as [māḇô'](../../strongs/h/h3996.md) into a [ʿîr](../../strongs/h/h5892.md) wherein is made a [bāqaʿ](../../strongs/h/h1234.md).

<a name="ezekiel_26_11"></a>Ezekiel 26:11

With the [parsâ](../../strongs/h/h6541.md) of his [sûs](../../strongs/h/h5483.md) shall he [rāmas](../../strongs/h/h7429.md) all thy [ḥûṣ](../../strongs/h/h2351.md): he shall [harag](../../strongs/h/h2026.md) thy ['am](../../strongs/h/h5971.md) by the [chereb](../../strongs/h/h2719.md), and thy ['oz](../../strongs/h/h5797.md) [maṣṣēḇâ](../../strongs/h/h4676.md) shall [yarad](../../strongs/h/h3381.md) to the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_26_12"></a>Ezekiel 26:12

And they shall make a [šālal](../../strongs/h/h7997.md) of thy [ḥayil](../../strongs/h/h2428.md), and make a [bāzaz](../../strongs/h/h962.md) of thy [rᵊḵullâ](../../strongs/h/h7404.md): and they shall [harac](../../strongs/h/h2040.md) thy [ḥômâ](../../strongs/h/h2346.md), and [nāṯaṣ](../../strongs/h/h5422.md) thy [ḥemdâ](../../strongs/h/h2532.md) [bayith](../../strongs/h/h1004.md): and they shall [śûm](../../strongs/h/h7760.md) thy ['eben](../../strongs/h/h68.md) and thy ['ets](../../strongs/h/h6086.md) and thy ['aphar](../../strongs/h/h6083.md) in the midst of the [mayim](../../strongs/h/h4325.md).

<a name="ezekiel_26_13"></a>Ezekiel 26:13

And I will cause the [hāmôn](../../strongs/h/h1995.md) of thy [šîr](../../strongs/h/h7892.md) to [shabath](../../strongs/h/h7673.md); and the [qowl](../../strongs/h/h6963.md) of thy [kinnôr](../../strongs/h/h3658.md) shall be no more [shama'](../../strongs/h/h8085.md).

<a name="ezekiel_26_14"></a>Ezekiel 26:14

And I will [nathan](../../strongs/h/h5414.md) thee like the [ṣᵊḥîaḥ](../../strongs/h/h6706.md) of a [cela'](../../strongs/h/h5553.md): thou shalt [mišṭôaḥ](../../strongs/h/h4894.md) [ḥērem](../../strongs/h/h2764.md) [mišṭôaḥ](../../strongs/h/h4894.md); thou shalt be [bānâ](../../strongs/h/h1129.md) no more: for I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) it, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_26_15"></a>Ezekiel 26:15

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) to [Ṣōr](../../strongs/h/h6865.md); Shall not the ['î](../../strongs/h/h339.md) [rāʿaš](../../strongs/h/h7493.md) at the [qowl](../../strongs/h/h6963.md) of thy [mapeleṯ](../../strongs/h/h4658.md), when the [ḥālāl](../../strongs/h/h2491.md) ['ānaq](../../strongs/h/h602.md), when the [hereḡ](../../strongs/h/h2027.md) is [harag](../../strongs/h/h2026.md) in the midst of thee?l

<a name="ezekiel_26_16"></a>Ezekiel 26:16

Then all the [nāśî'](../../strongs/h/h5387.md) of the [yam](../../strongs/h/h3220.md) shall [yarad](../../strongs/h/h3381.md) from their [kicce'](../../strongs/h/h3678.md), and [cuwr](../../strongs/h/h5493.md) their [mᵊʿîl](../../strongs/h/h4598.md), and [pāšaṭ](../../strongs/h/h6584.md) their [riqmâ](../../strongs/h/h7553.md) [beḡeḏ](../../strongs/h/h899.md): they shall [labash](../../strongs/h/h3847.md) themselves with [ḥărāḏâ](../../strongs/h/h2731.md); they shall [yashab](../../strongs/h/h3427.md) upon the ['erets](../../strongs/h/h776.md), and shall [ḥārēḏ](../../strongs/h/h2729.md) at every [reḡaʿ](../../strongs/h/h7281.md), and be [šāmēm](../../strongs/h/h8074.md) at thee.

<a name="ezekiel_26_17"></a>Ezekiel 26:17

And they shall take [nasa'](../../strongs/h/h5375.md) a [qînâ](../../strongs/h/h7015.md) for thee, and ['āmar](../../strongs/h/h559.md) to thee, How art thou ['abad](../../strongs/h/h6.md), that wast [yashab](../../strongs/h/h3427.md) of [yam](../../strongs/h/h3220.md), the [halal](../../strongs/h/h1984.md) [ʿîr](../../strongs/h/h5892.md), which wast [ḥāzāq](../../strongs/h/h2389.md) in the [yam](../../strongs/h/h3220.md), she and her [yashab](../../strongs/h/h3427.md), which [nathan](../../strongs/h/h5414.md) their [ḥitîṯ](../../strongs/h/h2851.md) to be on all that [yashab](../../strongs/h/h3427.md) it!

<a name="ezekiel_26_18"></a>Ezekiel 26:18

Now shall the ['î](../../strongs/h/h339.md) [ḥārēḏ](../../strongs/h/h2729.md) in the [yowm](../../strongs/h/h3117.md) of thy [mapeleṯ](../../strongs/h/h4658.md); yea, the ['î](../../strongs/h/h339.md) that are in the [yam](../../strongs/h/h3220.md) shall be [bahal](../../strongs/h/h926.md) at thy [yāṣā'](../../strongs/h/h3318.md).

<a name="ezekiel_26_19"></a>Ezekiel 26:19

For thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); When I shall [nathan](../../strongs/h/h5414.md) thee a [ḥāraḇ](../../strongs/h/h2717.md) [ʿîr](../../strongs/h/h5892.md), like the [ʿîr](../../strongs/h/h5892.md) that are not [yashab](../../strongs/h/h3427.md); when I shall [ʿālâ](../../strongs/h/h5927.md) the [tĕhowm](../../strongs/h/h8415.md) upon thee, and [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md) shall [kāsâ](../../strongs/h/h3680.md) thee;

<a name="ezekiel_26_20"></a>Ezekiel 26:20

When I shall bring thee [yarad](../../strongs/h/h3381.md) with them that [yarad](../../strongs/h/h3381.md) into the [bowr](../../strongs/h/h953.md), with the ['am](../../strongs/h/h5971.md) of ['owlam](../../strongs/h/h5769.md), and shall [yashab](../../strongs/h/h3427.md) thee in the low [taḥtî](../../strongs/h/h8482.md) of the ['erets](../../strongs/h/h776.md), in places [chorbah](../../strongs/h/h2723.md) of ['owlam](../../strongs/h/h5769.md), with them that [yarad](../../strongs/h/h3381.md) to the [bowr](../../strongs/h/h953.md), that thou be not [yashab](../../strongs/h/h3427.md); and I shall [nathan](../../strongs/h/h5414.md) [ṣᵊḇî](../../strongs/h/h6643.md) in the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md);

<a name="ezekiel_26_21"></a>Ezekiel 26:21

I will [nathan](../../strongs/h/h5414.md) thee a [ballāhâ](../../strongs/h/h1091.md), and thou shalt be no more: though thou be [bāqaš](../../strongs/h/h1245.md), yet shalt thou ['owlam](../../strongs/h/h5769.md) be [māṣā'](../../strongs/h/h4672.md) again, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 25](ezekiel_25.md) - [Ezekiel 27](ezekiel_27.md)