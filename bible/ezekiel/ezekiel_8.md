# [Ezekiel 8](https://www.blueletterbible.org/kjv/ezekiel/8)

<a name="ezekiel_8_1"></a>Ezekiel 8:1

And it came to pass in the sixth [šānâ](../../strongs/h/h8141.md), in the sixth month, in the fifth day of the [ḥōḏeš](../../strongs/h/h2320.md), as I [yashab](../../strongs/h/h3427.md) in mine [bayith](../../strongs/h/h1004.md), and the [zāqēn](../../strongs/h/h2205.md) of [Yehuwdah](../../strongs/h/h3063.md) [yashab](../../strongs/h/h3427.md) [paniym](../../strongs/h/h6440.md) me, that the [yad](../../strongs/h/h3027.md) of the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) [naphal](../../strongs/h/h5307.md) there upon me.

<a name="ezekiel_8_2"></a>Ezekiel 8:2

Then I [ra'ah](../../strongs/h/h7200.md), and lo a [dĕmuwth](../../strongs/h/h1823.md) as the [mar'ê](../../strongs/h/h4758.md) of ['esh](../../strongs/h/h784.md): from the [mar'ê](../../strongs/h/h4758.md) of his [māṯnayim](../../strongs/h/h4975.md) even [maṭṭâ](../../strongs/h/h4295.md), ['esh](../../strongs/h/h784.md); and from his [māṯnayim](../../strongs/h/h4975.md) even [maʿal](../../strongs/h/h4605.md), as the [mar'ê](../../strongs/h/h4758.md) of [zōhar](../../strongs/h/h2096.md), as the ['ayin](../../strongs/h/h5869.md) of [ḥašmāl](../../strongs/h/h2830.md).

<a name="ezekiel_8_3"></a>Ezekiel 8:3

And he [shalach](../../strongs/h/h7971.md) the [taḇnîṯ](../../strongs/h/h8403.md) of a [yad](../../strongs/h/h3027.md), and [laqach](../../strongs/h/h3947.md) me by a [ṣîṣiṯ](../../strongs/h/h6734.md) of mine [ro'sh](../../strongs/h/h7218.md); and the [ruwach](../../strongs/h/h7307.md) [nasa'](../../strongs/h/h5375.md) me between the ['erets](../../strongs/h/h776.md) and the [shamayim](../../strongs/h/h8064.md), and [bow'](../../strongs/h/h935.md) me in the [mar'â](../../strongs/h/h4759.md) of ['Elohiym](../../strongs/h/h430.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), to the [peṯaḥ](../../strongs/h/h6607.md) of the [pᵊnîmî](../../strongs/h/h6442.md) [sha'ar](../../strongs/h/h8179.md) that [panah](../../strongs/h/h6437.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md); where was the [môšāḇ](../../strongs/h/h4186.md) of the [semel](../../strongs/h/h5566.md) of [qin'â](../../strongs/h/h7068.md), which [qānâ](../../strongs/h/h7069.md).

<a name="ezekiel_8_4"></a>Ezekiel 8:4

And, behold, the [kabowd](../../strongs/h/h3519.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) was there, according to the [mar'ê](../../strongs/h/h4758.md) that I [ra'ah](../../strongs/h/h7200.md) in the [biqʿâ](../../strongs/h/h1237.md).

<a name="ezekiel_8_5"></a>Ezekiel 8:5

Then ['āmar](../../strongs/h/h559.md) he unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nasa'](../../strongs/h/h5375.md) thine ['ayin](../../strongs/h/h5869.md) now the [derek](../../strongs/h/h1870.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md). So I [nasa'](../../strongs/h/h5375.md) mine ['ayin](../../strongs/h/h5869.md) the [derek](../../strongs/h/h1870.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md), and behold [ṣāp̄ôn](../../strongs/h/h6828.md) at the [sha'ar](../../strongs/h/h8179.md) of the [mizbeach](../../strongs/h/h4196.md) this [semel](../../strongs/h/h5566.md) of [qin'â](../../strongs/h/h7068.md) in the [bi'â](../../strongs/h/h872.md).

<a name="ezekiel_8_6"></a>Ezekiel 8:6

He ['āmar](../../strongs/h/h559.md) furthermore unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [ra'ah](../../strongs/h/h7200.md) thou what they ['asah](../../strongs/h/h6213.md)? even the [gadowl](../../strongs/h/h1419.md) [tôʿēḇâ](../../strongs/h/h8441.md) that the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) here, that I should [rachaq](../../strongs/h/h7368.md) from my [miqdash](../../strongs/h/h4720.md)? but [shuwb](../../strongs/h/h7725.md) thee yet again, and thou shalt [ra'ah](../../strongs/h/h7200.md) [gadowl](../../strongs/h/h1419.md) [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="ezekiel_8_7"></a>Ezekiel 8:7

And he [bow'](../../strongs/h/h935.md) me to the [peṯaḥ](../../strongs/h/h6607.md) of the [ḥāṣēr](../../strongs/h/h2691.md); and when I [ra'ah](../../strongs/h/h7200.md), behold a [ḥôr](../../strongs/h/h2356.md) in the [qîr](../../strongs/h/h7023.md).

<a name="ezekiel_8_8"></a>Ezekiel 8:8

Then ['āmar](../../strongs/h/h559.md) he unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [ḥāṯar](../../strongs/h/h2864.md) now in the [qîr](../../strongs/h/h7023.md): and when I had [ḥāṯar](../../strongs/h/h2864.md) in the [qîr](../../strongs/h/h7023.md), behold a [peṯaḥ](../../strongs/h/h6607.md).

<a name="ezekiel_8_9"></a>Ezekiel 8:9

And he ['āmar](../../strongs/h/h559.md) unto me, [bow'](../../strongs/h/h935.md), and [ra'ah](../../strongs/h/h7200.md) the [ra'](../../strongs/h/h7451.md) [tôʿēḇâ](../../strongs/h/h8441.md) that they ['asah](../../strongs/h/h6213.md) here.

<a name="ezekiel_8_10"></a>Ezekiel 8:10

So I [bow'](../../strongs/h/h935.md) and [ra'ah](../../strongs/h/h7200.md); and behold every [taḇnîṯ](../../strongs/h/h8403.md) of [remeś](../../strongs/h/h7431.md), and [šeqeṣ](../../strongs/h/h8263.md) [bĕhemah](../../strongs/h/h929.md), and all the [gillûl](../../strongs/h/h1544.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), [ḥāqâ](../../strongs/h/h2707.md) upon the [qîr](../../strongs/h/h7023.md) [cabiyb](../../strongs/h/h5439.md).

<a name="ezekiel_8_11"></a>Ezekiel 8:11

And there ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) them seventy ['iysh](../../strongs/h/h376.md) of the [zāqēn](../../strongs/h/h2205.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), and in the midst of them ['amad](../../strongs/h/h5975.md) [Ya'Ăzanyâ](../../strongs/h/h2970.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md), with every ['iysh](../../strongs/h/h376.md) his [miqṭereṯ](../../strongs/h/h4730.md) in his [yad](../../strongs/h/h3027.md); and a [ʿāṯār](../../strongs/h/h6282.md) [ʿānān](../../strongs/h/h6051.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md) [ʿālâ](../../strongs/h/h5927.md).

<a name="ezekiel_8_12"></a>Ezekiel 8:12

Then ['āmar](../../strongs/h/h559.md) he unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), hast thou [ra'ah](../../strongs/h/h7200.md) what the [zāqēn](../../strongs/h/h2205.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) in the [choshek](../../strongs/h/h2822.md), every ['iysh](../../strongs/h/h376.md) in the [ḥeḏer](../../strongs/h/h2315.md) of his [maśkîṯ](../../strongs/h/h4906.md)? for they ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) us not; [Yĕhovah](../../strongs/h/h3068.md) hath ['azab](../../strongs/h/h5800.md) the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_8_13"></a>Ezekiel 8:13

He ['āmar](../../strongs/h/h559.md) also unto me, [shuwb](../../strongs/h/h7725.md) thee yet again, and thou shalt [ra'ah](../../strongs/h/h7200.md) [gadowl](../../strongs/h/h1419.md) [tôʿēḇâ](../../strongs/h/h8441.md) that they ['asah](../../strongs/h/h6213.md).

<a name="ezekiel_8_14"></a>Ezekiel 8:14

Then he [bow'](../../strongs/h/h935.md) me to the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md) which was toward the [ṣāp̄ôn](../../strongs/h/h6828.md); and, behold, there [yashab](../../strongs/h/h3427.md) ['ishshah](../../strongs/h/h802.md) [bāḵâ](../../strongs/h/h1058.md) for [tammûz](../../strongs/h/h8542.md).

<a name="ezekiel_8_15"></a>Ezekiel 8:15

Then ['āmar](../../strongs/h/h559.md) he unto me, Hast thou [ra'ah](../../strongs/h/h7200.md) this, O [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md)? [shuwb](../../strongs/h/h7725.md) thee yet again, and thou shalt [ra'ah](../../strongs/h/h7200.md) [gadowl](../../strongs/h/h1419.md) [tôʿēḇâ](../../strongs/h/h8441.md) than these.

<a name="ezekiel_8_16"></a>Ezekiel 8:16

And he [bow'](../../strongs/h/h935.md) me into the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md), and, behold, at the [peṯaḥ](../../strongs/h/h6607.md) of the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md), between the ['ûlām](../../strongs/h/h197.md) and the [mizbeach](../../strongs/h/h4196.md), were about five and twenty ['iysh](../../strongs/h/h376.md), with their ['āḥôr](../../strongs/h/h268.md) toward the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md), and their [paniym](../../strongs/h/h6440.md) toward the [qeḏem](../../strongs/h/h6924.md); and they [shachah](../../strongs/h/h7812.md) the [šemeš](../../strongs/h/h8121.md) toward the [qeḏem](../../strongs/h/h6924.md).

<a name="ezekiel_8_17"></a>Ezekiel 8:17

Then he ['āmar](../../strongs/h/h559.md) unto me, Hast thou [ra'ah](../../strongs/h/h7200.md) this, O [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md)? Is it a [qālal](../../strongs/h/h7043.md) to the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) that they ['asah](../../strongs/h/h6213.md) the [tôʿēḇâ](../../strongs/h/h8441.md) which they ['asah](../../strongs/h/h6213.md) here? for they have [mālā'](../../strongs/h/h4390.md) the ['erets](../../strongs/h/h776.md) with [chamac](../../strongs/h/h2555.md), and have [shuwb](../../strongs/h/h7725.md) to provoke me to [kāʿas](../../strongs/h/h3707.md): and, lo, they [shalach](../../strongs/h/h7971.md) the [zᵊmôrâ](../../strongs/h/h2156.md) to their ['aph](../../strongs/h/h639.md).

<a name="ezekiel_8_18"></a>Ezekiel 8:18

Therefore will I also ['asah](../../strongs/h/h6213.md) in [chemah](../../strongs/h/h2534.md): mine ['ayin](../../strongs/h/h5869.md) shall not [ḥûs](../../strongs/h/h2347.md), neither will I have [ḥāmal](../../strongs/h/h2550.md): and though they [qara'](../../strongs/h/h7121.md) in mine ['ozen](../../strongs/h/h241.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md), yet will I not [shama'](../../strongs/h/h8085.md) them.

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 7](ezekiel_7.md) - [Ezekiel 9](ezekiel_9.md)