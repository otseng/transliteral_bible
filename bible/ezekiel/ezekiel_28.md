# [Ezekiel 28](https://www.blueletterbible.org/kjv/ezekiel/28)

<a name="ezekiel_28_1"></a>Ezekiel 28:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came again unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_28_2"></a>Ezekiel 28:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), ['āmar](../../strongs/h/h559.md) unto the [nāḡîḏ](../../strongs/h/h5057.md) of [Ṣōr](../../strongs/h/h6865.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because thine [leb](../../strongs/h/h3820.md) is lifted [gāḇah](../../strongs/h/h1361.md), and thou hast ['āmar](../../strongs/h/h559.md), I am an ['el](../../strongs/h/h410.md), I [yashab](../../strongs/h/h3427.md) in the [môšāḇ](../../strongs/h/h4186.md) of ['Elohiym](../../strongs/h/h430.md), in the [leb](../../strongs/h/h3820.md) of the [yam](../../strongs/h/h3220.md); yet thou art an ['āḏām](../../strongs/h/h120.md), and not ['el](../../strongs/h/h410.md), though thou [nathan](../../strongs/h/h5414.md) thine [leb](../../strongs/h/h3820.md) as the [leb](../../strongs/h/h3820.md) of ['Elohiym](../../strongs/h/h430.md):

<a name="ezekiel_28_3"></a>Ezekiel 28:3

Behold, thou art [ḥāḵām](../../strongs/h/h2450.md) than [Dinîyē'L](../../strongs/h/h1840.md); there is no [sāṯam](../../strongs/h/h5640.md) that they can [ʿāmam](../../strongs/h/h6004.md) from thee:

<a name="ezekiel_28_4"></a>Ezekiel 28:4

With thy [ḥāḵmâ](../../strongs/h/h2451.md) and with thine [tāḇûn](../../strongs/h/h8394.md) thou hast ['asah](../../strongs/h/h6213.md) thee [ḥayil](../../strongs/h/h2428.md), and hast ['asah](../../strongs/h/h6213.md) [zāhāḇ](../../strongs/h/h2091.md) and [keceph](../../strongs/h/h3701.md) into thy ['ôṣār](../../strongs/h/h214.md):

<a name="ezekiel_28_5"></a>Ezekiel 28:5

By thy [rōḇ](../../strongs/h/h7230.md) [ḥāḵmâ](../../strongs/h/h2451.md) and by thy [rᵊḵullâ](../../strongs/h/h7404.md) hast thou [rabah](../../strongs/h/h7235.md) thy [ḥayil](../../strongs/h/h2428.md), and thine [lebab](../../strongs/h/h3824.md) is [gāḇah](../../strongs/h/h1361.md) because of thy [ḥayil](../../strongs/h/h2428.md):

<a name="ezekiel_28_6"></a>Ezekiel 28:6

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because thou hast [nathan](../../strongs/h/h5414.md) thine [leb](../../strongs/h/h3820.md) as the [lebab](../../strongs/h/h3824.md) of ['Elohiym](../../strongs/h/h430.md);

<a name="ezekiel_28_7"></a>Ezekiel 28:7

Behold, therefore I will [bow'](../../strongs/h/h935.md) [zûr](../../strongs/h/h2114.md) upon thee, the [ʿārîṣ](../../strongs/h/h6184.md) of the [gowy](../../strongs/h/h1471.md): and they shall [rîq](../../strongs/h/h7324.md) their [chereb](../../strongs/h/h2719.md) against the [yᵊp̄î](../../strongs/h/h3308.md) of thy [ḥāḵmâ](../../strongs/h/h2451.md), and they shall [ḥālal](../../strongs/h/h2490.md) thy [yip̄ʿâ](../../strongs/h/h3314.md).

<a name="ezekiel_28_8"></a>Ezekiel 28:8

They shall bring thee [yarad](../../strongs/h/h3381.md) to the [shachath](../../strongs/h/h7845.md), and thou shalt [muwth](../../strongs/h/h4191.md) the [māmôṯ](../../strongs/h/h4463.md) of them that are [ḥālāl](../../strongs/h/h2491.md) in the [leb](../../strongs/h/h3820.md) of the [yam](../../strongs/h/h3220.md).

<a name="ezekiel_28_9"></a>Ezekiel 28:9

Wilt thou ['āmar](../../strongs/h/h559.md) ['āmar](../../strongs/h/h559.md) [paniym](../../strongs/h/h6440.md) him that [harag](../../strongs/h/h2026.md) thee, I am ['Elohiym](../../strongs/h/h430.md)? but thou shalt be an ['āḏām](../../strongs/h/h120.md), and no ['el](../../strongs/h/h410.md), in the [yad](../../strongs/h/h3027.md) of him that [ḥālal](../../strongs/h/h2490.md) thee.

<a name="ezekiel_28_10"></a>Ezekiel 28:10

Thou shalt [muwth](../../strongs/h/h4191.md) the [maveth](../../strongs/h/h4194.md) of the [ʿārēl](../../strongs/h/h6189.md) by the [yad](../../strongs/h/h3027.md) of [zûr](../../strongs/h/h2114.md): for I have [dabar](../../strongs/h/h1696.md) it, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_28_11"></a>Ezekiel 28:11

Moreover the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_28_12"></a>Ezekiel 28:12

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nasa'](../../strongs/h/h5375.md) a [qînâ](../../strongs/h/h7015.md) upon the [melek](../../strongs/h/h4428.md) of [Ṣōr](../../strongs/h/h6865.md), and ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Thou [ḥāṯam](../../strongs/h/h2856.md) the [tāḵnîṯ](../../strongs/h/h8508.md), [mālē'](../../strongs/h/h4392.md) of [ḥāḵmâ](../../strongs/h/h2451.md), and [kālîl](../../strongs/h/h3632.md) in [yᵊp̄î](../../strongs/h/h3308.md).

<a name="ezekiel_28_13"></a>Ezekiel 28:13

Thou hast been in ['Eden](../../strongs/h/h5731.md) the [gan](../../strongs/h/h1588.md) of ['Elohiym](../../strongs/h/h430.md); every [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md) was thy [mᵊsukâ](../../strongs/h/h4540.md), the ['ōḏem](../../strongs/h/h124.md), [piṭḏâ](../../strongs/h/h6357.md), and the [yahălōm](../../strongs/h/h3095.md), the [taršîš](../../strongs/h/h8658.md), the [šōham](../../strongs/h/h7718.md), and the [yāšp̄ih](../../strongs/h/h3471.md), the [sapîr](../../strongs/h/h5601.md), the [nōp̄eḵ](../../strongs/h/h5306.md), and the [bāreqeṯ](../../strongs/h/h1304.md), and [zāhāḇ](../../strongs/h/h2091.md): the [mĕla'kah](../../strongs/h/h4399.md) of thy [tōp̄](../../strongs/h/h8596.md) and of thy [neqeḇ](../../strongs/h/h5345.md) was [kuwn](../../strongs/h/h3559.md) in thee in the [yowm](../../strongs/h/h3117.md) that thou wast [bara'](../../strongs/h/h1254.md).

<a name="ezekiel_28_14"></a>Ezekiel 28:14

Thou art the [mimšaḥ](../../strongs/h/h4473.md) [kĕruwb](../../strongs/h/h3742.md) that [cakak](../../strongs/h/h5526.md); and I have [nathan](../../strongs/h/h5414.md) thee so: thou wast upon the [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md) of ['Elohiym](../../strongs/h/h430.md); thou hast [halak](../../strongs/h/h1980.md) in the midst of the ['eben](../../strongs/h/h68.md) of ['esh](../../strongs/h/h784.md).

<a name="ezekiel_28_15"></a>Ezekiel 28:15

Thou wast [tamiym](../../strongs/h/h8549.md) in thy [derek](../../strongs/h/h1870.md) from the [yowm](../../strongs/h/h3117.md) that thou wast [bara'](../../strongs/h/h1254.md), till ['evel](../../strongs/h/h5766.md) was [māṣā'](../../strongs/h/h4672.md) in thee.

<a name="ezekiel_28_16"></a>Ezekiel 28:16

By the [rōḇ](../../strongs/h/h7230.md) of thy [rᵊḵullâ](../../strongs/h/h7404.md) they have [mālā'](../../strongs/h/h4390.md) the midst of thee with [chamac](../../strongs/h/h2555.md), and thou hast [chata'](../../strongs/h/h2398.md): therefore I will cast thee as [ḥālal](../../strongs/h/h2490.md) out of the [har](../../strongs/h/h2022.md) of ['Elohiym](../../strongs/h/h430.md): and I will ['abad](../../strongs/h/h6.md) thee, O [cakak](../../strongs/h/h5526.md) [kĕruwb](../../strongs/h/h3742.md), from the midst of the ['eben](../../strongs/h/h68.md) of ['esh](../../strongs/h/h784.md).

<a name="ezekiel_28_17"></a>Ezekiel 28:17

Thine [leb](../../strongs/h/h3820.md) was lifted [gāḇah](../../strongs/h/h1361.md) because of thy [yᵊp̄î](../../strongs/h/h3308.md), thou hast [shachath](../../strongs/h/h7843.md) thy [ḥāḵmâ](../../strongs/h/h2451.md) by reason of thy [yip̄ʿâ](../../strongs/h/h3314.md): I will [shalak](../../strongs/h/h7993.md) thee to the ['erets](../../strongs/h/h776.md), I will [nathan](../../strongs/h/h5414.md) thee [paniym](../../strongs/h/h6440.md) [melek](../../strongs/h/h4428.md), that they may [ra'ah](../../strongs/h/h7200.md) thee.

<a name="ezekiel_28_18"></a>Ezekiel 28:18

Thou hast [ḥālal](../../strongs/h/h2490.md) thy [miqdash](../../strongs/h/h4720.md) by the [rōḇ](../../strongs/h/h7230.md) of thine ['avon](../../strongs/h/h5771.md), by the ['evel](../../strongs/h/h5766.md) of thy [rᵊḵullâ](../../strongs/h/h7404.md); therefore will I [yāṣā'](../../strongs/h/h3318.md) an ['esh](../../strongs/h/h784.md) from the midst of thee, it shall ['akal](../../strongs/h/h398.md) thee, and I will [nathan](../../strongs/h/h5414.md) thee to ['ēp̄er](../../strongs/h/h665.md) upon the ['erets](../../strongs/h/h776.md) in the ['ayin](../../strongs/h/h5869.md) of all them that [ra'ah](../../strongs/h/h7200.md) thee.

<a name="ezekiel_28_19"></a>Ezekiel 28:19

All they that [yada'](../../strongs/h/h3045.md) thee among the ['am](../../strongs/h/h5971.md) shall be [šāmēm](../../strongs/h/h8074.md) at thee: thou shalt be a [ballāhâ](../../strongs/h/h1091.md), and never shalt thou be any more ['owlam](../../strongs/h/h5769.md).

<a name="ezekiel_28_20"></a>Ezekiel 28:20

Again the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_28_21"></a>Ezekiel 28:21

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [śûm](../../strongs/h/h7760.md) thy [paniym](../../strongs/h/h6440.md) against [Ṣîḏôn](../../strongs/h/h6721.md), and [nāḇā'](../../strongs/h/h5012.md) against it,

<a name="ezekiel_28_22"></a>Ezekiel 28:22

And ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I am against thee, O [Ṣîḏôn](../../strongs/h/h6721.md); and I will be [kabad](../../strongs/h/h3513.md) in the midst of thee: and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when I shall have ['asah](../../strongs/h/h6213.md) [šep̄eṭ](../../strongs/h/h8201.md) in her, and shall be [qadash](../../strongs/h/h6942.md) in her.

<a name="ezekiel_28_23"></a>Ezekiel 28:23

For I will [shalach](../../strongs/h/h7971.md) into her [deḇer](../../strongs/h/h1698.md), and [dam](../../strongs/h/h1818.md) into her [ḥûṣ](../../strongs/h/h2351.md); and the [ḥālāl](../../strongs/h/h2491.md) shall be [naphal](../../strongs/h/h5307.md) in the midst of her by the [chereb](../../strongs/h/h2719.md) upon her [cabiyb](../../strongs/h/h5439.md); and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_28_24"></a>Ezekiel 28:24

And there shall be no more a [mā'ar](../../strongs/h/h3992.md) [sallôn](../../strongs/h/h5544.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), nor any [kā'aḇ](../../strongs/h/h3510.md) [qowts](../../strongs/h/h6975.md) of all that are [cabiyb](../../strongs/h/h5439.md) them, that [shâʼṭ](../../strongs/h/h7590.md) them; and they shall [yada'](../../strongs/h/h3045.md) that I am the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_28_25"></a>Ezekiel 28:25

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); When I shall have [qāḇaṣ](../../strongs/h/h6908.md) the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) from the ['am](../../strongs/h/h5971.md) among whom they are [puwts](../../strongs/h/h6327.md), and shall be [qadash](../../strongs/h/h6942.md) in them in the ['ayin](../../strongs/h/h5869.md) of the [gowy](../../strongs/h/h1471.md), then shall they [yashab](../../strongs/h/h3427.md) in their ['ăḏāmâ](../../strongs/h/h127.md) that I have [nathan](../../strongs/h/h5414.md) to my ['ebed](../../strongs/h/h5650.md) [Ya'aqob](../../strongs/h/h3290.md).

<a name="ezekiel_28_26"></a>Ezekiel 28:26

And they shall [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md) therein, and shall [bānâ](../../strongs/h/h1129.md) [bayith](../../strongs/h/h1004.md), and [nāṭaʿ](../../strongs/h/h5193.md) [kerem](../../strongs/h/h3754.md); yea, they shall [yashab](../../strongs/h/h3427.md) with [betach](../../strongs/h/h983.md), when I have ['asah](../../strongs/h/h6213.md) [šep̄eṭ](../../strongs/h/h8201.md) upon all those that [shâʼṭ](../../strongs/h/h7590.md) them [cabiyb](../../strongs/h/h5439.md) them; and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 27](ezekiel_27.md) - [Ezekiel 29](ezekiel_29.md)