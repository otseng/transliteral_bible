# [Ezekiel 22](https://www.blueletterbible.org/kjv/ezekiel/22)

<a name="ezekiel_22_1"></a>Ezekiel 22:1

Moreover the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_22_2"></a>Ezekiel 22:2

Now, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), wilt thou [shaphat](../../strongs/h/h8199.md), wilt thou [shaphat](../../strongs/h/h8199.md) the [dam](../../strongs/h/h1818.md) [ʿîr](../../strongs/h/h5892.md)? yea, thou shalt [yada'](../../strongs/h/h3045.md) her all her [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="ezekiel_22_3"></a>Ezekiel 22:3

Then ['āmar](../../strongs/h/h559.md) thou, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), The [ʿîr](../../strongs/h/h5892.md) [šāp̄aḵ](../../strongs/h/h8210.md) [dam](../../strongs/h/h1818.md) in the midst of it, that her [ʿēṯ](../../strongs/h/h6256.md) may [bow'](../../strongs/h/h935.md), and ['asah](../../strongs/h/h6213.md) [gillûl](../../strongs/h/h1544.md) against herself to [ṭāmē'](../../strongs/h/h2930.md) herself.

<a name="ezekiel_22_4"></a>Ezekiel 22:4

Thou art become ['asham](../../strongs/h/h816.md) in thy [dam](../../strongs/h/h1818.md) that thou hast [šāp̄aḵ](../../strongs/h/h8210.md); and hast [ṭāmē'](../../strongs/h/h2930.md) thyself in thine [gillûl](../../strongs/h/h1544.md) which thou hast ['asah](../../strongs/h/h6213.md); and thou hast caused thy [yowm](../../strongs/h/h3117.md) to [qāraḇ](../../strongs/h/h7126.md), and art [bow'](../../strongs/h/h935.md) even unto thy [šānâ](../../strongs/h/h8141.md): therefore have I [nathan](../../strongs/h/h5414.md) thee a [cherpah](../../strongs/h/h2781.md) unto the [gowy](../../strongs/h/h1471.md), and a [qallāsâ](../../strongs/h/h7048.md) to all ['erets](../../strongs/h/h776.md).

<a name="ezekiel_22_5"></a>Ezekiel 22:5

Those that be [qarowb](../../strongs/h/h7138.md), and those that be [rachowq](../../strongs/h/h7350.md) from thee, shall [qālas](../../strongs/h/h7046.md) thee, which art [tame'](../../strongs/h/h2931.md) [shem](../../strongs/h/h8034.md) and [rab](../../strongs/h/h7227.md) [mᵊhûmâ](../../strongs/h/h4103.md).

<a name="ezekiel_22_6"></a>Ezekiel 22:6

Behold, the [nāśî'](../../strongs/h/h5387.md) of [Yisra'el](../../strongs/h/h3478.md), every ['iysh](../../strongs/h/h376.md) were in thee to their [zerowa'](../../strongs/h/h2220.md) to [šāp̄aḵ](../../strongs/h/h8210.md) [dam](../../strongs/h/h1818.md).

<a name="ezekiel_22_7"></a>Ezekiel 22:7

In thee have they [qālal](../../strongs/h/h7043.md) by ['ab](../../strongs/h/h1.md) and ['em](../../strongs/h/h517.md): in the midst of thee have they ['asah](../../strongs/h/h6213.md) by [ʿōšeq](../../strongs/h/h6233.md) with the [ger](../../strongs/h/h1616.md): in thee have they [yānâ](../../strongs/h/h3238.md) the [yathowm](../../strongs/h/h3490.md) and the ['almānâ](../../strongs/h/h490.md).

<a name="ezekiel_22_8"></a>Ezekiel 22:8

Thou hast [bazah](../../strongs/h/h959.md) mine [qodesh](../../strongs/h/h6944.md), and hast [ḥālal](../../strongs/h/h2490.md) my [shabbath](../../strongs/h/h7676.md).

<a name="ezekiel_22_9"></a>Ezekiel 22:9

In thee are ['enowsh](../../strongs/h/h582.md) that [rāḵîl](../../strongs/h/h7400.md) to [šāp̄aḵ](../../strongs/h/h8210.md) [dam](../../strongs/h/h1818.md): and in thee they ['akal](../../strongs/h/h398.md) upon the [har](../../strongs/h/h2022.md): in the midst of thee they ['asah](../../strongs/h/h6213.md) [zimmâ](../../strongs/h/h2154.md).

<a name="ezekiel_22_10"></a>Ezekiel 22:10

In thee have they [gālâ](../../strongs/h/h1540.md) their ['ab](../../strongs/h/h1.md) [ʿervâ](../../strongs/h/h6172.md): in thee have they [ʿānâ](../../strongs/h/h6031.md) her that was set [nidâ](../../strongs/h/h5079.md) for [tame'](../../strongs/h/h2931.md).

<a name="ezekiel_22_11"></a>Ezekiel 22:11

And ['iysh](../../strongs/h/h376.md) hath ['asah](../../strongs/h/h6213.md) [tôʿēḇâ](../../strongs/h/h8441.md) with his [rea'](../../strongs/h/h7453.md) ['ishshah](../../strongs/h/h802.md); and ['iysh](../../strongs/h/h376.md) hath [zimmâ](../../strongs/h/h2154.md) [ṭāmē'](../../strongs/h/h2930.md) his [kallâ](../../strongs/h/h3618.md); and ['iysh](../../strongs/h/h376.md) in thee hath [ʿānâ](../../strongs/h/h6031.md) his ['āḥôṯ](../../strongs/h/h269.md), his ['ab](../../strongs/h/h1.md) [bath](../../strongs/h/h1323.md).

<a name="ezekiel_22_12"></a>Ezekiel 22:12

In thee have they [laqach](../../strongs/h/h3947.md) [shachad](../../strongs/h/h7810.md) to [šāp̄aḵ](../../strongs/h/h8210.md) [dam](../../strongs/h/h1818.md); thou hast [laqach](../../strongs/h/h3947.md) [neshek](../../strongs/h/h5392.md) and [tarbîṯ](../../strongs/h/h8636.md), and thou hast [batsa'](../../strongs/h/h1214.md) of thy [rea'](../../strongs/h/h7453.md) by [ʿōšeq](../../strongs/h/h6233.md), and hast [shakach](../../strongs/h/h7911.md) me, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_22_13"></a>Ezekiel 22:13

Behold, therefore I have [nakah](../../strongs/h/h5221.md) mine [kaph](../../strongs/h/h3709.md) at thy [beṣaʿ](../../strongs/h/h1215.md) which thou hast ['asah](../../strongs/h/h6213.md), and at thy [dam](../../strongs/h/h1818.md) which hath been in the midst of thee.

<a name="ezekiel_22_14"></a>Ezekiel 22:14

Can thine [leb](../../strongs/h/h3820.md) ['amad](../../strongs/h/h5975.md), or can thine [yad](../../strongs/h/h3027.md) be [ḥāzaq](../../strongs/h/h2388.md), in the [yowm](../../strongs/h/h3117.md) that I shall ['asah](../../strongs/h/h6213.md) with thee? I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) it, and will ['asah](../../strongs/h/h6213.md) it.

<a name="ezekiel_22_15"></a>Ezekiel 22:15

And I will [puwts](../../strongs/h/h6327.md) thee among the [gowy](../../strongs/h/h1471.md), and [zārâ](../../strongs/h/h2219.md) thee in the ['erets](../../strongs/h/h776.md), and will [tamam](../../strongs/h/h8552.md) thy [ṭām'â](../../strongs/h/h2932.md) out of thee.

<a name="ezekiel_22_16"></a>Ezekiel 22:16

And thou shalt take thine [ḥālal](../../strongs/h/h2490.md) [nāḥal](../../strongs/h/h5157.md) in thyself in the ['ayin](../../strongs/h/h5869.md) of the [gowy](../../strongs/h/h1471.md), and thou shalt [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_22_17"></a>Ezekiel 22:17

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_22_18"></a>Ezekiel 22:18

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) is to me become [sîḡ](../../strongs/h/h5509.md): all they are [nᵊḥšeṯ](../../strongs/h/h5178.md), and [bᵊḏîl](../../strongs/h/h913.md), and [barzel](../../strongs/h/h1270.md), and [ʿōp̄ereṯ](../../strongs/h/h5777.md), in the midst of the [kûr](../../strongs/h/h3564.md); they are even the [sîḡ](../../strongs/h/h5509.md) of [keceph](../../strongs/h/h3701.md).

<a name="ezekiel_22_19"></a>Ezekiel 22:19

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because ye are all become [sîḡ](../../strongs/h/h5509.md), behold, therefore I will [qāḇaṣ](../../strongs/h/h6908.md) you into the midst of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="ezekiel_22_20"></a>Ezekiel 22:20

As they [qᵊḇuṣâ](../../strongs/h/h6910.md) [keceph](../../strongs/h/h3701.md), and [nᵊḥšeṯ](../../strongs/h/h5178.md), and [barzel](../../strongs/h/h1270.md), and [ʿōp̄ereṯ](../../strongs/h/h5777.md), and [bᵊḏîl](../../strongs/h/h913.md), into the midst of the [kûr](../../strongs/h/h3564.md), to [nāp̄aḥ](../../strongs/h/h5301.md) the ['esh](../../strongs/h/h784.md) upon it, to [nāṯaḵ](../../strongs/h/h5413.md) it; so will I [qāḇaṣ](../../strongs/h/h6908.md) you in mine ['aph](../../strongs/h/h639.md) and in my [chemah](../../strongs/h/h2534.md), and I will [yānaḥ](../../strongs/h/h3240.md) you there, and [nāṯaḵ](../../strongs/h/h5413.md) you.

<a name="ezekiel_22_21"></a>Ezekiel 22:21

Yea, I will [kānas](../../strongs/h/h3664.md) you, and [nāp̄aḥ](../../strongs/h/h5301.md) upon you in the ['esh](../../strongs/h/h784.md) of my ['ebrah](../../strongs/h/h5678.md), and ye shall be [nāṯaḵ](../../strongs/h/h5413.md) in the midst thereof.

<a name="ezekiel_22_22"></a>Ezekiel 22:22

As [keceph](../../strongs/h/h3701.md) is [hitûḵ](../../strongs/h/h2046.md) in the midst of the [kûr](../../strongs/h/h3564.md), so shall ye be [nāṯaḵ](../../strongs/h/h5413.md) in the midst thereof; and ye shall [yada'](../../strongs/h/h3045.md) that I [Yĕhovah](../../strongs/h/h3068.md) have [šāp̄aḵ](../../strongs/h/h8210.md) my [chemah](../../strongs/h/h2534.md) upon you.

<a name="ezekiel_22_23"></a>Ezekiel 22:23

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_22_24"></a>Ezekiel 22:24

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), ['āmar](../../strongs/h/h559.md) unto her, Thou art the ['erets](../../strongs/h/h776.md) that is not [ṭāhēr](../../strongs/h/h2891.md), nor [gōšem](../../strongs/h/h1656.md) upon in the [yowm](../../strongs/h/h3117.md) of [zaʿam](../../strongs/h/h2195.md).

<a name="ezekiel_22_25"></a>Ezekiel 22:25

There is a [qešer](../../strongs/h/h7195.md) of her [nāḇî'](../../strongs/h/h5030.md) in the midst thereof, like a [šā'aḡ](../../strongs/h/h7580.md) ['ariy](../../strongs/h/h738.md) [taraph](../../strongs/h/h2963.md) the [ṭerep̄](../../strongs/h/h2964.md); they have ['akal](../../strongs/h/h398.md) [nephesh](../../strongs/h/h5315.md); they have [laqach](../../strongs/h/h3947.md) the [ḥōsen](../../strongs/h/h2633.md) and [yᵊqār](../../strongs/h/h3366.md); they have made her [rabah](../../strongs/h/h7235.md) ['almānâ](../../strongs/h/h490.md) in the midst thereof.

<a name="ezekiel_22_26"></a>Ezekiel 22:26

Her [kōhēn](../../strongs/h/h3548.md) have [ḥāmas](../../strongs/h/h2554.md) my [towrah](../../strongs/h/h8451.md), and have [ḥālal](../../strongs/h/h2490.md) mine [qodesh](../../strongs/h/h6944.md): they have put no [bāḏal](../../strongs/h/h914.md) between the [qodesh](../../strongs/h/h6944.md) and [ḥōl](../../strongs/h/h2455.md), neither have they [yada'](../../strongs/h/h3045.md) difference between the [tame'](../../strongs/h/h2931.md) and the [tahowr](../../strongs/h/h2889.md), and have ['alam](../../strongs/h/h5956.md) their ['ayin](../../strongs/h/h5869.md) from my [shabbath](../../strongs/h/h7676.md), and I am [ḥālal](../../strongs/h/h2490.md) among them.

<a name="ezekiel_22_27"></a>Ezekiel 22:27

Her [śar](../../strongs/h/h8269.md) in the [qereḇ](../../strongs/h/h7130.md) thereof are like [zᵊ'ēḇ](../../strongs/h/h2061.md) [taraph](../../strongs/h/h2963.md) the [ṭerep̄](../../strongs/h/h2964.md), to [šāp̄aḵ](../../strongs/h/h8210.md) [dam](../../strongs/h/h1818.md), and to ['abad](../../strongs/h/h6.md) [nephesh](../../strongs/h/h5315.md), to [batsa'](../../strongs/h/h1214.md) [beṣaʿ](../../strongs/h/h1215.md).

<a name="ezekiel_22_28"></a>Ezekiel 22:28

And her [nāḇî'](../../strongs/h/h5030.md) have [ṭûaḥ](../../strongs/h/h2902.md) them with [tāp̄ēl](../../strongs/h/h8602.md), [ḥōzê](../../strongs/h/h2374.md) [shav'](../../strongs/h/h7723.md), and [qāsam](../../strongs/h/h7080.md) [kazab](../../strongs/h/h3577.md) unto them, ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), when [Yĕhovah](../../strongs/h/h3068.md) hath not [dabar](../../strongs/h/h1696.md).

<a name="ezekiel_22_29"></a>Ezekiel 22:29

The ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) have used [ʿāšaq](../../strongs/h/h6231.md) [ʿōšeq](../../strongs/h/h6233.md), and [gāzal](../../strongs/h/h1497.md) [gāzēl](../../strongs/h/h1498.md), and have [yānâ](../../strongs/h/h3238.md) the ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md): yea, they have [ʿāšaq](../../strongs/h/h6231.md) the [ger](../../strongs/h/h1616.md) [mishpat](../../strongs/h/h4941.md).

<a name="ezekiel_22_30"></a>Ezekiel 22:30

And I [bāqaš](../../strongs/h/h1245.md) for an ['iysh](../../strongs/h/h376.md) among them, that should make [gāḏar](../../strongs/h/h1443.md) the [gāḏēr](../../strongs/h/h1447.md), and ['amad](../../strongs/h/h5975.md) in the [pereṣ](../../strongs/h/h6556.md) [paniym](../../strongs/h/h6440.md) me for the ['erets](../../strongs/h/h776.md), that I should not [shachath](../../strongs/h/h7843.md) it: but I [māṣā'](../../strongs/h/h4672.md) none.

<a name="ezekiel_22_31"></a>Ezekiel 22:31

Therefore have I [šāp̄aḵ](../../strongs/h/h8210.md) mine [zaʿam](../../strongs/h/h2195.md) upon them; I have [kalah](../../strongs/h/h3615.md) them with the ['esh](../../strongs/h/h784.md) of my ['ebrah](../../strongs/h/h5678.md): their own [derek](../../strongs/h/h1870.md) have I [nathan](../../strongs/h/h5414.md) upon their [ro'sh](../../strongs/h/h7218.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 21](ezekiel_21.md) - [Ezekiel 23](ezekiel_23.md)