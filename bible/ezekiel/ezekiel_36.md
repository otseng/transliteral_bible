# [Ezekiel 36](https://www.blueletterbible.org/kjv/ezekiel/36)

<a name="ezekiel_36_1"></a>Ezekiel 36:1

Also, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nāḇā'](../../strongs/h/h5012.md) unto the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md), Ye [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md):

<a name="ezekiel_36_2"></a>Ezekiel 36:2

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because the ['oyeb](../../strongs/h/h341.md) hath ['āmar](../../strongs/h/h559.md) against you, [he'āḥ](../../strongs/h/h1889.md), even the ['owlam](../../strongs/h/h5769.md) [bāmâ](../../strongs/h/h1116.md) are ours in [môrāšâ](../../strongs/h/h4181.md):

<a name="ezekiel_36_3"></a>Ezekiel 36:3

Therefore [nāḇā'](../../strongs/h/h5012.md) and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because they have made you [šāmēm](../../strongs/h/h8074.md), and [šā'ap̄](../../strongs/h/h7602.md) you [cabiyb](../../strongs/h/h5439.md), that ye might be a [môrāšâ](../../strongs/h/h4181.md) unto the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the [gowy](../../strongs/h/h1471.md), and ye are [ʿālâ](../../strongs/h/h5927.md) in the [saphah](../../strongs/h/h8193.md) of [lashown](../../strongs/h/h3956.md), and are a [dibâ](../../strongs/h/h1681.md) of the ['am](../../strongs/h/h5971.md):

<a name="ezekiel_36_4"></a>Ezekiel 36:4

Therefore, ye [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) to the [har](../../strongs/h/h2022.md), and to the [giḇʿâ](../../strongs/h/h1389.md), to the ['āp̄îq](../../strongs/h/h650.md), and to the [gay'](../../strongs/h/h1516.md), to the [šāmēm](../../strongs/h/h8074.md) [chorbah](../../strongs/h/h2723.md), and to the [ʿîr](../../strongs/h/h5892.md) that are ['azab](../../strongs/h/h5800.md), which became a [baz](../../strongs/h/h957.md) and [laʿaḡ](../../strongs/h/h3933.md) to the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the [gowy](../../strongs/h/h1471.md) that are [cabiyb](../../strongs/h/h5439.md);

<a name="ezekiel_36_5"></a>Ezekiel 36:5

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Surely in the ['esh](../../strongs/h/h784.md) of my [qin'â](../../strongs/h/h7068.md) have I [dabar](../../strongs/h/h1696.md) against the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the [gowy](../../strongs/h/h1471.md), and against all ['Ĕḏōm](../../strongs/h/h123.md), which have [nathan](../../strongs/h/h5414.md) my ['erets](../../strongs/h/h776.md) into their [môrāšâ](../../strongs/h/h4181.md) with the [simchah](../../strongs/h/h8057.md) of all their [lebab](../../strongs/h/h3824.md), with [šᵊ'āṭ](../../strongs/h/h7589.md) [nephesh](../../strongs/h/h5315.md), to [miḡrāš](../../strongs/h/h4054.md) it for a [baz](../../strongs/h/h957.md).

<a name="ezekiel_36_6"></a>Ezekiel 36:6

[nāḇā'](../../strongs/h/h5012.md) therefore concerning the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto the [har](../../strongs/h/h2022.md), and to the [giḇʿâ](../../strongs/h/h1389.md), to the ['āp̄îq](../../strongs/h/h650.md), and to the [gay'](../../strongs/h/h1516.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I have [dabar](../../strongs/h/h1696.md) in my [qin'â](../../strongs/h/h7068.md) and in my [chemah](../../strongs/h/h2534.md), because ye have [nasa'](../../strongs/h/h5375.md) the [kĕlimmah](../../strongs/h/h3639.md) of the [gowy](../../strongs/h/h1471.md):

<a name="ezekiel_36_7"></a>Ezekiel 36:7

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); I have [nasa'](../../strongs/h/h5375.md) mine [yad](../../strongs/h/h3027.md), Surely the [gowy](../../strongs/h/h1471.md) that are [cabiyb](../../strongs/h/h5439.md) you, they shall [nasa'](../../strongs/h/h5375.md) their [kĕlimmah](../../strongs/h/h3639.md).

<a name="ezekiel_36_8"></a>Ezekiel 36:8

But ye, O [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md), ye shall shoot [nathan](../../strongs/h/h5414.md) your [ʿānāp̄](../../strongs/h/h6057.md), and [nasa'](../../strongs/h/h5375.md) your [pĕriy](../../strongs/h/h6529.md) to my ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md); for they are at [qāraḇ](../../strongs/h/h7126.md) to [bow'](../../strongs/h/h935.md).

<a name="ezekiel_36_9"></a>Ezekiel 36:9

For, behold, I am for you, and I will [panah](../../strongs/h/h6437.md) unto you, and ye shall be ['abad](../../strongs/h/h5647.md) and [zāraʿ](../../strongs/h/h2232.md):

<a name="ezekiel_36_10"></a>Ezekiel 36:10

And I will [rabah](../../strongs/h/h7235.md) ['āḏām](../../strongs/h/h120.md) upon you, all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), even all of it: and the [ʿîr](../../strongs/h/h5892.md) shall be [yashab](../../strongs/h/h3427.md), and the [chorbah](../../strongs/h/h2723.md) shall be [bānâ](../../strongs/h/h1129.md):

<a name="ezekiel_36_11"></a>Ezekiel 36:11

And I will [rabah](../../strongs/h/h7235.md) upon you ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md); and they shall [rabah](../../strongs/h/h7235.md) and bring [parah](../../strongs/h/h6509.md): and I will [yashab](../../strongs/h/h3427.md) you after your [qaḏmâ](../../strongs/h/h6927.md), and will [ṭôḇ](../../strongs/h/h2895.md) unto you than at your [ri'šâ](../../strongs/h/h7221.md): and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_36_12"></a>Ezekiel 36:12

Yea, I will cause ['āḏām](../../strongs/h/h120.md) to [yālaḵ](../../strongs/h/h3212.md) upon you, even my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md); and they shall [yarash](../../strongs/h/h3423.md) thee, and thou shalt be their [nachalah](../../strongs/h/h5159.md), and thou shalt no more henceforth [šāḵōl](../../strongs/h/h7921.md) them of men.

<a name="ezekiel_36_13"></a>Ezekiel 36:13

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because they ['āmar](../../strongs/h/h559.md) unto you, Thou ['akal](../../strongs/h/h398.md) ['āḏām](../../strongs/h/h120.md), and hast [šāḵōl](../../strongs/h/h7921.md) thy [gowy](../../strongs/h/h1471.md);

<a name="ezekiel_36_14"></a>Ezekiel 36:14

Therefore thou shalt ['akal](../../strongs/h/h398.md) ['āḏām](../../strongs/h/h120.md) no more, neither [šāḵōl](../../strongs/h/h7921.md) [kashal](../../strongs/h/h3782.md) thy [gowy](../../strongs/h/h1471.md) any more, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_36_15"></a>Ezekiel 36:15

Neither will I cause men to [shama'](../../strongs/h/h8085.md) in thee the [kĕlimmah](../../strongs/h/h3639.md) of the [gowy](../../strongs/h/h1471.md) any more, neither shalt thou [nasa'](../../strongs/h/h5375.md) the [cherpah](../../strongs/h/h2781.md) of the ['am](../../strongs/h/h5971.md) any more, neither shalt thou cause thy [gowy](../../strongs/h/h1471.md) to [kashal](../../strongs/h/h3782.md) any more, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_36_16"></a>Ezekiel 36:16

Moreover the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_36_17"></a>Ezekiel 36:17

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), when the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) [yashab](../../strongs/h/h3427.md) in their own ['ăḏāmâ](../../strongs/h/h127.md), they [ṭāmē'](../../strongs/h/h2930.md) it by their own [derek](../../strongs/h/h1870.md) and by their ['aliylah](../../strongs/h/h5949.md): their [derek](../../strongs/h/h1870.md) was [paniym](../../strongs/h/h6440.md) me as the [ṭām'â](../../strongs/h/h2932.md) of a [nidâ](../../strongs/h/h5079.md).

<a name="ezekiel_36_18"></a>Ezekiel 36:18

Wherefore I [šāp̄aḵ](../../strongs/h/h8210.md) my [chemah](../../strongs/h/h2534.md) upon them for the [dam](../../strongs/h/h1818.md) that they had [šāp̄aḵ](../../strongs/h/h8210.md) upon the ['erets](../../strongs/h/h776.md), and for their [gillûl](../../strongs/h/h1544.md) wherewith they had [ṭāmē'](../../strongs/h/h2930.md) it:

<a name="ezekiel_36_19"></a>Ezekiel 36:19

And I [puwts](../../strongs/h/h6327.md) them among the [gowy](../../strongs/h/h1471.md), and they were [zārâ](../../strongs/h/h2219.md) through the ['erets](../../strongs/h/h776.md): according to their [derek](../../strongs/h/h1870.md) and according to their ['aliylah](../../strongs/h/h5949.md) I [shaphat](../../strongs/h/h8199.md) them.

<a name="ezekiel_36_20"></a>Ezekiel 36:20

And when they [bow'](../../strongs/h/h935.md) unto the [gowy](../../strongs/h/h1471.md), whither they [bow'](../../strongs/h/h935.md), they [ḥālal](../../strongs/h/h2490.md) my [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md), when they ['āmar](../../strongs/h/h559.md) to them, These are the ['am](../../strongs/h/h5971.md) of [Yĕhovah](../../strongs/h/h3068.md), and are [yāṣā'](../../strongs/h/h3318.md) out of his ['erets](../../strongs/h/h776.md).

<a name="ezekiel_36_21"></a>Ezekiel 36:21

But I had [ḥāmal](../../strongs/h/h2550.md) for mine [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md), which the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) had [ḥālal](../../strongs/h/h2490.md) among the [gowy](../../strongs/h/h1471.md), whither they [bow'](../../strongs/h/h935.md).

<a name="ezekiel_36_22"></a>Ezekiel 36:22

Therefore ['āmar](../../strongs/h/h559.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); I ['asah](../../strongs/h/h6213.md) not this for your sakes, O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), but for mine [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md) sake, which ye have [ḥālal](../../strongs/h/h2490.md) among the [gowy](../../strongs/h/h1471.md), whither ye [bow'](../../strongs/h/h935.md).

<a name="ezekiel_36_23"></a>Ezekiel 36:23

And I will [qadash](../../strongs/h/h6942.md) my [gadowl](../../strongs/h/h1419.md) [shem](../../strongs/h/h8034.md), which was [ḥālal](../../strongs/h/h2490.md) among the [gowy](../../strongs/h/h1471.md), which ye have [ḥālal](../../strongs/h/h2490.md) in the [tavek](../../strongs/h/h8432.md) of them; and the [gowy](../../strongs/h/h1471.md) shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), when I shall be [qadash](../../strongs/h/h6942.md) in you before their ['ayin](../../strongs/h/h5869.md).

<a name="ezekiel_36_24"></a>Ezekiel 36:24

For I will [laqach](../../strongs/h/h3947.md) you from among the [gowy](../../strongs/h/h1471.md), and [qāḇaṣ](../../strongs/h/h6908.md) you out of all ['erets](../../strongs/h/h776.md), and will [bow'](../../strongs/h/h935.md) you into your own ['ăḏāmâ](../../strongs/h/h127.md).

<a name="ezekiel_36_25"></a>Ezekiel 36:25

Then will I [zāraq](../../strongs/h/h2236.md) [tahowr](../../strongs/h/h2889.md) [mayim](../../strongs/h/h4325.md) upon you, and ye shall be [ṭāhēr](../../strongs/h/h2891.md): from all your [ṭām'â](../../strongs/h/h2932.md), and from all your [gillûl](../../strongs/h/h1544.md), will I [ṭāhēr](../../strongs/h/h2891.md) you.

<a name="ezekiel_36_26"></a>Ezekiel 36:26

A [ḥāḏāš](../../strongs/h/h2319.md) [leb](../../strongs/h/h3820.md) also will I [nathan](../../strongs/h/h5414.md) you, and a [ḥāḏāš](../../strongs/h/h2319.md) [ruwach](../../strongs/h/h7307.md) will I [nathan](../../strongs/h/h5414.md) [qereḇ](../../strongs/h/h7130.md) you: and I will [cuwr](../../strongs/h/h5493.md) the ['eben](../../strongs/h/h68.md) [leb](../../strongs/h/h3820.md) out of your [basar](../../strongs/h/h1320.md), and I will [nathan](../../strongs/h/h5414.md) you a [leb](../../strongs/h/h3820.md) of [basar](../../strongs/h/h1320.md).

<a name="ezekiel_36_27"></a>Ezekiel 36:27

And I will [nathan](../../strongs/h/h5414.md) my [ruwach](../../strongs/h/h7307.md) [qereḇ](../../strongs/h/h7130.md) you, and ['asah](../../strongs/h/h6213.md) you to [yālaḵ](../../strongs/h/h3212.md) in my [choq](../../strongs/h/h2706.md), and ye shall [shamar](../../strongs/h/h8104.md) my [mishpat](../../strongs/h/h4941.md), and ['asah](../../strongs/h/h6213.md) them.

<a name="ezekiel_36_28"></a>Ezekiel 36:28

And ye shall [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) that I [nathan](../../strongs/h/h5414.md) to your ['ab](../../strongs/h/h1.md); and ye shall be my ['am](../../strongs/h/h5971.md), and I will be your ['Elohiym](../../strongs/h/h430.md).

<a name="ezekiel_36_29"></a>Ezekiel 36:29

I will also [yasha'](../../strongs/h/h3467.md) you from all your [ṭām'â](../../strongs/h/h2932.md): and I will [qara'](../../strongs/h/h7121.md) for the [dagan](../../strongs/h/h1715.md), and will [rabah](../../strongs/h/h7235.md) it, and [nathan](../../strongs/h/h5414.md) no [rāʿāḇ](../../strongs/h/h7458.md) upon you.

<a name="ezekiel_36_30"></a>Ezekiel 36:30

And I will [rabah](../../strongs/h/h7235.md) the [pĕriy](../../strongs/h/h6529.md) of the ['ets](../../strongs/h/h6086.md), and the [tᵊnûḇâ](../../strongs/h/h8570.md) of the [sadeh](../../strongs/h/h7704.md), that ye shall [laqach](../../strongs/h/h3947.md) no more [cherpah](../../strongs/h/h2781.md) of [rāʿāḇ](../../strongs/h/h7458.md) among the [gowy](../../strongs/h/h1471.md).

<a name="ezekiel_36_31"></a>Ezekiel 36:31

Then shall ye [zakar](../../strongs/h/h2142.md) your own [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md), and your [maʿălāl](../../strongs/h/h4611.md) that were not [towb](../../strongs/h/h2896.md), and shall [qûṭ](../../strongs/h/h6962.md) yourselves in your own [paniym](../../strongs/h/h6440.md) for your ['avon](../../strongs/h/h5771.md) and for your [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="ezekiel_36_32"></a>Ezekiel 36:32

Not for your sakes ['asah](../../strongs/h/h6213.md) I this, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), be it [yada'](../../strongs/h/h3045.md) unto you: be [buwsh](../../strongs/h/h954.md) and [kālam](../../strongs/h/h3637.md) for your own [derek](../../strongs/h/h1870.md), O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_36_33"></a>Ezekiel 36:33

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); In the [yowm](../../strongs/h/h3117.md) that I shall have [ṭāhēr](../../strongs/h/h2891.md) you from all your ['avon](../../strongs/h/h5771.md) I will also cause you to [yashab](../../strongs/h/h3427.md) in the [ʿîr](../../strongs/h/h5892.md), and the [chorbah](../../strongs/h/h2723.md) shall be [bānâ](../../strongs/h/h1129.md).

<a name="ezekiel_36_34"></a>Ezekiel 36:34

And the [šāmēm](../../strongs/h/h8074.md) ['erets](../../strongs/h/h776.md) shall be ['abad](../../strongs/h/h5647.md), whereas it lay [šᵊmāmâ](../../strongs/h/h8077.md) in the ['ayin](../../strongs/h/h5869.md) of all that ['abar](../../strongs/h/h5674.md).

<a name="ezekiel_36_35"></a>Ezekiel 36:35

And they shall ['āmar](../../strongs/h/h559.md), [hallēzû](../../strongs/h/h1977.md) ['erets](../../strongs/h/h776.md) that was [šāmēm](../../strongs/h/h8074.md) is become like the [gan](../../strongs/h/h1588.md) of ['Eden](../../strongs/h/h5731.md); and the [ḥārēḇ](../../strongs/h/h2720.md) and [šāmēm](../../strongs/h/h8074.md) and [harac](../../strongs/h/h2040.md) [ʿîr](../../strongs/h/h5892.md) are become [bāṣar](../../strongs/h/h1219.md), and are [yashab](../../strongs/h/h3427.md).

<a name="ezekiel_36_36"></a>Ezekiel 36:36

Then the [gowy](../../strongs/h/h1471.md) that are [šā'ar](../../strongs/h/h7604.md) [cabiyb](../../strongs/h/h5439.md) you shall [yada'](../../strongs/h/h3045.md) that I [Yĕhovah](../../strongs/h/h3068.md) [bānâ](../../strongs/h/h1129.md) the [harac](../../strongs/h/h2040.md) places, and [nāṭaʿ](../../strongs/h/h5193.md) that that was [šāmēm](../../strongs/h/h8074.md): I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) it, and I will ['asah](../../strongs/h/h6213.md) it.

<a name="ezekiel_36_37"></a>Ezekiel 36:37

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); I will yet for this be [darash](../../strongs/h/h1875.md) of by the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), to ['asah](../../strongs/h/h6213.md) it for them; I will [rabah](../../strongs/h/h7235.md) them with ['āḏām](../../strongs/h/h120.md) like a [tso'n](../../strongs/h/h6629.md).

<a name="ezekiel_36_38"></a>Ezekiel 36:38

As the [qodesh](../../strongs/h/h6944.md) [tso'n](../../strongs/h/h6629.md), as the [tso'n](../../strongs/h/h6629.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) in her solemn [môʿēḏ](../../strongs/h/h4150.md); so shall the [ḥārēḇ](../../strongs/h/h2720.md) [ʿîr](../../strongs/h/h5892.md) be [mālē'](../../strongs/h/h4392.md) with [tso'n](../../strongs/h/h6629.md) of ['āḏām](../../strongs/h/h120.md): and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 35](ezekiel_35.md) - [Ezekiel 37](ezekiel_37.md)