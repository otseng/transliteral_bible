# [Ezekiel 16](https://www.blueletterbible.org/kjv/ezekiel/16)

<a name="ezekiel_16_1"></a>Ezekiel 16:1

Again the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_16_2"></a>Ezekiel 16:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), cause [Yĕruwshalaim](../../strongs/h/h3389.md) to [yada'](../../strongs/h/h3045.md) her [tôʿēḇâ](../../strongs/h/h8441.md),

<a name="ezekiel_16_3"></a>Ezekiel 16:3

And ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) unto [Yĕruwshalaim](../../strongs/h/h3389.md); Thy [mᵊḵôrâ](../../strongs/h/h4351.md) and thy [môleḏeṯ](../../strongs/h/h4138.md) is of the ['erets](../../strongs/h/h776.md) of [Kᵊnaʿănî](../../strongs/h/h3669.md); thy ['ab](../../strongs/h/h1.md) was an ['Ĕmōrî](../../strongs/h/h567.md), and thy ['em](../../strongs/h/h517.md) an [Ḥitî](../../strongs/h/h2850.md).

<a name="ezekiel_16_4"></a>Ezekiel 16:4

And as for thy [môleḏeṯ](../../strongs/h/h4138.md), in the [yowm](../../strongs/h/h3117.md) thou wast [yalad](../../strongs/h/h3205.md) thy [šōr](../../strongs/h/h8270.md) was not [karath](../../strongs/h/h3772.md), neither wast thou [rāḥaṣ](../../strongs/h/h7364.md) in [mayim](../../strongs/h/h4325.md) to [mišʿî](../../strongs/h/h4935.md) thee; thou wast not [mālaḥ](../../strongs/h/h4414.md) at [mālaḥ](../../strongs/h/h4414.md), nor [ḥāṯal](../../strongs/h/h2853.md) at [ḥāṯal](../../strongs/h/h2853.md).

<a name="ezekiel_16_5"></a>Ezekiel 16:5

None ['ayin](../../strongs/h/h5869.md) [ḥûs](../../strongs/h/h2347.md) thee, to ['asah](../../strongs/h/h6213.md) any of these unto thee, to have [ḥāmal](../../strongs/h/h2550.md) upon thee; but thou wast [shalak](../../strongs/h/h7993.md) in the [paniym](../../strongs/h/h6440.md) [sadeh](../../strongs/h/h7704.md), to the [gōʿal](../../strongs/h/h1604.md) of thy [nephesh](../../strongs/h/h5315.md), in the [yowm](../../strongs/h/h3117.md) that thou wast [yalad](../../strongs/h/h3205.md).

<a name="ezekiel_16_6"></a>Ezekiel 16:6

And when I ['abar](../../strongs/h/h5674.md) by thee, and [ra'ah](../../strongs/h/h7200.md) thee [bûs](../../strongs/h/h947.md) in thine own [dam](../../strongs/h/h1818.md), I ['āmar](../../strongs/h/h559.md) unto thee when thou wast in thy [dam](../../strongs/h/h1818.md), [ḥāyâ](../../strongs/h/h2421.md); yea, I ['āmar](../../strongs/h/h559.md) unto thee when thou wast in thy [dam](../../strongs/h/h1818.md), [ḥāyâ](../../strongs/h/h2421.md).

<a name="ezekiel_16_7"></a>Ezekiel 16:7

I have [nathan](../../strongs/h/h5414.md) thee to multiply as the [ṣemaḥ](../../strongs/h/h6780.md) of the [sadeh](../../strongs/h/h7704.md), and thou hast [rabah](../../strongs/h/h7235.md) and [gāḏal](../../strongs/h/h1431.md), and thou art [bow'](../../strongs/h/h935.md) to [ʿădiy](../../strongs/h/h5716.md) [ʿădiy](../../strongs/h/h5716.md): thy [šaḏ](../../strongs/h/h7699.md) are [kuwn](../../strongs/h/h3559.md), and thine [śēʿār](../../strongs/h/h8181.md) is [ṣāmaḥ](../../strongs/h/h6779.md), whereas thou wast ['eyrom](../../strongs/h/h5903.md) and [ʿeryâ](../../strongs/h/h6181.md).

<a name="ezekiel_16_8"></a>Ezekiel 16:8

Now when I ['abar](../../strongs/h/h5674.md) by thee, and [ra'ah](../../strongs/h/h7200.md) upon thee, behold, thy [ʿēṯ](../../strongs/h/h6256.md) was the [ʿēṯ](../../strongs/h/h6256.md) of [dôḏ](../../strongs/h/h1730.md); and I [pāraś](../../strongs/h/h6566.md) my [kanaph](../../strongs/h/h3671.md) over thee, and [kāsâ](../../strongs/h/h3680.md) thy [ʿervâ](../../strongs/h/h6172.md): yea, I [shaba'](../../strongs/h/h7650.md) unto thee, and [bow'](../../strongs/h/h935.md) into a [bĕriyth](../../strongs/h/h1285.md) with thee, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), and thou becamest mine.

<a name="ezekiel_16_9"></a>Ezekiel 16:9

Then [rāḥaṣ](../../strongs/h/h7364.md) I thee with [mayim](../../strongs/h/h4325.md); yea, I throughly [šāṭap̄](../../strongs/h/h7857.md) thy [dam](../../strongs/h/h1818.md) from thee, and I [sûḵ](../../strongs/h/h5480.md) thee with [šemen](../../strongs/h/h8081.md).

<a name="ezekiel_16_10"></a>Ezekiel 16:10

I [labash](../../strongs/h/h3847.md) thee also with [riqmâ](../../strongs/h/h7553.md), and [nāʿal](../../strongs/h/h5274.md) thee with [taḥaš](../../strongs/h/h8476.md), and I [ḥāḇaš](../../strongs/h/h2280.md) thee about with [šēš](../../strongs/h/h8336.md), and I [kāsâ](../../strongs/h/h3680.md) thee with [mᵊšî](../../strongs/h/h4897.md).

<a name="ezekiel_16_11"></a>Ezekiel 16:11

I [ʿāḏâ](../../strongs/h/h5710.md) thee also with [ʿădiy](../../strongs/h/h5716.md), and I [nathan](../../strongs/h/h5414.md) [ṣāmîḏ](../../strongs/h/h6781.md) upon thy [yad](../../strongs/h/h3027.md), and a [rāḇîḏ](../../strongs/h/h7242.md) on thy [garown](../../strongs/h/h1627.md).

<a name="ezekiel_16_12"></a>Ezekiel 16:12

And I [nathan](../../strongs/h/h5414.md) a [nezem](../../strongs/h/h5141.md) on thy ['aph](../../strongs/h/h639.md), and [ʿāḡîl](../../strongs/h/h5694.md) in thine ['ozen](../../strongs/h/h241.md), and a [tip̄'ārâ](../../strongs/h/h8597.md) [ʿăṭārâ](../../strongs/h/h5850.md) upon thine [ro'sh](../../strongs/h/h7218.md).

<a name="ezekiel_16_13"></a>Ezekiel 16:13

Thus wast thou [ʿāḏâ](../../strongs/h/h5710.md) with [zāhāḇ](../../strongs/h/h2091.md) and [keceph](../../strongs/h/h3701.md); and thy [malbûš](../../strongs/h/h4403.md) was of fine [šēš](../../strongs/h/h8336.md) [šēš](../../strongs/h/h8336.md), and [mᵊšî](../../strongs/h/h4897.md), and [riqmâ](../../strongs/h/h7553.md); thou didst ['akal](../../strongs/h/h398.md) [sōleṯ](../../strongs/h/h5560.md), and [dĕbash](../../strongs/h/h1706.md), and [šemen](../../strongs/h/h8081.md): and thou wast [me'od](../../strongs/h/h3966.md) [yāp̄â](../../strongs/h/h3302.md), and thou didst [tsalach](../../strongs/h/h6743.md) into a [mᵊlûḵâ](../../strongs/h/h4410.md).

<a name="ezekiel_16_14"></a>Ezekiel 16:14

And thy [shem](../../strongs/h/h8034.md) [yāṣā'](../../strongs/h/h3318.md) among the [gowy](../../strongs/h/h1471.md) for thy [yᵊp̄î](../../strongs/h/h3308.md): for it was [kālîl](../../strongs/h/h3632.md) through my [hadar](../../strongs/h/h1926.md), which I had [śûm](../../strongs/h/h7760.md) upon thee, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_16_15"></a>Ezekiel 16:15

But thou didst [batach](../../strongs/h/h982.md) in thine own [yᵊp̄î](../../strongs/h/h3308.md), and playedst the [zānâ](../../strongs/h/h2181.md) because of thy [shem](../../strongs/h/h8034.md), and [šāp̄aḵ](../../strongs/h/h8210.md) thy [taznûṯ](../../strongs/h/h8457.md) on every one that ['abar](../../strongs/h/h5674.md); his it was.

<a name="ezekiel_16_16"></a>Ezekiel 16:16

And of thy [beḡeḏ](../../strongs/h/h899.md) thou didst [laqach](../../strongs/h/h3947.md), and ['asah](../../strongs/h/h6213.md) thy [bāmâ](../../strongs/h/h1116.md) with divers [ṭālā'](../../strongs/h/h2921.md), and playedst the [zānâ](../../strongs/h/h2181.md) thereupon: the like things shall not [bow'](../../strongs/h/h935.md), neither shall it be so.

<a name="ezekiel_16_17"></a>Ezekiel 16:17

Thou hast also [laqach](../../strongs/h/h3947.md) thy [tip̄'ārâ](../../strongs/h/h8597.md) [kĕliy](../../strongs/h/h3627.md) of my [zāhāḇ](../../strongs/h/h2091.md) and of my [keceph](../../strongs/h/h3701.md), which I had [nathan](../../strongs/h/h5414.md) thee, and ['asah](../../strongs/h/h6213.md) to thyself [tselem](../../strongs/h/h6754.md) of [zāḵār](../../strongs/h/h2145.md), and didst commit [zānâ](../../strongs/h/h2181.md) with them,

<a name="ezekiel_16_18"></a>Ezekiel 16:18

And [laqach](../../strongs/h/h3947.md) thy [riqmâ](../../strongs/h/h7553.md) [beḡeḏ](../../strongs/h/h899.md), and [kāsâ](../../strongs/h/h3680.md) them: and thou hast [nathan](../../strongs/h/h5414.md) mine [šemen](../../strongs/h/h8081.md) and mine [qᵊṭōreṯ](../../strongs/h/h7004.md) [paniym](../../strongs/h/h6440.md) them.

<a name="ezekiel_16_19"></a>Ezekiel 16:19

My [lechem](../../strongs/h/h3899.md) also which I [nathan](../../strongs/h/h5414.md) thee, [sōleṯ](../../strongs/h/h5560.md), and [šemen](../../strongs/h/h8081.md), and [dĕbash](../../strongs/h/h1706.md), wherewith I ['akal](../../strongs/h/h398.md) thee, thou hast even [nathan](../../strongs/h/h5414.md) it [paniym](../../strongs/h/h6440.md) them for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md): and thus it was, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_16_20"></a>Ezekiel 16:20

Moreover thou hast [laqach](../../strongs/h/h3947.md) thy [ben](../../strongs/h/h1121.md) and thy [bath](../../strongs/h/h1323.md), whom thou hast [yalad](../../strongs/h/h3205.md) unto me, and these hast thou [zabach](../../strongs/h/h2076.md) unto them to be ['akal](../../strongs/h/h398.md). Is this of thy [taznûṯ](../../strongs/h/h8457.md) a small [mᵊʿaṭ](../../strongs/h/h4592.md),

<a name="ezekiel_16_21"></a>Ezekiel 16:21

That thou hast [šāḥaṭ](../../strongs/h/h7819.md) my [ben](../../strongs/h/h1121.md), and [nathan](../../strongs/h/h5414.md) them to cause them to ['abar](../../strongs/h/h5674.md) for them?

<a name="ezekiel_16_22"></a>Ezekiel 16:22

And in all thine [tôʿēḇâ](../../strongs/h/h8441.md) and thy [taznûṯ](../../strongs/h/h8457.md) thou hast not [zakar](../../strongs/h/h2142.md) the [yowm](../../strongs/h/h3117.md) of thy [nāʿur](../../strongs/h/h5271.md), when thou wast ['eyrom](../../strongs/h/h5903.md) and [ʿeryâ](../../strongs/h/h6181.md), and wast [bûs](../../strongs/h/h947.md) in thy [dam](../../strongs/h/h1818.md).

<a name="ezekiel_16_23"></a>Ezekiel 16:23

And it came to pass ['aḥar](../../strongs/h/h310.md) all thy [ra'](../../strongs/h/h7451.md), (['owy](../../strongs/h/h188.md), ['owy](../../strongs/h/h188.md) unto thee! [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md);)

<a name="ezekiel_16_24"></a>Ezekiel 16:24

That thou hast also [bānâ](../../strongs/h/h1129.md) unto thee a [gaḇ](../../strongs/h/h1354.md), and hast ['asah](../../strongs/h/h6213.md) thee a [rāmâ](../../strongs/h/h7413.md) in every [rᵊḥōḇ](../../strongs/h/h7339.md).

<a name="ezekiel_16_25"></a>Ezekiel 16:25

Thou hast [bānâ](../../strongs/h/h1129.md) thy [rāmâ](../../strongs/h/h7413.md) at every [ro'sh](../../strongs/h/h7218.md) of the [derek](../../strongs/h/h1870.md), and hast made thy [yᵊp̄î](../../strongs/h/h3308.md) to be [ta'ab](../../strongs/h/h8581.md), and hast [pāśaq](../../strongs/h/h6589.md) thy [regel](../../strongs/h/h7272.md) to every one that ['abar](../../strongs/h/h5674.md), and [rabah](../../strongs/h/h7235.md) thy [taznûṯ](../../strongs/h/h8457.md).

<a name="ezekiel_16_26"></a>Ezekiel 16:26

Thou hast also committed [zānâ](../../strongs/h/h2181.md) with the [ben](../../strongs/h/h1121.md) [Mitsrayim](../../strongs/h/h4714.md) thy [šāḵēn](../../strongs/h/h7934.md), [gāḏēl](../../strongs/h/h1432.md) of [basar](../../strongs/h/h1320.md); and hast [rabah](../../strongs/h/h7235.md) thy [taznûṯ](../../strongs/h/h8457.md), to provoke me to [kāʿas](../../strongs/h/h3707.md).

<a name="ezekiel_16_27"></a>Ezekiel 16:27

Behold, therefore I have [natah](../../strongs/h/h5186.md) my [yad](../../strongs/h/h3027.md) over thee, and have [gāraʿ](../../strongs/h/h1639.md) thine [choq](../../strongs/h/h2706.md), and [nathan](../../strongs/h/h5414.md) thee unto the [nephesh](../../strongs/h/h5315.md) of them that [sane'](../../strongs/h/h8130.md) thee, the [bath](../../strongs/h/h1323.md) of the [Pᵊlištî](../../strongs/h/h6430.md), which are [kālam](../../strongs/h/h3637.md) of thy [zimmâ](../../strongs/h/h2154.md) [derek](../../strongs/h/h1870.md).

<a name="ezekiel_16_28"></a>Ezekiel 16:28

Thou hast played the [zānâ](../../strongs/h/h2181.md) also with the [ben](../../strongs/h/h1121.md) ['Aššûr](../../strongs/h/h804.md), because thou wast [sāׂbaʿ](../../strongs/h/h7646.md); yea, thou hast played the [zānâ](../../strongs/h/h2181.md) with them, and yet couldest not be [śāḇʿâ](../../strongs/h/h7654.md).

<a name="ezekiel_16_29"></a>Ezekiel 16:29

Thou hast moreover [rabah](../../strongs/h/h7235.md) thy [taznûṯ](../../strongs/h/h8457.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md) unto [Kaśdîmâ](../../strongs/h/h3778.md); and yet thou wast not [sāׂbaʿ](../../strongs/h/h7646.md) herewith.

<a name="ezekiel_16_30"></a>Ezekiel 16:30

How ['āmal](../../strongs/h/h535.md) is thine [libbah](../../strongs/h/h3826.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), seeing thou ['asah](../../strongs/h/h6213.md) all these things, the [ma'aseh](../../strongs/h/h4639.md) of a [šalleṭeṯ](../../strongs/h/h7986.md) [zānâ](../../strongs/h/h2181.md) ['ishshah](../../strongs/h/h802.md);

<a name="ezekiel_16_31"></a>Ezekiel 16:31

In that thou [bānâ](../../strongs/h/h1129.md) thine [gaḇ](../../strongs/h/h1354.md) in the [ro'sh](../../strongs/h/h7218.md) of every [derek](../../strongs/h/h1870.md), and ['asah](../../strongs/h/h6213.md) thine high [rāmâ](../../strongs/h/h7413.md) in every [rᵊḥōḇ](../../strongs/h/h7339.md); and hast not been as a [zānâ](../../strongs/h/h2181.md), in that thou [qālas](../../strongs/h/h7046.md) ['eṯnan](../../strongs/h/h868.md);

<a name="ezekiel_16_32"></a>Ezekiel 16:32

But as an ['ishshah](../../strongs/h/h802.md) that committeth [na'aph](../../strongs/h/h5003.md), which [laqach](../../strongs/h/h3947.md) [zûr](../../strongs/h/h2114.md) instead of her ['iysh](../../strongs/h/h376.md)!

<a name="ezekiel_16_33"></a>Ezekiel 16:33

They [nathan](../../strongs/h/h5414.md) [nēḏê](../../strongs/h/h5078.md) to all [zānâ](../../strongs/h/h2181.md): but thou [nathan](../../strongs/h/h5414.md) thy [nāḏān](../../strongs/h/h5083.md) to all thy ['ahab](../../strongs/h/h157.md), and [šāḥaḏ](../../strongs/h/h7809.md) them, that they may [bow'](../../strongs/h/h935.md) unto thee [cabiyb](../../strongs/h/h5439.md) for thy [taznûṯ](../../strongs/h/h8457.md).

<a name="ezekiel_16_34"></a>Ezekiel 16:34

And the [hēp̄eḵ](../../strongs/h/h2016.md) is in thee from other ['ishshah](../../strongs/h/h802.md) in thy [taznûṯ](../../strongs/h/h8457.md), whereas none ['aḥar](../../strongs/h/h310.md) thee to commit [zānâ](../../strongs/h/h2181.md): and in that thou [nathan](../../strongs/h/h5414.md) an ['eṯnan](../../strongs/h/h868.md), and no ['eṯnan](../../strongs/h/h868.md) is [nathan](../../strongs/h/h5414.md) unto thee, therefore thou art [hēp̄eḵ](../../strongs/h/h2016.md).

<a name="ezekiel_16_35"></a>Ezekiel 16:35

Wherefore, O [zānâ](../../strongs/h/h2181.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md):

<a name="ezekiel_16_36"></a>Ezekiel 16:36

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because thy [nᵊḥšeṯ](../../strongs/h/h5178.md) was [šāp̄aḵ](../../strongs/h/h8210.md), and thy [ʿervâ](../../strongs/h/h6172.md) [gālâ](../../strongs/h/h1540.md) through thy [taznûṯ](../../strongs/h/h8457.md) with thy ['ahab](../../strongs/h/h157.md), and with all the [gillûl](../../strongs/h/h1544.md) of thy [tôʿēḇâ](../../strongs/h/h8441.md), and by the [dam](../../strongs/h/h1818.md) of thy [ben](../../strongs/h/h1121.md), which thou didst [nathan](../../strongs/h/h5414.md) unto them;

<a name="ezekiel_16_37"></a>Ezekiel 16:37

Behold, therefore I will [qāḇaṣ](../../strongs/h/h6908.md) all thy ['ahab](../../strongs/h/h157.md), with whom thou hast taken [ʿāraḇ](../../strongs/h/h6149.md), and all them that thou hast ['ahab](../../strongs/h/h157.md), with all them that thou hast [sane'](../../strongs/h/h8130.md); I will even [qāḇaṣ](../../strongs/h/h6908.md) them [cabiyb](../../strongs/h/h5439.md) against thee, and will [gālâ](../../strongs/h/h1540.md) thy [ʿervâ](../../strongs/h/h6172.md) unto them, that they may [ra'ah](../../strongs/h/h7200.md) all thy [ʿervâ](../../strongs/h/h6172.md).

<a name="ezekiel_16_38"></a>Ezekiel 16:38

And I will [shaphat](../../strongs/h/h8199.md) thee, as women that [na'aph](../../strongs/h/h5003.md) and [šāp̄aḵ](../../strongs/h/h8210.md) [dam](../../strongs/h/h1818.md) are [mishpat](../../strongs/h/h4941.md); and I will [nathan](../../strongs/h/h5414.md) thee [dam](../../strongs/h/h1818.md) in [chemah](../../strongs/h/h2534.md) and [qin'â](../../strongs/h/h7068.md).

<a name="ezekiel_16_39"></a>Ezekiel 16:39

And I will also [nathan](../../strongs/h/h5414.md) thee into their [yad](../../strongs/h/h3027.md), and they shall throw [harac](../../strongs/h/h2040.md) thine [gaḇ](../../strongs/h/h1354.md), and shall [nāṯaṣ](../../strongs/h/h5422.md) thy [rāmâ](../../strongs/h/h7413.md): they shall [pāšaṭ](../../strongs/h/h6584.md) thee also of thy [beḡeḏ](../../strongs/h/h899.md), and shall [laqach](../../strongs/h/h3947.md) thy [tip̄'ārâ](../../strongs/h/h8597.md) [kĕliy](../../strongs/h/h3627.md), and [yānaḥ](../../strongs/h/h3240.md) thee ['eyrom](../../strongs/h/h5903.md) and [ʿeryâ](../../strongs/h/h6181.md).

<a name="ezekiel_16_40"></a>Ezekiel 16:40

They shall also [ʿālâ](../../strongs/h/h5927.md) a [qāhēl](../../strongs/h/h6951.md) against thee, and they shall [rāḡam](../../strongs/h/h7275.md) thee with ['eben](../../strongs/h/h68.md), and thrust thee [bāṯaq](../../strongs/h/h1333.md) with their [chereb](../../strongs/h/h2719.md).

<a name="ezekiel_16_41"></a>Ezekiel 16:41

And they shall [śārap̄](../../strongs/h/h8313.md) thine [bayith](../../strongs/h/h1004.md) with ['esh](../../strongs/h/h784.md), and ['asah](../../strongs/h/h6213.md) [šep̄eṭ](../../strongs/h/h8201.md) upon thee in the ['ayin](../../strongs/h/h5869.md) of [rab](../../strongs/h/h7227.md) ['ishshah](../../strongs/h/h802.md): and I will cause thee to [shabath](../../strongs/h/h7673.md) from playing the [zānâ](../../strongs/h/h2181.md), and thou also shalt [nathan](../../strongs/h/h5414.md) no ['eṯnan](../../strongs/h/h868.md) any more.

<a name="ezekiel_16_42"></a>Ezekiel 16:42

So will I make my [chemah](../../strongs/h/h2534.md) toward thee to [nuwach](../../strongs/h/h5117.md), and my [qin'â](../../strongs/h/h7068.md) shall [cuwr](../../strongs/h/h5493.md) from thee, and I will be [šāqaṭ](../../strongs/h/h8252.md), and will be no more [kāʿas](../../strongs/h/h3707.md).

<a name="ezekiel_16_43"></a>Ezekiel 16:43

Because thou hast not [zakar](../../strongs/h/h2142.md) the [yowm](../../strongs/h/h3117.md) of thy [nāʿur](../../strongs/h/h5271.md), but hast [ragaz](../../strongs/h/h7264.md) me in all these things; [hē'](../../strongs/h/h1887.md), therefore I also will [nathan](../../strongs/h/h5414.md) thy [derek](../../strongs/h/h1870.md) upon thine [ro'sh](../../strongs/h/h7218.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md): and thou shalt not ['asah](../../strongs/h/h6213.md) this [zimmâ](../../strongs/h/h2154.md) above all thine [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="ezekiel_16_44"></a>Ezekiel 16:44

Behold, every one that useth [māšal](../../strongs/h/h4911.md) shall use this [māšal](../../strongs/h/h4911.md) against thee, ['āmar](../../strongs/h/h559.md), As is the ['em](../../strongs/h/h517.md), so is her [bath](../../strongs/h/h1323.md).

<a name="ezekiel_16_45"></a>Ezekiel 16:45

Thou art thy ['em](../../strongs/h/h517.md) [bath](../../strongs/h/h1323.md), that [gāʿal](../../strongs/h/h1602.md) her ['iysh](../../strongs/h/h376.md) and her [ben](../../strongs/h/h1121.md); and thou art the ['āḥôṯ](../../strongs/h/h269.md) of thy ['āḥôṯ](../../strongs/h/h269.md), which [gāʿal](../../strongs/h/h1602.md) their ['enowsh](../../strongs/h/h582.md) and their [ben](../../strongs/h/h1121.md): your ['em](../../strongs/h/h517.md) was an [Ḥitî](../../strongs/h/h2850.md), and your ['ab](../../strongs/h/h1.md) an ['Ĕmōrî](../../strongs/h/h567.md).

<a name="ezekiel_16_46"></a>Ezekiel 16:46

And thine [gadowl](../../strongs/h/h1419.md) ['āḥôṯ](../../strongs/h/h269.md) is [Šōmrôn](../../strongs/h/h8111.md), she and her [bath](../../strongs/h/h1323.md) that [yashab](../../strongs/h/h3427.md) at thy [śᵊmō'l](../../strongs/h/h8040.md): and thy [qāṭān](../../strongs/h/h6996.md) ['āḥôṯ](../../strongs/h/h269.md), that [yashab](../../strongs/h/h3427.md) at thy [yamiyn](../../strongs/h/h3225.md), is [Sᵊḏōm](../../strongs/h/h5467.md) and her [bath](../../strongs/h/h1323.md).

<a name="ezekiel_16_47"></a>Ezekiel 16:47

Yet hast thou not [halak](../../strongs/h/h1980.md) after their [derek](../../strongs/h/h1870.md), nor ['asah](../../strongs/h/h6213.md) after their [tôʿēḇâ](../../strongs/h/h8441.md): but, as if that were a [qāṭ](../../strongs/h/h6985.md) [mᵊʿaṭ](../../strongs/h/h4592.md) [qûṭ](../../strongs/h/h6962.md) thing, thou wast [shachath](../../strongs/h/h7843.md) more than they in all thy [derek](../../strongs/h/h1870.md).

<a name="ezekiel_16_48"></a>Ezekiel 16:48

As I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), [Sᵊḏōm](../../strongs/h/h5467.md) thy ['āḥôṯ](../../strongs/h/h269.md) hath not ['asah](../../strongs/h/h6213.md), she nor her [bath](../../strongs/h/h1323.md), as thou hast ['asah](../../strongs/h/h6213.md), thou and thy [bath](../../strongs/h/h1323.md).

<a name="ezekiel_16_49"></a>Ezekiel 16:49

Behold, this was the ['avon](../../strongs/h/h5771.md) of thy ['āḥôṯ](../../strongs/h/h269.md) [Sᵊḏōm](../../strongs/h/h5467.md), [gā'ôn](../../strongs/h/h1347.md), [śiḇʿâ](../../strongs/h/h7653.md) of [lechem](../../strongs/h/h3899.md), and [šalvâ](../../strongs/h/h7962.md) of [šāqaṭ](../../strongs/h/h8252.md) was in her and in her [bath](../../strongs/h/h1323.md), neither did she [ḥāzaq](../../strongs/h/h2388.md) the [yad](../../strongs/h/h3027.md) of the ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md).

<a name="ezekiel_16_50"></a>Ezekiel 16:50

And they were [gāḇah](../../strongs/h/h1361.md), and ['asah](../../strongs/h/h6213.md) [tôʿēḇâ](../../strongs/h/h8441.md) [paniym](../../strongs/h/h6440.md) me: therefore I took them [cuwr](../../strongs/h/h5493.md) as I [ra'ah](../../strongs/h/h7200.md).

<a name="ezekiel_16_51"></a>Ezekiel 16:51

Neither hath [Šōmrôn](../../strongs/h/h8111.md) [chata'](../../strongs/h/h2398.md) [ḥēṣî](../../strongs/h/h2677.md) of thy [chatta'ath](../../strongs/h/h2403.md); but thou hast [rabah](../../strongs/h/h7235.md) thine [tôʿēḇâ](../../strongs/h/h8441.md) more than they, and hast [ṣāḏaq](../../strongs/h/h6663.md) thy ['āḥôṯ](../../strongs/h/h269.md) in all thine [tôʿēḇâ](../../strongs/h/h8441.md) which thou hast ['asah](../../strongs/h/h6213.md).

<a name="ezekiel_16_52"></a>Ezekiel 16:52

Thou also, which hast [palal](../../strongs/h/h6419.md) thy ['āḥôṯ](../../strongs/h/h269.md), [nasa'](../../strongs/h/h5375.md) thine own [kĕlimmah](../../strongs/h/h3639.md) for thy [chatta'ath](../../strongs/h/h2403.md) that thou hast committed more [ta'ab](../../strongs/h/h8581.md) than they: they are more [ṣāḏaq](../../strongs/h/h6663.md) than thou: yea, be thou [buwsh](../../strongs/h/h954.md) also, and [nasa'](../../strongs/h/h5375.md) thy [kĕlimmah](../../strongs/h/h3639.md), in that thou hast [ṣāḏaq](../../strongs/h/h6663.md) thy ['āḥôṯ](../../strongs/h/h269.md).

<a name="ezekiel_16_53"></a>Ezekiel 16:53

When I shall [shuwb](../../strongs/h/h7725.md) their [shebuwth](../../strongs/h/h7622.md) [shebuwth](../../strongs/h/h7622.md), the [shebuwth](../../strongs/h/h7622.md) [shebuwth](../../strongs/h/h7622.md) of [Sᵊḏōm](../../strongs/h/h5467.md) and her [bath](../../strongs/h/h1323.md), and the [shebuwth](../../strongs/h/h7622.md) [shebuwth](../../strongs/h/h7622.md) of [Šōmrôn](../../strongs/h/h8111.md) and her [bath](../../strongs/h/h1323.md), then will I bring again the [shebuwth](../../strongs/h/h7622.md) [shebuwth](../../strongs/h/h7622.md) of thy [shebuwth](../../strongs/h/h7622.md) in the midst of them:

<a name="ezekiel_16_54"></a>Ezekiel 16:54

That thou mayest [nasa'](../../strongs/h/h5375.md) thine own [kĕlimmah](../../strongs/h/h3639.md), and mayest be [kālam](../../strongs/h/h3637.md) in all that thou hast ['asah](../../strongs/h/h6213.md), in that thou art a [nacham](../../strongs/h/h5162.md) unto them.

<a name="ezekiel_16_55"></a>Ezekiel 16:55

When thy ['āḥôṯ](../../strongs/h/h269.md), [Sᵊḏōm](../../strongs/h/h5467.md) and her [bath](../../strongs/h/h1323.md), shall [shuwb](../../strongs/h/h7725.md) to their former [qaḏmâ](../../strongs/h/h6927.md), and [Šōmrôn](../../strongs/h/h8111.md) and her [bath](../../strongs/h/h1323.md) shall [shuwb](../../strongs/h/h7725.md) to their former [qaḏmâ](../../strongs/h/h6927.md), then thou and thy [bath](../../strongs/h/h1323.md) shall [shuwb](../../strongs/h/h7725.md) to your former [qaḏmâ](../../strongs/h/h6927.md).

<a name="ezekiel_16_56"></a>Ezekiel 16:56

For thy ['āḥôṯ](../../strongs/h/h269.md) [Sᵊḏōm](../../strongs/h/h5467.md) was not [šᵊmûʿâ](../../strongs/h/h8052.md) by thy [peh](../../strongs/h/h6310.md) in the [yowm](../../strongs/h/h3117.md) of thy [gā'ôn](../../strongs/h/h1347.md),

<a name="ezekiel_16_57"></a>Ezekiel 16:57

Before thy [ra'](../../strongs/h/h7451.md) was [gālâ](../../strongs/h/h1540.md), as at the [ʿēṯ](../../strongs/h/h6256.md) of thy [cherpah](../../strongs/h/h2781.md) of the [bath](../../strongs/h/h1323.md) of ['Ărām](../../strongs/h/h758.md), and all that are [cabiyb](../../strongs/h/h5439.md) her, the [bath](../../strongs/h/h1323.md) of the [Pᵊlištî](../../strongs/h/h6430.md), which [shâʼṭ](../../strongs/h/h7590.md) thee [cabiyb](../../strongs/h/h5439.md).

<a name="ezekiel_16_58"></a>Ezekiel 16:58

Thou hast [nasa'](../../strongs/h/h5375.md) thy [zimmâ](../../strongs/h/h2154.md) and thine [tôʿēḇâ](../../strongs/h/h8441.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_16_59"></a>Ezekiel 16:59

For thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); I will even ['asah](../../strongs/h/h6213.md) with thee as thou hast ['asah](../../strongs/h/h6213.md), which hast [bazah](../../strongs/h/h959.md) the ['alah](../../strongs/h/h423.md) in [pārar](../../strongs/h/h6565.md) the [bĕriyth](../../strongs/h/h1285.md).

<a name="ezekiel_16_60"></a>Ezekiel 16:60

Nevertheless I will [zakar](../../strongs/h/h2142.md) my [bĕriyth](../../strongs/h/h1285.md) with thee in the [yowm](../../strongs/h/h3117.md) of thy [nāʿur](../../strongs/h/h5271.md), and I will [quwm](../../strongs/h/h6965.md) unto thee an ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md).

<a name="ezekiel_16_61"></a>Ezekiel 16:61

Then thou shalt [zakar](../../strongs/h/h2142.md) thy [derek](../../strongs/h/h1870.md), and be [kālam](../../strongs/h/h3637.md), when thou shalt [laqach](../../strongs/h/h3947.md) thy ['āḥôṯ](../../strongs/h/h269.md), thine [gadowl](../../strongs/h/h1419.md) and thy [qāṭān](../../strongs/h/h6996.md): and I will [nathan](../../strongs/h/h5414.md) them unto thee for [bath](../../strongs/h/h1323.md), but not by thy [bĕriyth](../../strongs/h/h1285.md).

<a name="ezekiel_16_62"></a>Ezekiel 16:62

And I will [quwm](../../strongs/h/h6965.md) my [bĕriyth](../../strongs/h/h1285.md) with thee; and thou shalt [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md):

<a name="ezekiel_16_63"></a>Ezekiel 16:63

That thou mayest [zakar](../../strongs/h/h2142.md), and be [buwsh](../../strongs/h/h954.md), and never [pitāḥôn](../../strongs/h/h6610.md) thy [peh](../../strongs/h/h6310.md) any more [paniym](../../strongs/h/h6440.md) of thy [kĕlimmah](../../strongs/h/h3639.md), when I am [kāp̄ar](../../strongs/h/h3722.md) toward thee for all that thou hast ['asah](../../strongs/h/h6213.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 15](ezekiel_15.md) - [Ezekiel 17](ezekiel_17.md)