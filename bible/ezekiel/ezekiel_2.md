# [Ezekiel 2](https://www.blueletterbible.org/kjv/ezekiel/2)

<a name="ezekiel_2_1"></a>Ezekiel 2:1

And he ['āmar](../../strongs/h/h559.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), ['amad](../../strongs/h/h5975.md) upon thy [regel](../../strongs/h/h7272.md), and I will [dabar](../../strongs/h/h1696.md) unto thee.

<a name="ezekiel_2_2"></a>Ezekiel 2:2

And the [ruwach](../../strongs/h/h7307.md) [bow'](../../strongs/h/h935.md) into me when he [dabar](../../strongs/h/h1696.md) unto me, and ['amad](../../strongs/h/h5975.md) me upon my [regel](../../strongs/h/h7272.md), that I [shama'](../../strongs/h/h8085.md) him that [dabar](../../strongs/h/h1696.md) unto me.

<a name="ezekiel_2_3"></a>Ezekiel 2:3

And he ['āmar](../../strongs/h/h559.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), I [shalach](../../strongs/h/h7971.md) thee to the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), to a [māraḏ](../../strongs/h/h4775.md) [gowy](../../strongs/h/h1471.md) that hath [māraḏ](../../strongs/h/h4775.md) against me: they and their ['ab](../../strongs/h/h1.md) have [pāšaʿ](../../strongs/h/h6586.md) against me, even unto this ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md).

<a name="ezekiel_2_4"></a>Ezekiel 2:4

For they are [qāšê](../../strongs/h/h7186.md) [paniym](../../strongs/h/h6440.md) [ben](../../strongs/h/h1121.md) and [ḥāzāq](../../strongs/h/h2389.md) [leb](../../strongs/h/h3820.md). I do [shalach](../../strongs/h/h7971.md) thee unto them; and thou shalt ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_2_5"></a>Ezekiel 2:5

And they, whether they will [shama'](../../strongs/h/h8085.md), or whether they will [ḥāḏal](../../strongs/h/h2308.md), (for they are a [mᵊrî](../../strongs/h/h4805.md) [bayith](../../strongs/h/h1004.md),) yet shall [yada'](../../strongs/h/h3045.md) that there hath been a [nāḇî'](../../strongs/h/h5030.md) among them.

<a name="ezekiel_2_6"></a>Ezekiel 2:6

And thou, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), be not [yare'](../../strongs/h/h3372.md) of them, neither be [yare'](../../strongs/h/h3372.md) of their [dabar](../../strongs/h/h1697.md), though [sārāḇ](../../strongs/h/h5621.md) and [sallôn](../../strongs/h/h5544.md) be with thee, and thou dost [yashab](../../strongs/h/h3427.md) among [ʿaqrāḇ](../../strongs/h/h6137.md): be not [yare'](../../strongs/h/h3372.md) of their [dabar](../../strongs/h/h1697.md), nor be [ḥāṯaṯ](../../strongs/h/h2865.md) at their [paniym](../../strongs/h/h6440.md), though they be a [mᵊrî](../../strongs/h/h4805.md) [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_2_7"></a>Ezekiel 2:7

And thou shalt [dabar](../../strongs/h/h1696.md) my [dabar](../../strongs/h/h1697.md) unto them, whether they will [shama'](../../strongs/h/h8085.md), or whether they will [ḥāḏal](../../strongs/h/h2308.md): for they are [mᵊrî](../../strongs/h/h4805.md).

<a name="ezekiel_2_8"></a>Ezekiel 2:8

But thou, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [shama'](../../strongs/h/h8085.md) what I [dabar](../../strongs/h/h1696.md) unto thee; Be not thou [mᵊrî](../../strongs/h/h4805.md) like that [mᵊrî](../../strongs/h/h4805.md) [bayith](../../strongs/h/h1004.md): [pāṣâ](../../strongs/h/h6475.md) thy [peh](../../strongs/h/h6310.md), and ['akal](../../strongs/h/h398.md) that I [nathan](../../strongs/h/h5414.md) thee.

<a name="ezekiel_2_9"></a>Ezekiel 2:9

And when I [ra'ah](../../strongs/h/h7200.md), behold, a [yad](../../strongs/h/h3027.md) was [shalach](../../strongs/h/h7971.md) unto me; and, lo, a [mᵊḡillâ](../../strongs/h/h4039.md) of a [sēp̄er](../../strongs/h/h5612.md) was therein;

<a name="ezekiel_2_10"></a>Ezekiel 2:10

And he [pāraś](../../strongs/h/h6566.md) it [paniym](../../strongs/h/h6440.md) me; and it was [kāṯaḇ](../../strongs/h/h3789.md) [paniym](../../strongs/h/h6440.md) and ['āḥôr](../../strongs/h/h268.md): and there was [kāṯaḇ](../../strongs/h/h3789.md) therein [qînâ](../../strongs/h/h7015.md), and [heḡê](../../strongs/h/h1899.md), and [hî](../../strongs/h/h1958.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 1](ezekiel_1.md) - [Ezekiel 3](ezekiel_3.md)