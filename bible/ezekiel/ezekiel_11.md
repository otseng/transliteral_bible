# [Ezekiel 11](https://www.blueletterbible.org/kjv/ezekiel/11)

<a name="ezekiel_11_1"></a>Ezekiel 11:1

Moreover the [ruwach](../../strongs/h/h7307.md) [nasa'](../../strongs/h/h5375.md) me, and [bow'](../../strongs/h/h935.md) me unto the [qaḏmōnî](../../strongs/h/h6931.md) [sha'ar](../../strongs/h/h8179.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md), which [panah](../../strongs/h/h6437.md) [qāḏîm](../../strongs/h/h6921.md): and behold at the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md) five and twenty ['iysh](../../strongs/h/h376.md); among whom I [ra'ah](../../strongs/h/h7200.md) [Ya'Ăzanyâ](../../strongs/h/h2970.md) the [ben](../../strongs/h/h1121.md) of [ʿAzzûr](../../strongs/h/h5809.md), and [Pᵊlaṭyâ](../../strongs/h/h6410.md) the [ben](../../strongs/h/h1121.md) of [Bᵊnāyâ](../../strongs/h/h1141.md), [śar](../../strongs/h/h8269.md) of the ['am](../../strongs/h/h5971.md).

<a name="ezekiel_11_2"></a>Ezekiel 11:2

Then ['āmar](../../strongs/h/h559.md) he unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), these are the ['enowsh](../../strongs/h/h582.md) that [chashab](../../strongs/h/h2803.md) ['aven](../../strongs/h/h205.md), and [ya'ats](../../strongs/h/h3289.md) [ra'](../../strongs/h/h7451.md) ['etsah](../../strongs/h/h6098.md) in this [ʿîr](../../strongs/h/h5892.md):

<a name="ezekiel_11_3"></a>Ezekiel 11:3

Which ['āmar](../../strongs/h/h559.md), It is not [qarowb](../../strongs/h/h7138.md); let us [bānâ](../../strongs/h/h1129.md) [bayith](../../strongs/h/h1004.md): this is the [sîr](../../strongs/h/h5518.md), and we be the [basar](../../strongs/h/h1320.md).

<a name="ezekiel_11_4"></a>Ezekiel 11:4

Therefore [nāḇā'](../../strongs/h/h5012.md) against them, [nāḇā'](../../strongs/h/h5012.md), O [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md).

<a name="ezekiel_11_5"></a>Ezekiel 11:5

And the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) [naphal](../../strongs/h/h5307.md) upon me, and ['āmar](../../strongs/h/h559.md) unto me, ['āmar](../../strongs/h/h559.md); Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Thus have ye ['āmar](../../strongs/h/h559.md), O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md): for I [yada'](../../strongs/h/h3045.md) the things that [maʿălâ](../../strongs/h/h4609.md) into your [ruwach](../../strongs/h/h7307.md), every one of them.

<a name="ezekiel_11_6"></a>Ezekiel 11:6

Ye have [rabah](../../strongs/h/h7235.md) your [ḥālāl](../../strongs/h/h2491.md) in this [ʿîr](../../strongs/h/h5892.md), and ye have [mālā'](../../strongs/h/h4390.md) the [ḥûṣ](../../strongs/h/h2351.md) thereof with the [ḥālāl](../../strongs/h/h2491.md).

<a name="ezekiel_11_7"></a>Ezekiel 11:7

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Your [ḥālāl](../../strongs/h/h2491.md) whom ye have [śûm](../../strongs/h/h7760.md) in the midst of it, they are the [basar](../../strongs/h/h1320.md), and this the [sîr](../../strongs/h/h5518.md): but I will [yāṣā'](../../strongs/h/h3318.md) you out of the midst of it.

<a name="ezekiel_11_8"></a>Ezekiel 11:8

Ye have [yare'](../../strongs/h/h3372.md) the [chereb](../../strongs/h/h2719.md); and I will [bow'](../../strongs/h/h935.md) a [chereb](../../strongs/h/h2719.md) upon you, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_11_9"></a>Ezekiel 11:9

And I will bring you [yāṣā'](../../strongs/h/h3318.md) of the midst thereof, and [nathan](../../strongs/h/h5414.md) you into the [yad](../../strongs/h/h3027.md) of [zûr](../../strongs/h/h2114.md), and will ['asah](../../strongs/h/h6213.md) [šep̄eṭ](../../strongs/h/h8201.md) among you.

<a name="ezekiel_11_10"></a>Ezekiel 11:10

Ye shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md); I will [shaphat](../../strongs/h/h8199.md) you in the [gᵊḇûl](../../strongs/h/h1366.md) of [Yisra'el](../../strongs/h/h3478.md); and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_11_11"></a>Ezekiel 11:11

This shall not be your [sîr](../../strongs/h/h5518.md), neither shall ye be the [basar](../../strongs/h/h1320.md) in the midst thereof; but I will [shaphat](../../strongs/h/h8199.md) you in the [gᵊḇûl](../../strongs/h/h1366.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="ezekiel_11_12"></a>Ezekiel 11:12

And ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md): for ye have not [halak](../../strongs/h/h1980.md) in my [choq](../../strongs/h/h2706.md), neither ['asah](../../strongs/h/h6213.md) my [mishpat](../../strongs/h/h4941.md), but have ['asah](../../strongs/h/h6213.md) after the [mishpat](../../strongs/h/h4941.md) of the [gowy](../../strongs/h/h1471.md) that are [cabiyb](../../strongs/h/h5439.md) you.

<a name="ezekiel_11_13"></a>Ezekiel 11:13

And it came to pass, when I [nāḇā'](../../strongs/h/h5012.md), that [Pᵊlaṭyâ](../../strongs/h/h6410.md) the [ben](../../strongs/h/h1121.md) of [Bᵊnāyâ](../../strongs/h/h1141.md) [muwth](../../strongs/h/h4191.md). Then I [naphal](../../strongs/h/h5307.md) upon my [paniym](../../strongs/h/h6440.md), and [zāʿaq](../../strongs/h/h2199.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md), and ['āmar](../../strongs/h/h559.md), ['ăhâ](../../strongs/h/h162.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md)! wilt thou ['asah](../../strongs/h/h6213.md) a full [kālâ](../../strongs/h/h3617.md) of the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="ezekiel_11_14"></a>Ezekiel 11:14

Again the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_11_15"></a>Ezekiel 11:15

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), thy ['ach](../../strongs/h/h251.md), even thy ['ach](../../strongs/h/h251.md), the ['enowsh](../../strongs/h/h582.md) of thy [gᵊ'ullâ](../../strongs/h/h1353.md), and all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) wholly, are they unto whom the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) have ['āmar](../../strongs/h/h559.md), [rachaq](../../strongs/h/h7368.md) you from [Yĕhovah](../../strongs/h/h3068.md): unto us is this ['erets](../../strongs/h/h776.md) [nathan](../../strongs/h/h5414.md) in [môrāšâ](../../strongs/h/h4181.md).

<a name="ezekiel_11_16"></a>Ezekiel 11:16

Therefore ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Although I have [rachaq](../../strongs/h/h7368.md) them among the [gowy](../../strongs/h/h1471.md), and although I have [puwts](../../strongs/h/h6327.md) them among the ['erets](../../strongs/h/h776.md), yet will I be to them as a [mᵊʿaṭ](../../strongs/h/h4592.md) [miqdash](../../strongs/h/h4720.md) in the ['erets](../../strongs/h/h776.md) where they shall [bow'](../../strongs/h/h935.md).

<a name="ezekiel_11_17"></a>Ezekiel 11:17

Therefore ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); I will even [qāḇaṣ](../../strongs/h/h6908.md) you from the ['am](../../strongs/h/h5971.md), and ['āsap̄](../../strongs/h/h622.md) you out of the ['erets](../../strongs/h/h776.md) where ye have been [puwts](../../strongs/h/h6327.md), and I will [nathan](../../strongs/h/h5414.md) you the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_11_18"></a>Ezekiel 11:18

And they shall [bow'](../../strongs/h/h935.md) thither, and they shall [cuwr](../../strongs/h/h5493.md) all the [šiqqûṣ](../../strongs/h/h8251.md) thereof and all the [tôʿēḇâ](../../strongs/h/h8441.md) thereof from thence.

<a name="ezekiel_11_19"></a>Ezekiel 11:19

And I will [nathan](../../strongs/h/h5414.md) them one [leb](../../strongs/h/h3820.md), and I will [nathan](../../strongs/h/h5414.md) a [ḥāḏāš](../../strongs/h/h2319.md) [ruwach](../../strongs/h/h7307.md) [qereḇ](../../strongs/h/h7130.md) you; and I will [cuwr](../../strongs/h/h5493.md) the ['eben](../../strongs/h/h68.md) [leb](../../strongs/h/h3820.md) out of their [basar](../../strongs/h/h1320.md), and will [nathan](../../strongs/h/h5414.md) them a [leb](../../strongs/h/h3820.md) of [basar](../../strongs/h/h1320.md):

<a name="ezekiel_11_20"></a>Ezekiel 11:20

That they may [yālaḵ](../../strongs/h/h3212.md) in my [chuqqah](../../strongs/h/h2708.md), and [shamar](../../strongs/h/h8104.md) mine [mishpat](../../strongs/h/h4941.md), and ['asah](../../strongs/h/h6213.md) them: and they shall be my ['am](../../strongs/h/h5971.md), and I will be their ['Elohiym](../../strongs/h/h430.md).

<a name="ezekiel_11_21"></a>Ezekiel 11:21

But as for them whose [leb](../../strongs/h/h3820.md) [halak](../../strongs/h/h1980.md) after the [leb](../../strongs/h/h3820.md) of their [šiqqûṣ](../../strongs/h/h8251.md) and their [tôʿēḇâ](../../strongs/h/h8441.md), I will [nathan](../../strongs/h/h5414.md) their [derek](../../strongs/h/h1870.md) upon their own [ro'sh](../../strongs/h/h7218.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_11_22"></a>Ezekiel 11:22

Then did the [kĕruwb](../../strongs/h/h3742.md) [nasa'](../../strongs/h/h5375.md) their [kanaph](../../strongs/h/h3671.md), and the ['ôp̄ān](../../strongs/h/h212.md) [ʿummâ](../../strongs/h/h5980.md) them; and the [kabowd](../../strongs/h/h3519.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) was over them [maʿal](../../strongs/h/h4605.md).

<a name="ezekiel_11_23"></a>Ezekiel 11:23

And the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [ʿālâ](../../strongs/h/h5927.md) from the midst of the [ʿîr](../../strongs/h/h5892.md), and ['amad](../../strongs/h/h5975.md) upon the [har](../../strongs/h/h2022.md) which is on the [qeḏem](../../strongs/h/h6924.md) of the [ʿîr](../../strongs/h/h5892.md).

<a name="ezekiel_11_24"></a>Ezekiel 11:24

Afterwards the [ruwach](../../strongs/h/h7307.md) [nasa'](../../strongs/h/h5375.md) me, and [bow'](../../strongs/h/h935.md) me in a [mar'ê](../../strongs/h/h4758.md) by the [ruwach](../../strongs/h/h7307.md) of ['Elohiym](../../strongs/h/h430.md) into [Kaśdîmâ](../../strongs/h/h3778.md), to them of the [gôlâ](../../strongs/h/h1473.md). So the [mar'ê](../../strongs/h/h4758.md) that I had [ra'ah](../../strongs/h/h7200.md) [ʿālâ](../../strongs/h/h5927.md) from me.

<a name="ezekiel_11_25"></a>Ezekiel 11:25

Then I [dabar](../../strongs/h/h1696.md) unto them of the [gôlâ](../../strongs/h/h1473.md) all the [dabar](../../strongs/h/h1697.md) that [Yĕhovah](../../strongs/h/h3068.md) had [ra'ah](../../strongs/h/h7200.md) me.

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 10](ezekiel_10.md) - [Ezekiel 12](ezekiel_12.md)