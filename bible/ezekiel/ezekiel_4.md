# [Ezekiel 4](https://www.blueletterbible.org/kjv/ezekiel/4)

<a name="ezekiel_4_1"></a>Ezekiel 4:1

Thou also, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [laqach](../../strongs/h/h3947.md) thee a [lᵊḇēnâ](../../strongs/h/h3843.md), and [nathan](../../strongs/h/h5414.md) it [paniym](../../strongs/h/h6440.md) thee, and [ḥāqaq](../../strongs/h/h2710.md) upon it the [ʿîr](../../strongs/h/h5892.md), even [Yĕruwshalaim](../../strongs/h/h3389.md):

<a name="ezekiel_4_2"></a>Ezekiel 4:2

And [nathan](../../strongs/h/h5414.md) [māṣôr](../../strongs/h/h4692.md) against it, and [bānâ](../../strongs/h/h1129.md) a [dāyēq](../../strongs/h/h1785.md) against it, and [šāp̄aḵ](../../strongs/h/h8210.md) a [sōllâ](../../strongs/h/h5550.md) against it; [nathan](../../strongs/h/h5414.md) the [maḥănê](../../strongs/h/h4264.md) also against it, and [śûm](../../strongs/h/h7760.md) [kar](../../strongs/h/h3733.md) against it [cabiyb](../../strongs/h/h5439.md).

<a name="ezekiel_4_3"></a>Ezekiel 4:3

Moreover [laqach](../../strongs/h/h3947.md) thou unto thee a [barzel](../../strongs/h/h1270.md) [maḥăḇaṯ](../../strongs/h/h4227.md), and [nathan](../../strongs/h/h5414.md) it for a [qîr](../../strongs/h/h7023.md) of [barzel](../../strongs/h/h1270.md) between thee and the [ʿîr](../../strongs/h/h5892.md): and [kuwn](../../strongs/h/h3559.md) thy [paniym](../../strongs/h/h6440.md) against it, and it shall be [māṣôr](../../strongs/h/h4692.md), and thou shalt lay [ṣûr](../../strongs/h/h6696.md) against it. This shall be a ['ôṯ](../../strongs/h/h226.md) to the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_4_4"></a>Ezekiel 4:4

[shakab](../../strongs/h/h7901.md) thou also upon thy [śᵊmā'lî](../../strongs/h/h8042.md) [ṣaḏ](../../strongs/h/h6654.md), and [śûm](../../strongs/h/h7760.md) the ['avon](../../strongs/h/h5771.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) upon it: according to the [mispār](../../strongs/h/h4557.md) of the [yowm](../../strongs/h/h3117.md) that thou shalt [shakab](../../strongs/h/h7901.md) upon it thou shalt [nasa'](../../strongs/h/h5375.md) their ['avon](../../strongs/h/h5771.md).

<a name="ezekiel_4_5"></a>Ezekiel 4:5

For I have [nathan](../../strongs/h/h5414.md) upon thee the [šānâ](../../strongs/h/h8141.md) of their ['avon](../../strongs/h/h5771.md), according to the [mispār](../../strongs/h/h4557.md) of the [yowm](../../strongs/h/h3117.md), three hundred and ninety [yowm](../../strongs/h/h3117.md): so shalt thou [nasa'](../../strongs/h/h5375.md) the ['avon](../../strongs/h/h5771.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_4_6"></a>Ezekiel 4:6

And when thou hast [kalah](../../strongs/h/h3615.md) them, [shakab](../../strongs/h/h7901.md) again on thy [yᵊmānî](../../strongs/h/h3233.md) [yᵊmînî](../../strongs/h/h3227.md) [ṣaḏ](../../strongs/h/h6654.md), and thou shalt [nasa'](../../strongs/h/h5375.md) the ['avon](../../strongs/h/h5771.md) of the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) forty [yowm](../../strongs/h/h3117.md): I have [nathan](../../strongs/h/h5414.md) thee each [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) for a [šānâ](../../strongs/h/h8141.md) [šānâ](../../strongs/h/h8141.md).

<a name="ezekiel_4_7"></a>Ezekiel 4:7

Therefore thou shalt [kuwn](../../strongs/h/h3559.md) thy [paniym](../../strongs/h/h6440.md) toward the [māṣôr](../../strongs/h/h4692.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and thine [zerowa'](../../strongs/h/h2220.md) shall be [ḥāśap̄](../../strongs/h/h2834.md), and thou shalt [nāḇā'](../../strongs/h/h5012.md) against it.

<a name="ezekiel_4_8"></a>Ezekiel 4:8

And, behold, I will [nathan](../../strongs/h/h5414.md) [ʿăḇōṯ](../../strongs/h/h5688.md) upon thee, and thou shalt not [hāp̄aḵ](../../strongs/h/h2015.md) thee from one [ṣaḏ](../../strongs/h/h6654.md) to [ṣaḏ](../../strongs/h/h6654.md), till thou hast [kalah](../../strongs/h/h3615.md) the [yowm](../../strongs/h/h3117.md) of thy [māṣôr](../../strongs/h/h4692.md).

<a name="ezekiel_4_9"></a>Ezekiel 4:9

[laqach](../../strongs/h/h3947.md) thou also unto thee [ḥiṭṭâ](../../strongs/h/h2406.md), and [śᵊʿōrâ](../../strongs/h/h8184.md), and [pôl](../../strongs/h/h6321.md), and [ʿāḏāš](../../strongs/h/h5742.md), and [dōḥan](../../strongs/h/h1764.md), and [kussemeṯ](../../strongs/h/h3698.md), and [nathan](../../strongs/h/h5414.md) them in one [kĕliy](../../strongs/h/h3627.md), and ['asah](../../strongs/h/h6213.md) thee [lechem](../../strongs/h/h3899.md) thereof, according to the [mispār](../../strongs/h/h4557.md) of the [yowm](../../strongs/h/h3117.md) that thou shalt [shakab](../../strongs/h/h7901.md) upon thy [ṣaḏ](../../strongs/h/h6654.md), three hundred and ninety [yowm](../../strongs/h/h3117.md) shalt thou ['akal](../../strongs/h/h398.md) thereof.

<a name="ezekiel_4_10"></a>Ezekiel 4:10

And thy [ma'akal](../../strongs/h/h3978.md) which thou shalt ['akal](../../strongs/h/h398.md) shall be by [mišqôl](../../strongs/h/h4946.md), twenty [šeqel](../../strongs/h/h8255.md) a [yowm](../../strongs/h/h3117.md): from [ʿēṯ](../../strongs/h/h6256.md) to [ʿēṯ](../../strongs/h/h6256.md) shalt thou ['akal](../../strongs/h/h398.md) it.

<a name="ezekiel_4_11"></a>Ezekiel 4:11

Thou shalt [šāṯâ](../../strongs/h/h8354.md) also [mayim](../../strongs/h/h4325.md) by [mᵊśûrâ](../../strongs/h/h4884.md), the sixth part of an [hîn](../../strongs/h/h1969.md): from [ʿēṯ](../../strongs/h/h6256.md) to [ʿēṯ](../../strongs/h/h6256.md) shalt thou [šāṯâ](../../strongs/h/h8354.md).

<a name="ezekiel_4_12"></a>Ezekiel 4:12

And thou shalt ['akal](../../strongs/h/h398.md) it as [śᵊʿōrâ](../../strongs/h/h8184.md) [ʿugâ](../../strongs/h/h5692.md), and thou shalt [ʿûḡ](../../strongs/h/h5746.md) it with [gēlel](../../strongs/h/h1561.md) that cometh [ṣē'â](../../strongs/h/h6627.md) of ['āḏām](../../strongs/h/h120.md), in their ['ayin](../../strongs/h/h5869.md).

<a name="ezekiel_4_13"></a>Ezekiel 4:13

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), Even thus shall the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['akal](../../strongs/h/h398.md) their [tame'](../../strongs/h/h2931.md) [lechem](../../strongs/h/h3899.md) among the [gowy](../../strongs/h/h1471.md), whither I will [nāḏaḥ](../../strongs/h/h5080.md) them.

<a name="ezekiel_4_14"></a>Ezekiel 4:14

Then ['āmar](../../strongs/h/h559.md) I, ['ăhâ](../../strongs/h/h162.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md)! behold, my [nephesh](../../strongs/h/h5315.md) hath not been [ṭāmē'](../../strongs/h/h2930.md): for from my [nāʿur](../../strongs/h/h5271.md) up even till now have I not ['akal](../../strongs/h/h398.md) of that which [nᵊḇēlâ](../../strongs/h/h5038.md), or is [ṭᵊrēp̄â](../../strongs/h/h2966.md); neither [bow'](../../strongs/h/h935.md) there [pigûl](../../strongs/h/h6292.md) [basar](../../strongs/h/h1320.md) into my [peh](../../strongs/h/h6310.md).

<a name="ezekiel_4_15"></a>Ezekiel 4:15

Then he ['āmar](../../strongs/h/h559.md) unto me, [ra'ah](../../strongs/h/h7200.md), I have [nathan](../../strongs/h/h5414.md) thee [bāqār](../../strongs/h/h1241.md) [ṣp̄vʿ](../../strongs/h/h6832.md) [ṣp̄vʿ](../../strongs/h/h6832.md) for ['āḏām](../../strongs/h/h120.md) [gēlel](../../strongs/h/h1561.md), and thou shalt ['asah](../../strongs/h/h6213.md) thy [lechem](../../strongs/h/h3899.md) therewith.

<a name="ezekiel_4_16"></a>Ezekiel 4:16

Moreover he ['āmar](../../strongs/h/h559.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), behold, I will [shabar](../../strongs/h/h7665.md) the [maṭṭê](../../strongs/h/h4294.md) of [lechem](../../strongs/h/h3899.md) in [Yĕruwshalaim](../../strongs/h/h3389.md): and they shall ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) by [mišqāl](../../strongs/h/h4948.md), and with [dᵊ'āḡâ](../../strongs/h/h1674.md); and they shall [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md) by [mᵊśûrâ](../../strongs/h/h4884.md), and with [šimmāmôn](../../strongs/h/h8078.md):

<a name="ezekiel_4_17"></a>Ezekiel 4:17

That they may [ḥāsēr](../../strongs/h/h2637.md) [lechem](../../strongs/h/h3899.md) and [mayim](../../strongs/h/h4325.md), and be [šāmēm](../../strongs/h/h8074.md) ['iysh](../../strongs/h/h376.md) with ['ach](../../strongs/h/h251.md), and [māqaq](../../strongs/h/h4743.md) for their ['avon](../../strongs/h/h5771.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 3](ezekiel_3.md) - [Ezekiel 5](ezekiel_5.md)