# [Ezekiel 5](https://www.blueletterbible.org/kjv/ezekiel/5)

<a name="ezekiel_5_1"></a>Ezekiel 5:1

And thou, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [laqach](../../strongs/h/h3947.md) thee a [ḥaḏ](../../strongs/h/h2299.md) [chereb](../../strongs/h/h2719.md), [laqach](../../strongs/h/h3947.md) thee a [gallāḇ](../../strongs/h/h1532.md) [taʿar](../../strongs/h/h8593.md), and cause it to ['abar](../../strongs/h/h5674.md) upon thine [ro'sh](../../strongs/h/h7218.md) and upon thy [zāqān](../../strongs/h/h2206.md): then [laqach](../../strongs/h/h3947.md) thee [mō'znayim](../../strongs/h/h3976.md) to [mišqāl](../../strongs/h/h4948.md), and [chalaq](../../strongs/h/h2505.md) the hair.

<a name="ezekiel_5_2"></a>Ezekiel 5:2

Thou shalt [bāʿar](../../strongs/h/h1197.md) with ['ûr](../../strongs/h/h217.md) a third part in the midst of the [ʿîr](../../strongs/h/h5892.md), when the [yowm](../../strongs/h/h3117.md) of the [māṣôr](../../strongs/h/h4692.md) are [mālā'](../../strongs/h/h4390.md): and thou shalt [laqach](../../strongs/h/h3947.md) a third part, and [nakah](../../strongs/h/h5221.md) [cabiyb](../../strongs/h/h5439.md) it with a [chereb](../../strongs/h/h2719.md): and a third part thou shalt [zārâ](../../strongs/h/h2219.md) in the [ruwach](../../strongs/h/h7307.md); and I will [rîq](../../strongs/h/h7324.md) a [chereb](../../strongs/h/h2719.md) ['aḥar](../../strongs/h/h310.md) them.

<a name="ezekiel_5_3"></a>Ezekiel 5:3

Thou shalt also [laqach](../../strongs/h/h3947.md) thereof a [mᵊʿaṭ](../../strongs/h/h4592.md) in [mispār](../../strongs/h/h4557.md), and [ṣûr](../../strongs/h/h6696.md) them in thy [kanaph](../../strongs/h/h3671.md).

<a name="ezekiel_5_4"></a>Ezekiel 5:4

Then take of them [laqach](../../strongs/h/h3947.md), and [shalak](../../strongs/h/h7993.md) them into the midst of the ['esh](../../strongs/h/h784.md), and [śārap̄](../../strongs/h/h8313.md) them in the ['esh](../../strongs/h/h784.md); for thereof shall an ['esh](../../strongs/h/h784.md) [yāṣā'](../../strongs/h/h3318.md) into all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_5_5"></a>Ezekiel 5:5

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); This is [Yĕruwshalaim](../../strongs/h/h3389.md): I have [śûm](../../strongs/h/h7760.md) it in the midst of the [gowy](../../strongs/h/h1471.md) and ['erets](../../strongs/h/h776.md) that are [cabiyb](../../strongs/h/h5439.md) her.

<a name="ezekiel_5_6"></a>Ezekiel 5:6

And she hath [marah](../../strongs/h/h4784.md) my [mishpat](../../strongs/h/h4941.md) into [rišʿâ](../../strongs/h/h7564.md) more than the [gowy](../../strongs/h/h1471.md), and my [chuqqah](../../strongs/h/h2708.md) more than the ['erets](../../strongs/h/h776.md) that are [cabiyb](../../strongs/h/h5439.md) her: for they have [mā'as](../../strongs/h/h3988.md) my [mishpat](../../strongs/h/h4941.md) and my [chuqqah](../../strongs/h/h2708.md), they have not [halak](../../strongs/h/h1980.md) in them.

<a name="ezekiel_5_7"></a>Ezekiel 5:7

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because ye [hāmôn](../../strongs/h/h1995.md) more than the [gowy](../../strongs/h/h1471.md) that are [cabiyb](../../strongs/h/h5439.md) you, and have not [halak](../../strongs/h/h1980.md) in my [chuqqah](../../strongs/h/h2708.md), neither have ['asah](../../strongs/h/h6213.md) my [mishpat](../../strongs/h/h4941.md), neither have ['asah](../../strongs/h/h6213.md) according to the [mishpat](../../strongs/h/h4941.md) of the [gowy](../../strongs/h/h1471.md) that are [cabiyb](../../strongs/h/h5439.md) you;

<a name="ezekiel_5_8"></a>Ezekiel 5:8

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I, even I, am against thee, and will ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) in the midst of thee in the ['ayin](../../strongs/h/h5869.md) of the [gowy](../../strongs/h/h1471.md).

<a name="ezekiel_5_9"></a>Ezekiel 5:9

And I will ['asah](../../strongs/h/h6213.md) in thee that which I have not ['asah](../../strongs/h/h6213.md), and whereunto I will not ['asah](../../strongs/h/h6213.md) any more the like, because of all thine [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="ezekiel_5_10"></a>Ezekiel 5:10

Therefore the ['ab](../../strongs/h/h1.md) shall ['akal](../../strongs/h/h398.md) the [ben](../../strongs/h/h1121.md) in the midst of thee, and the [ben](../../strongs/h/h1121.md) shall ['akal](../../strongs/h/h398.md) their ['ab](../../strongs/h/h1.md); and I will ['asah](../../strongs/h/h6213.md) [šep̄eṭ](../../strongs/h/h8201.md) in thee, and the whole [šᵊ'ērîṯ](../../strongs/h/h7611.md) of thee will I [zārâ](../../strongs/h/h2219.md) into all the [ruwach](../../strongs/h/h7307.md).

<a name="ezekiel_5_11"></a>Ezekiel 5:11

Wherefore, as I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Surely, because thou hast [ṭāmē'](../../strongs/h/h2930.md) my [miqdash](../../strongs/h/h4720.md) with all thy [šiqqûṣ](../../strongs/h/h8251.md), and with all thine [tôʿēḇâ](../../strongs/h/h8441.md), therefore will I also [gāraʿ](../../strongs/h/h1639.md) thee; neither shall mine ['ayin](../../strongs/h/h5869.md) [ḥûs](../../strongs/h/h2347.md), neither will I have any [ḥāmal](../../strongs/h/h2550.md).

<a name="ezekiel_5_12"></a>Ezekiel 5:12

A third part of thee shall [muwth](../../strongs/h/h4191.md) with the [deḇer](../../strongs/h/h1698.md), and with [rāʿāḇ](../../strongs/h/h7458.md) shall they be [kalah](../../strongs/h/h3615.md) in the midst of thee: and a third part shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md) [cabiyb](../../strongs/h/h5439.md) thee; and I will [zārâ](../../strongs/h/h2219.md) a third part into all the [ruwach](../../strongs/h/h7307.md), and I will [rîq](../../strongs/h/h7324.md) a [chereb](../../strongs/h/h2719.md) ['aḥar](../../strongs/h/h310.md) them.

<a name="ezekiel_5_13"></a>Ezekiel 5:13

Thus shall mine ['aph](../../strongs/h/h639.md) be [kalah](../../strongs/h/h3615.md), and I will cause my [chemah](../../strongs/h/h2534.md) to [nuwach](../../strongs/h/h5117.md) upon them, and I will be [nacham](../../strongs/h/h5162.md): and they shall [yada'](../../strongs/h/h3045.md) that I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) it in my [qin'â](../../strongs/h/h7068.md), when I have [kalah](../../strongs/h/h3615.md) my [chemah](../../strongs/h/h2534.md) in them.

<a name="ezekiel_5_14"></a>Ezekiel 5:14

Moreover I will [nathan](../../strongs/h/h5414.md) thee [chorbah](../../strongs/h/h2723.md), and a [cherpah](../../strongs/h/h2781.md) among the [gowy](../../strongs/h/h1471.md) that are [cabiyb](../../strongs/h/h5439.md) thee, in the ['ayin](../../strongs/h/h5869.md) of all that ['abar](../../strongs/h/h5674.md).

<a name="ezekiel_5_15"></a>Ezekiel 5:15

So it shall be a [cherpah](../../strongs/h/h2781.md) and a [gᵊḏûp̄â](../../strongs/h/h1422.md), a [mûsār](../../strongs/h/h4148.md) and a [mᵊšammâ](../../strongs/h/h4923.md) unto the [gowy](../../strongs/h/h1471.md) that are [cabiyb](../../strongs/h/h5439.md) thee, when I shall ['asah](../../strongs/h/h6213.md) [šep̄eṭ](../../strongs/h/h8201.md) in thee in ['aph](../../strongs/h/h639.md) and in [chemah](../../strongs/h/h2534.md) and in [chemah](../../strongs/h/h2534.md) [tôḵēḥâ](../../strongs/h/h8433.md). I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) it.

<a name="ezekiel_5_16"></a>Ezekiel 5:16

When I shall [shalach](../../strongs/h/h7971.md) upon them the [ra'](../../strongs/h/h7451.md) [chets](../../strongs/h/h2671.md) of [rāʿāḇ](../../strongs/h/h7458.md), which shall be for their [mašḥîṯ](../../strongs/h/h4889.md), and which I will [shalach](../../strongs/h/h7971.md) to [shachath](../../strongs/h/h7843.md) you: and I will [yāsap̄](../../strongs/h/h3254.md) the [rāʿāḇ](../../strongs/h/h7458.md) upon you, and will [shabar](../../strongs/h/h7665.md) your [maṭṭê](../../strongs/h/h4294.md) of [lechem](../../strongs/h/h3899.md):

<a name="ezekiel_5_17"></a>Ezekiel 5:17

So will I [shalach](../../strongs/h/h7971.md) upon you [rāʿāḇ](../../strongs/h/h7458.md) and [ra'](../../strongs/h/h7451.md) [chay](../../strongs/h/h2416.md), and they shall [šāḵōl](../../strongs/h/h7921.md) thee; and [deḇer](../../strongs/h/h1698.md) and [dam](../../strongs/h/h1818.md) shall ['abar](../../strongs/h/h5674.md) thee; and I will [bow'](../../strongs/h/h935.md) the [chereb](../../strongs/h/h2719.md) upon thee. I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) it.

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 4](ezekiel_4.md) - [Ezekiel 6](ezekiel_6.md)