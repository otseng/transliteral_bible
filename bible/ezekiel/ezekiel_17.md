# [Ezekiel 17](https://www.blueletterbible.org/kjv/ezekiel/17)

<a name="ezekiel_17_1"></a>Ezekiel 17:1

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_17_2"></a>Ezekiel 17:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [ḥûḏ](../../strongs/h/h2330.md) a [ḥîḏâ](../../strongs/h/h2420.md), and [māšal](../../strongs/h/h4911.md) a [māšāl](../../strongs/h/h4912.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md);

<a name="ezekiel_17_3"></a>Ezekiel 17:3

And ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); A [gadowl](../../strongs/h/h1419.md) [nesheׁr](../../strongs/h/h5404.md) with [gadowl](../../strongs/h/h1419.md) [kanaph](../../strongs/h/h3671.md), ['ēḇer](../../strongs/h/h83.md) ['ārēḵ](../../strongs/h/h750.md), [mālē'](../../strongs/h/h4392.md) of [nôṣâ](../../strongs/h/h5133.md), which had divers [riqmâ](../../strongs/h/h7553.md), [bow'](../../strongs/h/h935.md) unto [Lᵊḇānôn](../../strongs/h/h3844.md), and [laqach](../../strongs/h/h3947.md) the highest [ṣammereṯ](../../strongs/h/h6788.md) of the ['erez](../../strongs/h/h730.md):

<a name="ezekiel_17_4"></a>Ezekiel 17:4

He [qāṭap̄](../../strongs/h/h6998.md) the [ro'sh](../../strongs/h/h7218.md) of his [yᵊnîqâ](../../strongs/h/h3242.md), and [bow'](../../strongs/h/h935.md) it into an ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md); he [śûm](../../strongs/h/h7760.md) it in a [ʿîr](../../strongs/h/h5892.md) of [rāḵal](../../strongs/h/h7402.md).

<a name="ezekiel_17_5"></a>Ezekiel 17:5

He [laqach](../../strongs/h/h3947.md) also of the [zera'](../../strongs/h/h2233.md) of the ['erets](../../strongs/h/h776.md), and [nathan](../../strongs/h/h5414.md) it in a [zera'](../../strongs/h/h2233.md) [sadeh](../../strongs/h/h7704.md); he [laqach](../../strongs/h/h3947.md) it by [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md), and [śûm](../../strongs/h/h7760.md) it as a [ṣap̄ṣāp̄â](../../strongs/h/h6851.md).

<a name="ezekiel_17_6"></a>Ezekiel 17:6

And it [ṣāmaḥ](../../strongs/h/h6779.md), and became a [sāraḥ](../../strongs/h/h5628.md) [gep̄en](../../strongs/h/h1612.md) of [šāp̄āl](../../strongs/h/h8217.md) [qômâ](../../strongs/h/h6967.md), whose [dālîṯ](../../strongs/h/h1808.md) [panah](../../strongs/h/h6437.md) toward him, and the [šereš](../../strongs/h/h8328.md) thereof were under him: so it became a [gep̄en](../../strongs/h/h1612.md), and brought ['asah](../../strongs/h/h6213.md) [baḏ](../../strongs/h/h905.md), and [shalach](../../strongs/h/h7971.md) forth [pō'râ](../../strongs/h/h6288.md).

<a name="ezekiel_17_7"></a>Ezekiel 17:7

There was also another [gadowl](../../strongs/h/h1419.md) [nesheׁr](../../strongs/h/h5404.md) with [gadowl](../../strongs/h/h1419.md) [kanaph](../../strongs/h/h3671.md) and [rab](../../strongs/h/h7227.md) [nôṣâ](../../strongs/h/h5133.md): and, behold, this [gep̄en](../../strongs/h/h1612.md) did [kāp̄an](../../strongs/h/h3719.md) her [šereš](../../strongs/h/h8328.md) toward him, and shot [shalach](../../strongs/h/h7971.md) her [dālîṯ](../../strongs/h/h1808.md) toward him, that he might [šāqâ](../../strongs/h/h8248.md) it by the [ʿărûḡâ](../../strongs/h/h6170.md) of her [maṭṭāʿ](../../strongs/h/h4302.md).

<a name="ezekiel_17_8"></a>Ezekiel 17:8

It was [šāṯal](../../strongs/h/h8362.md) in a [towb](../../strongs/h/h2896.md) [sadeh](../../strongs/h/h7704.md) by [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md), that it might ['asah](../../strongs/h/h6213.md) [ʿānāp̄](../../strongs/h/h6057.md), and that it might [nasa'](../../strongs/h/h5375.md) [pĕriy](../../strongs/h/h6529.md), that it might be an ['adereṯ](../../strongs/h/h155.md) [gep̄en](../../strongs/h/h1612.md).

<a name="ezekiel_17_9"></a>Ezekiel 17:9

['āmar](../../strongs/h/h559.md) thou, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Shall it [tsalach](../../strongs/h/h6743.md)? shall he not pull [nathaq](../../strongs/h/h5423.md) the [šereš](../../strongs/h/h8328.md) thereof, and cut [qāsas](../../strongs/h/h7082.md) the [pĕriy](../../strongs/h/h6529.md) thereof, that it [yāḇēš](../../strongs/h/h3001.md)? it shall [yāḇēš](../../strongs/h/h3001.md) in all the [ṭerep̄](../../strongs/h/h2964.md) of her [ṣemaḥ](../../strongs/h/h6780.md), even without [gadowl](../../strongs/h/h1419.md) [zerowa'](../../strongs/h/h2220.md) or [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) to pluck it [nasa'](../../strongs/h/h5375.md) by the [šereš](../../strongs/h/h8328.md) thereof.

<a name="ezekiel_17_10"></a>Ezekiel 17:10

Yea, behold, being [šāṯal](../../strongs/h/h8362.md), shall it [tsalach](../../strongs/h/h6743.md)? shall it not [yāḇēš](../../strongs/h/h3001.md) [yāḇēš](../../strongs/h/h3001.md), when the [qāḏîm](../../strongs/h/h6921.md) [ruwach](../../strongs/h/h7307.md) [naga'](../../strongs/h/h5060.md) it? it shall [yāḇēš](../../strongs/h/h3001.md) in the [ʿărûḡâ](../../strongs/h/h6170.md) where it [ṣemaḥ](../../strongs/h/h6780.md).

<a name="ezekiel_17_11"></a>Ezekiel 17:11

Moreover the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_17_12"></a>Ezekiel 17:12

['āmar](../../strongs/h/h559.md) now to the [mᵊrî](../../strongs/h/h4805.md) [bayith](../../strongs/h/h1004.md), [yada'](../../strongs/h/h3045.md) ye not what these things mean? ['āmar](../../strongs/h/h559.md) them, Behold, the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) is [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), and hath [laqach](../../strongs/h/h3947.md) the [melek](../../strongs/h/h4428.md) thereof, and the [śar](../../strongs/h/h8269.md) thereof, and [bow'](../../strongs/h/h935.md) them with him to [Bāḇel](../../strongs/h/h894.md);

<a name="ezekiel_17_13"></a>Ezekiel 17:13

And hath [laqach](../../strongs/h/h3947.md) of the [mᵊlûḵâ](../../strongs/h/h4410.md) [zera'](../../strongs/h/h2233.md), and [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with him, and hath [bow'](../../strongs/h/h935.md) an ['alah](../../strongs/h/h423.md) of him: he hath also [laqach](../../strongs/h/h3947.md) the ['ayil](../../strongs/h/h352.md) of the ['erets](../../strongs/h/h776.md):

<a name="ezekiel_17_14"></a>Ezekiel 17:14

That the [mamlāḵâ](../../strongs/h/h4467.md) might be [šāp̄āl](../../strongs/h/h8217.md), that it might not [nasa'](../../strongs/h/h5375.md) itself, but that by [shamar](../../strongs/h/h8104.md) of his [bĕriyth](../../strongs/h/h1285.md) it might ['amad](../../strongs/h/h5975.md).

<a name="ezekiel_17_15"></a>Ezekiel 17:15

But he [māraḏ](../../strongs/h/h4775.md) against him in [shalach](../../strongs/h/h7971.md) his [mal'ak](../../strongs/h/h4397.md) into [Mitsrayim](../../strongs/h/h4714.md), that they might [nathan](../../strongs/h/h5414.md) him [sûs](../../strongs/h/h5483.md) and [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md). Shall he [tsalach](../../strongs/h/h6743.md)? shall he [mālaṭ](../../strongs/h/h4422.md) that ['asah](../../strongs/h/h6213.md) such things? or shall he [pārar](../../strongs/h/h6565.md) the [bĕriyth](../../strongs/h/h1285.md), and be [mālaṭ](../../strongs/h/h4422.md)?

<a name="ezekiel_17_16"></a>Ezekiel 17:16

As I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), surely in the [maqowm](../../strongs/h/h4725.md) where the [melek](../../strongs/h/h4428.md) dwelleth that made him [mālaḵ](../../strongs/h/h4427.md), whose ['alah](../../strongs/h/h423.md) he [bazah](../../strongs/h/h959.md), and whose [bĕriyth](../../strongs/h/h1285.md) he [pārar](../../strongs/h/h6565.md), even with him in the midst of [Bāḇel](../../strongs/h/h894.md) he shall [muwth](../../strongs/h/h4191.md).

<a name="ezekiel_17_17"></a>Ezekiel 17:17

Neither shall [Parʿô](../../strongs/h/h6547.md) with his [gadowl](../../strongs/h/h1419.md) [ḥayil](../../strongs/h/h2428.md) and [rab](../../strongs/h/h7227.md) [qāhēl](../../strongs/h/h6951.md) ['asah](../../strongs/h/h6213.md) for him in the [milḥāmâ](../../strongs/h/h4421.md), by [šāp̄aḵ](../../strongs/h/h8210.md) [sōllâ](../../strongs/h/h5550.md), and [bānâ](../../strongs/h/h1129.md) [dāyēq](../../strongs/h/h1785.md), to [karath](../../strongs/h/h3772.md) [rab](../../strongs/h/h7227.md) [nephesh](../../strongs/h/h5315.md):

<a name="ezekiel_17_18"></a>Ezekiel 17:18

Seeing he [bazah](../../strongs/h/h959.md) the ['alah](../../strongs/h/h423.md) by [pārar](../../strongs/h/h6565.md) the [bĕriyth](../../strongs/h/h1285.md), when, lo, he had [nathan](../../strongs/h/h5414.md) his [yad](../../strongs/h/h3027.md), and hath ['asah](../../strongs/h/h6213.md) all these things, he shall not [mālaṭ](../../strongs/h/h4422.md).

<a name="ezekiel_17_19"></a>Ezekiel 17:19

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); As I [chay](../../strongs/h/h2416.md), surely mine ['alah](../../strongs/h/h423.md) that he hath [bazah](../../strongs/h/h959.md), and my [bĕriyth](../../strongs/h/h1285.md) that he hath [pûr](../../strongs/h/h6331.md), even it will I [nathan](../../strongs/h/h5414.md) upon his own [ro'sh](../../strongs/h/h7218.md).

<a name="ezekiel_17_20"></a>Ezekiel 17:20

And I will [pāraś](../../strongs/h/h6566.md) my [rešeṯ](../../strongs/h/h7568.md) upon him, and he shall be [tāp̄aś](../../strongs/h/h8610.md) in my [matsuwd](../../strongs/h/h4686.md), and I will [bow'](../../strongs/h/h935.md) him to [Bāḇel](../../strongs/h/h894.md), and will [shaphat](../../strongs/h/h8199.md) with him there for his [māʿal](../../strongs/h/h4603.md) that he hath [maʿal](../../strongs/h/h4604.md) against me.

<a name="ezekiel_17_21"></a>Ezekiel 17:21

And all his [miḇrāḥ](../../strongs/h/h4015.md) with all his ['ăḡap̄](../../strongs/h/h102.md) shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md), and they that [šā'ar](../../strongs/h/h7604.md) shall be [pāraś](../../strongs/h/h6566.md) toward all [ruwach](../../strongs/h/h7307.md): and ye shall [yada'](../../strongs/h/h3045.md) that I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) it.

<a name="ezekiel_17_22"></a>Ezekiel 17:22

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); I will also [laqach](../../strongs/h/h3947.md) of the [ṣammereṯ](../../strongs/h/h6788.md) of the [ruwm](../../strongs/h/h7311.md) ['erez](../../strongs/h/h730.md), and will [nathan](../../strongs/h/h5414.md) it; I will [qāṭap̄](../../strongs/h/h6998.md) from the [ro'sh](../../strongs/h/h7218.md) of his [yôneqeṯ](../../strongs/h/h3127.md) a [raḵ](../../strongs/h/h7390.md), and will [šāṯal](../../strongs/h/h8362.md) it upon a [gāḇōha](../../strongs/h/h1364.md) [har](../../strongs/h/h2022.md) and [tālal](../../strongs/h/h8524.md):

<a name="ezekiel_17_23"></a>Ezekiel 17:23

In the [har](../../strongs/h/h2022.md) of the [marowm](../../strongs/h/h4791.md) of [Yisra'el](../../strongs/h/h3478.md) will I [šāṯal](../../strongs/h/h8362.md) it: and it shall [nasa'](../../strongs/h/h5375.md) [ʿānāp̄](../../strongs/h/h6057.md), and ['asah](../../strongs/h/h6213.md) [pĕriy](../../strongs/h/h6529.md), and be an ['addiyr](../../strongs/h/h117.md) ['erez](../../strongs/h/h730.md): and under it shall [shakan](../../strongs/h/h7931.md) all [tsippowr](../../strongs/h/h6833.md) of every [kanaph](../../strongs/h/h3671.md); in the [ṣēl](../../strongs/h/h6738.md) of the [dālîṯ](../../strongs/h/h1808.md) thereof shall they [shakan](../../strongs/h/h7931.md).

<a name="ezekiel_17_24"></a>Ezekiel 17:24

And all the ['ets](../../strongs/h/h6086.md) of the [sadeh](../../strongs/h/h7704.md) shall [yada'](../../strongs/h/h3045.md) that I [Yĕhovah](../../strongs/h/h3068.md) have brought [šāp̄ēl](../../strongs/h/h8213.md) the [gāḇōha](../../strongs/h/h1364.md) ['ets](../../strongs/h/h6086.md), have [gāḇah](../../strongs/h/h1361.md) the [šāp̄āl](../../strongs/h/h8217.md) ['ets](../../strongs/h/h6086.md), have [yāḇēš](../../strongs/h/h3001.md) up the [laḥ](../../strongs/h/h3892.md) ['ets](../../strongs/h/h6086.md), and have made the [yāḇēš](../../strongs/h/h3002.md) ['ets](../../strongs/h/h6086.md) to [pāraḥ](../../strongs/h/h6524.md): I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) and have ['asah](../../strongs/h/h6213.md) it.

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 16](ezekiel_16.md) - [Ezekiel 18](ezekiel_18.md)