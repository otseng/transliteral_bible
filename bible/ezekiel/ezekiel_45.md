# [Ezekiel 45](https://www.blueletterbible.org/kjv/ezekiel/45)

<a name="ezekiel_45_1"></a>Ezekiel 45:1

Moreover, when ye shall [naphal](../../strongs/h/h5307.md) the ['erets](../../strongs/h/h776.md) for [nachalah](../../strongs/h/h5159.md), ye shall [ruwm](../../strongs/h/h7311.md) a [tᵊrûmâ](../../strongs/h/h8641.md) unto [Yĕhovah](../../strongs/h/h3068.md), a [qodesh](../../strongs/h/h6944.md) of the ['erets](../../strongs/h/h776.md): the ['ōreḵ](../../strongs/h/h753.md) shall be the ['ōreḵ](../../strongs/h/h753.md) of five and twenty thousand reeds, and the [rōḥaḇ](../../strongs/h/h7341.md) shall be ten thousand. This shall be [qodesh](../../strongs/h/h6944.md) in all the [gᵊḇûl](../../strongs/h/h1366.md) thereof [cabiyb](../../strongs/h/h5439.md).

<a name="ezekiel_45_2"></a>Ezekiel 45:2

Of this there shall be for the [qodesh](../../strongs/h/h6944.md) five hundred in length, with five hundred in breadth, [rāḇaʿ](../../strongs/h/h7251.md) [cabiyb](../../strongs/h/h5439.md); and fifty ['ammâ](../../strongs/h/h520.md) [cabiyb](../../strongs/h/h5439.md) for the [miḡrāš](../../strongs/h/h4054.md) thereof.

<a name="ezekiel_45_3"></a>Ezekiel 45:3

And of this [midâ](../../strongs/h/h4060.md) shalt thou [māḏaḏ](../../strongs/h/h4058.md) the ['ōreḵ](../../strongs/h/h753.md) of five and twenty thousand, and the [rōḥaḇ](../../strongs/h/h7341.md) of ten thousand: and in it shall be the [miqdash](../../strongs/h/h4720.md) and the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md).

<a name="ezekiel_45_4"></a>Ezekiel 45:4

The [qodesh](../../strongs/h/h6944.md) portion of the ['erets](../../strongs/h/h776.md) shall be for the [kōhēn](../../strongs/h/h3548.md) the [sharath](../../strongs/h/h8334.md) of the [miqdash](../../strongs/h/h4720.md), which shall [qārēḇ](../../strongs/h/h7131.md) to [sharath](../../strongs/h/h8334.md) unto [Yĕhovah](../../strongs/h/h3068.md): and it shall be a [maqowm](../../strongs/h/h4725.md) for their [bayith](../../strongs/h/h1004.md), and a [miqdash](../../strongs/h/h4720.md) for the [miqdash](../../strongs/h/h4720.md).

<a name="ezekiel_45_5"></a>Ezekiel 45:5

And the five and twenty thousand of ['ōreḵ](../../strongs/h/h753.md), and the ten thousand of [rōḥaḇ](../../strongs/h/h7341.md), shall also the [Lᵊvî](../../strongs/h/h3881.md), the [sharath](../../strongs/h/h8334.md) of the [bayith](../../strongs/h/h1004.md), have for themselves, for an ['achuzzah](../../strongs/h/h272.md) for twenty [liškâ](../../strongs/h/h3957.md).

<a name="ezekiel_45_6"></a>Ezekiel 45:6

And ye shall [nathan](../../strongs/h/h5414.md) the ['achuzzah](../../strongs/h/h272.md) of the [ʿîr](../../strongs/h/h5892.md) five thousand [rōḥaḇ](../../strongs/h/h7341.md), and five and twenty thousand ['ōreḵ](../../strongs/h/h753.md), over [ʿummâ](../../strongs/h/h5980.md) the [tᵊrûmâ](../../strongs/h/h8641.md) of the [qodesh](../../strongs/h/h6944.md) portion: it shall be for the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_45_7"></a>Ezekiel 45:7

And for the [nāśî'](../../strongs/h/h5387.md) on the one side and on the other side of the [tᵊrûmâ](../../strongs/h/h8641.md) of the [qodesh](../../strongs/h/h6944.md), and of the ['achuzzah](../../strongs/h/h272.md) of the [ʿîr](../../strongs/h/h5892.md), [paniym](../../strongs/h/h6440.md) the [tᵊrûmâ](../../strongs/h/h8641.md) of the [qodesh](../../strongs/h/h6944.md), and [paniym](../../strongs/h/h6440.md) the ['achuzzah](../../strongs/h/h272.md) of the [ʿîr](../../strongs/h/h5892.md), from the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md) [yam](../../strongs/h/h3220.md), and from the [qeḏem](../../strongs/h/h6924.md) [pē'â](../../strongs/h/h6285.md) [qāḏîm](../../strongs/h/h6921.md): and the ['ōreḵ](../../strongs/h/h753.md) shall be over [ʿummâ](../../strongs/h/h5980.md) one of the [cheleq](../../strongs/h/h2506.md), from the [yam](../../strongs/h/h3220.md) [gᵊḇûl](../../strongs/h/h1366.md) unto the [qāḏîm](../../strongs/h/h6921.md) [gᵊḇûl](../../strongs/h/h1366.md).

<a name="ezekiel_45_8"></a>Ezekiel 45:8

In the ['erets](../../strongs/h/h776.md) shall be his ['achuzzah](../../strongs/h/h272.md) in [Yisra'el](../../strongs/h/h3478.md): and my [nāśî'](../../strongs/h/h5387.md) shall no more [yānâ](../../strongs/h/h3238.md) my ['am](../../strongs/h/h5971.md); and the rest of the ['erets](../../strongs/h/h776.md) shall they [nathan](../../strongs/h/h5414.md) to the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) according to their [shebet](../../strongs/h/h7626.md).

<a name="ezekiel_45_9"></a>Ezekiel 45:9

Thus [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Let it [rab](../../strongs/h/h7227.md) you, O [nāśî'](../../strongs/h/h5387.md) of [Yisra'el](../../strongs/h/h3478.md): [cuwr](../../strongs/h/h5493.md) [chamac](../../strongs/h/h2555.md) and [shod](../../strongs/h/h7701.md), and ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md), take [ruwm](../../strongs/h/h7311.md) your [gᵊrušâ](../../strongs/h/h1646.md) from my ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_45_10"></a>Ezekiel 45:10

Ye shall have [tsedeq](../../strongs/h/h6664.md) [mō'znayim](../../strongs/h/h3976.md), and a [tsedeq](../../strongs/h/h6664.md) ['êp̄â](../../strongs/h/h374.md), and a [tsedeq](../../strongs/h/h6664.md) [baṯ](../../strongs/h/h1324.md).

<a name="ezekiel_45_11"></a>Ezekiel 45:11

The ['êp̄â](../../strongs/h/h374.md) and the [baṯ](../../strongs/h/h1324.md) shall be of one [tōḵen](../../strongs/h/h8506.md), that the [baṯ](../../strongs/h/h1324.md) may [nasa'](../../strongs/h/h5375.md) the tenth [maʿăśēr](../../strongs/h/h4643.md) of an [ḥōmer](../../strongs/h/h2563.md), and the ['êp̄â](../../strongs/h/h374.md) the tenth part of an [ḥōmer](../../strongs/h/h2563.md): the [maṯkōneṯ](../../strongs/h/h4971.md) thereof shall be after the [ḥōmer](../../strongs/h/h2563.md).

<a name="ezekiel_45_12"></a>Ezekiel 45:12

And the [šeqel](../../strongs/h/h8255.md) shall be twenty [gērâ](../../strongs/h/h1626.md): twenty [šeqel](../../strongs/h/h8255.md), five and twenty [šeqel](../../strongs/h/h8255.md), fifteen [šeqel](../../strongs/h/h8255.md), shall be your [mānê](../../strongs/h/h4488.md).

<a name="ezekiel_45_13"></a>Ezekiel 45:13

This is the [tᵊrûmâ](../../strongs/h/h8641.md) that ye shall [ruwm](../../strongs/h/h7311.md); the sixth part of an ['êp̄â](../../strongs/h/h374.md) of an [ḥōmer](../../strongs/h/h2563.md) of [ḥiṭṭâ](../../strongs/h/h2406.md), and ye shall give the sixth [šiššâ](../../strongs/h/h8341.md) of an ['êp̄â](../../strongs/h/h374.md) of an [ḥōmer](../../strongs/h/h2563.md) of [śᵊʿōrâ](../../strongs/h/h8184.md):

<a name="ezekiel_45_14"></a>Ezekiel 45:14

Concerning the [choq](../../strongs/h/h2706.md) of [šemen](../../strongs/h/h8081.md), the [baṯ](../../strongs/h/h1324.md) of [šemen](../../strongs/h/h8081.md), the tenth [maʿăśēr](../../strongs/h/h4643.md) of a [baṯ](../../strongs/h/h1324.md) out of the [kōr](../../strongs/h/h3734.md), which is an [ḥōmer](../../strongs/h/h2563.md) of ten [baṯ](../../strongs/h/h1324.md); for ten [baṯ](../../strongs/h/h1324.md) are an [ḥōmer](../../strongs/h/h2563.md):

<a name="ezekiel_45_15"></a>Ezekiel 45:15

And one [śê](../../strongs/h/h7716.md) out of the [tso'n](../../strongs/h/h6629.md), out of two hundred, out of the fat [mašqê](../../strongs/h/h4945.md) of [Yisra'el](../../strongs/h/h3478.md); for a [minchah](../../strongs/h/h4503.md), and for a [ʿōlâ](../../strongs/h/h5930.md), and for [šelem](../../strongs/h/h8002.md), to make [kāp̄ar](../../strongs/h/h3722.md) for them, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_45_16"></a>Ezekiel 45:16

All the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) this [tᵊrûmâ](../../strongs/h/h8641.md) for the [nāśî'](../../strongs/h/h5387.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_45_17"></a>Ezekiel 45:17

And it shall be the [nāśî'](../../strongs/h/h5387.md) [ʿōlâ](../../strongs/h/h5930.md), and [minchah](../../strongs/h/h4503.md), and [necek](../../strongs/h/h5262.md), in the [ḥāḡ](../../strongs/h/h2282.md), and in the [ḥōḏeš](../../strongs/h/h2320.md), and in the [shabbath](../../strongs/h/h7676.md), in all [môʿēḏ](../../strongs/h/h4150.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md): he shall ['asah](../../strongs/h/h6213.md) the sin [chatta'ath](../../strongs/h/h2403.md), and the [minchah](../../strongs/h/h4503.md), and the [ʿōlâ](../../strongs/h/h5930.md), and the [šelem](../../strongs/h/h8002.md), to make [kāp̄ar](../../strongs/h/h3722.md) for the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_45_18"></a>Ezekiel 45:18

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); In the [ri'šôn](../../strongs/h/h7223.md), in the ['echad](../../strongs/h/h259.md) of the [ḥōḏeš](../../strongs/h/h2320.md), thou shalt [laqach](../../strongs/h/h3947.md) a [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md) without [tamiym](../../strongs/h/h8549.md), and [chata'](../../strongs/h/h2398.md) the [miqdash](../../strongs/h/h4720.md):

<a name="ezekiel_45_19"></a>Ezekiel 45:19

And the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) of the [dam](../../strongs/h/h1818.md) of the [chatta'ath](../../strongs/h/h2403.md), and [nathan](../../strongs/h/h5414.md) it upon the [mᵊzûzâ](../../strongs/h/h4201.md) of the [bayith](../../strongs/h/h1004.md), and upon the four [pinnâ](../../strongs/h/h6438.md) of the [ʿăzārâ](../../strongs/h/h5835.md) of the [mizbeach](../../strongs/h/h4196.md), and upon the [mᵊzûzâ](../../strongs/h/h4201.md) of the [sha'ar](../../strongs/h/h8179.md) of the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md).

<a name="ezekiel_45_20"></a>Ezekiel 45:20

And so thou shalt ['asah](../../strongs/h/h6213.md) the seventh day of the [ḥōḏeš](../../strongs/h/h2320.md) for every ['iysh](../../strongs/h/h376.md) that [šāḡâ](../../strongs/h/h7686.md), and for him that is [pᵊṯî](../../strongs/h/h6612.md): so shall ye [kāp̄ar](../../strongs/h/h3722.md) the [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_45_21"></a>Ezekiel 45:21

In the [ri'šôn](../../strongs/h/h7223.md) month, in the fourteenth [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md), ye shall have the [pecach](../../strongs/h/h6453.md), a [ḥāḡ](../../strongs/h/h2282.md) of [šāḇûaʿ](../../strongs/h/h7620.md) [yowm](../../strongs/h/h3117.md); [maṣṣâ](../../strongs/h/h4682.md) shall be ['akal](../../strongs/h/h398.md).

<a name="ezekiel_45_22"></a>Ezekiel 45:22

And upon that [yowm](../../strongs/h/h3117.md) shall the [nāśî'](../../strongs/h/h5387.md) ['asah](../../strongs/h/h6213.md) for himself and for all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) a [par](../../strongs/h/h6499.md) for a [chatta'ath](../../strongs/h/h2403.md).

<a name="ezekiel_45_23"></a>Ezekiel 45:23

And seven [yowm](../../strongs/h/h3117.md) of the [ḥāḡ](../../strongs/h/h2282.md) he shall ['asah](../../strongs/h/h6213.md) a [ʿōlâ](../../strongs/h/h5930.md) to [Yĕhovah](../../strongs/h/h3068.md), seven [par](../../strongs/h/h6499.md) and seven ['ayil](../../strongs/h/h352.md) without [tamiym](../../strongs/h/h8549.md) [yowm](../../strongs/h/h3117.md) the seven [yowm](../../strongs/h/h3117.md); and a [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) [yowm](../../strongs/h/h3117.md) for a [chatta'ath](../../strongs/h/h2403.md).

<a name="ezekiel_45_24"></a>Ezekiel 45:24

And he shall ['asah](../../strongs/h/h6213.md) a [minchah](../../strongs/h/h4503.md) of an ['êp̄â](../../strongs/h/h374.md) for a [par](../../strongs/h/h6499.md), and an ['êp̄â](../../strongs/h/h374.md) for an ['ayil](../../strongs/h/h352.md), and an [hîn](../../strongs/h/h1969.md) of [šemen](../../strongs/h/h8081.md) for an ['êp̄â](../../strongs/h/h374.md).

<a name="ezekiel_45_25"></a>Ezekiel 45:25

In the seventh, in the fifteenth [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md), shall he ['asah](../../strongs/h/h6213.md) the like in the [ḥāḡ](../../strongs/h/h2282.md) of the seven [yowm](../../strongs/h/h3117.md), according to the sin [chatta'ath](../../strongs/h/h2403.md), according to the [ʿōlâ](../../strongs/h/h5930.md), and according to the [minchah](../../strongs/h/h4503.md), and according to the [šemen](../../strongs/h/h8081.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 44](ezekiel_44.md) - [Ezekiel 46](ezekiel_46.md)