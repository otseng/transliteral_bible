# [Ezekiel 20](https://www.blueletterbible.org/kjv/ezekiel/20)

<a name="ezekiel_20_1"></a>Ezekiel 20:1

And it came to pass in the seventh [šānâ](../../strongs/h/h8141.md), in the fifth month, the tenth day of the [ḥōḏeš](../../strongs/h/h2320.md), that ['enowsh](../../strongs/h/h582.md) of the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) to [darash](../../strongs/h/h1875.md) of [Yĕhovah](../../strongs/h/h3068.md), and [yashab](../../strongs/h/h3427.md) [paniym](../../strongs/h/h6440.md) me.

<a name="ezekiel_20_2"></a>Ezekiel 20:2

Then came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_20_3"></a>Ezekiel 20:3

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [dabar](../../strongs/h/h1696.md) unto the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Are ye [bow'](../../strongs/h/h935.md) to [darash](../../strongs/h/h1875.md) of me? As I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), I will not be [darash](../../strongs/h/h1875.md) of by you.

<a name="ezekiel_20_4"></a>Ezekiel 20:4

Wilt thou [shaphat](../../strongs/h/h8199.md) them, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), wilt thou [shaphat](../../strongs/h/h8199.md) them? cause them to [yada'](../../strongs/h/h3045.md) the [tôʿēḇâ](../../strongs/h/h8441.md) of their ['ab](../../strongs/h/h1.md):

<a name="ezekiel_20_5"></a>Ezekiel 20:5

And ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); In the [yowm](../../strongs/h/h3117.md) when I [bāḥar](../../strongs/h/h977.md) [Yisra'el](../../strongs/h/h3478.md), and [nasa'](../../strongs/h/h5375.md) mine [yad](../../strongs/h/h3027.md) unto the [zera'](../../strongs/h/h2233.md) of the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), and made myself [yada'](../../strongs/h/h3045.md) unto them in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), when I [nasa'](../../strongs/h/h5375.md) up mine [yad](../../strongs/h/h3027.md) unto them, ['āmar](../../strongs/h/h559.md), I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md);

<a name="ezekiel_20_6"></a>Ezekiel 20:6

In the [yowm](../../strongs/h/h3117.md) that I [nasa'](../../strongs/h/h5375.md) mine [yad](../../strongs/h/h3027.md) unto them, to [yāṣā'](../../strongs/h/h3318.md) them of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) into an ['erets](../../strongs/h/h776.md) that I had [tûr](../../strongs/h/h8446.md) for them, [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md), which is the [ṣᵊḇî](../../strongs/h/h6643.md) of all ['erets](../../strongs/h/h776.md):

<a name="ezekiel_20_7"></a>Ezekiel 20:7

Then ['āmar](../../strongs/h/h559.md) I unto them, [shalak](../../strongs/h/h7993.md) ye every ['iysh](../../strongs/h/h376.md) the [šiqqûṣ](../../strongs/h/h8251.md) of his ['ayin](../../strongs/h/h5869.md), and [ṭāmē'](../../strongs/h/h2930.md) not yourselves with the [gillûl](../../strongs/h/h1544.md) of [Mitsrayim](../../strongs/h/h4714.md): I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="ezekiel_20_8"></a>Ezekiel 20:8

But they [marah](../../strongs/h/h4784.md) against me, and ['āḇâ](../../strongs/h/h14.md) not [shama'](../../strongs/h/h8085.md) unto me: they did not every ['iysh](../../strongs/h/h376.md) [shalak](../../strongs/h/h7993.md) the [šiqqûṣ](../../strongs/h/h8251.md) of their ['ayin](../../strongs/h/h5869.md), neither did they ['azab](../../strongs/h/h5800.md) the [gillûl](../../strongs/h/h1544.md) of [Mitsrayim](../../strongs/h/h4714.md): then I ['āmar](../../strongs/h/h559.md), I will [šāp̄aḵ](../../strongs/h/h8210.md) my [chemah](../../strongs/h/h2534.md) upon them, to [kalah](../../strongs/h/h3615.md) my ['aph](../../strongs/h/h639.md) against them in the midst of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="ezekiel_20_9"></a>Ezekiel 20:9

But I ['asah](../../strongs/h/h6213.md) for my [shem](../../strongs/h/h8034.md) sake, that it should not be [ḥālal](../../strongs/h/h2490.md) ['ayin](../../strongs/h/h5869.md) the [gowy](../../strongs/h/h1471.md), among whom they were, in whose ['ayin](../../strongs/h/h5869.md) I made myself [yada'](../../strongs/h/h3045.md) unto them, in bringing them [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="ezekiel_20_10"></a>Ezekiel 20:10

Wherefore I caused them to [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and [bow'](../../strongs/h/h935.md) them into the [midbar](../../strongs/h/h4057.md).

<a name="ezekiel_20_11"></a>Ezekiel 20:11

And I [nathan](../../strongs/h/h5414.md) them my [chuqqah](../../strongs/h/h2708.md), and [yada'](../../strongs/h/h3045.md) them my [mishpat](../../strongs/h/h4941.md), which if an ['āḏām](../../strongs/h/h120.md) ['asah](../../strongs/h/h6213.md), he shall even [chayay](../../strongs/h/h2425.md) in them.

<a name="ezekiel_20_12"></a>Ezekiel 20:12

Moreover also I [nathan](../../strongs/h/h5414.md) them my [shabbath](../../strongs/h/h7676.md), to be a ['ôṯ](../../strongs/h/h226.md) between me and them, that they might [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md) that [qadash](../../strongs/h/h6942.md) them.

<a name="ezekiel_20_13"></a>Ezekiel 20:13

But the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) [marah](../../strongs/h/h4784.md) against me in the [midbar](../../strongs/h/h4057.md): they [halak](../../strongs/h/h1980.md) not in my [chuqqah](../../strongs/h/h2708.md), and they [mā'as](../../strongs/h/h3988.md) my [mishpat](../../strongs/h/h4941.md), which if an ['āḏām](../../strongs/h/h120.md) ['asah](../../strongs/h/h6213.md), he shall even [chayay](../../strongs/h/h2425.md) in them; and my [shabbath](../../strongs/h/h7676.md) they [me'od](../../strongs/h/h3966.md) [ḥālal](../../strongs/h/h2490.md): then I ['āmar](../../strongs/h/h559.md), I would [šāp̄aḵ](../../strongs/h/h8210.md) my [chemah](../../strongs/h/h2534.md) upon them in the [midbar](../../strongs/h/h4057.md), to [kalah](../../strongs/h/h3615.md) them.

<a name="ezekiel_20_14"></a>Ezekiel 20:14

But I ['asah](../../strongs/h/h6213.md) for my [shem](../../strongs/h/h8034.md) sake, that it should not be [ḥālal](../../strongs/h/h2490.md) ['ayin](../../strongs/h/h5869.md) the [gowy](../../strongs/h/h1471.md), in whose ['ayin](../../strongs/h/h5869.md) I [yāṣā'](../../strongs/h/h3318.md) them.

<a name="ezekiel_20_15"></a>Ezekiel 20:15

Yet also I [nasa'](../../strongs/h/h5375.md) my [yad](../../strongs/h/h3027.md) unto them in the [midbar](../../strongs/h/h4057.md), that I would not [bow'](../../strongs/h/h935.md) them into the ['erets](../../strongs/h/h776.md) which I had [nathan](../../strongs/h/h5414.md) them, [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md), which is the [ṣᵊḇî](../../strongs/h/h6643.md) of all ['erets](../../strongs/h/h776.md);

<a name="ezekiel_20_16"></a>Ezekiel 20:16

Because they [mā'as](../../strongs/h/h3988.md) my [mishpat](../../strongs/h/h4941.md), and [halak](../../strongs/h/h1980.md) not in my [chuqqah](../../strongs/h/h2708.md), but [ḥālal](../../strongs/h/h2490.md) my [shabbath](../../strongs/h/h7676.md): for their [leb](../../strongs/h/h3820.md) [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) their [gillûl](../../strongs/h/h1544.md).

<a name="ezekiel_20_17"></a>Ezekiel 20:17

Nevertheless mine ['ayin](../../strongs/h/h5869.md) [ḥûs](../../strongs/h/h2347.md) them from [shachath](../../strongs/h/h7843.md) them, neither did I ['asah](../../strongs/h/h6213.md) a [kālâ](../../strongs/h/h3617.md) of them in the [midbar](../../strongs/h/h4057.md).

<a name="ezekiel_20_18"></a>Ezekiel 20:18

But I ['āmar](../../strongs/h/h559.md) unto their [ben](../../strongs/h/h1121.md) in the [midbar](../../strongs/h/h4057.md), [yālaḵ](../../strongs/h/h3212.md) ye not in the [choq](../../strongs/h/h2706.md) of your ['ab](../../strongs/h/h1.md), neither [shamar](../../strongs/h/h8104.md) their [mishpat](../../strongs/h/h4941.md), nor [ṭāmē'](../../strongs/h/h2930.md) yourselves with their [gillûl](../../strongs/h/h1544.md):

<a name="ezekiel_20_19"></a>Ezekiel 20:19

I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md); [yālaḵ](../../strongs/h/h3212.md) in my [chuqqah](../../strongs/h/h2708.md), and [shamar](../../strongs/h/h8104.md) my [mishpat](../../strongs/h/h4941.md), and ['asah](../../strongs/h/h6213.md) them;

<a name="ezekiel_20_20"></a>Ezekiel 20:20

And [qadash](../../strongs/h/h6942.md) my [shabbath](../../strongs/h/h7676.md); and they shall be a ['ôṯ](../../strongs/h/h226.md) between me and you, that ye may [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="ezekiel_20_21"></a>Ezekiel 20:21

Notwithstanding the [ben](../../strongs/h/h1121.md) [marah](../../strongs/h/h4784.md) against me: they [halak](../../strongs/h/h1980.md) not in my [chuqqah](../../strongs/h/h2708.md), neither [shamar](../../strongs/h/h8104.md) my [mishpat](../../strongs/h/h4941.md) to ['asah](../../strongs/h/h6213.md) them, which if an ['āḏām](../../strongs/h/h120.md) ['asah](../../strongs/h/h6213.md), he shall even [chayay](../../strongs/h/h2425.md) in them; they [ḥālal](../../strongs/h/h2490.md) my [shabbath](../../strongs/h/h7676.md): then I ['āmar](../../strongs/h/h559.md), I would [šāp̄aḵ](../../strongs/h/h8210.md) my [chemah](../../strongs/h/h2534.md) upon them, to [kalah](../../strongs/h/h3615.md) my ['aph](../../strongs/h/h639.md) against them in the [midbar](../../strongs/h/h4057.md).

<a name="ezekiel_20_22"></a>Ezekiel 20:22

Nevertheless I [shuwb](../../strongs/h/h7725.md) mine [yad](../../strongs/h/h3027.md), and ['asah](../../strongs/h/h6213.md) for my [shem](../../strongs/h/h8034.md) sake, that it should not be [ḥālal](../../strongs/h/h2490.md) in the ['ayin](../../strongs/h/h5869.md) of the [gowy](../../strongs/h/h1471.md), in whose ['ayin](../../strongs/h/h5869.md) I brought them [yāṣā'](../../strongs/h/h3318.md).

<a name="ezekiel_20_23"></a>Ezekiel 20:23

I [nasa'](../../strongs/h/h5375.md) mine [yad](../../strongs/h/h3027.md) unto them also in the [midbar](../../strongs/h/h4057.md), that I would [puwts](../../strongs/h/h6327.md) them among the [gowy](../../strongs/h/h1471.md), and [zārâ](../../strongs/h/h2219.md) them through the ['erets](../../strongs/h/h776.md);

<a name="ezekiel_20_24"></a>Ezekiel 20:24

Because they had not ['asah](../../strongs/h/h6213.md) my [mishpat](../../strongs/h/h4941.md), but had [mā'as](../../strongs/h/h3988.md) my [chuqqah](../../strongs/h/h2708.md), and had [ḥālal](../../strongs/h/h2490.md) my [shabbath](../../strongs/h/h7676.md), and their ['ayin](../../strongs/h/h5869.md) were ['aḥar](../../strongs/h/h310.md) their ['ab](../../strongs/h/h1.md) [gillûl](../../strongs/h/h1544.md).

<a name="ezekiel_20_25"></a>Ezekiel 20:25

Wherefore I [nathan](../../strongs/h/h5414.md) them also [choq](../../strongs/h/h2706.md) that were not [towb](../../strongs/h/h2896.md), and [mishpat](../../strongs/h/h4941.md) whereby they should not [ḥāyâ](../../strongs/h/h2421.md);

<a name="ezekiel_20_26"></a>Ezekiel 20:26

And I [ṭāmē'](../../strongs/h/h2930.md) them in their own [matānâ](../../strongs/h/h4979.md), in that they caused to ['abar](../../strongs/h/h5674.md) the fire all that [peṭer](../../strongs/h/h6363.md) the [raḥam](../../strongs/h/h7356.md), that I might make them [šāmēm](../../strongs/h/h8074.md), to the end that they might [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_20_27"></a>Ezekiel 20:27

Therefore, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [dabar](../../strongs/h/h1696.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Yet in this your ['ab](../../strongs/h/h1.md) have [gāḏap̄](../../strongs/h/h1442.md) me, in that they have [māʿal](../../strongs/h/h4603.md) a [maʿal](../../strongs/h/h4604.md) against me.

<a name="ezekiel_20_28"></a>Ezekiel 20:28

For when I had [bow'](../../strongs/h/h935.md) them into the ['erets](../../strongs/h/h776.md), for the which I [nasa'](../../strongs/h/h5375.md) mine [yad](../../strongs/h/h3027.md) to [nathan](../../strongs/h/h5414.md) it to them, then they [ra'ah](../../strongs/h/h7200.md) every [ruwm](../../strongs/h/h7311.md) [giḇʿâ](../../strongs/h/h1389.md), and all the [ʿāḇōṯ](../../strongs/h/h5687.md) ['ets](../../strongs/h/h6086.md), and they [zabach](../../strongs/h/h2076.md) there their [zebach](../../strongs/h/h2077.md), and there they [nathan](../../strongs/h/h5414.md) the [ka'ac](../../strongs/h/h3708.md) of their [qorban](../../strongs/h/h7133.md): there also they [śûm](../../strongs/h/h7760.md) their [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md), and poured [nacak](../../strongs/h/h5258.md) there their [necek](../../strongs/h/h5262.md).

<a name="ezekiel_20_29"></a>Ezekiel 20:29

Then I ['āmar](../../strongs/h/h559.md) unto them, What is the [bāmâ](../../strongs/h/h1116.md) whereunto ye [bow'](../../strongs/h/h935.md)? And the [shem](../../strongs/h/h8034.md) thereof is [qara'](../../strongs/h/h7121.md) [Ḇmh](../../strongs/h/h1117.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="ezekiel_20_30"></a>Ezekiel 20:30

Wherefore ['āmar](../../strongs/h/h559.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Are ye [ṭāmē'](../../strongs/h/h2930.md) after the [derek](../../strongs/h/h1870.md) of your ['ab](../../strongs/h/h1.md)? and commit ye [zānâ](../../strongs/h/h2181.md) ['aḥar](../../strongs/h/h310.md) their [šiqqûṣ](../../strongs/h/h8251.md)?

<a name="ezekiel_20_31"></a>Ezekiel 20:31

For when ye [nasa'](../../strongs/h/h5375.md) your [matānâ](../../strongs/h/h4979.md), when ye make your [ben](../../strongs/h/h1121.md) to ['abar](../../strongs/h/h5674.md) the ['esh](../../strongs/h/h784.md), ye [ṭāmē'](../../strongs/h/h2930.md) yourselves with all your [gillûl](../../strongs/h/h1544.md), even unto this [yowm](../../strongs/h/h3117.md): and shall I be [darash](../../strongs/h/h1875.md) of by you, O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md)? As I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), I will not be [darash](../../strongs/h/h1875.md) of by you.

<a name="ezekiel_20_32"></a>Ezekiel 20:32

And that which [ʿālâ](../../strongs/h/h5927.md) into your [ruwach](../../strongs/h/h7307.md) shall not be at all, that ye ['āmar](../../strongs/h/h559.md), We will be as the [gowy](../../strongs/h/h1471.md), as the [mišpāḥâ](../../strongs/h/h4940.md) of the ['erets](../../strongs/h/h776.md), to [sharath](../../strongs/h/h8334.md) ['ets](../../strongs/h/h6086.md) and ['eben](../../strongs/h/h68.md).

<a name="ezekiel_20_33"></a>Ezekiel 20:33

As I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), surely with a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md), and with a [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md), and with [chemah](../../strongs/h/h2534.md) poured [šāp̄aḵ](../../strongs/h/h8210.md), will I [mālaḵ](../../strongs/h/h4427.md) over you:

<a name="ezekiel_20_34"></a>Ezekiel 20:34

And I will bring you [yāṣā'](../../strongs/h/h3318.md) from the ['am](../../strongs/h/h5971.md), and will [qāḇaṣ](../../strongs/h/h6908.md) you out of the ['erets](../../strongs/h/h776.md) wherein ye are [puwts](../../strongs/h/h6327.md), with a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md), and with a [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md), and with [chemah](../../strongs/h/h2534.md) poured [šāp̄aḵ](../../strongs/h/h8210.md).

<a name="ezekiel_20_35"></a>Ezekiel 20:35

And I will [bow'](../../strongs/h/h935.md) you into the [midbar](../../strongs/h/h4057.md) of the ['am](../../strongs/h/h5971.md), and there will I [shaphat](../../strongs/h/h8199.md) with you [paniym](../../strongs/h/h6440.md) to [paniym](../../strongs/h/h6440.md).

<a name="ezekiel_20_36"></a>Ezekiel 20:36

Like as I [shaphat](../../strongs/h/h8199.md) with your ['ab](../../strongs/h/h1.md) in the [midbar](../../strongs/h/h4057.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), so will I [shaphat](../../strongs/h/h8199.md) with you, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_20_37"></a>Ezekiel 20:37

And I will cause you to ['abar](../../strongs/h/h5674.md) under the [shebet](../../strongs/h/h7626.md), and I will [bow'](../../strongs/h/h935.md) you into the [māsōreṯ](../../strongs/h/h4562.md) of the [bĕriyth](../../strongs/h/h1285.md):

<a name="ezekiel_20_38"></a>Ezekiel 20:38

And I will [bārar](../../strongs/h/h1305.md) from among you the [māraḏ](../../strongs/h/h4775.md), and them that [pāšaʿ](../../strongs/h/h6586.md) against me: I will [yāṣā'](../../strongs/h/h3318.md) them out of the ['erets](../../strongs/h/h776.md) where they [māḡûr](../../strongs/h/h4033.md), and they shall not [bow'](../../strongs/h/h935.md) into the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md): and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_20_39"></a>Ezekiel 20:39

As for you, O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); [yālaḵ](../../strongs/h/h3212.md) ye, ['abad](../../strongs/h/h5647.md) ye every ['iysh](../../strongs/h/h376.md) his [gillûl](../../strongs/h/h1544.md), and ['aḥar](../../strongs/h/h310.md) also, if ye will not [shama'](../../strongs/h/h8085.md) unto me: but [ḥālal](../../strongs/h/h2490.md) ye my [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md) no more with your [matānâ](../../strongs/h/h4979.md), and with your [gillûl](../../strongs/h/h1544.md).

<a name="ezekiel_20_40"></a>Ezekiel 20:40

For in mine [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md), in the [har](../../strongs/h/h2022.md) of the [marowm](../../strongs/h/h4791.md) of [Yisra'el](../../strongs/h/h3478.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), there shall all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), all of them in the ['erets](../../strongs/h/h776.md), ['abad](../../strongs/h/h5647.md) me: there will I [ratsah](../../strongs/h/h7521.md) them, and there will I [darash](../../strongs/h/h1875.md) your [tᵊrûmâ](../../strongs/h/h8641.md), and the [re'shiyth](../../strongs/h/h7225.md) of your [maśśᵊ'ēṯ](../../strongs/h/h4864.md), with all your [qodesh](../../strongs/h/h6944.md).

<a name="ezekiel_20_41"></a>Ezekiel 20:41

I will [ratsah](../../strongs/h/h7521.md) you with your [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md), when I bring you [yāṣā'](../../strongs/h/h3318.md) from the ['am](../../strongs/h/h5971.md), and [qāḇaṣ](../../strongs/h/h6908.md) you out of the ['erets](../../strongs/h/h776.md) wherein ye have been [puwts](../../strongs/h/h6327.md); and I will be [qadash](../../strongs/h/h6942.md) in you ['ayin](../../strongs/h/h5869.md) the [gowy](../../strongs/h/h1471.md).

<a name="ezekiel_20_42"></a>Ezekiel 20:42

And ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when I shall [bow'](../../strongs/h/h935.md) you into the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md), into the ['erets](../../strongs/h/h776.md) for the which I [nasa'](../../strongs/h/h5375.md) mine [yad](../../strongs/h/h3027.md) to [nathan](../../strongs/h/h5414.md) it to your ['ab](../../strongs/h/h1.md).

<a name="ezekiel_20_43"></a>Ezekiel 20:43

And there shall ye [zakar](../../strongs/h/h2142.md) your [derek](../../strongs/h/h1870.md), and all your ['aliylah](../../strongs/h/h5949.md), wherein ye have been [ṭāmē'](../../strongs/h/h2930.md); and ye shall [qûṭ](../../strongs/h/h6962.md) yourselves in your own [paniym](../../strongs/h/h6440.md) for all your [ra'](../../strongs/h/h7451.md) that ye have ['asah](../../strongs/h/h6213.md).

<a name="ezekiel_20_44"></a>Ezekiel 20:44

And ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when I have ['asah](../../strongs/h/h6213.md) with you for my [shem](../../strongs/h/h8034.md) sake, not according to your [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md), nor according to your [shachath](../../strongs/h/h7843.md) ['aliylah](../../strongs/h/h5949.md), O ye [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_20_45"></a>Ezekiel 20:45

Moreover the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_20_46"></a>Ezekiel 20:46

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [śûm](../../strongs/h/h7760.md) thy [paniym](../../strongs/h/h6440.md) [derek](../../strongs/h/h1870.md) the [têmān](../../strongs/h/h8486.md), and [nāṭap̄](../../strongs/h/h5197.md) toward the [dārôm](../../strongs/h/h1864.md), and [nāḇā'](../../strongs/h/h5012.md) against the [yaʿar](../../strongs/h/h3293.md) of the [neḡeḇ](../../strongs/h/h5045.md) [sadeh](../../strongs/h/h7704.md);

<a name="ezekiel_20_47"></a>Ezekiel 20:47

And ['āmar](../../strongs/h/h559.md) to the [yaʿar](../../strongs/h/h3293.md) of the [neḡeḇ](../../strongs/h/h5045.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md); Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I will [yāṣaṯ](../../strongs/h/h3341.md) an ['esh](../../strongs/h/h784.md) in thee, and it shall ['akal](../../strongs/h/h398.md) every [laḥ](../../strongs/h/h3892.md) ['ets](../../strongs/h/h6086.md) in thee, and every [yāḇēš](../../strongs/h/h3002.md) ['ets](../../strongs/h/h6086.md): the [lehāḇâ](../../strongs/h/h3852.md) [šalheḇeṯ](../../strongs/h/h7957.md) shall not be [kāḇâ](../../strongs/h/h3518.md), and all [paniym](../../strongs/h/h6440.md) from the [neḡeḇ](../../strongs/h/h5045.md) to the [ṣāp̄ôn](../../strongs/h/h6828.md) shall be [ṣāraḇ](../../strongs/h/h6866.md) therein.

<a name="ezekiel_20_48"></a>Ezekiel 20:48

And all [basar](../../strongs/h/h1320.md) shall [ra'ah](../../strongs/h/h7200.md) that I [Yĕhovah](../../strongs/h/h3068.md) have [bāʿar](../../strongs/h/h1197.md) it: it shall not be [kāḇâ](../../strongs/h/h3518.md).

<a name="ezekiel_20_49"></a>Ezekiel 20:49

Then ['āmar](../../strongs/h/h559.md) I, ['ăhâ](../../strongs/h/h162.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md)! they ['āmar](../../strongs/h/h559.md) of me, Doth he not [māšal](../../strongs/h/h4911.md) [māšāl](../../strongs/h/h4912.md)?

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 19](ezekiel_19.md) - [Ezekiel 21](ezekiel_21.md)