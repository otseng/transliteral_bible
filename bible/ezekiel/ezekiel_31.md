# [Ezekiel 31](https://www.blueletterbible.org/kjv/ezekiel/31)

<a name="ezekiel_31_1"></a>Ezekiel 31:1

And it came to pass in the eleventh [šānâ](../../strongs/h/h8141.md), in the third month, in the first day of the [ḥōḏeš](../../strongs/h/h2320.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_31_2"></a>Ezekiel 31:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), ['āmar](../../strongs/h/h559.md) unto [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), and to his [hāmôn](../../strongs/h/h1995.md); Whom art thou [dāmâ](../../strongs/h/h1819.md) in thy [gōḏel](../../strongs/h/h1433.md)?

<a name="ezekiel_31_3"></a>Ezekiel 31:3

Behold, the ['Aššûr](../../strongs/h/h804.md) was an ['erez](../../strongs/h/h730.md) in [Lᵊḇānôn](../../strongs/h/h3844.md) with [yāp̄ê](../../strongs/h/h3303.md) [ʿānāp̄](../../strongs/h/h6057.md), and with a [ṣālal](../../strongs/h/h6751.md) [ḥōreš](../../strongs/h/h2793.md), and of a [gāḇâ](../../strongs/h/h1362.md) [qômâ](../../strongs/h/h6967.md); and his [ṣammereṯ](../../strongs/h/h6788.md) was among the [ʿăḇōṯ](../../strongs/h/h5688.md).

<a name="ezekiel_31_4"></a>Ezekiel 31:4

The [mayim](../../strongs/h/h4325.md) made him [gāḏal](../../strongs/h/h1431.md), the [tĕhowm](../../strongs/h/h8415.md) set him up on [ruwm](../../strongs/h/h7311.md) with her [nāhār](../../strongs/h/h5104.md) [halak](../../strongs/h/h1980.md) [cabiyb](../../strongs/h/h5439.md) his [maṭṭāʿ](../../strongs/h/h4302.md), and [shalach](../../strongs/h/h7971.md) her [tᵊʿālâ](../../strongs/h/h8585.md) unto all the ['ets](../../strongs/h/h6086.md) of the [sadeh](../../strongs/h/h7704.md).

<a name="ezekiel_31_5"></a>Ezekiel 31:5

Therefore his [qômâ](../../strongs/h/h6967.md) was [gāḇah](../../strongs/h/h1361.md) above all the ['ets](../../strongs/h/h6086.md) of the [sadeh](../../strongs/h/h7704.md), and his [sarʿapâ](../../strongs/h/h5634.md) were [rabah](../../strongs/h/h7235.md), and his [pō'râ](../../strongs/h/h6288.md) became ['arak](../../strongs/h/h748.md) because of the [rab](../../strongs/h/h7227.md) of [mayim](../../strongs/h/h4325.md), when he [shalach](../../strongs/h/h7971.md).

<a name="ezekiel_31_6"></a>Ezekiel 31:6

All the [ʿôp̄](../../strongs/h/h5775.md) of [shamayim](../../strongs/h/h8064.md) made their [qānan](../../strongs/h/h7077.md) in his [sᵊʿapâ](../../strongs/h/h5589.md), and under his [pō'râ](../../strongs/h/h6288.md) did all the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) bring forth their [yalad](../../strongs/h/h3205.md), and under his [ṣēl](../../strongs/h/h6738.md) [yashab](../../strongs/h/h3427.md) all [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md).

<a name="ezekiel_31_7"></a>Ezekiel 31:7

Thus was he [yāp̄â](../../strongs/h/h3302.md) in his [gōḏel](../../strongs/h/h1433.md), in the ['ōreḵ](../../strongs/h/h753.md) of his [dālîṯ](../../strongs/h/h1808.md): for his [šereš](../../strongs/h/h8328.md) was by [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md).

<a name="ezekiel_31_8"></a>Ezekiel 31:8

The ['erez](../../strongs/h/h730.md) in the [gan](../../strongs/h/h1588.md) of ['Elohiym](../../strongs/h/h430.md) could not [ʿāmam](../../strongs/h/h6004.md) him: the [bᵊrôš](../../strongs/h/h1265.md) were not [dāmâ](../../strongs/h/h1819.md) his [sᵊʿapâ](../../strongs/h/h5589.md), and the [ʿarmôn](../../strongs/h/h6196.md) were not like his [pō'râ](../../strongs/h/h6288.md); nor any ['ets](../../strongs/h/h6086.md) in the [gan](../../strongs/h/h1588.md) of ['Elohiym](../../strongs/h/h430.md) was [dāmâ](../../strongs/h/h1819.md) unto him in his [yᵊp̄î](../../strongs/h/h3308.md).

<a name="ezekiel_31_9"></a>Ezekiel 31:9

I have ['asah](../../strongs/h/h6213.md) him [yāp̄ê](../../strongs/h/h3303.md) by the [rōḇ](../../strongs/h/h7230.md) of his [dālîṯ](../../strongs/h/h1808.md): so that all the ['ets](../../strongs/h/h6086.md) of ['Eden](../../strongs/h/h5731.md), that were in the [gan](../../strongs/h/h1588.md) of ['Elohiym](../../strongs/h/h430.md), [qānā'](../../strongs/h/h7065.md) him.

<a name="ezekiel_31_10"></a>Ezekiel 31:10

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because thou hast [gāḇah](../../strongs/h/h1361.md) thyself in [qômâ](../../strongs/h/h6967.md), and he hath [nathan](../../strongs/h/h5414.md) his [ṣammereṯ](../../strongs/h/h6788.md) among the [ʿăḇōṯ](../../strongs/h/h5688.md), and his [lebab](../../strongs/h/h3824.md) is [ruwm](../../strongs/h/h7311.md) in his [gobahh](../../strongs/h/h1363.md);

<a name="ezekiel_31_11"></a>Ezekiel 31:11

I have therefore [nathan](../../strongs/h/h5414.md) him into the [yad](../../strongs/h/h3027.md) of the ['el](../../strongs/h/h410.md) of the [gowy](../../strongs/h/h1471.md); he shall ['asah](../../strongs/h/h6213.md) ['asah](../../strongs/h/h6213.md) with him: I have [gāraš](../../strongs/h/h1644.md) him for his [resha'](../../strongs/h/h7562.md).

<a name="ezekiel_31_12"></a>Ezekiel 31:12

And [zûr](../../strongs/h/h2114.md), the [ʿārîṣ](../../strongs/h/h6184.md) of the [gowy](../../strongs/h/h1471.md), have [karath](../../strongs/h/h3772.md) him, and have [nāṭaš](../../strongs/h/h5203.md) him: upon the [har](../../strongs/h/h2022.md) and in all the [gay'](../../strongs/h/h1516.md) his [dālîṯ](../../strongs/h/h1808.md) are [naphal](../../strongs/h/h5307.md), and his [pō'râ](../../strongs/h/h6288.md) are [shabar](../../strongs/h/h7665.md) by all the ['āp̄îq](../../strongs/h/h650.md) of the ['erets](../../strongs/h/h776.md); and all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) are [yarad](../../strongs/h/h3381.md) from his [ṣēl](../../strongs/h/h6738.md), and have [nāṭaš](../../strongs/h/h5203.md) him.

<a name="ezekiel_31_13"></a>Ezekiel 31:13

Upon his [mapeleṯ](../../strongs/h/h4658.md) shall all the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md) [shakan](../../strongs/h/h7931.md), and all the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) shall be upon his [pō'râ](../../strongs/h/h6288.md):

<a name="ezekiel_31_14"></a>Ezekiel 31:14

To the end that none of all the ['ets](../../strongs/h/h6086.md) by the [mayim](../../strongs/h/h4325.md) [gāḇah](../../strongs/h/h1361.md) themselves for their [qômâ](../../strongs/h/h6967.md), neither [nathan](../../strongs/h/h5414.md) their [ṣammereṯ](../../strongs/h/h6788.md) among the [ʿăḇōṯ](../../strongs/h/h5688.md), neither their ['ayil](../../strongs/h/h352.md) ['amad](../../strongs/h/h5975.md) in their [gobahh](../../strongs/h/h1363.md), all that [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md): for they are all [nathan](../../strongs/h/h5414.md) unto [maveth](../../strongs/h/h4194.md), to the nether [taḥtî](../../strongs/h/h8482.md) of the ['erets](../../strongs/h/h776.md), in the midst of the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), with them that [yarad](../../strongs/h/h3381.md) to the [bowr](../../strongs/h/h953.md).

<a name="ezekiel_31_15"></a>Ezekiel 31:15

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); In the [yowm](../../strongs/h/h3117.md) when he [yarad](../../strongs/h/h3381.md) to the [shĕ'owl](../../strongs/h/h7585.md) I caused an ['āḇal](../../strongs/h/h56.md): I [kāsâ](../../strongs/h/h3680.md) the [tĕhowm](../../strongs/h/h8415.md) for him, and I [mānaʿ](../../strongs/h/h4513.md) the [nāhār](../../strongs/h/h5104.md) thereof, and the [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md) were [kālā'](../../strongs/h/h3607.md): and I caused [Lᵊḇānôn](../../strongs/h/h3844.md) to [qāḏar](../../strongs/h/h6937.md) for him, and all the ['ets](../../strongs/h/h6086.md) of the [sadeh](../../strongs/h/h7704.md) [ʿulp̄ê](../../strongs/h/h5969.md) for him.

<a name="ezekiel_31_16"></a>Ezekiel 31:16

I made the [gowy](../../strongs/h/h1471.md) to [rāʿaš](../../strongs/h/h7493.md) at the [qowl](../../strongs/h/h6963.md) of his [mapeleṯ](../../strongs/h/h4658.md), when I [yarad](../../strongs/h/h3381.md) him to [shĕ'owl](../../strongs/h/h7585.md) with them that [yarad](../../strongs/h/h3381.md) into the [bowr](../../strongs/h/h953.md): and all the ['ets](../../strongs/h/h6086.md) of ['Eden](../../strongs/h/h5731.md), the [miḇḥār](../../strongs/h/h4005.md) and [towb](../../strongs/h/h2896.md) of [Lᵊḇānôn](../../strongs/h/h3844.md), all that [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md), shall be [nacham](../../strongs/h/h5162.md) in the [taḥtî](../../strongs/h/h8482.md) of the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_31_17"></a>Ezekiel 31:17

They also [yarad](../../strongs/h/h3381.md) into [shĕ'owl](../../strongs/h/h7585.md) with him unto them that be [ḥālāl](../../strongs/h/h2491.md) with the [chereb](../../strongs/h/h2719.md); and they that were his [zerowa'](../../strongs/h/h2220.md), that [yashab](../../strongs/h/h3427.md) under his [ṣēl](../../strongs/h/h6738.md) in the [tavek](../../strongs/h/h8432.md) of the [gowy](../../strongs/h/h1471.md).

<a name="ezekiel_31_18"></a>Ezekiel 31:18

To whom art thou thus [dāmâ](../../strongs/h/h1819.md) in [kabowd](../../strongs/h/h3519.md) and in [gōḏel](../../strongs/h/h1433.md) among the ['ets](../../strongs/h/h6086.md) of ['Eden](../../strongs/h/h5731.md)? yet shalt thou be [yarad](../../strongs/h/h3381.md) with the ['ets](../../strongs/h/h6086.md) of ['Eden](../../strongs/h/h5731.md) unto the [taḥtî](../../strongs/h/h8482.md) of the ['erets](../../strongs/h/h776.md): thou shalt [shakab](../../strongs/h/h7901.md) in the midst of the [ʿārēl](../../strongs/h/h6189.md) with them that be [ḥālāl](../../strongs/h/h2491.md) by the [chereb](../../strongs/h/h2719.md). This is [Parʿô](../../strongs/h/h6547.md) and all his [hāmôn](../../strongs/h/h1995.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 30](ezekiel_30.md) - [Ezekiel 32](ezekiel_32.md)