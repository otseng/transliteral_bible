# [Ezekiel 35](https://www.blueletterbible.org/kjv/ezekiel/35)

<a name="ezekiel_35_1"></a>Ezekiel 35:1

Moreover the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_35_2"></a>Ezekiel 35:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [śûm](../../strongs/h/h7760.md) thy [paniym](../../strongs/h/h6440.md) against [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md), and [nāḇā'](../../strongs/h/h5012.md) against it,

<a name="ezekiel_35_3"></a>Ezekiel 35:3

And ['āmar](../../strongs/h/h559.md) unto it, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, O [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md), I am against thee, and I will [natah](../../strongs/h/h5186.md) mine [yad](../../strongs/h/h3027.md) against thee, and I will [nathan](../../strongs/h/h5414.md) thee [mᵊšammâ](../../strongs/h/h4923.md) [šᵊmāmâ](../../strongs/h/h8077.md).

<a name="ezekiel_35_4"></a>Ezekiel 35:4

I will [śûm](../../strongs/h/h7760.md) thy [ʿîr](../../strongs/h/h5892.md) [chorbah](../../strongs/h/h2723.md), and thou shalt be [šᵊmāmâ](../../strongs/h/h8077.md), and thou shalt [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_35_5"></a>Ezekiel 35:5

Because thou hast had a ['owlam](../../strongs/h/h5769.md) ['eybah](../../strongs/h/h342.md), and hast [nāḡar](../../strongs/h/h5064.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) by the [yad](../../strongs/h/h3027.md) of the [chereb](../../strongs/h/h2719.md) in the [ʿēṯ](../../strongs/h/h6256.md) of their ['êḏ](../../strongs/h/h343.md), in the [ʿēṯ](../../strongs/h/h6256.md) that their ['avon](../../strongs/h/h5771.md) had a [qēṣ](../../strongs/h/h7093.md):

<a name="ezekiel_35_6"></a>Ezekiel 35:6

Therefore, as I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), I will ['asah](../../strongs/h/h6213.md) thee unto [dam](../../strongs/h/h1818.md), and [dam](../../strongs/h/h1818.md) shall [radaph](../../strongs/h/h7291.md) thee: ['im](../../strongs/h/h518.md) thou hast not [sane'](../../strongs/h/h8130.md) [dam](../../strongs/h/h1818.md), even [dam](../../strongs/h/h1818.md) shall [radaph](../../strongs/h/h7291.md) thee.

<a name="ezekiel_35_7"></a>Ezekiel 35:7

Thus will I [nathan](../../strongs/h/h5414.md) [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md) [šᵊmāmâ](../../strongs/h/h8077.md) [šᵊmāmâ](../../strongs/h/h8077.md), and [karath](../../strongs/h/h3772.md) from it him that ['abar](../../strongs/h/h5674.md) and him that [shuwb](../../strongs/h/h7725.md).

<a name="ezekiel_35_8"></a>Ezekiel 35:8

And I will [mālā'](../../strongs/h/h4390.md) his [har](../../strongs/h/h2022.md) with his [ḥālāl](../../strongs/h/h2491.md): in thy [giḇʿâ](../../strongs/h/h1389.md), and in thy [gay'](../../strongs/h/h1516.md), and in all thy ['āp̄îq](../../strongs/h/h650.md), shall they [naphal](../../strongs/h/h5307.md) that are [ḥālāl](../../strongs/h/h2491.md) with the [chereb](../../strongs/h/h2719.md).

<a name="ezekiel_35_9"></a>Ezekiel 35:9

I will [nathan](../../strongs/h/h5414.md) thee ['owlam](../../strongs/h/h5769.md) [šᵊmāmâ](../../strongs/h/h8077.md), and thy [ʿîr](../../strongs/h/h5892.md) shall not [shuwb](../../strongs/h/h7725.md) [yashab](../../strongs/h/h3427.md): and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_35_10"></a>Ezekiel 35:10

Because thou hast ['āmar](../../strongs/h/h559.md), These two [gowy](../../strongs/h/h1471.md) and these two ['erets](../../strongs/h/h776.md) shall be mine, and we will [yarash](../../strongs/h/h3423.md) it; whereas [Yĕhovah](../../strongs/h/h3068.md) was there:

<a name="ezekiel_35_11"></a>Ezekiel 35:11

Therefore, as I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), I will even ['asah](../../strongs/h/h6213.md) according to thine ['aph](../../strongs/h/h639.md), and according to thine [qin'â](../../strongs/h/h7068.md) which thou hast ['asah](../../strongs/h/h6213.md) out of thy [śin'â](../../strongs/h/h8135.md) against them; and I will make myself [yada'](../../strongs/h/h3045.md) among them, when I have [shaphat](../../strongs/h/h8199.md) thee.

<a name="ezekiel_35_12"></a>Ezekiel 35:12

And thou shalt [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), and that I have [shama'](../../strongs/h/h8085.md) all thy [ne'āṣâ](../../strongs/h/h5007.md) which thou hast ['āmar](../../strongs/h/h559.md) against the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), They are [šāmēm](../../strongs/h/h8074.md) [šᵊmāmâ](../../strongs/h/h8077.md), they are [nathan](../../strongs/h/h5414.md) us to ['oklah](../../strongs/h/h402.md).

<a name="ezekiel_35_13"></a>Ezekiel 35:13

Thus with your [peh](../../strongs/h/h6310.md) ye have [gāḏal](../../strongs/h/h1431.md) against me, and have [ʿāṯar](../../strongs/h/h6280.md) your [dabar](../../strongs/h/h1697.md) against me: I have [shama'](../../strongs/h/h8085.md) them.

<a name="ezekiel_35_14"></a>Ezekiel 35:14

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); When the ['erets](../../strongs/h/h776.md) [samach](../../strongs/h/h8055.md), I will ['asah](../../strongs/h/h6213.md) thee [šᵊmāmâ](../../strongs/h/h8077.md).

<a name="ezekiel_35_15"></a>Ezekiel 35:15

As thou didst [simchah](../../strongs/h/h8057.md) at the [nachalah](../../strongs/h/h5159.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), because it was [šāmēm](../../strongs/h/h8074.md), so will I ['asah](../../strongs/h/h6213.md) unto thee: thou shalt be [šᵊmāmâ](../../strongs/h/h8077.md), O [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md), and all ['Ĕḏōm](../../strongs/h/h123.md), even all of it: and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 34](ezekiel_34.md) - [Ezekiel 36](ezekiel_36.md)