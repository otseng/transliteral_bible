# [Ezekiel 32](https://www.blueletterbible.org/kjv/ezekiel/32)

<a name="ezekiel_32_1"></a>Ezekiel 32:1

And it came to pass in the twelfth [šānâ](../../strongs/h/h8141.md), in the twelfth [ḥōḏeš](../../strongs/h/h2320.md), in the first day of the [ḥōḏeš](../../strongs/h/h2320.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_32_2"></a>Ezekiel 32:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nasa'](../../strongs/h/h5375.md) a [qînâ](../../strongs/h/h7015.md) for [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), and ['āmar](../../strongs/h/h559.md) unto him, Thou art [dāmâ](../../strongs/h/h1819.md) a [kephiyr](../../strongs/h/h3715.md) of the [gowy](../../strongs/h/h1471.md), and thou art as a [tannîn](../../strongs/h/h8577.md) [tan](../../strongs/h/h8565.md) in the [yam](../../strongs/h/h3220.md): and thou [gîaḥ](../../strongs/h/h1518.md) with thy [nāhār](../../strongs/h/h5104.md), and [dālaḥ](../../strongs/h/h1804.md) the [mayim](../../strongs/h/h4325.md) with thy [regel](../../strongs/h/h7272.md), and [rāp̄aś](../../strongs/h/h7515.md) their [nāhār](../../strongs/h/h5104.md).

<a name="ezekiel_32_3"></a>Ezekiel 32:3

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); I will therefore spread [pāraś](../../strongs/h/h6566.md) my [rešeṯ](../../strongs/h/h7568.md) over thee with a [qāhēl](../../strongs/h/h6951.md) of [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md); and they shall bring thee [ʿālâ](../../strongs/h/h5927.md) in my [ḥērem](../../strongs/h/h2764.md).

<a name="ezekiel_32_4"></a>Ezekiel 32:4

Then will I [nāṭaš](../../strongs/h/h5203.md) thee upon the ['erets](../../strongs/h/h776.md), I will cast thee [ṭûl](../../strongs/h/h2904.md) upon the [paniym](../../strongs/h/h6440.md) [sadeh](../../strongs/h/h7704.md), and will cause all the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md) to [shakan](../../strongs/h/h7931.md) upon thee, and I will [sāׂbaʿ](../../strongs/h/h7646.md) the [chay](../../strongs/h/h2416.md) of the ['erets](../../strongs/h/h776.md) with thee.

<a name="ezekiel_32_5"></a>Ezekiel 32:5

And I will [nathan](../../strongs/h/h5414.md) thy [basar](../../strongs/h/h1320.md) upon the [har](../../strongs/h/h2022.md), and [mālā'](../../strongs/h/h4390.md) the [gay'](../../strongs/h/h1516.md) with thy [rāmûṯ](../../strongs/h/h7419.md).

<a name="ezekiel_32_6"></a>Ezekiel 32:6

I will also [šāqâ](../../strongs/h/h8248.md) with thy [dam](../../strongs/h/h1818.md) the ['erets](../../strongs/h/h776.md) wherein thou [ṣāp̄â](../../strongs/h/h6824.md), even to the [har](../../strongs/h/h2022.md); and the ['āp̄îq](../../strongs/h/h650.md) shall be [mālā'](../../strongs/h/h4390.md) of thee.

<a name="ezekiel_32_7"></a>Ezekiel 32:7

And when I shall [kāḇâ](../../strongs/h/h3518.md) thee, I will [kāsâ](../../strongs/h/h3680.md) the [shamayim](../../strongs/h/h8064.md), and [qāḏar](../../strongs/h/h6937.md) the [kowkab](../../strongs/h/h3556.md) thereof [qāḏar](../../strongs/h/h6937.md); I will [kāsâ](../../strongs/h/h3680.md) the [šemeš](../../strongs/h/h8121.md) with a [ʿānān](../../strongs/h/h6051.md), and the [yareach](../../strongs/h/h3394.md) shall not ['owr](../../strongs/h/h215.md) her ['owr](../../strongs/h/h216.md).

<a name="ezekiel_32_8"></a>Ezekiel 32:8

All the [ma'owr](../../strongs/h/h3974.md) ['owr](../../strongs/h/h216.md) of [shamayim](../../strongs/h/h8064.md) will I make [qāḏar](../../strongs/h/h6937.md) over thee, and [nathan](../../strongs/h/h5414.md) [choshek](../../strongs/h/h2822.md) upon thy ['erets](../../strongs/h/h776.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_32_9"></a>Ezekiel 32:9

I will also [kāʿas](../../strongs/h/h3707.md) the [leb](../../strongs/h/h3820.md) of [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md), when I shall [bow'](../../strongs/h/h935.md) thy [šeḇar](../../strongs/h/h7667.md) among the [gowy](../../strongs/h/h1471.md), into the ['erets](../../strongs/h/h776.md) which thou hast not [yada'](../../strongs/h/h3045.md).

<a name="ezekiel_32_10"></a>Ezekiel 32:10

Yea, I will make [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) [šāmēm](../../strongs/h/h8074.md) at thee, and their [melek](../../strongs/h/h4428.md) shall be [śaʿar](../../strongs/h/h8178.md) [śāʿar](../../strongs/h/h8175.md) for thee, when I shall ['uwph](../../strongs/h/h5774.md) my [chereb](../../strongs/h/h2719.md) before [paniym](../../strongs/h/h6440.md); and they shall [ḥārēḏ](../../strongs/h/h2729.md) at every [reḡaʿ](../../strongs/h/h7281.md), every ['iysh](../../strongs/h/h376.md) for his own [nephesh](../../strongs/h/h5315.md), in the [yowm](../../strongs/h/h3117.md) of thy [mapeleṯ](../../strongs/h/h4658.md).

<a name="ezekiel_32_11"></a>Ezekiel 32:11

For thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); The [chereb](../../strongs/h/h2719.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) shall [bow'](../../strongs/h/h935.md) upon thee.

<a name="ezekiel_32_12"></a>Ezekiel 32:12

By the [chereb](../../strongs/h/h2719.md) of the [gibôr](../../strongs/h/h1368.md) will I cause thy [hāmôn](../../strongs/h/h1995.md) to [naphal](../../strongs/h/h5307.md), the [ʿārîṣ](../../strongs/h/h6184.md) of the [gowy](../../strongs/h/h1471.md), all of them: and they shall [shadad](../../strongs/h/h7703.md) the [gā'ôn](../../strongs/h/h1347.md) of [Mitsrayim](../../strongs/h/h4714.md), and all the [hāmôn](../../strongs/h/h1995.md) thereof shall be [šāmaḏ](../../strongs/h/h8045.md).

<a name="ezekiel_32_13"></a>Ezekiel 32:13

I will ['abad](../../strongs/h/h6.md) also all the [bĕhemah](../../strongs/h/h929.md) thereof from beside the [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md); neither shall the [regel](../../strongs/h/h7272.md) of ['āḏām](../../strongs/h/h120.md) [dālaḥ](../../strongs/h/h1804.md) them any more, nor the [parsâ](../../strongs/h/h6541.md) of [bĕhemah](../../strongs/h/h929.md) [dālaḥ](../../strongs/h/h1804.md) them.

<a name="ezekiel_32_14"></a>Ezekiel 32:14

Then will I make their [mayim](../../strongs/h/h4325.md) [šāqaʿ](../../strongs/h/h8257.md), and cause their [nāhār](../../strongs/h/h5104.md) to [yālaḵ](../../strongs/h/h3212.md) like [šemen](../../strongs/h/h8081.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_32_15"></a>Ezekiel 32:15

When I shall [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) [šᵊmāmâ](../../strongs/h/h8077.md), and the ['erets](../../strongs/h/h776.md) shall be [šāmēm](../../strongs/h/h8074.md) of that whereof it was [mᵊlō'](../../strongs/h/h4393.md), when I shall [nakah](../../strongs/h/h5221.md) all them that [yashab](../../strongs/h/h3427.md) therein, then shall they [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_32_16"></a>Ezekiel 32:16

This is the [qînâ](../../strongs/h/h7015.md) wherewith they shall [qônēn](../../strongs/h/h6969.md) her: the [bath](../../strongs/h/h1323.md) of the [gowy](../../strongs/h/h1471.md) shall [qônēn](../../strongs/h/h6969.md) her: they shall [qônēn](../../strongs/h/h6969.md) for her, even for [Mitsrayim](../../strongs/h/h4714.md), and for all her [hāmôn](../../strongs/h/h1995.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_32_17"></a>Ezekiel 32:17

It came to pass also in the twelfth [šānâ](../../strongs/h/h8141.md), in the fifteenth day of the [ḥōḏeš](../../strongs/h/h2320.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_32_18"></a>Ezekiel 32:18

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nāhâ](../../strongs/h/h5091.md) for the [hāmôn](../../strongs/h/h1995.md) of [Mitsrayim](../../strongs/h/h4714.md), and [yarad](../../strongs/h/h3381.md) them, even her, and the [bath](../../strongs/h/h1323.md) of the ['addiyr](../../strongs/h/h117.md) [gowy](../../strongs/h/h1471.md), unto the [taḥtî](../../strongs/h/h8482.md) of the ['erets](../../strongs/h/h776.md), with them that [yarad](../../strongs/h/h3381.md) into the [bowr](../../strongs/h/h953.md).

<a name="ezekiel_32_19"></a>Ezekiel 32:19

Whom dost thou pass in [nāʿēm](../../strongs/h/h5276.md)? [yarad](../../strongs/h/h3381.md), and be thou [shakab](../../strongs/h/h7901.md) with the [ʿārēl](../../strongs/h/h6189.md).

<a name="ezekiel_32_20"></a>Ezekiel 32:20

They shall [naphal](../../strongs/h/h5307.md) in the [tavek](../../strongs/h/h8432.md) of them that are [ḥālāl](../../strongs/h/h2491.md) by the [chereb](../../strongs/h/h2719.md): she is [nathan](../../strongs/h/h5414.md) to the [chereb](../../strongs/h/h2719.md): [mashak](../../strongs/h/h4900.md) her and all her [hāmôn](../../strongs/h/h1995.md).

<a name="ezekiel_32_21"></a>Ezekiel 32:21

The ['el](../../strongs/h/h410.md) among the [gibôr](../../strongs/h/h1368.md) shall [dabar](../../strongs/h/h1696.md) to him out of the [tavek](../../strongs/h/h8432.md) of [shĕ'owl](../../strongs/h/h7585.md) with them that [ʿāzar](../../strongs/h/h5826.md) him: they are [yarad](../../strongs/h/h3381.md), they [shakab](../../strongs/h/h7901.md) [ʿārēl](../../strongs/h/h6189.md), [ḥālāl](../../strongs/h/h2491.md) by the [chereb](../../strongs/h/h2719.md).

<a name="ezekiel_32_22"></a>Ezekiel 32:22

['Aššûr](../../strongs/h/h804.md) is there and all her [qāhēl](../../strongs/h/h6951.md): his [qeber](../../strongs/h/h6913.md) are [cabiyb](../../strongs/h/h5439.md) him: all of them [ḥālāl](../../strongs/h/h2491.md), [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md):

<a name="ezekiel_32_23"></a>Ezekiel 32:23

Whose [qeber](../../strongs/h/h6913.md) are [nathan](../../strongs/h/h5414.md) in the [yᵊrēḵâ](../../strongs/h/h3411.md) of the [bowr](../../strongs/h/h953.md), and her [qāhēl](../../strongs/h/h6951.md) is [cabiyb](../../strongs/h/h5439.md) her [qᵊḇûrâ](../../strongs/h/h6900.md): all of them [ḥālāl](../../strongs/h/h2491.md), [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md), which [nathan](../../strongs/h/h5414.md) [ḥitîṯ](../../strongs/h/h2851.md) in the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md).

<a name="ezekiel_32_24"></a>Ezekiel 32:24

There is [ʿÊlām](../../strongs/h/h5867.md) and all her [hāmôn](../../strongs/h/h1995.md) [cabiyb](../../strongs/h/h5439.md) her [qᵊḇûrâ](../../strongs/h/h6900.md), all of them [ḥālāl](../../strongs/h/h2491.md), [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md), which are [yarad](../../strongs/h/h3381.md) [ʿārēl](../../strongs/h/h6189.md) into the nether [taḥtî](../../strongs/h/h8482.md) of the ['erets](../../strongs/h/h776.md), which [nathan](../../strongs/h/h5414.md) their [ḥitîṯ](../../strongs/h/h2851.md) in the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md); yet have they [nasa'](../../strongs/h/h5375.md) their [kĕlimmah](../../strongs/h/h3639.md) with them that [yarad](../../strongs/h/h3381.md) to the [bowr](../../strongs/h/h953.md).

<a name="ezekiel_32_25"></a>Ezekiel 32:25

They have [nathan](../../strongs/h/h5414.md) her a [miškāḇ](../../strongs/h/h4904.md) in the [tavek](../../strongs/h/h8432.md) of the [ḥālāl](../../strongs/h/h2491.md) with all her [hāmôn](../../strongs/h/h1995.md): her [qeber](../../strongs/h/h6913.md) are [cabiyb](../../strongs/h/h5439.md) him: all of them [ʿārēl](../../strongs/h/h6189.md), [ḥālāl](../../strongs/h/h2491.md) by the [chereb](../../strongs/h/h2719.md): though their [ḥitîṯ](../../strongs/h/h2851.md) was [nathan](../../strongs/h/h5414.md) in the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md), yet have they [nasa'](../../strongs/h/h5375.md) their [kĕlimmah](../../strongs/h/h3639.md) with them that [yarad](../../strongs/h/h3381.md) to the [bowr](../../strongs/h/h953.md): he is [nathan](../../strongs/h/h5414.md) in the [tavek](../../strongs/h/h8432.md) of them that be [ḥālāl](../../strongs/h/h2491.md).

<a name="ezekiel_32_26"></a>Ezekiel 32:26

There is [Mešeḵ](../../strongs/h/h4902.md), [Tuḇal](../../strongs/h/h8422.md), and all her [hāmôn](../../strongs/h/h1995.md): her [qeber](../../strongs/h/h6913.md) are [cabiyb](../../strongs/h/h5439.md) him: all of them [ʿārēl](../../strongs/h/h6189.md), [ḥālal](../../strongs/h/h2490.md) by the [chereb](../../strongs/h/h2719.md), though they [nathan](../../strongs/h/h5414.md) their [ḥitîṯ](../../strongs/h/h2851.md) in the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md).

<a name="ezekiel_32_27"></a>Ezekiel 32:27

And they shall not [shakab](../../strongs/h/h7901.md) with the [gibôr](../../strongs/h/h1368.md) that are [naphal](../../strongs/h/h5307.md) of the [ʿārēl](../../strongs/h/h6189.md), which are [yarad](../../strongs/h/h3381.md) to [shĕ'owl](../../strongs/h/h7585.md) with their [kĕliy](../../strongs/h/h3627.md) of [milḥāmâ](../../strongs/h/h4421.md): and they have [nathan](../../strongs/h/h5414.md) their [chereb](../../strongs/h/h2719.md) under their [ro'sh](../../strongs/h/h7218.md), but their ['avon](../../strongs/h/h5771.md) shall be upon their ['etsem](../../strongs/h/h6106.md), though they were the [ḥitîṯ](../../strongs/h/h2851.md) of the [gibôr](../../strongs/h/h1368.md) in the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md).

<a name="ezekiel_32_28"></a>Ezekiel 32:28

Yea, thou shalt be [shabar](../../strongs/h/h7665.md) in the [tavek](../../strongs/h/h8432.md) of the [ʿārēl](../../strongs/h/h6189.md), and shalt [shakab](../../strongs/h/h7901.md) with them that are [ḥālāl](../../strongs/h/h2491.md) with the [chereb](../../strongs/h/h2719.md).

<a name="ezekiel_32_29"></a>Ezekiel 32:29

There is ['Ĕḏōm](../../strongs/h/h123.md), her [melek](../../strongs/h/h4428.md), and all her [nāśî'](../../strongs/h/h5387.md), which with their [gᵊḇûrâ](../../strongs/h/h1369.md) are [nathan](../../strongs/h/h5414.md) by them that were [ḥālāl](../../strongs/h/h2491.md) by the [chereb](../../strongs/h/h2719.md): they shall [shakab](../../strongs/h/h7901.md) with the [ʿārēl](../../strongs/h/h6189.md), and with them that [yarad](../../strongs/h/h3381.md) to the [bowr](../../strongs/h/h953.md).

<a name="ezekiel_32_30"></a>Ezekiel 32:30

There be the [nāsîḵ](../../strongs/h/h5257.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md), all of them, and all the [Ṣîḏōnî](../../strongs/h/h6722.md), which are [yarad](../../strongs/h/h3381.md) with the [ḥālāl](../../strongs/h/h2491.md); with their [ḥitîṯ](../../strongs/h/h2851.md) they are [buwsh](../../strongs/h/h954.md) of their [gᵊḇûrâ](../../strongs/h/h1369.md); and they [shakab](../../strongs/h/h7901.md) [ʿārēl](../../strongs/h/h6189.md) with them that be [ḥālāl](../../strongs/h/h2491.md) by the [chereb](../../strongs/h/h2719.md), and [nasa'](../../strongs/h/h5375.md) their [kĕlimmah](../../strongs/h/h3639.md) with them that [yarad](../../strongs/h/h3381.md) to the [bowr](../../strongs/h/h953.md).

<a name="ezekiel_32_31"></a>Ezekiel 32:31

[Parʿô](../../strongs/h/h6547.md) shall [ra'ah](../../strongs/h/h7200.md) them, and shall be [nacham](../../strongs/h/h5162.md) over all his [hāmôn](../../strongs/h/h1995.md), even [Parʿô](../../strongs/h/h6547.md) and all his [ḥayil](../../strongs/h/h2428.md) [ḥālāl](../../strongs/h/h2491.md) by the [chereb](../../strongs/h/h2719.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_32_32"></a>Ezekiel 32:32

For I have [nathan](../../strongs/h/h5414.md) my [ḥitîṯ](../../strongs/h/h2851.md) in the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md): and he shall be [shakab](../../strongs/h/h7901.md) in the [tavek](../../strongs/h/h8432.md) of the [ʿārēl](../../strongs/h/h6189.md) with them that are [ḥālāl](../../strongs/h/h2491.md) with the [chereb](../../strongs/h/h2719.md), even [Parʿô](../../strongs/h/h6547.md) and all his [hāmôn](../../strongs/h/h1995.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 31](ezekiel_31.md) - [Ezekiel 33](ezekiel_33.md)