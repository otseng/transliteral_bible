# [Ezekiel 25](https://www.blueletterbible.org/kjv/ezekiel/25)

<a name="ezekiel_25_1"></a>Ezekiel 25:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came again unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_25_2"></a>Ezekiel 25:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [śûm](../../strongs/h/h7760.md) thy [paniym](../../strongs/h/h6440.md) against the [ben](../../strongs/h/h1121.md) [ʿAmmôn](../../strongs/h/h5983.md), and [nāḇā'](../../strongs/h/h5012.md) against them;

<a name="ezekiel_25_3"></a>Ezekiel 25:3

And ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) [ʿAmmôn](../../strongs/h/h5983.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because thou ['āmar](../../strongs/h/h559.md), [he'āḥ](../../strongs/h/h1889.md), against my [miqdash](../../strongs/h/h4720.md), when it was [ḥālal](../../strongs/h/h2490.md); and against the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md), when it was [šāmēm](../../strongs/h/h8074.md); and against the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md), when they [halak](../../strongs/h/h1980.md) into [gôlâ](../../strongs/h/h1473.md);

<a name="ezekiel_25_4"></a>Ezekiel 25:4

Behold, therefore I will [nathan](../../strongs/h/h5414.md) thee to the [ben](../../strongs/h/h1121.md) of the [qeḏem](../../strongs/h/h6924.md) for a [môrāšâ](../../strongs/h/h4181.md), and they shall [yashab](../../strongs/h/h3427.md) their [ṭîrâ](../../strongs/h/h2918.md) in thee, and [nathan](../../strongs/h/h5414.md) their [miškān](../../strongs/h/h4908.md) in thee: they shall ['akal](../../strongs/h/h398.md) thy [pĕriy](../../strongs/h/h6529.md), and they shall [šāṯâ](../../strongs/h/h8354.md) thy [chalab](../../strongs/h/h2461.md).

<a name="ezekiel_25_5"></a>Ezekiel 25:5

And I will [nathan](../../strongs/h/h5414.md) [Rabâ](../../strongs/h/h7237.md) a [nāvê](../../strongs/h/h5116.md) for [gāmāl](../../strongs/h/h1581.md), and the [ben](../../strongs/h/h1121.md) [ʿAmmôn](../../strongs/h/h5983.md) a [marbēṣ](../../strongs/h/h4769.md) for [tso'n](../../strongs/h/h6629.md): and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_25_6"></a>Ezekiel 25:6

For thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because thou hast [māḥā'](../../strongs/h/h4222.md) thine [yad](../../strongs/h/h3027.md), and [rāqaʿ](../../strongs/h/h7554.md) with the [regel](../../strongs/h/h7272.md), and [samach](../../strongs/h/h8055.md) in [nephesh](../../strongs/h/h5315.md) with all thy [šᵊ'āṭ](../../strongs/h/h7589.md) against the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md);

<a name="ezekiel_25_7"></a>Ezekiel 25:7

Behold, therefore I will [natah](../../strongs/h/h5186.md) mine [yad](../../strongs/h/h3027.md) upon thee, and will [nathan](../../strongs/h/h5414.md) thee for a [baz](../../strongs/h/h957.md) [baḡ](../../strongs/h/h897.md) to the [gowy](../../strongs/h/h1471.md); and I will [karath](../../strongs/h/h3772.md) thee from the ['am](../../strongs/h/h5971.md), and I will cause thee to ['abad](../../strongs/h/h6.md) out of the ['erets](../../strongs/h/h776.md): I will [šāmaḏ](../../strongs/h/h8045.md) thee; and thou shalt [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_25_8"></a>Ezekiel 25:8

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because that [Mô'āḇ](../../strongs/h/h4124.md) and [Śēʿîr](../../strongs/h/h8165.md) do ['āmar](../../strongs/h/h559.md), Behold, the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) is like unto all the [gowy](../../strongs/h/h1471.md);

<a name="ezekiel_25_9"></a>Ezekiel 25:9

Therefore, behold, I will [pāṯaḥ](../../strongs/h/h6605.md) the [kāṯēp̄](../../strongs/h/h3802.md) of [Mô'āḇ](../../strongs/h/h4124.md) from the [ʿîr](../../strongs/h/h5892.md), from his [ʿîr](../../strongs/h/h5892.md) which are on his [qāṣê](../../strongs/h/h7097.md), the [ṣᵊḇî](../../strongs/h/h6643.md) of the ['erets](../../strongs/h/h776.md), [Bêṯ Hayšîmôṯ](../../strongs/h/h1020.md), [BaʿAl MᵊʿÔn](../../strongs/h/h1186.md), and [Qiryāṯayim](../../strongs/h/h7156.md),

<a name="ezekiel_25_10"></a>Ezekiel 25:10

Unto the [ben](../../strongs/h/h1121.md) of the [qeḏem](../../strongs/h/h6924.md) with the [ben](../../strongs/h/h1121.md) [ʿAmmôn](../../strongs/h/h5983.md), and will [nathan](../../strongs/h/h5414.md) them in [môrāšâ](../../strongs/h/h4181.md), that the [ben](../../strongs/h/h1121.md) [ʿAmmôn](../../strongs/h/h5983.md) may not be [zakar](../../strongs/h/h2142.md) among the [gowy](../../strongs/h/h1471.md).

<a name="ezekiel_25_11"></a>Ezekiel 25:11

And I will ['asah](../../strongs/h/h6213.md) [šep̄eṭ](../../strongs/h/h8201.md) upon [Mô'āḇ](../../strongs/h/h4124.md); and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_25_12"></a>Ezekiel 25:12

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because that ['Ĕḏōm](../../strongs/h/h123.md) hath ['asah](../../strongs/h/h6213.md) against the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) by [naqam](../../strongs/h/h5358.md) [nāqām](../../strongs/h/h5359.md), and hath ['asham](../../strongs/h/h816.md) ['asham](../../strongs/h/h816.md), and [naqam](../../strongs/h/h5358.md) himself upon them;

<a name="ezekiel_25_13"></a>Ezekiel 25:13

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); I will also [natah](../../strongs/h/h5186.md) mine [yad](../../strongs/h/h3027.md) upon ['Ĕḏōm](../../strongs/h/h123.md), and will [karath](../../strongs/h/h3772.md) ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md) from it; and I will [nathan](../../strongs/h/h5414.md) it [chorbah](../../strongs/h/h2723.md) from [Têmān](../../strongs/h/h8487.md); and they of [Dᵊḏān](../../strongs/h/h1719.md) shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md).

<a name="ezekiel_25_14"></a>Ezekiel 25:14

And I will [nathan](../../strongs/h/h5414.md) my [nᵊqāmâ](../../strongs/h/h5360.md) upon ['Ĕḏōm](../../strongs/h/h123.md) by the [yad](../../strongs/h/h3027.md) of my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md): and they shall ['asah](../../strongs/h/h6213.md) in ['Ĕḏōm](../../strongs/h/h123.md) according to mine ['aph](../../strongs/h/h639.md) and according to my [chemah](../../strongs/h/h2534.md); and they shall [yada'](../../strongs/h/h3045.md) my [nᵊqāmâ](../../strongs/h/h5360.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_25_15"></a>Ezekiel 25:15

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because the [Pᵊlištî](../../strongs/h/h6430.md) have ['asah](../../strongs/h/h6213.md) by [nᵊqāmâ](../../strongs/h/h5360.md), and have [naqam](../../strongs/h/h5358.md) [nāqām](../../strongs/h/h5359.md) with a [šᵊ'āṭ](../../strongs/h/h7589.md) [nephesh](../../strongs/h/h5315.md), to [mašḥîṯ](../../strongs/h/h4889.md) it for the ['owlam](../../strongs/h/h5769.md) ['eybah](../../strongs/h/h342.md);

<a name="ezekiel_25_16"></a>Ezekiel 25:16

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I will [natah](../../strongs/h/h5186.md) mine [yad](../../strongs/h/h3027.md) upon the [Pᵊlištî](../../strongs/h/h6430.md), and I will [karath](../../strongs/h/h3772.md) the [kᵊrēṯî](../../strongs/h/h3774.md), and ['abad](../../strongs/h/h6.md) the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the [yam](../../strongs/h/h3220.md) [ḥôp̄](../../strongs/h/h2348.md).

<a name="ezekiel_25_17"></a>Ezekiel 25:17

And I will ['asah](../../strongs/h/h6213.md) [gadowl](../../strongs/h/h1419.md) [nᵊqāmâ](../../strongs/h/h5360.md) upon them with [chemah](../../strongs/h/h2534.md) [tôḵēḥâ](../../strongs/h/h8433.md); and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when I shall [nathan](../../strongs/h/h5414.md) my [nᵊqāmâ](../../strongs/h/h5360.md) upon them.

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 24](ezekiel_24.md) - [Ezekiel 26](ezekiel_26.md)