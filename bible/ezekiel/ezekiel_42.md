# [Ezekiel 42](https://www.blueletterbible.org/kjv/ezekiel/42)

<a name="ezekiel_42_1"></a>Ezekiel 42:1

Then he [yāṣā'](../../strongs/h/h3318.md) me into the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md), the [derek](../../strongs/h/h1870.md) the [ṣāp̄ôn](../../strongs/h/h6828.md): and he [bow'](../../strongs/h/h935.md) me into the [liškâ](../../strongs/h/h3957.md) that was over against the [gizrâ](../../strongs/h/h1508.md), and which was before the [binyān](../../strongs/h/h1146.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md).

<a name="ezekiel_42_2"></a>Ezekiel 42:2

[paniym](../../strongs/h/h6440.md) the ['ōreḵ](../../strongs/h/h753.md) of an hundred ['ammâ](../../strongs/h/h520.md) was the [ṣāp̄ôn](../../strongs/h/h6828.md) [peṯaḥ](../../strongs/h/h6607.md), and the [rōḥaḇ](../../strongs/h/h7341.md) was fifty ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_42_3"></a>Ezekiel 42:3

Over against the twenty which were for the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md), and over against the [ritspah](../../strongs/h/h7531.md) which was for the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md), was ['atîq](../../strongs/h/h862.md) [paniym](../../strongs/h/h6440.md) ['atîq](../../strongs/h/h862.md) in three.

<a name="ezekiel_42_4"></a>Ezekiel 42:4

And [paniym](../../strongs/h/h6440.md) the [liškâ](../../strongs/h/h3957.md) was a [mahălāḵ](../../strongs/h/h4109.md) of ten ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md) [pᵊnîmî](../../strongs/h/h6442.md), a [derek](../../strongs/h/h1870.md) of one ['ammâ](../../strongs/h/h520.md); and their [peṯaḥ](../../strongs/h/h6607.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md).

<a name="ezekiel_42_5"></a>Ezekiel 42:5

Now the ['elyown](../../strongs/h/h5945.md) [liškâ](../../strongs/h/h3957.md) were [qāṣar](../../strongs/h/h7114.md): for the ['atîq](../../strongs/h/h862.md) were ['akal](../../strongs/h/h398.md) than these, than the [taḥtôn](../../strongs/h/h8481.md), and than the [tîḵôn](../../strongs/h/h8484.md) of the [binyān](../../strongs/h/h1146.md).

<a name="ezekiel_42_6"></a>Ezekiel 42:6

For they were in [šālaš](../../strongs/h/h8027.md) stories, but had not [ʿammûḏ](../../strongs/h/h5982.md) as the [ʿammûḏ](../../strongs/h/h5982.md) of the [ḥāṣēr](../../strongs/h/h2691.md): therefore was ['āṣal](../../strongs/h/h680.md) more than the [taḥtôn](../../strongs/h/h8481.md) and the [tîḵôn](../../strongs/h/h8484.md) from the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_42_7"></a>Ezekiel 42:7

And the [gāḏēr](../../strongs/h/h1447.md) that was [ḥûṣ](../../strongs/h/h2351.md) over [ʿummâ](../../strongs/h/h5980.md) the [liškâ](../../strongs/h/h3957.md), [derek](../../strongs/h/h1870.md) the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md) on the [paniym](../../strongs/h/h6440.md) of the [liškâ](../../strongs/h/h3957.md), the ['ōreḵ](../../strongs/h/h753.md) thereof was fifty ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_42_8"></a>Ezekiel 42:8

For the ['ōreḵ](../../strongs/h/h753.md) of the [liškâ](../../strongs/h/h3957.md) that were in the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md) was fifty ['ammâ](../../strongs/h/h520.md): and, lo, [paniym](../../strongs/h/h6440.md) the [heykal](../../strongs/h/h1964.md) were an hundred ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_42_9"></a>Ezekiel 42:9

And from under these [liškâ](../../strongs/h/h3957.md) was the [māḇô'](../../strongs/h/h3996.md) [bow'](../../strongs/h/h935.md) on the [qāḏîm](../../strongs/h/h6921.md), as one [bow'](../../strongs/h/h935.md) into them from the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md).

<a name="ezekiel_42_10"></a>Ezekiel 42:10

The [liškâ](../../strongs/h/h3957.md) were in the [rōḥaḇ](../../strongs/h/h7341.md) of the [geḏer](../../strongs/h/h1444.md) of the [ḥāṣēr](../../strongs/h/h2691.md) [derek](../../strongs/h/h1870.md) the [qāḏîm](../../strongs/h/h6921.md), over [paniym](../../strongs/h/h6440.md) the [gizrâ](../../strongs/h/h1508.md), and over [paniym](../../strongs/h/h6440.md) the [binyān](../../strongs/h/h1146.md).

<a name="ezekiel_42_11"></a>Ezekiel 42:11

And the [derek](../../strongs/h/h1870.md) [paniym](../../strongs/h/h6440.md) them was like the [mar'ê](../../strongs/h/h4758.md) of the [liškâ](../../strongs/h/h3957.md) which were [derek](../../strongs/h/h1870.md) the [ṣāp̄ôn](../../strongs/h/h6828.md), as long ['ōreḵ](../../strongs/h/h753.md) they, and as broad as [rōḥaḇ](../../strongs/h/h7341.md) they: and all their goings [môṣā'](../../strongs/h/h4161.md) were both according to their [mishpat](../../strongs/h/h4941.md), and according to their [peṯaḥ](../../strongs/h/h6607.md).

<a name="ezekiel_42_12"></a>Ezekiel 42:12

And according to the [peṯaḥ](../../strongs/h/h6607.md) of the [liškâ](../../strongs/h/h3957.md) that were [derek](../../strongs/h/h1870.md) the [dārôm](../../strongs/h/h1864.md) was a [peṯaḥ](../../strongs/h/h6607.md) in the [ro'sh](../../strongs/h/h7218.md) of the [derek](../../strongs/h/h1870.md), even the [derek](../../strongs/h/h1870.md) [hāḡîn](../../strongs/h/h1903.md) [paniym](../../strongs/h/h6440.md) the [gᵊḏērâ](../../strongs/h/h1448.md) [derek](../../strongs/h/h1870.md) the [qāḏîm](../../strongs/h/h6921.md), as one [bow'](../../strongs/h/h935.md) into them.

<a name="ezekiel_42_13"></a>Ezekiel 42:13

Then ['āmar](../../strongs/h/h559.md) he unto me, The [ṣāp̄ôn](../../strongs/h/h6828.md) [liškâ](../../strongs/h/h3957.md) and the [dārôm](../../strongs/h/h1864.md) [liškâ](../../strongs/h/h3957.md), which are [paniym](../../strongs/h/h6440.md) the [gizrâ](../../strongs/h/h1508.md), they be [qodesh](../../strongs/h/h6944.md) [liškâ](../../strongs/h/h3957.md), where the [kōhēn](../../strongs/h/h3548.md) that [qarowb](../../strongs/h/h7138.md) unto [Yĕhovah](../../strongs/h/h3068.md) shall ['akal](../../strongs/h/h398.md) the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md): there shall they [yānaḥ](../../strongs/h/h3240.md) the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md), and the [minchah](../../strongs/h/h4503.md), and the [chatta'ath](../../strongs/h/h2403.md), and the ['āšām](../../strongs/h/h817.md); for the [maqowm](../../strongs/h/h4725.md) is [qadowsh](../../strongs/h/h6918.md).

<a name="ezekiel_42_14"></a>Ezekiel 42:14

When the [kōhēn](../../strongs/h/h3548.md) [bow'](../../strongs/h/h935.md) therein, then shall they not [yāṣā'](../../strongs/h/h3318.md) of the [qodesh](../../strongs/h/h6944.md) place into the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md), but there they shall [yānaḥ](../../strongs/h/h3240.md) their [beḡeḏ](../../strongs/h/h899.md) wherein they [sharath](../../strongs/h/h8334.md); for they are [qodesh](../../strongs/h/h6944.md); and shall [labash](../../strongs/h/h3847.md) [labash](../../strongs/h/h3847.md) on ['aḥēr](../../strongs/h/h312.md) [beḡeḏ](../../strongs/h/h899.md), and shall [qāraḇ](../../strongs/h/h7126.md) to those things which are for the ['am](../../strongs/h/h5971.md).

<a name="ezekiel_42_15"></a>Ezekiel 42:15

Now when he had made a [kalah](../../strongs/h/h3615.md) of [midâ](../../strongs/h/h4060.md) the [pᵊnîmî](../../strongs/h/h6442.md) [bayith](../../strongs/h/h1004.md), he brought me [yāṣā'](../../strongs/h/h3318.md) [derek](../../strongs/h/h1870.md) the [sha'ar](../../strongs/h/h8179.md) whose [paniym](../../strongs/h/h6440.md) is [derek](../../strongs/h/h1870.md) the [qāḏîm](../../strongs/h/h6921.md), and [māḏaḏ](../../strongs/h/h4058.md) it [cabiyb](../../strongs/h/h5439.md).

<a name="ezekiel_42_16"></a>Ezekiel 42:16

He [māḏaḏ](../../strongs/h/h4058.md) the [qāḏîm](../../strongs/h/h6921.md) [ruwach](../../strongs/h/h7307.md) with the [midâ](../../strongs/h/h4060.md) [qānê](../../strongs/h/h7070.md), five ['ammâ](../../strongs/h/h520.md) hundred [qānê](../../strongs/h/h7070.md), with the [midâ](../../strongs/h/h4060.md) [qānê](../../strongs/h/h7070.md) [cabiyb](../../strongs/h/h5439.md).

<a name="ezekiel_42_17"></a>Ezekiel 42:17

He [māḏaḏ](../../strongs/h/h4058.md) the [ṣāp̄ôn](../../strongs/h/h6828.md) [ruwach](../../strongs/h/h7307.md), five hundred [qānê](../../strongs/h/h7070.md), with the [midâ](../../strongs/h/h4060.md) [qānê](../../strongs/h/h7070.md) [cabiyb](../../strongs/h/h5439.md).

<a name="ezekiel_42_18"></a>Ezekiel 42:18

He [māḏaḏ](../../strongs/h/h4058.md) the [dārôm](../../strongs/h/h1864.md) [ruwach](../../strongs/h/h7307.md), five hundred [qānê](../../strongs/h/h7070.md), with the [midâ](../../strongs/h/h4060.md) [qānê](../../strongs/h/h7070.md).

<a name="ezekiel_42_19"></a>Ezekiel 42:19

He [cabab](../../strongs/h/h5437.md) to the [yam](../../strongs/h/h3220.md) [ruwach](../../strongs/h/h7307.md), and [māḏaḏ](../../strongs/h/h4058.md) five hundred [qānê](../../strongs/h/h7070.md) with the [midâ](../../strongs/h/h4060.md) [qānê](../../strongs/h/h7070.md).

<a name="ezekiel_42_20"></a>Ezekiel 42:20

He [māḏaḏ](../../strongs/h/h4058.md) it by the four [ruwach](../../strongs/h/h7307.md): it had a [ḥômâ](../../strongs/h/h2346.md) [cabiyb](../../strongs/h/h5439.md), five hundred reeds ['ōreḵ](../../strongs/h/h753.md), and five hundred [rōḥaḇ](../../strongs/h/h7341.md), to make a [bāḏal](../../strongs/h/h914.md) between the [qodesh](../../strongs/h/h6944.md) and the profane [ḥōl](../../strongs/h/h2455.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 41](ezekiel_41.md) - [Ezekiel 43](ezekiel_43.md)