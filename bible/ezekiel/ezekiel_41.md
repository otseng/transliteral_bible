# [Ezekiel 41](https://www.blueletterbible.org/kjv/ezekiel/41)

<a name="ezekiel_41_1"></a>Ezekiel 41:1

Afterward he [bow'](../../strongs/h/h935.md) me to the [heykal](../../strongs/h/h1964.md), and [māḏaḏ](../../strongs/h/h4058.md) the ['ayil](../../strongs/h/h352.md), six ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md) on the one side, and six ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md) on the other side, which was the [rōḥaḇ](../../strongs/h/h7341.md) of the ['ohel](../../strongs/h/h168.md).

<a name="ezekiel_41_2"></a>Ezekiel 41:2

And the [rōḥaḇ](../../strongs/h/h7341.md) of the [peṯaḥ](../../strongs/h/h6607.md) was ten ['ammâ](../../strongs/h/h520.md); and the [kāṯēp̄](../../strongs/h/h3802.md) of the [peṯaḥ](../../strongs/h/h6607.md) were five ['ammâ](../../strongs/h/h520.md) on the one side, and five ['ammâ](../../strongs/h/h520.md) on the other side: and he [māḏaḏ](../../strongs/h/h4058.md) the ['ōreḵ](../../strongs/h/h753.md) thereof, forty ['ammâ](../../strongs/h/h520.md): and the [rōḥaḇ](../../strongs/h/h7341.md), twenty ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_41_3"></a>Ezekiel 41:3

Then [bow'](../../strongs/h/h935.md) he [pᵊnîmâ](../../strongs/h/h6441.md), and [māḏaḏ](../../strongs/h/h4058.md) the ['ayil](../../strongs/h/h352.md) of the [peṯaḥ](../../strongs/h/h6607.md), two ['ammâ](../../strongs/h/h520.md); and the [peṯaḥ](../../strongs/h/h6607.md), six ['ammâ](../../strongs/h/h520.md); and the [rōḥaḇ](../../strongs/h/h7341.md) of the [peṯaḥ](../../strongs/h/h6607.md), seven ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_41_4"></a>Ezekiel 41:4

So he [māḏaḏ](../../strongs/h/h4058.md) the ['ōreḵ](../../strongs/h/h753.md) thereof, twenty ['ammâ](../../strongs/h/h520.md); and the [rōḥaḇ](../../strongs/h/h7341.md), twenty ['ammâ](../../strongs/h/h520.md), [paniym](../../strongs/h/h6440.md) the [heykal](../../strongs/h/h1964.md): and he ['āmar](../../strongs/h/h559.md) unto me, This is the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md).

<a name="ezekiel_41_5"></a>Ezekiel 41:5

After he [māḏaḏ](../../strongs/h/h4058.md) the [qîr](../../strongs/h/h7023.md) of the [bayith](../../strongs/h/h1004.md), six ['ammâ](../../strongs/h/h520.md); and the [rōḥaḇ](../../strongs/h/h7341.md) of every [tsela'](../../strongs/h/h6763.md), four ['ammâ](../../strongs/h/h520.md), [cabiyb](../../strongs/h/h5439.md) the [bayith](../../strongs/h/h1004.md) [cabiyb](../../strongs/h/h5439.md).

<a name="ezekiel_41_6"></a>Ezekiel 41:6

And the [tsela'](../../strongs/h/h6763.md) [tsela'](../../strongs/h/h6763.md) were three, one over [tsela'](../../strongs/h/h6763.md), and thirty in [pa'am](../../strongs/h/h6471.md); and they [bow'](../../strongs/h/h935.md) into the [qîr](../../strongs/h/h7023.md) which was of the [bayith](../../strongs/h/h1004.md) for the [tsela'](../../strongs/h/h6763.md) [cabiyb](../../strongs/h/h5439.md), that they might have ['āḥaz](../../strongs/h/h270.md), but they had not ['āḥaz](../../strongs/h/h270.md) in the [qîr](../../strongs/h/h7023.md) of the [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_41_7"></a>Ezekiel 41:7

And there was a [rāḥaḇ](../../strongs/h/h7337.md), and a [cabab](../../strongs/h/h5437.md) still [maʿal](../../strongs/h/h4605.md) to the [tsela'](../../strongs/h/h6763.md): for the [mûsaḇ](../../strongs/h/h4141.md) of the [bayith](../../strongs/h/h1004.md) went still [maʿal](../../strongs/h/h4605.md) [cabiyb](../../strongs/h/h5439.md) the [bayith](../../strongs/h/h1004.md): therefore the [rōḥaḇ](../../strongs/h/h7341.md) of the [bayith](../../strongs/h/h1004.md) was still [maʿal](../../strongs/h/h4605.md), and so [ʿālâ](../../strongs/h/h5927.md) from the [taḥtôn](../../strongs/h/h8481.md) to the ['elyown](../../strongs/h/h5945.md) by the [tîḵôn](../../strongs/h/h8484.md).

<a name="ezekiel_41_8"></a>Ezekiel 41:8

I [ra'ah](../../strongs/h/h7200.md) also the [gobahh](../../strongs/h/h1363.md) of the [bayith](../../strongs/h/h1004.md) [cabiyb](../../strongs/h/h5439.md): the [mysḏh](../../strongs/h/h4328.md) of the [tsela'](../../strongs/h/h6763.md) were a [mᵊlō'](../../strongs/h/h4393.md) [qānê](../../strongs/h/h7070.md) of six ['aṣṣîl](../../strongs/h/h679.md) ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_41_9"></a>Ezekiel 41:9

The [rōḥaḇ](../../strongs/h/h7341.md) of the [qîr](../../strongs/h/h7023.md), which was for the [tsela'](../../strongs/h/h6763.md) [ḥûṣ](../../strongs/h/h2351.md), was five ['ammâ](../../strongs/h/h520.md): and that which was [yānaḥ](../../strongs/h/h3240.md) was the [bayith](../../strongs/h/h1004.md) of the [tsela'](../../strongs/h/h6763.md) that were [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_41_10"></a>Ezekiel 41:10

And between the [liškâ](../../strongs/h/h3957.md) was the [rōḥaḇ](../../strongs/h/h7341.md) of twenty ['ammâ](../../strongs/h/h520.md) [cabiyb](../../strongs/h/h5439.md) the [bayith](../../strongs/h/h1004.md) [cabiyb](../../strongs/h/h5439.md).

<a name="ezekiel_41_11"></a>Ezekiel 41:11

And the [peṯaḥ](../../strongs/h/h6607.md) of the [tsela'](../../strongs/h/h6763.md) were toward the place that was [yānaḥ](../../strongs/h/h3240.md), one [peṯaḥ](../../strongs/h/h6607.md) [derek](../../strongs/h/h1870.md) the [ṣāp̄ôn](../../strongs/h/h6828.md), and another [peṯaḥ](../../strongs/h/h6607.md) toward the [dārôm](../../strongs/h/h1864.md): and the [rōḥaḇ](../../strongs/h/h7341.md) of the [maqowm](../../strongs/h/h4725.md) that was [yānaḥ](../../strongs/h/h3240.md) was five ['ammâ](../../strongs/h/h520.md) [cabiyb](../../strongs/h/h5439.md).

<a name="ezekiel_41_12"></a>Ezekiel 41:12

Now the [binyān](../../strongs/h/h1146.md) that was [paniym](../../strongs/h/h6440.md) the separate [gizrâ](../../strongs/h/h1508.md) at the [pē'â](../../strongs/h/h6285.md) [derek](../../strongs/h/h1870.md) the [yam](../../strongs/h/h3220.md) was seventy ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md); and the [qîr](../../strongs/h/h7023.md) of the [binyān](../../strongs/h/h1146.md) was five ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md) [cabiyb](../../strongs/h/h5439.md), and the ['ōreḵ](../../strongs/h/h753.md) thereof ninety ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_41_13"></a>Ezekiel 41:13

So he [māḏaḏ](../../strongs/h/h4058.md) the [bayith](../../strongs/h/h1004.md), an hundred ['ammâ](../../strongs/h/h520.md) ['ōreḵ](../../strongs/h/h753.md); and the separate [gizrâ](../../strongs/h/h1508.md), and the [binyâ](../../strongs/h/h1140.md), with the [qîr](../../strongs/h/h7023.md) thereof, an hundred ['ammâ](../../strongs/h/h520.md) ['ōreḵ](../../strongs/h/h753.md);

<a name="ezekiel_41_14"></a>Ezekiel 41:14

Also the [rōḥaḇ](../../strongs/h/h7341.md) of the [paniym](../../strongs/h/h6440.md) of the [bayith](../../strongs/h/h1004.md), and of the separate [gizrâ](../../strongs/h/h1508.md) toward the [qāḏîm](../../strongs/h/h6921.md), an hundred ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_41_15"></a>Ezekiel 41:15

And he [māḏaḏ](../../strongs/h/h4058.md) the ['ōreḵ](../../strongs/h/h753.md) of the [binyān](../../strongs/h/h1146.md) over [paniym](../../strongs/h/h6440.md) the separate [gizrâ](../../strongs/h/h1508.md) which was ['aḥar](../../strongs/h/h310.md) it, and the ['atîq](../../strongs/h/h862.md) thereof on the one side and on the other side, an hundred ['ammâ](../../strongs/h/h520.md), with the [pᵊnîmî](../../strongs/h/h6442.md) [heykal](../../strongs/h/h1964.md), and the ['ûlām](../../strongs/h/h197.md) of the [ḥāṣēr](../../strongs/h/h2691.md);

<a name="ezekiel_41_16"></a>Ezekiel 41:16

The [caph](../../strongs/h/h5592.md), and the ['āṭam](../../strongs/h/h331.md) [ḥallôn](../../strongs/h/h2474.md), and the ['atîq](../../strongs/h/h862.md) [cabiyb](../../strongs/h/h5439.md) on their [šālôš](../../strongs/h/h7969.md), over [neḡeḏ](../../strongs/h/h5048.md) the [caph](../../strongs/h/h5592.md), [šāḥîp̄](../../strongs/h/h7824.md) with ['ets](../../strongs/h/h6086.md) [cabiyb](../../strongs/h/h5439.md), and from the ['erets](../../strongs/h/h776.md) up to the [ḥallôn](../../strongs/h/h2474.md), and the [ḥallôn](../../strongs/h/h2474.md) were [kāsâ](../../strongs/h/h3680.md);

<a name="ezekiel_41_17"></a>Ezekiel 41:17

To that above the [peṯaḥ](../../strongs/h/h6607.md), even unto the [pᵊnîmî](../../strongs/h/h6442.md) [bayith](../../strongs/h/h1004.md), and [ḥûṣ](../../strongs/h/h2351.md), and by all the [qîr](../../strongs/h/h7023.md) [cabiyb](../../strongs/h/h5439.md) [pᵊnîmî](../../strongs/h/h6442.md) and [ḥîṣôn](../../strongs/h/h2435.md), by [midâ](../../strongs/h/h4060.md).

<a name="ezekiel_41_18"></a>Ezekiel 41:18

And it was ['asah](../../strongs/h/h6213.md) with [kĕruwb](../../strongs/h/h3742.md) and [timmōrâ](../../strongs/h/h8561.md), so that a [timmōrâ](../../strongs/h/h8561.md) was between a [kĕruwb](../../strongs/h/h3742.md) and a [kĕruwb](../../strongs/h/h3742.md); and every [kĕruwb](../../strongs/h/h3742.md) had two [paniym](../../strongs/h/h6440.md);

<a name="ezekiel_41_19"></a>Ezekiel 41:19

So that the [paniym](../../strongs/h/h6440.md) of an ['āḏām](../../strongs/h/h120.md) was toward the [timmōrâ](../../strongs/h/h8561.md) on the one side, and the [paniym](../../strongs/h/h6440.md) of a [kephiyr](../../strongs/h/h3715.md) toward the [timmōrâ](../../strongs/h/h8561.md) on the other side: it was ['asah](../../strongs/h/h6213.md) through all the [bayith](../../strongs/h/h1004.md) [cabiyb](../../strongs/h/h5439.md).

<a name="ezekiel_41_20"></a>Ezekiel 41:20

From the ['erets](../../strongs/h/h776.md) unto above the [peṯaḥ](../../strongs/h/h6607.md) were [kĕruwb](../../strongs/h/h3742.md) and [timmōrâ](../../strongs/h/h8561.md) ['asah](../../strongs/h/h6213.md), and on the [qîr](../../strongs/h/h7023.md) of the [heykal](../../strongs/h/h1964.md).

<a name="ezekiel_41_21"></a>Ezekiel 41:21

The [mᵊzûzâ](../../strongs/h/h4201.md) of the [heykal](../../strongs/h/h1964.md) were [rāḇaʿ](../../strongs/h/h7251.md), and the [paniym](../../strongs/h/h6440.md) of the [qodesh](../../strongs/h/h6944.md); the [mar'ê](../../strongs/h/h4758.md) of the one as the [mar'ê](../../strongs/h/h4758.md) of the other.

<a name="ezekiel_41_22"></a>Ezekiel 41:22

The [mizbeach](../../strongs/h/h4196.md) of ['ets](../../strongs/h/h6086.md) was three ['ammâ](../../strongs/h/h520.md) [gāḇōha](../../strongs/h/h1364.md), and the ['ōreḵ](../../strongs/h/h753.md) thereof two ['ammâ](../../strongs/h/h520.md); and the [miqṣôaʿ](../../strongs/h/h4740.md) thereof, and the ['ōreḵ](../../strongs/h/h753.md) thereof, and the [qîr](../../strongs/h/h7023.md) thereof, were of ['ets](../../strongs/h/h6086.md): and he [dabar](../../strongs/h/h1696.md) unto me, This is the [šulḥān](../../strongs/h/h7979.md) that is [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_41_23"></a>Ezekiel 41:23

And the [heykal](../../strongs/h/h1964.md) and the [qodesh](../../strongs/h/h6944.md) had two [deleṯ](../../strongs/h/h1817.md).

<a name="ezekiel_41_24"></a>Ezekiel 41:24

And the [deleṯ](../../strongs/h/h1817.md) had two [deleṯ](../../strongs/h/h1817.md) apiece, two [mûsaḇâ](../../strongs/h/h4142.md) [deleṯ](../../strongs/h/h1817.md); [šᵊnayim](../../strongs/h/h8147.md) for the one [deleṯ](../../strongs/h/h1817.md), and two [deleṯ](../../strongs/h/h1817.md) for the ['aḥēr](../../strongs/h/h312.md).

<a name="ezekiel_41_25"></a>Ezekiel 41:25

And there were ['asah](../../strongs/h/h6213.md) on them, on the [deleṯ](../../strongs/h/h1817.md) of the [heykal](../../strongs/h/h1964.md), [kĕruwb](../../strongs/h/h3742.md) and [timmōrâ](../../strongs/h/h8561.md), like as were ['asah](../../strongs/h/h6213.md) upon the [qîr](../../strongs/h/h7023.md); and there were [ʿāḇ](../../strongs/h/h5646.md) ['ets](../../strongs/h/h6086.md) upon the [paniym](../../strongs/h/h6440.md) of the ['ûlām](../../strongs/h/h197.md) [ḥûṣ](../../strongs/h/h2351.md).

<a name="ezekiel_41_26"></a>Ezekiel 41:26

And there were ['āṭam](../../strongs/h/h331.md) [ḥallôn](../../strongs/h/h2474.md) and [timmōrâ](../../strongs/h/h8561.md) on the one side and on the other side, on the [kāṯēp̄](../../strongs/h/h3802.md) of the ['ûlām](../../strongs/h/h197.md), and upon the [tsela'](../../strongs/h/h6763.md) of the [bayith](../../strongs/h/h1004.md), and [ʿāḇ](../../strongs/h/h5646.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 40](ezekiel_40.md) - [Ezekiel 42](ezekiel_42.md)