# [Ezekiel 29](https://www.blueletterbible.org/kjv/ezekiel/29)

<a name="ezekiel_29_1"></a>Ezekiel 29:1

In the tenth [šānâ](../../strongs/h/h8141.md), in the tenth month, in the twelfth day of the [ḥōḏeš](../../strongs/h/h2320.md), the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_29_2"></a>Ezekiel 29:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [śûm](../../strongs/h/h7760.md) thy [paniym](../../strongs/h/h6440.md) against [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), and [nāḇā'](../../strongs/h/h5012.md) against him, and against all [Mitsrayim](../../strongs/h/h4714.md):

<a name="ezekiel_29_3"></a>Ezekiel 29:3

[dabar](../../strongs/h/h1696.md), and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I am against thee, [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), the [gadowl](../../strongs/h/h1419.md) [tannîn](../../strongs/h/h8577.md) that [rāḇaṣ](../../strongs/h/h7257.md) in the midst of his [yᵊ'ōr](../../strongs/h/h2975.md), which hath ['āmar](../../strongs/h/h559.md), My [yᵊ'ōr](../../strongs/h/h2975.md) is mine own, and I have ['asah](../../strongs/h/h6213.md) it for myself.

<a name="ezekiel_29_4"></a>Ezekiel 29:4

But I will [nathan](../../strongs/h/h5414.md) [ḥāḥ](../../strongs/h/h2397.md) [ḥāḥ](../../strongs/h/h2397.md) in thy [lᵊḥî](../../strongs/h/h3895.md), and I will cause the [dāḡâ](../../strongs/h/h1710.md) of thy [yᵊ'ōr](../../strongs/h/h2975.md) to [dāḇaq](../../strongs/h/h1692.md) unto thy [qaśqeśeṯ](../../strongs/h/h7193.md), and I will [ʿālâ](../../strongs/h/h5927.md) thee out of the midst of thy [yᵊ'ōr](../../strongs/h/h2975.md), and all the [dāḡâ](../../strongs/h/h1710.md) of thy [yᵊ'ōr](../../strongs/h/h2975.md) shall [dāḇaq](../../strongs/h/h1692.md) unto thy [qaśqeśeṯ](../../strongs/h/h7193.md).

<a name="ezekiel_29_5"></a>Ezekiel 29:5

And I will [nāṭaš](../../strongs/h/h5203.md) thee thrown into the [midbar](../../strongs/h/h4057.md), thee and all the [dāḡâ](../../strongs/h/h1710.md) of thy [yᵊ'ōr](../../strongs/h/h2975.md): thou shalt [naphal](../../strongs/h/h5307.md) upon the [paniym](../../strongs/h/h6440.md) [sadeh](../../strongs/h/h7704.md); thou shalt not be ['āsap̄](../../strongs/h/h622.md), nor [qāḇaṣ](../../strongs/h/h6908.md): I have [nathan](../../strongs/h/h5414.md) thee for ['oklah](../../strongs/h/h402.md) to the [chay](../../strongs/h/h2416.md) of the ['erets](../../strongs/h/h776.md) and to the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md).

<a name="ezekiel_29_6"></a>Ezekiel 29:6

And all the [yashab](../../strongs/h/h3427.md) of [Mitsrayim](../../strongs/h/h4714.md) shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), because they have been a [mašʿēnâ](../../strongs/h/h4938.md) of [qānê](../../strongs/h/h7070.md) to the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_29_7"></a>Ezekiel 29:7

When they [tāp̄aś](../../strongs/h/h8610.md) of thee by thy [kaph](../../strongs/h/h3709.md), thou didst [rāṣaṣ](../../strongs/h/h7533.md), and [bāqaʿ](../../strongs/h/h1234.md) all their [kāṯēp̄](../../strongs/h/h3802.md): and when they [šāʿan](../../strongs/h/h8172.md) upon thee, thou [shabar](../../strongs/h/h7665.md), and madest all their [māṯnayim](../../strongs/h/h4975.md) to be at a [ʿāmaḏ](../../strongs/h/h5976.md).

<a name="ezekiel_29_8"></a>Ezekiel 29:8

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I will [bow'](../../strongs/h/h935.md) a [chereb](../../strongs/h/h2719.md) upon thee, and [karath](../../strongs/h/h3772.md) ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md) out of thee.

<a name="ezekiel_29_9"></a>Ezekiel 29:9

And the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) shall be [šᵊmāmâ](../../strongs/h/h8077.md) and [chorbah](../../strongs/h/h2723.md); and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md): because he hath ['āmar](../../strongs/h/h559.md), The [yᵊ'ōr](../../strongs/h/h2975.md) is mine, and I have ['asah](../../strongs/h/h6213.md) it.

<a name="ezekiel_29_10"></a>Ezekiel 29:10

Behold, therefore I am against thee, and against thy [yᵊ'ōr](../../strongs/h/h2975.md), and I will [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) [ḥōreḇ](../../strongs/h/h2721.md) [chorbah](../../strongs/h/h2723.md) and [šᵊmāmâ](../../strongs/h/h8077.md), from the [Miḡdôl](../../strongs/h/h4024.md) of [Sᵊvēnê](../../strongs/h/h5482.md) even unto the [gᵊḇûl](../../strongs/h/h1366.md) of [Kûš](../../strongs/h/h3568.md).

<a name="ezekiel_29_11"></a>Ezekiel 29:11

No [regel](../../strongs/h/h7272.md) of ['āḏām](../../strongs/h/h120.md) shall ['abar](../../strongs/h/h5674.md) it, nor [regel](../../strongs/h/h7272.md) of [bĕhemah](../../strongs/h/h929.md) shall ['abar](../../strongs/h/h5674.md) it, neither shall it be [yashab](../../strongs/h/h3427.md) forty [šānâ](../../strongs/h/h8141.md).

<a name="ezekiel_29_12"></a>Ezekiel 29:12

And I will [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) [šᵊmāmâ](../../strongs/h/h8077.md) in the midst of the ['erets](../../strongs/h/h776.md) that are [šāmēm](../../strongs/h/h8074.md), and her [ʿîr](../../strongs/h/h5892.md) among the [ʿîr](../../strongs/h/h5892.md) that are laid [ḥāraḇ](../../strongs/h/h2717.md) shall be [šᵊmāmâ](../../strongs/h/h8077.md) forty [šānâ](../../strongs/h/h8141.md): and I will [puwts](../../strongs/h/h6327.md) the [Mitsrayim](../../strongs/h/h4714.md) among the [gowy](../../strongs/h/h1471.md), and will [zārâ](../../strongs/h/h2219.md) them through the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_29_13"></a>Ezekiel 29:13

Yet thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); At the [qēṣ](../../strongs/h/h7093.md) of forty [šānâ](../../strongs/h/h8141.md) will I [qāḇaṣ](../../strongs/h/h6908.md) the [Mitsrayim](../../strongs/h/h4714.md) from the ['am](../../strongs/h/h5971.md) whither they were [puwts](../../strongs/h/h6327.md):

<a name="ezekiel_29_14"></a>Ezekiel 29:14

And I will [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) of [Mitsrayim](../../strongs/h/h4714.md), and will cause them to [shuwb](../../strongs/h/h7725.md) into the ['erets](../../strongs/h/h776.md) of [Paṯrôs](../../strongs/h/h6624.md), into the ['erets](../../strongs/h/h776.md) of their [mᵊḵôrâ](../../strongs/h/h4351.md); and they shall be there a [šāp̄āl](../../strongs/h/h8217.md) [mamlāḵâ](../../strongs/h/h4467.md).

<a name="ezekiel_29_15"></a>Ezekiel 29:15

It shall be the [šāp̄āl](../../strongs/h/h8217.md) of the [mamlāḵâ](../../strongs/h/h4467.md); neither shall it [nasa'](../../strongs/h/h5375.md) itself any more above the [gowy](../../strongs/h/h1471.md): for I will [māʿaṭ](../../strongs/h/h4591.md) them, that they shall no more [radah](../../strongs/h/h7287.md) over the [gowy](../../strongs/h/h1471.md).

<a name="ezekiel_29_16"></a>Ezekiel 29:16

And it shall be no more the [miḇṭāḥ](../../strongs/h/h4009.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), which [zakar](../../strongs/h/h2142.md) their ['avon](../../strongs/h/h5771.md) to [zakar](../../strongs/h/h2142.md), when they shall [panah](../../strongs/h/h6437.md) ['aḥar](../../strongs/h/h310.md) them: but they shall [yada'](../../strongs/h/h3045.md) that I am the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_29_17"></a>Ezekiel 29:17

And it came to pass in the seven and twentieth [šānâ](../../strongs/h/h8141.md), in the [ri'šôn](../../strongs/h/h7223.md) month, in the first day of the [ḥōḏeš](../../strongs/h/h2320.md), the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_29_18"></a>Ezekiel 29:18

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) ['abad](../../strongs/h/h5647.md) his [ḥayil](../../strongs/h/h2428.md) to ['abad](../../strongs/h/h5647.md) a [gadowl](../../strongs/h/h1419.md) [ʿăḇōḏâ](../../strongs/h/h5656.md) against [Ṣōr](../../strongs/h/h6865.md): every [ro'sh](../../strongs/h/h7218.md) was made [qāraḥ](../../strongs/h/h7139.md), and every [kāṯēp̄](../../strongs/h/h3802.md) was [māraṭ](../../strongs/h/h4803.md): yet had he no [śāḵār](../../strongs/h/h7939.md), nor his [ḥayil](../../strongs/h/h2428.md), for [Ṣōr](../../strongs/h/h6865.md), for the [ʿăḇōḏâ](../../strongs/h/h5656.md) that he had ['abad](../../strongs/h/h5647.md) against it:

<a name="ezekiel_29_19"></a>Ezekiel 29:19

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I will [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) unto [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md); and he shall [nasa'](../../strongs/h/h5375.md) her [hāmôn](../../strongs/h/h1995.md), and [šālal](../../strongs/h/h7997.md) her [šālāl](../../strongs/h/h7998.md), and [bāzaz](../../strongs/h/h962.md) her [baz](../../strongs/h/h957.md); and it shall be the [śāḵār](../../strongs/h/h7939.md) for his [ḥayil](../../strongs/h/h2428.md).

<a name="ezekiel_29_20"></a>Ezekiel 29:20

I have [nathan](../../strongs/h/h5414.md) him the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) for his [pe'ullah](../../strongs/h/h6468.md) wherewith he ['abad](../../strongs/h/h5647.md) against it, because they ['asah](../../strongs/h/h6213.md) for me, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_29_21"></a>Ezekiel 29:21

In that [yowm](../../strongs/h/h3117.md) will I [ṣāmaḥ](../../strongs/h/h6779.md) the [qeren](../../strongs/h/h7161.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) to [ṣāmaḥ](../../strongs/h/h6779.md), and I will [nathan](../../strongs/h/h5414.md) thee the [pitāḥôn](../../strongs/h/h6610.md) of the [peh](../../strongs/h/h6310.md) in the midst of them; and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 28](ezekiel_28.md) - [Ezekiel 30](ezekiel_30.md)