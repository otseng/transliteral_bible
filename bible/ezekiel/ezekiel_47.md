# [Ezekiel 47](https://www.blueletterbible.org/kjv/ezekiel/47)

<a name="ezekiel_47_1"></a>Ezekiel 47:1

Afterward he [shuwb](../../strongs/h/h7725.md) me unto the [peṯaḥ](../../strongs/h/h6607.md) of the [bayith](../../strongs/h/h1004.md); and, behold, [mayim](../../strongs/h/h4325.md) [yāṣā'](../../strongs/h/h3318.md) from under the [mip̄tān](../../strongs/h/h4670.md) of the [bayith](../../strongs/h/h1004.md) [qāḏîm](../../strongs/h/h6921.md): for the [paniym](../../strongs/h/h6440.md) of the [bayith](../../strongs/h/h1004.md) the [qāḏîm](../../strongs/h/h6921.md), and the [mayim](../../strongs/h/h4325.md) [yarad](../../strongs/h/h3381.md) from under from the [yᵊmānî](../../strongs/h/h3233.md) [kāṯēp̄](../../strongs/h/h3802.md) of the [bayith](../../strongs/h/h1004.md), at the [neḡeḇ](../../strongs/h/h5045.md) side of the [mizbeach](../../strongs/h/h4196.md).

<a name="ezekiel_47_2"></a>Ezekiel 47:2

Then he [yāṣā'](../../strongs/h/h3318.md) me of the [derek](../../strongs/h/h1870.md) of the [sha'ar](../../strongs/h/h8179.md) [ṣāp̄ôn](../../strongs/h/h6828.md), and [cabab](../../strongs/h/h5437.md) me the [derek](../../strongs/h/h1870.md) without unto the [ḥûṣ](../../strongs/h/h2351.md) [sha'ar](../../strongs/h/h8179.md) by the [derek](../../strongs/h/h1870.md) that [panah](../../strongs/h/h6437.md) [qāḏîm](../../strongs/h/h6921.md); and, behold, there [pāḵâ](../../strongs/h/h6379.md) [mayim](../../strongs/h/h4325.md) on the [yᵊmānî](../../strongs/h/h3233.md) [kāṯēp̄](../../strongs/h/h3802.md).

<a name="ezekiel_47_3"></a>Ezekiel 47:3

And when the ['iysh](../../strongs/h/h376.md) that had the [qāv](../../strongs/h/h6957.md) in his [yad](../../strongs/h/h3027.md) [yāṣā'](../../strongs/h/h3318.md) [qāḏîm](../../strongs/h/h6921.md), he [māḏaḏ](../../strongs/h/h4058.md) a thousand ['ammâ](../../strongs/h/h520.md), and he ['abar](../../strongs/h/h5674.md) me the [mayim](../../strongs/h/h4325.md); the [mayim](../../strongs/h/h4325.md) were to the ['ep̄es](../../strongs/h/h657.md).

<a name="ezekiel_47_4"></a>Ezekiel 47:4

Again he [māḏaḏ](../../strongs/h/h4058.md) a thousand, and ['abar](../../strongs/h/h5674.md) me the [mayim](../../strongs/h/h4325.md); the [mayim](../../strongs/h/h4325.md) were to the [bereḵ](../../strongs/h/h1290.md). Again he [māḏaḏ](../../strongs/h/h4058.md) a thousand, and ['abar](../../strongs/h/h5674.md) me; the [mayim](../../strongs/h/h4325.md) were to the [māṯnayim](../../strongs/h/h4975.md).

<a name="ezekiel_47_5"></a>Ezekiel 47:5

Afterward he [māḏaḏ](../../strongs/h/h4058.md) a thousand; and it was a [nachal](../../strongs/h/h5158.md) that I [yakol](../../strongs/h/h3201.md) not ['abar](../../strongs/h/h5674.md): for the [mayim](../../strongs/h/h4325.md) were [gā'â](../../strongs/h/h1342.md), [mayim](../../strongs/h/h4325.md) to [śāḥû](../../strongs/h/h7813.md), a [nachal](../../strongs/h/h5158.md) that could not be ['abar](../../strongs/h/h5674.md).

<a name="ezekiel_47_6"></a>Ezekiel 47:6

And he ['āmar](../../strongs/h/h559.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), hast thou [ra'ah](../../strongs/h/h7200.md) this? Then he [yālaḵ](../../strongs/h/h3212.md) me, and caused me to [shuwb](../../strongs/h/h7725.md) to the [saphah](../../strongs/h/h8193.md) of the [nachal](../../strongs/h/h5158.md).

<a name="ezekiel_47_7"></a>Ezekiel 47:7

Now when I had [shuwb](../../strongs/h/h7725.md), behold, at the [saphah](../../strongs/h/h8193.md) of the [nachal](../../strongs/h/h5158.md) were [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md) ['ets](../../strongs/h/h6086.md) on the one side and on the other.

<a name="ezekiel_47_8"></a>Ezekiel 47:8

Then ['āmar](../../strongs/h/h559.md) he unto me, These [mayim](../../strongs/h/h4325.md) [yāṣā'](../../strongs/h/h3318.md) toward the [qaḏmôn](../../strongs/h/h6930.md) [gᵊlîlâ](../../strongs/h/h1552.md), and [yarad](../../strongs/h/h3381.md) into the ['arabah](../../strongs/h/h6160.md), and [bow'](../../strongs/h/h935.md) into the [yam](../../strongs/h/h3220.md): which being [yāṣā'](../../strongs/h/h3318.md) into the [yam](../../strongs/h/h3220.md), the [mayim](../../strongs/h/h4325.md) shall be [rapha'](../../strongs/h/h7495.md).

<a name="ezekiel_47_9"></a>Ezekiel 47:9

And it shall come to pass, that every [nephesh](../../strongs/h/h5315.md) that [chay](../../strongs/h/h2416.md), which [šāraṣ](../../strongs/h/h8317.md), whithersoever the [nachal](../../strongs/h/h5158.md) shall [bow'](../../strongs/h/h935.md), shall [ḥāyâ](../../strongs/h/h2421.md): and there shall be a [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md) multitude of [dāḡâ](../../strongs/h/h1710.md), because these [mayim](../../strongs/h/h4325.md) shall [bow'](../../strongs/h/h935.md) thither: for they shall be [rapha'](../../strongs/h/h7495.md); and every thing shall [chayay](../../strongs/h/h2425.md) whither the [nachal](../../strongs/h/h5158.md) [bow'](../../strongs/h/h935.md).

<a name="ezekiel_47_10"></a>Ezekiel 47:10

And it shall come to pass, that the [dûḡ](../../strongs/h/h1728.md) shall ['amad](../../strongs/h/h5975.md) ['amad](../../strongs/h/h5975.md) upon it from [ʿÊn Ḡeḏî](../../strongs/h/h5872.md) even unto [ʿÊn ʿEḡlayim](../../strongs/h/h5882.md); they shall be a place to [mišṭôaḥ](../../strongs/h/h4894.md) [ḥērem](../../strongs/h/h2764.md); their [dāḡâ](../../strongs/h/h1710.md) shall be according to their [miyn](../../strongs/h/h4327.md), as the [dāḡâ](../../strongs/h/h1710.md) of the [gadowl](../../strongs/h/h1419.md) [yam](../../strongs/h/h3220.md), [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md).

<a name="ezekiel_47_11"></a>Ezekiel 47:11

But the [biṣṣâ](../../strongs/h/h1207.md) thereof and the [geḇe'](../../strongs/h/h1360.md) thereof shall not be [rapha'](../../strongs/h/h7495.md); they shall be [nathan](../../strongs/h/h5414.md) to [melaḥ](../../strongs/h/h4417.md).

<a name="ezekiel_47_12"></a>Ezekiel 47:12

And by the [nachal](../../strongs/h/h5158.md) upon the [saphah](../../strongs/h/h8193.md) thereof, on this side and on that side, shall [ʿālâ](../../strongs/h/h5927.md) all ['ets](../../strongs/h/h6086.md) for [ma'akal](../../strongs/h/h3978.md), whose ['aleh](../../strongs/h/h5929.md) shall not [nabel](../../strongs/h/h5034.md), neither shall the [pĕriy](../../strongs/h/h6529.md) thereof be [tamam](../../strongs/h/h8552.md): it shall [bāḵar](../../strongs/h/h1069.md) according to his [ḥōḏeš](../../strongs/h/h2320.md), because their [mayim](../../strongs/h/h4325.md) they [yāṣā'](../../strongs/h/h3318.md) of the [miqdash](../../strongs/h/h4720.md): and the [pĕriy](../../strongs/h/h6529.md) thereof shall be for [ma'akal](../../strongs/h/h3978.md), and the ['aleh](../../strongs/h/h5929.md) thereof for [tᵊrûp̄â](../../strongs/h/h8644.md).

<a name="ezekiel_47_13"></a>Ezekiel 47:13

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); [gê](../../strongs/h/h1454.md) shall be the [gᵊḇûl](../../strongs/h/h1366.md), whereby ye shall [nāḥal](../../strongs/h/h5157.md) the ['erets](../../strongs/h/h776.md) according to the twelve [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md): [Yôsēp̄](../../strongs/h/h3130.md) shall have two [chebel](../../strongs/h/h2256.md).

<a name="ezekiel_47_14"></a>Ezekiel 47:14

And ye shall [nāḥal](../../strongs/h/h5157.md) it, ['iysh](../../strongs/h/h376.md) as well as ['ach](../../strongs/h/h251.md): concerning the which I [nasa'](../../strongs/h/h5375.md) mine [yad](../../strongs/h/h3027.md) to [nathan](../../strongs/h/h5414.md) it unto your ['ab](../../strongs/h/h1.md): and this ['erets](../../strongs/h/h776.md) shall [naphal](../../strongs/h/h5307.md) unto you for [nachalah](../../strongs/h/h5159.md).

<a name="ezekiel_47_15"></a>Ezekiel 47:15

And this shall be the [gᵊḇûl](../../strongs/h/h1366.md) of the ['erets](../../strongs/h/h776.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md) [pē'â](../../strongs/h/h6285.md), from the [gadowl](../../strongs/h/h1419.md) [yam](../../strongs/h/h3220.md), the [derek](../../strongs/h/h1870.md) of [Ḥeṯlōn](../../strongs/h/h2855.md), as men [bow'](../../strongs/h/h935.md) to [Ṣᵊḏāḏ](../../strongs/h/h6657.md);

<a name="ezekiel_47_16"></a>Ezekiel 47:16

[Ḥămāṯ](../../strongs/h/h2574.md), [Bērôṯâ](../../strongs/h/h1268.md), [Siḇrayim](../../strongs/h/h5453.md), which is between the [gᵊḇûl](../../strongs/h/h1366.md) of [Dammeśeq](../../strongs/h/h1834.md) and the [gᵊḇûl](../../strongs/h/h1366.md) of [Ḥămāṯ](../../strongs/h/h2574.md); [Ḥāṣar Hatîḵôn](../../strongs/h/h2694.md), which is by the [gᵊḇûl](../../strongs/h/h1366.md) of [Ḥavrān](../../strongs/h/h2362.md).

<a name="ezekiel_47_17"></a>Ezekiel 47:17

And the [gᵊḇûl](../../strongs/h/h1366.md) from the [yam](../../strongs/h/h3220.md) shall be [Ḥăṣar ʿÊnôn](../../strongs/h/h2703.md), the [gᵊḇûl](../../strongs/h/h1366.md) of [Dammeśeq](../../strongs/h/h1834.md), and the [ṣāp̄ôn](../../strongs/h/h6828.md) [ṣāp̄ôn](../../strongs/h/h6828.md), and the [gᵊḇûl](../../strongs/h/h1366.md) of [Ḥămāṯ](../../strongs/h/h2574.md). And this is the [ṣāp̄ôn](../../strongs/h/h6828.md) [pē'â](../../strongs/h/h6285.md).

<a name="ezekiel_47_18"></a>Ezekiel 47:18

And the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) ye shall [māḏaḏ](../../strongs/h/h4058.md) from [Ḥavrān](../../strongs/h/h2362.md), and from [Dammeśeq](../../strongs/h/h1834.md), and from [Gilʿāḏ](../../strongs/h/h1568.md), and from the ['erets](../../strongs/h/h776.md) of [Yisra'el](../../strongs/h/h3478.md) by [Yardēn](../../strongs/h/h3383.md), from the [gᵊḇûl](../../strongs/h/h1366.md) unto the [qaḏmōnî](../../strongs/h/h6931.md) [yam](../../strongs/h/h3220.md). And this is the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md).

<a name="ezekiel_47_19"></a>Ezekiel 47:19

And the [neḡeḇ](../../strongs/h/h5045.md) [pē'â](../../strongs/h/h6285.md) [têmān](../../strongs/h/h8486.md), from [Tāmār](../../strongs/h/h8559.md) even to the [mayim](../../strongs/h/h4325.md) of [Mᵊrîḇâ](../../strongs/h/h4808.md) [Mᵊrîḇâ](../../strongs/h/h4809.md) in [Qāḏēš](../../strongs/h/h6946.md), the [nachal](../../strongs/h/h5158.md) to the [gadowl](../../strongs/h/h1419.md) [yam](../../strongs/h/h3220.md). And this is the [têmān](../../strongs/h/h8486.md) [pē'â](../../strongs/h/h6285.md) [neḡeḇ](../../strongs/h/h5045.md).

<a name="ezekiel_47_20"></a>Ezekiel 47:20

The [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md) also shall be the [gadowl](../../strongs/h/h1419.md) [yam](../../strongs/h/h3220.md) from the [gᵊḇûl](../../strongs/h/h1366.md), till a man [bow'](../../strongs/h/h935.md) over against [Ḥămāṯ](../../strongs/h/h2574.md). This is the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md).

<a name="ezekiel_47_21"></a>Ezekiel 47:21

So shall ye [chalaq](../../strongs/h/h2505.md) this ['erets](../../strongs/h/h776.md) unto you according to the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_47_22"></a>Ezekiel 47:22

And it shall come to pass, that ye shall [naphal](../../strongs/h/h5307.md) it by lot for a [nachalah](../../strongs/h/h5159.md) unto you, and to the [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) [tavek](../../strongs/h/h8432.md) you, which shall [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) [tavek](../../strongs/h/h8432.md) you: and they shall be unto you as ['ezrāḥ](../../strongs/h/h249.md) in the ['ezrāḥ](../../strongs/h/h249.md) among the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); they shall [naphal](../../strongs/h/h5307.md) [nachalah](../../strongs/h/h5159.md) with you [tavek](../../strongs/h/h8432.md) the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_47_23"></a>Ezekiel 47:23

And it shall come to pass, that in what [shebet](../../strongs/h/h7626.md) the [ger](../../strongs/h/h1616.md) [guwr](../../strongs/h/h1481.md), there shall ye [nathan](../../strongs/h/h5414.md) him his [nachalah](../../strongs/h/h5159.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 46](ezekiel_46.md) - [Ezekiel 48](ezekiel_48.md)