# [Ezekiel 21](https://www.blueletterbible.org/kjv/ezekiel/21)

<a name="ezekiel_21_1"></a>Ezekiel 21:1

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_21_2"></a>Ezekiel 21:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [śûm](../../strongs/h/h7760.md) thy [paniym](../../strongs/h/h6440.md) toward [Yĕruwshalaim](../../strongs/h/h3389.md), and [nāṭap̄](../../strongs/h/h5197.md) toward the [miqdash](../../strongs/h/h4720.md), and [nāḇā'](../../strongs/h/h5012.md) against the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md),

<a name="ezekiel_21_3"></a>Ezekiel 21:3

And ['āmar](../../strongs/h/h559.md) to the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, I am against thee, and will [yāṣā'](../../strongs/h/h3318.md) my [chereb](../../strongs/h/h2719.md) out of his [taʿar](../../strongs/h/h8593.md), and will [karath](../../strongs/h/h3772.md) from thee the [tsaddiyq](../../strongs/h/h6662.md) and the [rasha'](../../strongs/h/h7563.md).

<a name="ezekiel_21_4"></a>Ezekiel 21:4

Seeing then that I will [karath](../../strongs/h/h3772.md) from thee the [tsaddiyq](../../strongs/h/h6662.md) and the [rasha'](../../strongs/h/h7563.md), therefore shall my [chereb](../../strongs/h/h2719.md) [yāṣā'](../../strongs/h/h3318.md) out of his [taʿar](../../strongs/h/h8593.md) against all [basar](../../strongs/h/h1320.md) from the [neḡeḇ](../../strongs/h/h5045.md) to the [ṣāp̄ôn](../../strongs/h/h6828.md):

<a name="ezekiel_21_5"></a>Ezekiel 21:5

That all [basar](../../strongs/h/h1320.md) may [yada'](../../strongs/h/h3045.md) that I [Yĕhovah](../../strongs/h/h3068.md) have [yāṣā'](../../strongs/h/h3318.md) my [chereb](../../strongs/h/h2719.md) out of his [taʿar](../../strongs/h/h8593.md): it shall not [shuwb](../../strongs/h/h7725.md) any more.

<a name="ezekiel_21_6"></a>Ezekiel 21:6

['ānaḥ](../../strongs/h/h584.md) therefore, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), with the [šibārôn](../../strongs/h/h7670.md) of thy [māṯnayim](../../strongs/h/h4975.md); and with [mᵊrîrûṯ](../../strongs/h/h4814.md) ['ānaḥ](../../strongs/h/h584.md) before their ['ayin](../../strongs/h/h5869.md).

<a name="ezekiel_21_7"></a>Ezekiel 21:7

And it shall be, when they ['āmar](../../strongs/h/h559.md) unto thee, Wherefore ['ānaḥ](../../strongs/h/h584.md) thou? that thou shalt ['āmar](../../strongs/h/h559.md), For the [šᵊmûʿâ](../../strongs/h/h8052.md); because it [bow'](../../strongs/h/h935.md): and every [leb](../../strongs/h/h3820.md) shall [māsas](../../strongs/h/h4549.md), and all [yad](../../strongs/h/h3027.md) shall be [rāp̄â](../../strongs/h/h7503.md), and every [ruwach](../../strongs/h/h7307.md) shall [kāhâ](../../strongs/h/h3543.md), and all [bereḵ](../../strongs/h/h1290.md) shall be [yālaḵ](../../strongs/h/h3212.md) as [mayim](../../strongs/h/h4325.md): behold, it [bow'](../../strongs/h/h935.md), and shall be brought to pass, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_21_8"></a>Ezekiel 21:8

Again the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_21_9"></a>Ezekiel 21:9

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nāḇā'](../../strongs/h/h5012.md), and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); ['āmar](../../strongs/h/h559.md), A [chereb](../../strongs/h/h2719.md), a [chereb](../../strongs/h/h2719.md) is [ḥāḏaḏ](../../strongs/h/h2300.md), and also [māraṭ](../../strongs/h/h4803.md):

<a name="ezekiel_21_10"></a>Ezekiel 21:10

It is [ḥāḏaḏ](../../strongs/h/h2300.md) to make a [ṭeḇaḥ](../../strongs/h/h2874.md) [ṭāḇaḥ](../../strongs/h/h2873.md); it is [môrāṭ](../../strongs/h/h4178.md) that it may [baraq](../../strongs/h/h1300.md): should we then make [śûś](../../strongs/h/h7797.md)? it [mā'as](../../strongs/h/h3988.md) the [shebet](../../strongs/h/h7626.md) of my [ben](../../strongs/h/h1121.md), as every ['ets](../../strongs/h/h6086.md).

<a name="ezekiel_21_11"></a>Ezekiel 21:11

And he hath [nathan](../../strongs/h/h5414.md) it to be [māraṭ](../../strongs/h/h4803.md), that it may be [kaph](../../strongs/h/h3709.md) [tāp̄aś](../../strongs/h/h8610.md): this [chereb](../../strongs/h/h2719.md) is [ḥāḏaḏ](../../strongs/h/h2300.md), and it is [môrāṭ](../../strongs/h/h4178.md), to [nathan](../../strongs/h/h5414.md) it into the [yad](../../strongs/h/h3027.md) of the [harag](../../strongs/h/h2026.md).

<a name="ezekiel_21_12"></a>Ezekiel 21:12

[zāʿaq](../../strongs/h/h2199.md) and [yālal](../../strongs/h/h3213.md), [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md): for it shall be upon my ['am](../../strongs/h/h5971.md), it shall be upon all the [nāśî'](../../strongs/h/h5387.md) of [Yisra'el](../../strongs/h/h3478.md): [māḡar](../../strongs/h/h4048.md) by reason of the [chereb](../../strongs/h/h2719.md) shall be upon my ['am](../../strongs/h/h5971.md): [sāp̄aq](../../strongs/h/h5606.md) therefore upon thy [yārēḵ](../../strongs/h/h3409.md).

<a name="ezekiel_21_13"></a>Ezekiel 21:13

Because it is a [bachan](../../strongs/h/h974.md), and what if the sword [mā'as](../../strongs/h/h3988.md) even the [shebet](../../strongs/h/h7626.md)? it shall be no more, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_21_14"></a>Ezekiel 21:14

Thou therefore, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nāḇā'](../../strongs/h/h5012.md), and [nakah](../../strongs/h/h5221.md) thine [kaph](../../strongs/h/h3709.md) [kaph](../../strongs/h/h3709.md) [nakah](../../strongs/h/h5221.md), and let the [chereb](../../strongs/h/h2719.md) be [kāp̄al](../../strongs/h/h3717.md) the third time, the [chereb](../../strongs/h/h2719.md) of the [ḥālāl](../../strongs/h/h2491.md): it is the [chereb](../../strongs/h/h2719.md) of the [gadowl](../../strongs/h/h1419.md) men that are [ḥālāl](../../strongs/h/h2491.md), which entereth into their [ḥāḏar](../../strongs/h/h2314.md).

<a name="ezekiel_21_15"></a>Ezekiel 21:15

I have [nathan](../../strongs/h/h5414.md) the ['iḇḥâ](../../strongs/h/h19.md) of the [chereb](../../strongs/h/h2719.md) against all their [sha'ar](../../strongs/h/h8179.md), that their [leb](../../strongs/h/h3820.md) may [mûḡ](../../strongs/h/h4127.md), and their [miḵšôl](../../strongs/h/h4383.md) be [rabah](../../strongs/h/h7235.md): ['āḥ](../../strongs/h/h253.md)! it is ['asah](../../strongs/h/h6213.md) [baraq](../../strongs/h/h1300.md), it is [māʿōṭ](../../strongs/h/h4593.md) for the [ṭeḇaḥ](../../strongs/h/h2874.md).

<a name="ezekiel_21_16"></a>Ezekiel 21:16

['āḥaḏ](../../strongs/h/h258.md), either [śûm](../../strongs/h/h7760.md) the [yāman](../../strongs/h/h3231.md), or on the [śam'al](../../strongs/h/h8041.md), whithersoever thy [paniym](../../strongs/h/h6440.md) is [yāʿaḏ](../../strongs/h/h3259.md).

<a name="ezekiel_21_17"></a>Ezekiel 21:17

I will also [nakah](../../strongs/h/h5221.md) mine [kaph](../../strongs/h/h3709.md) [kaph](../../strongs/h/h3709.md), and I will cause my [chemah](../../strongs/h/h2534.md) to [nuwach](../../strongs/h/h5117.md): I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) it.

<a name="ezekiel_21_18"></a>Ezekiel 21:18

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me again, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_21_19"></a>Ezekiel 21:19

Also, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [śûm](../../strongs/h/h7760.md) thee two [derek](../../strongs/h/h1870.md), that the [chereb](../../strongs/h/h2719.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) may [bow'](../../strongs/h/h935.md): both twain shall [yāṣā'](../../strongs/h/h3318.md) out of one ['erets](../../strongs/h/h776.md): and [bara'](../../strongs/h/h1254.md) thou a [yad](../../strongs/h/h3027.md), [bara'](../../strongs/h/h1254.md) it at the [ro'sh](../../strongs/h/h7218.md) of the [derek](../../strongs/h/h1870.md) to the [ʿîr](../../strongs/h/h5892.md).

<a name="ezekiel_21_20"></a>Ezekiel 21:20

[śûm](../../strongs/h/h7760.md) a [derek](../../strongs/h/h1870.md), that the [chereb](../../strongs/h/h2719.md) may [bow'](../../strongs/h/h935.md) to [Rabâ](../../strongs/h/h7237.md) of the [ben](../../strongs/h/h1121.md) [ʿAmmôn](../../strongs/h/h5983.md), and to [Yehuwdah](../../strongs/h/h3063.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) the [bāṣar](../../strongs/h/h1219.md).

<a name="ezekiel_21_21"></a>Ezekiel 21:21

For the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) ['amad](../../strongs/h/h5975.md) at the ['em](../../strongs/h/h517.md) of the [derek](../../strongs/h/h1870.md), at the [ro'sh](../../strongs/h/h7218.md) of the two [derek](../../strongs/h/h1870.md), to [qāsam](../../strongs/h/h7080.md) [qesem](../../strongs/h/h7081.md): he made his [chets](../../strongs/h/h2671.md) [qālal](../../strongs/h/h7043.md), he [sha'al](../../strongs/h/h7592.md) with [tᵊrāp̄îm](../../strongs/h/h8655.md), he [ra'ah](../../strongs/h/h7200.md) in the [kāḇēḏ](../../strongs/h/h3516.md).

<a name="ezekiel_21_22"></a>Ezekiel 21:22

At his [yamiyn](../../strongs/h/h3225.md) was the [qesem](../../strongs/h/h7081.md) for [Yĕruwshalaim](../../strongs/h/h3389.md), to [śûm](../../strongs/h/h7760.md) [kar](../../strongs/h/h3733.md), to [pāṯaḥ](../../strongs/h/h6605.md) the [peh](../../strongs/h/h6310.md) in the [reṣaḥ](../../strongs/h/h7524.md), to [ruwm](../../strongs/h/h7311.md) the [qowl](../../strongs/h/h6963.md) with [tᵊrûʿâ](../../strongs/h/h8643.md), to [śûm](../../strongs/h/h7760.md) [kar](../../strongs/h/h3733.md) against the [sha'ar](../../strongs/h/h8179.md), to [šāp̄aḵ](../../strongs/h/h8210.md) a [sōllâ](../../strongs/h/h5550.md), and to [bānâ](../../strongs/h/h1129.md) a [dāyēq](../../strongs/h/h1785.md).

<a name="ezekiel_21_23"></a>Ezekiel 21:23

And it shall be unto them as a [shav'](../../strongs/h/h7723.md) [qāsam](../../strongs/h/h7080.md) in their ['ayin](../../strongs/h/h5869.md), to them that have [shaba'](../../strongs/h/h7650.md) [šᵊḇûʿâ](../../strongs/h/h7621.md): but he will call to [zakar](../../strongs/h/h2142.md) the ['avon](../../strongs/h/h5771.md), that they may be [tāp̄aś](../../strongs/h/h8610.md).

<a name="ezekiel_21_24"></a>Ezekiel 21:24

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because ye have made your ['avon](../../strongs/h/h5771.md) to be [zakar](../../strongs/h/h2142.md), in that your [pesha'](../../strongs/h/h6588.md) are [gālâ](../../strongs/h/h1540.md), so that in all your ['aliylah](../../strongs/h/h5949.md) your [chatta'ath](../../strongs/h/h2403.md) do [ra'ah](../../strongs/h/h7200.md); because, I say, that ye are come to [zakar](../../strongs/h/h2142.md), ye shall be [tāp̄aś](../../strongs/h/h8610.md) with the [kaph](../../strongs/h/h3709.md).

<a name="ezekiel_21_25"></a>Ezekiel 21:25

And thou, [ḥālāl](../../strongs/h/h2491.md) [rasha'](../../strongs/h/h7563.md) [nāśî'](../../strongs/h/h5387.md) of [Yisra'el](../../strongs/h/h3478.md), whose [yowm](../../strongs/h/h3117.md) is [bow'](../../strongs/h/h935.md), [ʿēṯ](../../strongs/h/h6256.md) ['avon](../../strongs/h/h5771.md) shall have a [qēṣ](../../strongs/h/h7093.md),

<a name="ezekiel_21_26"></a>Ezekiel 21:26

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); [cuwr](../../strongs/h/h5493.md) the [miṣnep̄eṯ](../../strongs/h/h4701.md), and [ruwm](../../strongs/h/h7311.md) the [ʿăṭārâ](../../strongs/h/h5850.md): this shall not be the same: [gāḇah](../../strongs/h/h1361.md) him that is [šāp̄āl](../../strongs/h/h8217.md), and [šāp̄ēl](../../strongs/h/h8213.md) him that is [gāḇōha](../../strongs/h/h1364.md).

<a name="ezekiel_21_27"></a>Ezekiel 21:27

I will [ʿaûâ](../../strongs/h/h5754.md), [ʿaûâ](../../strongs/h/h5754.md), [ʿaûâ](../../strongs/h/h5754.md), it: and it shall [śûm](../../strongs/h/h7760.md) no more, until he [bow'](../../strongs/h/h935.md) whose [mishpat](../../strongs/h/h4941.md) it is; and I will [nathan](../../strongs/h/h5414.md) it him.

<a name="ezekiel_21_28"></a>Ezekiel 21:28

And thou, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nāḇā'](../../strongs/h/h5012.md) and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) concerning the [ben](../../strongs/h/h1121.md) [ʿAmmôn](../../strongs/h/h5983.md), and concerning their [cherpah](../../strongs/h/h2781.md); even ['āmar](../../strongs/h/h559.md) thou, The [chereb](../../strongs/h/h2719.md), the [chereb](../../strongs/h/h2719.md) is [pāṯaḥ](../../strongs/h/h6605.md): for the [ṭeḇaḥ](../../strongs/h/h2874.md) it is [māraṭ](../../strongs/h/h4803.md), to ['akal](../../strongs/h/h398.md) because of the [baraq](../../strongs/h/h1300.md):

<a name="ezekiel_21_29"></a>Ezekiel 21:29

Whiles they [chazah](../../strongs/h/h2372.md) [shav'](../../strongs/h/h7723.md) unto thee, whiles they [qāsam](../../strongs/h/h7080.md) a [kazab](../../strongs/h/h3577.md) unto thee, to [nathan](../../strongs/h/h5414.md) thee upon the [ṣaûā'r](../../strongs/h/h6677.md) of them that are [ḥālāl](../../strongs/h/h2491.md), of the [rasha'](../../strongs/h/h7563.md), whose [yowm](../../strongs/h/h3117.md) is [bow'](../../strongs/h/h935.md), [ʿēṯ](../../strongs/h/h6256.md) their ['avon](../../strongs/h/h5771.md) shall have a [qēṣ](../../strongs/h/h7093.md).

<a name="ezekiel_21_30"></a>Ezekiel 21:30

Shall I cause it to [shuwb](../../strongs/h/h7725.md) into his [taʿar](../../strongs/h/h8593.md)? I will [shaphat](../../strongs/h/h8199.md) thee in the [maqowm](../../strongs/h/h4725.md) where thou wast [bara'](../../strongs/h/h1254.md), in the ['erets](../../strongs/h/h776.md) of thy [mᵊḵôrâ](../../strongs/h/h4351.md).

<a name="ezekiel_21_31"></a>Ezekiel 21:31

And I will [šāp̄aḵ](../../strongs/h/h8210.md) mine [zaʿam](../../strongs/h/h2195.md) upon thee, I will [puwach](../../strongs/h/h6315.md) against thee in the ['esh](../../strongs/h/h784.md) of my ['ebrah](../../strongs/h/h5678.md), and [nathan](../../strongs/h/h5414.md) thee into the [yad](../../strongs/h/h3027.md) of [bāʿar](../../strongs/h/h1197.md) ['enowsh](../../strongs/h/h582.md), and [ḥārāš](../../strongs/h/h2796.md) to [mašḥîṯ](../../strongs/h/h4889.md).

<a name="ezekiel_21_32"></a>Ezekiel 21:32

Thou shalt be for ['oklah](../../strongs/h/h402.md) to the ['esh](../../strongs/h/h784.md); thy [dam](../../strongs/h/h1818.md) shall be in the midst of the ['erets](../../strongs/h/h776.md); thou shalt be no more [zakar](../../strongs/h/h2142.md): for I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) it.

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 20](ezekiel_20.md) - [Ezekiel 22](ezekiel_22.md)