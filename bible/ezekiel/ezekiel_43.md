# [Ezekiel 43](https://www.blueletterbible.org/kjv/ezekiel/43)

<a name="ezekiel_43_1"></a>Ezekiel 43:1

Afterward he [yālaḵ](../../strongs/h/h3212.md) me to the [sha'ar](../../strongs/h/h8179.md), even the [sha'ar](../../strongs/h/h8179.md) that [panah](../../strongs/h/h6437.md) [derek](../../strongs/h/h1870.md) the [qāḏîm](../../strongs/h/h6921.md):

<a name="ezekiel_43_2"></a>Ezekiel 43:2

And, behold, the [kabowd](../../strongs/h/h3519.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) from the [derek](../../strongs/h/h1870.md) of the [qāḏîm](../../strongs/h/h6921.md): and his [qowl](../../strongs/h/h6963.md) was like a [qowl](../../strongs/h/h6963.md) of [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md): and the ['erets](../../strongs/h/h776.md) ['owr](../../strongs/h/h215.md) with his [kabowd](../../strongs/h/h3519.md).

<a name="ezekiel_43_3"></a>Ezekiel 43:3

And it was according to the [mar'ê](../../strongs/h/h4758.md) of the [mar'ê](../../strongs/h/h4758.md) which I [ra'ah](../../strongs/h/h7200.md), even according to the [mar'ê](../../strongs/h/h4758.md) that I [ra'ah](../../strongs/h/h7200.md) when I [bow'](../../strongs/h/h935.md) to [shachath](../../strongs/h/h7843.md) the [ʿîr](../../strongs/h/h5892.md): and the [mar'â](../../strongs/h/h4759.md) were like the [mar'ê](../../strongs/h/h4758.md) that I [ra'ah](../../strongs/h/h7200.md) by the [nāhār](../../strongs/h/h5104.md) [kᵊḇār](../../strongs/h/h3529.md); and I [naphal](../../strongs/h/h5307.md) upon my [paniym](../../strongs/h/h6440.md).

<a name="ezekiel_43_4"></a>Ezekiel 43:4

And the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) by the [derek](../../strongs/h/h1870.md) of the [sha'ar](../../strongs/h/h8179.md) whose [paniym](../../strongs/h/h6440.md) is [derek](../../strongs/h/h1870.md) the [qāḏîm](../../strongs/h/h6921.md).

<a name="ezekiel_43_5"></a>Ezekiel 43:5

So the [ruwach](../../strongs/h/h7307.md) [nasa'](../../strongs/h/h5375.md) me, and [bow'](../../strongs/h/h935.md) me into the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md); and, behold, the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [mālā'](../../strongs/h/h4390.md) the [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_43_6"></a>Ezekiel 43:6

And I [shama'](../../strongs/h/h8085.md) him [dabar](../../strongs/h/h1696.md) unto me out of the [bayith](../../strongs/h/h1004.md); and the ['iysh](../../strongs/h/h376.md) ['amad](../../strongs/h/h5975.md) by me.

<a name="ezekiel_43_7"></a>Ezekiel 43:7

And he ['āmar](../../strongs/h/h559.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), the [maqowm](../../strongs/h/h4725.md) of my [kicce'](../../strongs/h/h3678.md), and the [maqowm](../../strongs/h/h4725.md) of the [kaph](../../strongs/h/h3709.md) of my [regel](../../strongs/h/h7272.md), where I will [shakan](../../strongs/h/h7931.md) in the [tavek](../../strongs/h/h8432.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['owlam](../../strongs/h/h5769.md), and my [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md), shall the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) no more [ṭāmē'](../../strongs/h/h2930.md), neither they, nor their [melek](../../strongs/h/h4428.md), by their [zᵊnûṯ](../../strongs/h/h2184.md), nor by the [peḡer](../../strongs/h/h6297.md) of their [melek](../../strongs/h/h4428.md) in their [bāmâ](../../strongs/h/h1116.md).

<a name="ezekiel_43_8"></a>Ezekiel 43:8

In their [nathan](../../strongs/h/h5414.md) of their [caph](../../strongs/h/h5592.md) by my [caph](../../strongs/h/h5592.md), and their [mᵊzûzâ](../../strongs/h/h4201.md) by my [mᵊzûzâ](../../strongs/h/h4201.md), and the [qîr](../../strongs/h/h7023.md) between me and them, they have even [ṭāmē'](../../strongs/h/h2930.md) my [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md) by their [tôʿēḇâ](../../strongs/h/h8441.md) that they have ['asah](../../strongs/h/h6213.md): wherefore I have [kalah](../../strongs/h/h3615.md) them in mine ['aph](../../strongs/h/h639.md).

<a name="ezekiel_43_9"></a>Ezekiel 43:9

Now let them put [rachaq](../../strongs/h/h7368.md) their [zᵊnûṯ](../../strongs/h/h2184.md), and the [peḡer](../../strongs/h/h6297.md) of their [melek](../../strongs/h/h4428.md), [rachaq](../../strongs/h/h7368.md) from me, and I will [shakan](../../strongs/h/h7931.md) in the [tavek](../../strongs/h/h8432.md) of them ['owlam](../../strongs/h/h5769.md).

<a name="ezekiel_43_10"></a>Ezekiel 43:10

Thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nāḡaḏ](../../strongs/h/h5046.md) the [bayith](../../strongs/h/h1004.md) to the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), that they may be [kālam](../../strongs/h/h3637.md) of their ['avon](../../strongs/h/h5771.md): and let them [māḏaḏ](../../strongs/h/h4058.md) the [tāḵnîṯ](../../strongs/h/h8508.md).

<a name="ezekiel_43_11"></a>Ezekiel 43:11

And if they be [kālam](../../strongs/h/h3637.md) of all that they have ['asah](../../strongs/h/h6213.md), [yada'](../../strongs/h/h3045.md) them the [ṣûrâ](../../strongs/h/h6699.md) of the [bayith](../../strongs/h/h1004.md), and the [tᵊḵûnâ](../../strongs/h/h8498.md) thereof, and the [môṣā'](../../strongs/h/h4161.md) thereof, and the [môḇā'](../../strongs/h/h4126.md) in thereof, and all the [ṣûrâ](../../strongs/h/h6699.md) thereof, and all the [chuqqah](../../strongs/h/h2708.md) thereof, and all the [ṣûrâ](../../strongs/h/h6699.md) thereof, and all the [towrah](../../strongs/h/h8451.md) thereof: and [kāṯaḇ](../../strongs/h/h3789.md) it in their ['ayin](../../strongs/h/h5869.md), that they may [shamar](../../strongs/h/h8104.md) the [ṣûrâ](../../strongs/h/h6699.md) thereof, and all the [chuqqah](../../strongs/h/h2708.md) thereof, and ['asah](../../strongs/h/h6213.md) them.

<a name="ezekiel_43_12"></a>Ezekiel 43:12

This is the [towrah](../../strongs/h/h8451.md) of the [bayith](../../strongs/h/h1004.md); Upon the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md) the [gᵊḇûl](../../strongs/h/h1366.md) thereof [cabiyb](../../strongs/h/h5439.md) shall be [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md). Behold, this is the [towrah](../../strongs/h/h8451.md) of the [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_43_13"></a>Ezekiel 43:13

And these are the [midâ](../../strongs/h/h4060.md) of the [mizbeach](../../strongs/h/h4196.md) after the ['ammâ](../../strongs/h/h520.md): The ['ammâ](../../strongs/h/h520.md) is an ['ammâ](../../strongs/h/h520.md) and an hand [ṭōp̄aḥ](../../strongs/h/h2948.md); even the [ḥêq](../../strongs/h/h2436.md) shall be an ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) an ['ammâ](../../strongs/h/h520.md), and the [gᵊḇûl](../../strongs/h/h1366.md) thereof by the [saphah](../../strongs/h/h8193.md) thereof [cabiyb](../../strongs/h/h5439.md) shall be a [zereṯ](../../strongs/h/h2239.md): and this shall be the higher [gaḇ](../../strongs/h/h1354.md) of the [mizbeach](../../strongs/h/h4196.md).

<a name="ezekiel_43_14"></a>Ezekiel 43:14

And from the [ḥêq](../../strongs/h/h2436.md) upon the ['erets](../../strongs/h/h776.md) even to the [taḥtôn](../../strongs/h/h8481.md) [ʿăzārâ](../../strongs/h/h5835.md) shall be two ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) one ['ammâ](../../strongs/h/h520.md); and from the [qāṭān](../../strongs/h/h6996.md) [ʿăzārâ](../../strongs/h/h5835.md) even to the [gadowl](../../strongs/h/h1419.md) [ʿăzārâ](../../strongs/h/h5835.md) shall be four ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) one ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_43_15"></a>Ezekiel 43:15

So the ['ări'êl](../../strongs/h/h741.md) [har'ēl](../../strongs/h/h2025.md) shall be four ['ammâ](../../strongs/h/h520.md); and from the ['ări'êl](../../strongs/h/h741.md) and [maʿal](../../strongs/h/h4605.md) shall be four [qeren](../../strongs/h/h7161.md).

<a name="ezekiel_43_16"></a>Ezekiel 43:16

And the ['ări'êl](../../strongs/h/h741.md) shall be twelve ['ōreḵ](../../strongs/h/h753.md), twelve [rōḥaḇ](../../strongs/h/h7341.md), [rāḇaʿ](../../strongs/h/h7251.md) in the four [reḇaʿ](../../strongs/h/h7253.md) thereof.

<a name="ezekiel_43_17"></a>Ezekiel 43:17

And the [ʿăzārâ](../../strongs/h/h5835.md) shall be fourteen ['ōreḵ](../../strongs/h/h753.md) and fourteen [rōḥaḇ](../../strongs/h/h7341.md) in the four [reḇaʿ](../../strongs/h/h7253.md) thereof; and the [gᵊḇûl](../../strongs/h/h1366.md) [cabiyb](../../strongs/h/h5439.md) it shall be [ḥēṣî](../../strongs/h/h2677.md) an ['ammâ](../../strongs/h/h520.md); and the [ḥêq](../../strongs/h/h2436.md) thereof shall be an ['ammâ](../../strongs/h/h520.md) [cabiyb](../../strongs/h/h5439.md); and his [maʿălâ](../../strongs/h/h4609.md) shall [panah](../../strongs/h/h6437.md) toward the [qāḏîm](../../strongs/h/h6921.md).

<a name="ezekiel_43_18"></a>Ezekiel 43:18

And he ['āmar](../../strongs/h/h559.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); These are the [chuqqah](../../strongs/h/h2708.md) of the [mizbeach](../../strongs/h/h4196.md) in the [yowm](../../strongs/h/h3117.md) when they shall ['asah](../../strongs/h/h6213.md) it, to [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) thereon, and to [zāraq](../../strongs/h/h2236.md) [dam](../../strongs/h/h1818.md) thereon.

<a name="ezekiel_43_19"></a>Ezekiel 43:19

And thou shalt [nathan](../../strongs/h/h5414.md) to the [kōhēn](../../strongs/h/h3548.md) the [Lᵊvî](../../strongs/h/h3881.md) that be of the [zera'](../../strongs/h/h2233.md) of [Ṣāḏôq](../../strongs/h/h6659.md), which [qarowb](../../strongs/h/h7138.md) unto me, to [sharath](../../strongs/h/h8334.md) unto me, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), a [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md) for a [chatta'ath](../../strongs/h/h2403.md).

<a name="ezekiel_43_20"></a>Ezekiel 43:20

And thou shalt [laqach](../../strongs/h/h3947.md) of the [dam](../../strongs/h/h1818.md) thereof, and [nathan](../../strongs/h/h5414.md) it on the four [qeren](../../strongs/h/h7161.md) of it, and on the four [pinnâ](../../strongs/h/h6438.md) of the [ʿăzārâ](../../strongs/h/h5835.md), and upon the [gᵊḇûl](../../strongs/h/h1366.md) [cabiyb](../../strongs/h/h5439.md): thus shalt thou [chata'](../../strongs/h/h2398.md) and [kāp̄ar](../../strongs/h/h3722.md) it.

<a name="ezekiel_43_21"></a>Ezekiel 43:21

Thou shalt [laqach](../../strongs/h/h3947.md) the [par](../../strongs/h/h6499.md) also of the [chatta'ath](../../strongs/h/h2403.md), and he shall [śārap̄](../../strongs/h/h8313.md) it in the [mip̄qāḏ](../../strongs/h/h4662.md) of the [bayith](../../strongs/h/h1004.md), [ḥûṣ](../../strongs/h/h2351.md) the [miqdash](../../strongs/h/h4720.md).

<a name="ezekiel_43_22"></a>Ezekiel 43:22

And on the second [yowm](../../strongs/h/h3117.md) thou shalt [qāraḇ](../../strongs/h/h7126.md) a [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) without [tamiym](../../strongs/h/h8549.md) for a [chatta'ath](../../strongs/h/h2403.md); and they shall [chata'](../../strongs/h/h2398.md) the [mizbeach](../../strongs/h/h4196.md), as they did [chata'](../../strongs/h/h2398.md) it with the [par](../../strongs/h/h6499.md).

<a name="ezekiel_43_23"></a>Ezekiel 43:23

When thou hast made a [kalah](../../strongs/h/h3615.md) of [chata'](../../strongs/h/h2398.md) it, thou shalt [qāraḇ](../../strongs/h/h7126.md) a [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md) without [tamiym](../../strongs/h/h8549.md), and an ['ayil](../../strongs/h/h352.md) out of the [tso'n](../../strongs/h/h6629.md) without [tamiym](../../strongs/h/h8549.md).

<a name="ezekiel_43_24"></a>Ezekiel 43:24

And thou shalt [qāraḇ](../../strongs/h/h7126.md) them [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and the [kōhēn](../../strongs/h/h3548.md) shall [shalak](../../strongs/h/h7993.md) [melaḥ](../../strongs/h/h4417.md) upon them, and they shall offer them [ʿālâ](../../strongs/h/h5927.md) for a [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_43_25"></a>Ezekiel 43:25

Seven [yowm](../../strongs/h/h3117.md) shalt thou ['asah](../../strongs/h/h6213.md) every [yowm](../../strongs/h/h3117.md) a [śāʿîr](../../strongs/h/h8163.md) for a sin [chatta'ath](../../strongs/h/h2403.md): they shall also ['asah](../../strongs/h/h6213.md) a [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), and an ['ayil](../../strongs/h/h352.md) out of the [tso'n](../../strongs/h/h6629.md), without [tamiym](../../strongs/h/h8549.md).

<a name="ezekiel_43_26"></a>Ezekiel 43:26

Seven [yowm](../../strongs/h/h3117.md) shall they [kāp̄ar](../../strongs/h/h3722.md) the [mizbeach](../../strongs/h/h4196.md) and [ṭāhēr](../../strongs/h/h2891.md) it; and they shall [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md).

<a name="ezekiel_43_27"></a>Ezekiel 43:27

And when these [yowm](../../strongs/h/h3117.md) are [kalah](../../strongs/h/h3615.md), it shall be, that upon the eighth [yowm](../../strongs/h/h3117.md), and so [hāl'â](../../strongs/h/h1973.md), the [kōhēn](../../strongs/h/h3548.md) shall ['asah](../../strongs/h/h6213.md) your [ʿōlâ](../../strongs/h/h5930.md) upon the [mizbeach](../../strongs/h/h4196.md), and your [šelem](../../strongs/h/h8002.md); and I will [ratsah](../../strongs/h/h7521.md) you, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 42](ezekiel_42.md) - [Ezekiel 44](ezekiel_44.md)