# [Ezekiel 48](https://www.blueletterbible.org/kjv/ezekiel/48)

<a name="ezekiel_48_1"></a>Ezekiel 48:1

Now these are the [shem](../../strongs/h/h8034.md) of the [shebet](../../strongs/h/h7626.md). From the [ṣāp̄ôn](../../strongs/h/h6828.md) [qāṣê](../../strongs/h/h7097.md) to the [yad](../../strongs/h/h3027.md) of the [derek](../../strongs/h/h1870.md) of [Ḥeṯlōn](../../strongs/h/h2855.md), as one [bow'](../../strongs/h/h935.md) to [Ḥămāṯ](../../strongs/h/h2574.md), [Ḥăṣar ʿÊnān](../../strongs/h/h2704.md), the [gᵊḇûl](../../strongs/h/h1366.md) of [Dammeśeq](../../strongs/h/h1834.md) [ṣāp̄ôn](../../strongs/h/h6828.md), to the [yad](../../strongs/h/h3027.md) of [Ḥămāṯ](../../strongs/h/h2574.md); for these are his [pē'â](../../strongs/h/h6285.md) [qāḏîm](../../strongs/h/h6921.md) and [yam](../../strongs/h/h3220.md); [Dān](../../strongs/h/h1835.md).

<a name="ezekiel_48_2"></a>Ezekiel 48:2

And by the [gᵊḇûl](../../strongs/h/h1366.md) of [Dān](../../strongs/h/h1835.md), from the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) unto the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md), ['Āšēr](../../strongs/h/h836.md).

<a name="ezekiel_48_3"></a>Ezekiel 48:3

And by the [gᵊḇûl](../../strongs/h/h1366.md) of ['Āšēr](../../strongs/h/h836.md), from the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) even unto the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md), [Nap̄tālî](../../strongs/h/h5321.md).

<a name="ezekiel_48_4"></a>Ezekiel 48:4

And by the [gᵊḇûl](../../strongs/h/h1366.md) of [Nap̄tālî](../../strongs/h/h5321.md), from the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) unto the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md), [Mᵊnaššê](../../strongs/h/h4519.md).

<a name="ezekiel_48_5"></a>Ezekiel 48:5

And by the [gᵊḇûl](../../strongs/h/h1366.md) of [Mᵊnaššê](../../strongs/h/h4519.md), from the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) unto the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md), ['Ep̄rayim](../../strongs/h/h669.md).

<a name="ezekiel_48_6"></a>Ezekiel 48:6

And by the [gᵊḇûl](../../strongs/h/h1366.md) of ['Ep̄rayim](../../strongs/h/h669.md), from the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) even unto the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md), [Rᵊ'ûḇēn](../../strongs/h/h7205.md).

<a name="ezekiel_48_7"></a>Ezekiel 48:7

And by the [gᵊḇûl](../../strongs/h/h1366.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), from the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) unto the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md), [Yehuwdah](../../strongs/h/h3063.md).

<a name="ezekiel_48_8"></a>Ezekiel 48:8

And by the [gᵊḇûl](../../strongs/h/h1366.md) of [Yehuwdah](../../strongs/h/h3063.md), from the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) unto the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md), shall be the [tᵊrûmâ](../../strongs/h/h8641.md) which ye shall [ruwm](../../strongs/h/h7311.md) of five and twenty thousand [rōḥaḇ](../../strongs/h/h7341.md), and in ['ōreḵ](../../strongs/h/h753.md) as one of the other [cheleq](../../strongs/h/h2506.md), from the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) unto the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md): and the [miqdash](../../strongs/h/h4720.md) shall be in the [tavek](../../strongs/h/h8432.md) of it.

<a name="ezekiel_48_9"></a>Ezekiel 48:9

The [tᵊrûmâ](../../strongs/h/h8641.md) that ye shall [ruwm](../../strongs/h/h7311.md) unto [Yĕhovah](../../strongs/h/h3068.md) shall be of five and twenty thousand in ['ōreḵ](../../strongs/h/h753.md), and of ten thousand in [rōḥaḇ](../../strongs/h/h7341.md).

<a name="ezekiel_48_10"></a>Ezekiel 48:10

And for them, even for the [kōhēn](../../strongs/h/h3548.md), shall be this [qodesh](../../strongs/h/h6944.md) [tᵊrûmâ](../../strongs/h/h8641.md); toward the [ṣāp̄ôn](../../strongs/h/h6828.md) five and twenty thousand in length, and toward the [yam](../../strongs/h/h3220.md) ten thousand in [rōḥaḇ](../../strongs/h/h7341.md), and toward the [qāḏîm](../../strongs/h/h6921.md) ten thousand in [rōḥaḇ](../../strongs/h/h7341.md), and toward the [neḡeḇ](../../strongs/h/h5045.md) five and twenty thousand in ['ōreḵ](../../strongs/h/h753.md): and the [miqdash](../../strongs/h/h4720.md) of [Yĕhovah](../../strongs/h/h3068.md) shall be in the [tavek](../../strongs/h/h8432.md) thereof.

<a name="ezekiel_48_11"></a>Ezekiel 48:11

It shall be for the [kōhēn](../../strongs/h/h3548.md) that are [qadash](../../strongs/h/h6942.md) of the [ben](../../strongs/h/h1121.md) of [Ṣāḏôq](../../strongs/h/h6659.md); which have [shamar](../../strongs/h/h8104.md) my [mišmereṯ](../../strongs/h/h4931.md), which went not [tāʿâ](../../strongs/h/h8582.md) when the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) went [tāʿâ](../../strongs/h/h8582.md), as the [Lᵊvî](../../strongs/h/h3881.md) went [tāʿâ](../../strongs/h/h8582.md).

<a name="ezekiel_48_12"></a>Ezekiel 48:12

And this [tᵊrûmîâ](../../strongs/h/h8642.md) of the ['erets](../../strongs/h/h776.md) that is [tᵊrûmâ](../../strongs/h/h8641.md) shall be unto them a thing [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) by the [gᵊḇûl](../../strongs/h/h1366.md) of the [Lᵊvî](../../strongs/h/h3881.md).

<a name="ezekiel_48_13"></a>Ezekiel 48:13

And over [ʿummâ](../../strongs/h/h5980.md) the [gᵊḇûl](../../strongs/h/h1366.md) of the [kōhēn](../../strongs/h/h3548.md) the [Lᵊvî](../../strongs/h/h3881.md) shall have five and twenty thousand in ['ōreḵ](../../strongs/h/h753.md), and ten thousand in [rōḥaḇ](../../strongs/h/h7341.md): all the ['ōreḵ](../../strongs/h/h753.md) shall be five and twenty thousand, and the [rōḥaḇ](../../strongs/h/h7341.md) ten thousand.

<a name="ezekiel_48_14"></a>Ezekiel 48:14

And they shall not [māḵar](../../strongs/h/h4376.md) of it, neither [mûr](../../strongs/h/h4171.md), nor ['abar](../../strongs/h/h5674.md) ['abar](../../strongs/h/h5674.md) the [re'shiyth](../../strongs/h/h7225.md) of the ['erets](../../strongs/h/h776.md): for it is [qodesh](../../strongs/h/h6944.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_48_15"></a>Ezekiel 48:15

And the five thousand, that are [yāṯar](../../strongs/h/h3498.md) in the [rōḥaḇ](../../strongs/h/h7341.md) over [paniym](../../strongs/h/h6440.md) the five and twenty thousand, shall be a [ḥōl](../../strongs/h/h2455.md) for the [ʿîr](../../strongs/h/h5892.md), for [môšāḇ](../../strongs/h/h4186.md), and for [miḡrāš](../../strongs/h/h4054.md): and the [ʿîr](../../strongs/h/h5892.md) shall be in the [tavek](../../strongs/h/h8432.md) thereof.

<a name="ezekiel_48_16"></a>Ezekiel 48:16

And these shall be the [midâ](../../strongs/h/h4060.md) thereof; the [ṣāp̄ôn](../../strongs/h/h6828.md) [pē'â](../../strongs/h/h6285.md) four thousand and five hundred, and the [neḡeḇ](../../strongs/h/h5045.md) [pē'â](../../strongs/h/h6285.md) four thousand and five hundred, and on the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) four thousand and five hundred, and the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md) four thousand and five hundred.

<a name="ezekiel_48_17"></a>Ezekiel 48:17

And the [miḡrāš](../../strongs/h/h4054.md) of the [ʿîr](../../strongs/h/h5892.md) shall be toward the [ṣāp̄ôn](../../strongs/h/h6828.md) two hundred and fifty, and toward the [neḡeḇ](../../strongs/h/h5045.md) two hundred and fifty, and toward the [qāḏîm](../../strongs/h/h6921.md) two hundred and fifty, and toward the [yam](../../strongs/h/h3220.md) two hundred and fifty.

<a name="ezekiel_48_18"></a>Ezekiel 48:18

And the [yāṯar](../../strongs/h/h3498.md) in ['ōreḵ](../../strongs/h/h753.md) over [ʿummâ](../../strongs/h/h5980.md) the [tᵊrûmâ](../../strongs/h/h8641.md) of the [qodesh](../../strongs/h/h6944.md) shall be ten thousand [qāḏîm](../../strongs/h/h6921.md), and ten thousand [yam](../../strongs/h/h3220.md): and it shall be over [ʿummâ](../../strongs/h/h5980.md) the [tᵊrûmâ](../../strongs/h/h8641.md) of the [qodesh](../../strongs/h/h6944.md) portion; and the [tᵊḇû'â](../../strongs/h/h8393.md) thereof shall be for [lechem](../../strongs/h/h3899.md) unto them that ['abad](../../strongs/h/h5647.md) the [ʿîr](../../strongs/h/h5892.md).

<a name="ezekiel_48_19"></a>Ezekiel 48:19

And they that ['abad](../../strongs/h/h5647.md) the [ʿîr](../../strongs/h/h5892.md) shall ['abad](../../strongs/h/h5647.md) it out of all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_48_20"></a>Ezekiel 48:20

All the [tᵊrûmâ](../../strongs/h/h8641.md) shall be five and twenty thousand by five and twenty thousand: ye shall [ruwm](../../strongs/h/h7311.md) the [qodesh](../../strongs/h/h6944.md) [tᵊrûmâ](../../strongs/h/h8641.md) foursquare, with the ['achuzzah](../../strongs/h/h272.md) of the [ʿîr](../../strongs/h/h5892.md).

<a name="ezekiel_48_21"></a>Ezekiel 48:21

And the [yāṯar](../../strongs/h/h3498.md) shall be for the [nāśî'](../../strongs/h/h5387.md), on the one side and on the other of the [qodesh](../../strongs/h/h6944.md) [tᵊrûmâ](../../strongs/h/h8641.md), and of the ['achuzzah](../../strongs/h/h272.md) of the [ʿîr](../../strongs/h/h5892.md), over [paniym](../../strongs/h/h6440.md) the five and twenty thousand of the [tᵊrûmâ](../../strongs/h/h8641.md) toward the [qāḏîm](../../strongs/h/h6921.md) [gᵊḇûl](../../strongs/h/h1366.md), and [yam](../../strongs/h/h3220.md) over [paniym](../../strongs/h/h6440.md) the five and twenty thousand toward the [yam](../../strongs/h/h3220.md) [gᵊḇûl](../../strongs/h/h1366.md), over [ʿummâ](../../strongs/h/h5980.md) the [cheleq](../../strongs/h/h2506.md) for the [nāśî'](../../strongs/h/h5387.md): and it shall be the [qodesh](../../strongs/h/h6944.md) [tᵊrûmâ](../../strongs/h/h8641.md); and the [miqdash](../../strongs/h/h4720.md) of the [bayith](../../strongs/h/h1004.md) shall be in the [tavek](../../strongs/h/h8432.md) thereof.

<a name="ezekiel_48_22"></a>Ezekiel 48:22

Moreover from the ['achuzzah](../../strongs/h/h272.md) of the [Lᵊvî](../../strongs/h/h3881.md), and from the ['achuzzah](../../strongs/h/h272.md) of the [ʿîr](../../strongs/h/h5892.md), being in the [tavek](../../strongs/h/h8432.md) of that which is the [nāśî'](../../strongs/h/h5387.md), between the [gᵊḇûl](../../strongs/h/h1366.md) of [Yehuwdah](../../strongs/h/h3063.md) and the [gᵊḇûl](../../strongs/h/h1366.md) of [Binyāmîn](../../strongs/h/h1144.md), shall be for the [nāśî'](../../strongs/h/h5387.md).

<a name="ezekiel_48_23"></a>Ezekiel 48:23

As for the [yeṯer](../../strongs/h/h3499.md) of the [shebet](../../strongs/h/h7626.md), from the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) unto the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md), [Binyāmîn](../../strongs/h/h1144.md) ['echad](../../strongs/h/h259.md).

<a name="ezekiel_48_24"></a>Ezekiel 48:24

And by the [gᵊḇûl](../../strongs/h/h1366.md) of [Binyāmîn](../../strongs/h/h1144.md), from the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) unto the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md), [Šimʿôn](../../strongs/h/h8095.md) ['echad](../../strongs/h/h259.md).

<a name="ezekiel_48_25"></a>Ezekiel 48:25

And by the [gᵊḇûl](../../strongs/h/h1366.md) of [Šimʿôn](../../strongs/h/h8095.md), from the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) unto the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md), [Yiśśāśḵār](../../strongs/h/h3485.md) ['echad](../../strongs/h/h259.md).

<a name="ezekiel_48_26"></a>Ezekiel 48:26

And by the [gᵊḇûl](../../strongs/h/h1366.md) of [Yiśśāśḵār](../../strongs/h/h3485.md), from the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) unto the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md), [Zᵊḇûlûn](../../strongs/h/h2074.md) ['echad](../../strongs/h/h259.md).

<a name="ezekiel_48_27"></a>Ezekiel 48:27

And by the [gᵊḇûl](../../strongs/h/h1366.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md), from the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) unto the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md), [Gāḏ](../../strongs/h/h1410.md) ['echad](../../strongs/h/h259.md).

<a name="ezekiel_48_28"></a>Ezekiel 48:28

And by the [gᵊḇûl](../../strongs/h/h1366.md) of [Gāḏ](../../strongs/h/h1410.md), at the [neḡeḇ](../../strongs/h/h5045.md) [pē'â](../../strongs/h/h6285.md) [têmān](../../strongs/h/h8486.md), the [gᵊḇûl](../../strongs/h/h1366.md) shall be even from [Tāmār](../../strongs/h/h8559.md) unto the [mayim](../../strongs/h/h4325.md) of [Mᵊrîḇâ](../../strongs/h/h4808.md) [Mᵊrîḇâ](../../strongs/h/h4809.md) in [Qāḏēš](../../strongs/h/h6946.md), and to the [nachal](../../strongs/h/h5158.md) toward the [gadowl](../../strongs/h/h1419.md) [yam](../../strongs/h/h3220.md).

<a name="ezekiel_48_29"></a>Ezekiel 48:29

This is the ['erets](../../strongs/h/h776.md) which ye shall [naphal](../../strongs/h/h5307.md) by lot unto the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) for [nachalah](../../strongs/h/h5159.md), and these are their [maḥălōqeṯ](../../strongs/h/h4256.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_48_30"></a>Ezekiel 48:30

And these are the [tôṣā'ôṯ](../../strongs/h/h8444.md) of the [ʿîr](../../strongs/h/h5892.md) on the [ṣāp̄ôn](../../strongs/h/h6828.md) [pē'â](../../strongs/h/h6285.md), four thousand and five hundred [midâ](../../strongs/h/h4060.md).

<a name="ezekiel_48_31"></a>Ezekiel 48:31

And the [sha'ar](../../strongs/h/h8179.md) of the [ʿîr](../../strongs/h/h5892.md) shall be after the [shem](../../strongs/h/h8034.md) of the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md): three [sha'ar](../../strongs/h/h8179.md) [ṣāp̄ôn](../../strongs/h/h6828.md); one [sha'ar](../../strongs/h/h8179.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), one [sha'ar](../../strongs/h/h8179.md) of [Yehuwdah](../../strongs/h/h3063.md), one [sha'ar](../../strongs/h/h8179.md) of [Lēvî](../../strongs/h/h3878.md).

<a name="ezekiel_48_32"></a>Ezekiel 48:32

And at the [qāḏîm](../../strongs/h/h6921.md) [pē'â](../../strongs/h/h6285.md) four thousand and five hundred: and three [sha'ar](../../strongs/h/h8179.md); and one [sha'ar](../../strongs/h/h8179.md) of [Yôsēp̄](../../strongs/h/h3130.md), one [sha'ar](../../strongs/h/h8179.md) of [Binyāmîn](../../strongs/h/h1144.md), one [sha'ar](../../strongs/h/h8179.md) of [Dān](../../strongs/h/h1835.md).

<a name="ezekiel_48_33"></a>Ezekiel 48:33

And at the [neḡeḇ](../../strongs/h/h5045.md) [pē'â](../../strongs/h/h6285.md) four thousand and five hundred [midâ](../../strongs/h/h4060.md): and three [sha'ar](../../strongs/h/h8179.md); one [sha'ar](../../strongs/h/h8179.md) of [Šimʿôn](../../strongs/h/h8095.md), one [sha'ar](../../strongs/h/h8179.md) of [Yiśśāśḵār](../../strongs/h/h3485.md), one [sha'ar](../../strongs/h/h8179.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md).

<a name="ezekiel_48_34"></a>Ezekiel 48:34

At the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md) four thousand and five hundred, with their three [sha'ar](../../strongs/h/h8179.md); one [sha'ar](../../strongs/h/h8179.md) of [Gāḏ](../../strongs/h/h1410.md), one [sha'ar](../../strongs/h/h8179.md) of ['Āšēr](../../strongs/h/h836.md), one [sha'ar](../../strongs/h/h8179.md) of [Nap̄tālî](../../strongs/h/h5321.md).

<a name="ezekiel_48_35"></a>Ezekiel 48:35

It was [cabiyb](../../strongs/h/h5439.md) eighteen thousand measures: and the [shem](../../strongs/h/h8034.md) of the [ʿîr](../../strongs/h/h5892.md) from that [yowm](../../strongs/h/h3117.md) shall be, [Yᵊhōvâ Šāmâ](../../strongs/h/h3074.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 47](ezekiel_47.md)