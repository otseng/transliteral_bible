# [Ezekiel 19](https://www.blueletterbible.org/kjv/ezekiel/19)

<a name="ezekiel_19_1"></a>Ezekiel 19:1

Moreover take thou [nasa'](../../strongs/h/h5375.md) a [qînâ](../../strongs/h/h7015.md) for the [nāśî'](../../strongs/h/h5387.md) of [Yisra'el](../../strongs/h/h3478.md),

<a name="ezekiel_19_2"></a>Ezekiel 19:2

And ['āmar](../../strongs/h/h559.md), What is thy ['em](../../strongs/h/h517.md)? A [lāḇî'](../../strongs/h/h3833.md): she [rāḇaṣ](../../strongs/h/h7257.md) among ['ariy](../../strongs/h/h738.md), she [rabah](../../strongs/h/h7235.md) her [gûr](../../strongs/h/h1482.md) among [kephiyr](../../strongs/h/h3715.md).

<a name="ezekiel_19_3"></a>Ezekiel 19:3

And she [ʿālâ](../../strongs/h/h5927.md) one of her [gûr](../../strongs/h/h1482.md): it became a [kephiyr](../../strongs/h/h3715.md), and it [lamad](../../strongs/h/h3925.md) to [taraph](../../strongs/h/h2963.md) the [ṭerep̄](../../strongs/h/h2964.md); it ['akal](../../strongs/h/h398.md) ['āḏām](../../strongs/h/h120.md).

<a name="ezekiel_19_4"></a>Ezekiel 19:4

The [gowy](../../strongs/h/h1471.md) also [shama'](../../strongs/h/h8085.md) of him; he was [tāp̄aś](../../strongs/h/h8610.md) in their [shachath](../../strongs/h/h7845.md), and they [bow'](../../strongs/h/h935.md) him with [ḥāḥ](../../strongs/h/h2397.md) unto the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="ezekiel_19_5"></a>Ezekiel 19:5

Now when she [ra'ah](../../strongs/h/h7200.md) that she had [yāḥal](../../strongs/h/h3176.md), and her [tiqvâ](../../strongs/h/h8615.md) was ['abad](../../strongs/h/h6.md), then she [laqach](../../strongs/h/h3947.md) another of her [gûr](../../strongs/h/h1482.md), and [śûm](../../strongs/h/h7760.md) him a [kephiyr](../../strongs/h/h3715.md).

<a name="ezekiel_19_6"></a>Ezekiel 19:6

And he [halak](../../strongs/h/h1980.md) among the ['ariy](../../strongs/h/h738.md), he became a [kephiyr](../../strongs/h/h3715.md), and [lamad](../../strongs/h/h3925.md) to [taraph](../../strongs/h/h2963.md) the [ṭerep̄](../../strongs/h/h2964.md), and ['akal](../../strongs/h/h398.md) ['āḏām](../../strongs/h/h120.md).

<a name="ezekiel_19_7"></a>Ezekiel 19:7

And he [yada'](../../strongs/h/h3045.md) their ['almānâ](../../strongs/h/h490.md), and he [ḥāraḇ](../../strongs/h/h2717.md) their [ʿîr](../../strongs/h/h5892.md); and the ['erets](../../strongs/h/h776.md) was [yāšam](../../strongs/h/h3456.md), and the [mᵊlō'](../../strongs/h/h4393.md) thereof, by the [qowl](../../strongs/h/h6963.md) of his [šᵊ'āḡâ](../../strongs/h/h7581.md).

<a name="ezekiel_19_8"></a>Ezekiel 19:8

Then the [gowy](../../strongs/h/h1471.md) [nathan](../../strongs/h/h5414.md) [cabiyb](../../strongs/h/h5439.md) him from the [mᵊḏînâ](../../strongs/h/h4082.md), and [pāraś](../../strongs/h/h6566.md) their [rešeṯ](../../strongs/h/h7568.md) over him: he was [tāp̄aś](../../strongs/h/h8610.md) in their [shachath](../../strongs/h/h7845.md).

<a name="ezekiel_19_9"></a>Ezekiel 19:9

And they [nathan](../../strongs/h/h5414.md) him in [sûḡar](../../strongs/h/h5474.md) in [ḥāḥ](../../strongs/h/h2397.md), and [bow'](../../strongs/h/h935.md) him to the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md): they [bow'](../../strongs/h/h935.md) him into [māṣôḏ](../../strongs/h/h4685.md), that his [qowl](../../strongs/h/h6963.md) should no more be [shama'](../../strongs/h/h8085.md) upon the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_19_10"></a>Ezekiel 19:10

Thy ['em](../../strongs/h/h517.md) is like a [gep̄en](../../strongs/h/h1612.md) in thy [dam](../../strongs/h/h1818.md) [dam](../../strongs/h/h1818.md), [šāṯal](../../strongs/h/h8362.md) by the [mayim](../../strongs/h/h4325.md): she was [parah](../../strongs/h/h6509.md) and full of [ʿānēp̄](../../strongs/h/h6058.md) by reason of [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md).

<a name="ezekiel_19_11"></a>Ezekiel 19:11

And she had ['oz](../../strongs/h/h5797.md) [maṭṭê](../../strongs/h/h4294.md) for the [shebet](../../strongs/h/h7626.md) of them that [mashal](../../strongs/h/h4910.md), and her [qômâ](../../strongs/h/h6967.md) was [gāḇah](../../strongs/h/h1361.md) among the thick [ʿăḇōṯ](../../strongs/h/h5688.md), and she [ra'ah](../../strongs/h/h7200.md) in her [gobahh](../../strongs/h/h1363.md) with the [rōḇ](../../strongs/h/h7230.md) of her [dālîṯ](../../strongs/h/h1808.md).

<a name="ezekiel_19_12"></a>Ezekiel 19:12

But she was [nathash](../../strongs/h/h5428.md) in [chemah](../../strongs/h/h2534.md), she was [shalak](../../strongs/h/h7993.md) to the ['erets](../../strongs/h/h776.md), and the [qāḏîm](../../strongs/h/h6921.md) [ruwach](../../strongs/h/h7307.md) [yāḇēš](../../strongs/h/h3001.md) her [pĕriy](../../strongs/h/h6529.md): her ['oz](../../strongs/h/h5797.md) [maṭṭê](../../strongs/h/h4294.md) were [paraq](../../strongs/h/h6561.md) and [yāḇēš](../../strongs/h/h3001.md); the ['esh](../../strongs/h/h784.md) ['akal](../../strongs/h/h398.md) them.

<a name="ezekiel_19_13"></a>Ezekiel 19:13

And now she is [šāṯal](../../strongs/h/h8362.md) in the [midbar](../../strongs/h/h4057.md), in a [ṣîyâ](../../strongs/h/h6723.md) and [ṣāmā'](../../strongs/h/h6772.md) ['erets](../../strongs/h/h776.md).

<a name="ezekiel_19_14"></a>Ezekiel 19:14

And ['esh](../../strongs/h/h784.md) is [yāṣā'](../../strongs/h/h3318.md) of a [maṭṭê](../../strongs/h/h4294.md) of her [baḏ](../../strongs/h/h905.md), which hath ['akal](../../strongs/h/h398.md) her [pĕriy](../../strongs/h/h6529.md), so that she hath no ['oz](../../strongs/h/h5797.md) [maṭṭê](../../strongs/h/h4294.md) to be a [shebet](../../strongs/h/h7626.md) to [mashal](../../strongs/h/h4910.md). This is a [qînâ](../../strongs/h/h7015.md), and shall be for a [qînâ](../../strongs/h/h7015.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 18](ezekiel_18.md) - [Ezekiel 20](ezekiel_20.md)