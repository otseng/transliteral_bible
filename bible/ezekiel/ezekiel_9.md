# [Ezekiel 9](https://www.blueletterbible.org/kjv/ezekiel/9)

<a name="ezekiel_9_1"></a>Ezekiel 9:1

He [qara'](../../strongs/h/h7121.md) also in mine ['ozen](../../strongs/h/h241.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md), ['āmar](../../strongs/h/h559.md), Cause them that have [pᵊqudâ](../../strongs/h/h6486.md) over the [ʿîr](../../strongs/h/h5892.md) to [qāraḇ](../../strongs/h/h7126.md), even every ['iysh](../../strongs/h/h376.md) with his [mašḥēṯ](../../strongs/h/h4892.md) [kĕliy](../../strongs/h/h3627.md) in his [yad](../../strongs/h/h3027.md).

<a name="ezekiel_9_2"></a>Ezekiel 9:2

And, behold, six ['enowsh](../../strongs/h/h582.md) [bow'](../../strongs/h/h935.md) from the [derek](../../strongs/h/h1870.md) of the ['elyown](../../strongs/h/h5945.md) [sha'ar](../../strongs/h/h8179.md), which [panah](../../strongs/h/h6437.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md), and every ['iysh](../../strongs/h/h376.md) a [mapāṣ](../../strongs/h/h4660.md) [kĕliy](../../strongs/h/h3627.md) in his [yad](../../strongs/h/h3027.md); and one ['iysh](../../strongs/h/h376.md) among them was [labash](../../strongs/h/h3847.md) with [baḏ](../../strongs/h/h906.md), with a [sāp̄ar](../../strongs/h/h5608.md) [qeseṯ](../../strongs/h/h7083.md) by his [māṯnayim](../../strongs/h/h4975.md): and they [bow'](../../strongs/h/h935.md), and ['amad](../../strongs/h/h5975.md) beside the [nᵊḥšeṯ](../../strongs/h/h5178.md) [mizbeach](../../strongs/h/h4196.md).

<a name="ezekiel_9_3"></a>Ezekiel 9:3

And the [kabowd](../../strongs/h/h3519.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) was [ʿālâ](../../strongs/h/h5927.md) from the [kĕruwb](../../strongs/h/h3742.md), whereupon he was, to the [mip̄tān](../../strongs/h/h4670.md) of the [bayith](../../strongs/h/h1004.md). And he [qara'](../../strongs/h/h7121.md) to the ['iysh](../../strongs/h/h376.md) [labash](../../strongs/h/h3847.md) with [baḏ](../../strongs/h/h906.md), which had the [sāp̄ar](../../strongs/h/h5608.md) [qeseṯ](../../strongs/h/h7083.md) by his [māṯnayim](../../strongs/h/h4975.md);

<a name="ezekiel_9_4"></a>Ezekiel 9:4

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, ['abar](../../strongs/h/h5674.md) the midst of the [ʿîr](../../strongs/h/h5892.md), through the midst of [Yĕruwshalaim](../../strongs/h/h3389.md), and [tāvâ](../../strongs/h/h8427.md) a [tāv](../../strongs/h/h8420.md) upon the [mēṣaḥ](../../strongs/h/h4696.md) of the ['enowsh](../../strongs/h/h582.md) that ['ānaḥ](../../strongs/h/h584.md) and that ['ānaq](../../strongs/h/h602.md) for all the [tôʿēḇâ](../../strongs/h/h8441.md) that be ['asah](../../strongs/h/h6213.md) in the midst thereof.

<a name="ezekiel_9_5"></a>Ezekiel 9:5

And to the others he ['āmar](../../strongs/h/h559.md) in mine ['ozen](../../strongs/h/h241.md), ['abar](../../strongs/h/h5674.md) ye ['aḥar](../../strongs/h/h310.md) him through the [ʿîr](../../strongs/h/h5892.md), and [nakah](../../strongs/h/h5221.md): let not your ['ayin](../../strongs/h/h5869.md) [ḥûs](../../strongs/h/h2347.md), neither have ye [ḥāmal](../../strongs/h/h2550.md):

<a name="ezekiel_9_6"></a>Ezekiel 9:6

[harag](../../strongs/h/h2026.md) [mašḥîṯ](../../strongs/h/h4889.md) [zāqēn](../../strongs/h/h2205.md) and [bāḥûr](../../strongs/h/h970.md), both [bᵊṯûlâ](../../strongs/h/h1330.md), and [ṭap̄](../../strongs/h/h2945.md), and ['ishshah](../../strongs/h/h802.md): but [nāḡaš](../../strongs/h/h5066.md) not any ['iysh](../../strongs/h/h376.md) upon whom is the [tāv](../../strongs/h/h8420.md); and [ḥālal](../../strongs/h/h2490.md) at my [miqdash](../../strongs/h/h4720.md). Then they [ḥālal](../../strongs/h/h2490.md) at the [zāqēn](../../strongs/h/h2205.md) ['enowsh](../../strongs/h/h582.md) which were [paniym](../../strongs/h/h6440.md) the [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_9_7"></a>Ezekiel 9:7

And he ['āmar](../../strongs/h/h559.md) unto them, [ṭāmē'](../../strongs/h/h2930.md) the [bayith](../../strongs/h/h1004.md), and [mālā'](../../strongs/h/h4390.md) the [ḥāṣēr](../../strongs/h/h2691.md) with the [ḥālāl](../../strongs/h/h2491.md): [yāṣā'](../../strongs/h/h3318.md) ye. And they [yāṣā'](../../strongs/h/h3318.md), and [nakah](../../strongs/h/h5221.md) in the [ʿîr](../../strongs/h/h5892.md).

<a name="ezekiel_9_8"></a>Ezekiel 9:8

And it came to pass, while they were [nakah](../../strongs/h/h5221.md) them, and I was [šā'ar](../../strongs/h/h7604.md), that I [naphal](../../strongs/h/h5307.md) upon my [paniym](../../strongs/h/h6440.md), and [zāʿaq](../../strongs/h/h2199.md), and ['āmar](../../strongs/h/h559.md), ['ăhâ](../../strongs/h/h162.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md)! wilt thou [shachath](../../strongs/h/h7843.md) all the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yisra'el](../../strongs/h/h3478.md) in thy [šāp̄aḵ](../../strongs/h/h8210.md) of thy [chemah](../../strongs/h/h2534.md) upon [Yĕruwshalaim](../../strongs/h/h3389.md)?

<a name="ezekiel_9_9"></a>Ezekiel 9:9

Then ['āmar](../../strongs/h/h559.md) he unto me, The ['avon](../../strongs/h/h5771.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) and [Yehuwdah](../../strongs/h/h3063.md) is [me'od](../../strongs/h/h3966.md) [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md), and the ['erets](../../strongs/h/h776.md) is [mālā'](../../strongs/h/h4390.md) of [dam](../../strongs/h/h1818.md), and the [ʿîr](../../strongs/h/h5892.md) [mālā'](../../strongs/h/h4390.md) of [muṭṭê](../../strongs/h/h4297.md): for they ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) hath ['azab](../../strongs/h/h5800.md) the ['erets](../../strongs/h/h776.md), and [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) not.

<a name="ezekiel_9_10"></a>Ezekiel 9:10

And as for me also, mine ['ayin](../../strongs/h/h5869.md) shall not [ḥûs](../../strongs/h/h2347.md), neither will I have [ḥāmal](../../strongs/h/h2550.md), but I will [nathan](../../strongs/h/h5414.md) their [derek](../../strongs/h/h1870.md) upon their [ro'sh](../../strongs/h/h7218.md).

<a name="ezekiel_9_11"></a>Ezekiel 9:11

And, behold, the ['iysh](../../strongs/h/h376.md) [labash](../../strongs/h/h3847.md) with [baḏ](../../strongs/h/h906.md), which had the [qeseṯ](../../strongs/h/h7083.md) by his [māṯnayim](../../strongs/h/h4975.md), [shuwb](../../strongs/h/h7725.md) the [dabar](../../strongs/h/h1697.md), ['āmar](../../strongs/h/h559.md), I have ['asah](../../strongs/h/h6213.md) as thou hast [tsavah](../../strongs/h/h6680.md) me.

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 8](ezekiel_8.md) - [Ezekiel 10](ezekiel_10.md)