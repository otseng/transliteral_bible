# [Ezekiel 40](https://www.blueletterbible.org/kjv/ezekiel/40)

<a name="ezekiel_40_1"></a>Ezekiel 40:1

In the five and twentieth [šānâ](../../strongs/h/h8141.md) of our [gālûṯ](../../strongs/h/h1546.md), in the [ro'sh](../../strongs/h/h7218.md) of the [šānâ](../../strongs/h/h8141.md), in the tenth day of the [ḥōḏeš](../../strongs/h/h2320.md), in the fourteenth [šānâ](../../strongs/h/h8141.md) ['aḥar](../../strongs/h/h310.md) that the [ʿîr](../../strongs/h/h5892.md) was [nakah](../../strongs/h/h5221.md), in the ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md) the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) was upon me, and [bow'](../../strongs/h/h935.md) me thither.

<a name="ezekiel_40_2"></a>Ezekiel 40:2

In the [mar'â](../../strongs/h/h4759.md) of ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) he me into the ['erets](../../strongs/h/h776.md) of [Yisra'el](../../strongs/h/h3478.md), and [nuwach](../../strongs/h/h5117.md) me upon a [me'od](../../strongs/h/h3966.md) [gāḇōha](../../strongs/h/h1364.md) [har](../../strongs/h/h2022.md), by which was as the [miḇnê](../../strongs/h/h4011.md) of a [ʿîr](../../strongs/h/h5892.md) on the [neḡeḇ](../../strongs/h/h5045.md).

<a name="ezekiel_40_3"></a>Ezekiel 40:3

And he [bow'](../../strongs/h/h935.md) me thither, and, behold, there was an ['iysh](../../strongs/h/h376.md), whose [mar'ê](../../strongs/h/h4758.md) was like the [mar'ê](../../strongs/h/h4758.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md), with a [pāṯîl](../../strongs/h/h6616.md) of [pēšeṯ](../../strongs/h/h6593.md) in his [yad](../../strongs/h/h3027.md), and a [midâ](../../strongs/h/h4060.md) [qānê](../../strongs/h/h7070.md); and he ['amad](../../strongs/h/h5975.md) in the [sha'ar](../../strongs/h/h8179.md).

<a name="ezekiel_40_4"></a>Ezekiel 40:4

And the ['iysh](../../strongs/h/h376.md) [dabar](../../strongs/h/h1696.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [ra'ah](../../strongs/h/h7200.md) with thine ['ayin](../../strongs/h/h5869.md), and [shama'](../../strongs/h/h8085.md) with thine ['ozen](../../strongs/h/h241.md), and [śûm](../../strongs/h/h7760.md) thine [leb](../../strongs/h/h3820.md) upon all that I shall [ra'ah](../../strongs/h/h7200.md) thee; for to the intent that I might [ra'ah](../../strongs/h/h7200.md) them unto thee art thou [bow'](../../strongs/h/h935.md) hither: [nāḡaḏ](../../strongs/h/h5046.md) all that thou [ra'ah](../../strongs/h/h7200.md) to the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_40_5"></a>Ezekiel 40:5

And behold a [ḥômâ](../../strongs/h/h2346.md) on the [ḥûṣ](../../strongs/h/h2351.md) of the [bayith](../../strongs/h/h1004.md) [cabiyb](../../strongs/h/h5439.md), and in the ['iysh](../../strongs/h/h376.md) [yad](../../strongs/h/h3027.md) a [midâ](../../strongs/h/h4060.md) [qānê](../../strongs/h/h7070.md) of six ['ammâ](../../strongs/h/h520.md) long by the ['ammâ](../../strongs/h/h520.md) and an [ṭōp̄aḥ](../../strongs/h/h2948.md): so he [māḏaḏ](../../strongs/h/h4058.md) the [rōḥaḇ](../../strongs/h/h7341.md) of the [binyān](../../strongs/h/h1146.md), one [qānê](../../strongs/h/h7070.md); and the [qômâ](../../strongs/h/h6967.md), one [qānê](../../strongs/h/h7070.md).

<a name="ezekiel_40_6"></a>Ezekiel 40:6

Then [bow'](../../strongs/h/h935.md) he unto the [sha'ar](../../strongs/h/h8179.md) which [paniym](../../strongs/h/h6440.md) [derek](../../strongs/h/h1870.md) the [qāḏîm](../../strongs/h/h6921.md), and [ʿālâ](../../strongs/h/h5927.md) the [maʿălâ](../../strongs/h/h4609.md) thereof, and [māḏaḏ](../../strongs/h/h4058.md) the [caph](../../strongs/h/h5592.md) of the [sha'ar](../../strongs/h/h8179.md), which was one [qānê](../../strongs/h/h7070.md) [rōḥaḇ](../../strongs/h/h7341.md); and the other [caph](../../strongs/h/h5592.md), which was one [qānê](../../strongs/h/h7070.md) [rōḥaḇ](../../strongs/h/h7341.md).

<a name="ezekiel_40_7"></a>Ezekiel 40:7

And every [tā'](../../strongs/h/h8372.md) was one [qānê](../../strongs/h/h7070.md) ['ōreḵ](../../strongs/h/h753.md), and one [qānê](../../strongs/h/h7070.md) [rōḥaḇ](../../strongs/h/h7341.md); and between the [tā'](../../strongs/h/h8372.md) were five ['ammâ](../../strongs/h/h520.md); and the [caph](../../strongs/h/h5592.md) of the [sha'ar](../../strongs/h/h8179.md) by the ['ûlām](../../strongs/h/h197.md) of the [sha'ar](../../strongs/h/h8179.md) [bayith](../../strongs/h/h1004.md) was one [qānê](../../strongs/h/h7070.md).

<a name="ezekiel_40_8"></a>Ezekiel 40:8

He [māḏaḏ](../../strongs/h/h4058.md) also the ['ûlām](../../strongs/h/h197.md) of the [sha'ar](../../strongs/h/h8179.md) [bayith](../../strongs/h/h1004.md), one [qānê](../../strongs/h/h7070.md).

<a name="ezekiel_40_9"></a>Ezekiel 40:9

Then [māḏaḏ](../../strongs/h/h4058.md) he the ['ûlām](../../strongs/h/h197.md) of the [sha'ar](../../strongs/h/h8179.md), eight ['ammâ](../../strongs/h/h520.md); and the ['ayil](../../strongs/h/h352.md) thereof, two ['ammâ](../../strongs/h/h520.md); and the ['ûlām](../../strongs/h/h197.md) of the [sha'ar](../../strongs/h/h8179.md) was [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_40_10"></a>Ezekiel 40:10

And the [tā'](../../strongs/h/h8372.md) of the [sha'ar](../../strongs/h/h8179.md) [derek](../../strongs/h/h1870.md) [qāḏîm](../../strongs/h/h6921.md) were three on this side, and three on that side; they three were of one [midâ](../../strongs/h/h4060.md): and the ['ayil](../../strongs/h/h352.md) had one [midâ](../../strongs/h/h4060.md) on this side and on that side.

<a name="ezekiel_40_11"></a>Ezekiel 40:11

And he [māḏaḏ](../../strongs/h/h4058.md) the [rōḥaḇ](../../strongs/h/h7341.md) of the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md), ten ['ammâ](../../strongs/h/h520.md); and the ['ōreḵ](../../strongs/h/h753.md) of the [sha'ar](../../strongs/h/h8179.md), thirteen ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_40_12"></a>Ezekiel 40:12

The [gᵊḇûl](../../strongs/h/h1366.md) also [paniym](../../strongs/h/h6440.md) the [tā'](../../strongs/h/h8372.md) was one ['ammâ](../../strongs/h/h520.md) on this side, and the [gᵊḇûl](../../strongs/h/h1366.md) was one ['ammâ](../../strongs/h/h520.md) on that side: and the [tā'](../../strongs/h/h8372.md) were six ['ammâ](../../strongs/h/h520.md) on this side, and six ['ammâ](../../strongs/h/h520.md) on that side.

<a name="ezekiel_40_13"></a>Ezekiel 40:13

He [māḏaḏ](../../strongs/h/h4058.md) then the [sha'ar](../../strongs/h/h8179.md) from the [gāḡ](../../strongs/h/h1406.md) of one [tā'](../../strongs/h/h8372.md) to the [gāḡ](../../strongs/h/h1406.md) of another: the [rōḥaḇ](../../strongs/h/h7341.md) was five and twenty ['ammâ](../../strongs/h/h520.md), [peṯaḥ](../../strongs/h/h6607.md) against [peṯaḥ](../../strongs/h/h6607.md).

<a name="ezekiel_40_14"></a>Ezekiel 40:14

He ['asah](../../strongs/h/h6213.md) also ['ayil](../../strongs/h/h352.md) of threescore ['ammâ](../../strongs/h/h520.md), even unto the ['ayil](../../strongs/h/h352.md) of the [ḥāṣēr](../../strongs/h/h2691.md) [cabiyb](../../strongs/h/h5439.md) the [sha'ar](../../strongs/h/h8179.md).

<a name="ezekiel_40_15"></a>Ezekiel 40:15

And from the [paniym](../../strongs/h/h6440.md) of the [sha'ar](../../strongs/h/h8179.md) of the [y'iṯôn](../../strongs/h/h2978.md) unto the [paniym](../../strongs/h/h6440.md) of the ['ûlām](../../strongs/h/h197.md) of the [pᵊnîmî](../../strongs/h/h6442.md) [sha'ar](../../strongs/h/h8179.md) were fifty ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_40_16"></a>Ezekiel 40:16

And there were ['āṭam](../../strongs/h/h331.md) [ḥallôn](../../strongs/h/h2474.md) to the [tā'](../../strongs/h/h8372.md), and to their ['ayil](../../strongs/h/h352.md) [pᵊnîmâ](../../strongs/h/h6441.md) the [sha'ar](../../strongs/h/h8179.md) [cabiyb](../../strongs/h/h5439.md), and likewise to the ['êlām](../../strongs/h/h361.md): and [ḥallôn](../../strongs/h/h2474.md) were [cabiyb](../../strongs/h/h5439.md) [pᵊnîmâ](../../strongs/h/h6441.md): and upon each ['ayil](../../strongs/h/h352.md) were [timmōrâ](../../strongs/h/h8561.md).

<a name="ezekiel_40_17"></a>Ezekiel 40:17

Then [bow'](../../strongs/h/h935.md) he me into the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md), and, lo, there were [liškâ](../../strongs/h/h3957.md), and a [ritspah](../../strongs/h/h7531.md) ['asah](../../strongs/h/h6213.md) for the [ḥāṣēr](../../strongs/h/h2691.md) [cabiyb](../../strongs/h/h5439.md): thirty [liškâ](../../strongs/h/h3957.md) were upon the [ritspah](../../strongs/h/h7531.md).

<a name="ezekiel_40_18"></a>Ezekiel 40:18

And the [ritspah](../../strongs/h/h7531.md) by the [kāṯēp̄](../../strongs/h/h3802.md) of the [sha'ar](../../strongs/h/h8179.md) over [ʿummâ](../../strongs/h/h5980.md) the ['ōreḵ](../../strongs/h/h753.md) of the [sha'ar](../../strongs/h/h8179.md) was the [taḥtôn](../../strongs/h/h8481.md) [ritspah](../../strongs/h/h7531.md).

<a name="ezekiel_40_19"></a>Ezekiel 40:19

Then he [māḏaḏ](../../strongs/h/h4058.md) the [rōḥaḇ](../../strongs/h/h7341.md) from the [paniym](../../strongs/h/h6440.md) of the [taḥtôn](../../strongs/h/h8481.md) [sha'ar](../../strongs/h/h8179.md) unto the [paniym](../../strongs/h/h6440.md) of the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md) [ḥûṣ](../../strongs/h/h2351.md), an hundred ['ammâ](../../strongs/h/h520.md) [qāḏîm](../../strongs/h/h6921.md) and [ṣāp̄ôn](../../strongs/h/h6828.md).

<a name="ezekiel_40_20"></a>Ezekiel 40:20

And the [sha'ar](../../strongs/h/h8179.md) of the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md) that [paniym](../../strongs/h/h6440.md) [derek](../../strongs/h/h1870.md) the [ṣāp̄ôn](../../strongs/h/h6828.md), he [māḏaḏ](../../strongs/h/h4058.md) the ['ōreḵ](../../strongs/h/h753.md) thereof, and the [rōḥaḇ](../../strongs/h/h7341.md) thereof.

<a name="ezekiel_40_21"></a>Ezekiel 40:21

And the [tā'](../../strongs/h/h8372.md) thereof were three on this side and three on that side; and the ['ayil](../../strongs/h/h352.md) thereof and the ['êlām](../../strongs/h/h361.md) thereof were after the [midâ](../../strongs/h/h4060.md) of the [ri'šôn](../../strongs/h/h7223.md) [sha'ar](../../strongs/h/h8179.md): the ['ōreḵ](../../strongs/h/h753.md) thereof was fifty ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) five and twenty ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_40_22"></a>Ezekiel 40:22

And their [ḥallôn](../../strongs/h/h2474.md), and their ['êlām](../../strongs/h/h361.md), and their [timmōrâ](../../strongs/h/h8561.md), were after the [midâ](../../strongs/h/h4060.md) of the [sha'ar](../../strongs/h/h8179.md) that [paniym](../../strongs/h/h6440.md) [derek](../../strongs/h/h1870.md) the [qāḏîm](../../strongs/h/h6921.md); and they [ʿālâ](../../strongs/h/h5927.md) unto it by seven [maʿălâ](../../strongs/h/h4609.md); and the ['êlām](../../strongs/h/h361.md) thereof were [paniym](../../strongs/h/h6440.md) them.

<a name="ezekiel_40_23"></a>Ezekiel 40:23

And the [sha'ar](../../strongs/h/h8179.md) of the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md) was over against the [sha'ar](../../strongs/h/h8179.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md), and toward the [qāḏîm](../../strongs/h/h6921.md); and he [māḏaḏ](../../strongs/h/h4058.md) from [sha'ar](../../strongs/h/h8179.md) to [sha'ar](../../strongs/h/h8179.md) an hundred ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_40_24"></a>Ezekiel 40:24

After that he [yālaḵ](../../strongs/h/h3212.md) me [derek](../../strongs/h/h1870.md) the [dārôm](../../strongs/h/h1864.md), and behold a [sha'ar](../../strongs/h/h8179.md) [derek](../../strongs/h/h1870.md) the [dārôm](../../strongs/h/h1864.md): and he [māḏaḏ](../../strongs/h/h4058.md) the ['ayil](../../strongs/h/h352.md) thereof and the ['êlām](../../strongs/h/h361.md) thereof according to these [midâ](../../strongs/h/h4060.md).

<a name="ezekiel_40_25"></a>Ezekiel 40:25

And there were [ḥallôn](../../strongs/h/h2474.md) in it and in the ['êlām](../../strongs/h/h361.md) thereof [cabiyb](../../strongs/h/h5439.md), like those [ḥallôn](../../strongs/h/h2474.md): the ['ōreḵ](../../strongs/h/h753.md) was fifty ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) five and twenty ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_40_26"></a>Ezekiel 40:26

And there were seven [maʿălâ](../../strongs/h/h4609.md) to [ʿōlâ](../../strongs/h/h5930.md) to it, and the ['êlām](../../strongs/h/h361.md) thereof were [paniym](../../strongs/h/h6440.md) them: and it had [timmōrâ](../../strongs/h/h8561.md), one on this side, and another on that side, upon the ['ayil](../../strongs/h/h352.md) thereof.

<a name="ezekiel_40_27"></a>Ezekiel 40:27

And there was a [sha'ar](../../strongs/h/h8179.md) in the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md) [derek](../../strongs/h/h1870.md) the [dārôm](../../strongs/h/h1864.md): and he [māḏaḏ](../../strongs/h/h4058.md) from [sha'ar](../../strongs/h/h8179.md) to [sha'ar](../../strongs/h/h8179.md) [derek](../../strongs/h/h1870.md) the [dārôm](../../strongs/h/h1864.md) an hundred ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_40_28"></a>Ezekiel 40:28

And he [bow'](../../strongs/h/h935.md) me to the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md) by the [dārôm](../../strongs/h/h1864.md) [sha'ar](../../strongs/h/h8179.md): and he [māḏaḏ](../../strongs/h/h4058.md) the [dārôm](../../strongs/h/h1864.md) [sha'ar](../../strongs/h/h8179.md) according to these [midâ](../../strongs/h/h4060.md);

<a name="ezekiel_40_29"></a>Ezekiel 40:29

And the [tā'](../../strongs/h/h8372.md) thereof, and the ['ayil](../../strongs/h/h352.md) thereof, and the ['êlām](../../strongs/h/h361.md) thereof, according to these [midâ](../../strongs/h/h4060.md): and there were [ḥallôn](../../strongs/h/h2474.md) in it and in the ['êlām](../../strongs/h/h361.md) thereof [cabiyb](../../strongs/h/h5439.md): it was fifty ['ammâ](../../strongs/h/h520.md) ['ōreḵ](../../strongs/h/h753.md), and five and twenty ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md).

<a name="ezekiel_40_30"></a>Ezekiel 40:30

And the ['êlām](../../strongs/h/h361.md) [cabiyb](../../strongs/h/h5439.md) were five and twenty ['ammâ](../../strongs/h/h520.md) ['ōreḵ](../../strongs/h/h753.md), and five ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md).

<a name="ezekiel_40_31"></a>Ezekiel 40:31

And the ['êlām](../../strongs/h/h361.md) thereof were toward the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md); and [timmōrâ](../../strongs/h/h8561.md) were upon the ['ayil](../../strongs/h/h352.md) thereof: and the [maʿălê](../../strongs/h/h4608.md) to it had eight [maʿălâ](../../strongs/h/h4609.md).

<a name="ezekiel_40_32"></a>Ezekiel 40:32

And he [bow'](../../strongs/h/h935.md) me into the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md) [derek](../../strongs/h/h1870.md) the [qāḏîm](../../strongs/h/h6921.md): and he [māḏaḏ](../../strongs/h/h4058.md) the [sha'ar](../../strongs/h/h8179.md) according to these [midâ](../../strongs/h/h4060.md).

<a name="ezekiel_40_33"></a>Ezekiel 40:33

And the [tā'](../../strongs/h/h8372.md) thereof, and the ['ayil](../../strongs/h/h352.md) thereof, and the ['êlām](../../strongs/h/h361.md) thereof, were according to these [midâ](../../strongs/h/h4060.md): and there were [ḥallôn](../../strongs/h/h2474.md) therein and in the ['êlām](../../strongs/h/h361.md) thereof [cabiyb](../../strongs/h/h5439.md): it was fifty ['ammâ](../../strongs/h/h520.md) ['ōreḵ](../../strongs/h/h753.md), and five and twenty ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md).

<a name="ezekiel_40_34"></a>Ezekiel 40:34

And the ['êlām](../../strongs/h/h361.md) thereof were toward the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md); and [timmōrâ](../../strongs/h/h8561.md) were upon the ['ayil](../../strongs/h/h352.md) thereof, on this side, and on that side: and the [maʿălê](../../strongs/h/h4608.md) to it had eight [maʿălâ](../../strongs/h/h4609.md).

<a name="ezekiel_40_35"></a>Ezekiel 40:35

And he [bow'](../../strongs/h/h935.md) me to the [ṣāp̄ôn](../../strongs/h/h6828.md) [sha'ar](../../strongs/h/h8179.md), and [māḏaḏ](../../strongs/h/h4058.md) it according to these [midâ](../../strongs/h/h4060.md);

<a name="ezekiel_40_36"></a>Ezekiel 40:36

The [tā'](../../strongs/h/h8372.md) thereof, the ['ayil](../../strongs/h/h352.md) thereof, and the ['êlām](../../strongs/h/h361.md) thereof, and the [ḥallôn](../../strongs/h/h2474.md) to it [cabiyb](../../strongs/h/h5439.md): the ['ōreḵ](../../strongs/h/h753.md) was fifty ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) five and twenty ['ammâ](../../strongs/h/h520.md).

<a name="ezekiel_40_37"></a>Ezekiel 40:37

And the ['ayil](../../strongs/h/h352.md) thereof were toward the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md); and [timmōrâ](../../strongs/h/h8561.md) were upon the ['ayil](../../strongs/h/h352.md) thereof, on this side, and on that side: and the [maʿălê](../../strongs/h/h4608.md) to it had eight [maʿălâ](../../strongs/h/h4609.md).

<a name="ezekiel_40_38"></a>Ezekiel 40:38

And the [liškâ](../../strongs/h/h3957.md) and the [peṯaḥ](../../strongs/h/h6607.md) thereof were by the ['ayil](../../strongs/h/h352.md) of the [sha'ar](../../strongs/h/h8179.md), where they [dûaḥ](../../strongs/h/h1740.md) the [ʿōlâ](../../strongs/h/h5930.md).

<a name="ezekiel_40_39"></a>Ezekiel 40:39

And in the ['ûlām](../../strongs/h/h197.md) of the [sha'ar](../../strongs/h/h8179.md) were two [šulḥān](../../strongs/h/h7979.md) on this side, and two [šulḥān](../../strongs/h/h7979.md) on that side, to [šāḥaṭ](../../strongs/h/h7819.md) thereon the [ʿōlâ](../../strongs/h/h5930.md) and the sin [chatta'ath](../../strongs/h/h2403.md) and the ['āšām](../../strongs/h/h817.md).

<a name="ezekiel_40_40"></a>Ezekiel 40:40

And at the [kāṯēp̄](../../strongs/h/h3802.md) [ḥûṣ](../../strongs/h/h2351.md), as one [ʿālâ](../../strongs/h/h5927.md) to the [peṯaḥ](../../strongs/h/h6607.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md) [sha'ar](../../strongs/h/h8179.md), were two [šulḥān](../../strongs/h/h7979.md); and on the ['aḥēr](../../strongs/h/h312.md) [kāṯēp̄](../../strongs/h/h3802.md), which was at the ['ûlām](../../strongs/h/h197.md) of the [sha'ar](../../strongs/h/h8179.md), were two [šulḥān](../../strongs/h/h7979.md).

<a name="ezekiel_40_41"></a>Ezekiel 40:41

Four [šulḥān](../../strongs/h/h7979.md) were on this side, and four [šulḥān](../../strongs/h/h7979.md) on that side, by the [kāṯēp̄](../../strongs/h/h3802.md) of the [sha'ar](../../strongs/h/h8179.md); eight [šulḥān](../../strongs/h/h7979.md), whereupon they [šāḥaṭ](../../strongs/h/h7819.md).

<a name="ezekiel_40_42"></a>Ezekiel 40:42

And the four [šulḥān](../../strongs/h/h7979.md) were of [gāzîṯ](../../strongs/h/h1496.md) ['eben](../../strongs/h/h68.md) for the [ʿōlâ](../../strongs/h/h5930.md), of an ['ammâ](../../strongs/h/h520.md) and an [ḥēṣî](../../strongs/h/h2677.md) ['ōreḵ](../../strongs/h/h753.md), and an ['ammâ](../../strongs/h/h520.md) and an [ḥēṣî](../../strongs/h/h2677.md) [rōḥaḇ](../../strongs/h/h7341.md), and one ['ammâ](../../strongs/h/h520.md) [gobahh](../../strongs/h/h1363.md): whereupon also they [yānaḥ](../../strongs/h/h3240.md) the [kĕliy](../../strongs/h/h3627.md) wherewith they [šāḥaṭ](../../strongs/h/h7819.md) the [ʿōlâ](../../strongs/h/h5930.md) and the [zebach](../../strongs/h/h2077.md).

<a name="ezekiel_40_43"></a>Ezekiel 40:43

And [bayith](../../strongs/h/h1004.md) were [šᵊp̄atayim](../../strongs/h/h8240.md), an [ṭōp̄aḥ](../../strongs/h/h2948.md) broad, [kuwn](../../strongs/h/h3559.md) [cabiyb](../../strongs/h/h5439.md): and upon the [šulḥān](../../strongs/h/h7979.md) was the [basar](../../strongs/h/h1320.md) of the [qorban](../../strongs/h/h7133.md).

<a name="ezekiel_40_44"></a>Ezekiel 40:44

And [ḥûṣ](../../strongs/h/h2351.md) the [pᵊnîmî](../../strongs/h/h6442.md) [sha'ar](../../strongs/h/h8179.md) were the [liškâ](../../strongs/h/h3957.md) of the [shiyr](../../strongs/h/h7891.md) in the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md), which was at the [kāṯēp̄](../../strongs/h/h3802.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md) [sha'ar](../../strongs/h/h8179.md); and their [paniym](../../strongs/h/h6440.md) was [derek](../../strongs/h/h1870.md) the [dārôm](../../strongs/h/h1864.md): one at the [kāṯēp̄](../../strongs/h/h3802.md) of the [qāḏîm](../../strongs/h/h6921.md) [sha'ar](../../strongs/h/h8179.md) having the [paniym](../../strongs/h/h6440.md) [derek](../../strongs/h/h1870.md) the [ṣāp̄ôn](../../strongs/h/h6828.md).

<a name="ezekiel_40_45"></a>Ezekiel 40:45

And he [dabar](../../strongs/h/h1696.md) unto me, [zô](../../strongs/h/h2090.md) [liškâ](../../strongs/h/h3957.md), whose [paniym](../../strongs/h/h6440.md) is [derek](../../strongs/h/h1870.md) the [dārôm](../../strongs/h/h1864.md), is for the [kōhēn](../../strongs/h/h3548.md), the [shamar](../../strongs/h/h8104.md) of the [mišmereṯ](../../strongs/h/h4931.md) of the [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_40_46"></a>Ezekiel 40:46

And the [liškâ](../../strongs/h/h3957.md) whose [paniym](../../strongs/h/h6440.md) is [derek](../../strongs/h/h1870.md) the [ṣāp̄ôn](../../strongs/h/h6828.md) is for the [kōhēn](../../strongs/h/h3548.md), the [shamar](../../strongs/h/h8104.md) of the [mišmereṯ](../../strongs/h/h4931.md) of the [mizbeach](../../strongs/h/h4196.md): these are the [ben](../../strongs/h/h1121.md) of [Ṣāḏôq](../../strongs/h/h6659.md) among the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md), which come [qārēḇ](../../strongs/h/h7131.md) to [Yĕhovah](../../strongs/h/h3068.md) to [sharath](../../strongs/h/h8334.md) unto him.

<a name="ezekiel_40_47"></a>Ezekiel 40:47

So he [māḏaḏ](../../strongs/h/h4058.md) the [ḥāṣēr](../../strongs/h/h2691.md), an hundred ['ammâ](../../strongs/h/h520.md) ['ōreḵ](../../strongs/h/h753.md), and an hundred ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md), [rāḇaʿ](../../strongs/h/h7251.md); and the [mizbeach](../../strongs/h/h4196.md) that was [paniym](../../strongs/h/h6440.md) the [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_40_48"></a>Ezekiel 40:48

And he [bow'](../../strongs/h/h935.md) me to the ['ûlām](../../strongs/h/h197.md) of the [bayith](../../strongs/h/h1004.md), and [māḏaḏ](../../strongs/h/h4058.md) each ['ayil](../../strongs/h/h352.md) of the ['ûlām](../../strongs/h/h197.md), five ['ammâ](../../strongs/h/h520.md) on this side, and five ['ammâ](../../strongs/h/h520.md) on that side: and the [rōḥaḇ](../../strongs/h/h7341.md) of the [sha'ar](../../strongs/h/h8179.md) was three ['ammâ](../../strongs/h/h520.md) on this side, and three ['ammâ](../../strongs/h/h520.md) on that side.

<a name="ezekiel_40_49"></a>Ezekiel 40:49

The ['ōreḵ](../../strongs/h/h753.md) of the ['ûlām](../../strongs/h/h197.md) was twenty ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) eleven ['ammâ](../../strongs/h/h520.md); and he brought me by the [maʿălâ](../../strongs/h/h4609.md) whereby they [ʿālâ](../../strongs/h/h5927.md) to it: and there were [ʿammûḏ](../../strongs/h/h5982.md) by the ['ayil](../../strongs/h/h352.md), one on this side, and another on that side.

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 39](ezekiel_39.md) - [Ezekiel 41](ezekiel_41.md)