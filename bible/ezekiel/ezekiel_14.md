# [Ezekiel 14](https://www.blueletterbible.org/kjv/ezekiel/14)

<a name="ezekiel_14_1"></a>Ezekiel 14:1

Then [bow'](../../strongs/h/h935.md) ['enowsh](../../strongs/h/h582.md) of the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md) unto me, and [yashab](../../strongs/h/h3427.md) [paniym](../../strongs/h/h6440.md) me.

<a name="ezekiel_14_2"></a>Ezekiel 14:2

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_14_3"></a>Ezekiel 14:3

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), these ['enowsh](../../strongs/h/h582.md) have [ʿālâ](../../strongs/h/h5927.md) their [gillûl](../../strongs/h/h1544.md) in their [leb](../../strongs/h/h3820.md), and [nathan](../../strongs/h/h5414.md) the [miḵšôl](../../strongs/h/h4383.md) of their ['avon](../../strongs/h/h5771.md) before their [paniym](../../strongs/h/h6440.md): should I be [darash](../../strongs/h/h1875.md) of at [darash](../../strongs/h/h1875.md) by them?

<a name="ezekiel_14_4"></a>Ezekiel 14:4

Therefore [dabar](../../strongs/h/h1696.md) unto them, and ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Every ['iysh](../../strongs/h/h376.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) that [ʿālâ](../../strongs/h/h5927.md) his [gillûl](../../strongs/h/h1544.md) in his [leb](../../strongs/h/h3820.md), and [śûm](../../strongs/h/h7760.md) the [miḵšôl](../../strongs/h/h4383.md) of his ['avon](../../strongs/h/h5771.md) before his [paniym](../../strongs/h/h6440.md), and [bow'](../../strongs/h/h935.md) to the [nāḇî'](../../strongs/h/h5030.md); I [Yĕhovah](../../strongs/h/h3068.md) will ['anah](../../strongs/h/h6030.md) him that [bow'](../../strongs/h/h935.md) according to the [rōḇ](../../strongs/h/h7230.md) of his [gillûl](../../strongs/h/h1544.md);

<a name="ezekiel_14_5"></a>Ezekiel 14:5

That I may [tāp̄aś](../../strongs/h/h8610.md) the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) in their own [leb](../../strongs/h/h3820.md), because they are all [zûr](../../strongs/h/h2114.md) from me through their [gillûl](../../strongs/h/h1544.md).

<a name="ezekiel_14_6"></a>Ezekiel 14:6

Therefore ['āmar](../../strongs/h/h559.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); [shuwb](../../strongs/h/h7725.md), and [shuwb](../../strongs/h/h7725.md) from your [gillûl](../../strongs/h/h1544.md); and [shuwb](../../strongs/h/h7725.md) your [paniym](../../strongs/h/h6440.md) from all your [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="ezekiel_14_7"></a>Ezekiel 14:7

For every ['iysh](../../strongs/h/h376.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), or of the [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) in [Yisra'el](../../strongs/h/h3478.md), which [nāzar](../../strongs/h/h5144.md) himself from ['aḥar](../../strongs/h/h310.md), and [ʿālâ](../../strongs/h/h5927.md) his [gillûl](../../strongs/h/h1544.md) in his [leb](../../strongs/h/h3820.md), and [śûm](../../strongs/h/h7760.md) the [miḵšôl](../../strongs/h/h4383.md) of his ['avon](../../strongs/h/h5771.md) before his [paniym](../../strongs/h/h6440.md), and [bow'](../../strongs/h/h935.md) to a [nāḇî'](../../strongs/h/h5030.md) to [darash](../../strongs/h/h1875.md) of him concerning me; I [Yĕhovah](../../strongs/h/h3068.md) will ['anah](../../strongs/h/h6030.md) him by myself:

<a name="ezekiel_14_8"></a>Ezekiel 14:8

And I will [nathan](../../strongs/h/h5414.md) my [paniym](../../strongs/h/h6440.md) against that ['iysh](../../strongs/h/h376.md), and will [šāmēm](../../strongs/h/h8074.md) him a ['ôṯ](../../strongs/h/h226.md) and a [māšāl](../../strongs/h/h4912.md), and I will [karath](../../strongs/h/h3772.md) him from the midst of my ['am](../../strongs/h/h5971.md); and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_14_9"></a>Ezekiel 14:9

And if the [nāḇî'](../../strongs/h/h5030.md) be [pāṯâ](../../strongs/h/h6601.md) when he hath [dabar](../../strongs/h/h1696.md) a [dabar](../../strongs/h/h1697.md), I [Yĕhovah](../../strongs/h/h3068.md) have [pāṯâ](../../strongs/h/h6601.md) that [nāḇî'](../../strongs/h/h5030.md), and I will [natah](../../strongs/h/h5186.md) my [yad](../../strongs/h/h3027.md) upon him, and will [šāmaḏ](../../strongs/h/h8045.md) him from the midst of my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_14_10"></a>Ezekiel 14:10

And they shall [nasa'](../../strongs/h/h5375.md) the ['avon](../../strongs/h/h5771.md): the ['avon](../../strongs/h/h5771.md) of the [nāḇî'](../../strongs/h/h5030.md) shall be even as the ['avon](../../strongs/h/h5771.md) of him that [darash](../../strongs/h/h1875.md) unto him;

<a name="ezekiel_14_11"></a>Ezekiel 14:11

That the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) may go no more [tāʿâ](../../strongs/h/h8582.md) from ['aḥar](../../strongs/h/h310.md), neither be [ṭāmē'](../../strongs/h/h2930.md) any more with all their [pesha'](../../strongs/h/h6588.md); but that they may be my ['am](../../strongs/h/h5971.md), and I may be their ['Elohiym](../../strongs/h/h430.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_14_12"></a>Ezekiel 14:12

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came again to me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_14_13"></a>Ezekiel 14:13

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), when the ['erets](../../strongs/h/h776.md) [chata'](../../strongs/h/h2398.md) against me by [māʿal](../../strongs/h/h4603.md) [maʿal](../../strongs/h/h4604.md), then will I [natah](../../strongs/h/h5186.md) mine [yad](../../strongs/h/h3027.md) upon it, and will [shabar](../../strongs/h/h7665.md) the [maṭṭê](../../strongs/h/h4294.md) of the [lechem](../../strongs/h/h3899.md) thereof, and will [shalach](../../strongs/h/h7971.md) [rāʿāḇ](../../strongs/h/h7458.md) upon it, and will [karath](../../strongs/h/h3772.md) ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md) from it:

<a name="ezekiel_14_14"></a>Ezekiel 14:14

Though these three ['enowsh](../../strongs/h/h582.md), [Nōaḥ](../../strongs/h/h5146.md), [Dinîyē'L](../../strongs/h/h1840.md), and ['Îyôḇ](../../strongs/h/h347.md), were in it, they should [natsal](../../strongs/h/h5337.md) but their own [nephesh](../../strongs/h/h5315.md) by their [tsedaqah](../../strongs/h/h6666.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_14_15"></a>Ezekiel 14:15

If I cause [ra'](../../strongs/h/h7451.md) [chay](../../strongs/h/h2416.md) to ['abar](../../strongs/h/h5674.md) the ['erets](../../strongs/h/h776.md), and they [šāḵōl](../../strongs/h/h7921.md) it, so that it be [šᵊmāmâ](../../strongs/h/h8077.md), that no man may ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) of the [chay](../../strongs/h/h2416.md):

<a name="ezekiel_14_16"></a>Ezekiel 14:16

Though these three ['enowsh](../../strongs/h/h582.md) were in it, as I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), they shall [natsal](../../strongs/h/h5337.md) neither [ben](../../strongs/h/h1121.md) nor [bath](../../strongs/h/h1323.md); they only shall be [natsal](../../strongs/h/h5337.md), but the ['erets](../../strongs/h/h776.md) shall be [šᵊmāmâ](../../strongs/h/h8077.md).

<a name="ezekiel_14_17"></a>Ezekiel 14:17

Or if I [bow'](../../strongs/h/h935.md) a [chereb](../../strongs/h/h2719.md) upon that ['erets](../../strongs/h/h776.md), and ['āmar](../../strongs/h/h559.md), [chereb](../../strongs/h/h2719.md), ['abar](../../strongs/h/h5674.md) the ['erets](../../strongs/h/h776.md); so that I [karath](../../strongs/h/h3772.md) ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md) from it:

<a name="ezekiel_14_18"></a>Ezekiel 14:18

Though these three ['enowsh](../../strongs/h/h582.md) were in it, as I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), they shall [natsal](../../strongs/h/h5337.md) neither [ben](../../strongs/h/h1121.md) nor [bath](../../strongs/h/h1323.md), but they only shall be [natsal](../../strongs/h/h5337.md) themselves.

<a name="ezekiel_14_19"></a>Ezekiel 14:19

Or if I [shalach](../../strongs/h/h7971.md) a [deḇer](../../strongs/h/h1698.md) into that ['erets](../../strongs/h/h776.md), and [šāp̄aḵ](../../strongs/h/h8210.md) my [chemah](../../strongs/h/h2534.md) upon it in [dam](../../strongs/h/h1818.md), to [karath](../../strongs/h/h3772.md) from it ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md):

<a name="ezekiel_14_20"></a>Ezekiel 14:20

Though [Nōaḥ](../../strongs/h/h5146.md), [Dinîyē'L](../../strongs/h/h1840.md), and ['Îyôḇ](../../strongs/h/h347.md), were in it, as I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), they shall [natsal](../../strongs/h/h5337.md) neither [ben](../../strongs/h/h1121.md) nor [bath](../../strongs/h/h1323.md); they shall but [natsal](../../strongs/h/h5337.md) their own [nephesh](../../strongs/h/h5315.md) by their [tsedaqah](../../strongs/h/h6666.md).

<a name="ezekiel_14_21"></a>Ezekiel 14:21

For thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); How much more when I [shalach](../../strongs/h/h7971.md) my four [ra'](../../strongs/h/h7451.md) [šep̄eṭ](../../strongs/h/h8201.md) upon [Yĕruwshalaim](../../strongs/h/h3389.md), the [chereb](../../strongs/h/h2719.md), and the [rāʿāḇ](../../strongs/h/h7458.md), and the [ra'](../../strongs/h/h7451.md) [chay](../../strongs/h/h2416.md), and the [deḇer](../../strongs/h/h1698.md), to [karath](../../strongs/h/h3772.md) from it ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md)?

<a name="ezekiel_14_22"></a>Ezekiel 14:22

Yet, behold, therein shall be [yāṯar](../../strongs/h/h3498.md) a [pᵊlêṭâ](../../strongs/h/h6413.md) that shall be [yāṣā'](../../strongs/h/h3318.md), both [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md): behold, they shall [yāṣā'](../../strongs/h/h3318.md) unto you, and ye shall [ra'ah](../../strongs/h/h7200.md) their [derek](../../strongs/h/h1870.md) and their ['aliylah](../../strongs/h/h5949.md): and ye shall be [nacham](../../strongs/h/h5162.md) concerning the [ra'](../../strongs/h/h7451.md) that I have [bow'](../../strongs/h/h935.md) upon [Yĕruwshalaim](../../strongs/h/h3389.md), even concerning all that I have [bow'](../../strongs/h/h935.md) upon it.

<a name="ezekiel_14_23"></a>Ezekiel 14:23

And they shall [nacham](../../strongs/h/h5162.md) you, when ye [ra'ah](../../strongs/h/h7200.md) their [derek](../../strongs/h/h1870.md) and their ['aliylah](../../strongs/h/h5949.md): and ye shall [yada'](../../strongs/h/h3045.md) that I have not ['asah](../../strongs/h/h6213.md) without [ḥinnām](../../strongs/h/h2600.md) all that I have ['asah](../../strongs/h/h6213.md) in it, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 13](ezekiel_13.md) - [Ezekiel 15](ezekiel_15.md)