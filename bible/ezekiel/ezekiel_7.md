# [Ezekiel 7](https://www.blueletterbible.org/kjv/ezekiel/7)

<a name="ezekiel_7_1"></a>Ezekiel 7:1

Moreover the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_7_2"></a>Ezekiel 7:2

Also, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) unto the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md); A [qēṣ](../../strongs/h/h7093.md), the [qēṣ](../../strongs/h/h7093.md) is [bow'](../../strongs/h/h935.md) upon the four [kanaph](../../strongs/h/h3671.md) of the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_7_3"></a>Ezekiel 7:3

Now is the [qēṣ](../../strongs/h/h7093.md) come upon thee, and I will [shalach](../../strongs/h/h7971.md) mine ['aph](../../strongs/h/h639.md) upon thee, and will [shaphat](../../strongs/h/h8199.md) thee according to thy [derek](../../strongs/h/h1870.md), and will [nathan](../../strongs/h/h5414.md) upon thee all thine [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="ezekiel_7_4"></a>Ezekiel 7:4

And mine ['ayin](../../strongs/h/h5869.md) shall not [ḥûs](../../strongs/h/h2347.md) thee, neither will I have [ḥāmal](../../strongs/h/h2550.md): but I will [nathan](../../strongs/h/h5414.md) thy [derek](../../strongs/h/h1870.md) upon thee, and thine [tôʿēḇâ](../../strongs/h/h8441.md) shall be in the midst of thee: and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_7_5"></a>Ezekiel 7:5

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); A [ra'](../../strongs/h/h7451.md), an only [ra'](../../strongs/h/h7451.md), behold, is [bow'](../../strongs/h/h935.md).

<a name="ezekiel_7_6"></a>Ezekiel 7:6

A [qēṣ](../../strongs/h/h7093.md) is [bow'](../../strongs/h/h935.md), the [qēṣ](../../strongs/h/h7093.md) is [bow'](../../strongs/h/h935.md): it [quwts](../../strongs/h/h6974.md) for thee; behold, it is [bow'](../../strongs/h/h935.md).

<a name="ezekiel_7_7"></a>Ezekiel 7:7

The [ṣᵊp̄îrâ](../../strongs/h/h6843.md) is [bow'](../../strongs/h/h935.md) unto thee, O thou that [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md): the [ʿēṯ](../../strongs/h/h6256.md) is [bow'](../../strongs/h/h935.md) [bow'](../../strongs/h/h935.md), the [yowm](../../strongs/h/h3117.md) of [mᵊhûmâ](../../strongs/h/h4103.md) is [qarowb](../../strongs/h/h7138.md), and not the [hēḏ](../../strongs/h/h1906.md) of the [har](../../strongs/h/h2022.md).

<a name="ezekiel_7_8"></a>Ezekiel 7:8

Now will I [qarowb](../../strongs/h/h7138.md) [šāp̄aḵ](../../strongs/h/h8210.md) my [chemah](../../strongs/h/h2534.md) upon thee, and [kalah](../../strongs/h/h3615.md) mine ['aph](../../strongs/h/h639.md) upon thee: and I will [shaphat](../../strongs/h/h8199.md) thee according to thy [derek](../../strongs/h/h1870.md), and will [nathan](../../strongs/h/h5414.md) thee for all thine [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="ezekiel_7_9"></a>Ezekiel 7:9

And mine ['ayin](../../strongs/h/h5869.md) shall not [ḥûs](../../strongs/h/h2347.md), neither will I have [ḥāmal](../../strongs/h/h2550.md): I will [nathan](../../strongs/h/h5414.md) thee according to thy [derek](../../strongs/h/h1870.md) and thine [tôʿēḇâ](../../strongs/h/h8441.md) that are in the midst of thee; and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md) that [nakah](../../strongs/h/h5221.md).

<a name="ezekiel_7_10"></a>Ezekiel 7:10

Behold the [yowm](../../strongs/h/h3117.md), behold, it is [bow'](../../strongs/h/h935.md): the [ṣᵊp̄îrâ](../../strongs/h/h6843.md) is [yāṣā'](../../strongs/h/h3318.md); the [maṭṭê](../../strongs/h/h4294.md) hath [tsuwts](../../strongs/h/h6692.md), [zāḏôn](../../strongs/h/h2087.md) hath [pāraḥ](../../strongs/h/h6524.md).

<a name="ezekiel_7_11"></a>Ezekiel 7:11

[chamac](../../strongs/h/h2555.md) is [quwm](../../strongs/h/h6965.md) into a [maṭṭê](../../strongs/h/h4294.md) of [resha'](../../strongs/h/h7562.md): none of them, nor of their [hāmôn](../../strongs/h/h1995.md), nor of [hām](../../strongs/h/h1991.md) of theirs: neither shall there be [nōha](../../strongs/h/h5089.md) for them.

<a name="ezekiel_7_12"></a>Ezekiel 7:12

The [ʿēṯ](../../strongs/h/h6256.md) is [bow'](../../strongs/h/h935.md), the [yowm](../../strongs/h/h3117.md) [naga'](../../strongs/h/h5060.md): let not the [qānâ](../../strongs/h/h7069.md) [samach](../../strongs/h/h8055.md), nor the [māḵar](../../strongs/h/h4376.md) ['āḇal](../../strongs/h/h56.md): for [charown](../../strongs/h/h2740.md) is upon all the [hāmôn](../../strongs/h/h1995.md) thereof.

<a name="ezekiel_7_13"></a>Ezekiel 7:13

For the [māḵar](../../strongs/h/h4376.md) shall not [shuwb](../../strongs/h/h7725.md) to that which is [mimkār](../../strongs/h/h4465.md), although they were yet [chay](../../strongs/h/h2416.md): for the [ḥāzôn](../../strongs/h/h2377.md) is touching the whole [hāmôn](../../strongs/h/h1995.md) thereof, which shall not [shuwb](../../strongs/h/h7725.md); neither shall ['iysh](../../strongs/h/h376.md) [ḥāzaq](../../strongs/h/h2388.md) himself in the ['avon](../../strongs/h/h5771.md) of his [chay](../../strongs/h/h2416.md).

<a name="ezekiel_7_14"></a>Ezekiel 7:14

They have [tāqaʿ](../../strongs/h/h8628.md) the [tāqôaʿ](../../strongs/h/h8619.md), even to make all [kuwn](../../strongs/h/h3559.md); but none [halak](../../strongs/h/h1980.md) to the [milḥāmâ](../../strongs/h/h4421.md): for my [charown](../../strongs/h/h2740.md) is upon all the [hāmôn](../../strongs/h/h1995.md) thereof.

<a name="ezekiel_7_15"></a>Ezekiel 7:15

The [chereb](../../strongs/h/h2719.md) is [ḥûṣ](../../strongs/h/h2351.md), and the [deḇer](../../strongs/h/h1698.md) and the [rāʿāḇ](../../strongs/h/h7458.md) [bayith](../../strongs/h/h1004.md): he that is in the [sadeh](../../strongs/h/h7704.md) shall [muwth](../../strongs/h/h4191.md) with the [chereb](../../strongs/h/h2719.md); and he that is in the [ʿîr](../../strongs/h/h5892.md), [rāʿāḇ](../../strongs/h/h7458.md) and [deḇer](../../strongs/h/h1698.md) shall ['akal](../../strongs/h/h398.md) him.

<a name="ezekiel_7_16"></a>Ezekiel 7:16

But they that [palat](../../strongs/h/h6403.md) of them shall [pālîṭ](../../strongs/h/h6412.md), and shall be on the [har](../../strongs/h/h2022.md) like [yônâ](../../strongs/h/h3123.md) of the [gay'](../../strongs/h/h1516.md), all of them [hāmâ](../../strongs/h/h1993.md), every ['iysh](../../strongs/h/h376.md) for his ['avon](../../strongs/h/h5771.md).

<a name="ezekiel_7_17"></a>Ezekiel 7:17

All [yad](../../strongs/h/h3027.md) shall be [rāp̄â](../../strongs/h/h7503.md), and all [bereḵ](../../strongs/h/h1290.md) shall be [yālaḵ](../../strongs/h/h3212.md) as [mayim](../../strongs/h/h4325.md).

<a name="ezekiel_7_18"></a>Ezekiel 7:18

They shall also [ḥāḡar](../../strongs/h/h2296.md) themselves with [śaq](../../strongs/h/h8242.md), and [pallāṣûṯ](../../strongs/h/h6427.md) shall [kāsâ](../../strongs/h/h3680.md) them; and [bûšâ](../../strongs/h/h955.md) shall be upon all [paniym](../../strongs/h/h6440.md), and [qrḥ](../../strongs/h/h7144.md) upon all their [ro'sh](../../strongs/h/h7218.md).

<a name="ezekiel_7_19"></a>Ezekiel 7:19

They shall [shalak](../../strongs/h/h7993.md) their [keceph](../../strongs/h/h3701.md) in the [ḥûṣ](../../strongs/h/h2351.md), and their [zāhāḇ](../../strongs/h/h2091.md) shall be [nidâ](../../strongs/h/h5079.md): their [keceph](../../strongs/h/h3701.md) and their [zāhāḇ](../../strongs/h/h2091.md) shall not be [yakol](../../strongs/h/h3201.md) to [natsal](../../strongs/h/h5337.md) them in the [yowm](../../strongs/h/h3117.md) of the ['ebrah](../../strongs/h/h5678.md) of [Yĕhovah](../../strongs/h/h3068.md): they shall not [sāׂbaʿ](../../strongs/h/h7646.md) their [nephesh](../../strongs/h/h5315.md), neither [mālā'](../../strongs/h/h4390.md) their [me'ah](../../strongs/h/h4578.md): because it is the [miḵšôl](../../strongs/h/h4383.md) of their ['avon](../../strongs/h/h5771.md).

<a name="ezekiel_7_20"></a>Ezekiel 7:20

As for the [ṣᵊḇî](../../strongs/h/h6643.md) of his [ʿădiy](../../strongs/h/h5716.md), he [śûm](../../strongs/h/h7760.md) it in [gā'ôn](../../strongs/h/h1347.md): but they ['asah](../../strongs/h/h6213.md) the [tselem](../../strongs/h/h6754.md) of their [tôʿēḇâ](../../strongs/h/h8441.md) and of their [šiqqûṣ](../../strongs/h/h8251.md) therein: therefore have I [nathan](../../strongs/h/h5414.md) it [nidâ](../../strongs/h/h5079.md) from them.

<a name="ezekiel_7_21"></a>Ezekiel 7:21

And I will [nathan](../../strongs/h/h5414.md) it into the [yad](../../strongs/h/h3027.md) of the [zûr](../../strongs/h/h2114.md) for a [baz](../../strongs/h/h957.md), and to the [rasha'](../../strongs/h/h7563.md) of the ['erets](../../strongs/h/h776.md) for a [šālāl](../../strongs/h/h7998.md); and they shall [ḥālal](../../strongs/h/h2490.md) it.

<a name="ezekiel_7_22"></a>Ezekiel 7:22

My [paniym](../../strongs/h/h6440.md) will I [cabab](../../strongs/h/h5437.md) also from them, and they shall [ḥālal](../../strongs/h/h2490.md) my [tsaphan](../../strongs/h/h6845.md): for the [periyts](../../strongs/h/h6530.md) shall [bow'](../../strongs/h/h935.md) into it, and [ḥālal](../../strongs/h/h2490.md) it.

<a name="ezekiel_7_23"></a>Ezekiel 7:23

['asah](../../strongs/h/h6213.md) a [ratôq](../../strongs/h/h7569.md): for the ['erets](../../strongs/h/h776.md) is [mālā'](../../strongs/h/h4390.md) of [dam](../../strongs/h/h1818.md) [mishpat](../../strongs/h/h4941.md), and the [ʿîr](../../strongs/h/h5892.md) is [mālā'](../../strongs/h/h4390.md) of [chamac](../../strongs/h/h2555.md).

<a name="ezekiel_7_24"></a>Ezekiel 7:24

Wherefore I will [bow'](../../strongs/h/h935.md) the [ra'](../../strongs/h/h7451.md) of the [gowy](../../strongs/h/h1471.md), and they shall [yarash](../../strongs/h/h3423.md) their [bayith](../../strongs/h/h1004.md): I will also make the [gā'ôn](../../strongs/h/h1347.md) of the ['az](../../strongs/h/h5794.md) to [shabath](../../strongs/h/h7673.md); and their holy [qadash](../../strongs/h/h6942.md) shall be [ḥālal](../../strongs/h/h2490.md) [nāḥal](../../strongs/h/h5157.md).

<a name="ezekiel_7_25"></a>Ezekiel 7:25

[qᵊp̄āḏâ](../../strongs/h/h7089.md) [bow'](../../strongs/h/h935.md); and they shall [bāqaš](../../strongs/h/h1245.md) [shalowm](../../strongs/h/h7965.md), and there shall be none.

<a name="ezekiel_7_26"></a>Ezekiel 7:26

[hvô](../../strongs/h/h1943.md) shall [bow'](../../strongs/h/h935.md) upon [hvô](../../strongs/h/h1943.md), and [šᵊmûʿâ](../../strongs/h/h8052.md) shall be upon [šᵊmûʿâ](../../strongs/h/h8052.md); then shall they [bāqaš](../../strongs/h/h1245.md) a [ḥāzôn](../../strongs/h/h2377.md) of the [nāḇî'](../../strongs/h/h5030.md); but the [towrah](../../strongs/h/h8451.md) shall ['abad](../../strongs/h/h6.md) from the [kōhēn](../../strongs/h/h3548.md), and ['etsah](../../strongs/h/h6098.md) from the [zāqēn](../../strongs/h/h2205.md).

<a name="ezekiel_7_27"></a>Ezekiel 7:27

The [melek](../../strongs/h/h4428.md) shall ['āḇal](../../strongs/h/h56.md), and the [nāśî'](../../strongs/h/h5387.md) shall be [labash](../../strongs/h/h3847.md) with [šᵊmāmâ](../../strongs/h/h8077.md), and the [yad](../../strongs/h/h3027.md) of the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) shall be [bahal](../../strongs/h/h926.md): I will ['asah](../../strongs/h/h6213.md) unto them after their [derek](../../strongs/h/h1870.md), and according to their [mishpat](../../strongs/h/h4941.md) will I [shaphat](../../strongs/h/h8199.md) them; and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 6](ezekiel_6.md) - [Ezekiel 8](ezekiel_8.md)