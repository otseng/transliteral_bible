# [Ezekiel 15](https://www.blueletterbible.org/kjv/ezekiel/15)

<a name="ezekiel_15_1"></a>Ezekiel 15:1

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_15_2"></a>Ezekiel 15:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), What is the [gep̄en](../../strongs/h/h1612.md) ['ets](../../strongs/h/h6086.md) more than any ['ets](../../strongs/h/h6086.md), or than a [zᵊmôrâ](../../strongs/h/h2156.md) which is among the ['ets](../../strongs/h/h6086.md) of the [yaʿar](../../strongs/h/h3293.md)?

<a name="ezekiel_15_3"></a>Ezekiel 15:3

Shall ['ets](../../strongs/h/h6086.md) be [laqach](../../strongs/h/h3947.md) thereof to ['asah](../../strongs/h/h6213.md) any [mĕla'kah](../../strongs/h/h4399.md)? or will men [laqach](../../strongs/h/h3947.md) a [yāṯēḏ](../../strongs/h/h3489.md) of it to [tālâ](../../strongs/h/h8518.md) any [kĕliy](../../strongs/h/h3627.md) thereon?

<a name="ezekiel_15_4"></a>Ezekiel 15:4

Behold, it is [nathan](../../strongs/h/h5414.md) into the ['esh](../../strongs/h/h784.md) for ['oklah](../../strongs/h/h402.md); the ['esh](../../strongs/h/h784.md) ['akal](../../strongs/h/h398.md) both the [qāṣâ](../../strongs/h/h7098.md) of it, and the midst of it is [ḥārar](../../strongs/h/h2787.md). Is it [tsalach](../../strongs/h/h6743.md) for any [mĕla'kah](../../strongs/h/h4399.md)?

<a name="ezekiel_15_5"></a>Ezekiel 15:5

Behold, when it was [tamiym](../../strongs/h/h8549.md), it was ['asah](../../strongs/h/h6213.md) for no [mĕla'kah](../../strongs/h/h4399.md): how much less shall it be ['asah](../../strongs/h/h6213.md) yet for any [mĕla'kah](../../strongs/h/h4399.md), when the ['esh](../../strongs/h/h784.md) hath ['akal](../../strongs/h/h398.md) it, and it is [ḥārar](../../strongs/h/h2787.md)?

<a name="ezekiel_15_6"></a>Ezekiel 15:6

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); As the [gep̄en](../../strongs/h/h1612.md) ['ets](../../strongs/h/h6086.md) among the ['ets](../../strongs/h/h6086.md) of the [yaʿar](../../strongs/h/h3293.md), which I have [nathan](../../strongs/h/h5414.md) to the ['esh](../../strongs/h/h784.md) for ['oklah](../../strongs/h/h402.md), so will I [nathan](../../strongs/h/h5414.md) the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="ezekiel_15_7"></a>Ezekiel 15:7

And I will [nathan](../../strongs/h/h5414.md) my [paniym](../../strongs/h/h6440.md) against them; they shall [yāṣā'](../../strongs/h/h3318.md) from one ['esh](../../strongs/h/h784.md), and another ['esh](../../strongs/h/h784.md) shall ['akal](../../strongs/h/h398.md) them; and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when I [śûm](../../strongs/h/h7760.md) my [paniym](../../strongs/h/h6440.md) against them.

<a name="ezekiel_15_8"></a>Ezekiel 15:8

And I will [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) [šᵊmāmâ](../../strongs/h/h8077.md), because they have [māʿal](../../strongs/h/h4603.md) a [maʿal](../../strongs/h/h4604.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 14](ezekiel_14.md) - [Ezekiel 16](ezekiel_16.md)