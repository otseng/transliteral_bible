# [Ezekiel 37](https://www.blueletterbible.org/kjv/ezekiel/37)

<a name="ezekiel_37_1"></a>Ezekiel 37:1

The [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) was upon me, and [yāṣā'](../../strongs/h/h3318.md) me in the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md), and set me [nuwach](../../strongs/h/h5117.md) in the [tavek](../../strongs/h/h8432.md) of the [biqʿâ](../../strongs/h/h1237.md) which was [mālē'](../../strongs/h/h4392.md) of ['etsem](../../strongs/h/h6106.md),

<a name="ezekiel_37_2"></a>Ezekiel 37:2

And caused me to ['abar](../../strongs/h/h5674.md) by them [cabiyb](../../strongs/h/h5439.md): and, behold, there were [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md) in the [paniym](../../strongs/h/h6440.md) [biqʿâ](../../strongs/h/h1237.md); and, lo, they were [me'od](../../strongs/h/h3966.md) [yāḇēš](../../strongs/h/h3002.md).

<a name="ezekiel_37_3"></a>Ezekiel 37:3

And he ['āmar](../../strongs/h/h559.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), can these ['etsem](../../strongs/h/h6106.md) [ḥāyâ](../../strongs/h/h2421.md)? And I ['āmar](../../strongs/h/h559.md), ['Adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), thou [yada'](../../strongs/h/h3045.md).

<a name="ezekiel_37_4"></a>Ezekiel 37:4

Again he ['āmar](../../strongs/h/h559.md) unto me, [nāḇā'](../../strongs/h/h5012.md) upon these ['etsem](../../strongs/h/h6106.md), and ['āmar](../../strongs/h/h559.md) unto them, O ye [yāḇēš](../../strongs/h/h3002.md) ['etsem](../../strongs/h/h6106.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_37_5"></a>Ezekiel 37:5

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) unto these ['etsem](../../strongs/h/h6106.md); Behold, I will cause [ruwach](../../strongs/h/h7307.md) to [bow'](../../strongs/h/h935.md) into you, and ye shall [ḥāyâ](../../strongs/h/h2421.md):

<a name="ezekiel_37_6"></a>Ezekiel 37:6

And I will [nathan](../../strongs/h/h5414.md) [gîḏ](../../strongs/h/h1517.md) upon you, and will [ʿālâ](../../strongs/h/h5927.md) [basar](../../strongs/h/h1320.md) upon you, and [qāram](../../strongs/h/h7159.md) you with ['owr](../../strongs/h/h5785.md), and [nathan](../../strongs/h/h5414.md) [ruwach](../../strongs/h/h7307.md) in you, and ye shall [ḥāyâ](../../strongs/h/h2421.md); and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_37_7"></a>Ezekiel 37:7

So I [nāḇā'](../../strongs/h/h5012.md) as I was [tsavah](../../strongs/h/h6680.md): and as I [nāḇā'](../../strongs/h/h5012.md), there was a [qowl](../../strongs/h/h6963.md), and behold a [raʿaš](../../strongs/h/h7494.md), and the ['etsem](../../strongs/h/h6106.md) [qāraḇ](../../strongs/h/h7126.md), ['etsem](../../strongs/h/h6106.md) to his ['etsem](../../strongs/h/h6106.md).

<a name="ezekiel_37_8"></a>Ezekiel 37:8

And when I [ra'ah](../../strongs/h/h7200.md), lo, the [gîḏ](../../strongs/h/h1517.md) and the [basar](../../strongs/h/h1320.md) [ʿālâ](../../strongs/h/h5927.md) upon them, and the ['owr](../../strongs/h/h5785.md) [qāram](../../strongs/h/h7159.md) them [maʿal](../../strongs/h/h4605.md): but there was no [ruwach](../../strongs/h/h7307.md) in them.

<a name="ezekiel_37_9"></a>Ezekiel 37:9

Then ['āmar](../../strongs/h/h559.md) he unto me, [nāḇā'](../../strongs/h/h5012.md) unto the [ruwach](../../strongs/h/h7307.md), [nāḇā'](../../strongs/h/h5012.md), [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), and ['āmar](../../strongs/h/h559.md) to the [ruwach](../../strongs/h/h7307.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); [bow'](../../strongs/h/h935.md) from the four [ruwach](../../strongs/h/h7307.md), O [ruwach](../../strongs/h/h7307.md), and [nāp̄aḥ](../../strongs/h/h5301.md) upon these [harag](../../strongs/h/h2026.md), that they may [ḥāyâ](../../strongs/h/h2421.md).

<a name="ezekiel_37_10"></a>Ezekiel 37:10

So I [nāḇā'](../../strongs/h/h5012.md) as he [tsavah](../../strongs/h/h6680.md) me, and the [ruwach](../../strongs/h/h7307.md) [bow'](../../strongs/h/h935.md) into them, and they [ḥāyâ](../../strongs/h/h2421.md), and ['amad](../../strongs/h/h5975.md) upon their [regel](../../strongs/h/h7272.md), a [me'od](../../strongs/h/h3966.md) [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) [ḥayil](../../strongs/h/h2428.md).

<a name="ezekiel_37_11"></a>Ezekiel 37:11

Then he ['āmar](../../strongs/h/h559.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), these ['etsem](../../strongs/h/h6106.md) are the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md): behold, they ['āmar](../../strongs/h/h559.md), Our ['etsem](../../strongs/h/h6106.md) are [yāḇēš](../../strongs/h/h3001.md), and our [tiqvâ](../../strongs/h/h8615.md) is ['abad](../../strongs/h/h6.md): we are [gāzar](../../strongs/h/h1504.md).

<a name="ezekiel_37_12"></a>Ezekiel 37:12

Therefore [nāḇā'](../../strongs/h/h5012.md) and ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, O my ['am](../../strongs/h/h5971.md), I will [pāṯaḥ](../../strongs/h/h6605.md) your [qeber](../../strongs/h/h6913.md), and cause you to [ʿālâ](../../strongs/h/h5927.md) out of your [qeber](../../strongs/h/h6913.md), and [bow'](../../strongs/h/h935.md) you into the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_37_13"></a>Ezekiel 37:13

And ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when I have [pāṯaḥ](../../strongs/h/h6605.md) your [qeber](../../strongs/h/h6913.md), O my ['am](../../strongs/h/h5971.md), and [ʿālâ](../../strongs/h/h5927.md) you out of your [qeber](../../strongs/h/h6913.md),

<a name="ezekiel_37_14"></a>Ezekiel 37:14

And shall [nathan](../../strongs/h/h5414.md) my [ruwach](../../strongs/h/h7307.md) in you, and ye shall [ḥāyâ](../../strongs/h/h2421.md), and I shall [yānaḥ](../../strongs/h/h3240.md) you in your own ['ăḏāmâ](../../strongs/h/h127.md): then shall ye [yada'](../../strongs/h/h3045.md) that I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) it, and ['asah](../../strongs/h/h6213.md) it, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_37_15"></a>Ezekiel 37:15

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came again unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_37_16"></a>Ezekiel 37:16

Moreover, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [laqach](../../strongs/h/h3947.md) thee one ['ets](../../strongs/h/h6086.md), and [kāṯaḇ](../../strongs/h/h3789.md) upon it, For [Yehuwdah](../../strongs/h/h3063.md), and for the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) his [ḥāḇēr](../../strongs/h/h2270.md): then [laqach](../../strongs/h/h3947.md) another ['ets](../../strongs/h/h6086.md), and [kāṯaḇ](../../strongs/h/h3789.md) upon it, For [Yôsēp̄](../../strongs/h/h3130.md), the ['ets](../../strongs/h/h6086.md) of ['Ep̄rayim](../../strongs/h/h669.md), and for all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) his [ḥāḇēr](../../strongs/h/h2270.md):

<a name="ezekiel_37_17"></a>Ezekiel 37:17

And [qāraḇ](../../strongs/h/h7126.md) them one to another into one ['ets](../../strongs/h/h6086.md); and they shall become one in thine [yad](../../strongs/h/h3027.md).

<a name="ezekiel_37_18"></a>Ezekiel 37:18

And when the [ben](../../strongs/h/h1121.md) of thy ['am](../../strongs/h/h5971.md) shall ['āmar](../../strongs/h/h559.md) unto thee, ['āmar](../../strongs/h/h559.md), Wilt thou not [nāḡaḏ](../../strongs/h/h5046.md) us what thou meanest by these?

<a name="ezekiel_37_19"></a>Ezekiel 37:19

[dabar](../../strongs/h/h1696.md) unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I will [laqach](../../strongs/h/h3947.md) the ['ets](../../strongs/h/h6086.md) of [Yôsēp̄](../../strongs/h/h3130.md), which is in the [yad](../../strongs/h/h3027.md) of ['Ep̄rayim](../../strongs/h/h669.md), and the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) his [ḥāḇēr](../../strongs/h/h2270.md), and will [nathan](../../strongs/h/h5414.md) them with him, even with the ['ets](../../strongs/h/h6086.md) of [Yehuwdah](../../strongs/h/h3063.md), and ['asah](../../strongs/h/h6213.md) them one ['ets](../../strongs/h/h6086.md), and they shall be one in mine [yad](../../strongs/h/h3027.md).

<a name="ezekiel_37_20"></a>Ezekiel 37:20

And the ['ets](../../strongs/h/h6086.md) whereon thou [kāṯaḇ](../../strongs/h/h3789.md) shall be in thine [yad](../../strongs/h/h3027.md) before their ['ayin](../../strongs/h/h5869.md).

<a name="ezekiel_37_21"></a>Ezekiel 37:21

And [dabar](../../strongs/h/h1696.md) unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I will [laqach](../../strongs/h/h3947.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) from among the [gowy](../../strongs/h/h1471.md), whither they be [halak](../../strongs/h/h1980.md), and will [qāḇaṣ](../../strongs/h/h6908.md) them [cabiyb](../../strongs/h/h5439.md), and [bow'](../../strongs/h/h935.md) them into their own ['ăḏāmâ](../../strongs/h/h127.md):

<a name="ezekiel_37_22"></a>Ezekiel 37:22

And I will ['asah](../../strongs/h/h6213.md) them one [gowy](../../strongs/h/h1471.md) in the ['erets](../../strongs/h/h776.md) upon the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md); and one [melek](../../strongs/h/h4428.md) shall be [melek](../../strongs/h/h4428.md) to them all: and they shall be no more two [gowy](../../strongs/h/h1471.md), neither shall they be [ḥāṣâ](../../strongs/h/h2673.md) into two [mamlāḵâ](../../strongs/h/h4467.md) any more at all:

<a name="ezekiel_37_23"></a>Ezekiel 37:23

Neither shall they [ṭāmē'](../../strongs/h/h2930.md) themselves any more with their [gillûl](../../strongs/h/h1544.md), nor with their [šiqqûṣ](../../strongs/h/h8251.md), nor with any of their [pesha'](../../strongs/h/h6588.md): but I will [yasha'](../../strongs/h/h3467.md) them out of all their [môšāḇ](../../strongs/h/h4186.md), wherein they have [chata'](../../strongs/h/h2398.md), and will [ṭāhēr](../../strongs/h/h2891.md) them: so shall they be my ['am](../../strongs/h/h5971.md), and I will be their ['Elohiym](../../strongs/h/h430.md).

<a name="ezekiel_37_24"></a>Ezekiel 37:24

And [Dāviḏ](../../strongs/h/h1732.md) my ['ebed](../../strongs/h/h5650.md) shall be [melek](../../strongs/h/h4428.md) over them; and they all shall have one [ra'ah](../../strongs/h/h7462.md): they shall also [yālaḵ](../../strongs/h/h3212.md) in my [mishpat](../../strongs/h/h4941.md), and [shamar](../../strongs/h/h8104.md) my [chuqqah](../../strongs/h/h2708.md), and ['asah](../../strongs/h/h6213.md) them.

<a name="ezekiel_37_25"></a>Ezekiel 37:25

And they shall [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) that I have [nathan](../../strongs/h/h5414.md) unto [Ya'aqob](../../strongs/h/h3290.md) my ['ebed](../../strongs/h/h5650.md), wherein your ['ab](../../strongs/h/h1.md) have [yashab](../../strongs/h/h3427.md); and they shall [yashab](../../strongs/h/h3427.md) therein, even they, and their [ben](../../strongs/h/h1121.md), and their [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md) ['owlam](../../strongs/h/h5769.md): and my ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) shall be their [nāśî'](../../strongs/h/h5387.md) ['owlam](../../strongs/h/h5769.md).

<a name="ezekiel_37_26"></a>Ezekiel 37:26

Moreover I will [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) of [shalowm](../../strongs/h/h7965.md) with them; it shall be an ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md) with them: and I will [nathan](../../strongs/h/h5414.md) them, and [rabah](../../strongs/h/h7235.md) them, and will [nathan](../../strongs/h/h5414.md) my [miqdash](../../strongs/h/h4720.md) in the [tavek](../../strongs/h/h8432.md) of them ['owlam](../../strongs/h/h5769.md).

<a name="ezekiel_37_27"></a>Ezekiel 37:27

My [miškān](../../strongs/h/h4908.md) also shall be with them: yea, I will be their ['Elohiym](../../strongs/h/h430.md), and they shall be my ['am](../../strongs/h/h5971.md).

<a name="ezekiel_37_28"></a>Ezekiel 37:28

And the [gowy](../../strongs/h/h1471.md) shall [yada'](../../strongs/h/h3045.md) that I [Yĕhovah](../../strongs/h/h3068.md) do [qadash](../../strongs/h/h6942.md) [Yisra'el](../../strongs/h/h3478.md), when my [miqdash](../../strongs/h/h4720.md) shall be in the [tavek](../../strongs/h/h8432.md) of them ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 36](ezekiel_36.md) - [Ezekiel 38](ezekiel_38.md)