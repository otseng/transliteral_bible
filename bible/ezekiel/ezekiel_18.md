# [Ezekiel 18](https://www.blueletterbible.org/kjv/ezekiel/18)

<a name="ezekiel_18_1"></a>Ezekiel 18:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me again, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_18_2"></a>Ezekiel 18:2

What mean ye, that ye [māšal](../../strongs/h/h4911.md) this [māšāl](../../strongs/h/h4912.md) concerning the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), The ['ab](../../strongs/h/h1.md) have ['akal](../../strongs/h/h398.md) [bōser](../../strongs/h/h1155.md), and the [ben](../../strongs/h/h1121.md) [šēn](../../strongs/h/h8127.md) are set on [qāhâ](../../strongs/h/h6949.md)?

<a name="ezekiel_18_3"></a>Ezekiel 18:3

As I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), ye shall not any more to [māšal](../../strongs/h/h4911.md) this [māšāl](../../strongs/h/h4912.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_18_4"></a>Ezekiel 18:4

Behold, all [nephesh](../../strongs/h/h5315.md) are mine; as the [nephesh](../../strongs/h/h5315.md) of the ['ab](../../strongs/h/h1.md), so also the [nephesh](../../strongs/h/h5315.md) of the [ben](../../strongs/h/h1121.md) is mine: the [nephesh](../../strongs/h/h5315.md) that [chata'](../../strongs/h/h2398.md), it shall [muwth](../../strongs/h/h4191.md).

<a name="ezekiel_18_5"></a>Ezekiel 18:5

But if an ['iysh](../../strongs/h/h376.md) be [tsaddiyq](../../strongs/h/h6662.md), and ['asah](../../strongs/h/h6213.md) that which is [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md),

<a name="ezekiel_18_6"></a>Ezekiel 18:6

And hath not ['akal](../../strongs/h/h398.md) upon the [har](../../strongs/h/h2022.md), neither hath [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md) to the [gillûl](../../strongs/h/h1544.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), neither hath [ṭāmē'](../../strongs/h/h2930.md) his [rea'](../../strongs/h/h7453.md) ['ishshah](../../strongs/h/h802.md), neither hath [qāraḇ](../../strongs/h/h7126.md) to a [nidâ](../../strongs/h/h5079.md) ['ishshah](../../strongs/h/h802.md),

<a name="ezekiel_18_7"></a>Ezekiel 18:7

And hath not [yānâ](../../strongs/h/h3238.md) ['iysh](../../strongs/h/h376.md), but hath [shuwb](../../strongs/h/h7725.md) to the [ḥôḇ](../../strongs/h/h2326.md) his [ḥăḇōl](../../strongs/h/h2258.md), hath [gāzal](../../strongs/h/h1497.md) none by [gᵊzēlâ](../../strongs/h/h1500.md), hath [nathan](../../strongs/h/h5414.md) his [lechem](../../strongs/h/h3899.md) to the [rāʿēḇ](../../strongs/h/h7457.md), and hath [kāsâ](../../strongs/h/h3680.md) the ['eyrom](../../strongs/h/h5903.md) with a [beḡeḏ](../../strongs/h/h899.md);

<a name="ezekiel_18_8"></a>Ezekiel 18:8

He that hath not [nathan](../../strongs/h/h5414.md) upon [neshek](../../strongs/h/h5392.md), neither hath [laqach](../../strongs/h/h3947.md) any [tarbîṯ](../../strongs/h/h8636.md), that hath [shuwb](../../strongs/h/h7725.md) his [yad](../../strongs/h/h3027.md) from ['evel](../../strongs/h/h5766.md), hath ['asah](../../strongs/h/h6213.md) ['emeth](../../strongs/h/h571.md) [mishpat](../../strongs/h/h4941.md) between ['iysh](../../strongs/h/h376.md) and ['iysh](../../strongs/h/h376.md),

<a name="ezekiel_18_9"></a>Ezekiel 18:9

Hath [halak](../../strongs/h/h1980.md) in my [chuqqah](../../strongs/h/h2708.md), and hath [shamar](../../strongs/h/h8104.md) my [mishpat](../../strongs/h/h4941.md), to ['asah](../../strongs/h/h6213.md) ['emeth](../../strongs/h/h571.md); he is [tsaddiyq](../../strongs/h/h6662.md), he shall [ḥāyâ](../../strongs/h/h2421.md) [ḥāyâ](../../strongs/h/h2421.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_18_10"></a>Ezekiel 18:10

If he [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md) that is a [periyts](../../strongs/h/h6530.md), a [šāp̄aḵ](../../strongs/h/h8210.md) of [dam](../../strongs/h/h1818.md), and that ['asah](../../strongs/h/h6213.md) the ['ach](../../strongs/h/h251.md) to any one of these,

<a name="ezekiel_18_11"></a>Ezekiel 18:11

And that ['asah](../../strongs/h/h6213.md) not any of those, but even hath ['akal](../../strongs/h/h398.md) upon the [har](../../strongs/h/h2022.md), and [ṭāmē'](../../strongs/h/h2930.md) his [rea'](../../strongs/h/h7453.md) ['ishshah](../../strongs/h/h802.md),

<a name="ezekiel_18_12"></a>Ezekiel 18:12

Hath [yānâ](../../strongs/h/h3238.md) the ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md), hath [gāzal](../../strongs/h/h1497.md) by [gᵊzēlâ](../../strongs/h/h1500.md), hath not [shuwb](../../strongs/h/h7725.md) the [ḥăḇōl](../../strongs/h/h2258.md), and hath [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md) to the [gillûl](../../strongs/h/h1544.md), hath ['asah](../../strongs/h/h6213.md) [tôʿēḇâ](../../strongs/h/h8441.md),

<a name="ezekiel_18_13"></a>Ezekiel 18:13

Hath [nathan](../../strongs/h/h5414.md) upon [neshek](../../strongs/h/h5392.md), and hath [laqach](../../strongs/h/h3947.md) [tarbîṯ](../../strongs/h/h8636.md): shall he then [chayay](../../strongs/h/h2425.md)? he shall not [ḥāyâ](../../strongs/h/h2421.md): he hath ['asah](../../strongs/h/h6213.md) all these [tôʿēḇâ](../../strongs/h/h8441.md); he shall [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md); his [dam](../../strongs/h/h1818.md) shall be upon him.

<a name="ezekiel_18_14"></a>Ezekiel 18:14

Now, lo, if he [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md), that [ra'ah](../../strongs/h/h7200.md) all his ['ab](../../strongs/h/h1.md) [chatta'ath](../../strongs/h/h2403.md) which he hath ['asah](../../strongs/h/h6213.md), and [ra'ah](../../strongs/h/h7200.md), and ['asah](../../strongs/h/h6213.md) not such like,

<a name="ezekiel_18_15"></a>Ezekiel 18:15

That hath not ['akal](../../strongs/h/h398.md) upon the [har](../../strongs/h/h2022.md), neither hath [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md) to the [gillûl](../../strongs/h/h1544.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), hath not [ṭāmē'](../../strongs/h/h2930.md) his [rea'](../../strongs/h/h7453.md) ['ishshah](../../strongs/h/h802.md),

<a name="ezekiel_18_16"></a>Ezekiel 18:16

Neither hath [yānâ](../../strongs/h/h3238.md) ['iysh](../../strongs/h/h376.md), hath not [chabal](../../strongs/h/h2254.md) the [ḥăḇōl](../../strongs/h/h2258.md), neither hath [gāzal](../../strongs/h/h1497.md) by [gᵊzēlâ](../../strongs/h/h1500.md), but hath [nathan](../../strongs/h/h5414.md) his [lechem](../../strongs/h/h3899.md) to the [rāʿēḇ](../../strongs/h/h7457.md), and hath [kāsâ](../../strongs/h/h3680.md) the ['eyrom](../../strongs/h/h5903.md) with a [beḡeḏ](../../strongs/h/h899.md),

<a name="ezekiel_18_17"></a>Ezekiel 18:17

That hath [shuwb](../../strongs/h/h7725.md) his [yad](../../strongs/h/h3027.md) from the ['aniy](../../strongs/h/h6041.md), that hath not [laqach](../../strongs/h/h3947.md) [neshek](../../strongs/h/h5392.md) nor [tarbîṯ](../../strongs/h/h8636.md), hath ['asah](../../strongs/h/h6213.md) my [mishpat](../../strongs/h/h4941.md), hath [halak](../../strongs/h/h1980.md) in my [chuqqah](../../strongs/h/h2708.md); he shall not [muwth](../../strongs/h/h4191.md) for the ['avon](../../strongs/h/h5771.md) of his ['ab](../../strongs/h/h1.md), he shall [ḥāyâ](../../strongs/h/h2421.md) [ḥāyâ](../../strongs/h/h2421.md).

<a name="ezekiel_18_18"></a>Ezekiel 18:18

As for his ['ab](../../strongs/h/h1.md), because he [ʿōšeq](../../strongs/h/h6233.md) [ʿāšaq](../../strongs/h/h6231.md), [gāzal](../../strongs/h/h1497.md) his ['ach](../../strongs/h/h251.md) by [gēzel](../../strongs/h/h1499.md), and ['asah](../../strongs/h/h6213.md) that which is not [towb](../../strongs/h/h2896.md) among his ['am](../../strongs/h/h5971.md), lo, even he shall [muwth](../../strongs/h/h4191.md) in his ['avon](../../strongs/h/h5771.md).

<a name="ezekiel_18_19"></a>Ezekiel 18:19

Yet ['āmar](../../strongs/h/h559.md) ye, Why? doth not the [ben](../../strongs/h/h1121.md) [nasa'](../../strongs/h/h5375.md) the ['avon](../../strongs/h/h5771.md) of the ['ab](../../strongs/h/h1.md)? When the [ben](../../strongs/h/h1121.md) hath ['asah](../../strongs/h/h6213.md) that which is [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md), and hath [shamar](../../strongs/h/h8104.md) all my [chuqqah](../../strongs/h/h2708.md), and hath ['asah](../../strongs/h/h6213.md) them, he shall [ḥāyâ](../../strongs/h/h2421.md) [ḥāyâ](../../strongs/h/h2421.md).

<a name="ezekiel_18_20"></a>Ezekiel 18:20

The [nephesh](../../strongs/h/h5315.md) that [chata'](../../strongs/h/h2398.md), it shall [muwth](../../strongs/h/h4191.md). The [ben](../../strongs/h/h1121.md) shall not [nasa'](../../strongs/h/h5375.md) the ['avon](../../strongs/h/h5771.md) of the ['ab](../../strongs/h/h1.md), neither shall the ['ab](../../strongs/h/h1.md) [nasa'](../../strongs/h/h5375.md) the ['avon](../../strongs/h/h5771.md) of the [ben](../../strongs/h/h1121.md): the [tsedaqah](../../strongs/h/h6666.md) of the [tsaddiyq](../../strongs/h/h6662.md) shall be upon him, and the [rišʿâ](../../strongs/h/h7564.md) of the [rasha'](../../strongs/h/h7563.md) shall be upon him.

<a name="ezekiel_18_21"></a>Ezekiel 18:21

But if the [rasha'](../../strongs/h/h7563.md) will [shuwb](../../strongs/h/h7725.md) from all his [chatta'ath](../../strongs/h/h2403.md) that he hath ['asah](../../strongs/h/h6213.md), and [shamar](../../strongs/h/h8104.md) all my [chuqqah](../../strongs/h/h2708.md), and ['asah](../../strongs/h/h6213.md) that which is [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md), he shall [ḥāyâ](../../strongs/h/h2421.md) [ḥāyâ](../../strongs/h/h2421.md), he shall not [muwth](../../strongs/h/h4191.md).

<a name="ezekiel_18_22"></a>Ezekiel 18:22

All his [pesha'](../../strongs/h/h6588.md) that he hath ['asah](../../strongs/h/h6213.md), they shall not be [zakar](../../strongs/h/h2142.md) unto him: in his [tsedaqah](../../strongs/h/h6666.md) that he hath ['asah](../../strongs/h/h6213.md) he shall [ḥāyâ](../../strongs/h/h2421.md).

<a name="ezekiel_18_23"></a>Ezekiel 18:23

Have I any [ḥāp̄ēṣ](../../strongs/h/h2654.md) at [ḥāp̄ēṣ](../../strongs/h/h2654.md) that the [rasha'](../../strongs/h/h7563.md) should [maveth](../../strongs/h/h4194.md)? [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md): and not that he should [shuwb](../../strongs/h/h7725.md) from his [derek](../../strongs/h/h1870.md), and [ḥāyâ](../../strongs/h/h2421.md)?

<a name="ezekiel_18_24"></a>Ezekiel 18:24

But when the [tsaddiyq](../../strongs/h/h6662.md) [shuwb](../../strongs/h/h7725.md) from his [tsedaqah](../../strongs/h/h6666.md), and ['asah](../../strongs/h/h6213.md) ['evel](../../strongs/h/h5766.md), and ['asah](../../strongs/h/h6213.md) according to all the [tôʿēḇâ](../../strongs/h/h8441.md) that the [rasha'](../../strongs/h/h7563.md) man ['asah](../../strongs/h/h6213.md), shall he [chayay](../../strongs/h/h2425.md)? All his [tsedaqah](../../strongs/h/h6666.md) that he hath ['asah](../../strongs/h/h6213.md) shall not be [zakar](../../strongs/h/h2142.md): in his [māʿal](../../strongs/h/h4603.md) that he hath [maʿal](../../strongs/h/h4604.md), and in his [chatta'ath](../../strongs/h/h2403.md) that he hath [chata'](../../strongs/h/h2398.md), in them shall he [muwth](../../strongs/h/h4191.md).

<a name="ezekiel_18_25"></a>Ezekiel 18:25

Yet ye ['āmar](../../strongs/h/h559.md), The [derek](../../strongs/h/h1870.md) of the ['adonay](../../strongs/h/h136.md) is not [tāḵan](../../strongs/h/h8505.md). [shama'](../../strongs/h/h8085.md) now, O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md); Is not my [derek](../../strongs/h/h1870.md) [tāḵan](../../strongs/h/h8505.md)? are not your [derek](../../strongs/h/h1870.md) [tāḵan](../../strongs/h/h8505.md)?

<a name="ezekiel_18_26"></a>Ezekiel 18:26

When a [tsaddiyq](../../strongs/h/h6662.md) [shuwb](../../strongs/h/h7725.md) from his [tsedaqah](../../strongs/h/h6666.md), and ['asah](../../strongs/h/h6213.md) ['evel](../../strongs/h/h5766.md), and [muwth](../../strongs/h/h4191.md) in them; for his ['evel](../../strongs/h/h5766.md) that he hath ['asah](../../strongs/h/h6213.md) shall he [muwth](../../strongs/h/h4191.md).

<a name="ezekiel_18_27"></a>Ezekiel 18:27

Again, when the [rasha'](../../strongs/h/h7563.md) [shuwb](../../strongs/h/h7725.md) from his [rišʿâ](../../strongs/h/h7564.md) that he hath ['asah](../../strongs/h/h6213.md), and ['asah](../../strongs/h/h6213.md) that which is [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md), he shall [ḥāyâ](../../strongs/h/h2421.md) his [nephesh](../../strongs/h/h5315.md) [ḥāyâ](../../strongs/h/h2421.md).

<a name="ezekiel_18_28"></a>Ezekiel 18:28

Because he [ra'ah](../../strongs/h/h7200.md), and [shuwb](../../strongs/h/h7725.md) from all his [pesha'](../../strongs/h/h6588.md) that he hath ['asah](../../strongs/h/h6213.md), he shall [ḥāyâ](../../strongs/h/h2421.md) [ḥāyâ](../../strongs/h/h2421.md), he shall not [muwth](../../strongs/h/h4191.md).

<a name="ezekiel_18_29"></a>Ezekiel 18:29

Yet ['āmar](../../strongs/h/h559.md) the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), The [derek](../../strongs/h/h1870.md) of the ['adonay](../../strongs/h/h136.md) is not [tāḵan](../../strongs/h/h8505.md). O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), are not my [derek](../../strongs/h/h1870.md) [tāḵan](../../strongs/h/h8505.md)? are not your [derek](../../strongs/h/h1870.md) [tāḵan](../../strongs/h/h8505.md)?

<a name="ezekiel_18_30"></a>Ezekiel 18:30

Therefore I will [shaphat](../../strongs/h/h8199.md) you, O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), every ['iysh](../../strongs/h/h376.md) according to his [derek](../../strongs/h/h1870.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md). [shuwb](../../strongs/h/h7725.md), and [shuwb](../../strongs/h/h7725.md) from all your [pesha'](../../strongs/h/h6588.md); so ['avon](../../strongs/h/h5771.md) shall not be your [miḵšôl](../../strongs/h/h4383.md).

<a name="ezekiel_18_31"></a>Ezekiel 18:31

[shalak](../../strongs/h/h7993.md) from you all your [pesha'](../../strongs/h/h6588.md), whereby ye have [pāšaʿ](../../strongs/h/h6586.md); and ['asah](../../strongs/h/h6213.md) you a [ḥāḏāš](../../strongs/h/h2319.md) [leb](../../strongs/h/h3820.md) and a [ḥāḏāš](../../strongs/h/h2319.md) [ruwach](../../strongs/h/h7307.md): for why will ye [muwth](../../strongs/h/h4191.md), O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="ezekiel_18_32"></a>Ezekiel 18:32

For I have no [ḥāp̄ēṣ](../../strongs/h/h2654.md) in the [maveth](../../strongs/h/h4194.md) of him that [muwth](../../strongs/h/h4191.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md): wherefore [shuwb](../../strongs/h/h7725.md), and [ḥāyâ](../../strongs/h/h2421.md) ye.

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 17](ezekiel_17.md) - [Ezekiel 19](ezekiel_19.md)