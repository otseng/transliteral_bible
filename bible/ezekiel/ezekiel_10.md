# [Ezekiel 10](https://www.blueletterbible.org/kjv/ezekiel/10)

<a name="ezekiel_10_1"></a>Ezekiel 10:1

Then I [ra'ah](../../strongs/h/h7200.md), and, behold, in the [raqiya'](../../strongs/h/h7549.md) that was above the [ro'sh](../../strongs/h/h7218.md) of the [kĕruwb](../../strongs/h/h3742.md) there [ra'ah](../../strongs/h/h7200.md) over them as it were a [sapîr](../../strongs/h/h5601.md) ['eben](../../strongs/h/h68.md), as the [mar'ê](../../strongs/h/h4758.md) of the [dĕmuwth](../../strongs/h/h1823.md) of a [kicce'](../../strongs/h/h3678.md).

<a name="ezekiel_10_2"></a>Ezekiel 10:2

And he ['āmar](../../strongs/h/h559.md) unto the ['iysh](../../strongs/h/h376.md) [labash](../../strongs/h/h3847.md) with [baḏ](../../strongs/h/h906.md), and ['āmar](../../strongs/h/h559.md), [bow'](../../strongs/h/h935.md) between the [galgal](../../strongs/h/h1534.md), even under the [kĕruwb](../../strongs/h/h3742.md), and [mālā'](../../strongs/h/h4390.md) thine [ḥōp̄en](../../strongs/h/h2651.md) with [gechel](../../strongs/h/h1513.md) of ['esh](../../strongs/h/h784.md) from between the [kĕruwb](../../strongs/h/h3742.md), and [zāraq](../../strongs/h/h2236.md) them over the [ʿîr](../../strongs/h/h5892.md). And he [bow'](../../strongs/h/h935.md) in my ['ayin](../../strongs/h/h5869.md).

<a name="ezekiel_10_3"></a>Ezekiel 10:3

Now the [kĕruwb](../../strongs/h/h3742.md) ['amad](../../strongs/h/h5975.md) on the [yamiyn](../../strongs/h/h3225.md) of the [bayith](../../strongs/h/h1004.md), when the ['iysh](../../strongs/h/h376.md) [bow'](../../strongs/h/h935.md); and the [ʿānān](../../strongs/h/h6051.md) [mālā'](../../strongs/h/h4390.md) the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md).

<a name="ezekiel_10_4"></a>Ezekiel 10:4

Then the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) went [ruwm](../../strongs/h/h7311.md) from the [kĕruwb](../../strongs/h/h3742.md), and stood over the [mip̄tān](../../strongs/h/h4670.md) of the [bayith](../../strongs/h/h1004.md); and the [bayith](../../strongs/h/h1004.md) was [mālā'](../../strongs/h/h4390.md) with the [ʿānān](../../strongs/h/h6051.md), and the [ḥāṣēr](../../strongs/h/h2691.md) was [mālā'](../../strongs/h/h4390.md) of the [nogahh](../../strongs/h/h5051.md) of [Yĕhovah](../../strongs/h/h3068.md) [kabowd](../../strongs/h/h3519.md).

<a name="ezekiel_10_5"></a>Ezekiel 10:5

And the [qowl](../../strongs/h/h6963.md) of the [kĕruwb](../../strongs/h/h3742.md) [kanaph](../../strongs/h/h3671.md) was [shama'](../../strongs/h/h8085.md) even to the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md), as the [qowl](../../strongs/h/h6963.md) of the [Šaday](../../strongs/h/h7706.md) ['el](../../strongs/h/h410.md) when he [dabar](../../strongs/h/h1696.md).

<a name="ezekiel_10_6"></a>Ezekiel 10:6

And it came to pass, that when he had [tsavah](../../strongs/h/h6680.md) the ['iysh](../../strongs/h/h376.md) [labash](../../strongs/h/h3847.md) with [baḏ](../../strongs/h/h906.md), ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) ['esh](../../strongs/h/h784.md) from between the [galgal](../../strongs/h/h1534.md), from between the [kĕruwb](../../strongs/h/h3742.md); then he [bow'](../../strongs/h/h935.md), and ['amad](../../strongs/h/h5975.md) beside the ['ôp̄ān](../../strongs/h/h212.md).

<a name="ezekiel_10_7"></a>Ezekiel 10:7

And one [kĕruwb](../../strongs/h/h3742.md) [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) from between the [kĕruwb](../../strongs/h/h3742.md) unto the ['esh](../../strongs/h/h784.md) that was between the [kĕruwb](../../strongs/h/h3742.md), and [nasa'](../../strongs/h/h5375.md) thereof, and [nathan](../../strongs/h/h5414.md) it into the [ḥōp̄en](../../strongs/h/h2651.md) of him that was [labash](../../strongs/h/h3847.md) with [baḏ](../../strongs/h/h906.md): who [laqach](../../strongs/h/h3947.md) it, and [yāṣā'](../../strongs/h/h3318.md).

<a name="ezekiel_10_8"></a>Ezekiel 10:8

And there [ra'ah](../../strongs/h/h7200.md) in the [kĕruwb](../../strongs/h/h3742.md) the [taḇnîṯ](../../strongs/h/h8403.md) of an ['āḏām](../../strongs/h/h120.md) [yad](../../strongs/h/h3027.md) under their [kanaph](../../strongs/h/h3671.md).

<a name="ezekiel_10_9"></a>Ezekiel 10:9

And when I [ra'ah](../../strongs/h/h7200.md), behold the four ['ôp̄ān](../../strongs/h/h212.md) by the [kĕruwb](../../strongs/h/h3742.md), one ['ôp̄ān](../../strongs/h/h212.md) by one [kĕruwb](../../strongs/h/h3742.md), and another ['ôp̄ān](../../strongs/h/h212.md) by another [kĕruwb](../../strongs/h/h3742.md): and the [mar'ê](../../strongs/h/h4758.md) of the ['ôp̄ān](../../strongs/h/h212.md) was as the ['ayin](../../strongs/h/h5869.md) of a [taršîš](../../strongs/h/h8658.md) ['eben](../../strongs/h/h68.md).

<a name="ezekiel_10_10"></a>Ezekiel 10:10

And as for their [mar'ê](../../strongs/h/h4758.md), they four had one [dĕmuwth](../../strongs/h/h1823.md), as if a ['ôp̄ān](../../strongs/h/h212.md) had been in the midst of a ['ôp̄ān](../../strongs/h/h212.md).

<a name="ezekiel_10_11"></a>Ezekiel 10:11

When they [yālaḵ](../../strongs/h/h3212.md), they [yālaḵ](../../strongs/h/h3212.md) upon their four sides; they [cabab](../../strongs/h/h5437.md) not as they [yālaḵ](../../strongs/h/h3212.md), but to the [maqowm](../../strongs/h/h4725.md) whither the [ro'sh](../../strongs/h/h7218.md) [panah](../../strongs/h/h6437.md) they [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) it; they [cabab](../../strongs/h/h5437.md) not as they [yālaḵ](../../strongs/h/h3212.md).

<a name="ezekiel_10_12"></a>Ezekiel 10:12

And their whole [basar](../../strongs/h/h1320.md), and their [gaḇ](../../strongs/h/h1354.md), and their [yad](../../strongs/h/h3027.md), and their [kanaph](../../strongs/h/h3671.md), and the ['ôp̄ān](../../strongs/h/h212.md), were [mālē'](../../strongs/h/h4392.md) of ['ayin](../../strongs/h/h5869.md) [cabiyb](../../strongs/h/h5439.md), even the ['ôp̄ān](../../strongs/h/h212.md) that they four had.

<a name="ezekiel_10_13"></a>Ezekiel 10:13

As for the ['ôp̄ān](../../strongs/h/h212.md), it was [qara'](../../strongs/h/h7121.md) unto them in my ['ozen](../../strongs/h/h241.md), O [galgal](../../strongs/h/h1534.md).

<a name="ezekiel_10_14"></a>Ezekiel 10:14

And every one had four [paniym](../../strongs/h/h6440.md): the first [paniym](../../strongs/h/h6440.md) was the [paniym](../../strongs/h/h6440.md) of a [kĕruwb](../../strongs/h/h3742.md), and the second [paniym](../../strongs/h/h6440.md) was the [paniym](../../strongs/h/h6440.md) of an ['āḏām](../../strongs/h/h120.md), and the third the [paniym](../../strongs/h/h6440.md) of an ['ariy](../../strongs/h/h738.md), and the fourth the [paniym](../../strongs/h/h6440.md) of a [nesheׁr](../../strongs/h/h5404.md).

<a name="ezekiel_10_15"></a>Ezekiel 10:15

And the [kĕruwb](../../strongs/h/h3742.md) were lifted [rāmam](../../strongs/h/h7426.md). This is the [chay](../../strongs/h/h2416.md) that I [ra'ah](../../strongs/h/h7200.md) by the [nāhār](../../strongs/h/h5104.md) of [kᵊḇār](../../strongs/h/h3529.md).

<a name="ezekiel_10_16"></a>Ezekiel 10:16

And when the [kĕruwb](../../strongs/h/h3742.md) [yālaḵ](../../strongs/h/h3212.md), the ['ôp̄ān](../../strongs/h/h212.md) [yālaḵ](../../strongs/h/h3212.md) by them: and when the [kĕruwb](../../strongs/h/h3742.md) [nasa'](../../strongs/h/h5375.md) their [kanaph](../../strongs/h/h3671.md) to mount [ruwm](../../strongs/h/h7311.md) from the ['erets](../../strongs/h/h776.md), the same ['ôp̄ān](../../strongs/h/h212.md) also [cabab](../../strongs/h/h5437.md) not from beside them.

<a name="ezekiel_10_17"></a>Ezekiel 10:17

When they ['amad](../../strongs/h/h5975.md), these ['amad](../../strongs/h/h5975.md); and when they were [ruwm](../../strongs/h/h7311.md), these [rāmam](../../strongs/h/h7426.md) themselves also: for the [ruwach](../../strongs/h/h7307.md) of the [chay](../../strongs/h/h2416.md) was in them.

<a name="ezekiel_10_18"></a>Ezekiel 10:18

Then the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) from off the [mip̄tān](../../strongs/h/h4670.md) of the [bayith](../../strongs/h/h1004.md), and ['amad](../../strongs/h/h5975.md) over the [kĕruwb](../../strongs/h/h3742.md).

<a name="ezekiel_10_19"></a>Ezekiel 10:19

And the [kĕruwb](../../strongs/h/h3742.md) [nasa'](../../strongs/h/h5375.md) their [kanaph](../../strongs/h/h3671.md), and [rāmam](../../strongs/h/h7426.md) from the ['erets](../../strongs/h/h776.md) in my ['ayin](../../strongs/h/h5869.md): when they [yāṣā'](../../strongs/h/h3318.md), the ['ôp̄ān](../../strongs/h/h212.md) also were [ʿummâ](../../strongs/h/h5980.md) them, and every one ['amad](../../strongs/h/h5975.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the [qaḏmōnî](../../strongs/h/h6931.md) [sha'ar](../../strongs/h/h8179.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md); and the [kabowd](../../strongs/h/h3519.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) was over them [maʿal](../../strongs/h/h4605.md).

<a name="ezekiel_10_20"></a>Ezekiel 10:20

This is the [chay](../../strongs/h/h2416.md) that I [ra'ah](../../strongs/h/h7200.md) under the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) by the [nāhār](../../strongs/h/h5104.md) of [kᵊḇār](../../strongs/h/h3529.md); and I [yada'](../../strongs/h/h3045.md) that they were the [kĕruwb](../../strongs/h/h3742.md).

<a name="ezekiel_10_21"></a>Ezekiel 10:21

Every one had four [paniym](../../strongs/h/h6440.md) apiece, and every one four [kanaph](../../strongs/h/h3671.md); and the [dĕmuwth](../../strongs/h/h1823.md) of the [yad](../../strongs/h/h3027.md) of an ['āḏām](../../strongs/h/h120.md) was under their [kanaph](../../strongs/h/h3671.md).

<a name="ezekiel_10_22"></a>Ezekiel 10:22

And the [dĕmuwth](../../strongs/h/h1823.md) of their [paniym](../../strongs/h/h6440.md) was the same [paniym](../../strongs/h/h6440.md) which I [ra'ah](../../strongs/h/h7200.md) by the [nāhār](../../strongs/h/h5104.md) of [kᵊḇār](../../strongs/h/h3529.md), their [mar'ê](../../strongs/h/h4758.md) and themselves: they [yālaḵ](../../strongs/h/h3212.md) every ['iysh](../../strongs/h/h376.md) [ʿēḇer](../../strongs/h/h5676.md) [paniym](../../strongs/h/h6440.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 9](ezekiel_9.md) - [Ezekiel 11](ezekiel_11.md)