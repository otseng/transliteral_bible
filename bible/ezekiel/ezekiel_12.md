# [Ezekiel 12](https://www.blueletterbible.org/kjv/ezekiel/12)

<a name="ezekiel_12_1"></a>Ezekiel 12:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) also came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_12_2"></a>Ezekiel 12:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), thou [yashab](../../strongs/h/h3427.md) in the midst of a [mᵊrî](../../strongs/h/h4805.md) [bayith](../../strongs/h/h1004.md), which have ['ayin](../../strongs/h/h5869.md) to [ra'ah](../../strongs/h/h7200.md), and [ra'ah](../../strongs/h/h7200.md) not; they have ['ozen](../../strongs/h/h241.md) to [shama'](../../strongs/h/h8085.md), and [shama'](../../strongs/h/h8085.md) not: for they are a [mᵊrî](../../strongs/h/h4805.md) [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_12_3"></a>Ezekiel 12:3

Therefore, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), ['asah](../../strongs/h/h6213.md) thee [kĕliy](../../strongs/h/h3627.md) for [gôlâ](../../strongs/h/h1473.md), and [gālâ](../../strongs/h/h1540.md) by [yômām](../../strongs/h/h3119.md) in their ['ayin](../../strongs/h/h5869.md); and thou shalt [gālâ](../../strongs/h/h1540.md) from thy [maqowm](../../strongs/h/h4725.md) to ['aḥēr](../../strongs/h/h312.md) [maqowm](../../strongs/h/h4725.md) in their ['ayin](../../strongs/h/h5869.md): it may be they will [ra'ah](../../strongs/h/h7200.md), though they be a [mᵊrî](../../strongs/h/h4805.md) [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_12_4"></a>Ezekiel 12:4

Then shalt thou [yāṣā'](../../strongs/h/h3318.md) thy [kĕliy](../../strongs/h/h3627.md) by [yômām](../../strongs/h/h3119.md) in their ['ayin](../../strongs/h/h5869.md), as [kĕliy](../../strongs/h/h3627.md) for [gôlâ](../../strongs/h/h1473.md): and thou shalt [yāṣā'](../../strongs/h/h3318.md) at ['ereb](../../strongs/h/h6153.md) in their ['ayin](../../strongs/h/h5869.md), as they that go [môṣā'](../../strongs/h/h4161.md) into [gôlâ](../../strongs/h/h1473.md).

<a name="ezekiel_12_5"></a>Ezekiel 12:5

[ḥāṯar](../../strongs/h/h2864.md) thou through the [qîr](../../strongs/h/h7023.md) in their ['ayin](../../strongs/h/h5869.md), and carry [yāṣā'](../../strongs/h/h3318.md) thereby.

<a name="ezekiel_12_6"></a>Ezekiel 12:6

In their ['ayin](../../strongs/h/h5869.md) shalt thou [nasa'](../../strongs/h/h5375.md) it upon thy [kāṯēp̄](../../strongs/h/h3802.md), and [yāṣā'](../../strongs/h/h3318.md) it in the [ʿălāṭâ](../../strongs/h/h5939.md): thou shalt [kāsâ](../../strongs/h/h3680.md) thy [paniym](../../strongs/h/h6440.md), that thou [ra'ah](../../strongs/h/h7200.md) not the ['erets](../../strongs/h/h776.md): for I have [nathan](../../strongs/h/h5414.md) thee for a [môp̄ēṯ](../../strongs/h/h4159.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_12_7"></a>Ezekiel 12:7

And I ['asah](../../strongs/h/h6213.md) so as I was [tsavah](../../strongs/h/h6680.md): I [yāṣā'](../../strongs/h/h3318.md) my [kĕliy](../../strongs/h/h3627.md) by [yômām](../../strongs/h/h3119.md), as [kĕliy](../../strongs/h/h3627.md) for [gôlâ](../../strongs/h/h1473.md), and in the ['ereb](../../strongs/h/h6153.md) I [ḥāṯar](../../strongs/h/h2864.md) through the [qîr](../../strongs/h/h7023.md) with mine [yad](../../strongs/h/h3027.md); I [yāṣā'](../../strongs/h/h3318.md) it in the [ʿălāṭâ](../../strongs/h/h5939.md), and I [nasa'](../../strongs/h/h5375.md) it upon my [kāṯēp̄](../../strongs/h/h3802.md) in their ['ayin](../../strongs/h/h5869.md).

<a name="ezekiel_12_8"></a>Ezekiel 12:8

And in the [boqer](../../strongs/h/h1242.md) came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_12_9"></a>Ezekiel 12:9

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), hath not the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), the [mᵊrî](../../strongs/h/h4805.md) [bayith](../../strongs/h/h1004.md), ['āmar](../../strongs/h/h559.md) unto thee, What ['asah](../../strongs/h/h6213.md) thou?

<a name="ezekiel_12_10"></a>Ezekiel 12:10

['āmar](../../strongs/h/h559.md) thou unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); This [maśśā'](../../strongs/h/h4853.md) concerneth the [nāśî'](../../strongs/h/h5387.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), and all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) that are among them.

<a name="ezekiel_12_11"></a>Ezekiel 12:11

['āmar](../../strongs/h/h559.md), I am your [môp̄ēṯ](../../strongs/h/h4159.md): like as I have ['asah](../../strongs/h/h6213.md), so shall it be ['asah](../../strongs/h/h6213.md) unto them: they shall [gôlâ](../../strongs/h/h1473.md) and [yālaḵ](../../strongs/h/h3212.md) into [šᵊḇî](../../strongs/h/h7628.md).

<a name="ezekiel_12_12"></a>Ezekiel 12:12

And the [nāśî'](../../strongs/h/h5387.md) that is among them shall [nasa'](../../strongs/h/h5375.md) upon his [kāṯēp̄](../../strongs/h/h3802.md) in the [ʿălāṭâ](../../strongs/h/h5939.md), and shall [yāṣā'](../../strongs/h/h3318.md): they shall [ḥāṯar](../../strongs/h/h2864.md) through the [qîr](../../strongs/h/h7023.md) to carry [yāṣā'](../../strongs/h/h3318.md) thereby: he shall [kāsâ](../../strongs/h/h3680.md) his [paniym](../../strongs/h/h6440.md), that he [ra'ah](../../strongs/h/h7200.md) not the ['erets](../../strongs/h/h776.md) with his ['ayin](../../strongs/h/h5869.md).

<a name="ezekiel_12_13"></a>Ezekiel 12:13

My [rešeṯ](../../strongs/h/h7568.md) also will I [pāraś](../../strongs/h/h6566.md) upon him, and he shall be [tāp̄aś](../../strongs/h/h8610.md) in my [matsuwd](../../strongs/h/h4686.md): and I will [bow'](../../strongs/h/h935.md) him to [Bāḇel](../../strongs/h/h894.md) to the ['erets](../../strongs/h/h776.md) of the [Kaśdîmâ](../../strongs/h/h3778.md); yet shall he not [ra'ah](../../strongs/h/h7200.md) it, though he shall [muwth](../../strongs/h/h4191.md) there.

<a name="ezekiel_12_14"></a>Ezekiel 12:14

And I will [zārâ](../../strongs/h/h2219.md) toward every [ruwach](../../strongs/h/h7307.md) all that are [cabiyb](../../strongs/h/h5439.md) him to ['ezer](../../strongs/h/h5828.md) him, and all his ['ăḡap̄](../../strongs/h/h102.md); and I will [rîq](../../strongs/h/h7324.md) the [chereb](../../strongs/h/h2719.md) ['aḥar](../../strongs/h/h310.md) them.

<a name="ezekiel_12_15"></a>Ezekiel 12:15

And they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when I shall [puwts](../../strongs/h/h6327.md) them among the [gowy](../../strongs/h/h1471.md), and [zārâ](../../strongs/h/h2219.md) them in the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_12_16"></a>Ezekiel 12:16

But I will [yāṯar](../../strongs/h/h3498.md) a [mispār](../../strongs/h/h4557.md) ['enowsh](../../strongs/h/h582.md) of them from the [chereb](../../strongs/h/h2719.md), from the [rāʿāḇ](../../strongs/h/h7458.md), and from the [deḇer](../../strongs/h/h1698.md); that they may [sāp̄ar](../../strongs/h/h5608.md) all their [tôʿēḇâ](../../strongs/h/h8441.md) among the [gowy](../../strongs/h/h1471.md) whither they [bow'](../../strongs/h/h935.md); and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_12_17"></a>Ezekiel 12:17

Moreover the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_12_18"></a>Ezekiel 12:18

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), ['akal](../../strongs/h/h398.md) thy [lechem](../../strongs/h/h3899.md) with [raʿaš](../../strongs/h/h7494.md), and [šāṯâ](../../strongs/h/h8354.md) thy [mayim](../../strongs/h/h4325.md) with [rāḡzâ](../../strongs/h/h7269.md) and with [dᵊ'āḡâ](../../strongs/h/h1674.md);

<a name="ezekiel_12_19"></a>Ezekiel 12:19

And ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md) of the ['ăḏāmâ](../../strongs/h/h127.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and of the ['erets](../../strongs/h/h776.md) of [Yisra'el](../../strongs/h/h3478.md); They shall ['akal](../../strongs/h/h398.md) their [lechem](../../strongs/h/h3899.md) with [dᵊ'āḡâ](../../strongs/h/h1674.md), and [šāṯâ](../../strongs/h/h8354.md) their [mayim](../../strongs/h/h4325.md) with [šimmāmôn](../../strongs/h/h8078.md), that her ['erets](../../strongs/h/h776.md) may be [yāšam](../../strongs/h/h3456.md) from all that is [mᵊlō'](../../strongs/h/h4393.md), because of the [chamac](../../strongs/h/h2555.md) of all them that [yashab](../../strongs/h/h3427.md) therein.

<a name="ezekiel_12_20"></a>Ezekiel 12:20

And the [ʿîr](../../strongs/h/h5892.md) that are [yashab](../../strongs/h/h3427.md) shall be [ḥāraḇ](../../strongs/h/h2717.md), and the ['erets](../../strongs/h/h776.md) shall be [šᵊmāmâ](../../strongs/h/h8077.md); and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_12_21"></a>Ezekiel 12:21

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_12_22"></a>Ezekiel 12:22

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), what is that [māšāl](../../strongs/h/h4912.md) that ye have in the ['ăḏāmâ](../../strongs/h/h127.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), The [yowm](../../strongs/h/h3117.md) are ['arak](../../strongs/h/h748.md), and every [ḥāzôn](../../strongs/h/h2377.md) ['abad](../../strongs/h/h6.md)?

<a name="ezekiel_12_23"></a>Ezekiel 12:23

['āmar](../../strongs/h/h559.md) them therefore, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); I will make this [māšāl](../../strongs/h/h4912.md) to [shabath](../../strongs/h/h7673.md), and they shall no more use it as a [māšal](../../strongs/h/h4911.md) in [Yisra'el](../../strongs/h/h3478.md); but [dabar](../../strongs/h/h1696.md) unto them, The [yowm](../../strongs/h/h3117.md) are at [qāraḇ](../../strongs/h/h7126.md), and the [dabar](../../strongs/h/h1697.md) of every [ḥāzôn](../../strongs/h/h2377.md).

<a name="ezekiel_12_24"></a>Ezekiel 12:24

For there shall be no more any [shav'](../../strongs/h/h7723.md) [ḥāzôn](../../strongs/h/h2377.md) nor [ḥālāq](../../strongs/h/h2509.md) [miqsām](../../strongs/h/h4738.md) within the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_12_25"></a>Ezekiel 12:25

For I am [Yĕhovah](../../strongs/h/h3068.md): I will [dabar](../../strongs/h/h1696.md), and the [dabar](../../strongs/h/h1697.md) that I shall [dabar](../../strongs/h/h1696.md) shall come to ['asah](../../strongs/h/h6213.md); it shall be no more [mashak](../../strongs/h/h4900.md): for in your [yowm](../../strongs/h/h3117.md), O [mᵊrî](../../strongs/h/h4805.md) [bayith](../../strongs/h/h1004.md), will I [dabar](../../strongs/h/h1696.md) the [dabar](../../strongs/h/h1697.md), and will ['asah](../../strongs/h/h6213.md) it, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_12_26"></a>Ezekiel 12:26

Again the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_12_27"></a>Ezekiel 12:27

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), behold, they of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md), The [ḥāzôn](../../strongs/h/h2377.md) that he [chazah](../../strongs/h/h2372.md) is for [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md) to come, and he [nāḇā'](../../strongs/h/h5012.md) of the [ʿēṯ](../../strongs/h/h6256.md) that are [rachowq](../../strongs/h/h7350.md) off.

<a name="ezekiel_12_28"></a>Ezekiel 12:28

Therefore ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); There shall none of my [dabar](../../strongs/h/h1697.md) be [mashak](../../strongs/h/h4900.md) any more, but the [dabar](../../strongs/h/h1697.md) which I have [dabar](../../strongs/h/h1696.md) shall be ['asah](../../strongs/h/h6213.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 11](ezekiel_11.md) - [Ezekiel 13](ezekiel_13.md)