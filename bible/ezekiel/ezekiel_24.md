# [Ezekiel 24](https://www.blueletterbible.org/kjv/ezekiel/24)

<a name="ezekiel_24_1"></a>Ezekiel 24:1

Again in the ninth [šānâ](../../strongs/h/h8141.md), in the tenth [ḥōḏeš](../../strongs/h/h2320.md), in the tenth day of the [ḥōḏeš](../../strongs/h/h2320.md), the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_24_2"></a>Ezekiel 24:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [kāṯaḇ](../../strongs/h/h3789.md) thee the [shem](../../strongs/h/h8034.md) of the [yowm](../../strongs/h/h3117.md), even of this ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md): the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [camak](../../strongs/h/h5564.md) himself against [Yĕruwshalaim](../../strongs/h/h3389.md) this ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md).

<a name="ezekiel_24_3"></a>Ezekiel 24:3

And [māšal](../../strongs/h/h4911.md) a [māšāl](../../strongs/h/h4912.md) unto the [mᵊrî](../../strongs/h/h4805.md) [bayith](../../strongs/h/h1004.md), and ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); [šāp̄aṯ](../../strongs/h/h8239.md) a [sîr](../../strongs/h/h5518.md), [šāp̄aṯ](../../strongs/h/h8239.md) it, and also [yāṣaq](../../strongs/h/h3332.md) [mayim](../../strongs/h/h4325.md) into it:

<a name="ezekiel_24_4"></a>Ezekiel 24:4

['āsap̄](../../strongs/h/h622.md) the [nēṯaḥ](../../strongs/h/h5409.md) thereof into it, even every [towb](../../strongs/h/h2896.md) [nēṯaḥ](../../strongs/h/h5409.md), the [yārēḵ](../../strongs/h/h3409.md), and the [kāṯēp̄](../../strongs/h/h3802.md); [mālā'](../../strongs/h/h4390.md) it with the [miḇḥār](../../strongs/h/h4005.md) ['etsem](../../strongs/h/h6106.md).

<a name="ezekiel_24_5"></a>Ezekiel 24:5

[laqach](../../strongs/h/h3947.md) the [miḇḥār](../../strongs/h/h4005.md) of the [tso'n](../../strongs/h/h6629.md), and [dûr](../../strongs/h/h1754.md) also the ['etsem](../../strongs/h/h6106.md) under it, and make it [rāṯaḥ](../../strongs/h/h7570.md) [reṯaḥ](../../strongs/h/h7571.md), and let them [bāšal](../../strongs/h/h1310.md) the ['etsem](../../strongs/h/h6106.md) of it therein.

<a name="ezekiel_24_6"></a>Ezekiel 24:6

Wherefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); ['owy](../../strongs/h/h188.md) to the [dam](../../strongs/h/h1818.md) [ʿîr](../../strongs/h/h5892.md), to the [sîr](../../strongs/h/h5518.md) whose [ḥel'â](../../strongs/h/h2457.md) is therein, and whose [ḥel'â](../../strongs/h/h2457.md) is not [yāṣā'](../../strongs/h/h3318.md) of it! [yāṣā'](../../strongs/h/h3318.md) it [nēṯaḥ](../../strongs/h/h5409.md) by [nēṯaḥ](../../strongs/h/h5409.md); let no [gôrāl](../../strongs/h/h1486.md) [naphal](../../strongs/h/h5307.md) upon it.

<a name="ezekiel_24_7"></a>Ezekiel 24:7

For her [dam](../../strongs/h/h1818.md) is in the midst of her; she [śûm](../../strongs/h/h7760.md) it upon the [ṣᵊḥîaḥ](../../strongs/h/h6706.md) of a [cela'](../../strongs/h/h5553.md); she [šāp̄aḵ](../../strongs/h/h8210.md) it not upon the ['erets](../../strongs/h/h776.md), to [kāsâ](../../strongs/h/h3680.md) it with ['aphar](../../strongs/h/h6083.md);

<a name="ezekiel_24_8"></a>Ezekiel 24:8

That it might cause [chemah](../../strongs/h/h2534.md) to [ʿālâ](../../strongs/h/h5927.md) to [naqam](../../strongs/h/h5358.md) [nāqām](../../strongs/h/h5359.md); I have [nathan](../../strongs/h/h5414.md) her [dam](../../strongs/h/h1818.md) upon the [ṣᵊḥîaḥ](../../strongs/h/h6706.md) of a [cela'](../../strongs/h/h5553.md), that it should not be [kāsâ](../../strongs/h/h3680.md).

<a name="ezekiel_24_9"></a>Ezekiel 24:9

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); ['owy](../../strongs/h/h188.md) to the [dam](../../strongs/h/h1818.md) [ʿîr](../../strongs/h/h5892.md)! I will even make the [mᵊḏûrâ](../../strongs/h/h4071.md) [gāḏal](../../strongs/h/h1431.md).

<a name="ezekiel_24_10"></a>Ezekiel 24:10

[rabah](../../strongs/h/h7235.md) on ['ets](../../strongs/h/h6086.md), [dalaq](../../strongs/h/h1814.md) the ['esh](../../strongs/h/h784.md), [tamam](../../strongs/h/h8552.md) the [basar](../../strongs/h/h1320.md), and [rāqaḥ](../../strongs/h/h7543.md) it [merqāḥâ](../../strongs/h/h4841.md), and let the ['etsem](../../strongs/h/h6106.md) be [ḥārar](../../strongs/h/h2787.md).

<a name="ezekiel_24_11"></a>Ezekiel 24:11

Then ['amad](../../strongs/h/h5975.md) it [reyq](../../strongs/h/h7386.md) upon the [gechel](../../strongs/h/h1513.md) thereof, that the [nᵊḥšeṯ](../../strongs/h/h5178.md) of it may be [yāḥam](../../strongs/h/h3179.md), and may [ḥārar](../../strongs/h/h2787.md), and that the [ṭām'â](../../strongs/h/h2932.md) of it may be [nāṯaḵ](../../strongs/h/h5413.md) in it, that the [ḥel'â](../../strongs/h/h2457.md) of it may be [tamam](../../strongs/h/h8552.md).

<a name="ezekiel_24_12"></a>Ezekiel 24:12

She hath [lā'â](../../strongs/h/h3811.md) herself with [tᵊ'unîm](../../strongs/h/h8383.md), and her [rab](../../strongs/h/h7227.md) [ḥel'â](../../strongs/h/h2457.md) went not [yāṣā'](../../strongs/h/h3318.md) out of her: her [ḥel'â](../../strongs/h/h2457.md) shall be in the ['esh](../../strongs/h/h784.md).

<a name="ezekiel_24_13"></a>Ezekiel 24:13

In thy [ṭām'â](../../strongs/h/h2932.md) is [zimmâ](../../strongs/h/h2154.md): because I have [ṭāhēr](../../strongs/h/h2891.md) thee, and thou wast not [ṭāhēr](../../strongs/h/h2891.md), thou shalt not be [ṭāhēr](../../strongs/h/h2891.md) from thy [ṭām'â](../../strongs/h/h2932.md) any more, till I have caused my [chemah](../../strongs/h/h2534.md) to [nuwach](../../strongs/h/h5117.md) upon thee.

<a name="ezekiel_24_14"></a>Ezekiel 24:14

I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) it: it shall [bow'](../../strongs/h/h935.md), and I will ['asah](../../strongs/h/h6213.md) it; I will not go [pāraʿ](../../strongs/h/h6544.md), neither will I [ḥûs](../../strongs/h/h2347.md), neither will I [nacham](../../strongs/h/h5162.md); according to thy [derek](../../strongs/h/h1870.md), and according to thy ['aliylah](../../strongs/h/h5949.md), shall they [shaphat](../../strongs/h/h8199.md) thee, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_24_15"></a>Ezekiel 24:15

Also the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_24_16"></a>Ezekiel 24:16

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), behold, I [laqach](../../strongs/h/h3947.md) from thee the [maḥmāḏ](../../strongs/h/h4261.md) of thine ['ayin](../../strongs/h/h5869.md) with a [magēp̄â](../../strongs/h/h4046.md): yet neither shalt thou [sāp̄aḏ](../../strongs/h/h5594.md) nor [bāḵâ](../../strongs/h/h1058.md), neither shall thy [dim'ah](../../strongs/h/h1832.md) run [bow'](../../strongs/h/h935.md).

<a name="ezekiel_24_17"></a>Ezekiel 24:17

[damam](../../strongs/h/h1826.md) to ['ānaq](../../strongs/h/h602.md), ['asah](../../strongs/h/h6213.md) no ['ēḇel](../../strongs/h/h60.md) for the [muwth](../../strongs/h/h4191.md), [ḥāḇaš](../../strongs/h/h2280.md) the tire of thine [pᵊ'ēr](../../strongs/h/h6287.md) upon thee, and put [śûm](../../strongs/h/h7760.md) thy [naʿal](../../strongs/h/h5275.md) upon thy [regel](../../strongs/h/h7272.md), and [ʿāṭâ](../../strongs/h/h5844.md) not thy [śāp̄ām](../../strongs/h/h8222.md), and ['akal](../../strongs/h/h398.md) not the [lechem](../../strongs/h/h3899.md) of ['enowsh](../../strongs/h/h582.md).

<a name="ezekiel_24_18"></a>Ezekiel 24:18

So I [dabar](../../strongs/h/h1696.md) unto the ['am](../../strongs/h/h5971.md) in the [boqer](../../strongs/h/h1242.md): and at ['ereb](../../strongs/h/h6153.md) my ['ishshah](../../strongs/h/h802.md) [muwth](../../strongs/h/h4191.md); and I ['asah](../../strongs/h/h6213.md) in the [boqer](../../strongs/h/h1242.md) as I was [tsavah](../../strongs/h/h6680.md).

<a name="ezekiel_24_19"></a>Ezekiel 24:19

And the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md) unto me, Wilt thou not [nāḡaḏ](../../strongs/h/h5046.md) us what these things are to us, that thou ['asah](../../strongs/h/h6213.md) so?

<a name="ezekiel_24_20"></a>Ezekiel 24:20

Then I ['āmar](../../strongs/h/h559.md) them, The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_24_21"></a>Ezekiel 24:21

['āmar](../../strongs/h/h559.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I will [ḥālal](../../strongs/h/h2490.md) my [miqdash](../../strongs/h/h4720.md), the [gā'ôn](../../strongs/h/h1347.md) of your ['oz](../../strongs/h/h5797.md), the [maḥmāḏ](../../strongs/h/h4261.md) of your ['ayin](../../strongs/h/h5869.md), and that which your [nephesh](../../strongs/h/h5315.md) [maḥmāl](../../strongs/h/h4263.md); and your [ben](../../strongs/h/h1121.md) and your [bath](../../strongs/h/h1323.md) whom ye have ['azab](../../strongs/h/h5800.md) shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md).

<a name="ezekiel_24_22"></a>Ezekiel 24:22

And ye shall ['asah](../../strongs/h/h6213.md) as I have ['asah](../../strongs/h/h6213.md): ye shall not [ʿāṭâ](../../strongs/h/h5844.md) your [śāp̄ām](../../strongs/h/h8222.md), nor ['akal](../../strongs/h/h398.md) the [lechem](../../strongs/h/h3899.md) of ['enowsh](../../strongs/h/h582.md).

<a name="ezekiel_24_23"></a>Ezekiel 24:23

And your [pᵊ'ēr](../../strongs/h/h6287.md) shall be upon your [ro'sh](../../strongs/h/h7218.md), and your [naʿal](../../strongs/h/h5275.md) upon your [regel](../../strongs/h/h7272.md): ye shall not [sāp̄aḏ](../../strongs/h/h5594.md) nor [bāḵâ](../../strongs/h/h1058.md); but ye shall [māqaq](../../strongs/h/h4743.md) for your ['avon](../../strongs/h/h5771.md), and [nāham](../../strongs/h/h5098.md) ['iysh](../../strongs/h/h376.md) toward ['ach](../../strongs/h/h251.md).

<a name="ezekiel_24_24"></a>Ezekiel 24:24

Thus [Yᵊḥezqē'L](../../strongs/h/h3168.md) is unto you a [môp̄ēṯ](../../strongs/h/h4159.md): according to all that he hath ['asah](../../strongs/h/h6213.md) shall ye ['asah](../../strongs/h/h6213.md): and when this [bow'](../../strongs/h/h935.md), ye shall [yada'](../../strongs/h/h3045.md) that I am the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_24_25"></a>Ezekiel 24:25

Also, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), shall it not be in the [yowm](../../strongs/h/h3117.md) when I [laqach](../../strongs/h/h3947.md) from them their [māʿôz](../../strongs/h/h4581.md), the [māśôś](../../strongs/h/h4885.md) of their [tip̄'ārâ](../../strongs/h/h8597.md), the [maḥmāḏ](../../strongs/h/h4261.md) of their ['ayin](../../strongs/h/h5869.md), and that whereupon they [maśśā'](../../strongs/h/h4853.md) their [nephesh](../../strongs/h/h5315.md), their [ben](../../strongs/h/h1121.md) and their [bath](../../strongs/h/h1323.md),

<a name="ezekiel_24_26"></a>Ezekiel 24:26

That he that [pālîṭ](../../strongs/h/h6412.md) in that [yowm](../../strongs/h/h3117.md) shall [bow'](../../strongs/h/h935.md) unto thee, to cause thee to [hašmāʿûṯ](../../strongs/h/h2045.md) it with thine ['ozen](../../strongs/h/h241.md)?

<a name="ezekiel_24_27"></a>Ezekiel 24:27

In that [yowm](../../strongs/h/h3117.md) shall thy [peh](../../strongs/h/h6310.md) be [pāṯaḥ](../../strongs/h/h6605.md) to him which is [pālîṭ](../../strongs/h/h6412.md), and thou shalt [dabar](../../strongs/h/h1696.md), and be no more ['ālam](../../strongs/h/h481.md): and thou shalt be a [môp̄ēṯ](../../strongs/h/h4159.md) unto them; and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 23](ezekiel_23.md) - [Ezekiel 25](ezekiel_25.md)