# [Ezekiel 3](https://www.blueletterbible.org/kjv/ezekiel/3)

<a name="ezekiel_3_1"></a>Ezekiel 3:1

Moreover he ['āmar](../../strongs/h/h559.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), ['akal](../../strongs/h/h398.md) that thou [māṣā'](../../strongs/h/h4672.md); ['akal](../../strongs/h/h398.md) this [mᵊḡillâ](../../strongs/h/h4039.md), and [yālaḵ](../../strongs/h/h3212.md) [dabar](../../strongs/h/h1696.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_3_2"></a>Ezekiel 3:2

So I [pāṯaḥ](../../strongs/h/h6605.md) my [peh](../../strongs/h/h6310.md), and he caused me to ['akal](../../strongs/h/h398.md) that [mᵊḡillâ](../../strongs/h/h4039.md).

<a name="ezekiel_3_3"></a>Ezekiel 3:3

And he ['āmar](../../strongs/h/h559.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), cause thy [beten](../../strongs/h/h990.md) to ['akal](../../strongs/h/h398.md), and [mālā'](../../strongs/h/h4390.md) thy [me'ah](../../strongs/h/h4578.md) with this [mᵊḡillâ](../../strongs/h/h4039.md) that I [nathan](../../strongs/h/h5414.md) thee. Then did I ['akal](../../strongs/h/h398.md) it; and it was in my [peh](../../strongs/h/h6310.md) as [dĕbash](../../strongs/h/h1706.md) for [māṯôq](../../strongs/h/h4966.md).

<a name="ezekiel_3_4"></a>Ezekiel 3:4

And he ['āmar](../../strongs/h/h559.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [yālaḵ](../../strongs/h/h3212.md), [bow'](../../strongs/h/h935.md) thee unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), and [dabar](../../strongs/h/h1696.md) with my [dabar](../../strongs/h/h1697.md) unto them.

<a name="ezekiel_3_5"></a>Ezekiel 3:5

For thou art not [shalach](../../strongs/h/h7971.md) to an ['am](../../strongs/h/h5971.md) of a [ʿāmēq](../../strongs/h/h6012.md) [saphah](../../strongs/h/h8193.md) and of a [kāḇēḏ](../../strongs/h/h3515.md) [lashown](../../strongs/h/h3956.md), but to the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md);

<a name="ezekiel_3_6"></a>Ezekiel 3:6

Not to [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) of a [ʿāmēq](../../strongs/h/h6012.md) [saphah](../../strongs/h/h8193.md) and of a [kāḇēḏ](../../strongs/h/h3515.md) [lashown](../../strongs/h/h3956.md), whose [dabar](../../strongs/h/h1697.md) thou canst not [shama'](../../strongs/h/h8085.md). Surely, had I [shalach](../../strongs/h/h7971.md) thee to them, they would have [shama'](../../strongs/h/h8085.md) unto thee.

<a name="ezekiel_3_7"></a>Ezekiel 3:7

But the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) ['āḇâ](../../strongs/h/h14.md) not [shama'](../../strongs/h/h8085.md) unto thee; for they ['āḇâ](../../strongs/h/h14.md) not [shama'](../../strongs/h/h8085.md) unto me: for all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) are [ḥāzāq](../../strongs/h/h2389.md) [mēṣaḥ](../../strongs/h/h4696.md) and [qāšê](../../strongs/h/h7186.md) [leb](../../strongs/h/h3820.md).

<a name="ezekiel_3_8"></a>Ezekiel 3:8

Behold, I have [nathan](../../strongs/h/h5414.md) thy [paniym](../../strongs/h/h6440.md) [ḥāzāq](../../strongs/h/h2389.md) [ʿummâ](../../strongs/h/h5980.md) their [paniym](../../strongs/h/h6440.md), and thy [mēṣaḥ](../../strongs/h/h4696.md) [ḥāzāq](../../strongs/h/h2389.md) [ʿummâ](../../strongs/h/h5980.md) their [mēṣaḥ](../../strongs/h/h4696.md).

<a name="ezekiel_3_9"></a>Ezekiel 3:9

As a [šāmîr](../../strongs/h/h8068.md) [ḥāzāq](../../strongs/h/h2389.md) than [ṣōr](../../strongs/h/h6864.md) have I [nathan](../../strongs/h/h5414.md) thy [mēṣaḥ](../../strongs/h/h4696.md): [yare'](../../strongs/h/h3372.md) them not, neither be [ḥāṯaṯ](../../strongs/h/h2865.md) at their [paniym](../../strongs/h/h6440.md), though they be a [mᵊrî](../../strongs/h/h4805.md) [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_3_10"></a>Ezekiel 3:10

Moreover he ['āmar](../../strongs/h/h559.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), all my [dabar](../../strongs/h/h1697.md) that I shall [dabar](../../strongs/h/h1696.md) unto thee [laqach](../../strongs/h/h3947.md) in thine [lebab](../../strongs/h/h3824.md), and [shama'](../../strongs/h/h8085.md) with thine ['ozen](../../strongs/h/h241.md).

<a name="ezekiel_3_11"></a>Ezekiel 3:11

And [yālaḵ](../../strongs/h/h3212.md), [bow'](../../strongs/h/h935.md) thee to them of the [gôlâ](../../strongs/h/h1473.md), unto the [ben](../../strongs/h/h1121.md) of thy ['am](../../strongs/h/h5971.md), and [dabar](../../strongs/h/h1696.md) unto them, and ['āmar](../../strongs/h/h559.md) them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); whether they will [shama'](../../strongs/h/h8085.md), or whether they will [ḥāḏal](../../strongs/h/h2308.md).

<a name="ezekiel_3_12"></a>Ezekiel 3:12

Then the [ruwach](../../strongs/h/h7307.md) took me [nasa'](../../strongs/h/h5375.md), and I [shama'](../../strongs/h/h8085.md) ['aḥar](../../strongs/h/h310.md) me a [qowl](../../strongs/h/h6963.md) of a [gadowl](../../strongs/h/h1419.md) [raʿaš](../../strongs/h/h7494.md), saying, [barak](../../strongs/h/h1288.md) be the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) from his [maqowm](../../strongs/h/h4725.md).

<a name="ezekiel_3_13"></a>Ezekiel 3:13

I heard also the [qowl](../../strongs/h/h6963.md) of the [kanaph](../../strongs/h/h3671.md) of the [chay](../../strongs/h/h2416.md) that [nashaq](../../strongs/h/h5401.md) ['ishshah](../../strongs/h/h802.md) ['āḥôṯ](../../strongs/h/h269.md), and the [qowl](../../strongs/h/h6963.md) of the ['ôp̄ān](../../strongs/h/h212.md) over [ʿummâ](../../strongs/h/h5980.md) them, and a [qowl](../../strongs/h/h6963.md) of a [gadowl](../../strongs/h/h1419.md) [raʿaš](../../strongs/h/h7494.md).

<a name="ezekiel_3_14"></a>Ezekiel 3:14

So the [ruwach](../../strongs/h/h7307.md) [nasa'](../../strongs/h/h5375.md) me, and [laqach](../../strongs/h/h3947.md) me, and I [yālaḵ](../../strongs/h/h3212.md) in [mar](../../strongs/h/h4751.md), in the [chemah](../../strongs/h/h2534.md) of my [ruwach](../../strongs/h/h7307.md); but the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥāzaq](../../strongs/h/h2388.md) upon me.

<a name="ezekiel_3_15"></a>Ezekiel 3:15

Then I [bow'](../../strongs/h/h935.md) to them of the [gôlâ](../../strongs/h/h1473.md) at [Tēl 'Āḇîḇ](../../strongs/h/h8512.md), that [yashab](../../strongs/h/h3427.md) by the [nāhār](../../strongs/h/h5104.md) of [kᵊḇār](../../strongs/h/h3529.md), and I [yashab](../../strongs/h/h3427.md) where they [yashab](../../strongs/h/h3427.md), and [yashab](../../strongs/h/h3427.md) there [šāmēm](../../strongs/h/h8074.md) among them seven [yowm](../../strongs/h/h3117.md).

<a name="ezekiel_3_16"></a>Ezekiel 3:16

And it came to pass at the [qāṣê](../../strongs/h/h7097.md) of seven [yowm](../../strongs/h/h3117.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_3_17"></a>Ezekiel 3:17

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), I have [nathan](../../strongs/h/h5414.md) thee a [tsaphah](../../strongs/h/h6822.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md): therefore [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) at my [peh](../../strongs/h/h6310.md), and give them [zāhar](../../strongs/h/h2094.md) from me.

<a name="ezekiel_3_18"></a>Ezekiel 3:18

When I ['āmar](../../strongs/h/h559.md) unto the [rasha'](../../strongs/h/h7563.md), Thou shalt [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md); and thou givest him not [zāhar](../../strongs/h/h2094.md), nor [dabar](../../strongs/h/h1696.md) to [zāhar](../../strongs/h/h2094.md) the [rasha'](../../strongs/h/h7563.md) from his [rasha'](../../strongs/h/h7563.md) [derek](../../strongs/h/h1870.md), to [ḥāyâ](../../strongs/h/h2421.md); the same [rasha'](../../strongs/h/h7563.md) shall [muwth](../../strongs/h/h4191.md) in his ['avon](../../strongs/h/h5771.md); but his [dam](../../strongs/h/h1818.md) will I [bāqaš](../../strongs/h/h1245.md) at thine [yad](../../strongs/h/h3027.md).

<a name="ezekiel_3_19"></a>Ezekiel 3:19

Yet if thou [zāhar](../../strongs/h/h2094.md) the [rasha'](../../strongs/h/h7563.md), and he [shuwb](../../strongs/h/h7725.md) not from his [resha'](../../strongs/h/h7562.md), nor from his [rasha'](../../strongs/h/h7563.md) [derek](../../strongs/h/h1870.md), he shall [muwth](../../strongs/h/h4191.md) in his ['avon](../../strongs/h/h5771.md); but thou hast [natsal](../../strongs/h/h5337.md) thy [nephesh](../../strongs/h/h5315.md).

<a name="ezekiel_3_20"></a>Ezekiel 3:20

[shuwb](../../strongs/h/h7725.md), When a [tsaddiyq](../../strongs/h/h6662.md) man doth [shuwb](../../strongs/h/h7725.md) from his [tsedeq](../../strongs/h/h6664.md), and ['asah](../../strongs/h/h6213.md) ['evel](../../strongs/h/h5766.md), and I [nathan](../../strongs/h/h5414.md) a [miḵšôl](../../strongs/h/h4383.md) [paniym](../../strongs/h/h6440.md) him, he shall [muwth](../../strongs/h/h4191.md): because thou hast not given him [zāhar](../../strongs/h/h2094.md), he shall [muwth](../../strongs/h/h4191.md) in his [chatta'ath](../../strongs/h/h2403.md), and his [tsedaqah](../../strongs/h/h6666.md) which he hath ['asah](../../strongs/h/h6213.md) shall not be [zakar](../../strongs/h/h2142.md); but his [dam](../../strongs/h/h1818.md) will I [bāqaš](../../strongs/h/h1245.md) at thine [yad](../../strongs/h/h3027.md).

<a name="ezekiel_3_21"></a>Ezekiel 3:21

Nevertheless if thou [zāhar](../../strongs/h/h2094.md) the [tsaddiyq](../../strongs/h/h6662.md), that the [tsaddiyq](../../strongs/h/h6662.md) [chata'](../../strongs/h/h2398.md) not, and he doth not [chata'](../../strongs/h/h2398.md), he shall [ḥāyâ](../../strongs/h/h2421.md) [ḥāyâ](../../strongs/h/h2421.md), because he is [zāhar](../../strongs/h/h2094.md); also thou hast [natsal](../../strongs/h/h5337.md) thy [nephesh](../../strongs/h/h5315.md).

<a name="ezekiel_3_22"></a>Ezekiel 3:22

And the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) was there upon me; and he ['āmar](../../strongs/h/h559.md) unto me, [quwm](../../strongs/h/h6965.md), [yāṣā'](../../strongs/h/h3318.md) into the [biqʿâ](../../strongs/h/h1237.md), and I will there [dabar](../../strongs/h/h1696.md) with thee.

<a name="ezekiel_3_23"></a>Ezekiel 3:23

Then I [quwm](../../strongs/h/h6965.md), and [yāṣā'](../../strongs/h/h3318.md) into the [biqʿâ](../../strongs/h/h1237.md): and, behold, the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) ['amad](../../strongs/h/h5975.md) there, as the [kabowd](../../strongs/h/h3519.md) which I [ra'ah](../../strongs/h/h7200.md) by the [nāhār](../../strongs/h/h5104.md) of [kᵊḇār](../../strongs/h/h3529.md): and I [naphal](../../strongs/h/h5307.md) on my [paniym](../../strongs/h/h6440.md).

<a name="ezekiel_3_24"></a>Ezekiel 3:24

Then the [ruwach](../../strongs/h/h7307.md) [bow'](../../strongs/h/h935.md) into me, and ['amad](../../strongs/h/h5975.md) me upon my [regel](../../strongs/h/h7272.md), and [dabar](../../strongs/h/h1696.md) with me, and ['āmar](../../strongs/h/h559.md) unto me, [bow'](../../strongs/h/h935.md), [cagar](../../strongs/h/h5462.md) thyself within thine [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_3_25"></a>Ezekiel 3:25

But thou, O [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), behold, they shall [nathan](../../strongs/h/h5414.md) [ʿăḇōṯ](../../strongs/h/h5688.md) upon thee, and shall ['āsar](../../strongs/h/h631.md) thee with them, and thou shalt not [yāṣā'](../../strongs/h/h3318.md) among them:

<a name="ezekiel_3_26"></a>Ezekiel 3:26

And I will make thy [lashown](../../strongs/h/h3956.md) [dāḇaq](../../strongs/h/h1692.md) to the roof of thy [ḥēḵ](../../strongs/h/h2441.md), that thou shalt be ['ālam](../../strongs/h/h481.md), and shalt not be to them an ['iysh](../../strongs/h/h376.md) [yakach](../../strongs/h/h3198.md): for they are a [mᵊrî](../../strongs/h/h4805.md) [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_3_27"></a>Ezekiel 3:27

But when I [dabar](../../strongs/h/h1696.md) with thee, I will [pāṯaḥ](../../strongs/h/h6605.md) thy [peh](../../strongs/h/h6310.md), and thou shalt ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); He that [shama'](../../strongs/h/h8085.md), let him [shama'](../../strongs/h/h8085.md); and he that [ḥāḏēl](../../strongs/h/h2310.md), let him [ḥāḏal](../../strongs/h/h2308.md): for they are a [mᵊrî](../../strongs/h/h4805.md) [bayith](../../strongs/h/h1004.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 2](ezekiel_2.md) - [Ezekiel 4](ezekiel_4.md)