# [Ezekiel 23](https://www.blueletterbible.org/kjv/ezekiel/23)

<a name="ezekiel_23_1"></a>Ezekiel 23:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came again unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_23_2"></a>Ezekiel 23:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), there were two ['ishshah](../../strongs/h/h802.md), the [bath](../../strongs/h/h1323.md) of one ['em](../../strongs/h/h517.md):

<a name="ezekiel_23_3"></a>Ezekiel 23:3

And they [zānâ](../../strongs/h/h2181.md) in [Mitsrayim](../../strongs/h/h4714.md); they [zānâ](../../strongs/h/h2181.md) in their [nāʿur](../../strongs/h/h5271.md): there were their [šaḏ](../../strongs/h/h7699.md) [māʿaḵ](../../strongs/h/h4600.md), and there they ['asah](../../strongs/h/h6213.md) the [daḏ](../../strongs/h/h1717.md) of their [bᵊṯûlîm](../../strongs/h/h1331.md).

<a name="ezekiel_23_4"></a>Ezekiel 23:4

And the [shem](../../strongs/h/h8034.md) of them were ['Āhŏlâ](../../strongs/h/h170.md) the [gadowl](../../strongs/h/h1419.md), and ['Āhŏlîḇâ](../../strongs/h/h172.md) her ['āḥôṯ](../../strongs/h/h269.md): and they were mine, and they [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md). Thus were their [shem](../../strongs/h/h8034.md); [Šōmrôn](../../strongs/h/h8111.md) is ['Āhŏlâ](../../strongs/h/h170.md), and [Yĕruwshalaim](../../strongs/h/h3389.md) ['Āhŏlîḇâ](../../strongs/h/h172.md).

<a name="ezekiel_23_5"></a>Ezekiel 23:5

And ['Āhŏlâ](../../strongs/h/h170.md) played the [zānâ](../../strongs/h/h2181.md) when she was mine; and she [ʿāḡaḇ](../../strongs/h/h5689.md) on her ['ahab](../../strongs/h/h157.md), on the ['Aššûr](../../strongs/h/h804.md) her [qarowb](../../strongs/h/h7138.md),

<a name="ezekiel_23_6"></a>Ezekiel 23:6

Which were [labash](../../strongs/h/h3847.md) with [tᵊḵēleṯ](../../strongs/h/h8504.md), [peḥâ](../../strongs/h/h6346.md) and [sāḡān](../../strongs/h/h5461.md), all of them [ḥemeḏ](../../strongs/h/h2531.md) [bāḥûr](../../strongs/h/h970.md), [pārāš](../../strongs/h/h6571.md) [rāḵaḇ](../../strongs/h/h7392.md) upon [sûs](../../strongs/h/h5483.md).

<a name="ezekiel_23_7"></a>Ezekiel 23:7

Thus she [nathan](../../strongs/h/h5414.md) her [taznûṯ](../../strongs/h/h8457.md) with them, with all them that were the [miḇḥār](../../strongs/h/h4005.md) [ben](../../strongs/h/h1121.md) of ['Aššûr](../../strongs/h/h804.md), and with all on whom she [ʿāḡaḇ](../../strongs/h/h5689.md): with all their [gillûl](../../strongs/h/h1544.md) she [ṭāmē'](../../strongs/h/h2930.md) herself.

<a name="ezekiel_23_8"></a>Ezekiel 23:8

Neither ['azab](../../strongs/h/h5800.md) she her [taznûṯ](../../strongs/h/h8457.md) brought from [Mitsrayim](../../strongs/h/h4714.md): for in her [nāʿur](../../strongs/h/h5271.md) they [shakab](../../strongs/h/h7901.md) with her, and they ['asah](../../strongs/h/h6213.md) the [daḏ](../../strongs/h/h1717.md) of her [bᵊṯûlîm](../../strongs/h/h1331.md), and [šāp̄aḵ](../../strongs/h/h8210.md) their [taznûṯ](../../strongs/h/h8457.md) upon her.

<a name="ezekiel_23_9"></a>Ezekiel 23:9

Wherefore I have [nathan](../../strongs/h/h5414.md) her into the [yad](../../strongs/h/h3027.md) of her ['ahab](../../strongs/h/h157.md), into the [yad](../../strongs/h/h3027.md) of the [ben](../../strongs/h/h1121.md) ['Aššûr](../../strongs/h/h804.md), upon whom she [ʿāḡaḇ](../../strongs/h/h5689.md).

<a name="ezekiel_23_10"></a>Ezekiel 23:10

These [gālâ](../../strongs/h/h1540.md) her [ʿervâ](../../strongs/h/h6172.md): they [laqach](../../strongs/h/h3947.md) her [ben](../../strongs/h/h1121.md) and her [bath](../../strongs/h/h1323.md), and [harag](../../strongs/h/h2026.md) her with the [chereb](../../strongs/h/h2719.md): and she became [shem](../../strongs/h/h8034.md) among ['ishshah](../../strongs/h/h802.md); for they had ['asah](../../strongs/h/h6213.md) [šᵊp̄ôṭ](../../strongs/h/h8196.md) upon her.

<a name="ezekiel_23_11"></a>Ezekiel 23:11

And when her ['āḥôṯ](../../strongs/h/h269.md) ['Āhŏlîḇâ](../../strongs/h/h172.md) [ra'ah](../../strongs/h/h7200.md) this, she was more [shachath](../../strongs/h/h7843.md) in her inordinate [ʿăḡāḇâ](../../strongs/h/h5691.md) than she, and in her [taznûṯ](../../strongs/h/h8457.md) more than her ['āḥôṯ](../../strongs/h/h269.md) in her [zᵊnûnîm](../../strongs/h/h2183.md).

<a name="ezekiel_23_12"></a>Ezekiel 23:12

She [ʿāḡaḇ](../../strongs/h/h5689.md) upon the [ben](../../strongs/h/h1121.md) ['Aššûr](../../strongs/h/h804.md) her [qarowb](../../strongs/h/h7138.md), [peḥâ](../../strongs/h/h6346.md) and [sāḡān](../../strongs/h/h5461.md) [labash](../../strongs/h/h3847.md) most [miḵlôl](../../strongs/h/h4358.md), [pārāš](../../strongs/h/h6571.md) [rāḵaḇ](../../strongs/h/h7392.md) upon [sûs](../../strongs/h/h5483.md), all of them [ḥemeḏ](../../strongs/h/h2531.md) [bāḥûr](../../strongs/h/h970.md).

<a name="ezekiel_23_13"></a>Ezekiel 23:13

Then I [ra'ah](../../strongs/h/h7200.md) that she was [ṭāmē'](../../strongs/h/h2930.md), that they took both one [derek](../../strongs/h/h1870.md),

<a name="ezekiel_23_14"></a>Ezekiel 23:14

And that she increased her [taznûṯ](../../strongs/h/h8457.md): for when she [ra'ah](../../strongs/h/h7200.md) ['enowsh](../../strongs/h/h582.md) [ḥāqâ](../../strongs/h/h2707.md) upon the [qîr](../../strongs/h/h7023.md), the [tselem](../../strongs/h/h6754.md) of the [Kaśdîmâ](../../strongs/h/h3778.md) [ḥāqaq](../../strongs/h/h2710.md) with [šāšēr](../../strongs/h/h8350.md),

<a name="ezekiel_23_15"></a>Ezekiel 23:15

[ḥăḡôr](../../strongs/h/h2289.md) with ['ēzôr](../../strongs/h/h232.md) upon their [māṯnayim](../../strongs/h/h4975.md), [sāraḥ](../../strongs/h/h5628.md) in [ṭᵊḇûlîm](../../strongs/h/h2871.md) upon their [ro'sh](../../strongs/h/h7218.md), all of them [šālîš](../../strongs/h/h7991.md) to look [mar'ê](../../strongs/h/h4758.md), after the [dĕmuwth](../../strongs/h/h1823.md) of the [ben](../../strongs/h/h1121.md) [Bāḇel](../../strongs/h/h894.md) of [Kaśdîmâ](../../strongs/h/h3778.md), the ['erets](../../strongs/h/h776.md) of their [môleḏeṯ](../../strongs/h/h4138.md):

<a name="ezekiel_23_16"></a>Ezekiel 23:16

And as soon as she [mar'ê](../../strongs/h/h4758.md) them with her ['ayin](../../strongs/h/h5869.md), she [ʿāḡaḇ](../../strongs/h/h5689.md) upon them, and [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) unto them into [Kaśdîmâ](../../strongs/h/h3778.md).

<a name="ezekiel_23_17"></a>Ezekiel 23:17

And the [ben](../../strongs/h/h1121.md) [Bāḇel](../../strongs/h/h894.md) [bow'](../../strongs/h/h935.md) to her into the [miškāḇ](../../strongs/h/h4904.md) of [dôḏ](../../strongs/h/h1730.md), and they [ṭāmē'](../../strongs/h/h2930.md) her with their [taznûṯ](../../strongs/h/h8457.md), and she was [ṭāmē'](../../strongs/h/h2930.md) with them, and her [nephesh](../../strongs/h/h5315.md) was [yāqaʿ](../../strongs/h/h3363.md) from them.

<a name="ezekiel_23_18"></a>Ezekiel 23:18

So she [gālâ](../../strongs/h/h1540.md) her [taznûṯ](../../strongs/h/h8457.md), and [gālâ](../../strongs/h/h1540.md) her [ʿervâ](../../strongs/h/h6172.md): then my [nephesh](../../strongs/h/h5315.md) was [yāqaʿ](../../strongs/h/h3363.md) from her, like as my [nephesh](../../strongs/h/h5315.md) was [nāqaʿ](../../strongs/h/h5361.md) from her ['āḥôṯ](../../strongs/h/h269.md).

<a name="ezekiel_23_19"></a>Ezekiel 23:19

Yet she [rabah](../../strongs/h/h7235.md) her [taznûṯ](../../strongs/h/h8457.md), in calling to [zakar](../../strongs/h/h2142.md) the [yowm](../../strongs/h/h3117.md) of her [nāʿur](../../strongs/h/h5271.md), wherein she had played the [zānâ](../../strongs/h/h2181.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="ezekiel_23_20"></a>Ezekiel 23:20

For she [ʿāḡaḇ](../../strongs/h/h5689.md) upon their [pîleḡeš](../../strongs/h/h6370.md), whose [basar](../../strongs/h/h1320.md) is as the [basar](../../strongs/h/h1320.md) of [chamowr](../../strongs/h/h2543.md), and whose [zirmâ](../../strongs/h/h2231.md) is like the [zirmâ](../../strongs/h/h2231.md) of [sûs](../../strongs/h/h5483.md).

<a name="ezekiel_23_21"></a>Ezekiel 23:21

Thus thou calledst to [paqad](../../strongs/h/h6485.md) the [zimmâ](../../strongs/h/h2154.md) of thy [nāʿur](../../strongs/h/h5271.md), in ['asah](../../strongs/h/h6213.md) thy [daḏ](../../strongs/h/h1717.md) by the [Mitsrayim](../../strongs/h/h4714.md) for the [šaḏ](../../strongs/h/h7699.md) of thy [nāʿur](../../strongs/h/h5271.md).

<a name="ezekiel_23_22"></a>Ezekiel 23:22

Therefore, O ['Āhŏlîḇâ](../../strongs/h/h172.md), thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I will [ʿûr](../../strongs/h/h5782.md) thy ['ahab](../../strongs/h/h157.md) against thee, from whom thy [nephesh](../../strongs/h/h5315.md) is [nāqaʿ](../../strongs/h/h5361.md), and I will [bow'](../../strongs/h/h935.md) them against thee [cabiyb](../../strongs/h/h5439.md);

<a name="ezekiel_23_23"></a>Ezekiel 23:23

The [ben](../../strongs/h/h1121.md) [Bāḇel](../../strongs/h/h894.md), and all the [Kaśdîmâ](../../strongs/h/h3778.md), [pᵊqôḏ](../../strongs/h/h6489.md), and [Šôaʿ](../../strongs/h/h7772.md), and [Qûaʿ](../../strongs/h/h6970.md), and all the [ben](../../strongs/h/h1121.md) ['Aššûr](../../strongs/h/h804.md) with them: all of them [ḥemeḏ](../../strongs/h/h2531.md) [bāḥûr](../../strongs/h/h970.md), [peḥâ](../../strongs/h/h6346.md) and [sāḡān](../../strongs/h/h5461.md), [šālîš](../../strongs/h/h7991.md) and [qara'](../../strongs/h/h7121.md), all of them [rāḵaḇ](../../strongs/h/h7392.md) upon [sûs](../../strongs/h/h5483.md).

<a name="ezekiel_23_24"></a>Ezekiel 23:24

And they shall [bow'](../../strongs/h/h935.md) against thee with [hōṣen](../../strongs/h/h2021.md), [reḵeḇ](../../strongs/h/h7393.md), and [galgal](../../strongs/h/h1534.md), and with a [qāhēl](../../strongs/h/h6951.md) of ['am](../../strongs/h/h5971.md), which shall [śûm](../../strongs/h/h7760.md) against thee [tsinnah](../../strongs/h/h6793.md) and [magen](../../strongs/h/h4043.md) and [qôḇaʿ](../../strongs/h/h6959.md) [cabiyb](../../strongs/h/h5439.md): and I will [nathan](../../strongs/h/h5414.md) [mishpat](../../strongs/h/h4941.md) [paniym](../../strongs/h/h6440.md) them, and they shall [shaphat](../../strongs/h/h8199.md) thee according to their [mishpat](../../strongs/h/h4941.md).

<a name="ezekiel_23_25"></a>Ezekiel 23:25

And I will [nathan](../../strongs/h/h5414.md) my [qin'â](../../strongs/h/h7068.md) against thee, and they shall ['asah](../../strongs/h/h6213.md) [chemah](../../strongs/h/h2534.md) with thee: they shall [cuwr](../../strongs/h/h5493.md) thy ['aph](../../strongs/h/h639.md) and thine ['ozen](../../strongs/h/h241.md); and thy ['aḥărîṯ](../../strongs/h/h319.md) shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md): they shall [laqach](../../strongs/h/h3947.md) thy [ben](../../strongs/h/h1121.md) and thy [bath](../../strongs/h/h1323.md); and thy ['aḥărîṯ](../../strongs/h/h319.md) shall be ['akal](../../strongs/h/h398.md) by the ['esh](../../strongs/h/h784.md).

<a name="ezekiel_23_26"></a>Ezekiel 23:26

They shall also [pāšaṭ](../../strongs/h/h6584.md) thee out of thy [beḡeḏ](../../strongs/h/h899.md), and [laqach](../../strongs/h/h3947.md) thy [tip̄'ārâ](../../strongs/h/h8597.md) [kĕliy](../../strongs/h/h3627.md).

<a name="ezekiel_23_27"></a>Ezekiel 23:27

Thus will I make thy [zimmâ](../../strongs/h/h2154.md) to [shabath](../../strongs/h/h7673.md) from thee, and thy [zᵊnûṯ](../../strongs/h/h2184.md) brought from the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): so that thou shalt not [nasa'](../../strongs/h/h5375.md) thine ['ayin](../../strongs/h/h5869.md) unto them, nor [zakar](../../strongs/h/h2142.md) [Mitsrayim](../../strongs/h/h4714.md) any more.

<a name="ezekiel_23_28"></a>Ezekiel 23:28

For thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I will [nathan](../../strongs/h/h5414.md) thee into the [yad](../../strongs/h/h3027.md) of them whom thou [sane'](../../strongs/h/h8130.md), into the [yad](../../strongs/h/h3027.md) of them from whom thy [nephesh](../../strongs/h/h5315.md) is [nāqaʿ](../../strongs/h/h5361.md):

<a name="ezekiel_23_29"></a>Ezekiel 23:29

And they shall ['asah](../../strongs/h/h6213.md) with thee [śin'â](../../strongs/h/h8135.md), and shall [laqach](../../strongs/h/h3947.md) all thy [yᵊḡîaʿ](../../strongs/h/h3018.md), and shall ['azab](../../strongs/h/h5800.md) thee ['eyrom](../../strongs/h/h5903.md) and [ʿeryâ](../../strongs/h/h6181.md): and the [ʿervâ](../../strongs/h/h6172.md) of thy [zᵊnûnîm](../../strongs/h/h2183.md) shall be [gālâ](../../strongs/h/h1540.md), both thy [zimmâ](../../strongs/h/h2154.md) and thy [taznûṯ](../../strongs/h/h8457.md).

<a name="ezekiel_23_30"></a>Ezekiel 23:30

I will ['asah](../../strongs/h/h6213.md) these things unto thee, because thou hast [zānâ](../../strongs/h/h2181.md) ['aḥar](../../strongs/h/h310.md) the [gowy](../../strongs/h/h1471.md), and because thou art [ṭāmē'](../../strongs/h/h2930.md) with their [gillûl](../../strongs/h/h1544.md).

<a name="ezekiel_23_31"></a>Ezekiel 23:31

Thou hast [halak](../../strongs/h/h1980.md) in the [derek](../../strongs/h/h1870.md) of thy ['āḥôṯ](../../strongs/h/h269.md); therefore will I [nathan](../../strongs/h/h5414.md) her [kowc](../../strongs/h/h3563.md) into thine [yad](../../strongs/h/h3027.md).

<a name="ezekiel_23_32"></a>Ezekiel 23:32

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Thou shalt [šāṯâ](../../strongs/h/h8354.md) of thy ['āḥôṯ](../../strongs/h/h269.md) [kowc](../../strongs/h/h3563.md) [ʿāmōq](../../strongs/h/h6013.md) and [rāḥāḇ](../../strongs/h/h7342.md): thou shalt be [ṣāḥaq](../../strongs/h/h6712.md) and had in [laʿaḡ](../../strongs/h/h3933.md); it [kûl](../../strongs/h/h3557.md) [mirbâ](../../strongs/h/h4767.md).

<a name="ezekiel_23_33"></a>Ezekiel 23:33

Thou shalt be [mālā'](../../strongs/h/h4390.md) with [šikārôn](../../strongs/h/h7943.md) and [yagown](../../strongs/h/h3015.md), with the [kowc](../../strongs/h/h3563.md) of [šammâ](../../strongs/h/h8047.md) and [šᵊmāmâ](../../strongs/h/h8077.md), with the [kowc](../../strongs/h/h3563.md) of thy ['āḥôṯ](../../strongs/h/h269.md) [Šōmrôn](../../strongs/h/h8111.md).

<a name="ezekiel_23_34"></a>Ezekiel 23:34

Thou shalt even [šāṯâ](../../strongs/h/h8354.md) it and [māṣâ](../../strongs/h/h4680.md) it, and thou shalt [gāram](../../strongs/h/h1633.md) the [ḥereś](../../strongs/h/h2789.md) thereof, and [nathaq](../../strongs/h/h5423.md) thine own [šaḏ](../../strongs/h/h7699.md): for I have [dabar](../../strongs/h/h1696.md) it, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_23_35"></a>Ezekiel 23:35

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Because thou hast [shakach](../../strongs/h/h7911.md) me, and [shalak](../../strongs/h/h7993.md) me ['aḥar](../../strongs/h/h310.md) thy [gav](../../strongs/h/h1458.md), therefore [nasa'](../../strongs/h/h5375.md) thou also thy [zimmâ](../../strongs/h/h2154.md) and thy [taznûṯ](../../strongs/h/h8457.md).

<a name="ezekiel_23_36"></a>Ezekiel 23:36

[Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) moreover unto me; [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), wilt thou [shaphat](../../strongs/h/h8199.md) ['Āhŏlâ](../../strongs/h/h170.md) and ['Āhŏlîḇâ](../../strongs/h/h172.md)? yea, [nāḡaḏ](../../strongs/h/h5046.md) unto them their [tôʿēḇâ](../../strongs/h/h8441.md);

<a name="ezekiel_23_37"></a>Ezekiel 23:37

That they have committed [na'aph](../../strongs/h/h5003.md), and [dam](../../strongs/h/h1818.md) is in their [yad](../../strongs/h/h3027.md), and with their [gillûl](../../strongs/h/h1544.md) have they committed [na'aph](../../strongs/h/h5003.md), and have also caused their [ben](../../strongs/h/h1121.md), whom they [yalad](../../strongs/h/h3205.md) unto me, to pass for them ['abar](../../strongs/h/h5674.md), to ['oklah](../../strongs/h/h402.md) them.

<a name="ezekiel_23_38"></a>Ezekiel 23:38

Moreover this they have ['asah](../../strongs/h/h6213.md) unto me: they have [ṭāmē'](../../strongs/h/h2930.md) my [miqdash](../../strongs/h/h4720.md) in the same [yowm](../../strongs/h/h3117.md), and have [ḥālal](../../strongs/h/h2490.md) my [shabbath](../../strongs/h/h7676.md).

<a name="ezekiel_23_39"></a>Ezekiel 23:39

For when they had [šāḥaṭ](../../strongs/h/h7819.md) their [ben](../../strongs/h/h1121.md) to their [gillûl](../../strongs/h/h1544.md), then they [bow'](../../strongs/h/h935.md) the same [yowm](../../strongs/h/h3117.md) into my [miqdash](../../strongs/h/h4720.md) to [ḥālal](../../strongs/h/h2490.md) it; and, lo, thus have they ['asah](../../strongs/h/h6213.md) in the midst of mine [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_23_40"></a>Ezekiel 23:40

And furthermore, that ye have [shalach](../../strongs/h/h7971.md) for ['enowsh](../../strongs/h/h582.md) to [bow'](../../strongs/h/h935.md) from [merḥāq](../../strongs/h/h4801.md), unto whom a [mal'ak](../../strongs/h/h4397.md) was [shalach](../../strongs/h/h7971.md); and, lo, they [bow'](../../strongs/h/h935.md): for whom thou didst [rāḥaṣ](../../strongs/h/h7364.md) thyself, [kāḥal](../../strongs/h/h3583.md) thy ['ayin](../../strongs/h/h5869.md), and [ʿāḏâ](../../strongs/h/h5710.md) thyself with [ʿădiy](../../strongs/h/h5716.md),

<a name="ezekiel_23_41"></a>Ezekiel 23:41

And [yashab](../../strongs/h/h3427.md) upon a [kᵊḇûdâ](../../strongs/h/h3520.md) [mittah](../../strongs/h/h4296.md), and a [šulḥān](../../strongs/h/h7979.md) ['arak](../../strongs/h/h6186.md) [paniym](../../strongs/h/h6440.md) it, whereupon thou hast [śûm](../../strongs/h/h7760.md) mine [qᵊṭōreṯ](../../strongs/h/h7004.md) and mine [šemen](../../strongs/h/h8081.md).

<a name="ezekiel_23_42"></a>Ezekiel 23:42

And a [qowl](../../strongs/h/h6963.md) of a [hāmôn](../../strongs/h/h1995.md) being at [šālēv](../../strongs/h/h7961.md) was with her: and with the ['enowsh](../../strongs/h/h582.md) of the common [rōḇ](../../strongs/h/h7230.md) ['āḏām](../../strongs/h/h120.md) were [bow'](../../strongs/h/h935.md) [sᵊḇā'î](../../strongs/h/h5436.md) [sāḇā'](../../strongs/h/h5433.md) from the [midbar](../../strongs/h/h4057.md), which [nathan](../../strongs/h/h5414.md) [ṣāmîḏ](../../strongs/h/h6781.md) upon their [yad](../../strongs/h/h3027.md), and [tip̄'ārâ](../../strongs/h/h8597.md) [ʿăṭārâ](../../strongs/h/h5850.md) upon their [ro'sh](../../strongs/h/h7218.md).

<a name="ezekiel_23_43"></a>Ezekiel 23:43

Then ['āmar](../../strongs/h/h559.md) I unto her that was [bālê](../../strongs/h/h1087.md) in [ni'up̄îm](../../strongs/h/h5004.md), Will they now [zānâ](../../strongs/h/h2181.md) [taznûṯ](../../strongs/h/h8457.md) with her, and she with them?

<a name="ezekiel_23_44"></a>Ezekiel 23:44

Yet they [bow'](../../strongs/h/h935.md) unto her, as they [bow'](../../strongs/h/h935.md) unto an ['ishshah](../../strongs/h/h802.md) that playeth the [zānâ](../../strongs/h/h2181.md): so went they [bow'](../../strongs/h/h935.md) unto ['Āhŏlâ](../../strongs/h/h170.md) and unto ['Āhŏlîḇâ](../../strongs/h/h172.md), the [zimmâ](../../strongs/h/h2154.md) ['ishshah](../../strongs/h/h802.md).

<a name="ezekiel_23_45"></a>Ezekiel 23:45

And the [tsaddiyq](../../strongs/h/h6662.md) ['enowsh](../../strongs/h/h582.md), they shall [shaphat](../../strongs/h/h8199.md) them after the [mishpat](../../strongs/h/h4941.md) of [na'aph](../../strongs/h/h5003.md), and after the [mishpat](../../strongs/h/h4941.md) of women that [šāp̄aḵ](../../strongs/h/h8210.md) [dam](../../strongs/h/h1818.md); because they are [na'aph](../../strongs/h/h5003.md), and [dam](../../strongs/h/h1818.md) is in their [yad](../../strongs/h/h3027.md).

<a name="ezekiel_23_46"></a>Ezekiel 23:46

For thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); I will [ʿālâ](../../strongs/h/h5927.md) a [qāhēl](../../strongs/h/h6951.md) upon them, and will [nathan](../../strongs/h/h5414.md) them to be [zaʿăvâ](../../strongs/h/h2189.md) and [baz](../../strongs/h/h957.md).

<a name="ezekiel_23_47"></a>Ezekiel 23:47

And the [qāhēl](../../strongs/h/h6951.md) shall [rāḡam](../../strongs/h/h7275.md) them with ['eben](../../strongs/h/h68.md), and [bara'](../../strongs/h/h1254.md) them with their [chereb](../../strongs/h/h2719.md); they shall [harag](../../strongs/h/h2026.md) their [ben](../../strongs/h/h1121.md) and their [bath](../../strongs/h/h1323.md), and [śārap̄](../../strongs/h/h8313.md) their [bayith](../../strongs/h/h1004.md) with ['esh](../../strongs/h/h784.md).

<a name="ezekiel_23_48"></a>Ezekiel 23:48

Thus will I cause [zimmâ](../../strongs/h/h2154.md) to [shabath](../../strongs/h/h7673.md) out of the ['erets](../../strongs/h/h776.md), that all ['ishshah](../../strongs/h/h802.md) may be [yacar](../../strongs/h/h3256.md) not to ['asah](../../strongs/h/h6213.md) after your [zimmâ](../../strongs/h/h2154.md).

<a name="ezekiel_23_49"></a>Ezekiel 23:49

And they shall [nathan](../../strongs/h/h5414.md) your [zimmâ](../../strongs/h/h2154.md) upon you, and ye shall [nasa'](../../strongs/h/h5375.md) the [ḥēṭĕ'](../../strongs/h/h2399.md) of your [gillûl](../../strongs/h/h1544.md): and ye shall [yada'](../../strongs/h/h3045.md) that I am the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 22](ezekiel_22.md) - [Ezekiel 24](ezekiel_24.md)