# [Ezekiel 1](https://www.blueletterbible.org/kjv/ezekiel/1)

<a name="ezekiel_1_1"></a>Ezekiel 1:1

Now it came to pass in the thirtieth [šānâ](../../strongs/h/h8141.md), in the fourth month, in the fifth day of the [ḥōḏeš](../../strongs/h/h2320.md), as I was among the [gôlâ](../../strongs/h/h1473.md) by the [nāhār](../../strongs/h/h5104.md) of [kᵊḇār](../../strongs/h/h3529.md), that the [shamayim](../../strongs/h/h8064.md) were [pāṯaḥ](../../strongs/h/h6605.md), and I [ra'ah](../../strongs/h/h7200.md) [mar'â](../../strongs/h/h4759.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="ezekiel_1_2"></a>Ezekiel 1:2

In the fifth day of the [ḥōḏeš](../../strongs/h/h2320.md), which was the fifth [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [Yôyāḵîn](../../strongs/h/h3112.md) [gālûṯ](../../strongs/h/h1546.md),

<a name="ezekiel_1_3"></a>Ezekiel 1:3

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came expressly unto [Yᵊḥezqē'L](../../strongs/h/h3168.md) the [kōhēn](../../strongs/h/h3548.md), the [ben](../../strongs/h/h1121.md) of [Bûzî](../../strongs/h/h941.md), in the ['erets](../../strongs/h/h776.md) of the [Kaśdîmâ](../../strongs/h/h3778.md) by the [nāhār](../../strongs/h/h5104.md) [kᵊḇār](../../strongs/h/h3529.md); and the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) was there upon him.

<a name="ezekiel_1_4"></a>Ezekiel 1:4

And I [ra'ah](../../strongs/h/h7200.md), and, behold, a [ruwach](../../strongs/h/h7307.md) [saʿar](../../strongs/h/h5591.md) [bow'](../../strongs/h/h935.md) out of the [ṣāp̄ôn](../../strongs/h/h6828.md), a [gadowl](../../strongs/h/h1419.md) [ʿānān](../../strongs/h/h6051.md), and an ['esh](../../strongs/h/h784.md) [laqach](../../strongs/h/h3947.md) itself, and a [nogahh](../../strongs/h/h5051.md) was [cabiyb](../../strongs/h/h5439.md) it, and out of the midst thereof as the ['ayin](../../strongs/h/h5869.md) of [ḥašmāl](../../strongs/h/h2830.md), out of the midst of the ['esh](../../strongs/h/h784.md).

<a name="ezekiel_1_5"></a>Ezekiel 1:5

Also out of the midst thereof came the [dĕmuwth](../../strongs/h/h1823.md) of four [chay](../../strongs/h/h2416.md). And this was their [mar'ê](../../strongs/h/h4758.md); they had the [dĕmuwth](../../strongs/h/h1823.md) of an ['āḏām](../../strongs/h/h120.md).

<a name="ezekiel_1_6"></a>Ezekiel 1:6

And every one had four [paniym](../../strongs/h/h6440.md), and every one had four [kanaph](../../strongs/h/h3671.md).

<a name="ezekiel_1_7"></a>Ezekiel 1:7

And their [regel](../../strongs/h/h7272.md) were [yashar](../../strongs/h/h3477.md) [regel](../../strongs/h/h7272.md); and the [kaph](../../strongs/h/h3709.md) of their [regel](../../strongs/h/h7272.md) was like the [kaph](../../strongs/h/h3709.md) of a [ʿēḡel](../../strongs/h/h5695.md) [regel](../../strongs/h/h7272.md): and they [nāṣaṣ](../../strongs/h/h5340.md) like the ['ayin](../../strongs/h/h5869.md) of [qālāl](../../strongs/h/h7044.md) [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="ezekiel_1_8"></a>Ezekiel 1:8

And they had the [yad](../../strongs/h/h3027.md) of an ['āḏām](../../strongs/h/h120.md) under their [kanaph](../../strongs/h/h3671.md) on their four sides; and they four had their [paniym](../../strongs/h/h6440.md) and their [kanaph](../../strongs/h/h3671.md).

<a name="ezekiel_1_9"></a>Ezekiel 1:9

Their [kanaph](../../strongs/h/h3671.md) were [ḥāḇar](../../strongs/h/h2266.md) ['ishshah](../../strongs/h/h802.md) to ['āḥôṯ](../../strongs/h/h269.md); they [cabab](../../strongs/h/h5437.md) not when they [yālaḵ](../../strongs/h/h3212.md); they [yālaḵ](../../strongs/h/h3212.md) every ['iysh](../../strongs/h/h376.md) [ʿēḇer](../../strongs/h/h5676.md) [paniym](../../strongs/h/h6440.md).

<a name="ezekiel_1_10"></a>Ezekiel 1:10

As for the [dĕmuwth](../../strongs/h/h1823.md) of their [paniym](../../strongs/h/h6440.md), they four had the [paniym](../../strongs/h/h6440.md) of an ['āḏām](../../strongs/h/h120.md), and the [paniym](../../strongs/h/h6440.md) of an ['ariy](../../strongs/h/h738.md), on the [yamiyn](../../strongs/h/h3225.md): and they four had the [paniym](../../strongs/h/h6440.md) of a [showr](../../strongs/h/h7794.md) on the [śᵊmō'l](../../strongs/h/h8040.md); they four also had the [paniym](../../strongs/h/h6440.md) of a [nesheׁr](../../strongs/h/h5404.md).

<a name="ezekiel_1_11"></a>Ezekiel 1:11

Thus were their [paniym](../../strongs/h/h6440.md): and their [kanaph](../../strongs/h/h3671.md) were [pāraḏ](../../strongs/h/h6504.md) [maʿal](../../strongs/h/h4605.md); two wings of every ['iysh](../../strongs/h/h376.md) were [ḥāḇar](../../strongs/h/h2266.md) ['iysh](../../strongs/h/h376.md) to ['iysh](../../strongs/h/h376.md), and two [kāsâ](../../strongs/h/h3680.md) their [gᵊvîyâ](../../strongs/h/h1472.md).

<a name="ezekiel_1_12"></a>Ezekiel 1:12

And they [yālaḵ](../../strongs/h/h3212.md) every ['iysh](../../strongs/h/h376.md) [ʿēḇer](../../strongs/h/h5676.md) [paniym](../../strongs/h/h6440.md): whither the [ruwach](../../strongs/h/h7307.md) was to [yālaḵ](../../strongs/h/h3212.md), they [yālaḵ](../../strongs/h/h3212.md); and they [cabab](../../strongs/h/h5437.md) not when they [yālaḵ](../../strongs/h/h3212.md).

<a name="ezekiel_1_13"></a>Ezekiel 1:13

As for the [dĕmuwth](../../strongs/h/h1823.md) of the [chay](../../strongs/h/h2416.md), their [mar'ê](../../strongs/h/h4758.md) was like [bāʿar](../../strongs/h/h1197.md) [gechel](../../strongs/h/h1513.md) of ['esh](../../strongs/h/h784.md), and like the [mar'ê](../../strongs/h/h4758.md) of [lapîḏ](../../strongs/h/h3940.md): it went up and [halak](../../strongs/h/h1980.md) among the [chay](../../strongs/h/h2416.md); and the ['esh](../../strongs/h/h784.md) was [nogahh](../../strongs/h/h5051.md), and out of the ['esh](../../strongs/h/h784.md) [yāṣā'](../../strongs/h/h3318.md) [baraq](../../strongs/h/h1300.md).

<a name="ezekiel_1_14"></a>Ezekiel 1:14

And the [chay](../../strongs/h/h2416.md) [rāṣā'](../../strongs/h/h7519.md) and [shuwb](../../strongs/h/h7725.md) as the [mar'ê](../../strongs/h/h4758.md) of [bāzāq](../../strongs/h/h965.md).

<a name="ezekiel_1_15"></a>Ezekiel 1:15

Now as I [ra'ah](../../strongs/h/h7200.md) the [chay](../../strongs/h/h2416.md), behold one ['ôp̄ān](../../strongs/h/h212.md) upon the ['erets](../../strongs/h/h776.md) by the [chay](../../strongs/h/h2416.md), with his four [paniym](../../strongs/h/h6440.md).

<a name="ezekiel_1_16"></a>Ezekiel 1:16

The [mar'ê](../../strongs/h/h4758.md) of the ['ôp̄ān](../../strongs/h/h212.md) and their [ma'aseh](../../strongs/h/h4639.md) was like unto the ['ayin](../../strongs/h/h5869.md) of a [taršîš](../../strongs/h/h8658.md): and they four had one [dĕmuwth](../../strongs/h/h1823.md): and their [mar'ê](../../strongs/h/h4758.md) and their [ma'aseh](../../strongs/h/h4639.md) was as it were a ['ôp̄ān](../../strongs/h/h212.md) in the middle of a ['ôp̄ān](../../strongs/h/h212.md).

<a name="ezekiel_1_17"></a>Ezekiel 1:17

When they [yālaḵ](../../strongs/h/h3212.md), they [yālaḵ](../../strongs/h/h3212.md) upon their four sides: and they [cabab](../../strongs/h/h5437.md) not when they [yālaḵ](../../strongs/h/h3212.md).

<a name="ezekiel_1_18"></a>Ezekiel 1:18

As for their [gaḇ](../../strongs/h/h1354.md), they were so [gobahh](../../strongs/h/h1363.md) that they were [yir'ah](../../strongs/h/h3374.md); and their [gaḇ](../../strongs/h/h1354.md) were [mālē'](../../strongs/h/h4392.md) of ['ayin](../../strongs/h/h5869.md) [cabiyb](../../strongs/h/h5439.md) them four.

<a name="ezekiel_1_19"></a>Ezekiel 1:19

And when the [chay](../../strongs/h/h2416.md) [yālaḵ](../../strongs/h/h3212.md), the ['ôp̄ān](../../strongs/h/h212.md) [yālaḵ](../../strongs/h/h3212.md) by them: and when the [chay](../../strongs/h/h2416.md) were [nasa'](../../strongs/h/h5375.md) from the ['erets](../../strongs/h/h776.md), the ['ôp̄ān](../../strongs/h/h212.md) were [nasa'](../../strongs/h/h5375.md).

<a name="ezekiel_1_20"></a>Ezekiel 1:20

Whithersoever the [ruwach](../../strongs/h/h7307.md) was to [yālaḵ](../../strongs/h/h3212.md), they [yālaḵ](../../strongs/h/h3212.md), thither was their [ruwach](../../strongs/h/h7307.md) to [yālaḵ](../../strongs/h/h3212.md); and the ['ôp̄ān](../../strongs/h/h212.md) were [nasa'](../../strongs/h/h5375.md) over [ʿummâ](../../strongs/h/h5980.md) them: for the [ruwach](../../strongs/h/h7307.md) of the [chay](../../strongs/h/h2416.md) was in the ['ôp̄ān](../../strongs/h/h212.md).

<a name="ezekiel_1_21"></a>Ezekiel 1:21

When those [yālaḵ](../../strongs/h/h3212.md), these [yālaḵ](../../strongs/h/h3212.md); and when those ['amad](../../strongs/h/h5975.md), these ['amad](../../strongs/h/h5975.md); and when those were [nasa'](../../strongs/h/h5375.md) from the ['erets](../../strongs/h/h776.md), the ['ôp̄ān](../../strongs/h/h212.md) were [nasa'](../../strongs/h/h5375.md) over [ʿummâ](../../strongs/h/h5980.md) them: for the [ruwach](../../strongs/h/h7307.md) of the [chay](../../strongs/h/h2416.md) was in the ['ôp̄ān](../../strongs/h/h212.md).

<a name="ezekiel_1_22"></a>Ezekiel 1:22

And the [dĕmuwth](../../strongs/h/h1823.md) of the [raqiya'](../../strongs/h/h7549.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [chay](../../strongs/h/h2416.md) was as the ['ayin](../../strongs/h/h5869.md) of the [yare'](../../strongs/h/h3372.md) [qeraḥ](../../strongs/h/h7140.md), [natah](../../strongs/h/h5186.md) over their [ro'sh](../../strongs/h/h7218.md) [maʿal](../../strongs/h/h4605.md).

<a name="ezekiel_1_23"></a>Ezekiel 1:23

And under the [raqiya'](../../strongs/h/h7549.md) were their [kanaph](../../strongs/h/h3671.md) [yashar](../../strongs/h/h3477.md), the ['ishshah](../../strongs/h/h802.md) toward the ['āḥôṯ](../../strongs/h/h269.md): every ['iysh](../../strongs/h/h376.md) had two, which [kāsâ](../../strongs/h/h3680.md) on this side, and every ['iysh](../../strongs/h/h376.md) had two, which [kāsâ](../../strongs/h/h3680.md) on that side, their [gᵊvîyâ](../../strongs/h/h1472.md).

<a name="ezekiel_1_24"></a>Ezekiel 1:24

And when they [yālaḵ](../../strongs/h/h3212.md), I [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of their [kanaph](../../strongs/h/h3671.md), like the [qowl](../../strongs/h/h6963.md) of [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md), as the [qowl](../../strongs/h/h6963.md) of the [Šaday](../../strongs/h/h7706.md), the [qowl](../../strongs/h/h6963.md) of [hămullâ](../../strongs/h/h1999.md), as the [qowl](../../strongs/h/h6963.md) of a [maḥănê](../../strongs/h/h4264.md): when they ['amad](../../strongs/h/h5975.md), they let [rāp̄â](../../strongs/h/h7503.md) their [kanaph](../../strongs/h/h3671.md).

<a name="ezekiel_1_25"></a>Ezekiel 1:25

And there was a [qowl](../../strongs/h/h6963.md) from the [raqiya'](../../strongs/h/h7549.md) that was over their [ro'sh](../../strongs/h/h7218.md), when they ['amad](../../strongs/h/h5975.md), and had let [rāp̄â](../../strongs/h/h7503.md) their [kanaph](../../strongs/h/h3671.md).

<a name="ezekiel_1_26"></a>Ezekiel 1:26

And [maʿal](../../strongs/h/h4605.md) the [raqiya'](../../strongs/h/h7549.md) that was over their [ro'sh](../../strongs/h/h7218.md) was the [dĕmuwth](../../strongs/h/h1823.md) of a [kicce'](../../strongs/h/h3678.md), as the [mar'ê](../../strongs/h/h4758.md) of a [sapîr](../../strongs/h/h5601.md) ['eben](../../strongs/h/h68.md): and upon the [dĕmuwth](../../strongs/h/h1823.md) of the [kicce'](../../strongs/h/h3678.md) was the [dĕmuwth](../../strongs/h/h1823.md) as the [mar'ê](../../strongs/h/h4758.md) of an ['āḏām](../../strongs/h/h120.md) [maʿal](../../strongs/h/h4605.md) upon it.

<a name="ezekiel_1_27"></a>Ezekiel 1:27

And I [ra'ah](../../strongs/h/h7200.md) as the ['ayin](../../strongs/h/h5869.md) of [ḥašmāl](../../strongs/h/h2830.md), as the [mar'ê](../../strongs/h/h4758.md) of ['esh](../../strongs/h/h784.md) [cabiyb](../../strongs/h/h5439.md) [bayith](../../strongs/h/h1004.md) it, from the [mar'ê](../../strongs/h/h4758.md) of his [māṯnayim](../../strongs/h/h4975.md) even [maʿal](../../strongs/h/h4605.md), and from the [mar'ê](../../strongs/h/h4758.md) of his [māṯnayim](../../strongs/h/h4975.md) even [maṭṭâ](../../strongs/h/h4295.md), I [ra'ah](../../strongs/h/h7200.md) as it were the [mar'ê](../../strongs/h/h4758.md) of ['esh](../../strongs/h/h784.md), and it had [nogahh](../../strongs/h/h5051.md) [cabiyb](../../strongs/h/h5439.md).

<a name="ezekiel_1_28"></a>Ezekiel 1:28

As the [mar'ê](../../strongs/h/h4758.md) of the [qesheth](../../strongs/h/h7198.md) that is in the [ʿānān](../../strongs/h/h6051.md) in the [yowm](../../strongs/h/h3117.md) of [gešem](../../strongs/h/h1653.md), so was the [mar'ê](../../strongs/h/h4758.md) of the [nogahh](../../strongs/h/h5051.md) [cabiyb](../../strongs/h/h5439.md). This was the [mar'ê](../../strongs/h/h4758.md) of the [dĕmuwth](../../strongs/h/h1823.md) of the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md). And when I [ra'ah](../../strongs/h/h7200.md) it, I [naphal](../../strongs/h/h5307.md) upon my [paniym](../../strongs/h/h6440.md), and I [shama'](../../strongs/h/h8085.md) a [qowl](../../strongs/h/h6963.md) of one that [dabar](../../strongs/h/h1696.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 2](ezekiel_2.md)