# [Ezekiel 44](https://www.blueletterbible.org/kjv/ezekiel/44)

<a name="ezekiel_44_1"></a>Ezekiel 44:1

Then he [shuwb](../../strongs/h/h7725.md) me the [derek](../../strongs/h/h1870.md) of the [sha'ar](../../strongs/h/h8179.md) of the [ḥîṣôn](../../strongs/h/h2435.md) [miqdash](../../strongs/h/h4720.md) which [panah](../../strongs/h/h6437.md) toward the [qāḏîm](../../strongs/h/h6921.md); and it was [cagar](../../strongs/h/h5462.md).

<a name="ezekiel_44_2"></a>Ezekiel 44:2

Then ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto me; This [sha'ar](../../strongs/h/h8179.md) shall be [cagar](../../strongs/h/h5462.md), it shall not be [pāṯaḥ](../../strongs/h/h6605.md), and no ['iysh](../../strongs/h/h376.md) shall [bow'](../../strongs/h/h935.md) in by it; because [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), hath [bow'](../../strongs/h/h935.md) in by it, therefore it shall be [cagar](../../strongs/h/h5462.md).

<a name="ezekiel_44_3"></a>Ezekiel 44:3

It is for the [nāśî'](../../strongs/h/h5387.md); the [nāśî'](../../strongs/h/h5387.md), he shall [yashab](../../strongs/h/h3427.md) in it to ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); he shall [bow'](../../strongs/h/h935.md) by the [derek](../../strongs/h/h1870.md) of the ['ûlām](../../strongs/h/h197.md) of that [sha'ar](../../strongs/h/h8179.md), and shall [yāṣā'](../../strongs/h/h3318.md) by the [derek](../../strongs/h/h1870.md) of the same.

<a name="ezekiel_44_4"></a>Ezekiel 44:4

Then [bow'](../../strongs/h/h935.md) he me the [derek](../../strongs/h/h1870.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md) [sha'ar](../../strongs/h/h8179.md) [paniym](../../strongs/h/h6440.md) the [bayith](../../strongs/h/h1004.md): and I [ra'ah](../../strongs/h/h7200.md), and, behold, the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [mālā'](../../strongs/h/h4390.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md): and I [naphal](../../strongs/h/h5307.md) upon my [paniym](../../strongs/h/h6440.md).

<a name="ezekiel_44_5"></a>Ezekiel 44:5

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [śûm](../../strongs/h/h7760.md) [leb](../../strongs/h/h3820.md), and [ra'ah](../../strongs/h/h7200.md) with thine ['ayin](../../strongs/h/h5869.md), and [shama'](../../strongs/h/h8085.md) with thine ['ozen](../../strongs/h/h241.md) all that I [dabar](../../strongs/h/h1696.md) unto thee concerning all the [chuqqah](../../strongs/h/h2708.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and all the [towrah](../../strongs/h/h8451.md) thereof; and [śûm](../../strongs/h/h7760.md) [leb](../../strongs/h/h3820.md) the [māḇô'](../../strongs/h/h3996.md) of the [bayith](../../strongs/h/h1004.md), with every [môṣā'](../../strongs/h/h4161.md) of the [miqdash](../../strongs/h/h4720.md).

<a name="ezekiel_44_6"></a>Ezekiel 44:6

And thou shalt ['āmar](../../strongs/h/h559.md) to the [mᵊrî](../../strongs/h/h4805.md), even to the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); O ye [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), let it [rab](../../strongs/h/h7227.md) you of all your [tôʿēḇâ](../../strongs/h/h8441.md),

<a name="ezekiel_44_7"></a>Ezekiel 44:7

In that ye have [bow'](../../strongs/h/h935.md) [ben](../../strongs/h/h1121.md) [nēḵār](../../strongs/h/h5236.md), [ʿārēl](../../strongs/h/h6189.md) in [leb](../../strongs/h/h3820.md), and [ʿārēl](../../strongs/h/h6189.md) in [basar](../../strongs/h/h1320.md), to be in my [miqdash](../../strongs/h/h4720.md), to [ḥālal](../../strongs/h/h2490.md) it, even my [bayith](../../strongs/h/h1004.md), when ye [qāraḇ](../../strongs/h/h7126.md) my [lechem](../../strongs/h/h3899.md), the [cheleb](../../strongs/h/h2459.md) and the [dam](../../strongs/h/h1818.md), and they have [pārar](../../strongs/h/h6565.md) my [bĕriyth](../../strongs/h/h1285.md) because of all your [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="ezekiel_44_8"></a>Ezekiel 44:8

And ye have not [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of mine [qodesh](../../strongs/h/h6944.md): but ye have [śûm](../../strongs/h/h7760.md) [shamar](../../strongs/h/h8104.md) of my [mišmereṯ](../../strongs/h/h4931.md) in my [miqdash](../../strongs/h/h4720.md) for yourselves.

<a name="ezekiel_44_9"></a>Ezekiel 44:9

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); No [ben](../../strongs/h/h1121.md) [nēḵār](../../strongs/h/h5236.md), [ʿārēl](../../strongs/h/h6189.md) in [leb](../../strongs/h/h3820.md), nor [ʿārēl](../../strongs/h/h6189.md) in [basar](../../strongs/h/h1320.md), shall [bow'](../../strongs/h/h935.md) into my [miqdash](../../strongs/h/h4720.md), that is [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_44_10"></a>Ezekiel 44:10

And the [Lᵊvî](../../strongs/h/h3881.md) that are [rachaq](../../strongs/h/h7368.md) from me, when [Yisra'el](../../strongs/h/h3478.md) [tāʿâ](../../strongs/h/h8582.md), which [tāʿâ](../../strongs/h/h8582.md) from me ['aḥar](../../strongs/h/h310.md) their [gillûl](../../strongs/h/h1544.md); they shall even [nasa'](../../strongs/h/h5375.md) their ['avon](../../strongs/h/h5771.md).

<a name="ezekiel_44_11"></a>Ezekiel 44:11

Yet they shall be [sharath](../../strongs/h/h8334.md) in my [miqdash](../../strongs/h/h4720.md), having [pᵊqudâ](../../strongs/h/h6486.md) at the [sha'ar](../../strongs/h/h8179.md) of the [bayith](../../strongs/h/h1004.md), and [sharath](../../strongs/h/h8334.md) to the [bayith](../../strongs/h/h1004.md): they shall [šāḥaṭ](../../strongs/h/h7819.md) the [ʿōlâ](../../strongs/h/h5930.md) and the [zebach](../../strongs/h/h2077.md) for the ['am](../../strongs/h/h5971.md), and they shall ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) them to [sharath](../../strongs/h/h8334.md) unto them.

<a name="ezekiel_44_12"></a>Ezekiel 44:12

Because they [sharath](../../strongs/h/h8334.md) unto them [paniym](../../strongs/h/h6440.md) their [gillûl](../../strongs/h/h1544.md), and caused the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) to [miḵšôl](../../strongs/h/h4383.md) into ['avon](../../strongs/h/h5771.md); therefore have I [nasa'](../../strongs/h/h5375.md) mine [yad](../../strongs/h/h3027.md) against them, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), and they shall [nasa'](../../strongs/h/h5375.md) their ['avon](../../strongs/h/h5771.md).

<a name="ezekiel_44_13"></a>Ezekiel 44:13

And they shall not come [nāḡaš](../../strongs/h/h5066.md) unto me, to [kāhan](../../strongs/h/h3547.md) unto me, nor to [nāḡaš](../../strongs/h/h5066.md) to any of my [qodesh](../../strongs/h/h6944.md), in the [qodesh](../../strongs/h/h6944.md): but they shall [nasa'](../../strongs/h/h5375.md) their [kĕlimmah](../../strongs/h/h3639.md), and their [tôʿēḇâ](../../strongs/h/h8441.md) which they have ['asah](../../strongs/h/h6213.md).

<a name="ezekiel_44_14"></a>Ezekiel 44:14

But I will [nathan](../../strongs/h/h5414.md) them [shamar](../../strongs/h/h8104.md) of the [mišmereṯ](../../strongs/h/h4931.md) of the [bayith](../../strongs/h/h1004.md), for all the [ʿăḇōḏâ](../../strongs/h/h5656.md) thereof, and for all that shall be ['asah](../../strongs/h/h6213.md) therein.

<a name="ezekiel_44_15"></a>Ezekiel 44:15

But the [kōhēn](../../strongs/h/h3548.md) the [Lᵊvî](../../strongs/h/h3881.md), the [ben](../../strongs/h/h1121.md) of [Ṣāḏôq](../../strongs/h/h6659.md), that [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of my [miqdash](../../strongs/h/h4720.md) when the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) went [tāʿâ](../../strongs/h/h8582.md) from me, they shall [qāraḇ](../../strongs/h/h7126.md) to me to [sharath](../../strongs/h/h8334.md) unto me, and they shall ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) me to [qāraḇ](../../strongs/h/h7126.md) unto me the [cheleb](../../strongs/h/h2459.md) and the [dam](../../strongs/h/h1818.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md):

<a name="ezekiel_44_16"></a>Ezekiel 44:16

They shall [bow'](../../strongs/h/h935.md) into my [miqdash](../../strongs/h/h4720.md), and they shall [qāraḇ](../../strongs/h/h7126.md) to my [šulḥān](../../strongs/h/h7979.md), to [sharath](../../strongs/h/h8334.md) unto me, and they shall [shamar](../../strongs/h/h8104.md) my [mišmereṯ](../../strongs/h/h4931.md).

<a name="ezekiel_44_17"></a>Ezekiel 44:17

And it shall come to pass, that when they [bow'](../../strongs/h/h935.md) in at the [sha'ar](../../strongs/h/h8179.md) of the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md), they shall be [labash](../../strongs/h/h3847.md) with [pēšeṯ](../../strongs/h/h6593.md) [beḡeḏ](../../strongs/h/h899.md); and no [ṣemer](../../strongs/h/h6785.md) shall [ʿālâ](../../strongs/h/h5927.md) upon them, whiles they [sharath](../../strongs/h/h8334.md) in the [sha'ar](../../strongs/h/h8179.md) of the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md), and [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_44_18"></a>Ezekiel 44:18

They shall have [pēšeṯ](../../strongs/h/h6593.md) [pᵊ'ēr](../../strongs/h/h6287.md) upon their [ro'sh](../../strongs/h/h7218.md), and shall have [pēšeṯ](../../strongs/h/h6593.md) [miḵnās](../../strongs/h/h4370.md) upon their [māṯnayim](../../strongs/h/h4975.md); they shall not [ḥāḡar](../../strongs/h/h2296.md) themselves with [yezaʿ](../../strongs/h/h3154.md).

<a name="ezekiel_44_19"></a>Ezekiel 44:19

And when they [yāṣā'](../../strongs/h/h3318.md) into the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md), even into the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md) to the ['am](../../strongs/h/h5971.md), they shall put [pāšaṭ](../../strongs/h/h6584.md) their [beḡeḏ](../../strongs/h/h899.md) wherein they [sharath](../../strongs/h/h8334.md), and [yānaḥ](../../strongs/h/h3240.md) them in the [qodesh](../../strongs/h/h6944.md) [liškâ](../../strongs/h/h3957.md), and they shall [labash](../../strongs/h/h3847.md) on ['aḥēr](../../strongs/h/h312.md) [beḡeḏ](../../strongs/h/h899.md); and they shall not [qadash](../../strongs/h/h6942.md) the ['am](../../strongs/h/h5971.md) with their [beḡeḏ](../../strongs/h/h899.md).

<a name="ezekiel_44_20"></a>Ezekiel 44:20

Neither shall they [gālaḥ](../../strongs/h/h1548.md) their [ro'sh](../../strongs/h/h7218.md), nor suffer their [peraʿ](../../strongs/h/h6545.md) to [shalach](../../strongs/h/h7971.md); they shall [kāsam](../../strongs/h/h3697.md) [kāsam](../../strongs/h/h3697.md) their [ro'sh](../../strongs/h/h7218.md).

<a name="ezekiel_44_21"></a>Ezekiel 44:21

Neither shall any [kōhēn](../../strongs/h/h3548.md) [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md), when they [bow'](../../strongs/h/h935.md) into the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md).

<a name="ezekiel_44_22"></a>Ezekiel 44:22

Neither shall they [laqach](../../strongs/h/h3947.md) for their ['ishshah](../../strongs/h/h802.md) an ['almānâ](../../strongs/h/h490.md), nor her that is put [gāraš](../../strongs/h/h1644.md): but they shall [laqach](../../strongs/h/h3947.md) [bᵊṯûlâ](../../strongs/h/h1330.md) of the [zera'](../../strongs/h/h2233.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), or an ['almānâ](../../strongs/h/h490.md) that had a [kōhēn](../../strongs/h/h3548.md) before.

<a name="ezekiel_44_23"></a>Ezekiel 44:23

And they shall [yārâ](../../strongs/h/h3384.md) my ['am](../../strongs/h/h5971.md) the [qodesh](../../strongs/h/h6944.md) and [ḥōl](../../strongs/h/h2455.md), and [yada'](../../strongs/h/h3045.md) between the [tame'](../../strongs/h/h2931.md) and the [tahowr](../../strongs/h/h2889.md).

<a name="ezekiel_44_24"></a>Ezekiel 44:24

And in [rîḇ](../../strongs/h/h7379.md) they shall ['amad](../../strongs/h/h5975.md) in [shaphat](../../strongs/h/h8199.md); and they shall [shaphat](../../strongs/h/h8199.md) it according to my [mishpat](../../strongs/h/h4941.md): and they shall [shamar](../../strongs/h/h8104.md) my [towrah](../../strongs/h/h8451.md) and my [chuqqah](../../strongs/h/h2708.md) in all mine [môʿēḏ](../../strongs/h/h4150.md); and they shall [qadash](../../strongs/h/h6942.md) my [shabbath](../../strongs/h/h7676.md).

<a name="ezekiel_44_25"></a>Ezekiel 44:25

And they shall [bow'](../../strongs/h/h935.md) at no [muwth](../../strongs/h/h4191.md) ['āḏām](../../strongs/h/h120.md) to [ṭāmē'](../../strongs/h/h2930.md) themselves: but for ['ab](../../strongs/h/h1.md), or for ['em](../../strongs/h/h517.md), or for [ben](../../strongs/h/h1121.md), or for [bath](../../strongs/h/h1323.md), for ['ach](../../strongs/h/h251.md), or for ['āḥôṯ](../../strongs/h/h269.md) that hath had no ['iysh](../../strongs/h/h376.md), they may [ṭāmē'](../../strongs/h/h2930.md) themselves.

<a name="ezekiel_44_26"></a>Ezekiel 44:26

And ['aḥar](../../strongs/h/h310.md) he is [ṭāhŏrâ](../../strongs/h/h2893.md), they shall [sāp̄ar](../../strongs/h/h5608.md) unto him seven [yowm](../../strongs/h/h3117.md).

<a name="ezekiel_44_27"></a>Ezekiel 44:27

And in the [yowm](../../strongs/h/h3117.md) that he [bow'](../../strongs/h/h935.md) into the [qodesh](../../strongs/h/h6944.md), unto the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md), to [sharath](../../strongs/h/h8334.md) in the [qodesh](../../strongs/h/h6944.md), he shall [qāraḇ](../../strongs/h/h7126.md) his [chatta'ath](../../strongs/h/h2403.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_44_28"></a>Ezekiel 44:28

And it shall be unto them for a [nachalah](../../strongs/h/h5159.md): I am their [nachalah](../../strongs/h/h5159.md): and ye shall [nathan](../../strongs/h/h5414.md) them no ['achuzzah](../../strongs/h/h272.md) in [Yisra'el](../../strongs/h/h3478.md): I am their ['achuzzah](../../strongs/h/h272.md).

<a name="ezekiel_44_29"></a>Ezekiel 44:29

They shall ['akal](../../strongs/h/h398.md) the [minchah](../../strongs/h/h4503.md), and the [chatta'ath](../../strongs/h/h2403.md), and the ['āšām](../../strongs/h/h817.md); and every [ḥērem](../../strongs/h/h2764.md) in [Yisra'el](../../strongs/h/h3478.md) shall be theirs.

<a name="ezekiel_44_30"></a>Ezekiel 44:30

And the [re'shiyth](../../strongs/h/h7225.md) of all the [bikûr](../../strongs/h/h1061.md) of all things, and every [tᵊrûmâ](../../strongs/h/h8641.md) of all, of every sort of your [tᵊrûmâ](../../strongs/h/h8641.md), shall be the [kōhēn](../../strongs/h/h3548.md): ye shall also [nathan](../../strongs/h/h5414.md) unto the [kōhēn](../../strongs/h/h3548.md) the [re'shiyth](../../strongs/h/h7225.md) of your [ʿărîsâ](../../strongs/h/h6182.md), that he may cause the [bĕrakah](../../strongs/h/h1293.md) to [nuwach](../../strongs/h/h5117.md) in thine [bayith](../../strongs/h/h1004.md).

<a name="ezekiel_44_31"></a>Ezekiel 44:31

The [kōhēn](../../strongs/h/h3548.md) shall not ['akal](../../strongs/h/h398.md) of any thing that is [nᵊḇēlâ](../../strongs/h/h5038.md), or [ṭᵊrēp̄â](../../strongs/h/h2966.md), whether it be [ʿôp̄](../../strongs/h/h5775.md) or [bĕhemah](../../strongs/h/h929.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 43](ezekiel_43.md) - [Ezekiel 45](ezekiel_45.md)