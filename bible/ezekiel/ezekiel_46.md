# [Ezekiel 46](https://www.blueletterbible.org/kjv/ezekiel/46)

<a name="ezekiel_46_1"></a>Ezekiel 46:1

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); The [sha'ar](../../strongs/h/h8179.md) of the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md) that [panah](../../strongs/h/h6437.md) toward the [qāḏîm](../../strongs/h/h6921.md) shall be [cagar](../../strongs/h/h5462.md) the six [ma'aseh](../../strongs/h/h4639.md) [yowm](../../strongs/h/h3117.md); but on the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md) it shall be [pāṯaḥ](../../strongs/h/h6605.md), and in the [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md) it shall be [pāṯaḥ](../../strongs/h/h6605.md).

<a name="ezekiel_46_2"></a>Ezekiel 46:2

And the [nāśî'](../../strongs/h/h5387.md) shall [bow'](../../strongs/h/h935.md) by the [derek](../../strongs/h/h1870.md) of the ['ûlām](../../strongs/h/h197.md) of that [sha'ar](../../strongs/h/h8179.md) [ḥûṣ](../../strongs/h/h2351.md), and shall ['amad](../../strongs/h/h5975.md) by the [mᵊzûzâ](../../strongs/h/h4201.md) of the [sha'ar](../../strongs/h/h8179.md), and the [kōhēn](../../strongs/h/h3548.md) shall ['asah](../../strongs/h/h6213.md) his [ʿōlâ](../../strongs/h/h5930.md) and his [šelem](../../strongs/h/h8002.md), and he shall [shachah](../../strongs/h/h7812.md) at the [mip̄tān](../../strongs/h/h4670.md) of the [sha'ar](../../strongs/h/h8179.md): then he shall [yāṣā'](../../strongs/h/h3318.md); but the [sha'ar](../../strongs/h/h8179.md) shall not be [cagar](../../strongs/h/h5462.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="ezekiel_46_3"></a>Ezekiel 46:3

Likewise the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) shall [shachah](../../strongs/h/h7812.md) at the [peṯaḥ](../../strongs/h/h6607.md) of this [sha'ar](../../strongs/h/h8179.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) in the [shabbath](../../strongs/h/h7676.md) and in the [ḥōḏeš](../../strongs/h/h2320.md).

<a name="ezekiel_46_4"></a>Ezekiel 46:4

And the [ʿōlâ](../../strongs/h/h5930.md) that the [nāśî'](../../strongs/h/h5387.md) shall [qāraḇ](../../strongs/h/h7126.md) unto [Yĕhovah](../../strongs/h/h3068.md) in the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md) shall be six [keḇeś](../../strongs/h/h3532.md) without [tamiym](../../strongs/h/h8549.md), and an ['ayil](../../strongs/h/h352.md) without [tamiym](../../strongs/h/h8549.md).

<a name="ezekiel_46_5"></a>Ezekiel 46:5

And the [minchah](../../strongs/h/h4503.md) shall be an ['êp̄â](../../strongs/h/h374.md) for an ['ayil](../../strongs/h/h352.md), and the [minchah](../../strongs/h/h4503.md) for the [keḇeś](../../strongs/h/h3532.md) as he shall be [yad](../../strongs/h/h3027.md) to [mataṯ](../../strongs/h/h4991.md), and an [hîn](../../strongs/h/h1969.md) of [šemen](../../strongs/h/h8081.md) to an ['êp̄â](../../strongs/h/h374.md).

<a name="ezekiel_46_6"></a>Ezekiel 46:6

And in the [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md) it shall be a [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md) without [tamiym](../../strongs/h/h8549.md), and six [keḇeś](../../strongs/h/h3532.md), and an ['ayil](../../strongs/h/h352.md): they shall be without [tamiym](../../strongs/h/h8549.md).

<a name="ezekiel_46_7"></a>Ezekiel 46:7

And he shall ['asah](../../strongs/h/h6213.md) a [minchah](../../strongs/h/h4503.md), an ['êp̄â](../../strongs/h/h374.md) for a [par](../../strongs/h/h6499.md), and an ['êp̄â](../../strongs/h/h374.md) for an ['ayil](../../strongs/h/h352.md), and for the [keḇeś](../../strongs/h/h3532.md) according as his [yad](../../strongs/h/h3027.md) shall [nāśaḡ](../../strongs/h/h5381.md) unto, and an [hîn](../../strongs/h/h1969.md) of [šemen](../../strongs/h/h8081.md) to an ['êp̄â](../../strongs/h/h374.md).

<a name="ezekiel_46_8"></a>Ezekiel 46:8

And when the [nāśî'](../../strongs/h/h5387.md) shall [bow'](../../strongs/h/h935.md), he shall [bow'](../../strongs/h/h935.md) in by the [derek](../../strongs/h/h1870.md) of the ['ûlām](../../strongs/h/h197.md) of that [sha'ar](../../strongs/h/h8179.md), and he shall [yāṣā'](../../strongs/h/h3318.md) by the [derek](../../strongs/h/h1870.md) thereof.

<a name="ezekiel_46_9"></a>Ezekiel 46:9

But when the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) shall [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) in the [môʿēḏ](../../strongs/h/h4150.md), he that [bow'](../../strongs/h/h935.md) in by the [derek](../../strongs/h/h1870.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md) [sha'ar](../../strongs/h/h8179.md) to [shachah](../../strongs/h/h7812.md) shall [yāṣā'](../../strongs/h/h3318.md) by the [derek](../../strongs/h/h1870.md) of the [neḡeḇ](../../strongs/h/h5045.md) [sha'ar](../../strongs/h/h8179.md); and he that [bow'](../../strongs/h/h935.md) by the [derek](../../strongs/h/h1870.md) of the [neḡeḇ](../../strongs/h/h5045.md) [sha'ar](../../strongs/h/h8179.md) shall [yāṣā'](../../strongs/h/h3318.md) by the [derek](../../strongs/h/h1870.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md) [sha'ar](../../strongs/h/h8179.md): he shall not [shuwb](../../strongs/h/h7725.md) by the [derek](../../strongs/h/h1870.md) of the [sha'ar](../../strongs/h/h8179.md) whereby he [bow'](../../strongs/h/h935.md), but shall [yāṣā'](../../strongs/h/h3318.md) over [nēḵaḥ](../../strongs/h/h5226.md) it.

<a name="ezekiel_46_10"></a>Ezekiel 46:10

And the [nāśî'](../../strongs/h/h5387.md) in the [tavek](../../strongs/h/h8432.md) of them, when they [bow'](../../strongs/h/h935.md), shall [bow'](../../strongs/h/h935.md); and when they [yāṣā'](../../strongs/h/h3318.md), shall [yāṣā'](../../strongs/h/h3318.md).

<a name="ezekiel_46_11"></a>Ezekiel 46:11

And in the [ḥāḡ](../../strongs/h/h2282.md) and in the [môʿēḏ](../../strongs/h/h4150.md) the [minchah](../../strongs/h/h4503.md) shall be an ['êp̄â](../../strongs/h/h374.md) to a [par](../../strongs/h/h6499.md), and an ['êp̄â](../../strongs/h/h374.md) to an ['ayil](../../strongs/h/h352.md), and to the [keḇeś](../../strongs/h/h3532.md) as he is [yad](../../strongs/h/h3027.md) to [mataṯ](../../strongs/h/h4991.md), and an [hîn](../../strongs/h/h1969.md) of [šemen](../../strongs/h/h8081.md) to an ['êp̄â](../../strongs/h/h374.md).

<a name="ezekiel_46_12"></a>Ezekiel 46:12

Now when the [nāśî'](../../strongs/h/h5387.md) shall ['asah](../../strongs/h/h6213.md) a [nᵊḏāḇâ](../../strongs/h/h5071.md) [ʿōlâ](../../strongs/h/h5930.md) or [šelem](../../strongs/h/h8002.md) [nᵊḏāḇâ](../../strongs/h/h5071.md) unto [Yĕhovah](../../strongs/h/h3068.md), one shall then [pāṯaḥ](../../strongs/h/h6605.md) him the [sha'ar](../../strongs/h/h8179.md) that [panah](../../strongs/h/h6437.md) toward the [qāḏîm](../../strongs/h/h6921.md), and he shall ['asah](../../strongs/h/h6213.md) his [ʿōlâ](../../strongs/h/h5930.md) and his [šelem](../../strongs/h/h8002.md), as he ['asah](../../strongs/h/h6213.md) on the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md): then he shall [yāṣā'](../../strongs/h/h3318.md); and ['aḥar](../../strongs/h/h310.md) his [yāṣā'](../../strongs/h/h3318.md) one shall [cagar](../../strongs/h/h5462.md) the [sha'ar](../../strongs/h/h8179.md).

<a name="ezekiel_46_13"></a>Ezekiel 46:13

Thou shalt [yowm](../../strongs/h/h3117.md) ['asah](../../strongs/h/h6213.md) a [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md) of a [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) without [tamiym](../../strongs/h/h8549.md): thou shalt ['asah](../../strongs/h/h6213.md) it [boqer](../../strongs/h/h1242.md) [boqer](../../strongs/h/h1242.md).

<a name="ezekiel_46_14"></a>Ezekiel 46:14

And thou shalt ['asah](../../strongs/h/h6213.md) a [minchah](../../strongs/h/h4503.md) for it every [boqer](../../strongs/h/h1242.md), the sixth part of an ['êp̄â](../../strongs/h/h374.md), and the third part of an [hîn](../../strongs/h/h1969.md) of [šemen](../../strongs/h/h8081.md), to [rāsas](../../strongs/h/h7450.md) the [sōleṯ](../../strongs/h/h5560.md); a [minchah](../../strongs/h/h4503.md) [tāmîḏ](../../strongs/h/h8548.md) by a ['owlam](../../strongs/h/h5769.md) [chuqqah](../../strongs/h/h2708.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_46_15"></a>Ezekiel 46:15

Thus shall they ['asah](../../strongs/h/h6213.md) ['asah](../../strongs/h/h6213.md) the [keḇeś](../../strongs/h/h3532.md), and the [minchah](../../strongs/h/h4503.md), and the [šemen](../../strongs/h/h8081.md), every [boqer](../../strongs/h/h1242.md) for a [tāmîḏ](../../strongs/h/h8548.md) [ʿōlâ](../../strongs/h/h5930.md).

<a name="ezekiel_46_16"></a>Ezekiel 46:16

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); If the [nāśî'](../../strongs/h/h5387.md) [nathan](../../strongs/h/h5414.md) a [matānâ](../../strongs/h/h4979.md) unto ['iysh](../../strongs/h/h376.md) of his [ben](../../strongs/h/h1121.md), the [nachalah](../../strongs/h/h5159.md) thereof shall be his [ben](../../strongs/h/h1121.md); it shall be their ['achuzzah](../../strongs/h/h272.md) by [nachalah](../../strongs/h/h5159.md).

<a name="ezekiel_46_17"></a>Ezekiel 46:17

But if he [nathan](../../strongs/h/h5414.md) a [matānâ](../../strongs/h/h4979.md) of his [nachalah](../../strongs/h/h5159.md) to one of his ['ebed](../../strongs/h/h5650.md), then it shall be his to the [šānâ](../../strongs/h/h8141.md) of [dᵊrôr](../../strongs/h/h1865.md); after it shall [shuwb](../../strongs/h/h7725.md) to the [nāśî'](../../strongs/h/h5387.md): but his [nachalah](../../strongs/h/h5159.md) shall be his [ben](../../strongs/h/h1121.md) for them.

<a name="ezekiel_46_18"></a>Ezekiel 46:18

Moreover the [nāśî'](../../strongs/h/h5387.md) shall not [laqach](../../strongs/h/h3947.md) of the ['am](../../strongs/h/h5971.md) [nachalah](../../strongs/h/h5159.md) by [yānâ](../../strongs/h/h3238.md), to [yānâ](../../strongs/h/h3238.md) them out of their ['achuzzah](../../strongs/h/h272.md); but he shall give his [ben](../../strongs/h/h1121.md) [nāḥal](../../strongs/h/h5157.md) out of his own ['achuzzah](../../strongs/h/h272.md): that my ['am](../../strongs/h/h5971.md) be not [puwts](../../strongs/h/h6327.md) every ['iysh](../../strongs/h/h376.md) from his ['achuzzah](../../strongs/h/h272.md).

<a name="ezekiel_46_19"></a>Ezekiel 46:19

After he [bow'](../../strongs/h/h935.md) me through the [māḇô'](../../strongs/h/h3996.md), which was at the [kāṯēp̄](../../strongs/h/h3802.md) of the [sha'ar](../../strongs/h/h8179.md), into the [qodesh](../../strongs/h/h6944.md) [liškâ](../../strongs/h/h3957.md) of the [kōhēn](../../strongs/h/h3548.md), which [panah](../../strongs/h/h6437.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md): and, behold, there was a [maqowm](../../strongs/h/h4725.md) on the two [yᵊrēḵâ](../../strongs/h/h3411.md) [yam](../../strongs/h/h3220.md).

<a name="ezekiel_46_20"></a>Ezekiel 46:20

Then ['āmar](../../strongs/h/h559.md) he unto me, This is the [maqowm](../../strongs/h/h4725.md) where the [kōhēn](../../strongs/h/h3548.md) shall [bāšal](../../strongs/h/h1310.md) the ['āšām](../../strongs/h/h817.md) and the sin [chatta'ath](../../strongs/h/h2403.md), where they shall ['āp̄â](../../strongs/h/h644.md) the [minchah](../../strongs/h/h4503.md); that they [yāṣā'](../../strongs/h/h3318.md) them not out into the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md), to [qadash](../../strongs/h/h6942.md) the ['am](../../strongs/h/h5971.md).

<a name="ezekiel_46_21"></a>Ezekiel 46:21

Then he [yāṣā'](../../strongs/h/h3318.md) me into the [ḥîṣôn](../../strongs/h/h2435.md) [ḥāṣēr](../../strongs/h/h2691.md), and caused me to ['abar](../../strongs/h/h5674.md) the four [miqṣôaʿ](../../strongs/h/h4740.md) of the [ḥāṣēr](../../strongs/h/h2691.md); and, behold, in [miqṣôaʿ](../../strongs/h/h4740.md) [miqṣôaʿ](../../strongs/h/h4740.md) of the [ḥāṣēr](../../strongs/h/h2691.md) [ḥāṣēr](../../strongs/h/h2691.md) there was a [ḥāṣēr](../../strongs/h/h2691.md).

<a name="ezekiel_46_22"></a>Ezekiel 46:22

In the four [miqṣôaʿ](../../strongs/h/h4740.md) of the [ḥāṣēr](../../strongs/h/h2691.md) there were [ḥāṣēr](../../strongs/h/h2691.md) [qāṭar](../../strongs/h/h7000.md) of forty ['ōreḵ](../../strongs/h/h753.md) and thirty [rōḥaḇ](../../strongs/h/h7341.md): these four [qāṣaʿ](../../strongs/h/h7106.md) were of one [midâ](../../strongs/h/h4060.md).

<a name="ezekiel_46_23"></a>Ezekiel 46:23

And there was a [ṭûr](../../strongs/h/h2905.md) [cabiyb](../../strongs/h/h5439.md) in them, [cabiyb](../../strongs/h/h5439.md) them four, and it was ['asah](../../strongs/h/h6213.md) with [mᵊḇaššᵊlâ](../../strongs/h/h4018.md) under the [ṭîrâ](../../strongs/h/h2918.md) [cabiyb](../../strongs/h/h5439.md).

<a name="ezekiel_46_24"></a>Ezekiel 46:24

Then ['āmar](../../strongs/h/h559.md) he unto me, These are the [bayith](../../strongs/h/h1004.md) of them that [bāšal](../../strongs/h/h1310.md), where the [sharath](../../strongs/h/h8334.md) of the [bayith](../../strongs/h/h1004.md) shall [bāšal](../../strongs/h/h1310.md) the [zebach](../../strongs/h/h2077.md) of the ['am](../../strongs/h/h5971.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 45](ezekiel_45.md) - [Ezekiel 47](ezekiel_47.md)