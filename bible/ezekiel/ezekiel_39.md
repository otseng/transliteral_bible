# [Ezekiel 39](https://www.blueletterbible.org/kjv/ezekiel/39)

<a name="ezekiel_39_1"></a>Ezekiel 39:1

Therefore, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nāḇā'](../../strongs/h/h5012.md) against [Gôḡ](../../strongs/h/h1463.md), and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I am against thee, O [Gôḡ](../../strongs/h/h1463.md), the [ro'sh](../../strongs/h/h7218.md) [nāśî'](../../strongs/h/h5387.md) of [Mešeḵ](../../strongs/h/h4902.md) and [Tuḇal](../../strongs/h/h8422.md):

<a name="ezekiel_39_2"></a>Ezekiel 39:2

And I will [shuwb](../../strongs/h/h7725.md) thee, and leave but the sixth [šāšā'](../../strongs/h/h8338.md) of thee, and will cause thee to [ʿālâ](../../strongs/h/h5927.md) from the [ṣāp̄ôn](../../strongs/h/h6828.md) [yᵊrēḵâ](../../strongs/h/h3411.md), and will [bow'](../../strongs/h/h935.md) thee upon the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="ezekiel_39_3"></a>Ezekiel 39:3

And I will [nakah](../../strongs/h/h5221.md) thy [qesheth](../../strongs/h/h7198.md) out of thy [śᵊmō'l](../../strongs/h/h8040.md) [yad](../../strongs/h/h3027.md), and will cause thine [chets](../../strongs/h/h2671.md) to [naphal](../../strongs/h/h5307.md) out of thy [yamiyn](../../strongs/h/h3225.md) [yad](../../strongs/h/h3027.md).

<a name="ezekiel_39_4"></a>Ezekiel 39:4

Thou shalt [naphal](../../strongs/h/h5307.md) upon the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md), thou, and all thy ['ăḡap̄](../../strongs/h/h102.md), and the ['am](../../strongs/h/h5971.md) that is with thee: I will [nathan](../../strongs/h/h5414.md) thee unto the [ʿayiṭ](../../strongs/h/h5861.md) [tsippowr](../../strongs/h/h6833.md) of every [kanaph](../../strongs/h/h3671.md), and to the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) to be ['oklah](../../strongs/h/h402.md).

<a name="ezekiel_39_5"></a>Ezekiel 39:5

Thou shalt [naphal](../../strongs/h/h5307.md) upon the [paniym](../../strongs/h/h6440.md) [sadeh](../../strongs/h/h7704.md): for I have [dabar](../../strongs/h/h1696.md) it, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_39_6"></a>Ezekiel 39:6

And I will [shalach](../../strongs/h/h7971.md) an ['esh](../../strongs/h/h784.md) on [Māḡôḡ](../../strongs/h/h4031.md), and among them that [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md) in the ['î](../../strongs/h/h339.md): and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_39_7"></a>Ezekiel 39:7

So will I make my [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md) [yada'](../../strongs/h/h3045.md) in the [tavek](../../strongs/h/h8432.md) of my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md); and I will not let them [ḥālal](../../strongs/h/h2490.md) my [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md) any more: and the [gowy](../../strongs/h/h1471.md) shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), the [qadowsh](../../strongs/h/h6918.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_39_8"></a>Ezekiel 39:8

Behold, it is [bow'](../../strongs/h/h935.md), and it is [hayah](../../strongs/h/h1961.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); this is the [yowm](../../strongs/h/h3117.md) whereof I have [dabar](../../strongs/h/h1696.md).

<a name="ezekiel_39_9"></a>Ezekiel 39:9

And they that [yashab](../../strongs/h/h3427.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yisra'el](../../strongs/h/h3478.md) shall [yāṣā'](../../strongs/h/h3318.md), and shall [bāʿar](../../strongs/h/h1197.md) on ['esh](../../strongs/h/h784.md) and [nāśaq](../../strongs/h/h5400.md) the [nešeq](../../strongs/h/h5402.md), both the [magen](../../strongs/h/h4043.md) and the [tsinnah](../../strongs/h/h6793.md), the [qesheth](../../strongs/h/h7198.md) and the [chets](../../strongs/h/h2671.md), and the [yad](../../strongs/h/h3027.md) [maqqēl](../../strongs/h/h4731.md), and the [rōmaḥ](../../strongs/h/h7420.md), and they shall [bāʿar](../../strongs/h/h1197.md) them with ['esh](../../strongs/h/h784.md) seven [šānâ](../../strongs/h/h8141.md):

<a name="ezekiel_39_10"></a>Ezekiel 39:10

So that they shall [nasa'](../../strongs/h/h5375.md) no ['ets](../../strongs/h/h6086.md) out of the [sadeh](../../strongs/h/h7704.md), neither [ḥāṭaḇ](../../strongs/h/h2404.md) any out of the [yaʿar](../../strongs/h/h3293.md); for they shall [bāʿar](../../strongs/h/h1197.md) the [nešeq](../../strongs/h/h5402.md) with ['esh](../../strongs/h/h784.md): and they shall [šālal](../../strongs/h/h7997.md) those that [šālal](../../strongs/h/h7997.md) them, and [bāzaz](../../strongs/h/h962.md) those that [bāzaz](../../strongs/h/h962.md) them, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_39_11"></a>Ezekiel 39:11

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that I will [nathan](../../strongs/h/h5414.md) unto [Gôḡ](../../strongs/h/h1463.md) a [maqowm](../../strongs/h/h4725.md) there of [qeber](../../strongs/h/h6913.md) in [Yisra'el](../../strongs/h/h3478.md), the [gay'](../../strongs/h/h1516.md) of the ['abar](../../strongs/h/h5674.md) on the [qḏmh](../../strongs/h/h6926.md) of the [yam](../../strongs/h/h3220.md): and it shall [ḥāsam](../../strongs/h/h2629.md) the ['abar](../../strongs/h/h5674.md): and there shall they [qāḇar](../../strongs/h/h6912.md) [Gôḡ](../../strongs/h/h1463.md) and all his [hāmôn](../../strongs/h/h1995.md): and they shall [qara'](../../strongs/h/h7121.md) it The [gay'](../../strongs/h/h1516.md) of [Hămôn Gôḡ](../../strongs/h/h1996.md).

<a name="ezekiel_39_12"></a>Ezekiel 39:12

And seven [ḥōḏeš](../../strongs/h/h2320.md) shall the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) be [qāḇar](../../strongs/h/h6912.md) of them, that they may [ṭāhēr](../../strongs/h/h2891.md) the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_39_13"></a>Ezekiel 39:13

Yea, all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) shall [qāḇar](../../strongs/h/h6912.md) them; and it shall be to them a [shem](../../strongs/h/h8034.md) the [yowm](../../strongs/h/h3117.md) that I shall be [kabad](../../strongs/h/h3513.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_39_14"></a>Ezekiel 39:14

And they shall [bāḏal](../../strongs/h/h914.md) ['enowsh](../../strongs/h/h582.md) of [tāmîḏ](../../strongs/h/h8548.md), ['abar](../../strongs/h/h5674.md) the ['erets](../../strongs/h/h776.md) to [qāḇar](../../strongs/h/h6912.md) with the ['abar](../../strongs/h/h5674.md) those that [yāṯar](../../strongs/h/h3498.md) upon the [paniym](../../strongs/h/h6440.md) of the ['erets](../../strongs/h/h776.md), to [ṭāhēr](../../strongs/h/h2891.md) it: after the [qāṣê](../../strongs/h/h7097.md) of seven [ḥōḏeš](../../strongs/h/h2320.md) shall they [chaqar](../../strongs/h/h2713.md).

<a name="ezekiel_39_15"></a>Ezekiel 39:15

And the ['abar](../../strongs/h/h5674.md) that ['abar](../../strongs/h/h5674.md) the ['erets](../../strongs/h/h776.md), when any [ra'ah](../../strongs/h/h7200.md) an ['āḏām](../../strongs/h/h120.md) ['etsem](../../strongs/h/h6106.md), then shall he set [bānâ](../../strongs/h/h1129.md) a [ṣîyûn](../../strongs/h/h6725.md) by it, till the [qāḇar](../../strongs/h/h6912.md) have [qāḇar](../../strongs/h/h6912.md) it in the [gay'](../../strongs/h/h1516.md) of [Hămôn Gôḡ](../../strongs/h/h1996.md).

<a name="ezekiel_39_16"></a>Ezekiel 39:16

And also the [shem](../../strongs/h/h8034.md) of the [ʿîr](../../strongs/h/h5892.md) shall be [Hămônâ](../../strongs/h/h1997.md). Thus shall they [ṭāhēr](../../strongs/h/h2891.md) the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_39_17"></a>Ezekiel 39:17

And, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); ['āmar](../../strongs/h/h559.md) unto every [kanaph](../../strongs/h/h3671.md) [tsippowr](../../strongs/h/h6833.md), and to every [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md), [qāḇaṣ](../../strongs/h/h6908.md) yourselves, and [bow'](../../strongs/h/h935.md); ['āsap̄](../../strongs/h/h622.md) yourselves [cabiyb](../../strongs/h/h5439.md) to my [zebach](../../strongs/h/h2077.md) that I do [zabach](../../strongs/h/h2076.md) for you, even a [gadowl](../../strongs/h/h1419.md) [zebach](../../strongs/h/h2077.md) upon the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md), that ye may ['akal](../../strongs/h/h398.md) [basar](../../strongs/h/h1320.md), and [šāṯâ](../../strongs/h/h8354.md) [dam](../../strongs/h/h1818.md).

<a name="ezekiel_39_18"></a>Ezekiel 39:18

Ye shall ['akal](../../strongs/h/h398.md) the [basar](../../strongs/h/h1320.md) of the [gibôr](../../strongs/h/h1368.md), and [šāṯâ](../../strongs/h/h8354.md) the [dam](../../strongs/h/h1818.md) of the [nāśî'](../../strongs/h/h5387.md) of the ['erets](../../strongs/h/h776.md), of ['ayil](../../strongs/h/h352.md), of [kar](../../strongs/h/h3733.md), and of [ʿatûḏ](../../strongs/h/h6260.md), of [par](../../strongs/h/h6499.md), all of them [mᵊrî'](../../strongs/h/h4806.md) of [Bāšān](../../strongs/h/h1316.md).

<a name="ezekiel_39_19"></a>Ezekiel 39:19

And ye shall ['akal](../../strongs/h/h398.md) [cheleb](../../strongs/h/h2459.md) till ye be [śāḇʿâ](../../strongs/h/h7654.md), and [šāṯâ](../../strongs/h/h8354.md) [dam](../../strongs/h/h1818.md) till ye be [šikārôn](../../strongs/h/h7943.md), of my [zebach](../../strongs/h/h2077.md) which I have [zabach](../../strongs/h/h2076.md) for you.

<a name="ezekiel_39_20"></a>Ezekiel 39:20

Thus ye shall be [sāׂbaʿ](../../strongs/h/h7646.md) at my [šulḥān](../../strongs/h/h7979.md) with [sûs](../../strongs/h/h5483.md) and [reḵeḇ](../../strongs/h/h7393.md), with [gibôr](../../strongs/h/h1368.md), and with all ['iysh](../../strongs/h/h376.md) of [milḥāmâ](../../strongs/h/h4421.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_39_21"></a>Ezekiel 39:21

And I will [nathan](../../strongs/h/h5414.md) my [kabowd](../../strongs/h/h3519.md) among the [gowy](../../strongs/h/h1471.md), and all the [gowy](../../strongs/h/h1471.md) shall [ra'ah](../../strongs/h/h7200.md) my [mishpat](../../strongs/h/h4941.md) that I have ['asah](../../strongs/h/h6213.md), and my [yad](../../strongs/h/h3027.md) that I have [śûm](../../strongs/h/h7760.md) upon them.

<a name="ezekiel_39_22"></a>Ezekiel 39:22

So the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md) from that [yowm](../../strongs/h/h3117.md) and [hāl'â](../../strongs/h/h1973.md).

<a name="ezekiel_39_23"></a>Ezekiel 39:23

And the [gowy](../../strongs/h/h1471.md) shall [yada'](../../strongs/h/h3045.md) that the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) went into [gālâ](../../strongs/h/h1540.md) for their ['avon](../../strongs/h/h5771.md): because they [māʿal](../../strongs/h/h4603.md) against me, therefore [cathar](../../strongs/h/h5641.md) I my [paniym](../../strongs/h/h6440.md) from them, and [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of their [tsar](../../strongs/h/h6862.md): so [naphal](../../strongs/h/h5307.md) they all by the [chereb](../../strongs/h/h2719.md).

<a name="ezekiel_39_24"></a>Ezekiel 39:24

According to their [ṭām'â](../../strongs/h/h2932.md) and according to their [pesha'](../../strongs/h/h6588.md) have I ['asah](../../strongs/h/h6213.md) unto them, and [cathar](../../strongs/h/h5641.md) my [paniym](../../strongs/h/h6440.md) from them.

<a name="ezekiel_39_25"></a>Ezekiel 39:25

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Now will I [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) [shebuwth](../../strongs/h/h7622.md) of [Ya'aqob](../../strongs/h/h3290.md), and have [racham](../../strongs/h/h7355.md) upon the whole [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), and will be [qānā'](../../strongs/h/h7065.md) for my [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md);

<a name="ezekiel_39_26"></a>Ezekiel 39:26

After that they have [nasa'](../../strongs/h/h5375.md) their [kĕlimmah](../../strongs/h/h3639.md), and all their [maʿal](../../strongs/h/h4604.md) whereby they have [māʿal](../../strongs/h/h4603.md) against me, when they [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md) in their ['ăḏāmâ](../../strongs/h/h127.md), and none made them [ḥārēḏ](../../strongs/h/h2729.md).

<a name="ezekiel_39_27"></a>Ezekiel 39:27

When I have [shuwb](../../strongs/h/h7725.md) them from the ['am](../../strongs/h/h5971.md), and [qāḇaṣ](../../strongs/h/h6908.md) them out of their ['oyeb](../../strongs/h/h341.md) ['erets](../../strongs/h/h776.md), and am [qadash](../../strongs/h/h6942.md) in them in the ['ayin](../../strongs/h/h5869.md) of [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md);

<a name="ezekiel_39_28"></a>Ezekiel 39:28

Then shall they [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), which caused them to be led into [gālâ](../../strongs/h/h1540.md) among the [gowy](../../strongs/h/h1471.md): but I have [kānas](../../strongs/h/h3664.md) them unto their own ['ăḏāmâ](../../strongs/h/h127.md), and have [yāṯar](../../strongs/h/h3498.md) none of them any more there.

<a name="ezekiel_39_29"></a>Ezekiel 39:29

Neither will I [cathar](../../strongs/h/h5641.md) my [paniym](../../strongs/h/h6440.md) any more from them: for I have [šāp̄aḵ](../../strongs/h/h8210.md) my [ruwach](../../strongs/h/h7307.md) upon the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 38](ezekiel_38.md) - [Ezekiel 40](ezekiel_40.md)