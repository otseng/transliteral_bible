# [Ezekiel 27](https://www.blueletterbible.org/kjv/ezekiel/27)

<a name="ezekiel_27_1"></a>Ezekiel 27:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came again unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_27_2"></a>Ezekiel 27:2

Now, thou [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nasa'](../../strongs/h/h5375.md) a [qînâ](../../strongs/h/h7015.md) for [Ṣōr](../../strongs/h/h6865.md);

<a name="ezekiel_27_3"></a>Ezekiel 27:3

And ['āmar](../../strongs/h/h559.md) unto [Ṣōr](../../strongs/h/h6865.md), O thou that art [yashab](../../strongs/h/h3427.md) at the [mᵊḇô'â](../../strongs/h/h3997.md) of the [yam](../../strongs/h/h3220.md), which art a [rāḵal](../../strongs/h/h7402.md) of the ['am](../../strongs/h/h5971.md) for [rab](../../strongs/h/h7227.md) ['î](../../strongs/h/h339.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); O [Ṣōr](../../strongs/h/h6865.md), thou hast ['āmar](../../strongs/h/h559.md), I am of [kālîl](../../strongs/h/h3632.md) [yᵊp̄î](../../strongs/h/h3308.md).

<a name="ezekiel_27_4"></a>Ezekiel 27:4

Thy [gᵊḇûl](../../strongs/h/h1366.md) are in the [leb](../../strongs/h/h3820.md) of the [yam](../../strongs/h/h3220.md), thy [bānâ](../../strongs/h/h1129.md) have [kālal](../../strongs/h/h3634.md) thy [yᵊp̄î](../../strongs/h/h3308.md).

<a name="ezekiel_27_5"></a>Ezekiel 27:5

They have [bānâ](../../strongs/h/h1129.md) all thy [lûaḥ](../../strongs/h/h3871.md) of fir [bᵊrôš](../../strongs/h/h1265.md) of [Śᵊnîr](../../strongs/h/h8149.md): they have [laqach](../../strongs/h/h3947.md) ['erez](../../strongs/h/h730.md) from [Lᵊḇānôn](../../strongs/h/h3844.md) to ['asah](../../strongs/h/h6213.md) [tōren](../../strongs/h/h8650.md) for thee.

<a name="ezekiel_27_6"></a>Ezekiel 27:6

Of the ['allôn](../../strongs/h/h437.md) of [Bāšān](../../strongs/h/h1316.md) have they ['asah](../../strongs/h/h6213.md) thine [māšôṭ](../../strongs/h/h4880.md); the [bath](../../strongs/h/h1323.md) of the ['ăšur](../../strongs/h/h839.md) have ['asah](../../strongs/h/h6213.md) thy [qereš](../../strongs/h/h7175.md) of [šēn](../../strongs/h/h8127.md), out of the ['î](../../strongs/h/h339.md) of [Kitîm](../../strongs/h/h3794.md).

<a name="ezekiel_27_7"></a>Ezekiel 27:7

Fine [šēš](../../strongs/h/h8336.md) with [riqmâ](../../strongs/h/h7553.md) from [Mitsrayim](../../strongs/h/h4714.md) was that which thou [mip̄rāś](../../strongs/h/h4666.md) to be thy [nēs](../../strongs/h/h5251.md); [tᵊḵēleṯ](../../strongs/h/h8504.md) and ['argāmān](../../strongs/h/h713.md) from the ['î](../../strongs/h/h339.md) of ['Ĕlîšâ](../../strongs/h/h473.md) was that which [mᵊḵassê](../../strongs/h/h4374.md) thee.

<a name="ezekiel_27_8"></a>Ezekiel 27:8

The [yashab](../../strongs/h/h3427.md) of [Ṣîḏôn](../../strongs/h/h6721.md) and ['Arvaḏ](../../strongs/h/h719.md) were thy [šûṭ](../../strongs/h/h7751.md): thy [ḥāḵām](../../strongs/h/h2450.md) men, O [Ṣōr](../../strongs/h/h6865.md), that were in thee, were thy [ḥōḇēl](../../strongs/h/h2259.md).

<a name="ezekiel_27_9"></a>Ezekiel 27:9

The [zāqēn](../../strongs/h/h2205.md) of [Gᵊḇal](../../strongs/h/h1380.md) and the [ḥāḵām](../../strongs/h/h2450.md) men thereof were in thee thy [beḏeq](../../strongs/h/h919.md) [ḥāzaq](../../strongs/h/h2388.md): all the ['ŏnîyâ](../../strongs/h/h591.md) of the [yam](../../strongs/h/h3220.md) with their [mallāḥ](../../strongs/h/h4419.md) were in thee to [ʿāraḇ](../../strongs/h/h6148.md) thy [maʿărāḇ](../../strongs/h/h4627.md).

<a name="ezekiel_27_10"></a>Ezekiel 27:10

They of [Pāras](../../strongs/h/h6539.md) and of [Lûḏ](../../strongs/h/h3865.md) and of [Pûṭ](../../strongs/h/h6316.md) were in thine [ḥayil](../../strongs/h/h2428.md), thy ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md): they [tālâ](../../strongs/h/h8518.md) the [magen](../../strongs/h/h4043.md) and [kôḇaʿ](../../strongs/h/h3553.md) in thee; they set [nathan](../../strongs/h/h5414.md) thy [hadar](../../strongs/h/h1926.md).

<a name="ezekiel_27_11"></a>Ezekiel 27:11

The [ben](../../strongs/h/h1121.md) of ['Arvaḏ](../../strongs/h/h719.md) with thine [ḥayil](../../strongs/h/h2428.md) were upon thy [ḥômâ](../../strongs/h/h2346.md) [cabiyb](../../strongs/h/h5439.md), and the [gammāḏîm](../../strongs/h/h1575.md) were in thy [miḡdāl](../../strongs/h/h4026.md): they [tālâ](../../strongs/h/h8518.md) their [šeleṭ](../../strongs/h/h7982.md) upon thy [ḥômâ](../../strongs/h/h2346.md) [cabiyb](../../strongs/h/h5439.md); they have [kālal](../../strongs/h/h3634.md) thy [yᵊp̄î](../../strongs/h/h3308.md) [kālal](../../strongs/h/h3634.md).

<a name="ezekiel_27_12"></a>Ezekiel 27:12

[Taršîš](../../strongs/h/h8659.md) was thy [sāḥar](../../strongs/h/h5503.md) by reason of the [rōḇ](../../strongs/h/h7230.md) of all kind of [hôn](../../strongs/h/h1952.md); with [keceph](../../strongs/h/h3701.md), [barzel](../../strongs/h/h1270.md), [bᵊḏîl](../../strongs/h/h913.md), and [ʿōp̄ereṯ](../../strongs/h/h5777.md), they [nathan](../../strongs/h/h5414.md) in thy [ʿizzāḇôn](../../strongs/h/h5801.md).

<a name="ezekiel_27_13"></a>Ezekiel 27:13

[Yāvān](../../strongs/h/h3120.md), [Tuḇal](../../strongs/h/h8422.md), and [Mešeḵ](../../strongs/h/h4902.md), they were thy [rāḵal](../../strongs/h/h7402.md): they [nathan](../../strongs/h/h5414.md) the [nephesh](../../strongs/h/h5315.md) of ['āḏām](../../strongs/h/h120.md) and [kĕliy](../../strongs/h/h3627.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) in thy [maʿărāḇ](../../strongs/h/h4627.md).

<a name="ezekiel_27_14"></a>Ezekiel 27:14

They of the [bayith](../../strongs/h/h1004.md) of [Tōḡarmâ](../../strongs/h/h8425.md) [nathan](../../strongs/h/h5414.md) in thy [ʿizzāḇôn](../../strongs/h/h5801.md) with [sûs](../../strongs/h/h5483.md) and [pārāš](../../strongs/h/h6571.md) and [pereḏ](../../strongs/h/h6505.md).

<a name="ezekiel_27_15"></a>Ezekiel 27:15

The [ben](../../strongs/h/h1121.md) of [Dᵊḏān](../../strongs/h/h1719.md) were thy [rāḵal](../../strongs/h/h7402.md); [rab](../../strongs/h/h7227.md) ['î](../../strongs/h/h339.md) were the [sᵊḥōrâ](../../strongs/h/h5506.md) of thine [yad](../../strongs/h/h3027.md): they [shuwb](../../strongs/h/h7725.md) thee for an ['eškār](../../strongs/h/h814.md) [qeren](../../strongs/h/h7161.md) of [šēn](../../strongs/h/h8127.md) and [hāḇnî](../../strongs/h/h1894.md).

<a name="ezekiel_27_16"></a>Ezekiel 27:16

['Ărām](../../strongs/h/h758.md) was thy [sāḥar](../../strongs/h/h5503.md) by reason of the [rōḇ](../../strongs/h/h7230.md) of the wares of thy [ma'aseh](../../strongs/h/h4639.md): they [nathan](../../strongs/h/h5414.md) in thy [ʿizzāḇôn](../../strongs/h/h5801.md) with [nōp̄eḵ](../../strongs/h/h5306.md), ['argāmān](../../strongs/h/h713.md), and [riqmâ](../../strongs/h/h7553.md), and [bûṣ](../../strongs/h/h948.md), and [rā'mâ](../../strongs/h/h7215.md), and [kaḏkōḏ](../../strongs/h/h3539.md).

<a name="ezekiel_27_17"></a>Ezekiel 27:17

[Yehuwdah](../../strongs/h/h3063.md), and the ['erets](../../strongs/h/h776.md) of [Yisra'el](../../strongs/h/h3478.md), they were thy [rāḵal](../../strongs/h/h7402.md): they [nathan](../../strongs/h/h5414.md) in thy [maʿărāḇ](../../strongs/h/h4627.md) [ḥiṭṭâ](../../strongs/h/h2406.md) of [Minnîṯ](../../strongs/h/h4511.md), and [pannaḡ](../../strongs/h/h6436.md), and [dĕbash](../../strongs/h/h1706.md), and [šemen](../../strongs/h/h8081.md), and [ṣŏrî](../../strongs/h/h6875.md).

<a name="ezekiel_27_18"></a>Ezekiel 27:18

[Dammeśeq](../../strongs/h/h1834.md) was thy [sāḥar](../../strongs/h/h5503.md) in the [rōḇ](../../strongs/h/h7230.md) of the wares of thy [ma'aseh](../../strongs/h/h4639.md), for the [rōḇ](../../strongs/h/h7230.md) of all [hôn](../../strongs/h/h1952.md); in the [yayin](../../strongs/h/h3196.md) of [Ḥelbôn](../../strongs/h/h2463.md), and [ṣaḥar](../../strongs/h/h6713.md) [ṣemer](../../strongs/h/h6785.md).

<a name="ezekiel_27_19"></a>Ezekiel 27:19

[Vᵊḏān](../../strongs/h/h2051.md) [Dān](../../strongs/h/h1835.md) also and [Yāvān](../../strongs/h/h3120.md) going to and ['āzal](../../strongs/h/h235.md) [nathan](../../strongs/h/h5414.md) in thy [ʿizzāḇôn](../../strongs/h/h5801.md): [ʿāšôṯ](../../strongs/h/h6219.md) [barzel](../../strongs/h/h1270.md), [qidâ](../../strongs/h/h6916.md), and [qānê](../../strongs/h/h7070.md), were in thy [maʿărāḇ](../../strongs/h/h4627.md).

<a name="ezekiel_27_20"></a>Ezekiel 27:20

[Dᵊḏān](../../strongs/h/h1719.md) was thy [rāḵal](../../strongs/h/h7402.md) in [ḥōp̄eš](../../strongs/h/h2667.md) [beḡeḏ](../../strongs/h/h899.md) for [riḵbâ](../../strongs/h/h7396.md).

<a name="ezekiel_27_21"></a>Ezekiel 27:21

[ʿĂrāḇ](../../strongs/h/h6152.md), and all the [nāśî'](../../strongs/h/h5387.md) of [Qēḏār](../../strongs/h/h6938.md), they [sāḥar](../../strongs/h/h5503.md) with [yad](../../strongs/h/h3027.md) in [kar](../../strongs/h/h3733.md), and ['ayil](../../strongs/h/h352.md), and [ʿatûḏ](../../strongs/h/h6260.md): in these were they thy [sāḥar](../../strongs/h/h5503.md).

<a name="ezekiel_27_22"></a>Ezekiel 27:22

The [rāḵal](../../strongs/h/h7402.md) of [Šᵊḇā'](../../strongs/h/h7614.md) and [Raʿmâ](../../strongs/h/h7484.md), they were thy [rāḵal](../../strongs/h/h7402.md): they [nathan](../../strongs/h/h5414.md) in thy [ʿizzāḇôn](../../strongs/h/h5801.md) with [ro'sh](../../strongs/h/h7218.md) of all [beśem](../../strongs/h/h1314.md), and with all [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md), and [zāhāḇ](../../strongs/h/h2091.md).

<a name="ezekiel_27_23"></a>Ezekiel 27:23

[Ḥārān](../../strongs/h/h2771.md), and [Kannê](../../strongs/h/h3656.md), and [ʿEḏen](../../strongs/h/h5729.md), the [rāḵal](../../strongs/h/h7402.md) of [Šᵊḇā'](../../strongs/h/h7614.md), ['Aššûr](../../strongs/h/h804.md), and [Kilmaḏ](../../strongs/h/h3638.md), were thy [rāḵal](../../strongs/h/h7402.md).

<a name="ezekiel_27_24"></a>Ezekiel 27:24

These were thy [rāḵal](../../strongs/h/h7402.md) in all [maḵlulîm](../../strongs/h/h4360.md) of things, in [tᵊḵēleṯ](../../strongs/h/h8504.md) [gᵊlôm](../../strongs/h/h1545.md), and [riqmâ](../../strongs/h/h7553.md), and in [gᵊnāzîm](../../strongs/h/h1595.md) of [bᵊrômîm](../../strongs/h/h1264.md), [ḥāḇaš](../../strongs/h/h2280.md) with [chebel](../../strongs/h/h2256.md), and made of ['āraz](../../strongs/h/h729.md), among thy [markōleṯ](../../strongs/h/h4819.md).

<a name="ezekiel_27_25"></a>Ezekiel 27:25

The ['ŏnîyâ](../../strongs/h/h591.md) of [Taršîš](../../strongs/h/h8659.md) did [šûr](../../strongs/h/h7788.md) of thee in thy [maʿărāḇ](../../strongs/h/h4627.md): and thou wast [mālā'](../../strongs/h/h4390.md), and made [me'od](../../strongs/h/h3966.md) [kabad](../../strongs/h/h3513.md) in the [leb](../../strongs/h/h3820.md) of the [yam](../../strongs/h/h3220.md).

<a name="ezekiel_27_26"></a>Ezekiel 27:26

Thy [šûṭ](../../strongs/h/h7751.md) have [bow'](../../strongs/h/h935.md) thee into [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md): the [qāḏîm](../../strongs/h/h6921.md) [ruwach](../../strongs/h/h7307.md) hath [shabar](../../strongs/h/h7665.md) thee in the [leb](../../strongs/h/h3820.md) of the [yam](../../strongs/h/h3220.md).

<a name="ezekiel_27_27"></a>Ezekiel 27:27

Thy [hôn](../../strongs/h/h1952.md), and thy [ʿizzāḇôn](../../strongs/h/h5801.md), thy [maʿărāḇ](../../strongs/h/h4627.md), thy [mallāḥ](../../strongs/h/h4419.md), and thy [ḥōḇēl](../../strongs/h/h2259.md), thy [beḏeq](../../strongs/h/h919.md) [ḥāzaq](../../strongs/h/h2388.md), and the [ʿāraḇ](../../strongs/h/h6148.md) of thy [maʿărāḇ](../../strongs/h/h4627.md), and all thy ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md), that are in thee, and in all thy [qāhēl](../../strongs/h/h6951.md) which is in the midst of thee, shall [naphal](../../strongs/h/h5307.md) into the [leb](../../strongs/h/h3820.md) of the [yam](../../strongs/h/h3220.md) in the [yowm](../../strongs/h/h3117.md) of thy [mapeleṯ](../../strongs/h/h4658.md).

<a name="ezekiel_27_28"></a>Ezekiel 27:28

The [miḡrāš](../../strongs/h/h4054.md) shall [rāʿaš](../../strongs/h/h7493.md) at the [qowl](../../strongs/h/h6963.md) of the [zaʿaq](../../strongs/h/h2201.md) of thy [ḥōḇēl](../../strongs/h/h2259.md).

<a name="ezekiel_27_29"></a>Ezekiel 27:29

And all that [tāp̄aś](../../strongs/h/h8610.md) the [māšôṭ](../../strongs/h/h4880.md), the [mallāḥ](../../strongs/h/h4419.md), and all the [ḥōḇēl](../../strongs/h/h2259.md) of the [yam](../../strongs/h/h3220.md), shall [yarad](../../strongs/h/h3381.md) from their ['ŏnîyâ](../../strongs/h/h591.md), they shall ['amad](../../strongs/h/h5975.md) upon the ['erets](../../strongs/h/h776.md);

<a name="ezekiel_27_30"></a>Ezekiel 27:30

And shall cause their [qowl](../../strongs/h/h6963.md) to be [shama'](../../strongs/h/h8085.md) against thee, and shall [zāʿaq](../../strongs/h/h2199.md) [mar](../../strongs/h/h4751.md), and shall [ʿālâ](../../strongs/h/h5927.md) ['aphar](../../strongs/h/h6083.md) upon their [ro'sh](../../strongs/h/h7218.md), they shall [pālaš](../../strongs/h/h6428.md) themselves in the ['ēp̄er](../../strongs/h/h665.md):

<a name="ezekiel_27_31"></a>Ezekiel 27:31

And they shall make themselves [qrḥ](../../strongs/h/h7144.md) [qāraḥ](../../strongs/h/h7139.md) for thee, and [ḥāḡar](../../strongs/h/h2296.md) them with [śaq](../../strongs/h/h8242.md), and they shall [bāḵâ](../../strongs/h/h1058.md) for thee with [mar](../../strongs/h/h4751.md) of [nephesh](../../strongs/h/h5315.md) and [mar](../../strongs/h/h4751.md) [mispēḏ](../../strongs/h/h4553.md).

<a name="ezekiel_27_32"></a>Ezekiel 27:32

And in their [nî](../../strongs/h/h5204.md) they shall take [nasa'](../../strongs/h/h5375.md) a [qînâ](../../strongs/h/h7015.md) for thee, and [qônēn](../../strongs/h/h6969.md) over thee, what is like [Ṣōr](../../strongs/h/h6865.md), like the [dummâ](../../strongs/h/h1822.md) in the midst of the [yam](../../strongs/h/h3220.md)?

<a name="ezekiel_27_33"></a>Ezekiel 27:33

When thy [ʿizzāḇôn](../../strongs/h/h5801.md) [yāṣā'](../../strongs/h/h3318.md) out of the [yam](../../strongs/h/h3220.md), thou [sāׂbaʿ](../../strongs/h/h7646.md) [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md); thou didst [ʿāšar](../../strongs/h/h6238.md) the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md) with the [rōḇ](../../strongs/h/h7230.md) of thy [hôn](../../strongs/h/h1952.md) and of thy [maʿărāḇ](../../strongs/h/h4627.md).

<a name="ezekiel_27_34"></a>Ezekiel 27:34

In the [ʿēṯ](../../strongs/h/h6256.md) when thou shalt be [shabar](../../strongs/h/h7665.md) by the [yam](../../strongs/h/h3220.md) in the [maʿămaqqîm](../../strongs/h/h4615.md) of the [mayim](../../strongs/h/h4325.md) thy [maʿărāḇ](../../strongs/h/h4627.md) and all thy [qāhēl](../../strongs/h/h6951.md) in the midst of thee shall [naphal](../../strongs/h/h5307.md).

<a name="ezekiel_27_35"></a>Ezekiel 27:35

All the [yashab](../../strongs/h/h3427.md) of the ['î](../../strongs/h/h339.md) shall be [šāmēm](../../strongs/h/h8074.md) at thee, and their [melek](../../strongs/h/h4428.md) shall be [śaʿar](../../strongs/h/h8178.md) [śāʿar](../../strongs/h/h8175.md), they shall be [ra'am](../../strongs/h/h7481.md) in their [paniym](../../strongs/h/h6440.md).

<a name="ezekiel_27_36"></a>Ezekiel 27:36

The [sāḥar](../../strongs/h/h5503.md) among the ['am](../../strongs/h/h5971.md) shall [šāraq](../../strongs/h/h8319.md) at thee; thou shalt be a [ballāhâ](../../strongs/h/h1091.md), and never shalt be any more ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 26](ezekiel_26.md) - [Ezekiel 28](ezekiel_28.md)