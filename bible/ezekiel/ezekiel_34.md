# [Ezekiel 34](https://www.blueletterbible.org/kjv/ezekiel/34)

<a name="ezekiel_34_1"></a>Ezekiel 34:1

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_34_2"></a>Ezekiel 34:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nāḇā'](../../strongs/h/h5012.md) against the [ra'ah](../../strongs/h/h7462.md) of [Yisra'el](../../strongs/h/h3478.md), [nāḇā'](../../strongs/h/h5012.md), and ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) unto the [ra'ah](../../strongs/h/h7462.md); [hôy](../../strongs/h/h1945.md) be to the [ra'ah](../../strongs/h/h7462.md) of [Yisra'el](../../strongs/h/h3478.md) that do [ra'ah](../../strongs/h/h7462.md) themselves! should not the [ra'ah](../../strongs/h/h7462.md) [ra'ah](../../strongs/h/h7462.md) the [tso'n](../../strongs/h/h6629.md)?

<a name="ezekiel_34_3"></a>Ezekiel 34:3

Ye ['akal](../../strongs/h/h398.md) the [cheleb](../../strongs/h/h2459.md), and ye [labash](../../strongs/h/h3847.md) you with the [ṣemer](../../strongs/h/h6785.md), ye [zabach](../../strongs/h/h2076.md) them that are [bārî'](../../strongs/h/h1277.md): but ye [ra'ah](../../strongs/h/h7462.md) not the [tso'n](../../strongs/h/h6629.md).

<a name="ezekiel_34_4"></a>Ezekiel 34:4

The [ḥālâ](../../strongs/h/h2470.md) have ye not [ḥāzaq](../../strongs/h/h2388.md), neither have ye [rapha'](../../strongs/h/h7495.md) that which was [ḥālâ](../../strongs/h/h2470.md), neither have ye [ḥāḇaš](../../strongs/h/h2280.md) that which was [shabar](../../strongs/h/h7665.md), neither have ye [shuwb](../../strongs/h/h7725.md) that which was [nāḏaḥ](../../strongs/h/h5080.md), neither have ye [bāqaš](../../strongs/h/h1245.md) that which was ['abad](../../strongs/h/h6.md); but with [ḥāzqâ](../../strongs/h/h2394.md) and with [pereḵ](../../strongs/h/h6531.md) have ye [radah](../../strongs/h/h7287.md) them.

<a name="ezekiel_34_5"></a>Ezekiel 34:5

And they were [puwts](../../strongs/h/h6327.md), because there is no [ra'ah](../../strongs/h/h7462.md): and they became ['oklah](../../strongs/h/h402.md) to all the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md), when they were [puwts](../../strongs/h/h6327.md).

<a name="ezekiel_34_6"></a>Ezekiel 34:6

My [tso'n](../../strongs/h/h6629.md) [šāḡâ](../../strongs/h/h7686.md) through all the [har](../../strongs/h/h2022.md), and upon every [ruwm](../../strongs/h/h7311.md) [giḇʿâ](../../strongs/h/h1389.md): yea, my [tso'n](../../strongs/h/h6629.md) was [puwts](../../strongs/h/h6327.md) upon all the [paniym](../../strongs/h/h6440.md) of the ['erets](../../strongs/h/h776.md), and none did [darash](../../strongs/h/h1875.md) or [bāqaš](../../strongs/h/h1245.md) after them.

<a name="ezekiel_34_7"></a>Ezekiel 34:7

Therefore, ye [ra'ah](../../strongs/h/h7462.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md);

<a name="ezekiel_34_8"></a>Ezekiel 34:8

As I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), surely because my [tso'n](../../strongs/h/h6629.md) became a [baz](../../strongs/h/h957.md), and my [tso'n](../../strongs/h/h6629.md) became ['oklah](../../strongs/h/h402.md) to every [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md), because there was no [ra'ah](../../strongs/h/h7462.md), neither did my [ra'ah](../../strongs/h/h7462.md) [darash](../../strongs/h/h1875.md) for my [tso'n](../../strongs/h/h6629.md), but the [ra'ah](../../strongs/h/h7462.md) [ra'ah](../../strongs/h/h7462.md) themselves, and [ra'ah](../../strongs/h/h7462.md) not my [tso'n](../../strongs/h/h6629.md);

<a name="ezekiel_34_9"></a>Ezekiel 34:9

Therefore, O ye [ra'ah](../../strongs/h/h7462.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md);

<a name="ezekiel_34_10"></a>Ezekiel 34:10

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I am against the [ra'ah](../../strongs/h/h7462.md); and I will [darash](../../strongs/h/h1875.md) my [tso'n](../../strongs/h/h6629.md) at their [yad](../../strongs/h/h3027.md), and cause them to [shabath](../../strongs/h/h7673.md) from [ra'ah](../../strongs/h/h7462.md) the [tso'n](../../strongs/h/h6629.md); neither shall the [ra'ah](../../strongs/h/h7462.md) [ra'ah](../../strongs/h/h7462.md) themselves any more; for I will [natsal](../../strongs/h/h5337.md) my [tso'n](../../strongs/h/h6629.md) from their [peh](../../strongs/h/h6310.md), that they may not be ['oklah](../../strongs/h/h402.md) for them.

<a name="ezekiel_34_11"></a>Ezekiel 34:11

For thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I, even I, will both [darash](../../strongs/h/h1875.md) my [tso'n](../../strongs/h/h6629.md), and [bāqar](../../strongs/h/h1239.md) them.

<a name="ezekiel_34_12"></a>Ezekiel 34:12

As a [ra'ah](../../strongs/h/h7462.md) [baqqārâ](../../strongs/h/h1243.md) his [ʿēḏer](../../strongs/h/h5739.md) in the [yowm](../../strongs/h/h3117.md) that he is [tavek](../../strongs/h/h8432.md) his [tso'n](../../strongs/h/h6629.md) that are [pāraš](../../strongs/h/h6567.md); so will I [bāqar](../../strongs/h/h1239.md) my [tso'n](../../strongs/h/h6629.md), and will [natsal](../../strongs/h/h5337.md) them out of all [maqowm](../../strongs/h/h4725.md) where they have been [puwts](../../strongs/h/h6327.md) in the [ʿānān](../../strongs/h/h6051.md) and ['araphel](../../strongs/h/h6205.md) [yowm](../../strongs/h/h3117.md).

<a name="ezekiel_34_13"></a>Ezekiel 34:13

And I will [yāṣā'](../../strongs/h/h3318.md) them from the ['am](../../strongs/h/h5971.md), and [qāḇaṣ](../../strongs/h/h6908.md) them from the ['erets](../../strongs/h/h776.md), and will [bow'](../../strongs/h/h935.md) them to their own ['ăḏāmâ](../../strongs/h/h127.md), and [ra'ah](../../strongs/h/h7462.md) them upon the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md) by the ['āp̄îq](../../strongs/h/h650.md), and in all the [môšāḇ](../../strongs/h/h4186.md) of the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_34_14"></a>Ezekiel 34:14

I will [ra'ah](../../strongs/h/h7462.md) them in a [towb](../../strongs/h/h2896.md) [mirʿê](../../strongs/h/h4829.md), and upon the [marowm](../../strongs/h/h4791.md) [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md) shall their [nāvê](../../strongs/h/h5116.md) be: there shall they [rāḇaṣ](../../strongs/h/h7257.md) in a [towb](../../strongs/h/h2896.md) [nāvê](../../strongs/h/h5116.md), and in a [šāmēn](../../strongs/h/h8082.md) [mirʿê](../../strongs/h/h4829.md) shall they [ra'ah](../../strongs/h/h7462.md) upon the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezekiel_34_15"></a>Ezekiel 34:15

I will [ra'ah](../../strongs/h/h7462.md) my [tso'n](../../strongs/h/h6629.md), and I will cause them to [rāḇaṣ](../../strongs/h/h7257.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_34_16"></a>Ezekiel 34:16

I will [bāqaš](../../strongs/h/h1245.md) that which was ['abad](../../strongs/h/h6.md), and [shuwb](../../strongs/h/h7725.md) that which was [nāḏaḥ](../../strongs/h/h5080.md), and will [ḥāḇaš](../../strongs/h/h2280.md) that which was [shabar](../../strongs/h/h7665.md), and will [ḥāzaq](../../strongs/h/h2388.md) that which was [ḥālâ](../../strongs/h/h2470.md): but I will [šāmaḏ](../../strongs/h/h8045.md) the [šāmēn](../../strongs/h/h8082.md) and the [ḥāzāq](../../strongs/h/h2389.md); I will [ra'ah](../../strongs/h/h7462.md) them with [mishpat](../../strongs/h/h4941.md).

<a name="ezekiel_34_17"></a>Ezekiel 34:17

And as for you, O my [tso'n](../../strongs/h/h6629.md), thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I [shaphat](../../strongs/h/h8199.md) between [śê](../../strongs/h/h7716.md) and [śê](../../strongs/h/h7716.md), between the ['ayil](../../strongs/h/h352.md) and the he [ʿatûḏ](../../strongs/h/h6260.md).

<a name="ezekiel_34_18"></a>Ezekiel 34:18

Seemeth it a [mᵊʿaṭ](../../strongs/h/h4592.md) unto you to have [ra'ah](../../strongs/h/h7462.md) the [towb](../../strongs/h/h2896.md) [mirʿê](../../strongs/h/h4829.md), but ye must [rāmas](../../strongs/h/h7429.md) with your [regel](../../strongs/h/h7272.md) the [yeṯer](../../strongs/h/h3499.md) of your [mirʿê](../../strongs/h/h4829.md)? and to have [šāṯâ](../../strongs/h/h8354.md) of the [mišqāʿ](../../strongs/h/h4950.md) [mayim](../../strongs/h/h4325.md), but ye must [rāp̄aś](../../strongs/h/h7515.md) the [yāṯar](../../strongs/h/h3498.md) with your [regel](../../strongs/h/h7272.md)?

<a name="ezekiel_34_19"></a>Ezekiel 34:19

And as for my [tso'n](../../strongs/h/h6629.md), they [ra'ah](../../strongs/h/h7462.md) that which ye have [mirmās](../../strongs/h/h4823.md) with your [regel](../../strongs/h/h7272.md); and they [šāṯâ](../../strongs/h/h8354.md) that which ye have [mirpāś](../../strongs/h/h4833.md) with your [regel](../../strongs/h/h7272.md).

<a name="ezekiel_34_20"></a>Ezekiel 34:20

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) unto them; Behold, I, even I, will [shaphat](../../strongs/h/h8199.md) between the [bᵊrî](../../strongs/h/h1274.md) [śê](../../strongs/h/h7716.md) and between the [rāzê](../../strongs/h/h7330.md) [śê](../../strongs/h/h7716.md).

<a name="ezekiel_34_21"></a>Ezekiel 34:21

Because ye have [hāḏap̄](../../strongs/h/h1920.md) with [ṣaḏ](../../strongs/h/h6654.md) and with [kāṯēp̄](../../strongs/h/h3802.md), and [nāḡaḥ](../../strongs/h/h5055.md) all the [ḥālâ](../../strongs/h/h2470.md) with your [qeren](../../strongs/h/h7161.md), till ye have [puwts](../../strongs/h/h6327.md) them [ḥûṣ](../../strongs/h/h2351.md);

<a name="ezekiel_34_22"></a>Ezekiel 34:22

Therefore will I [yasha'](../../strongs/h/h3467.md) my [tso'n](../../strongs/h/h6629.md), and they shall no more be a [baz](../../strongs/h/h957.md); and I will [shaphat](../../strongs/h/h8199.md) between [śê](../../strongs/h/h7716.md) and [śê](../../strongs/h/h7716.md).

<a name="ezekiel_34_23"></a>Ezekiel 34:23

And I will [quwm](../../strongs/h/h6965.md) one [ra'ah](../../strongs/h/h7462.md) over them, and he shall [ra'ah](../../strongs/h/h7462.md) them, even my ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md); he shall [ra'ah](../../strongs/h/h7462.md) them, and he shall be their [ra'ah](../../strongs/h/h7462.md).

<a name="ezekiel_34_24"></a>Ezekiel 34:24

And I [Yĕhovah](../../strongs/h/h3068.md) will be their ['Elohiym](../../strongs/h/h430.md), and my ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) a [nāśî'](../../strongs/h/h5387.md) [tavek](../../strongs/h/h8432.md) them; I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) it.

<a name="ezekiel_34_25"></a>Ezekiel 34:25

And I will [karath](../../strongs/h/h3772.md) with them a [bĕriyth](../../strongs/h/h1285.md) of [shalowm](../../strongs/h/h7965.md), and will cause the [ra'](../../strongs/h/h7451.md) [chay](../../strongs/h/h2416.md) to [shabath](../../strongs/h/h7673.md) out of the ['erets](../../strongs/h/h776.md): and they shall [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md) in the [midbar](../../strongs/h/h4057.md), and [yashen](../../strongs/h/h3462.md) in the [yaʿar](../../strongs/h/h3293.md) [yᵊʿôrîm](../../strongs/h/h3264.md) .

<a name="ezekiel_34_26"></a>Ezekiel 34:26

And I will [nathan](../../strongs/h/h5414.md) them and the [cabiyb](../../strongs/h/h5439.md) my [giḇʿâ](../../strongs/h/h1389.md) a [bĕrakah](../../strongs/h/h1293.md); and I will cause the [gešem](../../strongs/h/h1653.md) to [yarad](../../strongs/h/h3381.md) in his [ʿēṯ](../../strongs/h/h6256.md); there shall be [gešem](../../strongs/h/h1653.md) of [bĕrakah](../../strongs/h/h1293.md).

<a name="ezekiel_34_27"></a>Ezekiel 34:27

And the ['ets](../../strongs/h/h6086.md) of the [sadeh](../../strongs/h/h7704.md) shall [nathan](../../strongs/h/h5414.md) her [pĕriy](../../strongs/h/h6529.md), and the ['erets](../../strongs/h/h776.md) shall [nathan](../../strongs/h/h5414.md) her [yᵊḇûl](../../strongs/h/h2981.md), and they shall be [betach](../../strongs/h/h983.md) in their ['ăḏāmâ](../../strongs/h/h127.md), and shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when I have [shabar](../../strongs/h/h7665.md) the [môṭâ](../../strongs/h/h4133.md) of their [ʿōl](../../strongs/h/h5923.md), and [natsal](../../strongs/h/h5337.md) them out of the [yad](../../strongs/h/h3027.md) of those that ['abad](../../strongs/h/h5647.md) themselves of them.

<a name="ezekiel_34_28"></a>Ezekiel 34:28

And they shall no more be a [baz](../../strongs/h/h957.md) to the [gowy](../../strongs/h/h1471.md), neither shall the [chay](../../strongs/h/h2416.md) of the ['erets](../../strongs/h/h776.md) ['akal](../../strongs/h/h398.md) them; but they shall [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md), and none shall make them [ḥārēḏ](../../strongs/h/h2729.md).

<a name="ezekiel_34_29"></a>Ezekiel 34:29

And I will [quwm](../../strongs/h/h6965.md) for them a [maṭṭāʿ](../../strongs/h/h4302.md) of [shem](../../strongs/h/h8034.md), and they shall be no more ['āsap̄](../../strongs/h/h622.md) with [rāʿāḇ](../../strongs/h/h7458.md) in the ['erets](../../strongs/h/h776.md), neither [nasa'](../../strongs/h/h5375.md) the [kĕlimmah](../../strongs/h/h3639.md) of the [gowy](../../strongs/h/h1471.md) any more.

<a name="ezekiel_34_30"></a>Ezekiel 34:30

Thus shall they [yada'](../../strongs/h/h3045.md) that I [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md) am with them, and that they, even the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), are my ['am](../../strongs/h/h5971.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_34_31"></a>Ezekiel 34:31

And ye my [tso'n](../../strongs/h/h6629.md), the [tso'n](../../strongs/h/h6629.md) of my [marʿîṯ](../../strongs/h/h4830.md), are ['āḏām](../../strongs/h/h120.md), and I am your ['Elohiym](../../strongs/h/h430.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 33](ezekiel_33.md) - [Ezekiel 35](ezekiel_35.md)