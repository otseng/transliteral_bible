# [Ezekiel 30](https://www.blueletterbible.org/kjv/ezekiel/30)

<a name="ezekiel_30_1"></a>Ezekiel 30:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came again unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_30_2"></a>Ezekiel 30:2

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), [nāḇā'](../../strongs/h/h5012.md) and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); [yālal](../../strongs/h/h3213.md) ye, [hâ](../../strongs/h/h1929.md) the [yowm](../../strongs/h/h3117.md)!

<a name="ezekiel_30_3"></a>Ezekiel 30:3

For the [yowm](../../strongs/h/h3117.md) is [qarowb](../../strongs/h/h7138.md), even the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) is [qarowb](../../strongs/h/h7138.md), a [ʿānān](../../strongs/h/h6051.md) [yowm](../../strongs/h/h3117.md); it shall be the [ʿēṯ](../../strongs/h/h6256.md) of the [gowy](../../strongs/h/h1471.md).

<a name="ezekiel_30_4"></a>Ezekiel 30:4

And the [chereb](../../strongs/h/h2719.md) shall [bow'](../../strongs/h/h935.md) upon [Mitsrayim](../../strongs/h/h4714.md), and [ḥalḥālâ](../../strongs/h/h2479.md) shall be in [Kûš](../../strongs/h/h3568.md), when the [ḥālāl](../../strongs/h/h2491.md) shall [naphal](../../strongs/h/h5307.md) in [Mitsrayim](../../strongs/h/h4714.md), and they shall [laqach](../../strongs/h/h3947.md) her [hāmôn](../../strongs/h/h1995.md), and her [yᵊsôḏ](../../strongs/h/h3247.md) shall be [harac](../../strongs/h/h2040.md).

<a name="ezekiel_30_5"></a>Ezekiel 30:5

[Kûš](../../strongs/h/h3568.md), and [Pûṭ](../../strongs/h/h6316.md), and [Lûḏ](../../strongs/h/h3865.md), and all the [ʿēreḇ](../../strongs/h/h6154.md) people, and [Kûḇ](../../strongs/h/h3552.md), and the [ben](../../strongs/h/h1121.md) of the ['erets](../../strongs/h/h776.md) that is in [bĕriyth](../../strongs/h/h1285.md), shall [naphal](../../strongs/h/h5307.md) with them by the [chereb](../../strongs/h/h2719.md).

<a name="ezekiel_30_6"></a>Ezekiel 30:6

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); They also that [camak](../../strongs/h/h5564.md) [Mitsrayim](../../strongs/h/h4714.md) shall [naphal](../../strongs/h/h5307.md); and the [gā'ôn](../../strongs/h/h1347.md) of her ['oz](../../strongs/h/h5797.md) shall [yarad](../../strongs/h/h3381.md): from the [Miḡdôl](../../strongs/h/h4024.md) of [Sᵊvēnê](../../strongs/h/h5482.md) shall they [naphal](../../strongs/h/h5307.md) in it by the [chereb](../../strongs/h/h2719.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="ezekiel_30_7"></a>Ezekiel 30:7

And they shall be [šāmēm](../../strongs/h/h8074.md) in the midst of the ['erets](../../strongs/h/h776.md) that are [šāmēm](../../strongs/h/h8074.md), and her [ʿîr](../../strongs/h/h5892.md) shall be in the midst of the [ʿîr](../../strongs/h/h5892.md) that are [ḥāraḇ](../../strongs/h/h2717.md).

<a name="ezekiel_30_8"></a>Ezekiel 30:8

And they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when I have [nathan](../../strongs/h/h5414.md) an ['esh](../../strongs/h/h784.md) in [Mitsrayim](../../strongs/h/h4714.md), and when all her [ʿāzar](../../strongs/h/h5826.md) shall be [shabar](../../strongs/h/h7665.md).

<a name="ezekiel_30_9"></a>Ezekiel 30:9

In that [yowm](../../strongs/h/h3117.md) shall [mal'ak](../../strongs/h/h4397.md) [yāṣā'](../../strongs/h/h3318.md) from [paniym](../../strongs/h/h6440.md) in [ṣî](../../strongs/h/h6716.md) to make the [betach](../../strongs/h/h983.md) [Kûš](../../strongs/h/h3568.md) [ḥārēḏ](../../strongs/h/h2729.md), and [ḥalḥālâ](../../strongs/h/h2479.md) shall come upon them, as in the [yowm](../../strongs/h/h3117.md) of [Mitsrayim](../../strongs/h/h4714.md): for, lo, it [bow'](../../strongs/h/h935.md).

<a name="ezekiel_30_10"></a>Ezekiel 30:10

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); I will also make the [hāmôn](../../strongs/h/h1995.md) of [Mitsrayim](../../strongs/h/h4714.md) to [shabath](../../strongs/h/h7673.md) by the [yad](../../strongs/h/h3027.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md).

<a name="ezekiel_30_11"></a>Ezekiel 30:11

He and his ['am](../../strongs/h/h5971.md) with him, the [ʿārîṣ](../../strongs/h/h6184.md) of the [gowy](../../strongs/h/h1471.md), shall be [bow'](../../strongs/h/h935.md) to [shachath](../../strongs/h/h7843.md) the ['erets](../../strongs/h/h776.md): and they shall [rîq](../../strongs/h/h7324.md) their [chereb](../../strongs/h/h2719.md) against [Mitsrayim](../../strongs/h/h4714.md), and [mālā'](../../strongs/h/h4390.md) the ['erets](../../strongs/h/h776.md) with the [ḥālāl](../../strongs/h/h2491.md).

<a name="ezekiel_30_12"></a>Ezekiel 30:12

And I will [nathan](../../strongs/h/h5414.md) the [yᵊ'ōr](../../strongs/h/h2975.md) [ḥārāḇâ](../../strongs/h/h2724.md), and [māḵar](../../strongs/h/h4376.md) the ['erets](../../strongs/h/h776.md) into the [yad](../../strongs/h/h3027.md) of the [ra'](../../strongs/h/h7451.md): and I will make the ['erets](../../strongs/h/h776.md) [šāmēm](../../strongs/h/h8074.md), and all that is [mᵊlō'](../../strongs/h/h4393.md), by the [yad](../../strongs/h/h3027.md) of [zûr](../../strongs/h/h2114.md): I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md) it.

<a name="ezekiel_30_13"></a>Ezekiel 30:13

Thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); I will also ['abad](../../strongs/h/h6.md) the [gillûl](../../strongs/h/h1544.md), and I will cause their ['ĕlîl](../../strongs/h/h457.md) to [shabath](../../strongs/h/h7673.md) out of [Nōp̄](../../strongs/h/h5297.md); and there shall be no more a [nāśî'](../../strongs/h/h5387.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): and I will [nathan](../../strongs/h/h5414.md) a [yir'ah](../../strongs/h/h3374.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="ezekiel_30_14"></a>Ezekiel 30:14

And I will make [Paṯrôs](../../strongs/h/h6624.md) [šāmēm](../../strongs/h/h8074.md), and will [nathan](../../strongs/h/h5414.md) ['esh](../../strongs/h/h784.md) in [Ṣōʿan](../../strongs/h/h6814.md), and will ['asah](../../strongs/h/h6213.md) [šep̄eṭ](../../strongs/h/h8201.md) in [Nō'](../../strongs/h/h4996.md).

<a name="ezekiel_30_15"></a>Ezekiel 30:15

And I will [šāp̄aḵ](../../strongs/h/h8210.md) my [chemah](../../strongs/h/h2534.md) upon [Sîn](../../strongs/h/h5512.md), the [māʿôz](../../strongs/h/h4581.md) of [Mitsrayim](../../strongs/h/h4714.md); and I will [karath](../../strongs/h/h3772.md) the [hāmôn](../../strongs/h/h1995.md) of [Nō'](../../strongs/h/h4996.md).

<a name="ezekiel_30_16"></a>Ezekiel 30:16

And I will [nathan](../../strongs/h/h5414.md) ['esh](../../strongs/h/h784.md) in [Mitsrayim](../../strongs/h/h4714.md): [Sîn](../../strongs/h/h5512.md) shall have [chuwl](../../strongs/h/h2342.md) [chuwl](../../strongs/h/h2342.md), and [Nō'](../../strongs/h/h4996.md) shall be rent [bāqaʿ](../../strongs/h/h1234.md), and [Nōp̄](../../strongs/h/h5297.md) shall have [tsar](../../strongs/h/h6862.md) [yômām](../../strongs/h/h3119.md).

<a name="ezekiel_30_17"></a>Ezekiel 30:17

The [bāḥûr](../../strongs/h/h970.md) of ['Āven](../../strongs/h/h206.md) and of [Pî-Ḇeseṯ](../../strongs/h/h6364.md) shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md): and shall [yālaḵ](../../strongs/h/h3212.md) into [šᵊḇî](../../strongs/h/h7628.md).

<a name="ezekiel_30_18"></a>Ezekiel 30:18

At [Tᵊḥap̄Nᵊḥēs](../../strongs/h/h8471.md) also the [yowm](../../strongs/h/h3117.md) shall be [ḥāšaḵ](../../strongs/h/h2821.md) [ḥāśaḵ](../../strongs/h/h2820.md), when I shall [shabar](../../strongs/h/h7665.md) there the [môṭâ](../../strongs/h/h4133.md) of [Mitsrayim](../../strongs/h/h4714.md): and the [gā'ôn](../../strongs/h/h1347.md) of her ['oz](../../strongs/h/h5797.md) shall [shabath](../../strongs/h/h7673.md) in her: as for her, a [ʿānān](../../strongs/h/h6051.md) shall [kāsâ](../../strongs/h/h3680.md) her, and her [bath](../../strongs/h/h1323.md) shall [yālaḵ](../../strongs/h/h3212.md) into [šᵊḇî](../../strongs/h/h7628.md).

<a name="ezekiel_30_19"></a>Ezekiel 30:19

Thus will I ['asah](../../strongs/h/h6213.md) [šep̄eṭ](../../strongs/h/h8201.md) in [Mitsrayim](../../strongs/h/h4714.md): and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezekiel_30_20"></a>Ezekiel 30:20

And it came to pass in the eleventh [šānâ](../../strongs/h/h8141.md), in the [ri'šôn](../../strongs/h/h7223.md) month, in the seventh day of the [ḥōḏeš](../../strongs/h/h2320.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="ezekiel_30_21"></a>Ezekiel 30:21

[ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), I have [shabar](../../strongs/h/h7665.md) the [zerowa'](../../strongs/h/h2220.md) of [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md); and, lo, it shall not be bound [ḥāḇaš](../../strongs/h/h2280.md) to be [rp̄v'h](../../strongs/h/h7499.md) [nathan](../../strongs/h/h5414.md), to [śûm](../../strongs/h/h7760.md) a [ḥitûl](../../strongs/h/h2848.md) to [ḥāḇaš](../../strongs/h/h2280.md) it, to make it [ḥāzaq](../../strongs/h/h2388.md) to [tāp̄aś](../../strongs/h/h8610.md) the [chereb](../../strongs/h/h2719.md).

<a name="ezekiel_30_22"></a>Ezekiel 30:22

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, I am against [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), and will [shabar](../../strongs/h/h7665.md) his [zerowa'](../../strongs/h/h2220.md), the [ḥāzāq](../../strongs/h/h2389.md), and that which was [shabar](../../strongs/h/h7665.md); and I will cause the [chereb](../../strongs/h/h2719.md) to [naphal](../../strongs/h/h5307.md) out of his [yad](../../strongs/h/h3027.md).

<a name="ezekiel_30_23"></a>Ezekiel 30:23

And I will [puwts](../../strongs/h/h6327.md) the [Mitsrayim](../../strongs/h/h4714.md) among the [gowy](../../strongs/h/h1471.md), and will [zārâ](../../strongs/h/h2219.md) them through the ['erets](../../strongs/h/h776.md).

<a name="ezekiel_30_24"></a>Ezekiel 30:24

And I will [ḥāzaq](../../strongs/h/h2388.md) the [zerowa'](../../strongs/h/h2220.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and [nathan](../../strongs/h/h5414.md) my [chereb](../../strongs/h/h2719.md) in his [yad](../../strongs/h/h3027.md): but I will [shabar](../../strongs/h/h7665.md) [Parʿô](../../strongs/h/h6547.md) [zerowa'](../../strongs/h/h2220.md), and he shall [nā'aq](../../strongs/h/h5008.md) [paniym](../../strongs/h/h6440.md) him with the [nᵊ'āqâ](../../strongs/h/h5009.md) of a [ḥālāl](../../strongs/h/h2491.md).

<a name="ezekiel_30_25"></a>Ezekiel 30:25

But I will [ḥāzaq](../../strongs/h/h2388.md) the [zerowa'](../../strongs/h/h2220.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and the [zerowa'](../../strongs/h/h2220.md) of [Parʿô](../../strongs/h/h6547.md) shall [naphal](../../strongs/h/h5307.md); and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when I shall [nathan](../../strongs/h/h5414.md) my [chereb](../../strongs/h/h2719.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and he shall [natah](../../strongs/h/h5186.md) it upon the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="ezekiel_30_26"></a>Ezekiel 30:26

And I will [puwts](../../strongs/h/h6327.md) the [Mitsrayim](../../strongs/h/h4714.md) among the [gowy](../../strongs/h/h1471.md), and [zārâ](../../strongs/h/h2219.md) them among the ['erets](../../strongs/h/h776.md); and they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Ezekiel](ezekiel.md)

[Ezekiel 29](ezekiel_29.md) - [Ezekiel 31](ezekiel_31.md)