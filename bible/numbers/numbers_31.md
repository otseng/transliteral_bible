# [Numbers 31](https://www.blueletterbible.org/kjv/num/31)

<a name="numbers_31_1"></a>Numbers 31:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_31_2"></a>Numbers 31:2

[naqam](../../strongs/h/h5358.md) [nᵊqāmâ](../../strongs/h/h5360.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) of the [Miḏyānî](../../strongs/h/h4084.md): ['aḥar](../../strongs/h/h310.md) shalt thou be ['āsap̄](../../strongs/h/h622.md) unto thy ['am](../../strongs/h/h5971.md).

<a name="numbers_31_3"></a>Numbers 31:3

And [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) unto the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), [chalats](../../strongs/h/h2502.md) ['enowsh](../../strongs/h/h582.md) of yourselves unto the [tsaba'](../../strongs/h/h6635.md), and let them go against the [Miḏyān](../../strongs/h/h4080.md), and [nathan](../../strongs/h/h5414.md) [nᵊqāmâ](../../strongs/h/h5360.md) [Yĕhovah](../../strongs/h/h3068.md) of [Miḏyān](../../strongs/h/h4080.md).

<a name="numbers_31_4"></a>Numbers 31:4

Of [maṭṭê](../../strongs/h/h4294.md) [maṭṭê](../../strongs/h/h4294.md) a thousand, throughout all the [maṭṭê](../../strongs/h/h4294.md) of [Yisra'el](../../strongs/h/h3478.md), shall ye [shalach](../../strongs/h/h7971.md) to the [tsaba'](../../strongs/h/h6635.md).

<a name="numbers_31_5"></a>Numbers 31:5

So there were [māsar](../../strongs/h/h4560.md) out of the thousands of [Yisra'el](../../strongs/h/h3478.md), a thousand of every [maṭṭê](../../strongs/h/h4294.md), twelve thousand [chalats](../../strongs/h/h2502.md) for [tsaba'](../../strongs/h/h6635.md).

<a name="numbers_31_6"></a>Numbers 31:6

And [Mōshe](../../strongs/h/h4872.md) [shalach](../../strongs/h/h7971.md) them to the [tsaba'](../../strongs/h/h6635.md), a thousand of every [maṭṭê](../../strongs/h/h4294.md), them and [Pînḥās](../../strongs/h/h6372.md) the [ben](../../strongs/h/h1121.md) of ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), to the [tsaba'](../../strongs/h/h6635.md), with the [qodesh](../../strongs/h/h6944.md) [kĕliy](../../strongs/h/h3627.md), and the [ḥăṣōṣrâ](../../strongs/h/h2689.md) to [tᵊrûʿâ](../../strongs/h/h8643.md) in his [yad](../../strongs/h/h3027.md).

<a name="numbers_31_7"></a>Numbers 31:7

And they [ṣᵊḇā'](../../strongs/h/h6633.md) against the [Miḏyān](../../strongs/h/h4080.md), as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md); and they [harag](../../strongs/h/h2026.md) all the [zāḵār](../../strongs/h/h2145.md).

<a name="numbers_31_8"></a>Numbers 31:8

And they [harag](../../strongs/h/h2026.md) the [melek](../../strongs/h/h4428.md) of [Miḏyān](../../strongs/h/h4080.md), beside the rest of them that were [ḥālāl](../../strongs/h/h2491.md); namely, ['Ĕvî](../../strongs/h/h189.md), and [Reqem](../../strongs/h/h7552.md), and [Ṣaûār](../../strongs/h/h6698.md), and [Ḥûr](../../strongs/h/h2354.md), and [Reḇaʿ](../../strongs/h/h7254.md), five [melek](../../strongs/h/h4428.md) of [Miḏyān](../../strongs/h/h4080.md): [Bilʿām](../../strongs/h/h1109.md) also the [ben](../../strongs/h/h1121.md) of [Bᵊʿôr](../../strongs/h/h1160.md) they [harag](../../strongs/h/h2026.md) with the [chereb](../../strongs/h/h2719.md).

<a name="numbers_31_9"></a>Numbers 31:9

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) took all the ['ishshah](../../strongs/h/h802.md) of [Miḏyān](../../strongs/h/h4080.md) [šāḇâ](../../strongs/h/h7617.md), and their [ṭap̄](../../strongs/h/h2945.md), and took the [bāzaz](../../strongs/h/h962.md) of all their [bĕhemah](../../strongs/h/h929.md), and all their [miqnê](../../strongs/h/h4735.md), and all their [ḥayil](../../strongs/h/h2428.md).

<a name="numbers_31_10"></a>Numbers 31:10

And they [śārap̄](../../strongs/h/h8313.md) all their [ʿîr](../../strongs/h/h5892.md) wherein they [môšāḇ](../../strongs/h/h4186.md), and all their [ṭîrâ](../../strongs/h/h2918.md), with ['esh](../../strongs/h/h784.md).

<a name="numbers_31_11"></a>Numbers 31:11

And they [laqach](../../strongs/h/h3947.md) all the [šālāl](../../strongs/h/h7998.md), and all the [malqôaḥ](../../strongs/h/h4455.md), both of ['adam](../../strongs/h/h120.md) and of [bĕhemah](../../strongs/h/h929.md).

<a name="numbers_31_12"></a>Numbers 31:12

And they [bow'](../../strongs/h/h935.md) the [šᵊḇî](../../strongs/h/h7628.md), and the [malqôaḥ](../../strongs/h/h4455.md), and the [šālāl](../../strongs/h/h7998.md), unto [Mōshe](../../strongs/h/h4872.md), and ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and unto the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), unto the [maḥănê](../../strongs/h/h4264.md) at the ['arabah](../../strongs/h/h6160.md) of [Mô'āḇ](../../strongs/h/h4124.md), which are by [Yardēn](../../strongs/h/h3383.md) near [Yᵊrēḥô](../../strongs/h/h3405.md).

<a name="numbers_31_13"></a>Numbers 31:13

And [Mōshe](../../strongs/h/h4872.md), and ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and all the [nāśî'](../../strongs/h/h5387.md) of the ['edah](../../strongs/h/h5712.md), [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) them [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md).

<a name="numbers_31_14"></a>Numbers 31:14

And [Mōshe](../../strongs/h/h4872.md) was [qāṣap̄](../../strongs/h/h7107.md) with the [paqad](../../strongs/h/h6485.md) of the [ḥayil](../../strongs/h/h2428.md), with the [śar](../../strongs/h/h8269.md) over thousands, and [śar](../../strongs/h/h8269.md) over hundreds, which [bow'](../../strongs/h/h935.md) [tsaba'](../../strongs/h/h6635.md) the [milḥāmâ](../../strongs/h/h4421.md).

<a name="numbers_31_15"></a>Numbers 31:15

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto them, Have ye [ḥāyâ](../../strongs/h/h2421.md) all the [nᵊqēḇâ](../../strongs/h/h5347.md)?

<a name="numbers_31_16"></a>Numbers 31:16

Behold, these caused the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), through the [dabar](../../strongs/h/h1697.md) of [Bilʿām](../../strongs/h/h1109.md), to [māsar](../../strongs/h/h4560.md) [maʿal](../../strongs/h/h4604.md) against [Yĕhovah](../../strongs/h/h3068.md) in the [dabar](../../strongs/h/h1697.md) of [P̄ᵊʿôr](../../strongs/h/h6465.md), and there was a [magēp̄â](../../strongs/h/h4046.md) among the ['edah](../../strongs/h/h5712.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_31_17"></a>Numbers 31:17

Now therefore [harag](../../strongs/h/h2026.md) every [zāḵār](../../strongs/h/h2145.md) among the [ṭap̄](../../strongs/h/h2945.md), and [harag](../../strongs/h/h2026.md) every ['ishshah](../../strongs/h/h802.md) that hath [yada'](../../strongs/h/h3045.md) ['iysh](../../strongs/h/h376.md) by [miškāḇ](../../strongs/h/h4904.md) with him.

<a name="numbers_31_18"></a>Numbers 31:18

But all the ['ishshah](../../strongs/h/h802.md) [ṭap̄](../../strongs/h/h2945.md), that have not [yada'](../../strongs/h/h3045.md) a [zāḵār](../../strongs/h/h2145.md) by [miškāḇ](../../strongs/h/h4904.md) with him, [ḥāyâ](../../strongs/h/h2421.md) for yourselves.

<a name="numbers_31_19"></a>Numbers 31:19

And do ye [ḥānâ](../../strongs/h/h2583.md) [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md) seven [yowm](../../strongs/h/h3117.md): whosoever hath [harag](../../strongs/h/h2026.md) any [nephesh](../../strongs/h/h5315.md), and whosoever hath [naga'](../../strongs/h/h5060.md) any [ḥālāl](../../strongs/h/h2491.md), [chata'](../../strongs/h/h2398.md) both yourselves and your [šᵊḇî](../../strongs/h/h7628.md) on the third [yowm](../../strongs/h/h3117.md), and on the seventh [yowm](../../strongs/h/h3117.md).

<a name="numbers_31_20"></a>Numbers 31:20

And [chata'](../../strongs/h/h2398.md) all your [beḡeḏ](../../strongs/h/h899.md), and all that is [kĕliy](../../strongs/h/h3627.md) of ['owr](../../strongs/h/h5785.md), and all [ma'aseh](../../strongs/h/h4639.md) of [ʿēz](../../strongs/h/h5795.md), and all things [kĕliy](../../strongs/h/h3627.md) of ['ets](../../strongs/h/h6086.md).

<a name="numbers_31_21"></a>Numbers 31:21

And ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md) ['āmar](../../strongs/h/h559.md) unto the ['enowsh](../../strongs/h/h582.md) of [tsaba'](../../strongs/h/h6635.md) which [bow'](../../strongs/h/h935.md) to the [milḥāmâ](../../strongs/h/h4421.md), This is the [chuqqah](../../strongs/h/h2708.md) of the [towrah](../../strongs/h/h8451.md) which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md);

<a name="numbers_31_22"></a>Numbers 31:22

Only the [zāhāḇ](../../strongs/h/h2091.md), and the [keceph](../../strongs/h/h3701.md), the [nᵊḥšeṯ](../../strongs/h/h5178.md), the [barzel](../../strongs/h/h1270.md), the [bᵊḏîl](../../strongs/h/h913.md), and the [ʿōp̄ereṯ](../../strongs/h/h5777.md),

<a name="numbers_31_23"></a>Numbers 31:23

Every [dabar](../../strongs/h/h1697.md) that may [bow'](../../strongs/h/h935.md) the ['esh](../../strongs/h/h784.md), ye shall ['abar](../../strongs/h/h5674.md) it through the ['esh](../../strongs/h/h784.md), and it shall be [ṭāhēr](../../strongs/h/h2891.md): nevertheless it shall be [chata'](../../strongs/h/h2398.md) with the [mayim](../../strongs/h/h4325.md) of [nidâ](../../strongs/h/h5079.md): and all that [bow'](../../strongs/h/h935.md) not the ['esh](../../strongs/h/h784.md) ye shall ['abar](../../strongs/h/h5674.md) through the [mayim](../../strongs/h/h4325.md).

<a name="numbers_31_24"></a>Numbers 31:24

And ye shall [kāḇas](../../strongs/h/h3526.md) your [beḡeḏ](../../strongs/h/h899.md) on the seventh [yowm](../../strongs/h/h3117.md), and ye shall be [ṭāhēr](../../strongs/h/h2891.md), and ['aḥar](../../strongs/h/h310.md) ye shall [bow'](../../strongs/h/h935.md) into the [maḥănê](../../strongs/h/h4264.md).

<a name="numbers_31_25"></a>Numbers 31:25

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_31_26"></a>Numbers 31:26

[nasa'](../../strongs/h/h5375.md) the [ro'sh](../../strongs/h/h7218.md) of the [malqôaḥ](../../strongs/h/h4455.md) that was [šᵊḇî](../../strongs/h/h7628.md), both of ['adam](../../strongs/h/h120.md) and of [bĕhemah](../../strongs/h/h929.md), thou, and ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and the [ro'sh](../../strongs/h/h7218.md) ['ab](../../strongs/h/h1.md) of the ['edah](../../strongs/h/h5712.md):

<a name="numbers_31_27"></a>Numbers 31:27

And [ḥāṣâ](../../strongs/h/h2673.md) the [malqôaḥ](../../strongs/h/h4455.md); between them that [tāp̄aś](../../strongs/h/h8610.md) the [milḥāmâ](../../strongs/h/h4421.md) upon them, who [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md), and between all the ['edah](../../strongs/h/h5712.md):

<a name="numbers_31_28"></a>Numbers 31:28

And [ruwm](../../strongs/h/h7311.md) a [meḵes](../../strongs/h/h4371.md) unto [Yĕhovah](../../strongs/h/h3068.md) of the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md) which [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md): one [nephesh](../../strongs/h/h5315.md) of five hundred, both of the ['āḏām](../../strongs/h/h120.md), and of the [bāqār](../../strongs/h/h1241.md), and of the [chamowr](../../strongs/h/h2543.md), and of the [tso'n](../../strongs/h/h6629.md):

<a name="numbers_31_29"></a>Numbers 31:29

[laqach](../../strongs/h/h3947.md) it of their [maḥăṣîṯ](../../strongs/h/h4276.md), and [nathan](../../strongs/h/h5414.md) it unto ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), for a [tᵊrûmâ](../../strongs/h/h8641.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_31_30"></a>Numbers 31:30

And of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [maḥăṣîṯ](../../strongs/h/h4276.md), thou shalt [laqach](../../strongs/h/h3947.md) one ['āḥaz](../../strongs/h/h270.md) of fifty, of the ['āḏām](../../strongs/h/h120.md), of the [bāqār](../../strongs/h/h1241.md), of the [chamowr](../../strongs/h/h2543.md), and of the [tso'n](../../strongs/h/h6629.md), of all manner of [bĕhemah](../../strongs/h/h929.md), and [nathan](../../strongs/h/h5414.md) them unto the [Lᵊvî](../../strongs/h/h3881.md), which [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of the [miškān](../../strongs/h/h4908.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_31_31"></a>Numbers 31:31

And [Mōshe](../../strongs/h/h4872.md) and ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md) ['asah](../../strongs/h/h6213.md) as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="numbers_31_32"></a>Numbers 31:32

And the [malqôaḥ](../../strongs/h/h4455.md), being the [yeṯer](../../strongs/h/h3499.md) of the [baz](../../strongs/h/h957.md) which the ['am](../../strongs/h/h5971.md) of [tsaba'](../../strongs/h/h6635.md) had [bāzaz](../../strongs/h/h962.md), was six hundred thousand and seventy thousand and five thousand [tso'n](../../strongs/h/h6629.md),

<a name="numbers_31_33"></a>Numbers 31:33

And threescore and twelve thousand [bāqār](../../strongs/h/h1241.md),

<a name="numbers_31_34"></a>Numbers 31:34

And threescore and one thousand [chamowr](../../strongs/h/h2543.md),

<a name="numbers_31_35"></a>Numbers 31:35

And thirty and two thousand [nephesh](../../strongs/h/h5315.md) ['āḏām](../../strongs/h/h120.md) in all, of ['ishshah](../../strongs/h/h802.md) that had not [yada'](../../strongs/h/h3045.md) [zāḵār](../../strongs/h/h2145.md) by [miškāḇ](../../strongs/h/h4904.md) with him.

<a name="numbers_31_36"></a>Numbers 31:36

And the [meḥĕṣâ](../../strongs/h/h4275.md), which was the [cheleq](../../strongs/h/h2506.md) of them that [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md), was in [mispār](../../strongs/h/h4557.md) three hundred thousand and seven and thirty thousand and five hundred [tso'n](../../strongs/h/h6629.md):

<a name="numbers_31_37"></a>Numbers 31:37

And [Yĕhovah](../../strongs/h/h3068.md) [meḵes](../../strongs/h/h4371.md) of the [tso'n](../../strongs/h/h6629.md) was six hundred and threescore and fifteen.

<a name="numbers_31_38"></a>Numbers 31:38

And the [bāqār](../../strongs/h/h1241.md) were thirty and six thousand; of which [Yĕhovah](../../strongs/h/h3068.md) [meḵes](../../strongs/h/h4371.md) was threescore and twelve.

<a name="numbers_31_39"></a>Numbers 31:39

And the [chamowr](../../strongs/h/h2543.md) were thirty thousand and five hundred; of which [Yĕhovah](../../strongs/h/h3068.md) [meḵes](../../strongs/h/h4371.md) was threescore and one.

<a name="numbers_31_40"></a>Numbers 31:40

And the ['āḏām](../../strongs/h/h120.md) [nephesh](../../strongs/h/h5315.md) were sixteen thousand; of which [Yĕhovah](../../strongs/h/h3068.md) [meḵes](../../strongs/h/h4371.md) was thirty and two [nephesh](../../strongs/h/h5315.md).

<a name="numbers_31_41"></a>Numbers 31:41

And [Mōshe](../../strongs/h/h4872.md) [nathan](../../strongs/h/h5414.md) the [meḵes](../../strongs/h/h4371.md), which was [Yĕhovah](../../strongs/h/h3068.md) [tᵊrûmâ](../../strongs/h/h8641.md), unto ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="numbers_31_42"></a>Numbers 31:42

And of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [maḥăṣîṯ](../../strongs/h/h4276.md), which [Mōshe](../../strongs/h/h4872.md) [ḥāṣâ](../../strongs/h/h2673.md) from the ['enowsh](../../strongs/h/h582.md) that [ṣᵊḇā'](../../strongs/h/h6633.md),

<a name="numbers_31_43"></a>Numbers 31:43

(Now the [meḥĕṣâ](../../strongs/h/h4275.md) that pertained unto the ['edah](../../strongs/h/h5712.md) was three hundred thousand and thirty thousand and seven thousand and five hundred [tso'n](../../strongs/h/h6629.md),

<a name="numbers_31_44"></a>Numbers 31:44

And thirty and six thousand [bāqār](../../strongs/h/h1241.md),

<a name="numbers_31_45"></a>Numbers 31:45

And thirty thousand [chamowr](../../strongs/h/h2543.md) and five hundred,

<a name="numbers_31_46"></a>Numbers 31:46

And sixteen thousand [nephesh](../../strongs/h/h5315.md) ['āḏām](../../strongs/h/h120.md);)

<a name="numbers_31_47"></a>Numbers 31:47

Even of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [maḥăṣîṯ](../../strongs/h/h4276.md), [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) one ['āḥaz](../../strongs/h/h270.md) of fifty, both of ['adam](../../strongs/h/h120.md) and of [bĕhemah](../../strongs/h/h929.md), and [nathan](../../strongs/h/h5414.md) them unto the [Lᵊvî](../../strongs/h/h3881.md), which [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of the [miškān](../../strongs/h/h4908.md) of [Yĕhovah](../../strongs/h/h3068.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="numbers_31_48"></a>Numbers 31:48

And the [paqad](../../strongs/h/h6485.md) which were over thousands of the [tsaba'](../../strongs/h/h6635.md), the [śar](../../strongs/h/h8269.md) of thousands, and [śar](../../strongs/h/h8269.md) of hundreds, [qāraḇ](../../strongs/h/h7126.md) unto [Mōshe](../../strongs/h/h4872.md):

<a name="numbers_31_49"></a>Numbers 31:49

And they ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), Thy ['ebed](../../strongs/h/h5650.md) have [nasa'](../../strongs/h/h5375.md)n the [ro'sh](../../strongs/h/h7218.md) of the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md) which are under our [yad](../../strongs/h/h3027.md), and there [paqad](../../strongs/h/h6485.md) not one ['iysh](../../strongs/h/h376.md) of us.

<a name="numbers_31_50"></a>Numbers 31:50

We have therefore [qāraḇ](../../strongs/h/h7126.md) a [qorban](../../strongs/h/h7133.md) for [Yĕhovah](../../strongs/h/h3068.md), what every ['iysh](../../strongs/h/h376.md) hath [māṣā'](../../strongs/h/h4672.md), of [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md), ['eṣʿāḏâ](../../strongs/h/h685.md), and [ṣāmîḏ](../../strongs/h/h6781.md), [ṭabaʿaṯ](../../strongs/h/h2885.md), [ʿāḡîl](../../strongs/h/h5694.md), and [kûmāz](../../strongs/h/h3558.md), to make a [kāp̄ar](../../strongs/h/h3722.md) for our [nephesh](../../strongs/h/h5315.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_31_51"></a>Numbers 31:51

And [Mōshe](../../strongs/h/h4872.md) and ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md) [laqach](../../strongs/h/h3947.md) the [zāhāḇ](../../strongs/h/h2091.md) of them, even all [ma'aseh](../../strongs/h/h4639.md) [kĕliy](../../strongs/h/h3627.md).

<a name="numbers_31_52"></a>Numbers 31:52

And all the [zāhāḇ](../../strongs/h/h2091.md) of the [tᵊrûmâ](../../strongs/h/h8641.md) that they [ruwm](../../strongs/h/h7311.md) to [Yĕhovah](../../strongs/h/h3068.md), of the [śar](../../strongs/h/h8269.md) of thousands, and of the [śar](../../strongs/h/h8269.md) of hundreds, was sixteen thousand seven hundred and fifty [šeqel](../../strongs/h/h8255.md).

<a name="numbers_31_53"></a>Numbers 31:53

(For the ['enowsh](../../strongs/h/h582.md) of [tsaba'](../../strongs/h/h6635.md) had [bāzaz](../../strongs/h/h962.md), every ['iysh](../../strongs/h/h376.md) for himself.)

<a name="numbers_31_54"></a>Numbers 31:54

And [Mōshe](../../strongs/h/h4872.md) and ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md) [laqach](../../strongs/h/h3947.md) the [zāhāḇ](../../strongs/h/h2091.md) of the [śar](../../strongs/h/h8269.md) of thousands and of hundreds, and [bow'](../../strongs/h/h935.md) it into the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), for a [zikārôn](../../strongs/h/h2146.md) for the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 30](numbers_30.md) - [Numbers 32](numbers_32.md)