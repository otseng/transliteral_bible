# [Numbers 16](https://www.blueletterbible.org/kjv/num/16)

<a name="numbers_16_1"></a>Numbers 16:1

Now [Qōraḥ](../../strongs/h/h7141.md), the [ben](../../strongs/h/h1121.md) of [Yiṣhār](../../strongs/h/h3324.md), the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md), the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md), and [Dāṯān](../../strongs/h/h1885.md) and ['Ăḇîrām](../../strongs/h/h48.md), the [ben](../../strongs/h/h1121.md) of ['Ĕlî'āḇ](../../strongs/h/h446.md), and ['ôn](../../strongs/h/h203.md), the [ben](../../strongs/h/h1121.md) of [peleṯ](../../strongs/h/h6431.md), [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), [laqach](../../strongs/h/h3947.md) men:

<a name="numbers_16_2"></a>Numbers 16:2

And they [quwm](../../strongs/h/h6965.md) [paniym](../../strongs/h/h6440.md) [Mōshe](../../strongs/h/h4872.md), with ['enowsh](../../strongs/h/h582.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), two hundred and fifty [nāśî'](../../strongs/h/h5387.md) of the ['edah](../../strongs/h/h5712.md), [qārî'](../../strongs/h/h7148.md) in the [môʿēḏ](../../strongs/h/h4150.md), ['enowsh](../../strongs/h/h582.md) of [shem](../../strongs/h/h8034.md):

<a name="numbers_16_3"></a>Numbers 16:3

And they [qāhal](../../strongs/h/h6950.md) themselves against [Mōshe](../../strongs/h/h4872.md) and against ['Ahărôn](../../strongs/h/h175.md), and ['āmar](../../strongs/h/h559.md) unto them, Ye [rab](../../strongs/h/h7227.md) upon you, seeing all the ['edah](../../strongs/h/h5712.md) are [qadowsh](../../strongs/h/h6918.md), every one of them, and [Yĕhovah](../../strongs/h/h3068.md) is [tavek](../../strongs/h/h8432.md) them: wherefore then [nasa'](../../strongs/h/h5375.md) ye yourselves above the [qāhēl](../../strongs/h/h6951.md) of [Yĕhovah](../../strongs/h/h3068.md)?

<a name="numbers_16_4"></a>Numbers 16:4

And when [Mōshe](../../strongs/h/h4872.md) [shama'](../../strongs/h/h8085.md) it, he [naphal](../../strongs/h/h5307.md) upon his [paniym](../../strongs/h/h6440.md):

<a name="numbers_16_5"></a>Numbers 16:5

And he [dabar](../../strongs/h/h1696.md) unto [Qōraḥ](../../strongs/h/h7141.md) and unto all his ['edah](../../strongs/h/h5712.md), ['āmar](../../strongs/h/h559.md), Even [boqer](../../strongs/h/h1242.md) [Yĕhovah](../../strongs/h/h3068.md) will [yada'](../../strongs/h/h3045.md) who are his, and who is [qadowsh](../../strongs/h/h6918.md); and will cause him to [qāraḇ](../../strongs/h/h7126.md) unto him: even him whom he hath [bāḥar](../../strongs/h/h977.md) will he cause to [qāraḇ](../../strongs/h/h7126.md) unto him.

<a name="numbers_16_6"></a>Numbers 16:6

This ['asah](../../strongs/h/h6213.md); [laqach](../../strongs/h/h3947.md) you [maḥtâ](../../strongs/h/h4289.md), [Qōraḥ](../../strongs/h/h7141.md), and all his ['edah](../../strongs/h/h5712.md);

<a name="numbers_16_7"></a>Numbers 16:7

And [nathan](../../strongs/h/h5414.md) ['esh](../../strongs/h/h784.md) therein, and [śûm](../../strongs/h/h7760.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) in them [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) [māḥār](../../strongs/h/h4279.md): and it shall be that the ['iysh](../../strongs/h/h376.md) whom [Yĕhovah](../../strongs/h/h3068.md) doth [bāḥar](../../strongs/h/h977.md), he shall be [qadowsh](../../strongs/h/h6918.md): ye take [rab](../../strongs/h/h7227.md) upon you, ye [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md).

<a name="numbers_16_8"></a>Numbers 16:8

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto [Qōraḥ](../../strongs/h/h7141.md), [shama'](../../strongs/h/h8085.md), I pray you, ye [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md):

<a name="numbers_16_9"></a>Numbers 16:9

Seemeth it but a [mᵊʿaṭ](../../strongs/h/h4592.md) unto you, that the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) hath [bāḏal](../../strongs/h/h914.md) you from the ['edah](../../strongs/h/h5712.md) of [Yisra'el](../../strongs/h/h3478.md), to [qāraḇ](../../strongs/h/h7126.md) you to himself to ['abad](../../strongs/h/h5647.md) the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [miškān](../../strongs/h/h4908.md) of [Yĕhovah](../../strongs/h/h3068.md), and to ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the ['edah](../../strongs/h/h5712.md) to [sharath](../../strongs/h/h8334.md) unto them?

<a name="numbers_16_10"></a>Numbers 16:10

And he hath [qāraḇ](../../strongs/h/h7126.md) thee to him, and all thy ['ach](../../strongs/h/h251.md) the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md) with thee: and [bāqaš](../../strongs/h/h1245.md) ye the [kᵊhunnâ](../../strongs/h/h3550.md) also?

<a name="numbers_16_11"></a>Numbers 16:11

For which [kēn](../../strongs/h/h3651.md) both thou and all thy ['edah](../../strongs/h/h5712.md) are [yāʿaḏ](../../strongs/h/h3259.md) against [Yĕhovah](../../strongs/h/h3068.md): and what is ['Ahărôn](../../strongs/h/h175.md), that ye [lûn](../../strongs/h/h3885.md) [lûn](../../strongs/h/h3885.md) against him?

<a name="numbers_16_12"></a>Numbers 16:12

And [Mōshe](../../strongs/h/h4872.md) [shalach](../../strongs/h/h7971.md) to [qara'](../../strongs/h/h7121.md) [Dāṯān](../../strongs/h/h1885.md) and ['Ăḇîrām](../../strongs/h/h48.md), the [ben](../../strongs/h/h1121.md) of ['Ĕlî'āḇ](../../strongs/h/h446.md): which ['āmar](../../strongs/h/h559.md), We will not [ʿālâ](../../strongs/h/h5927.md):

<a name="numbers_16_13"></a>Numbers 16:13

Is it a [mᵊʿaṭ](../../strongs/h/h4592.md) that thou hast [ʿālâ](../../strongs/h/h5927.md) us up out of an ['erets](../../strongs/h/h776.md) that [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md), to [muwth](../../strongs/h/h4191.md) us in the [midbar](../../strongs/h/h4057.md), except thou [śārar](../../strongs/h/h8323.md) thyself over us?

<a name="numbers_16_14"></a>Numbers 16:14

Moreover thou hast not [bow'](../../strongs/h/h935.md) us into an ['erets](../../strongs/h/h776.md) that [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md), or [nathan](../../strongs/h/h5414.md) us [nachalah](../../strongs/h/h5159.md) of [sadeh](../../strongs/h/h7704.md) and [kerem](../../strongs/h/h3754.md): wilt thou [nāqar](../../strongs/h/h5365.md) the ['ayin](../../strongs/h/h5869.md) of these ['enowsh](../../strongs/h/h582.md)? we will not [ʿālâ](../../strongs/h/h5927.md).

<a name="numbers_16_15"></a>Numbers 16:15

And [Mōshe](../../strongs/h/h4872.md) was [me'od](../../strongs/h/h3966.md) [ḥārâ](../../strongs/h/h2734.md), and ['āmar](../../strongs/h/h559.md) unto [Yĕhovah](../../strongs/h/h3068.md), [panah](../../strongs/h/h6437.md) not thou their [minchah](../../strongs/h/h4503.md): I have not [nasa'](../../strongs/h/h5375.md) one [chamowr](../../strongs/h/h2543.md) from them, neither have I [ra'a'](../../strongs/h/h7489.md) one of them.

<a name="numbers_16_16"></a>Numbers 16:16

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto [Qōraḥ](../../strongs/h/h7141.md), Be thou and all thy ['edah](../../strongs/h/h5712.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), thou, and they, and ['Ahărôn](../../strongs/h/h175.md), [māḥār](../../strongs/h/h4279.md):

<a name="numbers_16_17"></a>Numbers 16:17

And [laqach](../../strongs/h/h3947.md) every ['iysh](../../strongs/h/h376.md) his [maḥtâ](../../strongs/h/h4289.md), and [nathan](../../strongs/h/h5414.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) in them, and [qāraḇ](../../strongs/h/h7126.md) ye [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) every ['iysh](../../strongs/h/h376.md) his [maḥtâ](../../strongs/h/h4289.md), two hundred and fifty [maḥtâ](../../strongs/h/h4289.md); thou also, and ['Ahărôn](../../strongs/h/h175.md), ['iysh](../../strongs/h/h376.md) of you his [maḥtâ](../../strongs/h/h4289.md).

<a name="numbers_16_18"></a>Numbers 16:18

And they [laqach](../../strongs/h/h3947.md) every ['iysh](../../strongs/h/h376.md) his [maḥtâ](../../strongs/h/h4289.md), and [nathan](../../strongs/h/h5414.md) ['esh](../../strongs/h/h784.md) in them, and [śûm](../../strongs/h/h7760.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) thereon, and ['amad](../../strongs/h/h5975.md) in the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) with [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md).

<a name="numbers_16_19"></a>Numbers 16:19

And [Qōraḥ](../../strongs/h/h7141.md) [qāhal](../../strongs/h/h6950.md) all the ['edah](../../strongs/h/h5712.md) against them unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md): and the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) unto all the ['edah](../../strongs/h/h5712.md).

<a name="numbers_16_20"></a>Numbers 16:20

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) and unto ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_16_21"></a>Numbers 16:21

[bāḏal](../../strongs/h/h914.md) yourselves from [tavek](../../strongs/h/h8432.md) this ['edah](../../strongs/h/h5712.md), that I may [kalah](../../strongs/h/h3615.md) them in a [reḡaʿ](../../strongs/h/h7281.md).

<a name="numbers_16_22"></a>Numbers 16:22

And they [naphal](../../strongs/h/h5307.md) upon their [paniym](../../strongs/h/h6440.md), and ['āmar](../../strongs/h/h559.md), ['el](../../strongs/h/h410.md), the ['Elohiym](../../strongs/h/h430.md) of the [ruwach](../../strongs/h/h7307.md) of all [basar](../../strongs/h/h1320.md), shall one ['iysh](../../strongs/h/h376.md) [chata'](../../strongs/h/h2398.md), and wilt thou be [qāṣap̄](../../strongs/h/h7107.md) with all the ['edah](../../strongs/h/h5712.md)?

<a name="numbers_16_23"></a>Numbers 16:23

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_16_24"></a>Numbers 16:24

[dabar](../../strongs/h/h1696.md) unto the ['edah](../../strongs/h/h5712.md), ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md) you from [cabiyb](../../strongs/h/h5439.md) the [miškān](../../strongs/h/h4908.md) of [Qōraḥ](../../strongs/h/h7141.md), [Dāṯān](../../strongs/h/h1885.md), and ['Ăḇîrām](../../strongs/h/h48.md).

<a name="numbers_16_25"></a>Numbers 16:25

And [Mōshe](../../strongs/h/h4872.md) [quwm](../../strongs/h/h6965.md) and [yālaḵ](../../strongs/h/h3212.md) unto [Dāṯān](../../strongs/h/h1885.md) and ['Ăḇîrām](../../strongs/h/h48.md); and the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md) [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md).

<a name="numbers_16_26"></a>Numbers 16:26

And he [dabar](../../strongs/h/h1696.md) unto the ['edah](../../strongs/h/h5712.md), ['āmar](../../strongs/h/h559.md), [cuwr](../../strongs/h/h5493.md), I pray you, from the ['ohel](../../strongs/h/h168.md) of these [rasha'](../../strongs/h/h7563.md) ['enowsh](../../strongs/h/h582.md), and [naga'](../../strongs/h/h5060.md) nothing of their's, lest ye be [sāp̄â](../../strongs/h/h5595.md) in all their [chatta'ath](../../strongs/h/h2403.md).

<a name="numbers_16_27"></a>Numbers 16:27

So they [ʿālâ](../../strongs/h/h5927.md) from the [miškān](../../strongs/h/h4908.md) of [Qōraḥ](../../strongs/h/h7141.md), [Dāṯān](../../strongs/h/h1885.md), and ['Ăḇîrām](../../strongs/h/h48.md), on [cabiyb](../../strongs/h/h5439.md): and [Dāṯān](../../strongs/h/h1885.md) and ['Ăḇîrām](../../strongs/h/h48.md) [yāṣā'](../../strongs/h/h3318.md), and [nāṣaḇ](../../strongs/h/h5324.md) in the [peṯaḥ](../../strongs/h/h6607.md) of their ['ohel](../../strongs/h/h168.md), and their ['ishshah](../../strongs/h/h802.md), and their [ben](../../strongs/h/h1121.md), and their [ṭap̄](../../strongs/h/h2945.md).

<a name="numbers_16_28"></a>Numbers 16:28

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), Hereby ye shall [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) hath [shalach](../../strongs/h/h7971.md) me to ['asah](../../strongs/h/h6213.md) all these [ma'aseh](../../strongs/h/h4639.md); for I have not done them of mine own [leb](../../strongs/h/h3820.md).

<a name="numbers_16_29"></a>Numbers 16:29

If these [muwth](../../strongs/h/h4191.md) the [maveth](../../strongs/h/h4194.md) of all ['adam](../../strongs/h/h120.md), or if they be [paqad](../../strongs/h/h6485.md) after the [pᵊqudâ](../../strongs/h/h6486.md) of all ['adam](../../strongs/h/h120.md); then [Yĕhovah](../../strongs/h/h3068.md) hath not [shalach](../../strongs/h/h7971.md) me.

<a name="numbers_16_30"></a>Numbers 16:30

But if [Yĕhovah](../../strongs/h/h3068.md) [bara'](../../strongs/h/h1254.md) a [bᵊrî'â](../../strongs/h/h1278.md), and the ['ăḏāmâ](../../strongs/h/h127.md) [pāṣâ](../../strongs/h/h6475.md) her [peh](../../strongs/h/h6310.md), and [bālaʿ](../../strongs/h/h1104.md) them, with all that appertain unto them, and they [yarad](../../strongs/h/h3381.md) [chay](../../strongs/h/h2416.md) into [shĕ'owl](../../strongs/h/h7585.md); then ye shall [yada'](../../strongs/h/h3045.md) that these ['enowsh](../../strongs/h/h582.md) have [na'ats](../../strongs/h/h5006.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_16_31"></a>Numbers 16:31

And it came to pass, as he had made a [kalah](../../strongs/h/h3615.md) of [dabar](../../strongs/h/h1696.md) all these [dabar](../../strongs/h/h1697.md), that the ['adamah](../../strongs/h/h127.md) [bāqaʿ](../../strongs/h/h1234.md) that was under them:

<a name="numbers_16_32"></a>Numbers 16:32

And the ['erets](../../strongs/h/h776.md) [pāṯaḥ](../../strongs/h/h6605.md) her [peh](../../strongs/h/h6310.md), and [bālaʿ](../../strongs/h/h1104.md) them, and their [bayith](../../strongs/h/h1004.md), and all the ['adam](../../strongs/h/h120.md) that appertained unto [Qōraḥ](../../strongs/h/h7141.md), and all their [rᵊḵûš](../../strongs/h/h7399.md).

<a name="numbers_16_33"></a>Numbers 16:33

They, and all that appertained to them, [yarad](../../strongs/h/h3381.md) [chay](../../strongs/h/h2416.md) into [shĕ'owl](../../strongs/h/h7585.md), and the ['erets](../../strongs/h/h776.md) [kāsâ](../../strongs/h/h3680.md) upon them: and they ['abad](../../strongs/h/h6.md) from [tavek](../../strongs/h/h8432.md) the [qāhēl](../../strongs/h/h6951.md).

<a name="numbers_16_34"></a>Numbers 16:34

And all [Yisra'el](../../strongs/h/h3478.md) that were [cabiyb](../../strongs/h/h5439.md) them [nûs](../../strongs/h/h5127.md) at the [qowl](../../strongs/h/h6963.md) of them: for they ['āmar](../../strongs/h/h559.md), Lest the ['erets](../../strongs/h/h776.md) [bālaʿ](../../strongs/h/h1104.md) us also.

<a name="numbers_16_35"></a>Numbers 16:35

And there [yāṣā'](../../strongs/h/h3318.md) an ['esh](../../strongs/h/h784.md) from [Yĕhovah](../../strongs/h/h3068.md), and ['akal](../../strongs/h/h398.md) the two hundred and fifty ['iysh](../../strongs/h/h376.md) that [qāraḇ](../../strongs/h/h7126.md) [qᵊṭōreṯ](../../strongs/h/h7004.md).

<a name="numbers_16_36"></a>Numbers 16:36

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_16_37"></a>Numbers 16:37

['āmar](../../strongs/h/h559.md) unto ['Elʿāzār](../../strongs/h/h499.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md), that he [ruwm](../../strongs/h/h7311.md) the [maḥtâ](../../strongs/h/h4289.md) [bayin](../../strongs/h/h996.md) of the [śᵊrēp̄â](../../strongs/h/h8316.md), and [zārâ](../../strongs/h/h2219.md) thou the ['esh](../../strongs/h/h784.md) [hāl'â](../../strongs/h/h1973.md); for they are [qadash](../../strongs/h/h6942.md).

<a name="numbers_16_38"></a>Numbers 16:38

The [maḥtâ](../../strongs/h/h4289.md) of these [chatta'](../../strongs/h/h2400.md) against their own [nephesh](../../strongs/h/h5315.md), let them ['asah](../../strongs/h/h6213.md) them [rqʿ](../../strongs/h/h7555.md) [paḥ](../../strongs/h/h6341.md) for a [ṣipûy](../../strongs/h/h6826.md) of the [mizbeach](../../strongs/h/h4196.md): for they [qāraḇ](../../strongs/h/h7126.md) them [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), therefore they are [qadash](../../strongs/h/h6942.md): and they shall be an ['ôṯ](../../strongs/h/h226.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_16_39"></a>Numbers 16:39

And ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md) [laqach](../../strongs/h/h3947.md) the [nᵊḥšeṯ](../../strongs/h/h5178.md) [maḥtâ](../../strongs/h/h4289.md), wherewith they that were [śārap̄](../../strongs/h/h8313.md) had [qāraḇ](../../strongs/h/h7126.md); and they were [rāqaʿ](../../strongs/h/h7554.md) for a [ṣipûy](../../strongs/h/h6826.md) of the [mizbeach](../../strongs/h/h4196.md):

<a name="numbers_16_40"></a>Numbers 16:40

To be a [zikārôn](../../strongs/h/h2146.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that no ['iysh](../../strongs/h/h376.md) [zûr](../../strongs/h/h2114.md), which is not of the [zera'](../../strongs/h/h2233.md) of ['Ahărôn](../../strongs/h/h175.md), [qāraḇ](../../strongs/h/h7126.md) to [qāṭar](../../strongs/h/h6999.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); that he be not as [Qōraḥ](../../strongs/h/h7141.md), and as his ['edah](../../strongs/h/h5712.md): as [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) to him by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

<a name="numbers_16_41"></a>Numbers 16:41

But on the [māḥŏrāṯ](../../strongs/h/h4283.md) all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [lûn](../../strongs/h/h3885.md) against [Mōshe](../../strongs/h/h4872.md) and against ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md), Ye have [muwth](../../strongs/h/h4191.md) the ['am](../../strongs/h/h5971.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_16_42"></a>Numbers 16:42

And it came to pass, when the ['edah](../../strongs/h/h5712.md) was [qāhal](../../strongs/h/h6950.md) against [Mōshe](../../strongs/h/h4872.md) and against ['Ahărôn](../../strongs/h/h175.md), that they [panah](../../strongs/h/h6437.md) toward the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md): and, behold, the [ʿānān](../../strongs/h/h6051.md) [kāsâ](../../strongs/h/h3680.md) it, and the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md).

<a name="numbers_16_43"></a>Numbers 16:43

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="numbers_16_44"></a>Numbers 16:44

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_16_45"></a>Numbers 16:45

[rāmam](../../strongs/h/h7426.md) you from [tavek](../../strongs/h/h8432.md) this ['edah](../../strongs/h/h5712.md), that I may [kalah](../../strongs/h/h3615.md) them as in a [reḡaʿ](../../strongs/h/h7281.md). And they [naphal](../../strongs/h/h5307.md) upon their [paniym](../../strongs/h/h6440.md).

<a name="numbers_16_46"></a>Numbers 16:46

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), [laqach](../../strongs/h/h3947.md) a [maḥtâ](../../strongs/h/h4289.md), and [nathan](../../strongs/h/h5414.md) ['esh](../../strongs/h/h784.md) therein from [paniym](../../strongs/h/h6440.md) the [mizbeach](../../strongs/h/h4196.md), and [śûm](../../strongs/h/h7760.md) on [qᵊṭōreṯ](../../strongs/h/h7004.md), and [yālaḵ](../../strongs/h/h3212.md) [mᵊhērâ](../../strongs/h/h4120.md) unto the ['edah](../../strongs/h/h5712.md), and make a [kāp̄ar](../../strongs/h/h3722.md) for them: for there is [qeṣep̄](../../strongs/h/h7110.md) [yāṣā'](../../strongs/h/h3318.md) from [Yĕhovah](../../strongs/h/h3068.md); the [neḡep̄](../../strongs/h/h5063.md) is [ḥālal](../../strongs/h/h2490.md).

<a name="numbers_16_47"></a>Numbers 16:47

And ['Ahărôn](../../strongs/h/h175.md) [laqach](../../strongs/h/h3947.md) as [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md), and [rûṣ](../../strongs/h/h7323.md) into the [tavek](../../strongs/h/h8432.md) of the [qāhēl](../../strongs/h/h6951.md); and, behold, the [neḡep̄](../../strongs/h/h5063.md) was [ḥālal](../../strongs/h/h2490.md) among the ['am](../../strongs/h/h5971.md): and he [nathan](../../strongs/h/h5414.md) on [qᵊṭōreṯ](../../strongs/h/h7004.md), and made a [kāp̄ar](../../strongs/h/h3722.md) for the ['am](../../strongs/h/h5971.md).

<a name="numbers_16_48"></a>Numbers 16:48

And he ['amad](../../strongs/h/h5975.md) between the [muwth](../../strongs/h/h4191.md) and the [chay](../../strongs/h/h2416.md); and the [magēp̄â](../../strongs/h/h4046.md) was [ʿāṣar](../../strongs/h/h6113.md).

<a name="numbers_16_49"></a>Numbers 16:49

Now they that [muwth](../../strongs/h/h4191.md) in the [magēp̄â](../../strongs/h/h4046.md) were fourteen thousand and seven hundred, beside them that [muwth](../../strongs/h/h4191.md) about the [dabar](../../strongs/h/h1697.md) of [Qōraḥ](../../strongs/h/h7141.md).

<a name="numbers_16_50"></a>Numbers 16:50

And ['Ahărôn](../../strongs/h/h175.md) [shuwb](../../strongs/h/h7725.md) unto [Mōshe](../../strongs/h/h4872.md) unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md): and the [magēp̄â](../../strongs/h/h4046.md) was [ʿāṣar](../../strongs/h/h6113.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 15](numbers_15.md) - [Numbers 17](numbers_17.md)