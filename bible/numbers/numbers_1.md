# [Numbers 1](https://www.blueletterbible.org/kjv/num/1)

<a name="numbers_1_1"></a>Numbers 1:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) in the [midbar](../../strongs/h/h4057.md) of [Sînay](../../strongs/h/h5514.md), in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), on the first of the second [ḥōḏeš](../../strongs/h/h2320.md), in the second [šānâ](../../strongs/h/h8141.md) after they were [yāṣā'](../../strongs/h/h3318.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_1_2"></a>Numbers 1:2

[nasa'](../../strongs/h/h5375.md) ye the [ro'sh](../../strongs/h/h7218.md) of all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), with the [mispār](../../strongs/h/h4557.md) of their [shem](../../strongs/h/h8034.md), every [zāḵār](../../strongs/h/h2145.md) by their [gulgōleṯ](../../strongs/h/h1538.md);

<a name="numbers_1_3"></a>Numbers 1:3

From twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that are able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md) in [Yisra'el](../../strongs/h/h3478.md): thou and ['Ahărôn](../../strongs/h/h175.md) shall [paqad](../../strongs/h/h6485.md) them by their [tsaba'](../../strongs/h/h6635.md).

<a name="numbers_1_4"></a>Numbers 1:4

And with you there shall be an ['iysh](../../strongs/h/h376.md) of ['iysh](../../strongs/h/h376.md) [maṭṭê](../../strongs/h/h4294.md); every one [ro'sh](../../strongs/h/h7218.md) of the [bayith](../../strongs/h/h1004.md) of his ['ab](../../strongs/h/h1.md).

<a name="numbers_1_5"></a>Numbers 1:5

And these are the [shem](../../strongs/h/h8034.md) of the ['enowsh](../../strongs/h/h582.md) that shall ['amad](../../strongs/h/h5975.md) with you: of [Rᵊ'ûḇēn](../../strongs/h/h7205.md); ['ĕlîṣûr](../../strongs/h/h468.md) the [ben](../../strongs/h/h1121.md) of [šᵊḏê'ûr](../../strongs/h/h7707.md).

<a name="numbers_1_6"></a>Numbers 1:6

Of [Šimʿôn](../../strongs/h/h8095.md); [šᵊlumî'ēl](../../strongs/h/h8017.md) the [ben](../../strongs/h/h1121.md) of [ṣûrîšaday](../../strongs/h/h6701.md).

<a name="numbers_1_7"></a>Numbers 1:7

Of [Yehuwdah](../../strongs/h/h3063.md); [Naḥšôn](../../strongs/h/h5177.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmînāḏāḇ](../../strongs/h/h5992.md).

<a name="numbers_1_8"></a>Numbers 1:8

Of [Yiśśāśḵār](../../strongs/h/h3485.md); [Nᵊṯan'ēl](../../strongs/h/h5417.md) the [ben](../../strongs/h/h1121.md) of [ṣûʿar](../../strongs/h/h6686.md).

<a name="numbers_1_9"></a>Numbers 1:9

Of [Zᵊḇûlûn](../../strongs/h/h2074.md); ['Ĕlî'āḇ](../../strongs/h/h446.md) the [ben](../../strongs/h/h1121.md) of [ḥēlōn](../../strongs/h/h2497.md).

<a name="numbers_1_10"></a>Numbers 1:10

Of the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md): of ['Ep̄rayim](../../strongs/h/h669.md); ['Ĕlîšāmāʿ](../../strongs/h/h476.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmîhûḏ](../../strongs/h/h5989.md): of [Mᵊnaššê](../../strongs/h/h4519.md); [gamlî'ēl](../../strongs/h/h1583.md) the [ben](../../strongs/h/h1121.md) of [pᵊḏâṣûr](../../strongs/h/h6301.md).

<a name="numbers_1_11"></a>Numbers 1:11

Of [Binyāmîn](../../strongs/h/h1144.md); ['ăḇîḏān](../../strongs/h/h27.md) the [ben](../../strongs/h/h1121.md) of [giḏʿōnî](../../strongs/h/h1441.md).

<a name="numbers_1_12"></a>Numbers 1:12

Of [Dān](../../strongs/h/h1835.md); ['ăḥîʿezer](../../strongs/h/h295.md) the [ben](../../strongs/h/h1121.md) of [ʿammîšaḏay](../../strongs/h/h5996.md).

<a name="numbers_1_13"></a>Numbers 1:13

Of ['Āšēr](../../strongs/h/h836.md); [Paḡʿî'ēl](../../strongs/h/h6295.md) the [ben](../../strongs/h/h1121.md) of [ʿāḵrān](../../strongs/h/h5918.md).

<a name="numbers_1_14"></a>Numbers 1:14

Of [Gāḏ](../../strongs/h/h1410.md); ['elyāsāp̄](../../strongs/h/h460.md) the [ben](../../strongs/h/h1121.md) of [dᵊʿû'ēl](../../strongs/h/h1845.md).

<a name="numbers_1_15"></a>Numbers 1:15

Of [Nap̄tālî](../../strongs/h/h5321.md); ['ăḥîraʿ](../../strongs/h/h299.md) the [ben](../../strongs/h/h1121.md) of [ʿênān](../../strongs/h/h5881.md).

<a name="numbers_1_16"></a>Numbers 1:16

These [qārî'](../../strongs/h/h7148.md) the [qara'](../../strongs/h/h7121.md) of the ['edah](../../strongs/h/h5712.md), [nāśî'](../../strongs/h/h5387.md) of the [maṭṭê](../../strongs/h/h4294.md) of their ['ab](../../strongs/h/h1.md), [ro'sh](../../strongs/h/h7218.md) of thousands in [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_1_17"></a>Numbers 1:17

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [laqach](../../strongs/h/h3947.md) these ['enowsh](../../strongs/h/h582.md) which are [nāqaḇ](../../strongs/h/h5344.md) by their [shem](../../strongs/h/h8034.md):

<a name="numbers_1_18"></a>Numbers 1:18

And they [qāhal](../../strongs/h/h6950.md) all the ['edah](../../strongs/h/h5712.md) on the first of the second [ḥōḏeš](../../strongs/h/h2320.md), and they declared their [yalad](../../strongs/h/h3205.md) after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), according to the [mispār](../../strongs/h/h4557.md) of the [shem](../../strongs/h/h8034.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), by their [gulgōleṯ](../../strongs/h/h1538.md).

<a name="numbers_1_19"></a>Numbers 1:19

As [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md), so he [paqad](../../strongs/h/h6485.md) them in the [midbar](../../strongs/h/h4057.md) of [Sînay](../../strongs/h/h5514.md).

<a name="numbers_1_20"></a>Numbers 1:20

And the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), [Yisra'el](../../strongs/h/h3478.md) [bᵊḵôr](../../strongs/h/h1060.md), by their [towlĕdah](../../strongs/h/h8435.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), according to the [mispār](../../strongs/h/h4557.md) of the [shem](../../strongs/h/h8034.md), by their [gulgōleṯ](../../strongs/h/h1538.md), every [zāḵār](../../strongs/h/h2145.md) from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that were able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md);

<a name="numbers_1_21"></a>Numbers 1:21

Those that were [paqad](../../strongs/h/h6485.md) of them, even of the [maṭṭê](../../strongs/h/h4294.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), were forty and six thousand and five hundred.

<a name="numbers_1_22"></a>Numbers 1:22

Of the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md), by their [towlĕdah](../../strongs/h/h8435.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), those that were [paqad](../../strongs/h/h6485.md) of them, according to the [mispār](../../strongs/h/h4557.md) of the [shem](../../strongs/h/h8034.md), by their [gulgōleṯ](../../strongs/h/h1538.md), every [zāḵār](../../strongs/h/h2145.md) from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that were able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md);

<a name="numbers_1_23"></a>Numbers 1:23

Those that were [paqad](../../strongs/h/h6485.md) of them, even of the [maṭṭê](../../strongs/h/h4294.md) of [Šimʿôn](../../strongs/h/h8095.md), were fifty and nine thousand and three hundred.

<a name="numbers_1_24"></a>Numbers 1:24

Of the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md), by their [towlĕdah](../../strongs/h/h8435.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), according to the [mispār](../../strongs/h/h4557.md) of the [shem](../../strongs/h/h8034.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that were able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md);

<a name="numbers_1_25"></a>Numbers 1:25

Those that were [paqad](../../strongs/h/h6485.md) of them, even of the [maṭṭê](../../strongs/h/h4294.md) of [Gāḏ](../../strongs/h/h1410.md), were forty and five thousand six hundred and fifty.

<a name="numbers_1_26"></a>Numbers 1:26

Of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md), by their [towlĕdah](../../strongs/h/h8435.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), according to the [mispār](../../strongs/h/h4557.md) of the [shem](../../strongs/h/h8034.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that were able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md);

<a name="numbers_1_27"></a>Numbers 1:27

Those that were [paqad](../../strongs/h/h6485.md) of them, even of the [maṭṭê](../../strongs/h/h4294.md) of [Yehuwdah](../../strongs/h/h3063.md), were threescore and fourteen thousand and six hundred.

<a name="numbers_1_28"></a>Numbers 1:28

Of the [ben](../../strongs/h/h1121.md) of [Yiśśāśḵār](../../strongs/h/h3485.md), by their [towlĕdah](../../strongs/h/h8435.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), according to the [mispār](../../strongs/h/h4557.md) of the [shem](../../strongs/h/h8034.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that were able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md);

<a name="numbers_1_29"></a>Numbers 1:29

Those that were [paqad](../../strongs/h/h6485.md) of them, even of the [maṭṭê](../../strongs/h/h4294.md) of [Yiśśāśḵār](../../strongs/h/h3485.md), were fifty and four thousand and four hundred.

<a name="numbers_1_30"></a>Numbers 1:30

Of the [ben](../../strongs/h/h1121.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md), by their [towlĕdah](../../strongs/h/h8435.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), according to the [mispār](../../strongs/h/h4557.md) of the [shem](../../strongs/h/h8034.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that were able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md);

<a name="numbers_1_31"></a>Numbers 1:31

Those that were [paqad](../../strongs/h/h6485.md) of them, even of the [maṭṭê](../../strongs/h/h4294.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md), were fifty and seven thousand and four hundred.

<a name="numbers_1_32"></a>Numbers 1:32

Of the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md), namely, of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md), by their [towlĕdah](../../strongs/h/h8435.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), according to the [mispār](../../strongs/h/h4557.md) of the [shem](../../strongs/h/h8034.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that were able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md);

<a name="numbers_1_33"></a>Numbers 1:33

Those that were [paqad](../../strongs/h/h6485.md) of them, even of the [maṭṭê](../../strongs/h/h4294.md) of ['Ep̄rayim](../../strongs/h/h669.md), were forty thousand and five hundred.

<a name="numbers_1_34"></a>Numbers 1:34

Of the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md), by their [towlĕdah](../../strongs/h/h8435.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), according to the [mispār](../../strongs/h/h4557.md) of the [shem](../../strongs/h/h8034.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that were able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md);

<a name="numbers_1_35"></a>Numbers 1:35

Those that were [paqad](../../strongs/h/h6485.md) of them, even of the [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md), were thirty and two thousand and two hundred.

<a name="numbers_1_36"></a>Numbers 1:36

Of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md), by their [towlĕdah](../../strongs/h/h8435.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), according to the [mispār](../../strongs/h/h4557.md) of the [shem](../../strongs/h/h8034.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that were able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md);

<a name="numbers_1_37"></a>Numbers 1:37

Those that were [paqad](../../strongs/h/h6485.md) of them, even of the [maṭṭê](../../strongs/h/h4294.md) of [Binyāmîn](../../strongs/h/h1144.md), were thirty and five thousand and four hundred.

<a name="numbers_1_38"></a>Numbers 1:38

Of the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md), by their [towlĕdah](../../strongs/h/h8435.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), according to the [mispār](../../strongs/h/h4557.md) of the [shem](../../strongs/h/h8034.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that were able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md);

<a name="numbers_1_39"></a>Numbers 1:39

Those that were [paqad](../../strongs/h/h6485.md) of them, even of the [maṭṭê](../../strongs/h/h4294.md) of [Dān](../../strongs/h/h1835.md), were threescore and two thousand and seven hundred.

<a name="numbers_1_40"></a>Numbers 1:40

Of the [ben](../../strongs/h/h1121.md) of ['Āšēr](../../strongs/h/h836.md), by their [towlĕdah](../../strongs/h/h8435.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), according to the [mispār](../../strongs/h/h4557.md) of the [shem](../../strongs/h/h8034.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that were able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md);

<a name="numbers_1_41"></a>Numbers 1:41

Those that were [paqad](../../strongs/h/h6485.md) of them, even of the [maṭṭê](../../strongs/h/h4294.md) of ['Āšēr](../../strongs/h/h836.md), were forty and one thousand and five hundred.

<a name="numbers_1_42"></a>Numbers 1:42

Of the [ben](../../strongs/h/h1121.md) of [Nap̄tālî](../../strongs/h/h5321.md), throughout their [towlĕdah](../../strongs/h/h8435.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), according to the [mispār](../../strongs/h/h4557.md) of the [shem](../../strongs/h/h8034.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that were able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md);

<a name="numbers_1_43"></a>Numbers 1:43

Those that were [paqad](../../strongs/h/h6485.md) of them, even of the [maṭṭê](../../strongs/h/h4294.md) of [Nap̄tālî](../../strongs/h/h5321.md), were fifty and three thousand and four hundred.

<a name="numbers_1_44"></a>Numbers 1:44

These are those that were [paqad](../../strongs/h/h6485.md), which [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [paqad](../../strongs/h/h6485.md), and the [nāśî'](../../strongs/h/h5387.md) of [Yisra'el](../../strongs/h/h3478.md), being twelve ['iysh](../../strongs/h/h376.md): each one was for the [bayith](../../strongs/h/h1004.md) of his ['ab](../../strongs/h/h1.md).

<a name="numbers_1_45"></a>Numbers 1:45

So were all those that were [paqad](../../strongs/h/h6485.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), all that were able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md) in [Yisra'el](../../strongs/h/h3478.md);

<a name="numbers_1_46"></a>Numbers 1:46

Even all they that were [paqad](../../strongs/h/h6485.md) were six hundred thousand and three thousand and five hundred and fifty.

<a name="numbers_1_47"></a>Numbers 1:47

But the [Lᵊvî](../../strongs/h/h3881.md) after the [maṭṭê](../../strongs/h/h4294.md) of their ['ab](../../strongs/h/h1.md) were not [paqad](../../strongs/h/h6485.md) [tavek](../../strongs/h/h8432.md) them.

<a name="numbers_1_48"></a>Numbers 1:48

For [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_1_49"></a>Numbers 1:49

Only thou shalt not [paqad](../../strongs/h/h6485.md) the [maṭṭê](../../strongs/h/h4294.md) of [Lēvî](../../strongs/h/h3878.md), neither [nasa'](../../strongs/h/h5375.md) the [ro'sh](../../strongs/h/h7218.md) of them [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="numbers_1_50"></a>Numbers 1:50

But thou shalt [paqad](../../strongs/h/h6485.md) the [Lᵊvî](../../strongs/h/h3881.md) over the [miškān](../../strongs/h/h4908.md) of [ʿēḏûṯ](../../strongs/h/h5715.md), and over all the [kĕliy](../../strongs/h/h3627.md) thereof, and over all things that belong to it: they shall [nasa'](../../strongs/h/h5375.md) the [miškān](../../strongs/h/h4908.md), and all the [kĕliy](../../strongs/h/h3627.md) thereof; and they shall [sharath](../../strongs/h/h8334.md) unto it, and shall [ḥānâ](../../strongs/h/h2583.md) [cabiyb](../../strongs/h/h5439.md) the [miškān](../../strongs/h/h4908.md).

<a name="numbers_1_51"></a>Numbers 1:51

And when the [miškān](../../strongs/h/h4908.md) [nāsaʿ](../../strongs/h/h5265.md), the [Lᵊvî](../../strongs/h/h3881.md) shall [yarad](../../strongs/h/h3381.md) it: and when the [miškān](../../strongs/h/h4908.md) is to be [ḥānâ](../../strongs/h/h2583.md), the [Lᵊvî](../../strongs/h/h3881.md) shall [quwm](../../strongs/h/h6965.md) it: and the [zûr](../../strongs/h/h2114.md) that [qārēḇ](../../strongs/h/h7131.md) shall [muwth](../../strongs/h/h4191.md).

<a name="numbers_1_52"></a>Numbers 1:52

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) shall [ḥānâ](../../strongs/h/h2583.md), every ['iysh](../../strongs/h/h376.md) by his own [maḥănê](../../strongs/h/h4264.md), and every ['iysh](../../strongs/h/h376.md) by his own [deḡel](../../strongs/h/h1714.md), throughout their [tsaba'](../../strongs/h/h6635.md).

<a name="numbers_1_53"></a>Numbers 1:53

But the [Lᵊvî](../../strongs/h/h3881.md) shall [ḥānâ](../../strongs/h/h2583.md) [cabiyb](../../strongs/h/h5439.md) the [miškān](../../strongs/h/h4908.md) of [ʿēḏûṯ](../../strongs/h/h5715.md), that there be no [qeṣep̄](../../strongs/h/h7110.md) upon the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): and the [Lᵊvî](../../strongs/h/h3881.md) shall [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of the [miškān](../../strongs/h/h4908.md) of [ʿēḏûṯ](../../strongs/h/h5715.md).

<a name="numbers_1_54"></a>Numbers 1:54

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) according to all that [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md), so ['asah](../../strongs/h/h6213.md) they.

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 2](numbers_2.md)