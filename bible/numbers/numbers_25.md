# [Numbers 25](https://www.blueletterbible.org/kjv/num/25)

<a name="numbers_25_1"></a>Numbers 25:1

And [Yisra'el](../../strongs/h/h3478.md) [yashab](../../strongs/h/h3427.md) in [Šiṭṭāym](../../strongs/h/h7851.md), and the ['am](../../strongs/h/h5971.md) [ḥālal](../../strongs/h/h2490.md) to [zānâ](../../strongs/h/h2181.md) with the [bath](../../strongs/h/h1323.md) of [Mô'āḇ](../../strongs/h/h4124.md).

<a name="numbers_25_2"></a>Numbers 25:2

And they [qara'](../../strongs/h/h7121.md) the ['am](../../strongs/h/h5971.md) unto the [zebach](../../strongs/h/h2077.md) of their ['Elohiym](../../strongs/h/h430.md): and the ['am](../../strongs/h/h5971.md) did ['akal](../../strongs/h/h398.md), and [shachah](../../strongs/h/h7812.md) to their ['Elohiym](../../strongs/h/h430.md).

<a name="numbers_25_3"></a>Numbers 25:3

And [Yisra'el](../../strongs/h/h3478.md) [ṣāmaḏ](../../strongs/h/h6775.md) himself unto [Baʿal pᵊʿôr](../../strongs/h/h1187.md): and the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_25_4"></a>Numbers 25:4

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [laqach](../../strongs/h/h3947.md) all the [ro'sh](../../strongs/h/h7218.md) of the ['am](../../strongs/h/h5971.md), and [yāqaʿ](../../strongs/h/h3363.md) them [neḡeḏ](../../strongs/h/h5048.md) [Yĕhovah](../../strongs/h/h3068.md) against the [šemeš](../../strongs/h/h8121.md), that the [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) may be [shuwb](../../strongs/h/h7725.md) from [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_25_5"></a>Numbers 25:5

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto the [shaphat](../../strongs/h/h8199.md) of [Yisra'el](../../strongs/h/h3478.md), [harag](../../strongs/h/h2026.md) ye ['iysh](../../strongs/h/h376.md) ['iysh](../../strongs/h/h376.md) his ['enowsh](../../strongs/h/h582.md) that were [ṣāmaḏ](../../strongs/h/h6775.md) unto [Baʿal pᵊʿôr](../../strongs/h/h1187.md).

<a name="numbers_25_6"></a>Numbers 25:6

And, behold, ['iysh](../../strongs/h/h376.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) and [qāraḇ](../../strongs/h/h7126.md) unto his ['ach](../../strongs/h/h251.md) a [Miḏyānî](../../strongs/h/h4084.md) in the ['ayin](../../strongs/h/h5869.md) of [Mōshe](../../strongs/h/h4872.md), and in the ['ayin](../../strongs/h/h5869.md) of all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), who were [bāḵâ](../../strongs/h/h1058.md) before the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="numbers_25_7"></a>Numbers 25:7

And when [Pînḥās](../../strongs/h/h6372.md), the [ben](../../strongs/h/h1121.md) of ['Elʿāzār](../../strongs/h/h499.md), the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md), [ra'ah](../../strongs/h/h7200.md) it, he [quwm](../../strongs/h/h6965.md) from [tavek](../../strongs/h/h8432.md) the ['edah](../../strongs/h/h5712.md), and [laqach](../../strongs/h/h3947.md) a [rōmaḥ](../../strongs/h/h7420.md) in his [yad](../../strongs/h/h3027.md);

<a name="numbers_25_8"></a>Numbers 25:8

And he [bow'](../../strongs/h/h935.md) ['aḥar](../../strongs/h/h310.md) the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) into the [qubâ](../../strongs/h/h6898.md), and [dāqar](../../strongs/h/h1856.md) [šᵊnayim](../../strongs/h/h8147.md) of them through, the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md), and the ['ishshah](../../strongs/h/h802.md) through her [qōḇâ](../../strongs/h/h6897.md). So the [magēp̄â](../../strongs/h/h4046.md) was [ʿāṣar](../../strongs/h/h6113.md) from the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_25_9"></a>Numbers 25:9

And those that [muwth](../../strongs/h/h4191.md) in the [magēp̄â](../../strongs/h/h4046.md) were twenty and four thousand.

<a name="numbers_25_10"></a>Numbers 25:10

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_25_11"></a>Numbers 25:11

[Pînḥās](../../strongs/h/h6372.md), the [ben](../../strongs/h/h1121.md) of ['Elʿāzār](../../strongs/h/h499.md), the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md), hath [shuwb](../../strongs/h/h7725.md) my [chemah](../../strongs/h/h2534.md) from the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), while he was [qānā'](../../strongs/h/h7065.md) for my [qin'â](../../strongs/h/h7068.md) [tavek](../../strongs/h/h8432.md) them, that I [kalah](../../strongs/h/h3615.md) not the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) in my [qin'â](../../strongs/h/h7068.md).

<a name="numbers_25_12"></a>Numbers 25:12

Wherefore ['āmar](../../strongs/h/h559.md), Behold, I [nathan](../../strongs/h/h5414.md) unto him my [bĕriyth](../../strongs/h/h1285.md) of [shalowm](../../strongs/h/h7965.md):

<a name="numbers_25_13"></a>Numbers 25:13

And he shall have it, and his [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) him, even the [bĕriyth](../../strongs/h/h1285.md) of an ['owlam](../../strongs/h/h5769.md) [kᵊhunnâ](../../strongs/h/h3550.md); ['ăšer](../../strongs/h/h834.md) he was [qānā'](../../strongs/h/h7065.md) for his ['Elohiym](../../strongs/h/h430.md), and made a [kāp̄ar](../../strongs/h/h3722.md) for the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_25_14"></a>Numbers 25:14

Now the [shem](../../strongs/h/h8034.md) of the ['iysh](../../strongs/h/h376.md) that was [nakah](../../strongs/h/h5221.md), even that was [nakah](../../strongs/h/h5221.md) with the [Miḏyānî](../../strongs/h/h4084.md), was [Zimrî](../../strongs/h/h2174.md), the [ben](../../strongs/h/h1121.md) of [Sallû](../../strongs/h/h5543.md), a [nāśî'](../../strongs/h/h5387.md) of an ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) among the [Šimʿōnî](../../strongs/h/h8099.md).

<a name="numbers_25_15"></a>Numbers 25:15

And the [shem](../../strongs/h/h8034.md) of the [Miḏyānî](../../strongs/h/h4084.md) ['ishshah](../../strongs/h/h802.md) that was [nakah](../../strongs/h/h5221.md) was [kāzbî](../../strongs/h/h3579.md), the [bath](../../strongs/h/h1323.md) of [Ṣaûār](../../strongs/h/h6698.md); he was [ro'sh](../../strongs/h/h7218.md) over a ['ummah](../../strongs/h/h523.md), and of an ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) in [Miḏyān](../../strongs/h/h4080.md).

<a name="numbers_25_16"></a>Numbers 25:16

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_25_17"></a>Numbers 25:17

[tsarar](../../strongs/h/h6887.md) the [Miḏyānî](../../strongs/h/h4084.md), and [nakah](../../strongs/h/h5221.md) them:

<a name="numbers_25_18"></a>Numbers 25:18

For they [tsarar](../../strongs/h/h6887.md) you with their [nēḵel](../../strongs/h/h5231.md), wherewith they have [nāḵal](../../strongs/h/h5230.md) you in the [dabar](../../strongs/h/h1697.md) of [P̄ᵊʿôr](../../strongs/h/h6465.md), and in the [dabar](../../strongs/h/h1697.md) of [kāzbî](../../strongs/h/h3579.md), the [bath](../../strongs/h/h1323.md) of a [nāśî'](../../strongs/h/h5387.md) of [Miḏyān](../../strongs/h/h4080.md), their ['āḥôṯ](../../strongs/h/h269.md), which was [nakah](../../strongs/h/h5221.md) in the [yowm](../../strongs/h/h3117.md) of the [magēp̄â](../../strongs/h/h4046.md) for [P̄ᵊʿôr](../../strongs/h/h6465.md) [dabar](../../strongs/h/h1697.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 24](numbers_24.md) - [Numbers 26](numbers_26.md)