# [Numbers 20](https://www.blueletterbible.org/kjv/num/20)

<a name="numbers_20_1"></a>Numbers 20:1

Then [bow'](../../strongs/h/h935.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), even the whole ['edah](../../strongs/h/h5712.md), into the [midbar](../../strongs/h/h4057.md) of [Ṣin](../../strongs/h/h6790.md) in the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md): and the ['am](../../strongs/h/h5971.md) [yashab](../../strongs/h/h3427.md) in [Qāḏēš](../../strongs/h/h6946.md); and [Miryām](../../strongs/h/h4813.md) [muwth](../../strongs/h/h4191.md) there, and was [qāḇar](../../strongs/h/h6912.md) there.

<a name="numbers_20_2"></a>Numbers 20:2

And there was no [mayim](../../strongs/h/h4325.md) for the ['edah](../../strongs/h/h5712.md): and they [qāhal](../../strongs/h/h6950.md) themselves together against [Mōshe](../../strongs/h/h4872.md) and against ['Ahărôn](../../strongs/h/h175.md).

<a name="numbers_20_3"></a>Numbers 20:3

And the ['am](../../strongs/h/h5971.md) [riyb](../../strongs/h/h7378.md) with [Mōshe](../../strongs/h/h4872.md), and ['āmar](../../strongs/h/h559.md), ['āmar](../../strongs/h/h559.md), [lû'](../../strongs/h/h3863.md) that we had [gāvaʿ](../../strongs/h/h1478.md) when our ['ach](../../strongs/h/h251.md) [gāvaʿ](../../strongs/h/h1478.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md)!

<a name="numbers_20_4"></a>Numbers 20:4

And why have ye [bow'](../../strongs/h/h935.md) the [qāhēl](../../strongs/h/h6951.md) of [Yĕhovah](../../strongs/h/h3068.md) into this [midbar](../../strongs/h/h4057.md), that we and our [bᵊʿîr](../../strongs/h/h1165.md) should [muwth](../../strongs/h/h4191.md) there?

<a name="numbers_20_5"></a>Numbers 20:5

And wherefore have ye made us to [ʿālâ](../../strongs/h/h5927.md) out of [Mitsrayim](../../strongs/h/h4714.md), to [bow'](../../strongs/h/h935.md) us in unto this [ra'](../../strongs/h/h7451.md) [maqowm](../../strongs/h/h4725.md)? it is no [maqowm](../../strongs/h/h4725.md) of [zera'](../../strongs/h/h2233.md), or of [tĕ'en](../../strongs/h/h8384.md), or of [gep̄en](../../strongs/h/h1612.md), or of [rimmôn](../../strongs/h/h7416.md); neither is there any [mayim](../../strongs/h/h4325.md) to [šāṯâ](../../strongs/h/h8354.md).

<a name="numbers_20_6"></a>Numbers 20:6

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [bow'](../../strongs/h/h935.md) from the [paniym](../../strongs/h/h6440.md) of the [qāhēl](../../strongs/h/h6951.md) unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and they [naphal](../../strongs/h/h5307.md) upon their [paniym](../../strongs/h/h6440.md): and the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) unto them.

<a name="numbers_20_7"></a>Numbers 20:7

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_20_8"></a>Numbers 20:8

[laqach](../../strongs/h/h3947.md) the [maṭṭê](../../strongs/h/h4294.md), and [qāhal](../../strongs/h/h6950.md) thou the ['edah](../../strongs/h/h5712.md) together, thou, and ['Ahărôn](../../strongs/h/h175.md) thy ['ach](../../strongs/h/h251.md), and [dabar](../../strongs/h/h1696.md) ye unto the [cela'](../../strongs/h/h5553.md) before their ['ayin](../../strongs/h/h5869.md); and it shall [nathan](../../strongs/h/h5414.md) his [mayim](../../strongs/h/h4325.md), and thou shalt [yāṣā'](../../strongs/h/h3318.md) to them [mayim](../../strongs/h/h4325.md) out of the [cela'](../../strongs/h/h5553.md): so thou shalt [šāqâ](../../strongs/h/h8248.md) the ['edah](../../strongs/h/h5712.md) and their [bᵊʿîr](../../strongs/h/h1165.md).

<a name="numbers_20_9"></a>Numbers 20:9

And [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) the [maṭṭê](../../strongs/h/h4294.md) from [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), as he [tsavah](../../strongs/h/h6680.md) him.

<a name="numbers_20_10"></a>Numbers 20:10

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [qāhal](../../strongs/h/h6950.md) the [qāhēl](../../strongs/h/h6951.md) together [paniym](../../strongs/h/h6440.md) the [cela'](../../strongs/h/h5553.md), and he ['āmar](../../strongs/h/h559.md) unto them, [shama'](../../strongs/h/h8085.md) now, ye [marah](../../strongs/h/h4784.md); must we [yāṣā'](../../strongs/h/h3318.md) you [mayim](../../strongs/h/h4325.md) out of this [cela'](../../strongs/h/h5553.md)?

<a name="numbers_20_11"></a>Numbers 20:11

And [Mōshe](../../strongs/h/h4872.md) [ruwm](../../strongs/h/h7311.md) his [yad](../../strongs/h/h3027.md), and with his [maṭṭê](../../strongs/h/h4294.md) he [nakah](../../strongs/h/h5221.md) the [cela'](../../strongs/h/h5553.md) twice: and the [mayim](../../strongs/h/h4325.md) [yāṣā'](../../strongs/h/h3318.md) [rab](../../strongs/h/h7227.md), and the ['edah](../../strongs/h/h5712.md) [šāṯâ](../../strongs/h/h8354.md), and their [bᵊʿîr](../../strongs/h/h1165.md) also.

<a name="numbers_20_12"></a>Numbers 20:12

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md), Because ye ['aman](../../strongs/h/h539.md) me not, to [qadash](../../strongs/h/h6942.md) me in the ['ayin](../../strongs/h/h5869.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), therefore ye shall not [bow'](../../strongs/h/h935.md) this [qāhēl](../../strongs/h/h6951.md) into the ['erets](../../strongs/h/h776.md) which I have [nathan](../../strongs/h/h5414.md) them.

<a name="numbers_20_13"></a>Numbers 20:13

This is the [mayim](../../strongs/h/h4325.md) of [Mᵊrîḇâ](../../strongs/h/h4809.md); because the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [riyb](../../strongs/h/h7378.md) with [Yĕhovah](../../strongs/h/h3068.md), and he was [qadash](../../strongs/h/h6942.md) in them.

<a name="numbers_20_14"></a>Numbers 20:14

And [Mōshe](../../strongs/h/h4872.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) from [Qāḏēš](../../strongs/h/h6946.md) unto the [melek](../../strongs/h/h4428.md) of ['Ĕḏōm](../../strongs/h/h123.md), Thus ['āmar](../../strongs/h/h559.md) thy ['ach](../../strongs/h/h251.md) [Yisra'el](../../strongs/h/h3478.md), Thou [yada'](../../strongs/h/h3045.md) all the [tᵊlā'â](../../strongs/h/h8513.md) that hath [māṣā'](../../strongs/h/h4672.md) us:

<a name="numbers_20_15"></a>Numbers 20:15

How our ['ab](../../strongs/h/h1.md) [yarad](../../strongs/h/h3381.md) into [Mitsrayim](../../strongs/h/h4714.md), and we have [yashab](../../strongs/h/h3427.md) in [Mitsrayim](../../strongs/h/h4714.md) a [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md); and the [Mitsrayim](../../strongs/h/h4714.md) [ra'a'](../../strongs/h/h7489.md) us, and our ['ab](../../strongs/h/h1.md):

<a name="numbers_20_16"></a>Numbers 20:16

And when we [ṣāʿaq](../../strongs/h/h6817.md) unto [Yĕhovah](../../strongs/h/h3068.md), he [shama'](../../strongs/h/h8085.md) our [qowl](../../strongs/h/h6963.md), and [shalach](../../strongs/h/h7971.md) a [mal'ak](../../strongs/h/h4397.md), and hath [yāṣā'](../../strongs/h/h3318.md) us out of [Mitsrayim](../../strongs/h/h4714.md): and, behold, we are in [Qāḏēš](../../strongs/h/h6946.md), a [ʿîr](../../strongs/h/h5892.md) in the [qāṣê](../../strongs/h/h7097.md) of thy [gᵊḇûl](../../strongs/h/h1366.md):

<a name="numbers_20_17"></a>Numbers 20:17

Let us ['abar](../../strongs/h/h5674.md), I pray thee, through thy ['erets](../../strongs/h/h776.md): we will not ['abar](../../strongs/h/h5674.md) through the [sadeh](../../strongs/h/h7704.md), or through the [kerem](../../strongs/h/h3754.md), neither will we [šāṯâ](../../strongs/h/h8354.md) of the [mayim](../../strongs/h/h4325.md) of the [bᵊ'ēr](../../strongs/h/h875.md): we will [yālaḵ](../../strongs/h/h3212.md) by the [melek](../../strongs/h/h4428.md) [derek](../../strongs/h/h1870.md), we will not [natah](../../strongs/h/h5186.md) to the [yamiyn](../../strongs/h/h3225.md) nor to the [śᵊmō'l](../../strongs/h/h8040.md), until we have ['abar](../../strongs/h/h5674.md) thy [gᵊḇûl](../../strongs/h/h1366.md).

<a name="numbers_20_18"></a>Numbers 20:18

And ['Ĕḏōm](../../strongs/h/h123.md) ['āmar](../../strongs/h/h559.md) unto him, Thou shalt not ['abar](../../strongs/h/h5674.md) by me, lest I [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) thee with the [chereb](../../strongs/h/h2719.md).

<a name="numbers_20_19"></a>Numbers 20:19

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto him, We will [ʿālâ](../../strongs/h/h5927.md) by the [mĕcillah](../../strongs/h/h4546.md): and if I and my [miqnê](../../strongs/h/h4735.md) [šāṯâ](../../strongs/h/h8354.md) of thy [mayim](../../strongs/h/h4325.md), then I will [nathan](../../strongs/h/h5414.md) [meḵer](../../strongs/h/h4377.md) for it: I will only, without doing [dabar](../../strongs/h/h1697.md) else, ['abar](../../strongs/h/h5674.md) on my [regel](../../strongs/h/h7272.md).

<a name="numbers_20_20"></a>Numbers 20:20

And he ['āmar](../../strongs/h/h559.md), Thou shalt not ['abar](../../strongs/h/h5674.md). And ['Ĕḏōm](../../strongs/h/h123.md) [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) him with [kāḇēḏ](../../strongs/h/h3515.md) ['am](../../strongs/h/h5971.md), and with a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md).

<a name="numbers_20_21"></a>Numbers 20:21

Thus ['Ĕḏōm](../../strongs/h/h123.md) [mā'ēn](../../strongs/h/h3985.md) to [nathan](../../strongs/h/h5414.md) [Yisra'el](../../strongs/h/h3478.md) ['abar](../../strongs/h/h5674.md) through his [gᵊḇûl](../../strongs/h/h1366.md): wherefore [Yisra'el](../../strongs/h/h3478.md) [natah](../../strongs/h/h5186.md) from him.

<a name="numbers_20_22"></a>Numbers 20:22

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), even the ['edah](../../strongs/h/h5712.md), [nāsaʿ](../../strongs/h/h5265.md) from [Qāḏēš](../../strongs/h/h6946.md), and [bow'](../../strongs/h/h935.md) unto [har](../../strongs/h/h2022.md) [hōr](../../strongs/h/h2023.md).

<a name="numbers_20_23"></a>Numbers 20:23

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) in [har](../../strongs/h/h2022.md) [hōr](../../strongs/h/h2023.md), by the [gᵊḇûl](../../strongs/h/h1366.md) of the ['erets](../../strongs/h/h776.md) of ['Ĕḏōm](../../strongs/h/h123.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_20_24"></a>Numbers 20:24

['Ahărôn](../../strongs/h/h175.md) shall be ['āsap̄](../../strongs/h/h622.md) unto his ['am](../../strongs/h/h5971.md): for he shall not [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) which I have [nathan](../../strongs/h/h5414.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), because ye [marah](../../strongs/h/h4784.md) against my [peh](../../strongs/h/h6310.md) at the [mayim](../../strongs/h/h4325.md) of [Mᵊrîḇâ](../../strongs/h/h4809.md).

<a name="numbers_20_25"></a>Numbers 20:25

[laqach](../../strongs/h/h3947.md) ['Ahărôn](../../strongs/h/h175.md) and ['Elʿāzār](../../strongs/h/h499.md) his [ben](../../strongs/h/h1121.md), and [ʿālâ](../../strongs/h/h5927.md) them unto [har](../../strongs/h/h2022.md) [Hōr](../../strongs/h/h2023.md):

<a name="numbers_20_26"></a>Numbers 20:26

And [pāšaṭ](../../strongs/h/h6584.md) ['Ahărôn](../../strongs/h/h175.md) of his [beḡeḏ](../../strongs/h/h899.md), and [labash](../../strongs/h/h3847.md) them upon ['Elʿāzār](../../strongs/h/h499.md) his [ben](../../strongs/h/h1121.md): and ['Ahărôn](../../strongs/h/h175.md) shall be ['āsap̄](../../strongs/h/h622.md) unto his people, and shall [muwth](../../strongs/h/h4191.md) there.

<a name="numbers_20_27"></a>Numbers 20:27

And [Mōshe](../../strongs/h/h4872.md) ['asah](../../strongs/h/h6213.md) as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md): and they [ʿālâ](../../strongs/h/h5927.md) into [har](../../strongs/h/h2022.md) [Hōr](../../strongs/h/h2023.md) in the ['ayin](../../strongs/h/h5869.md) of all the ['edah](../../strongs/h/h5712.md).

<a name="numbers_20_28"></a>Numbers 20:28

And [Mōshe](../../strongs/h/h4872.md) [pāšaṭ](../../strongs/h/h6584.md) ['Ahărôn](../../strongs/h/h175.md) of his [beḡeḏ](../../strongs/h/h899.md), and [labash](../../strongs/h/h3847.md) them upon ['Elʿāzār](../../strongs/h/h499.md) his [ben](../../strongs/h/h1121.md); and ['Ahărôn](../../strongs/h/h175.md) [muwth](../../strongs/h/h4191.md) there in the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md): and [Mōshe](../../strongs/h/h4872.md) and ['Elʿāzār](../../strongs/h/h499.md) [yarad](../../strongs/h/h3381.md) from the [har](../../strongs/h/h2022.md).

<a name="numbers_20_29"></a>Numbers 20:29

And when all the ['edah](../../strongs/h/h5712.md) [ra'ah](../../strongs/h/h7200.md) that ['Ahărôn](../../strongs/h/h175.md) was [gāvaʿ](../../strongs/h/h1478.md), they [bāḵâ](../../strongs/h/h1058.md) for ['Ahărôn](../../strongs/h/h175.md) thirty [yowm](../../strongs/h/h3117.md), even all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 19](numbers_19.md) - [Numbers 21](numbers_21.md)