# [Numbers 15](https://www.blueletterbible.org/kjv/num/15)

<a name="numbers_15_1"></a>Numbers 15:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_15_2"></a>Numbers 15:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, When ye be [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) of your [môšāḇ](../../strongs/h/h4186.md), which I [nathan](../../strongs/h/h5414.md) unto you,

<a name="numbers_15_3"></a>Numbers 15:3

And will ['asah](../../strongs/h/h6213.md) an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md), an [ʿōlâ](../../strongs/h/h5930.md), or a [zebach](../../strongs/h/h2077.md) in [pala'](../../strongs/h/h6381.md) a [neḏer](../../strongs/h/h5088.md), or in a [nᵊḏāḇâ](../../strongs/h/h5071.md), or in your [môʿēḏ](../../strongs/h/h4150.md), to ['asah](../../strongs/h/h6213.md) a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md), of the [bāqār](../../strongs/h/h1241.md) or of the [tso'n](../../strongs/h/h6629.md):

<a name="numbers_15_4"></a>Numbers 15:4

Then shall he that [qāraḇ](../../strongs/h/h7126.md) his [qorban](../../strongs/h/h7133.md) unto [Yĕhovah](../../strongs/h/h3068.md) [qāraḇ](../../strongs/h/h7126.md) a [minchah](../../strongs/h/h4503.md) of a [ʿiśśārôn](../../strongs/h/h6241.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with the fourth of an [hîn](../../strongs/h/h1969.md) of [šemen](../../strongs/h/h8081.md).

<a name="numbers_15_5"></a>Numbers 15:5

And the fourth of an [hîn](../../strongs/h/h1969.md) of [yayin](../../strongs/h/h3196.md) for a [necek](../../strongs/h/h5262.md) shalt thou ['asah](../../strongs/h/h6213.md) with the an [ʿōlâ](../../strongs/h/h5930.md) or [zebach](../../strongs/h/h2077.md), for one [keḇeś](../../strongs/h/h3532.md).

<a name="numbers_15_6"></a>Numbers 15:6

Or for an ['ayil](../../strongs/h/h352.md), thou shalt ['asah](../../strongs/h/h6213.md) for a [minchah](../../strongs/h/h4503.md) two [ʿiśśārôn](../../strongs/h/h6241.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with the third of an [hîn](../../strongs/h/h1969.md) of [šemen](../../strongs/h/h8081.md).

<a name="numbers_15_7"></a>Numbers 15:7

And for a drink [necek](../../strongs/h/h5262.md) thou shalt [qāraḇ](../../strongs/h/h7126.md) the third of an [hîn](../../strongs/h/h1969.md) of [yayin](../../strongs/h/h3196.md), for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_15_8"></a>Numbers 15:8

And when thou ['asah](../../strongs/h/h6213.md) a [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) for an [ʿōlâ](../../strongs/h/h5930.md), or for a [zebach](../../strongs/h/h2077.md) in [pala'](../../strongs/h/h6381.md) a [neḏer](../../strongs/h/h5088.md), or [šelem](../../strongs/h/h8002.md) unto [Yĕhovah](../../strongs/h/h3068.md):

<a name="numbers_15_9"></a>Numbers 15:9

Then shall he [qāraḇ](../../strongs/h/h7126.md) with a [bāqār](../../strongs/h/h1241.md) [ben](../../strongs/h/h1121.md) a [minchah](../../strongs/h/h4503.md) of three [ʿiśśārôn](../../strongs/h/h6241.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with half an [hîn](../../strongs/h/h1969.md) of [šemen](../../strongs/h/h8081.md).

<a name="numbers_15_10"></a>Numbers 15:10

And thou shalt [qāraḇ](../../strongs/h/h7126.md) for a drink [necek](../../strongs/h/h5262.md) half an [hîn](../../strongs/h/h1969.md) of [yayin](../../strongs/h/h3196.md), for an ['iššê](../../strongs/h/h801.md), of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_15_11"></a>Numbers 15:11

Thus shall it be ['asah](../../strongs/h/h6213.md) for one [showr](../../strongs/h/h7794.md), or for one ['ayil](../../strongs/h/h352.md), or for a [keḇeś](../../strongs/h/h3532.md) [śê](../../strongs/h/h7716.md), or a [ʿēz](../../strongs/h/h5795.md).

<a name="numbers_15_12"></a>Numbers 15:12

According to the [mispār](../../strongs/h/h4557.md) that ye shall ['asah](../../strongs/h/h6213.md), [kāḵâ](../../strongs/h/h3602.md) shall ye ['asah](../../strongs/h/h6213.md) to every one according to their [mispār](../../strongs/h/h4557.md).

<a name="numbers_15_13"></a>Numbers 15:13

All that are ['ezrāḥ](../../strongs/h/h249.md) shall ['asah](../../strongs/h/h6213.md) these things after this manner, in [qāraḇ](../../strongs/h/h7126.md) an ['iššê](../../strongs/h/h801.md), of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_15_14"></a>Numbers 15:14

And if a [ger](../../strongs/h/h1616.md) [guwr](../../strongs/h/h1481.md) with you, or whosoever be [tavek](../../strongs/h/h8432.md) you in your [dôr](../../strongs/h/h1755.md), and will ['asah](../../strongs/h/h6213.md) an ['iššê](../../strongs/h/h801.md), of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md); as ye ['asah](../../strongs/h/h6213.md), so he shall ['asah](../../strongs/h/h6213.md).

<a name="numbers_15_15"></a>Numbers 15:15

One [chuqqah](../../strongs/h/h2708.md) shall be both for you of the [qāhēl](../../strongs/h/h6951.md), and also for the [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) with you, a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) in your [dôr](../../strongs/h/h1755.md): as ye are, so shall the [ger](../../strongs/h/h1616.md) be [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_15_16"></a>Numbers 15:16

One [towrah](../../strongs/h/h8451.md) and one [mishpat](../../strongs/h/h4941.md) shall be for you, and for the [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) with you.

<a name="numbers_15_17"></a>Numbers 15:17

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_15_18"></a>Numbers 15:18

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, When ye [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) whither I [bow'](../../strongs/h/h935.md) you,

<a name="numbers_15_19"></a>Numbers 15:19

Then it shall be, that, when ye ['akal](../../strongs/h/h398.md) of the [lechem](../../strongs/h/h3899.md) of the ['erets](../../strongs/h/h776.md), ye shall [ruwm](../../strongs/h/h7311.md) a [tᵊrûmâ](../../strongs/h/h8641.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_15_20"></a>Numbers 15:20

Ye shall [ruwm](../../strongs/h/h7311.md) a [ḥallâ](../../strongs/h/h2471.md) of the [re'shiyth](../../strongs/h/h7225.md) of your [ʿărîsâ](../../strongs/h/h6182.md) for a [tᵊrûmâ](../../strongs/h/h8641.md): as ye do the [tᵊrûmâ](../../strongs/h/h8641.md) of the [gōren](../../strongs/h/h1637.md), so shall ye [ruwm](../../strongs/h/h7311.md) it.

<a name="numbers_15_21"></a>Numbers 15:21

Of the [re'shiyth](../../strongs/h/h7225.md) of your [ʿărîsâ](../../strongs/h/h6182.md) ye shall [nathan](../../strongs/h/h5414.md) unto [Yĕhovah](../../strongs/h/h3068.md) a [tᵊrûmâ](../../strongs/h/h8641.md) in your [dôr](../../strongs/h/h1755.md).

<a name="numbers_15_22"></a>Numbers 15:22

And if ye have [šāḡâ](../../strongs/h/h7686.md), and not ['asah](../../strongs/h/h6213.md) all these [mitsvah](../../strongs/h/h4687.md), which [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md),

<a name="numbers_15_23"></a>Numbers 15:23

Even all that [Yĕhovah](../../strongs/h/h3068.md) hath [tsavah](../../strongs/h/h6680.md) you by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md), from the [yowm](../../strongs/h/h3117.md) that [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md), and [hāl'â](../../strongs/h/h1973.md) among your [dôr](../../strongs/h/h1755.md);

<a name="numbers_15_24"></a>Numbers 15:24

Then it shall be, if ['asah](../../strongs/h/h6213.md) by [šᵊḡāḡâ](../../strongs/h/h7684.md) without the ['ayin](../../strongs/h/h5869.md) of the ['edah](../../strongs/h/h5712.md), that all the ['edah](../../strongs/h/h5712.md) shall ['asah](../../strongs/h/h6213.md) one [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md) for an [ʿōlâ](../../strongs/h/h5930.md), for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md), with his [minchah](../../strongs/h/h4503.md), and his drink [necek](../../strongs/h/h5262.md), according to the [mishpat](../../strongs/h/h4941.md), and one [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md).

<a name="numbers_15_25"></a>Numbers 15:25

And the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and it shall be [sālaḥ](../../strongs/h/h5545.md) them; for it is [šᵊḡāḡâ](../../strongs/h/h7684.md): and they shall [bow'](../../strongs/h/h935.md) their [qorban](../../strongs/h/h7133.md), an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md), and their [chatta'ath](../../strongs/h/h2403.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), for their [šᵊḡāḡâ](../../strongs/h/h7684.md):

<a name="numbers_15_26"></a>Numbers 15:26

And it shall be [sālaḥ](../../strongs/h/h5545.md) all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and the [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) [tavek](../../strongs/h/h8432.md) them; seeing all the ['am](../../strongs/h/h5971.md) were in [šᵊḡāḡâ](../../strongs/h/h7684.md).

<a name="numbers_15_27"></a>Numbers 15:27

And if any [nephesh](../../strongs/h/h5315.md) [chata'](../../strongs/h/h2398.md) through [šᵊḡāḡâ](../../strongs/h/h7684.md), then he shall [qāraḇ](../../strongs/h/h7126.md) an [ʿēz](../../strongs/h/h5795.md) of the [bath](../../strongs/h/h1323.md) [šānâ](../../strongs/h/h8141.md) for a [chatta'ath](../../strongs/h/h2403.md).

<a name="numbers_15_28"></a>Numbers 15:28

And the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for the [nephesh](../../strongs/h/h5315.md) that [šāḡaḡ](../../strongs/h/h7683.md), when he [chata'](../../strongs/h/h2398.md) by [šᵊḡāḡâ](../../strongs/h/h7684.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), to make a [kāp̄ar](../../strongs/h/h3722.md) for him; and it shall be [sālaḥ](../../strongs/h/h5545.md) him.

<a name="numbers_15_29"></a>Numbers 15:29

Ye shall have one [towrah](../../strongs/h/h8451.md) for him that ['asah](../../strongs/h/h6213.md) through [šᵊḡāḡâ](../../strongs/h/h7684.md), both for him that is ['ezrāḥ](../../strongs/h/h249.md) among the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and for the [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) [tavek](../../strongs/h/h8432.md) them.

<a name="numbers_15_30"></a>Numbers 15:30

But the [nephesh](../../strongs/h/h5315.md) that ['asah](../../strongs/h/h6213.md) [ruwm](../../strongs/h/h7311.md) [yad](../../strongs/h/h3027.md), whether he be ['ezrāḥ](../../strongs/h/h249.md), or a [ger](../../strongs/h/h1616.md), the same [gāḏap̄](../../strongs/h/h1442.md) [Yĕhovah](../../strongs/h/h3068.md); and that [nephesh](../../strongs/h/h5315.md) shall be [karath](../../strongs/h/h3772.md) from [qereḇ](../../strongs/h/h7130.md) his ['am](../../strongs/h/h5971.md).

<a name="numbers_15_31"></a>Numbers 15:31

Because he hath [bazah](../../strongs/h/h959.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), and hath [pārar](../../strongs/h/h6565.md) his [mitsvah](../../strongs/h/h4687.md), that [nephesh](../../strongs/h/h5315.md) shall [karath](../../strongs/h/h3772.md) [karath](../../strongs/h/h3772.md); his ['avon](../../strongs/h/h5771.md) shall be upon him.

<a name="numbers_15_32"></a>Numbers 15:32

And while the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were in the [midbar](../../strongs/h/h4057.md), they [māṣā'](../../strongs/h/h4672.md) an ['iysh](../../strongs/h/h376.md) that [qāšaš](../../strongs/h/h7197.md) ['ets](../../strongs/h/h6086.md) upon the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md).

<a name="numbers_15_33"></a>Numbers 15:33

And they that [māṣā'](../../strongs/h/h4672.md) him [qāšaš](../../strongs/h/h7197.md) ['ets](../../strongs/h/h6086.md) [qāraḇ](../../strongs/h/h7126.md) him unto [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md), and unto all the ['edah](../../strongs/h/h5712.md).

<a name="numbers_15_34"></a>Numbers 15:34

And they [yānaḥ](../../strongs/h/h3240.md) him in [mišmār](../../strongs/h/h4929.md), because it was not [pāraš](../../strongs/h/h6567.md) what should be ['asah](../../strongs/h/h6213.md) to him.

<a name="numbers_15_35"></a>Numbers 15:35

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), The ['iysh](../../strongs/h/h376.md) shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md): all the ['edah](../../strongs/h/h5712.md) shall [rāḡam](../../strongs/h/h7275.md) him with ['eben](../../strongs/h/h68.md) [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md).

<a name="numbers_15_36"></a>Numbers 15:36

And all the ['edah](../../strongs/h/h5712.md) [yāṣā'](../../strongs/h/h3318.md) him [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md), and [rāḡam](../../strongs/h/h7275.md) him with ['eben](../../strongs/h/h68.md), and he [muwth](../../strongs/h/h4191.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="numbers_15_37"></a>Numbers 15:37

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_15_38"></a>Numbers 15:38

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) them that they ['asah](../../strongs/h/h6213.md) them [ṣîṣiṯ](../../strongs/h/h6734.md) the [kanaph](../../strongs/h/h3671.md) of their [beḡeḏ](../../strongs/h/h899.md) throughout their [dôr](../../strongs/h/h1755.md), and that they [nathan](../../strongs/h/h5414.md) upon the [ṣîṣiṯ](../../strongs/h/h6734.md) of the [kanaph](../../strongs/h/h3671.md) a [pāṯîl](../../strongs/h/h6616.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md):

<a name="numbers_15_39"></a>Numbers 15:39

And it shall be unto you for a [ṣîṣiṯ](../../strongs/h/h6734.md), that ye may [ra'ah](../../strongs/h/h7200.md) upon it, and [zakar](../../strongs/h/h2142.md) all the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md), and ['asah](../../strongs/h/h6213.md) them; and that ye [tûr](../../strongs/h/h8446.md) not ['aḥar](../../strongs/h/h310.md) your own [lebab](../../strongs/h/h3824.md) and your own ['ayin](../../strongs/h/h5869.md), ['aḥar](../../strongs/h/h310.md) which ye use to [zānâ](../../strongs/h/h2181.md):

<a name="numbers_15_40"></a>Numbers 15:40

That ye may [zakar](../../strongs/h/h2142.md), and ['asah](../../strongs/h/h6213.md) all my [mitsvah](../../strongs/h/h4687.md), and be [qadowsh](../../strongs/h/h6918.md) unto your ['Elohiym](../../strongs/h/h430.md).

<a name="numbers_15_41"></a>Numbers 15:41

I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), which [yāṣā'](../../strongs/h/h3318.md) you out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), to be your ['Elohiym](../../strongs/h/h430.md): I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 14](numbers_14.md) - [Numbers 16](numbers_16.md)