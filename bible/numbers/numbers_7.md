# [Numbers 7](https://www.blueletterbible.org/kjv/num/7)

<a name="numbers_7_1"></a>Numbers 7:1

And it came to pass on the [yowm](../../strongs/h/h3117.md) that [Mōshe](../../strongs/h/h4872.md) had [kalah](../../strongs/h/h3615.md) [quwm](../../strongs/h/h6965.md) the [miškān](../../strongs/h/h4908.md), and had [māšaḥ](../../strongs/h/h4886.md) it, and [qadash](../../strongs/h/h6942.md) it, and all the [kĕliy](../../strongs/h/h3627.md) thereof, both the [mizbeach](../../strongs/h/h4196.md) and all the [kĕliy](../../strongs/h/h3627.md) thereof, and had [māšaḥ](../../strongs/h/h4886.md) them, and [qadash](../../strongs/h/h6942.md) them;

<a name="numbers_7_2"></a>Numbers 7:2

That the [nāśî'](../../strongs/h/h5387.md) of [Yisra'el](../../strongs/h/h3478.md), [ro'sh](../../strongs/h/h7218.md) of the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), who were the [nāśî'](../../strongs/h/h5387.md) of the [maṭṭê](../../strongs/h/h4294.md), and were ['amad](../../strongs/h/h5975.md) them that were [paqad](../../strongs/h/h6485.md), [qāraḇ](../../strongs/h/h7126.md):

<a name="numbers_7_3"></a>Numbers 7:3

And they [bow'](../../strongs/h/h935.md) their [qorban](../../strongs/h/h7133.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), six [ṣaḇ](../../strongs/h/h6632.md) [ʿăḡālâ](../../strongs/h/h5699.md), and twelve [bāqār](../../strongs/h/h1241.md); an [ʿăḡālâ](../../strongs/h/h5699.md) for two of the [nāśî'](../../strongs/h/h5387.md), and for each one a [showr](../../strongs/h/h7794.md): and they [qāraḇ](../../strongs/h/h7126.md) them [paniym](../../strongs/h/h6440.md) the [miškān](../../strongs/h/h4908.md).

<a name="numbers_7_4"></a>Numbers 7:4

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_7_5"></a>Numbers 7:5

[laqach](../../strongs/h/h3947.md) it of them, that they may be to ['abad](../../strongs/h/h5647.md) the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md); and thou shalt [nathan](../../strongs/h/h5414.md) them unto the [Lᵊvî](../../strongs/h/h3881.md), to every ['iysh](../../strongs/h/h376.md) [peh](../../strongs/h/h6310.md) to his [ʿăḇōḏâ](../../strongs/h/h5656.md).

<a name="numbers_7_6"></a>Numbers 7:6

And [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) the [ʿăḡālâ](../../strongs/h/h5699.md) and the [bāqār](../../strongs/h/h1241.md), and [nathan](../../strongs/h/h5414.md) them unto the [Lᵊvî](../../strongs/h/h3881.md).

<a name="numbers_7_7"></a>Numbers 7:7

Two [ʿăḡālâ](../../strongs/h/h5699.md) and four [bāqār](../../strongs/h/h1241.md) he [nathan](../../strongs/h/h5414.md) unto the [ben](../../strongs/h/h1121.md) of [Gēršôn](../../strongs/h/h1648.md), [peh](../../strongs/h/h6310.md) to their [ʿăḇōḏâ](../../strongs/h/h5656.md):

<a name="numbers_7_8"></a>Numbers 7:8

And four [ʿăḡālâ](../../strongs/h/h5699.md) and eight [bāqār](../../strongs/h/h1241.md) he [nathan](../../strongs/h/h5414.md) unto the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md), [peh](../../strongs/h/h6310.md) unto their [ʿăḇōḏâ](../../strongs/h/h5656.md), under the [yad](../../strongs/h/h3027.md) of ['Îṯāmār](../../strongs/h/h385.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md).

<a name="numbers_7_9"></a>Numbers 7:9

But unto the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md) he [nathan](../../strongs/h/h5414.md) [lō'](../../strongs/h/h3808.md): because the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [qodesh](../../strongs/h/h6944.md) belonging unto them was that they should [nasa'](../../strongs/h/h5375.md) upon their [kāṯēp̄](../../strongs/h/h3802.md).

<a name="numbers_7_10"></a>Numbers 7:10

And the [nāśî'](../../strongs/h/h5387.md) [qāraḇ](../../strongs/h/h7126.md) for [ḥănukâ](../../strongs/h/h2598.md) of the [mizbeach](../../strongs/h/h4196.md) in the [yowm](../../strongs/h/h3117.md) that it was [māšaḥ](../../strongs/h/h4886.md), even the [nāśî'](../../strongs/h/h5387.md) [qāraḇ](../../strongs/h/h7126.md) their [qorban](../../strongs/h/h7133.md) [paniym](../../strongs/h/h6440.md) the [mizbeach](../../strongs/h/h4196.md).

<a name="numbers_7_11"></a>Numbers 7:11

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), They shall [qāraḇ](../../strongs/h/h7126.md) their [qorban](../../strongs/h/h7133.md), each [nāśî'](../../strongs/h/h5387.md) on his [yowm](../../strongs/h/h3117.md), for the [ḥănukâ](../../strongs/h/h2598.md) of the [mizbeach](../../strongs/h/h4196.md).

<a name="numbers_7_12"></a>Numbers 7:12

And he that [qāraḇ](../../strongs/h/h7126.md) his [qorban](../../strongs/h/h7133.md) the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) was [Naḥšôn](../../strongs/h/h5177.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmînāḏāḇ](../../strongs/h/h5992.md), of the [maṭṭê](../../strongs/h/h4294.md) of [Yehuwdah](../../strongs/h/h3063.md):

<a name="numbers_7_13"></a>Numbers 7:13

And his [qorban](../../strongs/h/h7133.md) was one [keceph](../../strongs/h/h3701.md) [qᵊʿārâ](../../strongs/h/h7086.md), the [mišqāl](../../strongs/h/h4948.md) thereof was an hundred and thirty, one [keceph](../../strongs/h/h3701.md) [mizrāq](../../strongs/h/h4219.md) of seventy [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md); both of them were [mālē'](../../strongs/h/h4392.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md):

<a name="numbers_7_14"></a>Numbers 7:14

One [kaph](../../strongs/h/h3709.md) of ten [zāhāḇ](../../strongs/h/h2091.md), [mālē'](../../strongs/h/h4392.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md):

<a name="numbers_7_15"></a>Numbers 7:15

One [bāqār](../../strongs/h/h1241.md) [ben](../../strongs/h/h1121.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), one [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md), for an [ʿōlâ](../../strongs/h/h5930.md):

<a name="numbers_7_16"></a>Numbers 7:16

One [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md):

<a name="numbers_7_17"></a>Numbers 7:17

And for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), two [bāqār](../../strongs/h/h1241.md), five ['ayil](../../strongs/h/h352.md), five [ʿatûḏ](../../strongs/h/h6260.md), five [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md): this was the [qorban](../../strongs/h/h7133.md) of [Naḥšôn](../../strongs/h/h5177.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmînāḏāḇ](../../strongs/h/h5992.md).

<a name="numbers_7_18"></a>Numbers 7:18

On the second [yowm](../../strongs/h/h3117.md) [Nᵊṯan'ēl](../../strongs/h/h5417.md) the [ben](../../strongs/h/h1121.md) of [ṣûʿar](../../strongs/h/h6686.md), [nāśî'](../../strongs/h/h5387.md) of [Yiśśāśḵār](../../strongs/h/h3485.md), did [qāraḇ](../../strongs/h/h7126.md):

<a name="numbers_7_19"></a>Numbers 7:19

He [qāraḇ](../../strongs/h/h7126.md) for his [qorban](../../strongs/h/h7133.md) one [keceph](../../strongs/h/h3701.md) [qᵊʿārâ](../../strongs/h/h7086.md), the [mišqāl](../../strongs/h/h4948.md) whereof was an hundred and thirty, one [keceph](../../strongs/h/h3701.md) [mizrāq](../../strongs/h/h4219.md) of seventy [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md); both of them [mālē'](../../strongs/h/h4392.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md):

<a name="numbers_7_20"></a>Numbers 7:20

One [kaph](../../strongs/h/h3709.md) of [zāhāḇ](../../strongs/h/h2091.md) of ten, [mālē'](../../strongs/h/h4392.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md):

<a name="numbers_7_21"></a>Numbers 7:21

One [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), one [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md), for an [ʿōlâ](../../strongs/h/h5930.md):

<a name="numbers_7_22"></a>Numbers 7:22

One [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md):

<a name="numbers_7_23"></a>Numbers 7:23

And for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), two [bāqār](../../strongs/h/h1241.md), five ['ayil](../../strongs/h/h352.md), five [ʿatûḏ](../../strongs/h/h6260.md), five [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md): this was the [qorban](../../strongs/h/h7133.md) of [Nᵊṯan'ēl](../../strongs/h/h5417.md) the [ben](../../strongs/h/h1121.md) of [ṣûʿar](../../strongs/h/h6686.md).

<a name="numbers_7_24"></a>Numbers 7:24

On the third [yowm](../../strongs/h/h3117.md) ['Ĕlî'āḇ](../../strongs/h/h446.md) the [ben](../../strongs/h/h1121.md) of [ḥēlōn](../../strongs/h/h2497.md), [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md):

<a name="numbers_7_25"></a>Numbers 7:25

His [qorban](../../strongs/h/h7133.md) was one [keceph](../../strongs/h/h3701.md) [qᵊʿārâ](../../strongs/h/h7086.md), the [mišqāl](../../strongs/h/h4948.md) whereof was an hundred and thirty, one [keceph](../../strongs/h/h3701.md) [mizrāq](../../strongs/h/h4219.md) of seventy [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md); both of them [mālē'](../../strongs/h/h4392.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md):

<a name="numbers_7_26"></a>Numbers 7:26

One [zāhāḇ](../../strongs/h/h2091.md) [kaph](../../strongs/h/h3709.md) of ten, [mālē'](../../strongs/h/h4392.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md):

<a name="numbers_7_27"></a>Numbers 7:27

One [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), one [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md), for an [ʿōlâ](../../strongs/h/h5930.md):

<a name="numbers_7_28"></a>Numbers 7:28

One [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md):

<a name="numbers_7_29"></a>Numbers 7:29

And for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), two [bāqār](../../strongs/h/h1241.md), five ['ayil](../../strongs/h/h352.md), five [ʿatûḏ](../../strongs/h/h6260.md), five [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md): this was the [qorban](../../strongs/h/h7133.md) of ['Ĕlî'āḇ](../../strongs/h/h446.md) the [ben](../../strongs/h/h1121.md) of [ḥēlōn](../../strongs/h/h2497.md).

<a name="numbers_7_30"></a>Numbers 7:30

On the fourth [yowm](../../strongs/h/h3117.md) ['ĕlîṣûr](../../strongs/h/h468.md) the [ben](../../strongs/h/h1121.md) of [šᵊḏê'ûr](../../strongs/h/h7707.md), [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md):

<a name="numbers_7_31"></a>Numbers 7:31

His [qorban](../../strongs/h/h7133.md) was one [keceph](../../strongs/h/h3701.md) [qᵊʿārâ](../../strongs/h/h7086.md) of the [mišqāl](../../strongs/h/h4948.md) of an hundred and thirty, one [keceph](../../strongs/h/h3701.md) [mizrāq](../../strongs/h/h4219.md) of seventy [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md); both of them [mālē'](../../strongs/h/h4392.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md):

<a name="numbers_7_32"></a>Numbers 7:32

One [zāhāḇ](../../strongs/h/h2091.md) [kaph](../../strongs/h/h3709.md) of ten, [mālē'](../../strongs/h/h4392.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md):

<a name="numbers_7_33"></a>Numbers 7:33

One [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), one [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md), for an [ʿōlâ](../../strongs/h/h5930.md):

<a name="numbers_7_34"></a>Numbers 7:34

One [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md):

<a name="numbers_7_35"></a>Numbers 7:35

And for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), two [bāqār](../../strongs/h/h1241.md), five ['ayil](../../strongs/h/h352.md), five [ʿatûḏ](../../strongs/h/h6260.md), five [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md): this was the [qorban](../../strongs/h/h7133.md) of ['ĕlîṣûr](../../strongs/h/h468.md) the [ben](../../strongs/h/h1121.md) of [šᵊḏê'ûr](../../strongs/h/h7707.md).

<a name="numbers_7_36"></a>Numbers 7:36

On the fifth [yowm](../../strongs/h/h3117.md) [šᵊlumî'ēl](../../strongs/h/h8017.md) the [ben](../../strongs/h/h1121.md) of [ṣûrîšaday](../../strongs/h/h6701.md), [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md):

<a name="numbers_7_37"></a>Numbers 7:37

His [qorban](../../strongs/h/h7133.md) was one [keceph](../../strongs/h/h3701.md) [qᵊʿārâ](../../strongs/h/h7086.md), the [mišqāl](../../strongs/h/h4948.md) whereof was an hundred and thirty, one [keceph](../../strongs/h/h3701.md) [mizrāq](../../strongs/h/h4219.md) of seventy [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md); both of them [mālē'](../../strongs/h/h4392.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md):

<a name="numbers_7_38"></a>Numbers 7:38

One [zāhāḇ](../../strongs/h/h2091.md) [kaph](../../strongs/h/h3709.md) of ten, [mālē'](../../strongs/h/h4392.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md):

<a name="numbers_7_39"></a>Numbers 7:39

One [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), one [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md), for an [ʿōlâ](../../strongs/h/h5930.md):

<a name="numbers_7_40"></a>Numbers 7:40

One [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md):

<a name="numbers_7_41"></a>Numbers 7:41

And for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), two [bāqār](../../strongs/h/h1241.md), five ['ayil](../../strongs/h/h352.md), five [ʿatûḏ](../../strongs/h/h6260.md), five [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md): this was the [qorban](../../strongs/h/h7133.md) of [šᵊlumî'ēl](../../strongs/h/h8017.md) the [ben](../../strongs/h/h1121.md) of [ṣûrîšaday](../../strongs/h/h6701.md).

<a name="numbers_7_42"></a>Numbers 7:42

On the sixth [yowm](../../strongs/h/h3117.md) ['elyāsāp̄](../../strongs/h/h460.md) the [ben](../../strongs/h/h1121.md) of [dᵊʿû'ēl](../../strongs/h/h1845.md), [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md):

<a name="numbers_7_43"></a>Numbers 7:43

His [qorban](../../strongs/h/h7133.md) was one [keceph](../../strongs/h/h3701.md) [qᵊʿārâ](../../strongs/h/h7086.md) of the [mišqāl](../../strongs/h/h4948.md) of an hundred and thirty, a [keceph](../../strongs/h/h3701.md) [mizrāq](../../strongs/h/h4219.md) of seventy [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md); both of them [mālē'](../../strongs/h/h4392.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md):

<a name="numbers_7_44"></a>Numbers 7:44

One [zāhāḇ](../../strongs/h/h2091.md) [kaph](../../strongs/h/h3709.md) of ten, [mālē'](../../strongs/h/h4392.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md):

<a name="numbers_7_45"></a>Numbers 7:45

One [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), one [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md), for an [ʿōlâ](../../strongs/h/h5930.md):

<a name="numbers_7_46"></a>Numbers 7:46

One [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md):

<a name="numbers_7_47"></a>Numbers 7:47

And for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), two [bāqār](../../strongs/h/h1241.md), five ['ayil](../../strongs/h/h352.md), five [ʿatûḏ](../../strongs/h/h6260.md), five [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md): this was the [qorban](../../strongs/h/h7133.md) of ['elyāsāp̄](../../strongs/h/h460.md) the [ben](../../strongs/h/h1121.md) of [dᵊʿû'ēl](../../strongs/h/h1845.md).

<a name="numbers_7_48"></a>Numbers 7:48

On the seventh [yowm](../../strongs/h/h3117.md) ['Ĕlîšāmāʿ](../../strongs/h/h476.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmîhûḏ](../../strongs/h/h5989.md), [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md):

<a name="numbers_7_49"></a>Numbers 7:49

His [qorban](../../strongs/h/h7133.md) was one [keceph](../../strongs/h/h3701.md) [qᵊʿārâ](../../strongs/h/h7086.md), the [mišqāl](../../strongs/h/h4948.md) whereof was an hundred and thirty, one [keceph](../../strongs/h/h3701.md) [mizrāq](../../strongs/h/h4219.md) of seventy [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md); both of them [mālē'](../../strongs/h/h4392.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md):

<a name="numbers_7_50"></a>Numbers 7:50

One [zāhāḇ](../../strongs/h/h2091.md) [kaph](../../strongs/h/h3709.md) of ten, [mālē'](../../strongs/h/h4392.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md):

<a name="numbers_7_51"></a>Numbers 7:51

One [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), one [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md), for an [ʿōlâ](../../strongs/h/h5930.md):

<a name="numbers_7_52"></a>Numbers 7:52

One [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md):

<a name="numbers_7_53"></a>Numbers 7:53

And for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), two [bāqār](../../strongs/h/h1241.md), five ['ayil](../../strongs/h/h352.md), five [ʿatûḏ](../../strongs/h/h6260.md), five [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md): this was the [qorban](../../strongs/h/h7133.md) of ['Ĕlîšāmāʿ](../../strongs/h/h476.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmîhûḏ](../../strongs/h/h5989.md).

<a name="numbers_7_54"></a>Numbers 7:54

On the eighth [yowm](../../strongs/h/h3117.md) offered [gamlî'ēl](../../strongs/h/h1583.md) the [ben](../../strongs/h/h1121.md) of [pᵊḏâṣûr](../../strongs/h/h6301.md), [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md):

<a name="numbers_7_55"></a>Numbers 7:55

His [qorban](../../strongs/h/h7133.md) was one [keceph](../../strongs/h/h3701.md) [qᵊʿārâ](../../strongs/h/h7086.md) of the [mišqāl](../../strongs/h/h4948.md) of an hundred and thirty, one [keceph](../../strongs/h/h3701.md) [mizrāq](../../strongs/h/h4219.md) of seventy [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md); both of them [mālē'](../../strongs/h/h4392.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md):

<a name="numbers_7_56"></a>Numbers 7:56

One [zāhāḇ](../../strongs/h/h2091.md) [kaph](../../strongs/h/h3709.md) of ten, [mālē'](../../strongs/h/h4392.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md):

<a name="numbers_7_57"></a>Numbers 7:57

One [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), one [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md), for an [ʿōlâ](../../strongs/h/h5930.md):

<a name="numbers_7_58"></a>Numbers 7:58

One [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md):

<a name="numbers_7_59"></a>Numbers 7:59

And for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), two [bāqār](../../strongs/h/h1241.md), five ['ayil](../../strongs/h/h352.md), five [ʿatûḏ](../../strongs/h/h6260.md), five [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md): this was the [qorban](../../strongs/h/h7133.md) of [gamlî'ēl](../../strongs/h/h1583.md) the [ben](../../strongs/h/h1121.md) of [pᵊḏâṣûr](../../strongs/h/h6301.md).

<a name="numbers_7_60"></a>Numbers 7:60

On the ninth [yowm](../../strongs/h/h3117.md) ['ăḇîḏān](../../strongs/h/h27.md) the [ben](../../strongs/h/h1121.md) of [giḏʿōnî](../../strongs/h/h1441.md), [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md):

<a name="numbers_7_61"></a>Numbers 7:61

His [qorban](../../strongs/h/h7133.md) was one [keceph](../../strongs/h/h3701.md) [qᵊʿārâ](../../strongs/h/h7086.md), the [mišqāl](../../strongs/h/h4948.md) whereof was an hundred and thirty, one [keceph](../../strongs/h/h3701.md) [mizrāq](../../strongs/h/h4219.md) of seventy [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md); both of them [mālē'](../../strongs/h/h4392.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md):

<a name="numbers_7_62"></a>Numbers 7:62

One [zāhāḇ](../../strongs/h/h2091.md) [kaph](../../strongs/h/h3709.md) of ten, [mālē'](../../strongs/h/h4392.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md):

<a name="numbers_7_63"></a>Numbers 7:63

One [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), one [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md), for an [ʿōlâ](../../strongs/h/h5930.md):

<a name="numbers_7_64"></a>Numbers 7:64

One [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md):

<a name="numbers_7_65"></a>Numbers 7:65

And for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), two [bāqār](../../strongs/h/h1241.md), five ['ayil](../../strongs/h/h352.md), five [ʿatûḏ](../../strongs/h/h6260.md), five [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md): this was the [qorban](../../strongs/h/h7133.md) of ['ăḇîḏān](../../strongs/h/h27.md) the [ben](../../strongs/h/h1121.md) of [giḏʿōnî](../../strongs/h/h1441.md).

<a name="numbers_7_66"></a>Numbers 7:66

On the tenth [yowm](../../strongs/h/h3117.md) ['ăḥîʿezer](../../strongs/h/h295.md) the [ben](../../strongs/h/h1121.md) of [ʿammîšaḏay](../../strongs/h/h5996.md), [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md):

<a name="numbers_7_67"></a>Numbers 7:67

His [qorban](../../strongs/h/h7133.md) was one [keceph](../../strongs/h/h3701.md) [qᵊʿārâ](../../strongs/h/h7086.md), the [mišqāl](../../strongs/h/h4948.md) whereof was an hundred and thirty, one [keceph](../../strongs/h/h3701.md) [mizrāq](../../strongs/h/h4219.md) of seventy [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md); both of them [mālē'](../../strongs/h/h4392.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md):

<a name="numbers_7_68"></a>Numbers 7:68

One [zāhāḇ](../../strongs/h/h2091.md) [kaph](../../strongs/h/h3709.md) of ten, [mālē'](../../strongs/h/h4392.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md):

<a name="numbers_7_69"></a>Numbers 7:69

One [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), one [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md), for an [ʿōlâ](../../strongs/h/h5930.md):

<a name="numbers_7_70"></a>Numbers 7:70

One [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md):

<a name="numbers_7_71"></a>Numbers 7:71

And for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), two [bāqār](../../strongs/h/h1241.md), five ['ayil](../../strongs/h/h352.md), five [ʿatûḏ](../../strongs/h/h6260.md), five [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md): this was the [qorban](../../strongs/h/h7133.md) of ['ăḥîʿezer](../../strongs/h/h295.md) the [ben](../../strongs/h/h1121.md) of [ʿammîšaḏay](../../strongs/h/h5996.md).

<a name="numbers_7_72"></a>Numbers 7:72

On the eleventh [yowm](../../strongs/h/h3117.md) [Paḡʿî'ēl](../../strongs/h/h6295.md) the [ben](../../strongs/h/h1121.md) of [ʿāḵrān](../../strongs/h/h5918.md), [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of ['Āšēr](../../strongs/h/h836.md):

<a name="numbers_7_73"></a>Numbers 7:73

His [qorban](../../strongs/h/h7133.md) was one [keceph](../../strongs/h/h3701.md) [qᵊʿārâ](../../strongs/h/h7086.md), the [mišqāl](../../strongs/h/h4948.md) whereof was an hundred and thirty, one [keceph](../../strongs/h/h3701.md) [mizrāq](../../strongs/h/h4219.md) of seventy [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md); both of them [mālē'](../../strongs/h/h4392.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md):

<a name="numbers_7_74"></a>Numbers 7:74

One [zāhāḇ](../../strongs/h/h2091.md) [kaph](../../strongs/h/h3709.md) of ten, [mālē'](../../strongs/h/h4392.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md):

<a name="numbers_7_75"></a>Numbers 7:75

One [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), one [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md), for an [ʿōlâ](../../strongs/h/h5930.md):

<a name="numbers_7_76"></a>Numbers 7:76

One [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md):

<a name="numbers_7_77"></a>Numbers 7:77

And for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), two [bāqār](../../strongs/h/h1241.md), five ['ayil](../../strongs/h/h352.md), five [ʿatûḏ](../../strongs/h/h6260.md), five [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md): this was the [qorban](../../strongs/h/h7133.md) of [Paḡʿî'ēl](../../strongs/h/h6295.md) the [ben](../../strongs/h/h1121.md) of [ʿāḵrān](../../strongs/h/h5918.md).

<a name="numbers_7_78"></a>Numbers 7:78

On the twelfth [yowm](../../strongs/h/h3117.md) ['ăḥîraʿ](../../strongs/h/h299.md) the [ben](../../strongs/h/h1121.md) of [ʿênān](../../strongs/h/h5881.md), [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Nap̄tālî](../../strongs/h/h5321.md):

<a name="numbers_7_79"></a>Numbers 7:79

His [qorban](../../strongs/h/h7133.md) was one [keceph](../../strongs/h/h3701.md) [qᵊʿārâ](../../strongs/h/h7086.md), the [mišqāl](../../strongs/h/h4948.md) whereof was an hundred and thirty, one [keceph](../../strongs/h/h3701.md) [mizrāq](../../strongs/h/h4219.md) of seventy [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md); both of them [mālē'](../../strongs/h/h4392.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md):

<a name="numbers_7_80"></a>Numbers 7:80

One [zāhāḇ](../../strongs/h/h2091.md) [kaph](../../strongs/h/h3709.md) of ten, [mālē'](../../strongs/h/h4392.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md):

<a name="numbers_7_81"></a>Numbers 7:81

One [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), one [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md), for an [ʿōlâ](../../strongs/h/h5930.md):

<a name="numbers_7_82"></a>Numbers 7:82

One [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md):

<a name="numbers_7_83"></a>Numbers 7:83

And for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), two [bāqār](../../strongs/h/h1241.md), five ['ayil](../../strongs/h/h352.md), five [ʿatûḏ](../../strongs/h/h6260.md), five [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md): this was the [qorban](../../strongs/h/h7133.md) of ['ăḥîraʿ](../../strongs/h/h299.md) the [ben](../../strongs/h/h1121.md) of [ʿênān](../../strongs/h/h5881.md).

<a name="numbers_7_84"></a>Numbers 7:84

This was the [ḥănukâ](../../strongs/h/h2598.md) of the [mizbeach](../../strongs/h/h4196.md), in the [yowm](../../strongs/h/h3117.md) when it was [māšaḥ](../../strongs/h/h4886.md), by the [nāśî'](../../strongs/h/h5387.md) of [Yisra'el](../../strongs/h/h3478.md): twelve [qᵊʿārâ](../../strongs/h/h7086.md) of [keceph](../../strongs/h/h3701.md), twelve [keceph](../../strongs/h/h3701.md) [mizrāq](../../strongs/h/h4219.md), twelve [kaph](../../strongs/h/h3709.md) of [zāhāḇ](../../strongs/h/h2091.md):

<a name="numbers_7_85"></a>Numbers 7:85

Each [qᵊʿārâ](../../strongs/h/h7086.md) of [keceph](../../strongs/h/h3701.md) weighing an hundred and thirty, each [mizrāq](../../strongs/h/h4219.md) seventy: all the [keceph](../../strongs/h/h3701.md) [kĕliy](../../strongs/h/h3627.md) weighed two thousand and four hundred, after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md):

<a name="numbers_7_86"></a>Numbers 7:86

The [zāhāḇ](../../strongs/h/h2091.md) [kaph](../../strongs/h/h3709.md) were twelve, [mālē'](../../strongs/h/h4392.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md), weighing ten [kaph](../../strongs/h/h3709.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md): all the [zāhāḇ](../../strongs/h/h2091.md) of the [kaph](../../strongs/h/h3709.md) was an hundred and twenty.

<a name="numbers_7_87"></a>Numbers 7:87

All the [bāqār](../../strongs/h/h1241.md) for the an [ʿōlâ](../../strongs/h/h5930.md) were twelve [par](../../strongs/h/h6499.md), the ['ayil](../../strongs/h/h352.md) twelve, the [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) twelve, with their [minchah](../../strongs/h/h4503.md): and the [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for [chatta'ath](../../strongs/h/h2403.md) twelve.

<a name="numbers_7_88"></a>Numbers 7:88

And all the [bāqār](../../strongs/h/h1241.md) for the [zebach](../../strongs/h/h2077.md) of the [šelem](../../strongs/h/h8002.md) were twenty and four [par](../../strongs/h/h6499.md), the ['ayil](../../strongs/h/h352.md) sixty, the [ʿatûḏ](../../strongs/h/h6260.md) sixty, the [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) sixty. This was the [ḥănukâ](../../strongs/h/h2598.md) of the [mizbeach](../../strongs/h/h4196.md), ['aḥar](../../strongs/h/h310.md) that it was [māšaḥ](../../strongs/h/h4886.md).

<a name="numbers_7_89"></a>Numbers 7:89

And when [Mōshe](../../strongs/h/h4872.md) was [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) to [dabar](../../strongs/h/h1696.md) with him, then he [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of one [dabar](../../strongs/h/h1696.md) unto him from off the [kapōreṯ](../../strongs/h/h3727.md) that was upon the ['ārôn](../../strongs/h/h727.md) of [ʿēḏûṯ](../../strongs/h/h5715.md), from between the two [kĕruwb](../../strongs/h/h3742.md): and he [dabar](../../strongs/h/h1696.md) unto him.

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 6](numbers_6.md) - [Numbers 8](numbers_8.md)