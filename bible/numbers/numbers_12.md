# [Numbers 12](https://www.blueletterbible.org/kjv/num/12)

<a name="numbers_12_1"></a>Numbers 12:1

And [Miryām](../../strongs/h/h4813.md) and ['Ahărôn](../../strongs/h/h175.md) [dabar](../../strongs/h/h1696.md) against [Mōshe](../../strongs/h/h4872.md) because of the [kûšîṯ](../../strongs/h/h3571.md) ['ishshah](../../strongs/h/h802.md) whom he had [laqach](../../strongs/h/h3947.md): for he had [laqach](../../strongs/h/h3947.md) a [kûšîṯ](../../strongs/h/h3571.md) ['ishshah](../../strongs/h/h802.md).

<a name="numbers_12_2"></a>Numbers 12:2

And they ['āmar](../../strongs/h/h559.md), Hath [Yĕhovah](../../strongs/h/h3068.md) indeed [dabar](../../strongs/h/h1696.md) only by [Mōshe](../../strongs/h/h4872.md)? hath he not [dabar](../../strongs/h/h1696.md) also by us? And [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) it.

<a name="numbers_12_3"></a>Numbers 12:3

(Now the ['iysh](../../strongs/h/h376.md) [Mōshe](../../strongs/h/h4872.md) was [me'od](../../strongs/h/h3966.md) ['anav](../../strongs/h/h6035.md) ['anav](../../strongs/h/h6035.md), above all the ['adam](../../strongs/h/h120.md) which were upon the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md).)

<a name="numbers_12_4"></a>Numbers 12:4

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) [piṯ'ōm](../../strongs/h/h6597.md) unto [Mōshe](../../strongs/h/h4872.md), and unto ['Ahărôn](../../strongs/h/h175.md), and unto [Miryām](../../strongs/h/h4813.md), [yāṣā'](../../strongs/h/h3318.md) ye three unto the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md). And they three [yāṣā'](../../strongs/h/h3318.md).

<a name="numbers_12_5"></a>Numbers 12:5

And [Yĕhovah](../../strongs/h/h3068.md) [yarad](../../strongs/h/h3381.md) in the [ʿammûḏ](../../strongs/h/h5982.md) of the [ʿānān](../../strongs/h/h6051.md), and ['amad](../../strongs/h/h5975.md) in the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md), and [qara'](../../strongs/h/h7121.md) ['Ahărôn](../../strongs/h/h175.md) and [Miryām](../../strongs/h/h4813.md): and they both [yāṣā'](../../strongs/h/h3318.md).

<a name="numbers_12_6"></a>Numbers 12:6

And he ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) now my [dabar](../../strongs/h/h1697.md): If there be a [nāḇî'](../../strongs/h/h5030.md) among you, I [Yĕhovah](../../strongs/h/h3068.md) will [yada'](../../strongs/h/h3045.md) myself unto him in a [mar'â](../../strongs/h/h4759.md), and will [dabar](../../strongs/h/h1696.md) unto him in a [ḥălôm](../../strongs/h/h2472.md).

<a name="numbers_12_7"></a>Numbers 12:7

My ['ebed](../../strongs/h/h5650.md) [Mōshe](../../strongs/h/h4872.md) is not so, who is ['aman](../../strongs/h/h539.md) in all mine [bayith](../../strongs/h/h1004.md).

<a name="numbers_12_8"></a>Numbers 12:8

With him will I [dabar](../../strongs/h/h1696.md) [peh](../../strongs/h/h6310.md) to [peh](../../strongs/h/h6310.md), even [mar'ê](../../strongs/h/h4758.md), and not in [ḥîḏâ](../../strongs/h/h2420.md); and the [tĕmuwnah](../../strongs/h/h8544.md) of [Yĕhovah](../../strongs/h/h3068.md) shall he [nabat](../../strongs/h/h5027.md): wherefore then were ye not [yare'](../../strongs/h/h3372.md) to [dabar](../../strongs/h/h1696.md) against my ['ebed](../../strongs/h/h5650.md) [Mōshe](../../strongs/h/h4872.md)?

<a name="numbers_12_9"></a>Numbers 12:9

And the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against them; and he [yālaḵ](../../strongs/h/h3212.md).

<a name="numbers_12_10"></a>Numbers 12:10

And the [ʿānān](../../strongs/h/h6051.md) [cuwr](../../strongs/h/h5493.md) from off the ['ohel](../../strongs/h/h168.md); and, behold, [Miryām](../../strongs/h/h4813.md) became [ṣāraʿ](../../strongs/h/h6879.md), as [šeleḡ](../../strongs/h/h7950.md): and ['Ahărôn](../../strongs/h/h175.md) [panah](../../strongs/h/h6437.md) upon [Miryām](../../strongs/h/h4813.md), and, behold, she was [ṣāraʿ](../../strongs/h/h6879.md).

<a name="numbers_12_11"></a>Numbers 12:11

And ['Ahărôn](../../strongs/h/h175.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [bî](../../strongs/h/h994.md), my ['adown](../../strongs/h/h113.md), I beseech thee, [shiyth](../../strongs/h/h7896.md) not the [chatta'ath](../../strongs/h/h2403.md) upon us, wherein we have [yā'al](../../strongs/h/h2973.md), and wherein we have [chata'](../../strongs/h/h2398.md).

<a name="numbers_12_12"></a>Numbers 12:12

Let her not be as one [na'](../../strongs/h/h4994.md) [muwth](../../strongs/h/h4191.md), of whom the [basar](../../strongs/h/h1320.md) is [ḥēṣî](../../strongs/h/h2677.md) ['akal](../../strongs/h/h398.md) when he [yāṣā'](../../strongs/h/h3318.md) of his ['em](../../strongs/h/h517.md) [reḥem](../../strongs/h/h7358.md).

<a name="numbers_12_13"></a>Numbers 12:13

And [Mōshe](../../strongs/h/h4872.md) [ṣāʿaq](../../strongs/h/h6817.md) unto [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), [rapha'](../../strongs/h/h7495.md) her now, ['el](../../strongs/h/h410.md), I beseech thee.

<a name="numbers_12_14"></a>Numbers 12:14

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), If her ['ab](../../strongs/h/h1.md) had [yāraq](../../strongs/h/h3417.md) [yāraq](../../strongs/h/h3417.md) in her [paniym](../../strongs/h/h6440.md), should she not be [kālam](../../strongs/h/h3637.md) seven days? let her be [cagar](../../strongs/h/h5462.md) [ḥûṣ](../../strongs/h/h2351.md) from the [maḥănê](../../strongs/h/h4264.md) seven [yowm](../../strongs/h/h3117.md), and ['aḥar](../../strongs/h/h310.md) that let her be ['āsap̄](../../strongs/h/h622.md) in again.

<a name="numbers_12_15"></a>Numbers 12:15

And [Miryām](../../strongs/h/h4813.md) was [cagar](../../strongs/h/h5462.md) [ḥûṣ](../../strongs/h/h2351.md) from the [maḥănê](../../strongs/h/h4264.md) seven [yowm](../../strongs/h/h3117.md): and the ['am](../../strongs/h/h5971.md) [nāsaʿ](../../strongs/h/h5265.md) not till [Miryām](../../strongs/h/h4813.md) was ['āsap̄](../../strongs/h/h622.md) in again.

<a name="numbers_12_16"></a>Numbers 12:16

And ['aḥar](../../strongs/h/h310.md) the ['am](../../strongs/h/h5971.md) [nāsaʿ](../../strongs/h/h5265.md) from [Ḥăṣērôṯ](../../strongs/h/h2698.md), and [ḥānâ](../../strongs/h/h2583.md) in the [midbar](../../strongs/h/h4057.md) of [Pā'rān](../../strongs/h/h6290.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 11](numbers_11.md) - [Numbers 13](numbers_13.md)