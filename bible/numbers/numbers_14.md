# [Numbers 14](https://www.blueletterbible.org/kjv/num/14)

<a name="numbers_14_1"></a>Numbers 14:1

And all the ['edah](../../strongs/h/h5712.md) [nasa'](../../strongs/h/h5375.md) their [qowl](../../strongs/h/h6963.md), and [nathan](../../strongs/h/h5414.md); and the ['am](../../strongs/h/h5971.md) [bāḵâ](../../strongs/h/h1058.md) that [layil](../../strongs/h/h3915.md).

<a name="numbers_14_2"></a>Numbers 14:2

And all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [lûn](../../strongs/h/h3885.md) against [Mōshe](../../strongs/h/h4872.md) and against ['Ahărôn](../../strongs/h/h175.md): and the whole ['edah](../../strongs/h/h5712.md) ['āmar](../../strongs/h/h559.md) unto them, [lû'](../../strongs/h/h3863.md) that we had [muwth](../../strongs/h/h4191.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md)! or [lû'](../../strongs/h/h3863.md) we had [muwth](../../strongs/h/h4191.md) in this [midbar](../../strongs/h/h4057.md)!

<a name="numbers_14_3"></a>Numbers 14:3

And wherefore hath [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md) us unto this ['erets](../../strongs/h/h776.md), to [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md), that our ['ishshah](../../strongs/h/h802.md) and our [ṭap̄](../../strongs/h/h2945.md) should be a [baz](../../strongs/h/h957.md)? were it not [towb](../../strongs/h/h2896.md) for us to [shuwb](../../strongs/h/h7725.md) into [Mitsrayim](../../strongs/h/h4714.md)?

<a name="numbers_14_4"></a>Numbers 14:4

And they ['āmar](../../strongs/h/h559.md) ['iysh](../../strongs/h/h376.md) to ['ach](../../strongs/h/h251.md), Let us [nathan](../../strongs/h/h5414.md) a [ro'sh](../../strongs/h/h7218.md), and let us [shuwb](../../strongs/h/h7725.md) into [Mitsrayim](../../strongs/h/h4714.md).

<a name="numbers_14_5"></a>Numbers 14:5

Then [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [naphal](../../strongs/h/h5307.md) on their [paniym](../../strongs/h/h6440.md) [paniym](../../strongs/h/h6440.md) all the [qāhēl](../../strongs/h/h6951.md) of the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_14_6"></a>Numbers 14:6

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), and [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md), which were of them that [tûr](../../strongs/h/h8446.md) the ['erets](../../strongs/h/h776.md), [qāraʿ](../../strongs/h/h7167.md) their [beḡeḏ](../../strongs/h/h899.md):

<a name="numbers_14_7"></a>Numbers 14:7

And they ['āmar](../../strongs/h/h559.md) unto all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), The ['erets](../../strongs/h/h776.md), which we ['abar](../../strongs/h/h5674.md) to [tûr](../../strongs/h/h8446.md) it, is a [me'od](../../strongs/h/h3966.md) [me'od](../../strongs/h/h3966.md) [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md).

<a name="numbers_14_8"></a>Numbers 14:8

If [Yĕhovah](../../strongs/h/h3068.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) in us, then he will [bow'](../../strongs/h/h935.md) us into this ['erets](../../strongs/h/h776.md), and [nathan](../../strongs/h/h5414.md) it us; an ['erets](../../strongs/h/h776.md) which [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md).

<a name="numbers_14_9"></a>Numbers 14:9

Only [māraḏ](../../strongs/h/h4775.md) not ye against [Yĕhovah](../../strongs/h/h3068.md), neither [yare'](../../strongs/h/h3372.md) ye the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md); for they are [lechem](../../strongs/h/h3899.md) for us: their [ṣēl](../../strongs/h/h6738.md) is [cuwr](../../strongs/h/h5493.md) from them, and [Yĕhovah](../../strongs/h/h3068.md) is with us: [yare'](../../strongs/h/h3372.md) them not.

<a name="numbers_14_10"></a>Numbers 14:10

But all the ['edah](../../strongs/h/h5712.md) ['āmar](../../strongs/h/h559.md) [rāḡam](../../strongs/h/h7275.md) them with ['eben](../../strongs/h/h68.md). And the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) before all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_14_11"></a>Numbers 14:11

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), How long will this ['am](../../strongs/h/h5971.md) [na'ats](../../strongs/h/h5006.md) me?  and how long will it [lō'](../../strongs/h/h3808.md) they ['aman](../../strongs/h/h539.md) me, for all the ['ôṯ](../../strongs/h/h226.md) which I have ['asah](../../strongs/h/h6213.md) [qereḇ](../../strongs/h/h7130.md) them?

<a name="numbers_14_12"></a>Numbers 14:12

I will [nakah](../../strongs/h/h5221.md) them with the [deḇer](../../strongs/h/h1698.md), and [yarash](../../strongs/h/h3423.md) them, and will ['asah](../../strongs/h/h6213.md) of thee a [gadowl](../../strongs/h/h1419.md) [gowy](../../strongs/h/h1471.md) and ['atsuwm](../../strongs/h/h6099.md) than they.

<a name="numbers_14_13"></a>Numbers 14:13

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto [Yĕhovah](../../strongs/h/h3068.md), Then the [Mitsrayim](../../strongs/h/h4714.md) shall [shama'](../../strongs/h/h8085.md) it, (for thou [ʿālâ](../../strongs/h/h5927.md) this ['am](../../strongs/h/h5971.md) in thy [koach](../../strongs/h/h3581.md) from [qereḇ](../../strongs/h/h7130.md) them;)

<a name="numbers_14_14"></a>Numbers 14:14

And they will ['āmar](../../strongs/h/h559.md) it to the [yashab](../../strongs/h/h3427.md) of this ['erets](../../strongs/h/h776.md): for they have [shama'](../../strongs/h/h8085.md) that thou [Yĕhovah](../../strongs/h/h3068.md) art [qereḇ](../../strongs/h/h7130.md) this ['am](../../strongs/h/h5971.md), that thou [Yĕhovah](../../strongs/h/h3068.md) art [ra'ah](../../strongs/h/h7200.md) ['ayin](../../strongs/h/h5869.md) to ['ayin](../../strongs/h/h5869.md), and that thy [ʿānān](../../strongs/h/h6051.md) ['amad](../../strongs/h/h5975.md) over them, and that thou [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) them, by [yômām](../../strongs/h/h3119.md) in a [ʿammûḏ](../../strongs/h/h5982.md) of a [ʿānān](../../strongs/h/h6051.md), and in a [ʿammûḏ](../../strongs/h/h5982.md) of ['esh](../../strongs/h/h784.md) by [layil](../../strongs/h/h3915.md).

<a name="numbers_14_15"></a>Numbers 14:15

Now if thou shalt [muwth](../../strongs/h/h4191.md) all this ['am](../../strongs/h/h5971.md) as one ['iysh](../../strongs/h/h376.md), then the [gowy](../../strongs/h/h1471.md) which have [shama'](../../strongs/h/h8085.md) the [šēmaʿ](../../strongs/h/h8088.md) of thee will ['āmar](../../strongs/h/h559.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_14_16"></a>Numbers 14:16

Because [Yĕhovah](../../strongs/h/h3068.md) was [biltî](../../strongs/h/h1115.md) [yakol](../../strongs/h/h3201.md) to [bow'](../../strongs/h/h935.md) this ['am](../../strongs/h/h5971.md) into the ['erets](../../strongs/h/h776.md) which he [shaba'](../../strongs/h/h7650.md) unto them, therefore he hath [šāḥaṭ](../../strongs/h/h7819.md) them in the [midbar](../../strongs/h/h4057.md).

<a name="numbers_14_17"></a>Numbers 14:17

And now, I beseech thee, let the [koach](../../strongs/h/h3581.md) of my ['adonay](../../strongs/h/h136.md) be [gāḏal](../../strongs/h/h1431.md), ['ăšer](../../strongs/h/h834.md) as thou hast [dabar](../../strongs/h/h1696.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_14_18"></a>Numbers 14:18

[Yĕhovah](../../strongs/h/h3068.md) is ['ārēḵ](../../strongs/h/h750.md) ['aph](../../strongs/h/h639.md), and of [rab](../../strongs/h/h7227.md) [checed](../../strongs/h/h2617.md), [nasa'](../../strongs/h/h5375.md) ['avon](../../strongs/h/h5771.md) and [pesha'](../../strongs/h/h6588.md), and not [naqah](../../strongs/h/h5352.md) [naqah](../../strongs/h/h5352.md), [paqad](../../strongs/h/h6485.md) the ['avon](../../strongs/h/h5771.md) of the ['ab](../../strongs/h/h1.md) upon the [ben](../../strongs/h/h1121.md) unto the third and fourth.

<a name="numbers_14_19"></a>Numbers 14:19

[sālaḥ](../../strongs/h/h5545.md), I beseech thee, the ['avon](../../strongs/h/h5771.md) of this ['am](../../strongs/h/h5971.md) according unto the [gōḏel](../../strongs/h/h1433.md) of thy [checed](../../strongs/h/h2617.md), and as thou hast [nasa'](../../strongs/h/h5375.md) this ['am](../../strongs/h/h5971.md), from [Mitsrayim](../../strongs/h/h4714.md) even until [hēnnâ](../../strongs/h/h2008.md).

<a name="numbers_14_20"></a>Numbers 14:20

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), I have [sālaḥ](../../strongs/h/h5545.md) according to thy [dabar](../../strongs/h/h1697.md):

<a name="numbers_14_21"></a>Numbers 14:21

['ûlām](../../strongs/h/h199.md) as I [chay](../../strongs/h/h2416.md), all the ['erets](../../strongs/h/h776.md) shall be [mālā'](../../strongs/h/h4390.md) with the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_14_22"></a>Numbers 14:22

Because all those ['enowsh](../../strongs/h/h582.md) which have [ra'ah](../../strongs/h/h7200.md) my [kabowd](../../strongs/h/h3519.md), and my ['ôṯ](../../strongs/h/h226.md), which I ['asah](../../strongs/h/h6213.md) in [Mitsrayim](../../strongs/h/h4714.md) and in the [midbar](../../strongs/h/h4057.md), and have [nāsâ](../../strongs/h/h5254.md) me now these ten [pa'am](../../strongs/h/h6471.md), and have not [shama'](../../strongs/h/h8085.md) to my [qowl](../../strongs/h/h6963.md);

<a name="numbers_14_23"></a>Numbers 14:23

Surely they shall not [ra'ah](../../strongs/h/h7200.md) the ['erets](../../strongs/h/h776.md) which I [shaba'](../../strongs/h/h7650.md) unto their ['ab](../../strongs/h/h1.md), neither shall any of them that [na'ats](../../strongs/h/h5006.md) me [ra'ah](../../strongs/h/h7200.md) it:

<a name="numbers_14_24"></a>Numbers 14:24

But my ['ebed](../../strongs/h/h5650.md) [Kālēḇ](../../strongs/h/h3612.md), because he had ['aḥēr](../../strongs/h/h312.md) [ruwach](../../strongs/h/h7307.md) with him, and hath ['aḥar](../../strongs/h/h310.md) me [mālā'](../../strongs/h/h4390.md), him will I [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) whereinto he [bow'](../../strongs/h/h935.md); and his [zera'](../../strongs/h/h2233.md) shall [yarash](../../strongs/h/h3423.md) it.

<a name="numbers_14_25"></a>Numbers 14:25

(Now the [ʿămālēqî](../../strongs/h/h6003.md) and the [Kᵊnaʿănî](../../strongs/h/h3669.md) [yashab](../../strongs/h/h3427.md) in the [ʿēmeq](../../strongs/h/h6010.md).) [māḥār](../../strongs/h/h4279.md) [panah](../../strongs/h/h6437.md) you, and [nāsaʿ](../../strongs/h/h5265.md) you into the [midbar](../../strongs/h/h4057.md) by the [derek](../../strongs/h/h1870.md) of the [Sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md).

<a name="numbers_14_26"></a>Numbers 14:26

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) and unto ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_14_27"></a>Numbers 14:27

How long this [ra'](../../strongs/h/h7451.md) ['edah](../../strongs/h/h5712.md), which [lûn](../../strongs/h/h3885.md) against me? I have [shama'](../../strongs/h/h8085.md) the [tᵊlûnāṯ](../../strongs/h/h8519.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which they [lûn](../../strongs/h/h3885.md) against me.

<a name="numbers_14_28"></a>Numbers 14:28

['āmar](../../strongs/h/h559.md) unto them, [lō'](../../strongs/h/h3808.md) I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), as ye have [dabar](../../strongs/h/h1696.md) in mine ['ozen](../../strongs/h/h241.md), so will I ['asah](../../strongs/h/h6213.md) to you:

<a name="numbers_14_29"></a>Numbers 14:29

Your [peḡer](../../strongs/h/h6297.md) shall [naphal](../../strongs/h/h5307.md) in this [midbar](../../strongs/h/h4057.md); and all that were [paqad](../../strongs/h/h6485.md) of you, according to your [mispār](../../strongs/h/h4557.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md) which have [lûn](../../strongs/h/h3885.md) against me.

<a name="numbers_14_30"></a>Numbers 14:30

Doubtless ye shall not [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md), concerning which I [yad](../../strongs/h/h3027.md) [nasa'](../../strongs/h/h5375.md) to make you [shakan](../../strongs/h/h7931.md) therein, save [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md), and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md).

<a name="numbers_14_31"></a>Numbers 14:31

But your [ṭap̄](../../strongs/h/h2945.md), which ye ['āmar](../../strongs/h/h559.md) should be a [baz](../../strongs/h/h957.md), them will I [bow'](../../strongs/h/h935.md) in, and they shall [yada'](../../strongs/h/h3045.md) the ['erets](../../strongs/h/h776.md) which ye have [mā'as](../../strongs/h/h3988.md).

<a name="numbers_14_32"></a>Numbers 14:32

But as for you, your [peḡer](../../strongs/h/h6297.md), they shall [naphal](../../strongs/h/h5307.md) in this [midbar](../../strongs/h/h4057.md).

<a name="numbers_14_33"></a>Numbers 14:33

And your [ben](../../strongs/h/h1121.md) shall [ra'ah](../../strongs/h/h7462.md) in the [midbar](../../strongs/h/h4057.md) forty [šānâ](../../strongs/h/h8141.md), and [nasa'](../../strongs/h/h5375.md) your [zᵊnûṯ](../../strongs/h/h2184.md), until your [peḡer](../../strongs/h/h6297.md) be [tamam](../../strongs/h/h8552.md) in the [midbar](../../strongs/h/h4057.md).

<a name="numbers_14_34"></a>Numbers 14:34

After the [mispār](../../strongs/h/h4557.md) of the [yowm](../../strongs/h/h3117.md) in which ye [tûr](../../strongs/h/h8446.md) the ['erets](../../strongs/h/h776.md), even forty [yowm](../../strongs/h/h3117.md), each [yowm](../../strongs/h/h3117.md) for a [šānâ](../../strongs/h/h8141.md), shall ye [nasa'](../../strongs/h/h5375.md) your ['avon](../../strongs/h/h5771.md), even forty [šānâ](../../strongs/h/h8141.md), and ye shall [yada'](../../strongs/h/h3045.md) my [tᵊnû'â](../../strongs/h/h8569.md).

<a name="numbers_14_35"></a>Numbers 14:35

I [Yĕhovah](../../strongs/h/h3068.md) have [dabar](../../strongs/h/h1696.md), I will surely ['asah](../../strongs/h/h6213.md) it unto all this [ra'](../../strongs/h/h7451.md) ['edah](../../strongs/h/h5712.md), that are [yāʿaḏ](../../strongs/h/h3259.md) against me: in this [midbar](../../strongs/h/h4057.md) they shall be [tamam](../../strongs/h/h8552.md), and there they shall [muwth](../../strongs/h/h4191.md).

<a name="numbers_14_36"></a>Numbers 14:36

And the ['enowsh](../../strongs/h/h582.md), which [Mōshe](../../strongs/h/h4872.md) [shalach](../../strongs/h/h7971.md) to [tûr](../../strongs/h/h8446.md) the ['erets](../../strongs/h/h776.md), who [shuwb](../../strongs/h/h7725.md), and made all the ['edah](../../strongs/h/h5712.md) to [lûn](../../strongs/h/h3885.md) [lûn](../../strongs/h/h3885.md) against him, by [yāṣā'](../../strongs/h/h3318.md) a [dibâ](../../strongs/h/h1681.md) upon the ['erets](../../strongs/h/h776.md),

<a name="numbers_14_37"></a>Numbers 14:37

Even ['enowsh](../../strongs/h/h582.md) ['enowsh](../../strongs/h/h582.md) that did [yāṣā'](../../strongs/h/h3318.md) the [ra'](../../strongs/h/h7451.md) [dibâ](../../strongs/h/h1681.md) upon the ['erets](../../strongs/h/h776.md), [muwth](../../strongs/h/h4191.md) by the [magēp̄â](../../strongs/h/h4046.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_14_38"></a>Numbers 14:38

But [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), and [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md), which were of the ['enowsh](../../strongs/h/h582.md) that [halak](../../strongs/h/h1980.md) to [tûr](../../strongs/h/h8446.md) the ['erets](../../strongs/h/h776.md), [ḥāyâ](../../strongs/h/h2421.md) still.

<a name="numbers_14_39"></a>Numbers 14:39

And [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) these [dabar](../../strongs/h/h1697.md) unto all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): and the ['am](../../strongs/h/h5971.md) ['āḇal](../../strongs/h/h56.md) [me'od](../../strongs/h/h3966.md).

<a name="numbers_14_40"></a>Numbers 14:40

And they [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [ʿālâ](../../strongs/h/h5927.md) them up into the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md), ['āmar](../../strongs/h/h559.md), [hinneh](../../strongs/h/h2009.md), we be here, and will [ʿālâ](../../strongs/h/h5927.md) unto the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) hath ['āmar](../../strongs/h/h559.md): for we have [chata'](../../strongs/h/h2398.md).

<a name="numbers_14_41"></a>Numbers 14:41

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), Wherefore now do ye ['abar](../../strongs/h/h5674.md) the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md)? but it shall not [tsalach](../../strongs/h/h6743.md).

<a name="numbers_14_42"></a>Numbers 14:42

[ʿālâ](../../strongs/h/h5927.md) not, for [Yĕhovah](../../strongs/h/h3068.md) is not [qereḇ](../../strongs/h/h7130.md) you; that ye be not [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) your ['oyeb](../../strongs/h/h341.md).

<a name="numbers_14_43"></a>Numbers 14:43

For the [ʿămālēqî](../../strongs/h/h6003.md) and the [Kᵊnaʿănî](../../strongs/h/h3669.md) are there [paniym](../../strongs/h/h6440.md) you, and ye shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md): because ye are [shuwb](../../strongs/h/h7725.md) ['aḥar](../../strongs/h/h310.md) from [Yĕhovah](../../strongs/h/h3068.md), therefore [Yĕhovah](../../strongs/h/h3068.md) will not be with you.

<a name="numbers_14_44"></a>Numbers 14:44

But they [ʿāp̄al](../../strongs/h/h6075.md) to [ʿālâ](../../strongs/h/h5927.md) unto the [har](../../strongs/h/h2022.md) [ro'sh](../../strongs/h/h7218.md): nevertheless the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md), and [Mōshe](../../strongs/h/h4872.md), [mûš](../../strongs/h/h4185.md) not [qereḇ](../../strongs/h/h7130.md) of the [maḥănê](../../strongs/h/h4264.md).

<a name="numbers_14_45"></a>Numbers 14:45

Then the [ʿămālēqî](../../strongs/h/h6003.md) [yarad](../../strongs/h/h3381.md), and the [Kᵊnaʿănî](../../strongs/h/h3669.md) which [yashab](../../strongs/h/h3427.md) in that [har](../../strongs/h/h2022.md), and [nakah](../../strongs/h/h5221.md) them, and [kāṯaṯ](../../strongs/h/h3807.md) them, even unto [Ḥārmâ](../../strongs/h/h2767.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 13](numbers_13.md) - [Numbers 15](numbers_15.md)