# [Numbers 24](https://www.blueletterbible.org/kjv/num/24)

<a name="numbers_24_1"></a>Numbers 24:1

And when [Bilʿām](../../strongs/h/h1109.md) [ra'ah](../../strongs/h/h7200.md) that it ['ayin](../../strongs/h/h5869.md) [ṭôḇ](../../strongs/h/h2895.md) [Yĕhovah](../../strongs/h/h3068.md) to [barak](../../strongs/h/h1288.md) [Yisra'el](../../strongs/h/h3478.md), he [halak](../../strongs/h/h1980.md) not, as at other [pa'am](../../strongs/h/h6471.md), to [qārā'](../../strongs/h/h7125.md) for [naḥaš](../../strongs/h/h5173.md), but he [shiyth](../../strongs/h/h7896.md) his [paniym](../../strongs/h/h6440.md) toward the [midbar](../../strongs/h/h4057.md).

<a name="numbers_24_2"></a>Numbers 24:2

And [Bilʿām](../../strongs/h/h1109.md) [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md), and he [ra'ah](../../strongs/h/h7200.md) [Yisra'el](../../strongs/h/h3478.md) [shakan](../../strongs/h/h7931.md) according to their [shebet](../../strongs/h/h7626.md); and the [ruwach](../../strongs/h/h7307.md) of ['Elohiym](../../strongs/h/h430.md) came upon him.

<a name="numbers_24_3"></a>Numbers 24:3

And he [nasa'](../../strongs/h/h5375.md) his [māšāl](../../strongs/h/h4912.md), and ['āmar](../../strongs/h/h559.md), [Bilʿām](../../strongs/h/h1109.md) the [ben](../../strongs/h/h1121.md) of [Bᵊʿôr](../../strongs/h/h1160.md) hath [nᵊ'um](../../strongs/h/h5002.md), and the [geḇer](../../strongs/h/h1397.md) whose ['ayin](../../strongs/h/h5869.md) are [šāṯam](../../strongs/h/h8365.md) hath [nᵊ'um](../../strongs/h/h5002.md):

<a name="numbers_24_4"></a>Numbers 24:4

He hath [nᵊ'um](../../strongs/h/h5002.md), which [shama'](../../strongs/h/h8085.md) the ['emer](../../strongs/h/h561.md) of ['el](../../strongs/h/h410.md), which [chazah](../../strongs/h/h2372.md) the [maḥăzê](../../strongs/h/h4236.md) of [Šaday](../../strongs/h/h7706.md), [naphal](../../strongs/h/h5307.md), but having his ['ayin](../../strongs/h/h5869.md) [gālâ](../../strongs/h/h1540.md):

<a name="numbers_24_5"></a>Numbers 24:5

How [ṭôḇ](../../strongs/h/h2895.md) are thy ['ohel](../../strongs/h/h168.md), O [Ya'aqob](../../strongs/h/h3290.md), and thy [miškān](../../strongs/h/h4908.md), O [Yisra'el](../../strongs/h/h3478.md)!

<a name="numbers_24_6"></a>Numbers 24:6

As the [nachal](../../strongs/h/h5158.md) are they [natah](../../strongs/h/h5186.md), as [gannâ](../../strongs/h/h1593.md) by the river[nāhār](../../strongs/h/h5104.md), as the ['ăhālîm](../../strongs/h/h174.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [nāṭaʿ](../../strongs/h/h5193.md), and as ['erez](../../strongs/h/h730.md) beside the [mayim](../../strongs/h/h4325.md).

<a name="numbers_24_7"></a>Numbers 24:7

He shall [nāzal](../../strongs/h/h5140.md) the [mayim](../../strongs/h/h4325.md) of his [dᵊlî](../../strongs/h/h1805.md), and his [zera'](../../strongs/h/h2233.md) shall be in [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md), and his [melek](../../strongs/h/h4428.md) shall be [ruwm](../../strongs/h/h7311.md) than ['Ăḡaḡ](../../strongs/h/h90.md), and his [malkuwth](../../strongs/h/h4438.md) shall be [nasa'](../../strongs/h/h5375.md).

<a name="numbers_24_8"></a>Numbers 24:8

['el](../../strongs/h/h410.md) [yāṣā'](../../strongs/h/h3318.md) him out of [Mitsrayim](../../strongs/h/h4714.md); he hath as it were the [tôʿāp̄ôṯ](../../strongs/h/h8443.md) of a [rᵊ'ēm](../../strongs/h/h7214.md): he shall ['akal](../../strongs/h/h398.md) the [gowy](../../strongs/h/h1471.md) his [tsar](../../strongs/h/h6862.md), and shall [gāram](../../strongs/h/h1633.md) their ['etsem](../../strongs/h/h6106.md), and [māḥaṣ](../../strongs/h/h4272.md) them through with his [chets](../../strongs/h/h2671.md).

<a name="numbers_24_9"></a>Numbers 24:9

He [kara'](../../strongs/h/h3766.md), he [shakab](../../strongs/h/h7901.md) as an ['ariy](../../strongs/h/h738.md), and as a [lāḇî'](../../strongs/h/h3833.md): who shall [quwm](../../strongs/h/h6965.md) him? [barak](../../strongs/h/h1288.md) is he that [barak](../../strongs/h/h1288.md) thee, and ['arar](../../strongs/h/h779.md) is he that ['arar](../../strongs/h/h779.md) thee.

<a name="numbers_24_10"></a>Numbers 24:10

And [Bālāq](../../strongs/h/h1111.md) ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md) against [Bilʿām](../../strongs/h/h1109.md), and he [sāp̄aq](../../strongs/h/h5606.md) his [kaph](../../strongs/h/h3709.md): and [Bālāq](../../strongs/h/h1111.md) ['āmar](../../strongs/h/h559.md) unto [Bilʿām](../../strongs/h/h1109.md), I [qara'](../../strongs/h/h7121.md) thee to [qāḇaḇ](../../strongs/h/h6895.md) mine ['oyeb](../../strongs/h/h341.md), and, behold, thou hast [barak](../../strongs/h/h1288.md) [barak](../../strongs/h/h1288.md) them these three [pa'am](../../strongs/h/h6471.md).

<a name="numbers_24_11"></a>Numbers 24:11

Therefore now [bāraḥ](../../strongs/h/h1272.md) thou to thy [maqowm](../../strongs/h/h4725.md): I ['āmar](../../strongs/h/h559.md) to [kabad](../../strongs/h/h3513.md) thee unto [kabad](../../strongs/h/h3513.md) [kabad](../../strongs/h/h3513.md); but, lo, [Yĕhovah](../../strongs/h/h3068.md) hath [mānaʿ](../../strongs/h/h4513.md) thee from [kabowd](../../strongs/h/h3519.md).

<a name="numbers_24_12"></a>Numbers 24:12

And [Bilʿām](../../strongs/h/h1109.md) ['āmar](../../strongs/h/h559.md) unto [Bālāq](../../strongs/h/h1111.md), [dabar](../../strongs/h/h1696.md) I not also to thy [mal'ak](../../strongs/h/h4397.md) which thou [shalach](../../strongs/h/h7971.md) unto me, ['āmar](../../strongs/h/h559.md),

<a name="numbers_24_13"></a>Numbers 24:13

If [Bālāq](../../strongs/h/h1111.md) would [nathan](../../strongs/h/h5414.md) me his [bayith](../../strongs/h/h1004.md) [mᵊlō'](../../strongs/h/h4393.md) of [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md), I [yakol](../../strongs/h/h3201.md) ['abar](../../strongs/h/h5674.md) the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md), to ['asah](../../strongs/h/h6213.md) either [towb](../../strongs/h/h2896.md) or [ra'](../../strongs/h/h7451.md) of mine own [leb](../../strongs/h/h3820.md); but what [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md), that will I [dabar](../../strongs/h/h1696.md)?

<a name="numbers_24_14"></a>Numbers 24:14

And now, behold, I [halak](../../strongs/h/h1980.md) unto my ['am](../../strongs/h/h5971.md): [yālaḵ](../../strongs/h/h3212.md) therefore, and I will [ya'ats](../../strongs/h/h3289.md) thee what this ['am](../../strongs/h/h5971.md) shall ['asah](../../strongs/h/h6213.md) to thy ['am](../../strongs/h/h5971.md) in the ['aḥărîṯ](../../strongs/h/h319.md) [yowm](../../strongs/h/h3117.md).

<a name="numbers_24_15"></a>Numbers 24:15

And he [nasa'](../../strongs/h/h5375.md) his [māšāl](../../strongs/h/h4912.md), and ['āmar](../../strongs/h/h559.md), [Bilʿām](../../strongs/h/h1109.md) the [ben](../../strongs/h/h1121.md) of [Bᵊʿôr](../../strongs/h/h1160.md) hath [nᵊ'um](../../strongs/h/h5002.md), and the [geḇer](../../strongs/h/h1397.md) whose ['ayin](../../strongs/h/h5869.md) are [šāṯam](../../strongs/h/h8365.md) hath [nᵊ'um](../../strongs/h/h5002.md):

<a name="numbers_24_16"></a>Numbers 24:16

He hath [nᵊ'um](../../strongs/h/h5002.md), which [shama'](../../strongs/h/h8085.md) the ['emer](../../strongs/h/h561.md) of ['el](../../strongs/h/h410.md), and [yada'](../../strongs/h/h3045.md) the [da'ath](../../strongs/h/h1847.md) of the ['elyown](../../strongs/h/h5945.md), which [chazah](../../strongs/h/h2372.md) the [maḥăzê](../../strongs/h/h4236.md) of the [Šaday](../../strongs/h/h7706.md), [naphal](../../strongs/h/h5307.md), but having his ['ayin](../../strongs/h/h5869.md) [gālâ](../../strongs/h/h1540.md):

<a name="numbers_24_17"></a>Numbers 24:17

I shall [ra'ah](../../strongs/h/h7200.md) him, but not now: I shall [šûr](../../strongs/h/h7789.md) him, but not [qarowb](../../strongs/h/h7138.md): there shall [dāraḵ](../../strongs/h/h1869.md) a [kowkab](../../strongs/h/h3556.md) out of [Ya'aqob](../../strongs/h/h3290.md), and a [shebet](../../strongs/h/h7626.md) shall [quwm](../../strongs/h/h6965.md) out of [Yisra'el](../../strongs/h/h3478.md), and shall [māḥaṣ](../../strongs/h/h4272.md) the [pē'â](../../strongs/h/h6285.md) of [Mô'āḇ](../../strongs/h/h4124.md), and [qûr](../../strongs/h/h6979.md) all the [ben](../../strongs/h/h1121.md) of [Šēṯ](../../strongs/h/h8352.md) [Šēṯ](../../strongs/h/h8351.md).

<a name="numbers_24_18"></a>Numbers 24:18

And ['Ĕḏōm](../../strongs/h/h123.md) shall be a [yᵊrēšâ](../../strongs/h/h3424.md), [Śēʿîr](../../strongs/h/h8165.md) also shall be a [yᵊrēšâ](../../strongs/h/h3424.md) for his ['oyeb](../../strongs/h/h341.md); and [Yisra'el](../../strongs/h/h3478.md) shall ['asah](../../strongs/h/h6213.md) [ḥayil](../../strongs/h/h2428.md).

<a name="numbers_24_19"></a>Numbers 24:19

Out of [Ya'aqob](../../strongs/h/h3290.md) shall come he that shall have [radah](../../strongs/h/h7287.md), and shall ['abad](../../strongs/h/h6.md) him that [śārîḏ](../../strongs/h/h8300.md) of the [ʿîr](../../strongs/h/h5892.md).

<a name="numbers_24_20"></a>Numbers 24:20

And when he [ra'ah](../../strongs/h/h7200.md) on [ʿĂmālēq](../../strongs/h/h6002.md), he [nasa'](../../strongs/h/h5375.md) his [māšāl](../../strongs/h/h4912.md), and ['āmar](../../strongs/h/h559.md), [ʿĂmālēq](../../strongs/h/h6002.md) was the [re'shiyth](../../strongs/h/h7225.md) of the [gowy](../../strongs/h/h1471.md); but his ['aḥărîṯ](../../strongs/h/h319.md) shall be that he ['ōḇēḏ](../../strongs/h/h8.md) [ʿaḏ](../../strongs/h/h5703.md).

<a name="numbers_24_21"></a>Numbers 24:21

And he [ra'ah](../../strongs/h/h7200.md) on the [Qênî](../../strongs/h/h7017.md), and [nasa'](../../strongs/h/h5375.md) his [māšāl](../../strongs/h/h4912.md), and ['āmar](../../strongs/h/h559.md), ['êṯān](../../strongs/h/h386.md) is thy [môšāḇ](../../strongs/h/h4186.md), and thou [śûm](../../strongs/h/h7760.md) thy [qēn](../../strongs/h/h7064.md) in a [cela'](../../strongs/h/h5553.md).

<a name="numbers_24_22"></a>Numbers 24:22

Nevertheless the [Qayin](../../strongs/h/h7014.md) shall be [bāʿar](../../strongs/h/h1197.md), until ['Aššûr](../../strongs/h/h804.md) shall [šāḇâ](../../strongs/h/h7617.md) thee.

<a name="numbers_24_23"></a>Numbers 24:23

And he [nasa'](../../strongs/h/h5375.md) his [māšāl](../../strongs/h/h4912.md), and ['āmar](../../strongs/h/h559.md), ['owy](../../strongs/h/h188.md), who shall [ḥāyâ](../../strongs/h/h2421.md) when ['el](../../strongs/h/h410.md) [śûm](../../strongs/h/h7760.md) this!

<a name="numbers_24_24"></a>Numbers 24:24

And [ṣî](../../strongs/h/h6716.md) shall come from the [yad](../../strongs/h/h3027.md) of [Kitîm](../../strongs/h/h3794.md), and shall [ʿānâ](../../strongs/h/h6031.md) ['Aššûr](../../strongs/h/h804.md), and shall [ʿānâ](../../strongs/h/h6031.md) [ʿēḇer](../../strongs/h/h5677.md), and he also shall ['ōḇēḏ](../../strongs/h/h8.md) [ʿaḏ](../../strongs/h/h5703.md).

<a name="numbers_24_25"></a>Numbers 24:25

And [Bilʿām](../../strongs/h/h1109.md) [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) and [shuwb](../../strongs/h/h7725.md) to his [maqowm](../../strongs/h/h4725.md): and [Bālāq](../../strongs/h/h1111.md) also [halak](../../strongs/h/h1980.md) his [derek](../../strongs/h/h1870.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 23](numbers_23.md) - [Numbers 25](numbers_25.md)