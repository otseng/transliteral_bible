# [Numbers 5](https://www.blueletterbible.org/kjv/num/5)

<a name="numbers_5_1"></a>Numbers 5:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_5_2"></a>Numbers 5:2

[tsavah](../../strongs/h/h6680.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they [shalach](../../strongs/h/h7971.md) of the [maḥănê](../../strongs/h/h4264.md) every [ṣāraʿ](../../strongs/h/h6879.md), and every one that [zûḇ](../../strongs/h/h2100.md), and whosoever is [tame'](../../strongs/h/h2931.md) by the [nephesh](../../strongs/h/h5315.md):

<a name="numbers_5_3"></a>Numbers 5:3

Both [zāḵār](../../strongs/h/h2145.md) and [nᵊqēḇâ](../../strongs/h/h5347.md) shall ye [shalach](../../strongs/h/h7971.md), [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md) shall ye [shalach](../../strongs/h/h7971.md) them; that they [ṭāmē'](../../strongs/h/h2930.md) not their [maḥănê](../../strongs/h/h4264.md), in the [tavek](../../strongs/h/h8432.md) whereof I [shakan](../../strongs/h/h7931.md).

<a name="numbers_5_4"></a>Numbers 5:4

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md), and [shalach](../../strongs/h/h7971.md) them [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md): as [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['asah](../../strongs/h/h6213.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_5_5"></a>Numbers 5:5

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_5_6"></a>Numbers 5:6

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), When an ['iysh](../../strongs/h/h376.md) or ['ishshah](../../strongs/h/h802.md) shall ['asah](../../strongs/h/h6213.md) any [chatta'ath](../../strongs/h/h2403.md) that ['adam](../../strongs/h/h120.md) [māʿal](../../strongs/h/h4603.md), to do a [maʿal](../../strongs/h/h4604.md) against [Yĕhovah](../../strongs/h/h3068.md), and that [nephesh](../../strongs/h/h5315.md) be ['asham](../../strongs/h/h816.md);

<a name="numbers_5_7"></a>Numbers 5:7

Then they shall [yadah](../../strongs/h/h3034.md) their [chatta'ath](../../strongs/h/h2403.md) which they have ['asah](../../strongs/h/h6213.md): and he shall [shuwb](../../strongs/h/h7725.md) his ['āšām](../../strongs/h/h817.md) with the [ro'sh](../../strongs/h/h7218.md) thereof, and [yāsap̄](../../strongs/h/h3254.md) unto it the fifth part thereof, and [nathan](../../strongs/h/h5414.md) it unto him against whom he hath ['asham](../../strongs/h/h816.md).

<a name="numbers_5_8"></a>Numbers 5:8

But if the ['iysh](../../strongs/h/h376.md) have no [gā'al](../../strongs/h/h1350.md) to [shuwb](../../strongs/h/h7725.md) the ['āšām](../../strongs/h/h817.md) unto, let the ['āšām](../../strongs/h/h817.md) be [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md), even to the [kōhēn](../../strongs/h/h3548.md); beside the ['ayil](../../strongs/h/h352.md) of the [kipur](../../strongs/h/h3725.md), whereby shall [kāp̄ar](../../strongs/h/h3722.md) for him.

<a name="numbers_5_9"></a>Numbers 5:9

And every [tᵊrûmâ](../../strongs/h/h8641.md) of all the [qodesh](../../strongs/h/h6944.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which they [qāraḇ](../../strongs/h/h7126.md) unto the [kōhēn](../../strongs/h/h3548.md), shall be his.

<a name="numbers_5_10"></a>Numbers 5:10

And every ['iysh](../../strongs/h/h376.md) [qodesh](../../strongs/h/h6944.md) shall be his: whatsoever any ['iysh](../../strongs/h/h376.md) [nathan](../../strongs/h/h5414.md) the [kōhēn](../../strongs/h/h3548.md), it shall be his.

<a name="numbers_5_11"></a>Numbers 5:11

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_5_12"></a>Numbers 5:12

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, If ['iysh](../../strongs/h/h376.md) ['ishshah](../../strongs/h/h802.md) [śāṭâ](../../strongs/h/h7847.md), and [māʿal](../../strongs/h/h4603.md) a [maʿal](../../strongs/h/h4604.md) against him,

<a name="numbers_5_13"></a>Numbers 5:13

And an ['iysh](../../strongs/h/h376.md) [shakab](../../strongs/h/h7901.md) with her [šᵊḵāḇâ](../../strongs/h/h7902.md) [zera'](../../strongs/h/h2233.md), and it be ['alam](../../strongs/h/h5956.md) from the ['ayin](../../strongs/h/h5869.md) of her ['iysh](../../strongs/h/h376.md), and be [cathar](../../strongs/h/h5641.md), and she be [ṭāmē'](../../strongs/h/h2930.md), and there be no ['ed](../../strongs/h/h5707.md) against her, neither she be [tāp̄aś](../../strongs/h/h8610.md) with the manner;

<a name="numbers_5_14"></a>Numbers 5:14

And the [ruwach](../../strongs/h/h7307.md) of [qin'â](../../strongs/h/h7068.md) ['abar](../../strongs/h/h5674.md) upon him, and he be [qānā'](../../strongs/h/h7065.md) of his ['ishshah](../../strongs/h/h802.md), and she be [ṭāmē'](../../strongs/h/h2930.md): or if the [ruwach](../../strongs/h/h7307.md) of [qin'â](../../strongs/h/h7068.md) ['abar](../../strongs/h/h5674.md) upon him, and he be [qānā'](../../strongs/h/h7065.md) of his ['ishshah](../../strongs/h/h802.md), and she be not [ṭāmē'](../../strongs/h/h2930.md):

<a name="numbers_5_15"></a>Numbers 5:15

Then shall the ['iysh](../../strongs/h/h376.md) [bow'](../../strongs/h/h935.md) his ['ishshah](../../strongs/h/h802.md) unto the [kōhēn](../../strongs/h/h3548.md), and he shall [bow'](../../strongs/h/h935.md) her [qorban](../../strongs/h/h7133.md) for her, the tenth of an ['êp̄â](../../strongs/h/h374.md) of [śᵊʿōrâ](../../strongs/h/h8184.md) [qemaḥ](../../strongs/h/h7058.md); he shall [yāṣaq](../../strongs/h/h3332.md) no [šemen](../../strongs/h/h8081.md) upon it, nor [nathan](../../strongs/h/h5414.md) [lᵊḇônâ](../../strongs/h/h3828.md) thereon; for it is a [minchah](../../strongs/h/h4503.md) of [qin'â](../../strongs/h/h7068.md), a [minchah](../../strongs/h/h4503.md) of [zikārôn](../../strongs/h/h2146.md), [zakar](../../strongs/h/h2142.md) ['avon](../../strongs/h/h5771.md).

<a name="numbers_5_16"></a>Numbers 5:16

And the [kōhēn](../../strongs/h/h3548.md) shall [qāraḇ](../../strongs/h/h7126.md) her, and ['amad](../../strongs/h/h5975.md) her [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md):

<a name="numbers_5_17"></a>Numbers 5:17

And the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) [qadowsh](../../strongs/h/h6918.md) [mayim](../../strongs/h/h4325.md) in an [ḥereś](../../strongs/h/h2789.md) [kĕliy](../../strongs/h/h3627.md); and of the ['aphar](../../strongs/h/h6083.md) that is in the [qarqaʿ](../../strongs/h/h7172.md) of the [miškān](../../strongs/h/h4908.md) the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md), and [nathan](../../strongs/h/h5414.md) it into the [mayim](../../strongs/h/h4325.md):

<a name="numbers_5_18"></a>Numbers 5:18

And the [kōhēn](../../strongs/h/h3548.md) shall ['amad](../../strongs/h/h5975.md) the ['ishshah](../../strongs/h/h802.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and [pāraʿ](../../strongs/h/h6544.md) the ['ishshah](../../strongs/h/h802.md) [ro'sh](../../strongs/h/h7218.md), and [nathan](../../strongs/h/h5414.md) the [minchah](../../strongs/h/h4503.md) of [zikārôn](../../strongs/h/h2146.md) in her [kaph](../../strongs/h/h3709.md), which is the [qin'â](../../strongs/h/h7068.md) [minchah](../../strongs/h/h4503.md): and the [kōhēn](../../strongs/h/h3548.md) shall have in his [yad](../../strongs/h/h3027.md) the [mar](../../strongs/h/h4751.md) [mayim](../../strongs/h/h4325.md) that causeth the ['arar](../../strongs/h/h779.md):

<a name="numbers_5_19"></a>Numbers 5:19

And the [kōhēn](../../strongs/h/h3548.md) shall charge her by a [shaba'](../../strongs/h/h7650.md), and ['āmar](../../strongs/h/h559.md) unto the ['ishshah](../../strongs/h/h802.md), If no ['iysh](../../strongs/h/h376.md) have [shakab](../../strongs/h/h7901.md) with thee, and if thou hast not [śāṭâ](../../strongs/h/h7847.md) to [ṭām'â](../../strongs/h/h2932.md) with another instead of thy ['iysh](../../strongs/h/h376.md), be thou [naqah](../../strongs/h/h5352.md) from this [mar](../../strongs/h/h4751.md) [mayim](../../strongs/h/h4325.md) that causeth the ['arar](../../strongs/h/h779.md):

<a name="numbers_5_20"></a>Numbers 5:20

But if thou hast [śāṭâ](../../strongs/h/h7847.md) to another instead of thy ['iysh](../../strongs/h/h376.md), and if thou be [ṭāmē'](../../strongs/h/h2930.md), and some ['iysh](../../strongs/h/h376.md) [nathan](../../strongs/h/h5414.md) [šᵊḵōḇeṯ](../../strongs/h/h7903.md) with thee [bilʿăḏê](../../strongs/h/h1107.md) thine ['iysh](../../strongs/h/h376.md):

<a name="numbers_5_21"></a>Numbers 5:21

Then the [kōhēn](../../strongs/h/h3548.md) shall [shaba'](../../strongs/h/h7650.md) the ['ishshah](../../strongs/h/h802.md) with a [šᵊḇûʿâ](../../strongs/h/h7621.md) of ['alah](../../strongs/h/h423.md), and the [kōhēn](../../strongs/h/h3548.md) shall ['āmar](../../strongs/h/h559.md) unto the ['ishshah](../../strongs/h/h802.md), [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) thee an ['alah](../../strongs/h/h423.md) and a [šᵊḇûʿâ](../../strongs/h/h7621.md) [tavek](../../strongs/h/h8432.md) thy ['am](../../strongs/h/h5971.md), when [Yĕhovah](../../strongs/h/h3068.md) doth [nathan](../../strongs/h/h5414.md) thy [yārēḵ](../../strongs/h/h3409.md) to [naphal](../../strongs/h/h5307.md), and thy [beten](../../strongs/h/h990.md) to [ṣāḇê](../../strongs/h/h6639.md);

<a name="numbers_5_22"></a>Numbers 5:22

And this [mayim](../../strongs/h/h4325.md) that causeth the ['arar](../../strongs/h/h779.md) shall [bow'](../../strongs/h/h935.md) into thy [me'ah](../../strongs/h/h4578.md), to make thy [beten](../../strongs/h/h990.md) to [ṣāḇâ](../../strongs/h/h6638.md), and thy [yārēḵ](../../strongs/h/h3409.md) to [naphal](../../strongs/h/h5307.md): And the ['ishshah](../../strongs/h/h802.md) shall ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md), ['amen](../../strongs/h/h543.md).

<a name="numbers_5_23"></a>Numbers 5:23

And the [kōhēn](../../strongs/h/h3548.md) shall [kāṯaḇ](../../strongs/h/h3789.md) these ['alah](../../strongs/h/h423.md) in a [sēp̄er](../../strongs/h/h5612.md), and he shall [māḥâ](../../strongs/h/h4229.md) them out with the [mar](../../strongs/h/h4751.md) [mayim](../../strongs/h/h4325.md):

<a name="numbers_5_24"></a>Numbers 5:24

And he shall cause the ['ishshah](../../strongs/h/h802.md) to [šāqâ](../../strongs/h/h8248.md) the [mar](../../strongs/h/h4751.md) [mayim](../../strongs/h/h4325.md) that causeth the ['arar](../../strongs/h/h779.md): and the [šāqâ](../../strongs/h/h8248.md) that causeth the ['arar](../../strongs/h/h779.md) shall [bow'](../../strongs/h/h935.md) into her, and become [mar](../../strongs/h/h4751.md).

<a name="numbers_5_25"></a>Numbers 5:25

Then the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) the [qin'â](../../strongs/h/h7068.md) [minchah](../../strongs/h/h4503.md) out of the ['ishshah](../../strongs/h/h802.md) [yad](../../strongs/h/h3027.md), and shall [nûp̄](../../strongs/h/h5130.md) the [minchah](../../strongs/h/h4503.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and [qāraḇ](../../strongs/h/h7126.md) it upon the [mizbeach](../../strongs/h/h4196.md):

<a name="numbers_5_26"></a>Numbers 5:26

And the [kōhēn](../../strongs/h/h3548.md) shall [qāmaṣ](../../strongs/h/h7061.md) of the [minchah](../../strongs/h/h4503.md), even the ['azkārâ](../../strongs/h/h234.md) thereof, and [qāṭar](../../strongs/h/h6999.md) it upon the [mizbeach](../../strongs/h/h4196.md), and ['aḥar](../../strongs/h/h310.md) shall cause the ['ishshah](../../strongs/h/h802.md) to [šāqâ](../../strongs/h/h8248.md) the [mayim](../../strongs/h/h4325.md).

<a name="numbers_5_27"></a>Numbers 5:27

And when he hath made her to [šāqâ](../../strongs/h/h8248.md) the [mayim](../../strongs/h/h4325.md), then it shall come to pass, that, if she be [ṭāmē'](../../strongs/h/h2930.md), and have [māʿal](../../strongs/h/h4603.md) [maʿal](../../strongs/h/h4604.md) against her ['iysh](../../strongs/h/h376.md), that the [šāqâ](../../strongs/h/h8248.md) that causeth the ['arar](../../strongs/h/h779.md) shall [bow'](../../strongs/h/h935.md) into her, and become [mar](../../strongs/h/h4751.md), and her [beten](../../strongs/h/h990.md) shall [ṣāḇâ](../../strongs/h/h6638.md), and her [yārēḵ](../../strongs/h/h3409.md) shall [naphal](../../strongs/h/h5307.md): and the ['ishshah](../../strongs/h/h802.md) shall be an ['alah](../../strongs/h/h423.md) [qereḇ](../../strongs/h/h7130.md) her ['am](../../strongs/h/h5971.md).

<a name="numbers_5_28"></a>Numbers 5:28

And if the ['ishshah](../../strongs/h/h802.md) be not [ṭāmē'](../../strongs/h/h2930.md), but be [tahowr](../../strongs/h/h2889.md); then she shall be [naqah](../../strongs/h/h5352.md), and shall [zāraʿ](../../strongs/h/h2232.md) [zera'](../../strongs/h/h2233.md).

<a name="numbers_5_29"></a>Numbers 5:29

This is the [towrah](../../strongs/h/h8451.md) of [qin'â](../../strongs/h/h7068.md), when an ['ishshah](../../strongs/h/h802.md) [śāṭâ](../../strongs/h/h7847.md) to another instead of her ['iysh](../../strongs/h/h376.md), and is [ṭāmē'](../../strongs/h/h2930.md);

<a name="numbers_5_30"></a>Numbers 5:30

Or when the [ruwach](../../strongs/h/h7307.md) of [qin'â](../../strongs/h/h7068.md) ['abar](../../strongs/h/h5674.md) upon ['iysh](../../strongs/h/h376.md), and he be [qānā'](../../strongs/h/h7065.md) over his ['ishshah](../../strongs/h/h802.md), and shall ['amad](../../strongs/h/h5975.md) the ['ishshah](../../strongs/h/h802.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and the [kōhēn](../../strongs/h/h3548.md) shall ['asah](../../strongs/h/h6213.md) upon her all this [towrah](../../strongs/h/h8451.md).

<a name="numbers_5_31"></a>Numbers 5:31

Then shall the ['iysh](../../strongs/h/h376.md) be [naqah](../../strongs/h/h5352.md) from ['avon](../../strongs/h/h5771.md), and this ['ishshah](../../strongs/h/h802.md) shall [nasa'](../../strongs/h/h5375.md) her ['avon](../../strongs/h/h5771.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 4](numbers_4.md) - [Numbers 6](numbers_6.md)