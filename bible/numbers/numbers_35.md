# [Numbers 35](https://www.blueletterbible.org/kjv/num/35)

<a name="numbers_35_1"></a>Numbers 35:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) in the ['arabah](../../strongs/h/h6160.md) of [Mô'āḇ](../../strongs/h/h4124.md) by [Yardēn](../../strongs/h/h3383.md) near [Yᵊrēḥô](../../strongs/h/h3405.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_35_2"></a>Numbers 35:2

[tsavah](../../strongs/h/h6680.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they [nathan](../../strongs/h/h5414.md) unto the [Lᵊvî](../../strongs/h/h3881.md) of the [nachalah](../../strongs/h/h5159.md) of their ['achuzzah](../../strongs/h/h272.md) [ʿîr](../../strongs/h/h5892.md) to [yashab](../../strongs/h/h3427.md) in; and ye shall [nathan](../../strongs/h/h5414.md) also unto the [Lᵊvî](../../strongs/h/h3881.md) [miḡrāš](../../strongs/h/h4054.md) for the [ʿîr](../../strongs/h/h5892.md) [cabiyb](../../strongs/h/h5439.md) them.

<a name="numbers_35_3"></a>Numbers 35:3

And the [ʿîr](../../strongs/h/h5892.md) shall they have to [yashab](../../strongs/h/h3427.md) in; and the [miḡrāš](../../strongs/h/h4054.md) of them shall be for their [bĕhemah](../../strongs/h/h929.md), and for their [rᵊḵûš](../../strongs/h/h7399.md), and for all their [chay](../../strongs/h/h2416.md).

<a name="numbers_35_4"></a>Numbers 35:4

And the [miḡrāš](../../strongs/h/h4054.md) of the [ʿîr](../../strongs/h/h5892.md), which ye shall [nathan](../../strongs/h/h5414.md) unto the [Lᵊvî](../../strongs/h/h3881.md), shall reach from the [qîr](../../strongs/h/h7023.md) of the [ʿîr](../../strongs/h/h5892.md) and [ḥûṣ](../../strongs/h/h2351.md) a thousand ['ammâ](../../strongs/h/h520.md) [cabiyb](../../strongs/h/h5439.md).

<a name="numbers_35_5"></a>Numbers 35:5

And ye shall [māḏaḏ](../../strongs/h/h4058.md) from [ḥûṣ](../../strongs/h/h2351.md) the [ʿîr](../../strongs/h/h5892.md) on the [qeḏem](../../strongs/h/h6924.md) [pē'â](../../strongs/h/h6285.md) two thousand ['ammâ](../../strongs/h/h520.md), and on the [neḡeḇ](../../strongs/h/h5045.md) [pē'â](../../strongs/h/h6285.md) two thousand ['ammâ](../../strongs/h/h520.md), and on the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md) two thousand ['ammâ](../../strongs/h/h520.md), and on the [ṣāp̄ôn](../../strongs/h/h6828.md) [pē'â](../../strongs/h/h6285.md) two thousand ['ammâ](../../strongs/h/h520.md); and the [ʿîr](../../strongs/h/h5892.md) shall be in the [tavek](../../strongs/h/h8432.md): this shall be to them the [miḡrāš](../../strongs/h/h4054.md) of the [ʿîr](../../strongs/h/h5892.md).

<a name="numbers_35_6"></a>Numbers 35:6

And among the [ʿîr](../../strongs/h/h5892.md) which ye shall [nathan](../../strongs/h/h5414.md) unto the [Lᵊvî](../../strongs/h/h3881.md) there shall be six [ʿîr](../../strongs/h/h5892.md) for [miqlāṭ](../../strongs/h/h4733.md), which ye shall [nathan](../../strongs/h/h5414.md) for the [ratsach](../../strongs/h/h7523.md), that he may [nûs](../../strongs/h/h5127.md) thither: and to them ye shall [nathan](../../strongs/h/h5414.md) forty and two [ʿîr](../../strongs/h/h5892.md).

<a name="numbers_35_7"></a>Numbers 35:7

So all the [ʿîr](../../strongs/h/h5892.md) which ye shall [nathan](../../strongs/h/h5414.md) to the [Lᵊvî](../../strongs/h/h3881.md) shall be forty and eight [ʿîr](../../strongs/h/h5892.md): them shall ye with their [miḡrāš](../../strongs/h/h4054.md).

<a name="numbers_35_8"></a>Numbers 35:8

And the [ʿîr](../../strongs/h/h5892.md) which ye shall [nathan](../../strongs/h/h5414.md) shall be of the ['achuzzah](../../strongs/h/h272.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): from them that have [rab](../../strongs/h/h7227.md) ye shall [rabah](../../strongs/h/h7235.md); but from them that have [mᵊʿaṭ](../../strongs/h/h4592.md) ye shall [māʿaṭ](../../strongs/h/h4591.md): every ['iysh](../../strongs/h/h376.md) shall [nathan](../../strongs/h/h5414.md) of his [ʿîr](../../strongs/h/h5892.md) unto the [Lᵊvî](../../strongs/h/h3881.md) [peh](../../strongs/h/h6310.md) to his [nachalah](../../strongs/h/h5159.md) which he [nāḥal](../../strongs/h/h5157.md).

<a name="numbers_35_9"></a>Numbers 35:9

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_35_10"></a>Numbers 35:10

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, When ye be ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) into the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md);

<a name="numbers_35_11"></a>Numbers 35:11

Then ye shall [qārâ](../../strongs/h/h7136.md) you [ʿîr](../../strongs/h/h5892.md) to be [ʿîr](../../strongs/h/h5892.md) of [miqlāṭ](../../strongs/h/h4733.md) for you; that the [ratsach](../../strongs/h/h7523.md) may [nûs](../../strongs/h/h5127.md) thither, which [nakah](../../strongs/h/h5221.md) any [nephesh](../../strongs/h/h5315.md) at [šᵊḡāḡâ](../../strongs/h/h7684.md).

<a name="numbers_35_12"></a>Numbers 35:12

And they shall be unto you [ʿîr](../../strongs/h/h5892.md) for [miqlāṭ](../../strongs/h/h4733.md) from the [gā'al](../../strongs/h/h1350.md); that the [ratsach](../../strongs/h/h7523.md) [muwth](../../strongs/h/h4191.md) not, until he ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the ['edah](../../strongs/h/h5712.md) in [mishpat](../../strongs/h/h4941.md).

<a name="numbers_35_13"></a>Numbers 35:13

And of these [ʿîr](../../strongs/h/h5892.md) which ye shall [nathan](../../strongs/h/h5414.md) six [ʿîr](../../strongs/h/h5892.md) shall ye have for [miqlāṭ](../../strongs/h/h4733.md).

<a name="numbers_35_14"></a>Numbers 35:14

Ye shall [nathan](../../strongs/h/h5414.md) three [ʿîr](../../strongs/h/h5892.md) on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md), and three [ʿîr](../../strongs/h/h5892.md) shall ye [nathan](../../strongs/h/h5414.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), which shall be [ʿîr](../../strongs/h/h5892.md) of [miqlāṭ](../../strongs/h/h4733.md).

<a name="numbers_35_15"></a>Numbers 35:15

These six [ʿîr](../../strongs/h/h5892.md) shall be a [miqlāṭ](../../strongs/h/h4733.md), for the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and for the [ger](../../strongs/h/h1616.md), and for the [tôšāḇ](../../strongs/h/h8453.md) [tavek](../../strongs/h/h8432.md) them: that every one that [nakah](../../strongs/h/h5221.md) any [nephesh](../../strongs/h/h5315.md) [šᵊḡāḡâ](../../strongs/h/h7684.md) may [nûs](../../strongs/h/h5127.md) thither.

<a name="numbers_35_16"></a>Numbers 35:16

And if he [nakah](../../strongs/h/h5221.md) him with a [kĕliy](../../strongs/h/h3627.md) of [barzel](../../strongs/h/h1270.md), so that he [muwth](../../strongs/h/h4191.md), he is a [ratsach](../../strongs/h/h7523.md): the [ratsach](../../strongs/h/h7523.md) shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="numbers_35_17"></a>Numbers 35:17

And if he [nakah](../../strongs/h/h5221.md) him with [yad](../../strongs/h/h3027.md) an ['eben](../../strongs/h/h68.md), wherewith he may [muwth](../../strongs/h/h4191.md), and he [muwth](../../strongs/h/h4191.md), he is a [ratsach](../../strongs/h/h7523.md): the [ratsach](../../strongs/h/h7523.md) shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="numbers_35_18"></a>Numbers 35:18

Or if he [nakah](../../strongs/h/h5221.md) him with a [yad](../../strongs/h/h3027.md) [kĕliy](../../strongs/h/h3627.md) of ['ets](../../strongs/h/h6086.md), wherewith he may [muwth](../../strongs/h/h4191.md), and he [muwth](../../strongs/h/h4191.md), he is a [ratsach](../../strongs/h/h7523.md): the [ratsach](../../strongs/h/h7523.md) shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="numbers_35_19"></a>Numbers 35:19

The [gā'al](../../strongs/h/h1350.md) of [dam](../../strongs/h/h1818.md) himself shall [muwth](../../strongs/h/h4191.md) the [ratsach](../../strongs/h/h7523.md): when he [pāḡaʿ](../../strongs/h/h6293.md) him, he shall [muwth](../../strongs/h/h4191.md) him.

<a name="numbers_35_20"></a>Numbers 35:20

But if he [hāḏap̄](../../strongs/h/h1920.md) him of [śin'â](../../strongs/h/h8135.md), or [shalak](../../strongs/h/h7993.md) at him by [ṣᵊḏîyâ](../../strongs/h/h6660.md), that he [muwth](../../strongs/h/h4191.md);

<a name="numbers_35_21"></a>Numbers 35:21

Or in ['eybah](../../strongs/h/h342.md) [nakah](../../strongs/h/h5221.md) him with his [yad](../../strongs/h/h3027.md), that he [muwth](../../strongs/h/h4191.md): he that [nakah](../../strongs/h/h5221.md) him shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md); for he is a [ratsach](../../strongs/h/h7523.md): the [gā'al](../../strongs/h/h1350.md) of [dam](../../strongs/h/h1818.md) shall [muwth](../../strongs/h/h4191.md) the [ratsach](../../strongs/h/h7523.md), when he [pāḡaʿ](../../strongs/h/h6293.md) him.

<a name="numbers_35_22"></a>Numbers 35:22

But if he [hāḏap̄](../../strongs/h/h1920.md) him [peṯaʿ](../../strongs/h/h6621.md) without ['eybah](../../strongs/h/h342.md), or have [shalak](../../strongs/h/h7993.md) upon him [kĕliy](../../strongs/h/h3627.md) without [ṣᵊḏîyâ](../../strongs/h/h6660.md),

<a name="numbers_35_23"></a>Numbers 35:23

Or with any ['eben](../../strongs/h/h68.md), wherewith a man may [muwth](../../strongs/h/h4191.md), [ra'ah](../../strongs/h/h7200.md) him not, and [naphal](../../strongs/h/h5307.md) it upon him, that he [muwth](../../strongs/h/h4191.md), and was not his ['oyeb](../../strongs/h/h341.md), neither [bāqaš](../../strongs/h/h1245.md) his [ra'](../../strongs/h/h7451.md):

<a name="numbers_35_24"></a>Numbers 35:24

Then the ['edah](../../strongs/h/h5712.md) shall [shaphat](../../strongs/h/h8199.md) between the [nakah](../../strongs/h/h5221.md) and the [gā'al](../../strongs/h/h1350.md) of [dam](../../strongs/h/h1818.md) according to these [mishpat](../../strongs/h/h4941.md):

<a name="numbers_35_25"></a>Numbers 35:25

And the ['edah](../../strongs/h/h5712.md) shall [natsal](../../strongs/h/h5337.md) the [ratsach](../../strongs/h/h7523.md) out of the [yad](../../strongs/h/h3027.md) of the [gā'al](../../strongs/h/h1350.md) of [dam](../../strongs/h/h1818.md), and the ['edah](../../strongs/h/h5712.md) shall [shuwb](../../strongs/h/h7725.md) him to the [ʿîr](../../strongs/h/h5892.md) of his [miqlāṭ](../../strongs/h/h4733.md), whither he was [nûs](../../strongs/h/h5127.md): and he shall [yashab](../../strongs/h/h3427.md) in it unto the [maveth](../../strongs/h/h4194.md) of the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md), which was [māšaḥ](../../strongs/h/h4886.md) with the [qodesh](../../strongs/h/h6944.md) [šemen](../../strongs/h/h8081.md).

<a name="numbers_35_26"></a>Numbers 35:26

But if the [ratsach](../../strongs/h/h7523.md) shall [yāṣā'](../../strongs/h/h3318.md) [yāṣā'](../../strongs/h/h3318.md) without the [gᵊḇûl](../../strongs/h/h1366.md) of the [ʿîr](../../strongs/h/h5892.md) of his [miqlāṭ](../../strongs/h/h4733.md), whither he was [nûs](../../strongs/h/h5127.md);

<a name="numbers_35_27"></a>Numbers 35:27

And the [gā'al](../../strongs/h/h1350.md) of [dam](../../strongs/h/h1818.md) [māṣā'](../../strongs/h/h4672.md) him [ḥûṣ](../../strongs/h/h2351.md) the [gᵊḇûl](../../strongs/h/h1366.md) of the [ʿîr](../../strongs/h/h5892.md) of his [miqlāṭ](../../strongs/h/h4733.md), and the [gā'al](../../strongs/h/h1350.md) of [dam](../../strongs/h/h1818.md) [ratsach](../../strongs/h/h7523.md) the [ratsach](../../strongs/h/h7523.md); he shall not be guilty of [dam](../../strongs/h/h1818.md):

<a name="numbers_35_28"></a>Numbers 35:28

Because he should have [yashab](../../strongs/h/h3427.md) in the [ʿîr](../../strongs/h/h5892.md) of his [miqlāṭ](../../strongs/h/h4733.md) until the [maveth](../../strongs/h/h4194.md) of the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md): but ['aḥar](../../strongs/h/h310.md) the [maveth](../../strongs/h/h4194.md) of the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md) the [ratsach](../../strongs/h/h7523.md) shall [shuwb](../../strongs/h/h7725.md) into the ['erets](../../strongs/h/h776.md) of his ['achuzzah](../../strongs/h/h272.md).

<a name="numbers_35_29"></a>Numbers 35:29

So these things shall be for a [chuqqah](../../strongs/h/h2708.md) of [mishpat](../../strongs/h/h4941.md) unto you throughout your [dôr](../../strongs/h/h1755.md) in all your [môšāḇ](../../strongs/h/h4186.md).

<a name="numbers_35_30"></a>Numbers 35:30

Whoso [nakah](../../strongs/h/h5221.md) any [nephesh](../../strongs/h/h5315.md), the [ratsach](../../strongs/h/h7523.md) shall be put to [ratsach](../../strongs/h/h7523.md) by the [peh](../../strongs/h/h6310.md) of ['ed](../../strongs/h/h5707.md): but one ['ed](../../strongs/h/h5707.md) shall not ['anah](../../strongs/h/h6030.md) against any [nephesh](../../strongs/h/h5315.md) to cause him to [muwth](../../strongs/h/h4191.md).

<a name="numbers_35_31"></a>Numbers 35:31

Moreover ye shall [laqach](../../strongs/h/h3947.md) no [kōp̄er](../../strongs/h/h3724.md) for the [nephesh](../../strongs/h/h5315.md) of a [ratsach](../../strongs/h/h7523.md), which is [rasha'](../../strongs/h/h7563.md) of [muwth](../../strongs/h/h4191.md): but he shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="numbers_35_32"></a>Numbers 35:32

And ye shall [laqach](../../strongs/h/h3947.md) no [kōp̄er](../../strongs/h/h3724.md) for him that is [nûs](../../strongs/h/h5127.md) to the [ʿîr](../../strongs/h/h5892.md) of his [miqlāṭ](../../strongs/h/h4733.md), that he should [shuwb](../../strongs/h/h7725.md) to [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md), until the [maveth](../../strongs/h/h4194.md) of the [kōhēn](../../strongs/h/h3548.md).

<a name="numbers_35_33"></a>Numbers 35:33

So ye shall not [ḥānēp̄](../../strongs/h/h2610.md) the ['erets](../../strongs/h/h776.md) wherein ye are: for [dam](../../strongs/h/h1818.md) it [ḥānēp̄](../../strongs/h/h2610.md) the ['erets](../../strongs/h/h776.md): and the ['erets](../../strongs/h/h776.md) cannot be [kāp̄ar](../../strongs/h/h3722.md) of the [dam](../../strongs/h/h1818.md) that is [šāp̄aḵ](../../strongs/h/h8210.md) therein, but by the [dam](../../strongs/h/h1818.md) of him that [šāp̄aḵ](../../strongs/h/h8210.md) it.

<a name="numbers_35_34"></a>Numbers 35:34

[ṭāmē'](../../strongs/h/h2930.md) not therefore the ['erets](../../strongs/h/h776.md) which ye shall [yashab](../../strongs/h/h3427.md), wherein I [shakan](../../strongs/h/h7931.md): for I [Yĕhovah](../../strongs/h/h3068.md) [shakan](../../strongs/h/h7931.md) [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 34](numbers_34.md) - [Numbers 36](numbers_36.md)