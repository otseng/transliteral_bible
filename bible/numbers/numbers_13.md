# [Numbers 13](https://www.blueletterbible.org/kjv/num/13)

<a name="numbers_13_1"></a>Numbers 13:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_13_2"></a>Numbers 13:2

[shalach](../../strongs/h/h7971.md) thou ['enowsh](../../strongs/h/h582.md), that they may [tûr](../../strongs/h/h8446.md) the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), which I [nathan](../../strongs/h/h5414.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): of ['iysh](../../strongs/h/h376.md) [maṭṭê](../../strongs/h/h4294.md) of their ['ab](../../strongs/h/h1.md) shall ye [shalach](../../strongs/h/h7971.md) an ['echad](../../strongs/h/h259.md), every one a [nāśî'](../../strongs/h/h5387.md) among them.

<a name="numbers_13_3"></a>Numbers 13:3

And [Mōshe](../../strongs/h/h4872.md) by the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) them from the [midbar](../../strongs/h/h4057.md) of [Pā'rān](../../strongs/h/h6290.md): all those ['enowsh](../../strongs/h/h582.md) were [ro'sh](../../strongs/h/h7218.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_13_4"></a>Numbers 13:4

And these were their [shem](../../strongs/h/h8034.md): of the [maṭṭê](../../strongs/h/h4294.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), [Šammûaʿ](../../strongs/h/h8051.md) the [ben](../../strongs/h/h1121.md) of [Zakûr](../../strongs/h/h2139.md).

<a name="numbers_13_5"></a>Numbers 13:5

Of the [maṭṭê](../../strongs/h/h4294.md) of [Šimʿôn](../../strongs/h/h8095.md), [Šāp̄āṭ](../../strongs/h/h8202.md) the [ben](../../strongs/h/h1121.md) of [Ḥōrî](../../strongs/h/h2753.md).

<a name="numbers_13_6"></a>Numbers 13:6

Of the [maṭṭê](../../strongs/h/h4294.md) of [Yehuwdah](../../strongs/h/h3063.md), [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md).

<a name="numbers_13_7"></a>Numbers 13:7

Of the [maṭṭê](../../strongs/h/h4294.md) of [Yiśśāśḵār](../../strongs/h/h3485.md), [Yiḡ'āl](../../strongs/h/h3008.md) the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md).

<a name="numbers_13_8"></a>Numbers 13:8

Of the [maṭṭê](../../strongs/h/h4294.md) of ['Ep̄rayim](../../strongs/h/h669.md), [Hôšēaʿ](../../strongs/h/h1954.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md).

<a name="numbers_13_9"></a>Numbers 13:9

Of the [maṭṭê](../../strongs/h/h4294.md) of [Binyāmîn](../../strongs/h/h1144.md), [palṭî](../../strongs/h/h6406.md) the [ben](../../strongs/h/h1121.md) of [rāp̄û'](../../strongs/h/h7505.md).

<a name="numbers_13_10"></a>Numbers 13:10

Of the [maṭṭê](../../strongs/h/h4294.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md), [gadî'ēl](../../strongs/h/h1427.md) the [ben](../../strongs/h/h1121.md) of [sôḏî](../../strongs/h/h5476.md).

<a name="numbers_13_11"></a>Numbers 13:11

Of the [maṭṭê](../../strongs/h/h4294.md) of [Yôsēp̄](../../strongs/h/h3130.md), namely, of the [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md), [gadî](../../strongs/h/h1426.md) the [ben](../../strongs/h/h1121.md) of [sûsî](../../strongs/h/h5485.md).

<a name="numbers_13_12"></a>Numbers 13:12

Of the [maṭṭê](../../strongs/h/h4294.md) of [Dān](../../strongs/h/h1835.md), [ʿAmmî'ēl](../../strongs/h/h5988.md) the [ben](../../strongs/h/h1121.md) of [gᵊmallî](../../strongs/h/h1582.md).

<a name="numbers_13_13"></a>Numbers 13:13

Of the [maṭṭê](../../strongs/h/h4294.md) of ['Āšēr](../../strongs/h/h836.md), [sᵊṯûr](../../strongs/h/h5639.md) the [ben](../../strongs/h/h1121.md) of [Mîḵā'ēl](../../strongs/h/h4317.md).

<a name="numbers_13_14"></a>Numbers 13:14

Of the [maṭṭê](../../strongs/h/h4294.md) of [Nap̄tālî](../../strongs/h/h5321.md), [naḥbî](../../strongs/h/h5147.md) the [ben](../../strongs/h/h1121.md) of [vāp̄sî](../../strongs/h/h2058.md).

<a name="numbers_13_15"></a>Numbers 13:15

Of the [maṭṭê](../../strongs/h/h4294.md) of [Gāḏ](../../strongs/h/h1410.md), [gᵊ'û'ēl](../../strongs/h/h1345.md) the [ben](../../strongs/h/h1121.md) of [māḵî](../../strongs/h/h4352.md).

<a name="numbers_13_16"></a>Numbers 13:16

These are the [shem](../../strongs/h/h8034.md) of the ['enowsh](../../strongs/h/h582.md) which [Mōshe](../../strongs/h/h4872.md) [shalach](../../strongs/h/h7971.md) to [tûr](../../strongs/h/h8446.md) the ['erets](../../strongs/h/h776.md). And [Mōshe](../../strongs/h/h4872.md) [qara'](../../strongs/h/h7121.md) [Hôšēaʿ](../../strongs/h/h1954.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md).

<a name="numbers_13_17"></a>Numbers 13:17

And [Mōshe](../../strongs/h/h4872.md) [shalach](../../strongs/h/h7971.md) them to [tûr](../../strongs/h/h8446.md) the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), and ['āmar](../../strongs/h/h559.md) unto them, [ʿālâ](../../strongs/h/h5927.md) you this way [neḡeḇ](../../strongs/h/h5045.md), and [ʿālâ](../../strongs/h/h5927.md) into the [har](../../strongs/h/h2022.md):

<a name="numbers_13_18"></a>Numbers 13:18

And [ra'ah](../../strongs/h/h7200.md) the ['erets](../../strongs/h/h776.md), what it is, and the ['am](../../strongs/h/h5971.md) that [yashab](../../strongs/h/h3427.md) therein, whether they be [ḥāzāq](../../strongs/h/h2389.md) or [rāp̄ê](../../strongs/h/h7504.md), [mᵊʿaṭ](../../strongs/h/h4592.md) or [rab](../../strongs/h/h7227.md);

<a name="numbers_13_19"></a>Numbers 13:19

And what the ['erets](../../strongs/h/h776.md) is that they [yashab](../../strongs/h/h3427.md) in, whether it be [towb](../../strongs/h/h2896.md) or [ra'](../../strongs/h/h7451.md); and what [ʿîr](../../strongs/h/h5892.md) they be that they [yashab](../../strongs/h/h3427.md) in, whether in [maḥănê](../../strongs/h/h4264.md), or in [miḇṣār](../../strongs/h/h4013.md);

<a name="numbers_13_20"></a>Numbers 13:20

And what the ['erets](../../strongs/h/h776.md) is, whether it be [šāmēn](../../strongs/h/h8082.md) or [rāzê](../../strongs/h/h7330.md), whether there be ['ets](../../strongs/h/h6086.md) therein, or not. And be ye of [ḥāzaq](../../strongs/h/h2388.md), and [laqach](../../strongs/h/h3947.md) of the [pĕriy](../../strongs/h/h6529.md) of the ['erets](../../strongs/h/h776.md). Now the [yowm](../../strongs/h/h3117.md) was the [yowm](../../strongs/h/h3117.md) of the [bikûr](../../strongs/h/h1061.md) [ʿēnāḇ](../../strongs/h/h6025.md).

<a name="numbers_13_21"></a>Numbers 13:21

So they [ʿālâ](../../strongs/h/h5927.md), and [tûr](../../strongs/h/h8446.md) the ['erets](../../strongs/h/h776.md) from the [midbar](../../strongs/h/h4057.md) of [Ṣin](../../strongs/h/h6790.md) unto [rᵊḥōḇ](../../strongs/h/h7340.md), as men [bow'](../../strongs/h/h935.md) to [Ḥămāṯ](../../strongs/h/h2574.md).

<a name="numbers_13_22"></a>Numbers 13:22

And they [ʿālâ](../../strongs/h/h5927.md) by the [neḡeḇ](../../strongs/h/h5045.md), and [bow'](../../strongs/h/h935.md) unto [Ḥeḇrôn](../../strongs/h/h2275.md); where ['Ăḥîmān](../../strongs/h/h289.md), [šēšay](../../strongs/h/h8344.md), and [Talmay](../../strongs/h/h8526.md), the [yālîḏ](../../strongs/h/h3211.md) of [ʿĂnāq](../../strongs/h/h6061.md), were. (Now [Ḥeḇrôn](../../strongs/h/h2275.md) was [bānâ](../../strongs/h/h1129.md) seven [šānâ](../../strongs/h/h8141.md) [paniym](../../strongs/h/h6440.md) [Ṣōʿan](../../strongs/h/h6814.md) in [Mitsrayim](../../strongs/h/h4714.md).)

<a name="numbers_13_23"></a>Numbers 13:23

And they [bow'](../../strongs/h/h935.md) unto the [nachal](../../strongs/h/h5158.md) of ['eškōl](../../strongs/h/h812.md), and [karath](../../strongs/h/h3772.md) from thence a [zᵊmôrâ](../../strongs/h/h2156.md) with one ['eškōôl](../../strongs/h/h811.md) of [ʿēnāḇ](../../strongs/h/h6025.md), and they [nasa'](../../strongs/h/h5375.md) it between two upon a [môṭ](../../strongs/h/h4132.md); and they brought of the [rimmôn](../../strongs/h/h7416.md), and of the [tĕ'en](../../strongs/h/h8384.md).

<a name="numbers_13_24"></a>Numbers 13:24

The [maqowm](../../strongs/h/h4725.md) was [qara'](../../strongs/h/h7121.md) the [nachal](../../strongs/h/h5158.md) ['eškōl](../../strongs/h/h812.md), because of the ['eškōôl](../../strongs/h/h811.md) which the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [karath](../../strongs/h/h3772.md) from thence.

<a name="numbers_13_25"></a>Numbers 13:25

And they [shuwb](../../strongs/h/h7725.md) from [tûr](../../strongs/h/h8446.md) of the ['erets](../../strongs/h/h776.md) [qēṣ](../../strongs/h/h7093.md) forty [yowm](../../strongs/h/h3117.md).

<a name="numbers_13_26"></a>Numbers 13:26

And they [yālaḵ](../../strongs/h/h3212.md) and [bow'](../../strongs/h/h935.md) to [Mōshe](../../strongs/h/h4872.md), and to ['Ahărôn](../../strongs/h/h175.md), and to all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), unto the [midbar](../../strongs/h/h4057.md) of [Pā'rān](../../strongs/h/h6290.md), to [Qāḏēš](../../strongs/h/h6946.md); and [shuwb](../../strongs/h/h7725.md) [dabar](../../strongs/h/h1697.md) unto them, and unto all the ['edah](../../strongs/h/h5712.md), and [ra'ah](../../strongs/h/h7200.md) them the [pĕriy](../../strongs/h/h6529.md) of the ['erets](../../strongs/h/h776.md).

<a name="numbers_13_27"></a>Numbers 13:27

And they [sāp̄ar](../../strongs/h/h5608.md) him, and ['āmar](../../strongs/h/h559.md), We [bow'](../../strongs/h/h935.md) unto the ['erets](../../strongs/h/h776.md) whither thou [shalach](../../strongs/h/h7971.md) us, and surely it [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md); and this is the [pĕriy](../../strongs/h/h6529.md) of it.

<a name="numbers_13_28"></a>Numbers 13:28

['ep̄es](../../strongs/h/h657.md) the ['am](../../strongs/h/h5971.md) be ['az](../../strongs/h/h5794.md) that [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md), and the [ʿîr](../../strongs/h/h5892.md) are [bāṣar](../../strongs/h/h1219.md), and [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md): and moreover we [ra'ah](../../strongs/h/h7200.md) the [yālîḏ](../../strongs/h/h3211.md) of [ʿĂnāq](../../strongs/h/h6061.md) there.

<a name="numbers_13_29"></a>Numbers 13:29

The [ʿĂmālēq](../../strongs/h/h6002.md) [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of the [neḡeḇ](../../strongs/h/h5045.md): and the [Ḥitî](../../strongs/h/h2850.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md), and the ['Ĕmōrî](../../strongs/h/h567.md), [yashab](../../strongs/h/h3427.md) in the [har](../../strongs/h/h2022.md): and the [Kᵊnaʿănî](../../strongs/h/h3669.md) [yashab](../../strongs/h/h3427.md) by the [yam](../../strongs/h/h3220.md), and by the [yad](../../strongs/h/h3027.md) of [Yardēn](../../strongs/h/h3383.md).

<a name="numbers_13_30"></a>Numbers 13:30

And [Kālēḇ](../../strongs/h/h3612.md) [hāsâ](../../strongs/h/h2013.md) the ['am](../../strongs/h/h5971.md) before [Mōshe](../../strongs/h/h4872.md), and ['āmar](../../strongs/h/h559.md), Let us [ʿālâ](../../strongs/h/h5927.md) [ʿālâ](../../strongs/h/h5927.md), and [yarash](../../strongs/h/h3423.md) it; for we are [yakol](../../strongs/h/h3201.md) [yakol](../../strongs/h/h3201.md) it.

<a name="numbers_13_31"></a>Numbers 13:31

But the ['enowsh](../../strongs/h/h582.md) that [ʿālâ](../../strongs/h/h5927.md) with him ['āmar](../../strongs/h/h559.md), We be not [yakol](../../strongs/h/h3201.md) to [ʿālâ](../../strongs/h/h5927.md) against the ['am](../../strongs/h/h5971.md); for they are [ḥāzāq](../../strongs/h/h2389.md) than we.

<a name="numbers_13_32"></a>Numbers 13:32

And they [yāṣā'](../../strongs/h/h3318.md) a [dibâ](../../strongs/h/h1681.md) of the ['erets](../../strongs/h/h776.md) which they had [tûr](../../strongs/h/h8446.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), The ['erets](../../strongs/h/h776.md), through which we have ['abar](../../strongs/h/h5674.md) to [tûr](../../strongs/h/h8446.md) it, is an ['erets](../../strongs/h/h776.md) that ['akal](../../strongs/h/h398.md) the [yashab](../../strongs/h/h3427.md) thereof; and all the ['am](../../strongs/h/h5971.md) that we [ra'ah](../../strongs/h/h7200.md) in [tavek](../../strongs/h/h8432.md) are ['enowsh](../../strongs/h/h582.md) of [midâ](../../strongs/h/h4060.md).

<a name="numbers_13_33"></a>Numbers 13:33

And there we [ra'ah](../../strongs/h/h7200.md) the [nāp̄îl](../../strongs/h/h5303.md), the [ben](../../strongs/h/h1121.md) of [ʿĂnāq](../../strongs/h/h6061.md), which come of the [nāp̄îl](../../strongs/h/h5303.md): and we were in our own ['ayin](../../strongs/h/h5869.md) as [ḥāḡāḇ](../../strongs/h/h2284.md), and so we were in their ['ayin](../../strongs/h/h5869.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 12](numbers_12.md) - [Numbers 14](numbers_14.md)