# [Numbers 27](https://www.blueletterbible.org/kjv/num/27)

<a name="numbers_27_1"></a>Numbers 27:1

Then [qāraḇ](../../strongs/h/h7126.md) the [bath](../../strongs/h/h1323.md) of [Ṣᵊlāp̄Ḥāḏ](../../strongs/h/h6765.md), the [ben](../../strongs/h/h1121.md) of [Ḥēp̄er](../../strongs/h/h2660.md), the [ben](../../strongs/h/h1121.md) of [Gilʿāḏ](../../strongs/h/h1568.md), the [ben](../../strongs/h/h1121.md) of [Māḵîr](../../strongs/h/h4353.md), the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md), of the [mišpāḥâ](../../strongs/h/h4940.md) of [Mᵊnaššê](../../strongs/h/h4519.md) the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md): and these are the [shem](../../strongs/h/h8034.md) of his [bath](../../strongs/h/h1323.md); [Maḥlâ](../../strongs/h/h4244.md), [NōʿÂ](../../strongs/h/h5270.md), and [Ḥāḡlâ](../../strongs/h/h2295.md), and [Milkâ](../../strongs/h/h4435.md), and [Tirṣâ](../../strongs/h/h8656.md).

<a name="numbers_27_2"></a>Numbers 27:2

And they ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Mōshe](../../strongs/h/h4872.md), and [paniym](../../strongs/h/h6440.md) ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and [paniym](../../strongs/h/h6440.md) the [nāśî'](../../strongs/h/h5387.md) and all the ['edah](../../strongs/h/h5712.md), by the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_27_3"></a>Numbers 27:3

Our ['ab](../../strongs/h/h1.md) [muwth](../../strongs/h/h4191.md) in the [midbar](../../strongs/h/h4057.md), and he was not [tavek](../../strongs/h/h8432.md) the ['edah](../../strongs/h/h5712.md) of them that [yāʿaḏ](../../strongs/h/h3259.md) against [Yĕhovah](../../strongs/h/h3068.md) in the ['edah](../../strongs/h/h5712.md) of [Qōraḥ](../../strongs/h/h7141.md); but [muwth](../../strongs/h/h4191.md) in his own [ḥēṭĕ'](../../strongs/h/h2399.md), and had no [ben](../../strongs/h/h1121.md).

<a name="numbers_27_4"></a>Numbers 27:4

Why should the [shem](../../strongs/h/h8034.md) of our ['ab](../../strongs/h/h1.md) be [gāraʿ](../../strongs/h/h1639.md) from [tavek](../../strongs/h/h8432.md) his [mišpāḥâ](../../strongs/h/h4940.md), because he hath no [ben](../../strongs/h/h1121.md)? [nathan](../../strongs/h/h5414.md) unto us therefore an ['achuzzah](../../strongs/h/h272.md) [tavek](../../strongs/h/h8432.md) the ['ach](../../strongs/h/h251.md) of our ['ab](../../strongs/h/h1.md).

<a name="numbers_27_5"></a>Numbers 27:5

And [Mōshe](../../strongs/h/h4872.md) [qāraḇ](../../strongs/h/h7126.md) their [mishpat](../../strongs/h/h4941.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_27_6"></a>Numbers 27:6

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_27_7"></a>Numbers 27:7

The [bath](../../strongs/h/h1323.md) of [Ṣᵊlāp̄Ḥāḏ](../../strongs/h/h6765.md) [dabar](../../strongs/h/h1696.md) [kēn](../../strongs/h/h3651.md): thou shalt [nathan](../../strongs/h/h5414.md) [nathan](../../strongs/h/h5414.md) them an ['achuzzah](../../strongs/h/h272.md) of a [nachalah](../../strongs/h/h5159.md) [tavek](../../strongs/h/h8432.md) their ['ab](../../strongs/h/h1.md) ['ach](../../strongs/h/h251.md); and thou shalt cause the [nachalah](../../strongs/h/h5159.md) of their ['ab](../../strongs/h/h1.md) to ['abar](../../strongs/h/h5674.md) unto them.

<a name="numbers_27_8"></a>Numbers 27:8

And thou shalt [dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), If an ['iysh](../../strongs/h/h376.md) [muwth](../../strongs/h/h4191.md), and have no [ben](../../strongs/h/h1121.md), then ye shall cause his [nachalah](../../strongs/h/h5159.md) to ['abar](../../strongs/h/h5674.md) unto his [bath](../../strongs/h/h1323.md).

<a name="numbers_27_9"></a>Numbers 27:9

And if he have no [bath](../../strongs/h/h1323.md), then ye shall [nathan](../../strongs/h/h5414.md) his [nachalah](../../strongs/h/h5159.md) unto his ['ach](../../strongs/h/h251.md).

<a name="numbers_27_10"></a>Numbers 27:10

And if he have no ['ach](../../strongs/h/h251.md), then ye shall [nathan](../../strongs/h/h5414.md) his [nachalah](../../strongs/h/h5159.md) unto his ['ab](../../strongs/h/h1.md) ['ach](../../strongs/h/h251.md).

<a name="numbers_27_11"></a>Numbers 27:11

And if his ['ab](../../strongs/h/h1.md) have no ['ach](../../strongs/h/h251.md), then ye shall [nathan](../../strongs/h/h5414.md) his [nachalah](../../strongs/h/h5159.md) unto his [šᵊ'ēr](../../strongs/h/h7607.md) that is [qarowb](../../strongs/h/h7138.md) to him of his [mišpāḥâ](../../strongs/h/h4940.md), and he shall [yarash](../../strongs/h/h3423.md) it: and it shall be unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) a [chuqqah](../../strongs/h/h2708.md) of [mishpat](../../strongs/h/h4941.md), as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="numbers_27_12"></a>Numbers 27:12

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [ʿālâ](../../strongs/h/h5927.md) into this [har](../../strongs/h/h2022.md) [ʿĂḇārîm](../../strongs/h/h5682.md), and [ra'ah](../../strongs/h/h7200.md) the ['erets](../../strongs/h/h776.md) which I have [nathan](../../strongs/h/h5414.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_27_13"></a>Numbers 27:13

And when thou hast [ra'ah](../../strongs/h/h7200.md) it, thou also shalt be ['āsap̄](../../strongs/h/h622.md) unto thy ['am](../../strongs/h/h5971.md), as ['Ahărôn](../../strongs/h/h175.md) thy ['ach](../../strongs/h/h251.md) was ['āsap̄](../../strongs/h/h622.md).

<a name="numbers_27_14"></a>Numbers 27:14

For ye [marah](../../strongs/h/h4784.md) against my [peh](../../strongs/h/h6310.md) in the [midbar](../../strongs/h/h4057.md) of [Ṣin](../../strongs/h/h6790.md), in the [Mᵊrîḇâ](../../strongs/h/h4808.md) of the ['edah](../../strongs/h/h5712.md), to [qadash](../../strongs/h/h6942.md) me at the [mayim](../../strongs/h/h4325.md) before their ['ayin](../../strongs/h/h5869.md): that is the [mayim](../../strongs/h/h4325.md) of [Mᵊrîḇâ](../../strongs/h/h4809.md) in [Qāḏēš](../../strongs/h/h6946.md) in the [midbar](../../strongs/h/h4057.md) of [Ṣin](../../strongs/h/h6790.md).

<a name="numbers_27_15"></a>Numbers 27:15

And [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) unto [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_27_16"></a>Numbers 27:16

Let [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of the [ruwach](../../strongs/h/h7307.md) of all [basar](../../strongs/h/h1320.md), [paqad](../../strongs/h/h6485.md) an ['iysh](../../strongs/h/h376.md) over the ['edah](../../strongs/h/h5712.md),

<a name="numbers_27_17"></a>Numbers 27:17

Which may [yāṣā'](../../strongs/h/h3318.md) [paniym](../../strongs/h/h6440.md) them, and which may [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) them, and which may [yāṣā'](../../strongs/h/h3318.md) them, and which may [bow'](../../strongs/h/h935.md) them; that the ['edah](../../strongs/h/h5712.md) of [Yĕhovah](../../strongs/h/h3068.md) be not as [tso'n](../../strongs/h/h6629.md) which have no [ra'ah](../../strongs/h/h7462.md).

<a name="numbers_27_18"></a>Numbers 27:18

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [laqach](../../strongs/h/h3947.md) thee [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), an ['iysh](../../strongs/h/h376.md) in whom is the [ruwach](../../strongs/h/h7307.md), and [camak](../../strongs/h/h5564.md) thine [yad](../../strongs/h/h3027.md) upon him;

<a name="numbers_27_19"></a>Numbers 27:19

And ['amad](../../strongs/h/h5975.md) him [paniym](../../strongs/h/h6440.md) ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and [paniym](../../strongs/h/h6440.md) all the ['edah](../../strongs/h/h5712.md); and give him a [tsavah](../../strongs/h/h6680.md) in their ['ayin](../../strongs/h/h5869.md).

<a name="numbers_27_20"></a>Numbers 27:20

And thou shalt [nathan](../../strongs/h/h5414.md) some of thine [howd](../../strongs/h/h1935.md) upon him, that all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) may be [shama'](../../strongs/h/h8085.md).

<a name="numbers_27_21"></a>Numbers 27:21

And he shall ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), who shall [sha'al](../../strongs/h/h7592.md) for him after the [mishpat](../../strongs/h/h4941.md) of ['Ûrîm](../../strongs/h/h224.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): at his [peh](../../strongs/h/h6310.md) shall they [yāṣā'](../../strongs/h/h3318.md), and at his [peh](../../strongs/h/h6310.md) they shall [bow'](../../strongs/h/h935.md), both he, and all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) with him, even all the ['edah](../../strongs/h/h5712.md).

<a name="numbers_27_22"></a>Numbers 27:22

And [Mōshe](../../strongs/h/h4872.md) ['asah](../../strongs/h/h6213.md) as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) him: and he [laqach](../../strongs/h/h3947.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and ['amad](../../strongs/h/h5975.md) him [paniym](../../strongs/h/h6440.md) ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and [paniym](../../strongs/h/h6440.md) all the ['edah](../../strongs/h/h5712.md):

<a name="numbers_27_23"></a>Numbers 27:23

And he [camak](../../strongs/h/h5564.md) his [yad](../../strongs/h/h3027.md) upon him, and gave him a [tsavah](../../strongs/h/h6680.md), as [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 26](numbers_26.md) - [Numbers 28](numbers_28.md)