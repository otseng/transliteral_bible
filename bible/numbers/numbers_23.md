# [Numbers 23](https://www.blueletterbible.org/kjv/num/23)

<a name="numbers_23_1"></a>Numbers 23:1

And [Bilʿām](../../strongs/h/h1109.md) ['āmar](../../strongs/h/h559.md) unto [Bālāq](../../strongs/h/h1111.md), [bānâ](../../strongs/h/h1129.md) me here seven [mizbeach](../../strongs/h/h4196.md), and [kuwn](../../strongs/h/h3559.md) me here seven [par](../../strongs/h/h6499.md) and seven ['ayil](../../strongs/h/h352.md).

<a name="numbers_23_2"></a>Numbers 23:2

And [Bālāq](../../strongs/h/h1111.md) ['asah](../../strongs/h/h6213.md) as [Bilʿām](../../strongs/h/h1109.md) had [dabar](../../strongs/h/h1696.md); and [Bālāq](../../strongs/h/h1111.md) and [Bilʿām](../../strongs/h/h1109.md) [ʿālâ](../../strongs/h/h5927.md) on every [mizbeach](../../strongs/h/h4196.md) a [par](../../strongs/h/h6499.md) and an ['ayil](../../strongs/h/h352.md).

<a name="numbers_23_3"></a>Numbers 23:3

And [Bilʿām](../../strongs/h/h1109.md) ['āmar](../../strongs/h/h559.md) unto [Bālāq](../../strongs/h/h1111.md), [yatsab](../../strongs/h/h3320.md) by thy [ʿōlâ](../../strongs/h/h5930.md), and I will [yālaḵ](../../strongs/h/h3212.md): peradventure [Yĕhovah](../../strongs/h/h3068.md) will [qārâ](../../strongs/h/h7136.md) to [qārā'](../../strongs/h/h7125.md) me: and [dabar](../../strongs/h/h1697.md) he [ra'ah](../../strongs/h/h7200.md) me I will [nāḡaḏ](../../strongs/h/h5046.md) thee. And he [yālaḵ](../../strongs/h/h3212.md) to a [šᵊp̄î](../../strongs/h/h8205.md).

<a name="numbers_23_4"></a>Numbers 23:4

And ['Elohiym](../../strongs/h/h430.md) [qārâ](../../strongs/h/h7136.md) [Bilʿām](../../strongs/h/h1109.md): and he ['āmar](../../strongs/h/h559.md) unto him, I have ['arak](../../strongs/h/h6186.md) seven [mizbeach](../../strongs/h/h4196.md), and I have [ʿālâ](../../strongs/h/h5927.md) upon every [mizbeach](../../strongs/h/h4196.md) a [par](../../strongs/h/h6499.md) and an ['ayil](../../strongs/h/h352.md).

<a name="numbers_23_5"></a>Numbers 23:5

And [Yĕhovah](../../strongs/h/h3068.md) [śûm](../../strongs/h/h7760.md) a [dabar](../../strongs/h/h1697.md) in [Bilʿām](../../strongs/h/h1109.md) [peh](../../strongs/h/h6310.md), and ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md) unto [Bālāq](../../strongs/h/h1111.md), and thus thou shalt [dabar](../../strongs/h/h1696.md).

<a name="numbers_23_6"></a>Numbers 23:6

And he [shuwb](../../strongs/h/h7725.md) unto him, and, lo, he [nāṣaḇ](../../strongs/h/h5324.md) by his [ʿōlâ](../../strongs/h/h5930.md), he, and all the [śar](../../strongs/h/h8269.md) of [Mô'āḇ](../../strongs/h/h4124.md).

<a name="numbers_23_7"></a>Numbers 23:7

And he [nasa'](../../strongs/h/h5375.md) his [māšāl](../../strongs/h/h4912.md), and ['āmar](../../strongs/h/h559.md), [Bālāq](../../strongs/h/h1111.md) the [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md) hath [nachah](../../strongs/h/h5148.md) me from ['Ărām](../../strongs/h/h758.md), out of the [har](../../strongs/h/h2042.md) of the [qeḏem](../../strongs/h/h6924.md), [yālaḵ](../../strongs/h/h3212.md), ['arar](../../strongs/h/h779.md) me [Ya'aqob](../../strongs/h/h3290.md), and [yālaḵ](../../strongs/h/h3212.md), [za'am](../../strongs/h/h2194.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_23_8"></a>Numbers 23:8

How shall I [nāqaḇ](../../strongs/h/h5344.md), whom ['el](../../strongs/h/h410.md) hath not [qāḇaḇ](../../strongs/h/h6895.md)? or how shall I [za'am](../../strongs/h/h2194.md), whom [Yĕhovah](../../strongs/h/h3068.md) hath not [za'am](../../strongs/h/h2194.md)?

<a name="numbers_23_9"></a>Numbers 23:9

For from the [ro'sh](../../strongs/h/h7218.md) of the [tsuwr](../../strongs/h/h6697.md) I [ra'ah](../../strongs/h/h7200.md) him, and from the [giḇʿâ](../../strongs/h/h1389.md) I [šûr](../../strongs/h/h7789.md) him: lo, the ['am](../../strongs/h/h5971.md) shall [shakan](../../strongs/h/h7931.md) [bāḏāḏ](../../strongs/h/h910.md), and shall not be [chashab](../../strongs/h/h2803.md) among the [gowy](../../strongs/h/h1471.md).

<a name="numbers_23_10"></a>Numbers 23:10

Who can [mānâ](../../strongs/h/h4487.md) the ['aphar](../../strongs/h/h6083.md) of [Ya'aqob](../../strongs/h/h3290.md), and the [mispār](../../strongs/h/h4557.md) of the [rōḇaʿ](../../strongs/h/h7255.md) of [Yisra'el](../../strongs/h/h3478.md)? Let me [muwth](../../strongs/h/h4191.md) [nephesh](../../strongs/h/h5315.md) the [maveth](../../strongs/h/h4194.md) of the [yashar](../../strongs/h/h3477.md), and let my ['aḥărîṯ](../../strongs/h/h319.md) end be like his!

<a name="numbers_23_11"></a>Numbers 23:11

And [Bālāq](../../strongs/h/h1111.md) ['āmar](../../strongs/h/h559.md) unto [Bilʿām](../../strongs/h/h1109.md), What hast thou ['asah](../../strongs/h/h6213.md) unto me? I [laqach](../../strongs/h/h3947.md) thee to [qāḇaḇ](../../strongs/h/h6895.md) mine ['oyeb](../../strongs/h/h341.md), and, behold, thou hast [barak](../../strongs/h/h1288.md) [barak](../../strongs/h/h1288.md) them.

<a name="numbers_23_12"></a>Numbers 23:12

And he ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), Must I not [shamar](../../strongs/h/h8104.md) to [dabar](../../strongs/h/h1696.md) that which [Yĕhovah](../../strongs/h/h3068.md) hath [śûm](../../strongs/h/h7760.md) in my [peh](../../strongs/h/h6310.md)?

<a name="numbers_23_13"></a>Numbers 23:13

And [Bālāq](../../strongs/h/h1111.md) ['āmar](../../strongs/h/h559.md) unto him, [yālaḵ](../../strongs/h/h3212.md), I pray thee, with me unto ['aḥēr](../../strongs/h/h312.md) [maqowm](../../strongs/h/h4725.md), from whence thou mayest [ra'ah](../../strongs/h/h7200.md) them: thou shalt [ra'ah](../../strongs/h/h7200.md) ['ep̄es](../../strongs/h/h657.md) the [qāṣê](../../strongs/h/h7097.md) of them, and shalt not [ra'ah](../../strongs/h/h7200.md) them all: and [qāḇaḇ](../../strongs/h/h6895.md) me them from thence.

<a name="numbers_23_14"></a>Numbers 23:14

And he [laqach](../../strongs/h/h3947.md) him into the [sadeh](../../strongs/h/h7704.md) of [ṣp̄ym](../../strongs/h/h6839.md) [tsaphah](../../strongs/h/h6822.md), to the [ro'sh](../../strongs/h/h7218.md) of [Pisgâ](../../strongs/h/h6449.md), and [bānâ](../../strongs/h/h1129.md) seven [mizbeach](../../strongs/h/h4196.md), and [ʿālâ](../../strongs/h/h5927.md) a [par](../../strongs/h/h6499.md) and an ['ayil](../../strongs/h/h352.md) on every [mizbeach](../../strongs/h/h4196.md).

<a name="numbers_23_15"></a>Numbers 23:15

And he ['āmar](../../strongs/h/h559.md) unto [Bālāq](../../strongs/h/h1111.md), [yatsab](../../strongs/h/h3320.md) here by thy [ʿōlâ](../../strongs/h/h5930.md), while I [qārâ](../../strongs/h/h7136.md) [kô](../../strongs/h/h3541.md).

<a name="numbers_23_16"></a>Numbers 23:16

And [Yĕhovah](../../strongs/h/h3068.md) [qārâ](../../strongs/h/h7136.md) [Bilʿām](../../strongs/h/h1109.md), and [śûm](../../strongs/h/h7760.md) a [dabar](../../strongs/h/h1697.md) in his [peh](../../strongs/h/h6310.md), and ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md) unto [Bālāq](../../strongs/h/h1111.md), and [dabar](../../strongs/h/h1696.md) thus.

<a name="numbers_23_17"></a>Numbers 23:17

And when he [bow'](../../strongs/h/h935.md) to him, behold, he [nāṣaḇ](../../strongs/h/h5324.md) by his an [ʿōlâ](../../strongs/h/h5930.md), and the [śar](../../strongs/h/h8269.md) of [Mô'āḇ](../../strongs/h/h4124.md) with him. And [Bālāq](../../strongs/h/h1111.md) ['āmar](../../strongs/h/h559.md) unto him, What hath [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md)?

<a name="numbers_23_18"></a>Numbers 23:18

And he [nasa'](../../strongs/h/h5375.md) his [māšāl](../../strongs/h/h4912.md), and ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md), [Bālāq](../../strongs/h/h1111.md), and [shama'](../../strongs/h/h8085.md); ['azan](../../strongs/h/h238.md) unto me, thou [ben](../../strongs/h/h1121.md) of [Tṣipôr](../../strongs/h/h6834.md):

<a name="numbers_23_19"></a>Numbers 23:19

['el](../../strongs/h/h410.md) is not an ['iysh](../../strongs/h/h376.md), that he should [kāzaḇ](../../strongs/h/h3576.md); neither the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md), that he should [nacham](../../strongs/h/h5162.md): hath he ['āmar](../../strongs/h/h559.md), and shall he not ['asah](../../strongs/h/h6213.md) it? or hath he [dabar](../../strongs/h/h1696.md), and shall he not [quwm](../../strongs/h/h6965.md) it?

<a name="numbers_23_20"></a>Numbers 23:20

Behold, I have [laqach](../../strongs/h/h3947.md) to [barak](../../strongs/h/h1288.md): and he hath [barak](../../strongs/h/h1288.md); and I cannot [shuwb](../../strongs/h/h7725.md) it.

<a name="numbers_23_21"></a>Numbers 23:21

He hath not [nabat](../../strongs/h/h5027.md) ['aven](../../strongs/h/h205.md) in [Ya'aqob](../../strongs/h/h3290.md), neither hath he [ra'ah](../../strongs/h/h7200.md) ['amal](../../strongs/h/h5999.md) in [Yisra'el](../../strongs/h/h3478.md): [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md) is with him, and the [tᵊrûʿâ](../../strongs/h/h8643.md) of a [melek](../../strongs/h/h4428.md) is among them.

<a name="numbers_23_22"></a>Numbers 23:22

['el](../../strongs/h/h410.md) [yāṣā'](../../strongs/h/h3318.md) them of [Mitsrayim](../../strongs/h/h4714.md); he hath as it were the [tôʿāp̄ôṯ](../../strongs/h/h8443.md) of a [rᵊ'ēm](../../strongs/h/h7214.md).

<a name="numbers_23_23"></a>Numbers 23:23

Surely there is no [naḥaš](../../strongs/h/h5173.md) against [Ya'aqob](../../strongs/h/h3290.md), neither is there any [qesem](../../strongs/h/h7081.md) against [Yisra'el](../../strongs/h/h3478.md): according to this [ʿēṯ](../../strongs/h/h6256.md) it shall be ['āmar](../../strongs/h/h559.md) of [Ya'aqob](../../strongs/h/h3290.md) and of [Yisra'el](../../strongs/h/h3478.md), What hath ['el](../../strongs/h/h410.md) [pa'al](../../strongs/h/h6466.md)!

<a name="numbers_23_24"></a>Numbers 23:24

Behold, the ['am](../../strongs/h/h5971.md) shall [quwm](../../strongs/h/h6965.md) as a [lāḇî'](../../strongs/h/h3833.md), and [nasa'](../../strongs/h/h5375.md) himself as an ['ariy](../../strongs/h/h738.md): he shall not [shakab](../../strongs/h/h7901.md) until he ['akal](../../strongs/h/h398.md) of the [ṭerep̄](../../strongs/h/h2964.md), and [šāṯâ](../../strongs/h/h8354.md) the [dam](../../strongs/h/h1818.md) of the [ḥālāl](../../strongs/h/h2491.md).

<a name="numbers_23_25"></a>Numbers 23:25

And [Bālāq](../../strongs/h/h1111.md) ['āmar](../../strongs/h/h559.md) unto [Bilʿām](../../strongs/h/h1109.md), Neither [nāqaḇ](../../strongs/h/h5344.md) them at [qāḇaḇ](../../strongs/h/h6895.md), nor [barak](../../strongs/h/h1288.md) [barak](../../strongs/h/h1288.md) them.

<a name="numbers_23_26"></a>Numbers 23:26

But [Bilʿām](../../strongs/h/h1109.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto [Bālāq](../../strongs/h/h1111.md), [dabar](../../strongs/h/h1696.md) not I thee, ['āmar](../../strongs/h/h559.md), All that [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md), that I must ['asah](../../strongs/h/h6213.md)?

<a name="numbers_23_27"></a>Numbers 23:27

And [Bālāq](../../strongs/h/h1111.md) ['āmar](../../strongs/h/h559.md) unto [Bilʿām](../../strongs/h/h1109.md), [yālaḵ](../../strongs/h/h3212.md), I pray thee, I will [laqach](../../strongs/h/h3947.md) thee unto ['aḥēr](../../strongs/h/h312.md) [maqowm](../../strongs/h/h4725.md); peradventure it will [yashar](../../strongs/h/h3474.md) ['ayin](../../strongs/h/h5869.md) ['Elohiym](../../strongs/h/h430.md) that thou mayest [qāḇaḇ](../../strongs/h/h6895.md) me them from thence.

<a name="numbers_23_28"></a>Numbers 23:28

And [Bālāq](../../strongs/h/h1111.md) [laqach](../../strongs/h/h3947.md) [Bilʿām](../../strongs/h/h1109.md) unto the [ro'sh](../../strongs/h/h7218.md) of [P̄ᵊʿôr](../../strongs/h/h6465.md), that [šāqap̄](../../strongs/h/h8259.md) [paniym](../../strongs/h/h6440.md) [yᵊšîmôn](../../strongs/h/h3452.md).

<a name="numbers_23_29"></a>Numbers 23:29

And [Bilʿām](../../strongs/h/h1109.md) ['āmar](../../strongs/h/h559.md) unto [Bālāq](../../strongs/h/h1111.md), [bānâ](../../strongs/h/h1129.md) me here seven [mizbeach](../../strongs/h/h4196.md), and [kuwn](../../strongs/h/h3559.md) me here seven [par](../../strongs/h/h6499.md) and seven ['ayil](../../strongs/h/h352.md).

<a name="numbers_23_30"></a>Numbers 23:30

And [Bālāq](../../strongs/h/h1111.md) ['asah](../../strongs/h/h6213.md) as [Bilʿām](../../strongs/h/h1109.md) had ['āmar](../../strongs/h/h559.md), and [ʿālâ](../../strongs/h/h5927.md) a [par](../../strongs/h/h6499.md) and an ['ayil](../../strongs/h/h352.md) on every [mizbeach](../../strongs/h/h4196.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 22](numbers_22.md) - [Numbers 24](numbers_24.md)