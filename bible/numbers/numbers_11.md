# [Numbers 11](https://www.blueletterbible.org/kjv/num/11)

<a name="numbers_11_1"></a>Numbers 11:1

And when the ['am](../../strongs/h/h5971.md) ['ānan](../../strongs/h/h596.md), it [ra'](../../strongs/h/h7451.md) ['ozen](../../strongs/h/h241.md) [Yĕhovah](../../strongs/h/h3068.md): and [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) it; and his ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md); and the ['esh](../../strongs/h/h784.md) of [Yĕhovah](../../strongs/h/h3068.md) [bāʿar](../../strongs/h/h1197.md) among them, and ['akal](../../strongs/h/h398.md) them that were in the [qāṣê](../../strongs/h/h7097.md) of the [maḥănê](../../strongs/h/h4264.md).

<a name="numbers_11_2"></a>Numbers 11:2

And the ['am](../../strongs/h/h5971.md) [ṣāʿaq](../../strongs/h/h6817.md) unto [Mōshe](../../strongs/h/h4872.md); and when [Mōshe](../../strongs/h/h4872.md) [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md), the ['esh](../../strongs/h/h784.md) was [šāqaʿ](../../strongs/h/h8257.md).

<a name="numbers_11_3"></a>Numbers 11:3

And he [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of the [maqowm](../../strongs/h/h4725.md) [taḇʿērâ](../../strongs/h/h8404.md): because the ['esh](../../strongs/h/h784.md) of [Yĕhovah](../../strongs/h/h3068.md) [bāʿar](../../strongs/h/h1197.md) among them.

<a name="numbers_11_4"></a>Numbers 11:4

And the ['ăsap̄sup̄](../../strongs/h/h628.md) that was [qereḇ](../../strongs/h/h7130.md) them ['āvâ](../../strongs/h/h183.md) [ta'avah](../../strongs/h/h8378.md): and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) also [bāḵâ](../../strongs/h/h1058.md) [shuwb](../../strongs/h/h7725.md), and ['āmar](../../strongs/h/h559.md), Who shall give us [basar](../../strongs/h/h1320.md) to ['akal](../../strongs/h/h398.md)?

<a name="numbers_11_5"></a>Numbers 11:5

We [zakar](../../strongs/h/h2142.md) the [dāḡâ](../../strongs/h/h1710.md), which we did ['akal](../../strongs/h/h398.md) in [Mitsrayim](../../strongs/h/h4714.md) [ḥinnām](../../strongs/h/h2600.md); the [qiššu'â](../../strongs/h/h7180.md), and the ['ăḇaṭṭiḥîm](../../strongs/h/h20.md), and the [chatsiyr](../../strongs/h/h2682.md), and the [beṣel](../../strongs/h/h1211.md), and the [šûm](../../strongs/h/h7762.md):

<a name="numbers_11_6"></a>Numbers 11:6

But now our [nephesh](../../strongs/h/h5315.md) is [yāḇēš](../../strongs/h/h3002.md): there is nothing at all, beside this [man](../../strongs/h/h4478.md), before our ['ayin](../../strongs/h/h5869.md).

<a name="numbers_11_7"></a>Numbers 11:7

And the [man](../../strongs/h/h4478.md) was as [gaḏ](../../strongs/h/h1407.md) [zera'](../../strongs/h/h2233.md), and the ['ayin](../../strongs/h/h5869.md) thereof as the ['ayin](../../strongs/h/h5869.md) of [bᵊḏōlaḥ](../../strongs/h/h916.md).

<a name="numbers_11_8"></a>Numbers 11:8

And the ['am](../../strongs/h/h5971.md) [šûṭ](../../strongs/h/h7751.md), and [lāqaṭ](../../strongs/h/h3950.md) it, and [ṭāḥan](../../strongs/h/h2912.md) it in [rēḥayim](../../strongs/h/h7347.md), or [dûḵ](../../strongs/h/h1743.md) it in a [mᵊḏōḵâ](../../strongs/h/h4085.md), and [bāšal](../../strongs/h/h1310.md) it in [pārûr](../../strongs/h/h6517.md), and ['asah](../../strongs/h/h6213.md) [ʿugâ](../../strongs/h/h5692.md) of it: and the [ṭaʿam](../../strongs/h/h2940.md) of it was as the [ṭaʿam](../../strongs/h/h2940.md) of [lᵊšaḏ](../../strongs/h/h3955.md) [šemen](../../strongs/h/h8081.md).

<a name="numbers_11_9"></a>Numbers 11:9

And when the [ṭal](../../strongs/h/h2919.md) [yarad](../../strongs/h/h3381.md) upon the [maḥănê](../../strongs/h/h4264.md) in the [layil](../../strongs/h/h3915.md), the [man](../../strongs/h/h4478.md) [yarad](../../strongs/h/h3381.md) upon it.

<a name="numbers_11_10"></a>Numbers 11:10

Then [Mōshe](../../strongs/h/h4872.md) [shama'](../../strongs/h/h8085.md) the ['am](../../strongs/h/h5971.md) [bāḵâ](../../strongs/h/h1058.md) throughout their [mišpāḥâ](../../strongs/h/h4940.md), every ['iysh](../../strongs/h/h376.md) in the [peṯaḥ](../../strongs/h/h6607.md) of his ['ohel](../../strongs/h/h168.md): and the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) [me'od](../../strongs/h/h3966.md); [Mōshe](../../strongs/h/h4872.md) also was [ra'a'](../../strongs/h/h7489.md) ['ayin](../../strongs/h/h5869.md).

<a name="numbers_11_11"></a>Numbers 11:11

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto [Yĕhovah](../../strongs/h/h3068.md), Wherefore hast thou [ra'a'](../../strongs/h/h7489.md) thy ['ebed](../../strongs/h/h5650.md)? and wherefore have I not [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md), that thou [śûm](../../strongs/h/h7760.md) the [maśśā'](../../strongs/h/h4853.md) of all this ['am](../../strongs/h/h5971.md) upon me?

<a name="numbers_11_12"></a>Numbers 11:12

Have I [harah](../../strongs/h/h2029.md) all this ['am](../../strongs/h/h5971.md)? have I [yalad](../../strongs/h/h3205.md) them, that thou shouldest ['āmar](../../strongs/h/h559.md) unto me, [nasa'](../../strongs/h/h5375.md) them in thy [ḥêq](../../strongs/h/h2436.md), as an ['aman](../../strongs/h/h539.md) [nasa'](../../strongs/h/h5375.md) the [yānaq](../../strongs/h/h3243.md), unto the ['ăḏāmâ](../../strongs/h/h127.md) which thou [shaba'](../../strongs/h/h7650.md) unto their ['ab](../../strongs/h/h1.md)?

<a name="numbers_11_13"></a>Numbers 11:13

Whence should I have [basar](../../strongs/h/h1320.md) to [nathan](../../strongs/h/h5414.md) unto all this ['am](../../strongs/h/h5971.md)? for they [bāḵâ](../../strongs/h/h1058.md) unto me, ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) us [basar](../../strongs/h/h1320.md), that we may ['akal](../../strongs/h/h398.md).

<a name="numbers_11_14"></a>Numbers 11:14

I am not [yakol](../../strongs/h/h3201.md) to [nasa'](../../strongs/h/h5375.md) all this ['am](../../strongs/h/h5971.md) [baḏ](../../strongs/h/h905.md), because it is too [kāḇēḏ](../../strongs/h/h3515.md) for me.

<a name="numbers_11_15"></a>Numbers 11:15

And if thou ['asah](../../strongs/h/h6213.md) thus with me, [harag](../../strongs/h/h2026.md) me, I pray thee, [harag](../../strongs/h/h2026.md), if I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md); and let me not [ra'ah](../../strongs/h/h7200.md) my [ra'](../../strongs/h/h7451.md).

<a name="numbers_11_16"></a>Numbers 11:16

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āsap̄](../../strongs/h/h622.md) unto me seventy ['iysh](../../strongs/h/h376.md) of the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md), whom thou [yada'](../../strongs/h/h3045.md) to be the [zāqēn](../../strongs/h/h2205.md) of the ['am](../../strongs/h/h5971.md), and [šāṭar](../../strongs/h/h7860.md) over them; and [laqach](../../strongs/h/h3947.md) them unto the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), that they may [yatsab](../../strongs/h/h3320.md) there with thee.

<a name="numbers_11_17"></a>Numbers 11:17

And I will [yarad](../../strongs/h/h3381.md) and [dabar](../../strongs/h/h1696.md) with thee there: and I will ['āṣal](../../strongs/h/h680.md) of the [ruwach](../../strongs/h/h7307.md) which is upon thee, and will [śûm](../../strongs/h/h7760.md) it upon them; and they shall [nasa'](../../strongs/h/h5375.md) the [maśśā'](../../strongs/h/h4853.md) of the ['am](../../strongs/h/h5971.md) with thee, that thou [nasa'](../../strongs/h/h5375.md) it not thyself alone.

<a name="numbers_11_18"></a>Numbers 11:18

And ['āmar](../../strongs/h/h559.md) thou unto the ['am](../../strongs/h/h5971.md), [qadash](../../strongs/h/h6942.md) yourselves against [māḥār](../../strongs/h/h4279.md), and ye shall ['akal](../../strongs/h/h398.md) [basar](../../strongs/h/h1320.md): for ye have [bāḵâ](../../strongs/h/h1058.md) in the ['ozen](../../strongs/h/h241.md) of [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), Who shall give us [basar](../../strongs/h/h1320.md) to ['akal](../../strongs/h/h398.md)? for it was [ṭôḇ](../../strongs/h/h2895.md) with us in [Mitsrayim](../../strongs/h/h4714.md): therefore [Yĕhovah](../../strongs/h/h3068.md) will [nathan](../../strongs/h/h5414.md) you [basar](../../strongs/h/h1320.md), and ye shall ['akal](../../strongs/h/h398.md).

<a name="numbers_11_19"></a>Numbers 11:19

Ye shall not ['akal](../../strongs/h/h398.md) one [yowm](../../strongs/h/h3117.md), nor two [yowm](../../strongs/h/h3117.md), nor five [yowm](../../strongs/h/h3117.md), neither ten [yowm](../../strongs/h/h3117.md), nor twenty [yowm](../../strongs/h/h3117.md);

<a name="numbers_11_20"></a>Numbers 11:20

But even a [ḥōḏeš](../../strongs/h/h2320.md) [yowm](../../strongs/h/h3117.md), until it [yāṣā'](../../strongs/h/h3318.md) at your ['aph](../../strongs/h/h639.md), and it be [zārā'](../../strongs/h/h2214.md) unto you: because that ye have [mā'as](../../strongs/h/h3988.md) [Yĕhovah](../../strongs/h/h3068.md) which is [qereḇ](../../strongs/h/h7130.md) you, and have [bāḵâ](../../strongs/h/h1058.md) [paniym](../../strongs/h/h6440.md) him, ['āmar](../../strongs/h/h559.md), Why [yāṣā'](../../strongs/h/h3318.md) we out of [Mitsrayim](../../strongs/h/h4714.md)?

<a name="numbers_11_21"></a>Numbers 11:21

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), The ['am](../../strongs/h/h5971.md), [qereḇ](../../strongs/h/h7130.md) whom I am, are six hundred thousand [raḡlî](../../strongs/h/h7273.md); and thou hast ['āmar](../../strongs/h/h559.md), I will [nathan](../../strongs/h/h5414.md) them [basar](../../strongs/h/h1320.md), that they may ['akal](../../strongs/h/h398.md) a [yowm](../../strongs/h/h3117.md) [ḥōḏeš](../../strongs/h/h2320.md).

<a name="numbers_11_22"></a>Numbers 11:22

Shall the [tso'n](../../strongs/h/h6629.md) and the [bāqār](../../strongs/h/h1241.md) be [šāḥaṭ](../../strongs/h/h7819.md) for them, to [māṣā'](../../strongs/h/h4672.md) them?  or shall all the [dag](../../strongs/h/h1709.md) of the [yam](../../strongs/h/h3220.md) be ['āsap̄](../../strongs/h/h622.md) for them, to [māṣā'](../../strongs/h/h4672.md) them?

<a name="numbers_11_23"></a>Numbers 11:23

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), Is [Yĕhovah](../../strongs/h/h3068.md) [yad](../../strongs/h/h3027.md) [qāṣar](../../strongs/h/h7114.md)? thou shalt [ra'ah](../../strongs/h/h7200.md) now whether my [dabar](../../strongs/h/h1697.md) shall [qārâ](../../strongs/h/h7136.md) unto thee or not.

<a name="numbers_11_24"></a>Numbers 11:24

And [Mōshe](../../strongs/h/h4872.md) [yāṣā'](../../strongs/h/h3318.md), and [dabar](../../strongs/h/h1696.md) the ['am](../../strongs/h/h5971.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), and ['āsap̄](../../strongs/h/h622.md) the seventy ['iysh](../../strongs/h/h376.md) of the [zāqēn](../../strongs/h/h2205.md) of the ['am](../../strongs/h/h5971.md), and ['amad](../../strongs/h/h5975.md) them [cabiyb](../../strongs/h/h5439.md) the ['ohel](../../strongs/h/h168.md).

<a name="numbers_11_25"></a>Numbers 11:25

And [Yĕhovah](../../strongs/h/h3068.md) [yarad](../../strongs/h/h3381.md) in a [ʿānān](../../strongs/h/h6051.md), and [dabar](../../strongs/h/h1696.md) unto him, and ['āṣal](../../strongs/h/h680.md) of the [ruwach](../../strongs/h/h7307.md) that was upon him, and [nathan](../../strongs/h/h5414.md) it unto the seventy [zāqēn](../../strongs/h/h2205.md) ['iysh](../../strongs/h/h376.md): and it came to pass, that, when the [ruwach](../../strongs/h/h7307.md) [nuwach](../../strongs/h/h5117.md) upon them, they [nāḇā'](../../strongs/h/h5012.md), and did [yāsap̄](../../strongs/h/h3254.md).

<a name="numbers_11_26"></a>Numbers 11:26

But there [šā'ar](../../strongs/h/h7604.md) two of the ['enowsh](../../strongs/h/h582.md) in the [maḥănê](../../strongs/h/h4264.md), the [shem](../../strongs/h/h8034.md) of the one was ['eldāḏ](../../strongs/h/h419.md), and the [shem](../../strongs/h/h8034.md) of the other [mêḏāḏ](../../strongs/h/h4312.md): and the [ruwach](../../strongs/h/h7307.md) [nuwach](../../strongs/h/h5117.md) upon them; and they were of them that were [kāṯaḇ](../../strongs/h/h3789.md), but [yāṣā'](../../strongs/h/h3318.md) not unto the ['ohel](../../strongs/h/h168.md): and they [nāḇā'](../../strongs/h/h5012.md) in the [maḥănê](../../strongs/h/h4264.md).

<a name="numbers_11_27"></a>Numbers 11:27

And there [rûṣ](../../strongs/h/h7323.md) a [naʿar](../../strongs/h/h5288.md), and [nāḡaḏ](../../strongs/h/h5046.md) [Mōshe](../../strongs/h/h4872.md), and ['āmar](../../strongs/h/h559.md), ['eldāḏ](../../strongs/h/h419.md) and [mêḏāḏ](../../strongs/h/h4312.md) do [nāḇā'](../../strongs/h/h5012.md) in the [maḥănê](../../strongs/h/h4264.md).

<a name="numbers_11_28"></a>Numbers 11:28

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), the [sharath](../../strongs/h/h8334.md) of [Mōshe](../../strongs/h/h4872.md), one of his [bᵊḥurîm](../../strongs/h/h979.md), ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), My ['adown](../../strongs/h/h113.md) [Mōshe](../../strongs/h/h4872.md), [kālā'](../../strongs/h/h3607.md) them.

<a name="numbers_11_29"></a>Numbers 11:29

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto him, [qānā'](../../strongs/h/h7065.md) thou for my sake? [nathan](../../strongs/h/h5414.md) [Yĕhovah](../../strongs/h/h3068.md) ['am](../../strongs/h/h5971.md) were [nāḇî'](../../strongs/h/h5030.md), and that [Yĕhovah](../../strongs/h/h3068.md) would [nathan](../../strongs/h/h5414.md) his [ruwach](../../strongs/h/h7307.md) upon them!

<a name="numbers_11_30"></a>Numbers 11:30

And [Mōshe](../../strongs/h/h4872.md) ['āsap̄](../../strongs/h/h622.md) him into the [maḥănê](../../strongs/h/h4264.md), he and the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_11_31"></a>Numbers 11:31

And there [nāsaʿ](../../strongs/h/h5265.md) a [ruwach](../../strongs/h/h7307.md) from [Yĕhovah](../../strongs/h/h3068.md), and [gûz](../../strongs/h/h1468.md) [śᵊlav](../../strongs/h/h7958.md) from the [yam](../../strongs/h/h3220.md), and let them [nāṭaš](../../strongs/h/h5203.md) by the [maḥănê](../../strongs/h/h4264.md), as it were a [yowm](../../strongs/h/h3117.md) [derek](../../strongs/h/h1870.md) [kô](../../strongs/h/h3541.md), and as it were a [yowm](../../strongs/h/h3117.md) [derek](../../strongs/h/h1870.md) on the other [kô](../../strongs/h/h3541.md), [cabiyb](../../strongs/h/h5439.md) the [maḥănê](../../strongs/h/h4264.md), and as it were two ['ammâ](../../strongs/h/h520.md) high upon the [paniym](../../strongs/h/h6440.md) of the ['erets](../../strongs/h/h776.md).

<a name="numbers_11_32"></a>Numbers 11:32

And the ['am](../../strongs/h/h5971.md) [quwm](../../strongs/h/h6965.md) all that [yowm](../../strongs/h/h3117.md), and all that [layil](../../strongs/h/h3915.md), and all the [māḥŏrāṯ](../../strongs/h/h4283.md) [yowm](../../strongs/h/h3117.md), and they ['āsap̄](../../strongs/h/h622.md) the [śᵊlav](../../strongs/h/h7958.md): he that [māʿaṭ](../../strongs/h/h4591.md) ['āsap̄](../../strongs/h/h622.md) ten [ḥōmer](../../strongs/h/h2563.md): and they [šāṭaḥ](../../strongs/h/h7849.md) [šāṭaḥ](../../strongs/h/h7849.md) them for themselves [cabiyb](../../strongs/h/h5439.md) the [maḥănê](../../strongs/h/h4264.md).

<a name="numbers_11_33"></a>Numbers 11:33

And while the [basar](../../strongs/h/h1320.md) was yet between their [šēn](../../strongs/h/h8127.md), ere it was [karath](../../strongs/h/h3772.md), the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against the ['am](../../strongs/h/h5971.md), and [Yĕhovah](../../strongs/h/h3068.md) [nakah](../../strongs/h/h5221.md) the ['am](../../strongs/h/h5971.md) with a [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md) [makâ](../../strongs/h/h4347.md).

<a name="numbers_11_34"></a>Numbers 11:34

And he [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of that [maqowm](../../strongs/h/h4725.md) [Qiḇrôṯ hata'ăvâ](../../strongs/h/h6914.md): because there they [qāḇar](../../strongs/h/h6912.md) the ['am](../../strongs/h/h5971.md) that ['āvâ](../../strongs/h/h183.md).

<a name="numbers_11_35"></a>Numbers 11:35

And the ['am](../../strongs/h/h5971.md) [nāsaʿ](../../strongs/h/h5265.md) from [Qiḇrôṯ hata'ăvâ](../../strongs/h/h6914.md) unto [Ḥăṣērôṯ](../../strongs/h/h2698.md); and abode at [Ḥăṣērôṯ](../../strongs/h/h2698.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 10](numbers_10.md) - [Numbers 12](numbers_12.md)