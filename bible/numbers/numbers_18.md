# [Numbers 18](https://www.blueletterbible.org/kjv/num/18)

<a name="numbers_18_1"></a>Numbers 18:1

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), Thou and thy [ben](../../strongs/h/h1121.md) and thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) with thee shall [nasa'](../../strongs/h/h5375.md) the ['avon](../../strongs/h/h5771.md) of the [miqdash](../../strongs/h/h4720.md): and thou and thy [ben](../../strongs/h/h1121.md) with thee shall [nasa'](../../strongs/h/h5375.md) the ['avon](../../strongs/h/h5771.md) of your [kᵊhunnâ](../../strongs/h/h3550.md).

<a name="numbers_18_2"></a>Numbers 18:2

And thy ['ach](../../strongs/h/h251.md) also of the [maṭṭê](../../strongs/h/h4294.md) of [Lēvî](../../strongs/h/h3878.md), the [shebet](../../strongs/h/h7626.md) of thy ['ab](../../strongs/h/h1.md), [qāraḇ](../../strongs/h/h7126.md) thou with thee, that they may be [lāvâ](../../strongs/h/h3867.md) unto thee, and [sharath](../../strongs/h/h8334.md) unto thee: but thou and thy [ben](../../strongs/h/h1121.md) with thee shall minister [paniym](../../strongs/h/h6440.md) the ['ohel](../../strongs/h/h168.md) of [ʿēḏûṯ](../../strongs/h/h5715.md).

<a name="numbers_18_3"></a>Numbers 18:3

And they shall [shamar](../../strongs/h/h8104.md) thy [mišmereṯ](../../strongs/h/h4931.md), and the [mišmereṯ](../../strongs/h/h4931.md) of all the ['ohel](../../strongs/h/h168.md): only they shall not [qāraḇ](../../strongs/h/h7126.md) the [kĕliy](../../strongs/h/h3627.md) of the [qodesh](../../strongs/h/h6944.md) and the [mizbeach](../../strongs/h/h4196.md), that neither they, nor ye also, [muwth](../../strongs/h/h4191.md).

<a name="numbers_18_4"></a>Numbers 18:4

And they shall be [lāvâ](../../strongs/h/h3867.md) unto thee, and [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), for all the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the ['ohel](../../strongs/h/h168.md): and a [zûr](../../strongs/h/h2114.md) shall not [qāraḇ](../../strongs/h/h7126.md) unto you.

<a name="numbers_18_5"></a>Numbers 18:5

And ye shall [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of the [qodesh](../../strongs/h/h6944.md), and the [mišmereṯ](../../strongs/h/h4931.md) of the [mizbeach](../../strongs/h/h4196.md): that there be no [qeṣep̄](../../strongs/h/h7110.md) any more upon the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_18_6"></a>Numbers 18:6

And I, behold, I have [laqach](../../strongs/h/h3947.md) your ['ach](../../strongs/h/h251.md) the [Lᵊvî](../../strongs/h/h3881.md) from [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): to you they are [nathan](../../strongs/h/h5414.md) as a [matānâ](../../strongs/h/h4979.md) for [Yĕhovah](../../strongs/h/h3068.md), to ['abad](../../strongs/h/h5647.md) the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="numbers_18_7"></a>Numbers 18:7

Therefore thou and thy [ben](../../strongs/h/h1121.md) with thee shall [shamar](../../strongs/h/h8104.md) your [kᵊhunnâ](../../strongs/h/h3550.md) for every [dabar](../../strongs/h/h1697.md) of the [mizbeach](../../strongs/h/h4196.md), and [bayith](../../strongs/h/h1004.md) the [pārōḵeṯ](../../strongs/h/h6532.md); and ye shall ['abad](../../strongs/h/h5647.md): I have [nathan](../../strongs/h/h5414.md) your [kᵊhunnâ](../../strongs/h/h3550.md) unto you as a [ʿăḇōḏâ](../../strongs/h/h5656.md) of [matānâ](../../strongs/h/h4979.md): and the [zûr](../../strongs/h/h2114.md) that [qārēḇ](../../strongs/h/h7131.md) shall be put to [muwth](../../strongs/h/h4191.md).

<a name="numbers_18_8"></a>Numbers 18:8

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto ['Ahărôn](../../strongs/h/h175.md), Behold, I also have [nathan](../../strongs/h/h5414.md) thee the [mišmereṯ](../../strongs/h/h4931.md) of mine [tᵊrûmâ](../../strongs/h/h8641.md) of all the [qodesh](../../strongs/h/h6944.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); unto thee have I [nathan](../../strongs/h/h5414.md) them by reason of the [māšḥâ](../../strongs/h/h4888.md), and to thy [ben](../../strongs/h/h1121.md), by a [choq](../../strongs/h/h2706.md) ['owlam](../../strongs/h/h5769.md).

<a name="numbers_18_9"></a>Numbers 18:9

This shall be thine of the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md), reserved from the ['esh](../../strongs/h/h784.md): every [qorban](../../strongs/h/h7133.md) of theirs, every [minchah](../../strongs/h/h4503.md) of theirs, and every [chatta'ath](../../strongs/h/h2403.md) of theirs, and every ['āšām](../../strongs/h/h817.md) of theirs which they shall [shuwb](../../strongs/h/h7725.md) unto me, shall be [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) for thee and for thy [ben](../../strongs/h/h1121.md).

<a name="numbers_18_10"></a>Numbers 18:10

In the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) shalt thou ['akal](../../strongs/h/h398.md) it; every [zāḵār](../../strongs/h/h2145.md) shall ['akal](../../strongs/h/h398.md) it: it shall be [qodesh](../../strongs/h/h6944.md) unto thee.

<a name="numbers_18_11"></a>Numbers 18:11

And this is thine; the [tᵊrûmâ](../../strongs/h/h8641.md) of their [matān](../../strongs/h/h4976.md), with all the [tᵊnûp̄â](../../strongs/h/h8573.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): I have [nathan](../../strongs/h/h5414.md) them unto thee, and to thy [ben](../../strongs/h/h1121.md) and to thy [bath](../../strongs/h/h1323.md) with thee, by a [choq](../../strongs/h/h2706.md) ['owlam](../../strongs/h/h5769.md): every one that is [tahowr](../../strongs/h/h2889.md) in thy [bayith](../../strongs/h/h1004.md) shall ['akal](../../strongs/h/h398.md) of it.

<a name="numbers_18_12"></a>Numbers 18:12

All the [cheleb](../../strongs/h/h2459.md) of the [yiṣhār](../../strongs/h/h3323.md), and all the [cheleb](../../strongs/h/h2459.md) of the [tiyrowsh](../../strongs/h/h8492.md), and of the [dagan](../../strongs/h/h1715.md), the [re'shiyth](../../strongs/h/h7225.md) of them which they shall [nathan](../../strongs/h/h5414.md) unto [Yĕhovah](../../strongs/h/h3068.md), them have I [nathan](../../strongs/h/h5414.md) thee.

<a name="numbers_18_13"></a>Numbers 18:13

And whatsoever is [bikûr](../../strongs/h/h1061.md)in the ['erets](../../strongs/h/h776.md), which they shall [bow'](../../strongs/h/h935.md) unto [Yĕhovah](../../strongs/h/h3068.md), shall be thine; every one that is [tahowr](../../strongs/h/h2889.md) in thine [bayith](../../strongs/h/h1004.md) shall ['akal](../../strongs/h/h398.md) of it.

<a name="numbers_18_14"></a>Numbers 18:14

Every thing [ḥērem](../../strongs/h/h2764.md) in [Yisra'el](../../strongs/h/h3478.md) shall be thine.

<a name="numbers_18_15"></a>Numbers 18:15

Every thing that [peṭer](../../strongs/h/h6363.md) the [reḥem](../../strongs/h/h7358.md) in all [basar](../../strongs/h/h1320.md), which they [qāraḇ](../../strongs/h/h7126.md) unto [Yĕhovah](../../strongs/h/h3068.md), whether it be of ['adam](../../strongs/h/h120.md) or [bĕhemah](../../strongs/h/h929.md), shall be thine: nevertheless the [bᵊḵôr](../../strongs/h/h1060.md) of ['adam](../../strongs/h/h120.md) shalt thou [pāḏâ](../../strongs/h/h6299.md) [pāḏâ](../../strongs/h/h6299.md), and the [bᵊḵôr](../../strongs/h/h1060.md) of [tame'](../../strongs/h/h2931.md) [bĕhemah](../../strongs/h/h929.md) shalt thou [pāḏâ](../../strongs/h/h6299.md).

<a name="numbers_18_16"></a>Numbers 18:16

And those that are to be [pāḏâ](../../strongs/h/h6299.md) from a [ḥōḏeš](../../strongs/h/h2320.md) [ben](../../strongs/h/h1121.md) shalt thou [pāḏâ](../../strongs/h/h6299.md), according to thine [ʿēreḵ](../../strongs/h/h6187.md), for the [keceph](../../strongs/h/h3701.md) of five [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md), which is twenty [gērâ](../../strongs/h/h1626.md).

<a name="numbers_18_17"></a>Numbers 18:17

But the [bᵊḵôr](../../strongs/h/h1060.md) of a [showr](../../strongs/h/h7794.md), or the [bᵊḵôr](../../strongs/h/h1060.md) of a [keśeḇ](../../strongs/h/h3775.md), or the [bᵊḵôr](../../strongs/h/h1060.md) of an [ʿēz](../../strongs/h/h5795.md), thou shalt not [pāḏâ](../../strongs/h/h6299.md); they are [qodesh](../../strongs/h/h6944.md): thou shalt [zāraq](../../strongs/h/h2236.md) their [dam](../../strongs/h/h1818.md) upon the [mizbeach](../../strongs/h/h4196.md), and shalt [qāṭar](../../strongs/h/h6999.md) their [cheleb](../../strongs/h/h2459.md) for an ['iššê](../../strongs/h/h801.md), for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_18_18"></a>Numbers 18:18

And the [basar](../../strongs/h/h1320.md) of them shall be thine, as the [tᵊnûp̄â](../../strongs/h/h8573.md) [ḥāzê](../../strongs/h/h2373.md) and as the [yamiyn](../../strongs/h/h3225.md) [šôq](../../strongs/h/h7785.md) are thine.

<a name="numbers_18_19"></a>Numbers 18:19

All the [tᵊrûmâ](../../strongs/h/h8641.md) of the [qodesh](../../strongs/h/h6944.md), which the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ruwm](../../strongs/h/h7311.md) unto [Yĕhovah](../../strongs/h/h3068.md), have I [nathan](../../strongs/h/h5414.md) thee, and thy [ben](../../strongs/h/h1121.md) and thy [bath](../../strongs/h/h1323.md) with thee, by a [choq](../../strongs/h/h2706.md) ['owlam](../../strongs/h/h5769.md): it is a [bĕriyth](../../strongs/h/h1285.md) of [melaḥ](../../strongs/h/h4417.md) ['owlam](../../strongs/h/h5769.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) unto thee and to thy [zera'](../../strongs/h/h2233.md) with thee.

<a name="numbers_18_20"></a>Numbers 18:20

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), Thou shalt have no [nāḥal](../../strongs/h/h5157.md) in their ['erets](../../strongs/h/h776.md), neither shalt thou have any [cheleq](../../strongs/h/h2506.md) [tavek](../../strongs/h/h8432.md) them: I am thy [cheleq](../../strongs/h/h2506.md) and thine [nachalah](../../strongs/h/h5159.md) [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_18_21"></a>Numbers 18:21

And, behold, I have [nathan](../../strongs/h/h5414.md) the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md) all the [maʿăśēr](../../strongs/h/h4643.md) in [Yisra'el](../../strongs/h/h3478.md) for a [nachalah](../../strongs/h/h5159.md), [ḥēlep̄](../../strongs/h/h2500.md) their [ʿăḇōḏâ](../../strongs/h/h5656.md) which they ['abad](../../strongs/h/h5647.md), even the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="numbers_18_22"></a>Numbers 18:22

Neither must the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) henceforth [qāraḇ](../../strongs/h/h7126.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), lest they [nasa'](../../strongs/h/h5375.md) [ḥēṭĕ'](../../strongs/h/h2399.md), and [muwth](../../strongs/h/h4191.md).

<a name="numbers_18_23"></a>Numbers 18:23

But the [Lᵊvî](../../strongs/h/h3881.md) shall ['abad](../../strongs/h/h5647.md) the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and they shall [nasa'](../../strongs/h/h5375.md) their ['avon](../../strongs/h/h5771.md): it shall be a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) throughout your [dôr](../../strongs/h/h1755.md), that [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) they [nāḥal](../../strongs/h/h5157.md) no [nachalah](../../strongs/h/h5159.md).

<a name="numbers_18_24"></a>Numbers 18:24

But the [maʿăśēr](../../strongs/h/h4643.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which they [ruwm](../../strongs/h/h7311.md) as a [tᵊrûmâ](../../strongs/h/h8641.md) unto [Yĕhovah](../../strongs/h/h3068.md), I have [nathan](../../strongs/h/h5414.md) to the [Lᵊvî](../../strongs/h/h3881.md) to [nachalah](../../strongs/h/h5159.md): therefore I have ['āmar](../../strongs/h/h559.md) unto them, [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) they shall [nāḥal](../../strongs/h/h5157.md) no [nachalah](../../strongs/h/h5159.md).

<a name="numbers_18_25"></a>Numbers 18:25

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_18_26"></a>Numbers 18:26

Thus [dabar](../../strongs/h/h1696.md) unto the [Lᵊvî](../../strongs/h/h3881.md), and ['āmar](../../strongs/h/h559.md) unto them, When ye [laqach](../../strongs/h/h3947.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) the [maʿăśēr](../../strongs/h/h4643.md) which I have [nathan](../../strongs/h/h5414.md) you from them for your [nachalah](../../strongs/h/h5159.md), then ye shall [ruwm](../../strongs/h/h7311.md) up a [tᵊrûmâ](../../strongs/h/h8641.md) of it for [Yĕhovah](../../strongs/h/h3068.md), even a tenth of the [maʿăśēr](../../strongs/h/h4643.md).

<a name="numbers_18_27"></a>Numbers 18:27

And this your [tᵊrûmâ](../../strongs/h/h8641.md) shall be [chashab](../../strongs/h/h2803.md) unto you, as though it were the [dagan](../../strongs/h/h1715.md) of the [gōren](../../strongs/h/h1637.md), and as the [mᵊlē'â](../../strongs/h/h4395.md) of the [yeqeḇ](../../strongs/h/h3342.md).

<a name="numbers_18_28"></a>Numbers 18:28

Thus ye also shall [ruwm](../../strongs/h/h7311.md) a [tᵊrûmâ](../../strongs/h/h8641.md) unto [Yĕhovah](../../strongs/h/h3068.md) of all your [maʿăśēr](../../strongs/h/h4643.md), which ye [laqach](../../strongs/h/h3947.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); and ye shall [nathan](../../strongs/h/h5414.md) thereof [Yĕhovah](../../strongs/h/h3068.md) [tᵊrûmâ](../../strongs/h/h8641.md) to ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md).

<a name="numbers_18_29"></a>Numbers 18:29

Out of all your [matānâ](../../strongs/h/h4979.md) ye shall [ruwm](../../strongs/h/h7311.md) every [tᵊrûmâ](../../strongs/h/h8641.md) of [Yĕhovah](../../strongs/h/h3068.md), of all the [cheleb](../../strongs/h/h2459.md) thereof, even the [miqdash](../../strongs/h/h4720.md) thereof out of it.

<a name="numbers_18_30"></a>Numbers 18:30

Therefore thou shalt ['āmar](../../strongs/h/h559.md) unto them, When ye have [ruwm](../../strongs/h/h7311.md) the [cheleb](../../strongs/h/h2459.md) thereof from it, then it shall be [chashab](../../strongs/h/h2803.md) unto the [Lᵊvî](../../strongs/h/h3881.md) as the [tᵊḇû'â](../../strongs/h/h8393.md) of the [gōren](../../strongs/h/h1637.md), and as the [tᵊḇû'â](../../strongs/h/h8393.md) of the [yeqeḇ](../../strongs/h/h3342.md).

<a name="numbers_18_31"></a>Numbers 18:31

And ye shall ['akal](../../strongs/h/h398.md) it in every [maqowm](../../strongs/h/h4725.md), ye and your [bayith](../../strongs/h/h1004.md): it is your [śāḵār](../../strongs/h/h7939.md) [ḥēlep̄](../../strongs/h/h2500.md) your [ʿăḇōḏâ](../../strongs/h/h5656.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="numbers_18_32"></a>Numbers 18:32

And ye shall [nasa'](../../strongs/h/h5375.md) no [ḥēṭĕ'](../../strongs/h/h2399.md) by reason of it, when ye have [ruwm](../../strongs/h/h7311.md) from it the [cheleb](../../strongs/h/h2459.md) of it: neither shall ye [ḥālal](../../strongs/h/h2490.md) the [qodesh](../../strongs/h/h6944.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), lest ye [muwth](../../strongs/h/h4191.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 17](numbers_17.md) - [Numbers 19](numbers_19.md)