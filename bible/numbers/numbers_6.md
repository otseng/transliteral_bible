# [Numbers 6](https://www.blueletterbible.org/kjv/num/6)

<a name="numbers_6_1"></a>Numbers 6:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_6_2"></a>Numbers 6:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, When either ['iysh](../../strongs/h/h376.md) or ['ishshah](../../strongs/h/h802.md) shall [pala'](../../strongs/h/h6381.md) themselves to [nāḏar](../../strongs/h/h5087.md) a [neḏer](../../strongs/h/h5088.md) of a [nāzîr](../../strongs/h/h5139.md), to [nāzar](../../strongs/h/h5144.md) themselves unto [Yĕhovah](../../strongs/h/h3068.md):

<a name="numbers_6_3"></a>Numbers 6:3

He shall [nāzar](../../strongs/h/h5144.md) himself from [yayin](../../strongs/h/h3196.md) and [šēḵār](../../strongs/h/h7941.md), and shall [šāṯâ](../../strongs/h/h8354.md) no [ḥōmeṣ](../../strongs/h/h2558.md) of [yayin](../../strongs/h/h3196.md), or [ḥōmeṣ](../../strongs/h/h2558.md) of [šēḵār](../../strongs/h/h7941.md), neither shall he [šāṯâ](../../strongs/h/h8354.md) any [mišrâ](../../strongs/h/h4952.md) of [ʿēnāḇ](../../strongs/h/h6025.md), nor ['akal](../../strongs/h/h398.md) [laḥ](../../strongs/h/h3892.md) [ʿēnāḇ](../../strongs/h/h6025.md), or [yāḇēš](../../strongs/h/h3002.md).

<a name="numbers_6_4"></a>Numbers 6:4

All the [yowm](../../strongs/h/h3117.md) of his [nēzer](../../strongs/h/h5145.md) shall he ['akal](../../strongs/h/h398.md) nothing that is ['asah](../../strongs/h/h6213.md) of the [yayin](../../strongs/h/h3196.md) [gep̄en](../../strongs/h/h1612.md), from the [ḥarṣān](../../strongs/h/h2785.md) even to the [zāḡ](../../strongs/h/h2085.md).

<a name="numbers_6_5"></a>Numbers 6:5

All the [yowm](../../strongs/h/h3117.md) of the [neḏer](../../strongs/h/h5088.md) of his [nēzer](../../strongs/h/h5145.md) there shall no [taʿar](../../strongs/h/h8593.md) ['abar](../../strongs/h/h5674.md) upon his [ro'sh](../../strongs/h/h7218.md): until the [yowm](../../strongs/h/h3117.md) be [mālā'](../../strongs/h/h4390.md), in the which he [nāzar](../../strongs/h/h5144.md) himself unto [Yĕhovah](../../strongs/h/h3068.md), he shall be [qadowsh](../../strongs/h/h6918.md), and shall let the [peraʿ](../../strongs/h/h6545.md) of the [śēʿār](../../strongs/h/h8181.md) of his [ro'sh](../../strongs/h/h7218.md) [gāḏal](../../strongs/h/h1431.md).

<a name="numbers_6_6"></a>Numbers 6:6

All the [yowm](../../strongs/h/h3117.md) that he [nāzar](../../strongs/h/h5144.md) himself unto [Yĕhovah](../../strongs/h/h3068.md) he shall [bow'](../../strongs/h/h935.md) at no [muwth](../../strongs/h/h4191.md) [nephesh](../../strongs/h/h5315.md).

<a name="numbers_6_7"></a>Numbers 6:7

He shall not make himself [ṭāmē'](../../strongs/h/h2930.md) for his ['ab](../../strongs/h/h1.md), or for his ['em](../../strongs/h/h517.md), for his ['ach](../../strongs/h/h251.md), or for his ['āḥôṯ](../../strongs/h/h269.md), when they [maveth](../../strongs/h/h4194.md): because the [nēzer](../../strongs/h/h5145.md) of his ['Elohiym](../../strongs/h/h430.md) is upon his [ro'sh](../../strongs/h/h7218.md).

<a name="numbers_6_8"></a>Numbers 6:8

All the [yowm](../../strongs/h/h3117.md) of his [nēzer](../../strongs/h/h5145.md) he is [qadowsh](../../strongs/h/h6918.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_6_9"></a>Numbers 6:9

And if any [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md) [peṯaʿ](../../strongs/h/h6621.md) [piṯ'ōm](../../strongs/h/h6597.md) by him, and he hath [ṭāmē'](../../strongs/h/h2930.md) the [ro'sh](../../strongs/h/h7218.md) of his [nēzer](../../strongs/h/h5145.md); then he shall [gālaḥ](../../strongs/h/h1548.md) his [ro'sh](../../strongs/h/h7218.md) in the [yowm](../../strongs/h/h3117.md) of his [ṭāhŏrâ](../../strongs/h/h2893.md), on the seventh [yowm](../../strongs/h/h3117.md) shall he [gālaḥ](../../strongs/h/h1548.md) it.

<a name="numbers_6_10"></a>Numbers 6:10

And on the eighth [yowm](../../strongs/h/h3117.md) he shall [bow'](../../strongs/h/h935.md) two [tôr](../../strongs/h/h8449.md), or two [ben](../../strongs/h/h1121.md) [yônâ](../../strongs/h/h3123.md), to the [kōhēn](../../strongs/h/h3548.md), to the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md):

<a name="numbers_6_11"></a>Numbers 6:11

And the [kōhēn](../../strongs/h/h3548.md) shall ['asah](../../strongs/h/h6213.md) the one for a [chatta'ath](../../strongs/h/h2403.md), and the other for an [ʿōlâ](../../strongs/h/h5930.md), and make a [kāp̄ar](../../strongs/h/h3722.md) for him, for that he [chata'](../../strongs/h/h2398.md) by the [nephesh](../../strongs/h/h5315.md), and shall [qadash](../../strongs/h/h6942.md) his [ro'sh](../../strongs/h/h7218.md) that same [yowm](../../strongs/h/h3117.md).

<a name="numbers_6_12"></a>Numbers 6:12

And he shall [nāzar](../../strongs/h/h5144.md) unto [Yĕhovah](../../strongs/h/h3068.md) the [yowm](../../strongs/h/h3117.md) of his [nēzer](../../strongs/h/h5145.md), and shall [bow'](../../strongs/h/h935.md) a [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) for an ['āšām](../../strongs/h/h817.md): but the [yowm](../../strongs/h/h3117.md) that were [ri'šôn](../../strongs/h/h7223.md) shall be [naphal](../../strongs/h/h5307.md), because his [nēzer](../../strongs/h/h5145.md) was [ṭāmē'](../../strongs/h/h2930.md).

<a name="numbers_6_13"></a>Numbers 6:13

And this is the [towrah](../../strongs/h/h8451.md) of the [nāzîr](../../strongs/h/h5139.md), when the [yowm](../../strongs/h/h3117.md) of his [nēzer](../../strongs/h/h5145.md) are [mālā'](../../strongs/h/h4390.md): he shall be [bow'](../../strongs/h/h935.md) unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md):

<a name="numbers_6_14"></a>Numbers 6:14

And he shall [qāraḇ](../../strongs/h/h7126.md) his [qorban](../../strongs/h/h7133.md) unto [Yĕhovah](../../strongs/h/h3068.md), one [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md) for an [ʿōlâ](../../strongs/h/h5930.md), and one [kiḇśâ](../../strongs/h/h3535.md) of the [bath](../../strongs/h/h1323.md) [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md) for a [chatta'ath](../../strongs/h/h2403.md), and one ['ayil](../../strongs/h/h352.md) [tamiym](../../strongs/h/h8549.md) for [šelem](../../strongs/h/h8002.md),

<a name="numbers_6_15"></a>Numbers 6:15

And a [sal](../../strongs/h/h5536.md) of [maṣṣâ](../../strongs/h/h4682.md), [ḥallâ](../../strongs/h/h2471.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), and [rāqîq](../../strongs/h/h7550.md) of [maṣṣâ](../../strongs/h/h4682.md) [māšaḥ](../../strongs/h/h4886.md) with [šemen](../../strongs/h/h8081.md), and their [minchah](../../strongs/h/h4503.md), and their [necek](../../strongs/h/h5262.md).

<a name="numbers_6_16"></a>Numbers 6:16

And the [kōhēn](../../strongs/h/h3548.md) shall [qāraḇ](../../strongs/h/h7126.md) them [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and shall ['asah](../../strongs/h/h6213.md) his [chatta'ath](../../strongs/h/h2403.md), and his an [ʿōlâ](../../strongs/h/h5930.md):

<a name="numbers_6_17"></a>Numbers 6:17

And he shall ['asah](../../strongs/h/h6213.md) the ['ayil](../../strongs/h/h352.md) for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md) unto [Yĕhovah](../../strongs/h/h3068.md), with the [sal](../../strongs/h/h5536.md) of [maṣṣâ](../../strongs/h/h4682.md): the [kōhēn](../../strongs/h/h3548.md) shall ['asah](../../strongs/h/h6213.md) also his [minchah](../../strongs/h/h4503.md), and his [necek](../../strongs/h/h5262.md).

<a name="numbers_6_18"></a>Numbers 6:18

And the [nāzîr](../../strongs/h/h5139.md) shall [gālaḥ](../../strongs/h/h1548.md) the [ro'sh](../../strongs/h/h7218.md) of his [nēzer](../../strongs/h/h5145.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and shall [laqach](../../strongs/h/h3947.md) the [śēʿār](../../strongs/h/h8181.md) of the [ro'sh](../../strongs/h/h7218.md) of his [nēzer](../../strongs/h/h5145.md), and [nathan](../../strongs/h/h5414.md) it in the ['esh](../../strongs/h/h784.md) which is under the [zebach](../../strongs/h/h2077.md) of the [šelem](../../strongs/h/h8002.md).

<a name="numbers_6_19"></a>Numbers 6:19

And the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) the [bāšēl](../../strongs/h/h1311.md) [zerowa'](../../strongs/h/h2220.md) of the ['ayil](../../strongs/h/h352.md), and one [maṣṣâ](../../strongs/h/h4682.md) [ḥallâ](../../strongs/h/h2471.md) out of the [sal](../../strongs/h/h5536.md), and one [maṣṣâ](../../strongs/h/h4682.md) [rāqîq](../../strongs/h/h7550.md), and shall [nathan](../../strongs/h/h5414.md) them upon the [kaph](../../strongs/h/h3709.md) of the [nāzîr](../../strongs/h/h5139.md), ['aḥar](../../strongs/h/h310.md) his [nēzer](../../strongs/h/h5145.md) is [gālaḥ](../../strongs/h/h1548.md):

<a name="numbers_6_20"></a>Numbers 6:20

And the [kōhēn](../../strongs/h/h3548.md) shall [nûp̄](../../strongs/h/h5130.md) them for a [tᵊnûp̄â](../../strongs/h/h8573.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): this is [qodesh](../../strongs/h/h6944.md) for the [kōhēn](../../strongs/h/h3548.md), with the [tᵊnûp̄â](../../strongs/h/h8573.md) [ḥāzê](../../strongs/h/h2373.md) and [tᵊrûmâ](../../strongs/h/h8641.md) [šôq](../../strongs/h/h7785.md): and ['aḥar](../../strongs/h/h310.md) that the [nāzîr](../../strongs/h/h5139.md) may [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md).

<a name="numbers_6_21"></a>Numbers 6:21

This is the [towrah](../../strongs/h/h8451.md) of the [nāzîr](../../strongs/h/h5139.md) who hath [nāḏar](../../strongs/h/h5087.md), and of his [qorban](../../strongs/h/h7133.md) unto [Yĕhovah](../../strongs/h/h3068.md) for his [nēzer](../../strongs/h/h5145.md), beside that that his [yad](../../strongs/h/h3027.md) shall [nāśaḡ](../../strongs/h/h5381.md): [peh](../../strongs/h/h6310.md) to the [neḏer](../../strongs/h/h5088.md) which he [nāḏar](../../strongs/h/h5087.md), so he must ['asah](../../strongs/h/h6213.md) after the [towrah](../../strongs/h/h8451.md) of his [nēzer](../../strongs/h/h5145.md).

<a name="numbers_6_22"></a>Numbers 6:22

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_6_23"></a>Numbers 6:23

[dabar](../../strongs/h/h1696.md) unto ['Ahărôn](../../strongs/h/h175.md) and unto his [ben](../../strongs/h/h1121.md), ['āmar](../../strongs/h/h559.md), [kô](../../strongs/h/h3541.md) ye shall [barak](../../strongs/h/h1288.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md) unto them,

<a name="numbers_6_24"></a>Numbers 6:24

[Yĕhovah](../../strongs/h/h3068.md) [barak](../../strongs/h/h1288.md) thee, and [shamar](../../strongs/h/h8104.md) thee: [^1]

<a name="numbers_6_25"></a>Numbers 6:25

[Yĕhovah](../../strongs/h/h3068.md) [paniym](../../strongs/h/h6440.md) ['owr](../../strongs/h/h215.md) upon thee, and be [chanan](../../strongs/h/h2603.md) unto thee:

<a name="numbers_6_26"></a>Numbers 6:26

[Yĕhovah](../../strongs/h/h3068.md) [nasa'](../../strongs/h/h5375.md) his [paniym](../../strongs/h/h6440.md) upon thee, and [śûm](../../strongs/h/h7760.md) thee [shalowm](../../strongs/h/h7965.md).

<a name="numbers_6_27"></a>Numbers 6:27

And they shall [śûm](../../strongs/h/h7760.md) my [shem](../../strongs/h/h8034.md) upon the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and I will [barak](../../strongs/h/h1288.md) them.

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 5](numbers_5.md) - [Numbers 7](numbers_7.md)

---

[^1]: [Numbers 6:24](../../commentary/numbers/numbers_6_commentary.md#numbers_6:24)
