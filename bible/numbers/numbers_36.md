# [Numbers 36](https://www.blueletterbible.org/kjv/num/36)

<a name="numbers_36_1"></a>Numbers 36:1

And the [ro'sh](../../strongs/h/h7218.md) ['ab](../../strongs/h/h1.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Gilʿāḏ](../../strongs/h/h1568.md), the [ben](../../strongs/h/h1121.md) of [Māḵîr](../../strongs/h/h4353.md), the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md), of the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md), [qāraḇ](../../strongs/h/h7126.md), and [dabar](../../strongs/h/h1696.md) [paniym](../../strongs/h/h6440.md) [Mōshe](../../strongs/h/h4872.md), and [paniym](../../strongs/h/h6440.md) the [nāśî'](../../strongs/h/h5387.md), the [ro'sh](../../strongs/h/h7218.md) ['ab](../../strongs/h/h1.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="numbers_36_2"></a>Numbers 36:2

And they ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) my ['adown](../../strongs/h/h113.md) to [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) for a [nachalah](../../strongs/h/h5159.md) by [gôrāl](../../strongs/h/h1486.md) to the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): and my ['adown](../../strongs/h/h113.md) was [tsavah](../../strongs/h/h6680.md) by [Yĕhovah](../../strongs/h/h3068.md) to [nathan](../../strongs/h/h5414.md) the [nachalah](../../strongs/h/h5159.md) of [Ṣᵊlāp̄Ḥāḏ](../../strongs/h/h6765.md) our ['ach](../../strongs/h/h251.md) unto his [bath](../../strongs/h/h1323.md).

<a name="numbers_36_3"></a>Numbers 36:3

And if they be ['ishshah](../../strongs/h/h802.md) to any of the [ben](../../strongs/h/h1121.md) of the other [shebet](../../strongs/h/h7626.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), then shall their [nachalah](../../strongs/h/h5159.md) be [gāraʿ](../../strongs/h/h1639.md) from the [nachalah](../../strongs/h/h5159.md) of our ['ab](../../strongs/h/h1.md), and shall be [yāsap̄](../../strongs/h/h3254.md) to the [nachalah](../../strongs/h/h5159.md) of the [maṭṭê](../../strongs/h/h4294.md) whereunto they are received: so shall it be [gāraʿ](../../strongs/h/h1639.md) from the [gôrāl](../../strongs/h/h1486.md) of our [nachalah](../../strongs/h/h5159.md).

<a name="numbers_36_4"></a>Numbers 36:4

And when the [yôḇēl](../../strongs/h/h3104.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) shall be, then shall their [nachalah](../../strongs/h/h5159.md) be [yāsap̄](../../strongs/h/h3254.md) unto the [nachalah](../../strongs/h/h5159.md) of the [maṭṭê](../../strongs/h/h4294.md) whereunto they are received: so shall their [nachalah](../../strongs/h/h5159.md) be [gāraʿ](../../strongs/h/h1639.md) from the [nachalah](../../strongs/h/h5159.md) of the [maṭṭê](../../strongs/h/h4294.md) of our ['ab](../../strongs/h/h1.md).

<a name="numbers_36_5"></a>Numbers 36:5

And [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), The [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md) hath [dabar](../../strongs/h/h1696.md) well.

<a name="numbers_36_6"></a>Numbers 36:6

This is the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) doth [tsavah](../../strongs/h/h6680.md) concerning the [bath](../../strongs/h/h1323.md) of [Ṣᵊlāp̄Ḥāḏ](../../strongs/h/h6765.md), ['āmar](../../strongs/h/h559.md), Let them ['ishshah](../../strongs/h/h802.md) to whom they ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md); only to the [mišpāḥâ](../../strongs/h/h4940.md) of the [maṭṭê](../../strongs/h/h4294.md) of their ['ab](../../strongs/h/h1.md) shall they ['ishshah](../../strongs/h/h802.md).

<a name="numbers_36_7"></a>Numbers 36:7

So shall not the [nachalah](../../strongs/h/h5159.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [cabab](../../strongs/h/h5437.md) from [maṭṭê](../../strongs/h/h4294.md) to [maṭṭê](../../strongs/h/h4294.md): for every ['iysh](../../strongs/h/h376.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) shall [dāḇaq](../../strongs/h/h1692.md) himself to the [nachalah](../../strongs/h/h5159.md) of the [maṭṭê](../../strongs/h/h4294.md) of his ['ab](../../strongs/h/h1.md).

<a name="numbers_36_8"></a>Numbers 36:8

And every [bath](../../strongs/h/h1323.md), that [yarash](../../strongs/h/h3423.md) a [nachalah](../../strongs/h/h5159.md) in any [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), shall be ['ishshah](../../strongs/h/h802.md) unto one of the [mišpāḥâ](../../strongs/h/h4940.md) of the [maṭṭê](../../strongs/h/h4294.md) of her ['ab](../../strongs/h/h1.md), that the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) may [yarash](../../strongs/h/h3423.md) every ['iysh](../../strongs/h/h376.md) the [nachalah](../../strongs/h/h5159.md) of his ['ab](../../strongs/h/h1.md).

<a name="numbers_36_9"></a>Numbers 36:9

Neither shall the [nachalah](../../strongs/h/h5159.md) [cabab](../../strongs/h/h5437.md) from one [maṭṭê](../../strongs/h/h4294.md) to ['aḥēr](../../strongs/h/h312.md) [maṭṭê](../../strongs/h/h4294.md); but every ['iysh](../../strongs/h/h376.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) shall [dāḇaq](../../strongs/h/h1692.md) himself to his own [nachalah](../../strongs/h/h5159.md).

<a name="numbers_36_10"></a>Numbers 36:10

Even as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md), so ['asah](../../strongs/h/h6213.md) the [bath](../../strongs/h/h1323.md) of [Ṣᵊlāp̄Ḥāḏ](../../strongs/h/h6765.md):

<a name="numbers_36_11"></a>Numbers 36:11

For [Maḥlâ](../../strongs/h/h4244.md), [Tirṣâ](../../strongs/h/h8656.md), and [Ḥāḡlâ](../../strongs/h/h2295.md), and [Milkâ](../../strongs/h/h4435.md), and [NōʿÂ](../../strongs/h/h5270.md), the [bath](../../strongs/h/h1323.md) of [Ṣᵊlāp̄Ḥāḏ](../../strongs/h/h6765.md), were ['ishshah](../../strongs/h/h802.md) unto their [dôḏ](../../strongs/h/h1730.md) [ben](../../strongs/h/h1121.md):

<a name="numbers_36_12"></a>Numbers 36:12

And they were ['ishshah](../../strongs/h/h802.md) into the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md) the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md), and their [nachalah](../../strongs/h/h5159.md) remained in the [maṭṭê](../../strongs/h/h4294.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of their ['ab](../../strongs/h/h1.md).

<a name="numbers_36_13"></a>Numbers 36:13

These are the [mitsvah](../../strongs/h/h4687.md) and the [mishpat](../../strongs/h/h4941.md), which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) in the ['arabah](../../strongs/h/h6160.md) of [Mô'āḇ](../../strongs/h/h4124.md) by [Yardēn](../../strongs/h/h3383.md) near [Yᵊrēḥô](../../strongs/h/h3405.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 35](numbers_35.md)