# [Numbers 3](https://www.blueletterbible.org/kjv/num/3)

<a name="numbers_3_1"></a>Numbers 3:1

These also are the [towlĕdah](../../strongs/h/h8435.md) of ['Ahărôn](../../strongs/h/h175.md) and [Mōshe](../../strongs/h/h4872.md) in the [yowm](../../strongs/h/h3117.md) that [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) with [Mōshe](../../strongs/h/h4872.md) in [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md).

<a name="numbers_3_2"></a>Numbers 3:2

And these are the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md); [Nāḏāḇ](../../strongs/h/h5070.md) the [bᵊḵôr](../../strongs/h/h1060.md), and ['Ăḇîhû'](../../strongs/h/h30.md), ['Elʿāzār](../../strongs/h/h499.md), and ['Îṯāmār](../../strongs/h/h385.md).

<a name="numbers_3_3"></a>Numbers 3:3

These are the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), the [kōhēn](../../strongs/h/h3548.md) which were [māšaḥ](../../strongs/h/h4886.md), whom he [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) to [kāhan](../../strongs/h/h3547.md).

<a name="numbers_3_4"></a>Numbers 3:4

And [Nāḏāḇ](../../strongs/h/h5070.md) and ['Ăḇîhû'](../../strongs/h/h30.md) [muwth](../../strongs/h/h4191.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), when they [qāraḇ](../../strongs/h/h7126.md) [zûr](../../strongs/h/h2114.md) ['esh](../../strongs/h/h784.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), in the [midbar](../../strongs/h/h4057.md) of [Sînay](../../strongs/h/h5514.md), and they had no [ben](../../strongs/h/h1121.md): and ['Elʿāzār](../../strongs/h/h499.md) and ['Îṯāmār](../../strongs/h/h385.md) [kāhan](../../strongs/h/h3547.md) in the [paniym](../../strongs/h/h6440.md) of ['Ahărôn](../../strongs/h/h175.md) their ['ab](../../strongs/h/h1.md).

<a name="numbers_3_5"></a>Numbers 3:5

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_3_6"></a>Numbers 3:6

[qāraḇ](../../strongs/h/h7126.md) the [maṭṭê](../../strongs/h/h4294.md) of [Lēvî](../../strongs/h/h3878.md), and ['amad](../../strongs/h/h5975.md) them [paniym](../../strongs/h/h6440.md) ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md), that they may [sharath](../../strongs/h/h8334.md) unto him.

<a name="numbers_3_7"></a>Numbers 3:7

And they shall [shamar](../../strongs/h/h8104.md) his [mišmereṯ](../../strongs/h/h4931.md), and the [mišmereṯ](../../strongs/h/h4931.md) of the ['edah](../../strongs/h/h5712.md) [paniym](../../strongs/h/h6440.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), to ['abad](../../strongs/h/h5647.md) the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [miškān](../../strongs/h/h4908.md).

<a name="numbers_3_8"></a>Numbers 3:8

And they shall [shamar](../../strongs/h/h8104.md) all the [kĕliy](../../strongs/h/h3627.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and the [mišmereṯ](../../strongs/h/h4931.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), to ['abad](../../strongs/h/h5647.md) the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [miškān](../../strongs/h/h4908.md).

<a name="numbers_3_9"></a>Numbers 3:9

And thou shalt [nathan](../../strongs/h/h5414.md) the [Lᵊvî](../../strongs/h/h3881.md) unto ['Ahărôn](../../strongs/h/h175.md) and to his [ben](../../strongs/h/h1121.md): they are [nathan](../../strongs/h/h5414.md) unto him out of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_3_10"></a>Numbers 3:10

And thou shalt [paqad](../../strongs/h/h6485.md) ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md), and they shall [shamar](../../strongs/h/h8104.md) their [kᵊhunnâ](../../strongs/h/h3550.md): and the [zûr](../../strongs/h/h2114.md) that [qārēḇ](../../strongs/h/h7131.md) shall be [muwth](../../strongs/h/h4191.md).

<a name="numbers_3_11"></a>Numbers 3:11

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_3_12"></a>Numbers 3:12

And I, behold, I have [laqach](../../strongs/h/h3947.md) the [Lᵊvî](../../strongs/h/h3881.md) from [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) instead of all the [bᵊḵôr](../../strongs/h/h1060.md) that [peṭer](../../strongs/h/h6363.md) the [reḥem](../../strongs/h/h7358.md) among the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): therefore the [Lᵊvî](../../strongs/h/h3881.md) shall be mine;

<a name="numbers_3_13"></a>Numbers 3:13

Because all the [bᵊḵôr](../../strongs/h/h1060.md) are mine; for on the [yowm](../../strongs/h/h3117.md) that I [nakah](../../strongs/h/h5221.md) all the [bᵊḵôr](../../strongs/h/h1060.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) I [qadash](../../strongs/h/h6942.md) unto me all the [bᵊḵôr](../../strongs/h/h1060.md) in [Yisra'el](../../strongs/h/h3478.md), both ['adam](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md): mine shall they be: I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_3_14"></a>Numbers 3:14

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) in the [midbar](../../strongs/h/h4057.md) of [Sînay](../../strongs/h/h5514.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_3_15"></a>Numbers 3:15

[paqad](../../strongs/h/h6485.md) the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md) after the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), by their [mišpāḥâ](../../strongs/h/h4940.md): every [zāḵār](../../strongs/h/h2145.md) from a [ḥōḏeš](../../strongs/h/h2320.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md) shalt thou [paqad](../../strongs/h/h6485.md) them.

<a name="numbers_3_16"></a>Numbers 3:16

And [Mōshe](../../strongs/h/h4872.md) [paqad](../../strongs/h/h6485.md) them according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md), as he was [tsavah](../../strongs/h/h6680.md).

<a name="numbers_3_17"></a>Numbers 3:17

And these were the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md) by their [shem](../../strongs/h/h8034.md); [Gēršôn](../../strongs/h/h1648.md), and [Qᵊhāṯ](../../strongs/h/h6955.md), and [Mᵊrārî](../../strongs/h/h4847.md).

<a name="numbers_3_18"></a>Numbers 3:18

And these are the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of [Gēršôn](../../strongs/h/h1648.md) by their [mišpāḥâ](../../strongs/h/h4940.md); [Liḇnî](../../strongs/h/h3845.md), and [Šimʿî](../../strongs/h/h8096.md).

<a name="numbers_3_19"></a>Numbers 3:19

And the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md) by their [mišpāḥâ](../../strongs/h/h4940.md); [ʿAmrām](../../strongs/h/h6019.md), and [Yiṣhār](../../strongs/h/h3324.md), [Ḥeḇrôn](../../strongs/h/h2275.md), and [ʿUzzî'ēl](../../strongs/h/h5816.md).

<a name="numbers_3_20"></a>Numbers 3:20

And the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md) by their [mišpāḥâ](../../strongs/h/h4940.md); [Maḥlî](../../strongs/h/h4249.md), and [Mûšî](../../strongs/h/h4187.md). These are the [mišpāḥâ](../../strongs/h/h4940.md) of the [Lᵊvî](../../strongs/h/h3881.md) according to the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md).

<a name="numbers_3_21"></a>Numbers 3:21

Of [Gēršôn](../../strongs/h/h1648.md) was the [mišpāḥâ](../../strongs/h/h4940.md) of the [Liḇnî](../../strongs/h/h3846.md), and the [mišpāḥâ](../../strongs/h/h4940.md) of the [Šimʿî](../../strongs/h/h8097.md): these are the [mišpāḥâ](../../strongs/h/h4940.md) of the [Gēršunnî](../../strongs/h/h1649.md).

<a name="numbers_3_22"></a>Numbers 3:22

Those that were [paqad](../../strongs/h/h6485.md) of them, according to the [mispār](../../strongs/h/h4557.md) of all the [zāḵār](../../strongs/h/h2145.md), from a [ḥōḏeš](../../strongs/h/h2320.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), even those that were [paqad](../../strongs/h/h6485.md) of them were seven thousand and five hundred.

<a name="numbers_3_23"></a>Numbers 3:23

The [mišpāḥâ](../../strongs/h/h4940.md) of the [Gēršunnî](../../strongs/h/h1649.md) shall [ḥānâ](../../strongs/h/h2583.md) ['aḥar](../../strongs/h/h310.md) the [miškān](../../strongs/h/h4908.md) [yam](../../strongs/h/h3220.md).

<a name="numbers_3_24"></a>Numbers 3:24

And the [nāśî'](../../strongs/h/h5387.md) of the [bayith](../../strongs/h/h1004.md) of the ['ab](../../strongs/h/h1.md) of the [Gēršunnî](../../strongs/h/h1649.md) shall be ['elyāsāp̄](../../strongs/h/h460.md) the [ben](../../strongs/h/h1121.md) of [lā'ēl](../../strongs/h/h3815.md).

<a name="numbers_3_25"></a>Numbers 3:25

And the [mišmereṯ](../../strongs/h/h4931.md) of the [ben](../../strongs/h/h1121.md) of [Gēršôn](../../strongs/h/h1648.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) shall be the [miškān](../../strongs/h/h4908.md), and the ['ohel](../../strongs/h/h168.md), the [miḵsê](../../strongs/h/h4372.md) thereof, and the [māsāḵ](../../strongs/h/h4539.md) for the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md),

<a name="numbers_3_26"></a>Numbers 3:26

And the [qelaʿ](../../strongs/h/h7050.md) of the [ḥāṣēr](../../strongs/h/h2691.md), and the [māsāḵ](../../strongs/h/h4539.md) for the [peṯaḥ](../../strongs/h/h6607.md) of the [ḥāṣēr](../../strongs/h/h2691.md), which is by the [miškān](../../strongs/h/h4908.md), and by the [mizbeach](../../strongs/h/h4196.md) [cabiyb](../../strongs/h/h5439.md), and the [mêṯār](../../strongs/h/h4340.md) of it for all the [ʿăḇōḏâ](../../strongs/h/h5656.md) thereof.

<a name="numbers_3_27"></a>Numbers 3:27

And of [Qᵊhāṯ](../../strongs/h/h6955.md) was the [mišpāḥâ](../../strongs/h/h4940.md) of the [ʿAmrāmî](../../strongs/h/h6020.md), and the [mišpāḥâ](../../strongs/h/h4940.md) of the [Yiṣhārî](../../strongs/h/h3325.md), and the [mišpāḥâ](../../strongs/h/h4940.md) of the [Ḥeḇrônî](../../strongs/h/h2276.md), and the [mišpāḥâ](../../strongs/h/h4940.md) of the [ʿĀzzî'ēlî](../../strongs/h/h5817.md): these are the [mišpāḥâ](../../strongs/h/h4940.md) of the [Qᵊhāṯî](../../strongs/h/h6956.md).

<a name="numbers_3_28"></a>Numbers 3:28

In the [mispār](../../strongs/h/h4557.md) of all the [zāḵār](../../strongs/h/h2145.md), from a [ḥōḏeš](../../strongs/h/h2320.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), were eight thousand and six hundred, [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of the [qodesh](../../strongs/h/h6944.md).

<a name="numbers_3_29"></a>Numbers 3:29

The [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md) shall [ḥānâ](../../strongs/h/h2583.md) on the [yārēḵ](../../strongs/h/h3409.md) of the [miškān](../../strongs/h/h4908.md) [têmān](../../strongs/h/h8486.md).

<a name="numbers_3_30"></a>Numbers 3:30

And the [nāśî'](../../strongs/h/h5387.md) of the [bayith](../../strongs/h/h1004.md) of the ['ab](../../strongs/h/h1.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of the [Qᵊhāṯî](../../strongs/h/h6956.md) shall be ['Ĕlîṣāp̄ān](../../strongs/h/h469.md) the [ben](../../strongs/h/h1121.md) of [ʿUzzî'ēl](../../strongs/h/h5816.md).

<a name="numbers_3_31"></a>Numbers 3:31

And their [mišmereṯ](../../strongs/h/h4931.md) shall be the ['ārôn](../../strongs/h/h727.md), and the [šulḥān](../../strongs/h/h7979.md), and the [mᵊnôrâ](../../strongs/h/h4501.md), and the [mizbeach](../../strongs/h/h4196.md), and the [kĕliy](../../strongs/h/h3627.md) of the [qodesh](../../strongs/h/h6944.md) wherewith they [sharath](../../strongs/h/h8334.md), and the [māsāḵ](../../strongs/h/h4539.md), and all the [ʿăḇōḏâ](../../strongs/h/h5656.md) thereof.

<a name="numbers_3_32"></a>Numbers 3:32

And ['Elʿāzār](../../strongs/h/h499.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md) shall be [nāśî'](../../strongs/h/h5387.md) over the [nāśî'](../../strongs/h/h5387.md) of the [Lᵊvî](../../strongs/h/h3881.md), and have the [pᵊqudâ](../../strongs/h/h6486.md) of them that [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of the [qodesh](../../strongs/h/h6944.md).

<a name="numbers_3_33"></a>Numbers 3:33

Of [Mᵊrārî](../../strongs/h/h4847.md) was the [mišpāḥâ](../../strongs/h/h4940.md) of the [Maḥlî](../../strongs/h/h4250.md), and the [mišpāḥâ](../../strongs/h/h4940.md) of the [Mûšî](../../strongs/h/h4188.md): these are the [mišpāḥâ](../../strongs/h/h4940.md) of [Mᵊrārî](../../strongs/h/h4847.md).

<a name="numbers_3_34"></a>Numbers 3:34

And those that were [paqad](../../strongs/h/h6485.md) of them, according to the [mispār](../../strongs/h/h4557.md) of all the [zāḵār](../../strongs/h/h2145.md), from a [ḥōḏeš](../../strongs/h/h2320.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), were six thousand and two hundred.

<a name="numbers_3_35"></a>Numbers 3:35

And the [nāśî'](../../strongs/h/h5387.md) of the [bayith](../../strongs/h/h1004.md) of the ['ab](../../strongs/h/h1.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of [Mᵊrārî](../../strongs/h/h4847.md) was [ṣûrî'ēl](../../strongs/h/h6700.md) the [ben](../../strongs/h/h1121.md) of ['Ăḇîhayil](../../strongs/h/h32.md): these shall [ḥānâ](../../strongs/h/h2583.md) on the [yārēḵ](../../strongs/h/h3409.md) of the [miškān](../../strongs/h/h4908.md) [ṣāp̄ôn](../../strongs/h/h6828.md).

<a name="numbers_3_36"></a>Numbers 3:36

And under the [pᵊqudâ](../../strongs/h/h6486.md) and [mišmereṯ](../../strongs/h/h4931.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md) shall be the [qereš](../../strongs/h/h7175.md) of the [miškān](../../strongs/h/h4908.md), and the [bᵊrîaḥ](../../strongs/h/h1280.md) thereof, and the [ʿammûḏ](../../strongs/h/h5982.md) thereof, and the ['eḏen](../../strongs/h/h134.md) thereof, and all the [kĕliy](../../strongs/h/h3627.md) thereof, and all that [ʿăḇōḏâ](../../strongs/h/h5656.md) thereto,

<a name="numbers_3_37"></a>Numbers 3:37

And the [ʿammûḏ](../../strongs/h/h5982.md) of the [ḥāṣēr](../../strongs/h/h2691.md) [cabiyb](../../strongs/h/h5439.md), and their ['eḏen](../../strongs/h/h134.md), and their [yāṯēḏ](../../strongs/h/h3489.md), and their [mêṯār](../../strongs/h/h4340.md).

<a name="numbers_3_38"></a>Numbers 3:38

But those that [ḥānâ](../../strongs/h/h2583.md) [paniym](../../strongs/h/h6440.md) the [miškān](../../strongs/h/h4908.md) toward the [qeḏem](../../strongs/h/h6924.md), even [paniym](../../strongs/h/h6440.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) [mizrach](../../strongs/h/h4217.md), shall be [Mōshe](../../strongs/h/h4872.md), and ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md), [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of the [miqdash](../../strongs/h/h4720.md) for the [mišmereṯ](../../strongs/h/h4931.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); and the [zûr](../../strongs/h/h2114.md) that [qārēḇ](../../strongs/h/h7131.md) shall be [muwth](../../strongs/h/h4191.md).

<a name="numbers_3_39"></a>Numbers 3:39

All that were [paqad](../../strongs/h/h6485.md) of the [Lᵊvî](../../strongs/h/h3881.md), which [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [paqad](../../strongs/h/h6485.md) at the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md), throughout their [mišpāḥâ](../../strongs/h/h4940.md), all the [zāḵār](../../strongs/h/h2145.md) from a [ḥōḏeš](../../strongs/h/h2320.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), were twenty and two thousand.

<a name="numbers_3_40"></a>Numbers 3:40

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [paqad](../../strongs/h/h6485.md) all the [bᵊḵôr](../../strongs/h/h1060.md) of the [zāḵār](../../strongs/h/h2145.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) from a [ḥōḏeš](../../strongs/h/h2320.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), and [nasa'](../../strongs/h/h5375.md) the [mispār](../../strongs/h/h4557.md) of their [shem](../../strongs/h/h8034.md).

<a name="numbers_3_41"></a>Numbers 3:41

And thou shalt [laqach](../../strongs/h/h3947.md) the [Lᵊvî](../../strongs/h/h3881.md) for me (I am [Yĕhovah](../../strongs/h/h3068.md)) instead of all the [bᵊḵôr](../../strongs/h/h1060.md) among the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); and the [bĕhemah](../../strongs/h/h929.md) of the [Lᵊvî](../../strongs/h/h3881.md) instead of all the [bᵊḵôr](../../strongs/h/h1060.md) among the [bĕhemah](../../strongs/h/h929.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_3_42"></a>Numbers 3:42

And [Mōshe](../../strongs/h/h4872.md) [paqad](../../strongs/h/h6485.md), as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) him, all the [bᵊḵôr](../../strongs/h/h1060.md) among the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_3_43"></a>Numbers 3:43

And all the [bᵊḵôr](../../strongs/h/h1060.md) [zāḵār](../../strongs/h/h2145.md) by the [mispār](../../strongs/h/h4557.md) of [shem](../../strongs/h/h8034.md), from a [ḥōḏeš](../../strongs/h/h2320.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), of those that were [paqad](../../strongs/h/h6485.md) of them, were twenty and two thousand two hundred and threescore and thirteen.

<a name="numbers_3_44"></a>Numbers 3:44

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_3_45"></a>Numbers 3:45

[laqach](../../strongs/h/h3947.md) the [Lᵊvî](../../strongs/h/h3881.md) instead of all the [bᵊḵôr](../../strongs/h/h1060.md) among the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and the [bĕhemah](../../strongs/h/h929.md) of the [Lᵊvî](../../strongs/h/h3881.md) instead of their [bĕhemah](../../strongs/h/h929.md); and the [Lᵊvî](../../strongs/h/h3881.md) shall be mine: I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_3_46"></a>Numbers 3:46

And for those that are to be [pᵊḏûy](../../strongs/h/h6302.md) of the two hundred and threescore and thirteen of the [bᵊḵôr](../../strongs/h/h1060.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which are [ʿāḏap̄](../../strongs/h/h5736.md) than the [Lᵊvî](../../strongs/h/h3881.md);

<a name="numbers_3_47"></a>Numbers 3:47

Thou shalt even [laqach](../../strongs/h/h3947.md) five [šeqel](../../strongs/h/h8255.md) apiece by the [gulgōleṯ](../../strongs/h/h1538.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md) shalt thou [laqach](../../strongs/h/h3947.md) them: (the [šeqel](../../strongs/h/h8255.md) is twenty [gērâ](../../strongs/h/h1626.md):)

<a name="numbers_3_48"></a>Numbers 3:48

And thou shalt [nathan](../../strongs/h/h5414.md) the [keceph](../../strongs/h/h3701.md), wherewith the [ʿāḏap̄](../../strongs/h/h5736.md) of them is to be [pᵊḏûy](../../strongs/h/h6302.md), unto ['Ahărôn](../../strongs/h/h175.md) and to his [ben](../../strongs/h/h1121.md).

<a name="numbers_3_49"></a>Numbers 3:49

And [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) the [piḏyôm](../../strongs/h/h6306.md) [keceph](../../strongs/h/h3701.md) of them that were [ʿāḏap̄](../../strongs/h/h5736.md) them that were [pᵊḏûy](../../strongs/h/h6302.md) by the [Lᵊvî](../../strongs/h/h3881.md):

<a name="numbers_3_50"></a>Numbers 3:50

Of the [bᵊḵôr](../../strongs/h/h1060.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [laqach](../../strongs/h/h3947.md) he the [keceph](../../strongs/h/h3701.md); a thousand three hundred and threescore and five, after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md):

<a name="numbers_3_51"></a>Numbers 3:51

And [Mōshe](../../strongs/h/h4872.md) [nathan](../../strongs/h/h5414.md) the [keceph](../../strongs/h/h3701.md) of them that were [pᵊḏûy](../../strongs/h/h6302.md) [piḏyôm](../../strongs/h/h6306.md) unto ['Ahărôn](../../strongs/h/h175.md) and to his [ben](../../strongs/h/h1121.md), according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md), as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 2](numbers_2.md) - [Numbers 4](numbers_4.md)