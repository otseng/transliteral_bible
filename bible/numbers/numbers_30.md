# [Numbers 30](https://www.blueletterbible.org/kjv/num/30)

<a name="numbers_30_1"></a>Numbers 30:1

And [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) unto the [ro'sh](../../strongs/h/h7218.md) of the [maṭṭê](../../strongs/h/h4294.md) concerning the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), This is the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [tsavah](../../strongs/h/h6680.md).

<a name="numbers_30_2"></a>Numbers 30:2

If an ['iysh](../../strongs/h/h376.md) [nāḏar](../../strongs/h/h5087.md) a [neḏer](../../strongs/h/h5088.md) unto [Yĕhovah](../../strongs/h/h3068.md), or [shaba'](../../strongs/h/h7650.md) a [šᵊḇûʿâ](../../strongs/h/h7621.md) to ['āsar](../../strongs/h/h631.md) his [nephesh](../../strongs/h/h5315.md) with an ['ĕsār](../../strongs/h/h632.md); he shall not [ḥālal](../../strongs/h/h2490.md) his [dabar](../../strongs/h/h1697.md), he shall ['asah](../../strongs/h/h6213.md) according to all that [yāṣā'](../../strongs/h/h3318.md) out of his [peh](../../strongs/h/h6310.md).

<a name="numbers_30_3"></a>Numbers 30:3

If an ['ishshah](../../strongs/h/h802.md) also [nāḏar](../../strongs/h/h5087.md) a [neḏer](../../strongs/h/h5088.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['āsar](../../strongs/h/h631.md) herself by an ['ĕsār](../../strongs/h/h632.md), being in her ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) in her [nāʿur](../../strongs/h/h5271.md);

<a name="numbers_30_4"></a>Numbers 30:4

And her ['ab](../../strongs/h/h1.md) [shama'](../../strongs/h/h8085.md) her [neḏer](../../strongs/h/h5088.md), and her ['ĕsār](../../strongs/h/h632.md) wherewith she hath ['āsar](../../strongs/h/h631.md) her [nephesh](../../strongs/h/h5315.md), and her ['ab](../../strongs/h/h1.md) shall [ḥāraš](../../strongs/h/h2790.md) at her; then all her [neḏer](../../strongs/h/h5088.md) shall [quwm](../../strongs/h/h6965.md), and every ['ĕsār](../../strongs/h/h632.md) wherewith she hath ['āsar](../../strongs/h/h631.md) her [nephesh](../../strongs/h/h5315.md) shall [quwm](../../strongs/h/h6965.md).

<a name="numbers_30_5"></a>Numbers 30:5

But if her ['ab](../../strongs/h/h1.md) [nô'](../../strongs/h/h5106.md) her in the [yowm](../../strongs/h/h3117.md) that he [shama'](../../strongs/h/h8085.md); not any of her [neḏer](../../strongs/h/h5088.md), or of her ['ĕsār](../../strongs/h/h632.md) wherewith she hath ['āsar](../../strongs/h/h631.md) her [nephesh](../../strongs/h/h5315.md), shall [quwm](../../strongs/h/h6965.md): and [Yĕhovah](../../strongs/h/h3068.md) shall [sālaḥ](../../strongs/h/h5545.md) her, because her ['ab](../../strongs/h/h1.md) [nô'](../../strongs/h/h5106.md) her.

<a name="numbers_30_6"></a>Numbers 30:6

And if she had at all an ['iysh](../../strongs/h/h376.md), when she [neḏer](../../strongs/h/h5088.md), or [miḇṭā'](../../strongs/h/h4008.md) ought out of her [saphah](../../strongs/h/h8193.md), wherewith she ['āsar](../../strongs/h/h631.md) her [nephesh](../../strongs/h/h5315.md);

<a name="numbers_30_7"></a>Numbers 30:7

And her ['iysh](../../strongs/h/h376.md) [shama'](../../strongs/h/h8085.md) it, and [ḥāraš](../../strongs/h/h2790.md) at her in the [yowm](../../strongs/h/h3117.md) that he [shama'](../../strongs/h/h8085.md) it: then her [neḏer](../../strongs/h/h5088.md) shall [quwm](../../strongs/h/h6965.md), and her ['ĕsār](../../strongs/h/h632.md) wherewith she ['āsar](../../strongs/h/h631.md) her [nephesh](../../strongs/h/h5315.md) shall [quwm](../../strongs/h/h6965.md).

<a name="numbers_30_8"></a>Numbers 30:8

But if her ['iysh](../../strongs/h/h376.md) [nô'](../../strongs/h/h5106.md) her on the [yowm](../../strongs/h/h3117.md) that he [shama'](../../strongs/h/h8085.md) it; then he shall [neḏer](../../strongs/h/h5088.md) her, and that which she [miḇṭā'](../../strongs/h/h4008.md) with her [saphah](../../strongs/h/h8193.md), wherewith she ['āsar](../../strongs/h/h631.md) her [nephesh](../../strongs/h/h5315.md), of [pārar](../../strongs/h/h6565.md): and [Yĕhovah](../../strongs/h/h3068.md) shall [sālaḥ](../../strongs/h/h5545.md) her.

<a name="numbers_30_9"></a>Numbers 30:9

But every [neḏer](../../strongs/h/h5088.md) of an ['almānâ](../../strongs/h/h490.md), and of her that is [gāraš](../../strongs/h/h1644.md), wherewith they have ['āsar](../../strongs/h/h631.md) their [nephesh](../../strongs/h/h5315.md), shall [quwm](../../strongs/h/h6965.md) against her.

<a name="numbers_30_10"></a>Numbers 30:10

And if she [nāḏar](../../strongs/h/h5087.md) in her ['iysh](../../strongs/h/h376.md) [bayith](../../strongs/h/h1004.md), or ['āsar](../../strongs/h/h631.md) her [nephesh](../../strongs/h/h5315.md) by an ['ĕsār](../../strongs/h/h632.md) with a [šᵊḇûʿâ](../../strongs/h/h7621.md);

<a name="numbers_30_11"></a>Numbers 30:11

And her ['iysh](../../strongs/h/h376.md) [shama'](../../strongs/h/h8085.md) it, and [ḥāraš](../../strongs/h/h2790.md) at her, and [nô'](../../strongs/h/h5106.md) her not: then all her [neḏer](../../strongs/h/h5088.md) shall [quwm](../../strongs/h/h6965.md), and every ['ĕsār](../../strongs/h/h632.md) wherewith she ['āsar](../../strongs/h/h631.md) her [nephesh](../../strongs/h/h5315.md) shall [quwm](../../strongs/h/h6965.md).

<a name="numbers_30_12"></a>Numbers 30:12

But if her ['iysh](../../strongs/h/h376.md) hath [pārar](../../strongs/h/h6565.md) [pārar](../../strongs/h/h6565.md) them on the [yowm](../../strongs/h/h3117.md) he [shama'](../../strongs/h/h8085.md) them; then whatsoever [môṣā'](../../strongs/h/h4161.md) of her [saphah](../../strongs/h/h8193.md) concerning her [neḏer](../../strongs/h/h5088.md), or concerning the ['ĕsār](../../strongs/h/h632.md) of her [nephesh](../../strongs/h/h5315.md), shall not [quwm](../../strongs/h/h6965.md): her ['iysh](../../strongs/h/h376.md) hath [pārar](../../strongs/h/h6565.md) them; and [Yĕhovah](../../strongs/h/h3068.md) shall [sālaḥ](../../strongs/h/h5545.md) her.

<a name="numbers_30_13"></a>Numbers 30:13

Every [neḏer](../../strongs/h/h5088.md), and every ['ĕsār](../../strongs/h/h632.md) [šᵊḇûʿâ](../../strongs/h/h7621.md) to [ʿānâ](../../strongs/h/h6031.md) the [nephesh](../../strongs/h/h5315.md), her ['iysh](../../strongs/h/h376.md) may [quwm](../../strongs/h/h6965.md) it, or her ['iysh](../../strongs/h/h376.md) may [pārar](../../strongs/h/h6565.md) it.

<a name="numbers_30_14"></a>Numbers 30:14

But if her ['iysh](../../strongs/h/h376.md) [ḥāraš](../../strongs/h/h2790.md) [ḥāraš](../../strongs/h/h2790.md) at her from [yowm](../../strongs/h/h3117.md) to [yowm](../../strongs/h/h3117.md); then he [quwm](../../strongs/h/h6965.md) all her [neḏer](../../strongs/h/h5088.md), or all her ['ĕsār](../../strongs/h/h632.md), which are upon her: he [quwm](../../strongs/h/h6965.md) them, because he [ḥāraš](../../strongs/h/h2790.md) her in the [yowm](../../strongs/h/h3117.md) that he [shama'](../../strongs/h/h8085.md) them.

<a name="numbers_30_15"></a>Numbers 30:15

But if he shall [pārar](../../strongs/h/h6565.md) [pārar](../../strongs/h/h6565.md) them ['aḥar](../../strongs/h/h310.md) that he hath [shama'](../../strongs/h/h8085.md) them; then he shall [nasa'](../../strongs/h/h5375.md) her ['avon](../../strongs/h/h5771.md).

<a name="numbers_30_16"></a>Numbers 30:16

These are the [choq](../../strongs/h/h2706.md), which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md), between an ['iysh](../../strongs/h/h376.md) and his ['ishshah](../../strongs/h/h802.md), between the ['ab](../../strongs/h/h1.md) and his [bath](../../strongs/h/h1323.md), being yet in her [nāʿur](../../strongs/h/h5271.md) in her ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 29](numbers_29.md) - [Numbers 31](numbers_31.md)