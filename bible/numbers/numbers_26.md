# [Numbers 26](https://www.blueletterbible.org/kjv/num/26)

<a name="numbers_26_1"></a>Numbers 26:1

And it came to pass ['aḥar](../../strongs/h/h310.md) the [magēp̄â](../../strongs/h/h4046.md), that [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md) and unto ['Elʿāzār](../../strongs/h/h499.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_26_2"></a>Numbers 26:2

[nasa'](../../strongs/h/h5375.md) the [ro'sh](../../strongs/h/h7218.md) of all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), throughout their ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), all that are able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_26_3"></a>Numbers 26:3

And [Mōshe](../../strongs/h/h4872.md) and ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md) [dabar](../../strongs/h/h1696.md) with them in the ['arabah](../../strongs/h/h6160.md) of [Mô'āḇ](../../strongs/h/h4124.md) by [Yardēn](../../strongs/h/h3383.md) near [Yᵊrēḥô](../../strongs/h/h3405.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_26_4"></a>Numbers 26:4

From twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md) and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="numbers_26_5"></a>Numbers 26:5

[Rᵊ'ûḇēn](../../strongs/h/h7205.md), the [bᵊḵôr](../../strongs/h/h1060.md) of [Yisra'el](../../strongs/h/h3478.md): the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md); [Ḥănôḵ](../../strongs/h/h2585.md), of whom cometh the [mišpāḥâ](../../strongs/h/h4940.md) of the [ḥănōḵî](../../strongs/h/h2599.md): of [Pallû'](../../strongs/h/h6396.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [pallu'î](../../strongs/h/h6384.md):

<a name="numbers_26_6"></a>Numbers 26:6

Of [Ḥeṣrôn](../../strongs/h/h2696.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Ḥeṣrônî](../../strongs/h/h2697.md): of [Karmî](../../strongs/h/h3756.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Karmî](../../strongs/h/h3757.md).

<a name="numbers_26_7"></a>Numbers 26:7

These are the [mišpāḥâ](../../strongs/h/h4940.md) of the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md): and they that were [paqad](../../strongs/h/h6485.md) of them were forty and three thousand and seven hundred and thirty.

<a name="numbers_26_8"></a>Numbers 26:8

And the [ben](../../strongs/h/h1121.md) of [Pallû'](../../strongs/h/h6396.md); ['Ĕlî'āḇ](../../strongs/h/h446.md).

<a name="numbers_26_9"></a>Numbers 26:9

And the [ben](../../strongs/h/h1121.md) of ['Ĕlî'āḇ](../../strongs/h/h446.md); [Nᵊmû'ēl](../../strongs/h/h5241.md), and [Dāṯān](../../strongs/h/h1885.md), and ['Ăḇîrām](../../strongs/h/h48.md). This is that [Dāṯān](../../strongs/h/h1885.md) and ['Ăḇîrām](../../strongs/h/h48.md), which were [qārî'](../../strongs/h/h7148.md) [qara'](../../strongs/h/h7121.md) in the ['edah](../../strongs/h/h5712.md), who [nāṣâ](../../strongs/h/h5327.md) against [Mōshe](../../strongs/h/h4872.md) and against ['Ahărôn](../../strongs/h/h175.md) in the ['edah](../../strongs/h/h5712.md) of [Qōraḥ](../../strongs/h/h7141.md), when they [nāṣâ](../../strongs/h/h5327.md) against [Yĕhovah](../../strongs/h/h3068.md):

<a name="numbers_26_10"></a>Numbers 26:10

And the ['erets](../../strongs/h/h776.md) [pāṯaḥ](../../strongs/h/h6605.md) her [peh](../../strongs/h/h6310.md), and [bālaʿ](../../strongs/h/h1104.md) them together with [Qōraḥ](../../strongs/h/h7141.md), when that ['edah](../../strongs/h/h5712.md) [maveth](../../strongs/h/h4194.md), what time the ['esh](../../strongs/h/h784.md) ['akal](../../strongs/h/h398.md) two hundred and fifty ['iysh](../../strongs/h/h376.md): and they became a [nēs](../../strongs/h/h5251.md).

<a name="numbers_26_11"></a>Numbers 26:11

Notwithstanding the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md) [muwth](../../strongs/h/h4191.md) not.

<a name="numbers_26_12"></a>Numbers 26:12

The [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md) after their [mišpāḥâ](../../strongs/h/h4940.md): of [Nᵊmû'ēl](../../strongs/h/h5241.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Nᵊmû'ēlî](../../strongs/h/h5242.md): of [Yāmîn](../../strongs/h/h3226.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Yāmînî](../../strongs/h/h3228.md): of [Yāḵîn](../../strongs/h/h3199.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Yāḵînî](../../strongs/h/h3200.md):

<a name="numbers_26_13"></a>Numbers 26:13

Of [Zeraḥ](../../strongs/h/h2226.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Zarḥî](../../strongs/h/h2227.md): of [Šā'ûl](../../strongs/h/h7586.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Šā'ûlî](../../strongs/h/h7587.md).

<a name="numbers_26_14"></a>Numbers 26:14

These are the [mišpāḥâ](../../strongs/h/h4940.md) of the [Šimʿōnî](../../strongs/h/h8099.md), twenty and two thousand and two hundred.

<a name="numbers_26_15"></a>Numbers 26:15

The [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) after their [mišpāḥâ](../../strongs/h/h4940.md): of [Ṣp̄vn](../../strongs/h/h6827.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Ṣᵊp̄ônî](../../strongs/h/h6831.md): of [Ḥagî](../../strongs/h/h2291.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Ḥagî](../../strongs/h/h2291.md): of [Šûnî](../../strongs/h/h7764.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Šûnî](../../strongs/h/h7765.md):

<a name="numbers_26_16"></a>Numbers 26:16

Of ['Āznî](../../strongs/h/h244.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the ['Āznî](../../strongs/h/h244.md): of [ʿĒrî](../../strongs/h/h6179.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [ʿĒrî](../../strongs/h/h6180.md):

<a name="numbers_26_17"></a>Numbers 26:17

Of ['Ărôḏ](../../strongs/h/h720.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the ['Ărôḏî](../../strongs/h/h722.md): of ['Ar'ēlî](../../strongs/h/h692.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the ['Ar'ēlî](../../strongs/h/h692.md).

<a name="numbers_26_18"></a>Numbers 26:18

These are the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) according to those that were [paqad](../../strongs/h/h6485.md) of them, forty thousand and five hundred.

<a name="numbers_26_19"></a>Numbers 26:19

The [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) were [ʿĒr](../../strongs/h/h6147.md) and ['Ônān](../../strongs/h/h209.md): and [ʿĒr](../../strongs/h/h6147.md) and ['Ônān](../../strongs/h/h209.md) [muwth](../../strongs/h/h4191.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md).

<a name="numbers_26_20"></a>Numbers 26:20

And the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) after their [mišpāḥâ](../../strongs/h/h4940.md) were; of [Šēlâ](../../strongs/h/h7956.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Šēlānî](../../strongs/h/h8024.md): of [Pereṣ](../../strongs/h/h6557.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Parṣî](../../strongs/h/h6558.md): of [Zeraḥ](../../strongs/h/h2226.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Zarḥî](../../strongs/h/h2227.md).

<a name="numbers_26_21"></a>Numbers 26:21

And the [ben](../../strongs/h/h1121.md) of [Pereṣ](../../strongs/h/h6557.md) were; of [Ḥeṣrôn](../../strongs/h/h2696.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [ḥeṣrônî](../../strongs/h/h2697.md): of [Ḥāmûl](../../strongs/h/h2538.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Ḥāmûlî](../../strongs/h/h2539.md).

<a name="numbers_26_22"></a>Numbers 26:22

These are the [mišpāḥâ](../../strongs/h/h4940.md)s of [Yehuwdah](../../strongs/h/h3063.md) according to those that were [paqad](../../strongs/h/h6485.md) of them, threescore and sixteen thousand and five hundred.

<a name="numbers_26_23"></a>Numbers 26:23

Of the [ben](../../strongs/h/h1121.md) of [Yiśśāśḵār](../../strongs/h/h3485.md) after their [mišpāḥâ](../../strongs/h/h4940.md): of [Tôlāʿ](../../strongs/h/h8439.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Tôlāʿî](../../strongs/h/h8440.md): of [Pû'â](../../strongs/h/h6312.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Pûnî](../../strongs/h/h6324.md):

<a name="numbers_26_24"></a>Numbers 26:24

Of [Yāšûḇ](../../strongs/h/h3437.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Yāšuḇî](../../strongs/h/h3432.md): of [Šimrôn](../../strongs/h/h8110.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Šimrōnî](../../strongs/h/h8117.md).

<a name="numbers_26_25"></a>Numbers 26:25

These are the [mišpāḥâ](../../strongs/h/h4940.md) of [Yiśśāśḵār](../../strongs/h/h3485.md) according to those that were [paqad](../../strongs/h/h6485.md) of them, threescore and four thousand and three hundred.

<a name="numbers_26_26"></a>Numbers 26:26

Of the [ben](../../strongs/h/h1121.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md) after their [mišpāḥâ](../../strongs/h/h4940.md): of [Sereḏ](../../strongs/h/h5624.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Sardî](../../strongs/h/h5625.md): of ['Êlôn](../../strongs/h/h356.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the ['ēlōnî](../../strongs/h/h440.md): of [Yaḥlᵊ'ēl](../../strongs/h/h3177.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Yaḥlᵊ'ēlî](../../strongs/h/h3178.md).

<a name="numbers_26_27"></a>Numbers 26:27

These are the [mišpāḥâ](../../strongs/h/h4940.md) of the [Zᵊḇûlōnî](../../strongs/h/h2075.md) according to those that were [paqad](../../strongs/h/h6485.md) of them, threescore thousand and five hundred.

<a name="numbers_26_28"></a>Numbers 26:28

The [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md) after their [mišpāḥâ](../../strongs/h/h4940.md) were [Mᵊnaššê](../../strongs/h/h4519.md) and ['Ep̄rayim](../../strongs/h/h669.md).

<a name="numbers_26_29"></a>Numbers 26:29

Of the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md): of [Māḵîr](../../strongs/h/h4353.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Māḵîrî](../../strongs/h/h4354.md): and [Māḵîr](../../strongs/h/h4353.md) [yalad](../../strongs/h/h3205.md) [Gilʿāḏ](../../strongs/h/h1568.md): of [Gilʿāḏ](../../strongs/h/h1568.md) come the [mišpāḥâ](../../strongs/h/h4940.md) of the [Gilʿāḏî](../../strongs/h/h1569.md).

<a name="numbers_26_30"></a>Numbers 26:30

These are the [ben](../../strongs/h/h1121.md) of [Gilʿāḏ](../../strongs/h/h1568.md): of ['ÎʿEzer](../../strongs/h/h372.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the ['Îʿezrî](../../strongs/h/h373.md): of [Ḥēleq](../../strongs/h/h2507.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Ḥelqî](../../strongs/h/h2516.md):

<a name="numbers_26_31"></a>Numbers 26:31

And of ['Aśrî'Ēl](../../strongs/h/h844.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the ['Aśri'ēlî](../../strongs/h/h845.md): and of [Šeḵem](../../strongs/h/h7928.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Šiḵmî](../../strongs/h/h7930.md):

<a name="numbers_26_32"></a>Numbers 26:32

And of [Šᵊmîḏāʿ](../../strongs/h/h8061.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [ŠᵊmîḏāʿÎ](../../strongs/h/h8062.md): and of [Ḥēp̄Er](../../strongs/h/h2660.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Ḥep̄rî](../../strongs/h/h2662.md).

<a name="numbers_26_33"></a>Numbers 26:33

And [Ṣᵊlāp̄Ḥāḏ](../../strongs/h/h6765.md) the [ben](../../strongs/h/h1121.md) of [Ḥēp̄Er](../../strongs/h/h2660.md) had no [ben](../../strongs/h/h1121.md), but [bath](../../strongs/h/h1323.md): and the [shem](../../strongs/h/h8034.md) of the [bath](../../strongs/h/h1323.md) of [Ṣᵊlāp̄Ḥāḏ](../../strongs/h/h6765.md) were [Maḥlâ](../../strongs/h/h4244.md), and [NōʿÂ](../../strongs/h/h5270.md), [Ḥāḡlâ](../../strongs/h/h2295.md), [Milkâ](../../strongs/h/h4435.md), and [Tirṣâ](../../strongs/h/h8656.md).

<a name="numbers_26_34"></a>Numbers 26:34

These are the [mišpāḥâ](../../strongs/h/h4940.md) of [Mᵊnaššê](../../strongs/h/h4519.md), and those that were [paqad](../../strongs/h/h6485.md) of them, fifty and two thousand and seven hundred.

<a name="numbers_26_35"></a>Numbers 26:35

These are the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md) after their [mišpāḥâ](../../strongs/h/h4940.md): of [Šûṯelaḥ](../../strongs/h/h7803.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Šuṯalḥî](../../strongs/h/h8364.md): of [Beḵer](../../strongs/h/h1071.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Baḵrî](../../strongs/h/h1076.md): of [Taḥan](../../strongs/h/h8465.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Taḥanî](../../strongs/h/h8470.md).

<a name="numbers_26_36"></a>Numbers 26:36

And these are the [ben](../../strongs/h/h1121.md) of [Šûṯelaḥ](../../strongs/h/h7803.md): of [ʿĒrān](../../strongs/h/h6197.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [ʿĒrānî](../../strongs/h/h6198.md).

<a name="numbers_26_37"></a>Numbers 26:37

These are the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md) according to those that were [paqad](../../strongs/h/h6485.md) of them, thirty and two thousand and five hundred. These are the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md) after their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="numbers_26_38"></a>Numbers 26:38

The [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) after their [mišpāḥâ](../../strongs/h/h4940.md): of [Belaʿ](../../strongs/h/h1106.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Bilʿî](../../strongs/h/h1108.md): of ['Ašbēl](../../strongs/h/h788.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the ['Ašbēlî](../../strongs/h/h789.md): of ['Ăḥîrām](../../strongs/h/h297.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the ['Ăḥîrāmy](../../strongs/h/h298.md):

<a name="numbers_26_39"></a>Numbers 26:39

Of [Šᵊp̄Ûp̄Ām](../../strongs/h/h8197.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Šûp̄āmî](../../strongs/h/h7781.md): of [Ḥûp̄Ām](../../strongs/h/h2349.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Ḥûp̄āmî](../../strongs/h/h2350.md).

<a name="numbers_26_40"></a>Numbers 26:40

And the [ben](../../strongs/h/h1121.md) of [Belaʿ](../../strongs/h/h1106.md) were ['Ardᵊ](../../strongs/h/h714.md) and [Naʿămān](../../strongs/h/h5283.md): the [mišpāḥâ](../../strongs/h/h4940.md) of the ['Ardî](../../strongs/h/h716.md): and of [Naʿămān](../../strongs/h/h5283.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Naʿămî](../../strongs/h/h5280.md).

<a name="numbers_26_41"></a>Numbers 26:41

These are the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) after their [mišpāḥâ](../../strongs/h/h4940.md): and they that were [paqad](../../strongs/h/h6485.md) of them were forty and five thousand and six hundred.

<a name="numbers_26_42"></a>Numbers 26:42

These are the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md) after their [mišpāḥâ](../../strongs/h/h4940.md): of [Šûḥām](../../strongs/h/h7748.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Šûḥāmî](../../strongs/h/h7749.md). These are the [mišpāḥâ](../../strongs/h/h4940.md) of [Dān](../../strongs/h/h1835.md) after their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="numbers_26_43"></a>Numbers 26:43

All the [mišpāḥâ](../../strongs/h/h4940.md) of the [Šûḥāmî](../../strongs/h/h7749.md), according to those that were [paqad](../../strongs/h/h6485.md) of them, were threescore and four thousand and four hundred.

<a name="numbers_26_44"></a>Numbers 26:44

Of the [ben](../../strongs/h/h1121.md) of ['Āšēr](../../strongs/h/h836.md) after their [mišpāḥâ](../../strongs/h/h4940.md): of [Yimnâ](../../strongs/h/h3232.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Yimnâ](../../strongs/h/h3232.md): of [Yišvî](../../strongs/h/h3440.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Yišvî](../../strongs/h/h3441.md): of [Bᵊrîʿâ](../../strongs/h/h1283.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Bᵊrîʿî](../../strongs/h/h1284.md).

<a name="numbers_26_45"></a>Numbers 26:45

Of the [ben](../../strongs/h/h1121.md) of [Bᵊrîʿâ](../../strongs/h/h1283.md): of [Ḥeḇer](../../strongs/h/h2268.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Ḥeḇrî](../../strongs/h/h2277.md): of [Malkî'ēl](../../strongs/h/h4439.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Malkî'ēlî](../../strongs/h/h4440.md).

<a name="numbers_26_46"></a>Numbers 26:46

And the [shem](../../strongs/h/h8034.md) of the [bath](../../strongs/h/h1323.md) of ['Āšēr](../../strongs/h/h836.md) was [Śeraḥ](../../strongs/h/h8294.md).

<a name="numbers_26_47"></a>Numbers 26:47

These are the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of ['Āšēr](../../strongs/h/h836.md) according to those that were [paqad](../../strongs/h/h6485.md) of them; who were fifty and three thousand and four hundred.

<a name="numbers_26_48"></a>Numbers 26:48

Of the [ben](../../strongs/h/h1121.md) of [Nap̄tālî](../../strongs/h/h5321.md) after their [mišpāḥâ](../../strongs/h/h4940.md)s: of [Yaḥṣᵊ'ēl](../../strongs/h/h3183.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Yaḥṣᵊ'ēlî](../../strongs/h/h3184.md): of [Gûnî](../../strongs/h/h1476.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Gûnî](../../strongs/h/h1477.md):

<a name="numbers_26_49"></a>Numbers 26:49

Of [Yēṣer](../../strongs/h/h3337.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Yiṣrî](../../strongs/h/h3340.md): of [Šillēm](../../strongs/h/h8006.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Šillēmî](../../strongs/h/h8016.md).

<a name="numbers_26_50"></a>Numbers 26:50

These are the [mišpāḥâ](../../strongs/h/h4940.md) of [Nap̄tālî](../../strongs/h/h5321.md) according to their [mišpāḥâ](../../strongs/h/h4940.md): and they that were [paqad](../../strongs/h/h6485.md) of them were forty and five thousand and four hundred.

<a name="numbers_26_51"></a>Numbers 26:51

These were the [paqad](../../strongs/h/h6485.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), six hundred thousand and a thousand seven hundred and thirty.

<a name="numbers_26_52"></a>Numbers 26:52

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_26_53"></a>Numbers 26:53

Unto these the ['erets](../../strongs/h/h776.md) shall be [chalaq](../../strongs/h/h2505.md) for a [nachalah](../../strongs/h/h5159.md) according to the [mispār](../../strongs/h/h4557.md) of [shem](../../strongs/h/h8034.md).

<a name="numbers_26_54"></a>Numbers 26:54

To [rab](../../strongs/h/h7227.md) thou shalt give the [rabah](../../strongs/h/h7235.md) [nachalah](../../strongs/h/h5159.md), and to [mᵊʿaṭ](../../strongs/h/h4592.md) thou shalt give the [māʿaṭ](../../strongs/h/h4591.md) [nachalah](../../strongs/h/h5159.md): to every ['iysh](../../strongs/h/h376.md) shall his [nachalah](../../strongs/h/h5159.md) be [nathan](../../strongs/h/h5414.md) [peh](../../strongs/h/h6310.md) to those that were [paqad](../../strongs/h/h6485.md) of him.

<a name="numbers_26_55"></a>Numbers 26:55

Notwithstanding the ['erets](../../strongs/h/h776.md) shall be [chalaq](../../strongs/h/h2505.md) by [gôrāl](../../strongs/h/h1486.md): according to the [shem](../../strongs/h/h8034.md) of the [maṭṭê](../../strongs/h/h4294.md) of their ['ab](../../strongs/h/h1.md) they shall [nāḥal](../../strongs/h/h5157.md).

<a name="numbers_26_56"></a>Numbers 26:56

[peh](../../strongs/h/h6310.md) to the [gôrāl](../../strongs/h/h1486.md) shall the [nachalah](../../strongs/h/h5159.md) thereof be [chalaq](../../strongs/h/h2505.md) between [rab](../../strongs/h/h7227.md) and [mᵊʿaṭ](../../strongs/h/h4592.md).

<a name="numbers_26_57"></a>Numbers 26:57

And these are they that were [paqad](../../strongs/h/h6485.md) of the [Lᵊvî](../../strongs/h/h3881.md) after their [mišpāḥâ](../../strongs/h/h4940.md): of [Gēršôn](../../strongs/h/h1648.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Gēršunnî](../../strongs/h/h1649.md): of [Qᵊhāṯ](../../strongs/h/h6955.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Qᵊhāṯî](../../strongs/h/h6956.md): of [Mᵊrārî](../../strongs/h/h4847.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Mᵊrārî](../../strongs/h/h4848.md).

<a name="numbers_26_58"></a>Numbers 26:58

These are the [mišpāḥâ](../../strongs/h/h4940.md) of the [Lᵊvî](../../strongs/h/h3881.md): the [mišpāḥâ](../../strongs/h/h4940.md) of the [Liḇnî](../../strongs/h/h3846.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Ḥeḇrônî](../../strongs/h/h2276.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Maḥlî](../../strongs/h/h4250.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [ûMšî](../../strongs/h/h4188.md), the [mišpāḥâ](../../strongs/h/h4940.md) of the [Qārḥî](../../strongs/h/h7145.md). And [Qᵊhāṯ](../../strongs/h/h6955.md) [yalad](../../strongs/h/h3205.md) [ʿAmrām](../../strongs/h/h6019.md).

<a name="numbers_26_59"></a>Numbers 26:59

And the [shem](../../strongs/h/h8034.md) of [ʿAmrām](../../strongs/h/h6019.md) ['ishshah](../../strongs/h/h802.md) was [Yôḵeḇeḏ](../../strongs/h/h3115.md), the [bath](../../strongs/h/h1323.md) of [Lēvî](../../strongs/h/h3878.md), whom her mother [yalad](../../strongs/h/h3205.md) to [Lēvî](../../strongs/h/h3878.md) in [Mitsrayim](../../strongs/h/h4714.md): and she [yalad](../../strongs/h/h3205.md) unto [ʿAmrām](../../strongs/h/h6019.md) ['Ahărôn](../../strongs/h/h175.md) and [Mōshe](../../strongs/h/h4872.md), and [Miryām](../../strongs/h/h4813.md) their ['āḥôṯ](../../strongs/h/h269.md).

<a name="numbers_26_60"></a>Numbers 26:60

And unto ['Ahărôn](../../strongs/h/h175.md) was [yalad](../../strongs/h/h3205.md) [Nāḏāḇ](../../strongs/h/h5070.md), and ['Ăḇîhû'](../../strongs/h/h30.md), ['Elʿāzār](../../strongs/h/h499.md), and ['Îṯāmār](../../strongs/h/h385.md).

<a name="numbers_26_61"></a>Numbers 26:61

And [Nāḏāḇ](../../strongs/h/h5070.md) and ['Ăḇîhû'](../../strongs/h/h30.md) [muwth](../../strongs/h/h4191.md), when they [qāraḇ](../../strongs/h/h7126.md) [zûr](../../strongs/h/h2114.md) ['esh](../../strongs/h/h784.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_26_62"></a>Numbers 26:62

And those that were [paqad](../../strongs/h/h6485.md) of them were twenty and three thousand, all [zāḵār](../../strongs/h/h2145.md) from a [ḥōḏeš](../../strongs/h/h2320.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md): for they were not [paqad](../../strongs/h/h6485.md) among the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), because there was no [nachalah](../../strongs/h/h5159.md) [nathan](../../strongs/h/h5414.md) them [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_26_63"></a>Numbers 26:63

These are they that were [paqad](../../strongs/h/h6485.md) by [Mōshe](../../strongs/h/h4872.md) and ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), who [paqad](../../strongs/h/h6485.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) in the ['arabah](../../strongs/h/h6160.md) of [Mô'āḇ](../../strongs/h/h4124.md) by [Yardēn](../../strongs/h/h3383.md) near [Yᵊrēḥô](../../strongs/h/h3405.md).

<a name="numbers_26_64"></a>Numbers 26:64

But among these there was not an ['iysh](../../strongs/h/h376.md) of them whom [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md) [paqad](../../strongs/h/h6485.md), when they [paqad](../../strongs/h/h6485.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) in the [midbar](../../strongs/h/h4057.md) of [Sînay](../../strongs/h/h5514.md).

<a name="numbers_26_65"></a>Numbers 26:65

For [Yĕhovah](../../strongs/h/h3068.md) had ['āmar](../../strongs/h/h559.md) of them, They shall [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md) in the [midbar](../../strongs/h/h4057.md). And there was not [yāṯar](../../strongs/h/h3498.md) an ['iysh](../../strongs/h/h376.md) of them, save [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md), and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 25](numbers_25.md) - [Numbers 27](numbers_27.md)