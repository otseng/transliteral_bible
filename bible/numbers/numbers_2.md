# [Numbers 2](https://www.blueletterbible.org/kjv/num/2)

<a name="numbers_2_1"></a>Numbers 2:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) and unto ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_2_2"></a>Numbers 2:2

Every ['iysh](../../strongs/h/h376.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) shall [ḥānâ](../../strongs/h/h2583.md) by his own [deḡel](../../strongs/h/h1714.md), with the ['ôṯ](../../strongs/h/h226.md) of their ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md): [neḡeḏ](../../strongs/h/h5048.md) [cabiyb](../../strongs/h/h5439.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) shall they [ḥānâ](../../strongs/h/h2583.md).

<a name="numbers_2_3"></a>Numbers 2:3

And on the [qeḏem](../../strongs/h/h6924.md) toward the [mizrach](../../strongs/h/h4217.md) of the sun shall they of the [deḡel](../../strongs/h/h1714.md) of the [maḥănê](../../strongs/h/h4264.md) of [Yehuwdah](../../strongs/h/h3063.md) [ḥānâ](../../strongs/h/h2583.md) throughout their [tsaba'](../../strongs/h/h6635.md): and [Naḥšôn](../../strongs/h/h5177.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmînāḏāḇ](../../strongs/h/h5992.md) shall be [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="numbers_2_4"></a>Numbers 2:4

And his [tsaba'](../../strongs/h/h6635.md), and those that were [paqad](../../strongs/h/h6485.md) of them, were threescore and fourteen thousand and six hundred.

<a name="numbers_2_5"></a>Numbers 2:5

And those that do [ḥānâ](../../strongs/h/h2583.md) next unto him shall be the [maṭṭê](../../strongs/h/h4294.md) of [Yiśśāśḵār](../../strongs/h/h3485.md): and [Nᵊṯan'ēl](../../strongs/h/h5417.md) the [ben](../../strongs/h/h1121.md) of [ṣûʿar](../../strongs/h/h6686.md) shall be [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Yiśśāśḵār](../../strongs/h/h3485.md).

<a name="numbers_2_6"></a>Numbers 2:6

And his [tsaba'](../../strongs/h/h6635.md), and those that were [paqad](../../strongs/h/h6485.md) thereof, were fifty and four thousand and four hundred.

<a name="numbers_2_7"></a>Numbers 2:7

Then the [maṭṭê](../../strongs/h/h4294.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md): and ['Ĕlî'āḇ](../../strongs/h/h446.md) the [ben](../../strongs/h/h1121.md) of [ḥēlōn](../../strongs/h/h2497.md) shall be [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md).

<a name="numbers_2_8"></a>Numbers 2:8

And his [tsaba'](../../strongs/h/h6635.md), and those that were [paqad](../../strongs/h/h6485.md) thereof, were fifty and seven thousand and four hundred.

<a name="numbers_2_9"></a>Numbers 2:9

All that were [paqad](../../strongs/h/h6485.md) in the [maḥănê](../../strongs/h/h4264.md) of [Yehuwdah](../../strongs/h/h3063.md) were an hundred thousand and fourscore thousand and six thousand and four hundred, throughout their [tsaba'](../../strongs/h/h6635.md). These shall [ri'šôn](../../strongs/h/h7223.md) [nāsaʿ](../../strongs/h/h5265.md).

<a name="numbers_2_10"></a>Numbers 2:10

On the [têmān](../../strongs/h/h8486.md) shall be the [deḡel](../../strongs/h/h1714.md) of the [maḥănê](../../strongs/h/h4264.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) according to their [tsaba'](../../strongs/h/h6635.md): and the [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) shall be ['ĕlîṣûr](../../strongs/h/h468.md) the [ben](../../strongs/h/h1121.md) of [šᵊḏê'ûr](../../strongs/h/h7707.md).

<a name="numbers_2_11"></a>Numbers 2:11

And his [tsaba'](../../strongs/h/h6635.md), and those that were [paqad](../../strongs/h/h6485.md) thereof, were forty and six thousand and five hundred.

<a name="numbers_2_12"></a>Numbers 2:12

And those which [ḥānâ](../../strongs/h/h2583.md) by him shall be the [maṭṭê](../../strongs/h/h4294.md) of [Šimʿôn](../../strongs/h/h8095.md): and the [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md) shall be [šᵊlumî'ēl](../../strongs/h/h8017.md) the [ben](../../strongs/h/h1121.md) of [ṣûrîšaday](../../strongs/h/h6701.md).

<a name="numbers_2_13"></a>Numbers 2:13

And his [tsaba'](../../strongs/h/h6635.md), and those that were [paqad](../../strongs/h/h6485.md) of them, were fifty and nine thousand and three hundred.

<a name="numbers_2_14"></a>Numbers 2:14

Then the [maṭṭê](../../strongs/h/h4294.md) of [Gāḏ](../../strongs/h/h1410.md): and the [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) shall be ['elyāsāp̄](../../strongs/h/h460.md) the [ben](../../strongs/h/h1121.md) of [Rᵊʿû'ēl](../../strongs/h/h7467.md).

<a name="numbers_2_15"></a>Numbers 2:15

And his [tsaba'](../../strongs/h/h6635.md), and those that were [paqad](../../strongs/h/h6485.md) of them, were forty and five thousand and six hundred and fifty.

<a name="numbers_2_16"></a>Numbers 2:16

All that were [paqad](../../strongs/h/h6485.md) in the [maḥănê](../../strongs/h/h4264.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) were an hundred thousand and fifty and one thousand and four hundred and fifty, throughout their [tsaba'](../../strongs/h/h6635.md). And they shall [nāsaʿ](../../strongs/h/h5265.md) in the second.

<a name="numbers_2_17"></a>Numbers 2:17

Then the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) shall [nāsaʿ](../../strongs/h/h5265.md) with the [maḥănê](../../strongs/h/h4264.md) of the [Lᵊvî](../../strongs/h/h3881.md) in the [tavek](../../strongs/h/h8432.md) of the [maḥănê](../../strongs/h/h4264.md): as they [ḥānâ](../../strongs/h/h2583.md), so shall they [nāsaʿ](../../strongs/h/h5265.md), every ['iysh](../../strongs/h/h376.md) in his [yad](../../strongs/h/h3027.md) by their [deḡel](../../strongs/h/h1714.md).

<a name="numbers_2_18"></a>Numbers 2:18

On the [yam](../../strongs/h/h3220.md) shall be the [deḡel](../../strongs/h/h1714.md) of the [maḥănê](../../strongs/h/h4264.md) of ['Ep̄rayim](../../strongs/h/h669.md) according to their [tsaba'](../../strongs/h/h6635.md): and the [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md) shall be ['Ĕlîšāmāʿ](../../strongs/h/h476.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmîhûḏ](../../strongs/h/h5989.md).

<a name="numbers_2_19"></a>Numbers 2:19

And his [tsaba'](../../strongs/h/h6635.md), and those that were [paqad](../../strongs/h/h6485.md) of them, were forty thousand and five hundred.

<a name="numbers_2_20"></a>Numbers 2:20

And by him shall be the [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md): and the [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md) shall be [gamlî'ēl](../../strongs/h/h1583.md) the [ben](../../strongs/h/h1121.md) of [pᵊḏâṣûr](../../strongs/h/h6301.md).

<a name="numbers_2_21"></a>Numbers 2:21

And his [tsaba'](../../strongs/h/h6635.md), and those that were [paqad](../../strongs/h/h6485.md) of them, were thirty and two thousand and two hundred.

<a name="numbers_2_22"></a>Numbers 2:22

Then the [maṭṭê](../../strongs/h/h4294.md) of [Binyāmîn](../../strongs/h/h1144.md): and the [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) shall be ['ăḇîḏān](../../strongs/h/h27.md) the [ben](../../strongs/h/h1121.md) of [giḏʿōnî](../../strongs/h/h1441.md).

<a name="numbers_2_23"></a>Numbers 2:23

And his [tsaba'](../../strongs/h/h6635.md), and those that were [paqad](../../strongs/h/h6485.md) of them, were thirty and five thousand and four hundred.

<a name="numbers_2_24"></a>Numbers 2:24

All that were [paqad](../../strongs/h/h6485.md) of the [maḥănê](../../strongs/h/h4264.md) of ['Ep̄rayim](../../strongs/h/h669.md) were an hundred thousand and eight thousand and an hundred, throughout their [tsaba'](../../strongs/h/h6635.md). And they shall [nāsaʿ](../../strongs/h/h5265.md) in the third.

<a name="numbers_2_25"></a>Numbers 2:25

The [deḡel](../../strongs/h/h1714.md) of the [maḥănê](../../strongs/h/h4264.md) of [Dān](../../strongs/h/h1835.md) shall be on the [ṣāp̄ôn](../../strongs/h/h6828.md) by their [tsaba'](../../strongs/h/h6635.md): and the [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md) shall be ['ăḥîʿezer](../../strongs/h/h295.md) the [ben](../../strongs/h/h1121.md) of [ʿammîšaḏay](../../strongs/h/h5996.md).

<a name="numbers_2_26"></a>Numbers 2:26

And his [tsaba'](../../strongs/h/h6635.md), and those that were [paqad](../../strongs/h/h6485.md) of them, were threescore and two thousand and seven hundred.

<a name="numbers_2_27"></a>Numbers 2:27

And those that [ḥānâ](../../strongs/h/h2583.md) by him shall be the [maṭṭê](../../strongs/h/h4294.md) of ['Āšēr](../../strongs/h/h836.md): and the [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of ['Āšēr](../../strongs/h/h836.md) shall be [Paḡʿî'ēl](../../strongs/h/h6295.md) the [ben](../../strongs/h/h1121.md) of [ʿāḵrān](../../strongs/h/h5918.md).

<a name="numbers_2_28"></a>Numbers 2:28

And his [tsaba'](../../strongs/h/h6635.md), and those that were [paqad](../../strongs/h/h6485.md) of them, were forty and one thousand and five hundred.

<a name="numbers_2_29"></a>Numbers 2:29

Then the [maṭṭê](../../strongs/h/h4294.md) of [Nap̄tālî](../../strongs/h/h5321.md): and the [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Nap̄tālî](../../strongs/h/h5321.md) shall be ['ăḥîraʿ](../../strongs/h/h299.md) the [ben](../../strongs/h/h1121.md) of [ʿênān](../../strongs/h/h5881.md).

<a name="numbers_2_30"></a>Numbers 2:30

And his [tsaba'](../../strongs/h/h6635.md), and those that were [paqad](../../strongs/h/h6485.md) of them, were fifty and three thousand and four hundred.

<a name="numbers_2_31"></a>Numbers 2:31

All they that were [paqad](../../strongs/h/h6485.md) in the [maḥănê](../../strongs/h/h4264.md) of [Dān](../../strongs/h/h1835.md) were an hundred thousand and fifty and seven thousand and six hundred. They shall [nāsaʿ](../../strongs/h/h5265.md) ['aḥărôn](../../strongs/h/h314.md) with their [deḡel](../../strongs/h/h1714.md).

<a name="numbers_2_32"></a>Numbers 2:32

These are those which were [paqad](../../strongs/h/h6485.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md): all those that were [paqad](../../strongs/h/h6485.md) of the [maḥănê](../../strongs/h/h4264.md) throughout their [tsaba'](../../strongs/h/h6635.md) were six hundred thousand and three thousand and five hundred and fifty.

<a name="numbers_2_33"></a>Numbers 2:33

But the [Lᵊvî](../../strongs/h/h3881.md) were not [paqad](../../strongs/h/h6485.md) [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="numbers_2_34"></a>Numbers 2:34

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) according to all that [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md): so they [ḥānâ](../../strongs/h/h2583.md) by their [deḡel](../../strongs/h/h1714.md), and so they [nāsaʿ](../../strongs/h/h5265.md), every ['iysh](../../strongs/h/h376.md) after their [mišpāḥâ](../../strongs/h/h4940.md), according to the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 1](numbers_1.md) - [Numbers 3](numbers_3.md)