# [Numbers 32](https://www.blueletterbible.org/kjv/num/32)

<a name="numbers_32_1"></a>Numbers 32:1

Now the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) and the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) had a [me'od](../../strongs/h/h3966.md) ['atsuwm](../../strongs/h/h6099.md) [rab](../../strongs/h/h7227.md) of [miqnê](../../strongs/h/h4735.md): and when they [ra'ah](../../strongs/h/h7200.md) the ['erets](../../strongs/h/h776.md) of [Yaʿzêr](../../strongs/h/h3270.md), and the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md), that, behold, the [maqowm](../../strongs/h/h4725.md) was a [maqowm](../../strongs/h/h4725.md) for [miqnê](../../strongs/h/h4735.md);

<a name="numbers_32_2"></a>Numbers 32:2

The [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) and the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) [bow'](../../strongs/h/h935.md) and ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), and to ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and unto the [nāśî'](../../strongs/h/h5387.md) of the ['edah](../../strongs/h/h5712.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_32_3"></a>Numbers 32:3

[ʿĂṭārôṯ](../../strongs/h/h5852.md), and [Dîḇōvn](../../strongs/h/h1769.md), and [Yaʿzêr](../../strongs/h/h3270.md), and [Nimrâ](../../strongs/h/h5247.md), and [Hešbôn](../../strongs/h/h2809.md), and ['Elʿālē'](../../strongs/h/h500.md), and [Śᵊḇām](../../strongs/h/h7643.md), and [Nᵊḇô](../../strongs/h/h5015.md), and [Bᵊʿōn](../../strongs/h/h1194.md),

<a name="numbers_32_4"></a>Numbers 32:4

Even the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) [nakah](../../strongs/h/h5221.md) [paniym](../../strongs/h/h6440.md) the ['edah](../../strongs/h/h5712.md) of [Yisra'el](../../strongs/h/h3478.md), is an ['erets](../../strongs/h/h776.md) for [miqnê](../../strongs/h/h4735.md), and thy ['ebed](../../strongs/h/h5650.md) have [miqnê](../../strongs/h/h4735.md):

<a name="numbers_32_5"></a>Numbers 32:5

Wherefore, ['āmar](../../strongs/h/h559.md) they, if we have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md), let this ['erets](../../strongs/h/h776.md) be [nathan](../../strongs/h/h5414.md) unto thy ['ebed](../../strongs/h/h5650.md) for an ['achuzzah](../../strongs/h/h272.md), and ['abar](../../strongs/h/h5674.md) us not [Yardēn](../../strongs/h/h3383.md).

<a name="numbers_32_6"></a>Numbers 32:6

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) and to the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), Shall your ['ach](../../strongs/h/h251.md) [bow'](../../strongs/h/h935.md) to [milḥāmâ](../../strongs/h/h4421.md), and shall ye [yashab](../../strongs/h/h3427.md) here?

<a name="numbers_32_7"></a>Numbers 32:7

And wherefore [nô'](../../strongs/h/h5106.md) [nô'](../../strongs/h/h5106.md) ye the [leb](../../strongs/h/h3820.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) from ['abar](../../strongs/h/h5674.md) into the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) them?

<a name="numbers_32_8"></a>Numbers 32:8

Thus ['asah](../../strongs/h/h6213.md) your ['ab](../../strongs/h/h1.md), when I [shalach](../../strongs/h/h7971.md) them from [Qāḏēš Barnēaʿ](../../strongs/h/h6947.md) to [ra'ah](../../strongs/h/h7200.md) the ['erets](../../strongs/h/h776.md).

<a name="numbers_32_9"></a>Numbers 32:9

For when they [ʿālâ](../../strongs/h/h5927.md) unto the [nachal](../../strongs/h/h5158.md) of ['eškōl](../../strongs/h/h812.md), and [ra'ah](../../strongs/h/h7200.md) the ['erets](../../strongs/h/h776.md), they [nô'](../../strongs/h/h5106.md) the [leb](../../strongs/h/h3820.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they should not [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) had [nathan](../../strongs/h/h5414.md) them.

<a name="numbers_32_10"></a>Numbers 32:10

And [Yĕhovah](../../strongs/h/h3068.md) ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md) the same [yowm](../../strongs/h/h3117.md), and he [shaba'](../../strongs/h/h7650.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_32_11"></a>Numbers 32:11

Surely none of the ['enowsh](../../strongs/h/h582.md) that [ʿālâ](../../strongs/h/h5927.md) out of [Mitsrayim](../../strongs/h/h4714.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), shall [ra'ah](../../strongs/h/h7200.md) the ['ăḏāmâ](../../strongs/h/h127.md) which I [shaba'](../../strongs/h/h7650.md) unto ['Abraham](../../strongs/h/h85.md), unto [Yiṣḥāq](../../strongs/h/h3327.md), and unto [Ya'aqob](../../strongs/h/h3290.md); because they have not [mālā'](../../strongs/h/h4390.md) ['aḥar](../../strongs/h/h310.md) me:

<a name="numbers_32_12"></a>Numbers 32:12

Save [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md) the [qᵊnizzî](../../strongs/h/h7074.md), and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md): for they have [mālā'](../../strongs/h/h4390.md) ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_32_13"></a>Numbers 32:13

And [Yĕhovah](../../strongs/h/h3068.md) ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md) against [Yisra'el](../../strongs/h/h3478.md), and he made them [nûaʿ](../../strongs/h/h5128.md) in the [midbar](../../strongs/h/h4057.md) forty [šānâ](../../strongs/h/h8141.md), until all the [dôr](../../strongs/h/h1755.md), that had ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), was [tamam](../../strongs/h/h8552.md).

<a name="numbers_32_14"></a>Numbers 32:14

And, behold, ye are [quwm](../../strongs/h/h6965.md) in your ['ab](../../strongs/h/h1.md) stead, a [tarbûṯ](../../strongs/h/h8635.md) of [chatta'](../../strongs/h/h2400.md) ['enowsh](../../strongs/h/h582.md), to [sāp̄â](../../strongs/h/h5595.md) yet the [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) toward [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_32_15"></a>Numbers 32:15

For if ye [shuwb](../../strongs/h/h7725.md) from ['aḥar](../../strongs/h/h310.md) him, he will yet [yāsap̄](../../strongs/h/h3254.md) [yānaḥ](../../strongs/h/h3240.md) them in the [midbar](../../strongs/h/h4057.md); and ye shall [shachath](../../strongs/h/h7843.md) all this ['am](../../strongs/h/h5971.md).

<a name="numbers_32_16"></a>Numbers 32:16

And they [nāḡaš](../../strongs/h/h5066.md) unto him, and ['āmar](../../strongs/h/h559.md), We will [bānâ](../../strongs/h/h1129.md) [gᵊḏērâ](../../strongs/h/h1448.md) [tso'n](../../strongs/h/h6629.md) here for our [miqnê](../../strongs/h/h4735.md), and [ʿîr](../../strongs/h/h5892.md) for our [ṭap̄](../../strongs/h/h2945.md):

<a name="numbers_32_17"></a>Numbers 32:17

But we ourselves will [ḥûš](../../strongs/h/h2363.md) [chalats](../../strongs/h/h2502.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), until we have [bow'](../../strongs/h/h935.md) them unto their [maqowm](../../strongs/h/h4725.md): and our [ṭap̄](../../strongs/h/h2945.md) shall [yashab](../../strongs/h/h3427.md) in the [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md) [paniym](../../strongs/h/h6440.md) of the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md).

<a name="numbers_32_18"></a>Numbers 32:18

We will not [shuwb](../../strongs/h/h7725.md) unto our [bayith](../../strongs/h/h1004.md), until the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) have [nāḥal](../../strongs/h/h5157.md) every ['iysh](../../strongs/h/h376.md) his [nachalah](../../strongs/h/h5159.md).

<a name="numbers_32_19"></a>Numbers 32:19

For we will not [nāḥal](../../strongs/h/h5157.md) with them on [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md), or [hāl'â](../../strongs/h/h1973.md); because our [nachalah](../../strongs/h/h5159.md) is [bow'](../../strongs/h/h935.md) to us on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) [mizrach](../../strongs/h/h4217.md).

<a name="numbers_32_20"></a>Numbers 32:20

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto them, If ye will ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md), if ye will [chalats](../../strongs/h/h2502.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) to [milḥāmâ](../../strongs/h/h4421.md),

<a name="numbers_32_21"></a>Numbers 32:21

And will ['abar](../../strongs/h/h5674.md) all of you [chalats](../../strongs/h/h2502.md) over [Yardēn](../../strongs/h/h3383.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), until he hath [yarash](../../strongs/h/h3423.md) his ['oyeb](../../strongs/h/h341.md) from [paniym](../../strongs/h/h6440.md) him,

<a name="numbers_32_22"></a>Numbers 32:22

And the ['erets](../../strongs/h/h776.md) be [kāḇaš](../../strongs/h/h3533.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): then ['aḥar](../../strongs/h/h310.md) ye shall [shuwb](../../strongs/h/h7725.md), and be [naqiy](../../strongs/h/h5355.md) before [Yĕhovah](../../strongs/h/h3068.md), and before [Yisra'el](../../strongs/h/h3478.md); and this ['erets](../../strongs/h/h776.md) shall be your ['achuzzah](../../strongs/h/h272.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_32_23"></a>Numbers 32:23

But if ye will not ['asah](../../strongs/h/h6213.md) so, behold, ye have [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md): and [yada'](../../strongs/h/h3045.md) your [chatta'ath](../../strongs/h/h2403.md) will [māṣā'](../../strongs/h/h4672.md) you.

<a name="numbers_32_24"></a>Numbers 32:24

[bānâ](../../strongs/h/h1129.md) you [ʿîr](../../strongs/h/h5892.md) for your [ṭap̄](../../strongs/h/h2945.md), and [gᵊḏērâ](../../strongs/h/h1448.md) for your [tsone'](../../strongs/h/h6792.md); and ['asah](../../strongs/h/h6213.md) that which hath [yāṣā'](../../strongs/h/h3318.md) of your [peh](../../strongs/h/h6310.md).

<a name="numbers_32_25"></a>Numbers 32:25

And the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) and the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md), Thy ['ebed](../../strongs/h/h5650.md) will ['asah](../../strongs/h/h6213.md) as my ['adown](../../strongs/h/h113.md) [tsavah](../../strongs/h/h6680.md).

<a name="numbers_32_26"></a>Numbers 32:26

Our [ṭap̄](../../strongs/h/h2945.md), our ['ishshah](../../strongs/h/h802.md), our [miqnê](../../strongs/h/h4735.md), and all our [bĕhemah](../../strongs/h/h929.md), shall be there in the [ʿîr](../../strongs/h/h5892.md) of [Gilʿāḏ](../../strongs/h/h1568.md):

<a name="numbers_32_27"></a>Numbers 32:27

But thy ['ebed](../../strongs/h/h5650.md) will ['abar](../../strongs/h/h5674.md), every [chalats](../../strongs/h/h2502.md) for [tsaba'](../../strongs/h/h6635.md), [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) to [milḥāmâ](../../strongs/h/h4421.md), as my ['adown](../../strongs/h/h113.md) [dabar](../../strongs/h/h1696.md).

<a name="numbers_32_28"></a>Numbers 32:28

So concerning them [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md) ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), and the [ro'sh](../../strongs/h/h7218.md) ['ab](../../strongs/h/h1.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="numbers_32_29"></a>Numbers 32:29

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto them, If the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) and the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) will ['abar](../../strongs/h/h5674.md) with you [Yardēn](../../strongs/h/h3383.md), every [chalats](../../strongs/h/h2502.md) to [milḥāmâ](../../strongs/h/h4421.md), [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and the ['erets](../../strongs/h/h776.md) shall be [kāḇaš](../../strongs/h/h3533.md) [paniym](../../strongs/h/h6440.md) you; then ye shall [nathan](../../strongs/h/h5414.md) them the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md) for an ['achuzzah](../../strongs/h/h272.md):

<a name="numbers_32_30"></a>Numbers 32:30

But if they will not ['abar](../../strongs/h/h5674.md) with you [chalats](../../strongs/h/h2502.md), they shall have ['āḥaz](../../strongs/h/h270.md) [tavek](../../strongs/h/h8432.md) you in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md).

<a name="numbers_32_31"></a>Numbers 32:31

And the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) and the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) ['anah](../../strongs/h/h6030.md), ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) unto thy ['ebed](../../strongs/h/h5650.md), so will we ['asah](../../strongs/h/h6213.md).

<a name="numbers_32_32"></a>Numbers 32:32

We will ['abar](../../strongs/h/h5674.md) [chalats](../../strongs/h/h2502.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) into the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), that the ['achuzzah](../../strongs/h/h272.md) of our [nachalah](../../strongs/h/h5159.md) on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) may be ours.

<a name="numbers_32_33"></a>Numbers 32:33

And [Mōshe](../../strongs/h/h4872.md) [nathan](../../strongs/h/h5414.md) unto them, even to the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md), and to the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), and unto [ḥēṣî](../../strongs/h/h2677.md) the [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md) the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md), the [mamlāḵâ](../../strongs/h/h4467.md) of [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), and the [mamlāḵâ](../../strongs/h/h4467.md) of [ʿÔḡ](../../strongs/h/h5747.md) [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md), the ['erets](../../strongs/h/h776.md), with the [ʿîr](../../strongs/h/h5892.md) thereof in the [gᵊḇûlâ](../../strongs/h/h1367.md), even the [ʿîr](../../strongs/h/h5892.md) of the ['erets](../../strongs/h/h776.md) [cabiyb](../../strongs/h/h5439.md).

<a name="numbers_32_34"></a>Numbers 32:34

And the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) [bānâ](../../strongs/h/h1129.md) [Dîḇōvn](../../strongs/h/h1769.md), and [ʿĂṭārôṯ](../../strongs/h/h5852.md), and [ʿĂrôʿēr](../../strongs/h/h6177.md),

<a name="numbers_32_35"></a>Numbers 32:35

And [ʿAṭrôṯ Šôp̄Ān](../../strongs/h/h5855.md), and [Yaʿzêr](../../strongs/h/h3270.md), and [Yāḡbahâ](../../strongs/h/h3011.md),

<a name="numbers_32_36"></a>Numbers 32:36

And [Bêṯ Nimrâ](../../strongs/h/h1039.md), and [Bêṯ Hārān](../../strongs/h/h1028.md), [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md): and [gᵊḏērâ](../../strongs/h/h1448.md) for [tso'n](../../strongs/h/h6629.md).

<a name="numbers_32_37"></a>Numbers 32:37

And the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) [bānâ](../../strongs/h/h1129.md) [Hešbôn](../../strongs/h/h2809.md), and ['Elʿālē'](../../strongs/h/h500.md), and [Qiryāṯayim](../../strongs/h/h7156.md),

<a name="numbers_32_38"></a>Numbers 32:38

And [Nᵊḇô](../../strongs/h/h5015.md), and [BaʿAl MᵊʿÔn](../../strongs/h/h1186.md), (their [shem](../../strongs/h/h8034.md) being [mûsaḇâ](../../strongs/h/h4142.md),) and [Śᵊḇām](../../strongs/h/h7643.md): and [qara'](../../strongs/h/h7121.md) other [shem](../../strongs/h/h8034.md) unto the [ʿîr](../../strongs/h/h5892.md) which they [bānâ](../../strongs/h/h1129.md).

<a name="numbers_32_39"></a>Numbers 32:39

And the [ben](../../strongs/h/h1121.md) of [Māḵîr](../../strongs/h/h4353.md) the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md) [yālaḵ](../../strongs/h/h3212.md) to [Gilʿāḏ](../../strongs/h/h1568.md), and [lāḵaḏ](../../strongs/h/h3920.md) it, and [yarash](../../strongs/h/h3423.md) the ['Ĕmōrî](../../strongs/h/h567.md) which was in it.

<a name="numbers_32_40"></a>Numbers 32:40

And [Mōshe](../../strongs/h/h4872.md) [nathan](../../strongs/h/h5414.md) [Gilʿāḏ](../../strongs/h/h1568.md) unto [Māḵîr](../../strongs/h/h4353.md) the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md); and he [yashab](../../strongs/h/h3427.md) therein.

<a name="numbers_32_41"></a>Numbers 32:41

And [Yā'Îr](../../strongs/h/h2971.md) the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md) [halak](../../strongs/h/h1980.md) and [lāḵaḏ](../../strongs/h/h3920.md) the [ḥaûâ](../../strongs/h/h2333.md) thereof, and [qara'](../../strongs/h/h7121.md) them [Ḥaûôṯ YāʿÎr](../../strongs/h/h2334.md).

<a name="numbers_32_42"></a>Numbers 32:42

And [Nōḇaḥ](../../strongs/h/h5025.md) [halak](../../strongs/h/h1980.md) and [lāḵaḏ](../../strongs/h/h3920.md) [Qᵊnāṯ](../../strongs/h/h7079.md), and the [bath](../../strongs/h/h1323.md) thereof, and [qara'](../../strongs/h/h7121.md) it [Nōḇaḥ](../../strongs/h/h5025.md), after his own [shem](../../strongs/h/h8034.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 31](numbers_31.md) - [Numbers 33](numbers_33.md)