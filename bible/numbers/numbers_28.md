# [Numbers 28](https://www.blueletterbible.org/kjv/num/28)

<a name="numbers_28_1"></a>Numbers 28:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_28_2"></a>Numbers 28:2

[tsavah](../../strongs/h/h6680.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, My [qorban](../../strongs/h/h7133.md), and my [lechem](../../strongs/h/h3899.md) for my ['iššê](../../strongs/h/h801.md), for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto me, shall ye [shamar](../../strongs/h/h8104.md) to [qāraḇ](../../strongs/h/h7126.md) unto me in their [môʿēḏ](../../strongs/h/h4150.md).

<a name="numbers_28_3"></a>Numbers 28:3

And thou shalt ['āmar](../../strongs/h/h559.md) unto them, This is the ['iššê](../../strongs/h/h801.md) which ye shall [qāraḇ](../../strongs/h/h7126.md) unto [Yĕhovah](../../strongs/h/h3068.md); two [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md) [yowm](../../strongs/h/h3117.md), for a [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md).

<a name="numbers_28_4"></a>Numbers 28:4

The one [keḇeś](../../strongs/h/h3532.md) shalt thou ['asah](../../strongs/h/h6213.md) in the [boqer](../../strongs/h/h1242.md), and the other [keḇeś](../../strongs/h/h3532.md) shalt thou ['asah](../../strongs/h/h6213.md) at ['ereb](../../strongs/h/h6153.md);

<a name="numbers_28_5"></a>Numbers 28:5

And a tenth of an ['êp̄â](../../strongs/h/h374.md) of [sōleṯ](../../strongs/h/h5560.md) for a [minchah](../../strongs/h/h4503.md), [bālal](../../strongs/h/h1101.md) with the fourth of an [hîn](../../strongs/h/h1969.md) of [kāṯîṯ](../../strongs/h/h3795.md) [šemen](../../strongs/h/h8081.md).

<a name="numbers_28_6"></a>Numbers 28:6

It is a [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), which was ['asah](../../strongs/h/h6213.md) in [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md) for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md), an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_28_7"></a>Numbers 28:7

And the drink [necek](../../strongs/h/h5262.md) thereof shall be the fourth of an [hîn](../../strongs/h/h1969.md) for the one [keḇeś](../../strongs/h/h3532.md): in the [qodesh](../../strongs/h/h6944.md) shalt thou cause the [šēḵār](../../strongs/h/h7941.md) to be [nacak](../../strongs/h/h5258.md) unto [Yĕhovah](../../strongs/h/h3068.md) for a [necek](../../strongs/h/h5262.md).

<a name="numbers_28_8"></a>Numbers 28:8

And the other [keḇeś](../../strongs/h/h3532.md) shalt thou ['asah](../../strongs/h/h6213.md) at ['ereb](../../strongs/h/h6153.md): as the [minchah](../../strongs/h/h4503.md) of the [boqer](../../strongs/h/h1242.md), and as the [necek](../../strongs/h/h5262.md) thereof, thou shalt ['asah](../../strongs/h/h6213.md) it, an ['iššê](../../strongs/h/h801.md), of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_28_9"></a>Numbers 28:9

And on the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md) two [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md), and two tenth of [sōleṯ](../../strongs/h/h5560.md) for a [minchah](../../strongs/h/h4503.md), [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), and the [necek](../../strongs/h/h5262.md) thereof:

<a name="numbers_28_10"></a>Numbers 28:10

This is the an [ʿōlâ](../../strongs/h/h5930.md) of every [shabbath](../../strongs/h/h7676.md), beside the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), and his [necek](../../strongs/h/h5262.md).

<a name="numbers_28_11"></a>Numbers 28:11

And in the [ro'sh](../../strongs/h/h7218.md) of your [ḥōḏeš](../../strongs/h/h2320.md) ye shall [qāraḇ](../../strongs/h/h7126.md) an [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md); two [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), and one ['ayil](../../strongs/h/h352.md), seven [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md);

<a name="numbers_28_12"></a>Numbers 28:12

And three [ʿiśśārôn](../../strongs/h/h6241.md)s of [sōleṯ](../../strongs/h/h5560.md) for a [minchah](../../strongs/h/h4503.md), [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), for one [par](../../strongs/h/h6499.md); and two [ʿiśśārôn](../../strongs/h/h6241.md)s of [sōleṯ](../../strongs/h/h5560.md) for a [minchah](../../strongs/h/h4503.md), [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), for one ['ayil](../../strongs/h/h352.md);

<a name="numbers_28_13"></a>Numbers 28:13

And a several [ʿiśśārôn](../../strongs/h/h6241.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md) unto one [keḇeś](../../strongs/h/h3532.md); for an [ʿōlâ](../../strongs/h/h5930.md) of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md), an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_28_14"></a>Numbers 28:14

And their [necek](../../strongs/h/h5262.md) shall be half an [hîn](../../strongs/h/h1969.md) of [yayin](../../strongs/h/h3196.md) unto a [par](../../strongs/h/h6499.md), and the third part of an [hîn](../../strongs/h/h1969.md) unto an ['ayil](../../strongs/h/h352.md), and a fourth part of an [hîn](../../strongs/h/h1969.md) unto a [keḇeś](../../strongs/h/h3532.md): this is the [ʿōlâ](../../strongs/h/h5930.md) of every [ḥōḏeš](../../strongs/h/h2320.md) throughout the [ḥōḏeš](../../strongs/h/h2320.md) of the [šānâ](../../strongs/h/h8141.md).

<a name="numbers_28_15"></a>Numbers 28:15

And one [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md) unto [Yĕhovah](../../strongs/h/h3068.md) shall be ['asah](../../strongs/h/h6213.md), beside the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), and his [necek](../../strongs/h/h5262.md).

<a name="numbers_28_16"></a>Numbers 28:16

And in the fourteenth [yowm](../../strongs/h/h3117.md) of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md) is the [pecach](../../strongs/h/h6453.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_28_17"></a>Numbers 28:17

And in the fifteenth [yowm](../../strongs/h/h3117.md) of this [ḥōḏeš](../../strongs/h/h2320.md) is the [ḥāḡ](../../strongs/h/h2282.md): seven [yowm](../../strongs/h/h3117.md) shall [maṣṣâ](../../strongs/h/h4682.md) be ['akal](../../strongs/h/h398.md).

<a name="numbers_28_18"></a>Numbers 28:18

In the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) shall be a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md); ye shall ['asah](../../strongs/h/h6213.md) no manner of [ʿăḇōḏâ](../../strongs/h/h5656.md) [mĕla'kah](../../strongs/h/h4399.md) therein:

<a name="numbers_28_19"></a>Numbers 28:19

But ye shall [qāraḇ](../../strongs/h/h7126.md) an ['iššê](../../strongs/h/h801.md) for an [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md); two [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), and one ['ayil](../../strongs/h/h352.md), and seven [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md): they shall be unto you [tamiym](../../strongs/h/h8549.md):

<a name="numbers_28_20"></a>Numbers 28:20

And their [minchah](../../strongs/h/h4503.md) shall be of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md): three [ʿiśśārôn](../../strongs/h/h6241.md)s shall ye ['asah](../../strongs/h/h6213.md) for a [par](../../strongs/h/h6499.md), and two [ʿiśśārôn](../../strongs/h/h6241.md)s for an ['ayil](../../strongs/h/h352.md);

<a name="numbers_28_21"></a>Numbers 28:21

A several [ʿiśśārôn](../../strongs/h/h6241.md) shalt thou ['asah](../../strongs/h/h6213.md) for every [keḇeś](../../strongs/h/h3532.md), throughout the seven [keḇeś](../../strongs/h/h3532.md):

<a name="numbers_28_22"></a>Numbers 28:22

And one [śāʿîr](../../strongs/h/h8163.md) for a [chatta'ath](../../strongs/h/h2403.md), to make a [kāp̄ar](../../strongs/h/h3722.md) for you.

<a name="numbers_28_23"></a>Numbers 28:23

Ye shall ['asah](../../strongs/h/h6213.md) these beside the an [ʿōlâ](../../strongs/h/h5930.md) in the [boqer](../../strongs/h/h1242.md), which is for a [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md).

<a name="numbers_28_24"></a>Numbers 28:24

After this manner ye shall ['asah](../../strongs/h/h6213.md) [yowm](../../strongs/h/h3117.md), throughout the seven [yowm](../../strongs/h/h3117.md), the [lechem](../../strongs/h/h3899.md) of the ['iššê](../../strongs/h/h801.md), of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md): it shall be ['asah](../../strongs/h/h6213.md) beside the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), and his [necek](../../strongs/h/h5262.md).

<a name="numbers_28_25"></a>Numbers 28:25

And on the seventh [yowm](../../strongs/h/h3117.md) ye shall have a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md); ye shall ['asah](../../strongs/h/h6213.md) no [ʿăḇōḏâ](../../strongs/h/h5656.md) [mĕla'kah](../../strongs/h/h4399.md).

<a name="numbers_28_26"></a>Numbers 28:26

Also in the [yowm](../../strongs/h/h3117.md) of the [bikûr](../../strongs/h/h1061.md), when ye [qāraḇ](../../strongs/h/h7126.md) a [ḥāḏāš](../../strongs/h/h2319.md) [minchah](../../strongs/h/h4503.md) unto [Yĕhovah](../../strongs/h/h3068.md), after your [šāḇûaʿ](../../strongs/h/h7620.md) be out, ye shall have a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md); ye shall ['asah](../../strongs/h/h6213.md) no [ʿăḇōḏâ](../../strongs/h/h5656.md) [mĕla'kah](../../strongs/h/h4399.md):

<a name="numbers_28_27"></a>Numbers 28:27

But ye shall [qāraḇ](../../strongs/h/h7126.md) the an [ʿōlâ](../../strongs/h/h5930.md) for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md); two [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), seven [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md);

<a name="numbers_28_28"></a>Numbers 28:28

And their [minchah](../../strongs/h/h4503.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), three [ʿiśśārôn](../../strongs/h/h6241.md)s unto one [par](../../strongs/h/h6499.md), two [ʿiśśārôn](../../strongs/h/h6241.md)s unto one ['ayil](../../strongs/h/h352.md),

<a name="numbers_28_29"></a>Numbers 28:29

A several [ʿiśśārôn](../../strongs/h/h6241.md) unto one [keḇeś](../../strongs/h/h3532.md), throughout the seven [keḇeś](../../strongs/h/h3532.md);

<a name="numbers_28_30"></a>Numbers 28:30

And one [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md), to make a [kāp̄ar](../../strongs/h/h3722.md) for you.

<a name="numbers_28_31"></a>Numbers 28:31

Ye shall ['asah](../../strongs/h/h6213.md) them beside the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), and his [minchah](../../strongs/h/h4503.md), (they shall be unto you [tamiym](../../strongs/h/h8549.md)) and their [necek](../../strongs/h/h5262.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 27](numbers_27.md) - [Numbers 29](numbers_29.md)