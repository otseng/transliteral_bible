# [Numbers 21](https://www.blueletterbible.org/kjv/num/21)

<a name="numbers_21_1"></a>Numbers 21:1

And when [melek](../../strongs/h/h4428.md) [ʿĂrāḏ](../../strongs/h/h6166.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md), which [yashab](../../strongs/h/h3427.md) in the [neḡeḇ](../../strongs/h/h5045.md), [shama'](../../strongs/h/h8085.md) tell that [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) by the [derek](../../strongs/h/h1870.md) of the ['ăṯārîm](../../strongs/h/h871.md); then he [lāḥam](../../strongs/h/h3898.md) against [Yisra'el](../../strongs/h/h3478.md), and [šāḇâ](../../strongs/h/h7617.md) some of them [šᵊḇî](../../strongs/h/h7628.md).

<a name="numbers_21_2"></a>Numbers 21:2

And [Yisra'el](../../strongs/h/h3478.md) [nāḏar](../../strongs/h/h5087.md) a [neḏer](../../strongs/h/h5088.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), If thou wilt [nathan](../../strongs/h/h5414.md) [nathan](../../strongs/h/h5414.md) this ['am](../../strongs/h/h5971.md) into my [yad](../../strongs/h/h3027.md), then I will [ḥāram](../../strongs/h/h2763.md) their [ʿîr](../../strongs/h/h5892.md).

<a name="numbers_21_3"></a>Numbers 21:3

And [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) to the [qowl](../../strongs/h/h6963.md) of [Yisra'el](../../strongs/h/h3478.md), and [nathan](../../strongs/h/h5414.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md); and they [ḥāram](../../strongs/h/h2763.md) them and their [ʿîr](../../strongs/h/h5892.md): and he [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of the [maqowm](../../strongs/h/h4725.md) [Ḥārmâ](../../strongs/h/h2767.md).

<a name="numbers_21_4"></a>Numbers 21:4

And they [nāsaʿ](../../strongs/h/h5265.md) from [har](../../strongs/h/h2022.md) [Hōr](../../strongs/h/h2023.md) by the [derek](../../strongs/h/h1870.md) of the [Sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md), to [cabab](../../strongs/h/h5437.md) the ['erets](../../strongs/h/h776.md) of ['Ĕḏōm](../../strongs/h/h123.md): and the [nephesh](../../strongs/h/h5315.md) of the ['am](../../strongs/h/h5971.md) was much [qāṣar](../../strongs/h/h7114.md) because of the [derek](../../strongs/h/h1870.md).

<a name="numbers_21_5"></a>Numbers 21:5

And the ['am](../../strongs/h/h5971.md) [dabar](../../strongs/h/h1696.md) against ['Elohiym](../../strongs/h/h430.md), and against [Mōshe](../../strongs/h/h4872.md), Wherefore have ye [ʿālâ](../../strongs/h/h5927.md) us out of [Mitsrayim](../../strongs/h/h4714.md) to [muwth](../../strongs/h/h4191.md) in the [midbar](../../strongs/h/h4057.md)? for there is no [lechem](../../strongs/h/h3899.md), neither is there any [mayim](../../strongs/h/h4325.md); and our [nephesh](../../strongs/h/h5315.md) [qûṣ](../../strongs/h/h6973.md) this [qᵊlōqēl](../../strongs/h/h7052.md) [lechem](../../strongs/h/h3899.md).

<a name="numbers_21_6"></a>Numbers 21:6

And [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) [saraph](../../strongs/h/h8314.md) [nachash](../../strongs/h/h5175.md) among the ['am](../../strongs/h/h5971.md), and they [nāšaḵ](../../strongs/h/h5391.md) the ['am](../../strongs/h/h5971.md); and [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md) [muwth](../../strongs/h/h4191.md).

<a name="numbers_21_7"></a>Numbers 21:7

Therefore the ['am](../../strongs/h/h5971.md) [bow'](../../strongs/h/h935.md) to [Mōshe](../../strongs/h/h4872.md), and ['āmar](../../strongs/h/h559.md), We have [chata'](../../strongs/h/h2398.md), for we have [dabar](../../strongs/h/h1696.md) against [Yĕhovah](../../strongs/h/h3068.md), and against thee; [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md), that he [cuwr](../../strongs/h/h5493.md) the [nachash](../../strongs/h/h5175.md) from us. And [Mōshe](../../strongs/h/h4872.md) [palal](../../strongs/h/h6419.md) for the ['am](../../strongs/h/h5971.md).

<a name="numbers_21_8"></a>Numbers 21:8

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['asah](../../strongs/h/h6213.md) thee a [saraph](../../strongs/h/h8314.md), and [śûm](../../strongs/h/h7760.md) it upon a [nēs](../../strongs/h/h5251.md): and it shall come to pass, that every one that is [nāšaḵ](../../strongs/h/h5391.md), when he [ra'ah](../../strongs/h/h7200.md) upon it, shall [chayay](../../strongs/h/h2425.md).

<a name="numbers_21_9"></a>Numbers 21:9

And [Mōshe](../../strongs/h/h4872.md) ['asah](../../strongs/h/h6213.md) a [nachash](../../strongs/h/h5175.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md), and [śûm](../../strongs/h/h7760.md) it upon a [nēs](../../strongs/h/h5251.md), and it came to pass, that if a [nachash](../../strongs/h/h5175.md) had [nāšaḵ](../../strongs/h/h5391.md) any ['iysh](../../strongs/h/h376.md), when he [nabat](../../strongs/h/h5027.md) the [nachash](../../strongs/h/h5175.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md), he [chayay](../../strongs/h/h2425.md).

<a name="numbers_21_10"></a>Numbers 21:10

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāsaʿ](../../strongs/h/h5265.md), and [ḥānâ](../../strongs/h/h2583.md) in ['Ōḇōṯ](../../strongs/h/h88.md).

<a name="numbers_21_11"></a>Numbers 21:11

And they [nāsaʿ](../../strongs/h/h5265.md) from ['Ōḇōṯ](../../strongs/h/h88.md), and [ḥānâ](../../strongs/h/h2583.md) at [ʿîyê hāʿăḇārîm](../../strongs/h/h5863.md), in the [midbar](../../strongs/h/h4057.md) which is [paniym](../../strongs/h/h6440.md) [Mô'āḇ](../../strongs/h/h4124.md), toward the [mizrach](../../strongs/h/h4217.md) [šemeš](../../strongs/h/h8121.md).

<a name="numbers_21_12"></a>Numbers 21:12

From thence they [nāsaʿ](../../strongs/h/h5265.md), and [ḥānâ](../../strongs/h/h2583.md) in the [nachal](../../strongs/h/h5158.md) of [zereḏ](../../strongs/h/h2218.md).

<a name="numbers_21_13"></a>Numbers 21:13

From thence they [nāsaʿ](../../strongs/h/h5265.md), and [ḥānâ](../../strongs/h/h2583.md) on the other [ʿēḇer](../../strongs/h/h5676.md) of ['Arnôn](../../strongs/h/h769.md), which is in the [midbar](../../strongs/h/h4057.md) that [yāṣā'](../../strongs/h/h3318.md) of the [gᵊḇûl](../../strongs/h/h1366.md) of the ['Ĕmōrî](../../strongs/h/h567.md): for ['Arnôn](../../strongs/h/h769.md) is the [gᵊḇûl](../../strongs/h/h1366.md) of [Mô'āḇ](../../strongs/h/h4124.md), between [Mô'āḇ](../../strongs/h/h4124.md) and the ['Ĕmōrî](../../strongs/h/h567.md).

<a name="numbers_21_14"></a>Numbers 21:14

Wherefore it is ['āmar](../../strongs/h/h559.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [milḥāmâ](../../strongs/h/h4421.md) of [Yĕhovah](../../strongs/h/h3068.md), What he [vāhēḇ](../../strongs/h/h2052.md) in the [sûp̄â](../../strongs/h/h5492.md), and in the [nachal](../../strongs/h/h5158.md) of ['Arnôn](../../strongs/h/h769.md),

<a name="numbers_21_15"></a>Numbers 21:15

And at the ['ešeḏ](../../strongs/h/h793.md) of the [nachal](../../strongs/h/h5158.md) that [natah](../../strongs/h/h5186.md) to the [yashab](../../strongs/h/h3427.md) of [ʿĀr](../../strongs/h/h6144.md), and [šāʿan](../../strongs/h/h8172.md) upon the [gᵊḇûl](../../strongs/h/h1366.md) of [Mô'āḇ](../../strongs/h/h4124.md).

<a name="numbers_21_16"></a>Numbers 21:16

And from thence they went to [bᵊ'ēr](../../strongs/h/h876.md): that is the [bᵊ'ēr](../../strongs/h/h875.md) whereof [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āsap̄](../../strongs/h/h622.md) the ['am](../../strongs/h/h5971.md) ['āsap̄](../../strongs/h/h622.md), and I will [nathan](../../strongs/h/h5414.md) them [mayim](../../strongs/h/h4325.md).

<a name="numbers_21_17"></a>Numbers 21:17

Then [Yisra'el](../../strongs/h/h3478.md) [shiyr](../../strongs/h/h7891.md) this [šîr](../../strongs/h/h7892.md), [ʿālâ](../../strongs/h/h5927.md), O [bᵊ'ēr](../../strongs/h/h875.md); ['anah](../../strongs/h/h6030.md) ye unto it:

<a name="numbers_21_18"></a>Numbers 21:18

The [śar](../../strongs/h/h8269.md) [chaphar](../../strongs/h/h2658.md) the [bᵊ'ēr](../../strongs/h/h875.md), the [nāḏîḇ](../../strongs/h/h5081.md) of the ['am](../../strongs/h/h5971.md) [karah](../../strongs/h/h3738.md) it, by the direction of the [ḥāqaq](../../strongs/h/h2710.md), with their [mašʿēnâ](../../strongs/h/h4938.md). And from the [midbar](../../strongs/h/h4057.md) they went to [matānâ](../../strongs/h/h4980.md):

<a name="numbers_21_19"></a>Numbers 21:19

And from [matānâ](../../strongs/h/h4980.md) to [naḥălî'ēl](../../strongs/h/h5160.md): and from [naḥălî'ēl](../../strongs/h/h5160.md) to [bāmôṯ](../../strongs/h/h1120.md):

<a name="numbers_21_20"></a>Numbers 21:20

And from [bāmôṯ](../../strongs/h/h1120.md) in the [gay'](../../strongs/h/h1516.md), that is in the [sadeh](../../strongs/h/h7704.md) of [Mô'āḇ](../../strongs/h/h4124.md), to the [ro'sh](../../strongs/h/h7218.md) of [Pisgâ](../../strongs/h/h6449.md), which [šāqap̄](../../strongs/h/h8259.md) [paniym](../../strongs/h/h6440.md) [yᵊšîmôn](../../strongs/h/h3452.md).

<a name="numbers_21_21"></a>Numbers 21:21

And [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) unto [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_21_22"></a>Numbers 21:22

Let me ['abar](../../strongs/h/h5674.md) through thy ['erets](../../strongs/h/h776.md): we will not [natah](../../strongs/h/h5186.md) into the [sadeh](../../strongs/h/h7704.md), or into the [kerem](../../strongs/h/h3754.md); we will not [šāṯâ](../../strongs/h/h8354.md) of the [mayim](../../strongs/h/h4325.md) of the [bᵊ'ēr](../../strongs/h/h875.md): but we will [yālaḵ](../../strongs/h/h3212.md) by the [melek](../../strongs/h/h4428.md) [derek](../../strongs/h/h1870.md), until we be ['abar](../../strongs/h/h5674.md) thy [gᵊḇûl](../../strongs/h/h1366.md).

<a name="numbers_21_23"></a>Numbers 21:23

And [Sîḥôn](../../strongs/h/h5511.md) would not [nathan](../../strongs/h/h5414.md) [Yisra'el](../../strongs/h/h3478.md) to ['abar](../../strongs/h/h5674.md) his [gᵊḇûl](../../strongs/h/h1366.md): but [Sîḥôn](../../strongs/h/h5511.md) ['āsap̄](../../strongs/h/h622.md) all his ['am](../../strongs/h/h5971.md), and [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) [Yisra'el](../../strongs/h/h3478.md) into the [midbar](../../strongs/h/h4057.md): and he [bow'](../../strongs/h/h935.md) to [Yahaṣ](../../strongs/h/h3096.md), and [lāḥam](../../strongs/h/h3898.md) against [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_21_24"></a>Numbers 21:24

And [Yisra'el](../../strongs/h/h3478.md) [nakah](../../strongs/h/h5221.md) him with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), and [yarash](../../strongs/h/h3423.md) his ['erets](../../strongs/h/h776.md) from ['Arnôn](../../strongs/h/h769.md) unto [Yabōq](../../strongs/h/h2999.md), even unto the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md): for the [gᵊḇûl](../../strongs/h/h1366.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) was ['az](../../strongs/h/h5794.md).

<a name="numbers_21_25"></a>Numbers 21:25

And [Yisra'el](../../strongs/h/h3478.md) [laqach](../../strongs/h/h3947.md) all these [ʿîr](../../strongs/h/h5892.md): and [Yisra'el](../../strongs/h/h3478.md) [yashab](../../strongs/h/h3427.md) in all the [ʿîr](../../strongs/h/h5892.md) of the ['Ĕmōrî](../../strongs/h/h567.md), in [Hešbôn](../../strongs/h/h2809.md), and in all the [bath](../../strongs/h/h1323.md) thereof.

<a name="numbers_21_26"></a>Numbers 21:26

For [Hešbôn](../../strongs/h/h2809.md) was the [ʿîr](../../strongs/h/h5892.md) of [Sîḥôn](../../strongs/h/h5511.md) the [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), who had [lāḥam](../../strongs/h/h3898.md) against the [ri'šôn](../../strongs/h/h7223.md) [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md), and [laqach](../../strongs/h/h3947.md) all his ['erets](../../strongs/h/h776.md) out of his [yad](../../strongs/h/h3027.md), even unto ['Arnôn](../../strongs/h/h769.md).

<a name="numbers_21_27"></a>Numbers 21:27

Wherefore they that [māšal](../../strongs/h/h4911.md) ['āmar](../../strongs/h/h559.md), [bow'](../../strongs/h/h935.md) into [Hešbôn](../../strongs/h/h2809.md), let the [ʿîr](../../strongs/h/h5892.md) of [Sîḥôn](../../strongs/h/h5511.md) be [bānâ](../../strongs/h/h1129.md) and [kuwn](../../strongs/h/h3559.md):

<a name="numbers_21_28"></a>Numbers 21:28

For there is an ['esh](../../strongs/h/h784.md) [yāṣā'](../../strongs/h/h3318.md) of [Hešbôn](../../strongs/h/h2809.md), a [lehāḇâ](../../strongs/h/h3852.md) from the [qiryâ](../../strongs/h/h7151.md) of [Sîḥôn](../../strongs/h/h5511.md): it hath ['akal](../../strongs/h/h398.md) [ʿĀr](../../strongs/h/h6144.md) of [Mô'āḇ](../../strongs/h/h4124.md), and the [ḇʿly ḇmvṯ](../../strongs/h/h1181.md) of ['Arnôn](../../strongs/h/h769.md).

<a name="numbers_21_29"></a>Numbers 21:29

['owy](../../strongs/h/h188.md) to thee, [Mô'āḇ](../../strongs/h/h4124.md)! thou art ['abad](../../strongs/h/h6.md), O ['am](../../strongs/h/h5971.md) of [kᵊmôš](../../strongs/h/h3645.md): he hath [nathan](../../strongs/h/h5414.md) his [ben](../../strongs/h/h1121.md) that [pālîṭ](../../strongs/h/h6412.md), and his [bath](../../strongs/h/h1323.md), into [shebuwth](../../strongs/h/h7622.md) unto [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md).

<a name="numbers_21_30"></a>Numbers 21:30

We have [yārâ](../../strongs/h/h3384.md) at them; [Hešbôn](../../strongs/h/h2809.md) is ['abad](../../strongs/h/h6.md) even unto [Dîḇōvn](../../strongs/h/h1769.md), and we have [šāmēm](../../strongs/h/h8074.md) them even unto [Nōp̄aḥ](../../strongs/h/h5302.md), which reacheth unto [Mêḏḇā'](../../strongs/h/h4311.md).

<a name="numbers_21_31"></a>Numbers 21:31

Thus [Yisra'el](../../strongs/h/h3478.md) [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of the ['Ĕmōrî](../../strongs/h/h567.md).

<a name="numbers_21_32"></a>Numbers 21:32

And [Mōshe](../../strongs/h/h4872.md) [shalach](../../strongs/h/h7971.md) to [ragal](../../strongs/h/h7270.md) [Yaʿzêr](../../strongs/h/h3270.md), and they [lāḵaḏ](../../strongs/h/h3920.md) the [bath](../../strongs/h/h1323.md) thereof, and [yarash](../../strongs/h/h3423.md) [yarash](../../strongs/h/h3423.md) the ['Ĕmōrî](../../strongs/h/h567.md) that were there.

<a name="numbers_21_33"></a>Numbers 21:33

And they [panah](../../strongs/h/h6437.md) and [ʿālâ](../../strongs/h/h5927.md) by the [derek](../../strongs/h/h1870.md) of [Bāšān](../../strongs/h/h1316.md): and [ʿÔḡ](../../strongs/h/h5747.md) the [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md) [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) them, he, and all his ['am](../../strongs/h/h5971.md), to the [milḥāmâ](../../strongs/h/h4421.md) at ['Eḏrᵊʿî](../../strongs/h/h154.md).

<a name="numbers_21_34"></a>Numbers 21:34

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [yare'](../../strongs/h/h3372.md) him not: for I have [nathan](../../strongs/h/h5414.md) him into thy [yad](../../strongs/h/h3027.md), and all his ['am](../../strongs/h/h5971.md), and his ['erets](../../strongs/h/h776.md); and thou shalt ['asah](../../strongs/h/h6213.md) to him as thou ['asah](../../strongs/h/h6213.md) unto [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), which [yashab](../../strongs/h/h3427.md) at [Hešbôn](../../strongs/h/h2809.md).

<a name="numbers_21_35"></a>Numbers 21:35

So they [nakah](../../strongs/h/h5221.md) him, and his [ben](../../strongs/h/h1121.md), and all his ['am](../../strongs/h/h5971.md), until there was [biltî](../../strongs/h/h1115.md) [šā'ar](../../strongs/h/h7604.md) [śārîḏ](../../strongs/h/h8300.md): and they [yarash](../../strongs/h/h3423.md) his ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 20](numbers_20.md) - [Numbers 22](numbers_22.md)