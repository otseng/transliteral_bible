# [Numbers 33](https://www.blueletterbible.org/kjv/num/33)

<a name="numbers_33_1"></a>Numbers 33:1

These are the [massāʿ](../../strongs/h/h4550.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) with their [tsaba'](../../strongs/h/h6635.md) under the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md).

<a name="numbers_33_2"></a>Numbers 33:2

And [Mōshe](../../strongs/h/h4872.md) [kāṯaḇ](../../strongs/h/h3789.md) their [môṣā'](../../strongs/h/h4161.md) according to their [massāʿ](../../strongs/h/h4550.md) by the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md): and these are their [massāʿ](../../strongs/h/h4550.md) according to their [môṣā'](../../strongs/h/h4161.md).

<a name="numbers_33_3"></a>Numbers 33:3

And they [nāsaʿ](../../strongs/h/h5265.md) from [Raʿmᵊsēs](../../strongs/h/h7486.md) in the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md), on the fifteenth [yowm](../../strongs/h/h3117.md) of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md); on the [māḥŏrāṯ](../../strongs/h/h4283.md) after the [pecach](../../strongs/h/h6453.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yāṣā'](../../strongs/h/h3318.md) with a [ruwm](../../strongs/h/h7311.md) [yad](../../strongs/h/h3027.md) in the ['ayin](../../strongs/h/h5869.md) of all the [Mitsrayim](../../strongs/h/h4714.md).

<a name="numbers_33_4"></a>Numbers 33:4

For the [Mitsrayim](../../strongs/h/h4714.md) [qāḇar](../../strongs/h/h6912.md) all their [bᵊḵôr](../../strongs/h/h1060.md), which [Yĕhovah](../../strongs/h/h3068.md) had [nakah](../../strongs/h/h5221.md) among them: upon their ['Elohiym](../../strongs/h/h430.md) also [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) [šep̄eṭ](../../strongs/h/h8201.md).

<a name="numbers_33_5"></a>Numbers 33:5

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāsaʿ](../../strongs/h/h5265.md) from [Raʿmᵊsēs](../../strongs/h/h7486.md), and [ḥānâ](../../strongs/h/h2583.md) in [Sukôṯ](../../strongs/h/h5523.md).

<a name="numbers_33_6"></a>Numbers 33:6

And they [nāsaʿ](../../strongs/h/h5265.md) from [Sukôṯ](../../strongs/h/h5523.md), and [ḥānâ](../../strongs/h/h2583.md) in ['Ēṯām](../../strongs/h/h864.md), which is in the [qāṣê](../../strongs/h/h7097.md) of the [midbar](../../strongs/h/h4057.md).

<a name="numbers_33_7"></a>Numbers 33:7

And they [nāsaʿ](../../strongs/h/h5265.md) from ['Ēṯām](../../strongs/h/h864.md), and [shuwb](../../strongs/h/h7725.md) unto [Pî haḥirōṯ](../../strongs/h/h6367.md), which is before [Baʿal ṣᵊp̄ôn](../../strongs/h/h1189.md): and they [ḥānâ](../../strongs/h/h2583.md) [paniym](../../strongs/h/h6440.md) [Miḡdôl](../../strongs/h/h4024.md).

<a name="numbers_33_8"></a>Numbers 33:8

And they [nāsaʿ](../../strongs/h/h5265.md) from [paniym](../../strongs/h/h6440.md) [Pî haḥirōṯ](../../strongs/h/h6367.md), and ['abar](../../strongs/h/h5674.md) the [tavek](../../strongs/h/h8432.md) of the [yam](../../strongs/h/h3220.md) into the [midbar](../../strongs/h/h4057.md), and [yālaḵ](../../strongs/h/h3212.md) three [yowm](../../strongs/h/h3117.md) [derek](../../strongs/h/h1870.md) in the [midbar](../../strongs/h/h4057.md) of ['Ēṯām](../../strongs/h/h864.md), and [ḥānâ](../../strongs/h/h2583.md) in [Mārâ](../../strongs/h/h4785.md).

<a name="numbers_33_9"></a>Numbers 33:9

And they [nāsaʿ](../../strongs/h/h5265.md) from [Mārâ](../../strongs/h/h4785.md), and [bow'](../../strongs/h/h935.md) unto ['Êlim](../../strongs/h/h362.md): and in ['Êlim](../../strongs/h/h362.md) were twelve ['ayin](../../strongs/h/h5869.md) of [mayim](../../strongs/h/h4325.md), and threescore and ten [tāmār](../../strongs/h/h8558.md); and they [ḥānâ](../../strongs/h/h2583.md) there.

<a name="numbers_33_10"></a>Numbers 33:10

And they [nāsaʿ](../../strongs/h/h5265.md) from ['Êlim](../../strongs/h/h362.md), and [ḥānâ](../../strongs/h/h2583.md) by the [Sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md).

<a name="numbers_33_11"></a>Numbers 33:11

And they [nāsaʿ](../../strongs/h/h5265.md) from the [Sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md), and [ḥānâ](../../strongs/h/h2583.md) in the [midbar](../../strongs/h/h4057.md) of [Sîn](../../strongs/h/h5512.md).

<a name="numbers_33_12"></a>Numbers 33:12

And they took their [nāsaʿ](../../strongs/h/h5265.md) out of the [midbar](../../strongs/h/h4057.md) of [Sîn](../../strongs/h/h5512.md), and [ḥānâ](../../strongs/h/h2583.md) in [Dāp̄Qâ](../../strongs/h/h1850.md).

<a name="numbers_33_13"></a>Numbers 33:13

And they [nāsaʿ](../../strongs/h/h5265.md) from [Dāp̄Qâ](../../strongs/h/h1850.md), and [ḥānâ](../../strongs/h/h2583.md) in ['Ālûš](../../strongs/h/h442.md).

<a name="numbers_33_14"></a>Numbers 33:14

And they [nāsaʿ](../../strongs/h/h5265.md) from ['Ālûš](../../strongs/h/h442.md), and [ḥānâ](../../strongs/h/h2583.md) at [Rᵊp̄îḏîm](../../strongs/h/h7508.md), where was no [mayim](../../strongs/h/h4325.md) for the ['am](../../strongs/h/h5971.md) to [šāṯâ](../../strongs/h/h8354.md).

<a name="numbers_33_15"></a>Numbers 33:15

And they [nāsaʿ](../../strongs/h/h5265.md) from [Rᵊp̄îḏîm](../../strongs/h/h7508.md), and [ḥānâ](../../strongs/h/h2583.md) in the [midbar](../../strongs/h/h4057.md) of [Sînay](../../strongs/h/h5514.md).

<a name="numbers_33_16"></a>Numbers 33:16

And they [nāsaʿ](../../strongs/h/h5265.md) from the [midbar](../../strongs/h/h4057.md) of [Sînay](../../strongs/h/h5514.md), and [ḥānâ](../../strongs/h/h2583.md) at [Qiḇrôṯ hata'ăvâ](../../strongs/h/h6914.md).

<a name="numbers_33_17"></a>Numbers 33:17

And they [nāsaʿ](../../strongs/h/h5265.md) from [Qiḇrôṯ hata'ăvâ](../../strongs/h/h6914.md), and [ḥānâ](../../strongs/h/h2583.md) at [Ḥăṣērôṯ](../../strongs/h/h2698.md).

<a name="numbers_33_18"></a>Numbers 33:18

And they [nāsaʿ](../../strongs/h/h5265.md) from [Ḥăṣērôṯ](../../strongs/h/h2698.md), and [ḥānâ](../../strongs/h/h2583.md) in [Riṯmâ](../../strongs/h/h7575.md).

<a name="numbers_33_19"></a>Numbers 33:19

And they [nāsaʿ](../../strongs/h/h5265.md) from [Riṯmâ](../../strongs/h/h7575.md), and [ḥānâ](../../strongs/h/h2583.md) at [Rimmôn Pereṣ](../../strongs/h/h7428.md).

<a name="numbers_33_20"></a>Numbers 33:20

And they [nāsaʿ](../../strongs/h/h5265.md) from [Rimmôn Pereṣ](../../strongs/h/h7428.md), and [ḥānâ](../../strongs/h/h2583.md) in [Liḇnâ](../../strongs/h/h3841.md).

<a name="numbers_33_21"></a>Numbers 33:21

And they [nāsaʿ](../../strongs/h/h5265.md) from [Liḇnâ](../../strongs/h/h3841.md), and [ḥānâ](../../strongs/h/h2583.md) at [Rissâ](../../strongs/h/h7446.md).

<a name="numbers_33_22"></a>Numbers 33:22

And they [nāsaʿ](../../strongs/h/h5265.md) from [Rissâ](../../strongs/h/h7446.md), and [ḥānâ](../../strongs/h/h2583.md) in [Qᵊhēlāṯâ](../../strongs/h/h6954.md).

<a name="numbers_33_23"></a>Numbers 33:23

And they [nāsaʿ](../../strongs/h/h5265.md) from [Qᵊhēlāṯâ](../../strongs/h/h6954.md), and [ḥānâ](../../strongs/h/h2583.md) in [har](../../strongs/h/h2022.md) [Šep̄er](../../strongs/h/h8234.md).

<a name="numbers_33_24"></a>Numbers 33:24

And they [nāsaʿ](../../strongs/h/h5265.md) from [har](../../strongs/h/h2022.md) [Šep̄er](../../strongs/h/h8234.md), and [ḥānâ](../../strongs/h/h2583.md) in [Ḥărāḏâ](../../strongs/h/h2732.md).

<a name="numbers_33_25"></a>Numbers 33:25

And they [nāsaʿ](../../strongs/h/h5265.md) from [Ḥărāḏâ](../../strongs/h/h2732.md), and [ḥānâ](../../strongs/h/h2583.md) in [Maqhēlôṯ](../../strongs/h/h4722.md).

<a name="numbers_33_26"></a>Numbers 33:26

And they [nāsaʿ](../../strongs/h/h5265.md) from [Maqhēlôṯ](../../strongs/h/h4722.md), and [ḥānâ](../../strongs/h/h2583.md) at [Taḥaṯ](../../strongs/h/h8480.md).

<a name="numbers_33_27"></a>Numbers 33:27

And they [nāsaʿ](../../strongs/h/h5265.md) from [Taḥaṯ](../../strongs/h/h8480.md), and [ḥānâ](../../strongs/h/h2583.md) at [Teraḥ](../../strongs/h/h8646.md).

<a name="numbers_33_28"></a>Numbers 33:28

And they [nāsaʿ](../../strongs/h/h5265.md) from [Teraḥ](../../strongs/h/h8646.md), and [ḥānâ](../../strongs/h/h2583.md) in [Miṯqâ](../../strongs/h/h4989.md).

<a name="numbers_33_29"></a>Numbers 33:29

And they [nāsaʿ](../../strongs/h/h5265.md) from [Miṯqâ](../../strongs/h/h4989.md), and [ḥānâ](../../strongs/h/h2583.md) in [Ḥašmōnâ](../../strongs/h/h2832.md).

<a name="numbers_33_30"></a>Numbers 33:30

And they [nāsaʿ](../../strongs/h/h5265.md) from [Ḥašmōnâ](../../strongs/h/h2832.md), and [ḥānâ](../../strongs/h/h2583.md) at [Môsērâ](../../strongs/h/h4149.md).

<a name="numbers_33_31"></a>Numbers 33:31

And they [nāsaʿ](../../strongs/h/h5265.md) from [Môsērâ](../../strongs/h/h4149.md), and [ḥānâ](../../strongs/h/h2583.md) in [Bᵊnê YaʿĂqān](../../strongs/h/h1142.md).

<a name="numbers_33_32"></a>Numbers 33:32

And they [nāsaʿ](../../strongs/h/h5265.md) from [Bᵊnê YaʿĂqān](../../strongs/h/h1142.md), and [ḥānâ](../../strongs/h/h2583.md) at [Ḥōr Hagiḏgāḏ](../../strongs/h/h2735.md).

<a name="numbers_33_33"></a>Numbers 33:33

And they [nāsaʿ](../../strongs/h/h5265.md) from [Ḥōr Hagiḏgāḏ](../../strongs/h/h2735.md), and [ḥānâ](../../strongs/h/h2583.md) in [Yāṭḇāṯâ](../../strongs/h/h3193.md).

<a name="numbers_33_34"></a>Numbers 33:34

And they [nāsaʿ](../../strongs/h/h5265.md) from [Yāṭḇāṯâ](../../strongs/h/h3193.md), and [ḥānâ](../../strongs/h/h2583.md) at [ʿAḇrōnâ](../../strongs/h/h5684.md).

<a name="numbers_33_35"></a>Numbers 33:35

And they [nāsaʿ](../../strongs/h/h5265.md) from [ʿAḇrōnâ](../../strongs/h/h5684.md), and [ḥānâ](../../strongs/h/h2583.md) at [ʿEṣyôn Geḇer](../../strongs/h/h6100.md).

<a name="numbers_33_36"></a>Numbers 33:36

And they [nāsaʿ](../../strongs/h/h5265.md) from [ʿEṣyôn Geḇer](../../strongs/h/h6100.md), and [ḥānâ](../../strongs/h/h2583.md) in the [midbar](../../strongs/h/h4057.md) of [Ṣin](../../strongs/h/h6790.md), which is [Qāḏēš](../../strongs/h/h6946.md).

<a name="numbers_33_37"></a>Numbers 33:37

And they [nāsaʿ](../../strongs/h/h5265.md) from [Qāḏēš](../../strongs/h/h6946.md), and [ḥānâ](../../strongs/h/h2583.md) in [har](../../strongs/h/h2022.md) [Hōr](../../strongs/h/h2023.md), in the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md) of ['Ĕḏōm](../../strongs/h/h123.md).

<a name="numbers_33_38"></a>Numbers 33:38

And ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md) [ʿālâ](../../strongs/h/h5927.md) into [har](../../strongs/h/h2022.md) [Hōr](../../strongs/h/h2023.md) at the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md), and [muwth](../../strongs/h/h4191.md) there, in the fortieth [šānâ](../../strongs/h/h8141.md) after the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were [yāṣā'](../../strongs/h/h3318.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), in the first of the fifth [ḥōḏeš](../../strongs/h/h2320.md).

<a name="numbers_33_39"></a>Numbers 33:39

And ['Ahărôn](../../strongs/h/h175.md) was an hundred and twenty and three [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he [maveth](../../strongs/h/h4194.md) in [har](../../strongs/h/h2022.md) [Hōr](../../strongs/h/h2023.md).

<a name="numbers_33_40"></a>Numbers 33:40

And [melek](../../strongs/h/h4428.md) [ʿĂrāḏ](../../strongs/h/h6166.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md), which [yashab](../../strongs/h/h3427.md) in the [neḡeḇ](../../strongs/h/h5045.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), [shama'](../../strongs/h/h8085.md) of the [bow'](../../strongs/h/h935.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_33_41"></a>Numbers 33:41

And they [nāsaʿ](../../strongs/h/h5265.md) from [har](../../strongs/h/h2022.md) [Hōr](../../strongs/h/h2023.md), and [ḥānâ](../../strongs/h/h2583.md) in [Ṣalmōnâ](../../strongs/h/h6758.md).

<a name="numbers_33_42"></a>Numbers 33:42

And they [nāsaʿ](../../strongs/h/h5265.md) from [Ṣalmōnâ](../../strongs/h/h6758.md), and [ḥānâ](../../strongs/h/h2583.md) in [Pûnōn](../../strongs/h/h6325.md).

<a name="numbers_33_43"></a>Numbers 33:43

And they [nāsaʿ](../../strongs/h/h5265.md) from [Pûnōn](../../strongs/h/h6325.md), and [ḥānâ](../../strongs/h/h2583.md) in ['Ōḇōṯ](../../strongs/h/h88.md).

<a name="numbers_33_44"></a>Numbers 33:44

And they [nāsaʿ](../../strongs/h/h5265.md) from ['Ōḇōṯ](../../strongs/h/h88.md), and [ḥānâ](../../strongs/h/h2583.md) in [ʿÎyê hāʿăḇārîm](../../strongs/h/h5863.md), in the [gᵊḇûl](../../strongs/h/h1366.md) of [Mô'āḇ](../../strongs/h/h4124.md).

<a name="numbers_33_45"></a>Numbers 33:45

And they [nāsaʿ](../../strongs/h/h5265.md) from [ʿÎyîm](../../strongs/h/h5864.md), and [ḥānâ](../../strongs/h/h2583.md) in [Dîḇōvn](../../strongs/h/h1769.md).

<a name="numbers_33_46"></a>Numbers 33:46

And they [nāsaʿ](../../strongs/h/h5265.md) from [Dîḇōvn](../../strongs/h/h1769.md), and [ḥānâ](../../strongs/h/h2583.md) in [ʿAlmōn-Diḇlāṯaymâ](../../strongs/h/h5963.md).

<a name="numbers_33_47"></a>Numbers 33:47

And they [nāsaʿ](../../strongs/h/h5265.md) from [ʿAlmōn-Diḇlāṯaymâ](../../strongs/h/h5963.md), and [ḥānâ](../../strongs/h/h2583.md) in the [har](../../strongs/h/h2022.md) of [ʿĂḇārîm](../../strongs/h/h5682.md), [paniym](../../strongs/h/h6440.md) [Nᵊḇô](../../strongs/h/h5015.md).

<a name="numbers_33_48"></a>Numbers 33:48

And they [nāsaʿ](../../strongs/h/h5265.md) from the [har](../../strongs/h/h2022.md) of [ʿĂḇārîm](../../strongs/h/h5682.md), and [ḥānâ](../../strongs/h/h2583.md) in the ['arabah](../../strongs/h/h6160.md) of [Mô'āḇ](../../strongs/h/h4124.md) by [Yardēn](../../strongs/h/h3383.md) near [Yᵊrēḥô](../../strongs/h/h3405.md).

<a name="numbers_33_49"></a>Numbers 33:49

And they [ḥānâ](../../strongs/h/h2583.md) by [Yardēn](../../strongs/h/h3383.md), from [Bêṯ Hayšîmôṯ](../../strongs/h/h1020.md) even unto ['Āḇēl Hašiṭṭîm](../../strongs/h/h63.md) in the ['arabah](../../strongs/h/h6160.md) of [Mô'āḇ](../../strongs/h/h4124.md).

<a name="numbers_33_50"></a>Numbers 33:50

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) in the ['arabah](../../strongs/h/h6160.md) of [Mô'āḇ](../../strongs/h/h4124.md) by [Yardēn](../../strongs/h/h3383.md) near [Yᵊrēḥô](../../strongs/h/h3405.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_33_51"></a>Numbers 33:51

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, When ye are ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) into the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md);

<a name="numbers_33_52"></a>Numbers 33:52

Then ye shall [yarash](../../strongs/h/h3423.md) all the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) from [paniym](../../strongs/h/h6440.md) you, and ['abad](../../strongs/h/h6.md) all their [maśkîṯ](../../strongs/h/h4906.md), and ['abad](../../strongs/h/h6.md) all their [massēḵâ](../../strongs/h/h4541.md) [tselem](../../strongs/h/h6754.md), and [šāmaḏ](../../strongs/h/h8045.md) all their [bāmâ](../../strongs/h/h1116.md):

<a name="numbers_33_53"></a>Numbers 33:53

And ye shall [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md), and [yashab](../../strongs/h/h3427.md) therein: for I have [nathan](../../strongs/h/h5414.md) you the ['erets](../../strongs/h/h776.md) to [yarash](../../strongs/h/h3423.md) it.

<a name="numbers_33_54"></a>Numbers 33:54

And ye shall [nāḥal](../../strongs/h/h5157.md) the ['erets](../../strongs/h/h776.md) by [gôrāl](../../strongs/h/h1486.md) for a [nāḥal](../../strongs/h/h5157.md) among your [mišpāḥâ](../../strongs/h/h4940.md): and to the [rab](../../strongs/h/h7227.md) ye shall [rabah](../../strongs/h/h7235.md) [nachalah](../../strongs/h/h5159.md), and to the [mᵊʿaṭ](../../strongs/h/h4592.md) ye shall [māʿaṭ](../../strongs/h/h4591.md) [nachalah](../../strongs/h/h5159.md): every man's shall be in the place where his [gôrāl](../../strongs/h/h1486.md) [yāṣā'](../../strongs/h/h3318.md); according to the [maṭṭê](../../strongs/h/h4294.md) of your ['ab](../../strongs/h/h1.md) ye shall [nāḥal](../../strongs/h/h5157.md).

<a name="numbers_33_55"></a>Numbers 33:55

But if ye will not [yarash](../../strongs/h/h3423.md) the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) from [paniym](../../strongs/h/h6440.md) you; then it shall come to pass, that those which ye let [yāṯar](../../strongs/h/h3498.md) of them shall be [śēḵ](../../strongs/h/h7899.md) in your ['ayin](../../strongs/h/h5869.md), and [ṣᵊnînîm](../../strongs/h/h6796.md) in your [ṣaḏ](../../strongs/h/h6654.md), and shall [tsarar](../../strongs/h/h6887.md) you in the ['erets](../../strongs/h/h776.md) wherein ye [yashab](../../strongs/h/h3427.md).

<a name="numbers_33_56"></a>Numbers 33:56

Moreover it shall come to pass, that I shall ['asah](../../strongs/h/h6213.md) unto you, as I [dāmâ](../../strongs/h/h1819.md) to ['asah](../../strongs/h/h6213.md) unto them.

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 32](numbers_32.md) - [Numbers 34](numbers_34.md)