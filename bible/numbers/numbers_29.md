# [Numbers 29](https://www.blueletterbible.org/kjv/num/29)

<a name="numbers_29_1"></a>Numbers 29:1

And in the seventh [ḥōḏeš](../../strongs/h/h2320.md), on the first of the [ḥōḏeš](../../strongs/h/h2320.md), ye shall have a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md); ye shall ['asah](../../strongs/h/h6213.md) no [ʿăḇōḏâ](../../strongs/h/h5656.md) [mĕla'kah](../../strongs/h/h4399.md): it is a [yowm](../../strongs/h/h3117.md) of [tᵊrûʿâ](../../strongs/h/h8643.md) unto you.

<a name="numbers_29_2"></a>Numbers 29:2

And ye shall ['asah](../../strongs/h/h6213.md) an [ʿōlâ](../../strongs/h/h5930.md) for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md); one [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), and seven [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md):

<a name="numbers_29_3"></a>Numbers 29:3

And their [minchah](../../strongs/h/h4503.md) shall be of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), three [ʿiśśārôn](../../strongs/h/h6241.md)s for a [par](../../strongs/h/h6499.md), and two [ʿiśśārôn](../../strongs/h/h6241.md)s for an ['ayil](../../strongs/h/h352.md),

<a name="numbers_29_4"></a>Numbers 29:4

And one [ʿiśśārôn](../../strongs/h/h6241.md) for one [keḇeś](../../strongs/h/h3532.md), throughout the seven [keḇeś](../../strongs/h/h3532.md):

<a name="numbers_29_5"></a>Numbers 29:5

And one [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md), to make a [kāp̄ar](../../strongs/h/h3722.md) for you:

<a name="numbers_29_6"></a>Numbers 29:6

Beside the an [ʿōlâ](../../strongs/h/h5930.md) of the [ḥōḏeš](../../strongs/h/h2320.md), and his [minchah](../../strongs/h/h4503.md), and the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), and his [minchah](../../strongs/h/h4503.md), and their [necek](../../strongs/h/h5262.md), according unto their [mishpat](../../strongs/h/h4941.md), for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md), an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_29_7"></a>Numbers 29:7

And ye shall have on the tenth of this seventh [ḥōḏeš](../../strongs/h/h2320.md) a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md); and ye shall [ʿānâ](../../strongs/h/h6031.md) your [nephesh](../../strongs/h/h5315.md): ye shall not ['asah](../../strongs/h/h6213.md) any [mĕla'kah](../../strongs/h/h4399.md) therein:

<a name="numbers_29_8"></a>Numbers 29:8

But ye shall [qāraḇ](../../strongs/h/h7126.md) an [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md) for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md); one [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), and seven [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md); they shall be unto you [tamiym](../../strongs/h/h8549.md):

<a name="numbers_29_9"></a>Numbers 29:9

And their [minchah](../../strongs/h/h4503.md) shall be of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), three [ʿiśśārôn](../../strongs/h/h6241.md)s to a [par](../../strongs/h/h6499.md), and two [ʿiśśārôn](../../strongs/h/h6241.md)s to one ['ayil](../../strongs/h/h352.md),

<a name="numbers_29_10"></a>Numbers 29:10

A several [ʿiśśārôn](../../strongs/h/h6241.md) for one [keḇeś](../../strongs/h/h3532.md), throughout the seven [keḇeś](../../strongs/h/h3532.md):

<a name="numbers_29_11"></a>Numbers 29:11

One [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md); beside the [chatta'ath](../../strongs/h/h2403.md) of [kipur](../../strongs/h/h3725.md), and the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), and the [minchah](../../strongs/h/h4503.md) of it, and their [necek](../../strongs/h/h5262.md).

<a name="numbers_29_12"></a>Numbers 29:12

And on the fifteenth [yowm](../../strongs/h/h3117.md) of the seventh [ḥōḏeš](../../strongs/h/h2320.md) ye shall have a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md); ye shall ['asah](../../strongs/h/h6213.md) no [ʿăḇōḏâ](../../strongs/h/h5656.md) [mĕla'kah](../../strongs/h/h4399.md), and ye shall [ḥāḡaḡ](../../strongs/h/h2287.md) a [ḥāḡ](../../strongs/h/h2282.md) unto [Yĕhovah](../../strongs/h/h3068.md) seven [yowm](../../strongs/h/h3117.md):

<a name="numbers_29_13"></a>Numbers 29:13

And ye shall [qāraḇ](../../strongs/h/h7126.md) an [ʿōlâ](../../strongs/h/h5930.md), an ['iššê](../../strongs/h/h801.md), of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md); thirteen [bāqār](../../strongs/h/h1241.md) [ben](../../strongs/h/h1121.md) [par](../../strongs/h/h6499.md), two ['ayil](../../strongs/h/h352.md), and fourteen [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md); they shall be [tamiym](../../strongs/h/h8549.md):

<a name="numbers_29_14"></a>Numbers 29:14

And their [minchah](../../strongs/h/h4503.md) shall be of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), three [ʿiśśārôn](../../strongs/h/h6241.md)s unto every [par](../../strongs/h/h6499.md) of the thirteen [par](../../strongs/h/h6499.md), two [ʿiśśārôn](../../strongs/h/h6241.md) to each ['ayil](../../strongs/h/h352.md) of the two ['ayil](../../strongs/h/h352.md),

<a name="numbers_29_15"></a>Numbers 29:15

And a several [ʿiśśārôn](../../strongs/h/h6241.md) to each [keḇeś](../../strongs/h/h3532.md) of the fourteen [keḇeś](../../strongs/h/h3532.md):

<a name="numbers_29_16"></a>Numbers 29:16

And one [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md); beside the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), his [minchah](../../strongs/h/h4503.md), and his [necek](../../strongs/h/h5262.md).

<a name="numbers_29_17"></a>Numbers 29:17

And on the second [yowm](../../strongs/h/h3117.md) ye shall offer twelve [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), two ['ayil](../../strongs/h/h352.md), fourteen [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md):

<a name="numbers_29_18"></a>Numbers 29:18

And their [minchah](../../strongs/h/h4503.md) and their [necek](../../strongs/h/h5262.md) for the [par](../../strongs/h/h6499.md), for the ['ayil](../../strongs/h/h352.md), and for the [keḇeś](../../strongs/h/h3532.md), shall be according to their [mispār](../../strongs/h/h4557.md), after the [mishpat](../../strongs/h/h4941.md):

<a name="numbers_29_19"></a>Numbers 29:19

And one [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md); beside the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), and the [minchah](../../strongs/h/h4503.md) thereof, and their [necek](../../strongs/h/h5262.md).

<a name="numbers_29_20"></a>Numbers 29:20

And on the third [yowm](../../strongs/h/h3117.md) eleven [par](../../strongs/h/h6499.md), two ['ayil](../../strongs/h/h352.md), fourteen [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md);

<a name="numbers_29_21"></a>Numbers 29:21

And their [minchah](../../strongs/h/h4503.md) and their [necek](../../strongs/h/h5262.md) for the [par](../../strongs/h/h6499.md), for the ['ayil](../../strongs/h/h352.md), and for the [keḇeś](../../strongs/h/h3532.md), shall be according to their [mispār](../../strongs/h/h4557.md), after the [mishpat](../../strongs/h/h4941.md):

<a name="numbers_29_22"></a>Numbers 29:22

And one [śāʿîr](../../strongs/h/h8163.md) for a [chatta'ath](../../strongs/h/h2403.md); beside the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), and his [minchah](../../strongs/h/h4503.md), and his [necek](../../strongs/h/h5262.md).

<a name="numbers_29_23"></a>Numbers 29:23

And on the fourth [yowm](../../strongs/h/h3117.md) ten [par](../../strongs/h/h6499.md), two ['ayil](../../strongs/h/h352.md), and fourteen [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md):

<a name="numbers_29_24"></a>Numbers 29:24

Their [minchah](../../strongs/h/h4503.md) and their [necek](../../strongs/h/h5262.md) for the [par](../../strongs/h/h6499.md), for the ['ayil](../../strongs/h/h352.md), and for the [keḇeś](../../strongs/h/h3532.md), shall be according to their [mispār](../../strongs/h/h4557.md), after the [mishpat](../../strongs/h/h4941.md):

<a name="numbers_29_25"></a>Numbers 29:25

And one [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md); beside the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), his [minchah](../../strongs/h/h4503.md), and his [necek](../../strongs/h/h5262.md).

<a name="numbers_29_26"></a>Numbers 29:26

And on the fifth [yowm](../../strongs/h/h3117.md) nine [par](../../strongs/h/h6499.md), two ['ayil](../../strongs/h/h352.md), and fourteen [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md):

<a name="numbers_29_27"></a>Numbers 29:27

And their [minchah](../../strongs/h/h4503.md) and their [necek](../../strongs/h/h5262.md) for the [par](../../strongs/h/h6499.md), for the ['ayil](../../strongs/h/h352.md), and for the [keḇeś](../../strongs/h/h3532.md), shall be according to their [mispār](../../strongs/h/h4557.md), after the [mishpat](../../strongs/h/h4941.md):

<a name="numbers_29_28"></a>Numbers 29:28

And one [śāʿîr](../../strongs/h/h8163.md) for a [chatta'ath](../../strongs/h/h2403.md); beside the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), and his [minchah](../../strongs/h/h4503.md), and his [necek](../../strongs/h/h5262.md).

<a name="numbers_29_29"></a>Numbers 29:29

And on the sixth [yowm](../../strongs/h/h3117.md) eight [par](../../strongs/h/h6499.md), two ['ayil](../../strongs/h/h352.md), and fourteen [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md):

<a name="numbers_29_30"></a>Numbers 29:30

And their [minchah](../../strongs/h/h4503.md) and their [necek](../../strongs/h/h5262.md) for the [par](../../strongs/h/h6499.md), for the ['ayil](../../strongs/h/h352.md), and for the [keḇeś](../../strongs/h/h3532.md), shall be according to their [mispār](../../strongs/h/h4557.md), after the [mishpat](../../strongs/h/h4941.md):

<a name="numbers_29_31"></a>Numbers 29:31

And one [śāʿîr](../../strongs/h/h8163.md) for a [chatta'ath](../../strongs/h/h2403.md); beside the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), his [minchah](../../strongs/h/h4503.md), and his [necek](../../strongs/h/h5262.md).

<a name="numbers_29_32"></a>Numbers 29:32

And on the seventh [yowm](../../strongs/h/h3117.md) seven [par](../../strongs/h/h6499.md), two ['ayil](../../strongs/h/h352.md), and fourteen [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md):

<a name="numbers_29_33"></a>Numbers 29:33

And their [minchah](../../strongs/h/h4503.md) and their [necek](../../strongs/h/h5262.md) for the [par](../../strongs/h/h6499.md), for the ['ayil](../../strongs/h/h352.md), and for the [keḇeś](../../strongs/h/h3532.md), shall be according to their [mispār](../../strongs/h/h4557.md), after the [mishpat](../../strongs/h/h4941.md):

<a name="numbers_29_34"></a>Numbers 29:34

And one [śāʿîr](../../strongs/h/h8163.md) for a [chatta'ath](../../strongs/h/h2403.md); beside the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), his [minchah](../../strongs/h/h4503.md), and his [necek](../../strongs/h/h5262.md).

<a name="numbers_29_35"></a>Numbers 29:35

On the eighth [yowm](../../strongs/h/h3117.md) ye shall have an [ʿăṣārâ](../../strongs/h/h6116.md): ye shall ['asah](../../strongs/h/h6213.md) no [ʿăḇōḏâ](../../strongs/h/h5656.md) [mĕla'kah](../../strongs/h/h4399.md) therein:

<a name="numbers_29_36"></a>Numbers 29:36

But ye shall [qāraḇ](../../strongs/h/h7126.md) an [ʿōlâ](../../strongs/h/h5930.md), an ['iššê](../../strongs/h/h801.md), of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md): one [par](../../strongs/h/h6499.md), one ['ayil](../../strongs/h/h352.md), seven [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md):

<a name="numbers_29_37"></a>Numbers 29:37

Their [minchah](../../strongs/h/h4503.md) and their [necek](../../strongs/h/h5262.md) for the [par](../../strongs/h/h6499.md), for the ['ayil](../../strongs/h/h352.md), and for the [keḇeś](../../strongs/h/h3532.md), shall be according to their [mispār](../../strongs/h/h4557.md), after the [mishpat](../../strongs/h/h4941.md):

<a name="numbers_29_38"></a>Numbers 29:38

And one [śāʿîr](../../strongs/h/h8163.md) for a [chatta'ath](../../strongs/h/h2403.md); beside the [tāmîḏ](../../strongs/h/h8548.md) an [ʿōlâ](../../strongs/h/h5930.md), and his [minchah](../../strongs/h/h4503.md), and his [necek](../../strongs/h/h5262.md).

<a name="numbers_29_39"></a>Numbers 29:39

These things ye shall ['asah](../../strongs/h/h6213.md) unto [Yĕhovah](../../strongs/h/h3068.md) in your [môʿēḏ](../../strongs/h/h4150.md), beside your [neḏer](../../strongs/h/h5088.md), and your [nᵊḏāḇâ](../../strongs/h/h5071.md), for your an [ʿōlâ](../../strongs/h/h5930.md), and for your meat [minchah](../../strongs/h/h4503.md), and for your [necek](../../strongs/h/h5262.md), and for your [šelem](../../strongs/h/h8002.md).

<a name="numbers_29_40"></a>Numbers 29:40

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) according to all that [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 28](numbers_28.md) - [Numbers 30](numbers_30.md)