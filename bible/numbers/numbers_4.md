# [Numbers 4](https://www.blueletterbible.org/kjv/num/4)

<a name="numbers_4_1"></a>Numbers 4:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) and unto ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_4_2"></a>Numbers 4:2

[nasa'](../../strongs/h/h5375.md) the [ro'sh](../../strongs/h/h7218.md) of the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md) from [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md), after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md),

<a name="numbers_4_3"></a>Numbers 4:3

From thirty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md) even until fifty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), all that [bow'](../../strongs/h/h935.md) into the [tsaba'](../../strongs/h/h6635.md), to ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="numbers_4_4"></a>Numbers 4:4

This shall be the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), about the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md):

<a name="numbers_4_5"></a>Numbers 4:5

And when the [maḥănê](../../strongs/h/h4264.md) [nāsaʿ](../../strongs/h/h5265.md), ['Ahărôn](../../strongs/h/h175.md) shall [bow'](../../strongs/h/h935.md), and his [ben](../../strongs/h/h1121.md), and they shall [yarad](../../strongs/h/h3381.md) the [māsāḵ](../../strongs/h/h4539.md) [pārōḵeṯ](../../strongs/h/h6532.md), and [kāsâ](../../strongs/h/h3680.md) the ['ārôn](../../strongs/h/h727.md) of [ʿēḏûṯ](../../strongs/h/h5715.md) with it:

<a name="numbers_4_6"></a>Numbers 4:6

And shall [nathan](../../strongs/h/h5414.md) thereon the [kāsûy](../../strongs/h/h3681.md) of [taḥaš](../../strongs/h/h8476.md) ['owr](../../strongs/h/h5785.md), and shall [pāraś](../../strongs/h/h6566.md) [maʿal](../../strongs/h/h4605.md) it a [beḡeḏ](../../strongs/h/h899.md) [kālîl](../../strongs/h/h3632.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), and shall [śûm](../../strongs/h/h7760.md) in the [baḏ](../../strongs/h/h905.md) thereof.

<a name="numbers_4_7"></a>Numbers 4:7

And upon the [šulḥān](../../strongs/h/h7979.md) of [paniym](../../strongs/h/h6440.md) they shall [pāraś](../../strongs/h/h6566.md) a [beḡeḏ](../../strongs/h/h899.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), and [nathan](../../strongs/h/h5414.md) thereon the [qᵊʿārâ](../../strongs/h/h7086.md), and the [kaph](../../strongs/h/h3709.md), and the [mᵊnaqqîṯ](../../strongs/h/h4518.md), and [qaśvâ](../../strongs/h/h7184.md) to [necek](../../strongs/h/h5262.md): and the [tāmîḏ](../../strongs/h/h8548.md) [lechem](../../strongs/h/h3899.md) shall be thereon:

<a name="numbers_4_8"></a>Numbers 4:8

And they shall [pāraś](../../strongs/h/h6566.md) upon them a [beḡeḏ](../../strongs/h/h899.md) of [tôlāʿ](../../strongs/h/h8438.md) [šānî](../../strongs/h/h8144.md), and [kāsâ](../../strongs/h/h3680.md) the same with a [miḵsê](../../strongs/h/h4372.md) of [taḥaš](../../strongs/h/h8476.md) ['owr](../../strongs/h/h5785.md), and shall [śûm](../../strongs/h/h7760.md) in the [baḏ](../../strongs/h/h905.md) thereof.

<a name="numbers_4_9"></a>Numbers 4:9

And they shall [laqach](../../strongs/h/h3947.md) a [beḡeḏ](../../strongs/h/h899.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), and [kāsâ](../../strongs/h/h3680.md) the [mᵊnôrâ](../../strongs/h/h4501.md) of the [ma'owr](../../strongs/h/h3974.md), and his [nîr](../../strongs/h/h5216.md), and his [malqāḥayim](../../strongs/h/h4457.md), and his [maḥtâ](../../strongs/h/h4289.md), and all the [šemen](../../strongs/h/h8081.md) [kĕliy](../../strongs/h/h3627.md) thereof, wherewith they [sharath](../../strongs/h/h8334.md) unto it:

<a name="numbers_4_10"></a>Numbers 4:10

And they shall [nathan](../../strongs/h/h5414.md) it and all the [kĕliy](../../strongs/h/h3627.md) thereof within a [miḵsê](../../strongs/h/h4372.md) of [taḥaš](../../strongs/h/h8476.md) ['owr](../../strongs/h/h5785.md), and shall [nathan](../../strongs/h/h5414.md) it upon a [môṭ](../../strongs/h/h4132.md).

<a name="numbers_4_11"></a>Numbers 4:11

And upon the [zāhāḇ](../../strongs/h/h2091.md) [mizbeach](../../strongs/h/h4196.md) they shall [pāraś](../../strongs/h/h6566.md) a [beḡeḏ](../../strongs/h/h899.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), and [kāsâ](../../strongs/h/h3680.md) it with a [miḵsê](../../strongs/h/h4372.md) of [taḥaš](../../strongs/h/h8476.md) ['owr](../../strongs/h/h5785.md), and shall [śûm](../../strongs/h/h7760.md) to the [baḏ](../../strongs/h/h905.md) thereof:

<a name="numbers_4_12"></a>Numbers 4:12

And they shall [laqach](../../strongs/h/h3947.md) all the [kĕliy](../../strongs/h/h3627.md) of [šārēṯ](../../strongs/h/h8335.md), wherewith they [sharath](../../strongs/h/h8334.md) in the [qodesh](../../strongs/h/h6944.md), and [nathan](../../strongs/h/h5414.md) them in a [beḡeḏ](../../strongs/h/h899.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), and [kāsâ](../../strongs/h/h3680.md) them with a [miḵsê](../../strongs/h/h4372.md) of [taḥaš](../../strongs/h/h8476.md) ['owr](../../strongs/h/h5785.md), and shall [nathan](../../strongs/h/h5414.md) them on a [môṭ](../../strongs/h/h4132.md):

<a name="numbers_4_13"></a>Numbers 4:13

And they shall [dāšēn](../../strongs/h/h1878.md) from the [mizbeach](../../strongs/h/h4196.md), and [pāraś](../../strongs/h/h6566.md) an ['argāmān](../../strongs/h/h713.md) [beḡeḏ](../../strongs/h/h899.md) thereon:

<a name="numbers_4_14"></a>Numbers 4:14

And they shall [nathan](../../strongs/h/h5414.md) upon it all the [kĕliy](../../strongs/h/h3627.md) thereof, wherewith they [sharath](../../strongs/h/h8334.md) about it, even the [maḥtâ](../../strongs/h/h4289.md), the [mazlēḡ](../../strongs/h/h4207.md), and the [yāʿ](../../strongs/h/h3257.md), and the [mizrāq](../../strongs/h/h4219.md), all the [kĕliy](../../strongs/h/h3627.md) of the [mizbeach](../../strongs/h/h4196.md); and they shall [pāraś](../../strongs/h/h6566.md) upon it a [kāsûy](../../strongs/h/h3681.md) of [taḥaš](../../strongs/h/h8476.md) ['owr](../../strongs/h/h5785.md), and [śûm](../../strongs/h/h7760.md) to the [baḏ](../../strongs/h/h905.md) of it.

<a name="numbers_4_15"></a>Numbers 4:15

And when ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) have [kalah](../../strongs/h/h3615.md) of [kāsâ](../../strongs/h/h3680.md) the [qodesh](../../strongs/h/h6944.md), and all the [kĕliy](../../strongs/h/h3627.md) of the [qodesh](../../strongs/h/h6944.md), as the [maḥănê](../../strongs/h/h4264.md) is to [nāsaʿ](../../strongs/h/h5265.md); ['aḥar](../../strongs/h/h310.md) that, the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md) shall [bow'](../../strongs/h/h935.md) to [nasa'](../../strongs/h/h5375.md) it: but they shall not [naga'](../../strongs/h/h5060.md) any [qodesh](../../strongs/h/h6944.md), lest they [muwth](../../strongs/h/h4191.md). These things are the [maśśā'](../../strongs/h/h4853.md) of the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="numbers_4_16"></a>Numbers 4:16

And to the [pᵊqudâ](../../strongs/h/h6486.md) of ['Elʿāzār](../../strongs/h/h499.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md) pertaineth the [šemen](../../strongs/h/h8081.md) for the [ma'owr](../../strongs/h/h3974.md), and the [sam](../../strongs/h/h5561.md) [qᵊṭōreṯ](../../strongs/h/h7004.md), and the [tāmîḏ](../../strongs/h/h8548.md) [minchah](../../strongs/h/h4503.md), and the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and the [pᵊqudâ](../../strongs/h/h6486.md) of all the [miškān](../../strongs/h/h4908.md), and of all that therein is, in the [qodesh](../../strongs/h/h6944.md), and in the [kĕliy](../../strongs/h/h3627.md) thereof.

<a name="numbers_4_17"></a>Numbers 4:17

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) and unto ['Ahărôn](../../strongs/h/h175.md) ['āmar](../../strongs/h/h559.md),

<a name="numbers_4_18"></a>Numbers 4:18

[karath](../../strongs/h/h3772.md) ye not the [shebet](../../strongs/h/h7626.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of the [Qᵊhāṯî](../../strongs/h/h6956.md) from [tavek](../../strongs/h/h8432.md) the [Lᵊvî](../../strongs/h/h3881.md):

<a name="numbers_4_19"></a>Numbers 4:19

But thus ['asah](../../strongs/h/h6213.md) unto them, that they may [ḥāyâ](../../strongs/h/h2421.md), and not [muwth](../../strongs/h/h4191.md), when they [nāḡaš](../../strongs/h/h5066.md) unto the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md): ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) shall [bow'](../../strongs/h/h935.md), and [śûm](../../strongs/h/h7760.md) them every ['iysh](../../strongs/h/h376.md) to his [ʿăḇōḏâ](../../strongs/h/h5656.md) and to his [maśśā'](../../strongs/h/h4853.md):

<a name="numbers_4_20"></a>Numbers 4:20

But they shall not [bow'](../../strongs/h/h935.md) to [ra'ah](../../strongs/h/h7200.md) when the [qodesh](../../strongs/h/h6944.md) are [bālaʿ](../../strongs/h/h1104.md), lest they [muwth](../../strongs/h/h4191.md).

<a name="numbers_4_21"></a>Numbers 4:21

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_4_22"></a>Numbers 4:22

[nasa'](../../strongs/h/h5375.md) also the [ro'sh](../../strongs/h/h7218.md) of the [ben](../../strongs/h/h1121.md) of [Gēršôn](../../strongs/h/h1648.md), throughout the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), by their [mišpāḥâ](../../strongs/h/h4940.md);

<a name="numbers_4_23"></a>Numbers 4:23

From thirty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md) until fifty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) shalt thou [paqad](../../strongs/h/h6485.md) them; all that [bow'](../../strongs/h/h935.md) to [ṣᵊḇā'](../../strongs/h/h6633.md) the [tsaba'](../../strongs/h/h6635.md), to ['abad](../../strongs/h/h5647.md) the [ʿăḇōḏâ](../../strongs/h/h5656.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="numbers_4_24"></a>Numbers 4:24

This is the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of the [Gēršunnî](../../strongs/h/h1649.md), to ['abad](../../strongs/h/h5647.md), and for [maśśā'](../../strongs/h/h4853.md):

<a name="numbers_4_25"></a>Numbers 4:25

And they shall [nasa'](../../strongs/h/h5375.md) the [yᵊrîʿâ](../../strongs/h/h3407.md) of the [miškān](../../strongs/h/h4908.md), and the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), his [miḵsê](../../strongs/h/h4372.md), and the [miḵsê](../../strongs/h/h4372.md) of the [taḥaš](../../strongs/h/h8476.md) that is [maʿal](../../strongs/h/h4605.md) upon it, and the [māsāḵ](../../strongs/h/h4539.md) for the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md),

<a name="numbers_4_26"></a>Numbers 4:26

And the [qelaʿ](../../strongs/h/h7050.md) of the [ḥāṣēr](../../strongs/h/h2691.md), and the [māsāḵ](../../strongs/h/h4539.md) for the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md) of the [ḥāṣēr](../../strongs/h/h2691.md), which is by the [miškān](../../strongs/h/h4908.md) and by the [mizbeach](../../strongs/h/h4196.md) [cabiyb](../../strongs/h/h5439.md), and their [mêṯār](../../strongs/h/h4340.md), and all the [kĕliy](../../strongs/h/h3627.md) of their [ʿăḇōḏâ](../../strongs/h/h5656.md), and all that is ['asah](../../strongs/h/h6213.md) for them: so shall they ['abad](../../strongs/h/h5647.md).

<a name="numbers_4_27"></a>Numbers 4:27

At the [peh](../../strongs/h/h6310.md) of ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) shall be all the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [ben](../../strongs/h/h1121.md) of the [Gēršunnî](../../strongs/h/h1649.md), in all their [maśśā'](../../strongs/h/h4853.md), and in all their [ʿăḇōḏâ](../../strongs/h/h5656.md): and ye shall [paqad](../../strongs/h/h6485.md) unto them in [mišmereṯ](../../strongs/h/h4931.md) all their [maśśā'](../../strongs/h/h4853.md).

<a name="numbers_4_28"></a>Numbers 4:28

This is the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Gēršunnî](../../strongs/h/h1649.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md): and their [mišmereṯ](../../strongs/h/h4931.md) shall be under the [yad](../../strongs/h/h3027.md) of ['Îṯāmār](../../strongs/h/h385.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md).

<a name="numbers_4_29"></a>Numbers 4:29

As for the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md), thou shalt [paqad](../../strongs/h/h6485.md) them after their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md);

<a name="numbers_4_30"></a>Numbers 4:30

From thirty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md) even unto fifty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) shalt thou [paqad](../../strongs/h/h6485.md) them, every one that [bow'](../../strongs/h/h935.md) into the [tsaba'](../../strongs/h/h6635.md), to ['abad](../../strongs/h/h5647.md) the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="numbers_4_31"></a>Numbers 4:31

And this is the [mišmereṯ](../../strongs/h/h4931.md) of their [maśśā'](../../strongs/h/h4853.md), according to all their [ʿăḇōḏâ](../../strongs/h/h5656.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md); the [qereš](../../strongs/h/h7175.md) of the [miškān](../../strongs/h/h4908.md), and the [bᵊrîaḥ](../../strongs/h/h1280.md) thereof, and the [ʿammûḏ](../../strongs/h/h5982.md) thereof, and ['eḏen](../../strongs/h/h134.md) thereof,

<a name="numbers_4_32"></a>Numbers 4:32

And the [ʿammûḏ](../../strongs/h/h5982.md) of the [ḥāṣēr](../../strongs/h/h2691.md) [cabiyb](../../strongs/h/h5439.md), and their ['eḏen](../../strongs/h/h134.md), and their [yāṯēḏ](../../strongs/h/h3489.md), and their [mêṯār](../../strongs/h/h4340.md), with all their [kĕliy](../../strongs/h/h3627.md), and with all their [ʿăḇōḏâ](../../strongs/h/h5656.md): and by [shem](../../strongs/h/h8034.md) ye shall [paqad](../../strongs/h/h6485.md) the [kĕliy](../../strongs/h/h3627.md) of the [mišmereṯ](../../strongs/h/h4931.md) of their [maśśā'](../../strongs/h/h4853.md).

<a name="numbers_4_33"></a>Numbers 4:33

This is the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md), according to all their [ʿăḇōḏâ](../../strongs/h/h5656.md), in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), under the [yad](../../strongs/h/h3027.md) of ['Îṯāmār](../../strongs/h/h385.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md).

<a name="numbers_4_34"></a>Numbers 4:34

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) and the [nāśî'](../../strongs/h/h5387.md) of the ['edah](../../strongs/h/h5712.md) [paqad](../../strongs/h/h6485.md) the [ben](../../strongs/h/h1121.md) of the [Qᵊhāṯî](../../strongs/h/h6956.md) after their [mišpāḥâ](../../strongs/h/h4940.md), and after the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md),

<a name="numbers_4_35"></a>Numbers 4:35

From thirty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md) even unto fifty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), every one that [bow'](../../strongs/h/h935.md) into the [tsaba'](../../strongs/h/h6635.md), for the [ʿăḇōḏâ](../../strongs/h/h5656.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md):

<a name="numbers_4_36"></a>Numbers 4:36

And those that were [paqad](../../strongs/h/h6485.md) of them by their [mišpāḥâ](../../strongs/h/h4940.md) were two thousand seven hundred and fifty.

<a name="numbers_4_37"></a>Numbers 4:37

These were they that were [paqad](../../strongs/h/h6485.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of the [Qᵊhāṯî](../../strongs/h/h6956.md), all that might ['abad](../../strongs/h/h5647.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), which [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) did [paqad](../../strongs/h/h6485.md) according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

<a name="numbers_4_38"></a>Numbers 4:38

And those that were [paqad](../../strongs/h/h6485.md) of the [ben](../../strongs/h/h1121.md) of [Gēršôn](../../strongs/h/h1648.md), throughout their [mišpāḥâ](../../strongs/h/h4940.md), and by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md),

<a name="numbers_4_39"></a>Numbers 4:39

From thirty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md) even unto fifty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), every one that [bow'](../../strongs/h/h935.md) into the [tsaba'](../../strongs/h/h6635.md), for the [ʿăḇōḏâ](../../strongs/h/h5656.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md),

<a name="numbers_4_40"></a>Numbers 4:40

Even those that were [paqad](../../strongs/h/h6485.md) of them, throughout their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), were two thousand and six hundred and thirty.

<a name="numbers_4_41"></a>Numbers 4:41

These are they that were [paqad](../../strongs/h/h6485.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Gēršôn](../../strongs/h/h1648.md), of all that might ['abad](../../strongs/h/h5647.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), whom [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) did [paqad](../../strongs/h/h6485.md) according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_4_42"></a>Numbers 4:42

And those that were [paqad](../../strongs/h/h6485.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md), throughout their [mišpāḥâ](../../strongs/h/h4940.md), by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md),

<a name="numbers_4_43"></a>Numbers 4:43

From thirty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md) even unto fifty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), every one that [bow'](../../strongs/h/h935.md) into the [tsaba'](../../strongs/h/h6635.md), for the [ʿăḇōḏâ](../../strongs/h/h5656.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md),

<a name="numbers_4_44"></a>Numbers 4:44

Even those that were [paqad](../../strongs/h/h6485.md) of them after their [mišpāḥâ](../../strongs/h/h4940.md), were three thousand and two hundred.

<a name="numbers_4_45"></a>Numbers 4:45

These be those that were [paqad](../../strongs/h/h6485.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md), whom [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [paqad](../../strongs/h/h6485.md) according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

<a name="numbers_4_46"></a>Numbers 4:46

All those that were [paqad](../../strongs/h/h6485.md) of the [Lᵊvî](../../strongs/h/h3881.md), whom [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) and the [nāśî'](../../strongs/h/h5387.md) of [Yisra'el](../../strongs/h/h3478.md) [paqad](../../strongs/h/h6485.md), after their [mišpāḥâ](../../strongs/h/h4940.md), and after the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md),

<a name="numbers_4_47"></a>Numbers 4:47

From thirty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md) even unto fifty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), every one that [bow'](../../strongs/h/h935.md) to ['abad](../../strongs/h/h5647.md) the [ʿăḇōḏâ](../../strongs/h/h5656.md), and the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [maśśā'](../../strongs/h/h4853.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="numbers_4_48"></a>Numbers 4:48

Even those that were [paqad](../../strongs/h/h6485.md) of them, were eight thousand and five hundred and fourscore,

<a name="numbers_4_49"></a>Numbers 4:49

According to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) they were [paqad](../../strongs/h/h6485.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md), ['iysh](../../strongs/h/h376.md) ['iysh](../../strongs/h/h376.md) according to his [ʿăḇōḏâ](../../strongs/h/h5656.md), and according to his [maśśā'](../../strongs/h/h4853.md): thus were they [paqad](../../strongs/h/h6485.md) of him, as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 3](numbers_3.md) - [Numbers 5](numbers_5.md)