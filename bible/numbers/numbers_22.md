# [Numbers 22](https://www.blueletterbible.org/kjv/num/22)

<a name="numbers_22_1"></a>Numbers 22:1

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāsaʿ](../../strongs/h/h5265.md), and [ḥānâ](../../strongs/h/h2583.md) in the ['arabah](../../strongs/h/h6160.md) of [Mô'āḇ](../../strongs/h/h4124.md) on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) by [Yᵊrēḥô](../../strongs/h/h3405.md).

<a name="numbers_22_2"></a>Numbers 22:2

And [Bālāq](../../strongs/h/h1111.md) the [ben](../../strongs/h/h1121.md) of [ṣipôr](../../strongs/h/h6834.md) [ra'ah](../../strongs/h/h7200.md) all that [Yisra'el](../../strongs/h/h3478.md) had ['asah](../../strongs/h/h6213.md) to the ['Ĕmōrî](../../strongs/h/h567.md).

<a name="numbers_22_3"></a>Numbers 22:3

And [Mô'āḇ](../../strongs/h/h4124.md) was [me'od](../../strongs/h/h3966.md) [guwr](../../strongs/h/h1481.md) [paniym](../../strongs/h/h6440.md) the ['am](../../strongs/h/h5971.md), because they were [rab](../../strongs/h/h7227.md): and [Mô'āḇ](../../strongs/h/h4124.md) was [qûṣ](../../strongs/h/h6973.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_22_4"></a>Numbers 22:4

And [Mô'āḇ](../../strongs/h/h4124.md) ['āmar](../../strongs/h/h559.md) unto the [zāqēn](../../strongs/h/h2205.md) of [Miḏyān](../../strongs/h/h4080.md), Now shall this [qāhēl](../../strongs/h/h6951.md) [lāḥaḵ](../../strongs/h/h3897.md) all that are [cabiyb](../../strongs/h/h5439.md) us, as the [showr](../../strongs/h/h7794.md) [lāḥaḵ](../../strongs/h/h3897.md) the [yereq](../../strongs/h/h3418.md) of the [sadeh](../../strongs/h/h7704.md). And [Bālāq](../../strongs/h/h1111.md) the [ben](../../strongs/h/h1121.md) of [Tṣipôr](../../strongs/h/h6834.md) was [melek](../../strongs/h/h4428.md) of the [Mô'āḇ](../../strongs/h/h4124.md) at that [ʿēṯ](../../strongs/h/h6256.md).

<a name="numbers_22_5"></a>Numbers 22:5

He [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) therefore unto [Bilʿām](../../strongs/h/h1109.md) the [ben](../../strongs/h/h1121.md) of [Bᵊʿôr](../../strongs/h/h1160.md) to [Pᵊṯôr](../../strongs/h/h6604.md), which is by the [nāhār](../../strongs/h/h5104.md) of the ['erets](../../strongs/h/h776.md) of the [ben](../../strongs/h/h1121.md) of his ['am](../../strongs/h/h5971.md), to [qara'](../../strongs/h/h7121.md) him, ['āmar](../../strongs/h/h559.md), Behold, there is an ['am](../../strongs/h/h5971.md) [yāṣā'](../../strongs/h/h3318.md) from [Mitsrayim](../../strongs/h/h4714.md): behold, they [kāsâ](../../strongs/h/h3680.md) the ['ayin](../../strongs/h/h5869.md) of the ['erets](../../strongs/h/h776.md), and they [yashab](../../strongs/h/h3427.md) [môl](../../strongs/h/h4136.md) me:

<a name="numbers_22_6"></a>Numbers 22:6

[yālaḵ](../../strongs/h/h3212.md) now therefore, I pray thee, ['arar](../../strongs/h/h779.md) me this ['am](../../strongs/h/h5971.md); for they are too ['atsuwm](../../strongs/h/h6099.md) for me: peradventure I shall [yakol](../../strongs/h/h3201.md), that we may [nakah](../../strongs/h/h5221.md) them, and that I may [gāraš](../../strongs/h/h1644.md) them of the ['erets](../../strongs/h/h776.md): for I [yada'](../../strongs/h/h3045.md) that he whom thou [barak](../../strongs/h/h1288.md) is [barak](../../strongs/h/h1288.md), and he whom thou ['arar](../../strongs/h/h779.md) is ['arar](../../strongs/h/h779.md).

<a name="numbers_22_7"></a>Numbers 22:7

And the [zāqēn](../../strongs/h/h2205.md) of [Mô'āḇ](../../strongs/h/h4124.md) and the [zāqēn](../../strongs/h/h2205.md) of [Miḏyān](../../strongs/h/h4080.md) [yālaḵ](../../strongs/h/h3212.md) with the [qesem](../../strongs/h/h7081.md) in their [yad](../../strongs/h/h3027.md); and they [bow'](../../strongs/h/h935.md) unto [Bilʿām](../../strongs/h/h1109.md), and [dabar](../../strongs/h/h1696.md) unto him the [dabar](../../strongs/h/h1697.md) of [Bālāq](../../strongs/h/h1111.md).

<a name="numbers_22_8"></a>Numbers 22:8

And he ['āmar](../../strongs/h/h559.md) unto them, [lûn](../../strongs/h/h3885.md) here this [layil](../../strongs/h/h3915.md), and I will [shuwb](../../strongs/h/h7725.md) you [dabar](../../strongs/h/h1697.md), as [Yĕhovah](../../strongs/h/h3068.md) shall [dabar](../../strongs/h/h1696.md) unto me: and the [śar](../../strongs/h/h8269.md) of [Mô'āḇ](../../strongs/h/h4124.md) [yashab](../../strongs/h/h3427.md) with [Bilʿām](../../strongs/h/h1109.md).

<a name="numbers_22_9"></a>Numbers 22:9

And ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) unto [Bilʿām](../../strongs/h/h1109.md), and ['āmar](../../strongs/h/h559.md), What ['enowsh](../../strongs/h/h582.md) are these with thee?

<a name="numbers_22_10"></a>Numbers 22:10

And [Bilʿām](../../strongs/h/h1109.md) ['āmar](../../strongs/h/h559.md) unto ['Elohiym](../../strongs/h/h430.md), [Bālāq](../../strongs/h/h1111.md) the [ben](../../strongs/h/h1121.md) of [Tṣipôr](../../strongs/h/h6834.md), [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md), hath [shalach](../../strongs/h/h7971.md) unto me,

<a name="numbers_22_11"></a>Numbers 22:11

Behold, there is an ['am](../../strongs/h/h5971.md) [yāṣā'](../../strongs/h/h3318.md) of [Mitsrayim](../../strongs/h/h4714.md), which [kāsâ](../../strongs/h/h3680.md) the ['ayin](../../strongs/h/h5869.md) of the ['erets](../../strongs/h/h776.md): [yālaḵ](../../strongs/h/h3212.md) now, [qāḇaḇ](../../strongs/h/h6895.md) me them; peradventure I shall be [yakol](../../strongs/h/h3201.md) to [lāḥam](../../strongs/h/h3898.md) them, and [gāraš](../../strongs/h/h1644.md) them.

<a name="numbers_22_12"></a>Numbers 22:12

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto [Bilʿām](../../strongs/h/h1109.md), Thou shalt not [yālaḵ](../../strongs/h/h3212.md) with them; thou shalt not ['arar](../../strongs/h/h779.md) the ['am](../../strongs/h/h5971.md): for they are [barak](../../strongs/h/h1288.md).

<a name="numbers_22_13"></a>Numbers 22:13

And [Bilʿām](../../strongs/h/h1109.md) [quwm](../../strongs/h/h6965.md) in the [boqer](../../strongs/h/h1242.md), and ['āmar](../../strongs/h/h559.md) unto the [śar](../../strongs/h/h8269.md) of [Bālāq](../../strongs/h/h1111.md), [yālaḵ](../../strongs/h/h3212.md) you into your ['erets](../../strongs/h/h776.md): for [Yĕhovah](../../strongs/h/h3068.md) [mā'ēn](../../strongs/h/h3985.md) to [nathan](../../strongs/h/h5414.md) me to [halak](../../strongs/h/h1980.md) with you.

<a name="numbers_22_14"></a>Numbers 22:14

And the [śar](../../strongs/h/h8269.md) of [Mô'āḇ](../../strongs/h/h4124.md) [quwm](../../strongs/h/h6965.md), and they [bow'](../../strongs/h/h935.md) unto [Bālāq](../../strongs/h/h1111.md), and ['āmar](../../strongs/h/h559.md), [Bilʿām](../../strongs/h/h1109.md) [mā'ēn](../../strongs/h/h3985.md) to [halak](../../strongs/h/h1980.md) with us.

<a name="numbers_22_15"></a>Numbers 22:15

And [Bālāq](../../strongs/h/h1111.md) [shalach](../../strongs/h/h7971.md) yet again [śar](../../strongs/h/h8269.md), [rab](../../strongs/h/h7227.md), and [kabad](../../strongs/h/h3513.md) than they.

<a name="numbers_22_16"></a>Numbers 22:16

And they [bow'](../../strongs/h/h935.md) to [Bilʿām](../../strongs/h/h1109.md), and ['āmar](../../strongs/h/h559.md) to him, Thus ['āmar](../../strongs/h/h559.md) [Bālāq](../../strongs/h/h1111.md) the [ben](../../strongs/h/h1121.md) of [Tṣipôr](../../strongs/h/h6834.md), Let nothing, I pray thee, [mānaʿ](../../strongs/h/h4513.md) thee from [halak](../../strongs/h/h1980.md) unto me:

<a name="numbers_22_17"></a>Numbers 22:17

For I will [kabad](../../strongs/h/h3513.md) thee unto [me'od](../../strongs/h/h3966.md) [kabad](../../strongs/h/h3513.md) [kabad](../../strongs/h/h3513.md), and I will ['asah](../../strongs/h/h6213.md) whatsoever thou ['āmar](../../strongs/h/h559.md) unto me: [yālaḵ](../../strongs/h/h3212.md) therefore, I pray thee, [qāḇaḇ](../../strongs/h/h6895.md) me this ['am](../../strongs/h/h5971.md).

<a name="numbers_22_18"></a>Numbers 22:18

And [Bilʿām](../../strongs/h/h1109.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto the ['ebed](../../strongs/h/h5650.md) of [Bālāq](../../strongs/h/h1111.md), If [Bālāq](../../strongs/h/h1111.md) would [nathan](../../strongs/h/h5414.md) me his [bayith](../../strongs/h/h1004.md) [mᵊlō'](../../strongs/h/h4393.md) of [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md), I [yakol](../../strongs/h/h3201.md) ['abar](../../strongs/h/h5674.md) the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), to ['asah](../../strongs/h/h6213.md) [qāṭān](../../strongs/h/h6996.md) or [gadowl](../../strongs/h/h1419.md).

<a name="numbers_22_19"></a>Numbers 22:19

Now therefore, I pray you, [yashab](../../strongs/h/h3427.md) ye also here this [layil](../../strongs/h/h3915.md), that I may [yada'](../../strongs/h/h3045.md) what [Yĕhovah](../../strongs/h/h3068.md) will [dabar](../../strongs/h/h1696.md) unto me [yāsap̄](../../strongs/h/h3254.md).

<a name="numbers_22_20"></a>Numbers 22:20

And ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) unto [Bilʿām](../../strongs/h/h1109.md) at [layil](../../strongs/h/h3915.md), and ['āmar](../../strongs/h/h559.md) unto him, If the ['enowsh](../../strongs/h/h582.md) [bow'](../../strongs/h/h935.md) to [qara'](../../strongs/h/h7121.md) thee, [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) with them; but ['aḵ](../../strongs/h/h389.md) the [dabar](../../strongs/h/h1697.md) which I shall [dabar](../../strongs/h/h1696.md) unto thee, that shalt thou ['asah](../../strongs/h/h6213.md).

<a name="numbers_22_21"></a>Numbers 22:21

And [Bilʿām](../../strongs/h/h1109.md) [quwm](../../strongs/h/h6965.md) in the [boqer](../../strongs/h/h1242.md), and [ḥāḇaš](../../strongs/h/h2280.md) his ['āṯôn](../../strongs/h/h860.md), and [yālaḵ](../../strongs/h/h3212.md) with the [śar](../../strongs/h/h8269.md) of [Mô'āḇ](../../strongs/h/h4124.md).

<a name="numbers_22_22"></a>Numbers 22:22

And ['Elohiym](../../strongs/h/h430.md) ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md) because he [halak](../../strongs/h/h1980.md): and the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [yatsab](../../strongs/h/h3320.md) in the [derek](../../strongs/h/h1870.md) for a [satan](../../strongs/h/h7854.md) against him. Now he was [rāḵaḇ](../../strongs/h/h7392.md) upon his ['āṯôn](../../strongs/h/h860.md), and his two [naʿar](../../strongs/h/h5288.md) were with him.

<a name="numbers_22_23"></a>Numbers 22:23

And the ['āṯôn](../../strongs/h/h860.md) [ra'ah](../../strongs/h/h7200.md) the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [nāṣaḇ](../../strongs/h/h5324.md) in the [derek](../../strongs/h/h1870.md), and his [chereb](../../strongs/h/h2719.md) [šālap̄](../../strongs/h/h8025.md) in his [yad](../../strongs/h/h3027.md): and the ['āṯôn](../../strongs/h/h860.md) [natah](../../strongs/h/h5186.md) out of the [derek](../../strongs/h/h1870.md), and [yālaḵ](../../strongs/h/h3212.md) into the [sadeh](../../strongs/h/h7704.md): and [Bilʿām](../../strongs/h/h1109.md) [nakah](../../strongs/h/h5221.md) the ['āṯôn](../../strongs/h/h860.md), to [natah](../../strongs/h/h5186.md) her into the [derek](../../strongs/h/h1870.md).

<a name="numbers_22_24"></a>Numbers 22:24

But the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['amad](../../strongs/h/h5975.md) in a [mišʿôl](../../strongs/h/h4934.md) of the [kerem](../../strongs/h/h3754.md), a [gāḏēr](../../strongs/h/h1447.md) being on this side, and a [gāḏēr](../../strongs/h/h1447.md) on that side.

<a name="numbers_22_25"></a>Numbers 22:25

And when the ['āṯôn](../../strongs/h/h860.md) [ra'ah](../../strongs/h/h7200.md) the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md), she [lāḥaṣ](../../strongs/h/h3905.md) herself unto the [qîr](../../strongs/h/h7023.md), and [lāḥaṣ](../../strongs/h/h3905.md) [Bilʿām](../../strongs/h/h1109.md) [regel](../../strongs/h/h7272.md) against the wal[qîr](../../strongs/h/h7023.md): and he [nakah](../../strongs/h/h5221.md) her again.

<a name="numbers_22_26"></a>Numbers 22:26

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['abar](../../strongs/h/h5674.md) [yāsap̄](../../strongs/h/h3254.md), and ['amad](../../strongs/h/h5975.md) in a [tsar](../../strongs/h/h6862.md) [maqowm](../../strongs/h/h4725.md), where was no [derek](../../strongs/h/h1870.md) to [natah](../../strongs/h/h5186.md) either to the [yamiyn](../../strongs/h/h3225.md) or to the [śᵊmō'l](../../strongs/h/h8040.md).

<a name="numbers_22_27"></a>Numbers 22:27

And when the ['āṯôn](../../strongs/h/h860.md) [ra'ah](../../strongs/h/h7200.md) the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md), she [rāḇaṣ](../../strongs/h/h7257.md) under [Bilʿām](../../strongs/h/h1109.md): and [Bilʿām](../../strongs/h/h1109.md) ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md), and he [nakah](../../strongs/h/h5221.md) the ['āṯôn](../../strongs/h/h860.md) with a [maqqēl](../../strongs/h/h4731.md).

<a name="numbers_22_28"></a>Numbers 22:28

And [Yĕhovah](../../strongs/h/h3068.md) [pāṯaḥ](../../strongs/h/h6605.md) the [peh](../../strongs/h/h6310.md) of the ['āṯôn](../../strongs/h/h860.md), and she ['āmar](../../strongs/h/h559.md) unto [Bilʿām](../../strongs/h/h1109.md), What have I ['asah](../../strongs/h/h6213.md) unto thee, that thou hast [nakah](../../strongs/h/h5221.md) me these three [regel](../../strongs/h/h7272.md)?

<a name="numbers_22_29"></a>Numbers 22:29

And [Bilʿām](../../strongs/h/h1109.md) ['āmar](../../strongs/h/h559.md) unto the ['āṯôn](../../strongs/h/h860.md), Because thou hast [ʿālal](../../strongs/h/h5953.md) me: I would there were a [chereb](../../strongs/h/h2719.md) in mine [yad](../../strongs/h/h3027.md), for now would I [harag](../../strongs/h/h2026.md) thee.

<a name="numbers_22_30"></a>Numbers 22:30

And the ['āṯôn](../../strongs/h/h860.md) ['āmar](../../strongs/h/h559.md) unto [Bilʿām](../../strongs/h/h1109.md), Am not I thine ['āṯôn](../../strongs/h/h860.md), upon which thou hast [rāḵaḇ](../../strongs/h/h7392.md) ever since I was thine unto this [yowm](../../strongs/h/h3117.md)? was I [sāḵan](../../strongs/h/h5532.md) [sāḵan](../../strongs/h/h5532.md) to ['asah](../../strongs/h/h6213.md) so unto thee? And he ['āmar](../../strongs/h/h559.md), Nay.

<a name="numbers_22_31"></a>Numbers 22:31

Then [Yĕhovah](../../strongs/h/h3068.md) [gālâ](../../strongs/h/h1540.md) the ['ayin](../../strongs/h/h5869.md) of [Bilʿām](../../strongs/h/h1109.md), and he [ra'ah](../../strongs/h/h7200.md) the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [nāṣaḇ](../../strongs/h/h5324.md) in the [derek](../../strongs/h/h1870.md), and his [chereb](../../strongs/h/h2719.md) [šālap̄](../../strongs/h/h8025.md) in his [yad](../../strongs/h/h3027.md): and he [qāḏaḏ](../../strongs/h/h6915.md), and [shachah](../../strongs/h/h7812.md) on his ['aph](../../strongs/h/h639.md).

<a name="numbers_22_32"></a>Numbers 22:32

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, Wherefore hast thou [nakah](../../strongs/h/h5221.md) thine ['āṯôn](../../strongs/h/h860.md) these three [regel](../../strongs/h/h7272.md)? behold, I [yāṣā'](../../strongs/h/h3318.md) to [satan](../../strongs/h/h7854.md) thee, because thy [derek](../../strongs/h/h1870.md) is [yāraṭ](../../strongs/h/h3399.md) before me:

<a name="numbers_22_33"></a>Numbers 22:33

And the ['āṯôn](../../strongs/h/h860.md) [ra'ah](../../strongs/h/h7200.md) me, and [natah](../../strongs/h/h5186.md) from [paniym](../../strongs/h/h6440.md) these three [regel](../../strongs/h/h7272.md): unless she had [natah](../../strongs/h/h5186.md) from [paniym](../../strongs/h/h6440.md), surely now also I had [harag](../../strongs/h/h2026.md) thee, and [ḥāyâ](../../strongs/h/h2421.md) her.

<a name="numbers_22_34"></a>Numbers 22:34

And [Bilʿām](../../strongs/h/h1109.md) ['āmar](../../strongs/h/h559.md) unto the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md), I have [chata'](../../strongs/h/h2398.md); for I [yada'](../../strongs/h/h3045.md) not that thou [nāṣaḇ](../../strongs/h/h5324.md) in the [derek](../../strongs/h/h1870.md) [qārā'](../../strongs/h/h7125.md) me: now therefore, if it [ra'a'](../../strongs/h/h7489.md) ['ayin](../../strongs/h/h5869.md), I will [shuwb](../../strongs/h/h7725.md).

<a name="numbers_22_35"></a>Numbers 22:35

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Bilʿām](../../strongs/h/h1109.md), [yālaḵ](../../strongs/h/h3212.md) with the ['enowsh](../../strongs/h/h582.md): but ['ep̄es](../../strongs/h/h657.md) the [dabar](../../strongs/h/h1697.md) that I shall [dabar](../../strongs/h/h1696.md) unto thee, that thou shalt [dabar](../../strongs/h/h1696.md). So [Bilʿām](../../strongs/h/h1109.md) [yālaḵ](../../strongs/h/h3212.md) with the [śar](../../strongs/h/h8269.md) of [Bālāq](../../strongs/h/h1111.md).

<a name="numbers_22_36"></a>Numbers 22:36

And when [Bālāq](../../strongs/h/h1111.md) [shama'](../../strongs/h/h8085.md) that [Bilʿām](../../strongs/h/h1109.md) was [bow'](../../strongs/h/h935.md), he [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) him unto a [ʿîr](../../strongs/h/h5892.md) of [Mô'āḇ](../../strongs/h/h4124.md), which is in the [gᵊḇûl](../../strongs/h/h1366.md) of ['Arnôn](../../strongs/h/h769.md), which is in the [qāṣê](../../strongs/h/h7097.md) [gᵊḇûl](../../strongs/h/h1366.md).

<a name="numbers_22_37"></a>Numbers 22:37

And [Bālāq](../../strongs/h/h1111.md) ['āmar](../../strongs/h/h559.md) unto [Bilʿām](../../strongs/h/h1109.md), Did I not [shalach](../../strongs/h/h7971.md) [shalach](../../strongs/h/h7971.md) unto thee to [qara'](../../strongs/h/h7121.md) thee? wherefore [halak](../../strongs/h/h1980.md) thou not unto me? am I not [yakol](../../strongs/h/h3201.md) ['umnām](../../strongs/h/h552.md) to [kabad](../../strongs/h/h3513.md) thee?

<a name="numbers_22_38"></a>Numbers 22:38

And [Bilʿām](../../strongs/h/h1109.md) ['āmar](../../strongs/h/h559.md) unto [Bālāq](../../strongs/h/h1111.md), Lo, I am [bow'](../../strongs/h/h935.md) unto thee: have I now any [yakol](../../strongs/h/h3201.md) to [dabar](../../strongs/h/h1696.md) [mᵊ'ûmâ](../../strongs/h/h3972.md)? the [dabar](../../strongs/h/h1697.md) that ['Elohiym](../../strongs/h/h430.md) [śûm](../../strongs/h/h7760.md) in my [peh](../../strongs/h/h6310.md), that shall I [dabar](../../strongs/h/h1696.md).

<a name="numbers_22_39"></a>Numbers 22:39

And [Bilʿām](../../strongs/h/h1109.md) [yālaḵ](../../strongs/h/h3212.md) with [Bālāq](../../strongs/h/h1111.md), and they [bow'](../../strongs/h/h935.md) unto [ḥûṣ](../../strongs/h/h2351.md) [qiryaṯ ḥuṣôṯ](../../strongs/h/h7155.md).

<a name="numbers_22_40"></a>Numbers 22:40

And [Bālāq](../../strongs/h/h1111.md) [zabach](../../strongs/h/h2076.md) [bāqār](../../strongs/h/h1241.md) and [tso'n](../../strongs/h/h6629.md), and [shalach](../../strongs/h/h7971.md) to [Bilʿām](../../strongs/h/h1109.md), and to the [śar](../../strongs/h/h8269.md) that were with him.

<a name="numbers_22_41"></a>Numbers 22:41

And it came to pass on the [boqer](../../strongs/h/h1242.md), that [Bālāq](../../strongs/h/h1111.md) [laqach](../../strongs/h/h3947.md) [Bilʿām](../../strongs/h/h1109.md), and [ʿālâ](../../strongs/h/h5927.md) him into the [bāmâ](../../strongs/h/h1116.md) of [bāmôṯ](../../strongs/h/h1120.md), that thence he might [ra'ah](../../strongs/h/h7200.md) the [qāṣê](../../strongs/h/h7097.md) of the ['am](../../strongs/h/h5971.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 21](numbers_21.md) - [Numbers 23](numbers_23.md)