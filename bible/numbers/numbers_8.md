# [Numbers 8](https://www.blueletterbible.org/kjv/num/8)

<a name="numbers_8_1"></a>Numbers 8:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_8_2"></a>Numbers 8:2

[dabar](../../strongs/h/h1696.md) unto ['Ahărôn](../../strongs/h/h175.md) and ['āmar](../../strongs/h/h559.md) unto him, When thou [ʿālâ](../../strongs/h/h5927.md) the [nîr](../../strongs/h/h5216.md), the seven [nîr](../../strongs/h/h5216.md) shall give ['owr](../../strongs/h/h215.md) over [paniym](../../strongs/h/h6440.md) [môl](../../strongs/h/h4136.md) the [mᵊnôrâ](../../strongs/h/h4501.md).

<a name="numbers_8_3"></a>Numbers 8:3

And ['Ahărôn](../../strongs/h/h175.md) ['asah](../../strongs/h/h6213.md) so; he [ʿālâ](../../strongs/h/h5927.md) the [nîr](../../strongs/h/h5216.md) thereof over [môl](../../strongs/h/h4136.md) [paniym](../../strongs/h/h6440.md) the [mᵊnôrâ](../../strongs/h/h4501.md), as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="numbers_8_4"></a>Numbers 8:4

And this [ma'aseh](../../strongs/h/h4639.md) of the [mᵊnôrâ](../../strongs/h/h4501.md) was of [miqšâ](../../strongs/h/h4749.md) [zāhāḇ](../../strongs/h/h2091.md), unto the [yārēḵ](../../strongs/h/h3409.md) thereof, unto the [peraḥ](../../strongs/h/h6525.md) thereof, was [miqšâ](../../strongs/h/h4749.md): according unto the [mar'ê](../../strongs/h/h4758.md) which [Yĕhovah](../../strongs/h/h3068.md) had [ra'ah](../../strongs/h/h7200.md) [Mōshe](../../strongs/h/h4872.md), so he ['asah](../../strongs/h/h6213.md) the [mᵊnôrâ](../../strongs/h/h4501.md).

<a name="numbers_8_5"></a>Numbers 8:5

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_8_6"></a>Numbers 8:6

[laqach](../../strongs/h/h3947.md) the [Lᵊvî](../../strongs/h/h3881.md) from [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and [ṭāhēr](../../strongs/h/h2891.md) them.

<a name="numbers_8_7"></a>Numbers 8:7

And thus shalt thou ['asah](../../strongs/h/h6213.md) unto them, to [ṭāhēr](../../strongs/h/h2891.md) them: [nāzâ](../../strongs/h/h5137.md) [mayim](../../strongs/h/h4325.md) of [chatta'ath](../../strongs/h/h2403.md) upon them, and let them [taʿar](../../strongs/h/h8593.md) ['abar](../../strongs/h/h5674.md) all their [basar](../../strongs/h/h1320.md), and let them [kāḇas](../../strongs/h/h3526.md) their [beḡeḏ](../../strongs/h/h899.md), and so make themselves [ṭāhēr](../../strongs/h/h2891.md).

<a name="numbers_8_8"></a>Numbers 8:8

Then let them [laqach](../../strongs/h/h3947.md) a [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md) with his [minchah](../../strongs/h/h4503.md), even [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), and another [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md) shalt thou [laqach](../../strongs/h/h3947.md) for a [chatta'ath](../../strongs/h/h2403.md).

<a name="numbers_8_9"></a>Numbers 8:9

And thou shalt [qāraḇ](../../strongs/h/h7126.md) the [Lᵊvî](../../strongs/h/h3881.md) [paniym](../../strongs/h/h6440.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md): and thou shalt [qāhal](../../strongs/h/h6950.md) the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [qāhal](../../strongs/h/h6950.md):

<a name="numbers_8_10"></a>Numbers 8:10

And thou shalt [qāraḇ](../../strongs/h/h7126.md) the [Lᵊvî](../../strongs/h/h3881.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) shall [camak](../../strongs/h/h5564.md) their [yad](../../strongs/h/h3027.md) upon the [Lᵊvî](../../strongs/h/h3881.md):

<a name="numbers_8_11"></a>Numbers 8:11

And ['Ahărôn](../../strongs/h/h175.md) shall [nûp̄](../../strongs/h/h5130.md) the [Lᵊvî](../../strongs/h/h3881.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) for a [tᵊnûp̄â](../../strongs/h/h8573.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they may ['abad](../../strongs/h/h5647.md) the [ʿăḇōḏâ](../../strongs/h/h5656.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_8_12"></a>Numbers 8:12

And the [Lᵊvî](../../strongs/h/h3881.md) shall [camak](../../strongs/h/h5564.md) their [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [par](../../strongs/h/h6499.md): and thou shalt ['asah](../../strongs/h/h6213.md) the one for a [chatta'ath](../../strongs/h/h2403.md), and the other for an [ʿōlâ](../../strongs/h/h5930.md), unto [Yĕhovah](../../strongs/h/h3068.md), to make a [kāp̄ar](../../strongs/h/h3722.md) for the [Lᵊvî](../../strongs/h/h3881.md).

<a name="numbers_8_13"></a>Numbers 8:13

And thou shalt ['amad](../../strongs/h/h5975.md) the [Lᵊvî](../../strongs/h/h3881.md) [paniym](../../strongs/h/h6440.md) ['Ahărôn](../../strongs/h/h175.md), and [paniym](../../strongs/h/h6440.md) his [ben](../../strongs/h/h1121.md), and [nûp̄](../../strongs/h/h5130.md) them for a [tᵊnûp̄â](../../strongs/h/h8573.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_8_14"></a>Numbers 8:14

Thus shalt thou [bāḏal](../../strongs/h/h914.md) the [Lᵊvî](../../strongs/h/h3881.md) from [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): and the [Lᵊvî](../../strongs/h/h3881.md) shall be mine.

<a name="numbers_8_15"></a>Numbers 8:15

And ['aḥar](../../strongs/h/h310.md) that shall the [Lᵊvî](../../strongs/h/h3881.md) [bow'](../../strongs/h/h935.md) to ['abad](../../strongs/h/h5647.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md): and thou shalt [ṭāhēr](../../strongs/h/h2891.md) them, and [nûp̄](../../strongs/h/h5130.md) them for a [tᵊnûp̄â](../../strongs/h/h8573.md).

<a name="numbers_8_16"></a>Numbers 8:16

For they are wholly [nathan](../../strongs/h/h5414.md) unto me from [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); instead of such as [peṭer](../../strongs/h/h6363.md) every [reḥem](../../strongs/h/h7358.md), even instead of the [bᵊḵôr](../../strongs/h/h1060.md) of all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), have I [laqach](../../strongs/h/h3947.md) them unto me.

<a name="numbers_8_17"></a>Numbers 8:17

For all the [bᵊḵôr](../../strongs/h/h1060.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) are mine, both ['adam](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md): on the [yowm](../../strongs/h/h3117.md) that I [nakah](../../strongs/h/h5221.md) every [bᵊḵôr](../../strongs/h/h1060.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) I [qadash](../../strongs/h/h6942.md) them for myself.

<a name="numbers_8_18"></a>Numbers 8:18

And I have [laqach](../../strongs/h/h3947.md) the [Lᵊvî](../../strongs/h/h3881.md) for all the [bᵊḵôr](../../strongs/h/h1060.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_8_19"></a>Numbers 8:19

And I have [nathan](../../strongs/h/h5414.md) the [Lᵊvî](../../strongs/h/h3881.md) as a [nathan](../../strongs/h/h5414.md) to ['Ahărôn](../../strongs/h/h175.md) and to his [ben](../../strongs/h/h1121.md) from [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), to ['abad](../../strongs/h/h5647.md) the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and to make a [kāp̄ar](../../strongs/h/h3722.md) for the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): that there be no [neḡep̄](../../strongs/h/h5063.md) among the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), when the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāḡaš](../../strongs/h/h5066.md) unto the [qodesh](../../strongs/h/h6944.md).

<a name="numbers_8_20"></a>Numbers 8:20

And [Mōshe](../../strongs/h/h4872.md), and ['Ahărôn](../../strongs/h/h175.md), and all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), did to the [Lᵊvî](../../strongs/h/h3881.md) according unto all that [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md) concerning the [Lᵊvî](../../strongs/h/h3881.md), so ['asah](../../strongs/h/h6213.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) unto them.

<a name="numbers_8_21"></a>Numbers 8:21

And the [Lᵊvî](../../strongs/h/h3881.md) were [chata'](../../strongs/h/h2398.md), and they [kāḇas](../../strongs/h/h3526.md) their [beḡeḏ](../../strongs/h/h899.md); and ['Ahărôn](../../strongs/h/h175.md) [nûp̄](../../strongs/h/h5130.md) them as a [tᵊnûp̄â](../../strongs/h/h8573.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); and ['Ahărôn](../../strongs/h/h175.md) made a [kāp̄ar](../../strongs/h/h3722.md) for them to [ṭāhēr](../../strongs/h/h2891.md) them.

<a name="numbers_8_22"></a>Numbers 8:22

And ['aḥar](../../strongs/h/h310.md) that [bow'](../../strongs/h/h935.md) the [Lᵊvî](../../strongs/h/h3881.md) in to ['abad](../../strongs/h/h5647.md) their [ʿăḇōḏâ](../../strongs/h/h5656.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) [paniym](../../strongs/h/h6440.md) ['Ahărôn](../../strongs/h/h175.md), and [paniym](../../strongs/h/h6440.md) his [ben](../../strongs/h/h1121.md): as [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md) concerning the [Lᵊvî](../../strongs/h/h3881.md), so ['asah](../../strongs/h/h6213.md) they unto them.

<a name="numbers_8_23"></a>Numbers 8:23

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_8_24"></a>Numbers 8:24

This is it that belongeth unto the [Lᵊvî](../../strongs/h/h3881.md): from twenty and five [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md) they shall [bow'](../../strongs/h/h935.md) to [ṣᵊḇā'](../../strongs/h/h6633.md) [tsaba'](../../strongs/h/h6635.md) upon the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md):

<a name="numbers_8_25"></a>Numbers 8:25

And from the [ben](../../strongs/h/h1121.md) of fifty [šānâ](../../strongs/h/h8141.md) they shall [shuwb](../../strongs/h/h7725.md) [tsaba'](../../strongs/h/h6635.md) upon the [ʿăḇōḏâ](../../strongs/h/h5656.md) thereof, and shall ['abad](../../strongs/h/h5647.md) no more:

<a name="numbers_8_26"></a>Numbers 8:26

But shall [sharath](../../strongs/h/h8334.md) with their ['ach](../../strongs/h/h251.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), to [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md), and shall ['abad](../../strongs/h/h5647.md) no [ʿăḇōḏâ](../../strongs/h/h5656.md). Thus shalt thou ['asah](../../strongs/h/h6213.md) unto the [Lᵊvî](../../strongs/h/h3881.md) touching their [mišmereṯ](../../strongs/h/h4931.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 7](numbers_7.md) - [Numbers 9](numbers_9.md)