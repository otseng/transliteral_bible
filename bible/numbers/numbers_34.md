# [Numbers 34](https://www.blueletterbible.org/kjv/num/34)

<a name="numbers_34_1"></a>Numbers 34:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_34_2"></a>Numbers 34:2

[tsavah](../../strongs/h/h6680.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, When ye [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md); (this is the ['erets](../../strongs/h/h776.md) that shall [naphal](../../strongs/h/h5307.md) unto you for a [nachalah](../../strongs/h/h5159.md), even the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md) with the [gᵊḇûlâ](../../strongs/h/h1367.md) thereof:)

<a name="numbers_34_3"></a>Numbers 34:3

Then your [neḡeḇ](../../strongs/h/h5045.md) [pē'â](../../strongs/h/h6285.md) shall be from the [midbar](../../strongs/h/h4057.md) of [Ṣin](../../strongs/h/h6790.md) along by the [yad](../../strongs/h/h3027.md) of ['Ĕḏōm](../../strongs/h/h123.md), and your [neḡeḇ](../../strongs/h/h5045.md) [gᵊḇûl](../../strongs/h/h1366.md) shall be the outmost [qāṣê](../../strongs/h/h7097.md) of the [melaḥ](../../strongs/h/h4417.md) [yam](../../strongs/h/h3220.md) [qeḏem](../../strongs/h/h6924.md):

<a name="numbers_34_4"></a>Numbers 34:4

And your [gᵊḇûl](../../strongs/h/h1366.md) shall [cabab](../../strongs/h/h5437.md) from the [neḡeḇ](../../strongs/h/h5045.md) to the [maʿălê](../../strongs/h/h4608.md) of [MaʿĂlê ʿAqrabîm](../../strongs/h/h4610.md), and ['abar](../../strongs/h/h5674.md) to [Ṣin](../../strongs/h/h6790.md): and the [tôṣā'ôṯ](../../strongs/h/h8444.md) thereof shall be from the [neḡeḇ](../../strongs/h/h5045.md) to [Qāḏēš Barnēaʿ](../../strongs/h/h6947.md), and shall [yāṣā'](../../strongs/h/h3318.md) to [Ḥăṣar-'Adār](../../strongs/h/h2692.md), and ['abar](../../strongs/h/h5674.md) to [ʿAṣmôn](../../strongs/h/h6111.md):

<a name="numbers_34_5"></a>Numbers 34:5

And the [gᵊḇûl](../../strongs/h/h1366.md) shall [cabab](../../strongs/h/h5437.md) from [ʿAṣmôn](../../strongs/h/h6111.md) unto the [nachal](../../strongs/h/h5158.md) of [Mitsrayim](../../strongs/h/h4714.md), and the [tôṣā'ôṯ](../../strongs/h/h8444.md) of it shall be at the [yam](../../strongs/h/h3220.md).

<a name="numbers_34_6"></a>Numbers 34:6

And as for the [yam](../../strongs/h/h3220.md) [gᵊḇûl](../../strongs/h/h1366.md), ye shall even have the [gadowl](../../strongs/h/h1419.md) [yam](../../strongs/h/h3220.md) for a [gᵊḇûl](../../strongs/h/h1366.md): this shall be your [yam](../../strongs/h/h3220.md) [gᵊḇûl](../../strongs/h/h1366.md).

<a name="numbers_34_7"></a>Numbers 34:7

And this shall be your [ṣāp̄ôn](../../strongs/h/h6828.md) [gᵊḇûl](../../strongs/h/h1366.md): from the [gadowl](../../strongs/h/h1419.md) [yam](../../strongs/h/h3220.md) ye shall [tā'â](../../strongs/h/h8376.md) for you [har](../../strongs/h/h2022.md) [Hōr](../../strongs/h/h2023.md):

<a name="numbers_34_8"></a>Numbers 34:8

From [har](../../strongs/h/h2022.md) [Hōr](../../strongs/h/h2023.md) ye shall [tā'â](../../strongs/h/h8376.md) your border unto the [bow'](../../strongs/h/h935.md) of [Ḥămāṯ](../../strongs/h/h2574.md); and the [tôṣā'ôṯ](../../strongs/h/h8444.md) of the [gᵊḇûl](../../strongs/h/h1366.md) shall be to [Ṣᵊḏāḏ](../../strongs/h/h6657.md):

<a name="numbers_34_9"></a>Numbers 34:9

And the [gᵊḇûl](../../strongs/h/h1366.md) shall [yāṣā'](../../strongs/h/h3318.md) to [Zip̄Rôn](../../strongs/h/h2202.md), and the [tôṣā'ôṯ](../../strongs/h/h8444.md) of it shall be at [Ḥăṣar ʿÊnān](../../strongs/h/h2704.md): this shall be your [ṣāp̄ôn](../../strongs/h/h6828.md) [gᵊḇûl](../../strongs/h/h1366.md).

<a name="numbers_34_10"></a>Numbers 34:10

And ye shall ['āvâ](../../strongs/h/h184.md) your [qeḏem](../../strongs/h/h6924.md) [gᵊḇûl](../../strongs/h/h1366.md) from [Ḥăṣar ʿÊnān](../../strongs/h/h2704.md) to [Šᵊp̄Ām](../../strongs/h/h8221.md):

<a name="numbers_34_11"></a>Numbers 34:11

And the [gᵊḇûl](../../strongs/h/h1366.md) shall [yarad](../../strongs/h/h3381.md) from [Šᵊp̄Ām](../../strongs/h/h8221.md) to [Riḇlâ](../../strongs/h/h7247.md), on the [qeḏem](../../strongs/h/h6924.md) of [ʿAyin](../../strongs/h/h5871.md); and the [gᵊḇûl](../../strongs/h/h1366.md) shall [yarad](../../strongs/h/h3381.md), and shall [māḥâ](../../strongs/h/h4229.md) unto the [kāṯēp̄](../../strongs/h/h3802.md) of the [yam](../../strongs/h/h3220.md) of [Kinnᵊrôṯ](../../strongs/h/h3672.md) [qeḏem](../../strongs/h/h6924.md):

<a name="numbers_34_12"></a>Numbers 34:12

And the [gᵊḇûl](../../strongs/h/h1366.md) shall [yarad](../../strongs/h/h3381.md) to [Yardēn](../../strongs/h/h3383.md), and the [tôṣā'ôṯ](../../strongs/h/h8444.md) of it shall be at the [melaḥ](../../strongs/h/h4417.md) [yam](../../strongs/h/h3220.md): this shall be your ['erets](../../strongs/h/h776.md) with the [gᵊḇûlâ](../../strongs/h/h1367.md) thereof [cabiyb](../../strongs/h/h5439.md).

<a name="numbers_34_13"></a>Numbers 34:13

And [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), This is the ['erets](../../strongs/h/h776.md) which ye shall [nāḥal](../../strongs/h/h5157.md) by [gôrāl](../../strongs/h/h1486.md), which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) to [nathan](../../strongs/h/h5414.md) unto the nine [maṭṭê](../../strongs/h/h4294.md), and to the [ḥēṣî](../../strongs/h/h2677.md) [maṭṭê](../../strongs/h/h4294.md):

<a name="numbers_34_14"></a>Numbers 34:14

For the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēnî](../../strongs/h/h7206.md) according to the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), and the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Gāḏî](../../strongs/h/h1425.md) according to the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), have [laqach](../../strongs/h/h3947.md); and [ḥēṣî](../../strongs/h/h2677.md) the [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md) have [laqach](../../strongs/h/h3947.md) their [nachalah](../../strongs/h/h5159.md):

<a name="numbers_34_15"></a>Numbers 34:15

The two [maṭṭê](../../strongs/h/h4294.md) and the [ḥēṣî](../../strongs/h/h2677.md) [maṭṭê](../../strongs/h/h4294.md) have [laqach](../../strongs/h/h3947.md) their [nachalah](../../strongs/h/h5159.md) on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) near [Yᵊrēḥô](../../strongs/h/h3405.md) [qeḏem](../../strongs/h/h6924.md), toward the [mizrach](../../strongs/h/h4217.md).

<a name="numbers_34_16"></a>Numbers 34:16

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_34_17"></a>Numbers 34:17

These are the [shem](../../strongs/h/h8034.md) of the ['enowsh](../../strongs/h/h582.md) which shall [nāḥal](../../strongs/h/h5157.md) the ['erets](../../strongs/h/h776.md) unto you: ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md).

<a name="numbers_34_18"></a>Numbers 34:18

And ye shall [laqach](../../strongs/h/h3947.md) one [nāśî'](../../strongs/h/h5387.md) of every [maṭṭê](../../strongs/h/h4294.md), to [nāḥal](../../strongs/h/h5157.md) the ['erets](../../strongs/h/h776.md) by [nāḥal](../../strongs/h/h5157.md).

<a name="numbers_34_19"></a>Numbers 34:19

And the [shem](../../strongs/h/h8034.md) of the ['enowsh](../../strongs/h/h582.md) are these: Of the [maṭṭê](../../strongs/h/h4294.md) of [Yehuwdah](../../strongs/h/h3063.md), [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md).

<a name="numbers_34_20"></a>Numbers 34:20

And of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md), [Šᵊmû'Ēl](../../strongs/h/h8050.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmîhûḏ](../../strongs/h/h5989.md).

<a name="numbers_34_21"></a>Numbers 34:21

Of the [maṭṭê](../../strongs/h/h4294.md) of [Binyāmîn](../../strongs/h/h1144.md), ['Ĕlîḏāḏ](../../strongs/h/h449.md) the [ben](../../strongs/h/h1121.md) of [Kislôn](../../strongs/h/h3692.md).

<a name="numbers_34_22"></a>Numbers 34:22

And the [nāśî'](../../strongs/h/h5387.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md), [Buqqî](../../strongs/h/h1231.md) the [ben](../../strongs/h/h1121.md) of [Yāḡlî](../../strongs/h/h3020.md).

<a name="numbers_34_23"></a>Numbers 34:23

The [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md), for the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md), [Ḥannî'Ēl](../../strongs/h/h2592.md) the [ben](../../strongs/h/h1121.md) of ['Ēp̄Ōḏ](../../strongs/h/h641.md).

<a name="numbers_34_24"></a>Numbers 34:24

And the [nāśî'](../../strongs/h/h5387.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md), [Qᵊmû'ēl](../../strongs/h/h7055.md) the [ben](../../strongs/h/h1121.md) of [Šip̄Ṭān](../../strongs/h/h8204.md).

<a name="numbers_34_25"></a>Numbers 34:25

And the [nāśî'](../../strongs/h/h5387.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md), ['Ĕlîṣāp̄ān](../../strongs/h/h469.md) the [ben](../../strongs/h/h1121.md) of [Parnāḵ](../../strongs/h/h6535.md).

<a name="numbers_34_26"></a>Numbers 34:26

And the [nāśî'](../../strongs/h/h5387.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yiśśāśḵār](../../strongs/h/h3485.md), [Palṭî'Ēl](../../strongs/h/h6409.md) the [ben](../../strongs/h/h1121.md) of [ʿAzzān](../../strongs/h/h5821.md).

<a name="numbers_34_27"></a>Numbers 34:27

And the [nāśî'](../../strongs/h/h5387.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of ['Āšēr](../../strongs/h/h836.md), ['Ăḥîhûḏ](../../strongs/h/h282.md) the [ben](../../strongs/h/h1121.md) of [Šᵊlōmî](../../strongs/h/h8015.md).

<a name="numbers_34_28"></a>Numbers 34:28

And the [nāśî'](../../strongs/h/h5387.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Nap̄tālî](../../strongs/h/h5321.md), [Pᵊḏah'Ēl](../../strongs/h/h6300.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmîhûḏ](../../strongs/h/h5989.md).

<a name="numbers_34_29"></a>Numbers 34:29

These are they whom [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) to divide the [nāḥal](../../strongs/h/h5157.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 33](numbers_33.md) - [Numbers 35](numbers_35.md)