# [Numbers 10](https://www.blueletterbible.org/kjv/num/10)

<a name="numbers_10_1"></a>Numbers 10:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_10_2"></a>Numbers 10:2

['asah](../../strongs/h/h6213.md) thee two [ḥăṣōṣrâ](../../strongs/h/h2689.md) of [keceph](../../strongs/h/h3701.md); of a [miqšâ](../../strongs/h/h4749.md) shalt thou ['asah](../../strongs/h/h6213.md) them: that thou mayest [hayah](../../strongs/h/h1961.md) them for the [miqrā'](../../strongs/h/h4744.md) of the ['edah](../../strongs/h/h5712.md), and for the [massāʿ](../../strongs/h/h4550.md) of the [maḥănê](../../strongs/h/h4264.md).

<a name="numbers_10_3"></a>Numbers 10:3

And when they shall [tāqaʿ](../../strongs/h/h8628.md) with them, all the ['edah](../../strongs/h/h5712.md) shall [yāʿaḏ](../../strongs/h/h3259.md) themselves to thee at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="numbers_10_4"></a>Numbers 10:4

And if they [tāqaʿ](../../strongs/h/h8628.md) but with one, then the [nāśî'](../../strongs/h/h5387.md), which are [ro'sh](../../strongs/h/h7218.md) of the thousands of [Yisra'el](../../strongs/h/h3478.md), shall [yāʿaḏ](../../strongs/h/h3259.md) themselves unto thee.

<a name="numbers_10_5"></a>Numbers 10:5

When ye [tāqaʿ](../../strongs/h/h8628.md) a [tᵊrûʿâ](../../strongs/h/h8643.md), then the [maḥănê](../../strongs/h/h4264.md) that [ḥānâ](../../strongs/h/h2583.md) on the [qeḏem](../../strongs/h/h6924.md) shall [nāsaʿ](../../strongs/h/h5265.md).

<a name="numbers_10_6"></a>Numbers 10:6

When ye [tāqaʿ](../../strongs/h/h8628.md) a [tᵊrûʿâ](../../strongs/h/h8643.md) the second time, then the [maḥănê](../../strongs/h/h4264.md) that [ḥānâ](../../strongs/h/h2583.md) on the [têmān](../../strongs/h/h8486.md) shall [nāsaʿ](../../strongs/h/h5265.md): they shall [tāqaʿ](../../strongs/h/h8628.md) a [tᵊrûʿâ](../../strongs/h/h8643.md) for their [massāʿ](../../strongs/h/h4550.md).

<a name="numbers_10_7"></a>Numbers 10:7

But when the [qāhēl](../../strongs/h/h6951.md) is to be [qāhal](../../strongs/h/h6950.md), ye shall [tāqaʿ](../../strongs/h/h8628.md), but ye shall not [rûaʿ](../../strongs/h/h7321.md).

<a name="numbers_10_8"></a>Numbers 10:8

And the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), the [kōhēn](../../strongs/h/h3548.md), shall [tāqaʿ](../../strongs/h/h8628.md) with the [ḥăṣōṣrâ](../../strongs/h/h2689.md); and they shall be to you for a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) throughout your [dôr](../../strongs/h/h1755.md).

<a name="numbers_10_9"></a>Numbers 10:9

And if ye [bow'](../../strongs/h/h935.md) to [milḥāmâ](../../strongs/h/h4421.md) in your ['erets](../../strongs/h/h776.md) [tsar](../../strongs/h/h6862.md) the [tsarar](../../strongs/h/h6887.md), then ye shall [rûaʿ](../../strongs/h/h7321.md) with the [ḥăṣōṣrâ](../../strongs/h/h2689.md); and ye shall be [zakar](../../strongs/h/h2142.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and ye shall be [yasha'](../../strongs/h/h3467.md) from your ['oyeb](../../strongs/h/h341.md).

<a name="numbers_10_10"></a>Numbers 10:10

Also in the [yowm](../../strongs/h/h3117.md) of your [simchah](../../strongs/h/h8057.md), and in your [môʿēḏ](../../strongs/h/h4150.md), and in the [ro'sh](../../strongs/h/h7218.md) of your [ḥōḏeš](../../strongs/h/h2320.md), ye shall [tāqaʿ](../../strongs/h/h8628.md) with the [ḥăṣōṣrâ](../../strongs/h/h2689.md) over your an [ʿōlâ](../../strongs/h/h5930.md), and over the [zebach](../../strongs/h/h2077.md) of your [šelem](../../strongs/h/h8002.md); that they may be to you for a [zikārôn](../../strongs/h/h2146.md) [paniym](../../strongs/h/h6440.md) your ['Elohiym](../../strongs/h/h430.md): I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="numbers_10_11"></a>Numbers 10:11

And it came to pass on the twentieth of the second [ḥōḏeš](../../strongs/h/h2320.md), in the second [šānâ](../../strongs/h/h8141.md), that the [ʿānān](../../strongs/h/h6051.md) was [ʿālâ](../../strongs/h/h5927.md) from off the [miškān](../../strongs/h/h4908.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md).

<a name="numbers_10_12"></a>Numbers 10:12

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāsaʿ](../../strongs/h/h5265.md) their [massāʿ](../../strongs/h/h4550.md) out of the [midbar](../../strongs/h/h4057.md) of [Sînay](../../strongs/h/h5514.md); and the [ʿānān](../../strongs/h/h6051.md) [shakan](../../strongs/h/h7931.md) in the [midbar](../../strongs/h/h4057.md) of [Pā'rān](../../strongs/h/h6290.md).

<a name="numbers_10_13"></a>Numbers 10:13

And they [ri'šôn](../../strongs/h/h7223.md) took their [nāsaʿ](../../strongs/h/h5265.md) according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

<a name="numbers_10_14"></a>Numbers 10:14

In the [ri'šôn](../../strongs/h/h7223.md) [nāsaʿ](../../strongs/h/h5265.md) the [deḡel](../../strongs/h/h1714.md) of the [maḥănê](../../strongs/h/h4264.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) according to their [tsaba'](../../strongs/h/h6635.md): and over his host was [Naḥšôn](../../strongs/h/h5177.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmînāḏāḇ](../../strongs/h/h5992.md).

<a name="numbers_10_15"></a>Numbers 10:15

And over the [tsaba'](../../strongs/h/h6635.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yiśśāśḵār](../../strongs/h/h3485.md) was [Nᵊṯan'ēl](../../strongs/h/h5417.md) the [ben](../../strongs/h/h1121.md) of [ṣûʿar](../../strongs/h/h6686.md).

<a name="numbers_10_16"></a>Numbers 10:16

And over the [tsaba'](../../strongs/h/h6635.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md) was ['Ĕlî'āḇ](../../strongs/h/h446.md) the [ben](../../strongs/h/h1121.md) of [ḥēlōn](../../strongs/h/h2497.md).

<a name="numbers_10_17"></a>Numbers 10:17

And the [miškān](../../strongs/h/h4908.md) was [yarad](../../strongs/h/h3381.md); and the [ben](../../strongs/h/h1121.md) of [Gēršôn](../../strongs/h/h1648.md) and the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md) [nāsaʿ](../../strongs/h/h5265.md), [nasa'](../../strongs/h/h5375.md) the [miškān](../../strongs/h/h4908.md).

<a name="numbers_10_18"></a>Numbers 10:18

And the [deḡel](../../strongs/h/h1714.md) of the [maḥănê](../../strongs/h/h4264.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) [nāsaʿ](../../strongs/h/h5265.md) according to their [tsaba'](../../strongs/h/h6635.md): and over his [tsaba'](../../strongs/h/h6635.md) was ['ĕlîṣûr](../../strongs/h/h468.md) the [ben](../../strongs/h/h1121.md) of [šᵊḏê'ûr](../../strongs/h/h7707.md).

<a name="numbers_10_19"></a>Numbers 10:19

And over the [tsaba'](../../strongs/h/h6635.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md) was [šᵊlumî'ēl](../../strongs/h/h8017.md) the [ben](../../strongs/h/h1121.md) of [ṣûrîšaday](../../strongs/h/h6701.md).

<a name="numbers_10_20"></a>Numbers 10:20

And over the [tsaba'](../../strongs/h/h6635.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) was ['elyāsāp̄](../../strongs/h/h460.md) the [ben](../../strongs/h/h1121.md) of [dᵊʿû'ēl](../../strongs/h/h1845.md).

<a name="numbers_10_21"></a>Numbers 10:21

And the [Qᵊhāṯî](../../strongs/h/h6956.md) [nāsaʿ](../../strongs/h/h5265.md), [nasa'](../../strongs/h/h5375.md) the [miqdash](../../strongs/h/h4720.md): and the other did [quwm](../../strongs/h/h6965.md) the [miškān](../../strongs/h/h4908.md) against they [bow'](../../strongs/h/h935.md).

<a name="numbers_10_22"></a>Numbers 10:22

And the [deḡel](../../strongs/h/h1714.md) of the [maḥănê](../../strongs/h/h4264.md) of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md) [nāsaʿ](../../strongs/h/h5265.md) according to their [tsaba'](../../strongs/h/h6635.md): and over his [tsaba'](../../strongs/h/h6635.md) was ['Ĕlîšāmāʿ](../../strongs/h/h476.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmîhûḏ](../../strongs/h/h5989.md).

<a name="numbers_10_23"></a>Numbers 10:23

And over the [tsaba'](../../strongs/h/h6635.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md) was [gamlî'ēl](../../strongs/h/h1583.md) the [ben](../../strongs/h/h1121.md) of [pᵊḏâṣûr](../../strongs/h/h6301.md).

<a name="numbers_10_24"></a>Numbers 10:24

And over the [tsaba'](../../strongs/h/h6635.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) was ['ăḇîḏān](../../strongs/h/h27.md) the [ben](../../strongs/h/h1121.md) of [giḏʿōnî](../../strongs/h/h1441.md).

<a name="numbers_10_25"></a>Numbers 10:25

And the [deḡel](../../strongs/h/h1714.md) of the [maḥănê](../../strongs/h/h4264.md) of the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md) [nāsaʿ](../../strongs/h/h5265.md), which was the ['āsap̄](../../strongs/h/h622.md) of all the [maḥănê](../../strongs/h/h4264.md) throughout their [tsaba'](../../strongs/h/h6635.md): and over his [tsaba'](../../strongs/h/h6635.md) was ['ăḥîʿezer](../../strongs/h/h295.md) the [ben](../../strongs/h/h1121.md) of [ʿammîšaḏay](../../strongs/h/h5996.md).

<a name="numbers_10_26"></a>Numbers 10:26

And over the [tsaba'](../../strongs/h/h6635.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of ['Āšēr](../../strongs/h/h836.md) was [Paḡʿî'ēl](../../strongs/h/h6295.md) the [ben](../../strongs/h/h1121.md) of [ʿāḵrān](../../strongs/h/h5918.md).

<a name="numbers_10_27"></a>Numbers 10:27

And over the [tsaba'](../../strongs/h/h6635.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Nap̄tālî](../../strongs/h/h5321.md) was ['ăḥîraʿ](../../strongs/h/h299.md) the [ben](../../strongs/h/h1121.md) of [ʿênān](../../strongs/h/h5881.md).

<a name="numbers_10_28"></a>Numbers 10:28

Thus were the [massāʿ](../../strongs/h/h4550.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) according to their [tsaba'](../../strongs/h/h6635.md), when they [nāsaʿ](../../strongs/h/h5265.md).

<a name="numbers_10_29"></a>Numbers 10:29

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto [Ḥōḇāḇ](../../strongs/h/h2246.md), the [ben](../../strongs/h/h1121.md) of [Rᵊʿû'ēl](../../strongs/h/h7467.md) the [Miḏyānî](../../strongs/h/h4084.md), [Mōshe](../../strongs/h/h4872.md) [ḥāṯan](../../strongs/h/h2859.md), We are [nāsaʿ](../../strongs/h/h5265.md) unto the [maqowm](../../strongs/h/h4725.md) of which [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), I will [nathan](../../strongs/h/h5414.md) it you: [yālaḵ](../../strongs/h/h3212.md) thou with us, and we will do thee [ṭôḇ](../../strongs/h/h2895.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) [towb](../../strongs/h/h2896.md) concerning [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_10_30"></a>Numbers 10:30

And he ['āmar](../../strongs/h/h559.md) unto him, I will not [yālaḵ](../../strongs/h/h3212.md); but I will [yālaḵ](../../strongs/h/h3212.md) to mine own ['erets](../../strongs/h/h776.md), and to my [môleḏeṯ](../../strongs/h/h4138.md).

<a name="numbers_10_31"></a>Numbers 10:31

And he ['āmar](../../strongs/h/h559.md), ['azab](../../strongs/h/h5800.md) us not, I pray thee; forasmuch as thou [kēn](../../strongs/h/h3651.md) [yada'](../../strongs/h/h3045.md) how we are to [ḥānâ](../../strongs/h/h2583.md) in the [midbar](../../strongs/h/h4057.md), and thou mayest be to us instead of ['ayin](../../strongs/h/h5869.md).

<a name="numbers_10_32"></a>Numbers 10:32

And it shall be, if thou [yālaḵ](../../strongs/h/h3212.md) with us, yea, it shall be, that what [towb](../../strongs/h/h2896.md) [Yĕhovah](../../strongs/h/h3068.md) shall [yatab](../../strongs/h/h3190.md) unto us, the same will we [ṭôḇ](../../strongs/h/h2895.md) unto thee.

<a name="numbers_10_33"></a>Numbers 10:33

And they [nāsaʿ](../../strongs/h/h5265.md) from the [har](../../strongs/h/h2022.md) of [Yĕhovah](../../strongs/h/h3068.md) three [yowm](../../strongs/h/h3117.md) [derek](../../strongs/h/h1870.md): and the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) [nāsaʿ](../../strongs/h/h5265.md) [paniym](../../strongs/h/h6440.md) them in the three [yowm](../../strongs/h/h3117.md) [derek](../../strongs/h/h1870.md), to [tûr](../../strongs/h/h8446.md) a [mᵊnûḥâ](../../strongs/h/h4496.md) for them.

<a name="numbers_10_34"></a>Numbers 10:34

And the [ʿānān](../../strongs/h/h6051.md) of [Yĕhovah](../../strongs/h/h3068.md) was upon them by [yômām](../../strongs/h/h3119.md), when they [nāsaʿ](../../strongs/h/h5265.md) of the [maḥănê](../../strongs/h/h4264.md).

<a name="numbers_10_35"></a>Numbers 10:35

And it came to pass, when the ['ārôn](../../strongs/h/h727.md) [nāsaʿ](../../strongs/h/h5265.md), that [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md), [Yĕhovah](../../strongs/h/h3068.md), and let thine ['oyeb](../../strongs/h/h341.md) be [puwts](../../strongs/h/h6327.md); and let them that [sane'](../../strongs/h/h8130.md) thee [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) thee.

<a name="numbers_10_36"></a>Numbers 10:36

And when it [nuwach](../../strongs/h/h5117.md), he ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md), [Yĕhovah](../../strongs/h/h3068.md), unto the [rᵊḇāḇâ](../../strongs/h/h7233.md) ['elep̄](../../strongs/h/h505.md) of [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 9](numbers_9.md) - [Numbers 11](numbers_11.md)