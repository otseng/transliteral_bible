# [Numbers 19](https://www.blueletterbible.org/kjv/num/19)

<a name="numbers_19_1"></a>Numbers 19:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) and unto ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_19_2"></a>Numbers 19:2

This is the [chuqqah](../../strongs/h/h2708.md) of the [towrah](../../strongs/h/h8451.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [tsavah](../../strongs/h/h6680.md), ['āmar](../../strongs/h/h559.md), [dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they [laqach](../../strongs/h/h3947.md) thee an ['āḏōm](../../strongs/h/h122.md) [pārâ](../../strongs/h/h6510.md) [tamiym](../../strongs/h/h8549.md), wherein is no [mᵊ'ûm](../../strongs/h/h3971.md), and upon which never [ʿālâ](../../strongs/h/h5927.md) [ʿōl](../../strongs/h/h5923.md):

<a name="numbers_19_3"></a>Numbers 19:3

And ye shall [nathan](../../strongs/h/h5414.md) her unto ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), that he may [yāṣā'](../../strongs/h/h3318.md) her [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md), and one shall [šāḥaṭ](../../strongs/h/h7819.md) her [paniym](../../strongs/h/h6440.md):

<a name="numbers_19_4"></a>Numbers 19:4

And ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) of her [dam](../../strongs/h/h1818.md) with his ['etsba'](../../strongs/h/h676.md), and [nāzâ](../../strongs/h/h5137.md) of her [dam](../../strongs/h/h1818.md) [nōḵaḥ](../../strongs/h/h5227.md) [paniym](../../strongs/h/h6440.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) seven times:

<a name="numbers_19_5"></a>Numbers 19:5

And one shall [śārap̄](../../strongs/h/h8313.md) the [pārâ](../../strongs/h/h6510.md) in his ['ayin](../../strongs/h/h5869.md); her ['owr](../../strongs/h/h5785.md), and her [basar](../../strongs/h/h1320.md), and her [dam](../../strongs/h/h1818.md), with her [pereš](../../strongs/h/h6569.md), shall he [śārap̄](../../strongs/h/h8313.md):

<a name="numbers_19_6"></a>Numbers 19:6

And the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) ['erez](../../strongs/h/h730.md) ['ets](../../strongs/h/h6086.md), and ['ēzôḇ](../../strongs/h/h231.md), and [tôlāʿ](../../strongs/h/h8438.md) [šānî](../../strongs/h/h8144.md), and [shalak](../../strongs/h/h7993.md) it into the [tavek](../../strongs/h/h8432.md) of the [śᵊrēp̄â](../../strongs/h/h8316.md) of the [pārâ](../../strongs/h/h6510.md).

<a name="numbers_19_7"></a>Numbers 19:7

Then the [kōhēn](../../strongs/h/h3548.md) shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and he shall [rāḥaṣ](../../strongs/h/h7364.md) his [basar](../../strongs/h/h1320.md) in [mayim](../../strongs/h/h4325.md), and ['aḥar](../../strongs/h/h310.md) he shall [bow'](../../strongs/h/h935.md) into the [maḥănê](../../strongs/h/h4264.md), and the [kōhēn](../../strongs/h/h3548.md) shall be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="numbers_19_8"></a>Numbers 19:8

And he that [śārap̄](../../strongs/h/h8313.md) her shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md) in [mayim](../../strongs/h/h4325.md), and [rāḥaṣ](../../strongs/h/h7364.md) his [basar](../../strongs/h/h1320.md) in [mayim](../../strongs/h/h4325.md), and shall be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="numbers_19_9"></a>Numbers 19:9

And an ['iysh](../../strongs/h/h376.md) that is [tahowr](../../strongs/h/h2889.md) shall ['āsap̄](../../strongs/h/h622.md) up the ['ēp̄er](../../strongs/h/h665.md) of the [pārâ](../../strongs/h/h6510.md), and [yānaḥ](../../strongs/h/h3240.md) them [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md) in a [tahowr](../../strongs/h/h2889.md) [maqowm](../../strongs/h/h4725.md), and it shall be [mišmereṯ](../../strongs/h/h4931.md) for the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) for a [mayim](../../strongs/h/h4325.md) of [nidâ](../../strongs/h/h5079.md): it is a purification for [chatta'ath](../../strongs/h/h2403.md).

<a name="numbers_19_10"></a>Numbers 19:10

And he that ['āsap̄](../../strongs/h/h622.md) the ['ēp̄er](../../strongs/h/h665.md) of the [pārâ](../../strongs/h/h6510.md) shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md): and it shall be unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and unto the [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) [tavek](../../strongs/h/h8432.md) them, for a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md).

<a name="numbers_19_11"></a>Numbers 19:11

He that [naga'](../../strongs/h/h5060.md) the [muwth](../../strongs/h/h4191.md) [nephesh](../../strongs/h/h5315.md) of any ['adam](../../strongs/h/h120.md) shall be [ṭāmē'](../../strongs/h/h2930.md) seven [yowm](../../strongs/h/h3117.md).

<a name="numbers_19_12"></a>Numbers 19:12

He shall [chata'](../../strongs/h/h2398.md) himself with it on the third [yowm](../../strongs/h/h3117.md), and on the seventh [yowm](../../strongs/h/h3117.md) he shall be [ṭāhēr](../../strongs/h/h2891.md): but if he [chata'](../../strongs/h/h2398.md) not himself the third [yowm](../../strongs/h/h3117.md), then the seventh [yowm](../../strongs/h/h3117.md) he shall not be [ṭāhēr](../../strongs/h/h2891.md).

<a name="numbers_19_13"></a>Numbers 19:13

Whosoever [naga'](../../strongs/h/h5060.md) the [muwth](../../strongs/h/h4191.md) [nephesh](../../strongs/h/h5315.md) of any ['adam](../../strongs/h/h120.md) that is [muwth](../../strongs/h/h4191.md), and [chata'](../../strongs/h/h2398.md) not himself, [ṭāmē'](../../strongs/h/h2930.md) the [miškān](../../strongs/h/h4908.md) of [Yĕhovah](../../strongs/h/h3068.md); and that [nephesh](../../strongs/h/h5315.md) shall be [karath](../../strongs/h/h3772.md) from [Yisra'el](../../strongs/h/h3478.md): because the [mayim](../../strongs/h/h4325.md) of [nidâ](../../strongs/h/h5079.md) was not [zāraq](../../strongs/h/h2236.md) upon him, he shall be [tame'](../../strongs/h/h2931.md); his [ṭām'â](../../strongs/h/h2932.md) is yet upon him.

<a name="numbers_19_14"></a>Numbers 19:14

This is the [towrah](../../strongs/h/h8451.md), when an ['adam](../../strongs/h/h120.md) [muwth](../../strongs/h/h4191.md) in an ['ohel](../../strongs/h/h168.md): all that [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md), and all that is in the ['ohel](../../strongs/h/h168.md), shall be [ṭāmē'](../../strongs/h/h2930.md) seven [yowm](../../strongs/h/h3117.md).

<a name="numbers_19_15"></a>Numbers 19:15

And every [pāṯaḥ](../../strongs/h/h6605.md) [kĕliy](../../strongs/h/h3627.md), which hath no [ṣāmîḏ](../../strongs/h/h6781.md) [pāṯîl](../../strongs/h/h6616.md) upon it, is [tame'](../../strongs/h/h2931.md).

<a name="numbers_19_16"></a>Numbers 19:16

And whosoever [naga'](../../strongs/h/h5060.md) one that is [ḥālāl](../../strongs/h/h2491.md) with a [chereb](../../strongs/h/h2719.md) in the [paniym](../../strongs/h/h6440.md) [sadeh](../../strongs/h/h7704.md), or a [muwth](../../strongs/h/h4191.md), or an ['etsem](../../strongs/h/h6106.md) of an ['adam](../../strongs/h/h120.md), or a [qeber](../../strongs/h/h6913.md), shall be [ṭāmē'](../../strongs/h/h2930.md) seven [yowm](../../strongs/h/h3117.md).

<a name="numbers_19_17"></a>Numbers 19:17

And for a [tame'](../../strongs/h/h2931.md) they shall [laqach](../../strongs/h/h3947.md) of the ['aphar](../../strongs/h/h6083.md) of the [śᵊrēp̄â](../../strongs/h/h8316.md) of [chatta'ath](../../strongs/h/h2403.md), and [chay](../../strongs/h/h2416.md) [mayim](../../strongs/h/h4325.md) shall be [nathan](../../strongs/h/h5414.md) thereto in a [kĕliy](../../strongs/h/h3627.md):

<a name="numbers_19_18"></a>Numbers 19:18

And a [tahowr](../../strongs/h/h2889.md) ['iysh](../../strongs/h/h376.md) shall [laqach](../../strongs/h/h3947.md) ['ēzôḇ](../../strongs/h/h231.md), and [ṭāḇal](../../strongs/h/h2881.md) it in the [mayim](../../strongs/h/h4325.md), and [nāzâ](../../strongs/h/h5137.md) it upon the ['ohel](../../strongs/h/h168.md), and upon all the [kĕliy](../../strongs/h/h3627.md), and upon the [nephesh](../../strongs/h/h5315.md) that were there, and upon him that [naga'](../../strongs/h/h5060.md) an ['etsem](../../strongs/h/h6106.md), or one [ḥālāl](../../strongs/h/h2491.md), or one [muwth](../../strongs/h/h4191.md), or a [qeber](../../strongs/h/h6913.md):

<a name="numbers_19_19"></a>Numbers 19:19

And the [tahowr](../../strongs/h/h2889.md) shall [nāzâ](../../strongs/h/h5137.md) upon the [tame'](../../strongs/h/h2931.md) on the third [yowm](../../strongs/h/h3117.md), and on the seventh [yowm](../../strongs/h/h3117.md): and on the seventh [yowm](../../strongs/h/h3117.md) he shall [chata'](../../strongs/h/h2398.md) himself, and [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) himself in [mayim](../../strongs/h/h4325.md), and shall be [ṭāhēr](../../strongs/h/h2891.md) at ['ereb](../../strongs/h/h6153.md).

<a name="numbers_19_20"></a>Numbers 19:20

But the ['iysh](../../strongs/h/h376.md) that shall be [ṭāmē'](../../strongs/h/h2930.md), and shall not [chata'](../../strongs/h/h2398.md) himself, that [nephesh](../../strongs/h/h5315.md) shall be [karath](../../strongs/h/h3772.md) from [tavek](../../strongs/h/h8432.md) the [qāhēl](../../strongs/h/h6951.md), because he hath [ṭāmē'](../../strongs/h/h2930.md) the [miqdash](../../strongs/h/h4720.md) of [Yĕhovah](../../strongs/h/h3068.md): the [mayim](../../strongs/h/h4325.md) of [nidâ](../../strongs/h/h5079.md) hath not been [zāraq](../../strongs/h/h2236.md) upon him; he is [tame'](../../strongs/h/h2931.md).

<a name="numbers_19_21"></a>Numbers 19:21

And it shall be a ['owlam](../../strongs/h/h5769.md) [chuqqah](../../strongs/h/h2708.md) unto them, that he that [nāzâ](../../strongs/h/h5137.md) the [mayim](../../strongs/h/h4325.md) of [nidâ](../../strongs/h/h5079.md) shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md); and he that [naga'](../../strongs/h/h5060.md) the [mayim](../../strongs/h/h4325.md) of [nidâ](../../strongs/h/h5079.md) shall be [ṭāmē'](../../strongs/h/h2930.md) until ['ereb](../../strongs/h/h6153.md).

<a name="numbers_19_22"></a>Numbers 19:22

And whatsoever the [tame'](../../strongs/h/h2931.md) [naga'](../../strongs/h/h5060.md) shall be [ṭāmē'](../../strongs/h/h2930.md); and the [nephesh](../../strongs/h/h5315.md) that [naga'](../../strongs/h/h5060.md) it shall be [ṭāmē'](../../strongs/h/h2930.md) until ['ereb](../../strongs/h/h6153.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 18](numbers_18.md) - [Numbers 20](numbers_20.md)