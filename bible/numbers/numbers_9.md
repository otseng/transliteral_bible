# [Numbers 9](https://www.blueletterbible.org/kjv/num/9)

<a name="numbers_9_1"></a>Numbers 9:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) in the [midbar](../../strongs/h/h4057.md) of [Sînay](../../strongs/h/h5514.md), in the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md) of the second [šānâ](../../strongs/h/h8141.md) after they were [yāṣā'](../../strongs/h/h3318.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_9_2"></a>Numbers 9:2

Let the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) also ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) at his [môʿēḏ](../../strongs/h/h4150.md).

<a name="numbers_9_3"></a>Numbers 9:3

In the fourteenth [yowm](../../strongs/h/h3117.md) of this [ḥōḏeš](../../strongs/h/h2320.md), at ['ereb](../../strongs/h/h6153.md), ye shall ['asah](../../strongs/h/h6213.md) it in his [môʿēḏ](../../strongs/h/h4150.md): according to all the [chuqqah](../../strongs/h/h2708.md) of it, and according to all the [mishpat](../../strongs/h/h4941.md) thereof, shall ye ['asah](../../strongs/h/h6213.md) it.

<a name="numbers_9_4"></a>Numbers 9:4

And [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they should ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md).

<a name="numbers_9_5"></a>Numbers 9:5

And they ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) on the fourteenth [yowm](../../strongs/h/h3117.md) of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md) at ['ereb](../../strongs/h/h6153.md) in the [midbar](../../strongs/h/h4057.md) of [Sînay](../../strongs/h/h5514.md): according to all that [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md), so ['asah](../../strongs/h/h6213.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="numbers_9_6"></a>Numbers 9:6

And there were certain ['enowsh](../../strongs/h/h582.md), who were [tame'](../../strongs/h/h2931.md) by the [nephesh](../../strongs/h/h5315.md) of an ['adam](../../strongs/h/h120.md), that they [yakol](../../strongs/h/h3201.md) not ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) on that [yowm](../../strongs/h/h3117.md): and they [qāraḇ](../../strongs/h/h7126.md) [paniym](../../strongs/h/h6440.md) [Mōshe](../../strongs/h/h4872.md) and [paniym](../../strongs/h/h6440.md) ['Ahărôn](../../strongs/h/h175.md) on that [yowm](../../strongs/h/h3117.md):

<a name="numbers_9_7"></a>Numbers 9:7

And those ['enowsh](../../strongs/h/h582.md) ['āmar](../../strongs/h/h559.md) unto him, We are [tame'](../../strongs/h/h2931.md) by the [nephesh](../../strongs/h/h5315.md) of an ['adam](../../strongs/h/h120.md): wherefore are we [gāraʿ](../../strongs/h/h1639.md), that we may not [qāraḇ](../../strongs/h/h7126.md) a [qorban](../../strongs/h/h7133.md) of [Yĕhovah](../../strongs/h/h3068.md) in his [môʿēḏ](../../strongs/h/h4150.md) [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="numbers_9_8"></a>Numbers 9:8

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto them, ['amad](../../strongs/h/h5975.md), and I will [shama'](../../strongs/h/h8085.md) what [Yĕhovah](../../strongs/h/h3068.md) will [tsavah](../../strongs/h/h6680.md) concerning you.

<a name="numbers_9_9"></a>Numbers 9:9

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_9_10"></a>Numbers 9:10

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), If any ['iysh](../../strongs/h/h376.md) of you or of your [dôr](../../strongs/h/h1755.md) shall be [tame'](../../strongs/h/h2931.md) by reason of a [nephesh](../../strongs/h/h5315.md), or be in a [derek](../../strongs/h/h1870.md) [rachowq](../../strongs/h/h7350.md), yet he shall ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="numbers_9_11"></a>Numbers 9:11

The fourteenth [yowm](../../strongs/h/h3117.md) of the second [ḥōḏeš](../../strongs/h/h2320.md) at ['ereb](../../strongs/h/h6153.md) they shall ['asah](../../strongs/h/h6213.md) it, and ['akal](../../strongs/h/h398.md) it with [maṣṣâ](../../strongs/h/h4682.md) and [merōr](../../strongs/h/h4844.md).

<a name="numbers_9_12"></a>Numbers 9:12

They shall [šā'ar](../../strongs/h/h7604.md) none of it unto the [boqer](../../strongs/h/h1242.md), nor [shabar](../../strongs/h/h7665.md) any ['etsem](../../strongs/h/h6106.md) of it: according to all the [chuqqah](../../strongs/h/h2708.md) of the [pecach](../../strongs/h/h6453.md) they shall ['asah](../../strongs/h/h6213.md) it.

<a name="numbers_9_13"></a>Numbers 9:13

But the ['iysh](../../strongs/h/h376.md) that is [tahowr](../../strongs/h/h2889.md), and is not in a [derek](../../strongs/h/h1870.md), and [ḥāḏal](../../strongs/h/h2308.md) to ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md), even the [nephesh](../../strongs/h/h5315.md) shall be [karath](../../strongs/h/h3772.md) from among his ['am](../../strongs/h/h5971.md): because he [qāraḇ](../../strongs/h/h7126.md) not the [qorban](../../strongs/h/h7133.md) of [Yĕhovah](../../strongs/h/h3068.md) in his [môʿēḏ](../../strongs/h/h4150.md), that ['iysh](../../strongs/h/h376.md) shall [nasa'](../../strongs/h/h5375.md) his [ḥēṭĕ'](../../strongs/h/h2399.md).

<a name="numbers_9_14"></a>Numbers 9:14

And if a [ger](../../strongs/h/h1616.md) shall [guwr](../../strongs/h/h1481.md) among you, and will ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) unto [Yĕhovah](../../strongs/h/h3068.md); according to the [chuqqah](../../strongs/h/h2708.md) of the [pecach](../../strongs/h/h6453.md), and according to the [mishpat](../../strongs/h/h4941.md) thereof, so shall he ['asah](../../strongs/h/h6213.md): ye shall have one [chuqqah](../../strongs/h/h2708.md), both for the [ger](../../strongs/h/h1616.md), and for him that was ['ezrāḥ](../../strongs/h/h249.md) in the ['erets](../../strongs/h/h776.md).

<a name="numbers_9_15"></a>Numbers 9:15

And on the [yowm](../../strongs/h/h3117.md) that the [miškān](../../strongs/h/h4908.md) was [quwm](../../strongs/h/h6965.md) the [ʿānān](../../strongs/h/h6051.md) [kāsâ](../../strongs/h/h3680.md) the [miškān](../../strongs/h/h4908.md), namely, the ['ohel](../../strongs/h/h168.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md): and at ['ereb](../../strongs/h/h6153.md) there was upon the [miškān](../../strongs/h/h4908.md) as it were the [mar'ê](../../strongs/h/h4758.md) of ['esh](../../strongs/h/h784.md), until the [boqer](../../strongs/h/h1242.md).

<a name="numbers_9_16"></a>Numbers 9:16

So it was [tāmîḏ](../../strongs/h/h8548.md): the [ʿānān](../../strongs/h/h6051.md) [kāsâ](../../strongs/h/h3680.md) it by day, and the [mar'ê](../../strongs/h/h4758.md) of ['esh](../../strongs/h/h784.md) by [layil](../../strongs/h/h3915.md).

<a name="numbers_9_17"></a>Numbers 9:17

And [peh](../../strongs/h/h6310.md) the [ʿānān](../../strongs/h/h6051.md) was [ʿālâ](../../strongs/h/h5927.md) from the ['ohel](../../strongs/h/h168.md), then ['aḥar](../../strongs/h/h310.md) that the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāsaʿ](../../strongs/h/h5265.md): and in the [maqowm](../../strongs/h/h4725.md) where the [ʿānān](../../strongs/h/h6051.md) [shakan](../../strongs/h/h7931.md), there the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ḥānâ](../../strongs/h/h2583.md).

<a name="numbers_9_18"></a>Numbers 9:18

At the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāsaʿ](../../strongs/h/h5265.md), and at the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) they [ḥānâ](../../strongs/h/h2583.md): as [yowm](../../strongs/h/h3117.md) the [ʿānān](../../strongs/h/h6051.md) [shakan](../../strongs/h/h7931.md) upon the [miškān](../../strongs/h/h4908.md) they [ḥānâ](../../strongs/h/h2583.md).

<a name="numbers_9_19"></a>Numbers 9:19

And when the [ʿānān](../../strongs/h/h6051.md) ['arak](../../strongs/h/h748.md) upon the [miškān](../../strongs/h/h4908.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md), then the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of [Yĕhovah](../../strongs/h/h3068.md), and [nāsaʿ](../../strongs/h/h5265.md) not.

<a name="numbers_9_20"></a>Numbers 9:20

And so it was, when the [ʿānān](../../strongs/h/h6051.md) was a [mispār](../../strongs/h/h4557.md) [yowm](../../strongs/h/h3117.md) upon the [miškān](../../strongs/h/h4908.md); according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) they [ḥānâ](../../strongs/h/h2583.md), and according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) they [nāsaʿ](../../strongs/h/h5265.md).

<a name="numbers_9_21"></a>Numbers 9:21

And so it was, when the [ʿānān](../../strongs/h/h6051.md) abode from ['ereb](../../strongs/h/h6153.md) unto the [boqer](../../strongs/h/h1242.md), and that the [ʿānān](../../strongs/h/h6051.md) was [ʿālâ](../../strongs/h/h5927.md) in the [boqer](../../strongs/h/h1242.md), then they [nāsaʿ](../../strongs/h/h5265.md): whether it was by [yômām](../../strongs/h/h3119.md) or by [layil](../../strongs/h/h3915.md) that the [ʿānān](../../strongs/h/h6051.md) was [ʿālâ](../../strongs/h/h5927.md), they [nāsaʿ](../../strongs/h/h5265.md).

<a name="numbers_9_22"></a>Numbers 9:22

Or whether it were two [yowm](../../strongs/h/h3117.md), or a [ḥōḏeš](../../strongs/h/h2320.md), or a [yowm](../../strongs/h/h3117.md), that the [ʿānān](../../strongs/h/h6051.md) ['arak](../../strongs/h/h748.md) upon the [miškān](../../strongs/h/h4908.md), [shakan](../../strongs/h/h7931.md) thereon, the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ḥānâ](../../strongs/h/h2583.md), and [nāsaʿ](../../strongs/h/h5265.md) not: but when it was [ʿālâ](../../strongs/h/h5927.md), they [nāsaʿ](../../strongs/h/h5265.md).

<a name="numbers_9_23"></a>Numbers 9:23

At the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) they [ḥānâ](../../strongs/h/h2583.md), and at the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) they [nāsaʿ](../../strongs/h/h5265.md): they [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of [Yĕhovah](../../strongs/h/h3068.md), at the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 8](numbers_8.md) - [Numbers 10](numbers_10.md)