# [Numbers 17](https://www.blueletterbible.org/kjv/num/17)

<a name="numbers_17_1"></a>Numbers 17:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="numbers_17_2"></a>Numbers 17:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and [laqach](../../strongs/h/h3947.md) of every one of them a [maṭṭê](../../strongs/h/h4294.md) [maṭṭê](../../strongs/h/h4294.md) according to the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), of all their [nāśî'](../../strongs/h/h5387.md) according to the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md) twelve [maṭṭê](../../strongs/h/h4294.md): [kāṯaḇ](../../strongs/h/h3789.md) thou every ['iysh](../../strongs/h/h376.md) [shem](../../strongs/h/h8034.md) upon his [maṭṭê](../../strongs/h/h4294.md).

<a name="numbers_17_3"></a>Numbers 17:3

And thou shalt [kāṯaḇ](../../strongs/h/h3789.md) ['Ahărôn](../../strongs/h/h175.md) [shem](../../strongs/h/h8034.md) upon the [maṭṭê](../../strongs/h/h4294.md) of [Lēvî](../../strongs/h/h3878.md): for one [maṭṭê](../../strongs/h/h4294.md) shall be for the [ro'sh](../../strongs/h/h7218.md) of the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md).

<a name="numbers_17_4"></a>Numbers 17:4

And thou shalt [yānaḥ](../../strongs/h/h3240.md) them in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) [paniym](../../strongs/h/h6440.md) the [ʿēḏûṯ](../../strongs/h/h5715.md), where I will [yāʿaḏ](../../strongs/h/h3259.md) with you.

<a name="numbers_17_5"></a>Numbers 17:5

And it shall come to pass, that the ['iysh](../../strongs/h/h376.md) [maṭṭê](../../strongs/h/h4294.md), whom I shall [bāḥar](../../strongs/h/h977.md), shall [pāraḥ](../../strongs/h/h6524.md): and I will make to [šāḵaḵ](../../strongs/h/h7918.md) from me the [tᵊlûnāṯ](../../strongs/h/h8519.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), whereby they [lûn](../../strongs/h/h3885.md) against you.

<a name="numbers_17_6"></a>Numbers 17:6

And [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and every one of their [nāśî'](../../strongs/h/h5387.md) [nathan](../../strongs/h/h5414.md) him a [maṭṭê](../../strongs/h/h4294.md) [maṭṭê](../../strongs/h/h4294.md), for [nāśî'](../../strongs/h/h5387.md) [nāśî'](../../strongs/h/h5387.md) one, according to their ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), even twelve [maṭṭê](../../strongs/h/h4294.md): and the [maṭṭê](../../strongs/h/h4294.md) of ['Ahărôn](../../strongs/h/h175.md) was [tavek](../../strongs/h/h8432.md) their [maṭṭê](../../strongs/h/h4294.md).

<a name="numbers_17_7"></a>Numbers 17:7

And [Mōshe](../../strongs/h/h4872.md) [yānaḥ](../../strongs/h/h3240.md) the [maṭṭê](../../strongs/h/h4294.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) in the ['ohel](../../strongs/h/h168.md) of [ʿēḏûṯ](../../strongs/h/h5715.md).

<a name="numbers_17_8"></a>Numbers 17:8

And it came to pass, that on the [māḥŏrāṯ](../../strongs/h/h4283.md) [Mōshe](../../strongs/h/h4872.md) [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md) of [ʿēḏûṯ](../../strongs/h/h5715.md); and, behold, the [maṭṭê](../../strongs/h/h4294.md) of ['Ahărôn](../../strongs/h/h175.md) for the [bayith](../../strongs/h/h1004.md) of [Lēvî](../../strongs/h/h3878.md) was [pāraḥ](../../strongs/h/h6524.md), and [yāṣā'](../../strongs/h/h3318.md) [peraḥ](../../strongs/h/h6525.md), and [tsuwts](../../strongs/h/h6692.md) [tsiyts](../../strongs/h/h6731.md), and [gamal](../../strongs/h/h1580.md) [šāqēḏ](../../strongs/h/h8247.md).

<a name="numbers_17_9"></a>Numbers 17:9

And [Mōshe](../../strongs/h/h4872.md) [yāṣā'](../../strongs/h/h3318.md) all the [maṭṭê](../../strongs/h/h4294.md) from [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) unto all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): and they [ra'ah](../../strongs/h/h7200.md), and [laqach](../../strongs/h/h3947.md) every ['iysh](../../strongs/h/h376.md) his [maṭṭê](../../strongs/h/h4294.md).

<a name="numbers_17_10"></a>Numbers 17:10

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [shuwb](../../strongs/h/h7725.md) ['Ahărôn](../../strongs/h/h175.md) [maṭṭê](../../strongs/h/h4294.md) again [paniym](../../strongs/h/h6440.md) the [ʿēḏûṯ](../../strongs/h/h5715.md), to be [mišmereṯ](../../strongs/h/h4931.md) for an ['ôṯ](../../strongs/h/h226.md) against the [mᵊrî](../../strongs/h/h4805.md) [ben](../../strongs/h/h1121.md); and thou shalt [kalah](../../strongs/h/h3615.md) their [tᵊlûnāṯ](../../strongs/h/h8519.md) from me, that they [muwth](../../strongs/h/h4191.md) not.

<a name="numbers_17_11"></a>Numbers 17:11

And [Mōshe](../../strongs/h/h4872.md) ['asah](../../strongs/h/h6213.md) so: as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) him, so ['asah](../../strongs/h/h6213.md) he.

<a name="numbers_17_12"></a>Numbers 17:12

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md), Behold, we [gāvaʿ](../../strongs/h/h1478.md), we ['abad](../../strongs/h/h6.md), we all ['abad](../../strongs/h/h6.md).

<a name="numbers_17_13"></a>Numbers 17:13

Whosoever [qārēḇ](../../strongs/h/h7131.md) unto the [miškān](../../strongs/h/h4908.md) of [Yĕhovah](../../strongs/h/h3068.md) shall [muwth](../../strongs/h/h4191.md): shall we be [tamam](../../strongs/h/h8552.md) with [gāvaʿ](../../strongs/h/h1478.md)?

---

[Transliteral Bible](../bible.md)

[Numbers](numbers.md)

[Numbers 16](numbers_16.md) - [Numbers 18](numbers_18.md)