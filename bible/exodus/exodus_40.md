# [Exodus 40](https://www.blueletterbible.org/kjv/exo/40/1)

<a name="exodus_40_1"></a>Exodus 40:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_40_2"></a>Exodus 40:2

On the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) of the ['echad](../../strongs/h/h259.md) [ḥōḏeš](../../strongs/h/h2320.md) shalt thou [quwm](../../strongs/h/h6965.md) the [miškān](../../strongs/h/h4908.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="exodus_40_3"></a>Exodus 40:3

And thou shalt [śûm](../../strongs/h/h7760.md) therein the ['ārôn](../../strongs/h/h727.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md), and [cakak](../../strongs/h/h5526.md) the ['ārôn](../../strongs/h/h727.md) with the [pārōḵeṯ](../../strongs/h/h6532.md).

<a name="exodus_40_4"></a>Exodus 40:4

And thou shalt [bow'](../../strongs/h/h935.md) in the [šulḥān](../../strongs/h/h7979.md), and ['arak](../../strongs/h/h6186.md) the [ʿēreḵ](../../strongs/h/h6187.md) upon it; and thou shalt [bow'](../../strongs/h/h935.md) in the [mᵊnôrâ](../../strongs/h/h4501.md), and [ʿālâ](../../strongs/h/h5927.md) the [nîr](../../strongs/h/h5216.md) thereof.

<a name="exodus_40_5"></a>Exodus 40:5

And thou shalt [nathan](../../strongs/h/h5414.md) thea [mizbeach](../../strongs/h/h4196.md) of [zāhāḇ](../../strongs/h/h2091.md) for the [qᵊṭōreṯ](../../strongs/h/h7004.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md), and [śûm](../../strongs/h/h7760.md) the [māsāḵ](../../strongs/h/h4539.md) of the [peṯaḥ](../../strongs/h/h6607.md) to the [miškān](../../strongs/h/h4908.md).

<a name="exodus_40_6"></a>Exodus 40:6

And thou shalt [nathan](../../strongs/h/h5414.md) thea [mizbeach](../../strongs/h/h4196.md) of the [ʿōlâ](../../strongs/h/h5930.md) [paniym](../../strongs/h/h6440.md) the [peṯaḥ](../../strongs/h/h6607.md) of the [miškān](../../strongs/h/h4908.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="exodus_40_7"></a>Exodus 40:7

And thou shalt [nathan](../../strongs/h/h5414.md) the [kîyôr](../../strongs/h/h3595.md) between the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) and thea [mizbeach](../../strongs/h/h4196.md), and shalt [nathan](../../strongs/h/h5414.md) [mayim](../../strongs/h/h4325.md) therein.

<a name="exodus_40_8"></a>Exodus 40:8

And thou shalt [śûm](../../strongs/h/h7760.md) the [ḥāṣēr](../../strongs/h/h2691.md) [cabiyb](../../strongs/h/h5439.md), and [nathan](../../strongs/h/h5414.md) the [māsāḵ](../../strongs/h/h4539.md) at the [ḥāṣēr](../../strongs/h/h2691.md) [sha'ar](../../strongs/h/h8179.md).

<a name="exodus_40_9"></a>Exodus 40:9

And thou shalt [laqach](../../strongs/h/h3947.md) the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and [māšaḥ](../../strongs/h/h4886.md) the [miškān](../../strongs/h/h4908.md), and all that is therein, and shalt [qadash](../../strongs/h/h6942.md) it, and all the [kĕliy](../../strongs/h/h3627.md) thereof: and it shall be [qodesh](../../strongs/h/h6944.md).

<a name="exodus_40_10"></a>Exodus 40:10

And thou shalt [māšaḥ](../../strongs/h/h4886.md) thea [mizbeach](../../strongs/h/h4196.md) of the [ʿōlâ](../../strongs/h/h5930.md), and all his [kĕliy](../../strongs/h/h3627.md), and [qadash](../../strongs/h/h6942.md) thea [mizbeach](../../strongs/h/h4196.md): and it shall be a [mizbeach](../../strongs/h/h4196.md) [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md).

<a name="exodus_40_11"></a>Exodus 40:11

And thou shalt [māšaḥ](../../strongs/h/h4886.md) the [kîyôr](../../strongs/h/h3595.md) and his [kēn](../../strongs/h/h3653.md), and [qadash](../../strongs/h/h6942.md) it.

<a name="exodus_40_12"></a>Exodus 40:12

And thou shalt [qāraḇ](../../strongs/h/h7126.md) ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and [rāḥaṣ](../../strongs/h/h7364.md) them with [mayim](../../strongs/h/h4325.md).

<a name="exodus_40_13"></a>Exodus 40:13

And thou shalt [labash](../../strongs/h/h3847.md) ['Ahărôn](../../strongs/h/h175.md) the [qodesh](../../strongs/h/h6944.md) [beḡeḏ](../../strongs/h/h899.md), and [māšaḥ](../../strongs/h/h4886.md) him, and [qadash](../../strongs/h/h6942.md) him; that he may [kāhan](../../strongs/h/h3547.md).

<a name="exodus_40_14"></a>Exodus 40:14

And thou shalt [qāraḇ](../../strongs/h/h7126.md) his [ben](../../strongs/h/h1121.md), and [labash](../../strongs/h/h3847.md) them with [kĕthoneth](../../strongs/h/h3801.md):

<a name="exodus_40_15"></a>Exodus 40:15

And thou shalt [māšaḥ](../../strongs/h/h4886.md) them, as thou didst [māšaḥ](../../strongs/h/h4886.md) their ['ab](../../strongs/h/h1.md), that they may [kāhan](../../strongs/h/h3547.md): for their [māšḥâ](../../strongs/h/h4888.md) shall surely be an ['owlam](../../strongs/h/h5769.md) [kᵊhunnâ](../../strongs/h/h3550.md) throughout their [dôr](../../strongs/h/h1755.md).

<a name="exodus_40_16"></a>Exodus 40:16

Thus did [Mōshe](../../strongs/h/h4872.md): according to all that [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) him, so ['asah](../../strongs/h/h6213.md) he.

<a name="exodus_40_17"></a>Exodus 40:17

And it came to pass in the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md) in the second [šānâ](../../strongs/h/h8141.md), on the first day of the [ḥōḏeš](../../strongs/h/h2320.md), that the [miškān](../../strongs/h/h4908.md) was [quwm](../../strongs/h/h6965.md).

<a name="exodus_40_18"></a>Exodus 40:18

And [Mōshe](../../strongs/h/h4872.md) [quwm](../../strongs/h/h6965.md) the [miškān](../../strongs/h/h4908.md), and [nathan](../../strongs/h/h5414.md) his ['eḏen](../../strongs/h/h134.md), and [śûm](../../strongs/h/h7760.md) the [qereš](../../strongs/h/h7175.md) thereof, and [nathan](../../strongs/h/h5414.md) in the [bᵊrîaḥ](../../strongs/h/h1280.md) thereof, and [quwm](../../strongs/h/h6965.md) his [ʿammûḏ](../../strongs/h/h5982.md).

<a name="exodus_40_19"></a>Exodus 40:19

And he [pāraś](../../strongs/h/h6566.md) the ['ohel](../../strongs/h/h168.md) over the [miškān](../../strongs/h/h4908.md), and [śûm](../../strongs/h/h7760.md) the [miḵsê](../../strongs/h/h4372.md) of the ['ohel](../../strongs/h/h168.md) [maʿal](../../strongs/h/h4605.md) upon it; as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_40_20"></a>Exodus 40:20

And he [laqach](../../strongs/h/h3947.md) and [nathan](../../strongs/h/h5414.md) the [ʿēḏûṯ](../../strongs/h/h5715.md) into the ['ārôn](../../strongs/h/h727.md), and [śûm](../../strongs/h/h7760.md) the [baḏ](../../strongs/h/h905.md) on the ['ārôn](../../strongs/h/h727.md), and [nathan](../../strongs/h/h5414.md) the [kapōreṯ](../../strongs/h/h3727.md) [maʿal](../../strongs/h/h4605.md) upon the ['ārôn](../../strongs/h/h727.md):

<a name="exodus_40_21"></a>Exodus 40:21

And he [bow'](../../strongs/h/h935.md) the ['ārôn](../../strongs/h/h727.md) into the [miškān](../../strongs/h/h4908.md), and [śûm](../../strongs/h/h7760.md) the [pārōḵeṯ](../../strongs/h/h6532.md) of the [māsāḵ](../../strongs/h/h4539.md), and [cakak](../../strongs/h/h5526.md) the ['ārôn](../../strongs/h/h727.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_40_22"></a>Exodus 40:22

And he [nathan](../../strongs/h/h5414.md) the [šulḥān](../../strongs/h/h7979.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), upon the [yārēḵ](../../strongs/h/h3409.md) of the [miškān](../../strongs/h/h4908.md) [ṣāp̄ôn](../../strongs/h/h6828.md), [ḥûṣ](../../strongs/h/h2351.md) the [pārōḵeṯ](../../strongs/h/h6532.md).

<a name="exodus_40_23"></a>Exodus 40:23

And he [ʿēreḵ](../../strongs/h/h6187.md) the [lechem](../../strongs/h/h3899.md) in ['arak](../../strongs/h/h6186.md) upon it [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); as [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_40_24"></a>Exodus 40:24

And he [śûm](../../strongs/h/h7760.md) the [mᵊnôrâ](../../strongs/h/h4501.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), over against the [šulḥān](../../strongs/h/h7979.md), on the [yārēḵ](../../strongs/h/h3409.md) of the [miškān](../../strongs/h/h4908.md) [neḡeḇ](../../strongs/h/h5045.md).

<a name="exodus_40_25"></a>Exodus 40:25

And he [ʿālâ](../../strongs/h/h5927.md) the [nîr](../../strongs/h/h5216.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_40_26"></a>Exodus 40:26

And he [śûm](../../strongs/h/h7760.md) the [zāhāḇ](../../strongs/h/h2091.md)a [mizbeach](../../strongs/h/h4196.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) [paniym](../../strongs/h/h6440.md) the [pārōḵeṯ](../../strongs/h/h6532.md):

<a name="exodus_40_27"></a>Exodus 40:27

And he [qāṭar](../../strongs/h/h6999.md) [sam](../../strongs/h/h5561.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) thereon; as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_40_28"></a>Exodus 40:28

And he [śûm](../../strongs/h/h7760.md) the [māsāḵ](../../strongs/h/h4539.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the [miškān](../../strongs/h/h4908.md).

<a name="exodus_40_29"></a>Exodus 40:29

And he [śûm](../../strongs/h/h7760.md) thea [mizbeach](../../strongs/h/h4196.md) of [ʿōlâ](../../strongs/h/h5930.md) by the [peṯaḥ](../../strongs/h/h6607.md) of the [miškān](../../strongs/h/h4908.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and [ʿālâ](../../strongs/h/h5927.md) upon it the [ʿōlâ](../../strongs/h/h5930.md) and the [minchah](../../strongs/h/h4503.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_40_30"></a>Exodus 40:30

And he [śûm](../../strongs/h/h7760.md) the [kîyôr](../../strongs/h/h3595.md) between the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) and thea [mizbeach](../../strongs/h/h4196.md), and [nathan](../../strongs/h/h5414.md) [mayim](../../strongs/h/h4325.md) there, to [rāḥaṣ](../../strongs/h/h7364.md) withal.

<a name="exodus_40_31"></a>Exodus 40:31

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) [rāḥaṣ](../../strongs/h/h7364.md) their [yad](../../strongs/h/h3027.md) and their [regel](../../strongs/h/h7272.md) thereat:

<a name="exodus_40_32"></a>Exodus 40:32

When they [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and when they [qāraḇ](../../strongs/h/h7126.md) unto thea [mizbeach](../../strongs/h/h4196.md), they [rāḥaṣ](../../strongs/h/h7364.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_40_33"></a>Exodus 40:33

And he [quwm](../../strongs/h/h6965.md) the [ḥāṣēr](../../strongs/h/h2691.md) [cabiyb](../../strongs/h/h5439.md) the [miškān](../../strongs/h/h4908.md) and thea [mizbeach](../../strongs/h/h4196.md), and [nathan](../../strongs/h/h5414.md) the [māsāḵ](../../strongs/h/h4539.md) of the [ḥāṣēr](../../strongs/h/h2691.md) [sha'ar](../../strongs/h/h8179.md). So [Mōshe](../../strongs/h/h4872.md) [kalah](../../strongs/h/h3615.md) the [mĕla'kah](../../strongs/h/h4399.md).

<a name="exodus_40_34"></a>Exodus 40:34

Then a [ʿānān](../../strongs/h/h6051.md) [kāsâ](../../strongs/h/h3680.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [mālā'](../../strongs/h/h4390.md) the [miškān](../../strongs/h/h4908.md).

<a name="exodus_40_35"></a>Exodus 40:35

And [Mōshe](../../strongs/h/h4872.md) was not [yakol](../../strongs/h/h3201.md) to [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), because the [ʿānān](../../strongs/h/h6051.md) [shakan](../../strongs/h/h7931.md) thereon, and the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [mālā'](../../strongs/h/h4390.md) the [miškān](../../strongs/h/h4908.md).

<a name="exodus_40_36"></a>Exodus 40:36

And when the [ʿānān](../../strongs/h/h6051.md) was [ʿālâ](../../strongs/h/h5927.md) from over the [miškān](../../strongs/h/h4908.md), the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāsaʿ](../../strongs/h/h5265.md) in all their [massāʿ](../../strongs/h/h4550.md):

<a name="exodus_40_37"></a>Exodus 40:37

But if the [ʿānān](../../strongs/h/h6051.md) were not [ʿālâ](../../strongs/h/h5927.md), then they [nāsaʿ](../../strongs/h/h5265.md) not till the [yowm](../../strongs/h/h3117.md) that it was [ʿālâ](../../strongs/h/h5927.md).

<a name="exodus_40_38"></a>Exodus 40:38

For the [ʿānān](../../strongs/h/h6051.md) of [Yĕhovah](../../strongs/h/h3068.md) was upon the [miškān](../../strongs/h/h4908.md) [yômām](../../strongs/h/h3119.md), and ['esh](../../strongs/h/h784.md) was on it by [layil](../../strongs/h/h3915.md), in the ['ayin](../../strongs/h/h5869.md) of all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), throughout all their [massāʿ](../../strongs/h/h4550.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 39](exodus_39.md) - [Exodus 41](exodus_41.md)