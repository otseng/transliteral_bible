# [Exodus 6](https://www.blueletterbible.org/kjv/exo/6/1/s_56001)

<a name="exodus_6_1"></a>Exodus 6:1

Then [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), Now shalt thou [ra'ah](../../strongs/h/h7200.md) what I will ['asah](../../strongs/h/h6213.md) to [Parʿô](../../strongs/h/h6547.md): for with a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md) shall he [shalach](../../strongs/h/h7971.md) them, and with a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md) shall he [gāraš](../../strongs/h/h1644.md) them of his ['erets](../../strongs/h/h776.md).

<a name="exodus_6_2"></a>Exodus 6:2

And ['Elohiym](../../strongs/h/h430.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), and ['āmar](../../strongs/h/h559.md) unto him, I am [Yĕhovah](../../strongs/h/h3068.md):

<a name="exodus_6_3"></a>Exodus 6:3

And I [ra'ah](../../strongs/h/h7200.md) unto ['Abraham](../../strongs/h/h85.md), unto [Yiṣḥāq](../../strongs/h/h3327.md), and unto [Ya'aqob](../../strongs/h/h3290.md), by the name of ['el](../../strongs/h/h410.md) [Šaday](../../strongs/h/h7706.md), but by my [shem](../../strongs/h/h8034.md) [Yĕhovah](../../strongs/h/h3068.md) was I not [yada'](../../strongs/h/h3045.md) to them.

<a name="exodus_6_4"></a>Exodus 6:4

And I have also [quwm](../../strongs/h/h6965.md) my [bĕriyth](../../strongs/h/h1285.md) with them, to [nathan](../../strongs/h/h5414.md) them the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), the ['erets](../../strongs/h/h776.md) of their [māḡûr](../../strongs/h/h4033.md), wherein they were [guwr](../../strongs/h/h1481.md).

<a name="exodus_6_5"></a>Exodus 6:5

And I have also [shama'](../../strongs/h/h8085.md) the [nᵊ'āqâ](../../strongs/h/h5009.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), whom the [Mitsrayim](../../strongs/h/h4714.md) keep in ['abad](../../strongs/h/h5647.md); and I have [zakar](../../strongs/h/h2142.md) my [bĕriyth](../../strongs/h/h1285.md).

<a name="exodus_6_6"></a>Exodus 6:6

Wherefore ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), I am [Yĕhovah](../../strongs/h/h3068.md), and I will [yāṣā'](../../strongs/h/h3318.md) you out from under the [sᵊḇālâ](../../strongs/h/h5450.md) of the [Mitsrayim](../../strongs/h/h4714.md), and I will [natsal](../../strongs/h/h5337.md) you out of their [ʿăḇōḏâ](../../strongs/h/h5656.md), and I will [gā'al](../../strongs/h/h1350.md) you with a [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md), and with [gadowl](../../strongs/h/h1419.md) [šep̄eṭ](../../strongs/h/h8201.md):

<a name="exodus_6_7"></a>Exodus 6:7

And I will [laqach](../../strongs/h/h3947.md) you to me for an ['am](../../strongs/h/h5971.md), and I will be to you an ['Elohiym](../../strongs/h/h430.md): and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), which [yāṣā'](../../strongs/h/h3318.md) you out from under the [sᵊḇālâ](../../strongs/h/h5450.md) of the [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_6_8"></a>Exodus 6:8

And I will [bow'](../../strongs/h/h935.md) you in unto the ['erets](../../strongs/h/h776.md), concerning the which I did [nasa'](../../strongs/h/h5375.md) [yad](../../strongs/h/h3027.md) to [nathan](../../strongs/h/h5414.md) it to ['Abraham](../../strongs/h/h85.md), to [Yiṣḥāq](../../strongs/h/h3327.md), and to [Ya'aqob](../../strongs/h/h3290.md); and I will [nathan](../../strongs/h/h5414.md) it you for a [môrāšâ](../../strongs/h/h4181.md): I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_6_9"></a>Exodus 6:9

And [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) so unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): but they [shama'](../../strongs/h/h8085.md) not unto [Mōshe](../../strongs/h/h4872.md) for [qōṣer](../../strongs/h/h7115.md) of [ruwach](../../strongs/h/h7307.md), and for [qāšê](../../strongs/h/h7186.md) [ʿăḇōḏâ](../../strongs/h/h5656.md).

<a name="exodus_6_10"></a>Exodus 6:10

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_6_11"></a>Exodus 6:11

[bow'](../../strongs/h/h935.md) in, [dabar](../../strongs/h/h1696.md) unto [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), that he let the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md) of his ['erets](../../strongs/h/h776.md).

<a name="exodus_6_12"></a>Exodus 6:12

And [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), Behold, the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) have not [shama'](../../strongs/h/h8085.md) unto me; how then shall [Parʿô](../../strongs/h/h6547.md) [shama'](../../strongs/h/h8085.md) me, who am of [ʿārēl](../../strongs/h/h6189.md) [saphah](../../strongs/h/h8193.md)?

<a name="exodus_6_13"></a>Exodus 6:13

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) and unto ['Ahărôn](../../strongs/h/h175.md), and gave them a [tsavah](../../strongs/h/h6680.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and unto [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), to [yāṣā'](../../strongs/h/h3318.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_6_14"></a>Exodus 6:14

These be the [ro'sh](../../strongs/h/h7218.md) of their ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md): The [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) the [bᵊḵôr](../../strongs/h/h1060.md) of [Yisra'el](../../strongs/h/h3478.md); [Ḥănôḵ](../../strongs/h/h2585.md), and [Pallû'](../../strongs/h/h6396.md), [Ḥeṣrôn](../../strongs/h/h2696.md), and [Karmî](../../strongs/h/h3756.md): these be the [mišpāḥâ](../../strongs/h/h4940.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md).

<a name="exodus_6_15"></a>Exodus 6:15

And the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md); [yᵊmû'ēl](../../strongs/h/h3223.md), and [yāmîn](../../strongs/h/h3226.md), and ['ōhaḏ](../../strongs/h/h161.md), and [Yāḵîn](../../strongs/h/h3199.md), and [Ṣōḥar](../../strongs/h/h6714.md), and [Šā'ûl](../../strongs/h/h7586.md) the [ben](../../strongs/h/h1121.md) of a [Kᵊnaʿănî](../../strongs/h/h3669.md): these are the [mišpāḥâ](../../strongs/h/h4940.md) of [Šimʿôn](../../strongs/h/h8095.md).

<a name="exodus_6_16"></a>Exodus 6:16

And these are the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md) according to their [towlĕdah](../../strongs/h/h8435.md); [Gēršôn](../../strongs/h/h1648.md), and [Qᵊhāṯ](../../strongs/h/h6955.md), and [Mᵊrārî](../../strongs/h/h4847.md): and the [šānâ](../../strongs/h/h8141.md) of the [chay](../../strongs/h/h2416.md) of [Lēvî](../../strongs/h/h3878.md) were an hundred thirty and seven [šānâ](../../strongs/h/h8141.md).

<a name="exodus_6_17"></a>Exodus 6:17

The [ben](../../strongs/h/h1121.md) of [Gēršôn](../../strongs/h/h1648.md); [Liḇnî](../../strongs/h/h3845.md), and [Šimʿî](../../strongs/h/h8096.md), according to their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="exodus_6_18"></a>Exodus 6:18

And the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md); [ʿAmrām](../../strongs/h/h6019.md), and [Yiṣhār](../../strongs/h/h3324.md), and [Ḥeḇrôn](../../strongs/h/h2275.md), and [ʿUzzî'ēl](../../strongs/h/h5816.md): and the [šānâ](../../strongs/h/h8141.md) of the [chay](../../strongs/h/h2416.md) of [Qᵊhāṯ](../../strongs/h/h6955.md) were an hundred thirty and three [šānâ](../../strongs/h/h8141.md).

<a name="exodus_6_19"></a>Exodus 6:19

And the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md); [Maḥlî](../../strongs/h/h4249.md) and [Mûšî](../../strongs/h/h4187.md): these are the [mišpāḥâ](../../strongs/h/h4940.md) of [Lēvî](../../strongs/h/h3878.md) according to their [towlĕdah](../../strongs/h/h8435.md).

<a name="exodus_6_20"></a>Exodus 6:20

And [ʿAmrām](../../strongs/h/h6019.md) [laqach](../../strongs/h/h3947.md) him [Yôḵeḇeḏ](../../strongs/h/h3115.md) his [dôḏâ](../../strongs/h/h1733.md) to ['ishshah](../../strongs/h/h802.md); and she [yalad](../../strongs/h/h3205.md) him ['Ahărôn](../../strongs/h/h175.md) and [Mōshe](../../strongs/h/h4872.md): and the [šānâ](../../strongs/h/h8141.md) of the [chay](../../strongs/h/h2416.md) of [ʿAmrām](../../strongs/h/h6019.md) were an hundred and thirty and seven years.

<a name="exodus_6_21"></a>Exodus 6:21

And the [ben](../../strongs/h/h1121.md) of [Yiṣhār](../../strongs/h/h3324.md); [Qōraḥ](../../strongs/h/h7141.md), and [Nep̄eḡ](../../strongs/h/h5298.md), and [Ziḵrî](../../strongs/h/h2147.md).

<a name="exodus_6_22"></a>Exodus 6:22

And the [ben](../../strongs/h/h1121.md) of [ʿUzzî'ēl](../../strongs/h/h5816.md); [Mîšā'ēl](../../strongs/h/h4332.md), and ['Ĕlîṣāp̄ān](../../strongs/h/h469.md), and [siṯrî](../../strongs/h/h5644.md).

<a name="exodus_6_23"></a>Exodus 6:23

And ['Ahărôn](../../strongs/h/h175.md) [laqach](../../strongs/h/h3947.md) him ['ĕlîšeḇaʿ](../../strongs/h/h472.md), [bath](../../strongs/h/h1323.md) of [ʿAmmînāḏāḇ](../../strongs/h/h5992.md), ['āḥôṯ](../../strongs/h/h269.md) of [Naḥšôn](../../strongs/h/h5177.md), to ['ishshah](../../strongs/h/h802.md); and she [yalad](../../strongs/h/h3205.md) him [Nāḏāḇ](../../strongs/h/h5070.md), and ['Ăḇîhû'](../../strongs/h/h30.md), ['Elʿāzār](../../strongs/h/h499.md), and ['Îṯāmār](../../strongs/h/h385.md).

<a name="exodus_6_24"></a>Exodus 6:24

And the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md); ['Assîr](../../strongs/h/h617.md), and ['Elqānâ](../../strongs/h/h511.md), and ['ăḇî'āsāp̄](../../strongs/h/h23.md): these are the [mišpāḥâ](../../strongs/h/h4940.md) of the [Qārḥî](../../strongs/h/h7145.md).

<a name="exodus_6_25"></a>Exodus 6:25

And ['Elʿāzār](../../strongs/h/h499.md) ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md) [laqach](../../strongs/h/h3947.md) him one of the [bath](../../strongs/h/h1323.md) of [pûṭî'ēl](../../strongs/h/h6317.md) to ['ishshah](../../strongs/h/h802.md); and she [yalad](../../strongs/h/h3205.md) him [Pînḥās](../../strongs/h/h6372.md): these are the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of the [Lᵊvî](../../strongs/h/h3881.md) according to their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="exodus_6_26"></a>Exodus 6:26

These are that ['Ahărôn](../../strongs/h/h175.md) and [Mōshe](../../strongs/h/h4872.md), to whom [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), [yāṣā'](../../strongs/h/h3318.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) from the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) according to their [tsaba'](../../strongs/h/h6635.md).

<a name="exodus_6_27"></a>Exodus 6:27

These are they which [dabar](../../strongs/h/h1696.md) to [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), to [yāṣā'](../../strongs/h/h3318.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) from [Mitsrayim](../../strongs/h/h4714.md): these are that [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md).

<a name="exodus_6_28"></a>Exodus 6:28

And it came to pass on the [yowm](../../strongs/h/h3117.md) when [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md),

<a name="exodus_6_29"></a>Exodus 6:29

That [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md), I am [Yĕhovah](../../strongs/h/h3068.md): [dabar](../../strongs/h/h1696.md) thou unto [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) all that I [dabar](../../strongs/h/h1696.md) unto thee.

<a name="exodus_6_30"></a>Exodus 6:30

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), Behold, I am of [ʿārēl](../../strongs/h/h6189.md) [saphah](../../strongs/h/h8193.md), and how shall [Parʿô](../../strongs/h/h6547.md) [shama'](../../strongs/h/h8085.md) unto me?

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 5](exodus_5.md) - [Exodus 7](exodus_7.md)