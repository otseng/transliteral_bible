# [Exodus 30](https://www.blueletterbible.org/kjv/exo/30/1)

<a name="exodus_30_1"></a>Exodus 30:1

And thou shalt ['asah](../../strongs/h/h6213.md) a [mizbeach](../../strongs/h/h4196.md) to [miqṭār](../../strongs/h/h4729.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) upon: of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md) shalt thou ['asah](../../strongs/h/h6213.md) it.

<a name="exodus_30_2"></a>Exodus 30:2

An ['ammâ](../../strongs/h/h520.md) shall be the ['ōreḵ](../../strongs/h/h753.md) thereof, and an ['ammâ](../../strongs/h/h520.md) the [rōḥaḇ](../../strongs/h/h7341.md) thereof; [rāḇaʿ](../../strongs/h/h7251.md) shall it be: and two ['ammâ](../../strongs/h/h520.md) shall be the [qômâ](../../strongs/h/h6967.md) thereof: the [qeren](../../strongs/h/h7161.md) thereof shall be of the same.

<a name="exodus_30_3"></a>Exodus 30:3

And thou shalt [ṣāp̄â](../../strongs/h/h6823.md) it with [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md), the [gāḡ](../../strongs/h/h1406.md) thereof, and the [qîr](../../strongs/h/h7023.md) thereof [cabiyb](../../strongs/h/h5439.md), and the [qeren](../../strongs/h/h7161.md) thereof; and thou shalt ['asah](../../strongs/h/h6213.md) unto it a [zēr](../../strongs/h/h2213.md) of [zāhāḇ](../../strongs/h/h2091.md) [cabiyb](../../strongs/h/h5439.md).

<a name="exodus_30_4"></a>Exodus 30:4

And two [zāhāḇ](../../strongs/h/h2091.md) [ṭabaʿaṯ](../../strongs/h/h2885.md) shalt thou ['asah](../../strongs/h/h6213.md) to it under the [zēr](../../strongs/h/h2213.md) of it, by the two [tsela'](../../strongs/h/h6763.md) thereof, upon the two [ṣaḏ](../../strongs/h/h6654.md) of it shalt thou ['asah](../../strongs/h/h6213.md) it; and they shall be for [bayith](../../strongs/h/h1004.md) for the [baḏ](../../strongs/h/h905.md) to [nasa'](../../strongs/h/h5375.md) it withal.

<a name="exodus_30_5"></a>Exodus 30:5

And thou shalt ['asah](../../strongs/h/h6213.md) the [baḏ](../../strongs/h/h905.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md), and [ṣāp̄â](../../strongs/h/h6823.md) them with [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_30_6"></a>Exodus 30:6

And thou shalt [nathan](../../strongs/h/h5414.md) it [paniym](../../strongs/h/h6440.md) the [pārōḵeṯ](../../strongs/h/h6532.md) that is by the ['ārôn](../../strongs/h/h727.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md), [paniym](../../strongs/h/h6440.md) the [kapōreṯ](../../strongs/h/h3727.md) that is over the [ʿēḏûṯ](../../strongs/h/h5715.md), where I will [yāʿaḏ](../../strongs/h/h3259.md) with thee.

<a name="exodus_30_7"></a>Exodus 30:7

And ['Ahărôn](../../strongs/h/h175.md) shall [qāṭar](../../strongs/h/h6999.md) thereon [sam](../../strongs/h/h5561.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) every [boqer](../../strongs/h/h1242.md): when he [yatab](../../strongs/h/h3190.md) the [nîr](../../strongs/h/h5216.md), he shall [qāṭar](../../strongs/h/h6999.md) upon it.

<a name="exodus_30_8"></a>Exodus 30:8

And when ['Ahărôn](../../strongs/h/h175.md) [ʿālâ](../../strongs/h/h5927.md) the [nîr](../../strongs/h/h5216.md) at ['ereb](../../strongs/h/h6153.md), he shall [qāṭar](../../strongs/h/h6999.md) upon it, a [tāmîḏ](../../strongs/h/h8548.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) throughout your [dôr](../../strongs/h/h1755.md).

<a name="exodus_30_9"></a>Exodus 30:9

Ye shall [ʿālâ](../../strongs/h/h5927.md) no [zûr](../../strongs/h/h2114.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) thereon, nor [ʿōlâ](../../strongs/h/h5930.md), nor [minchah](../../strongs/h/h4503.md); neither shall ye [nacak](../../strongs/h/h5258.md) [necek](../../strongs/h/h5262.md) thereon.

<a name="exodus_30_10"></a>Exodus 30:10

And ['Ahărôn](../../strongs/h/h175.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) upon the [qeren](../../strongs/h/h7161.md) of it once in a [šānâ](../../strongs/h/h8141.md) with the [dam](../../strongs/h/h1818.md) of the [chatta'ath](../../strongs/h/h2403.md) of [kipur](../../strongs/h/h3725.md): once in the [šānâ](../../strongs/h/h8141.md) shall he [kāp̄ar](../../strongs/h/h3722.md) upon it throughout your [dôr](../../strongs/h/h1755.md): it is [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_30_11"></a>Exodus 30:11

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_30_12"></a>Exodus 30:12

When thou [nasa'](../../strongs/h/h5375.md) the [ro'sh](../../strongs/h/h7218.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) after their [paqad](../../strongs/h/h6485.md), then shall they [nathan](../../strongs/h/h5414.md) every ['iysh](../../strongs/h/h376.md) a [kōp̄er](../../strongs/h/h3724.md) for his [nephesh](../../strongs/h/h5315.md) unto [Yĕhovah](../../strongs/h/h3068.md), when thou [paqad](../../strongs/h/h6485.md) them; that there be no [neḡep̄](../../strongs/h/h5063.md) among them, when thou [paqad](../../strongs/h/h6485.md) them.

<a name="exodus_30_13"></a>Exodus 30:13

This they shall [nathan](../../strongs/h/h5414.md), every one that ['abar](../../strongs/h/h5674.md) among them that are [paqad](../../strongs/h/h6485.md), half a [šeqel](../../strongs/h/h8255.md) after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md): (a [šeqel](../../strongs/h/h8255.md) is twenty [gērâ](../../strongs/h/h1626.md):) an half [šeqel](../../strongs/h/h8255.md) shall be the [tᵊrûmâ](../../strongs/h/h8641.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_30_14"></a>Exodus 30:14

Every one that ['abar](../../strongs/h/h5674.md) among them that are [paqad](../../strongs/h/h6485.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), shall [nathan](../../strongs/h/h5414.md) a [tᵊrûmâ](../../strongs/h/h8641.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_30_15"></a>Exodus 30:15

The [ʿāšîr](../../strongs/h/h6223.md) shall not [rabah](../../strongs/h/h7235.md), and the [dal](../../strongs/h/h1800.md) shall not [māʿaṭ](../../strongs/h/h4591.md) than half a [šeqel](../../strongs/h/h8255.md), when they [nathan](../../strongs/h/h5414.md) a [tᵊrûmâ](../../strongs/h/h8641.md) unto [Yĕhovah](../../strongs/h/h3068.md), to make a [kāp̄ar](../../strongs/h/h3722.md) for your [nephesh](../../strongs/h/h5315.md).

<a name="exodus_30_16"></a>Exodus 30:16

And thou shalt [laqach](../../strongs/h/h3947.md) the [kipur](../../strongs/h/h3725.md) [keceph](../../strongs/h/h3701.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and shalt [nathan](../../strongs/h/h5414.md) it for the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md); that it may be a [zikārôn](../../strongs/h/h2146.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), to make a [kāp̄ar](../../strongs/h/h3722.md) for your [nephesh](../../strongs/h/h5315.md).

<a name="exodus_30_17"></a>Exodus 30:17

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_30_18"></a>Exodus 30:18

Thou shalt also ['asah](../../strongs/h/h6213.md) a [kîyôr](../../strongs/h/h3595.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md), and his [kēn](../../strongs/h/h3653.md) also of [nᵊḥšeṯ](../../strongs/h/h5178.md), to [rāḥaṣ](../../strongs/h/h7364.md) withal: and thou shalt [nathan](../../strongs/h/h5414.md) it between the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) and thea [mizbeach](../../strongs/h/h4196.md), and thou shalt [nathan](../../strongs/h/h5414.md) [mayim](../../strongs/h/h4325.md) therein.

<a name="exodus_30_19"></a>Exodus 30:19

For ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) shall [rāḥaṣ](../../strongs/h/h7364.md) their [yad](../../strongs/h/h3027.md) and their [regel](../../strongs/h/h7272.md) thereat:

<a name="exodus_30_20"></a>Exodus 30:20

When they [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), they shall [rāḥaṣ](../../strongs/h/h7364.md) with [mayim](../../strongs/h/h4325.md), that they [muwth](../../strongs/h/h4191.md) not; or when they [nāḡaš](../../strongs/h/h5066.md) to thea [mizbeach](../../strongs/h/h4196.md) to [sharath](../../strongs/h/h8334.md), to [qāṭar](../../strongs/h/h6999.md) ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md):

<a name="exodus_30_21"></a>Exodus 30:21

So they shall [rāḥaṣ](../../strongs/h/h7364.md) their [yad](../../strongs/h/h3027.md) and their [regel](../../strongs/h/h7272.md), that they [muwth](../../strongs/h/h4191.md) not: and it shall be a [choq](../../strongs/h/h2706.md) ['owlam](../../strongs/h/h5769.md) to them, even to him and to his [zera'](../../strongs/h/h2233.md) throughout their [dôr](../../strongs/h/h1755.md).

<a name="exodus_30_22"></a>Exodus 30:22

Moreover [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_30_23"></a>Exodus 30:23

[laqach](../../strongs/h/h3947.md) thou also unto thee [ro'sh](../../strongs/h/h7218.md) [beśem](../../strongs/h/h1314.md), of [dᵊrôr](../../strongs/h/h1865.md) [mōr](../../strongs/h/h4753.md) five hundred shekels, and of [beśem](../../strongs/h/h1314.md) [qinnāmôn](../../strongs/h/h7076.md) half so much, even two hundred and fifty shekels, and of [beśem](../../strongs/h/h1314.md) [qānê](../../strongs/h/h7070.md) two hundred and fifty shekels,

<a name="exodus_30_24"></a>Exodus 30:24

And of [qidâ](../../strongs/h/h6916.md) five hundred shekels, after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md), and of [šemen](../../strongs/h/h8081.md) [zayiṯ](../../strongs/h/h2132.md) an [hîn](../../strongs/h/h1969.md):

<a name="exodus_30_25"></a>Exodus 30:25

And thou shalt ['asah](../../strongs/h/h6213.md) it a [šemen](../../strongs/h/h8081.md) of [qodesh](../../strongs/h/h6944.md) [māšḥâ](../../strongs/h/h4888.md), a [rōqaḥ](../../strongs/h/h7545.md) [mirqaḥaṯ](../../strongs/h/h4842.md) after the [ma'aseh](../../strongs/h/h4639.md) of the [rāqaḥ](../../strongs/h/h7543.md): it shall be a [qodesh](../../strongs/h/h6944.md) [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md).

<a name="exodus_30_26"></a>Exodus 30:26

And thou shalt [māšaḥ](../../strongs/h/h4886.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) therewith, and the ['ārôn](../../strongs/h/h727.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md),

<a name="exodus_30_27"></a>Exodus 30:27

And the [šulḥān](../../strongs/h/h7979.md) and all his [kĕliy](../../strongs/h/h3627.md), and the [mᵊnôrâ](../../strongs/h/h4501.md) and his [kĕliy](../../strongs/h/h3627.md), and thea [mizbeach](../../strongs/h/h4196.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md),

<a name="exodus_30_28"></a>Exodus 30:28

And thea [mizbeach](../../strongs/h/h4196.md) of [ʿōlâ](../../strongs/h/h5930.md) with all his [kĕliy](../../strongs/h/h3627.md), and the [kîyôr](../../strongs/h/h3595.md) and his [kēn](../../strongs/h/h3653.md).

<a name="exodus_30_29"></a>Exodus 30:29

And thou shalt [qadash](../../strongs/h/h6942.md) them, that they may be [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md): whatsoever [naga'](../../strongs/h/h5060.md) them shall be [qadash](../../strongs/h/h6942.md).

<a name="exodus_30_30"></a>Exodus 30:30

And thou shalt [māšaḥ](../../strongs/h/h4886.md) ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md), and [qadash](../../strongs/h/h6942.md) them, that they may [kāhan](../../strongs/h/h3547.md).

<a name="exodus_30_31"></a>Exodus 30:31

And thou shalt [dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), This shall be a [qodesh](../../strongs/h/h6944.md) [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md) unto me throughout your [dôr](../../strongs/h/h1755.md).

<a name="exodus_30_32"></a>Exodus 30:32

Upon ['adam](../../strongs/h/h120.md) [basar](../../strongs/h/h1320.md) shall it not be [yāsaḵ](../../strongs/h/h3251.md), neither shall ye ['asah](../../strongs/h/h6213.md) any other like it, after the [maṯkōneṯ](../../strongs/h/h4971.md) of it: it is [qodesh](../../strongs/h/h6944.md), and it shall be [qodesh](../../strongs/h/h6944.md) unto you.

<a name="exodus_30_33"></a>Exodus 30:33

['ăšer](../../strongs/h/h834.md) ['iysh](../../strongs/h/h376.md) [rāqaḥ](../../strongs/h/h7543.md) any like it, or whosoever [nathan](../../strongs/h/h5414.md) any of it upon a [zûr](../../strongs/h/h2114.md), shall even be [karath](../../strongs/h/h3772.md) from his ['am](../../strongs/h/h5971.md).

<a name="exodus_30_34"></a>Exodus 30:34

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [laqach](../../strongs/h/h3947.md) unto thee [sam](../../strongs/h/h5561.md), [nāṭāp̄](../../strongs/h/h5198.md), and [šᵊḥēleṯ](../../strongs/h/h7827.md), and [ḥelbᵊnâ](../../strongs/h/h2464.md); these [sam](../../strongs/h/h5561.md) [sam](../../strongs/h/h5561.md) with [zāḵ](../../strongs/h/h2134.md) [lᵊḇônâ](../../strongs/h/h3828.md): of [baḏ](../../strongs/h/h905.md) shall there be a [baḏ](../../strongs/h/h905.md) weight:

<a name="exodus_30_35"></a>Exodus 30:35

And thou shalt ['asah](../../strongs/h/h6213.md) it a [qᵊṭōreṯ](../../strongs/h/h7004.md), a [rōqaḥ](../../strongs/h/h7545.md) after the [ma'aseh](../../strongs/h/h4639.md) of the [rāqaḥ](../../strongs/h/h7543.md), [mālaḥ](../../strongs/h/h4414.md), [tahowr](../../strongs/h/h2889.md) and [qodesh](../../strongs/h/h6944.md):

<a name="exodus_30_36"></a>Exodus 30:36

And thou shalt [šāḥaq](../../strongs/h/h7833.md) some of it [dāqaq](../../strongs/h/h1854.md), and [nathan](../../strongs/h/h5414.md) of it [paniym](../../strongs/h/h6440.md) the [ʿēḏûṯ](../../strongs/h/h5715.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), where I will [yāʿaḏ](../../strongs/h/h3259.md) with thee: it shall be unto you [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md).

<a name="exodus_30_37"></a>Exodus 30:37

And as for the [qᵊṭōreṯ](../../strongs/h/h7004.md) which thou shalt ['asah](../../strongs/h/h6213.md), ye shall not ['asah](../../strongs/h/h6213.md) to yourselves according to the [maṯkōneṯ](../../strongs/h/h4971.md) thereof: it shall be unto thee [qodesh](../../strongs/h/h6944.md) for [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_30_38"></a>Exodus 30:38

['ăšer](../../strongs/h/h834.md) ['iysh](../../strongs/h/h376.md) shall ['asah](../../strongs/h/h6213.md) like unto that, to [rîaḥ](../../strongs/h/h7306.md) thereto, shall even be [karath](../../strongs/h/h3772.md) from his ['am](../../strongs/h/h5971.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 29](exodus_29.md) - [Exodus 31](exodus_31.md)