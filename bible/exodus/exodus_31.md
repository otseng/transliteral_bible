# [Exodus 31](https://www.blueletterbible.org/kjv/exo/31/1)

<a name="exodus_31_1"></a>Exodus 31:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_31_2"></a>Exodus 31:2

[ra'ah](../../strongs/h/h7200.md), I have [qara'](../../strongs/h/h7121.md) by [shem](../../strongs/h/h8034.md) [Bᵊṣal'ēl](../../strongs/h/h1212.md) the [ben](../../strongs/h/h1121.md) of ['Ûrî](../../strongs/h/h221.md), the [ben](../../strongs/h/h1121.md) of [Ḥûr](../../strongs/h/h2354.md), of the [maṭṭê](../../strongs/h/h4294.md) of [Yehuwdah](../../strongs/h/h3063.md):

<a name="exodus_31_3"></a>Exodus 31:3

And I have [mālā'](../../strongs/h/h4390.md) him with the [ruwach](../../strongs/h/h7307.md) of ['Elohiym](../../strongs/h/h430.md), in [ḥāḵmâ](../../strongs/h/h2451.md), and in [tāḇûn](../../strongs/h/h8394.md), and in [da'ath](../../strongs/h/h1847.md), and in all [mĕla'kah](../../strongs/h/h4399.md),

<a name="exodus_31_4"></a>Exodus 31:4

To [chashab](../../strongs/h/h2803.md) [maḥăšāḇâ](../../strongs/h/h4284.md), to ['asah](../../strongs/h/h6213.md) in [zāhāḇ](../../strongs/h/h2091.md), and in [keceph](../../strongs/h/h3701.md), and in [nᵊḥšeṯ](../../strongs/h/h5178.md),

<a name="exodus_31_5"></a>Exodus 31:5

And in [ḥărōšeṯ](../../strongs/h/h2799.md) of ['eben](../../strongs/h/h68.md), to [mālā'](../../strongs/h/h4390.md) them, and in [ḥărōšeṯ](../../strongs/h/h2799.md) of ['ets](../../strongs/h/h6086.md), to ['asah](../../strongs/h/h6213.md) in all manner of [mĕla'kah](../../strongs/h/h4399.md).

<a name="exodus_31_6"></a>Exodus 31:6

And I, behold, I have [nathan](../../strongs/h/h5414.md) with him ['āhŏlî'āḇ](../../strongs/h/h171.md), the [ben](../../strongs/h/h1121.md) of ['ăḥîsāmāḵ](../../strongs/h/h294.md), of the [maṭṭê](../../strongs/h/h4294.md) of [Dān](../../strongs/h/h1835.md): and in the [leb](../../strongs/h/h3820.md) of all that are [ḥāḵām](../../strongs/h/h2450.md) [leb](../../strongs/h/h3820.md) I have [nathan](../../strongs/h/h5414.md) [ḥāḵmâ](../../strongs/h/h2451.md), that they may ['asah](../../strongs/h/h6213.md) all that I have [tsavah](../../strongs/h/h6680.md) thee;

<a name="exodus_31_7"></a>Exodus 31:7

The ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and the ['ārôn](../../strongs/h/h727.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md), and the [kapōreṯ](../../strongs/h/h3727.md) that is thereupon, and all the [kĕliy](../../strongs/h/h3627.md) of the ['ohel](../../strongs/h/h168.md),

<a name="exodus_31_8"></a>Exodus 31:8

And the [šulḥān](../../strongs/h/h7979.md) and his [kĕliy](../../strongs/h/h3627.md), and the [tahowr](../../strongs/h/h2889.md) [mᵊnôrâ](../../strongs/h/h4501.md) with all his [kĕliy](../../strongs/h/h3627.md), and thea [mizbeach](../../strongs/h/h4196.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md),

<a name="exodus_31_9"></a>Exodus 31:9

And thea [mizbeach](../../strongs/h/h4196.md) of [ʿōlâ](../../strongs/h/h5930.md) with all his [kĕliy](../../strongs/h/h3627.md), and the [kîyôr](../../strongs/h/h3595.md) and his [kēn](../../strongs/h/h3653.md),

<a name="exodus_31_10"></a>Exodus 31:10

And the [beḡeḏ](../../strongs/h/h899.md) of [śᵊrāḏ](../../strongs/h/h8278.md), and the [qodesh](../../strongs/h/h6944.md) [beḡeḏ](../../strongs/h/h899.md) for ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md), and the [beḡeḏ](../../strongs/h/h899.md) of his [ben](../../strongs/h/h1121.md), to [kāhan](../../strongs/h/h3547.md),

<a name="exodus_31_11"></a>Exodus 31:11

And the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and [sam](../../strongs/h/h5561.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) for the [qodesh](../../strongs/h/h6944.md) place: according to all that I have [tsavah](../../strongs/h/h6680.md) thee shall they ['asah](../../strongs/h/h6213.md).

<a name="exodus_31_12"></a>Exodus 31:12

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_31_13"></a>Exodus 31:13

[dabar](../../strongs/h/h1696.md) thou also unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), ['aḵ](../../strongs/h/h389.md) my [shabbath](../../strongs/h/h7676.md) ye shall [shamar](../../strongs/h/h8104.md): for it is an ['ôṯ](../../strongs/h/h226.md) between me and you throughout your [dôr](../../strongs/h/h1755.md); that ye may [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md) that doth [qadash](../../strongs/h/h6942.md) you.

<a name="exodus_31_14"></a>Exodus 31:14

Ye shall [shamar](../../strongs/h/h8104.md) the [shabbath](../../strongs/h/h7676.md) therefore; for it is [qodesh](../../strongs/h/h6944.md) unto you: every one that [ḥālal](../../strongs/h/h2490.md) it shall  be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md): for whosoever ['asah](../../strongs/h/h6213.md) any [mĕla'kah](../../strongs/h/h4399.md) therein, that [nephesh](../../strongs/h/h5315.md) shall be [karath](../../strongs/h/h3772.md) from [qereḇ](../../strongs/h/h7130.md) his ['am](../../strongs/h/h5971.md).

<a name="exodus_31_15"></a>Exodus 31:15

Six [yowm](../../strongs/h/h3117.md) may [mĕla'kah](../../strongs/h/h4399.md) be ['asah](../../strongs/h/h6213.md); but in the [šᵊḇîʿî](../../strongs/h/h7637.md) is the [shabbath](../../strongs/h/h7676.md) of [šabāṯôn](../../strongs/h/h7677.md), [qodesh](../../strongs/h/h6944.md) to [Yĕhovah](../../strongs/h/h3068.md): whosoever ['asah](../../strongs/h/h6213.md) any [mĕla'kah](../../strongs/h/h4399.md) in the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md), he shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="exodus_31_16"></a>Exodus 31:16

Wherefore the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) shall [shamar](../../strongs/h/h8104.md) the [shabbath](../../strongs/h/h7676.md), to ['asah](../../strongs/h/h6213.md) the [shabbath](../../strongs/h/h7676.md) throughout their [dôr](../../strongs/h/h1755.md), for a ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md).

<a name="exodus_31_17"></a>Exodus 31:17

It is an ['ôṯ](../../strongs/h/h226.md) between me and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['owlam](../../strongs/h/h5769.md): for in six [yowm](../../strongs/h/h3117.md) [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md), and on the seventh [yowm](../../strongs/h/h3117.md) he [shabath](../../strongs/h/h7673.md), and was [nāp̄aš](../../strongs/h/h5314.md).

<a name="exodus_31_18"></a>Exodus 31:18

And he [nathan](../../strongs/h/h5414.md) unto [Mōshe](../../strongs/h/h4872.md), when he had made a [kalah](../../strongs/h/h3615.md) of [dabar](../../strongs/h/h1696.md) with him upon [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md), two [lûaḥ](../../strongs/h/h3871.md) of [ʿēḏûṯ](../../strongs/h/h5715.md), [lûaḥ](../../strongs/h/h3871.md) of ['eben](../../strongs/h/h68.md), [kāṯaḇ](../../strongs/h/h3789.md) with the ['etsba'](../../strongs/h/h676.md) of ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 30](exodus_30.md) - [Exodus 32](exodus_32.md)