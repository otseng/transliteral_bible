# [Exodus 3](https://www.blueletterbible.org/kjv/exo/3/1/s_53001)

<a name="exodus_3_1"></a>Exodus 3:1

Now [Mōshe](../../strongs/h/h4872.md) [hayah](../../strongs/h/h1961.md) [ra'ah](../../strongs/h/h7462.md) the [tso'n](../../strongs/h/h6629.md) of [Yiṯrô](../../strongs/h/h3503.md) his [ḥāṯan](../../strongs/h/h2859.md), the [kōhēn](../../strongs/h/h3548.md) of [Miḏyān](../../strongs/h/h4080.md): and he [nāhaḡ](../../strongs/h/h5090.md) the [tso'n](../../strongs/h/h6629.md) to the ['aḥar](../../strongs/h/h310.md) of the [midbar](../../strongs/h/h4057.md), and [bow'](../../strongs/h/h935.md) to the [har](../../strongs/h/h2022.md) of ['Elohiym](../../strongs/h/h430.md), even to [ḥōrēḇ](../../strongs/h/h2722.md).

<a name="exodus_3_2"></a>Exodus 3:2

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) unto him in a [labâ](../../strongs/h/h3827.md) of ['esh](../../strongs/h/h784.md) out of the [tavek](../../strongs/h/h8432.md) of a [sᵊnê](../../strongs/h/h5572.md): and he [ra'ah](../../strongs/h/h7200.md), and, behold, the [sᵊnê](../../strongs/h/h5572.md) [bāʿar](../../strongs/h/h1197.md) with ['esh](../../strongs/h/h784.md), and the [sᵊnê](../../strongs/h/h5572.md) was not ['akal](../../strongs/h/h398.md).

<a name="exodus_3_3"></a>Exodus 3:3

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), I will [cuwr](../../strongs/h/h5493.md), and [ra'ah](../../strongs/h/h7200.md) this [gadowl](../../strongs/h/h1419.md) [mar'ê](../../strongs/h/h4758.md), why the [sᵊnê](../../strongs/h/h5572.md) is not [bāʿar](../../strongs/h/h1197.md).

<a name="exodus_3_4"></a>Exodus 3:4

And when [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) that he [cuwr](../../strongs/h/h5493.md) to [ra'ah](../../strongs/h/h7200.md), ['Elohiym](../../strongs/h/h430.md) [qara'](../../strongs/h/h7121.md) unto him out of the [tavek](../../strongs/h/h8432.md) of the [sᵊnê](../../strongs/h/h5572.md), and ['āmar](../../strongs/h/h559.md), [Mōshe](../../strongs/h/h4872.md), [Mōshe](../../strongs/h/h4872.md). And he ['āmar](../../strongs/h/h559.md), Here am I.

<a name="exodus_3_5"></a>Exodus 3:5

And he ['āmar](../../strongs/h/h559.md), [qāraḇ](../../strongs/h/h7126.md) not hither: [nāšal](../../strongs/h/h5394.md) thy [naʿal](../../strongs/h/h5275.md) from off thy [regel](../../strongs/h/h7272.md), for the [maqowm](../../strongs/h/h4725.md) whereon thou ['amad](../../strongs/h/h5975.md) is [qodesh](../../strongs/h/h6944.md) ['adamah](../../strongs/h/h127.md).

<a name="exodus_3_6"></a>Exodus 3:6

Moreover he ['āmar](../../strongs/h/h559.md), I am the ['Elohiym](../../strongs/h/h430.md) of thy ['ab](../../strongs/h/h1.md), the ['Elohiym](../../strongs/h/h430.md) of ['Abraham](../../strongs/h/h85.md), the ['Elohiym](../../strongs/h/h430.md) of [Yiṣḥāq](../../strongs/h/h3327.md), and the ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md). And [Mōshe](../../strongs/h/h4872.md) [cathar](../../strongs/h/h5641.md) his [paniym](../../strongs/h/h6440.md); for he was [yare'](../../strongs/h/h3372.md) to [nabat](../../strongs/h/h5027.md) upon ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_3_7"></a>Exodus 3:7

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), I have [ra'ah](../../strongs/h/h7200.md) [ra'ah](../../strongs/h/h7200.md) the ['oniy](../../strongs/h/h6040.md) of my ['am](../../strongs/h/h5971.md) which are in [Mitsrayim](../../strongs/h/h4714.md), and have [shama'](../../strongs/h/h8085.md) their [tsa'aqah](../../strongs/h/h6818.md) by [paniym](../../strongs/h/h6440.md) of their [nāḡaś](../../strongs/h/h5065.md); for I [yada'](../../strongs/h/h3045.md) their [maḵ'ōḇ](../../strongs/h/h4341.md);

<a name="exodus_3_8"></a>Exodus 3:8

And I am [yarad](../../strongs/h/h3381.md) to [natsal](../../strongs/h/h5337.md) them out of the [yad](../../strongs/h/h3027.md) of the [Mitsrayim](../../strongs/h/h4714.md), and to [ʿālâ](../../strongs/h/h5927.md) them up out of that ['erets](../../strongs/h/h776.md) unto a [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md) and a [rāḥāḇ](../../strongs/h/h7342.md), unto an ['erets](../../strongs/h/h776.md) [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md); unto the [maqowm](../../strongs/h/h4725.md) of the [Kᵊnaʿănî](../../strongs/h/h3669.md), and the [Ḥitî](../../strongs/h/h2850.md), and the ['Ĕmōrî](../../strongs/h/h567.md), and the [Pᵊrizzî](../../strongs/h/h6522.md), and the [Ḥiûî](../../strongs/h/h2340.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md).

<a name="exodus_3_9"></a>Exodus 3:9

Now therefore, behold, the [tsa'aqah](../../strongs/h/h6818.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) is [bow'](../../strongs/h/h935.md) unto me: and I have also [ra'ah](../../strongs/h/h7200.md) the [laḥaṣ](../../strongs/h/h3906.md) wherewith the [Mitsrayim](../../strongs/h/h4714.md) [lāḥaṣ](../../strongs/h/h3905.md) them.

<a name="exodus_3_10"></a>Exodus 3:10

[yālaḵ](../../strongs/h/h3212.md) now therefore, and I will [shalach](../../strongs/h/h7971.md) thee unto [Parʿô](../../strongs/h/h6547.md), that thou mayest [yāṣā'](../../strongs/h/h3318.md) my ['am](../../strongs/h/h5971.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) out of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_3_11"></a>Exodus 3:11

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto ['Elohiym](../../strongs/h/h430.md), Who am I, that I should [yālaḵ](../../strongs/h/h3212.md) unto [Parʿô](../../strongs/h/h6547.md), and that I should [yāṣā'](../../strongs/h/h3318.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) out of [Mitsrayim](../../strongs/h/h4714.md)?

<a name="exodus_3_12"></a>Exodus 3:12

And he ['āmar](../../strongs/h/h559.md), Certainly I will be with thee; and this shall be an ['ôṯ](../../strongs/h/h226.md) unto thee, that I have [shalach](../../strongs/h/h7971.md) thee: When thou hast [yāṣā'](../../strongs/h/h3318.md) the ['am](../../strongs/h/h5971.md) out of [Mitsrayim](../../strongs/h/h4714.md), ye shall ['abad](../../strongs/h/h5647.md) ['Elohiym](../../strongs/h/h430.md) upon this [har](../../strongs/h/h2022.md).

<a name="exodus_3_13"></a>Exodus 3:13

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto ['Elohiym](../../strongs/h/h430.md), Behold, when I [bow'](../../strongs/h/h935.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and shall ['āmar](../../strongs/h/h559.md) unto them, The ['Elohiym](../../strongs/h/h430.md) of your ['ab](../../strongs/h/h1.md) hath [shalach](../../strongs/h/h7971.md) me unto you; and they shall ['āmar](../../strongs/h/h559.md) to me, What is his [shem](../../strongs/h/h8034.md)? what shall I ['āmar](../../strongs/h/h559.md) unto them?

<a name="exodus_3_14"></a>Exodus 3:14

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [hayah](../../strongs/h/h1961.md) [hayah](../../strongs/h/h1961.md): and he ['āmar](../../strongs/h/h559.md), Thus shalt thou ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), I AM hath [shalach](../../strongs/h/h7971.md) me unto you.

<a name="exodus_3_15"></a>Exodus 3:15

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) moreover unto [Mōshe](../../strongs/h/h4872.md), Thus shalt thou ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of your ['ab](../../strongs/h/h1.md), the ['Elohiym](../../strongs/h/h430.md) of ['Abraham](../../strongs/h/h85.md), the ['Elohiym](../../strongs/h/h430.md) of [Yiṣḥāq](../../strongs/h/h3327.md), and the ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md), hath [shalach](../../strongs/h/h7971.md) me unto you: this is my [shem](../../strongs/h/h8034.md) ['owlam](../../strongs/h/h5769.md), and this is my [zeker](../../strongs/h/h2143.md) unto all [dôr](../../strongs/h/h1755.md).

<a name="exodus_3_16"></a>Exodus 3:16

[yālaḵ](../../strongs/h/h3212.md), and ['āsap̄](../../strongs/h/h622.md) the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md) ['āsap̄](../../strongs/h/h622.md), and ['āmar](../../strongs/h/h559.md) unto them, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of your ['ab](../../strongs/h/h1.md), the ['Elohiym](../../strongs/h/h430.md) of ['Abraham](../../strongs/h/h85.md), of [Yiṣḥāq](../../strongs/h/h3327.md), and of [Ya'aqob](../../strongs/h/h3290.md), [ra'ah](../../strongs/h/h7200.md) unto me, ['āmar](../../strongs/h/h559.md), I have [paqad](../../strongs/h/h6485.md) [paqad](../../strongs/h/h6485.md) you, and seen that which is ['asah](../../strongs/h/h6213.md) to you in [Mitsrayim](../../strongs/h/h4714.md):

<a name="exodus_3_17"></a>Exodus 3:17

And I have ['āmar](../../strongs/h/h559.md), I will [ʿālâ](../../strongs/h/h5927.md) you up out of the ['oniy](../../strongs/h/h6040.md) of [Mitsrayim](../../strongs/h/h4714.md) unto the ['erets](../../strongs/h/h776.md) of the [Kᵊnaʿănî](../../strongs/h/h3669.md), and the [Ḥitî](../../strongs/h/h2850.md), and the ['Ĕmōrî](../../strongs/h/h567.md), and the [Pᵊrizzî](../../strongs/h/h6522.md), and the [Ḥiûî](../../strongs/h/h2340.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md), unto an ['erets](../../strongs/h/h776.md) [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md).

<a name="exodus_3_18"></a>Exodus 3:18

And they shall [shama'](../../strongs/h/h8085.md) to thy [qowl](../../strongs/h/h6963.md): and thou shalt [bow'](../../strongs/h/h935.md), thou and the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md), unto the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), and ye shall ['āmar](../../strongs/h/h559.md) unto him, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of the [ʿiḇrî](../../strongs/h/h5680.md) hath [qārâ](../../strongs/h/h7136.md) with us: and now let us [yālaḵ](../../strongs/h/h3212.md), we beseech thee, three [yowm](../../strongs/h/h3117.md) [derek](../../strongs/h/h1870.md) into the [midbar](../../strongs/h/h4057.md), that we may [zabach](../../strongs/h/h2076.md) to [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_3_19"></a>Exodus 3:19

And I [yada'](../../strongs/h/h3045.md) that the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) will not [nathan](../../strongs/h/h5414.md) you [halak](../../strongs/h/h1980.md), no, not by a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md).

<a name="exodus_3_20"></a>Exodus 3:20

And I will [shalach](../../strongs/h/h7971.md) my [yad](../../strongs/h/h3027.md), and [nakah](../../strongs/h/h5221.md) [Mitsrayim](../../strongs/h/h4714.md) with all my [pala'](../../strongs/h/h6381.md) which I will ['asah](../../strongs/h/h6213.md) in the [qereḇ](../../strongs/h/h7130.md) thereof: and ['aḥar](../../strongs/h/h310.md) that he will [shalach](../../strongs/h/h7971.md) you.

<a name="exodus_3_21"></a>Exodus 3:21

And I will [nathan](../../strongs/h/h5414.md) this ['am](../../strongs/h/h5971.md) [ḥēn](../../strongs/h/h2580.md) in the ['ayin](../../strongs/h/h5869.md) of the [Mitsrayim](../../strongs/h/h4714.md): and it shall come to pass, that, when ye [yālaḵ](../../strongs/h/h3212.md), ye shall not [yālaḵ](../../strongs/h/h3212.md) [rêqām](../../strongs/h/h7387.md).

<a name="exodus_3_22"></a>Exodus 3:22

But every ['ishshah](../../strongs/h/h802.md) shall [sha'al](../../strongs/h/h7592.md) of her [šāḵēn](../../strongs/h/h7934.md), and of her that [guwr](../../strongs/h/h1481.md) in her [bayith](../../strongs/h/h1004.md), [kĕliy](../../strongs/h/h3627.md) of [keceph](../../strongs/h/h3701.md), and [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md), and [śimlâ](../../strongs/h/h8071.md): and ye shall [śûm](../../strongs/h/h7760.md) them upon your [ben](../../strongs/h/h1121.md), and upon your [bath](../../strongs/h/h1323.md); and ye shall [natsal](../../strongs/h/h5337.md) the [Mitsrayim](../../strongs/h/h4714.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 2](exodus_2.md) - [Exodus 4](exodus_4.md)