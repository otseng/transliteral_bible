# [Exodus 2](https://www.blueletterbible.org/kjv/exo/2/1/s_52001)

<a name="exodus_2_1"></a>Exodus 2:1

And there [yālaḵ](../../strongs/h/h3212.md) an ['iysh](../../strongs/h/h376.md) of the [bayith](../../strongs/h/h1004.md) of [Lēvî](../../strongs/h/h3878.md), and [laqach](../../strongs/h/h3947.md) to wife a [bath](../../strongs/h/h1323.md) of [Lēvî](../../strongs/h/h3878.md).

<a name="exodus_2_2"></a>Exodus 2:2

And the ['ishshah](../../strongs/h/h802.md) [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md): and when she [ra'ah](../../strongs/h/h7200.md) him that he was a [towb](../../strongs/h/h2896.md), she [tsaphan](../../strongs/h/h6845.md) him three [yeraḥ](../../strongs/h/h3391.md).

<a name="exodus_2_3"></a>Exodus 2:3

And when she [yakol](../../strongs/h/h3201.md) not [ʿôḏ](../../strongs/h/h5750.md) [tsaphan](../../strongs/h/h6845.md) him, she [laqach](../../strongs/h/h3947.md) for him a [tēḇâ](../../strongs/h/h8392.md) of [gōme'](../../strongs/h/h1573.md), and [ḥāmar](../../strongs/h/h2560.md) it with [ḥēmār](../../strongs/h/h2564.md) and with [zep̄eṯ](../../strongs/h/h2203.md), and [śûm](../../strongs/h/h7760.md) the [yeleḏ](../../strongs/h/h3206.md) therein; and she [śûm](../../strongs/h/h7760.md) it in the [sûp̄](../../strongs/h/h5488.md) by the [yᵊ'ōr](../../strongs/h/h2975.md) [saphah](../../strongs/h/h8193.md).

<a name="exodus_2_4"></a>Exodus 2:4

And his ['āḥôṯ](../../strongs/h/h269.md) [yatsab](../../strongs/h/h3320.md) [rachowq](../../strongs/h/h7350.md), to [yada'](../../strongs/h/h3045.md) what would be ['asah](../../strongs/h/h6213.md) to him.

<a name="exodus_2_5"></a>Exodus 2:5

And the [bath](../../strongs/h/h1323.md) of [Parʿô](../../strongs/h/h6547.md) [yarad](../../strongs/h/h3381.md) to [rāḥaṣ](../../strongs/h/h7364.md) herself at the [yᵊ'ōr](../../strongs/h/h2975.md); and her [naʿărâ](../../strongs/h/h5291.md) [halak](../../strongs/h/h1980.md) along by the [yᵊ'ōr](../../strongs/h/h2975.md) [yad](../../strongs/h/h3027.md); and when she [ra'ah](../../strongs/h/h7200.md) the [tēḇâ](../../strongs/h/h8392.md) [tavek](../../strongs/h/h8432.md) the [sûp̄](../../strongs/h/h5488.md), she [shalach](../../strongs/h/h7971.md) her ['amah](../../strongs/h/h519.md) to [laqach](../../strongs/h/h3947.md) it.

<a name="exodus_2_6"></a>Exodus 2:6

And when she had [pāṯaḥ](../../strongs/h/h6605.md) it, she [ra'ah](../../strongs/h/h7200.md) the [yeleḏ](../../strongs/h/h3206.md): and, behold, the [naʿar](../../strongs/h/h5288.md) [bāḵâ](../../strongs/h/h1058.md). And she had [ḥāmal](../../strongs/h/h2550.md) on him, and ['āmar](../../strongs/h/h559.md), This is one of the [ʿiḇrî](../../strongs/h/h5680.md) [yeleḏ](../../strongs/h/h3206.md).

<a name="exodus_2_7"></a>Exodus 2:7

Then ['āmar](../../strongs/h/h559.md) his ['āḥôṯ](../../strongs/h/h269.md) to [Parʿô](../../strongs/h/h6547.md) [bath](../../strongs/h/h1323.md), Shall I [yālaḵ](../../strongs/h/h3212.md) and [qara'](../../strongs/h/h7121.md) to thee a [yānaq](../../strongs/h/h3243.md) of the [ʿiḇrî](../../strongs/h/h5680.md) ['ishshah](../../strongs/h/h802.md), that she may [yānaq](../../strongs/h/h3243.md) the [yeleḏ](../../strongs/h/h3206.md) for thee?

<a name="exodus_2_8"></a>Exodus 2:8

And [Parʿô](../../strongs/h/h6547.md) [bath](../../strongs/h/h1323.md) ['āmar](../../strongs/h/h559.md) to her, [yālaḵ](../../strongs/h/h3212.md). And the [ʿalmâ](../../strongs/h/h5959.md) [yālaḵ](../../strongs/h/h3212.md) and [qara'](../../strongs/h/h7121.md) the [yeleḏ](../../strongs/h/h3206.md) ['em](../../strongs/h/h517.md).

<a name="exodus_2_9"></a>Exodus 2:9

And [Parʿô](../../strongs/h/h6547.md) [bath](../../strongs/h/h1323.md) ['āmar](../../strongs/h/h559.md) unto her, [yālaḵ](../../strongs/h/h3212.md) this [yeleḏ](../../strongs/h/h3206.md) [yālaḵ](../../strongs/h/h3212.md), and [yānaq](../../strongs/h/h3243.md) it for me, and I will [nathan](../../strongs/h/h5414.md) thee thy [śāḵār](../../strongs/h/h7939.md). And the ['ishshah](../../strongs/h/h802.md) [laqach](../../strongs/h/h3947.md) the [yeleḏ](../../strongs/h/h3206.md), and [nûq](../../strongs/h/h5134.md) it.

<a name="exodus_2_10"></a>Exodus 2:10

And the [yeleḏ](../../strongs/h/h3206.md) [gāḏal](../../strongs/h/h1431.md), and she [bow'](../../strongs/h/h935.md) him unto [Parʿô](../../strongs/h/h6547.md) [bath](../../strongs/h/h1323.md), and he became her [ben](../../strongs/h/h1121.md). And she [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Mōshe](../../strongs/h/h4872.md): and she ['āmar](../../strongs/h/h559.md), Because I [mashah](../../strongs/h/h4871.md) him out of the [mayim](../../strongs/h/h4325.md).

<a name="exodus_2_11"></a>Exodus 2:11

And it came to pass in those [yowm](../../strongs/h/h3117.md), when [Mōshe](../../strongs/h/h4872.md) was [gāḏal](../../strongs/h/h1431.md), that he [yāṣā'](../../strongs/h/h3318.md) unto his ['ach](../../strongs/h/h251.md), and [ra'ah](../../strongs/h/h7200.md) on their [sᵊḇālâ](../../strongs/h/h5450.md): and he [ra'ah](../../strongs/h/h7200.md) a [Miṣrî](../../strongs/h/h4713.md) ['iysh](../../strongs/h/h376.md) [nakah](../../strongs/h/h5221.md) an [ʿiḇrî](../../strongs/h/h5680.md), one of his ['ach](../../strongs/h/h251.md).

<a name="exodus_2_12"></a>Exodus 2:12

And he [panah](../../strongs/h/h6437.md) [kô](../../strongs/h/h3541.md) and [kô](../../strongs/h/h3541.md), and when he [ra'ah](../../strongs/h/h7200.md) that there was no ['iysh](../../strongs/h/h376.md), he [nakah](../../strongs/h/h5221.md) the [Miṣrî](../../strongs/h/h4713.md), and [taman](../../strongs/h/h2934.md) him in the [ḥôl](../../strongs/h/h2344.md).

<a name="exodus_2_13"></a>Exodus 2:13

And when he [yāṣā'](../../strongs/h/h3318.md) the second [yowm](../../strongs/h/h3117.md), behold, two ['enowsh](../../strongs/h/h582.md) of the [ʿiḇrî](../../strongs/h/h5680.md) [nāṣâ](../../strongs/h/h5327.md) together: and he ['āmar](../../strongs/h/h559.md) to him that did the [rasha'](../../strongs/h/h7563.md), Wherefore [nakah](../../strongs/h/h5221.md) thou thy [rea'](../../strongs/h/h7453.md)?

<a name="exodus_2_14"></a>Exodus 2:14

And he ['āmar](../../strongs/h/h559.md), Who [śûm](../../strongs/h/h7760.md) ['iysh](../../strongs/h/h376.md) a [śar](../../strongs/h/h8269.md) and a [shaphat](../../strongs/h/h8199.md) over us? intendest thou to [harag](../../strongs/h/h2026.md) me, as thou [harag](../../strongs/h/h2026.md) the [Miṣrî](../../strongs/h/h4713.md)? And [Mōshe](../../strongs/h/h4872.md) [yare'](../../strongs/h/h3372.md), and ['āmar](../../strongs/h/h559.md), ['āḵēn](../../strongs/h/h403.md) this [dabar](../../strongs/h/h1697.md) is [yada'](../../strongs/h/h3045.md).

<a name="exodus_2_15"></a>Exodus 2:15

Now when [Parʿô](../../strongs/h/h6547.md) [shama'](../../strongs/h/h8085.md) this [dabar](../../strongs/h/h1697.md), he [bāqaš](../../strongs/h/h1245.md) to [harag](../../strongs/h/h2026.md) [Mōshe](../../strongs/h/h4872.md). But [Mōshe](../../strongs/h/h4872.md) [bāraḥ](../../strongs/h/h1272.md) from the [paniym](../../strongs/h/h6440.md) of [Parʿô](../../strongs/h/h6547.md), and [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [Miḏyān](../../strongs/h/h4080.md): and he [yashab](../../strongs/h/h3427.md) by a [bᵊ'ēr](../../strongs/h/h875.md).

<a name="exodus_2_16"></a>Exodus 2:16

Now the [kōhēn](../../strongs/h/h3548.md) of [Miḏyān](../../strongs/h/h4080.md) had seven [bath](../../strongs/h/h1323.md): and they [bow'](../../strongs/h/h935.md) and [dālâ](../../strongs/h/h1802.md), and [mālā'](../../strongs/h/h4390.md) the [rahaṭ](../../strongs/h/h7298.md) to [šāqâ](../../strongs/h/h8248.md) their ['ab](../../strongs/h/h1.md) [tso'n](../../strongs/h/h6629.md).

<a name="exodus_2_17"></a>Exodus 2:17

And the [ra'ah](../../strongs/h/h7462.md) [bow'](../../strongs/h/h935.md) and [gāraš](../../strongs/h/h1644.md) them: but [Mōshe](../../strongs/h/h4872.md) [quwm](../../strongs/h/h6965.md) and [yasha'](../../strongs/h/h3467.md) them, and [šāqâ](../../strongs/h/h8248.md) their [tso'n](../../strongs/h/h6629.md).

<a name="exodus_2_18"></a>Exodus 2:18

And when they [bow'](../../strongs/h/h935.md) to [Rᵊʿû'ēl](../../strongs/h/h7467.md) their ['ab](../../strongs/h/h1.md), he ['āmar](../../strongs/h/h559.md), How is it that ye are [bow'](../../strongs/h/h935.md) so [māhar](../../strongs/h/h4116.md) [yowm](../../strongs/h/h3117.md)?

<a name="exodus_2_19"></a>Exodus 2:19

And they ['āmar](../../strongs/h/h559.md), An [Miṣrî](../../strongs/h/h4713.md) ['iysh](../../strongs/h/h376.md) [natsal](../../strongs/h/h5337.md) us out of the [yad](../../strongs/h/h3027.md) of the [ra'ah](../../strongs/h/h7462.md), and also [dālâ](../../strongs/h/h1802.md) [dālâ](../../strongs/h/h1802.md) for us, and [šāqâ](../../strongs/h/h8248.md) the [tso'n](../../strongs/h/h6629.md).

<a name="exodus_2_20"></a>Exodus 2:20

And he ['āmar](../../strongs/h/h559.md) unto his [bath](../../strongs/h/h1323.md), And where is he? why is it that ye have ['azab](../../strongs/h/h5800.md) the ['iysh](../../strongs/h/h376.md)? [qara'](../../strongs/h/h7121.md) him, that he may ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md).

<a name="exodus_2_21"></a>Exodus 2:21

And [Mōshe](../../strongs/h/h4872.md) was [yā'al](../../strongs/h/h2974.md) to [yashab](../../strongs/h/h3427.md) with the ['iysh](../../strongs/h/h376.md): and he [nathan](../../strongs/h/h5414.md) [Mōshe](../../strongs/h/h4872.md) [ṣipōrâ](../../strongs/h/h6855.md) his [bath](../../strongs/h/h1323.md).

<a name="exodus_2_22"></a>Exodus 2:22

And she [yalad](../../strongs/h/h3205.md) him a [ben](../../strongs/h/h1121.md), and he [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Gēršōm](../../strongs/h/h1647.md): for he ['āmar](../../strongs/h/h559.md), I have been a [ger](../../strongs/h/h1616.md) in a [nāḵrî](../../strongs/h/h5237.md) ['erets](../../strongs/h/h776.md).

<a name="exodus_2_23"></a>Exodus 2:23

And it came to pass in [hēm](../../strongs/h/h1992.md) [rab](../../strongs/h/h7227.md) of [yowm](../../strongs/h/h3117.md), that the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) [muwth](../../strongs/h/h4191.md): and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['ānaḥ](../../strongs/h/h584.md) [min](../../strongs/h/h4480.md) the [ʿăḇōḏâ](../../strongs/h/h5656.md), and they [zāʿaq](../../strongs/h/h2199.md), and their [shav'ah](../../strongs/h/h7775.md) [ʿālâ](../../strongs/h/h5927.md) unto ['Elohiym](../../strongs/h/h430.md) by reason of the [ʿăḇōḏâ](../../strongs/h/h5656.md).

<a name="exodus_2_24"></a>Exodus 2:24

And ['Elohiym](../../strongs/h/h430.md) [shama'](../../strongs/h/h8085.md) their [nᵊ'āqâ](../../strongs/h/h5009.md), and ['Elohiym](../../strongs/h/h430.md) [zakar](../../strongs/h/h2142.md) his [bĕriyth](../../strongs/h/h1285.md) with ['Abraham](../../strongs/h/h85.md), with [Yiṣḥāq](../../strongs/h/h3327.md), and with [Ya'aqob](../../strongs/h/h3290.md).

<a name="exodus_2_25"></a>Exodus 2:25

And ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) upon the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['Elohiym](../../strongs/h/h430.md) had [yada'](../../strongs/h/h3045.md) unto them.

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 1](exodus_1.md) - [Exodus 3](exodus_3.md)