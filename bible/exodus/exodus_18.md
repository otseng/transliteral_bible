# [Exodus 18](https://www.blueletterbible.org/kjv/exo/18/1/s_68001)

<a name="exodus_18_1"></a>Exodus 18:1

When [Yiṯrô](../../strongs/h/h3503.md), the [kōhēn](../../strongs/h/h3548.md) of [Miḏyān](../../strongs/h/h4080.md), [Mōshe](../../strongs/h/h4872.md) [ḥāṯan](../../strongs/h/h2859.md), [shama'](../../strongs/h/h8085.md) of all that ['Elohiym](../../strongs/h/h430.md) had ['asah](../../strongs/h/h6213.md) for [Mōshe](../../strongs/h/h4872.md), and for [Yisra'el](../../strongs/h/h3478.md) his ['am](../../strongs/h/h5971.md), and that [Yĕhovah](../../strongs/h/h3068.md) had [yāṣā'](../../strongs/h/h3318.md) [Yisra'el](../../strongs/h/h3478.md) out of [Mitsrayim](../../strongs/h/h4714.md);

<a name="exodus_18_2"></a>Exodus 18:2

Then [Yiṯrô](../../strongs/h/h3503.md), [Mōshe](../../strongs/h/h4872.md) [ḥāṯan](../../strongs/h/h2859.md), [laqach](../../strongs/h/h3947.md) [ṣipōrâ](../../strongs/h/h6855.md), [Mōshe](../../strongs/h/h4872.md) ['ishshah](../../strongs/h/h802.md), ['aḥar](../../strongs/h/h310.md) he had [šillûḥîm](../../strongs/h/h7964.md),

<a name="exodus_18_3"></a>Exodus 18:3

And her two [ben](../../strongs/h/h1121.md); of which the [shem](../../strongs/h/h8034.md) of the one was [Gēršōm](../../strongs/h/h1647.md); for he ['āmar](../../strongs/h/h559.md), I have been a [ger](../../strongs/h/h1616.md) in a [nāḵrî](../../strongs/h/h5237.md) ['erets](../../strongs/h/h776.md):

<a name="exodus_18_4"></a>Exodus 18:4

And the [shem](../../strongs/h/h8034.md) of the other was ['Ĕlîʿezer](../../strongs/h/h461.md); for the ['Elohiym](../../strongs/h/h430.md) of my ['ab](../../strongs/h/h1.md), said he, was mine ['ezer](../../strongs/h/h5828.md), and [natsal](../../strongs/h/h5337.md) me from the [chereb](../../strongs/h/h2719.md) of [Parʿô](../../strongs/h/h6547.md):

<a name="exodus_18_5"></a>Exodus 18:5

And [Yiṯrô](../../strongs/h/h3503.md), [Mōshe](../../strongs/h/h4872.md) [ḥāṯan](../../strongs/h/h2859.md), [bow'](../../strongs/h/h935.md) with his [ben](../../strongs/h/h1121.md) and his ['ishshah](../../strongs/h/h802.md) unto [Mōshe](../../strongs/h/h4872.md) into the [midbar](../../strongs/h/h4057.md), where he [ḥānâ](../../strongs/h/h2583.md) at the [har](../../strongs/h/h2022.md) of ['Elohiym](../../strongs/h/h430.md):

<a name="exodus_18_6"></a>Exodus 18:6

And he ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), I thy [ḥāṯan](../../strongs/h/h2859.md) [Yiṯrô](../../strongs/h/h3503.md) am [bow'](../../strongs/h/h935.md) unto thee, and thy ['ishshah](../../strongs/h/h802.md), and her two [ben](../../strongs/h/h1121.md) with her.

<a name="exodus_18_7"></a>Exodus 18:7

And [Mōshe](../../strongs/h/h4872.md) [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) his [ḥāṯan](../../strongs/h/h2859.md), and [shachah](../../strongs/h/h7812.md), and [nashaq](../../strongs/h/h5401.md) him; and they [sha'al](../../strongs/h/h7592.md) ['iysh](../../strongs/h/h376.md) [rea'](../../strongs/h/h7453.md) of their [shalowm](../../strongs/h/h7965.md); and they [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md).

<a name="exodus_18_8"></a>Exodus 18:8

And [Mōshe](../../strongs/h/h4872.md) [sāp̄ar](../../strongs/h/h5608.md) his [ḥāṯan](../../strongs/h/h2859.md) all that [Yĕhovah](../../strongs/h/h3068.md) had ['asah](../../strongs/h/h6213.md) unto [Parʿô](../../strongs/h/h6547.md) and to the [Mitsrayim](../../strongs/h/h4714.md) for [Yisra'el](../../strongs/h/h3478.md) ['ôḏôṯ](../../strongs/h/h182.md), and all the [tᵊlā'â](../../strongs/h/h8513.md) that had [māṣā'](../../strongs/h/h4672.md) upon them by the [derek](../../strongs/h/h1870.md), and how [Yĕhovah](../../strongs/h/h3068.md) [natsal](../../strongs/h/h5337.md) them.

<a name="exodus_18_9"></a>Exodus 18:9

And [Yiṯrô](../../strongs/h/h3503.md) [ḥāḏâ](../../strongs/h/h2302.md) for all the [towb](../../strongs/h/h2896.md) which [Yĕhovah](../../strongs/h/h3068.md) had ['asah](../../strongs/h/h6213.md) to [Yisra'el](../../strongs/h/h3478.md), whom he had [natsal](../../strongs/h/h5337.md) out of the [yad](../../strongs/h/h3027.md) of the [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_18_10"></a>Exodus 18:10

And [Yiṯrô](../../strongs/h/h3503.md) ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md), who hath [natsal](../../strongs/h/h5337.md) you out of the [yad](../../strongs/h/h3027.md) of the [Mitsrayim](../../strongs/h/h4714.md), and out of the [yad](../../strongs/h/h3027.md) of [Parʿô](../../strongs/h/h6547.md), who hath [natsal](../../strongs/h/h5337.md) the ['am](../../strongs/h/h5971.md) from under the [yad](../../strongs/h/h3027.md) of the [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_18_11"></a>Exodus 18:11

Now I [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) is [gadowl](../../strongs/h/h1419.md) than all ['Elohiym](../../strongs/h/h430.md): for in the [dabar](../../strongs/h/h1697.md) wherein they [zûḏ](../../strongs/h/h2102.md) he was above them.

<a name="exodus_18_12"></a>Exodus 18:12

And [Yiṯrô](../../strongs/h/h3503.md), [Mōshe](../../strongs/h/h4872.md) [ḥāṯan](../../strongs/h/h2859.md), [laqach](../../strongs/h/h3947.md) an [ʿōlâ](../../strongs/h/h5930.md) and [zebach](../../strongs/h/h2077.md) for ['Elohiym](../../strongs/h/h430.md): and ['Ahărôn](../../strongs/h/h175.md) [bow'](../../strongs/h/h935.md), and all the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md), to ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) with [Mōshe](../../strongs/h/h4872.md) [ḥāṯan](../../strongs/h/h2859.md) [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_18_13"></a>Exodus 18:13

And it came to pass on the [māḥŏrāṯ](../../strongs/h/h4283.md), that [Mōshe](../../strongs/h/h4872.md) [yashab](../../strongs/h/h3427.md) to [shaphat](../../strongs/h/h8199.md) the ['am](../../strongs/h/h5971.md): and the ['am](../../strongs/h/h5971.md) ['amad](../../strongs/h/h5975.md) by [Mōshe](../../strongs/h/h4872.md) from the [boqer](../../strongs/h/h1242.md) unto the ['ereb](../../strongs/h/h6153.md).

<a name="exodus_18_14"></a>Exodus 18:14

And when [Mōshe](../../strongs/h/h4872.md) [ḥāṯan](../../strongs/h/h2859.md) [ra'ah](../../strongs/h/h7200.md) all that he ['asah](../../strongs/h/h6213.md) to the ['am](../../strongs/h/h5971.md), he ['āmar](../../strongs/h/h559.md), What is this [dabar](../../strongs/h/h1697.md) that thou ['asah](../../strongs/h/h6213.md) to the ['am](../../strongs/h/h5971.md)? why [yashab](../../strongs/h/h3427.md) thou thyself alone, and all the ['am](../../strongs/h/h5971.md) [nāṣaḇ](../../strongs/h/h5324.md) by thee from [boqer](../../strongs/h/h1242.md) unto ['ereb](../../strongs/h/h6153.md)?

<a name="exodus_18_15"></a>Exodus 18:15

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto his [ḥāṯan](../../strongs/h/h2859.md), Because the ['am](../../strongs/h/h5971.md) [bow'](../../strongs/h/h935.md) unto me to [darash](../../strongs/h/h1875.md) of ['Elohiym](../../strongs/h/h430.md):

<a name="exodus_18_16"></a>Exodus 18:16

When they have a [dabar](../../strongs/h/h1697.md), they [bow'](../../strongs/h/h935.md) unto me; and I [shaphat](../../strongs/h/h8199.md) between ['iysh](../../strongs/h/h376.md) and [rea'](../../strongs/h/h7453.md), and I do make them [yada'](../../strongs/h/h3045.md) the [choq](../../strongs/h/h2706.md) of ['Elohiym](../../strongs/h/h430.md), and his [towrah](../../strongs/h/h8451.md).

<a name="exodus_18_17"></a>Exodus 18:17

And [Mōshe](../../strongs/h/h4872.md) [ḥāṯan](../../strongs/h/h2859.md) ['āmar](../../strongs/h/h559.md) unto him, The [dabar](../../strongs/h/h1697.md) that thou ['asah](../../strongs/h/h6213.md) is not [towb](../../strongs/h/h2896.md).

<a name="exodus_18_18"></a>Exodus 18:18

Thou wilt [nabel](../../strongs/h/h5034.md) [nabel](../../strongs/h/h5034.md), both thou, and this ['am](../../strongs/h/h5971.md) that is with thee: for this [dabar](../../strongs/h/h1697.md) is too [kāḇēḏ](../../strongs/h/h3515.md) for thee; thou art not [yakol](../../strongs/h/h3201.md) to ['asah](../../strongs/h/h6213.md) it thyself alone.

<a name="exodus_18_19"></a>Exodus 18:19

[shama'](../../strongs/h/h8085.md) now unto my [qowl](../../strongs/h/h6963.md), I will give thee [ya'ats](../../strongs/h/h3289.md), and ['Elohiym](../../strongs/h/h430.md) shall be with thee: Be thou for the ['am](../../strongs/h/h5971.md) to [môl](../../strongs/h/h4136.md) ['Elohiym](../../strongs/h/h430.md), that thou mayest [bow'](../../strongs/h/h935.md) the [dabar](../../strongs/h/h1697.md) unto ['Elohiym](../../strongs/h/h430.md):

<a name="exodus_18_20"></a>Exodus 18:20

And thou shalt [zāhar](../../strongs/h/h2094.md) them [choq](../../strongs/h/h2706.md) and [towrah](../../strongs/h/h8451.md), and shalt [yada'](../../strongs/h/h3045.md) them the [derek](../../strongs/h/h1870.md) wherein they must [yālaḵ](../../strongs/h/h3212.md), and the [ma'aseh](../../strongs/h/h4639.md) that they must ['asah](../../strongs/h/h6213.md).

<a name="exodus_18_21"></a>Exodus 18:21

Moreover thou shalt [chazah](../../strongs/h/h2372.md) out of all the ['am](../../strongs/h/h5971.md) [ḥayil](../../strongs/h/h2428.md) ['enowsh](../../strongs/h/h582.md), such as [yārē'](../../strongs/h/h3373.md) ['Elohiym](../../strongs/h/h430.md), ['enowsh](../../strongs/h/h582.md) of ['emeth](../../strongs/h/h571.md), [sane'](../../strongs/h/h8130.md) [beṣaʿ](../../strongs/h/h1215.md); and [śûm](../../strongs/h/h7760.md) such over them, to be [śar](../../strongs/h/h8269.md) of ['elep̄](../../strongs/h/h505.md), and [śar](../../strongs/h/h8269.md) of [mē'â](../../strongs/h/h3967.md), [śar](../../strongs/h/h8269.md) of [ḥămisheem](../../strongs/h/h2572.md), and [śar](../../strongs/h/h8269.md) of [ʿeśer](../../strongs/h/h6235.md):

<a name="exodus_18_22"></a>Exodus 18:22

And let them [shaphat](../../strongs/h/h8199.md) the ['am](../../strongs/h/h5971.md) at all [ʿēṯ](../../strongs/h/h6256.md): and it shall be, that every [gadowl](../../strongs/h/h1419.md) [dabar](../../strongs/h/h1697.md) they shall [bow'](../../strongs/h/h935.md) unto thee, but every [qāṭān](../../strongs/h/h6996.md) [dabar](../../strongs/h/h1697.md) they shall [shaphat](../../strongs/h/h8199.md): so shall it be [qālal](../../strongs/h/h7043.md) for thyself, and they shall [nasa'](../../strongs/h/h5375.md) the burden with thee.

<a name="exodus_18_23"></a>Exodus 18:23

If thou shalt ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md), and ['Elohiym](../../strongs/h/h430.md) [tsavah](../../strongs/h/h6680.md) thee so, then thou shalt be [yakol](../../strongs/h/h3201.md) to ['amad](../../strongs/h/h5975.md), and all this ['am](../../strongs/h/h5971.md) shall also [bow'](../../strongs/h/h935.md) to their [maqowm](../../strongs/h/h4725.md) in [shalowm](../../strongs/h/h7965.md).

<a name="exodus_18_24"></a>Exodus 18:24

So [Mōshe](../../strongs/h/h4872.md) [shama'](../../strongs/h/h8085.md) to the [qowl](../../strongs/h/h6963.md) of his [ḥāṯan](../../strongs/h/h2859.md), and ['asah](../../strongs/h/h6213.md) all that he had ['āmar](../../strongs/h/h559.md).

<a name="exodus_18_25"></a>Exodus 18:25

And [Mōshe](../../strongs/h/h4872.md) [bāḥar](../../strongs/h/h977.md) [ḥayil](../../strongs/h/h2428.md) ['enowsh](../../strongs/h/h582.md) out of all [Yisra'el](../../strongs/h/h3478.md), and [nathan](../../strongs/h/h5414.md) them [ro'sh](../../strongs/h/h7218.md) over the ['am](../../strongs/h/h5971.md), [śar](../../strongs/h/h8269.md) of ['elep̄](../../strongs/h/h505.md), [śar](../../strongs/h/h8269.md) of [mē'â](../../strongs/h/h3967.md), [śar](../../strongs/h/h8269.md) of [ḥămisheem](../../strongs/h/h2572.md), and [śar](../../strongs/h/h8269.md) of tens.

<a name="exodus_18_26"></a>Exodus 18:26

And they [shaphat](../../strongs/h/h8199.md) the ['am](../../strongs/h/h5971.md) at all [ʿēṯ](../../strongs/h/h6256.md): the [qāšê](../../strongs/h/h7186.md) [dabar](../../strongs/h/h1697.md) they [bow'](../../strongs/h/h935.md) unto [Mōshe](../../strongs/h/h4872.md), but every [qāṭān](../../strongs/h/h6996.md) [dabar](../../strongs/h/h1697.md) they [shaphat](../../strongs/h/h8199.md) themselves.

<a name="exodus_18_27"></a>Exodus 18:27

And [Mōshe](../../strongs/h/h4872.md) let his [ḥāṯan](../../strongs/h/h2859.md) [shalach](../../strongs/h/h7971.md); and he [yālaḵ](../../strongs/h/h3212.md) his way into his own ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 17](exodus_17.md) - [Exodus 19](exodus_19.md)