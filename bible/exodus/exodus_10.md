# [Exodus 10](https://www.blueletterbible.org/kjv/exo/10/1/s_60001)

<a name="exodus_10_1"></a>Exodus 10:1

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [bow'](../../strongs/h/h935.md) in unto [Parʿô](../../strongs/h/h6547.md): for I have [kabad](../../strongs/h/h3513.md) his [leb](../../strongs/h/h3820.md), and the [leb](../../strongs/h/h3820.md) of his ['ebed](../../strongs/h/h5650.md), that I might [shiyth](../../strongs/h/h7896.md) these my ['ôṯ](../../strongs/h/h226.md) [qereḇ](../../strongs/h/h7130.md) him:

<a name="exodus_10_2"></a>Exodus 10:2

And that thou mayest [sāp̄ar](../../strongs/h/h5608.md) in the ['ozen](../../strongs/h/h241.md) of thy [ben](../../strongs/h/h1121.md), and of thy [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md), what things I have [ʿālal](../../strongs/h/h5953.md) in [Mitsrayim](../../strongs/h/h4714.md), and my ['ôṯ](../../strongs/h/h226.md) which I have [śûm](../../strongs/h/h7760.md) among them; that ye may [yada'](../../strongs/h/h3045.md) how that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_10_3"></a>Exodus 10:3

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [bow'](../../strongs/h/h935.md) unto [Parʿô](../../strongs/h/h6547.md), and ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of the [ʿiḇrî](../../strongs/h/h5680.md), [māṯay](../../strongs/h/h4970.md) wilt thou [mā'ēn](../../strongs/h/h3985.md) to [ʿānâ](../../strongs/h/h6031.md) thyself [paniym](../../strongs/h/h6440.md) me? let my ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), that they may ['abad](../../strongs/h/h5647.md) me.

<a name="exodus_10_4"></a>Exodus 10:4

[kî](../../strongs/h/h3588.md), if thou [mā'ēn](../../strongs/h/h3986.md) to let my ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), behold, [māḥār](../../strongs/h/h4279.md) will I [bow'](../../strongs/h/h935.md) the ['arbê](../../strongs/h/h697.md) into thy [gᵊḇûl](../../strongs/h/h1366.md):

<a name="exodus_10_5"></a>Exodus 10:5

And they shall [kāsâ](../../strongs/h/h3680.md) the ['ayin](../../strongs/h/h5869.md) of the ['erets](../../strongs/h/h776.md), that one cannot be [yakol](../../strongs/h/h3201.md) to [ra'ah](../../strongs/h/h7200.md) the ['erets](../../strongs/h/h776.md): and they shall ['akal](../../strongs/h/h398.md) the [yeṯer](../../strongs/h/h3499.md) of that which is [pᵊlêṭâ](../../strongs/h/h6413.md), which [šā'ar](../../strongs/h/h7604.md) unto you from the [barad](../../strongs/h/h1259.md), and shall ['akal](../../strongs/h/h398.md) every ['ets](../../strongs/h/h6086.md) which [ṣāmaḥ](../../strongs/h/h6779.md) for you out of the [sadeh](../../strongs/h/h7704.md):

<a name="exodus_10_6"></a>Exodus 10:6

And they shall [mālā'](../../strongs/h/h4390.md) thy [bayith](../../strongs/h/h1004.md), and the [bayith](../../strongs/h/h1004.md) of all thy ['ebed](../../strongs/h/h5650.md), and the [bayith](../../strongs/h/h1004.md) of all the [Mitsrayim](../../strongs/h/h4714.md); which neither thy ['ab](../../strongs/h/h1.md), nor thy ['ab](../../strongs/h/h1.md) ['ab](../../strongs/h/h1.md) have [ra'ah](../../strongs/h/h7200.md), since the [yowm](../../strongs/h/h3117.md) that they were upon the ['ăḏāmâ](../../strongs/h/h127.md) unto this [yowm](../../strongs/h/h3117.md). And he [panah](../../strongs/h/h6437.md) himself, and [yāṣā'](../../strongs/h/h3318.md) from [Parʿô](../../strongs/h/h6547.md).

<a name="exodus_10_7"></a>Exodus 10:7

And [Parʿô](../../strongs/h/h6547.md) ['ebed](../../strongs/h/h5650.md) ['āmar](../../strongs/h/h559.md) unto him, How long shall this man be a [mowqesh](../../strongs/h/h4170.md) unto us? let the ['enowsh](../../strongs/h/h582.md) [shalach](../../strongs/h/h7971.md), that they may ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md): [yada'](../../strongs/h/h3045.md) thou not yet that [Mitsrayim](../../strongs/h/h4714.md) is ['abad](../../strongs/h/h6.md)?

<a name="exodus_10_8"></a>Exodus 10:8

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [shuwb](../../strongs/h/h7725.md) unto [Parʿô](../../strongs/h/h6547.md): and he ['āmar](../../strongs/h/h559.md) unto them, [yālaḵ](../../strongs/h/h3212.md), ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md): but who are they that shall [halak](../../strongs/h/h1980.md)?

<a name="exodus_10_9"></a>Exodus 10:9

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), We will [yālaḵ](../../strongs/h/h3212.md) with our [naʿar](../../strongs/h/h5288.md) and with our [zāqēn](../../strongs/h/h2205.md), with our [ben](../../strongs/h/h1121.md) and with our [bath](../../strongs/h/h1323.md), with our [tso'n](../../strongs/h/h6629.md) and with our [bāqār](../../strongs/h/h1241.md) will we [yālaḵ](../../strongs/h/h3212.md); for we must hold a [ḥāḡ](../../strongs/h/h2282.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_10_10"></a>Exodus 10:10

And he ['āmar](../../strongs/h/h559.md) unto them, Let [Yĕhovah](../../strongs/h/h3068.md) be so with you, as I will let you [shalach](../../strongs/h/h7971.md), and your [ṭap̄](../../strongs/h/h2945.md): [ra'ah](../../strongs/h/h7200.md) to it; for [ra'](../../strongs/h/h7451.md) is [paniym](../../strongs/h/h6440.md) you.

<a name="exodus_10_11"></a>Exodus 10:11

Not so: [yālaḵ](../../strongs/h/h3212.md) now ye that are [geḇer](../../strongs/h/h1397.md), and ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md); for that ye did [bāqaš](../../strongs/h/h1245.md). And they were [gāraš](../../strongs/h/h1644.md) from [Parʿô](../../strongs/h/h6547.md) [paniym](../../strongs/h/h6440.md).

<a name="exodus_10_12"></a>Exodus 10:12

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [natah](../../strongs/h/h5186.md) thine [yad](../../strongs/h/h3027.md) over the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) for the ['arbê](../../strongs/h/h697.md), that they may [ʿālâ](../../strongs/h/h5927.md) upon the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and ['akal](../../strongs/h/h398.md) every ['eseb](../../strongs/h/h6212.md) of the ['erets](../../strongs/h/h776.md), even all that the [barad](../../strongs/h/h1259.md) hath [šā'ar](../../strongs/h/h7604.md).

<a name="exodus_10_13"></a>Exodus 10:13

And [Mōshe](../../strongs/h/h4872.md) [natah](../../strongs/h/h5186.md) his [maṭṭê](../../strongs/h/h4294.md) over the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and [Yĕhovah](../../strongs/h/h3068.md) [nāhaḡ](../../strongs/h/h5090.md) a [qāḏîm](../../strongs/h/h6921.md) [ruwach](../../strongs/h/h7307.md) upon the ['erets](../../strongs/h/h776.md) all that [yowm](../../strongs/h/h3117.md), and all that [layil](../../strongs/h/h3915.md); and when it was [boqer](../../strongs/h/h1242.md), the [qāḏîm](../../strongs/h/h6921.md) [ruwach](../../strongs/h/h7307.md) [nasa'](../../strongs/h/h5375.md) the ['arbê](../../strongs/h/h697.md).

<a name="exodus_10_14"></a>Exodus 10:14

And the ['arbê](../../strongs/h/h697.md) [ʿālâ](../../strongs/h/h5927.md) over all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and [nuwach](../../strongs/h/h5117.md) in all the [gᵊḇûl](../../strongs/h/h1366.md) of [Mitsrayim](../../strongs/h/h4714.md): [me'od](../../strongs/h/h3966.md) [kāḇēḏ](../../strongs/h/h3515.md) were they; [paniym](../../strongs/h/h6440.md) them there were no such ['arbê](../../strongs/h/h697.md) as they, neither ['aḥar](../../strongs/h/h310.md) them shall be such.

<a name="exodus_10_15"></a>Exodus 10:15

For they [kāsâ](../../strongs/h/h3680.md) the ['ayin](../../strongs/h/h5869.md) of the ['erets](../../strongs/h/h776.md), so that the ['erets](../../strongs/h/h776.md) was [ḥāšaḵ](../../strongs/h/h2821.md); and they did ['akal](../../strongs/h/h398.md) every ['eseb](../../strongs/h/h6212.md) of the ['erets](../../strongs/h/h776.md), and all the [pĕriy](../../strongs/h/h6529.md) of the ['ets](../../strongs/h/h6086.md) which the [barad](../../strongs/h/h1259.md) had left: and there [yāṯar](../../strongs/h/h3498.md) not any [yereq](../../strongs/h/h3418.md) in the ['ets](../../strongs/h/h6086.md), or in the ['eseb](../../strongs/h/h6212.md) of the [sadeh](../../strongs/h/h7704.md), through all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_10_16"></a>Exodus 10:16

Then [Parʿô](../../strongs/h/h6547.md) [qara'](../../strongs/h/h7121.md) for [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) in [māhar](../../strongs/h/h4116.md); and he ['āmar](../../strongs/h/h559.md), I have [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and against you.

<a name="exodus_10_17"></a>Exodus 10:17

Now therefore [nasa'](../../strongs/h/h5375.md), I pray thee, my [chatta'ath](../../strongs/h/h2403.md) only this [pa'am](../../strongs/h/h6471.md), and [ʿāṯar](../../strongs/h/h6279.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), that he may take[cuwr](../../strongs/h/h5493.md) from me this [maveth](../../strongs/h/h4194.md) only.

<a name="exodus_10_18"></a>Exodus 10:18

And he [yāṣā'](../../strongs/h/h3318.md) from [Parʿô](../../strongs/h/h6547.md), and [ʿāṯar](../../strongs/h/h6279.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_10_19"></a>Exodus 10:19

And [Yĕhovah](../../strongs/h/h3068.md) [hāp̄aḵ](../../strongs/h/h2015.md) a [me'od](../../strongs/h/h3966.md) [ḥāzāq](../../strongs/h/h2389.md) [yam](../../strongs/h/h3220.md) [ruwach](../../strongs/h/h7307.md), which [nasa'](../../strongs/h/h5375.md) the ['arbê](../../strongs/h/h697.md), and [tāqaʿ](../../strongs/h/h8628.md) them into the [Sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md); there [šā'ar](../../strongs/h/h7604.md) not one ['arbê](../../strongs/h/h697.md) in all the [gᵊḇûl](../../strongs/h/h1366.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_10_20"></a>Exodus 10:20

But [Yĕhovah](../../strongs/h/h3068.md) [ḥāzaq](../../strongs/h/h2388.md) [Parʿô](../../strongs/h/h6547.md) [leb](../../strongs/h/h3820.md), so that he would not let the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md).

<a name="exodus_10_21"></a>Exodus 10:21

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [natah](../../strongs/h/h5186.md) thine [yad](../../strongs/h/h3027.md) toward [shamayim](../../strongs/h/h8064.md), that there may be [choshek](../../strongs/h/h2822.md) over the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), even [choshek](../../strongs/h/h2822.md) which may be [māšaš](../../strongs/h/h4959.md).

<a name="exodus_10_22"></a>Exodus 10:22

And [Mōshe](../../strongs/h/h4872.md) [natah](../../strongs/h/h5186.md) his [yad](../../strongs/h/h3027.md) toward [shamayim](../../strongs/h/h8064.md); and there was an ['ăp̄ēlâ](../../strongs/h/h653.md) [choshek](../../strongs/h/h2822.md) in all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) three [yowm](../../strongs/h/h3117.md):

<a name="exodus_10_23"></a>Exodus 10:23

They [ra'ah](../../strongs/h/h7200.md) not ['iysh](../../strongs/h/h376.md) ['ach](../../strongs/h/h251.md), neither [quwm](../../strongs/h/h6965.md) ['iysh](../../strongs/h/h376.md) from his place for three [yowm](../../strongs/h/h3117.md): but all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) had ['owr](../../strongs/h/h216.md) in their [môšāḇ](../../strongs/h/h4186.md).

<a name="exodus_10_24"></a>Exodus 10:24

And [Parʿô](../../strongs/h/h6547.md) [qara'](../../strongs/h/h7121.md) unto [Mōshe](../../strongs/h/h4872.md), and ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) ye, ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md); only let your [tso'n](../../strongs/h/h6629.md) and your [bāqār](../../strongs/h/h1241.md) be [yāṣaḡ](../../strongs/h/h3322.md): let your [ṭap̄](../../strongs/h/h2945.md) also [yālaḵ](../../strongs/h/h3212.md) with you.

<a name="exodus_10_25"></a>Exodus 10:25

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), Thou must [nathan](../../strongs/h/h5414.md) [yad](../../strongs/h/h3027.md) also [zebach](../../strongs/h/h2077.md) and [ʿōlâ](../../strongs/h/h5930.md), that we may ['asah](../../strongs/h/h6213.md) unto [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_10_26"></a>Exodus 10:26

Our [miqnê](../../strongs/h/h4735.md) also shall [yālaḵ](../../strongs/h/h3212.md) with us; there shall not a [parsâ](../../strongs/h/h6541.md) [šā'ar](../../strongs/h/h7604.md); for thereof must we [laqach](../../strongs/h/h3947.md) to ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md); and we [yada'](../../strongs/h/h3045.md) not with what we must ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md), until we [bow'](../../strongs/h/h935.md) thither.

<a name="exodus_10_27"></a>Exodus 10:27

But [Yĕhovah](../../strongs/h/h3068.md) [ḥāzaq](../../strongs/h/h2388.md) [Parʿô](../../strongs/h/h6547.md) [leb](../../strongs/h/h3820.md), and he ['āḇâ](../../strongs/h/h14.md) not let them [shalach](../../strongs/h/h7971.md).

<a name="exodus_10_28"></a>Exodus 10:28

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md) unto him, [yālaḵ](../../strongs/h/h3212.md) thee from me, [shamar](../../strongs/h/h8104.md) to thyself, [ra'ah](../../strongs/h/h7200.md) my [paniym](../../strongs/h/h6440.md) no more; for in that [yowm](../../strongs/h/h3117.md) thou [ra'ah](../../strongs/h/h7200.md) my [paniym](../../strongs/h/h6440.md) thou shalt [muwth](../../strongs/h/h4191.md).

<a name="exodus_10_29"></a>Exodus 10:29

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), Thou hast [dabar](../../strongs/h/h1696.md) [kēn](../../strongs/h/h3651.md), I will [ra'ah](../../strongs/h/h7200.md) thy [paniym](../../strongs/h/h6440.md) [yāsap̄](../../strongs/h/h3254.md) no more.

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 9](exodus_9.md) - [Exodus 11](exodus_11.md)