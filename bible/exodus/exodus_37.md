# [Exodus 37](https://www.blueletterbible.org/kjv/exo/37/1)

<a name="exodus_37_1"></a>Exodus 37:1

And [Bᵊṣal'ēl](../../strongs/h/h1212.md) ['asah](../../strongs/h/h6213.md) the ['ārôn](../../strongs/h/h727.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md): two ['ammâ](../../strongs/h/h520.md) and a half was the ['ōreḵ](../../strongs/h/h753.md) of it, and an ['ammâ](../../strongs/h/h520.md) and a half the [rōḥaḇ](../../strongs/h/h7341.md) of it, and an ['ammâ](../../strongs/h/h520.md) and a half the [qômâ](../../strongs/h/h6967.md) of it:

<a name="exodus_37_2"></a>Exodus 37:2

And he [ṣāp̄â](../../strongs/h/h6823.md) it with [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md) [bayith](../../strongs/h/h1004.md) and [ḥûṣ](../../strongs/h/h2351.md), and ['asah](../../strongs/h/h6213.md) a [zēr](../../strongs/h/h2213.md) of [zāhāḇ](../../strongs/h/h2091.md) to it [cabiyb](../../strongs/h/h5439.md).

<a name="exodus_37_3"></a>Exodus 37:3

And he [yāṣaq](../../strongs/h/h3332.md) for it four [ṭabaʿaṯ](../../strongs/h/h2885.md) of [zāhāḇ](../../strongs/h/h2091.md), to be set by the four [pa'am](../../strongs/h/h6471.md) of it; even two [ṭabaʿaṯ](../../strongs/h/h2885.md) upon the one [tsela'](../../strongs/h/h6763.md) of it, and two [ṭabaʿaṯ](../../strongs/h/h2885.md) upon the other [tsela'](../../strongs/h/h6763.md) of it.

<a name="exodus_37_4"></a>Exodus 37:4

And he ['asah](../../strongs/h/h6213.md) [baḏ](../../strongs/h/h905.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md), and [ṣāp̄â](../../strongs/h/h6823.md) them with [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_37_5"></a>Exodus 37:5

And he [bow'](../../strongs/h/h935.md) the [baḏ](../../strongs/h/h905.md) into the [ṭabaʿaṯ](../../strongs/h/h2885.md) by the [tsela'](../../strongs/h/h6763.md) of the ['ārôn](../../strongs/h/h727.md), to [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md).

<a name="exodus_37_6"></a>Exodus 37:6

And he ['asah](../../strongs/h/h6213.md) the [kapōreṯ](../../strongs/h/h3727.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md): two ['ammâ](../../strongs/h/h520.md) and a half was the ['ōreḵ](../../strongs/h/h753.md) thereof, and one ['ammâ](../../strongs/h/h520.md) and a half the [rōḥaḇ](../../strongs/h/h7341.md) thereof.

<a name="exodus_37_7"></a>Exodus 37:7

And he ['asah](../../strongs/h/h6213.md) two [kĕruwb](../../strongs/h/h3742.md) of [zāhāḇ](../../strongs/h/h2091.md), [miqšâ](../../strongs/h/h4749.md) ['asah](../../strongs/h/h6213.md) he them, on the two [qāṣâ](../../strongs/h/h7098.md) of the [kapōreṯ](../../strongs/h/h3727.md);

<a name="exodus_37_8"></a>Exodus 37:8

One [kĕruwb](../../strongs/h/h3742.md) on the [qāṣâ](../../strongs/h/h7098.md) on this side, and another [kĕruwb](../../strongs/h/h3742.md) on the other [qāṣâ](../../strongs/h/h7098.md) on that side: out of the [kapōreṯ](../../strongs/h/h3727.md) ['asah](../../strongs/h/h6213.md) he the [kĕruwb](../../strongs/h/h3742.md) on the two [qāṣâ](../../strongs/h/h7098.md) [qeṣev](../../strongs/h/h7099.md).

<a name="exodus_37_9"></a>Exodus 37:9

And the [kĕruwb](../../strongs/h/h3742.md) [pāraś](../../strongs/h/h6566.md) their [kanaph](../../strongs/h/h3671.md) on [maʿal](../../strongs/h/h4605.md), and [cakak](../../strongs/h/h5526.md) with their [kanaph](../../strongs/h/h3671.md) over the [kapōreṯ](../../strongs/h/h3727.md), with their [paniym](../../strongs/h/h6440.md) ['iysh](../../strongs/h/h376.md) to ['ach](../../strongs/h/h251.md); even to the [kapōreṯ](../../strongs/h/h3727.md) were the [paniym](../../strongs/h/h6440.md) of the [kĕruwb](../../strongs/h/h3742.md).

<a name="exodus_37_10"></a>Exodus 37:10

And he ['asah](../../strongs/h/h6213.md) the [šulḥān](../../strongs/h/h7979.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md): two ['ammâ](../../strongs/h/h520.md) was the ['ōreḵ](../../strongs/h/h753.md) thereof, and an ['ammâ](../../strongs/h/h520.md) the [rōḥaḇ](../../strongs/h/h7341.md) thereof, and an ['ammâ](../../strongs/h/h520.md) and a half the [qômâ](../../strongs/h/h6967.md) thereof:

<a name="exodus_37_11"></a>Exodus 37:11

And he [ṣāp̄â](../../strongs/h/h6823.md) it with [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md), and ['asah](../../strongs/h/h6213.md) thereunto a [zēr](../../strongs/h/h2213.md) of [zāhāḇ](../../strongs/h/h2091.md) [cabiyb](../../strongs/h/h5439.md).

<a name="exodus_37_12"></a>Exodus 37:12

Also he ['asah](../../strongs/h/h6213.md) thereunto a [misgereṯ](../../strongs/h/h4526.md) of a [ṭōp̄aḥ](../../strongs/h/h2948.md) [cabiyb](../../strongs/h/h5439.md); and ['asah](../../strongs/h/h6213.md) a [zēr](../../strongs/h/h2213.md) of [zāhāḇ](../../strongs/h/h2091.md) for the [misgereṯ](../../strongs/h/h4526.md) thereof [cabiyb](../../strongs/h/h5439.md).

<a name="exodus_37_13"></a>Exodus 37:13

And he [yāṣaq](../../strongs/h/h3332.md) for it four [ṭabaʿaṯ](../../strongs/h/h2885.md) of [zāhāḇ](../../strongs/h/h2091.md), and [nathan](../../strongs/h/h5414.md) the [ṭabaʿaṯ](../../strongs/h/h2885.md) upon the four [pē'â](../../strongs/h/h6285.md) that were in the four [regel](../../strongs/h/h7272.md) thereof.

<a name="exodus_37_14"></a>Exodus 37:14

[ʿummâ](../../strongs/h/h5980.md) the [misgereṯ](../../strongs/h/h4526.md) were the [ṭabaʿaṯ](../../strongs/h/h2885.md), the [bayith](../../strongs/h/h1004.md) for the [baḏ](../../strongs/h/h905.md) to [nasa'](../../strongs/h/h5375.md) the [šulḥān](../../strongs/h/h7979.md).

<a name="exodus_37_15"></a>Exodus 37:15

And he ['asah](../../strongs/h/h6213.md) the [baḏ](../../strongs/h/h905.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md), and [ṣāp̄â](../../strongs/h/h6823.md) them with [zāhāḇ](../../strongs/h/h2091.md), to [nasa'](../../strongs/h/h5375.md) the [šulḥān](../../strongs/h/h7979.md).

<a name="exodus_37_16"></a>Exodus 37:16

And he ['asah](../../strongs/h/h6213.md) the [kĕliy](../../strongs/h/h3627.md) which were upon the [šulḥān](../../strongs/h/h7979.md), his [qᵊʿārâ](../../strongs/h/h7086.md), and his [kaph](../../strongs/h/h3709.md), and his [mᵊnaqqîṯ](../../strongs/h/h4518.md), and his [qaśvâ](../../strongs/h/h7184.md) to [nacak](../../strongs/h/h5258.md) withal, of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_37_17"></a>Exodus 37:17

And he ['asah](../../strongs/h/h6213.md) the [mᵊnôrâ](../../strongs/h/h4501.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md): of [miqšâ](../../strongs/h/h4749.md) ['asah](../../strongs/h/h6213.md) he the [mᵊnôrâ](../../strongs/h/h4501.md); his [yārēḵ](../../strongs/h/h3409.md), and his [qānê](../../strongs/h/h7070.md), his [gāḇîaʿ](../../strongs/h/h1375.md), his [kap̄tôr](../../strongs/h/h3730.md), and his [peraḥ](../../strongs/h/h6525.md), were of the same:

<a name="exodus_37_18"></a>Exodus 37:18

And six [qānê](../../strongs/h/h7070.md) [yāṣā'](../../strongs/h/h3318.md) of the [ṣaḏ](../../strongs/h/h6654.md) thereof; three [qānê](../../strongs/h/h7070.md) of the [mᵊnôrâ](../../strongs/h/h4501.md) out of the one [ṣaḏ](../../strongs/h/h6654.md) thereof, and three [qānê](../../strongs/h/h7070.md) of the [mᵊnôrâ](../../strongs/h/h4501.md) out of the other [ṣaḏ](../../strongs/h/h6654.md) thereof:

<a name="exodus_37_19"></a>Exodus 37:19

Three [gāḇîaʿ](../../strongs/h/h1375.md) made after the fashion of [šāqaḏ](../../strongs/h/h8246.md) in one [qānê](../../strongs/h/h7070.md), a [kap̄tôr](../../strongs/h/h3730.md) and a [peraḥ](../../strongs/h/h6525.md); and three [gāḇîaʿ](../../strongs/h/h1375.md) made like [šāqaḏ](../../strongs/h/h8246.md) in another [qānê](../../strongs/h/h7070.md), a [kap̄tôr](../../strongs/h/h3730.md) and a [peraḥ](../../strongs/h/h6525.md): so throughout the six [qānê](../../strongs/h/h7070.md) [yāṣā'](../../strongs/h/h3318.md) of the [mᵊnôrâ](../../strongs/h/h4501.md).

<a name="exodus_37_20"></a>Exodus 37:20

And in the [mᵊnôrâ](../../strongs/h/h4501.md) were four [gāḇîaʿ](../../strongs/h/h1375.md) made like [šāqaḏ](../../strongs/h/h8246.md), his [kap̄tôr](../../strongs/h/h3730.md), and his [peraḥ](../../strongs/h/h6525.md):

<a name="exodus_37_21"></a>Exodus 37:21

And a [kap̄tôr](../../strongs/h/h3730.md) under two [qānê](../../strongs/h/h7070.md) of the same, and a [kap̄tôr](../../strongs/h/h3730.md) under two [qānê](../../strongs/h/h7070.md) of the same, and a [kap̄tôr](../../strongs/h/h3730.md) under two [qānê](../../strongs/h/h7070.md) of the same, according to the six [qānê](../../strongs/h/h7070.md) [yāṣā'](../../strongs/h/h3318.md) of it.

<a name="exodus_37_22"></a>Exodus 37:22

Their [kap̄tôr](../../strongs/h/h3730.md) and their [qānê](../../strongs/h/h7070.md) were of the same: all of it was one [miqšâ](../../strongs/h/h4749.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_37_23"></a>Exodus 37:23

And he ['asah](../../strongs/h/h6213.md) his seven [nîr](../../strongs/h/h5216.md), and his [malqāḥayim](../../strongs/h/h4457.md), and his [maḥtâ](../../strongs/h/h4289.md), of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_37_24"></a>Exodus 37:24

Of a [kikār](../../strongs/h/h3603.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md) ['asah](../../strongs/h/h6213.md) he it, and all the [kĕliy](../../strongs/h/h3627.md) thereof.

<a name="exodus_37_25"></a>Exodus 37:25

And he ['asah](../../strongs/h/h6213.md) the [qᵊṭōreṯ](../../strongs/h/h7004.md)a [mizbeach](../../strongs/h/h4196.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md): the ['ōreḵ](../../strongs/h/h753.md) of it was an ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) of it an ['ammâ](../../strongs/h/h520.md); it was [rāḇaʿ](../../strongs/h/h7251.md); and two ['ammâ](../../strongs/h/h520.md) was the [qômâ](../../strongs/h/h6967.md) of it; the [qeren](../../strongs/h/h7161.md) thereof were of the same.

<a name="exodus_37_26"></a>Exodus 37:26

And he [ṣāp̄â](../../strongs/h/h6823.md) it with [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md), both the [gāḡ](../../strongs/h/h1406.md) of it, and the [qîr](../../strongs/h/h7023.md) thereof [cabiyb](../../strongs/h/h5439.md), and the [qeren](../../strongs/h/h7161.md) of it: also he ['asah](../../strongs/h/h6213.md) unto it a [zēr](../../strongs/h/h2213.md) of [zāhāḇ](../../strongs/h/h2091.md) [cabiyb](../../strongs/h/h5439.md).

<a name="exodus_37_27"></a>Exodus 37:27

And he ['asah](../../strongs/h/h6213.md) two [ṭabaʿaṯ](../../strongs/h/h2885.md) of [zāhāḇ](../../strongs/h/h2091.md) for it under the [zēr](../../strongs/h/h2213.md) thereof, by the two [tsela'](../../strongs/h/h6763.md) of it, upon the two [ṣaḏ](../../strongs/h/h6654.md) thereof, to be [bayith](../../strongs/h/h1004.md) for the [baḏ](../../strongs/h/h905.md) to [nasa'](../../strongs/h/h5375.md) it withal.

<a name="exodus_37_28"></a>Exodus 37:28

And he ['asah](../../strongs/h/h6213.md) the [baḏ](../../strongs/h/h905.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md), and [ṣāp̄â](../../strongs/h/h6823.md) them with [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_37_29"></a>Exodus 37:29

And he ['asah](../../strongs/h/h6213.md) the [qodesh](../../strongs/h/h6944.md) [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and the [tahowr](../../strongs/h/h2889.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) of [sam](../../strongs/h/h5561.md), according to the [ma'aseh](../../strongs/h/h4639.md) of the [rāqaḥ](../../strongs/h/h7543.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 36](exodus_36.md) - [Exodus 38](exodus_38.md)