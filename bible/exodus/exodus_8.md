# [Exodus 8](https://www.blueletterbible.org/kjv/exo/8/1/s_58001)

<a name="exodus_8_1"></a>Exodus 8:1

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [bow'](../../strongs/h/h935.md) unto [Parʿô](../../strongs/h/h6547.md), and ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Let my ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), that they may ['abad](../../strongs/h/h5647.md) me.

<a name="exodus_8_2"></a>Exodus 8:2

And if thou [mā'ēn](../../strongs/h/h3986.md) to let them [shalach](../../strongs/h/h7971.md), behold, I will [nāḡap̄](../../strongs/h/h5062.md) all thy [gᵊḇûl](../../strongs/h/h1366.md) with [ṣᵊp̄ardēaʿ](../../strongs/h/h6854.md):

<a name="exodus_8_3"></a>Exodus 8:3

And the [yᵊ'ōr](../../strongs/h/h2975.md) shall [šāraṣ](../../strongs/h/h8317.md) [ṣᵊp̄ardēaʿ](../../strongs/h/h6854.md), which shall [ʿālâ](../../strongs/h/h5927.md) and [bow'](../../strongs/h/h935.md) into thine [bayith](../../strongs/h/h1004.md), and into thy [ḥeḏer](../../strongs/h/h2315.md) [miškāḇ](../../strongs/h/h4904.md), and upon thy [mittah](../../strongs/h/h4296.md), and into the [bayith](../../strongs/h/h1004.md) of thy ['ebed](../../strongs/h/h5650.md), and upon thy ['am](../../strongs/h/h5971.md), and into thine [tannûr](../../strongs/h/h8574.md), and into thy [miš'ereṯ](../../strongs/h/h4863.md):

<a name="exodus_8_4"></a>Exodus 8:4

And the [ṣᵊp̄ardēaʿ](../../strongs/h/h6854.md) shall [ʿālâ](../../strongs/h/h5927.md) both on thee, and upon thy ['am](../../strongs/h/h5971.md), and upon all thy ['ebed](../../strongs/h/h5650.md).

<a name="exodus_8_5"></a>Exodus 8:5

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), [natah](../../strongs/h/h5186.md) thine [yad](../../strongs/h/h3027.md) with thy [maṭṭê](../../strongs/h/h4294.md) over the [nāhār](../../strongs/h/h5104.md), over the [yᵊ'ōr](../../strongs/h/h2975.md), and over the ['ăḡam](../../strongs/h/h98.md), and cause [ṣᵊp̄ardēaʿ](../../strongs/h/h6854.md) to [ʿālâ](../../strongs/h/h5927.md) upon the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_8_6"></a>Exodus 8:6

And ['Ahărôn](../../strongs/h/h175.md) [natah](../../strongs/h/h5186.md) his [yad](../../strongs/h/h3027.md) over the [mayim](../../strongs/h/h4325.md) of [Mitsrayim](../../strongs/h/h4714.md); and the [ṣᵊp̄ardēaʿ](../../strongs/h/h6854.md) [ʿālâ](../../strongs/h/h5927.md), and [kāsâ](../../strongs/h/h3680.md) the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_8_7"></a>Exodus 8:7

And the [ḥarṭōm](../../strongs/h/h2748.md) ['asah](../../strongs/h/h6213.md) so with their [lāṭ](../../strongs/h/h3909.md), and [ʿālâ](../../strongs/h/h5927.md) [ṣᵊp̄ardēaʿ](../../strongs/h/h6854.md) upon the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_8_8"></a>Exodus 8:8

Then [Parʿô](../../strongs/h/h6547.md) [qara'](../../strongs/h/h7121.md) for [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md), and ['āmar](../../strongs/h/h559.md), [ʿāṯar](../../strongs/h/h6279.md) [Yĕhovah](../../strongs/h/h3068.md), that he may [cuwr](../../strongs/h/h5493.md) the [ṣᵊp̄ardēaʿ](../../strongs/h/h6854.md) from me, and from my ['am](../../strongs/h/h5971.md); and I will let the ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), that they may [zabach](../../strongs/h/h2076.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_8_9"></a>Exodus 8:9

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto [Parʿô](../../strongs/h/h6547.md), [pā'ar](../../strongs/h/h6286.md) over me: when shall I [ʿāṯar](../../strongs/h/h6279.md) for thee, and for thy ['ebed](../../strongs/h/h5650.md), and for thy ['am](../../strongs/h/h5971.md), to [karath](../../strongs/h/h3772.md) the [ṣᵊp̄ardēaʿ](../../strongs/h/h6854.md) from thee and thy [bayith](../../strongs/h/h1004.md), that they may [šā'ar](../../strongs/h/h7604.md) in the [yᵊ'ōr](../../strongs/h/h2975.md) only?

<a name="exodus_8_10"></a>Exodus 8:10

And he ['āmar](../../strongs/h/h559.md), [māḥār](../../strongs/h/h4279.md). And he ['āmar](../../strongs/h/h559.md), Be it according to thy [dabar](../../strongs/h/h1697.md): that thou mayest [yada'](../../strongs/h/h3045.md) that there is none like unto [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_8_11"></a>Exodus 8:11

And the [ṣᵊp̄ardēaʿ](../../strongs/h/h6854.md) shall [cuwr](../../strongs/h/h5493.md) from thee, and from thy [bayith](../../strongs/h/h1004.md), and from thy ['ebed](../../strongs/h/h5650.md), and from thy ['am](../../strongs/h/h5971.md); they shall [šā'ar](../../strongs/h/h7604.md) in the [yᵊ'ōr](../../strongs/h/h2975.md) only.

<a name="exodus_8_12"></a>Exodus 8:12

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [yāṣā'](../../strongs/h/h3318.md) from [Parʿô](../../strongs/h/h6547.md): and [Mōshe](../../strongs/h/h4872.md) [ṣāʿaq](../../strongs/h/h6817.md) unto [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1697.md) the [ṣᵊp̄ardēaʿ](../../strongs/h/h6854.md) which he had [śûm](../../strongs/h/h7760.md) against [Parʿô](../../strongs/h/h6547.md).

<a name="exodus_8_13"></a>Exodus 8:13

And [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) according to the [dabar](../../strongs/h/h1697.md) of [Mōshe](../../strongs/h/h4872.md); and the [ṣᵊp̄ardēaʿ](../../strongs/h/h6854.md) [muwth](../../strongs/h/h4191.md) out of the [bayith](../../strongs/h/h1004.md), out of the [ḥāṣēr](../../strongs/h/h2691.md), and out of the [sadeh](../../strongs/h/h7704.md).

<a name="exodus_8_14"></a>Exodus 8:14

And they [ṣāḇar](../../strongs/h/h6651.md) them upon [ḥōmer](../../strongs/h/h2563.md): and the ['erets](../../strongs/h/h776.md) [bā'aš](../../strongs/h/h887.md).

<a name="exodus_8_15"></a>Exodus 8:15

But when [Parʿô](../../strongs/h/h6547.md) [ra'ah](../../strongs/h/h7200.md) that there was [rᵊvāḥâ](../../strongs/h/h7309.md), he [kabad](../../strongs/h/h3513.md) his [leb](../../strongs/h/h3820.md), and [shama'](../../strongs/h/h8085.md) not unto them; as [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md).

<a name="exodus_8_16"></a>Exodus 8:16

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), [natah](../../strongs/h/h5186.md) thy [maṭṭê](../../strongs/h/h4294.md), and [nakah](../../strongs/h/h5221.md) the ['aphar](../../strongs/h/h6083.md) of the ['erets](../../strongs/h/h776.md), that it may become [kēn](../../strongs/h/h3654.md) throughout all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_8_17"></a>Exodus 8:17

And they ['asah](../../strongs/h/h6213.md) so; for ['Ahărôn](../../strongs/h/h175.md) [natah](../../strongs/h/h5186.md) his [yad](../../strongs/h/h3027.md) with his [maṭṭê](../../strongs/h/h4294.md), and [nakah](../../strongs/h/h5221.md) the ['aphar](../../strongs/h/h6083.md) of the ['erets](../../strongs/h/h776.md), and it became [kēn](../../strongs/h/h3654.md) in ['adam](../../strongs/h/h120.md), and in [bĕhemah](../../strongs/h/h929.md); all the ['aphar](../../strongs/h/h6083.md) of the ['erets](../../strongs/h/h776.md) became [kēn](../../strongs/h/h3654.md) throughout all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_8_18"></a>Exodus 8:18

And the [ḥarṭōm](../../strongs/h/h2748.md) ['asah](../../strongs/h/h6213.md) so with their [lāṭ](../../strongs/h/h3909.md) to [yāṣā'](../../strongs/h/h3318.md) [kēn](../../strongs/h/h3654.md), but they [yakol](../../strongs/h/h3201.md) not: so there were [kēn](../../strongs/h/h3654.md) upon ['adam](../../strongs/h/h120.md), and upon [bĕhemah](../../strongs/h/h929.md).

<a name="exodus_8_19"></a>Exodus 8:19

Then the [ḥarṭōm](../../strongs/h/h2748.md) ['āmar](../../strongs/h/h559.md) unto [Parʿô](../../strongs/h/h6547.md), This is the ['etsba'](../../strongs/h/h676.md) of ['Elohiym](../../strongs/h/h430.md): and [Parʿô](../../strongs/h/h6547.md) [leb](../../strongs/h/h3820.md) was [ḥāzaq](../../strongs/h/h2388.md), and he [shama'](../../strongs/h/h8085.md) not unto them; as [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md).

<a name="exodus_8_20"></a>Exodus 8:20

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [yatsab](../../strongs/h/h3320.md) [paniym](../../strongs/h/h6440.md) [Parʿô](../../strongs/h/h6547.md); lo, he [yāṣā'](../../strongs/h/h3318.md) to the [mayim](../../strongs/h/h4325.md); and ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Let my ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), that they may ['abad](../../strongs/h/h5647.md) me.

<a name="exodus_8_21"></a>Exodus 8:21

Else, if thou wilt not let my ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), behold, I will [shalach](../../strongs/h/h7971.md) [ʿārōḇ](../../strongs/h/h6157.md) upon thee, and upon thy ['ebed](../../strongs/h/h5650.md), and upon thy ['am](../../strongs/h/h5971.md), and into thy [bayith](../../strongs/h/h1004.md): and the [bayith](../../strongs/h/h1004.md) of the [Mitsrayim](../../strongs/h/h4714.md) shall be [mālā'](../../strongs/h/h4390.md) of [ʿārōḇ](../../strongs/h/h6157.md), and also the ['adamah](../../strongs/h/h127.md) whereon they are.

<a name="exodus_8_22"></a>Exodus 8:22

And I will [palah](../../strongs/h/h6395.md) in that [yowm](../../strongs/h/h3117.md) the ['erets](../../strongs/h/h776.md) of [gōšen](../../strongs/h/h1657.md), in which my ['am](../../strongs/h/h5971.md) ['amad](../../strongs/h/h5975.md), that no [ʿārōḇ](../../strongs/h/h6157.md) shall be there; to the [maʿan](../../strongs/h/h4616.md) thou mayest [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md) in the [qereḇ](../../strongs/h/h7130.md) of the ['erets](../../strongs/h/h776.md).

<a name="exodus_8_23"></a>Exodus 8:23

And I will [śûm](../../strongs/h/h7760.md) a [pᵊḏûṯ](../../strongs/h/h6304.md) between my ['am](../../strongs/h/h5971.md) and thy ['am](../../strongs/h/h5971.md): [māḥār](../../strongs/h/h4279.md) shall this ['ôṯ](../../strongs/h/h226.md) be.

<a name="exodus_8_24"></a>Exodus 8:24

And [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) so; and there [bow'](../../strongs/h/h935.md) a [kāḇēḏ](../../strongs/h/h3515.md) [ʿārōḇ](../../strongs/h/h6157.md) into the [bayith](../../strongs/h/h1004.md) of [Parʿô](../../strongs/h/h6547.md), and into his ['ebed](../../strongs/h/h5650.md) [bayith](../../strongs/h/h1004.md), and into all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): the ['erets](../../strongs/h/h776.md) was [shachath](../../strongs/h/h7843.md) by [paniym](../../strongs/h/h6440.md) of the [ʿārōḇ](../../strongs/h/h6157.md).

<a name="exodus_8_25"></a>Exodus 8:25

And [Parʿô](../../strongs/h/h6547.md) [qara'](../../strongs/h/h7121.md) for [Mōshe](../../strongs/h/h4872.md) and for ['Ahărôn](../../strongs/h/h175.md), and ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) ye, [zabach](../../strongs/h/h2076.md) to your ['Elohiym](../../strongs/h/h430.md) in the ['erets](../../strongs/h/h776.md).

<a name="exodus_8_26"></a>Exodus 8:26

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), It is not [kuwn](../../strongs/h/h3559.md) so to ['asah](../../strongs/h/h6213.md); [hen](../../strongs/h/h2005.md) we shall [zabach](../../strongs/h/h2076.md) the [tôʿēḇâ](../../strongs/h/h8441.md) of the [Mitsrayim](../../strongs/h/h4714.md) to [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md): lo, shall we [zabach](../../strongs/h/h2076.md) the [tôʿēḇâ](../../strongs/h/h8441.md) of the [Mitsrayim](../../strongs/h/h4714.md) before their ['ayin](../../strongs/h/h5869.md), and will they not [sāqal](../../strongs/h/h5619.md) us?

<a name="exodus_8_27"></a>Exodus 8:27

We will [yālaḵ](../../strongs/h/h3212.md) three [yowm](../../strongs/h/h3117.md) [derek](../../strongs/h/h1870.md) into the [midbar](../../strongs/h/h4057.md), and [zabach](../../strongs/h/h2076.md) to [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), as he shall ['āmar](../../strongs/h/h559.md) us.

<a name="exodus_8_28"></a>Exodus 8:28

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md), I will let you [shalach](../../strongs/h/h7971.md), that ye may [zabach](../../strongs/h/h2076.md) to [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) in the [midbar](../../strongs/h/h4057.md); only ye shall not [yālaḵ](../../strongs/h/h3212.md) [rachaq](../../strongs/h/h7368.md) [rachaq](../../strongs/h/h7368.md): [ʿāṯar](../../strongs/h/h6279.md) for me.

<a name="exodus_8_29"></a>Exodus 8:29

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), Behold, I [yāṣā'](../../strongs/h/h3318.md) from thee, and I will [ʿāṯar](../../strongs/h/h6279.md) [Yĕhovah](../../strongs/h/h3068.md) that the [ʿārōḇ](../../strongs/h/h6157.md) may [cuwr](../../strongs/h/h5493.md) from [Parʿô](../../strongs/h/h6547.md), from his ['ebed](../../strongs/h/h5650.md), and from his ['am](../../strongs/h/h5971.md), [māḥār](../../strongs/h/h4279.md): but let not [Parʿô](../../strongs/h/h6547.md) [hāṯal](../../strongs/h/h2048.md) any more in not letting the ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md) to [zabach](../../strongs/h/h2076.md) to [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_8_30"></a>Exodus 8:30

And [Mōshe](../../strongs/h/h4872.md) [yāṣā'](../../strongs/h/h3318.md) from [Parʿô](../../strongs/h/h6547.md), and [ʿāṯar](../../strongs/h/h6279.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_8_31"></a>Exodus 8:31

And [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) according to the [dabar](../../strongs/h/h1697.md) of [Mōshe](../../strongs/h/h4872.md); and he [cuwr](../../strongs/h/h5493.md) the [ʿārōḇ](../../strongs/h/h6157.md) from [Parʿô](../../strongs/h/h6547.md), from his ['ebed](../../strongs/h/h5650.md), and from his ['am](../../strongs/h/h5971.md); there [šā'ar](../../strongs/h/h7604.md) not ['echad](../../strongs/h/h259.md).

<a name="exodus_8_32"></a>Exodus 8:32

And [Parʿô](../../strongs/h/h6547.md) [kabad](../../strongs/h/h3513.md) his [leb](../../strongs/h/h3820.md) at this [pa'am](../../strongs/h/h6471.md) also, neither would he let the ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 7](exodus_7.md) - [Exodus 9](exodus_9.md)