# [Exodus 5](https://www.blueletterbible.org/kjv/exo/5/1/s_55001)

<a name="exodus_5_1"></a>Exodus 5:1

And ['aḥar](../../strongs/h/h310.md) [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [bow'](../../strongs/h/h935.md), and ['āmar](../../strongs/h/h559.md) [Parʿô](../../strongs/h/h6547.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), Let my ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), that they may [ḥāḡaḡ](../../strongs/h/h2287.md) unto me in the [midbar](../../strongs/h/h4057.md).

<a name="exodus_5_2"></a>Exodus 5:2

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md), Who is [Yĕhovah](../../strongs/h/h3068.md), that I should [shama'](../../strongs/h/h8085.md) his [qowl](../../strongs/h/h6963.md) to let [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md)? I [yada'](../../strongs/h/h3045.md) not [Yĕhovah](../../strongs/h/h3068.md), neither will I let [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md).

<a name="exodus_5_3"></a>Exodus 5:3

And they ['āmar](../../strongs/h/h559.md), The ['Elohiym](../../strongs/h/h430.md) of the [ʿiḇrî](../../strongs/h/h5680.md) hath [qārā'](../../strongs/h/h7122.md) with us: let us [yālaḵ](../../strongs/h/h3212.md), we pray thee, three [yowm](../../strongs/h/h3117.md) [derek](../../strongs/h/h1870.md) into the [midbar](../../strongs/h/h4057.md), and [zabach](../../strongs/h/h2076.md) unto [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md); lest he [pāḡaʿ](../../strongs/h/h6293.md) upon us with [deḇer](../../strongs/h/h1698.md), or with the [chereb](../../strongs/h/h2719.md).

<a name="exodus_5_4"></a>Exodus 5:4

And the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) ['āmar](../../strongs/h/h559.md) unto them, Wherefore do ye, [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md), [pāraʿ](../../strongs/h/h6544.md) the ['am](../../strongs/h/h5971.md) from their [ma'aseh](../../strongs/h/h4639.md)? [yālaḵ](../../strongs/h/h3212.md) you unto your [sᵊḇālâ](../../strongs/h/h5450.md).

<a name="exodus_5_5"></a>Exodus 5:5

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md), Behold, the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) now are [rab](../../strongs/h/h7227.md), and ye make them [shabath](../../strongs/h/h7673.md) from their [sᵊḇālâ](../../strongs/h/h5450.md).

<a name="exodus_5_6"></a>Exodus 5:6

And [Parʿô](../../strongs/h/h6547.md) [tsavah](../../strongs/h/h6680.md) the same [yowm](../../strongs/h/h3117.md) the [nāḡaś](../../strongs/h/h5065.md) of the ['am](../../strongs/h/h5971.md), and their [šāṭar](../../strongs/h/h7860.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_5_7"></a>Exodus 5:7

Ye shall no [yāsap̄](../../strongs/h/h3254.md) [nathan](../../strongs/h/h5414.md) the ['am](../../strongs/h/h5971.md) [teḇen](../../strongs/h/h8401.md) to [lāḇan](../../strongs/h/h3835.md) [lᵊḇēnâ](../../strongs/h/h3843.md), as [šilšôm](../../strongs/h/h8032.md) [tᵊmôl](../../strongs/h/h8543.md): let them [yālaḵ](../../strongs/h/h3212.md) and [qāšaš](../../strongs/h/h7197.md) [teḇen](../../strongs/h/h8401.md) for themselves.

<a name="exodus_5_8"></a>Exodus 5:8

And the [maṯkōneṯ](../../strongs/h/h4971.md) of the [lᵊḇēnâ](../../strongs/h/h3843.md), which they did ['asah](../../strongs/h/h6213.md) [šilšôm](../../strongs/h/h8032.md) [tᵊmôl](../../strongs/h/h8543.md), ye shall [śûm](../../strongs/h/h7760.md) upon them; ye shall not [gāraʿ](../../strongs/h/h1639.md) ought thereof: for they be [rāp̄â](../../strongs/h/h7503.md); therefore they [ṣāʿaq](../../strongs/h/h6817.md), ['āmar](../../strongs/h/h559.md), Let us [yālaḵ](../../strongs/h/h3212.md) and [zabach](../../strongs/h/h2076.md) to our ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_5_9"></a>Exodus 5:9

Let there more [ʿăḇōḏâ](../../strongs/h/h5656.md) be [kabad](../../strongs/h/h3513.md) upon the ['enowsh](../../strongs/h/h582.md), that they may ['asah](../../strongs/h/h6213.md) therein; and let them not [šāʿâ](../../strongs/h/h8159.md) [sheqer](../../strongs/h/h8267.md) [dabar](../../strongs/h/h1697.md).

<a name="exodus_5_10"></a>Exodus 5:10

And the [nāḡaś](../../strongs/h/h5065.md) of the ['am](../../strongs/h/h5971.md) [yāṣā'](../../strongs/h/h3318.md), and their [šāṭar](../../strongs/h/h7860.md), and they ['āmar](../../strongs/h/h559.md) to the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Parʿô](../../strongs/h/h6547.md), I will not [nathan](../../strongs/h/h5414.md) you [teḇen](../../strongs/h/h8401.md).

<a name="exodus_5_11"></a>Exodus 5:11

[yālaḵ](../../strongs/h/h3212.md) ye, [laqach](../../strongs/h/h3947.md) you [teḇen](../../strongs/h/h8401.md) where ye can [māṣā'](../../strongs/h/h4672.md) it: yet not [dabar](../../strongs/h/h1697.md) of your [ʿăḇōḏâ](../../strongs/h/h5656.md) shall be [gāraʿ](../../strongs/h/h1639.md).

<a name="exodus_5_12"></a>Exodus 5:12

So the ['am](../../strongs/h/h5971.md) were [puwts](../../strongs/h/h6327.md) throughout all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) to [qāšaš](../../strongs/h/h7197.md) [qaš](../../strongs/h/h7179.md) instead of [teḇen](../../strongs/h/h8401.md).

<a name="exodus_5_13"></a>Exodus 5:13

And the [nāḡaś](../../strongs/h/h5065.md) ['ûṣ](../../strongs/h/h213.md) them, ['āmar](../../strongs/h/h559.md), [kalah](../../strongs/h/h3615.md) your [ma'aseh](../../strongs/h/h4639.md), your [yowm](../../strongs/h/h3117.md) [dabar](../../strongs/h/h1697.md), as when there was [teḇen](../../strongs/h/h8401.md).

<a name="exodus_5_14"></a>Exodus 5:14

And the [šāṭar](../../strongs/h/h7860.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which [Parʿô](../../strongs/h/h6547.md) [nāḡaś](../../strongs/h/h5065.md) had [śûm](../../strongs/h/h7760.md) over them, were [nakah](../../strongs/h/h5221.md), and ['āmar](../../strongs/h/h559.md), Wherefore have ye not [kalah](../../strongs/h/h3615.md) your [choq](../../strongs/h/h2706.md) in [lāḇan](../../strongs/h/h3835.md) both [tᵊmôl](../../strongs/h/h8543.md) and to [yowm](../../strongs/h/h3117.md), as [šilšôm](../../strongs/h/h8032.md) [tᵊmôl](../../strongs/h/h8543.md)?

<a name="exodus_5_15"></a>Exodus 5:15

Then the [šāṭar](../../strongs/h/h7860.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) and [ṣāʿaq](../../strongs/h/h6817.md) unto [Parʿô](../../strongs/h/h6547.md), ['āmar](../../strongs/h/h559.md), Wherefore ['asah](../../strongs/h/h6213.md) thou thus with thy ['ebed](../../strongs/h/h5650.md)?

<a name="exodus_5_16"></a>Exodus 5:16

There is no [teḇen](../../strongs/h/h8401.md) [nathan](../../strongs/h/h5414.md) unto thy ['ebed](../../strongs/h/h5650.md), and they ['āmar](../../strongs/h/h559.md) to us, ['asah](../../strongs/h/h6213.md) [lᵊḇēnâ](../../strongs/h/h3843.md): and, behold, thy ['ebed](../../strongs/h/h5650.md) are [nakah](../../strongs/h/h5221.md); but the [chata'](../../strongs/h/h2398.md) is in thine own ['am](../../strongs/h/h5971.md).

<a name="exodus_5_17"></a>Exodus 5:17

But he ['āmar](../../strongs/h/h559.md), Ye are [rāp̄â](../../strongs/h/h7503.md), ye are [rāp̄â](../../strongs/h/h7503.md): therefore ye ['āmar](../../strongs/h/h559.md), Let us [yālaḵ](../../strongs/h/h3212.md) and [zabach](../../strongs/h/h2076.md) to [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_5_18"></a>Exodus 5:18

[yālaḵ](../../strongs/h/h3212.md) therefore now, and ['abad](../../strongs/h/h5647.md); for there shall no [teḇen](../../strongs/h/h8401.md) be [nathan](../../strongs/h/h5414.md) you, yet shall ye [nathan](../../strongs/h/h5414.md) the [tōḵen](../../strongs/h/h8506.md) of [lᵊḇēnâ](../../strongs/h/h3843.md).

<a name="exodus_5_19"></a>Exodus 5:19

And the [šāṭar](../../strongs/h/h7860.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) did [ra'ah](../../strongs/h/h7200.md) that they were in [ra'](../../strongs/h/h7451.md) case, after it was ['āmar](../../strongs/h/h559.md), Ye shall not [gāraʿ](../../strongs/h/h1639.md) ought from your [lᵊḇēnâ](../../strongs/h/h3843.md) of your [yowm](../../strongs/h/h3117.md) [dabar](../../strongs/h/h1697.md).

<a name="exodus_5_20"></a>Exodus 5:20

And they [pāḡaʿ](../../strongs/h/h6293.md) [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md), who [nāṣaḇ](../../strongs/h/h5324.md) in the [qārā'](../../strongs/h/h7125.md), as they [yāṣā'](../../strongs/h/h3318.md) from [Parʿô](../../strongs/h/h6547.md):

<a name="exodus_5_21"></a>Exodus 5:21

And they ['āmar](../../strongs/h/h559.md) unto them, [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) upon you, and [shaphat](../../strongs/h/h8199.md); because ye have made our [rêaḥ](../../strongs/h/h7381.md) to [bā'aš](../../strongs/h/h887.md) in the ['ayin](../../strongs/h/h5869.md) of [Parʿô](../../strongs/h/h6547.md), and in the ['ayin](../../strongs/h/h5869.md) of his ['ebed](../../strongs/h/h5650.md), to [nathan](../../strongs/h/h5414.md) a [chereb](../../strongs/h/h2719.md) in their [yad](../../strongs/h/h3027.md) to [harag](../../strongs/h/h2026.md) us.

<a name="exodus_5_22"></a>Exodus 5:22

And [Mōshe](../../strongs/h/h4872.md) [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), ['adonay](../../strongs/h/h136.md), wherefore hast thou [ra'a'](../../strongs/h/h7489.md) this ['am](../../strongs/h/h5971.md)? why is it that thou hast [shalach](../../strongs/h/h7971.md) me?

<a name="exodus_5_23"></a>Exodus 5:23

For since I [bow'](../../strongs/h/h935.md) to [Parʿô](../../strongs/h/h6547.md) to [dabar](../../strongs/h/h1696.md) in thy [shem](../../strongs/h/h8034.md), he hath done [ra'a'](../../strongs/h/h7489.md) to this ['am](../../strongs/h/h5971.md); neither hast thou [natsal](../../strongs/h/h5337.md) thy ['am](../../strongs/h/h5971.md) at all.

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 4](exodus_4.md) - [Exodus 6](exodus_6.md)