# [Exodus 24](https://www.blueletterbible.org/kjv/exo/24/1/)

<a name="exodus_24_1"></a>Exodus 24:1

And he ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [ʿālâ](../../strongs/h/h5927.md) unto [Yĕhovah](../../strongs/h/h3068.md), thou, and ['Ahărôn](../../strongs/h/h175.md), [Nāḏāḇ](../../strongs/h/h5070.md), and ['Ăḇîhû'](../../strongs/h/h30.md), and seventy of the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md); and [shachah](../../strongs/h/h7812.md) ye [rachowq](../../strongs/h/h7350.md).

<a name="exodus_24_2"></a>Exodus 24:2

And [Mōshe](../../strongs/h/h4872.md) alone shall [nāḡaš](../../strongs/h/h5066.md) [Yĕhovah](../../strongs/h/h3068.md): but they shall not [nāḡaš](../../strongs/h/h5066.md); neither shall the ['am](../../strongs/h/h5971.md) [ʿālâ](../../strongs/h/h5927.md) with him.

<a name="exodus_24_3"></a>Exodus 24:3

And [Mōshe](../../strongs/h/h4872.md) [bow'](../../strongs/h/h935.md) and [sāp̄ar](../../strongs/h/h5608.md) the ['am](../../strongs/h/h5971.md) all the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), and all the [mishpat](../../strongs/h/h4941.md): and all the ['am](../../strongs/h/h5971.md) ['anah](../../strongs/h/h6030.md) with one [qowl](../../strongs/h/h6963.md), and ['āmar](../../strongs/h/h559.md), All the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) will we ['asah](../../strongs/h/h6213.md).

<a name="exodus_24_4"></a>Exodus 24:4

And [Mōshe](../../strongs/h/h4872.md) [kāṯaḇ](../../strongs/h/h3789.md) all the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), and [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) under the [har](../../strongs/h/h2022.md), and [maṣṣēḇâ](../../strongs/h/h4676.md), according to the twelve [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="exodus_24_5"></a>Exodus 24:5

And he [shalach](../../strongs/h/h7971.md) [naʿar](../../strongs/h/h5288.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md), and [zabach](../../strongs/h/h2076.md) [šelem](../../strongs/h/h8002.md) [zebach](../../strongs/h/h2077.md) of [par](../../strongs/h/h6499.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_24_6"></a>Exodus 24:6

And [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) [ḥēṣî](../../strongs/h/h2677.md) of the [dam](../../strongs/h/h1818.md), and [śûm](../../strongs/h/h7760.md) it in ['agān](../../strongs/h/h101.md); and [ḥēṣî](../../strongs/h/h2677.md) of the [dam](../../strongs/h/h1818.md) he [zāraq](../../strongs/h/h2236.md) on thea [mizbeach](../../strongs/h/h4196.md).

<a name="exodus_24_7"></a>Exodus 24:7

And he [laqach](../../strongs/h/h3947.md) the [sēp̄er](../../strongs/h/h5612.md) of the [bĕriyth](../../strongs/h/h1285.md), and [qara'](../../strongs/h/h7121.md) in the ['ozen](../../strongs/h/h241.md) of the ['am](../../strongs/h/h5971.md): and they ['āmar](../../strongs/h/h559.md), All that [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) will we ['asah](../../strongs/h/h6213.md), and [shama'](../../strongs/h/h8085.md).

<a name="exodus_24_8"></a>Exodus 24:8

And [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) the [dam](../../strongs/h/h1818.md), and [zāraq](../../strongs/h/h2236.md) it on the ['am](../../strongs/h/h5971.md), and ['āmar](../../strongs/h/h559.md), Behold the [dam](../../strongs/h/h1818.md) of the [bĕriyth](../../strongs/h/h1285.md), which [Yĕhovah](../../strongs/h/h3068.md) hath [karath](../../strongs/h/h3772.md) with you concerning all these [dabar](../../strongs/h/h1697.md).

<a name="exodus_24_9"></a>Exodus 24:9

Then [ʿālâ](../../strongs/h/h5927.md) [Mōshe](../../strongs/h/h4872.md), and ['Ahărôn](../../strongs/h/h175.md), [Nāḏāḇ](../../strongs/h/h5070.md), and ['Ăḇîhû'](../../strongs/h/h30.md), and seventy of the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="exodus_24_10"></a>Exodus 24:10

And they [ra'ah](../../strongs/h/h7200.md) the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md): and there was under his [regel](../../strongs/h/h7272.md) as it were a [Liḇnâ](../../strongs/h/h3840.md) [ma'aseh](../../strongs/h/h4639.md) of a [sapîr](../../strongs/h/h5601.md), and as it were the ['etsem](../../strongs/h/h6106.md) of [shamayim](../../strongs/h/h8064.md) in his [ṭōhar](../../strongs/h/h2892.md).

<a name="exodus_24_11"></a>Exodus 24:11

And upon the ['āṣîl](../../strongs/h/h678.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) he [shalach](../../strongs/h/h7971.md) not his [yad](../../strongs/h/h3027.md): also they [chazah](../../strongs/h/h2372.md) ['Elohiym](../../strongs/h/h430.md), and did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md).

<a name="exodus_24_12"></a>Exodus 24:12

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [ʿālâ](../../strongs/h/h5927.md) to me into the [har](../../strongs/h/h2022.md), and be there: and I will [nathan](../../strongs/h/h5414.md) thee [lûaḥ](../../strongs/h/h3871.md) of ['eben](../../strongs/h/h68.md), and a [towrah](../../strongs/h/h8451.md), and [mitsvah](../../strongs/h/h4687.md) which I have [kāṯaḇ](../../strongs/h/h3789.md); that thou mayest [yārâ](../../strongs/h/h3384.md) them.

<a name="exodus_24_13"></a>Exodus 24:13

And [Mōshe](../../strongs/h/h4872.md) [quwm](../../strongs/h/h6965.md), and his [sharath](../../strongs/h/h8334.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md): and [Mōshe](../../strongs/h/h4872.md) [ʿālâ](../../strongs/h/h5927.md) into the [har](../../strongs/h/h2022.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_24_14"></a>Exodus 24:14

And he ['āmar](../../strongs/h/h559.md) unto the [zāqēn](../../strongs/h/h2205.md), [yashab](../../strongs/h/h3427.md) ye here for us, until we [shuwb](../../strongs/h/h7725.md) unto you: and, behold, ['Ahărôn](../../strongs/h/h175.md) and [Ḥûr](../../strongs/h/h2354.md) are with you: if any [baʿal](../../strongs/h/h1167.md) have any [dabar](../../strongs/h/h1697.md) to do, let him [nāḡaš](../../strongs/h/h5066.md) unto them.

<a name="exodus_24_15"></a>Exodus 24:15

And [Mōshe](../../strongs/h/h4872.md) [ʿālâ](../../strongs/h/h5927.md) into the [har](../../strongs/h/h2022.md), and a [ʿānān](../../strongs/h/h6051.md) [kāsâ](../../strongs/h/h3680.md) the [har](../../strongs/h/h2022.md).

<a name="exodus_24_16"></a>Exodus 24:16

And the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [shakan](../../strongs/h/h7931.md) upon [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md), and the [ʿānān](../../strongs/h/h6051.md) [kāsâ](../../strongs/h/h3680.md) it [šēš](../../strongs/h/h8337.md) [yowm](../../strongs/h/h3117.md): and the [šᵊḇîʿî](../../strongs/h/h7637.md) [yowm](../../strongs/h/h3117.md) he [qara'](../../strongs/h/h7121.md) unto [Mōshe](../../strongs/h/h4872.md) out of the [tavek](../../strongs/h/h8432.md) of the [ʿānān](../../strongs/h/h6051.md).

<a name="exodus_24_17"></a>Exodus 24:17

And the [mar'ê](../../strongs/h/h4758.md) of the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) was like ['akal](../../strongs/h/h398.md) ['esh](../../strongs/h/h784.md) on the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md) in the ['ayin](../../strongs/h/h5869.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="exodus_24_18"></a>Exodus 24:18

And [Mōshe](../../strongs/h/h4872.md) [bow'](../../strongs/h/h935.md) into the [tavek](../../strongs/h/h8432.md) of the [ʿānān](../../strongs/h/h6051.md), and [ʿālâ](../../strongs/h/h5927.md) into the [har](../../strongs/h/h2022.md): and [Mōshe](../../strongs/h/h4872.md) was in the [har](../../strongs/h/h2022.md) ['arbāʿîm](../../strongs/h/h705.md) [yowm](../../strongs/h/h3117.md) and ['arbāʿîm](../../strongs/h/h705.md) [layil](../../strongs/h/h3915.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 23](exodus_23.md) - [Exodus 25](exodus_25.md)