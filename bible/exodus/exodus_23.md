# [Exodus 23](https://www.blueletterbible.org/kjv/exo/23/1/)

<a name="exodus_23_1"></a>Exodus 23:1

Thou shalt not [nasa'](../../strongs/h/h5375.md) a [shav'](../../strongs/h/h7723.md) [šēmaʿ](../../strongs/h/h8088.md): [shiyth](../../strongs/h/h7896.md) not thine [yad](../../strongs/h/h3027.md) with the [rasha'](../../strongs/h/h7563.md) to be a [chamac](../../strongs/h/h2555.md) ['ed](../../strongs/h/h5707.md).

<a name="exodus_23_2"></a>Exodus 23:2

Thou shalt not ['aḥar](../../strongs/h/h310.md) a [rab](../../strongs/h/h7227.md) to do [ra'](../../strongs/h/h7451.md); neither shalt thou ['anah](../../strongs/h/h6030.md) in a [rîḇ](../../strongs/h/h7379.md) to [natah](../../strongs/h/h5186.md) after [rab](../../strongs/h/h7227.md) to [natah](../../strongs/h/h5186.md):

<a name="exodus_23_3"></a>Exodus 23:3

Neither shalt thou [hāḏar](../../strongs/h/h1921.md) a [dal](../../strongs/h/h1800.md) man in his [rîḇ](../../strongs/h/h7379.md).

<a name="exodus_23_4"></a>Exodus 23:4

If thou [pāḡaʿ](../../strongs/h/h6293.md) thine ['oyeb](../../strongs/h/h341.md) [showr](../../strongs/h/h7794.md) or his [chamowr](../../strongs/h/h2543.md) going [tāʿâ](../../strongs/h/h8582.md), thou shalt [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md).

<a name="exodus_23_5"></a>Exodus 23:5

If thou [ra'ah](../../strongs/h/h7200.md) the [chamowr](../../strongs/h/h2543.md) of him that [sane'](../../strongs/h/h8130.md) thee [rāḇaṣ](../../strongs/h/h7257.md) under his [maśśā'](../../strongs/h/h4853.md), and wouldest [ḥāḏal](../../strongs/h/h2308.md) to ['azab](../../strongs/h/h5800.md) him, thou shalt ['azab](../../strongs/h/h5800.md) ['azab](../../strongs/h/h5800.md) with him.

<a name="exodus_23_6"></a>Exodus 23:6

Thou shalt not [natah](../../strongs/h/h5186.md) the [mishpat](../../strongs/h/h4941.md) of thy ['ebyown](../../strongs/h/h34.md) in his [rîḇ](../../strongs/h/h7379.md).

<a name="exodus_23_7"></a>Exodus 23:7

[rachaq](../../strongs/h/h7368.md) from a [sheqer](../../strongs/h/h8267.md) [dabar](../../strongs/h/h1697.md); and the [naqiy](../../strongs/h/h5355.md) and [tsaddiyq](../../strongs/h/h6662.md) [harag](../../strongs/h/h2026.md) thou not: for I will not [ṣāḏaq](../../strongs/h/h6663.md) the [rasha'](../../strongs/h/h7563.md).

<a name="exodus_23_8"></a>Exodus 23:8

And thou shalt [laqach](../../strongs/h/h3947.md) no [shachad](../../strongs/h/h7810.md): for the [shachad](../../strongs/h/h7810.md) [ʿāvar](../../strongs/h/h5786.md) the [piqqēaḥ](../../strongs/h/h6493.md), and [sālap̄](../../strongs/h/h5557.md) the [dabar](../../strongs/h/h1697.md) of the [tsaddiyq](../../strongs/h/h6662.md).

<a name="exodus_23_9"></a>Exodus 23:9

Also thou shalt not [lāḥaṣ](../../strongs/h/h3905.md) a [ger](../../strongs/h/h1616.md): for ye [yada'](../../strongs/h/h3045.md) the [nephesh](../../strongs/h/h5315.md) of a [ger](../../strongs/h/h1616.md), [kî](../../strongs/h/h3588.md) ye were [ger](../../strongs/h/h1616.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_23_10"></a>Exodus 23:10

And six [šānâ](../../strongs/h/h8141.md) thou shalt [zāraʿ](../../strongs/h/h2232.md) thy ['erets](../../strongs/h/h776.md), and shalt ['āsap̄](../../strongs/h/h622.md) in the [tᵊḇû'â](../../strongs/h/h8393.md) thereof:

<a name="exodus_23_11"></a>Exodus 23:11

But the [šᵊḇîʿî](../../strongs/h/h7637.md) thou shalt let it [šāmaṭ](../../strongs/h/h8058.md) and [nāṭaš](../../strongs/h/h5203.md); that the ['ebyown](../../strongs/h/h34.md) of thy ['am](../../strongs/h/h5971.md) may ['akal](../../strongs/h/h398.md): and what they [yeṯer](../../strongs/h/h3499.md) the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) shall ['akal](../../strongs/h/h398.md). In like manner thou shalt ['asah](../../strongs/h/h6213.md) with thy [kerem](../../strongs/h/h3754.md), and with thy [zayiṯ](../../strongs/h/h2132.md).

<a name="exodus_23_12"></a>Exodus 23:12

[šēš](../../strongs/h/h8337.md) [yowm](../../strongs/h/h3117.md) thou shalt ['asah](../../strongs/h/h6213.md) thy [ma'aseh](../../strongs/h/h4639.md), and on the [šᵊḇîʿî](../../strongs/h/h7637.md) [yowm](../../strongs/h/h3117.md) thou shalt [shabath](../../strongs/h/h7673.md): that thine [showr](../../strongs/h/h7794.md) and thine [chamowr](../../strongs/h/h2543.md) may [nuwach](../../strongs/h/h5117.md), and the [ben](../../strongs/h/h1121.md) of thy ['amah](../../strongs/h/h519.md), and the [ger](../../strongs/h/h1616.md), may be [nāp̄aš](../../strongs/h/h5314.md).

<a name="exodus_23_13"></a>Exodus 23:13

And in all things that I have ['āmar](../../strongs/h/h559.md) unto you [shamar](../../strongs/h/h8104.md): and make no [zakar](../../strongs/h/h2142.md) of the [shem](../../strongs/h/h8034.md) of ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), neither let it be [shama'](../../strongs/h/h8085.md) out of thy [peh](../../strongs/h/h6310.md).

<a name="exodus_23_14"></a>Exodus 23:14

[šālôš](../../strongs/h/h7969.md) [regel](../../strongs/h/h7272.md) thou shalt [ḥāḡaḡ](../../strongs/h/h2287.md) unto me in the [šānâ](../../strongs/h/h8141.md).

<a name="exodus_23_15"></a>Exodus 23:15

Thou shalt [shamar](../../strongs/h/h8104.md) the [ḥāḡ](../../strongs/h/h2282.md) of [maṣṣâ](../../strongs/h/h4682.md): (thou shalt ['akal](../../strongs/h/h398.md) [maṣṣâ](../../strongs/h/h4682.md) [šeḇaʿ](../../strongs/h/h7651.md) [yowm](../../strongs/h/h3117.md), as I [tsavah](../../strongs/h/h6680.md) thee, in the [môʿēḏ](../../strongs/h/h4150.md) of the [ḥōḏeš](../../strongs/h/h2320.md) ['āḇîḇ](../../strongs/h/h24.md); for in it thou [yāṣā'](../../strongs/h/h3318.md) from [Mitsrayim](../../strongs/h/h4714.md): and none shall [ra'ah](../../strongs/h/h7200.md) [paniym](../../strongs/h/h6440.md) me [rêqām](../../strongs/h/h7387.md):)

<a name="exodus_23_16"></a>Exodus 23:16

And the [ḥāḡ](../../strongs/h/h2282.md) of [qāṣîr](../../strongs/h/h7105.md), the [bikûr](../../strongs/h/h1061.md) of thy [ma'aseh](../../strongs/h/h4639.md), which thou hast [zāraʿ](../../strongs/h/h2232.md) in the [sadeh](../../strongs/h/h7704.md): and the [ḥāḡ](../../strongs/h/h2282.md) of ['āsîp̄](../../strongs/h/h614.md), which is in the [yāṣā'](../../strongs/h/h3318.md) of the [šānâ](../../strongs/h/h8141.md), when thou hast ['āsap̄](../../strongs/h/h622.md) in thy [ma'aseh](../../strongs/h/h4639.md) out of the [sadeh](../../strongs/h/h7704.md).

<a name="exodus_23_17"></a>Exodus 23:17

[šālôš](../../strongs/h/h7969.md) [pa'am](../../strongs/h/h6471.md) in the [šānâ](../../strongs/h/h8141.md) all thy [zāḵûr](../../strongs/h/h2138.md) shall [ra'ah](../../strongs/h/h7200.md) [paniym](../../strongs/h/h6440.md) ['adown](../../strongs/h/h113.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_23_18"></a>Exodus 23:18

Thou shalt not [zabach](../../strongs/h/h2076.md) the [dam](../../strongs/h/h1818.md) of my [zebach](../../strongs/h/h2077.md) with [ḥāmēṣ](../../strongs/h/h2557.md); neither shall the [cheleb](../../strongs/h/h2459.md) of my [ḥāḡ](../../strongs/h/h2282.md) [lûn](../../strongs/h/h3885.md) until the [boqer](../../strongs/h/h1242.md).

<a name="exodus_23_19"></a>Exodus 23:19

The [re'shiyth](../../strongs/h/h7225.md) of the [bikûr](../../strongs/h/h1061.md) of thy ['ăḏāmâ](../../strongs/h/h127.md) thou shalt [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md). Thou shalt not [bāšal](../../strongs/h/h1310.md) a [gᵊḏî](../../strongs/h/h1423.md) in his ['em](../../strongs/h/h517.md) [chalab](../../strongs/h/h2461.md).

<a name="exodus_23_20"></a>Exodus 23:20

Behold, I [shalach](../../strongs/h/h7971.md) a [mal'ak](../../strongs/h/h4397.md) [paniym](../../strongs/h/h6440.md) thee, to [shamar](../../strongs/h/h8104.md) thee in the [derek](../../strongs/h/h1870.md), and to [bow'](../../strongs/h/h935.md) thee into the [maqowm](../../strongs/h/h4725.md) which I have [kuwn](../../strongs/h/h3559.md).

<a name="exodus_23_21"></a>Exodus 23:21

[shamar](../../strongs/h/h8104.md) [paniym](../../strongs/h/h6440.md) him, and [shama'](../../strongs/h/h8085.md) his [qowl](../../strongs/h/h6963.md), [mārar](../../strongs/h/h4843.md) him not; for he will not [nasa'](../../strongs/h/h5375.md) your [pesha'](../../strongs/h/h6588.md): for my [shem](../../strongs/h/h8034.md) is [qereḇ](../../strongs/h/h7130.md).

<a name="exodus_23_22"></a>Exodus 23:22

But if thou shalt indeed [shama'](../../strongs/h/h8085.md) his [qowl](../../strongs/h/h6963.md), and ['asah](../../strongs/h/h6213.md) all that I [dabar](../../strongs/h/h1696.md); then I will be an ['āyaḇ](../../strongs/h/h340.md) unto thine ['oyeb](../../strongs/h/h341.md), and a [tsarar](../../strongs/h/h6887.md) unto thine [ṣûr](../../strongs/h/h6696.md).

<a name="exodus_23_23"></a>Exodus 23:23

For mine [mal'ak](../../strongs/h/h4397.md) shall [yālaḵ](../../strongs/h/h3212.md) [paniym](../../strongs/h/h6440.md) thee, and [bow'](../../strongs/h/h935.md) thee in unto the ['Ĕmōrî](../../strongs/h/h567.md), and the [Ḥitî](../../strongs/h/h2850.md), and the [Pᵊrizzî](../../strongs/h/h6522.md), and the [Kᵊnaʿănî](../../strongs/h/h3669.md), the [Ḥiûî](../../strongs/h/h2340.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md): and I will [kāḥaḏ](../../strongs/h/h3582.md) them.

<a name="exodus_23_24"></a>Exodus 23:24

Thou shalt not [shachah](../../strongs/h/h7812.md) to their ['Elohiym](../../strongs/h/h430.md), nor ['abad](../../strongs/h/h5647.md) them, nor ['asah](../../strongs/h/h6213.md) after their [ma'aseh](../../strongs/h/h4639.md): but thou shalt [harac](../../strongs/h/h2040.md) [harac](../../strongs/h/h2040.md) them, and [shabar](../../strongs/h/h7665.md) [shabar](../../strongs/h/h7665.md) their [maṣṣēḇâ](../../strongs/h/h4676.md).

<a name="exodus_23_25"></a>Exodus 23:25

And ye shall ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and he shall [barak](../../strongs/h/h1288.md) thy [lechem](../../strongs/h/h3899.md), and thy [mayim](../../strongs/h/h4325.md); and I will [cuwr](../../strongs/h/h5493.md) [maḥălê](../../strongs/h/h4245.md) away from the [qereḇ](../../strongs/h/h7130.md) of thee.

<a name="exodus_23_26"></a>Exodus 23:26

There shall nothing [šāḵōl](../../strongs/h/h7921.md), nor be [ʿāqār](../../strongs/h/h6135.md), in thy ['erets](../../strongs/h/h776.md): the [mispār](../../strongs/h/h4557.md) of thy [yowm](../../strongs/h/h3117.md) I will [mālā'](../../strongs/h/h4390.md).

<a name="exodus_23_27"></a>Exodus 23:27

I will [shalach](../../strongs/h/h7971.md) my ['êmâ](../../strongs/h/h367.md) [paniym](../../strongs/h/h6440.md) thee, and will [hāmam](../../strongs/h/h2000.md) all the ['am](../../strongs/h/h5971.md) to whom thou shalt [bow'](../../strongs/h/h935.md), and I will [nathan](../../strongs/h/h5414.md) all thine ['oyeb](../../strongs/h/h341.md) [ʿōrep̄](../../strongs/h/h6203.md) unto thee.

<a name="exodus_23_28"></a>Exodus 23:28

And I will [shalach](../../strongs/h/h7971.md) [ṣirʿâ](../../strongs/h/h6880.md) [paniym](../../strongs/h/h6440.md) thee, which shall [gāraš](../../strongs/h/h1644.md) the [Ḥiûî](../../strongs/h/h2340.md), the [Kᵊnaʿănî](../../strongs/h/h3669.md), and the [Ḥitî](../../strongs/h/h2850.md), from [paniym](../../strongs/h/h6440.md) thee.

<a name="exodus_23_29"></a>Exodus 23:29

I will not [gāraš](../../strongs/h/h1644.md) them from [paniym](../../strongs/h/h6440.md) thee in one [šānâ](../../strongs/h/h8141.md); lest the ['erets](../../strongs/h/h776.md) become [šᵊmāmâ](../../strongs/h/h8077.md), and the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) [rab](../../strongs/h/h7227.md) against thee.

<a name="exodus_23_30"></a>Exodus 23:30

By [mᵊʿaṭ](../../strongs/h/h4592.md) and [mᵊʿaṭ](../../strongs/h/h4592.md) I will [gāraš](../../strongs/h/h1644.md) them from [paniym](../../strongs/h/h6440.md) thee, until thou be [parah](../../strongs/h/h6509.md), and [nāḥal](../../strongs/h/h5157.md) the ['erets](../../strongs/h/h776.md).

<a name="exodus_23_31"></a>Exodus 23:31

And I will [shiyth](../../strongs/h/h7896.md) thy [gᵊḇûl](../../strongs/h/h1366.md) from the [Sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md) even unto the [yam](../../strongs/h/h3220.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and from the [midbar](../../strongs/h/h4057.md) unto the [nāhār](../../strongs/h/h5104.md): for I will [nathan](../../strongs/h/h5414.md) the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) into your [yad](../../strongs/h/h3027.md); and thou shalt [gāraš](../../strongs/h/h1644.md) them [paniym](../../strongs/h/h6440.md) thee.

<a name="exodus_23_32"></a>Exodus 23:32

Thou shalt [karath](../../strongs/h/h3772.md) no [bĕriyth](../../strongs/h/h1285.md) with them, nor with their ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_23_33"></a>Exodus 23:33

They shall not [yashab](../../strongs/h/h3427.md) in thy ['erets](../../strongs/h/h776.md), lest they make thee [chata'](../../strongs/h/h2398.md) against me: for if thou ['abad](../../strongs/h/h5647.md) their ['Elohiym](../../strongs/h/h430.md), it will surely be a [mowqesh](../../strongs/h/h4170.md) unto thee.

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 22](exodus_22.md) - [Exodus 24](exodus_24.md)