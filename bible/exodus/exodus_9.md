# [Exodus 9](https://www.blueletterbible.org/kjv/exo/9/1/s_59001)

<a name="exodus_9_1"></a>Exodus 9:1

Then [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [bow'](../../strongs/h/h935.md) in unto [Parʿô](../../strongs/h/h6547.md), and [dabar](../../strongs/h/h1696.md) him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of the [ʿiḇrî](../../strongs/h/h5680.md), Let my ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), that they may ['abad](../../strongs/h/h5647.md) me.

<a name="exodus_9_2"></a>Exodus 9:2

For if thou [mā'ēn](../../strongs/h/h3986.md) to let them [shalach](../../strongs/h/h7971.md), and wilt [ḥāzaq](../../strongs/h/h2388.md) them still,

<a name="exodus_9_3"></a>Exodus 9:3

Behold, the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) is upon thy [miqnê](../../strongs/h/h4735.md) which is in the [sadeh](../../strongs/h/h7704.md), upon the [sûs](../../strongs/h/h5483.md), upon the [chamowr](../../strongs/h/h2543.md), upon the [gāmāl](../../strongs/h/h1581.md), upon the [bāqār](../../strongs/h/h1241.md), and upon the [tso'n](../../strongs/h/h6629.md): there shall be a [me'od](../../strongs/h/h3966.md) [kāḇēḏ](../../strongs/h/h3515.md) [deḇer](../../strongs/h/h1698.md).

<a name="exodus_9_4"></a>Exodus 9:4

And [Yĕhovah](../../strongs/h/h3068.md) shall [palah](../../strongs/h/h6395.md) between the [miqnê](../../strongs/h/h4735.md) of [Yisra'el](../../strongs/h/h3478.md) and the [miqnê](../../strongs/h/h4735.md) of [Mitsrayim](../../strongs/h/h4714.md): and there shall [dabar](../../strongs/h/h1697.md) [muwth](../../strongs/h/h4191.md) of all that is the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="exodus_9_5"></a>Exodus 9:5

And [Yĕhovah](../../strongs/h/h3068.md) [śûm](../../strongs/h/h7760.md) a [môʿēḏ](../../strongs/h/h4150.md), ['āmar](../../strongs/h/h559.md), [māḥār](../../strongs/h/h4279.md) [Yĕhovah](../../strongs/h/h3068.md) shall ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md) in the ['erets](../../strongs/h/h776.md).

<a name="exodus_9_6"></a>Exodus 9:6

And [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) that [dabar](../../strongs/h/h1697.md) on the [māḥŏrāṯ](../../strongs/h/h4283.md), and all the [miqnê](../../strongs/h/h4735.md) of [Mitsrayim](../../strongs/h/h4714.md) [muwth](../../strongs/h/h4191.md): but of the [miqnê](../../strongs/h/h4735.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [muwth](../../strongs/h/h4191.md) not ['echad](../../strongs/h/h259.md).

<a name="exodus_9_7"></a>Exodus 9:7

And [Parʿô](../../strongs/h/h6547.md) [shalach](../../strongs/h/h7971.md), and, behold, there was not ['echad](../../strongs/h/h259.md) of the [miqnê](../../strongs/h/h4735.md) of the [Yisra'el](../../strongs/h/h3478.md) [muwth](../../strongs/h/h4191.md). And the [leb](../../strongs/h/h3820.md) of [Parʿô](../../strongs/h/h6547.md) was [kabad](../../strongs/h/h3513.md), and he did not let the ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md).

<a name="exodus_9_8"></a>Exodus 9:8

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md) and unto ['Ahărôn](../../strongs/h/h175.md), [laqach](../../strongs/h/h3947.md) to you [ḥōp̄en](../../strongs/h/h2651.md) [mᵊlō'](../../strongs/h/h4393.md) of [pîaḥ](../../strongs/h/h6368.md) of the [kiḇšān](../../strongs/h/h3536.md), and let [Mōshe](../../strongs/h/h4872.md) [zāraq](../../strongs/h/h2236.md) it toward the [shamayim](../../strongs/h/h8064.md) in the ['ayin](../../strongs/h/h5869.md) of [Parʿô](../../strongs/h/h6547.md).

<a name="exodus_9_9"></a>Exodus 9:9

And it shall become ['āḇāq](../../strongs/h/h80.md) in all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and shall be a [šiḥîn](../../strongs/h/h7822.md) [pāraḥ](../../strongs/h/h6524.md) with ['ăḇaʿbuʿōṯ](../../strongs/h/h76.md) upon ['adam](../../strongs/h/h120.md), and upon [bĕhemah](../../strongs/h/h929.md), throughout all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_9_10"></a>Exodus 9:10

And they [laqach](../../strongs/h/h3947.md) [pîaḥ](../../strongs/h/h6368.md) of the [kiḇšān](../../strongs/h/h3536.md), and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Parʿô](../../strongs/h/h6547.md); and [Mōshe](../../strongs/h/h4872.md) [zāraq](../../strongs/h/h2236.md) it up toward [shamayim](../../strongs/h/h8064.md); and it became a [šiḥîn](../../strongs/h/h7822.md) [pāraḥ](../../strongs/h/h6524.md) with ['ăḇaʿbuʿōṯ](../../strongs/h/h76.md) upon ['adam](../../strongs/h/h120.md), and upon [bĕhemah](../../strongs/h/h929.md).

<a name="exodus_9_11"></a>Exodus 9:11

And the [ḥarṭōm](../../strongs/h/h2748.md) [yakol](../../strongs/h/h3201.md) not ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Mōshe](../../strongs/h/h4872.md) [paniym](../../strongs/h/h6440.md) of the [šiḥîn](../../strongs/h/h7822.md); for the [šiḥîn](../../strongs/h/h7822.md) was upon the [ḥarṭōm](../../strongs/h/h2748.md), and upon all the [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_9_12"></a>Exodus 9:12

And [Yĕhovah](../../strongs/h/h3068.md) [ḥāzaq](../../strongs/h/h2388.md) the [leb](../../strongs/h/h3820.md) of [Parʿô](../../strongs/h/h6547.md), and he [shama'](../../strongs/h/h8085.md) not unto them; as [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_9_13"></a>Exodus 9:13

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [yatsab](../../strongs/h/h3320.md) [paniym](../../strongs/h/h6440.md) [Parʿô](../../strongs/h/h6547.md), and ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of the [ʿiḇrî](../../strongs/h/h5680.md), Let my ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), that they may ['abad](../../strongs/h/h5647.md) me.

<a name="exodus_9_14"></a>Exodus 9:14

For I will at this [pa'am](../../strongs/h/h6471.md) [shalach](../../strongs/h/h7971.md) all my [magēp̄â](../../strongs/h/h4046.md) upon thine [leb](../../strongs/h/h3820.md), and upon thy ['ebed](../../strongs/h/h5650.md), and upon thy ['am](../../strongs/h/h5971.md); that thou mayest [yada'](../../strongs/h/h3045.md) that there is none like me in all the ['erets](../../strongs/h/h776.md).

<a name="exodus_9_15"></a>Exodus 9:15

For now I will [shalach](../../strongs/h/h7971.md) my [yad](../../strongs/h/h3027.md), that I may [nakah](../../strongs/h/h5221.md) thee and thy ['am](../../strongs/h/h5971.md) with [deḇer](../../strongs/h/h1698.md); and thou shalt be [kāḥaḏ](../../strongs/h/h3582.md) from the ['erets](../../strongs/h/h776.md).

<a name="exodus_9_16"></a>Exodus 9:16

And in ['ûlām](../../strongs/h/h199.md) [ʿăḇûr](../../strongs/h/h5668.md) for this cause have I ['amad](../../strongs/h/h5975.md) thee up, for to [ra'ah](../../strongs/h/h7200.md) in thee my [koach](../../strongs/h/h3581.md); and that my [shem](../../strongs/h/h8034.md) may be [sāp̄ar](../../strongs/h/h5608.md) throughout all the ['erets](../../strongs/h/h776.md).

<a name="exodus_9_17"></a>Exodus 9:17

As yet [sālal](../../strongs/h/h5549.md) thou thyself against my ['am](../../strongs/h/h5971.md), that thou wilt not let them [shalach](../../strongs/h/h7971.md)?

<a name="exodus_9_18"></a>Exodus 9:18

Behold, [māḥār](../../strongs/h/h4279.md) about this [ʿēṯ](../../strongs/h/h6256.md) I will cause it to [matar](../../strongs/h/h4305.md) a [me'od](../../strongs/h/h3966.md) [kāḇēḏ](../../strongs/h/h3515.md) [barad](../../strongs/h/h1259.md), such as hath not been in [Mitsrayim](../../strongs/h/h4714.md) since the [yowm](../../strongs/h/h3117.md) [yacad](../../strongs/h/h3245.md) thereof even until now.

<a name="exodus_9_19"></a>Exodus 9:19

[shalach](../../strongs/h/h7971.md) therefore now, and [ʿûz](../../strongs/h/h5756.md) thy [miqnê](../../strongs/h/h4735.md), and all that thou hast in the [sadeh](../../strongs/h/h7704.md); for upon every ['adam](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md) which shall be [māṣā'](../../strongs/h/h4672.md) in the [sadeh](../../strongs/h/h7704.md), and shall not be ['āsap̄](../../strongs/h/h622.md) [bayith](../../strongs/h/h1004.md), the [barad](../../strongs/h/h1259.md) shall [yarad](../../strongs/h/h3381.md) upon them, and they shall [muwth](../../strongs/h/h4191.md).

<a name="exodus_9_20"></a>Exodus 9:20

He that [yārē'](../../strongs/h/h3373.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) among the ['ebed](../../strongs/h/h5650.md) of [Parʿô](../../strongs/h/h6547.md) made his ['ebed](../../strongs/h/h5650.md) and his [miqnê](../../strongs/h/h4735.md) [nûs](../../strongs/h/h5127.md) into the [bayith](../../strongs/h/h1004.md):

<a name="exodus_9_21"></a>Exodus 9:21

And he that [śûm](../../strongs/h/h7760.md) [leb](../../strongs/h/h3820.md) not the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) ['azab](../../strongs/h/h5800.md) his ['ebed](../../strongs/h/h5650.md) and his [miqnê](../../strongs/h/h4735.md) in the [sadeh](../../strongs/h/h7704.md).

<a name="exodus_9_22"></a>Exodus 9:22

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [natah](../../strongs/h/h5186.md) thine [yad](../../strongs/h/h3027.md) toward [shamayim](../../strongs/h/h8064.md), that there may be [barad](../../strongs/h/h1259.md) in all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), upon ['adam](../../strongs/h/h120.md), and upon [bĕhemah](../../strongs/h/h929.md), and upon every ['eseb](../../strongs/h/h6212.md) of the [sadeh](../../strongs/h/h7704.md), throughout the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_9_23"></a>Exodus 9:23

And [Mōshe](../../strongs/h/h4872.md) [natah](../../strongs/h/h5186.md) his [maṭṭê](../../strongs/h/h4294.md) toward [shamayim](../../strongs/h/h8064.md): and [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) [qowl](../../strongs/h/h6963.md) and [barad](../../strongs/h/h1259.md), and the ['esh](../../strongs/h/h784.md) [halak](../../strongs/h/h1980.md) upon the ['erets](../../strongs/h/h776.md); and [Yĕhovah](../../strongs/h/h3068.md) [matar](../../strongs/h/h4305.md) [barad](../../strongs/h/h1259.md) upon the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_9_24"></a>Exodus 9:24

So there was [barad](../../strongs/h/h1259.md), and ['esh](../../strongs/h/h784.md) [laqach](../../strongs/h/h3947.md) [tavek](../../strongs/h/h8432.md) with the [barad](../../strongs/h/h1259.md), [me'od](../../strongs/h/h3966.md) [kāḇēḏ](../../strongs/h/h3515.md), such as there was none like it in all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) since it became a [gowy](../../strongs/h/h1471.md).

<a name="exodus_9_25"></a>Exodus 9:25

And the [barad](../../strongs/h/h1259.md) [nakah](../../strongs/h/h5221.md) throughout all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) all that was in the [sadeh](../../strongs/h/h7704.md), both ['adam](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md); and the [barad](../../strongs/h/h1259.md) [nakah](../../strongs/h/h5221.md) every ['eseb](../../strongs/h/h6212.md) of the [sadeh](../../strongs/h/h7704.md), and [shabar](../../strongs/h/h7665.md) every ['ets](../../strongs/h/h6086.md) of the [sadeh](../../strongs/h/h7704.md).

<a name="exodus_9_26"></a>Exodus 9:26

Only in the ['erets](../../strongs/h/h776.md) of [gōšen](../../strongs/h/h1657.md), where the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were, was there no [barad](../../strongs/h/h1259.md).

<a name="exodus_9_27"></a>Exodus 9:27

And [Parʿô](../../strongs/h/h6547.md) [shalach](../../strongs/h/h7971.md), and [qara'](../../strongs/h/h7121.md) for [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md), and ['āmar](../../strongs/h/h559.md) unto them, I have [chata'](../../strongs/h/h2398.md) this [pa'am](../../strongs/h/h6471.md): [Yĕhovah](../../strongs/h/h3068.md) is [tsaddiyq](../../strongs/h/h6662.md), and I and my ['am](../../strongs/h/h5971.md) are [rasha'](../../strongs/h/h7563.md).

<a name="exodus_9_28"></a>Exodus 9:28

[ʿāṯar](../../strongs/h/h6279.md) [Yĕhovah](../../strongs/h/h3068.md) (for it is [rab](../../strongs/h/h7227.md)) that there be no more ['Elohiym](../../strongs/h/h430.md) [qowl](../../strongs/h/h6963.md) and [barad](../../strongs/h/h1259.md); and I will let you [shalach](../../strongs/h/h7971.md), and ye shall ['amad](../../strongs/h/h5975.md) no [yāsap̄](../../strongs/h/h3254.md).

<a name="exodus_9_29"></a>Exodus 9:29

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto him, As soon as I am [yāṣā'](../../strongs/h/h3318.md) of the [ʿîr](../../strongs/h/h5892.md), I will [pāraś](../../strongs/h/h6566.md) my [kaph](../../strongs/h/h3709.md) unto [Yĕhovah](../../strongs/h/h3068.md); and the [qowl](../../strongs/h/h6963.md) shall [ḥāḏal](../../strongs/h/h2308.md), neither shall there be any more [barad](../../strongs/h/h1259.md); that thou mayest [yada'](../../strongs/h/h3045.md) how that the ['erets](../../strongs/h/h776.md) is [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_9_30"></a>Exodus 9:30

But as for thee and thy ['ebed](../../strongs/h/h5650.md), I [yada'](../../strongs/h/h3045.md) that ye will not yet [yare'](../../strongs/h/h3372.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_9_31"></a>Exodus 9:31

And the [pištê](../../strongs/h/h6594.md) and the [śᵊʿōrâ](../../strongs/h/h8184.md) was [nakah](../../strongs/h/h5221.md): for the [śᵊʿōrâ](../../strongs/h/h8184.md) was in the ['āḇîḇ](../../strongs/h/h24.md), and the [pištê](../../strongs/h/h6594.md) was [giḇʿōl](../../strongs/h/h1392.md).

<a name="exodus_9_32"></a>Exodus 9:32

But the [ḥiṭṭâ](../../strongs/h/h2406.md) and the [kussemeṯ](../../strongs/h/h3698.md) were not [nakah](../../strongs/h/h5221.md): for they were not ['āp̄îl](../../strongs/h/h648.md).

<a name="exodus_9_33"></a>Exodus 9:33

And [Mōshe](../../strongs/h/h4872.md) [yāṣā'](../../strongs/h/h3318.md) of the [ʿîr](../../strongs/h/h5892.md) from [Parʿô](../../strongs/h/h6547.md), and [pāraś](../../strongs/h/h6566.md) his [kaph](../../strongs/h/h3709.md) unto [Yĕhovah](../../strongs/h/h3068.md): and the [qowl](../../strongs/h/h6963.md) and [barad](../../strongs/h/h1259.md) [ḥāḏal](../../strongs/h/h2308.md), and the [māṭār](../../strongs/h/h4306.md) was not [nāṯaḵ](../../strongs/h/h5413.md) upon the ['erets](../../strongs/h/h776.md).

<a name="exodus_9_34"></a>Exodus 9:34

And when [Parʿô](../../strongs/h/h6547.md) [ra'ah](../../strongs/h/h7200.md) that the [māṭār](../../strongs/h/h4306.md) and the [barad](../../strongs/h/h1259.md) and the [qowl](../../strongs/h/h6963.md) were [ḥāḏal](../../strongs/h/h2308.md), he [chata'](../../strongs/h/h2398.md) yet [yāsap̄](../../strongs/h/h3254.md), and [kabad](../../strongs/h/h3513.md) his [leb](../../strongs/h/h3820.md), he and his ['ebed](../../strongs/h/h5650.md).

<a name="exodus_9_35"></a>Exodus 9:35

And the [leb](../../strongs/h/h3820.md) of [Parʿô](../../strongs/h/h6547.md) was [ḥāzaq](../../strongs/h/h2388.md), neither would he let the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md); as [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md) [yad](../../strongs/h/h3027.md) [Mōshe](../../strongs/h/h4872.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 8](exodus_8.md) - [Exodus 10](exodus_10.md)