# [Exodus 16](https://www.blueletterbible.org/kjv/exo/16/1/s_66001)

<a name="exodus_16_1"></a>Exodus 16:1

And they took their [nāsaʿ](../../strongs/h/h5265.md) from ['Êlim](../../strongs/h/h362.md), and all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) unto the [midbar](../../strongs/h/h4057.md) of [Sîn](../../strongs/h/h5512.md), which is between ['Êlim](../../strongs/h/h362.md) and [Sînay](../../strongs/h/h5514.md), on the fifteenth [yowm](../../strongs/h/h3117.md) of the second [ḥōḏeš](../../strongs/h/h2320.md) after their [yāṣā'](../../strongs/h/h3318.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_16_2"></a>Exodus 16:2

And the whole ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [lûn](../../strongs/h/h3885.md) against [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) in the [midbar](../../strongs/h/h4057.md):

<a name="exodus_16_3"></a>Exodus 16:3

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto them, [mî](../../strongs/h/h4310.md) [nathan](../../strongs/h/h5414.md) we had [muwth](../../strongs/h/h4191.md) by the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), when we [yashab](../../strongs/h/h3427.md) by the [basar](../../strongs/h/h1320.md) [sîr](../../strongs/h/h5518.md), and when we did ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) to the [śōḇaʿ](../../strongs/h/h7648.md); for ye have [yāṣā'](../../strongs/h/h3318.md) us into this [midbar](../../strongs/h/h4057.md), to [muwth](../../strongs/h/h4191.md) this [qāhēl](../../strongs/h/h6951.md) with [rāʿāḇ](../../strongs/h/h7458.md).

<a name="exodus_16_4"></a>Exodus 16:4

Then ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto [Mōshe](../../strongs/h/h4872.md), Behold, I will [matar](../../strongs/h/h4305.md) [lechem](../../strongs/h/h3899.md) from [shamayim](../../strongs/h/h8064.md) for you; and the ['am](../../strongs/h/h5971.md) shall [yāṣā'](../../strongs/h/h3318.md) and [lāqaṭ](../../strongs/h/h3950.md) a [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md), that I may [nāsâ](../../strongs/h/h5254.md) them, whether they will [yālaḵ](../../strongs/h/h3212.md) in my [towrah](../../strongs/h/h8451.md), or no.

<a name="exodus_16_5"></a>Exodus 16:5

And it shall come to pass, that on the sixth [yowm](../../strongs/h/h3117.md) they shall [kuwn](../../strongs/h/h3559.md) that which they [bow'](../../strongs/h/h935.md); and it shall be [mišnê](../../strongs/h/h4932.md) as much as they [lāqaṭ](../../strongs/h/h3950.md) [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md).

<a name="exodus_16_6"></a>Exodus 16:6

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) ['āmar](../../strongs/h/h559.md) unto all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), At ['ereb](../../strongs/h/h6153.md), then ye shall [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) hath [yāṣā'](../../strongs/h/h3318.md) you from the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md):

<a name="exodus_16_7"></a>Exodus 16:7

And in the [boqer](../../strongs/h/h1242.md), then ye shall [ra'ah](../../strongs/h/h7200.md) the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md); for that he [shama'](../../strongs/h/h8085.md) your [tᵊlûnāṯ](../../strongs/h/h8519.md) against [Yĕhovah](../../strongs/h/h3068.md): and what are we, that ye [lûn](../../strongs/h/h3885.md) [lûn](../../strongs/h/h3885.md) against us?

<a name="exodus_16_8"></a>Exodus 16:8

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), This shall be, when [Yĕhovah](../../strongs/h/h3068.md) shall [nathan](../../strongs/h/h5414.md) you in the ['ereb](../../strongs/h/h6153.md) [basar](../../strongs/h/h1320.md) to ['akal](../../strongs/h/h398.md), and in the [boqer](../../strongs/h/h1242.md) [lechem](../../strongs/h/h3899.md) to the [sāׂbaʿ](../../strongs/h/h7646.md); for that [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) your [tᵊlûnāṯ](../../strongs/h/h8519.md) which ye [lûn](../../strongs/h/h3885.md) against him: and what are we? your [tᵊlûnāṯ](../../strongs/h/h8519.md) are not against us, but against [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_16_9"></a>Exodus 16:9

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md) unto all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), [qāraḇ](../../strongs/h/h7126.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): for he hath [shama'](../../strongs/h/h8085.md) your [tᵊlûnāṯ](../../strongs/h/h8519.md).

<a name="exodus_16_10"></a>Exodus 16:10

And it came to pass, as ['Ahărôn](../../strongs/h/h175.md) [dabar](../../strongs/h/h1696.md) unto the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they [panah](../../strongs/h/h6437.md) toward the [midbar](../../strongs/h/h4057.md), and, behold, the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) in the [ʿānān](../../strongs/h/h6051.md).

<a name="exodus_16_11"></a>Exodus 16:11

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_16_12"></a>Exodus 16:12

I have [shama'](../../strongs/h/h8085.md) the [tᵊlûnāṯ](../../strongs/h/h8519.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): [dabar](../../strongs/h/h1696.md) unto them, ['āmar](../../strongs/h/h559.md), At ['ereb](../../strongs/h/h6153.md) ye shall ['akal](../../strongs/h/h398.md) [basar](../../strongs/h/h1320.md), and in the [boqer](../../strongs/h/h1242.md) ye shall be [sāׂbaʿ](../../strongs/h/h7646.md) with [lechem](../../strongs/h/h3899.md); and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_16_13"></a>Exodus 16:13

And it came to pass, that at ['ereb](../../strongs/h/h6153.md) the [śᵊlav](../../strongs/h/h7958.md) [ʿālâ](../../strongs/h/h5927.md), and [kāsâ](../../strongs/h/h3680.md) the [maḥănê](../../strongs/h/h4264.md): and in the [boqer](../../strongs/h/h1242.md) the [ṭal](../../strongs/h/h2919.md) [šᵊḵāḇâ](../../strongs/h/h7902.md) [cabiyb](../../strongs/h/h5439.md) the [maḥănê](../../strongs/h/h4264.md).

<a name="exodus_16_14"></a>Exodus 16:14

And when the [ṭal](../../strongs/h/h2919.md) that [šᵊḵāḇâ](../../strongs/h/h7902.md) was [ʿālâ](../../strongs/h/h5927.md), behold, upon the [paniym](../../strongs/h/h6440.md) of the [midbar](../../strongs/h/h4057.md) there lay a [daq](../../strongs/h/h1851.md) [ḥaspas](../../strongs/h/h2636.md), as [daq](../../strongs/h/h1851.md) as the [kᵊp̄ôr](../../strongs/h/h3713.md) on the ['erets](../../strongs/h/h776.md).

<a name="exodus_16_15"></a>Exodus 16:15

And when the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ra'ah](../../strongs/h/h7200.md) it, they ['āmar](../../strongs/h/h559.md) ['iysh](../../strongs/h/h376.md) to ['ach](../../strongs/h/h251.md), It is [man](../../strongs/h/h4478.md): for they [yada'](../../strongs/h/h3045.md) not what it was. And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto them, This is the [lechem](../../strongs/h/h3899.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) you to ['oklah](../../strongs/h/h402.md).

<a name="exodus_16_16"></a>Exodus 16:16

This is the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [tsavah](../../strongs/h/h6680.md), [lāqaṭ](../../strongs/h/h3950.md) of it every ['iysh](../../strongs/h/h376.md) [peh](../../strongs/h/h6310.md) to his ['ōḵel](../../strongs/h/h400.md), an [ʿōmer](../../strongs/h/h6016.md) for [gulgōleṯ](../../strongs/h/h1538.md), according to the [mispār](../../strongs/h/h4557.md) of your [nephesh](../../strongs/h/h5315.md); [laqach](../../strongs/h/h3947.md) ye every ['iysh](../../strongs/h/h376.md) for them which are in his ['ohel](../../strongs/h/h168.md).

<a name="exodus_16_17"></a>Exodus 16:17

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) so, and [lāqaṭ](../../strongs/h/h3950.md), some [rabah](../../strongs/h/h7235.md), some [māʿaṭ](../../strongs/h/h4591.md).

<a name="exodus_16_18"></a>Exodus 16:18

And when they did [māḏaḏ](../../strongs/h/h4058.md) it with an [ʿōmer](../../strongs/h/h6016.md), he that [rabah](../../strongs/h/h7235.md) had [ʿāḏap̄](../../strongs/h/h5736.md), and he that [māʿaṭ](../../strongs/h/h4591.md) had no [ḥāsēr](../../strongs/h/h2637.md); they [lāqaṭ](../../strongs/h/h3950.md) every ['iysh](../../strongs/h/h376.md) [peh](../../strongs/h/h6310.md) to his ['ōḵel](../../strongs/h/h400.md).

<a name="exodus_16_19"></a>Exodus 16:19

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), Let no ['iysh](../../strongs/h/h376.md) [yāṯar](../../strongs/h/h3498.md) of it till the [boqer](../../strongs/h/h1242.md).

<a name="exodus_16_20"></a>Exodus 16:20

Notwithstanding they [shama'](../../strongs/h/h8085.md) not unto [Mōshe](../../strongs/h/h4872.md); but ['enowsh](../../strongs/h/h582.md) of them [yāṯar](../../strongs/h/h3498.md) of it until the [boqer](../../strongs/h/h1242.md), and it [ruwm](../../strongs/h/h7311.md) [tôlāʿ](../../strongs/h/h8438.md), and [bā'aš](../../strongs/h/h887.md): and [Mōshe](../../strongs/h/h4872.md) was [qāṣap̄](../../strongs/h/h7107.md) with them.

<a name="exodus_16_21"></a>Exodus 16:21

And they [lāqaṭ](../../strongs/h/h3950.md) it every [boqer](../../strongs/h/h1242.md), every ['iysh](../../strongs/h/h376.md) [peh](../../strongs/h/h6310.md) to his ['ōḵel](../../strongs/h/h400.md): and when the [šemeš](../../strongs/h/h8121.md) [ḥāmam](../../strongs/h/h2552.md), it [māsas](../../strongs/h/h4549.md).

<a name="exodus_16_22"></a>Exodus 16:22

And it came to pass, that on the sixth [yowm](../../strongs/h/h3117.md) they [lāqaṭ](../../strongs/h/h3950.md) [mišnê](../../strongs/h/h4932.md) as much [lechem](../../strongs/h/h3899.md), two [ʿōmer](../../strongs/h/h6016.md) for ['echad](../../strongs/h/h259.md): and all the [nāśî'](../../strongs/h/h5387.md) of the ['edah](../../strongs/h/h5712.md) [bow'](../../strongs/h/h935.md) and [nāḡaḏ](../../strongs/h/h5046.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_16_23"></a>Exodus 16:23

And he ['āmar](../../strongs/h/h559.md) unto them, This is that which [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md), [māḥār](../../strongs/h/h4279.md) is the [šabāṯôn](../../strongs/h/h7677.md) of the [qodesh](../../strongs/h/h6944.md) [shabbath](../../strongs/h/h7676.md) unto [Yĕhovah](../../strongs/h/h3068.md): ['āp̄â](../../strongs/h/h644.md) that which ye will ['āp̄â](../../strongs/h/h644.md) to day, and [bāšal](../../strongs/h/h1310.md) that ye will [bāšal](../../strongs/h/h1310.md); and that which [ʿāḏap̄](../../strongs/h/h5736.md) [yānaḥ](../../strongs/h/h3240.md) for you to be [mišmereṯ](../../strongs/h/h4931.md) until the [boqer](../../strongs/h/h1242.md).

<a name="exodus_16_24"></a>Exodus 16:24

And they [yānaḥ](../../strongs/h/h3240.md) it till the [boqer](../../strongs/h/h1242.md), as [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md): and it did not [bā'aš](../../strongs/h/h887.md), neither was there any [rimmâ](../../strongs/h/h7415.md) therein.

<a name="exodus_16_25"></a>Exodus 16:25

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), ['akal](../../strongs/h/h398.md) that to [yowm](../../strongs/h/h3117.md); for [yowm](../../strongs/h/h3117.md) is a [shabbath](../../strongs/h/h7676.md) unto [Yĕhovah](../../strongs/h/h3068.md): [yowm](../../strongs/h/h3117.md) ye shall not [māṣā'](../../strongs/h/h4672.md) it in the [sadeh](../../strongs/h/h7704.md).

<a name="exodus_16_26"></a>Exodus 16:26

Six [yowm](../../strongs/h/h3117.md) ye shall [lāqaṭ](../../strongs/h/h3950.md) it; but on the seventh [yowm](../../strongs/h/h3117.md), which is the [shabbath](../../strongs/h/h7676.md), in it there shall be none.

<a name="exodus_16_27"></a>Exodus 16:27

And it came to pass, that there [yāṣā'](../../strongs/h/h3318.md) some of the ['am](../../strongs/h/h5971.md) on the seventh [yowm](../../strongs/h/h3117.md) for to [lāqaṭ](../../strongs/h/h3950.md), and they [māṣā'](../../strongs/h/h4672.md) none.

<a name="exodus_16_28"></a>Exodus 16:28

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), How long [mā'ēn](../../strongs/h/h3985.md) ye to [shamar](../../strongs/h/h8104.md) my [mitsvah](../../strongs/h/h4687.md) and my [towrah](../../strongs/h/h8451.md)?

<a name="exodus_16_29"></a>Exodus 16:29

[ra'ah](../../strongs/h/h7200.md), for that [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) you the [shabbath](../../strongs/h/h7676.md), therefore he [nathan](../../strongs/h/h5414.md) you on the sixth [yowm](../../strongs/h/h3117.md) the [lechem](../../strongs/h/h3899.md) of two [yowm](../../strongs/h/h3117.md); [yashab](../../strongs/h/h3427.md) ye every ['iysh](../../strongs/h/h376.md) in his place, let no ['iysh](../../strongs/h/h376.md) [yāṣā'](../../strongs/h/h3318.md) of his [maqowm](../../strongs/h/h4725.md) on the seventh [yowm](../../strongs/h/h3117.md).

<a name="exodus_16_30"></a>Exodus 16:30

So the ['am](../../strongs/h/h5971.md) [shabath](../../strongs/h/h7673.md) on the [šᵊḇîʿî](../../strongs/h/h7637.md) [yowm](../../strongs/h/h3117.md).

<a name="exodus_16_31"></a>Exodus 16:31

And the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) thereof [man](../../strongs/h/h4478.md): and it was like [gaḏ](../../strongs/h/h1407.md) [zera'](../../strongs/h/h2233.md), [lāḇān](../../strongs/h/h3836.md); and the [ṭaʿam](../../strongs/h/h2940.md) of it was like [ṣᵊp̄îḥiṯ](../../strongs/h/h6838.md) made with [dĕbash](../../strongs/h/h1706.md).

<a name="exodus_16_32"></a>Exodus 16:32

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), This is the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md), [mᵊlō'](../../strongs/h/h4393.md) an [ʿōmer](../../strongs/h/h6016.md) of it to be [mišmereṯ](../../strongs/h/h4931.md) for your [dôr](../../strongs/h/h1755.md); that they may [ra'ah](../../strongs/h/h7200.md) the [lechem](../../strongs/h/h3899.md) wherewith I have ['akal](../../strongs/h/h398.md) you in the [midbar](../../strongs/h/h4057.md), when I [yāṣā'](../../strongs/h/h3318.md) you from the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_16_33"></a>Exodus 16:33

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), [laqach](../../strongs/h/h3947.md) a [ṣinṣeneṯ](../../strongs/h/h6803.md), and [nathan](../../strongs/h/h5414.md) an [ʿōmer](../../strongs/h/h6016.md) [mᵊlō'](../../strongs/h/h4393.md) of [man](../../strongs/h/h4478.md) therein, and [yānaḥ](../../strongs/h/h3240.md) it [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), to be [mišmereṯ](../../strongs/h/h4931.md) for your [dôr](../../strongs/h/h1755.md).

<a name="exodus_16_34"></a>Exodus 16:34

As [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md), so ['Ahărôn](../../strongs/h/h175.md) [yānaḥ](../../strongs/h/h3240.md) it up [paniym](../../strongs/h/h6440.md) the [ʿēḏûṯ](../../strongs/h/h5715.md), to be [mišmereṯ](../../strongs/h/h4931.md).

<a name="exodus_16_35"></a>Exodus 16:35

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) did ['akal](../../strongs/h/h398.md) [man](../../strongs/h/h4478.md) ['arbāʿîm](../../strongs/h/h705.md) [šānâ](../../strongs/h/h8141.md), until they [bow'](../../strongs/h/h935.md) to an ['erets](../../strongs/h/h776.md) [yashab](../../strongs/h/h3427.md); they did ['akal](../../strongs/h/h398.md) [man](../../strongs/h/h4478.md), until they [bow'](../../strongs/h/h935.md) unto the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md).

<a name="exodus_16_36"></a>Exodus 16:36

Now an [ʿōmer](../../strongs/h/h6016.md) is the [ʿăśîrî](../../strongs/h/h6224.md) part of an ['êp̄â](../../strongs/h/h374.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 15](exodus_15.md) - [Exodus 17](exodus_17.md)