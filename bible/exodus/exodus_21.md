# [Exodus 21](https://www.blueletterbible.org/kjv/exo/21/1/s_71001)

<a name="exodus_21_1"></a>Exodus 21:1

Now these are the [mishpat](../../strongs/h/h4941.md) which thou shalt [śûm](../../strongs/h/h7760.md) [paniym](../../strongs/h/h6440.md) them.

<a name="exodus_21_2"></a>Exodus 21:2

If thou [qānâ](../../strongs/h/h7069.md) an [ʿiḇrî](../../strongs/h/h5680.md) ['ebed](../../strongs/h/h5650.md), six [šānâ](../../strongs/h/h8141.md) he shall ['abad](../../strongs/h/h5647.md): and in the [šᵊḇîʿî](../../strongs/h/h7637.md) he shall [yāṣā'](../../strongs/h/h3318.md) [ḥāp̄šî](../../strongs/h/h2670.md) [ḥinnām](../../strongs/h/h2600.md).

<a name="exodus_21_3"></a>Exodus 21:3

If he [bow'](../../strongs/h/h935.md) by [gap̄](../../strongs/h/h1610.md), he shall [yāṣā'](../../strongs/h/h3318.md) by [gap̄](../../strongs/h/h1610.md): if he were [baʿal](../../strongs/h/h1167.md) ['ishshah](../../strongs/h/h802.md), then his ['ishshah](../../strongs/h/h802.md) shall [yāṣā'](../../strongs/h/h3318.md) with him.

<a name="exodus_21_4"></a>Exodus 21:4

If his ['adown](../../strongs/h/h113.md) have [nathan](../../strongs/h/h5414.md) him an ['ishshah](../../strongs/h/h802.md), and she have [yalad](../../strongs/h/h3205.md) him [ben](../../strongs/h/h1121.md) or [bath](../../strongs/h/h1323.md); the ['ishshah](../../strongs/h/h802.md) and her [yeleḏ](../../strongs/h/h3206.md) shall be her ['adown](../../strongs/h/h113.md), and he shall [yāṣā'](../../strongs/h/h3318.md) by [gap̄](../../strongs/h/h1610.md).

<a name="exodus_21_5"></a>Exodus 21:5

And if the ['ebed](../../strongs/h/h5650.md) shall ['āmar](../../strongs/h/h559.md) ['āmar](../../strongs/h/h559.md), I ['ahab](../../strongs/h/h157.md) my ['adown](../../strongs/h/h113.md), my ['ishshah](../../strongs/h/h802.md), and my [ben](../../strongs/h/h1121.md); I will not [yāṣā'](../../strongs/h/h3318.md) [ḥāp̄šî](../../strongs/h/h2670.md):

<a name="exodus_21_6"></a>Exodus 21:6

Then his ['adown](../../strongs/h/h113.md) shall [nāḡaš](../../strongs/h/h5066.md) him unto the ['Elohiym](../../strongs/h/h430.md); he shall also [nāḡaš](../../strongs/h/h5066.md) him to the [deleṯ](../../strongs/h/h1817.md), or unto the [mᵊzûzâ](../../strongs/h/h4201.md); and his ['adown](../../strongs/h/h113.md) shall [rāṣaʿ](../../strongs/h/h7527.md) his ['ozen](../../strongs/h/h241.md) through with a [marṣēaʿ](../../strongs/h/h4836.md); and he shall ['abad](../../strongs/h/h5647.md) him ['owlam](../../strongs/h/h5769.md).

<a name="exodus_21_7"></a>Exodus 21:7

And if an ['iysh](../../strongs/h/h376.md) [māḵar](../../strongs/h/h4376.md) his [bath](../../strongs/h/h1323.md) to be an ['amah](../../strongs/h/h519.md), she shall not [yāṣā'](../../strongs/h/h3318.md) as the ['ebed](../../strongs/h/h5650.md) [yāṣā'](../../strongs/h/h3318.md).

<a name="exodus_21_8"></a>Exodus 21:8

If she [ra'](../../strongs/h/h7451.md) ['ayin](../../strongs/h/h5869.md) her ['adown](../../strongs/h/h113.md), who hath [yāʿaḏ](../../strongs/h/h3259.md) her to himself, then shall he let her be [pāḏâ](../../strongs/h/h6299.md): to [māḵar](../../strongs/h/h4376.md) her unto a [nāḵrî](../../strongs/h/h5237.md) ['am](../../strongs/h/h5971.md) he shall have no [mashal](../../strongs/h/h4910.md), seeing he hath dealt [bāḡaḏ](../../strongs/h/h898.md) with her.

<a name="exodus_21_9"></a>Exodus 21:9

And if he have [yāʿaḏ](../../strongs/h/h3259.md) her unto his [ben](../../strongs/h/h1121.md), he shall ['asah](../../strongs/h/h6213.md) with her after the [mishpat](../../strongs/h/h4941.md) of [bath](../../strongs/h/h1323.md).

<a name="exodus_21_10"></a>Exodus 21:10

If he [laqach](../../strongs/h/h3947.md) him ['aḥēr](../../strongs/h/h312.md); her [šᵊ'ēr](../../strongs/h/h7607.md), her [kᵊsûṯ](../../strongs/h/h3682.md), and her [ʿônâ](../../strongs/h/h5772.md), shall he not [gāraʿ](../../strongs/h/h1639.md).

<a name="exodus_21_11"></a>Exodus 21:11

And if he ['asah](../../strongs/h/h6213.md) not these three unto her, then shall she [yāṣā'](../../strongs/h/h3318.md) [ḥinnām](../../strongs/h/h2600.md) without [keceph](../../strongs/h/h3701.md).

<a name="exodus_21_12"></a>Exodus 21:12

He that [nakah](../../strongs/h/h5221.md) an ['iysh](../../strongs/h/h376.md), so that he [muwth](../../strongs/h/h4191.md), shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="exodus_21_13"></a>Exodus 21:13

And if a man [ṣāḏâ](../../strongs/h/h6658.md) not, but ['Elohiym](../../strongs/h/h430.md) ['ānâ](../../strongs/h/h579.md) him into his [yad](../../strongs/h/h3027.md); then I will [śûm](../../strongs/h/h7760.md) thee a [maqowm](../../strongs/h/h4725.md) whither he shall [nûs](../../strongs/h/h5127.md).

<a name="exodus_21_14"></a>Exodus 21:14

But if an ['iysh](../../strongs/h/h376.md) [zûḏ](../../strongs/h/h2102.md) upon his [rea'](../../strongs/h/h7453.md), to [harag](../../strongs/h/h2026.md) him with [ʿārmâ](../../strongs/h/h6195.md); thou shalt [laqach](../../strongs/h/h3947.md) him from minea [mizbeach](../../strongs/h/h4196.md), that he may [muwth](../../strongs/h/h4191.md).

<a name="exodus_21_15"></a>Exodus 21:15

And he that [nakah](../../strongs/h/h5221.md) his ['ab](../../strongs/h/h1.md), or his ['em](../../strongs/h/h517.md), shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="exodus_21_16"></a>Exodus 21:16

And he that [ganab](../../strongs/h/h1589.md) an ['iysh](../../strongs/h/h376.md), and [māḵar](../../strongs/h/h4376.md) him, or if he be [māṣā'](../../strongs/h/h4672.md) in his [yad](../../strongs/h/h3027.md), he shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="exodus_21_17"></a>Exodus 21:17

And he that [qālal](../../strongs/h/h7043.md) his ['ab](../../strongs/h/h1.md), or his ['em](../../strongs/h/h517.md), shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="exodus_21_18"></a>Exodus 21:18

And if ['enowsh](../../strongs/h/h582.md) [riyb](../../strongs/h/h7378.md) together, and ['iysh](../../strongs/h/h376.md) [nakah](../../strongs/h/h5221.md) [rea'](../../strongs/h/h7453.md) with an ['eben](../../strongs/h/h68.md), or with his ['eḡrōp̄](../../strongs/h/h106.md), and he [muwth](../../strongs/h/h4191.md) not, but [naphal](../../strongs/h/h5307.md) his [miškāḇ](../../strongs/h/h4904.md):

<a name="exodus_21_19"></a>Exodus 21:19

If he [quwm](../../strongs/h/h6965.md) again, and [halak](../../strongs/h/h1980.md) [ḥûṣ](../../strongs/h/h2351.md) upon his [mašʿēnâ](../../strongs/h/h4938.md), then shall he that [nakah](../../strongs/h/h5221.md) him be [naqah](../../strongs/h/h5352.md): only he shall [nathan](../../strongs/h/h5414.md) for the [šeḇeṯ](../../strongs/h/h7674.md), and shall cause him to be [rapha'](../../strongs/h/h7495.md) [rapha'](../../strongs/h/h7495.md).

<a name="exodus_21_20"></a>Exodus 21:20

And if an ['iysh](../../strongs/h/h376.md) [nakah](../../strongs/h/h5221.md) his ['ebed](../../strongs/h/h5650.md), or his ['amah](../../strongs/h/h519.md), with a [shebet](../../strongs/h/h7626.md), and he [muwth](../../strongs/h/h4191.md) under his [yad](../../strongs/h/h3027.md); he shall be [naqam](../../strongs/h/h5358.md) [naqam](../../strongs/h/h5358.md).

<a name="exodus_21_21"></a>Exodus 21:21

['aḵ](../../strongs/h/h389.md), if he ['amad](../../strongs/h/h5975.md) a [yowm](../../strongs/h/h3117.md) or two, he shall not be [naqam](../../strongs/h/h5358.md): for he is his [keceph](../../strongs/h/h3701.md).

<a name="exodus_21_22"></a>Exodus 21:22

If ['enowsh](../../strongs/h/h582.md) [nāṣâ](../../strongs/h/h5327.md), and [nāḡap̄](../../strongs/h/h5062.md) an ['ishshah](../../strongs/h/h802.md) [hārê](../../strongs/h/h2030.md), so that her [yeleḏ](../../strongs/h/h3206.md) [yāṣā'](../../strongs/h/h3318.md) from her, and yet no ['āsôn](../../strongs/h/h611.md): he shall [ʿānaš](../../strongs/h/h6064.md) [ʿānaš](../../strongs/h/h6064.md), according as the ['ishshah](../../strongs/h/h802.md) [baʿal](../../strongs/h/h1167.md) will [shiyth](../../strongs/h/h7896.md) upon him; and he shall [nathan](../../strongs/h/h5414.md) as the [pālîl](../../strongs/h/h6414.md) determine.

<a name="exodus_21_23"></a>Exodus 21:23

And if any ['āsôn](../../strongs/h/h611.md), then thou shalt [nathan](../../strongs/h/h5414.md) [nephesh](../../strongs/h/h5315.md) for [nephesh](../../strongs/h/h5315.md),

<a name="exodus_21_24"></a>Exodus 21:24

['ayin](../../strongs/h/h5869.md) for ['ayin](../../strongs/h/h5869.md), [šēn](../../strongs/h/h8127.md) for [šēn](../../strongs/h/h8127.md), [yad](../../strongs/h/h3027.md) for [yad](../../strongs/h/h3027.md), [regel](../../strongs/h/h7272.md) for [regel](../../strongs/h/h7272.md),

<a name="exodus_21_25"></a>Exodus 21:25

[kᵊvîyâ](../../strongs/h/h3555.md) for [kᵊvîyâ](../../strongs/h/h3555.md), [peṣaʿ](../../strongs/h/h6482.md) for [peṣaʿ](../../strongs/h/h6482.md), [ḥabûrâ](../../strongs/h/h2250.md) for [ḥabûrâ](../../strongs/h/h2250.md).

<a name="exodus_21_26"></a>Exodus 21:26

And if an ['iysh](../../strongs/h/h376.md) [nakah](../../strongs/h/h5221.md) the ['ayin](../../strongs/h/h5869.md) of his ['ebed](../../strongs/h/h5650.md), or the ['ayin](../../strongs/h/h5869.md) of his ['amah](../../strongs/h/h519.md), that it [shachath](../../strongs/h/h7843.md); he shall let him [shalach](../../strongs/h/h7971.md) [ḥāp̄šî](../../strongs/h/h2670.md) for his ['ayin](../../strongs/h/h5869.md) sake.

<a name="exodus_21_27"></a>Exodus 21:27

And if he [naphal](../../strongs/h/h5307.md) out his ['ebed](../../strongs/h/h5650.md) [šēn](../../strongs/h/h8127.md), or his ['amah](../../strongs/h/h519.md) [šēn](../../strongs/h/h8127.md); he shall let him [shalach](../../strongs/h/h7971.md) [ḥāp̄šî](../../strongs/h/h2670.md) for his [šēn](../../strongs/h/h8127.md) sake.

<a name="exodus_21_28"></a>Exodus 21:28

If a [showr](../../strongs/h/h7794.md) [nāḡaḥ](../../strongs/h/h5055.md) an ['iysh](../../strongs/h/h376.md) or an ['ishshah](../../strongs/h/h802.md), that they [muwth](../../strongs/h/h4191.md): then the [showr](../../strongs/h/h7794.md) shall be [sāqal](../../strongs/h/h5619.md) [sāqal](../../strongs/h/h5619.md), and his [basar](../../strongs/h/h1320.md) shall not be ['akal](../../strongs/h/h398.md); but the [baʿal](../../strongs/h/h1167.md) of the [showr](../../strongs/h/h7794.md) shall be [naqiy](../../strongs/h/h5355.md).

<a name="exodus_21_29"></a>Exodus 21:29

But if the [showr](../../strongs/h/h7794.md) were wont to [nagāḥ](../../strongs/h/h5056.md) in [tᵊmôl](../../strongs/h/h8543.md) [šilšôm](../../strongs/h/h8032.md), and it hath been [ʿûḏ](../../strongs/h/h5749.md) to his [baʿal](../../strongs/h/h1167.md), and he hath not [shamar](../../strongs/h/h8104.md) him in, but that he hath [muwth](../../strongs/h/h4191.md) an ['iysh](../../strongs/h/h376.md) or an ['ishshah](../../strongs/h/h802.md); the [showr](../../strongs/h/h7794.md) shall be [sāqal](../../strongs/h/h5619.md), and his [baʿal](../../strongs/h/h1167.md) also shall be [muwth](../../strongs/h/h4191.md).

<a name="exodus_21_30"></a>Exodus 21:30

If there be [shiyth](../../strongs/h/h7896.md) on him a [kōp̄er](../../strongs/h/h3724.md), then he shall [nathan](../../strongs/h/h5414.md) for the [piḏyôm](../../strongs/h/h6306.md) of his [nephesh](../../strongs/h/h5315.md) whatsoever is [shiyth](../../strongs/h/h7896.md) upon him.

<a name="exodus_21_31"></a>Exodus 21:31

Whether he have [nāḡaḥ](../../strongs/h/h5055.md) a [ben](../../strongs/h/h1121.md), or have [nāḡaḥ](../../strongs/h/h5055.md) a [bath](../../strongs/h/h1323.md), according to this [mishpat](../../strongs/h/h4941.md) shall it be ['asah](../../strongs/h/h6213.md) unto him.

<a name="exodus_21_32"></a>Exodus 21:32

If the [showr](../../strongs/h/h7794.md) shall [nāḡaḥ](../../strongs/h/h5055.md) an ['ebed](../../strongs/h/h5650.md) or an ['amah](../../strongs/h/h519.md); he shall [nathan](../../strongs/h/h5414.md) unto their ['adown](../../strongs/h/h113.md) thirty [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md), and the [showr](../../strongs/h/h7794.md) shall be [sāqal](../../strongs/h/h5619.md).

<a name="exodus_21_33"></a>Exodus 21:33

And if an ['iysh](../../strongs/h/h376.md) shall [pāṯaḥ](../../strongs/h/h6605.md) a [bowr](../../strongs/h/h953.md), or if an ['iysh](../../strongs/h/h376.md) shall [karah](../../strongs/h/h3738.md) a [bowr](../../strongs/h/h953.md), and not [kāsâ](../../strongs/h/h3680.md) it, and a [showr](../../strongs/h/h7794.md) or a [chamowr](../../strongs/h/h2543.md) [naphal](../../strongs/h/h5307.md) therein;

<a name="exodus_21_34"></a>Exodus 21:34

The [baʿal](../../strongs/h/h1167.md) of the [bowr](../../strongs/h/h953.md) shall [shalam](../../strongs/h/h7999.md), and [shuwb](../../strongs/h/h7725.md) [keceph](../../strongs/h/h3701.md) unto the [baʿal](../../strongs/h/h1167.md) of them; and the [muwth](../../strongs/h/h4191.md) shall be his.

<a name="exodus_21_35"></a>Exodus 21:35

And if an ['iysh](../../strongs/h/h376.md) [showr](../../strongs/h/h7794.md) [nāḡap̄](../../strongs/h/h5062.md) [rea'](../../strongs/h/h7453.md), that he [muwth](../../strongs/h/h4191.md); then they shall [māḵar](../../strongs/h/h4376.md) the [chay](../../strongs/h/h2416.md) [showr](../../strongs/h/h7794.md), and [ḥāṣâ](../../strongs/h/h2673.md) the [keceph](../../strongs/h/h3701.md) of it; and the [muwth](../../strongs/h/h4191.md) also they shall [ḥāṣâ](../../strongs/h/h2673.md).

<a name="exodus_21_36"></a>Exodus 21:36

Or if it be [yada'](../../strongs/h/h3045.md) that the [showr](../../strongs/h/h7794.md) hath used to [nagāḥ](../../strongs/h/h5056.md) in [tᵊmôl](../../strongs/h/h8543.md) [šilšôm](../../strongs/h/h8032.md), and his [baʿal](../../strongs/h/h1167.md) hath not [shamar](../../strongs/h/h8104.md) him in; he shall [shalam](../../strongs/h/h7999.md) [shalam](../../strongs/h/h7999.md) [showr](../../strongs/h/h7794.md) for [showr](../../strongs/h/h7794.md); and the [muwth](../../strongs/h/h4191.md) shall be his own.

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 20](exodus_20.md) - [Exodus 22](exodus_22.md)