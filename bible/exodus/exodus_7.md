# [Exodus 7](https://www.blueletterbible.org/kjv/exo/7/1/s_57001)

<a name="exodus_7_1"></a>Exodus 7:1

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [ra'ah](../../strongs/h/h7200.md), I have [nathan](../../strongs/h/h5414.md) thee an ['Elohiym](../../strongs/h/h430.md) to [Parʿô](../../strongs/h/h6547.md): and ['Ahărôn](../../strongs/h/h175.md) thy ['ach](../../strongs/h/h251.md) shall be thy [nāḇî'](../../strongs/h/h5030.md).

<a name="exodus_7_2"></a>Exodus 7:2 

Thou shalt [dabar](../../strongs/h/h1696.md) all that I [tsavah](../../strongs/h/h6680.md) thee: and ['Ahărôn](../../strongs/h/h175.md) thy ['ach](../../strongs/h/h251.md) shall [dabar](../../strongs/h/h1696.md) unto [Parʿô](../../strongs/h/h6547.md), that he [shalach](../../strongs/h/h7971.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) out of his ['erets](../../strongs/h/h776.md).

<a name="exodus_7_3"></a>Exodus 7:3

And I will [qāšâ](../../strongs/h/h7185.md) [Parʿô](../../strongs/h/h6547.md) [leb](../../strongs/h/h3820.md), and [rabah](../../strongs/h/h7235.md) my ['ôṯ](../../strongs/h/h226.md) and my [môp̄ēṯ](../../strongs/h/h4159.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_7_4"></a>Exodus 7:4

But [Parʿô](../../strongs/h/h6547.md) shall not [shama'](../../strongs/h/h8085.md) unto you, that I may [nathan](../../strongs/h/h5414.md) my [yad](../../strongs/h/h3027.md) upon [Mitsrayim](../../strongs/h/h4714.md), and [yāṣā'](../../strongs/h/h3318.md) mine [tsaba'](../../strongs/h/h6635.md), and my ['am](../../strongs/h/h5971.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) by [gadowl](../../strongs/h/h1419.md) [šep̄eṭ](../../strongs/h/h8201.md).

<a name="exodus_7_5"></a>Exodus 7:5

And the [Mitsrayim](../../strongs/h/h4714.md) shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when I [natah](../../strongs/h/h5186.md) mine [yad](../../strongs/h/h3027.md) upon [Mitsrayim](../../strongs/h/h4714.md), and [yāṣā'](../../strongs/h/h3318.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) from [tavek](../../strongs/h/h8432.md) them.

<a name="exodus_7_6"></a>Exodus 7:6

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) ['asah](../../strongs/h/h6213.md) as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) them, so ['asah](../../strongs/h/h6213.md) they.

<a name="exodus_7_7"></a>Exodus 7:7

And [Mōshe](../../strongs/h/h4872.md) was fourscore [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), and ['Ahărôn](../../strongs/h/h175.md) fourscore and three years [ben](../../strongs/h/h1121.md), when they [dabar](../../strongs/h/h1696.md) unto [Parʿô](../../strongs/h/h6547.md).

<a name="exodus_7_8"></a>Exodus 7:8

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md) and unto ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_7_9"></a>Exodus 7:9

When [Parʿô](../../strongs/h/h6547.md) shall [dabar](../../strongs/h/h1696.md) unto you, ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) a [môp̄ēṯ](../../strongs/h/h4159.md) for you: then thou shalt ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), [laqach](../../strongs/h/h3947.md) thy [maṭṭê](../../strongs/h/h4294.md), and [shalak](../../strongs/h/h7993.md) it [paniym](../../strongs/h/h6440.md) [Parʿô](../../strongs/h/h6547.md), and it shall become a [tannîn](../../strongs/h/h8577.md).

<a name="exodus_7_10"></a>Exodus 7:10

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [bow'](../../strongs/h/h935.md) unto [Parʿô](../../strongs/h/h6547.md), and they ['asah](../../strongs/h/h6213.md) so as [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md): and ['Ahărôn](../../strongs/h/h175.md) [shalak](../../strongs/h/h7993.md) his [maṭṭê](../../strongs/h/h4294.md) [paniym](../../strongs/h/h6440.md) [Parʿô](../../strongs/h/h6547.md), and [paniym](../../strongs/h/h6440.md) his ['ebed](../../strongs/h/h5650.md), and it became a [tannîn](../../strongs/h/h8577.md).

<a name="exodus_7_11"></a>Exodus 7:11

Then [Parʿô](../../strongs/h/h6547.md) also [qara'](../../strongs/h/h7121.md) the [ḥāḵām](../../strongs/h/h2450.md) and the [kāšap̄](../../strongs/h/h3784.md): now the [ḥarṭōm](../../strongs/h/h2748.md) of [Mitsrayim](../../strongs/h/h4714.md), they also ['asah](../../strongs/h/h6213.md) [kēn](../../strongs/h/h3651.md) with their [lāhaṭ](../../strongs/h/h3858.md).

<a name="exodus_7_12"></a>Exodus 7:12

For they [shalak](../../strongs/h/h7993.md) every ['iysh](../../strongs/h/h376.md) his [maṭṭê](../../strongs/h/h4294.md), and they became [tannîn](../../strongs/h/h8577.md): but ['Ahărôn](../../strongs/h/h175.md) [maṭṭê](../../strongs/h/h4294.md) [bālaʿ](../../strongs/h/h1104.md) their [maṭṭê](../../strongs/h/h4294.md).

<a name="exodus_7_13"></a>Exodus 7:13

And he [ḥāzaq](../../strongs/h/h2388.md) [Parʿô](../../strongs/h/h6547.md) [leb](../../strongs/h/h3820.md), that he [shama'](../../strongs/h/h8085.md) not unto them; as [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md).

<a name="exodus_7_14"></a>Exodus 7:14

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [Parʿô](../../strongs/h/h6547.md) [leb](../../strongs/h/h3820.md) is [kāḇēḏ](../../strongs/h/h3515.md), he [mā'ēn](../../strongs/h/h3985.md) to let the ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md).

<a name="exodus_7_15"></a>Exodus 7:15

[yālaḵ](../../strongs/h/h3212.md) thee unto [Parʿô](../../strongs/h/h6547.md) in the [boqer](../../strongs/h/h1242.md); lo, he [yāṣā'](../../strongs/h/h3318.md) unto the [mayim](../../strongs/h/h4325.md); and thou shalt [nāṣaḇ](../../strongs/h/h5324.md) by the [yᵊ'ōr](../../strongs/h/h2975.md) [saphah](../../strongs/h/h8193.md) against he [qārā'](../../strongs/h/h7125.md); and the [maṭṭê](../../strongs/h/h4294.md) which was [hāp̄aḵ](../../strongs/h/h2015.md) to a [nachash](../../strongs/h/h5175.md) shalt thou [laqach](../../strongs/h/h3947.md) in thine [yad](../../strongs/h/h3027.md).

<a name="exodus_7_16"></a>Exodus 7:16

And thou shalt ['āmar](../../strongs/h/h559.md) unto him, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of the [ʿiḇrî](../../strongs/h/h5680.md) hath [shalach](../../strongs/h/h7971.md) me unto thee, ['āmar](../../strongs/h/h559.md), Let my ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), that they may ['abad](../../strongs/h/h5647.md) me in the [midbar](../../strongs/h/h4057.md): and, behold, hitherto thou wouldest not [shama'](../../strongs/h/h8085.md).

<a name="exodus_7_17"></a>Exodus 7:17

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), In this thou shalt [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md): behold, I will [nakah](../../strongs/h/h5221.md) with the [maṭṭê](../../strongs/h/h4294.md) that is in mine [yad](../../strongs/h/h3027.md) upon the [mayim](../../strongs/h/h4325.md) which are in the [yᵊ'ōr](../../strongs/h/h2975.md), and they shall be [hāp̄aḵ](../../strongs/h/h2015.md) to [dam](../../strongs/h/h1818.md).

<a name="exodus_7_18"></a>Exodus 7:18

And the [dāḡâ](../../strongs/h/h1710.md) that is in the [yᵊ'ōr](../../strongs/h/h2975.md) shall [muwth](../../strongs/h/h4191.md), and the [yᵊ'ōr](../../strongs/h/h2975.md) shall [bā'aš](../../strongs/h/h887.md); and the [Mitsrayim](../../strongs/h/h4714.md) shall [lā'â](../../strongs/h/h3811.md) to [šāṯâ](../../strongs/h/h8354.md) of the [mayim](../../strongs/h/h4325.md) of the [yᵊ'ōr](../../strongs/h/h2975.md).

<a name="exodus_7_19"></a>Exodus 7:19

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), [laqach](../../strongs/h/h3947.md) thy [maṭṭê](../../strongs/h/h4294.md), and [natah](../../strongs/h/h5186.md) thine [yad](../../strongs/h/h3027.md) upon the [mayim](../../strongs/h/h4325.md) of [Mitsrayim](../../strongs/h/h4714.md), upon their [nāhār](../../strongs/h/h5104.md), upon their [yᵊ'ōr](../../strongs/h/h2975.md), and upon their ['ăḡam](../../strongs/h/h98.md), and upon all their [miqvê](../../strongs/h/h4723.md) of [mayim](../../strongs/h/h4325.md), that they may become [dam](../../strongs/h/h1818.md); and that there may be [dam](../../strongs/h/h1818.md) throughout all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), both in vessels of ['ets](../../strongs/h/h6086.md), and in vessels of ['eben](../../strongs/h/h68.md).

<a name="exodus_7_20"></a>Exodus 7:20

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) ['asah](../../strongs/h/h6213.md) so, as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md); and he [ruwm](../../strongs/h/h7311.md) the [maṭṭê](../../strongs/h/h4294.md), and [nakah](../../strongs/h/h5221.md) the [mayim](../../strongs/h/h4325.md) that were in the [yᵊ'ōr](../../strongs/h/h2975.md), in the ['ayin](../../strongs/h/h5869.md) of [Parʿô](../../strongs/h/h6547.md), and in the ['ayin](../../strongs/h/h5869.md) of his ['ebed](../../strongs/h/h5650.md); and all the [mayim](../../strongs/h/h4325.md) that were in the [yᵊ'ōr](../../strongs/h/h2975.md) were [hāp̄aḵ](../../strongs/h/h2015.md) to [dam](../../strongs/h/h1818.md).

<a name="exodus_7_21"></a>Exodus 7:21

And the [dāḡâ](../../strongs/h/h1710.md) that was in the [yᵊ'ōr](../../strongs/h/h2975.md) [muwth](../../strongs/h/h4191.md); and the [yᵊ'ōr](../../strongs/h/h2975.md) [bā'aš](../../strongs/h/h887.md), and the [Mitsrayim](../../strongs/h/h4714.md) [yakol](../../strongs/h/h3201.md) not [šāṯâ](../../strongs/h/h8354.md) of the [mayim](../../strongs/h/h4325.md) of the [yᵊ'ōr](../../strongs/h/h2975.md); and there was [dam](../../strongs/h/h1818.md) throughout all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_7_22"></a>Exodus 7:22

And the [ḥarṭōm](../../strongs/h/h2748.md) of [Mitsrayim](../../strongs/h/h4714.md) ['asah](../../strongs/h/h6213.md) so with their [lāṭ](../../strongs/h/h3909.md): and [Parʿô](../../strongs/h/h6547.md) [leb](../../strongs/h/h3820.md) was [ḥāzaq](../../strongs/h/h2388.md), neither did he [shama'](../../strongs/h/h8085.md) unto them; as [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md).

<a name="exodus_7_23"></a>Exodus 7:23

And [Parʿô](../../strongs/h/h6547.md) [panah](../../strongs/h/h6437.md) and [bow'](../../strongs/h/h935.md) into his [bayith](../../strongs/h/h1004.md), neither did he [shiyth](../../strongs/h/h7896.md) his [leb](../../strongs/h/h3820.md) to this also.

<a name="exodus_7_24"></a>Exodus 7:24

And all the [Mitsrayim](../../strongs/h/h4714.md) [chaphar](../../strongs/h/h2658.md) [cabiyb](../../strongs/h/h5439.md) the [yᵊ'ōr](../../strongs/h/h2975.md) for [mayim](../../strongs/h/h4325.md) to [šāṯâ](../../strongs/h/h8354.md); for they [yakol](../../strongs/h/h3201.md) not [šāṯâ](../../strongs/h/h8354.md) of the [mayim](../../strongs/h/h4325.md) of the [yᵊ'ōr](../../strongs/h/h2975.md).

<a name="exodus_7_25"></a>Exodus 7:25

And seven [yowm](../../strongs/h/h3117.md) were [mālā'](../../strongs/h/h4390.md), ['aḥar](../../strongs/h/h310.md) that [Yĕhovah](../../strongs/h/h3068.md) had [nakah](../../strongs/h/h5221.md) the [yᵊ'ōr](../../strongs/h/h2975.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 6](exodus_6.md) - [Exodus 8](exodus_8.md)