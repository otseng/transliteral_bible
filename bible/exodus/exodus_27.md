# [Exodus 27](https://www.blueletterbible.org/kjv/exo/27/1/)

<a name="exodus_27_1"></a>Exodus 27:1

And thou shalt ['asah](../../strongs/h/h6213.md) a [mizbeach](../../strongs/h/h4196.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md), five ['ammâ](../../strongs/h/h520.md) ['ōreḵ](../../strongs/h/h753.md), and five ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md); thea [mizbeach](../../strongs/h/h4196.md) shall be [rāḇaʿ](../../strongs/h/h7251.md): and the [qômâ](../../strongs/h/h6967.md) thereof shall be three ['ammâ](../../strongs/h/h520.md).

<a name="exodus_27_2"></a>Exodus 27:2

And thou shalt ['asah](../../strongs/h/h6213.md) the [qeren](../../strongs/h/h7161.md) of it upon the four [pinnâ](../../strongs/h/h6438.md) thereof: his [qeren](../../strongs/h/h7161.md) shall be of the same: and thou shalt [ṣāp̄â](../../strongs/h/h6823.md) it with [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="exodus_27_3"></a>Exodus 27:3

And thou shalt ['asah](../../strongs/h/h6213.md) his [sîr](../../strongs/h/h5518.md) to [dāšēn](../../strongs/h/h1878.md), and his [yāʿ](../../strongs/h/h3257.md), and his [mizrāq](../../strongs/h/h4219.md), and his [mazlēḡ](../../strongs/h/h4207.md), and his [maḥtâ](../../strongs/h/h4289.md): all the [kĕliy](../../strongs/h/h3627.md) thereof thou shalt ['asah](../../strongs/h/h6213.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="exodus_27_4"></a>Exodus 27:4

And thou shalt ['asah](../../strongs/h/h6213.md) for it a [maḵbēr](../../strongs/h/h4345.md) of [ma'aseh](../../strongs/h/h4639.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md); and upon the [rešeṯ](../../strongs/h/h7568.md) shalt thou ['asah](../../strongs/h/h6213.md) four [nᵊḥšeṯ](../../strongs/h/h5178.md) [ṭabaʿaṯ](../../strongs/h/h2885.md) in the four [qāṣâ](../../strongs/h/h7098.md) thereof.

<a name="exodus_27_5"></a>Exodus 27:5

And thou shalt [nathan](../../strongs/h/h5414.md) it under the [karkōḇ](../../strongs/h/h3749.md) of thea [mizbeach](../../strongs/h/h4196.md) [maṭṭâ](../../strongs/h/h4295.md), that the [rešeṯ](../../strongs/h/h7568.md) may be even to the [ḥēṣî](../../strongs/h/h2677.md) of thea [mizbeach](../../strongs/h/h4196.md).

<a name="exodus_27_6"></a>Exodus 27:6

And thou shalt ['asah](../../strongs/h/h6213.md) [baḏ](../../strongs/h/h905.md) for thea [mizbeach](../../strongs/h/h4196.md), [baḏ](../../strongs/h/h905.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md), and [ṣāp̄â](../../strongs/h/h6823.md) them with [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="exodus_27_7"></a>Exodus 27:7

And the [baḏ](../../strongs/h/h905.md) shall be [bow'](../../strongs/h/h935.md) into the [ṭabaʿaṯ](../../strongs/h/h2885.md), and the [baḏ](../../strongs/h/h905.md) shall be upon the two [tsela'](../../strongs/h/h6763.md) of thea [mizbeach](../../strongs/h/h4196.md), to [nasa'](../../strongs/h/h5375.md) it.

<a name="exodus_27_8"></a>Exodus 27:8

[nāḇaḇ](../../strongs/h/h5014.md) with [lûaḥ](../../strongs/h/h3871.md) shalt thou ['asah](../../strongs/h/h6213.md) it: as it was [ra'ah](../../strongs/h/h7200.md) thee in the [har](../../strongs/h/h2022.md), so shall they ['asah](../../strongs/h/h6213.md) it.

<a name="exodus_27_9"></a>Exodus 27:9

And thou shalt ['asah](../../strongs/h/h6213.md) the [ḥāṣēr](../../strongs/h/h2691.md) of the [miškān](../../strongs/h/h4908.md): for the [neḡeḇ](../../strongs/h/h5045.md) [pē'â](../../strongs/h/h6285.md) [têmān](../../strongs/h/h8486.md) there shall be [qelaʿ](../../strongs/h/h7050.md) for the [ḥāṣēr](../../strongs/h/h2691.md) of [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md) of an hundred ['ammâ](../../strongs/h/h520.md) ['ōreḵ](../../strongs/h/h753.md) for one [pē'â](../../strongs/h/h6285.md):

<a name="exodus_27_10"></a>Exodus 27:10

And the twenty [ʿammûḏ](../../strongs/h/h5982.md) thereof and their twenty ['eḏen](../../strongs/h/h134.md) shall be of [nᵊḥšeṯ](../../strongs/h/h5178.md); the [vāv](../../strongs/h/h2053.md) of the [ʿammûḏ](../../strongs/h/h5982.md) and their [ḥăšûqîm](../../strongs/h/h2838.md) shall be of [keceph](../../strongs/h/h3701.md).

<a name="exodus_27_11"></a>Exodus 27:11

And likewise for the [ṣāp̄ôn](../../strongs/h/h6828.md) [pē'â](../../strongs/h/h6285.md) in ['ōreḵ](../../strongs/h/h753.md) there shall be [qelaʿ](../../strongs/h/h7050.md) of an hundred ['ōreḵ](../../strongs/h/h753.md), and his twenty [ʿammûḏ](../../strongs/h/h5982.md) and their twenty ['eḏen](../../strongs/h/h134.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md); the [vāv](../../strongs/h/h2053.md) of the [ʿammûḏ](../../strongs/h/h5982.md) and their [ḥăšûqîm](../../strongs/h/h2838.md) of [keceph](../../strongs/h/h3701.md).

<a name="exodus_27_12"></a>Exodus 27:12

And for the [rōḥaḇ](../../strongs/h/h7341.md) of the [ḥāṣēr](../../strongs/h/h2691.md) on the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md) shall be [qelaʿ](../../strongs/h/h7050.md) of fifty ['ammâ](../../strongs/h/h520.md): their [ʿammûḏ](../../strongs/h/h5982.md) ten, and their ['eḏen](../../strongs/h/h134.md) ten.

<a name="exodus_27_13"></a>Exodus 27:13

And the [rōḥaḇ](../../strongs/h/h7341.md) of the [ḥāṣēr](../../strongs/h/h2691.md) on the [qeḏem](../../strongs/h/h6924.md) [pē'â](../../strongs/h/h6285.md) [mizrach](../../strongs/h/h4217.md) shall be fifty ['ammâ](../../strongs/h/h520.md).

<a name="exodus_27_14"></a>Exodus 27:14

The [qelaʿ](../../strongs/h/h7050.md) of one [kāṯēp̄](../../strongs/h/h3802.md) shall be fifteen ['ammâ](../../strongs/h/h520.md): their [ʿammûḏ](../../strongs/h/h5982.md) three, and their ['eḏen](../../strongs/h/h134.md) three.

<a name="exodus_27_15"></a>Exodus 27:15

And on the other [kāṯēp̄](../../strongs/h/h3802.md) shall be [qelaʿ](../../strongs/h/h7050.md) fifteen: their [ʿammûḏ](../../strongs/h/h5982.md) three, and their ['eḏen](../../strongs/h/h134.md) three.

<a name="exodus_27_16"></a>Exodus 27:16

And for the [sha'ar](../../strongs/h/h8179.md) of the [ḥāṣēr](../../strongs/h/h2691.md) shall be a [māsāḵ](../../strongs/h/h4539.md) of twenty ['ammâ](../../strongs/h/h520.md), of [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md), wrought with [rāqam](../../strongs/h/h7551.md) [ma'aseh](../../strongs/h/h4639.md): and their [ʿammûḏ](../../strongs/h/h5982.md) shall be four, and their ['eḏen](../../strongs/h/h134.md) four.

<a name="exodus_27_17"></a>Exodus 27:17

All the [ʿammûḏ](../../strongs/h/h5982.md) [cabiyb](../../strongs/h/h5439.md) the [ḥāṣēr](../../strongs/h/h2691.md) shall be [ḥāšaq](../../strongs/h/h2836.md) with [keceph](../../strongs/h/h3701.md); their [vāv](../../strongs/h/h2053.md) shall be of [keceph](../../strongs/h/h3701.md), and their ['eḏen](../../strongs/h/h134.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="exodus_27_18"></a>Exodus 27:18

The ['ōreḵ](../../strongs/h/h753.md) of the [ḥāṣēr](../../strongs/h/h2691.md) shall be an hundred ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) [ḥămisheem](../../strongs/h/h2572.md) [ḥămisheem](../../strongs/h/h2572.md), and the [qômâ](../../strongs/h/h6967.md) five ['ammâ](../../strongs/h/h520.md) of [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md), and their ['eḏen](../../strongs/h/h134.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="exodus_27_19"></a>Exodus 27:19

All the [kĕliy](../../strongs/h/h3627.md) of the [miškān](../../strongs/h/h4908.md) in all the [ʿăḇōḏâ](../../strongs/h/h5656.md) thereof, and all the [yāṯēḏ](../../strongs/h/h3489.md) thereof, and all the [yāṯēḏ](../../strongs/h/h3489.md) of the [ḥāṣēr](../../strongs/h/h2691.md), shall be of [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="exodus_27_20"></a>Exodus 27:20

And thou shalt [tsavah](../../strongs/h/h6680.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they [laqach](../../strongs/h/h3947.md) thee [zāḵ](../../strongs/h/h2134.md) [šemen](../../strongs/h/h8081.md) [zayiṯ](../../strongs/h/h2132.md) [kāṯîṯ](../../strongs/h/h3795.md) for the [ma'owr](../../strongs/h/h3974.md), to cause the [nîr](../../strongs/h/h5216.md) to [ʿālâ](../../strongs/h/h5927.md) [tāmîḏ](../../strongs/h/h8548.md).

<a name="exodus_27_21"></a>Exodus 27:21

In the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) [ḥûṣ](../../strongs/h/h2351.md) the [pārōḵeṯ](../../strongs/h/h6532.md), which is before the [ʿēḏûṯ](../../strongs/h/h5715.md), ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) shall ['arak](../../strongs/h/h6186.md) it from ['ereb](../../strongs/h/h6153.md) to [boqer](../../strongs/h/h1242.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): it shall be a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) unto their [dôr](../../strongs/h/h1755.md) on the behalf of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 26](exodus_26.md) - [Exodus 28](exodus_28.md)