# [Exodus 34](https://www.blueletterbible.org/kjv/exo/34/1)

<a name="exodus_34_1"></a>Exodus 34:1

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [pāsal](../../strongs/h/h6458.md) thee two [lûaḥ](../../strongs/h/h3871.md) of ['eben](../../strongs/h/h68.md) like unto the [ri'šôn](../../strongs/h/h7223.md): and I will [kāṯaḇ](../../strongs/h/h3789.md) upon these [lûaḥ](../../strongs/h/h3871.md) the [dabar](../../strongs/h/h1697.md) that were in the first [lûaḥ](../../strongs/h/h3871.md), which thou [shabar](../../strongs/h/h7665.md).

<a name="exodus_34_2"></a>Exodus 34:2

And be [kuwn](../../strongs/h/h3559.md) in the [boqer](../../strongs/h/h1242.md), and [ʿālâ](../../strongs/h/h5927.md) in the [boqer](../../strongs/h/h1242.md) unto [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md), and [nāṣaḇ](../../strongs/h/h5324.md) thyself there to me in the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md).

<a name="exodus_34_3"></a>Exodus 34:3

And no ['iysh](../../strongs/h/h376.md) shall [ʿālâ](../../strongs/h/h5927.md) with thee, neither let any ['iysh](../../strongs/h/h376.md) be [ra'ah](../../strongs/h/h7200.md) throughout all the [har](../../strongs/h/h2022.md); neither let the [tso'n](../../strongs/h/h6629.md) nor [bāqār](../../strongs/h/h1241.md) [ra'ah](../../strongs/h/h7462.md) [môl](../../strongs/h/h4136.md) that [har](../../strongs/h/h2022.md).

<a name="exodus_34_4"></a>Exodus 34:4

And he [pāsal](../../strongs/h/h6458.md) two [lûaḥ](../../strongs/h/h3871.md) of ['eben](../../strongs/h/h68.md) like unto the [ri'šôn](../../strongs/h/h7223.md); and [Mōshe](../../strongs/h/h4872.md) [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [ʿālâ](../../strongs/h/h5927.md) unto [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md), as [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md) him, and [laqach](../../strongs/h/h3947.md) in his [yad](../../strongs/h/h3027.md) the two [lûaḥ](../../strongs/h/h3871.md) of ['eben](../../strongs/h/h68.md).

<a name="exodus_34_5"></a>Exodus 34:5

And [Yĕhovah](../../strongs/h/h3068.md) [yarad](../../strongs/h/h3381.md) in the [ʿānān](../../strongs/h/h6051.md), and [yatsab](../../strongs/h/h3320.md) with him there, and [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_34_6"></a>Exodus 34:6

And [Yĕhovah](../../strongs/h/h3068.md) ['abar](../../strongs/h/h5674.md) by [paniym](../../strongs/h/h6440.md) him, and [qara'](../../strongs/h/h7121.md), [Yĕhovah](../../strongs/h/h3068.md), [Yĕhovah](../../strongs/h/h3068.md) ['el](../../strongs/h/h410.md), [raḥwm](../../strongs/h/h7349.md) and [ḥanwn](../../strongs/h/h2587.md), ['ārēḵ](../../strongs/h/h750.md) ['aph](../../strongs/h/h639.md), and [rab](../../strongs/h/h7227.md) in [checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md),

<a name="exodus_34_7"></a>Exodus 34:7

[nāṣar](../../strongs/h/h5341.md) [checed](../../strongs/h/h2617.md) for ['elep̄](../../strongs/h/h505.md), [nasa'](../../strongs/h/h5375.md) ['avon](../../strongs/h/h5771.md) and [pesha'](../../strongs/h/h6588.md) and [chatta'ath](../../strongs/h/h2403.md), and that will not [naqah](../../strongs/h/h5352.md) [naqah](../../strongs/h/h5352.md); [paqad](../../strongs/h/h6485.md) the ['avon](../../strongs/h/h5771.md) of the ['ab](../../strongs/h/h1.md) upon the [ben](../../strongs/h/h1121.md), and upon the [ben](../../strongs/h/h1121.md), unto the third and to the fourth.

<a name="exodus_34_8"></a>Exodus 34:8

And [Mōshe](../../strongs/h/h4872.md) [māhar](../../strongs/h/h4116.md), and [qāḏaḏ](../../strongs/h/h6915.md) toward the ['erets](../../strongs/h/h776.md), and [shachah](../../strongs/h/h7812.md).

<a name="exodus_34_9"></a>Exodus 34:9

And he ['āmar](../../strongs/h/h559.md), If now I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md), ['adonay](../../strongs/h/h136.md), let my ['adonay](../../strongs/h/h136.md), I pray thee, [yālaḵ](../../strongs/h/h3212.md) [qereḇ](../../strongs/h/h7130.md) us; for it is a [qāšê](../../strongs/h/h7186.md) [ʿōrep̄](../../strongs/h/h6203.md) ['am](../../strongs/h/h5971.md); and [sālaḥ](../../strongs/h/h5545.md) our ['avon](../../strongs/h/h5771.md) and our [chatta'ath](../../strongs/h/h2403.md), and take us for thine [nāḥal](../../strongs/h/h5157.md).

<a name="exodus_34_10"></a>Exodus 34:10

And he ['āmar](../../strongs/h/h559.md), Behold, I [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md): before all thy ['am](../../strongs/h/h5971.md) I will ['asah](../../strongs/h/h6213.md) [pala'](../../strongs/h/h6381.md), such as have not been [bara'](../../strongs/h/h1254.md) in all the ['erets](../../strongs/h/h776.md), nor in any [gowy](../../strongs/h/h1471.md): and all the ['am](../../strongs/h/h5971.md) [qereḇ](../../strongs/h/h7130.md) which thou art shall [ra'ah](../../strongs/h/h7200.md) the [ma'aseh](../../strongs/h/h4639.md) of [Yĕhovah](../../strongs/h/h3068.md): for it is a [yare'](../../strongs/h/h3372.md) that I will ['asah](../../strongs/h/h6213.md) with thee.

<a name="exodus_34_11"></a>Exodus 34:11

[shamar](../../strongs/h/h8104.md) thou that which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md): behold, I [gāraš](../../strongs/h/h1644.md) [paniym](../../strongs/h/h6440.md) thee the ['Ĕmōrî](../../strongs/h/h567.md), and the [Kᵊnaʿănî](../../strongs/h/h3669.md), and the [Ḥitî](../../strongs/h/h2850.md), and the [Pᵊrizzî](../../strongs/h/h6522.md), and the [Ḥiûî](../../strongs/h/h2340.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md).

<a name="exodus_34_12"></a>Exodus 34:12

[shamar](../../strongs/h/h8104.md) to thyself, lest thou [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) whither thou [bow'](../../strongs/h/h935.md), lest it be for a [mowqesh](../../strongs/h/h4170.md) in the [qereḇ](../../strongs/h/h7130.md) of thee:


<a name="exodus_34_13"></a>Exodus 34:13

But ye shall [nāṯaṣ](../../strongs/h/h5422.md) theira [mizbeach](../../strongs/h/h4196.md), [shabar](../../strongs/h/h7665.md) their [maṣṣēḇâ](../../strongs/h/h4676.md), and [karath](../../strongs/h/h3772.md) their ['ăšērâ](../../strongs/h/h842.md):

<a name="exodus_34_14"></a>Exodus 34:14

For thou shalt [shachah](../../strongs/h/h7812.md) no ['aḥēr](../../strongs/h/h312.md) ['el](../../strongs/h/h410.md): for [Yĕhovah](../../strongs/h/h3068.md), whose [shem](../../strongs/h/h8034.md) is [qanna'](../../strongs/h/h7067.md), is a [qanna'](../../strongs/h/h7067.md) ['el](../../strongs/h/h410.md):

<a name="exodus_34_15"></a>Exodus 34:15

Lest thou [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md), and they [zānâ](../../strongs/h/h2181.md) ['aḥar](../../strongs/h/h310.md) their ['Elohiym](../../strongs/h/h430.md), and do [zabach](../../strongs/h/h2076.md) unto their ['Elohiym](../../strongs/h/h430.md), and one [qara'](../../strongs/h/h7121.md) thee, and thou ['akal](../../strongs/h/h398.md) of his [zebach](../../strongs/h/h2077.md);

<a name="exodus_34_16"></a>Exodus 34:16

And thou [laqach](../../strongs/h/h3947.md) of their [bath](../../strongs/h/h1323.md) unto thy [ben](../../strongs/h/h1121.md), and their [bath](../../strongs/h/h1323.md) [zānâ](../../strongs/h/h2181.md) ['aḥar](../../strongs/h/h310.md) their ['Elohiym](../../strongs/h/h430.md), and make thy [ben](../../strongs/h/h1121.md) [zānâ](../../strongs/h/h2181.md) ['aḥar](../../strongs/h/h310.md) their ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_34_17"></a>Exodus 34:17

Thou shalt ['asah](../../strongs/h/h6213.md) thee no [massēḵâ](../../strongs/h/h4541.md) ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_34_18"></a>Exodus 34:18

The [ḥāḡ](../../strongs/h/h2282.md) of [maṣṣâ](../../strongs/h/h4682.md) shalt thou [shamar](../../strongs/h/h8104.md). Seven [yowm](../../strongs/h/h3117.md) thou shalt ['akal](../../strongs/h/h398.md) [maṣṣâ](../../strongs/h/h4682.md), as I [tsavah](../../strongs/h/h6680.md) thee, in the [môʿēḏ](../../strongs/h/h4150.md) of the [ḥōḏeš](../../strongs/h/h2320.md) ['āḇîḇ](../../strongs/h/h24.md): for in the [ḥōḏeš](../../strongs/h/h2320.md) ['āḇîḇ](../../strongs/h/h24.md) thou [yāṣā'](../../strongs/h/h3318.md) from [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_34_19"></a>Exodus 34:19

All that [peṭer](../../strongs/h/h6363.md) the [reḥem](../../strongs/h/h7358.md) is mine; and every [peṭer](../../strongs/h/h6363.md) among thy [miqnê](../../strongs/h/h4735.md), whether [showr](../../strongs/h/h7794.md) or [śê](../../strongs/h/h7716.md), that is [zakar](../../strongs/h/h2142.md). [^1]

<a name="exodus_34_20"></a>Exodus 34:20

But the [peṭer](../../strongs/h/h6363.md) of a [chamowr](../../strongs/h/h2543.md) thou shalt [pāḏâ](../../strongs/h/h6299.md) with a [śê](../../strongs/h/h7716.md): and if thou [pāḏâ](../../strongs/h/h6299.md) him not, then shalt thou [ʿārap̄](../../strongs/h/h6202.md). All the [bᵊḵôr](../../strongs/h/h1060.md) of thy [ben](../../strongs/h/h1121.md) thou shalt [pāḏâ](../../strongs/h/h6299.md). And none shall [ra'ah](../../strongs/h/h7200.md) [paniym](../../strongs/h/h6440.md) me [rêqām](../../strongs/h/h7387.md).

<a name="exodus_34_21"></a>Exodus 34:21

Six [yowm](../../strongs/h/h3117.md) thou shalt ['abad](../../strongs/h/h5647.md), but on the seventh [yowm](../../strongs/h/h3117.md) thou shalt [shabath](../../strongs/h/h7673.md): in [ḥārîš](../../strongs/h/h2758.md) and in [qāṣîr](../../strongs/h/h7105.md) thou shalt [shabath](../../strongs/h/h7673.md).

<a name="exodus_34_22"></a>Exodus 34:22

And thou shalt ['asah](../../strongs/h/h6213.md) the [ḥāḡ](../../strongs/h/h2282.md) of [šāḇûaʿ](../../strongs/h/h7620.md), of the [bikûr](../../strongs/h/h1061.md) of [ḥiṭṭâ](../../strongs/h/h2406.md) [qāṣîr](../../strongs/h/h7105.md), and the [ḥāḡ](../../strongs/h/h2282.md) of ['āsîp̄](../../strongs/h/h614.md) at the [šānâ](../../strongs/h/h8141.md) [tᵊqûp̄â](../../strongs/h/h8622.md).

<a name="exodus_34_23"></a>Exodus 34:23

Thrice in the [šānâ](../../strongs/h/h8141.md) shall all your [zāḵûr](../../strongs/h/h2138.md) [ra'ah](../../strongs/h/h7200.md) [paniym](../../strongs/h/h6440.md) ['adown](../../strongs/h/h113.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="exodus_34_24"></a>Exodus 34:24

For I will [yarash](../../strongs/h/h3423.md) the [gowy](../../strongs/h/h1471.md) [paniym](../../strongs/h/h6440.md) thee, and [rāḥaḇ](../../strongs/h/h7337.md) thy [gᵊḇûl](../../strongs/h/h1366.md): neither shall any ['iysh](../../strongs/h/h376.md) [chamad](../../strongs/h/h2530.md) thy ['erets](../../strongs/h/h776.md), when thou shalt [ʿālâ](../../strongs/h/h5927.md) to [ra'ah](../../strongs/h/h7200.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) thrice in the [šānâ](../../strongs/h/h8141.md).

<a name="exodus_34_25"></a>Exodus 34:25

Thou shalt not [šāḥaṭ](../../strongs/h/h7819.md) the [dam](../../strongs/h/h1818.md) of my [zebach](../../strongs/h/h2077.md) with [ḥāmēṣ](../../strongs/h/h2557.md); neither shall the [zebach](../../strongs/h/h2077.md) of the [ḥāḡ](../../strongs/h/h2282.md) of the [pecach](../../strongs/h/h6453.md) be [lûn](../../strongs/h/h3885.md) unto the [boqer](../../strongs/h/h1242.md).

<a name="exodus_34_26"></a>Exodus 34:26

The [re'shiyth](../../strongs/h/h7225.md) of the [bikûr](../../strongs/h/h1061.md) of thy ['ăḏāmâ](../../strongs/h/h127.md) thou shalt [bow'](../../strongs/h/h935.md) unto the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md). Thou shalt not [bāšal](../../strongs/h/h1310.md) a [gᵊḏî](../../strongs/h/h1423.md) in his ['em](../../strongs/h/h517.md) [chalab](../../strongs/h/h2461.md).

<a name="exodus_34_27"></a>Exodus 34:27

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [kāṯaḇ](../../strongs/h/h3789.md) thou these [dabar](../../strongs/h/h1697.md): for after the [peh](../../strongs/h/h6310.md) of these [dabar](../../strongs/h/h1697.md) I have [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with thee and with [Yisra'el](../../strongs/h/h3478.md).

<a name="exodus_34_28"></a>Exodus 34:28

And he was there with [Yĕhovah](../../strongs/h/h3068.md) forty [yowm](../../strongs/h/h3117.md) and forty [layil](../../strongs/h/h3915.md); he did neither ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md), nor [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md). And he [kāṯaḇ](../../strongs/h/h3789.md) upon the [lûaḥ](../../strongs/h/h3871.md) the [dabar](../../strongs/h/h1697.md) of the [bĕriyth](../../strongs/h/h1285.md), the ten [dabar](../../strongs/h/h1697.md).

<a name="exodus_34_29"></a>Exodus 34:29

And it came to pass, when [Mōshe](../../strongs/h/h4872.md) [yarad](../../strongs/h/h3381.md) from [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md) with the two [lûaḥ](../../strongs/h/h3871.md) of [ʿēḏûṯ](../../strongs/h/h5715.md) in [Mōshe](../../strongs/h/h4872.md) [yad](../../strongs/h/h3027.md), when he [yarad](../../strongs/h/h3381.md) from the [har](../../strongs/h/h2022.md), that [Mōshe](../../strongs/h/h4872.md) [yada'](../../strongs/h/h3045.md) not that the ['owr](../../strongs/h/h5785.md) of his [paniym](../../strongs/h/h6440.md) [qāran](../../strongs/h/h7160.md) while he [dabar](../../strongs/h/h1696.md) with him.

<a name="exodus_34_30"></a>Exodus 34:30

And when ['Ahărôn](../../strongs/h/h175.md) and all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ra'ah](../../strongs/h/h7200.md) [Mōshe](../../strongs/h/h4872.md), behold, the ['owr](../../strongs/h/h5785.md) of his [paniym](../../strongs/h/h6440.md) [qāran](../../strongs/h/h7160.md); and they were [yare'](../../strongs/h/h3372.md) to [nāḡaš](../../strongs/h/h5066.md) him.

<a name="exodus_34_31"></a>Exodus 34:31

And [Mōshe](../../strongs/h/h4872.md) [qara'](../../strongs/h/h7121.md) unto them; and ['Ahărôn](../../strongs/h/h175.md) and all the [nāśî'](../../strongs/h/h5387.md) of the ['edah](../../strongs/h/h5712.md) [shuwb](../../strongs/h/h7725.md) unto him: and [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) with them.

<a name="exodus_34_32"></a>Exodus 34:32

And ['aḥar](../../strongs/h/h310.md) all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāḡaš](../../strongs/h/h5066.md): and he gave them in [tsavah](../../strongs/h/h6680.md) all that [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md) with him in [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md).

<a name="exodus_34_33"></a>Exodus 34:33

And till [Mōshe](../../strongs/h/h4872.md) had [kalah](../../strongs/h/h3615.md) [dabar](../../strongs/h/h1696.md) with them, he [nathan](../../strongs/h/h5414.md) a [masvê](../../strongs/h/h4533.md) on his [paniym](../../strongs/h/h6440.md).

<a name="exodus_34_34"></a>Exodus 34:34

But when [Mōshe](../../strongs/h/h4872.md) [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) to [dabar](../../strongs/h/h1696.md) with him, he [cuwr](../../strongs/h/h5493.md) the [masvê](../../strongs/h/h4533.md), until he [yāṣā'](../../strongs/h/h3318.md). And he [yāṣā'](../../strongs/h/h3318.md), and [dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) that which he was [tsavah](../../strongs/h/h6680.md).

<a name="exodus_34_35"></a>Exodus 34:35

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ra'ah](../../strongs/h/h7200.md) the [paniym](../../strongs/h/h6440.md) of [Mōshe](../../strongs/h/h4872.md), that the ['owr](../../strongs/h/h5785.md) of [Mōshe](../../strongs/h/h4872.md) [paniym](../../strongs/h/h6440.md) [qāran](../../strongs/h/h7160.md): and [Mōshe](../../strongs/h/h4872.md) [shuwb](../../strongs/h/h7725.md) the [masvê](../../strongs/h/h4533.md) upon his [paniym](../../strongs/h/h6440.md), until he [bow'](../../strongs/h/h935.md) to [dabar](../../strongs/h/h1696.md) with him.

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 33](exodus_33.md) - [Exodus 35](exodus_35.md)

---

[^1]: [Exodus 34:19 Commentary](../../commentary/exodus/exodus_34_commentary.md#exodus_34_19)