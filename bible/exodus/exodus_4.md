# [Exodus 4](https://www.blueletterbible.org/kjv/exo/4/1/s_54001)

<a name="exodus_4_1"></a>Exodus 4:1

And [Mōshe](../../strongs/h/h4872.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), But, behold, they will not ['aman](../../strongs/h/h539.md) me, nor [shama'](../../strongs/h/h8085.md) unto my [qowl](../../strongs/h/h6963.md): for they will ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) hath not [ra'ah](../../strongs/h/h7200.md) unto thee.

<a name="exodus_4_2"></a>Exodus 4:2

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, What is that in thine [yad](../../strongs/h/h3027.md)? And he ['āmar](../../strongs/h/h559.md), A [maṭṭê](../../strongs/h/h4294.md).

<a name="exodus_4_3"></a>Exodus 4:3

And he ['āmar](../../strongs/h/h559.md), [shalak](../../strongs/h/h7993.md) it on the ['erets](../../strongs/h/h776.md). And he [shalak](../../strongs/h/h7993.md) it on the ['erets](../../strongs/h/h776.md), and it became a [nachash](../../strongs/h/h5175.md); and [Mōshe](../../strongs/h/h4872.md) [nûs](../../strongs/h/h5127.md) from [paniym](../../strongs/h/h6440.md) it.

<a name="exodus_4_4"></a>Exodus 4:4

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [shalach](../../strongs/h/h7971.md) thine [yad](../../strongs/h/h3027.md), and ['āḥaz](../../strongs/h/h270.md) it by the [zānāḇ](../../strongs/h/h2180.md). And he [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md), and [ḥāzaq](../../strongs/h/h2388.md) it, and it became a [maṭṭê](../../strongs/h/h4294.md) in his [kaph](../../strongs/h/h3709.md):

<a name="exodus_4_5"></a>Exodus 4:5

That they may ['aman](../../strongs/h/h539.md) that [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md), the ['Elohiym](../../strongs/h/h430.md) of ['Abraham](../../strongs/h/h85.md), the ['Elohiym](../../strongs/h/h430.md) of [Yiṣḥāq](../../strongs/h/h3327.md), and the ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md), hath [ra'ah](../../strongs/h/h7200.md) unto thee.

<a name="exodus_4_6"></a>Exodus 4:6

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) [ʿôḏ](../../strongs/h/h5750.md) unto him, [bow'](../../strongs/h/h935.md) now thine [yad](../../strongs/h/h3027.md) into thy [ḥêq](../../strongs/h/h2436.md). And he [bow'](../../strongs/h/h935.md) his [yad](../../strongs/h/h3027.md) into his [ḥêq](../../strongs/h/h2436.md): and when he [yāṣā'](../../strongs/h/h3318.md) it out, behold, his [yad](../../strongs/h/h3027.md) was [ṣāraʿ](../../strongs/h/h6879.md) as [šeleḡ](../../strongs/h/h7950.md).

<a name="exodus_4_7"></a>Exodus 4:7

And he ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md) thine [yad](../../strongs/h/h3027.md) into thy [ḥêq](../../strongs/h/h2436.md). And he [shuwb](../../strongs/h/h7725.md) his [yad](../../strongs/h/h3027.md) into his [ḥêq](../../strongs/h/h2436.md) again; and [yāṣā'](../../strongs/h/h3318.md) it out of his [ḥêq](../../strongs/h/h2436.md), and, behold, it was [shuwb](../../strongs/h/h7725.md) as his other [basar](../../strongs/h/h1320.md).

<a name="exodus_4_8"></a>Exodus 4:8

And it shall come to pass, if they will not ['aman](../../strongs/h/h539.md) thee, neither [shama'](../../strongs/h/h8085.md) to the [qowl](../../strongs/h/h6963.md) of the [ri'šôn](../../strongs/h/h7223.md) ['ôṯ](../../strongs/h/h226.md), that they will ['aman](../../strongs/h/h539.md) the [qowl](../../strongs/h/h6963.md) of the ['aḥărôn](../../strongs/h/h314.md) ['ôṯ](../../strongs/h/h226.md).

<a name="exodus_4_9"></a>Exodus 4:9

And it shall come to pass, if they will not ['aman](../../strongs/h/h539.md) also these two ['ôṯ](../../strongs/h/h226.md), neither [shama'](../../strongs/h/h8085.md) unto thy [qowl](../../strongs/h/h6963.md), that thou shalt [laqach](../../strongs/h/h3947.md) of the [mayim](../../strongs/h/h4325.md) of the [yᵊ'ōr](../../strongs/h/h2975.md), and [šāp̄aḵ](../../strongs/h/h8210.md) it upon the [yabāšâ](../../strongs/h/h3004.md): and the [mayim](../../strongs/h/h4325.md) which thou [laqach](../../strongs/h/h3947.md) out of the [yᵊ'ōr](../../strongs/h/h2975.md) shall become [dam](../../strongs/h/h1818.md) upon the [yabešeṯ](../../strongs/h/h3006.md).

<a name="exodus_4_10"></a>Exodus 4:10

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto [Yĕhovah](../../strongs/h/h3068.md), [bî](../../strongs/h/h994.md) ['adonay](../../strongs/h/h136.md), ['iysh](../../strongs/h/h376.md) am not [dabar](../../strongs/h/h1697.md), neither [šilšôm](../../strongs/h/h8032.md) [tᵊmôl](../../strongs/h/h8543.md), nor since thou hast [dabar](../../strongs/h/h1696.md) unto thy ['ebed](../../strongs/h/h5650.md): but I am [kāḇēḏ](../../strongs/h/h3515.md) of [peh](../../strongs/h/h6310.md), and of a [kāḇēḏ](../../strongs/h/h3515.md) [lashown](../../strongs/h/h3956.md).

<a name="exodus_4_11"></a>Exodus 4:11

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, Who hath [śûm](../../strongs/h/h7760.md) ['adam](../../strongs/h/h120.md) [peh](../../strongs/h/h6310.md)? or who [śûm](../../strongs/h/h7760.md) the ['illēm](../../strongs/h/h483.md), or [ḥērēš](../../strongs/h/h2795.md), or the [piqqēaḥ](../../strongs/h/h6493.md), or the [ʿiûēr](../../strongs/h/h5787.md)? have not I [Yĕhovah](../../strongs/h/h3068.md)?

<a name="exodus_4_12"></a>Exodus 4:12

Now therefore [yālaḵ](../../strongs/h/h3212.md), and I will be with thy [peh](../../strongs/h/h6310.md), and [yārâ](../../strongs/h/h3384.md) thee what thou shalt [dabar](../../strongs/h/h1696.md).

<a name="exodus_4_13"></a>Exodus 4:13

And he ['āmar](../../strongs/h/h559.md), [bî](../../strongs/h/h994.md) ['adonay](../../strongs/h/h136.md), [shalach](../../strongs/h/h7971.md), I pray thee, by the [yad](../../strongs/h/h3027.md) of him whom thou wilt [shalach](../../strongs/h/h7971.md).

<a name="exodus_4_14"></a>Exodus 4:14

And the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against [Mōshe](../../strongs/h/h4872.md), and he ['āmar](../../strongs/h/h559.md), Is not ['Ahărôn](../../strongs/h/h175.md) the [Lᵊvî](../../strongs/h/h3881.md) thy ['ach](../../strongs/h/h251.md)? I [yada'](../../strongs/h/h3045.md) that he can [dabar](../../strongs/h/h1696.md) [dabar](../../strongs/h/h1696.md). And also, behold, he [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) thee: and when he [ra'ah](../../strongs/h/h7200.md) thee, he will be [samach](../../strongs/h/h8055.md) in his [leb](../../strongs/h/h3820.md).

<a name="exodus_4_15"></a>Exodus 4:15

And thou shalt [dabar](../../strongs/h/h1696.md) unto him, and [śûm](../../strongs/h/h7760.md) [dabar](../../strongs/h/h1697.md) in his [peh](../../strongs/h/h6310.md): and I will be with thy [peh](../../strongs/h/h6310.md), and with his [peh](../../strongs/h/h6310.md), and will [yārâ](../../strongs/h/h3384.md) you what ye shall ['asah](../../strongs/h/h6213.md).

<a name="exodus_4_16"></a>Exodus 4:16

And he shall be thy [dabar](../../strongs/h/h1696.md) unto the ['am](../../strongs/h/h5971.md): and he shall be, even he shall be to thee instead of a [peh](../../strongs/h/h6310.md), and thou shalt be to him instead of ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_4_17"></a>Exodus 4:17

And thou shalt [laqach](../../strongs/h/h3947.md) this [maṭṭê](../../strongs/h/h4294.md) in thine [yad](../../strongs/h/h3027.md), wherewith thou shalt ['asah](../../strongs/h/h6213.md) ['ôṯ](../../strongs/h/h226.md).

<a name="exodus_4_18"></a>Exodus 4:18

And [Mōshe](../../strongs/h/h4872.md) [yālaḵ](../../strongs/h/h3212.md) and [shuwb](../../strongs/h/h7725.md) to [Yeṯer](../../strongs/h/h3500.md) his [ḥāṯan](../../strongs/h/h2859.md), and ['āmar](../../strongs/h/h559.md) unto him, Let me [yālaḵ](../../strongs/h/h3212.md), I pray thee, and [shuwb](../../strongs/h/h7725.md) unto my ['ach](../../strongs/h/h251.md) which are in [Mitsrayim](../../strongs/h/h4714.md), and [ra'ah](../../strongs/h/h7200.md) whether they be yet [chay](../../strongs/h/h2416.md). And [Yiṯrô](../../strongs/h/h3503.md) ['āmar](../../strongs/h/h559.md) to [Mōshe](../../strongs/h/h4872.md), [yālaḵ](../../strongs/h/h3212.md) in [shalowm](../../strongs/h/h7965.md).

<a name="exodus_4_19"></a>Exodus 4:19

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md) in [Miḏyān](../../strongs/h/h4080.md), [yālaḵ](../../strongs/h/h3212.md), [shuwb](../../strongs/h/h7725.md) into [Mitsrayim](../../strongs/h/h4714.md): for all the ['enowsh](../../strongs/h/h582.md) are [muwth](../../strongs/h/h4191.md) which [bāqaš](../../strongs/h/h1245.md) thy [nephesh](../../strongs/h/h5315.md).

<a name="exodus_4_20"></a>Exodus 4:20

And [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) his ['ishshah](../../strongs/h/h802.md) and his [ben](../../strongs/h/h1121.md), and [rāḵaḇ](../../strongs/h/h7392.md) them upon a [chamowr](../../strongs/h/h2543.md), and he [shuwb](../../strongs/h/h7725.md) to the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): and [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) the [maṭṭê](../../strongs/h/h4294.md) of ['Elohiym](../../strongs/h/h430.md) in his [yad](../../strongs/h/h3027.md).

<a name="exodus_4_21"></a>Exodus 4:21

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), When thou [yālaḵ](../../strongs/h/h3212.md) to [shuwb](../../strongs/h/h7725.md) into [Mitsrayim](../../strongs/h/h4714.md), [ra'ah](../../strongs/h/h7200.md) that thou ['asah](../../strongs/h/h6213.md) all those [môp̄ēṯ](../../strongs/h/h4159.md) [paniym](../../strongs/h/h6440.md) [Parʿô](../../strongs/h/h6547.md), which I have [śûm](../../strongs/h/h7760.md) in thine [yad](../../strongs/h/h3027.md): but I will [ḥāzaq](../../strongs/h/h2388.md) his [leb](../../strongs/h/h3820.md), that he shall not let the ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md).

<a name="exodus_4_22"></a>Exodus 4:22

And thou shalt ['āmar](../../strongs/h/h559.md) unto [Parʿô](../../strongs/h/h6547.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), [Yisra'el](../../strongs/h/h3478.md) is my [ben](../../strongs/h/h1121.md), even my [bᵊḵôr](../../strongs/h/h1060.md):

<a name="exodus_4_23"></a>Exodus 4:23

And I ['āmar](../../strongs/h/h559.md) unto thee, Let my [ben](../../strongs/h/h1121.md) [shalach](../../strongs/h/h7971.md), that he may ['abad](../../strongs/h/h5647.md) me: and if thou [mā'ēn](../../strongs/h/h3985.md) to let him [shalach](../../strongs/h/h7971.md), behold, I will [harag](../../strongs/h/h2026.md) thy [ben](../../strongs/h/h1121.md), even thy [bᵊḵôr](../../strongs/h/h1060.md).

<a name="exodus_4_24"></a>Exodus 4:24

And it came to pass by the [derek](../../strongs/h/h1870.md) in the [mālôn](../../strongs/h/h4411.md), that [Yĕhovah](../../strongs/h/h3068.md) [pāḡaš](../../strongs/h/h6298.md) him, and [bāqaš](../../strongs/h/h1245.md) to [muwth](../../strongs/h/h4191.md) him.

<a name="exodus_4_25"></a>Exodus 4:25

Then [ṣipōrâ](../../strongs/h/h6855.md) [laqach](../../strongs/h/h3947.md) a [ṣōr](../../strongs/h/h6864.md), and [karath](../../strongs/h/h3772.md) the [ʿārlâ](../../strongs/h/h6190.md) of her [ben](../../strongs/h/h1121.md), and [naga'](../../strongs/h/h5060.md) it at his [regel](../../strongs/h/h7272.md), and ['āmar](../../strongs/h/h559.md), Surely a [dam](../../strongs/h/h1818.md) [ḥāṯān](../../strongs/h/h2860.md) art thou to me.

<a name="exodus_4_26"></a>Exodus 4:26

So he let him [rāp̄â](../../strongs/h/h7503.md): then she ['āmar](../../strongs/h/h559.md), A [dam](../../strongs/h/h1818.md) [ḥāṯān](../../strongs/h/h2860.md) thou art, because of the [muwlah](../../strongs/h/h4139.md).

<a name="exodus_4_27"></a>Exodus 4:27

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) to ['Ahărôn](../../strongs/h/h175.md), [yālaḵ](../../strongs/h/h3212.md) into the [midbar](../../strongs/h/h4057.md) to [qārā'](../../strongs/h/h7125.md) [Mōshe](../../strongs/h/h4872.md). And he [yālaḵ](../../strongs/h/h3212.md), and [pāḡaš](../../strongs/h/h6298.md) him in the [har](../../strongs/h/h2022.md) of ['Elohiym](../../strongs/h/h430.md), and [nashaq](../../strongs/h/h5401.md) him.

<a name="exodus_4_28"></a>Exodus 4:28

And [Mōshe](../../strongs/h/h4872.md) [nāḡaḏ](../../strongs/h/h5046.md) ['Ahărôn](../../strongs/h/h175.md) all the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) who had [shalach](../../strongs/h/h7971.md) him, and all the ['ôṯ](../../strongs/h/h226.md) which he had [tsavah](../../strongs/h/h6680.md) him.

<a name="exodus_4_29"></a>Exodus 4:29

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [yālaḵ](../../strongs/h/h3212.md) and ['āsap̄](../../strongs/h/h622.md) all the [zāqēn](../../strongs/h/h2205.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="exodus_4_30"></a>Exodus 4:30

And ['Ahărôn](../../strongs/h/h175.md) [dabar](../../strongs/h/h1696.md) all the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), and ['asah](../../strongs/h/h6213.md) the ['ôṯ](../../strongs/h/h226.md) in the ['ayin](../../strongs/h/h5869.md) of the ['am](../../strongs/h/h5971.md).

<a name="exodus_4_31"></a>Exodus 4:31

And the ['am](../../strongs/h/h5971.md) ['aman](../../strongs/h/h539.md): and when they [shama'](../../strongs/h/h8085.md) that [Yĕhovah](../../strongs/h/h3068.md) had [paqad](../../strongs/h/h6485.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and that he had [ra'ah](../../strongs/h/h7200.md) upon their ['oniy](../../strongs/h/h6040.md), then they [qāḏaḏ](../../strongs/h/h6915.md) and [shachah](../../strongs/h/h7812.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 3](exodus_3.md) - [Exodus 5](exodus_5.md)