# [Exodus 1](https://www.blueletterbible.org/kjv/exo/1/1/s_51001)

<a name="exodus_1_1"></a>Exodus 1:1

Now these are the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md); every ['iysh](../../strongs/h/h376.md) and his [bayith](../../strongs/h/h1004.md) [bow'](../../strongs/h/h935.md) with [Ya'aqob](../../strongs/h/h3290.md).

<a name="exodus_1_2"></a>Exodus 1:2

[Rᵊ'ûḇēn](../../strongs/h/h7205.md), [Šimʿôn](../../strongs/h/h8095.md), [Lēvî](../../strongs/h/h3878.md), and [Yehuwdah](../../strongs/h/h3063.md),

<a name="exodus_1_3"></a>Exodus 1:3

[Yiśśāśḵār](../../strongs/h/h3485.md), [Zᵊḇûlûn](../../strongs/h/h2074.md), and [Binyāmîn](../../strongs/h/h1144.md),

<a name="exodus_1_4"></a>Exodus 1:4

[Dān](../../strongs/h/h1835.md), and [Nap̄tālî](../../strongs/h/h5321.md), [Gāḏ](../../strongs/h/h1410.md), and ['Āšēr](../../strongs/h/h836.md).

<a name="exodus_1_5"></a>Exodus 1:5

And all the [nephesh](../../strongs/h/h5315.md) that [yāṣā'](../../strongs/h/h3318.md) of the [yārēḵ](../../strongs/h/h3409.md) of [Ya'aqob](../../strongs/h/h3290.md) were seventy [nephesh](../../strongs/h/h5315.md): for [Yôsēp̄](../../strongs/h/h3130.md) was in [Mitsrayim](../../strongs/h/h4714.md) already.

<a name="exodus_1_6"></a>Exodus 1:6

And [Yôsēp̄](../../strongs/h/h3130.md) [muwth](../../strongs/h/h4191.md), and all his ['ach](../../strongs/h/h251.md), and all that [dôr](../../strongs/h/h1755.md).

<a name="exodus_1_7"></a>Exodus 1:7

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were [parah](../../strongs/h/h6509.md), and [šāraṣ](../../strongs/h/h8317.md), and [rabah](../../strongs/h/h7235.md), and [me'od](../../strongs/h/h3966.md) [me'od](../../strongs/h/h3966.md) [ʿāṣam](../../strongs/h/h6105.md); and the ['erets](../../strongs/h/h776.md) was [mālā'](../../strongs/h/h4390.md) with them.

<a name="exodus_1_8"></a>Exodus 1:8

Now there [quwm](../../strongs/h/h6965.md) a [ḥāḏāš](../../strongs/h/h2319.md) [melek](../../strongs/h/h4428.md) over [Mitsrayim](../../strongs/h/h4714.md), which [yada'](../../strongs/h/h3045.md) not [Yôsēp̄](../../strongs/h/h3130.md).

<a name="exodus_1_9"></a>Exodus 1:9

And he ['āmar](../../strongs/h/h559.md) unto his ['am](../../strongs/h/h5971.md), Behold, the ['am](../../strongs/h/h5971.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) are [rab](../../strongs/h/h7227.md) and ['atsuwm](../../strongs/h/h6099.md) than we:

<a name="exodus_1_10"></a>Exodus 1:10

[yāhaḇ](../../strongs/h/h3051.md), let us [ḥāḵam](../../strongs/h/h2449.md) with them; lest they [rabah](../../strongs/h/h7235.md), and it come to pass, that, when there [qārā'](../../strongs/h/h7122.md) any [milḥāmâ](../../strongs/h/h4421.md), they [yāsap̄](../../strongs/h/h3254.md) also unto our [sane'](../../strongs/h/h8130.md), and [lāḥam](../../strongs/h/h3898.md) against us, and so [ʿālâ](../../strongs/h/h5927.md) them out of the ['erets](../../strongs/h/h776.md).

<a name="exodus_1_11"></a>Exodus 1:11

Therefore they did [śûm](../../strongs/h/h7760.md) over them [mas](../../strongs/h/h4522.md) [śar](../../strongs/h/h8269.md) to [ʿānâ](../../strongs/h/h6031.md) them with their [sᵊḇālâ](../../strongs/h/h5450.md). And they [bānâ](../../strongs/h/h1129.md) for [Parʿô](../../strongs/h/h6547.md) [miskᵊnôṯ](../../strongs/h/h4543.md) [ʿîr](../../strongs/h/h5892.md), [piṯōm](../../strongs/h/h6619.md) and [Raʿmᵊsēs](../../strongs/h/h7486.md).

<a name="exodus_1_12"></a>Exodus 1:12

But the more they [ʿānâ](../../strongs/h/h6031.md) them, the [kēn](../../strongs/h/h3651.md) they [rabah](../../strongs/h/h7235.md) and [pāraṣ](../../strongs/h/h6555.md). And they were [qûṣ](../../strongs/h/h6973.md) [paniym](../../strongs/h/h6440.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="exodus_1_13"></a>Exodus 1:13

And the [Mitsrayim](../../strongs/h/h4714.md) made the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) to ['abad](../../strongs/h/h5647.md) with [pereḵ](../../strongs/h/h6531.md):

<a name="exodus_1_14"></a>Exodus 1:14

And they made their [chay](../../strongs/h/h2416.md) [mārar](../../strongs/h/h4843.md) with [qāšê](../../strongs/h/h7186.md) [ʿăḇōḏâ](../../strongs/h/h5656.md), in [ḥōmer](../../strongs/h/h2563.md), and in [lᵊḇēnâ](../../strongs/h/h3843.md), and in all manner of [ʿăḇōḏâ](../../strongs/h/h5656.md) in the [sadeh](../../strongs/h/h7704.md): all their [ʿăḇōḏâ](../../strongs/h/h5656.md), wherein they made them ['abad](../../strongs/h/h5647.md), was with [pereḵ](../../strongs/h/h6531.md).

<a name="exodus_1_15"></a>Exodus 1:15

And the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) ['āmar](../../strongs/h/h559.md) to the [ʿiḇrî](../../strongs/h/h5680.md) [yalad](../../strongs/h/h3205.md), of which the [shem](../../strongs/h/h8034.md) of the one was [šip̄râ](../../strongs/h/h8236.md), and the [shem](../../strongs/h/h8034.md) of the other [pûʿâ](../../strongs/h/h6326.md):

<a name="exodus_1_16"></a>Exodus 1:16

And he ['āmar](../../strongs/h/h559.md), When ye do the office of a [yalad](../../strongs/h/h3205.md) to the [ʿiḇrî](../../strongs/h/h5680.md), and [ra'ah](../../strongs/h/h7200.md) them upon the ['ōḇen](../../strongs/h/h70.md); if it be a [ben](../../strongs/h/h1121.md), then ye shall [muwth](../../strongs/h/h4191.md) him: but if it be a [bath](../../strongs/h/h1323.md), then she shall [chayay](../../strongs/h/h2425.md).

<a name="exodus_1_17"></a>Exodus 1:17

But the [yalad](../../strongs/h/h3205.md) [yare'](../../strongs/h/h3372.md) ['Elohiym](../../strongs/h/h430.md), and ['asah](../../strongs/h/h6213.md) not as the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) [dabar](../../strongs/h/h1696.md) them, but [ḥāyâ](../../strongs/h/h2421.md) the [yeleḏ](../../strongs/h/h3206.md) [ḥāyâ](../../strongs/h/h2421.md).

<a name="exodus_1_18"></a>Exodus 1:18

And the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) [qara'](../../strongs/h/h7121.md) for the [yalad](../../strongs/h/h3205.md), and ['āmar](../../strongs/h/h559.md) unto them, [madûaʿ](../../strongs/h/h4069.md) have ye ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md), and have [ḥāyâ](../../strongs/h/h2421.md) the [yeleḏ](../../strongs/h/h3206.md)?

<a name="exodus_1_19"></a>Exodus 1:19

And the [yalad](../../strongs/h/h3205.md) ['āmar](../../strongs/h/h559.md) unto [Parʿô](../../strongs/h/h6547.md), Because the [ʿiḇrî](../../strongs/h/h5680.md) ['ishshah](../../strongs/h/h802.md) are not as the [Miṣrî](../../strongs/h/h4713.md); for they are [ḥāyê](../../strongs/h/h2422.md), and are [yalad](../../strongs/h/h3205.md) [ṭerem](../../strongs/h/h2962.md) the [yalad](../../strongs/h/h3205.md) [bow'](../../strongs/h/h935.md) unto them.

<a name="exodus_1_20"></a>Exodus 1:20

Therefore ['Elohiym](../../strongs/h/h430.md) dealt [yatab](../../strongs/h/h3190.md) with the [yalad](../../strongs/h/h3205.md): and the ['am](../../strongs/h/h5971.md) [rabah](../../strongs/h/h7235.md), and [me'od](../../strongs/h/h3966.md) [ʿāṣam](../../strongs/h/h6105.md).

<a name="exodus_1_21"></a>Exodus 1:21

And it came to pass, because the [yalad](../../strongs/h/h3205.md) [yare'](../../strongs/h/h3372.md) ['Elohiym](../../strongs/h/h430.md), that he ['asah](../../strongs/h/h6213.md) them [bayith](../../strongs/h/h1004.md).

<a name="exodus_1_22"></a>Exodus 1:22

And [Parʿô](../../strongs/h/h6547.md) [tsavah](../../strongs/h/h6680.md) all his ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), Every [ben](../../strongs/h/h1121.md) that is [yillôḏ](../../strongs/h/h3209.md) ye shall [shalak](../../strongs/h/h7993.md) into the [yᵊ'ōr](../../strongs/h/h2975.md), and every [bath](../../strongs/h/h1323.md) ye shall [ḥāyâ](../../strongs/h/h2421.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 2](exodus_2.md)