# [Exodus 28](https://www.blueletterbible.org/kjv/exo/28/1/)

<a name="exodus_28_1"></a>Exodus 28:1

And [qāraḇ](../../strongs/h/h7126.md) thou unto thee ['Ahărôn](../../strongs/h/h175.md) thy ['ach](../../strongs/h/h251.md), and his [ben](../../strongs/h/h1121.md) with him, from [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that he may [kāhan](../../strongs/h/h3547.md), even ['Ahărôn](../../strongs/h/h175.md), [Nāḏāḇ](../../strongs/h/h5070.md) and ['Ăḇîhû'](../../strongs/h/h30.md), ['Elʿāzār](../../strongs/h/h499.md) and ['Îṯāmār](../../strongs/h/h385.md), ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md).

<a name="exodus_28_2"></a>Exodus 28:2

And thou shalt ['asah](../../strongs/h/h6213.md) [qodesh](../../strongs/h/h6944.md) [beḡeḏ](../../strongs/h/h899.md) for ['Ahărôn](../../strongs/h/h175.md) thy ['ach](../../strongs/h/h251.md) for [kabowd](../../strongs/h/h3519.md) and for [tip̄'ārâ](../../strongs/h/h8597.md).

<a name="exodus_28_3"></a>Exodus 28:3

And thou shalt [dabar](../../strongs/h/h1696.md) unto all that are [ḥāḵām](../../strongs/h/h2450.md) [leb](../../strongs/h/h3820.md), whom I have [mālā'](../../strongs/h/h4390.md) with the [ruwach](../../strongs/h/h7307.md) of [ḥāḵmâ](../../strongs/h/h2451.md), that they may ['asah](../../strongs/h/h6213.md) ['Ahărôn](../../strongs/h/h175.md) [beḡeḏ](../../strongs/h/h899.md) to [qadash](../../strongs/h/h6942.md) him, that he may [kāhan](../../strongs/h/h3547.md).

<a name="exodus_28_4"></a>Exodus 28:4

And these are the [beḡeḏ](../../strongs/h/h899.md) which they shall ['asah](../../strongs/h/h6213.md); a [ḥōšen](../../strongs/h/h2833.md), and an ['ēp̄ôḏ](../../strongs/h/h646.md), and a [mᵊʿîl](../../strongs/h/h4598.md), and a [tašbēṣ](../../strongs/h/h8665.md) [kĕthoneth](../../strongs/h/h3801.md), a [miṣnep̄eṯ](../../strongs/h/h4701.md), and an ['aḇnēṭ](../../strongs/h/h73.md): and they shall ['asah](../../strongs/h/h6213.md) [qodesh](../../strongs/h/h6944.md) [beḡeḏ](../../strongs/h/h899.md) for ['Ahărôn](../../strongs/h/h175.md) thy ['ach](../../strongs/h/h251.md), and his [ben](../../strongs/h/h1121.md), that he may [kāhan](../../strongs/h/h3547.md).

<a name="exodus_28_5"></a>Exodus 28:5

And they shall [laqach](../../strongs/h/h3947.md) [zāhāḇ](../../strongs/h/h2091.md), and [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šēš](../../strongs/h/h8336.md).

<a name="exodus_28_6"></a>Exodus 28:6

And they shall ['asah](../../strongs/h/h6213.md) the ['ēp̄ôḏ](../../strongs/h/h646.md) of [zāhāḇ](../../strongs/h/h2091.md), of [tᵊḵēleṯ](../../strongs/h/h8504.md), and of ['argāmān](../../strongs/h/h713.md), of [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md), with [chashab](../../strongs/h/h2803.md) [ma'aseh](../../strongs/h/h4639.md).

<a name="exodus_28_7"></a>Exodus 28:7

It shall have the two [kāṯēp̄](../../strongs/h/h3802.md) thereof [ḥāḇar](../../strongs/h/h2266.md) at the two [qāṣâ](../../strongs/h/h7098.md) thereof; and so it shall be [ḥāḇar](../../strongs/h/h2266.md).

<a name="exodus_28_8"></a>Exodus 28:8

And the [ḥēšeḇ](../../strongs/h/h2805.md) of the ['ăp̄udâ](../../strongs/h/h642.md), which is upon it, shall be of the same, according to the [ma'aseh](../../strongs/h/h4639.md) thereof; even of [zāhāḇ](../../strongs/h/h2091.md), of [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md).

<a name="exodus_28_9"></a>Exodus 28:9

And thou shalt [laqach](../../strongs/h/h3947.md) two [šōham](../../strongs/h/h7718.md) ['eben](../../strongs/h/h68.md), and [pāṯaḥ](../../strongs/h/h6605.md) on them the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="exodus_28_10"></a>Exodus 28:10

Six of their [shem](../../strongs/h/h8034.md) on one ['eben](../../strongs/h/h68.md), and the other six [shem](../../strongs/h/h8034.md) of the [yāṯar](../../strongs/h/h3498.md) on the other ['eben](../../strongs/h/h68.md), according to their [towlĕdah](../../strongs/h/h8435.md).

<a name="exodus_28_11"></a>Exodus 28:11

With the [ma'aseh](../../strongs/h/h4639.md) of a [ḥārāš](../../strongs/h/h2796.md) in ['eben](../../strongs/h/h68.md), like the [pitûaḥ](../../strongs/h/h6603.md) of a [ḥôṯām](../../strongs/h/h2368.md), shalt thou [pāṯaḥ](../../strongs/h/h6605.md) the two ['eben](../../strongs/h/h68.md) with the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): thou shalt ['asah](../../strongs/h/h6213.md) them to be [mûsaḇâ](../../strongs/h/h4142.md) in [mišbᵊṣôṯ](../../strongs/h/h4865.md) of [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_28_12"></a>Exodus 28:12

And thou shalt [śûm](../../strongs/h/h7760.md) the two ['eben](../../strongs/h/h68.md) upon the [kāṯēp̄](../../strongs/h/h3802.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md) for ['eben](../../strongs/h/h68.md) of [zikārôn](../../strongs/h/h2146.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): and ['Ahărôn](../../strongs/h/h175.md) shall [nasa'](../../strongs/h/h5375.md) their [shem](../../strongs/h/h8034.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) upon his two [kāṯēp̄](../../strongs/h/h3802.md) for a [zikārôn](../../strongs/h/h2146.md).

<a name="exodus_28_13"></a>Exodus 28:13

And thou shalt ['asah](../../strongs/h/h6213.md) [mišbᵊṣôṯ](../../strongs/h/h4865.md) of [zāhāḇ](../../strongs/h/h2091.md);

<a name="exodus_28_14"></a>Exodus 28:14

And two [šaršᵊrâ](../../strongs/h/h8333.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md) at the [miḡbālôṯ](../../strongs/h/h4020.md); of [ʿăḇōṯ](../../strongs/h/h5688.md) [ma'aseh](../../strongs/h/h4639.md) shalt thou ['asah](../../strongs/h/h6213.md) them, and [nathan](../../strongs/h/h5414.md) the [ʿăḇōṯ](../../strongs/h/h5688.md) [šaršᵊrâ](../../strongs/h/h8333.md) to the [mišbᵊṣôṯ](../../strongs/h/h4865.md).

<a name="exodus_28_15"></a>Exodus 28:15

And thou shalt ['asah](../../strongs/h/h6213.md) the [ḥōšen](../../strongs/h/h2833.md) of [mishpat](../../strongs/h/h4941.md) with [chashab](../../strongs/h/h2803.md) [ma'aseh](../../strongs/h/h4639.md); after the [ma'aseh](../../strongs/h/h4639.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md) thou shalt ['asah](../../strongs/h/h6213.md) it; of [zāhāḇ](../../strongs/h/h2091.md), of [tᵊḵēleṯ](../../strongs/h/h8504.md), and of ['argāmān](../../strongs/h/h713.md), and of [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and of [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md), shalt thou ['asah](../../strongs/h/h6213.md) it.

<a name="exodus_28_16"></a>Exodus 28:16

[rāḇaʿ](../../strongs/h/h7251.md) it shall be being [kāp̄al](../../strongs/h/h3717.md); a [zereṯ](../../strongs/h/h2239.md) shall be the ['ōreḵ](../../strongs/h/h753.md) thereof, and a [zereṯ](../../strongs/h/h2239.md) shall be the [rōḥaḇ](../../strongs/h/h7341.md) thereof.

<a name="exodus_28_17"></a>Exodus 28:17

And thou shalt [mālā'](../../strongs/h/h4390.md) in it [millu'â](../../strongs/h/h4396.md) of ['eben](../../strongs/h/h68.md), even four [ṭûr](../../strongs/h/h2905.md) of ['eben](../../strongs/h/h68.md): the first [ṭûr](../../strongs/h/h2905.md) shall be a ['ōḏem](../../strongs/h/h124.md), a [piṭḏâ](../../strongs/h/h6357.md), and a [bāreqeṯ](../../strongs/h/h1304.md): this shall be the first [ṭûr](../../strongs/h/h2905.md).

<a name="exodus_28_18"></a>Exodus 28:18

And the second [ṭûr](../../strongs/h/h2905.md) shall be a [nōp̄eḵ](../../strongs/h/h5306.md), a [sapîr](../../strongs/h/h5601.md), and a [yahălōm](../../strongs/h/h3095.md).

<a name="exodus_28_19"></a>Exodus 28:19

And the third [ṭûr](../../strongs/h/h2905.md) a [lešem](../../strongs/h/h3958.md), a [šᵊḇô](../../strongs/h/h7618.md), and an ['aḥlāmâ](../../strongs/h/h306.md).

<a name="exodus_28_20"></a>Exodus 28:20

And the fourth [ṭûr](../../strongs/h/h2905.md) a [taršîš](../../strongs/h/h8658.md), and a [šōham](../../strongs/h/h7718.md), and a [yāšp̄ih](../../strongs/h/h3471.md): they shall be [šāḇaṣ](../../strongs/h/h7660.md) in [zāhāḇ](../../strongs/h/h2091.md) in their [millu'â](../../strongs/h/h4396.md).

<a name="exodus_28_21"></a>Exodus 28:21

And the ['eben](../../strongs/h/h68.md) shall be with the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), twelve, according to their [shem](../../strongs/h/h8034.md), like the [pitûaḥ](../../strongs/h/h6603.md) of a [ḥôṯām](../../strongs/h/h2368.md); every ['iysh](../../strongs/h/h376.md) with his [shem](../../strongs/h/h8034.md) shall they be according to the twelve [shebet](../../strongs/h/h7626.md).

<a name="exodus_28_22"></a>Exodus 28:22

And thou shalt ['asah](../../strongs/h/h6213.md) upon the [ḥōšen](../../strongs/h/h2833.md) [šaršâ](../../strongs/h/h8331.md) at the [gaḇluṯ](../../strongs/h/h1383.md) of [ʿăḇōṯ](../../strongs/h/h5688.md) [ma'aseh](../../strongs/h/h4639.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_28_23"></a>Exodus 28:23

And thou shalt ['asah](../../strongs/h/h6213.md) upon the [ḥōšen](../../strongs/h/h2833.md) two [ṭabaʿaṯ](../../strongs/h/h2885.md) of [zāhāḇ](../../strongs/h/h2091.md), and shalt [nathan](../../strongs/h/h5414.md) the two [ṭabaʿaṯ](../../strongs/h/h2885.md) on the two [qāṣâ](../../strongs/h/h7098.md) of the [ḥōšen](../../strongs/h/h2833.md).

<a name="exodus_28_24"></a>Exodus 28:24

And thou shalt [nathan](../../strongs/h/h5414.md) the two [ʿăḇōṯ](../../strongs/h/h5688.md) [zāhāḇ](../../strongs/h/h2091.md) in the two [ṭabaʿaṯ](../../strongs/h/h2885.md) which are on the [qāṣâ](../../strongs/h/h7098.md) of the [ḥōšen](../../strongs/h/h2833.md).

<a name="exodus_28_25"></a>Exodus 28:25

And the other two [qāṣâ](../../strongs/h/h7098.md) of the two [ʿăḇōṯ](../../strongs/h/h5688.md) thou shalt [nathan](../../strongs/h/h5414.md) in the two [mišbᵊṣôṯ](../../strongs/h/h4865.md), and [nathan](../../strongs/h/h5414.md) them on the [kāṯēp̄](../../strongs/h/h3802.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md) [paniym](../../strongs/h/h6440.md) [môl](../../strongs/h/h4136.md).

<a name="exodus_28_26"></a>Exodus 28:26

And thou shalt ['asah](../../strongs/h/h6213.md) two [ṭabaʿaṯ](../../strongs/h/h2885.md) of [zāhāḇ](../../strongs/h/h2091.md), and thou shalt [śûm](../../strongs/h/h7760.md) them upon the two [qāṣâ](../../strongs/h/h7098.md) of the [ḥōšen](../../strongs/h/h2833.md) in the [saphah](../../strongs/h/h8193.md) thereof, which is in the [ʿēḇer](../../strongs/h/h5676.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md) [bayith](../../strongs/h/h1004.md).

<a name="exodus_28_27"></a>Exodus 28:27

And two other [ṭabaʿaṯ](../../strongs/h/h2885.md) of [zāhāḇ](../../strongs/h/h2091.md) thou shalt ['asah](../../strongs/h/h6213.md), and shalt [nathan](../../strongs/h/h5414.md) them on the two [kāṯēp̄](../../strongs/h/h3802.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md) [maṭṭâ](../../strongs/h/h4295.md), [môl](../../strongs/h/h4136.md) the [paniym](../../strongs/h/h6440.md) thereof, over [ʿummâ](../../strongs/h/h5980.md) the other [maḥbereṯ](../../strongs/h/h4225.md) thereof, [maʿal](../../strongs/h/h4605.md) the [ḥēšeḇ](../../strongs/h/h2805.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md).

<a name="exodus_28_28"></a>Exodus 28:28

And they shall [rāḵas](../../strongs/h/h7405.md) the [ḥōšen](../../strongs/h/h2833.md) by the [ṭabaʿaṯ](../../strongs/h/h2885.md) thereof unto the [ṭabaʿaṯ](../../strongs/h/h2885.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md) with a [pāṯîl](../../strongs/h/h6616.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), that it may be above the [ḥēšeḇ](../../strongs/h/h2805.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md), and that the [ḥōšen](../../strongs/h/h2833.md) be not [zāḥaḥ](../../strongs/h/h2118.md) from the ['ēp̄ôḏ](../../strongs/h/h646.md).

<a name="exodus_28_29"></a>Exodus 28:29

And ['Ahărôn](../../strongs/h/h175.md) shall [nasa'](../../strongs/h/h5375.md) the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) in the [ḥōšen](../../strongs/h/h2833.md) of [mishpat](../../strongs/h/h4941.md) upon his [leb](../../strongs/h/h3820.md), when he [bow'](../../strongs/h/h935.md) in unto the [qodesh](../../strongs/h/h6944.md), for a [zikārôn](../../strongs/h/h2146.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) [tāmîḏ](../../strongs/h/h8548.md).

<a name="exodus_28_30"></a>Exodus 28:30

And thou shalt [nathan](../../strongs/h/h5414.md) in the [ḥōšen](../../strongs/h/h2833.md) of [mishpat](../../strongs/h/h4941.md) the ['Ûrîm](../../strongs/h/h224.md) and the [Tummîm](../../strongs/h/h8550.md); and they shall be upon ['Ahărôn](../../strongs/h/h175.md) [leb](../../strongs/h/h3820.md), when he [bow'](../../strongs/h/h935.md) in [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): and ['Ahărôn](../../strongs/h/h175.md) shall [nasa'](../../strongs/h/h5375.md) the [mishpat](../../strongs/h/h4941.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) upon his [leb](../../strongs/h/h3820.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) [tāmîḏ](../../strongs/h/h8548.md).

<a name="exodus_28_31"></a>Exodus 28:31

And thou shalt ['asah](../../strongs/h/h6213.md) the [mᵊʿîl](../../strongs/h/h4598.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md) [kālîl](../../strongs/h/h3632.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md).

<a name="exodus_28_32"></a>Exodus 28:32

And there shall be a [peh](../../strongs/h/h6310.md) in the [ro'sh](../../strongs/h/h7218.md) of it, in the [tavek](../../strongs/h/h8432.md) thereof: it shall have a [saphah](../../strongs/h/h8193.md) of ['āraḡ](../../strongs/h/h707.md) [ma'aseh](../../strongs/h/h4639.md) [cabiyb](../../strongs/h/h5439.md) the [peh](../../strongs/h/h6310.md) of it, as it were the [peh](../../strongs/h/h6310.md) of a [taḥrā'](../../strongs/h/h8473.md), that it be not [qāraʿ](../../strongs/h/h7167.md).

<a name="exodus_28_33"></a>Exodus 28:33

And beneath upon the [shuwl](../../strongs/h/h7757.md) of it thou shalt ['asah](../../strongs/h/h6213.md) [rimmôn](../../strongs/h/h7416.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), and of ['argāmān](../../strongs/h/h713.md), and of [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), [cabiyb](../../strongs/h/h5439.md) the [shuwl](../../strongs/h/h7757.md) thereof; and [paʿămôn](../../strongs/h/h6472.md) of [zāhāḇ](../../strongs/h/h2091.md) [tavek](../../strongs/h/h8432.md) them [cabiyb](../../strongs/h/h5439.md):

<a name="exodus_28_34"></a>Exodus 28:34

A [zāhāḇ](../../strongs/h/h2091.md) [paʿămôn](../../strongs/h/h6472.md) and a [rimmôn](../../strongs/h/h7416.md), a [zāhāḇ](../../strongs/h/h2091.md) [paʿămôn](../../strongs/h/h6472.md) and a [rimmôn](../../strongs/h/h7416.md), upon the [shuwl](../../strongs/h/h7757.md) of the [mᵊʿîl](../../strongs/h/h4598.md) [cabiyb](../../strongs/h/h5439.md).

<a name="exodus_28_35"></a>Exodus 28:35

And it shall be upon ['Ahărôn](../../strongs/h/h175.md) to [sharath](../../strongs/h/h8334.md): and his [qowl](../../strongs/h/h6963.md) shall be [shama'](../../strongs/h/h8085.md) when he [bow'](../../strongs/h/h935.md) in unto the [qodesh](../../strongs/h/h6944.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and when he [yāṣā'](../../strongs/h/h3318.md) out, that he [muwth](../../strongs/h/h4191.md) not.

<a name="exodus_28_36"></a>Exodus 28:36

And thou shalt ['asah](../../strongs/h/h6213.md) a [tsiyts](../../strongs/h/h6731.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md), and [pāṯaḥ](../../strongs/h/h6605.md) upon it, like the [pitûaḥ](../../strongs/h/h6603.md) of a [ḥôṯām](../../strongs/h/h2368.md), [qodesh](../../strongs/h/h6944.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_28_37"></a>Exodus 28:37

And thou shalt [śûm](../../strongs/h/h7760.md) it on a [tᵊḵēleṯ](../../strongs/h/h8504.md) [pāṯîl](../../strongs/h/h6616.md), that it may be upon the [miṣnep̄eṯ](../../strongs/h/h4701.md); upon the [paniym](../../strongs/h/h6440.md) [môl](../../strongs/h/h4136.md) of the [miṣnep̄eṯ](../../strongs/h/h4701.md) it shall be.

<a name="exodus_28_38"></a>Exodus 28:38

And it shall be upon ['Ahărôn](../../strongs/h/h175.md) [mēṣaḥ](../../strongs/h/h4696.md), that ['Ahărôn](../../strongs/h/h175.md) may [nasa'](../../strongs/h/h5375.md) the ['avon](../../strongs/h/h5771.md) of the [qodesh](../../strongs/h/h6944.md), which the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) shall [qadash](../../strongs/h/h6942.md) in all their [qodesh](../../strongs/h/h6944.md) [matānâ](../../strongs/h/h4979.md); and it shall be [tāmîḏ](../../strongs/h/h8548.md) upon his [mēṣaḥ](../../strongs/h/h4696.md), that they may be [ratsown](../../strongs/h/h7522.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_28_39"></a>Exodus 28:39

And thou shalt [šāḇaṣ](../../strongs/h/h7660.md) the [kĕthoneth](../../strongs/h/h3801.md) of [šēš](../../strongs/h/h8336.md), and thou shalt ['asah](../../strongs/h/h6213.md) the [miṣnep̄eṯ](../../strongs/h/h4701.md) of [šēš](../../strongs/h/h8336.md), and thou shalt ['asah](../../strongs/h/h6213.md) the ['aḇnēṭ](../../strongs/h/h73.md) of [rāqam](../../strongs/h/h7551.md) [ma'aseh](../../strongs/h/h4639.md).

<a name="exodus_28_40"></a>Exodus 28:40

And for ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md) thou shalt ['asah](../../strongs/h/h6213.md) [kĕthoneth](../../strongs/h/h3801.md), and thou shalt ['asah](../../strongs/h/h6213.md) for them ['aḇnēṭ](../../strongs/h/h73.md), and [miḡbāʿâ](../../strongs/h/h4021.md) shalt thou ['asah](../../strongs/h/h6213.md) for them, for [kabowd](../../strongs/h/h3519.md) and for [tip̄'ārâ](../../strongs/h/h8597.md).

<a name="exodus_28_41"></a>Exodus 28:41

And thou shalt [labash](../../strongs/h/h3847.md) them upon ['Ahărôn](../../strongs/h/h175.md) thy ['ach](../../strongs/h/h251.md), and his [ben](../../strongs/h/h1121.md) with him; and shalt [māšaḥ](../../strongs/h/h4886.md) them, and [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) them, and [qadash](../../strongs/h/h6942.md) them, that they may [kāhan](../../strongs/h/h3547.md).

<a name="exodus_28_42"></a>Exodus 28:42

And thou shalt ['asah](../../strongs/h/h6213.md) them [baḏ](../../strongs/h/h906.md) [miḵnās](../../strongs/h/h4370.md) to [kāsâ](../../strongs/h/h3680.md) their [basar](../../strongs/h/h1320.md) [ʿervâ](../../strongs/h/h6172.md); from the [māṯnayim](../../strongs/h/h4975.md) even unto the [yārēḵ](../../strongs/h/h3409.md) they shall reach:

<a name="exodus_28_43"></a>Exodus 28:43

And they shall be upon ['Ahărôn](../../strongs/h/h175.md), and upon his [ben](../../strongs/h/h1121.md), when they [bow'](../../strongs/h/h935.md) unto the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), or when they [nāḡaš](../../strongs/h/h5066.md) unto thea [mizbeach](../../strongs/h/h4196.md) to [sharath](../../strongs/h/h8334.md) in the [qodesh](../../strongs/h/h6944.md) place; that they [nasa'](../../strongs/h/h5375.md) not ['avon](../../strongs/h/h5771.md), and [muwth](../../strongs/h/h4191.md): it shall be a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) unto him and his [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) him.

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 27](exodus_27.md) - [Exodus 29](exodus_29.md)