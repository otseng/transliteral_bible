# [Exodus 29](https://www.blueletterbible.org/kjv/exo/29/1/)

<a name="exodus_29_1"></a>Exodus 29:1

And this is the [dabar](../../strongs/h/h1697.md) that thou shalt ['asah](../../strongs/h/h6213.md) unto them to [qadash](../../strongs/h/h6942.md) them, to [kāhan](../../strongs/h/h3547.md): [laqach](../../strongs/h/h3947.md) one [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md), and two ['ayil](../../strongs/h/h352.md) [tamiym](../../strongs/h/h8549.md),

<a name="exodus_29_2"></a>Exodus 29:2

And [maṣṣâ](../../strongs/h/h4682.md) [lechem](../../strongs/h/h3899.md), and [ḥallâ](../../strongs/h/h2471.md) [maṣṣâ](../../strongs/h/h4682.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), and [rāqîq](../../strongs/h/h7550.md) [maṣṣâ](../../strongs/h/h4682.md) [māšaḥ](../../strongs/h/h4886.md) with [šemen](../../strongs/h/h8081.md): of [ḥiṭṭâ](../../strongs/h/h2406.md) [sōleṯ](../../strongs/h/h5560.md) shalt thou ['asah](../../strongs/h/h6213.md) them.

<a name="exodus_29_3"></a>Exodus 29:3

And thou shalt [nathan](../../strongs/h/h5414.md) them into one [sal](../../strongs/h/h5536.md), and [qāraḇ](../../strongs/h/h7126.md) them in the [sal](../../strongs/h/h5536.md), with the [par](../../strongs/h/h6499.md) and the two ['ayil](../../strongs/h/h352.md).

<a name="exodus_29_4"></a>Exodus 29:4

And ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) thou shalt [qāraḇ](../../strongs/h/h7126.md) unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and shalt [rāḥaṣ](../../strongs/h/h7364.md) them with [mayim](../../strongs/h/h4325.md).

<a name="exodus_29_5"></a>Exodus 29:5

And thou shalt [laqach](../../strongs/h/h3947.md) the [beḡeḏ](../../strongs/h/h899.md), and [labash](../../strongs/h/h3847.md) upon ['Ahărôn](../../strongs/h/h175.md) the [kĕthoneth](../../strongs/h/h3801.md), and the [mᵊʿîl](../../strongs/h/h4598.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md), and the ['ēp̄ôḏ](../../strongs/h/h646.md), and the [ḥōšen](../../strongs/h/h2833.md), and ['āp̄aḏ](../../strongs/h/h640.md) him with the [ḥēšeḇ](../../strongs/h/h2805.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md):

<a name="exodus_29_6"></a>Exodus 29:6

And thou shalt [śûm](../../strongs/h/h7760.md) the [miṣnep̄eṯ](../../strongs/h/h4701.md) upon his [ro'sh](../../strongs/h/h7218.md), and [nathan](../../strongs/h/h5414.md) the [qodesh](../../strongs/h/h6944.md) [nēzer](../../strongs/h/h5145.md) upon the [miṣnep̄eṯ](../../strongs/h/h4701.md).

<a name="exodus_29_7"></a>Exodus 29:7

Then shalt thou [laqach](../../strongs/h/h3947.md) the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and [yāṣaq](../../strongs/h/h3332.md) it upon his [ro'sh](../../strongs/h/h7218.md), and [māšaḥ](../../strongs/h/h4886.md) him.

<a name="exodus_29_8"></a>Exodus 29:8

And thou shalt [qāraḇ](../../strongs/h/h7126.md) his [ben](../../strongs/h/h1121.md), and [labash](../../strongs/h/h3847.md) [kĕthoneth](../../strongs/h/h3801.md) upon them.

<a name="exodus_29_9"></a>Exodus 29:9

And thou shalt [ḥāḡar](../../strongs/h/h2296.md) them with ['aḇnēṭ](../../strongs/h/h73.md), ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md), and [ḥāḇaš](../../strongs/h/h2280.md) the [miḡbāʿâ](../../strongs/h/h4021.md) on them: and the [kᵊhunnâ](../../strongs/h/h3550.md) shall be theirs for a ['owlam](../../strongs/h/h5769.md) [chuqqah](../../strongs/h/h2708.md): and thou shalt [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md).

<a name="exodus_29_10"></a>Exodus 29:10

And thou shalt cause a [par](../../strongs/h/h6499.md) to be [qāraḇ](../../strongs/h/h7126.md) [paniym](../../strongs/h/h6440.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md): and ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) shall [camak](../../strongs/h/h5564.md) their [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [par](../../strongs/h/h6499.md).

<a name="exodus_29_11"></a>Exodus 29:11

And thou shalt [šāḥaṭ](../../strongs/h/h7819.md) the [par](../../strongs/h/h6499.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), by the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="exodus_29_12"></a>Exodus 29:12

And thou shalt [laqach](../../strongs/h/h3947.md) of the [dam](../../strongs/h/h1818.md) of the [par](../../strongs/h/h6499.md), and [nathan](../../strongs/h/h5414.md) it upon the [qeren](../../strongs/h/h7161.md) of thea [mizbeach](../../strongs/h/h4196.md) with thy ['etsba'](../../strongs/h/h676.md), and [šāp̄aḵ](../../strongs/h/h8210.md) all the [dam](../../strongs/h/h1818.md) beside the [yᵊsôḏ](../../strongs/h/h3247.md) of thea [mizbeach](../../strongs/h/h4196.md).

<a name="exodus_29_13"></a>Exodus 29:13

And thou shalt [laqach](../../strongs/h/h3947.md) all the [cheleb](../../strongs/h/h2459.md) that [kāsâ](../../strongs/h/h3680.md) the [qereḇ](../../strongs/h/h7130.md), and the [yōṯereṯ](../../strongs/h/h3508.md) that is above the [kāḇēḏ](../../strongs/h/h3516.md), and the two [kilyah](../../strongs/h/h3629.md), and the [cheleb](../../strongs/h/h2459.md) that is upon them, and [qāṭar](../../strongs/h/h6999.md) them upon thea [mizbeach](../../strongs/h/h4196.md).

<a name="exodus_29_14"></a>Exodus 29:14

But the [basar](../../strongs/h/h1320.md) of the [par](../../strongs/h/h6499.md), and his ['owr](../../strongs/h/h5785.md), and his [pereš](../../strongs/h/h6569.md), shalt thou [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md) [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md): it is a [chatta'ath](../../strongs/h/h2403.md).

<a name="exodus_29_15"></a>Exodus 29:15

Thou shalt also [laqach](../../strongs/h/h3947.md) one ['ayil](../../strongs/h/h352.md); and ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) shall [camak](../../strongs/h/h5564.md) their [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of the ['ayil](../../strongs/h/h352.md).

<a name="exodus_29_16"></a>Exodus 29:16

And thou shalt [šāḥaṭ](../../strongs/h/h7819.md) the ['ayil](../../strongs/h/h352.md), and thou shalt [laqach](../../strongs/h/h3947.md) his [dam](../../strongs/h/h1818.md), and [zāraq](../../strongs/h/h2236.md) it [cabiyb](../../strongs/h/h5439.md) upon thea [mizbeach](../../strongs/h/h4196.md).

<a name="exodus_29_17"></a>Exodus 29:17

And thou shalt [nāṯaḥ](../../strongs/h/h5408.md) the ['ayil](../../strongs/h/h352.md) in [nēṯaḥ](../../strongs/h/h5409.md), and [rāḥaṣ](../../strongs/h/h7364.md) the [qereḇ](../../strongs/h/h7130.md) of him, and his [keraʿ](../../strongs/h/h3767.md), and [nathan](../../strongs/h/h5414.md) them unto his [nēṯaḥ](../../strongs/h/h5409.md), and unto his [ro'sh](../../strongs/h/h7218.md).

<a name="exodus_29_18"></a>Exodus 29:18

And thou shalt [qāṭar](../../strongs/h/h6999.md) the ['ayil](../../strongs/h/h352.md) upon thea [mizbeach](../../strongs/h/h4196.md): it is an [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md): it is a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md), an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_29_19"></a>Exodus 29:19

And thou shalt [laqach](../../strongs/h/h3947.md) the other ['ayil](../../strongs/h/h352.md); and ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) shall [camak](../../strongs/h/h5564.md) their [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of the ['ayil](../../strongs/h/h352.md).

<a name="exodus_29_20"></a>Exodus 29:20

Then shalt thou [šāḥaṭ](../../strongs/h/h7819.md) the ['ayil](../../strongs/h/h352.md), and [laqach](../../strongs/h/h3947.md) of his [dam](../../strongs/h/h1818.md), and [nathan](../../strongs/h/h5414.md) it upon the [tᵊnûḵ](../../strongs/h/h8571.md) of the ['ozen](../../strongs/h/h241.md) of ['Ahărôn](../../strongs/h/h175.md), and upon the [tᵊnûḵ](../../strongs/h/h8571.md) of the [yᵊmānî](../../strongs/h/h3233.md) ['ozen](../../strongs/h/h241.md) of his [ben](../../strongs/h/h1121.md), and upon the [bōhen](../../strongs/h/h931.md) of their [yᵊmānî](../../strongs/h/h3233.md) [yad](../../strongs/h/h3027.md), and upon the [bōhen](../../strongs/h/h931.md) of their [yᵊmānî](../../strongs/h/h3233.md) [regel](../../strongs/h/h7272.md), and [zāraq](../../strongs/h/h2236.md) the [dam](../../strongs/h/h1818.md) upon thea [mizbeach](../../strongs/h/h4196.md) [cabiyb](../../strongs/h/h5439.md).

<a name="exodus_29_21"></a>Exodus 29:21

And thou shalt [laqach](../../strongs/h/h3947.md) of the [dam](../../strongs/h/h1818.md) that is upon thea [mizbeach](../../strongs/h/h4196.md), and of the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and [nāzâ](../../strongs/h/h5137.md) it upon ['Ahărôn](../../strongs/h/h175.md), and upon his [beḡeḏ](../../strongs/h/h899.md), and upon his [ben](../../strongs/h/h1121.md), and upon the [beḡeḏ](../../strongs/h/h899.md) of his [ben](../../strongs/h/h1121.md) with him: and he shall be [qadash](../../strongs/h/h6942.md), and his [beḡeḏ](../../strongs/h/h899.md), and his [ben](../../strongs/h/h1121.md), and his [ben](../../strongs/h/h1121.md) [beḡeḏ](../../strongs/h/h899.md) with him.

<a name="exodus_29_22"></a>Exodus 29:22

Also thou shalt [laqach](../../strongs/h/h3947.md) of the ['ayil](../../strongs/h/h352.md) the [cheleb](../../strongs/h/h2459.md) and the ['alyâ](../../strongs/h/h451.md), and the [cheleb](../../strongs/h/h2459.md) that [kāsâ](../../strongs/h/h3680.md) the [qereḇ](../../strongs/h/h7130.md), and the [yōṯereṯ](../../strongs/h/h3508.md) above the [kāḇēḏ](../../strongs/h/h3516.md), and the two [kilyah](../../strongs/h/h3629.md), and the [cheleb](../../strongs/h/h2459.md) that is upon them, and the [yamiyn](../../strongs/h/h3225.md) [šôq](../../strongs/h/h7785.md); for it is an ['ayil](../../strongs/h/h352.md) of [millu'](../../strongs/h/h4394.md):

<a name="exodus_29_23"></a>Exodus 29:23

And one [kikār](../../strongs/h/h3603.md) of [lechem](../../strongs/h/h3899.md), and one [ḥallâ](../../strongs/h/h2471.md) of [šemen](../../strongs/h/h8081.md), and one [rāqîq](../../strongs/h/h7550.md) out of the [sal](../../strongs/h/h5536.md) of the [maṣṣâ](../../strongs/h/h4682.md) that is [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md):

<a name="exodus_29_24"></a>Exodus 29:24

And thou shalt [śûm](../../strongs/h/h7760.md) all in the [kaph](../../strongs/h/h3709.md) of ['Ahărôn](../../strongs/h/h175.md), and in the [kaph](../../strongs/h/h3709.md) of his [ben](../../strongs/h/h1121.md); and shalt [nûp̄](../../strongs/h/h5130.md) them for a [tᵊnûp̄â](../../strongs/h/h8573.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_29_25"></a>Exodus 29:25

And thou shalt [laqach](../../strongs/h/h3947.md) them of their [yad](../../strongs/h/h3027.md), and [qāṭar](../../strongs/h/h6999.md) them upon thea [mizbeach](../../strongs/h/h4196.md) for an [ʿōlâ](../../strongs/h/h5930.md), for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): it is an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_29_26"></a>Exodus 29:26

And thou shalt [laqach](../../strongs/h/h3947.md) the [ḥāzê](../../strongs/h/h2373.md) of the ['ayil](../../strongs/h/h352.md) of ['Ahărôn](../../strongs/h/h175.md) [millu'](../../strongs/h/h4394.md), and [nûp̄](../../strongs/h/h5130.md) it for a [tᵊnûp̄â](../../strongs/h/h8573.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): and it shall be thy [mānâ](../../strongs/h/h4490.md).

<a name="exodus_29_27"></a>Exodus 29:27

And thou shalt [qadash](../../strongs/h/h6942.md) the [ḥāzê](../../strongs/h/h2373.md) of the [tᵊnûp̄â](../../strongs/h/h8573.md), and the [šôq](../../strongs/h/h7785.md) of the [tᵊrûmâ](../../strongs/h/h8641.md), which is [nûp̄](../../strongs/h/h5130.md), and which is [ruwm](../../strongs/h/h7311.md), of the ['ayil](../../strongs/h/h352.md) of the [millu'](../../strongs/h/h4394.md), even of that which is for ['Ahărôn](../../strongs/h/h175.md), and of that which is for his [ben](../../strongs/h/h1121.md):

<a name="exodus_29_28"></a>Exodus 29:28

And it shall be ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) by a [choq](../../strongs/h/h2706.md) ['owlam](../../strongs/h/h5769.md) from the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): for it is a [tᵊrûmâ](../../strongs/h/h8641.md): and it shall be a [tᵊrûmâ](../../strongs/h/h8641.md) from the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) of the [zebach](../../strongs/h/h2077.md) of their [šelem](../../strongs/h/h8002.md), even their [tᵊrûmâ](../../strongs/h/h8641.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_29_29"></a>Exodus 29:29

And the [qodesh](../../strongs/h/h6944.md) [beḡeḏ](../../strongs/h/h899.md) of ['Ahărôn](../../strongs/h/h175.md) shall be his [ben](../../strongs/h/h1121.md) ['aḥar](../../strongs/h/h310.md) him, to be [māšḥâ](../../strongs/h/h4888.md) therein, and to be [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) in them.

<a name="exodus_29_30"></a>Exodus 29:30

And that [ben](../../strongs/h/h1121.md) that is [kōhēn](../../strongs/h/h3548.md) in his stead shall [labash](../../strongs/h/h3847.md) them on seven [yowm](../../strongs/h/h3117.md), when he [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) to [sharath](../../strongs/h/h8334.md) in the [qodesh](../../strongs/h/h6944.md).

<a name="exodus_29_31"></a>Exodus 29:31

And thou shalt [laqach](../../strongs/h/h3947.md) the ['ayil](../../strongs/h/h352.md) of the [millu'](../../strongs/h/h4394.md), and [bāšal](../../strongs/h/h1310.md) his [basar](../../strongs/h/h1320.md) in the [qadowsh](../../strongs/h/h6918.md) [maqowm](../../strongs/h/h4725.md).

<a name="exodus_29_32"></a>Exodus 29:32

And ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) shall ['akal](../../strongs/h/h398.md) the [basar](../../strongs/h/h1320.md) of the ['ayil](../../strongs/h/h352.md), and the [lechem](../../strongs/h/h3899.md) that is in the [sal](../../strongs/h/h5536.md) by the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="exodus_29_33"></a>Exodus 29:33

And they shall ['akal](../../strongs/h/h398.md) those things wherewith the [kāp̄ar](../../strongs/h/h3722.md), to [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) and to [qadash](../../strongs/h/h6942.md) them: but a [zûr](../../strongs/h/h2114.md) shall not ['akal](../../strongs/h/h398.md) thereof, because they are [qodesh](../../strongs/h/h6944.md).

<a name="exodus_29_34"></a>Exodus 29:34

And if ought of the [basar](../../strongs/h/h1320.md) of the [millu'](../../strongs/h/h4394.md), or of the [lechem](../../strongs/h/h3899.md), [yāṯar](../../strongs/h/h3498.md) unto the [boqer](../../strongs/h/h1242.md), then thou shalt [śārap̄](../../strongs/h/h8313.md) the [yāṯar](../../strongs/h/h3498.md) with ['esh](../../strongs/h/h784.md): it shall not be ['akal](../../strongs/h/h398.md), because it is [qodesh](../../strongs/h/h6944.md).

<a name="exodus_29_35"></a>Exodus 29:35

And thus shalt thou ['asah](../../strongs/h/h6213.md) unto ['Ahărôn](../../strongs/h/h175.md), and to his [ben](../../strongs/h/h1121.md), according to all things which I have [tsavah](../../strongs/h/h6680.md) thee: seven [yowm](../../strongs/h/h3117.md) shalt thou [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) them.

<a name="exodus_29_36"></a>Exodus 29:36

And thou shalt ['asah](../../strongs/h/h6213.md) every [yowm](../../strongs/h/h3117.md) a [par](../../strongs/h/h6499.md) for a [chatta'ath](../../strongs/h/h2403.md) for [kipur](../../strongs/h/h3725.md): and thou shalt [chata'](../../strongs/h/h2398.md) thea [mizbeach](../../strongs/h/h4196.md), when thou hast [kāp̄ar](../../strongs/h/h3722.md) for it, and thou shalt [māšaḥ](../../strongs/h/h4886.md) it, to [qadash](../../strongs/h/h6942.md) it.

<a name="exodus_29_37"></a>Exodus 29:37

Seven [yowm](../../strongs/h/h3117.md) thou shalt [kāp̄ar](../../strongs/h/h3722.md) for thea [mizbeach](../../strongs/h/h4196.md), and [qadash](../../strongs/h/h6942.md) it; and it shall be a [mizbeach](../../strongs/h/h4196.md) [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md): whatsoever [naga'](../../strongs/h/h5060.md) thea [mizbeach](../../strongs/h/h4196.md) shall be [qadash](../../strongs/h/h6942.md).

<a name="exodus_29_38"></a>Exodus 29:38

Now this is that which thou shalt ['asah](../../strongs/h/h6213.md) upon thea [mizbeach](../../strongs/h/h4196.md); two [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) [yowm](../../strongs/h/h3117.md) by [yowm](../../strongs/h/h3117.md) [tāmîḏ](../../strongs/h/h8548.md).

<a name="exodus_29_39"></a>Exodus 29:39

The one [keḇeś](../../strongs/h/h3532.md) thou shalt ['asah](../../strongs/h/h6213.md) in the [boqer](../../strongs/h/h1242.md); and the other [keḇeś](../../strongs/h/h3532.md) thou shalt ['asah](../../strongs/h/h6213.md) at ['ereb](../../strongs/h/h6153.md):

<a name="exodus_29_40"></a>Exodus 29:40

And with the one [keḇeś](../../strongs/h/h3532.md) a [ʿiśśārôn](../../strongs/h/h6241.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with the fourth part of an [hîn](../../strongs/h/h1969.md) of [kāṯîṯ](../../strongs/h/h3795.md) [šemen](../../strongs/h/h8081.md); and the fourth part of an [hîn](../../strongs/h/h1969.md) of [yayin](../../strongs/h/h3196.md) for a [necek](../../strongs/h/h5262.md).

<a name="exodus_29_41"></a>Exodus 29:41

And the other [keḇeś](../../strongs/h/h3532.md) thou shalt ['asah](../../strongs/h/h6213.md) at ['ereb](../../strongs/h/h6153.md), and shalt ['asah](../../strongs/h/h6213.md) thereto according to the [minchah](../../strongs/h/h4503.md) of the [boqer](../../strongs/h/h1242.md), and according to the [necek](../../strongs/h/h5262.md) thereof, for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md), an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_29_42"></a>Exodus 29:42

This shall be a [tāmîḏ](../../strongs/h/h8548.md) [ʿōlâ](../../strongs/h/h5930.md) throughout your [dôr](../../strongs/h/h1755.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): where I will [yāʿaḏ](../../strongs/h/h3259.md) you, to [dabar](../../strongs/h/h1696.md) there unto thee.

<a name="exodus_29_43"></a>Exodus 29:43

And there I will [yāʿaḏ](../../strongs/h/h3259.md) with the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and the tabernacle shall be [qadash](../../strongs/h/h6942.md) by my [kabowd](../../strongs/h/h3519.md).

<a name="exodus_29_44"></a>Exodus 29:44

And I will [qadash](../../strongs/h/h6942.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and thea [mizbeach](../../strongs/h/h4196.md): I will [qadash](../../strongs/h/h6942.md) also both ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md), to [kāhan](../../strongs/h/h3547.md).

<a name="exodus_29_45"></a>Exodus 29:45

And I will [shakan](../../strongs/h/h7931.md) [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and will be their ['Elohiym](../../strongs/h/h430.md).

<a name="exodus_29_46"></a>Exodus 29:46

And they shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), that [yāṣā'](../../strongs/h/h3318.md) them out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), that I may [shakan](../../strongs/h/h7931.md) [tavek](../../strongs/h/h8432.md) them: I am [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 28](exodus_28.md) - [Exodus 30](exodus_30.md)