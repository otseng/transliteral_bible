# [Exodus 22](https://www.blueletterbible.org/kjv/exo/22/1/)

<a name="exodus_22_1"></a>Exodus 22:1

If an ['iysh](../../strongs/h/h376.md) shall [ganab](../../strongs/h/h1589.md) a [showr](../../strongs/h/h7794.md), or a [śê](../../strongs/h/h7716.md), and [ṭāḇaḥ](../../strongs/h/h2873.md) it, or [māḵar](../../strongs/h/h4376.md) it; he shall [shalam](../../strongs/h/h7999.md) five [bāqār](../../strongs/h/h1241.md) for a [showr](../../strongs/h/h7794.md), and four [tso'n](../../strongs/h/h6629.md) for a [śê](../../strongs/h/h7716.md).

<a name="exodus_22_2"></a>Exodus 22:2

If a [gannāḇ](../../strongs/h/h1590.md) be [māṣā'](../../strongs/h/h4672.md) [maḥtereṯ](../../strongs/h/h4290.md), and be [nakah](../../strongs/h/h5221.md) that he [muwth](../../strongs/h/h4191.md), there shall no [dam](../../strongs/h/h1818.md) be shed for him.

<a name="exodus_22_3"></a>Exodus 22:3

If the [šemeš](../../strongs/h/h8121.md) be [zāraḥ](../../strongs/h/h2224.md) upon him, there shall be [dam](../../strongs/h/h1818.md) shed for him; for he should make [shalam](../../strongs/h/h7999.md) [shalam](../../strongs/h/h7999.md); if he have nothing, then he shall be [māḵar](../../strongs/h/h4376.md) for his [gᵊnēḇâ](../../strongs/h/h1591.md).

<a name="exodus_22_4"></a>Exodus 22:4

If the [gᵊnēḇâ](../../strongs/h/h1591.md) be [māṣā'](../../strongs/h/h4672.md) [māṣā'](../../strongs/h/h4672.md) in his [yad](../../strongs/h/h3027.md) [chay](../../strongs/h/h2416.md), whether it be [showr](../../strongs/h/h7794.md), or [chamowr](../../strongs/h/h2543.md), or [śê](../../strongs/h/h7716.md); he shall [shalam](../../strongs/h/h7999.md) [šᵊnayim](../../strongs/h/h8147.md).

<a name="exodus_22_5"></a>Exodus 22:5

If an ['iysh](../../strongs/h/h376.md) shall cause a [sadeh](../../strongs/h/h7704.md) or [kerem](../../strongs/h/h3754.md) to be [bāʿar](../../strongs/h/h1197.md), and shall [shalach](../../strongs/h/h7971.md) in his [bᵊʿîr](../../strongs/h/h1165.md), and shall [bāʿar](../../strongs/h/h1197.md) in ['aḥēr](../../strongs/h/h312.md) [sadeh](../../strongs/h/h7704.md); of the [mêṭāḇ](../../strongs/h/h4315.md) of his own [sadeh](../../strongs/h/h7704.md), and of the [mêṭāḇ](../../strongs/h/h4315.md) of his own [kerem](../../strongs/h/h3754.md), shall he make [shalam](../../strongs/h/h7999.md).

<a name="exodus_22_6"></a>Exodus 22:6

If ['esh](../../strongs/h/h784.md) [yāṣā'](../../strongs/h/h3318.md), and [māṣā'](../../strongs/h/h4672.md) in [qowts](../../strongs/h/h6975.md), so that the [gāḏîš](../../strongs/h/h1430.md), or the [qāmâ](../../strongs/h/h7054.md), or the [sadeh](../../strongs/h/h7704.md), be ['akal](../../strongs/h/h398.md) therewith; he that [bāʿar](../../strongs/h/h1197.md) the [bᵊʿērâ](../../strongs/h/h1200.md) shall [shalam](../../strongs/h/h7999.md) [shalam](../../strongs/h/h7999.md).

<a name="exodus_22_7"></a>Exodus 22:7

If an ['iysh](../../strongs/h/h376.md) shall [nathan](../../strongs/h/h5414.md) unto his [rea'](../../strongs/h/h7453.md) [keceph](../../strongs/h/h3701.md) or [kĕliy](../../strongs/h/h3627.md) to [shamar](../../strongs/h/h8104.md), and it be [ganab](../../strongs/h/h1589.md) out of the ['iysh](../../strongs/h/h376.md) [bayith](../../strongs/h/h1004.md); if the [gannāḇ](../../strongs/h/h1590.md) be [māṣā'](../../strongs/h/h4672.md), let him [shalam](../../strongs/h/h7999.md) [šᵊnayim](../../strongs/h/h8147.md).

<a name="exodus_22_8"></a>Exodus 22:8

If the [gannāḇ](../../strongs/h/h1590.md) be not [māṣā'](../../strongs/h/h4672.md), then the [baʿal](../../strongs/h/h1167.md) of the [bayith](../../strongs/h/h1004.md) shall be [qāraḇ](../../strongs/h/h7126.md) unto the ['Elohiym](../../strongs/h/h430.md), to see whether he have [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) unto his [rea'](../../strongs/h/h7453.md) [mĕla'kah](../../strongs/h/h4399.md).

<a name="exodus_22_9"></a>Exodus 22:9

For all [dabar](../../strongs/h/h1697.md) of [pesha'](../../strongs/h/h6588.md), whether it be for [showr](../../strongs/h/h7794.md), for [chamowr](../../strongs/h/h2543.md), for [śê](../../strongs/h/h7716.md), for [śalmâ](../../strongs/h/h8008.md), or for any ['ăḇēḏâ](../../strongs/h/h9.md) which another ['āmar](../../strongs/h/h559.md) to be his, the [dabar](../../strongs/h/h1697.md) of [šᵊnayim](../../strongs/h/h8147.md) shall [bow'](../../strongs/h/h935.md) before the ['Elohiym](../../strongs/h/h430.md); and whom the ['Elohiym](../../strongs/h/h430.md) shall [rāšaʿ](../../strongs/h/h7561.md), he shall [shalam](../../strongs/h/h7999.md) [šᵊnayim](../../strongs/h/h8147.md) unto his [rea'](../../strongs/h/h7453.md).

<a name="exodus_22_10"></a>Exodus 22:10

If an ['iysh](../../strongs/h/h376.md) [nathan](../../strongs/h/h5414.md) unto his [rea'](../../strongs/h/h7453.md) a [chamowr](../../strongs/h/h2543.md), or a [showr](../../strongs/h/h7794.md), or a [śê](../../strongs/h/h7716.md), or any [bĕhemah](../../strongs/h/h929.md), to [shamar](../../strongs/h/h8104.md); and it [muwth](../../strongs/h/h4191.md), or be [shabar](../../strongs/h/h7665.md), or [šāḇâ](../../strongs/h/h7617.md), no man [ra'ah](../../strongs/h/h7200.md) it:

<a name="exodus_22_11"></a>Exodus 22:11

Then shall a [šᵊḇûʿâ](../../strongs/h/h7621.md) of [Yĕhovah](../../strongs/h/h3068.md) be between them [šᵊnayim](../../strongs/h/h8147.md), that he hath not [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) unto his [rea'](../../strongs/h/h7453.md) [mĕla'kah](../../strongs/h/h4399.md); and the [baʿal](../../strongs/h/h1167.md) of it shall [laqach](../../strongs/h/h3947.md) thereof, and he shall not make it [shalam](../../strongs/h/h7999.md).

<a name="exodus_22_12"></a>Exodus 22:12

And if it be [ganab](../../strongs/h/h1589.md) from him, he shall [shalam](../../strongs/h/h7999.md) unto the [baʿal](../../strongs/h/h1167.md) thereof.

<a name="exodus_22_13"></a>Exodus 22:13

If it be [taraph](../../strongs/h/h2963.md), then let him [bow'](../../strongs/h/h935.md) it for ['ed](../../strongs/h/h5707.md), and he shall not [shalam](../../strongs/h/h7999.md) that which was [ṭᵊrēp̄â](../../strongs/h/h2966.md).

<a name="exodus_22_14"></a>Exodus 22:14

And if an ['iysh](../../strongs/h/h376.md) [sha'al](../../strongs/h/h7592.md) ought of his [rea'](../../strongs/h/h7453.md), and it be [shabar](../../strongs/h/h7665.md), or [muwth](../../strongs/h/h4191.md), the [baʿal](../../strongs/h/h1167.md) thereof being not with it, he shall [shalam](../../strongs/h/h7999.md) [shalam](../../strongs/h/h7999.md).

<a name="exodus_22_15"></a>Exodus 22:15

But if the [baʿal](../../strongs/h/h1167.md) thereof be with it, he shall not [shalam](../../strongs/h/h7999.md): if it be a [śāḵîr](../../strongs/h/h7916.md) thing, it [bow'](../../strongs/h/h935.md) for his [śāḵār](../../strongs/h/h7939.md).

<a name="exodus_22_16"></a>Exodus 22:16

And if an ['iysh](../../strongs/h/h376.md) [pāṯâ](../../strongs/h/h6601.md) a [bᵊṯûlâ](../../strongs/h/h1330.md) that is not ['āraś](../../strongs/h/h781.md), and [shakab](../../strongs/h/h7901.md) with her, he shall [māhar](../../strongs/h/h4117.md) [māhar](../../strongs/h/h4117.md) her to be his ['ishshah](../../strongs/h/h802.md).

<a name="exodus_22_17"></a>Exodus 22:17

If her ['ab](../../strongs/h/h1.md) [mā'ēn](../../strongs/h/h3985.md) [mā'ēn](../../strongs/h/h3985.md) to [nathan](../../strongs/h/h5414.md) her unto him, he shall [šāqal](../../strongs/h/h8254.md) [keceph](../../strongs/h/h3701.md) according to the [mōhar](../../strongs/h/h4119.md) of [bᵊṯûlâ](../../strongs/h/h1330.md).

<a name="exodus_22_18"></a>Exodus 22:18

Thou shalt not suffer a [kāšap̄](../../strongs/h/h3784.md) to [ḥāyâ](../../strongs/h/h2421.md).

<a name="exodus_22_19"></a>Exodus 22:19

Whosoever [shakab](../../strongs/h/h7901.md) with a [bĕhemah](../../strongs/h/h929.md) shall [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="exodus_22_20"></a>Exodus 22:20

He that [zabach](../../strongs/h/h2076.md) unto any ['Elohiym](../../strongs/h/h430.md), [biltî](../../strongs/h/h1115.md) unto [Yĕhovah](../../strongs/h/h3068.md) only, he shall be [ḥāram](../../strongs/h/h2763.md).

<a name="exodus_22_21"></a>Exodus 22:21

Thou shalt neither [yānâ](../../strongs/h/h3238.md) a [ger](../../strongs/h/h1616.md), nor [lāḥaṣ](../../strongs/h/h3905.md) him: for ye were [ger](../../strongs/h/h1616.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_22_22"></a>Exodus 22:22

Ye shall not [ʿānâ](../../strongs/h/h6031.md) any ['almānâ](../../strongs/h/h490.md), or [yathowm](../../strongs/h/h3490.md).

<a name="exodus_22_23"></a>Exodus 22:23

If thou [ʿānâ](../../strongs/h/h6031.md) them in any [ʿānâ](../../strongs/h/h6031.md), and they [ṣāʿaq](../../strongs/h/h6817.md) [ṣāʿaq](../../strongs/h/h6817.md) unto me, I will [shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) their [tsa'aqah](../../strongs/h/h6818.md);

<a name="exodus_22_24"></a>Exodus 22:24

And my ['aph](../../strongs/h/h639.md) shall [ḥārâ](../../strongs/h/h2734.md), and I will [harag](../../strongs/h/h2026.md) you with the [chereb](../../strongs/h/h2719.md); and your ['ishshah](../../strongs/h/h802.md) shall be ['almānâ](../../strongs/h/h490.md), and your [ben](../../strongs/h/h1121.md) [yathowm](../../strongs/h/h3490.md).

<a name="exodus_22_25"></a>Exodus 22:25

If thou [lāvâ](../../strongs/h/h3867.md) [keceph](../../strongs/h/h3701.md) to any of my ['am](../../strongs/h/h5971.md) that is ['aniy](../../strongs/h/h6041.md) by thee, thou shalt not be to him as a [nāšâ](../../strongs/h/h5383.md), neither shalt thou [śûm](../../strongs/h/h7760.md) upon him [neshek](../../strongs/h/h5392.md).

<a name="exodus_22_26"></a>Exodus 22:26

If thou at all take thy [rea'](../../strongs/h/h7453.md) [śalmâ](../../strongs/h/h8008.md) to [chabal](../../strongs/h/h2254.md), thou shalt [shuwb](../../strongs/h/h7725.md) it unto him by that the [šemeš](../../strongs/h/h8121.md) [bow'](../../strongs/h/h935.md):

<a name="exodus_22_27"></a>Exodus 22:27

For that is his [kᵊsûṯ](../../strongs/h/h3682.md) only, it is his [śimlâ](../../strongs/h/h8071.md) for his ['owr](../../strongs/h/h5785.md): wherein shall he [shakab](../../strongs/h/h7901.md)? and it shall come to pass, when he [ṣāʿaq](../../strongs/h/h6817.md) unto me, that I will [shama'](../../strongs/h/h8085.md); for I am [ḥanwn](../../strongs/h/h2587.md).

<a name="exodus_22_28"></a>Exodus 22:28

Thou shalt not [qālal](../../strongs/h/h7043.md) the ['Elohiym](../../strongs/h/h430.md), nor ['arar](../../strongs/h/h779.md) the [nāśî'](../../strongs/h/h5387.md) of thy ['am](../../strongs/h/h5971.md).

<a name="exodus_22_29"></a>Exodus 22:29

Thou shalt not ['āḥar](../../strongs/h/h309.md) to offer the first of thy [mᵊlē'â](../../strongs/h/h4395.md), and of thy [demaʿ](../../strongs/h/h1831.md): the [bᵊḵôr](../../strongs/h/h1060.md) of thy [ben](../../strongs/h/h1121.md) shalt thou [nathan](../../strongs/h/h5414.md) unto me.

<a name="exodus_22_30"></a>Exodus 22:30

Likewise shalt thou ['asah](../../strongs/h/h6213.md) with thine [showr](../../strongs/h/h7794.md), and with thy [tso'n](../../strongs/h/h6629.md): seven [yowm](../../strongs/h/h3117.md) it shall be with his ['em](../../strongs/h/h517.md); on the eighth [yowm](../../strongs/h/h3117.md) thou shalt [nathan](../../strongs/h/h5414.md) it me.

<a name="exodus_22_31"></a>Exodus 22:31

And ye shall be [qodesh](../../strongs/h/h6944.md) ['enowsh](../../strongs/h/h582.md) unto me: neither shall ye ['akal](../../strongs/h/h398.md) any [basar](../../strongs/h/h1320.md) that is [ṭᵊrēp̄â](../../strongs/h/h2966.md) in the [sadeh](../../strongs/h/h7704.md); ye shall [shalak](../../strongs/h/h7993.md) it to the [keleḇ](../../strongs/h/h3611.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 21](exodus_21.md) - [Exodus 23](exodus_23.md)