# [Exodus 12](https://www.blueletterbible.org/kjv/exo/12/1/s_62001)

<a name="exodus_12_1"></a>Exodus 12:1

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) ['āmar](../../strongs/h/h559.md),

<a name="exodus_12_2"></a>Exodus 12:2

This [ḥōḏeš](../../strongs/h/h2320.md) shall be unto you the [ro'sh](../../strongs/h/h7218.md) of [ḥōḏeš](../../strongs/h/h2320.md): it shall be the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md) of the [šānâ](../../strongs/h/h8141.md) to you.

<a name="exodus_12_3"></a>Exodus 12:3

[dabar](../../strongs/h/h1696.md) ye unto all the ['edah](../../strongs/h/h5712.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), In the [ʿāśôr](../../strongs/h/h6218.md) of this [ḥōḏeš](../../strongs/h/h2320.md) they shall [laqach](../../strongs/h/h3947.md) to them every ['iysh](../../strongs/h/h376.md) a [śê](../../strongs/h/h7716.md), according to the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), a [śê](../../strongs/h/h7716.md) for a [bayith](../../strongs/h/h1004.md):

<a name="exodus_12_4"></a>Exodus 12:4

And if the [bayith](../../strongs/h/h1004.md) be too [māʿaṭ](../../strongs/h/h4591.md) for the [śê](../../strongs/h/h7716.md), let him and his [šāḵēn](../../strongs/h/h7934.md) [qarowb](../../strongs/h/h7138.md) unto his [bayith](../../strongs/h/h1004.md) [laqach](../../strongs/h/h3947.md) it according to the [miḵsâ](../../strongs/h/h4373.md) of the [nephesh](../../strongs/h/h5315.md); every ['iysh](../../strongs/h/h376.md) [peh](../../strongs/h/h6310.md) to his ['ōḵel](../../strongs/h/h400.md) shall make your [kāsas](../../strongs/h/h3699.md) for the [śê](../../strongs/h/h7716.md).

<a name="exodus_12_5"></a>Exodus 12:5

Your [śê](../../strongs/h/h7716.md) shall be [tamiym](../../strongs/h/h8549.md), a [zāḵār](../../strongs/h/h2145.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md): ye shall [laqach](../../strongs/h/h3947.md) it out from the [keḇeś](../../strongs/h/h3532.md), or from the [ʿēz](../../strongs/h/h5795.md):

<a name="exodus_12_6"></a>Exodus 12:6

And ye shall [mišmereṯ](../../strongs/h/h4931.md) it up until the fourteenth [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md): and the [kōl](../../strongs/h/h3605.md) [qāhēl](../../strongs/h/h6951.md) of the ['edah](../../strongs/h/h5712.md) of [Yisra'el](../../strongs/h/h3478.md) shall [šāḥaṭ](../../strongs/h/h7819.md) it in the ['ereb](../../strongs/h/h6153.md).

<a name="exodus_12_7"></a>Exodus 12:7

And they shall [laqach](../../strongs/h/h3947.md) of the [dam](../../strongs/h/h1818.md), and [nathan](../../strongs/h/h5414.md) it on the [šᵊnayim](../../strongs/h/h8147.md) [mᵊzûzâ](../../strongs/h/h4201.md) and on the [mašqôp̄](../../strongs/h/h4947.md) of the [bayith](../../strongs/h/h1004.md), wherein they shall ['akal](../../strongs/h/h398.md) it.

<a name="exodus_12_8"></a>Exodus 12:8

And they shall ['akal](../../strongs/h/h398.md) the [basar](../../strongs/h/h1320.md) in that [layil](../../strongs/h/h3915.md), [ṣālî](../../strongs/h/h6748.md) with ['esh](../../strongs/h/h784.md), and [maṣṣâ](../../strongs/h/h4682.md); and with [merōr](../../strongs/h/h4844.md) they shall ['akal](../../strongs/h/h398.md) it.

<a name="exodus_12_9"></a>Exodus 12:9

['akal](../../strongs/h/h398.md) not of it [nā'](../../strongs/h/h4995.md), nor [bāšal](../../strongs/h/h1310.md) [bāšēl](../../strongs/h/h1311.md) at all with [mayim](../../strongs/h/h4325.md), but [ṣālî](../../strongs/h/h6748.md) with ['esh](../../strongs/h/h784.md); his [ro'sh](../../strongs/h/h7218.md) with his [keraʿ](../../strongs/h/h3767.md), and with the [qereḇ](../../strongs/h/h7130.md) thereof.

<a name="exodus_12_10"></a>Exodus 12:10

And ye shall let nothing of it [yāṯar](../../strongs/h/h3498.md) until the [boqer](../../strongs/h/h1242.md); and that which [yāṯar](../../strongs/h/h3498.md) of it until the [boqer](../../strongs/h/h1242.md) ye shall [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md).

<a name="exodus_12_11"></a>Exodus 12:11

And [kāḵâ](../../strongs/h/h3602.md) shall ye ['akal](../../strongs/h/h398.md) it; with your [māṯnayim](../../strongs/h/h4975.md) [ḥāḡar](../../strongs/h/h2296.md), your [naʿal](../../strongs/h/h5275.md) on your [regel](../../strongs/h/h7272.md), and your [maqqēl](../../strongs/h/h4731.md) in your [yad](../../strongs/h/h3027.md); and ye shall ['akal](../../strongs/h/h398.md) it in [ḥipāzôn](../../strongs/h/h2649.md): it is [Yĕhovah](../../strongs/h/h3068.md) [pecach](../../strongs/h/h6453.md).

<a name="exodus_12_12"></a>Exodus 12:12

For I will ['abar](../../strongs/h/h5674.md) through the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) this [layil](../../strongs/h/h3915.md), and will [nakah](../../strongs/h/h5221.md) all the [bᵊḵôr](../../strongs/h/h1060.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), both ['adam](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md); and against all the ['Elohiym](../../strongs/h/h430.md) of [Mitsrayim](../../strongs/h/h4714.md) I will ['asah](../../strongs/h/h6213.md) [šep̄eṭ](../../strongs/h/h8201.md): I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_12_13"></a>Exodus 12:13

And the [dam](../../strongs/h/h1818.md) shall be to you for an ['ôṯ](../../strongs/h/h226.md) upon the [bayith](../../strongs/h/h1004.md) where ye are: and when I [ra'ah](../../strongs/h/h7200.md) the [dam](../../strongs/h/h1818.md), I will [pāsaḥ](../../strongs/h/h6452.md) you, and the [neḡep̄](../../strongs/h/h5063.md) shall not be upon you to [mašḥîṯ](../../strongs/h/h4889.md) you, when I [nakah](../../strongs/h/h5221.md) the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_12_14"></a>Exodus 12:14

And this [yowm](../../strongs/h/h3117.md) shall be unto you for a [zikārôn](../../strongs/h/h2146.md); and ye shall [ḥāḡaḡ](../../strongs/h/h2287.md) it a [ḥāḡ](../../strongs/h/h2282.md) to [Yĕhovah](../../strongs/h/h3068.md) throughout your [dôr](../../strongs/h/h1755.md); ye shall keep it a [ḥāḡaḡ](../../strongs/h/h2287.md) by a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md).

<a name="exodus_12_15"></a>Exodus 12:15

[šeḇaʿ](../../strongs/h/h7651.md) [yowm](../../strongs/h/h3117.md) shall ye ['akal](../../strongs/h/h398.md) [maṣṣâ](../../strongs/h/h4682.md); even the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) ye shall [shabath](../../strongs/h/h7673.md) [śᵊ'ōr](../../strongs/h/h7603.md) out of your [bayith](../../strongs/h/h1004.md): for whosoever ['akal](../../strongs/h/h398.md) [ḥāmēṣ](../../strongs/h/h2557.md) from the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) until the [šᵊḇîʿî](../../strongs/h/h7637.md) [yowm](../../strongs/h/h3117.md), that [nephesh](../../strongs/h/h5315.md) shall be [karath](../../strongs/h/h3772.md) from [Yisra'el](../../strongs/h/h3478.md).

<a name="exodus_12_16"></a>Exodus 12:16

And in the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) there shall be a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md), and in the [šᵊḇîʿî](../../strongs/h/h7637.md) [yowm](../../strongs/h/h3117.md) there shall be a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md) to you; no manner of [mĕla'kah](../../strongs/h/h4399.md) shall be ['asah](../../strongs/h/h6213.md) in them, ['aḵ](../../strongs/h/h389.md) that which every [nephesh](../../strongs/h/h5315.md) must ['akal](../../strongs/h/h398.md), that only may be ['asah](../../strongs/h/h6213.md) of you.

<a name="exodus_12_17"></a>Exodus 12:17

And ye shall [shamar](../../strongs/h/h8104.md) the feast of [maṣṣâ](../../strongs/h/h4682.md); for in this ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md) have I [yāṣā'](../../strongs/h/h3318.md) your [tsaba'](../../strongs/h/h6635.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): therefore shall ye [shamar](../../strongs/h/h8104.md) this [yowm](../../strongs/h/h3117.md) in your [dôr](../../strongs/h/h1755.md) by a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md).

<a name="exodus_12_18"></a>Exodus 12:18

In the [ri'šôn](../../strongs/h/h7223.md) month, on the [ʿeśer](../../strongs/h/h6240.md) ['arbaʿ](../../strongs/h/h702.md) [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md) at ['ereb](../../strongs/h/h6153.md), ye shall ['akal](../../strongs/h/h398.md) [maṣṣâ](../../strongs/h/h4682.md), until the ['echad](../../strongs/h/h259.md) and [ʿeśrîm](../../strongs/h/h6242.md) [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md) at ['ereb](../../strongs/h/h6153.md).

<a name="exodus_12_19"></a>Exodus 12:19

[šeḇaʿ](../../strongs/h/h7651.md) [yowm](../../strongs/h/h3117.md) shall there be no [śᵊ'ōr](../../strongs/h/h7603.md) [māṣā'](../../strongs/h/h4672.md) in your [bayith](../../strongs/h/h1004.md): for whosoever ['akal](../../strongs/h/h398.md) that which is [ḥāmēṣ](../../strongs/h/h2556.md), even that [nephesh](../../strongs/h/h5315.md) shall be [karath](../../strongs/h/h3772.md) from the ['edah](../../strongs/h/h5712.md) of [Yisra'el](../../strongs/h/h3478.md), whether he be a [ger](../../strongs/h/h1616.md), or ['ezrāḥ](../../strongs/h/h249.md) in the ['erets](../../strongs/h/h776.md).

<a name="exodus_12_20"></a>Exodus 12:20

Ye shall ['akal](../../strongs/h/h398.md) nothing [ḥāmēṣ](../../strongs/h/h2556.md); in all your [môšāḇ](../../strongs/h/h4186.md) shall ye ['akal](../../strongs/h/h398.md) [maṣṣâ](../../strongs/h/h4682.md).

<a name="exodus_12_21"></a>Exodus 12:21

Then [Mōshe](../../strongs/h/h4872.md) [qara'](../../strongs/h/h7121.md) for all the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, [mashak](../../strongs/h/h4900.md) and [laqach](../../strongs/h/h3947.md) you a [tso'n](../../strongs/h/h6629.md) according to your [mišpāḥâ](../../strongs/h/h4940.md), and [šāḥaṭ](../../strongs/h/h7819.md) the [pecach](../../strongs/h/h6453.md).

<a name="exodus_12_22"></a>Exodus 12:22

And ye shall [laqach](../../strongs/h/h3947.md) a ['ăḡudâ](../../strongs/h/h92.md) of ['ēzôḇ](../../strongs/h/h231.md), and [ṭāḇal](../../strongs/h/h2881.md) it in the [dam](../../strongs/h/h1818.md) that is in the [caph](../../strongs/h/h5592.md), and [naga'](../../strongs/h/h5060.md) the [mašqôp̄](../../strongs/h/h4947.md) and the [šᵊnayim](../../strongs/h/h8147.md) [mᵊzûzâ](../../strongs/h/h4201.md) with the [dam](../../strongs/h/h1818.md) that is in the [caph](../../strongs/h/h5592.md); and ['iysh](../../strongs/h/h376.md) of you shall [yāṣā'](../../strongs/h/h3318.md) at the [peṯaḥ](../../strongs/h/h6607.md) of his [bayith](../../strongs/h/h1004.md) until the [boqer](../../strongs/h/h1242.md).

<a name="exodus_12_23"></a>Exodus 12:23

For [Yĕhovah](../../strongs/h/h3068.md) will ['abar](../../strongs/h/h5674.md) to [nāḡap̄](../../strongs/h/h5062.md) the [Mitsrayim](../../strongs/h/h4714.md); and when he [ra'ah](../../strongs/h/h7200.md) the [dam](../../strongs/h/h1818.md) upon the [mašqôp̄](../../strongs/h/h4947.md), and on the [šᵊnayim](../../strongs/h/h8147.md) [mᵊzûzâ](../../strongs/h/h4201.md), [Yĕhovah](../../strongs/h/h3068.md) will [pāsaḥ](../../strongs/h/h6452.md) the [peṯaḥ](../../strongs/h/h6607.md), and will not [nathan](../../strongs/h/h5414.md) the [shachath](../../strongs/h/h7843.md) to [bow'](../../strongs/h/h935.md) unto your [bayith](../../strongs/h/h1004.md) to [nāḡap̄](../../strongs/h/h5062.md) you.

<a name="exodus_12_24"></a>Exodus 12:24

And ye shall [shamar](../../strongs/h/h8104.md) this [dabar](../../strongs/h/h1697.md) for a [choq](../../strongs/h/h2706.md) to thee and to thy [ben](../../strongs/h/h1121.md) for ['owlam](../../strongs/h/h5769.md).

<a name="exodus_12_25"></a>Exodus 12:25

And it shall come to pass, when ye be [bow'](../../strongs/h/h935.md) to the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) will [nathan](../../strongs/h/h5414.md) you, according as he hath [dabar](../../strongs/h/h1696.md), that ye shall [shamar](../../strongs/h/h8104.md) this [ʿăḇōḏâ](../../strongs/h/h5656.md).

<a name="exodus_12_26"></a>Exodus 12:26

And it shall come to pass, when your [ben](../../strongs/h/h1121.md) shall ['āmar](../../strongs/h/h559.md) unto you, What mean ye by this [ʿăḇōḏâ](../../strongs/h/h5656.md)?

<a name="exodus_12_27"></a>Exodus 12:27

That ye shall ['āmar](../../strongs/h/h559.md), It is the [zebach](../../strongs/h/h2077.md) of [Yĕhovah](../../strongs/h/h3068.md) [pecach](../../strongs/h/h6453.md), who [pāsaḥ](../../strongs/h/h6452.md) the [bayith](../../strongs/h/h1004.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) in [Mitsrayim](../../strongs/h/h4714.md), when he [nāḡap̄](../../strongs/h/h5062.md) the [Mitsrayim](../../strongs/h/h4714.md), and [natsal](../../strongs/h/h5337.md) our [bayith](../../strongs/h/h1004.md). And the ['am](../../strongs/h/h5971.md) [qāḏaḏ](../../strongs/h/h6915.md) and [shachah](../../strongs/h/h7812.md).

<a name="exodus_12_28"></a>Exodus 12:28

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yālaḵ](../../strongs/h/h3212.md), and ['asah](../../strongs/h/h6213.md) as [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md), so ['asah](../../strongs/h/h6213.md) they.

<a name="exodus_12_29"></a>Exodus 12:29

And it came to pass, that at [ḥēṣî](../../strongs/h/h2677.md) [layil](../../strongs/h/h3915.md) [Yĕhovah](../../strongs/h/h3068.md) [nakah](../../strongs/h/h5221.md) all the [bᵊḵôr](../../strongs/h/h1060.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), from the [bᵊḵôr](../../strongs/h/h1060.md) of [Parʿô](../../strongs/h/h6547.md) that [yashab](../../strongs/h/h3427.md) on his [kicce'](../../strongs/h/h3678.md) unto the [bᵊḵôr](../../strongs/h/h1060.md) of the [šᵊḇî](../../strongs/h/h7628.md) that was in the [bayith](../../strongs/h/h1004.md) [bowr](../../strongs/h/h953.md); and all the [bᵊḵôr](../../strongs/h/h1060.md) of [bĕhemah](../../strongs/h/h929.md).

<a name="exodus_12_30"></a>Exodus 12:30

And [Parʿô](../../strongs/h/h6547.md) [quwm](../../strongs/h/h6965.md) in the [layil](../../strongs/h/h3915.md), he, and all his ['ebed](../../strongs/h/h5650.md), and all the [Mitsrayim](../../strongs/h/h4714.md); and there was a [gadowl](../../strongs/h/h1419.md) [tsa'aqah](../../strongs/h/h6818.md) in [Mitsrayim](../../strongs/h/h4714.md); for there was not a [bayith](../../strongs/h/h1004.md) where there was not one [muwth](../../strongs/h/h4191.md).

<a name="exodus_12_31"></a>Exodus 12:31

And he [qara'](../../strongs/h/h7121.md) for [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) by [layil](../../strongs/h/h3915.md), and ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md), and [yāṣā'](../../strongs/h/h3318.md) you forth from [tavek](../../strongs/h/h8432.md) my ['am](../../strongs/h/h5971.md), both ye and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); and [yālaḵ](../../strongs/h/h3212.md), ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md), as ye have [dabar](../../strongs/h/h1696.md).

<a name="exodus_12_32"></a>Exodus 12:32

Also [laqach](../../strongs/h/h3947.md) your [tso'n](../../strongs/h/h6629.md) and your [bāqār](../../strongs/h/h1241.md), as ye have [dabar](../../strongs/h/h1696.md), and [yālaḵ](../../strongs/h/h3212.md); and [barak](../../strongs/h/h1288.md) me also.

<a name="exodus_12_33"></a>Exodus 12:33

And the [Mitsrayim](../../strongs/h/h4714.md) were [ḥāzaq](../../strongs/h/h2388.md) upon the ['am](../../strongs/h/h5971.md), that they might [shalach](../../strongs/h/h7971.md) them out of the ['erets](../../strongs/h/h776.md) in [māhar](../../strongs/h/h4116.md); for they ['āmar](../../strongs/h/h559.md), We be all [muwth](../../strongs/h/h4191.md) men.

<a name="exodus_12_34"></a>Exodus 12:34

And the ['am](../../strongs/h/h5971.md) [nasa'](../../strongs/h/h5375.md) their [bāṣēq](../../strongs/h/h1217.md) before it was [ḥāmēṣ](../../strongs/h/h2556.md), their [miš'ereṯ](../../strongs/h/h4863.md) being [tsarar](../../strongs/h/h6887.md) in their [śimlâ](../../strongs/h/h8071.md) upon their [šᵊḵem](../../strongs/h/h7926.md).

<a name="exodus_12_35"></a>Exodus 12:35

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) according to the [dabar](../../strongs/h/h1697.md) of [Mōshe](../../strongs/h/h4872.md); and they [sha'al](../../strongs/h/h7592.md) of the [Mitsrayim](../../strongs/h/h4714.md) [kĕliy](../../strongs/h/h3627.md) of [keceph](../../strongs/h/h3701.md), and [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md), and [śimlâ](../../strongs/h/h8071.md):

<a name="exodus_12_36"></a>Exodus 12:36

And [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) the ['am](../../strongs/h/h5971.md) [ḥēn](../../strongs/h/h2580.md) in the ['ayin](../../strongs/h/h5869.md) of the [Mitsrayim](../../strongs/h/h4714.md), so that they [sha'al](../../strongs/h/h7592.md) unto them such things as they required. And they [natsal](../../strongs/h/h5337.md) the [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_12_37"></a>Exodus 12:37

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāsaʿ](../../strongs/h/h5265.md) from [Raʿmᵊsēs](../../strongs/h/h7486.md) to [Sukôṯ](../../strongs/h/h5523.md), about [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md) ['elep̄](../../strongs/h/h505.md) [raḡlî](../../strongs/h/h7273.md) that were [geḇer](../../strongs/h/h1397.md), [baḏ](../../strongs/h/h905.md) [ṭap̄](../../strongs/h/h2945.md).

<a name="exodus_12_38"></a>Exodus 12:38

And an [ʿēreḇ](../../strongs/h/h6154.md) [rab](../../strongs/h/h7227.md) [ʿālâ](../../strongs/h/h5927.md) also with them; and [tso'n](../../strongs/h/h6629.md), and [bāqār](../../strongs/h/h1241.md), even [me'od](../../strongs/h/h3966.md) [kāḇēḏ](../../strongs/h/h3515.md) [miqnê](../../strongs/h/h4735.md). [^1]

<a name="exodus_12_39"></a>Exodus 12:39

And they ['āp̄â](../../strongs/h/h644.md) [maṣṣâ](../../strongs/h/h4682.md) [ʿugâ](../../strongs/h/h5692.md) of the [bāṣēq](../../strongs/h/h1217.md) which they [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md), for it was not [ḥāmēṣ](../../strongs/h/h2556.md); because they were [gāraš](../../strongs/h/h1644.md) of [Mitsrayim](../../strongs/h/h4714.md), and [yakol](../../strongs/h/h3201.md) not [māhah](../../strongs/h/h4102.md), neither had they ['asah](../../strongs/h/h6213.md) for themselves any [ṣêḏâ](../../strongs/h/h6720.md).

<a name="exodus_12_40"></a>Exodus 12:40

Now the [môšāḇ](../../strongs/h/h4186.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), who [yashab](../../strongs/h/h3427.md) in [Mitsrayim](../../strongs/h/h4714.md), was four hundred and thirty [šānâ](../../strongs/h/h8141.md).

<a name="exodus_12_41"></a>Exodus 12:41

And it came to pass at the [qēṣ](../../strongs/h/h7093.md) of the four hundred and thirty [šānâ](../../strongs/h/h8141.md), even the ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md) it came to pass, that all the [tsaba'](../../strongs/h/h6635.md) of [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) from the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_12_42"></a>Exodus 12:42

It is a [layil](../../strongs/h/h3915.md) to be [šimmurîm](../../strongs/h/h8107.md) unto [Yĕhovah](../../strongs/h/h3068.md) for [yāṣā'](../../strongs/h/h3318.md) them out from the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): this is that [layil](../../strongs/h/h3915.md) of [Yĕhovah](../../strongs/h/h3068.md) to be [šimmurîm](../../strongs/h/h8107.md) of all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) in their [dôr](../../strongs/h/h1755.md).

<a name="exodus_12_43"></a>Exodus 12:43

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md), This is the [chuqqah](../../strongs/h/h2708.md) of the [pecach](../../strongs/h/h6453.md): There shall no [ben](../../strongs/h/h1121.md) [nēḵār](../../strongs/h/h5236.md) ['akal](../../strongs/h/h398.md) thereof:

<a name="exodus_12_44"></a>Exodus 12:44

But every ['iysh](../../strongs/h/h376.md) ['ebed](../../strongs/h/h5650.md) that is [miqnâ](../../strongs/h/h4736.md) for [keceph](../../strongs/h/h3701.md), when thou hast [muwl](../../strongs/h/h4135.md) him, then shall he ['akal](../../strongs/h/h398.md) thereof.

<a name="exodus_12_45"></a>Exodus 12:45

A [tôšāḇ](../../strongs/h/h8453.md) and a [śāḵîr](../../strongs/h/h7916.md) shall not ['akal](../../strongs/h/h398.md) thereof.

<a name="exodus_12_46"></a>Exodus 12:46

In ['echad](../../strongs/h/h259.md) [bayith](../../strongs/h/h1004.md) shall it be ['akal](../../strongs/h/h398.md); thou shalt not carry[yāṣā'](../../strongs/h/h3318.md) ought of the [basar](../../strongs/h/h1320.md) [ḥûṣ](../../strongs/h/h2351.md) out of the [bayith](../../strongs/h/h1004.md); neither shall ye [shabar](../../strongs/h/h7665.md) an ['etsem](../../strongs/h/h6106.md) thereof.

<a name="exodus_12_47"></a>Exodus 12:47

All the ['edah](../../strongs/h/h5712.md) of [Yisra'el](../../strongs/h/h3478.md) shall ['asah](../../strongs/h/h6213.md) it.

<a name="exodus_12_48"></a>Exodus 12:48

And when a [ger](../../strongs/h/h1616.md) shall [guwr](../../strongs/h/h1481.md) with thee, and will ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) to [Yĕhovah](../../strongs/h/h3068.md), let all his [zāḵār](../../strongs/h/h2145.md) be [muwl](../../strongs/h/h4135.md), and then let him [qāraḇ](../../strongs/h/h7126.md) and ['asah](../../strongs/h/h6213.md) it; and he shall be as one that is ['ezrāḥ](../../strongs/h/h249.md) in the ['erets](../../strongs/h/h776.md): for no [ʿārēl](../../strongs/h/h6189.md) shall ['akal](../../strongs/h/h398.md) thereof.

<a name="exodus_12_49"></a>Exodus 12:49

['echad](../../strongs/h/h259.md) [towrah](../../strongs/h/h8451.md) shall be to him that is ['ezrāḥ](../../strongs/h/h249.md), and unto the [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) [tavek](../../strongs/h/h8432.md) you.

<a name="exodus_12_50"></a>Exodus 12:50

Thus did all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md), so ['asah](../../strongs/h/h6213.md) they.

<a name="exodus_12_51"></a>Exodus 12:51

And it came to pass the ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md), that [Yĕhovah](../../strongs/h/h3068.md) did [yāṣā'](../../strongs/h/h3318.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) by their [tsaba'](../../strongs/h/h6635.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 11](exodus_11.md) - [Exodus 13](exodus_13.md)

---

[^1]: [Exodus 12:38 Commentary](../../commentary/exodus/exodus_12_commentary.md#exodus_12_38)
