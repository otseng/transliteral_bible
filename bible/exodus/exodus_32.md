# [Exodus 32](https://www.blueletterbible.org/kjv/exo/32/1)

<a name="exodus_32_1"></a>Exodus 32:1

And when the ['am](../../strongs/h/h5971.md) [ra'ah](../../strongs/h/h7200.md) that [Mōshe](../../strongs/h/h4872.md) [buwsh](../../strongs/h/h954.md) to [yarad](../../strongs/h/h3381.md) out of the [har](../../strongs/h/h2022.md), the ['am](../../strongs/h/h5971.md) [qāhal](../../strongs/h/h6950.md) unto ['Ahărôn](../../strongs/h/h175.md), and ['āmar](../../strongs/h/h559.md) unto him, [quwm](../../strongs/h/h6965.md), ['asah](../../strongs/h/h6213.md) us ['Elohiym](../../strongs/h/h430.md), which shall [yālaḵ](../../strongs/h/h3212.md) [paniym](../../strongs/h/h6440.md) us; for as for this [Mōshe](../../strongs/h/h4872.md), the ['iysh](../../strongs/h/h376.md) that [ʿālâ](../../strongs/h/h5927.md) us up out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), we [yada'](../../strongs/h/h3045.md) not what is become of him.

<a name="exodus_32_2"></a>Exodus 32:2

And ['Ahărôn](../../strongs/h/h175.md) ['āmar](../../strongs/h/h559.md) unto them, [paraq](../../strongs/h/h6561.md) the [zāhāḇ](../../strongs/h/h2091.md) [nezem](../../strongs/h/h5141.md), which are in the ['ozen](../../strongs/h/h241.md) of your ['ishshah](../../strongs/h/h802.md), of your [ben](../../strongs/h/h1121.md), and of your [bath](../../strongs/h/h1323.md), and [bow'](../../strongs/h/h935.md) them unto me.

<a name="exodus_32_3"></a>Exodus 32:3

And all the ['am](../../strongs/h/h5971.md) [paraq](../../strongs/h/h6561.md) the [zāhāḇ](../../strongs/h/h2091.md) [nezem](../../strongs/h/h5141.md) which were in their ['ozen](../../strongs/h/h241.md), and [bow'](../../strongs/h/h935.md) them unto ['Ahărôn](../../strongs/h/h175.md).

<a name="exodus_32_4"></a>Exodus 32:4

And he [laqach](../../strongs/h/h3947.md) them at their [yad](../../strongs/h/h3027.md), and [ṣûr](../../strongs/h/h6696.md) it with a [ḥereṭ](../../strongs/h/h2747.md), after he had ['asah](../../strongs/h/h6213.md) it a [massēḵâ](../../strongs/h/h4541.md) [ʿēḡel](../../strongs/h/h5695.md): and they ['āmar](../../strongs/h/h559.md), These be thy ['Elohiym](../../strongs/h/h430.md), O [Yisra'el](../../strongs/h/h3478.md), which [ʿālâ](../../strongs/h/h5927.md) thee up out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_32_5"></a>Exodus 32:5

And when ['Ahărôn](../../strongs/h/h175.md) [ra'ah](../../strongs/h/h7200.md) it, he [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) [paniym](../../strongs/h/h6440.md) it; and ['Ahărôn](../../strongs/h/h175.md) [qara'](../../strongs/h/h7121.md), and ['āmar](../../strongs/h/h559.md), [māḥār](../../strongs/h/h4279.md) is a [ḥāḡ](../../strongs/h/h2282.md) to [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_32_6"></a>Exodus 32:6

And they [šāḵam](../../strongs/h/h7925.md) on the [māḥŏrāṯ](../../strongs/h/h4283.md), and [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md), and [nāḡaš](../../strongs/h/h5066.md) [šelem](../../strongs/h/h8002.md); and the ['am](../../strongs/h/h5971.md) [yashab](../../strongs/h/h3427.md) down to ['akal](../../strongs/h/h398.md) and to [šāṯâ](../../strongs/h/h8354.md), and [quwm](../../strongs/h/h6965.md) to [ṣāḥaq](../../strongs/h/h6711.md).

<a name="exodus_32_7"></a>Exodus 32:7

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), [yālaḵ](../../strongs/h/h3212.md), [yarad](../../strongs/h/h3381.md); for thy ['am](../../strongs/h/h5971.md), which thou [ʿālâ](../../strongs/h/h5927.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), have [shachath](../../strongs/h/h7843.md) themselves:

<a name="exodus_32_8"></a>Exodus 32:8

They have [cuwr](../../strongs/h/h5493.md) aside [mahēr](../../strongs/h/h4118.md) out of the [derek](../../strongs/h/h1870.md) which I [tsavah](../../strongs/h/h6680.md) them: they have ['asah](../../strongs/h/h6213.md) them a [massēḵâ](../../strongs/h/h4541.md) [ʿēḡel](../../strongs/h/h5695.md), and have [shachah](../../strongs/h/h7812.md) it, and have [zabach](../../strongs/h/h2076.md) thereunto, and ['āmar](../../strongs/h/h559.md), These be thy ['Elohiym](../../strongs/h/h430.md), O [Yisra'el](../../strongs/h/h3478.md), which have [ʿālâ](../../strongs/h/h5927.md) thee out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_32_9"></a>Exodus 32:9

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), I have [ra'ah](../../strongs/h/h7200.md) this ['am](../../strongs/h/h5971.md), and, behold, it is a [qāšê](../../strongs/h/h7186.md) [ʿōrep̄](../../strongs/h/h6203.md) ['am](../../strongs/h/h5971.md):

<a name="exodus_32_10"></a>Exodus 32:10

Now therefore [yānaḥ](../../strongs/h/h3240.md), that my ['aph](../../strongs/h/h639.md) may [ḥārâ](../../strongs/h/h2734.md) against them, and that I may [kalah](../../strongs/h/h3615.md) them: and I will ['asah](../../strongs/h/h6213.md) of thee a [gadowl](../../strongs/h/h1419.md) [gowy](../../strongs/h/h1471.md).

<a name="exodus_32_11"></a>Exodus 32:11

And [Mōshe](../../strongs/h/h4872.md) [ḥālâ](../../strongs/h/h2470.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md), and ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md), why doth thy ['aph](../../strongs/h/h639.md) [ḥārâ](../../strongs/h/h2734.md) against thy ['am](../../strongs/h/h5971.md), which thou hast [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) with [gadowl](../../strongs/h/h1419.md) [koach](../../strongs/h/h3581.md), and with a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md)?

<a name="exodus_32_12"></a>Exodus 32:12

Wherefore should the [Mitsrayim](../../strongs/h/h4714.md) ['āmar](../../strongs/h/h559.md), and ['āmar](../../strongs/h/h559.md), For [ra'](../../strongs/h/h7451.md) did he [yāṣā'](../../strongs/h/h3318.md) them out, to [harag](../../strongs/h/h2026.md) them in the [har](../../strongs/h/h2022.md), and to [kalah](../../strongs/h/h3615.md) them from the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md)? [shuwb](../../strongs/h/h7725.md) from thy [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md), and [nacham](../../strongs/h/h5162.md) of this [ra'](../../strongs/h/h7451.md) against thy ['am](../../strongs/h/h5971.md).

<a name="exodus_32_13"></a>Exodus 32:13

[zakar](../../strongs/h/h2142.md) ['Abraham](../../strongs/h/h85.md), [Yiṣḥāq](../../strongs/h/h3327.md), and [Yisra'el](../../strongs/h/h3478.md), thy ['ebed](../../strongs/h/h5650.md), to whom thou [shaba'](../../strongs/h/h7650.md) by thine own self, and [dabar](../../strongs/h/h1696.md) unto them, I will [rabah](../../strongs/h/h7235.md) your [zera'](../../strongs/h/h2233.md) as the [kowkab](../../strongs/h/h3556.md) of [shamayim](../../strongs/h/h8064.md), and all this ['erets](../../strongs/h/h776.md) that I have ['āmar](../../strongs/h/h559.md) of will I [nathan](../../strongs/h/h5414.md) unto your [zera'](../../strongs/h/h2233.md), and they shall [nāḥal](../../strongs/h/h5157.md) it ['owlam](../../strongs/h/h5769.md).

<a name="exodus_32_14"></a>Exodus 32:14

And [Yĕhovah](../../strongs/h/h3068.md) [nacham](../../strongs/h/h5162.md) of the [ra'](../../strongs/h/h7451.md) which he [dabar](../../strongs/h/h1696.md) to ['asah](../../strongs/h/h6213.md) unto his ['am](../../strongs/h/h5971.md).

<a name="exodus_32_15"></a>Exodus 32:15

And [Mōshe](../../strongs/h/h4872.md) [panah](../../strongs/h/h6437.md), and [yarad](../../strongs/h/h3381.md) from the [har](../../strongs/h/h2022.md), and the two [lûaḥ](../../strongs/h/h3871.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md) were in his [yad](../../strongs/h/h3027.md): the [lûaḥ](../../strongs/h/h3871.md) were [kāṯaḇ](../../strongs/h/h3789.md) on [šᵊnayim](../../strongs/h/h8147.md) their [ʿēḇer](../../strongs/h/h5676.md); on the one side and on the other were they [kāṯaḇ](../../strongs/h/h3789.md).

<a name="exodus_32_16"></a>Exodus 32:16

And the [lûaḥ](../../strongs/h/h3871.md) were the [ma'aseh](../../strongs/h/h4639.md) of ['Elohiym](../../strongs/h/h430.md), and the [miḵtāḇ](../../strongs/h/h4385.md) was the [miḵtāḇ](../../strongs/h/h4385.md) of ['Elohiym](../../strongs/h/h430.md), [ḥāraṯ](../../strongs/h/h2801.md) upon the [lûaḥ](../../strongs/h/h3871.md).

<a name="exodus_32_17"></a>Exodus 32:17

And when [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the ['am](../../strongs/h/h5971.md) as they [rēaʿ](../../strongs/h/h7452.md), he ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), There is a [qowl](../../strongs/h/h6963.md) of [milḥāmâ](../../strongs/h/h4421.md) in the [maḥănê](../../strongs/h/h4264.md).

<a name="exodus_32_18"></a>Exodus 32:18

And he ['āmar](../../strongs/h/h559.md), It is not the [qowl](../../strongs/h/h6963.md) of them that ['anah](../../strongs/h/h6030.md) for [gᵊḇûrâ](../../strongs/h/h1369.md), neither is it the [qowl](../../strongs/h/h6963.md) of them that ['anah](../../strongs/h/h6030.md) for [ḥălûšâ](../../strongs/h/h2476.md): but the [qowl](../../strongs/h/h6963.md) of them that [ʿānâ](../../strongs/h/h6031.md) do I [shama'](../../strongs/h/h8085.md).

<a name="exodus_32_19"></a>Exodus 32:19

And it came to pass, as soon as he [qāraḇ](../../strongs/h/h7126.md) unto the [maḥănê](../../strongs/h/h4264.md), that he [ra'ah](../../strongs/h/h7200.md) the [ʿēḡel](../../strongs/h/h5695.md), and the [mᵊḥōlâ](../../strongs/h/h4246.md): and [Mōshe](../../strongs/h/h4872.md) ['aph](../../strongs/h/h639.md) [ḥārâ](../../strongs/h/h2734.md), and he [shalak](../../strongs/h/h7993.md) the [lûaḥ](../../strongs/h/h3871.md) out of his [yad](../../strongs/h/h3027.md), and [shabar](../../strongs/h/h7665.md) them [taḥaṯ](../../strongs/h/h8478.md) the [har](../../strongs/h/h2022.md).

<a name="exodus_32_20"></a>Exodus 32:20

And he [laqach](../../strongs/h/h3947.md) the [ʿēḡel](../../strongs/h/h5695.md) which they had ['asah](../../strongs/h/h6213.md), and [śārap̄](../../strongs/h/h8313.md) it in the ['esh](../../strongs/h/h784.md), and [ṭāḥan](../../strongs/h/h2912.md) it to [dāqaq](../../strongs/h/h1854.md), and [zārâ](../../strongs/h/h2219.md) it [paniym](../../strongs/h/h6440.md) the [mayim](../../strongs/h/h4325.md), and made the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [šāqâ](../../strongs/h/h8248.md) of it.

<a name="exodus_32_21"></a>Exodus 32:21

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), What ['asah](../../strongs/h/h6213.md) this ['am](../../strongs/h/h5971.md) unto thee, that thou hast [bow'](../../strongs/h/h935.md) so [gadowl](../../strongs/h/h1419.md) a [ḥăṭā'â](../../strongs/h/h2401.md) upon them?

<a name="exodus_32_22"></a>Exodus 32:22

And ['Ahărôn](../../strongs/h/h175.md) ['āmar](../../strongs/h/h559.md), Let not the ['aph](../../strongs/h/h639.md) of my ['adown](../../strongs/h/h113.md) [ḥārâ](../../strongs/h/h2734.md): thou [yada'](../../strongs/h/h3045.md) the ['am](../../strongs/h/h5971.md), that they are set on [ra'](../../strongs/h/h7451.md).

<a name="exodus_32_23"></a>Exodus 32:23

For they ['āmar](../../strongs/h/h559.md) unto me, ['asah](../../strongs/h/h6213.md) us ['Elohiym](../../strongs/h/h430.md), which shall [yālaḵ](../../strongs/h/h3212.md) [paniym](../../strongs/h/h6440.md) us: for as for this [Mōshe](../../strongs/h/h4872.md), the ['iysh](../../strongs/h/h376.md) that [ʿālâ](../../strongs/h/h5927.md) us out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), we [yada'](../../strongs/h/h3045.md) not what is become of him.

<a name="exodus_32_24"></a>Exodus 32:24

And I ['āmar](../../strongs/h/h559.md) unto them, Whosoever hath any [zāhāḇ](../../strongs/h/h2091.md), let them [paraq](../../strongs/h/h6561.md) it off. So they [nathan](../../strongs/h/h5414.md) it me: then I [shalak](../../strongs/h/h7993.md) it into the ['esh](../../strongs/h/h784.md), and there [yāṣā'](../../strongs/h/h3318.md) this [ʿēḡel](../../strongs/h/h5695.md).

<a name="exodus_32_25"></a>Exodus 32:25

And when [Mōshe](../../strongs/h/h4872.md) [ra'ah](../../strongs/h/h7200.md) that the ['am](../../strongs/h/h5971.md) were [pāraʿ](../../strongs/h/h6544.md); (for ['Ahărôn](../../strongs/h/h175.md) had made them [pāraʿ](../../strongs/h/h6544.md) unto their [šimṣâ](../../strongs/h/h8103.md) among their [quwm](../../strongs/h/h6965.md):)

<a name="exodus_32_26"></a>Exodus 32:26

Then [Mōshe](../../strongs/h/h4872.md) ['amad](../../strongs/h/h5975.md) in the [sha'ar](../../strongs/h/h8179.md) of the [maḥănê](../../strongs/h/h4264.md), and ['āmar](../../strongs/h/h559.md), Who is on [Yĕhovah](../../strongs/h/h3068.md) side? let him come unto me. And all the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md) ['āsap̄](../../strongs/h/h622.md) themselves together unto him.

<a name="exodus_32_27"></a>Exodus 32:27

And he ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), [śûm](../../strongs/h/h7760.md) every ['iysh](../../strongs/h/h376.md) his [chereb](../../strongs/h/h2719.md) by his [yārēḵ](../../strongs/h/h3409.md), and ['abar](../../strongs/h/h5674.md) [shuwb](../../strongs/h/h7725.md) from [sha'ar](../../strongs/h/h8179.md) to [sha'ar](../../strongs/h/h8179.md) throughout the [maḥănê](../../strongs/h/h4264.md), and [harag](../../strongs/h/h2026.md) every ['iysh](../../strongs/h/h376.md) his ['ach](../../strongs/h/h251.md), and every ['iysh](../../strongs/h/h376.md) his [rea'](../../strongs/h/h7453.md), and every ['iysh](../../strongs/h/h376.md) his [qarowb](../../strongs/h/h7138.md).

<a name="exodus_32_28"></a>Exodus 32:28

And the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md) ['asah](../../strongs/h/h6213.md) according to the [dabar](../../strongs/h/h1697.md) of [Mōshe](../../strongs/h/h4872.md): and there [naphal](../../strongs/h/h5307.md) of the ['am](../../strongs/h/h5971.md) that [yowm](../../strongs/h/h3117.md) about three thousand ['iysh](../../strongs/h/h376.md).

<a name="exodus_32_29"></a>Exodus 32:29

For [Mōshe](../../strongs/h/h4872.md) had ['āmar](../../strongs/h/h559.md), [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) [yowm](../../strongs/h/h3117.md) to [Yĕhovah](../../strongs/h/h3068.md), even every ['iysh](../../strongs/h/h376.md) upon his [ben](../../strongs/h/h1121.md), and upon his ['ach](../../strongs/h/h251.md); that he may [nathan](../../strongs/h/h5414.md) upon you a [bĕrakah](../../strongs/h/h1293.md) this [yowm](../../strongs/h/h3117.md).

<a name="exodus_32_30"></a>Exodus 32:30

And it came to pass on the [māḥŏrāṯ](../../strongs/h/h4283.md), that [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), Ye have [chata'](../../strongs/h/h2398.md) a [gadowl](../../strongs/h/h1419.md) [ḥăṭā'â](../../strongs/h/h2401.md): and now I will [ʿālâ](../../strongs/h/h5927.md) unto [Yĕhovah](../../strongs/h/h3068.md); peradventure I shall make a [kāp̄ar](../../strongs/h/h3722.md) for your [chatta'ath](../../strongs/h/h2403.md).

<a name="exodus_32_31"></a>Exodus 32:31

And [Mōshe](../../strongs/h/h4872.md) [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), ['ānnā'](../../strongs/h/h577.md), this ['am](../../strongs/h/h5971.md) have [chata'](../../strongs/h/h2398.md) a [gadowl](../../strongs/h/h1419.md) [ḥăṭā'â](../../strongs/h/h2401.md), and have ['asah](../../strongs/h/h6213.md) them ['Elohiym](../../strongs/h/h430.md) of [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_32_32"></a>Exodus 32:32

Yet now, if thou wilt [nasa'](../../strongs/h/h5375.md) their [chatta'ath](../../strongs/h/h2403.md)--; and if not, [māḥâ](../../strongs/h/h4229.md) me, I pray thee, out of thy [sēp̄er](../../strongs/h/h5612.md) which thou hast [kāṯaḇ](../../strongs/h/h3789.md).

<a name="exodus_32_33"></a>Exodus 32:33

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), Whosoever hath [chata'](../../strongs/h/h2398.md) against me, him will I [māḥâ](../../strongs/h/h4229.md) of my [sēp̄er](../../strongs/h/h5612.md).

<a name="exodus_32_34"></a>Exodus 32:34

Therefore now [yālaḵ](../../strongs/h/h3212.md), [nachah](../../strongs/h/h5148.md) the ['am](../../strongs/h/h5971.md) unto of which I have [dabar](../../strongs/h/h1696.md) unto thee: behold, mine [mal'ak](../../strongs/h/h4397.md) shall [yālaḵ](../../strongs/h/h3212.md) [paniym](../../strongs/h/h6440.md) thee: nevertheless in the [yowm](../../strongs/h/h3117.md) when I [paqad](../../strongs/h/h6485.md) I will [paqad](../../strongs/h/h6485.md) their [chatta'ath](../../strongs/h/h2403.md) upon them.

<a name="exodus_32_35"></a>Exodus 32:35

And [Yĕhovah](../../strongs/h/h3068.md) [nāḡap̄](../../strongs/h/h5062.md) the ['am](../../strongs/h/h5971.md), because they ['asah](../../strongs/h/h6213.md) the [ʿēḡel](../../strongs/h/h5695.md), which ['Ahărôn](../../strongs/h/h175.md) ['asah](../../strongs/h/h6213.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 31](exodus_31.md) - [Exodus 33](exodus_33.md)