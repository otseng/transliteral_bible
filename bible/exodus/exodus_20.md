# [Exodus 20](https://www.blueletterbible.org/kjv/exo/20/1/s_70001)

<a name="exodus_20_1"></a>Exodus 20:1

And ['Elohiym](../../strongs/h/h430.md) [dabar](../../strongs/h/h1696.md) all these [dabar](../../strongs/h/h1697.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_20_2"></a>Exodus 20:2

[Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), which have [yāṣā'](../../strongs/h/h3318.md) thee out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), out of the [bayith](../../strongs/h/h1004.md) of ['ebed](../../strongs/h/h5650.md). [^1]

<a name="exodus_20_3"></a>Exodus 20:3

Thou shalt have no ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md) [paniym](../../strongs/h/h6440.md) me. [^2]

<a name="exodus_20_4"></a>Exodus 20:4

Thou shalt not ['asah](../../strongs/h/h6213.md) unto thee any [pecel](../../strongs/h/h6459.md), or any [tĕmuwnah](../../strongs/h/h8544.md) that in [shamayim](../../strongs/h/h8064.md) [maʿal](../../strongs/h/h4605.md), or that in the ['erets](../../strongs/h/h776.md) beneath, or that in the [mayim](../../strongs/h/h4325.md) under the ['erets](../../strongs/h/h776.md).

<a name="exodus_20_5"></a>Exodus 20:5

Thou shalt not [shachah](../../strongs/h/h7812.md) thyself to them, nor ['abad](../../strongs/h/h5647.md) them: for I [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) am [qanna'](../../strongs/h/h7067.md) ['el](../../strongs/h/h410.md), [paqad](../../strongs/h/h6485.md) the ['avon](../../strongs/h/h5771.md) of the ['ab](../../strongs/h/h1.md) upon the [ben](../../strongs/h/h1121.md) unto the third and fourth generation of them that [sane'](../../strongs/h/h8130.md) me;

<a name="exodus_20_6"></a>Exodus 20:6

And ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) unto thousands of them that ['ahab](../../strongs/h/h157.md) me, and [shamar](../../strongs/h/h8104.md) my [mitsvah](../../strongs/h/h4687.md). [^3]

<a name="exodus_20_7"></a>Exodus 20:7

Thou shalt not [nasa'](../../strongs/h/h5375.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) in [shav'](../../strongs/h/h7723.md); for [Yĕhovah](../../strongs/h/h3068.md) will not hold him [naqah](../../strongs/h/h5352.md) that taketh his [shem](../../strongs/h/h8034.md) in [shav'](../../strongs/h/h7723.md). [^4]

<a name="exodus_20_8"></a>Exodus 20:8

[zakar](../../strongs/h/h2142.md) the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md), to [qadash](../../strongs/h/h6942.md) it.

<a name="exodus_20_9"></a>Exodus 20:9

Six [yowm](../../strongs/h/h3117.md) shalt thou ['abad](../../strongs/h/h5647.md), and ['asah](../../strongs/h/h6213.md) all thy [mĕla'kah](../../strongs/h/h4399.md):

<a name="exodus_20_10"></a>Exodus 20:10

But the seventh [yowm](../../strongs/h/h3117.md) is the [shabbath](../../strongs/h/h7676.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md): in it thou shalt not ['asah](../../strongs/h/h6213.md) any [mĕla'kah](../../strongs/h/h4399.md), thou, nor thy [ben](../../strongs/h/h1121.md), nor thy [bath](../../strongs/h/h1323.md), thy ['ebed](../../strongs/h/h5650.md), nor thy ['amah](../../strongs/h/h519.md), nor thy [bĕhemah](../../strongs/h/h929.md), nor thy [ger](../../strongs/h/h1616.md) that is within thy [sha'ar](../../strongs/h/h8179.md):

<a name="exodus_20_11"></a>Exodus 20:11

For in six [yowm](../../strongs/h/h3117.md) [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md), the [yam](../../strongs/h/h3220.md), and all that in them is, and [nuwach](../../strongs/h/h5117.md) the seventh [yowm](../../strongs/h/h3117.md): wherefore [Yĕhovah](../../strongs/h/h3068.md) [barak](../../strongs/h/h1288.md) the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md), and [qadash](../../strongs/h/h6942.md) it. [^5]

<a name="exodus_20_12"></a>Exodus 20:12

[kabad](../../strongs/h/h3513.md) thy ['ab](../../strongs/h/h1.md) and thy ['em](../../strongs/h/h517.md): that thy [yowm](../../strongs/h/h3117.md) may be ['arak](../../strongs/h/h748.md) upon the ['ăḏāmâ](../../strongs/h/h127.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee. [^6]

<a name="exodus_20_13"></a>Exodus 20:13

Thou shalt not [ratsach](../../strongs/h/h7523.md). [^7]

<a name="exodus_20_14"></a>Exodus 20:14

Thou shalt not commit [na'aph](../../strongs/h/h5003.md). [^8]

<a name="exodus_20_15"></a>Exodus 20:15

Thou shalt not [ganab](../../strongs/h/h1589.md). [^9]

<a name="exodus_20_16"></a>Exodus 20:16

Thou shalt not ['anah](../../strongs/h/h6030.md) [sheqer](../../strongs/h/h8267.md) ['ed](../../strongs/h/h5707.md) against thy [rea'](../../strongs/h/h7453.md). [^10]

<a name="exodus_20_17"></a>Exodus 20:17

Thou shalt not [chamad](../../strongs/h/h2530.md) thy [rea'](../../strongs/h/h7453.md) [bayith](../../strongs/h/h1004.md), thou shalt not [chamad](../../strongs/h/h2530.md) thy [rea'](../../strongs/h/h7453.md) ['ishshah](../../strongs/h/h802.md), nor his ['ebed](../../strongs/h/h5650.md), nor his ['amah](../../strongs/h/h519.md), nor his [showr](../../strongs/h/h7794.md), nor his [chamowr](../../strongs/h/h2543.md), nor any thing that is thy [rea'](../../strongs/h/h7453.md). [^11]

<a name="exodus_20_18"></a>Exodus 20:18

And all the ['am](../../strongs/h/h5971.md) [ra'ah](../../strongs/h/h7200.md) the [qowl](../../strongs/h/h6963.md), and the [lapîḏ](../../strongs/h/h3940.md), and the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md), and the [har](../../strongs/h/h2022.md) [ʿāšēn](../../strongs/h/h6226.md): and when the ['am](../../strongs/h/h5971.md) [ra'ah](../../strongs/h/h7200.md) it, they [nûaʿ](../../strongs/h/h5128.md), and ['amad](../../strongs/h/h5975.md) [rachowq](../../strongs/h/h7350.md).

<a name="exodus_20_19"></a>Exodus 20:19

And they ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [dabar](../../strongs/h/h1696.md) thou with us, and we will [shama'](../../strongs/h/h8085.md): but let not ['Elohiym](../../strongs/h/h430.md) [dabar](../../strongs/h/h1696.md) with us, lest we [muwth](../../strongs/h/h4191.md).

<a name="exodus_20_20"></a>Exodus 20:20

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), [yare'](../../strongs/h/h3372.md) not: for ['Elohiym](../../strongs/h/h430.md) is [bow'](../../strongs/h/h935.md) to [nāsâ](../../strongs/h/h5254.md) you, and that his [yir'ah](../../strongs/h/h3374.md) may be before your [paniym](../../strongs/h/h6440.md), that ye [chata'](../../strongs/h/h2398.md) not.

<a name="exodus_20_21"></a>Exodus 20:21

And the ['am](../../strongs/h/h5971.md) ['amad](../../strongs/h/h5975.md) [rachowq](../../strongs/h/h7350.md), and [Mōshe](../../strongs/h/h4872.md) [nāḡaš](../../strongs/h/h5066.md) unto the ['araphel](../../strongs/h/h6205.md) where ['Elohiym](../../strongs/h/h430.md) was.

<a name="exodus_20_22"></a>Exodus 20:22

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), Thus thou shalt ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), Ye have [ra'ah](../../strongs/h/h7200.md) that I have [dabar](../../strongs/h/h1696.md) with you from [shamayim](../../strongs/h/h8064.md).

<a name="exodus_20_23"></a>Exodus 20:23

Ye shall not ['asah](../../strongs/h/h6213.md) with me ['Elohiym](../../strongs/h/h430.md) of [keceph](../../strongs/h/h3701.md), neither shall ye ['asah](../../strongs/h/h6213.md) unto you ['Elohiym](../../strongs/h/h430.md) of [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_20_24"></a>Exodus 20:24

a [mizbeach](../../strongs/h/h4196.md) of ['ăḏāmâ](../../strongs/h/h127.md) thou shalt ['asah](../../strongs/h/h6213.md) unto me, and shalt [zabach](../../strongs/h/h2076.md) thereon thy [ʿōlâ](../../strongs/h/h5930.md), and thy [šelem](../../strongs/h/h8002.md), thy [tso'n](../../strongs/h/h6629.md), and thine [bāqār](../../strongs/h/h1241.md): in all [maqowm](../../strongs/h/h4725.md) where I [zakar](../../strongs/h/h2142.md) my [shem](../../strongs/h/h8034.md) I will [bow'](../../strongs/h/h935.md) unto thee, and I will [barak](../../strongs/h/h1288.md) thee.

<a name="exodus_20_25"></a>Exodus 20:25

And if thou wilt ['asah](../../strongs/h/h6213.md) me a [mizbeach](../../strongs/h/h4196.md) of ['eben](../../strongs/h/h68.md), thou shalt not [bānâ](../../strongs/h/h1129.md) it of [gāzîṯ](../../strongs/h/h1496.md): for if thou [nûp̄](../../strongs/h/h5130.md) thy [chereb](../../strongs/h/h2719.md) upon it, thou hast [ḥālal](../../strongs/h/h2490.md) it.

<a name="exodus_20_26"></a>Exodus 20:26

Neither shalt thou [ʿālâ](../../strongs/h/h5927.md) by [maʿălâ](../../strongs/h/h4609.md) unto minea [mizbeach](../../strongs/h/h4196.md), that thy [ʿervâ](../../strongs/h/h6172.md) be not [gālâ](../../strongs/h/h1540.md) thereon.

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 19](exodus_19.md) - [Exodus 21](exodus_21.md)

---

[^1]: [Exodus 20:2 Commentary](../../commentary/exodus/exodus_20_commentary.md#exodus_20_2)

[^2]: [Exodus 20:3 Commentary](../../commentary/exodus/exodus_20_commentary.md#exodus_20_3)

[^3]: [Exodus 20:6 Commentary](../../commentary/exodus/exodus_20_commentary.md#exodus_20_6)

[^4]: [Exodus 20:7 Commentary](../../commentary/exodus/exodus_20_commentary.md#exodus_20_7)

[^5]: [Exodus 20:11 Commentary](../../commentary/exodus/exodus_20_commentary.md#exodus_20_11)

[^6]: [Exodus 20:12 Commentary](../../commentary/exodus/exodus_20_commentary.md#exodus_20_12)

[^7]: [Exodus 20:13 Commentary](../../commentary/exodus/exodus_20_commentary.md#exodus_20_13)

[^8]: [Exodus 20:14 Commentary](../../commentary/exodus/exodus_20_commentary.md#exodus_20_14)

[^9]: [Exodus 20:15 Commentary](../../commentary/exodus/exodus_20_commentary.md#exodus_20_15)

[^10]: [Exodus 20:16 Commentary](../../commentary/exodus/exodus_20_commentary.md#exodus_20_16)

[^11]: [Exodus 20:17 Commentary](../../commentary/exodus/exodus_20_commentary.md#exodus_20_17)
