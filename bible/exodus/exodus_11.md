# [Exodus 11](https://www.blueletterbible.org/kjv/exo/11/1/s_61001)

<a name="exodus_11_1"></a>Exodus 11:1

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), Yet will I [bow'](../../strongs/h/h935.md) one [neḡaʿ](../../strongs/h/h5061.md) more upon [Parʿô](../../strongs/h/h6547.md), and upon [Mitsrayim](../../strongs/h/h4714.md); ['aḥar](../../strongs/h/h310.md) he will let you [shalach](../../strongs/h/h7971.md) hence: when he shall let you [shalach](../../strongs/h/h7971.md), he shall [gāraš](../../strongs/h/h1644.md) [gāraš](../../strongs/h/h1644.md) out hence [kālâ](../../strongs/h/h3617.md).

<a name="exodus_11_2"></a>Exodus 11:2

[dabar](../../strongs/h/h1696.md) now in the ['ozen](../../strongs/h/h241.md) of the ['am](../../strongs/h/h5971.md), and let every ['iysh](../../strongs/h/h376.md) [sha'al](../../strongs/h/h7592.md) of his [rea'](../../strongs/h/h7453.md), and every ['ishshah](../../strongs/h/h802.md) of her [rᵊʿûṯ](../../strongs/h/h7468.md), [kĕliy](../../strongs/h/h3627.md) of [keceph](../../strongs/h/h3701.md) and [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_11_3"></a>Exodus 11:3

And [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) the ['am](../../strongs/h/h5971.md) [ḥēn](../../strongs/h/h2580.md) in the ['ayin](../../strongs/h/h5869.md) of the [Mitsrayim](../../strongs/h/h4714.md). Moreover the ['iysh](../../strongs/h/h376.md) [Mōshe](../../strongs/h/h4872.md) was [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), in the ['ayin](../../strongs/h/h5869.md) of [Parʿô](../../strongs/h/h6547.md) ['ebed](../../strongs/h/h5650.md), and in the ['ayin](../../strongs/h/h5869.md) of the ['am](../../strongs/h/h5971.md).

<a name="exodus_11_4"></a>Exodus 11:4

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), About [layil](../../strongs/h/h3915.md) [ḥāṣôṯ](../../strongs/h/h2676.md) will I [yāṣā'](../../strongs/h/h3318.md) into the [tavek](../../strongs/h/h8432.md) of [Mitsrayim](../../strongs/h/h4714.md):

<a name="exodus_11_5"></a>Exodus 11:5

And all the [bᵊḵôr](../../strongs/h/h1060.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) shall [muwth](../../strongs/h/h4191.md), from the [bᵊḵôr](../../strongs/h/h1060.md) of [Parʿô](../../strongs/h/h6547.md) that [yashab](../../strongs/h/h3427.md) upon his [kicce'](../../strongs/h/h3678.md), even unto the [bᵊḵôr](../../strongs/h/h1060.md) of the [šip̄ḥâ](../../strongs/h/h8198.md) that is ['aḥar](../../strongs/h/h310.md) the [rēḥayim](../../strongs/h/h7347.md); and all the [bᵊḵôr](../../strongs/h/h1060.md) of [bĕhemah](../../strongs/h/h929.md).

<a name="exodus_11_6"></a>Exodus 11:6

And there shall be a [gadowl](../../strongs/h/h1419.md) [tsa'aqah](../../strongs/h/h6818.md) throughout all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), such as there was none like it, nor shall be like it [yāsap̄](../../strongs/h/h3254.md).

<a name="exodus_11_7"></a>Exodus 11:7

But against any of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) shall not a [keleḇ](../../strongs/h/h3611.md) [ḥāraṣ](../../strongs/h/h2782.md) his [lashown](../../strongs/h/h3956.md), against ['iysh](../../strongs/h/h376.md) or [bĕhemah](../../strongs/h/h929.md): that ye may [yada'](../../strongs/h/h3045.md) how that [Yĕhovah](../../strongs/h/h3068.md) doth [palah](../../strongs/h/h6395.md) between the [Mitsrayim](../../strongs/h/h4714.md) and [Yisra'el](../../strongs/h/h3478.md).

<a name="exodus_11_8"></a>Exodus 11:8

And all these thy ['ebed](../../strongs/h/h5650.md) shall [yarad](../../strongs/h/h3381.md) unto me, and [shachah](../../strongs/h/h7812.md) themselves unto me, ['āmar](../../strongs/h/h559.md), [yāṣā'](../../strongs/h/h3318.md) thee, and all the ['am](../../strongs/h/h5971.md) that [regel](../../strongs/h/h7272.md) thee: and ['aḥar](../../strongs/h/h310.md) that I will [yāṣā'](../../strongs/h/h3318.md). And he [yāṣā'](../../strongs/h/h3318.md) from [Parʿô](../../strongs/h/h6547.md) in a [ḥŏrî](../../strongs/h/h2750.md) ['aph](../../strongs/h/h639.md).

<a name="exodus_11_9"></a>Exodus 11:9

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [Parʿô](../../strongs/h/h6547.md) shall not [shama'](../../strongs/h/h8085.md) unto you; that my [môp̄ēṯ](../../strongs/h/h4159.md) may be [rabah](../../strongs/h/h7235.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_11_10"></a>Exodus 11:10

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) ['asah](../../strongs/h/h6213.md) all these [môp̄ēṯ](../../strongs/h/h4159.md) [paniym](../../strongs/h/h6440.md) [Parʿô](../../strongs/h/h6547.md): and [Yĕhovah](../../strongs/h/h3068.md) [ḥāzaq](../../strongs/h/h2388.md) [Parʿô](../../strongs/h/h6547.md) [leb](../../strongs/h/h3820.md), so that he would not let the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md) of his ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 10](exodus_10.md) - [Exodus 12](exodus_12.md)