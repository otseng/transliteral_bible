# [Exodus 19](https://www.blueletterbible.org/kjv/exo/19/1/s_69001)

<a name="exodus_19_1"></a>Exodus 19:1

In the third [ḥōḏeš](../../strongs/h/h2320.md), when the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), the same [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md) they into the [midbar](../../strongs/h/h4057.md) of [Sînay](../../strongs/h/h5514.md).

<a name="exodus_19_2"></a>Exodus 19:2

For they were [nāsaʿ](../../strongs/h/h5265.md) from [Rᵊp̄îḏîm](../../strongs/h/h7508.md), and were [bow'](../../strongs/h/h935.md) to the [midbar](../../strongs/h/h4057.md) of [Sînay](../../strongs/h/h5514.md), and had [ḥānâ](../../strongs/h/h2583.md) in the [midbar](../../strongs/h/h4057.md); and there [Yisra'el](../../strongs/h/h3478.md) [ḥānâ](../../strongs/h/h2583.md) before the [har](../../strongs/h/h2022.md).

<a name="exodus_19_3"></a>Exodus 19:3

And [Mōshe](../../strongs/h/h4872.md) [ʿālâ](../../strongs/h/h5927.md) unto ['Elohiym](../../strongs/h/h430.md), and [Yĕhovah](../../strongs/h/h3068.md) [qara'](../../strongs/h/h7121.md) unto him out of the [har](../../strongs/h/h2022.md), ['āmar](../../strongs/h/h559.md), Thus shalt thou ['āmar](../../strongs/h/h559.md) to the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), and [nāḡaḏ](../../strongs/h/h5046.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md);

<a name="exodus_19_4"></a>Exodus 19:4

Ye have [ra'ah](../../strongs/h/h7200.md) what I ['asah](../../strongs/h/h6213.md) unto the [Mitsrayim](../../strongs/h/h4714.md), and how I [nasa'](../../strongs/h/h5375.md) you on [nesheׁr](../../strongs/h/h5404.md) [kanaph](../../strongs/h/h3671.md), and [bow'](../../strongs/h/h935.md) you unto myself.

<a name="exodus_19_5"></a>Exodus 19:5

Now therefore, if ye will [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md) indeed, and [shamar](../../strongs/h/h8104.md) my [bĕriyth](../../strongs/h/h1285.md), then ye shall be a peculiar [sᵊḡullâ](../../strongs/h/h5459.md) unto me above all ['am](../../strongs/h/h5971.md): for all the ['erets](../../strongs/h/h776.md) is mine:

<a name="exodus_19_6"></a>Exodus 19:6

And ye shall be unto me a [mamlāḵâ](../../strongs/h/h4467.md) of [kōhēn](../../strongs/h/h3548.md), and a [qadowsh](../../strongs/h/h6918.md) [gowy](../../strongs/h/h1471.md). These are the [dabar](../../strongs/h/h1697.md) which thou shalt [dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="exodus_19_7"></a>Exodus 19:7

And [Mōshe](../../strongs/h/h4872.md) [bow'](../../strongs/h/h935.md) and [qara'](../../strongs/h/h7121.md) for the [zāqēn](../../strongs/h/h2205.md) of the ['am](../../strongs/h/h5971.md), and [śûm](../../strongs/h/h7760.md) before their [paniym](../../strongs/h/h6440.md) all these [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) him.

<a name="exodus_19_8"></a>Exodus 19:8

And all the ['am](../../strongs/h/h5971.md) ['anah](../../strongs/h/h6030.md) [yaḥaḏ](../../strongs/h/h3162.md), and ['āmar](../../strongs/h/h559.md), All that [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) we will ['asah](../../strongs/h/h6213.md). And [Mōshe](../../strongs/h/h4872.md) [shuwb](../../strongs/h/h7725.md) the [dabar](../../strongs/h/h1697.md) of the ['am](../../strongs/h/h5971.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_19_9"></a>Exodus 19:9

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), Lo, I [bow'](../../strongs/h/h935.md) unto thee in an ['ab](../../strongs/h/h5645.md) [ʿānān](../../strongs/h/h6051.md), that the ['am](../../strongs/h/h5971.md) may [shama'](../../strongs/h/h8085.md) when I [dabar](../../strongs/h/h1696.md) with thee, and ['aman](../../strongs/h/h539.md) thee ['owlam](../../strongs/h/h5769.md). And [Mōshe](../../strongs/h/h4872.md) [nāḡaḏ](../../strongs/h/h5046.md) the [dabar](../../strongs/h/h1697.md) of the ['am](../../strongs/h/h5971.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_19_10"></a>Exodus 19:10

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [yālaḵ](../../strongs/h/h3212.md) unto the ['am](../../strongs/h/h5971.md), and [qadash](../../strongs/h/h6942.md) them to [yowm](../../strongs/h/h3117.md) and [māḥār](../../strongs/h/h4279.md), and let them [kāḇas](../../strongs/h/h3526.md) their [śimlâ](../../strongs/h/h8071.md),

<a name="exodus_19_11"></a>Exodus 19:11

And be [kuwn](../../strongs/h/h3559.md) against the third [yowm](../../strongs/h/h3117.md): for the third [yowm](../../strongs/h/h3117.md) [Yĕhovah](../../strongs/h/h3068.md) will [yarad](../../strongs/h/h3381.md) in the ['ayin](../../strongs/h/h5869.md) of all the ['am](../../strongs/h/h5971.md) upon [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md).

<a name="exodus_19_12"></a>Exodus 19:12

And thou shalt [gāḇal](../../strongs/h/h1379.md) unto the ['am](../../strongs/h/h5971.md) [cabiyb](../../strongs/h/h5439.md), ['āmar](../../strongs/h/h559.md), [shamar](../../strongs/h/h8104.md) to yourselves, that ye [ʿālâ](../../strongs/h/h5927.md) not into the [har](../../strongs/h/h2022.md), or [naga'](../../strongs/h/h5060.md) the [qāṣê](../../strongs/h/h7097.md) of it: whosoever [naga'](../../strongs/h/h5060.md) the [har](../../strongs/h/h2022.md) shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md):

<a name="exodus_19_13"></a>Exodus 19:13

There shall not a [yad](../../strongs/h/h3027.md) [naga'](../../strongs/h/h5060.md) it, but he shall [sāqal](../../strongs/h/h5619.md) [sāqal](../../strongs/h/h5619.md), or [yārâ](../../strongs/h/h3384.md) [yārâ](../../strongs/h/h3384.md); whether it be [bĕhemah](../../strongs/h/h929.md) or ['iysh](../../strongs/h/h376.md), it shall not [ḥāyâ](../../strongs/h/h2421.md): when the [yôḇēl](../../strongs/h/h3104.md) [mashak](../../strongs/h/h4900.md), they shall [ʿālâ](../../strongs/h/h5927.md) to the [har](../../strongs/h/h2022.md).

<a name="exodus_19_14"></a>Exodus 19:14

And [Mōshe](../../strongs/h/h4872.md) [yarad](../../strongs/h/h3381.md) from the [har](../../strongs/h/h2022.md) unto the ['am](../../strongs/h/h5971.md), and [qadash](../../strongs/h/h6942.md) the ['am](../../strongs/h/h5971.md); and they [kāḇas](../../strongs/h/h3526.md) their [śimlâ](../../strongs/h/h8071.md).

<a name="exodus_19_15"></a>Exodus 19:15

And he ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), Be [kuwn](../../strongs/h/h3559.md) against the third [yowm](../../strongs/h/h3117.md): [nāḡaš](../../strongs/h/h5066.md) not at your ['ishshah](../../strongs/h/h802.md).

<a name="exodus_19_16"></a>Exodus 19:16

And it came to pass on the third [yowm](../../strongs/h/h3117.md) in the [boqer](../../strongs/h/h1242.md), that there were [qowl](../../strongs/h/h6963.md) and [baraq](../../strongs/h/h1300.md), and a [kāḇēḏ](../../strongs/h/h3515.md) [ʿānān](../../strongs/h/h6051.md) upon the [har](../../strongs/h/h2022.md), and the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md) [me'od](../../strongs/h/h3966.md) [ḥāzāq](../../strongs/h/h2389.md); so that all the ['am](../../strongs/h/h5971.md) that was in the [maḥănê](../../strongs/h/h4264.md) [ḥārēḏ](../../strongs/h/h2729.md).

<a name="exodus_19_17"></a>Exodus 19:17

And [Mōshe](../../strongs/h/h4872.md) [yāṣā'](../../strongs/h/h3318.md) the ['am](../../strongs/h/h5971.md) out of the [maḥănê](../../strongs/h/h4264.md) to [qārā'](../../strongs/h/h7125.md) with ['Elohiym](../../strongs/h/h430.md); and they [yatsab](../../strongs/h/h3320.md) at the [taḥtî](../../strongs/h/h8482.md) part of the [har](../../strongs/h/h2022.md).

<a name="exodus_19_18"></a>Exodus 19:18

And [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md) was altogether on an [ʿāšēn](../../strongs/h/h6225.md), [paniym](../../strongs/h/h6440.md) ['ăšer](../../strongs/h/h834.md) [Yĕhovah](../../strongs/h/h3068.md) [yarad](../../strongs/h/h3381.md) upon it in ['esh](../../strongs/h/h784.md): and the ['ashan](../../strongs/h/h6227.md) thereof [ʿālâ](../../strongs/h/h5927.md) as the ['ashan](../../strongs/h/h6227.md) of a [kiḇšān](../../strongs/h/h3536.md), and the [har](../../strongs/h/h2022.md) [ḥārēḏ](../../strongs/h/h2729.md) [me'od](../../strongs/h/h3966.md).

<a name="exodus_19_19"></a>Exodus 19:19

And when the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md) [qowl](../../strongs/h/h6963.md) [halak](../../strongs/h/h1980.md), and [ḥāzēq](../../strongs/h/h2390.md) and [me'od](../../strongs/h/h3966.md), [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md), and ['Elohiym](../../strongs/h/h430.md) ['anah](../../strongs/h/h6030.md) him by a [qowl](../../strongs/h/h6963.md).

<a name="exodus_19_20"></a>Exodus 19:20

And [Yĕhovah](../../strongs/h/h3068.md) [yarad](../../strongs/h/h3381.md) upon [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md), on the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md): and [Yĕhovah](../../strongs/h/h3068.md) [qara'](../../strongs/h/h7121.md) [Mōshe](../../strongs/h/h4872.md) up to the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md); and [Mōshe](../../strongs/h/h4872.md) [ʿālâ](../../strongs/h/h5927.md).

<a name="exodus_19_21"></a>Exodus 19:21

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [yarad](../../strongs/h/h3381.md), [ʿûḏ](../../strongs/h/h5749.md) the ['am](../../strongs/h/h5971.md), lest they [harac](../../strongs/h/h2040.md) unto [Yĕhovah](../../strongs/h/h3068.md) to [ra'ah](../../strongs/h/h7200.md), and [rab](../../strongs/h/h7227.md) of them [naphal](../../strongs/h/h5307.md).

<a name="exodus_19_22"></a>Exodus 19:22

And let the [kōhēn](../../strongs/h/h3548.md) also, which [nāḡaš](../../strongs/h/h5066.md) to [Yĕhovah](../../strongs/h/h3068.md), [qadash](../../strongs/h/h6942.md) themselves, lest [Yĕhovah](../../strongs/h/h3068.md) [pāraṣ](../../strongs/h/h6555.md) upon them.

<a name="exodus_19_23"></a>Exodus 19:23

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto [Yĕhovah](../../strongs/h/h3068.md), The ['am](../../strongs/h/h5971.md) [yakol](../../strongs/h/h3201.md) [ʿālâ](../../strongs/h/h5927.md) to [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md): for thou [ʿûḏ](../../strongs/h/h5749.md) us, ['āmar](../../strongs/h/h559.md), [gāḇal](../../strongs/h/h1379.md) about the [har](../../strongs/h/h2022.md), and [qadash](../../strongs/h/h6942.md) it.

<a name="exodus_19_24"></a>Exodus 19:24

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, [yālaḵ](../../strongs/h/h3212.md), [yarad](../../strongs/h/h3381.md), and thou shalt [ʿālâ](../../strongs/h/h5927.md), thou, and ['Ahărôn](../../strongs/h/h175.md) with thee: but let not the [kōhēn](../../strongs/h/h3548.md) and the ['am](../../strongs/h/h5971.md) [harac](../../strongs/h/h2040.md) to [ʿālâ](../../strongs/h/h5927.md) unto [Yĕhovah](../../strongs/h/h3068.md), lest he [pāraṣ](../../strongs/h/h6555.md) upon them.

<a name="exodus_19_25"></a>Exodus 19:25

So [Mōshe](../../strongs/h/h4872.md) [yarad](../../strongs/h/h3381.md) unto the ['am](../../strongs/h/h5971.md), and ['āmar](../../strongs/h/h559.md) unto them.

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 18](exodus_18.md) - [Exodus 20](exodus_20.md)