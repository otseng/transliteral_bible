# [Exodus 17](https://www.blueletterbible.org/kjv/exo/17/1/s_67001)

<a name="exodus_17_1"></a>Exodus 17:1

And all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāsaʿ](../../strongs/h/h5265.md) from the [midbar](../../strongs/h/h4057.md) of [Sîn](../../strongs/h/h5512.md), after their [massāʿ](../../strongs/h/h4550.md), according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md), and [ḥānâ](../../strongs/h/h2583.md) in [Rᵊp̄îḏîm](../../strongs/h/h7508.md): and there was no [mayim](../../strongs/h/h4325.md) for the ['am](../../strongs/h/h5971.md) to [šāṯâ](../../strongs/h/h8354.md).

<a name="exodus_17_2"></a>Exodus 17:2

Wherefore the ['am](../../strongs/h/h5971.md) did [riyb](../../strongs/h/h7378.md) with [Mōshe](../../strongs/h/h4872.md), and ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) us [mayim](../../strongs/h/h4325.md) that we may [šāṯâ](../../strongs/h/h8354.md). And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto them, Why [riyb](../../strongs/h/h7378.md) ye with me? wherefore do ye [nāsâ](../../strongs/h/h5254.md) [Yĕhovah](../../strongs/h/h3068.md)?

<a name="exodus_17_3"></a>Exodus 17:3

And the ['am](../../strongs/h/h5971.md) [ṣāmē'](../../strongs/h/h6770.md) there for [mayim](../../strongs/h/h4325.md); and the ['am](../../strongs/h/h5971.md) [lûn](../../strongs/h/h3885.md) against [Mōshe](../../strongs/h/h4872.md), and ['āmar](../../strongs/h/h559.md), Wherefore is this that thou hast [ʿālâ](../../strongs/h/h5927.md) us up out of [Mitsrayim](../../strongs/h/h4714.md), to [muwth](../../strongs/h/h4191.md) us and our [ben](../../strongs/h/h1121.md) and our [miqnê](../../strongs/h/h4735.md) with [ṣāmā'](../../strongs/h/h6772.md)?

<a name="exodus_17_4"></a>Exodus 17:4

And [Mōshe](../../strongs/h/h4872.md) [ṣāʿaq](../../strongs/h/h6817.md) unto [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), What shall I ['asah](../../strongs/h/h6213.md) unto this ['am](../../strongs/h/h5971.md)? they be [mᵊʿaṭ](../../strongs/h/h4592.md) ready to [sāqal](../../strongs/h/h5619.md) me.

<a name="exodus_17_5"></a>Exodus 17:5

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) the ['am](../../strongs/h/h5971.md), and [laqach](../../strongs/h/h3947.md) with thee of the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md); and thy [maṭṭê](../../strongs/h/h4294.md), wherewith thou [nakah](../../strongs/h/h5221.md) the [yᵊ'ōr](../../strongs/h/h2975.md), [laqach](../../strongs/h/h3947.md) in thine [yad](../../strongs/h/h3027.md), and [halak](../../strongs/h/h1980.md).

<a name="exodus_17_6"></a>Exodus 17:6

Behold, I will ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) thee there upon the [tsuwr](../../strongs/h/h6697.md) in [ḥōrēḇ](../../strongs/h/h2722.md); and thou shalt [nakah](../../strongs/h/h5221.md) the [tsuwr](../../strongs/h/h6697.md), and there shall [yāṣā'](../../strongs/h/h3318.md) [mayim](../../strongs/h/h4325.md) of it, that the ['am](../../strongs/h/h5971.md) may [šāṯâ](../../strongs/h/h8354.md). And [Mōshe](../../strongs/h/h4872.md) ['asah](../../strongs/h/h6213.md) so in the ['ayin](../../strongs/h/h5869.md) of the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="exodus_17_7"></a>Exodus 17:7

And he [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of the [maqowm](../../strongs/h/h4725.md) [massâ](../../strongs/h/h4532.md), and [Mᵊrîḇâ](../../strongs/h/h4809.md), because of the [rîḇ](../../strongs/h/h7379.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and because they [nāsâ](../../strongs/h/h5254.md) [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), Is [Yĕhovah](../../strongs/h/h3068.md) [qereḇ](../../strongs/h/h7130.md) us, or not?

<a name="exodus_17_8"></a>Exodus 17:8

Then [bow'](../../strongs/h/h935.md) [ʿĂmālēq](../../strongs/h/h6002.md), and [lāḥam](../../strongs/h/h3898.md) with [Yisra'el](../../strongs/h/h3478.md) in [Rᵊp̄îḏîm](../../strongs/h/h7508.md).

<a name="exodus_17_9"></a>Exodus 17:9

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), [bāḥar](../../strongs/h/h977.md) us out ['enowsh](../../strongs/h/h582.md), and [yāṣā'](../../strongs/h/h3318.md), [lāḥam](../../strongs/h/h3898.md) with [ʿĂmālēq](../../strongs/h/h6002.md): [māḥār](../../strongs/h/h4279.md) I will [nāṣaḇ](../../strongs/h/h5324.md) on the [ro'sh](../../strongs/h/h7218.md) of the [giḇʿâ](../../strongs/h/h1389.md) with the [maṭṭê](../../strongs/h/h4294.md) of ['Elohiym](../../strongs/h/h430.md) in mine [yad](../../strongs/h/h3027.md).

<a name="exodus_17_10"></a>Exodus 17:10

So [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['asah](../../strongs/h/h6213.md) as [Mōshe](../../strongs/h/h4872.md) had ['āmar](../../strongs/h/h559.md) to him, and [lāḥam](../../strongs/h/h3898.md) with [ʿĂmālēq](../../strongs/h/h6002.md): and [Mōshe](../../strongs/h/h4872.md), ['Ahărôn](../../strongs/h/h175.md), and [Ḥûr](../../strongs/h/h2354.md) [ʿālâ](../../strongs/h/h5927.md) to the [ro'sh](../../strongs/h/h7218.md) of the [giḇʿâ](../../strongs/h/h1389.md).

<a name="exodus_17_11"></a>Exodus 17:11

And it came to pass, when [Mōshe](../../strongs/h/h4872.md) he[ruwm](../../strongs/h/h7311.md) his [yad](../../strongs/h/h3027.md), that [Yisra'el](../../strongs/h/h3478.md) [gabar](../../strongs/h/h1396.md): and when he [nuwach](../../strongs/h/h5117.md) his [yad](../../strongs/h/h3027.md), [ʿĂmālēq](../../strongs/h/h6002.md) [gabar](../../strongs/h/h1396.md).

<a name="exodus_17_12"></a>Exodus 17:12

But [Mōshe](../../strongs/h/h4872.md) [yad](../../strongs/h/h3027.md) were [kāḇēḏ](../../strongs/h/h3515.md); and they [laqach](../../strongs/h/h3947.md) an ['eben](../../strongs/h/h68.md), and [śûm](../../strongs/h/h7760.md) it under him, and he [yashab](../../strongs/h/h3427.md) thereon; and ['Ahărôn](../../strongs/h/h175.md) and [Ḥûr](../../strongs/h/h2354.md) [tamak](../../strongs/h/h8551.md) his [yad](../../strongs/h/h3027.md), the ['echad](../../strongs/h/h259.md), and ['echad](../../strongs/h/h259.md); and his [yad](../../strongs/h/h3027.md) were ['ĕmûnâ](../../strongs/h/h530.md) until the [bow'](../../strongs/h/h935.md) of the [šemeš](../../strongs/h/h8121.md).

<a name="exodus_17_13"></a>Exodus 17:13

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [ḥālaš](../../strongs/h/h2522.md) [ʿĂmālēq](../../strongs/h/h6002.md) and his ['am](../../strongs/h/h5971.md) with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md).

<a name="exodus_17_14"></a>Exodus 17:14

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [kāṯaḇ](../../strongs/h/h3789.md) this for a [zikārôn](../../strongs/h/h2146.md) in a [sēp̄er](../../strongs/h/h5612.md), and [śûm](../../strongs/h/h7760.md) it in the ['ozen](../../strongs/h/h241.md) of [Yᵊhôšûaʿ](../../strongs/h/h3091.md): for I will [māḥâ](../../strongs/h/h4229.md) [māḥâ](../../strongs/h/h4229.md) the [zeker](../../strongs/h/h2143.md) of [ʿĂmālēq](../../strongs/h/h6002.md) from under [shamayim](../../strongs/h/h8064.md).

<a name="exodus_17_15"></a>Exodus 17:15

And [Mōshe](../../strongs/h/h4872.md) [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md), and [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of it [Yᵊhōvâ nissî](../../strongs/h/h3071.md):

<a name="exodus_17_16"></a>Exodus 17:16

For he ['āmar](../../strongs/h/h559.md), Because [Yahh](../../strongs/h/h3050.md) hath [yad](../../strongs/h/h3027.md) [kēs](../../strongs/h/h3676.md) that [Yĕhovah](../../strongs/h/h3068.md) will have [milḥāmâ](../../strongs/h/h4421.md) with [ʿĂmālēq](../../strongs/h/h6002.md) from [dôr](../../strongs/h/h1755.md) to [dôr](../../strongs/h/h1755.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 16](exodus_16.md) - [Exodus 18](exodus_18.md)