# [Exodus 26](https://www.blueletterbible.org/kjv/exo/26/1/)

<a name="exodus_26_1"></a>Exodus 26:1

Moreover thou shalt ['asah](../../strongs/h/h6213.md) the [miškān](../../strongs/h/h4908.md) with ten [yᵊrîʿâ](../../strongs/h/h3407.md) of [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md), and [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md): with [kĕruwb](../../strongs/h/h3742.md) of [chashab](../../strongs/h/h2803.md) [ma'aseh](../../strongs/h/h4639.md) shalt thou ['asah](../../strongs/h/h6213.md) them.

<a name="exodus_26_2"></a>Exodus 26:2

The ['ōreḵ](../../strongs/h/h753.md) of one [yᵊrîʿâ](../../strongs/h/h3407.md) shall be eight and twenty ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) of one [yᵊrîʿâ](../../strongs/h/h3407.md) four ['ammâ](../../strongs/h/h520.md): and every one of the [yᵊrîʿâ](../../strongs/h/h3407.md) shall have one [midâ](../../strongs/h/h4060.md).

<a name="exodus_26_3"></a>Exodus 26:3

The five [yᵊrîʿâ](../../strongs/h/h3407.md) shall be [ḥāḇar](../../strongs/h/h2266.md) ['ishshah](../../strongs/h/h802.md) to ['āḥôṯ](../../strongs/h/h269.md); and other five [yᵊrîʿâ](../../strongs/h/h3407.md) shall be [ḥāḇar](../../strongs/h/h2266.md) ['ishshah](../../strongs/h/h802.md) to ['āḥôṯ](../../strongs/h/h269.md). [^1]

<a name="exodus_26_4"></a>Exodus 26:4

And thou shalt ['asah](../../strongs/h/h6213.md) [lûlay](../../strongs/h/h3924.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md) upon the [saphah](../../strongs/h/h8193.md) of the one [yᵊrîʿâ](../../strongs/h/h3407.md) from the [qāṣâ](../../strongs/h/h7098.md) in the [ḥōḇereṯ](../../strongs/h/h2279.md); and likewise shalt thou ['asah](../../strongs/h/h6213.md) in the [qîṣôn](../../strongs/h/h7020.md) [saphah](../../strongs/h/h8193.md) of another [yᵊrîʿâ](../../strongs/h/h3407.md), in the [maḥbereṯ](../../strongs/h/h4225.md) of the second.

<a name="exodus_26_5"></a>Exodus 26:5

Fifty [lûlay](../../strongs/h/h3924.md) shalt thou ['asah](../../strongs/h/h6213.md) in the one [yᵊrîʿâ](../../strongs/h/h3407.md), and fifty [lûlay](../../strongs/h/h3924.md) shalt thou ['asah](../../strongs/h/h6213.md) in the [qāṣê](../../strongs/h/h7097.md) of the [yᵊrîʿâ](../../strongs/h/h3407.md) that is in the [maḥbereṯ](../../strongs/h/h4225.md) of the second; that the [lûlay](../../strongs/h/h3924.md) may [qāḇal](../../strongs/h/h6901.md) ['ishshah](../../strongs/h/h802.md) of ['āḥôṯ](../../strongs/h/h269.md).

<a name="exodus_26_6"></a>Exodus 26:6

And thou shalt ['asah](../../strongs/h/h6213.md) fifty [qeres](../../strongs/h/h7165.md) of [zāhāḇ](../../strongs/h/h2091.md), and [ḥāḇar](../../strongs/h/h2266.md) the [yᵊrîʿâ](../../strongs/h/h3407.md) ['ishshah](../../strongs/h/h802.md) ['āḥôṯ](../../strongs/h/h269.md) the [qeres](../../strongs/h/h7165.md): and it shall be one [miškān](../../strongs/h/h4908.md).

<a name="exodus_26_7"></a>Exodus 26:7

And thou shalt ['asah](../../strongs/h/h6213.md) [yᵊrîʿâ](../../strongs/h/h3407.md) of [ʿēz](../../strongs/h/h5795.md) to be a ['ohel](../../strongs/h/h168.md) upon the [miškān](../../strongs/h/h4908.md): eleven [yᵊrîʿâ](../../strongs/h/h3407.md) shalt thou ['asah](../../strongs/h/h6213.md).

<a name="exodus_26_8"></a>Exodus 26:8

The ['ōreḵ](../../strongs/h/h753.md) of one [yᵊrîʿâ](../../strongs/h/h3407.md) shall be thirty ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) of one [yᵊrîʿâ](../../strongs/h/h3407.md) four ['ammâ](../../strongs/h/h520.md): and the eleven [yᵊrîʿâ](../../strongs/h/h3407.md) shall be all of one [midâ](../../strongs/h/h4060.md).

<a name="exodus_26_9"></a>Exodus 26:9

And thou shalt [ḥāḇar](../../strongs/h/h2266.md) five [yᵊrîʿâ](../../strongs/h/h3407.md) by themselves, and six [yᵊrîʿâ](../../strongs/h/h3407.md) by themselves, and shalt [kāp̄al](../../strongs/h/h3717.md) the sixth [yᵊrîʿâ](../../strongs/h/h3407.md) in the [môl](../../strongs/h/h4136.md) [paniym](../../strongs/h/h6440.md) of the ['ohel](../../strongs/h/h168.md).

<a name="exodus_26_10"></a>Exodus 26:10

And thou shalt ['asah](../../strongs/h/h6213.md) fifty [lûlay](../../strongs/h/h3924.md) on the [saphah](../../strongs/h/h8193.md) of the one [yᵊrîʿâ](../../strongs/h/h3407.md) that is [qîṣôn](../../strongs/h/h7020.md) in the [ḥōḇereṯ](../../strongs/h/h2279.md), and fifty [lûlay](../../strongs/h/h3924.md) in the [saphah](../../strongs/h/h8193.md) of the [yᵊrîʿâ](../../strongs/h/h3407.md) which [ḥōḇereṯ](../../strongs/h/h2279.md) the second.

<a name="exodus_26_11"></a>Exodus 26:11

And thou shalt ['asah](../../strongs/h/h6213.md) fifty [qeres](../../strongs/h/h7165.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md), and [bow'](../../strongs/h/h935.md) the [qeres](../../strongs/h/h7165.md) into the [lûlay](../../strongs/h/h3924.md), and [ḥāḇar](../../strongs/h/h2266.md) the ['ohel](../../strongs/h/h168.md) together, that it may be ['echad](../../strongs/h/h259.md).

<a name="exodus_26_12"></a>Exodus 26:12

And the [seraḥ](../../strongs/h/h5629.md) that [ʿāḏap̄](../../strongs/h/h5736.md) of the [yᵊrîʿâ](../../strongs/h/h3407.md) of the ['ohel](../../strongs/h/h168.md), the half [yᵊrîʿâ](../../strongs/h/h3407.md) that [ʿāḏap̄](../../strongs/h/h5736.md), shall [sāraḥ](../../strongs/h/h5628.md) over the ['āḥôr](../../strongs/h/h268.md) of the [miškān](../../strongs/h/h4908.md).

<a name="exodus_26_13"></a>Exodus 26:13

And an ['ammâ](../../strongs/h/h520.md) on the one side, and an ['ammâ](../../strongs/h/h520.md) on the other side of that which [ʿāḏap̄](../../strongs/h/h5736.md) in the ['ōreḵ](../../strongs/h/h753.md) of the [yᵊrîʿâ](../../strongs/h/h3407.md) of the ['ohel](../../strongs/h/h168.md), it shall [sāraḥ](../../strongs/h/h5628.md) the [ṣaḏ](../../strongs/h/h6654.md) of the [miškān](../../strongs/h/h4908.md) on this side and on that side, to [kāsâ](../../strongs/h/h3680.md) it.

<a name="exodus_26_14"></a>Exodus 26:14

And thou shalt ['asah](../../strongs/h/h6213.md) a [miḵsê](../../strongs/h/h4372.md) for the ['ohel](../../strongs/h/h168.md) of ['ayil](../../strongs/h/h352.md) ['owr](../../strongs/h/h5785.md) ['āḏam](../../strongs/h/h119.md), and a [miḵsê](../../strongs/h/h4372.md) [maʿal](../../strongs/h/h4605.md) of [taḥaš](../../strongs/h/h8476.md) ['owr](../../strongs/h/h5785.md).

<a name="exodus_26_15"></a>Exodus 26:15

And thou shalt ['asah](../../strongs/h/h6213.md) [qereš](../../strongs/h/h7175.md) for the [miškān](../../strongs/h/h4908.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md) ['amad](../../strongs/h/h5975.md).

<a name="exodus_26_16"></a>Exodus 26:16

Ten ['ammâ](../../strongs/h/h520.md) shall be the ['ōreḵ](../../strongs/h/h753.md) of a [qereš](../../strongs/h/h7175.md), and an ['ammâ](../../strongs/h/h520.md) and a half shall be the [rōḥaḇ](../../strongs/h/h7341.md) of one [qereš](../../strongs/h/h7175.md).

<a name="exodus_26_17"></a>Exodus 26:17

Two [yad](../../strongs/h/h3027.md) shall there be in one [qereš](../../strongs/h/h7175.md), [šālaḇ](../../strongs/h/h7947.md) ['ishshah](../../strongs/h/h802.md) ['āḥôṯ](../../strongs/h/h269.md): thus shalt thou ['asah](../../strongs/h/h6213.md) for all the [qereš](../../strongs/h/h7175.md) of the [miškān](../../strongs/h/h4908.md).

<a name="exodus_26_18"></a>Exodus 26:18

And thou shalt ['asah](../../strongs/h/h6213.md) the [qereš](../../strongs/h/h7175.md) for the [miškān](../../strongs/h/h4908.md), twenty [qereš](../../strongs/h/h7175.md) on the [neḡeḇ](../../strongs/h/h5045.md) [pē'â](../../strongs/h/h6285.md) [têmān](../../strongs/h/h8486.md).

<a name="exodus_26_19"></a>Exodus 26:19

And thou shalt ['asah](../../strongs/h/h6213.md) forty ['eḏen](../../strongs/h/h134.md) of [keceph](../../strongs/h/h3701.md) under the twenty [qereš](../../strongs/h/h7175.md); two ['eḏen](../../strongs/h/h134.md) under one [qereš](../../strongs/h/h7175.md) for his two [yad](../../strongs/h/h3027.md), and two ['eḏen](../../strongs/h/h134.md) under another [qereš](../../strongs/h/h7175.md) for his two [yad](../../strongs/h/h3027.md).

<a name="exodus_26_20"></a>Exodus 26:20

And for the second [tsela'](../../strongs/h/h6763.md) of the [miškān](../../strongs/h/h4908.md) on the [ṣāp̄ôn](../../strongs/h/h6828.md) [pē'â](../../strongs/h/h6285.md) there shall be twenty [qereš](../../strongs/h/h7175.md):

<a name="exodus_26_21"></a>Exodus 26:21

And their forty ['eḏen](../../strongs/h/h134.md) of [keceph](../../strongs/h/h3701.md); two ['eḏen](../../strongs/h/h134.md) under one [qereš](../../strongs/h/h7175.md), and two ['eḏen](../../strongs/h/h134.md) under another [qereš](../../strongs/h/h7175.md).

<a name="exodus_26_22"></a>Exodus 26:22

And for the [yᵊrēḵâ](../../strongs/h/h3411.md) of the [miškān](../../strongs/h/h4908.md) [yam](../../strongs/h/h3220.md) thou shalt ['asah](../../strongs/h/h6213.md) six [qereš](../../strongs/h/h7175.md).

<a name="exodus_26_23"></a>Exodus 26:23

And two [qereš](../../strongs/h/h7175.md) shalt thou ['asah](../../strongs/h/h6213.md) for the [maqṣuʿâ](../../strongs/h/h4742.md) of the [miškān](../../strongs/h/h4908.md) in the two [yᵊrēḵâ](../../strongs/h/h3411.md).

<a name="exodus_26_24"></a>Exodus 26:24

And they shall be [tā'am](../../strongs/h/h8382.md) together [maṭṭâ](../../strongs/h/h4295.md), and they shall be [tā'am](../../strongs/h/h8382.md) [yaḥaḏ](../../strongs/h/h3162.md) the [ro'sh](../../strongs/h/h7218.md) of it unto one [ṭabaʿaṯ](../../strongs/h/h2885.md): thus shall it be for them both; they shall be for the two [miqṣôaʿ](../../strongs/h/h4740.md).

<a name="exodus_26_25"></a>Exodus 26:25

And they shall be eight [qereš](../../strongs/h/h7175.md), and their ['eḏen](../../strongs/h/h134.md) of [keceph](../../strongs/h/h3701.md), sixteen ['eḏen](../../strongs/h/h134.md); two ['eḏen](../../strongs/h/h134.md) under one [qereš](../../strongs/h/h7175.md), and two ['eḏen](../../strongs/h/h134.md) under another [qereš](../../strongs/h/h7175.md).

<a name="exodus_26_26"></a>Exodus 26:26

And thou shalt ['asah](../../strongs/h/h6213.md) [bᵊrîaḥ](../../strongs/h/h1280.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md); five for the [qereš](../../strongs/h/h7175.md) of the one [tsela'](../../strongs/h/h6763.md) of the [miškān](../../strongs/h/h4908.md),

<a name="exodus_26_27"></a>Exodus 26:27

And five [bᵊrîaḥ](../../strongs/h/h1280.md) for the [qereš](../../strongs/h/h7175.md) of the other [tsela'](../../strongs/h/h6763.md) of the [miškān](../../strongs/h/h4908.md), and five [bᵊrîaḥ](../../strongs/h/h1280.md) for the [qereš](../../strongs/h/h7175.md) of the [tsela'](../../strongs/h/h6763.md) of the [miškān](../../strongs/h/h4908.md), for the [yᵊrēḵâ](../../strongs/h/h3411.md) [yam](../../strongs/h/h3220.md).

<a name="exodus_26_28"></a>Exodus 26:28

And the [tîḵôn](../../strongs/h/h8484.md) [bᵊrîaḥ](../../strongs/h/h1280.md) in the [tavek](../../strongs/h/h8432.md) of the [qereš](../../strongs/h/h7175.md) shall [bāraḥ](../../strongs/h/h1272.md) from [qāṣê](../../strongs/h/h7097.md) to [qāṣê](../../strongs/h/h7097.md).

<a name="exodus_26_29"></a>Exodus 26:29

And thou shalt [ṣāp̄â](../../strongs/h/h6823.md) the [qereš](../../strongs/h/h7175.md) with [zāhāḇ](../../strongs/h/h2091.md), and ['asah](../../strongs/h/h6213.md) their [ṭabaʿaṯ](../../strongs/h/h2885.md) of [zāhāḇ](../../strongs/h/h2091.md) for [bayith](../../strongs/h/h1004.md) for the [bᵊrîaḥ](../../strongs/h/h1280.md): and thou shalt [ṣāp̄â](../../strongs/h/h6823.md) the [bᵊrîaḥ](../../strongs/h/h1280.md) with [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_26_30"></a>Exodus 26:30

And thou shalt [quwm](../../strongs/h/h6965.md) the [miškān](../../strongs/h/h4908.md) according to the [mishpat](../../strongs/h/h4941.md) thereof which was [ra'ah](../../strongs/h/h7200.md) thee in the [har](../../strongs/h/h2022.md).

<a name="exodus_26_31"></a>Exodus 26:31

And thou shalt ['asah](../../strongs/h/h6213.md) a [pārōḵeṯ](../../strongs/h/h6532.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md) of [chashab](../../strongs/h/h2803.md) [ma'aseh](../../strongs/h/h4639.md): with [kĕruwb](../../strongs/h/h3742.md) shall it be ['asah](../../strongs/h/h6213.md):

<a name="exodus_26_32"></a>Exodus 26:32

And thou shalt [nathan](../../strongs/h/h5414.md) it upon four [ʿammûḏ](../../strongs/h/h5982.md) of [šiṭṭâ](../../strongs/h/h7848.md) [ṣāp̄â](../../strongs/h/h6823.md) with [zāhāḇ](../../strongs/h/h2091.md): their [vāv](../../strongs/h/h2053.md) shall be of [zāhāḇ](../../strongs/h/h2091.md), upon the four ['eḏen](../../strongs/h/h134.md) of [keceph](../../strongs/h/h3701.md).

<a name="exodus_26_33"></a>Exodus 26:33

And thou shalt [nathan](../../strongs/h/h5414.md) the [pārōḵeṯ](../../strongs/h/h6532.md) under the [qeres](../../strongs/h/h7165.md), that thou mayest [bow'](../../strongs/h/h935.md) in thither [bayith](../../strongs/h/h1004.md) the [pārōḵeṯ](../../strongs/h/h6532.md) the ['ārôn](../../strongs/h/h727.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md): and the [pārōḵeṯ](../../strongs/h/h6532.md) shall [bāḏal](../../strongs/h/h914.md) unto you between the [qodesh](../../strongs/h/h6944.md) and the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md).

<a name="exodus_26_34"></a>Exodus 26:34

And thou shalt [nathan](../../strongs/h/h5414.md) the [kapōreṯ](../../strongs/h/h3727.md) upon the ['ārôn](../../strongs/h/h727.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md) in the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md).

<a name="exodus_26_35"></a>Exodus 26:35

And thou shalt [śûm](../../strongs/h/h7760.md) the [šulḥān](../../strongs/h/h7979.md) [ḥûṣ](../../strongs/h/h2351.md) the [pārōḵeṯ](../../strongs/h/h6532.md), and the [mᵊnôrâ](../../strongs/h/h4501.md) over against the [šulḥān](../../strongs/h/h7979.md) on the [tsela'](../../strongs/h/h6763.md) of the [miškān](../../strongs/h/h4908.md) toward the [têmān](../../strongs/h/h8486.md): and thou shalt [nathan](../../strongs/h/h5414.md) the [šulḥān](../../strongs/h/h7979.md) on the [ṣāp̄ôn](../../strongs/h/h6828.md) [tsela'](../../strongs/h/h6763.md).

<a name="exodus_26_36"></a>Exodus 26:36

And thou shalt ['asah](../../strongs/h/h6213.md) a [māsāḵ](../../strongs/h/h4539.md) for the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md), of [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [tôlāʿ](../../strongs/h/h8438.md) [šānî](../../strongs/h/h8144.md), and [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md), [ma'aseh](../../strongs/h/h4639.md) with [rāqam](../../strongs/h/h7551.md).

<a name="exodus_26_37"></a>Exodus 26:37

And thou shalt ['asah](../../strongs/h/h6213.md) for the [māsāḵ](../../strongs/h/h4539.md) five [ʿammûḏ](../../strongs/h/h5982.md) of [šiṭṭâ](../../strongs/h/h7848.md), and [ṣāp̄â](../../strongs/h/h6823.md) them with [zāhāḇ](../../strongs/h/h2091.md), and their [vāv](../../strongs/h/h2053.md) shall be of [zāhāḇ](../../strongs/h/h2091.md): and thou shalt [yāṣaq](../../strongs/h/h3332.md) five ['eḏen](../../strongs/h/h134.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) for them.

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 25](exodus_25.md) - [Exodus 27](exodus_27.md)

---

[^1]: [Exodus 26:3 Commentary](../../commentary/exodus/exodus_26_commentary.md#exodus_26_3)