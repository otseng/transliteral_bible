# [Exodus 36](https://www.blueletterbible.org/kjv/exo/36/1)

<a name="exodus_36_1"></a>Exodus 36:1

Then ['asah](../../strongs/h/h6213.md) [Bᵊṣal'ēl](../../strongs/h/h1212.md) and ['āhŏlî'āḇ](../../strongs/h/h171.md), and every [ḥāḵām](../../strongs/h/h2450.md) [leb](../../strongs/h/h3820.md) ['iysh](../../strongs/h/h376.md), in whom [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) [ḥāḵmâ](../../strongs/h/h2451.md) and [tāḇûn](../../strongs/h/h8394.md) to [yada'](../../strongs/h/h3045.md) how to ['asah](../../strongs/h/h6213.md) all manner of [mĕla'kah](../../strongs/h/h4399.md) for the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [qodesh](../../strongs/h/h6944.md), according to all that [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md).

<a name="exodus_36_2"></a>Exodus 36:2

And [Mōshe](../../strongs/h/h4872.md) [qara'](../../strongs/h/h7121.md) [Bᵊṣal'ēl](../../strongs/h/h1212.md) and ['āhŏlî'āḇ](../../strongs/h/h171.md), and every [ḥāḵām](../../strongs/h/h2450.md) [leb](../../strongs/h/h3820.md) ['iysh](../../strongs/h/h376.md), in whose [leb](../../strongs/h/h3820.md) [Yĕhovah](../../strongs/h/h3068.md) had [nathan](../../strongs/h/h5414.md) [ḥāḵmâ](../../strongs/h/h2451.md), even every one whose [leb](../../strongs/h/h3820.md) [nasa'](../../strongs/h/h5375.md) him to [qāraḇ](../../strongs/h/h7126.md) unto the [mĕla'kah](../../strongs/h/h4399.md) to ['asah](../../strongs/h/h6213.md) it:

<a name="exodus_36_3"></a>Exodus 36:3

And they [laqach](../../strongs/h/h3947.md) [paniym](../../strongs/h/h6440.md) [Mōshe](../../strongs/h/h4872.md) all the [tᵊrûmâ](../../strongs/h/h8641.md), which the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) had [bow'](../../strongs/h/h935.md) for the [mĕla'kah](../../strongs/h/h4399.md) of the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [qodesh](../../strongs/h/h6944.md), to ['asah](../../strongs/h/h6213.md) it withal. And they [bow'](../../strongs/h/h935.md) yet unto him [nᵊḏāḇâ](../../strongs/h/h5071.md) every [boqer](../../strongs/h/h1242.md).

<a name="exodus_36_4"></a>Exodus 36:4

And all the [ḥāḵām](../../strongs/h/h2450.md), that ['asah](../../strongs/h/h6213.md) all the [mĕla'kah](../../strongs/h/h4399.md) of the [qodesh](../../strongs/h/h6944.md), [bow'](../../strongs/h/h935.md) every ['iysh](../../strongs/h/h376.md) from his [mĕla'kah](../../strongs/h/h4399.md) which they ['asah](../../strongs/h/h6213.md);

<a name="exodus_36_5"></a>Exodus 36:5

And they ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md), The ['am](../../strongs/h/h5971.md) [bow'](../../strongs/h/h935.md) much [rabah](../../strongs/h/h7235.md) than [day](../../strongs/h/h1767.md) for the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [mĕla'kah](../../strongs/h/h4399.md), which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) to ['asah](../../strongs/h/h6213.md).

<a name="exodus_36_6"></a>Exodus 36:6

And [Mōshe](../../strongs/h/h4872.md) gave [tsavah](../../strongs/h/h6680.md), and they [qowl](../../strongs/h/h6963.md) ['abar](../../strongs/h/h5674.md) throughout the [maḥănê](../../strongs/h/h4264.md), ['āmar](../../strongs/h/h559.md), Let neither ['iysh](../../strongs/h/h376.md) nor ['ishshah](../../strongs/h/h802.md) ['asah](../../strongs/h/h6213.md) any more [mĕla'kah](../../strongs/h/h4399.md) for the [tᵊrûmâ](../../strongs/h/h8641.md) of the [qodesh](../../strongs/h/h6944.md). So the ['am](../../strongs/h/h5971.md) were [kālā'](../../strongs/h/h3607.md) from [bow'](../../strongs/h/h935.md).

<a name="exodus_36_7"></a>Exodus 36:7

For the [mĕla'kah](../../strongs/h/h4399.md) they had was [day](../../strongs/h/h1767.md) for all the [mĕla'kah](../../strongs/h/h4399.md) to ['asah](../../strongs/h/h6213.md) it, and [yāṯar](../../strongs/h/h3498.md).

<a name="exodus_36_8"></a>Exodus 36:8

And every [ḥāḵām](../../strongs/h/h2450.md) [leb](../../strongs/h/h3820.md) among them that ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md) of the [miškān](../../strongs/h/h4908.md) ['asah](../../strongs/h/h6213.md) ten [yᵊrîʿâ](../../strongs/h/h3407.md) of [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md), and [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md): with [kĕruwb](../../strongs/h/h3742.md) of [chashab](../../strongs/h/h2803.md) [ma'aseh](../../strongs/h/h4639.md) ['asah](../../strongs/h/h6213.md) he them.

<a name="exodus_36_9"></a>Exodus 36:9

The ['ōreḵ](../../strongs/h/h753.md) of one [yᵊrîʿâ](../../strongs/h/h3407.md) was twenty and eight ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) of one [yᵊrîʿâ](../../strongs/h/h3407.md) four ['ammâ](../../strongs/h/h520.md): the [yᵊrîʿâ](../../strongs/h/h3407.md) were all of one [midâ](../../strongs/h/h4060.md).

<a name="exodus_36_10"></a>Exodus 36:10

And he [ḥāḇar](../../strongs/h/h2266.md) the five [yᵊrîʿâ](../../strongs/h/h3407.md) one unto another: and the other five [yᵊrîʿâ](../../strongs/h/h3407.md) he [ḥāḇar](../../strongs/h/h2266.md) one unto another.

<a name="exodus_36_11"></a>Exodus 36:11

And he ['asah](../../strongs/h/h6213.md) [lûlay](../../strongs/h/h3924.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md) on the [saphah](../../strongs/h/h8193.md) of one [yᵊrîʿâ](../../strongs/h/h3407.md) from the [qāṣâ](../../strongs/h/h7098.md) in the [maḥbereṯ](../../strongs/h/h4225.md): likewise he ['asah](../../strongs/h/h6213.md) in the [qîṣôn](../../strongs/h/h7020.md) [saphah](../../strongs/h/h8193.md) of another [yᵊrîʿâ](../../strongs/h/h3407.md), in the [maḥbereṯ](../../strongs/h/h4225.md) of the second.

<a name="exodus_36_12"></a>Exodus 36:12

Fifty [lûlay](../../strongs/h/h3924.md) ['asah](../../strongs/h/h6213.md) he in one [yᵊrîʿâ](../../strongs/h/h3407.md), and fifty [lûlay](../../strongs/h/h3924.md) ['asah](../../strongs/h/h6213.md) he in the [qāṣê](../../strongs/h/h7097.md) of the [yᵊrîʿâ](../../strongs/h/h3407.md) which was in the [maḥbereṯ](../../strongs/h/h4225.md) of the second: the [lûlay](../../strongs/h/h3924.md) [qāḇal](../../strongs/h/h6901.md) one to another.

<a name="exodus_36_13"></a>Exodus 36:13

And he ['asah](../../strongs/h/h6213.md) fifty [qeres](../../strongs/h/h7165.md) of [zāhāḇ](../../strongs/h/h2091.md), and [ḥāḇar](../../strongs/h/h2266.md) the [yᵊrîʿâ](../../strongs/h/h3407.md) one unto another with the [qeres](../../strongs/h/h7165.md): so it became one [miškān](../../strongs/h/h4908.md).

<a name="exodus_36_14"></a>Exodus 36:14

And he ['asah](../../strongs/h/h6213.md) [yᵊrîʿâ](../../strongs/h/h3407.md) of [ʿēz](../../strongs/h/h5795.md) hair for the ['ohel](../../strongs/h/h168.md) over the [miškān](../../strongs/h/h4908.md): eleven [yᵊrîʿâ](../../strongs/h/h3407.md) he ['asah](../../strongs/h/h6213.md) them.

<a name="exodus_36_15"></a>Exodus 36:15

The ['ōreḵ](../../strongs/h/h753.md) of one [yᵊrîʿâ](../../strongs/h/h3407.md) was thirty ['ammâ](../../strongs/h/h520.md), and four ['ammâ](../../strongs/h/h520.md) was the [rōḥaḇ](../../strongs/h/h7341.md) of one [yᵊrîʿâ](../../strongs/h/h3407.md): the eleven [yᵊrîʿâ](../../strongs/h/h3407.md) were of one [midâ](../../strongs/h/h4060.md).

<a name="exodus_36_16"></a>Exodus 36:16

And he [ḥāḇar](../../strongs/h/h2266.md) five [yᵊrîʿâ](../../strongs/h/h3407.md) by themselves, and six [yᵊrîʿâ](../../strongs/h/h3407.md) by themselves.

<a name="exodus_36_17"></a>Exodus 36:17

And he ['asah](../../strongs/h/h6213.md) fifty [lûlay](../../strongs/h/h3924.md) upon the [qîṣôn](../../strongs/h/h7020.md) [saphah](../../strongs/h/h8193.md) of the [yᵊrîʿâ](../../strongs/h/h3407.md) in the [maḥbereṯ](../../strongs/h/h4225.md), and fifty [lûlay](../../strongs/h/h3924.md) ['asah](../../strongs/h/h6213.md) he upon the [saphah](../../strongs/h/h8193.md) of the [yᵊrîʿâ](../../strongs/h/h3407.md) which [ḥōḇereṯ](../../strongs/h/h2279.md) the second.

<a name="exodus_36_18"></a>Exodus 36:18

And he ['asah](../../strongs/h/h6213.md) fifty [qeres](../../strongs/h/h7165.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) to [ḥāḇar](../../strongs/h/h2266.md) the ['ohel](../../strongs/h/h168.md) [ḥāḇar](../../strongs/h/h2266.md), that it might be one.

<a name="exodus_36_19"></a>Exodus 36:19

And he ['asah](../../strongs/h/h6213.md) a [miḵsê](../../strongs/h/h4372.md) for the ['ohel](../../strongs/h/h168.md) of ['ayil](../../strongs/h/h352.md) ['owr](../../strongs/h/h5785.md) ['āḏam](../../strongs/h/h119.md), and a [miḵsê](../../strongs/h/h4372.md) of [taḥaš](../../strongs/h/h8476.md) ['owr](../../strongs/h/h5785.md) [maʿal](../../strongs/h/h4605.md) that.

<a name="exodus_36_20"></a>Exodus 36:20

And he ['asah](../../strongs/h/h6213.md) [qereš](../../strongs/h/h7175.md) for the [miškān](../../strongs/h/h4908.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md), ['amad](../../strongs/h/h5975.md).

<a name="exodus_36_21"></a>Exodus 36:21

The ['ōreḵ](../../strongs/h/h753.md) of a [qereš](../../strongs/h/h7175.md) was ten ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) of a [qereš](../../strongs/h/h7175.md) one ['ammâ](../../strongs/h/h520.md) and a half.

<a name="exodus_36_22"></a>Exodus 36:22

One [qereš](../../strongs/h/h7175.md) had two [yad](../../strongs/h/h3027.md), [šālaḇ](../../strongs/h/h7947.md) one from another: thus did he ['asah](../../strongs/h/h6213.md) for all the [qereš](../../strongs/h/h7175.md) of the [miškān](../../strongs/h/h4908.md).

<a name="exodus_36_23"></a>Exodus 36:23

And he ['asah](../../strongs/h/h6213.md) [qereš](../../strongs/h/h7175.md) for the [miškān](../../strongs/h/h4908.md); twenty [qereš](../../strongs/h/h7175.md) for the [neḡeḇ](../../strongs/h/h5045.md) [pē'â](../../strongs/h/h6285.md) [têmān](../../strongs/h/h8486.md):

<a name="exodus_36_24"></a>Exodus 36:24

And forty ['eḏen](../../strongs/h/h134.md) of [keceph](../../strongs/h/h3701.md) he ['asah](../../strongs/h/h6213.md) under the twenty [qereš](../../strongs/h/h7175.md); two ['eḏen](../../strongs/h/h134.md) under one [qereš](../../strongs/h/h7175.md) for his two [yad](../../strongs/h/h3027.md), and two ['eḏen](../../strongs/h/h134.md) under another [qereš](../../strongs/h/h7175.md) for his two [yad](../../strongs/h/h3027.md).

<a name="exodus_36_25"></a>Exodus 36:25

And for the other [tsela'](../../strongs/h/h6763.md) of the [miškān](../../strongs/h/h4908.md), which is toward the [ṣāp̄ôn](../../strongs/h/h6828.md) [pē'â](../../strongs/h/h6285.md), he ['asah](../../strongs/h/h6213.md) twenty [qereš](../../strongs/h/h7175.md),

<a name="exodus_36_26"></a>Exodus 36:26

And their forty ['eḏen](../../strongs/h/h134.md) of [keceph](../../strongs/h/h3701.md); two ['eḏen](../../strongs/h/h134.md) under one [qereš](../../strongs/h/h7175.md), and two ['eḏen](../../strongs/h/h134.md) under another [qereš](../../strongs/h/h7175.md).

<a name="exodus_36_27"></a>Exodus 36:27

And for the [yᵊrēḵâ](../../strongs/h/h3411.md) of the [miškān](../../strongs/h/h4908.md) [yam](../../strongs/h/h3220.md) he ['asah](../../strongs/h/h6213.md) six [qereš](../../strongs/h/h7175.md).

<a name="exodus_36_28"></a>Exodus 36:28

And two [qereš](../../strongs/h/h7175.md) ['asah](../../strongs/h/h6213.md) he for the [maqṣuʿâ](../../strongs/h/h4742.md) of the [miškān](../../strongs/h/h4908.md) in the two [yᵊrēḵâ](../../strongs/h/h3411.md).

<a name="exodus_36_29"></a>Exodus 36:29

And they were [tā'am](../../strongs/h/h8382.md) [maṭṭâ](../../strongs/h/h4295.md), and [tā'am](../../strongs/h/h8382.md) [yaḥaḏ](../../strongs/h/h3162.md) at the [ro'sh](../../strongs/h/h7218.md) thereof, to one [ṭabaʿaṯ](../../strongs/h/h2885.md): thus he ['asah](../../strongs/h/h6213.md) to both of them in both the [miqṣôaʿ](../../strongs/h/h4740.md).

<a name="exodus_36_30"></a>Exodus 36:30

And there were eight [qereš](../../strongs/h/h7175.md); and their ['eḏen](../../strongs/h/h134.md) were sixteen ['eḏen](../../strongs/h/h134.md) of [keceph](../../strongs/h/h3701.md), under every [qereš](../../strongs/h/h7175.md) two ['eḏen](../../strongs/h/h134.md).

<a name="exodus_36_31"></a>Exodus 36:31

And he ['asah](../../strongs/h/h6213.md) [bᵊrîaḥ](../../strongs/h/h1280.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md); five for the [qereš](../../strongs/h/h7175.md) of the one [tsela'](../../strongs/h/h6763.md) of the [miškān](../../strongs/h/h4908.md),

<a name="exodus_36_32"></a>Exodus 36:32

And five [bᵊrîaḥ](../../strongs/h/h1280.md) for the [qereš](../../strongs/h/h7175.md) of the other [tsela'](../../strongs/h/h6763.md) of the [miškān](../../strongs/h/h4908.md), and five [bᵊrîaḥ](../../strongs/h/h1280.md) for the [qereš](../../strongs/h/h7175.md) of the [miškān](../../strongs/h/h4908.md) for the [yᵊrēḵâ](../../strongs/h/h3411.md) [yam](../../strongs/h/h3220.md).

<a name="exodus_36_33"></a>Exodus 36:33

And he ['asah](../../strongs/h/h6213.md) the [tîḵôn](../../strongs/h/h8484.md) [bᵊrîaḥ](../../strongs/h/h1280.md) to [bāraḥ](../../strongs/h/h1272.md) [tavek](../../strongs/h/h8432.md) the [qereš](../../strongs/h/h7175.md) from the [qāṣê](../../strongs/h/h7097.md) to the [qāṣê](../../strongs/h/h7097.md).

<a name="exodus_36_34"></a>Exodus 36:34

And he [ṣāp̄â](../../strongs/h/h6823.md) the [qereš](../../strongs/h/h7175.md) with [zāhāḇ](../../strongs/h/h2091.md), and ['asah](../../strongs/h/h6213.md) their [ṭabaʿaṯ](../../strongs/h/h2885.md) of [zāhāḇ](../../strongs/h/h2091.md) to be [bayith](../../strongs/h/h1004.md) for the [bᵊrîaḥ](../../strongs/h/h1280.md), and [ṣāp̄â](../../strongs/h/h6823.md) the [bᵊrîaḥ](../../strongs/h/h1280.md) with [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_36_35"></a>Exodus 36:35

And he ['asah](../../strongs/h/h6213.md) a [pārōḵeṯ](../../strongs/h/h6532.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md): with [kĕruwb](../../strongs/h/h3742.md) ['asah](../../strongs/h/h6213.md) he it of [chashab](../../strongs/h/h2803.md) [ma'aseh](../../strongs/h/h4639.md).

<a name="exodus_36_36"></a>Exodus 36:36

And he ['asah](../../strongs/h/h6213.md) thereunto four [ʿammûḏ](../../strongs/h/h5982.md) of [šiṭṭâ](../../strongs/h/h7848.md), and [ṣāp̄â](../../strongs/h/h6823.md) them with [zāhāḇ](../../strongs/h/h2091.md): their [vāv](../../strongs/h/h2053.md) were of [zāhāḇ](../../strongs/h/h2091.md); and he [yāṣaq](../../strongs/h/h3332.md) for them four ['eḏen](../../strongs/h/h134.md) of [keceph](../../strongs/h/h3701.md).

<a name="exodus_36_37"></a>Exodus 36:37

And he ['asah](../../strongs/h/h6213.md) a [māsāḵ](../../strongs/h/h4539.md) for the ['ohel](../../strongs/h/h168.md) [peṯaḥ](../../strongs/h/h6607.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md), of [rāqam](../../strongs/h/h7551.md) [ma'aseh](../../strongs/h/h4639.md);

<a name="exodus_36_38"></a>Exodus 36:38

And the five [ʿammûḏ](../../strongs/h/h5982.md) of it with their [vāv](../../strongs/h/h2053.md): and he [ṣāp̄â](../../strongs/h/h6823.md) their [ro'sh](../../strongs/h/h7218.md) and their [ḥăšûqîm](../../strongs/h/h2838.md) with [zāhāḇ](../../strongs/h/h2091.md): but their five ['eḏen](../../strongs/h/h134.md) were of [nᵊḥšeṯ](../../strongs/h/h5178.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 35](exodus_35.md) - [Exodus 37](exodus_37.md)