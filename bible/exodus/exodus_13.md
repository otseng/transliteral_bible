# [Exodus 13](https://www.blueletterbible.org/kjv/exo/13/1/s_63001)

<a name="exodus_13_1"></a>Exodus 13:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_13_2"></a>Exodus 13:2

[qadash](../../strongs/h/h6942.md) unto me all the [bᵊḵôr](../../strongs/h/h1060.md), whatsoever [peṭer](../../strongs/h/h6363.md) the [reḥem](../../strongs/h/h7358.md) among the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), both of ['adam](../../strongs/h/h120.md) and of [bĕhemah](../../strongs/h/h929.md): it is mine.

<a name="exodus_13_3"></a>Exodus 13:3

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), [zakar](../../strongs/h/h2142.md) this [yowm](../../strongs/h/h3117.md), in which ye [yāṣā'](../../strongs/h/h3318.md) from [Mitsrayim](../../strongs/h/h4714.md), out of the [bayith](../../strongs/h/h1004.md) of ['ebed](../../strongs/h/h5650.md); for by [ḥōzeq](../../strongs/h/h2392.md) of [yad](../../strongs/h/h3027.md) [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) you out: there shall no [ḥāmēṣ](../../strongs/h/h2557.md) be ['akal](../../strongs/h/h398.md).

<a name="exodus_13_4"></a>Exodus 13:4

This [yowm](../../strongs/h/h3117.md) [yāṣā'](../../strongs/h/h3318.md) ye out in the [ḥōḏeš](../../strongs/h/h2320.md) ['āḇîḇ](../../strongs/h/h24.md).

<a name="exodus_13_5"></a>Exodus 13:5

And it shall be when [Yĕhovah](../../strongs/h/h3068.md) shall [bow'](../../strongs/h/h935.md) thee into the ['erets](../../strongs/h/h776.md) of the [Kᵊnaʿănî](../../strongs/h/h3669.md), and the [Ḥitî](../../strongs/h/h2850.md), and the ['Ĕmōrî](../../strongs/h/h567.md), and the [Ḥiûî](../../strongs/h/h2340.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md), which he [shaba'](../../strongs/h/h7650.md) unto thy ['ab](../../strongs/h/h1.md) to [nathan](../../strongs/h/h5414.md) thee, an ['erets](../../strongs/h/h776.md) [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md), that thou shalt ['abad](../../strongs/h/h5647.md) this [ʿăḇōḏâ](../../strongs/h/h5656.md) in this [ḥōḏeš](../../strongs/h/h2320.md).

<a name="exodus_13_6"></a>Exodus 13:6

[šeḇaʿ](../../strongs/h/h7651.md) [yowm](../../strongs/h/h3117.md) thou shalt ['akal](../../strongs/h/h398.md) [maṣṣâ](../../strongs/h/h4682.md), and in the [šᵊḇîʿî](../../strongs/h/h7637.md) [yowm](../../strongs/h/h3117.md) shall be a [ḥāḡ](../../strongs/h/h2282.md) to [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_13_7"></a>Exodus 13:7

[maṣṣâ](../../strongs/h/h4682.md) shall be ['akal](../../strongs/h/h398.md) [šeḇaʿ](../../strongs/h/h7651.md) [yowm](../../strongs/h/h3117.md); and there shall no [ḥāmēṣ](../../strongs/h/h2557.md) be [ra'ah](../../strongs/h/h7200.md) with thee, neither shall there be [śᵊ'ōr](../../strongs/h/h7603.md) [ra'ah](../../strongs/h/h7200.md) with thee in all thy [gᵊḇûl](../../strongs/h/h1366.md).

<a name="exodus_13_8"></a>Exodus 13:8

And thou shalt [nāḡaḏ](../../strongs/h/h5046.md) thy [ben](../../strongs/h/h1121.md) in that [yowm](../../strongs/h/h3117.md), ['āmar](../../strongs/h/h559.md), This is done because of that which [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) unto me when I [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_13_9"></a>Exodus 13:9

And it shall be for an ['ôṯ](../../strongs/h/h226.md) unto thee upon thine [yad](../../strongs/h/h3027.md), and for a [zikārôn](../../strongs/h/h2146.md) between thine ['ayin](../../strongs/h/h5869.md), that [Yĕhovah](../../strongs/h/h3068.md) [towrah](../../strongs/h/h8451.md) may be in thy [peh](../../strongs/h/h6310.md): for with a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md) hath [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) thee out of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_13_10"></a>Exodus 13:10

Thou shalt therefore [shamar](../../strongs/h/h8104.md) this [chuqqah](../../strongs/h/h2708.md) in his [môʿēḏ](../../strongs/h/h4150.md) from [yowm](../../strongs/h/h3117.md) to [yowm](../../strongs/h/h3117.md).

<a name="exodus_13_11"></a>Exodus 13:11

And it shall be when [Yĕhovah](../../strongs/h/h3068.md) shall [bow'](../../strongs/h/h935.md) thee into the ['erets](../../strongs/h/h776.md) of the [Kᵊnaʿănî](../../strongs/h/h3669.md), as he [shaba'](../../strongs/h/h7650.md) unto thee and to thy ['ab](../../strongs/h/h1.md), and shall [nathan](../../strongs/h/h5414.md) it thee,

<a name="exodus_13_12"></a>Exodus 13:12

That thou shalt ['abar](../../strongs/h/h5674.md) unto [Yĕhovah](../../strongs/h/h3068.md) all that [peṭer](../../strongs/h/h6363.md) the [reḥem](../../strongs/h/h7358.md), and every [peṭer](../../strongs/h/h6363.md) that [šeḡer](../../strongs/h/h7698.md) of a [bĕhemah](../../strongs/h/h929.md) which thou hast; the [zāḵār](../../strongs/h/h2145.md) shall be [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_13_13"></a>Exodus 13:13

And every [peṭer](../../strongs/h/h6363.md) of a [chamowr](../../strongs/h/h2543.md) thou shalt [pāḏâ](../../strongs/h/h6299.md) with a [śê](../../strongs/h/h7716.md); and if thou wilt not [pāḏâ](../../strongs/h/h6299.md) it, then thou shalt [ʿārap̄](../../strongs/h/h6202.md): and all the [bᵊḵôr](../../strongs/h/h1060.md) of ['adam](../../strongs/h/h120.md) among thy [ben](../../strongs/h/h1121.md) shalt thou [pāḏâ](../../strongs/h/h6299.md).

<a name="exodus_13_14"></a>Exodus 13:14

And it shall be when thy [ben](../../strongs/h/h1121.md) [sha'al](../../strongs/h/h7592.md) thee in [māḥār](../../strongs/h/h4279.md), ['āmar](../../strongs/h/h559.md), What is this? that thou shalt ['āmar](../../strongs/h/h559.md) unto him, By [ḥōzeq](../../strongs/h/h2392.md) of [yad](../../strongs/h/h3027.md) [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) us out from [Mitsrayim](../../strongs/h/h4714.md), from the [bayith](../../strongs/h/h1004.md) of ['ebed](../../strongs/h/h5650.md):

<a name="exodus_13_15"></a>Exodus 13:15

And it came to pass, when [Parʿô](../../strongs/h/h6547.md) would [qāšâ](../../strongs/h/h7185.md) let us [shalach](../../strongs/h/h7971.md), that [Yĕhovah](../../strongs/h/h3068.md) [harag](../../strongs/h/h2026.md) all the [bᵊḵôr](../../strongs/h/h1060.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), both the [bᵊḵôr](../../strongs/h/h1060.md) of ['adam](../../strongs/h/h120.md), and the [bᵊḵôr](../../strongs/h/h1060.md) of [bĕhemah](../../strongs/h/h929.md): therefore I [zabach](../../strongs/h/h2076.md) to [Yĕhovah](../../strongs/h/h3068.md) all that [peṭer](../../strongs/h/h6363.md) the [reḥem](../../strongs/h/h7358.md), being [zāḵār](../../strongs/h/h2145.md); but all the [bᵊḵôr](../../strongs/h/h1060.md) of my [ben](../../strongs/h/h1121.md) I [pāḏâ](../../strongs/h/h6299.md).

<a name="exodus_13_16"></a>Exodus 13:16

And it shall be for an ['ôṯ](../../strongs/h/h226.md) upon thine [yad](../../strongs/h/h3027.md), and for [ṭôṭāp̄ôṯ](../../strongs/h/h2903.md) between thine ['ayin](../../strongs/h/h5869.md): for by [ḥōzeq](../../strongs/h/h2392.md) of [yad](../../strongs/h/h3027.md) [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) us forth out of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_13_17"></a>Exodus 13:17

And it came to pass, when [Parʿô](../../strongs/h/h6547.md) had let the ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), that ['Elohiym](../../strongs/h/h430.md) [nachah](../../strongs/h/h5148.md) them not through the [derek](../../strongs/h/h1870.md) of the ['erets](../../strongs/h/h776.md) of the [Pᵊlištî](../../strongs/h/h6430.md), although that was [qarowb](../../strongs/h/h7138.md); for ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), Lest peradventure the ['am](../../strongs/h/h5971.md) [nacham](../../strongs/h/h5162.md) when they [ra'ah](../../strongs/h/h7200.md) [milḥāmâ](../../strongs/h/h4421.md), and they [shuwb](../../strongs/h/h7725.md) to [Mitsrayim](../../strongs/h/h4714.md):

<a name="exodus_13_18"></a>Exodus 13:18

But ['Elohiym](../../strongs/h/h430.md) [cabab](../../strongs/h/h5437.md) the ['am](../../strongs/h/h5971.md), through the [derek](../../strongs/h/h1870.md) of the [midbar](../../strongs/h/h4057.md) of the [Sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md): and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md) [ḥāmaš](../../strongs/h/h2571.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_13_19"></a>Exodus 13:19

And [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) the ['etsem](../../strongs/h/h6106.md) of [Yôsēp̄](../../strongs/h/h3130.md) with him: for he had [shaba'](../../strongs/h/h7650.md) [shaba'](../../strongs/h/h7650.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) will [paqad](../../strongs/h/h6485.md) [paqad](../../strongs/h/h6485.md) you; and ye shall [ʿālâ](../../strongs/h/h5927.md) my ['etsem](../../strongs/h/h6106.md) away hence with you.

<a name="exodus_13_20"></a>Exodus 13:20

And they took their [nāsaʿ](../../strongs/h/h5265.md) from [Sukôṯ](../../strongs/h/h5523.md), and [ḥānâ](../../strongs/h/h2583.md) in ['Ēṯām](../../strongs/h/h864.md), in the [qāṣê](../../strongs/h/h7097.md) of the [midbar](../../strongs/h/h4057.md).

<a name="exodus_13_21"></a>Exodus 13:21

And [Yĕhovah](../../strongs/h/h3068.md) [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) them by [yômām](../../strongs/h/h3119.md) in a [ʿammûḏ](../../strongs/h/h5982.md) of a [ʿānān](../../strongs/h/h6051.md), to [nachah](../../strongs/h/h5148.md) them the [derek](../../strongs/h/h1870.md); and by [layil](../../strongs/h/h3915.md) in a [ʿammûḏ](../../strongs/h/h5982.md) of ['esh](../../strongs/h/h784.md), to give them ['owr](../../strongs/h/h215.md); to [yālaḵ](../../strongs/h/h3212.md) by [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md):

<a name="exodus_13_22"></a>Exodus 13:22

He [mûš](../../strongs/h/h4185.md) not the [ʿammûḏ](../../strongs/h/h5982.md) of the [ʿānān](../../strongs/h/h6051.md) by [yômām](../../strongs/h/h3119.md), nor the [ʿammûḏ](../../strongs/h/h5982.md) of ['esh](../../strongs/h/h784.md) by [layil](../../strongs/h/h3915.md), from [paniym](../../strongs/h/h6440.md) the ['am](../../strongs/h/h5971.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 12](exodus_12.md) - [Exodus 14](exodus_14.md)