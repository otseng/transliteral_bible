# [Exodus 35](https://www.blueletterbible.org/kjv/exo/35/1)

<a name="exodus_35_1"></a>Exodus 35:1

And [Mōshe](../../strongs/h/h4872.md) [qāhal](../../strongs/h/h6950.md) all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, These are the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [tsavah](../../strongs/h/h6680.md), that ye should ['asah](../../strongs/h/h6213.md) them.

<a name="exodus_35_2"></a>Exodus 35:2

Six [yowm](../../strongs/h/h3117.md) shall [mĕla'kah](../../strongs/h/h4399.md) be ['asah](../../strongs/h/h6213.md), but on the seventh [yowm](../../strongs/h/h3117.md) there shall be to you a [qodesh](../../strongs/h/h6944.md), a [shabbath](../../strongs/h/h7676.md) of [šabāṯôn](../../strongs/h/h7677.md) to [Yĕhovah](../../strongs/h/h3068.md): whosoever ['asah](../../strongs/h/h6213.md) [mĕla'kah](../../strongs/h/h4399.md) therein shall be [muwth](../../strongs/h/h4191.md).

<a name="exodus_35_3"></a>Exodus 35:3

Ye shall [bāʿar](../../strongs/h/h1197.md) no ['esh](../../strongs/h/h784.md) throughout your [môšāḇ](../../strongs/h/h4186.md) upon the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md).

<a name="exodus_35_4"></a>Exodus 35:4

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), This is the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_35_5"></a>Exodus 35:5

[laqach](../../strongs/h/h3947.md) ye from among you a [tᵊrûmâ](../../strongs/h/h8641.md) unto [Yĕhovah](../../strongs/h/h3068.md): whosoever is of a [nāḏîḇ](../../strongs/h/h5081.md) [leb](../../strongs/h/h3820.md), let him [bow'](../../strongs/h/h935.md) it, a [tᵊrûmâ](../../strongs/h/h8641.md) of [Yĕhovah](../../strongs/h/h3068.md); [zāhāḇ](../../strongs/h/h2091.md), and [keceph](../../strongs/h/h3701.md), and [nᵊḥšeṯ](../../strongs/h/h5178.md),

<a name="exodus_35_6"></a>Exodus 35:6

And [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šēš](../../strongs/h/h8336.md), and [ʿēz](../../strongs/h/h5795.md),

<a name="exodus_35_7"></a>Exodus 35:7

And ['ayil](../../strongs/h/h352.md) ['owr](../../strongs/h/h5785.md) ['āḏam](../../strongs/h/h119.md), and [taḥaš](../../strongs/h/h8476.md), and [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md),

<a name="exodus_35_8"></a>Exodus 35:8

And [šemen](../../strongs/h/h8081.md) for the [ma'owr](../../strongs/h/h3974.md), and [beśem](../../strongs/h/h1314.md) for [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and for the [sam](../../strongs/h/h5561.md) [qᵊṭōreṯ](../../strongs/h/h7004.md),

<a name="exodus_35_9"></a>Exodus 35:9

And [šōham](../../strongs/h/h7718.md) ['eben](../../strongs/h/h68.md), and ['eben](../../strongs/h/h68.md) to be [millu'](../../strongs/h/h4394.md) for the ['ēp̄ôḏ](../../strongs/h/h646.md), and for the [ḥōšen](../../strongs/h/h2833.md).

<a name="exodus_35_10"></a>Exodus 35:10

And every [ḥāḵām](../../strongs/h/h2450.md) [leb](../../strongs/h/h3820.md) among you shall [bow'](../../strongs/h/h935.md), and ['asah](../../strongs/h/h6213.md) all that [Yĕhovah](../../strongs/h/h3068.md) hath [tsavah](../../strongs/h/h6680.md);

<a name="exodus_35_11"></a>Exodus 35:11

The [miškān](../../strongs/h/h4908.md), his ['ohel](../../strongs/h/h168.md), and his [miḵsê](../../strongs/h/h4372.md), his [qeres](../../strongs/h/h7165.md), and his [qereš](../../strongs/h/h7175.md), his [bᵊrîaḥ](../../strongs/h/h1280.md), his [ʿammûḏ](../../strongs/h/h5982.md), and his ['eḏen](../../strongs/h/h134.md),

<a name="exodus_35_12"></a>Exodus 35:12

The ['ārôn](../../strongs/h/h727.md), and the [baḏ](../../strongs/h/h905.md) thereof, with the [kapōreṯ](../../strongs/h/h3727.md), and the [pārōḵeṯ](../../strongs/h/h6532.md) of the [māsāḵ](../../strongs/h/h4539.md),

<a name="exodus_35_13"></a>Exodus 35:13

The [šulḥān](../../strongs/h/h7979.md), and his [baḏ](../../strongs/h/h905.md), and all his [kĕliy](../../strongs/h/h3627.md), and the [paniym](../../strongs/h/h6440.md) [lechem](../../strongs/h/h3899.md),

<a name="exodus_35_14"></a>Exodus 35:14

The [mᵊnôrâ](../../strongs/h/h4501.md) also for the [ma'owr](../../strongs/h/h3974.md), and his [kĕliy](../../strongs/h/h3627.md), and his [nîr](../../strongs/h/h5216.md), with the [šemen](../../strongs/h/h8081.md) for the [ma'owr](../../strongs/h/h3974.md),

<a name="exodus_35_15"></a>Exodus 35:15

And the [qᵊṭōreṯ](../../strongs/h/h7004.md)a [mizbeach](../../strongs/h/h4196.md), and his [baḏ](../../strongs/h/h905.md), and the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and the [sam](../../strongs/h/h5561.md) [qᵊṭōreṯ](../../strongs/h/h7004.md), and the [māsāḵ](../../strongs/h/h4539.md) [peṯaḥ](../../strongs/h/h6607.md) of the [miškān](../../strongs/h/h4908.md),

<a name="exodus_35_16"></a>Exodus 35:16

Thea [mizbeach](../../strongs/h/h4196.md) of [ʿōlâ](../../strongs/h/h5930.md), with his [nᵊḥšeṯ](../../strongs/h/h5178.md) [maḵbēr](../../strongs/h/h4345.md), his [baḏ](../../strongs/h/h905.md), and all his [kĕliy](../../strongs/h/h3627.md), the [kîyôr](../../strongs/h/h3595.md) and his [kēn](../../strongs/h/h3653.md),

<a name="exodus_35_17"></a>Exodus 35:17

The [qelaʿ](../../strongs/h/h7050.md) of the [ḥāṣēr](../../strongs/h/h2691.md), his [ʿammûḏ](../../strongs/h/h5982.md), and their ['eḏen](../../strongs/h/h134.md), and the [māsāḵ](../../strongs/h/h4539.md) for the [sha'ar](../../strongs/h/h8179.md) of the [ḥāṣēr](../../strongs/h/h2691.md),

<a name="exodus_35_18"></a>Exodus 35:18

The [yāṯēḏ](../../strongs/h/h3489.md) of the [miškān](../../strongs/h/h4908.md), and the [yāṯēḏ](../../strongs/h/h3489.md) of the [ḥāṣēr](../../strongs/h/h2691.md), and their [mêṯār](../../strongs/h/h4340.md),

<a name="exodus_35_19"></a>Exodus 35:19

The [beḡeḏ](../../strongs/h/h899.md) of [śᵊrāḏ](../../strongs/h/h8278.md), to do [sharath](../../strongs/h/h8334.md) in the [qodesh](../../strongs/h/h6944.md), the [qodesh](../../strongs/h/h6944.md) [beḡeḏ](../../strongs/h/h899.md) for ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md), and the [beḡeḏ](../../strongs/h/h899.md) of his [ben](../../strongs/h/h1121.md), to [kāhan](../../strongs/h/h3547.md).

<a name="exodus_35_20"></a>Exodus 35:20

And all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yāṣā'](../../strongs/h/h3318.md) from the [paniym](../../strongs/h/h6440.md) of [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_35_21"></a>Exodus 35:21

And they [bow'](../../strongs/h/h935.md), every ['iysh](../../strongs/h/h376.md) whose [leb](../../strongs/h/h3820.md) [nasa'](../../strongs/h/h5375.md) him, and every one whom his [ruwach](../../strongs/h/h7307.md) [nāḏaḇ](../../strongs/h/h5068.md), and they [bow'](../../strongs/h/h935.md) [Yĕhovah](../../strongs/h/h3068.md) [tᵊrûmâ](../../strongs/h/h8641.md) to the [mĕla'kah](../../strongs/h/h4399.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and for all his [ʿăḇōḏâ](../../strongs/h/h5656.md), and for the [qodesh](../../strongs/h/h6944.md) [beḡeḏ](../../strongs/h/h899.md).

<a name="exodus_35_22"></a>Exodus 35:22

And they [bow'](../../strongs/h/h935.md), both ['enowsh](../../strongs/h/h582.md) and ['ishshah](../../strongs/h/h802.md), as many as were [nāḏîḇ](../../strongs/h/h5081.md) [leb](../../strongs/h/h3820.md), and [bow'](../../strongs/h/h935.md) [ḥāḥ](../../strongs/h/h2397.md), and [nezem](../../strongs/h/h5141.md), and [ṭabaʿaṯ](../../strongs/h/h2885.md), and [kûmāz](../../strongs/h/h3558.md), all [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md): and every ['iysh](../../strongs/h/h376.md) that [nûp̄](../../strongs/h/h5130.md) [tᵊnûp̄â](../../strongs/h/h8573.md) of [zāhāḇ](../../strongs/h/h2091.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_35_23"></a>Exodus 35:23

And every ['iysh](../../strongs/h/h376.md), with whom was [māṣā'](../../strongs/h/h4672.md) [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šēš](../../strongs/h/h8336.md), and [ʿēz](../../strongs/h/h5795.md), and ['āḏam](../../strongs/h/h119.md) ['owr](../../strongs/h/h5785.md) of ['ayil](../../strongs/h/h352.md), and [taḥaš](../../strongs/h/h8476.md) ['owr](../../strongs/h/h5785.md), [bow'](../../strongs/h/h935.md) them.

<a name="exodus_35_24"></a>Exodus 35:24

Every one that did [ruwm](../../strongs/h/h7311.md) a [tᵊrûmâ](../../strongs/h/h8641.md) of [keceph](../../strongs/h/h3701.md) and [nᵊḥšeṯ](../../strongs/h/h5178.md) [bow'](../../strongs/h/h935.md) [Yĕhovah](../../strongs/h/h3068.md) [tᵊrûmâ](../../strongs/h/h8641.md): and every man, with whom was [māṣā'](../../strongs/h/h4672.md) [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md) for any [mĕla'kah](../../strongs/h/h4399.md) of the [ʿăḇōḏâ](../../strongs/h/h5656.md), [bow'](../../strongs/h/h935.md) it.

<a name="exodus_35_25"></a>Exodus 35:25

And all the ['ishshah](../../strongs/h/h802.md) that were [ḥāḵām](../../strongs/h/h2450.md) [leb](../../strongs/h/h3820.md) did [ṭāvâ](../../strongs/h/h2901.md) with their [yad](../../strongs/h/h3027.md), and [bow'](../../strongs/h/h935.md) that which they had [maṭvê](../../strongs/h/h4299.md), both of [tᵊḵēleṯ](../../strongs/h/h8504.md), and of ['argāmān](../../strongs/h/h713.md), and of [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and of [šēš](../../strongs/h/h8336.md).

<a name="exodus_35_26"></a>Exodus 35:26

And all the ['ishshah](../../strongs/h/h802.md) whose [leb](../../strongs/h/h3820.md) [nasa'](../../strongs/h/h5375.md) them up in [ḥāḵmâ](../../strongs/h/h2451.md) [ṭāvâ](../../strongs/h/h2901.md) [ʿēz](../../strongs/h/h5795.md) hair.

<a name="exodus_35_27"></a>Exodus 35:27

And the [nāśî'](../../strongs/h/h5387.md) [bow'](../../strongs/h/h935.md) [šōham](../../strongs/h/h7718.md) ['eben](../../strongs/h/h68.md), and ['eben](../../strongs/h/h68.md) to be [millu'](../../strongs/h/h4394.md), for the ['ēp̄ôḏ](../../strongs/h/h646.md), and for the [ḥōšen](../../strongs/h/h2833.md);

<a name="exodus_35_28"></a>Exodus 35:28

And [beśem](../../strongs/h/h1314.md), and [šemen](../../strongs/h/h8081.md) for the [ma'owr](../../strongs/h/h3974.md), and for the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and for the [sam](../../strongs/h/h5561.md) [qᵊṭōreṯ](../../strongs/h/h7004.md).

<a name="exodus_35_29"></a>Exodus 35:29

The [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) a [nᵊḏāḇâ](../../strongs/h/h5071.md) unto [Yĕhovah](../../strongs/h/h3068.md), every ['iysh](../../strongs/h/h376.md) and ['ishshah](../../strongs/h/h802.md), whose [leb](../../strongs/h/h3820.md) made them [nāḏaḇ](../../strongs/h/h5068.md) to [bow'](../../strongs/h/h935.md) for all manner of [mĕla'kah](../../strongs/h/h4399.md), which [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md) to be ['asah](../../strongs/h/h6213.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_35_30"></a>Exodus 35:30

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), [ra'ah](../../strongs/h/h7200.md), [Yĕhovah](../../strongs/h/h3068.md) hath [qara'](../../strongs/h/h7121.md) by [shem](../../strongs/h/h8034.md) [Bᵊṣal'ēl](../../strongs/h/h1212.md) the [ben](../../strongs/h/h1121.md) of ['Ûrî](../../strongs/h/h221.md), the [ben](../../strongs/h/h1121.md) of [Ḥûr](../../strongs/h/h2354.md), of the [maṭṭê](../../strongs/h/h4294.md) of [Yehuwdah](../../strongs/h/h3063.md);

<a name="exodus_35_31"></a>Exodus 35:31

And he hath [mālā'](../../strongs/h/h4390.md) him with the [ruwach](../../strongs/h/h7307.md) of ['Elohiym](../../strongs/h/h430.md), in [ḥāḵmâ](../../strongs/h/h2451.md), in [tāḇûn](../../strongs/h/h8394.md), and in [da'ath](../../strongs/h/h1847.md), and in all manner of [mĕla'kah](../../strongs/h/h4399.md);

<a name="exodus_35_32"></a>Exodus 35:32

And to [chashab](../../strongs/h/h2803.md) [maḥăšāḇâ](../../strongs/h/h4284.md), to ['asah](../../strongs/h/h6213.md) in [zāhāḇ](../../strongs/h/h2091.md), and in [keceph](../../strongs/h/h3701.md), and in [nᵊḥšeṯ](../../strongs/h/h5178.md),

<a name="exodus_35_33"></a>Exodus 35:33

And in the [ḥărōšeṯ](../../strongs/h/h2799.md) of ['eben](../../strongs/h/h68.md), to [mālā'](../../strongs/h/h4390.md) them, and in [ḥărōšeṯ](../../strongs/h/h2799.md) of ['ets](../../strongs/h/h6086.md), to ['asah](../../strongs/h/h6213.md) any manner of [maḥăšāḇâ](../../strongs/h/h4284.md) [mĕla'kah](../../strongs/h/h4399.md).

<a name="exodus_35_34"></a>Exodus 35:34

And he hath [nathan](../../strongs/h/h5414.md) in his [leb](../../strongs/h/h3820.md) that he may [yārâ](../../strongs/h/h3384.md), both he, and ['āhŏlî'āḇ](../../strongs/h/h171.md), the [ben](../../strongs/h/h1121.md) of ['ăḥîsāmāḵ](../../strongs/h/h294.md), of the [maṭṭê](../../strongs/h/h4294.md) of [Dān](../../strongs/h/h1835.md).

<a name="exodus_35_35"></a>Exodus 35:35

Them hath he [mālā'](../../strongs/h/h4390.md) with [ḥāḵmâ](../../strongs/h/h2451.md) of [leb](../../strongs/h/h3820.md), to ['asah](../../strongs/h/h6213.md) all manner of [mĕla'kah](../../strongs/h/h4399.md), of the [ḥārāš](../../strongs/h/h2796.md), and of the [chashab](../../strongs/h/h2803.md), and of the [rāqam](../../strongs/h/h7551.md), in [tᵊḵēleṯ](../../strongs/h/h8504.md), and in ['argāmān](../../strongs/h/h713.md), in [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and in [šēš](../../strongs/h/h8336.md), and of the ['āraḡ](../../strongs/h/h707.md), even of them that ['asah](../../strongs/h/h6213.md) any [mĕla'kah](../../strongs/h/h4399.md), and of those that [chashab](../../strongs/h/h2803.md) [maḥăšāḇâ](../../strongs/h/h4284.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 34](exodus_34.md) - [Exodus 36](exodus_36.md)