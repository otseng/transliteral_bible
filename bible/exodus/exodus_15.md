# [Exodus 15](https://www.blueletterbible.org/kjv/exo/15/1/s_65001)

<a name="exodus_15_1"></a>Exodus 15:1

Then [shiyr](../../strongs/h/h7891.md) [Mōshe](../../strongs/h/h4872.md) and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) this [šîr](../../strongs/h/h7892.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), ['āmar](../../strongs/h/h559.md), I will [shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md), for he hath [gā'â](../../strongs/h/h1342.md) [gā'â](../../strongs/h/h1342.md): the [sûs](../../strongs/h/h5483.md) and his [rāḵaḇ](../../strongs/h/h7392.md) hath he [rāmâ](../../strongs/h/h7411.md) into the [yam](../../strongs/h/h3220.md).

<a name="exodus_15_2"></a>Exodus 15:2

[Yahh](../../strongs/h/h3050.md) is my ['oz](../../strongs/h/h5797.md) and [zimrāṯ](../../strongs/h/h2176.md), and he is become my [yĕshuw'ah](../../strongs/h/h3444.md): he is my ['el](../../strongs/h/h410.md), and I will [nāvâ](../../strongs/h/h5115.md); my ['ab](../../strongs/h/h1.md) ['Elohiym](../../strongs/h/h430.md), and I will [ruwm](../../strongs/h/h7311.md) him.

<a name="exodus_15_3"></a>Exodus 15:3

[Yĕhovah](../../strongs/h/h3068.md) is an ['iysh](../../strongs/h/h376.md) of [milḥāmâ](../../strongs/h/h4421.md): [Yĕhovah](../../strongs/h/h3068.md) is his [shem](../../strongs/h/h8034.md).

<a name="exodus_15_4"></a>Exodus 15:4

[Parʿô](../../strongs/h/h6547.md) [merkāḇâ](../../strongs/h/h4818.md) and his [ḥayil](../../strongs/h/h2428.md) hath he [yārâ](../../strongs/h/h3384.md) into the [yam](../../strongs/h/h3220.md): his [miḇḥār](../../strongs/h/h4005.md) [šālîš](../../strongs/h/h7991.md) also are [ṭāḇaʿ](../../strongs/h/h2883.md) in the [Sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md).

<a name="exodus_15_5"></a>Exodus 15:5

The [tĕhowm](../../strongs/h/h8415.md) have [kāsâ](../../strongs/h/h3680.md) them: they [yarad](../../strongs/h/h3381.md) into the [mᵊṣôlâ](../../strongs/h/h4688.md) as an ['eben](../../strongs/h/h68.md).

<a name="exodus_15_6"></a>Exodus 15:6

Thy [yamiyn](../../strongs/h/h3225.md), [Yĕhovah](../../strongs/h/h3068.md), is become ['āḏar](../../strongs/h/h142.md) in [koach](../../strongs/h/h3581.md): thy [yamiyn](../../strongs/h/h3225.md), [Yĕhovah](../../strongs/h/h3068.md), hath [rāʿaṣ](../../strongs/h/h7492.md) the ['oyeb](../../strongs/h/h341.md).

<a name="exodus_15_7"></a>Exodus 15:7

And in the [rōḇ](../../strongs/h/h7230.md) of thine [gā'ôn](../../strongs/h/h1347.md) thou hast [harac](../../strongs/h/h2040.md) them that [quwm](../../strongs/h/h6965.md) against thee: thou [shalach](../../strongs/h/h7971.md) thy [charown](../../strongs/h/h2740.md), which ['akal](../../strongs/h/h398.md) them as [qaš](../../strongs/h/h7179.md).

<a name="exodus_15_8"></a>Exodus 15:8

And with the [ruwach](../../strongs/h/h7307.md) of thy ['aph](../../strongs/h/h639.md) the [mayim](../../strongs/h/h4325.md) were [ʿāram](../../strongs/h/h6192.md), the [nāzal](../../strongs/h/h5140.md) [nāṣaḇ](../../strongs/h/h5324.md) as a [nēḏ](../../strongs/h/h5067.md), and the [tĕhowm](../../strongs/h/h8415.md) were [qāp̄ā'](../../strongs/h/h7087.md) in the [leb](../../strongs/h/h3820.md) of the [yam](../../strongs/h/h3220.md).

<a name="exodus_15_9"></a>Exodus 15:9

The ['oyeb](../../strongs/h/h341.md) ['āmar](../../strongs/h/h559.md), I will [radaph](../../strongs/h/h7291.md), I will [nāśaḡ](../../strongs/h/h5381.md), I will [chalaq](../../strongs/h/h2505.md) the [šālāl](../../strongs/h/h7998.md); my [nephesh](../../strongs/h/h5315.md) shall be [mālā'](../../strongs/h/h4390.md) upon them; I will [rîq](../../strongs/h/h7324.md) my [chereb](../../strongs/h/h2719.md), my [yad](../../strongs/h/h3027.md) shall [yarash](../../strongs/h/h3423.md) them.

<a name="exodus_15_10"></a>Exodus 15:10

Thou didst [nāšap̄](../../strongs/h/h5398.md) with thy [ruwach](../../strongs/h/h7307.md), the [yam](../../strongs/h/h3220.md) [kāsâ](../../strongs/h/h3680.md) them: they [ṣālal](../../strongs/h/h6749.md) as [ʿōp̄ereṯ](../../strongs/h/h5777.md) in the ['addiyr](../../strongs/h/h117.md) [mayim](../../strongs/h/h4325.md).

<a name="exodus_15_11"></a>Exodus 15:11

Who is like unto thee, [Yĕhovah](../../strongs/h/h3068.md), among the ['el](../../strongs/h/h410.md)? who is like thee, ['āḏar](../../strongs/h/h142.md) in [qodesh](../../strongs/h/h6944.md), [yare'](../../strongs/h/h3372.md) in [tehillah](../../strongs/h/h8416.md), ['asah](../../strongs/h/h6213.md) [pele'](../../strongs/h/h6382.md)?

<a name="exodus_15_12"></a>Exodus 15:12

Thou [natah](../../strongs/h/h5186.md) thy [yamiyn](../../strongs/h/h3225.md), the ['erets](../../strongs/h/h776.md) [bālaʿ](../../strongs/h/h1104.md) them.

<a name="exodus_15_13"></a>Exodus 15:13

Thou in thy [checed](../../strongs/h/h2617.md) hast [nachah](../../strongs/h/h5148.md) the ['am](../../strongs/h/h5971.md) [zû](../../strongs/h/h2098.md) thou hast [gā'al](../../strongs/h/h1350.md): thou hast [nāhal](../../strongs/h/h5095.md) them in thy ['oz](../../strongs/h/h5797.md) unto thy [qodesh](../../strongs/h/h6944.md) [nāvê](../../strongs/h/h5116.md).

<a name="exodus_15_14"></a>Exodus 15:14

The ['am](../../strongs/h/h5971.md) shall [shama'](../../strongs/h/h8085.md), and [ragaz](../../strongs/h/h7264.md): [ḥîl](../../strongs/h/h2427.md) shall ['āḥaz](../../strongs/h/h270.md) on the [yashab](../../strongs/h/h3427.md) of [pᵊlešeṯ](../../strongs/h/h6429.md).

<a name="exodus_15_15"></a>Exodus 15:15

Then the ['allûp̄](../../strongs/h/h441.md) of ['Ĕḏōm](../../strongs/h/h123.md) shall be [bahal](../../strongs/h/h926.md); the ['ayil](../../strongs/h/h352.md) of [Mô'āḇ](../../strongs/h/h4124.md), [ra'ad](../../strongs/h/h7461.md) shall ['āḥaz](../../strongs/h/h270.md) upon them; all the [yashab](../../strongs/h/h3427.md) of [Kĕna'an](../../strongs/h/h3667.md) shall [mûḡ](../../strongs/h/h4127.md).

<a name="exodus_15_16"></a>Exodus 15:16

['êmâ](../../strongs/h/h367.md) and [paḥaḏ](../../strongs/h/h6343.md) shall [naphal](../../strongs/h/h5307.md) upon them; by the [gadowl](../../strongs/h/h1419.md) of thine [zerowa'](../../strongs/h/h2220.md) they shall be as [damam](../../strongs/h/h1826.md) as an ['eben](../../strongs/h/h68.md); till thy ['am](../../strongs/h/h5971.md) ['abar](../../strongs/h/h5674.md), [Yĕhovah](../../strongs/h/h3068.md), till the ['am](../../strongs/h/h5971.md) ['abar](../../strongs/h/h5674.md), [zû](../../strongs/h/h2098.md) thou hast [qānâ](../../strongs/h/h7069.md).

<a name="exodus_15_17"></a>Exodus 15:17

Thou shalt [bow'](../../strongs/h/h935.md) them in, and [nāṭaʿ](../../strongs/h/h5193.md) them in the [har](../../strongs/h/h2022.md) of thine [nachalah](../../strongs/h/h5159.md), in the [māḵôn](../../strongs/h/h4349.md), [Yĕhovah](../../strongs/h/h3068.md), which thou hast [pa'al](../../strongs/h/h6466.md) for thee to [yashab](../../strongs/h/h3427.md) in, in the [miqdash](../../strongs/h/h4720.md), ['adonay](../../strongs/h/h136.md), which thy [yad](../../strongs/h/h3027.md) have [kuwn](../../strongs/h/h3559.md).

<a name="exodus_15_18"></a>Exodus 15:18

[Yĕhovah](../../strongs/h/h3068.md) shall [mālaḵ](../../strongs/h/h4427.md) ['owlam](../../strongs/h/h5769.md) and [ʿaḏ](../../strongs/h/h5703.md).

<a name="exodus_15_19"></a>Exodus 15:19

For the [sûs](../../strongs/h/h5483.md) of [Parʿô](../../strongs/h/h6547.md) [bow'](../../strongs/h/h935.md) with his [reḵeḇ](../../strongs/h/h7393.md) and with his [pārāš](../../strongs/h/h6571.md) into the [yam](../../strongs/h/h3220.md), and [Yĕhovah](../../strongs/h/h3068.md) [shuwb](../../strongs/h/h7725.md) the [mayim](../../strongs/h/h4325.md) of the [yam](../../strongs/h/h3220.md) upon them; but the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [halak](../../strongs/h/h1980.md) on [yabāšâ](../../strongs/h/h3004.md) in the [tavek](../../strongs/h/h8432.md) of the [yam](../../strongs/h/h3220.md).

<a name="exodus_15_20"></a>Exodus 15:20

And [Miryām](../../strongs/h/h4813.md) the [nᵊḇî'â](../../strongs/h/h5031.md), the ['āḥôṯ](../../strongs/h/h269.md) of ['Ahărôn](../../strongs/h/h175.md), [laqach](../../strongs/h/h3947.md) a [tōp̄](../../strongs/h/h8596.md) in her [yad](../../strongs/h/h3027.md); and all the ['ishshah](../../strongs/h/h802.md) [yāṣā'](../../strongs/h/h3318.md) ['aḥar](../../strongs/h/h310.md) her with [tōp̄](../../strongs/h/h8596.md) and with [mᵊḥōlâ](../../strongs/h/h4246.md).

<a name="exodus_15_21"></a>Exodus 15:21

And [Miryām](../../strongs/h/h4813.md) ['anah](../../strongs/h/h6030.md) them, [shiyr](../../strongs/h/h7891.md) ye to [Yĕhovah](../../strongs/h/h3068.md), for he hath [gā'â](../../strongs/h/h1342.md) [gā'â](../../strongs/h/h1342.md); the [sûs](../../strongs/h/h5483.md) and his [rāḵaḇ](../../strongs/h/h7392.md) hath he [rāmâ](../../strongs/h/h7411.md) into the [yam](../../strongs/h/h3220.md).

<a name="exodus_15_22"></a>Exodus 15:22

So [Mōshe](../../strongs/h/h4872.md) [nāsaʿ](../../strongs/h/h5265.md) [Yisra'el](../../strongs/h/h3478.md) from the [Sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md), and they [yāṣā'](../../strongs/h/h3318.md) into the [midbar](../../strongs/h/h4057.md) of [šûr](../../strongs/h/h7793.md); and they [yālaḵ](../../strongs/h/h3212.md) three [yowm](../../strongs/h/h3117.md) in the [midbar](../../strongs/h/h4057.md), and [māṣā'](../../strongs/h/h4672.md) no [mayim](../../strongs/h/h4325.md).

<a name="exodus_15_23"></a>Exodus 15:23

And when they [bow'](../../strongs/h/h935.md) to [Mārâ](../../strongs/h/h4785.md), they [yakol](../../strongs/h/h3201.md) not [šāṯâ](../../strongs/h/h8354.md) of the [mayim](../../strongs/h/h4325.md) of [Mārâ](../../strongs/h/h4785.md), for they were [mar](../../strongs/h/h4751.md): therefore the [shem](../../strongs/h/h8034.md) of it was [qara'](../../strongs/h/h7121.md) [Mārâ](../../strongs/h/h4785.md).

<a name="exodus_15_24"></a>Exodus 15:24

And the ['am](../../strongs/h/h5971.md) [lûn](../../strongs/h/h3885.md) against [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md), What shall we [šāṯâ](../../strongs/h/h8354.md)?

<a name="exodus_15_25"></a>Exodus 15:25

And he [ṣāʿaq](../../strongs/h/h6817.md) unto [Yĕhovah](../../strongs/h/h3068.md); and [Yĕhovah](../../strongs/h/h3068.md) [yārâ](../../strongs/h/h3384.md) him an ['ets](../../strongs/h/h6086.md), which when he had [shalak](../../strongs/h/h7993.md) into the [mayim](../../strongs/h/h4325.md), the [mayim](../../strongs/h/h4325.md) were made [māṯaq](../../strongs/h/h4985.md): there he [śûm](../../strongs/h/h7760.md) for them a [choq](../../strongs/h/h2706.md) and a [mishpat](../../strongs/h/h4941.md), and there he [nāsâ](../../strongs/h/h5254.md) them,

<a name="exodus_15_26"></a>Exodus 15:26

And ['āmar](../../strongs/h/h559.md), If thou wilt [shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) to the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and wilt ['asah](../../strongs/h/h6213.md) that which is [yashar](../../strongs/h/h3477.md) in his ['ayin](../../strongs/h/h5869.md), and wilt ['azan](../../strongs/h/h238.md) to his [mitsvah](../../strongs/h/h4687.md), and [shamar](../../strongs/h/h8104.md) all his [choq](../../strongs/h/h2706.md), I will [śûm](../../strongs/h/h7760.md) none of these [maḥălê](../../strongs/h/h4245.md) upon thee, which I have [śûm](../../strongs/h/h7760.md) upon the [Mitsrayim](../../strongs/h/h4714.md): for I am [Yĕhovah](../../strongs/h/h3068.md) that [rapha'](../../strongs/h/h7495.md) thee.

<a name="exodus_15_27"></a>Exodus 15:27

And they [bow'](../../strongs/h/h935.md) to ['Êlim](../../strongs/h/h362.md), where were twelve ['ayin](../../strongs/h/h5869.md) of [mayim](../../strongs/h/h4325.md), and threescore and ten [tāmār](../../strongs/h/h8558.md): and they [ḥānâ](../../strongs/h/h2583.md) there by the [mayim](../../strongs/h/h4325.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 14](exodus_14.md) - [Exodus 16](exodus_16.md)