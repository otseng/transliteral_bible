# [Exodus 14](https://www.blueletterbible.org/kjv/exo/14/1/s_64001)

<a name="exodus_14_1"></a>Exodus 14:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_14_2"></a>Exodus 14:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they [shuwb](../../strongs/h/h7725.md) and [ḥānâ](../../strongs/h/h2583.md) [paniym](../../strongs/h/h6440.md) [Pî haḥirōṯ](../../strongs/h/h6367.md), between [Miḡdôl](../../strongs/h/h4024.md) and the [yam](../../strongs/h/h3220.md), over [paniym](../../strongs/h/h6440.md) [Baʿal ṣᵊp̄ôn](../../strongs/h/h1189.md): [nēḵaḥ](../../strongs/h/h5226.md) it shall ye [ḥānâ](../../strongs/h/h2583.md) by the [yam](../../strongs/h/h3220.md).

<a name="exodus_14_3"></a>Exodus 14:3

For [Parʿô](../../strongs/h/h6547.md) will ['āmar](../../strongs/h/h559.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), They are [bûḵ](../../strongs/h/h943.md) in the ['erets](../../strongs/h/h776.md), the [midbar](../../strongs/h/h4057.md) hath [cagar](../../strongs/h/h5462.md) them in.

<a name="exodus_14_4"></a>Exodus 14:4

And I will [ḥāzaq](../../strongs/h/h2388.md) [Parʿô](../../strongs/h/h6547.md) [leb](../../strongs/h/h3820.md), that he shall [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) them; and I will be [kabad](../../strongs/h/h3513.md) upon [Parʿô](../../strongs/h/h6547.md), and upon all his [ḥayil](../../strongs/h/h2428.md); that the [Mitsrayim](../../strongs/h/h4714.md) may [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md). And they ['asah](../../strongs/h/h6213.md) so.

<a name="exodus_14_5"></a>Exodus 14:5

And it was [nāḡaḏ](../../strongs/h/h5046.md) the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) that the ['am](../../strongs/h/h5971.md) [bāraḥ](../../strongs/h/h1272.md): and the [lebab](../../strongs/h/h3824.md) of [Parʿô](../../strongs/h/h6547.md) and of his ['ebed](../../strongs/h/h5650.md) was [hāp̄aḵ](../../strongs/h/h2015.md) against the ['am](../../strongs/h/h5971.md), and they ['āmar](../../strongs/h/h559.md), Why have we ['asah](../../strongs/h/h6213.md) this, that we have let [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md) from ['abad](../../strongs/h/h5647.md) us?

<a name="exodus_14_6"></a>Exodus 14:6

And he ['āsar](../../strongs/h/h631.md) his [reḵeḇ](../../strongs/h/h7393.md), and [laqach](../../strongs/h/h3947.md) his ['am](../../strongs/h/h5971.md) with him:

<a name="exodus_14_7"></a>Exodus 14:7

And he [laqach](../../strongs/h/h3947.md) six hundred [bāḥar](../../strongs/h/h977.md) [reḵeḇ](../../strongs/h/h7393.md), and all the [reḵeḇ](../../strongs/h/h7393.md) of [Mitsrayim](../../strongs/h/h4714.md), and [šālîš](../../strongs/h/h7991.md) over every one of them.

<a name="exodus_14_8"></a>Exodus 14:8

And [Yĕhovah](../../strongs/h/h3068.md) [ḥāzaq](../../strongs/h/h2388.md) the [leb](../../strongs/h/h3820.md) of [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), and he [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yāṣā'](../../strongs/h/h3318.md) with a [ruwm](../../strongs/h/h7311.md) [yad](../../strongs/h/h3027.md).

<a name="exodus_14_9"></a>Exodus 14:9

But the [Mitsrayim](../../strongs/h/h4714.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) them, all the [sûs](../../strongs/h/h5483.md) and [reḵeḇ](../../strongs/h/h7393.md) of [Parʿô](../../strongs/h/h6547.md), and his [pārāš](../../strongs/h/h6571.md), and his [ḥayil](../../strongs/h/h2428.md), and [nāśaḡ](../../strongs/h/h5381.md) them [ḥānâ](../../strongs/h/h2583.md) by the [yam](../../strongs/h/h3220.md), beside [Pî haḥirōṯ](../../strongs/h/h6367.md), [paniym](../../strongs/h/h6440.md) [Baʿal ṣᵊp̄ôn](../../strongs/h/h1189.md).

<a name="exodus_14_10"></a>Exodus 14:10

And when [Parʿô](../../strongs/h/h6547.md) [qāraḇ](../../strongs/h/h7126.md), the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nasa'](../../strongs/h/h5375.md) their ['ayin](../../strongs/h/h5869.md), and, behold, the [Mitsrayim](../../strongs/h/h4714.md) [nāsaʿ](../../strongs/h/h5265.md) ['aḥar](../../strongs/h/h310.md) them; and they were [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md): and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ṣāʿaq](../../strongs/h/h6817.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_14_11"></a>Exodus 14:11

And they ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), Because there were no [qeber](../../strongs/h/h6913.md) in [Mitsrayim](../../strongs/h/h4714.md), hast thou [laqach](../../strongs/h/h3947.md) us to [muwth](../../strongs/h/h4191.md) in the [midbar](../../strongs/h/h4057.md)? wherefore hast thou ['asah](../../strongs/h/h6213.md) thus with us, to [yāṣā'](../../strongs/h/h3318.md) us out of [Mitsrayim](../../strongs/h/h4714.md)?

<a name="exodus_14_12"></a>Exodus 14:12

Is not this the [dabar](../../strongs/h/h1697.md) that we did [dabar](../../strongs/h/h1696.md) thee in [Mitsrayim](../../strongs/h/h4714.md), ['āmar](../../strongs/h/h559.md), [ḥāḏal](../../strongs/h/h2308.md), that we may ['abad](../../strongs/h/h5647.md) the [Mitsrayim](../../strongs/h/h4714.md)? For it had been [towb](../../strongs/h/h2896.md) for us to ['abad](../../strongs/h/h5647.md) the [Mitsrayim](../../strongs/h/h4714.md), than that we should [muwth](../../strongs/h/h4191.md) in the [midbar](../../strongs/h/h4057.md).

<a name="exodus_14_13"></a>Exodus 14:13

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), [yare'](../../strongs/h/h3372.md) ye not, [yatsab](../../strongs/h/h3320.md) still, and [ra'ah](../../strongs/h/h7200.md) the [yĕshuw'ah](../../strongs/h/h3444.md) of [Yĕhovah](../../strongs/h/h3068.md), which he will ['asah](../../strongs/h/h6213.md) to you to [yowm](../../strongs/h/h3117.md): for the [Mitsrayim](../../strongs/h/h4714.md) whom ye have [ra'ah](../../strongs/h/h7200.md) to [yowm](../../strongs/h/h3117.md), ye shall [ra'ah](../../strongs/h/h7200.md) them [yāsap̄](../../strongs/h/h3254.md) no more ['owlam](../../strongs/h/h5769.md).

<a name="exodus_14_14"></a>Exodus 14:14

[Yĕhovah](../../strongs/h/h3068.md) shall [lāḥam](../../strongs/h/h3898.md) for you, and ye shall [ḥāraš](../../strongs/h/h2790.md).

<a name="exodus_14_15"></a>Exodus 14:15

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), Wherefore [ṣāʿaq](../../strongs/h/h6817.md) thou unto me? [dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they [nāsaʿ](../../strongs/h/h5265.md):

<a name="exodus_14_16"></a>Exodus 14:16

But [ruwm](../../strongs/h/h7311.md) thou thy [maṭṭê](../../strongs/h/h4294.md), and [natah](../../strongs/h/h5186.md) thine [yad](../../strongs/h/h3027.md) over the [yam](../../strongs/h/h3220.md), and [bāqaʿ](../../strongs/h/h1234.md) it: and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) shall [bow'](../../strongs/h/h935.md) on [yabāšâ](../../strongs/h/h3004.md) through the [tavek](../../strongs/h/h8432.md) of the [yam](../../strongs/h/h3220.md).

<a name="exodus_14_17"></a>Exodus 14:17

And I, behold, I will [ḥāzaq](../../strongs/h/h2388.md) the [leb](../../strongs/h/h3820.md) of the [Mitsrayim](../../strongs/h/h4714.md), and they shall [bow'](../../strongs/h/h935.md) ['aḥar](../../strongs/h/h310.md): and I will [kabad](../../strongs/h/h3513.md) me upon [Parʿô](../../strongs/h/h6547.md), and upon all his [ḥayil](../../strongs/h/h2428.md), upon his [reḵeḇ](../../strongs/h/h7393.md), and upon his [pārāš](../../strongs/h/h6571.md).

<a name="exodus_14_18"></a>Exodus 14:18

And the [Mitsrayim](../../strongs/h/h4714.md) shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md), when I have gotten me [kabad](../../strongs/h/h3513.md) upon [Parʿô](../../strongs/h/h6547.md), upon his [reḵeḇ](../../strongs/h/h7393.md), and upon his [pārāš](../../strongs/h/h6571.md).

<a name="exodus_14_19"></a>Exodus 14:19

And the [mal'ak](../../strongs/h/h4397.md) of ['Elohiym](../../strongs/h/h430.md), which [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) the [maḥănê](../../strongs/h/h4264.md) of [Yisra'el](../../strongs/h/h3478.md), [nāsaʿ](../../strongs/h/h5265.md) and [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) them; and the [ʿammûḏ](../../strongs/h/h5982.md) of the [ʿānān](../../strongs/h/h6051.md) [nāsaʿ](../../strongs/h/h5265.md) from before their [paniym](../../strongs/h/h6440.md), and ['amad](../../strongs/h/h5975.md) ['aḥar](../../strongs/h/h310.md) them:

<a name="exodus_14_20"></a>Exodus 14:20

And it [bow'](../../strongs/h/h935.md) between the [maḥănê](../../strongs/h/h4264.md) of the [Mitsrayim](../../strongs/h/h4714.md) and the [maḥănê](../../strongs/h/h4264.md) of [Yisra'el](../../strongs/h/h3478.md); and it was a [ʿānān](../../strongs/h/h6051.md) and [choshek](../../strongs/h/h2822.md) to them, but it gave ['owr](../../strongs/h/h215.md) by [layil](../../strongs/h/h3915.md) to these: so that the one came not [qāraḇ](../../strongs/h/h7126.md) the other all the [layil](../../strongs/h/h3915.md).

<a name="exodus_14_21"></a>Exodus 14:21

And [Mōshe](../../strongs/h/h4872.md) [natah](../../strongs/h/h5186.md) his [yad](../../strongs/h/h3027.md) over the [yam](../../strongs/h/h3220.md); and [Yĕhovah](../../strongs/h/h3068.md) caused the [yam](../../strongs/h/h3220.md) to [yālaḵ](../../strongs/h/h3212.md) back by an ['az](../../strongs/h/h5794.md) [qāḏîm](../../strongs/h/h6921.md) [ruwach](../../strongs/h/h7307.md) all that [layil](../../strongs/h/h3915.md), and [śûm](../../strongs/h/h7760.md) the [yam](../../strongs/h/h3220.md) [ḥārāḇâ](../../strongs/h/h2724.md), and the [mayim](../../strongs/h/h4325.md) were [bāqaʿ](../../strongs/h/h1234.md).

<a name="exodus_14_22"></a>Exodus 14:22

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) into the [tavek](../../strongs/h/h8432.md) of the [yam](../../strongs/h/h3220.md) upon the [yabāšâ](../../strongs/h/h3004.md): and the [mayim](../../strongs/h/h4325.md) were a [ḥômâ](../../strongs/h/h2346.md) unto them on their [yamiyn](../../strongs/h/h3225.md), and on their [śᵊmō'l](../../strongs/h/h8040.md).

<a name="exodus_14_23"></a>Exodus 14:23

And the [Mitsrayim](../../strongs/h/h4714.md) [radaph](../../strongs/h/h7291.md), and [bow'](../../strongs/h/h935.md) ['aḥar](../../strongs/h/h310.md) them to the [tavek](../../strongs/h/h8432.md) of the [yam](../../strongs/h/h3220.md), even all [Parʿô](../../strongs/h/h6547.md) [sûs](../../strongs/h/h5483.md), his [reḵeḇ](../../strongs/h/h7393.md), and his [pārāš](../../strongs/h/h6571.md).

<a name="exodus_14_24"></a>Exodus 14:24

And it came to pass, that in the [boqer](../../strongs/h/h1242.md) ['ašmurâ](../../strongs/h/h821.md) [Yĕhovah](../../strongs/h/h3068.md) [šāqap̄](../../strongs/h/h8259.md) unto the [maḥănê](../../strongs/h/h4264.md) of the [Mitsrayim](../../strongs/h/h4714.md) through the [ʿammûḏ](../../strongs/h/h5982.md) of ['esh](../../strongs/h/h784.md) and of the [ʿānān](../../strongs/h/h6051.md), and [hāmam](../../strongs/h/h2000.md) the [maḥănê](../../strongs/h/h4264.md) of the [Mitsrayim](../../strongs/h/h4714.md),

<a name="exodus_14_25"></a>Exodus 14:25

And [cuwr](../../strongs/h/h5493.md) their [merkāḇâ](../../strongs/h/h4818.md) ['ôp̄ān](../../strongs/h/h212.md), that they [nāhaḡ](../../strongs/h/h5090.md) them [kᵊḇēḏuṯ](../../strongs/h/h3517.md): so that the [Mitsrayim](../../strongs/h/h4714.md) ['āmar](../../strongs/h/h559.md), Let us [nûs](../../strongs/h/h5127.md) from the [paniym](../../strongs/h/h6440.md) of [Yisra'el](../../strongs/h/h3478.md); for [Yĕhovah](../../strongs/h/h3068.md) [lāḥam](../../strongs/h/h3898.md) for them against the [Mitsrayim](../../strongs/h/h4714.md).

<a name="exodus_14_26"></a>Exodus 14:26

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [natah](../../strongs/h/h5186.md) thine [yad](../../strongs/h/h3027.md) over the [yam](../../strongs/h/h3220.md), that the [mayim](../../strongs/h/h4325.md) may [shuwb](../../strongs/h/h7725.md) upon the [Mitsrayim](../../strongs/h/h4714.md), upon their [reḵeḇ](../../strongs/h/h7393.md), and upon their [pārāš](../../strongs/h/h6571.md).

<a name="exodus_14_27"></a>Exodus 14:27

And [Mōshe](../../strongs/h/h4872.md) [natah](../../strongs/h/h5186.md) his [yad](../../strongs/h/h3027.md) over the [yam](../../strongs/h/h3220.md), and the [yam](../../strongs/h/h3220.md) [shuwb](../../strongs/h/h7725.md) to his ['êṯān](../../strongs/h/h386.md) when the [boqer](../../strongs/h/h1242.md) [panah](../../strongs/h/h6437.md); and the [Mitsrayim](../../strongs/h/h4714.md) [nûs](../../strongs/h/h5127.md) [qārā'](../../strongs/h/h7125.md) it; and [Yĕhovah](../../strongs/h/h3068.md) [nāʿar](../../strongs/h/h5287.md) the [Mitsrayim](../../strongs/h/h4714.md) in the [tavek](../../strongs/h/h8432.md) of the [yam](../../strongs/h/h3220.md).

<a name="exodus_14_28"></a>Exodus 14:28

And the [mayim](../../strongs/h/h4325.md) [shuwb](../../strongs/h/h7725.md), and [kāsâ](../../strongs/h/h3680.md) the [reḵeḇ](../../strongs/h/h7393.md), and the [pārāš](../../strongs/h/h6571.md), and all the [ḥayil](../../strongs/h/h2428.md) of [Parʿô](../../strongs/h/h6547.md) that [bow'](../../strongs/h/h935.md) into the [yam](../../strongs/h/h3220.md) ['aḥar](../../strongs/h/h310.md) them; there [šā'ar](../../strongs/h/h7604.md) not so much as one of them.

<a name="exodus_14_29"></a>Exodus 14:29

But the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [halak](../../strongs/h/h1980.md) upon [yabāšâ](../../strongs/h/h3004.md) in the [tavek](../../strongs/h/h8432.md) of the [yam](../../strongs/h/h3220.md); and the [mayim](../../strongs/h/h4325.md) were a [ḥômâ](../../strongs/h/h2346.md) unto them on their [yamiyn](../../strongs/h/h3225.md), and on their [śᵊmō'l](../../strongs/h/h8040.md).

<a name="exodus_14_30"></a>Exodus 14:30

Thus [Yĕhovah](../../strongs/h/h3068.md) [yasha'](../../strongs/h/h3467.md) [Yisra'el](../../strongs/h/h3478.md) that [yowm](../../strongs/h/h3117.md) out of the [yad](../../strongs/h/h3027.md) of the [Mitsrayim](../../strongs/h/h4714.md); and [Yisra'el](../../strongs/h/h3478.md) [ra'ah](../../strongs/h/h7200.md) the [Mitsrayim](../../strongs/h/h4714.md) [muwth](../../strongs/h/h4191.md) upon the [yam](../../strongs/h/h3220.md) [saphah](../../strongs/h/h8193.md).

<a name="exodus_14_31"></a>Exodus 14:31

And [Yisra'el](../../strongs/h/h3478.md) [ra'ah](../../strongs/h/h7200.md) that [gadowl](../../strongs/h/h1419.md) [yad](../../strongs/h/h3027.md) which [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) upon the [Mitsrayim](../../strongs/h/h4714.md): and the ['am](../../strongs/h/h5971.md) [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md), and ['aman](../../strongs/h/h539.md) [Yĕhovah](../../strongs/h/h3068.md), and his ['ebed](../../strongs/h/h5650.md) [Mōshe](../../strongs/h/h4872.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 13](exodus_13.md) - [Exodus 15](exodus_15.md)