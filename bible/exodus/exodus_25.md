# [Exodus 25](https://www.blueletterbible.org/kjv/exo/25/1/)

<a name="exodus_25_1"></a>Exodus 25:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="exodus_25_2"></a>Exodus 25:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they [laqach](../../strongs/h/h3947.md) me a [tᵊrûmâ](../../strongs/h/h8641.md): of every ['iysh](../../strongs/h/h376.md) that [nāḏaḇ](../../strongs/h/h5068.md) with his [leb](../../strongs/h/h3820.md) ye shall [laqach](../../strongs/h/h3947.md) my [tᵊrûmâ](../../strongs/h/h8641.md).

<a name="exodus_25_3"></a>Exodus 25:3

And this is the [tᵊrûmâ](../../strongs/h/h8641.md) which ye shall [laqach](../../strongs/h/h3947.md) of them; [zāhāḇ](../../strongs/h/h2091.md), and [keceph](../../strongs/h/h3701.md), and [nᵊḥšeṯ](../../strongs/h/h5178.md),

<a name="exodus_25_4"></a>Exodus 25:4

And [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [tôlāʿ](../../strongs/h/h8438.md) [šānî](../../strongs/h/h8144.md), and [šēš](../../strongs/h/h8336.md), and [ʿēz](../../strongs/h/h5795.md) hair,

<a name="exodus_25_5"></a>Exodus 25:5

And ['ayil](../../strongs/h/h352.md) ['owr](../../strongs/h/h5785.md) ['āḏam](../../strongs/h/h119.md), and [taḥaš](../../strongs/h/h8476.md) ['owr](../../strongs/h/h5785.md), and [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md),

<a name="exodus_25_6"></a>Exodus 25:6

[šemen](../../strongs/h/h8081.md) for the [ma'owr](../../strongs/h/h3974.md), [beśem](../../strongs/h/h1314.md) for [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and for [sam](../../strongs/h/h5561.md) [qᵊṭōreṯ](../../strongs/h/h7004.md),

<a name="exodus_25_7"></a>Exodus 25:7

[šōham](../../strongs/h/h7718.md) ['eben](../../strongs/h/h68.md), and ['eben](../../strongs/h/h68.md) to be [millu'](../../strongs/h/h4394.md) in the ['ēp̄ôḏ](../../strongs/h/h646.md), and in the [ḥōšen](../../strongs/h/h2833.md).

<a name="exodus_25_8"></a>Exodus 25:8

And let them ['asah](../../strongs/h/h6213.md) me a [miqdash](../../strongs/h/h4720.md); that I may [shakan](../../strongs/h/h7931.md) [tavek](../../strongs/h/h8432.md) them.

<a name="exodus_25_9"></a>Exodus 25:9

According to all that I [ra'ah](../../strongs/h/h7200.md) thee, after the [taḇnîṯ](../../strongs/h/h8403.md) of the [miškān](../../strongs/h/h4908.md), and the [taḇnîṯ](../../strongs/h/h8403.md) of all the [kĕliy](../../strongs/h/h3627.md) thereof, even so shall ye ['asah](../../strongs/h/h6213.md) it.

<a name="exodus_25_10"></a>Exodus 25:10

And they shall ['asah](../../strongs/h/h6213.md) an ['ārôn](../../strongs/h/h727.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md): two ['ammâ](../../strongs/h/h520.md) and a half shall be the ['ōreḵ](../../strongs/h/h753.md) thereof, and an ['ammâ](../../strongs/h/h520.md) and a half the [rōḥaḇ](../../strongs/h/h7341.md) thereof, and an ['ammâ](../../strongs/h/h520.md) and a half the [qômâ](../../strongs/h/h6967.md) thereof.

<a name="exodus_25_11"></a>Exodus 25:11

And thou shalt [ṣāp̄â](../../strongs/h/h6823.md) it with [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md), [bayith](../../strongs/h/h1004.md) and [ḥûṣ](../../strongs/h/h2351.md) shalt thou [ṣāp̄â](../../strongs/h/h6823.md) it, and shalt ['asah](../../strongs/h/h6213.md) upon it a [zēr](../../strongs/h/h2213.md) of [zāhāḇ](../../strongs/h/h2091.md) [cabiyb](../../strongs/h/h5439.md).

<a name="exodus_25_12"></a>Exodus 25:12

And thou shalt [yāṣaq](../../strongs/h/h3332.md) four [ṭabaʿaṯ](../../strongs/h/h2885.md) of [zāhāḇ](../../strongs/h/h2091.md) for it, and [nathan](../../strongs/h/h5414.md) them in the four [pa'am](../../strongs/h/h6471.md) thereof; and two [ṭabaʿaṯ](../../strongs/h/h2885.md) shall be in the one [tsela'](../../strongs/h/h6763.md) of it, and two [ṭabaʿaṯ](../../strongs/h/h2885.md) in the [šēnî](../../strongs/h/h8145.md).

<a name="exodus_25_13"></a>Exodus 25:13

And thou shalt ['asah](../../strongs/h/h6213.md) [baḏ](../../strongs/h/h905.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md), and [ṣāp̄â](../../strongs/h/h6823.md) them with [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_25_14"></a>Exodus 25:14

And thou shalt [bow'](../../strongs/h/h935.md) the [baḏ](../../strongs/h/h905.md) into the [ṭabaʿaṯ](../../strongs/h/h2885.md) by the [tsela'](../../strongs/h/h6763.md) of the ['ārôn](../../strongs/h/h727.md), that the ['ārôn](../../strongs/h/h727.md) may be [nasa'](../../strongs/h/h5375.md) with them.

<a name="exodus_25_15"></a>Exodus 25:15

The [baḏ](../../strongs/h/h905.md) shall be in the [ṭabaʿaṯ](../../strongs/h/h2885.md) of the ['ārôn](../../strongs/h/h727.md): they shall not be [cuwr](../../strongs/h/h5493.md) from it.

<a name="exodus_25_16"></a>Exodus 25:16

And thou shalt [nathan](../../strongs/h/h5414.md) into the ['ārôn](../../strongs/h/h727.md) the [ʿēḏûṯ](../../strongs/h/h5715.md) which I shall [nathan](../../strongs/h/h5414.md) thee.

<a name="exodus_25_17"></a>Exodus 25:17

And thou shalt ['asah](../../strongs/h/h6213.md) a [kapōreṯ](../../strongs/h/h3727.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md): two ['ammâ](../../strongs/h/h520.md) and a half shall be the ['ōreḵ](../../strongs/h/h753.md) thereof, and an ['ammâ](../../strongs/h/h520.md) and a half the [rōḥaḇ](../../strongs/h/h7341.md) thereof.

<a name="exodus_25_18"></a>Exodus 25:18

And thou shalt ['asah](../../strongs/h/h6213.md) two [kĕruwb](../../strongs/h/h3742.md) of [zāhāḇ](../../strongs/h/h2091.md), of [miqšâ](../../strongs/h/h4749.md) shalt thou ['asah](../../strongs/h/h6213.md) them, in the two [qāṣâ](../../strongs/h/h7098.md) of the [kapōreṯ](../../strongs/h/h3727.md).

<a name="exodus_25_19"></a>Exodus 25:19

And ['asah](../../strongs/h/h6213.md) one [kĕruwb](../../strongs/h/h3742.md) on the [qāṣâ](../../strongs/h/h7098.md), and the other [kĕruwb](../../strongs/h/h3742.md) on the other [qāṣâ](../../strongs/h/h7098.md): even of the [kapōreṯ](../../strongs/h/h3727.md) shall ye ['asah](../../strongs/h/h6213.md) the [kĕruwb](../../strongs/h/h3742.md) on the two [qāṣâ](../../strongs/h/h7098.md) thereof.

<a name="exodus_25_20"></a>Exodus 25:20

And the [kĕruwb](../../strongs/h/h3742.md) shall [pāraś](../../strongs/h/h6566.md) their [kanaph](../../strongs/h/h3671.md) on [maʿal](../../strongs/h/h4605.md), [cakak](../../strongs/h/h5526.md) the [kapōreṯ](../../strongs/h/h3727.md) with their [kanaph](../../strongs/h/h3671.md), and their [paniym](../../strongs/h/h6440.md) ['iysh](../../strongs/h/h376.md) to ['ach](../../strongs/h/h251.md); toward the [kapōreṯ](../../strongs/h/h3727.md) shall the [paniym](../../strongs/h/h6440.md) of the [kĕruwb](../../strongs/h/h3742.md) be.

<a name="exodus_25_21"></a>Exodus 25:21

And thou shalt [nathan](../../strongs/h/h5414.md) the [kapōreṯ](../../strongs/h/h3727.md) [maʿal](../../strongs/h/h4605.md) upon the ['ārôn](../../strongs/h/h727.md); and in the ['ārôn](../../strongs/h/h727.md) thou shalt [nathan](../../strongs/h/h5414.md) the [ʿēḏûṯ](../../strongs/h/h5715.md) that I shall [nathan](../../strongs/h/h5414.md) thee.

<a name="exodus_25_22"></a>Exodus 25:22

And there I will [yāʿaḏ](../../strongs/h/h3259.md) with thee, and I will [dabar](../../strongs/h/h1696.md) with thee from above the [kapōreṯ](../../strongs/h/h3727.md), from between the two [kĕruwb](../../strongs/h/h3742.md) which are upon the ['ārôn](../../strongs/h/h727.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md), of all things which I will give thee in [tsavah](../../strongs/h/h6680.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="exodus_25_23"></a>Exodus 25:23

Thou shalt also ['asah](../../strongs/h/h6213.md) a [šulḥān](../../strongs/h/h7979.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md): two ['ammâ](../../strongs/h/h520.md) shall be the ['ōreḵ](../../strongs/h/h753.md) thereof, and an ['ammâ](../../strongs/h/h520.md) the [rōḥaḇ](../../strongs/h/h7341.md) thereof, and an ['ammâ](../../strongs/h/h520.md) and a half the [qômâ](../../strongs/h/h6967.md) thereof.

<a name="exodus_25_24"></a>Exodus 25:24

And thou shalt [ṣāp̄â](../../strongs/h/h6823.md) it with [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md), and ['asah](../../strongs/h/h6213.md) thereto a [zēr](../../strongs/h/h2213.md) of [zāhāḇ](../../strongs/h/h2091.md) [cabiyb](../../strongs/h/h5439.md).

<a name="exodus_25_25"></a>Exodus 25:25

And thou shalt ['asah](../../strongs/h/h6213.md) unto it a [misgereṯ](../../strongs/h/h4526.md) of a [ṭōp̄aḥ](../../strongs/h/h2948.md) [cabiyb](../../strongs/h/h5439.md), and thou shalt ['asah](../../strongs/h/h6213.md) a [zāhāḇ](../../strongs/h/h2091.md) [zēr](../../strongs/h/h2213.md) to the [misgereṯ](../../strongs/h/h4526.md) thereof [cabiyb](../../strongs/h/h5439.md).

<a name="exodus_25_26"></a>Exodus 25:26

And thou shalt ['asah](../../strongs/h/h6213.md) for it four [ṭabaʿaṯ](../../strongs/h/h2885.md) of [zāhāḇ](../../strongs/h/h2091.md), and [nathan](../../strongs/h/h5414.md) the [ṭabaʿaṯ](../../strongs/h/h2885.md) in the four [pē'â](../../strongs/h/h6285.md) that are on the four [regel](../../strongs/h/h7272.md) thereof.

<a name="exodus_25_27"></a>Exodus 25:27

Over [ʿummâ](../../strongs/h/h5980.md) the [misgereṯ](../../strongs/h/h4526.md) shall the [ṭabaʿaṯ](../../strongs/h/h2885.md) be for [bayith](../../strongs/h/h1004.md) of the [baḏ](../../strongs/h/h905.md) to [nasa'](../../strongs/h/h5375.md) the [šulḥān](../../strongs/h/h7979.md).

<a name="exodus_25_28"></a>Exodus 25:28

And thou shalt ['asah](../../strongs/h/h6213.md) the [baḏ](../../strongs/h/h905.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md), and [ṣāp̄â](../../strongs/h/h6823.md) them with [zāhāḇ](../../strongs/h/h2091.md), that the [šulḥān](../../strongs/h/h7979.md) may be [nasa'](../../strongs/h/h5375.md) with them.

<a name="exodus_25_29"></a>Exodus 25:29

And thou shalt ['asah](../../strongs/h/h6213.md) the [qᵊʿārâ](../../strongs/h/h7086.md) thereof, and [kaph](../../strongs/h/h3709.md) thereof, and [qaśvâ](../../strongs/h/h7184.md) thereof, and [mᵊnaqqîṯ](../../strongs/h/h4518.md) thereof, to [nacak](../../strongs/h/h5258.md) withal: of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md) shalt thou ['asah](../../strongs/h/h6213.md) them.

<a name="exodus_25_30"></a>Exodus 25:30

And thou shalt [nathan](../../strongs/h/h5414.md) upon the [šulḥān](../../strongs/h/h7979.md) [lechem](../../strongs/h/h3899.md) [paniym](../../strongs/h/h6440.md) me [tāmîḏ](../../strongs/h/h8548.md).

<a name="exodus_25_31"></a>Exodus 25:31

And thou shalt ['asah](../../strongs/h/h6213.md) a [mᵊnôrâ](../../strongs/h/h4501.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md): of [miqšâ](../../strongs/h/h4749.md) shall the [mᵊnôrâ](../../strongs/h/h4501.md) be ['asah](../../strongs/h/h6213.md): his [yārēḵ](../../strongs/h/h3409.md), and his [qānê](../../strongs/h/h7070.md), his [gāḇîaʿ](../../strongs/h/h1375.md), his [kap̄tôr](../../strongs/h/h3730.md), and his [peraḥ](../../strongs/h/h6525.md), shall be of the same.

<a name="exodus_25_32"></a>Exodus 25:32

And six [qānê](../../strongs/h/h7070.md) shall [yāṣā'](../../strongs/h/h3318.md) of the [ṣaḏ](../../strongs/h/h6654.md) of it; three [qānê](../../strongs/h/h7070.md) of the [mᵊnôrâ](../../strongs/h/h4501.md) out of the one [ṣaḏ](../../strongs/h/h6654.md), and three [qānê](../../strongs/h/h7070.md) of the [mᵊnôrâ](../../strongs/h/h4501.md) out of the other [ṣaḏ](../../strongs/h/h6654.md):

<a name="exodus_25_33"></a>Exodus 25:33

Three [gāḇîaʿ](../../strongs/h/h1375.md) made like unto [šāqaḏ](../../strongs/h/h8246.md), with a [kap̄tôr](../../strongs/h/h3730.md) and a [peraḥ](../../strongs/h/h6525.md) in one [qānê](../../strongs/h/h7070.md); and three [gāḇîaʿ](../../strongs/h/h1375.md) made like [šāqaḏ](../../strongs/h/h8246.md) in the other [qānê](../../strongs/h/h7070.md), with a [kap̄tôr](../../strongs/h/h3730.md) and a [peraḥ](../../strongs/h/h6525.md): so in the six [qānê](../../strongs/h/h7070.md) that [yāṣā'](../../strongs/h/h3318.md) of the [mᵊnôrâ](../../strongs/h/h4501.md).

<a name="exodus_25_34"></a>Exodus 25:34

And in the [mᵊnôrâ](../../strongs/h/h4501.md) shall be four [gāḇîaʿ](../../strongs/h/h1375.md) made like unto [šāqaḏ](../../strongs/h/h8246.md), with their [kap̄tôr](../../strongs/h/h3730.md) and their [peraḥ](../../strongs/h/h6525.md).

<a name="exodus_25_35"></a>Exodus 25:35

And there shall be a [kap̄tôr](../../strongs/h/h3730.md) under two [qānê](../../strongs/h/h7070.md) of the same, and a [kap̄tôr](../../strongs/h/h3730.md) under two [qānê](../../strongs/h/h7070.md) of the same, and a [kap̄tôr](../../strongs/h/h3730.md) under two [qānê](../../strongs/h/h7070.md) of the same, according to the six [qānê](../../strongs/h/h7070.md) that [yāṣā'](../../strongs/h/h3318.md) of the [mᵊnôrâ](../../strongs/h/h4501.md).

<a name="exodus_25_36"></a>Exodus 25:36

Their [kap̄tôr](../../strongs/h/h3730.md) and their [qānê](../../strongs/h/h7070.md) shall be of the same: all it shall be one [miqšâ](../../strongs/h/h4749.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_25_37"></a>Exodus 25:37

And thou shalt ['asah](../../strongs/h/h6213.md) the seven [nîr](../../strongs/h/h5216.md) thereof: and they shall [ʿālâ](../../strongs/h/h5927.md) the [nîr](../../strongs/h/h5216.md) thereof, that they may ['owr](../../strongs/h/h215.md) over [ʿēḇer](../../strongs/h/h5676.md) [paniym](../../strongs/h/h6440.md).

<a name="exodus_25_38"></a>Exodus 25:38

And the [malqāḥayim](../../strongs/h/h4457.md) thereof, and the [maḥtâ](../../strongs/h/h4289.md) thereof, shall be of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_25_39"></a>Exodus 25:39

Of a [kikār](../../strongs/h/h3603.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md) shall he ['asah](../../strongs/h/h6213.md) it, with all these [kĕliy](../../strongs/h/h3627.md).

<a name="exodus_25_40"></a>Exodus 25:40

And [ra'ah](../../strongs/h/h7200.md) that thou ['asah](../../strongs/h/h6213.md) them after their [taḇnîṯ](../../strongs/h/h8403.md), which was [ra'ah](../../strongs/h/h7200.md) thee in the [har](../../strongs/h/h2022.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 24](exodus_24.md) - [Exodus 26](exodus_26.md)