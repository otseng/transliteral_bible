# [Exodus 33](https://www.blueletterbible.org/kjv/exo/33/1)

<a name="exodus_33_1"></a>Exodus 33:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), [yālaḵ](../../strongs/h/h3212.md), and [ʿālâ](../../strongs/h/h5927.md) hence, thou and the ['am](../../strongs/h/h5971.md) which thou hast [ʿālâ](../../strongs/h/h5927.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), unto the ['erets](../../strongs/h/h776.md) which I [shaba'](../../strongs/h/h7650.md) unto ['Abraham](../../strongs/h/h85.md), to [Yiṣḥāq](../../strongs/h/h3327.md), and to [Ya'aqob](../../strongs/h/h3290.md), ['āmar](../../strongs/h/h559.md), Unto thy [zera'](../../strongs/h/h2233.md) will I [nathan](../../strongs/h/h5414.md) it:

<a name="exodus_33_2"></a>Exodus 33:2

And I will [shalach](../../strongs/h/h7971.md) a [mal'ak](../../strongs/h/h4397.md) [paniym](../../strongs/h/h6440.md) thee; and I will [gāraš](../../strongs/h/h1644.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md), the ['Ĕmōrî](../../strongs/h/h567.md), and the [Ḥitî](../../strongs/h/h2850.md), and the [Pᵊrizzî](../../strongs/h/h6522.md), the [Ḥiûî](../../strongs/h/h2340.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md):

<a name="exodus_33_3"></a>Exodus 33:3

Unto an ['erets](../../strongs/h/h776.md) [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md): for I will not [ʿālâ](../../strongs/h/h5927.md) in the [qereḇ](../../strongs/h/h7130.md) of thee; for thou art a [qāšê](../../strongs/h/h7186.md) [ʿōrep̄](../../strongs/h/h6203.md) ['am](../../strongs/h/h5971.md): lest I [kalah](../../strongs/h/h3615.md) thee in the [derek](../../strongs/h/h1870.md).

<a name="exodus_33_4"></a>Exodus 33:4

And when the ['am](../../strongs/h/h5971.md) [shama'](../../strongs/h/h8085.md) these [ra'](../../strongs/h/h7451.md) [dabar](../../strongs/h/h1697.md), they ['āḇal](../../strongs/h/h56.md): and no ['iysh](../../strongs/h/h376.md) did [shiyth](../../strongs/h/h7896.md) on him his [ʿădiy](../../strongs/h/h5716.md).

<a name="exodus_33_5"></a>Exodus 33:5

For [Yĕhovah](../../strongs/h/h3068.md) had ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), Ye are a [qāšê](../../strongs/h/h7186.md) [ʿōrep̄](../../strongs/h/h6203.md) ['am](../../strongs/h/h5971.md): I will [ʿālâ](../../strongs/h/h5927.md) into the [qereḇ](../../strongs/h/h7130.md) of thee in a [reḡaʿ](../../strongs/h/h7281.md), and [kalah](../../strongs/h/h3615.md) thee: therefore now [yarad](../../strongs/h/h3381.md) thy [ʿădiy](../../strongs/h/h5716.md) from thee, that I may [yada'](../../strongs/h/h3045.md) what to ['asah](../../strongs/h/h6213.md) unto thee.

<a name="exodus_33_6"></a>Exodus 33:6

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [natsal](../../strongs/h/h5337.md) themselves of their [ʿădiy](../../strongs/h/h5716.md) by the [har](../../strongs/h/h2022.md) [ḥōrēḇ](../../strongs/h/h2722.md).

<a name="exodus_33_7"></a>Exodus 33:7

And [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) the ['ohel](../../strongs/h/h168.md), and [natah](../../strongs/h/h5186.md) it [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md), [rachaq](../../strongs/h/h7368.md) from the [maḥănê](../../strongs/h/h4264.md), and [qara'](../../strongs/h/h7121.md) it the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md). And it came to pass, that every one which [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) unto the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), which was [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md).

<a name="exodus_33_8"></a>Exodus 33:8

And it came to pass, when [Mōshe](../../strongs/h/h4872.md) [yāṣā'](../../strongs/h/h3318.md) unto the ['ohel](../../strongs/h/h168.md), that all the ['am](../../strongs/h/h5971.md) [quwm](../../strongs/h/h6965.md), and [nāṣaḇ](../../strongs/h/h5324.md) every ['iysh](../../strongs/h/h376.md) at his ['ohel](../../strongs/h/h168.md) [peṯaḥ](../../strongs/h/h6607.md), and [nabat](../../strongs/h/h5027.md) ['aḥar](../../strongs/h/h310.md) [Mōshe](../../strongs/h/h4872.md), until he was [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md).

<a name="exodus_33_9"></a>Exodus 33:9

And it came to pass, as [Mōshe](../../strongs/h/h4872.md) [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md), the [ʿānān](../../strongs/h/h6051.md) [ʿammûḏ](../../strongs/h/h5982.md) [yarad](../../strongs/h/h3381.md), and ['amad](../../strongs/h/h5975.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md), and [dabar](../../strongs/h/h1696.md) with [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_33_10"></a>Exodus 33:10

And all the ['am](../../strongs/h/h5971.md) [ra'ah](../../strongs/h/h7200.md) the [ʿānān](../../strongs/h/h6051.md) [ʿammûḏ](../../strongs/h/h5982.md) ['amad](../../strongs/h/h5975.md) at the ['ohel](../../strongs/h/h168.md) [peṯaḥ](../../strongs/h/h6607.md): and all the ['am](../../strongs/h/h5971.md) [quwm](../../strongs/h/h6965.md) and [shachah](../../strongs/h/h7812.md), every ['iysh](../../strongs/h/h376.md) in his ['ohel](../../strongs/h/h168.md) [peṯaḥ](../../strongs/h/h6607.md).

<a name="exodus_33_11"></a>Exodus 33:11

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) [paniym](../../strongs/h/h6440.md) to [paniym](../../strongs/h/h6440.md), as an ['iysh](../../strongs/h/h376.md) [dabar](../../strongs/h/h1696.md) unto his [rea'](../../strongs/h/h7453.md). And he [shuwb](../../strongs/h/h7725.md) into the [maḥănê](../../strongs/h/h4264.md): but his [sharath](../../strongs/h/h8334.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), a [naʿar](../../strongs/h/h5288.md), [mûš](../../strongs/h/h4185.md) not [tavek](../../strongs/h/h8432.md) of the ['ohel](../../strongs/h/h168.md).

<a name="exodus_33_12"></a>Exodus 33:12

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto [Yĕhovah](../../strongs/h/h3068.md), [ra'ah](../../strongs/h/h7200.md), thou ['āmar](../../strongs/h/h559.md) unto me, [ʿālâ](../../strongs/h/h5927.md) this ['am](../../strongs/h/h5971.md): and thou hast not let me [yada'](../../strongs/h/h3045.md) whom thou wilt [shalach](../../strongs/h/h7971.md) with me. Yet thou hast ['āmar](../../strongs/h/h559.md), I [yada'](../../strongs/h/h3045.md) thee by [shem](../../strongs/h/h8034.md), and thou hast also [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in my ['ayin](../../strongs/h/h5869.md).

<a name="exodus_33_13"></a>Exodus 33:13

Now therefore, I pray thee, if I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md), [yada'](../../strongs/h/h3045.md) me now thy [derek](../../strongs/h/h1870.md), that I may [yada'](../../strongs/h/h3045.md) thee, that I may [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md): and [ra'ah](../../strongs/h/h7200.md) that this [gowy](../../strongs/h/h1471.md) is thy ['am](../../strongs/h/h5971.md).

<a name="exodus_33_14"></a>Exodus 33:14

And he ['āmar](../../strongs/h/h559.md), My [paniym](../../strongs/h/h6440.md) shall [yālaḵ](../../strongs/h/h3212.md) with thee, and I will give thee [nuwach](../../strongs/h/h5117.md).

<a name="exodus_33_15"></a>Exodus 33:15

And he ['āmar](../../strongs/h/h559.md) unto him, If thy [paniym](../../strongs/h/h6440.md) [halak](../../strongs/h/h1980.md) not with me, [ʿālâ](../../strongs/h/h5927.md) us not hence.

<a name="exodus_33_16"></a>Exodus 33:16

For wherein shall it be [yada'](../../strongs/h/h3045.md) here that I and thy ['am](../../strongs/h/h5971.md) have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md)? is it not in that thou [yālaḵ](../../strongs/h/h3212.md) with us? so shall we be [palah](../../strongs/h/h6395.md), I and thy ['am](../../strongs/h/h5971.md), from all the ['am](../../strongs/h/h5971.md) that are upon the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="exodus_33_17"></a>Exodus 33:17

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), I will ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md) also that thou hast [dabar](../../strongs/h/h1696.md): for thou hast [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in my ['ayin](../../strongs/h/h5869.md), and I [yada'](../../strongs/h/h3045.md) thee by [shem](../../strongs/h/h8034.md).

<a name="exodus_33_18"></a>Exodus 33:18

And he ['āmar](../../strongs/h/h559.md), I [na'](../../strongs/h/h4994.md) thee, [ra'ah](../../strongs/h/h7200.md) me thy [kabowd](../../strongs/h/h3519.md).

<a name="exodus_33_19"></a>Exodus 33:19

And he ['āmar](../../strongs/h/h559.md), I will make all my [ṭûḇ](../../strongs/h/h2898.md) ['abar](../../strongs/h/h5674.md) before thee, and I will [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) [paniym](../../strongs/h/h6440.md) thee; and will be [chanan](../../strongs/h/h2603.md) to whom I will be [chanan](../../strongs/h/h2603.md), and will shew [racham](../../strongs/h/h7355.md) on whom I will shew [racham](../../strongs/h/h7355.md).

<a name="exodus_33_20"></a>Exodus 33:20

And he ['āmar](../../strongs/h/h559.md), Thou [yakol](../../strongs/h/h3201.md) not [ra'ah](../../strongs/h/h7200.md) my [paniym](../../strongs/h/h6440.md): for there shall no ['adam](../../strongs/h/h120.md) [ra'ah](../../strongs/h/h7200.md) me, and [chayay](../../strongs/h/h2425.md).

<a name="exodus_33_21"></a>Exodus 33:21

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), Behold, there is a [maqowm](../../strongs/h/h4725.md) by me, and thou shalt [nāṣaḇ](../../strongs/h/h5324.md) upon a [tsuwr](../../strongs/h/h6697.md):

<a name="exodus_33_22"></a>Exodus 33:22

And it shall come to pass, while my [kabowd](../../strongs/h/h3519.md) ['abar](../../strongs/h/h5674.md) by, that I will [śûm](../../strongs/h/h7760.md) thee in a [nᵊqārâ](../../strongs/h/h5366.md) of the [tsuwr](../../strongs/h/h6697.md), and will [cakak](../../strongs/h/h5526.md) thee with my [kaph](../../strongs/h/h3709.md) while I ['abar](../../strongs/h/h5674.md) by:

<a name="exodus_33_23"></a>Exodus 33:23

And I will [cuwr](../../strongs/h/h5493.md) away mine [kaph](../../strongs/h/h3709.md), and thou shalt [ra'ah](../../strongs/h/h7200.md) my ['āḥôr](../../strongs/h/h268.md): but my [paniym](../../strongs/h/h6440.md) shall not be [ra'ah](../../strongs/h/h7200.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 32](exodus_32.md) - [Exodus 34](exodus_34.md)