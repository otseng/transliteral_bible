# [Exodus 39](https://www.blueletterbible.org/kjv/exo/39/1)

<a name="exodus_39_1"></a>Exodus 39:1

And of the [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), they ['asah](../../strongs/h/h6213.md) [beḡeḏ](../../strongs/h/h899.md) of [śᵊrāḏ](../../strongs/h/h8278.md), to do [sharath](../../strongs/h/h8334.md) in the [qodesh](../../strongs/h/h6944.md), and ['asah](../../strongs/h/h6213.md) the [qodesh](../../strongs/h/h6944.md) [beḡeḏ](../../strongs/h/h899.md) for ['Ahărôn](../../strongs/h/h175.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_39_2"></a>Exodus 39:2

And he ['asah](../../strongs/h/h6213.md) the ['ēp̄ôḏ](../../strongs/h/h646.md) of [zāhāḇ](../../strongs/h/h2091.md), [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [tôlāʿ](../../strongs/h/h8438.md) [šānî](../../strongs/h/h8144.md), and [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md).

<a name="exodus_39_3"></a>Exodus 39:3

And they did [rāqaʿ](../../strongs/h/h7554.md) the [zāhāḇ](../../strongs/h/h2091.md) into [paḥ](../../strongs/h/h6341.md), and [qāṣaṣ](../../strongs/h/h7112.md) it into [pāṯîl](../../strongs/h/h6616.md), to ['asah](../../strongs/h/h6213.md) [tavek](../../strongs/h/h8432.md) it in the [tᵊḵēleṯ](../../strongs/h/h8504.md), and in the ['argāmān](../../strongs/h/h713.md), and in the [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and in the [šēš](../../strongs/h/h8336.md), with [chashab](../../strongs/h/h2803.md) [ma'aseh](../../strongs/h/h4639.md).

<a name="exodus_39_4"></a>Exodus 39:4

They ['asah](../../strongs/h/h6213.md) [kāṯēp̄](../../strongs/h/h3802.md) for it, to [ḥāḇar](../../strongs/h/h2266.md) it: by the two [qāṣâ](../../strongs/h/h7098.md) [qeṣev](../../strongs/h/h7099.md) was it [ḥāḇar](../../strongs/h/h2266.md).

<a name="exodus_39_5"></a>Exodus 39:5

And the [ḥēšeḇ](../../strongs/h/h2805.md) of his ['ăp̄udâ](../../strongs/h/h642.md), that was upon it, was of the same, according to the [ma'aseh](../../strongs/h/h4639.md) thereof; of [zāhāḇ](../../strongs/h/h2091.md), [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_39_6"></a>Exodus 39:6

And they ['asah](../../strongs/h/h6213.md) [šōham](../../strongs/h/h7718.md) ['eben](../../strongs/h/h68.md) [mûsaḇâ](../../strongs/h/h4142.md) in [mišbᵊṣôṯ](../../strongs/h/h4865.md) of [zāhāḇ](../../strongs/h/h2091.md), [pāṯaḥ](../../strongs/h/h6605.md), as [ḥôṯām](../../strongs/h/h2368.md) are [pitûaḥ](../../strongs/h/h6603.md), with the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="exodus_39_7"></a>Exodus 39:7

And he [śûm](../../strongs/h/h7760.md) them on the [kāṯēp̄](../../strongs/h/h3802.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md), that they should be ['eben](../../strongs/h/h68.md) for a [zikārôn](../../strongs/h/h2146.md) to the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_39_8"></a>Exodus 39:8

And he ['asah](../../strongs/h/h6213.md) the [ḥōšen](../../strongs/h/h2833.md) of [chashab](../../strongs/h/h2803.md) [ma'aseh](../../strongs/h/h4639.md), like the [ma'aseh](../../strongs/h/h4639.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md); of [zāhāḇ](../../strongs/h/h2091.md), [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md).

<a name="exodus_39_9"></a>Exodus 39:9

It was [rāḇaʿ](../../strongs/h/h7251.md); they ['asah](../../strongs/h/h6213.md) the [ḥōšen](../../strongs/h/h2833.md) [kāp̄al](../../strongs/h/h3717.md): a [zereṯ](../../strongs/h/h2239.md) was the ['ōreḵ](../../strongs/h/h753.md) thereof, and a [zereṯ](../../strongs/h/h2239.md) the [rōḥaḇ](../../strongs/h/h7341.md) thereof, being [kāp̄al](../../strongs/h/h3717.md).

<a name="exodus_39_10"></a>Exodus 39:10

And they [mālā'](../../strongs/h/h4390.md) in it four [ṭûr](../../strongs/h/h2905.md) of ['eben](../../strongs/h/h68.md): the first [ṭûr](../../strongs/h/h2905.md) was a ['ōḏem](../../strongs/h/h124.md), a [piṭḏâ](../../strongs/h/h6357.md), and a [bāreqeṯ](../../strongs/h/h1304.md): this was the first [ṭûr](../../strongs/h/h2905.md).

<a name="exodus_39_11"></a>Exodus 39:11

And the second [ṭûr](../../strongs/h/h2905.md), a [nōp̄eḵ](../../strongs/h/h5306.md), a [sapîr](../../strongs/h/h5601.md), and a [yahălōm](../../strongs/h/h3095.md).

<a name="exodus_39_12"></a>Exodus 39:12

And the third [ṭûr](../../strongs/h/h2905.md), a [lešem](../../strongs/h/h3958.md), a [šᵊḇô](../../strongs/h/h7618.md), and an ['aḥlāmâ](../../strongs/h/h306.md).

<a name="exodus_39_13"></a>Exodus 39:13

And the fourth [ṭûr](../../strongs/h/h2905.md), a [taršîš](../../strongs/h/h8658.md), a [šōham](../../strongs/h/h7718.md), and a [yāšp̄ih](../../strongs/h/h3471.md): they were [mûsaḇâ](../../strongs/h/h4142.md) in [mišbᵊṣôṯ](../../strongs/h/h4865.md) of [zāhāḇ](../../strongs/h/h2091.md) in their [millu'â](../../strongs/h/h4396.md).

<a name="exodus_39_14"></a>Exodus 39:14

And the ['eben](../../strongs/h/h68.md) were according to the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), twelve, according to their [shem](../../strongs/h/h8034.md), like the [pitûaḥ](../../strongs/h/h6603.md) of a [ḥôṯām](../../strongs/h/h2368.md), every ['iysh](../../strongs/h/h376.md) with his [shem](../../strongs/h/h8034.md), according to the twelve [shebet](../../strongs/h/h7626.md).

<a name="exodus_39_15"></a>Exodus 39:15

And they ['asah](../../strongs/h/h6213.md) upon the [ḥōšen](../../strongs/h/h2833.md) [šaršᵊrâ](../../strongs/h/h8333.md) at the [gaḇluṯ](../../strongs/h/h1383.md), of [ʿăḇōṯ](../../strongs/h/h5688.md) [ma'aseh](../../strongs/h/h4639.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md).

<a name="exodus_39_16"></a>Exodus 39:16

And they ['asah](../../strongs/h/h6213.md) two [mišbᵊṣôṯ](../../strongs/h/h4865.md) of [zāhāḇ](../../strongs/h/h2091.md), and two [zāhāḇ](../../strongs/h/h2091.md) [ṭabaʿaṯ](../../strongs/h/h2885.md); and [nathan](../../strongs/h/h5414.md) the two [ṭabaʿaṯ](../../strongs/h/h2885.md) in the two [qāṣâ](../../strongs/h/h7098.md) of the [ḥōšen](../../strongs/h/h2833.md).

<a name="exodus_39_17"></a>Exodus 39:17

And they [nathan](../../strongs/h/h5414.md) the two [ʿăḇōṯ](../../strongs/h/h5688.md) of [zāhāḇ](../../strongs/h/h2091.md) in the two [ṭabaʿaṯ](../../strongs/h/h2885.md) on the [qāṣâ](../../strongs/h/h7098.md) of the [ḥōšen](../../strongs/h/h2833.md).

<a name="exodus_39_18"></a>Exodus 39:18

And the two [qāṣâ](../../strongs/h/h7098.md) of the two [ʿăḇōṯ](../../strongs/h/h5688.md) they [nathan](../../strongs/h/h5414.md) in the two [mišbᵊṣôṯ](../../strongs/h/h4865.md), and [nathan](../../strongs/h/h5414.md) them on the [kāṯēp̄](../../strongs/h/h3802.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md), [paniym](../../strongs/h/h6440.md) [môl](../../strongs/h/h4136.md).

<a name="exodus_39_19"></a>Exodus 39:19

And they ['asah](../../strongs/h/h6213.md) two [ṭabaʿaṯ](../../strongs/h/h2885.md) of [zāhāḇ](../../strongs/h/h2091.md), and [śûm](../../strongs/h/h7760.md) them on the two [qāṣâ](../../strongs/h/h7098.md) of the [ḥōšen](../../strongs/h/h2833.md), upon the [saphah](../../strongs/h/h8193.md) of it, which was on the [ʿēḇer](../../strongs/h/h5676.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md) [bayith](../../strongs/h/h1004.md).

<a name="exodus_39_20"></a>Exodus 39:20

And they ['asah](../../strongs/h/h6213.md) two other [zāhāḇ](../../strongs/h/h2091.md) [ṭabaʿaṯ](../../strongs/h/h2885.md), and [nathan](../../strongs/h/h5414.md) them on the two [kāṯēp̄](../../strongs/h/h3802.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md) [maṭṭâ](../../strongs/h/h4295.md), [môl](../../strongs/h/h4136.md) the [paniym](../../strongs/h/h6440.md) of it, [ʿummâ](../../strongs/h/h5980.md) the other [maḥbereṯ](../../strongs/h/h4225.md) thereof, [maʿal](../../strongs/h/h4605.md) the [ḥēšeḇ](../../strongs/h/h2805.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md).

<a name="exodus_39_21"></a>Exodus 39:21

And they did [rāḵas](../../strongs/h/h7405.md) the [ḥōšen](../../strongs/h/h2833.md) by his [ṭabaʿaṯ](../../strongs/h/h2885.md) unto the [ṭabaʿaṯ](../../strongs/h/h2885.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md) with a [pāṯîl](../../strongs/h/h6616.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), that it might be above the [ḥēšeḇ](../../strongs/h/h2805.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md), and that the [ḥōšen](../../strongs/h/h2833.md) might not be [zāḥaḥ](../../strongs/h/h2118.md) from the ['ēp̄ôḏ](../../strongs/h/h646.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_39_22"></a>Exodus 39:22

And he ['asah](../../strongs/h/h6213.md) the [mᵊʿîl](../../strongs/h/h4598.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md) of ['āraḡ](../../strongs/h/h707.md) [ma'aseh](../../strongs/h/h4639.md), [kālîl](../../strongs/h/h3632.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md).

<a name="exodus_39_23"></a>Exodus 39:23

And there was a [peh](../../strongs/h/h6310.md) in the [tavek](../../strongs/h/h8432.md) of the [mᵊʿîl](../../strongs/h/h4598.md), as the [peh](../../strongs/h/h6310.md) of a [taḥrā'](../../strongs/h/h8473.md), with a [saphah](../../strongs/h/h8193.md) [cabiyb](../../strongs/h/h5439.md) the [peh](../../strongs/h/h6310.md), that it should not [qāraʿ](../../strongs/h/h7167.md).

<a name="exodus_39_24"></a>Exodus 39:24

And they ['asah](../../strongs/h/h6213.md) upon the [shuwl](../../strongs/h/h7757.md) of the [mᵊʿîl](../../strongs/h/h4598.md) [rimmôn](../../strongs/h/h7416.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šāzar](../../strongs/h/h7806.md).

<a name="exodus_39_25"></a>Exodus 39:25

And they ['asah](../../strongs/h/h6213.md) [paʿămôn](../../strongs/h/h6472.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md), and [nathan](../../strongs/h/h5414.md) the [paʿămôn](../../strongs/h/h6472.md) [tavek](../../strongs/h/h8432.md) the [rimmôn](../../strongs/h/h7416.md) upon the [shuwl](../../strongs/h/h7757.md) of the [mᵊʿîl](../../strongs/h/h4598.md), [cabiyb](../../strongs/h/h5439.md) [tavek](../../strongs/h/h8432.md) the [rimmôn](../../strongs/h/h7416.md);

<a name="exodus_39_26"></a>Exodus 39:26

A [paʿămôn](../../strongs/h/h6472.md) and a [rimmôn](../../strongs/h/h7416.md), a [paʿămôn](../../strongs/h/h6472.md) and a [rimmôn](../../strongs/h/h7416.md), [cabiyb](../../strongs/h/h5439.md) the [shuwl](../../strongs/h/h7757.md) of the [mᵊʿîl](../../strongs/h/h4598.md) to [sharath](../../strongs/h/h8334.md) in; as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_39_27"></a>Exodus 39:27

And they ['asah](../../strongs/h/h6213.md) [kĕthoneth](../../strongs/h/h3801.md) of [šēš](../../strongs/h/h8336.md) of ['āraḡ](../../strongs/h/h707.md) [ma'aseh](../../strongs/h/h4639.md) for ['Ahărôn](../../strongs/h/h175.md), and for his [ben](../../strongs/h/h1121.md),

<a name="exodus_39_28"></a>Exodus 39:28

And a [miṣnep̄eṯ](../../strongs/h/h4701.md) of [šēš](../../strongs/h/h8336.md), and [pᵊ'ēr](../../strongs/h/h6287.md) [miḡbāʿâ](../../strongs/h/h4021.md) of [šēš](../../strongs/h/h8336.md), and [baḏ](../../strongs/h/h906.md) [miḵnās](../../strongs/h/h4370.md) of [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md),

<a name="exodus_39_29"></a>Exodus 39:29

And an ['aḇnēṭ](../../strongs/h/h73.md) of [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md), and [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), of [rāqam](../../strongs/h/h7551.md) [ma'aseh](../../strongs/h/h4639.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_39_30"></a>Exodus 39:30

And they ['asah](../../strongs/h/h6213.md) the [tsiyts](../../strongs/h/h6731.md) of the [qodesh](../../strongs/h/h6944.md) [nēzer](../../strongs/h/h5145.md) of [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md), and [kāṯaḇ](../../strongs/h/h3789.md) upon it a [miḵtāḇ](../../strongs/h/h4385.md), like to the [pitûaḥ](../../strongs/h/h6603.md) of a [ḥôṯām](../../strongs/h/h2368.md), [qodesh](../../strongs/h/h6944.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="exodus_39_31"></a>Exodus 39:31

And they [nathan](../../strongs/h/h5414.md) unto it a [pāṯîl](../../strongs/h/h6616.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), to [nathan](../../strongs/h/h5414.md) it on [maʿal](../../strongs/h/h4605.md) upon the [miṣnep̄eṯ](../../strongs/h/h4701.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_39_32"></a>Exodus 39:32

Thus was all the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [miškān](../../strongs/h/h4908.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) [kalah](../../strongs/h/h3615.md): and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) according to all that [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md), so ['asah](../../strongs/h/h6213.md) they.

<a name="exodus_39_33"></a>Exodus 39:33

And they [bow'](../../strongs/h/h935.md) the [miškān](../../strongs/h/h4908.md) unto [Mōshe](../../strongs/h/h4872.md), the ['ohel](../../strongs/h/h168.md), and all his [kĕliy](../../strongs/h/h3627.md), his [qeres](../../strongs/h/h7165.md), his [qereš](../../strongs/h/h7175.md), his [bᵊrîaḥ](../../strongs/h/h1280.md), and his [ʿammûḏ](../../strongs/h/h5982.md), and his ['eḏen](../../strongs/h/h134.md),

<a name="exodus_39_34"></a>Exodus 39:34

And the [miḵsê](../../strongs/h/h4372.md) of ['ayil](../../strongs/h/h352.md) ['owr](../../strongs/h/h5785.md) ['āḏam](../../strongs/h/h119.md), and the [miḵsê](../../strongs/h/h4372.md) of [taḥaš](../../strongs/h/h8476.md) ['owr](../../strongs/h/h5785.md), and the [pārōḵeṯ](../../strongs/h/h6532.md) of the [māsāḵ](../../strongs/h/h4539.md),

<a name="exodus_39_35"></a>Exodus 39:35

The ['ārôn](../../strongs/h/h727.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md), and the [baḏ](../../strongs/h/h905.md) thereof, and the [kapōreṯ](../../strongs/h/h3727.md),

<a name="exodus_39_36"></a>Exodus 39:36

The [šulḥān](../../strongs/h/h7979.md), and all the [kĕliy](../../strongs/h/h3627.md) thereof, and the [paniym](../../strongs/h/h6440.md) [lechem](../../strongs/h/h3899.md),

<a name="exodus_39_37"></a>Exodus 39:37

The [tahowr](../../strongs/h/h2889.md) [mᵊnôrâ](../../strongs/h/h4501.md), with the [nîr](../../strongs/h/h5216.md) thereof, even with the [nîr](../../strongs/h/h5216.md) to be [maʿărāḵâ](../../strongs/h/h4634.md), and all the [kĕliy](../../strongs/h/h3627.md) thereof, and the [šemen](../../strongs/h/h8081.md) for [ma'owr](../../strongs/h/h3974.md),

<a name="exodus_39_38"></a>Exodus 39:38

And the [zāhāḇ](../../strongs/h/h2091.md)a [mizbeach](../../strongs/h/h4196.md), and the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and the [sam](../../strongs/h/h5561.md) [qᵊṭōreṯ](../../strongs/h/h7004.md), and the [māsāḵ](../../strongs/h/h4539.md) for the ['ohel](../../strongs/h/h168.md) [peṯaḥ](../../strongs/h/h6607.md),

<a name="exodus_39_39"></a>Exodus 39:39

The [nᵊḥšeṯ](../../strongs/h/h5178.md)a [mizbeach](../../strongs/h/h4196.md), and his [maḵbēr](../../strongs/h/h4345.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md), his [baḏ](../../strongs/h/h905.md), and all his [kĕliy](../../strongs/h/h3627.md), the [kîyôr](../../strongs/h/h3595.md) and his [kēn](../../strongs/h/h3653.md),

<a name="exodus_39_40"></a>Exodus 39:40

The [qelaʿ](../../strongs/h/h7050.md) of the [ḥāṣēr](../../strongs/h/h2691.md), his [ʿammûḏ](../../strongs/h/h5982.md), and his ['eḏen](../../strongs/h/h134.md), and the [māsāḵ](../../strongs/h/h4539.md) for the [ḥāṣēr](../../strongs/h/h2691.md) [sha'ar](../../strongs/h/h8179.md), his [mêṯār](../../strongs/h/h4340.md), and his [yāṯēḏ](../../strongs/h/h3489.md), and all the [kĕliy](../../strongs/h/h3627.md) of the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [miškān](../../strongs/h/h4908.md), for the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md),

<a name="exodus_39_41"></a>Exodus 39:41

The [beḡeḏ](../../strongs/h/h899.md) of [śᵊrāḏ](../../strongs/h/h8278.md) to [sharath](../../strongs/h/h8334.md) in the [qodesh](../../strongs/h/h6944.md), and the [qodesh](../../strongs/h/h6944.md) [beḡeḏ](../../strongs/h/h899.md) for ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md), and his [ben](../../strongs/h/h1121.md) [beḡeḏ](../../strongs/h/h899.md), to [kāhan](../../strongs/h/h3547.md).

<a name="exodus_39_42"></a>Exodus 39:42

According to all that [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md), so the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) all the [ʿăḇōḏâ](../../strongs/h/h5656.md).

<a name="exodus_39_43"></a>Exodus 39:43

And [Mōshe](../../strongs/h/h4872.md) did [ra'ah](../../strongs/h/h7200.md) upon all the [mĕla'kah](../../strongs/h/h4399.md), and, behold, they had ['asah](../../strongs/h/h6213.md) it as [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md), even so had they ['asah](../../strongs/h/h6213.md) it: and [Mōshe](../../strongs/h/h4872.md) [barak](../../strongs/h/h1288.md) them.

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 38](exodus_38.md) - [Exodus 40](exodus_40.md)