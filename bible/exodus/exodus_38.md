# [Exodus 38](https://www.blueletterbible.org/kjv/exo/38/1)

<a name="exodus_38_1"></a>Exodus 38:1

And he ['asah](../../strongs/h/h6213.md) thea [mizbeach](../../strongs/h/h4196.md) of [ʿōlâ](../../strongs/h/h5930.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md): five ['ammâ](../../strongs/h/h520.md) was the ['ōreḵ](../../strongs/h/h753.md) thereof, and five ['ammâ](../../strongs/h/h520.md) the [rōḥaḇ](../../strongs/h/h7341.md) thereof; it was [rāḇaʿ](../../strongs/h/h7251.md); and three ['ammâ](../../strongs/h/h520.md) the [qômâ](../../strongs/h/h6967.md) thereof.

<a name="exodus_38_2"></a>Exodus 38:2

And he ['asah](../../strongs/h/h6213.md) the [qeren](../../strongs/h/h7161.md) thereof on the four [pinnâ](../../strongs/h/h6438.md) of it; the [qeren](../../strongs/h/h7161.md) thereof were of the same: and he [ṣāp̄â](../../strongs/h/h6823.md) it with [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="exodus_38_3"></a>Exodus 38:3

And he ['asah](../../strongs/h/h6213.md) all the [kĕliy](../../strongs/h/h3627.md) of thea [mizbeach](../../strongs/h/h4196.md), the [sîr](../../strongs/h/h5518.md), and the [yāʿ](../../strongs/h/h3257.md), and the [mizrāq](../../strongs/h/h4219.md), and the [mazlēḡ](../../strongs/h/h4207.md), and the [maḥtâ](../../strongs/h/h4289.md): all the [kĕliy](../../strongs/h/h3627.md) thereof ['asah](../../strongs/h/h6213.md) he of [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="exodus_38_4"></a>Exodus 38:4

And he ['asah](../../strongs/h/h6213.md) for thea [mizbeach](../../strongs/h/h4196.md) a [nᵊḥšeṯ](../../strongs/h/h5178.md) [maḵbēr](../../strongs/h/h4345.md) of [rešeṯ](../../strongs/h/h7568.md) [ma'aseh](../../strongs/h/h4639.md) under the [karkōḇ](../../strongs/h/h3749.md) thereof [maṭṭâ](../../strongs/h/h4295.md) unto the [ḥēṣî](../../strongs/h/h2677.md) of it.

<a name="exodus_38_5"></a>Exodus 38:5

And he [yāṣaq](../../strongs/h/h3332.md) four [ṭabaʿaṯ](../../strongs/h/h2885.md) for the four [qeṣev](../../strongs/h/h7099.md) of the [maḵbēr](../../strongs/h/h4345.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md), to be [bayith](../../strongs/h/h1004.md) for the [baḏ](../../strongs/h/h905.md).

<a name="exodus_38_6"></a>Exodus 38:6

And he ['asah](../../strongs/h/h6213.md) the [baḏ](../../strongs/h/h905.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md), and [ṣāp̄â](../../strongs/h/h6823.md) them with [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="exodus_38_7"></a>Exodus 38:7

And he [bow'](../../strongs/h/h935.md) the [baḏ](../../strongs/h/h905.md) into the [ṭabaʿaṯ](../../strongs/h/h2885.md) on the [tsela'](../../strongs/h/h6763.md) of thea [mizbeach](../../strongs/h/h4196.md), to [nasa'](../../strongs/h/h5375.md) it withal; he ['asah](../../strongs/h/h6213.md) the [nāḇaḇ](../../strongs/h/h5014.md) with [lûaḥ](../../strongs/h/h3871.md).

<a name="exodus_38_8"></a>Exodus 38:8

And he ['asah](../../strongs/h/h6213.md) the [kîyôr](../../strongs/h/h3595.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md), and the [kēn](../../strongs/h/h3653.md) of it of [nᵊḥšeṯ](../../strongs/h/h5178.md), of the [mar'â](../../strongs/h/h4759.md) of the [ṣᵊḇā'](../../strongs/h/h6633.md), which [ṣᵊḇā'](../../strongs/h/h6633.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="exodus_38_9"></a>Exodus 38:9

And he ['asah](../../strongs/h/h6213.md) the [ḥāṣēr](../../strongs/h/h2691.md): on the [neḡeḇ](../../strongs/h/h5045.md) [pē'â](../../strongs/h/h6285.md) [têmān](../../strongs/h/h8486.md) the [qelaʿ](../../strongs/h/h7050.md) of the [ḥāṣēr](../../strongs/h/h2691.md) were of [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md), an hundred ['ammâ](../../strongs/h/h520.md):

<a name="exodus_38_10"></a>Exodus 38:10

Their [ʿammûḏ](../../strongs/h/h5982.md) were twenty, and their [nᵊḥšeṯ](../../strongs/h/h5178.md) ['eḏen](../../strongs/h/h134.md) twenty; the [vāv](../../strongs/h/h2053.md) of the [ʿammûḏ](../../strongs/h/h5982.md) and their [ḥăšûqîm](../../strongs/h/h2838.md) were of [keceph](../../strongs/h/h3701.md).

<a name="exodus_38_11"></a>Exodus 38:11

And for the [ṣāp̄ôn](../../strongs/h/h6828.md) [pē'â](../../strongs/h/h6285.md) the hangings were an hundred ['ammâ](../../strongs/h/h520.md), their [ʿammûḏ](../../strongs/h/h5982.md) were twenty, and their ['eḏen](../../strongs/h/h134.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) twenty; the [vāv](../../strongs/h/h2053.md) of the [ʿammûḏ](../../strongs/h/h5982.md) and their [ḥăšûqîm](../../strongs/h/h2838.md) of [keceph](../../strongs/h/h3701.md).

<a name="exodus_38_12"></a>Exodus 38:12

And for the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md) were [qelaʿ](../../strongs/h/h7050.md) of fifty ['ammâ](../../strongs/h/h520.md), their [ʿammûḏ](../../strongs/h/h5982.md) ten, and their ['eḏen](../../strongs/h/h134.md) ten; the [vāv](../../strongs/h/h2053.md) of the [ʿammûḏ](../../strongs/h/h5982.md) and their [ḥăšûqîm](../../strongs/h/h2838.md) of [keceph](../../strongs/h/h3701.md).

<a name="exodus_38_13"></a>Exodus 38:13

And for the [qeḏem](../../strongs/h/h6924.md) [pē'â](../../strongs/h/h6285.md) [mizrach](../../strongs/h/h4217.md) fifty ['ammâ](../../strongs/h/h520.md).

<a name="exodus_38_14"></a>Exodus 38:14

The [qelaʿ](../../strongs/h/h7050.md) of the one [kāṯēp̄](../../strongs/h/h3802.md) of the gate were fifteen ['ammâ](../../strongs/h/h520.md); their [ʿammûḏ](../../strongs/h/h5982.md) three, and their ['eḏen](../../strongs/h/h134.md) three.

<a name="exodus_38_15"></a>Exodus 38:15

And for the other [kāṯēp̄](../../strongs/h/h3802.md) of the [ḥāṣēr](../../strongs/h/h2691.md) [sha'ar](../../strongs/h/h8179.md), on this hand and that hand, were [qelaʿ](../../strongs/h/h7050.md) of fifteen ['ammâ](../../strongs/h/h520.md); their [ʿammûḏ](../../strongs/h/h5982.md) three, and their ['eḏen](../../strongs/h/h134.md) three.

<a name="exodus_38_16"></a>Exodus 38:16

All the [qelaʿ](../../strongs/h/h7050.md) of the [ḥāṣēr](../../strongs/h/h2691.md) [cabiyb](../../strongs/h/h5439.md) were of [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md).

<a name="exodus_38_17"></a>Exodus 38:17

And the ['eḏen](../../strongs/h/h134.md) for the [ʿammûḏ](../../strongs/h/h5982.md) were of [nᵊḥšeṯ](../../strongs/h/h5178.md); the [vāv](../../strongs/h/h2053.md) of the [ʿammûḏ](../../strongs/h/h5982.md) and their [ḥăšûqîm](../../strongs/h/h2838.md) of [keceph](../../strongs/h/h3701.md); and the [ṣipûy](../../strongs/h/h6826.md) of their [ro'sh](../../strongs/h/h7218.md) of [keceph](../../strongs/h/h3701.md); and all the [ʿammûḏ](../../strongs/h/h5982.md) of the [ḥāṣēr](../../strongs/h/h2691.md) were [ḥāšaq](../../strongs/h/h2836.md) with [keceph](../../strongs/h/h3701.md).

<a name="exodus_38_18"></a>Exodus 38:18

And the [māsāḵ](../../strongs/h/h4539.md) for the [sha'ar](../../strongs/h/h8179.md) of the [ḥāṣēr](../../strongs/h/h2691.md) was [rāqam](../../strongs/h/h7551.md) [ma'aseh](../../strongs/h/h4639.md), of [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šāzar](../../strongs/h/h7806.md) [šēš](../../strongs/h/h8336.md): and twenty ['ammâ](../../strongs/h/h520.md) was the ['ōreḵ](../../strongs/h/h753.md), and the [qômâ](../../strongs/h/h6967.md) in the [rōḥaḇ](../../strongs/h/h7341.md) was five ['ammâ](../../strongs/h/h520.md), [ʿummâ](../../strongs/h/h5980.md) to the [qelaʿ](../../strongs/h/h7050.md) of the [ḥāṣēr](../../strongs/h/h2691.md).

<a name="exodus_38_19"></a>Exodus 38:19

And their [ʿammûḏ](../../strongs/h/h5982.md) were four, and their ['eḏen](../../strongs/h/h134.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) four; their [vāv](../../strongs/h/h2053.md) of [keceph](../../strongs/h/h3701.md), and the [ṣipûy](../../strongs/h/h6826.md) of their [ro'sh](../../strongs/h/h7218.md) and their [ḥăšûqîm](../../strongs/h/h2838.md) of [keceph](../../strongs/h/h3701.md).

<a name="exodus_38_20"></a>Exodus 38:20

And all the [yāṯēḏ](../../strongs/h/h3489.md) of the [miškān](../../strongs/h/h4908.md), and of the [ḥāṣēr](../../strongs/h/h2691.md) [cabiyb](../../strongs/h/h5439.md), were of [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="exodus_38_21"></a>Exodus 38:21

This is the [paqad](../../strongs/h/h6485.md) of the [miškān](../../strongs/h/h4908.md), even of the [miškān](../../strongs/h/h4908.md) of [ʿēḏûṯ](../../strongs/h/h5715.md), as it was [paqad](../../strongs/h/h6485.md), according to the [peh](../../strongs/h/h6310.md) of [Mōshe](../../strongs/h/h4872.md), for the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [Lᵊvî](../../strongs/h/h3881.md), by the [yad](../../strongs/h/h3027.md) of ['Îṯāmār](../../strongs/h/h385.md), [ben](../../strongs/h/h1121.md) to ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md).

<a name="exodus_38_22"></a>Exodus 38:22

And [Bᵊṣal'ēl](../../strongs/h/h1212.md) the [ben](../../strongs/h/h1121.md) ['Ûrî](../../strongs/h/h221.md), the [ben](../../strongs/h/h1121.md) of [Ḥûr](../../strongs/h/h2354.md), of the [maṭṭê](../../strongs/h/h4294.md) of [Yehuwdah](../../strongs/h/h3063.md), ['asah](../../strongs/h/h6213.md) all that [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="exodus_38_23"></a>Exodus 38:23

And with him was ['āhŏlî'āḇ](../../strongs/h/h171.md), [ben](../../strongs/h/h1121.md) of ['ăḥîsāmāḵ](../../strongs/h/h294.md), of the [maṭṭê](../../strongs/h/h4294.md) of [Dān](../../strongs/h/h1835.md), an [ḥārāš](../../strongs/h/h2796.md), and a [chashab](../../strongs/h/h2803.md), and a [rāqam](../../strongs/h/h7551.md) in [tᵊḵēleṯ](../../strongs/h/h8504.md), and in ['argāmān](../../strongs/h/h713.md), and in [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and [šēš](../../strongs/h/h8336.md).

<a name="exodus_38_24"></a>Exodus 38:24

All the [zāhāḇ](../../strongs/h/h2091.md) that was ['asah](../../strongs/h/h6213.md) for the [mĕla'kah](../../strongs/h/h4399.md) in all the [mĕla'kah](../../strongs/h/h4399.md) of the [qodesh](../../strongs/h/h6944.md), even the [zāhāḇ](../../strongs/h/h2091.md) of the [tᵊnûp̄â](../../strongs/h/h8573.md), was twenty and nine [kikār](../../strongs/h/h3603.md), and seven hundred and thirty [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md).

<a name="exodus_38_25"></a>Exodus 38:25

And the [keceph](../../strongs/h/h3701.md) of them that were [paqad](../../strongs/h/h6485.md) of the ['edah](../../strongs/h/h5712.md) was an hundred [kikār](../../strongs/h/h3603.md), and a thousand seven hundred and threescore and fifteen [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md):

<a name="exodus_38_26"></a>Exodus 38:26

A [beqaʿ](../../strongs/h/h1235.md) for [gulgōleṯ](../../strongs/h/h1538.md), that is, half a [šeqel](../../strongs/h/h8255.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md), for every one that ['abar](../../strongs/h/h5674.md) to be [paqad](../../strongs/h/h6485.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), for six hundred thousand and three thousand and five hundred and fifty men.

<a name="exodus_38_27"></a>Exodus 38:27

And of the hundred [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md) were [yāṣaq](../../strongs/h/h3332.md) the ['eḏen](../../strongs/h/h134.md) of the [qodesh](../../strongs/h/h6944.md), and the ['eḏen](../../strongs/h/h134.md) of the [pārōḵeṯ](../../strongs/h/h6532.md); an hundred ['eḏen](../../strongs/h/h134.md) of the hundred [kikār](../../strongs/h/h3603.md), a [kikār](../../strongs/h/h3603.md) for an ['eḏen](../../strongs/h/h134.md).

<a name="exodus_38_28"></a>Exodus 38:28

And of the thousand seven hundred seventy and five he ['asah](../../strongs/h/h6213.md) [vāv](../../strongs/h/h2053.md) for the [ʿammûḏ](../../strongs/h/h5982.md), and [ṣāp̄â](../../strongs/h/h6823.md) their [ro'sh](../../strongs/h/h7218.md), and [ḥāšaq](../../strongs/h/h2836.md) them.

<a name="exodus_38_29"></a>Exodus 38:29

And the [nᵊḥšeṯ](../../strongs/h/h5178.md) of the [tᵊnûp̄â](../../strongs/h/h8573.md) was seventy [kikār](../../strongs/h/h3603.md), and two thousand and four hundred [šeqel](../../strongs/h/h8255.md).

<a name="exodus_38_30"></a>Exodus 38:30

And therewith he ['asah](../../strongs/h/h6213.md) the ['eḏen](../../strongs/h/h134.md) to the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and the [nᵊḥšeṯ](../../strongs/h/h5178.md)a [mizbeach](../../strongs/h/h4196.md), and the [nᵊḥšeṯ](../../strongs/h/h5178.md) [maḵbēr](../../strongs/h/h4345.md) for it, and all the [kĕliy](../../strongs/h/h3627.md) of thea [mizbeach](../../strongs/h/h4196.md),

<a name="exodus_38_31"></a>Exodus 38:31

And the ['eḏen](../../strongs/h/h134.md) of the [ḥāṣēr](../../strongs/h/h2691.md) [cabiyb](../../strongs/h/h5439.md), and the ['eḏen](../../strongs/h/h134.md) of the [ḥāṣēr](../../strongs/h/h2691.md) [sha'ar](../../strongs/h/h8179.md), and all the [yāṯēḏ](../../strongs/h/h3489.md) of the [miškān](../../strongs/h/h4908.md), and all the [yāṯēḏ](../../strongs/h/h3489.md) of the [ḥāṣēr](../../strongs/h/h2691.md) [cabiyb](../../strongs/h/h5439.md).

---

[Transliteral Bible](../bible.md)

[Exodus](exodus.md)

[Exodus 37](exodus_37.md) - [Exodus 39](exodus_39.md)