# [Jude 1](https://www.blueletterbible.org/kjv/jde/1/1/s_1167001)

<a name="jude_1_1"></a>Jude 1:1

[Ioudas](../../strongs/g/g2455.md), the [doulos](../../strongs/g/g1401.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), and [adelphos](../../strongs/g/g80.md) of [Iakōbos](../../strongs/g/g2385.md), to them that are [hagiazō](../../strongs/g/g37.md) by [theos](../../strongs/g/g2316.md) the [patēr](../../strongs/g/g3962.md), and [tēreō](../../strongs/g/g5083.md) in [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), [klētos](../../strongs/g/g2822.md):

<a name="jude_1_2"></a>Jude 1:2

[eleos](../../strongs/g/g1656.md) unto you, and [eirēnē](../../strongs/g/g1515.md), and [agapē](../../strongs/g/g26.md), be [plēthynō](../../strongs/g/g4129.md).

<a name="jude_1_3"></a>Jude 1:3

[agapētos](../../strongs/g/g27.md), when I [poieō](../../strongs/g/g4160.md) all [spoudē](../../strongs/g/g4710.md) to [graphō](../../strongs/g/g1125.md) unto you of the [koinos](../../strongs/g/g2839.md) [sōtēria](../../strongs/g/g4991.md), it was [anagkē](../../strongs/g/g318.md) for me to [graphō](../../strongs/g/g1125.md) unto you, and [parakaleō](../../strongs/g/g3870.md) that ye should [epagōnizomai](../../strongs/g/g1864.md) for the [pistis](../../strongs/g/g4102.md) which was [hapax](../../strongs/g/g530.md) [paradidōmi](../../strongs/g/g3860.md) unto the [hagios](../../strongs/g/g40.md).

<a name="jude_1_4"></a>Jude 1:4

For there are certain [anthrōpos](../../strongs/g/g444.md) [pareisdy(n)ō](../../strongs/g/g3921.md), who were before of [palai](../../strongs/g/g3819.md) [prographō](../../strongs/g/g4270.md) to this [krima](../../strongs/g/g2917.md), [asebēs](../../strongs/g/g765.md), [metatithēmi](../../strongs/g/g3346.md) the [charis](../../strongs/g/g5485.md) of our [theos](../../strongs/g/g2316.md) into [aselgeia](../../strongs/g/g766.md), and [arneomai](../../strongs/g/g720.md) the only [despotēs](../../strongs/g/g1203.md) [theos](../../strongs/g/g2316.md), and our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="jude_1_5"></a>Jude 1:5

I [boulomai](../../strongs/g/g1014.md) therefore [hypomimnēskō](../../strongs/g/g5279.md) you, though ye [hapax](../../strongs/g/g530.md) [eidō](../../strongs/g/g1492.md) this, how that the [kyrios](../../strongs/g/g2962.md), having [sōzō](../../strongs/g/g4982.md) the [laos](../../strongs/g/g2992.md) out of the [gē](../../strongs/g/g1093.md) of [Aigyptos](../../strongs/g/g125.md), afterward [apollymi](../../strongs/g/g622.md) them that [pisteuō](../../strongs/g/g4100.md) not.

<a name="jude_1_6"></a>Jude 1:6

And the [aggelos](../../strongs/g/g32.md) which [tēreō](../../strongs/g/g5083.md) not their [archē](../../strongs/g/g746.md), but [apoleipō](../../strongs/g/g620.md) their own [oikētērion](../../strongs/g/g3613.md), he hath [tēreō](../../strongs/g/g5083.md) in [aïdios](../../strongs/g/g126.md) [desmos](../../strongs/g/g1199.md) under [zophos](../../strongs/g/g2217.md) unto the [krisis](../../strongs/g/g2920.md) of the [megas](../../strongs/g/g3173.md) [hēmera](../../strongs/g/g2250.md).

<a name="jude_1_7"></a>Jude 1:7

Even as [Sodoma](../../strongs/g/g4670.md) and [Gomorra](../../strongs/g/g1116.md), and the [polis](../../strongs/g/g4172.md) about them in [homoios](../../strongs/g/g3664.md) [toutois](../../strongs/g/g5125.md) [tropos](../../strongs/g/g5158.md), [ekporneuō](../../strongs/g/g1608.md), and [aperchomai](../../strongs/g/g565.md) after [heteros](../../strongs/g/g2087.md) [sarx](../../strongs/g/g4561.md), are [prokeimai](../../strongs/g/g4295.md) a [deigma](../../strongs/g/g1164.md), [hypechō](../../strongs/g/g5254.md) the [dikē](../../strongs/g/g1349.md) of [aiōnios](../../strongs/g/g166.md) [pyr](../../strongs/g/g4442.md).

<a name="jude_1_8"></a>Jude 1:8

[homoiōs](../../strongs/g/g3668.md) also these [enypniazomai](../../strongs/g/g1797.md) [miainō](../../strongs/g/g3392.md) the [sarx](../../strongs/g/g4561.md), [atheteō](../../strongs/g/g114.md) [kyriotēs](../../strongs/g/g2963.md), and [blasphēmeō](../../strongs/g/g987.md) of [doxa](../../strongs/g/g1391.md).

<a name="jude_1_9"></a>Jude 1:9

Yet [Michaēl](../../strongs/g/g3413.md) the [archangelos](../../strongs/g/g743.md), when [diakrinō](../../strongs/g/g1252.md) with the [diabolos](../../strongs/g/g1228.md) he [dialegomai](../../strongs/g/g1256.md) about the [sōma](../../strongs/g/g4983.md) of [Mōÿsēs](../../strongs/g/g3475.md), [tolmaō](../../strongs/g/g5111.md) not [epipherō](../../strongs/g/g2018.md) him a [blasphēmia](../../strongs/g/g988.md) [krisis](../../strongs/g/g2920.md), but [eipon](../../strongs/g/g2036.md), The [kyrios](../../strongs/g/g2962.md) [epitimaō](../../strongs/g/g2008.md) thee.

<a name="jude_1_10"></a>Jude 1:10

But these [blasphēmeō](../../strongs/g/g987.md) of those things which they [eidō](../../strongs/g/g1492.md) not: but what they [epistamai](../../strongs/g/g1987.md) [physikōs](../../strongs/g/g5447.md), as [alogos](../../strongs/g/g249.md) [zōon](../../strongs/g/g2226.md), in those things they [phtheirō](../../strongs/g/g5351.md).

<a name="jude_1_11"></a>Jude 1:11

[ouai](../../strongs/g/g3759.md) unto them! for they have [poreuō](../../strongs/g/g4198.md) in the [hodos](../../strongs/g/g3598.md) of [kain](../../strongs/g/g2535.md), and [ekcheō](../../strongs/g/g1632.md) the [planē](../../strongs/g/g4106.md) of [Balaam](../../strongs/g/g903.md) for [misthos](../../strongs/g/g3408.md), and [apollymi](../../strongs/g/g622.md) in the [antilogia](../../strongs/g/g485.md) of [kore](../../strongs/g/g2879.md).

<a name="jude_1_12"></a>Jude 1:12

These are [spilas](../../strongs/g/g4694.md) in your [agapē](../../strongs/g/g26.md), when they [syneuōcheomai](../../strongs/g/g4910.md) with you, [poimainō](../../strongs/g/g4165.md) themselves [aphobōs](../../strongs/g/g870.md): [nephelē](../../strongs/g/g3507.md) [anydros](../../strongs/g/g504.md), [peripherō](../../strongs/g/g4064.md) of [anemos](../../strongs/g/g417.md); [dendron](../../strongs/g/g1186.md) [phthinopōrinos](../../strongs/g/g5352.md), [akarpos](../../strongs/g/g175.md), twice [apothnēskō](../../strongs/g/g599.md), [ekrizoō](../../strongs/g/g1610.md);


<a name="jude_1_13"></a>Jude 1:13

[agrios](../../strongs/g/g66.md) [kyma](../../strongs/g/g2949.md) of the [thalassa](../../strongs/g/g2281.md), [epaphrizō](../../strongs/g/g1890.md) their own [aischynē](../../strongs/g/g152.md); [planētēs](../../strongs/g/g4107.md) [astēr](../../strongs/g/g792.md), to whom is [tēreō](../../strongs/g/g5083.md) the [zophos](../../strongs/g/g2217.md) of [skotos](../../strongs/g/g4655.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md).

<a name="jude_1_14"></a>Jude 1:14

And [Henōch](../../strongs/g/g1802.md) also, the seventh from [Adam](../../strongs/g/g76.md), [prophēteuō](../../strongs/g/g4395.md) of these, [legō](../../strongs/g/g3004.md), [idou](../../strongs/g/g2400.md), the [kyrios](../../strongs/g/g2962.md) [erchomai](../../strongs/g/g2064.md) with [myrias](../../strongs/g/g3461.md) of his [hagios](../../strongs/g/g40.md),

<a name="jude_1_15"></a>Jude 1:15

To [poieō](../../strongs/g/g4160.md) [krisis](../../strongs/g/g2920.md) upon all, and to [exelegchō](../../strongs/g/g1827.md) all that are [asebēs](../../strongs/g/g765.md) among them of all their [asebeia](../../strongs/g/g763.md) [ergon](../../strongs/g/g2041.md) which they have [asebeō](../../strongs/g/g764.md), and of all their [sklēros](../../strongs/g/g4642.md) which [asebēs](../../strongs/g/g765.md) [hamartōlos](../../strongs/g/g268.md) have [laleō](../../strongs/g/g2980.md) against him.

<a name="jude_1_16"></a>Jude 1:16

These are [gongystēs](../../strongs/g/g1113.md), [mempsimoiros](../../strongs/g/g3202.md), [poreuō](../../strongs/g/g4198.md) after their own [epithymia](../../strongs/g/g1939.md); and their [stoma](../../strongs/g/g4750.md) [laleō](../../strongs/g/g2980.md) [hyperogkos](../../strongs/g/g5246.md), [prosōpon](../../strongs/g/g4383.md) in [thaumazō](../../strongs/g/g2296.md) because of [ōpheleia](../../strongs/g/g5622.md).

<a name="jude_1_17"></a>Jude 1:17

But, [agapētos](../../strongs/g/g27.md), [mnaomai](../../strongs/g/g3415.md) ye the [rhēma](../../strongs/g/g4487.md) which were [proereō](../../strongs/g/g4280.md) of the [apostolos](../../strongs/g/g652.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md);

<a name="jude_1_18"></a>Jude 1:18

How that they [legō](../../strongs/g/g3004.md) you there should be [empaiktēs](../../strongs/g/g1703.md) in the [eschatos](../../strongs/g/g2078.md) [chronos](../../strongs/g/g5550.md), who should [poreuō](../../strongs/g/g4198.md) after their own [asebeia](../../strongs/g/g763.md) [epithymia](../../strongs/g/g1939.md).

<a name="jude_1_19"></a>Jude 1:19

These be they who [apodiorizō](../../strongs/g/g592.md) themselves, [psychikos](../../strongs/g/g5591.md), having not the [pneuma](../../strongs/g/g4151.md).

<a name="jude_1_20"></a>Jude 1:20

But ye, [agapētos](../../strongs/g/g27.md), [epoikodomeō](../../strongs/g/g2026.md) yourselves on your [hagios](../../strongs/g/g40.md) [pistis](../../strongs/g/g4102.md), [proseuchomai](../../strongs/g/g4336.md) in the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md),

<a name="jude_1_21"></a>Jude 1:21

[tēreō](../../strongs/g/g5083.md) yourselves in the [agapē](../../strongs/g/g26.md) of [theos](../../strongs/g/g2316.md), [prosdechomai](../../strongs/g/g4327.md) the [eleos](../../strongs/g/g1656.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) unto [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md).

<a name="jude_1_22"></a>Jude 1:22

And of some have [eleeō](../../strongs/g/g1653.md), [diakrinō](../../strongs/g/g1252.md): [^1]

<a name="jude_1_23"></a>Jude 1:23

And others [sōzō](../../strongs/g/g4982.md) with [phobos](../../strongs/g/g5401.md), [harpazō](../../strongs/g/g726.md) of the [pyr](../../strongs/g/g4442.md); [miseō](../../strongs/g/g3404.md) even the [chitōn](../../strongs/g/g5509.md) [spiloō](../../strongs/g/g4695.md) by the [sarx](../../strongs/g/g4561.md).

<a name="jude_1_24"></a>Jude 1:24

Now unto him that [dynamai](../../strongs/g/g1410.md) to [phylassō](../../strongs/g/g5442.md) you [aptaistos](../../strongs/g/g679.md), and to [histēmi](../../strongs/g/g2476.md) you [amōmos](../../strongs/g/g299.md) before the [katenōpion](../../strongs/g/g2714.md) of his [doxa](../../strongs/g/g1391.md) with [agalliasis](../../strongs/g/g20.md),

<a name="jude_1_25"></a>Jude 1:25

To the [monos](../../strongs/g/g3441.md) [sophos](../../strongs/g/g4680.md) [theos](../../strongs/g/g2316.md) our [sōtēr](../../strongs/g/g4990.md), [doxa](../../strongs/g/g1391.md) and [megalōsynē](../../strongs/g/g3172.md), [kratos](../../strongs/g/g2904.md) and [exousia](../../strongs/g/g1849.md), both [nyn](../../strongs/g/g3568.md) and [eis](../../strongs/g/g1519.md) [pas](../../strongs/g/g3956.md) [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md). [^2]

---

[Transliteral Bible](../bible.md)

[Jude](jude.md)

---

[^1]: [Jude 1:22 Commentary](../../commentary/jude/jude_1_commentary.md#jude_1_22)

[^2]: [Jude 1:25 Commentary](../../commentary/jude/jude_1_commentary.md#jude_1_25)
