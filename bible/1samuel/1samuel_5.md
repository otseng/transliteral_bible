# [1Samuel 5](https://www.blueletterbible.org/kjv/1samuel/5)

<a name="1samuel_5_1"></a>1Samuel 5:1

And the [Pᵊlištî](../../strongs/h/h6430.md) [laqach](../../strongs/h/h3947.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md), and [bow'](../../strongs/h/h935.md) it from ['eḇen hāʿezer](../../strongs/h/h72.md) unto ['Ašdôḏ](../../strongs/h/h795.md).

<a name="1samuel_5_2"></a>1Samuel 5:2

When the [Pᵊlištî](../../strongs/h/h6430.md) [laqach](../../strongs/h/h3947.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md), they [bow'](../../strongs/h/h935.md) it into the [bayith](../../strongs/h/h1004.md) of [Dāḡôn](../../strongs/h/h1712.md), and [yāṣaḡ](../../strongs/h/h3322.md) it by [Dāḡôn](../../strongs/h/h1712.md).

<a name="1samuel_5_3"></a>1Samuel 5:3

And when they of ['ašdôḏî](../../strongs/h/h796.md) [šāḵam](../../strongs/h/h7925.md) on the [māḥŏrāṯ](../../strongs/h/h4283.md), behold, [Dāḡôn](../../strongs/h/h1712.md) was [naphal](../../strongs/h/h5307.md) upon his [paniym](../../strongs/h/h6440.md) to the ['erets](../../strongs/h/h776.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md). And they [laqach](../../strongs/h/h3947.md) [Dāḡôn](../../strongs/h/h1712.md), and [shuwb](../../strongs/h/h7725.md) him in his [maqowm](../../strongs/h/h4725.md) [shuwb](../../strongs/h/h7725.md).

<a name="1samuel_5_4"></a>1Samuel 5:4

And when they [šāḵam](../../strongs/h/h7925.md) on the [māḥŏrāṯ](../../strongs/h/h4283.md) [boqer](../../strongs/h/h1242.md), behold, [Dāḡôn](../../strongs/h/h1712.md) was [naphal](../../strongs/h/h5307.md) upon his [paniym](../../strongs/h/h6440.md) to the ['erets](../../strongs/h/h776.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md); and the [ro'sh](../../strongs/h/h7218.md) of [Dāḡôn](../../strongs/h/h1712.md) and both the [kaph](../../strongs/h/h3709.md) of his [yad](../../strongs/h/h3027.md) were [karath](../../strongs/h/h3772.md) upon the [mip̄tān](../../strongs/h/h4670.md); only the stump of [Dāḡôn](../../strongs/h/h1712.md) was [šā'ar](../../strongs/h/h7604.md) to him.

<a name="1samuel_5_5"></a>1Samuel 5:5

Therefore neither the [kōhēn](../../strongs/h/h3548.md) of [Dāḡôn](../../strongs/h/h1712.md), nor any that [bow'](../../strongs/h/h935.md) into [Dāḡôn](../../strongs/h/h1712.md) [bayith](../../strongs/h/h1004.md), [dāraḵ](../../strongs/h/h1869.md) on the [mip̄tān](../../strongs/h/h4670.md) of [Dāḡôn](../../strongs/h/h1712.md) in ['Ašdôḏ](../../strongs/h/h795.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="1samuel_5_6"></a>1Samuel 5:6

But the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) was [kabad](../../strongs/h/h3513.md) upon them of ['ašdôḏî](../../strongs/h/h796.md), and he [šāmēm](../../strongs/h/h8074.md) them, and [nakah](../../strongs/h/h5221.md) them with [ṭᵊḥôrîm](../../strongs/h/h2914.md) [ʿōp̄el](../../strongs/h/h6076.md), even ['Ašdôḏ](../../strongs/h/h795.md) and the [gᵊḇûl](../../strongs/h/h1366.md) thereof.

<a name="1samuel_5_7"></a>1Samuel 5:7

And when the ['enowsh](../../strongs/h/h582.md) of ['Ašdôḏ](../../strongs/h/h795.md) [ra'ah](../../strongs/h/h7200.md) that it was so, they ['āmar](../../strongs/h/h559.md), The ['ārôn](../../strongs/h/h727.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) shall not [yashab](../../strongs/h/h3427.md) with us: for his [yad](../../strongs/h/h3027.md) is [qāšâ](../../strongs/h/h7185.md) upon us, and upon [Dāḡôn](../../strongs/h/h1712.md) our ['Elohiym](../../strongs/h/h430.md).

<a name="1samuel_5_8"></a>1Samuel 5:8

They [shalach](../../strongs/h/h7971.md) therefore and ['āsap̄](../../strongs/h/h622.md) all the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md) unto them, and ['āmar](../../strongs/h/h559.md), What shall we ['asah](../../strongs/h/h6213.md) with the ['ārôn](../../strongs/h/h727.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md)? And they ['āmar](../../strongs/h/h559.md), Let the ['ārôn](../../strongs/h/h727.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) be [cabab](../../strongs/h/h5437.md) unto [Gaṯ](../../strongs/h/h1661.md). And they [cabab](../../strongs/h/h5437.md) the ['ārôn](../../strongs/h/h727.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [cabab](../../strongs/h/h5437.md) thither.

<a name="1samuel_5_9"></a>1Samuel 5:9

And it was so, that, ['aḥar](../../strongs/h/h310.md) they had [cabab](../../strongs/h/h5437.md) it, the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) was against the [ʿîr](../../strongs/h/h5892.md) with a [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) [mᵊhûmâ](../../strongs/h/h4103.md): and he [nakah](../../strongs/h/h5221.md) the ['enowsh](../../strongs/h/h582.md) of the [ʿîr](../../strongs/h/h5892.md), both [qāṭān](../../strongs/h/h6996.md) and [gadowl](../../strongs/h/h1419.md), and they had [ṭᵊḥôrîm](../../strongs/h/h2914.md) [ʿōp̄el](../../strongs/h/h6076.md) in their [śāṯar](../../strongs/h/h8368.md).

<a name="1samuel_5_10"></a>1Samuel 5:10

Therefore they [shalach](../../strongs/h/h7971.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) to [ʿEqrôn](../../strongs/h/h6138.md). And it came to pass, as the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) to [ʿEqrôn](../../strongs/h/h6138.md), that the [ʿeqrônî](../../strongs/h/h6139.md) [zāʿaq](../../strongs/h/h2199.md), ['āmar](../../strongs/h/h559.md), They have [cabab](../../strongs/h/h5437.md) the ['ārôn](../../strongs/h/h727.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) to us, to [muwth](../../strongs/h/h4191.md) us and our ['am](../../strongs/h/h5971.md).

<a name="1samuel_5_11"></a>1Samuel 5:11

So they [shalach](../../strongs/h/h7971.md) and ['āsap̄](../../strongs/h/h622.md) all the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and ['āmar](../../strongs/h/h559.md), [shalach](../../strongs/h/h7971.md) the ['ārôn](../../strongs/h/h727.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), and let it [shuwb](../../strongs/h/h7725.md) to his own [maqowm](../../strongs/h/h4725.md), that it [muwth](../../strongs/h/h4191.md) us not, and our ['am](../../strongs/h/h5971.md): for there was a [maveth](../../strongs/h/h4194.md) [mᵊhûmâ](../../strongs/h/h4103.md) throughout all the [ʿîr](../../strongs/h/h5892.md); the [yad](../../strongs/h/h3027.md) of ['Elohiym](../../strongs/h/h430.md) was [me'od](../../strongs/h/h3966.md) [kabad](../../strongs/h/h3513.md) there.

<a name="1samuel_5_12"></a>1Samuel 5:12

And the ['enowsh](../../strongs/h/h582.md) that [muwth](../../strongs/h/h4191.md) not were [nakah](../../strongs/h/h5221.md) with the [ṭᵊḥôrîm](../../strongs/h/h2914.md) [ʿōp̄el](../../strongs/h/h6076.md): and the [shav'ah](../../strongs/h/h7775.md) of the [ʿîr](../../strongs/h/h5892.md) [ʿālâ](../../strongs/h/h5927.md) to [shamayim](../../strongs/h/h8064.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 4](1samuel_4.md) - [1Samuel 6](1samuel_6.md)