# [1Samuel 15](https://www.blueletterbible.org/kjv/1samuel/15)

<a name="1samuel_15_1"></a>1Samuel 15:1

[Šᵊmû'Ēl](../../strongs/h/h8050.md) also ['āmar](../../strongs/h/h559.md) unto [Šā'ûl](../../strongs/h/h7586.md), [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) me to [māšaḥ](../../strongs/h/h4886.md) thee to be [melek](../../strongs/h/h4428.md) over his ['am](../../strongs/h/h5971.md), over [Yisra'el](../../strongs/h/h3478.md): now therefore [shama'](../../strongs/h/h8085.md) thou unto the [qowl](../../strongs/h/h6963.md) of the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_15_2"></a>1Samuel 15:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), I [paqad](../../strongs/h/h6485.md) that which [ʿĂmālēq](../../strongs/h/h6002.md) ['asah](../../strongs/h/h6213.md) to [Yisra'el](../../strongs/h/h3478.md), how he [śûm](../../strongs/h/h7760.md) wait for him in the [derek](../../strongs/h/h1870.md), when he [ʿālâ](../../strongs/h/h5927.md) from [Mitsrayim](../../strongs/h/h4714.md).

<a name="1samuel_15_3"></a>1Samuel 15:3

Now [yālaḵ](../../strongs/h/h3212.md) and [nakah](../../strongs/h/h5221.md) [ʿĂmālēq](../../strongs/h/h6002.md), and [ḥāram](../../strongs/h/h2763.md) all that they have, and [ḥāmal](../../strongs/h/h2550.md) them not; but [muwth](../../strongs/h/h4191.md) both ['iysh](../../strongs/h/h376.md) and ['ishshah](../../strongs/h/h802.md), ['owlel](../../strongs/h/h5768.md) and [yānaq](../../strongs/h/h3243.md), [showr](../../strongs/h/h7794.md) and [śê](../../strongs/h/h7716.md), [gāmāl](../../strongs/h/h1581.md) and [chamowr](../../strongs/h/h2543.md).

<a name="1samuel_15_4"></a>1Samuel 15:4

And [Šā'ûl](../../strongs/h/h7586.md) [shama'](../../strongs/h/h8085.md) the ['am](../../strongs/h/h5971.md) [shama'](../../strongs/h/h8085.md), and [paqad](../../strongs/h/h6485.md) them in [Ṭᵊlā'Îm](../../strongs/h/h2923.md), two hundred thousand [raḡlî](../../strongs/h/h7273.md), and ten thousand ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="1samuel_15_5"></a>1Samuel 15:5

And [Šā'ûl](../../strongs/h/h7586.md) [bow'](../../strongs/h/h935.md) to a [ʿîr](../../strongs/h/h5892.md) of [ʿĂmālēq](../../strongs/h/h6002.md), and laid [riyb](../../strongs/h/h7378.md) ['arab](../../strongs/h/h693.md) in the [nachal](../../strongs/h/h5158.md).

<a name="1samuel_15_6"></a>1Samuel 15:6

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto the [Qênî](../../strongs/h/h7017.md), [yālaḵ](../../strongs/h/h3212.md), [cuwr](../../strongs/h/h5493.md), get you [yarad](../../strongs/h/h3381.md) from [tavek](../../strongs/h/h8432.md) the [ʿĂmālēq](../../strongs/h/h6002.md), lest I ['āsap̄](../../strongs/h/h622.md) you with them: for ye ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) to all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), when they [ʿālâ](../../strongs/h/h5927.md) out of [Mitsrayim](../../strongs/h/h4714.md). So the [Qênî](../../strongs/h/h7017.md) [cuwr](../../strongs/h/h5493.md) from [tavek](../../strongs/h/h8432.md) the [ʿămālēqî](../../strongs/h/h6003.md).

<a name="1samuel_15_7"></a>1Samuel 15:7

And [Šā'ûl](../../strongs/h/h7586.md) [nakah](../../strongs/h/h5221.md) the [ʿĂmālēq](../../strongs/h/h6002.md) from [Ḥăvîlâ](../../strongs/h/h2341.md) until thou [bow'](../../strongs/h/h935.md) to [šûr](../../strongs/h/h7793.md), that is over [paniym](../../strongs/h/h6440.md) [Mitsrayim](../../strongs/h/h4714.md).

<a name="1samuel_15_8"></a>1Samuel 15:8

And he [tāp̄aś](../../strongs/h/h8610.md) ['Ăḡaḡ](../../strongs/h/h90.md) the [melek](../../strongs/h/h4428.md) of the [ʿĂmālēq](../../strongs/h/h6002.md) [chay](../../strongs/h/h2416.md), and [ḥāram](../../strongs/h/h2763.md) all the ['am](../../strongs/h/h5971.md) with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md).

<a name="1samuel_15_9"></a>1Samuel 15:9

But [Šā'ûl](../../strongs/h/h7586.md) and the ['am](../../strongs/h/h5971.md) [ḥāmal](../../strongs/h/h2550.md) ['Ăḡaḡ](../../strongs/h/h90.md), and the [mêṭāḇ](../../strongs/h/h4315.md) of the [tso'n](../../strongs/h/h6629.md), and of the [bāqār](../../strongs/h/h1241.md), and of the [mišnê](../../strongs/h/h4932.md), and the [kar](../../strongs/h/h3733.md), and all that was [towb](../../strongs/h/h2896.md), and ['āḇâ](../../strongs/h/h14.md) not [ḥāram](../../strongs/h/h2763.md) them: but every [mĕla'kah](../../strongs/h/h4399.md) that was [nᵊmiḇzâ](../../strongs/h/h5240.md) and [māsas](../../strongs/h/h4549.md), that they [ḥāram](../../strongs/h/h2763.md).

<a name="1samuel_15_10"></a>1Samuel 15:10

Then came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto [Šᵊmû'Ēl](../../strongs/h/h8050.md), ['āmar](../../strongs/h/h559.md),

<a name="1samuel_15_11"></a>1Samuel 15:11

It [nacham](../../strongs/h/h5162.md) me that I have set [mālaḵ](../../strongs/h/h4427.md) [Šā'ûl](../../strongs/h/h7586.md) to be [melek](../../strongs/h/h4428.md): for he is [shuwb](../../strongs/h/h7725.md) from ['aḥar](../../strongs/h/h310.md) me, and hath not [quwm](../../strongs/h/h6965.md) my [dabar](../../strongs/h/h1697.md). And it [ḥārâ](../../strongs/h/h2734.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md); and he [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md) all [layil](../../strongs/h/h3915.md).

<a name="1samuel_15_12"></a>1Samuel 15:12

And when [Šᵊmû'Ēl](../../strongs/h/h8050.md) [šāḵam](../../strongs/h/h7925.md) to [qārā'](../../strongs/h/h7125.md) [Šā'ûl](../../strongs/h/h7586.md) in the [boqer](../../strongs/h/h1242.md), it was [nāḡaḏ](../../strongs/h/h5046.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md), ['āmar](../../strongs/h/h559.md), [Šā'ûl](../../strongs/h/h7586.md) [bow'](../../strongs/h/h935.md) to [Karmel](../../strongs/h/h3760.md), and, behold, he [nāṣaḇ](../../strongs/h/h5324.md) him a [yad](../../strongs/h/h3027.md), and is [cabab](../../strongs/h/h5437.md), and ['abar](../../strongs/h/h5674.md), and [yarad](../../strongs/h/h3381.md) to [Gilgāl](../../strongs/h/h1537.md).

<a name="1samuel_15_13"></a>1Samuel 15:13

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [bow'](../../strongs/h/h935.md) to [Šā'ûl](../../strongs/h/h7586.md): and [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto him, [barak](../../strongs/h/h1288.md) be thou of [Yĕhovah](../../strongs/h/h3068.md): I have [quwm](../../strongs/h/h6965.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_15_14"></a>1Samuel 15:14

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md), What meaneth then this [qowl](../../strongs/h/h6963.md) of the [tso'n](../../strongs/h/h6629.md) in mine ['ozen](../../strongs/h/h241.md), and the [qowl](../../strongs/h/h6963.md) of the [bāqār](../../strongs/h/h1241.md) which I [shama'](../../strongs/h/h8085.md)?

<a name="1samuel_15_15"></a>1Samuel 15:15

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), They have [bow'](../../strongs/h/h935.md) them from the [ʿămālēqî](../../strongs/h/h6003.md): for the ['am](../../strongs/h/h5971.md) [ḥāmal](../../strongs/h/h2550.md) the [mêṭāḇ](../../strongs/h/h4315.md) of the [tso'n](../../strongs/h/h6629.md) and of the [bāqār](../../strongs/h/h1241.md), to [zabach](../../strongs/h/h2076.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md); and the [yāṯar](../../strongs/h/h3498.md) we have [ḥāram](../../strongs/h/h2763.md).

<a name="1samuel_15_16"></a>1Samuel 15:16

Then [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) unto [Šā'ûl](../../strongs/h/h7586.md), [rāp̄â](../../strongs/h/h7503.md), and I will [nāḡaḏ](../../strongs/h/h5046.md) thee what [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) to me this [layil](../../strongs/h/h3915.md). And he ['āmar](../../strongs/h/h559.md) unto him, [dabar](../../strongs/h/h1696.md).

<a name="1samuel_15_17"></a>1Samuel 15:17

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md), When thou wast [qāṭān](../../strongs/h/h6996.md) in thine own ['ayin](../../strongs/h/h5869.md), wast thou not made the [ro'sh](../../strongs/h/h7218.md) of the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), and [Yĕhovah](../../strongs/h/h3068.md) [māšaḥ](../../strongs/h/h4886.md) thee [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md)?

<a name="1samuel_15_18"></a>1Samuel 15:18

And [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) thee on a [derek](../../strongs/h/h1870.md), and ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) and [ḥāram](../../strongs/h/h2763.md) the [chatta'](../../strongs/h/h2400.md) the [ʿĂmālēq](../../strongs/h/h6002.md), and [lāḥam](../../strongs/h/h3898.md) against them until they be [kalah](../../strongs/h/h3615.md).

<a name="1samuel_15_19"></a>1Samuel 15:19

Wherefore then didst thou not [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md), but didst [ʿîṭ](../../strongs/h/h5860.md) upon the [šālāl](../../strongs/h/h7998.md), and ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md)?

<a name="1samuel_15_20"></a>1Samuel 15:20

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto [Šᵊmû'Ēl](../../strongs/h/h8050.md), Yea, I have [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md), and have [yālaḵ](../../strongs/h/h3212.md) the [derek](../../strongs/h/h1870.md) which [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) me, and have [bow'](../../strongs/h/h935.md) ['Ăḡaḡ](../../strongs/h/h90.md) the [melek](../../strongs/h/h4428.md) of [ʿĂmālēq](../../strongs/h/h6002.md), and have [ḥāram](../../strongs/h/h2763.md) the [ʿĂmālēq](../../strongs/h/h6002.md).

<a name="1samuel_15_21"></a>1Samuel 15:21

But the ['am](../../strongs/h/h5971.md) [laqach](../../strongs/h/h3947.md) of the [šālāl](../../strongs/h/h7998.md), [tso'n](../../strongs/h/h6629.md) and [bāqār](../../strongs/h/h1241.md), the [re'shiyth](../../strongs/h/h7225.md) of the things which should have been utterly [ḥērem](../../strongs/h/h2764.md), to [zabach](../../strongs/h/h2076.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) in [Gilgāl](../../strongs/h/h1537.md).

<a name="1samuel_15_22"></a>1Samuel 15:22

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md), Hath [Yĕhovah](../../strongs/h/h3068.md) as [chephets](../../strongs/h/h2656.md) in [ʿōlâ](../../strongs/h/h5930.md) and [zebach](../../strongs/h/h2077.md), as in [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md)? Behold, to [shama'](../../strongs/h/h8085.md) is [towb](../../strongs/h/h2896.md) than [zebach](../../strongs/h/h2077.md), and to [qashab](../../strongs/h/h7181.md) than the [cheleb](../../strongs/h/h2459.md) of ['ayil](../../strongs/h/h352.md).

<a name="1samuel_15_23"></a>1Samuel 15:23

For [mᵊrî](../../strongs/h/h4805.md) is as the [chatta'ath](../../strongs/h/h2403.md) of [qesem](../../strongs/h/h7081.md), and [pāṣar](../../strongs/h/h6484.md) is as ['aven](../../strongs/h/h205.md) and [tᵊrāp̄îm](../../strongs/h/h8655.md). Because thou hast [mā'as](../../strongs/h/h3988.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), he hath also [mā'as](../../strongs/h/h3988.md) thee from being [melek](../../strongs/h/h4428.md).

<a name="1samuel_15_24"></a>1Samuel 15:24

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto [Šᵊmû'Ēl](../../strongs/h/h8050.md), I have [chata'](../../strongs/h/h2398.md): for I have ['abar](../../strongs/h/h5674.md) the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md), and thy [dabar](../../strongs/h/h1697.md): because I [yare'](../../strongs/h/h3372.md) the ['am](../../strongs/h/h5971.md), and [shama'](../../strongs/h/h8085.md) their [qowl](../../strongs/h/h6963.md).

<a name="1samuel_15_25"></a>1Samuel 15:25

Now therefore, I pray thee, [nasa'](../../strongs/h/h5375.md) my [chatta'ath](../../strongs/h/h2403.md), and [shuwb](../../strongs/h/h7725.md) with me, that I may [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_15_26"></a>1Samuel 15:26

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) unto [Šā'ûl](../../strongs/h/h7586.md), I will not [shuwb](../../strongs/h/h7725.md) with thee: for thou hast [mā'as](../../strongs/h/h3988.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), and [Yĕhovah](../../strongs/h/h3068.md) hath [mā'as](../../strongs/h/h3988.md) thee from being [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md).

<a name="1samuel_15_27"></a>1Samuel 15:27

And as [Šᵊmû'Ēl](../../strongs/h/h8050.md) turned [cabab](../../strongs/h/h5437.md) to go [yālaḵ](../../strongs/h/h3212.md), he laid [ḥāzaq](../../strongs/h/h2388.md) upon the [kanaph](../../strongs/h/h3671.md) of his [mᵊʿîl](../../strongs/h/h4598.md), and it [qāraʿ](../../strongs/h/h7167.md).

<a name="1samuel_15_28"></a>1Samuel 15:28

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) unto him, [Yĕhovah](../../strongs/h/h3068.md) hath [qāraʿ](../../strongs/h/h7167.md) the [mamlāḵûṯ](../../strongs/h/h4468.md) of [Yisra'el](../../strongs/h/h3478.md) from thee this [yowm](../../strongs/h/h3117.md), and hath [nathan](../../strongs/h/h5414.md) it to a [rea'](../../strongs/h/h7453.md) of thine, that is [towb](../../strongs/h/h2896.md) than thou.

<a name="1samuel_15_29"></a>1Samuel 15:29

And also the [netsach](../../strongs/h/h5331.md) of [Yisra'el](../../strongs/h/h3478.md) will not [šāqar](../../strongs/h/h8266.md) nor [nacham](../../strongs/h/h5162.md): for he is not an ['āḏām](../../strongs/h/h120.md), that he should [nacham](../../strongs/h/h5162.md).

<a name="1samuel_15_30"></a>1Samuel 15:30

Then he ['āmar](../../strongs/h/h559.md), I have [chata'](../../strongs/h/h2398.md): yet [kabad](../../strongs/h/h3513.md) me now, I pray thee, before the [zāqēn](../../strongs/h/h2205.md) of my ['am](../../strongs/h/h5971.md), and before [Yisra'el](../../strongs/h/h3478.md), and [shuwb](../../strongs/h/h7725.md) with me, that I may [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="1samuel_15_31"></a>1Samuel 15:31

So [Šᵊmû'Ēl](../../strongs/h/h8050.md) [shuwb](../../strongs/h/h7725.md) ['aḥar](../../strongs/h/h310.md) [Šā'ûl](../../strongs/h/h7586.md); and [Šā'ûl](../../strongs/h/h7586.md) [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_15_32"></a>1Samuel 15:32

Then ['āmar](../../strongs/h/h559.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md), Bring ye [nāḡaš](../../strongs/h/h5066.md) to me ['Ăḡaḡ](../../strongs/h/h90.md) the [melek](../../strongs/h/h4428.md) of the [ʿĂmālēq](../../strongs/h/h6002.md). And ['Ăḡaḡ](../../strongs/h/h90.md) [yālaḵ](../../strongs/h/h3212.md) unto him [maʿăḏān](../../strongs/h/h4574.md). And ['Ăḡaḡ](../../strongs/h/h90.md) ['āmar](../../strongs/h/h559.md), ['āḵēn](../../strongs/h/h403.md) the [mar](../../strongs/h/h4751.md) of [maveth](../../strongs/h/h4194.md) is [cuwr](../../strongs/h/h5493.md).

<a name="1samuel_15_33"></a>1Samuel 15:33

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md), As thy [chereb](../../strongs/h/h2719.md) hath made ['ishshah](../../strongs/h/h802.md) [šāḵōl](../../strongs/h/h7921.md), so shall thy ['em](../../strongs/h/h517.md) be [šāḵōl](../../strongs/h/h7921.md) among ['ishshah](../../strongs/h/h802.md). And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [šāsap̄](../../strongs/h/h8158.md) ['Ăḡaḡ](../../strongs/h/h90.md) in [šāsap̄](../../strongs/h/h8158.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) in [Gilgāl](../../strongs/h/h1537.md).

<a name="1samuel_15_34"></a>1Samuel 15:34

Then [Šᵊmû'Ēl](../../strongs/h/h8050.md) [yālaḵ](../../strongs/h/h3212.md) to [rāmâ](../../strongs/h/h7414.md); and [Šā'ûl](../../strongs/h/h7586.md) [ʿālâ](../../strongs/h/h5927.md) to his [bayith](../../strongs/h/h1004.md) to [giḇʿâ](../../strongs/h/h1390.md) of [Šā'ûl](../../strongs/h/h7586.md).

<a name="1samuel_15_35"></a>1Samuel 15:35

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) came no more to [ra'ah](../../strongs/h/h7200.md) [Šā'ûl](../../strongs/h/h7586.md) until the [yowm](../../strongs/h/h3117.md) of his [maveth](../../strongs/h/h4194.md): nevertheless [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āḇal](../../strongs/h/h56.md) for [Šā'ûl](../../strongs/h/h7586.md): and [Yĕhovah](../../strongs/h/h3068.md) [nacham](../../strongs/h/h5162.md) that he had made [Šā'ûl](../../strongs/h/h7586.md) [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 14](1samuel_14.md) - [1Samuel 16](1samuel_16.md)