# [1Samuel 13](https://www.blueletterbible.org/kjv/1samuel/13)

<a name="1samuel_13_1"></a>1Samuel 13:1

[Šā'ûl](../../strongs/h/h7586.md) [mālaḵ](../../strongs/h/h4427.md) one [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md); and when he had [mālaḵ](../../strongs/h/h4427.md) two [šānâ](../../strongs/h/h8141.md) over [Yisra'el](../../strongs/h/h3478.md),

<a name="1samuel_13_2"></a>1Samuel 13:2

[Šā'ûl](../../strongs/h/h7586.md) [bāḥar](../../strongs/h/h977.md) him three thousand men of [Yisra'el](../../strongs/h/h3478.md); whereof two thousand were with [Šā'ûl](../../strongs/h/h7586.md) in [Miḵmās](../../strongs/h/h4363.md) and in [har](../../strongs/h/h2022.md) [Bêṯ-'ēl](../../strongs/h/h1008.md), and a thousand were with [Yônāṯān](../../strongs/h/h3129.md) in [giḇʿâ](../../strongs/h/h1390.md) of [Binyāmîn](../../strongs/h/h1144.md): and the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md) he [shalach](../../strongs/h/h7971.md) every ['iysh](../../strongs/h/h376.md) to his ['ohel](../../strongs/h/h168.md).

<a name="1samuel_13_3"></a>1Samuel 13:3

And [Yônāṯān](../../strongs/h/h3129.md) [nakah](../../strongs/h/h5221.md) the [nᵊṣîḇ](../../strongs/h/h5333.md) of the [Pᵊlištî](../../strongs/h/h6430.md) that was in [Geḇaʿ](../../strongs/h/h1387.md), and the [Pᵊlištî](../../strongs/h/h6430.md) [shama'](../../strongs/h/h8085.md) of it. And [Šā'ûl](../../strongs/h/h7586.md) [tāqaʿ](../../strongs/h/h8628.md) the [šôp̄ār](../../strongs/h/h7782.md) throughout all the ['erets](../../strongs/h/h776.md), ['āmar](../../strongs/h/h559.md), Let the [ʿiḇrî](../../strongs/h/h5680.md) [shama'](../../strongs/h/h8085.md).

<a name="1samuel_13_4"></a>1Samuel 13:4

And all [Yisra'el](../../strongs/h/h3478.md) [shama'](../../strongs/h/h8085.md) ['āmar](../../strongs/h/h559.md) that [Šā'ûl](../../strongs/h/h7586.md) had [nakah](../../strongs/h/h5221.md) a [nᵊṣîḇ](../../strongs/h/h5333.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and that [Yisra'el](../../strongs/h/h3478.md) also was had in [bā'aš](../../strongs/h/h887.md) with the [Pᵊlištî](../../strongs/h/h6430.md). And the ['am](../../strongs/h/h5971.md) were called [ṣāʿaq](../../strongs/h/h6817.md) ['aḥar](../../strongs/h/h310.md) [Šā'ûl](../../strongs/h/h7586.md) to [Gilgāl](../../strongs/h/h1537.md).

<a name="1samuel_13_5"></a>1Samuel 13:5

And the [Pᵊlištî](../../strongs/h/h6430.md) ['āsap̄](../../strongs/h/h622.md) themselves to [lāḥam](../../strongs/h/h3898.md) with [Yisra'el](../../strongs/h/h3478.md), thirty thousand [reḵeḇ](../../strongs/h/h7393.md), and six thousand [pārāš](../../strongs/h/h6571.md), and ['am](../../strongs/h/h5971.md) as the [ḥôl](../../strongs/h/h2344.md) which is on the [yam](../../strongs/h/h3220.md) [saphah](../../strongs/h/h8193.md) in [rōḇ](../../strongs/h/h7230.md): and they [ʿālâ](../../strongs/h/h5927.md), and [ḥānâ](../../strongs/h/h2583.md) in [Miḵmās](../../strongs/h/h4363.md), [qḏmh](../../strongs/h/h6926.md) from [Bêṯ 'Āven](../../strongs/h/h1007.md).

<a name="1samuel_13_6"></a>1Samuel 13:6

When the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [ra'ah](../../strongs/h/h7200.md) that they were in a [tsarar](../../strongs/h/h6887.md), (for the ['am](../../strongs/h/h5971.md) were [nāḡaś](../../strongs/h/h5065.md),) then the ['am](../../strongs/h/h5971.md) did [chaba'](../../strongs/h/h2244.md) themselves in [mᵊʿārâ](../../strongs/h/h4631.md), and in [ḥāvāḥ](../../strongs/h/h2337.md), and in [cela'](../../strongs/h/h5553.md), and in high [ṣārîaḥ](../../strongs/h/h6877.md), and in [bowr](../../strongs/h/h953.md).

<a name="1samuel_13_7"></a>1Samuel 13:7

And some of the [ʿiḇrî](../../strongs/h/h5680.md) ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) to the ['erets](../../strongs/h/h776.md) of [Gāḏ](../../strongs/h/h1410.md) and [Gilʿāḏ](../../strongs/h/h1568.md). As for [Šā'ûl](../../strongs/h/h7586.md), he was yet in [Gilgāl](../../strongs/h/h1537.md), and all the ['am](../../strongs/h/h5971.md) ['aḥar](../../strongs/h/h310.md) him [ḥārēḏ](../../strongs/h/h2729.md).

<a name="1samuel_13_8"></a>1Samuel 13:8

And he [yāḥal](../../strongs/h/h3176.md) [yāḥal](../../strongs/h/h3176.md) seven [yowm](../../strongs/h/h3117.md), according to the set [môʿēḏ](../../strongs/h/h4150.md) that [Šᵊmû'Ēl](../../strongs/h/h8050.md) had appointed: but [Šᵊmû'Ēl](../../strongs/h/h8050.md) [bow'](../../strongs/h/h935.md) not to [Gilgāl](../../strongs/h/h1537.md); and the ['am](../../strongs/h/h5971.md) were [puwts](../../strongs/h/h6327.md) from him.

<a name="1samuel_13_9"></a>1Samuel 13:9

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), Bring [nāḡaš](../../strongs/h/h5066.md) a [ʿōlâ](../../strongs/h/h5930.md) to me, and [šelem](../../strongs/h/h8002.md). And he [ʿālâ](../../strongs/h/h5927.md) the [ʿōlâ](../../strongs/h/h5930.md).

<a name="1samuel_13_10"></a>1Samuel 13:10

And it came to pass, that as soon as he had made a [kalah](../../strongs/h/h3615.md) of [ʿālâ](../../strongs/h/h5927.md) the [ʿōlâ](../../strongs/h/h5930.md), behold, [Šᵊmû'Ēl](../../strongs/h/h8050.md) [bow'](../../strongs/h/h935.md); and [Šā'ûl](../../strongs/h/h7586.md) [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) him, that he might [barak](../../strongs/h/h1288.md) him.

<a name="1samuel_13_11"></a>1Samuel 13:11

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md), What hast thou ['asah](../../strongs/h/h6213.md)? And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), Because I [ra'ah](../../strongs/h/h7200.md) that the ['am](../../strongs/h/h5971.md) were [naphats](../../strongs/h/h5310.md) from me, and that thou [bow'](../../strongs/h/h935.md) not within the [yowm](../../strongs/h/h3117.md) [môʿēḏ](../../strongs/h/h4150.md), and that the [Pᵊlištî](../../strongs/h/h6430.md) ['āsap̄](../../strongs/h/h622.md) themselves at [Miḵmās](../../strongs/h/h4363.md);

<a name="1samuel_13_12"></a>1Samuel 13:12

Therefore ['āmar](../../strongs/h/h559.md) I, The [Pᵊlištî](../../strongs/h/h6430.md) will [yarad](../../strongs/h/h3381.md) now upon me to [Gilgāl](../../strongs/h/h1537.md), and I have not made [ḥālâ](../../strongs/h/h2470.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): I ['āp̄aq](../../strongs/h/h662.md) myself therefore, and [ʿālâ](../../strongs/h/h5927.md) a [ʿōlâ](../../strongs/h/h5930.md).

<a name="1samuel_13_13"></a>1Samuel 13:13

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) to [Šā'ûl](../../strongs/h/h7586.md), Thou hast done [sāḵal](../../strongs/h/h5528.md): thou hast not [shamar](../../strongs/h/h8104.md) the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), which he [tsavah](../../strongs/h/h6680.md) thee: for now would [Yĕhovah](../../strongs/h/h3068.md) have [kuwn](../../strongs/h/h3559.md) thy [mamlāḵâ](../../strongs/h/h4467.md) upon [Yisra'el](../../strongs/h/h3478.md) ['owlam](../../strongs/h/h5769.md).

<a name="1samuel_13_14"></a>1Samuel 13:14

But now thy [mamlāḵâ](../../strongs/h/h4467.md) shall not [quwm](../../strongs/h/h6965.md): [Yĕhovah](../../strongs/h/h3068.md) hath [bāqaš](../../strongs/h/h1245.md) him an ['iysh](../../strongs/h/h376.md) after his own [lebab](../../strongs/h/h3824.md), and [Yĕhovah](../../strongs/h/h3068.md) hath [tsavah](../../strongs/h/h6680.md) him to be [nāḡîḏ](../../strongs/h/h5057.md) over his ['am](../../strongs/h/h5971.md), because thou hast not [shamar](../../strongs/h/h8104.md) that which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) thee.

<a name="1samuel_13_15"></a>1Samuel 13:15

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [quwm](../../strongs/h/h6965.md), and gat him [ʿālâ](../../strongs/h/h5927.md) from [Gilgāl](../../strongs/h/h1537.md) unto [giḇʿâ](../../strongs/h/h1390.md) of [Binyāmîn](../../strongs/h/h1144.md). And [Šā'ûl](../../strongs/h/h7586.md) [paqad](../../strongs/h/h6485.md) the ['am](../../strongs/h/h5971.md) that were [māṣā'](../../strongs/h/h4672.md) with him, about six hundred ['iysh](../../strongs/h/h376.md).

<a name="1samuel_13_16"></a>1Samuel 13:16

And [Šā'ûl](../../strongs/h/h7586.md), and [Yônāṯān](../../strongs/h/h3129.md) his [ben](../../strongs/h/h1121.md), and the ['am](../../strongs/h/h5971.md) that were [māṣā'](../../strongs/h/h4672.md) with them, [yashab](../../strongs/h/h3427.md) in [Geḇaʿ](../../strongs/h/h1387.md) of [Binyāmîn](../../strongs/h/h1144.md): but the [Pᵊlištî](../../strongs/h/h6430.md) [ḥānâ](../../strongs/h/h2583.md) in [Miḵmās](../../strongs/h/h4363.md).

<a name="1samuel_13_17"></a>1Samuel 13:17

And the [shachath](../../strongs/h/h7843.md) [yāṣā'](../../strongs/h/h3318.md) of the [maḥănê](../../strongs/h/h4264.md) of the [Pᵊlištî](../../strongs/h/h6430.md) in three [ro'sh](../../strongs/h/h7218.md): one [ro'sh](../../strongs/h/h7218.md) [panah](../../strongs/h/h6437.md) unto the [derek](../../strongs/h/h1870.md) that leadeth to [ʿĀp̄Râ](../../strongs/h/h6084.md), unto the ['erets](../../strongs/h/h776.md) of [ŠûʿĀl](../../strongs/h/h7777.md):

<a name="1samuel_13_18"></a>1Samuel 13:18

And another [ro'sh](../../strongs/h/h7218.md) [panah](../../strongs/h/h6437.md) the [derek](../../strongs/h/h1870.md) to [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md): and another [ro'sh](../../strongs/h/h7218.md) [panah](../../strongs/h/h6437.md) to the [derek](../../strongs/h/h1870.md) of the [gᵊḇûl](../../strongs/h/h1366.md) that [šāqap̄](../../strongs/h/h8259.md) to the [gay'](../../strongs/h/h1516.md) of [ṢᵊḇōʿÎm](../../strongs/h/h6650.md) toward the [midbar](../../strongs/h/h4057.md).

<a name="1samuel_13_19"></a>1Samuel 13:19

Now there was no [ḥārāš](../../strongs/h/h2796.md) [māṣā'](../../strongs/h/h4672.md) throughout all the ['erets](../../strongs/h/h776.md) of [Yisra'el](../../strongs/h/h3478.md): for the [Pᵊlištî](../../strongs/h/h6430.md) ['āmar](../../strongs/h/h559.md), Lest the [ʿiḇrî](../../strongs/h/h5680.md) ['asah](../../strongs/h/h6213.md) them [chereb](../../strongs/h/h2719.md) or [ḥănîṯ](../../strongs/h/h2595.md):

<a name="1samuel_13_20"></a>1Samuel 13:20

But all the [Yisra'el](../../strongs/h/h3478.md) [yarad](../../strongs/h/h3381.md) to the [Pᵊlištî](../../strongs/h/h6430.md), to [lāṭaš](../../strongs/h/h3913.md) every ['iysh](../../strongs/h/h376.md) his [maḥărešeṯ](../../strongs/h/h4282.md), and his ['ēṯ](../../strongs/h/h855.md), and his [qardōm](../../strongs/h/h7134.md), and his [maḥărēšâ](../../strongs/h/h4281.md).

<a name="1samuel_13_21"></a>1Samuel 13:21

Yet they had a [pᵊṣîrâ](../../strongs/h/h6477.md) [peh](../../strongs/h/h6310.md) for the [maḥărēšâ](../../strongs/h/h4281.md), and for the ['ēṯ](../../strongs/h/h855.md), and for the forks H7053, and for the [qardōm](../../strongs/h/h7134.md), and to [nāṣaḇ](../../strongs/h/h5324.md) the [dārḇôn](../../strongs/h/h1861.md).

<a name="1samuel_13_22"></a>1Samuel 13:22

So it came to pass in the [yowm](../../strongs/h/h3117.md) of [milḥāmâ](../../strongs/h/h4421.md), that there was neither [chereb](../../strongs/h/h2719.md) nor [ḥănîṯ](../../strongs/h/h2595.md) [māṣā'](../../strongs/h/h4672.md) in the [yad](../../strongs/h/h3027.md) of any of the ['am](../../strongs/h/h5971.md) that were with [Šā'ûl](../../strongs/h/h7586.md) and [Yônāṯān](../../strongs/h/h3129.md): but with [Šā'ûl](../../strongs/h/h7586.md) and with [Yônāṯān](../../strongs/h/h3129.md) his [ben](../../strongs/h/h1121.md) was there [māṣā'](../../strongs/h/h4672.md).

<a name="1samuel_13_23"></a>1Samuel 13:23

And the [maṣṣāḇ](../../strongs/h/h4673.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [yāṣā'](../../strongs/h/h3318.md) to the [maʿăḇār](../../strongs/h/h4569.md) of [Miḵmās](../../strongs/h/h4363.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 12](1samuel_12.md) - [1Samuel 14](1samuel_14.md)