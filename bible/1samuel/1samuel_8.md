# [1Samuel 8](https://www.blueletterbible.org/kjv/1samuel/8)

<a name="1samuel_8_1"></a>1Samuel 8:1

And it came to pass, when [Šᵊmû'Ēl](../../strongs/h/h8050.md) was [zāqēn](../../strongs/h/h2204.md), that he [śûm](../../strongs/h/h7760.md) his [ben](../../strongs/h/h1121.md) [shaphat](../../strongs/h/h8199.md) over [Yisra'el](../../strongs/h/h3478.md).

<a name="1samuel_8_2"></a>1Samuel 8:2

Now the [shem](../../strongs/h/h8034.md) of his [ben](../../strongs/h/h1121.md) [bᵊḵôr](../../strongs/h/h1060.md) was [Yô'Ēl](../../strongs/h/h3100.md); and the [shem](../../strongs/h/h8034.md) of his [mišnê](../../strongs/h/h4932.md), ['Ăḇîâ](../../strongs/h/h29.md): they were [shaphat](../../strongs/h/h8199.md) in [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md).

<a name="1samuel_8_3"></a>1Samuel 8:3

And his [ben](../../strongs/h/h1121.md) [halak](../../strongs/h/h1980.md) not in his [derek](../../strongs/h/h1870.md), but [natah](../../strongs/h/h5186.md) ['aḥar](../../strongs/h/h310.md) [beṣaʿ](../../strongs/h/h1215.md), and [laqach](../../strongs/h/h3947.md) [shachad](../../strongs/h/h7810.md), and [natah](../../strongs/h/h5186.md) [mishpat](../../strongs/h/h4941.md).

<a name="1samuel_8_4"></a>1Samuel 8:4

Then all the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md) gathered themselves [qāḇaṣ](../../strongs/h/h6908.md), and [bow'](../../strongs/h/h935.md) to [Šᵊmû'Ēl](../../strongs/h/h8050.md) unto [rāmâ](../../strongs/h/h7414.md),

<a name="1samuel_8_5"></a>1Samuel 8:5

And ['āmar](../../strongs/h/h559.md) unto him, Behold, thou art [zāqēn](../../strongs/h/h2204.md), and thy [ben](../../strongs/h/h1121.md) [halak](../../strongs/h/h1980.md) not in thy [derek](../../strongs/h/h1870.md): now [śûm](../../strongs/h/h7760.md) us a [melek](../../strongs/h/h4428.md) to [shaphat](../../strongs/h/h8199.md) us like all the [gowy](../../strongs/h/h1471.md).

<a name="1samuel_8_6"></a>1Samuel 8:6

But the [dabar](../../strongs/h/h1697.md) [yāraʿ](../../strongs/h/h3415.md) ['ayin](../../strongs/h/h5869.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md), when they ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) us a [melek](../../strongs/h/h4428.md) to [shaphat](../../strongs/h/h8199.md) us. And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_8_7"></a>1Samuel 8:7

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Šᵊmû'Ēl](../../strongs/h/h8050.md), [shama'](../../strongs/h/h8085.md) unto the [qowl](../../strongs/h/h6963.md) of the ['am](../../strongs/h/h5971.md) in all that they ['āmar](../../strongs/h/h559.md) unto thee: for they have not [mā'as](../../strongs/h/h3988.md) thee, but they have [mā'as](../../strongs/h/h3988.md) me, that I should not [mālaḵ](../../strongs/h/h4427.md) over them.

<a name="1samuel_8_8"></a>1Samuel 8:8

According to all the [ma'aseh](../../strongs/h/h4639.md) which they have ['asah](../../strongs/h/h6213.md) since the [yowm](../../strongs/h/h3117.md) that I [ʿālâ](../../strongs/h/h5927.md) them out of [Mitsrayim](../../strongs/h/h4714.md) even unto this [yowm](../../strongs/h/h3117.md), wherewith they have ['azab](../../strongs/h/h5800.md) me, and ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), so ['asah](../../strongs/h/h6213.md) they also unto thee.

<a name="1samuel_8_9"></a>1Samuel 8:9

Now therefore [shama'](../../strongs/h/h8085.md) unto their [qowl](../../strongs/h/h6963.md): howbeit yet [ʿûḏ](../../strongs/h/h5749.md) [ʿûḏ](../../strongs/h/h5749.md) unto them, and [nāḡaḏ](../../strongs/h/h5046.md) them the [mishpat](../../strongs/h/h4941.md) of the [melek](../../strongs/h/h4428.md) that shall [mālaḵ](../../strongs/h/h4427.md) over them.

<a name="1samuel_8_10"></a>1Samuel 8:10

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) all the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto the ['am](../../strongs/h/h5971.md) that [sha'al](../../strongs/h/h7592.md) of him a [melek](../../strongs/h/h4428.md).

<a name="1samuel_8_11"></a>1Samuel 8:11

And he ['āmar](../../strongs/h/h559.md), This will be the [mishpat](../../strongs/h/h4941.md) of the [melek](../../strongs/h/h4428.md) that shall [mālaḵ](../../strongs/h/h4427.md) over you: He will [laqach](../../strongs/h/h3947.md) your [ben](../../strongs/h/h1121.md), and [śûm](../../strongs/h/h7760.md) them for himself, for his [merkāḇâ](../../strongs/h/h4818.md), and to be his [pārāš](../../strongs/h/h6571.md); and some shall [rûṣ](../../strongs/h/h7323.md) [paniym](../../strongs/h/h6440.md) his [merkāḇâ](../../strongs/h/h4818.md).

<a name="1samuel_8_12"></a>1Samuel 8:12

And he will [śûm](../../strongs/h/h7760.md) him [śar](../../strongs/h/h8269.md) over thousands, and [śar](../../strongs/h/h8269.md) over fifties; and will set them to [ḥāraš](../../strongs/h/h2790.md) his [ḥārîš](../../strongs/h/h2758.md), and to [qāṣar](../../strongs/h/h7114.md) his [qāṣîr](../../strongs/h/h7105.md), and to ['asah](../../strongs/h/h6213.md) his [kĕliy](../../strongs/h/h3627.md) of [milḥāmâ](../../strongs/h/h4421.md), and [kĕliy](../../strongs/h/h3627.md) of his [reḵeḇ](../../strongs/h/h7393.md).

<a name="1samuel_8_13"></a>1Samuel 8:13

And he will [laqach](../../strongs/h/h3947.md) your [bath](../../strongs/h/h1323.md) to be [rqḥh](../../strongs/h/h7548.md), and to be [ṭabāḥâ](../../strongs/h/h2879.md), and to be ['āp̄â](../../strongs/h/h644.md).

<a name="1samuel_8_14"></a>1Samuel 8:14

And he will [laqach](../../strongs/h/h3947.md) your [sadeh](../../strongs/h/h7704.md), and your [kerem](../../strongs/h/h3754.md), and your [zayiṯ](../../strongs/h/h2132.md), even the [towb](../../strongs/h/h2896.md) of them, and [nathan](../../strongs/h/h5414.md) them to his ['ebed](../../strongs/h/h5650.md).

<a name="1samuel_8_15"></a>1Samuel 8:15

And he will take the [ʿāśar](../../strongs/h/h6237.md) of your [zera'](../../strongs/h/h2233.md), and of your [kerem](../../strongs/h/h3754.md), and [nathan](../../strongs/h/h5414.md) to his [sārîs](../../strongs/h/h5631.md), and to his ['ebed](../../strongs/h/h5650.md).

<a name="1samuel_8_16"></a>1Samuel 8:16

And he will [laqach](../../strongs/h/h3947.md) your ['ebed](../../strongs/h/h5650.md), and your [šip̄ḥâ](../../strongs/h/h8198.md), and your [towb](../../strongs/h/h2896.md) [bāḥûr](../../strongs/h/h970.md), and your [chamowr](../../strongs/h/h2543.md), and ['asah](../../strongs/h/h6213.md) them to his [mĕla'kah](../../strongs/h/h4399.md).

<a name="1samuel_8_17"></a>1Samuel 8:17

He will take the [ʿāśar](../../strongs/h/h6237.md) of your [tso'n](../../strongs/h/h6629.md): and ye shall be his ['ebed](../../strongs/h/h5650.md).

<a name="1samuel_8_18"></a>1Samuel 8:18

And ye shall [zāʿaq](../../strongs/h/h2199.md) in that [yowm](../../strongs/h/h3117.md) [paniym](../../strongs/h/h6440.md) of your [melek](../../strongs/h/h4428.md) which ye shall have [bāḥar](../../strongs/h/h977.md) you; and [Yĕhovah](../../strongs/h/h3068.md) will not ['anah](../../strongs/h/h6030.md) you in that [yowm](../../strongs/h/h3117.md).

<a name="1samuel_8_19"></a>1Samuel 8:19

Nevertheless the ['am](../../strongs/h/h5971.md) [mā'ēn](../../strongs/h/h3985.md) to [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Šᵊmû'Ēl](../../strongs/h/h8050.md); and they ['āmar](../../strongs/h/h559.md), Nay; but we will have a [melek](../../strongs/h/h4428.md) over us;

<a name="1samuel_8_20"></a>1Samuel 8:20

That we also may be like all the [gowy](../../strongs/h/h1471.md); and that our [melek](../../strongs/h/h4428.md) may [shaphat](../../strongs/h/h8199.md) us, and [yāṣā'](../../strongs/h/h3318.md) [paniym](../../strongs/h/h6440.md) us, and [lāḥam](../../strongs/h/h3898.md) our [milḥāmâ](../../strongs/h/h4421.md).

<a name="1samuel_8_21"></a>1Samuel 8:21

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [shama'](../../strongs/h/h8085.md) all the [dabar](../../strongs/h/h1697.md) of the ['am](../../strongs/h/h5971.md), and he [dabar](../../strongs/h/h1696.md) them in the ['ozen](../../strongs/h/h241.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_8_22"></a>1Samuel 8:22

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) to [Šᵊmû'Ēl](../../strongs/h/h8050.md), [shama'](../../strongs/h/h8085.md) unto their [qowl](../../strongs/h/h6963.md), and [mālaḵ](../../strongs/h/h4427.md) them a [melek](../../strongs/h/h4428.md). And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) unto the ['enowsh](../../strongs/h/h582.md) of [Yisra'el](../../strongs/h/h3478.md), [yālaḵ](../../strongs/h/h3212.md) ye every ['iysh](../../strongs/h/h376.md) unto his [ʿîr](../../strongs/h/h5892.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 7](1samuel_7.md) - [1Samuel 9](1samuel_9.md)