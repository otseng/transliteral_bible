# [1Samuel 28](https://www.blueletterbible.org/kjv/1samuel/28)

<a name="1samuel_28_1"></a>1Samuel 28:1

And it came to pass in those [yowm](../../strongs/h/h3117.md), that the [Pᵊlištî](../../strongs/h/h6430.md) [qāḇaṣ](../../strongs/h/h6908.md) their [maḥănê](../../strongs/h/h4264.md) [qāḇaṣ](../../strongs/h/h6908.md) for [tsaba'](../../strongs/h/h6635.md), to [lāḥam](../../strongs/h/h3898.md) with [Yisra'el](../../strongs/h/h3478.md). And ['Āḵîš](../../strongs/h/h397.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), [yada'](../../strongs/h/h3045.md) thou [yada'](../../strongs/h/h3045.md), that thou shalt [yāṣā'](../../strongs/h/h3318.md) with me to [maḥănê](../../strongs/h/h4264.md), thou and thy ['enowsh](../../strongs/h/h582.md).

<a name="1samuel_28_2"></a>1Samuel 28:2

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to ['Āḵîš](../../strongs/h/h397.md), Surely thou shalt [yada'](../../strongs/h/h3045.md) what thy ['ebed](../../strongs/h/h5650.md) can ['asah](../../strongs/h/h6213.md). And ['Āḵîš](../../strongs/h/h397.md) ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), Therefore will I [śûm](../../strongs/h/h7760.md) thee [shamar](../../strongs/h/h8104.md) of mine [ro'sh](../../strongs/h/h7218.md) for [yowm](../../strongs/h/h3117.md).

<a name="1samuel_28_3"></a>1Samuel 28:3

Now [Šᵊmû'Ēl](../../strongs/h/h8050.md) was [muwth](../../strongs/h/h4191.md), and all [Yisra'el](../../strongs/h/h3478.md) had [sāp̄aḏ](../../strongs/h/h5594.md) him, and [qāḇar](../../strongs/h/h6912.md) him in [rāmâ](../../strongs/h/h7414.md), even in his own [ʿîr](../../strongs/h/h5892.md). And [Šā'ûl](../../strongs/h/h7586.md) had put [cuwr](../../strongs/h/h5493.md) those that had ['ôḇ](../../strongs/h/h178.md), and the [yiḏʿōnî](../../strongs/h/h3049.md), out of the ['erets](../../strongs/h/h776.md).

<a name="1samuel_28_4"></a>1Samuel 28:4

And the [Pᵊlištî](../../strongs/h/h6430.md) gathered themselves [qāḇaṣ](../../strongs/h/h6908.md), and [bow'](../../strongs/h/h935.md) and [ḥānâ](../../strongs/h/h2583.md) in [Šûnēm](../../strongs/h/h7766.md): and [Šā'ûl](../../strongs/h/h7586.md) [qāḇaṣ](../../strongs/h/h6908.md) all [Yisra'el](../../strongs/h/h3478.md) [qāḇaṣ](../../strongs/h/h6908.md), and they [ḥānâ](../../strongs/h/h2583.md) in [Gilbōaʿ](../../strongs/h/h1533.md).

<a name="1samuel_28_5"></a>1Samuel 28:5

And when [Šā'ûl](../../strongs/h/h7586.md) [ra'ah](../../strongs/h/h7200.md) the [maḥănê](../../strongs/h/h4264.md) of the [Pᵊlištî](../../strongs/h/h6430.md), he was [yare'](../../strongs/h/h3372.md), and his [leb](../../strongs/h/h3820.md) [me'od](../../strongs/h/h3966.md) [ḥārēḏ](../../strongs/h/h2729.md).

<a name="1samuel_28_6"></a>1Samuel 28:6

And when [Šā'ûl](../../strongs/h/h7586.md) [sha'al](../../strongs/h/h7592.md) of [Yĕhovah](../../strongs/h/h3068.md), [Yĕhovah](../../strongs/h/h3068.md) ['anah](../../strongs/h/h6030.md) him not, neither by [ḥălôm](../../strongs/h/h2472.md), nor by ['Ûrîm](../../strongs/h/h224.md), nor by [nāḇî'](../../strongs/h/h5030.md).

<a name="1samuel_28_7"></a>1Samuel 28:7

Then ['āmar](../../strongs/h/h559.md) [Šā'ûl](../../strongs/h/h7586.md) unto his ['ebed](../../strongs/h/h5650.md), [bāqaš](../../strongs/h/h1245.md) me an ['ishshah](../../strongs/h/h802.md) that [baʿălâ](../../strongs/h/h1172.md) a ['ôḇ](../../strongs/h/h178.md), that I may [yālaḵ](../../strongs/h/h3212.md) to her, and [darash](../../strongs/h/h1875.md) of her. And his ['ebed](../../strongs/h/h5650.md) ['āmar](../../strongs/h/h559.md) to him, Behold, there is an ['ishshah](../../strongs/h/h802.md) that [baʿălâ](../../strongs/h/h1172.md) a ['ôḇ](../../strongs/h/h178.md) at [ʿÊn-Ḏôr](../../strongs/h/h5874.md).

<a name="1samuel_28_8"></a>1Samuel 28:8

And [Šā'ûl](../../strongs/h/h7586.md) [ḥāp̄aś](../../strongs/h/h2664.md) himself, and put [labash](../../strongs/h/h3847.md) ['aḥēr](../../strongs/h/h312.md) [beḡeḏ](../../strongs/h/h899.md), and he [yālaḵ](../../strongs/h/h3212.md), and two ['enowsh](../../strongs/h/h582.md) with him, and they [bow'](../../strongs/h/h935.md) to the ['ishshah](../../strongs/h/h802.md) by [layil](../../strongs/h/h3915.md): and he ['āmar](../../strongs/h/h559.md), I pray thee, [qāsam](../../strongs/h/h7080.md) unto me by the ['ôḇ](../../strongs/h/h178.md), and [ʿālâ](../../strongs/h/h5927.md) me him, whom I shall ['āmar](../../strongs/h/h559.md) unto thee.

<a name="1samuel_28_9"></a>1Samuel 28:9

And the ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md) unto him, Behold, thou [yada'](../../strongs/h/h3045.md) what [Šā'ûl](../../strongs/h/h7586.md) hath ['asah](../../strongs/h/h6213.md), how he hath [karath](../../strongs/h/h3772.md) those that have ['ôḇ](../../strongs/h/h178.md), and the [yiḏʿōnî](../../strongs/h/h3049.md), out of the ['erets](../../strongs/h/h776.md): wherefore then layest thou a [nāqaš](../../strongs/h/h5367.md) for my [nephesh](../../strongs/h/h5315.md), to cause me to [muwth](../../strongs/h/h4191.md)?

<a name="1samuel_28_10"></a>1Samuel 28:10

And [Šā'ûl](../../strongs/h/h7586.md) [shaba'](../../strongs/h/h7650.md) to her by [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), there shall no ['avon](../../strongs/h/h5771.md) [qārâ](../../strongs/h/h7136.md) to thee for this [dabar](../../strongs/h/h1697.md).

<a name="1samuel_28_11"></a>1Samuel 28:11

Then ['āmar](../../strongs/h/h559.md) the ['ishshah](../../strongs/h/h802.md), Whom shall I [ʿālâ](../../strongs/h/h5927.md) unto thee? And he ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md) me [Šᵊmû'Ēl](../../strongs/h/h8050.md).

<a name="1samuel_28_12"></a>1Samuel 28:12

And when the ['ishshah](../../strongs/h/h802.md) [ra'ah](../../strongs/h/h7200.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md), she [zāʿaq](../../strongs/h/h2199.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md): and the ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md) to [Šā'ûl](../../strongs/h/h7586.md), ['āmar](../../strongs/h/h559.md), Why hast thou [rāmâ](../../strongs/h/h7411.md) me? for thou art [Šā'ûl](../../strongs/h/h7586.md).

<a name="1samuel_28_13"></a>1Samuel 28:13

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto her, Be not [yare'](../../strongs/h/h3372.md): for what [ra'ah](../../strongs/h/h7200.md) thou? And the ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md) unto [Šā'ûl](../../strongs/h/h7586.md), I [ra'ah](../../strongs/h/h7200.md) ['Elohiym](../../strongs/h/h430.md) [ʿālâ](../../strongs/h/h5927.md) out of the ['erets](../../strongs/h/h776.md).

<a name="1samuel_28_14"></a>1Samuel 28:14

And he ['āmar](../../strongs/h/h559.md) unto her, What [tō'ar](../../strongs/h/h8389.md) is he of? And she ['āmar](../../strongs/h/h559.md), A [zāqēn](../../strongs/h/h2205.md) ['iysh](../../strongs/h/h376.md) [ʿālâ](../../strongs/h/h5927.md); and he is [ʿāṭâ](../../strongs/h/h5844.md) with a [mᵊʿîl](../../strongs/h/h4598.md). And [Šā'ûl](../../strongs/h/h7586.md) [yada'](../../strongs/h/h3045.md) that it was [Šᵊmû'Ēl](../../strongs/h/h8050.md), and he [qāḏaḏ](../../strongs/h/h6915.md) with his ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md), and [shachah](../../strongs/h/h7812.md) himself.

<a name="1samuel_28_15"></a>1Samuel 28:15

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) to [Šā'ûl](../../strongs/h/h7586.md), Why hast thou [ragaz](../../strongs/h/h7264.md) me, to [ʿālâ](../../strongs/h/h5927.md) me? And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), I am [me'od](../../strongs/h/h3966.md) [tsarar](../../strongs/h/h6887.md); for the [Pᵊlištî](../../strongs/h/h6430.md) make [lāḥam](../../strongs/h/h3898.md) against me, and ['Elohiym](../../strongs/h/h430.md) is [cuwr](../../strongs/h/h5493.md) from me, and ['anah](../../strongs/h/h6030.md) me no more, neither [yad](../../strongs/h/h3027.md) [nāḇî'](../../strongs/h/h5030.md), nor by [ḥălôm](../../strongs/h/h2472.md): therefore I have [qara'](../../strongs/h/h7121.md) thee, that thou mayest [yada'](../../strongs/h/h3045.md) unto me what I shall ['asah](../../strongs/h/h6213.md).

<a name="1samuel_28_16"></a>1Samuel 28:16

Then ['āmar](../../strongs/h/h559.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md), Wherefore then dost thou [sha'al](../../strongs/h/h7592.md) of me, seeing [Yĕhovah](../../strongs/h/h3068.md) is [cuwr](../../strongs/h/h5493.md) from thee, and is become thine [ʿĀr](../../strongs/h/h6145.md)?

<a name="1samuel_28_17"></a>1Samuel 28:17

And [Yĕhovah](../../strongs/h/h3068.md) hath ['asah](../../strongs/h/h6213.md) to him, as he [dabar](../../strongs/h/h1696.md) by [yad](../../strongs/h/h3027.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [qāraʿ](../../strongs/h/h7167.md) the [mamlāḵâ](../../strongs/h/h4467.md) out of thine [yad](../../strongs/h/h3027.md), and [nathan](../../strongs/h/h5414.md) it to thy [rea'](../../strongs/h/h7453.md), even to [Dāviḏ](../../strongs/h/h1732.md):

<a name="1samuel_28_18"></a>1Samuel 28:18

Because thou [shama'](../../strongs/h/h8085.md) not the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md), nor ['asah](../../strongs/h/h6213.md) his [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) upon [ʿĂmālēq](../../strongs/h/h6002.md), therefore hath [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md) unto thee this [yowm](../../strongs/h/h3117.md).

<a name="1samuel_28_19"></a>1Samuel 28:19

Moreover [Yĕhovah](../../strongs/h/h3068.md) will also [nathan](../../strongs/h/h5414.md) [Yisra'el](../../strongs/h/h3478.md) with thee into the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md): and [māḥār](../../strongs/h/h4279.md) shalt thou and thy [ben](../../strongs/h/h1121.md) be with me: [Yĕhovah](../../strongs/h/h3068.md) also shall [nathan](../../strongs/h/h5414.md) the [maḥănê](../../strongs/h/h4264.md) of [Yisra'el](../../strongs/h/h3478.md) into the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1samuel_28_20"></a>1Samuel 28:20

Then [Šā'ûl](../../strongs/h/h7586.md) [naphal](../../strongs/h/h5307.md) [māhar](../../strongs/h/h4116.md) [mᵊlō'](../../strongs/h/h4393.md) [qômâ](../../strongs/h/h6967.md) on the ['erets](../../strongs/h/h776.md), and was [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md), because of the [dabar](../../strongs/h/h1697.md) of [Šᵊmû'Ēl](../../strongs/h/h8050.md): and there was no [koach](../../strongs/h/h3581.md) in him; for he had ['akal](../../strongs/h/h398.md) no [lechem](../../strongs/h/h3899.md) all the [yowm](../../strongs/h/h3117.md), nor all the [layil](../../strongs/h/h3915.md).

<a name="1samuel_28_21"></a>1Samuel 28:21

And the ['ishshah](../../strongs/h/h802.md) [bow'](../../strongs/h/h935.md) unto [Šā'ûl](../../strongs/h/h7586.md), and [ra'ah](../../strongs/h/h7200.md) that he was [me'od](../../strongs/h/h3966.md) [bahal](../../strongs/h/h926.md), and ['āmar](../../strongs/h/h559.md) unto him, Behold, thine [šip̄ḥâ](../../strongs/h/h8198.md) hath [shama'](../../strongs/h/h8085.md) thy [qowl](../../strongs/h/h6963.md), and I have [śûm](../../strongs/h/h7760.md) my [nephesh](../../strongs/h/h5315.md) in my [kaph](../../strongs/h/h3709.md), and have [shama'](../../strongs/h/h8085.md) unto thy [dabar](../../strongs/h/h1697.md) which thou [dabar](../../strongs/h/h1696.md) unto me.

<a name="1samuel_28_22"></a>1Samuel 28:22

Now therefore, I pray thee, [shama'](../../strongs/h/h8085.md) thou also unto the [qowl](../../strongs/h/h6963.md) of thine [šip̄ḥâ](../../strongs/h/h8198.md), and let me [śûm](../../strongs/h/h7760.md) a [paṯ](../../strongs/h/h6595.md) of [lechem](../../strongs/h/h3899.md) [paniym](../../strongs/h/h6440.md) thee; and ['akal](../../strongs/h/h398.md), that thou mayest have [koach](../../strongs/h/h3581.md), when thou [yālaḵ](../../strongs/h/h3212.md) on thy [derek](../../strongs/h/h1870.md).

<a name="1samuel_28_23"></a>1Samuel 28:23

But he [mā'ēn](../../strongs/h/h3985.md), and ['āmar](../../strongs/h/h559.md), I will not ['akal](../../strongs/h/h398.md). But his ['ebed](../../strongs/h/h5650.md), together with the ['ishshah](../../strongs/h/h802.md), [pāraṣ](../../strongs/h/h6555.md) him; and he [shama'](../../strongs/h/h8085.md) unto their [qowl](../../strongs/h/h6963.md). So he [quwm](../../strongs/h/h6965.md) from the ['erets](../../strongs/h/h776.md), and [yashab](../../strongs/h/h3427.md) upon the [mittah](../../strongs/h/h4296.md).

<a name="1samuel_28_24"></a>1Samuel 28:24

And the ['ishshah](../../strongs/h/h802.md) had a [marbēq](../../strongs/h/h4770.md) [ʿēḡel](../../strongs/h/h5695.md) in the [bayith](../../strongs/h/h1004.md); and she [māhar](../../strongs/h/h4116.md), and [zabach](../../strongs/h/h2076.md) it, and [laqach](../../strongs/h/h3947.md) [qemaḥ](../../strongs/h/h7058.md), and [lûš](../../strongs/h/h3888.md) it, and did ['āp̄â](../../strongs/h/h644.md) [maṣṣâ](../../strongs/h/h4682.md) thereof:

<a name="1samuel_28_25"></a>1Samuel 28:25

And she [nāḡaš](../../strongs/h/h5066.md) it [paniym](../../strongs/h/h6440.md) [Šā'ûl](../../strongs/h/h7586.md), and [paniym](../../strongs/h/h6440.md) his ['ebed](../../strongs/h/h5650.md); and they did ['akal](../../strongs/h/h398.md). Then they [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) that [layil](../../strongs/h/h3915.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 27](1samuel_27.md) - [1Samuel 29](1samuel_29.md)