# [1Samuel 23](https://www.blueletterbible.org/kjv/1samuel/23)

<a name="1samuel_23_1"></a>1Samuel 23:1

Then they [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), Behold, the [Pᵊlištî](../../strongs/h/h6430.md) [lāḥam](../../strongs/h/h3898.md) against [QᵊʿÎlâ](../../strongs/h/h7084.md), and they [šāsâ](../../strongs/h/h8154.md) the [gōren](../../strongs/h/h1637.md).

<a name="1samuel_23_2"></a>1Samuel 23:2

Therefore [Dāviḏ](../../strongs/h/h1732.md) [sha'al](../../strongs/h/h7592.md) of [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), Shall I [yālaḵ](../../strongs/h/h3212.md) and [nakah](../../strongs/h/h5221.md) these [Pᵊlištî](../../strongs/h/h6430.md)? And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), [yālaḵ](../../strongs/h/h3212.md), and [nakah](../../strongs/h/h5221.md) the [Pᵊlištî](../../strongs/h/h6430.md), and [yasha'](../../strongs/h/h3467.md) [QᵊʿÎlâ](../../strongs/h/h7084.md).

<a name="1samuel_23_3"></a>1Samuel 23:3

And [Dāviḏ](../../strongs/h/h1732.md) ['enowsh](../../strongs/h/h582.md) ['āmar](../../strongs/h/h559.md) unto him, Behold, we be [yārē'](../../strongs/h/h3373.md) here in [Yehuwdah](../../strongs/h/h3063.md): how much more then if we [yālaḵ](../../strongs/h/h3212.md) to [QᵊʿÎlâ](../../strongs/h/h7084.md) against the [maʿărāḵâ](../../strongs/h/h4634.md) of the [Pᵊlištî](../../strongs/h/h6430.md)?

<a name="1samuel_23_4"></a>1Samuel 23:4

Then [Dāviḏ](../../strongs/h/h1732.md) [sha'al](../../strongs/h/h7592.md) of [Yĕhovah](../../strongs/h/h3068.md) yet again. And [Yĕhovah](../../strongs/h/h3068.md) ['anah](../../strongs/h/h6030.md) him and ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md), [yarad](../../strongs/h/h3381.md) to [QᵊʿÎlâ](../../strongs/h/h7084.md); for I will [nathan](../../strongs/h/h5414.md) the [Pᵊlištî](../../strongs/h/h6430.md) into thine [yad](../../strongs/h/h3027.md).

<a name="1samuel_23_5"></a>1Samuel 23:5

So [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) [yālaḵ](../../strongs/h/h3212.md) to [QᵊʿÎlâ](../../strongs/h/h7084.md), and [lāḥam](../../strongs/h/h3898.md) with the [Pᵊlištî](../../strongs/h/h6430.md), and brought [nāhaḡ](../../strongs/h/h5090.md) their [miqnê](../../strongs/h/h4735.md), and [nakah](../../strongs/h/h5221.md) them with a [gadowl](../../strongs/h/h1419.md) [makâ](../../strongs/h/h4347.md). So [Dāviḏ](../../strongs/h/h1732.md) [yasha'](../../strongs/h/h3467.md) the [yashab](../../strongs/h/h3427.md) of [QᵊʿÎlâ](../../strongs/h/h7084.md).

<a name="1samuel_23_6"></a>1Samuel 23:6

And it came to pass, when ['Eḇyāṯār](../../strongs/h/h54.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîmeleḵ](../../strongs/h/h288.md) [bāraḥ](../../strongs/h/h1272.md) to [Dāviḏ](../../strongs/h/h1732.md) to [QᵊʿÎlâ](../../strongs/h/h7084.md), that he [yarad](../../strongs/h/h3381.md) with an ['ēp̄ôḏ](../../strongs/h/h646.md) in his [yad](../../strongs/h/h3027.md).

<a name="1samuel_23_7"></a>1Samuel 23:7

And it was [nāḡaḏ](../../strongs/h/h5046.md) [Šā'ûl](../../strongs/h/h7586.md) that [Dāviḏ](../../strongs/h/h1732.md) was [bow'](../../strongs/h/h935.md) to [QᵊʿÎlâ](../../strongs/h/h7084.md). And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) hath [nāḵar](../../strongs/h/h5234.md) him into mine [yad](../../strongs/h/h3027.md); for he is [cagar](../../strongs/h/h5462.md), by [bow'](../../strongs/h/h935.md) into a [ʿîr](../../strongs/h/h5892.md) that hath [deleṯ](../../strongs/h/h1817.md) and [bᵊrîaḥ](../../strongs/h/h1280.md).

<a name="1samuel_23_8"></a>1Samuel 23:8

And [Šā'ûl](../../strongs/h/h7586.md) [shama'](../../strongs/h/h8085.md) all the ['am](../../strongs/h/h5971.md) [shama'](../../strongs/h/h8085.md) to [milḥāmâ](../../strongs/h/h4421.md), to [yarad](../../strongs/h/h3381.md) to [QᵊʿÎlâ](../../strongs/h/h7084.md), to [ṣûr](../../strongs/h/h6696.md) [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md).

<a name="1samuel_23_9"></a>1Samuel 23:9

And [Dāviḏ](../../strongs/h/h1732.md) [yada'](../../strongs/h/h3045.md) that [Šā'ûl](../../strongs/h/h7586.md) [ḥāraš](../../strongs/h/h2790.md) [ra'](../../strongs/h/h7451.md) against him; and he ['āmar](../../strongs/h/h559.md) to ['Eḇyāṯār](../../strongs/h/h54.md) the [kōhēn](../../strongs/h/h3548.md), Bring [nāḡaš](../../strongs/h/h5066.md) the ['ēp̄ôḏ](../../strongs/h/h646.md).

<a name="1samuel_23_10"></a>1Samuel 23:10

Then ['āmar](../../strongs/h/h559.md) [Dāviḏ](../../strongs/h/h1732.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), thy ['ebed](../../strongs/h/h5650.md) hath [shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) that [Šā'ûl](../../strongs/h/h7586.md) [bāqaš](../../strongs/h/h1245.md) to [bow'](../../strongs/h/h935.md) to [QᵊʿÎlâ](../../strongs/h/h7084.md), to [shachath](../../strongs/h/h7843.md) the [ʿîr](../../strongs/h/h5892.md) for my sake.

<a name="1samuel_23_11"></a>1Samuel 23:11

Will the [baʿal](../../strongs/h/h1167.md) of [QᵊʿÎlâ](../../strongs/h/h7084.md) deliver me [cagar](../../strongs/h/h5462.md) into his [yad](../../strongs/h/h3027.md)? will [Šā'ûl](../../strongs/h/h7586.md) [yarad](../../strongs/h/h3381.md), as thy ['ebed](../../strongs/h/h5650.md) hath [shama'](../../strongs/h/h8085.md)? [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), I beseech thee, [nāḡaḏ](../../strongs/h/h5046.md) thy ['ebed](../../strongs/h/h5650.md). And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), He will [yarad](../../strongs/h/h3381.md).

<a name="1samuel_23_12"></a>1Samuel 23:12

Then ['āmar](../../strongs/h/h559.md) [Dāviḏ](../../strongs/h/h1732.md), Will the [baʿal](../../strongs/h/h1167.md) of [QᵊʿÎlâ](../../strongs/h/h7084.md) [cagar](../../strongs/h/h5462.md) me and my ['enowsh](../../strongs/h/h582.md) into the [yad](../../strongs/h/h3027.md) of [Šā'ûl](../../strongs/h/h7586.md)? And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), They will deliver thee [cagar](../../strongs/h/h5462.md).

<a name="1samuel_23_13"></a>1Samuel 23:13

Then [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md), which were about six hundred ['iysh](../../strongs/h/h376.md), [quwm](../../strongs/h/h6965.md) and [yāṣā'](../../strongs/h/h3318.md) out of [QᵊʿÎlâ](../../strongs/h/h7084.md), and [halak](../../strongs/h/h1980.md) whithersoever they could [halak](../../strongs/h/h1980.md). And it was [nāḡaḏ](../../strongs/h/h5046.md) [Šā'ûl](../../strongs/h/h7586.md) that [Dāviḏ](../../strongs/h/h1732.md) was [mālaṭ](../../strongs/h/h4422.md) from [QᵊʿÎlâ](../../strongs/h/h7084.md); and he [ḥāḏal](../../strongs/h/h2308.md) to [yāṣā'](../../strongs/h/h3318.md).

<a name="1samuel_23_14"></a>1Samuel 23:14

And [Dāviḏ](../../strongs/h/h1732.md) [yashab](../../strongs/h/h3427.md) in the [midbar](../../strongs/h/h4057.md) in strong [mᵊṣāḏ](../../strongs/h/h4679.md), and [yashab](../../strongs/h/h3427.md) in a [har](../../strongs/h/h2022.md) in the [midbar](../../strongs/h/h4057.md) of [Zîp̄](../../strongs/h/h2128.md). And [Šā'ûl](../../strongs/h/h7586.md) [bāqaš](../../strongs/h/h1245.md) him every [yowm](../../strongs/h/h3117.md), but ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) him not into his [yad](../../strongs/h/h3027.md).

<a name="1samuel_23_15"></a>1Samuel 23:15

And [Dāviḏ](../../strongs/h/h1732.md) [ra'ah](../../strongs/h/h7200.md) that [Šā'ûl](../../strongs/h/h7586.md) was [yāṣā'](../../strongs/h/h3318.md) to [bāqaš](../../strongs/h/h1245.md) his [nephesh](../../strongs/h/h5315.md): and [Dāviḏ](../../strongs/h/h1732.md) was in the [midbar](../../strongs/h/h4057.md) of [Zîp̄](../../strongs/h/h2128.md) in a [ḥōreš](../../strongs/h/h2793.md).

<a name="1samuel_23_16"></a>1Samuel 23:16

And [Yᵊhônāṯān](../../strongs/h/h3083.md) [Šā'ûl](../../strongs/h/h7586.md) [ben](../../strongs/h/h1121.md) [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) to [Dāviḏ](../../strongs/h/h1732.md) into the [ḥōreš](../../strongs/h/h2793.md), and [ḥāzaq](../../strongs/h/h2388.md) his [yad](../../strongs/h/h3027.md) in ['Elohiym](../../strongs/h/h430.md).

<a name="1samuel_23_17"></a>1Samuel 23:17

And he ['āmar](../../strongs/h/h559.md) unto him, [yare'](../../strongs/h/h3372.md) not: for the [yad](../../strongs/h/h3027.md) of [Šā'ûl](../../strongs/h/h7586.md) my ['ab](../../strongs/h/h1.md) shall not [māṣā'](../../strongs/h/h4672.md) thee; and thou shalt be [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md), and I shall be [mišnê](../../strongs/h/h4932.md) unto thee; and that also [Šā'ûl](../../strongs/h/h7586.md) my ['ab](../../strongs/h/h1.md) [yada'](../../strongs/h/h3045.md).

<a name="1samuel_23_18"></a>1Samuel 23:18

And they two [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): and [Dāviḏ](../../strongs/h/h1732.md) [yashab](../../strongs/h/h3427.md) in the [ḥōreš](../../strongs/h/h2793.md), and [Yᵊhônāṯān](../../strongs/h/h3083.md) [halak](../../strongs/h/h1980.md) to his [bayith](../../strongs/h/h1004.md).

<a name="1samuel_23_19"></a>1Samuel 23:19

Then [ʿālâ](../../strongs/h/h5927.md) the [Zîp̄î](../../strongs/h/h2130.md) to [Šā'ûl](../../strongs/h/h7586.md) to [giḇʿâ](../../strongs/h/h1390.md), ['āmar](../../strongs/h/h559.md), Doth not [Dāviḏ](../../strongs/h/h1732.md) [cathar](../../strongs/h/h5641.md) himself with us in [mᵊṣāḏ](../../strongs/h/h4679.md) in the [ḥōreš](../../strongs/h/h2793.md), in the [giḇʿâ](../../strongs/h/h1389.md) of [Ḥăḵîlâ](../../strongs/h/h2444.md), which is on the [yamiyn](../../strongs/h/h3225.md) of [yᵊšîmôn](../../strongs/h/h3452.md)?

<a name="1samuel_23_20"></a>1Samuel 23:20

Now therefore, O [melek](../../strongs/h/h4428.md), [yarad](../../strongs/h/h3381.md) according to all the ['aûâ](../../strongs/h/h185.md) of thy [nephesh](../../strongs/h/h5315.md) to [yarad](../../strongs/h/h3381.md); and our part shall be to [cagar](../../strongs/h/h5462.md) him into the [melek](../../strongs/h/h4428.md) [yad](../../strongs/h/h3027.md).

<a name="1samuel_23_21"></a>1Samuel 23:21

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be ye of [Yĕhovah](../../strongs/h/h3068.md); for ye have [ḥāmal](../../strongs/h/h2550.md) on me.

<a name="1samuel_23_22"></a>1Samuel 23:22

[yālaḵ](../../strongs/h/h3212.md), I pray you, [kuwn](../../strongs/h/h3559.md) yet, and [yada'](../../strongs/h/h3045.md) and [ra'ah](../../strongs/h/h7200.md) his [maqowm](../../strongs/h/h4725.md) where his [regel](../../strongs/h/h7272.md) is, and who hath [ra'ah](../../strongs/h/h7200.md) him there: for it is ['āmar](../../strongs/h/h559.md) me that he [ʿāram](../../strongs/h/h6191.md) [ʿāram](../../strongs/h/h6191.md).

<a name="1samuel_23_23"></a>1Samuel 23:23

[ra'ah](../../strongs/h/h7200.md) therefore, and [yada'](../../strongs/h/h3045.md) of all the [maḥăḇē'](../../strongs/h/h4224.md) where he [chaba'](../../strongs/h/h2244.md) himself, and come ye [shuwb](../../strongs/h/h7725.md) to me with the [kuwn](../../strongs/h/h3559.md), and I will [halak](../../strongs/h/h1980.md) with you: and it shall come to pass, if he be in the ['erets](../../strongs/h/h776.md), that I will [ḥāp̄aś](../../strongs/h/h2664.md) him out throughout all the thousands of [Yehuwdah](../../strongs/h/h3063.md).

<a name="1samuel_23_24"></a>1Samuel 23:24

And they [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) to [Zîp̄](../../strongs/h/h2128.md) [paniym](../../strongs/h/h6440.md) [Šā'ûl](../../strongs/h/h7586.md): but [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) were in the [midbar](../../strongs/h/h4057.md) of [MāʿÔn](../../strongs/h/h4584.md), in the ['arabah](../../strongs/h/h6160.md) on the [yamiyn](../../strongs/h/h3225.md) of [yᵊšîmôn](../../strongs/h/h3452.md).

<a name="1samuel_23_25"></a>1Samuel 23:25

[Šā'ûl](../../strongs/h/h7586.md) also and his ['enowsh](../../strongs/h/h582.md) [yālaḵ](../../strongs/h/h3212.md) to [bāqaš](../../strongs/h/h1245.md) him. And they [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md): wherefore he [yarad](../../strongs/h/h3381.md) into a [cela'](../../strongs/h/h5553.md), and [yashab](../../strongs/h/h3427.md) in the [midbar](../../strongs/h/h4057.md) of [MāʿÔn](../../strongs/h/h4584.md). And when [Šā'ûl](../../strongs/h/h7586.md) [shama'](../../strongs/h/h8085.md) that, he [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) [Dāviḏ](../../strongs/h/h1732.md) in the [midbar](../../strongs/h/h4057.md) of [MāʿÔn](../../strongs/h/h4584.md).

<a name="1samuel_23_26"></a>1Samuel 23:26

And [Šā'ûl](../../strongs/h/h7586.md) [yālaḵ](../../strongs/h/h3212.md) on this [ṣaḏ](../../strongs/h/h6654.md) of the [har](../../strongs/h/h2022.md), and [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) on that [ṣaḏ](../../strongs/h/h6654.md) of the [har](../../strongs/h/h2022.md): and [Dāviḏ](../../strongs/h/h1732.md) made [ḥāp̄az](../../strongs/h/h2648.md) to get [yālaḵ](../../strongs/h/h3212.md) for [paniym](../../strongs/h/h6440.md) of [Šā'ûl](../../strongs/h/h7586.md); for [Šā'ûl](../../strongs/h/h7586.md) and his ['enowsh](../../strongs/h/h582.md) ['atar](../../strongs/h/h5849.md) [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) round ['atar](../../strongs/h/h5849.md) to [tāp̄aś](../../strongs/h/h8610.md) them.

<a name="1samuel_23_27"></a>1Samuel 23:27

But there [bow'](../../strongs/h/h935.md) a [mal'ak](../../strongs/h/h4397.md) unto [Šā'ûl](../../strongs/h/h7586.md), ['āmar](../../strongs/h/h559.md), [māhar](../../strongs/h/h4116.md) thee, and [yālaḵ](../../strongs/h/h3212.md); for the [Pᵊlištî](../../strongs/h/h6430.md) have [pāšaṭ](../../strongs/h/h6584.md) the ['erets](../../strongs/h/h776.md).

<a name="1samuel_23_28"></a>1Samuel 23:28

Wherefore [Šā'ûl](../../strongs/h/h7586.md) [shuwb](../../strongs/h/h7725.md) from [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) [Dāviḏ](../../strongs/h/h1732.md), and [yālaḵ](../../strongs/h/h3212.md) [qārā'](../../strongs/h/h7125.md) the [Pᵊlištî](../../strongs/h/h6430.md): therefore they [qara'](../../strongs/h/h7121.md) that [maqowm](../../strongs/h/h4725.md) [Selaʿ Hammaḥlᵊqôṯ](../../strongs/h/h5555.md).

<a name="1samuel_23_29"></a>1Samuel 23:29

And [Dāviḏ](../../strongs/h/h1732.md) [ʿālâ](../../strongs/h/h5927.md) from thence, and [yashab](../../strongs/h/h3427.md) in [mᵊṣāḏ](../../strongs/h/h4679.md) at [ʿÊn Ḡeḏî](../../strongs/h/h5872.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 22](1samuel_22.md) - [1Samuel 24](1samuel_24.md)