# [1Samuel 27](https://www.blueletterbible.org/kjv/1samuel/27)

<a name="1samuel_27_1"></a>1Samuel 27:1

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) in his [leb](../../strongs/h/h3820.md), I shall now [sāp̄â](../../strongs/h/h5595.md) one [yowm](../../strongs/h/h3117.md) by the [yad](../../strongs/h/h3027.md) of [Šā'ûl](../../strongs/h/h7586.md): there is nothing [towb](../../strongs/h/h2896.md) for me than that I should [mālaṭ](../../strongs/h/h4422.md) [mālaṭ](../../strongs/h/h4422.md) into the ['erets](../../strongs/h/h776.md) of the [Pᵊlištî](../../strongs/h/h6430.md); and [Šā'ûl](../../strongs/h/h7586.md) shall [yā'aš](../../strongs/h/h2976.md) of me, to [bāqaš](../../strongs/h/h1245.md) me any more in any [gᵊḇûl](../../strongs/h/h1366.md) of [Yisra'el](../../strongs/h/h3478.md): so shall I [mālaṭ](../../strongs/h/h4422.md) out of his [yad](../../strongs/h/h3027.md).

<a name="1samuel_27_2"></a>1Samuel 27:2

And [Dāviḏ](../../strongs/h/h1732.md) [quwm](../../strongs/h/h6965.md), and he ['abar](../../strongs/h/h5674.md) with the six hundred ['iysh](../../strongs/h/h376.md) that were with him unto ['Āḵîš](../../strongs/h/h397.md), the [ben](../../strongs/h/h1121.md) of [MāʿÔḵ](../../strongs/h/h4582.md), [melek](../../strongs/h/h4428.md) of [Gaṯ](../../strongs/h/h1661.md).

<a name="1samuel_27_3"></a>1Samuel 27:3

And [Dāviḏ](../../strongs/h/h1732.md) [yashab](../../strongs/h/h3427.md) with ['Āḵîš](../../strongs/h/h397.md) at [Gaṯ](../../strongs/h/h1661.md), he and his ['enowsh](../../strongs/h/h582.md), every ['iysh](../../strongs/h/h376.md) with his [bayith](../../strongs/h/h1004.md), even [Dāviḏ](../../strongs/h/h1732.md) with his two ['ishshah](../../strongs/h/h802.md), ['ĂḥînōʿAm](../../strongs/h/h293.md) the [Yizrᵊʿē'lîṯ](../../strongs/h/h3159.md), and ['Ăḇîḡayil](../../strongs/h/h26.md) the [Karmᵊlîṯ](../../strongs/h/h3762.md), [Nāḇāl](../../strongs/h/h5037.md) ['ishshah](../../strongs/h/h802.md).

<a name="1samuel_27_4"></a>1Samuel 27:4

And it was [nāḡaḏ](../../strongs/h/h5046.md) [Šā'ûl](../../strongs/h/h7586.md) that [Dāviḏ](../../strongs/h/h1732.md) was [bāraḥ](../../strongs/h/h1272.md) to [Gaṯ](../../strongs/h/h1661.md): and he [bāqaš](../../strongs/h/h1245.md) no [yāsap̄](../../strongs/h/h3254.md) [yāsap̄](../../strongs/h/h3254.md) for him.

<a name="1samuel_27_5"></a>1Samuel 27:5

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto ['Āḵîš](../../strongs/h/h397.md), If I have now [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thine ['ayin](../../strongs/h/h5869.md), let them [nathan](../../strongs/h/h5414.md) me a [maqowm](../../strongs/h/h4725.md) in some [ʿîr](../../strongs/h/h5892.md) in the [sadeh](../../strongs/h/h7704.md), that I may [yashab](../../strongs/h/h3427.md) there: for why should thy ['ebed](../../strongs/h/h5650.md) [yashab](../../strongs/h/h3427.md) in the [mamlāḵâ](../../strongs/h/h4467.md) [ʿîr](../../strongs/h/h5892.md) with thee?

<a name="1samuel_27_6"></a>1Samuel 27:6

Then ['Āḵîš](../../strongs/h/h397.md) [nathan](../../strongs/h/h5414.md) him [Ṣiqlāḡ](../../strongs/h/h6860.md) that [yowm](../../strongs/h/h3117.md): wherefore [Ṣiqlāḡ](../../strongs/h/h6860.md) pertaineth unto the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="1samuel_27_7"></a>1Samuel 27:7

And the [mispār](../../strongs/h/h4557.md) [yowm](../../strongs/h/h3117.md) that [Dāviḏ](../../strongs/h/h1732.md) [yashab](../../strongs/h/h3427.md) in the [sadeh](../../strongs/h/h7704.md) of the [Pᵊlištî](../../strongs/h/h6430.md) was a full [yowm](../../strongs/h/h3117.md) and four [ḥōḏeš](../../strongs/h/h2320.md).

<a name="1samuel_27_8"></a>1Samuel 27:8

And [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) [ʿālâ](../../strongs/h/h5927.md), and [pāšaṭ](../../strongs/h/h6584.md) the [Gᵊšûrî](../../strongs/h/h1651.md), and the [gizrî](../../strongs/h/h1511.md), and the [ʿămālēqî](../../strongs/h/h6003.md): for those nations were of ['owlam](../../strongs/h/h5769.md) the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md), as thou [bow'](../../strongs/h/h935.md) to [šûr](../../strongs/h/h7793.md), even unto the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="1samuel_27_9"></a>1Samuel 27:9

And [Dāviḏ](../../strongs/h/h1732.md) [nakah](../../strongs/h/h5221.md) the ['erets](../../strongs/h/h776.md), and left neither ['iysh](../../strongs/h/h376.md) nor ['ishshah](../../strongs/h/h802.md) [ḥāyâ](../../strongs/h/h2421.md), and took [laqach](../../strongs/h/h3947.md) the [tso'n](../../strongs/h/h6629.md), and the [bāqār](../../strongs/h/h1241.md), and the [chamowr](../../strongs/h/h2543.md), and the [gāmāl](../../strongs/h/h1581.md), and the [beḡeḏ](../../strongs/h/h899.md), and [shuwb](../../strongs/h/h7725.md), and [bow'](../../strongs/h/h935.md) to ['Āḵîš](../../strongs/h/h397.md).

<a name="1samuel_27_10"></a>1Samuel 27:10

And ['Āḵîš](../../strongs/h/h397.md) ['āmar](../../strongs/h/h559.md), Whither have ye made a [pāšaṭ](../../strongs/h/h6584.md) to [yowm](../../strongs/h/h3117.md)? And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), Against the [neḡeḇ](../../strongs/h/h5045.md) of [Yehuwdah](../../strongs/h/h3063.md), and against the [neḡeḇ](../../strongs/h/h5045.md) of the [yᵊraḥmᵊ'ēlî](../../strongs/h/h3397.md), and against the [neḡeḇ](../../strongs/h/h5045.md) of the [Qênî](../../strongs/h/h7017.md).

<a name="1samuel_27_11"></a>1Samuel 27:11

And [Dāviḏ](../../strongs/h/h1732.md) [ḥāyâ](../../strongs/h/h2421.md) neither ['iysh](../../strongs/h/h376.md) nor ['ishshah](../../strongs/h/h802.md) [ḥāyâ](../../strongs/h/h2421.md), to [bow'](../../strongs/h/h935.md) tidings to [Gaṯ](../../strongs/h/h1661.md), ['āmar](../../strongs/h/h559.md), Lest they should [nāḡaḏ](../../strongs/h/h5046.md) on us, ['āmar](../../strongs/h/h559.md), So ['asah](../../strongs/h/h6213.md) [Dāviḏ](../../strongs/h/h1732.md), and so will be his [mishpat](../../strongs/h/h4941.md) all the [yowm](../../strongs/h/h3117.md) he [yashab](../../strongs/h/h3427.md) in the [sadeh](../../strongs/h/h7704.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1samuel_27_12"></a>1Samuel 27:12

And ['Āḵîš](../../strongs/h/h397.md) ['aman](../../strongs/h/h539.md) [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), He hath made his ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) [bā'aš](../../strongs/h/h887.md) to [bā'aš](../../strongs/h/h887.md) him; therefore he shall be my ['ebed](../../strongs/h/h5650.md) ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 26](1samuel_26.md) - [1Samuel 28](1samuel_28.md)