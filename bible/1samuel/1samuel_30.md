# [1Samuel 30](https://www.blueletterbible.org/kjv/1samuel/30)

<a name="1samuel_30_1"></a>1Samuel 30:1

And it came to pass, when [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) were [bow'](../../strongs/h/h935.md) to [Ṣiqlāḡ](../../strongs/h/h6860.md) on the third [yowm](../../strongs/h/h3117.md), that the [ʿămālēqî](../../strongs/h/h6003.md) had [pāšaṭ](../../strongs/h/h6584.md) the [neḡeḇ](../../strongs/h/h5045.md), and [Ṣiqlāḡ](../../strongs/h/h6860.md), and [nakah](../../strongs/h/h5221.md) [Ṣiqlāḡ](../../strongs/h/h6860.md), and [śārap̄](../../strongs/h/h8313.md) it with ['esh](../../strongs/h/h784.md);

<a name="1samuel_30_2"></a>1Samuel 30:2

And had taken the ['ishshah](../../strongs/h/h802.md) [šāḇâ](../../strongs/h/h7617.md), that were therein: they [muwth](../../strongs/h/h4191.md) not ['iysh](../../strongs/h/h376.md), either [gadowl](../../strongs/h/h1419.md) or [qāṭān](../../strongs/h/h6996.md), but [nāhaḡ](../../strongs/h/h5090.md) them, and [yālaḵ](../../strongs/h/h3212.md) on their [derek](../../strongs/h/h1870.md).

<a name="1samuel_30_3"></a>1Samuel 30:3

So [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) [bow'](../../strongs/h/h935.md) to the [ʿîr](../../strongs/h/h5892.md), and, behold, it was [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md); and their ['ishshah](../../strongs/h/h802.md), and their [ben](../../strongs/h/h1121.md), and their [bath](../../strongs/h/h1323.md), were [šāḇâ](../../strongs/h/h7617.md).

<a name="1samuel_30_4"></a>1Samuel 30:4

Then [Dāviḏ](../../strongs/h/h1732.md) and the ['am](../../strongs/h/h5971.md) that were with him [nasa'](../../strongs/h/h5375.md) their [qowl](../../strongs/h/h6963.md) and [bāḵâ](../../strongs/h/h1058.md), until they had no more [koach](../../strongs/h/h3581.md) to [bāḵâ](../../strongs/h/h1058.md).

<a name="1samuel_30_5"></a>1Samuel 30:5

And [Dāviḏ](../../strongs/h/h1732.md) two ['ishshah](../../strongs/h/h802.md) were taken [šāḇâ](../../strongs/h/h7617.md), ['ĂḥînōʿAm](../../strongs/h/h293.md) the [Yizrᵊʿē'lîṯ](../../strongs/h/h3159.md), and ['Ăḇîḡayil](../../strongs/h/h26.md) the ['ishshah](../../strongs/h/h802.md) of [Nāḇāl](../../strongs/h/h5037.md) the [Karmᵊlî](../../strongs/h/h3761.md).

<a name="1samuel_30_6"></a>1Samuel 30:6

And [Dāviḏ](../../strongs/h/h1732.md) was [me'od](../../strongs/h/h3966.md) [yāṣar](../../strongs/h/h3334.md); for the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md) of [sāqal](../../strongs/h/h5619.md) him, because the [nephesh](../../strongs/h/h5315.md) of all the ['am](../../strongs/h/h5971.md) was [mārar](../../strongs/h/h4843.md), every ['iysh](../../strongs/h/h376.md) for his [ben](../../strongs/h/h1121.md) and for his [bath](../../strongs/h/h1323.md): but [Dāviḏ](../../strongs/h/h1732.md) [ḥāzaq](../../strongs/h/h2388.md) himself in [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md).

<a name="1samuel_30_7"></a>1Samuel 30:7

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to ['Eḇyāṯār](../../strongs/h/h54.md) the [kōhēn](../../strongs/h/h3548.md), ['Ăḥîmeleḵ](../../strongs/h/h288.md) [ben](../../strongs/h/h1121.md), I pray thee, bring me [nāḡaš](../../strongs/h/h5066.md) the ['ēp̄ôḏ](../../strongs/h/h646.md). And ['Eḇyāṯār](../../strongs/h/h54.md) [nāḡaš](../../strongs/h/h5066.md) thither the ['ēp̄ôḏ](../../strongs/h/h646.md) to [Dāviḏ](../../strongs/h/h1732.md).

<a name="1samuel_30_8"></a>1Samuel 30:8

And [Dāviḏ](../../strongs/h/h1732.md) [sha'al](../../strongs/h/h7592.md) at [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), Shall I [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) this [gᵊḏûḏ](../../strongs/h/h1416.md)? shall I [nāśaḡ](../../strongs/h/h5381.md) them? And he ['āmar](../../strongs/h/h559.md) him, [radaph](../../strongs/h/h7291.md): for thou shalt [nāśaḡ](../../strongs/h/h5381.md) [nāśaḡ](../../strongs/h/h5381.md) them, and without [natsal](../../strongs/h/h5337.md) [natsal](../../strongs/h/h5337.md) all.

<a name="1samuel_30_9"></a>1Samuel 30:9

So [Dāviḏ](../../strongs/h/h1732.md) [yālaḵ](../../strongs/h/h3212.md), he and the six hundred ['iysh](../../strongs/h/h376.md) that were with him, and [bow'](../../strongs/h/h935.md) to the [nachal](../../strongs/h/h5158.md) [bᵊśôr](../../strongs/h/h1308.md), where those that were [yāṯar](../../strongs/h/h3498.md) ['amad](../../strongs/h/h5975.md).

<a name="1samuel_30_10"></a>1Samuel 30:10

But [Dāviḏ](../../strongs/h/h1732.md) [radaph](../../strongs/h/h7291.md), he and four hundred ['iysh](../../strongs/h/h376.md): for two hundred ['amad](../../strongs/h/h5975.md), which were so [pāḡar](../../strongs/h/h6296.md) that they could not ['abar](../../strongs/h/h5674.md) the [nachal](../../strongs/h/h5158.md) [bᵊśôr](../../strongs/h/h1308.md).

<a name="1samuel_30_11"></a>1Samuel 30:11

And they [māṣā'](../../strongs/h/h4672.md) an ['iysh](../../strongs/h/h376.md) [Miṣrî](../../strongs/h/h4713.md) in the [sadeh](../../strongs/h/h7704.md), and [laqach](../../strongs/h/h3947.md) him to [Dāviḏ](../../strongs/h/h1732.md), and [nathan](../../strongs/h/h5414.md) him [lechem](../../strongs/h/h3899.md), and he did ['akal](../../strongs/h/h398.md); and they made him [šāqâ](../../strongs/h/h8248.md) [mayim](../../strongs/h/h4325.md);

<a name="1samuel_30_12"></a>1Samuel 30:12

And they [nathan](../../strongs/h/h5414.md) him a [pelaḥ](../../strongs/h/h6400.md) of a [dᵊḇēlâ](../../strongs/h/h1690.md) of figs, and two [ṣmvqym](../../strongs/h/h6778.md): and when he had ['akal](../../strongs/h/h398.md), his [ruwach](../../strongs/h/h7307.md) [shuwb](../../strongs/h/h7725.md) to him: for he had ['akal](../../strongs/h/h398.md) no [lechem](../../strongs/h/h3899.md), nor [šāṯâ](../../strongs/h/h8354.md) any [mayim](../../strongs/h/h4325.md), three [yowm](../../strongs/h/h3117.md) and three [layil](../../strongs/h/h3915.md).

<a name="1samuel_30_13"></a>1Samuel 30:13

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto him, To whom belongest thou? and whence art thou? And he ['āmar](../../strongs/h/h559.md), I am a [naʿar](../../strongs/h/h5288.md) of [Miṣrî](../../strongs/h/h4713.md), ['ebed](../../strongs/h/h5650.md) to an ['iysh](../../strongs/h/h376.md) [ʿămālēqî](../../strongs/h/h6003.md); and my ['adown](../../strongs/h/h113.md) ['azab](../../strongs/h/h5800.md) me, because three [yowm](../../strongs/h/h3117.md) agone I fell [ḥālâ](../../strongs/h/h2470.md).

<a name="1samuel_30_14"></a>1Samuel 30:14

We made a [pāšaṭ](../../strongs/h/h6584.md) upon the [neḡeḇ](../../strongs/h/h5045.md) of the [kᵊrēṯî](../../strongs/h/h3774.md), and upon the coast which belongeth to [Yehuwdah](../../strongs/h/h3063.md), and upon the [neḡeḇ](../../strongs/h/h5045.md) of [Kālēḇ](../../strongs/h/h3612.md); and we [śārap̄](../../strongs/h/h8313.md) [Ṣiqlāḡ](../../strongs/h/h6860.md) with ['esh](../../strongs/h/h784.md).

<a name="1samuel_30_15"></a>1Samuel 30:15

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to him, Canst thou [yarad](../../strongs/h/h3381.md) me to this [gᵊḏûḏ](../../strongs/h/h1416.md)? And he ['āmar](../../strongs/h/h559.md), [shaba'](../../strongs/h/h7650.md) unto me by ['Elohiym](../../strongs/h/h430.md), that thou wilt neither [muwth](../../strongs/h/h4191.md) me, nor [cagar](../../strongs/h/h5462.md) me into the [yad](../../strongs/h/h3027.md) of my ['adown](../../strongs/h/h113.md), and I will bring thee [yarad](../../strongs/h/h3381.md) to this [gᵊḏûḏ](../../strongs/h/h1416.md).

<a name="1samuel_30_16"></a>1Samuel 30:16

And when he had [yarad](../../strongs/h/h3381.md) him, behold, they were [nāṭaš](../../strongs/h/h5203.md) upon [paniym](../../strongs/h/h6440.md) the ['erets](../../strongs/h/h776.md), ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and [ḥāḡaḡ](../../strongs/h/h2287.md), because of all the [gadowl](../../strongs/h/h1419.md) [šālāl](../../strongs/h/h7998.md) that they had [laqach](../../strongs/h/h3947.md) out of the ['erets](../../strongs/h/h776.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and out of the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="1samuel_30_17"></a>1Samuel 30:17

And [Dāviḏ](../../strongs/h/h1732.md) [nakah](../../strongs/h/h5221.md) them from the [nešep̄](../../strongs/h/h5399.md) even unto the ['ereb](../../strongs/h/h6153.md) of the next [māḥŏrāṯ](../../strongs/h/h4283.md): and there [mālaṭ](../../strongs/h/h4422.md) not an ['iysh](../../strongs/h/h376.md) of them, save four hundred [naʿar](../../strongs/h/h5288.md) ['iysh](../../strongs/h/h376.md), which [rāḵaḇ](../../strongs/h/h7392.md) upon [gāmāl](../../strongs/h/h1581.md), and [nûs](../../strongs/h/h5127.md).

<a name="1samuel_30_18"></a>1Samuel 30:18

And [Dāviḏ](../../strongs/h/h1732.md) [natsal](../../strongs/h/h5337.md) all that the [ʿĂmālēq](../../strongs/h/h6002.md) had carried [laqach](../../strongs/h/h3947.md): and [Dāviḏ](../../strongs/h/h1732.md) [natsal](../../strongs/h/h5337.md) his two ['ishshah](../../strongs/h/h802.md).

<a name="1samuel_30_19"></a>1Samuel 30:19

And there was nothing [ʿāḏar](../../strongs/h/h5737.md) to them, neither [qāṭān](../../strongs/h/h6996.md) nor [gadowl](../../strongs/h/h1419.md), neither [ben](../../strongs/h/h1121.md) nor [bath](../../strongs/h/h1323.md), neither [šālāl](../../strongs/h/h7998.md), nor any thing that they had [laqach](../../strongs/h/h3947.md) to them: [Dāviḏ](../../strongs/h/h1732.md) [shuwb](../../strongs/h/h7725.md) all.

<a name="1samuel_30_20"></a>1Samuel 30:20

And [Dāviḏ](../../strongs/h/h1732.md) [laqach](../../strongs/h/h3947.md) all the [tso'n](../../strongs/h/h6629.md) and the [bāqār](../../strongs/h/h1241.md), which they [nāhaḡ](../../strongs/h/h5090.md) [paniym](../../strongs/h/h6440.md) those other [miqnê](../../strongs/h/h4735.md), and ['āmar](../../strongs/h/h559.md), This is [Dāviḏ](../../strongs/h/h1732.md) [šālāl](../../strongs/h/h7998.md).

<a name="1samuel_30_21"></a>1Samuel 30:21

And [Dāviḏ](../../strongs/h/h1732.md) [bow'](../../strongs/h/h935.md) to the two hundred ['enowsh](../../strongs/h/h582.md), which were so [pāḡar](../../strongs/h/h6296.md) that they could not [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) [Dāviḏ](../../strongs/h/h1732.md), whom they had made also to [yashab](../../strongs/h/h3427.md) at the [nachal](../../strongs/h/h5158.md) [bᵊśôr](../../strongs/h/h1308.md): and they [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) [Dāviḏ](../../strongs/h/h1732.md), and to [qārā'](../../strongs/h/h7125.md) the ['am](../../strongs/h/h5971.md) that were with him: and when [Dāviḏ](../../strongs/h/h1732.md) came [nāḡaš](../../strongs/h/h5066.md) to the ['am](../../strongs/h/h5971.md), he [sha'al](../../strongs/h/h7592.md) [shalowm](../../strongs/h/h7965.md) them.

<a name="1samuel_30_22"></a>1Samuel 30:22

Then ['anah](../../strongs/h/h6030.md) all the [ra'](../../strongs/h/h7451.md) ['iysh](../../strongs/h/h376.md) and men of [beliya'al](../../strongs/h/h1100.md), of ['enowsh](../../strongs/h/h582.md) that [halak](../../strongs/h/h1980.md) with [Dāviḏ](../../strongs/h/h1732.md), and ['āmar](../../strongs/h/h559.md), Because they [halak](../../strongs/h/h1980.md) not with us, we will not [nathan](../../strongs/h/h5414.md) them ought of the [šālāl](../../strongs/h/h7998.md) that we have [natsal](../../strongs/h/h5337.md), save to every ['iysh](../../strongs/h/h376.md) his ['ishshah](../../strongs/h/h802.md) and his [ben](../../strongs/h/h1121.md), that they may [nāhaḡ](../../strongs/h/h5090.md) them, and [yālaḵ](../../strongs/h/h3212.md).

<a name="1samuel_30_23"></a>1Samuel 30:23

Then ['āmar](../../strongs/h/h559.md) [Dāviḏ](../../strongs/h/h1732.md), Ye shall not do ['asah](../../strongs/h/h6213.md), my ['ach](../../strongs/h/h251.md), with that which [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) us, who hath [shamar](../../strongs/h/h8104.md) us, and [nathan](../../strongs/h/h5414.md) the [gᵊḏûḏ](../../strongs/h/h1416.md) that [bow'](../../strongs/h/h935.md) against us into our [yad](../../strongs/h/h3027.md).

<a name="1samuel_30_24"></a>1Samuel 30:24

For who will [shama'](../../strongs/h/h8085.md) unto you in this [dabar](../../strongs/h/h1697.md)? but as his [cheleq](../../strongs/h/h2506.md) is that goeth [yarad](../../strongs/h/h3381.md) [yarad](../../strongs/h/h3381.md) to the [milḥāmâ](../../strongs/h/h4421.md), so shall his [cheleq](../../strongs/h/h2506.md) be that [yashab](../../strongs/h/h3427.md) by the [kĕliy](../../strongs/h/h3627.md): they shall [chalaq](../../strongs/h/h2505.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="1samuel_30_25"></a>1Samuel 30:25

And it was so from that [yowm](../../strongs/h/h3117.md) [maʿal](../../strongs/h/h4605.md), that he [śûm](../../strongs/h/h7760.md) it a [choq](../../strongs/h/h2706.md) and a [mishpat](../../strongs/h/h4941.md) for [Yisra'el](../../strongs/h/h3478.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="1samuel_30_26"></a>1Samuel 30:26

And when [Dāviḏ](../../strongs/h/h1732.md) [bow'](../../strongs/h/h935.md) to [Ṣiqlāḡ](../../strongs/h/h6860.md), he [shalach](../../strongs/h/h7971.md) of the [šālāl](../../strongs/h/h7998.md) unto the [zāqēn](../../strongs/h/h2205.md) of [Yehuwdah](../../strongs/h/h3063.md), even to his [rea'](../../strongs/h/h7453.md), ['āmar](../../strongs/h/h559.md), Behold a [bĕrakah](../../strongs/h/h1293.md) for you of the [šālāl](../../strongs/h/h7998.md) of the ['oyeb](../../strongs/h/h341.md) of [Yĕhovah](../../strongs/h/h3068.md);

<a name="1samuel_30_27"></a>1Samuel 30:27

To them which were in [Bêṯ-'ēl](../../strongs/h/h1008.md), and to them which were in [neḡeḇ](../../strongs/h/h5045.md) [Rimmôṯ](../../strongs/h/h7418.md), and to them which were in [Yatîr](../../strongs/h/h3492.md),

<a name="1samuel_30_28"></a>1Samuel 30:28

And to them which were in [ʿĂrôʿēr](../../strongs/h/h6177.md), and to them which were in [Śip̄Môṯ](../../strongs/h/h8224.md), and to them which were in ['Eštᵊmōaʿ](../../strongs/h/h851.md),

<a name="1samuel_30_29"></a>1Samuel 30:29

And to them which were in [Rāḵāl](../../strongs/h/h7403.md), and to them which were in the [ʿîr](../../strongs/h/h5892.md) of the [yᵊraḥmᵊ'ēlî](../../strongs/h/h3397.md), and to them which were in the [ʿîr](../../strongs/h/h5892.md) of the [Qênî](../../strongs/h/h7017.md),

<a name="1samuel_30_30"></a>1Samuel 30:30

And to them which were in [Ḥārmâ](../../strongs/h/h2767.md), and to them which were in [Kôr ʿĀšān](../../strongs/h/h3565.md), and to them which were in [ʿĂṯāḵ](../../strongs/h/h6269.md),

<a name="1samuel_30_31"></a>1Samuel 30:31

And to them which were in [Ḥeḇrôn](../../strongs/h/h2275.md), and to all the [maqowm](../../strongs/h/h4725.md) where [Dāviḏ](../../strongs/h/h1732.md) himself and his ['enowsh](../../strongs/h/h582.md) were wont to [halak](../../strongs/h/h1980.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 29](1samuel_29.md) - [1Samuel 31](1samuel_31.md)