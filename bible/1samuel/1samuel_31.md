# [1Samuel 31](https://www.blueletterbible.org/kjv/1samuel/31)

<a name="1samuel_31_1"></a>1Samuel 31:1

Now the [Pᵊlištî](../../strongs/h/h6430.md) [lāḥam](../../strongs/h/h3898.md) against [Yisra'el](../../strongs/h/h3478.md): and the ['enowsh](../../strongs/h/h582.md) of [Yisra'el](../../strongs/h/h3478.md) [nûs](../../strongs/h/h5127.md) from [paniym](../../strongs/h/h6440.md) the [Pᵊlištî](../../strongs/h/h6430.md), and [naphal](../../strongs/h/h5307.md) [ḥālāl](../../strongs/h/h2491.md) in [har](../../strongs/h/h2022.md) [Gilbōaʿ](../../strongs/h/h1533.md).

<a name="1samuel_31_2"></a>1Samuel 31:2

And the [Pᵊlištî](../../strongs/h/h6430.md) [dāḇaq](../../strongs/h/h1692.md) upon [Šā'ûl](../../strongs/h/h7586.md) and upon his [ben](../../strongs/h/h1121.md); and the [Pᵊlištî](../../strongs/h/h6430.md) [nakah](../../strongs/h/h5221.md) [Yᵊhônāṯān](../../strongs/h/h3083.md), and ['Ăḇînāḏāḇ](../../strongs/h/h41.md), and [Malkîšûaʿ](../../strongs/h/h4444.md), [Šā'ûl](../../strongs/h/h7586.md) [ben](../../strongs/h/h1121.md).

<a name="1samuel_31_3"></a>1Samuel 31:3

And the [milḥāmâ](../../strongs/h/h4421.md) went [kabad](../../strongs/h/h3513.md) against [Šā'ûl](../../strongs/h/h7586.md), and the [yārâ](../../strongs/h/h3384.md) ['enowsh](../../strongs/h/h582.md) [qesheth](../../strongs/h/h7198.md) [māṣā'](../../strongs/h/h4672.md) him; and he was [me'od](../../strongs/h/h3966.md) [chuwl](../../strongs/h/h2342.md) of the [yārâ](../../strongs/h/h3384.md) ['enowsh](../../strongs/h/h582.md) [qesheth](../../strongs/h/h7198.md).

<a name="1samuel_31_4"></a>1Samuel 31:4

Then ['āmar](../../strongs/h/h559.md) [Šā'ûl](../../strongs/h/h7586.md) unto his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md), [šālap̄](../../strongs/h/h8025.md) thy [chereb](../../strongs/h/h2719.md), and [dāqar](../../strongs/h/h1856.md) me therewith; lest these [ʿārēl](../../strongs/h/h6189.md) [bow'](../../strongs/h/h935.md) and [dāqar](../../strongs/h/h1856.md) me, and [ʿālal](../../strongs/h/h5953.md) me. But his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md) ['āḇâ](../../strongs/h/h14.md) not; for he was [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md). Therefore [Šā'ûl](../../strongs/h/h7586.md) [laqach](../../strongs/h/h3947.md) a [chereb](../../strongs/h/h2719.md), and [naphal](../../strongs/h/h5307.md) upon it.

<a name="1samuel_31_5"></a>1Samuel 31:5

And when his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md) [ra'ah](../../strongs/h/h7200.md) that [Šā'ûl](../../strongs/h/h7586.md) was [muwth](../../strongs/h/h4191.md), he [naphal](../../strongs/h/h5307.md) likewise upon his [chereb](../../strongs/h/h2719.md), and [muwth](../../strongs/h/h4191.md) with him.

<a name="1samuel_31_6"></a>1Samuel 31:6

So [Šā'ûl](../../strongs/h/h7586.md) [muwth](../../strongs/h/h4191.md), and his three [ben](../../strongs/h/h1121.md), and his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md), and all his ['enowsh](../../strongs/h/h582.md), that same [yowm](../../strongs/h/h3117.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="1samuel_31_7"></a>1Samuel 31:7

And when the ['enowsh](../../strongs/h/h582.md) of [Yisra'el](../../strongs/h/h3478.md) that were on the other [ʿēḇer](../../strongs/h/h5676.md) of the [ʿēmeq](../../strongs/h/h6010.md), and they that were on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md), [ra'ah](../../strongs/h/h7200.md) that the ['enowsh](../../strongs/h/h582.md) of [Yisra'el](../../strongs/h/h3478.md) [nûs](../../strongs/h/h5127.md), and that [Šā'ûl](../../strongs/h/h7586.md) and his [ben](../../strongs/h/h1121.md) were [muwth](../../strongs/h/h4191.md), they ['azab](../../strongs/h/h5800.md) the [ʿîr](../../strongs/h/h5892.md), and [nûs](../../strongs/h/h5127.md); and the [Pᵊlištî](../../strongs/h/h6430.md) [bow'](../../strongs/h/h935.md) and [yashab](../../strongs/h/h3427.md) in them.

<a name="1samuel_31_8"></a>1Samuel 31:8

And it came to pass on the [māḥŏrāṯ](../../strongs/h/h4283.md), when the [Pᵊlištî](../../strongs/h/h6430.md) [bow'](../../strongs/h/h935.md) to [pāšaṭ](../../strongs/h/h6584.md) the [ḥālāl](../../strongs/h/h2491.md), that they [māṣā'](../../strongs/h/h4672.md) [Šā'ûl](../../strongs/h/h7586.md) and his three [ben](../../strongs/h/h1121.md) [naphal](../../strongs/h/h5307.md) in [har](../../strongs/h/h2022.md) [Gilbōaʿ](../../strongs/h/h1533.md).

<a name="1samuel_31_9"></a>1Samuel 31:9

And they [karath](../../strongs/h/h3772.md) his [ro'sh](../../strongs/h/h7218.md), and [pāšaṭ](../../strongs/h/h6584.md) his [kĕliy](../../strongs/h/h3627.md), and [shalach](../../strongs/h/h7971.md) into the ['erets](../../strongs/h/h776.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [cabiyb](../../strongs/h/h5439.md), to [bāśar](../../strongs/h/h1319.md) it in the [bayith](../../strongs/h/h1004.md) of their [ʿāṣāḇ](../../strongs/h/h6091.md), and among the ['am](../../strongs/h/h5971.md).

<a name="1samuel_31_10"></a>1Samuel 31:10

And they [śûm](../../strongs/h/h7760.md) his [kĕliy](../../strongs/h/h3627.md) in the [bayith](../../strongs/h/h1004.md) of [ʿAštārōṯ](../../strongs/h/h6252.md) [bêṯ ʿaštārôṯ](../../strongs/h/h1045.md): and they [tāqaʿ](../../strongs/h/h8628.md) his [gᵊvîyâ](../../strongs/h/h1472.md) to the [ḥômâ](../../strongs/h/h2346.md) of [Bêṯ Šᵊ'Ān](../../strongs/h/h1052.md).

<a name="1samuel_31_11"></a>1Samuel 31:11

And when the [yashab](../../strongs/h/h3427.md) of [Yāḇēš](../../strongs/h/h3003.md) [Gilʿāḏ](../../strongs/h/h1568.md) [shama'](../../strongs/h/h8085.md) of that which the [Pᵊlištî](../../strongs/h/h6430.md) had ['asah](../../strongs/h/h6213.md) to [Šā'ûl](../../strongs/h/h7586.md);

<a name="1samuel_31_12"></a>1Samuel 31:12

All the [ḥayil](../../strongs/h/h2428.md) ['iysh](../../strongs/h/h376.md) H381 [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) all [layil](../../strongs/h/h3915.md), and [laqach](../../strongs/h/h3947.md) the [gᵊvîyâ](../../strongs/h/h1472.md) of [Šā'ûl](../../strongs/h/h7586.md) and the [gᵊvîyâ](../../strongs/h/h1472.md) of his [ben](../../strongs/h/h1121.md) from the [ḥômâ](../../strongs/h/h2346.md) of [Bêṯ Šᵊ'Ān](../../strongs/h/h1052.md), and [bow'](../../strongs/h/h935.md) to [Yāḇēš](../../strongs/h/h3003.md), and [śārap̄](../../strongs/h/h8313.md) them there.

<a name="1samuel_31_13"></a>1Samuel 31:13

And they [laqach](../../strongs/h/h3947.md) their ['etsem](../../strongs/h/h6106.md), and [qāḇar](../../strongs/h/h6912.md) them under an ['ēšel](../../strongs/h/h815.md) at [Yāḇēš](../../strongs/h/h3003.md), and [ṣûm](../../strongs/h/h6684.md) seven [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 30](1samuel_30.md)