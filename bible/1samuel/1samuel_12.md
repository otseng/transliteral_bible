# [1Samuel 12](https://www.blueletterbible.org/kjv/1samuel/12)

<a name="1samuel_12_1"></a>1Samuel 12:1

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) unto all [Yisra'el](../../strongs/h/h3478.md), Behold, I have [shama'](../../strongs/h/h8085.md) unto your [qowl](../../strongs/h/h6963.md) in all that ye ['āmar](../../strongs/h/h559.md) unto me, and have [mālaḵ](../../strongs/h/h4427.md) a [melek](../../strongs/h/h4428.md) over you.

<a name="1samuel_12_2"></a>1Samuel 12:2

And now, behold, the [melek](../../strongs/h/h4428.md) [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) you: and I am [zāqēn](../../strongs/h/h2204.md) and [śîḇ](../../strongs/h/h7867.md); and, behold, my [ben](../../strongs/h/h1121.md) are with you: and I have [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) you from my [nāʿur](../../strongs/h/h5271.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="1samuel_12_3"></a>1Samuel 12:3

Behold, here I am: ['anah](../../strongs/h/h6030.md) against me before [Yĕhovah](../../strongs/h/h3068.md), and before his [mashiyach](../../strongs/h/h4899.md): whose [showr](../../strongs/h/h7794.md) have I [laqach](../../strongs/h/h3947.md)? or whose [chamowr](../../strongs/h/h2543.md) have I [laqach](../../strongs/h/h3947.md)? or whom have I [ʿāšaq](../../strongs/h/h6231.md)? whom have I [rāṣaṣ](../../strongs/h/h7533.md)? or of whose [yad](../../strongs/h/h3027.md) have I received any [kōp̄er](../../strongs/h/h3724.md) to ['alam](../../strongs/h/h5956.md) mine ['ayin](../../strongs/h/h5869.md) therewith? and I will [shuwb](../../strongs/h/h7725.md) it you.

<a name="1samuel_12_4"></a>1Samuel 12:4

And they ['āmar](../../strongs/h/h559.md), Thou hast not [ʿāšaq](../../strongs/h/h6231.md) us, nor [rāṣaṣ](../../strongs/h/h7533.md) us, neither hast thou [laqach](../../strongs/h/h3947.md) [mᵊ'ûmâ](../../strongs/h/h3972.md) of any ['iysh](../../strongs/h/h376.md) [yad](../../strongs/h/h3027.md).

<a name="1samuel_12_5"></a>1Samuel 12:5

And he ['āmar](../../strongs/h/h559.md) unto them, [Yĕhovah](../../strongs/h/h3068.md) is ['ed](../../strongs/h/h5707.md) against you, and his [mashiyach](../../strongs/h/h4899.md) is ['ed](../../strongs/h/h5707.md) this [yowm](../../strongs/h/h3117.md), that ye have not [māṣā'](../../strongs/h/h4672.md) [mᵊ'ûmâ](../../strongs/h/h3972.md) in my [yad](../../strongs/h/h3027.md). And they ['āmar](../../strongs/h/h559.md), He is ['ed](../../strongs/h/h5707.md).

<a name="1samuel_12_6"></a>1Samuel 12:6

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), It is [Yĕhovah](../../strongs/h/h3068.md) that ['asah](../../strongs/h/h6213.md) [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md), and that [ʿālâ](../../strongs/h/h5927.md) your ['ab](../../strongs/h/h1.md) [ʿālâ](../../strongs/h/h5927.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="1samuel_12_7"></a>1Samuel 12:7

Now therefore [yatsab](../../strongs/h/h3320.md), that I may [shaphat](../../strongs/h/h8199.md) with you [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) of all the [tsedaqah](../../strongs/h/h6666.md) of [Yĕhovah](../../strongs/h/h3068.md), which he ['asah](../../strongs/h/h6213.md) to you and to your ['ab](../../strongs/h/h1.md).

<a name="1samuel_12_8"></a>1Samuel 12:8

When [Ya'aqob](../../strongs/h/h3290.md) was [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md), and your ['ab](../../strongs/h/h1.md) [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md), then [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md), which [yāṣā'](../../strongs/h/h3318.md) your ['ab](../../strongs/h/h1.md) out of [Mitsrayim](../../strongs/h/h4714.md), and made them [yashab](../../strongs/h/h3427.md) in this [maqowm](../../strongs/h/h4725.md).

<a name="1samuel_12_9"></a>1Samuel 12:9

And when they [shakach](../../strongs/h/h7911.md) [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), he [māḵar](../../strongs/h/h4376.md) them into the [yad](../../strongs/h/h3027.md) of [Sîsrā'](../../strongs/h/h5516.md), [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) of [Ḥāṣôr](../../strongs/h/h2674.md), and into the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md), and they [lāḥam](../../strongs/h/h3898.md) against them.

<a name="1samuel_12_10"></a>1Samuel 12:10

And they [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), We have [chata'](../../strongs/h/h2398.md), because we have ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md), and have ['abad](../../strongs/h/h5647.md) [BaʿAl](../../strongs/h/h1168.md) and [ʿAštārōṯ](../../strongs/h/h6252.md): but now [natsal](../../strongs/h/h5337.md) us out of the [yad](../../strongs/h/h3027.md) of our ['oyeb](../../strongs/h/h341.md), and we will ['abad](../../strongs/h/h5647.md) thee.

<a name="1samuel_12_11"></a>1Samuel 12:11

And [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) [YᵊrubaʿAl](../../strongs/h/h3378.md), and [Bᵊḏān](../../strongs/h/h917.md), and [Yip̄tāḥ](../../strongs/h/h3316.md), and [Šᵊmû'Ēl](../../strongs/h/h8050.md), and [natsal](../../strongs/h/h5337.md) you out of the [yad](../../strongs/h/h3027.md) of your ['oyeb](../../strongs/h/h341.md) [cabiyb](../../strongs/h/h5439.md), and ye [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md).

<a name="1samuel_12_12"></a>1Samuel 12:12

And when ye [ra'ah](../../strongs/h/h7200.md) that [Nāḥāš](../../strongs/h/h5176.md) the [melek](../../strongs/h/h4428.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [bow'](../../strongs/h/h935.md) against you, ye ['āmar](../../strongs/h/h559.md) unto me, Nay; but a [melek](../../strongs/h/h4428.md) shall [mālaḵ](../../strongs/h/h4427.md) over us: when [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) was your [melek](../../strongs/h/h4428.md).

<a name="1samuel_12_13"></a>1Samuel 12:13

Now therefore behold the [melek](../../strongs/h/h4428.md) whom ye have [bāḥar](../../strongs/h/h977.md), and whom ye have [sha'al](../../strongs/h/h7592.md)! and, behold, [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) a [melek](../../strongs/h/h4428.md) over you.

<a name="1samuel_12_14"></a>1Samuel 12:14

If ye will [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md), and ['abad](../../strongs/h/h5647.md) him, and [shama'](../../strongs/h/h8085.md) his [qowl](../../strongs/h/h6963.md), and not [marah](../../strongs/h/h4784.md) against the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md), then shall both ye and also the [melek](../../strongs/h/h4428.md) that [mālaḵ](../../strongs/h/h4427.md) over you continue ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md):

<a name="1samuel_12_15"></a>1Samuel 12:15

But if ye will not [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md), but [marah](../../strongs/h/h4784.md) against the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md), then shall the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) be against you, as it was against your ['ab](../../strongs/h/h1.md).

<a name="1samuel_12_16"></a>1Samuel 12:16

Now therefore [yatsab](../../strongs/h/h3320.md) and [ra'ah](../../strongs/h/h7200.md) this [gadowl](../../strongs/h/h1419.md) [dabar](../../strongs/h/h1697.md), which [Yĕhovah](../../strongs/h/h3068.md) will ['asah](../../strongs/h/h6213.md) before your ['ayin](../../strongs/h/h5869.md).

<a name="1samuel_12_17"></a>1Samuel 12:17

Is it not [ḥiṭṭâ](../../strongs/h/h2406.md) [qāṣîr](../../strongs/h/h7105.md) to [yowm](../../strongs/h/h3117.md)? I will [qara'](../../strongs/h/h7121.md) unto [Yĕhovah](../../strongs/h/h3068.md), and he shall [nathan](../../strongs/h/h5414.md) [qowl](../../strongs/h/h6963.md) and [māṭār](../../strongs/h/h4306.md); that ye may [yada'](../../strongs/h/h3045.md) and [ra'ah](../../strongs/h/h7200.md) that your [ra'](../../strongs/h/h7451.md) is [rab](../../strongs/h/h7227.md), which ye have ['asah](../../strongs/h/h6213.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), in [sha'al](../../strongs/h/h7592.md) you a [melek](../../strongs/h/h4428.md).

<a name="1samuel_12_18"></a>1Samuel 12:18

So [Šᵊmû'Ēl](../../strongs/h/h8050.md) [qara'](../../strongs/h/h7121.md) unto [Yĕhovah](../../strongs/h/h3068.md); and [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) [qowl](../../strongs/h/h6963.md) and [māṭār](../../strongs/h/h4306.md) that [yowm](../../strongs/h/h3117.md): and all the ['am](../../strongs/h/h5971.md) [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) and [Šᵊmû'Ēl](../../strongs/h/h8050.md).

<a name="1samuel_12_19"></a>1Samuel 12:19

And all the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md) unto [Šᵊmû'Ēl](../../strongs/h/h8050.md), [palal](../../strongs/h/h6419.md) for thy ['ebed](../../strongs/h/h5650.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), that we [muwth](../../strongs/h/h4191.md) not: for we have added unto all our [chatta'ath](../../strongs/h/h2403.md) this [ra'](../../strongs/h/h7451.md), to [sha'al](../../strongs/h/h7592.md) us a [melek](../../strongs/h/h4428.md).

<a name="1samuel_12_20"></a>1Samuel 12:20

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), [yare'](../../strongs/h/h3372.md) not: ye have ['asah](../../strongs/h/h6213.md) all this [ra'](../../strongs/h/h7451.md): yet turn not [cuwr](../../strongs/h/h5493.md) from ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md), but ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) with all your [lebab](../../strongs/h/h3824.md);

<a name="1samuel_12_21"></a>1Samuel 12:21

And turn ye not [cuwr](../../strongs/h/h5493.md): for then should ye go ['aḥar](../../strongs/h/h310.md) [tohuw](../../strongs/h/h8414.md) things, which cannot [yāʿal](../../strongs/h/h3276.md) nor [natsal](../../strongs/h/h5337.md); for they are [tohuw](../../strongs/h/h8414.md).

<a name="1samuel_12_22"></a>1Samuel 12:22

For [Yĕhovah](../../strongs/h/h3068.md) will not [nāṭaš](../../strongs/h/h5203.md) his ['am](../../strongs/h/h5971.md) for his [gadowl](../../strongs/h/h1419.md) [shem](../../strongs/h/h8034.md) sake: because it hath [yā'al](../../strongs/h/h2974.md) [Yĕhovah](../../strongs/h/h3068.md) to ['asah](../../strongs/h/h6213.md) you his ['am](../../strongs/h/h5971.md).

<a name="1samuel_12_23"></a>1Samuel 12:23

Moreover as for me, [ḥālîlâ](../../strongs/h/h2486.md) that I should [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md) in [ḥāḏal](../../strongs/h/h2308.md) to [palal](../../strongs/h/h6419.md) for you: but I will [yārâ](../../strongs/h/h3384.md) you the [towb](../../strongs/h/h2896.md) and the [yashar](../../strongs/h/h3477.md) [derek](../../strongs/h/h1870.md):

<a name="1samuel_12_24"></a>1Samuel 12:24

Only [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md), and ['abad](../../strongs/h/h5647.md) him in ['emeth](../../strongs/h/h571.md) with all your [lebab](../../strongs/h/h3824.md): for [ra'ah](../../strongs/h/h7200.md) how great things he hath [gāḏal](../../strongs/h/h1431.md) for you.

<a name="1samuel_12_25"></a>1Samuel 12:25

But if ye shall [ra'a'](../../strongs/h/h7489.md) do [ra'a'](../../strongs/h/h7489.md), ye shall be [sāp̄â](../../strongs/h/h5595.md), both ye and your [melek](../../strongs/h/h4428.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 11](1samuel_11.md) - [1Samuel 13](1samuel_13.md)