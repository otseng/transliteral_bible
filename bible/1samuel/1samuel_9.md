# [1Samuel 9](https://www.blueletterbible.org/kjv/1samuel/9)

<a name="1samuel_9_1"></a>1Samuel 9:1

Now there was an ['iysh](../../strongs/h/h376.md) of [Binyāmîn](../../strongs/h/h1144.md), whose [shem](../../strongs/h/h8034.md) was [Qîš](../../strongs/h/h7027.md), the [ben](../../strongs/h/h1121.md) of ['Ăḇî'Ēl](../../strongs/h/h22.md), the [ben](../../strongs/h/h1121.md) of [ṣᵊrôr](../../strongs/h/h6872.md), the [ben](../../strongs/h/h1121.md) of [Bᵊḵôraṯ](../../strongs/h/h1064.md), the [ben](../../strongs/h/h1121.md) of ['Ăp̄Îaḥ](../../strongs/h/h647.md), a [Ben-yᵊmînî](../../strongs/h/h1145.md), a [gibôr](../../strongs/h/h1368.md) ['iysh](../../strongs/h/h376.md) of [ḥayil](../../strongs/h/h2428.md).

<a name="1samuel_9_2"></a>1Samuel 9:2

And he had a [ben](../../strongs/h/h1121.md), whose [shem](../../strongs/h/h8034.md) was [Šā'ûl](../../strongs/h/h7586.md), a choice [bāḥûr](../../strongs/h/h970.md), and a [towb](../../strongs/h/h2896.md): and there was not among the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) a [towb](../../strongs/h/h2896.md) ['iysh](../../strongs/h/h376.md) than he: from his [šᵊḵem](../../strongs/h/h7926.md) and [maʿal](../../strongs/h/h4605.md) he was [gāḇōha](../../strongs/h/h1364.md) than any of the ['am](../../strongs/h/h5971.md).

<a name="1samuel_9_3"></a>1Samuel 9:3

And the ['āṯôn](../../strongs/h/h860.md) of [Qîš](../../strongs/h/h7027.md) [Šā'ûl](../../strongs/h/h7586.md) ['ab](../../strongs/h/h1.md) were ['abad](../../strongs/h/h6.md). And [Qîš](../../strongs/h/h7027.md) ['āmar](../../strongs/h/h559.md) to [Šā'ûl](../../strongs/h/h7586.md) his [ben](../../strongs/h/h1121.md), [laqach](../../strongs/h/h3947.md) now one of the [naʿar](../../strongs/h/h5288.md) with thee, and [quwm](../../strongs/h/h6965.md), [yālaḵ](../../strongs/h/h3212.md) [bāqaš](../../strongs/h/h1245.md) the ['āṯôn](../../strongs/h/h860.md).

<a name="1samuel_9_4"></a>1Samuel 9:4

And he ['abar](../../strongs/h/h5674.md) [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), and ['abar](../../strongs/h/h5674.md) the ['erets](../../strongs/h/h776.md) of [šālišâ](../../strongs/h/h8031.md), but they [māṣā'](../../strongs/h/h4672.md) them not: then they ['abar](../../strongs/h/h5674.md) the ['erets](../../strongs/h/h776.md) of [ŠaʿĂlîm](../../strongs/h/h8171.md), and there they were not: and he ['abar](../../strongs/h/h5674.md) the ['erets](../../strongs/h/h776.md) of the [Ben-yᵊmînî](../../strongs/h/h1145.md), but they [māṣā'](../../strongs/h/h4672.md) them not.

<a name="1samuel_9_5"></a>1Samuel 9:5

And when they were [bow'](../../strongs/h/h935.md) to the ['erets](../../strongs/h/h776.md) of [Ṣûp̄](../../strongs/h/h6689.md), [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) to his [naʿar](../../strongs/h/h5288.md) that was with him, [yālaḵ](../../strongs/h/h3212.md), and let us [shuwb](../../strongs/h/h7725.md); lest my ['ab](../../strongs/h/h1.md) [ḥāḏal](../../strongs/h/h2308.md) for the ['āṯôn](../../strongs/h/h860.md), and take [dā'aḡ](../../strongs/h/h1672.md) for us.

<a name="1samuel_9_6"></a>1Samuel 9:6

And he ['āmar](../../strongs/h/h559.md) unto him, Behold now, there is in this [ʿîr](../../strongs/h/h5892.md) an ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), and he is a [kabad](../../strongs/h/h3513.md) ['iysh](../../strongs/h/h376.md); all that he [dabar](../../strongs/h/h1696.md) [bow'](../../strongs/h/h935.md) surely to [bow'](../../strongs/h/h935.md): now let us [yālaḵ](../../strongs/h/h3212.md) thither; peradventure he can [nāḡaḏ](../../strongs/h/h5046.md) us our [derek](../../strongs/h/h1870.md) that we should [halak](../../strongs/h/h1980.md).

<a name="1samuel_9_7"></a>1Samuel 9:7

Then ['āmar](../../strongs/h/h559.md) [Šā'ûl](../../strongs/h/h7586.md) to his [naʿar](../../strongs/h/h5288.md), But, behold, if we [yālaḵ](../../strongs/h/h3212.md), what shall we [bow'](../../strongs/h/h935.md) the ['iysh](../../strongs/h/h376.md)? for the [lechem](../../strongs/h/h3899.md) is ['āzal](../../strongs/h/h235.md) in our [kĕliy](../../strongs/h/h3627.md), and there is not a [tᵊšûrâ](../../strongs/h/h8670.md) to [bow'](../../strongs/h/h935.md) to the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md): what have we?

<a name="1samuel_9_8"></a>1Samuel 9:8

And the [naʿar](../../strongs/h/h5288.md) ['anah](../../strongs/h/h6030.md) [Šā'ûl](../../strongs/h/h7586.md) again, and ['āmar](../../strongs/h/h559.md), Behold, I [māṣā'](../../strongs/h/h4672.md) here at [yad](../../strongs/h/h3027.md) the fourth part of a [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md): that will I [nathan](../../strongs/h/h5414.md) to the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), to [nāḡaḏ](../../strongs/h/h5046.md) us our [derek](../../strongs/h/h1870.md).

<a name="1samuel_9_9"></a>1Samuel 9:9

(Beforetime [paniym](../../strongs/h/h6440.md) in [Yisra'el](../../strongs/h/h3478.md), when an ['iysh](../../strongs/h/h376.md) [yālaḵ](../../strongs/h/h3212.md) to [darash](../../strongs/h/h1875.md) of ['Elohiym](../../strongs/h/h430.md), thus he ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), and let us [yālaḵ](../../strongs/h/h3212.md) to the [ra'ah](../../strongs/h/h7200.md): for he that is [yowm](../../strongs/h/h3117.md) called a [nāḇî'](../../strongs/h/h5030.md) was [paniym](../../strongs/h/h6440.md) [qara'](../../strongs/h/h7121.md) a [ra'ah](../../strongs/h/h7200.md).)

<a name="1samuel_9_10"></a>1Samuel 9:10

Then [dabar](../../strongs/h/h1697.md) [Šā'ûl](../../strongs/h/h7586.md) to his [naʿar](../../strongs/h/h5288.md), [towb](../../strongs/h/h2896.md) ['āmar](../../strongs/h/h559.md); [yālaḵ](../../strongs/h/h3212.md), let us [yālaḵ](../../strongs/h/h3212.md). So they [yālaḵ](../../strongs/h/h3212.md) unto the [ʿîr](../../strongs/h/h5892.md) where the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) was.

<a name="1samuel_9_11"></a>1Samuel 9:11

And as they [ʿālâ](../../strongs/h/h5927.md) the [maʿălê](../../strongs/h/h4608.md) to the [ʿîr](../../strongs/h/h5892.md), they [māṣā'](../../strongs/h/h4672.md) [naʿărâ](../../strongs/h/h5291.md) [yāṣā'](../../strongs/h/h3318.md) to [šā'aḇ](../../strongs/h/h7579.md) [mayim](../../strongs/h/h4325.md), and ['āmar](../../strongs/h/h559.md) unto them, Is the [ra'ah](../../strongs/h/h7200.md) here?

<a name="1samuel_9_12"></a>1Samuel 9:12

And they ['anah](../../strongs/h/h6030.md) them, and ['āmar](../../strongs/h/h559.md), He is; behold, he is [paniym](../../strongs/h/h6440.md) you: make [māhar](../../strongs/h/h4116.md) now, for he [bow'](../../strongs/h/h935.md) to [yowm](../../strongs/h/h3117.md) to the [ʿîr](../../strongs/h/h5892.md); for there is a [zebach](../../strongs/h/h2077.md) of the ['am](../../strongs/h/h5971.md) to [yowm](../../strongs/h/h3117.md) in the [bāmâ](../../strongs/h/h1116.md):

<a name="1samuel_9_13"></a>1Samuel 9:13

As soon as ye be [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md), ye shall straightway [māṣā'](../../strongs/h/h4672.md) him, before he [ʿālâ](../../strongs/h/h5927.md) to the [bāmâ](../../strongs/h/h1116.md) to ['akal](../../strongs/h/h398.md): for the ['am](../../strongs/h/h5971.md) will not ['akal](../../strongs/h/h398.md) until he [bow'](../../strongs/h/h935.md), because he doth [barak](../../strongs/h/h1288.md) the [zebach](../../strongs/h/h2077.md); and ['aḥar](../../strongs/h/h310.md)  they ['akal](../../strongs/h/h398.md) that be [qara'](../../strongs/h/h7121.md). Now therefore get you [ʿālâ](../../strongs/h/h5927.md); for about this [yowm](../../strongs/h/h3117.md) ye shall [māṣā'](../../strongs/h/h4672.md) him.

<a name="1samuel_9_14"></a>1Samuel 9:14

And they [ʿālâ](../../strongs/h/h5927.md) [tavek](../../strongs/h/h8432.md) the [ʿîr](../../strongs/h/h5892.md): and when they were [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md), behold, [Šᵊmû'Ēl](../../strongs/h/h8050.md) [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) them, for to [ʿālâ](../../strongs/h/h5927.md) to the [bāmâ](../../strongs/h/h1116.md).

<a name="1samuel_9_15"></a>1Samuel 9:15

Now [Yĕhovah](../../strongs/h/h3068.md) had [gālâ](../../strongs/h/h1540.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md) in his ['ozen](../../strongs/h/h241.md) a [yowm](../../strongs/h/h3117.md) [paniym](../../strongs/h/h6440.md) [Šā'ûl](../../strongs/h/h7586.md) [bow'](../../strongs/h/h935.md), ['āmar](../../strongs/h/h559.md),

<a name="1samuel_9_16"></a>1Samuel 9:16

To [māḥār](../../strongs/h/h4279.md) about this [ʿēṯ](../../strongs/h/h6256.md) I will [shalach](../../strongs/h/h7971.md) thee an ['iysh](../../strongs/h/h376.md) out of the ['erets](../../strongs/h/h776.md) of [Binyāmîn](../../strongs/h/h1144.md), and thou shalt [māšaḥ](../../strongs/h/h4886.md) him to be [nāḡîḏ](../../strongs/h/h5057.md) over my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), that he may [yasha'](../../strongs/h/h3467.md) my ['am](../../strongs/h/h5971.md) out of the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md): for I have [ra'ah](../../strongs/h/h7200.md) upon my ['am](../../strongs/h/h5971.md), because their [tsa'aqah](../../strongs/h/h6818.md) is [bow'](../../strongs/h/h935.md) unto me.

<a name="1samuel_9_17"></a>1Samuel 9:17

And when [Šᵊmû'Ēl](../../strongs/h/h8050.md) [ra'ah](../../strongs/h/h7200.md) [Šā'ûl](../../strongs/h/h7586.md), [Yĕhovah](../../strongs/h/h3068.md) ['anah](../../strongs/h/h6030.md) unto him, Behold the ['iysh](../../strongs/h/h376.md) whom I ['āmar](../../strongs/h/h559.md) to thee of! this same shall [ʿāṣar](../../strongs/h/h6113.md) over my ['am](../../strongs/h/h5971.md).

<a name="1samuel_9_18"></a>1Samuel 9:18

Then [Šā'ûl](../../strongs/h/h7586.md) [nāḡaš](../../strongs/h/h5066.md) to [Šᵊmû'Ēl](../../strongs/h/h8050.md) [tavek](../../strongs/h/h8432.md) the [sha'ar](../../strongs/h/h8179.md), and ['āmar](../../strongs/h/h559.md), [nāḡaḏ](../../strongs/h/h5046.md) me, I pray thee, where the [ra'ah](../../strongs/h/h7200.md) [bayith](../../strongs/h/h1004.md) is.

<a name="1samuel_9_19"></a>1Samuel 9:19

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['anah](../../strongs/h/h6030.md) [Šā'ûl](../../strongs/h/h7586.md), and ['āmar](../../strongs/h/h559.md), I am the [ra'ah](../../strongs/h/h7200.md): [ʿālâ](../../strongs/h/h5927.md) [paniym](../../strongs/h/h6440.md) me unto the [bāmâ](../../strongs/h/h1116.md); for ye shall ['akal](../../strongs/h/h398.md) with me to [yowm](../../strongs/h/h3117.md), and to [boqer](../../strongs/h/h1242.md) I will let thee [shalach](../../strongs/h/h7971.md), and will [nāḡaḏ](../../strongs/h/h5046.md) thee all that is in thine [lebab](../../strongs/h/h3824.md).

<a name="1samuel_9_20"></a>1Samuel 9:20

And as for thine ['āṯôn](../../strongs/h/h860.md) that were ['abad](../../strongs/h/h6.md) three [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md), [śûm](../../strongs/h/h7760.md) not thy [leb](../../strongs/h/h3820.md) on them; for they are [māṣā'](../../strongs/h/h4672.md). And on whom is all the [ḥemdâ](../../strongs/h/h2532.md) of [Yisra'el](../../strongs/h/h3478.md)? Is it not on thee, and on all thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md)?

<a name="1samuel_9_21"></a>1Samuel 9:21

And [Šā'ûl](../../strongs/h/h7586.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), Am not I a [Ben-yᵊmînî](../../strongs/h/h1145.md), of the [qāṭān](../../strongs/h/h6996.md) of the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md)? and my [mišpāḥâ](../../strongs/h/h4940.md) the [ṣāʿîr](../../strongs/h/h6810.md) of all the [mišpāḥâ](../../strongs/h/h4940.md) of the [shebet](../../strongs/h/h7626.md) of [Binyāmîn](../../strongs/h/h1144.md)? wherefore then [dabar](../../strongs/h/h1696.md) thou [dabar](../../strongs/h/h1697.md) to me?

<a name="1samuel_9_22"></a>1Samuel 9:22

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [laqach](../../strongs/h/h3947.md) [Šā'ûl](../../strongs/h/h7586.md) and his [naʿar](../../strongs/h/h5288.md), and [bow'](../../strongs/h/h935.md) them into the [liškâ](../../strongs/h/h3957.md), and made them [nathan](../../strongs/h/h5414.md) in the [ro'sh](../../strongs/h/h7218.md) [maqowm](../../strongs/h/h4725.md) among them that were [qara'](../../strongs/h/h7121.md), which were about thirty ['iysh](../../strongs/h/h376.md).

<a name="1samuel_9_23"></a>1Samuel 9:23

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) unto the [ṭabāḥ](../../strongs/h/h2876.md), [nathan](../../strongs/h/h5414.md) the [mānâ](../../strongs/h/h4490.md) which I [nathan](../../strongs/h/h5414.md) thee, of which I ['āmar](../../strongs/h/h559.md) unto thee, [śûm](../../strongs/h/h7760.md) it by thee.

<a name="1samuel_9_24"></a>1Samuel 9:24

And the [ṭabāḥ](../../strongs/h/h2876.md) [ruwm](../../strongs/h/h7311.md) the [šôq](../../strongs/h/h7785.md), and that which was upon it, and [śûm](../../strongs/h/h7760.md) it [paniym](../../strongs/h/h6440.md) [Šā'ûl](../../strongs/h/h7586.md). And ['āmar](../../strongs/h/h559.md), Behold that which is [šā'ar](../../strongs/h/h7604.md)! [śûm](../../strongs/h/h7760.md) it [paniym](../../strongs/h/h6440.md) thee, and ['akal](../../strongs/h/h398.md): for unto this [môʿēḏ](../../strongs/h/h4150.md) hath it been [shamar](../../strongs/h/h8104.md) for thee since I ['āmar](../../strongs/h/h559.md), I have [qara'](../../strongs/h/h7121.md) the ['am](../../strongs/h/h5971.md). So [Šā'ûl](../../strongs/h/h7586.md) did ['akal](../../strongs/h/h398.md) with [Šᵊmû'Ēl](../../strongs/h/h8050.md) that [yowm](../../strongs/h/h3117.md).

<a name="1samuel_9_25"></a>1Samuel 9:25

And when they were [yarad](../../strongs/h/h3381.md) from the [bāmâ](../../strongs/h/h1116.md) into the [ʿîr](../../strongs/h/h5892.md), Samuel [dabar](../../strongs/h/h1696.md) with [Šā'ûl](../../strongs/h/h7586.md) upon the top of the [gāḡ](../../strongs/h/h1406.md).

<a name="1samuel_9_26"></a>1Samuel 9:26

And they [šāḵam](../../strongs/h/h7925.md): and it came to pass about the [ʿālâ](../../strongs/h/h5927.md) of the [šaḥar](../../strongs/h/h7837.md), that [Šᵊmû'Ēl](../../strongs/h/h8050.md) [qara'](../../strongs/h/h7121.md) [Šā'ûl](../../strongs/h/h7586.md) to the top of the [gāḡ](../../strongs/h/h1406.md), ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md), that I may send thee [shalach](../../strongs/h/h7971.md). And [Šā'ûl](../../strongs/h/h7586.md) [quwm](../../strongs/h/h6965.md), and they [yāṣā'](../../strongs/h/h3318.md) both of them, he and [Šᵊmû'Ēl](../../strongs/h/h8050.md), [ḥûṣ](../../strongs/h/h2351.md).

<a name="1samuel_9_27"></a>1Samuel 9:27

And as they were [yarad](../../strongs/h/h3381.md) to the [qāṣê](../../strongs/h/h7097.md) of the [ʿîr](../../strongs/h/h5892.md), [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) to [Šā'ûl](../../strongs/h/h7586.md), ['āmar](../../strongs/h/h559.md) the [naʿar](../../strongs/h/h5288.md) ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) us, (and he ['abar](../../strongs/h/h5674.md),) but ['amad](../../strongs/h/h5975.md) thou still a [yowm](../../strongs/h/h3117.md), that I may [shama'](../../strongs/h/h8085.md) thee the [dabar](../../strongs/h/h1697.md) of ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 8](1samuel_8.md) - [1Samuel 10](1samuel_10.md)