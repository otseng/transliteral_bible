# [1Samuel 22](https://www.blueletterbible.org/kjv/1samuel/22)

<a name="1samuel_22_1"></a>1Samuel 22:1

[Dāviḏ](../../strongs/h/h1732.md) therefore [yālaḵ](../../strongs/h/h3212.md) thence, and [mālaṭ](../../strongs/h/h4422.md) to the [mᵊʿārâ](../../strongs/h/h4631.md) [ʿĂḏullām](../../strongs/h/h5725.md): and when his ['ach](../../strongs/h/h251.md) and all his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) [shama'](../../strongs/h/h8085.md) it, they [yarad](../../strongs/h/h3381.md) thither to him.

<a name="1samuel_22_2"></a>1Samuel 22:2

And every ['iysh](../../strongs/h/h376.md) that was in [māṣôq](../../strongs/h/h4689.md), and every ['iysh](../../strongs/h/h376.md) that was in [nāšā'](../../strongs/h/h5378.md), and every ['iysh](../../strongs/h/h376.md) that was [mar](../../strongs/h/h4751.md) [nephesh](../../strongs/h/h5315.md), [qāḇaṣ](../../strongs/h/h6908.md) themselves unto him; and he became a [śar](../../strongs/h/h8269.md) over them: and there were with him about four hundred ['iysh](../../strongs/h/h376.md).

<a name="1samuel_22_3"></a>1Samuel 22:3

And [Dāviḏ](../../strongs/h/h1732.md) [yālaḵ](../../strongs/h/h3212.md) thence to [Miṣpê](../../strongs/h/h4708.md) of [Mô'āḇ](../../strongs/h/h4124.md): and he ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md), Let my ['ab](../../strongs/h/h1.md) and my ['em](../../strongs/h/h517.md), I pray thee, [yāṣā'](../../strongs/h/h3318.md), and be with you, till I [yada'](../../strongs/h/h3045.md) what ['Elohiym](../../strongs/h/h430.md) will ['asah](../../strongs/h/h6213.md) for me.

<a name="1samuel_22_4"></a>1Samuel 22:4

And he [nachah](../../strongs/h/h5148.md) them [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md): and they [yashab](../../strongs/h/h3427.md) with him all the [yowm](../../strongs/h/h3117.md) that [Dāviḏ](../../strongs/h/h1732.md) was in the [matsuwd](../../strongs/h/h4686.md).

<a name="1samuel_22_5"></a>1Samuel 22:5

And the [nāḇî'](../../strongs/h/h5030.md) [Gāḏ](../../strongs/h/h1410.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), [yashab](../../strongs/h/h3427.md) not in the [matsuwd](../../strongs/h/h4686.md); [yālaḵ](../../strongs/h/h3212.md), and [bow'](../../strongs/h/h935.md) thee into the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md). Then [Dāviḏ](../../strongs/h/h1732.md) [yālaḵ](../../strongs/h/h3212.md), and [bow'](../../strongs/h/h935.md) into the [yaʿar](../../strongs/h/h3293.md) of [Ḥereṯ](../../strongs/h/h2802.md).

<a name="1samuel_22_6"></a>1Samuel 22:6

When [Šā'ûl](../../strongs/h/h7586.md) [shama'](../../strongs/h/h8085.md) that [Dāviḏ](../../strongs/h/h1732.md) was [yada'](../../strongs/h/h3045.md), and the ['enowsh](../../strongs/h/h582.md) that were with him, (now [Šā'ûl](../../strongs/h/h7586.md) [yashab](../../strongs/h/h3427.md) in [giḇʿâ](../../strongs/h/h1390.md) under an ['ēšel](../../strongs/h/h815.md) in [rāmâ](../../strongs/h/h7414.md), having his [ḥănîṯ](../../strongs/h/h2595.md) in his [yad](../../strongs/h/h3027.md), and all his ['ebed](../../strongs/h/h5650.md) were [nāṣaḇ](../../strongs/h/h5324.md) about him;)

<a name="1samuel_22_7"></a>1Samuel 22:7

Then [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto his ['ebed](../../strongs/h/h5650.md) that [nāṣaḇ](../../strongs/h/h5324.md) about him, Hear [shama'](../../strongs/h/h8085.md), ye [Ben-yᵊmînî](../../strongs/h/h1145.md); will the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md) [nathan](../../strongs/h/h5414.md) every one of you [sadeh](../../strongs/h/h7704.md) and [kerem](../../strongs/h/h3754.md), and [śûm](../../strongs/h/h7760.md) you all [śar](../../strongs/h/h8269.md) of thousands, and [śar](../../strongs/h/h8269.md) of hundreds;

<a name="1samuel_22_8"></a>1Samuel 22:8

That all of you have [qāšar](../../strongs/h/h7194.md) against me, and there is none that [gālâ](../../strongs/h/h1540.md) ['ozen](../../strongs/h/h241.md) me that my [ben](../../strongs/h/h1121.md) hath made a [karath](../../strongs/h/h3772.md) with the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md), and there is none of you that is [ḥālâ](../../strongs/h/h2470.md) for me, or [gālâ](../../strongs/h/h1540.md) ['ozen](../../strongs/h/h241.md) unto me that my [ben](../../strongs/h/h1121.md) hath stirred [quwm](../../strongs/h/h6965.md) my ['ebed](../../strongs/h/h5650.md) against me, to ['arab](../../strongs/h/h693.md), as at this [yowm](../../strongs/h/h3117.md)?

<a name="1samuel_22_9"></a>1Samuel 22:9

Then ['anah](../../strongs/h/h6030.md) [Dō'Ēḡ](../../strongs/h/h1673.md) the ['Ăḏōmî](../../strongs/h/h130.md), which was [nāṣaḇ](../../strongs/h/h5324.md) over the ['ebed](../../strongs/h/h5650.md) of [Šā'ûl](../../strongs/h/h7586.md), and ['āmar](../../strongs/h/h559.md), I [ra'ah](../../strongs/h/h7200.md) the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md) [bow'](../../strongs/h/h935.md) to [nōḇ](../../strongs/h/h5011.md), to ['Ăḥîmeleḵ](../../strongs/h/h288.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîṭûḇ](../../strongs/h/h285.md).

<a name="1samuel_22_10"></a>1Samuel 22:10

And he [sha'al](../../strongs/h/h7592.md) of [Yĕhovah](../../strongs/h/h3068.md) for him, and [nathan](../../strongs/h/h5414.md) him [ṣêḏâ](../../strongs/h/h6720.md), and [nathan](../../strongs/h/h5414.md) him the [chereb](../../strongs/h/h2719.md) of [Gālyaṯ](../../strongs/h/h1555.md) the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1samuel_22_11"></a>1Samuel 22:11

Then the [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md) to [qara'](../../strongs/h/h7121.md) ['Ăḥîmeleḵ](../../strongs/h/h288.md) the [kōhēn](../../strongs/h/h3548.md), the [ben](../../strongs/h/h1121.md) of ['Ăḥîṭûḇ](../../strongs/h/h285.md), and all his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), the [kōhēn](../../strongs/h/h3548.md) that were in [nōḇ](../../strongs/h/h5011.md): and they [bow'](../../strongs/h/h935.md) all of them to the [melek](../../strongs/h/h4428.md).

<a name="1samuel_22_12"></a>1Samuel 22:12

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) now, thou [ben](../../strongs/h/h1121.md) of ['Ăḥîṭûḇ](../../strongs/h/h285.md). And he ['āmar](../../strongs/h/h559.md), Here I am, my ['adown](../../strongs/h/h113.md).

<a name="1samuel_22_13"></a>1Samuel 22:13

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto him, Why have ye [qāšar](../../strongs/h/h7194.md) against me, thou and the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md), in that thou hast [nathan](../../strongs/h/h5414.md) him [lechem](../../strongs/h/h3899.md), and a [chereb](../../strongs/h/h2719.md), and hast [sha'al](../../strongs/h/h7592.md) of ['Elohiym](../../strongs/h/h430.md) for him, that he should [quwm](../../strongs/h/h6965.md) against me, to ['arab](../../strongs/h/h693.md), as at this [yowm](../../strongs/h/h3117.md)?

<a name="1samuel_22_14"></a>1Samuel 22:14

Then ['Ăḥîmeleḵ](../../strongs/h/h288.md) ['anah](../../strongs/h/h6030.md) the [melek](../../strongs/h/h4428.md), and ['āmar](../../strongs/h/h559.md), And who is so ['aman](../../strongs/h/h539.md) among all thy ['ebed](../../strongs/h/h5650.md) as [Dāviḏ](../../strongs/h/h1732.md), which is the [melek](../../strongs/h/h4428.md) [ḥāṯān](../../strongs/h/h2860.md), and [cuwr](../../strongs/h/h5493.md) at thy [mišmaʿaṯ](../../strongs/h/h4928.md), and is [kabad](../../strongs/h/h3513.md) in thine [bayith](../../strongs/h/h1004.md)?

<a name="1samuel_22_15"></a>1Samuel 22:15

Did I [yowm](../../strongs/h/h3117.md) [ḥālal](../../strongs/h/h2490.md) to [sha'al](../../strongs/h/h7592.md) of ['Elohiym](../../strongs/h/h430.md) for him? be it far from [ḥālîlâ](../../strongs/h/h2486.md): let not the [melek](../../strongs/h/h4428.md) [śûm](../../strongs/h/h7760.md) any [dabar](../../strongs/h/h1697.md) unto his ['ebed](../../strongs/h/h5650.md), nor to all the [bayith](../../strongs/h/h1004.md) of my ['ab](../../strongs/h/h1.md): for thy ['ebed](../../strongs/h/h5650.md) [yada'](../../strongs/h/h3045.md) [dabar](../../strongs/h/h1697.md) of all this, [qāṭān](../../strongs/h/h6996.md) or [gadowl](../../strongs/h/h1419.md).

<a name="1samuel_22_16"></a>1Samuel 22:16

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), Thou shalt [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md), ['Ăḥîmeleḵ](../../strongs/h/h288.md), thou, and all thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md).

<a name="1samuel_22_17"></a>1Samuel 22:17

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto the [rûṣ](../../strongs/h/h7323.md) that [nāṣaḇ](../../strongs/h/h5324.md) about him, [cabab](../../strongs/h/h5437.md), and [muwth](../../strongs/h/h4191.md) the [kōhēn](../../strongs/h/h3548.md) of [Yĕhovah](../../strongs/h/h3068.md); because their [yad](../../strongs/h/h3027.md) also is with [Dāviḏ](../../strongs/h/h1732.md), and because they [yada'](../../strongs/h/h3045.md) when he [bāraḥ](../../strongs/h/h1272.md), and did not [gālâ](../../strongs/h/h1540.md) ['ozen](../../strongs/h/h241.md) it to me. But the ['ebed](../../strongs/h/h5650.md) of the [melek](../../strongs/h/h4428.md) ['āḇâ](../../strongs/h/h14.md) not [shalach](../../strongs/h/h7971.md) their [yad](../../strongs/h/h3027.md) to [pāḡaʿ](../../strongs/h/h6293.md) upon the [kōhēn](../../strongs/h/h3548.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_22_18"></a>1Samuel 22:18

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) to [Dō'Ēḡ](../../strongs/h/h1673.md), [cabab](../../strongs/h/h5437.md) thou, and [pāḡaʿ](../../strongs/h/h6293.md) upon the [kōhēn](../../strongs/h/h3548.md). And [Dō'Ēḡ](../../strongs/h/h1673.md) the ['Ăḏōmî](../../strongs/h/h130.md) [cabab](../../strongs/h/h5437.md), and he [pāḡaʿ](../../strongs/h/h6293.md) upon the [kōhēn](../../strongs/h/h3548.md), and [muwth](../../strongs/h/h4191.md) on that [yowm](../../strongs/h/h3117.md) fourscore and five ['iysh](../../strongs/h/h376.md) that did [nasa'](../../strongs/h/h5375.md) a [baḏ](../../strongs/h/h906.md) ['ēp̄ôḏ](../../strongs/h/h646.md).

<a name="1samuel_22_19"></a>1Samuel 22:19

And [nōḇ](../../strongs/h/h5011.md), the [ʿîr](../../strongs/h/h5892.md) of the [kōhēn](../../strongs/h/h3548.md), [nakah](../../strongs/h/h5221.md) he with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), both ['iysh](../../strongs/h/h376.md) and ['ishshah](../../strongs/h/h802.md), ['owlel](../../strongs/h/h5768.md) and [yānaq](../../strongs/h/h3243.md), and [showr](../../strongs/h/h7794.md), and [chamowr](../../strongs/h/h2543.md), and [śê](../../strongs/h/h7716.md), with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md).

<a name="1samuel_22_20"></a>1Samuel 22:20

And one of the [ben](../../strongs/h/h1121.md) of ['Ăḥîmeleḵ](../../strongs/h/h288.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîṭûḇ](../../strongs/h/h285.md), [shem](../../strongs/h/h8034.md) ['Eḇyāṯār](../../strongs/h/h54.md), [mālaṭ](../../strongs/h/h4422.md), and [bāraḥ](../../strongs/h/h1272.md) ['aḥar](../../strongs/h/h310.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="1samuel_22_21"></a>1Samuel 22:21

And ['Eḇyāṯār](../../strongs/h/h54.md) [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md) that [Šā'ûl](../../strongs/h/h7586.md) had [harag](../../strongs/h/h2026.md) [Yĕhovah](../../strongs/h/h3068.md) [kōhēn](../../strongs/h/h3548.md).

<a name="1samuel_22_22"></a>1Samuel 22:22

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto ['Eḇyāṯār](../../strongs/h/h54.md), I [yada'](../../strongs/h/h3045.md) it that [yowm](../../strongs/h/h3117.md), when [Dō'Ēḡ](../../strongs/h/h1673.md) the ['Ăḏōmî](../../strongs/h/h130.md) was there, that he would [nāḡaḏ](../../strongs/h/h5046.md) [nāḡaḏ](../../strongs/h/h5046.md) [Šā'ûl](../../strongs/h/h7586.md): I have [cabab](../../strongs/h/h5437.md) the death of all the [nephesh](../../strongs/h/h5315.md) of thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md).

<a name="1samuel_22_23"></a>1Samuel 22:23

[yashab](../../strongs/h/h3427.md) thou with me, [yare'](../../strongs/h/h3372.md) not: for he that [bāqaš](../../strongs/h/h1245.md) my [nephesh](../../strongs/h/h5315.md) [bāqaš](../../strongs/h/h1245.md) thy [nephesh](../../strongs/h/h5315.md): but with me thou shalt be in [mišmereṯ](../../strongs/h/h4931.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 21](1samuel_21.md) - [1Samuel 23](1samuel_23.md)