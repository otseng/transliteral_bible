# [1Samuel 6](https://www.blueletterbible.org/kjv/1samuel/6)

<a name="1samuel_6_1"></a>1Samuel 6:1

And the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) was in the [sadeh](../../strongs/h/h7704.md) of the [Pᵊlištî](../../strongs/h/h6430.md) seven [ḥōḏeš](../../strongs/h/h2320.md).

<a name="1samuel_6_2"></a>1Samuel 6:2

And the [Pᵊlištî](../../strongs/h/h6430.md) [qara'](../../strongs/h/h7121.md) for the [kōhēn](../../strongs/h/h3548.md) and the [qāsam](../../strongs/h/h7080.md), ['āmar](../../strongs/h/h559.md), What shall we ['asah](../../strongs/h/h6213.md) to the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md)? [yada'](../../strongs/h/h3045.md) us wherewith we shall [shalach](../../strongs/h/h7971.md) it to his [maqowm](../../strongs/h/h4725.md).

<a name="1samuel_6_3"></a>1Samuel 6:3

And they ['āmar](../../strongs/h/h559.md), If ye [shalach](../../strongs/h/h7971.md) the ['ārôn](../../strongs/h/h727.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), [shalach](../../strongs/h/h7971.md) it not [rêqām](../../strongs/h/h7387.md); but in any [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) him a trespass ['āšām](../../strongs/h/h817.md): then ye shall be [rapha'](../../strongs/h/h7495.md), and it shall be [yada'](../../strongs/h/h3045.md) to you why his [yad](../../strongs/h/h3027.md) is not [cuwr](../../strongs/h/h5493.md) from you.

<a name="1samuel_6_4"></a>1Samuel 6:4

Then ['āmar](../../strongs/h/h559.md) they, What shall be the trespass ['āšām](../../strongs/h/h817.md) which we shall [shuwb](../../strongs/h/h7725.md) to him? They ['āmar](../../strongs/h/h559.md), Five [zāhāḇ](../../strongs/h/h2091.md) [ṭᵊḥôrîm](../../strongs/h/h2914.md) [ʿōp̄el](../../strongs/h/h6076.md), and five [zāhāḇ](../../strongs/h/h2091.md) [ʿaḵbār](../../strongs/h/h5909.md), according to the [mispār](../../strongs/h/h4557.md) of the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md): for one [magēp̄â](../../strongs/h/h4046.md) was on you all, and on your [seren](../../strongs/h/h5633.md).

<a name="1samuel_6_5"></a>1Samuel 6:5

Wherefore ye shall ['asah](../../strongs/h/h6213.md) [tselem](../../strongs/h/h6754.md) of your [ṭᵊḥôrîm](../../strongs/h/h2914.md) [ʿōp̄el](../../strongs/h/h6076.md), and [tselem](../../strongs/h/h6754.md) of your [ʿaḵbār](../../strongs/h/h5909.md) that [shachath](../../strongs/h/h7843.md) the ['erets](../../strongs/h/h776.md); and ye shall [nathan](../../strongs/h/h5414.md) [kabowd](../../strongs/h/h3519.md) unto the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md): peradventure he will [qālal](../../strongs/h/h7043.md) his [yad](../../strongs/h/h3027.md) from off you, and from off your ['Elohiym](../../strongs/h/h430.md), and from off your ['erets](../../strongs/h/h776.md).

<a name="1samuel_6_6"></a>1Samuel 6:6

Wherefore then do ye [kabad](../../strongs/h/h3513.md) your [lebab](../../strongs/h/h3824.md), as the [Mitsrayim](../../strongs/h/h4714.md) and [Parʿô](../../strongs/h/h6547.md) [kabad](../../strongs/h/h3513.md) their [leb](../../strongs/h/h3820.md)? when he had wrought [ʿālal](../../strongs/h/h5953.md) among them, did they not let the people [shalach](../../strongs/h/h7971.md), and they [yālaḵ](../../strongs/h/h3212.md)?

<a name="1samuel_6_7"></a>1Samuel 6:7

Now therefore ['asah](../../strongs/h/h6213.md) a [ḥāḏāš](../../strongs/h/h2319.md) [ʿăḡālâ](../../strongs/h/h5699.md), and [laqach](../../strongs/h/h3947.md) two [ʿûl](../../strongs/h/h5763.md) [pārâ](../../strongs/h/h6510.md), on which there hath [ʿālâ](../../strongs/h/h5927.md) no [ʿōl](../../strongs/h/h5923.md), and ['āsar](../../strongs/h/h631.md) the [pārâ](../../strongs/h/h6510.md) to the [ʿăḡālâ](../../strongs/h/h5699.md), and [shuwb](../../strongs/h/h7725.md) their [ben](../../strongs/h/h1121.md) [bayith](../../strongs/h/h1004.md) from ['aḥar](../../strongs/h/h310.md):

<a name="1samuel_6_8"></a>1Samuel 6:8

And [laqach](../../strongs/h/h3947.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md), and [nathan](../../strongs/h/h5414.md) it upon the [ʿăḡālâ](../../strongs/h/h5699.md); and [śûm](../../strongs/h/h7760.md) the [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md), which ye [shuwb](../../strongs/h/h7725.md) him for an ['āšām](../../strongs/h/h817.md), in an ['argāz](../../strongs/h/h712.md) by the [ṣaḏ](../../strongs/h/h6654.md) thereof; and [shalach](../../strongs/h/h7971.md) it, that it may [halak](../../strongs/h/h1980.md).

<a name="1samuel_6_9"></a>1Samuel 6:9

And [ra'ah](../../strongs/h/h7200.md), if it [ʿālâ](../../strongs/h/h5927.md) by the [derek](../../strongs/h/h1870.md) of his own [gᵊḇûl](../../strongs/h/h1366.md) to [Bêṯ Šemeš](../../strongs/h/h1053.md), then he hath ['asah](../../strongs/h/h6213.md) us this [gadowl](../../strongs/h/h1419.md) [ra'](../../strongs/h/h7451.md): but if not, then we shall [yada'](../../strongs/h/h3045.md) that it is not his [yad](../../strongs/h/h3027.md) that [naga'](../../strongs/h/h5060.md) us: it was a [miqrê](../../strongs/h/h4745.md) that happened to us.

<a name="1samuel_6_10"></a>1Samuel 6:10

And the ['enowsh](../../strongs/h/h582.md) did ['asah](../../strongs/h/h6213.md); and [laqach](../../strongs/h/h3947.md) two [ʿûl](../../strongs/h/h5763.md) [pārâ](../../strongs/h/h6510.md), and ['āsar](../../strongs/h/h631.md) them to the [ʿăḡālâ](../../strongs/h/h5699.md), and [kālā'](../../strongs/h/h3607.md) their [ben](../../strongs/h/h1121.md) at [bayith](../../strongs/h/h1004.md):

<a name="1samuel_6_11"></a>1Samuel 6:11

And they [śûm](../../strongs/h/h7760.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) upon the [ʿăḡālâ](../../strongs/h/h5699.md), and the ['argāz](../../strongs/h/h712.md) with the [ʿaḵbār](../../strongs/h/h5909.md) of [zāhāḇ](../../strongs/h/h2091.md) and the [tselem](../../strongs/h/h6754.md) of their [ṭᵊḥôrîm](../../strongs/h/h2914.md).

<a name="1samuel_6_12"></a>1Samuel 6:12

And the [pārâ](../../strongs/h/h6510.md) took the [yashar](../../strongs/h/h3474.md) [derek](../../strongs/h/h1870.md) to the [derek](../../strongs/h/h1870.md) of [Bêṯ Šemeš](../../strongs/h/h1053.md), and [halak](../../strongs/h/h1980.md) the [mĕcillah](../../strongs/h/h4546.md) , [gāʿâ](../../strongs/h/h1600.md) as they [halak](../../strongs/h/h1980.md), and [cuwr](../../strongs/h/h5493.md) not to the [yamiyn](../../strongs/h/h3225.md) or to the [śᵊmō'l](../../strongs/h/h8040.md); and the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) them unto the [gᵊḇûl](../../strongs/h/h1366.md) of [Bêṯ Šemeš](../../strongs/h/h1053.md).

<a name="1samuel_6_13"></a>1Samuel 6:13

And they of [Bêṯ Šemeš](../../strongs/h/h1053.md) were [qāṣar](../../strongs/h/h7114.md) their [ḥiṭṭâ](../../strongs/h/h2406.md) [qāṣîr](../../strongs/h/h7105.md) in the [ʿēmeq](../../strongs/h/h6010.md): and they [nasa'](../../strongs/h/h5375.md) their ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md) the ['ārôn](../../strongs/h/h727.md), and [samach](../../strongs/h/h8055.md) to [ra'ah](../../strongs/h/h7200.md) it.

<a name="1samuel_6_14"></a>1Samuel 6:14

And the [ʿăḡālâ](../../strongs/h/h5699.md) [bow'](../../strongs/h/h935.md) into the [sadeh](../../strongs/h/h7704.md) of [Yᵊhôšûaʿ](../../strongs/h/h3091.md), a [bêṯ-haššimšî](../../strongs/h/h1030.md), and ['amad](../../strongs/h/h5975.md) there, where there was a [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md): and they [bāqaʿ](../../strongs/h/h1234.md) the ['ets](../../strongs/h/h6086.md) of the [ʿăḡālâ](../../strongs/h/h5699.md), and [ʿālâ](../../strongs/h/h5927.md) the [pārâ](../../strongs/h/h6510.md) a [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_6_15"></a>1Samuel 6:15

And the [Lᵊvî](../../strongs/h/h3881.md) [yarad](../../strongs/h/h3381.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md), and the ['argāz](../../strongs/h/h712.md) that was with it, wherein the [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md) were, and [śûm](../../strongs/h/h7760.md) them on the [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md): and the ['enowsh](../../strongs/h/h582.md) of [Bêṯ Šemeš](../../strongs/h/h1053.md) [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) and [zabach](../../strongs/h/h2076.md) [zebach](../../strongs/h/h2077.md) the same [yowm](../../strongs/h/h3117.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_6_16"></a>1Samuel 6:16

And when the five [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md) had [ra'ah](../../strongs/h/h7200.md) it, they [shuwb](../../strongs/h/h7725.md) to [ʿEqrôn](../../strongs/h/h6138.md) the same [yowm](../../strongs/h/h3117.md).

<a name="1samuel_6_17"></a>1Samuel 6:17

And these are the [zāhāḇ](../../strongs/h/h2091.md) [ṭᵊḥôrîm](../../strongs/h/h2914.md) which the [Pᵊlištî](../../strongs/h/h6430.md) [shuwb](../../strongs/h/h7725.md) for an ['āšām](../../strongs/h/h817.md) unto [Yĕhovah](../../strongs/h/h3068.md); for ['Ašdôḏ](../../strongs/h/h795.md) one, for [ʿAzzâ](../../strongs/h/h5804.md) one, for ['Ašqᵊlôn](../../strongs/h/h831.md) one, for [Gaṯ](../../strongs/h/h1661.md) one, for [ʿEqrôn](../../strongs/h/h6138.md) one;

<a name="1samuel_6_18"></a>1Samuel 6:18

And the [zāhāḇ](../../strongs/h/h2091.md) [ʿaḵbār](../../strongs/h/h5909.md), the [mispār](../../strongs/h/h4557.md) of all the [ʿîr](../../strongs/h/h5892.md) of the [Pᵊlištî](../../strongs/h/h6430.md) to the five [seren](../../strongs/h/h5633.md), both of [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md), and of [pᵊrāzî](../../strongs/h/h6521.md) [kōp̄er](../../strongs/h/h3724.md), even unto the [gadowl](../../strongs/h/h1419.md) ['Āḇēl](../../strongs/h/h59.md), whereon they set [yānaḥ](../../strongs/h/h3240.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md):  unto this [yowm](../../strongs/h/h3117.md) in the [sadeh](../../strongs/h/h7704.md) of [Yᵊhôšûaʿ](../../strongs/h/h3091.md), the [bêṯ-haššimšî](../../strongs/h/h1030.md).

<a name="1samuel_6_19"></a>1Samuel 6:19

And he [nakah](../../strongs/h/h5221.md) the ['iysh](../../strongs/h/h376.md) of [Bêṯ Šemeš](../../strongs/h/h1053.md), because they had [ra'ah](../../strongs/h/h7200.md) into the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md), even he [nakah](../../strongs/h/h5221.md) of the ['am](../../strongs/h/h5971.md) fifty thousand and threescore and ten ['enowsh](../../strongs/h/h582.md): and the ['am](../../strongs/h/h5971.md) ['āḇal](../../strongs/h/h56.md), because [Yĕhovah](../../strongs/h/h3068.md) had [nakah](../../strongs/h/h5221.md) many of the ['am](../../strongs/h/h5971.md) with a [gadowl](../../strongs/h/h1419.md) [makâ](../../strongs/h/h4347.md).

<a name="1samuel_6_20"></a>1Samuel 6:20

And the ['enowsh](../../strongs/h/h582.md) of [Bêṯ Šemeš](../../strongs/h/h1053.md) ['āmar](../../strongs/h/h559.md), Who is [yakol](../../strongs/h/h3201.md) to ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) this [qadowsh](../../strongs/h/h6918.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md)? and to whom shall he [ʿālâ](../../strongs/h/h5927.md) from us?

<a name="1samuel_6_21"></a>1Samuel 6:21

And they [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to the [yashab](../../strongs/h/h3427.md) of [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md), ['āmar](../../strongs/h/h559.md), The [Pᵊlištî](../../strongs/h/h6430.md) have [shuwb](../../strongs/h/h7725.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md); come ye [yarad](../../strongs/h/h3381.md), and fetch it [ʿālâ](../../strongs/h/h5927.md) to you.

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 5](1samuel_5.md) - [1Samuel 7](1samuel_7.md)