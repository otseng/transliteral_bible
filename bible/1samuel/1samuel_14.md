# [1Samuel 14](https://www.blueletterbible.org/kjv/1samuel/14)

<a name="1samuel_14_1"></a>1Samuel 14:1

Now it came to pass upon a [yowm](../../strongs/h/h3117.md), that [Yônāṯān](../../strongs/h/h3129.md) the [ben](../../strongs/h/h1121.md) of [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto the [naʿar](../../strongs/h/h5288.md) that [nasa'](../../strongs/h/h5375.md) his [kĕliy](../../strongs/h/h3627.md), [yālaḵ](../../strongs/h/h3212.md), and let us ['abar](../../strongs/h/h5674.md) to the [Pᵊlištî](../../strongs/h/h6430.md) [maṣṣāḇ](../../strongs/h/h4673.md), that is on the other [ʿēḇer](../../strongs/h/h5676.md) [hallāz](../../strongs/h/h1975.md). But he [nāḡaḏ](../../strongs/h/h5046.md) not his ['ab](../../strongs/h/h1.md).

<a name="1samuel_14_2"></a>1Samuel 14:2

And [Šā'ûl](../../strongs/h/h7586.md) [yashab](../../strongs/h/h3427.md) in the [qāṣê](../../strongs/h/h7097.md) part of [giḇʿâ](../../strongs/h/h1390.md) under a [rimmôn](../../strongs/h/h7416.md) which is in [miḡrôn](../../strongs/h/h4051.md): and the ['am](../../strongs/h/h5971.md) that were with him were about six hundred ['iysh](../../strongs/h/h376.md);

<a name="1samuel_14_3"></a>1Samuel 14:3

And ['Ăḥîyâ](../../strongs/h/h281.md), the [ben](../../strongs/h/h1121.md) of ['Ăḥîṭûḇ](../../strongs/h/h285.md), ['Î-Ḵāḇôḏ](../../strongs/h/h350.md) ['ach](../../strongs/h/h251.md), the [ben](../../strongs/h/h1121.md) of [Pînḥās](../../strongs/h/h6372.md), the [ben](../../strongs/h/h1121.md) of [ʿĒlî](../../strongs/h/h5941.md), [Yĕhovah](../../strongs/h/h3068.md) [kōhēn](../../strongs/h/h3548.md) in [Šîlô](../../strongs/h/h7887.md), [nasa'](../../strongs/h/h5375.md) an ['ēp̄ôḏ](../../strongs/h/h646.md). And the ['am](../../strongs/h/h5971.md) [yada'](../../strongs/h/h3045.md) not that [Yônāṯān](../../strongs/h/h3129.md) was [halak](../../strongs/h/h1980.md).

<a name="1samuel_14_4"></a>1Samuel 14:4

And between the [maʿăḇār](../../strongs/h/h4569.md), by which [Yônāṯān](../../strongs/h/h3129.md) [bāqaš](../../strongs/h/h1245.md) to ['abar](../../strongs/h/h5674.md) unto the [Pᵊlištî](../../strongs/h/h6430.md) [maṣṣāḇ](../../strongs/h/h4673.md), there was a [šēn](../../strongs/h/h8127.md) [cela'](../../strongs/h/h5553.md) on the one [ʿēḇer](../../strongs/h/h5676.md), and a [šēn](../../strongs/h/h8127.md) [cela'](../../strongs/h/h5553.md) on the other [ʿēḇer](../../strongs/h/h5676.md): and the [shem](../../strongs/h/h8034.md) of the one was [Bôṣēṣ](../../strongs/h/h949.md), and the [shem](../../strongs/h/h8034.md) of the other [Senê](../../strongs/h/h5573.md).

<a name="1samuel_14_5"></a>1Samuel 14:5

The [šēn](../../strongs/h/h8127.md) of the one was [māṣûq](../../strongs/h/h4690.md) [ṣāp̄ôn](../../strongs/h/h6828.md) over [môl](../../strongs/h/h4136.md) [Miḵmās](../../strongs/h/h4363.md), and the other [neḡeḇ](../../strongs/h/h5045.md) over [môl](../../strongs/h/h4136.md) [Geḇaʿ](../../strongs/h/h1387.md).

<a name="1samuel_14_6"></a>1Samuel 14:6

And [Yᵊhônāṯān](../../strongs/h/h3083.md) ['āmar](../../strongs/h/h559.md) to the [naʿar](../../strongs/h/h5288.md) that [nasa'](../../strongs/h/h5375.md) his [kĕliy](../../strongs/h/h3627.md), [yālaḵ](../../strongs/h/h3212.md), and let us ['abar](../../strongs/h/h5674.md) unto the [maṣṣāḇ](../../strongs/h/h4673.md) of these [ʿārēl](../../strongs/h/h6189.md): it may be that [Yĕhovah](../../strongs/h/h3068.md) will ['asah](../../strongs/h/h6213.md) for us: for there is no [maʿṣôr](../../strongs/h/h4622.md) to [Yĕhovah](../../strongs/h/h3068.md) to [yasha'](../../strongs/h/h3467.md) by [rab](../../strongs/h/h7227.md) or by [mᵊʿaṭ](../../strongs/h/h4592.md).

<a name="1samuel_14_7"></a>1Samuel 14:7

And his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md) ['āmar](../../strongs/h/h559.md) unto him, ['asah](../../strongs/h/h6213.md) all that is in thine [lebab](../../strongs/h/h3824.md): [natah](../../strongs/h/h5186.md) thee; behold, I am with thee according to thy [lebab](../../strongs/h/h3824.md).

<a name="1samuel_14_8"></a>1Samuel 14:8

Then ['āmar](../../strongs/h/h559.md) [Yᵊhônāṯān](../../strongs/h/h3083.md), Behold, we will ['abar](../../strongs/h/h5674.md) unto these ['enowsh](../../strongs/h/h582.md), and we will [gālâ](../../strongs/h/h1540.md) ourselves unto them.

<a name="1samuel_14_9"></a>1Samuel 14:9

If they ['āmar](../../strongs/h/h559.md) thus unto us, [damam](../../strongs/h/h1826.md) until we [naga'](../../strongs/h/h5060.md) to you; then we will ['amad](../../strongs/h/h5975.md) in our place, and will not [ʿālâ](../../strongs/h/h5927.md) unto them.

<a name="1samuel_14_10"></a>1Samuel 14:10

But if they ['āmar](../../strongs/h/h559.md) thus, [ʿālâ](../../strongs/h/h5927.md) unto us; then we will [ʿālâ](../../strongs/h/h5927.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) them into our [yad](../../strongs/h/h3027.md): and this shall be a ['ôṯ](../../strongs/h/h226.md) unto us.

<a name="1samuel_14_11"></a>1Samuel 14:11

And both of them [gālâ](../../strongs/h/h1540.md) themselves unto the [maṣṣāḇ](../../strongs/h/h4673.md) of the [Pᵊlištî](../../strongs/h/h6430.md): and the [Pᵊlištî](../../strongs/h/h6430.md) ['āmar](../../strongs/h/h559.md), Behold, the [ʿiḇrî](../../strongs/h/h5680.md) [yāṣā'](../../strongs/h/h3318.md) out of the [ḥôr](../../strongs/h/h2356.md) where they had [chaba'](../../strongs/h/h2244.md) themselves.

<a name="1samuel_14_12"></a>1Samuel 14:12

And the ['enowsh](../../strongs/h/h582.md) of the [maṣṣāḇâ](../../strongs/h/h4675.md) ['anah](../../strongs/h/h6030.md) [Yônāṯān](../../strongs/h/h3129.md) and his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md), and ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md) to us, and we will [yada'](../../strongs/h/h3045.md) you a [dabar](../../strongs/h/h1697.md). And [Yônāṯān](../../strongs/h/h3129.md) ['āmar](../../strongs/h/h559.md) unto his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md), [ʿālâ](../../strongs/h/h5927.md) ['aḥar](../../strongs/h/h310.md) me: for [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1samuel_14_13"></a>1Samuel 14:13

And [Yônāṯān](../../strongs/h/h3129.md) [ʿālâ](../../strongs/h/h5927.md) upon his [yad](../../strongs/h/h3027.md) and upon his [regel](../../strongs/h/h7272.md), and his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md) ['aḥar](../../strongs/h/h310.md) him: and they [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) [Yônāṯān](../../strongs/h/h3129.md); and his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md) [muwth](../../strongs/h/h4191.md) ['aḥar](../../strongs/h/h310.md) him.

<a name="1samuel_14_14"></a>1Samuel 14:14

And that [ri'šôn](../../strongs/h/h7223.md) [makâ](../../strongs/h/h4347.md), which [Yônāṯān](../../strongs/h/h3129.md) and his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md) [nakah](../../strongs/h/h5221.md), was about twenty ['iysh](../../strongs/h/h376.md), within as it were an half [maʿănâ](../../strongs/h/h4618.md) of [sadeh](../../strongs/h/h7704.md), a [ṣemeḏ](../../strongs/h/h6776.md).

<a name="1samuel_14_15"></a>1Samuel 14:15

And there was [ḥărāḏâ](../../strongs/h/h2731.md) in the [maḥănê](../../strongs/h/h4264.md), in the [sadeh](../../strongs/h/h7704.md), and among all the ['am](../../strongs/h/h5971.md): the [maṣṣāḇ](../../strongs/h/h4673.md), and the [shachath](../../strongs/h/h7843.md), they also [ḥārēḏ](../../strongs/h/h2729.md), and the ['erets](../../strongs/h/h776.md) [ragaz](../../strongs/h/h7264.md): so it was a very ['Elohiym](../../strongs/h/h430.md) [ḥărāḏâ](../../strongs/h/h2731.md).

<a name="1samuel_14_16"></a>1Samuel 14:16

And the [tsaphah](../../strongs/h/h6822.md) of [Šā'ûl](../../strongs/h/h7586.md) in [giḇʿâ](../../strongs/h/h1390.md) of [Binyāmîn](../../strongs/h/h1144.md) [ra'ah](../../strongs/h/h7200.md); and, behold, the [hāmôn](../../strongs/h/h1995.md) melted [mûḡ](../../strongs/h/h4127.md), and they [yālaḵ](../../strongs/h/h3212.md) on [hālam](../../strongs/h/h1986.md) one another.

<a name="1samuel_14_17"></a>1Samuel 14:17

Then ['āmar](../../strongs/h/h559.md) [Šā'ûl](../../strongs/h/h7586.md) unto the ['am](../../strongs/h/h5971.md) that were with him, [paqad](../../strongs/h/h6485.md) now, and [ra'ah](../../strongs/h/h7200.md) who is [halak](../../strongs/h/h1980.md) from us. And when they had [paqad](../../strongs/h/h6485.md), behold, [Yônāṯān](../../strongs/h/h3129.md) and his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md) were not there.

<a name="1samuel_14_18"></a>1Samuel 14:18

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto ['Ăḥîyâ](../../strongs/h/h281.md), [nāḡaš](../../strongs/h/h5066.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md). For the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) was at that [yowm](../../strongs/h/h3117.md) with the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1samuel_14_19"></a>1Samuel 14:19

And it came to pass, while [Šā'ûl](../../strongs/h/h7586.md) [dabar](../../strongs/h/h1696.md) unto the [kōhēn](../../strongs/h/h3548.md), that the [hāmôn](../../strongs/h/h1995.md) that was in the [maḥănê](../../strongs/h/h4264.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [yālaḵ](../../strongs/h/h3212.md) [halak](../../strongs/h/h1980.md) and [rab](../../strongs/h/h7227.md): and [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto the [kōhēn](../../strongs/h/h3548.md), ['āsap̄](../../strongs/h/h622.md) thine [yad](../../strongs/h/h3027.md).

<a name="1samuel_14_20"></a>1Samuel 14:20

And [Šā'ûl](../../strongs/h/h7586.md) and all the ['am](../../strongs/h/h5971.md) that were with him [zāʿaq](../../strongs/h/h2199.md) themselves, and they [bow'](../../strongs/h/h935.md) to the [milḥāmâ](../../strongs/h/h4421.md): and, behold, every ['iysh](../../strongs/h/h376.md) [chereb](../../strongs/h/h2719.md) was against his [rea'](../../strongs/h/h7453.md), and there was a [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) [mᵊhûmâ](../../strongs/h/h4103.md).

<a name="1samuel_14_21"></a>1Samuel 14:21

Moreover the [ʿiḇrî](../../strongs/h/h5680.md) that were with the [Pᵊlištî](../../strongs/h/h6430.md) ['eṯmôl](../../strongs/h/h865.md) that [šilšôm](../../strongs/h/h8032.md), which [ʿālâ](../../strongs/h/h5927.md) with them into the [maḥănê](../../strongs/h/h4264.md) from the country [cabiyb](../../strongs/h/h5439.md), even they also turned to be with the [Yisra'el](../../strongs/h/h3478.md) that were with [Šā'ûl](../../strongs/h/h7586.md) and [Yônāṯān](../../strongs/h/h3129.md).

<a name="1samuel_14_22"></a>1Samuel 14:22

Likewise all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) which had [chaba'](../../strongs/h/h2244.md) themselves in [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), when they [shama'](../../strongs/h/h8085.md) that the [Pᵊlištî](../../strongs/h/h6430.md) [nûs](../../strongs/h/h5127.md), even they also followed [dāḇaq](../../strongs/h/h1692.md) ['aḥar](../../strongs/h/h310.md) them in the [milḥāmâ](../../strongs/h/h4421.md).

<a name="1samuel_14_23"></a>1Samuel 14:23

So [Yĕhovah](../../strongs/h/h3068.md) [yasha'](../../strongs/h/h3467.md) [Yisra'el](../../strongs/h/h3478.md) that [yowm](../../strongs/h/h3117.md): and the [milḥāmâ](../../strongs/h/h4421.md) ['abar](../../strongs/h/h5674.md) unto [Bêṯ 'Āven](../../strongs/h/h1007.md).

<a name="1samuel_14_24"></a>1Samuel 14:24

And the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) were [nāḡaś](../../strongs/h/h5065.md) that [yowm](../../strongs/h/h3117.md): for [Šā'ûl](../../strongs/h/h7586.md) had ['ālâ](../../strongs/h/h422.md) the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), ['arar](../../strongs/h/h779.md) be the ['iysh](../../strongs/h/h376.md) that ['akal](../../strongs/h/h398.md) any [lechem](../../strongs/h/h3899.md) until ['ereb](../../strongs/h/h6153.md), that I may be [naqam](../../strongs/h/h5358.md) on mine ['oyeb](../../strongs/h/h341.md). So none of the ['am](../../strongs/h/h5971.md) [ṭāʿam](../../strongs/h/h2938.md) any [lechem](../../strongs/h/h3899.md).

<a name="1samuel_14_25"></a>1Samuel 14:25

And all they of the ['erets](../../strongs/h/h776.md) [bow'](../../strongs/h/h935.md) to a [yaʿar](../../strongs/h/h3293.md); and there was [dĕbash](../../strongs/h/h1706.md) [paniym](../../strongs/h/h6440.md) the [sadeh](../../strongs/h/h7704.md).

<a name="1samuel_14_26"></a>1Samuel 14:26

And when the ['am](../../strongs/h/h5971.md) were [bow'](../../strongs/h/h935.md) into the [yaʿar](../../strongs/h/h3293.md), behold, the [dĕbash](../../strongs/h/h1706.md) [hēleḵ](../../strongs/h/h1982.md); but no man [nāśaḡ](../../strongs/h/h5381.md) his [yad](../../strongs/h/h3027.md) to his [peh](../../strongs/h/h6310.md): for the ['am](../../strongs/h/h5971.md) [yare'](../../strongs/h/h3372.md) the [šᵊḇûʿâ](../../strongs/h/h7621.md).

<a name="1samuel_14_27"></a>1Samuel 14:27

But [Yônāṯān](../../strongs/h/h3129.md) [shama'](../../strongs/h/h8085.md) not when his ['ab](../../strongs/h/h1.md) [shaba'](../../strongs/h/h7650.md) the ['am](../../strongs/h/h5971.md) with the [shaba'](../../strongs/h/h7650.md): wherefore he [shalach](../../strongs/h/h7971.md) the [qāṣê](../../strongs/h/h7097.md) of the [maṭṭê](../../strongs/h/h4294.md) that was in his [yad](../../strongs/h/h3027.md), and [ṭāḇal](../../strongs/h/h2881.md) it in a [yaʿărâ](../../strongs/h/h3295.md) [dĕbash](../../strongs/h/h1706.md), and [shuwb](../../strongs/h/h7725.md) his [yad](../../strongs/h/h3027.md) to his [peh](../../strongs/h/h6310.md); and his ['ayin](../../strongs/h/h5869.md) were ['owr](../../strongs/h/h215.md).

<a name="1samuel_14_28"></a>1Samuel 14:28

Then ['anah](../../strongs/h/h6030.md) ['iysh](../../strongs/h/h376.md) of the ['am](../../strongs/h/h5971.md), and ['āmar](../../strongs/h/h559.md), Thy ['ab](../../strongs/h/h1.md) [shaba'](../../strongs/h/h7650.md) [shaba'](../../strongs/h/h7650.md) the ['am](../../strongs/h/h5971.md) with a [shaba'](../../strongs/h/h7650.md), ['āmar](../../strongs/h/h559.md), ['arar](../../strongs/h/h779.md) be the ['iysh](../../strongs/h/h376.md) that ['akal](../../strongs/h/h398.md) any [lechem](../../strongs/h/h3899.md) this [yowm](../../strongs/h/h3117.md). And the ['am](../../strongs/h/h5971.md) were ['uwph](../../strongs/h/h5774.md).

<a name="1samuel_14_29"></a>1Samuel 14:29

Then ['āmar](../../strongs/h/h559.md) [Yônāṯān](../../strongs/h/h3129.md), My ['ab](../../strongs/h/h1.md) hath [ʿāḵar](../../strongs/h/h5916.md) the ['erets](../../strongs/h/h776.md): [ra'ah](../../strongs/h/h7200.md), I pray you, how mine ['ayin](../../strongs/h/h5869.md) have been ['owr](../../strongs/h/h215.md), because I [ṭāʿam](../../strongs/h/h2938.md) a [mᵊʿaṭ](../../strongs/h/h4592.md) of this [dĕbash](../../strongs/h/h1706.md).

<a name="1samuel_14_30"></a>1Samuel 14:30

How much more, if haply the ['am](../../strongs/h/h5971.md) had ['akal](../../strongs/h/h398.md) ['akal](../../strongs/h/h398.md) to [yowm](../../strongs/h/h3117.md) of the [šālāl](../../strongs/h/h7998.md) of their ['oyeb](../../strongs/h/h341.md) which they [māṣā'](../../strongs/h/h4672.md)? for had there not been now a much [rabah](../../strongs/h/h7235.md) [makâ](../../strongs/h/h4347.md) among the [Pᵊlištî](../../strongs/h/h6430.md)?

<a name="1samuel_14_31"></a>1Samuel 14:31

And they [nakah](../../strongs/h/h5221.md) the [Pᵊlištî](../../strongs/h/h6430.md) that [yowm](../../strongs/h/h3117.md) from [Miḵmās](../../strongs/h/h4363.md) to ['Ayyālôn](../../strongs/h/h357.md): and the ['am](../../strongs/h/h5971.md) were [me'od](../../strongs/h/h3966.md) ['uwph](../../strongs/h/h5774.md).

<a name="1samuel_14_32"></a>1Samuel 14:32

And the ['am](../../strongs/h/h5971.md) [ʿîṭ](../../strongs/h/h5860.md) ['asah](../../strongs/h/h6213.md) upon the [šālāl](../../strongs/h/h7998.md), and [laqach](../../strongs/h/h3947.md) [tso'n](../../strongs/h/h6629.md), and [bāqār](../../strongs/h/h1241.md), and [ben](../../strongs/h/h1121.md), and [šāḥaṭ](../../strongs/h/h7819.md) them on the ['erets](../../strongs/h/h776.md): and the ['am](../../strongs/h/h5971.md) did ['akal](../../strongs/h/h398.md) them with the [dam](../../strongs/h/h1818.md).

<a name="1samuel_14_33"></a>1Samuel 14:33

Then they [nāḡaḏ](../../strongs/h/h5046.md) [Šā'ûl](../../strongs/h/h7586.md), ['āmar](../../strongs/h/h559.md), Behold, the ['am](../../strongs/h/h5971.md) [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md), in that they ['akal](../../strongs/h/h398.md) with the [dam](../../strongs/h/h1818.md). And he ['āmar](../../strongs/h/h559.md), Ye have [bāḡaḏ](../../strongs/h/h898.md): [gālal](../../strongs/h/h1556.md) a [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md) unto me this [yowm](../../strongs/h/h3117.md).

<a name="1samuel_14_34"></a>1Samuel 14:34

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), [puwts](../../strongs/h/h6327.md) yourselves among the ['am](../../strongs/h/h5971.md), and ['āmar](../../strongs/h/h559.md) unto them, Bring me [nāḡaš](../../strongs/h/h5066.md) every ['iysh](../../strongs/h/h376.md) his [showr](../../strongs/h/h7794.md), and every ['iysh](../../strongs/h/h376.md) his [śê](../../strongs/h/h7716.md), and [šāḥaṭ](../../strongs/h/h7819.md) them here, and ['akal](../../strongs/h/h398.md); and [chata'](../../strongs/h/h2398.md) not against [Yĕhovah](../../strongs/h/h3068.md) in ['akal](../../strongs/h/h398.md) with the [dam](../../strongs/h/h1818.md). And all the ['am](../../strongs/h/h5971.md) [nāḡaš](../../strongs/h/h5066.md) every ['iysh](../../strongs/h/h376.md) his [showr](../../strongs/h/h7794.md) with [yad](../../strongs/h/h3027.md) that [layil](../../strongs/h/h3915.md), and [šāḥaṭ](../../strongs/h/h7819.md) them there.

<a name="1samuel_14_35"></a>1Samuel 14:35

And [Šā'ûl](../../strongs/h/h7586.md) [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md): the same was the [ḥālal](../../strongs/h/h2490.md) [mizbeach](../../strongs/h/h4196.md) that he [bānâ](../../strongs/h/h1129.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_14_36"></a>1Samuel 14:36

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), Let us [yarad](../../strongs/h/h3381.md) ['aḥar](../../strongs/h/h310.md) the [Pᵊlištî](../../strongs/h/h6430.md) by [layil](../../strongs/h/h3915.md), and [bāzaz](../../strongs/h/h962.md) them until the [boqer](../../strongs/h/h1242.md) ['owr](../../strongs/h/h216.md), and let us not [šā'ar](../../strongs/h/h7604.md) an ['iysh](../../strongs/h/h376.md) of them. And they ['āmar](../../strongs/h/h559.md), ['asah](../../strongs/h/h6213.md) whatsoever ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md) unto thee. Then ['āmar](../../strongs/h/h559.md) the [kōhēn](../../strongs/h/h3548.md), Let us [qāraḇ](../../strongs/h/h7126.md) hither unto ['Elohiym](../../strongs/h/h430.md).

<a name="1samuel_14_37"></a>1Samuel 14:37

And [Šā'ûl](../../strongs/h/h7586.md) [sha'al](../../strongs/h/h7592.md) counsel of ['Elohiym](../../strongs/h/h430.md), Shall I [yarad](../../strongs/h/h3381.md) ['aḥar](../../strongs/h/h310.md) the [Pᵊlištî](../../strongs/h/h6430.md)? wilt thou [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of [Yisra'el](../../strongs/h/h3478.md)? But he ['anah](../../strongs/h/h6030.md) him not that [yowm](../../strongs/h/h3117.md).

<a name="1samuel_14_38"></a>1Samuel 14:38

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), Draw ye [nāḡaš](../../strongs/h/h5066.md) hither, all the [pinnâ](../../strongs/h/h6438.md) of the ['am](../../strongs/h/h5971.md): and [yada'](../../strongs/h/h3045.md) and [ra'ah](../../strongs/h/h7200.md) wherein this [chatta'ath](../../strongs/h/h2403.md) hath been this [yowm](../../strongs/h/h3117.md).

<a name="1samuel_14_39"></a>1Samuel 14:39

For, as [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), which [yasha'](../../strongs/h/h3467.md) [Yisra'el](../../strongs/h/h3478.md), though it be in [Yônāṯān](../../strongs/h/h3129.md) my [ben](../../strongs/h/h1121.md), he shall [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md). But there was not a man among all the ['am](../../strongs/h/h5971.md) that ['anah](../../strongs/h/h6030.md) him.

<a name="1samuel_14_40"></a>1Samuel 14:40

Then ['āmar](../../strongs/h/h559.md) he unto all [Yisra'el](../../strongs/h/h3478.md), Be ye on one [ʿēḇer](../../strongs/h/h5676.md), and I and [Yônāṯān](../../strongs/h/h3129.md) my [ben](../../strongs/h/h1121.md) will be on the other [ʿēḇer](../../strongs/h/h5676.md). And the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md) unto [Šā'ûl](../../strongs/h/h7586.md), ['asah](../../strongs/h/h6213.md) what ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md) unto thee.

<a name="1samuel_14_41"></a>1Samuel 14:41

Therefore [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), [yāhaḇ](../../strongs/h/h3051.md) a [tamiym](../../strongs/h/h8549.md) lot. And [Šā'ûl](../../strongs/h/h7586.md) and [Yônāṯān](../../strongs/h/h3129.md) were [lāḵaḏ](../../strongs/h/h3920.md): but the ['am](../../strongs/h/h5971.md) [yāṣā'](../../strongs/h/h3318.md).

<a name="1samuel_14_42"></a>1Samuel 14:42

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), [naphal](../../strongs/h/h5307.md) between me and [Yônāṯān](../../strongs/h/h3129.md) my [ben](../../strongs/h/h1121.md). And [Yônāṯān](../../strongs/h/h3129.md) was [lāḵaḏ](../../strongs/h/h3920.md).

<a name="1samuel_14_43"></a>1Samuel 14:43

Then [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) to [Yônāṯān](../../strongs/h/h3129.md), [nāḡaḏ](../../strongs/h/h5046.md) me what thou hast ['asah](../../strongs/h/h6213.md). And [Yônāṯān](../../strongs/h/h3129.md) [nāḡaḏ](../../strongs/h/h5046.md) him, and ['āmar](../../strongs/h/h559.md), I did [ṭāʿam](../../strongs/h/h2938.md) [ṭāʿam](../../strongs/h/h2938.md) a [mᵊʿaṭ](../../strongs/h/h4592.md) [dĕbash](../../strongs/h/h1706.md) with the [qāṣê](../../strongs/h/h7097.md) of the [maṭṭê](../../strongs/h/h4294.md) that was in mine [yad](../../strongs/h/h3027.md), and, [hinneh](../../strongs/h/h2009.md), I must [muwth](../../strongs/h/h4191.md).

<a name="1samuel_14_44"></a>1Samuel 14:44

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) do ['asah](../../strongs/h/h6213.md) and more also: for thou shalt [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md), [Yônāṯān](../../strongs/h/h3129.md).

<a name="1samuel_14_45"></a>1Samuel 14:45

And the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md) unto [Šā'ûl](../../strongs/h/h7586.md), Shall [Yônāṯān](../../strongs/h/h3129.md) [muwth](../../strongs/h/h4191.md), who hath ['asah](../../strongs/h/h6213.md) this [gadowl](../../strongs/h/h1419.md) [yĕshuw'ah](../../strongs/h/h3444.md) in [Yisra'el](../../strongs/h/h3478.md)? [ḥālîlâ](../../strongs/h/h2486.md): as [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), there shall not one [śaʿărâ](../../strongs/h/h8185.md) of his [ro'sh](../../strongs/h/h7218.md) [naphal](../../strongs/h/h5307.md) to the ['erets](../../strongs/h/h776.md); for he hath ['asah](../../strongs/h/h6213.md) with ['Elohiym](../../strongs/h/h430.md) this [yowm](../../strongs/h/h3117.md). So the ['am](../../strongs/h/h5971.md) [pāḏâ](../../strongs/h/h6299.md) [Yônāṯān](../../strongs/h/h3129.md), that he [muwth](../../strongs/h/h4191.md) not.

<a name="1samuel_14_46"></a>1Samuel 14:46

Then [Šā'ûl](../../strongs/h/h7586.md) [ʿālâ](../../strongs/h/h5927.md) from ['aḥar](../../strongs/h/h310.md) the [Pᵊlištî](../../strongs/h/h6430.md): and the [Pᵊlištî](../../strongs/h/h6430.md) [halak](../../strongs/h/h1980.md) to their own [maqowm](../../strongs/h/h4725.md).

<a name="1samuel_14_47"></a>1Samuel 14:47

So [Šā'ûl](../../strongs/h/h7586.md) [lāḵaḏ](../../strongs/h/h3920.md) the [mᵊlûḵâ](../../strongs/h/h4410.md) over [Yisra'el](../../strongs/h/h3478.md), and [lāḥam](../../strongs/h/h3898.md) against all his ['oyeb](../../strongs/h/h341.md) [cabiyb](../../strongs/h/h5439.md), against [Mô'āḇ](../../strongs/h/h4124.md), and against the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and against ['Ĕḏōm](../../strongs/h/h123.md), and against the [melek](../../strongs/h/h4428.md) of [Ṣôḇā'](../../strongs/h/h6678.md), and against the [Pᵊlištî](../../strongs/h/h6430.md): and whithersoever he [panah](../../strongs/h/h6437.md) himself, he [rāšaʿ](../../strongs/h/h7561.md) them.

<a name="1samuel_14_48"></a>1Samuel 14:48

And he ['asah](../../strongs/h/h6213.md) an [ḥayil](../../strongs/h/h2428.md), and [nakah](../../strongs/h/h5221.md) the [ʿĂmālēq](../../strongs/h/h6002.md), and [natsal](../../strongs/h/h5337.md) [Yisra'el](../../strongs/h/h3478.md) out of the [yad](../../strongs/h/h3027.md) of them that [šāsâ](../../strongs/h/h8154.md) them.

<a name="1samuel_14_49"></a>1Samuel 14:49

Now the [ben](../../strongs/h/h1121.md) of [Šā'ûl](../../strongs/h/h7586.md) were [Yônāṯān](../../strongs/h/h3129.md), and [Yišvî](../../strongs/h/h3440.md), and [Malkîšûaʿ](../../strongs/h/h4444.md): and the [shem](../../strongs/h/h8034.md) of his two [bath](../../strongs/h/h1323.md) were these; the [shem](../../strongs/h/h8034.md) of the [bᵊḵîrâ](../../strongs/h/h1067.md) [Mēraḇ](../../strongs/h/h4764.md), and the [shem](../../strongs/h/h8034.md) of the [qāṭān](../../strongs/h/h6996.md) [Mîḵāl](../../strongs/h/h4324.md):

<a name="1samuel_14_50"></a>1Samuel 14:50

And the [shem](../../strongs/h/h8034.md) of [Šā'ûl](../../strongs/h/h7586.md) ['ishshah](../../strongs/h/h802.md) was ['ĂḥînōʿAm](../../strongs/h/h293.md), the [bath](../../strongs/h/h1323.md) of ['ĂḥîmaʿAṣ](../../strongs/h/h290.md): and the [shem](../../strongs/h/h8034.md) of the [śar](../../strongs/h/h8269.md) of his [tsaba'](../../strongs/h/h6635.md) was ['Aḇnēr](../../strongs/h/h74.md), the [ben](../../strongs/h/h1121.md) of [Nēr](../../strongs/h/h5369.md), [Šā'ûl](../../strongs/h/h7586.md) [dôḏ](../../strongs/h/h1730.md).

<a name="1samuel_14_51"></a>1Samuel 14:51

And [Qîš](../../strongs/h/h7027.md) was the ['ab](../../strongs/h/h1.md) of [Šā'ûl](../../strongs/h/h7586.md); and [Nēr](../../strongs/h/h5369.md) the ['ab](../../strongs/h/h1.md) of ['Aḇnēr](../../strongs/h/h74.md) was the [ben](../../strongs/h/h1121.md) of ['Ăḇî'Ēl](../../strongs/h/h22.md).

<a name="1samuel_14_52"></a>1Samuel 14:52

And there was [ḥāzāq](../../strongs/h/h2389.md) [milḥāmâ](../../strongs/h/h4421.md) against the [Pᵊlištî](../../strongs/h/h6430.md) all the [yowm](../../strongs/h/h3117.md) of [Šā'ûl](../../strongs/h/h7586.md): and when [Šā'ûl](../../strongs/h/h7586.md) [ra'ah](../../strongs/h/h7200.md) any [gibôr](../../strongs/h/h1368.md) ['iysh](../../strongs/h/h376.md), or any [ḥayil](../../strongs/h/h2428.md) [ben](../../strongs/h/h1121.md), he ['āsap̄](../../strongs/h/h622.md) him unto him.

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 13](1samuel_13.md) - [1Samuel 15](1samuel_15.md)