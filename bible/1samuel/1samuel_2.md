# [1Samuel 2](https://www.blueletterbible.org/kjv/1samuel/2)

<a name="1samuel_2_1"></a>1Samuel 2:1

And [Ḥannâ](../../strongs/h/h2584.md) [palal](../../strongs/h/h6419.md), and ['āmar](../../strongs/h/h559.md), My [leb](../../strongs/h/h3820.md) ['alats](../../strongs/h/h5970.md) in [Yĕhovah](../../strongs/h/h3068.md), mine [qeren](../../strongs/h/h7161.md) is [ruwm](../../strongs/h/h7311.md) in [Yĕhovah](../../strongs/h/h3068.md): my [peh](../../strongs/h/h6310.md) is [rāḥaḇ](../../strongs/h/h7337.md) over mine ['oyeb](../../strongs/h/h341.md); because I [samach](../../strongs/h/h8055.md) in thy [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="1samuel_2_2"></a>1Samuel 2:2

There is none [qadowsh](../../strongs/h/h6918.md) as [Yĕhovah](../../strongs/h/h3068.md): for there is none beside thee: neither is there any [tsuwr](../../strongs/h/h6697.md) like our ['Elohiym](../../strongs/h/h430.md).

<a name="1samuel_2_3"></a>1Samuel 2:3

[dabar](../../strongs/h/h1696.md) no [rabah](../../strongs/h/h7235.md) so [gāḇōha](../../strongs/h/h1364.md) [gāḇōha](../../strongs/h/h1364.md); let not [ʿāṯāq](../../strongs/h/h6277.md) [yāṣā'](../../strongs/h/h3318.md) out of your [peh](../../strongs/h/h6310.md): for [Yĕhovah](../../strongs/h/h3068.md) is an ['el](../../strongs/h/h410.md) of [dēʿâ](../../strongs/h/h1844.md), and by him ['aliylah](../../strongs/h/h5949.md) are [tāḵan](../../strongs/h/h8505.md).

<a name="1samuel_2_4"></a>1Samuel 2:4

The [qesheth](../../strongs/h/h7198.md) of the [gibôr](../../strongs/h/h1368.md) are [ḥaṯ](../../strongs/h/h2844.md), and they that [kashal](../../strongs/h/h3782.md) are ['āzar](../../strongs/h/h247.md) with [ḥayil](../../strongs/h/h2428.md).

<a name="1samuel_2_5"></a>1Samuel 2:5

They that were [śāḇēaʿ](../../strongs/h/h7649.md) have [śāḵar](../../strongs/h/h7936.md) themselves for [lechem](../../strongs/h/h3899.md); and they that were [rāʿēḇ](../../strongs/h/h7457.md) [ḥāḏal](../../strongs/h/h2308.md): so that the [ʿāqār](../../strongs/h/h6135.md) hath [yalad](../../strongs/h/h3205.md) seven; and she that hath [rab](../../strongs/h/h7227.md) [ben](../../strongs/h/h1121.md) is waxed ['āmal](../../strongs/h/h535.md).

<a name="1samuel_2_6"></a>1Samuel 2:6

[Yĕhovah](../../strongs/h/h3068.md) [muwth](../../strongs/h/h4191.md), and [ḥāyâ](../../strongs/h/h2421.md): he [yarad](../../strongs/h/h3381.md) to the [shĕ'owl](../../strongs/h/h7585.md), and bringeth [ʿālâ](../../strongs/h/h5927.md).

<a name="1samuel_2_7"></a>1Samuel 2:7

[Yĕhovah](../../strongs/h/h3068.md) [yarash](../../strongs/h/h3423.md), and [ʿāšar](../../strongs/h/h6238.md): he [šāp̄ēl](../../strongs/h/h8213.md), and [ruwm](../../strongs/h/h7311.md).

<a name="1samuel_2_8"></a>1Samuel 2:8

He [quwm](../../strongs/h/h6965.md) the [dal](../../strongs/h/h1800.md) out of the ['aphar](../../strongs/h/h6083.md), and lifteth [ruwm](../../strongs/h/h7311.md) the ['ebyown](../../strongs/h/h34.md) from the ['ašpōṯ](../../strongs/h/h830.md), to [yashab](../../strongs/h/h3427.md) them among [nāḏîḇ](../../strongs/h/h5081.md), and to make them [nāḥal](../../strongs/h/h5157.md) the [kicce'](../../strongs/h/h3678.md) of [kabowd](../../strongs/h/h3519.md): for the [māṣûq](../../strongs/h/h4690.md) of the ['erets](../../strongs/h/h776.md) are [Yĕhovah](../../strongs/h/h3068.md), and he hath [shiyth](../../strongs/h/h7896.md) the [tebel](../../strongs/h/h8398.md) upon them.

<a name="1samuel_2_9"></a>1Samuel 2:9

He will [shamar](../../strongs/h/h8104.md) the [regel](../../strongs/h/h7272.md) of his [chaciyd](../../strongs/h/h2623.md), and the [rasha'](../../strongs/h/h7563.md) shall be [damam](../../strongs/h/h1826.md) in [choshek](../../strongs/h/h2822.md); for by [koach](../../strongs/h/h3581.md) shall no ['iysh](../../strongs/h/h376.md) [gabar](../../strongs/h/h1396.md).

<a name="1samuel_2_10"></a>1Samuel 2:10

The [riyb](../../strongs/h/h7378.md) of [Yĕhovah](../../strongs/h/h3068.md) shall be broken to [ḥāṯaṯ](../../strongs/h/h2865.md); out of [shamayim](../../strongs/h/h8064.md) shall he [ra'am](../../strongs/h/h7481.md) upon them: [Yĕhovah](../../strongs/h/h3068.md) shall [diyn](../../strongs/h/h1777.md) the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md); and he shall [nathan](../../strongs/h/h5414.md) ['oz](../../strongs/h/h5797.md) unto his [melek](../../strongs/h/h4428.md), and [ruwm](../../strongs/h/h7311.md) the [qeren](../../strongs/h/h7161.md) of his [mashiyach](../../strongs/h/h4899.md).

<a name="1samuel_2_11"></a>1Samuel 2:11

And ['Elqānâ](../../strongs/h/h511.md) [yālaḵ](../../strongs/h/h3212.md) to [rāmâ](../../strongs/h/h7414.md) to his [bayith](../../strongs/h/h1004.md). And the [naʿar](../../strongs/h/h5288.md) did [sharath](../../strongs/h/h8334.md) unto [Yĕhovah](../../strongs/h/h3068.md) [paniym](../../strongs/h/h6440.md) [ʿĒlî](../../strongs/h/h5941.md) the [kōhēn](../../strongs/h/h3548.md).

<a name="1samuel_2_12"></a>1Samuel 2:12

Now the [ben](../../strongs/h/h1121.md) of [ʿĒlî](../../strongs/h/h5941.md) were [ben](../../strongs/h/h1121.md) of [beliya'al](../../strongs/h/h1100.md); they [yada'](../../strongs/h/h3045.md) not [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_2_13"></a>1Samuel 2:13

And the [kōhēn](../../strongs/h/h3548.md) [mishpat](../../strongs/h/h4941.md) with the ['am](../../strongs/h/h5971.md) was, that, when any ['iysh](../../strongs/h/h376.md) [zabach](../../strongs/h/h2076.md) [zebach](../../strongs/h/h2077.md), the [kōhēn](../../strongs/h/h3548.md) [naʿar](../../strongs/h/h5288.md) [bow'](../../strongs/h/h935.md), while the [basar](../../strongs/h/h1320.md) was in [bāšal](../../strongs/h/h1310.md), with a [mazlēḡ](../../strongs/h/h4207.md) of three [šēn](../../strongs/h/h8127.md) in his [yad](../../strongs/h/h3027.md);

<a name="1samuel_2_14"></a>1Samuel 2:14

And he [nakah](../../strongs/h/h5221.md) it into the [kîyôr](../../strongs/h/h3595.md), or [dûḏ](../../strongs/h/h1731.md), or [qallaḥaṯ](../../strongs/h/h7037.md), or [pārûr](../../strongs/h/h6517.md); all that the [mazlēḡ](../../strongs/h/h4207.md) [ʿālâ](../../strongs/h/h5927.md) the [kōhēn](../../strongs/h/h3548.md) [laqach](../../strongs/h/h3947.md) for himself. So they ['asah](../../strongs/h/h6213.md) in [Šîlô](../../strongs/h/h7887.md) unto all the [Yisra'el](../../strongs/h/h3478.md) that [bow'](../../strongs/h/h935.md) thither.

<a name="1samuel_2_15"></a>1Samuel 2:15

Also before they [qāṭar](../../strongs/h/h6999.md) the [cheleb](../../strongs/h/h2459.md), the [kōhēn](../../strongs/h/h3548.md) [naʿar](../../strongs/h/h5288.md) [bow'](../../strongs/h/h935.md), and ['āmar](../../strongs/h/h559.md) to the ['iysh](../../strongs/h/h376.md) that [zabach](../../strongs/h/h2076.md), [nathan](../../strongs/h/h5414.md) [basar](../../strongs/h/h1320.md) to [ṣālâ](../../strongs/h/h6740.md) for the [kōhēn](../../strongs/h/h3548.md); for he will not [laqach](../../strongs/h/h3947.md) [bāšal](../../strongs/h/h1310.md) [basar](../../strongs/h/h1320.md) of thee, but [chay](../../strongs/h/h2416.md).

<a name="1samuel_2_16"></a>1Samuel 2:16

And if any ['iysh](../../strongs/h/h376.md) ['āmar](../../strongs/h/h559.md) unto him, Let them not [qāṭar](../../strongs/h/h6999.md) to [qāṭar](../../strongs/h/h6999.md) the [cheleb](../../strongs/h/h2459.md) [yowm](../../strongs/h/h3117.md), and then [laqach](../../strongs/h/h3947.md) as much as thy [nephesh](../../strongs/h/h5315.md) ['āvâ](../../strongs/h/h183.md); then he would ['āmar](../../strongs/h/h559.md) him, Nay; but thou shalt [nathan](../../strongs/h/h5414.md) it me now: and if not, I will [laqach](../../strongs/h/h3947.md) it by [ḥāzqâ](../../strongs/h/h2394.md).

<a name="1samuel_2_17"></a>1Samuel 2:17

Wherefore the [chatta'ath](../../strongs/h/h2403.md) of the [naʿar](../../strongs/h/h5288.md) was [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): for ['enowsh](../../strongs/h/h582.md) [na'ats](../../strongs/h/h5006.md) the [minchah](../../strongs/h/h4503.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_2_18"></a>1Samuel 2:18

But [Šᵊmû'Ēl](../../strongs/h/h8050.md) [sharath](../../strongs/h/h8334.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), being a [naʿar](../../strongs/h/h5288.md), [ḥāḡar](../../strongs/h/h2296.md) with a [baḏ](../../strongs/h/h906.md) ['ēp̄ôḏ](../../strongs/h/h646.md).

<a name="1samuel_2_19"></a>1Samuel 2:19

Moreover his ['em](../../strongs/h/h517.md) ['asah](../../strongs/h/h6213.md) him a [qāṭān](../../strongs/h/h6996.md) [mᵊʿîl](../../strongs/h/h4598.md), and [ʿālâ](../../strongs/h/h5927.md) it to him from [yowm](../../strongs/h/h3117.md) to [yowm](../../strongs/h/h3117.md), when she [ʿālâ](../../strongs/h/h5927.md) with her ['iysh](../../strongs/h/h376.md) to [zabach](../../strongs/h/h2076.md) the [yowm](../../strongs/h/h3117.md) [zebach](../../strongs/h/h2077.md).

<a name="1samuel_2_20"></a>1Samuel 2:20

And [ʿĒlî](../../strongs/h/h5941.md) [barak](../../strongs/h/h1288.md) ['Elqānâ](../../strongs/h/h511.md) and his ['ishshah](../../strongs/h/h802.md), and ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [śûm](../../strongs/h/h7760.md) thee [zera'](../../strongs/h/h2233.md) of this ['ishshah](../../strongs/h/h802.md) for the [šᵊ'ēlâ](../../strongs/h/h7596.md) which is [sha'al](../../strongs/h/h7592.md) to [Yĕhovah](../../strongs/h/h3068.md). And they [halak](../../strongs/h/h1980.md) unto their own [maqowm](../../strongs/h/h4725.md).

<a name="1samuel_2_21"></a>1Samuel 2:21

And [Yĕhovah](../../strongs/h/h3068.md) [paqad](../../strongs/h/h6485.md) [Ḥannâ](../../strongs/h/h2584.md), so that she [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) three [ben](../../strongs/h/h1121.md) and two [bath](../../strongs/h/h1323.md). And the [naʿar](../../strongs/h/h5288.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md) [gāḏal](../../strongs/h/h1431.md) before [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_2_22"></a>1Samuel 2:22

Now [ʿĒlî](../../strongs/h/h5941.md) was [me'od](../../strongs/h/h3966.md) [zāqēn](../../strongs/h/h2204.md), and [shama'](../../strongs/h/h8085.md) all that his [ben](../../strongs/h/h1121.md) ['asah](../../strongs/h/h6213.md) unto all [Yisra'el](../../strongs/h/h3478.md); and how they [shakab](../../strongs/h/h7901.md) with the ['ishshah](../../strongs/h/h802.md) that [ṣᵊḇā'](../../strongs/h/h6633.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="1samuel_2_23"></a>1Samuel 2:23

And he ['āmar](../../strongs/h/h559.md) unto them, Why ['asah](../../strongs/h/h6213.md) ye such [dabar](../../strongs/h/h1697.md)? for I [shama'](../../strongs/h/h8085.md) of your [ra'](../../strongs/h/h7451.md) [dabar](../../strongs/h/h1697.md) by all this ['am](../../strongs/h/h5971.md).

<a name="1samuel_2_24"></a>1Samuel 2:24

Nay, my [ben](../../strongs/h/h1121.md); for it is no [towb](../../strongs/h/h2896.md) [šᵊmûʿâ](../../strongs/h/h8052.md) that I [shama'](../../strongs/h/h8085.md): ye make [Yĕhovah](../../strongs/h/h3068.md) ['am](../../strongs/h/h5971.md) to ['abar](../../strongs/h/h5674.md).

<a name="1samuel_2_25"></a>1Samuel 2:25

If one ['iysh](../../strongs/h/h376.md) [chata'](../../strongs/h/h2398.md) against ['iysh](../../strongs/h/h376.md), the ['Elohiym](../../strongs/h/h430.md) shall [palal](../../strongs/h/h6419.md) him: but if an ['iysh](../../strongs/h/h376.md) [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md), who shall [palal](../../strongs/h/h6419.md) for him? Notwithstanding they [shama'](../../strongs/h/h8085.md) not unto the [qowl](../../strongs/h/h6963.md) of their ['ab](../../strongs/h/h1.md), because [Yĕhovah](../../strongs/h/h3068.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) [muwth](../../strongs/h/h4191.md) them.

<a name="1samuel_2_26"></a>1Samuel 2:26

And the [naʿar](../../strongs/h/h5288.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md) [gāḏēl](../../strongs/h/h1432.md) [halak](../../strongs/h/h1980.md), and was in [towb](../../strongs/h/h2896.md) both with [Yĕhovah](../../strongs/h/h3068.md), and also with ['enowsh](../../strongs/h/h582.md).

<a name="1samuel_2_27"></a>1Samuel 2:27

And there [bow'](../../strongs/h/h935.md) an ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) unto [ʿĒlî](../../strongs/h/h5941.md), and ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Did I [gālâ](../../strongs/h/h1540.md) [gālâ](../../strongs/h/h1540.md) unto the [bayith](../../strongs/h/h1004.md) of thy ['ab](../../strongs/h/h1.md), when they were in [Mitsrayim](../../strongs/h/h4714.md) in [Parʿô](../../strongs/h/h6547.md) [bayith](../../strongs/h/h1004.md)?

<a name="1samuel_2_28"></a>1Samuel 2:28

And did I [bāḥar](../../strongs/h/h977.md) him out of all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) to be my [kōhēn](../../strongs/h/h3548.md), to [ʿālâ](../../strongs/h/h5927.md) upon mine [mizbeach](../../strongs/h/h4196.md), to [qāṭar](../../strongs/h/h6999.md) [qᵊṭōreṯ](../../strongs/h/h7004.md), to [nasa'](../../strongs/h/h5375.md) an ['ēp̄ôḏ](../../strongs/h/h646.md) [paniym](../../strongs/h/h6440.md) me? and did I [nathan](../../strongs/h/h5414.md) unto the [bayith](../../strongs/h/h1004.md) of thy ['ab](../../strongs/h/h1.md) all the ['iššê](../../strongs/h/h801.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="1samuel_2_29"></a>1Samuel 2:29

Wherefore [bāʿaṭ](../../strongs/h/h1163.md) ye at my [zebach](../../strongs/h/h2077.md) and at mine [minchah](../../strongs/h/h4503.md), which I have [tsavah](../../strongs/h/h6680.md) in my [māʿôn](../../strongs/h/h4583.md); and [kabad](../../strongs/h/h3513.md) thy [ben](../../strongs/h/h1121.md) above me, to make yourselves [bara'](../../strongs/h/h1254.md) with the [re'shiyth](../../strongs/h/h7225.md) of all the [minchah](../../strongs/h/h4503.md) of [Yisra'el](../../strongs/h/h3478.md) my ['am](../../strongs/h/h5971.md)?

<a name="1samuel_2_30"></a>1Samuel 2:30

Wherefore [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [nᵊ'um](../../strongs/h/h5002.md), I ['āmar](../../strongs/h/h559.md) ['āmar](../../strongs/h/h559.md) that thy [bayith](../../strongs/h/h1004.md), and the [bayith](../../strongs/h/h1004.md) of thy ['ab](../../strongs/h/h1.md), should [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) me ['owlam](../../strongs/h/h5769.md): but now [Yĕhovah](../../strongs/h/h3068.md) [nᵊ'um](../../strongs/h/h5002.md), Be it far from [ḥālîlâ](../../strongs/h/h2486.md); for them that [kabad](../../strongs/h/h3513.md) me I will [kabad](../../strongs/h/h3513.md), and they that [bazah](../../strongs/h/h959.md) me shall be [qālal](../../strongs/h/h7043.md).

<a name="1samuel_2_31"></a>1Samuel 2:31

Behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), that I will [gāḏaʿ](../../strongs/h/h1438.md) thine [zerowa'](../../strongs/h/h2220.md), and the [zerowa'](../../strongs/h/h2220.md) of thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), that there shall not be an old [zāqēn](../../strongs/h/h2205.md) in thine [bayith](../../strongs/h/h1004.md).

<a name="1samuel_2_32"></a>1Samuel 2:32

And thou shalt [nabat](../../strongs/h/h5027.md) a [tsar](../../strongs/h/h6862.md) in my [māʿôn](../../strongs/h/h4583.md), in all the wealth which God shall [yatab](../../strongs/h/h3190.md) [Yisra'el](../../strongs/h/h3478.md): and there shall not be an old [zāqēn](../../strongs/h/h2205.md) in thine [bayith](../../strongs/h/h1004.md) for [yowm](../../strongs/h/h3117.md).

<a name="1samuel_2_33"></a>1Samuel 2:33

And the ['iysh](../../strongs/h/h376.md) of thine, whom I shall not [karath](../../strongs/h/h3772.md) from mine [mizbeach](../../strongs/h/h4196.md), shall be to [kalah](../../strongs/h/h3615.md) thine ['ayin](../../strongs/h/h5869.md), and to ['āḏaḇ](../../strongs/h/h109.md) thine [nephesh](../../strongs/h/h5315.md): and all the [marbîṯ](../../strongs/h/h4768.md) of thine [bayith](../../strongs/h/h1004.md) shall [muwth](../../strongs/h/h4191.md) in the flower of their ['enowsh](../../strongs/h/h582.md).

<a name="1samuel_2_34"></a>1Samuel 2:34

And this shall be a ['ôṯ](../../strongs/h/h226.md) unto thee, that shall [bow'](../../strongs/h/h935.md) upon thy two [ben](../../strongs/h/h1121.md), on [Ḥāp̄Nî](../../strongs/h/h2652.md) and [Pînḥās](../../strongs/h/h6372.md); in one [yowm](../../strongs/h/h3117.md) they shall [muwth](../../strongs/h/h4191.md) both of them.

<a name="1samuel_2_35"></a>1Samuel 2:35

And I will raise me [quwm](../../strongs/h/h6965.md) an ['aman](../../strongs/h/h539.md) [kōhēn](../../strongs/h/h3548.md), that shall ['asah](../../strongs/h/h6213.md) according to that which is in mine [lebab](../../strongs/h/h3824.md) and in my [nephesh](../../strongs/h/h5315.md): and I will [bānâ](../../strongs/h/h1129.md) him an ['aman](../../strongs/h/h539.md) [bayith](../../strongs/h/h1004.md); and he shall [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) mine [mashiyach](../../strongs/h/h4899.md) for [yowm](../../strongs/h/h3117.md).

<a name="1samuel_2_36"></a>1Samuel 2:36

And it shall come to pass, that every one that is [yāṯar](../../strongs/h/h3498.md) in thine [bayith](../../strongs/h/h1004.md) shall [bow'](../../strongs/h/h935.md) and [shachah](../../strongs/h/h7812.md) to him for a ['ăḡôrâ](../../strongs/h/h95.md) of [keceph](../../strongs/h/h3701.md) and a [kikār](../../strongs/h/h3603.md) of [lechem](../../strongs/h/h3899.md), and shall ['āmar](../../strongs/h/h559.md), [sāp̄aḥ](../../strongs/h/h5596.md) me, I pray thee, into one of the priests' [kᵊhunnâ](../../strongs/h/h3550.md), that I may ['akal](../../strongs/h/h398.md) a [paṯ](../../strongs/h/h6595.md) of [lechem](../../strongs/h/h3899.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 1](1samuel_1.md) - [1Samuel 3](1samuel_3.md)