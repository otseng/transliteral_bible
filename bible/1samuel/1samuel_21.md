# [1Samuel 21](https://www.blueletterbible.org/kjv/1samuel/21)

<a name="1samuel_21_1"></a>1Samuel 21:1

Then [bow'](../../strongs/h/h935.md) [Dāviḏ](../../strongs/h/h1732.md) to [nōḇ](../../strongs/h/h5011.md) to ['Ăḥîmeleḵ](../../strongs/h/h288.md) the [kōhēn](../../strongs/h/h3548.md): and ['Ăḥîmeleḵ](../../strongs/h/h288.md) was [ḥārēḏ](../../strongs/h/h2729.md) at the [qārā'](../../strongs/h/h7125.md) of [Dāviḏ](../../strongs/h/h1732.md), and ['āmar](../../strongs/h/h559.md) unto him, Why art thou alone, and no ['iysh](../../strongs/h/h376.md) with thee?

<a name="1samuel_21_2"></a>1Samuel 21:2

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto ['Ăḥîmeleḵ](../../strongs/h/h288.md) the [kōhēn](../../strongs/h/h3548.md), The [melek](../../strongs/h/h4428.md) hath [tsavah](../../strongs/h/h6680.md) me a [dabar](../../strongs/h/h1697.md), and hath ['āmar](../../strongs/h/h559.md) unto me, Let no ['iysh](../../strongs/h/h376.md) [yada'](../../strongs/h/h3045.md) any [mᵊ'ûmâ](../../strongs/h/h3972.md) of the [dabar](../../strongs/h/h1697.md) whereabout I [shalach](../../strongs/h/h7971.md) thee, and what I have [tsavah](../../strongs/h/h6680.md) thee: and I have [yada'](../../strongs/h/h3045.md) my [naʿar](../../strongs/h/h5288.md) to [palōnî](../../strongs/h/h6423.md) and ['almōnî](../../strongs/h/h492.md) a [maqowm](../../strongs/h/h4725.md).

<a name="1samuel_21_3"></a>1Samuel 21:3

Now therefore what is under thine [yad](../../strongs/h/h3027.md)? [nathan](../../strongs/h/h5414.md) me five loaves of [lechem](../../strongs/h/h3899.md) in mine [yad](../../strongs/h/h3027.md), or what there is [māṣā'](../../strongs/h/h4672.md).

<a name="1samuel_21_4"></a>1Samuel 21:4

And the [kōhēn](../../strongs/h/h3548.md) ['anah](../../strongs/h/h6030.md) [Dāviḏ](../../strongs/h/h1732.md), and ['āmar](../../strongs/h/h559.md), There is no [ḥōl](../../strongs/h/h2455.md) [lechem](../../strongs/h/h3899.md) under mine [yad](../../strongs/h/h3027.md), but there is [qodesh](../../strongs/h/h6944.md) [lechem](../../strongs/h/h3899.md); if the [naʿar](../../strongs/h/h5288.md) have [shamar](../../strongs/h/h8104.md) themselves at least from ['ishshah](../../strongs/h/h802.md).

<a name="1samuel_21_5"></a>1Samuel 21:5

And [Dāviḏ](../../strongs/h/h1732.md) ['anah](../../strongs/h/h6030.md) the [kōhēn](../../strongs/h/h3548.md), and ['āmar](../../strongs/h/h559.md) unto him, ['im](../../strongs/h/h518.md) ['ishshah](../../strongs/h/h802.md) have been [ʿāṣar](../../strongs/h/h6113.md) from us about these [šilšôm](../../strongs/h/h8032.md) [tᵊmôl](../../strongs/h/h8543.md), since I [yāṣā'](../../strongs/h/h3318.md), and the [kĕliy](../../strongs/h/h3627.md) of the [naʿar](../../strongs/h/h5288.md) are [qodesh](../../strongs/h/h6944.md), and the bread is in a [derek](../../strongs/h/h1870.md) [ḥōl](../../strongs/h/h2455.md), yea, though it were [qadash](../../strongs/h/h6942.md) this [yowm](../../strongs/h/h3117.md) in the [kĕliy](../../strongs/h/h3627.md).

<a name="1samuel_21_6"></a>1Samuel 21:6

So the [kōhēn](../../strongs/h/h3548.md) [nathan](../../strongs/h/h5414.md) him [qodesh](../../strongs/h/h6944.md): for there was no [lechem](../../strongs/h/h3899.md) there but the [lechem](../../strongs/h/h3899.md) [paniym](../../strongs/h/h6440.md), that was [cuwr](../../strongs/h/h5493.md) from [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), to [śûm](../../strongs/h/h7760.md) [ḥōm](../../strongs/h/h2527.md) [lechem](../../strongs/h/h3899.md) in the [yowm](../../strongs/h/h3117.md) when it was [laqach](../../strongs/h/h3947.md).

<a name="1samuel_21_7"></a>1Samuel 21:7

Now a certain ['iysh](../../strongs/h/h376.md) of the ['ebed](../../strongs/h/h5650.md) of [Šā'ûl](../../strongs/h/h7586.md) was there that [yowm](../../strongs/h/h3117.md), [ʿāṣar](../../strongs/h/h6113.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); and his [shem](../../strongs/h/h8034.md) was [Dō'Ēḡ](../../strongs/h/h1673.md), an ['Ăḏōmî](../../strongs/h/h130.md), the ['abîr](../../strongs/h/h47.md) of the [ra'ah](../../strongs/h/h7462.md) that belonged to [Šā'ûl](../../strongs/h/h7586.md).

<a name="1samuel_21_8"></a>1Samuel 21:8

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto ['Ăḥîmeleḵ](../../strongs/h/h288.md), And is there ['în](../../strongs/h/h371.md) here under thine [yad](../../strongs/h/h3027.md) [ḥănîṯ](../../strongs/h/h2595.md) or [chereb](../../strongs/h/h2719.md)? for I have neither [laqach](../../strongs/h/h3947.md) my [chereb](../../strongs/h/h2719.md) nor my [kĕliy](../../strongs/h/h3627.md) with [yad](../../strongs/h/h3027.md), because the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) required [nāḥaṣ](../../strongs/h/h5169.md).

<a name="1samuel_21_9"></a>1Samuel 21:9

And the [kōhēn](../../strongs/h/h3548.md) ['āmar](../../strongs/h/h559.md), The [chereb](../../strongs/h/h2719.md) of [Gālyaṯ](../../strongs/h/h1555.md) the [Pᵊlištî](../../strongs/h/h6430.md), whom thou [nakah](../../strongs/h/h5221.md) in the [ʿēmeq](../../strongs/h/h6010.md) of ['Ēlâ](../../strongs/h/h425.md), behold, it is here [lûṭ](../../strongs/h/h3874.md) in a [śimlâ](../../strongs/h/h8071.md) ['aḥar](../../strongs/h/h310.md) the ['ēp̄ôḏ](../../strongs/h/h646.md): if thou wilt [laqach](../../strongs/h/h3947.md) that, [laqach](../../strongs/h/h3947.md) it: for there is no ['aḥēr](../../strongs/h/h312.md) [zûlâ](../../strongs/h/h2108.md) that here. And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), There is none like that; [nathan](../../strongs/h/h5414.md) it me.

<a name="1samuel_21_10"></a>1Samuel 21:10

And [Dāviḏ](../../strongs/h/h1732.md) [quwm](../../strongs/h/h6965.md), and [bāraḥ](../../strongs/h/h1272.md) that [yowm](../../strongs/h/h3117.md) for [paniym](../../strongs/h/h6440.md) of [Šā'ûl](../../strongs/h/h7586.md), and [bow'](../../strongs/h/h935.md) to ['Āḵîš](../../strongs/h/h397.md) the [melek](../../strongs/h/h4428.md) of [Gaṯ](../../strongs/h/h1661.md).

<a name="1samuel_21_11"></a>1Samuel 21:11

And the ['ebed](../../strongs/h/h5650.md) of ['Āḵîš](../../strongs/h/h397.md) ['āmar](../../strongs/h/h559.md) unto him, Is not this [Dāviḏ](../../strongs/h/h1732.md) the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md)? did they not ['anah](../../strongs/h/h6030.md) one to another of him in [mᵊḥōlâ](../../strongs/h/h4246.md), ['āmar](../../strongs/h/h559.md), [Šā'ûl](../../strongs/h/h7586.md) hath [nakah](../../strongs/h/h5221.md) his thousands, and [Dāviḏ](../../strongs/h/h1732.md) his ten thousands?

<a name="1samuel_21_12"></a>1Samuel 21:12

And [Dāviḏ](../../strongs/h/h1732.md) [śûm](../../strongs/h/h7760.md) these [dabar](../../strongs/h/h1697.md) in his [lebab](../../strongs/h/h3824.md), and was [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md) [paniym](../../strongs/h/h6440.md) ['Āḵîš](../../strongs/h/h397.md) the [melek](../../strongs/h/h4428.md) of [Gaṯ](../../strongs/h/h1661.md).

<a name="1samuel_21_13"></a>1Samuel 21:13

And he [šānâ](../../strongs/h/h8138.md) his [ṭaʿam](../../strongs/h/h2940.md) ['ayin](../../strongs/h/h5869.md) them, and feigned himself [halal](../../strongs/h/h1984.md) in their [yad](../../strongs/h/h3027.md), and [tāvâ](../../strongs/h/h8427.md) on the [deleṯ](../../strongs/h/h1817.md) of the [sha'ar](../../strongs/h/h8179.md), and let his [rîr](../../strongs/h/h7388.md) fall [yarad](../../strongs/h/h3381.md) upon his [zāqān](../../strongs/h/h2206.md).

<a name="1samuel_21_14"></a>1Samuel 21:14

Then ['āmar](../../strongs/h/h559.md) ['Āḵîš](../../strongs/h/h397.md) unto his ['ebed](../../strongs/h/h5650.md), Lo, ye [ra'ah](../../strongs/h/h7200.md) the ['iysh](../../strongs/h/h376.md) is [šāḡaʿ](../../strongs/h/h7696.md): wherefore then have ye [bow'](../../strongs/h/h935.md) him to me?

<a name="1samuel_21_15"></a>1Samuel 21:15

Have I [ḥāsēr](../../strongs/h/h2638.md) of mad [šāḡaʿ](../../strongs/h/h7696.md), that ye have [bow'](../../strongs/h/h935.md) this fellow to play the mad [šāḡaʿ](../../strongs/h/h7696.md) in my presence? shall this fellow [bow'](../../strongs/h/h935.md) into my [bayith](../../strongs/h/h1004.md)?

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 20](1samuel_20.md) - [1Samuel 22](1samuel_22.md)