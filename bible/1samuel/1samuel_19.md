# [1Samuel 19](https://www.blueletterbible.org/kjv/1samuel/19)

<a name="1samuel_19_1"></a>1Samuel 19:1

And [Šā'ûl](../../strongs/h/h7586.md) [dabar](../../strongs/h/h1696.md) to [Yônāṯān](../../strongs/h/h3129.md) his [ben](../../strongs/h/h1121.md), and to all his ['ebed](../../strongs/h/h5650.md), that they should [muwth](../../strongs/h/h4191.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="1samuel_19_2"></a>1Samuel 19:2

But [Yᵊhônāṯān](../../strongs/h/h3083.md) [Šā'ûl](../../strongs/h/h7586.md) [ben](../../strongs/h/h1121.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) [me'od](../../strongs/h/h3966.md) in [Dāviḏ](../../strongs/h/h1732.md): and [Yᵊhônāṯān](../../strongs/h/h3083.md) [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), [Šā'ûl](../../strongs/h/h7586.md) my ['ab](../../strongs/h/h1.md) [bāqaš](../../strongs/h/h1245.md) to [muwth](../../strongs/h/h4191.md) thee: now therefore, I pray thee, take [shamar](../../strongs/h/h8104.md) to thyself until the [boqer](../../strongs/h/h1242.md), and [yashab](../../strongs/h/h3427.md) in a [cether](../../strongs/h/h5643.md) place, and [chaba'](../../strongs/h/h2244.md) thyself:

<a name="1samuel_19_3"></a>1Samuel 19:3

And I will [yāṣā'](../../strongs/h/h3318.md) and ['amad](../../strongs/h/h5975.md) [yad](../../strongs/h/h3027.md) my ['ab](../../strongs/h/h1.md) in the [sadeh](../../strongs/h/h7704.md) where thou art, and I will [dabar](../../strongs/h/h1696.md) with my ['ab](../../strongs/h/h1.md) of thee; and what I [ra'ah](../../strongs/h/h7200.md), that I will [nāḡaḏ](../../strongs/h/h5046.md) thee.

<a name="1samuel_19_4"></a>1Samuel 19:4

And [Yᵊhônāṯān](../../strongs/h/h3083.md) [dabar](../../strongs/h/h1696.md) [towb](../../strongs/h/h2896.md) of [Dāviḏ](../../strongs/h/h1732.md) unto [Šā'ûl](../../strongs/h/h7586.md) his ['ab](../../strongs/h/h1.md), and ['āmar](../../strongs/h/h559.md) unto him, Let not the [melek](../../strongs/h/h4428.md) [chata'](../../strongs/h/h2398.md) against his ['ebed](../../strongs/h/h5650.md), against [Dāviḏ](../../strongs/h/h1732.md); because he hath not [chata'](../../strongs/h/h2398.md) against thee, and because his [ma'aseh](../../strongs/h/h4639.md) have been to thee-ward [me'od](../../strongs/h/h3966.md) [towb](../../strongs/h/h2896.md):

<a name="1samuel_19_5"></a>1Samuel 19:5

For he did [śûm](../../strongs/h/h7760.md) his [nephesh](../../strongs/h/h5315.md) in his [kaph](../../strongs/h/h3709.md), and [nakah](../../strongs/h/h5221.md) the [Pᵊlištî](../../strongs/h/h6430.md), and [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) a [gadowl](../../strongs/h/h1419.md) [tᵊšûʿâ](../../strongs/h/h8668.md) for all [Yisra'el](../../strongs/h/h3478.md): thou [ra'ah](../../strongs/h/h7200.md) it, and didst [samach](../../strongs/h/h8055.md): wherefore then wilt thou [chata'](../../strongs/h/h2398.md) against [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md), to [muwth](../../strongs/h/h4191.md) [Dāviḏ](../../strongs/h/h1732.md) without a [ḥinnām](../../strongs/h/h2600.md)?

<a name="1samuel_19_6"></a>1Samuel 19:6

And [Šā'ûl](../../strongs/h/h7586.md) [shama'](../../strongs/h/h8085.md) unto the [qowl](../../strongs/h/h6963.md) of [Yᵊhônāṯān](../../strongs/h/h3083.md): and [Šā'ûl](../../strongs/h/h7586.md) [shaba'](../../strongs/h/h7650.md), As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), he shall not be [muwth](../../strongs/h/h4191.md).

<a name="1samuel_19_7"></a>1Samuel 19:7

And [Yᵊhônāṯān](../../strongs/h/h3083.md) [qara'](../../strongs/h/h7121.md) [Dāviḏ](../../strongs/h/h1732.md), and [Yᵊhônāṯān](../../strongs/h/h3083.md) [nāḡaḏ](../../strongs/h/h5046.md) him all those [dabar](../../strongs/h/h1697.md). And [Yᵊhônāṯān](../../strongs/h/h3083.md) [bow'](../../strongs/h/h935.md) [Dāviḏ](../../strongs/h/h1732.md) to [Šā'ûl](../../strongs/h/h7586.md), and he was in his [paniym](../../strongs/h/h6440.md), as in times ['eṯmôl](../../strongs/h/h865.md) [šilšôm](../../strongs/h/h8032.md).

<a name="1samuel_19_8"></a>1Samuel 19:8

And there was [milḥāmâ](../../strongs/h/h4421.md) again: and [Dāviḏ](../../strongs/h/h1732.md) [yāṣā'](../../strongs/h/h3318.md), and [lāḥam](../../strongs/h/h3898.md) with the [Pᵊlištî](../../strongs/h/h6430.md), and [nakah](../../strongs/h/h5221.md) them with a [gadowl](../../strongs/h/h1419.md) [makâ](../../strongs/h/h4347.md); and they [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) him.

<a name="1samuel_19_9"></a>1Samuel 19:9

And the [ra'](../../strongs/h/h7451.md) [ruwach](../../strongs/h/h7307.md) from [Yĕhovah](../../strongs/h/h3068.md) was upon [Šā'ûl](../../strongs/h/h7586.md), as he [yashab](../../strongs/h/h3427.md) in his [bayith](../../strongs/h/h1004.md) with his [ḥănîṯ](../../strongs/h/h2595.md) in his [yad](../../strongs/h/h3027.md): and [Dāviḏ](../../strongs/h/h1732.md) [nāḡan](../../strongs/h/h5059.md) with his [yad](../../strongs/h/h3027.md).

<a name="1samuel_19_10"></a>1Samuel 19:10

And [Šā'ûl](../../strongs/h/h7586.md) [bāqaš](../../strongs/h/h1245.md) to [nakah](../../strongs/h/h5221.md) [Dāviḏ](../../strongs/h/h1732.md) even to the [qîr](../../strongs/h/h7023.md) with the [ḥănîṯ](../../strongs/h/h2595.md); but he [pāṭar](../../strongs/h/h6362.md) out of [Šā'ûl](../../strongs/h/h7586.md) [paniym](../../strongs/h/h6440.md), and he [nakah](../../strongs/h/h5221.md) the [ḥănîṯ](../../strongs/h/h2595.md) into the [qîr](../../strongs/h/h7023.md): and [Dāviḏ](../../strongs/h/h1732.md) [nûs](../../strongs/h/h5127.md), and [mālaṭ](../../strongs/h/h4422.md) that [layil](../../strongs/h/h3915.md).

<a name="1samuel_19_11"></a>1Samuel 19:11

[Šā'ûl](../../strongs/h/h7586.md) also [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) unto [Dāviḏ](../../strongs/h/h1732.md) [bayith](../../strongs/h/h1004.md), to [shamar](../../strongs/h/h8104.md) him, and to [muwth](../../strongs/h/h4191.md) him in the [boqer](../../strongs/h/h1242.md): and [Mîḵāl](../../strongs/h/h4324.md) [Dāviḏ](../../strongs/h/h1732.md) ['ishshah](../../strongs/h/h802.md) [nāḡaḏ](../../strongs/h/h5046.md) him, ['āmar](../../strongs/h/h559.md), If thou [mālaṭ](../../strongs/h/h4422.md) not thy [nephesh](../../strongs/h/h5315.md) to [layil](../../strongs/h/h3915.md), [māḥār](../../strongs/h/h4279.md) thou shalt be [muwth](../../strongs/h/h4191.md).

<a name="1samuel_19_12"></a>1Samuel 19:12

So [Mîḵāl](../../strongs/h/h4324.md) [yarad](../../strongs/h/h3381.md) [Dāviḏ](../../strongs/h/h1732.md) [yarad](../../strongs/h/h3381.md) through a [ḥallôn](../../strongs/h/h2474.md): and he [yālaḵ](../../strongs/h/h3212.md), and [bāraḥ](../../strongs/h/h1272.md), and [mālaṭ](../../strongs/h/h4422.md).

<a name="1samuel_19_13"></a>1Samuel 19:13

And [Mîḵāl](../../strongs/h/h4324.md) [laqach](../../strongs/h/h3947.md) a [tᵊrāp̄îm](../../strongs/h/h8655.md), and [śûm](../../strongs/h/h7760.md) it in the [mittah](../../strongs/h/h4296.md), and [śûm](../../strongs/h/h7760.md) a [kᵊḇîr](../../strongs/h/h3523.md) of [ʿēz](../../strongs/h/h5795.md) for his [mᵊra'ăšôṯ](../../strongs/h/h4763.md), and [kāsâ](../../strongs/h/h3680.md) it with a [beḡeḏ](../../strongs/h/h899.md).

<a name="1samuel_19_14"></a>1Samuel 19:14

And when [Šā'ûl](../../strongs/h/h7586.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to [laqach](../../strongs/h/h3947.md) [Dāviḏ](../../strongs/h/h1732.md), she ['āmar](../../strongs/h/h559.md), He is [ḥālâ](../../strongs/h/h2470.md).

<a name="1samuel_19_15"></a>1Samuel 19:15

And [Šā'ûl](../../strongs/h/h7586.md) [shalach](../../strongs/h/h7971.md) the [mal'ak](../../strongs/h/h4397.md) again to [ra'ah](../../strongs/h/h7200.md) [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md) him to me in the [mittah](../../strongs/h/h4296.md), that I may [muwth](../../strongs/h/h4191.md) him.

<a name="1samuel_19_16"></a>1Samuel 19:16

And when the [mal'ak](../../strongs/h/h4397.md) were [bow'](../../strongs/h/h935.md), behold, there was a [tᵊrāp̄îm](../../strongs/h/h8655.md) in the [mittah](../../strongs/h/h4296.md), with a [kᵊḇîr](../../strongs/h/h3523.md) of [ʿēz](../../strongs/h/h5795.md) hair for his [mᵊra'ăšôṯ](../../strongs/h/h4763.md).

<a name="1samuel_19_17"></a>1Samuel 19:17

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto [Mîḵāl](../../strongs/h/h4324.md), Why hast thou [rāmâ](../../strongs/h/h7411.md) me, and [shalach](../../strongs/h/h7971.md) mine ['oyeb](../../strongs/h/h341.md), that he is [mālaṭ](../../strongs/h/h4422.md)? And [Mîḵāl](../../strongs/h/h4324.md) ['āmar](../../strongs/h/h559.md) [Šā'ûl](../../strongs/h/h7586.md), He ['āmar](../../strongs/h/h559.md) unto me, Let me [shalach](../../strongs/h/h7971.md); why should I [muwth](../../strongs/h/h4191.md) thee?

<a name="1samuel_19_18"></a>1Samuel 19:18

So [Dāviḏ](../../strongs/h/h1732.md) [bāraḥ](../../strongs/h/h1272.md), and [mālaṭ](../../strongs/h/h4422.md), and [bow'](../../strongs/h/h935.md) to [Šᵊmû'Ēl](../../strongs/h/h8050.md) to [rāmâ](../../strongs/h/h7414.md), and [nāḡaḏ](../../strongs/h/h5046.md) him all that [Šā'ûl](../../strongs/h/h7586.md) had ['asah](../../strongs/h/h6213.md) to him. And he and [Šᵊmû'Ēl](../../strongs/h/h8050.md) [yālaḵ](../../strongs/h/h3212.md) and [yashab](../../strongs/h/h3427.md) in [Nāvîṯ](../../strongs/h/h5121.md).

<a name="1samuel_19_19"></a>1Samuel 19:19

And it was [nāḡaḏ](../../strongs/h/h5046.md) [Šā'ûl](../../strongs/h/h7586.md), ['āmar](../../strongs/h/h559.md), Behold, [Dāviḏ](../../strongs/h/h1732.md) is at [Nāvîṯ](../../strongs/h/h5121.md) in [rāmâ](../../strongs/h/h7414.md).

<a name="1samuel_19_20"></a>1Samuel 19:20

And [Šā'ûl](../../strongs/h/h7586.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to [laqach](../../strongs/h/h3947.md) [Dāviḏ](../../strongs/h/h1732.md): and when they [ra'ah](../../strongs/h/h7200.md) the [lahăqâ](../../strongs/h/h3862.md) of the [nāḇî'](../../strongs/h/h5030.md) [nāḇā'](../../strongs/h/h5012.md), and [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['amad](../../strongs/h/h5975.md) as [nāṣaḇ](../../strongs/h/h5324.md) over them, the [ruwach](../../strongs/h/h7307.md) of ['Elohiym](../../strongs/h/h430.md) was upon the [mal'ak](../../strongs/h/h4397.md) of [Šā'ûl](../../strongs/h/h7586.md), and they also [nāḇā'](../../strongs/h/h5012.md).

<a name="1samuel_19_21"></a>1Samuel 19:21

And when it was [nāḡaḏ](../../strongs/h/h5046.md) [Šā'ûl](../../strongs/h/h7586.md), he [shalach](../../strongs/h/h7971.md) ['aḥēr](../../strongs/h/h312.md) [mal'ak](../../strongs/h/h4397.md), and they [nāḇā'](../../strongs/h/h5012.md) likewise. And [Šā'ûl](../../strongs/h/h7586.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) again the third time, and they [nāḇā'](../../strongs/h/h5012.md) also.

<a name="1samuel_19_22"></a>1Samuel 19:22

Then [yālaḵ](../../strongs/h/h3212.md) he also to [rāmâ](../../strongs/h/h7414.md), and [bow'](../../strongs/h/h935.md) to a [gadowl](../../strongs/h/h1419.md) [bowr](../../strongs/h/h953.md) that is in [Śēḵû](../../strongs/h/h7906.md): and he [sha'al](../../strongs/h/h7592.md) and ['āmar](../../strongs/h/h559.md), ['êp̄ô](../../strongs/h/h375.md) are [Šᵊmû'Ēl](../../strongs/h/h8050.md) and [Dāviḏ](../../strongs/h/h1732.md)? And one ['āmar](../../strongs/h/h559.md), Behold, they be at [Nāvîṯ](../../strongs/h/h5121.md) in [rāmâ](../../strongs/h/h7414.md).

<a name="1samuel_19_23"></a>1Samuel 19:23

And he [yālaḵ](../../strongs/h/h3212.md) thither to [Nāvîṯ](../../strongs/h/h5121.md) in [rāmâ](../../strongs/h/h7414.md): and the [ruwach](../../strongs/h/h7307.md) of ['Elohiym](../../strongs/h/h430.md) was upon him also, and he [yālaḵ](../../strongs/h/h3212.md) [halak](../../strongs/h/h1980.md), and [nāḇā'](../../strongs/h/h5012.md), until he [bow'](../../strongs/h/h935.md) to [Nāvîṯ](../../strongs/h/h5121.md) in [rāmâ](../../strongs/h/h7414.md).

<a name="1samuel_19_24"></a>1Samuel 19:24

And he [pāšaṭ](../../strongs/h/h6584.md) his [beḡeḏ](../../strongs/h/h899.md) also, and [nāḇā'](../../strongs/h/h5012.md) [paniym](../../strongs/h/h6440.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md) in like manner, and lay [naphal](../../strongs/h/h5307.md) ['arowm](../../strongs/h/h6174.md) all that [yowm](../../strongs/h/h3117.md) and all that [layil](../../strongs/h/h3915.md). Wherefore they ['āmar](../../strongs/h/h559.md), Is [Šā'ûl](../../strongs/h/h7586.md) also among the [nāḇî'](../../strongs/h/h5030.md)?

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 18](1samuel_18.md) - [1Samuel 20](1samuel_20.md)