# [1Samuel 26](https://www.blueletterbible.org/kjv/1samuel/26)

<a name="1samuel_26_1"></a>1Samuel 26:1

And the [Zîp̄î](../../strongs/h/h2130.md) [bow'](../../strongs/h/h935.md) unto [Šā'ûl](../../strongs/h/h7586.md) to [giḇʿâ](../../strongs/h/h1390.md), ['āmar](../../strongs/h/h559.md), Doth not [Dāviḏ](../../strongs/h/h1732.md) [cathar](../../strongs/h/h5641.md) himself in the [giḇʿâ](../../strongs/h/h1389.md) of [Ḥăḵîlâ](../../strongs/h/h2444.md), which is [paniym](../../strongs/h/h6440.md) [yᵊšîmôn](../../strongs/h/h3452.md)?

<a name="1samuel_26_2"></a>1Samuel 26:2

Then [Šā'ûl](../../strongs/h/h7586.md) [quwm](../../strongs/h/h6965.md), and [yarad](../../strongs/h/h3381.md) to the [midbar](../../strongs/h/h4057.md) of [Zîp̄](../../strongs/h/h2128.md), having three thousand [bāḥar](../../strongs/h/h977.md) ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) with him, to [bāqaš](../../strongs/h/h1245.md) [Dāviḏ](../../strongs/h/h1732.md) in the [midbar](../../strongs/h/h4057.md) of [Zîp̄](../../strongs/h/h2128.md).

<a name="1samuel_26_3"></a>1Samuel 26:3

And [Šā'ûl](../../strongs/h/h7586.md) [ḥānâ](../../strongs/h/h2583.md) in the [giḇʿâ](../../strongs/h/h1389.md) of [Ḥăḵîlâ](../../strongs/h/h2444.md), which is [paniym](../../strongs/h/h6440.md) [yᵊšîmôn](../../strongs/h/h3452.md), by the [derek](../../strongs/h/h1870.md). But [Dāviḏ](../../strongs/h/h1732.md) [yashab](../../strongs/h/h3427.md) in the [midbar](../../strongs/h/h4057.md), and he [ra'ah](../../strongs/h/h7200.md) that [Šā'ûl](../../strongs/h/h7586.md) [bow'](../../strongs/h/h935.md) ['aḥar](../../strongs/h/h310.md) him into the [midbar](../../strongs/h/h4057.md).

<a name="1samuel_26_4"></a>1Samuel 26:4

[Dāviḏ](../../strongs/h/h1732.md) therefore [shalach](../../strongs/h/h7971.md) [ragal](../../strongs/h/h7270.md), and [yada'](../../strongs/h/h3045.md) that [Šā'ûl](../../strongs/h/h7586.md) was [bow'](../../strongs/h/h935.md) in [kuwn](../../strongs/h/h3559.md).

<a name="1samuel_26_5"></a>1Samuel 26:5

And [Dāviḏ](../../strongs/h/h1732.md) [quwm](../../strongs/h/h6965.md), and [bow'](../../strongs/h/h935.md) to the [maqowm](../../strongs/h/h4725.md) where [Šā'ûl](../../strongs/h/h7586.md) had [ḥānâ](../../strongs/h/h2583.md): and [Dāviḏ](../../strongs/h/h1732.md) [ra'ah](../../strongs/h/h7200.md) the [maqowm](../../strongs/h/h4725.md) where [Šā'ûl](../../strongs/h/h7586.md) [shakab](../../strongs/h/h7901.md), and ['Aḇnēr](../../strongs/h/h74.md) the [ben](../../strongs/h/h1121.md) of [Nēr](../../strongs/h/h5369.md), the [śar](../../strongs/h/h8269.md) of his [tsaba'](../../strongs/h/h6635.md): and [Šā'ûl](../../strongs/h/h7586.md) [shakab](../../strongs/h/h7901.md) in the [ma'gal](../../strongs/h/h4570.md), and the ['am](../../strongs/h/h5971.md) [ḥānâ](../../strongs/h/h2583.md) [cabiyb](../../strongs/h/h5439.md) him.

<a name="1samuel_26_6"></a>1Samuel 26:6

Then ['anah](../../strongs/h/h6030.md) [Dāviḏ](../../strongs/h/h1732.md) and ['āmar](../../strongs/h/h559.md) to ['Ăḥîmeleḵ](../../strongs/h/h288.md) the [Ḥitî](../../strongs/h/h2850.md), and to ['Ăḇîšay](../../strongs/h/h52.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md), ['ach](../../strongs/h/h251.md) to [Yô'āḇ](../../strongs/h/h3097.md), ['āmar](../../strongs/h/h559.md), Who will [yarad](../../strongs/h/h3381.md) with me to [Šā'ûl](../../strongs/h/h7586.md) to the [maḥănê](../../strongs/h/h4264.md)? And ['Ăḇîšay](../../strongs/h/h52.md) ['āmar](../../strongs/h/h559.md), I will [yarad](../../strongs/h/h3381.md) with thee.

<a name="1samuel_26_7"></a>1Samuel 26:7

So [Dāviḏ](../../strongs/h/h1732.md) and ['Ăḇîšay](../../strongs/h/h52.md) [bow'](../../strongs/h/h935.md) to the ['am](../../strongs/h/h5971.md) by [layil](../../strongs/h/h3915.md): and, behold, [Šā'ûl](../../strongs/h/h7586.md) [shakab](../../strongs/h/h7901.md) [yāšēn](../../strongs/h/h3463.md) within the [ma'gal](../../strongs/h/h4570.md), and his [ḥănîṯ](../../strongs/h/h2595.md) [māʿaḵ](../../strongs/h/h4600.md) in the ['erets](../../strongs/h/h776.md) at his [mᵊra'ăšôṯ](../../strongs/h/h4763.md): but ['Aḇnēr](../../strongs/h/h74.md) and the ['am](../../strongs/h/h5971.md) [shakab](../../strongs/h/h7901.md) [cabiyb](../../strongs/h/h5439.md) him.

<a name="1samuel_26_8"></a>1Samuel 26:8

Then ['āmar](../../strongs/h/h559.md) ['Ăḇîšay](../../strongs/h/h52.md) to [Dāviḏ](../../strongs/h/h1732.md), ['Elohiym](../../strongs/h/h430.md) hath [cagar](../../strongs/h/h5462.md) thine ['oyeb](../../strongs/h/h341.md) into thine [yad](../../strongs/h/h3027.md) this [yowm](../../strongs/h/h3117.md): now therefore let me [nakah](../../strongs/h/h5221.md) him, I pray thee, with the [ḥănîṯ](../../strongs/h/h2595.md) even to the ['erets](../../strongs/h/h776.md) at once , and I will not smite him the second [šānâ](../../strongs/h/h8138.md).

<a name="1samuel_26_9"></a>1Samuel 26:9

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to ['Ăḇîšay](../../strongs/h/h52.md), [shachath](../../strongs/h/h7843.md) him not: for who can [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) against [Yĕhovah](../../strongs/h/h3068.md) [mashiyach](../../strongs/h/h4899.md), and be [naqah](../../strongs/h/h5352.md)?

<a name="1samuel_26_10"></a>1Samuel 26:10

[Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) furthermore, As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), [Yĕhovah](../../strongs/h/h3068.md) shall [nāḡap̄](../../strongs/h/h5062.md) him; or his [yowm](../../strongs/h/h3117.md) shall [bow'](../../strongs/h/h935.md) to [muwth](../../strongs/h/h4191.md); or he shall [yarad](../../strongs/h/h3381.md) into [milḥāmâ](../../strongs/h/h4421.md), and [sāp̄â](../../strongs/h/h5595.md).

<a name="1samuel_26_11"></a>1Samuel 26:11

[Yĕhovah](../../strongs/h/h3068.md) [ḥālîlâ](../../strongs/h/h2486.md) that I should [shalach](../../strongs/h/h7971.md) mine [yad](../../strongs/h/h3027.md) against [Yĕhovah](../../strongs/h/h3068.md) [mashiyach](../../strongs/h/h4899.md): but, I pray thee, [laqach](../../strongs/h/h3947.md) thou now the [ḥănîṯ](../../strongs/h/h2595.md) that is at his [mᵊra'ăšôṯ](../../strongs/h/h4763.md), and the [ṣapaḥaṯ](../../strongs/h/h6835.md) of [mayim](../../strongs/h/h4325.md), and let us [yālaḵ](../../strongs/h/h3212.md).

<a name="1samuel_26_12"></a>1Samuel 26:12

So [Dāviḏ](../../strongs/h/h1732.md) [laqach](../../strongs/h/h3947.md) the [ḥănîṯ](../../strongs/h/h2595.md) and the [ṣapaḥaṯ](../../strongs/h/h6835.md) of [mayim](../../strongs/h/h4325.md) from [Šā'ûl](../../strongs/h/h7586.md) [r'ש](../../strongs/h/h7226.md); and they gat them [yālaḵ](../../strongs/h/h3212.md), and no man [ra'ah](../../strongs/h/h7200.md) it, nor [yada'](../../strongs/h/h3045.md) it, neither [quwts](../../strongs/h/h6974.md): for they were all [yāšēn](../../strongs/h/h3463.md); because a [tardēmâ](../../strongs/h/h8639.md) from [Yĕhovah](../../strongs/h/h3068.md) was [naphal](../../strongs/h/h5307.md) upon them.

<a name="1samuel_26_13"></a>1Samuel 26:13

Then [Dāviḏ](../../strongs/h/h1732.md) ['abar](../../strongs/h/h5674.md) to the other [ʿēḇer](../../strongs/h/h5676.md), and ['amad](../../strongs/h/h5975.md) on the [ro'sh](../../strongs/h/h7218.md) of an [har](../../strongs/h/h2022.md) afar [rachowq](../../strongs/h/h7350.md); a [rab](../../strongs/h/h7227.md) [maqowm](../../strongs/h/h4725.md) being between them:

<a name="1samuel_26_14"></a>1Samuel 26:14

And [Dāviḏ](../../strongs/h/h1732.md) [qara'](../../strongs/h/h7121.md) to the ['am](../../strongs/h/h5971.md), and to ['Aḇnēr](../../strongs/h/h74.md) the [ben](../../strongs/h/h1121.md) of [Nēr](../../strongs/h/h5369.md), ['āmar](../../strongs/h/h559.md), ['anah](../../strongs/h/h6030.md) thou not, ['Aḇnēr](../../strongs/h/h74.md)? Then ['Aḇnēr](../../strongs/h/h74.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), Who art thou that [qara'](../../strongs/h/h7121.md) to the [melek](../../strongs/h/h4428.md)?

<a name="1samuel_26_15"></a>1Samuel 26:15

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to ['Aḇnēr](../../strongs/h/h74.md), Art not thou an ['iysh](../../strongs/h/h376.md)? and who is like to thee in [Yisra'el](../../strongs/h/h3478.md)? wherefore then hast thou not [shamar](../../strongs/h/h8104.md) thy ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md)? for there [bow'](../../strongs/h/h935.md) one of the ['am](../../strongs/h/h5971.md) in to [shachath](../../strongs/h/h7843.md) the [melek](../../strongs/h/h4428.md) thy ['adown](../../strongs/h/h113.md).

<a name="1samuel_26_16"></a>1Samuel 26:16

This [dabar](../../strongs/h/h1697.md) is not [towb](../../strongs/h/h2896.md) that thou hast ['asah](../../strongs/h/h6213.md). As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), ye are [ben](../../strongs/h/h1121.md) to [maveth](../../strongs/h/h4194.md), because ye have not [shamar](../../strongs/h/h8104.md) your ['adown](../../strongs/h/h113.md), [Yĕhovah](../../strongs/h/h3068.md) [mashiyach](../../strongs/h/h4899.md). And now [ra'ah](../../strongs/h/h7200.md) where the [melek](../../strongs/h/h4428.md) [ḥănîṯ](../../strongs/h/h2595.md) is, and the [ṣapaḥaṯ](../../strongs/h/h6835.md) of [mayim](../../strongs/h/h4325.md) that was at his [mᵊra'ăšôṯ](../../strongs/h/h4763.md).

<a name="1samuel_26_17"></a>1Samuel 26:17

And [Šā'ûl](../../strongs/h/h7586.md) [nāḵar](../../strongs/h/h5234.md) [Dāviḏ](../../strongs/h/h1732.md) [qowl](../../strongs/h/h6963.md), and ['āmar](../../strongs/h/h559.md), Is this thy [qowl](../../strongs/h/h6963.md), my [ben](../../strongs/h/h1121.md) [Dāviḏ](../../strongs/h/h1732.md)? And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), It is my [qowl](../../strongs/h/h6963.md), my ['adown](../../strongs/h/h113.md), O [melek](../../strongs/h/h4428.md).

<a name="1samuel_26_18"></a>1Samuel 26:18

And he ['āmar](../../strongs/h/h559.md), Wherefore doth my ['adown](../../strongs/h/h113.md) thus [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) his ['ebed](../../strongs/h/h5650.md)? for what have I ['asah](../../strongs/h/h6213.md)? or what [ra'](../../strongs/h/h7451.md) is in mine [yad](../../strongs/h/h3027.md)?

<a name="1samuel_26_19"></a>1Samuel 26:19

Now therefore, I pray thee, let my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of his ['ebed](../../strongs/h/h5650.md). If [Yĕhovah](../../strongs/h/h3068.md) have stirred thee [sûṯ](../../strongs/h/h5496.md) against me, let him [rîaḥ](../../strongs/h/h7306.md) a [minchah](../../strongs/h/h4503.md): but if they be the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), ['arar](../../strongs/h/h779.md) be they [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); for they have [gāraš](../../strongs/h/h1644.md) me out this [yowm](../../strongs/h/h3117.md) from [sāp̄aḥ](../../strongs/h/h5596.md) in the [nachalah](../../strongs/h/h5159.md) of [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md).

<a name="1samuel_26_20"></a>1Samuel 26:20

Now therefore, let not my [dam](../../strongs/h/h1818.md) [naphal](../../strongs/h/h5307.md) to the ['erets](../../strongs/h/h776.md) before the [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md): for the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) is [yāṣā'](../../strongs/h/h3318.md) to [bāqaš](../../strongs/h/h1245.md) a [parʿš](../../strongs/h/h6550.md), as when one doth [radaph](../../strongs/h/h7291.md) a [qōrē'](../../strongs/h/h7124.md) in the [har](../../strongs/h/h2022.md).

<a name="1samuel_26_21"></a>1Samuel 26:21

Then ['āmar](../../strongs/h/h559.md) [Šā'ûl](../../strongs/h/h7586.md), I have [chata'](../../strongs/h/h2398.md): [shuwb](../../strongs/h/h7725.md), my [ben](../../strongs/h/h1121.md) [Dāviḏ](../../strongs/h/h1732.md): for I will no more do thee [ra'a'](../../strongs/h/h7489.md), because my [nephesh](../../strongs/h/h5315.md) was [yāqar](../../strongs/h/h3365.md) in thine ['ayin](../../strongs/h/h5869.md) this [yowm](../../strongs/h/h3117.md): behold, I have [sāḵal](../../strongs/h/h5528.md), and have [šāḡâ](../../strongs/h/h7686.md) [rabah](../../strongs/h/h7235.md) [me'od](../../strongs/h/h3966.md).

<a name="1samuel_26_22"></a>1Samuel 26:22

And [Dāviḏ](../../strongs/h/h1732.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), Behold the [melek](../../strongs/h/h4428.md) [ḥănîṯ](../../strongs/h/h2595.md)! and let one of the [naʿar](../../strongs/h/h5288.md) ['abar](../../strongs/h/h5674.md) and [laqach](../../strongs/h/h3947.md) it.

<a name="1samuel_26_23"></a>1Samuel 26:23

[Yĕhovah](../../strongs/h/h3068.md) [shuwb](../../strongs/h/h7725.md) to every ['iysh](../../strongs/h/h376.md) his [tsedaqah](../../strongs/h/h6666.md) and his ['ĕmûnâ](../../strongs/h/h530.md): for [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) thee into my [yad](../../strongs/h/h3027.md) to [yowm](../../strongs/h/h3117.md), but I ['āḇâ](../../strongs/h/h14.md) not [shalach](../../strongs/h/h7971.md) mine [yad](../../strongs/h/h3027.md) against [Yĕhovah](../../strongs/h/h3068.md) [mashiyach](../../strongs/h/h4899.md).

<a name="1samuel_26_24"></a>1Samuel 26:24

And, behold, as thy [nephesh](../../strongs/h/h5315.md) was much set [gāḏal](../../strongs/h/h1431.md) this [yowm](../../strongs/h/h3117.md) in mine ['ayin](../../strongs/h/h5869.md), so let my [nephesh](../../strongs/h/h5315.md) be much set [gāḏal](../../strongs/h/h1431.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and let him [natsal](../../strongs/h/h5337.md) me out of all [tsarah](../../strongs/h/h6869.md).

<a name="1samuel_26_25"></a>1Samuel 26:25

Then [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), [barak](../../strongs/h/h1288.md) be thou, my [ben](../../strongs/h/h1121.md) [Dāviḏ](../../strongs/h/h1732.md): thou shalt both ['asah](../../strongs/h/h6213.md) ['asah](../../strongs/h/h6213.md) things, and also shalt [yakol](../../strongs/h/h3201.md) [yakol](../../strongs/h/h3201.md). So [Dāviḏ](../../strongs/h/h1732.md) [yālaḵ](../../strongs/h/h3212.md) on his [derek](../../strongs/h/h1870.md), and [Šā'ûl](../../strongs/h/h7586.md) [shuwb](../../strongs/h/h7725.md) to his [maqowm](../../strongs/h/h4725.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 25](1samuel_25.md) - [1Samuel 27](1samuel_27.md)