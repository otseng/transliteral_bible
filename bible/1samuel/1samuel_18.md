# [1Samuel 18](https://www.blueletterbible.org/kjv/1samuel/18)

<a name="1samuel_18_1"></a>1Samuel 18:1

And it came to pass, when he had made a [kalah](../../strongs/h/h3615.md) of [dabar](../../strongs/h/h1696.md) unto [Šā'ûl](../../strongs/h/h7586.md), that the [nephesh](../../strongs/h/h5315.md) of [Yᵊhônāṯān](../../strongs/h/h3083.md) was [qāšar](../../strongs/h/h7194.md) with the [nephesh](../../strongs/h/h5315.md) of [Dāviḏ](../../strongs/h/h1732.md), and [Yᵊhônāṯān](../../strongs/h/h3083.md) ['ahab](../../strongs/h/h157.md) him as his own [nephesh](../../strongs/h/h5315.md).

<a name="1samuel_18_2"></a>1Samuel 18:2

And [Šā'ûl](../../strongs/h/h7586.md) [laqach](../../strongs/h/h3947.md) him that [yowm](../../strongs/h/h3117.md), and would [nathan](../../strongs/h/h5414.md) him go no more [shuwb](../../strongs/h/h7725.md) to his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md).

<a name="1samuel_18_3"></a>1Samuel 18:3

Then [Yᵊhônāṯān](../../strongs/h/h3083.md) and [Dāviḏ](../../strongs/h/h1732.md) [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md), because he ['ahăḇâ](../../strongs/h/h160.md) him as his own [nephesh](../../strongs/h/h5315.md).

<a name="1samuel_18_4"></a>1Samuel 18:4

And [Yᵊhônāṯān](../../strongs/h/h3083.md) [pāšaṭ](../../strongs/h/h6584.md) himself of the [mᵊʿîl](../../strongs/h/h4598.md) that was upon him, and [nathan](../../strongs/h/h5414.md) it to [Dāviḏ](../../strongs/h/h1732.md), and his [maḏ](../../strongs/h/h4055.md), even to his [chereb](../../strongs/h/h2719.md), and to his [qesheth](../../strongs/h/h7198.md), and to his [ḥăḡôr](../../strongs/h/h2289.md).

<a name="1samuel_18_5"></a>1Samuel 18:5

And [Dāviḏ](../../strongs/h/h1732.md) [yāṣā'](../../strongs/h/h3318.md) whithersoever [Šā'ûl](../../strongs/h/h7586.md) [shalach](../../strongs/h/h7971.md) him, and behaved himself [sakal](../../strongs/h/h7919.md): and [Šā'ûl](../../strongs/h/h7586.md) [śûm](../../strongs/h/h7760.md) him over the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md), and he was [yatab](../../strongs/h/h3190.md) in the ['ayin](../../strongs/h/h5869.md) of all the ['am](../../strongs/h/h5971.md), and also in the ['ayin](../../strongs/h/h5869.md) of [Šā'ûl](../../strongs/h/h7586.md) ['ebed](../../strongs/h/h5650.md).

<a name="1samuel_18_6"></a>1Samuel 18:6

And it came to pass as they [bow'](../../strongs/h/h935.md), when [Dāviḏ](../../strongs/h/h1732.md) was [shuwb](../../strongs/h/h7725.md) from the [nakah](../../strongs/h/h5221.md) of the [Pᵊlištî](../../strongs/h/h6430.md), that the ['ishshah](../../strongs/h/h802.md) [yāṣā'](../../strongs/h/h3318.md) of all [ʿîr](../../strongs/h/h5892.md) of [Yisra'el](../../strongs/h/h3478.md), [shiyr](../../strongs/h/h7891.md) and [mᵊḥōlâ](../../strongs/h/h4246.md), to [qārā'](../../strongs/h/h7125.md) [melek](../../strongs/h/h4428.md) [Šā'ûl](../../strongs/h/h7586.md), with [tōp̄](../../strongs/h/h8596.md), with [simchah](../../strongs/h/h8057.md), and with [šālîš](../../strongs/h/h7991.md) of musick.

<a name="1samuel_18_7"></a>1Samuel 18:7

And the ['ishshah](../../strongs/h/h802.md) ['anah](../../strongs/h/h6030.md) one another as they [śāḥaq](../../strongs/h/h7832.md), and ['āmar](../../strongs/h/h559.md), [Šā'ûl](../../strongs/h/h7586.md) hath [nakah](../../strongs/h/h5221.md) his thousands, and [Dāviḏ](../../strongs/h/h1732.md) his ten thousands.

<a name="1samuel_18_8"></a>1Samuel 18:8

And [Šā'ûl](../../strongs/h/h7586.md) was [me'od](../../strongs/h/h3966.md) [ḥārâ](../../strongs/h/h2734.md), and the [dabar](../../strongs/h/h1697.md) [yāraʿ](../../strongs/h/h3415.md) ['ayin](../../strongs/h/h5869.md) him; and he ['āmar](../../strongs/h/h559.md), They have [nathan](../../strongs/h/h5414.md) unto [Dāviḏ](../../strongs/h/h1732.md) ten thousands, and to me they have [nathan](../../strongs/h/h5414.md) but thousands: and what can he have more but the [mᵊlûḵâ](../../strongs/h/h4410.md)?

<a name="1samuel_18_9"></a>1Samuel 18:9

And [Šā'ûl](../../strongs/h/h7586.md) [ʿāvan](../../strongs/h/h5770.md) [Dāviḏ](../../strongs/h/h1732.md) from that [yowm](../../strongs/h/h3117.md) and [hāl'â](../../strongs/h/h1973.md).

<a name="1samuel_18_10"></a>1Samuel 18:10

And it came to pass on the [māḥŏrāṯ](../../strongs/h/h4283.md), that the [ra'](../../strongs/h/h7451.md) [ruwach](../../strongs/h/h7307.md) from ['Elohiym](../../strongs/h/h430.md) [tsalach](../../strongs/h/h6743.md) upon [Šā'ûl](../../strongs/h/h7586.md), and he [nāḇā'](../../strongs/h/h5012.md) in the [tavek](../../strongs/h/h8432.md) of the [bayith](../../strongs/h/h1004.md): and [Dāviḏ](../../strongs/h/h1732.md) [nāḡan](../../strongs/h/h5059.md) with his [yad](../../strongs/h/h3027.md), as at other [yowm](../../strongs/h/h3117.md): and there was a [ḥănîṯ](../../strongs/h/h2595.md) in [Šā'ûl](../../strongs/h/h7586.md) [yad](../../strongs/h/h3027.md).

<a name="1samuel_18_11"></a>1Samuel 18:11

And [Šā'ûl](../../strongs/h/h7586.md) [ṭûl](../../strongs/h/h2904.md) the [ḥănîṯ](../../strongs/h/h2595.md); for he ['āmar](../../strongs/h/h559.md), I will [nakah](../../strongs/h/h5221.md) [Dāviḏ](../../strongs/h/h1732.md) even to the [qîr](../../strongs/h/h7023.md) with it. And [Dāviḏ](../../strongs/h/h1732.md) avoided [cabab](../../strongs/h/h5437.md) of his [paniym](../../strongs/h/h6440.md) twice.

<a name="1samuel_18_12"></a>1Samuel 18:12

And [Šā'ûl](../../strongs/h/h7586.md) was [yare'](../../strongs/h/h3372.md) [paniym](../../strongs/h/h6440.md) [Dāviḏ](../../strongs/h/h1732.md), because [Yĕhovah](../../strongs/h/h3068.md) was with him, and was [cuwr](../../strongs/h/h5493.md) from [Šā'ûl](../../strongs/h/h7586.md).

<a name="1samuel_18_13"></a>1Samuel 18:13

Therefore [Šā'ûl](../../strongs/h/h7586.md) [cuwr](../../strongs/h/h5493.md) him from him, and [śûm](../../strongs/h/h7760.md) him his [śar](../../strongs/h/h8269.md) over a thousand; and he [yāṣā'](../../strongs/h/h3318.md) and [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) the ['am](../../strongs/h/h5971.md).

<a name="1samuel_18_14"></a>1Samuel 18:14

And [Dāviḏ](../../strongs/h/h1732.md) behaved himself [sakal](../../strongs/h/h7919.md) in all his [derek](../../strongs/h/h1870.md); and [Yĕhovah](../../strongs/h/h3068.md) was with him.

<a name="1samuel_18_15"></a>1Samuel 18:15

Wherefore when [Šā'ûl](../../strongs/h/h7586.md) [ra'ah](../../strongs/h/h7200.md) that he [sakal](../../strongs/h/h7919.md) himself [me'od](../../strongs/h/h3966.md) [sakal](../../strongs/h/h7919.md), he was [guwr](../../strongs/h/h1481.md) [paniym](../../strongs/h/h6440.md) him.

<a name="1samuel_18_16"></a>1Samuel 18:16

But all [Yisra'el](../../strongs/h/h3478.md) and [Yehuwdah](../../strongs/h/h3063.md) ['ahab](../../strongs/h/h157.md) [Dāviḏ](../../strongs/h/h1732.md), because he [yāṣā'](../../strongs/h/h3318.md) and [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) them.

<a name="1samuel_18_17"></a>1Samuel 18:17

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), Behold my [gadowl](../../strongs/h/h1419.md) [bath](../../strongs/h/h1323.md) [Mēraḇ](../../strongs/h/h4764.md), her will I [nathan](../../strongs/h/h5414.md) thee to ['ishshah](../../strongs/h/h802.md): only be thou [ben](../../strongs/h/h1121.md) [ḥayil](../../strongs/h/h2428.md) for me, and [lāḥam](../../strongs/h/h3898.md) [Yĕhovah](../../strongs/h/h3068.md) [milḥāmâ](../../strongs/h/h4421.md). For [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), Let not mine [yad](../../strongs/h/h3027.md) be upon him, but let the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md) be upon him.

<a name="1samuel_18_18"></a>1Samuel 18:18

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto [Šā'ûl](../../strongs/h/h7586.md), Who am I? and what is my [chay](../../strongs/h/h2416.md), or my ['ab](../../strongs/h/h1.md) [mišpāḥâ](../../strongs/h/h4940.md) in [Yisra'el](../../strongs/h/h3478.md), that I should be [ḥāṯān](../../strongs/h/h2860.md) to the [melek](../../strongs/h/h4428.md)?

<a name="1samuel_18_19"></a>1Samuel 18:19

But it came to pass at the [ʿēṯ](../../strongs/h/h6256.md) when [Mēraḇ](../../strongs/h/h4764.md) [Šā'ûl](../../strongs/h/h7586.md) [bath](../../strongs/h/h1323.md) should have been [nathan](../../strongs/h/h5414.md) to [Dāviḏ](../../strongs/h/h1732.md), that she was [nathan](../../strongs/h/h5414.md) unto [ʿAḏrî'Ēl](../../strongs/h/h5741.md) the [mᵊḥōlāṯî](../../strongs/h/h4259.md) to ['ishshah](../../strongs/h/h802.md).

<a name="1samuel_18_20"></a>1Samuel 18:20

And [Mîḵāl](../../strongs/h/h4324.md) [Šā'ûl](../../strongs/h/h7586.md) [bath](../../strongs/h/h1323.md) ['ahab](../../strongs/h/h157.md) [Dāviḏ](../../strongs/h/h1732.md): and they [nāḡaḏ](../../strongs/h/h5046.md) [Šā'ûl](../../strongs/h/h7586.md), and the [dabar](../../strongs/h/h1697.md) [yashar](../../strongs/h/h3474.md) ['ayin](../../strongs/h/h5869.md) him.

<a name="1samuel_18_21"></a>1Samuel 18:21

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), I will [nathan](../../strongs/h/h5414.md) him her, that she may be a [mowqesh](../../strongs/h/h4170.md) to him, and that the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md) may be against him. Wherefore [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), Thou shalt this [yowm](../../strongs/h/h3117.md) be my son in [ḥāṯan](../../strongs/h/h2859.md) in the one of the twain.

<a name="1samuel_18_22"></a>1Samuel 18:22

And [Šā'ûl](../../strongs/h/h7586.md) [tsavah](../../strongs/h/h6680.md) his ['ebed](../../strongs/h/h5650.md), saying, [dabar](../../strongs/h/h1696.md) with [Dāviḏ](../../strongs/h/h1732.md) [lāṭ](../../strongs/h/h3909.md), and ['āmar](../../strongs/h/h559.md), Behold, the [melek](../../strongs/h/h4428.md) hath [ḥāp̄ēṣ](../../strongs/h/h2654.md) in thee, and all his ['ebed](../../strongs/h/h5650.md) ['ahab](../../strongs/h/h157.md) thee: now therefore be the [melek](../../strongs/h/h4428.md) son in [ḥāṯan](../../strongs/h/h2859.md).

<a name="1samuel_18_23"></a>1Samuel 18:23

And [Šā'ûl](../../strongs/h/h7586.md) ['ebed](../../strongs/h/h5650.md) [dabar](../../strongs/h/h1696.md) those [dabar](../../strongs/h/h1697.md) in the ['ozen](../../strongs/h/h241.md) of [Dāviḏ](../../strongs/h/h1732.md). And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), ['ayin](../../strongs/h/h5869.md) it to you a [qālal](../../strongs/h/h7043.md) thing to be a [melek](../../strongs/h/h4428.md) son in [ḥāṯan](../../strongs/h/h2859.md), seeing that I am a [rûš](../../strongs/h/h7326.md) ['iysh](../../strongs/h/h376.md), and lightly [qālâ](../../strongs/h/h7034.md)?

<a name="1samuel_18_24"></a>1Samuel 18:24

And the ['ebed](../../strongs/h/h5650.md) of [Šā'ûl](../../strongs/h/h7586.md) [nāḡaḏ](../../strongs/h/h5046.md) him, ['āmar](../../strongs/h/h559.md), On this [dabar](../../strongs/h/h1697.md) [dabar](../../strongs/h/h1696.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="1samuel_18_25"></a>1Samuel 18:25

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), Thus shall ye ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), The [melek](../../strongs/h/h4428.md) [chephets](../../strongs/h/h2656.md) not any [mōhar](../../strongs/h/h4119.md), but an hundred [ʿārlâ](../../strongs/h/h6190.md) of the [Pᵊlištî](../../strongs/h/h6430.md), to be [naqam](../../strongs/h/h5358.md) of the [melek](../../strongs/h/h4428.md) ['oyeb](../../strongs/h/h341.md). But [Šā'ûl](../../strongs/h/h7586.md) [chashab](../../strongs/h/h2803.md) to make [Dāviḏ](../../strongs/h/h1732.md) [naphal](../../strongs/h/h5307.md) by the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1samuel_18_26"></a>1Samuel 18:26

And when his ['ebed](../../strongs/h/h5650.md) [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md) these [dabar](../../strongs/h/h1697.md), it [yashar](../../strongs/h/h3474.md) [dabar](../../strongs/h/h1697.md) ['ayin](../../strongs/h/h5869.md) [Dāviḏ](../../strongs/h/h1732.md) well to be the [melek](../../strongs/h/h4428.md) son in [ḥāṯan](../../strongs/h/h2859.md): and the [yowm](../../strongs/h/h3117.md) were not [mālā'](../../strongs/h/h4390.md).

<a name="1samuel_18_27"></a>1Samuel 18:27

Wherefore [Dāviḏ](../../strongs/h/h1732.md) [quwm](../../strongs/h/h6965.md) and [yālaḵ](../../strongs/h/h3212.md), he and his ['enowsh](../../strongs/h/h582.md), and [nakah](../../strongs/h/h5221.md) of the [Pᵊlištî](../../strongs/h/h6430.md) two hundred ['iysh](../../strongs/h/h376.md); and [Dāviḏ](../../strongs/h/h1732.md) [bow'](../../strongs/h/h935.md) their [ʿārlâ](../../strongs/h/h6190.md), and they gave them in full [mālā'](../../strongs/h/h4390.md) to the [melek](../../strongs/h/h4428.md), that he might be the [melek](../../strongs/h/h4428.md) [ḥāṯan](../../strongs/h/h2859.md). And [Šā'ûl](../../strongs/h/h7586.md) [nathan](../../strongs/h/h5414.md) him [Mîḵāl](../../strongs/h/h4324.md) his [bath](../../strongs/h/h1323.md) to ['ishshah](../../strongs/h/h802.md).

<a name="1samuel_18_28"></a>1Samuel 18:28

And [Šā'ûl](../../strongs/h/h7586.md) [ra'ah](../../strongs/h/h7200.md) and [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) was with [Dāviḏ](../../strongs/h/h1732.md), and that [Mîḵāl](../../strongs/h/h4324.md) [Šā'ûl](../../strongs/h/h7586.md) [bath](../../strongs/h/h1323.md) ['ahab](../../strongs/h/h157.md) him.

<a name="1samuel_18_29"></a>1Samuel 18:29

And [Šā'ûl](../../strongs/h/h7586.md) was yet the more [yare'](../../strongs/h/h3372.md) [paniym](../../strongs/h/h6440.md) [Dāviḏ](../../strongs/h/h1732.md); and [Šā'ûl](../../strongs/h/h7586.md) became [Dāviḏ](../../strongs/h/h1732.md) ['oyeb](../../strongs/h/h341.md) [yowm](../../strongs/h/h3117.md).

<a name="1samuel_18_30"></a>1Samuel 18:30

Then the [śar](../../strongs/h/h8269.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [yāṣā'](../../strongs/h/h3318.md): and it came to pass, [day](../../strongs/h/h1767.md) they [yāṣā'](../../strongs/h/h3318.md), that [Dāviḏ](../../strongs/h/h1732.md) behaved himself more [sakal](../../strongs/h/h7919.md) than all the ['ebed](../../strongs/h/h5650.md) of [Šā'ûl](../../strongs/h/h7586.md); so that his [shem](../../strongs/h/h8034.md) was [me'od](../../strongs/h/h3966.md) set [yāqar](../../strongs/h/h3365.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 17](1samuel_17.md) - [1Samuel 19](1samuel_19.md)