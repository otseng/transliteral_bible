# [1Samuel 17](https://www.blueletterbible.org/kjv/1samuel/17)

<a name="1samuel_17_1"></a>1Samuel 17:1

Now the [Pᵊlištî](../../strongs/h/h6430.md) ['āsap̄](../../strongs/h/h622.md) their [maḥănê](../../strongs/h/h4264.md) to [milḥāmâ](../../strongs/h/h4421.md), and were ['āsap̄](../../strongs/h/h622.md) at [Śôḵô](../../strongs/h/h7755.md), which belongeth to [Yehuwdah](../../strongs/h/h3063.md), and [ḥānâ](../../strongs/h/h2583.md) between [Śôḵô](../../strongs/h/h7755.md) and [ʿĂzēqâ](../../strongs/h/h5825.md), in ['Ep̄Es Dammîm](../../strongs/h/h658.md).

<a name="1samuel_17_2"></a>1Samuel 17:2

And [Šā'ûl](../../strongs/h/h7586.md) and the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) were ['āsap̄](../../strongs/h/h622.md), and [ḥānâ](../../strongs/h/h2583.md) by the [ʿēmeq](../../strongs/h/h6010.md) of ['Ēlâ](../../strongs/h/h425.md), and set the [milḥāmâ](../../strongs/h/h4421.md) in ['arak](../../strongs/h/h6186.md) [qārā'](../../strongs/h/h7125.md) the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1samuel_17_3"></a>1Samuel 17:3

And the [Pᵊlištî](../../strongs/h/h6430.md) ['amad](../../strongs/h/h5975.md) on a [har](../../strongs/h/h2022.md) on the one side, and [Yisra'el](../../strongs/h/h3478.md) ['amad](../../strongs/h/h5975.md) on a [har](../../strongs/h/h2022.md) on the other side: and there was a [gay'](../../strongs/h/h1516.md) between them.

<a name="1samuel_17_4"></a>1Samuel 17:4

And there [yāṣā'](../../strongs/h/h3318.md) an ['iysh](../../strongs/h/h376.md) [bēnayim](../../strongs/h/h1143.md) out of the [maḥănê](../../strongs/h/h4264.md) of the [Pᵊlištî](../../strongs/h/h6430.md), [shem](../../strongs/h/h8034.md) [Gālyaṯ](../../strongs/h/h1555.md), of [Gaṯ](../../strongs/h/h1661.md), whose [gobahh](../../strongs/h/h1363.md) was six ['ammâ](../../strongs/h/h520.md) and a [zereṯ](../../strongs/h/h2239.md).

<a name="1samuel_17_5"></a>1Samuel 17:5

And he had a [kôḇaʿ](../../strongs/h/h3553.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) upon his [ro'sh](../../strongs/h/h7218.md), and he was [labash](../../strongs/h/h3847.md) with a [širyôn](../../strongs/h/h8302.md) of [qaśqeśeṯ](../../strongs/h/h7193.md); and the [mišqāl](../../strongs/h/h4948.md) of the [širyôn](../../strongs/h/h8302.md) was five thousand [šeqel](../../strongs/h/h8255.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="1samuel_17_6"></a>1Samuel 17:6

And he had [miṣḥâ](../../strongs/h/h4697.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) upon his [regel](../../strongs/h/h7272.md), and a [kîḏôn](../../strongs/h/h3591.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) between his [kāṯēp̄](../../strongs/h/h3802.md).

<a name="1samuel_17_7"></a>1Samuel 17:7

And the ['ets](../../strongs/h/h6086.md) [chets](../../strongs/h/h2671.md) of his [ḥănîṯ](../../strongs/h/h2595.md) was like an ['āraḡ](../../strongs/h/h707.md) [mānôr](../../strongs/h/h4500.md); and his [ḥănîṯ](../../strongs/h/h2595.md) [lehāḇâ](../../strongs/h/h3852.md) weighed six hundred [šeqel](../../strongs/h/h8255.md) of [barzel](../../strongs/h/h1270.md): and one [nasa'](../../strongs/h/h5375.md) a [tsinnah](../../strongs/h/h6793.md) [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) him.

<a name="1samuel_17_8"></a>1Samuel 17:8

And he ['amad](../../strongs/h/h5975.md) and [qara'](../../strongs/h/h7121.md) unto the [maʿărāḵâ](../../strongs/h/h4634.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, Why are ye [yāṣā'](../../strongs/h/h3318.md) to set your [milḥāmâ](../../strongs/h/h4421.md) in ['arak](../../strongs/h/h6186.md)? am not I a [Pᵊlištî](../../strongs/h/h6430.md), and ye ['ebed](../../strongs/h/h5650.md) to [Šā'ûl](../../strongs/h/h7586.md)? [bārâ](../../strongs/h/h1262.md) you an ['iysh](../../strongs/h/h376.md) for you, and let him [yarad](../../strongs/h/h3381.md) to me.

<a name="1samuel_17_9"></a>1Samuel 17:9

If he be [yakol](../../strongs/h/h3201.md) to [lāḥam](../../strongs/h/h3898.md) with me, and to [nakah](../../strongs/h/h5221.md) me, then will we be your ['ebed](../../strongs/h/h5650.md): but if I [yakol](../../strongs/h/h3201.md) against him, and [nakah](../../strongs/h/h5221.md) him, then shall ye be our ['ebed](../../strongs/h/h5650.md), and ['abad](../../strongs/h/h5647.md) us.

<a name="1samuel_17_10"></a>1Samuel 17:10

And the [Pᵊlištî](../../strongs/h/h6430.md) ['āmar](../../strongs/h/h559.md), I [ḥārap̄](../../strongs/h/h2778.md) the [maʿărāḵâ](../../strongs/h/h4634.md) of [Yisra'el](../../strongs/h/h3478.md) this [yowm](../../strongs/h/h3117.md); [nathan](../../strongs/h/h5414.md) me an ['iysh](../../strongs/h/h376.md), that we may [lāḥam](../../strongs/h/h3898.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="1samuel_17_11"></a>1Samuel 17:11

When [Šā'ûl](../../strongs/h/h7586.md) and all [Yisra'el](../../strongs/h/h3478.md) [shama'](../../strongs/h/h8085.md) those [dabar](../../strongs/h/h1697.md) of the [Pᵊlištî](../../strongs/h/h6430.md), they were [ḥāṯaṯ](../../strongs/h/h2865.md), and [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md).

<a name="1samuel_17_12"></a>1Samuel 17:12

Now [Dāviḏ](../../strongs/h/h1732.md) was the [ben](../../strongs/h/h1121.md) of that ['iysh](../../strongs/h/h376.md) ['ep̄rāṯî](../../strongs/h/h673.md) of [Bêṯ leḥem](../../strongs/h/h1035.md) [Yehuwdah](../../strongs/h/h3063.md), whose [shem](../../strongs/h/h8034.md) was [Yišay](../../strongs/h/h3448.md); and he had eight [ben](../../strongs/h/h1121.md): and the ['iysh](../../strongs/h/h376.md) [bow'](../../strongs/h/h935.md) among ['enowsh](../../strongs/h/h582.md) for a [zāqēn](../../strongs/h/h2204.md) in the [yowm](../../strongs/h/h3117.md) of [Šā'ûl](../../strongs/h/h7586.md).

<a name="1samuel_17_13"></a>1Samuel 17:13

And the three [gadowl](../../strongs/h/h1419.md) [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md) [yālaḵ](../../strongs/h/h3212.md) and ['aḥar](../../strongs/h/h310.md) [Šā'ûl](../../strongs/h/h7586.md) to the [milḥāmâ](../../strongs/h/h4421.md): and the [shem](../../strongs/h/h8034.md) of his three [ben](../../strongs/h/h1121.md) that [halak](../../strongs/h/h1980.md) to the [milḥāmâ](../../strongs/h/h4421.md) were ['Ĕlî'āḇ](../../strongs/h/h446.md) the [bᵊḵôr](../../strongs/h/h1060.md), and [mišnê](../../strongs/h/h4932.md) unto him ['Ăḇînāḏāḇ](../../strongs/h/h41.md), and the third [Šammâ](../../strongs/h/h8048.md).

<a name="1samuel_17_14"></a>1Samuel 17:14

And [Dāviḏ](../../strongs/h/h1732.md) was the [qāṭān](../../strongs/h/h6996.md): and the three [gadowl](../../strongs/h/h1419.md) [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) [Šā'ûl](../../strongs/h/h7586.md).

<a name="1samuel_17_15"></a>1Samuel 17:15

But [Dāviḏ](../../strongs/h/h1732.md) [halak](../../strongs/h/h1980.md) and [shuwb](../../strongs/h/h7725.md) from [Šā'ûl](../../strongs/h/h7586.md) to [ra'ah](../../strongs/h/h7462.md) his ['ab](../../strongs/h/h1.md) [tso'n](../../strongs/h/h6629.md) at [Bêṯ leḥem](../../strongs/h/h1035.md).

<a name="1samuel_17_16"></a>1Samuel 17:16

And the [Pᵊlištî](../../strongs/h/h6430.md) [nāḡaš](../../strongs/h/h5066.md) [šāḵam](../../strongs/h/h7925.md) and [ʿāraḇ](../../strongs/h/h6150.md), and [yatsab](../../strongs/h/h3320.md) himself forty [yowm](../../strongs/h/h3117.md).

<a name="1samuel_17_17"></a>1Samuel 17:17

And [Yišay](../../strongs/h/h3448.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md) his [ben](../../strongs/h/h1121.md), [laqach](../../strongs/h/h3947.md) now for thy ['ach](../../strongs/h/h251.md) an ['êp̄â](../../strongs/h/h374.md) of this [qālî](../../strongs/h/h7039.md), and these ten [lechem](../../strongs/h/h3899.md), and [rûṣ](../../strongs/h/h7323.md) to the [maḥănê](../../strongs/h/h4264.md) to thy ['ach](../../strongs/h/h251.md);

<a name="1samuel_17_18"></a>1Samuel 17:18

And [bow'](../../strongs/h/h935.md) these ten [chalab](../../strongs/h/h2461.md) [ḥārîṣ](../../strongs/h/h2757.md) unto the [śar](../../strongs/h/h8269.md) of their thousand, and [paqad](../../strongs/h/h6485.md) how thy ['ach](../../strongs/h/h251.md) [shalowm](../../strongs/h/h7965.md), and [laqach](../../strongs/h/h3947.md) their [ʿărubâ](../../strongs/h/h6161.md).

<a name="1samuel_17_19"></a>1Samuel 17:19

Now [Šā'ûl](../../strongs/h/h7586.md), and they, and all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md), were in the [ʿēmeq](../../strongs/h/h6010.md) of ['Ēlâ](../../strongs/h/h425.md), [lāḥam](../../strongs/h/h3898.md) with the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1samuel_17_20"></a>1Samuel 17:20

And [Dāviḏ](../../strongs/h/h1732.md) [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [nāṭaš](../../strongs/h/h5203.md) the [tso'n](../../strongs/h/h6629.md) with a [shamar](../../strongs/h/h8104.md), and [nasa'](../../strongs/h/h5375.md), and [yālaḵ](../../strongs/h/h3212.md), as [Yišay](../../strongs/h/h3448.md) had [tsavah](../../strongs/h/h6680.md) him; and he [bow'](../../strongs/h/h935.md) to the [ma'gal](../../strongs/h/h4570.md), as the [ḥayil](../../strongs/h/h2428.md) was [yāṣā'](../../strongs/h/h3318.md) to the [maʿărāḵâ](../../strongs/h/h4634.md), and [rûaʿ](../../strongs/h/h7321.md) for the [milḥāmâ](../../strongs/h/h4421.md).

<a name="1samuel_17_21"></a>1Samuel 17:21

For [Yisra'el](../../strongs/h/h3478.md) and the [Pᵊlištî](../../strongs/h/h6430.md) had ['arak](../../strongs/h/h6186.md), [maʿărāḵâ](../../strongs/h/h4634.md) [qārā'](../../strongs/h/h7125.md) [maʿărāḵâ](../../strongs/h/h4634.md).

<a name="1samuel_17_22"></a>1Samuel 17:22

And [Dāviḏ](../../strongs/h/h1732.md) [nāṭaš](../../strongs/h/h5203.md) his [kĕliy](../../strongs/h/h3627.md) in the [yad](../../strongs/h/h3027.md) of the [shamar](../../strongs/h/h8104.md) of the [kĕliy](../../strongs/h/h3627.md), and [rûṣ](../../strongs/h/h7323.md) into the [maʿărāḵâ](../../strongs/h/h4634.md), and [bow'](../../strongs/h/h935.md) and [sha'al](../../strongs/h/h7592.md) [shalowm](../../strongs/h/h7965.md) his ['ach](../../strongs/h/h251.md).

<a name="1samuel_17_23"></a>1Samuel 17:23

And as he [dabar](../../strongs/h/h1696.md) with them, behold, there [ʿālâ](../../strongs/h/h5927.md) the ['iysh](../../strongs/h/h376.md) H1143, the [Pᵊlištî](../../strongs/h/h6430.md) of [Gaṯ](../../strongs/h/h1661.md), [Gālyaṯ](../../strongs/h/h1555.md) by [shem](../../strongs/h/h8034.md), out of the [maʿărāḵâ](../../strongs/h/h4634.md) H4630 of the [Pᵊlištî](../../strongs/h/h6430.md), and [dabar](../../strongs/h/h1696.md) according to the same [dabar](../../strongs/h/h1697.md): and [Dāviḏ](../../strongs/h/h1732.md) [shama'](../../strongs/h/h8085.md) them.

<a name="1samuel_17_24"></a>1Samuel 17:24

And all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md), when they [ra'ah](../../strongs/h/h7200.md) the ['iysh](../../strongs/h/h376.md), [nûs](../../strongs/h/h5127.md) from [paniym](../../strongs/h/h6440.md), and were [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md).

<a name="1samuel_17_25"></a>1Samuel 17:25

And the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md), Have ye [ra'ah](../../strongs/h/h7200.md) this ['iysh](../../strongs/h/h376.md) that is [ʿālâ](../../strongs/h/h5927.md)? surely to [ḥārap̄](../../strongs/h/h2778.md) [Yisra'el](../../strongs/h/h3478.md) is he [ʿālâ](../../strongs/h/h5927.md): and it shall be, that the ['iysh](../../strongs/h/h376.md) who [nakah](../../strongs/h/h5221.md) him, the [melek](../../strongs/h/h4428.md) will [ʿāšar](../../strongs/h/h6238.md) him with [gadowl](../../strongs/h/h1419.md) [ʿōšer](../../strongs/h/h6239.md), and will [nathan](../../strongs/h/h5414.md) him his [bath](../../strongs/h/h1323.md), and ['asah](../../strongs/h/h6213.md) his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) [ḥāp̄šî](../../strongs/h/h2670.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="1samuel_17_26"></a>1Samuel 17:26

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to the ['enowsh](../../strongs/h/h582.md) that ['amad](../../strongs/h/h5975.md) by him, ['āmar](../../strongs/h/h559.md), What shall be ['asah](../../strongs/h/h6213.md) to the ['iysh](../../strongs/h/h376.md) that [nakah](../../strongs/h/h5221.md) [hallāz](../../strongs/h/h1975.md) [Pᵊlištî](../../strongs/h/h6430.md), and taketh [cuwr](../../strongs/h/h5493.md) the [cherpah](../../strongs/h/h2781.md) from [Yisra'el](../../strongs/h/h3478.md)? for who is this [ʿārēl](../../strongs/h/h6189.md) [Pᵊlištî](../../strongs/h/h6430.md), that he should [ḥārap̄](../../strongs/h/h2778.md) the [maʿărāḵâ](../../strongs/h/h4634.md) of the [chay](../../strongs/h/h2416.md) ['Elohiym](../../strongs/h/h430.md)?

<a name="1samuel_17_27"></a>1Samuel 17:27

And the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md) him after this [dabar](../../strongs/h/h1697.md), ['āmar](../../strongs/h/h559.md), So shall it be ['asah](../../strongs/h/h6213.md) to the ['iysh](../../strongs/h/h376.md) that [nakah](../../strongs/h/h5221.md) him.

<a name="1samuel_17_28"></a>1Samuel 17:28

And ['Ĕlî'āḇ](../../strongs/h/h446.md) his [gadowl](../../strongs/h/h1419.md) ['ach](../../strongs/h/h251.md) [shama'](../../strongs/h/h8085.md) when he [dabar](../../strongs/h/h1696.md) unto the ['enowsh](../../strongs/h/h582.md); and ['Ĕlî'āḇ](../../strongs/h/h446.md) ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md) against [Dāviḏ](../../strongs/h/h1732.md), and he ['āmar](../../strongs/h/h559.md), Why camest thou [yarad](../../strongs/h/h3381.md) hither? and with whom hast thou [nāṭaš](../../strongs/h/h5203.md) those [mᵊʿaṭ](../../strongs/h/h4592.md) [tso'n](../../strongs/h/h6629.md) in the [midbar](../../strongs/h/h4057.md)? I [yada'](../../strongs/h/h3045.md) thy [zāḏôn](../../strongs/h/h2087.md), and the [rōaʿ](../../strongs/h/h7455.md) of thine [lebab](../../strongs/h/h3824.md); for thou art [yarad](../../strongs/h/h3381.md) that thou mightest [ra'ah](../../strongs/h/h7200.md) the [milḥāmâ](../../strongs/h/h4421.md).

<a name="1samuel_17_29"></a>1Samuel 17:29

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), What have I now ['asah](../../strongs/h/h6213.md)? Is there not a [dabar](../../strongs/h/h1697.md)?

<a name="1samuel_17_30"></a>1Samuel 17:30

And he [cabab](../../strongs/h/h5437.md) from him [môl](../../strongs/h/h4136.md) ['aḥēr](../../strongs/h/h312.md), and ['āmar](../../strongs/h/h559.md) after the same [dabar](../../strongs/h/h1697.md): and the ['am](../../strongs/h/h5971.md) [dabar](../../strongs/h/h1697.md) him [shuwb](../../strongs/h/h7725.md) after the [ri'šôn](../../strongs/h/h7223.md) [dabar](../../strongs/h/h1697.md).

<a name="1samuel_17_31"></a>1Samuel 17:31

And when the [dabar](../../strongs/h/h1697.md) were [shama'](../../strongs/h/h8085.md) which [Dāviḏ](../../strongs/h/h1732.md) [dabar](../../strongs/h/h1696.md), they [nāḡaḏ](../../strongs/h/h5046.md) them [paniym](../../strongs/h/h6440.md) [Šā'ûl](../../strongs/h/h7586.md): and he [laqach](../../strongs/h/h3947.md) for him.

<a name="1samuel_17_32"></a>1Samuel 17:32

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to [Šā'ûl](../../strongs/h/h7586.md), Let no ['āḏām](../../strongs/h/h120.md) [leb](../../strongs/h/h3820.md) [naphal](../../strongs/h/h5307.md) because of him; thy ['ebed](../../strongs/h/h5650.md) will [yālaḵ](../../strongs/h/h3212.md) and [lāḥam](../../strongs/h/h3898.md) with this [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1samuel_17_33"></a>1Samuel 17:33

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), Thou art not [yakol](../../strongs/h/h3201.md) to [yālaḵ](../../strongs/h/h3212.md) against this [Pᵊlištî](../../strongs/h/h6430.md) to [lāḥam](../../strongs/h/h3898.md) with him: for thou art but a [naʿar](../../strongs/h/h5288.md), and he an ['iysh](../../strongs/h/h376.md) of [milḥāmâ](../../strongs/h/h4421.md) from his [nāʿur](../../strongs/h/h5271.md).

<a name="1samuel_17_34"></a>1Samuel 17:34

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto [Šā'ûl](../../strongs/h/h7586.md), Thy ['ebed](../../strongs/h/h5650.md) [ra'ah](../../strongs/h/h7462.md) his ['ab](../../strongs/h/h1.md) [tso'n](../../strongs/h/h6629.md), and there [bow'](../../strongs/h/h935.md) an ['ariy](../../strongs/h/h738.md), and a [dōḇ](../../strongs/h/h1677.md), and [nasa'](../../strongs/h/h5375.md) a [śê](../../strongs/h/h7716.md) [zê](../../strongs/h/h2089.md) out of the [ʿēḏer](../../strongs/h/h5739.md):

<a name="1samuel_17_35"></a>1Samuel 17:35

And I [yāṣā'](../../strongs/h/h3318.md) ['aḥar](../../strongs/h/h310.md) him, and [nakah](../../strongs/h/h5221.md) him, and [natsal](../../strongs/h/h5337.md) it out of his [peh](../../strongs/h/h6310.md): and when he [quwm](../../strongs/h/h6965.md) against me, I [ḥāzaq](../../strongs/h/h2388.md) him by his [zāqān](../../strongs/h/h2206.md), and [nakah](../../strongs/h/h5221.md) him, and [muwth](../../strongs/h/h4191.md) him.

<a name="1samuel_17_36"></a>1Samuel 17:36

Thy ['ebed](../../strongs/h/h5650.md) [nakah](../../strongs/h/h5221.md) both the ['ariy](../../strongs/h/h738.md) and the [dōḇ](../../strongs/h/h1677.md): and this [ʿārēl](../../strongs/h/h6189.md) [Pᵊlištî](../../strongs/h/h6430.md) shall be as one of them, seeing he hath [ḥārap̄](../../strongs/h/h2778.md) the [maʿărāḵâ](../../strongs/h/h4634.md) of the [chay](../../strongs/h/h2416.md) ['Elohiym](../../strongs/h/h430.md).

<a name="1samuel_17_37"></a>1Samuel 17:37

[Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) moreover, [Yĕhovah](../../strongs/h/h3068.md) that [natsal](../../strongs/h/h5337.md) me out of the [yad](../../strongs/h/h3027.md) of the ['ariy](../../strongs/h/h738.md), and out of the [yad](../../strongs/h/h3027.md) of the [dōḇ](../../strongs/h/h1677.md), he will [natsal](../../strongs/h/h5337.md) me out of the [yad](../../strongs/h/h3027.md) of this [Pᵊlištî](../../strongs/h/h6430.md). And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), [yālaḵ](../../strongs/h/h3212.md), and [Yĕhovah](../../strongs/h/h3068.md) be with thee.

<a name="1samuel_17_38"></a>1Samuel 17:38

And [Šā'ûl](../../strongs/h/h7586.md) [labash](../../strongs/h/h3847.md) [Dāviḏ](../../strongs/h/h1732.md) with his [maḏ](../../strongs/h/h4055.md), and he [nathan](../../strongs/h/h5414.md) a [qôḇaʿ](../../strongs/h/h6959.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) upon his [ro'sh](../../strongs/h/h7218.md); also he [labash](../../strongs/h/h3847.md) him with a [širyôn](../../strongs/h/h8302.md).

<a name="1samuel_17_39"></a>1Samuel 17:39

And [Dāviḏ](../../strongs/h/h1732.md) [ḥāḡar](../../strongs/h/h2296.md) his [chereb](../../strongs/h/h2719.md) upon his [maḏ](../../strongs/h/h4055.md), and he [yā'al](../../strongs/h/h2974.md) to [yālaḵ](../../strongs/h/h3212.md); for he had not [nāsâ](../../strongs/h/h5254.md) it. And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto [Šā'ûl](../../strongs/h/h7586.md), I [yakol](../../strongs/h/h3201.md) [yālaḵ](../../strongs/h/h3212.md) with these; for I have not [nāsâ](../../strongs/h/h5254.md) them. And [Dāviḏ](../../strongs/h/h1732.md) [cuwr](../../strongs/h/h5493.md) them off him.

<a name="1samuel_17_40"></a>1Samuel 17:40

And he [laqach](../../strongs/h/h3947.md) his [maqqēl](../../strongs/h/h4731.md) in his [yad](../../strongs/h/h3027.md), and [bāḥar](../../strongs/h/h977.md) him five [ḥalluq](../../strongs/h/h2512.md) ['eben](../../strongs/h/h68.md) out of the [nachal](../../strongs/h/h5158.md), and [śûm](../../strongs/h/h7760.md) them in a [ra'ah](../../strongs/h/h7462.md) [kĕliy](../../strongs/h/h3627.md) which he had, even in a [yalqûṭ](../../strongs/h/h3219.md); and his [qelaʿ](../../strongs/h/h7050.md) was in his [yad](../../strongs/h/h3027.md): and he [nāḡaš](../../strongs/h/h5066.md) to the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1samuel_17_41"></a>1Samuel 17:41

And the [Pᵊlištî](../../strongs/h/h6430.md) [yālaḵ](../../strongs/h/h3212.md) [halak](../../strongs/h/h1980.md) and [qārēḇ](../../strongs/h/h7131.md) unto [Dāviḏ](../../strongs/h/h1732.md); and the ['iysh](../../strongs/h/h376.md) that [nasa'](../../strongs/h/h5375.md) the [tsinnah](../../strongs/h/h6793.md) went [paniym](../../strongs/h/h6440.md) him.

<a name="1samuel_17_42"></a>1Samuel 17:42

And when the [Pᵊlištî](../../strongs/h/h6430.md) [nabat](../../strongs/h/h5027.md), and [ra'ah](../../strongs/h/h7200.md) [Dāviḏ](../../strongs/h/h1732.md), he [bazah](../../strongs/h/h959.md) him: for he was but a [naʿar](../../strongs/h/h5288.md), and ['aḏmōnî](../../strongs/h/h132.md), and of a [yāp̄ê](../../strongs/h/h3303.md) [mar'ê](../../strongs/h/h4758.md).

<a name="1samuel_17_43"></a>1Samuel 17:43

And the [Pᵊlištî](../../strongs/h/h6430.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), Am I a [keleḇ](../../strongs/h/h3611.md), that thou [bow'](../../strongs/h/h935.md) to me with [maqqēl](../../strongs/h/h4731.md)? And the [Pᵊlištî](../../strongs/h/h6430.md) [qālal](../../strongs/h/h7043.md) [Dāviḏ](../../strongs/h/h1732.md) by his ['Elohiym](../../strongs/h/h430.md).

<a name="1samuel_17_44"></a>1Samuel 17:44

And the [Pᵊlištî](../../strongs/h/h6430.md) ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), [yālaḵ](../../strongs/h/h3212.md) to me, and I will [nathan](../../strongs/h/h5414.md) thy [basar](../../strongs/h/h1320.md) unto the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and to the [bĕhemah](../../strongs/h/h929.md) of the [sadeh](../../strongs/h/h7704.md).

<a name="1samuel_17_45"></a>1Samuel 17:45

Then ['āmar](../../strongs/h/h559.md) [Dāviḏ](../../strongs/h/h1732.md) to the [Pᵊlištî](../../strongs/h/h6430.md), Thou [bow'](../../strongs/h/h935.md) to me with a [chereb](../../strongs/h/h2719.md), and with a [ḥănîṯ](../../strongs/h/h2595.md), and with a [kîḏôn](../../strongs/h/h3591.md): but I [bow'](../../strongs/h/h935.md) to thee in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of the [maʿărāḵâ](../../strongs/h/h4634.md) of [Yisra'el](../../strongs/h/h3478.md), whom thou hast [ḥārap̄](../../strongs/h/h2778.md).

<a name="1samuel_17_46"></a>1Samuel 17:46

This [yowm](../../strongs/h/h3117.md) will [Yĕhovah](../../strongs/h/h3068.md) [cagar](../../strongs/h/h5462.md) thee into mine [yad](../../strongs/h/h3027.md); and I will [nakah](../../strongs/h/h5221.md) thee, and [cuwr](../../strongs/h/h5493.md) thine [ro'sh](../../strongs/h/h7218.md) from thee; and I will [nathan](../../strongs/h/h5414.md) the [peḡer](../../strongs/h/h6297.md) of the [maḥănê](../../strongs/h/h4264.md) of the [Pᵊlištî](../../strongs/h/h6430.md) this [yowm](../../strongs/h/h3117.md) unto the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and to the [chay](../../strongs/h/h2416.md) of the ['erets](../../strongs/h/h776.md); that all the ['erets](../../strongs/h/h776.md) may [yada'](../../strongs/h/h3045.md) that there is an ['Elohiym](../../strongs/h/h430.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="1samuel_17_47"></a>1Samuel 17:47

And all this [qāhēl](../../strongs/h/h6951.md) shall [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) [yasha'](../../strongs/h/h3467.md) not with [chereb](../../strongs/h/h2719.md) and [ḥănîṯ](../../strongs/h/h2595.md): for the [milḥāmâ](../../strongs/h/h4421.md) is [Yĕhovah](../../strongs/h/h3068.md), and he will [nathan](../../strongs/h/h5414.md) you into our [yad](../../strongs/h/h3027.md).

<a name="1samuel_17_48"></a>1Samuel 17:48

And it came to pass, when the [Pᵊlištî](../../strongs/h/h6430.md) [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) and [qāraḇ](../../strongs/h/h7126.md) to [qārā'](../../strongs/h/h7125.md) [Dāviḏ](../../strongs/h/h1732.md), that [Dāviḏ](../../strongs/h/h1732.md) [māhar](../../strongs/h/h4116.md), and [rûṣ](../../strongs/h/h7323.md) toward the [maʿărāḵâ](../../strongs/h/h4634.md) to [qārā'](../../strongs/h/h7125.md) the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1samuel_17_49"></a>1Samuel 17:49

And [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) in his [kĕliy](../../strongs/h/h3627.md), and [laqach](../../strongs/h/h3947.md) thence an ['eben](../../strongs/h/h68.md), and [qālaʿ](../../strongs/h/h7049.md) it, and [nakah](../../strongs/h/h5221.md) the [Pᵊlištî](../../strongs/h/h6430.md) in his [mēṣaḥ](../../strongs/h/h4696.md), that the ['eben](../../strongs/h/h68.md) [ṭāḇaʿ](../../strongs/h/h2883.md) into his [mēṣaḥ](../../strongs/h/h4696.md); and he [naphal](../../strongs/h/h5307.md) upon his [paniym](../../strongs/h/h6440.md) to the ['erets](../../strongs/h/h776.md).

<a name="1samuel_17_50"></a>1Samuel 17:50

So [Dāviḏ](../../strongs/h/h1732.md) [ḥāzaq](../../strongs/h/h2388.md) over the [Pᵊlištî](../../strongs/h/h6430.md) with a [qelaʿ](../../strongs/h/h7050.md) and with an ['eben](../../strongs/h/h68.md), and [nakah](../../strongs/h/h5221.md) the [Pᵊlištî](../../strongs/h/h6430.md), and [muwth](../../strongs/h/h4191.md) him; but there was no [chereb](../../strongs/h/h2719.md) in the [yad](../../strongs/h/h3027.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="1samuel_17_51"></a>1Samuel 17:51

Therefore [Dāviḏ](../../strongs/h/h1732.md) [rûṣ](../../strongs/h/h7323.md), and ['amad](../../strongs/h/h5975.md) upon the [Pᵊlištî](../../strongs/h/h6430.md), and [laqach](../../strongs/h/h3947.md) his [chereb](../../strongs/h/h2719.md), and [šālap̄](../../strongs/h/h8025.md) it out of the [taʿar](../../strongs/h/h8593.md) thereof, and [muwth](../../strongs/h/h4191.md) him, and [karath](../../strongs/h/h3772.md) his [ro'sh](../../strongs/h/h7218.md) therewith. And when the [Pᵊlištî](../../strongs/h/h6430.md) [ra'ah](../../strongs/h/h7200.md) their [gibôr](../../strongs/h/h1368.md) was [muwth](../../strongs/h/h4191.md), they [nûs](../../strongs/h/h5127.md).

<a name="1samuel_17_52"></a>1Samuel 17:52

And the ['enowsh](../../strongs/h/h582.md) of [Yisra'el](../../strongs/h/h3478.md) and of [Yehuwdah](../../strongs/h/h3063.md) [quwm](../../strongs/h/h6965.md), and [rûaʿ](../../strongs/h/h7321.md), and [radaph](../../strongs/h/h7291.md) the [Pᵊlištî](../../strongs/h/h6430.md), until thou [bow'](../../strongs/h/h935.md) to the [gay'](../../strongs/h/h1516.md), and to the [sha'ar](../../strongs/h/h8179.md) of [ʿEqrôn](../../strongs/h/h6138.md). And the [ḥālāl](../../strongs/h/h2491.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [naphal](../../strongs/h/h5307.md) by the [derek](../../strongs/h/h1870.md) to [ŠaʿĂrayim](../../strongs/h/h8189.md), even unto [Gaṯ](../../strongs/h/h1661.md), and unto [ʿEqrôn](../../strongs/h/h6138.md).

<a name="1samuel_17_53"></a>1Samuel 17:53

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shuwb](../../strongs/h/h7725.md) from [dalaq](../../strongs/h/h1814.md) ['aḥar](../../strongs/h/h310.md) the [Pᵊlištî](../../strongs/h/h6430.md), and they [šāsas](../../strongs/h/h8155.md) their [maḥănê](../../strongs/h/h4264.md).

<a name="1samuel_17_54"></a>1Samuel 17:54

And [Dāviḏ](../../strongs/h/h1732.md) [laqach](../../strongs/h/h3947.md) the [ro'sh](../../strongs/h/h7218.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and [bow'](../../strongs/h/h935.md) it to [Yĕruwshalaim](../../strongs/h/h3389.md); but he [śûm](../../strongs/h/h7760.md) his [kĕliy](../../strongs/h/h3627.md) in his ['ohel](../../strongs/h/h168.md).

<a name="1samuel_17_55"></a>1Samuel 17:55

And when [Šā'ûl](../../strongs/h/h7586.md) [ra'ah](../../strongs/h/h7200.md) [Dāviḏ](../../strongs/h/h1732.md) [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) the [Pᵊlištî](../../strongs/h/h6430.md), he ['āmar](../../strongs/h/h559.md) unto ['Aḇnēr](../../strongs/h/h74.md), the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md), ['Aḇnēr](../../strongs/h/h74.md), whose [ben](../../strongs/h/h1121.md) is this [naʿar](../../strongs/h/h5288.md)? And ['Aḇnēr](../../strongs/h/h74.md) ['āmar](../../strongs/h/h559.md), As thy [nephesh](../../strongs/h/h5315.md) [chay](../../strongs/h/h2416.md), O [melek](../../strongs/h/h4428.md), I cannot [yada'](../../strongs/h/h3045.md).

<a name="1samuel_17_56"></a>1Samuel 17:56

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), [sha'al](../../strongs/h/h7592.md) thou whose [ben](../../strongs/h/h1121.md) the [ʿelem](../../strongs/h/h5958.md) is.

<a name="1samuel_17_57"></a>1Samuel 17:57

And as [Dāviḏ](../../strongs/h/h1732.md) [shuwb](../../strongs/h/h7725.md) from the [nakah](../../strongs/h/h5221.md) of the [Pᵊlištî](../../strongs/h/h6430.md), ['Aḇnēr](../../strongs/h/h74.md) [laqach](../../strongs/h/h3947.md) him, and [bow'](../../strongs/h/h935.md) him [paniym](../../strongs/h/h6440.md) [Šā'ûl](../../strongs/h/h7586.md) with the [ro'sh](../../strongs/h/h7218.md) of the [Pᵊlištî](../../strongs/h/h6430.md) in his [yad](../../strongs/h/h3027.md).

<a name="1samuel_17_58"></a>1Samuel 17:58

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) to him, Whose [ben](../../strongs/h/h1121.md) art thou, thou [naʿar](../../strongs/h/h5288.md)? And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), I am the [ben](../../strongs/h/h1121.md) of thy ['ebed](../../strongs/h/h5650.md) [Yišay](../../strongs/h/h3448.md) the [bêṯ-hallaḥmî](../../strongs/h/h1022.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 16](1samuel_16.md) - [1Samuel 18](1samuel_18.md)