# [1Samuel 4](https://www.blueletterbible.org/kjv/1samuel/4)

<a name="1samuel_4_1"></a>1Samuel 4:1

And the [dabar](../../strongs/h/h1697.md) of [Šᵊmû'Ēl](../../strongs/h/h8050.md) came to all [Yisra'el](../../strongs/h/h3478.md). Now [Yisra'el](../../strongs/h/h3478.md) [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) the [Pᵊlištî](../../strongs/h/h6430.md) to [milḥāmâ](../../strongs/h/h4421.md), and [ḥānâ](../../strongs/h/h2583.md) beside ['eḇen hāʿezer](../../strongs/h/h72.md): and the [Pᵊlištî](../../strongs/h/h6430.md) [ḥānâ](../../strongs/h/h2583.md) in ['Ăp̄Ēq](../../strongs/h/h663.md).

<a name="1samuel_4_2"></a>1Samuel 4:2

And the [Pᵊlištî](../../strongs/h/h6430.md) put themselves in ['arak](../../strongs/h/h6186.md) [qārā'](../../strongs/h/h7125.md) [Yisra'el](../../strongs/h/h3478.md): and when they [nāṭaš](../../strongs/h/h5203.md) [milḥāmâ](../../strongs/h/h4421.md), [Yisra'el](../../strongs/h/h3478.md) was [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) the [Pᵊlištî](../../strongs/h/h6430.md): and they [nakah](../../strongs/h/h5221.md) of the [maʿărāḵâ](../../strongs/h/h4634.md) in the [sadeh](../../strongs/h/h7704.md) about four thousand ['iysh](../../strongs/h/h376.md).

<a name="1samuel_4_3"></a>1Samuel 4:3

And when the ['am](../../strongs/h/h5971.md) were [bow'](../../strongs/h/h935.md) into the [maḥănê](../../strongs/h/h4264.md), the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md), Wherefore hath [Yĕhovah](../../strongs/h/h3068.md) [nāḡap̄](../../strongs/h/h5062.md) us to [yowm](../../strongs/h/h3117.md) [paniym](../../strongs/h/h6440.md) the [Pᵊlištî](../../strongs/h/h6430.md)? Let us [laqach](../../strongs/h/h3947.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) out of [Šîlô](../../strongs/h/h7887.md) unto us, that, when it [bow'](../../strongs/h/h935.md) [qereḇ](../../strongs/h/h7130.md) us, it may [yasha'](../../strongs/h/h3467.md) us out of the [kaph](../../strongs/h/h3709.md) of our ['oyeb](../../strongs/h/h341.md).

<a name="1samuel_4_4"></a>1Samuel 4:4

So the ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md) to [Šîlô](../../strongs/h/h7887.md), that they might [nasa'](../../strongs/h/h5375.md) from thence the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), which [yashab](../../strongs/h/h3427.md) between the [kĕruwb](../../strongs/h/h3742.md): and the two [ben](../../strongs/h/h1121.md) of [ʿĒlî](../../strongs/h/h5941.md), [Ḥāp̄Nî](../../strongs/h/h2652.md) and [Pînḥās](../../strongs/h/h6372.md), were there with the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="1samuel_4_5"></a>1Samuel 4:5

And when the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md) into the [maḥănê](../../strongs/h/h4264.md), all [Yisra'el](../../strongs/h/h3478.md) [rûaʿ](../../strongs/h/h7321.md) with a [gadowl](../../strongs/h/h1419.md) [tᵊrûʿâ](../../strongs/h/h8643.md), so that the ['erets](../../strongs/h/h776.md) rang [huwm](../../strongs/h/h1949.md).

<a name="1samuel_4_6"></a>1Samuel 4:6

And when the [Pᵊlištî](../../strongs/h/h6430.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [tᵊrûʿâ](../../strongs/h/h8643.md), they ['āmar](../../strongs/h/h559.md), What meaneth the [qowl](../../strongs/h/h6963.md) of this [gadowl](../../strongs/h/h1419.md) [tᵊrûʿâ](../../strongs/h/h8643.md) in the [maḥănê](../../strongs/h/h4264.md) of the [ʿiḇrî](../../strongs/h/h5680.md)? And they [yada'](../../strongs/h/h3045.md) that the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) was [bow'](../../strongs/h/h935.md) into the [maḥănê](../../strongs/h/h4264.md).

<a name="1samuel_4_7"></a>1Samuel 4:7

And the [Pᵊlištî](../../strongs/h/h6430.md) were [yare'](../../strongs/h/h3372.md), for they ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) is [bow'](../../strongs/h/h935.md) into the [maḥănê](../../strongs/h/h4264.md). And they ['āmar](../../strongs/h/h559.md), ['owy](../../strongs/h/h188.md) unto us! for there hath not been such a thing ['eṯmôl](../../strongs/h/h865.md) [šilšôm](../../strongs/h/h8032.md).

<a name="1samuel_4_8"></a>1Samuel 4:8

['owy](../../strongs/h/h188.md) unto us! who shall [natsal](../../strongs/h/h5337.md) us out of the [yad](../../strongs/h/h3027.md) of these ['addiyr](../../strongs/h/h117.md) ['Elohiym](../../strongs/h/h430.md)? these are the ['Elohiym](../../strongs/h/h430.md) that [nakah](../../strongs/h/h5221.md) the [Mitsrayim](../../strongs/h/h4714.md) with all the [makâ](../../strongs/h/h4347.md) in the [midbar](../../strongs/h/h4057.md).

<a name="1samuel_4_9"></a>1Samuel 4:9

Be [ḥāzaq](../../strongs/h/h2388.md), and quit yourselves like ['enowsh](../../strongs/h/h582.md), O ye [Pᵊlištî](../../strongs/h/h6430.md), that ye be not ['abad](../../strongs/h/h5647.md) unto the [ʿiḇrî](../../strongs/h/h5680.md), as they have ['abad](../../strongs/h/h5647.md) to you: quit yourselves like ['enowsh](../../strongs/h/h582.md), and [lāḥam](../../strongs/h/h3898.md).

<a name="1samuel_4_10"></a>1Samuel 4:10

And the [Pᵊlištî](../../strongs/h/h6430.md) [lāḥam](../../strongs/h/h3898.md), and [Yisra'el](../../strongs/h/h3478.md) was [nāḡap̄](../../strongs/h/h5062.md), and they [nûs](../../strongs/h/h5127.md) every ['iysh](../../strongs/h/h376.md) into his ['ohel](../../strongs/h/h168.md): and there was a [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) [makâ](../../strongs/h/h4347.md); for there [naphal](../../strongs/h/h5307.md) of [Yisra'el](../../strongs/h/h3478.md) thirty thousand [raḡlî](../../strongs/h/h7273.md).

<a name="1samuel_4_11"></a>1Samuel 4:11

And the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) was [laqach](../../strongs/h/h3947.md); and the two [ben](../../strongs/h/h1121.md) of [ʿĒlî](../../strongs/h/h5941.md), [Ḥāp̄Nî](../../strongs/h/h2652.md) and [Pînḥās](../../strongs/h/h6372.md), were [muwth](../../strongs/h/h4191.md).

<a name="1samuel_4_12"></a>1Samuel 4:12

And there [rûṣ](../../strongs/h/h7323.md) an ['iysh](../../strongs/h/h376.md) of [Binyāmîn](../../strongs/h/h1144.md) out of the [maʿărāḵâ](../../strongs/h/h4634.md), and [bow'](../../strongs/h/h935.md) to [Šîlô](../../strongs/h/h7887.md) the same [yowm](../../strongs/h/h3117.md) with his [maḏ](../../strongs/h/h4055.md) [qāraʿ](../../strongs/h/h7167.md), and with ['ăḏāmâ](../../strongs/h/h127.md) upon his [ro'sh](../../strongs/h/h7218.md).

<a name="1samuel_4_13"></a>1Samuel 4:13

And when he [bow'](../../strongs/h/h935.md), lo, [ʿĒlî](../../strongs/h/h5941.md) [yashab](../../strongs/h/h3427.md) upon a [kicce'](../../strongs/h/h3678.md) by the [derek](../../strongs/h/h1870.md) [yad](../../strongs/h/h3027.md) [yaḵ](../../strongs/h/h3197.md) [tsaphah](../../strongs/h/h6822.md): for his [leb](../../strongs/h/h3820.md) [ḥārēḏ](../../strongs/h/h2730.md) for the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md). And when the ['iysh](../../strongs/h/h376.md) [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md), and [nāḡaḏ](../../strongs/h/h5046.md) it, all the [ʿîr](../../strongs/h/h5892.md) [zāʿaq](../../strongs/h/h2199.md).

<a name="1samuel_4_14"></a>1Samuel 4:14

And when [ʿĒlî](../../strongs/h/h5941.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [tsa'aqah](../../strongs/h/h6818.md), he ['āmar](../../strongs/h/h559.md), What meaneth the [qowl](../../strongs/h/h6963.md) of this [hāmôn](../../strongs/h/h1995.md)? And the ['iysh](../../strongs/h/h376.md) [bow'](../../strongs/h/h935.md) in [māhar](../../strongs/h/h4116.md), and [nāḡaḏ](../../strongs/h/h5046.md) [ʿĒlî](../../strongs/h/h5941.md).

<a name="1samuel_4_15"></a>1Samuel 4:15

Now [ʿĒlî](../../strongs/h/h5941.md) was ninety and eight [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md); and his ['ayin](../../strongs/h/h5869.md) were [quwm](../../strongs/h/h6965.md), that he [yakol](../../strongs/h/h3201.md) not [ra'ah](../../strongs/h/h7200.md).

<a name="1samuel_4_16"></a>1Samuel 4:16

And the ['iysh](../../strongs/h/h376.md) ['āmar](../../strongs/h/h559.md) unto [ʿĒlî](../../strongs/h/h5941.md), I am he that [bow'](../../strongs/h/h935.md) out of the [maʿărāḵâ](../../strongs/h/h4634.md), and I [nûs](../../strongs/h/h5127.md) to [yowm](../../strongs/h/h3117.md) out of the [maʿărāḵâ](../../strongs/h/h4634.md). And he ['āmar](../../strongs/h/h559.md), What is there [dabar](../../strongs/h/h1697.md), my [ben](../../strongs/h/h1121.md)?

<a name="1samuel_4_17"></a>1Samuel 4:17

And the [bāśar](../../strongs/h/h1319.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), [Yisra'el](../../strongs/h/h3478.md) is [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) the [Pᵊlištî](../../strongs/h/h6430.md), and there hath been also a [gadowl](../../strongs/h/h1419.md) [magēp̄â](../../strongs/h/h4046.md) among the ['am](../../strongs/h/h5971.md), and thy two [ben](../../strongs/h/h1121.md) also, [Ḥāp̄Nî](../../strongs/h/h2652.md) and [Pînḥās](../../strongs/h/h6372.md), are [muwth](../../strongs/h/h4191.md), and the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) is [laqach](../../strongs/h/h3947.md).

<a name="1samuel_4_18"></a>1Samuel 4:18

And it came to pass, when he made [zakar](../../strongs/h/h2142.md) of the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md), that he [naphal](../../strongs/h/h5307.md) from off the [kicce'](../../strongs/h/h3678.md) ['ăḥōrannîṯ](../../strongs/h/h322.md) by the [yad](../../strongs/h/h3027.md) of the [sha'ar](../../strongs/h/h8179.md), and his [map̄reqeṯ](../../strongs/h/h4665.md) [shabar](../../strongs/h/h7665.md), and he [muwth](../../strongs/h/h4191.md): for he was a [zāqēn](../../strongs/h/h2204.md) ['iysh](../../strongs/h/h376.md), and [kabad](../../strongs/h/h3513.md). And he had [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md) forty [šānâ](../../strongs/h/h8141.md).

<a name="1samuel_4_19"></a>1Samuel 4:19

And his [kallâ](../../strongs/h/h3618.md), [Pînḥās](../../strongs/h/h6372.md) ['ishshah](../../strongs/h/h802.md), was with [hārê](../../strongs/h/h2030.md), near to be [yalad](../../strongs/h/h3205.md): and when she [shama'](../../strongs/h/h8085.md) the [šᵊmûʿâ](../../strongs/h/h8052.md) that the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) was [laqach](../../strongs/h/h3947.md), and that her [Ḥām](../../strongs/h/h2524.md) and her ['iysh](../../strongs/h/h376.md) were [muwth](../../strongs/h/h4191.md), she [kara'](../../strongs/h/h3766.md) herself and [yalad](../../strongs/h/h3205.md); for her [ṣîr](../../strongs/h/h6735.md) [hāp̄aḵ](../../strongs/h/h2015.md) upon her.

<a name="1samuel_4_20"></a>1Samuel 4:20

And about the [ʿēṯ](../../strongs/h/h6256.md) of her [muwth](../../strongs/h/h4191.md) the women that [nāṣaḇ](../../strongs/h/h5324.md) by her [dabar](../../strongs/h/h1696.md) unto her, [yare'](../../strongs/h/h3372.md) not; for thou hast [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md). But she ['anah](../../strongs/h/h6030.md) not, neither did she [shiyth](../../strongs/h/h7896.md) [leb](../../strongs/h/h3820.md) it.

<a name="1samuel_4_21"></a>1Samuel 4:21

And she [qara'](../../strongs/h/h7121.md) the [naʿar](../../strongs/h/h5288.md) ['Î-Ḵāḇôḏ](../../strongs/h/h350.md), ['āmar](../../strongs/h/h559.md), The [kabowd](../../strongs/h/h3519.md) is [gālâ](../../strongs/h/h1540.md) from [Yisra'el](../../strongs/h/h3478.md): because the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) was [laqach](../../strongs/h/h3947.md), and because of her [Ḥām](../../strongs/h/h2524.md) and her ['iysh](../../strongs/h/h376.md).

<a name="1samuel_4_22"></a>1Samuel 4:22

And she ['āmar](../../strongs/h/h559.md), The [kabowd](../../strongs/h/h3519.md) is [gālâ](../../strongs/h/h1540.md) from [Yisra'el](../../strongs/h/h3478.md): for the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) is [laqach](../../strongs/h/h3947.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 3](1samuel_3.md) - [1Samuel 5](1samuel_5.md)