# [1Samuel 3](https://www.blueletterbible.org/kjv/1samuel/3)

<a name="1samuel_3_1"></a>1Samuel 3:1

And the [naʿar](../../strongs/h/h5288.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md) [sharath](../../strongs/h/h8334.md) unto [Yĕhovah](../../strongs/h/h3068.md) [paniym](../../strongs/h/h6440.md) [ʿĒlî](../../strongs/h/h5941.md). And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) was [yāqār](../../strongs/h/h3368.md) in those [yowm](../../strongs/h/h3117.md); there was no [pāraṣ](../../strongs/h/h6555.md) [ḥāzôn](../../strongs/h/h2377.md).

<a name="1samuel_3_2"></a>1Samuel 3:2

And it came to pass at that [yowm](../../strongs/h/h3117.md), when [ʿĒlî](../../strongs/h/h5941.md) was [shakab](../../strongs/h/h7901.md) in his [maqowm](../../strongs/h/h4725.md), and his ['ayin](../../strongs/h/h5869.md) [ḥālal](../../strongs/h/h2490.md) to wax [kēhê](../../strongs/h/h3544.md), that he [yakol](../../strongs/h/h3201.md) not [ra'ah](../../strongs/h/h7200.md);

<a name="1samuel_3_3"></a>1Samuel 3:3

And ere the [nîr](../../strongs/h/h5216.md) of ['Elohiym](../../strongs/h/h430.md) went [kāḇâ](../../strongs/h/h3518.md) in the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md), where the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) was, and [Šᵊmû'Ēl](../../strongs/h/h8050.md) was [shakab](../../strongs/h/h7901.md) to sleep;

<a name="1samuel_3_4"></a>1Samuel 3:4

That [Yĕhovah](../../strongs/h/h3068.md) [qara'](../../strongs/h/h7121.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md): and he ['āmar](../../strongs/h/h559.md), Here am I.

<a name="1samuel_3_5"></a>1Samuel 3:5

And he [rûṣ](../../strongs/h/h7323.md) unto [ʿĒlî](../../strongs/h/h5941.md), and ['āmar](../../strongs/h/h559.md), Here am [hinneh](../../strongs/h/h2009.md); for thou [qara'](../../strongs/h/h7121.md) me. And he ['āmar](../../strongs/h/h559.md), I [qara'](../../strongs/h/h7121.md) not; [shakab](../../strongs/h/h7901.md) [shuwb](../../strongs/h/h7725.md). And he [yālaḵ](../../strongs/h/h3212.md) and lay [shakab](../../strongs/h/h7901.md).

<a name="1samuel_3_6"></a>1Samuel 3:6

And [Yĕhovah](../../strongs/h/h3068.md) [qara'](../../strongs/h/h7121.md) yet again, [Šᵊmû'Ēl](../../strongs/h/h8050.md). And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [quwm](../../strongs/h/h6965.md) and [yālaḵ](../../strongs/h/h3212.md) to [ʿĒlî](../../strongs/h/h5941.md), and ['āmar](../../strongs/h/h559.md), Here am I; for thou didst [qara'](../../strongs/h/h7121.md) me. And he ['āmar](../../strongs/h/h559.md), I [qara'](../../strongs/h/h7121.md) not, my [ben](../../strongs/h/h1121.md); [shakab](../../strongs/h/h7901.md) [shuwb](../../strongs/h/h7725.md).

<a name="1samuel_3_7"></a>1Samuel 3:7

Now [Šᵊmû'Ēl](../../strongs/h/h8050.md) did not yet [yada'](../../strongs/h/h3045.md) [Yĕhovah](../../strongs/h/h3068.md), neither was the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) yet [gālâ](../../strongs/h/h1540.md) unto him.

<a name="1samuel_3_8"></a>1Samuel 3:8

And [Yĕhovah](../../strongs/h/h3068.md) [qara'](../../strongs/h/h7121.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md) again the third time. And he [quwm](../../strongs/h/h6965.md) and [yālaḵ](../../strongs/h/h3212.md) to [ʿĒlî](../../strongs/h/h5941.md), and ['āmar](../../strongs/h/h559.md), Here am I; for thou didst [qara'](../../strongs/h/h7121.md) me. And [ʿĒlî](../../strongs/h/h5941.md) [bîn](../../strongs/h/h995.md) that [Yĕhovah](../../strongs/h/h3068.md) had [qara'](../../strongs/h/h7121.md) the [naʿar](../../strongs/h/h5288.md).

<a name="1samuel_3_9"></a>1Samuel 3:9

Therefore [ʿĒlî](../../strongs/h/h5941.md) ['āmar](../../strongs/h/h559.md) unto [Šᵊmû'Ēl](../../strongs/h/h8050.md), [yālaḵ](../../strongs/h/h3212.md), [shakab](../../strongs/h/h7901.md): and it shall be, if he [qara'](../../strongs/h/h7121.md) thee, that thou shalt ['āmar](../../strongs/h/h559.md), [dabar](../../strongs/h/h1696.md), [Yĕhovah](../../strongs/h/h3068.md); for thy ['ebed](../../strongs/h/h5650.md) [shama'](../../strongs/h/h8085.md). So [Šᵊmû'Ēl](../../strongs/h/h8050.md) [yālaḵ](../../strongs/h/h3212.md) and lay [shakab](../../strongs/h/h7901.md) in his [maqowm](../../strongs/h/h4725.md).

<a name="1samuel_3_10"></a>1Samuel 3:10

And [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md), and [yatsab](../../strongs/h/h3320.md), and [qara'](../../strongs/h/h7121.md) as at other times, [Šᵊmû'Ēl](../../strongs/h/h8050.md), [Šᵊmû'Ēl](../../strongs/h/h8050.md). Then [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md), [dabar](../../strongs/h/h1696.md); for thy ['ebed](../../strongs/h/h5650.md) [shama'](../../strongs/h/h8085.md).

<a name="1samuel_3_11"></a>1Samuel 3:11

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) to [Šᵊmû'Ēl](../../strongs/h/h8050.md), Behold, I will ['asah](../../strongs/h/h6213.md) a [dabar](../../strongs/h/h1697.md) in [Yisra'el](../../strongs/h/h3478.md), at which both the ['ozen](../../strongs/h/h241.md) of every one that [shama'](../../strongs/h/h8085.md) it shall [ṣālal](../../strongs/h/h6750.md).

<a name="1samuel_3_12"></a>1Samuel 3:12

In that [yowm](../../strongs/h/h3117.md) I will [quwm](../../strongs/h/h6965.md) against [ʿĒlî](../../strongs/h/h5941.md) all things which I have [dabar](../../strongs/h/h1696.md) concerning his [bayith](../../strongs/h/h1004.md): when I [ḥālal](../../strongs/h/h2490.md), I will also make a [kalah](../../strongs/h/h3615.md).

<a name="1samuel_3_13"></a>1Samuel 3:13

For I have [nāḡaḏ](../../strongs/h/h5046.md) him that I will [shaphat](../../strongs/h/h8199.md) his [bayith](../../strongs/h/h1004.md) ['owlam](../../strongs/h/h5769.md) for the ['avon](../../strongs/h/h5771.md) which he [yada'](../../strongs/h/h3045.md); because his [ben](../../strongs/h/h1121.md) made themselves [qālal](../../strongs/h/h7043.md), and he [kāhâ](../../strongs/h/h3543.md) them not.

<a name="1samuel_3_14"></a>1Samuel 3:14

And therefore I have [shaba'](../../strongs/h/h7650.md) unto the [bayith](../../strongs/h/h1004.md) of [ʿĒlî](../../strongs/h/h5941.md), that the ['avon](../../strongs/h/h5771.md) of [ʿĒlî](../../strongs/h/h5941.md) [bayith](../../strongs/h/h1004.md) shall not be [kāp̄ar](../../strongs/h/h3722.md) with [zebach](../../strongs/h/h2077.md) nor [minchah](../../strongs/h/h4503.md) ['owlam](../../strongs/h/h5769.md).

<a name="1samuel_3_15"></a>1Samuel 3:15

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [shakab](../../strongs/h/h7901.md) until the [boqer](../../strongs/h/h1242.md), and [pāṯaḥ](../../strongs/h/h6605.md) the [deleṯ](../../strongs/h/h1817.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md). And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [yare'](../../strongs/h/h3372.md) to [nāḡaḏ](../../strongs/h/h5046.md) [ʿĒlî](../../strongs/h/h5941.md) the [mar'â](../../strongs/h/h4759.md).

<a name="1samuel_3_16"></a>1Samuel 3:16

Then [ʿĒlî](../../strongs/h/h5941.md) [qara'](../../strongs/h/h7121.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md), and ['āmar](../../strongs/h/h559.md), [Šᵊmû'Ēl](../../strongs/h/h8050.md), my [ben](../../strongs/h/h1121.md). And he ['āmar](../../strongs/h/h559.md), Here am I.

<a name="1samuel_3_17"></a>1Samuel 3:17

And he ['āmar](../../strongs/h/h559.md), What is the [dabar](../../strongs/h/h1697.md) that hath [dabar](../../strongs/h/h1696.md) unto thee? I pray thee [kāḥaḏ](../../strongs/h/h3582.md) it not from me: ['Elohiym](../../strongs/h/h430.md) do ['asah](../../strongs/h/h6213.md) to thee, and more also, if thou [kāḥaḏ](../../strongs/h/h3582.md) any [dabar](../../strongs/h/h1697.md) from me of all the [dabar](../../strongs/h/h1697.md) that he [dabar](../../strongs/h/h1696.md) unto thee.

<a name="1samuel_3_18"></a>1Samuel 3:18

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [nāḡaḏ](../../strongs/h/h5046.md) him every [dabar](../../strongs/h/h1697.md), and [kāḥaḏ](../../strongs/h/h3582.md) nothing from him. And he ['āmar](../../strongs/h/h559.md), It is [Yĕhovah](../../strongs/h/h3068.md): let him ['asah](../../strongs/h/h6213.md) what ['ayin](../../strongs/h/h5869.md) him [towb](../../strongs/h/h2896.md).

<a name="1samuel_3_19"></a>1Samuel 3:19

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [gāḏal](../../strongs/h/h1431.md), and [Yĕhovah](../../strongs/h/h3068.md) was with him, and did let none of his [dabar](../../strongs/h/h1697.md) [naphal](../../strongs/h/h5307.md) to the ['erets](../../strongs/h/h776.md).

<a name="1samuel_3_20"></a>1Samuel 3:20

And all [Yisra'el](../../strongs/h/h3478.md) from [Dān](../../strongs/h/h1835.md) even to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md) [yada'](../../strongs/h/h3045.md) that [Šᵊmû'Ēl](../../strongs/h/h8050.md) was ['aman](../../strongs/h/h539.md) to be a [nāḇî'](../../strongs/h/h5030.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_3_21"></a>1Samuel 3:21

And [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) again in [Šîlô](../../strongs/h/h7887.md): for [Yĕhovah](../../strongs/h/h3068.md) [gālâ](../../strongs/h/h1540.md) himself to [Šᵊmû'Ēl](../../strongs/h/h8050.md) in [Šîlô](../../strongs/h/h7887.md) by the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 2](1samuel_2.md) - [1Samuel 4](1samuel_4.md)