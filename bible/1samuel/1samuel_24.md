# [1Samuel 24](https://www.blueletterbible.org/kjv/1samuel/24)

<a name="1samuel_24_1"></a>1Samuel 24:1

And it came to pass, when [Šā'ûl](../../strongs/h/h7586.md) was [shuwb](../../strongs/h/h7725.md) from ['aḥar](../../strongs/h/h310.md) the [Pᵊlištî](../../strongs/h/h6430.md), that it was [nāḡaḏ](../../strongs/h/h5046.md) him, ['āmar](../../strongs/h/h559.md), Behold, [Dāviḏ](../../strongs/h/h1732.md) is in the [midbar](../../strongs/h/h4057.md) of [ʿÊn Ḡeḏî](../../strongs/h/h5872.md).

<a name="1samuel_24_2"></a>1Samuel 24:2

Then [Šā'ûl](../../strongs/h/h7586.md) [laqach](../../strongs/h/h3947.md) three thousand [bāḥar](../../strongs/h/h977.md) ['iysh](../../strongs/h/h376.md) out of all [Yisra'el](../../strongs/h/h3478.md), and [yālaḵ](../../strongs/h/h3212.md) to [bāqaš](../../strongs/h/h1245.md) [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) [paniym](../../strongs/h/h6440.md) the [tsuwr](../../strongs/h/h6697.md) of the wild [yāʿēl](../../strongs/h/h3277.md).

<a name="1samuel_24_3"></a>1Samuel 24:3

And he [bow'](../../strongs/h/h935.md) to the [gᵊḏērâ](../../strongs/h/h1448.md) [tso'n](../../strongs/h/h6629.md) by the [derek](../../strongs/h/h1870.md), where was a [mᵊʿārâ](../../strongs/h/h4631.md); and [Šā'ûl](../../strongs/h/h7586.md) [bow'](../../strongs/h/h935.md) to [cakak](../../strongs/h/h5526.md) his [regel](../../strongs/h/h7272.md): and [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) [yashab](../../strongs/h/h3427.md) in the [yᵊrēḵâ](../../strongs/h/h3411.md) of the [mᵊʿārâ](../../strongs/h/h4631.md).

<a name="1samuel_24_4"></a>1Samuel 24:4

And the ['enowsh](../../strongs/h/h582.md) of [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto him, Behold the [yowm](../../strongs/h/h3117.md) of which [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto thee, Behold, I will [nathan](../../strongs/h/h5414.md) thine ['oyeb](../../strongs/h/h341.md) into thine [yad](../../strongs/h/h3027.md), that thou mayest ['asah](../../strongs/h/h6213.md) to him as it shall seem [yatab](../../strongs/h/h3190.md) unto ['ayin](../../strongs/h/h5869.md). Then [Dāviḏ](../../strongs/h/h1732.md) [quwm](../../strongs/h/h6965.md), and [karath](../../strongs/h/h3772.md) the [kanaph](../../strongs/h/h3671.md) of [Šā'ûl](../../strongs/h/h7586.md) [mᵊʿîl](../../strongs/h/h4598.md) [lāṭ](../../strongs/h/h3909.md).

<a name="1samuel_24_5"></a>1Samuel 24:5

And it came to pass ['aḥar](../../strongs/h/h310.md), that [Dāviḏ](../../strongs/h/h1732.md) [leb](../../strongs/h/h3820.md) [nakah](../../strongs/h/h5221.md) him, because he had [karath](../../strongs/h/h3772.md) [Šā'ûl](../../strongs/h/h7586.md) [kanaph](../../strongs/h/h3671.md).

<a name="1samuel_24_6"></a>1Samuel 24:6

And he ['āmar](../../strongs/h/h559.md) unto his ['enowsh](../../strongs/h/h582.md), [Yĕhovah](../../strongs/h/h3068.md) [ḥālîlâ](../../strongs/h/h2486.md) that I should ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md) unto my ['adown](../../strongs/h/h113.md), [Yĕhovah](../../strongs/h/h3068.md) [mashiyach](../../strongs/h/h4899.md), to [shalach](../../strongs/h/h7971.md) mine [yad](../../strongs/h/h3027.md) against him, seeing he is the [mashiyach](../../strongs/h/h4899.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_24_7"></a>1Samuel 24:7

So [Dāviḏ](../../strongs/h/h1732.md) [šāsaʿ](../../strongs/h/h8156.md) his ['enowsh](../../strongs/h/h582.md) with these [dabar](../../strongs/h/h1697.md), and [nathan](../../strongs/h/h5414.md) them not to [quwm](../../strongs/h/h6965.md) against [Šā'ûl](../../strongs/h/h7586.md). But [Šā'ûl](../../strongs/h/h7586.md) [quwm](../../strongs/h/h6965.md) out of the [mᵊʿārâ](../../strongs/h/h4631.md), and [yālaḵ](../../strongs/h/h3212.md) on his [derek](../../strongs/h/h1870.md).

<a name="1samuel_24_8"></a>1Samuel 24:8

[Dāviḏ](../../strongs/h/h1732.md) also [quwm](../../strongs/h/h6965.md) afterward, and [yāṣā'](../../strongs/h/h3318.md) of the [mᵊʿārâ](../../strongs/h/h4631.md), and [qara'](../../strongs/h/h7121.md) ['aḥar](../../strongs/h/h310.md) [Šā'ûl](../../strongs/h/h7586.md), ['āmar](../../strongs/h/h559.md), My ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md). And when [Šā'ûl](../../strongs/h/h7586.md) [nabat](../../strongs/h/h5027.md) ['aḥar](../../strongs/h/h310.md) him, [Dāviḏ](../../strongs/h/h1732.md) [qāḏaḏ](../../strongs/h/h6915.md) with his ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md), and [shachah](../../strongs/h/h7812.md) himself.

<a name="1samuel_24_9"></a>1Samuel 24:9

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to [Šā'ûl](../../strongs/h/h7586.md), Wherefore [shama'](../../strongs/h/h8085.md) thou ['āḏām](../../strongs/h/h120.md) [dabar](../../strongs/h/h1697.md), ['āmar](../../strongs/h/h559.md), Behold, [Dāviḏ](../../strongs/h/h1732.md) [bāqaš](../../strongs/h/h1245.md) thy [ra'](../../strongs/h/h7451.md)?

<a name="1samuel_24_10"></a>1Samuel 24:10

Behold, this [yowm](../../strongs/h/h3117.md) thine ['ayin](../../strongs/h/h5869.md) have [ra'ah](../../strongs/h/h7200.md) how that [Yĕhovah](../../strongs/h/h3068.md) had [nathan](../../strongs/h/h5414.md) thee to [yowm](../../strongs/h/h3117.md) into mine [yad](../../strongs/h/h3027.md) in the [mᵊʿārâ](../../strongs/h/h4631.md): and some ['āmar](../../strongs/h/h559.md) me [harag](../../strongs/h/h2026.md) thee: but mine eye [ḥûs](../../strongs/h/h2347.md) thee; and I ['āmar](../../strongs/h/h559.md), I will not [shalach](../../strongs/h/h7971.md) mine [yad](../../strongs/h/h3027.md) against my ['adown](../../strongs/h/h113.md); for he is [Yĕhovah](../../strongs/h/h3068.md) [mashiyach](../../strongs/h/h4899.md).

<a name="1samuel_24_11"></a>1Samuel 24:11

Moreover, my ['ab](../../strongs/h/h1.md), [ra'ah](../../strongs/h/h7200.md), yea, [ra'ah](../../strongs/h/h7200.md) the [kanaph](../../strongs/h/h3671.md) of thy [mᵊʿîl](../../strongs/h/h4598.md) in my [yad](../../strongs/h/h3027.md): for in that I [karath](../../strongs/h/h3772.md) the [kanaph](../../strongs/h/h3671.md) of thy [mᵊʿîl](../../strongs/h/h4598.md), and [harag](../../strongs/h/h2026.md) thee not, [yada'](../../strongs/h/h3045.md) thou and [ra'ah](../../strongs/h/h7200.md) that there is neither [ra'](../../strongs/h/h7451.md) nor [pesha'](../../strongs/h/h6588.md) in mine [yad](../../strongs/h/h3027.md), and I have not [chata'](../../strongs/h/h2398.md) against thee; yet thou [ṣāḏâ](../../strongs/h/h6658.md) my [nephesh](../../strongs/h/h5315.md) to [laqach](../../strongs/h/h3947.md) it.

<a name="1samuel_24_12"></a>1Samuel 24:12

[Yĕhovah](../../strongs/h/h3068.md) [shaphat](../../strongs/h/h8199.md) between me and thee, and [Yĕhovah](../../strongs/h/h3068.md) [naqam](../../strongs/h/h5358.md) me of thee: but mine [yad](../../strongs/h/h3027.md) shall not be upon thee.

<a name="1samuel_24_13"></a>1Samuel 24:13

As ['āmar](../../strongs/h/h559.md) the [māšāl](../../strongs/h/h4912.md) of the [qaḏmōnî](../../strongs/h/h6931.md), [resha'](../../strongs/h/h7562.md) [yāṣā'](../../strongs/h/h3318.md) from the [rasha'](../../strongs/h/h7563.md): but mine [yad](../../strongs/h/h3027.md) shall not be upon thee.

<a name="1samuel_24_14"></a>1Samuel 24:14

['aḥar](../../strongs/h/h310.md) whom is the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [yāṣā'](../../strongs/h/h3318.md)? ['aḥar](../../strongs/h/h310.md) whom dost thou [radaph](../../strongs/h/h7291.md)? ['aḥar](../../strongs/h/h310.md) a [muwth](../../strongs/h/h4191.md) [keleḇ](../../strongs/h/h3611.md), ['aḥar](../../strongs/h/h310.md) a [parʿš](../../strongs/h/h6550.md).

<a name="1samuel_24_15"></a>1Samuel 24:15

[Yĕhovah](../../strongs/h/h3068.md) therefore be [dayyān](../../strongs/h/h1781.md), and [shaphat](../../strongs/h/h8199.md) between me and thee, and [ra'ah](../../strongs/h/h7200.md), and [riyb](../../strongs/h/h7378.md) my [rîḇ](../../strongs/h/h7379.md), and [shaphat](../../strongs/h/h8199.md) me out of thine [yad](../../strongs/h/h3027.md).

<a name="1samuel_24_16"></a>1Samuel 24:16

And it came to pass, when [Dāviḏ](../../strongs/h/h1732.md) had made a [kalah](../../strongs/h/h3615.md) of [dabar](../../strongs/h/h1696.md) these [dabar](../../strongs/h/h1697.md) unto [Šā'ûl](../../strongs/h/h7586.md), that [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), Is this thy [qowl](../../strongs/h/h6963.md), my [ben](../../strongs/h/h1121.md) [Dāviḏ](../../strongs/h/h1732.md)? And [Šā'ûl](../../strongs/h/h7586.md) [nasa'](../../strongs/h/h5375.md) his [qowl](../../strongs/h/h6963.md), and [bāḵâ](../../strongs/h/h1058.md).

<a name="1samuel_24_17"></a>1Samuel 24:17

And he ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), Thou art more [tsaddiyq](../../strongs/h/h6662.md) than I: for thou hast [gamal](../../strongs/h/h1580.md) me [towb](../../strongs/h/h2896.md), whereas I have [gamal](../../strongs/h/h1580.md) thee [ra'](../../strongs/h/h7451.md).

<a name="1samuel_24_18"></a>1Samuel 24:18

And thou hast [nāḡaḏ](../../strongs/h/h5046.md) this [yowm](../../strongs/h/h3117.md) how that thou hast ['asah](../../strongs/h/h6213.md) [towb](../../strongs/h/h2896.md) with me: forasmuch as when [Yĕhovah](../../strongs/h/h3068.md) had [cagar](../../strongs/h/h5462.md) me into thine [yad](../../strongs/h/h3027.md), thou [harag](../../strongs/h/h2026.md) me not.

<a name="1samuel_24_19"></a>1Samuel 24:19

For if an ['iysh](../../strongs/h/h376.md) [māṣā'](../../strongs/h/h4672.md) his ['oyeb](../../strongs/h/h341.md), will he let him [shalach](../../strongs/h/h7971.md) [towb](../../strongs/h/h2896.md) [derek](../../strongs/h/h1870.md)? wherefore [Yĕhovah](../../strongs/h/h3068.md) [shalam](../../strongs/h/h7999.md) thee [towb](../../strongs/h/h2896.md) for that thou hast ['asah](../../strongs/h/h6213.md) unto me this [yowm](../../strongs/h/h3117.md).

<a name="1samuel_24_20"></a>1Samuel 24:20

And now, behold, I know [yada'](../../strongs/h/h3045.md) that thou shalt [mālaḵ](../../strongs/h/h4427.md) be [mālaḵ](../../strongs/h/h4427.md), and that the [mamlāḵâ](../../strongs/h/h4467.md) of [Yisra'el](../../strongs/h/h3478.md) shall be [quwm](../../strongs/h/h6965.md) in thine [yad](../../strongs/h/h3027.md).

<a name="1samuel_24_21"></a>1Samuel 24:21

[shaba'](../../strongs/h/h7650.md) now therefore unto me by [Yĕhovah](../../strongs/h/h3068.md), that thou wilt not [karath](../../strongs/h/h3772.md) my [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) me, and that thou wilt not [šāmaḏ](../../strongs/h/h8045.md) my [shem](../../strongs/h/h8034.md) out of my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md).

<a name="1samuel_24_22"></a>1Samuel 24:22

And [Dāviḏ](../../strongs/h/h1732.md) [shaba'](../../strongs/h/h7650.md) unto [Šā'ûl](../../strongs/h/h7586.md). And [Šā'ûl](../../strongs/h/h7586.md) [yālaḵ](../../strongs/h/h3212.md) [bayith](../../strongs/h/h1004.md); but [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) gat them [ʿālâ](../../strongs/h/h5927.md) unto the [matsuwd](../../strongs/h/h4686.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 23](1samuel_23.md) - [1Samuel 25](1samuel_25.md)