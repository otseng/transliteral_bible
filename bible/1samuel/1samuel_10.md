# [1Samuel 10](https://www.blueletterbible.org/kjv/1samuel/10)

<a name="1samuel_10_1"></a>1Samuel 10:1

Then [Šᵊmû'Ēl](../../strongs/h/h8050.md) [laqach](../../strongs/h/h3947.md) a [paḵ](../../strongs/h/h6378.md) of [šemen](../../strongs/h/h8081.md), and [yāṣaq](../../strongs/h/h3332.md) it upon his [ro'sh](../../strongs/h/h7218.md), and [nashaq](../../strongs/h/h5401.md) him, and ['āmar](../../strongs/h/h559.md), Is it not because [Yĕhovah](../../strongs/h/h3068.md) hath [māšaḥ](../../strongs/h/h4886.md) thee to be [nāḡîḏ](../../strongs/h/h5057.md) over his [nachalah](../../strongs/h/h5159.md)?

<a name="1samuel_10_2"></a>1Samuel 10:2

When thou art [yālaḵ](../../strongs/h/h3212.md) from me to [yowm](../../strongs/h/h3117.md), then thou shalt [māṣā'](../../strongs/h/h4672.md) two ['enowsh](../../strongs/h/h582.md) by [Rāḥēl](../../strongs/h/h7354.md) [qᵊḇûrâ](../../strongs/h/h6900.md) in the [gᵊḇûl](../../strongs/h/h1366.md) of [Binyāmîn](../../strongs/h/h1144.md) at [Ṣelṣaḥ](../../strongs/h/h6766.md); and they will ['āmar](../../strongs/h/h559.md) unto thee, The ['āṯôn](../../strongs/h/h860.md) which thou [halak](../../strongs/h/h1980.md) to [bāqaš](../../strongs/h/h1245.md) are [māṣā'](../../strongs/h/h4672.md): and, lo, thy ['ab](../../strongs/h/h1.md) hath [nāṭaš](../../strongs/h/h5203.md) the [dabar](../../strongs/h/h1697.md) of the ['āṯôn](../../strongs/h/h860.md), and [dā'aḡ](../../strongs/h/h1672.md) for you, ['āmar](../../strongs/h/h559.md), What shall I ['asah](../../strongs/h/h6213.md) for my [ben](../../strongs/h/h1121.md)?

<a name="1samuel_10_3"></a>1Samuel 10:3

Then shalt thou go [ḥālap̄](../../strongs/h/h2498.md) [hāl'â](../../strongs/h/h1973.md) from thence, and thou shalt [bow'](../../strongs/h/h935.md) to the ['ēlôn](../../strongs/h/h436.md) of [Tāḇôr](../../strongs/h/h8396.md), and there shall [māṣā'](../../strongs/h/h4672.md) thee three ['enowsh](../../strongs/h/h582.md) going [ʿālâ](../../strongs/h/h5927.md) to ['Elohiym](../../strongs/h/h430.md) to [Bêṯ-'ēl](../../strongs/h/h1008.md), one [nasa'](../../strongs/h/h5375.md) three [gᵊḏî](../../strongs/h/h1423.md), and another [nasa'](../../strongs/h/h5375.md) three [kikār](../../strongs/h/h3603.md) of [lechem](../../strongs/h/h3899.md), and another [nasa'](../../strongs/h/h5375.md) a [neḇel](../../strongs/h/h5035.md) of [yayin](../../strongs/h/h3196.md):

<a name="1samuel_10_4"></a>1Samuel 10:4

And they [sha'al](../../strongs/h/h7592.md) [shalowm](../../strongs/h/h7965.md) thee, and [nathan](../../strongs/h/h5414.md) thee two loaves of [lechem](../../strongs/h/h3899.md); which thou shalt [laqach](../../strongs/h/h3947.md) of their [yad](../../strongs/h/h3027.md).

<a name="1samuel_10_5"></a>1Samuel 10:5

['aḥar](../../strongs/h/h310.md) that thou shalt [bow'](../../strongs/h/h935.md) to the [giḇʿâ](../../strongs/h/h1389.md) of ['Elohiym](../../strongs/h/h430.md), where is the [nᵊṣîḇ](../../strongs/h/h5333.md) of the [Pᵊlištî](../../strongs/h/h6430.md): and it shall come to pass, when thou art [bow'](../../strongs/h/h935.md) to the [ʿîr](../../strongs/h/h5892.md), that thou shalt [pāḡaʿ](../../strongs/h/h6293.md) a [chebel](../../strongs/h/h2256.md) of [nāḇî'](../../strongs/h/h5030.md) coming [yarad](../../strongs/h/h3381.md) from the [bāmâ](../../strongs/h/h1116.md) with a [neḇel](../../strongs/h/h5035.md), and a [tōp̄](../../strongs/h/h8596.md), and a [ḥālîl](../../strongs/h/h2485.md), and a [kinnôr](../../strongs/h/h3658.md), [paniym](../../strongs/h/h6440.md) them; and they shall [nāḇā'](../../strongs/h/h5012.md):

<a name="1samuel_10_6"></a>1Samuel 10:6

And the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) will [tsalach](../../strongs/h/h6743.md) upon thee, and thou shalt [nāḇā'](../../strongs/h/h5012.md) with them, and shalt be [hāp̄aḵ](../../strongs/h/h2015.md) into ['aḥēr](../../strongs/h/h312.md) ['iysh](../../strongs/h/h376.md).

<a name="1samuel_10_7"></a>1Samuel 10:7

And let it be, when these ['ôṯ](../../strongs/h/h226.md) are [bow'](../../strongs/h/h935.md) unto thee, that thou ['asah](../../strongs/h/h6213.md) as [māṣā'](../../strongs/h/h4672.md) [yad](../../strongs/h/h3027.md) thee; for ['Elohiym](../../strongs/h/h430.md) is with thee.

<a name="1samuel_10_8"></a>1Samuel 10:8

And thou shalt [yarad](../../strongs/h/h3381.md) [paniym](../../strongs/h/h6440.md) me to [Gilgāl](../../strongs/h/h1537.md); and, behold, I will [yarad](../../strongs/h/h3381.md) unto thee, to [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md), and to [zabach](../../strongs/h/h2076.md) [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md): seven [yowm](../../strongs/h/h3117.md) shalt thou [yāḥal](../../strongs/h/h3176.md), till I [bow'](../../strongs/h/h935.md) to thee, and [yada'](../../strongs/h/h3045.md) thee what thou shalt ['asah](../../strongs/h/h6213.md).

<a name="1samuel_10_9"></a>1Samuel 10:9

And it was so, that when he had [panah](../../strongs/h/h6437.md) his [šᵊḵem](../../strongs/h/h7926.md) to [yālaḵ](../../strongs/h/h3212.md) from [Šᵊmû'Ēl](../../strongs/h/h8050.md), ['Elohiym](../../strongs/h/h430.md) [hāp̄aḵ](../../strongs/h/h2015.md) him ['aḥēr](../../strongs/h/h312.md) [leb](../../strongs/h/h3820.md): and all those ['ôṯ](../../strongs/h/h226.md) [bow'](../../strongs/h/h935.md) to pass that [yowm](../../strongs/h/h3117.md).

<a name="1samuel_10_10"></a>1Samuel 10:10

And when they [bow'](../../strongs/h/h935.md) thither to the [giḇʿâ](../../strongs/h/h1389.md), behold, a [chebel](../../strongs/h/h2256.md) of [nāḇî'](../../strongs/h/h5030.md) [qārā'](../../strongs/h/h7125.md) him; and the [ruwach](../../strongs/h/h7307.md) of ['Elohiym](../../strongs/h/h430.md) [tsalach](../../strongs/h/h6743.md) upon him, and he [nāḇā'](../../strongs/h/h5012.md) [tavek](../../strongs/h/h8432.md) them.

<a name="1samuel_10_11"></a>1Samuel 10:11

And it came to pass, when all that [yada'](../../strongs/h/h3045.md) him ['eṯmôl](../../strongs/h/h865.md) [šilšôm](../../strongs/h/h8032.md) [ra'ah](../../strongs/h/h7200.md) that, behold, he [nāḇā'](../../strongs/h/h5012.md) among the [nāḇî'](../../strongs/h/h5030.md), then the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md) ['iysh](../../strongs/h/h376.md) to [rea'](../../strongs/h/h7453.md), What is this that is come unto the [ben](../../strongs/h/h1121.md) of [Qîš](../../strongs/h/h7027.md)? Is [Šā'ûl](../../strongs/h/h7586.md) also among the [nāḇî'](../../strongs/h/h5030.md)?

<a name="1samuel_10_12"></a>1Samuel 10:12

And ['iysh](../../strongs/h/h376.md) of the same place ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), But who is their ['ab](../../strongs/h/h1.md)? Therefore it became a [māšāl](../../strongs/h/h4912.md), Is [Šā'ûl](../../strongs/h/h7586.md) also among the [nāḇî'](../../strongs/h/h5030.md)?

<a name="1samuel_10_13"></a>1Samuel 10:13

And when he had made a [kalah](../../strongs/h/h3615.md) of [nāḇā'](../../strongs/h/h5012.md), he [bow'](../../strongs/h/h935.md) to the [bāmâ](../../strongs/h/h1116.md).

<a name="1samuel_10_14"></a>1Samuel 10:14

And [Šā'ûl](../../strongs/h/h7586.md) [dôḏ](../../strongs/h/h1730.md) ['āmar](../../strongs/h/h559.md) unto him and to his [naʿar](../../strongs/h/h5288.md), Whither [halak](../../strongs/h/h1980.md) ye? And he ['āmar](../../strongs/h/h559.md), To [bāqaš](../../strongs/h/h1245.md) the ['āṯôn](../../strongs/h/h860.md): and when we [ra'ah](../../strongs/h/h7200.md) that they were no where, we [bow'](../../strongs/h/h935.md) to [Šᵊmû'Ēl](../../strongs/h/h8050.md).

<a name="1samuel_10_15"></a>1Samuel 10:15

And [Šā'ûl](../../strongs/h/h7586.md) [dôḏ](../../strongs/h/h1730.md) ['āmar](../../strongs/h/h559.md), [nāḡaḏ](../../strongs/h/h5046.md) me, I pray thee, what [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) unto you.

<a name="1samuel_10_16"></a>1Samuel 10:16

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto his [dôḏ](../../strongs/h/h1730.md), He [nāḡaḏ](../../strongs/h/h5046.md) us [nāḡaḏ](../../strongs/h/h5046.md) that the ['āṯôn](../../strongs/h/h860.md) were [māṣā'](../../strongs/h/h4672.md). But of the [dabar](../../strongs/h/h1697.md) of the [mᵊlûḵâ](../../strongs/h/h4410.md), whereof [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md), he [nāḡaḏ](../../strongs/h/h5046.md) him not.

<a name="1samuel_10_17"></a>1Samuel 10:17

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [ṣāʿaq](../../strongs/h/h6817.md) the ['am](../../strongs/h/h5971.md) together unto [Yĕhovah](../../strongs/h/h3068.md) to [Miṣpâ](../../strongs/h/h4709.md);

<a name="1samuel_10_18"></a>1Samuel 10:18

And ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), I [ʿālâ](../../strongs/h/h5927.md) [Yisra'el](../../strongs/h/h3478.md) out of [Mitsrayim](../../strongs/h/h4714.md), and [natsal](../../strongs/h/h5337.md) you out of the [yad](../../strongs/h/h3027.md) of the [Mitsrayim](../../strongs/h/h4714.md), and out of the [yad](../../strongs/h/h3027.md) of all [mamlāḵâ](../../strongs/h/h4467.md), and of them that [lāḥaṣ](../../strongs/h/h3905.md) you:

<a name="1samuel_10_19"></a>1Samuel 10:19

And ye have this [yowm](../../strongs/h/h3117.md) [mā'as](../../strongs/h/h3988.md) your ['Elohiym](../../strongs/h/h430.md), who himself [yasha'](../../strongs/h/h3467.md) you out of all your [ra'](../../strongs/h/h7451.md) and your [tsarah](../../strongs/h/h6869.md); and ye have ['āmar](../../strongs/h/h559.md) unto him, Nay, but [śûm](../../strongs/h/h7760.md) a [melek](../../strongs/h/h4428.md) over us. Now therefore [yatsab](../../strongs/h/h3320.md) yourselves [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) by your [shebet](../../strongs/h/h7626.md), and by your thousands.

<a name="1samuel_10_20"></a>1Samuel 10:20

And when [Šᵊmû'Ēl](../../strongs/h/h8050.md) had caused all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) to [qāraḇ](../../strongs/h/h7126.md), the [shebet](../../strongs/h/h7626.md) of [Binyāmîn](../../strongs/h/h1144.md) was [lāḵaḏ](../../strongs/h/h3920.md).

<a name="1samuel_10_21"></a>1Samuel 10:21

When he had caused the [shebet](../../strongs/h/h7626.md) of [Binyāmîn](../../strongs/h/h1144.md) to [qāraḇ](../../strongs/h/h7126.md) by their [mišpāḥâ](../../strongs/h/h4940.md), the [mišpāḥâ](../../strongs/h/h4940.md) of [maṭrî](../../strongs/h/h4309.md) was [lāḵaḏ](../../strongs/h/h3920.md), and [Šā'ûl](../../strongs/h/h7586.md) the [ben](../../strongs/h/h1121.md) of [Qîš](../../strongs/h/h7027.md) was [lāḵaḏ](../../strongs/h/h3920.md): and when they [bāqaš](../../strongs/h/h1245.md) him, he could not be [māṣā'](../../strongs/h/h4672.md).

<a name="1samuel_10_22"></a>1Samuel 10:22

Therefore they [sha'al](../../strongs/h/h7592.md) of [Yĕhovah](../../strongs/h/h3068.md) further, if the ['iysh](../../strongs/h/h376.md) should yet [bow'](../../strongs/h/h935.md) thither. And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), Behold, he hath [chaba'](../../strongs/h/h2244.md) himself among the [kĕliy](../../strongs/h/h3627.md).

<a name="1samuel_10_23"></a>1Samuel 10:23

And they [rûṣ](../../strongs/h/h7323.md) and [laqach](../../strongs/h/h3947.md) him thence: and when he [yatsab](../../strongs/h/h3320.md) [tavek](../../strongs/h/h8432.md) the ['am](../../strongs/h/h5971.md), he was [gāḇah](../../strongs/h/h1361.md) than any of the ['am](../../strongs/h/h5971.md) from his [šᵊḵem](../../strongs/h/h7926.md) and [maʿal](../../strongs/h/h4605.md).

<a name="1samuel_10_24"></a>1Samuel 10:24

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) to all the ['am](../../strongs/h/h5971.md), [ra'ah](../../strongs/h/h7200.md) ye him whom [Yĕhovah](../../strongs/h/h3068.md) hath [bāḥar](../../strongs/h/h977.md), that there is none like him among all the ['am](../../strongs/h/h5971.md)? And all the ['am](../../strongs/h/h5971.md) [rûaʿ](../../strongs/h/h7321.md), and ['āmar](../../strongs/h/h559.md), God [ḥāyâ](../../strongs/h/h2421.md) the [melek](../../strongs/h/h4428.md).

<a name="1samuel_10_25"></a>1Samuel 10:25

Then [Šᵊmû'Ēl](../../strongs/h/h8050.md) [dabar](../../strongs/h/h1696.md) the ['am](../../strongs/h/h5971.md) the [mishpat](../../strongs/h/h4941.md) of the [mᵊlûḵâ](../../strongs/h/h4410.md), and [kāṯaḇ](../../strongs/h/h3789.md) it in a [sēp̄er](../../strongs/h/h5612.md), and laid it [yānaḥ](../../strongs/h/h3240.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md). And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [shalach](../../strongs/h/h7971.md) all the ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), every ['iysh](../../strongs/h/h376.md) to his [bayith](../../strongs/h/h1004.md).

<a name="1samuel_10_26"></a>1Samuel 10:26

And [Šā'ûl](../../strongs/h/h7586.md) also [halak](../../strongs/h/h1980.md) [bayith](../../strongs/h/h1004.md) to [giḇʿâ](../../strongs/h/h1390.md); and there [yālaḵ](../../strongs/h/h3212.md) with him a band of [ḥayil](../../strongs/h/h2428.md), whose [leb](../../strongs/h/h3820.md) ['Elohiym](../../strongs/h/h430.md) had [naga'](../../strongs/h/h5060.md).

<a name="1samuel_10_27"></a>1Samuel 10:27

But the [ben](../../strongs/h/h1121.md) of [beliya'al](../../strongs/h/h1100.md) ['āmar](../../strongs/h/h559.md), How shall this man [yasha'](../../strongs/h/h3467.md) us? And they [bazah](../../strongs/h/h959.md) him, and [bow'](../../strongs/h/h935.md) him no [minchah](../../strongs/h/h4503.md). But he held his [ḥāraš](../../strongs/h/h2790.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 9](1samuel_9.md) - [1Samuel 11](1samuel_11.md)