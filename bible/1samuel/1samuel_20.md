# [1Samuel 20](https://www.blueletterbible.org/kjv/1samuel/20)

<a name="1samuel_20_1"></a>1Samuel 20:1

And [Dāviḏ](../../strongs/h/h1732.md) [bāraḥ](../../strongs/h/h1272.md) from [Nāvîṯ](../../strongs/h/h5121.md) in [rāmâ](../../strongs/h/h7414.md), and [bow'](../../strongs/h/h935.md) and ['āmar](../../strongs/h/h559.md) [paniym](../../strongs/h/h6440.md) [Yᵊhônāṯān](../../strongs/h/h3083.md), What have I ['asah](../../strongs/h/h6213.md)? what is mine ['avon](../../strongs/h/h5771.md)? and what is my [chatta'ath](../../strongs/h/h2403.md) [paniym](../../strongs/h/h6440.md) thy ['ab](../../strongs/h/h1.md), that he [bāqaš](../../strongs/h/h1245.md) my [nephesh](../../strongs/h/h5315.md)?

<a name="1samuel_20_2"></a>1Samuel 20:2

And he ['āmar](../../strongs/h/h559.md) unto him, [ḥālîlâ](../../strongs/h/h2486.md); thou shalt not [muwth](../../strongs/h/h4191.md): behold, my ['ab](../../strongs/h/h1.md) will ['asah](../../strongs/h/h6213.md) ['asah](../../strongs/h/h6213.md) [dabar](../../strongs/h/h1697.md) either [gadowl](../../strongs/h/h1419.md) or [qāṭān](../../strongs/h/h6996.md), but that he will [gālâ](../../strongs/h/h1540.md) ['ozen](../../strongs/h/h241.md) it me: and why should my ['ab](../../strongs/h/h1.md) [cathar](../../strongs/h/h5641.md) this [dabar](../../strongs/h/h1697.md) from me? it is not so.

<a name="1samuel_20_3"></a>1Samuel 20:3

And [Dāviḏ](../../strongs/h/h1732.md) [shaba'](../../strongs/h/h7650.md) moreover, and ['āmar](../../strongs/h/h559.md), Thy ['ab](../../strongs/h/h1.md) [yada'](../../strongs/h/h3045.md) [yada'](../../strongs/h/h3045.md) that I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thine ['ayin](../../strongs/h/h5869.md); and he ['āmar](../../strongs/h/h559.md), Let not [Yᵊhônāṯān](../../strongs/h/h3083.md) [yada'](../../strongs/h/h3045.md) this, lest he be [ʿāṣaḇ](../../strongs/h/h6087.md): but truly as [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), and as thy [nephesh](../../strongs/h/h5315.md) [chay](../../strongs/h/h2416.md), there is but a [peśaʿ](../../strongs/h/h6587.md) between me and [maveth](../../strongs/h/h4194.md).

<a name="1samuel_20_4"></a>1Samuel 20:4

Then ['āmar](../../strongs/h/h559.md) [Yᵊhônāṯān](../../strongs/h/h3083.md) unto [Dāviḏ](../../strongs/h/h1732.md), Whatsoever thy [nephesh](../../strongs/h/h5315.md) ['āmar](../../strongs/h/h559.md), I will even ['asah](../../strongs/h/h6213.md) it for thee.

<a name="1samuel_20_5"></a>1Samuel 20:5

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhônāṯān](../../strongs/h/h3083.md), Behold, [māḥār](../../strongs/h/h4279.md) is the new [ḥōḏeš](../../strongs/h/h2320.md), and I should not [yashab](../../strongs/h/h3427.md) to [yashab](../../strongs/h/h3427.md) with the [melek](../../strongs/h/h4428.md) at ['akal](../../strongs/h/h398.md): but let me [shalach](../../strongs/h/h7971.md), that I may [cathar](../../strongs/h/h5641.md) myself in the [sadeh](../../strongs/h/h7704.md) unto the third day at ['ereb](../../strongs/h/h6153.md).

<a name="1samuel_20_6"></a>1Samuel 20:6

If thy ['ab](../../strongs/h/h1.md) at [paqad](../../strongs/h/h6485.md) [paqad](../../strongs/h/h6485.md) me, then ['āmar](../../strongs/h/h559.md), [Dāviḏ](../../strongs/h/h1732.md) [sha'al](../../strongs/h/h7592.md) [sha'al](../../strongs/h/h7592.md) leave of me that he might [rûṣ](../../strongs/h/h7323.md) to [Bêṯ leḥem](../../strongs/h/h1035.md) his [ʿîr](../../strongs/h/h5892.md): for there is a [yowm](../../strongs/h/h3117.md) [zebach](../../strongs/h/h2077.md) there for all the [mišpāḥâ](../../strongs/h/h4940.md).

<a name="1samuel_20_7"></a>1Samuel 20:7

If he ['āmar](../../strongs/h/h559.md) thus, It is [towb](../../strongs/h/h2896.md); thy ['ebed](../../strongs/h/h5650.md) shall have [shalowm](../../strongs/h/h7965.md): but if he be [ḥārâ](../../strongs/h/h2734.md) [ḥārâ](../../strongs/h/h2734.md), then be [yada'](../../strongs/h/h3045.md) that [ra'](../../strongs/h/h7451.md) is [kalah](../../strongs/h/h3615.md) by him.

<a name="1samuel_20_8"></a>1Samuel 20:8

Therefore thou shalt ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) with thy ['ebed](../../strongs/h/h5650.md); for thou hast [bow'](../../strongs/h/h935.md) thy ['ebed](../../strongs/h/h5650.md) into a [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) with thee: notwithstanding, if there be in me ['avon](../../strongs/h/h5771.md), [muwth](../../strongs/h/h4191.md) me thyself; for why shouldest thou [bow'](../../strongs/h/h935.md) me to thy ['ab](../../strongs/h/h1.md)?

<a name="1samuel_20_9"></a>1Samuel 20:9

And [Yᵊhônāṯān](../../strongs/h/h3083.md) ['āmar](../../strongs/h/h559.md), Far be it from [ḥālîlâ](../../strongs/h/h2486.md): for if I [yada'](../../strongs/h/h3045.md) [yada'](../../strongs/h/h3045.md) that [ra'](../../strongs/h/h7451.md) were [kalah](../../strongs/h/h3615.md) by my ['ab](../../strongs/h/h1.md) to [bow'](../../strongs/h/h935.md) upon thee, then would not I [nāḡaḏ](../../strongs/h/h5046.md) it thee?

<a name="1samuel_20_10"></a>1Samuel 20:10

Then ['āmar](../../strongs/h/h559.md) [Dāviḏ](../../strongs/h/h1732.md) to [Yᵊhônāṯān](../../strongs/h/h3083.md), Who shall [nāḡaḏ](../../strongs/h/h5046.md) me? or what if thy ['ab](../../strongs/h/h1.md) ['anah](../../strongs/h/h6030.md) thee [qāšê](../../strongs/h/h7186.md)?

<a name="1samuel_20_11"></a>1Samuel 20:11

And [Yᵊhônāṯān](../../strongs/h/h3083.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), [yālaḵ](../../strongs/h/h3212.md), and let us [yāṣā'](../../strongs/h/h3318.md) into the [sadeh](../../strongs/h/h7704.md). And they [yāṣā'](../../strongs/h/h3318.md) both of them into the [sadeh](../../strongs/h/h7704.md).

<a name="1samuel_20_12"></a>1Samuel 20:12

And [Yᵊhônāṯān](../../strongs/h/h3083.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), when I have [chaqar](../../strongs/h/h2713.md) my ['ab](../../strongs/h/h1.md) about [māḥār](../../strongs/h/h4279.md) any [ʿēṯ](../../strongs/h/h6256.md), or the third day, and, behold, if there be [ṭôḇ](../../strongs/h/h2895.md) toward [Dāviḏ](../../strongs/h/h1732.md), and I then [shalach](../../strongs/h/h7971.md) not unto thee, and [gālâ](../../strongs/h/h1540.md) ['ozen](../../strongs/h/h241.md) it thee;

<a name="1samuel_20_13"></a>1Samuel 20:13

[Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) so and much more to [Yᵊhônāṯān](../../strongs/h/h3083.md): but if it [yatab](../../strongs/h/h3190.md) my ['ab](../../strongs/h/h1.md) to do thee [ra'](../../strongs/h/h7451.md), then I will [gālâ](../../strongs/h/h1540.md) ['ozen](../../strongs/h/h241.md) it thee, and [shalach](../../strongs/h/h7971.md) thee, that thou mayest [halak](../../strongs/h/h1980.md) in [shalowm](../../strongs/h/h7965.md): and [Yĕhovah](../../strongs/h/h3068.md) be with thee, as he hath been with my ['ab](../../strongs/h/h1.md).

<a name="1samuel_20_14"></a>1Samuel 20:14

And thou shalt not only while yet I [chay](../../strongs/h/h2416.md) ['asah](../../strongs/h/h6213.md) me the [checed](../../strongs/h/h2617.md) of [Yĕhovah](../../strongs/h/h3068.md), that I [muwth](../../strongs/h/h4191.md) not:

<a name="1samuel_20_15"></a>1Samuel 20:15

But also thou shalt not [karath](../../strongs/h/h3772.md) thy [checed](../../strongs/h/h2617.md) from my [bayith](../../strongs/h/h1004.md) ['owlam](../../strongs/h/h5769.md): no, not when [Yĕhovah](../../strongs/h/h3068.md) hath [karath](../../strongs/h/h3772.md) the ['oyeb](../../strongs/h/h341.md) of [Dāviḏ](../../strongs/h/h1732.md) every ['iysh](../../strongs/h/h376.md) from the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="1samuel_20_16"></a>1Samuel 20:16

So [Yᵊhônāṯān](../../strongs/h/h3083.md) [karath](../../strongs/h/h3772.md) with the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md), Let [Yĕhovah](../../strongs/h/h3068.md) even [bāqaš](../../strongs/h/h1245.md) it at the [yad](../../strongs/h/h3027.md) of [Dāviḏ](../../strongs/h/h1732.md) ['oyeb](../../strongs/h/h341.md).

<a name="1samuel_20_17"></a>1Samuel 20:17

And [Yᵊhônāṯān](../../strongs/h/h3083.md) caused [Dāviḏ](../../strongs/h/h1732.md) to [shaba'](../../strongs/h/h7650.md) again, because he ['ahăḇâ](../../strongs/h/h160.md) him: for he ['ahab](../../strongs/h/h157.md) him as he ['ahăḇâ](../../strongs/h/h160.md) his own [nephesh](../../strongs/h/h5315.md).

<a name="1samuel_20_18"></a>1Samuel 20:18

Then [Yᵊhônāṯān](../../strongs/h/h3083.md) ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), To [māḥār](../../strongs/h/h4279.md) is the [ḥōḏeš](../../strongs/h/h2320.md): and thou shalt be [paqad](../../strongs/h/h6485.md), because thy [môšāḇ](../../strongs/h/h4186.md) will be [paqad](../../strongs/h/h6485.md).

<a name="1samuel_20_19"></a>1Samuel 20:19

And when thou hast stayed three [šālaš](../../strongs/h/h8027.md), then thou shalt [yarad](../../strongs/h/h3381.md) [me'od](../../strongs/h/h3966.md), and [bow'](../../strongs/h/h935.md) to the [maqowm](../../strongs/h/h4725.md) where thou didst [cathar](../../strongs/h/h5641.md) thyself [yowm](../../strongs/h/h3117.md) the [ma'aseh](../../strongs/h/h4639.md) was in hand, and shalt [yashab](../../strongs/h/h3427.md) by the ['eben](../../strongs/h/h68.md) ['Ezel](../../strongs/h/h237.md).

<a name="1samuel_20_20"></a>1Samuel 20:20

And I will [yārâ](../../strongs/h/h3384.md) three [chets](../../strongs/h/h2671.md) on the [ṣaḏ](../../strongs/h/h6654.md) thereof, as though I [shalach](../../strongs/h/h7971.md) at a [maṭṭārâ](../../strongs/h/h4307.md).

<a name="1samuel_20_21"></a>1Samuel 20:21

And, behold, I will [shalach](../../strongs/h/h7971.md) a [naʿar](../../strongs/h/h5288.md), [yālaḵ](../../strongs/h/h3212.md), [māṣā'](../../strongs/h/h4672.md) the [chets](../../strongs/h/h2671.md). If I ['āmar](../../strongs/h/h559.md) ['āmar](../../strongs/h/h559.md) unto the [naʿar](../../strongs/h/h5288.md), Behold, the [chets](../../strongs/h/h2671.md) are on this side of thee, [laqach](../../strongs/h/h3947.md) them; then [bow'](../../strongs/h/h935.md) thou: for there is [shalowm](../../strongs/h/h7965.md) to thee, and no [dabar](../../strongs/h/h1697.md); as [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md).

<a name="1samuel_20_22"></a>1Samuel 20:22

But if I ['āmar](../../strongs/h/h559.md) thus unto the [ʿelem](../../strongs/h/h5958.md), Behold, the [chets](../../strongs/h/h2671.md) are [hāl'â](../../strongs/h/h1973.md) thee; go thy [yālaḵ](../../strongs/h/h3212.md): for [Yĕhovah](../../strongs/h/h3068.md) hath sent thee [shalach](../../strongs/h/h7971.md).

<a name="1samuel_20_23"></a>1Samuel 20:23

And the [dabar](../../strongs/h/h1697.md) which thou and I have [dabar](../../strongs/h/h1696.md) of, behold, [Yĕhovah](../../strongs/h/h3068.md) be between thee and me ['owlam](../../strongs/h/h5769.md).

<a name="1samuel_20_24"></a>1Samuel 20:24

So [Dāviḏ](../../strongs/h/h1732.md) [cathar](../../strongs/h/h5641.md) himself in the [sadeh](../../strongs/h/h7704.md): and when the [ḥōḏeš](../../strongs/h/h2320.md) was come, the [melek](../../strongs/h/h4428.md) [yashab](../../strongs/h/h3427.md) him to ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md).

<a name="1samuel_20_25"></a>1Samuel 20:25

And the [melek](../../strongs/h/h4428.md) [yashab](../../strongs/h/h3427.md) upon his [môšāḇ](../../strongs/h/h4186.md), as at other times, even upon a [môšāḇ](../../strongs/h/h4186.md) by the [qîr](../../strongs/h/h7023.md): and [Yᵊhônāṯān](../../strongs/h/h3083.md) [quwm](../../strongs/h/h6965.md), and ['Aḇnēr](../../strongs/h/h74.md) [yashab](../../strongs/h/h3427.md) by [Šā'ûl](../../strongs/h/h7586.md) [ṣaḏ](../../strongs/h/h6654.md), and [Dāviḏ](../../strongs/h/h1732.md) [maqowm](../../strongs/h/h4725.md) was [paqad](../../strongs/h/h6485.md).

<a name="1samuel_20_26"></a>1Samuel 20:26

Nevertheless [Šā'ûl](../../strongs/h/h7586.md) [dabar](../../strongs/h/h1696.md) not any [mᵊ'ûmâ](../../strongs/h/h3972.md) that [yowm](../../strongs/h/h3117.md): for he ['āmar](../../strongs/h/h559.md), Something hath [miqrê](../../strongs/h/h4745.md) him, he is not [tahowr](../../strongs/h/h2889.md); surely he is not [tahowr](../../strongs/h/h2889.md).

<a name="1samuel_20_27"></a>1Samuel 20:27

And it came to pass on the [māḥŏrāṯ](../../strongs/h/h4283.md), which was the second day of the [ḥōḏeš](../../strongs/h/h2320.md), that [Dāviḏ](../../strongs/h/h1732.md) [maqowm](../../strongs/h/h4725.md) was [paqad](../../strongs/h/h6485.md): and [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhônāṯān](../../strongs/h/h3083.md) his [ben](../../strongs/h/h1121.md), Wherefore [bow'](../../strongs/h/h935.md) not the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md) to [lechem](../../strongs/h/h3899.md), neither [tᵊmôl](../../strongs/h/h8543.md), nor to [yowm](../../strongs/h/h3117.md)?

<a name="1samuel_20_28"></a>1Samuel 20:28

And [Yᵊhônāṯān](../../strongs/h/h3083.md) ['anah](../../strongs/h/h6030.md) [Šā'ûl](../../strongs/h/h7586.md), [Dāviḏ](../../strongs/h/h1732.md) [sha'al](../../strongs/h/h7592.md) [sha'al](../../strongs/h/h7592.md) leave of me to go to [Bêṯ leḥem](../../strongs/h/h1035.md):

<a name="1samuel_20_29"></a>1Samuel 20:29

And he ['āmar](../../strongs/h/h559.md), Let me [shalach](../../strongs/h/h7971.md), I pray thee; for our [mišpāḥâ](../../strongs/h/h4940.md) hath a [zebach](../../strongs/h/h2077.md) in the [ʿîr](../../strongs/h/h5892.md); and my ['ach](../../strongs/h/h251.md), he hath [tsavah](../../strongs/h/h6680.md) me to be there: and now, if I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thine ['ayin](../../strongs/h/h5869.md), let me get [mālaṭ](../../strongs/h/h4422.md), I pray thee, and [ra'ah](../../strongs/h/h7200.md) my ['ach](../../strongs/h/h251.md). Therefore he [bow'](../../strongs/h/h935.md) not unto the [melek](../../strongs/h/h4428.md) [šulḥān](../../strongs/h/h7979.md).

<a name="1samuel_20_30"></a>1Samuel 20:30

Then [Šā'ûl](../../strongs/h/h7586.md) ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md) against [Yᵊhônāṯān](../../strongs/h/h3083.md), and he ['āmar](../../strongs/h/h559.md) unto him, Thou [ben](../../strongs/h/h1121.md) of the [ʿāvâ](../../strongs/h/h5753.md) [mardûṯ](../../strongs/h/h4780.md) woman, do not I [yada'](../../strongs/h/h3045.md) that thou hast [bāḥar](../../strongs/h/h977.md) the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md) to thine own [bšeṯ](../../strongs/h/h1322.md), and unto the [bšeṯ](../../strongs/h/h1322.md) of thy ['em](../../strongs/h/h517.md) [ʿervâ](../../strongs/h/h6172.md)?

<a name="1samuel_20_31"></a>1Samuel 20:31

For as long [yowm](../../strongs/h/h3117.md) the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md) [chayay](../../strongs/h/h2425.md) upon the ['ăḏāmâ](../../strongs/h/h127.md), thou shalt not be [kuwn](../../strongs/h/h3559.md), nor thy [malkuwth](../../strongs/h/h4438.md). Wherefore now [shalach](../../strongs/h/h7971.md) and [laqach](../../strongs/h/h3947.md) him unto me, for he shall surely [ben](../../strongs/h/h1121.md) [maveth](../../strongs/h/h4194.md).

<a name="1samuel_20_32"></a>1Samuel 20:32

And [Yᵊhônāṯān](../../strongs/h/h3083.md) ['anah](../../strongs/h/h6030.md) [Šā'ûl](../../strongs/h/h7586.md) his ['ab](../../strongs/h/h1.md), and ['āmar](../../strongs/h/h559.md) unto him, Wherefore shall he be [muwth](../../strongs/h/h4191.md)? what hath he ['asah](../../strongs/h/h6213.md)?

<a name="1samuel_20_33"></a>1Samuel 20:33

And [Šā'ûl](../../strongs/h/h7586.md) [ṭûl](../../strongs/h/h2904.md) a [ḥănîṯ](../../strongs/h/h2595.md) at him to [nakah](../../strongs/h/h5221.md) him: whereby [Yᵊhônāṯān](../../strongs/h/h3083.md) [yada'](../../strongs/h/h3045.md) that it was [kālâ](../../strongs/h/h3617.md) of his ['ab](../../strongs/h/h1.md) to [muwth](../../strongs/h/h4191.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="1samuel_20_34"></a>1Samuel 20:34

So [Yᵊhônāṯān](../../strongs/h/h3083.md) [quwm](../../strongs/h/h6965.md) from the [šulḥān](../../strongs/h/h7979.md) in [ḥŏrî](../../strongs/h/h2750.md) ['aph](../../strongs/h/h639.md), and did ['akal](../../strongs/h/h398.md) no [lechem](../../strongs/h/h3899.md) the second [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md): for he was [ʿāṣaḇ](../../strongs/h/h6087.md) for [Dāviḏ](../../strongs/h/h1732.md), because his ['ab](../../strongs/h/h1.md) had done him [kālam](../../strongs/h/h3637.md).

<a name="1samuel_20_35"></a>1Samuel 20:35

And it came to pass in the [boqer](../../strongs/h/h1242.md), that [Yᵊhônāṯān](../../strongs/h/h3083.md) [yāṣā'](../../strongs/h/h3318.md) into the [sadeh](../../strongs/h/h7704.md) at the time [môʿēḏ](../../strongs/h/h4150.md) with [Dāviḏ](../../strongs/h/h1732.md), and a [qāṭān](../../strongs/h/h6996.md) [naʿar](../../strongs/h/h5288.md) with him.

<a name="1samuel_20_36"></a>1Samuel 20:36

And he ['āmar](../../strongs/h/h559.md) unto his [naʿar](../../strongs/h/h5288.md), [rûṣ](../../strongs/h/h7323.md), [māṣā'](../../strongs/h/h4672.md) now the [chets](../../strongs/h/h2671.md) which I [yārâ](../../strongs/h/h3384.md). And as the [naʿar](../../strongs/h/h5288.md) [rûṣ](../../strongs/h/h7323.md), he [yārâ](../../strongs/h/h3384.md) an [ḥēṣî](../../strongs/h/h2678.md) ['abar](../../strongs/h/h5674.md) him.

<a name="1samuel_20_37"></a>1Samuel 20:37

And when the [naʿar](../../strongs/h/h5288.md) was [bow'](../../strongs/h/h935.md) to the [maqowm](../../strongs/h/h4725.md) of the [ḥēṣî](../../strongs/h/h2678.md) which [Yᵊhônāṯān](../../strongs/h/h3083.md) had [yārâ](../../strongs/h/h3384.md), [Yᵊhônāṯān](../../strongs/h/h3083.md) [qara'](../../strongs/h/h7121.md) ['aḥar](../../strongs/h/h310.md) the [naʿar](../../strongs/h/h5288.md), and ['āmar](../../strongs/h/h559.md), Is not the [ḥēṣî](../../strongs/h/h2678.md) [hāl'â](../../strongs/h/h1973.md) thee?

<a name="1samuel_20_38"></a>1Samuel 20:38

And [Yᵊhônāṯān](../../strongs/h/h3083.md) [qara'](../../strongs/h/h7121.md) ['aḥar](../../strongs/h/h310.md) the [naʿar](../../strongs/h/h5288.md), Make [mᵊhērâ](../../strongs/h/h4120.md), [ḥûš](../../strongs/h/h2363.md), ['amad](../../strongs/h/h5975.md) not. And [Yᵊhônāṯān](../../strongs/h/h3083.md) [naʿar](../../strongs/h/h5288.md) [lāqaṭ](../../strongs/h/h3950.md) the [chets](../../strongs/h/h2671.md) [ḥēṣî](../../strongs/h/h2678.md), and [bow'](../../strongs/h/h935.md) to his ['adown](../../strongs/h/h113.md).

<a name="1samuel_20_39"></a>1Samuel 20:39

But the [naʿar](../../strongs/h/h5288.md) [yada'](../../strongs/h/h3045.md) not any [mᵊ'ûmâ](../../strongs/h/h3972.md): only [Yᵊhônāṯān](../../strongs/h/h3083.md) and [Dāviḏ](../../strongs/h/h1732.md) [yada'](../../strongs/h/h3045.md) the [dabar](../../strongs/h/h1697.md).

<a name="1samuel_20_40"></a>1Samuel 20:40

And [Yᵊhônāṯān](../../strongs/h/h3083.md) [nathan](../../strongs/h/h5414.md) his [kĕliy](../../strongs/h/h3627.md) unto his [naʿar](../../strongs/h/h5288.md), and ['āmar](../../strongs/h/h559.md) unto him, [yālaḵ](../../strongs/h/h3212.md), [bow'](../../strongs/h/h935.md) them to the [ʿîr](../../strongs/h/h5892.md).

<a name="1samuel_20_41"></a>1Samuel 20:41

And as soon as the [naʿar](../../strongs/h/h5288.md) was [bow'](../../strongs/h/h935.md), [Dāviḏ](../../strongs/h/h1732.md) [quwm](../../strongs/h/h6965.md) out of a place toward the [neḡeḇ](../../strongs/h/h5045.md), and [naphal](../../strongs/h/h5307.md) on his ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md), and [shachah](../../strongs/h/h7812.md) himself three times: and they [nashaq](../../strongs/h/h5401.md) ['iysh](../../strongs/h/h376.md) [rea'](../../strongs/h/h7453.md), and [bāḵâ](../../strongs/h/h1058.md) ['iysh](../../strongs/h/h376.md) with [rea'](../../strongs/h/h7453.md), until [Dāviḏ](../../strongs/h/h1732.md) [gāḏal](../../strongs/h/h1431.md).

<a name="1samuel_20_42"></a>1Samuel 20:42

And [Yᵊhônāṯān](../../strongs/h/h3083.md) ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), [yālaḵ](../../strongs/h/h3212.md) in [shalowm](../../strongs/h/h7965.md), forasmuch as we have [shaba'](../../strongs/h/h7650.md) both of us in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) be between me and thee, and between my [zera'](../../strongs/h/h2233.md) and thy [zera'](../../strongs/h/h2233.md) ['owlam](../../strongs/h/h5769.md). And he [quwm](../../strongs/h/h6965.md) and [yālaḵ](../../strongs/h/h3212.md): and [Yᵊhônāṯān](../../strongs/h/h3083.md) [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 19](1samuel_19.md) - [1Samuel 21](1samuel_21.md)