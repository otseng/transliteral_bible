# [1Samuel 16](https://www.blueletterbible.org/kjv/1samuel/16)

<a name="1samuel_16_1"></a>1Samuel 16:1

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Šᵊmû'Ēl](../../strongs/h/h8050.md), How long wilt thou ['āḇal](../../strongs/h/h56.md) for [Šā'ûl](../../strongs/h/h7586.md), seeing I have [mā'as](../../strongs/h/h3988.md) him from [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md)? [mālā'](../../strongs/h/h4390.md) thine [qeren](../../strongs/h/h7161.md) with [šemen](../../strongs/h/h8081.md), and [yālaḵ](../../strongs/h/h3212.md), I will [shalach](../../strongs/h/h7971.md) thee to [Yišay](../../strongs/h/h3448.md) the [bêṯ-hallaḥmî](../../strongs/h/h1022.md): for I have [ra'ah](../../strongs/h/h7200.md) me a [melek](../../strongs/h/h4428.md) among his [ben](../../strongs/h/h1121.md).

<a name="1samuel_16_2"></a>1Samuel 16:2

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md), How can I [yālaḵ](../../strongs/h/h3212.md)? if [Šā'ûl](../../strongs/h/h7586.md) [shama'](../../strongs/h/h8085.md) it, he will [harag](../../strongs/h/h2026.md) me. And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) a [bāqār](../../strongs/h/h1241.md) [ʿeḡlâ](../../strongs/h/h5697.md) with [yad](../../strongs/h/h3027.md), and ['āmar](../../strongs/h/h559.md), I am [bow'](../../strongs/h/h935.md) to [zabach](../../strongs/h/h2076.md) to [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_16_3"></a>1Samuel 16:3

And [qara'](../../strongs/h/h7121.md) [Yišay](../../strongs/h/h3448.md) to the [zebach](../../strongs/h/h2077.md), and I will [yada'](../../strongs/h/h3045.md) thee what thou shalt ['asah](../../strongs/h/h6213.md): and thou shalt [māšaḥ](../../strongs/h/h4886.md) unto me him whom I ['āmar](../../strongs/h/h559.md) unto thee.

<a name="1samuel_16_4"></a>1Samuel 16:4

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['asah](../../strongs/h/h6213.md) that which [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md), and [bow'](../../strongs/h/h935.md) to [Bêṯ leḥem](../../strongs/h/h1035.md). And the [zāqēn](../../strongs/h/h2205.md) of the [ʿîr](../../strongs/h/h5892.md) [ḥārēḏ](../../strongs/h/h2729.md) at his [qārā'](../../strongs/h/h7125.md), and ['āmar](../../strongs/h/h559.md), [bow'](../../strongs/h/h935.md) thou [shalowm](../../strongs/h/h7965.md)?

<a name="1samuel_16_5"></a>1Samuel 16:5

And he ['āmar](../../strongs/h/h559.md), [shalowm](../../strongs/h/h7965.md): I am [bow'](../../strongs/h/h935.md) to [zabach](../../strongs/h/h2076.md) unto [Yĕhovah](../../strongs/h/h3068.md): [qadash](../../strongs/h/h6942.md) yourselves, and [bow'](../../strongs/h/h935.md) with me to the [zebach](../../strongs/h/h2077.md). And he [qadash](../../strongs/h/h6942.md) [Yišay](../../strongs/h/h3448.md) and his [ben](../../strongs/h/h1121.md), and [qara'](../../strongs/h/h7121.md) them to the [zebach](../../strongs/h/h2077.md).

<a name="1samuel_16_6"></a>1Samuel 16:6

And it came to pass, when they were [bow'](../../strongs/h/h935.md), that he [ra'ah](../../strongs/h/h7200.md) on ['Ĕlî'āḇ](../../strongs/h/h446.md), and ['āmar](../../strongs/h/h559.md), Surely [Yĕhovah](../../strongs/h/h3068.md) [mashiyach](../../strongs/h/h4899.md) is before him.

<a name="1samuel_16_7"></a>1Samuel 16:7

But [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Šᵊmû'Ēl](../../strongs/h/h8050.md), [nabat](../../strongs/h/h5027.md) not on his [mar'ê](../../strongs/h/h4758.md), or on the [gāḇōha](../../strongs/h/h1364.md) of his [qômâ](../../strongs/h/h6967.md); because I have [mā'as](../../strongs/h/h3988.md) him: for the LORD seeth not as ['āḏām](../../strongs/h/h120.md) [ra'ah](../../strongs/h/h7200.md); for ['āḏām](../../strongs/h/h120.md) [ra'ah](../../strongs/h/h7200.md) on the outward ['ayin](../../strongs/h/h5869.md), but [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) on the [lebab](../../strongs/h/h3824.md).

<a name="1samuel_16_8"></a>1Samuel 16:8

Then [Yišay](../../strongs/h/h3448.md) [qara'](../../strongs/h/h7121.md) ['Ăḇînāḏāḇ](../../strongs/h/h41.md), and made him ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md). And he ['āmar](../../strongs/h/h559.md), Neither hath [Yĕhovah](../../strongs/h/h3068.md) [bāḥar](../../strongs/h/h977.md) this.

<a name="1samuel_16_9"></a>1Samuel 16:9

Then [Yišay](../../strongs/h/h3448.md) made [Šammâ](../../strongs/h/h8048.md) to ['abar](../../strongs/h/h5674.md). And he ['āmar](../../strongs/h/h559.md), Neither hath [Yĕhovah](../../strongs/h/h3068.md) [bāḥar](../../strongs/h/h977.md) this.

<a name="1samuel_16_10"></a>1Samuel 16:10

Again, [Yišay](../../strongs/h/h3448.md) made seven of his [ben](../../strongs/h/h1121.md) to ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md). And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) unto [Yišay](../../strongs/h/h3448.md), [Yĕhovah](../../strongs/h/h3068.md) hath not [bāḥar](../../strongs/h/h977.md) these.

<a name="1samuel_16_11"></a>1Samuel 16:11

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) unto [Yišay](../../strongs/h/h3448.md), Are here [tamam](../../strongs/h/h8552.md) thy [naʿar](../../strongs/h/h5288.md)? And he ['āmar](../../strongs/h/h559.md), There [šā'ar](../../strongs/h/h7604.md) yet the [qāṭān](../../strongs/h/h6996.md), and, behold, he [ra'ah](../../strongs/h/h7462.md) the [tso'n](../../strongs/h/h6629.md). And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) unto [Yišay](../../strongs/h/h3448.md), [shalach](../../strongs/h/h7971.md) and [laqach](../../strongs/h/h3947.md) him: for we will not [cabab](../../strongs/h/h5437.md) till he [bow'](../../strongs/h/h935.md) hither.

<a name="1samuel_16_12"></a>1Samuel 16:12

And he [shalach](../../strongs/h/h7971.md), and [bow'](../../strongs/h/h935.md) him. Now he was ['aḏmōnî](../../strongs/h/h132.md), and withal of a [yāp̄ê](../../strongs/h/h3303.md) ['ayin](../../strongs/h/h5869.md), and [towb](../../strongs/h/h2896.md) to look [rŏ'î](../../strongs/h/h7210.md). And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md), [māšaḥ](../../strongs/h/h4886.md) him: for this is he.

<a name="1samuel_16_13"></a>1Samuel 16:13

Then [Šᵊmû'Ēl](../../strongs/h/h8050.md) [laqach](../../strongs/h/h3947.md) the [qeren](../../strongs/h/h7161.md) of [šemen](../../strongs/h/h8081.md), and [māšaḥ](../../strongs/h/h4886.md) him in the [qereḇ](../../strongs/h/h7130.md) of his ['ach](../../strongs/h/h251.md): and the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) [tsalach](../../strongs/h/h6743.md) upon [Dāviḏ](../../strongs/h/h1732.md) from that [yowm](../../strongs/h/h3117.md) [maʿal](../../strongs/h/h4605.md). So [Šᵊmû'Ēl](../../strongs/h/h8050.md) [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) to [rāmâ](../../strongs/h/h7414.md).

<a name="1samuel_16_14"></a>1Samuel 16:14

But the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) [cuwr](../../strongs/h/h5493.md) from [Šā'ûl](../../strongs/h/h7586.md), and a [ra'](../../strongs/h/h7451.md) [ruwach](../../strongs/h/h7307.md) from [Yĕhovah](../../strongs/h/h3068.md) [ba'ath](../../strongs/h/h1204.md) him.

<a name="1samuel_16_15"></a>1Samuel 16:15

And [Šā'ûl](../../strongs/h/h7586.md) ['ebed](../../strongs/h/h5650.md) ['āmar](../../strongs/h/h559.md) unto him, Behold now, a [ra'](../../strongs/h/h7451.md) [ruwach](../../strongs/h/h7307.md) from ['Elohiym](../../strongs/h/h430.md) [ba'ath](../../strongs/h/h1204.md) thee.

<a name="1samuel_16_16"></a>1Samuel 16:16

Let our ['adown](../../strongs/h/h113.md) now ['āmar](../../strongs/h/h559.md) thy ['ebed](../../strongs/h/h5650.md), which are [paniym](../../strongs/h/h6440.md) thee, to [bāqaš](../../strongs/h/h1245.md) an ['iysh](../../strongs/h/h376.md), who is a [yada'](../../strongs/h/h3045.md) [nāḡan](../../strongs/h/h5059.md) on a [kinnôr](../../strongs/h/h3658.md): and it shall come to pass, when the [ra'](../../strongs/h/h7451.md) [ruwach](../../strongs/h/h7307.md) from ['Elohiym](../../strongs/h/h430.md) is upon thee, that he shall [nāḡan](../../strongs/h/h5059.md) with his [yad](../../strongs/h/h3027.md), and thou shalt be [ṭôḇ](../../strongs/h/h2895.md).

<a name="1samuel_16_17"></a>1Samuel 16:17

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md) unto his ['ebed](../../strongs/h/h5650.md), [ra'ah](../../strongs/h/h7200.md) me now an ['iysh](../../strongs/h/h376.md) that can [nāḡan](../../strongs/h/h5059.md) [yatab](../../strongs/h/h3190.md), and [bow'](../../strongs/h/h935.md) him to me.

<a name="1samuel_16_18"></a>1Samuel 16:18

Then ['anah](../../strongs/h/h6030.md) one of the [naʿar](../../strongs/h/h5288.md), and ['āmar](../../strongs/h/h559.md), Behold, I have [ra'ah](../../strongs/h/h7200.md) a [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md) the [bêṯ-hallaḥmî](../../strongs/h/h1022.md), that is [yada'](../../strongs/h/h3045.md) in [nāḡan](../../strongs/h/h5059.md), and a [gibôr](../../strongs/h/h1368.md) valiant [ḥayil](../../strongs/h/h2428.md), and an ['iysh](../../strongs/h/h376.md) of [milḥāmâ](../../strongs/h/h4421.md), and [bîn](../../strongs/h/h995.md) in [dabar](../../strongs/h/h1697.md), and a [tō'ar](../../strongs/h/h8389.md) ['iysh](../../strongs/h/h376.md), and [Yĕhovah](../../strongs/h/h3068.md) is with him.

<a name="1samuel_16_19"></a>1Samuel 16:19

Wherefore [Šā'ûl](../../strongs/h/h7586.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) unto [Yišay](../../strongs/h/h3448.md), and ['āmar](../../strongs/h/h559.md), [shalach](../../strongs/h/h7971.md) me [Dāviḏ](../../strongs/h/h1732.md) thy [ben](../../strongs/h/h1121.md), which is with the [tso'n](../../strongs/h/h6629.md).

<a name="1samuel_16_20"></a>1Samuel 16:20

And [Yišay](../../strongs/h/h3448.md) [laqach](../../strongs/h/h3947.md) a [chamowr](../../strongs/h/h2543.md) laden with [lechem](../../strongs/h/h3899.md), and a [nō'ḏ](../../strongs/h/h4997.md) of [yayin](../../strongs/h/h3196.md), and a [gᵊḏî](../../strongs/h/h1423.md) [ʿēz](../../strongs/h/h5795.md), and [shalach](../../strongs/h/h7971.md) them [yad](../../strongs/h/h3027.md) [Dāviḏ](../../strongs/h/h1732.md) his [ben](../../strongs/h/h1121.md) unto [Šā'ûl](../../strongs/h/h7586.md).

<a name="1samuel_16_21"></a>1Samuel 16:21

And [Dāviḏ](../../strongs/h/h1732.md) [bow'](../../strongs/h/h935.md) to [Šā'ûl](../../strongs/h/h7586.md), and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) him: and he ['ahab](../../strongs/h/h157.md) him [me'od](../../strongs/h/h3966.md); and he became his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md).

<a name="1samuel_16_22"></a>1Samuel 16:22

And [Šā'ûl](../../strongs/h/h7586.md) [shalach](../../strongs/h/h7971.md) to [Yišay](../../strongs/h/h3448.md), ['āmar](../../strongs/h/h559.md), Let [Dāviḏ](../../strongs/h/h1732.md), I pray thee, ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) me; for he hath [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in my ['ayin](../../strongs/h/h5869.md).

<a name="1samuel_16_23"></a>1Samuel 16:23

And it came to pass, when the [ruwach](../../strongs/h/h7307.md) from ['Elohiym](../../strongs/h/h430.md) was upon [Šā'ûl](../../strongs/h/h7586.md), that [Dāviḏ](../../strongs/h/h1732.md) [laqach](../../strongs/h/h3947.md) a [kinnôr](../../strongs/h/h3658.md), and [nāḡan](../../strongs/h/h5059.md) with his [yad](../../strongs/h/h3027.md): so [Šā'ûl](../../strongs/h/h7586.md) was [rāvaḥ](../../strongs/h/h7304.md), and was [ṭôḇ](../../strongs/h/h2895.md), and the [ra'](../../strongs/h/h7451.md) [ruwach](../../strongs/h/h7307.md) [cuwr](../../strongs/h/h5493.md) from him.

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 15](1samuel_15.md) - [1Samuel 17](1samuel_17.md)