# [1Samuel 7](https://www.blueletterbible.org/kjv/1samuel/7)

<a name="1samuel_7_1"></a>1Samuel 7:1

And the ['enowsh](../../strongs/h/h582.md) of [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md) [bow'](../../strongs/h/h935.md), and [ʿālâ](../../strongs/h/h5927.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md), and [bow'](../../strongs/h/h935.md) it into the [bayith](../../strongs/h/h1004.md) of ['Ăḇînāḏāḇ](../../strongs/h/h41.md) in the [giḇʿâ](../../strongs/h/h1389.md), and [qadash](../../strongs/h/h6942.md) ['Elʿāzār](../../strongs/h/h499.md) his [ben](../../strongs/h/h1121.md) to [shamar](../../strongs/h/h8104.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_7_2"></a>1Samuel 7:2

And it came to pass, [yowm](../../strongs/h/h3117.md) the ['ārôn](../../strongs/h/h727.md) [yashab](../../strongs/h/h3427.md) in [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md), that the [yowm](../../strongs/h/h3117.md) was [rabah](../../strongs/h/h7235.md); for it was twenty [šānâ](../../strongs/h/h8141.md): and all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) [nāhâ](../../strongs/h/h5091.md) ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_7_3"></a>1Samuel 7:3

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md) unto all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), If ye do [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md) with all your [lebab](../../strongs/h/h3824.md), then put [cuwr](../../strongs/h/h5493.md) the [nēḵār](../../strongs/h/h5236.md) ['Elohiym](../../strongs/h/h430.md) and [ʿAštārōṯ](../../strongs/h/h6252.md) from [tavek](../../strongs/h/h8432.md) you, and [kuwn](../../strongs/h/h3559.md) your [lebab](../../strongs/h/h3824.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['abad](../../strongs/h/h5647.md) him only: and he will [natsal](../../strongs/h/h5337.md) you out of the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1samuel_7_4"></a>1Samuel 7:4

Then the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) did put [cuwr](../../strongs/h/h5493.md) [BaʿAl](../../strongs/h/h1168.md) and [ʿAštārōṯ](../../strongs/h/h6252.md), and ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) only.

<a name="1samuel_7_5"></a>1Samuel 7:5

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['āmar](../../strongs/h/h559.md), [qāḇaṣ](../../strongs/h/h6908.md) all [Yisra'el](../../strongs/h/h3478.md) to [Miṣpê](../../strongs/h/h4708.md), and I will [palal](../../strongs/h/h6419.md) for you unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_7_6"></a>1Samuel 7:6

And they [qāḇaṣ](../../strongs/h/h6908.md) to [Miṣpâ](../../strongs/h/h4709.md), and [šā'aḇ](../../strongs/h/h7579.md) [mayim](../../strongs/h/h4325.md), and [šāp̄aḵ](../../strongs/h/h8210.md) it [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and [ṣûm](../../strongs/h/h6684.md) on that [yowm](../../strongs/h/h3117.md), and ['āmar](../../strongs/h/h559.md) there, We have [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md). And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [shaphat](../../strongs/h/h8199.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) in [Miṣpê](../../strongs/h/h4708.md).

<a name="1samuel_7_7"></a>1Samuel 7:7

And when the [Pᵊlištî](../../strongs/h/h6430.md) [shama'](../../strongs/h/h8085.md) that the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were [qāḇaṣ](../../strongs/h/h6908.md) to [Miṣpê](../../strongs/h/h4708.md), the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [ʿālâ](../../strongs/h/h5927.md) against [Yisra'el](../../strongs/h/h3478.md). And when the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shama'](../../strongs/h/h8085.md) it, they were [yare'](../../strongs/h/h3372.md) [paniym](../../strongs/h/h6440.md) the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1samuel_7_8"></a>1Samuel 7:8

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) to [Šᵊmû'Ēl](../../strongs/h/h8050.md), [ḥāraš](../../strongs/h/h2790.md) not to [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) for us, that he will [yasha'](../../strongs/h/h3467.md) us out of the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1samuel_7_9"></a>1Samuel 7:9

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [laqach](../../strongs/h/h3947.md) a [chalab](../../strongs/h/h2461.md) [ṭālê](../../strongs/h/h2924.md), and [ʿālâ](../../strongs/h/h5927.md) it for a [ʿōlâ](../../strongs/h/h5930.md) [kālîl](../../strongs/h/h3632.md) unto [Yĕhovah](../../strongs/h/h3068.md): and [Šᵊmû'Ēl](../../strongs/h/h8050.md) [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md) for [Yisra'el](../../strongs/h/h3478.md); and [Yĕhovah](../../strongs/h/h3068.md) ['anah](../../strongs/h/h6030.md) him.

<a name="1samuel_7_10"></a>1Samuel 7:10

And as [Šᵊmû'Ēl](../../strongs/h/h8050.md) was offering [ʿālâ](../../strongs/h/h5927.md) the [ʿōlâ](../../strongs/h/h5930.md), the [Pᵊlištî](../../strongs/h/h6430.md) [nāḡaš](../../strongs/h/h5066.md) to [milḥāmâ](../../strongs/h/h4421.md) against [Yisra'el](../../strongs/h/h3478.md): but [Yĕhovah](../../strongs/h/h3068.md) [ra'am](../../strongs/h/h7481.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md) on that [yowm](../../strongs/h/h3117.md) upon the [Pᵊlištî](../../strongs/h/h6430.md), and [hāmam](../../strongs/h/h2000.md) them; and they were [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="1samuel_7_11"></a>1Samuel 7:11

And the ['enowsh](../../strongs/h/h582.md) of [Yisra'el](../../strongs/h/h3478.md) [yāṣā'](../../strongs/h/h3318.md) of [Miṣpâ](../../strongs/h/h4709.md), and [radaph](../../strongs/h/h7291.md) the [Pᵊlištî](../../strongs/h/h6430.md), and [nakah](../../strongs/h/h5221.md) them, until they came under [Bêṯ Kar](../../strongs/h/h1033.md).

<a name="1samuel_7_12"></a>1Samuel 7:12

Then [Šᵊmû'Ēl](../../strongs/h/h8050.md) [laqach](../../strongs/h/h3947.md) an ['eben](../../strongs/h/h68.md), and [śûm](../../strongs/h/h7760.md) it between [Miṣpâ](../../strongs/h/h4709.md) and [Šēn](../../strongs/h/h8129.md), and [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of it ['eḇen hāʿezer](../../strongs/h/h72.md), ['āmar](../../strongs/h/h559.md), Hitherto hath [Yĕhovah](../../strongs/h/h3068.md) [ʿāzar](../../strongs/h/h5826.md) us.

<a name="1samuel_7_13"></a>1Samuel 7:13

So the [Pᵊlištî](../../strongs/h/h6430.md) were [kānaʿ](../../strongs/h/h3665.md), and they [bow'](../../strongs/h/h935.md) no more into the [gᵊḇûl](../../strongs/h/h1366.md) of [Yisra'el](../../strongs/h/h3478.md): and the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) was against the [Pᵊlištî](../../strongs/h/h6430.md) all the [yowm](../../strongs/h/h3117.md) of [Šᵊmû'Ēl](../../strongs/h/h8050.md).

<a name="1samuel_7_14"></a>1Samuel 7:14

And the [ʿîr](../../strongs/h/h5892.md) which the [Pᵊlištî](../../strongs/h/h6430.md) had [laqach](../../strongs/h/h3947.md) from [Yisra'el](../../strongs/h/h3478.md) were [shuwb](../../strongs/h/h7725.md) to [Yisra'el](../../strongs/h/h3478.md), from [ʿEqrôn](../../strongs/h/h6138.md) even unto [Gaṯ](../../strongs/h/h1661.md); and the [gᵊḇûl](../../strongs/h/h1366.md) thereof did [Yisra'el](../../strongs/h/h3478.md) [natsal](../../strongs/h/h5337.md) out of the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md). And there was [shalowm](../../strongs/h/h7965.md) between [Yisra'el](../../strongs/h/h3478.md) and the ['Ĕmōrî](../../strongs/h/h567.md).

<a name="1samuel_7_15"></a>1Samuel 7:15

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md) all the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md).

<a name="1samuel_7_16"></a>1Samuel 7:16

And he [halak](../../strongs/h/h1980.md) [day](../../strongs/h/h1767.md) [šānâ](../../strongs/h/h8141.md) to [šānâ](../../strongs/h/h8141.md) in [cabab](../../strongs/h/h5437.md) to [Bêṯ-'ēl](../../strongs/h/h1008.md), and [Gilgāl](../../strongs/h/h1537.md), and [Miṣpâ](../../strongs/h/h4709.md), and [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md) in all those [maqowm](../../strongs/h/h4725.md).

<a name="1samuel_7_17"></a>1Samuel 7:17

And his [tᵊšûḇâ](../../strongs/h/h8666.md) was to [rāmâ](../../strongs/h/h7414.md); for there was his [bayith](../../strongs/h/h1004.md); and there he [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md); and there he [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 6](1samuel_6.md) - [1Samuel 8](1samuel_8.md)