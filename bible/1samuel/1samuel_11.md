# [1Samuel 11](https://www.blueletterbible.org/kjv/1samuel/11)

<a name="1samuel_11_1"></a>1Samuel 11:1

Then [Nāḥāš](../../strongs/h/h5176.md) the [ʿAmmôn](../../strongs/h/h5984.md) [ʿālâ](../../strongs/h/h5927.md), and [ḥānâ](../../strongs/h/h2583.md) against [Yāḇēš](../../strongs/h/h3003.md) [Gilʿāḏ](../../strongs/h/h1568.md): and all the ['enowsh](../../strongs/h/h582.md) of [Yāḇēš](../../strongs/h/h3003.md) ['āmar](../../strongs/h/h559.md) unto [Nāḥāš](../../strongs/h/h5176.md), [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with us, and we will ['abad](../../strongs/h/h5647.md) thee.

<a name="1samuel_11_2"></a>1Samuel 11:2

And [Nāḥāš](../../strongs/h/h5176.md) the [ʿAmmôn](../../strongs/h/h5984.md) ['āmar](../../strongs/h/h559.md) them, On this condition will I [karath](../../strongs/h/h3772.md) a covenant with you, that I may thrust [nāqar](../../strongs/h/h5365.md) all your [yamiyn](../../strongs/h/h3225.md) ['ayin](../../strongs/h/h5869.md), and [śûm](../../strongs/h/h7760.md) it for a [cherpah](../../strongs/h/h2781.md) upon all [Yisra'el](../../strongs/h/h3478.md).

<a name="1samuel_11_3"></a>1Samuel 11:3

And the [zāqēn](../../strongs/h/h2205.md) of [Yāḇēš](../../strongs/h/h3003.md) ['āmar](../../strongs/h/h559.md) unto him, Give us seven [yowm](../../strongs/h/h3117.md) [rāp̄â](../../strongs/h/h7503.md), that we may [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) unto all the [gᵊḇûl](../../strongs/h/h1366.md) of [Yisra'el](../../strongs/h/h3478.md): and then, if there be no man to [yasha'](../../strongs/h/h3467.md) us, we will [yāṣā'](../../strongs/h/h3318.md) to thee.

<a name="1samuel_11_4"></a>1Samuel 11:4

Then [bow'](../../strongs/h/h935.md) the [mal'ak](../../strongs/h/h4397.md) to [giḇʿâ](../../strongs/h/h1390.md) of [Šā'ûl](../../strongs/h/h7586.md), and [dabar](../../strongs/h/h1696.md) the [dabar](../../strongs/h/h1697.md) in the ['ozen](../../strongs/h/h241.md) of the ['am](../../strongs/h/h5971.md): and all the ['am](../../strongs/h/h5971.md) [nasa'](../../strongs/h/h5375.md) their [qowl](../../strongs/h/h6963.md), and [bāḵâ](../../strongs/h/h1058.md).

<a name="1samuel_11_5"></a>1Samuel 11:5

And, behold, [Šā'ûl](../../strongs/h/h7586.md) [bow'](../../strongs/h/h935.md) ['aḥar](../../strongs/h/h310.md) the [bāqār](../../strongs/h/h1241.md) out of the [sadeh](../../strongs/h/h7704.md); and [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), What aileth the ['am](../../strongs/h/h5971.md) that they [bāḵâ](../../strongs/h/h1058.md)? And they [sāp̄ar](../../strongs/h/h5608.md) him the [dabar](../../strongs/h/h1697.md) of the ['enowsh](../../strongs/h/h582.md) of [Yāḇēš](../../strongs/h/h3003.md).

<a name="1samuel_11_6"></a>1Samuel 11:6

And the [ruwach](../../strongs/h/h7307.md) of ['Elohiym](../../strongs/h/h430.md) [tsalach](../../strongs/h/h6743.md) upon [Šā'ûl](../../strongs/h/h7586.md) when he [shama'](../../strongs/h/h8085.md) those [dabar](../../strongs/h/h1697.md), and his ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md) [me'od](../../strongs/h/h3966.md).

<a name="1samuel_11_7"></a>1Samuel 11:7

And he [laqach](../../strongs/h/h3947.md) a [ṣemeḏ](../../strongs/h/h6776.md) of [bāqār](../../strongs/h/h1241.md), and hewed them in [nāṯaḥ](../../strongs/h/h5408.md), and [shalach](../../strongs/h/h7971.md) them throughout all the [gᵊḇûl](../../strongs/h/h1366.md) of [Yisra'el](../../strongs/h/h3478.md) by the [yad](../../strongs/h/h3027.md) of [mal'ak](../../strongs/h/h4397.md), ['āmar](../../strongs/h/h559.md), Whosoever cometh not [yāṣā'](../../strongs/h/h3318.md) ['aḥar](../../strongs/h/h310.md) [Šā'ûl](../../strongs/h/h7586.md) and ['aḥar](../../strongs/h/h310.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md), so shall it be ['asah](../../strongs/h/h6213.md) unto his [bāqār](../../strongs/h/h1241.md). And the [paḥaḏ](../../strongs/h/h6343.md) of [Yĕhovah](../../strongs/h/h3068.md) [naphal](../../strongs/h/h5307.md) on the ['am](../../strongs/h/h5971.md), and they [yāṣā'](../../strongs/h/h3318.md) with one ['iysh](../../strongs/h/h376.md).

<a name="1samuel_11_8"></a>1Samuel 11:8

And when he [paqad](../../strongs/h/h6485.md) them in [Bezeq](../../strongs/h/h966.md), the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were three hundred thousand, and the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) thirty thousand.

<a name="1samuel_11_9"></a>1Samuel 11:9

And they ['āmar](../../strongs/h/h559.md) unto the [mal'ak](../../strongs/h/h4397.md) that [bow'](../../strongs/h/h935.md), Thus shall ye ['āmar](../../strongs/h/h559.md) unto the ['iysh](../../strongs/h/h376.md) of [Yāḇēš](../../strongs/h/h3003.md) [Gilʿāḏ](../../strongs/h/h1568.md), To [māḥār](../../strongs/h/h4279.md), by that time the [šemeš](../../strongs/h/h8121.md) be [ḥōm](../../strongs/h/h2527.md), ye shall have [tᵊšûʿâ](../../strongs/h/h8668.md). And the [mal'ak](../../strongs/h/h4397.md) [bow'](../../strongs/h/h935.md) and [nāḡaḏ](../../strongs/h/h5046.md) it to the ['enowsh](../../strongs/h/h582.md) of [Yāḇēš](../../strongs/h/h3003.md); and they were [samach](../../strongs/h/h8055.md).

<a name="1samuel_11_10"></a>1Samuel 11:10

Therefore the ['enowsh](../../strongs/h/h582.md) of [Yāḇēš](../../strongs/h/h3003.md) ['āmar](../../strongs/h/h559.md), To [māḥār](../../strongs/h/h4279.md) we will [yāṣā'](../../strongs/h/h3318.md) unto you, and ye shall ['asah](../../strongs/h/h6213.md) with us all that ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md) unto you.

<a name="1samuel_11_11"></a>1Samuel 11:11

And it was so on the [māḥŏrāṯ](../../strongs/h/h4283.md), that [Šā'ûl](../../strongs/h/h7586.md) [śûm](../../strongs/h/h7760.md) the ['am](../../strongs/h/h5971.md) in three [ro'sh](../../strongs/h/h7218.md); and they [bow'](../../strongs/h/h935.md) into the [tavek](../../strongs/h/h8432.md) of the [maḥănê](../../strongs/h/h4264.md) in the [boqer](../../strongs/h/h1242.md) ['ašmurâ](../../strongs/h/h821.md), and [nakah](../../strongs/h/h5221.md) the [ʿAmmôn](../../strongs/h/h5983.md) until the [ḥōm](../../strongs/h/h2527.md) of the [yowm](../../strongs/h/h3117.md): and it came to pass, that they which [šā'ar](../../strongs/h/h7604.md) were [puwts](../../strongs/h/h6327.md), so that two of them were not [šā'ar](../../strongs/h/h7604.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="1samuel_11_12"></a>1Samuel 11:12

And the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md) unto [Šᵊmû'Ēl](../../strongs/h/h8050.md), Who is he that ['āmar](../../strongs/h/h559.md), Shall [Šā'ûl](../../strongs/h/h7586.md) [mālaḵ](../../strongs/h/h4427.md) over us? [nathan](../../strongs/h/h5414.md) the ['enowsh](../../strongs/h/h582.md), that we may put them to [muwth](../../strongs/h/h4191.md).

<a name="1samuel_11_13"></a>1Samuel 11:13

And [Šā'ûl](../../strongs/h/h7586.md) ['āmar](../../strongs/h/h559.md), There shall not an ['iysh](../../strongs/h/h376.md) be put to [muwth](../../strongs/h/h4191.md) this [yowm](../../strongs/h/h3117.md): for to [yowm](../../strongs/h/h3117.md) [Yĕhovah](../../strongs/h/h3068.md) hath ['asah](../../strongs/h/h6213.md) [tᵊšûʿâ](../../strongs/h/h8668.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="1samuel_11_14"></a>1Samuel 11:14

Then ['āmar](../../strongs/h/h559.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md) to the ['am](../../strongs/h/h5971.md), [yālaḵ](../../strongs/h/h3212.md), and let us [yālaḵ](../../strongs/h/h3212.md) to [Gilgāl](../../strongs/h/h1537.md), and [ḥādaš](../../strongs/h/h2318.md) the [mᵊlûḵâ](../../strongs/h/h4410.md) there.

<a name="1samuel_11_15"></a>1Samuel 11:15

And all the ['am](../../strongs/h/h5971.md) [yālaḵ](../../strongs/h/h3212.md) to [Gilgāl](../../strongs/h/h1537.md); and there they made [Šā'ûl](../../strongs/h/h7586.md) [mālaḵ](../../strongs/h/h4427.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) in [Gilgāl](../../strongs/h/h1537.md); and there they [zabach](../../strongs/h/h2076.md) [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); and there [Šā'ûl](../../strongs/h/h7586.md) and all the ['enowsh](../../strongs/h/h582.md) of [Yisra'el](../../strongs/h/h3478.md) [samach](../../strongs/h/h8055.md) [me'od](../../strongs/h/h3966.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 10](1samuel_10.md) - [1Samuel 12](1samuel_12.md)