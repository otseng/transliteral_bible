# [1Samuel 1](https://www.blueletterbible.org/kjv/1samuel/1)

<a name="1samuel_1_1"></a>1Samuel 1:1

Now there was a certain ['iysh](../../strongs/h/h376.md) of [Rmṯym Ṣvp̄Ym](../../strongs/h/h7436.md), of [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), and his [shem](../../strongs/h/h8034.md) was ['Elqānâ](../../strongs/h/h511.md), the [ben](../../strongs/h/h1121.md) of [Yᵊrōḥām](../../strongs/h/h3395.md), the [ben](../../strongs/h/h1121.md) of ['Ĕlîhû](../../strongs/h/h453.md), the [ben](../../strongs/h/h1121.md) of [Tōḥû](../../strongs/h/h8459.md), the [ben](../../strongs/h/h1121.md) of [Ṣûp̄](../../strongs/h/h6689.md), an ['ep̄rāṯî](../../strongs/h/h673.md):

<a name="1samuel_1_2"></a>1Samuel 1:2

And he had two ['ishshah](../../strongs/h/h802.md); the [shem](../../strongs/h/h8034.md) of the one was [Ḥannâ](../../strongs/h/h2584.md), and the [shem](../../strongs/h/h8034.md) of the other [Pᵊninnâ](../../strongs/h/h6444.md): and [Pᵊninnâ](../../strongs/h/h6444.md) had [yeleḏ](../../strongs/h/h3206.md), but [Ḥannâ](../../strongs/h/h2584.md) had no [yeleḏ](../../strongs/h/h3206.md).

<a name="1samuel_1_3"></a>1Samuel 1:3

And this ['iysh](../../strongs/h/h376.md) [ʿālâ](../../strongs/h/h5927.md) out of his [ʿîr](../../strongs/h/h5892.md) [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) to [shachah](../../strongs/h/h7812.md) and to [zabach](../../strongs/h/h2076.md) unto [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) in [Šîlô](../../strongs/h/h7887.md). And the two [ben](../../strongs/h/h1121.md) of [ʿĒlî](../../strongs/h/h5941.md), [Ḥāp̄Nî](../../strongs/h/h2652.md) and [Pînḥās](../../strongs/h/h6372.md), the [kōhēn](../../strongs/h/h3548.md) of [Yĕhovah](../../strongs/h/h3068.md), were there.

<a name="1samuel_1_4"></a>1Samuel 1:4

And when the [yowm](../../strongs/h/h3117.md) was that ['Elqānâ](../../strongs/h/h511.md) [zabach](../../strongs/h/h2076.md), he [nathan](../../strongs/h/h5414.md) to [Pᵊninnâ](../../strongs/h/h6444.md) his ['ishshah](../../strongs/h/h802.md), and to all her [ben](../../strongs/h/h1121.md) and her [bath](../../strongs/h/h1323.md), [mānâ](../../strongs/h/h4490.md):

<a name="1samuel_1_5"></a>1Samuel 1:5

But unto [Ḥannâ](../../strongs/h/h2584.md) he [nathan](../../strongs/h/h5414.md) an ['aph](../../strongs/h/h639.md) [mānâ](../../strongs/h/h4490.md); for he ['ahab](../../strongs/h/h157.md) [Ḥannâ](../../strongs/h/h2584.md): but [Yĕhovah](../../strongs/h/h3068.md) had [cagar](../../strongs/h/h5462.md) her [reḥem](../../strongs/h/h7358.md).

<a name="1samuel_1_6"></a>1Samuel 1:6

And her [tsarah](../../strongs/h/h6869.md) also [kāʿas](../../strongs/h/h3707.md) her [ka'ac](../../strongs/h/h3708.md), for to make her [ra'am](../../strongs/h/h7481.md), because [Yĕhovah](../../strongs/h/h3068.md) had [cagar](../../strongs/h/h5462.md) up her [reḥem](../../strongs/h/h7358.md).

<a name="1samuel_1_7"></a>1Samuel 1:7

And as he ['asah](../../strongs/h/h6213.md) so [šānâ](../../strongs/h/h8141.md) by [šānâ](../../strongs/h/h8141.md), [day](../../strongs/h/h1767.md) she [ʿālâ](../../strongs/h/h5927.md) to the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), so she [kāʿas](../../strongs/h/h3707.md) her; therefore she [bāḵâ](../../strongs/h/h1058.md), and did not ['akal](../../strongs/h/h398.md).

<a name="1samuel_1_8"></a>1Samuel 1:8

Then ['āmar](../../strongs/h/h559.md) ['Elqānâ](../../strongs/h/h511.md) her ['iysh](../../strongs/h/h376.md) to her, [Ḥannâ](../../strongs/h/h2584.md), why [bāḵâ](../../strongs/h/h1058.md) thou? and why ['akal](../../strongs/h/h398.md) thou not? and why is thy [lebab](../../strongs/h/h3824.md) [yāraʿ](../../strongs/h/h3415.md)? am not I [towb](../../strongs/h/h2896.md) to thee than ten [ben](../../strongs/h/h1121.md)?

<a name="1samuel_1_9"></a>1Samuel 1:9

So [Ḥannâ](../../strongs/h/h2584.md) [quwm](../../strongs/h/h6965.md) ['aḥar](../../strongs/h/h310.md) they had ['akal](../../strongs/h/h398.md) in [Šîlô](../../strongs/h/h7887.md), and ['aḥar](../../strongs/h/h310.md) they had [šāṯâ](../../strongs/h/h8354.md). Now [ʿĒlî](../../strongs/h/h5941.md) the [kōhēn](../../strongs/h/h3548.md) [yashab](../../strongs/h/h3427.md) upon a [kicce'](../../strongs/h/h3678.md) by a [mᵊzûzâ](../../strongs/h/h4201.md) of the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_1_10"></a>1Samuel 1:10

And she was in [mar](../../strongs/h/h4751.md) of [nephesh](../../strongs/h/h5315.md), and [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md), and [bāḵâ](../../strongs/h/h1058.md) [bāḵâ](../../strongs/h/h1058.md).

<a name="1samuel_1_11"></a>1Samuel 1:11

And she [nāḏar](../../strongs/h/h5087.md) a [neḏer](../../strongs/h/h5088.md), and ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), if thou wilt [ra'ah](../../strongs/h/h7200.md) [ra'ah](../../strongs/h/h7200.md) on the ['oniy](../../strongs/h/h6040.md) of thine ['amah](../../strongs/h/h519.md), and [zakar](../../strongs/h/h2142.md) me, and not [shakach](../../strongs/h/h7911.md) thine ['amah](../../strongs/h/h519.md), but wilt [nathan](../../strongs/h/h5414.md) unto thine ['amah](../../strongs/h/h519.md) an ['enowsh](../../strongs/h/h582.md) [zera'](../../strongs/h/h2233.md), then I will [nathan](../../strongs/h/h5414.md) him unto [Yĕhovah](../../strongs/h/h3068.md) all the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md), and there shall no [môrâ](../../strongs/h/h4177.md) [ʿālâ](../../strongs/h/h5927.md) upon his [ro'sh](../../strongs/h/h7218.md).

<a name="1samuel_1_12"></a>1Samuel 1:12

And it came to pass, as she [rabah](../../strongs/h/h7235.md) [palal](../../strongs/h/h6419.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), that [ʿĒlî](../../strongs/h/h5941.md) [shamar](../../strongs/h/h8104.md) her [peh](../../strongs/h/h6310.md).

<a name="1samuel_1_13"></a>1Samuel 1:13

Now [Ḥannâ](../../strongs/h/h2584.md), she [dabar](../../strongs/h/h1696.md) in her [leb](../../strongs/h/h3820.md); only her [saphah](../../strongs/h/h8193.md) [nûaʿ](../../strongs/h/h5128.md), but her [qowl](../../strongs/h/h6963.md) was not [shama'](../../strongs/h/h8085.md): therefore [ʿĒlî](../../strongs/h/h5941.md) [chashab](../../strongs/h/h2803.md) she had been [šikôr](../../strongs/h/h7910.md).

<a name="1samuel_1_14"></a>1Samuel 1:14

And [ʿĒlî](../../strongs/h/h5941.md) ['āmar](../../strongs/h/h559.md) unto her, How long wilt thou be [šāḵar](../../strongs/h/h7937.md)? put [cuwr](../../strongs/h/h5493.md) thy [yayin](../../strongs/h/h3196.md) from thee.

<a name="1samuel_1_15"></a>1Samuel 1:15

And [Ḥannâ](../../strongs/h/h2584.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), No, my ['adown](../../strongs/h/h113.md), I am an ['ishshah](../../strongs/h/h802.md) of a [qāšê](../../strongs/h/h7186.md) [ruwach](../../strongs/h/h7307.md): I have [šāṯâ](../../strongs/h/h8354.md) neither [yayin](../../strongs/h/h3196.md) nor [šēḵār](../../strongs/h/h7941.md), but have [šāp̄aḵ](../../strongs/h/h8210.md) my [nephesh](../../strongs/h/h5315.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_1_16"></a>1Samuel 1:16

[nathan](../../strongs/h/h5414.md) not thine ['amah](../../strongs/h/h519.md) [paniym](../../strongs/h/h6440.md) a [bath](../../strongs/h/h1323.md) of [beliya'al](../../strongs/h/h1100.md): for out of the [rōḇ](../../strongs/h/h7230.md) of my [śîaḥ](../../strongs/h/h7879.md) and [ka'ac](../../strongs/h/h3708.md) have I [dabar](../../strongs/h/h1696.md) hitherto.

<a name="1samuel_1_17"></a>1Samuel 1:17

Then [ʿĒlî](../../strongs/h/h5941.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) in [shalowm](../../strongs/h/h7965.md): and the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [nathan](../../strongs/h/h5414.md) thee thy [šᵊ'ēlâ](../../strongs/h/h7596.md) that thou hast [sha'al](../../strongs/h/h7592.md) of him.

<a name="1samuel_1_18"></a>1Samuel 1:18

And she ['āmar](../../strongs/h/h559.md), Let thine [šip̄ḥâ](../../strongs/h/h8198.md) [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md). So the ['ishshah](../../strongs/h/h802.md) [yālaḵ](../../strongs/h/h3212.md) her [derek](../../strongs/h/h1870.md), and did ['akal](../../strongs/h/h398.md), and her [paniym](../../strongs/h/h6440.md) was no more sad.

<a name="1samuel_1_19"></a>1Samuel 1:19

And they [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md) [šāḵam](../../strongs/h/h7925.md), and [shachah](../../strongs/h/h7812.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and [shuwb](../../strongs/h/h7725.md), and [bow'](../../strongs/h/h935.md) to their [bayith](../../strongs/h/h1004.md) to [rāmâ](../../strongs/h/h7414.md): and ['Elqānâ](../../strongs/h/h511.md) [yada'](../../strongs/h/h3045.md) [Ḥannâ](../../strongs/h/h2584.md) his ['ishshah](../../strongs/h/h802.md); and [Yĕhovah](../../strongs/h/h3068.md) [zakar](../../strongs/h/h2142.md) her.

<a name="1samuel_1_20"></a>1Samuel 1:20

Wherefore it came to pass, when the [yowm](../../strongs/h/h3117.md) was [tᵊqûp̄â](../../strongs/h/h8622.md) about after [Ḥannâ](../../strongs/h/h2584.md) had [harah](../../strongs/h/h2029.md), that she [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md), and [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md), saying, Because I have [sha'al](../../strongs/h/h7592.md) him of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_1_21"></a>1Samuel 1:21

And the ['iysh](../../strongs/h/h376.md) ['Elqānâ](../../strongs/h/h511.md), and all his [bayith](../../strongs/h/h1004.md), [ʿālâ](../../strongs/h/h5927.md) to [zabach](../../strongs/h/h2076.md) unto [Yĕhovah](../../strongs/h/h3068.md) the [yowm](../../strongs/h/h3117.md) [zebach](../../strongs/h/h2077.md), and his [neḏer](../../strongs/h/h5088.md).

<a name="1samuel_1_22"></a>1Samuel 1:22

But [Ḥannâ](../../strongs/h/h2584.md) went not [ʿālâ](../../strongs/h/h5927.md); for she ['āmar](../../strongs/h/h559.md) unto her ['iysh](../../strongs/h/h376.md), I will not go up until the [naʿar](../../strongs/h/h5288.md) be [gamal](../../strongs/h/h1580.md), and then I will [bow'](../../strongs/h/h935.md) him, that he may [ra'ah](../../strongs/h/h7200.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and there [yashab](../../strongs/h/h3427.md) ['owlam](../../strongs/h/h5769.md).

<a name="1samuel_1_23"></a>1Samuel 1:23

And ['Elqānâ](../../strongs/h/h511.md) her ['iysh](../../strongs/h/h376.md) ['āmar](../../strongs/h/h559.md) unto her, ['asah](../../strongs/h/h6213.md) what ['ayin](../../strongs/h/h5869.md) thee [towb](../../strongs/h/h2896.md); [yashab](../../strongs/h/h3427.md) until thou have [gamal](../../strongs/h/h1580.md) him; only [Yĕhovah](../../strongs/h/h3068.md) [quwm](../../strongs/h/h6965.md) his [dabar](../../strongs/h/h1697.md). So the ['ishshah](../../strongs/h/h802.md) [yashab](../../strongs/h/h3427.md), and gave her [ben](../../strongs/h/h1121.md) [yānaq](../../strongs/h/h3243.md) until she [gamal](../../strongs/h/h1580.md) him.

<a name="1samuel_1_24"></a>1Samuel 1:24

And when she had [gamal](../../strongs/h/h1580.md) him, she took him [ʿālâ](../../strongs/h/h5927.md) with her, with three [par](../../strongs/h/h6499.md), and one ['êp̄â](../../strongs/h/h374.md) of [qemaḥ](../../strongs/h/h7058.md), and a [neḇel](../../strongs/h/h5035.md) of [yayin](../../strongs/h/h3196.md), and [bow'](../../strongs/h/h935.md) him unto the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) in [Šîlô](../../strongs/h/h7887.md): and the [naʿar](../../strongs/h/h5288.md) was [naʿar](../../strongs/h/h5288.md).

<a name="1samuel_1_25"></a>1Samuel 1:25

And they [šāḥaṭ](../../strongs/h/h7819.md) a [par](../../strongs/h/h6499.md), and [bow'](../../strongs/h/h935.md) the [naʿar](../../strongs/h/h5288.md) to [ʿĒlî](../../strongs/h/h5941.md).

<a name="1samuel_1_26"></a>1Samuel 1:26

And she ['āmar](../../strongs/h/h559.md), [bî](../../strongs/h/h994.md) my ['adown](../../strongs/h/h113.md), as thy [nephesh](../../strongs/h/h5315.md) [chay](../../strongs/h/h2416.md), my ['adown](../../strongs/h/h113.md), I am the ['ishshah](../../strongs/h/h802.md) that [nāṣaḇ](../../strongs/h/h5324.md) by thee here, [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="1samuel_1_27"></a>1Samuel 1:27

For this [naʿar](../../strongs/h/h5288.md) I [palal](../../strongs/h/h6419.md); and [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) me my [šᵊ'ēlâ](../../strongs/h/h7596.md) which I [sha'al](../../strongs/h/h7592.md) of him:

<a name="1samuel_1_28"></a>1Samuel 1:28

Therefore also I have [sha'al](../../strongs/h/h7592.md) him to [Yĕhovah](../../strongs/h/h3068.md); as long as he [yowm](../../strongs/h/h3117.md) he shall be [sha'al](../../strongs/h/h7592.md) to [Yĕhovah](../../strongs/h/h3068.md). And he [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md) there.

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 2](1samuel_2.md)