# 1 Samuel

[1 Samuel Overview](../../commentary/1samuel/1samuel_overview.md)

[1 Samuel 1](1samuel_1.md)

[1 Samuel 2](1samuel_2.md)

[1 Samuel 3](1samuel_3.md)

[1 Samuel 4](1samuel_4.md)

[1 Samuel 5](1samuel_5.md)

[1 Samuel 6](1samuel_6.md)

[1 Samuel 7](1samuel_7.md)

[1 Samuel 8](1samuel_8.md)

[1 Samuel 9](1samuel_9.md)

[1 Samuel 10](1samuel_10.md)

[1 Samuel 11](1samuel_11.md)

[1 Samuel 12](1samuel_12.md)

[1 Samuel 13](1samuel_13.md)

[1 Samuel 14](1samuel_14.md)

[1 Samuel 15](1samuel_15.md)

[1 Samuel 16](1samuel_16.md)

[1 Samuel 17](1samuel_17.md)

[1 Samuel 18](1samuel_18.md)

[1 Samuel 19](1samuel_19.md)

[1 Samuel 20](1samuel_20.md)

[1 Samuel 21](1samuel_21.md)

[1 Samuel 22](1samuel_22.md)

[1 Samuel 23](1samuel_23.md)

[1 Samuel 24](1samuel_24.md)

[1 Samuel 25](1samuel_25.md)

[1 Samuel 26](1samuel_26.md)

[1 Samuel 27](1samuel_27.md)

[1 Samuel 28](1samuel_28.md)

[1 Samuel 29](1samuel_29.md)

[1 Samuel 30](1samuel_30.md)

[1 Samuel 31](1samuel_31.md)

---

[Transliteral Bible](../index.md)