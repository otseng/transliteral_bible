# [1Samuel 25](https://www.blueletterbible.org/kjv/1samuel/25)

<a name="1samuel_25_1"></a>1Samuel 25:1

And [Šᵊmû'Ēl](../../strongs/h/h8050.md) [muwth](../../strongs/h/h4191.md); and all the [Yisra'el](../../strongs/h/h3478.md) were [qāḇaṣ](../../strongs/h/h6908.md), and [sāp̄aḏ](../../strongs/h/h5594.md) him, and [qāḇar](../../strongs/h/h6912.md) him in his [bayith](../../strongs/h/h1004.md) at [rāmâ](../../strongs/h/h7414.md). And [Dāviḏ](../../strongs/h/h1732.md) [quwm](../../strongs/h/h6965.md), and [yarad](../../strongs/h/h3381.md) to the [midbar](../../strongs/h/h4057.md) of [Pā'rān](../../strongs/h/h6290.md).

<a name="1samuel_25_2"></a>1Samuel 25:2

And there was an ['iysh](../../strongs/h/h376.md) in [MāʿÔn](../../strongs/h/h4584.md), whose [ma'aseh](../../strongs/h/h4639.md) were in [Karmel](../../strongs/h/h3760.md); and the ['iysh](../../strongs/h/h376.md) was [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md), and he had three thousand [tso'n](../../strongs/h/h6629.md), and a thousand [ʿēz](../../strongs/h/h5795.md): and he was [gāzaz](../../strongs/h/h1494.md) his [tso'n](../../strongs/h/h6629.md) in [Karmel](../../strongs/h/h3760.md).

<a name="1samuel_25_3"></a>1Samuel 25:3

Now the [shem](../../strongs/h/h8034.md) of the ['iysh](../../strongs/h/h376.md) was [Nāḇāl](../../strongs/h/h5037.md); and the [shem](../../strongs/h/h8034.md) of his ['ishshah](../../strongs/h/h802.md) ['Ăḇîḡayil](../../strongs/h/h26.md): and she was an ['ishshah](../../strongs/h/h802.md) of [towb](../../strongs/h/h2896.md) [śēḵel](../../strongs/h/h7922.md), and of a [yāp̄ê](../../strongs/h/h3303.md) [tō'ar](../../strongs/h/h8389.md): but the ['iysh](../../strongs/h/h376.md) was [qāšê](../../strongs/h/h7186.md) and [ra'](../../strongs/h/h7451.md) in his [maʿălāl](../../strongs/h/h4611.md); and he was of the house of [ḵlḇv](../../strongs/h/h3614.md).

<a name="1samuel_25_4"></a>1Samuel 25:4

And [Dāviḏ](../../strongs/h/h1732.md) [shama'](../../strongs/h/h8085.md) in the [midbar](../../strongs/h/h4057.md) that [Nāḇāl](../../strongs/h/h5037.md) did [gāzaz](../../strongs/h/h1494.md) his [tso'n](../../strongs/h/h6629.md).

<a name="1samuel_25_5"></a>1Samuel 25:5

And [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) ten [naʿar](../../strongs/h/h5288.md), and [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto the [naʿar](../../strongs/h/h5288.md), Get you [ʿālâ](../../strongs/h/h5927.md) to [Karmel](../../strongs/h/h3760.md), and [bow'](../../strongs/h/h935.md) to [Nāḇāl](../../strongs/h/h5037.md), and [sha'al](../../strongs/h/h7592.md) [shalowm](../../strongs/h/h7965.md) him in my [shem](../../strongs/h/h8034.md):

<a name="1samuel_25_6"></a>1Samuel 25:6

And thus shall ye ['āmar](../../strongs/h/h559.md) to him that [chay](../../strongs/h/h2416.md), [shalowm](../../strongs/h/h7965.md) be both to thee, and [shalowm](../../strongs/h/h7965.md) be to thine [bayith](../../strongs/h/h1004.md), and [shalowm](../../strongs/h/h7965.md) be unto all that thou hast.

<a name="1samuel_25_7"></a>1Samuel 25:7

And now I have [shama'](../../strongs/h/h8085.md) that thou hast [gāzaz](../../strongs/h/h1494.md): now thy [ra'ah](../../strongs/h/h7462.md) which were with us, we [kālam](../../strongs/h/h3637.md) them not, neither was there [mᵊ'ûmâ](../../strongs/h/h3972.md) [paqad](../../strongs/h/h6485.md) unto them, all the [yowm](../../strongs/h/h3117.md) they were in [Karmel](../../strongs/h/h3760.md).

<a name="1samuel_25_8"></a>1Samuel 25:8

[sha'al](../../strongs/h/h7592.md) thy [naʿar](../../strongs/h/h5288.md), and they will [nāḡaḏ](../../strongs/h/h5046.md) thee. Wherefore let the [naʿar](../../strongs/h/h5288.md) [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thine ['ayin](../../strongs/h/h5869.md): for we [bow'](../../strongs/h/h935.md) in a [towb](../../strongs/h/h2896.md) [yowm](../../strongs/h/h3117.md): [nathan](../../strongs/h/h5414.md), I pray thee, whatsoever [māṣā'](../../strongs/h/h4672.md) to thine [yad](../../strongs/h/h3027.md) unto thy ['ebed](../../strongs/h/h5650.md), and to thy [ben](../../strongs/h/h1121.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="1samuel_25_9"></a>1Samuel 25:9

And when [Dāviḏ](../../strongs/h/h1732.md) [naʿar](../../strongs/h/h5288.md) [bow'](../../strongs/h/h935.md), they [dabar](../../strongs/h/h1696.md) to [Nāḇāl](../../strongs/h/h5037.md) according to all those [dabar](../../strongs/h/h1697.md) in the [shem](../../strongs/h/h8034.md) of [Dāviḏ](../../strongs/h/h1732.md), and [nuwach](../../strongs/h/h5117.md).

<a name="1samuel_25_10"></a>1Samuel 25:10

And [Nāḇāl](../../strongs/h/h5037.md) ['anah](../../strongs/h/h6030.md) [Dāviḏ](../../strongs/h/h1732.md) ['ebed](../../strongs/h/h5650.md), and ['āmar](../../strongs/h/h559.md), Who is [Dāviḏ](../../strongs/h/h1732.md)? and who is the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md)? there be [rabab](../../strongs/h/h7231.md) ['ebed](../../strongs/h/h5650.md) now a [yowm](../../strongs/h/h3117.md) that [pāraṣ](../../strongs/h/h6555.md) every ['iysh](../../strongs/h/h376.md) [paniym](../../strongs/h/h6440.md) his ['adown](../../strongs/h/h113.md).

<a name="1samuel_25_11"></a>1Samuel 25:11

Shall I then [laqach](../../strongs/h/h3947.md) my [lechem](../../strongs/h/h3899.md), and my [mayim](../../strongs/h/h4325.md), and my [ṭiḇḥâ](../../strongs/h/h2878.md) that I have [ṭāḇaḥ](../../strongs/h/h2873.md) for my [gāzaz](../../strongs/h/h1494.md), and [nathan](../../strongs/h/h5414.md) it unto ['enowsh](../../strongs/h/h582.md), whom I [yada'](../../strongs/h/h3045.md) not whence they be?

<a name="1samuel_25_12"></a>1Samuel 25:12

So [Dāviḏ](../../strongs/h/h1732.md) [naʿar](../../strongs/h/h5288.md) [hāp̄aḵ](../../strongs/h/h2015.md) their [derek](../../strongs/h/h1870.md), and went [shuwb](../../strongs/h/h7725.md), and [bow'](../../strongs/h/h935.md) and [nāḡaḏ](../../strongs/h/h5046.md) him all those [dabar](../../strongs/h/h1697.md).

<a name="1samuel_25_13"></a>1Samuel 25:13

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto his ['enowsh](../../strongs/h/h582.md), [ḥāḡar](../../strongs/h/h2296.md) ye every ['iysh](../../strongs/h/h376.md) his [chereb](../../strongs/h/h2719.md). And they [ḥāḡar](../../strongs/h/h2296.md) every ['iysh](../../strongs/h/h376.md) his [chereb](../../strongs/h/h2719.md); and [Dāviḏ](../../strongs/h/h1732.md) also [ḥāḡar](../../strongs/h/h2296.md) his [chereb](../../strongs/h/h2719.md): and there [ʿālâ](../../strongs/h/h5927.md) ['aḥar](../../strongs/h/h310.md) [Dāviḏ](../../strongs/h/h1732.md) about four hundred ['iysh](../../strongs/h/h376.md); and two hundred [yashab](../../strongs/h/h3427.md) by the [kĕliy](../../strongs/h/h3627.md).

<a name="1samuel_25_14"></a>1Samuel 25:14

But one of the [naʿar](../../strongs/h/h5288.md) [nāḡaḏ](../../strongs/h/h5046.md) ['Ăḇîḡayil](../../strongs/h/h26.md), [Nāḇāl](../../strongs/h/h5037.md) ['ishshah](../../strongs/h/h802.md), ['āmar](../../strongs/h/h559.md), Behold, [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) out of the [midbar](../../strongs/h/h4057.md) to [barak](../../strongs/h/h1288.md) our ['adown](../../strongs/h/h113.md); and he [ʿîṭ](../../strongs/h/h5860.md) on them.

<a name="1samuel_25_15"></a>1Samuel 25:15

But the ['enowsh](../../strongs/h/h582.md) were [me'od](../../strongs/h/h3966.md) [towb](../../strongs/h/h2896.md) unto us, and we were not [kālam](../../strongs/h/h3637.md), neither [paqad](../../strongs/h/h6485.md) we any [mᵊ'ûmâ](../../strongs/h/h3972.md), as long [yowm](../../strongs/h/h3117.md) we were [halak](../../strongs/h/h1980.md) with them, when we were in the [sadeh](../../strongs/h/h7704.md):

<a name="1samuel_25_16"></a>1Samuel 25:16

They were a [ḥômâ](../../strongs/h/h2346.md) unto us both by [layil](../../strongs/h/h3915.md) and [yômām](../../strongs/h/h3119.md), all the [yowm](../../strongs/h/h3117.md) we were with them [ra'ah](../../strongs/h/h7462.md) the [tso'n](../../strongs/h/h6629.md).

<a name="1samuel_25_17"></a>1Samuel 25:17

Now therefore [yada'](../../strongs/h/h3045.md) and [ra'ah](../../strongs/h/h7200.md) what thou wilt ['asah](../../strongs/h/h6213.md); for [ra'](../../strongs/h/h7451.md) is [kalah](../../strongs/h/h3615.md) against our ['adown](../../strongs/h/h113.md), and against all his [bayith](../../strongs/h/h1004.md): for he is such a [ben](../../strongs/h/h1121.md) of [beliya'al](../../strongs/h/h1100.md), that a man cannot [dabar](../../strongs/h/h1696.md) to him.

<a name="1samuel_25_18"></a>1Samuel 25:18

Then ['Ăḇîḡayil](../../strongs/h/h26.md) made [māhar](../../strongs/h/h4116.md), and [laqach](../../strongs/h/h3947.md) two hundred [lechem](../../strongs/h/h3899.md), and two [neḇel](../../strongs/h/h5035.md) of [yayin](../../strongs/h/h3196.md), and five [tso'n](../../strongs/h/h6629.md) ready ['asah](../../strongs/h/h6213.md), and five [sᵊ'â](../../strongs/h/h5429.md) of [qālî](../../strongs/h/h7039.md) corn, and an hundred clusters of [ṣmvqym](../../strongs/h/h6778.md), and two hundred [dᵊḇēlâ](../../strongs/h/h1690.md) of figs, and [śûm](../../strongs/h/h7760.md) them on [chamowr](../../strongs/h/h2543.md).

<a name="1samuel_25_19"></a>1Samuel 25:19

And she ['āmar](../../strongs/h/h559.md) unto her [naʿar](../../strongs/h/h5288.md), ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) me; behold, I [bow'](../../strongs/h/h935.md) ['aḥar](../../strongs/h/h310.md) you. But she [nāḡaḏ](../../strongs/h/h5046.md) not her ['iysh](../../strongs/h/h376.md) [Nāḇāl](../../strongs/h/h5037.md).

<a name="1samuel_25_20"></a>1Samuel 25:20

And it was so, as she [rāḵaḇ](../../strongs/h/h7392.md) on the [chamowr](../../strongs/h/h2543.md), that she [yarad](../../strongs/h/h3381.md) by the [cether](../../strongs/h/h5643.md) of the [har](../../strongs/h/h2022.md), and, behold, [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) [yarad](../../strongs/h/h3381.md) [qārā'](../../strongs/h/h7125.md) her; and she [pāḡaš](../../strongs/h/h6298.md) them.

<a name="1samuel_25_21"></a>1Samuel 25:21

Now [Dāviḏ](../../strongs/h/h1732.md) had ['āmar](../../strongs/h/h559.md), Surely in [sheqer](../../strongs/h/h8267.md) have I [shamar](../../strongs/h/h8104.md) all that this fellow hath in the [midbar](../../strongs/h/h4057.md), so that [mᵊ'ûmâ](../../strongs/h/h3972.md) was [paqad](../../strongs/h/h6485.md) of all that pertained unto him: and he hath [shuwb](../../strongs/h/h7725.md) me [ra'](../../strongs/h/h7451.md) for [towb](../../strongs/h/h2896.md).

<a name="1samuel_25_22"></a>1Samuel 25:22

So and more also ['asah](../../strongs/h/h6213.md) ['Elohiym](../../strongs/h/h430.md) unto the ['oyeb](../../strongs/h/h341.md) of [Dāviḏ](../../strongs/h/h1732.md), if I [šā'ar](../../strongs/h/h7604.md) of all that pertain to him by the [boqer](../../strongs/h/h1242.md) ['owr](../../strongs/h/h216.md) any that [šāṯan](../../strongs/h/h8366.md) against the [qîr](../../strongs/h/h7023.md).

<a name="1samuel_25_23"></a>1Samuel 25:23

And when ['Ăḇîḡayil](../../strongs/h/h26.md) [ra'ah](../../strongs/h/h7200.md) [Dāviḏ](../../strongs/h/h1732.md), she [māhar](../../strongs/h/h4116.md), and [yarad](../../strongs/h/h3381.md) the [chamowr](../../strongs/h/h2543.md), and [naphal](../../strongs/h/h5307.md) ['aph](../../strongs/h/h639.md) [Dāviḏ](../../strongs/h/h1732.md) on her [paniym](../../strongs/h/h6440.md), and [shachah](../../strongs/h/h7812.md) herself to the ['erets](../../strongs/h/h776.md),

<a name="1samuel_25_24"></a>1Samuel 25:24

And [naphal](../../strongs/h/h5307.md) at his [regel](../../strongs/h/h7272.md), and ['āmar](../../strongs/h/h559.md), Upon me, my ['adown](../../strongs/h/h113.md), upon me let this ['avon](../../strongs/h/h5771.md) be: and let thine ['amah](../../strongs/h/h519.md), I pray thee, [dabar](../../strongs/h/h1696.md) in thine ['ozen](../../strongs/h/h241.md), and [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of thine handmaid.

<a name="1samuel_25_25"></a>1Samuel 25:25

[śûm](../../strongs/h/h7760.md) not my ['adown](../../strongs/h/h113.md), I pray thee, [leb](../../strongs/h/h3820.md) this ['iysh](../../strongs/h/h376.md) of [beliya'al](../../strongs/h/h1100.md), even [Nāḇāl](../../strongs/h/h5037.md): for as his [shem](../../strongs/h/h8034.md) is, so is he; [Nāḇāl](../../strongs/h/h5037.md) is his [shem](../../strongs/h/h8034.md), and [nᵊḇālâ](../../strongs/h/h5039.md) is with him: but I thine ['amah](../../strongs/h/h519.md) [ra'ah](../../strongs/h/h7200.md) not the [naʿar](../../strongs/h/h5288.md) of my ['adown](../../strongs/h/h113.md), whom thou didst [shalach](../../strongs/h/h7971.md).

<a name="1samuel_25_26"></a>1Samuel 25:26

Now therefore, my ['adown](../../strongs/h/h113.md), as [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), and as thy [nephesh](../../strongs/h/h5315.md) [chay](../../strongs/h/h2416.md), seeing [Yĕhovah](../../strongs/h/h3068.md) hath [mānaʿ](../../strongs/h/h4513.md) thee from [bow'](../../strongs/h/h935.md) to shed [dam](../../strongs/h/h1818.md), and from [yasha'](../../strongs/h/h3467.md) thyself with thine own [yad](../../strongs/h/h3027.md), now let thine ['oyeb](../../strongs/h/h341.md), and they that [bāqaš](../../strongs/h/h1245.md) [ra'](../../strongs/h/h7451.md) to my ['adown](../../strongs/h/h113.md), be as [Nāḇāl](../../strongs/h/h5037.md).

<a name="1samuel_25_27"></a>1Samuel 25:27

And now this [bĕrakah](../../strongs/h/h1293.md) which thine [šip̄ḥâ](../../strongs/h/h8198.md) hath [bow'](../../strongs/h/h935.md) unto my ['adown](../../strongs/h/h113.md), let it even be [nathan](../../strongs/h/h5414.md) unto the [naʿar](../../strongs/h/h5288.md) that [halak](../../strongs/h/h1980.md) [regel](../../strongs/h/h7272.md) my ['adown](../../strongs/h/h113.md).

<a name="1samuel_25_28"></a>1Samuel 25:28

I pray thee, [nasa'](../../strongs/h/h5375.md) the [pesha'](../../strongs/h/h6588.md) of thine ['amah](../../strongs/h/h519.md): for [Yĕhovah](../../strongs/h/h3068.md) will ['asah](../../strongs/h/h6213.md) ['asah](../../strongs/h/h6213.md) my ['adown](../../strongs/h/h113.md) an ['aman](../../strongs/h/h539.md) [bayith](../../strongs/h/h1004.md); because my ['adown](../../strongs/h/h113.md) [lāḥam](../../strongs/h/h3898.md) the [milḥāmâ](../../strongs/h/h4421.md) of [Yĕhovah](../../strongs/h/h3068.md), and [ra'](../../strongs/h/h7451.md) hath not been [māṣā'](../../strongs/h/h4672.md) in thee all thy [yowm](../../strongs/h/h3117.md).

<a name="1samuel_25_29"></a>1Samuel 25:29

Yet an ['āḏām](../../strongs/h/h120.md) is [quwm](../../strongs/h/h6965.md) to [radaph](../../strongs/h/h7291.md) thee, and to [bāqaš](../../strongs/h/h1245.md) thy [nephesh](../../strongs/h/h5315.md): but the [nephesh](../../strongs/h/h5315.md) of my ['adown](../../strongs/h/h113.md) shall be [tsarar](../../strongs/h/h6887.md) in the [ṣᵊrôr](../../strongs/h/h6872.md) of [chay](../../strongs/h/h2416.md) with [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md); and the [nephesh](../../strongs/h/h5315.md) of thine ['oyeb](../../strongs/h/h341.md), them shall he sling [qālaʿ](../../strongs/h/h7049.md), as out of the [tavek](../../strongs/h/h8432.md) [kaph](../../strongs/h/h3709.md) of a [qelaʿ](../../strongs/h/h7050.md).

<a name="1samuel_25_30"></a>1Samuel 25:30

And it shall come to pass, when [Yĕhovah](../../strongs/h/h3068.md) shall have ['asah](../../strongs/h/h6213.md) to my ['adown](../../strongs/h/h113.md) according to all the [towb](../../strongs/h/h2896.md) that he hath [dabar](../../strongs/h/h1696.md) concerning thee, and shall have [tsavah](../../strongs/h/h6680.md) thee [nāḡîḏ](../../strongs/h/h5057.md) over [Yisra'el](../../strongs/h/h3478.md);

<a name="1samuel_25_31"></a>1Samuel 25:31

That this shall be no [pûqâ](../../strongs/h/h6330.md) unto thee, nor [miḵšôl](../../strongs/h/h4383.md) of [leb](../../strongs/h/h3820.md) unto my ['adown](../../strongs/h/h113.md), either that thou hast [šāp̄aḵ](../../strongs/h/h8210.md) [dam](../../strongs/h/h1818.md) [ḥinnām](../../strongs/h/h2600.md), or that my ['adown](../../strongs/h/h113.md) hath [yasha'](../../strongs/h/h3467.md) himself: but when [Yĕhovah](../../strongs/h/h3068.md) shall have dealt [yatab](../../strongs/h/h3190.md) with my ['adown](../../strongs/h/h113.md), then [zakar](../../strongs/h/h2142.md) thine ['amah](../../strongs/h/h519.md).

<a name="1samuel_25_32"></a>1Samuel 25:32

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to ['Ăḇîḡayil](../../strongs/h/h26.md), [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), which [shalach](../../strongs/h/h7971.md) thee this [yowm](../../strongs/h/h3117.md) to [qārā'](../../strongs/h/h7125.md) me:

<a name="1samuel_25_33"></a>1Samuel 25:33

And [barak](../../strongs/h/h1288.md) be thy [ṭaʿam](../../strongs/h/h2940.md), and [barak](../../strongs/h/h1288.md) be thou, which hast [kālā'](../../strongs/h/h3607.md) me this [yowm](../../strongs/h/h3117.md) from [bow'](../../strongs/h/h935.md) to shed [dam](../../strongs/h/h1818.md), and from [yasha'](../../strongs/h/h3467.md) myself with mine own [yad](../../strongs/h/h3027.md).

<a name="1samuel_25_34"></a>1Samuel 25:34

For in very deed, as [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [chay](../../strongs/h/h2416.md), which hath kept me [mānaʿ](../../strongs/h/h4513.md) from [ra'a'](../../strongs/h/h7489.md) thee, except thou hadst [māhar](../../strongs/h/h4116.md) and [bow'](../../strongs/h/h935.md) to [qārā'](../../strongs/h/h7125.md) me, surely there had not been [yāṯar](../../strongs/h/h3498.md) unto [Nāḇāl](../../strongs/h/h5037.md) by the [boqer](../../strongs/h/h1242.md) ['owr](../../strongs/h/h216.md) any that [šāṯan](../../strongs/h/h8366.md) against the [qîr](../../strongs/h/h7023.md).

<a name="1samuel_25_35"></a>1Samuel 25:35

So [Dāviḏ](../../strongs/h/h1732.md) [laqach](../../strongs/h/h3947.md) of her [yad](../../strongs/h/h3027.md) that which she had [bow'](../../strongs/h/h935.md) him, and ['āmar](../../strongs/h/h559.md) unto her, [ʿālâ](../../strongs/h/h5927.md) in [shalowm](../../strongs/h/h7965.md) to thine [bayith](../../strongs/h/h1004.md); [ra'ah](../../strongs/h/h7200.md), I have [shama'](../../strongs/h/h8085.md) to thy [qowl](../../strongs/h/h6963.md), and have [nasa'](../../strongs/h/h5375.md) thy [paniym](../../strongs/h/h6440.md).

<a name="1samuel_25_36"></a>1Samuel 25:36

And ['Ăḇîḡayil](../../strongs/h/h26.md) [bow'](../../strongs/h/h935.md) to [Nāḇāl](../../strongs/h/h5037.md); and, behold, he held a [mištê](../../strongs/h/h4960.md) in his [bayith](../../strongs/h/h1004.md), like the [mištê](../../strongs/h/h4960.md) of a [melek](../../strongs/h/h4428.md); and [Nāḇāl](../../strongs/h/h5037.md) [leb](../../strongs/h/h3820.md) was [towb](../../strongs/h/h2896.md) within him, for he was [me'od](../../strongs/h/h3966.md) [šikôr](../../strongs/h/h7910.md): wherefore she [nāḡaḏ](../../strongs/h/h5046.md) him [dabar](../../strongs/h/h1697.md), [qāṭān](../../strongs/h/h6996.md) or [gadowl](../../strongs/h/h1419.md), until the [boqer](../../strongs/h/h1242.md) ['owr](../../strongs/h/h216.md).

<a name="1samuel_25_37"></a>1Samuel 25:37

But it came to pass in the [boqer](../../strongs/h/h1242.md), when the [yayin](../../strongs/h/h3196.md) was [yāṣā'](../../strongs/h/h3318.md) of [Nāḇāl](../../strongs/h/h5037.md), and his ['ishshah](../../strongs/h/h802.md) had [nāḡaḏ](../../strongs/h/h5046.md) him these [dabar](../../strongs/h/h1697.md), that his [leb](../../strongs/h/h3820.md) [muwth](../../strongs/h/h4191.md) [qereḇ](../../strongs/h/h7130.md) him, and he became as an ['eben](../../strongs/h/h68.md).

<a name="1samuel_25_38"></a>1Samuel 25:38

And it came to pass about ten [yowm](../../strongs/h/h3117.md) after, that [Yĕhovah](../../strongs/h/h3068.md) [nāḡap̄](../../strongs/h/h5062.md) [Nāḇāl](../../strongs/h/h5037.md), that he [muwth](../../strongs/h/h4191.md).

<a name="1samuel_25_39"></a>1Samuel 25:39

And when [Dāviḏ](../../strongs/h/h1732.md) [shama'](../../strongs/h/h8085.md) that [Nāḇāl](../../strongs/h/h5037.md) was [muwth](../../strongs/h/h4191.md), he ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md), that hath [riyb](../../strongs/h/h7378.md) the [rîḇ](../../strongs/h/h7379.md) of my [cherpah](../../strongs/h/h2781.md) from the [yad](../../strongs/h/h3027.md) of [Nāḇāl](../../strongs/h/h5037.md), and hath [ḥāśaḵ](../../strongs/h/h2820.md) his ['ebed](../../strongs/h/h5650.md) from [ra'](../../strongs/h/h7451.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [shuwb](../../strongs/h/h7725.md) the [ra'](../../strongs/h/h7451.md) of [Nāḇāl](../../strongs/h/h5037.md) upon his own [ro'sh](../../strongs/h/h7218.md). And [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) and [dabar](../../strongs/h/h1696.md) with ['Ăḇîḡayil](../../strongs/h/h26.md), to [laqach](../../strongs/h/h3947.md) her to him to ['ishshah](../../strongs/h/h802.md).

<a name="1samuel_25_40"></a>1Samuel 25:40

And when the ['ebed](../../strongs/h/h5650.md) of [Dāviḏ](../../strongs/h/h1732.md) were [bow'](../../strongs/h/h935.md) to ['Ăḇîḡayil](../../strongs/h/h26.md) to [Karmel](../../strongs/h/h3760.md), they [dabar](../../strongs/h/h1696.md) unto her, ['āmar](../../strongs/h/h559.md), [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) us unto thee, to [laqach](../../strongs/h/h3947.md) thee to him to ['ishshah](../../strongs/h/h802.md).

<a name="1samuel_25_41"></a>1Samuel 25:41

And she [quwm](../../strongs/h/h6965.md), and [shachah](../../strongs/h/h7812.md) herself on her ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md), and ['āmar](../../strongs/h/h559.md), Behold, let thine ['amah](../../strongs/h/h519.md) be a [šip̄ḥâ](../../strongs/h/h8198.md) to [rāḥaṣ](../../strongs/h/h7364.md) the [regel](../../strongs/h/h7272.md) of the ['ebed](../../strongs/h/h5650.md) of my ['adown](../../strongs/h/h113.md).

<a name="1samuel_25_42"></a>1Samuel 25:42

And ['Ăḇîḡayil](../../strongs/h/h26.md) [māhar](../../strongs/h/h4116.md), and [quwm](../../strongs/h/h6965.md), and [rāḵaḇ](../../strongs/h/h7392.md) upon a [chamowr](../../strongs/h/h2543.md), with five [naʿărâ](../../strongs/h/h5291.md) of hers that [halak](../../strongs/h/h1980.md) [regel](../../strongs/h/h7272.md) her; and she [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) the [mal'ak](../../strongs/h/h4397.md) of [Dāviḏ](../../strongs/h/h1732.md), and became his ['ishshah](../../strongs/h/h802.md).

<a name="1samuel_25_43"></a>1Samuel 25:43

[Dāviḏ](../../strongs/h/h1732.md) also [laqach](../../strongs/h/h3947.md) ['ĂḥînōʿAm](../../strongs/h/h293.md) of [YizrᵊʿE'L](../../strongs/h/h3157.md); and they were also both of them his ['ishshah](../../strongs/h/h802.md).

<a name="1samuel_25_44"></a>1Samuel 25:44

But [Šā'ûl](../../strongs/h/h7586.md) had [nathan](../../strongs/h/h5414.md) [Mîḵāl](../../strongs/h/h4324.md) his [bath](../../strongs/h/h1323.md), [Dāviḏ](../../strongs/h/h1732.md) ['ishshah](../../strongs/h/h802.md), to [palṭî](../../strongs/h/h6406.md) the [ben](../../strongs/h/h1121.md) of [layiš](../../strongs/h/h3919.md), which was of [gallîm](../../strongs/h/h1554.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 24](1samuel_24.md) - [1Samuel 26](1samuel_26.md)