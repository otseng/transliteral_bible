# [1Samuel 29](https://www.blueletterbible.org/kjv/1samuel/29)

<a name="1samuel_29_1"></a>1Samuel 29:1

Now the [Pᵊlištî](../../strongs/h/h6430.md) [qāḇaṣ](../../strongs/h/h6908.md) all their [maḥănê](../../strongs/h/h4264.md) to ['Ăp̄Ēq](../../strongs/h/h663.md): and the [Yisra'el](../../strongs/h/h3478.md) [ḥānâ](../../strongs/h/h2583.md) by an ['ayin](../../strongs/h/h5869.md) which is in [YizrᵊʿE'L](../../strongs/h/h3157.md).

<a name="1samuel_29_2"></a>1Samuel 29:2

And the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md) ['abar](../../strongs/h/h5674.md) by hundreds, and by thousands: but [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) ['abar](../../strongs/h/h5674.md) in the ['aḥărôn](../../strongs/h/h314.md) with ['Āḵîš](../../strongs/h/h397.md).

<a name="1samuel_29_3"></a>1Samuel 29:3

Then ['āmar](../../strongs/h/h559.md) the [śar](../../strongs/h/h8269.md) of the [Pᵊlištî](../../strongs/h/h6430.md), What do these [ʿiḇrî](../../strongs/h/h5680.md) here? And ['Āḵîš](../../strongs/h/h397.md) ['āmar](../../strongs/h/h559.md) unto the [śar](../../strongs/h/h8269.md) of the [Pᵊlištî](../../strongs/h/h6430.md), Is not this [Dāviḏ](../../strongs/h/h1732.md), the ['ebed](../../strongs/h/h5650.md) of [Šā'ûl](../../strongs/h/h7586.md) the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), which hath been with me these [yowm](../../strongs/h/h3117.md), or these [šānâ](../../strongs/h/h8141.md), and I have [māṣā'](../../strongs/h/h4672.md) no [mᵊ'ûmâ](../../strongs/h/h3972.md) in him [yowm](../../strongs/h/h3117.md) he [naphal](../../strongs/h/h5307.md) unto me unto this [yowm](../../strongs/h/h3117.md)?

<a name="1samuel_29_4"></a>1Samuel 29:4

And the [śar](../../strongs/h/h8269.md) of the [Pᵊlištî](../../strongs/h/h6430.md) were [qāṣap̄](../../strongs/h/h7107.md) with him; and the [śar](../../strongs/h/h8269.md) of the [Pᵊlištî](../../strongs/h/h6430.md) ['āmar](../../strongs/h/h559.md) unto him, [shuwb](../../strongs/h/h7725.md) this ['iysh](../../strongs/h/h376.md) [shuwb](../../strongs/h/h7725.md), that he may [shuwb](../../strongs/h/h7725.md) to his [maqowm](../../strongs/h/h4725.md) which thou hast [paqad](../../strongs/h/h6485.md) him, and let him not [yarad](../../strongs/h/h3381.md) with us to [milḥāmâ](../../strongs/h/h4421.md), lest in the [milḥāmâ](../../strongs/h/h4421.md) he be a [satan](../../strongs/h/h7854.md) to us: for wherewith should he [ratsah](../../strongs/h/h7521.md) himself unto his ['adown](../../strongs/h/h113.md)? should it not be with the [ro'sh](../../strongs/h/h7218.md) of these ['enowsh](../../strongs/h/h582.md)?

<a name="1samuel_29_5"></a>1Samuel 29:5

Is not this [Dāviḏ](../../strongs/h/h1732.md), of whom they ['anah](../../strongs/h/h6030.md) one to another in [mᵊḥōlâ](../../strongs/h/h4246.md), ['āmar](../../strongs/h/h559.md), [Šā'ûl](../../strongs/h/h7586.md) [nakah](../../strongs/h/h5221.md) his thousands, and [Dāviḏ](../../strongs/h/h1732.md) his ten thousands?

<a name="1samuel_29_6"></a>1Samuel 29:6

Then ['Āḵîš](../../strongs/h/h397.md) [qara'](../../strongs/h/h7121.md) [Dāviḏ](../../strongs/h/h1732.md), and ['āmar](../../strongs/h/h559.md) unto him, Surely, as [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), thou hast been [yashar](../../strongs/h/h3477.md), and thy [yāṣā'](../../strongs/h/h3318.md) and thy [bow'](../../strongs/h/h935.md) with me in the [maḥănê](../../strongs/h/h4264.md) is [towb](../../strongs/h/h2896.md) in my ['ayin](../../strongs/h/h5869.md): for I have not [māṣā'](../../strongs/h/h4672.md) [ra'](../../strongs/h/h7451.md) in thee since the [yowm](../../strongs/h/h3117.md) of thy [bow'](../../strongs/h/h935.md) unto me unto this [yowm](../../strongs/h/h3117.md): nevertheless the [seren](../../strongs/h/h5633.md) [towb](../../strongs/h/h2896.md) thee ['ayin](../../strongs/h/h5869.md).

<a name="1samuel_29_7"></a>1Samuel 29:7

Wherefore now [shuwb](../../strongs/h/h7725.md), and [yālaḵ](../../strongs/h/h3212.md) in [shalowm](../../strongs/h/h7965.md), that thou ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) ['ayin](../../strongs/h/h5869.md) not the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1samuel_29_8"></a>1Samuel 29:8

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto ['Āḵîš](../../strongs/h/h397.md), But what have I ['asah](../../strongs/h/h6213.md)? and what hast thou [māṣā'](../../strongs/h/h4672.md) in thy ['ebed](../../strongs/h/h5650.md) so long [yowm](../../strongs/h/h3117.md) I have been with [paniym](../../strongs/h/h6440.md) unto this [yowm](../../strongs/h/h3117.md), that I may not [bow'](../../strongs/h/h935.md) [lāḥam](../../strongs/h/h3898.md) against the ['oyeb](../../strongs/h/h341.md) of my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md)?

<a name="1samuel_29_9"></a>1Samuel 29:9

And ['Āḵîš](../../strongs/h/h397.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), I [yada'](../../strongs/h/h3045.md) that thou art [towb](../../strongs/h/h2896.md) in my ['ayin](../../strongs/h/h5869.md), as a [mal'ak](../../strongs/h/h4397.md) of ['Elohiym](../../strongs/h/h430.md): notwithstanding the [śar](../../strongs/h/h8269.md) of the [Pᵊlištî](../../strongs/h/h6430.md) have ['āmar](../../strongs/h/h559.md), He shall not [ʿālâ](../../strongs/h/h5927.md) with us to the [milḥāmâ](../../strongs/h/h4421.md).

<a name="1samuel_29_10"></a>1Samuel 29:10

Wherefore now [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md) with thy ['adown](../../strongs/h/h113.md) ['ebed](../../strongs/h/h5650.md) that are [bow'](../../strongs/h/h935.md) with thee: and as soon as ye be up [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and have ['owr](../../strongs/h/h215.md), [yālaḵ](../../strongs/h/h3212.md).

<a name="1samuel_29_11"></a>1Samuel 29:11

So [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) [šāḵam](../../strongs/h/h7925.md) to [yālaḵ](../../strongs/h/h3212.md) in the [boqer](../../strongs/h/h1242.md), to [shuwb](../../strongs/h/h7725.md) into the ['erets](../../strongs/h/h776.md) of the [Pᵊlištî](../../strongs/h/h6430.md). And the [Pᵊlištî](../../strongs/h/h6430.md) [ʿālâ](../../strongs/h/h5927.md) to [YizrᵊʿE'L](../../strongs/h/h3157.md).

---

[Transliteral Bible](../bible.md)

[1Samuel](1samuel.md)

[1Samuel 28](1samuel_28.md) - [1Samuel 30](1samuel_30.md)