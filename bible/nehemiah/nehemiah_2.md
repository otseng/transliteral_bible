# [Nehemiah 2](https://www.blueletterbible.org/kjv/nehemiah/2)

<a name="nehemiah_2_1"></a>Nehemiah 2:1

And it came to pass in the [ḥōḏeš](../../strongs/h/h2320.md) [nîsān](../../strongs/h/h5212.md), in the [ʿeśrîm](../../strongs/h/h6242.md) [šānâ](../../strongs/h/h8141.md) of ['Artaḥšaštᵊ'](../../strongs/h/h783.md) the [melek](../../strongs/h/h4428.md), that [yayin](../../strongs/h/h3196.md) was [paniym](../../strongs/h/h6440.md) him: and I took [nasa'](../../strongs/h/h5375.md) the [yayin](../../strongs/h/h3196.md), and [nathan](../../strongs/h/h5414.md) it unto the [melek](../../strongs/h/h4428.md). Now I had not been beforetime [ra'](../../strongs/h/h7451.md) in his [paniym](../../strongs/h/h6440.md).

<a name="nehemiah_2_2"></a>Nehemiah 2:2

Wherefore the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto me, Why is thy [paniym](../../strongs/h/h6440.md) [ra'](../../strongs/h/h7451.md), seeing thou art not [ḥālâ](../../strongs/h/h2470.md)? this is nothing else but [rōaʿ](../../strongs/h/h7455.md) of [leb](../../strongs/h/h3820.md). Then I was [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md) [yare'](../../strongs/h/h3372.md),

<a name="nehemiah_2_3"></a>Nehemiah 2:3

And ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), Let the [melek](../../strongs/h/h4428.md) [ḥāyâ](../../strongs/h/h2421.md) ['owlam](../../strongs/h/h5769.md): why should not my [paniym](../../strongs/h/h6440.md) be [yāraʿ](../../strongs/h/h3415.md), when the [ʿîr](../../strongs/h/h5892.md), the [bayith](../../strongs/h/h1004.md) of my ['ab](../../strongs/h/h1.md) [qeber](../../strongs/h/h6913.md), lieth [ḥārēḇ](../../strongs/h/h2720.md), and the [sha'ar](../../strongs/h/h8179.md) thereof are ['akal](../../strongs/h/h398.md) with ['esh](../../strongs/h/h784.md)?

<a name="nehemiah_2_4"></a>Nehemiah 2:4

Then the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto me, For what dost thou make [bāqaš](../../strongs/h/h1245.md)? So I [palal](../../strongs/h/h6419.md) to the ['Elohiym](../../strongs/h/h430.md) of [shamayim](../../strongs/h/h8064.md).

<a name="nehemiah_2_5"></a>Nehemiah 2:5

And I ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), If it [ṭôḇ](../../strongs/h/h2895.md) the [melek](../../strongs/h/h4428.md), and if thy ['ebed](../../strongs/h/h5650.md) have found [yatab](../../strongs/h/h3190.md) in thy [paniym](../../strongs/h/h6440.md), that thou wouldest [shalach](../../strongs/h/h7971.md) me unto [Yehuwdah](../../strongs/h/h3063.md), unto the [ʿîr](../../strongs/h/h5892.md) of my ['ab](../../strongs/h/h1.md) [qeber](../../strongs/h/h6913.md), that I may [bānâ](../../strongs/h/h1129.md) it.

<a name="nehemiah_2_6"></a>Nehemiah 2:6

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto me, (the [šēḡāl](../../strongs/h/h7694.md) also [yashab](../../strongs/h/h3427.md) by ['ēṣel](../../strongs/h/h681.md),) For how long shall thy [mahălāḵ](../../strongs/h/h4109.md) be? and when wilt thou [shuwb](../../strongs/h/h7725.md)? So it [yatab](../../strongs/h/h3190.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) to [shalach](../../strongs/h/h7971.md) me; and I [nathan](../../strongs/h/h5414.md) him a [zᵊmān](../../strongs/h/h2165.md).

<a name="nehemiah_2_7"></a>Nehemiah 2:7

Moreover I ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), If it [ṭôḇ](../../strongs/h/h2895.md) the [melek](../../strongs/h/h4428.md), let ['agereṯ](../../strongs/h/h107.md) be [nathan](../../strongs/h/h5414.md) me to the [peḥâ](../../strongs/h/h6346.md) [ʿēḇer](../../strongs/h/h5676.md) the [nāhār](../../strongs/h/h5104.md), that they may convey me ['abar](../../strongs/h/h5674.md) till I [bow'](../../strongs/h/h935.md) into [Yehuwdah](../../strongs/h/h3063.md);

<a name="nehemiah_2_8"></a>Nehemiah 2:8

And an ['agereṯ](../../strongs/h/h107.md) unto ['Āsāp̄](../../strongs/h/h623.md) the [shamar](../../strongs/h/h8104.md) of the [melek](../../strongs/h/h4428.md) [pardēs](../../strongs/h/h6508.md), that he may [nathan](../../strongs/h/h5414.md) me ['ets](../../strongs/h/h6086.md) to make [qārâ](../../strongs/h/h7136.md) for the [sha'ar](../../strongs/h/h8179.md) of the [bîrâ](../../strongs/h/h1002.md) which appertained to the [bayith](../../strongs/h/h1004.md), and for the [ḥômâ](../../strongs/h/h2346.md) of the [ʿîr](../../strongs/h/h5892.md), and for the [bayith](../../strongs/h/h1004.md) that I shall [bow'](../../strongs/h/h935.md). And the [melek](../../strongs/h/h4428.md) [nathan](../../strongs/h/h5414.md) me, according to the [towb](../../strongs/h/h2896.md) [yad](../../strongs/h/h3027.md) of my ['Elohiym](../../strongs/h/h430.md) upon me.

<a name="nehemiah_2_9"></a>Nehemiah 2:9

Then I [bow'](../../strongs/h/h935.md) to the [peḥâ](../../strongs/h/h6346.md) [ʿēḇer](../../strongs/h/h5676.md) the [nāhār](../../strongs/h/h5104.md), and [nathan](../../strongs/h/h5414.md) them the [melek](../../strongs/h/h4428.md) ['agereṯ](../../strongs/h/h107.md). Now the [melek](../../strongs/h/h4428.md) had [shalach](../../strongs/h/h7971.md) [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md) and [pārāš](../../strongs/h/h6571.md) with me.

<a name="nehemiah_2_10"></a>Nehemiah 2:10

When [Sanḇallāṭ](../../strongs/h/h5571.md) the [ḥōrōnî](../../strongs/h/h2772.md), and [Ṭôḇîyâ](../../strongs/h/h2900.md) the ['ebed](../../strongs/h/h5650.md), the [ʿAmmôn](../../strongs/h/h5984.md), [shama'](../../strongs/h/h8085.md) of it, it [yāraʿ](../../strongs/h/h3415.md) them [ra'](../../strongs/h/h7451.md) [gadowl](../../strongs/h/h1419.md) that there was [bow'](../../strongs/h/h935.md) an ['āḏām](../../strongs/h/h120.md) to [bāqaš](../../strongs/h/h1245.md) the [towb](../../strongs/h/h2896.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="nehemiah_2_11"></a>Nehemiah 2:11

So I [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), and was there [šālôš](../../strongs/h/h7969.md) [yowm](../../strongs/h/h3117.md).

<a name="nehemiah_2_12"></a>Nehemiah 2:12

And I [quwm](../../strongs/h/h6965.md) in the [layil](../../strongs/h/h3915.md), I and [mᵊʿaṭ](../../strongs/h/h4592.md) [mᵊʿaṭ](../../strongs/h/h4592.md) ['enowsh](../../strongs/h/h582.md) with me; neither [nāḡaḏ](../../strongs/h/h5046.md) I any ['āḏām](../../strongs/h/h120.md) what my ['Elohiym](../../strongs/h/h430.md) had [nathan](../../strongs/h/h5414.md) in my [leb](../../strongs/h/h3820.md) to ['asah](../../strongs/h/h6213.md) at [Yĕruwshalaim](../../strongs/h/h3389.md): neither was there any [bĕhemah](../../strongs/h/h929.md) with me, save the [bĕhemah](../../strongs/h/h929.md) that I rode [rāḵaḇ](../../strongs/h/h7392.md).

<a name="nehemiah_2_13"></a>Nehemiah 2:13

And I [yāṣā'](../../strongs/h/h3318.md) by [layil](../../strongs/h/h3915.md) by the [sha'ar](../../strongs/h/h8179.md) of the [gay'](../../strongs/h/h1516.md), even [paniym](../../strongs/h/h6440.md) the [tannîn](../../strongs/h/h8577.md) ['ayin](../../strongs/h/h5869.md) H5886, and to the ['ašpōṯ](../../strongs/h/h830.md) [sha'ar](../../strongs/h/h8179.md), and [śāḇar](../../strongs/h/h7663.md) [shabar](../../strongs/h/h7665.md) the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), which were broken [pāraṣ](../../strongs/h/h6555.md), and the [sha'ar](../../strongs/h/h8179.md) thereof were ['akal](../../strongs/h/h398.md) with ['esh](../../strongs/h/h784.md).

<a name="nehemiah_2_14"></a>Nehemiah 2:14

Then I went ['abar](../../strongs/h/h5674.md) to the [sha'ar](../../strongs/h/h8179.md) of the ['ayin](../../strongs/h/h5869.md), and to the [melek](../../strongs/h/h4428.md) [bᵊrēḵâ](../../strongs/h/h1295.md): but there was no [maqowm](../../strongs/h/h4725.md) for the [bĕhemah](../../strongs/h/h929.md) that was under me to ['abar](../../strongs/h/h5674.md).

<a name="nehemiah_2_15"></a>Nehemiah 2:15

Then went I [ʿālâ](../../strongs/h/h5927.md) in the [layil](../../strongs/h/h3915.md) by the [nachal](../../strongs/h/h5158.md), and [śāḇar](../../strongs/h/h7663.md) [shabar](../../strongs/h/h7665.md) the [ḥômâ](../../strongs/h/h2346.md), and [shuwb](../../strongs/h/h7725.md), and [bow'](../../strongs/h/h935.md) by the [sha'ar](../../strongs/h/h8179.md) of the [gay'](../../strongs/h/h1516.md), and so [shuwb](../../strongs/h/h7725.md).

<a name="nehemiah_2_16"></a>Nehemiah 2:16

And the [sāḡān](../../strongs/h/h5461.md) [yada'](../../strongs/h/h3045.md) not whither I [halak](../../strongs/h/h1980.md), or what I ['asah](../../strongs/h/h6213.md); neither had I as [kēn](../../strongs/h/h3651.md) [nāḡaḏ](../../strongs/h/h5046.md) it to the [Yᵊhûḏî](../../strongs/h/h3064.md), nor to the [kōhēn](../../strongs/h/h3548.md), nor to the [ḥōr](../../strongs/h/h2715.md), nor to the [sāḡān](../../strongs/h/h5461.md), nor to the [yeṯer](../../strongs/h/h3499.md) that ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md).

<a name="nehemiah_2_17"></a>Nehemiah 2:17

Then ['āmar](../../strongs/h/h559.md) I unto them, Ye [ra'ah](../../strongs/h/h7200.md) the [ra'](../../strongs/h/h7451.md) that we are in, how [Yĕruwshalaim](../../strongs/h/h3389.md) lieth [ḥārēḇ](../../strongs/h/h2720.md), and the [sha'ar](../../strongs/h/h8179.md) thereof are [yāṣaṯ](../../strongs/h/h3341.md) with ['esh](../../strongs/h/h784.md): [yālaḵ](../../strongs/h/h3212.md), and let us [bānâ](../../strongs/h/h1129.md) the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), that we be no more a [cherpah](../../strongs/h/h2781.md).

<a name="nehemiah_2_18"></a>Nehemiah 2:18

Then I [nāḡaḏ](../../strongs/h/h5046.md) them of the [yad](../../strongs/h/h3027.md) of my ['Elohiym](../../strongs/h/h430.md) which was [towb](../../strongs/h/h2896.md) upon me; as also the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) that he had ['āmar](../../strongs/h/h559.md) unto me. And they ['āmar](../../strongs/h/h559.md), Let us [quwm](../../strongs/h/h6965.md) and [bānâ](../../strongs/h/h1129.md). So they [ḥāzaq](../../strongs/h/h2388.md) their [yad](../../strongs/h/h3027.md) for this [towb](../../strongs/h/h2896.md) work.

<a name="nehemiah_2_19"></a>Nehemiah 2:19

But when [Sanḇallāṭ](../../strongs/h/h5571.md) the [ḥōrōnî](../../strongs/h/h2772.md), and [Ṭôḇîyâ](../../strongs/h/h2900.md) the ['ebed](../../strongs/h/h5650.md), the [ʿAmmôn](../../strongs/h/h5984.md), and [Gešem](../../strongs/h/h1654.md) the [ʿĂrāḇî](../../strongs/h/h6163.md), [shama'](../../strongs/h/h8085.md) it, they laughed us to [lāʿaḡ](../../strongs/h/h3932.md), and [bazah](../../strongs/h/h959.md) us, and ['āmar](../../strongs/h/h559.md), What is this [dabar](../../strongs/h/h1697.md) that ye ['asah](../../strongs/h/h6213.md)? will ye [māraḏ](../../strongs/h/h4775.md) against the [melek](../../strongs/h/h4428.md)?

<a name="nehemiah_2_20"></a>Nehemiah 2:20

Then [shuwb](../../strongs/h/h7725.md) [dabar](../../strongs/h/h1697.md) I them, and ['āmar](../../strongs/h/h559.md) unto them, The ['Elohiym](../../strongs/h/h430.md) of [shamayim](../../strongs/h/h8064.md), he will [tsalach](../../strongs/h/h6743.md) us; therefore we his ['ebed](../../strongs/h/h5650.md) will [quwm](../../strongs/h/h6965.md) and [bānâ](../../strongs/h/h1129.md): but ye have no [cheleq](../../strongs/h/h2506.md), nor [tsedaqah](../../strongs/h/h6666.md), nor [zikārôn](../../strongs/h/h2146.md), in [Yĕruwshalaim](../../strongs/h/h3389.md).

---

[Transliteral Bible](../bible.md)

[Nehemiah](nehemiah.md)

[Nehemiah 1](nehemiah_1.md) - [Nehemiah 3](nehemiah_3.md)