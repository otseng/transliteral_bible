# [Nehemiah 3](https://www.blueletterbible.org/kjv/nehemiah/3)

<a name="nehemiah_3_1"></a>Nehemiah 3:1

Then ['Elyāšîḇ](../../strongs/h/h475.md) the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md) [quwm](../../strongs/h/h6965.md) with his ['ach](../../strongs/h/h251.md) the [kōhēn](../../strongs/h/h3548.md), and they [bānâ](../../strongs/h/h1129.md) the [tso'n](../../strongs/h/h6629.md) [sha'ar](../../strongs/h/h8179.md); they [qadash](../../strongs/h/h6942.md) it, and set ['amad](../../strongs/h/h5975.md) the [deleṯ](../../strongs/h/h1817.md) of it; even unto the [miḡdāl](../../strongs/h/h4026.md) of [mē'â](../../strongs/h/h3968.md) they [qadash](../../strongs/h/h6942.md) it, unto the [miḡdāl](../../strongs/h/h4026.md) of [Ḥănan'Ēl](../../strongs/h/h2606.md).

<a name="nehemiah_3_2"></a>Nehemiah 3:2

And next unto [yad](../../strongs/h/h3027.md) [bānâ](../../strongs/h/h1129.md) the ['enowsh](../../strongs/h/h582.md) of [Yᵊrēḥô](../../strongs/h/h3405.md). And next to them [bānâ](../../strongs/h/h1129.md) [Zakûr](../../strongs/h/h2139.md) the [ben](../../strongs/h/h1121.md) of ['Imrî](../../strongs/h/h566.md).

<a name="nehemiah_3_3"></a>Nehemiah 3:3

But the [dag](../../strongs/h/h1709.md) [sha'ar](../../strongs/h/h8179.md) did the [ben](../../strongs/h/h1121.md) of [Sᵊnā'Â](../../strongs/h/h5570.md) [bānâ](../../strongs/h/h1129.md), who also laid the [qārâ](../../strongs/h/h7136.md) thereof, and set ['amad](../../strongs/h/h5975.md) the [deleṯ](../../strongs/h/h1817.md) thereof, the [manʿûl](../../strongs/h/h4514.md) thereof, and the [bᵊrîaḥ](../../strongs/h/h1280.md) thereof.

<a name="nehemiah_3_4"></a>Nehemiah 3:4

And next unto [yad](../../strongs/h/h3027.md) [ḥāzaq](../../strongs/h/h2388.md) [Mᵊrēmôṯ](../../strongs/h/h4822.md) the [ben](../../strongs/h/h1121.md) of ['Ûrîyâ](../../strongs/h/h223.md), the [ben](../../strongs/h/h1121.md) of [Qôṣ](../../strongs/h/h6976.md). And next unto [yad](../../strongs/h/h3027.md) [ḥāzaq](../../strongs/h/h2388.md) [Mᵊšullām](../../strongs/h/h4918.md) the [ben](../../strongs/h/h1121.md) of [Bereḵyâ](../../strongs/h/h1296.md), the [ben](../../strongs/h/h1121.md) of [Mᵊšêzaḇ'Ēl](../../strongs/h/h4898.md). And next unto [yad](../../strongs/h/h3027.md) [ḥāzaq](../../strongs/h/h2388.md) [Ṣāḏôq](../../strongs/h/h6659.md) the [ben](../../strongs/h/h1121.md) of [BaʿĂnā'](../../strongs/h/h1195.md).

<a name="nehemiah_3_5"></a>Nehemiah 3:5

And next unto [yad](../../strongs/h/h3027.md) the [Tᵊqôʿî](../../strongs/h/h8621.md) [ḥāzaq](../../strongs/h/h2388.md); but their ['addiyr](../../strongs/h/h117.md) [bow'](../../strongs/h/h935.md) not their [ṣaûā'r](../../strongs/h/h6677.md) to the [ʿăḇōḏâ](../../strongs/h/h5656.md) of their ['adown](../../strongs/h/h113.md).

<a name="nehemiah_3_6"></a>Nehemiah 3:6

Moreover the [yāšān](../../strongs/h/h3465.md) [sha'ar](../../strongs/h/h8179.md) [ḥāzaq](../../strongs/h/h2388.md) [Yôyāḏāʿ](../../strongs/h/h3111.md) the [ben](../../strongs/h/h1121.md) of [Pāsēaḥ](../../strongs/h/h6454.md), and [Mᵊšullām](../../strongs/h/h4918.md) the [ben](../../strongs/h/h1121.md) of [Bᵊsôḏyâ](../../strongs/h/h1152.md); they laid the [qārâ](../../strongs/h/h7136.md) thereof, and set ['amad](../../strongs/h/h5975.md) the [deleṯ](../../strongs/h/h1817.md) thereof, and the [manʿûl](../../strongs/h/h4514.md) thereof, and the [bᵊrîaḥ](../../strongs/h/h1280.md) thereof.

<a name="nehemiah_3_7"></a>Nehemiah 3:7

And next unto [yad](../../strongs/h/h3027.md) [ḥāzaq](../../strongs/h/h2388.md) [Mᵊlaṭyâ](../../strongs/h/h4424.md) the [Giḇʿōnî](../../strongs/h/h1393.md), and [Yāḏôn](../../strongs/h/h3036.md) the [mērōnōṯî](../../strongs/h/h4824.md), the ['enowsh](../../strongs/h/h582.md) of [Giḇʿôn](../../strongs/h/h1391.md), and of [Miṣpâ](../../strongs/h/h4709.md), unto the [kicce'](../../strongs/h/h3678.md) of the [peḥâ](../../strongs/h/h6346.md) on this [ʿēḇer](../../strongs/h/h5676.md) the [nāhār](../../strongs/h/h5104.md).

<a name="nehemiah_3_8"></a>Nehemiah 3:8

Next unto [yad](../../strongs/h/h3027.md) [ḥāzaq](../../strongs/h/h2388.md) [ʿUzzî'ēl](../../strongs/h/h5816.md) the [ben](../../strongs/h/h1121.md) of [Ḥarhŏyâ](../../strongs/h/h2736.md), of the [tsaraph](../../strongs/h/h6884.md). Next unto [yad](../../strongs/h/h3027.md) also [ḥāzaq](../../strongs/h/h2388.md) [Ḥănanyâ](../../strongs/h/h2608.md) the [ben](../../strongs/h/h1121.md) of one of the [raqqāḥ](../../strongs/h/h7546.md), and they ['azab](../../strongs/h/h5800.md) [Yĕruwshalaim](../../strongs/h/h3389.md) unto the [rāḥāḇ](../../strongs/h/h7342.md) [ḥômâ](../../strongs/h/h2346.md).

<a name="nehemiah_3_9"></a>Nehemiah 3:9

And next unto [yad](../../strongs/h/h3027.md) [ḥāzaq](../../strongs/h/h2388.md) [Rᵊp̄Āyâ](../../strongs/h/h7509.md) the [ben](../../strongs/h/h1121.md) of [Ḥûr](../../strongs/h/h2354.md), the [śar](../../strongs/h/h8269.md) of the [ḥēṣî](../../strongs/h/h2677.md) [peleḵ](../../strongs/h/h6418.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="nehemiah_3_10"></a>Nehemiah 3:10

And next unto [yad](../../strongs/h/h3027.md) [ḥāzaq](../../strongs/h/h2388.md) [Yᵊḏāyâ](../../strongs/h/h3042.md) the [ben](../../strongs/h/h1121.md) of [Ḥărûmap̄](../../strongs/h/h2739.md), even over against his [bayith](../../strongs/h/h1004.md). And next unto [yad](../../strongs/h/h3027.md) [ḥāzaq](../../strongs/h/h2388.md) [Ḥaṭṭûš](../../strongs/h/h2407.md) the [ben](../../strongs/h/h1121.md) of [Ḥăšaḇnᵊyâ](../../strongs/h/h2813.md).

<a name="nehemiah_3_11"></a>Nehemiah 3:11

[Malkîyâ](../../strongs/h/h4441.md) the [ben](../../strongs/h/h1121.md) of [Ḥārim](../../strongs/h/h2766.md), and [Ḥaššûḇ](../../strongs/h/h2815.md) the [ben](../../strongs/h/h1121.md) of [Paḥaṯ Mô'Āḇ](../../strongs/h/h6355.md), [ḥāzaq](../../strongs/h/h2388.md) the [šēnî](../../strongs/h/h8145.md) [midâ](../../strongs/h/h4060.md), and the [miḡdāl](../../strongs/h/h4026.md) of the [tannûr](../../strongs/h/h8574.md).

<a name="nehemiah_3_12"></a>Nehemiah 3:12

And next unto [yad](../../strongs/h/h3027.md) [ḥāzaq](../../strongs/h/h2388.md) [Šallûm](../../strongs/h/h7967.md) the [ben](../../strongs/h/h1121.md) of [Lôḥēš](../../strongs/h/h3873.md), the [śar](../../strongs/h/h8269.md) of the [ḥēṣî](../../strongs/h/h2677.md) [peleḵ](../../strongs/h/h6418.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), he and his [bath](../../strongs/h/h1323.md).

<a name="nehemiah_3_13"></a>Nehemiah 3:13

The [gay'](../../strongs/h/h1516.md) [sha'ar](../../strongs/h/h8179.md) [ḥāzaq](../../strongs/h/h2388.md) [Ḥānûn](../../strongs/h/h2586.md), and the [yashab](../../strongs/h/h3427.md) of [Zānôaḥ](../../strongs/h/h2182.md); they [bānâ](../../strongs/h/h1129.md) it, and set ['amad](../../strongs/h/h5975.md) the [deleṯ](../../strongs/h/h1817.md) thereof, the [manʿûl](../../strongs/h/h4514.md) thereof, and the [bᵊrîaḥ](../../strongs/h/h1280.md) thereof, and an ['elep̄](../../strongs/h/h505.md) ['ammâ](../../strongs/h/h520.md) on the [ḥômâ](../../strongs/h/h2346.md) unto the ['ašpōṯ](../../strongs/h/h830.md) [sha'ar](../../strongs/h/h8179.md).

<a name="nehemiah_3_14"></a>Nehemiah 3:14

But the ['ašpōṯ](../../strongs/h/h830.md) [sha'ar](../../strongs/h/h8179.md) [ḥāzaq](../../strongs/h/h2388.md) [Malkîyâ](../../strongs/h/h4441.md) the [ben](../../strongs/h/h1121.md) of [Rēḵāḇ](../../strongs/h/h7394.md), the [śar](../../strongs/h/h8269.md) of [peleḵ](../../strongs/h/h6418.md) of [Bêṯ-Hakerem](../../strongs/h/h1021.md); he [bānâ](../../strongs/h/h1129.md) it, and set ['amad](../../strongs/h/h5975.md) the [deleṯ](../../strongs/h/h1817.md) thereof, the [manʿûl](../../strongs/h/h4514.md) thereof, and the [bᵊrîaḥ](../../strongs/h/h1280.md) thereof.

<a name="nehemiah_3_15"></a>Nehemiah 3:15

But the [sha'ar](../../strongs/h/h8179.md) of the ['ayin](../../strongs/h/h5869.md) [ḥāzaq](../../strongs/h/h2388.md) [Šallûn](../../strongs/h/h7968.md) the [ben](../../strongs/h/h1121.md) of [Kl-Ḥōzê](../../strongs/h/h3626.md), the [śar](../../strongs/h/h8269.md) of [peleḵ](../../strongs/h/h6418.md) of [Miṣpâ](../../strongs/h/h4709.md); he [bānâ](../../strongs/h/h1129.md) it, and [ṭālal](../../strongs/h/h2926.md) it, and set ['amad](../../strongs/h/h5975.md) the [deleṯ](../../strongs/h/h1817.md) thereof, the [manʿûl](../../strongs/h/h4514.md) thereof, and the [bᵊrîaḥ](../../strongs/h/h1280.md) thereof, and the [ḥômâ](../../strongs/h/h2346.md) of the [bᵊrēḵâ](../../strongs/h/h1295.md) of [šilōaḥ](../../strongs/h/h7975.md) by the [melek](../../strongs/h/h4428.md) [gan](../../strongs/h/h1588.md), and unto the [maʿălâ](../../strongs/h/h4609.md) that [yarad](../../strongs/h/h3381.md) from the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="nehemiah_3_16"></a>Nehemiah 3:16

['aḥar](../../strongs/h/h310.md) him [ḥāzaq](../../strongs/h/h2388.md) [Nᵊḥemyâ](../../strongs/h/h5166.md) the [ben](../../strongs/h/h1121.md) of [ʿAzbûq](../../strongs/h/h5802.md), the [śar](../../strongs/h/h8269.md) of the [ḥēṣî](../../strongs/h/h2677.md) [peleḵ](../../strongs/h/h6418.md) of [Bêṯ-Ṣûr](../../strongs/h/h1049.md), unto the place over against the [qeber](../../strongs/h/h6913.md) of [Dāviḏ](../../strongs/h/h1732.md), and to the [bᵊrēḵâ](../../strongs/h/h1295.md) that was ['asah](../../strongs/h/h6213.md), and unto the [bayith](../../strongs/h/h1004.md) of the [gibôr](../../strongs/h/h1368.md).

<a name="nehemiah_3_17"></a>Nehemiah 3:17

['aḥar](../../strongs/h/h310.md) him [ḥāzaq](../../strongs/h/h2388.md) the [Lᵊvî](../../strongs/h/h3881.md), [Rᵊḥûm](../../strongs/h/h7348.md) the [ben](../../strongs/h/h1121.md) of [Bānî](../../strongs/h/h1137.md). Next unto [yad](../../strongs/h/h3027.md) [ḥāzaq](../../strongs/h/h2388.md) [Ḥăšaḇyâ](../../strongs/h/h2811.md), the [śar](../../strongs/h/h8269.md) of the [ḥēṣî](../../strongs/h/h2677.md) [peleḵ](../../strongs/h/h6418.md) of [QᵊʿÎlâ](../../strongs/h/h7084.md), in his [peleḵ](../../strongs/h/h6418.md).

<a name="nehemiah_3_18"></a>Nehemiah 3:18

['aḥar](../../strongs/h/h310.md) him [ḥāzaq](../../strongs/h/h2388.md) their ['ach](../../strongs/h/h251.md), [Baûay](../../strongs/h/h942.md) the [ben](../../strongs/h/h1121.md) of [Ḥēnāḏāḏ](../../strongs/h/h2582.md), the [śar](../../strongs/h/h8269.md) of the [ḥēṣî](../../strongs/h/h2677.md) [peleḵ](../../strongs/h/h6418.md) of [QᵊʿÎlâ](../../strongs/h/h7084.md).

<a name="nehemiah_3_19"></a>Nehemiah 3:19

And next to [yad](../../strongs/h/h3027.md) [ḥāzaq](../../strongs/h/h2388.md) [ʿĒzer](../../strongs/h/h5829.md) the [ben](../../strongs/h/h1121.md) of [Yēšûaʿ](../../strongs/h/h3442.md), the [śar](../../strongs/h/h8269.md) of [Miṣpâ](../../strongs/h/h4709.md), [šēnî](../../strongs/h/h8145.md) [midâ](../../strongs/h/h4060.md) over against the going [ʿālâ](../../strongs/h/h5927.md) to the [nešeq](../../strongs/h/h5402.md) at the [miqṣôaʿ](../../strongs/h/h4740.md) of the wall.

<a name="nehemiah_3_20"></a>Nehemiah 3:20

['aḥar](../../strongs/h/h310.md) him [Bārûḵ](../../strongs/h/h1263.md) the [ben](../../strongs/h/h1121.md) of [Zabay](../../strongs/h/h2079.md) [ḥārâ](../../strongs/h/h2734.md) [ḥāzaq](../../strongs/h/h2388.md) the [šēnî](../../strongs/h/h8145.md) [midâ](../../strongs/h/h4060.md), from the [miqṣôaʿ](../../strongs/h/h4740.md) of the wall unto the [peṯaḥ](../../strongs/h/h6607.md) of the [bayith](../../strongs/h/h1004.md) of ['Elyāšîḇ](../../strongs/h/h475.md) the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md).

<a name="nehemiah_3_21"></a>Nehemiah 3:21

['aḥar](../../strongs/h/h310.md) him [ḥāzaq](../../strongs/h/h2388.md) [Mᵊrēmôṯ](../../strongs/h/h4822.md) the [ben](../../strongs/h/h1121.md) of ['Ûrîyâ](../../strongs/h/h223.md) the [ben](../../strongs/h/h1121.md) of [Qôṣ](../../strongs/h/h6976.md) [šēnî](../../strongs/h/h8145.md) [midâ](../../strongs/h/h4060.md), from the [peṯaḥ](../../strongs/h/h6607.md) of the [bayith](../../strongs/h/h1004.md) of ['Elyāšîḇ](../../strongs/h/h475.md) even to the [taḵlîṯ](../../strongs/h/h8503.md) of the [bayith](../../strongs/h/h1004.md) of ['Elyāšîḇ](../../strongs/h/h475.md).

<a name="nehemiah_3_22"></a>Nehemiah 3:22

And ['aḥar](../../strongs/h/h310.md) him [ḥāzaq](../../strongs/h/h2388.md) the [kōhēn](../../strongs/h/h3548.md), the ['enowsh](../../strongs/h/h582.md) of the [kikār](../../strongs/h/h3603.md).

<a name="nehemiah_3_23"></a>Nehemiah 3:23

['aḥar](../../strongs/h/h310.md) him [ḥāzaq](../../strongs/h/h2388.md) [Binyāmîn](../../strongs/h/h1144.md) and [Ḥaššûḇ](../../strongs/h/h2815.md) over against their [bayith](../../strongs/h/h1004.md). ['aḥar](../../strongs/h/h310.md) him [ḥāzaq](../../strongs/h/h2388.md) [ʿĂzaryâ](../../strongs/h/h5838.md) the [ben](../../strongs/h/h1121.md) of [MaʿĂśêâ](../../strongs/h/h4641.md) the [ben](../../strongs/h/h1121.md) of [ʿĂnanyâ](../../strongs/h/h6055.md) ['ēṣel](../../strongs/h/h681.md) his [bayith](../../strongs/h/h1004.md).

<a name="nehemiah_3_24"></a>Nehemiah 3:24

['aḥar](../../strongs/h/h310.md) him [ḥāzaq](../../strongs/h/h2388.md) [Binnûy](../../strongs/h/h1131.md) the [ben](../../strongs/h/h1121.md) of [Ḥēnāḏāḏ](../../strongs/h/h2582.md) [šēnî](../../strongs/h/h8145.md) [midâ](../../strongs/h/h4060.md), from the [bayith](../../strongs/h/h1004.md) of [ʿĂzaryâ](../../strongs/h/h5838.md) unto the [miqṣôaʿ](../../strongs/h/h4740.md) of the wall, even unto the [pinnâ](../../strongs/h/h6438.md).

<a name="nehemiah_3_25"></a>Nehemiah 3:25

[Pālāl](../../strongs/h/h6420.md) the [ben](../../strongs/h/h1121.md) of ['Ûzay](../../strongs/h/h186.md), over against the [miqṣôaʿ](../../strongs/h/h4740.md) of the wall, and the [miḡdāl](../../strongs/h/h4026.md) which lieth [yāṣā'](../../strongs/h/h3318.md) from the [melek](../../strongs/h/h4428.md) ['elyown](../../strongs/h/h5945.md) [bayith](../../strongs/h/h1004.md), that was by the [ḥāṣēr](../../strongs/h/h2691.md) of the [maṭṭārâ](../../strongs/h/h4307.md). ['aḥar](../../strongs/h/h310.md) him [Pᵊḏāyâ](../../strongs/h/h6305.md) the [ben](../../strongs/h/h1121.md) of [ParʿŠ](../../strongs/h/h6551.md).

<a name="nehemiah_3_26"></a>Nehemiah 3:26

Moreover the [Nāṯîn](../../strongs/h/h5411.md) [yashab](../../strongs/h/h3427.md) in [ʿŌp̄El](../../strongs/h/h6077.md), unto the place over against the [mayim](../../strongs/h/h4325.md) [sha'ar](../../strongs/h/h8179.md) toward the [mizrach](../../strongs/h/h4217.md), and the [miḡdāl](../../strongs/h/h4026.md) that lieth [yāṣā'](../../strongs/h/h3318.md).

<a name="nehemiah_3_27"></a>Nehemiah 3:27

['aḥar](../../strongs/h/h310.md) them the [Tᵊqôʿî](../../strongs/h/h8621.md) [ḥāzaq](../../strongs/h/h2388.md) [šēnî](../../strongs/h/h8145.md) [midâ](../../strongs/h/h4060.md), over against the [gadowl](../../strongs/h/h1419.md) [miḡdāl](../../strongs/h/h4026.md) that lieth [yāṣā'](../../strongs/h/h3318.md), even unto the [ḥômâ](../../strongs/h/h2346.md) of [ʿŌp̄El](../../strongs/h/h6077.md).

<a name="nehemiah_3_28"></a>Nehemiah 3:28

From above the [sûs](../../strongs/h/h5483.md) [sha'ar](../../strongs/h/h8179.md) [ḥāzaq](../../strongs/h/h2388.md) the [kōhēn](../../strongs/h/h3548.md), every ['iysh](../../strongs/h/h376.md) over [neḡeḏ](../../strongs/h/h5048.md) his [bayith](../../strongs/h/h1004.md).

<a name="nehemiah_3_29"></a>Nehemiah 3:29

['aḥar](../../strongs/h/h310.md) them [ḥāzaq](../../strongs/h/h2388.md) [Ṣāḏôq](../../strongs/h/h6659.md) the [ben](../../strongs/h/h1121.md) of ['Immēr](../../strongs/h/h564.md) over against his [bayith](../../strongs/h/h1004.md). ['aḥar](../../strongs/h/h310.md) him [ḥāzaq](../../strongs/h/h2388.md) also [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the [ben](../../strongs/h/h1121.md) of [Šᵊḵanyâ](../../strongs/h/h7935.md), the [shamar](../../strongs/h/h8104.md) of the [mizrach](../../strongs/h/h4217.md) [sha'ar](../../strongs/h/h8179.md).

<a name="nehemiah_3_30"></a>Nehemiah 3:30

['aḥar](../../strongs/h/h310.md) him [ḥāzaq](../../strongs/h/h2388.md) [Ḥănanyâ](../../strongs/h/h2608.md) the [ben](../../strongs/h/h1121.md) of [Šelemyâ](../../strongs/h/h8018.md), and [Ḥānûn](../../strongs/h/h2586.md) the [šiššî](../../strongs/h/h8345.md) [ben](../../strongs/h/h1121.md) of [Ṣālāp̄](../../strongs/h/h6764.md), [šēnî](../../strongs/h/h8145.md) [midâ](../../strongs/h/h4060.md). ['aḥar](../../strongs/h/h310.md) him [ḥāzaq](../../strongs/h/h2388.md) [Mᵊšullām](../../strongs/h/h4918.md) the [ben](../../strongs/h/h1121.md) of [Bereḵyâ](../../strongs/h/h1296.md) over against his [niškâ](../../strongs/h/h5393.md).

<a name="nehemiah_3_31"></a>Nehemiah 3:31

['aḥar](../../strongs/h/h310.md) him [ḥāzaq](../../strongs/h/h2388.md) [Malkîyâ](../../strongs/h/h4441.md) the [ṣōrp̄î](../../strongs/h/h6885.md) [ben](../../strongs/h/h1121.md) unto the [bayith](../../strongs/h/h1004.md) of the [Nāṯîn](../../strongs/h/h5411.md), and of the [rāḵal](../../strongs/h/h7402.md), over against the [sha'ar](../../strongs/h/h8179.md) [mip̄qāḏ](../../strongs/h/h4663.md), and to the going [ʿălîyâ](../../strongs/h/h5944.md) of the [pinnâ](../../strongs/h/h6438.md).

<a name="nehemiah_3_32"></a>Nehemiah 3:32

And between the going [ʿălîyâ](../../strongs/h/h5944.md) of the [pinnâ](../../strongs/h/h6438.md) unto the [tso'n](../../strongs/h/h6629.md) [sha'ar](../../strongs/h/h8179.md) [ḥāzaq](../../strongs/h/h2388.md) the [tsaraph](../../strongs/h/h6884.md) and the [rāḵal](../../strongs/h/h7402.md).

---

[Transliteral Bible](../bible.md)

[Nehemiah](nehemiah.md)

[Nehemiah 2](nehemiah_2.md) - [Nehemiah 4](nehemiah_4.md)