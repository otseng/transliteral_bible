# [Nehemiah 9](https://www.blueletterbible.org/kjv/nehemiah/9)

<a name="nehemiah_9_1"></a>Nehemiah 9:1

Now in the [ʿeśrîm](../../strongs/h/h6242.md) and ['arbaʿ](../../strongs/h/h702.md) [yowm](../../strongs/h/h3117.md) of this [ḥōḏeš](../../strongs/h/h2320.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were ['āsap̄](../../strongs/h/h622.md) with [ṣôm](../../strongs/h/h6685.md), and with [śaq](../../strongs/h/h8242.md), and ['ăḏāmâ](../../strongs/h/h127.md) upon them.

<a name="nehemiah_9_2"></a>Nehemiah 9:2

And the [zera'](../../strongs/h/h2233.md) of [Yisra'el](../../strongs/h/h3478.md) [bāḏal](../../strongs/h/h914.md) themselves from all [ben](../../strongs/h/h1121.md) [nēḵār](../../strongs/h/h5236.md), and ['amad](../../strongs/h/h5975.md) and [yadah](../../strongs/h/h3034.md) their [chatta'ath](../../strongs/h/h2403.md), and the ['avon](../../strongs/h/h5771.md) of their ['ab](../../strongs/h/h1.md).

<a name="nehemiah_9_3"></a>Nehemiah 9:3

And they stood [quwm](../../strongs/h/h6965.md) in their [ʿōmeḏ](../../strongs/h/h5977.md), and [qara'](../../strongs/h/h7121.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md) one fourth [rᵊḇîʿî](../../strongs/h/h7243.md) of the [yowm](../../strongs/h/h3117.md); and another fourth [rᵊḇîʿî](../../strongs/h/h7243.md) they [yadah](../../strongs/h/h3034.md), and [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md).

<a name="nehemiah_9_4"></a>Nehemiah 9:4

Then stood [quwm](../../strongs/h/h6965.md) upon the [maʿălê](../../strongs/h/h4608.md), of the [Lᵊvî](../../strongs/h/h3881.md), [Yēšûaʿ](../../strongs/h/h3442.md), and [Bānî](../../strongs/h/h1137.md), [Qaḏmî'Ēl](../../strongs/h/h6934.md), [Šᵊḇanyâ](../../strongs/h/h7645.md), [Bunnî](../../strongs/h/h1138.md), [Šērēḇyâ](../../strongs/h/h8274.md), [Bānî](../../strongs/h/h1137.md), and [Kᵊnānî](../../strongs/h/h3662.md), and [zāʿaq](../../strongs/h/h2199.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md) unto [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md).

<a name="nehemiah_9_5"></a>Nehemiah 9:5

Then the [Lᵊvî](../../strongs/h/h3881.md), [Yēšûaʿ](../../strongs/h/h3442.md), and [Qaḏmî'Ēl](../../strongs/h/h6934.md), [Bānî](../../strongs/h/h1137.md), [Ḥăšaḇnᵊyâ](../../strongs/h/h2813.md), [Šērēḇyâ](../../strongs/h/h8274.md), [Hôḏîyâ](../../strongs/h/h1941.md), [Šᵊḇanyâ](../../strongs/h/h7645.md), and [Pᵊṯaḥyâ](../../strongs/h/h6611.md), ['āmar](../../strongs/h/h559.md), Stand [quwm](../../strongs/h/h6965.md) and [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) ['owlam](../../strongs/h/h5769.md) and ['owlam](../../strongs/h/h5769.md): and [barak](../../strongs/h/h1288.md) be thy [kabowd](../../strongs/h/h3519.md) [shem](../../strongs/h/h8034.md), which is [ruwm](../../strongs/h/h7311.md) above all [bĕrakah](../../strongs/h/h1293.md) and [tehillah](../../strongs/h/h8416.md).

<a name="nehemiah_9_6"></a>Nehemiah 9:6

Thou, even thou, art [Yĕhovah](../../strongs/h/h3068.md) alone; thou hast ['asah](../../strongs/h/h6213.md) [shamayim](../../strongs/h/h8064.md), the [shamayim](../../strongs/h/h8064.md) of [shamayim](../../strongs/h/h8064.md), with all their [tsaba'](../../strongs/h/h6635.md), the ['erets](../../strongs/h/h776.md), and all things that are therein, the [yam](../../strongs/h/h3220.md), and all that is therein, and thou [ḥāyâ](../../strongs/h/h2421.md) them all; and the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md) [shachah](../../strongs/h/h7812.md) thee.

<a name="nehemiah_9_7"></a>Nehemiah 9:7

Thou art [Yĕhovah](../../strongs/h/h3068.md) the ['Elohiym](../../strongs/h/h430.md), who didst [bāḥar](../../strongs/h/h977.md) ['Aḇrām](../../strongs/h/h87.md), and broughtest him [yāṣā'](../../strongs/h/h3318.md) out of ['Ûr](../../strongs/h/h218.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), and [śûm](../../strongs/h/h7760.md) him the [shem](../../strongs/h/h8034.md) of ['Abraham](../../strongs/h/h85.md);

<a name="nehemiah_9_8"></a>Nehemiah 9:8

And [māṣā'](../../strongs/h/h4672.md) his [lebab](../../strongs/h/h3824.md) ['aman](../../strongs/h/h539.md) [paniym](../../strongs/h/h6440.md) thee, and [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with him to [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) of the [Kᵊnaʿănî](../../strongs/h/h3669.md), the [Ḥitî](../../strongs/h/h2850.md), the ['Ĕmōrî](../../strongs/h/h567.md), and the [Pᵊrizzî](../../strongs/h/h6522.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md), and the [Girgāšî](../../strongs/h/h1622.md), to [nathan](../../strongs/h/h5414.md) it, I say, to his [zera'](../../strongs/h/h2233.md), and hast [quwm](../../strongs/h/h6965.md) thy [dabar](../../strongs/h/h1697.md); for thou art [tsaddiyq](../../strongs/h/h6662.md):

<a name="nehemiah_9_9"></a>Nehemiah 9:9

And didst [ra'ah](../../strongs/h/h7200.md) the ['oniy](../../strongs/h/h6040.md) of our ['ab](../../strongs/h/h1.md) in [Mitsrayim](../../strongs/h/h4714.md), and [shama'](../../strongs/h/h8085.md) their [zaʿaq](../../strongs/h/h2201.md) by the [sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md);

<a name="nehemiah_9_10"></a>Nehemiah 9:10

And [nathan](../../strongs/h/h5414.md) ['ôṯ](../../strongs/h/h226.md) and [môp̄ēṯ](../../strongs/h/h4159.md) upon [Parʿô](../../strongs/h/h6547.md), and on all his ['ebed](../../strongs/h/h5650.md), and on all the ['am](../../strongs/h/h5971.md) of his ['erets](../../strongs/h/h776.md): for thou [yada'](../../strongs/h/h3045.md) that they dealt [zûḏ](../../strongs/h/h2102.md) against them. So didst thou ['asah](../../strongs/h/h6213.md) thee a [shem](../../strongs/h/h8034.md), as it is this [yowm](../../strongs/h/h3117.md).

<a name="nehemiah_9_11"></a>Nehemiah 9:11

And thou didst [bāqaʿ](../../strongs/h/h1234.md) the [yam](../../strongs/h/h3220.md) [paniym](../../strongs/h/h6440.md) them, so that they went ['abar](../../strongs/h/h5674.md) the [tavek](../../strongs/h/h8432.md) of the [yam](../../strongs/h/h3220.md) on the dry [yabāšâ](../../strongs/h/h3004.md); and their [radaph](../../strongs/h/h7291.md) thou [shalak](../../strongs/h/h7993.md) into the [mᵊṣôlâ](../../strongs/h/h4688.md), as an ['eben](../../strongs/h/h68.md) into the ['az](../../strongs/h/h5794.md) [mayim](../../strongs/h/h4325.md).

<a name="nehemiah_9_12"></a>Nehemiah 9:12

Moreover thou [nachah](../../strongs/h/h5148.md) them in the [yômām](../../strongs/h/h3119.md) by a [ʿānān](../../strongs/h/h6051.md) [ʿammûḏ](../../strongs/h/h5982.md); and in the [layil](../../strongs/h/h3915.md) by a [ʿammûḏ](../../strongs/h/h5982.md) of ['esh](../../strongs/h/h784.md), to give them ['owr](../../strongs/h/h215.md) in the [derek](../../strongs/h/h1870.md) wherein they should [yālaḵ](../../strongs/h/h3212.md).

<a name="nehemiah_9_13"></a>Nehemiah 9:13

Thou camest [yarad](../../strongs/h/h3381.md) also upon [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md), and [dabar](../../strongs/h/h1696.md) with them from [shamayim](../../strongs/h/h8064.md), and [nathan](../../strongs/h/h5414.md) them [yashar](../../strongs/h/h3477.md) [mishpat](../../strongs/h/h4941.md), and ['emeth](../../strongs/h/h571.md) [towrah](../../strongs/h/h8451.md), [towb](../../strongs/h/h2896.md) [choq](../../strongs/h/h2706.md) and [mitsvah](../../strongs/h/h4687.md):

<a name="nehemiah_9_14"></a>Nehemiah 9:14

And madest [yada'](../../strongs/h/h3045.md) unto them thy [qodesh](../../strongs/h/h6944.md) [shabbath](../../strongs/h/h7676.md), and [tsavah](../../strongs/h/h6680.md) them [mitsvah](../../strongs/h/h4687.md), [choq](../../strongs/h/h2706.md), and [towrah](../../strongs/h/h8451.md), by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md) thy ['ebed](../../strongs/h/h5650.md):

<a name="nehemiah_9_15"></a>Nehemiah 9:15

And [nathan](../../strongs/h/h5414.md) them [lechem](../../strongs/h/h3899.md) from [shamayim](../../strongs/h/h8064.md) for their [rāʿāḇ](../../strongs/h/h7458.md), and broughtest [yāṣā'](../../strongs/h/h3318.md) [mayim](../../strongs/h/h4325.md) for them out of the [cela'](../../strongs/h/h5553.md) for their [ṣāmā'](../../strongs/h/h6772.md), and ['āmar](../../strongs/h/h559.md) them that they should go [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md) which thou hadst [yad](../../strongs/h/h3027.md) [nasa'](../../strongs/h/h5375.md) to [nathan](../../strongs/h/h5414.md) them.

<a name="nehemiah_9_16"></a>Nehemiah 9:16

But they and our ['ab](../../strongs/h/h1.md) dealt [zûḏ](../../strongs/h/h2102.md), and [qāšâ](../../strongs/h/h7185.md) their [ʿōrep̄](../../strongs/h/h6203.md), and [shama'](../../strongs/h/h8085.md) not to thy [mitsvah](../../strongs/h/h4687.md),

<a name="nehemiah_9_17"></a>Nehemiah 9:17

And [mā'ēn](../../strongs/h/h3985.md) to [shama'](../../strongs/h/h8085.md), neither were [zakar](../../strongs/h/h2142.md) of thy [pala'](../../strongs/h/h6381.md) that thou ['asah](../../strongs/h/h6213.md) among them; but [qāšâ](../../strongs/h/h7185.md) their [ʿōrep̄](../../strongs/h/h6203.md), and in their [mᵊrî](../../strongs/h/h4805.md) [nathan](../../strongs/h/h5414.md) a [ro'sh](../../strongs/h/h7218.md) to [shuwb](../../strongs/h/h7725.md) to their [ʿaḇḏûṯ](../../strongs/h/h5659.md): but thou art an ['ĕlvôha](../../strongs/h/h433.md) ready to [sᵊlîḥâ](../../strongs/h/h5547.md), [ḥanwn](../../strongs/h/h2587.md) and [raḥwm](../../strongs/h/h7349.md), ['ārēḵ](../../strongs/h/h750.md) to ['aph](../../strongs/h/h639.md), and of [rab](../../strongs/h/h7227.md) [checed](../../strongs/h/h2617.md), and ['azab](../../strongs/h/h5800.md) them not.

<a name="nehemiah_9_18"></a>Nehemiah 9:18

Yea, when they had ['asah](../../strongs/h/h6213.md) them a [massēḵâ](../../strongs/h/h4541.md) [ʿēḡel](../../strongs/h/h5695.md), and ['āmar](../../strongs/h/h559.md), This is thy ['Elohiym](../../strongs/h/h430.md) that brought thee [ʿālâ](../../strongs/h/h5927.md) out of [Mitsrayim](../../strongs/h/h4714.md), and had ['asah](../../strongs/h/h6213.md) [gadowl](../../strongs/h/h1419.md) [ne'āṣâ](../../strongs/h/h5007.md);

<a name="nehemiah_9_19"></a>Nehemiah 9:19

Yet thou in thy [rab](../../strongs/h/h7227.md) [raḥam](../../strongs/h/h7356.md) ['azab](../../strongs/h/h5800.md) them not in the [midbar](../../strongs/h/h4057.md): the [ʿammûḏ](../../strongs/h/h5982.md) of the [ʿānān](../../strongs/h/h6051.md) [cuwr](../../strongs/h/h5493.md) not from them by [yômām](../../strongs/h/h3119.md), to [nachah](../../strongs/h/h5148.md) them in the [derek](../../strongs/h/h1870.md); neither the [ʿammûḏ](../../strongs/h/h5982.md) of ['esh](../../strongs/h/h784.md) by [layil](../../strongs/h/h3915.md), to shew them ['owr](../../strongs/h/h215.md), and the [derek](../../strongs/h/h1870.md) wherein they should [yālaḵ](../../strongs/h/h3212.md).

<a name="nehemiah_9_20"></a>Nehemiah 9:20

Thou [nathan](../../strongs/h/h5414.md) also thy [towb](../../strongs/h/h2896.md) [ruwach](../../strongs/h/h7307.md) to [sakal](../../strongs/h/h7919.md) them, and [mānaʿ](../../strongs/h/h4513.md) not thy [man](../../strongs/h/h4478.md) from their [peh](../../strongs/h/h6310.md), and [nathan](../../strongs/h/h5414.md) them [mayim](../../strongs/h/h4325.md) for their [ṣāmā'](../../strongs/h/h6772.md).

<a name="nehemiah_9_21"></a>Nehemiah 9:21

Yea, ['arbāʿîm](../../strongs/h/h705.md) [šānâ](../../strongs/h/h8141.md) didst thou [kûl](../../strongs/h/h3557.md) them in the [midbar](../../strongs/h/h4057.md), so that they [ḥāsēr](../../strongs/h/h2637.md) nothing; their [śalmâ](../../strongs/h/h8008.md) waxed not [bālâ](../../strongs/h/h1086.md), and their [regel](../../strongs/h/h7272.md) [bāṣēq](../../strongs/h/h1216.md) not.

<a name="nehemiah_9_22"></a>Nehemiah 9:22

Moreover thou [nathan](../../strongs/h/h5414.md) them [mamlāḵâ](../../strongs/h/h4467.md) and ['am](../../strongs/h/h5971.md), and didst [chalaq](../../strongs/h/h2505.md) them into [pē'â](../../strongs/h/h6285.md): so they [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md) of [Sîḥôn](../../strongs/h/h5511.md), and the ['erets](../../strongs/h/h776.md) of the [melek](../../strongs/h/h4428.md) of [Hešbôn](../../strongs/h/h2809.md), and the ['erets](../../strongs/h/h776.md) of [ʿÔḡ](../../strongs/h/h5747.md) [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md).

<a name="nehemiah_9_23"></a>Nehemiah 9:23

Their [ben](../../strongs/h/h1121.md) also [rabah](../../strongs/h/h7235.md) thou as the [kowkab](../../strongs/h/h3556.md) of [shamayim](../../strongs/h/h8064.md), and [bow'](../../strongs/h/h935.md) them into the ['erets](../../strongs/h/h776.md), concerning which thou hadst ['āmar](../../strongs/h/h559.md) to their ['ab](../../strongs/h/h1.md), that they should go [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) it.

<a name="nehemiah_9_24"></a>Nehemiah 9:24

So the [ben](../../strongs/h/h1121.md) [bow'](../../strongs/h/h935.md) and [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md), and thou [kānaʿ](../../strongs/h/h3665.md) [paniym](../../strongs/h/h6440.md) them the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md), the [Kᵊnaʿănî](../../strongs/h/h3669.md), and [nathan](../../strongs/h/h5414.md) them into their [yad](../../strongs/h/h3027.md), with their [melek](../../strongs/h/h4428.md), and the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), that they might ['asah](../../strongs/h/h6213.md) with them as they [ratsown](../../strongs/h/h7522.md).

<a name="nehemiah_9_25"></a>Nehemiah 9:25

And they [lāḵaḏ](../../strongs/h/h3920.md) [bāṣar](../../strongs/h/h1219.md) [ʿîr](../../strongs/h/h5892.md), and a [šāmēn](../../strongs/h/h8082.md) ['ăḏāmâ](../../strongs/h/h127.md), and [yarash](../../strongs/h/h3423.md) [bayith](../../strongs/h/h1004.md) [mālē'](../../strongs/h/h4392.md) of all [ṭûḇ](../../strongs/h/h2898.md), [bowr](../../strongs/h/h953.md) [ḥāṣaḇ](../../strongs/h/h2672.md), [kerem](../../strongs/h/h3754.md), and [zayiṯ](../../strongs/h/h2132.md), and [ma'akal](../../strongs/h/h3978.md) ['ets](../../strongs/h/h6086.md) in [rōḇ](../../strongs/h/h7230.md): so they did ['akal](../../strongs/h/h398.md), and were [sāׂbaʿ](../../strongs/h/h7646.md), and became [šāman](../../strongs/h/h8080.md), and [ʿāḏan](../../strongs/h/h5727.md) themselves in thy [gadowl](../../strongs/h/h1419.md) [ṭûḇ](../../strongs/h/h2898.md).

<a name="nehemiah_9_26"></a>Nehemiah 9:26

Nevertheless they were [marah](../../strongs/h/h4784.md), and [māraḏ](../../strongs/h/h4775.md) against thee, and [shalak](../../strongs/h/h7993.md) thy [towrah](../../strongs/h/h8451.md) ['aḥar](../../strongs/h/h310.md) their [gav](../../strongs/h/h1458.md), and [harag](../../strongs/h/h2026.md) thy [nāḇî'](../../strongs/h/h5030.md) which [ʿûḏ](../../strongs/h/h5749.md) against them to [shuwb](../../strongs/h/h7725.md) them to thee, and they ['asah](../../strongs/h/h6213.md) [gadowl](../../strongs/h/h1419.md) [ne'āṣâ](../../strongs/h/h5007.md).

<a name="nehemiah_9_27"></a>Nehemiah 9:27

Therefore thou [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of their [tsar](../../strongs/h/h6862.md), who [tsarar](../../strongs/h/h6887.md) them: and in the [ʿēṯ](../../strongs/h/h6256.md) of their [tsarah](../../strongs/h/h6869.md), when they [ṣāʿaq](../../strongs/h/h6817.md) unto thee, thou [shama'](../../strongs/h/h8085.md) them from [shamayim](../../strongs/h/h8064.md); and according to thy [rab](../../strongs/h/h7227.md) [raḥam](../../strongs/h/h7356.md) thou [nathan](../../strongs/h/h5414.md) them [yasha'](../../strongs/h/h3467.md), who [yasha'](../../strongs/h/h3467.md) them out of the [yad](../../strongs/h/h3027.md) of their [tsar](../../strongs/h/h6862.md).

<a name="nehemiah_9_28"></a>Nehemiah 9:28

But after they had [nuwach](../../strongs/h/h5117.md), they ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) [shuwb](../../strongs/h/h7725.md) [paniym](../../strongs/h/h6440.md) thee: therefore ['azab](../../strongs/h/h5800.md) thou them in the [yad](../../strongs/h/h3027.md) of their ['oyeb](../../strongs/h/h341.md), so that they had the [radah](../../strongs/h/h7287.md) over them: yet when they [shuwb](../../strongs/h/h7725.md), and [zāʿaq](../../strongs/h/h2199.md) unto thee, thou [shama'](../../strongs/h/h8085.md) them from [shamayim](../../strongs/h/h8064.md); and [rab](../../strongs/h/h7227.md) [ʿēṯ](../../strongs/h/h6256.md) didst thou [natsal](../../strongs/h/h5337.md) them according to thy [raḥam](../../strongs/h/h7356.md);

<a name="nehemiah_9_29"></a>Nehemiah 9:29

And [ʿûḏ](../../strongs/h/h5749.md) against them, that thou mightest bring them [shuwb](../../strongs/h/h7725.md) unto thy [towrah](../../strongs/h/h8451.md): yet they dealt [zûḏ](../../strongs/h/h2102.md), and [shama'](../../strongs/h/h8085.md) not unto thy [mitsvah](../../strongs/h/h4687.md), but [chata'](../../strongs/h/h2398.md) against thy [mishpat](../../strongs/h/h4941.md), (which if an ['āḏām](../../strongs/h/h120.md) ['asah](../../strongs/h/h6213.md), he shall [ḥāyâ](../../strongs/h/h2421.md) in them;) and [nathan](../../strongs/h/h5414.md) [sārar](../../strongs/h/h5637.md) the [kāṯēp̄](../../strongs/h/h3802.md), and [qāšâ](../../strongs/h/h7185.md) their [ʿōrep̄](../../strongs/h/h6203.md), and would not [shama'](../../strongs/h/h8085.md).

<a name="nehemiah_9_30"></a>Nehemiah 9:30

Yet [rab](../../strongs/h/h7227.md) [šānâ](../../strongs/h/h8141.md) didst thou [mashak](../../strongs/h/h4900.md) them, and [ʿûḏ](../../strongs/h/h5749.md) against them by thy [ruwach](../../strongs/h/h7307.md) [yad](../../strongs/h/h3027.md) thy [nāḇî'](../../strongs/h/h5030.md): yet would they not ['azan](../../strongs/h/h238.md): therefore [nathan](../../strongs/h/h5414.md) thou them into the [yad](../../strongs/h/h3027.md) of the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md).

<a name="nehemiah_9_31"></a>Nehemiah 9:31

Nevertheless for thy [rab](../../strongs/h/h7227.md) [raḥam](../../strongs/h/h7356.md) sake thou ['asah](../../strongs/h/h6213.md) not utterly [kālâ](../../strongs/h/h3617.md) them, nor ['azab](../../strongs/h/h5800.md) them; for thou art a [ḥanwn](../../strongs/h/h2587.md) and [raḥwm](../../strongs/h/h7349.md) ['el](../../strongs/h/h410.md).

<a name="nehemiah_9_32"></a>Nehemiah 9:32

Now therefore, our ['Elohiym](../../strongs/h/h430.md), the [gadowl](../../strongs/h/h1419.md), the [gibôr](../../strongs/h/h1368.md), and the [yare'](../../strongs/h/h3372.md) ['el](../../strongs/h/h410.md), who [shamar](../../strongs/h/h8104.md) [bĕriyth](../../strongs/h/h1285.md) and [checed](../../strongs/h/h2617.md), let not all the [tᵊlā'â](../../strongs/h/h8513.md) seem [māʿaṭ](../../strongs/h/h4591.md) [paniym](../../strongs/h/h6440.md) thee, that hath [māṣā'](../../strongs/h/h4672.md) upon us, on our [melek](../../strongs/h/h4428.md), on our [śar](../../strongs/h/h8269.md), and on our [kōhēn](../../strongs/h/h3548.md), and on our [nāḇî'](../../strongs/h/h5030.md), and on our ['ab](../../strongs/h/h1.md), and on all thy ['am](../../strongs/h/h5971.md), since the [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="nehemiah_9_33"></a>Nehemiah 9:33

Howbeit thou art [tsaddiyq](../../strongs/h/h6662.md) in all that is [bow'](../../strongs/h/h935.md) upon us; for thou hast ['asah](../../strongs/h/h6213.md) ['emeth](../../strongs/h/h571.md), but we have done [rāšaʿ](../../strongs/h/h7561.md):

<a name="nehemiah_9_34"></a>Nehemiah 9:34

Neither have our [melek](../../strongs/h/h4428.md), our [śar](../../strongs/h/h8269.md), our [kōhēn](../../strongs/h/h3548.md), nor our ['ab](../../strongs/h/h1.md), ['asah](../../strongs/h/h6213.md) thy [towrah](../../strongs/h/h8451.md), nor [qashab](../../strongs/h/h7181.md) unto thy [mitsvah](../../strongs/h/h4687.md) and thy [ʿēḏûṯ](../../strongs/h/h5715.md), wherewith thou didst [ʿûḏ](../../strongs/h/h5749.md) against them.

<a name="nehemiah_9_35"></a>Nehemiah 9:35

For they have not ['abad](../../strongs/h/h5647.md) thee in their [malkuwth](../../strongs/h/h4438.md), and in thy [rab](../../strongs/h/h7227.md) [ṭûḇ](../../strongs/h/h2898.md) that thou [nathan](../../strongs/h/h5414.md) them, and in the [rāḥāḇ](../../strongs/h/h7342.md) and [šāmēn](../../strongs/h/h8082.md) ['erets](../../strongs/h/h776.md) which thou [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) them, neither [shuwb](../../strongs/h/h7725.md) they from their [ra'](../../strongs/h/h7451.md) [maʿălāl](../../strongs/h/h4611.md).

<a name="nehemiah_9_36"></a>Nehemiah 9:36

Behold, we are ['ebed](../../strongs/h/h5650.md) this [yowm](../../strongs/h/h3117.md), and for the ['erets](../../strongs/h/h776.md) that thou [nathan](../../strongs/h/h5414.md) unto our ['ab](../../strongs/h/h1.md) to ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) thereof and the [ṭûḇ](../../strongs/h/h2898.md) thereof, behold, we are ['ebed](../../strongs/h/h5650.md) in it:

<a name="nehemiah_9_37"></a>Nehemiah 9:37

And it yieldeth [rabah](../../strongs/h/h7235.md) [tᵊḇû'â](../../strongs/h/h8393.md) unto the [melek](../../strongs/h/h4428.md) whom thou hast [nathan](../../strongs/h/h5414.md) over us because of our [chatta'ath](../../strongs/h/h2403.md): also they have [mashal](../../strongs/h/h4910.md) over our [gᵊvîyâ](../../strongs/h/h1472.md), and over our [bĕhemah](../../strongs/h/h929.md), at their [ratsown](../../strongs/h/h7522.md), and we are in [gadowl](../../strongs/h/h1419.md) [tsarah](../../strongs/h/h6869.md).

<a name="nehemiah_9_38"></a>Nehemiah 9:38

And because of all this we [karath](../../strongs/h/h3772.md) a ['ămānâ](../../strongs/h/h548.md) covenant, and [kāṯaḇ](../../strongs/h/h3789.md) it; and our [śar](../../strongs/h/h8269.md), [Lᵊvî](../../strongs/h/h3881.md), and [kōhēn](../../strongs/h/h3548.md), [ḥāṯam](../../strongs/h/h2856.md) unto it.

---

[Transliteral Bible](../bible.md)

[Nehemiah](nehemiah.md)

[Nehemiah 8](nehemiah_8.md) - [Nehemiah 10](nehemiah_10.md)