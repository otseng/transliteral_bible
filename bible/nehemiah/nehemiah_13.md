# [Nehemiah 13](https://www.blueletterbible.org/kjv/nehemiah/13)

<a name="nehemiah_13_1"></a>Nehemiah 13:1

On that [yowm](../../strongs/h/h3117.md) they [qara'](../../strongs/h/h7121.md) in the [sēp̄er](../../strongs/h/h5612.md) of [Mōshe](../../strongs/h/h4872.md) in the ['ozen](../../strongs/h/h241.md) of the ['am](../../strongs/h/h5971.md); and therein was [māṣā'](../../strongs/h/h4672.md) [kāṯaḇ](../../strongs/h/h3789.md), that the [ʿAmmôn](../../strongs/h/h5984.md) and the [Mô'āḇî](../../strongs/h/h4125.md) should not [bow'](../../strongs/h/h935.md) into the [qāhēl](../../strongs/h/h6951.md) of ['Elohiym](../../strongs/h/h430.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md);

<a name="nehemiah_13_2"></a>Nehemiah 13:2

Because they [qadam](../../strongs/h/h6923.md) not the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) with [lechem](../../strongs/h/h3899.md) and with [mayim](../../strongs/h/h4325.md), but [śāḵar](../../strongs/h/h7936.md) [Bilʿām](../../strongs/h/h1109.md) against them, that he should [qālal](../../strongs/h/h7043.md) them: howbeit our ['Elohiym](../../strongs/h/h430.md) [hāp̄aḵ](../../strongs/h/h2015.md) the [qᵊlālâ](../../strongs/h/h7045.md) into a [bĕrakah](../../strongs/h/h1293.md).

<a name="nehemiah_13_3"></a>Nehemiah 13:3

Now it came to pass, when they had [shama'](../../strongs/h/h8085.md) the [towrah](../../strongs/h/h8451.md), that they [bāḏal](../../strongs/h/h914.md) from [Yisra'el](../../strongs/h/h3478.md) all the mixed [ʿēreḇ](../../strongs/h/h6154.md).

<a name="nehemiah_13_4"></a>Nehemiah 13:4

And [paniym](../../strongs/h/h6440.md) this, ['Elyāšîḇ](../../strongs/h/h475.md) the [kōhēn](../../strongs/h/h3548.md), having the [nathan](../../strongs/h/h5414.md) of the [liškâ](../../strongs/h/h3957.md) of the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md), was [qarowb](../../strongs/h/h7138.md) unto [Ṭôḇîyâ](../../strongs/h/h2900.md):

<a name="nehemiah_13_5"></a>Nehemiah 13:5

And he had ['asah](../../strongs/h/h6213.md) for him a [gadowl](../../strongs/h/h1419.md) [liškâ](../../strongs/h/h3957.md), where [paniym](../../strongs/h/h6440.md) they [nathan](../../strongs/h/h5414.md) the meat [minchah](../../strongs/h/h4503.md), the [lᵊḇônâ](../../strongs/h/h3828.md), and the [kĕliy](../../strongs/h/h3627.md), and the [maʿăśēr](../../strongs/h/h4643.md) of the [dagan](../../strongs/h/h1715.md), the [tiyrowsh](../../strongs/h/h8492.md), and the [yiṣhār](../../strongs/h/h3323.md), which was [mitsvah](../../strongs/h/h4687.md) to be given to the [Lᵊvî](../../strongs/h/h3881.md), and the [shiyr](../../strongs/h/h7891.md), and the [šôʿēr](../../strongs/h/h7778.md); and the [tᵊrûmâ](../../strongs/h/h8641.md) of the [kōhēn](../../strongs/h/h3548.md).

<a name="nehemiah_13_6"></a>Nehemiah 13:6

But in all this time was not I at [Yĕruwshalaim](../../strongs/h/h3389.md): for in the [šᵊnayim](../../strongs/h/h8147.md) and [šᵊlōšîm](../../strongs/h/h7970.md) [šānâ](../../strongs/h/h8141.md) of ['Artaḥšaštᵊ'](../../strongs/h/h783.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [bow'](../../strongs/h/h935.md) I unto the [melek](../../strongs/h/h4428.md), and [qēṣ](../../strongs/h/h7093.md) certain [yowm](../../strongs/h/h3117.md) obtained I [sha'al](../../strongs/h/h7592.md) of the [melek](../../strongs/h/h4428.md):

<a name="nehemiah_13_7"></a>Nehemiah 13:7

And I [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), and [bîn](../../strongs/h/h995.md) of the [ra'](../../strongs/h/h7451.md) that ['Elyāšîḇ](../../strongs/h/h475.md) ['asah](../../strongs/h/h6213.md) for [Ṭôḇîyâ](../../strongs/h/h2900.md), in ['asah](../../strongs/h/h6213.md) him a [niškâ](../../strongs/h/h5393.md) in the [ḥāṣēr](../../strongs/h/h2691.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="nehemiah_13_8"></a>Nehemiah 13:8

And it [yāraʿ](../../strongs/h/h3415.md) me [me'od](../../strongs/h/h3966.md): therefore I [shalak](../../strongs/h/h7993.md) all the [bayith](../../strongs/h/h1004.md) [kĕliy](../../strongs/h/h3627.md) of [Ṭôḇîyâ](../../strongs/h/h2900.md) out [ḥûṣ](../../strongs/h/h2351.md) the [liškâ](../../strongs/h/h3957.md).

<a name="nehemiah_13_9"></a>Nehemiah 13:9

Then I ['āmar](../../strongs/h/h559.md), and they [ṭāhēr](../../strongs/h/h2891.md) the [liškâ](../../strongs/h/h3957.md): and thither brought I [shuwb](../../strongs/h/h7725.md) the [kĕliy](../../strongs/h/h3627.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), with the meat [minchah](../../strongs/h/h4503.md) and the [lᵊḇônâ](../../strongs/h/h3828.md).

<a name="nehemiah_13_10"></a>Nehemiah 13:10

And I [yada'](../../strongs/h/h3045.md) that the [mᵊnāṯ](../../strongs/h/h4521.md) of the [Lᵊvî](../../strongs/h/h3881.md) had not been [nathan](../../strongs/h/h5414.md) them: for the [Lᵊvî](../../strongs/h/h3881.md) and the [shiyr](../../strongs/h/h7891.md), that ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md), were [bāraḥ](../../strongs/h/h1272.md) every ['iysh](../../strongs/h/h376.md) to his [sadeh](../../strongs/h/h7704.md).

<a name="nehemiah_13_11"></a>Nehemiah 13:11

Then [riyb](../../strongs/h/h7378.md) I with the [sāḡān](../../strongs/h/h5461.md), and ['āmar](../../strongs/h/h559.md), Why is the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) ['azab](../../strongs/h/h5800.md)? And I gathered them [qāḇaṣ](../../strongs/h/h6908.md), and ['amad](../../strongs/h/h5975.md) them in their [ʿōmeḏ](../../strongs/h/h5977.md).

<a name="nehemiah_13_12"></a>Nehemiah 13:12

Then [bow'](../../strongs/h/h935.md) all [Yehuwdah](../../strongs/h/h3063.md) the [maʿăśēr](../../strongs/h/h4643.md) of the [dagan](../../strongs/h/h1715.md) and the [tiyrowsh](../../strongs/h/h8492.md) and the [yiṣhār](../../strongs/h/h3323.md) unto the ['ôṣār](../../strongs/h/h214.md).

<a name="nehemiah_13_13"></a>Nehemiah 13:13

And I made ['āṣar](../../strongs/h/h686.md) over the ['ôṣār](../../strongs/h/h214.md), [Šelemyâ](../../strongs/h/h8018.md) the [kōhēn](../../strongs/h/h3548.md), and [Ṣāḏôq](../../strongs/h/h6659.md) the [sāp̄ar](../../strongs/h/h5608.md), and of the [Lᵊvî](../../strongs/h/h3881.md), [Pᵊḏāyâ](../../strongs/h/h6305.md): and next to [yad](../../strongs/h/h3027.md) was [Ḥānān](../../strongs/h/h2605.md) the [ben](../../strongs/h/h1121.md) of [Zakûr](../../strongs/h/h2139.md), the [ben](../../strongs/h/h1121.md) of [Matanyâ](../../strongs/h/h4983.md): for they were [chashab](../../strongs/h/h2803.md) ['aman](../../strongs/h/h539.md), and their office was to [chalaq](../../strongs/h/h2505.md) unto their ['ach](../../strongs/h/h251.md).

<a name="nehemiah_13_14"></a>Nehemiah 13:14

[zakar](../../strongs/h/h2142.md) me, O my ['Elohiym](../../strongs/h/h430.md), concerning this, and wipe not [māḥâ](../../strongs/h/h4229.md) my [checed](../../strongs/h/h2617.md) deeds that I have ['asah](../../strongs/h/h6213.md) for the [bayith](../../strongs/h/h1004.md) of my ['Elohiym](../../strongs/h/h430.md), and for the [mišmār](../../strongs/h/h4929.md) thereof.

<a name="nehemiah_13_15"></a>Nehemiah 13:15

In those [yowm](../../strongs/h/h3117.md) [ra'ah](../../strongs/h/h7200.md) I in [Yehuwdah](../../strongs/h/h3063.md) some [dāraḵ](../../strongs/h/h1869.md) wine [gaṯ](../../strongs/h/h1660.md) on the [shabbath](../../strongs/h/h7676.md), and bringing [bow'](../../strongs/h/h935.md) [ʿărēmâ](../../strongs/h/h6194.md), and [ʿāmas](../../strongs/h/h6006.md) [chamowr](../../strongs/h/h2543.md); as also [yayin](../../strongs/h/h3196.md), [ʿēnāḇ](../../strongs/h/h6025.md), and [tĕ'en](../../strongs/h/h8384.md), and all manner of [maśśā'](../../strongs/h/h4853.md), which they [bow'](../../strongs/h/h935.md) into [Yĕruwshalaim](../../strongs/h/h3389.md) on the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md): and I [ʿûḏ](../../strongs/h/h5749.md) against them in the [yowm](../../strongs/h/h3117.md) wherein they [māḵar](../../strongs/h/h4376.md) [ṣayiḏ](../../strongs/h/h6718.md).

<a name="nehemiah_13_16"></a>Nehemiah 13:16

There [yashab](../../strongs/h/h3427.md) men of [ṣōrî](../../strongs/h/h6876.md) also therein, which [bow'](../../strongs/h/h935.md) [dag](../../strongs/h/h1709.md) [dag](../../strongs/h/h1709.md), and all manner of [meḵer](../../strongs/h/h4377.md), and [māḵar](../../strongs/h/h4376.md) on the [shabbath](../../strongs/h/h7676.md) unto the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md), and in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="nehemiah_13_17"></a>Nehemiah 13:17

Then I [riyb](../../strongs/h/h7378.md) with the [ḥōr](../../strongs/h/h2715.md) of [Yehuwdah](../../strongs/h/h3063.md), and ['āmar](../../strongs/h/h559.md) unto them, What [ra'](../../strongs/h/h7451.md) [dabar](../../strongs/h/h1697.md) is this that ye ['asah](../../strongs/h/h6213.md), and [ḥālal](../../strongs/h/h2490.md) the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md)?

<a name="nehemiah_13_18"></a>Nehemiah 13:18

['asah](../../strongs/h/h6213.md) not your ['ab](../../strongs/h/h1.md) thus, and [bow'](../../strongs/h/h935.md) not our ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) all this [ra'](../../strongs/h/h7451.md) upon us, and upon this [ʿîr](../../strongs/h/h5892.md)? yet ye [bow'](../../strongs/h/h935.md) [yāsap̄](../../strongs/h/h3254.md) [charown](../../strongs/h/h2740.md) upon [Yisra'el](../../strongs/h/h3478.md) by [ḥālal](../../strongs/h/h2490.md) the [shabbath](../../strongs/h/h7676.md).

<a name="nehemiah_13_19"></a>Nehemiah 13:19

And it came to pass, that when the [sha'ar](../../strongs/h/h8179.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) began to be [ṣālal](../../strongs/h/h6751.md) [paniym](../../strongs/h/h6440.md) the [shabbath](../../strongs/h/h7676.md), I ['āmar](../../strongs/h/h559.md) that the [deleṯ](../../strongs/h/h1817.md) should be [cagar](../../strongs/h/h5462.md), and ['āmar](../../strongs/h/h559.md) that they should not be [pāṯaḥ](../../strongs/h/h6605.md) till ['aḥar](../../strongs/h/h310.md) the [shabbath](../../strongs/h/h7676.md): and some of my [naʿar](../../strongs/h/h5288.md) ['amad](../../strongs/h/h5975.md) I at the [sha'ar](../../strongs/h/h8179.md), that there should no [maśśā'](../../strongs/h/h4853.md) be brought [bow'](../../strongs/h/h935.md) on the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md).

<a name="nehemiah_13_20"></a>Nehemiah 13:20

So the [rāḵal](../../strongs/h/h7402.md) and [māḵar](../../strongs/h/h4376.md) of all kind of [mimkār](../../strongs/h/h4465.md) [lûn](../../strongs/h/h3885.md) [ḥûṣ](../../strongs/h/h2351.md) [Yĕruwshalaim](../../strongs/h/h3389.md) [pa'am](../../strongs/h/h6471.md) or [šᵊnayim](../../strongs/h/h8147.md).

<a name="nehemiah_13_21"></a>Nehemiah 13:21

Then I [ʿûḏ](../../strongs/h/h5749.md) against them, and ['āmar](../../strongs/h/h559.md) unto them, Why [lûn](../../strongs/h/h3885.md) ye [neḡeḏ](../../strongs/h/h5048.md) the [ḥômâ](../../strongs/h/h2346.md)? if ye do so [šānâ](../../strongs/h/h8138.md), I will [shalach](../../strongs/h/h7971.md) [yad](../../strongs/h/h3027.md) on you. From that [ʿēṯ](../../strongs/h/h6256.md) forth [bow'](../../strongs/h/h935.md) they no more on the [shabbath](../../strongs/h/h7676.md).

<a name="nehemiah_13_22"></a>Nehemiah 13:22

And I ['āmar](../../strongs/h/h559.md) the [Lᵊvî](../../strongs/h/h3881.md) that they should [ṭāhēr](../../strongs/h/h2891.md) themselves, and that they should [bow'](../../strongs/h/h935.md) and [shamar](../../strongs/h/h8104.md) the [sha'ar](../../strongs/h/h8179.md), to [qadash](../../strongs/h/h6942.md) the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md). [zakar](../../strongs/h/h2142.md) me, O my ['Elohiym](../../strongs/h/h430.md), concerning this also, and [ḥûs](../../strongs/h/h2347.md) me according to the [rōḇ](../../strongs/h/h7230.md) of thy [checed](../../strongs/h/h2617.md).

<a name="nehemiah_13_23"></a>Nehemiah 13:23

In those [yowm](../../strongs/h/h3117.md) also [ra'ah](../../strongs/h/h7200.md) I [Yᵊhûḏî](../../strongs/h/h3064.md) that had [yashab](../../strongs/h/h3427.md) ['ishshah](../../strongs/h/h802.md) of ['ašdôḏî](../../strongs/h/h796.md), of [ʿAmmôn](../../strongs/h/h5984.md), and of [Mô'āḇî](../../strongs/h/h4125.md):

<a name="nehemiah_13_24"></a>Nehemiah 13:24

And their [ben](../../strongs/h/h1121.md) [dabar](../../strongs/h/h1696.md) [ḥēṣî](../../strongs/h/h2677.md) in the speech of Ashdod H797, and [nāḵar](../../strongs/h/h5234.md) not [dabar](../../strongs/h/h1696.md) in the Jews' [yᵊhûḏîṯ](../../strongs/h/h3066.md), but according to the [lashown](../../strongs/h/h3956.md) of ['am](../../strongs/h/h5971.md) ['am](../../strongs/h/h5971.md).

<a name="nehemiah_13_25"></a>Nehemiah 13:25

And I [riyb](../../strongs/h/h7378.md) with them, and [qālal](../../strongs/h/h7043.md) them, and [nakah](../../strongs/h/h5221.md) ['enowsh](../../strongs/h/h582.md) of them, and plucked off their [māraṭ](../../strongs/h/h4803.md), and made them [shaba'](../../strongs/h/h7650.md) by ['Elohiym](../../strongs/h/h430.md), saying, Ye shall not [nathan](../../strongs/h/h5414.md) your [bath](../../strongs/h/h1323.md) unto their [ben](../../strongs/h/h1121.md), nor [nasa'](../../strongs/h/h5375.md) their [bath](../../strongs/h/h1323.md) unto your [ben](../../strongs/h/h1121.md), or for yourselves.

<a name="nehemiah_13_26"></a>Nehemiah 13:26

Did not [Šᵊlōmô](../../strongs/h/h8010.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [chata'](../../strongs/h/h2398.md) by these things? yet among [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md) was there no [melek](../../strongs/h/h4428.md) like him, who was ['ahab](../../strongs/h/h157.md) of his ['Elohiym](../../strongs/h/h430.md), and ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) him [melek](../../strongs/h/h4428.md) over all [Yisra'el](../../strongs/h/h3478.md): nevertheless even [gam](../../strongs/h/h1571.md) did [nāḵrî](../../strongs/h/h5237.md) ['ishshah](../../strongs/h/h802.md) cause to [chata'](../../strongs/h/h2398.md).

<a name="nehemiah_13_27"></a>Nehemiah 13:27

Shall we then [shama'](../../strongs/h/h8085.md) unto you to ['asah](../../strongs/h/h6213.md) all this [gadowl](../../strongs/h/h1419.md) [ra'](../../strongs/h/h7451.md), to [māʿal](../../strongs/h/h4603.md) against our ['Elohiym](../../strongs/h/h430.md) in [yashab](../../strongs/h/h3427.md) [nāḵrî](../../strongs/h/h5237.md) ['ishshah](../../strongs/h/h802.md)?

<a name="nehemiah_13_28"></a>Nehemiah 13:28

And one of the [ben](../../strongs/h/h1121.md) of [Yôyāḏāʿ](../../strongs/h/h3111.md), the [ben](../../strongs/h/h1121.md) of ['Elyāšîḇ](../../strongs/h/h475.md) the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md), was son in [ḥāṯān](../../strongs/h/h2860.md) to [Sanḇallāṭ](../../strongs/h/h5571.md) the [ḥōrōnî](../../strongs/h/h2772.md): therefore I [bāraḥ](../../strongs/h/h1272.md) him from me.

<a name="nehemiah_13_29"></a>Nehemiah 13:29

[zakar](../../strongs/h/h2142.md) them, O my ['Elohiym](../../strongs/h/h430.md), because they have defiled H1352 the [kᵊhunnâ](../../strongs/h/h3550.md), and the [bĕriyth](../../strongs/h/h1285.md) of the [kᵊhunnâ](../../strongs/h/h3550.md), and of the [Lᵊvî](../../strongs/h/h3881.md).

<a name="nehemiah_13_30"></a>Nehemiah 13:30

Thus [ṭāhēr](../../strongs/h/h2891.md) I them from all [nēḵār](../../strongs/h/h5236.md), and ['amad](../../strongs/h/h5975.md) the [mišmereṯ](../../strongs/h/h4931.md) of the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md), every ['iysh](../../strongs/h/h376.md) in his [mĕla'kah](../../strongs/h/h4399.md);

<a name="nehemiah_13_31"></a>Nehemiah 13:31

And for the ['ets](../../strongs/h/h6086.md) [qorban](../../strongs/h/h7133.md), at [ʿēṯ](../../strongs/h/h6256.md) [zāman](../../strongs/h/h2163.md), and for the [bikûr](../../strongs/h/h1061.md). [zakar](../../strongs/h/h2142.md) me, O my ['Elohiym](../../strongs/h/h430.md), for [towb](../../strongs/h/h2896.md).

---

[Transliteral Bible](../bible.md)

[Nehemiah](nehemiah.md)

[Nehemiah 12](nehemiah_12.md) - [Nehemiah 14](nehemiah_14.md)