# [Nehemiah 1](https://www.blueletterbible.org/kjv/nehemiah/1)

<a name="nehemiah_1_1"></a>Nehemiah 1:1

The [dabar](../../strongs/h/h1697.md) of [Nᵊḥemyâ](../../strongs/h/h5166.md) the [ben](../../strongs/h/h1121.md) of [Ḥăḵalyâ](../../strongs/h/h2446.md). And it came to pass in the [ḥōḏeš](../../strongs/h/h2320.md) [Kislēv](../../strongs/h/h3691.md), in the [ʿeśrîm](../../strongs/h/h6242.md) [šānâ](../../strongs/h/h8141.md), as I was in [Šûšan](../../strongs/h/h7800.md) the [bîrâ](../../strongs/h/h1002.md),

<a name="nehemiah_1_2"></a>Nehemiah 1:2

That [Ḥănānî](../../strongs/h/h2607.md), ['echad](../../strongs/h/h259.md) of my ['ach](../../strongs/h/h251.md), [bow'](../../strongs/h/h935.md), he and certain ['enowsh](../../strongs/h/h582.md) of [Yehuwdah](../../strongs/h/h3063.md); and I [sha'al](../../strongs/h/h7592.md) them concerning the [Yᵊhûḏî](../../strongs/h/h3064.md) that had [pᵊlêṭâ](../../strongs/h/h6413.md), which were [šā'ar](../../strongs/h/h7604.md) of the [šᵊḇî](../../strongs/h/h7628.md), and concerning [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="nehemiah_1_3"></a>Nehemiah 1:3

And they ['āmar](../../strongs/h/h559.md) unto me, The [šā'ar](../../strongs/h/h7604.md) that are [šā'ar](../../strongs/h/h7604.md) of the [šᵊḇî](../../strongs/h/h7628.md) there in the [mᵊḏînâ](../../strongs/h/h4082.md) are in [gadowl](../../strongs/h/h1419.md) [ra'](../../strongs/h/h7451.md) and [cherpah](../../strongs/h/h2781.md): the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) also is broken [pāraṣ](../../strongs/h/h6555.md), and the [sha'ar](../../strongs/h/h8179.md) thereof are [yāṣaṯ](../../strongs/h/h3341.md) with ['esh](../../strongs/h/h784.md).

<a name="nehemiah_1_4"></a>Nehemiah 1:4

And it came to pass, when I [shama'](../../strongs/h/h8085.md) these [dabar](../../strongs/h/h1697.md), that I [yashab](../../strongs/h/h3427.md) and [bāḵâ](../../strongs/h/h1058.md), and ['āḇal](../../strongs/h/h56.md) certain [yowm](../../strongs/h/h3117.md), and [ṣûm](../../strongs/h/h6684.md), and [palal](../../strongs/h/h6419.md) [paniym](../../strongs/h/h6440.md) the ['Elohiym](../../strongs/h/h430.md) of [shamayim](../../strongs/h/h8064.md),

<a name="nehemiah_1_5"></a>Nehemiah 1:5

And ['āmar](../../strongs/h/h559.md), I ['ānnā'](../../strongs/h/h577.md) thee, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [shamayim](../../strongs/h/h8064.md), the [gadowl](../../strongs/h/h1419.md) and [yare'](../../strongs/h/h3372.md) ['el](../../strongs/h/h410.md), that [shamar](../../strongs/h/h8104.md) [bĕriyth](../../strongs/h/h1285.md) and [checed](../../strongs/h/h2617.md) for them that ['ahab](../../strongs/h/h157.md) him and [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md):

<a name="nehemiah_1_6"></a>Nehemiah 1:6

Let thine ['ozen](../../strongs/h/h241.md) now be [qaššāḇ](../../strongs/h/h7183.md), and thine ['ayin](../../strongs/h/h5869.md) [pāṯaḥ](../../strongs/h/h6605.md), that thou mayest [shama'](../../strongs/h/h8085.md) the [tĕphillah](../../strongs/h/h8605.md) of thy ['ebed](../../strongs/h/h5650.md), which I [palal](../../strongs/h/h6419.md) [paniym](../../strongs/h/h6440.md) thee [yowm](../../strongs/h/h3117.md), [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md), for the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) thy ['ebed](../../strongs/h/h5650.md), and [yadah](../../strongs/h/h3034.md) the [chatta'ath](../../strongs/h/h2403.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which we have [chata'](../../strongs/h/h2398.md) against thee: both I and my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) have [chata'](../../strongs/h/h2398.md).

<a name="nehemiah_1_7"></a>Nehemiah 1:7

We have dealt [chabal](../../strongs/h/h2254.md) [chabal](../../strongs/h/h2254.md) against thee, and have not [shamar](../../strongs/h/h8104.md) the [mitsvah](../../strongs/h/h4687.md), nor the [choq](../../strongs/h/h2706.md), nor the [mishpat](../../strongs/h/h4941.md), which thou [tsavah](../../strongs/h/h6680.md) thy ['ebed](../../strongs/h/h5650.md) [Mōshe](../../strongs/h/h4872.md).

<a name="nehemiah_1_8"></a>Nehemiah 1:8

[zakar](../../strongs/h/h2142.md), I beseech thee, the [dabar](../../strongs/h/h1697.md) that thou [tsavah](../../strongs/h/h6680.md) thy ['ebed](../../strongs/h/h5650.md) [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md), If ye [māʿal](../../strongs/h/h4603.md), I will scatter you [puwts](../../strongs/h/h6327.md) among the ['am](../../strongs/h/h5971.md):

<a name="nehemiah_1_9"></a>Nehemiah 1:9

But if ye [shuwb](../../strongs/h/h7725.md) unto me, and [shamar](../../strongs/h/h8104.md) my [mitsvah](../../strongs/h/h4687.md), and ['asah](../../strongs/h/h6213.md) them; though there were of you cast [nāḏaḥ](../../strongs/h/h5080.md) unto the uttermost [qāṣê](../../strongs/h/h7097.md) of the [shamayim](../../strongs/h/h8064.md), yet will I [qāḇaṣ](../../strongs/h/h6908.md) them from thence, and will [bow'](../../strongs/h/h935.md) them unto the [maqowm](../../strongs/h/h4725.md) that I have [bāḥar](../../strongs/h/h977.md) to [shakan](../../strongs/h/h7931.md) my [shem](../../strongs/h/h8034.md) there.

<a name="nehemiah_1_10"></a>Nehemiah 1:10

Now these are thy ['ebed](../../strongs/h/h5650.md) and thy ['am](../../strongs/h/h5971.md), whom thou hast [pāḏâ](../../strongs/h/h6299.md) by thy [gadowl](../../strongs/h/h1419.md) [koach](../../strongs/h/h3581.md), and by thy [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md).

<a name="nehemiah_1_11"></a>Nehemiah 1:11

['Adonay](../../strongs/h/h136.md), I ['ānnā'](../../strongs/h/h577.md) thee, let now thine ['ozen](../../strongs/h/h241.md) be [qaššāḇ](../../strongs/h/h7183.md) to the [tĕphillah](../../strongs/h/h8605.md) of thy ['ebed](../../strongs/h/h5650.md), and to the [tĕphillah](../../strongs/h/h8605.md) of thy ['ebed](../../strongs/h/h5650.md), who [chaphets](../../strongs/h/h2655.md) to [yare'](../../strongs/h/h3372.md) thy [shem](../../strongs/h/h8034.md): and [tsalach](../../strongs/h/h6743.md), I pray thee, thy ['ebed](../../strongs/h/h5650.md) this [yowm](../../strongs/h/h3117.md), and [nathan](../../strongs/h/h5414.md) him [raḥam](../../strongs/h/h7356.md) in the [paniym](../../strongs/h/h6440.md) of this ['iysh](../../strongs/h/h376.md). For I was the [melek](../../strongs/h/h4428.md) [šāqâ](../../strongs/h/h8248.md).

---

[Transliteral Bible](../bible.md)

[Nehemiah](nehemiah.md)

[Nehemiah 2](nehemiah_2.md)