# [Nehemiah 6](https://www.blueletterbible.org/kjv/nehemiah/6)

<a name="nehemiah_6_1"></a>Nehemiah 6:1

Now it came to pass, when [Sanḇallāṭ](../../strongs/h/h5571.md), and [Ṭôḇîyâ](../../strongs/h/h2900.md), and [Gešem](../../strongs/h/h1654.md) the [ʿĂrāḇî](../../strongs/h/h6163.md), and the [yeṯer](../../strongs/h/h3499.md) of our ['oyeb](../../strongs/h/h341.md), [shama'](../../strongs/h/h8085.md) that I had [bānâ](../../strongs/h/h1129.md) the [ḥômâ](../../strongs/h/h2346.md), and that there was no [pereṣ](../../strongs/h/h6556.md) [yāṯar](../../strongs/h/h3498.md) therein; (though [gam](../../strongs/h/h1571.md) [ʿaḏ](../../strongs/h/h5704.md) at that [ʿēṯ](../../strongs/h/h6256.md) I had not set ['amad](../../strongs/h/h5975.md) the [deleṯ](../../strongs/h/h1817.md) upon the [sha'ar](../../strongs/h/h8179.md);)

<a name="nehemiah_6_2"></a>Nehemiah 6:2

That [Sanḇallāṭ](../../strongs/h/h5571.md) and [Gešem](../../strongs/h/h1654.md) [shalach](../../strongs/h/h7971.md) unto me, ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), let us [yāʿaḏ](../../strongs/h/h3259.md) [yaḥaḏ](../../strongs/h/h3162.md) in some one of the [kephiyr](../../strongs/h/h3715.md) in the [biqʿâ](../../strongs/h/h1237.md) of ['Ônô](../../strongs/h/h207.md). But they [chashab](../../strongs/h/h2803.md) to ['asah](../../strongs/h/h6213.md) me [ra'](../../strongs/h/h7451.md).

<a name="nehemiah_6_3"></a>Nehemiah 6:3

And I [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) unto them, ['āmar](../../strongs/h/h559.md), I am ['asah](../../strongs/h/h6213.md) a [gadowl](../../strongs/h/h1419.md) [mĕla'kah](../../strongs/h/h4399.md), so that I [yakol](../../strongs/h/h3201.md) [yarad](../../strongs/h/h3381.md): why should the [mĕla'kah](../../strongs/h/h4399.md) [shabath](../../strongs/h/h7673.md), ['ăšer](../../strongs/h/h834.md) I [rāp̄â](../../strongs/h/h7503.md) it, and [yarad](../../strongs/h/h3381.md) to you?

<a name="nehemiah_6_4"></a>Nehemiah 6:4

Yet they [shalach](../../strongs/h/h7971.md) unto me ['arbaʿ](../../strongs/h/h702.md) [pa'am](../../strongs/h/h6471.md) after this [dabar](../../strongs/h/h1697.md); and I [shuwb](../../strongs/h/h7725.md) them after the same [dabar](../../strongs/h/h1697.md).

<a name="nehemiah_6_5"></a>Nehemiah 6:5

Then [shalach](../../strongs/h/h7971.md) [Sanḇallāṭ](../../strongs/h/h5571.md) his [naʿar](../../strongs/h/h5288.md) unto me in like [dabar](../../strongs/h/h1697.md) the [ḥămîšî](../../strongs/h/h2549.md) [pa'am](../../strongs/h/h6471.md) with a [pāṯaḥ](../../strongs/h/h6605.md) ['agereṯ](../../strongs/h/h107.md) in his [yad](../../strongs/h/h3027.md);

<a name="nehemiah_6_6"></a>Nehemiah 6:6

Wherein was [kāṯaḇ](../../strongs/h/h3789.md), It is [shama'](../../strongs/h/h8085.md) among the [gowy](../../strongs/h/h1471.md), and [Gešem](../../strongs/h/h1654.md) ['āmar](../../strongs/h/h559.md) it, that thou and the [Yᵊhûḏî](../../strongs/h/h3064.md) [chashab](../../strongs/h/h2803.md) to [māraḏ](../../strongs/h/h4775.md): for which [kēn](../../strongs/h/h3651.md) thou [bānâ](../../strongs/h/h1129.md) the [ḥômâ](../../strongs/h/h2346.md), that thou mayest [hāvâ](../../strongs/h/h1933.md) their [melek](../../strongs/h/h4428.md), according to these [dabar](../../strongs/h/h1697.md).

<a name="nehemiah_6_7"></a>Nehemiah 6:7

And thou hast also ['amad](../../strongs/h/h5975.md) [nāḇî'](../../strongs/h/h5030.md) to [qara'](../../strongs/h/h7121.md) of thee at [Yĕruwshalaim](../../strongs/h/h3389.md), ['āmar](../../strongs/h/h559.md), There is a [melek](../../strongs/h/h4428.md) in [Yehuwdah](../../strongs/h/h3063.md): and now shall it be [shama'](../../strongs/h/h8085.md) to the [melek](../../strongs/h/h4428.md) according to these [dabar](../../strongs/h/h1697.md). [yālaḵ](../../strongs/h/h3212.md) now therefore, and let us take [ya'ats](../../strongs/h/h3289.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="nehemiah_6_8"></a>Nehemiah 6:8

Then I [shalach](../../strongs/h/h7971.md) unto him, ['āmar](../../strongs/h/h559.md), There [hayah](../../strongs/h/h1961.md) no such [dabar](../../strongs/h/h1697.md) [hayah](../../strongs/h/h1961.md) as thou ['āmar](../../strongs/h/h559.md), but thou [bāḏā'](../../strongs/h/h908.md) them out of thine own [leb](../../strongs/h/h3820.md).

<a name="nehemiah_6_9"></a>Nehemiah 6:9

For they all made us [yare'](../../strongs/h/h3372.md), ['āmar](../../strongs/h/h559.md), Their [yad](../../strongs/h/h3027.md) shall be [rāp̄â](../../strongs/h/h7503.md) from the [mĕla'kah](../../strongs/h/h4399.md), that it be not ['asah](../../strongs/h/h6213.md). Now therefore, O God, [ḥāzaq](../../strongs/h/h2388.md) my [yad](../../strongs/h/h3027.md).

<a name="nehemiah_6_10"></a>Nehemiah 6:10

Afterward I [bow'](../../strongs/h/h935.md) unto the [bayith](../../strongs/h/h1004.md) of [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the [ben](../../strongs/h/h1121.md) of [Dᵊlāyâ](../../strongs/h/h1806.md) the [ben](../../strongs/h/h1121.md) of [Mᵊhêṭaḇ'ēl](../../strongs/h/h4105.md), who was [ʿāṣar](../../strongs/h/h6113.md); and he ['āmar](../../strongs/h/h559.md), Let us meet [yāʿaḏ](../../strongs/h/h3259.md) in the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), [tavek](../../strongs/h/h8432.md) the [heykal](../../strongs/h/h1964.md), and let us [cagar](../../strongs/h/h5462.md) the [deleṯ](../../strongs/h/h1817.md) of the [heykal](../../strongs/h/h1964.md): for they will [bow'](../../strongs/h/h935.md) to [harag](../../strongs/h/h2026.md) thee; yea, in the [layil](../../strongs/h/h3915.md) will they [bow'](../../strongs/h/h935.md) to [harag](../../strongs/h/h2026.md) thee.

<a name="nehemiah_6_11"></a>Nehemiah 6:11

And I ['āmar](../../strongs/h/h559.md), Should [kᵊmô](../../strongs/h/h3644.md) an ['iysh](../../strongs/h/h376.md) as I [bāraḥ](../../strongs/h/h1272.md)? and who is there, that, being as I am, would [bow'](../../strongs/h/h935.md) into the [heykal](../../strongs/h/h1964.md) to save his [chayay](../../strongs/h/h2425.md)? I will not go [bow'](../../strongs/h/h935.md).

<a name="nehemiah_6_12"></a>Nehemiah 6:12

And, lo, I [nāḵar](../../strongs/h/h5234.md) that ['Elohiym](../../strongs/h/h430.md) had not [shalach](../../strongs/h/h7971.md) him; but that he [dabar](../../strongs/h/h1696.md) this [nᵊḇû'â](../../strongs/h/h5016.md) against me: for [Ṭôḇîyâ](../../strongs/h/h2900.md) and [Sanḇallāṭ](../../strongs/h/h5571.md) had [śāḵar](../../strongs/h/h7936.md) him.

<a name="nehemiah_6_13"></a>Nehemiah 6:13

Therefore was he [śāḵar](../../strongs/h/h7936.md), that I should be [yare'](../../strongs/h/h3372.md), and do ['asah](../../strongs/h/h6213.md), and [chata'](../../strongs/h/h2398.md), and that they might have matter for a [ra'](../../strongs/h/h7451.md) [shem](../../strongs/h/h8034.md), that they might [ḥārap̄](../../strongs/h/h2778.md) me.

<a name="nehemiah_6_14"></a>Nehemiah 6:14

My ['Elohiym](../../strongs/h/h430.md), [zakar](../../strongs/h/h2142.md) thou upon [Ṭôḇîyâ](../../strongs/h/h2900.md) and [Sanḇallāṭ](../../strongs/h/h5571.md) according to these their [ma'aseh](../../strongs/h/h4639.md), and on the [nᵊḇî'â](../../strongs/h/h5031.md) [NôʿAḏyâ](../../strongs/h/h5129.md), and the [yeṯer](../../strongs/h/h3499.md) of the [nāḇî'](../../strongs/h/h5030.md), that would have put me in [yare'](../../strongs/h/h3372.md).

<a name="nehemiah_6_15"></a>Nehemiah 6:15

So the [ḥômâ](../../strongs/h/h2346.md) was [shalam](../../strongs/h/h7999.md) in the [ʿeśrîm](../../strongs/h/h6242.md) and [ḥāmēš](../../strongs/h/h2568.md) day of the month ['ĕlûl](../../strongs/h/h435.md), in [ḥămisheem](../../strongs/h/h2572.md) and [šᵊnayim](../../strongs/h/h8147.md) [yowm](../../strongs/h/h3117.md).

<a name="nehemiah_6_16"></a>Nehemiah 6:16

And it came to pass, that when all our ['oyeb](../../strongs/h/h341.md) [shama'](../../strongs/h/h8085.md) thereof, and all the [gowy](../../strongs/h/h1471.md) that were about [cabiyb](../../strongs/h/h5439.md) [ra'ah](../../strongs/h/h7200.md) these things, they were [me'od](../../strongs/h/h3966.md) [naphal](../../strongs/h/h5307.md) in their own ['ayin](../../strongs/h/h5869.md): for they [yada'](../../strongs/h/h3045.md) that this [mĕla'kah](../../strongs/h/h4399.md) was ['asah](../../strongs/h/h6213.md) of our ['Elohiym](../../strongs/h/h430.md).

<a name="nehemiah_6_17"></a>Nehemiah 6:17

Moreover in those [yowm](../../strongs/h/h3117.md) the [ḥōr](../../strongs/h/h2715.md) of [Yehuwdah](../../strongs/h/h3063.md) [halak](../../strongs/h/h1980.md) [rabah](../../strongs/h/h7235.md) ['agereṯ](../../strongs/h/h107.md) unto [Ṭôḇîyâ](../../strongs/h/h2900.md), and the letters of [Ṭôḇîyâ](../../strongs/h/h2900.md) [bow'](../../strongs/h/h935.md) unto them.

<a name="nehemiah_6_18"></a>Nehemiah 6:18

For there were [rab](../../strongs/h/h7227.md) in [Yehuwdah](../../strongs/h/h3063.md) [baʿal](../../strongs/h/h1167.md) [šᵊḇûʿâ](../../strongs/h/h7621.md) unto him, because he was the son in [ḥāṯān](../../strongs/h/h2860.md) of [Šᵊḵanyâ](../../strongs/h/h7935.md) the [ben](../../strongs/h/h1121.md) of ['Āraḥ](../../strongs/h/h733.md); and his [ben](../../strongs/h/h1121.md) [Yᵊhôḥānān](../../strongs/h/h3076.md) had [laqach](../../strongs/h/h3947.md) the [bath](../../strongs/h/h1323.md) of [Mᵊšullām](../../strongs/h/h4918.md) the [ben](../../strongs/h/h1121.md) of [Bereḵyâ](../../strongs/h/h1296.md).

<a name="nehemiah_6_19"></a>Nehemiah 6:19

Also they ['āmar](../../strongs/h/h559.md) his good [towb](../../strongs/h/h2896.md) [paniym](../../strongs/h/h6440.md) me, and [yāṣā'](../../strongs/h/h3318.md) my [dabar](../../strongs/h/h1697.md) to him. And [Ṭôḇîyâ](../../strongs/h/h2900.md) [shalach](../../strongs/h/h7971.md) ['agereṯ](../../strongs/h/h107.md) to put me in [yare'](../../strongs/h/h3372.md).

---

[Transliteral Bible](../bible.md)

[Nehemiah](nehemiah.md)

[Nehemiah 5](nehemiah_5.md) - [Nehemiah 7](nehemiah_7.md)