# [Nehemiah 5](https://www.blueletterbible.org/kjv/nehemiah/5)

<a name="nehemiah_5_1"></a>Nehemiah 5:1

And there was a [gadowl](../../strongs/h/h1419.md) [tsa'aqah](../../strongs/h/h6818.md) of the ['am](../../strongs/h/h5971.md) and of their ['ishshah](../../strongs/h/h802.md) against their ['ach](../../strongs/h/h251.md) the [Yᵊhûḏî](../../strongs/h/h3064.md).

<a name="nehemiah_5_2"></a>Nehemiah 5:2

For there [yēš](../../strongs/h/h3426.md) that ['āmar](../../strongs/h/h559.md), We, our [ben](../../strongs/h/h1121.md), and our [bath](../../strongs/h/h1323.md), are [rab](../../strongs/h/h7227.md): therefore we take [laqach](../../strongs/h/h3947.md) [dagan](../../strongs/h/h1715.md) for them, that we may ['akal](../../strongs/h/h398.md), and [ḥāyâ](../../strongs/h/h2421.md).

<a name="nehemiah_5_3"></a>Nehemiah 5:3

Some also there [yēš](../../strongs/h/h3426.md) that ['āmar](../../strongs/h/h559.md), We have [ʿāraḇ](../../strongs/h/h6148.md) our [sadeh](../../strongs/h/h7704.md), [kerem](../../strongs/h/h3754.md), and [bayith](../../strongs/h/h1004.md), that we might [laqach](../../strongs/h/h3947.md) [dagan](../../strongs/h/h1715.md), because of the [rāʿāḇ](../../strongs/h/h7458.md).

<a name="nehemiah_5_4"></a>Nehemiah 5:4

There [yēš](../../strongs/h/h3426.md) also that ['āmar](../../strongs/h/h559.md), We have [lāvâ](../../strongs/h/h3867.md) [keceph](../../strongs/h/h3701.md) for the [melek](../../strongs/h/h4428.md) [midâ](../../strongs/h/h4060.md), and that upon our [sadeh](../../strongs/h/h7704.md) and [kerem](../../strongs/h/h3754.md).

<a name="nehemiah_5_5"></a>Nehemiah 5:5

Yet now our [basar](../../strongs/h/h1320.md) is as the [basar](../../strongs/h/h1320.md) of our ['ach](../../strongs/h/h251.md), our [ben](../../strongs/h/h1121.md) as their [ben](../../strongs/h/h1121.md): and, lo, we bring into [kāḇaš](../../strongs/h/h3533.md) our [ben](../../strongs/h/h1121.md) and our [bath](../../strongs/h/h1323.md) to be ['ebed](../../strongs/h/h5650.md), and some of our [bath](../../strongs/h/h1323.md) [yēš](../../strongs/h/h3426.md) brought unto [kāḇaš](../../strongs/h/h3533.md) already: neither is it in our ['el](../../strongs/h/h410.md) [yad](../../strongs/h/h3027.md) to redeem them; for other ['aḥēr](../../strongs/h/h312.md) have our [sadeh](../../strongs/h/h7704.md) and [kerem](../../strongs/h/h3754.md).

<a name="nehemiah_5_6"></a>Nehemiah 5:6

And I was [me'od](../../strongs/h/h3966.md) [ḥārâ](../../strongs/h/h2734.md) when I [shama'](../../strongs/h/h8085.md) their [zaʿaq](../../strongs/h/h2201.md) and these [dabar](../../strongs/h/h1697.md).

<a name="nehemiah_5_7"></a>Nehemiah 5:7

Then I [mālaḵ](../../strongs/h/h4427.md) with [leb](../../strongs/h/h3820.md), and I [riyb](../../strongs/h/h7378.md) the [ḥōr](../../strongs/h/h2715.md), and the [sāḡān](../../strongs/h/h5461.md), and ['āmar](../../strongs/h/h559.md) unto them, Ye [nāšā'](../../strongs/h/h5378.md) [nāšâ](../../strongs/h/h5383.md) [nasa'](../../strongs/h/h5375.md) [maššā'](../../strongs/h/h4855.md), every ['iysh](../../strongs/h/h376.md) of his ['ach](../../strongs/h/h251.md). And I [nathan](../../strongs/h/h5414.md) a [gadowl](../../strongs/h/h1419.md) [qᵊhillâ](../../strongs/h/h6952.md) against them.

<a name="nehemiah_5_8"></a>Nehemiah 5:8

And I ['āmar](../../strongs/h/h559.md) unto them, We after our [day](../../strongs/h/h1767.md) have [qānâ](../../strongs/h/h7069.md) our ['ach](../../strongs/h/h251.md) the [Yᵊhûḏî](../../strongs/h/h3064.md), which were [māḵar](../../strongs/h/h4376.md) unto the [gowy](../../strongs/h/h1471.md); and will ye even [māḵar](../../strongs/h/h4376.md) your ['ach](../../strongs/h/h251.md)? or shall they be [māḵar](../../strongs/h/h4376.md) unto us? Then held they their [ḥāraš](../../strongs/h/h2790.md), and [māṣā'](../../strongs/h/h4672.md) [dabar](../../strongs/h/h1697.md) to answer.

<a name="nehemiah_5_9"></a>Nehemiah 5:9

Also I ['āmar](../../strongs/h/h559.md), It is not [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md) ye ['asah](../../strongs/h/h6213.md): ought ye not to [yālaḵ](../../strongs/h/h3212.md) in the [yir'ah](../../strongs/h/h3374.md) of our ['Elohiym](../../strongs/h/h430.md) because of the [cherpah](../../strongs/h/h2781.md) of the [gowy](../../strongs/h/h1471.md) our ['oyeb](../../strongs/h/h341.md)?

<a name="nehemiah_5_10"></a>Nehemiah 5:10

I likewise, and my ['ach](../../strongs/h/h251.md), and my [naʿar](../../strongs/h/h5288.md), might [nāšâ](../../strongs/h/h5383.md) of them [keceph](../../strongs/h/h3701.md) and [dagan](../../strongs/h/h1715.md): I pray you, let us leave ['azab](../../strongs/h/h5800.md) this [maššā'](../../strongs/h/h4855.md).

<a name="nehemiah_5_11"></a>Nehemiah 5:11

[shuwb](../../strongs/h/h7725.md), I pray you, to them, even this [yowm](../../strongs/h/h3117.md), their [sadeh](../../strongs/h/h7704.md), their [kerem](../../strongs/h/h3754.md), their [zayiṯ](../../strongs/h/h2132.md), and their [bayith](../../strongs/h/h1004.md), also the [mē'â](../../strongs/h/h3967.md) part of the [keceph](../../strongs/h/h3701.md), and of the [dagan](../../strongs/h/h1715.md), the [tiyrowsh](../../strongs/h/h8492.md), and the [yiṣhār](../../strongs/h/h3323.md), that ye [nāšâ](../../strongs/h/h5383.md) of them.

<a name="nehemiah_5_12"></a>Nehemiah 5:12

Then ['āmar](../../strongs/h/h559.md) they, We will [shuwb](../../strongs/h/h7725.md) them, and will [bāqaš](../../strongs/h/h1245.md) nothing of them; so will we ['asah](../../strongs/h/h6213.md) as thou ['āmar](../../strongs/h/h559.md). Then I [qara'](../../strongs/h/h7121.md) the [kōhēn](../../strongs/h/h3548.md), and took a [shaba'](../../strongs/h/h7650.md) of them, that they should ['asah](../../strongs/h/h6213.md) according to this [dabar](../../strongs/h/h1697.md).

<a name="nehemiah_5_13"></a>Nehemiah 5:13

Also I [nāʿar](../../strongs/h/h5287.md) my [ḥēṣen](../../strongs/h/h2684.md), and ['āmar](../../strongs/h/h559.md), So ['Elohiym](../../strongs/h/h430.md) shake [nāʿar](../../strongs/h/h5287.md) every ['iysh](../../strongs/h/h376.md) from his [bayith](../../strongs/h/h1004.md), and from his [yᵊḡîaʿ](../../strongs/h/h3018.md), that [quwm](../../strongs/h/h6965.md) not this [dabar](../../strongs/h/h1697.md), even thus be he shaken [nāʿar](../../strongs/h/h5287.md), and [reyq](../../strongs/h/h7386.md). And all the [qāhēl](../../strongs/h/h6951.md) ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md), and [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md). And the ['am](../../strongs/h/h5971.md) ['asah](../../strongs/h/h6213.md) according to this [dabar](../../strongs/h/h1697.md).

<a name="nehemiah_5_14"></a>Nehemiah 5:14

Moreover from the [yowm](../../strongs/h/h3117.md) that I was [tsavah](../../strongs/h/h6680.md) to be their [peḥâ](../../strongs/h/h6346.md) in the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md), from the [ʿeśrîm](../../strongs/h/h6242.md) [šānâ](../../strongs/h/h8141.md) even unto the [šᵊnayim](../../strongs/h/h8147.md) and [šᵊlōšîm](../../strongs/h/h7970.md) [šānâ](../../strongs/h/h8141.md) of ['Artaḥšaštᵊ'](../../strongs/h/h783.md) the [melek](../../strongs/h/h4428.md), that is, [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) [šānâ](../../strongs/h/h8141.md), I and my ['ach](../../strongs/h/h251.md) have not ['akal](../../strongs/h/h398.md) the [lechem](../../strongs/h/h3899.md) of the [peḥâ](../../strongs/h/h6346.md).

<a name="nehemiah_5_15"></a>Nehemiah 5:15

But the [ri'šôn](../../strongs/h/h7223.md) [peḥâ](../../strongs/h/h6346.md) that had been [paniym](../../strongs/h/h6440.md) me were [kabad](../../strongs/h/h3513.md) unto the ['am](../../strongs/h/h5971.md), and had [laqach](../../strongs/h/h3947.md) of them [lechem](../../strongs/h/h3899.md) and [yayin](../../strongs/h/h3196.md), ['aḥar](../../strongs/h/h310.md) ['arbāʿîm](../../strongs/h/h705.md) [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md); yea, [gam](../../strongs/h/h1571.md) their [naʿar](../../strongs/h/h5288.md) bare [šālaṭ](../../strongs/h/h7980.md) over the ['am](../../strongs/h/h5971.md): but so ['asah](../../strongs/h/h6213.md) not I, [paniym](../../strongs/h/h6440.md) of the [yir'ah](../../strongs/h/h3374.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="nehemiah_5_16"></a>Nehemiah 5:16

Yea, also I [ḥāzaq](../../strongs/h/h2388.md) in the [mĕla'kah](../../strongs/h/h4399.md) of this [ḥômâ](../../strongs/h/h2346.md), neither [qānâ](../../strongs/h/h7069.md) we any [sadeh](../../strongs/h/h7704.md): and all my [naʿar](../../strongs/h/h5288.md) were [qāḇaṣ](../../strongs/h/h6908.md) thither unto the [mĕla'kah](../../strongs/h/h4399.md).

<a name="nehemiah_5_17"></a>Nehemiah 5:17

Moreover there were at my [šulḥān](../../strongs/h/h7979.md) a [mē'â](../../strongs/h/h3967.md) and [ḥămisheem](../../strongs/h/h2572.md) ['iysh](../../strongs/h/h376.md) of the [Yᵊhûḏî](../../strongs/h/h3064.md) and [sāḡān](../../strongs/h/h5461.md), beside those that [bow'](../../strongs/h/h935.md) unto us from among the [gowy](../../strongs/h/h1471.md) that are about [cabiyb](../../strongs/h/h5439.md).

<a name="nehemiah_5_18"></a>Nehemiah 5:18

Now that which was ['asah](../../strongs/h/h6213.md) for me [yowm](../../strongs/h/h3117.md) was ['echad](../../strongs/h/h259.md) [showr](../../strongs/h/h7794.md) and [šēš](../../strongs/h/h8337.md) [bārar](../../strongs/h/h1305.md) [tso'n](../../strongs/h/h6629.md); also [tsippowr](../../strongs/h/h6833.md) were ['asah](../../strongs/h/h6213.md) for me, and once in [ʿeśer](../../strongs/h/h6235.md) [yowm](../../strongs/h/h3117.md) [rabah](../../strongs/h/h7235.md) of all sorts of [yayin](../../strongs/h/h3196.md): yet for [ʿim](../../strongs/h/h5973.md) this [bāqaš](../../strongs/h/h1245.md) not I the [lechem](../../strongs/h/h3899.md) of the [peḥâ](../../strongs/h/h6346.md), because the [ʿăḇōḏâ](../../strongs/h/h5656.md) was [kabad](../../strongs/h/h3513.md) upon this ['am](../../strongs/h/h5971.md).

<a name="nehemiah_5_19"></a>Nehemiah 5:19

[zakar](../../strongs/h/h2142.md) upon me, my ['Elohiym](../../strongs/h/h430.md), for [towb](../../strongs/h/h2896.md), according to all that I have ['asah](../../strongs/h/h6213.md) for this ['am](../../strongs/h/h5971.md).

---

[Transliteral Bible](../bible.md)

[Nehemiah](nehemiah.md)

[Nehemiah 4](nehemiah_4.md) - [Nehemiah 6](nehemiah_6.md)