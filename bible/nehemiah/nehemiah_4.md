# [Nehemiah 4](https://www.blueletterbible.org/kjv/nehemiah/4)

<a name="nehemiah_4_1"></a>Nehemiah 4:1

But it came to pass, that when [Sanḇallāṭ](../../strongs/h/h5571.md) [shama'](../../strongs/h/h8085.md) that we [bānâ](../../strongs/h/h1129.md) the [ḥômâ](../../strongs/h/h2346.md), he was [ḥārâ](../../strongs/h/h2734.md), and took [rabah](../../strongs/h/h7235.md) [kāʿas](../../strongs/h/h3707.md), and [lāʿaḡ](../../strongs/h/h3932.md) the [Yᵊhûḏî](../../strongs/h/h3064.md).

<a name="nehemiah_4_2"></a>Nehemiah 4:2

And he ['āmar](../../strongs/h/h559.md) [paniym](../../strongs/h/h6440.md) his ['ach](../../strongs/h/h251.md) and the [ḥayil](../../strongs/h/h2428.md) of [Šōmrôn](../../strongs/h/h8111.md), and ['āmar](../../strongs/h/h559.md), What ['asah](../../strongs/h/h6213.md) these ['ămēlāl](../../strongs/h/h537.md) [Yᵊhûḏî](../../strongs/h/h3064.md)? will they ['azab](../../strongs/h/h5800.md) themselves? will they [zabach](../../strongs/h/h2076.md)? will they make a [kalah](../../strongs/h/h3615.md) in a [yowm](../../strongs/h/h3117.md)? will they [ḥāyâ](../../strongs/h/h2421.md) the ['eben](../../strongs/h/h68.md) out of the [ʿărēmâ](../../strongs/h/h6194.md) of the ['aphar](../../strongs/h/h6083.md) which are [śārap̄](../../strongs/h/h8313.md)?

<a name="nehemiah_4_3"></a>Nehemiah 4:3

Now [Ṭôḇîyâ](../../strongs/h/h2900.md) the [ʿAmmôn](../../strongs/h/h5984.md) was by ['ēṣel](../../strongs/h/h681.md), and he ['āmar](../../strongs/h/h559.md), Even that which they [bānâ](../../strongs/h/h1129.md), if a [šûʿāl](../../strongs/h/h7776.md) [ʿālâ](../../strongs/h/h5927.md), he shall even break [pāraṣ](../../strongs/h/h6555.md) their ['eben](../../strongs/h/h68.md) [ḥômâ](../../strongs/h/h2346.md).

<a name="nehemiah_4_4"></a>Nehemiah 4:4

[shama'](../../strongs/h/h8085.md), O our ['Elohiym](../../strongs/h/h430.md); for we are [bûzâ](../../strongs/h/h939.md): and [shuwb](../../strongs/h/h7725.md) their [cherpah](../../strongs/h/h2781.md) upon their own [ro'sh](../../strongs/h/h7218.md), and [nathan](../../strongs/h/h5414.md) them for a [bizzâ](../../strongs/h/h961.md) in the ['erets](../../strongs/h/h776.md) of [šiḇyâ](../../strongs/h/h7633.md):

<a name="nehemiah_4_5"></a>Nehemiah 4:5

And [kāsâ](../../strongs/h/h3680.md) not their ['avon](../../strongs/h/h5771.md), and let not their [chatta'ath](../../strongs/h/h2403.md) be blotted [māḥâ](../../strongs/h/h4229.md) from [paniym](../../strongs/h/h6440.md) thee: for they have provoked thee to [kāʿas](../../strongs/h/h3707.md) before the [bānâ](../../strongs/h/h1129.md).

<a name="nehemiah_4_6"></a>Nehemiah 4:6

So [bānâ](../../strongs/h/h1129.md) we the [ḥômâ](../../strongs/h/h2346.md); and all the [ḥômâ](../../strongs/h/h2346.md) was joined [qāšar](../../strongs/h/h7194.md) unto the [ḥēṣî](../../strongs/h/h2677.md) thereof: for the ['am](../../strongs/h/h5971.md) had a [leb](../../strongs/h/h3820.md) to ['asah](../../strongs/h/h6213.md).

<a name="nehemiah_4_7"></a>Nehemiah 4:7

But it came to pass, that when [Sanḇallāṭ](../../strongs/h/h5571.md), and [Ṭôḇîyâ](../../strongs/h/h2900.md), and the [ʿĂrāḇî](../../strongs/h/h6163.md), and the [ʿAmmôn](../../strongs/h/h5984.md), and the ['ašdôḏî](../../strongs/h/h796.md), [shama'](../../strongs/h/h8085.md) that the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) were made ['ărûḵâ](../../strongs/h/h724.md) [ʿālâ](../../strongs/h/h5927.md), and that the [pāraṣ](../../strongs/h/h6555.md) [ḥālal](../../strongs/h/h2490.md) to be [sāṯam](../../strongs/h/h5640.md), then they were [me'od](../../strongs/h/h3966.md) [ḥārâ](../../strongs/h/h2734.md),

<a name="nehemiah_4_8"></a>Nehemiah 4:8

And [qāšar](../../strongs/h/h7194.md) all of them [yaḥaḏ](../../strongs/h/h3162.md) to [bow'](../../strongs/h/h935.md) and to [lāḥam](../../strongs/h/h3898.md) against [Yĕruwshalaim](../../strongs/h/h3389.md), and to ['asah](../../strongs/h/h6213.md) [tôʿâ](../../strongs/h/h8442.md) it.

<a name="nehemiah_4_9"></a>Nehemiah 4:9

Nevertheless we made our [palal](../../strongs/h/h6419.md) unto our ['Elohiym](../../strongs/h/h430.md), and ['amad](../../strongs/h/h5975.md) a [mišmār](../../strongs/h/h4929.md) against them [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md), [paniym](../../strongs/h/h6440.md) of them.

<a name="nehemiah_4_10"></a>Nehemiah 4:10

And [Yehuwdah](../../strongs/h/h3063.md) ['āmar](../../strongs/h/h559.md), The [koach](../../strongs/h/h3581.md) of the bearers of [sabāl](../../strongs/h/h5449.md) is [kashal](../../strongs/h/h3782.md), and there is [rabah](../../strongs/h/h7235.md) ['aphar](../../strongs/h/h6083.md); so that we are not [yakol](../../strongs/h/h3201.md) to [bānâ](../../strongs/h/h1129.md) the [ḥômâ](../../strongs/h/h2346.md).

<a name="nehemiah_4_11"></a>Nehemiah 4:11

And our [tsar](../../strongs/h/h6862.md) ['āmar](../../strongs/h/h559.md), They shall not [yada'](../../strongs/h/h3045.md), neither [ra'ah](../../strongs/h/h7200.md), till we [bow'](../../strongs/h/h935.md) in the [tavek](../../strongs/h/h8432.md) [tavek](../../strongs/h/h8432.md) them, and [harag](../../strongs/h/h2026.md) them, and cause the [mĕla'kah](../../strongs/h/h4399.md) to [shabath](../../strongs/h/h7673.md).

<a name="nehemiah_4_12"></a>Nehemiah 4:12

And it came to pass, that when the [Yᵊhûḏî](../../strongs/h/h3064.md) which [yashab](../../strongs/h/h3427.md) by ['ēṣel](../../strongs/h/h681.md) [bow'](../../strongs/h/h935.md), they ['āmar](../../strongs/h/h559.md) unto us [ʿeśer](../../strongs/h/h6235.md) [pa'am](../../strongs/h/h6471.md), From all [maqowm](../../strongs/h/h4725.md) whence ye shall [shuwb](../../strongs/h/h7725.md) unto us they will be upon you.

<a name="nehemiah_4_13"></a>Nehemiah 4:13

Therefore ['amad](../../strongs/h/h5975.md) I in the [taḥtî](../../strongs/h/h8482.md) [maqowm](../../strongs/h/h4725.md) ['aḥar](../../strongs/h/h310.md) the [ḥômâ](../../strongs/h/h2346.md), and on the higher [ṣᵊḥîaḥ](../../strongs/h/h6706.md) H6708, I even ['amad](../../strongs/h/h5975.md) the ['am](../../strongs/h/h5971.md) after their [mišpāḥâ](../../strongs/h/h4940.md) with their [chereb](../../strongs/h/h2719.md), their [rōmaḥ](../../strongs/h/h7420.md), and their [qesheth](../../strongs/h/h7198.md).

<a name="nehemiah_4_14"></a>Nehemiah 4:14

And I [ra'ah](../../strongs/h/h7200.md), and [quwm](../../strongs/h/h6965.md), and ['āmar](../../strongs/h/h559.md) unto the [ḥōr](../../strongs/h/h2715.md), and to the [sāḡān](../../strongs/h/h5461.md), and to the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md), Be not ye [yare'](../../strongs/h/h3372.md) of [paniym](../../strongs/h/h6440.md): [zakar](../../strongs/h/h2142.md) the ['adonay](../../strongs/h/h136.md), which is [gadowl](../../strongs/h/h1419.md) and [yare'](../../strongs/h/h3372.md), and [lāḥam](../../strongs/h/h3898.md) for your ['ach](../../strongs/h/h251.md), your [ben](../../strongs/h/h1121.md), and your [bath](../../strongs/h/h1323.md), your ['ishshah](../../strongs/h/h802.md), and your [bayith](../../strongs/h/h1004.md).

<a name="nehemiah_4_15"></a>Nehemiah 4:15

And it came to pass, when our ['oyeb](../../strongs/h/h341.md) [shama'](../../strongs/h/h8085.md) that it was [yada'](../../strongs/h/h3045.md) unto us, and ['Elohiym](../../strongs/h/h430.md) had [pārar](../../strongs/h/h6565.md) their ['etsah](../../strongs/h/h6098.md) to [pārar](../../strongs/h/h6565.md), that we [shuwb](../../strongs/h/h7725.md) all of us to the [ḥômâ](../../strongs/h/h2346.md), every ['iysh](../../strongs/h/h376.md) unto his [mĕla'kah](../../strongs/h/h4399.md).

<a name="nehemiah_4_16"></a>Nehemiah 4:16

And it came to pass from that [yowm](../../strongs/h/h3117.md) forth, that the [ḥēṣî](../../strongs/h/h2677.md) of my [naʿar](../../strongs/h/h5288.md) ['asah](../../strongs/h/h6213.md) in the [mĕla'kah](../../strongs/h/h4399.md), and the other [ḥēṣî](../../strongs/h/h2677.md) of them [ḥāzaq](../../strongs/h/h2388.md) both the [rōmaḥ](../../strongs/h/h7420.md), the [magen](../../strongs/h/h4043.md), and the [qesheth](../../strongs/h/h7198.md), and the [širyôn](../../strongs/h/h8302.md); and the [śar](../../strongs/h/h8269.md) were ['aḥar](../../strongs/h/h310.md) all the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="nehemiah_4_17"></a>Nehemiah 4:17

They which [bānâ](../../strongs/h/h1129.md) on the [ḥômâ](../../strongs/h/h2346.md), and they that [nasa'](../../strongs/h/h5375.md) [sēḇel](../../strongs/h/h5447.md), with those that [ʿāmas](../../strongs/h/h6006.md), every one with ['echad](../../strongs/h/h259.md) of his [yad](../../strongs/h/h3027.md) ['asah](../../strongs/h/h6213.md) in the [mĕla'kah](../../strongs/h/h4399.md), and with the ['echad](../../strongs/h/h259.md) hand [ḥāzaq](../../strongs/h/h2388.md) a [Šelaḥ](../../strongs/h/h7973.md).

<a name="nehemiah_4_18"></a>Nehemiah 4:18

For the [bānâ](../../strongs/h/h1129.md), every ['iysh](../../strongs/h/h376.md) had his [chereb](../../strongs/h/h2719.md) ['āsar](../../strongs/h/h631.md) by his [māṯnayim](../../strongs/h/h4975.md), and so [bānâ](../../strongs/h/h1129.md). And he that [tāqaʿ](../../strongs/h/h8628.md) the [šôp̄ār](../../strongs/h/h7782.md) was by ['ēṣel](../../strongs/h/h681.md).

<a name="nehemiah_4_19"></a>Nehemiah 4:19

And I ['āmar](../../strongs/h/h559.md) unto the [ḥōr](../../strongs/h/h2715.md), and to the [sāḡān](../../strongs/h/h5461.md), and to the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md), The [mĕla'kah](../../strongs/h/h4399.md) is [rabah](../../strongs/h/h7235.md) and [rāḥāḇ](../../strongs/h/h7342.md), and we are [pāraḏ](../../strongs/h/h6504.md) upon the [ḥômâ](../../strongs/h/h2346.md), ['iysh](../../strongs/h/h376.md) far [rachowq](../../strongs/h/h7350.md) ['ach](../../strongs/h/h251.md).

<a name="nehemiah_4_20"></a>Nehemiah 4:20

In what [maqowm](../../strongs/h/h4725.md) therefore ye [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md), [qāḇaṣ](../../strongs/h/h6908.md) ye thither unto us: our ['Elohiym](../../strongs/h/h430.md) shall [lāḥam](../../strongs/h/h3898.md) for us.

<a name="nehemiah_4_21"></a>Nehemiah 4:21

So we ['asah](../../strongs/h/h6213.md) in the [mĕla'kah](../../strongs/h/h4399.md): and [ḥēṣî](../../strongs/h/h2677.md) of them [ḥāzaq](../../strongs/h/h2388.md) the [rōmaḥ](../../strongs/h/h7420.md) from the [ʿālâ](../../strongs/h/h5927.md) of the [šaḥar](../../strongs/h/h7837.md) till the [kowkab](../../strongs/h/h3556.md) [yāṣā'](../../strongs/h/h3318.md).

<a name="nehemiah_4_22"></a>Nehemiah 4:22

Likewise at the same [ʿēṯ](../../strongs/h/h6256.md) ['āmar](../../strongs/h/h559.md) I unto the ['am](../../strongs/h/h5971.md), Let every ['iysh](../../strongs/h/h376.md) with his [naʿar](../../strongs/h/h5288.md) [lûn](../../strongs/h/h3885.md) [tavek](../../strongs/h/h8432.md) [Yĕruwshalaim](../../strongs/h/h3389.md), that in the [layil](../../strongs/h/h3915.md) they may be a [mišmār](../../strongs/h/h4929.md) to us, and [mĕla'kah](../../strongs/h/h4399.md) on the [yowm](../../strongs/h/h3117.md).

<a name="nehemiah_4_23"></a>Nehemiah 4:23

So neither I, nor my ['ach](../../strongs/h/h251.md), nor my [naʿar](../../strongs/h/h5288.md), nor the ['enowsh](../../strongs/h/h582.md) of the [mišmār](../../strongs/h/h4929.md) which ['aḥar](../../strongs/h/h310.md) me, none of ['ănaḥnû](../../strongs/h/h587.md) put [pāšaṭ](../../strongs/h/h6584.md) our [beḡeḏ](../../strongs/h/h899.md), saving that every ['iysh](../../strongs/h/h376.md) put them [Šelaḥ](../../strongs/h/h7973.md) for [mayim](../../strongs/h/h4325.md).

---

[Transliteral Bible](../bible.md)

[Nehemiah](nehemiah.md)

[Nehemiah 3](nehemiah_3.md) - [Nehemiah 5](nehemiah_5.md)