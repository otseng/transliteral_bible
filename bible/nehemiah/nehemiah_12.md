# [Nehemiah 12](https://www.blueletterbible.org/kjv/nehemiah/12)

<a name="nehemiah_12_1"></a>Nehemiah 12:1

Now these are the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md) that went [ʿālâ](../../strongs/h/h5927.md) with [Zᵊrubāḇel](../../strongs/h/h2216.md) the [ben](../../strongs/h/h1121.md) of [Šᵊ'Altî'Ēl](../../strongs/h/h7597.md), and [Yēšûaʿ](../../strongs/h/h3442.md): [Śᵊrāyâ](../../strongs/h/h8304.md), [Yirmᵊyâ](../../strongs/h/h3414.md), [ʿEzrā'](../../strongs/h/h5830.md),

<a name="nehemiah_12_2"></a>Nehemiah 12:2

['Ămaryâ](../../strongs/h/h568.md), [Mallûḵ](../../strongs/h/h4409.md), [Ḥaṭṭûš](../../strongs/h/h2407.md),

<a name="nehemiah_12_3"></a>Nehemiah 12:3

[Šᵊḵanyâ](../../strongs/h/h7935.md), [Rᵊḥûm](../../strongs/h/h7348.md), [Mᵊrēmôṯ](../../strongs/h/h4822.md),

<a name="nehemiah_12_4"></a>Nehemiah 12:4

[ʿIdô](../../strongs/h/h5714.md), [Ginnᵊṯôn](../../strongs/h/h1599.md), ['Ăḇîâ](../../strongs/h/h29.md),

<a name="nehemiah_12_5"></a>Nehemiah 12:5

[Minyāmîn](../../strongs/h/h4326.md), [MaʿAḏyâ](../../strongs/h/h4573.md), [Bilgâ](../../strongs/h/h1083.md),

<a name="nehemiah_12_6"></a>Nehemiah 12:6

[ŠᵊmaʿYâ](../../strongs/h/h8098.md), and [Yôyārîḇ](../../strongs/h/h3114.md), [YᵊḏaʿYâ](../../strongs/h/h3048.md),

<a name="nehemiah_12_7"></a>Nehemiah 12:7

[Sallû](../../strongs/h/h5543.md), [ʿĀmôq](../../strongs/h/h5987.md), [Ḥilqîyâ](../../strongs/h/h2518.md), [YᵊḏaʿYâ](../../strongs/h/h3048.md). These were the [ro'sh](../../strongs/h/h7218.md) of the [kōhēn](../../strongs/h/h3548.md) and of their ['ach](../../strongs/h/h251.md) in the [yowm](../../strongs/h/h3117.md) of [Yēšûaʿ](../../strongs/h/h3442.md).

<a name="nehemiah_12_8"></a>Nehemiah 12:8

Moreover the [Lᵊvî](../../strongs/h/h3881.md): [Yēšûaʿ](../../strongs/h/h3442.md), [Binnûy](../../strongs/h/h1131.md), [Qaḏmî'Ēl](../../strongs/h/h6934.md), [Šērēḇyâ](../../strongs/h/h8274.md), [Yehuwdah](../../strongs/h/h3063.md), and [Matanyâ](../../strongs/h/h4983.md), which was over the [huyyᵊḏôṯ](../../strongs/h/h1960.md), he and his ['ach](../../strongs/h/h251.md).

<a name="nehemiah_12_9"></a>Nehemiah 12:9

Also [Baqbuqyâ](../../strongs/h/h1229.md) and [ʿUnnî](../../strongs/h/h6042.md), their ['ach](../../strongs/h/h251.md), were over against them in the [mišmereṯ](../../strongs/h/h4931.md).

<a name="nehemiah_12_10"></a>Nehemiah 12:10

And [Yēšûaʿ](../../strongs/h/h3442.md) [yalad](../../strongs/h/h3205.md) [Yôyāqîm](../../strongs/h/h3113.md), [Yôyāqîm](../../strongs/h/h3113.md) also [yalad](../../strongs/h/h3205.md) ['Elyāšîḇ](../../strongs/h/h475.md), and ['Elyāšîḇ](../../strongs/h/h475.md) [yalad](../../strongs/h/h3205.md) [Yôyāḏāʿ](../../strongs/h/h3111.md),

<a name="nehemiah_12_11"></a>Nehemiah 12:11

And [Yôyāḏāʿ](../../strongs/h/h3111.md) [yalad](../../strongs/h/h3205.md) [Yônāṯān](../../strongs/h/h3129.md), and [Yônāṯān](../../strongs/h/h3129.md) [yalad](../../strongs/h/h3205.md) [Yadûaʿ](../../strongs/h/h3037.md).

<a name="nehemiah_12_12"></a>Nehemiah 12:12

And in the [yowm](../../strongs/h/h3117.md) of [Yôyāqîm](../../strongs/h/h3113.md) were [kōhēn](../../strongs/h/h3548.md), the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md): of [Śᵊrāyâ](../../strongs/h/h8304.md), [Mᵊrāyâ](../../strongs/h/h4811.md); of [Yirmᵊyâ](../../strongs/h/h3414.md), [Ḥănanyâ](../../strongs/h/h2608.md);

<a name="nehemiah_12_13"></a>Nehemiah 12:13

Of [ʿEzrā'](../../strongs/h/h5830.md), [Mᵊšullām](../../strongs/h/h4918.md); of ['Ămaryâ](../../strongs/h/h568.md), [Yᵊhôḥānān](../../strongs/h/h3076.md);

<a name="nehemiah_12_14"></a>Nehemiah 12:14

Of [Mallûḵ](../../strongs/h/h4409.md), [Yônāṯān](../../strongs/h/h3129.md); of [Šᵊḇanyâ](../../strongs/h/h7645.md), [Yôsēp̄](../../strongs/h/h3130.md);

<a name="nehemiah_12_15"></a>Nehemiah 12:15

Of [Ḥārim](../../strongs/h/h2766.md), [ʿAḏnā'](../../strongs/h/h5733.md); of [Mᵊrāyôṯ](../../strongs/h/h4812.md), [Ḥelqay](../../strongs/h/h2517.md);

<a name="nehemiah_12_16"></a>Nehemiah 12:16

Of [ʿIdô](../../strongs/h/h5714.md), [Zᵊḵaryâ](../../strongs/h/h2148.md); of [Ginnᵊṯôn](../../strongs/h/h1599.md), [Mᵊšullām](../../strongs/h/h4918.md);

<a name="nehemiah_12_17"></a>Nehemiah 12:17

Of ['Ăḇîâ](../../strongs/h/h29.md), [Ziḵrî](../../strongs/h/h2147.md); of [Minyāmîn](../../strongs/h/h4509.md), of [MôʿAḏyâ](../../strongs/h/h4153.md), [Pilṭay](../../strongs/h/h6408.md);

<a name="nehemiah_12_18"></a>Nehemiah 12:18

Of [Bilgâ](../../strongs/h/h1083.md), [Šammûaʿ](../../strongs/h/h8051.md); of [ŠᵊmaʿYâ](../../strongs/h/h8098.md), [Yᵊhônāṯān](../../strongs/h/h3083.md);

<a name="nehemiah_12_19"></a>Nehemiah 12:19

And of [Yôyārîḇ](../../strongs/h/h3114.md), [Matnay](../../strongs/h/h4982.md); of [YᵊḏaʿYâ](../../strongs/h/h3048.md), [ʿUzzî](../../strongs/h/h5813.md);

<a name="nehemiah_12_20"></a>Nehemiah 12:20

Of [Sallû](../../strongs/h/h5543.md), [Qallay](../../strongs/h/h7040.md); of [ʿĀmôq](../../strongs/h/h5987.md), [ʿēḇer](../../strongs/h/h5677.md);

<a name="nehemiah_12_21"></a>Nehemiah 12:21

Of [Ḥilqîyâ](../../strongs/h/h2518.md), [Ḥăšaḇyâ](../../strongs/h/h2811.md); of [YᵊḏaʿYâ](../../strongs/h/h3048.md), [Nᵊṯan'ēl](../../strongs/h/h5417.md).

<a name="nehemiah_12_22"></a>Nehemiah 12:22

The [Lᵊvî](../../strongs/h/h3881.md) in the [yowm](../../strongs/h/h3117.md) of ['Elyāšîḇ](../../strongs/h/h475.md), [Yôyāḏāʿ](../../strongs/h/h3111.md), and [Yôḥānān](../../strongs/h/h3110.md), and [Yadûaʿ](../../strongs/h/h3037.md), were [kāṯaḇ](../../strongs/h/h3789.md) [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md): also the [kōhēn](../../strongs/h/h3548.md), to the [malkuwth](../../strongs/h/h4438.md) of [Daryāveš](../../strongs/h/h1867.md) the [parsî](../../strongs/h/h6542.md).

<a name="nehemiah_12_23"></a>Nehemiah 12:23

The [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md), the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md), were [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md), even until the [yowm](../../strongs/h/h3117.md) of [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of ['Elyāšîḇ](../../strongs/h/h475.md).

<a name="nehemiah_12_24"></a>Nehemiah 12:24

And the [ro'sh](../../strongs/h/h7218.md) of the [Lᵊvî](../../strongs/h/h3881.md): [Ḥăšaḇyâ](../../strongs/h/h2811.md), [Šērēḇyâ](../../strongs/h/h8274.md), and [Yēšûaʿ](../../strongs/h/h3442.md) the [ben](../../strongs/h/h1121.md) of [Qaḏmî'Ēl](../../strongs/h/h6934.md), with their ['ach](../../strongs/h/h251.md) over against them, to [halal](../../strongs/h/h1984.md) and to [yadah](../../strongs/h/h3034.md), according to the [mitsvah](../../strongs/h/h4687.md) of [Dāviḏ](../../strongs/h/h1732.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), [mišmār](../../strongs/h/h4929.md) over [ʿummâ](../../strongs/h/h5980.md) [mišmār](../../strongs/h/h4929.md).

<a name="nehemiah_12_25"></a>Nehemiah 12:25

[Matanyâ](../../strongs/h/h4983.md), and [Baqbuqyâ](../../strongs/h/h1229.md), [ʿŌḇaḏyâ](../../strongs/h/h5662.md), [Mᵊšullām](../../strongs/h/h4918.md), [Ṭalmôn](../../strongs/h/h2929.md), [ʿAqqûḇ](../../strongs/h/h6126.md), were [šôʿēr](../../strongs/h/h7778.md) [shamar](../../strongs/h/h8104.md) the [mišmār](../../strongs/h/h4929.md) at the ['āsōp̄](../../strongs/h/h624.md) of the [sha'ar](../../strongs/h/h8179.md).

<a name="nehemiah_12_26"></a>Nehemiah 12:26

These were in the [yowm](../../strongs/h/h3117.md) of [Yôyāqîm](../../strongs/h/h3113.md) the [ben](../../strongs/h/h1121.md) of [Yēšûaʿ](../../strongs/h/h3442.md), the [ben](../../strongs/h/h1121.md) of [Yôṣāḏāq](../../strongs/h/h3136.md), and in the [yowm](../../strongs/h/h3117.md) of [Nᵊḥemyâ](../../strongs/h/h5166.md) the [peḥâ](../../strongs/h/h6346.md), and of [ʿEzrā'](../../strongs/h/h5830.md) the [kōhēn](../../strongs/h/h3548.md), the [sāp̄ar](../../strongs/h/h5608.md).

<a name="nehemiah_12_27"></a>Nehemiah 12:27

And at the [ḥănukâ](../../strongs/h/h2598.md) of the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) they [bāqaš](../../strongs/h/h1245.md) the [Lᵊvî](../../strongs/h/h3881.md) out of all their [maqowm](../../strongs/h/h4725.md), to [bow'](../../strongs/h/h935.md) them to [Yĕruwshalaim](../../strongs/h/h3389.md), to ['asah](../../strongs/h/h6213.md) the [ḥănukâ](../../strongs/h/h2598.md) with [simchah](../../strongs/h/h8057.md), both with [tôḏâ](../../strongs/h/h8426.md), and with [šîr](../../strongs/h/h7892.md), with [mᵊṣēleṯ](../../strongs/h/h4700.md), [neḇel](../../strongs/h/h5035.md), and with [kinnôr](../../strongs/h/h3658.md).

<a name="nehemiah_12_28"></a>Nehemiah 12:28

And the [ben](../../strongs/h/h1121.md) of the [shiyr](../../strongs/h/h7891.md) gathered themselves ['āsap̄](../../strongs/h/h622.md), both out of the plain [kikār](../../strongs/h/h3603.md) [cabiyb](../../strongs/h/h5439.md) [Yĕruwshalaim](../../strongs/h/h3389.md), and from the [ḥāṣēr](../../strongs/h/h2691.md) of [Nᵊṭōp̄āṯî](../../strongs/h/h5200.md);

<a name="nehemiah_12_29"></a>Nehemiah 12:29

Also from the [bayith](../../strongs/h/h1004.md) of [Gilgāl](../../strongs/h/h1537.md) H1019, and out of the [sadeh](../../strongs/h/h7704.md) of [Geḇaʿ](../../strongs/h/h1387.md) and [ʿAzmāveṯ](../../strongs/h/h5820.md): for the [shiyr](../../strongs/h/h7891.md) had [bānâ](../../strongs/h/h1129.md) them [ḥāṣēr](../../strongs/h/h2691.md) [cabiyb](../../strongs/h/h5439.md) [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="nehemiah_12_30"></a>Nehemiah 12:30

And the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md) [ṭāhēr](../../strongs/h/h2891.md) themselves, and [ṭāhēr](../../strongs/h/h2891.md) the ['am](../../strongs/h/h5971.md), and the [sha'ar](../../strongs/h/h8179.md), and the [ḥômâ](../../strongs/h/h2346.md).

<a name="nehemiah_12_31"></a>Nehemiah 12:31

Then I brought [ʿālâ](../../strongs/h/h5927.md) the [śar](../../strongs/h/h8269.md) of [Yehuwdah](../../strongs/h/h3063.md) upon the [ḥômâ](../../strongs/h/h2346.md), and ['amad](../../strongs/h/h5975.md) [šᵊnayim](../../strongs/h/h8147.md) [gadowl](../../strongs/h/h1419.md) companies of them that gave [tôḏâ](../../strongs/h/h8426.md), whereof one [tahălûḵâ](../../strongs/h/h8418.md) on the [yamiyn](../../strongs/h/h3225.md) upon the [ḥômâ](../../strongs/h/h2346.md) toward the ['ašpōṯ](../../strongs/h/h830.md) [sha'ar](../../strongs/h/h8179.md):

<a name="nehemiah_12_32"></a>Nehemiah 12:32

And ['aḥar](../../strongs/h/h310.md) them [yālaḵ](../../strongs/h/h3212.md) [HôšaʿYâ](../../strongs/h/h1955.md), and [ḥēṣî](../../strongs/h/h2677.md) of the [śar](../../strongs/h/h8269.md) of [Yehuwdah](../../strongs/h/h3063.md),

<a name="nehemiah_12_33"></a>Nehemiah 12:33

And [ʿĂzaryâ](../../strongs/h/h5838.md), [ʿEzrā'](../../strongs/h/h5830.md), and [Mᵊšullām](../../strongs/h/h4918.md),

<a name="nehemiah_12_34"></a>Nehemiah 12:34

[Yehuwdah](../../strongs/h/h3063.md), and [Binyāmîn](../../strongs/h/h1144.md), and [ŠᵊmaʿYâ](../../strongs/h/h8098.md), and [Yirmᵊyâ](../../strongs/h/h3414.md),

<a name="nehemiah_12_35"></a>Nehemiah 12:35

And certain of the [kōhēn](../../strongs/h/h3548.md) [ben](../../strongs/h/h1121.md) with [ḥăṣōṣrâ](../../strongs/h/h2689.md); namely, [Zᵊḵaryâ](../../strongs/h/h2148.md) the [ben](../../strongs/h/h1121.md) of [Yônāṯān](../../strongs/h/h3129.md), the [ben](../../strongs/h/h1121.md) of [ŠᵊmaʿYâ](../../strongs/h/h8098.md), the [ben](../../strongs/h/h1121.md) of [Matanyâ](../../strongs/h/h4983.md), the [ben](../../strongs/h/h1121.md) of [Mîḵāyâ](../../strongs/h/h4320.md), the [ben](../../strongs/h/h1121.md) of [Zakûr](../../strongs/h/h2139.md), the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md):

<a name="nehemiah_12_36"></a>Nehemiah 12:36

And his ['ach](../../strongs/h/h251.md), [ŠᵊmaʿYâ](../../strongs/h/h8098.md), and [ʿĂzar'Ēl](../../strongs/h/h5832.md), [Milălay](../../strongs/h/h4450.md), [Gilălî](../../strongs/h/h1562.md), [MāʿAy](../../strongs/h/h4597.md), [Nᵊṯan'ēl](../../strongs/h/h5417.md), and [Yehuwdah](../../strongs/h/h3063.md), [Ḥănānî](../../strongs/h/h2607.md), with the [šîr](../../strongs/h/h7892.md) [kĕliy](../../strongs/h/h3627.md) of [Dāviḏ](../../strongs/h/h1732.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), and [ʿEzrā'](../../strongs/h/h5830.md) the [sāp̄ar](../../strongs/h/h5608.md) [paniym](../../strongs/h/h6440.md) them.

<a name="nehemiah_12_37"></a>Nehemiah 12:37

And at the ['ayin](../../strongs/h/h5869.md) [sha'ar](../../strongs/h/h8179.md), which was over against them, they went [ʿālâ](../../strongs/h/h5927.md) by the [maʿălâ](../../strongs/h/h4609.md) of the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), at the going [maʿălê](../../strongs/h/h4608.md) of the [ḥômâ](../../strongs/h/h2346.md), above the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md), even unto the [mayim](../../strongs/h/h4325.md) [sha'ar](../../strongs/h/h8179.md) [mizrach](../../strongs/h/h4217.md).

<a name="nehemiah_12_38"></a>Nehemiah 12:38

And the [šēnî](../../strongs/h/h8145.md) company of them that gave [tôḏâ](../../strongs/h/h8426.md) [halak](../../strongs/h/h1980.md) over [môl](../../strongs/h/h4136.md) them, and I ['aḥar](../../strongs/h/h310.md) them, and the [ḥēṣî](../../strongs/h/h2677.md) of the ['am](../../strongs/h/h5971.md) upon the [ḥômâ](../../strongs/h/h2346.md), from beyond the [miḡdāl](../../strongs/h/h4026.md) of the [tannûr](../../strongs/h/h8574.md) even unto the [rāḥāḇ](../../strongs/h/h7342.md) [ḥômâ](../../strongs/h/h2346.md);

<a name="nehemiah_12_39"></a>Nehemiah 12:39

And from above the [sha'ar](../../strongs/h/h8179.md) of ['Ep̄rayim](../../strongs/h/h669.md), and above the [yāšān](../../strongs/h/h3465.md) [sha'ar](../../strongs/h/h8179.md), and above the [dag](../../strongs/h/h1709.md) [sha'ar](../../strongs/h/h8179.md), and the [miḡdāl](../../strongs/h/h4026.md) of [Ḥănan'Ēl](../../strongs/h/h2606.md), and the [miḡdāl](../../strongs/h/h4026.md) of [mē'â](../../strongs/h/h3968.md), even unto the [tso'n](../../strongs/h/h6629.md) [sha'ar](../../strongs/h/h8179.md): and they stood ['amad](../../strongs/h/h5975.md) in the [maṭṭārâ](../../strongs/h/h4307.md) [sha'ar](../../strongs/h/h8179.md).

<a name="nehemiah_12_40"></a>Nehemiah 12:40

So ['amad](../../strongs/h/h5975.md) the [šᵊnayim](../../strongs/h/h8147.md) companies of them that gave [tôḏâ](../../strongs/h/h8426.md) in the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), and I, and the [ḥēṣî](../../strongs/h/h2677.md) of the [sāḡān](../../strongs/h/h5461.md) with me:

<a name="nehemiah_12_41"></a>Nehemiah 12:41

And the [kōhēn](../../strongs/h/h3548.md); ['Elyāqîm](../../strongs/h/h471.md), [MaʿĂśêâ](../../strongs/h/h4641.md), [Minyāmîn](../../strongs/h/h4509.md), [Mîḵāyâ](../../strongs/h/h4320.md), ['ElyᵊhôʿÊnay](../../strongs/h/h454.md), [Zᵊḵaryâ](../../strongs/h/h2148.md), and [Ḥănanyâ](../../strongs/h/h2608.md), with [ḥăṣōṣrâ](../../strongs/h/h2689.md);

<a name="nehemiah_12_42"></a>Nehemiah 12:42

And [MaʿĂśêâ](../../strongs/h/h4641.md), and [ŠᵊmaʿYâ](../../strongs/h/h8098.md), and ['Elʿāzār](../../strongs/h/h499.md), and [ʿUzzî](../../strongs/h/h5813.md), and [Yᵊhôḥānān](../../strongs/h/h3076.md), and [Malkîyâ](../../strongs/h/h4441.md), and [ʿÊlām](../../strongs/h/h5867.md), and [ʿĒzer](../../strongs/h/h5829.md). And the [shiyr](../../strongs/h/h7891.md) sang [shama'](../../strongs/h/h8085.md), with [Yizraḥyâ](../../strongs/h/h3156.md) their [pāqîḏ](../../strongs/h/h6496.md).

<a name="nehemiah_12_43"></a>Nehemiah 12:43

Also that [yowm](../../strongs/h/h3117.md) they [zabach](../../strongs/h/h2076.md) [gadowl](../../strongs/h/h1419.md) [zebach](../../strongs/h/h2077.md), and [samach](../../strongs/h/h8055.md): for ['Elohiym](../../strongs/h/h430.md) had made them [samach](../../strongs/h/h8055.md) with [gadowl](../../strongs/h/h1419.md) [simchah](../../strongs/h/h8057.md): the ['ishshah](../../strongs/h/h802.md) also and the [yeleḏ](../../strongs/h/h3206.md) [samach](../../strongs/h/h8055.md): so that the [simchah](../../strongs/h/h8057.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) was [shama'](../../strongs/h/h8085.md) even afar [rachowq](../../strongs/h/h7350.md).

<a name="nehemiah_12_44"></a>Nehemiah 12:44

And at that [yowm](../../strongs/h/h3117.md) were ['enowsh](../../strongs/h/h582.md) [paqad](../../strongs/h/h6485.md) over the [niškâ](../../strongs/h/h5393.md) for the ['ôṣār](../../strongs/h/h214.md), for the [tᵊrûmâ](../../strongs/h/h8641.md), for the [re'shiyth](../../strongs/h/h7225.md), and for the [maʿăśēr](../../strongs/h/h4643.md), to [kānas](../../strongs/h/h3664.md) into them out of the [sadeh](../../strongs/h/h7704.md) of the [ʿîr](../../strongs/h/h5892.md) the [mᵊnāṯ](../../strongs/h/h4521.md) of the [towrah](../../strongs/h/h8451.md) for the [kōhēn](../../strongs/h/h3548.md) and [Lᵊvî](../../strongs/h/h3881.md): for [Yehuwdah](../../strongs/h/h3063.md) [simchah](../../strongs/h/h8057.md) for the [kōhēn](../../strongs/h/h3548.md) and for the [Lᵊvî](../../strongs/h/h3881.md) that ['amad](../../strongs/h/h5975.md).

<a name="nehemiah_12_45"></a>Nehemiah 12:45

And both the [shiyr](../../strongs/h/h7891.md) and the [šôʿēr](../../strongs/h/h7778.md) [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of their ['Elohiym](../../strongs/h/h430.md), and the [mišmereṯ](../../strongs/h/h4931.md) of the [ṭāhŏrâ](../../strongs/h/h2893.md), according to the [mitsvah](../../strongs/h/h4687.md) of [Dāviḏ](../../strongs/h/h1732.md), and of [Šᵊlōmô](../../strongs/h/h8010.md) his [ben](../../strongs/h/h1121.md).

<a name="nehemiah_12_46"></a>Nehemiah 12:46

For in the [yowm](../../strongs/h/h3117.md) of [Dāviḏ](../../strongs/h/h1732.md) and ['Āsāp̄](../../strongs/h/h623.md) of [qeḏem](../../strongs/h/h6924.md) there were [ro'sh](../../strongs/h/h7218.md) of the [shiyr](../../strongs/h/h7891.md), and [šîr](../../strongs/h/h7892.md) of [tehillah](../../strongs/h/h8416.md) and [yadah](../../strongs/h/h3034.md) unto ['Elohiym](../../strongs/h/h430.md).

<a name="nehemiah_12_47"></a>Nehemiah 12:47

And all [Yisra'el](../../strongs/h/h3478.md) in the [yowm](../../strongs/h/h3117.md) of [Zᵊrubāḇel](../../strongs/h/h2216.md), and in the [yowm](../../strongs/h/h3117.md) of [Nᵊḥemyâ](../../strongs/h/h5166.md), [nathan](../../strongs/h/h5414.md) the [mᵊnāṯ](../../strongs/h/h4521.md) of the [shiyr](../../strongs/h/h7891.md) and the [šôʿēr](../../strongs/h/h7778.md), every [yowm](../../strongs/h/h3117.md) his [dabar](../../strongs/h/h1697.md): and they [qadash](../../strongs/h/h6942.md) holy things unto the [Lᵊvî](../../strongs/h/h3881.md); and the [Lᵊvî](../../strongs/h/h3881.md) [qadash](../../strongs/h/h6942.md) them unto the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md).

---

[Transliteral Bible](../bible.md)

[Nehemiah](nehemiah.md)

[Nehemiah 11](nehemiah_11.md) - [Nehemiah 13](nehemiah_13.md)