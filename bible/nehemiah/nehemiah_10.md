# [Nehemiah 10](https://www.blueletterbible.org/kjv/nehemiah/10)

<a name="nehemiah_10_1"></a>Nehemiah 10:1

Now those that [ḥāṯam](../../strongs/h/h2856.md) were, [Nᵊḥemyâ](../../strongs/h/h5166.md), the [tiršāṯā'](../../strongs/h/h8660.md), the [ben](../../strongs/h/h1121.md) of [Ḥăḵalyâ](../../strongs/h/h2446.md), and [Ṣḏqyh](../../strongs/h/h6667.md),

<a name="nehemiah_10_2"></a>Nehemiah 10:2

[Śᵊrāyâ](../../strongs/h/h8304.md), [ʿĂzaryâ](../../strongs/h/h5838.md), [Yirmᵊyâ](../../strongs/h/h3414.md),

<a name="nehemiah_10_3"></a>Nehemiah 10:3

[Pašḥûr](../../strongs/h/h6583.md), ['Ămaryâ](../../strongs/h/h568.md), [Malkîyâ](../../strongs/h/h4441.md),

<a name="nehemiah_10_4"></a>Nehemiah 10:4

[Ḥaṭṭûš](../../strongs/h/h2407.md), [Šᵊḇanyâ](../../strongs/h/h7645.md), [Mallûḵ](../../strongs/h/h4409.md),

<a name="nehemiah_10_5"></a>Nehemiah 10:5

[Ḥārim](../../strongs/h/h2766.md), [Mᵊrēmôṯ](../../strongs/h/h4822.md), [ʿŌḇaḏyâ](../../strongs/h/h5662.md),

<a name="nehemiah_10_6"></a>Nehemiah 10:6

[Dinîyē'L](../../strongs/h/h1840.md), [Ginnᵊṯôn](../../strongs/h/h1599.md), [Bārûḵ](../../strongs/h/h1263.md),

<a name="nehemiah_10_7"></a>Nehemiah 10:7

[Mᵊšullām](../../strongs/h/h4918.md), ['Ăḇîâ](../../strongs/h/h29.md), [Minyāmîn](../../strongs/h/h4326.md),

<a name="nehemiah_10_8"></a>Nehemiah 10:8

[MaʿAzyâ](../../strongs/h/h4590.md), [Bilgāy](../../strongs/h/h1084.md), [ŠᵊmaʿYâ](../../strongs/h/h8098.md): these were the [kōhēn](../../strongs/h/h3548.md).

<a name="nehemiah_10_9"></a>Nehemiah 10:9

And the [Lᵊvî](../../strongs/h/h3881.md): both [Yēšûaʿ](../../strongs/h/h3442.md) the [ben](../../strongs/h/h1121.md) of ['Ăzanyâ](../../strongs/h/h245.md), [Binnûy](../../strongs/h/h1131.md) of the [ben](../../strongs/h/h1121.md) of [Ḥēnāḏāḏ](../../strongs/h/h2582.md), [Qaḏmî'Ēl](../../strongs/h/h6934.md);

<a name="nehemiah_10_10"></a>Nehemiah 10:10

And their ['ach](../../strongs/h/h251.md), [Šᵊḇanyâ](../../strongs/h/h7645.md), [Hôḏîyâ](../../strongs/h/h1941.md), [Qᵊlîṭā'](../../strongs/h/h7042.md), [Pᵊlāyâ](../../strongs/h/h6411.md), [Ḥānān](../../strongs/h/h2605.md),

<a name="nehemiah_10_11"></a>Nehemiah 10:11

[Mîḵā'](../../strongs/h/h4316.md), [rᵊḥōḇ](../../strongs/h/h7340.md), [Ḥăšaḇyâ](../../strongs/h/h2811.md),

<a name="nehemiah_10_12"></a>Nehemiah 10:12

[Zakûr](../../strongs/h/h2139.md), [Šērēḇyâ](../../strongs/h/h8274.md), [Šᵊḇanyâ](../../strongs/h/h7645.md),

<a name="nehemiah_10_13"></a>Nehemiah 10:13

[Hôḏîyâ](../../strongs/h/h1941.md), [Bānî](../../strongs/h/h1137.md), [Bᵊnînû](../../strongs/h/h1148.md).

<a name="nehemiah_10_14"></a>Nehemiah 10:14

The [ro'sh](../../strongs/h/h7218.md) of the ['am](../../strongs/h/h5971.md); [ParʿŠ](../../strongs/h/h6551.md), [Paḥaṯ Mô'Āḇ](../../strongs/h/h6355.md), [ʿÊlām](../../strongs/h/h5867.md), [Zatû'](../../strongs/h/h2240.md), [Bānî](../../strongs/h/h1137.md),

<a name="nehemiah_10_15"></a>Nehemiah 10:15

[Bunnî](../../strongs/h/h1138.md), [ʿAzgāḏ](../../strongs/h/h5803.md), [Bēḇay](../../strongs/h/h893.md),

<a name="nehemiah_10_16"></a>Nehemiah 10:16

['Ăḏōnîyâ](../../strongs/h/h138.md), [Biḡvay](../../strongs/h/h902.md), [ʿĀḏîn](../../strongs/h/h5720.md),

<a name="nehemiah_10_17"></a>Nehemiah 10:17

['Āṭēr](../../strongs/h/h333.md), [Ḥizqîyâ](../../strongs/h/h2396.md), [ʿAzzûr](../../strongs/h/h5809.md),

<a name="nehemiah_10_18"></a>Nehemiah 10:18

[Hôḏîyâ](../../strongs/h/h1941.md), [Ḥāšum](../../strongs/h/h2828.md), [Bēṣay](../../strongs/h/h1209.md),

<a name="nehemiah_10_19"></a>Nehemiah 10:19

[Ḥārîp̄](../../strongs/h/h2756.md), [ʿĂnāṯôṯ](../../strongs/h/h6068.md), [Nôḇāy](../../strongs/h/h5109.md),

<a name="nehemiah_10_20"></a>Nehemiah 10:20

[MaḡpîʿĀš](../../strongs/h/h4047.md), [Mᵊšullām](../../strongs/h/h4918.md), [Ḥēzîr](../../strongs/h/h2387.md),

<a name="nehemiah_10_21"></a>Nehemiah 10:21

[Mᵊšêzaḇ'Ēl](../../strongs/h/h4898.md), [Ṣāḏôq](../../strongs/h/h6659.md), [Yadûaʿ](../../strongs/h/h3037.md),

<a name="nehemiah_10_22"></a>Nehemiah 10:22

[Pᵊlaṭyâ](../../strongs/h/h6410.md), [Ḥānān](../../strongs/h/h2605.md), [ʿĂnāyâ](../../strongs/h/h6043.md),

<a name="nehemiah_10_23"></a>Nehemiah 10:23

[Hôšēaʿ](../../strongs/h/h1954.md), [Ḥănanyâ](../../strongs/h/h2608.md), [Ḥaššûḇ](../../strongs/h/h2815.md),

<a name="nehemiah_10_24"></a>Nehemiah 10:24

[Lôḥēš](../../strongs/h/h3873.md), [Pilḥā'](../../strongs/h/h6401.md), [Šôḇēq](../../strongs/h/h7733.md),

<a name="nehemiah_10_25"></a>Nehemiah 10:25

[Rᵊḥûm](../../strongs/h/h7348.md), [Ḥăšaḇnâ](../../strongs/h/h2812.md), [MaʿĂśêâ](../../strongs/h/h4641.md),

<a name="nehemiah_10_26"></a>Nehemiah 10:26

And ['Ăḥîyâ](../../strongs/h/h281.md), [Ḥānān](../../strongs/h/h2605.md), [ʿĀnān](../../strongs/h/h6052.md),

<a name="nehemiah_10_27"></a>Nehemiah 10:27

[Mallûḵ](../../strongs/h/h4409.md), [Ḥārim](../../strongs/h/h2766.md), [BaʿĂnâ](../../strongs/h/h1196.md).

<a name="nehemiah_10_28"></a>Nehemiah 10:28

And the [šᵊ'ār](../../strongs/h/h7605.md) of the ['am](../../strongs/h/h5971.md), the [kōhēn](../../strongs/h/h3548.md), the [Lᵊvî](../../strongs/h/h3881.md), the [šôʿēr](../../strongs/h/h7778.md), the [shiyr](../../strongs/h/h7891.md), the [Nāṯîn](../../strongs/h/h5411.md), and all they that had [bāḏal](../../strongs/h/h914.md) themselves from the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) unto the [towrah](../../strongs/h/h8451.md) of ['Elohiym](../../strongs/h/h430.md), their ['ishshah](../../strongs/h/h802.md), their [ben](../../strongs/h/h1121.md), and their [bath](../../strongs/h/h1323.md), every one having [yada'](../../strongs/h/h3045.md), and having [bîn](../../strongs/h/h995.md);

<a name="nehemiah_10_29"></a>Nehemiah 10:29

They [ḥāzaq](../../strongs/h/h2388.md) to their ['ach](../../strongs/h/h251.md), their ['addiyr](../../strongs/h/h117.md), and [bow'](../../strongs/h/h935.md) into an ['alah](../../strongs/h/h423.md), and into a [šᵊḇûʿâ](../../strongs/h/h7621.md), to [yālaḵ](../../strongs/h/h3212.md) in ['Elohiym](../../strongs/h/h430.md) [towrah](../../strongs/h/h8451.md), which was [nathan](../../strongs/h/h5414.md) [yad](../../strongs/h/h3027.md) [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of ['Elohiym](../../strongs/h/h430.md), and to [shamar](../../strongs/h/h8104.md) and ['asah](../../strongs/h/h6213.md) all the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['adown](../../strongs/h/h113.md), and his [mishpat](../../strongs/h/h4941.md) and his [choq](../../strongs/h/h2706.md);

<a name="nehemiah_10_30"></a>Nehemiah 10:30

And that we would not [nathan](../../strongs/h/h5414.md) our [bath](../../strongs/h/h1323.md) unto the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), nor [laqach](../../strongs/h/h3947.md) their [bath](../../strongs/h/h1323.md) for our [ben](../../strongs/h/h1121.md):

<a name="nehemiah_10_31"></a>Nehemiah 10:31

And if the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) [bow'](../../strongs/h/h935.md) [maqqāḥâ](../../strongs/h/h4728.md) or any [šēḇer](../../strongs/h/h7668.md) on the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md) to [māḵar](../../strongs/h/h4376.md), that we would not [laqach](../../strongs/h/h3947.md) it of them on the [shabbath](../../strongs/h/h7676.md), or on the [qodesh](../../strongs/h/h6944.md) [yowm](../../strongs/h/h3117.md): and that we would [nāṭaš](../../strongs/h/h5203.md) the [šᵊḇîʿî](../../strongs/h/h7637.md) [šānâ](../../strongs/h/h8141.md), and the [maśśā'](../../strongs/h/h4853.md) of every [yad](../../strongs/h/h3027.md).

<a name="nehemiah_10_32"></a>Nehemiah 10:32

Also we ['amad](../../strongs/h/h5975.md) [mitsvah](../../strongs/h/h4687.md) for us, to [nathan](../../strongs/h/h5414.md) ourselves [šānâ](../../strongs/h/h8141.md) with the third [šᵊlîšî](../../strongs/h/h7992.md) of a [šeqel](../../strongs/h/h8255.md) for the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md);

<a name="nehemiah_10_33"></a>Nehemiah 10:33

For the [maʿăreḵeṯ](../../strongs/h/h4635.md) [lechem](../../strongs/h/h3899.md), and for the [tāmîḏ](../../strongs/h/h8548.md) meat [minchah](../../strongs/h/h4503.md), and for the [tāmîḏ](../../strongs/h/h8548.md) [ʿōlâ](../../strongs/h/h5930.md), of the [shabbath](../../strongs/h/h7676.md), of the new [ḥōḏeš](../../strongs/h/h2320.md), for the set [môʿēḏ](../../strongs/h/h4150.md), and for the [qodesh](../../strongs/h/h6944.md) things, and for the sin [chatta'ath](../../strongs/h/h2403.md) to make a [kāp̄ar](../../strongs/h/h3722.md) for [Yisra'el](../../strongs/h/h3478.md), and for all the [mĕla'kah](../../strongs/h/h4399.md) of the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md).

<a name="nehemiah_10_34"></a>Nehemiah 10:34

And we [naphal](../../strongs/h/h5307.md) the [gôrāl](../../strongs/h/h1486.md) among the [kōhēn](../../strongs/h/h3548.md), the [Lᵊvî](../../strongs/h/h3881.md), and the ['am](../../strongs/h/h5971.md), for the ['ets](../../strongs/h/h6086.md) [qorban](../../strongs/h/h7133.md), to [bow'](../../strongs/h/h935.md) it into the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md), after the [bayith](../../strongs/h/h1004.md) of our ['ab](../../strongs/h/h1.md), at [ʿēṯ](../../strongs/h/h6256.md) [zāman](../../strongs/h/h2163.md) [šānâ](../../strongs/h/h8141.md) by [šānâ](../../strongs/h/h8141.md), to [bāʿar](../../strongs/h/h1197.md) upon the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), as it is [kāṯaḇ](../../strongs/h/h3789.md) in the [towrah](../../strongs/h/h8451.md):

<a name="nehemiah_10_35"></a>Nehemiah 10:35

And to [bow'](../../strongs/h/h935.md) the [bikûr](../../strongs/h/h1061.md) of our ['ăḏāmâ](../../strongs/h/h127.md), and the [bikûr](../../strongs/h/h1061.md) of all [pĕriy](../../strongs/h/h6529.md) of all ['ets](../../strongs/h/h6086.md), [šānâ](../../strongs/h/h8141.md) by [šānâ](../../strongs/h/h8141.md), unto the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md):

<a name="nehemiah_10_36"></a>Nehemiah 10:36

Also the [bᵊḵôr](../../strongs/h/h1060.md) of our [ben](../../strongs/h/h1121.md), and of our [bĕhemah](../../strongs/h/h929.md), as it is [kāṯaḇ](../../strongs/h/h3789.md) in the [towrah](../../strongs/h/h8451.md), and the [bᵊḵôrâ](../../strongs/h/h1062.md) of our [bāqār](../../strongs/h/h1241.md) and of our [tso'n](../../strongs/h/h6629.md), to [bow'](../../strongs/h/h935.md) to the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md), unto the [kōhēn](../../strongs/h/h3548.md) that [sharath](../../strongs/h/h8334.md) in the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md):

<a name="nehemiah_10_37"></a>Nehemiah 10:37

And that we should [bow'](../../strongs/h/h935.md) the [re'shiyth](../../strongs/h/h7225.md) of our [ʿărîsâ](../../strongs/h/h6182.md), and our [tᵊrûmâ](../../strongs/h/h8641.md), and the [pĕriy](../../strongs/h/h6529.md) of all manner of ['ets](../../strongs/h/h6086.md), of [tiyrowsh](../../strongs/h/h8492.md) and of [yiṣhār](../../strongs/h/h3323.md), unto the [kōhēn](../../strongs/h/h3548.md), to the [liškâ](../../strongs/h/h3957.md) of the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md); and the [maʿăśēr](../../strongs/h/h4643.md) of our ['ăḏāmâ](../../strongs/h/h127.md) unto the [Lᵊvî](../../strongs/h/h3881.md), that the same [Lᵊvî](../../strongs/h/h3881.md) might have the [ʿāśar](../../strongs/h/h6237.md) in all the [ʿîr](../../strongs/h/h5892.md) of our [ʿăḇōḏâ](../../strongs/h/h5656.md).

<a name="nehemiah_10_38"></a>Nehemiah 10:38

And the [kōhēn](../../strongs/h/h3548.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) shall be with the [Lᵊvî](../../strongs/h/h3881.md), when the [Lᵊvî](../../strongs/h/h3881.md) take [ʿāśar](../../strongs/h/h6237.md): and the [Lᵊvî](../../strongs/h/h3881.md) shall bring [ʿālâ](../../strongs/h/h5927.md) the [maʿăśēr](../../strongs/h/h4643.md) of the [maʿăśēr](../../strongs/h/h4643.md) unto the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md), to the [liškâ](../../strongs/h/h3957.md), into the ['ôṣār](../../strongs/h/h214.md) [bayith](../../strongs/h/h1004.md).

<a name="nehemiah_10_39"></a>Nehemiah 10:39

For the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) and the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md) shall [bow'](../../strongs/h/h935.md) the [tᵊrûmâ](../../strongs/h/h8641.md) of the [dagan](../../strongs/h/h1715.md), of the [tiyrowsh](../../strongs/h/h8492.md), and the [yiṣhār](../../strongs/h/h3323.md), unto the [liškâ](../../strongs/h/h3957.md), where are the [kĕliy](../../strongs/h/h3627.md) of the [miqdash](../../strongs/h/h4720.md), and the [kōhēn](../../strongs/h/h3548.md) that [sharath](../../strongs/h/h8334.md), and the [šôʿēr](../../strongs/h/h7778.md), and the [shiyr](../../strongs/h/h7891.md): and we will not ['azab](../../strongs/h/h5800.md) the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Nehemiah](nehemiah.md)

[Nehemiah 9](nehemiah_9.md) - [Nehemiah 11](nehemiah_11.md)