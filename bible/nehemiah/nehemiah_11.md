# [Nehemiah 11](https://www.blueletterbible.org/kjv/nehemiah/11)

<a name="nehemiah_11_1"></a>Nehemiah 11:1

And the [śar](../../strongs/h/h8269.md) of the ['am](../../strongs/h/h5971.md) [yashab](../../strongs/h/h3427.md) at [Yĕruwshalaim](../../strongs/h/h3389.md): the [šᵊ'ār](../../strongs/h/h7605.md) of the ['am](../../strongs/h/h5971.md) also [naphal](../../strongs/h/h5307.md) [gôrāl](../../strongs/h/h1486.md), to [bow'](../../strongs/h/h935.md) ['echad](../../strongs/h/h259.md) of [ʿeśer](../../strongs/h/h6235.md) to [yashab](../../strongs/h/h3427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) the [qodesh](../../strongs/h/h6944.md) [ʿîr](../../strongs/h/h5892.md), and [tēšaʿ](../../strongs/h/h8672.md) [yad](../../strongs/h/h3027.md) to dwell in other [ʿîr](../../strongs/h/h5892.md).

<a name="nehemiah_11_2"></a>Nehemiah 11:2

And the ['am](../../strongs/h/h5971.md) [barak](../../strongs/h/h1288.md) all the ['enowsh](../../strongs/h/h582.md), that willingly [nāḏaḇ](../../strongs/h/h5068.md) themselves to [yashab](../../strongs/h/h3427.md) at [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="nehemiah_11_3"></a>Nehemiah 11:3

Now these are the [ro'sh](../../strongs/h/h7218.md) of the [mᵊḏînâ](../../strongs/h/h4082.md) that [yashab](../../strongs/h/h3427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md): but in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md) [yashab](../../strongs/h/h3427.md) every ['iysh](../../strongs/h/h376.md) in his ['achuzzah](../../strongs/h/h272.md) in their [ʿîr](../../strongs/h/h5892.md), to wit, [Yisra'el](../../strongs/h/h3478.md), the [kōhēn](../../strongs/h/h3548.md), and the [Lᵊvî](../../strongs/h/h3881.md), and the [Nāṯîn](../../strongs/h/h5411.md), and the [ben](../../strongs/h/h1121.md) of [Šᵊlōmô](../../strongs/h/h8010.md) ['ebed](../../strongs/h/h5650.md).

<a name="nehemiah_11_4"></a>Nehemiah 11:4

And at [Yĕruwshalaim](../../strongs/h/h3389.md) [yashab](../../strongs/h/h3427.md) certain of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md), and of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md). Of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md); [ʿĂṯāyâ](../../strongs/h/h6265.md) the [ben](../../strongs/h/h1121.md) of ['Uzziyah](../../strongs/h/h5818.md), the [ben](../../strongs/h/h1121.md) of [Zᵊḵaryâ](../../strongs/h/h2148.md), the [ben](../../strongs/h/h1121.md) of ['Ămaryâ](../../strongs/h/h568.md), the [ben](../../strongs/h/h1121.md) of [Šᵊp̄Aṭyâ](../../strongs/h/h8203.md), the [ben](../../strongs/h/h1121.md) of [Mahălal'ēl](../../strongs/h/h4111.md), of the [ben](../../strongs/h/h1121.md) of [Pereṣ](../../strongs/h/h6557.md);

<a name="nehemiah_11_5"></a>Nehemiah 11:5

And [MaʿĂśêâ](../../strongs/h/h4641.md) the [ben](../../strongs/h/h1121.md) of [Bārûḵ](../../strongs/h/h1263.md), the [ben](../../strongs/h/h1121.md) of [Kl-Ḥōzê](../../strongs/h/h3626.md), the [ben](../../strongs/h/h1121.md) of [Ḥăzāyâ](../../strongs/h/h2382.md), the [ben](../../strongs/h/h1121.md) of [ʿĂḏāyâ](../../strongs/h/h5718.md), the [ben](../../strongs/h/h1121.md) of [Yôyārîḇ](../../strongs/h/h3114.md), the [ben](../../strongs/h/h1121.md) of [Zᵊḵaryâ](../../strongs/h/h2148.md), the [ben](../../strongs/h/h1121.md) of [Šilōnî](../../strongs/h/h8023.md).

<a name="nehemiah_11_6"></a>Nehemiah 11:6

All the [ben](../../strongs/h/h1121.md) of [Pereṣ](../../strongs/h/h6557.md) that [yashab](../../strongs/h/h3427.md) at [Yĕruwshalaim](../../strongs/h/h3389.md) were ['arbaʿ](../../strongs/h/h702.md) [mē'â](../../strongs/h/h3967.md) [šiššîm](../../strongs/h/h8346.md) and [šᵊmōnê](../../strongs/h/h8083.md) [ḥayil](../../strongs/h/h2428.md) ['enowsh](../../strongs/h/h582.md).

<a name="nehemiah_11_7"></a>Nehemiah 11:7

And these are the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md); [Sallû](../../strongs/h/h5543.md) the [ben](../../strongs/h/h1121.md) of [Mᵊšullām](../../strongs/h/h4918.md), the [ben](../../strongs/h/h1121.md) of [YôʿĒḏ](../../strongs/h/h3133.md), the [ben](../../strongs/h/h1121.md) of [Pᵊḏāyâ](../../strongs/h/h6305.md), the [ben](../../strongs/h/h1121.md) of [Qôlāyâ](../../strongs/h/h6964.md), the [ben](../../strongs/h/h1121.md) of [MaʿĂśêâ](../../strongs/h/h4641.md), the [ben](../../strongs/h/h1121.md) of ['Îṯî'Ēl](../../strongs/h/h384.md), the [ben](../../strongs/h/h1121.md) of [Yᵊšaʿyâ](../../strongs/h/h3470.md).

<a name="nehemiah_11_8"></a>Nehemiah 11:8

And ['aḥar](../../strongs/h/h310.md) him [Gabay](../../strongs/h/h1373.md), [Sallû](../../strongs/h/h5543.md), [tēšaʿ](../../strongs/h/h8672.md) [mē'â](../../strongs/h/h3967.md) [ʿeśrîm](../../strongs/h/h6242.md) and [šᵊmōnê](../../strongs/h/h8083.md).

<a name="nehemiah_11_9"></a>Nehemiah 11:9

And [Yô'Ēl](../../strongs/h/h3100.md) the [ben](../../strongs/h/h1121.md) of [Ziḵrî](../../strongs/h/h2147.md) was their [pāqîḏ](../../strongs/h/h6496.md): and [Yehuwdah](../../strongs/h/h3063.md) the [ben](../../strongs/h/h1121.md) of [Sᵊnû'Â](../../strongs/h/h5574.md) was [mišnê](../../strongs/h/h4932.md) over the [ʿîr](../../strongs/h/h5892.md).

<a name="nehemiah_11_10"></a>Nehemiah 11:10

Of the [kōhēn](../../strongs/h/h3548.md): [YᵊḏaʿYâ](../../strongs/h/h3048.md) the [ben](../../strongs/h/h1121.md) of [Yôyārîḇ](../../strongs/h/h3114.md), [Yāḵîn](../../strongs/h/h3199.md).

<a name="nehemiah_11_11"></a>Nehemiah 11:11

[Śᵊrāyâ](../../strongs/h/h8304.md) the [ben](../../strongs/h/h1121.md) of [Ḥilqîyâ](../../strongs/h/h2518.md), the [ben](../../strongs/h/h1121.md) of [Mᵊšullām](../../strongs/h/h4918.md), the [ben](../../strongs/h/h1121.md) of [Ṣāḏôq](../../strongs/h/h6659.md), the [ben](../../strongs/h/h1121.md) of [Mᵊrāyôṯ](../../strongs/h/h4812.md), the [ben](../../strongs/h/h1121.md) of ['Ăḥîṭûḇ](../../strongs/h/h285.md), was the [nāḡîḏ](../../strongs/h/h5057.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="nehemiah_11_12"></a>Nehemiah 11:12

And their ['ach](../../strongs/h/h251.md) that ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md) of the [bayith](../../strongs/h/h1004.md) were [šᵊmōnê](../../strongs/h/h8083.md) [mē'â](../../strongs/h/h3967.md) [ʿeśrîm](../../strongs/h/h6242.md) and [šᵊnayim](../../strongs/h/h8147.md): and [ʿĂḏāyâ](../../strongs/h/h5718.md) the [ben](../../strongs/h/h1121.md) of [Yᵊrōḥām](../../strongs/h/h3395.md), the [ben](../../strongs/h/h1121.md) of [Pᵊlalyâ](../../strongs/h/h6421.md), the [ben](../../strongs/h/h1121.md) of ['Amṣî](../../strongs/h/h557.md), the [ben](../../strongs/h/h1121.md) of [Zᵊḵaryâ](../../strongs/h/h2148.md), the [ben](../../strongs/h/h1121.md) of [Pašḥûr](../../strongs/h/h6583.md), the [ben](../../strongs/h/h1121.md) of [Malkîyâ](../../strongs/h/h4441.md),

<a name="nehemiah_11_13"></a>Nehemiah 11:13

And his ['ach](../../strongs/h/h251.md), [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md), [šᵊnayim](../../strongs/h/h8147.md) [mē'â](../../strongs/h/h3967.md) ['arbāʿîm](../../strongs/h/h705.md) and [šᵊnayim](../../strongs/h/h8147.md): and [ʿĂmašsay](../../strongs/h/h6023.md) the [ben](../../strongs/h/h1121.md) of [ʿĂzar'Ēl](../../strongs/h/h5832.md), the [ben](../../strongs/h/h1121.md) of ['Aḥzay](../../strongs/h/h273.md), the [ben](../../strongs/h/h1121.md) of [mᵊšillēmôṯ](../../strongs/h/h4919.md), the [ben](../../strongs/h/h1121.md) of ['Immēr](../../strongs/h/h564.md),

<a name="nehemiah_11_14"></a>Nehemiah 11:14

And their ['ach](../../strongs/h/h251.md), [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), a [mē'â](../../strongs/h/h3967.md) [ʿeśrîm](../../strongs/h/h6242.md) and [šᵊmōnê](../../strongs/h/h8083.md): and their [pāqîḏ](../../strongs/h/h6496.md) was [Zaḇdî'Ēl](../../strongs/h/h2068.md), the [ben](../../strongs/h/h1121.md) of one of the [gadowl](../../strongs/h/h1419.md).

<a name="nehemiah_11_15"></a>Nehemiah 11:15

Also of the [Lᵊvî](../../strongs/h/h3881.md): [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the [ben](../../strongs/h/h1121.md) of [Ḥaššûḇ](../../strongs/h/h2815.md), the [ben](../../strongs/h/h1121.md) of [ʿAzrîqām](../../strongs/h/h5840.md), the [ben](../../strongs/h/h1121.md) of [Ḥăšaḇyâ](../../strongs/h/h2811.md), the [ben](../../strongs/h/h1121.md) of [Bunnî](../../strongs/h/h1138.md);

<a name="nehemiah_11_16"></a>Nehemiah 11:16

And [Šabṯay](../../strongs/h/h7678.md) and [Yôzāḇāḏ](../../strongs/h/h3107.md), of the [ro'sh](../../strongs/h/h7218.md) of the [Lᵊvî](../../strongs/h/h3881.md), had the oversight of the [ḥîṣôn](../../strongs/h/h2435.md) [mĕla'kah](../../strongs/h/h4399.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="nehemiah_11_17"></a>Nehemiah 11:17

And [Matanyâ](../../strongs/h/h4983.md) the [ben](../../strongs/h/h1121.md) of [Mîḵā'](../../strongs/h/h4316.md), the [ben](../../strongs/h/h1121.md) of [Zaḇdî](../../strongs/h/h2067.md), the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md), was the [ro'sh](../../strongs/h/h7218.md) to [tᵊḥillâ](../../strongs/h/h8462.md) the [yadah](../../strongs/h/h3034.md) in [tĕphillah](../../strongs/h/h8605.md): and [Baqbuqyâ](../../strongs/h/h1229.md) the [mišnê](../../strongs/h/h4932.md) among his ['ach](../../strongs/h/h251.md), and [ʿAḇdā'](../../strongs/h/h5653.md) the [ben](../../strongs/h/h1121.md) of [Šammûaʿ](../../strongs/h/h8051.md), the [ben](../../strongs/h/h1121.md) of [Gālāl](../../strongs/h/h1559.md), the [ben](../../strongs/h/h1121.md) of [Yᵊḏûṯûn](../../strongs/h/h3038.md).

<a name="nehemiah_11_18"></a>Nehemiah 11:18

All the [Lᵊvî](../../strongs/h/h3881.md) in the [qodesh](../../strongs/h/h6944.md) [ʿîr](../../strongs/h/h5892.md) were two [mē'â](../../strongs/h/h3967.md) [šᵊmōnîm](../../strongs/h/h8084.md) and ['arbaʿ](../../strongs/h/h702.md).

<a name="nehemiah_11_19"></a>Nehemiah 11:19

Moreover the [šôʿēr](../../strongs/h/h7778.md), [ʿAqqûḇ](../../strongs/h/h6126.md), [Ṭalmôn](../../strongs/h/h2929.md), and their ['ach](../../strongs/h/h251.md) that [shamar](../../strongs/h/h8104.md) the [sha'ar](../../strongs/h/h8179.md), were a [mē'â](../../strongs/h/h3967.md) [šiḇʿîm](../../strongs/h/h7657.md) and [šᵊnayim](../../strongs/h/h8147.md).

<a name="nehemiah_11_20"></a>Nehemiah 11:20

And the [šᵊ'ār](../../strongs/h/h7605.md) of [Yisra'el](../../strongs/h/h3478.md), of the [kōhēn](../../strongs/h/h3548.md), and the [Lᵊvî](../../strongs/h/h3881.md), were in all the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), every ['iysh](../../strongs/h/h376.md) in his [nachalah](../../strongs/h/h5159.md).

<a name="nehemiah_11_21"></a>Nehemiah 11:21

But the [Nāṯîn](../../strongs/h/h5411.md) [yashab](../../strongs/h/h3427.md) in [ʿŌp̄El](../../strongs/h/h6077.md): and [Ṣyḥ'](../../strongs/h/h6727.md) and [Gišpā'](../../strongs/h/h1658.md) were over the [Nāṯîn](../../strongs/h/h5411.md).

<a name="nehemiah_11_22"></a>Nehemiah 11:22

The [pāqîḏ](../../strongs/h/h6496.md) also of the [Lᵊvî](../../strongs/h/h3881.md) at [Yĕruwshalaim](../../strongs/h/h3389.md) was [ʿUzzî](../../strongs/h/h5813.md) the [ben](../../strongs/h/h1121.md) of [Bānî](../../strongs/h/h1137.md), the [ben](../../strongs/h/h1121.md) of [Ḥăšaḇyâ](../../strongs/h/h2811.md), the [ben](../../strongs/h/h1121.md) of [Matanyâ](../../strongs/h/h4983.md), the [ben](../../strongs/h/h1121.md) of [Mîḵā'](../../strongs/h/h4316.md). Of the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md), the [shiyr](../../strongs/h/h7891.md) were [neḡeḏ](../../strongs/h/h5048.md) the [mĕla'kah](../../strongs/h/h4399.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="nehemiah_11_23"></a>Nehemiah 11:23

For it was the [melek](../../strongs/h/h4428.md) [mitsvah](../../strongs/h/h4687.md) concerning them, that a certain ['ămānâ](../../strongs/h/h548.md) should be for the [shiyr](../../strongs/h/h7891.md), [dabar](../../strongs/h/h1697.md) for every [yowm](../../strongs/h/h3117.md).

<a name="nehemiah_11_24"></a>Nehemiah 11:24

And [Pᵊṯaḥyâ](../../strongs/h/h6611.md) the [ben](../../strongs/h/h1121.md) of [Mᵊšêzaḇ'Ēl](../../strongs/h/h4898.md), of the [ben](../../strongs/h/h1121.md) of [Zeraḥ](../../strongs/h/h2226.md) the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md), was at the [melek](../../strongs/h/h4428.md) [yad](../../strongs/h/h3027.md) in all [dabar](../../strongs/h/h1697.md) concerning the ['am](../../strongs/h/h5971.md).

<a name="nehemiah_11_25"></a>Nehemiah 11:25

And for the [ḥāṣēr](../../strongs/h/h2691.md), with their [sadeh](../../strongs/h/h7704.md), some of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) [yashab](../../strongs/h/h3427.md) at [qiryaṯ 'arbaʿ](../../strongs/h/h7153.md), and in the [ḥāṣēr](../../strongs/h/h2691.md) thereof, and at [Dîḇōvn](../../strongs/h/h1769.md), and in the [bath](../../strongs/h/h1323.md) thereof, and at [Yᵊqaḇṣᵊ'Ēl](../../strongs/h/h3343.md), and in the [bath](../../strongs/h/h1323.md) thereof,

<a name="nehemiah_11_26"></a>Nehemiah 11:26

And at [Yēšûaʿ](../../strongs/h/h3442.md), and at [Môlāḏâ](../../strongs/h/h4137.md), and at [Bêṯ Peleṭ](../../strongs/h/h1046.md),

<a name="nehemiah_11_27"></a>Nehemiah 11:27

And at [Ḥăṣar ShÛʿĀl](../../strongs/h/h2705.md), and at [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md), and in the [bath](../../strongs/h/h1323.md) thereof,

<a name="nehemiah_11_28"></a>Nehemiah 11:28

And at [Ṣiqlāḡ](../../strongs/h/h6860.md), and at [Mᵊḵōnâ](../../strongs/h/h4368.md), and in the [bath](../../strongs/h/h1323.md) thereof,

<a name="nehemiah_11_29"></a>Nehemiah 11:29

And at [ʿÊn Rimmôn](../../strongs/h/h5884.md), and at [ṢārʿÂ](../../strongs/h/h6881.md), and at [Yarmûṯ](../../strongs/h/h3412.md),

<a name="nehemiah_11_30"></a>Nehemiah 11:30

[Zānôaḥ](../../strongs/h/h2182.md), [ʿĂḏullām](../../strongs/h/h5725.md), and in their [ḥāṣēr](../../strongs/h/h2691.md), at [Lāḵîš](../../strongs/h/h3923.md), and the [sadeh](../../strongs/h/h7704.md) thereof, at [ʿĂzēqâ](../../strongs/h/h5825.md), and in the [bath](../../strongs/h/h1323.md) thereof. And they [ḥānâ](../../strongs/h/h2583.md) from [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md) unto the [gay'](../../strongs/h/h1516.md) of [Hinnōm](../../strongs/h/h2011.md).

<a name="nehemiah_11_31"></a>Nehemiah 11:31

The [ben](../../strongs/h/h1121.md) also of [Binyāmîn](../../strongs/h/h1144.md) from [Geḇaʿ](../../strongs/h/h1387.md) dwelt at [Miḵmās](../../strongs/h/h4363.md), and [ʿAy](../../strongs/h/h5857.md), and [Bêṯ-'ēl](../../strongs/h/h1008.md), and in their [bath](../../strongs/h/h1323.md),

<a name="nehemiah_11_32"></a>Nehemiah 11:32

And at [ʿĂnāṯôṯ](../../strongs/h/h6068.md), [nōḇ](../../strongs/h/h5011.md), [ʿĂnanyâ](../../strongs/h/h6055.md),

<a name="nehemiah_11_33"></a>Nehemiah 11:33

[Ḥāṣôr](../../strongs/h/h2674.md), [rāmâ](../../strongs/h/h7414.md), [Gitayim](../../strongs/h/h1664.md),

<a name="nehemiah_11_34"></a>Nehemiah 11:34

[Ḥāḏîḏ](../../strongs/h/h2307.md), [ṢᵊḇōʿÎm](../../strongs/h/h6650.md), [Nᵊḇallāṭ](../../strongs/h/h5041.md),

<a name="nehemiah_11_35"></a>Nehemiah 11:35

[Lōḏ](../../strongs/h/h3850.md), and ['Ônô](../../strongs/h/h207.md), the [gay'](../../strongs/h/h1516.md) of [ḥereš](../../strongs/h/h2791.md) [Ḥărāšîm](../../strongs/h/h2798.md).

<a name="nehemiah_11_36"></a>Nehemiah 11:36

And of the [Lᵊvî](../../strongs/h/h3881.md) were [maḥălōqeṯ](../../strongs/h/h4256.md) in [Yehuwdah](../../strongs/h/h3063.md), and in [Binyāmîn](../../strongs/h/h1144.md).

---

[Transliteral Bible](../bible.md)

[Nehemiah](nehemiah.md)

[Nehemiah 10](nehemiah_10.md) - [Nehemiah 12](nehemiah_12.md)