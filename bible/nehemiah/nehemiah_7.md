# [Nehemiah 7](https://www.blueletterbible.org/kjv/nehemiah/7)

<a name="nehemiah_7_1"></a>Nehemiah 7:1

Now it came to pass, when the [ḥômâ](../../strongs/h/h2346.md) was [bānâ](../../strongs/h/h1129.md), and I had set ['amad](../../strongs/h/h5975.md) the [deleṯ](../../strongs/h/h1817.md), and the [šôʿēr](../../strongs/h/h7778.md) and the [shiyr](../../strongs/h/h7891.md) and the [Lᵊvî](../../strongs/h/h3881.md) were [paqad](../../strongs/h/h6485.md),

<a name="nehemiah_7_2"></a>Nehemiah 7:2

That I gave my ['ach](../../strongs/h/h251.md) [Ḥănānî](../../strongs/h/h2607.md), and [Ḥănanyâ](../../strongs/h/h2608.md) the [śar](../../strongs/h/h8269.md) of the [bîrâ](../../strongs/h/h1002.md), [tsavah](../../strongs/h/h6680.md) over [Yĕruwshalaim](../../strongs/h/h3389.md): for he was an ['emeth](../../strongs/h/h571.md) ['iysh](../../strongs/h/h376.md), and [yare'](../../strongs/h/h3372.md) ['Elohiym](../../strongs/h/h430.md) above [rab](../../strongs/h/h7227.md).

<a name="nehemiah_7_3"></a>Nehemiah 7:3

And I ['āmar](../../strongs/h/h559.md) unto them, Let not the [sha'ar](../../strongs/h/h8179.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) be [pāṯaḥ](../../strongs/h/h6605.md) until the [šemeš](../../strongs/h/h8121.md) be [ḥōm](../../strongs/h/h2527.md); and while they stand ['amad](../../strongs/h/h5975.md), let them [gûp̄](../../strongs/h/h1479.md) the [deleṯ](../../strongs/h/h1817.md), and ['āḥaz](../../strongs/h/h270.md) them: and ['amad](../../strongs/h/h5975.md) [mišmereṯ](../../strongs/h/h4931.md) of the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), every ['iysh](../../strongs/h/h376.md) in his [mišmār](../../strongs/h/h4929.md), and every ['iysh](../../strongs/h/h376.md) to be over against his [bayith](../../strongs/h/h1004.md).

<a name="nehemiah_7_4"></a>Nehemiah 7:4

Now the [ʿîr](../../strongs/h/h5892.md) was [rāḥāḇ](../../strongs/h/h7342.md) [yad](../../strongs/h/h3027.md) and [gadowl](../../strongs/h/h1419.md): but the ['am](../../strongs/h/h5971.md) were [mᵊʿaṭ](../../strongs/h/h4592.md) [tavek](../../strongs/h/h8432.md), and the [bayith](../../strongs/h/h1004.md) were not [bānâ](../../strongs/h/h1129.md).

<a name="nehemiah_7_5"></a>Nehemiah 7:5

And my ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) into mine [leb](../../strongs/h/h3820.md) to gather [qāḇaṣ](../../strongs/h/h6908.md) the [ḥōr](../../strongs/h/h2715.md), and the [sāḡān](../../strongs/h/h5461.md), and the ['am](../../strongs/h/h5971.md), that they might be reckoned by [yāḥaś](../../strongs/h/h3187.md). And I [māṣā'](../../strongs/h/h4672.md) a [sēp̄er](../../strongs/h/h5612.md) of the [yaḥaś](../../strongs/h/h3188.md) of them which [ʿālâ](../../strongs/h/h5927.md) at the [ri'šôn](../../strongs/h/h7223.md), and [māṣā'](../../strongs/h/h4672.md) [kāṯaḇ](../../strongs/h/h3789.md) therein,

<a name="nehemiah_7_6"></a>Nehemiah 7:6

These are the [ben](../../strongs/h/h1121.md) of the [mᵊḏînâ](../../strongs/h/h4082.md), that went [ʿālâ](../../strongs/h/h5927.md) out of the [šᵊḇî](../../strongs/h/h7628.md), of those that had been carried [gôlâ](../../strongs/h/h1473.md), whom [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) had carried [gālâ](../../strongs/h/h1540.md), and came [shuwb](../../strongs/h/h7725.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) and to [Yehuwdah](../../strongs/h/h3063.md), every ['iysh](../../strongs/h/h376.md) unto his [ʿîr](../../strongs/h/h5892.md);

<a name="nehemiah_7_7"></a>Nehemiah 7:7

Who [bow'](../../strongs/h/h935.md) with [Zᵊrubāḇel](../../strongs/h/h2216.md), [Yēšûaʿ](../../strongs/h/h3442.md), [Nᵊḥemyâ](../../strongs/h/h5166.md), [ʿĂzaryâ](../../strongs/h/h5838.md), [RaʿAmyâ](../../strongs/h/h7485.md), [Naḥămānî](../../strongs/h/h5167.md), [Mārdᵊḵay](../../strongs/h/h4782.md), [Bilšān](../../strongs/h/h1114.md), [Mispereṯ](../../strongs/h/h4559.md), [Biḡvay](../../strongs/h/h902.md), [Nᵊḥûm](../../strongs/h/h5149.md), [BaʿĂnâ](../../strongs/h/h1196.md). The [mispār](../../strongs/h/h4557.md), I say, of the ['enowsh](../../strongs/h/h582.md) of the ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md) was this;

<a name="nehemiah_7_8"></a>Nehemiah 7:8

The [ben](../../strongs/h/h1121.md) of [ParʿŠ](../../strongs/h/h6551.md), [šᵊnayim](../../strongs/h/h8147.md) ['elep̄](../../strongs/h/h505.md) a [mē'â](../../strongs/h/h3967.md) [šiḇʿîm](../../strongs/h/h7657.md) and [šᵊnayim](../../strongs/h/h8147.md).

<a name="nehemiah_7_9"></a>Nehemiah 7:9

The [ben](../../strongs/h/h1121.md) of [Šᵊp̄Aṭyâ](../../strongs/h/h8203.md), [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) [šiḇʿîm](../../strongs/h/h7657.md) and [šᵊnayim](../../strongs/h/h8147.md).

<a name="nehemiah_7_10"></a>Nehemiah 7:10

The [ben](../../strongs/h/h1121.md) of ['Āraḥ](../../strongs/h/h733.md), [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md) [ḥămisheem](../../strongs/h/h2572.md) and [šᵊnayim](../../strongs/h/h8147.md).

<a name="nehemiah_7_11"></a>Nehemiah 7:11

The [ben](../../strongs/h/h1121.md) of [Paḥaṯ Mô'Āḇ](../../strongs/h/h6355.md), of the [ben](../../strongs/h/h1121.md) of [Yēšûaʿ](../../strongs/h/h3442.md) and [Yô'āḇ](../../strongs/h/h3097.md), two ['elep̄](../../strongs/h/h505.md) and [šᵊmōnê](../../strongs/h/h8083.md) [mē'â](../../strongs/h/h3967.md) and [šᵊmōnê](../../strongs/h/h8083.md) [ʿeśer](../../strongs/h/h6240.md).

<a name="nehemiah_7_12"></a>Nehemiah 7:12

The [ben](../../strongs/h/h1121.md) of [ʿÊlām](../../strongs/h/h5867.md), an ['elep̄](../../strongs/h/h505.md) two [mē'â](../../strongs/h/h3967.md) [ḥămisheem](../../strongs/h/h2572.md) and ['arbaʿ](../../strongs/h/h702.md).

<a name="nehemiah_7_13"></a>Nehemiah 7:13

The [ben](../../strongs/h/h1121.md) of [Zatû'](../../strongs/h/h2240.md), [šᵊmōnê](../../strongs/h/h8083.md) [mē'â](../../strongs/h/h3967.md) ['arbāʿîm](../../strongs/h/h705.md) and [ḥāmēš](../../strongs/h/h2568.md).

<a name="nehemiah_7_14"></a>Nehemiah 7:14

The [ben](../../strongs/h/h1121.md) of [Zakay](../../strongs/h/h2140.md), [šeḇaʿ](../../strongs/h/h7651.md) [mē'â](../../strongs/h/h3967.md) and [šiššîm](../../strongs/h/h8346.md).

<a name="nehemiah_7_15"></a>Nehemiah 7:15

The [ben](../../strongs/h/h1121.md) of [Binnûy](../../strongs/h/h1131.md), [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md) ['arbāʿîm](../../strongs/h/h705.md) and [šᵊmōnê](../../strongs/h/h8083.md).

<a name="nehemiah_7_16"></a>Nehemiah 7:16

The [ben](../../strongs/h/h1121.md) of [Bēḇay](../../strongs/h/h893.md), [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md) [ʿeśrîm](../../strongs/h/h6242.md) and [šᵊmōnê](../../strongs/h/h8083.md).

<a name="nehemiah_7_17"></a>Nehemiah 7:17

The [ben](../../strongs/h/h1121.md) of [ʿAzgāḏ](../../strongs/h/h5803.md), [šᵊnayim](../../strongs/h/h8147.md) ['elep̄](../../strongs/h/h505.md) [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) [ʿeśrîm](../../strongs/h/h6242.md) and [šᵊnayim](../../strongs/h/h8147.md).

<a name="nehemiah_7_18"></a>Nehemiah 7:18

The [ben](../../strongs/h/h1121.md) of ['Ăḏōnîqām](../../strongs/h/h140.md), [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md) [šiššîm](../../strongs/h/h8346.md) and [šeḇaʿ](../../strongs/h/h7651.md).

<a name="nehemiah_7_19"></a>Nehemiah 7:19

The [ben](../../strongs/h/h1121.md) of [Biḡvay](../../strongs/h/h902.md), two ['elep̄](../../strongs/h/h505.md) [šiššîm](../../strongs/h/h8346.md) and [šeḇaʿ](../../strongs/h/h7651.md).

<a name="nehemiah_7_20"></a>Nehemiah 7:20

The [ben](../../strongs/h/h1121.md) of [ʿĀḏîn](../../strongs/h/h5720.md), [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md) [ḥămisheem](../../strongs/h/h2572.md) and [ḥāmēš](../../strongs/h/h2568.md).

<a name="nehemiah_7_21"></a>Nehemiah 7:21

The [ben](../../strongs/h/h1121.md) of ['Āṭēr](../../strongs/h/h333.md) of [Ḥizqîyâ](../../strongs/h/h2396.md), [tišʿîm](../../strongs/h/h8673.md) and [šᵊmōnê](../../strongs/h/h8083.md).

<a name="nehemiah_7_22"></a>Nehemiah 7:22

The [ben](../../strongs/h/h1121.md) of [Ḥāšum](../../strongs/h/h2828.md), [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) [ʿeśrîm](../../strongs/h/h6242.md) and [šᵊmōnê](../../strongs/h/h8083.md).

<a name="nehemiah_7_23"></a>Nehemiah 7:23

The [ben](../../strongs/h/h1121.md) of [Bēṣay](../../strongs/h/h1209.md), [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) [ʿeśrîm](../../strongs/h/h6242.md) and ['arbaʿ](../../strongs/h/h702.md).

<a name="nehemiah_7_24"></a>Nehemiah 7:24

The [ben](../../strongs/h/h1121.md) of [Ḥārîp̄](../../strongs/h/h2756.md), a [mē'â](../../strongs/h/h3967.md) and [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md).

<a name="nehemiah_7_25"></a>Nehemiah 7:25

The [ben](../../strongs/h/h1121.md) of [Giḇʿôn](../../strongs/h/h1391.md), [tišʿîm](../../strongs/h/h8673.md) and [ḥāmēš](../../strongs/h/h2568.md).

<a name="nehemiah_7_26"></a>Nehemiah 7:26

The ['enowsh](../../strongs/h/h582.md) of [Bêṯ leḥem](../../strongs/h/h1035.md) and [Nᵊṭōp̄Â](../../strongs/h/h5199.md), a [mē'â](../../strongs/h/h3967.md) [šᵊmōnîm](../../strongs/h/h8084.md) and [šᵊmōnê](../../strongs/h/h8083.md).

<a name="nehemiah_7_27"></a>Nehemiah 7:27

The ['enowsh](../../strongs/h/h582.md) of [ʿĂnāṯôṯ](../../strongs/h/h6068.md), a [mē'â](../../strongs/h/h3967.md) [ʿeśrîm](../../strongs/h/h6242.md) and [šᵊmōnê](../../strongs/h/h8083.md).

<a name="nehemiah_7_28"></a>Nehemiah 7:28

The ['enowsh](../../strongs/h/h582.md) of [Bêṯ-ʿAzmāveṯ](../../strongs/h/h1041.md), ['arbāʿîm](../../strongs/h/h705.md) and [šᵊnayim](../../strongs/h/h8147.md).

<a name="nehemiah_7_29"></a>Nehemiah 7:29

The ['enowsh](../../strongs/h/h582.md) of [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md), [Kᵊp̄Îrâ](../../strongs/h/h3716.md), and [Bᵊ'Ērôṯ](../../strongs/h/h881.md), [šeḇaʿ](../../strongs/h/h7651.md) [mē'â](../../strongs/h/h3967.md) ['arbāʿîm](../../strongs/h/h705.md) and [šālôš](../../strongs/h/h7969.md).

<a name="nehemiah_7_30"></a>Nehemiah 7:30

The ['enowsh](../../strongs/h/h582.md) of [rāmâ](../../strongs/h/h7414.md) and [Geḇaʿ](../../strongs/h/h1387.md), [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md) [ʿeśrîm](../../strongs/h/h6242.md) and ['echad](../../strongs/h/h259.md).

<a name="nehemiah_7_31"></a>Nehemiah 7:31

The ['enowsh](../../strongs/h/h582.md) of [Miḵmās](../../strongs/h/h4363.md), a [mē'â](../../strongs/h/h3967.md) and [ʿeśrîm](../../strongs/h/h6242.md) and [šᵊnayim](../../strongs/h/h8147.md).

<a name="nehemiah_7_32"></a>Nehemiah 7:32

The ['enowsh](../../strongs/h/h582.md) of [Bêṯ-'ēl](../../strongs/h/h1008.md) and [ʿAy](../../strongs/h/h5857.md), a [mē'â](../../strongs/h/h3967.md) [ʿeśrîm](../../strongs/h/h6242.md) and [šālôš](../../strongs/h/h7969.md).

<a name="nehemiah_7_33"></a>Nehemiah 7:33

The ['enowsh](../../strongs/h/h582.md) of the ['aḥēr](../../strongs/h/h312.md) [Nᵊḇô](../../strongs/h/h5015.md), [ḥămisheem](../../strongs/h/h2572.md) and [šᵊnayim](../../strongs/h/h8147.md).

<a name="nehemiah_7_34"></a>Nehemiah 7:34

The [ben](../../strongs/h/h1121.md) of the ['aḥēr](../../strongs/h/h312.md) [ʿÊlām](../../strongs/h/h5867.md), an ['elep̄](../../strongs/h/h505.md) two [mē'â](../../strongs/h/h3967.md) [ḥămisheem](../../strongs/h/h2572.md) and ['arbaʿ](../../strongs/h/h702.md).

<a name="nehemiah_7_35"></a>Nehemiah 7:35

The [ben](../../strongs/h/h1121.md) of [Ḥārim](../../strongs/h/h2766.md), [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) and [ʿeśrîm](../../strongs/h/h6242.md).

<a name="nehemiah_7_36"></a>Nehemiah 7:36

The [ben](../../strongs/h/h1121.md) of [Yᵊrēḥô](../../strongs/h/h3405.md), [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) ['arbāʿîm](../../strongs/h/h705.md) and [ḥāmēš](../../strongs/h/h2568.md).

<a name="nehemiah_7_37"></a>Nehemiah 7:37

The [ben](../../strongs/h/h1121.md) of [Lōḏ](../../strongs/h/h3850.md), [Ḥāḏîḏ](../../strongs/h/h2307.md), and ['Ônô](../../strongs/h/h207.md), [šeḇaʿ](../../strongs/h/h7651.md) [mē'â](../../strongs/h/h3967.md) [ʿeśrîm](../../strongs/h/h6242.md) and ['echad](../../strongs/h/h259.md).

<a name="nehemiah_7_38"></a>Nehemiah 7:38

The [ben](../../strongs/h/h1121.md) of [Sᵊnā'Â](../../strongs/h/h5570.md), [šālôš](../../strongs/h/h7969.md) ['elep̄](../../strongs/h/h505.md) [tēšaʿ](../../strongs/h/h8672.md) [mē'â](../../strongs/h/h3967.md) and [šᵊlōšîm](../../strongs/h/h7970.md).

<a name="nehemiah_7_39"></a>Nehemiah 7:39

The [kōhēn](../../strongs/h/h3548.md): the [ben](../../strongs/h/h1121.md) of [YᵊḏaʿYâ](../../strongs/h/h3048.md), of the [bayith](../../strongs/h/h1004.md) of [Yēšûaʿ](../../strongs/h/h3442.md), [tēšaʿ](../../strongs/h/h8672.md) [mē'â](../../strongs/h/h3967.md) [šiḇʿîm](../../strongs/h/h7657.md) and [šālôš](../../strongs/h/h7969.md).

<a name="nehemiah_7_40"></a>Nehemiah 7:40

The [ben](../../strongs/h/h1121.md) of ['Immēr](../../strongs/h/h564.md), an ['elep̄](../../strongs/h/h505.md) [ḥămisheem](../../strongs/h/h2572.md) and [šᵊnayim](../../strongs/h/h8147.md).

<a name="nehemiah_7_41"></a>Nehemiah 7:41

The [ben](../../strongs/h/h1121.md) of [Pašḥûr](../../strongs/h/h6583.md), an ['elep̄](../../strongs/h/h505.md) two [mē'â](../../strongs/h/h3967.md) ['arbāʿîm](../../strongs/h/h705.md) and [šeḇaʿ](../../strongs/h/h7651.md).

<a name="nehemiah_7_42"></a>Nehemiah 7:42

The [ben](../../strongs/h/h1121.md) of [Ḥārim](../../strongs/h/h2766.md), an ['elep̄](../../strongs/h/h505.md) and [šeḇaʿ](../../strongs/h/h7651.md) [ʿeśer](../../strongs/h/h6240.md).

<a name="nehemiah_7_43"></a>Nehemiah 7:43

The [Lᵊvî](../../strongs/h/h3881.md): the [ben](../../strongs/h/h1121.md) of [Yēšûaʿ](../../strongs/h/h3442.md), of [Qaḏmî'Ēl](../../strongs/h/h6934.md), and of the [ben](../../strongs/h/h1121.md) of [Hôḏyâ](../../strongs/h/h1937.md), [šiḇʿîm](../../strongs/h/h7657.md) and ['arbaʿ](../../strongs/h/h702.md).

<a name="nehemiah_7_44"></a>Nehemiah 7:44

The [shiyr](../../strongs/h/h7891.md): the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md), a [mē'â](../../strongs/h/h3967.md) ['arbāʿîm](../../strongs/h/h705.md) and [šᵊmōnê](../../strongs/h/h8083.md).

<a name="nehemiah_7_45"></a>Nehemiah 7:45

The [šôʿēr](../../strongs/h/h7778.md): the [ben](../../strongs/h/h1121.md) of [Šallûm](../../strongs/h/h7967.md), the [ben](../../strongs/h/h1121.md) of ['Āṭēr](../../strongs/h/h333.md), the [ben](../../strongs/h/h1121.md) of [Ṭalmôn](../../strongs/h/h2929.md), the [ben](../../strongs/h/h1121.md) of [ʿAqqûḇ](../../strongs/h/h6126.md), the [ben](../../strongs/h/h1121.md) of [Ḥăṭîṭā'](../../strongs/h/h2410.md), the [ben](../../strongs/h/h1121.md) of [Šōḇay](../../strongs/h/h7630.md), a [mē'â](../../strongs/h/h3967.md) [šᵊlōšîm](../../strongs/h/h7970.md) and [šᵊmōnê](../../strongs/h/h8083.md).

<a name="nehemiah_7_46"></a>Nehemiah 7:46

The [Nāṯîn](../../strongs/h/h5411.md): the [ben](../../strongs/h/h1121.md) of [Ṣyḥ'](../../strongs/h/h6727.md), the [ben](../../strongs/h/h1121.md) of [Ḥăśûp̄Ā'](../../strongs/h/h2817.md), the [ben](../../strongs/h/h1121.md) of [ṬabāʿÔṯ](../../strongs/h/h2884.md),

<a name="nehemiah_7_47"></a>Nehemiah 7:47

The [ben](../../strongs/h/h1121.md) of [Qêrōs](../../strongs/h/h7026.md), the [ben](../../strongs/h/h1121.md) of [SîʿĀ'](../../strongs/h/h5517.md), the [ben](../../strongs/h/h1121.md) of [Pāḏôn](../../strongs/h/h6303.md),

<a name="nehemiah_7_48"></a>Nehemiah 7:48

The [ben](../../strongs/h/h1121.md) of [Lᵊḇānâ](../../strongs/h/h3838.md), the [ben](../../strongs/h/h1121.md) of [Ḥăḡāḇā'](../../strongs/h/h2286.md), the [ben](../../strongs/h/h1121.md) of [Śalmay](../../strongs/h/h8014.md),

<a name="nehemiah_7_49"></a>Nehemiah 7:49

The [ben](../../strongs/h/h1121.md) of [Ḥānān](../../strongs/h/h2605.md), the [ben](../../strongs/h/h1121.md) of [Gidēl](../../strongs/h/h1435.md), the [ben](../../strongs/h/h1121.md) of [Gaḥar](../../strongs/h/h1515.md),

<a name="nehemiah_7_50"></a>Nehemiah 7:50

The [ben](../../strongs/h/h1121.md) of [Rᵊ'Āyâ](../../strongs/h/h7211.md), the [ben](../../strongs/h/h1121.md) of [Rᵊṣîn](../../strongs/h/h7526.md), the [ben](../../strongs/h/h1121.md) of [Nᵊqôḏā'](../../strongs/h/h5353.md),

<a name="nehemiah_7_51"></a>Nehemiah 7:51

The [ben](../../strongs/h/h1121.md) of [Gazzām](../../strongs/h/h1502.md), the [ben](../../strongs/h/h1121.md) of [ʿUzzā'](../../strongs/h/h5798.md), the [ben](../../strongs/h/h1121.md) of [Pāsēaḥ](../../strongs/h/h6454.md),

<a name="nehemiah_7_52"></a>Nehemiah 7:52

The [ben](../../strongs/h/h1121.md) of [Bēsay](../../strongs/h/h1153.md), the [ben](../../strongs/h/h1121.md) of [MᵊʿYny](../../strongs/h/h4586.md), the [ben](../../strongs/h/h1121.md) of [nᵊp̄ûšsîm](../../strongs/h/h5300.md) H5304,

<a name="nehemiah_7_53"></a>Nehemiah 7:53

The [ben](../../strongs/h/h1121.md) of [Baqbûq](../../strongs/h/h1227.md), the [ben](../../strongs/h/h1121.md) of [Ḥăqûp̄Ā'](../../strongs/h/h2709.md), the [ben](../../strongs/h/h1121.md) of [Ḥarḥûr](../../strongs/h/h2744.md),

<a name="nehemiah_7_54"></a>Nehemiah 7:54

The [ben](../../strongs/h/h1121.md) of [Baṣlûṯ](../../strongs/h/h1213.md), the [ben](../../strongs/h/h1121.md) of [Mᵊḥîḏā'](../../strongs/h/h4240.md), the [ben](../../strongs/h/h1121.md) of [Ḥaršā'](../../strongs/h/h2797.md),

<a name="nehemiah_7_55"></a>Nehemiah 7:55

The [ben](../../strongs/h/h1121.md) of [Barqôs](../../strongs/h/h1302.md), the [ben](../../strongs/h/h1121.md) of [Sîsrā'](../../strongs/h/h5516.md), the [ben](../../strongs/h/h1121.md) of [Temaḥ](../../strongs/h/h8547.md),

<a name="nehemiah_7_56"></a>Nehemiah 7:56

The [ben](../../strongs/h/h1121.md) of [Nᵊṣîaḥ](../../strongs/h/h5335.md), the [ben](../../strongs/h/h1121.md) of [Ḥăṭîp̄Ā'](../../strongs/h/h2412.md).

<a name="nehemiah_7_57"></a>Nehemiah 7:57

The [ben](../../strongs/h/h1121.md) of [Šᵊlōmô](../../strongs/h/h8010.md) ['ebed](../../strongs/h/h5650.md): the [ben](../../strongs/h/h1121.md) of [Sôṭay](../../strongs/h/h5479.md), the [ben](../../strongs/h/h1121.md) of [Sōp̄Ereṯ](../../strongs/h/h5618.md), the [ben](../../strongs/h/h1121.md) of [Pᵊrûḏā'](../../strongs/h/h6514.md),

<a name="nehemiah_7_58"></a>Nehemiah 7:58

The [ben](../../strongs/h/h1121.md) of [YaʿĂlā'](../../strongs/h/h3279.md), the [ben](../../strongs/h/h1121.md) of [Darqôn](../../strongs/h/h1874.md), the [ben](../../strongs/h/h1121.md) of [Gidēl](../../strongs/h/h1435.md),

<a name="nehemiah_7_59"></a>Nehemiah 7:59

The [ben](../../strongs/h/h1121.md) of [Šᵊp̄Aṭyâ](../../strongs/h/h8203.md), the [ben](../../strongs/h/h1121.md) of [Ḥaṭṭîl](../../strongs/h/h2411.md), the [ben](../../strongs/h/h1121.md) of Pochereth of [Pōḵereṯ Haṣṣᵊḇāyîm](../../strongs/h/h6380.md), the [ben](../../strongs/h/h1121.md) of ['Āmôn](../../strongs/h/h526.md).

<a name="nehemiah_7_60"></a>Nehemiah 7:60

All the [Nāṯîn](../../strongs/h/h5411.md), and the [ben](../../strongs/h/h1121.md) of [Šᵊlōmô](../../strongs/h/h8010.md) ['ebed](../../strongs/h/h5650.md), were [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) [tišʿîm](../../strongs/h/h8673.md) and [šᵊnayim](../../strongs/h/h8147.md).

<a name="nehemiah_7_61"></a>Nehemiah 7:61

And these were they which went [ʿālâ](../../strongs/h/h5927.md) also from [Tēl Melaḥ](../../strongs/h/h8528.md), [Tēl Ḥaršā'](../../strongs/h/h8521.md), [Kᵊrûḇ](../../strongs/h/h3743.md), ['āḏôn](../../strongs/h/h114.md), and ['Immēr](../../strongs/h/h564.md): but they [yakol](../../strongs/h/h3201.md) not [nāḡaḏ](../../strongs/h/h5046.md) their ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), nor their [zera'](../../strongs/h/h2233.md), whether they were of [Yisra'el](../../strongs/h/h3478.md).

<a name="nehemiah_7_62"></a>Nehemiah 7:62

The [ben](../../strongs/h/h1121.md) of [Dᵊlāyâ](../../strongs/h/h1806.md), the [ben](../../strongs/h/h1121.md) of [Ṭôḇîyâ](../../strongs/h/h2900.md), the [ben](../../strongs/h/h1121.md) of [Nᵊqôḏā'](../../strongs/h/h5353.md), [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md) ['arbāʿîm](../../strongs/h/h705.md) and [šᵊnayim](../../strongs/h/h8147.md).

<a name="nehemiah_7_63"></a>Nehemiah 7:63

And of the [kōhēn](../../strongs/h/h3548.md): the [ben](../../strongs/h/h1121.md) of [Ḥŏḇāyâ](../../strongs/h/h2252.md), the [ben](../../strongs/h/h1121.md) of [Qôṣ](../../strongs/h/h6976.md), the [ben](../../strongs/h/h1121.md) of [Barzillay](../../strongs/h/h1271.md), which [laqach](../../strongs/h/h3947.md) one of the [bath](../../strongs/h/h1323.md) of [Barzillay](../../strongs/h/h1271.md) the [Gilʿāḏî](../../strongs/h/h1569.md) to ['ishshah](../../strongs/h/h802.md), and was [qara'](../../strongs/h/h7121.md) after their [shem](../../strongs/h/h8034.md).

<a name="nehemiah_7_64"></a>Nehemiah 7:64

These [bāqaš](../../strongs/h/h1245.md) their [kᵊṯāḇ](../../strongs/h/h3791.md) among those that were reckoned by [yāḥaś](../../strongs/h/h3187.md), but it was not [māṣā'](../../strongs/h/h4672.md): therefore were they, as [gā'al](../../strongs/h/h1351.md), put from the [kᵊhunnâ](../../strongs/h/h3550.md).

<a name="nehemiah_7_65"></a>Nehemiah 7:65

And the [tiršāṯā'](../../strongs/h/h8660.md) ['āmar](../../strongs/h/h559.md) unto them, that they should not ['akal](../../strongs/h/h398.md) of the [qodesh](../../strongs/h/h6944.md) holy [qodesh](../../strongs/h/h6944.md), till there ['amad](../../strongs/h/h5975.md) up a [kōhēn](../../strongs/h/h3548.md) with ['Ûrîm](../../strongs/h/h224.md) and [Tummîm](../../strongs/h/h8550.md).

<a name="nehemiah_7_66"></a>Nehemiah 7:66

The whole [qāhēl](../../strongs/h/h6951.md) ['echad](../../strongs/h/h259.md) was ['arbaʿ](../../strongs/h/h702.md) [ribô'](../../strongs/h/h7239.md) and two ['elep̄](../../strongs/h/h505.md) [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) and [šiššîm](../../strongs/h/h8346.md),

<a name="nehemiah_7_67"></a>Nehemiah 7:67

Beside their ['ebed](../../strongs/h/h5650.md) and their ['amah](../../strongs/h/h519.md), of whom there were [šeḇaʿ](../../strongs/h/h7651.md) ['elep̄](../../strongs/h/h505.md) [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) [šᵊlōšîm](../../strongs/h/h7970.md) and [šeḇaʿ](../../strongs/h/h7651.md): and they had two [mē'â](../../strongs/h/h3967.md) ['arbāʿîm](../../strongs/h/h705.md) and [ḥāmēš](../../strongs/h/h2568.md) singing [shiyr](../../strongs/h/h7891.md) and singing [shiyr](../../strongs/h/h7891.md).

<a name="nehemiah_7_68"></a>Nehemiah 7:68

Their [sûs](../../strongs/h/h5483.md), [šeḇaʿ](../../strongs/h/h7651.md) [mē'â](../../strongs/h/h3967.md) [šᵊlōšîm](../../strongs/h/h7970.md) and [šēš](../../strongs/h/h8337.md): their [pereḏ](../../strongs/h/h6505.md), two [mē'â](../../strongs/h/h3967.md) ['arbāʿîm](../../strongs/h/h705.md) and [ḥāmēš](../../strongs/h/h2568.md):

<a name="nehemiah_7_69"></a>Nehemiah 7:69

Their [gāmāl](../../strongs/h/h1581.md), ['arbaʿ](../../strongs/h/h702.md) [mē'â](../../strongs/h/h3967.md) [šᵊlōšîm](../../strongs/h/h7970.md) and [ḥāmēš](../../strongs/h/h2568.md): [šēš](../../strongs/h/h8337.md) ['elep̄](../../strongs/h/h505.md) [šeḇaʿ](../../strongs/h/h7651.md) [mē'â](../../strongs/h/h3967.md) and [ʿeśrîm](../../strongs/h/h6242.md) [chamowr](../../strongs/h/h2543.md).

<a name="nehemiah_7_70"></a>Nehemiah 7:70

And some [qᵊṣāṯ](../../strongs/h/h7117.md) the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) [nathan](../../strongs/h/h5414.md) unto the [mĕla'kah](../../strongs/h/h4399.md). The [tiršāṯā'](../../strongs/h/h8660.md) [nathan](../../strongs/h/h5414.md) to the ['ôṣār](../../strongs/h/h214.md) an ['elep̄](../../strongs/h/h505.md) [darkᵊmôn](../../strongs/h/h1871.md) of [zāhāḇ](../../strongs/h/h2091.md), [ḥămisheem](../../strongs/h/h2572.md) [mizrāq](../../strongs/h/h4219.md), [ḥāmēš](../../strongs/h/h2568.md) [mē'â](../../strongs/h/h3967.md) and [šᵊlōšîm](../../strongs/h/h7970.md) [kōhēn](../../strongs/h/h3548.md) [kĕthoneth](../../strongs/h/h3801.md).

<a name="nehemiah_7_71"></a>Nehemiah 7:71

And some of the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) [nathan](../../strongs/h/h5414.md) to the ['ôṣār](../../strongs/h/h214.md) of the [mĕla'kah](../../strongs/h/h4399.md) [šᵊnayim](../../strongs/h/h8147.md) [ribô'](../../strongs/h/h7239.md) [darkᵊmôn](../../strongs/h/h1871.md) of [zāhāḇ](../../strongs/h/h2091.md), and two ['elep̄](../../strongs/h/h505.md) and two [mē'â](../../strongs/h/h3967.md) [mānê](../../strongs/h/h4488.md) of [keceph](../../strongs/h/h3701.md).

<a name="nehemiah_7_72"></a>Nehemiah 7:72

And that which the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the ['am](../../strongs/h/h5971.md) [nathan](../../strongs/h/h5414.md) was [šᵊnayim](../../strongs/h/h8147.md) [ribô'](../../strongs/h/h7239.md) [darkᵊmôn](../../strongs/h/h1871.md) of [zāhāḇ](../../strongs/h/h2091.md), and two ['elep̄](../../strongs/h/h505.md) [mānê](../../strongs/h/h4488.md) of [keceph](../../strongs/h/h3701.md), and [šiššîm](../../strongs/h/h8346.md) and [šeḇaʿ](../../strongs/h/h7651.md) [kōhēn](../../strongs/h/h3548.md) [kĕthoneth](../../strongs/h/h3801.md).

<a name="nehemiah_7_73"></a>Nehemiah 7:73

So the [kōhēn](../../strongs/h/h3548.md), and the [Lᵊvî](../../strongs/h/h3881.md), and the [šôʿēr](../../strongs/h/h7778.md), and the [shiyr](../../strongs/h/h7891.md), and some of the ['am](../../strongs/h/h5971.md), and the [Nāṯîn](../../strongs/h/h5411.md), and all [Yisra'el](../../strongs/h/h3478.md), [yashab](../../strongs/h/h3427.md) in their [ʿîr](../../strongs/h/h5892.md); and when the [šᵊḇîʿî](../../strongs/h/h7637.md) [ḥōḏeš](../../strongs/h/h2320.md) [naga'](../../strongs/h/h5060.md), the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were in their [ʿîr](../../strongs/h/h5892.md).

---

[Transliteral Bible](../bible.md)

[Nehemiah](nehemiah.md)

[Nehemiah 6](nehemiah_6.md) - [Nehemiah 8](nehemiah_8.md)