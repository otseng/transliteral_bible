# [Nehemiah 8](https://www.blueletterbible.org/kjv/nehemiah/8)

<a name="nehemiah_8_1"></a>Nehemiah 8:1

And all the ['am](../../strongs/h/h5971.md) gathered themselves ['āsap̄](../../strongs/h/h622.md) as ['echad](../../strongs/h/h259.md) ['iysh](../../strongs/h/h376.md) into the [rᵊḥōḇ](../../strongs/h/h7339.md) that was [paniym](../../strongs/h/h6440.md) the [mayim](../../strongs/h/h4325.md) [sha'ar](../../strongs/h/h8179.md); and they ['āmar](../../strongs/h/h559.md) unto [ʿEzrā'](../../strongs/h/h5830.md) the [sāp̄ar](../../strongs/h/h5608.md) to [bow'](../../strongs/h/h935.md) the [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md), which [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md) to [Yisra'el](../../strongs/h/h3478.md).

<a name="nehemiah_8_2"></a>Nehemiah 8:2

And [ʿEzrā'](../../strongs/h/h5830.md) the [kōhēn](../../strongs/h/h3548.md) [bow'](../../strongs/h/h935.md) the [towrah](../../strongs/h/h8451.md) [paniym](../../strongs/h/h6440.md) the [qāhēl](../../strongs/h/h6951.md) both of ['iysh](../../strongs/h/h376.md) and ['ishshah](../../strongs/h/h802.md), and all that could [shama'](../../strongs/h/h8085.md) with [bîn](../../strongs/h/h995.md), upon the ['echad](../../strongs/h/h259.md) [yowm](../../strongs/h/h3117.md) of the [šᵊḇîʿî](../../strongs/h/h7637.md) [ḥōḏeš](../../strongs/h/h2320.md).

<a name="nehemiah_8_3"></a>Nehemiah 8:3

And he [qara'](../../strongs/h/h7121.md) therein [paniym](../../strongs/h/h6440.md) the [rᵊḥōḇ](../../strongs/h/h7339.md) that was [paniym](../../strongs/h/h6440.md) the [mayim](../../strongs/h/h4325.md) [sha'ar](../../strongs/h/h8179.md) from the ['owr](../../strongs/h/h216.md) until [maḥăṣîṯ](../../strongs/h/h4276.md) [yowm](../../strongs/h/h3117.md), before the ['enowsh](../../strongs/h/h582.md) and the ['ishshah](../../strongs/h/h802.md), and those that could [bîn](../../strongs/h/h995.md); and the ['ozen](../../strongs/h/h241.md) of all the ['am](../../strongs/h/h5971.md) were attentive unto the [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md).

<a name="nehemiah_8_4"></a>Nehemiah 8:4

And [ʿEzrā'](../../strongs/h/h5830.md) the [sāp̄ar](../../strongs/h/h5608.md) ['amad](../../strongs/h/h5975.md) upon a [miḡdāl](../../strongs/h/h4026.md) of ['ets](../../strongs/h/h6086.md), which they had ['asah](../../strongs/h/h6213.md) for the [dabar](../../strongs/h/h1697.md); and ['ēṣel](../../strongs/h/h681.md) him ['amad](../../strongs/h/h5975.md) [Mataṯyâ](../../strongs/h/h4993.md), and [Šemaʿ](../../strongs/h/h8087.md), and [ʿĂnāyâ](../../strongs/h/h6043.md), and ['Ûrîyâ](../../strongs/h/h223.md), and [Ḥilqîyâ](../../strongs/h/h2518.md), and [MaʿĂśêâ](../../strongs/h/h4641.md), on his [yamiyn](../../strongs/h/h3225.md); and on his left [śᵊmō'l](../../strongs/h/h8040.md), [Pᵊḏāyâ](../../strongs/h/h6305.md), and [Mîšā'ēl](../../strongs/h/h4332.md), and [Malkîyâ](../../strongs/h/h4441.md), and [Ḥāšum](../../strongs/h/h2828.md), and [Ḥašḇadānâ](../../strongs/h/h2806.md), [Zᵊḵaryâ](../../strongs/h/h2148.md), and [Mᵊšullām](../../strongs/h/h4918.md).

<a name="nehemiah_8_5"></a>Nehemiah 8:5

And [ʿEzrā'](../../strongs/h/h5830.md) [pāṯaḥ](../../strongs/h/h6605.md) the [sēp̄er](../../strongs/h/h5612.md) in the ['ayin](../../strongs/h/h5869.md) of all the ['am](../../strongs/h/h5971.md); (for he was above all the ['am](../../strongs/h/h5971.md);) and when he [pāṯaḥ](../../strongs/h/h6605.md) it, all the ['am](../../strongs/h/h5971.md) stood ['amad](../../strongs/h/h5975.md):

<a name="nehemiah_8_6"></a>Nehemiah 8:6

And [ʿEzrā'](../../strongs/h/h5830.md) [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md), the [gadowl](../../strongs/h/h1419.md) ['Elohiym](../../strongs/h/h430.md). And all the ['am](../../strongs/h/h5971.md) ['anah](../../strongs/h/h6030.md), ['amen](../../strongs/h/h543.md), ['amen](../../strongs/h/h543.md), with lifting [mōʿal](../../strongs/h/h4607.md) their [yad](../../strongs/h/h3027.md): and they [qāḏaḏ](../../strongs/h/h6915.md) their heads, and [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md) with their ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md).

<a name="nehemiah_8_7"></a>Nehemiah 8:7

Also [Yēšûaʿ](../../strongs/h/h3442.md), and [Bānî](../../strongs/h/h1137.md), and [Šērēḇyâ](../../strongs/h/h8274.md), [yāmîn](../../strongs/h/h3226.md), [ʿAqqûḇ](../../strongs/h/h6126.md), [Šabṯay](../../strongs/h/h7678.md), [Hôḏîyâ](../../strongs/h/h1941.md), [MaʿĂśêâ](../../strongs/h/h4641.md), [Qᵊlîṭā'](../../strongs/h/h7042.md), [ʿĂzaryâ](../../strongs/h/h5838.md), [Yôzāḇāḏ](../../strongs/h/h3107.md), [Ḥānān](../../strongs/h/h2605.md), [Pᵊlāyâ](../../strongs/h/h6411.md), and the [Lᵊvî](../../strongs/h/h3881.md), caused the ['am](../../strongs/h/h5971.md) to [bîn](../../strongs/h/h995.md) the [towrah](../../strongs/h/h8451.md): and the ['am](../../strongs/h/h5971.md) stood in their [ʿōmeḏ](../../strongs/h/h5977.md).

<a name="nehemiah_8_8"></a>Nehemiah 8:8

So they [qara'](../../strongs/h/h7121.md) in the [sēp̄er](../../strongs/h/h5612.md) in the [towrah](../../strongs/h/h8451.md) of ['Elohiym](../../strongs/h/h430.md) [pāraš](../../strongs/h/h6567.md), and [śûm](../../strongs/h/h7760.md) the [śēḵel](../../strongs/h/h7922.md), and caused them to [bîn](../../strongs/h/h995.md) the [miqrā'](../../strongs/h/h4744.md).

<a name="nehemiah_8_9"></a>Nehemiah 8:9

And [Nᵊḥemyâ](../../strongs/h/h5166.md), which is the [tiršāṯā'](../../strongs/h/h8660.md), and [ʿEzrā'](../../strongs/h/h5830.md) the [kōhēn](../../strongs/h/h3548.md) the [sāp̄ar](../../strongs/h/h5608.md), and the [Lᵊvî](../../strongs/h/h3881.md) that [bîn](../../strongs/h/h995.md) the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md) unto all the ['am](../../strongs/h/h5971.md), This [yowm](../../strongs/h/h3117.md) is [qadowsh](../../strongs/h/h6918.md) unto [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md); ['āḇal](../../strongs/h/h56.md) not, nor [bāḵâ](../../strongs/h/h1058.md). For all the ['am](../../strongs/h/h5971.md) [bāḵâ](../../strongs/h/h1058.md), when they [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of the [towrah](../../strongs/h/h8451.md).

<a name="nehemiah_8_10"></a>Nehemiah 8:10

Then he ['āmar](../../strongs/h/h559.md) unto them, Go your [yālaḵ](../../strongs/h/h3212.md), ['akal](../../strongs/h/h398.md) the [mišmān](../../strongs/h/h4924.md), and [šāṯâ](../../strongs/h/h8354.md) the [mamtaqqîm](../../strongs/h/h4477.md), and [shalach](../../strongs/h/h7971.md) [mānâ](../../strongs/h/h4490.md) unto them for whom nothing is [kuwn](../../strongs/h/h3559.md): for this [yowm](../../strongs/h/h3117.md) is [qadowsh](../../strongs/h/h6918.md) unto our ['adown](../../strongs/h/h113.md): neither be ye [ʿāṣaḇ](../../strongs/h/h6087.md); for the [ḥeḏvâ](../../strongs/h/h2304.md) of [Yĕhovah](../../strongs/h/h3068.md) is your [māʿôz](../../strongs/h/h4581.md).

<a name="nehemiah_8_11"></a>Nehemiah 8:11

So the [Lᵊvî](../../strongs/h/h3881.md) [ḥāšâ](../../strongs/h/h2814.md) all the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), Hold your [hāsâ](../../strongs/h/h2013.md), for the [yowm](../../strongs/h/h3117.md) is [qadowsh](../../strongs/h/h6918.md); neither be ye [ʿāṣaḇ](../../strongs/h/h6087.md).

<a name="nehemiah_8_12"></a>Nehemiah 8:12

And all the ['am](../../strongs/h/h5971.md) went their [yālaḵ](../../strongs/h/h3212.md) to ['akal](../../strongs/h/h398.md), and to [šāṯâ](../../strongs/h/h8354.md), and to [shalach](../../strongs/h/h7971.md) [mānâ](../../strongs/h/h4490.md), and to ['asah](../../strongs/h/h6213.md) [gadowl](../../strongs/h/h1419.md) [simchah](../../strongs/h/h8057.md), because they had [bîn](../../strongs/h/h995.md) the [dabar](../../strongs/h/h1697.md) that were [yada'](../../strongs/h/h3045.md) unto them.

<a name="nehemiah_8_13"></a>Nehemiah 8:13

And on the [šēnî](../../strongs/h/h8145.md) [yowm](../../strongs/h/h3117.md) were gathered ['āsap̄](../../strongs/h/h622.md) the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of all the ['am](../../strongs/h/h5971.md), the [kōhēn](../../strongs/h/h3548.md), and the [Lᵊvî](../../strongs/h/h3881.md), unto [ʿEzrā'](../../strongs/h/h5830.md) the [sāp̄ar](../../strongs/h/h5608.md), even to [sakal](../../strongs/h/h7919.md) the [dabar](../../strongs/h/h1697.md) of the [towrah](../../strongs/h/h8451.md).

<a name="nehemiah_8_14"></a>Nehemiah 8:14

And they [māṣā'](../../strongs/h/h4672.md) [kāṯaḇ](../../strongs/h/h3789.md) in the [towrah](../../strongs/h/h8451.md) which [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md) [yad](../../strongs/h/h3027.md) [Mōshe](../../strongs/h/h4872.md), that the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) should [yashab](../../strongs/h/h3427.md) in [cukkah](../../strongs/h/h5521.md) in the [ḥāḡ](../../strongs/h/h2282.md) of the [šᵊḇîʿî](../../strongs/h/h7637.md) [ḥōḏeš](../../strongs/h/h2320.md):

<a name="nehemiah_8_15"></a>Nehemiah 8:15

And that they should [shama'](../../strongs/h/h8085.md) and ['abar](../../strongs/h/h5674.md) [qowl](../../strongs/h/h6963.md) in all their [ʿîr](../../strongs/h/h5892.md), and in [Yĕruwshalaim](../../strongs/h/h3389.md), ['āmar](../../strongs/h/h559.md), Go [yāṣā'](../../strongs/h/h3318.md) unto the [har](../../strongs/h/h2022.md), and [bow'](../../strongs/h/h935.md) [zayiṯ](../../strongs/h/h2132.md) ['aleh](../../strongs/h/h5929.md), and [šemen](../../strongs/h/h8081.md) ['ets](../../strongs/h/h6086.md) ['aleh](../../strongs/h/h5929.md), and [hăḏas](../../strongs/h/h1918.md) ['aleh](../../strongs/h/h5929.md), and [Tāmār](../../strongs/h/h8558.md) ['aleh](../../strongs/h/h5929.md), and ['aleh](../../strongs/h/h5929.md) of [ʿāḇōṯ](../../strongs/h/h5687.md) ['ets](../../strongs/h/h6086.md), to ['asah](../../strongs/h/h6213.md) [cukkah](../../strongs/h/h5521.md), as it is [kāṯaḇ](../../strongs/h/h3789.md).

<a name="nehemiah_8_16"></a>Nehemiah 8:16

So the ['am](../../strongs/h/h5971.md) [yāṣā'](../../strongs/h/h3318.md), and [bow'](../../strongs/h/h935.md) them, and ['asah](../../strongs/h/h6213.md) themselves [cukkah](../../strongs/h/h5521.md), every ['iysh](../../strongs/h/h376.md) upon the roof of his [gāḡ](../../strongs/h/h1406.md), and in their [ḥāṣēr](../../strongs/h/h2691.md), and in the [ḥāṣēr](../../strongs/h/h2691.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), and in the [rᵊḥōḇ](../../strongs/h/h7339.md) of the [mayim](../../strongs/h/h4325.md) [sha'ar](../../strongs/h/h8179.md), and in the [rᵊḥōḇ](../../strongs/h/h7339.md) of the [sha'ar](../../strongs/h/h8179.md) of ['Ep̄rayim](../../strongs/h/h669.md).

<a name="nehemiah_8_17"></a>Nehemiah 8:17

And all the [qāhēl](../../strongs/h/h6951.md) of them that were [shuwb](../../strongs/h/h7725.md) out of the [šᵊḇî](../../strongs/h/h7628.md) ['asah](../../strongs/h/h6213.md) [cukkah](../../strongs/h/h5521.md), and [yashab](../../strongs/h/h3427.md) under the [cukkah](../../strongs/h/h5521.md): for since the [yowm](../../strongs/h/h3117.md) of [Yēšûaʿ](../../strongs/h/h3442.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md) unto that [yowm](../../strongs/h/h3117.md) had not the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) done ['asah](../../strongs/h/h6213.md). And there was [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) [simchah](../../strongs/h/h8057.md).

<a name="nehemiah_8_18"></a>Nehemiah 8:18

Also [yowm](../../strongs/h/h3117.md) by [yowm](../../strongs/h/h3117.md), from the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) unto the ['aḥărôn](../../strongs/h/h314.md) [yowm](../../strongs/h/h3117.md), he [qara'](../../strongs/h/h7121.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md) of ['Elohiym](../../strongs/h/h430.md). And they ['asah](../../strongs/h/h6213.md) the [ḥāḡ](../../strongs/h/h2282.md) [šeḇaʿ](../../strongs/h/h7651.md) [yowm](../../strongs/h/h3117.md); and on the [šᵊmînî](../../strongs/h/h8066.md) [yowm](../../strongs/h/h3117.md) was a solemn [ʿăṣārâ](../../strongs/h/h6116.md), according unto the [mishpat](../../strongs/h/h4941.md).

---

[Transliteral Bible](../bible.md)

[Nehemiah](nehemiah.md)

[Nehemiah 7](nehemiah_7.md) - [Nehemiah 9](nehemiah_9.md)