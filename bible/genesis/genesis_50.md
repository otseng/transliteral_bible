# [Genesis 50](https://www.blueletterbible.org/kjv/gen/50/1/s_50001)

<a name="genesis_50_1"></a>Genesis 50:1

And [Yôsēp̄](../../strongs/h/h3130.md) [naphal](../../strongs/h/h5307.md) upon his ['ab](../../strongs/h/h1.md) [paniym](../../strongs/h/h6440.md), and [bāḵâ](../../strongs/h/h1058.md) upon him, and [nashaq](../../strongs/h/h5401.md) him.

<a name="genesis_50_2"></a>Genesis 50:2

And [Yôsēp̄](../../strongs/h/h3130.md) [tsavah](../../strongs/h/h6680.md) his ['ebed](../../strongs/h/h5650.md) the [rapha'](../../strongs/h/h7495.md) to [ḥānaṭ](../../strongs/h/h2590.md) his ['ab](../../strongs/h/h1.md): and the [rapha'](../../strongs/h/h7495.md) [ḥānaṭ](../../strongs/h/h2590.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="genesis_50_3"></a>Genesis 50:3

And forty [yowm](../../strongs/h/h3117.md) were [mālā'](../../strongs/h/h4390.md) for him; for so are [mālā'](../../strongs/h/h4390.md) the [yowm](../../strongs/h/h3117.md) of those which are [ḥānaṭ](../../strongs/h/h2590.md): and the [Mitsrayim](../../strongs/h/h4714.md) [bāḵâ](../../strongs/h/h1058.md) for him threescore and ten [yowm](../../strongs/h/h3117.md).

<a name="genesis_50_4"></a>Genesis 50:4

And when the [yowm](../../strongs/h/h3117.md) of his [bᵊḵîṯ](../../strongs/h/h1068.md) were ['abar](../../strongs/h/h5674.md), [Yôsēp̄](../../strongs/h/h3130.md) [dabar](../../strongs/h/h1696.md) unto the [bayith](../../strongs/h/h1004.md) of [Parʿô](../../strongs/h/h6547.md), ['āmar](../../strongs/h/h559.md), If now I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in your ['ayin](../../strongs/h/h5869.md), [dabar](../../strongs/h/h1696.md), I pray you, in the ['ozen](../../strongs/h/h241.md) of [Parʿô](../../strongs/h/h6547.md), ['āmar](../../strongs/h/h559.md),

<a name="genesis_50_5"></a>Genesis 50:5

My ['ab](../../strongs/h/h1.md) made me [shaba'](../../strongs/h/h7650.md), ['āmar](../../strongs/h/h559.md), [hinneh](../../strongs/h/h2009.md), I [muwth](../../strongs/h/h4191.md): in my [qeber](../../strongs/h/h6913.md) which I have [karah](../../strongs/h/h3738.md) for me in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), there shalt thou [qāḇar](../../strongs/h/h6912.md) me. Now therefore let me [ʿālâ](../../strongs/h/h5927.md), I pray thee, and [qāḇar](../../strongs/h/h6912.md) my ['ab](../../strongs/h/h1.md), and I will [shuwb](../../strongs/h/h7725.md).

<a name="genesis_50_6"></a>Genesis 50:6

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md), and [qāḇar](../../strongs/h/h6912.md) thy ['ab](../../strongs/h/h1.md), according as he made thee [shaba'](../../strongs/h/h7650.md).

<a name="genesis_50_7"></a>Genesis 50:7

And [Yôsēp̄](../../strongs/h/h3130.md) [ʿālâ](../../strongs/h/h5927.md) to [qāḇar](../../strongs/h/h6912.md) his ['ab](../../strongs/h/h1.md): and with him [ʿālâ](../../strongs/h/h5927.md) all the ['ebed](../../strongs/h/h5650.md) of [Parʿô](../../strongs/h/h6547.md), the [zāqēn](../../strongs/h/h2205.md) of his [bayith](../../strongs/h/h1004.md), and all the [zāqēn](../../strongs/h/h2205.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md),

<a name="genesis_50_8"></a>Genesis 50:8

And all the [bayith](../../strongs/h/h1004.md) of [Yôsēp̄](../../strongs/h/h3130.md), and his ['ach](../../strongs/h/h251.md), and his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md): only their [ṭap̄](../../strongs/h/h2945.md), and their [tso'n](../../strongs/h/h6629.md), and their [bāqār](../../strongs/h/h1241.md), they ['azab](../../strongs/h/h5800.md) in the ['erets](../../strongs/h/h776.md) of [gōšen](../../strongs/h/h1657.md).

<a name="genesis_50_9"></a>Genesis 50:9

And there [ʿālâ](../../strongs/h/h5927.md) with him both [reḵeḇ](../../strongs/h/h7393.md) and [pārāš](../../strongs/h/h6571.md): and it was a [me'od](../../strongs/h/h3966.md) [kāḇēḏ](../../strongs/h/h3515.md) [maḥănê](../../strongs/h/h4264.md).

<a name="genesis_50_10"></a>Genesis 50:10

And they [bow'](../../strongs/h/h935.md) to the [gōren](../../strongs/h/h1637.md) of ['āṭāḏ](../../strongs/h/h329.md), which is [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md), and there they [sāp̄aḏ](../../strongs/h/h5594.md) with a [gadowl](../../strongs/h/h1419.md) and [me'od](../../strongs/h/h3966.md) [kāḇēḏ](../../strongs/h/h3515.md) [mispēḏ](../../strongs/h/h4553.md): and he ['asah](../../strongs/h/h6213.md) an ['ēḇel](../../strongs/h/h60.md) for his ['ab](../../strongs/h/h1.md) seven [yowm](../../strongs/h/h3117.md).

<a name="genesis_50_11"></a>Genesis 50:11

And when the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md), the [Kᵊnaʿănî](../../strongs/h/h3669.md), [ra'ah](../../strongs/h/h7200.md) the ['ēḇel](../../strongs/h/h60.md) in the [gōren](../../strongs/h/h1637.md) of ['āṭāḏ](../../strongs/h/h329.md), they ['āmar](../../strongs/h/h559.md), This is a [kāḇēḏ](../../strongs/h/h3515.md) ['ēḇel](../../strongs/h/h60.md) to the [Mitsrayim](../../strongs/h/h4714.md): wherefore the [shem](../../strongs/h/h8034.md) of it was [qara'](../../strongs/h/h7121.md) ['āḇēl miṣrayim](../../strongs/h/h67.md), which is [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md).

<a name="genesis_50_12"></a>Genesis 50:12

And his [ben](../../strongs/h/h1121.md) ['asah](../../strongs/h/h6213.md) unto him according as he [tsavah](../../strongs/h/h6680.md) them:

<a name="genesis_50_13"></a>Genesis 50:13

For his [ben](../../strongs/h/h1121.md) [nasa'](../../strongs/h/h5375.md) him into the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), and [qāḇar](../../strongs/h/h6912.md) him in the [mᵊʿārâ](../../strongs/h/h4631.md) of the [sadeh](../../strongs/h/h7704.md) of [maḵpēlâ](../../strongs/h/h4375.md), which ['Abraham](../../strongs/h/h85.md) [qānâ](../../strongs/h/h7069.md) with the [sadeh](../../strongs/h/h7704.md) for an ['achuzzah](../../strongs/h/h272.md) of a [qeber](../../strongs/h/h6913.md) of [ʿep̄rôn](../../strongs/h/h6085.md) the [Ḥitî](../../strongs/h/h2850.md), [paniym](../../strongs/h/h6440.md) [mamrē'](../../strongs/h/h4471.md).

<a name="genesis_50_14"></a>Genesis 50:14

And [Yôsēp̄](../../strongs/h/h3130.md) [shuwb](../../strongs/h/h7725.md) into [Mitsrayim](../../strongs/h/h4714.md), he, and his ['ach](../../strongs/h/h251.md), and all that [ʿālâ](../../strongs/h/h5927.md) with him to [qāḇar](../../strongs/h/h6912.md) his ['ab](../../strongs/h/h1.md), ['aḥar](../../strongs/h/h310.md) he had [qāḇar](../../strongs/h/h6912.md) his ['ab](../../strongs/h/h1.md).

<a name="genesis_50_15"></a>Genesis 50:15

And when [Yôsēp̄](../../strongs/h/h3130.md) ['ach](../../strongs/h/h251.md) [ra'ah](../../strongs/h/h7200.md) that their ['ab](../../strongs/h/h1.md) was [muwth](../../strongs/h/h4191.md), they ['āmar](../../strongs/h/h559.md), [Yôsēp̄](../../strongs/h/h3130.md) will [lû'](../../strongs/h/h3863.md) [śāṭam](../../strongs/h/h7852.md) us, and will [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) all the [ra'](../../strongs/h/h7451.md) which we [gamal](../../strongs/h/h1580.md) unto him.

<a name="genesis_50_16"></a>Genesis 50:16

And they [tsavah](../../strongs/h/h6680.md) unto [Yôsēp̄](../../strongs/h/h3130.md), ['āmar](../../strongs/h/h559.md), Thy ['ab](../../strongs/h/h1.md) did [tsavah](../../strongs/h/h6680.md) [paniym](../../strongs/h/h6440.md) he [maveth](../../strongs/h/h4194.md), ['āmar](../../strongs/h/h559.md),

<a name="genesis_50_17"></a>Genesis 50:17

So shall ye ['āmar](../../strongs/h/h559.md) unto [Yôsēp̄](../../strongs/h/h3130.md), [nasa'](../../strongs/h/h5375.md), ['ānnā'](../../strongs/h/h577.md), the [pesha'](../../strongs/h/h6588.md) of thy ['ach](../../strongs/h/h251.md), and their [chatta'ath](../../strongs/h/h2403.md); for they [gamal](../../strongs/h/h1580.md) unto thee [ra'](../../strongs/h/h7451.md): and now, we pray thee, [nasa'](../../strongs/h/h5375.md) the [pesha'](../../strongs/h/h6588.md) of the ['ebed](../../strongs/h/h5650.md) of the ['Elohiym](../../strongs/h/h430.md) of thy ['ab](../../strongs/h/h1.md). And [Yôsēp̄](../../strongs/h/h3130.md) [bāḵâ](../../strongs/h/h1058.md) when they [dabar](../../strongs/h/h1696.md) unto him.

<a name="genesis_50_18"></a>Genesis 50:18

And his ['ach](../../strongs/h/h251.md) also [yālaḵ](../../strongs/h/h3212.md) and [naphal](../../strongs/h/h5307.md) before his [paniym](../../strongs/h/h6440.md); and they ['āmar](../../strongs/h/h559.md), Behold, we be thy ['ebed](../../strongs/h/h5650.md).

<a name="genesis_50_19"></a>Genesis 50:19

And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto them, [yare'](../../strongs/h/h3372.md) not: for am I in the place of ['Elohiym](../../strongs/h/h430.md)?

<a name="genesis_50_20"></a>Genesis 50:20

But as for you, ye [chashab](../../strongs/h/h2803.md) [ra'](../../strongs/h/h7451.md) against me; but ['Elohiym](../../strongs/h/h430.md) [chashab](../../strongs/h/h2803.md) unto [towb](../../strongs/h/h2896.md), to ['asah](../../strongs/h/h6213.md), as it is this [yowm](../../strongs/h/h3117.md), to [ḥāyâ](../../strongs/h/h2421.md) [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md).

<a name="genesis_50_21"></a>Genesis 50:21

Now therefore [yare'](../../strongs/h/h3372.md) ye not: I will [kûl](../../strongs/h/h3557.md) you, and your [ṭap̄](../../strongs/h/h2945.md). And he [nacham](../../strongs/h/h5162.md) them, and [dabar](../../strongs/h/h1696.md) [leb](../../strongs/h/h3820.md) unto them.

<a name="genesis_50_22"></a>Genesis 50:22

And [Yôsēp̄](../../strongs/h/h3130.md) [yashab](../../strongs/h/h3427.md) in [Mitsrayim](../../strongs/h/h4714.md), he, and his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md): and [Yôsēp̄](../../strongs/h/h3130.md) [ḥāyâ](../../strongs/h/h2421.md) an hundred and ten [šānâ](../../strongs/h/h8141.md).

<a name="genesis_50_23"></a>Genesis 50:23

And [Yôsēp̄](../../strongs/h/h3130.md) [ra'ah](../../strongs/h/h7200.md) ['Ep̄rayim](../../strongs/h/h669.md) [ben](../../strongs/h/h1121.md) of the [šillēšîm](../../strongs/h/h8029.md): the [ben](../../strongs/h/h1121.md) also of [Māḵîr](../../strongs/h/h4353.md) the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md) were [yalad](../../strongs/h/h3205.md) upon [Yôsēp̄](../../strongs/h/h3130.md) [bereḵ](../../strongs/h/h1290.md).

<a name="genesis_50_24"></a>Genesis 50:24

And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto his ['ach](../../strongs/h/h251.md), I [muwth](../../strongs/h/h4191.md): and ['Elohiym](../../strongs/h/h430.md) will [paqad](../../strongs/h/h6485.md) [paqad](../../strongs/h/h6485.md) you, and [ʿālâ](../../strongs/h/h5927.md) you of this ['erets](../../strongs/h/h776.md) unto the ['erets](../../strongs/h/h776.md) which he [shaba'](../../strongs/h/h7650.md) to ['Abraham](../../strongs/h/h85.md), to [Yiṣḥāq](../../strongs/h/h3327.md), and to [Ya'aqob](../../strongs/h/h3290.md).

<a name="genesis_50_25"></a>Genesis 50:25

And [Yôsēp̄](../../strongs/h/h3130.md) took a [shaba'](../../strongs/h/h7650.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) will [paqad](../../strongs/h/h6485.md) [paqad](../../strongs/h/h6485.md) you, and ye shall [ʿālâ](../../strongs/h/h5927.md) my ['etsem](../../strongs/h/h6106.md) from hence.

<a name="genesis_50_26"></a>Genesis 50:26

So [Yôsēp̄](../../strongs/h/h3130.md) [muwth](../../strongs/h/h4191.md), being an hundred and ten [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md): and they [ḥānaṭ](../../strongs/h/h2590.md) him, and he was [yāśam](../../strongs/h/h3455.md) in an ['ārôn](../../strongs/h/h727.md) in [Mitsrayim](../../strongs/h/h4714.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 49](genesis_49.md)