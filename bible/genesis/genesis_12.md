# [Genesis 12](https://www.blueletterbible.org/kjv/gen/12/1/s_12001)

<a name="genesis_12_1"></a>Genesis 12:1

Now [Yĕhovah](../../strongs/h/h3068.md) had ['āmar](../../strongs/h/h559.md) unto ['Aḇrām](../../strongs/h/h87.md), [yālaḵ](../../strongs/h/h3212.md) thee of thy ['erets](../../strongs/h/h776.md), and from thy [môleḏeṯ](../../strongs/h/h4138.md), and from thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), unto an ['erets](../../strongs/h/h776.md) that I will [ra'ah](../../strongs/h/h7200.md) thee:

<a name="genesis_12_2"></a>Genesis 12:2

And I will ['asah](../../strongs/h/h6213.md) of thee a [gadowl](../../strongs/h/h1419.md) [gowy](../../strongs/h/h1471.md), and I will [barak](../../strongs/h/h1288.md) thee, and [gāḏal](../../strongs/h/h1431.md) thy [shem](../../strongs/h/h8034.md) [gāḏal](../../strongs/h/h1431.md); and thou shalt be a [bĕrakah](../../strongs/h/h1293.md):

<a name="genesis_12_3"></a>Genesis 12:3

And I will [barak](../../strongs/h/h1288.md) them that [barak](../../strongs/h/h1288.md) thee, and ['arar](../../strongs/h/h779.md) him that [qālal](../../strongs/h/h7043.md) thee: and in thee shall all [mišpāḥâ](../../strongs/h/h4940.md) of the ['ăḏāmâ](../../strongs/h/h127.md) be [barak](../../strongs/h/h1288.md).

<a name="genesis_12_4"></a>Genesis 12:4

So ['Aḇrām](../../strongs/h/h87.md) [yālaḵ](../../strongs/h/h3212.md), as [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md) unto him; and [Lôṭ](../../strongs/h/h3876.md) went with him: and ['Aḇrām](../../strongs/h/h87.md) was seventy and five [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he [yāṣā'](../../strongs/h/h3318.md) out of [Ḥārān](../../strongs/h/h2771.md).

<a name="genesis_12_5"></a>Genesis 12:5

And ['Aḇrām](../../strongs/h/h87.md) [laqach](../../strongs/h/h3947.md) [Śāray](../../strongs/h/h8297.md) his ['ishshah](../../strongs/h/h802.md), and [Lôṭ](../../strongs/h/h3876.md) his ['ach](../../strongs/h/h251.md) [ben](../../strongs/h/h1121.md), and all their [rᵊḵûš](../../strongs/h/h7399.md) that they had [rāḵaš](../../strongs/h/h7408.md), and the [nephesh](../../strongs/h/h5315.md) that they had ['asah](../../strongs/h/h6213.md) in [Ḥārān](../../strongs/h/h2771.md); and they [yāṣā'](../../strongs/h/h3318.md) to [yālaḵ](../../strongs/h/h3212.md) into the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md); and into the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md) they [bow'](../../strongs/h/h935.md).

<a name="genesis_12_6"></a>Genesis 12:6

And ['Aḇrām](../../strongs/h/h87.md) ['abar](../../strongs/h/h5674.md) the ['erets](../../strongs/h/h776.md) unto the [maqowm](../../strongs/h/h4725.md) of [Šᵊḵem](../../strongs/h/h7927.md), unto the ['ēlôn](../../strongs/h/h436.md) of [Môrê](../../strongs/h/h4176.md). And the [Kᵊnaʿănî](../../strongs/h/h3669.md) was then in the ['erets](../../strongs/h/h776.md).

<a name="genesis_12_7"></a>Genesis 12:7

And [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) unto ['Aḇrām](../../strongs/h/h87.md), and ['āmar](../../strongs/h/h559.md), Unto thy [zera'](../../strongs/h/h2233.md) will I [nathan](../../strongs/h/h5414.md) this ['erets](../../strongs/h/h776.md): and there [bānâ](../../strongs/h/h1129.md) he a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md), who [ra'ah](../../strongs/h/h7200.md) unto him.

<a name="genesis_12_8"></a>Genesis 12:8

And he ['athaq](../../strongs/h/h6275.md) from thence unto a [har](../../strongs/h/h2022.md) on the [qeḏem](../../strongs/h/h6924.md) of [Bêṯ-'ēl](../../strongs/h/h1008.md), and [natah](../../strongs/h/h5186.md) his ['ohel](../../strongs/h/h168.md), having [Bêṯ-'ēl](../../strongs/h/h1008.md) on the [yam](../../strongs/h/h3220.md), and [ʿAy](../../strongs/h/h5857.md) on the [qeḏem](../../strongs/h/h6924.md): and there he [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md), and [qara'](../../strongs/h/h7121.md) upon the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="genesis_12_9"></a>Genesis 12:9

And ['Aḇrām](../../strongs/h/h87.md) [nāsaʿ](../../strongs/h/h5265.md), [halak](../../strongs/h/h1980.md) [nāsaʿ](../../strongs/h/h5265.md) toward the [neḡeḇ](../../strongs/h/h5045.md).

<a name="genesis_12_10"></a>Genesis 12:10

And there was a [rāʿāḇ](../../strongs/h/h7458.md) in the ['erets](../../strongs/h/h776.md): and ['Aḇrām](../../strongs/h/h87.md) [yarad](../../strongs/h/h3381.md) into [Mitsrayim](../../strongs/h/h4714.md) to [guwr](../../strongs/h/h1481.md) there; for the [rāʿāḇ](../../strongs/h/h7458.md) was [kāḇēḏ](../../strongs/h/h3515.md) in the ['erets](../../strongs/h/h776.md).

<a name="genesis_12_11"></a>Genesis 12:11

And it came to pass, when he was [qāraḇ](../../strongs/h/h7126.md) to [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md), that he ['āmar](../../strongs/h/h559.md) unto [Śāray](../../strongs/h/h8297.md) his ['ishshah](../../strongs/h/h802.md), [hinneh](../../strongs/h/h2009.md) [na'](../../strongs/h/h4994.md), I [yada'](../../strongs/h/h3045.md) that thou art a [yāp̄ê](../../strongs/h/h3303.md) ['ishshah](../../strongs/h/h802.md) to [mar'ê](../../strongs/h/h4758.md):

<a name="genesis_12_12"></a>Genesis 12:12

Therefore it shall come to pass, when the [Miṣrî](../../strongs/h/h4713.md) shall [ra'ah](../../strongs/h/h7200.md) thee, that they shall ['āmar](../../strongs/h/h559.md), This is his ['ishshah](../../strongs/h/h802.md): and they will [harag](../../strongs/h/h2026.md) me, but they will save thee [ḥāyâ](../../strongs/h/h2421.md).

<a name="genesis_12_13"></a>Genesis 12:13

['āmar](../../strongs/h/h559.md), [na'](../../strongs/h/h4994.md), thou art my ['āḥôṯ](../../strongs/h/h269.md): that it may be [yatab](../../strongs/h/h3190.md) with me for thy sake; and my [nephesh](../../strongs/h/h5315.md) shall [ḥāyâ](../../strongs/h/h2421.md) [gālāl](../../strongs/h/h1558.md) thee.

<a name="genesis_12_14"></a>Genesis 12:14

And it came to pass, that, when ['Aḇrām](../../strongs/h/h87.md) was [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md), the [Miṣrî](../../strongs/h/h4713.md) [ra'ah](../../strongs/h/h7200.md) the ['ishshah](../../strongs/h/h802.md) that she was [me'od](../../strongs/h/h3966.md) [yāp̄ê](../../strongs/h/h3303.md).

<a name="genesis_12_15"></a>Genesis 12:15

The [śar](../../strongs/h/h8269.md) also of [Parʿô](../../strongs/h/h6547.md) [ra'ah](../../strongs/h/h7200.md) her, and [halal](../../strongs/h/h1984.md) her before [Parʿô](../../strongs/h/h6547.md): and the ['ishshah](../../strongs/h/h802.md) was [laqach](../../strongs/h/h3947.md) into [Parʿô](../../strongs/h/h6547.md) [bayith](../../strongs/h/h1004.md).

<a name="genesis_12_16"></a>Genesis 12:16

And he [yatab](../../strongs/h/h3190.md) ['Aḇrām](../../strongs/h/h87.md) for her sake: and he had [tso'n](../../strongs/h/h6629.md), and [bāqār](../../strongs/h/h1241.md), and [chamowr](../../strongs/h/h2543.md), and ['ebed](../../strongs/h/h5650.md), and [šip̄ḥâ](../../strongs/h/h8198.md), and ['āṯôn](../../strongs/h/h860.md), and [gāmāl](../../strongs/h/h1581.md).

<a name="genesis_12_17"></a>Genesis 12:17

And [Yĕhovah](../../strongs/h/h3068.md) [naga'](../../strongs/h/h5060.md) [Parʿô](../../strongs/h/h6547.md) and his [bayith](../../strongs/h/h1004.md) with [gadowl](../../strongs/h/h1419.md) [neḡaʿ](../../strongs/h/h5061.md) [dabar](../../strongs/h/h1697.md) [Śāray](../../strongs/h/h8297.md) ['Aḇrām](../../strongs/h/h87.md) ['ishshah](../../strongs/h/h802.md).

<a name="genesis_12_18"></a>Genesis 12:18

And [Parʿô](../../strongs/h/h6547.md) [qara'](../../strongs/h/h7121.md) ['Aḇrām](../../strongs/h/h87.md) and ['āmar](../../strongs/h/h559.md), What is this that thou hast ['asah](../../strongs/h/h6213.md) unto me? why didst thou not [nāḡaḏ](../../strongs/h/h5046.md) me that she was thy ['ishshah](../../strongs/h/h802.md)?

<a name="genesis_12_19"></a>Genesis 12:19

Why ['āmar](../../strongs/h/h559.md) thou, She is my ['āḥôṯ](../../strongs/h/h269.md)? so I might have taken her to me to ['ishshah](../../strongs/h/h802.md): now therefore behold thy ['ishshah](../../strongs/h/h802.md), [laqach](../../strongs/h/h3947.md) her, and [yālaḵ](../../strongs/h/h3212.md).

<a name="genesis_12_20"></a>Genesis 12:20

And [Parʿô](../../strongs/h/h6547.md) [tsavah](../../strongs/h/h6680.md) his ['enowsh](../../strongs/h/h582.md) concerning him: and they [shalach](../../strongs/h/h7971.md) him, and his ['ishshah](../../strongs/h/h802.md), and all that he had.

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 11](genesis_11.md) - [Genesis 13](genesis_13.md)