# [Genesis 32](https://www.blueletterbible.org/kjv/gen/32/1/s_32001)

<a name="genesis_32_1"></a>Genesis 32:1

And [Ya'aqob](../../strongs/h/h3290.md) [halak](../../strongs/h/h1980.md) on his [derek](../../strongs/h/h1870.md), and the [mal'ak](../../strongs/h/h4397.md) of ['Elohiym](../../strongs/h/h430.md) [pāḡaʿ](../../strongs/h/h6293.md) him.

<a name="genesis_32_2"></a>Genesis 32:2

And when [Ya'aqob](../../strongs/h/h3290.md) [ra'ah](../../strongs/h/h7200.md) them, he ['āmar](../../strongs/h/h559.md), This is ['Elohiym](../../strongs/h/h430.md) [maḥănê](../../strongs/h/h4264.md): and he [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of that [maqowm](../../strongs/h/h4725.md) [Maḥănayim](../../strongs/h/h4266.md).

<a name="genesis_32_3"></a>Genesis 32:3

And [Ya'aqob](../../strongs/h/h3290.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) [paniym](../../strongs/h/h6440.md) him to [ʿĒśāv](../../strongs/h/h6215.md) his ['ach](../../strongs/h/h251.md) unto the ['erets](../../strongs/h/h776.md) of [Śēʿîr](../../strongs/h/h8165.md), the [sadeh](../../strongs/h/h7704.md) of ['Ĕḏōm](../../strongs/h/h123.md).

<a name="genesis_32_4"></a>Genesis 32:4

And he [tsavah](../../strongs/h/h6680.md) them, ['āmar](../../strongs/h/h559.md), Thus shall ye ['āmar](../../strongs/h/h559.md) unto my ['adown](../../strongs/h/h113.md) [ʿĒśāv](../../strongs/h/h6215.md); Thy ['ebed](../../strongs/h/h5650.md) [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md) thus, I have [guwr](../../strongs/h/h1481.md) with [Lāḇān](../../strongs/h/h3837.md), and ['āḥar](../../strongs/h/h309.md) until now:

<a name="genesis_32_5"></a>Genesis 32:5

And I have [showr](../../strongs/h/h7794.md), and [chamowr](../../strongs/h/h2543.md), [tso'n](../../strongs/h/h6629.md), and ['ebed](../../strongs/h/h5650.md), and [šip̄ḥâ](../../strongs/h/h8198.md): and I have [shalach](../../strongs/h/h7971.md) to [nāḡaḏ](../../strongs/h/h5046.md) my ['adown](../../strongs/h/h113.md), that I may [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md).

<a name="genesis_32_6"></a>Genesis 32:6

And the [mal'ak](../../strongs/h/h4397.md) [shuwb](../../strongs/h/h7725.md) to [Ya'aqob](../../strongs/h/h3290.md), ['āmar](../../strongs/h/h559.md), We [bow'](../../strongs/h/h935.md) to thy ['ach](../../strongs/h/h251.md) [ʿĒśāv](../../strongs/h/h6215.md), and also he [halak](../../strongs/h/h1980.md) to [qārā'](../../strongs/h/h7125.md) thee, and four hundred ['iysh](../../strongs/h/h376.md) with him.

<a name="genesis_32_7"></a>Genesis 32:7

Then [Ya'aqob](../../strongs/h/h3290.md) was [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md) and [yāṣar](../../strongs/h/h3334.md): and he [ḥāṣâ](../../strongs/h/h2673.md) the ['am](../../strongs/h/h5971.md) that was with him, and the [tso'n](../../strongs/h/h6629.md), and [bāqār](../../strongs/h/h1241.md), and the [gāmāl](../../strongs/h/h1581.md), into two [maḥănê](../../strongs/h/h4264.md);

<a name="genesis_32_8"></a>Genesis 32:8

And ['āmar](../../strongs/h/h559.md), If [ʿĒśāv](../../strongs/h/h6215.md) [bow'](../../strongs/h/h935.md) to the one [maḥănê](../../strongs/h/h4264.md), and [nakah](../../strongs/h/h5221.md) it, then the other [maḥănê](../../strongs/h/h4264.md) which is [šā'ar](../../strongs/h/h7604.md) shall [pᵊlêṭâ](../../strongs/h/h6413.md).

<a name="genesis_32_9"></a>Genesis 32:9

And [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) of my ['ab](../../strongs/h/h1.md) ['Abraham](../../strongs/h/h85.md), and ['Elohiym](../../strongs/h/h430.md) of my ['ab](../../strongs/h/h1.md) [Yiṣḥāq](../../strongs/h/h3327.md), [Yĕhovah](../../strongs/h/h3068.md) which ['āmar](../../strongs/h/h559.md) unto me, [shuwb](../../strongs/h/h7725.md) unto thy ['erets](../../strongs/h/h776.md), and to thy [môleḏeṯ](../../strongs/h/h4138.md), and I will [yatab](../../strongs/h/h3190.md) with thee:

<a name="genesis_32_10"></a>Genesis 32:10

I am not worthy of the [qāṭōn](../../strongs/h/h6994.md) of all the [checed](../../strongs/h/h2617.md), and of all the ['emeth](../../strongs/h/h571.md), which thou hast ['asah](../../strongs/h/h6213.md) unto thy ['ebed](../../strongs/h/h5650.md); for with my [maqqēl](../../strongs/h/h4731.md) I ['abar](../../strongs/h/h5674.md) this [Yardēn](../../strongs/h/h3383.md); and now I am become two [maḥănê](../../strongs/h/h4264.md).

<a name="genesis_32_11"></a>Genesis 32:11

[natsal](../../strongs/h/h5337.md) me, I pray thee, from the [yad](../../strongs/h/h3027.md) of my ['ach](../../strongs/h/h251.md), from the [yad](../../strongs/h/h3027.md) of [ʿĒśāv](../../strongs/h/h6215.md): for I [yārē'](../../strongs/h/h3373.md) him, lest he will [bow'](../../strongs/h/h935.md) and [nakah](../../strongs/h/h5221.md) me, and the ['em](../../strongs/h/h517.md) with the [ben](../../strongs/h/h1121.md).

<a name="genesis_32_12"></a>Genesis 32:12

And thou ['āmar](../../strongs/h/h559.md), I will do thee [yatab](../../strongs/h/h3190.md) [yatab](../../strongs/h/h3190.md), and [śûm](../../strongs/h/h7760.md) thy [zera'](../../strongs/h/h2233.md) as the [ḥôl](../../strongs/h/h2344.md) of the [yam](../../strongs/h/h3220.md), which cannot be [sāp̄ar](../../strongs/h/h5608.md) for [rōḇ](../../strongs/h/h7230.md).

<a name="genesis_32_13"></a>Genesis 32:13

And he [lûn](../../strongs/h/h3885.md) there that same [layil](../../strongs/h/h3915.md); and [laqach](../../strongs/h/h3947.md) of that which [bow'](../../strongs/h/h935.md) to his [yad](../../strongs/h/h3027.md) a [minchah](../../strongs/h/h4503.md) for [ʿĒśāv](../../strongs/h/h6215.md) his ['ach](../../strongs/h/h251.md);

<a name="genesis_32_14"></a>Genesis 32:14

Two hundred [ʿēz](../../strongs/h/h5795.md), and twenty [tayiš](../../strongs/h/h8495.md), two hundred [rāḥēl](../../strongs/h/h7353.md), and twenty ['ayil](../../strongs/h/h352.md),

<a name="genesis_32_15"></a>Genesis 32:15

Thirty [yānaq](../../strongs/h/h3243.md) [gāmāl](../../strongs/h/h1581.md) with their [ben](../../strongs/h/h1121.md), forty [pārâ](../../strongs/h/h6510.md), and ten [par](../../strongs/h/h6499.md), twenty ['āṯôn](../../strongs/h/h860.md), and ten [ʿayir](../../strongs/h/h5895.md).

<a name="genesis_32_16"></a>Genesis 32:16

And he [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of his ['ebed](../../strongs/h/h5650.md), every [ʿēḏer](../../strongs/h/h5739.md) by themselves; and ['āmar](../../strongs/h/h559.md) unto his ['ebed](../../strongs/h/h5650.md), ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) me, and [śûm](../../strongs/h/h7760.md) a [revaḥ](../../strongs/h/h7305.md) betwixt [ʿēḏer](../../strongs/h/h5739.md) and [ʿēḏer](../../strongs/h/h5739.md).

<a name="genesis_32_17"></a>Genesis 32:17

And he [tsavah](../../strongs/h/h6680.md) the [ri'šôn](../../strongs/h/h7223.md), ['āmar](../../strongs/h/h559.md), When [ʿĒśāv](../../strongs/h/h6215.md) my ['ach](../../strongs/h/h251.md) [pāḡaš](../../strongs/h/h6298.md) thee, and [sha'al](../../strongs/h/h7592.md) thee, ['āmar](../../strongs/h/h559.md), Whose art thou? and whither [yālaḵ](../../strongs/h/h3212.md) thou? and whose are these [paniym](../../strongs/h/h6440.md) thee?

<a name="genesis_32_18"></a>Genesis 32:18

Then thou shalt ['āmar](../../strongs/h/h559.md), They be thy ['ebed](../../strongs/h/h5650.md) [Ya'aqob](../../strongs/h/h3290.md); it is a [minchah](../../strongs/h/h4503.md) [shalach](../../strongs/h/h7971.md) unto my ['adown](../../strongs/h/h113.md) [ʿĒśāv](../../strongs/h/h6215.md): and, behold, also he is ['aḥar](../../strongs/h/h310.md) us.

<a name="genesis_32_19"></a>Genesis 32:19

And so [tsavah](../../strongs/h/h6680.md) he the [šēnî](../../strongs/h/h8145.md), and the [šᵊlîšî](../../strongs/h/h7992.md), and all that [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) the [ʿēḏer](../../strongs/h/h5739.md), ['āmar](../../strongs/h/h559.md), On this [dabar](../../strongs/h/h1697.md) shall ye [dabar](../../strongs/h/h1696.md) unto [ʿĒśāv](../../strongs/h/h6215.md), when ye [māṣā'](../../strongs/h/h4672.md) him.

<a name="genesis_32_20"></a>Genesis 32:20

And say ye moreover, Behold, thy ['ebed](../../strongs/h/h5650.md) [Ya'aqob](../../strongs/h/h3290.md) is ['aḥar](../../strongs/h/h310.md) us. For he ['āmar](../../strongs/h/h559.md), I will [kāp̄ar](../../strongs/h/h3722.md) him with the [minchah](../../strongs/h/h4503.md) that [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) me, and afterward I will [ra'ah](../../strongs/h/h7200.md) his [paniym](../../strongs/h/h6440.md); peradventure he will [nasa'](../../strongs/h/h5375.md) of me.

<a name="genesis_32_21"></a>Genesis 32:21

So ['abar](../../strongs/h/h5674.md) the [minchah](../../strongs/h/h4503.md) over [paniym](../../strongs/h/h6440.md) him: and himself [lûn](../../strongs/h/h3885.md) that [layil](../../strongs/h/h3915.md) in the [maḥănê](../../strongs/h/h4264.md).

<a name="genesis_32_22"></a>Genesis 32:22

And he [quwm](../../strongs/h/h6965.md) that [layil](../../strongs/h/h3915.md), and [laqach](../../strongs/h/h3947.md) his two ['ishshah](../../strongs/h/h802.md), and his two [šip̄ḥâ](../../strongs/h/h8198.md), and his eleven [yeleḏ](../../strongs/h/h3206.md), and ['abar](../../strongs/h/h5674.md) the [maʿăḇār](../../strongs/h/h4569.md) [Yabōq](../../strongs/h/h2999.md).

<a name="genesis_32_23"></a>Genesis 32:23

And he [laqach](../../strongs/h/h3947.md) them, and ['abar](../../strongs/h/h5674.md) them the [nachal](../../strongs/h/h5158.md), and ['abar](../../strongs/h/h5674.md) that he had.

<a name="genesis_32_24"></a>Genesis 32:24

And [Ya'aqob](../../strongs/h/h3290.md) was [yāṯar](../../strongs/h/h3498.md); and there ['āḇaq](../../strongs/h/h79.md) an ['iysh](../../strongs/h/h376.md) with him until the [ʿālâ](../../strongs/h/h5927.md) of the [šaḥar](../../strongs/h/h7837.md).

<a name="genesis_32_25"></a>Genesis 32:25

And when he [ra'ah](../../strongs/h/h7200.md) that he [yakol](../../strongs/h/h3201.md) not against him, he [naga'](../../strongs/h/h5060.md) the [yārēḵ](../../strongs/h/h3409.md); and the [kaph](../../strongs/h/h3709.md) of [Ya'aqob](../../strongs/h/h3290.md) [yārēḵ](../../strongs/h/h3409.md) was [yāqaʿ](../../strongs/h/h3363.md), as he ['āḇaq](../../strongs/h/h79.md) with him.

<a name="genesis_32_26"></a>Genesis 32:26

And he ['āmar](../../strongs/h/h559.md), [shalach](../../strongs/h/h7971.md), for the [šaḥar](../../strongs/h/h7837.md) [ʿālâ](../../strongs/h/h5927.md). And he ['āmar](../../strongs/h/h559.md), I will not [shalach](../../strongs/h/h7971.md) thee, except thou [barak](../../strongs/h/h1288.md) me.

<a name="genesis_32_27"></a>Genesis 32:27

And he ['āmar](../../strongs/h/h559.md) unto him, What is thy [shem](../../strongs/h/h8034.md)? And he ['āmar](../../strongs/h/h559.md), [Ya'aqob](../../strongs/h/h3290.md).

<a name="genesis_32_28"></a>Genesis 32:28

And he ['āmar](../../strongs/h/h559.md), Thy [shem](../../strongs/h/h8034.md) shall be ['āmar](../../strongs/h/h559.md) no more [Ya'aqob](../../strongs/h/h3290.md), but [Yisra'el](../../strongs/h/h3478.md): for hast thou [Śārâ](../../strongs/h/h8280.md) with ['Elohiym](../../strongs/h/h430.md) and with ['enowsh](../../strongs/h/h582.md), and hast [yakol](../../strongs/h/h3201.md).

<a name="genesis_32_29"></a>Genesis 32:29

And [Ya'aqob](../../strongs/h/h3290.md) [sha'al](../../strongs/h/h7592.md) him, and ['āmar](../../strongs/h/h559.md), [nāḡaḏ](../../strongs/h/h5046.md) me, I pray thee, thy [shem](../../strongs/h/h8034.md). And he ['āmar](../../strongs/h/h559.md), Wherefore is it that thou dost [sha'al](../../strongs/h/h7592.md) after my [shem](../../strongs/h/h8034.md)? And he [barak](../../strongs/h/h1288.md) him there.

<a name="genesis_32_30"></a>Genesis 32:30

And [Ya'aqob](../../strongs/h/h3290.md) [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of the [maqowm](../../strongs/h/h4725.md) [Pᵊnû'ēl](../../strongs/h/h6439.md): for I have [ra'ah](../../strongs/h/h7200.md) ['Elohiym](../../strongs/h/h430.md) [paniym](../../strongs/h/h6440.md) to [paniym](../../strongs/h/h6440.md), and my [nephesh](../../strongs/h/h5315.md) is [natsal](../../strongs/h/h5337.md).

<a name="genesis_32_31"></a>Genesis 32:31

And as he ['abar](../../strongs/h/h5674.md) [Pᵊnû'ēl](../../strongs/h/h6439.md) the [šemeš](../../strongs/h/h8121.md) [zāraḥ](../../strongs/h/h2224.md) upon him, and he [ṣālaʿ](../../strongs/h/h6760.md) upon his [yārēḵ](../../strongs/h/h3409.md).

<a name="genesis_32_32"></a>Genesis 32:32

Therefore the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['akal](../../strongs/h/h398.md) not of the [gîḏ](../../strongs/h/h1517.md) which [nāšê](../../strongs/h/h5384.md), which is upon the [kaph](../../strongs/h/h3709.md) of the [yārēḵ](../../strongs/h/h3409.md), unto this [yowm](../../strongs/h/h3117.md): because he [naga'](../../strongs/h/h5060.md) the [kaph](../../strongs/h/h3709.md) of [Ya'aqob](../../strongs/h/h3290.md) [yārēḵ](../../strongs/h/h3409.md) in the [gîḏ](../../strongs/h/h1517.md) that [nāšê](../../strongs/h/h5384.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 31](genesis_31.md) - [Genesis 33](genesis_33.md)