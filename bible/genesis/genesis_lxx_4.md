# [Genesis 4](https://www.blueletterbible.org/lxx/genesis/4)

<a name="genesis_4_1"></a>Genesis 4:1

And [Adam](../../strongs/g/g76.md) [ginōskō](../../strongs/g/g1097.md) [Heya](../../strongs/g/g2096.md) his [gynē](../../strongs/g/g1135.md). And [syllambanō](../../strongs/g/g4815.md), she [tiktō](../../strongs/g/g5088.md) [Kain](../../strongs/g/g2535.md), and [eipon](../../strongs/g/g2036.md), I [ktaomai](../../strongs/g/g2932.md) a [anthrōpos](../../strongs/g/g444.md) through [theos](../../strongs/g/g2316.md). 

<a name="genesis_4_2"></a>Genesis 4:2

And she [prostithēmi](../../strongs/g/g4369.md) to [tiktō](../../strongs/g/g5088.md) his [adelphos](../../strongs/g/g80.md), [Abel](../../strongs/g/g6.md). And [Abel](../../strongs/g/g6.md) became a [poimēn](../../strongs/g/g4166.md) of [probaton](../../strongs/g/g4263.md), but [Kain](../../strongs/g/g2535.md) was [ergazomai](../../strongs/g/g2038.md) the [gē](../../strongs/g/g1093.md). 

<a name="genesis_4_3"></a>Genesis 4:3

And it came to pass after some days, [Kain](../../strongs/g/g2535.md) [pherō](../../strongs/g/g5342.md) from the [karpos](../../strongs/g/g2590.md) of the [gē](../../strongs/g/g1093.md) a [thysia](../../strongs/g/g2378.md) to the [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_4_4"></a>Genesis 4:4

And [Abel](../../strongs/g/g6.md) [pherō](../../strongs/g/g5342.md) also himself from the [prōtotokos](../../strongs/g/g4416.md) of his [probaton](../../strongs/g/g4263.md), and from his fatlings (steatōn). And [theos](../../strongs/g/g2316.md) [epeidon](../../strongs/g/g1896.md) upon [Abel](../../strongs/g/g6.md) and upon his [dōron](../../strongs/g/g1435.md). 

<a name="genesis_4_5"></a>Genesis 4:5

But upon [Kain](../../strongs/g/g2535.md) and upon his [thysia](../../strongs/g/g2378.md), he did not [prosechō](../../strongs/g/g4337.md). And [Kain](../../strongs/g/g2535.md) [lypeō](../../strongs/g/g3076.md) [lian](../../strongs/g/g3029.md), and became downcast (synepesen) in the [prosōpon](../../strongs/g/g4383.md). 

<a name="genesis_4_6"></a>Genesis 4:6

And [eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) to [Kain](../../strongs/g/g2535.md), Why [perilypos](../../strongs/g/g4036.md) are you, and why is downcast (synepesen) your [prosōpon](../../strongs/g/g4383.md)? 

<a name="genesis_4_7"></a>Genesis 4:7

If not [orthōs](../../strongs/g/g3723.md) you [prospherō](../../strongs/g/g4374.md), [orthōs](../../strongs/g/g3723.md) but not [diaireō](../../strongs/g/g1244.md), you [hamartanō](../../strongs/g/g264.md)? Be [hēsychazō](../../strongs/g/g2270.md), to you shall be his submission (apostrophē), and you will [archomai](../../strongs/g/g756.md) him! 

<a name="genesis_4_8"></a>Genesis 4:8

And [Kain](../../strongs/g/g2535.md) [eipon](../../strongs/g/g2036.md) to [Abel](../../strongs/g/g6.md) his [adelphos](../../strongs/g/g80.md), Let us [dierchomai](../../strongs/g/g1330.md) into the plain (pedion). And it [ginomai](../../strongs/g/g1096.md) in their being in the plain (pedion), [Kain](../../strongs/g/g2535.md) [anistēmi](../../strongs/g/g450.md) against [Abel](../../strongs/g/g6.md) his [adelphos](../../strongs/g/g80.md), and [apokteinō](../../strongs/g/g615.md) him. 

<a name="genesis_4_9"></a>Genesis 4:9

And [eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) to [Kain](../../strongs/g/g2535.md), Where is [Abel](../../strongs/g/g6.md) your [adelphos](../../strongs/g/g80.md)? And he [eipon](../../strongs/g/g2036.md), I do not [ginōskō](../../strongs/g/g1097.md), not the [phylax](../../strongs/g/g5441.md) of my [adelphos](../../strongs/g/g80.md) I am.

<a name="genesis_4_10"></a>Genesis 4:10

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md), What did you [poieō](../../strongs/g/g4160.md)? The [phōnē](../../strongs/g/g5456.md) of the [haima](../../strongs/g/g129.md) of your [adelphos](../../strongs/g/g80.md) [boaō](../../strongs/g/g994.md) to me from the [gē](../../strongs/g/g1093.md). 

<a name="genesis_4_11"></a>Genesis 4:11

And now, [epikataratos](../../strongs/g/g1944.md) are you from the [gē](../../strongs/g/g1093.md) which gaped wide (echanen) her [stoma](../../strongs/g/g4750.md) to [dechomai](../../strongs/g/g1209.md) the [haima](../../strongs/g/g129.md) of your [adelphos](../../strongs/g/g80.md) from your [cheir](../../strongs/g/g5495.md). 

<a name="genesis_4_12"></a>Genesis 4:12

When you [ergazomai](../../strongs/g/g2038.md) the [gē](../../strongs/g/g1093.md), and it does not [prostithēmi](../../strongs/g/g4369.md) her [ischys](../../strongs/g/g2479.md) to [didōmi](../../strongs/g/g1325.md) to you; then in moaning (stenōn) and [tremō](../../strongs/g/g5141.md) you will be upon the [gē](../../strongs/g/g1093.md). 

<a name="genesis_4_13"></a>Genesis 4:13

And [Kain](../../strongs/g/g2535.md) [eipon](../../strongs/g/g2036.md) to the [kyrios](../../strongs/g/g2962.md), is [meizōn](../../strongs/g/g3187.md) my [aitia](../../strongs/g/g156.md) to [aphiēmi](../../strongs/g/g863.md) me. 

<a name="genesis_4_14"></a>Genesis 4:14

If you [ekballō](../../strongs/g/g1544.md) me [sēmeron](../../strongs/g/g4594.md) from the [prosōpon](../../strongs/g/g4383.md) of the [gē](../../strongs/g/g1093.md), and from your [prosōpon](../../strongs/g/g4383.md), I will [kryptō](../../strongs/g/g2928.md), and I will be moaning (stenōn) and [tremō](../../strongs/g/g5141.md) upon the [gē](../../strongs/g/g1093.md); and it will be all the ones [heuriskō](../../strongs/g/g2147.md) me will [apokteinō](../../strongs/g/g615.md) me. 

<a name="genesis_4_15"></a>Genesis 4:15

"And [eipon](../../strongs/g/g2036.md) to him the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md), Not so, all [apokteinō](../../strongs/g/g615.md) [Kain](../../strongs/g/g2535.md) [hepta](../../strongs/g/g2033.md) times by [ekdikeō](../../strongs/g/g1556.md) will be [paralyō](../../strongs/g/g3886.md). And [tithēmi](../../strongs/g/g5087.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) a [sēmeion](../../strongs/g/g4592.md) to [Kain](../../strongs/g/g2535.md) to not [anaireō](../../strongs/g/g337.md) him for all [heuriskō](../../strongs/g/g2147.md) him. "

<a name="genesis_4_16"></a>Genesis 4:16

[exerchomai](../../strongs/g/g1831.md) [Kain](../../strongs/g/g2535.md) from the [prosōpon](../../strongs/g/g4383.md) of [theos](../../strongs/g/g2316.md), and he [oikeō](../../strongs/g/g3611.md) in the [gē](../../strongs/g/g1093.md) of Nod, over [katenanti](../../strongs/g/g2713.md) Eden.

<a name="genesis_4_17"></a>Genesis 4:17

And [Kain](../../strongs/g/g2535.md) [ginōskō](../../strongs/g/g1097.md) his [gynē](../../strongs/g/g1135.md). And she, [syllambanō](../../strongs/g/g4815.md), [tiktō](../../strongs/g/g5088.md) [Henōch](../../strongs/g/g1802.md). And he was [oikodomeō](../../strongs/g/g3618.md) a [polis](../../strongs/g/g4172.md), and he [eponomazō](../../strongs/g/g2028.md) the [polis](../../strongs/g/g4172.md) after the [onoma](../../strongs/g/g3686.md) of his [huios](../../strongs/g/g5207.md) [Henōch](../../strongs/g/g1802.md).

<a name="genesis_4_18"></a>Genesis 4:18

was [gennaō](../../strongs/g/g1080.md) And to [Henōch](../../strongs/g/g1802.md) Irad; and Irad [gennaō](../../strongs/g/g1080.md) Mehujael; and Mehujael [gennaō](../../strongs/g/g1080.md) [Mathousala](../../strongs/g/g3103.md); and [Mathousala](../../strongs/g/g3103.md) [gennaō](../../strongs/g/g1080.md) [Lamech](../../strongs/g/g2984.md).

<a name="genesis_4_19"></a>Genesis 4:19

And [lambanō](../../strongs/g/g2983.md) to himself [Lamech](../../strongs/g/g2984.md) two [gynē](../../strongs/g/g1135.md); the [onoma](../../strongs/g/g3686.md) to the one was Adah, and the [onoma](../../strongs/g/g3686.md) to the second was Tubal-Zillah.

<a name="genesis_4_20"></a>Genesis 4:20

And Adah [tiktō](../../strongs/g/g5088.md) Jabel, this one was [patēr](../../strongs/g/g3962.md) of the ones [oikeō](../../strongs/g/g3611.md) in [skēnē](../../strongs/g/g4633.md), grazing cattle (ktēnotrophōn).

<a name="genesis_4_21"></a>Genesis 4:21

And the [onoma](../../strongs/g/g3686.md) to his [adelphos](../../strongs/g/g80.md) was Jubal, this one was the one introducing (katadeixas) the psaltery (psaltērion) and the [kithara](../../strongs/g/g2788.md). 

<a name="genesis_4_22"></a>Genesis 4:22

But Tubal-Zillah also herself [tiktō](../../strongs/g/g5088.md) Tubal-cain, and he was a hammer-smith (sphyrokopos) [chalkeus](../../strongs/g/g5471.md) of [chalkos](../../strongs/g/g5475.md) and of [sidēros](../../strongs/g/g4604.md). And the [adelphē](../../strongs/g/g79.md) of Tubal-cain was Naamah.

<a name="genesis_4_23"></a>Genesis 4:23

[eipon](../../strongs/g/g2036.md) And [Lamech](../../strongs/g/g2984.md) to his own [gynē](../../strongs/g/g1135.md), Adah and Tubal-Zillah, [akouō](../../strongs/g/g191.md) my [phōnē](../../strongs/g/g5456.md), O [gynē](../../strongs/g/g1135.md) of [Lamech](../../strongs/g/g2984.md)! [enōtizomai](../../strongs/g/g1801.md) my [logos](../../strongs/g/g3056.md)! for a [anēr](../../strongs/g/g435.md) I [apokteinō](../../strongs/g/g615.md) for giving a [trauma](../../strongs/g/g5134.md) to me, and a [neaniskos](../../strongs/g/g3495.md) for giving a [mōlōps](../../strongs/g/g3468.md) to me. 

<a name="genesis_4_24"></a>Genesis 4:24

For [heptakis](../../strongs/g/g2034.md) [ekdikeō](../../strongs/g/g1556.md) is for [Kain](../../strongs/g/g2535.md), but for [Lamech](../../strongs/g/g2984.md), [hebdomēkontakis](../../strongs/g/g1441.md) [hepta](../../strongs/g/g2033.md). 

<a name="genesis_4_25"></a>Genesis 4:25

[ginōskō](../../strongs/g/g1097.md) [Adam](../../strongs/g/g76.md) [Heya](../../strongs/g/g2096.md) his [gynē](../../strongs/g/g1135.md). And [syllambanō](../../strongs/g/g4815.md), she [tiktō](../../strongs/g/g5088.md) a [huios](../../strongs/g/g5207.md), and [eponomazō](../../strongs/g/g2028.md) his [onoma](../../strongs/g/g3686.md) [Sēth](../../strongs/g/g4589.md), [legō](../../strongs/g/g3004.md), [exanistēmi](../../strongs/g/g1817.md) For to me [theos](../../strongs/g/g2316.md) [sperma](../../strongs/g/g4690.md) another instead of [Abel](../../strongs/g/g6.md), whom [Kain](../../strongs/g/g2535.md) [apokteinō](../../strongs/g/g615.md). 

<a name="genesis_4_26"></a>Genesis 4:26

And to [Sēth](../../strongs/g/g4589.md) was [ginomai](../../strongs/g/g1096.md) a [huios](../../strongs/g/g5207.md). And he named his [onoma](../../strongs/g/g3686.md) [Enōs](../../strongs/g/g1800.md); this one [elpizō](../../strongs/g/g1679.md) [epikaleō](../../strongs/g/g1941.md) the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 3](genesis_lxx_3.md) - [Genesis 5](genesis_lxx_5.md)