# [Genesis 10](https://www.blueletterbible.org/lxx/genesis/10)

<a name="genesis_10_1"></a>Genesis 10:1

And these are the [genesis](../../strongs/g/g1078.md) of the [huios](../../strongs/g/g5207.md) of [Nōe](../../strongs/g/g3575.md) -- [Sēm](../../strongs/g/g4590.md), Ham, Japheth. And were [gennaō](../../strongs/g/g1080.md) to them [huios](../../strongs/g/g5207.md) after the [kataklysmos](../../strongs/g/g2627.md). 

<a name="genesis_10_2"></a>Genesis 10:2

The [huios](../../strongs/g/g5207.md) of Japheth -- Gomer, and [Magōg](../../strongs/g/g3098.md), and Madai, and Javan, and Tubal, and Meshech, and Tiras.

<a name="genesis_10_3"></a>Genesis 10:3

And the [huios](../../strongs/g/g5207.md) of Gomer -- Ashkenaz, and Riphath, and Togarmah.

<a name="genesis_10_4"></a>Genesis 10:4

And the [huios](../../strongs/g/g5207.md) of Javan -- Elisha, and Tarshish, Kittim, Dodanim.

<a name="genesis_10_5"></a>Genesis 10:5

From out of these were [aphorizō](../../strongs/g/g873.md) [nēsos](../../strongs/g/g3520.md) of the [ethnos](../../strongs/g/g1484.md) in their [gē](../../strongs/g/g1093.md), each according to [glōssa](../../strongs/g/g1100.md), among their [phylē](../../strongs/g/g5443.md), and among their [ethnos](../../strongs/g/g1484.md). 

<a name="genesis_10_6"></a>Genesis 10:6

And the sons of Ham -- Cush, and Mizraim, Phut, and [Chanaan](../../strongs/g/g5477.md).

<a name="genesis_10_7"></a>Genesis 10:7

And the [huios](../../strongs/g/g5207.md) of Cush -- Seba and Havilah, and Sabtah, and Raamah, and Sabtechah. And the [huios](../../strongs/g/g5207.md) of Raamah - Sheba and Dedan.

<a name="genesis_10_8"></a>Genesis 10:8

And Cush [gennaō](../../strongs/g/g1080.md) Nimrod. This one [archomai](../../strongs/g/g756.md) to be a giant (gigas) upon the [gē](../../strongs/g/g1093.md). 

<a name="genesis_10_9"></a>Genesis 10:9

This one was a giant (gigas) hunter (kynēgos) with [enantion](../../strongs/g/g1726.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md). On account of this they shall [eipon](../../strongs/g/g2046.md), As Nimrod a giant (gigas) hunter (kynēgos) with [enantion](../../strongs/g/g1726.md) the [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_10_10"></a>Genesis 10:10

And [ginomai](../../strongs/g/g1096.md) the [archē](../../strongs/g/g746.md) of his [basileia](../../strongs/g/g932.md) -- [Babylōn](../../strongs/g/g897.md), and Erech, and Accad, and Calneh, in the [gē](../../strongs/g/g1093.md) of Shinar.

<a name="genesis_10_11"></a>Genesis 10:11

From out of that [gē](../../strongs/g/g1093.md) [exerchomai](../../strongs/g/g1831.md) Assyria. And he [oikodomeō](../../strongs/g/g3618.md) [Nineuitēs](../../strongs/g/g3536.md), also in the [mesos](../../strongs/g/g3319.md) of Calah --

<a name="genesis_10_12"></a>Genesis 10:12

this is the [polis](../../strongs/g/g4172.md) [megas](../../strongs/g/g3173.md). 

<a name="genesis_10_13"></a>Genesis 10:13

And Mizraim [gennaō](../../strongs/g/g1080.md) the Ludim, and the Naphtuhim, and the Anamim, and the Lehabim,

<a name="genesis_10_14"></a>Genesis 10:14

and the Pathrusim and the Casluhim, from where came [exerchomai](../../strongs/g/g1831.md) the Philistines, and the Caphtorim.

<a name="genesis_10_15"></a>Genesis 10:15

And [Chanaan](../../strongs/g/g5477.md) [gennaō](../../strongs/g/g1080.md) [Sidōn](../../strongs/g/g4605.md) the [prōtotokos](../../strongs/g/g4416.md), 

<a name="genesis_10_16"></a>Genesis 10:16

and the Hittite, and the Jebusite, and the Amorite, and the Girgasite, and the Hivite, and the Arkite,

<a name="genesis_10_17"></a>Genesis 10:17

and the Sinite,

<a name="genesis_10_18"></a>Genesis 10:18

the Arvadite, and the Zemarite, and the Hamathite, and after these were [diaspeirō](../../strongs/g/g1289.md) the [phylē](../../strongs/g/g5443.md) of the [Chananaios](../../strongs/g/g5478.md).

<a name="genesis_10_19"></a>Genesis 10:19

And were the [horion](../../strongs/g/g3725.md) of the [Chananaios](../../strongs/g/g5478.md) from Sidon unto the [erchomai](../../strongs/g/g2064.md) into Gerar and [Gaza](../../strongs/g/g1048.md), unto the [erchomai](../../strongs/g/g2064.md) unto [Sodoma](../../strongs/g/g4670.md) and [Gomorra](../../strongs/g/g1116.md), Admah and Zeboim, unto Lasha.

<a name="genesis_10_20"></a>Genesis 10:20

These are the [huios](../../strongs/g/g5207.md) of Ham among their [phylē](../../strongs/g/g5443.md), according to their [glōssa](../../strongs/g/g1100.md), among their [chōra](../../strongs/g/g5561.md), and among their [ethnos](../../strongs/g/g1484.md). 

<a name="genesis_10_21"></a>Genesis 10:21

And to [Sēm](../../strongs/g/g4590.md) was [gennaō](../../strongs/g/g1080.md), even to him, the [patēr](../../strongs/g/g3962.md) of all the [huios](../../strongs/g/g5207.md) of [Eber](../../strongs/g/g1443.md), [adelphos](../../strongs/g/g80.md) of Japheth the [megas](../../strongs/g/g3173.md). 

<a name="genesis_10_22"></a>Genesis 10:22

[huios](../../strongs/g/g5207.md) of [Sēm](../../strongs/g/g4590.md) -- Elam, and Asshur, and [Arphaxad](../../strongs/g/g742.md), and Lud, and [Aram](../../strongs/g/g689.md), and [Kaïnam](../../strongs/g/g2536.md).

<a name="genesis_10_23"></a>Genesis 10:23

And the [huios](../../strongs/g/g5207.md) of Aram -- Uz, and Hul, and Gether, and Mash.

<a name="genesis_10_24"></a>Genesis 10:24

And Arphaxad [gennaō](../../strongs/g/g1080.md) Cainan; and Cainan [gennaō](../../strongs/g/g1080.md) Salah; and Salah [gennaō](../../strongs/g/g1080.md) Eber.

<a name="genesis_10_25"></a>Genesis 10:25

"And to [Eber](../../strongs/g/g1443.md) were [gennaō](../../strongs/g/g1080.md) [dyo](../../strongs/g/g1417.md) [huios](../../strongs/g/g5207.md), the [onoma](../../strongs/g/g3686.md) given to the one was Peleg; for in his [hēmera](../../strongs/g/g2250.md) was [diamerizō](../../strongs/g/g1266.md) the [gē](../../strongs/g/g1093.md); and the [onoma](../../strongs/g/g3686.md) of his [adelphos](../../strongs/g/g80.md) was Joktan."

<a name="genesis_10_26"></a>Genesis 10:26

And Joktan [gennaō](../../strongs/g/g1080.md) Almodad, and Sheleph, and Hazarmaveth, and Jerah

<a name="genesis_10_27"></a>Genesis 10:27

and Hadoram, and Uzal, and Diklah,

<a name="genesis_10_28"></a>Genesis 10:28

and Obal and Abimael Sheba,

<a name="genesis_10_29"></a>Genesis 10:29

and Ophir, and Havilah, and Jobab. All these were [huios](../../strongs/g/g5207.md) of Joktan.

<a name="genesis_10_30"></a>Genesis 10:30

And was their [katoikēsis](../../strongs/g/g2731.md) from Mesha unto the [erchomai](../../strongs/g/g2064.md) into Sephar, a [oros](../../strongs/g/g3735.md) of the [anatolē](../../strongs/g/g395.md). 

<a name="genesis_10_31"></a>Genesis 10:31

These are the [huios](../../strongs/g/g5207.md) of [Sēm](../../strongs/g/g4590.md) among their [phylē](../../strongs/g/g5443.md), according to their [glōssa](../../strongs/g/g1100.md), among their [chōra](../../strongs/g/g5561.md), and among their [ethnos](../../strongs/g/g1484.md). 

<a name="genesis_10_32"></a>Genesis 10:32

These are the [phylē](../../strongs/g/g5443.md) of the [huios](../../strongs/g/g5207.md) of [Nōe](../../strongs/g/g3575.md), according to their [genesis](../../strongs/g/g1078.md), according to their [ethnos](../../strongs/g/g1484.md). From these were [diaspeirō](../../strongs/g/g1289.md) [nēsos](../../strongs/g/g3520.md) of the [ethnos](../../strongs/g/g1484.md) upon the [gē](../../strongs/g/g1093.md) after the [kataklysmos](../../strongs/g/g2627.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 9](genesis_lxx_9.md) - [Genesis 11](genesis_lxx_11.md)