# [Genesis 22](https://www.blueletterbible.org/kjv/gen/22/1/s_22001)

<a name="genesis_22_1"></a>Genesis 22:1

And it came to pass ['aḥar](../../strongs/h/h310.md) these [dabar](../../strongs/h/h1697.md), that ['Elohiym](../../strongs/h/h430.md) did [nāsâ](../../strongs/h/h5254.md) ['Abraham](../../strongs/h/h85.md), and ['āmar](../../strongs/h/h559.md) unto him, ['Abraham](../../strongs/h/h85.md): and he ['āmar](../../strongs/h/h559.md), Behold, here I am.

<a name="genesis_22_2"></a>Genesis 22:2

And he ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) now thy [ben](../../strongs/h/h1121.md), thine [yāḥîḏ](../../strongs/h/h3173.md) [Yiṣḥāq](../../strongs/h/h3327.md), whom thou ['ahab](../../strongs/h/h157.md), and [yālaḵ](../../strongs/h/h3212.md) into the ['erets](../../strongs/h/h776.md) of [Môrîyâ](../../strongs/h/h4179.md); and [ʿālâ](../../strongs/h/h5927.md) him there for an [ʿōlâ](../../strongs/h/h5930.md) upon one of the [har](../../strongs/h/h2022.md) which I will ['āmar](../../strongs/h/h559.md) thee of. [^1]

<a name="genesis_22_3"></a>Genesis 22:3

And ['Abraham](../../strongs/h/h85.md) [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [ḥāḇaš](../../strongs/h/h2280.md) his [chamowr](../../strongs/h/h2543.md), and [laqach](../../strongs/h/h3947.md) two of his [naʿar](../../strongs/h/h5288.md) with him, and [Yiṣḥāq](../../strongs/h/h3327.md) his [ben](../../strongs/h/h1121.md), and [bāqaʿ](../../strongs/h/h1234.md) the ['ets](../../strongs/h/h6086.md) for the [ʿōlâ](../../strongs/h/h5930.md), and [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) unto the [maqowm](../../strongs/h/h4725.md) of which ['Elohiym](../../strongs/h/h430.md) had ['āmar](../../strongs/h/h559.md) him.

<a name="genesis_22_4"></a>Genesis 22:4

Then on the third [yowm](../../strongs/h/h3117.md) ['Abraham](../../strongs/h/h85.md) [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md) the [maqowm](../../strongs/h/h4725.md) [rachowq](../../strongs/h/h7350.md).

<a name="genesis_22_5"></a>Genesis 22:5

And ['Abraham](../../strongs/h/h85.md) ['āmar](../../strongs/h/h559.md) unto his [naʿar](../../strongs/h/h5288.md), [yashab](../../strongs/h/h3427.md) ye here with the [chamowr](../../strongs/h/h2543.md); and I and the [naʿar](../../strongs/h/h5288.md) will [yālaḵ](../../strongs/h/h3212.md) [kô](../../strongs/h/h3541.md) and [shachah](../../strongs/h/h7812.md), and [shuwb](../../strongs/h/h7725.md) to you.

<a name="genesis_22_6"></a>Genesis 22:6

And ['Abraham](../../strongs/h/h85.md) [laqach](../../strongs/h/h3947.md) the ['ets](../../strongs/h/h6086.md) of the [ʿōlâ](../../strongs/h/h5930.md), and [śûm](../../strongs/h/h7760.md) it upon [Yiṣḥāq](../../strongs/h/h3327.md) his [ben](../../strongs/h/h1121.md); and he [laqach](../../strongs/h/h3947.md) the ['esh](../../strongs/h/h784.md) in his [yad](../../strongs/h/h3027.md), and a [ma'ăḵeleṯ](../../strongs/h/h3979.md); and they [yālaḵ](../../strongs/h/h3212.md) both of them [yaḥaḏ](../../strongs/h/h3162.md).

<a name="genesis_22_7"></a>Genesis 22:7

And [Yiṣḥāq](../../strongs/h/h3327.md) ['āmar](../../strongs/h/h559.md) unto ['Abraham](../../strongs/h/h85.md) his ['ab](../../strongs/h/h1.md), and said, My ['ab](../../strongs/h/h1.md): and he ['āmar](../../strongs/h/h559.md), Here am I, my [ben](../../strongs/h/h1121.md). And he ['āmar](../../strongs/h/h559.md), Behold the ['esh](../../strongs/h/h784.md) and the ['ets](../../strongs/h/h6086.md): but where is the [śê](../../strongs/h/h7716.md) for an [ʿōlâ](../../strongs/h/h5930.md)?

<a name="genesis_22_8"></a>Genesis 22:8

And ['Abraham](../../strongs/h/h85.md) ['āmar](../../strongs/h/h559.md), My [ben](../../strongs/h/h1121.md), ['Elohiym](../../strongs/h/h430.md) will [ra'ah](../../strongs/h/h7200.md) himself a [śê](../../strongs/h/h7716.md) for an [ʿōlâ](../../strongs/h/h5930.md): so they [yālaḵ](../../strongs/h/h3212.md) both of them [yaḥaḏ](../../strongs/h/h3162.md).

<a name="genesis_22_9"></a>Genesis 22:9

And they [bow'](../../strongs/h/h935.md) to the [maqowm](../../strongs/h/h4725.md) which ['Elohiym](../../strongs/h/h430.md) had ['āmar](../../strongs/h/h559.md) him of; and ['Abraham](../../strongs/h/h85.md) [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) there, and ['arak](../../strongs/h/h6186.md) the ['ets](../../strongs/h/h6086.md), and [ʿāqaḏ](../../strongs/h/h6123.md) [Yiṣḥāq](../../strongs/h/h3327.md) his [ben](../../strongs/h/h1121.md), and [śûm](../../strongs/h/h7760.md) him on the [mizbeach](../../strongs/h/h4196.md) [maʿal](../../strongs/h/h4605.md) the ['ets](../../strongs/h/h6086.md).

<a name="genesis_22_10"></a>Genesis 22:10

And ['Abraham](../../strongs/h/h85.md) [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md), and [laqach](../../strongs/h/h3947.md) the [ma'ăḵeleṯ](../../strongs/h/h3979.md) to [šāḥaṭ](../../strongs/h/h7819.md) his [ben](../../strongs/h/h1121.md).

<a name="genesis_22_11"></a>Genesis 22:11

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [qara'](../../strongs/h/h7121.md) unto him out of [shamayim](../../strongs/h/h8064.md), and ['āmar](../../strongs/h/h559.md), ['Abraham](../../strongs/h/h85.md), ['Abraham](../../strongs/h/h85.md): and he ['āmar](../../strongs/h/h559.md), Here am I.

<a name="genesis_22_12"></a>Genesis 22:12

And he ['āmar](../../strongs/h/h559.md), [shalach](../../strongs/h/h7971.md) not thine [yad](../../strongs/h/h3027.md) upon the [naʿar](../../strongs/h/h5288.md), neither ['asah](../../strongs/h/h6213.md) thou [mᵊ'ûmâ](../../strongs/h/h3972.md) unto him: for [ʿatâ](../../strongs/h/h6258.md) I [yada'](../../strongs/h/h3045.md) that thou [yārē'](../../strongs/h/h3373.md) ['Elohiym](../../strongs/h/h430.md), seeing thou hast not [ḥāśaḵ](../../strongs/h/h2820.md) thy [ben](../../strongs/h/h1121.md), thine [yāḥîḏ](../../strongs/h/h3173.md) from me.

<a name="genesis_22_13"></a>Genesis 22:13

And ['Abraham](../../strongs/h/h85.md) [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md), and behold ['aḥar](../../strongs/h/h310.md) him an ['ayil](../../strongs/h/h352.md) ['āḥaz](../../strongs/h/h270.md) in a [sᵊḇāḵ](../../strongs/h/h5442.md) by his [qeren](../../strongs/h/h7161.md): and ['Abraham](../../strongs/h/h85.md) [yālaḵ](../../strongs/h/h3212.md) and [laqach](../../strongs/h/h3947.md) the ['ayil](../../strongs/h/h352.md), and [ʿālâ](../../strongs/h/h5927.md) him up for an [ʿōlâ](../../strongs/h/h5930.md) in the [taḥaṯ](../../strongs/h/h8478.md) of his [ben](../../strongs/h/h1121.md).

<a name="genesis_22_14"></a>Genesis 22:14

And ['Abraham](../../strongs/h/h85.md) [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of that [maqowm](../../strongs/h/h4725.md) [Yᵊhōvâ yir'ê](../../strongs/h/h3070.md): as it is ['āmar](../../strongs/h/h559.md) to this [yowm](../../strongs/h/h3117.md), In the [har](../../strongs/h/h2022.md) of [Yĕhovah](../../strongs/h/h3068.md) it shall be [ra'ah](../../strongs/h/h7200.md).

<a name="genesis_22_15"></a>Genesis 22:15

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [qara'](../../strongs/h/h7121.md) unto ['Abraham](../../strongs/h/h85.md) out of [shamayim](../../strongs/h/h8064.md) the [šēnî](../../strongs/h/h8145.md) time,

<a name="genesis_22_16"></a>Genesis 22:16

And ['āmar](../../strongs/h/h559.md), By myself have I [shaba'](../../strongs/h/h7650.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), for because thou hast ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md), and hast not [ḥāśaḵ](../../strongs/h/h2820.md) thy [ben](../../strongs/h/h1121.md), thine [yāḥîḏ](../../strongs/h/h3173.md):

<a name="genesis_22_17"></a>Genesis 22:17

That in [barak](../../strongs/h/h1288.md) I will [barak](../../strongs/h/h1288.md) thee, and in [rabah](../../strongs/h/h7235.md) I will [rabah](../../strongs/h/h7235.md) thy [zera'](../../strongs/h/h2233.md) as the [kowkab](../../strongs/h/h3556.md) of the [shamayim](../../strongs/h/h8064.md), and as the [ḥôl](../../strongs/h/h2344.md) which is upon the [yam](../../strongs/h/h3220.md) [saphah](../../strongs/h/h8193.md); and thy [zera'](../../strongs/h/h2233.md) shall [yarash](../../strongs/h/h3423.md) the [sha'ar](../../strongs/h/h8179.md) of his ['oyeb](../../strongs/h/h341.md);

<a name="genesis_22_18"></a>Genesis 22:18

And in thy [zera'](../../strongs/h/h2233.md) shall all the [gowy](../../strongs/h/h1471.md) of the ['erets](../../strongs/h/h776.md) be [barak](../../strongs/h/h1288.md); because thou hast [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md).

<a name="genesis_22_19"></a>Genesis 22:19

So ['Abraham](../../strongs/h/h85.md) [shuwb](../../strongs/h/h7725.md) unto his [naʿar](../../strongs/h/h5288.md), and they [quwm](../../strongs/h/h6965.md) and [yālaḵ](../../strongs/h/h3212.md) [yaḥaḏ](../../strongs/h/h3162.md) to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md); and ['Abraham](../../strongs/h/h85.md) [yashab](../../strongs/h/h3427.md) at [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md).

<a name="genesis_22_20"></a>Genesis 22:20

And it came to pass ['aḥar](../../strongs/h/h310.md) these [dabar](../../strongs/h/h1697.md), that it was [nāḡaḏ](../../strongs/h/h5046.md) ['Abraham](../../strongs/h/h85.md), ['āmar](../../strongs/h/h559.md), Behold, [Milkâ](../../strongs/h/h4435.md), she hath also [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) unto thy ['ach](../../strongs/h/h251.md) [Nāḥôr](../../strongs/h/h5152.md);

<a name="genesis_22_21"></a>Genesis 22:21

[ʿÛṣ](../../strongs/h/h5780.md) his [bᵊḵôr](../../strongs/h/h1060.md), and [Bûz](../../strongs/h/h938.md) his ['ach](../../strongs/h/h251.md), and [Qᵊmû'ēl](../../strongs/h/h7055.md) the ['ab](../../strongs/h/h1.md) of ['Ărām](../../strongs/h/h758.md),

<a name="genesis_22_22"></a>Genesis 22:22

And [keśeḏ](../../strongs/h/h3777.md), and [ḥăzô](../../strongs/h/h2375.md), and [pildāš](../../strongs/h/h6394.md), and [yiḏlāp̄](../../strongs/h/h3044.md), and [bᵊṯû'ēl](../../strongs/h/h1328.md).

<a name="genesis_22_23"></a>Genesis 22:23

And [bᵊṯû'ēl](../../strongs/h/h1328.md) [yalad](../../strongs/h/h3205.md) [riḇqâ](../../strongs/h/h7259.md): these eight [Milkâ](../../strongs/h/h4435.md) did [yalad](../../strongs/h/h3205.md) to [Nāḥôr](../../strongs/h/h5152.md), ['Abraham](../../strongs/h/h85.md) ['ach](../../strongs/h/h251.md).

<a name="genesis_22_24"></a>Genesis 22:24

And his [Pîleḡeš](../../strongs/h/h6370.md), whose [shem](../../strongs/h/h8034.md) was [Rᵊ'ûmâ](../../strongs/h/h7208.md), she [yalad](../../strongs/h/h3205.md) also [Ṭeḇaḥ](../../strongs/h/h2875.md), and [Gaḥam](../../strongs/h/h1514.md), and [Taḥaš](../../strongs/h/h8477.md), and [Maʿăḵâ](../../strongs/h/h4601.md).

---

[^1]: [Genesis 22:2 Commentary](../../commentary/genesis/genesis_22_commentary.md#genesis_22_2)

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 21](genesis_21.md) - [Genesis 23](genesis_23.md)