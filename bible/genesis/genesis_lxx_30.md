# [Genesis 30](https://www.blueletterbible.org/lxx/genesis/30)

<a name="genesis_30_1"></a>Genesis 30:1

[eidō](../../strongs/g/g1492.md) [Rhachēl](../../strongs/g/g4478.md) that she has not [tiktō](../../strongs/g/g5088.md) to [Iakōb](../../strongs/g/g2384.md). And [Rhachēl](../../strongs/g/g4478.md) [zēloō](../../strongs/g/g2206.md) her [adelphē](../../strongs/g/g79.md). And she [eipon](../../strongs/g/g2036.md) to [Iakōb](../../strongs/g/g2384.md), [didōmi](../../strongs/g/g1325.md) to me a [teknon](../../strongs/g/g5043.md)! and if not, I will [teleutaō](../../strongs/g/g5053.md).

<a name="genesis_30_2"></a>Genesis 30:2

being [thymoō](../../strongs/g/g2373.md) [Iakōb](../../strongs/g/g2384.md) with [Rhachēl](../../strongs/g/g4478.md), [eipon](../../strongs/g/g2036.md) to her, not in place of [theos](../../strongs/g/g2316.md) I am who [stereoō](../../strongs/g/g4732.md) you of the [karpos](../../strongs/g/g2590.md) of the [koilia](../../strongs/g/g2836.md)? 

<a name="genesis_30_3"></a>Genesis 30:3

[eipon](../../strongs/g/g2036.md) [Rhachēl](../../strongs/g/g4478.md) to [Iakōb](../../strongs/g/g2384.md), [idou](../../strongs/g/g2400.md), my [paidiskē](../../strongs/g/g3814.md) Bilhah. [eiserchomai](../../strongs/g/g1525.md) to her! and she will [tiktō](../../strongs/g/g5088.md) upon my [gony](../../strongs/g/g1119.md), and will produce children (teknopoiēsomai) even I from her. 

<a name="genesis_30_4"></a>Genesis 30:4

And she [didōmi](../../strongs/g/g1325.md) Bilhah to him -- her [paidiskē](../../strongs/g/g3814.md) to him as [gynē](../../strongs/g/g1135.md). And [eiserchomai](../../strongs/g/g1525.md) to her [Iakōb](../../strongs/g/g2384.md).

<a name="genesis_30_5"></a>Genesis 30:5

And [syllambanō](../../strongs/g/g4815.md) Bilhah the [paidiskē](../../strongs/g/g3814.md) of [Rhachēl](../../strongs/g/g4478.md). And she [tiktō](../../strongs/g/g5088.md) to [Iakōb](../../strongs/g/g2384.md) a [huios](../../strongs/g/g5207.md). 

<a name="genesis_30_6"></a>Genesis 30:6

And [Rhachēl](../../strongs/g/g4478.md) [eipon](../../strongs/g/g2036.md), [krinō](../../strongs/g/g2919.md) me [theos](../../strongs/g/g2316.md), and he [epakouō](../../strongs/g/g1873.md) my [phōnē](../../strongs/g/g5456.md), and he [didōmi](../../strongs/g/g1325.md) to me a [huios](../../strongs/g/g5207.md) through this one . On account of this she [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md), Dan.

<a name="genesis_30_7"></a>Genesis 30:7

And [syllambanō](../../strongs/g/g4815.md) again Bilhah the [paidiskē](../../strongs/g/g3814.md) of [Rhachēl](../../strongs/g/g4478.md). And she [tiktō](../../strongs/g/g5088.md) [huios](../../strongs/g/g5207.md) a [deuteros](../../strongs/g/g1208.md) to [Iakōb](../../strongs/g/g2384.md).

<a name="genesis_30_8"></a>Genesis 30:8

And [Rhachēl](../../strongs/g/g4478.md) [eipon](../../strongs/g/g2036.md), [syllambanō](../../strongs/g/g4815.md) me [theos](../../strongs/g/g2316.md), and I was [systrephō](../../strongs/g/g4962.md) by my [adelphē](../../strongs/g/g79.md), and I was able. And she [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md), [Nephthalim](../../strongs/g/g3508.md).

<a name="genesis_30_9"></a>Genesis 30:9

[eidō](../../strongs/g/g1492.md) Leah that she [histēmi](../../strongs/g/g2476.md) [tiktō](../../strongs/g/g5088.md). And she [lambanō](../../strongs/g/g2983.md) Zilpah her [paidiskē](../../strongs/g/g3814.md), and [didōmi](../../strongs/g/g1325.md) her to [Iakōb](../../strongs/g/g2384.md) as [gynē](../../strongs/g/g1135.md). 

<a name="genesis_30_10"></a>Genesis 30:10

And [syllambanō](../../strongs/g/g4815.md) Zilpah the [paidiskē](../../strongs/g/g3814.md) of Leah. And she [tiktō](../../strongs/g/g5088.md) to [Iakōb](../../strongs/g/g2384.md) a [huios](../../strongs/g/g5207.md). 

<a name="genesis_30_11"></a>Genesis 30:11

And Leah [eipon](../../strongs/g/g2036.md), I am in good luck (tychē). And she [eponomazō](../../strongs/g/g2028.md) his [onoma](../../strongs/g/g3686.md), [Gad](../../strongs/g/g1045.md).

<a name="genesis_30_12"></a>Genesis 30:12

And [syllambanō](../../strongs/g/g4815.md) again Zilpah the [paidiskē](../../strongs/g/g3814.md) of Leah. And she [tiktō](../../strongs/g/g5088.md) to [Iakōb](../../strongs/g/g2384.md) [huios](../../strongs/g/g5207.md) a [deuteros](../../strongs/g/g1208.md). 

<a name="genesis_30_13"></a>Genesis 30:13

And Leah [eipon](../../strongs/g/g2036.md), [makarios](../../strongs/g/g3107.md) am I, for [makarizō](../../strongs/g/g3106.md) me the [gynē](../../strongs/g/g1135.md). And she [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md), [Asēr](../../strongs/g/g768.md).

<a name="genesis_30_14"></a>Genesis 30:14

[poreuō](../../strongs/g/g4198.md) [Rhoubēn](../../strongs/g/g4502.md) in the [hēmera](../../strongs/g/g2250.md) [therismos](../../strongs/g/g2326.md) of the wheat (pyrōn), and he [heuriskō](../../strongs/g/g2147.md) apples (mēla) of mandrakes (mandragorou) in the [agros](../../strongs/g/g68.md). And he [pherō](../../strongs/g/g5342.md) them to Leah his [mētēr](../../strongs/g/g3384.md). [eipon](../../strongs/g/g2036.md) [Rhachēl](../../strongs/g/g4478.md) to Leah her [adelphē](../../strongs/g/g79.md), [didōmi](../../strongs/g/g1325.md) to me of the mandrakes (mandragorōn) of your [huios](../../strongs/g/g5207.md)!

<a name="genesis_30_15"></a>Genesis 30:15

[eipon](../../strongs/g/g2036.md) Leah, Is it not [hikanos](../../strongs/g/g2425.md) to you that you [lambanō](../../strongs/g/g2983.md) my [anēr](../../strongs/g/g435.md), shall also of the mandrakes (mandragorōn) of my [huios](../../strongs/g/g5207.md) you [lambanō](../../strongs/g/g2983.md)? [eipon](../../strongs/g/g2036.md) [Rhachēl](../../strongs/g/g4478.md), Not so, let him go to [koimaō](../../strongs/g/g2837.md) with you this [nyx](../../strongs/g/g3571.md) in return for the mandrakes (mandragorōn) of your [huios](../../strongs/g/g5207.md).

<a name="genesis_30_16"></a>Genesis 30:16

[eiserchomai](../../strongs/g/g1525.md) [Iakōb](../../strongs/g/g2384.md) from out of the [agros](../../strongs/g/g68.md) at [hespera](../../strongs/g/g2073.md). And [exerchomai](../../strongs/g/g1831.md) Leah to [synantēsis](../../strongs/g/g4877.md) him. And she [eipon](../../strongs/g/g2036.md), to me You shall [eiserchomai](../../strongs/g/g1525.md) [sēmeron](../../strongs/g/g4594.md), for I have hired (memisthōmai) you in return for the mandrakes (mandragorōn) of my [huios](../../strongs/g/g5207.md). And he went to [koimaō](../../strongs/g/g2837.md) with her that [nyx](../../strongs/g/g3571.md). 

<a name="genesis_30_17"></a>Genesis 30:17

And [theos](../../strongs/g/g2316.md) [epakouō](../../strongs/g/g1873.md) Leah. And [syllambanō](../../strongs/g/g4815.md), she [tiktō](../../strongs/g/g5088.md) [huios](../../strongs/g/g5207.md) a [pemptos](../../strongs/g/g3991.md) to [Iakōb](../../strongs/g/g2384.md).

<a name="genesis_30_18"></a>Genesis 30:18

And Leah [eipon](../../strongs/g/g2036.md), [didōmi](../../strongs/g/g1325.md) to me [theos](../../strongs/g/g2316.md) my [misthos](../../strongs/g/g3408.md) because I [didōmi](../../strongs/g/g1325.md) my [paidiskē](../../strongs/g/g3814.md) to my [anēr](../../strongs/g/g435.md). And she [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md), Issachar, which is, [misthos](../../strongs/g/g3408.md). 

<a name="genesis_30_19"></a>Genesis 30:19

And [syllambanō](../../strongs/g/g4815.md) again Leah. And she [tiktō](../../strongs/g/g5088.md) [huios](../../strongs/g/g5207.md) a [ektos](../../strongs/g/g1623.md) to [Iakōb](../../strongs/g/g2384.md).

<a name="genesis_30_20"></a>Genesis 30:20

And Leah [eipon](../../strongs/g/g2036.md), [theos](../../strongs/g/g2316.md) has [dōron](../../strongs/g/g1435.md) to me [dōron](../../strongs/g/g1435.md) a [kalos](../../strongs/g/g2570.md) in the [nyn](../../strongs/g/g3568.md) [kairos](../../strongs/g/g2540.md), will [hairetizō](../../strongs/g/g140.md) me my [anēr](../../strongs/g/g435.md) for having [tiktō](../../strongs/g/g5088.md) to him [ex](../../strongs/g/g1803.md) [huios](../../strongs/g/g5207.md). And she [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md), [Zaboulōn](../../strongs/g/g2194.md).

<a name="genesis_30_21"></a>Genesis 30:21

And after this she [tiktō](../../strongs/g/g5088.md) a [thygatēr](../../strongs/g/g2364.md), and she [kaleō](../../strongs/g/g2564.md) her [onoma](../../strongs/g/g3686.md), Dinah.

<a name="genesis_30_22"></a>Genesis 30:22

[remembered [mimnēskomai](../../strongs/g/g3403.md) And God] [theos](../../strongs/g/g2316.md) Rachel. And [heeded [epakouō](../../strongs/g/g1873.md) her God] [theos](../../strongs/g/g2316.md), and he [anoigō](../../strongs/g/g455.md) her [mētra](../../strongs/g/g3388.md). 

<a name="genesis_30_23"></a>Genesis 30:23

And [syllambanō](../../strongs/g/g4815.md), she [tiktō](../../strongs/g/g5088.md) to Jacob a [huios](../../strongs/g/g5207.md). [said [eipon](../../strongs/g/g2036.md) And Rachel], God removed from me the [oneidos](../../strongs/g/g3681.md). 

<a name="genesis_30_24"></a>Genesis 30:24

And she [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md) Joseph, [legō](../../strongs/g/g3004.md), God added to me [son [huios](../../strongs/g/g5207.md) another] ! 

<a name="genesis_30_25"></a>Genesis 30:25

And it came to pass when Rachel [tiktō](../../strongs/g/g5088.md) Joseph, Jacob [eipon](../../strongs/g/g2036.md) to Laban, [apostellō](../../strongs/g/g649.md) me that I may go [aperchomai](../../strongs/g/g565.md) into my [topos](../../strongs/g/g5117.md), and unto my [gē](../../strongs/g/g1093.md)! 

<a name="genesis_30_26"></a>Genesis 30:26

[apodidōmi](../../strongs/g/g591.md) to me my [gynē](../../strongs/g/g1135.md), and my [paidion](../../strongs/g/g3813.md)! for whom I have [douleuō](../../strongs/g/g1398.md) to you, that I might go [aperchomai](../../strongs/g/g565.md). For you [ginōskō](../../strongs/g/g1097.md) the [douleia](../../strongs/g/g1397.md) which I have [douleuō](../../strongs/g/g1398.md) you. 

<a name="genesis_30_27"></a>Genesis 30:27

[said [eipon](../../strongs/g/g2036.md) And to him Laban], If I [heuriskō](../../strongs/g/g2147.md) favor [enantion](../../strongs/g/g1726.md) you, could I foretell even -- [blessed [eulogeō](../../strongs/g/g2127.md) me God] [theos](../../strongs/g/g2316.md) in your [eisodos](../../strongs/g/g1529.md). 

<a name="genesis_30_28"></a>Genesis 30:28

You [diastellō](../../strongs/g/g1291.md) your [misthos](../../strongs/g/g3408.md) from me! and I will give it [didōmi](../../strongs/g/g1325.md). 

<a name="genesis_30_29"></a>Genesis 30:29

[said [eipon](../../strongs/g/g2036.md) And Jacob] to him, You [ginōskō](../../strongs/g/g1097.md) in what I [douleuō](../../strongs/g/g1398.md) you, and how much [was [ktēnos](../../strongs/g/g2934.md) of your] with me. 

<a name="genesis_30_30"></a>Genesis 30:30

[little [mikron](../../strongs/g/g3397.md) For there was] as much as was to you [enantion](../../strongs/g/g1726.md) me, and it was [auxanō](../../strongs/g/g837.md) into a [plēthos](../../strongs/g/g4128.md); and [blessed [eulogeō](../../strongs/g/g2127.md) you the lord ] [kyrios](../../strongs/g/g2962.md) by my [pous](../../strongs/g/g4228.md). Now then, when shall [produce [poieō](../../strongs/g/g4160.md) I also] for myself a [oikos](../../strongs/g/g3624.md)? 

<a name="genesis_30_31"></a>Genesis 30:31

And Laban [eipon](../../strongs/g/g2036.md) to him, What shall I give to you? [said [eipon](../../strongs/g/g2036.md) And to him Jacob], You shall not give to me anything. If you should [poieō](../../strongs/g/g4160.md) for me this [rhēma](../../strongs/g/g4487.md), again I will [poimainō](../../strongs/g/g4165.md) your [probaton](../../strongs/g/g4263.md), and guard them [phylassō](../../strongs/g/g5442.md). 

<a name="genesis_30_32"></a>Genesis 30:32

Let [go [parerchomai](../../strongs/g/g3928.md) all your flocks] [probaton](../../strongs/g/g4263.md) [sēmeron](../../strongs/g/g4594.md), and you [diachōrizō](../../strongs/g/g1316.md) from there every [sheep [probaton](../../strongs/g/g4263.md) gray] among the [arēn](../../strongs/g/g704.md), and all white-mixed and speckled among the goats! It will be to me a [misthos](../../strongs/g/g3408.md). 

<a name="genesis_30_33"></a>Genesis 30:33

And [will take [epakouō](../../strongs/g/g1873.md) to me my righteousness] [dikaiosynē](../../strongs/g/g1343.md) in the [day next] [epaurion](../../strongs/g/g1887.md); for [is my wage] [misthos](../../strongs/g/g3408.md) before you -- all which might not be speckled and white-mixed among the goats, and gray among the [arēn](../../strongs/g/g704.md), [stolen [kleptō](../../strongs/g/g2813.md) will be as ] by me. 

<a name="genesis_30_34"></a>Genesis 30:34

[said [eipon](../../strongs/g/g2036.md) And to him Laban], Let it be according to your [rhēma](../../strongs/g/g4487.md)! 

<a name="genesis_30_35"></a>Genesis 30:35

And he drew [diastellō](../../strongs/g/g1291.md) in that day the [he-goats [tragos](../../strongs/g/g5131.md) speckled], and the white-mixed, and all the goats, the speckled and the white-mixed, and all gray, the one which was among the [arēn](../../strongs/g/g704.md). And he [didōmi](../../strongs/g/g1325.md) by the [cheir](../../strongs/g/g5495.md) of his sons.

<a name="genesis_30_36"></a>Genesis 30:36

And he [aphistēmi](../../strongs/g/g868.md) a [hodos](../../strongs/g/g3598.md) of three days between them and between Jacob. And Jacob [poimainō](../../strongs/g/g4165.md) the [probaton](../../strongs/g/g4263.md) of Laban -- the ones left [hypoleipō](../../strongs/g/g5275.md). 

<a name="genesis_30_37"></a>Genesis 30:37

[took [lambanō](../../strongs/g/g2983.md) And to himself Jacob] a [rhabdos](../../strongs/g/g4464.md) [poplar of green] [chlōros](../../strongs/g/g5515.md), and of walnut, and of the plane tree. And [peeled them Jacob peels into white] [leukos](../../strongs/g/g3022.md), tearing away the [chlōros](../../strongs/g/g5515.md). And there appeared upon the [rhabdos](../../strongs/g/g4464.md) the [leukos](../../strongs/g/g3022.md) which he peeled -- [poikilos](../../strongs/g/g4164.md). 

<a name="genesis_30_38"></a>Genesis 30:38

And he [paratithēmi](../../strongs/g/g3908.md) the [rhabdos](../../strongs/g/g4464.md) which he peeled at the watering [lēnos](../../strongs/g/g3025.md) of the channels of the [hydōr](../../strongs/g/g5204.md). That whenever [came [erchomai](../../strongs/g/g2064.md) the flocks] [probaton](../../strongs/g/g4263.md) to [pinō](../../strongs/g/g4095.md) in front of the [rhabdos](../../strongs/g/g4464.md) of those having come to [pinō](../../strongs/g/g4095.md), the rods should stimulate the [probaton](../../strongs/g/g4263.md) at the [rhabdos](../../strongs/g/g4464.md). 

<a name="genesis_30_39"></a>Genesis 30:39

And [bore [tiktō](../../strongs/g/g5088.md) the flocks] [probaton](../../strongs/g/g4263.md) white-mixed, and [poikilos](../../strongs/g/g4164.md), and ashen speckled.

<a name="genesis_30_40"></a>Genesis 30:40

[the And [amnos](../../strongs/g/g286.md) drew [diastellō](../../strongs/g/g1291.md) Jacob]. And he [histēmi](../../strongs/g/g2476.md) [before [enantion](../../strongs/g/g1726.md) the [probaton](../../strongs/g/g4263.md) a ram white-mixed], and every colored [poikilos](../../strongs/g/g4164.md) among the [amnos](../../strongs/g/g286.md). And he [diachōrizō](../../strongs/g/g1316.md) to himself [poimnion](../../strongs/g/g4168.md) for himself, and did not [mignymi](../../strongs/g/g3396.md) them into the [probaton](../../strongs/g/g4263.md) of Laban.

<a name="genesis_30_41"></a>Genesis 30:41

And it came to pass in the [kairos](../../strongs/g/g2540.md) [which he stimulated the flocks] [probaton](../../strongs/g/g4263.md) in the [gastēr](../../strongs/g/g1064.md), [taking [lambanō](../../strongs/g/g2983.md) [tithēmi](../../strongs/g/g5087.md) Jacob] the [rhabdos](../../strongs/g/g4464.md) [enantion](../../strongs/g/g1726.md) the [probaton](../../strongs/g/g4263.md) at the watering [lēnos](../../strongs/g/g3025.md) for the purpose of stimulating them by the [rhabdos](../../strongs/g/g4464.md). 

<a name="genesis_30_42"></a>Genesis 30:42

But whenever [bore [tiktō](../../strongs/g/g5088.md) the flocks] [probaton](../../strongs/g/g4263.md) he did not place them [tithēmi](../../strongs/g/g5087.md). [became And the unmarked] [asēmos](../../strongs/g/g767.md) Laban's, and the [episēmos](../../strongs/g/g1978.md) Jacob's.

<a name="genesis_30_43"></a>Genesis 30:43

And [became [plouteō](../../strongs/g/g4147.md) the man] [anthrōpos](../../strongs/g/g444.md) [sphodra](../../strongs/g/g4970.md), [sphodra](../../strongs/g/g4970.md). And there was to him [cattle [ktēnos](../../strongs/g/g2934.md) much] [polys](../../strongs/g/g4183.md), and [bous](../../strongs/g/g1016.md), and [pais](../../strongs/g/g3816.md), and [paidiskē](../../strongs/g/g3814.md), and [kamēlos](../../strongs/g/g2574.md), and [onos](../../strongs/g/g3688.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 29](genesis_lxx_29.md) - [Genesis 31](genesis_lxx_31.md)