# [Genesis 25](https://www.blueletterbible.org/kjv/gen/25/1/s_25001)

<a name="genesis_25_1"></a>Genesis 25:1

Then [yāsap̄](../../strongs/h/h3254.md) ['Abraham](../../strongs/h/h85.md) [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md), and her [shem](../../strongs/h/h8034.md) was [Qᵊṭûrâ](../../strongs/h/h6989.md).

<a name="genesis_25_2"></a>Genesis 25:2

And she [yalad](../../strongs/h/h3205.md) him [Zimrān](../../strongs/h/h2175.md), and [Yāqšān](../../strongs/h/h3370.md), and [Mᵊḏān](../../strongs/h/h4091.md), and [Miḏyān](../../strongs/h/h4080.md), and [Yišbāq](../../strongs/h/h3435.md), and [Šûaḥ](../../strongs/h/h7744.md).

<a name="genesis_25_3"></a>Genesis 25:3

And [Yāqšān](../../strongs/h/h3370.md) [yalad](../../strongs/h/h3205.md) [Šᵊḇā'](../../strongs/h/h7614.md), and [Dᵊḏān](../../strongs/h/h1719.md). And the [ben](../../strongs/h/h1121.md) of [Dᵊḏān](../../strongs/h/h1719.md) were ['aššûrî](../../strongs/h/h805.md), and [lᵊṭûšim](../../strongs/h/h3912.md), and [lᵊ'ummîm](../../strongs/h/h3817.md).

<a name="genesis_25_4"></a>Genesis 25:4

And the [ben](../../strongs/h/h1121.md) of [Miḏyān](../../strongs/h/h4080.md); [ʿÊp̄â](../../strongs/h/h5891.md), and [ʿĒp̄er](../../strongs/h/h6081.md), and [Ḥănôḵ](../../strongs/h/h2585.md), and ['Ăḇîḏāʿ](../../strongs/h/h28.md), and ['Eldāʿâ](../../strongs/h/h420.md). All these were the [ben](../../strongs/h/h1121.md) of [Qᵊṭûrâ](../../strongs/h/h6989.md).

<a name="genesis_25_5"></a>Genesis 25:5

And ['Abraham](../../strongs/h/h85.md) [nathan](../../strongs/h/h5414.md) all that he had unto [Yiṣḥāq](../../strongs/h/h3327.md).

<a name="genesis_25_6"></a>Genesis 25:6

But unto the [ben](../../strongs/h/h1121.md) of the [pîleḡeš](../../strongs/h/h6370.md), which ['Abraham](../../strongs/h/h85.md) had, ['Abraham](../../strongs/h/h85.md) [nathan](../../strongs/h/h5414.md) [matānâ](../../strongs/h/h4979.md), and [shalach](../../strongs/h/h7971.md) from [Yiṣḥāq](../../strongs/h/h3327.md) his [ben](../../strongs/h/h1121.md), while he yet [chay](../../strongs/h/h2416.md), [qeḏem](../../strongs/h/h6924.md), unto the [qeḏem](../../strongs/h/h6924.md) ['erets](../../strongs/h/h776.md).

<a name="genesis_25_7"></a>Genesis 25:7

And these are the [yowm](../../strongs/h/h3117.md) of the [šānâ](../../strongs/h/h8141.md) of ['Abraham](../../strongs/h/h85.md) [chay](../../strongs/h/h2416.md) which he [chayay](../../strongs/h/h2425.md), an hundred threescore and fifteen [šānâ](../../strongs/h/h8141.md).

<a name="genesis_25_8"></a>Genesis 25:8

Then ['Abraham](../../strongs/h/h85.md) [gāvaʿ](../../strongs/h/h1478.md), and [muwth](../../strongs/h/h4191.md) in a [towb](../../strongs/h/h2896.md) [śêḇâ](../../strongs/h/h7872.md), a [zāqēn](../../strongs/h/h2205.md), and [śāḇēaʿ](../../strongs/h/h7649.md) of years; and was ['āsap̄](../../strongs/h/h622.md) to his ['am](../../strongs/h/h5971.md).

<a name="genesis_25_9"></a>Genesis 25:9

And his [ben](../../strongs/h/h1121.md) [Yiṣḥāq](../../strongs/h/h3327.md) and [Yišmāʿē'l](../../strongs/h/h3458.md) [qāḇar](../../strongs/h/h6912.md) him in the [mᵊʿārâ](../../strongs/h/h4631.md) of [maḵpēlâ](../../strongs/h/h4375.md), in the [sadeh](../../strongs/h/h7704.md) of [ʿep̄rôn](../../strongs/h/h6085.md) the [ben](../../strongs/h/h1121.md) of [Ṣōḥar](../../strongs/h/h6714.md) the [Ḥitî](../../strongs/h/h2850.md), which is [paniym](../../strongs/h/h6440.md) [mamrē'](../../strongs/h/h4471.md);

<a name="genesis_25_10"></a>Genesis 25:10

The [sadeh](../../strongs/h/h7704.md) which ['Abraham](../../strongs/h/h85.md) [qānâ](../../strongs/h/h7069.md) of the [ben](../../strongs/h/h1121.md) of [Ḥēṯ](../../strongs/h/h2845.md): there was ['Abraham](../../strongs/h/h85.md) [qāḇar](../../strongs/h/h6912.md), and [Śārâ](../../strongs/h/h8283.md) his ['ishshah](../../strongs/h/h802.md).

<a name="genesis_25_11"></a>Genesis 25:11

And it came to pass ['aḥar](../../strongs/h/h310.md) the [maveth](../../strongs/h/h4194.md) of ['Abraham](../../strongs/h/h85.md), that ['Elohiym](../../strongs/h/h430.md) [barak](../../strongs/h/h1288.md) his [ben](../../strongs/h/h1121.md) [Yiṣḥāq](../../strongs/h/h3327.md); and [Yiṣḥāq](../../strongs/h/h3327.md) [yashab](../../strongs/h/h3427.md) by [bᵊ'ēr laḥay rō'î](../../strongs/h/h883.md).

<a name="genesis_25_12"></a>Genesis 25:12

Now these are the [towlĕdah](../../strongs/h/h8435.md) of [Yišmāʿē'l](../../strongs/h/h3458.md), ['Abraham](../../strongs/h/h85.md) [ben](../../strongs/h/h1121.md), whom [Hāḡār](../../strongs/h/h1904.md) the [Miṣrî](../../strongs/h/h4713.md), [Śārâ](../../strongs/h/h8283.md) [šip̄ḥâ](../../strongs/h/h8198.md), [yalad](../../strongs/h/h3205.md) unto ['Abraham](../../strongs/h/h85.md):

<a name="genesis_25_13"></a>Genesis 25:13

And these are the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of [Yišmāʿē'l](../../strongs/h/h3458.md), by their [shem](../../strongs/h/h8034.md), according to their [towlĕdah](../../strongs/h/h8435.md): the [bᵊḵôr](../../strongs/h/h1060.md) of [Yišmāʿē'l](../../strongs/h/h3458.md), [Nᵊḇāyôṯ](../../strongs/h/h5032.md); and [Qēḏār](../../strongs/h/h6938.md), and ['Aḏbᵊ'ēl](../../strongs/h/h110.md), and [Miḇśām](../../strongs/h/h4017.md),

<a name="genesis_25_14"></a>Genesis 25:14

And [Mišmāʿ](../../strongs/h/h4927.md), and [Dûmâ](../../strongs/h/h1746.md), and [Maśśā'](../../strongs/h/h4854.md),

<a name="genesis_25_15"></a>Genesis 25:15

[ḥăḏar](../../strongs/h/h2316.md), and [Têmā'](../../strongs/h/h8485.md), [Yᵊṭûr](../../strongs/h/h3195.md), [Nāp̄îš](../../strongs/h/h5305.md), and [Qēḏmâ](../../strongs/h/h6929.md):

<a name="genesis_25_16"></a>Genesis 25:16

These are the [ben](../../strongs/h/h1121.md) of [Yišmāʿē'l](../../strongs/h/h3458.md), and these are their [shem](../../strongs/h/h8034.md), by their [ḥāṣēr](../../strongs/h/h2691.md), and by their [ṭîrâ](../../strongs/h/h2918.md); twelve [nāśî'](../../strongs/h/h5387.md) according to their ['ummah](../../strongs/h/h523.md).

<a name="genesis_25_17"></a>Genesis 25:17

And these are the [šānâ](../../strongs/h/h8141.md) of the [chay](../../strongs/h/h2416.md) of [Yišmāʿē'l](../../strongs/h/h3458.md), an hundred and thirty and seven years: and he [gāvaʿ](../../strongs/h/h1478.md) and [muwth](../../strongs/h/h4191.md); and was ['āsap̄](../../strongs/h/h622.md) unto his ['am](../../strongs/h/h5971.md).

<a name="genesis_25_18"></a>Genesis 25:18

And they [shakan](../../strongs/h/h7931.md) from [Ḥăvîlâ](../../strongs/h/h2341.md) unto [šûr](../../strongs/h/h7793.md), that is before [Mitsrayim](../../strongs/h/h4714.md), as thou [bow'](../../strongs/h/h935.md) toward ['Aššûr](../../strongs/h/h804.md): and he [naphal](../../strongs/h/h5307.md) in the [paniym](../../strongs/h/h6440.md) of all his ['ach](../../strongs/h/h251.md).

<a name="genesis_25_19"></a>Genesis 25:19

And these are the [towlĕdah](../../strongs/h/h8435.md) of [Yiṣḥāq](../../strongs/h/h3327.md), ['Abraham](../../strongs/h/h85.md) [ben](../../strongs/h/h1121.md): ['Abraham](../../strongs/h/h85.md) [yalad](../../strongs/h/h3205.md) [Yiṣḥāq](../../strongs/h/h3327.md):

<a name="genesis_25_20"></a>Genesis 25:20

And [Yiṣḥāq](../../strongs/h/h3327.md) was forty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he [laqach](../../strongs/h/h3947.md) [riḇqâ](../../strongs/h/h7259.md) to ['ishshah](../../strongs/h/h802.md), the [bath](../../strongs/h/h1323.md) of [bᵊṯû'ēl](../../strongs/h/h1328.md) the ['Ărammy](../../strongs/h/h761.md) of [padān](../../strongs/h/h6307.md), the ['āḥôṯ](../../strongs/h/h269.md) to [Lāḇān](../../strongs/h/h3837.md) the ['Ărammy](../../strongs/h/h761.md).

<a name="genesis_25_21"></a>Genesis 25:21

And [Yiṣḥāq](../../strongs/h/h3327.md) [ʿāṯar](../../strongs/h/h6279.md) [Yĕhovah](../../strongs/h/h3068.md) for his ['ishshah](../../strongs/h/h802.md), because she was [ʿāqār](../../strongs/h/h6135.md): and [Yĕhovah](../../strongs/h/h3068.md) was [ʿāṯar](../../strongs/h/h6279.md) of him, and [riḇqâ](../../strongs/h/h7259.md) his ['ishshah](../../strongs/h/h802.md) [harah](../../strongs/h/h2029.md).

<a name="genesis_25_22"></a>Genesis 25:22

And the [ben](../../strongs/h/h1121.md) [rāṣaṣ](../../strongs/h/h7533.md) [qereḇ](../../strongs/h/h7130.md) her; and she ['āmar](../../strongs/h/h559.md), If it be so, why am I thus? And she [yālaḵ](../../strongs/h/h3212.md) to [darash](../../strongs/h/h1875.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="genesis_25_23"></a>Genesis 25:23

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto her, Two [gowy](../../strongs/h/h1471.md) are in thy [beten](../../strongs/h/h990.md), and two manner of [lĕom](../../strongs/h/h3816.md) shall be [pāraḏ](../../strongs/h/h6504.md) from thy [me'ah](../../strongs/h/h4578.md); and the one [lĕom](../../strongs/h/h3816.md) shall be ['amats](../../strongs/h/h553.md) than the other [lĕom](../../strongs/h/h3816.md); and the [rab](../../strongs/h/h7227.md) shall ['abad](../../strongs/h/h5647.md) the [ṣāʿîr](../../strongs/h/h6810.md).

<a name="genesis_25_24"></a>Genesis 25:24

And when her [yowm](../../strongs/h/h3117.md) to be [yalad](../../strongs/h/h3205.md) were [mālā'](../../strongs/h/h4390.md), behold, there were [tᵊ'ômîm](../../strongs/h/h8380.md) in her [beten](../../strongs/h/h990.md).

<a name="genesis_25_25"></a>Genesis 25:25

And the [ri'šôn](../../strongs/h/h7223.md) [yāṣā'](../../strongs/h/h3318.md) ['aḏmōnî](../../strongs/h/h132.md), all over like a [śēʿār](../../strongs/h/h8181.md) ['adereṯ](../../strongs/h/h155.md); and they [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [ʿĒśāv](../../strongs/h/h6215.md).

<a name="genesis_25_26"></a>Genesis 25:26

And ['aḥar](../../strongs/h/h310.md) that [yāṣā'](../../strongs/h/h3318.md) his ['ach](../../strongs/h/h251.md), and his [yad](../../strongs/h/h3027.md) ['āḥaz](../../strongs/h/h270.md) on [ʿĒśāv](../../strongs/h/h6215.md) ['aqeb](../../strongs/h/h6119.md); and his [shem](../../strongs/h/h8034.md) was [qara'](../../strongs/h/h7121.md) [Ya'aqob](../../strongs/h/h3290.md): and [Yiṣḥāq](../../strongs/h/h3327.md) was threescore [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when she [yalad](../../strongs/h/h3205.md) them.

<a name="genesis_25_27"></a>Genesis 25:27

And the [naʿar](../../strongs/h/h5288.md) [gāḏal](../../strongs/h/h1431.md): and [ʿĒśāv](../../strongs/h/h6215.md) was a [yada'](../../strongs/h/h3045.md) [ṣayiḏ](../../strongs/h/h6718.md), an ['iysh](../../strongs/h/h376.md) of the [sadeh](../../strongs/h/h7704.md); and [Ya'aqob](../../strongs/h/h3290.md) was a [tām](../../strongs/h/h8535.md) ['iysh](../../strongs/h/h376.md), [yashab](../../strongs/h/h3427.md) in ['ohel](../../strongs/h/h168.md).

<a name="genesis_25_28"></a>Genesis 25:28

And [Yiṣḥāq](../../strongs/h/h3327.md) ['ahab](../../strongs/h/h157.md) [ʿĒśāv](../../strongs/h/h6215.md), because he did [peh](../../strongs/h/h6310.md) of his [ṣayiḏ](../../strongs/h/h6718.md): but [riḇqâ](../../strongs/h/h7259.md) ['ahab](../../strongs/h/h157.md) [Ya'aqob](../../strongs/h/h3290.md).

<a name="genesis_25_29"></a>Genesis 25:29

And [Ya'aqob](../../strongs/h/h3290.md) [zûḏ](../../strongs/h/h2102.md) [nāzîḏ](../../strongs/h/h5138.md): and [ʿĒśāv](../../strongs/h/h6215.md) [bow'](../../strongs/h/h935.md) from the [sadeh](../../strongs/h/h7704.md), and he was [ʿāyēp̄](../../strongs/h/h5889.md):

<a name="genesis_25_30"></a>Genesis 25:30

And [ʿĒśāv](../../strongs/h/h6215.md) ['āmar](../../strongs/h/h559.md) to [Ya'aqob](../../strongs/h/h3290.md), [lāʿaṭ](../../strongs/h/h3938.md) me, I pray thee, with that ['āḏōm](../../strongs/h/h122.md) pottage; for I am [ʿāyēp̄](../../strongs/h/h5889.md): therefore was his [shem](../../strongs/h/h8034.md) [qara'](../../strongs/h/h7121.md) ['Ĕḏōm](../../strongs/h/h123.md).

<a name="genesis_25_31"></a>Genesis 25:31

And [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md), [māḵar](../../strongs/h/h4376.md) me this [yowm](../../strongs/h/h3117.md) thy [bᵊḵôrâ](../../strongs/h/h1062.md).

<a name="genesis_25_32"></a>Genesis 25:32

And [ʿĒśāv](../../strongs/h/h6215.md) ['āmar](../../strongs/h/h559.md), Behold, I [halak](../../strongs/h/h1980.md) to [muwth](../../strongs/h/h4191.md): and what profit shall this [bᵊḵôrâ](../../strongs/h/h1062.md) do to me?

<a name="genesis_25_33"></a>Genesis 25:33

And [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md), [shaba'](../../strongs/h/h7650.md) to me this [yowm](../../strongs/h/h3117.md); and he [shaba'](../../strongs/h/h7650.md) unto him: and he [māḵar](../../strongs/h/h4376.md) his [bᵊḵôrâ](../../strongs/h/h1062.md) unto [Ya'aqob](../../strongs/h/h3290.md).

<a name="genesis_25_34"></a>Genesis 25:34

Then [Ya'aqob](../../strongs/h/h3290.md) [nathan](../../strongs/h/h5414.md) [ʿĒśāv](../../strongs/h/h6215.md) [lechem](../../strongs/h/h3899.md) and [nāzîḏ](../../strongs/h/h5138.md) of [ʿāḏāš](../../strongs/h/h5742.md); and he did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md): thus [ʿĒśāv](../../strongs/h/h6215.md) [bazah](../../strongs/h/h959.md) his [bᵊḵôrâ](../../strongs/h/h1062.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 24](genesis_24.md) - [Genesis 26](genesis_26.md)