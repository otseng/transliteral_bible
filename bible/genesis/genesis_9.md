# [Genesis 9](https://www.blueletterbible.org/kjv/gen/9/1/s_9001)

<a name="genesis_9_1"></a>Genesis 9:1

And ['Elohiym](../../strongs/h/h430.md) [barak](../../strongs/h/h1288.md) [Nōaḥ](../../strongs/h/h5146.md) and his [ben](../../strongs/h/h1121.md), and ['āmar](../../strongs/h/h559.md) unto them, Be [parah](../../strongs/h/h6509.md), and [rabah](../../strongs/h/h7235.md), and [mālā'](../../strongs/h/h4390.md) the ['erets](../../strongs/h/h776.md).

<a name="genesis_9_2"></a>Genesis 9:2

And the [mowra'](../../strongs/h/h4172.md) of you and the [ḥaṯ](../../strongs/h/h2844.md) of you shall be upon every [chay](../../strongs/h/h2416.md) of the ['erets](../../strongs/h/h776.md), and upon every [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), upon all that [rāmaś](../../strongs/h/h7430.md) upon the ['ăḏāmâ](../../strongs/h/h127.md), and upon all the [dag](../../strongs/h/h1709.md) of the [yam](../../strongs/h/h3220.md); into your [yad](../../strongs/h/h3027.md) are they [nathan](../../strongs/h/h5414.md).

<a name="genesis_9_3"></a>Genesis 9:3

Every [remeś](../../strongs/h/h7431.md) that [chay](../../strongs/h/h2416.md) shall be ['oklah](../../strongs/h/h402.md) for you; even as the [yereq](../../strongs/h/h3418.md) ['eseb](../../strongs/h/h6212.md) have I [nathan](../../strongs/h/h5414.md) you all things.

<a name="genesis_9_4"></a>Genesis 9:4

But [basar](../../strongs/h/h1320.md) with the [nephesh](../../strongs/h/h5315.md) thereof, which is the [dam](../../strongs/h/h1818.md) thereof, shall ye not ['akal](../../strongs/h/h398.md).

<a name="genesis_9_5"></a>Genesis 9:5

And ['aḵ](../../strongs/h/h389.md) your [dam](../../strongs/h/h1818.md) of your [nephesh](../../strongs/h/h5315.md) will I [darash](../../strongs/h/h1875.md); at the [yad](../../strongs/h/h3027.md) of every [chay](../../strongs/h/h2416.md) will I [darash](../../strongs/h/h1875.md) it, and at the [yad](../../strongs/h/h3027.md) of ['adam](../../strongs/h/h120.md); at the [yad](../../strongs/h/h3027.md) of every ['iysh](../../strongs/h/h376.md) ['ach](../../strongs/h/h251.md) will I [darash](../../strongs/h/h1875.md) the [nephesh](../../strongs/h/h5315.md) of ['adam](../../strongs/h/h120.md).

<a name="genesis_9_6"></a>Genesis 9:6

Whoso [šāp̄aḵ](../../strongs/h/h8210.md) ['adam](../../strongs/h/h120.md) [dam](../../strongs/h/h1818.md), by ['adam](../../strongs/h/h120.md) shall his [dam](../../strongs/h/h1818.md) be [šāp̄aḵ](../../strongs/h/h8210.md): for in the [tselem](../../strongs/h/h6754.md) of ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) he ['adam](../../strongs/h/h120.md).

<a name="genesis_9_7"></a>Genesis 9:7

And you, be ye [parah](../../strongs/h/h6509.md), and [rabah](../../strongs/h/h7235.md); [šāraṣ](../../strongs/h/h8317.md) in the ['erets](../../strongs/h/h776.md), and [rabah](../../strongs/h/h7235.md) therein.

<a name="genesis_9_8"></a>Genesis 9:8

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto [Nōaḥ](../../strongs/h/h5146.md), and to his [ben](../../strongs/h/h1121.md) with him, ['āmar](../../strongs/h/h559.md),

<a name="genesis_9_9"></a>Genesis 9:9

And I, behold, I [quwm](../../strongs/h/h6965.md) my [bĕriyth](../../strongs/h/h1285.md) with you, and with your [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) you;

<a name="genesis_9_10"></a>Genesis 9:10

And with every [chay](../../strongs/h/h2416.md) [nephesh](../../strongs/h/h5315.md) that is with you, of the [ʿôp̄](../../strongs/h/h5775.md), of the [bĕhemah](../../strongs/h/h929.md), and of every [chay](../../strongs/h/h2416.md) of the ['erets](../../strongs/h/h776.md) with you; from all that [yāṣā'](../../strongs/h/h3318.md) of the [tēḇâ](../../strongs/h/h8392.md), to every [chay](../../strongs/h/h2416.md) of the ['erets](../../strongs/h/h776.md).

<a name="genesis_9_11"></a>Genesis 9:11

And I will [quwm](../../strongs/h/h6965.md) my [bĕriyth](../../strongs/h/h1285.md) with you, neither shall all [basar](../../strongs/h/h1320.md) be [karath](../../strongs/h/h3772.md) any more by the [mayim](../../strongs/h/h4325.md) of a [mabûl](../../strongs/h/h3999.md); neither shall there any more be a [mabûl](../../strongs/h/h3999.md) to [shachath](../../strongs/h/h7843.md) the ['erets](../../strongs/h/h776.md).

<a name="genesis_9_12"></a>Genesis 9:12

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), This is the ['ôṯ](../../strongs/h/h226.md) of the [bĕriyth](../../strongs/h/h1285.md) which I [nathan](../../strongs/h/h5414.md) between me and you and every [chay](../../strongs/h/h2416.md) [nephesh](../../strongs/h/h5315.md) that is with you, for ['owlam](../../strongs/h/h5769.md) [dôr](../../strongs/h/h1755.md):

<a name="genesis_9_13"></a>Genesis 9:13

I do [nathan](../../strongs/h/h5414.md) my [qesheth](../../strongs/h/h7198.md) in the [ʿānān](../../strongs/h/h6051.md), and it shall be for an ['ôṯ](../../strongs/h/h226.md) of a [bĕriyth](../../strongs/h/h1285.md) between me and the ['erets](../../strongs/h/h776.md).

<a name="genesis_9_14"></a>Genesis 9:14

And it shall come to pass, when I [ʿānan](../../strongs/h/h6049.md) a [ʿānān](../../strongs/h/h6051.md) over the ['erets](../../strongs/h/h776.md), that the [qesheth](../../strongs/h/h7198.md) shall be [ra'ah](../../strongs/h/h7200.md) in the [ʿānān](../../strongs/h/h6051.md):

<a name="genesis_9_15"></a>Genesis 9:15

And I will [zakar](../../strongs/h/h2142.md) my [bĕriyth](../../strongs/h/h1285.md), which is between me and you and every [chay](../../strongs/h/h2416.md) [nephesh](../../strongs/h/h5315.md) of all [basar](../../strongs/h/h1320.md); and the [mayim](../../strongs/h/h4325.md) shall no more become a [mabûl](../../strongs/h/h3999.md) to [shachath](../../strongs/h/h7843.md) all [basar](../../strongs/h/h1320.md).

<a name="genesis_9_16"></a>Genesis 9:16

And the [qesheth](../../strongs/h/h7198.md) shall be in the [ʿānān](../../strongs/h/h6051.md); and I will [ra'ah](../../strongs/h/h7200.md) upon it, that I may [zakar](../../strongs/h/h2142.md) the ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md) between ['Elohiym](../../strongs/h/h430.md) and every [chay](../../strongs/h/h2416.md) [nephesh](../../strongs/h/h5315.md) of all [basar](../../strongs/h/h1320.md) that is upon the ['erets](../../strongs/h/h776.md).

<a name="genesis_9_17"></a>Genesis 9:17

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto [Nōaḥ](../../strongs/h/h5146.md), This is the ['ôṯ](../../strongs/h/h226.md) of the [bĕriyth](../../strongs/h/h1285.md), which I have [quwm](../../strongs/h/h6965.md) between me and all [basar](../../strongs/h/h1320.md) that is upon the ['erets](../../strongs/h/h776.md).

<a name="genesis_9_18"></a>Genesis 9:18

And the [ben](../../strongs/h/h1121.md) of [Nōaḥ](../../strongs/h/h5146.md), that [yāṣā'](../../strongs/h/h3318.md) of the [tēḇâ](../../strongs/h/h8392.md), were [Šēm](../../strongs/h/h8035.md), and [Ḥām](../../strongs/h/h2526.md), and [Yep̄eṯ](../../strongs/h/h3315.md): and [Ḥām](../../strongs/h/h2526.md) is the ['ab](../../strongs/h/h1.md) of [Kĕna'an](../../strongs/h/h3667.md).

<a name="genesis_9_19"></a>Genesis 9:19

These are the three [ben](../../strongs/h/h1121.md) of [Nōaḥ](../../strongs/h/h5146.md): and of them was the ['erets](../../strongs/h/h776.md) [naphats](../../strongs/h/h5310.md).

<a name="genesis_9_20"></a>Genesis 9:20

And [Nōaḥ](../../strongs/h/h5146.md) [ḥālal](../../strongs/h/h2490.md) to be an ['iysh](../../strongs/h/h376.md) ['ăḏāmâ](../../strongs/h/h127.md), and he [nāṭaʿ](../../strongs/h/h5193.md) a [kerem](../../strongs/h/h3754.md):

<a name="genesis_9_21"></a>Genesis 9:21

And he [šāṯâ](../../strongs/h/h8354.md) of the [yayin](../../strongs/h/h3196.md), and was [šāḵar](../../strongs/h/h7937.md); and he was [gālâ](../../strongs/h/h1540.md) [tavek](../../strongs/h/h8432.md) his ['ohel](../../strongs/h/h168.md).

<a name="genesis_9_22"></a>Genesis 9:22

And [Ḥām](../../strongs/h/h2526.md), the ['ab](../../strongs/h/h1.md) of [Kĕna'an](../../strongs/h/h3667.md), [ra'ah](../../strongs/h/h7200.md) the [ʿervâ](../../strongs/h/h6172.md) of his ['ab](../../strongs/h/h1.md), and [nāḡaḏ](../../strongs/h/h5046.md) his two ['ach](../../strongs/h/h251.md) [ḥûṣ](../../strongs/h/h2351.md).

<a name="genesis_9_23"></a>Genesis 9:23

And [Šēm](../../strongs/h/h8035.md) and [Yep̄eṯ](../../strongs/h/h3315.md) [laqach](../../strongs/h/h3947.md) a [śimlâ](../../strongs/h/h8071.md), and [śûm](../../strongs/h/h7760.md) it upon both their [šᵊḵem](../../strongs/h/h7926.md), and [yālaḵ](../../strongs/h/h3212.md) ['ăḥōrannîṯ](../../strongs/h/h322.md), and [kāsâ](../../strongs/h/h3680.md) the [ʿervâ](../../strongs/h/h6172.md) of their ['ab](../../strongs/h/h1.md); and their [paniym](../../strongs/h/h6440.md) were ['ăḥōrannîṯ](../../strongs/h/h322.md), and they [ra'ah](../../strongs/h/h7200.md) not their ['ab](../../strongs/h/h1.md) [ʿervâ](../../strongs/h/h6172.md).

<a name="genesis_9_24"></a>Genesis 9:24

And [Nōaḥ](../../strongs/h/h5146.md) [yāqaṣ](../../strongs/h/h3364.md) from his [yayin](../../strongs/h/h3196.md), and [yada'](../../strongs/h/h3045.md) what his [qāṭān](../../strongs/h/h6996.md) [ben](../../strongs/h/h1121.md) had ['asah](../../strongs/h/h6213.md) unto him.

<a name="genesis_9_25"></a>Genesis 9:25

And he ['āmar](../../strongs/h/h559.md), ['arar](../../strongs/h/h779.md) be [Kĕna'an](../../strongs/h/h3667.md); an ['ebed](../../strongs/h/h5650.md) of ['ebed](../../strongs/h/h5650.md) shall he be unto his ['ach](../../strongs/h/h251.md).

<a name="genesis_9_26"></a>Genesis 9:26

And he ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Šēm](../../strongs/h/h8035.md); and [Kĕna'an](../../strongs/h/h3667.md) shall be his ['ebed](../../strongs/h/h5650.md).

<a name="genesis_9_27"></a>Genesis 9:27

['Elohiym](../../strongs/h/h430.md) shall [pāṯâ](../../strongs/h/h6601.md) [Yep̄eṯ](../../strongs/h/h3315.md), and he shall [shakan](../../strongs/h/h7931.md) in the ['ohel](../../strongs/h/h168.md) of [Šēm](../../strongs/h/h8035.md); and [Kĕna'an](../../strongs/h/h3667.md) shall be his ['ebed](../../strongs/h/h5650.md).

<a name="genesis_9_28"></a>Genesis 9:28

And [Nōaḥ](../../strongs/h/h5146.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) the [mabûl](../../strongs/h/h3999.md) three hundred and fifty [šānâ](../../strongs/h/h8141.md).

<a name="genesis_9_29"></a>Genesis 9:29

And all the [yowm](../../strongs/h/h3117.md) of [Nōaḥ](../../strongs/h/h5146.md) were nine hundred and fifty [šānâ](../../strongs/h/h8141.md): and he [muwth](../../strongs/h/h4191.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 8](genesiss_8.md) - [Genesis 10](genesis_10.md)