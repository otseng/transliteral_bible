# [Genesis 19](https://www.blueletterbible.org/lxx/genesis/19)

<a name="genesis_19_1"></a>Genesis 19:1

[erchomai](../../strongs/g/g2064.md) the [dyo](../../strongs/g/g1417.md) [aggelos](../../strongs/g/g32.md) into [Sodoma](../../strongs/g/g4670.md) at [hespera](../../strongs/g/g2073.md). And [Lōt](../../strongs/g/g3091.md) [kathēmai](../../strongs/g/g2521.md) by the [polis](../../strongs/g/g4172.md) of [Sodoma](../../strongs/g/g4670.md). And [eidō](../../strongs/g/g1492.md), [Lōt](../../strongs/g/g3091.md) [exanistēmi](../../strongs/g/g1817.md) to [synantēsis](../../strongs/g/g4877.md) them, and did [proskyneō](../../strongs/g/g4352.md) his [prosōpon](../../strongs/g/g4383.md) upon the [gē](../../strongs/g/g1093.md). 

<a name="genesis_19_2"></a>Genesis 19:2

And he [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md) [kyrios](../../strongs/g/g2962.md), [ekklinō](../../strongs/g/g1578.md) to the [oikos](../../strongs/g/g3624.md) of your [pais](../../strongs/g/g3816.md), and [katalyō](../../strongs/g/g2647.md), and [niptō](../../strongs/g/g3538.md) your [pous](../../strongs/g/g4228.md)! that [orthrizō](../../strongs/g/g3719.md) you may [aperchomai](../../strongs/g/g565.md) into your [hodos](../../strongs/g/g3598.md). And they [eipon](../../strongs/g/g2036.md), No, but in the [plateia](../../strongs/g/g4113.md) we will [katalyō](../../strongs/g/g2647.md). 

<a name="genesis_19_3"></a>Genesis 19:3

And he constrained (katebiazeto) them, and they [ekklinō](../../strongs/g/g1578.md) towards him, and [eiserchomai](../../strongs/g/g1525.md) unto his [oikos](../../strongs/g/g3624.md). And he [poieō](../../strongs/g/g4160.md) for them a [potos](../../strongs/g/g4224.md), and [azymos](../../strongs/g/g106.md) he baked (epepsen) for them, and they [esthiō](../../strongs/g/g2068.md). 

<a name="genesis_19_4"></a>Genesis 19:4

Before [koimaō](../../strongs/g/g2837.md), the [anēr](../../strongs/g/g435.md) of the [polis](../../strongs/g/g4172.md), the Sodomites (Sodomitai), [perikykloō](../../strongs/g/g4033.md) the [oikia](../../strongs/g/g3614.md), from the [neaniskos](../../strongs/g/g3495.md) unto the [presbyteros](../../strongs/g/g4245.md), all the [laos](../../strongs/g/g2992.md) [hama](../../strongs/g/g260.md). 

<a name="genesis_19_5"></a>Genesis 19:5

And they called forth (exekalounto) [Lōt](../../strongs/g/g3091.md). And they [legō](../../strongs/g/g3004.md) to him, Where are they, the [anēr](../../strongs/g/g435.md), the ones [eiserchomai](../../strongs/g/g1525.md) to you this [nyx](../../strongs/g/g3571.md)? [exagō](../../strongs/g/g1806.md) them to us, that we may be intimate (syngenōmetha) with them! 

<a name="genesis_19_6"></a>Genesis 19:6

[exerchomai](../../strongs/g/g1831.md) [Lōt](../../strongs/g/g3091.md) to them, to the threshold (prothyron), the [thyra](../../strongs/g/g2374.md) he shut (proseōxen) behind him. 

<a name="genesis_19_7"></a>Genesis 19:7

And he [eipon](../../strongs/g/g2036.md) to them, By no [mēdamōs](../../strongs/g/g3365.md), [adelphos](../../strongs/g/g80.md), should you do wickedly (ponēreusēsthe).

<a name="genesis_19_8"></a>Genesis 19:8

But there are to me [dyo](../../strongs/g/g1417.md) [thygatēr](../../strongs/g/g2364.md) who [ginōskō](../../strongs/g/g1097.md) not a [anēr](../../strongs/g/g435.md). I will [exagō](../../strongs/g/g1806.md) them to you, and you [chraomai](../../strongs/g/g5530.md) them as [areskō](../../strongs/g/g700.md) you! Only to these [anēr](../../strongs/g/g435.md) you should not [poieō](../../strongs/g/g4160.md) [adikos](../../strongs/g/g94.md), because they [eiserchomai](../../strongs/g/g1525.md) under the protection (skepēn) of my [dokos](../../strongs/g/g1385.md).

<a name="genesis_19_9"></a>Genesis 19:9

And they [eipon](../../strongs/g/g2036.md) to him, You [aphistēmi](../../strongs/g/g868.md) there to [eiserchomai](../../strongs/g/g1525.md) to [paroikeō](../../strongs/g/g3939.md), and not with [krisis](../../strongs/g/g2920.md) to [krinō](../../strongs/g/g2919.md). Now then to you we will [kakoō](../../strongs/g/g2559.md) rather than them. And they were [parabiazomai](../../strongs/g/g3849.md) the [anēr](../../strongs/g/g435.md) [Lōt](../../strongs/g/g3091.md) [sphodra](../../strongs/g/g4970.md), and they [eggizō](../../strongs/g/g1448.md) to [syntribō](../../strongs/g/g4937.md) the [thyra](../../strongs/g/g2374.md). 

<a name="genesis_19_10"></a>Genesis 19:10

[ekteinō](../../strongs/g/g1614.md) the [anēr](../../strongs/g/g435.md) the [cheir](../../strongs/g/g5495.md), drew (eisespasanto) [Lōt](../../strongs/g/g3091.md) towards themselves into the [oikos](../../strongs/g/g3624.md), and the [thyra](../../strongs/g/g2374.md) of the [oikos](../../strongs/g/g3624.md) they [apokleiō](../../strongs/g/g608.md). 

<a name="genesis_19_11"></a>Genesis 19:11

And the [anēr](../../strongs/g/g435.md) being at the [thyra](../../strongs/g/g2374.md) of the [oikos](../../strongs/g/g3624.md) were [patassō](../../strongs/g/g3960.md) with inability to see (aorasia), from [mikron](../../strongs/g/g3397.md) unto [megas](../../strongs/g/g3173.md), and they were [paralyō](../../strongs/g/g3886.md) in [zēteō](../../strongs/g/g2212.md) the [thyra](../../strongs/g/g2374.md). 

<a name="genesis_19_12"></a>Genesis 19:12

[eipon](../../strongs/g/g2036.md) the [anēr](../../strongs/g/g435.md) to [Lōt](../../strongs/g/g3091.md), Are there to you here in-laws (gambroi), or [huios](../../strongs/g/g5207.md) or [thygatēr](../../strongs/g/g2364.md)? Or if any to you other there is in the [polis](../../strongs/g/g4172.md) you [exagō](../../strongs/g/g1806.md) them of this [topos](../../strongs/g/g5117.md)! 

<a name="genesis_19_13"></a>Genesis 19:13

For we [apollymi](../../strongs/g/g622.md) this [topos](../../strongs/g/g5117.md). For was [hypsoō](../../strongs/g/g5312.md) their [kraugē](../../strongs/g/g2906.md) before the [kyrios](../../strongs/g/g2962.md), and [apostellō](../../strongs/g/g649.md) us the [kyrios](../../strongs/g/g2962.md) to obliterate (ektripsai) it. 

<a name="genesis_19_14"></a>Genesis 19:14

[exerchomai](../../strongs/g/g1831.md) [Lōt](../../strongs/g/g3091.md) and [laleō](../../strongs/g/g2980.md) to his sons-in-law (gambrous), the ones [lambanō](../../strongs/g/g2983.md) his [thygatēr](../../strongs/g/g2364.md). And he [eipon](../../strongs/g/g2036.md), [anistēmi](../../strongs/g/g450.md), and [exerchomai](../../strongs/g/g1831.md) from out of this [topos](../../strongs/g/g5117.md), for the [kyrios](../../strongs/g/g2962.md) is obliterating the [polis](../../strongs/g/g4172.md). he [dokeō](../../strongs/g/g1380.md) But to be joking (geloiazein) [enantion](../../strongs/g/g1726.md) his sons-in-law (gambrōn).

<a name="genesis_19_15"></a>Genesis 19:15

And when [orthros](../../strongs/g/g3722.md) came [spoudazō](../../strongs/g/g4704.md) the [aggelos](../../strongs/g/g32.md) Lot, [legō](../../strongs/g/g3004.md), In [anistēmi](../../strongs/g/g450.md), [lambanō](../../strongs/g/g2983.md) your [gynē](../../strongs/g/g1135.md) and [dyo](../../strongs/g/g1417.md) [thygatēr](../../strongs/g/g2364.md) your whom you have, and [exerchomai](../../strongs/g/g1831.md)! that not also you should be [synapollymi](../../strongs/g/g4881.md) in the [anomia](../../strongs/g/g458.md) of the [polis](../../strongs/g/g4172.md). 

<a name="genesis_19_16"></a>Genesis 19:16

And they were [tarassō](../../strongs/g/g5015.md), and [krateō](../../strongs/g/g2902.md) the [aggelos](../../strongs/g/g32.md) his [cheir](../../strongs/g/g5495.md), and the [cheir](../../strongs/g/g5495.md) of his [gynē](../../strongs/g/g1135.md), and the [cheir](../../strongs/g/g5495.md) [dyo](../../strongs/g/g1417.md) [thygatēr](../../strongs/g/g2364.md) of his, in the [kyrios](../../strongs/g/g2962.md) [pheidomai](../../strongs/g/g5339.md) him. 

<a name="genesis_19_17"></a>Genesis 19:17

And [ginomai](../../strongs/g/g1096.md) when they [exagō](../../strongs/g/g1806.md) them [exō](../../strongs/g/g1854.md), and [eipon](../../strongs/g/g2036.md), By [sōzō](../../strongs/g/g4982.md) [sōzō](../../strongs/g/g4982.md) with your own [psychē](../../strongs/g/g5590.md)! You should not [periblepō](../../strongs/g/g4017.md) to the [opisō](../../strongs/g/g3694.md), nor [histēmi](../../strongs/g/g2476.md) in any [perichōros](../../strongs/g/g4066.md). into the [oros](../../strongs/g/g3735.md) [sōzō](../../strongs/g/g4982.md)! lest at any time you may be taken along with them [symparalambanō](../../strongs/g/g4838.md). 

<a name="genesis_19_18"></a>Genesis 19:18

[eipon](../../strongs/g/g2036.md) [Lōt](../../strongs/g/g3091.md) to them, I [deomai](../../strongs/g/g1189.md), O [kyrios](../../strongs/g/g2962.md), 

<a name="genesis_19_19"></a>Genesis 19:19

since [heuriskō](../../strongs/g/g2147.md) your [pais](../../strongs/g/g3816.md) [eleos](../../strongs/g/g1656.md) [enantion](../../strongs/g/g1726.md) you, and you [megalynō](../../strongs/g/g3170.md) your [dikaiosynē](../../strongs/g/g1343.md), which you [poieō](../../strongs/g/g4160.md) unto me, that may [zaō](../../strongs/g/g2198.md) my [psychē](../../strongs/g/g5590.md), but I will not be able to come through [diasōzō](../../strongs/g/g1295.md) into the [oros](../../strongs/g/g3735.md), lest [katalambanō](../../strongs/g/g2638.md) me [kakos](../../strongs/g/g2556.md) and I [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_19_20"></a>Genesis 19:20

[idou](../../strongs/g/g2400.md), this [polis](../../strongs/g/g4172.md) is [eggys](../../strongs/g/g1451.md) for me to [katapheugō](../../strongs/g/g2703.md) there, which is [mikron](../../strongs/g/g3397.md), there I will be [sōzō](../../strongs/g/g4982.md). not a [mikron](../../strongs/g/g3397.md) Is it that will [zaō](../../strongs/g/g2198.md) my [psychē](../../strongs/g/g5590.md) because of you? 

<a name="genesis_19_21"></a>Genesis 19:21

And he [eipon](../../strongs/g/g2036.md) to him, [idou](../../strongs/g/g2400.md), I [thaumazō](../../strongs/g/g2296.md) your [prosōpon](../../strongs/g/g4383.md) over this [rhēma](../../strongs/g/g4487.md), that I should not [katastrephō](../../strongs/g/g2690.md) the [polis](../../strongs/g/g4172.md), concerning of which you [laleō](../../strongs/g/g2980.md). 

<a name="genesis_19_22"></a>Genesis 19:22

You [speudō](../../strongs/g/g4692.md) then to [sōzō](../../strongs/g/g4982.md) there, not for I will be able to [poieō](../../strongs/g/g4160.md) the [pragma](../../strongs/g/g4229.md) until you [erchomai](../../strongs/g/g2064.md) there! On account of this he [kaleō](../../strongs/g/g2564.md) the [onoma](../../strongs/g/g3686.md) of that [polis](../../strongs/g/g4172.md), Zoar.

<a name="genesis_19_23"></a>Genesis 19:23

The [hēlios](../../strongs/g/g2246.md) [exerchomai](../../strongs/g/g1831.md) upon the [gē](../../strongs/g/g1093.md), and [Lōt](../../strongs/g/g3091.md) [eiserchomai](../../strongs/g/g1525.md) into Zoar.

<a name="genesis_19_24"></a>Genesis 19:24

And the [kyrios](../../strongs/g/g2962.md) [brechō](../../strongs/g/g1026.md) upon [Sodoma](../../strongs/g/g4670.md) and [Gomorra](../../strongs/g/g1116.md) [theion](../../strongs/g/g2303.md) and [pyr](../../strongs/g/g4442.md) from the [kyrios](../../strongs/g/g2962.md) from out of [ouranos](../../strongs/g/g3772.md). 

<a name="genesis_19_25"></a>Genesis 19:25

And he [katastrephō](../../strongs/g/g2690.md) these [polis](../../strongs/g/g4172.md), and [pas](../../strongs/g/g3956.md) the [perichōros](../../strongs/g/g4066.md), and all the ones [katoikeō](../../strongs/g/g2730.md) in the [polis](../../strongs/g/g4172.md), and the things [anatellō](../../strongs/g/g393.md) from out of the [gē](../../strongs/g/g1093.md). 

<a name="genesis_19_26"></a>Genesis 19:26

And [epiblepō](../../strongs/g/g1914.md) his [gynē](../../strongs/g/g1135.md) unto the [opisō](../../strongs/g/g3694.md), and she became a monument (stēlē) of [hals](../../strongs/g/g251.md). 

<a name="genesis_19_27"></a>Genesis 19:27

[orthrizō](../../strongs/g/g3719.md) [Abraam](../../strongs/g/g11.md) in the [prōï](../../strongs/g/g4404.md) to the [topos](../../strongs/g/g5117.md) of which he had [histēmi](../../strongs/g/g2476.md) [enantion](../../strongs/g/g1726.md) the [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_19_28"></a>Genesis 19:28

And he [epiblepō](../../strongs/g/g1914.md) upon the [prosōpon](../../strongs/g/g4383.md) of [Sodoma](../../strongs/g/g4670.md) and [Gomorra](../../strongs/g/g1116.md), and upon the [prosōpon](../../strongs/g/g4383.md) of the [perichōros](../../strongs/g/g4066.md). And he [eidō](../../strongs/g/g1492.md). And [idou](../../strongs/g/g2400.md), [anabainō](../../strongs/g/g305.md) a [phlox](../../strongs/g/g5395.md) from out of the [gē](../../strongs/g/g1093.md), as [atmis](../../strongs/g/g822.md) of a [kaminos](../../strongs/g/g2575.md). 

<a name="genesis_19_29"></a>Genesis 19:29

And [ginomai](../../strongs/g/g1096.md) in [kyrios](../../strongs/g/g2962.md) obliterating (ektripsai) all the [polis](../../strongs/g/g4172.md) [perioikos](../../strongs/g/g4040.md), [theos](../../strongs/g/g2316.md) [mnaomai](../../strongs/g/g3415.md) [Abraam](../../strongs/g/g11.md), and he [exapostellō](../../strongs/g/g1821.md) [Lōt](../../strongs/g/g3091.md) from the middle of the [katastrophē](../../strongs/g/g2692.md), in the [katastrephō](../../strongs/g/g2690.md) the [polis](../../strongs/g/g4172.md) in which [katoikeō](../../strongs/g/g2730.md) in them [Lōt](../../strongs/g/g3091.md).

<a name="genesis_19_30"></a>Genesis 19:30

[exerchomai](../../strongs/g/g1831.md) [Lōt](../../strongs/g/g3091.md) from out of Zoar, and [kathēmai](../../strongs/g/g2521.md) in the [oros](../../strongs/g/g3735.md), and the [dyo](../../strongs/g/g1417.md) [thygatēr](../../strongs/g/g2364.md) of his with him; for he [katoikeō](../../strongs/g/g2730.md) in Zoar. And he [katoikeō](../../strongs/g/g2730.md) in the [spēlaion](../../strongs/g/g4693.md), he and the [dyo](../../strongs/g/g1417.md) [thygatēr](../../strongs/g/g2364.md) of his with him. 

<a name="genesis_19_31"></a>Genesis 19:31

[eipon](../../strongs/g/g2036.md) the [presbyteros](../../strongs/g/g4245.md) to the [neos](../../strongs/g/g3501.md), Our [patēr](../../strongs/g/g3962.md) is [presbyteros](../../strongs/g/g4245.md), and there is no one upon the [gē](../../strongs/g/g1093.md) who will [eiserchomai](../../strongs/g/g1525.md) to us, as it is [kathēkō](../../strongs/g/g2520.md) in all the [gē](../../strongs/g/g1093.md). 

<a name="genesis_19_32"></a>Genesis 19:32

[deuro](../../strongs/g/g1204.md) and we should [potizō](../../strongs/g/g4222.md) our [patēr](../../strongs/g/g3962.md) [oinos](../../strongs/g/g3631.md), and we should go to [koimaō](../../strongs/g/g2837.md) with him, that we might [exanistēmi](../../strongs/g/g1817.md) from our [patēr](../../strongs/g/g3962.md) [sperma](../../strongs/g/g4690.md). 

<a name="genesis_19_33"></a>Genesis 19:33

And they [potizō](../../strongs/g/g4222.md) their [patēr](../../strongs/g/g3962.md) [oinos](../../strongs/g/g3631.md) in that [nyx](../../strongs/g/g3571.md). And [eiserchomai](../../strongs/g/g1525.md), the [presbyteros](../../strongs/g/g4245.md) went to [koimaō](../../strongs/g/g2837.md) with her [patēr](../../strongs/g/g3962.md) in that [nyx](../../strongs/g/g3571.md), and he did not [eidō](../../strongs/g/g1492.md) about her [koimaō](../../strongs/g/g2837.md) and [anistēmi](../../strongs/g/g450.md). 

<a name="genesis_19_34"></a>Genesis 19:34

And [ginomai](../../strongs/g/g1096.md) in the [epaurion](../../strongs/g/g1887.md), and [eipon](../../strongs/g/g2036.md) the [presbyteros](../../strongs/g/g4245.md) to the [neos](../../strongs/g/g3501.md), [idou](../../strongs/g/g2400.md), I went to [koimaō](../../strongs/g/g2837.md) [echthes](../../strongs/g/g5504.md) with our [patēr](../../strongs/g/g3962.md). We should [potizō](../../strongs/g/g4222.md) him [oinos](../../strongs/g/g3631.md) also in this [nyx](../../strongs/g/g3571.md), and [eiserchomai](../../strongs/g/g1525.md), you go to [koimaō](../../strongs/g/g2837.md) with him! that we might [exanistēmi](../../strongs/g/g1817.md) from our [patēr](../../strongs/g/g3962.md) [sperma](../../strongs/g/g4690.md). 

<a name="genesis_19_35"></a>Genesis 19:35

And they [potizō](../../strongs/g/g4222.md) also in that [nyx](../../strongs/g/g3571.md) for their [patēr](../../strongs/g/g3962.md) [oinos](../../strongs/g/g3631.md). And [eiserchomai](../../strongs/g/g1525.md), the [neos](../../strongs/g/g3501.md) went to [koimaō](../../strongs/g/g2837.md) with her [patēr](../../strongs/g/g3962.md). And he did not [eidō](../../strongs/g/g1492.md) about her [koimaō](../../strongs/g/g2837.md) and [anistēmi](../../strongs/g/g450.md). 

<a name="genesis_19_36"></a>Genesis 19:36

And [syllambanō](../../strongs/g/g4815.md) the [dyo](../../strongs/g/g1417.md) [thygatēr](../../strongs/g/g2364.md) of [Lōt](../../strongs/g/g3091.md) from their [patēr](../../strongs/g/g3962.md). 

<a name="genesis_19_37"></a>Genesis 19:37

And [tiktō](../../strongs/g/g5088.md) the [presbyteros](../../strongs/g/g4245.md) a [huios](../../strongs/g/g5207.md), and she [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md) Moab, [legō](../../strongs/g/g3004.md), He is from my [patēr](../../strongs/g/g3962.md). This one is [patēr](../../strongs/g/g3962.md) of the Moabites, unto [sēmeron](../../strongs/g/g4594.md) day. 

<a name="genesis_19_38"></a>Genesis 19:38

[tiktō](../../strongs/g/g5088.md) also the [neos](../../strongs/g/g3501.md) a [huios](../../strongs/g/g5207.md), and she [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md) Ammon, he is a [huios](../../strongs/g/g5207.md) of my [genos](../../strongs/g/g1085.md). This one is [patēr](../../strongs/g/g3962.md) of the Ammonites until [sēmeron](../../strongs/g/g4594.md) day. 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 18](genesis_lxx_18.md) - [Genesis 20](genesis_lxx_20.md)