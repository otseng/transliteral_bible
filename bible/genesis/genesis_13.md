# [Genesis 13](https://www.blueletterbible.org/kjv/gen/13/1/s_13001)

<a name="genesis_13_1"></a>Genesis 13:1

And ['Aḇrām](../../strongs/h/h87.md) [ʿālâ](../../strongs/h/h5927.md) out of [Mitsrayim](../../strongs/h/h4714.md), he, and his ['ishshah](../../strongs/h/h802.md), and all that he had, and [Lôṭ](../../strongs/h/h3876.md) with him, into the [neḡeḇ](../../strongs/h/h5045.md).

<a name="genesis_13_2"></a>Genesis 13:2

And ['Aḇrām](../../strongs/h/h87.md) was [me'od](../../strongs/h/h3966.md) [kabad](../../strongs/h/h3513.md) in [miqnê](../../strongs/h/h4735.md), in [keceph](../../strongs/h/h3701.md), and in [zāhāḇ](../../strongs/h/h2091.md).

<a name="genesis_13_3"></a>Genesis 13:3

And he [yālaḵ](../../strongs/h/h3212.md) on his [massāʿ](../../strongs/h/h4550.md) from the [neḡeḇ](../../strongs/h/h5045.md) even to [Bêṯ-'ēl](../../strongs/h/h1008.md), unto the [maqowm](../../strongs/h/h4725.md) where his ['ohel](../../strongs/h/h168.md) had been at the [tᵊḥillâ](../../strongs/h/h8462.md), between [Bêṯ-'ēl](../../strongs/h/h1008.md) and [ʿAy](../../strongs/h/h5857.md);

<a name="genesis_13_4"></a>Genesis 13:4

Unto the [maqowm](../../strongs/h/h4725.md) of the [mizbeach](../../strongs/h/h4196.md), which he had ['asah](../../strongs/h/h6213.md) there at the [ri'šôn](../../strongs/h/h7223.md): and there ['Aḇrām](../../strongs/h/h87.md) [qara'](../../strongs/h/h7121.md) on the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="genesis_13_5"></a>Genesis 13:5

And [Lôṭ](../../strongs/h/h3876.md) also, which [halak](../../strongs/h/h1980.md) with ['Aḇrām](../../strongs/h/h87.md), had [tso'n](../../strongs/h/h6629.md), and [bāqār](../../strongs/h/h1241.md), and ['ohel](../../strongs/h/h168.md).

<a name="genesis_13_6"></a>Genesis 13:6

And the ['erets](../../strongs/h/h776.md) was not able to [nasa'](../../strongs/h/h5375.md) them, that they might [yashab](../../strongs/h/h3427.md) [yaḥaḏ](../../strongs/h/h3162.md): for their [rᵊḵûš](../../strongs/h/h7399.md) was [rab](../../strongs/h/h7227.md), so that they [yakol](../../strongs/h/h3201.md) not [yashab](../../strongs/h/h3427.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="genesis_13_7"></a>Genesis 13:7

And there was a [rîḇ](../../strongs/h/h7379.md) between the [ra'ah](../../strongs/h/h7462.md) of ['Aḇrām](../../strongs/h/h87.md) [miqnê](../../strongs/h/h4735.md) and the [ra'ah](../../strongs/h/h7462.md) of [Lôṭ](../../strongs/h/h3876.md) [miqnê](../../strongs/h/h4735.md): and the [Kᵊnaʿănî](../../strongs/h/h3669.md) and the [Pᵊrizzî](../../strongs/h/h6522.md) [yashab](../../strongs/h/h3427.md) then in the ['erets](../../strongs/h/h776.md).

<a name="genesis_13_8"></a>Genesis 13:8

And ['Aḇrām](../../strongs/h/h87.md) ['āmar](../../strongs/h/h559.md) unto [Lôṭ](../../strongs/h/h3876.md), Let there be ['al](../../strongs/h/h408.md) [Mᵊrîḇâ](../../strongs/h/h4808.md), I pray thee, between me and thee, and between my [ra'ah](../../strongs/h/h7462.md) and thy [ra'ah](../../strongs/h/h7462.md); for ['enowsh](../../strongs/h/h582.md) be ['ach](../../strongs/h/h251.md).

<a name="genesis_13_9"></a>Genesis 13:9

Is not the ['erets](../../strongs/h/h776.md) [paniym](../../strongs/h/h6440.md) thee? [pāraḏ](../../strongs/h/h6504.md) thyself, I pray thee, from me: if thou wilt take the [śᵊmō'l](../../strongs/h/h8040.md), then I will go to the [yāman](../../strongs/h/h3231.md); or if thou depart to the [yamiyn](../../strongs/h/h3225.md), then I will go to the [śam'al](../../strongs/h/h8041.md).

<a name="genesis_13_10"></a>Genesis 13:10

And [Lôṭ](../../strongs/h/h3876.md) [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md) all the [kikār](../../strongs/h/h3603.md) of [Yardēn](../../strongs/h/h3383.md), that it was [mašqê](../../strongs/h/h4945.md) every where, [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) [shachath](../../strongs/h/h7843.md) [Sᵊḏōm](../../strongs/h/h5467.md) and [ʿĂmōrâ](../../strongs/h/h6017.md), even as the [gan](../../strongs/h/h1588.md) of [Yĕhovah](../../strongs/h/h3068.md), like the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), as thou [bow'](../../strongs/h/h935.md) unto [Ṣōʿar](../../strongs/h/h6820.md).

<a name="genesis_13_11"></a>Genesis 13:11

Then [Lôṭ](../../strongs/h/h3876.md) [bāḥar](../../strongs/h/h977.md) him all the [kikār](../../strongs/h/h3603.md) of [Yardēn](../../strongs/h/h3383.md); and [Lôṭ](../../strongs/h/h3876.md) [nāsaʿ](../../strongs/h/h5265.md) [qeḏem](../../strongs/h/h6924.md): and they [pāraḏ](../../strongs/h/h6504.md) themselves the ['iysh](../../strongs/h/h376.md) from the ['ach](../../strongs/h/h251.md).

<a name="genesis_13_12"></a>Genesis 13:12

['Aḇrām](../../strongs/h/h87.md) [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), and [Lôṭ](../../strongs/h/h3876.md) [yashab](../../strongs/h/h3427.md) in the [ʿîr](../../strongs/h/h5892.md) of the [kikār](../../strongs/h/h3603.md), and ['ōhel](../../strongs/h/h167.md) toward [Sᵊḏōm](../../strongs/h/h5467.md).

<a name="genesis_13_13"></a>Genesis 13:13

But the ['enowsh](../../strongs/h/h582.md) of [Sᵊḏōm](../../strongs/h/h5467.md) were [ra'](../../strongs/h/h7451.md) and [chatta'](../../strongs/h/h2400.md) before [Yĕhovah](../../strongs/h/h3068.md) [me'od](../../strongs/h/h3966.md).

<a name="genesis_13_14"></a>Genesis 13:14

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto ['Aḇrām](../../strongs/h/h87.md), ['aḥar](../../strongs/h/h310.md) that [Lôṭ](../../strongs/h/h3876.md) was [pāraḏ](../../strongs/h/h6504.md) from him, [nasa'](../../strongs/h/h5375.md) now thine ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md) from the [maqowm](../../strongs/h/h4725.md) where thou art [ṣāp̄ôn](../../strongs/h/h6828.md), and [neḡeḇ](../../strongs/h/h5045.md), and [qeḏem](../../strongs/h/h6924.md), and [yam](../../strongs/h/h3220.md):

<a name="genesis_13_15"></a>Genesis 13:15

For all the ['erets](../../strongs/h/h776.md) which thou [ra'ah](../../strongs/h/h7200.md), to thee will I [nathan](../../strongs/h/h5414.md) it, and to thy [zera'](../../strongs/h/h2233.md) ['owlam](../../strongs/h/h5769.md).

<a name="genesis_13_16"></a>Genesis 13:16

And I will [śûm](../../strongs/h/h7760.md) thy [zera'](../../strongs/h/h2233.md) as the ['aphar](../../strongs/h/h6083.md) of the ['erets](../../strongs/h/h776.md): so that if an ['iysh](../../strongs/h/h376.md) [yakol](../../strongs/h/h3201.md) [mānâ](../../strongs/h/h4487.md) the ['aphar](../../strongs/h/h6083.md) of the ['erets](../../strongs/h/h776.md), then shall thy [zera'](../../strongs/h/h2233.md) also be [mānâ](../../strongs/h/h4487.md).

<a name="genesis_13_17"></a>Genesis 13:17

[quwm](../../strongs/h/h6965.md), [halak](../../strongs/h/h1980.md) through the ['erets](../../strongs/h/h776.md) in the ['ōreḵ](../../strongs/h/h753.md) of it and in the [rōḥaḇ](../../strongs/h/h7341.md) of it; for I will [nathan](../../strongs/h/h5414.md) it unto thee.

<a name="genesis_13_18"></a>Genesis 13:18

Then ['Aḇrām](../../strongs/h/h87.md) ['ōhel](../../strongs/h/h167.md), and [bow'](../../strongs/h/h935.md) and [yashab](../../strongs/h/h3427.md) in the ['ēlôn](../../strongs/h/h436.md) of [mamrē'](../../strongs/h/h4471.md), which is in [Ḥeḇrôn](../../strongs/h/h2275.md), and [bānâ](../../strongs/h/h1129.md) there a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 12](genesis_12.md) - [Genesis 14](genesis_14.md)