# [Genesis 49](https://www.blueletterbible.org/kjv/gen/49/1/s_49001)

<a name="genesis_49_1"></a>Genesis 49:1

And [Ya'aqob](../../strongs/h/h3290.md) [qara'](../../strongs/h/h7121.md) unto his [ben](../../strongs/h/h1121.md), and ['āmar](../../strongs/h/h559.md), ['āsap̄](../../strongs/h/h622.md) yourselves, that I may [nāḡaḏ](../../strongs/h/h5046.md) you that which shall [qārā'](../../strongs/h/h7122.md) you in the ['aḥărîṯ](../../strongs/h/h319.md) [yowm](../../strongs/h/h3117.md).

<a name="genesis_49_2"></a>Genesis 49:2

[qāḇaṣ](../../strongs/h/h6908.md) yourselves, and [shama'](../../strongs/h/h8085.md), ye [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md); and [shama'](../../strongs/h/h8085.md) unto [Yisra'el](../../strongs/h/h3478.md) your ['ab](../../strongs/h/h1.md).

<a name="genesis_49_3"></a>Genesis 49:3

[Rᵊ'ûḇēn](../../strongs/h/h7205.md), thou art my [bᵊḵôr](../../strongs/h/h1060.md), my [koach](../../strongs/h/h3581.md), and the [re'shiyth](../../strongs/h/h7225.md) of my ['ôn](../../strongs/h/h202.md), the [yeṯer](../../strongs/h/h3499.md) of [śᵊ'ēṯ](../../strongs/h/h7613.md), and the [yeṯer](../../strongs/h/h3499.md) of ['az](../../strongs/h/h5794.md):

<a name="genesis_49_4"></a>Genesis 49:4

[paḥaz](../../strongs/h/h6349.md) as [mayim](../../strongs/h/h4325.md), thou shalt not [yāṯar](../../strongs/h/h3498.md); because thou [ʿālâ](../../strongs/h/h5927.md) to thy ['ab](../../strongs/h/h1.md) [miškāḇ](../../strongs/h/h4904.md); then [ḥālal](../../strongs/h/h2490.md) thou it: he [ʿālâ](../../strongs/h/h5927.md) to my [yāṣûaʿ](../../strongs/h/h3326.md).

<a name="genesis_49_5"></a>Genesis 49:5

[Šimʿôn](../../strongs/h/h8095.md) and [Lēvî](../../strongs/h/h3878.md) are ['ach](../../strongs/h/h251.md); [kĕliy](../../strongs/h/h3627.md) of [chamac](../../strongs/h/h2555.md) are in their [mᵊḵērâ](../../strongs/h/h4380.md).

<a name="genesis_49_6"></a>Genesis 49:6

O my [nephesh](../../strongs/h/h5315.md), [bow'](../../strongs/h/h935.md) not thou into their [sôḏ](../../strongs/h/h5475.md); unto their [qāhēl](../../strongs/h/h6951.md), mine [kabowd](../../strongs/h/h3519.md), be not thou [yāḥaḏ](../../strongs/h/h3161.md): for in their ['aph](../../strongs/h/h639.md) they [harag](../../strongs/h/h2026.md) an ['iysh](../../strongs/h/h376.md), and in their [ratsown](../../strongs/h/h7522.md) they [ʿāqar](../../strongs/h/h6131.md) a [showr](../../strongs/h/h7794.md).

<a name="genesis_49_7"></a>Genesis 49:7

['arar](../../strongs/h/h779.md) be their ['aph](../../strongs/h/h639.md), for it was ['az](../../strongs/h/h5794.md); and their ['ebrah](../../strongs/h/h5678.md), for it was [qāšâ](../../strongs/h/h7185.md): I will [chalaq](../../strongs/h/h2505.md) them in [Ya'aqob](../../strongs/h/h3290.md), and [puwts](../../strongs/h/h6327.md) them in [Yisra'el](../../strongs/h/h3478.md).

<a name="genesis_49_8"></a>Genesis 49:8

[Yehuwdah](../../strongs/h/h3063.md), thou art he whom thy ['ach](../../strongs/h/h251.md) shall [yadah](../../strongs/h/h3034.md): thy [yad](../../strongs/h/h3027.md) shall be in the [ʿōrep̄](../../strongs/h/h6203.md) of thine ['oyeb](../../strongs/h/h341.md); thy ['ab](../../strongs/h/h1.md) [ben](../../strongs/h/h1121.md) shall [shachah](../../strongs/h/h7812.md) before thee.

<a name="genesis_49_9"></a>Genesis 49:9

[Yehuwdah](../../strongs/h/h3063.md) is an ['ariy](../../strongs/h/h738.md) [gûr](../../strongs/h/h1482.md): from the [ṭerep̄](../../strongs/h/h2964.md), my [ben](../../strongs/h/h1121.md), thou art [ʿālâ](../../strongs/h/h5927.md): he [kara'](../../strongs/h/h3766.md), he [rāḇaṣ](../../strongs/h/h7257.md) as an ['ariy](../../strongs/h/h738.md), and as a [lāḇî'](../../strongs/h/h3833.md); who shall [quwm](../../strongs/h/h6965.md) him?

<a name="genesis_49_10"></a>Genesis 49:10

The [shebet](../../strongs/h/h7626.md) shall not [cuwr](../../strongs/h/h5493.md) from [Yehuwdah](../../strongs/h/h3063.md), nor a [ḥāqaq](../../strongs/h/h2710.md) from between his [regel](../../strongs/h/h7272.md), until [šîlô](../../strongs/h/h7886.md) [bow'](../../strongs/h/h935.md); and unto him shall the [yᵊqāhâ](../../strongs/h/h3349.md) of the ['am](../../strongs/h/h5971.md) be.

<a name="genesis_49_11"></a>Genesis 49:11

['āsar](../../strongs/h/h631.md) his [ʿayir](../../strongs/h/h5895.md) unto the [gep̄en](../../strongs/h/h1612.md), and his ['āṯôn](../../strongs/h/h860.md) [ben](../../strongs/h/h1121.md) unto the [śrēq](../../strongs/h/h8321.md); he [kāḇas](../../strongs/h/h3526.md) his [lᵊḇûš](../../strongs/h/h3830.md) in [yayin](../../strongs/h/h3196.md), and his [sûṯ](../../strongs/h/h5497.md) in the [dam](../../strongs/h/h1818.md) of [ʿēnāḇ](../../strongs/h/h6025.md):

<a name="genesis_49_12"></a>Genesis 49:12

His ['ayin](../../strongs/h/h5869.md) shall be [ḥaḵlîlî](../../strongs/h/h2447.md) with [yayin](../../strongs/h/h3196.md), and his [šēn](../../strongs/h/h8127.md) [lāḇān](../../strongs/h/h3836.md) with [chalab](../../strongs/h/h2461.md).

<a name="genesis_49_13"></a>Genesis 49:13

[Zᵊḇûlûn](../../strongs/h/h2074.md) shall [shakan](../../strongs/h/h7931.md) at the [ḥôp̄](../../strongs/h/h2348.md) of the [yam](../../strongs/h/h3220.md); and he shall be for a [ḥôp̄](../../strongs/h/h2348.md) of ['ŏnîyâ](../../strongs/h/h591.md); and his [yᵊrēḵâ](../../strongs/h/h3411.md) shall be unto [Ṣîḏôn](../../strongs/h/h6721.md).

<a name="genesis_49_14"></a>Genesis 49:14

[Yiśśāśḵār](../../strongs/h/h3485.md) is a [gerem](../../strongs/h/h1634.md) [chamowr](../../strongs/h/h2543.md) [rāḇaṣ](../../strongs/h/h7257.md) between [mišpᵊṯayim](../../strongs/h/h4942.md):

<a name="genesis_49_15"></a>Genesis 49:15

And he [ra'ah](../../strongs/h/h7200.md) that [mᵊnûḥâ](../../strongs/h/h4496.md) was [towb](../../strongs/h/h2896.md), and the ['erets](../../strongs/h/h776.md) that it was [nāʿēm](../../strongs/h/h5276.md); and [natah](../../strongs/h/h5186.md) his [šᵊḵem](../../strongs/h/h7926.md) to [sāḇal](../../strongs/h/h5445.md), and became an ['abad](../../strongs/h/h5647.md) unto [mas](../../strongs/h/h4522.md).

<a name="genesis_49_16"></a>Genesis 49:16

[Dān](../../strongs/h/h1835.md) shall [diyn](../../strongs/h/h1777.md) his ['am](../../strongs/h/h5971.md), as one of the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="genesis_49_17"></a>Genesis 49:17

[Dān](../../strongs/h/h1835.md) shall be a [nachash](../../strongs/h/h5175.md) by the [derek](../../strongs/h/h1870.md), a [šᵊp̄îp̄ōn](../../strongs/h/h8207.md) in the ['orach](../../strongs/h/h734.md), that [nāšaḵ](../../strongs/h/h5391.md) the [sûs](../../strongs/h/h5483.md) ['aqeb](../../strongs/h/h6119.md), so that his [rāḵaḇ](../../strongs/h/h7392.md) shall [naphal](../../strongs/h/h5307.md) ['āḥôr](../../strongs/h/h268.md).

<a name="genesis_49_18"></a>Genesis 49:18

I have [qāvâ](../../strongs/h/h6960.md) for thy [yĕshuw'ah](../../strongs/h/h3444.md), [Yĕhovah](../../strongs/h/h3068.md).

<a name="genesis_49_19"></a>Genesis 49:19

[Gāḏ](../../strongs/h/h1410.md), a [gᵊḏûḏ](../../strongs/h/h1416.md) shall [gûḏ](../../strongs/h/h1464.md) him: but he shall [gûḏ](../../strongs/h/h1464.md) at the ['aqeb](../../strongs/h/h6119.md).

<a name="genesis_49_20"></a>Genesis 49:20

Out of ['Āšēr](../../strongs/h/h836.md) his [lechem](../../strongs/h/h3899.md) shall be [šāmēn](../../strongs/h/h8082.md), and he shall [nathan](../../strongs/h/h5414.md) [melek](../../strongs/h/h4428.md) [maʿăḏān](../../strongs/h/h4574.md).

<a name="genesis_49_21"></a>Genesis 49:21

[Nap̄tālî](../../strongs/h/h5321.md) is an ['ayyālâ](../../strongs/h/h355.md) [shalach](../../strongs/h/h7971.md): he [nathan](../../strongs/h/h5414.md) [Šep̄er](../../strongs/h/h8233.md) ['emer](../../strongs/h/h561.md).

<a name="genesis_49_22"></a>Genesis 49:22

[Yôsēp̄](../../strongs/h/h3130.md) is a [parah](../../strongs/h/h6509.md) [ben](../../strongs/h/h1121.md), even a [parah](../../strongs/h/h6509.md) [ben](../../strongs/h/h1121.md) by an ['ayin](../../strongs/h/h5869.md); whose [bath](../../strongs/h/h1323.md) [ṣāʿaḏ](../../strongs/h/h6805.md) over the [šûrâ](../../strongs/h/h7791.md):

<a name="genesis_49_23"></a>Genesis 49:23

The [baʿal](../../strongs/h/h1167.md) [chets](../../strongs/h/h2671.md) have [mārar](../../strongs/h/h4843.md) him, and [rabab](../../strongs/h/h7232.md) at him, and [śāṭam](../../strongs/h/h7852.md) him:

<a name="genesis_49_24"></a>Genesis 49:24

But his [qesheth](../../strongs/h/h7198.md) [yashab](../../strongs/h/h3427.md) in ['êṯān](../../strongs/h/h386.md), and the [zerowa'](../../strongs/h/h2220.md) of his [yad](../../strongs/h/h3027.md) were made [pāzaz](../../strongs/h/h6339.md) by the [yad](../../strongs/h/h3027.md) of the ['abiyr](../../strongs/h/h46.md) of [Ya'aqob](../../strongs/h/h3290.md); (from thence is the [ra'ah](../../strongs/h/h7462.md), the ['eben](../../strongs/h/h68.md) of [Yisra'el](../../strongs/h/h3478.md):)

<a name="genesis_49_25"></a>Genesis 49:25

Even by the ['el](../../strongs/h/h410.md) of thy ['ab](../../strongs/h/h1.md), who shall [ʿāzar](../../strongs/h/h5826.md) thee; and by [Šaday](../../strongs/h/h7706.md), who shall [barak](../../strongs/h/h1288.md) thee with [bĕrakah](../../strongs/h/h1293.md) of [shamayim](../../strongs/h/h8064.md) [ʿal](../../strongs/h/h5920.md), [bĕrakah](../../strongs/h/h1293.md) of the [tĕhowm](../../strongs/h/h8415.md) that [rāḇaṣ](../../strongs/h/h7257.md) under, [bĕrakah](../../strongs/h/h1293.md) of the [šaḏ](../../strongs/h/h7699.md), and of the [raḥam](../../strongs/h/h7356.md):

<a name="genesis_49_26"></a>Genesis 49:26

The [bĕrakah](../../strongs/h/h1293.md) of thy ['ab](../../strongs/h/h1.md) have [gabar](../../strongs/h/h1396.md) above the [bĕrakah](../../strongs/h/h1293.md) of my [harah](../../strongs/h/h2029.md) unto the [ta'ăvâ](../../strongs/h/h8379.md) of the ['owlam](../../strongs/h/h5769.md) [giḇʿâ](../../strongs/h/h1389.md): they shall be on the [ro'sh](../../strongs/h/h7218.md) of [Yôsēp̄](../../strongs/h/h3130.md), and on the [qodqod](../../strongs/h/h6936.md) of him that was [nāzîr](../../strongs/h/h5139.md) from his ['ach](../../strongs/h/h251.md).

<a name="genesis_49_27"></a>Genesis 49:27

[Binyāmîn](../../strongs/h/h1144.md) shall [taraph](../../strongs/h/h2963.md) as a [zᵊ'ēḇ](../../strongs/h/h2061.md): in the [boqer](../../strongs/h/h1242.md) he shall ['akal](../../strongs/h/h398.md) the [ʿaḏ](../../strongs/h/h5706.md), and at ['ereb](../../strongs/h/h6153.md) he shall [chalaq](../../strongs/h/h2505.md) the [šālāl](../../strongs/h/h7998.md).

<a name="genesis_49_28"></a>Genesis 49:28

All these are the twelve [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md): and this is it that their ['ab](../../strongs/h/h1.md) [dabar](../../strongs/h/h1696.md) unto them, and [barak](../../strongs/h/h1288.md) them; every ['iysh](../../strongs/h/h376.md) according to his [bĕrakah](../../strongs/h/h1293.md) he [barak](../../strongs/h/h1288.md) them.

<a name="genesis_49_29"></a>Genesis 49:29

And he [tsavah](../../strongs/h/h6680.md) them, and ['āmar](../../strongs/h/h559.md) unto them, I am to be ['āsap̄](../../strongs/h/h622.md) unto my ['am](../../strongs/h/h5971.md): [qāḇar](../../strongs/h/h6912.md) me with my ['ab](../../strongs/h/h1.md) in the [mᵊʿārâ](../../strongs/h/h4631.md) that is in the [sadeh](../../strongs/h/h7704.md) of [ʿep̄rôn](../../strongs/h/h6085.md) the [Ḥitî](../../strongs/h/h2850.md),

<a name="genesis_49_30"></a>Genesis 49:30

In the [mᵊʿārâ](../../strongs/h/h4631.md) that is in the [sadeh](../../strongs/h/h7704.md) of [maḵpēlâ](../../strongs/h/h4375.md), which is [paniym](../../strongs/h/h6440.md) [mamrē'](../../strongs/h/h4471.md), in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), which ['Abraham](../../strongs/h/h85.md) [qānâ](../../strongs/h/h7069.md) with the [sadeh](../../strongs/h/h7704.md) of [ʿep̄rôn](../../strongs/h/h6085.md) the [Ḥitî](../../strongs/h/h2850.md) for an ['achuzzah](../../strongs/h/h272.md) of a [qeber](../../strongs/h/h6913.md).

<a name="genesis_49_31"></a>Genesis 49:31

There they [qāḇar](../../strongs/h/h6912.md) ['Abraham](../../strongs/h/h85.md) and [Śārâ](../../strongs/h/h8283.md) his ['ishshah](../../strongs/h/h802.md); there they [qāḇar](../../strongs/h/h6912.md) [Yiṣḥāq](../../strongs/h/h3327.md) and [riḇqâ](../../strongs/h/h7259.md) his ['ishshah](../../strongs/h/h802.md); and there I [qāḇar](../../strongs/h/h6912.md) [Lē'â](../../strongs/h/h3812.md).

<a name="genesis_49_32"></a>Genesis 49:32

The [miqnê](../../strongs/h/h4735.md) of the [sadeh](../../strongs/h/h7704.md) and of the [mᵊʿārâ](../../strongs/h/h4631.md) that is therein was from the [ben](../../strongs/h/h1121.md) of [Ḥēṯ](../../strongs/h/h2845.md).

<a name="genesis_49_33"></a>Genesis 49:33

And when [Ya'aqob](../../strongs/h/h3290.md) had made a [kalah](../../strongs/h/h3615.md) of [tsavah](../../strongs/h/h6680.md) his [ben](../../strongs/h/h1121.md), he ['āsap̄](../../strongs/h/h622.md) his [regel](../../strongs/h/h7272.md) into the [mittah](../../strongs/h/h4296.md), and [gāvaʿ](../../strongs/h/h1478.md), and was ['āsap̄](../../strongs/h/h622.md) unto his ['am](../../strongs/h/h5971.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 48](genesis_48.md) - [Genesis 50](genesis_50.md)