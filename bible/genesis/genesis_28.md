# [Genesis 28](https://www.blueletterbible.org/kjv/gen/28/1/s_28001)

<a name="genesis_28_1"></a>Genesis 28:1

And [Yiṣḥāq](../../strongs/h/h3327.md) [qara'](../../strongs/h/h7121.md) [Ya'aqob](../../strongs/h/h3290.md), and [barak](../../strongs/h/h1288.md) him, and [tsavah](../../strongs/h/h6680.md) him, and ['āmar](../../strongs/h/h559.md) unto him, Thou shalt not [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) of the [bath](../../strongs/h/h1323.md) of [Kĕna'an](../../strongs/h/h3667.md).

<a name="genesis_28_2"></a>Genesis 28:2

[quwm](../../strongs/h/h6965.md), [yālaḵ](../../strongs/h/h3212.md) to [padān](../../strongs/h/h6307.md), to the [bayith](../../strongs/h/h1004.md) of [bᵊṯû'ēl](../../strongs/h/h1328.md) thy ['em](../../strongs/h/h517.md) ['ab](../../strongs/h/h1.md); and [laqach](../../strongs/h/h3947.md) thee an ['ishshah](../../strongs/h/h802.md) from thence of the [bath](../../strongs/h/h1323.md) of [Lāḇān](../../strongs/h/h3837.md) thy ['em](../../strongs/h/h517.md) ['ach](../../strongs/h/h251.md).

<a name="genesis_28_3"></a>Genesis 28:3

And ['el](../../strongs/h/h410.md)[Šaday](../../strongs/h/h7706.md) [barak](../../strongs/h/h1288.md) thee, and make thee [parah](../../strongs/h/h6509.md), and [rabah](../../strongs/h/h7235.md) thee, that thou mayest be a [qāhēl](../../strongs/h/h6951.md) of ['am](../../strongs/h/h5971.md);

<a name="genesis_28_4"></a>Genesis 28:4

And [nathan](../../strongs/h/h5414.md) thee the [bĕrakah](../../strongs/h/h1293.md) of ['Abraham](../../strongs/h/h85.md), to thee, and to thy [zera'](../../strongs/h/h2233.md) with thee; that thou mayest [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md) wherein thou art a [māḡûr](../../strongs/h/h4033.md), which ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) unto ['Abraham](../../strongs/h/h85.md).

<a name="genesis_28_5"></a>Genesis 28:5

And [Yiṣḥāq](../../strongs/h/h3327.md) [shalach](../../strongs/h/h7971.md) [Ya'aqob](../../strongs/h/h3290.md): and he [yālaḵ](../../strongs/h/h3212.md) to [padān](../../strongs/h/h6307.md) unto [Lāḇān](../../strongs/h/h3837.md), [ben](../../strongs/h/h1121.md) of [bᵊṯû'ēl](../../strongs/h/h1328.md) the ['Ărammy](../../strongs/h/h761.md), the ['ach](../../strongs/h/h251.md) of [riḇqâ](../../strongs/h/h7259.md), [Ya'aqob](../../strongs/h/h3290.md) and [ʿĒśāv](../../strongs/h/h6215.md) ['em](../../strongs/h/h517.md).

<a name="genesis_28_6"></a>Genesis 28:6

When [ʿĒśāv](../../strongs/h/h6215.md) [ra'ah](../../strongs/h/h7200.md) that [Yiṣḥāq](../../strongs/h/h3327.md) had [barak](../../strongs/h/h1288.md) [Ya'aqob](../../strongs/h/h3290.md), and [shalach](../../strongs/h/h7971.md) him to [padān](../../strongs/h/h6307.md), to [laqach](../../strongs/h/h3947.md) him an ['ishshah](../../strongs/h/h802.md) from thence; and that as he [barak](../../strongs/h/h1288.md) him he gave him a [tsavah](../../strongs/h/h6680.md), ['āmar](../../strongs/h/h559.md), Thou shalt not [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) of the [bath](../../strongs/h/h1323.md) of [Kĕna'an](../../strongs/h/h3667.md);

<a name="genesis_28_7"></a>Genesis 28:7

And that [Ya'aqob](../../strongs/h/h3290.md) [shama'](../../strongs/h/h8085.md) his ['ab](../../strongs/h/h1.md) and his ['em](../../strongs/h/h517.md), and was [yālaḵ](../../strongs/h/h3212.md) to [padān](../../strongs/h/h6307.md);

<a name="genesis_28_8"></a>Genesis 28:8

And [ʿĒśāv](../../strongs/h/h6215.md) [ra'ah](../../strongs/h/h7200.md) that the [bath](../../strongs/h/h1323.md) of [Kĕna'an](../../strongs/h/h3667.md) ['ayin](../../strongs/h/h5869.md) [ra'](../../strongs/h/h7451.md) [Yiṣḥāq](../../strongs/h/h3327.md) his ['ab](../../strongs/h/h1.md);

<a name="genesis_28_9"></a>Genesis 28:9

Then [yālaḵ](../../strongs/h/h3212.md) [ʿĒśāv](../../strongs/h/h6215.md) unto [Yišmāʿē'l](../../strongs/h/h3458.md), and [laqach](../../strongs/h/h3947.md) unto the ['ishshah](../../strongs/h/h802.md) which he had [Maḥălaṯ](../../strongs/h/h4258.md) the [bath](../../strongs/h/h1323.md) of [Yišmāʿē'l](../../strongs/h/h3458.md) ['Abraham](../../strongs/h/h85.md) [ben](../../strongs/h/h1121.md), the ['āḥôṯ](../../strongs/h/h269.md) of [Nᵊḇāyôṯ](../../strongs/h/h5032.md), to be his ['ishshah](../../strongs/h/h802.md).

<a name="genesis_28_10"></a>Genesis 28:10

And [Ya'aqob](../../strongs/h/h3290.md) [yāṣā'](../../strongs/h/h3318.md) from [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md), and [yālaḵ](../../strongs/h/h3212.md) toward [Ḥārān](../../strongs/h/h2771.md).

<a name="genesis_28_11"></a>Genesis 28:11

And he [pāḡaʿ](../../strongs/h/h6293.md) a certain [maqowm](../../strongs/h/h4725.md), and [lûn](../../strongs/h/h3885.md), because the [šemeš](../../strongs/h/h8121.md) was [bow'](../../strongs/h/h935.md); and he [laqach](../../strongs/h/h3947.md) of the ['eben](../../strongs/h/h68.md) of that [maqowm](../../strongs/h/h4725.md), and [śûm](../../strongs/h/h7760.md) them for his [mᵊra'ăšôṯ](../../strongs/h/h4763.md), and [shakab](../../strongs/h/h7901.md) in that [maqowm](../../strongs/h/h4725.md) to [shakab](../../strongs/h/h7901.md).

<a name="genesis_28_12"></a>Genesis 28:12

And he [ḥālam](../../strongs/h/h2492.md), and behold a [sullām](../../strongs/h/h5551.md) [nāṣaḇ](../../strongs/h/h5324.md) on the ['erets](../../strongs/h/h776.md), and the [ro'sh](../../strongs/h/h7218.md) [naga'](../../strongs/h/h5060.md) to [shamayim](../../strongs/h/h8064.md): and behold the [mal'ak](../../strongs/h/h4397.md) of ['Elohiym](../../strongs/h/h430.md) [ʿālâ](../../strongs/h/h5927.md) and [yarad](../../strongs/h/h3381.md) on it.

<a name="genesis_28_13"></a>Genesis 28:13

And, behold, [Yĕhovah](../../strongs/h/h3068.md) [nāṣaḇ](../../strongs/h/h5324.md) above it, and ['āmar](../../strongs/h/h559.md), I am [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of ['Abraham](../../strongs/h/h85.md) thy ['ab](../../strongs/h/h1.md), and the ['Elohiym](../../strongs/h/h430.md) of [Yiṣḥāq](../../strongs/h/h3327.md): the ['erets](../../strongs/h/h776.md) whereon thou [shakab](../../strongs/h/h7901.md), to thee will I [nathan](../../strongs/h/h5414.md) it, and to thy [zera'](../../strongs/h/h2233.md);

<a name="genesis_28_14"></a>Genesis 28:14

And thy [zera'](../../strongs/h/h2233.md) shall be as the ['aphar](../../strongs/h/h6083.md) of the ['erets](../../strongs/h/h776.md), and thou shalt [pāraṣ](../../strongs/h/h6555.md) to the [yam](../../strongs/h/h3220.md), and to the [qeḏem](../../strongs/h/h6924.md), and to the [ṣāp̄ôn](../../strongs/h/h6828.md), and to the [neḡeḇ](../../strongs/h/h5045.md): and in thee and in thy [zera'](../../strongs/h/h2233.md) shall all the [mišpāḥâ](../../strongs/h/h4940.md) of the ['ăḏāmâ](../../strongs/h/h127.md) be [barak](../../strongs/h/h1288.md).

<a name="genesis_28_15"></a>Genesis 28:15

And, behold, I am with thee, and will [shamar](../../strongs/h/h8104.md) thee in all places whither thou [yālaḵ](../../strongs/h/h3212.md), and will [shuwb](../../strongs/h/h7725.md) into this ['ăḏāmâ](../../strongs/h/h127.md); for I will not ['azab](../../strongs/h/h5800.md) thee, until I have ['asah](../../strongs/h/h6213.md) that which I have [dabar](../../strongs/h/h1696.md) to thee of.

<a name="genesis_28_16"></a>Genesis 28:16

And [Ya'aqob](../../strongs/h/h3290.md) [yāqaṣ](../../strongs/h/h3364.md) out of his [šēnā'](../../strongs/h/h8142.md), and he ['āmar](../../strongs/h/h559.md), ['āḵēn](../../strongs/h/h403.md) [Yĕhovah](../../strongs/h/h3068.md) is in this [maqowm](../../strongs/h/h4725.md); and I [yada'](../../strongs/h/h3045.md) it not.

<a name="genesis_28_17"></a>Genesis 28:17

And he was [yare'](../../strongs/h/h3372.md), and ['āmar](../../strongs/h/h559.md), How [yare'](../../strongs/h/h3372.md) is this [maqowm](../../strongs/h/h4725.md)! this is none other but the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), and this is the [sha'ar](../../strongs/h/h8179.md) of [shamayim](../../strongs/h/h8064.md).

<a name="genesis_28_18"></a>Genesis 28:18

And [Ya'aqob](../../strongs/h/h3290.md) [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [laqach](../../strongs/h/h3947.md) the ['eben](../../strongs/h/h68.md) that he had [śûm](../../strongs/h/h7760.md) for his [mᵊra'ăšôṯ](../../strongs/h/h4763.md), and [śûm](../../strongs/h/h7760.md) it up for a [maṣṣēḇâ](../../strongs/h/h4676.md), and [yāṣaq](../../strongs/h/h3332.md) [šemen](../../strongs/h/h8081.md) upon the [ro'sh](../../strongs/h/h7218.md).

<a name="genesis_28_19"></a>Genesis 28:19

And he [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of that [maqowm](../../strongs/h/h4725.md) [Bêṯ-'ēl](../../strongs/h/h1008.md): but the [shem](../../strongs/h/h8034.md) of that [ʿîr](../../strongs/h/h5892.md) was called [Lûz](../../strongs/h/h3870.md) at the [ri'šôn](../../strongs/h/h7223.md).

<a name="genesis_28_20"></a>Genesis 28:20

And [Ya'aqob](../../strongs/h/h3290.md) [nāḏar](../../strongs/h/h5087.md) a [neḏer](../../strongs/h/h5088.md), ['āmar](../../strongs/h/h559.md), If ['Elohiym](../../strongs/h/h430.md) will be with me, and will [shamar](../../strongs/h/h8104.md) me in this [derek](../../strongs/h/h1870.md) that I [halak](../../strongs/h/h1980.md), and will [nathan](../../strongs/h/h5414.md) me [lechem](../../strongs/h/h3899.md) to ['akal](../../strongs/h/h398.md), and [beḡeḏ](../../strongs/h/h899.md) to [labash](../../strongs/h/h3847.md),

<a name="genesis_28_21"></a>Genesis 28:21

So that I [shuwb](../../strongs/h/h7725.md) to my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) in [shalowm](../../strongs/h/h7965.md); then shall [Yĕhovah](../../strongs/h/h3068.md) be my ['Elohiym](../../strongs/h/h430.md):

<a name="genesis_28_22"></a>Genesis 28:22

And this ['eben](../../strongs/h/h68.md), which I have [śûm](../../strongs/h/h7760.md) for a [maṣṣēḇâ](../../strongs/h/h4676.md), shall be ['Elohiym](../../strongs/h/h430.md) [bayith](../../strongs/h/h1004.md): and of all that thou shalt [nathan](../../strongs/h/h5414.md) me I will [ʿāśar](../../strongs/h/h6237.md) unto thee.

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 27](genesis_27.md) - [Genesis 29](genesis_29.md)