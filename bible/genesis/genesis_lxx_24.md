# [Genesis 24](https://www.blueletterbible.org/lxx/genesis/24)

<a name="genesis_24_1"></a>Genesis 24:1

And [Abraam](../../strongs/g/g11.md) was [presbyteros](../../strongs/g/g4245.md), [probainō](../../strongs/g/g4260.md) of [hēmera](../../strongs/g/g2250.md). And the [kyrios](../../strongs/g/g2962.md) [eulogeō](../../strongs/g/g2127.md) [Abraam](../../strongs/g/g11.md) according to all things . 

<a name="genesis_24_2"></a>Genesis 24:2

And [Abraam](../../strongs/g/g11.md) [eipon](../../strongs/g/g2036.md) to his [pais](../../strongs/g/g3816.md), to the [presbyteros](../../strongs/g/g4245.md) of his [oikia](../../strongs/g/g3614.md), to the one in [archōn](../../strongs/g/g758.md) of all his things, [tithēmi](../../strongs/g/g5087.md) your [cheir](../../strongs/g/g5495.md) under my [mēros](../../strongs/g/g3382.md)! 

<a name="genesis_24_3"></a>Genesis 24:3

And I [exorkizō](../../strongs/g/g1844.md) you by the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) of the [ouranos](../../strongs/g/g3772.md), and the [theos](../../strongs/g/g2316.md) of the [gē](../../strongs/g/g1093.md), that you should not [lambanō](../../strongs/g/g2983.md) a [gynē](../../strongs/g/g1135.md) to my [huios](../../strongs/g/g5207.md) [Isaak](../../strongs/g/g2464.md) from the [thygatēr](../../strongs/g/g2364.md) of the [Chananaios](../../strongs/g/g5478.md), with whom I [oikeō](../../strongs/g/g3611.md) among them. 

<a name="genesis_24_4"></a>Genesis 24:4

But only unto my [gē](../../strongs/g/g1093.md) of which I was shall you [poreuō](../../strongs/g/g4198.md), and unto my [phylē](../../strongs/g/g5443.md). And you shall [lambanō](../../strongs/g/g2983.md) a [gynē](../../strongs/g/g1135.md) for my [huios](../../strongs/g/g5207.md) [Isaak](../../strongs/g/g2464.md) from there. 

<a name="genesis_24_5"></a>Genesis 24:5

[eipon](../../strongs/g/g2036.md) to him the [pais](../../strongs/g/g3816.md), If at any time not [boulomai](../../strongs/g/g1014.md) the [gynē](../../strongs/g/g1135.md) to [poreuō](../../strongs/g/g4198.md) with me back into this [gē](../../strongs/g/g1093.md), shall I [apostrephō](../../strongs/g/g654.md) your [huios](../../strongs/g/g5207.md) into the [gē](../../strongs/g/g1093.md) from where you [exerchomai](../../strongs/g/g1831.md) from there? 

<a name="genesis_24_6"></a>Genesis 24:6

[eipon](../../strongs/g/g2036.md) to him [Abraam](../../strongs/g/g11.md), [prosechō](../../strongs/g/g4337.md) to yourself that you should not [apostrephō](../../strongs/g/g654.md) my [huios](../../strongs/g/g5207.md) there. 

<a name="genesis_24_7"></a>Genesis 24:7

The [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) of the [ouranos](../../strongs/g/g3772.md), and the [theos](../../strongs/g/g2316.md) of the [gē](../../strongs/g/g1093.md), who [lambanō](../../strongs/g/g2983.md) me from [oikos](../../strongs/g/g3624.md) my [patēr](../../strongs/g/g3962.md), and from the [gē](../../strongs/g/g1093.md) of which I was [gennaō](../../strongs/g/g1080.md), who [laleō](../../strongs/g/g2980.md) to me, and who [omnyō](../../strongs/g/g3660.md) to me, [legō](../../strongs/g/g3004.md), To you I will [didōmi](../../strongs/g/g1325.md) this [gē](../../strongs/g/g1093.md) and your [sperma](../../strongs/g/g4690.md) -- he will [apostellō](../../strongs/g/g649.md) his [aggelos](../../strongs/g/g32.md) in [emprosthen](../../strongs/g/g1715.md) of you, and you shall [lambanō](../../strongs/g/g2983.md) a [gynē](../../strongs/g/g1135.md), for my [huios](../../strongs/g/g5207.md) from there. 

<a name="genesis_24_8"></a>Genesis 24:8

And if should not want the [gynē](../../strongs/g/g1135.md) to [poreuō](../../strongs/g/g4198.md) with you into this [gē](../../strongs/g/g1093.md), you will be [katharos](../../strongs/g/g2513.md) from this [horkos](../../strongs/g/g3727.md). Only my [huios](../../strongs/g/g5207.md) you should not [apostrephō](../../strongs/g/g654.md) there. 

<a name="genesis_24_9"></a>Genesis 24:9

And [tithēmi](../../strongs/g/g5087.md) the [pais](../../strongs/g/g3816.md) his [cheir](../../strongs/g/g5495.md) under the [mēros](../../strongs/g/g3382.md) of [Abraam](../../strongs/g/g11.md) his [kyrios](../../strongs/g/g2962.md). And he [omnyō](../../strongs/g/g3660.md) to him on account of this [rhēma](../../strongs/g/g4487.md). 

<a name="genesis_24_10"></a>Genesis 24:10

And [lambanō](../../strongs/g/g2983.md) the [pais](../../strongs/g/g3816.md) ten [kamēlos](../../strongs/g/g2574.md) from the [kamēlos](../../strongs/g/g2574.md) of his [kyrios](../../strongs/g/g2962.md), and from all the [agathos](../../strongs/g/g18.md) of his [kyrios](../../strongs/g/g2962.md) with himself. And [anistēmi](../../strongs/g/g450.md) he [poreuō](../../strongs/g/g4198.md) into [Mesopotamia](../../strongs/g/g3318.md), into the [polis](../../strongs/g/g4172.md) of [Nachōr](../../strongs/g/g3493.md).

<a name="genesis_24_11"></a>Genesis 24:11

And he rested (ekoimisen) the [kamēlos](../../strongs/g/g2574.md) outside of the [polis](../../strongs/g/g4172.md) by the [phrear](../../strongs/g/g5421.md) of the [hydōr](../../strongs/g/g5204.md), towards [opse](../../strongs/g/g3796.md), when [ekporeuomai](../../strongs/g/g1607.md) drawing water (hydreuomenai).

<a name="genesis_24_12"></a>Genesis 24:12

And he [eipon](../../strongs/g/g2036.md), O [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) of my [kyrios](../../strongs/g/g2962.md) [Abraam](../../strongs/g/g11.md), [euodoo](../../strongs/g/g2137.md) [enantion](../../strongs/g/g1726.md) me [sēmeron](../../strongs/g/g4594.md), and [poieō](../../strongs/g/g4160.md) [eleos](../../strongs/g/g1656.md) with my [kyrios](../../strongs/g/g2962.md) [Abraam](../../strongs/g/g11.md)!

<a name="genesis_24_13"></a>Genesis 24:13

[idou](../../strongs/g/g2400.md), I [histēmi](../../strongs/g/g2476.md) near the [pēgē](../../strongs/g/g4077.md) of [hydōr](../../strongs/g/g5204.md), and the [thygatēr](../../strongs/g/g2364.md) of the [anthrōpos](../../strongs/g/g444.md) [oikeō](../../strongs/g/g3611.md) the [polis](../../strongs/g/g4172.md) [ekporeuomai](../../strongs/g/g1607.md) to [antleō](../../strongs/g/g501.md) [hydōr](../../strongs/g/g5204.md). 

<a name="genesis_24_14"></a>Genesis 24:14

And it shall be the [parthenos](../../strongs/g/g3933.md), who ever I should have [eipon](../../strongs/g/g2036.md), Incline (epiklinon) your [hydria](../../strongs/g/g5201.md)! that I might [pinō](../../strongs/g/g4095.md). And she should have [eipon](../../strongs/g/g2036.md) to me, You [pinō](../../strongs/g/g4095.md)! and your [kamēlos](../../strongs/g/g2574.md) I will [potizō](../../strongs/g/g4222.md) until whenever they [pauō](../../strongs/g/g3973.md) to [pinō](../../strongs/g/g4095.md) this one you [hetoimazō](../../strongs/g/g2090.md) for your [pais](../../strongs/g/g3816.md) [Isaak](../../strongs/g/g2464.md), and in this I will [ginōskō](../../strongs/g/g1097.md) that you [poieō](../../strongs/g/g4160.md) [eleos](../../strongs/g/g1656.md) with my [kyrios](../../strongs/g/g2962.md) [Abraam](../../strongs/g/g11.md).

<a name="genesis_24_15"></a>Genesis 24:15

And [ginomai](../../strongs/g/g1096.md) before [synteleō](../../strongs/g/g4931.md) his [laleō](../../strongs/g/g2980.md) in his [dianoia](../../strongs/g/g1271.md), that [idou](../../strongs/g/g2400.md), [Rhebekka](../../strongs/g/g4479.md) [ekporeuomai](../../strongs/g/g1607.md) the one [tiktō](../../strongs/g/g5088.md) Bethuel, [huios](../../strongs/g/g5207.md) of Milcah, the [gynē](../../strongs/g/g1135.md) of Nahor, and [adelphos](../../strongs/g/g80.md) of [Abraam](../../strongs/g/g11.md), having the [hydria](../../strongs/g/g5201.md) upon her [ōmos](../../strongs/g/g5606.md). 

<a name="genesis_24_16"></a>Genesis 24:16

And the [parthenos](../../strongs/g/g3933.md) was [kalos](../../strongs/g/g2570.md) in [opsis](../../strongs/g/g3799.md) [sphodra](../../strongs/g/g4970.md). She was a [parthenos](../../strongs/g/g3933.md), no [anēr](../../strongs/g/g435.md) [ginōskō](../../strongs/g/g1097.md) her. And [katabainō](../../strongs/g/g2597.md) upon the [pēgē](../../strongs/g/g4077.md), she [pimplēmi](../../strongs/g/g4130.md) her [hydria](../../strongs/g/g5201.md), and [anabainō](../../strongs/g/g305.md). 

<a name="genesis_24_17"></a>Genesis 24:17

Ran (epedramen) the [pais](../../strongs/g/g3816.md) to [synantēsis](../../strongs/g/g4877.md) her, and he [eipon](../../strongs/g/g2036.md), [potizō](../../strongs/g/g4222.md) a [mikron](../../strongs/g/g3397.md) [hydōr](../../strongs/g/g5204.md) from out of your [hydria](../../strongs/g/g5201.md)! 

<a name="genesis_24_18"></a>Genesis 24:18

 And she [eipon](../../strongs/g/g2036.md), [pinō](../../strongs/g/g4095.md), O [kyrios](../../strongs/g/g2962.md)! And she [speudō](../../strongs/g/g4692.md) and [kathaireō](../../strongs/g/g2507.md) the [hydria](../../strongs/g/g5201.md) upon her [brachiōn](../../strongs/g/g1023.md), and [potizō](../../strongs/g/g4222.md) him until he [pauō](../../strongs/g/g3973.md) [pinō](../../strongs/g/g4095.md). 

<a name="genesis_24_19"></a>Genesis 24:19

And she [eipon](../../strongs/g/g2036.md), Also to your [kamēlos](../../strongs/g/g2574.md) I will draw water (hydreusomai) until whenever all should have [pinō](../../strongs/g/g4095.md). 

<a name="genesis_24_20"></a>Genesis 24:20

And she [speudō](../../strongs/g/g4692.md) and emptied out (exekenōsen) the [hydria](../../strongs/g/g5201.md) into the channel (potistērion), and [trechō](../../strongs/g/g5143.md) yet again unto the [phrear](../../strongs/g/g5421.md) to [antleō](../../strongs/g/g501.md) [hydōr](../../strongs/g/g5204.md). And she drew water (hydreusato) for all the [kamēlos](../../strongs/g/g2574.md). 

<a name="genesis_24_21"></a>Genesis 24:21

And the [anthrōpos](../../strongs/g/g444.md) [katamanthanō](../../strongs/g/g2648.md) her, and remained silent (paresiōpa) to [ginōskō](../../strongs/g/g1097.md) if the [kyrios](../../strongs/g/g2962.md) [euodoo](../../strongs/g/g2137.md) his [hodos](../../strongs/g/g3598.md) or not. 

<a name="genesis_24_22"></a>Genesis 24:22

And [ginomai](../../strongs/g/g1096.md) when [pauō](../../strongs/g/g3973.md) all the [kamēlos](../../strongs/g/g2574.md) [pinō](../../strongs/g/g4095.md), [lambanō](../../strongs/g/g2983.md) the [anthrōpos](../../strongs/g/g444.md) ear-rings (enōtia) of [chrysous](../../strongs/g/g5552.md) worth up to a [drachmē](../../strongs/g/g1406.md) scale-weight (holkēs), and [dyo](../../strongs/g/g1417.md) bracelets (pselia), and put them upon her [cheir](../../strongs/g/g5495.md) -- [deka](../../strongs/g/g1176.md) pieces of [chrysous](../../strongs/g/g5552.md) was their weight (holkē). 

<a name="genesis_24_23"></a>Genesis 24:23

And he [eperōtaō](../../strongs/g/g1905.md) her, and [eipon](../../strongs/g/g2036.md), Whose [thygatēr](../../strongs/g/g2364.md) are you? [anaggellō](../../strongs/g/g312.md) to me if there is by your [patēr](../../strongs/g/g3962.md) a [topos](../../strongs/g/g5117.md) for us to [katalyō](../../strongs/g/g2647.md)! 

<a name="genesis_24_24"></a>Genesis 24:24

 And she [eipon](../../strongs/g/g2036.md) to him, the [thygatēr](../../strongs/g/g2364.md) of Bathuel I am, the son of Milcah, whom she [tiktō](../../strongs/g/g5088.md) [Nachōr](../../strongs/g/g3493.md).

<a name="genesis_24_25"></a>Genesis 24:25

And she [eipon](../../strongs/g/g2036.md) to him, also [achyron](../../strongs/g/g892.md) and [chortasma](../../strongs/g/g5527.md) there is [polys](../../strongs/g/g4183.md) by us, and a [topos](../../strongs/g/g5117.md) to [katalyō](../../strongs/g/g2647.md). 

<a name="genesis_24_26"></a>Genesis 24:26

And [eudokeō](../../strongs/g/g2106.md) the [anthrōpos](../../strongs/g/g444.md), did [proskyneō](../../strongs/g/g4352.md) to the [kyrios](../../strongs/g/g2962.md), and [eipon](../../strongs/g/g2036.md), 

<a name="genesis_24_27"></a>Genesis 24:27

[eulogētos](../../strongs/g/g2128.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) of my [kyrios](../../strongs/g/g2962.md) [Abraam](../../strongs/g/g11.md), who [egkataleipō](../../strongs/g/g1459.md) not [dikaiosynē](../../strongs/g/g1343.md) and [alētheia](../../strongs/g/g225.md) from my [kyrios](../../strongs/g/g2962.md). indeed [euodoo](../../strongs/g/g2137.md) me the [kyrios](../../strongs/g/g2962.md) to the [oikos](../../strongs/g/g3624.md) of the [adelphos](../../strongs/g/g80.md) of my [kyrios](../../strongs/g/g2962.md).

<a name="genesis_24_28"></a>Genesis 24:28

And [trechō](../../strongs/g/g5143.md), the [pais](../../strongs/g/g3816.md) [anaggellō](../../strongs/g/g312.md) in the [oikos](../../strongs/g/g3624.md) to her [mētēr](../../strongs/g/g3384.md) about these [rhēma](../../strongs/g/g4487.md). 

<a name="genesis_24_29"></a>Genesis 24:29

And to [Rhebekka](../../strongs/g/g4479.md) there was an [adelphos](../../strongs/g/g80.md) whose [onoma](../../strongs/g/g3686.md) was Laban. And Laban [trechō](../../strongs/g/g5143.md) to the [anthrōpos](../../strongs/g/g444.md) outside near the [pēgē](../../strongs/g/g4077.md). 

<a name="genesis_24_30"></a>Genesis 24:30

And [ginomai](../../strongs/g/g1096.md) when he [eidō](../../strongs/g/g1492.md) the ear-rings (enōtia), and the bracelets (pselia) upon the [cheir](../../strongs/g/g5495.md) of his [adelphē](../../strongs/g/g79.md), and when he [akouō](../../strongs/g/g191.md) the [rhēma](../../strongs/g/g4487.md) of [Rhebekka](../../strongs/g/g4479.md) his [adelphē](../../strongs/g/g79.md), [legō](../../strongs/g/g3004.md), So [laleō](../../strongs/g/g2980.md) to me the [anthrōpos](../../strongs/g/g444.md); that he [erchomai](../../strongs/g/g2064.md) to the [anthrōpos](../../strongs/g/g444.md) [histēmi](../../strongs/g/g2476.md) by himself by the [kamēlos](../../strongs/g/g2574.md) near the [pēgē](../../strongs/g/g4077.md). 

<a name="genesis_24_31"></a>Genesis 24:31

And he [eipon](../../strongs/g/g2036.md) to him, [deuro](../../strongs/g/g1204.md), [eiserchomai](../../strongs/g/g1525.md) [eulogētos](../../strongs/g/g2128.md) of the [kyrios](../../strongs/g/g2962.md)! Why [histēmi](../../strongs/g/g2476.md) outside? For I [hetoimazō](../../strongs/g/g2090.md) the [oikia](../../strongs/g/g3614.md), and a [topos](../../strongs/g/g5117.md) for the [kamēlos](../../strongs/g/g2574.md). 

<a name="genesis_24_32"></a>Genesis 24:32

[eiserchomai](../../strongs/g/g1525.md) the [anthrōpos](../../strongs/g/g444.md) into the [oikia](../../strongs/g/g3614.md). And he unharnessed (apesaxen) the [kamēlos](../../strongs/g/g2574.md), and [didōmi](../../strongs/g/g1325.md) [achyron](../../strongs/g/g892.md) and [chortasma](../../strongs/g/g5527.md) to the [kamēlos](../../strongs/g/g2574.md), and [hydōr](../../strongs/g/g5204.md) for his [pous](../../strongs/g/g4228.md), and for the [pous](../../strongs/g/g4228.md) of the [anēr](../../strongs/g/g435.md) with him. 

<a name="genesis_24_33"></a>Genesis 24:33

And he [paratithēmi](../../strongs/g/g3908.md) them [artos](../../strongs/g/g740.md) to [esthiō](../../strongs/g/g2068.md). And he [eipon](../../strongs/g/g2036.md), No way will I [esthiō](../../strongs/g/g2068.md) until [laleō](../../strongs/g/g2980.md) my [rhēma](../../strongs/g/g4487.md). And he [eipon](../../strongs/g/g2036.md), [laleō](../../strongs/g/g2980.md)! 

<a name="genesis_24_34"></a>Genesis 24:34

And he [eipon](../../strongs/g/g2036.md), A [pais](../../strongs/g/g3816.md) of [Abraam](../../strongs/g/g11.md) I am.

<a name="genesis_24_35"></a>Genesis 24:35

And the [kyrios](../../strongs/g/g2962.md) [eulogeō](../../strongs/g/g2127.md) my [kyrios](../../strongs/g/g2962.md) [sphodra](../../strongs/g/g4970.md); and he was [hypsoō](../../strongs/g/g5312.md), and he [didōmi](../../strongs/g/g1325.md) to him [probaton](../../strongs/g/g4263.md), and [moschos](../../strongs/g/g3448.md), and [argyrion](../../strongs/g/g694.md), and pieces of [chrysion](../../strongs/g/g5553.md), and [pais](../../strongs/g/g3816.md), and [paidiskē](../../strongs/g/g3814.md), [kamēlos](../../strongs/g/g2574.md), and [onos](../../strongs/g/g3688.md). 

<a name="genesis_24_36"></a>Genesis 24:36

And [tiktō](../../strongs/g/g5088.md) [Sarra](../../strongs/g/g4564.md) the [gynē](../../strongs/g/g1135.md) of my [kyrios](../../strongs/g/g2962.md) a [huios](../../strongs/g/g5207.md), one to my [kyrios](../../strongs/g/g2962.md) after his [gēraskō](../../strongs/g/g1095.md). And he [didōmi](../../strongs/g/g1325.md) to him as much as was his. 

<a name="genesis_24_37"></a>Genesis 24:37

And [horkizō](../../strongs/g/g3726.md) my [kyrios](../../strongs/g/g2962.md), [legō](../../strongs/g/g3004.md), You shall not [lambanō](../../strongs/g/g2983.md) a [gynē](../../strongs/g/g1135.md) for my [huios](../../strongs/g/g5207.md) from the [thygatēr](../../strongs/g/g2364.md) of the [Chananaios](../../strongs/g/g5478.md), in which I [paroikeō](../../strongs/g/g3939.md) in their [gē](../../strongs/g/g1093.md). 

<a name="genesis_24_38"></a>Genesis 24:38

But unto the [oikos](../../strongs/g/g3624.md) of my [patēr](../../strongs/g/g3962.md) you should [poreuō](../../strongs/g/g4198.md), and to my [phylē](../../strongs/g/g5443.md); and you shall [lambanō](../../strongs/g/g2983.md) a [gynē](../../strongs/g/g1135.md) for my [huios](../../strongs/g/g5207.md).

<a name="genesis_24_39"></a>Genesis 24:39

And I [eipon](../../strongs/g/g2036.md) to my [kyrios](../../strongs/g/g2962.md), Perhaps should not [poreuō](../../strongs/g/g4198.md) the [gynē](../../strongs/g/g1135.md) with me. 

<a name="genesis_24_40"></a>Genesis 24:40

And he [eipon](../../strongs/g/g2036.md) to me, the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) in whom I was [euaresteō](../../strongs/g/g2100.md) [enantion](../../strongs/g/g1726.md) him, he will [exapostellō](../../strongs/g/g1821.md) his [aggelos](../../strongs/g/g32.md) with you, and [euodoo](../../strongs/g/g2137.md) your [hodos](../../strongs/g/g3598.md). And you will [lambanō](../../strongs/g/g2983.md) a [gynē](../../strongs/g/g1135.md) for my [huios](../../strongs/g/g5207.md) from out of my [phylē](../../strongs/g/g5443.md), and from out of the [oikos](../../strongs/g/g3624.md) of my [patēr](../../strongs/g/g3962.md).

<a name="genesis_24_41"></a>Genesis 24:41

Then [athōos](../../strongs/g/g121.md) you will be concerning my [ara](../../strongs/g/g685.md). For when ever you should have [erchomai](../../strongs/g/g2064.md) to my [phylē](../../strongs/g/g5443.md), and not to you they should [didōmi](../../strongs/g/g1325.md) her, then you will be [athōos](../../strongs/g/g121.md) concerning my oath (horkismou).

<a name="genesis_24_42"></a>Genesis 24:42

And [erchomai](../../strongs/g/g2064.md) [sēmeron](../../strongs/g/g4594.md) upon the [pēgē](../../strongs/g/g4077.md), I [eipon](../../strongs/g/g2036.md), O [kyrios](../../strongs/g/g2962.md), the [theos](../../strongs/g/g2316.md) of my [kyrios](../../strongs/g/g2962.md) [Abraam](../../strongs/g/g11.md); if you [euodoo](../../strongs/g/g2137.md) my [hodos](../../strongs/g/g3598.md), in which now I [poreuō](../../strongs/g/g4198.md) by it, 

<a name="genesis_24_43"></a>Genesis 24:43

[idou](../../strongs/g/g2400.md), I [ephistēmi](../../strongs/g/g2186.md) near the [pēgē](../../strongs/g/g4077.md) of [hydōr](../../strongs/g/g5204.md), and the [thygatēr](../../strongs/g/g2364.md) of the [anthrōpos](../../strongs/g/g444.md) of the [polis](../../strongs/g/g4172.md) [ekporeuomai](../../strongs/g/g1607.md) to [antleō](../../strongs/g/g501.md) [hydōr](../../strongs/g/g5204.md), and it will be the [parthenos](../../strongs/g/g3933.md) to whom ever I should [eipon](../../strongs/g/g2036.md), to [potizō](../../strongs/g/g4222.md) me from your [hydria](../../strongs/g/g5201.md) a [mikron](../../strongs/g/g3397.md) [hydōr](../../strongs/g/g5204.md)! 

<a name="genesis_24_44"></a>Genesis 24:44

And she should have [eipon](../../strongs/g/g2036.md) to me, You also [pinō](../../strongs/g/g4095.md), and to your [kamēlos](../../strongs/g/g2574.md) I will draw water (hydreusomai). This is the [gynē](../../strongs/g/g1135.md) whom the [kyrios](../../strongs/g/g2962.md) [hetoimazō](../../strongs/g/g2090.md) for his own [therapōn](../../strongs/g/g2324.md) [Isaak](../../strongs/g/g2464.md); and in this you will [ginōskō](../../strongs/g/g1097.md) that you have [poieō](../../strongs/g/g4160.md) [eleos](../../strongs/g/g1656.md) to my [kyrios](../../strongs/g/g2962.md) [Abraam](../../strongs/g/g11.md).

<a name="genesis_24_45"></a>Genesis 24:45

And [ginomai](../../strongs/g/g1096.md) before the [synteleō](../../strongs/g/g4931.md) my [laleō](../../strongs/g/g2980.md) in my [dianoia](../../strongs/g/g1271.md), [eutheōs](../../strongs/g/g2112.md) [Rhebekka](../../strongs/g/g4479.md) [ekporeuomai](../../strongs/g/g1607.md) having the [hydria](../../strongs/g/g5201.md) upon the [ōmos](../../strongs/g/g5606.md), and she went [katabainō](../../strongs/g/g2597.md) to the [pēgē](../../strongs/g/g4077.md), and she drew water (hydreusato). And I [eipon](../../strongs/g/g2036.md) to her, [potizō](../../strongs/g/g4222.md)!

<a name="genesis_24_46"></a>Genesis 24:46

And she [speudō](../../strongs/g/g4692.md) to [kathaireō](../../strongs/g/g2507.md) the [hydria](../../strongs/g/g5201.md) upon her [brachiōn](../../strongs/g/g1023.md) from herself, and she [eipon](../../strongs/g/g2036.md), You [pinō](../../strongs/g/g4095.md)! and your [kamēlos](../../strongs/g/g2574.md) I will [potizō](../../strongs/g/g4222.md). And I [pinō](../../strongs/g/g4095.md), and the [kamēlos](../../strongs/g/g2574.md) she [potizō](../../strongs/g/g4222.md). 

<a name="genesis_24_47"></a>Genesis 24:47

And I [erōtaō](../../strongs/g/g2065.md) her, and [eipon](../../strongs/g/g2036.md), Whose [thygatēr](../../strongs/g/g2364.md) are you? And she [phēmi](../../strongs/g/g5346.md), the [thygatēr](../../strongs/g/g2364.md) of Bethuel I am, [huios](../../strongs/g/g5207.md) of Nahor, whom [tiktō](../../strongs/g/g5088.md) to him Milcah. And I [peritithēmi](../../strongs/g/g4060.md) her the ear-rings (enōtia), and the bracelets (pselia) about her [cheir](../../strongs/g/g5495.md). 

<a name="genesis_24_48"></a>Genesis 24:48

And finding [eudokeō](../../strongs/g/g2106.md), I did [proskyneō](../../strongs/g/g4352.md) to the lord [kyrios](../../strongs/g/g2962.md). And I [eulogeō](../../strongs/g/g2127.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) of my master Abraham, who [euodoo](../../strongs/g/g2137.md) me in the [hodos](../../strongs/g/g3598.md) of [alētheia](../../strongs/g/g225.md), to [lambanō](../../strongs/g/g2983.md) the [thygatēr](../../strongs/g/g2364.md) of the [adelphos](../../strongs/g/g80.md) of my [kyrios](../../strongs/g/g2962.md), for his [huios](../../strongs/g/g5207.md).

<a name="genesis_24_49"></a>Genesis 24:49

If then you [poieō](../../strongs/g/g4160.md) [eleos](../../strongs/g/g1656.md) and [dikaiosynē](../../strongs/g/g1343.md) to my [kyrios](../../strongs/g/g2962.md), [apaggellō](../../strongs/g/g518.md) to me that I might [epistrephō](../../strongs/g/g1994.md) to the [dexios](../../strongs/g/g1188.md) or [aristeros](../../strongs/g/g710.md)! 

<a name="genesis_24_50"></a>Genesis 24:50

And [apokrinomai](../../strongs/g/g611.md) Laban and Bethuel [eipon](../../strongs/g/g2036.md), From the [kyrios](../../strongs/g/g2962.md) came [exerchomai](../../strongs/g/g1831.md) this [pragma](../../strongs/g/g4229.md), we will not be able to [anteipon](../../strongs/g/g471.md) [kakos](../../strongs/g/g2556.md) or [kalos](../../strongs/g/g2570.md). 

<a name="genesis_24_51"></a>Genesis 24:51

[idou](../../strongs/g/g2400.md), [Rhebekka](../../strongs/g/g4479.md) is [enōpion](../../strongs/g/g1799.md) you, in taking her [lambanō](../../strongs/g/g2983.md), run (apotreche) ! and let her be [gynē](../../strongs/g/g1135.md) to the [huios](../../strongs/g/g5207.md) of your [kyrios](../../strongs/g/g2962.md)! as the [kyrios](../../strongs/g/g2962.md) [laleō](../../strongs/g/g2980.md).

<a name="genesis_24_52"></a>Genesis 24:52
G11

And [ginomai](../../strongs/g/g1096.md) in the [akouō](../../strongs/g/g191.md) of the [pais](../../strongs/g/g3816.md) of [Abraam](../../strongs/g/g11.md) their [rhēma](../../strongs/g/g4487.md), he did [proskyneō](../../strongs/g/g4352.md) upon the [gē](../../strongs/g/g1093.md) to the [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_24_53"></a>Genesis 24:53

And [ekpherō](../../strongs/g/g1627.md) the [pais](../../strongs/g/g3816.md) [skeuos](../../strongs/g/g4632.md) made of [argyreos](../../strongs/g/g693.md) and of [chrysous](../../strongs/g/g5552.md). And [himatismos](../../strongs/g/g2441.md) he [didōmi](../../strongs/g/g1325.md) to [Rhebekka](../../strongs/g/g4479.md), and [dōron](../../strongs/g/g1435.md) he [didōmi](../../strongs/g/g1325.md) to her [adelphos](../../strongs/g/g80.md), and to her [mētēr](../../strongs/g/g3384.md).

<a name="genesis_24_54"></a>Genesis 24:54

And they [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md). And he and the [anēr](../../strongs/g/g435.md) with him being went to [koimaō](../../strongs/g/g2837.md). And [anistēmi](../../strongs/g/g450.md) in the [prōï](../../strongs/g/g4404.md), he [eipon](../../strongs/g/g2036.md), [ekpempō](../../strongs/g/g1599.md)! that I may [aperchomai](../../strongs/g/g565.md) to my [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_24_55"></a>Genesis 24:55

[eipon](../../strongs/g/g2036.md) her [adelphos](../../strongs/g/g80.md) and [mētēr](../../strongs/g/g3384.md), [menō](../../strongs/g/g3306.md) the [parthenos](../../strongs/g/g3933.md) with us [hēmera](../../strongs/g/g2250.md) about [deka](../../strongs/g/g1176.md)! and after this she shall [aperchomai](../../strongs/g/g565.md). 

<a name="genesis_24_56"></a>Genesis 24:56

And he [eipon](../../strongs/g/g2036.md) to them, Do not [katechō](../../strongs/g/g2722.md) me, for the [kyrios](../../strongs/g/g2962.md) [euodoo](../../strongs/g/g2137.md) my [hodos](../../strongs/g/g3598.md)! [ekpempō](../../strongs/g/g1599.md) me! that I may [aperchomai](../../strongs/g/g565.md) to my [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_24_57"></a>Genesis 24:57

And they [eipon](../../strongs/g/g2036.md), We should [kaleō](../../strongs/g/g2564.md) the [pais](../../strongs/g/g3816.md), and we should [erōtaō](../../strongs/g/g2065.md) by her [stoma](../../strongs/g/g4750.md). 

<a name="genesis_24_58"></a>Genesis 24:58

And they [kaleō](../../strongs/g/g2564.md) [Rhebekka](../../strongs/g/g4479.md), and [eipon](../../strongs/g/g2036.md) to her, Will you [poreuō](../../strongs/g/g4198.md) with this [anthrōpos](../../strongs/g/g444.md)? And she [eipon](../../strongs/g/g2036.md), I will [poreuō](../../strongs/g/g4198.md). 

<a name="genesis_24_59"></a>Genesis 24:59

And they [ekpempō](../../strongs/g/g1599.md) [Rhebekka](../../strongs/g/g4479.md) their [adelphē](../../strongs/g/g79.md), and her [hyparchonta](../../strongs/g/g5224.md), and the [pais](../../strongs/g/g3816.md) of [Abraam](../../strongs/g/g11.md), and the ones with him. 

<a name="genesis_24_60"></a>Genesis 24:60

And they [eulogeō](../../strongs/g/g2127.md) [Rhebekka](../../strongs/g/g4479.md), and [eipon](../../strongs/g/g2036.md) to her, [adelphē](../../strongs/g/g79.md) our You are, become for a [chilioi](../../strongs/g/g5507.md) [myrias](../../strongs/g/g3461.md), and [klēronomeō](../../strongs/g/g2816.md) let your [sperma](../../strongs/g/g4690.md) the [polis](../../strongs/g/g4172.md) of your [hypenantios](../../strongs/g/g5227.md)! 

<a name="genesis_24_61"></a>Genesis 24:61

And [anistēmi](../../strongs/g/g450.md), [Rhebekka](../../strongs/g/g4479.md) and her handmaidens (habrai) [epibainō](../../strongs/g/g1910.md) upon the [kamēlos](../../strongs/g/g2574.md), and they [poreuō](../../strongs/g/g4198.md) with the [anthrōpos](../../strongs/g/g444.md). And [analambanō](../../strongs/g/g353.md) the [pais](../../strongs/g/g3816.md) [Rhebekka](../../strongs/g/g4479.md), he went [aperchomai](../../strongs/g/g565.md). 

<a name="genesis_24_62"></a>Genesis 24:62

And [Isaak](../../strongs/g/g2464.md) was [diaporeuomai](../../strongs/g/g1279.md) through the [erēmos](../../strongs/g/g2048.md) by the [phrear](../../strongs/g/g5421.md) of the [horasis](../../strongs/g/g3706.md). And he [katoikeō](../../strongs/g/g2730.md) in the [gē](../../strongs/g/g1093.md) towards the [lips](../../strongs/g/g3047.md). 

<a name="genesis_24_63"></a>Genesis 24:63

And [Isaak](../../strongs/g/g2464.md) [exerchomai](../../strongs/g/g1831.md) to meditate (adoleschēsai) in the plain (pedion) towards afternoon (deilēs). And [anablepō](../../strongs/g/g308.md) his [ophthalmos](../../strongs/g/g3788.md), he [eidō](../../strongs/g/g1492.md) [kamēlos](../../strongs/g/g2574.md) [erchomai](../../strongs/g/g2064.md). 

<a name="genesis_24_64"></a>Genesis 24:64

And [Rhebekka](../../strongs/g/g4479.md) [anablepō](../../strongs/g/g308.md) with the [ophthalmos](../../strongs/g/g3788.md), [eidō](../../strongs/g/g1492.md) [Isaak](../../strongs/g/g2464.md), and she leaped down (katepēdēsen) from the [kamēlos](../../strongs/g/g2574.md). 

<a name="genesis_24_65"></a>Genesis 24:65

And she [eipon](../../strongs/g/g2036.md) to the [pais](../../strongs/g/g3816.md), Who is that [anthrōpos](../../strongs/g/g444.md) [poreuō](../../strongs/g/g4198.md) in the plain (pediō) to [synantēsis](../../strongs/g/g4877.md) us? And [eipon](../../strongs/g/g2036.md) the [pais](../../strongs/g/g3816.md), This is my [kyrios](../../strongs/g/g2962.md). And she having [lambanō](../../strongs/g/g2983.md) the lightweight covering (theristron) [periballō](../../strongs/g/g4016.md) it. 

<a name="genesis_24_66"></a>Genesis 24:66

And [diēgeomai](../../strongs/g/g1334.md) the [pais](../../strongs/g/g3816.md) to [Isaak](../../strongs/g/g2464.md) all the [rhēma](../../strongs/g/g4487.md) which he [poieō](../../strongs/g/g4160.md). 

<a name="genesis_24_67"></a>Genesis 24:67

[eiserchomai](../../strongs/g/g1525.md) [Isaak](../../strongs/g/g2464.md) into the [oikos](../../strongs/g/g3624.md) of his [mētēr](../../strongs/g/g3384.md), and [lambanō](../../strongs/g/g2983.md) [Rhebekka](../../strongs/g/g4479.md), and she became his [gynē](../../strongs/g/g1135.md). And he [agapaō](../../strongs/g/g25.md) her. And [Isaak](../../strongs/g/g2464.md) was [parakaleō](../../strongs/g/g3870.md) concerning [Sarra](../../strongs/g/g4564.md) his [mētēr](../../strongs/g/g3384.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 23](genesis_lxx_23.md) - [Genesis 25](genesis_lxx_25.md)