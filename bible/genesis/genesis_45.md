# [Genesis 45](https://www.blueletterbible.org/kjv/gen/45/1/s_45001)

<a name="genesis_45_1"></a>Genesis 45:1

Then [Yôsēp̄](../../strongs/h/h3130.md) [yakol](../../strongs/h/h3201.md) not ['āp̄aq](../../strongs/h/h662.md) himself before all them that [nāṣaḇ](../../strongs/h/h5324.md) by him; and he [qara'](../../strongs/h/h7121.md), Cause every ['iysh](../../strongs/h/h376.md) to [yāṣā'](../../strongs/h/h3318.md) from me. And there ['amad](../../strongs/h/h5975.md) no ['iysh](../../strongs/h/h376.md) with him, while [Yôsēp̄](../../strongs/h/h3130.md) made himself [yada'](../../strongs/h/h3045.md) unto his ['ach](../../strongs/h/h251.md).

<a name="genesis_45_2"></a>Genesis 45:2

And he [qowl](../../strongs/h/h6963.md) [bĕkiy](../../strongs/h/h1065.md) [nathan](../../strongs/h/h5414.md): and the [Mitsrayim](../../strongs/h/h4714.md) and the [bayith](../../strongs/h/h1004.md) of [Parʿô](../../strongs/h/h6547.md) [shama'](../../strongs/h/h8085.md).

<a name="genesis_45_3"></a>Genesis 45:3

And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto his ['ach](../../strongs/h/h251.md), I am [Yôsēp̄](../../strongs/h/h3130.md); doth my ['ab](../../strongs/h/h1.md) yet [chay](../../strongs/h/h2416.md)? And his ['ach](../../strongs/h/h251.md) [yakol](../../strongs/h/h3201.md) not ['anah](../../strongs/h/h6030.md) him; for they were [bahal](../../strongs/h/h926.md) at his [paniym](../../strongs/h/h6440.md).

<a name="genesis_45_4"></a>Genesis 45:4

And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto his ['ach](../../strongs/h/h251.md), [nāḡaš](../../strongs/h/h5066.md) to me, I pray you. And they [nāḡaš](../../strongs/h/h5066.md). And he ['āmar](../../strongs/h/h559.md), I am [Yôsēp̄](../../strongs/h/h3130.md) your ['ach](../../strongs/h/h251.md), whom ye [māḵar](../../strongs/h/h4376.md) into [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_45_5"></a>Genesis 45:5

Now therefore be not [ʿāṣaḇ](../../strongs/h/h6087.md), nor [ḥārâ](../../strongs/h/h2734.md) with ['ayin](../../strongs/h/h5869.md), that ye [māḵar](../../strongs/h/h4376.md) me hither: for ['Elohiym](../../strongs/h/h430.md) did [shalach](../../strongs/h/h7971.md) me [paniym](../../strongs/h/h6440.md) you to [miḥyâ](../../strongs/h/h4241.md).

<a name="genesis_45_6"></a>Genesis 45:6

For these two [šānâ](../../strongs/h/h8141.md) hath the [rāʿāḇ](../../strongs/h/h7458.md) [qereḇ](../../strongs/h/h7130.md) the ['erets](../../strongs/h/h776.md): and yet there are five [šānâ](../../strongs/h/h8141.md), in the which there shall neither be [ḥārîš](../../strongs/h/h2758.md) nor [qāṣîr](../../strongs/h/h7105.md).

<a name="genesis_45_7"></a>Genesis 45:7

And ['Elohiym](../../strongs/h/h430.md) [shalach](../../strongs/h/h7971.md) me [paniym](../../strongs/h/h6440.md) you to [śûm](../../strongs/h/h7760.md) you a [šᵊ'ērîṯ](../../strongs/h/h7611.md) in the ['erets](../../strongs/h/h776.md), and to save your [ḥāyâ](../../strongs/h/h2421.md) by a [gadowl](../../strongs/h/h1419.md) [pᵊlêṭâ](../../strongs/h/h6413.md).

<a name="genesis_45_8"></a>Genesis 45:8

So now it was not you that [shalach](../../strongs/h/h7971.md) me hither, but ['Elohiym](../../strongs/h/h430.md): and he hath [śûm](../../strongs/h/h7760.md) me an ['ab](../../strongs/h/h1.md) to [Parʿô](../../strongs/h/h6547.md), and ['adown](../../strongs/h/h113.md) of all his [bayith](../../strongs/h/h1004.md), and a [mashal](../../strongs/h/h4910.md) throughout all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_45_9"></a>Genesis 45:9

[māhar](../../strongs/h/h4116.md) ye, and [ʿālâ](../../strongs/h/h5927.md) to my ['ab](../../strongs/h/h1.md), and ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) thy [ben](../../strongs/h/h1121.md) [Yôsēp̄](../../strongs/h/h3130.md), ['Elohiym](../../strongs/h/h430.md) hath [śûm](../../strongs/h/h7760.md) me ['adown](../../strongs/h/h113.md) of all [Mitsrayim](../../strongs/h/h4714.md): [yarad](../../strongs/h/h3381.md) unto me, ['amad](../../strongs/h/h5975.md) not:

<a name="genesis_45_10"></a>Genesis 45:10

And thou shalt [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [gōšen](../../strongs/h/h1657.md), and thou shalt be [qarowb](../../strongs/h/h7138.md) unto me, thou, and thy [ben](../../strongs/h/h1121.md), and thy [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md), and thy [tso'n](../../strongs/h/h6629.md), and thy [bāqār](../../strongs/h/h1241.md), and all that thou hast:

<a name="genesis_45_11"></a>Genesis 45:11

And there will I [kûl](../../strongs/h/h3557.md) thee; for yet there are five [šānâ](../../strongs/h/h8141.md) of [rāʿāḇ](../../strongs/h/h7458.md); lest thou, and thy [bayith](../../strongs/h/h1004.md), and all that thou hast, come to [yarash](../../strongs/h/h3423.md).

<a name="genesis_45_12"></a>Genesis 45:12

And, behold, your ['ayin](../../strongs/h/h5869.md) [ra'ah](../../strongs/h/h7200.md), and the ['ayin](../../strongs/h/h5869.md) of my ['ach](../../strongs/h/h251.md) [Binyāmîn](../../strongs/h/h1144.md), that it is my [peh](../../strongs/h/h6310.md) that [dabar](../../strongs/h/h1696.md) unto you.

<a name="genesis_45_13"></a>Genesis 45:13

And ye shall [nāḡaḏ](../../strongs/h/h5046.md) my ['ab](../../strongs/h/h1.md) of all my [kabowd](../../strongs/h/h3519.md) in [Mitsrayim](../../strongs/h/h4714.md), and of all that ye have [ra'ah](../../strongs/h/h7200.md); and ye shall [māhar](../../strongs/h/h4116.md) and [yarad](../../strongs/h/h3381.md) my ['ab](../../strongs/h/h1.md) hither.

<a name="genesis_45_14"></a>Genesis 45:14

And he [naphal](../../strongs/h/h5307.md) upon his ['ach](../../strongs/h/h251.md) [Binyāmîn](../../strongs/h/h1144.md) [ṣaûā'r](../../strongs/h/h6677.md), and [bāḵâ](../../strongs/h/h1058.md); and [Binyāmîn](../../strongs/h/h1144.md) [bāḵâ](../../strongs/h/h1058.md) upon his [ṣaûā'r](../../strongs/h/h6677.md).

<a name="genesis_45_15"></a>Genesis 45:15

Moreover he [nashaq](../../strongs/h/h5401.md) all his ['ach](../../strongs/h/h251.md), and [bāḵâ](../../strongs/h/h1058.md) upon them: and ['aḥar](../../strongs/h/h310.md) that his ['ach](../../strongs/h/h251.md) [dabar](../../strongs/h/h1696.md) with him.

<a name="genesis_45_16"></a>Genesis 45:16

And the [qowl](../../strongs/h/h6963.md) thereof was [shama'](../../strongs/h/h8085.md) in [Parʿô](../../strongs/h/h6547.md) [bayith](../../strongs/h/h1004.md), ['āmar](../../strongs/h/h559.md), [Yôsēp̄](../../strongs/h/h3130.md) ['ach](../../strongs/h/h251.md) are [bow'](../../strongs/h/h935.md): and it [yatab](../../strongs/h/h3190.md) ['ayin](../../strongs/h/h5869.md) [Parʿô](../../strongs/h/h6547.md), and his ['ebed](../../strongs/h/h5650.md).

<a name="genesis_45_17"></a>Genesis 45:17

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md) unto [Yôsēp̄](../../strongs/h/h3130.md), ['āmar](../../strongs/h/h559.md) unto thy ['ach](../../strongs/h/h251.md), This ['asah](../../strongs/h/h6213.md) ye; [ṭāʿan](../../strongs/h/h2943.md) your [bᵊʿîr](../../strongs/h/h1165.md), and [yālaḵ](../../strongs/h/h3212.md), [bow'](../../strongs/h/h935.md) you unto the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md);

<a name="genesis_45_18"></a>Genesis 45:18

And [laqach](../../strongs/h/h3947.md) your ['ab](../../strongs/h/h1.md) and your [bayith](../../strongs/h/h1004.md), and [bow'](../../strongs/h/h935.md) unto me: and I will [nathan](../../strongs/h/h5414.md) you the [ṭûḇ](../../strongs/h/h2898.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and ye shall ['akal](../../strongs/h/h398.md) the [cheleb](../../strongs/h/h2459.md) of the ['erets](../../strongs/h/h776.md).

<a name="genesis_45_19"></a>Genesis 45:19

Now thou art [tsavah](../../strongs/h/h6680.md), this ['asah](../../strongs/h/h6213.md) ye; [laqach](../../strongs/h/h3947.md) you [ʿăḡālâ](../../strongs/h/h5699.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) for your [ṭap̄](../../strongs/h/h2945.md), and for your ['ishshah](../../strongs/h/h802.md), and [nasa'](../../strongs/h/h5375.md) your ['ab](../../strongs/h/h1.md), and [bow'](../../strongs/h/h935.md).

<a name="genesis_45_20"></a>Genesis 45:20

['ayin](../../strongs/h/h5869.md) [ḥûs](../../strongs/h/h2347.md) not your [kĕliy](../../strongs/h/h3627.md); for the [ṭûḇ](../../strongs/h/h2898.md) of all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) is your's.

<a name="genesis_45_21"></a>Genesis 45:21

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) so: and [Yôsēp̄](../../strongs/h/h3130.md) [nathan](../../strongs/h/h5414.md) them [ʿăḡālâ](../../strongs/h/h5699.md), according to the [peh](../../strongs/h/h6310.md) of [Parʿô](../../strongs/h/h6547.md), and [nathan](../../strongs/h/h5414.md) them [ṣêḏâ](../../strongs/h/h6720.md) for the [derek](../../strongs/h/h1870.md).

<a name="genesis_45_22"></a>Genesis 45:22

To all of them he [nathan](../../strongs/h/h5414.md) each ['iysh](../../strongs/h/h376.md) [ḥălîp̄â](../../strongs/h/h2487.md) of [śimlâ](../../strongs/h/h8071.md); but to [Binyāmîn](../../strongs/h/h1144.md) he [nathan](../../strongs/h/h5414.md) three hundred pieces of [keceph](../../strongs/h/h3701.md), and five [ḥălîp̄â](../../strongs/h/h2487.md) of [śimlâ](../../strongs/h/h8071.md).

<a name="genesis_45_23"></a>Genesis 45:23

And to his ['ab](../../strongs/h/h1.md) he [shalach](../../strongs/h/h7971.md) after this manner; ten ['āṯôn](../../strongs/h/h860.md) [nasa'](../../strongs/h/h5375.md) with the [ṭûḇ](../../strongs/h/h2898.md) of [Mitsrayim](../../strongs/h/h4714.md), and ten [chamowr](../../strongs/h/h2543.md) [nasa'](../../strongs/h/h5375.md) with [bar](../../strongs/h/h1250.md) and [lechem](../../strongs/h/h3899.md) and [māzôn](../../strongs/h/h4202.md) for his ['ab](../../strongs/h/h1.md) by the [derek](../../strongs/h/h1870.md).

<a name="genesis_45_24"></a>Genesis 45:24

So he [shalach](../../strongs/h/h7971.md) his ['ach](../../strongs/h/h251.md), and they [yālaḵ](../../strongs/h/h3212.md): and he ['āmar](../../strongs/h/h559.md) unto them, See that ye not [ragaz](../../strongs/h/h7264.md) by the [derek](../../strongs/h/h1870.md).

<a name="genesis_45_25"></a>Genesis 45:25

And they [ʿālâ](../../strongs/h/h5927.md) out of [Mitsrayim](../../strongs/h/h4714.md), and [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md) unto [Ya'aqob](../../strongs/h/h3290.md) their ['ab](../../strongs/h/h1.md),

<a name="genesis_45_26"></a>Genesis 45:26

And [nāḡaḏ](../../strongs/h/h5046.md) him, ['āmar](../../strongs/h/h559.md), [Yôsēp̄](../../strongs/h/h3130.md) is yet [chay](../../strongs/h/h2416.md), and he is [mashal](../../strongs/h/h4910.md) over all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md). And [leb](../../strongs/h/h3820.md) [pûḡ](../../strongs/h/h6313.md), for he ['aman](../../strongs/h/h539.md) them not.

<a name="genesis_45_27"></a>Genesis 45:27

And they [dabar](../../strongs/h/h1696.md) him all the [dabar](../../strongs/h/h1697.md) of [Yôsēp̄](../../strongs/h/h3130.md), which he had [dabar](../../strongs/h/h1696.md) unto them: and when he [ra'ah](../../strongs/h/h7200.md) the [ʿăḡālâ](../../strongs/h/h5699.md) which [Yôsēp̄](../../strongs/h/h3130.md) had [shalach](../../strongs/h/h7971.md) to [nasa'](../../strongs/h/h5375.md) him, the [ruwach](../../strongs/h/h7307.md) of [Ya'aqob](../../strongs/h/h3290.md) their ['ab](../../strongs/h/h1.md) [ḥāyâ](../../strongs/h/h2421.md):

<a name="genesis_45_28"></a>Genesis 45:28

And [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md), It is [rab](../../strongs/h/h7227.md); [Yôsēp̄](../../strongs/h/h3130.md) my [ben](../../strongs/h/h1121.md) is yet [chay](../../strongs/h/h2416.md): I will [yālaḵ](../../strongs/h/h3212.md) and [ra'ah](../../strongs/h/h7200.md) him before I [muwth](../../strongs/h/h4191.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 44](genesis_44.md) - [Genesis 46](genesis_46.md)