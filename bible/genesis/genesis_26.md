# [Genesis 26](https://www.blueletterbible.org/kjv/gen/26/1/s_26001)

<a name="genesis_26_1"></a>Genesis 26:1

And there was a [rāʿāḇ](../../strongs/h/h7458.md) in the ['erets](../../strongs/h/h776.md), [baḏ](../../strongs/h/h905.md) the [ri'šôn](../../strongs/h/h7223.md) [rāʿāḇ](../../strongs/h/h7458.md) that was in the [yowm](../../strongs/h/h3117.md) of ['Abraham](../../strongs/h/h85.md). And [Yiṣḥāq](../../strongs/h/h3327.md) [yālaḵ](../../strongs/h/h3212.md) unto ['Ăḇîmeleḵ](../../strongs/h/h40.md) [melek](../../strongs/h/h4428.md) of the [Pᵊlištî](../../strongs/h/h6430.md) unto [gᵊrār](../../strongs/h/h1642.md).

<a name="genesis_26_2"></a>Genesis 26:2

And [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) unto him, and said, [yarad](../../strongs/h/h3381.md) not into [Mitsrayim](../../strongs/h/h4714.md); [shakan](../../strongs/h/h7931.md) in the ['erets](../../strongs/h/h776.md) which I shall ['āmar](../../strongs/h/h559.md) thee of:

<a name="genesis_26_3"></a>Genesis 26:3

[guwr](../../strongs/h/h1481.md) in this ['erets](../../strongs/h/h776.md), and I will be with thee, and will [barak](../../strongs/h/h1288.md) thee; for unto thee, and unto thy [zera'](../../strongs/h/h2233.md), I will [nathan](../../strongs/h/h5414.md) all these ['erets](../../strongs/h/h776.md), and I will [quwm](../../strongs/h/h6965.md) the [šᵊḇûʿâ](../../strongs/h/h7621.md) which I [shaba'](../../strongs/h/h7650.md) unto ['Abraham](../../strongs/h/h85.md) thy ['ab](../../strongs/h/h1.md);

<a name="genesis_26_4"></a>Genesis 26:4

And I will make thy [zera'](../../strongs/h/h2233.md) to [rabah](../../strongs/h/h7235.md) as the [kowkab](../../strongs/h/h3556.md) of [shamayim](../../strongs/h/h8064.md), and will [nathan](../../strongs/h/h5414.md) unto thy [zera'](../../strongs/h/h2233.md) all these ['erets](../../strongs/h/h776.md); and in thy [zera'](../../strongs/h/h2233.md) shall all the [gowy](../../strongs/h/h1471.md) of the ['erets](../../strongs/h/h776.md) be [barak](../../strongs/h/h1288.md);

<a name="genesis_26_5"></a>Genesis 26:5

Because that ['Abraham](../../strongs/h/h85.md) [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md), and [shamar](../../strongs/h/h8104.md) my [mišmereṯ](../../strongs/h/h4931.md), my [mitsvah](../../strongs/h/h4687.md), my [chuqqah](../../strongs/h/h2708.md), and my [towrah](../../strongs/h/h8451.md).

<a name="genesis_26_6"></a>Genesis 26:6

And [Yiṣḥāq](../../strongs/h/h3327.md) [yashab](../../strongs/h/h3427.md) in [gᵊrār](../../strongs/h/h1642.md):

<a name="genesis_26_7"></a>Genesis 26:7

And the ['enowsh](../../strongs/h/h582.md) of the [maqowm](../../strongs/h/h4725.md) [sha'al](../../strongs/h/h7592.md) him of his ['ishshah](../../strongs/h/h802.md); and he ['āmar](../../strongs/h/h559.md), She is my ['āḥôṯ](../../strongs/h/h269.md): for he [yare'](../../strongs/h/h3372.md) to ['āmar](../../strongs/h/h559.md), She is my ['ishshah](../../strongs/h/h802.md); lest, said he, the ['enowsh](../../strongs/h/h582.md) of the [maqowm](../../strongs/h/h4725.md) should [harag](../../strongs/h/h2026.md) me for [riḇqâ](../../strongs/h/h7259.md); because she was [towb](../../strongs/h/h2896.md) [mar'ê](../../strongs/h/h4758.md).

<a name="genesis_26_8"></a>Genesis 26:8

And it came to pass, when he had been there an ['arak](../../strongs/h/h748.md) [yowm](../../strongs/h/h3117.md), that ['Ăḇîmeleḵ](../../strongs/h/h40.md) [melek](../../strongs/h/h4428.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [šāqap̄](../../strongs/h/h8259.md) at a [ḥallôn](../../strongs/h/h2474.md), and [ra'ah](../../strongs/h/h7200.md), and, behold, [Yiṣḥāq](../../strongs/h/h3327.md) was [ṣāḥaq](../../strongs/h/h6711.md) with [riḇqâ](../../strongs/h/h7259.md) his ['ishshah](../../strongs/h/h802.md).

<a name="genesis_26_9"></a>Genesis 26:9

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) [qara'](../../strongs/h/h7121.md) [Yiṣḥāq](../../strongs/h/h3327.md), and said, Behold, ['aḵ](../../strongs/h/h389.md) she is thy ['ishshah](../../strongs/h/h802.md); and how saidst thou, She is my ['āḥôṯ](../../strongs/h/h269.md)? And [Yiṣḥāq](../../strongs/h/h3327.md) ['āmar](../../strongs/h/h559.md) unto him, Because I ['āmar](../../strongs/h/h559.md), Lest I [muwth](../../strongs/h/h4191.md) for her.

<a name="genesis_26_10"></a>Genesis 26:10

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) ['āmar](../../strongs/h/h559.md), What is this thou hast ['asah](../../strongs/h/h6213.md) unto us? one of the ['am](../../strongs/h/h5971.md) might [mᵊʿaṭ](../../strongs/h/h4592.md) have [shakab](../../strongs/h/h7901.md) with thy ['ishshah](../../strongs/h/h802.md), and thou shouldest have [bow'](../../strongs/h/h935.md) ['āšām](../../strongs/h/h817.md) upon us.

<a name="genesis_26_11"></a>Genesis 26:11

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) [tsavah](../../strongs/h/h6680.md) all his ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), He that [naga'](../../strongs/h/h5060.md) this ['iysh](../../strongs/h/h376.md) or his ['ishshah](../../strongs/h/h802.md) shall [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="genesis_26_12"></a>Genesis 26:12

Then [Yiṣḥāq](../../strongs/h/h3327.md) [zāraʿ](../../strongs/h/h2232.md) in that ['erets](../../strongs/h/h776.md), and [māṣā'](../../strongs/h/h4672.md) in the same [šānâ](../../strongs/h/h8141.md) an hundredfold: and [Yĕhovah](../../strongs/h/h3068.md) [barak](../../strongs/h/h1288.md) him.

<a name="genesis_26_13"></a>Genesis 26:13

And the ['iysh](../../strongs/h/h376.md) [gāḏal](../../strongs/h/h1431.md), and [yālaḵ](../../strongs/h/h3212.md) [halak](../../strongs/h/h1980.md), and [gāḏēl](../../strongs/h/h1432.md) until he became [me'od](../../strongs/h/h3966.md) [gāḏal](../../strongs/h/h1431.md):

<a name="genesis_26_14"></a>Genesis 26:14

For he had [miqnê](../../strongs/h/h4735.md) of [tso'n](../../strongs/h/h6629.md), and [miqnê](../../strongs/h/h4735.md) of [bāqār](../../strongs/h/h1241.md), and [rab](../../strongs/h/h7227.md) of [ʿăḇudâ](../../strongs/h/h5657.md): and the [Pᵊlištî](../../strongs/h/h6430.md) [qānā'](../../strongs/h/h7065.md) him.

<a name="genesis_26_15"></a>Genesis 26:15

For all the [bᵊ'ēr](../../strongs/h/h875.md) which his ['ab](../../strongs/h/h1.md) ['ebed](../../strongs/h/h5650.md) had [chaphar](../../strongs/h/h2658.md) in the [yowm](../../strongs/h/h3117.md) of ['Abraham](../../strongs/h/h85.md) his ['ab](../../strongs/h/h1.md), the [Pᵊlištî](../../strongs/h/h6430.md) had [sāṯam](../../strongs/h/h5640.md) them, and [mālā'](../../strongs/h/h4390.md) them with ['aphar](../../strongs/h/h6083.md).

<a name="genesis_26_16"></a>Genesis 26:16

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) ['āmar](../../strongs/h/h559.md) unto [Yiṣḥāq](../../strongs/h/h3327.md), [yālaḵ](../../strongs/h/h3212.md) from us; for thou art [me'od](../../strongs/h/h3966.md) [ʿāṣam](../../strongs/h/h6105.md) than we.

<a name="genesis_26_17"></a>Genesis 26:17

And [Yiṣḥāq](../../strongs/h/h3327.md) [yālaḵ](../../strongs/h/h3212.md) thence, and [ḥānâ](../../strongs/h/h2583.md) in the [nachal](../../strongs/h/h5158.md) of [gᵊrār](../../strongs/h/h1642.md), and [yashab](../../strongs/h/h3427.md) there.

<a name="genesis_26_18"></a>Genesis 26:18

And [Yiṣḥāq](../../strongs/h/h3327.md) [chaphar](../../strongs/h/h2658.md) [shuwb](../../strongs/h/h7725.md) the [bᵊ'ēr](../../strongs/h/h875.md) of [mayim](../../strongs/h/h4325.md), which they had [chaphar](../../strongs/h/h2658.md) in the [yowm](../../strongs/h/h3117.md) of ['Abraham](../../strongs/h/h85.md) his ['ab](../../strongs/h/h1.md); for the [Pᵊlištî](../../strongs/h/h6430.md) had [sāṯam](../../strongs/h/h5640.md) them ['aḥar](../../strongs/h/h310.md) the [maveth](../../strongs/h/h4194.md) of ['Abraham](../../strongs/h/h85.md): and he [qara'](../../strongs/h/h7121.md) their [shem](../../strongs/h/h8034.md) after the [shem](../../strongs/h/h8034.md) by which his ['ab](../../strongs/h/h1.md) had [qara'](../../strongs/h/h7121.md) them.

<a name="genesis_26_19"></a>Genesis 26:19

And [Yiṣḥāq](../../strongs/h/h3327.md) ['ebed](../../strongs/h/h5650.md) [chaphar](../../strongs/h/h2658.md) in the [nachal](../../strongs/h/h5158.md), and [māṣā'](../../strongs/h/h4672.md) there a [bᵊ'ēr](../../strongs/h/h875.md) of [chay](../../strongs/h/h2416.md) [mayim](../../strongs/h/h4325.md).

<a name="genesis_26_20"></a>Genesis 26:20

And the [ra'ah](../../strongs/h/h7462.md) of [gᵊrār](../../strongs/h/h1642.md) did [riyb](../../strongs/h/h7378.md) with [Yiṣḥāq](../../strongs/h/h3327.md) [ra'ah](../../strongs/h/h7462.md), ['āmar](../../strongs/h/h559.md), The [mayim](../../strongs/h/h4325.md) is ours: and he [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of the [bᵊ'ēr](../../strongs/h/h875.md) [ʿēśeq](../../strongs/h/h6230.md); because they [ʿāśaq](../../strongs/h/h6229.md) with him.

<a name="genesis_26_21"></a>Genesis 26:21

And they [chaphar](../../strongs/h/h2658.md) ['aḥēr](../../strongs/h/h312.md)[bᵊ'ēr](../../strongs/h/h875.md), and [riyb](../../strongs/h/h7378.md) for that also: and he [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of it [śiṭnâ](../../strongs/h/h7856.md).

<a name="genesis_26_22"></a>Genesis 26:22

And he ['athaq](../../strongs/h/h6275.md) from thence, and [chaphar](../../strongs/h/h2658.md) ['aḥēr](../../strongs/h/h312.md) [bᵊ'ēr](../../strongs/h/h875.md); and for that they [riyb](../../strongs/h/h7378.md) not: and he [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of it [Rᵊḥōḇôṯ](../../strongs/h/h7344.md); and he ['āmar](../../strongs/h/h559.md), For now [Yĕhovah](../../strongs/h/h3068.md) hath [rāḥaḇ](../../strongs/h/h7337.md) for us, and we shall be [parah](../../strongs/h/h6509.md) in the ['erets](../../strongs/h/h776.md).

<a name="genesis_26_23"></a>Genesis 26:23

And he [ʿālâ](../../strongs/h/h5927.md) from thence to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md).

<a name="genesis_26_24"></a>Genesis 26:24

And [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) unto him the same [layil](../../strongs/h/h3915.md), and ['āmar](../../strongs/h/h559.md), I am the ['Elohiym](../../strongs/h/h430.md) of ['Abraham](../../strongs/h/h85.md) thy ['ab](../../strongs/h/h1.md): [yare'](../../strongs/h/h3372.md) not, for I am with thee, and will [barak](../../strongs/h/h1288.md) thee, and [rabah](../../strongs/h/h7235.md) thy [zera'](../../strongs/h/h2233.md) for my ['ebed](../../strongs/h/h5650.md) ['Abraham](../../strongs/h/h85.md) sake.

<a name="genesis_26_25"></a>Genesis 26:25

And he [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) there, and [qara'](../../strongs/h/h7121.md) upon the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), and [natah](../../strongs/h/h5186.md) his ['ohel](../../strongs/h/h168.md) there: and there [Yiṣḥāq](../../strongs/h/h3327.md) ['ebed](../../strongs/h/h5650.md) [karah](../../strongs/h/h3738.md) a [bᵊ'ēr](../../strongs/h/h875.md).

<a name="genesis_26_26"></a>Genesis 26:26

Then ['Ăḇîmeleḵ](../../strongs/h/h40.md) [halak](../../strongs/h/h1980.md) to him from [gᵊrār](../../strongs/h/h1642.md), and ['ăḥuzzaṯ](../../strongs/h/h276.md) one of his [mērēaʿ](../../strongs/h/h4828.md), and [pîḵōl](../../strongs/h/h6369.md) the [śar](../../strongs/h/h8269.md) of his [tsaba'](../../strongs/h/h6635.md).

<a name="genesis_26_27"></a>Genesis 26:27

And [Yiṣḥāq](../../strongs/h/h3327.md) ['āmar](../../strongs/h/h559.md) unto them, Wherefore [bow'](../../strongs/h/h935.md) ye to me, seeing ye [sane'](../../strongs/h/h8130.md) me, and have [shalach](../../strongs/h/h7971.md) me away from you?

<a name="genesis_26_28"></a>Genesis 26:28

And they said, We [ra'ah](../../strongs/h/h7200.md) [ra'ah](../../strongs/h/h7200.md) that [Yĕhovah](../../strongs/h/h3068.md) was with thee: and we ['āmar](../../strongs/h/h559.md), Let there be now an ['alah](../../strongs/h/h423.md) betwixt us, even betwixt us and thee, and let us [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with thee;

<a name="genesis_26_29"></a>Genesis 26:29

That thou wilt ['asah](../../strongs/h/h6213.md) us no [ra'](../../strongs/h/h7451.md), as we have not [naga'](../../strongs/h/h5060.md) thee, and as we have ['asah](../../strongs/h/h6213.md) unto thee nothing but [towb](../../strongs/h/h2896.md), and have sent thee [shalach](../../strongs/h/h7971.md) in [shalowm](../../strongs/h/h7965.md): thou art [ʿatâ](../../strongs/h/h6258.md) the [barak](../../strongs/h/h1288.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="genesis_26_30"></a>Genesis 26:30

And he ['asah](../../strongs/h/h6213.md) them a [mištê](../../strongs/h/h4960.md), and they did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md).

<a name="genesis_26_31"></a>Genesis 26:31

And they [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [shaba'](../../strongs/h/h7650.md) ['iysh](../../strongs/h/h376.md) to ['ach](../../strongs/h/h251.md): and [Yiṣḥāq](../../strongs/h/h3327.md) [shalach](../../strongs/h/h7971.md) them away, and they [yālaḵ](../../strongs/h/h3212.md) from him in [shalowm](../../strongs/h/h7965.md).

<a name="genesis_26_32"></a>Genesis 26:32

And it came to pass the same [yowm](../../strongs/h/h3117.md), that [Yiṣḥāq](../../strongs/h/h3327.md) ['ebed](../../strongs/h/h5650.md) [bow'](../../strongs/h/h935.md), and [nāḡaḏ](../../strongs/h/h5046.md) him concerning the [bᵊ'ēr](../../strongs/h/h875.md) which they had [chaphar](../../strongs/h/h2658.md), and ['āmar](../../strongs/h/h559.md) unto him, We have [māṣā'](../../strongs/h/h4672.md) [mayim](../../strongs/h/h4325.md).

<a name="genesis_26_33"></a>Genesis 26:33

And he [qara'](../../strongs/h/h7121.md) it [šiḇʿâ](../../strongs/h/h7656.md): therefore the [shem](../../strongs/h/h8034.md) of the [ʿîr](../../strongs/h/h5892.md) is [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="genesis_26_34"></a>Genesis 26:34

And [ʿĒśāv](../../strongs/h/h6215.md) was forty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he [laqach](../../strongs/h/h3947.md) to ['ishshah](../../strongs/h/h802.md) [yᵊhûḏîṯ](../../strongs/h/h3067.md) the [bath](../../strongs/h/h1323.md) of [Bᵊ'ērî](../../strongs/h/h882.md) the [Ḥitî](../../strongs/h/h2850.md), and [bāśmaṯ](../../strongs/h/h1315.md) the [bath](../../strongs/h/h1323.md) of ['êlôn](../../strongs/h/h356.md) the [Ḥitî](../../strongs/h/h2850.md):

<a name="genesis_26_35"></a>Genesis 26:35

Which were a [mōrâ](../../strongs/h/h4786.md) of [ruwach](../../strongs/h/h7307.md) unto [Yiṣḥāq](../../strongs/h/h3327.md) and to [riḇqâ](../../strongs/h/h7259.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 25](genesis_25.md) - [Genesis 27](genesis_27.md)