# [Genesis 14](https://www.blueletterbible.org/lxx/genesis/14)

<a name="genesis_14_1"></a>Genesis 14:1

And [ginomai](../../strongs/g/g1096.md) in the [basileia](../../strongs/g/g932.md) of Amraphel [basileus](../../strongs/g/g935.md) of Shinar, and Arioch [basileus](../../strongs/g/g935.md) of Ellasar, and Chedorlaomer [basileus](../../strongs/g/g935.md) of Elam, and Tidal [basileus](../../strongs/g/g935.md) of [ethnos](../../strongs/g/g1484.md), 

<a name="genesis_14_2"></a>Genesis 14:2

they [poieō](../../strongs/g/g4160.md) [polemos](../../strongs/g/g4171.md) with Bera [basileus](../../strongs/g/g935.md) of [Sodoma](../../strongs/g/g4670.md), and with Birsha [basileus](../../strongs/g/g935.md) of [Gomorra](../../strongs/g/g1116.md), and with Shinab [basileus](../../strongs/g/g935.md) of Admah, and Shemeber [basileus](../../strongs/g/g935.md) of Zeboiim, and [basileus](../../strongs/g/g935.md) [Balak](../../strongs/g/g904.md) -- this is Zoar.

<a name="genesis_14_3"></a>Genesis 14:3

All these [symphōneō](../../strongs/g/g4856.md) upon the [pharagx](../../strongs/g/g5327.md) [halykos](../../strongs/g/g252.md), this being the [thalassa](../../strongs/g/g2281.md) of [hals](../../strongs/g/g251.md). 

<a name="genesis_14_4"></a>Genesis 14:4

[dōdeka](../../strongs/g/g1427.md) [etos](../../strongs/g/g2094.md) they [douleuō](../../strongs/g/g1398.md) to Chedorlaomer, but in the thirteenth (triskaidekatō) [etos](../../strongs/g/g2094.md) they [aphistēmi](../../strongs/g/g868.md). 

<a name="genesis_14_5"></a>Genesis 14:5

And in the [tessareskaidekatos](../../strongs/g/g5065.md) [etos](../../strongs/g/g2094.md) Chedorlaomer [erchomai](../../strongs/g/g2064.md) and the [basileus](../../strongs/g/g935.md) with him. And they [katakoptō](../../strongs/g/g2629.md) the giants (gigantas), of the ones in Astaroth Karnaim, and [ethnos](../../strongs/g/g1484.md) the [ischyros](../../strongs/g/g2478.md) together with them, and the Emim in Shaveh the [polis](../../strongs/g/g4172.md), 

<a name="genesis_14_6"></a>Genesis 14:6

and the Horites, the ones in the [oros](../../strongs/g/g3735.md) of Seir, unto the terebinth tree (tereminthou) of El-paran, which is in the [erēmos](../../strongs/g/g2048.md). 

<a name="genesis_14_7"></a>Genesis 14:7

And [anastrephō](../../strongs/g/g390.md), they [erchomai](../../strongs/g/g2064.md) upon the [pēgē](../../strongs/g/g4077.md) of [krisis](../../strongs/g/g2920.md), this is Kadesh. And they [katakoptō](../../strongs/g/g2629.md) all the [archōn](../../strongs/g/g758.md) of Amalek, and the Amorites, of the ones [katoikeō](../../strongs/g/g2730.md) in Hazezon-tamar.

<a name="genesis_14_8"></a>Genesis 14:8

And [exerchomai](../../strongs/g/g1831.md) the [basileus](../../strongs/g/g935.md) of [Sodoma](../../strongs/g/g4670.md), and the [basileus](../../strongs/g/g935.md) of [Gomorra](../../strongs/g/g1116.md), and the [basileus](../../strongs/g/g935.md) of Admah, and the [basileus](../../strongs/g/g935.md) of Zeboiim, and the [basileus](../../strongs/g/g935.md) of Bela -- this is Zoar. And they deployed (paretaxanto) themselves for [polemos](../../strongs/g/g4171.md) in the valley (koiladi) [halykos](../../strongs/g/g252.md)

<a name="genesis_14_9"></a>Genesis 14:9

against Chedorlaomer [basileus](../../strongs/g/g935.md) of Elam, and Tidal [basileus](../../strongs/g/g935.md) of [ethnos](../../strongs/g/g1484.md), and Amraphel [basileus](../../strongs/g/g935.md) of Shinar, and Arioch [basileus](../../strongs/g/g935.md) of Ellasar -- the [tessares](../../strongs/g/g5064.md) [basileus](../../strongs/g/g935.md) against the [pente](../../strongs/g/g4002.md). 

<a name="genesis_14_10"></a>Genesis 14:10

And the valley (koilas) [halykos](../../strongs/g/g252.md) had [phrear](../../strongs/g/g5421.md) of asphalt (asphaltou), [pheugō](../../strongs/g/g5343.md) and the [basileus](../../strongs/g/g935.md) of [Sodoma](../../strongs/g/g4670.md), and the [basileus](../../strongs/g/g935.md) of [Gomorra](../../strongs/g/g1116.md), and they [empiptō](../../strongs/g/g1706.md) there; and the ones being [kataleipō](../../strongs/g/g2641.md) [pheugō](../../strongs/g/g5343.md) into the [oreinos](../../strongs/g/g3714.md). 

<a name="genesis_14_11"></a>Genesis 14:11

And they [lambanō](../../strongs/g/g2983.md) the [hippos](../../strongs/g/g2462.md) all of [Sodoma](../../strongs/g/g4670.md) and [Gomorra](../../strongs/g/g1116.md), and all their [brōma](../../strongs/g/g1033.md), and they [aperchomai](../../strongs/g/g565.md). 

<a name="genesis_14_12"></a>Genesis 14:12

And they [lambanō](../../strongs/g/g2983.md) also [Lōt](../../strongs/g/g3091.md), the [huios](../../strongs/g/g5207.md) of the [adelphos](../../strongs/g/g80.md) of Abram, and his belongings (aposkeuēn), and moved (apokhonto) -- for he was [katoikeō](../../strongs/g/g2730.md) in [Sodoma](../../strongs/g/g4670.md).

<a name="genesis_14_13"></a>Genesis 14:13

[paraginomai](../../strongs/g/g3854.md) of the ones being rescued (anasōthentōn) [tis](../../strongs/g/g5100.md), [apaggellō](../../strongs/g/g518.md) to Abram, to the traveler (peratē). And he [katoikeō](../../strongs/g/g2730.md) at the oak (drui) of Mamre (Mambrē) the Amorite (Amoris) of the [adelphos](../../strongs/g/g80.md) of Eshcol, and the [adelphos](../../strongs/g/g80.md) of Aner, who were confederates (synōmotai) of Abram.

<a name="genesis_14_14"></a>Genesis 14:14

[akouō](../../strongs/g/g191.md) Abram that has been [aichmalōteuō](../../strongs/g/g162.md) [Lōt](../../strongs/g/g3091.md) the [huios](../../strongs/g/g5207.md) of his [adelphos](../../strongs/g/g80.md), [arithmeō](../../strongs/g/g705.md) own native-born (oikogeneis) his, [triakosioi](../../strongs/g/g5145.md) [deka](../../strongs/g/g1176.md) and [oktō](../../strongs/g/g3638.md). And he [katadikazō](../../strongs/g/g2613.md) unto Dan.

<a name="genesis_14_15"></a>Genesis 14:15

And he [epipiptō](../../strongs/g/g1968.md) upon them at [nyx](../../strongs/g/g3571.md), he and his [pais](../../strongs/g/g3816.md). And he [patassō](../../strongs/g/g3960.md) them, and [katadiōkō](../../strongs/g/g2614.md) them unto Hobah, which is at the [aristeros](../../strongs/g/g710.md) of [Damaskos](../../strongs/g/g1154.md).

<a name="genesis_14_16"></a>Genesis 14:16

And he [apostrephō](../../strongs/g/g654.md) all the [hippos](../../strongs/g/g2462.md) of [Sodoma](../../strongs/g/g4670.md); and [Lōt](../../strongs/g/g3091.md) the [huios](../../strongs/g/g5207.md) of his [adelphos](../../strongs/g/g80.md) he [apostrephō](../../strongs/g/g654.md), and all his [hyparchonta](../../strongs/g/g5224.md), and the [gynē](../../strongs/g/g1135.md), and the [laos](../../strongs/g/g2992.md). 

<a name="genesis_14_17"></a>Genesis 14:17

[exerchomai](../../strongs/g/g1831.md) the [basileus](../../strongs/g/g935.md) of [Sodoma](../../strongs/g/g4670.md) to [synantēsis](../../strongs/g/g4877.md) him, after his [apostrephō](../../strongs/g/g654.md) from the [kopē](../../strongs/g/g2871.md) of Chedorlaomer, and the [basileus](../../strongs/g/g935.md) with him, into the valley (koilada) of Shaveh -- this was the plain (pedion) of the [basileus](../../strongs/g/g935.md). 

<a name="genesis_14_18"></a>Genesis 14:18

And [Melchisedek](../../strongs/g/g3198.md) [basileus](../../strongs/g/g935.md) of [Salēm](../../strongs/g/g4532.md) [ekpherō](../../strongs/g/g1627.md) [artos](../../strongs/g/g740.md) and [oinos](../../strongs/g/g3631.md). And he was [hiereus](../../strongs/g/g2409.md) of [theos](../../strongs/g/g2316.md) the [hypsistos](../../strongs/g/g5310.md). 

<a name="genesis_14_19"></a>Genesis 14:19

And he [eulogeō](../../strongs/g/g2127.md) him. And he [eipon](../../strongs/g/g2036.md), Abram, a [eulogeō](../../strongs/g/g2127.md) to [theos](../../strongs/g/g2316.md) the [hypsistos](../../strongs/g/g5310.md), who [ktizō](../../strongs/g/g2936.md) the [ouranos](../../strongs/g/g3772.md) and the [gē](../../strongs/g/g1093.md). 

<a name="genesis_14_20"></a>Genesis 14:20

And [eulogētos](../../strongs/g/g2128.md) [theos](../../strongs/g/g2316.md) the [hypsistos](../../strongs/g/g5310.md) who [paradidōmi](../../strongs/g/g3860.md) your [echthros](../../strongs/g/g2190.md) under your hands (hypocheirious) to you. And [didōmi](../../strongs/g/g1325.md) to him Abram a [dekatē](../../strongs/g/g1181.md) from all. 

<a name="genesis_14_21"></a>Genesis 14:21

And [eipon](../../strongs/g/g2036.md) the [basileus](../../strongs/g/g935.md) of [Sodoma](../../strongs/g/g4670.md) to Abram, [didōmi](../../strongs/g/g1325.md) to me the [anēr](../../strongs/g/g435.md), but the [hippos](../../strongs/g/g2462.md) you [lambanō](../../strongs/g/g2983.md) to yourself! 

<a name="genesis_14_22"></a>Genesis 14:22

[eipon](../../strongs/g/g2036.md) Abram to the [basileus](../../strongs/g/g935.md) of [Sodoma](../../strongs/g/g4670.md), I will [ekteinō](../../strongs/g/g1614.md) my [cheir](../../strongs/g/g5495.md) to the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) the [hypsistos](../../strongs/g/g5310.md), who [ktizō](../../strongs/g/g2936.md) the [ouranos](../../strongs/g/g3772.md) and the [gē](../../strongs/g/g1093.md), 

<a name="genesis_14_23"></a>Genesis 14:23

that not from the string (spartiou) unto the knob (sphairōtēros) of the [hypodēma](../../strongs/g/g5266.md) will I [lambanō](../../strongs/g/g2983.md) for myself from all of your things , that you should not have [eipon](../../strongs/g/g2036.md) that, I [ploutizō](../../strongs/g/g4148.md) Abram;

<a name="genesis_14_24"></a>Genesis 14:24

except what [esthiō](../../strongs/g/g2068.md) the [neaniskos](../../strongs/g/g3495.md) and the [meris](../../strongs/g/g3310.md) of the [anēr](../../strongs/g/g435.md), of the ones [symporeuomai](../../strongs/g/g4848.md) with me, Eshcol, Aner, Mamre; these will [lambanō](../../strongs/g/g2983.md) a [meris](../../strongs/g/g3310.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 13](genesis_lxx_13.md) - [Genesis 15](genesis_lxx_15.md)