# [Genesis 33](https://www.blueletterbible.org/kjv/gen/33/1/s_33001)

<a name="genesis_33_1"></a>Genesis 33:1

And [Ya'aqob](../../strongs/h/h3290.md) [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md), and, behold, [ʿĒśāv](../../strongs/h/h6215.md) [bow'](../../strongs/h/h935.md), and with him four hundred ['iysh](../../strongs/h/h376.md). And he [ḥāṣâ](../../strongs/h/h2673.md) the [yeleḏ](../../strongs/h/h3206.md) unto [Lē'â](../../strongs/h/h3812.md), and unto [Rāḥēl](../../strongs/h/h7354.md), and unto the two [šip̄ḥâ](../../strongs/h/h8198.md).

<a name="genesis_33_2"></a>Genesis 33:2

And he [śûm](../../strongs/h/h7760.md) the [šip̄ḥâ](../../strongs/h/h8198.md) and their [yeleḏ](../../strongs/h/h3206.md) [ri'šôn](../../strongs/h/h7223.md), and [Lē'â](../../strongs/h/h3812.md) and her [yeleḏ](../../strongs/h/h3206.md) ['aḥărôn](../../strongs/h/h314.md), and [Rāḥēl](../../strongs/h/h7354.md) and [Yôsēp̄](../../strongs/h/h3130.md) ['aḥărôn](../../strongs/h/h314.md).

<a name="genesis_33_3"></a>Genesis 33:3

And he ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) them, and [shachah](../../strongs/h/h7812.md) himself to the ['erets](../../strongs/h/h776.md) seven [pa'am](../../strongs/h/h6471.md), until he [nāḡaš](../../strongs/h/h5066.md) to his ['ach](../../strongs/h/h251.md).

<a name="genesis_33_4"></a>Genesis 33:4

And [ʿĒśāv](../../strongs/h/h6215.md) [rûṣ](../../strongs/h/h7323.md) to [qārā'](../../strongs/h/h7125.md) him, and [ḥāḇaq](../../strongs/h/h2263.md) him, and [naphal](../../strongs/h/h5307.md) on his [ṣaûā'r](../../strongs/h/h6677.md), and [nashaq](../../strongs/h/h5401.md) him: and they [bāḵâ](../../strongs/h/h1058.md).

<a name="genesis_33_5"></a>Genesis 33:5

And he [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md) the ['ishshah](../../strongs/h/h802.md) and the [yeleḏ](../../strongs/h/h3206.md); and ['āmar](../../strongs/h/h559.md), Who are those with thee? And he ['āmar](../../strongs/h/h559.md), The [yeleḏ](../../strongs/h/h3206.md) which ['Elohiym](../../strongs/h/h430.md) hath [chanan](../../strongs/h/h2603.md) thy ['ebed](../../strongs/h/h5650.md).

<a name="genesis_33_6"></a>Genesis 33:6

Then the [šip̄ḥâ](../../strongs/h/h8198.md) [nāḡaš](../../strongs/h/h5066.md), they and their [yeleḏ](../../strongs/h/h3206.md), and they [shachah](../../strongs/h/h7812.md).

<a name="genesis_33_7"></a>Genesis 33:7

And [Lē'â](../../strongs/h/h3812.md) also with her [yeleḏ](../../strongs/h/h3206.md) [nāḡaš](../../strongs/h/h5066.md), and [shachah](../../strongs/h/h7812.md): and ['aḥar](../../strongs/h/h310.md) [nāḡaš](../../strongs/h/h5066.md) [Yôsēp̄](../../strongs/h/h3130.md) and [Rāḥēl](../../strongs/h/h7354.md), and they [shachah](../../strongs/h/h7812.md).

<a name="genesis_33_8"></a>Genesis 33:8

And he ['āmar](../../strongs/h/h559.md), What meanest thou by all this [maḥănê](../../strongs/h/h4264.md) which I [pāḡaš](../../strongs/h/h6298.md)? And he ['āmar](../../strongs/h/h559.md), These are to [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in the ['ayin](../../strongs/h/h5869.md) of my ['adown](../../strongs/h/h113.md).

<a name="genesis_33_9"></a>Genesis 33:9

And [ʿĒśāv](../../strongs/h/h6215.md) ['āmar](../../strongs/h/h559.md), I have [rab](../../strongs/h/h7227.md), my ['ach](../../strongs/h/h251.md); keep that thou hast unto thyself.

<a name="genesis_33_10"></a>Genesis 33:10

And [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md), Nay, I pray thee, if now I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md), then [laqach](../../strongs/h/h3947.md) my [minchah](../../strongs/h/h4503.md) at my [yad](../../strongs/h/h3027.md): for therefore I have [ra'ah](../../strongs/h/h7200.md) thy [paniym](../../strongs/h/h6440.md), as though I had [ra'ah](../../strongs/h/h7200.md) the [paniym](../../strongs/h/h6440.md) of ['Elohiym](../../strongs/h/h430.md), and thou wast [ratsah](../../strongs/h/h7521.md) with me.

<a name="genesis_33_11"></a>Genesis 33:11

[laqach](../../strongs/h/h3947.md), I pray thee, my [bĕrakah](../../strongs/h/h1293.md) that is [bow'](../../strongs/h/h935.md) to thee; because ['Elohiym](../../strongs/h/h430.md) hath [chanan](../../strongs/h/h2603.md) with me, and because I have enough. And he [pāṣar](../../strongs/h/h6484.md) him, and he [laqach](../../strongs/h/h3947.md) it.

<a name="genesis_33_12"></a>Genesis 33:12

And he ['āmar](../../strongs/h/h559.md), Let us take our [nāsaʿ](../../strongs/h/h5265.md), and let us [yālaḵ](../../strongs/h/h3212.md), and I will [yālaḵ](../../strongs/h/h3212.md) before thee.

<a name="genesis_33_13"></a>Genesis 33:13

And he ['āmar](../../strongs/h/h559.md) unto him, My ['adown](../../strongs/h/h113.md) [yada'](../../strongs/h/h3045.md) that the [yeleḏ](../../strongs/h/h3206.md) are [raḵ](../../strongs/h/h7390.md), and the [tso'n](../../strongs/h/h6629.md) and [bāqār](../../strongs/h/h1241.md) with [ʿûl](../../strongs/h/h5763.md) are with me: and if men should [dāp̄aq](../../strongs/h/h1849.md) them one [yowm](../../strongs/h/h3117.md), all the [tso'n](../../strongs/h/h6629.md) will [muwth](../../strongs/h/h4191.md).

<a name="genesis_33_14"></a>Genesis 33:14

Let my ['adown](../../strongs/h/h113.md), I pray thee, ['abar](../../strongs/h/h5674.md) before his ['ebed](../../strongs/h/h5650.md): and I will [nāhal](../../strongs/h/h5095.md) ['aṭ](../../strongs/h/h328.md), according as the [mĕla'kah](../../strongs/h/h4399.md) that goeth [paniym](../../strongs/h/h6440.md) me and the [yeleḏ](../../strongs/h/h3206.md) be able to [regel](../../strongs/h/h7272.md), until I [bow'](../../strongs/h/h935.md) unto my ['adown](../../strongs/h/h113.md) unto [Śēʿîr](../../strongs/h/h8165.md).

<a name="genesis_33_15"></a>Genesis 33:15

And [ʿĒśāv](../../strongs/h/h6215.md) ['āmar](../../strongs/h/h559.md), Let me now [yāṣaḡ](../../strongs/h/h3322.md) with thee some of the ['am](../../strongs/h/h5971.md) that are with me. And he ['āmar](../../strongs/h/h559.md), What needeth it? let me [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in the ['ayin](../../strongs/h/h5869.md) of my ['adown](../../strongs/h/h113.md).

<a name="genesis_33_16"></a>Genesis 33:16

So [ʿĒśāv](../../strongs/h/h6215.md) [shuwb](../../strongs/h/h7725.md) that [yowm](../../strongs/h/h3117.md) on his [derek](../../strongs/h/h1870.md) unto [Śēʿîr](../../strongs/h/h8165.md).

<a name="genesis_33_17"></a>Genesis 33:17

And [Ya'aqob](../../strongs/h/h3290.md) [nāsaʿ](../../strongs/h/h5265.md) to [Sukôṯ](../../strongs/h/h5523.md), and [bānâ](../../strongs/h/h1129.md) him a [bayith](../../strongs/h/h1004.md), and ['asah](../../strongs/h/h6213.md) [cukkah](../../strongs/h/h5521.md) for his [miqnê](../../strongs/h/h4735.md): therefore the [shem](../../strongs/h/h8034.md) of the [maqowm](../../strongs/h/h4725.md) is [qara'](../../strongs/h/h7121.md) [Sukôṯ](../../strongs/h/h5523.md).

<a name="genesis_33_18"></a>Genesis 33:18

And [Ya'aqob](../../strongs/h/h3290.md) [bow'](../../strongs/h/h935.md) to [šālēm](../../strongs/h/h8004.md), an [ʿîr](../../strongs/h/h5892.md) of [Šᵊḵem](../../strongs/h/h7927.md), which is in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), when he [bow'](../../strongs/h/h935.md) from [padān](../../strongs/h/h6307.md); and [ḥānâ](../../strongs/h/h2583.md) [paniym](../../strongs/h/h6440.md) the [ʿîr](../../strongs/h/h5892.md).

<a name="genesis_33_19"></a>Genesis 33:19

And he [qānâ](../../strongs/h/h7069.md) a [ḥelqâ](../../strongs/h/h2513.md) of a [sadeh](../../strongs/h/h7704.md), where he had [natah](../../strongs/h/h5186.md) his ['ohel](../../strongs/h/h168.md), at the [yad](../../strongs/h/h3027.md) of the [ben](../../strongs/h/h1121.md) of [ḥămôr](../../strongs/h/h2544.md), [Šᵊḵem](../../strongs/h/h7927.md) ['ab](../../strongs/h/h1.md), for an hundred [qᵊśîṭâ](../../strongs/h/h7192.md).

<a name="genesis_33_20"></a>Genesis 33:20

And he [nāṣaḇ](../../strongs/h/h5324.md) there a [mizbeach](../../strongs/h/h4196.md), and [qara'](../../strongs/h/h7121.md) it ['l 'lhy yשr'l](../../strongs/h/h415.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 32](genesis_32.md) - [Genesis 34](genesis_34.md)