# [Genesis 13](https://www.blueletterbible.org/lxx/genesis/13)

<a name="genesis_13_1"></a>Genesis 13:1

[anabainō](../../strongs/g/g305.md) Abram from out of [Aigyptos](../../strongs/g/g125.md), he, and his [gynē](../../strongs/g/g1135.md), and all of his, and [Lōt](../../strongs/g/g3091.md) with him, into the [erēmos](../../strongs/g/g2048.md). 

<a name="genesis_13_2"></a>Genesis 13:2

And Abram was [plousios](../../strongs/g/g4145.md) [sphodra](../../strongs/g/g4970.md) in [ktēnos](../../strongs/g/g2934.md), and [argyrion](../../strongs/g/g694.md), and [chrysion](../../strongs/g/g5553.md). 

<a name="genesis_13_3"></a>Genesis 13:3

And he [poreuō](../../strongs/g/g4198.md) from where he [erchomai](../../strongs/g/g2064.md) into the [erēmos](../../strongs/g/g2048.md), unto Beth-el, unto the [topos](../../strongs/g/g5117.md) where was his [skēnē](../../strongs/g/g4633.md) [proteros](../../strongs/g/g4387.md), between Beth-el and between Hai;

<a name="genesis_13_4"></a>Genesis 13:4

unto the [topos](../../strongs/g/g5117.md) of the [thysiastērion](../../strongs/g/g2379.md) which he [poieō](../../strongs/g/g4160.md) there at the [archē](../../strongs/g/g746.md). And [epikaleō](../../strongs/g/g1941.md) there Abram the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_13_5"></a>Genesis 13:5

And to [Lōt](../../strongs/g/g3091.md), the one [symporeuomai](../../strongs/g/g4848.md) with Abram, was [probaton](../../strongs/g/g4263.md), and [bous](../../strongs/g/g1016.md), and [ktēnos](../../strongs/g/g2934.md). 

<a name="genesis_13_6"></a>Genesis 13:6

And did not have [chōreō](../../strongs/g/g5562.md) for them the [gē](../../strongs/g/g1093.md) to [katoikeō](../../strongs/g/g2730.md) together, for were their [hyparchonta](../../strongs/g/g5224.md) [polys](../../strongs/g/g4183.md), and they were not able to [katoikeō](../../strongs/g/g2730.md) together. 

<a name="genesis_13_7"></a>Genesis 13:7

And there was a [machē](../../strongs/g/g3163.md) between the [poimēn](../../strongs/g/g4166.md) of the [ktēnos](../../strongs/g/g2934.md) of Abram, and between the [poimēn](../../strongs/g/g4166.md) of the [ktēnos](../../strongs/g/g2934.md) of [Lōt](../../strongs/g/g3091.md). And the [Chananaios](../../strongs/g/g5478.md) and the Perizzites (Pherezaioi) then [katoikeō](../../strongs/g/g2730.md) the [gē](../../strongs/g/g1093.md). 

<a name="genesis_13_8"></a>Genesis 13:8

[eipon](../../strongs/g/g2036.md) Abram to [Lōt](../../strongs/g/g3091.md), Do not let there be a [machē](../../strongs/g/g3163.md) between me and you, and between your [poimēn](../../strongs/g/g4166.md) and between my [poimēn](../../strongs/g/g4166.md)! for [anthrōpos](../../strongs/g/g444.md) [adelphos](../../strongs/g/g80.md) are we. 

<a name="genesis_13_9"></a>Genesis 13:9

not [idou](../../strongs/g/g2400.md) all the [gē](../../strongs/g/g1093.md) [enantion](../../strongs/g/g1726.md) you is? [diachōrizō](../../strongs/g/g1316.md) from me! If you go to the [aristeros](../../strongs/g/g710.md), I will go to the [dexios](../../strongs/g/g1188.md); but if you go to the [dexios](../../strongs/g/g1188.md), I will go to the [aristeros](../../strongs/g/g710.md). 

<a name="genesis_13_10"></a>Genesis 13:10

And [Lōt](../../strongs/g/g3091.md), [epairō](../../strongs/g/g1869.md) his [ophthalmos](../../strongs/g/g3788.md), [epeidon](../../strongs/g/g1896.md) all the [perichōros](../../strongs/g/g4066.md) the [Iordanēs](../../strongs/g/g2446.md), that all was [potizō](../../strongs/g/g4222.md) before the [katastrephō](../../strongs/g/g2690.md) by [theos](../../strongs/g/g2316.md) [Sodoma](../../strongs/g/g4670.md) and [Gomorra](../../strongs/g/g1116.md) as the [paradeisos](../../strongs/g/g3857.md) of [theos](../../strongs/g/g2316.md), and as the [gē](../../strongs/g/g1093.md) of [Aigyptos](../../strongs/g/g125.md) unto [erchomai](../../strongs/g/g2064.md) into Zoar.

<a name="genesis_13_11"></a>Genesis 13:11

And Lot [eklegomai](../../strongs/g/g1586.md) to himself all the [perichōros](../../strongs/g/g4066.md) the [Iordanēs](../../strongs/g/g2446.md). And [Lōt](../../strongs/g/g3091.md) [apairō](../../strongs/g/g522.md) from the [anatolē](../../strongs/g/g395.md). And they were [diachōrizō](../../strongs/g/g1316.md), each from his [adelphos](../../strongs/g/g80.md). 

<a name="genesis_13_12"></a>Genesis 13:12

And Abram [katoikeō](../../strongs/g/g2730.md) in the [gē](../../strongs/g/g1093.md) of [Chanaan](../../strongs/g/g5477.md), and [Lōt](../../strongs/g/g3091.md) [katoikeō](../../strongs/g/g2730.md) in a [polis](../../strongs/g/g4172.md) of the [perichōros](../../strongs/g/g4066.md), and he [skēnoō](../../strongs/g/g4637.md) in [Sodoma](../../strongs/g/g4670.md).

<a name="genesis_13_13"></a>Genesis 13:13

But the [anthrōpos](../../strongs/g/g444.md) in [Sodoma](../../strongs/g/g4670.md) were [ponēros](../../strongs/g/g4190.md) and [hamartōlos](../../strongs/g/g268.md) [enantion](../../strongs/g/g1726.md) [theos](../../strongs/g/g2316.md), [sphodra](../../strongs/g/g4970.md). 

<a name="genesis_13_14"></a>Genesis 13:14

 And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md) to Abram, after [Lōt](../../strongs/g/g3091.md) [diachōrizō](../../strongs/g/g1316.md) from him, [anablepō](../../strongs/g/g308.md) with your [ophthalmos](../../strongs/g/g3788.md), and [eidō](../../strongs/g/g1492.md) from the [topos](../../strongs/g/g5117.md) which now you are, to the [borras](../../strongs/g/g1005.md) and [lips](../../strongs/g/g3047.md), and [anatolē](../../strongs/g/g395.md), and [thalassa](../../strongs/g/g2281.md)! 

<a name="genesis_13_15"></a>Genesis 13:15

That all the [gē](../../strongs/g/g1093.md) which you [horaō](../../strongs/g/g3708.md), to you I will [didōmi](../../strongs/g/g1325.md) it, and to your [sperma](../../strongs/g/g4690.md), unto the [aiōn](../../strongs/g/g165.md). 

<a name="genesis_13_16"></a>Genesis 13:16

And I will [poieō](../../strongs/g/g4160.md) your [sperma](../../strongs/g/g4690.md) as the [ammos](../../strongs/g/g285.md) of the [gē](../../strongs/g/g1093.md). If is able anyone to count out (exarithmēsai) the [ammos](../../strongs/g/g285.md) of the [gē](../../strongs/g/g1093.md), then your [sperma](../../strongs/g/g4690.md) they shall count out (exarithmēthēsetai).

<a name="genesis_13_17"></a>Genesis 13:17

In [anistēmi](../../strongs/g/g450.md), you [diodeuō](../../strongs/g/g1353.md) the [gē](../../strongs/g/g1093.md) unto both the [mēkos](../../strongs/g/g3372.md) of it and unto the [platos](../../strongs/g/g4114.md); for to you I shall [didōmi](../../strongs/g/g1325.md) it, and to your [sperma](../../strongs/g/g4690.md), into the [aiōn](../../strongs/g/g165.md). 

<a name="genesis_13_18"></a>Genesis 13:18

And moving his tent (aposkēnōsas), Abram [erchomai](../../strongs/g/g2064.md) [katoikeō](../../strongs/g/g2730.md) around the oak (dryn) Mamre, which was in Hebron. And he [oikodomeō](../../strongs/g/g3618.md) there an [thysiastērion](../../strongs/g/g2379.md) to the [kyrios](../../strongs/g/g2962.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 12](genesis_lxx_12.md) - [Genesis 14](genesis_lxx_14.md)