# [Genesis 9](https://www.blueletterbible.org/lxx/genesis/9)

<a name="genesis_9_1"></a>Genesis 9:1

And [theos](../../strongs/g/g2316.md) [eulogeō](../../strongs/g/g2127.md) [Nōe](../../strongs/g/g3575.md), and his [huios](../../strongs/g/g5207.md), and [eipon](../../strongs/g/g2036.md) to them, [auxanō](../../strongs/g/g837.md) and [plēthynō](../../strongs/g/g4129.md), and [plēroō](../../strongs/g/g4137.md) the [gē](../../strongs/g/g1093.md), and [katakyrieuō](../../strongs/g/g2634.md) it! 

<a name="genesis_9_2"></a>Genesis 9:2

And the [phobos](../../strongs/g/g5401.md) of you and [tromos](../../strongs/g/g5156.md) will be upon all the [thērion](../../strongs/g/g2342.md) of the [gē](../../strongs/g/g1093.md), upon all the [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md), and upon all the things [kineō](../../strongs/g/g2795.md) upon the [gē](../../strongs/g/g1093.md), and upon all the [ichthys](../../strongs/g/g2486.md) of the [thalassa](../../strongs/g/g2281.md). Under your [cheir](../../strongs/g/g5495.md) I have [didōmi](../../strongs/g/g1325.md) them to you.

<a name="genesis_9_3"></a>Genesis 9:3

And every [herpeton](../../strongs/g/g2062.md) which is [zaō](../../strongs/g/g2198.md) shall be to you for [brōsis](../../strongs/g/g1035.md); as [lachanon](../../strongs/g/g3001.md) of [chortos](../../strongs/g/g5528.md) I have [didōmi](../../strongs/g/g1325.md) to you all. 

<a name="genesis_9_4"></a>Genesis 9:4

Except [kreas](../../strongs/g/g2907.md) with the [haima](../../strongs/g/g129.md) of [psychē](../../strongs/g/g5590.md) you shall not [esthiō](../../strongs/g/g2068.md). 

<a name="genesis_9_5"></a>Genesis 9:5

For even your [haima](../../strongs/g/g129.md) of your [psychē](../../strongs/g/g5590.md) at the [cheir](../../strongs/g/g5495.md) of all the [thērion](../../strongs/g/g2342.md) I shall [ekzēteō](../../strongs/g/g1567.md) it, and from the [cheir](../../strongs/g/g5495.md) of a [anthrōpos](../../strongs/g/g444.md) [adelphos](../../strongs/g/g80.md) I shall [ekzēteō](../../strongs/g/g1567.md) it. 

<a name="genesis_9_6"></a>Genesis 9:6

The [ekcheō](../../strongs/g/g1632.md) [haima](../../strongs/g/g129.md) of a [anthrōpos](../../strongs/g/g444.md), in return his [haima](../../strongs/g/g129.md) shall be [ekcheō](../../strongs/g/g1632.md); for in the [eikōn](../../strongs/g/g1504.md) of [theos](../../strongs/g/g2316.md) I [poieō](../../strongs/g/g4160.md) the [anthrōpos](../../strongs/g/g444.md). 

<a name="genesis_9_7"></a>Genesis 9:7

But you [auxanō](../../strongs/g/g837.md), and [plēthynō](../../strongs/g/g4129.md), and [plēroō](../../strongs/g/g4137.md) the [gē](../../strongs/g/g1093.md), and [plēthynō](../../strongs/g/g4129.md) upon the [gē](../../strongs/g/g1093.md)! 

<a name="genesis_9_8"></a>Genesis 9:8

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md) to [Nōe](../../strongs/g/g3575.md), and to his [huios](../../strongs/g/g5207.md), [legō](../../strongs/g/g3004.md), 

<a name="genesis_9_9"></a>Genesis 9:9

[idou](../../strongs/g/g2400.md), I [anistēmi](../../strongs/g/g450.md) my [diathēkē](../../strongs/g/g1242.md) to you, and to your [sperma](../../strongs/g/g4690.md) after you, 

<a name="genesis_9_10"></a>Genesis 9:10

and every [psychē](../../strongs/g/g5590.md) [zaō](../../strongs/g/g2198.md) after you, from [orneon](../../strongs/g/g3732.md) and from [ktēnos](../../strongs/g/g2934.md), and with all the [thērion](../../strongs/g/g2342.md) of the [gē](../../strongs/g/g1093.md), as many as are with you, from all the ones [exerchomai](../../strongs/g/g1831.md) from out of the [kibōtos](../../strongs/g/g2787.md). 

<a name="genesis_9_11"></a>Genesis 9:11

And I will [histēmi](../../strongs/g/g2476.md) my [diathēkē](../../strongs/g/g1242.md) with you, and will not [apothnēskō](../../strongs/g/g599.md) all [sarx](../../strongs/g/g4561.md) any longer from the [hydōr](../../strongs/g/g5204.md) of the [kataklysmos](../../strongs/g/g2627.md); and there shall not be any longer a [kataklysmos](../../strongs/g/g2627.md) of [hydōr](../../strongs/g/g5204.md) to [kataphtheirō](../../strongs/g/g2704.md) all the [gē](../../strongs/g/g1093.md). 

<a name="genesis_9_12"></a>Genesis 9:12

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md) to [Nōe](../../strongs/g/g3575.md), This is the [sēmeion](../../strongs/g/g4592.md) of the [diathēkē](../../strongs/g/g1242.md) which I [didōmi](../../strongs/g/g1325.md) between me and you, and between every [psychē](../../strongs/g/g5590.md) [zaō](../../strongs/g/g2198.md), as many as are with you for [genea](../../strongs/g/g1074.md) [aiōnios](../../strongs/g/g166.md).

<a name="genesis_9_13"></a>Genesis 9:13

 my [toxon](../../strongs/g/g5115.md) I [tithēmi](../../strongs/g/g5087.md) in the [nephelē](../../strongs/g/g3507.md), and it will be for a [sēmeion](../../strongs/g/g4592.md) of [diathēkē](../../strongs/g/g1242.md) between me and the [gē](../../strongs/g/g1093.md). 

<a name="genesis_9_14"></a>Genesis 9:14

And it will be in my collecting together (synnephein) the [nephelē](../../strongs/g/g3507.md) upon the [gē](../../strongs/g/g1093.md), will be [horaō](../../strongs/g/g3708.md) the [toxon](../../strongs/g/g5115.md) in the [nephelē](../../strongs/g/g3507.md). 

<a name="genesis_9_15"></a>Genesis 9:15

And I will [mimnēskomai](../../strongs/g/g3403.md) my [diathēkē](../../strongs/g/g1242.md) which is between me and you, and between every [psychē](../../strongs/g/g5590.md) [zaō](../../strongs/g/g2198.md) among all [sarx](../../strongs/g/g4561.md). And there will not be any longer the [hydōr](../../strongs/g/g5204.md) for a [kataklysmos](../../strongs/g/g2627.md) so as to [exaleiphō](../../strongs/g/g1813.md) all [sarx](../../strongs/g/g4561.md). 

<a name="genesis_9_16"></a>Genesis 9:16

And will be my [toxon](../../strongs/g/g5115.md) in the [nephelē](../../strongs/g/g3507.md); and I will [horaō](../../strongs/g/g3708.md) it to [mimnēskomai](../../strongs/g/g3403.md) [diathēkē](../../strongs/g/g1242.md) the [aiōnios](../../strongs/g/g166.md) between me and the [gē](../../strongs/g/g1093.md), and between [psychē](../../strongs/g/g5590.md) the [zaō](../../strongs/g/g2198.md) which is in all [sarx](../../strongs/g/g4561.md) upon the [gē](../../strongs/g/g1093.md). 

<a name="genesis_9_17"></a>Genesis 9:17

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md) to [Nōe](../../strongs/g/g3575.md), This is the [sēmeion](../../strongs/g/g4592.md) of the [diathēkē](../../strongs/g/g1242.md) of which I [diatithēmi](../../strongs/g/g1303.md) between me and between all [sarx](../../strongs/g/g4561.md) which is upon the [gē](../../strongs/g/g1093.md). 

<a name="genesis_9_18"></a>Genesis 9:18

And these were the [huios](../../strongs/g/g5207.md) of [Nōe](../../strongs/g/g3575.md), the ones [exerchomai](../../strongs/g/g1831.md) from the [kibōtos](../../strongs/g/g2787.md), [Sēm](../../strongs/g/g4590.md), Ham, Japheth. And Ham was [patēr](../../strongs/g/g3962.md) of [Chanaan](../../strongs/g/g5477.md).

<a name="genesis_9_19"></a>Genesis 9:19

These three are the [huios](../../strongs/g/g5207.md) of [Nōe](../../strongs/g/g3575.md). From these were [diaspeirō](../../strongs/g/g1289.md) upon all the [gē](../../strongs/g/g1093.md). 

<a name="genesis_9_20"></a>Genesis 9:20

And [archomai](../../strongs/g/g756.md) [Nōe](../../strongs/g/g3575.md) the [anthrōpos](../../strongs/g/g444.md) to be a [geōrgos](../../strongs/g/g1092.md) of the [gē](../../strongs/g/g1093.md). And he [phyteuō](../../strongs/g/g5452.md) a [ampelōn](../../strongs/g/g290.md), 

<a name="genesis_9_21"></a>Genesis 9:21

and [pinō](../../strongs/g/g4095.md) from the [oinos](../../strongs/g/g3631.md), and became [methyō](../../strongs/g/g3184.md), and became naked (egymnōthē) in his [oikos](../../strongs/g/g3624.md). 

<a name="genesis_9_22"></a>Genesis 9:22

And [eidō](../../strongs/g/g1492.md) Ham the [patēr](../../strongs/g/g3962.md) of [Chanaan](../../strongs/g/g5477.md) the nakedness (gymnōsin) of his [patēr](../../strongs/g/g3962.md); and [exerchomai](../../strongs/g/g1831.md), he [anaggellō](../../strongs/g/g312.md) [dyo](../../strongs/g/g1417.md) [adelphos](../../strongs/g/g80.md) to his [exō](../../strongs/g/g1854.md). 

<a name="genesis_9_23"></a>Genesis 9:23

And [lambanō](../../strongs/g/g2983.md) [Sēm](../../strongs/g/g4590.md) and Japheth the [himation](../../strongs/g/g2440.md), [epitithēmi](../../strongs/g/g2007.md) upon [dyo](../../strongs/g/g1417.md) [nōtos](../../strongs/g/g3577.md) their, and [poreuō](../../strongs/g/g4198.md) backwards (opisthophanōs), and they [sygkalyptō](../../strongs/g/g4780.md) the nakedness (gymnōsin) of their [patēr](../../strongs/g/g3962.md); and their [prosōpon](../../strongs/g/g4383.md) was backwards (opisthophanes), and the nakedness (gymnōsin) of their [patēr](../../strongs/g/g3962.md) they did not [eidō](../../strongs/g/g1492.md). 

<a name="genesis_9_24"></a>Genesis 9:24

[eknēphō](../../strongs/g/g1594.md) [Nōe](../../strongs/g/g3575.md) from the [oinos](../../strongs/g/g3631.md), and [ginōskō](../../strongs/g/g1097.md) as much as did [poieō](../../strongs/g/g4160.md) to him [huios](../../strongs/g/g5207.md) his [neos](../../strongs/g/g3501.md). 

<a name="genesis_9_25"></a>Genesis 9:25

And he [eipon](../../strongs/g/g2036.md), [epikataratos](../../strongs/g/g1944.md) [Chanaan](../../strongs/g/g5477.md) -- a [pais](../../strongs/g/g3816.md), a [oiketēs](../../strongs/g/g3610.md) he will be to his [adelphos](../../strongs/g/g80.md). 

<a name="genesis_9_26"></a>Genesis 9:26

And he [eipon](../../strongs/g/g2036.md), [eulogētos](../../strongs/g/g2128.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) of [Sēm](../../strongs/g/g4590.md), and [Chanaan](../../strongs/g/g5477.md) will be [pais](../../strongs/g/g3816.md) [oiketēs](../../strongs/g/g3610.md) his. 

<a name="genesis_9_27"></a>Genesis 9:27

May [theos](../../strongs/g/g2316.md) [platynō](../../strongs/g/g4115.md) to Japheth, and let him [katoikeō](../../strongs/g/g2730.md) in the [skēnōma](../../strongs/g/g4638.md) of [Sēm](../../strongs/g/g4590.md), and let [Chanaan](../../strongs/g/g5477.md) become their [pais](../../strongs/g/g3816.md). 

<a name="genesis_9_28"></a>Genesis 9:28

[zaō](../../strongs/g/g2198.md) [Nōe](../../strongs/g/g3575.md) after the [kataklysmos](../../strongs/g/g2627.md) [triakosioi](../../strongs/g/g5145.md) [pentēkonta](../../strongs/g/g4004.md) [etos](../../strongs/g/g2094.md). 

<a name="genesis_9_29"></a>Genesis 9:29

And were all the [hēmera](../../strongs/g/g2250.md) of [Nōe](../../strongs/g/g3575.md) nine hundred (ennakosia) [pentēkonta](../../strongs/g/g4004.md) [etos](../../strongs/g/g2094.md), and he [apothnēskō](../../strongs/g/g599.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 8](genesis_lxx_8.md) - [Genesis 10](genesis_lxx_10.md)