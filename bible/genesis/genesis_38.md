# [Genesis 38](https://www.blueletterbible.org/kjv/gen/38/1/s_38001)

<a name="genesis_38_1"></a>Genesis 38:1

And it came to pass at that [ʿēṯ](../../strongs/h/h6256.md), that [Yehuwdah](../../strongs/h/h3063.md) went[yarad](../../strongs/h/h3381.md) from his ['ach](../../strongs/h/h251.md), and [natah](../../strongs/h/h5186.md) to an ['iysh](../../strongs/h/h376.md) [ʿăḏullāmî](../../strongs/h/h5726.md), whose [shem](../../strongs/h/h8034.md) was [ḥîrâ](../../strongs/h/h2437.md).

<a name="genesis_38_2"></a>Genesis 38:2

And [Yehuwdah](../../strongs/h/h3063.md) [ra'ah](../../strongs/h/h7200.md) there a [bath](../../strongs/h/h1323.md) of an ['iysh](../../strongs/h/h376.md) [Kᵊnaʿănî](../../strongs/h/h3669.md), whose [shem](../../strongs/h/h8034.md) was [šôaʿ](../../strongs/h/h7770.md); and he [laqach](../../strongs/h/h3947.md) her, and [bow'](../../strongs/h/h935.md) unto her.

<a name="genesis_38_3"></a>Genesis 38:3

And she [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md); and he [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [ʿĒr](../../strongs/h/h6147.md).

<a name="genesis_38_4"></a>Genesis 38:4

And she [harah](../../strongs/h/h2029.md) again, and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md); and she [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) ['Ônān](../../strongs/h/h209.md).

<a name="genesis_38_5"></a>Genesis 38:5

And she yet [yāsap̄](../../strongs/h/h3254.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md); and [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Šēlâ](../../strongs/h/h7956.md): and he was at [kᵊzîḇ](../../strongs/h/h3580.md), when she [yalad](../../strongs/h/h3205.md) him.

<a name="genesis_38_6"></a>Genesis 38:6

And [Yehuwdah](../../strongs/h/h3063.md) [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) for [ʿĒr](../../strongs/h/h6147.md) his [bᵊḵôr](../../strongs/h/h1060.md), whose [shem](../../strongs/h/h8034.md) was [Tāmār](../../strongs/h/h8559.md).

<a name="genesis_38_7"></a>Genesis 38:7

And [ʿĒr](../../strongs/h/h6147.md), [Yehuwdah](../../strongs/h/h3063.md) [bᵊḵôr](../../strongs/h/h1060.md), was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md); and [Yĕhovah](../../strongs/h/h3068.md) [muwth](../../strongs/h/h4191.md) him.

<a name="genesis_38_8"></a>Genesis 38:8

And [Yehuwdah](../../strongs/h/h3063.md) ['āmar](../../strongs/h/h559.md) unto ['Ônān](../../strongs/h/h209.md), [bow'](../../strongs/h/h935.md) unto thy ['ach](../../strongs/h/h251.md) ['ishshah](../../strongs/h/h802.md), and [yibēm](../../strongs/h/h2992.md) her, and [quwm](../../strongs/h/h6965.md) [zera'](../../strongs/h/h2233.md) to thy ['ach](../../strongs/h/h251.md).

<a name="genesis_38_9"></a>Genesis 38:9

And ['Ônān](../../strongs/h/h209.md) [yada'](../../strongs/h/h3045.md) that the [zera'](../../strongs/h/h2233.md) should not be his; and it came to pass, when he [bow'](../../strongs/h/h935.md) unto his ['ach](../../strongs/h/h251.md) ['ishshah](../../strongs/h/h802.md), that he [shachath](../../strongs/h/h7843.md) it on the ['erets](../../strongs/h/h776.md), lest that he should [nathan](../../strongs/h/h5414.md) [zera'](../../strongs/h/h2233.md) to his ['ach](../../strongs/h/h251.md).

<a name="genesis_38_10"></a>Genesis 38:10

And the thing which he ['asah](../../strongs/h/h6213.md) [yāraʿ](../../strongs/h/h3415.md) ['ayin](../../strongs/h/h5869.md) [Yĕhovah](../../strongs/h/h3068.md): wherefore he [muwth](../../strongs/h/h4191.md) him also.

<a name="genesis_38_11"></a>Genesis 38:11

Then ['āmar](../../strongs/h/h559.md) [Yehuwdah](../../strongs/h/h3063.md) to [Tāmār](../../strongs/h/h8559.md) his [kallâ](../../strongs/h/h3618.md), [yashab](../../strongs/h/h3427.md) an ['almānâ](../../strongs/h/h490.md) at thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), till [Šēlâ](../../strongs/h/h7956.md) my [ben](../../strongs/h/h1121.md) be [gāḏal](../../strongs/h/h1431.md): for he ['āmar](../../strongs/h/h559.md), Lest peradventure he [muwth](../../strongs/h/h4191.md) also, as his ['ach](../../strongs/h/h251.md) did. And [Tāmār](../../strongs/h/h8559.md) [yālaḵ](../../strongs/h/h3212.md) and [yashab](../../strongs/h/h3427.md) in her ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md).

<a name="genesis_38_12"></a>Genesis 38:12

And in [rabah](../../strongs/h/h7235.md) of [yowm](../../strongs/h/h3117.md) the [bath](../../strongs/h/h1323.md) of [šôaʿ](../../strongs/h/h7770.md) [Yehuwdah](../../strongs/h/h3063.md) ['ishshah](../../strongs/h/h802.md) [muwth](../../strongs/h/h4191.md); and [Yehuwdah](../../strongs/h/h3063.md) was [nacham](../../strongs/h/h5162.md), and went[ʿālâ](../../strongs/h/h5927.md) unto his [gāzaz](../../strongs/h/h1494.md) [tso'n](../../strongs/h/h6629.md) to [Timnâ](../../strongs/h/h8553.md), he and his [rea'](../../strongs/h/h7453.md) [ḥîrâ](../../strongs/h/h2437.md) the [ʿăḏullāmî](../../strongs/h/h5726.md).

<a name="genesis_38_13"></a>Genesis 38:13

And it was [nāḡaḏ](../../strongs/h/h5046.md) [Tāmār](../../strongs/h/h8559.md), ['āmar](../../strongs/h/h559.md), Behold thy [Ḥām](../../strongs/h/h2524.md) [ʿālâ](../../strongs/h/h5927.md) to [Timnâ](../../strongs/h/h8553.md) to [gāzaz](../../strongs/h/h1494.md) his [tso'n](../../strongs/h/h6629.md).

<a name="genesis_38_14"></a>Genesis 38:14

And she put her ['almānûṯ](../../strongs/h/h491.md) [beḡeḏ](../../strongs/h/h899.md) [cuwr](../../strongs/h/h5493.md) from her, and [kāsâ](../../strongs/h/h3680.md) her with a [ṣāʿîp̄](../../strongs/h/h6809.md), and [ʿālap̄](../../strongs/h/h5968.md) herself, and [yashab](../../strongs/h/h3427.md) in an ['ayin](../../strongs/h/h5869.md) [peṯaḥ](../../strongs/h/h6607.md), which is by the [derek](../../strongs/h/h1870.md) to [Timnâ](../../strongs/h/h8553.md); for she [ra'ah](../../strongs/h/h7200.md) that [Šēlâ](../../strongs/h/h7956.md) was [gāḏal](../../strongs/h/h1431.md), and she was not [nathan](../../strongs/h/h5414.md) unto him to ['ishshah](../../strongs/h/h802.md).

<a name="genesis_38_15"></a>Genesis 38:15

When [Yehuwdah](../../strongs/h/h3063.md) [ra'ah](../../strongs/h/h7200.md) her, he [chashab](../../strongs/h/h2803.md) her to be a [zānâ](../../strongs/h/h2181.md); because she had [kāsâ](../../strongs/h/h3680.md) her [paniym](../../strongs/h/h6440.md).

<a name="genesis_38_16"></a>Genesis 38:16

And he [natah](../../strongs/h/h5186.md) unto her by the [derek](../../strongs/h/h1870.md), and ['āmar](../../strongs/h/h559.md), [yāhaḇ](../../strongs/h/h3051.md), I pray thee, let me [bow'](../../strongs/h/h935.md) in unto thee; (for he [yada'](../../strongs/h/h3045.md) not that she was his [kallâ](../../strongs/h/h3618.md).) And she ['āmar](../../strongs/h/h559.md), What wilt thou [nathan](../../strongs/h/h5414.md) me, that thou mayest [bow'](../../strongs/h/h935.md) in unto me?

<a name="genesis_38_17"></a>Genesis 38:17

And he ['āmar](../../strongs/h/h559.md), I will [shalach](../../strongs/h/h7971.md) thee a [ʿēz](../../strongs/h/h5795.md) [gᵊḏî](../../strongs/h/h1423.md) from the [tso'n](../../strongs/h/h6629.md). And she ['āmar](../../strongs/h/h559.md), Wilt thou [nathan](../../strongs/h/h5414.md) me a [ʿērāḇôn](../../strongs/h/h6162.md), till thou [shalach](../../strongs/h/h7971.md) it?

<a name="genesis_38_18"></a>Genesis 38:18

And he ['āmar](../../strongs/h/h559.md), What [ʿērāḇôn](../../strongs/h/h6162.md) shall I [nathan](../../strongs/h/h5414.md) thee? And she ['āmar](../../strongs/h/h559.md), Thy [ḥôṯām](../../strongs/h/h2368.md), and thy [pāṯîl](../../strongs/h/h6616.md), and thy [maṭṭê](../../strongs/h/h4294.md) that is in thine [yad](../../strongs/h/h3027.md). And he [nathan](../../strongs/h/h5414.md) it her, and [bow'](../../strongs/h/h935.md) in unto her, and she [harah](../../strongs/h/h2029.md) by him.

<a name="genesis_38_19"></a>Genesis 38:19

And she [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) away, and [cuwr](../../strongs/h/h5493.md) by her [ṣāʿîp̄](../../strongs/h/h6809.md) from her, and [labash](../../strongs/h/h3847.md) the [beḡeḏ](../../strongs/h/h899.md) of her ['almānûṯ](../../strongs/h/h491.md).

<a name="genesis_38_20"></a>Genesis 38:20

And [Yehuwdah](../../strongs/h/h3063.md) [shalach](../../strongs/h/h7971.md) the [gᵊḏî](../../strongs/h/h1423.md) [ʿēz](../../strongs/h/h5795.md) by the [yad](../../strongs/h/h3027.md) of his [rea'](../../strongs/h/h7453.md) the [ʿăḏullāmî](../../strongs/h/h5726.md), to [laqach](../../strongs/h/h3947.md) his [ʿērāḇôn](../../strongs/h/h6162.md) from the ['ishshah](../../strongs/h/h802.md) [yad](../../strongs/h/h3027.md): but he [māṣā'](../../strongs/h/h4672.md) her not.

<a name="genesis_38_21"></a>Genesis 38:21

Then he [sha'al](../../strongs/h/h7592.md) the ['enowsh](../../strongs/h/h582.md) of that [maqowm](../../strongs/h/h4725.md), ['āmar](../../strongs/h/h559.md), Where is the [qᵊḏēšâ](../../strongs/h/h6948.md), that was ['ayin](../../strongs/h/h5869.md) by the [derek](../../strongs/h/h1870.md)? And they ['āmar](../../strongs/h/h559.md), There was no [qᵊḏēšâ](../../strongs/h/h6948.md) in this.

<a name="genesis_38_22"></a>Genesis 38:22

And he [shuwb](../../strongs/h/h7725.md) to [Yehuwdah](../../strongs/h/h3063.md), and ['āmar](../../strongs/h/h559.md), I cannot [māṣā'](../../strongs/h/h4672.md) her; and also the ['enowsh](../../strongs/h/h582.md) of the [maqowm](../../strongs/h/h4725.md) ['āmar](../../strongs/h/h559.md), that there was no [qᵊḏēšâ](../../strongs/h/h6948.md).

<a name="genesis_38_23"></a>Genesis 38:23

And [Yehuwdah](../../strongs/h/h3063.md) ['āmar](../../strongs/h/h559.md), Let her [laqach](../../strongs/h/h3947.md) it to her, lest we be [bûz](../../strongs/h/h937.md): behold, I [shalach](../../strongs/h/h7971.md) this [gᵊḏî](../../strongs/h/h1423.md), and thou hast not [māṣā'](../../strongs/h/h4672.md) her.

<a name="genesis_38_24"></a>Genesis 38:24

And it came to pass about three [ḥōḏeš](../../strongs/h/h2320.md) after, that it was [nāḡaḏ](../../strongs/h/h5046.md) [Yehuwdah](../../strongs/h/h3063.md), ['āmar](../../strongs/h/h559.md), [Tāmār](../../strongs/h/h8559.md) thy [kallâ](../../strongs/h/h3618.md) hath [zānâ](../../strongs/h/h2181.md); and also, behold, she is [hārê](../../strongs/h/h2030.md) by [zᵊnûnîm](../../strongs/h/h2183.md). And [Yehuwdah](../../strongs/h/h3063.md) ['āmar](../../strongs/h/h559.md), [yāṣā'](../../strongs/h/h3318.md) her, and let her be [śārap̄](../../strongs/h/h8313.md).

<a name="genesis_38_25"></a>Genesis 38:25

When she was [yāṣā'](../../strongs/h/h3318.md), she [shalach](../../strongs/h/h7971.md) to her [Ḥām](../../strongs/h/h2524.md), ['āmar](../../strongs/h/h559.md), By the ['iysh](../../strongs/h/h376.md), whose these are, am I [hārê](../../strongs/h/h2030.md): and she ['āmar](../../strongs/h/h559.md), [nāḵar](../../strongs/h/h5234.md), I pray thee, whose are these, the [ḥōṯemeṯ](../../strongs/h/h2858.md), and [pāṯîl](../../strongs/h/h6616.md), and [maṭṭê](../../strongs/h/h4294.md).

<a name="genesis_38_26"></a>Genesis 38:26

And [Yehuwdah](../../strongs/h/h3063.md) [nāḵar](../../strongs/h/h5234.md) them, and ['āmar](../../strongs/h/h559.md), She hath been more [ṣāḏaq](../../strongs/h/h6663.md) than I; because that I [nathan](../../strongs/h/h5414.md) her not to [Šēlâ](../../strongs/h/h7956.md) my [ben](../../strongs/h/h1121.md). And he [yada'](../../strongs/h/h3045.md) her again no more.

<a name="genesis_38_27"></a>Genesis 38:27

And it came to pass in the [ʿēṯ](../../strongs/h/h6256.md) of her [yalad](../../strongs/h/h3205.md), that, behold, [tᵊ'ômîm](../../strongs/h/h8380.md) were in her [beten](../../strongs/h/h990.md).

<a name="genesis_38_28"></a>Genesis 38:28

And it came to pass, when she [yalad](../../strongs/h/h3205.md), that the one [nathan](../../strongs/h/h5414.md) his [yad](../../strongs/h/h3027.md): and the [yalad](../../strongs/h/h3205.md) [laqach](../../strongs/h/h3947.md) and [qāšar](../../strongs/h/h7194.md) upon his [yad](../../strongs/h/h3027.md) a [šānî](../../strongs/h/h8144.md), ['āmar](../../strongs/h/h559.md), This [yāṣā'](../../strongs/h/h3318.md) [ri'šôn](../../strongs/h/h7223.md).

<a name="genesis_38_29"></a>Genesis 38:29

And it came to pass, as he [shuwb](../../strongs/h/h7725.md) his [yad](../../strongs/h/h3027.md), that, behold, his ['ach](../../strongs/h/h251.md) [yāṣā'](../../strongs/h/h3318.md): and she ['āmar](../../strongs/h/h559.md), How hast thou [pāraṣ](../../strongs/h/h6555.md)? this [pereṣ](../../strongs/h/h6556.md) be upon thee: therefore his [shem](../../strongs/h/h8034.md) was [qara'](../../strongs/h/h7121.md) [Pereṣ](../../strongs/h/h6557.md).

<a name="genesis_38_30"></a>Genesis 38:30

And ['aḥar](../../strongs/h/h310.md) [yāṣā'](../../strongs/h/h3318.md) his ['ach](../../strongs/h/h251.md), that had the [šānî](../../strongs/h/h8144.md) upon his [yad](../../strongs/h/h3027.md): and his [shem](../../strongs/h/h8034.md) was [qara'](../../strongs/h/h7121.md) [Zeraḥ](../../strongs/h/h2226.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 37](genesis_37.md) - [Genesis 39](genesis_39.md)