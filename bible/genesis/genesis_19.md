# [Genesis 19](https://www.blueletterbible.org/kjv/gen/19/1/s_19001)

<a name="genesis_19_1"></a>Genesis 19:1

And there [bow'](../../strongs/h/h935.md) two [mal'ak](../../strongs/h/h4397.md) to [Sᵊḏōm](../../strongs/h/h5467.md) at ['ereb](../../strongs/h/h6153.md); and [Lôṭ](../../strongs/h/h3876.md) [yashab](../../strongs/h/h3427.md) in the [sha'ar](../../strongs/h/h8179.md) of [Sᵊḏōm](../../strongs/h/h5467.md): and [Lôṭ](../../strongs/h/h3876.md) [ra'ah](../../strongs/h/h7200.md) them [quwm](../../strongs/h/h6965.md) to [qārā'](../../strongs/h/h7125.md) them; and he [shachah](../../strongs/h/h7812.md) himself with his ['aph](../../strongs/h/h639.md) toward the ['erets](../../strongs/h/h776.md);

<a name="genesis_19_2"></a>Genesis 19:2

And he ['āmar](../../strongs/h/h559.md), Behold now, my ['adown](../../strongs/h/h113.md), [cuwr](../../strongs/h/h5493.md), I pray you, into your ['ebed](../../strongs/h/h5650.md) [bayith](../../strongs/h/h1004.md), and [lûn](../../strongs/h/h3885.md), and [rāḥaṣ](../../strongs/h/h7364.md) your [regel](../../strongs/h/h7272.md), and ye shall [šāḵam](../../strongs/h/h7925.md), and [halak](../../strongs/h/h1980.md) on your [derek](../../strongs/h/h1870.md). And they ['āmar](../../strongs/h/h559.md), Nay; but we will [lûn](../../strongs/h/h3885.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md).

<a name="genesis_19_3"></a>Genesis 19:3

And he [pāṣar](../../strongs/h/h6484.md) upon them [me'od](../../strongs/h/h3966.md); and they [cuwr](../../strongs/h/h5493.md) unto him, and [bow'](../../strongs/h/h935.md) into his [bayith](../../strongs/h/h1004.md); and he ['asah](../../strongs/h/h6213.md) them a [mištê](../../strongs/h/h4960.md), and did ['āp̄â](../../strongs/h/h644.md) [maṣṣâ](../../strongs/h/h4682.md), and they did ['akal](../../strongs/h/h398.md).

<a name="genesis_19_4"></a>Genesis 19:4

But before they [shakab](../../strongs/h/h7901.md), the ['enowsh](../../strongs/h/h582.md) of the [ʿîr](../../strongs/h/h5892.md), even the ['enowsh](../../strongs/h/h582.md) of [Sᵊḏōm](../../strongs/h/h5467.md), [cabab](../../strongs/h/h5437.md) the [bayith](../../strongs/h/h1004.md), both [zāqēn](../../strongs/h/h2205.md) and [naʿar](../../strongs/h/h5288.md), all the ['am](../../strongs/h/h5971.md) from every [qāṣê](../../strongs/h/h7097.md):

<a name="genesis_19_5"></a>Genesis 19:5

And they [qara'](../../strongs/h/h7121.md) unto [Lôṭ](../../strongs/h/h3876.md), and ['āmar](../../strongs/h/h559.md) unto him, Where are the ['enowsh](../../strongs/h/h582.md) which [bow'](../../strongs/h/h935.md) to thee this [layil](../../strongs/h/h3915.md)? [yāṣā'](../../strongs/h/h3318.md) them out unto us, that we may [yada'](../../strongs/h/h3045.md) them.

<a name="genesis_19_6"></a>Genesis 19:6

And [Lôṭ](../../strongs/h/h3876.md) [yāṣā'](../../strongs/h/h3318.md) at the [peṯaḥ](../../strongs/h/h6607.md) unto them, and [cagar](../../strongs/h/h5462.md) the [deleṯ](../../strongs/h/h1817.md) ['aḥar](../../strongs/h/h310.md) him,

<a name="genesis_19_7"></a>Genesis 19:7

And ['āmar](../../strongs/h/h559.md), I pray you, ['ach](../../strongs/h/h251.md), do not so [ra'a'](../../strongs/h/h7489.md).

<a name="genesis_19_8"></a>Genesis 19:8

Behold now, I have two [bath](../../strongs/h/h1323.md) which have not [yada'](../../strongs/h/h3045.md) ['iysh](../../strongs/h/h376.md); let me, I pray you, [yāṣā'](../../strongs/h/h3318.md) them out unto you, and ['asah](../../strongs/h/h6213.md) ye to them as is [towb](../../strongs/h/h2896.md) in your ['ayin](../../strongs/h/h5869.md): only unto these ['enowsh](../../strongs/h/h582.md) ['asah](../../strongs/h/h6213.md) ['al](../../strongs/h/h408.md) [dabar](../../strongs/h/h1697.md); for therefore [bow'](../../strongs/h/h935.md) they under the [ṣēl](../../strongs/h/h6738.md) of my [qôrâ](../../strongs/h/h6982.md).

<a name="genesis_19_9"></a>Genesis 19:9

And they ['āmar](../../strongs/h/h559.md), [nāḡaš](../../strongs/h/h5066.md) [hāl'â](../../strongs/h/h1973.md). And they ['āmar](../../strongs/h/h559.md) again, This one fellow [bow'](../../strongs/h/h935.md) to [guwr](../../strongs/h/h1481.md), and he will [shaphat](../../strongs/h/h8199.md) be a [shaphat](../../strongs/h/h8199.md): now will we [ra'a'](../../strongs/h/h7489.md) with thee, than with them. And they [pāṣar](../../strongs/h/h6484.md) [me'od](../../strongs/h/h3966.md) upon the ['iysh](../../strongs/h/h376.md), even [Lôṭ](../../strongs/h/h3876.md), and [nāḡaš](../../strongs/h/h5066.md) to [shabar](../../strongs/h/h7665.md) the [deleṯ](../../strongs/h/h1817.md).

<a name="genesis_19_10"></a>Genesis 19:10

But the ['enowsh](../../strongs/h/h582.md) [shalach](../../strongs/h/h7971.md) their [yad](../../strongs/h/h3027.md), and [bow'](../../strongs/h/h935.md) [Lôṭ](../../strongs/h/h3876.md) into the [bayith](../../strongs/h/h1004.md) to them, and [cagar](../../strongs/h/h5462.md) to the [deleṯ](../../strongs/h/h1817.md).

<a name="genesis_19_11"></a>Genesis 19:11

And they [nakah](../../strongs/h/h5221.md) the ['enowsh](../../strongs/h/h582.md) that were at the [peṯaḥ](../../strongs/h/h6607.md) of the [bayith](../../strongs/h/h1004.md) with [sanvērîm](../../strongs/h/h5575.md), both [qāṭān](../../strongs/h/h6996.md) and [gadowl](../../strongs/h/h1419.md): so that they [lā'â](../../strongs/h/h3811.md) themselves to [māṣā'](../../strongs/h/h4672.md) the [peṯaḥ](../../strongs/h/h6607.md).

<a name="genesis_19_12"></a>Genesis 19:12

And the ['enowsh](../../strongs/h/h582.md) ['āmar](../../strongs/h/h559.md) unto [Lôṭ](../../strongs/h/h3876.md), Hast thou here any besides? [ḥāṯān](../../strongs/h/h2860.md), and thy [ben](../../strongs/h/h1121.md), and thy [bath](../../strongs/h/h1323.md), and whatsoever thou hast in the [ʿîr](../../strongs/h/h5892.md), [yāṣā'](../../strongs/h/h3318.md) of this [maqowm](../../strongs/h/h4725.md):

<a name="genesis_19_13"></a>Genesis 19:13

For we will [shachath](../../strongs/h/h7843.md) this [maqowm](../../strongs/h/h4725.md), because the [tsa'aqah](../../strongs/h/h6818.md) of them is [gāḏal](../../strongs/h/h1431.md) before the [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md); and [Yĕhovah](../../strongs/h/h3068.md) hath [shalach](../../strongs/h/h7971.md) us to [shachath](../../strongs/h/h7843.md) it.

<a name="genesis_19_14"></a>Genesis 19:14

And [Lôṭ](../../strongs/h/h3876.md) [yāṣā'](../../strongs/h/h3318.md), and [dabar](../../strongs/h/h1696.md) unto his [ḥāṯān](../../strongs/h/h2860.md), which [laqach](../../strongs/h/h3947.md) his [bath](../../strongs/h/h1323.md), and ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md), [yāṣā'](../../strongs/h/h3318.md) you of this [maqowm](../../strongs/h/h4725.md); for [Yĕhovah](../../strongs/h/h3068.md) will [shachath](../../strongs/h/h7843.md) this [ʿîr](../../strongs/h/h5892.md). But he seemed as one that [ṣāḥaq](../../strongs/h/h6711.md) ['ayin](../../strongs/h/h5869.md) his [ḥāṯān](../../strongs/h/h2860.md).

<a name="genesis_19_15"></a>Genesis 19:15

And when the [šaḥar](../../strongs/h/h7837.md) [ʿālâ](../../strongs/h/h5927.md), then the [mal'ak](../../strongs/h/h4397.md) ['ûṣ](../../strongs/h/h213.md) [Lôṭ](../../strongs/h/h3876.md), ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md), [laqach](../../strongs/h/h3947.md) thy ['ishshah](../../strongs/h/h802.md), and thy two [bath](../../strongs/h/h1323.md), which are [māṣā'](../../strongs/h/h4672.md); lest thou be [sāp̄â](../../strongs/h/h5595.md) in the ['avon](../../strongs/h/h5771.md) of the [ʿîr](../../strongs/h/h5892.md).

<a name="genesis_19_16"></a>Genesis 19:16

And while he [māhah](../../strongs/h/h4102.md), the ['enowsh](../../strongs/h/h582.md) [ḥāzaq](../../strongs/h/h2388.md) upon his [yad](../../strongs/h/h3027.md), and upon the [yad](../../strongs/h/h3027.md) of his ['ishshah](../../strongs/h/h802.md), and upon the [yad](../../strongs/h/h3027.md) of his two [bath](../../strongs/h/h1323.md); [Yĕhovah](../../strongs/h/h3068.md) being [ḥemlâ](../../strongs/h/h2551.md) unto him: and they [yāṣā'](../../strongs/h/h3318.md) him, and [yānaḥ](../../strongs/h/h3240.md) him [ḥûṣ](../../strongs/h/h2351.md) the [ʿîr](../../strongs/h/h5892.md).

<a name="genesis_19_17"></a>Genesis 19:17

And it came to pass, when they had [yāṣā'](../../strongs/h/h3318.md) them [ḥûṣ](../../strongs/h/h2351.md), that he ['āmar](../../strongs/h/h559.md), [mālaṭ](../../strongs/h/h4422.md) for thy [nephesh](../../strongs/h/h5315.md); [nabat](../../strongs/h/h5027.md) not ['aḥar](../../strongs/h/h310.md) thee, neither ['amad](../../strongs/h/h5975.md) thou in all the [kikār](../../strongs/h/h3603.md); [mālaṭ](../../strongs/h/h4422.md) to the [har](../../strongs/h/h2022.md), lest thou be [sāp̄â](../../strongs/h/h5595.md).

<a name="genesis_19_18"></a>Genesis 19:18

And [Lôṭ](../../strongs/h/h3876.md) ['āmar](../../strongs/h/h559.md) unto them, Oh, not so, my ['adown](../../strongs/h/h113.md):

<a name="genesis_19_19"></a>Genesis 19:19

Behold now, thy ['ebed](../../strongs/h/h5650.md) hath [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md), and thou hast [gāḏal](../../strongs/h/h1431.md) thy [checed](../../strongs/h/h2617.md), which thou hast ['asah](../../strongs/h/h6213.md) unto me in [ḥāyâ](../../strongs/h/h2421.md) my [nephesh](../../strongs/h/h5315.md); and I [yakol](../../strongs/h/h3201.md) [mālaṭ](../../strongs/h/h4422.md) to the [har](../../strongs/h/h2022.md), lest some [ra'](../../strongs/h/h7451.md) [dāḇaq](../../strongs/h/h1692.md) me, and I [muwth](../../strongs/h/h4191.md):

<a name="genesis_19_20"></a>Genesis 19:20

Behold now, this [ʿîr](../../strongs/h/h5892.md) is [qarowb](../../strongs/h/h7138.md) to [nûs](../../strongs/h/h5127.md) unto, and it is a [miṣʿār](../../strongs/h/h4705.md): Oh, let me [mālaṭ](../../strongs/h/h4422.md) thither, (is it not a [miṣʿār](../../strongs/h/h4705.md)?) and my [nephesh](../../strongs/h/h5315.md) shall [ḥāyâ](../../strongs/h/h2421.md).

<a name="genesis_19_21"></a>Genesis 19:21

And he ['āmar](../../strongs/h/h559.md) unto him, [hinneh](../../strongs/h/h2009.md), I have [nasa'](../../strongs/h/h5375.md) [paniym](../../strongs/h/h6440.md) concerning this [dabar](../../strongs/h/h1697.md) also, that I will not [hāp̄aḵ](../../strongs/h/h2015.md) this [ʿîr](../../strongs/h/h5892.md), for the which thou hast [dabar](../../strongs/h/h1696.md).

<a name="genesis_19_22"></a>Genesis 19:22

[māhar](../../strongs/h/h4116.md) thee, [mālaṭ](../../strongs/h/h4422.md) thither; for I [yakol](../../strongs/h/h3201.md) ['asah](../../strongs/h/h6213.md) [dabar](../../strongs/h/h1697.md) till thou be [bow'](../../strongs/h/h935.md) thither. Therefore the [shem](../../strongs/h/h8034.md) of the [ʿîr](../../strongs/h/h5892.md) was [qara'](../../strongs/h/h7121.md) [Ṣōʿar](../../strongs/h/h6820.md).

<a name="genesis_19_23"></a>Genesis 19:23

The [šemeš](../../strongs/h/h8121.md) was [yāṣā'](../../strongs/h/h3318.md) upon the ['erets](../../strongs/h/h776.md) when [Lôṭ](../../strongs/h/h3876.md) [bow'](../../strongs/h/h935.md) into [Ṣōʿar](../../strongs/h/h6820.md).

<a name="genesis_19_24"></a>Genesis 19:24

Then [Yĕhovah](../../strongs/h/h3068.md) [matar](../../strongs/h/h4305.md) upon [Sᵊḏōm](../../strongs/h/h5467.md) and upon [ʿĂmōrâ](../../strongs/h/h6017.md) [gophriyth](../../strongs/h/h1614.md) and ['esh](../../strongs/h/h784.md) from [Yĕhovah](../../strongs/h/h3068.md) out of [shamayim](../../strongs/h/h8064.md);

<a name="genesis_19_25"></a>Genesis 19:25

And he [hāp̄aḵ](../../strongs/h/h2015.md) those [ʿîr](../../strongs/h/h5892.md), and all the [kikār](../../strongs/h/h3603.md), and all the [yashab](../../strongs/h/h3427.md) of the [ʿîr](../../strongs/h/h5892.md), and that which [ṣemaḥ](../../strongs/h/h6780.md) upon the ['adamah](../../strongs/h/h127.md).

<a name="genesis_19_26"></a>Genesis 19:26

But his ['ishshah](../../strongs/h/h802.md) [nabat](../../strongs/h/h5027.md) from ['aḥar](../../strongs/h/h310.md) him, and she became a [nᵊṣîḇ](../../strongs/h/h5333.md) of [melaḥ](../../strongs/h/h4417.md).

<a name="genesis_19_27"></a>Genesis 19:27

And ['Abraham](../../strongs/h/h85.md) [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md) to the [maqowm](../../strongs/h/h4725.md) where he ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md):

<a name="genesis_19_28"></a>Genesis 19:28

And he [šāqap̄](../../strongs/h/h8259.md) [paniym](../../strongs/h/h6440.md) [Sᵊḏōm](../../strongs/h/h5467.md) and [ʿĂmōrâ](../../strongs/h/h6017.md), and [paniym](../../strongs/h/h6440.md) all the ['erets](../../strongs/h/h776.md) of the [kikār](../../strongs/h/h3603.md), and [ra'ah](../../strongs/h/h7200.md), and, lo, the [qîṭôr](../../strongs/h/h7008.md) of the ['erets](../../strongs/h/h776.md) [ʿālâ](../../strongs/h/h5927.md) as the [qîṭôr](../../strongs/h/h7008.md) of a [kiḇšān](../../strongs/h/h3536.md).

<a name="genesis_19_29"></a>Genesis 19:29

And it came to pass, when ['Elohiym](../../strongs/h/h430.md) [shachath](../../strongs/h/h7843.md) the [ʿîr](../../strongs/h/h5892.md) of the [kikār](../../strongs/h/h3603.md), that ['Elohiym](../../strongs/h/h430.md) [zakar](../../strongs/h/h2142.md) ['Abraham](../../strongs/h/h85.md), and [shalach](../../strongs/h/h7971.md) [Lôṭ](../../strongs/h/h3876.md) out of the [tavek](../../strongs/h/h8432.md) of the [hăp̄ēḵâ](../../strongs/h/h2018.md), when he [hāp̄aḵ](../../strongs/h/h2015.md) the [ʿîr](../../strongs/h/h5892.md) in the which [Lôṭ](../../strongs/h/h3876.md) [yashab](../../strongs/h/h3427.md).

<a name="genesis_19_30"></a>Genesis 19:30

And [Lôṭ](../../strongs/h/h3876.md) [ʿālâ](../../strongs/h/h5927.md) out of [Ṣōʿar](../../strongs/h/h6820.md), and [yashab](../../strongs/h/h3427.md) in the [har](../../strongs/h/h2022.md), and his two [bath](../../strongs/h/h1323.md) with him; for he [yare'](../../strongs/h/h3372.md) to [yashab](../../strongs/h/h3427.md) in [Ṣōʿar](../../strongs/h/h6820.md): and he [yashab](../../strongs/h/h3427.md) in a [mᵊʿārâ](../../strongs/h/h4631.md), he and his two [bath](../../strongs/h/h1323.md).

<a name="genesis_19_31"></a>Genesis 19:31

And the [bᵊḵîrâ](../../strongs/h/h1067.md) ['āmar](../../strongs/h/h559.md) unto the [ṣāʿîr](../../strongs/h/h6810.md), Our ['ab](../../strongs/h/h1.md) is [zāqēn](../../strongs/h/h2204.md), and there is not an ['iysh](../../strongs/h/h376.md) in the ['erets](../../strongs/h/h776.md) to [bow'](../../strongs/h/h935.md) in unto us after the [derek](../../strongs/h/h1870.md) of all the ['erets](../../strongs/h/h776.md):

<a name="genesis_19_32"></a>Genesis 19:32

[yālaḵ](../../strongs/h/h3212.md), let us our ['ab](../../strongs/h/h1.md) [šāqâ](../../strongs/h/h8248.md) [yayin](../../strongs/h/h3196.md), and we will [shakab](../../strongs/h/h7901.md) with him, that we may [ḥāyâ](../../strongs/h/h2421.md) [zera'](../../strongs/h/h2233.md) of our ['ab](../../strongs/h/h1.md).

<a name="genesis_19_33"></a>Genesis 19:33

And they made their ['ab](../../strongs/h/h1.md) [šāqâ](../../strongs/h/h8248.md) [yayin](../../strongs/h/h3196.md) that [layil](../../strongs/h/h3915.md): and the [bᵊḵîrâ](../../strongs/h/h1067.md) [bow'](../../strongs/h/h935.md), and [shakab](../../strongs/h/h7901.md) with her ['ab](../../strongs/h/h1.md); and he [yada'](../../strongs/h/h3045.md) not when she [shakab](../../strongs/h/h7901.md), nor when she [quwm](../../strongs/h/h6965.md).

<a name="genesis_19_34"></a>Genesis 19:34

And it came to pass on the [māḥŏrāṯ](../../strongs/h/h4283.md), that the [bᵊḵîrâ](../../strongs/h/h1067.md) ['āmar](../../strongs/h/h559.md) unto the [ṣāʿîr](../../strongs/h/h6810.md), Behold, I [shakab](../../strongs/h/h7901.md) ['emeš](../../strongs/h/h570.md) with my ['ab](../../strongs/h/h1.md): let us make him [šāqâ](../../strongs/h/h8248.md) [yayin](../../strongs/h/h3196.md) this [layil](../../strongs/h/h3915.md) also; and thou [bow'](../../strongs/h/h935.md), and [shakab](../../strongs/h/h7901.md) with him, that we may [ḥāyâ](../../strongs/h/h2421.md) [zera'](../../strongs/h/h2233.md) of our ['ab](../../strongs/h/h1.md).

<a name="genesis_19_35"></a>Genesis 19:35

And they made their ['ab](../../strongs/h/h1.md) [šāqâ](../../strongs/h/h8248.md) [yayin](../../strongs/h/h3196.md) that [layil](../../strongs/h/h3915.md) also: and the [ṣāʿîr](../../strongs/h/h6810.md) [quwm](../../strongs/h/h6965.md), and [shakab](../../strongs/h/h7901.md) with him; and he [yada'](../../strongs/h/h3045.md) not when she [shakab](../../strongs/h/h7901.md), nor when she [quwm](../../strongs/h/h6965.md).

<a name="genesis_19_36"></a>Genesis 19:36

Thus were both the [bath](../../strongs/h/h1323.md) of [Lôṭ](../../strongs/h/h3876.md) [harah](../../strongs/h/h2029.md) by their ['ab](../../strongs/h/h1.md).

<a name="genesis_19_37"></a>Genesis 19:37

And the [bᵊḵîrâ](../../strongs/h/h1067.md) [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md), and [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Mô'āḇ](../../strongs/h/h4124.md): the same is the ['ab](../../strongs/h/h1.md) of the [Mô'āḇ](../../strongs/h/h4124.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="genesis_19_38"></a>Genesis 19:38

And the [ṣāʿîr](../../strongs/h/h6810.md), she also [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md), and [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [ben-ʿammî](../../strongs/h/h1151.md): the same is the ['ab](../../strongs/h/h1.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) unto this [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 18](genesis_18.md) - [Genesis 20](genesis_20.md)