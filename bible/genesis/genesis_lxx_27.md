# [Genesis 27](https://www.blueletterbible.org/lxx/genesis/27)

<a name="genesis_27_1"></a>Genesis 27:1

And [ginomai](../../strongs/g/g1096.md) after the [gēraskō](../../strongs/g/g1095.md) of [Isaak](../../strongs/g/g2464.md), that were blunted (ēmblynthēsan) his [ophthalmos](../../strongs/g/g3788.md) to [horaō](../../strongs/g/g3708.md). And he [kaleō](../../strongs/g/g2564.md) [Ēsau](../../strongs/g/g2269.md) [huios](../../strongs/g/g5207.md) his [presbyteros](../../strongs/g/g4245.md), and [eipon](../../strongs/g/g2036.md) to him, O my [huios](../../strongs/g/g5207.md). And he [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), it is I. 

<a name="genesis_27_2"></a>Genesis 27:2

And he [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), I have [gēraskō](../../strongs/g/g1095.md), and I do not [ginōskō](../../strongs/g/g1097.md) the [hēmera](../../strongs/g/g2250.md) of my [teleutē](../../strongs/g/g5054.md).

<a name="genesis_27_3"></a>Genesis 27:3

Now then, [lambanō](../../strongs/g/g2983.md) your [skeuos](../../strongs/g/g4632.md), both the quiver (pharetran) and the [toxon](../../strongs/g/g5115.md)! and [exerchomai](../../strongs/g/g1831.md) into the plain (pedion), and [thēreuō](../../strongs/g/g2340.md) for me [thēra](../../strongs/g/g2339.md)! 

<a name="genesis_27_4"></a>Genesis 27:4

And [poieō](../../strongs/g/g4160.md) for me food (edesmata) as I am [phileō](../../strongs/g/g5368.md), and [pherō](../../strongs/g/g5342.md) to me! that I may [esthiō](../../strongs/g/g2068.md), so that may [eulogeō](../../strongs/g/g2127.md) you my [psychē](../../strongs/g/g5590.md) before my [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_27_5"></a>Genesis 27:5

And [Rhebekka](../../strongs/g/g4479.md) [akouō](../../strongs/g/g191.md) [Isaak](../../strongs/g/g2464.md) [laleō](../../strongs/g/g2980.md) to [Ēsau](../../strongs/g/g2269.md) his [huios](../../strongs/g/g5207.md). [poreuō](../../strongs/g/g4198.md) [Ēsau](../../strongs/g/g2269.md) into the plain (pedion) to [thēreuō](../../strongs/g/g2340.md) [thēra](../../strongs/g/g2339.md) for his [patēr](../../strongs/g/g3962.md).

<a name="genesis_27_6"></a>Genesis 27:6

And [Rhebekka](../../strongs/g/g4479.md) [eipon](../../strongs/g/g2036.md) to [Iakōb](../../strongs/g/g2384.md) [huios](../../strongs/g/g5207.md) her [elassōn](../../strongs/g/g1640.md), [eidō](../../strongs/g/g1492.md)! I [akouō](../../strongs/g/g191.md) your [patēr](../../strongs/g/g3962.md) [laleō](../../strongs/g/g2980.md) to [Ēsau](../../strongs/g/g2269.md) your [adelphos](../../strongs/g/g80.md), [legō](../../strongs/g/g3004.md), 

<a name="genesis_27_7"></a>Genesis 27:7

[pherō](../../strongs/g/g5342.md) to me [thēra](../../strongs/g/g2339.md), and [poieō](../../strongs/g/g4160.md) food (edesmata) for me! that [esthiō](../../strongs/g/g2068.md), I may [eulogeō](../../strongs/g/g2127.md) you [enantion](../../strongs/g/g1726.md) the [kyrios](../../strongs/g/g2962.md), before my [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_27_8"></a>Genesis 27:8

Now then, O my [huios](../../strongs/g/g5207.md), [akouō](../../strongs/g/g191.md) me as I [entellō](../../strongs/g/g1781.md) to you!

<a name="genesis_27_9"></a>Genesis 27:9

And [poreuō](../../strongs/g/g4198.md) unto the [probaton](../../strongs/g/g4263.md), you [lambanō](../../strongs/g/g2983.md) for me from there [dyo](../../strongs/g/g1417.md) [eriphos](../../strongs/g/g2056.md), [hapalos](../../strongs/g/g527.md) and [kalos](../../strongs/g/g2570.md)! And I will [poieō](../../strongs/g/g4160.md) them for food (edesmata) for your [patēr](../../strongs/g/g3962.md) as he is [phileō](../../strongs/g/g5368.md). 

<a name="genesis_27_10"></a>Genesis 27:10

And you shall [eispherō](../../strongs/g/g1533.md) them to your [patēr](../../strongs/g/g3962.md), and he shall [esthiō](../../strongs/g/g2068.md), that may [eulogeō](../../strongs/g/g2127.md) you your [patēr](../../strongs/g/g3962.md) before his [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_27_11"></a>Genesis 27:11

[eipon](../../strongs/g/g2036.md) [Iakōb](../../strongs/g/g2384.md) to [Rhebekka](../../strongs/g/g4479.md) his [mētēr](../../strongs/g/g3384.md), is My [adelphos](../../strongs/g/g80.md) [anēr](../../strongs/g/g435.md) a hairy (dasys), and I am [anēr](../../strongs/g/g435.md) a [leios](../../strongs/g/g3006.md); 

<a name="genesis_27_12"></a>Genesis 27:12

lest at any time should [psēlaphaō](../../strongs/g/g5584.md) me my [patēr](../../strongs/g/g3962.md), and I will be [enantion](../../strongs/g/g1726.md) him as [kataphroneō](../../strongs/g/g2706.md), and I shall [epagō](../../strongs/g/g1863.md) upon myself a [katara](../../strongs/g/g2671.md), and not a [eulogia](../../strongs/g/g2129.md). 

<a name="genesis_27_13"></a>Genesis 27:13

[eipon](../../strongs/g/g2036.md) to him the [mētēr](../../strongs/g/g3384.md), Upon me be your [katara](../../strongs/g/g2671.md), [teknon](../../strongs/g/g5043.md). Only you [epakouō](../../strongs/g/g1873.md) my [phōnē](../../strongs/g/g5456.md)! And in [poreuō](../../strongs/g/g4198.md) [pherō](../../strongs/g/g5342.md) to me! 

<a name="genesis_27_14"></a>Genesis 27:14

And [poreuō](../../strongs/g/g4198.md), he [lambanō](../../strongs/g/g2983.md) them, and [pherō](../../strongs/g/g5342.md) to the [mētēr](../../strongs/g/g3384.md). And [poieō](../../strongs/g/g4160.md) his [mētēr](../../strongs/g/g3384.md) food (edesmata) as was [phileō](../../strongs/g/g5368.md) his [patēr](../../strongs/g/g3962.md). 

<a name="genesis_27_15"></a>Genesis 27:15

And [Rhebekka](../../strongs/g/g4479.md) having [lambanō](../../strongs/g/g2983.md) the [stolē](../../strongs/g/g4749.md) of [Ēsau](../../strongs/g/g2269.md) [huios](../../strongs/g/g5207.md) her [presbyteros](../../strongs/g/g4245.md), the [kalos](../../strongs/g/g2570.md), which was by her in the [oikos](../../strongs/g/g3624.md), she put it [endyō](../../strongs/g/g1746.md) [Iakōb](../../strongs/g/g2384.md) [huios](../../strongs/g/g5207.md) her [neos](../../strongs/g/g3501.md). 

<a name="genesis_27_16"></a>Genesis 27:16

And the [derma](../../strongs/g/g1192.md) of the [eriphos](../../strongs/g/g2056.md) she [peritithēmi](../../strongs/g/g4060.md) upon his [brachiōn](../../strongs/g/g1023.md), and upon the [gymnos](../../strongs/g/g1131.md) of his [trachēlos](../../strongs/g/g5137.md).

<a name="genesis_27_17"></a>Genesis 27:17

And she [didōmi](../../strongs/g/g1325.md) the foods (edesmata) and the [artos](../../strongs/g/g740.md) which she [poieō](../../strongs/g/g4160.md) unto the [cheir](../../strongs/g/g5495.md) of [Iakōb](../../strongs/g/g2384.md) her [huios](../../strongs/g/g5207.md). 

<a name="genesis_27_18"></a>Genesis 27:18

And he [eispherō](../../strongs/g/g1533.md) to his [patēr](../../strongs/g/g3962.md). And he [eipon](../../strongs/g/g2036.md), [patēr](../../strongs/g/g3962.md)! And he [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), it is I, who are you [teknon](../../strongs/g/g5043.md)? 

<a name="genesis_27_19"></a>Genesis 27:19

And [Iakōb](../../strongs/g/g2384.md) [eipon](../../strongs/g/g2036.md) to his [patēr](../../strongs/g/g3962.md), I am [Ēsau](../../strongs/g/g2269.md) your [prōtotokos](../../strongs/g/g4416.md). I [poieō](../../strongs/g/g4160.md) as you [laleō](../../strongs/g/g2980.md) me. In [anistēmi](../../strongs/g/g450.md) come take a [kathizō](../../strongs/g/g2523.md) and [esthiō](../../strongs/g/g2068.md) from my [thēra](../../strongs/g/g2339.md)! that may [eulogeō](../../strongs/g/g2127.md) me your [psychē](../../strongs/g/g5590.md). 

<a name="genesis_27_20"></a>Genesis 27:20

[eipon](../../strongs/g/g2036.md) [Isaak](../../strongs/g/g2464.md) to his [huios](../../strongs/g/g5207.md), What is this which so [tachys](../../strongs/g/g5036.md) you [heuriskō](../../strongs/g/g2147.md), [ō](../../strongs/g/g5599.md) [teknon](../../strongs/g/g5043.md)? And he [eipon](../../strongs/g/g2036.md), that which [paradidōmi](../../strongs/g/g3860.md) the [kyrios](../../strongs/g/g2962.md) your [theos](../../strongs/g/g2316.md) [enantion](../../strongs/g/g1726.md) me. 

<a name="genesis_27_21"></a>Genesis 27:21

[eipon](../../strongs/g/g2036.md) [Isaak](../../strongs/g/g2464.md) to [Iakōb](../../strongs/g/g2384.md), [eggizō](../../strongs/g/g1448.md) me, and I shall [psēlaphaō](../../strongs/g/g5584.md) you [teknon](../../strongs/g/g5043.md), to see if you are my [huios](../../strongs/g/g5207.md) [Ēsau](../../strongs/g/g2269.md) or not! 

<a name="genesis_27_22"></a>Genesis 27:22

[eggizō](../../strongs/g/g1448.md) [Iakōb](../../strongs/g/g2384.md) to [Isaak](../../strongs/g/g2464.md) his [patēr](../../strongs/g/g3962.md). And he [psēlaphaō](../../strongs/g/g5584.md) him, and [eipon](../../strongs/g/g2036.md), Indeed the [phōnē](../../strongs/g/g5456.md) is the [phōnē](../../strongs/g/g5456.md) of [Iakōb](../../strongs/g/g2384.md), but the [cheir](../../strongs/g/g5495.md) are the [cheir](../../strongs/g/g5495.md) of [Ēsau](../../strongs/g/g2269.md).

<a name="genesis_27_23"></a>Genesis 27:23

And he did not [epiginōskō](../../strongs/g/g1921.md) him, were for his [cheir](../../strongs/g/g5495.md) as the [cheir](../../strongs/g/g5495.md) of [Ēsau](../../strongs/g/g2269.md) his [adelphos](../../strongs/g/g80.md) -- hairy (daseiai); and he [eulogeō](../../strongs/g/g2127.md) him. 

<a name="genesis_27_24"></a>Genesis 27:24

And he [eipon](../../strongs/g/g2036.md), You are my [huios](../../strongs/g/g5207.md) [Ēsau](../../strongs/g/g2269.md)? And he [eipon](../../strongs/g/g2036.md), I am . 

<a name="genesis_27_25"></a>Genesis 27:25

And he [eipon](../../strongs/g/g2036.md), [prosagō](../../strongs/g/g4317.md) to me! and I will [esthiō](../../strongs/g/g2068.md) from your [thēra](../../strongs/g/g2339.md), [teknon](../../strongs/g/g5043.md), that may [eulogeō](../../strongs/g/g2127.md) you my [psychē](../../strongs/g/g5590.md). And he [prospherō](../../strongs/g/g4374.md) it to him, and he [esthiō](../../strongs/g/g2068.md); and he [eispherō](../../strongs/g/g1533.md) to him [oinos](../../strongs/g/g3631.md), and he [pinō](../../strongs/g/g4095.md). 

<a name="genesis_27_26"></a>Genesis 27:26

And [eipon](../../strongs/g/g2036.md) to him [Isaak](../../strongs/g/g2464.md) his [patēr](../../strongs/g/g3962.md), [eggizō](../../strongs/g/g1448.md) me, and [phileō](../../strongs/g/g5368.md) me [teknon](../../strongs/g/g5043.md)! 

<a name="genesis_27_27"></a>Genesis 27:27

And [eggizō](../../strongs/g/g1448.md), he [phileō](../../strongs/g/g5368.md) him. And he smelled (ōsphranthē) the [osmē](../../strongs/g/g3744.md) of his [himation](../../strongs/g/g2440.md), and [eulogeō](../../strongs/g/g2127.md) him, and [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), the [osmē](../../strongs/g/g3744.md) of my [huios](../../strongs/g/g5207.md) is as the [osmē](../../strongs/g/g3744.md) [agros](../../strongs/g/g68.md) of a [plērēs](../../strongs/g/g4134.md) which the [kyrios](../../strongs/g/g2962.md) [eulogeō](../../strongs/g/g2127.md);

<a name="genesis_27_28"></a>Genesis 27:28

and may [didōmi](../../strongs/g/g1325.md) to you [theos](../../strongs/g/g2316.md) from the dew (drosou) of the [ouranos](../../strongs/g/g3772.md), and from the [piotēs](../../strongs/g/g4096.md) of the [gē](../../strongs/g/g1093.md), and a [plēthos](../../strongs/g/g4128.md) of [sitos](../../strongs/g/g4621.md) and [oinos](../../strongs/g/g3631.md). 

<a name="genesis_27_29"></a>Genesis 27:29

And let be [douleuō](../../strongs/g/g1398.md) to you [ethnos](../../strongs/g/g1484.md)! And let do [proskyneō](../../strongs/g/g4352.md) to you [archōn](../../strongs/g/g758.md)! And you become [kyrios](../../strongs/g/g2962.md) of your [adelphos](../../strongs/g/g80.md)! And will do [proskyneō](../../strongs/g/g4352.md) to you the [huios](../../strongs/g/g5207.md) of your [patēr](../../strongs/g/g3962.md). The one [kataraomai](../../strongs/g/g2672.md) you will be [epikataratos](../../strongs/g/g1944.md), and the one [eulogeō](../../strongs/g/g2127.md) you will be for a [eulogeō](../../strongs/g/g2127.md). 

<a name="genesis_27_30"></a>Genesis 27:30

And [ginomai](../../strongs/g/g1096.md) after the [pauō](../../strongs/g/g3973.md) of [Isaak](../../strongs/g/g2464.md) [eulogeō](../../strongs/g/g2127.md) [Iakōb](../../strongs/g/g2384.md) his [huios](../../strongs/g/g5207.md), that it [ginomai](../../strongs/g/g1096.md) as [Iakōb](../../strongs/g/g2384.md) went forth from the [prosōpon](../../strongs/g/g4383.md) of [Isaak](../../strongs/g/g2464.md) his [patēr](../../strongs/g/g3962.md), that [Ēsau](../../strongs/g/g2269.md) his [adelphos](../../strongs/g/g80.md) [erchomai](../../strongs/g/g2064.md) from the [thēra](../../strongs/g/g2339.md). 

<a name="genesis_27_31"></a>Genesis 27:31

And he [poieō](../../strongs/g/g4160.md) also himself food (edesmata), and [prospherō](../../strongs/g/g4374.md) it his [patēr](../../strongs/g/g3962.md). And he [eipon](../../strongs/g/g2036.md) to the [patēr](../../strongs/g/g3962.md), [anistēmi](../../strongs/g/g450.md) my [patēr](../../strongs/g/g3962.md), and [esthiō](../../strongs/g/g2068.md) from the [thēra](../../strongs/g/g2339.md) of his [huios](../../strongs/g/g5207.md)! that may [eulogeō](../../strongs/g/g2127.md) me your [psychē](../../strongs/g/g5590.md). 

<a name="genesis_27_32"></a>Genesis 27:32

And [eipon](../../strongs/g/g2036.md) to him [Isaak](../../strongs/g/g2464.md) his [patēr](../../strongs/g/g3962.md), Who are you? And he [eipon](../../strongs/g/g2036.md), I am your [huios](../../strongs/g/g5207.md), the [prōtotokos](../../strongs/g/g4416.md), [Ēsau](../../strongs/g/g2269.md).

<a name="genesis_27_33"></a>Genesis 27:33

was [existēmi](../../strongs/g/g1839.md) [Isaak](../../strongs/g/g2464.md) [ekstasis](../../strongs/g/g1611.md) [megas](../../strongs/g/g3173.md) with an [sphodra](../../strongs/g/g4970.md). And he [eipon](../../strongs/g/g2036.md), Who then is the one [thēreuō](../../strongs/g/g2340.md) [thēra](../../strongs/g/g2339.md) for me, and [eispherō](../../strongs/g/g1533.md) it to me? And I [esthiō](../../strongs/g/g2068.md) from all before your [erchomai](../../strongs/g/g2064.md), and [eulogeō](../../strongs/g/g2127.md) him, and a [eulogeō](../../strongs/g/g2127.md) let it be!

<a name="genesis_27_34"></a>Genesis 27:34

And it [ginomai](../../strongs/g/g1096.md) when [Ēsau](../../strongs/g/g2269.md) [akouō](../../strongs/g/g191.md) the [rhēma](../../strongs/g/g4487.md) of his [patēr](../../strongs/g/g3962.md) [Isaak](../../strongs/g/g2464.md), he [anaboaō](../../strongs/g/g310.md) [phōnē](../../strongs/g/g5456.md) with a [megas](../../strongs/g/g3173.md) and [pikros](../../strongs/g/g4089.md) [sphodra](../../strongs/g/g4970.md). And he [eipon](../../strongs/g/g2036.md), You [eulogeō](../../strongs/g/g2127.md) indeed also me [patēr](../../strongs/g/g3962.md)! 

<a name="genesis_27_35"></a>Genesis 27:35

And he [eipon](../../strongs/g/g2036.md) to him, in [erchomai](../../strongs/g/g2064.md), your [adelphos](../../strongs/g/g80.md) with [dolos](../../strongs/g/g1388.md) [lambanō](../../strongs/g/g2983.md) your [eulogia](../../strongs/g/g2129.md). 

<a name="genesis_27_36"></a>Genesis 27:36

And he [eipon](../../strongs/g/g2036.md), [dikaiōs](../../strongs/g/g1346.md) was [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md) [Iakōb](../../strongs/g/g2384.md). For he has stomped (epterniken) upon me already this [deuteros](../../strongs/g/g1208.md) time. Even my [prōtotokia](../../strongs/g/g4415.md) he has [lambanō](../../strongs/g/g2983.md), and now he [lambanō](../../strongs/g/g2983.md) my [eulogia](../../strongs/g/g2129.md). And [Ēsau](../../strongs/g/g2269.md) [eipon](../../strongs/g/g2036.md) to his [patēr](../../strongs/g/g3962.md), Do you not [hypoleipō](../../strongs/g/g5275.md) to me a [eulogia](../../strongs/g/g2129.md), O [patēr](../../strongs/g/g3962.md)? 

<a name="genesis_27_37"></a>Genesis 27:37

And [apokrinomai](../../strongs/g/g611.md) [Isaak](../../strongs/g/g2464.md) [eipon](../../strongs/g/g2036.md) to [Ēsau](../../strongs/g/g2269.md), If [kyrios](../../strongs/g/g2962.md) him I [poieō](../../strongs/g/g4160.md) of you, and all his [adelphos](../../strongs/g/g80.md) I [poieō](../../strongs/g/g4160.md) his [oiketēs](../../strongs/g/g3610.md), with [sitos](../../strongs/g/g4621.md) and [oinos](../../strongs/g/g3631.md) to [stērizō](../../strongs/g/g4741.md) him -- then to you what may I [poieō](../../strongs/g/g4160.md) [teknon](../../strongs/g/g5043.md)? 

<a name="genesis_27_38"></a>Genesis 27:38

[eipon](../../strongs/g/g2036.md) [Ēsau](../../strongs/g/g2269.md) to his [patēr](../../strongs/g/g3962.md), not [eulogia](../../strongs/g/g2129.md) one in you is there, O [patēr](../../strongs/g/g3962.md)? [eulogeō](../../strongs/g/g2127.md) indeed me also [patēr](../../strongs/g/g3962.md)! [katanyssomai](../../strongs/g/g2660.md) [Isaak](../../strongs/g/g2464.md), [anaboaō](../../strongs/g/g310.md) a [phōnē](../../strongs/g/g5456.md) [Ēsau](../../strongs/g/g2269.md), and he [klaiō](../../strongs/g/g2799.md). 

<a name="genesis_27_39"></a>Genesis 27:39

[apokrinomai](../../strongs/g/g611.md) [Isaak](../../strongs/g/g2464.md) his [patēr](../../strongs/g/g3962.md) [eipon](../../strongs/g/g2036.md) to him, [idou](../../strongs/g/g2400.md), from the [piotēs](../../strongs/g/g4096.md) of the [gē](../../strongs/g/g1093.md) will be your [katoikēsis](../../strongs/g/g2731.md), and from the dew (drosou) of the [ouranos](../../strongs/g/g3772.md) [anōthen](../../strongs/g/g509.md). 

<a name="genesis_27_40"></a>Genesis 27:40

And upon your [machaira](../../strongs/g/g3162.md) you shall [zaō](../../strongs/g/g2198.md), and of your [adelphos](../../strongs/g/g80.md) you will be a [douleuō](../../strongs/g/g1398.md). And it will be when ever you should [kathaireō](../../strongs/g/g2507.md) and [eklyō](../../strongs/g/g1590.md) his [zygos](../../strongs/g/g2218.md) from your [trachēlos](../../strongs/g/g5137.md). 

<a name="genesis_27_41"></a>Genesis 27:41

And [Ēsau](../../strongs/g/g2269.md) was angry (enekotei) at [Iakōb](../../strongs/g/g2384.md) on account of the [eulogia](../../strongs/g/g2129.md) of which [eulogeō](../../strongs/g/g2127.md) him his [patēr](../../strongs/g/g3962.md). [eipon](../../strongs/g/g2036.md) [Ēsau](../../strongs/g/g2269.md) in his [dianoia](../../strongs/g/g1271.md), Let [eggizō](../../strongs/g/g1448.md) the [hēmera](../../strongs/g/g2250.md) of the [penthos](../../strongs/g/g3997.md) of my [patēr](../../strongs/g/g3962.md)! that I may [apokteinō](../../strongs/g/g615.md) [Iakōb](../../strongs/g/g2384.md) my [adelphos](../../strongs/g/g80.md). 

<a name="genesis_27_42"></a>Genesis 27:42

And were [apaggellō](../../strongs/g/g518.md) to [Rhebekka](../../strongs/g/g4479.md) the [rhēma](../../strongs/g/g4487.md) of [Ēsau](../../strongs/g/g2269.md) [huios](../../strongs/g/g5207.md) her [presbyteros](../../strongs/g/g4245.md). And [pempō](../../strongs/g/g3992.md), she [kaleō](../../strongs/g/g2564.md) [Iakōb](../../strongs/g/g2384.md) [huios](../../strongs/g/g5207.md) her [neos](../../strongs/g/g3501.md), and she [eipon](../../strongs/g/g2036.md) to him, [idou](../../strongs/g/g2400.md), [Ēsau](../../strongs/g/g2269.md), your [adelphos](../../strongs/g/g80.md) [apeileō](../../strongs/g/g546.md) you -- to [apokteinō](../../strongs/g/g615.md) you. 

<a name="genesis_27_43"></a>Genesis 27:43

Now then, [teknon](../../strongs/g/g5043.md), [akouō](../../strongs/g/g191.md) my [phōnē](../../strongs/g/g5456.md)! and [anistēmi](../../strongs/g/g450.md) run away (apodrathi) into [Mesopotamia](../../strongs/g/g3318.md) to Laban my [adelphos](../../strongs/g/g80.md) in Haran!

<a name="genesis_27_44"></a>Genesis 27:44

And [oikeō](../../strongs/g/g3611.md) with him some [hēmera](../../strongs/g/g2250.md)! 

<a name="genesis_27_45"></a>Genesis 27:45

until the [apostrephō](../../strongs/g/g654.md) the [thymos](../../strongs/g/g2372.md) and the [orgē](../../strongs/g/g3709.md) of your [adelphos](../../strongs/g/g80.md) from you, and he should [epilanthanomai](../../strongs/g/g1950.md) what you have [poieō](../../strongs/g/g4160.md) to him. And [apostellō](../../strongs/g/g649.md), I will [metapempō](../../strongs/g/g3343.md) you from there, lest at any time I should be childless (ateknōthō) from the two of you in day one. 

<a name="genesis_27_46"></a>Genesis 27:46

[eipon](../../strongs/g/g2036.md) [Rhebekka](../../strongs/g/g4479.md) to [Isaak](../../strongs/g/g2464.md), I [prosochthizō](../../strongs/g/g4360.md) my [zōē](../../strongs/g/g2222.md), on account of the [thygatēr](../../strongs/g/g2364.md) of the [huios](../../strongs/g/g5207.md) of Heth. If [Iakōb](../../strongs/g/g2384.md) shall take a [gynē](../../strongs/g/g1135.md) from the [thygatēr](../../strongs/g/g2364.md) of this [gē](../../strongs/g/g1093.md), why is it for me to [zaō](../../strongs/g/g2198.md)? 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 26](genesis_lxx_26.md) - [Genesis 28](genesis_lxx_28.md)