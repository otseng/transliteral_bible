# [Genesis 20](https://www.blueletterbible.org/lxx/genesis/20)

<a name="genesis_20_1"></a>Genesis 20:1

And [kineō](../../strongs/g/g2795.md) from there [Abraam](../../strongs/g/g11.md) into the [gē](../../strongs/g/g1093.md) towards the [lips](../../strongs/g/g3047.md), and [oikeō](../../strongs/g/g3611.md) between Kadesh and between Shur, and [paroikeō](../../strongs/g/g3939.md) in Gerar.

<a name="genesis_20_2"></a>Genesis 20:2

[eipon](../../strongs/g/g2036.md) [Abraam](../../strongs/g/g11.md) concerning [Sarra](../../strongs/g/g4564.md) his [gynē](../../strongs/g/g1135.md), that, my [adelphē](../../strongs/g/g79.md) She is. For he [phobeō](../../strongs/g/g5399.md) to [eipon](../../strongs/g/g2036.md) that, my [gynē](../../strongs/g/g1135.md) She is, lest at any time should [apokteinō](../../strongs/g/g615.md) him the [anēr](../../strongs/g/g435.md) of the [polis](../../strongs/g/g4172.md) on account of her. [apostellō](../../strongs/g/g649.md) so Abimelech [basileus](../../strongs/g/g935.md) of Gera, and he [lambanō](../../strongs/g/g2983.md) [Sarra](../../strongs/g/g4564.md).

<a name="genesis_20_3"></a>Genesis 20:3

And [theos](../../strongs/g/g2316.md) [eiserchomai](../../strongs/g/g1525.md) to Abimelech by [hypnos](../../strongs/g/g5258.md) in the [nyx](../../strongs/g/g3571.md). And he [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), you [apothnēskō](../../strongs/g/g599.md) on account of the [gynē](../../strongs/g/g1135.md) of whom you [lambanō](../../strongs/g/g2983.md), for she is [synoikeō](../../strongs/g/g4924.md) with a [anēr](../../strongs/g/g435.md). 

<a name="genesis_20_4"></a>Genesis 20:4

And Abimelech not [haptomai](../../strongs/g/g680.md) her. And he [eipon](../../strongs/g/g2036.md), O [kyrios](../../strongs/g/g2962.md), a [ethnos](../../strongs/g/g1484.md) [agnoeō](../../strongs/g/g50.md) and [dikaios](../../strongs/g/g1342.md) will you [apollymi](../../strongs/g/g622.md)? 

<a name="genesis_20_5"></a>Genesis 20:5

not he to me [eipon](../../strongs/g/g2036.md), [adelphē](../../strongs/g/g79.md) she is my; and she [eipon](../../strongs/g/g2036.md) to me, [adelphos](../../strongs/g/g80.md) he is my? With a [katharos](../../strongs/g/g2513.md) [kardia](../../strongs/g/g2588.md), and with [dikaiosynē](../../strongs/g/g1343.md) of [cheir](../../strongs/g/g5495.md) I [poieō](../../strongs/g/g4160.md) this. 

<a name="genesis_20_6"></a>Genesis 20:6

[eipon](../../strongs/g/g2036.md) to him [theos](../../strongs/g/g2316.md) by [hypnos](../../strongs/g/g5258.md), I [ginōskō](../../strongs/g/g1097.md) that with a [katharos](../../strongs/g/g2513.md) [kardia](../../strongs/g/g2588.md) you [poieō](../../strongs/g/g4160.md) this, and I [pheidomai](../../strongs/g/g5339.md) you for to not [hamartanō](../../strongs/g/g264.md) you against me; because of this I did not [aphiēmi](../../strongs/g/g863.md) you [haptomai](../../strongs/g/g680.md) her. 

<a name="genesis_20_7"></a>Genesis 20:7

And now [apodidōmi](../../strongs/g/g591.md) the [gynē](../../strongs/g/g1135.md) of the [anthrōpos](../../strongs/g/g444.md), for he is a [prophētēs](../../strongs/g/g4396.md), and he will [proseuchomai](../../strongs/g/g4336.md) your account, and you shall [zaō](../../strongs/g/g2198.md). But if you do not [apodidōmi](../../strongs/g/g591.md), you shall [ginōskō](../../strongs/g/g1097.md) that you shall [apothnēskō](../../strongs/g/g599.md), you and all yours. 

<a name="genesis_20_8"></a>Genesis 20:8

And Abimelech rose early (ōrthrisen) in the [prōï](../../strongs/g/g4404.md), and he [kaleō](../../strongs/g/g2564.md) all his [pais](../../strongs/g/g3816.md). And he [laleō](../../strongs/g/g2980.md) all these [rhēma](../../strongs/g/g4487.md) into their [ous](../../strongs/g/g3775.md). [phobeō](../../strongs/g/g5399.md) all the [anthrōpos](../../strongs/g/g444.md) [sphodra](../../strongs/g/g4970.md). 

<a name="genesis_20_9"></a>Genesis 20:9

And Abimelech [kaleō](../../strongs/g/g2564.md) [Abraam](../../strongs/g/g11.md). And he [eipon](../../strongs/g/g2036.md) to him, What is this you [poieō](../../strongs/g/g4160.md) to us, lest we [hamartanō](../../strongs/g/g264.md) against you, that you [epagō](../../strongs/g/g1863.md) upon me and upon my [basileia](../../strongs/g/g932.md) [hamartia](../../strongs/g/g266.md) a [megas](../../strongs/g/g3173.md)? A [ergon](../../strongs/g/g2041.md) which no one will [poieō](../../strongs/g/g4160.md), you have [poieō](../../strongs/g/g4160.md) to me. 

<a name="genesis_20_10"></a>Genesis 20:10

[eipon](../../strongs/g/g2036.md) Abimelech to [Abraam](../../strongs/g/g11.md), What seeing (enidōn) did you [poieō](../../strongs/g/g4160.md) this? 

<a name="genesis_20_11"></a>Genesis 20:11

[eipon](../../strongs/g/g2036.md) [Abraam](../../strongs/g/g11.md), for I [eipon](../../strongs/g/g2036.md), surely there is not [theosebeia](../../strongs/g/g2317.md) in this [topos](../../strongs/g/g5117.md), me and they will [apokteinō](../../strongs/g/g615.md) because of my [gynē](../../strongs/g/g1135.md).

<a name="genesis_20_12"></a>Genesis 20:12

For also [alēthōs](../../strongs/g/g230.md) [adelphē](../../strongs/g/g79.md) she is my from my [patēr](../../strongs/g/g3962.md), but not from my [mētēr](../../strongs/g/g3384.md); and she became to me for [gynē](../../strongs/g/g1135.md). 

<a name="genesis_20_13"></a>Genesis 20:13

And it happened when [exagō](../../strongs/g/g1806.md) me [theos](../../strongs/g/g2316.md) from the [oikos](../../strongs/g/g3624.md) of my [patēr](../../strongs/g/g3962.md), and I [eipon](../../strongs/g/g2036.md) to her, This [dikaiosynē](../../strongs/g/g1343.md) you will [poieō](../../strongs/g/g4160.md) for me in every [topos](../../strongs/g/g5117.md) of which ever I [eiserchomai](../../strongs/g/g1525.md); there you [eipon](../../strongs/g/g2036.md) of me that, [adelphos](../../strongs/g/g80.md) He is my!

<a name="genesis_20_14"></a>Genesis 20:14

[lambanō](../../strongs/g/g2983.md) Abimelech a [chilioi](../../strongs/g/g5507.md) [didrachmon](../../strongs/g/g1323.md) [probaton](../../strongs/g/g4263.md), and [moschos](../../strongs/g/g3448.md), and [pais](../../strongs/g/g3816.md), and [paidiskē](../../strongs/g/g3814.md), and [didōmi](../../strongs/g/g1325.md) to [Abraam](../../strongs/g/g11.md), and he [apodidōmi](../../strongs/g/g591.md) to him [Sarra](../../strongs/g/g4564.md) his [gynē](../../strongs/g/g1135.md). 

<a name="genesis_20_15"></a>Genesis 20:15

And Abimelech [eipon](../../strongs/g/g2036.md) to [Abraam](../../strongs/g/g11.md), [idou](../../strongs/g/g2400.md), my [gē](../../strongs/g/g1093.md) is [enantion](../../strongs/g/g1726.md) you, which ever it may [areskō](../../strongs/g/g700.md) you, [katoikeō](../../strongs/g/g2730.md) there! 

<a name="genesis_20_16"></a>Genesis 20:16

"And to [Sarra](../../strongs/g/g4564.md) he [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), I have [didōmi](../../strongs/g/g1325.md) a [chilioi](../../strongs/g/g5507.md) [didrachmon](../../strongs/g/g1323.md) to your [adelphos](../../strongs/g/g80.md), these will be to you for the [timē](../../strongs/g/g5092.md) of your [prosōpon](../../strongs/g/g4383.md), and all the ones with you; and in all things you be [alētheuō](../../strongs/g/g226.md)! "

<a name="genesis_20_17"></a>Genesis 20:17

[proseuchomai](../../strongs/g/g4336.md) [Abraam](../../strongs/g/g11.md) to [theos](../../strongs/g/g2316.md). And [theos](../../strongs/g/g2316.md) [iaomai](../../strongs/g/g2390.md) Abimelech, and his [gynē](../../strongs/g/g1135.md), and his [paidiskē](../../strongs/g/g3814.md), and they [tiktō](../../strongs/g/g5088.md). 

<a name="genesis_20_18"></a>Genesis 20:18

For in [sygkleiō](../../strongs/g/g4788.md), the [kyrios](../../strongs/g/g2962.md) [exōthen](../../strongs/g/g1855.md) every [mētra](../../strongs/g/g3388.md) in the [oikos](../../strongs/g/g3624.md) of Abimelech, because of [Sarra](../../strongs/g/g4564.md) the [gynē](../../strongs/g/g1135.md) of [Abraam](../../strongs/g/g11.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 19](genesis_lxx_19.md) - [Genesis 21](genesis_lxx_21.md)