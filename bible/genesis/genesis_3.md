    # [Genesis 3](https://www.blueletterbible.org/kjv/gen/3/1/s_1001)

<a name="genesis_3_1"></a>Genesis 3:1

Now the [nachash](../../strongs/h/h5175.md) was more ['aruwm](../../strongs/h/h6175.md) than any [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) which [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) had ['asah](../../strongs/h/h6213.md). And he ['āmar](../../strongs/h/h559.md) unto the ['ishshah](../../strongs/h/h802.md), ['aph](../../strongs/h/h637.md), hath ['Elohiym](../../strongs/h/h430.md) said, Ye shall not ['akal](../../strongs/h/h398.md) of every ['ets](../../strongs/h/h6086.md) of the [gan](../../strongs/h/h1588.md)?

<a name="genesis_3_2"></a>Genesis 3:2

And the ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md) unto the [nachash](../../strongs/h/h5175.md), We may ['akal](../../strongs/h/h398.md) of the [pĕriy](../../strongs/h/h6529.md) of the ['ets](../../strongs/h/h6086.md) of the [gan](../../strongs/h/h1588.md):

<a name="genesis_3_3"></a>Genesis 3:3

But of the [pĕriy](../../strongs/h/h6529.md) of the ['ets](../../strongs/h/h6086.md) which is in the [tavek](../../strongs/h/h8432.md) of the [gan](../../strongs/h/h1588.md), ['Elohiym](../../strongs/h/h430.md) hath ['āmar](../../strongs/h/h559.md), Ye shall not ['akal](../../strongs/h/h398.md) of it, neither shall ye [naga'](../../strongs/h/h5060.md) it, lest ye [muwth](../../strongs/h/h4191.md).

<a name="genesis_3_4"></a>Genesis 3:4

And the [nachash](../../strongs/h/h5175.md) ['āmar](../../strongs/h/h559.md) unto the ['ishshah](../../strongs/h/h802.md), Ye shall not [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md):

<a name="genesis_3_5"></a>Genesis 3:5

For ['Elohiym](../../strongs/h/h430.md) doth [yada'](../../strongs/h/h3045.md) that in the [yowm](../../strongs/h/h3117.md) ye ['akal](../../strongs/h/h398.md) thereof, then your ['ayin](../../strongs/h/h5869.md) shall be [paqach](../../strongs/h/h6491.md), and ye shall be as ['Elohiym](../../strongs/h/h430.md), [yada'](../../strongs/h/h3045.md) [towb](../../strongs/h/h2896.md) and [ra'](../../strongs/h/h7451.md).

<a name="genesis_3_6"></a>Genesis 3:6

And when the ['ishshah](../../strongs/h/h802.md) [ra'ah](../../strongs/h/h7200.md) that the ['ets](../../strongs/h/h6086.md) was [towb](../../strongs/h/h2896.md) for [ma'akal](../../strongs/h/h3978.md), and that it was [ta'avah](../../strongs/h/h8378.md) to the ['ayin](../../strongs/h/h5869.md), and an ['ets](../../strongs/h/h6086.md) to be [chamad](../../strongs/h/h2530.md) to make one [sakal](../../strongs/h/h7919.md), she [laqach](../../strongs/h/h3947.md) of the [pĕriy](../../strongs/h/h6529.md) thereof, and did ['akal](../../strongs/h/h398.md), and [nathan](../../strongs/h/h5414.md) also unto her ['iysh](../../strongs/h/h376.md) with her; and he did ['akal](../../strongs/h/h398.md).

<a name="genesis_3_7"></a>Genesis 3:7

And the ['ayin](../../strongs/h/h5869.md) of them both were [paqach](../../strongs/h/h6491.md), and they [yada'](../../strongs/h/h3045.md) that they ['eyrom](../../strongs/h/h5903.md); and they [taphar](../../strongs/h/h8609.md) [tĕ'en](../../strongs/h/h8384.md) ['aleh](../../strongs/h/h5929.md), and ['asah](../../strongs/h/h6213.md) themselves [chagowr](../../strongs/h/h2290.md).

<a name="genesis_3_8"></a>Genesis 3:8

And they [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) [halak](../../strongs/h/h1980.md) in the [gan](../../strongs/h/h1588.md) in the [ruwach](../../strongs/h/h7307.md) of the [yowm](../../strongs/h/h3117.md): and ['Adam](../../strongs/h/h120.md) and his ['ishshah](../../strongs/h/h802.md) [chaba'](../../strongs/h/h2244.md) themselves from the [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) [tavek](../../strongs/h/h8432.md) the ['ets](../../strongs/h/h6086.md) of the [gan](../../strongs/h/h1588.md).

<a name="genesis_3_9"></a>Genesis 3:9

And [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) [qara'](../../strongs/h/h7121.md) unto ['adam](../../strongs/h/h120.md), and ['āmar](../../strongs/h/h559.md) unto him, ['ay](../../strongs/h/h335.md)?

<a name="genesis_3_10"></a>Genesis 3:10

And he ['āmar](../../strongs/h/h559.md), I [shama'](../../strongs/h/h8085.md) thy [qowl](../../strongs/h/h6963.md) in the [gan](../../strongs/h/h1588.md), and I was [yare'](../../strongs/h/h3372.md), because I ['eyrom](../../strongs/h/h5903.md); and I [chaba'](../../strongs/h/h2244.md) myself.

<a name="genesis_3_11"></a>Genesis 3:11

And he ['āmar](../../strongs/h/h559.md), Who [nāḡaḏ](../../strongs/h/h5046.md) thee that thou ['eyrom](../../strongs/h/h5903.md)? Hast thou ['akal](../../strongs/h/h398.md) of the ['ets](../../strongs/h/h6086.md), whereof I [tsavah](../../strongs/h/h6680.md) thee that thou shouldest not ['akal](../../strongs/h/h398.md)?

<a name="genesis_3_12"></a>Genesis 3:12

And ['adam](../../strongs/h/h120.md) ['āmar](../../strongs/h/h559.md), The ['ishshah](../../strongs/h/h802.md) whom thou [nathan](../../strongs/h/h5414.md) with me, she [nathan](../../strongs/h/h5414.md) me of the ['ets](../../strongs/h/h6086.md), and I did ['akal](../../strongs/h/h398.md).

<a name="genesis_3_13"></a>Genesis 3:13

And [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto the ['ishshah](../../strongs/h/h802.md), What this thou hast ['asah](../../strongs/h/h6213.md)? And the ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md), The [nachash](../../strongs/h/h5175.md) [nasha'](../../strongs/h/h5377.md) me, and I did ['akal](../../strongs/h/h398.md).

<a name="genesis_3_14"></a>Genesis 3:14

And [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto the [nachash](../../strongs/h/h5175.md), Because thou hast ['asah](../../strongs/h/h6213.md) this, thou ['arar](../../strongs/h/h779.md) above all [bĕhemah](../../strongs/h/h929.md), and above every [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md); upon thy [gachown](../../strongs/h/h1512.md) shalt thou [yālaḵ](../../strongs/h/h3212.md), and ['aphar](../../strongs/h/h6083.md) shalt thou ['akal](../../strongs/h/h398.md) all the [yowm](../../strongs/h/h3117.md) of thy [chay](../../strongs/h/h2416.md):

<a name="genesis_3_15"></a>Genesis 3:15

And I will [shiyth](../../strongs/h/h7896.md) ['eybah](../../strongs/h/h342.md) between thee and the ['ishshah](../../strongs/h/h802.md), and between thy [zera'](../../strongs/h/h2233.md) and her [zera'](../../strongs/h/h2233.md); it shall [shuwph](../../strongs/h/h7779.md) thy [ro'sh](../../strongs/h/h7218.md), and thou shalt [shuwph](../../strongs/h/h7779.md) his ['aqeb](../../strongs/h/h6119.md).

<a name="genesis_3_16"></a>Genesis 3:16

Unto the ['ishshah](../../strongs/h/h802.md) he ['āmar](../../strongs/h/h559.md), I will [rabah](../../strongs/h/h7235.md) [rabah](../../strongs/h/h7235.md) thy ['itstsabown](../../strongs/h/h6093.md) and thy [herown](../../strongs/h/h2032.md); in ['etseb](../../strongs/h/h6089.md) thou shalt [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md); and thy [tᵊšûqâ](../../strongs/h/h8669.md) to thy ['iysh](../../strongs/h/h376.md), and he shall [mashal](../../strongs/h/h4910.md) over thee. [^1]

<a name="genesis_3_17"></a>Genesis 3:17

And unto ['Āḏām](../../strongs/h/h121.md) he ['āmar](../../strongs/h/h559.md), Because thou hast [shama'](../../strongs/h/h8085.md) unto the [qowl](../../strongs/h/h6963.md) of thy ['ishshah](../../strongs/h/h802.md), and hast ['akal](../../strongs/h/h398.md) of the ['ets](../../strongs/h/h6086.md), of which I [tsavah](../../strongs/h/h6680.md) thee, ['āmar](../../strongs/h/h559.md), Thou shalt not ['akal](../../strongs/h/h398.md) of it: ['arar](../../strongs/h/h779.md) the ['adamah](../../strongs/h/h127.md) for thy sake; in ['itstsabown](../../strongs/h/h6093.md) shalt thou ['akal](../../strongs/h/h398.md) it all the [yowm](../../strongs/h/h3117.md) of thy [chay](../../strongs/h/h2416.md);

<a name="genesis_3_18"></a>Genesis 3:18

[qowts](../../strongs/h/h6975.md) also and [dardar](../../strongs/h/h1863.md) shall it [ṣāmaḥ](../../strongs/h/h6779.md) to thee; and thou shalt ['akal](../../strongs/h/h398.md) the ['eseb](../../strongs/h/h6212.md) of the [sadeh](../../strongs/h/h7704.md);

<a name="genesis_3_19"></a>Genesis 3:19

In the [ze'ah](../../strongs/h/h2188.md) of thy ['aph](../../strongs/h/h639.md) shalt thou ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md), till thou [shuwb](../../strongs/h/h7725.md) unto the ['adamah](../../strongs/h/h127.md); for out of it wast thou [laqach](../../strongs/h/h3947.md): for ['aphar](../../strongs/h/h6083.md) thou, and unto ['aphar](../../strongs/h/h6083.md) shalt thou [shuwb](../../strongs/h/h7725.md).

<a name="genesis_3_20"></a>Genesis 3:20

And ['Adam](../../strongs/h/h120.md) [qara'](../../strongs/h/h7121.md) his ['ishshah](../../strongs/h/h802.md) [shem](../../strongs/h/h8034.md) [Chavvah](../../strongs/h/h2332.md); because she was the ['em](../../strongs/h/h517.md) of [chay](../../strongs/h/h2416.md).

<a name="genesis_3_21"></a>Genesis 3:21

Unto ['Adam](../../strongs/h/h120.md) also and to his ['ishshah](../../strongs/h/h802.md) did [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) [kĕthoneth](../../strongs/h/h3801.md) of ['owr](../../strongs/h/h5785.md), and [labash](../../strongs/h/h3847.md) them.

<a name="genesis_3_22"></a>Genesis 3:22

And [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), [hen](../../strongs/h/h2005.md), the ['adam](../../strongs/h/h120.md) is become as one of us, to [yada'](../../strongs/h/h3045.md) [towb](../../strongs/h/h2896.md) and [ra'](../../strongs/h/h7451.md): and now, lest he [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md), and [laqach](../../strongs/h/h3947.md) also of the ['ets](../../strongs/h/h6086.md) of [chay](../../strongs/h/h2416.md), and ['akal](../../strongs/h/h398.md), and [chayay](../../strongs/h/h2425.md) ['owlam](../../strongs/h/h5769.md):

<a name="genesis_3_23"></a>Genesis 3:23

Therefore [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) [shalach](../../strongs/h/h7971.md) him from the [gan](../../strongs/h/h1588.md) of ['Eden](../../strongs/h/h5731.md), to ['abad](../../strongs/h/h5647.md) the ['adamah](../../strongs/h/h127.md) from whence he was [laqach](../../strongs/h/h3947.md).

<a name="genesis_3_24"></a>Genesis 3:24

So he [gāraš](../../strongs/h/h1644.md) the ['adam](../../strongs/h/h120.md); and he [shakan](../../strongs/h/h7931.md) at the [qeḏem](../../strongs/h/h6924.md) of the [gan](../../strongs/h/h1588.md) of ['Eden](../../strongs/h/h5731.md) [kĕruwb](../../strongs/h/h3742.md), and a [lāhaṭ](../../strongs/h/h3858.md) [chereb](../../strongs/h/h2719.md) which [hāp̄aḵ](../../strongs/h/h2015.md), to [shamar](../../strongs/h/h8104.md) the [derek](../../strongs/h/h1870.md) of the ['ets](../../strongs/h/h6086.md) of [chay](../../strongs/h/h2416.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 2](genesis_2.md) - [Genesis 4](genesis_4.md)

---

[^1]: [Genesis 3:16 Commentary](../../commentary/genesis/genesis_3_commentary.md#genesis_3_16)