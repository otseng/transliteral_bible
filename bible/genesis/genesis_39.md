# [Genesis 39](https://www.blueletterbible.org/kjv/gen/39/1/s_39001)

<a name="genesis_39_1"></a>Genesis 39:1

And [Yôsēp̄](../../strongs/h/h3130.md) was [yarad](../../strongs/h/h3381.md) to [Mitsrayim](../../strongs/h/h4714.md); and [p̄vṭyp̄r](../../strongs/h/h6318.md), a [sārîs](../../strongs/h/h5631.md) of [Parʿô](../../strongs/h/h6547.md), [śar](../../strongs/h/h8269.md) of the [ṭabāḥ](../../strongs/h/h2876.md), an ['iysh](../../strongs/h/h376.md) [Miṣrî](../../strongs/h/h4713.md), [qānâ](../../strongs/h/h7069.md) him of the [yad](../../strongs/h/h3027.md) of the [Yišmᵊʿē'lî](../../strongs/h/h3459.md), which had [yarad](../../strongs/h/h3381.md) him thither.

<a name="genesis_39_2"></a>Genesis 39:2

And [Yĕhovah](../../strongs/h/h3068.md) was with [Yôsēp̄](../../strongs/h/h3130.md), and he was a [tsalach](../../strongs/h/h6743.md) ['iysh](../../strongs/h/h376.md); and he was in the [bayith](../../strongs/h/h1004.md) of his ['adown](../../strongs/h/h113.md) the [Miṣrî](../../strongs/h/h4713.md).

<a name="genesis_39_3"></a>Genesis 39:3

And his ['adown](../../strongs/h/h113.md) [ra'ah](../../strongs/h/h7200.md) that [Yĕhovah](../../strongs/h/h3068.md) was with him, and that [Yĕhovah](../../strongs/h/h3068.md) made all that he ['asah](../../strongs/h/h6213.md) to [tsalach](../../strongs/h/h6743.md) in his [yad](../../strongs/h/h3027.md).

<a name="genesis_39_4"></a>Genesis 39:4

And [Yôsēp̄](../../strongs/h/h3130.md) [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in his ['ayin](../../strongs/h/h5869.md), and he [sharath](../../strongs/h/h8334.md) him: and he made him [paqad](../../strongs/h/h6485.md) over his [bayith](../../strongs/h/h1004.md), and all that he had he [nathan](../../strongs/h/h5414.md) into his [yad](../../strongs/h/h3027.md).

<a name="genesis_39_5"></a>Genesis 39:5

And it came to pass from the ['āz](../../strongs/h/h227.md) that he had made him [paqad](../../strongs/h/h6485.md) in his [bayith](../../strongs/h/h1004.md), and over all that he had, that [Yĕhovah](../../strongs/h/h3068.md) [barak](../../strongs/h/h1288.md) the [Miṣrî](../../strongs/h/h4713.md) [bayith](../../strongs/h/h1004.md) for [Yôsēp̄](../../strongs/h/h3130.md) [gālāl](../../strongs/h/h1558.md); and the [bĕrakah](../../strongs/h/h1293.md) of [Yĕhovah](../../strongs/h/h3068.md) was upon all that he had in the [bayith](../../strongs/h/h1004.md), and in the [sadeh](../../strongs/h/h7704.md).

<a name="genesis_39_6"></a>Genesis 39:6

And he ['azab](../../strongs/h/h5800.md) all that he had in [Yôsēp̄](../../strongs/h/h3130.md) [yad](../../strongs/h/h3027.md); and he [yada'](../../strongs/h/h3045.md) not [mᵊ'ûmâ](../../strongs/h/h3972.md) he had, save the [lechem](../../strongs/h/h3899.md) which he did ['akal](../../strongs/h/h398.md). And [Yôsēp̄](../../strongs/h/h3130.md) was [tō'ar](../../strongs/h/h8389.md), and [yāp̄ê](../../strongs/h/h3303.md) [mar'ê](../../strongs/h/h4758.md).

<a name="genesis_39_7"></a>Genesis 39:7

And it came to pass ['aḥar](../../strongs/h/h310.md) these [dabar](../../strongs/h/h1697.md), that his ['adown](../../strongs/h/h113.md) ['ishshah](../../strongs/h/h802.md) [nasa'](../../strongs/h/h5375.md) her ['ayin](../../strongs/h/h5869.md) upon [Yôsēp̄](../../strongs/h/h3130.md); and she ['āmar](../../strongs/h/h559.md), [shakab](../../strongs/h/h7901.md) with me.

<a name="genesis_39_8"></a>Genesis 39:8

But he [mā'ēn](../../strongs/h/h3985.md), and ['āmar](../../strongs/h/h559.md) unto his ['adown](../../strongs/h/h113.md) ['ishshah](../../strongs/h/h802.md), Behold, my ['adown](../../strongs/h/h113.md) [yada'](../../strongs/h/h3045.md) not what is with me in the [bayith](../../strongs/h/h1004.md), and he hath [nathan](../../strongs/h/h5414.md) all that he hath to my [yad](../../strongs/h/h3027.md);

<a name="genesis_39_9"></a>Genesis 39:9

There is none [gadowl](../../strongs/h/h1419.md) in this [bayith](../../strongs/h/h1004.md) than I; neither hath he [ḥāśaḵ](../../strongs/h/h2820.md) [mᵊ'ûmâ](../../strongs/h/h3972.md) from me but thee, because thou art his ['ishshah](../../strongs/h/h802.md): how then can I ['asah](../../strongs/h/h6213.md) this [gadowl](../../strongs/h/h1419.md) [ra'](../../strongs/h/h7451.md), and [chata'](../../strongs/h/h2398.md) against ['Elohiym](../../strongs/h/h430.md)?

<a name="genesis_39_10"></a>Genesis 39:10

And it came to pass, as she [dabar](../../strongs/h/h1696.md) to [Yôsēp̄](../../strongs/h/h3130.md) [yowm](../../strongs/h/h3117.md) by [yowm](../../strongs/h/h3117.md), that he [shama'](../../strongs/h/h8085.md) not unto her, to [shakab](../../strongs/h/h7901.md) by her, or to be with her.

<a name="genesis_39_11"></a>Genesis 39:11

And it came to pass about this [yowm](../../strongs/h/h3117.md), that [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) to ['asah](../../strongs/h/h6213.md) his [mĕla'kah](../../strongs/h/h4399.md); and there was none of the ['iysh](../../strongs/h/h376.md) ['enowsh](../../strongs/h/h582.md) of the [bayith](../../strongs/h/h1004.md) there within.

<a name="genesis_39_12"></a>Genesis 39:12

And she [tāp̄aś](../../strongs/h/h8610.md) him by his [beḡeḏ](../../strongs/h/h899.md), ['āmar](../../strongs/h/h559.md), [shakab](../../strongs/h/h7901.md) with me: and he ['azab](../../strongs/h/h5800.md) his [beḡeḏ](../../strongs/h/h899.md) in her [yad](../../strongs/h/h3027.md), and [nûs](../../strongs/h/h5127.md), and [yāṣā'](../../strongs/h/h3318.md) him [ḥûṣ](../../strongs/h/h2351.md).

<a name="genesis_39_13"></a>Genesis 39:13

And it came to pass, when she [ra'ah](../../strongs/h/h7200.md) that he had ['azab](../../strongs/h/h5800.md) his [beḡeḏ](../../strongs/h/h899.md) in her [yad](../../strongs/h/h3027.md), and was [nûs](../../strongs/h/h5127.md) [ḥûṣ](../../strongs/h/h2351.md),

<a name="genesis_39_14"></a>Genesis 39:14

That she [qara'](../../strongs/h/h7121.md) unto the ['enowsh](../../strongs/h/h582.md) of her [bayith](../../strongs/h/h1004.md), and ['āmar](../../strongs/h/h559.md) unto them, ['āmar](../../strongs/h/h559.md), [ra'ah](../../strongs/h/h7200.md), he hath [bow'](../../strongs/h/h935.md) an ['iysh](../../strongs/h/h376.md) [ʿiḇrî](../../strongs/h/h5680.md) unto us to [ṣāḥaq](../../strongs/h/h6711.md) us; he [bow'](../../strongs/h/h935.md) unto me to [shakab](../../strongs/h/h7901.md) with me, and I [qara'](../../strongs/h/h7121.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md):

<a name="genesis_39_15"></a>Genesis 39:15

And it came to pass, when he [shama'](../../strongs/h/h8085.md) that I [ruwm](../../strongs/h/h7311.md) my [qowl](../../strongs/h/h6963.md) and [qara'](../../strongs/h/h7121.md), that he ['azab](../../strongs/h/h5800.md) his [beḡeḏ](../../strongs/h/h899.md) with me, and [nûs](../../strongs/h/h5127.md), and [yāṣā'](../../strongs/h/h3318.md) him [ḥûṣ](../../strongs/h/h2351.md).

<a name="genesis_39_16"></a>Genesis 39:16

And she [yānaḥ](../../strongs/h/h3240.md) his [beḡeḏ](../../strongs/h/h899.md) by her, until his ['adown](../../strongs/h/h113.md) [bow'](../../strongs/h/h935.md) [bayith](../../strongs/h/h1004.md).

<a name="genesis_39_17"></a>Genesis 39:17

And she [dabar](../../strongs/h/h1696.md) unto him according to these [dabar](../../strongs/h/h1697.md), ['āmar](../../strongs/h/h559.md), The [ʿiḇrî](../../strongs/h/h5680.md) ['ebed](../../strongs/h/h5650.md), which thou hast [bow'](../../strongs/h/h935.md) unto us, [bow'](../../strongs/h/h935.md) unto me to [ṣāḥaq](../../strongs/h/h6711.md) me:

<a name="genesis_39_18"></a>Genesis 39:18

And it came to pass, as I [ruwm](../../strongs/h/h7311.md) my [qowl](../../strongs/h/h6963.md) and [qara'](../../strongs/h/h7121.md), that he ['azab](../../strongs/h/h5800.md) his [beḡeḏ](../../strongs/h/h899.md) with me, and [nûs](../../strongs/h/h5127.md) [ḥûṣ](../../strongs/h/h2351.md).

<a name="genesis_39_19"></a>Genesis 39:19

And it came to pass, when his ['adown](../../strongs/h/h113.md) [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of his ['ishshah](../../strongs/h/h802.md), which she [dabar](../../strongs/h/h1696.md) unto him, ['āmar](../../strongs/h/h559.md), After this [dabar](../../strongs/h/h1697.md) ['asah](../../strongs/h/h6213.md) thy ['ebed](../../strongs/h/h5650.md) to me; that his ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md).

<a name="genesis_39_20"></a>Genesis 39:20

And [Yôsēp̄](../../strongs/h/h3130.md) ['adown](../../strongs/h/h113.md) [laqach](../../strongs/h/h3947.md) him, and [nathan](../../strongs/h/h5414.md) him into the [bayith](../../strongs/h/h1004.md) [sōhar](../../strongs/h/h5470.md), a [maqowm](../../strongs/h/h4725.md) where the [melek](../../strongs/h/h4428.md) ['āsîr](../../strongs/h/h615.md) were ['āsar](../../strongs/h/h631.md): and he was there in the [bayith](../../strongs/h/h1004.md) [sōhar](../../strongs/h/h5470.md).

<a name="genesis_39_21"></a>Genesis 39:21

But [Yĕhovah](../../strongs/h/h3068.md) was with [Yôsēp̄](../../strongs/h/h3130.md), and [natah](../../strongs/h/h5186.md) him [checed](../../strongs/h/h2617.md), and [nathan](../../strongs/h/h5414.md) him [ḥēn](../../strongs/h/h2580.md) in the ['ayin](../../strongs/h/h5869.md) of the [śar](../../strongs/h/h8269.md) of the [bayith](../../strongs/h/h1004.md) [sōhar](../../strongs/h/h5470.md).

<a name="genesis_39_22"></a>Genesis 39:22

And the [śar](../../strongs/h/h8269.md) of the [bayith](../../strongs/h/h1004.md) [sōhar](../../strongs/h/h5470.md) [nathan](../../strongs/h/h5414.md) to [Yôsēp̄](../../strongs/h/h3130.md) [yad](../../strongs/h/h3027.md) all the ['āsîr](../../strongs/h/h615.md) that were in the [bayith](../../strongs/h/h1004.md) [sōhar](../../strongs/h/h5470.md); and whatsoever they ['asah](../../strongs/h/h6213.md) there, he was the ['asah](../../strongs/h/h6213.md) of it.

<a name="genesis_39_23"></a>Genesis 39:23

The [śar](../../strongs/h/h8269.md) of the [bayith](../../strongs/h/h1004.md) [sōhar](../../strongs/h/h5470.md) [ra'ah](../../strongs/h/h7200.md) not to [mᵊ'ûmâ](../../strongs/h/h3972.md) that was under his [yad](../../strongs/h/h3027.md); because [Yĕhovah](../../strongs/h/h3068.md) was with him, and that which he ['asah](../../strongs/h/h6213.md), [Yĕhovah](../../strongs/h/h3068.md) made it to [tsalach](../../strongs/h/h6743.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 38](genesis_38.md) - [Genesis 40](genesis_40.md)