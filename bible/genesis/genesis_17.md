# [Genesis 17](https://www.blueletterbible.org/kjv/gen/17/1/s_17001)

<a name="genesis_17_1"></a>Genesis 17:1

And when ['Aḇrām](../../strongs/h/h87.md) was ninety [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and nine, [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) to ['Aḇrām](../../strongs/h/h87.md), and ['āmar](../../strongs/h/h559.md) unto him, I am [Šaday](../../strongs/h/h7706.md) ['el](../../strongs/h/h410.md); [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) me, and be thou [tamiym](../../strongs/h/h8549.md).

<a name="genesis_17_2"></a>Genesis 17:2

And I will [nathan](../../strongs/h/h5414.md) my [bĕriyth](../../strongs/h/h1285.md) between me and thee, and will [rabah](../../strongs/h/h7235.md) thee [me'od](../../strongs/h/h3966.md) [me'od](../../strongs/h/h3966.md).

<a name="genesis_17_3"></a>Genesis 17:3

And ['Aḇrām](../../strongs/h/h87.md) [naphal](../../strongs/h/h5307.md) on his [paniym](../../strongs/h/h6440.md): and ['Elohiym](../../strongs/h/h430.md) [dabar](../../strongs/h/h1696.md) with him, ['āmar](../../strongs/h/h559.md),

<a name="genesis_17_4"></a>Genesis 17:4

As for me, behold, my [bĕriyth](../../strongs/h/h1285.md) is with thee, and thou shalt be an ['ab](../../strongs/h/h1.md) of [hāmôn](../../strongs/h/h1995.md) [gowy](../../strongs/h/h1471.md).

<a name="genesis_17_5"></a>Genesis 17:5

Neither shall thy [shem](../../strongs/h/h8034.md) any more be [qara'](../../strongs/h/h7121.md) ['Aḇrām](../../strongs/h/h87.md), but thy [shem](../../strongs/h/h8034.md) shall be ['Abraham](../../strongs/h/h85.md); for an ['ab](../../strongs/h/h1.md) of [hāmôn](../../strongs/h/h1995.md) [gowy](../../strongs/h/h1471.md) have I [nathan](../../strongs/h/h5414.md) thee.

<a name="genesis_17_6"></a>Genesis 17:6

And I will make thee [me'od](../../strongs/h/h3966.md) [me'od](../../strongs/h/h3966.md) [parah](../../strongs/h/h6509.md), and I will [nathan](../../strongs/h/h5414.md) [gowy](../../strongs/h/h1471.md) of thee, and [melek](../../strongs/h/h4428.md) shall [yāṣā'](../../strongs/h/h3318.md) of thee.

<a name="genesis_17_7"></a>Genesis 17:7

And I will [quwm](../../strongs/h/h6965.md) my [bĕriyth](../../strongs/h/h1285.md) between me and thee and thy [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) thee in their [dôr](../../strongs/h/h1755.md) for an ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md), to be an ['Elohiym](../../strongs/h/h430.md) unto thee, and to thy [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) thee.

<a name="genesis_17_8"></a>Genesis 17:8

And I will [nathan](../../strongs/h/h5414.md) unto thee, and to thy [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) thee, the ['erets](../../strongs/h/h776.md) wherein thou art a [māḡûr](../../strongs/h/h4033.md), all the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), for an ['owlam](../../strongs/h/h5769.md) ['achuzzah](../../strongs/h/h272.md); and I will be their ['Elohiym](../../strongs/h/h430.md).

<a name="genesis_17_9"></a>Genesis 17:9

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto ['Abraham](../../strongs/h/h85.md), Thou shalt [shamar](../../strongs/h/h8104.md) my [bĕriyth](../../strongs/h/h1285.md) therefore, thou, and thy [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) thee in their [dôr](../../strongs/h/h1755.md).

<a name="genesis_17_10"></a>Genesis 17:10

This is my [bĕriyth](../../strongs/h/h1285.md), which ye shall [shamar](../../strongs/h/h8104.md), between me and you and thy [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) thee; Every [zāḵār](../../strongs/h/h2145.md) among you shall be [muwl](../../strongs/h/h4135.md).

<a name="genesis_17_11"></a>Genesis 17:11

And ye shall [namal](../../strongs/h/h5243.md) the [basar](../../strongs/h/h1320.md) of your [ʿārlâ](../../strongs/h/h6190.md); and it shall be an ['ôṯ](../../strongs/h/h226.md) of the [bĕriyth](../../strongs/h/h1285.md) betwixt me and you.

<a name="genesis_17_12"></a>Genesis 17:12

And he that is eight [yowm](../../strongs/h/h3117.md) [ben](../../strongs/h/h1121.md) shall be [muwl](../../strongs/h/h4135.md) among you, every [zāḵār](../../strongs/h/h2145.md) in your [dôr](../../strongs/h/h1755.md), he that is [yālîḏ](../../strongs/h/h3211.md) in the [bayith](../../strongs/h/h1004.md), or [miqnâ](../../strongs/h/h4736.md) with [keceph](../../strongs/h/h3701.md) of any [ben](../../strongs/h/h1121.md) [nēḵār](../../strongs/h/h5236.md), which is not of thy [zera'](../../strongs/h/h2233.md).

<a name="genesis_17_13"></a>Genesis 17:13

He that is [yālîḏ](../../strongs/h/h3211.md) in thy [bayith](../../strongs/h/h1004.md), and he that is [miqnâ](../../strongs/h/h4736.md) with thy [keceph](../../strongs/h/h3701.md), must needs be [muwl](../../strongs/h/h4135.md): and my [bĕriyth](../../strongs/h/h1285.md) shall be in your [basar](../../strongs/h/h1320.md) for an ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md).

<a name="genesis_17_14"></a>Genesis 17:14

And the [ʿārēl](../../strongs/h/h6189.md) [zāḵār](../../strongs/h/h2145.md) whose [basar](../../strongs/h/h1320.md) of his [ʿārlâ](../../strongs/h/h6190.md) is not [muwl](../../strongs/h/h4135.md), that [nephesh](../../strongs/h/h5315.md) shall be [karath](../../strongs/h/h3772.md) from his ['am](../../strongs/h/h5971.md); he hath [pārar](../../strongs/h/h6565.md) my [bĕriyth](../../strongs/h/h1285.md).

<a name="genesis_17_15"></a>Genesis 17:15

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto ['Abraham](../../strongs/h/h85.md), As for [Śāray](../../strongs/h/h8297.md) thy ['ishshah](../../strongs/h/h802.md), thou shalt not [qara'](../../strongs/h/h7121.md) her [shem](../../strongs/h/h8034.md) [Śāray](../../strongs/h/h8297.md), but [Śārâ](../../strongs/h/h8283.md) shall her [shem](../../strongs/h/h8034.md) be.

<a name="genesis_17_16"></a>Genesis 17:16

And I will [barak](../../strongs/h/h1288.md) her, and [nathan](../../strongs/h/h5414.md) thee a [ben](../../strongs/h/h1121.md) also of her: yea, I will [barak](../../strongs/h/h1288.md) her, and she shall be a mother of [gowy](../../strongs/h/h1471.md); [melek](../../strongs/h/h4428.md) of ['am](../../strongs/h/h5971.md) shall be of her.

<a name="genesis_17_17"></a>Genesis 17:17

Then ['Abraham](../../strongs/h/h85.md) [naphal](../../strongs/h/h5307.md) upon his [paniym](../../strongs/h/h6440.md), and [ṣāḥaq](../../strongs/h/h6711.md), and ['āmar](../../strongs/h/h559.md) in his [leb](../../strongs/h/h3820.md), Shall a child be [yalad](../../strongs/h/h3205.md) unto him that is an hundred [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md)? and shall [Śārâ](../../strongs/h/h8283.md), that is ninety [šānâ](../../strongs/h/h8141.md) [bath](../../strongs/h/h1323.md), [yalad](../../strongs/h/h3205.md)?

<a name="genesis_17_18"></a>Genesis 17:18

And ['Abraham](../../strongs/h/h85.md) ['āmar](../../strongs/h/h559.md) unto ['Elohiym](../../strongs/h/h430.md), O that [Yišmāʿē'l](../../strongs/h/h3458.md) might [ḥāyâ](../../strongs/h/h2421.md) [paniym](../../strongs/h/h6440.md) thee!

<a name="genesis_17_19"></a>Genesis 17:19

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), [Śārâ](../../strongs/h/h8283.md) thy ['ishshah](../../strongs/h/h802.md) shall [yalad](../../strongs/h/h3205.md) thee a [ben](../../strongs/h/h1121.md) ['ăḇāl](../../strongs/h/h61.md); and thou shalt [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Yiṣḥāq](../../strongs/h/h3327.md): and I will [quwm](../../strongs/h/h6965.md) my [bĕriyth](../../strongs/h/h1285.md) with him for an ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md), and with his [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) him.

<a name="genesis_17_20"></a>Genesis 17:20

And as for [Yišmāʿē'l](../../strongs/h/h3458.md), I have [shama'](../../strongs/h/h8085.md) thee: Behold, I have [barak](../../strongs/h/h1288.md) him, and will make him [parah](../../strongs/h/h6509.md), and will [rabah](../../strongs/h/h7235.md) him [me'od](../../strongs/h/h3966.md) [me'od](../../strongs/h/h3966.md); twelve [nāśî'](../../strongs/h/h5387.md) shall he [yalad](../../strongs/h/h3205.md), and I will [nathan](../../strongs/h/h5414.md) him a [gadowl](../../strongs/h/h1419.md) [gowy](../../strongs/h/h1471.md).

<a name="genesis_17_21"></a>Genesis 17:21

But my [bĕriyth](../../strongs/h/h1285.md) will I [quwm](../../strongs/h/h6965.md) with [Yiṣḥāq](../../strongs/h/h3327.md), which [Śārâ](../../strongs/h/h8283.md) shall [yalad](../../strongs/h/h3205.md) unto thee at this [môʿēḏ](../../strongs/h/h4150.md) in the ['aḥēr](../../strongs/h/h312.md) [šānâ](../../strongs/h/h8141.md).

<a name="genesis_17_22"></a>Genesis 17:22

And he [kalah](../../strongs/h/h3615.md) [dabar](../../strongs/h/h1696.md) with him, and ['Elohiym](../../strongs/h/h430.md) [ʿālâ](../../strongs/h/h5927.md) from ['Abraham](../../strongs/h/h85.md).

<a name="genesis_17_23"></a>Genesis 17:23

And ['Abraham](../../strongs/h/h85.md) [laqach](../../strongs/h/h3947.md) [Yišmāʿē'l](../../strongs/h/h3458.md) his [ben](../../strongs/h/h1121.md), and all that were [yālîḏ](../../strongs/h/h3211.md) in his [bayith](../../strongs/h/h1004.md), and all that were [miqnâ](../../strongs/h/h4736.md) with his [keceph](../../strongs/h/h3701.md), every [zāḵār](../../strongs/h/h2145.md) among the ['enowsh](../../strongs/h/h582.md) of ['Abraham](../../strongs/h/h85.md) [bayith](../../strongs/h/h1004.md); and [muwl](../../strongs/h/h4135.md) the [basar](../../strongs/h/h1320.md) of their [ʿārlâ](../../strongs/h/h6190.md) in the ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md), as ['Elohiym](../../strongs/h/h430.md) had [dabar](../../strongs/h/h1696.md) unto him.

<a name="genesis_17_24"></a>Genesis 17:24

And ['Abraham](../../strongs/h/h85.md) was ninety [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and nine, when he was [muwl](../../strongs/h/h4135.md) in the [basar](../../strongs/h/h1320.md) of his [ʿārlâ](../../strongs/h/h6190.md).

<a name="genesis_17_25"></a>Genesis 17:25

And [Yišmāʿē'l](../../strongs/h/h3458.md) his [ben](../../strongs/h/h1121.md) was thirteen [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), when he was [muwl](../../strongs/h/h4135.md) in the [basar](../../strongs/h/h1320.md) of his [ʿārlâ](../../strongs/h/h6190.md).

<a name="genesis_17_26"></a>Genesis 17:26

In the ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md) was ['Abraham](../../strongs/h/h85.md) [muwl](../../strongs/h/h4135.md), and [Yišmāʿē'l](../../strongs/h/h3458.md) his [ben](../../strongs/h/h1121.md).

<a name="genesis_17_27"></a>Genesis 17:27

And all the ['enowsh](../../strongs/h/h582.md) of his [bayith](../../strongs/h/h1004.md), [yālîḏ](../../strongs/h/h3211.md) in the [bayith](../../strongs/h/h1004.md), and [miqnâ](../../strongs/h/h4736.md) with [keceph](../../strongs/h/h3701.md) of the [ben](../../strongs/h/h1121.md) [nēḵār](../../strongs/h/h5236.md), were [muwl](../../strongs/h/h4135.md) with him.

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 16](genesis_16.md) - [Genesis 18](genesis_18.md)