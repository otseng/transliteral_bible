# [Genesis 21](https://www.blueletterbible.org/kjv/gen/21/1/s_21001)

<a name="genesis_21_1"></a>Genesis 21:1

And [Yĕhovah](../../strongs/h/h3068.md) [paqad](../../strongs/h/h6485.md) [Śārâ](../../strongs/h/h8283.md) as he had ['āmar](../../strongs/h/h559.md), and [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) unto [Śārâ](../../strongs/h/h8283.md) as he had [dabar](../../strongs/h/h1696.md).

<a name="genesis_21_2"></a>Genesis 21:2

For [Śārâ](../../strongs/h/h8283.md) [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) ['Abraham](../../strongs/h/h85.md) a [ben](../../strongs/h/h1121.md) in his [zāqun](../../strongs/h/h2208.md), at the [môʿēḏ](../../strongs/h/h4150.md) of which ['Elohiym](../../strongs/h/h430.md) had [dabar](../../strongs/h/h1696.md) to him.

<a name="genesis_21_3"></a>Genesis 21:3

And ['Abraham](../../strongs/h/h85.md) [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of his [ben](../../strongs/h/h1121.md) that was [yalad](../../strongs/h/h3205.md) unto him, whom [Śārâ](../../strongs/h/h8283.md) [yalad](../../strongs/h/h3205.md) to him, [Yiṣḥāq](../../strongs/h/h3327.md).

<a name="genesis_21_4"></a>Genesis 21:4

And ['Abraham](../../strongs/h/h85.md) [muwl](../../strongs/h/h4135.md) his [ben](../../strongs/h/h1121.md) [Yiṣḥāq](../../strongs/h/h3327.md) being eight [yowm](../../strongs/h/h3117.md) [ben](../../strongs/h/h1121.md), as ['Elohiym](../../strongs/h/h430.md) had [tsavah](../../strongs/h/h6680.md) him.

<a name="genesis_21_5"></a>Genesis 21:5

And ['Abraham](../../strongs/h/h85.md) was an hundred [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), when his [ben](../../strongs/h/h1121.md) [Yiṣḥāq](../../strongs/h/h3327.md) was [yalad](../../strongs/h/h3205.md) unto him.

<a name="genesis_21_6"></a>Genesis 21:6

And [Śārâ](../../strongs/h/h8283.md) ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) hath ['asah](../../strongs/h/h6213.md) me to [ṣāḥaq](../../strongs/h/h6712.md), so that all that [shama'](../../strongs/h/h8085.md) will [ṣāḥaq](../../strongs/h/h6711.md) with me.

<a name="genesis_21_7"></a>Genesis 21:7

And she ['āmar](../../strongs/h/h559.md), Who would have [mālal](../../strongs/h/h4448.md) unto ['Abraham](../../strongs/h/h85.md), that [Śārâ](../../strongs/h/h8283.md) should have given [ben](../../strongs/h/h1121.md) [yānaq](../../strongs/h/h3243.md)? for I have [yalad](../../strongs/h/h3205.md) him a [ben](../../strongs/h/h1121.md) in his [zāqun](../../strongs/h/h2208.md).

<a name="genesis_21_8"></a>Genesis 21:8

And the [yeleḏ](../../strongs/h/h3206.md) [gāḏal](../../strongs/h/h1431.md), and was [gamal](../../strongs/h/h1580.md): and ['Abraham](../../strongs/h/h85.md) ['asah](../../strongs/h/h6213.md) a [gadowl](../../strongs/h/h1419.md) [mištê](../../strongs/h/h4960.md) the same [yowm](../../strongs/h/h3117.md) that [Yiṣḥāq](../../strongs/h/h3327.md) was [gamal](../../strongs/h/h1580.md).

<a name="genesis_21_9"></a>Genesis 21:9

And [Śārâ](../../strongs/h/h8283.md) [ra'ah](../../strongs/h/h7200.md) the [ben](../../strongs/h/h1121.md) of [Hāḡār](../../strongs/h/h1904.md) the [Miṣrî](../../strongs/h/h4713.md), which she had [yalad](../../strongs/h/h3205.md) unto ['Abraham](../../strongs/h/h85.md), [ṣāḥaq](../../strongs/h/h6711.md).

<a name="genesis_21_10"></a>Genesis 21:10

Wherefore she ['āmar](../../strongs/h/h559.md) unto ['Abraham](../../strongs/h/h85.md), [gāraš](../../strongs/h/h1644.md) this ['amah](../../strongs/h/h519.md) and her [ben](../../strongs/h/h1121.md): for the [ben](../../strongs/h/h1121.md) of this ['amah](../../strongs/h/h519.md) shall not be [yarash](../../strongs/h/h3423.md) with my [ben](../../strongs/h/h1121.md), even with [Yiṣḥāq](../../strongs/h/h3327.md).

<a name="genesis_21_11"></a>Genesis 21:11

And the [dabar](../../strongs/h/h1697.md) was [me'od](../../strongs/h/h3966.md) [yāraʿ](../../strongs/h/h3415.md) in ['Abraham](../../strongs/h/h85.md) ['ayin](../../strongs/h/h5869.md) because of his [ben](../../strongs/h/h1121.md).

<a name="genesis_21_12"></a>Genesis 21:12

And ['Elohiym](../../strongs/h/h430.md) said unto ['Abraham](../../strongs/h/h85.md), Let it not be [yāraʿ](../../strongs/h/h3415.md) in thy ['ayin](../../strongs/h/h5869.md) because of the [naʿar](../../strongs/h/h5288.md), and because of thy ['amah](../../strongs/h/h519.md); in all that [Śārâ](../../strongs/h/h8283.md) hath ['āmar](../../strongs/h/h559.md) unto thee, [shama'](../../strongs/h/h8085.md) unto her [qowl](../../strongs/h/h6963.md); for in [Yiṣḥāq](../../strongs/h/h3327.md) shall thy [zera'](../../strongs/h/h2233.md) be [qara'](../../strongs/h/h7121.md).

<a name="genesis_21_13"></a>Genesis 21:13

And also of the [ben](../../strongs/h/h1121.md) of the ['amah](../../strongs/h/h519.md) will I [śûm](../../strongs/h/h7760.md) a [gowy](../../strongs/h/h1471.md), because he is thy [zera'](../../strongs/h/h2233.md).

<a name="genesis_21_14"></a>Genesis 21:14

And ['Abraham](../../strongs/h/h85.md) [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [laqach](../../strongs/h/h3947.md) [lechem](../../strongs/h/h3899.md), and a [ḥēmeṯ](../../strongs/h/h2573.md) of [mayim](../../strongs/h/h4325.md), and [nathan](../../strongs/h/h5414.md) it unto [Hāḡār](../../strongs/h/h1904.md), [śûm](../../strongs/h/h7760.md) it on her [šᵊḵem](../../strongs/h/h7926.md), and the [yeleḏ](../../strongs/h/h3206.md), and [shalach](../../strongs/h/h7971.md) her away: and she [yālaḵ](../../strongs/h/h3212.md), and [tāʿâ](../../strongs/h/h8582.md) in the [midbar](../../strongs/h/h4057.md) of [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md).

<a name="genesis_21_15"></a>Genesis 21:15

And the [mayim](../../strongs/h/h4325.md) was [kalah](../../strongs/h/h3615.md) in the [ḥēmeṯ](../../strongs/h/h2573.md), and she [shalak](../../strongs/h/h7993.md) the [yeleḏ](../../strongs/h/h3206.md) under one of the [śîaḥ](../../strongs/h/h7880.md).

<a name="genesis_21_16"></a>Genesis 21:16

And she [yālaḵ](../../strongs/h/h3212.md), and [yashab](../../strongs/h/h3427.md) [neḡeḏ](../../strongs/h/h5048.md) him a [rachaq](../../strongs/h/h7368.md), as it were a [ṭāḥâ](../../strongs/h/h2909.md) [qesheth](../../strongs/h/h7198.md): for she ['āmar](../../strongs/h/h559.md), Let me not [ra'ah](../../strongs/h/h7200.md) the [maveth](../../strongs/h/h4194.md) of the [yeleḏ](../../strongs/h/h3206.md). And she [yashab](../../strongs/h/h3427.md) him, and [nasa'](../../strongs/h/h5375.md) her [qowl](../../strongs/h/h6963.md), and [bāḵâ](../../strongs/h/h1058.md).

<a name="genesis_21_17"></a>Genesis 21:17

And ['Elohiym](../../strongs/h/h430.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [naʿar](../../strongs/h/h5288.md); and the [mal'ak](../../strongs/h/h4397.md) of ['Elohiym](../../strongs/h/h430.md) [qara'](../../strongs/h/h7121.md) to [Hāḡār](../../strongs/h/h1904.md) out of [shamayim](../../strongs/h/h8064.md), and ['āmar](../../strongs/h/h559.md) unto her, What aileth thee, [Hāḡār](../../strongs/h/h1904.md)? [yare'](../../strongs/h/h3372.md) not; for ['Elohiym](../../strongs/h/h430.md) hath [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [naʿar](../../strongs/h/h5288.md) where he is.

<a name="genesis_21_18"></a>Genesis 21:18

[quwm](../../strongs/h/h6965.md), [nasa'](../../strongs/h/h5375.md) the [naʿar](../../strongs/h/h5288.md), and [ḥāzaq](../../strongs/h/h2388.md) him in thine [yad](../../strongs/h/h3027.md); for I will [śûm](../../strongs/h/h7760.md) him a [gadowl](../../strongs/h/h1419.md) [gowy](../../strongs/h/h1471.md).

<a name="genesis_21_19"></a>Genesis 21:19

And ['Elohiym](../../strongs/h/h430.md) [paqach](../../strongs/h/h6491.md) her ['ayin](../../strongs/h/h5869.md), and she [ra'ah](../../strongs/h/h7200.md) a [bᵊ'ēr](../../strongs/h/h875.md) of [mayim](../../strongs/h/h4325.md); and she [yālaḵ](../../strongs/h/h3212.md), and [mālā'](../../strongs/h/h4390.md) the [ḥēmeṯ](../../strongs/h/h2573.md) with [mayim](../../strongs/h/h4325.md), and [šāqâ](../../strongs/h/h8248.md) the [naʿar](../../strongs/h/h5288.md).

<a name="genesis_21_20"></a>Genesis 21:20

And ['Elohiym](../../strongs/h/h430.md) was with the [naʿar](../../strongs/h/h5288.md); and he [gāḏal](../../strongs/h/h1431.md), and [yashab](../../strongs/h/h3427.md) in the [midbar](../../strongs/h/h4057.md), and became a [rabah](../../strongs/h/h7235.md) [qaššāṯ](../../strongs/h/h7199.md).

<a name="genesis_21_21"></a>Genesis 21:21

And he [yashab](../../strongs/h/h3427.md) in the [midbar](../../strongs/h/h4057.md) of [Pā'rān](../../strongs/h/h6290.md): and his ['em](../../strongs/h/h517.md) [laqach](../../strongs/h/h3947.md) him an ['ishshah](../../strongs/h/h802.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_21_22"></a>Genesis 21:22

And it came to pass at that [ʿēṯ](../../strongs/h/h6256.md), that ['Ăḇîmeleḵ](../../strongs/h/h40.md) and [pîḵōl](../../strongs/h/h6369.md) the [śar](../../strongs/h/h8269.md) of his [tsaba'](../../strongs/h/h6635.md) ['āmar](../../strongs/h/h559.md) unto ['Abraham](../../strongs/h/h85.md), ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) is with thee in all that thou ['asah](../../strongs/h/h6213.md):

<a name="genesis_21_23"></a>Genesis 21:23

Now therefore [shaba'](../../strongs/h/h7650.md) unto me here by ['Elohiym](../../strongs/h/h430.md) that thou wilt not [šāqar](../../strongs/h/h8266.md) with me, nor with my [nîn](../../strongs/h/h5209.md), nor with my [neḵeḏ](../../strongs/h/h5220.md): but according to the [checed](../../strongs/h/h2617.md) that I have ['asah](../../strongs/h/h6213.md) unto thee, thou shalt ['asah](../../strongs/h/h6213.md) unto me, and to the ['erets](../../strongs/h/h776.md) wherein thou hast [guwr](../../strongs/h/h1481.md).

<a name="genesis_21_24"></a>Genesis 21:24

And ['Abraham](../../strongs/h/h85.md) ['āmar](../../strongs/h/h559.md), I will [shaba'](../../strongs/h/h7650.md).

<a name="genesis_21_25"></a>Genesis 21:25

And ['Abraham](../../strongs/h/h85.md) [yakach](../../strongs/h/h3198.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md) because of a [bᵊ'ēr](../../strongs/h/h875.md) of [mayim](../../strongs/h/h4325.md), which ['Ăḇîmeleḵ](../../strongs/h/h40.md) ['ebed](../../strongs/h/h5650.md) had [gāzal](../../strongs/h/h1497.md).

<a name="genesis_21_26"></a>Genesis 21:26

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) ['āmar](../../strongs/h/h559.md), I [yada'](../../strongs/h/h3045.md) not who hath ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md); neither didst thou [nāḡaḏ](../../strongs/h/h5046.md) me, neither yet [shama'](../../strongs/h/h8085.md) I of it, but to [yowm](../../strongs/h/h3117.md).

<a name="genesis_21_27"></a>Genesis 21:27

And ['Abraham](../../strongs/h/h85.md) [laqach](../../strongs/h/h3947.md) [tso'n](../../strongs/h/h6629.md) and [bāqār](../../strongs/h/h1241.md), and [nathan](../../strongs/h/h5414.md) them unto ['Ăḇîmeleḵ](../../strongs/h/h40.md); and both of them [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md).

<a name="genesis_21_28"></a>Genesis 21:28

And ['Abraham](../../strongs/h/h85.md) [nāṣaḇ](../../strongs/h/h5324.md) seven ewe [kiḇśâ](../../strongs/h/h3535.md) of the [tso'n](../../strongs/h/h6629.md) by themselves.

<a name="genesis_21_29"></a>Genesis 21:29

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) ['āmar](../../strongs/h/h559.md) unto ['Abraham](../../strongs/h/h85.md), What mean these seven [kiḇśâ](../../strongs/h/h3535.md) which thou hast [nāṣaḇ](../../strongs/h/h5324.md) by themselves?

<a name="genesis_21_30"></a>Genesis 21:30

And he ['āmar](../../strongs/h/h559.md), For these seven [kiḇśâ](../../strongs/h/h3535.md) shalt thou [laqach](../../strongs/h/h3947.md) of my [yad](../../strongs/h/h3027.md), that they may be a [ʿēḏâ](../../strongs/h/h5713.md) unto me, that I have [chaphar](../../strongs/h/h2658.md) this [bᵊ'ēr](../../strongs/h/h875.md).

<a name="genesis_21_31"></a>Genesis 21:31

Wherefore he [qara'](../../strongs/h/h7121.md) that [maqowm](../../strongs/h/h4725.md) [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md); because there they [shaba'](../../strongs/h/h7650.md) both of them.

<a name="genesis_21_32"></a>Genesis 21:32

Thus they [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) at [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md): then ['Ăḇîmeleḵ](../../strongs/h/h40.md) [quwm](../../strongs/h/h6965.md), and [pîḵōl](../../strongs/h/h6369.md) the [śar](../../strongs/h/h8269.md) of his [tsaba'](../../strongs/h/h6635.md), and they [shuwb](../../strongs/h/h7725.md) into the ['erets](../../strongs/h/h776.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="genesis_21_33"></a>Genesis 21:33

And [nāṭaʿ](../../strongs/h/h5193.md) an ['ēšel](../../strongs/h/h815.md) in [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md), and [qara'](../../strongs/h/h7121.md) there on the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), the ['owlam](../../strongs/h/h5769.md) ['el](../../strongs/h/h410.md).

<a name="genesis_21_34"></a>Genesis 21:34

And ['Abraham](../../strongs/h/h85.md) [guwr](../../strongs/h/h1481.md) in the [Pᵊlištî](../../strongs/h/h6430.md) ['erets](../../strongs/h/h776.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 20](genesis_20.md) - [Genesis 22](genesis_22.md)