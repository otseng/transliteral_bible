# [Genesis 7](https://www.blueletterbible.org/kjv/gen/7/1/s_7001)

<a name="genesis_7_1"></a>Genesis 7:1

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Nōaḥ](../../strongs/h/h5146.md), [bow'](../../strongs/h/h935.md) thou and all thy [bayith](../../strongs/h/h1004.md) into the [tēḇâ](../../strongs/h/h8392.md); for thee have I [ra'ah](../../strongs/h/h7200.md) [tsaddiyq](../../strongs/h/h6662.md) [paniym](../../strongs/h/h6440.md) in this [dôr](../../strongs/h/h1755.md).

<a name="genesis_7_2"></a>Genesis 7:2

Of every [tahowr](../../strongs/h/h2889.md) [bĕhemah](../../strongs/h/h929.md) thou shalt [laqach](../../strongs/h/h3947.md) to thee by [šeḇaʿ](../../strongs/h/h7651.md) [šeḇaʿ](../../strongs/h/h7651.md), the ['iysh](../../strongs/h/h376.md) and his ['ishshah](../../strongs/h/h802.md): and of [bĕhemah](../../strongs/h/h929.md) that are not [tahowr](../../strongs/h/h2889.md) by [šᵊnayim](../../strongs/h/h8147.md), the ['iysh](../../strongs/h/h376.md) and his ['ishshah](../../strongs/h/h802.md).

<a name="genesis_7_3"></a>Genesis 7:3

Of [ʿôp̄](../../strongs/h/h5775.md) also of the [shamayim](../../strongs/h/h8064.md) by [šeḇaʿ](../../strongs/h/h7651.md) [šeḇaʿ](../../strongs/h/h7651.md), the [zāḵār](../../strongs/h/h2145.md) and the [nᵊqēḇâ](../../strongs/h/h5347.md); to [ḥāyâ](../../strongs/h/h2421.md) [zera'](../../strongs/h/h2233.md) upon the [paniym](../../strongs/h/h6440.md) of all the ['erets](../../strongs/h/h776.md).

<a name="genesis_7_4"></a>Genesis 7:4

For yet seven [yowm](../../strongs/h/h3117.md), and I will cause it to [matar](../../strongs/h/h4305.md) upon the ['erets](../../strongs/h/h776.md) forty [yowm](../../strongs/h/h3117.md) and forty [layil](../../strongs/h/h3915.md); and every [yᵊqûm](../../strongs/h/h3351.md) that I have ['asah](../../strongs/h/h6213.md) will I [māḥâ](../../strongs/h/h4229.md) from off the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="genesis_7_5"></a>Genesis 7:5

And [Nōaḥ](../../strongs/h/h5146.md) ['asah](../../strongs/h/h6213.md) according unto all that [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) him.

<a name="genesis_7_6"></a>Genesis 7:6

And [Nōaḥ](../../strongs/h/h5146.md) was six hundred [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when the [mabûl](../../strongs/h/h3999.md) of [mayim](../../strongs/h/h4325.md) was upon the ['erets](../../strongs/h/h776.md).

<a name="genesis_7_7"></a>Genesis 7:7

And [Nōaḥ](../../strongs/h/h5146.md) [bow'](../../strongs/h/h935.md), and his [ben](../../strongs/h/h1121.md), and his ['ishshah](../../strongs/h/h802.md), and his [ben](../../strongs/h/h1121.md) ['ishshah](../../strongs/h/h802.md) with him, into the [tēḇâ](../../strongs/h/h8392.md), [paniym](../../strongs/h/h6440.md) of the [mayim](../../strongs/h/h4325.md) of the [mabûl](../../strongs/h/h3999.md).

<a name="genesis_7_8"></a>Genesis 7:8

Of [tahowr](../../strongs/h/h2889.md) [bĕhemah](../../strongs/h/h929.md), and of [bĕhemah](../../strongs/h/h929.md) that are not [tahowr](../../strongs/h/h2889.md), and of [ʿôp̄](../../strongs/h/h5775.md), and of every thing that [rāmaś](../../strongs/h/h7430.md) upon the ['ăḏāmâ](../../strongs/h/h127.md),

<a name="genesis_7_9"></a>Genesis 7:9

There [bow'](../../strongs/h/h935.md) [šᵊnayim](../../strongs/h/h8147.md) and [šᵊnayim](../../strongs/h/h8147.md) unto [Nōaḥ](../../strongs/h/h5146.md) into the [tēḇâ](../../strongs/h/h8392.md), the [zāḵār](../../strongs/h/h2145.md) and the [nᵊqēḇâ](../../strongs/h/h5347.md), as ['Elohiym](../../strongs/h/h430.md) had [tsavah](../../strongs/h/h6680.md) [Nōaḥ](../../strongs/h/h5146.md).

<a name="genesis_7_10"></a>Genesis 7:10

And it came to pass after [šeḇaʿ](../../strongs/h/h7651.md) [yowm](../../strongs/h/h3117.md), that the [mayim](../../strongs/h/h4325.md) of the [mabûl](../../strongs/h/h3999.md) were upon the ['erets](../../strongs/h/h776.md).

<a name="genesis_7_11"></a>Genesis 7:11

In the six hundredth [šānâ](../../strongs/h/h8141.md) of [Nōaḥ](../../strongs/h/h5146.md) [chay](../../strongs/h/h2416.md), in the second [ḥōḏeš](../../strongs/h/h2320.md), the seventeenth [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md), the [zê](../../strongs/h/h2088.md) [yowm](../../strongs/h/h3117.md) were all the [maʿyān](../../strongs/h/h4599.md) of the [rab](../../strongs/h/h7227.md) [tĕhowm](../../strongs/h/h8415.md) [bāqaʿ](../../strongs/h/h1234.md), and the ['ărubâ](../../strongs/h/h699.md) of [shamayim](../../strongs/h/h8064.md) were [pāṯaḥ](../../strongs/h/h6605.md).

<a name="genesis_7_12"></a>Genesis 7:12

And the [gešem](../../strongs/h/h1653.md) was upon the ['erets](../../strongs/h/h776.md) ['arbāʿîm](../../strongs/h/h705.md) [yowm](../../strongs/h/h3117.md) and ['arbāʿîm](../../strongs/h/h705.md) [layil](../../strongs/h/h3915.md).

<a name="genesis_7_13"></a>Genesis 7:13

In the ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md) [Nōaḥ](../../strongs/h/h5146.md), and [Šēm](../../strongs/h/h8035.md), and [Ḥām](../../strongs/h/h2526.md), and [Yep̄eṯ](../../strongs/h/h3315.md), the [ben](../../strongs/h/h1121.md) of [Nōaḥ](../../strongs/h/h5146.md), and [Nōaḥ](../../strongs/h/h5146.md) ['ishshah](../../strongs/h/h802.md), and the [šālôš](../../strongs/h/h7969.md) ['ishshah](../../strongs/h/h802.md) of his [ben](../../strongs/h/h1121.md) with them, into the [tēḇâ](../../strongs/h/h8392.md);

<a name="genesis_7_14"></a>Genesis 7:14

They, and every [chay](../../strongs/h/h2416.md) after his [miyn](../../strongs/h/h4327.md), and all the [bĕhemah](../../strongs/h/h929.md) after their [miyn](../../strongs/h/h4327.md), and every [remeś](../../strongs/h/h7431.md) that [rāmaś](../../strongs/h/h7430.md) upon the ['erets](../../strongs/h/h776.md) after his [miyn](../../strongs/h/h4327.md), and every [ʿôp̄](../../strongs/h/h5775.md) after his [miyn](../../strongs/h/h4327.md), every [tsippowr](../../strongs/h/h6833.md) of every [kanaph](../../strongs/h/h3671.md).

<a name="genesis_7_15"></a>Genesis 7:15

And they [bow'](../../strongs/h/h935.md) unto [Nōaḥ](../../strongs/h/h5146.md) into the [tēḇâ](../../strongs/h/h8392.md), [šᵊnayim](../../strongs/h/h8147.md) and [šᵊnayim](../../strongs/h/h8147.md) of all [basar](../../strongs/h/h1320.md), wherein is the [ruwach](../../strongs/h/h7307.md) of [chay](../../strongs/h/h2416.md).

<a name="genesis_7_16"></a>Genesis 7:16

And they that [bow'](../../strongs/h/h935.md) in, [bow'](../../strongs/h/h935.md) in [zāḵār](../../strongs/h/h2145.md) and [nᵊqēḇâ](../../strongs/h/h5347.md) of all [basar](../../strongs/h/h1320.md), as ['Elohiym](../../strongs/h/h430.md) had [tsavah](../../strongs/h/h6680.md) him: and [Yĕhovah](../../strongs/h/h3068.md) [cagar](../../strongs/h/h5462.md) him in.

<a name="genesis_7_17"></a>Genesis 7:17

And the [mabûl](../../strongs/h/h3999.md) was ['arbāʿîm](../../strongs/h/h705.md) [yowm](../../strongs/h/h3117.md) upon the ['erets](../../strongs/h/h776.md); and the [mayim](../../strongs/h/h4325.md) [rabah](../../strongs/h/h7235.md), and [nasa'](../../strongs/h/h5375.md) the [tēḇâ](../../strongs/h/h8392.md), and it was [ruwm](../../strongs/h/h7311.md) above the ['erets](../../strongs/h/h776.md).

<a name="genesis_7_18"></a>Genesis 7:18

And the [mayim](../../strongs/h/h4325.md) [gabar](../../strongs/h/h1396.md), and were [rabah](../../strongs/h/h7235.md) [me'od](../../strongs/h/h3966.md) upon the ['erets](../../strongs/h/h776.md); and the [tēḇâ](../../strongs/h/h8392.md) [yālaḵ](../../strongs/h/h3212.md) upon the [paniym](../../strongs/h/h6440.md) of the [mayim](../../strongs/h/h4325.md).

<a name="genesis_7_19"></a>Genesis 7:19

And the [mayim](../../strongs/h/h4325.md) [gabar](../../strongs/h/h1396.md) [me'od](../../strongs/h/h3966.md) [me'od](../../strongs/h/h3966.md) upon the ['erets](../../strongs/h/h776.md); and all the [gāḇōha](../../strongs/h/h1364.md) [har](../../strongs/h/h2022.md), that were [taḥaṯ](../../strongs/h/h8478.md) the [shamayim](../../strongs/h/h8064.md), were [kāsâ](../../strongs/h/h3680.md).

<a name="genesis_7_20"></a>Genesis 7:20

Fifteen ['ammâ](../../strongs/h/h520.md) [maʿal](../../strongs/h/h4605.md) did the [mayim](../../strongs/h/h4325.md) [gabar](../../strongs/h/h1396.md); and the [har](../../strongs/h/h2022.md) were [kāsâ](../../strongs/h/h3680.md).

<a name="genesis_7_21"></a>Genesis 7:21

And all [basar](../../strongs/h/h1320.md) [gāvaʿ](../../strongs/h/h1478.md) that [rāmaś](../../strongs/h/h7430.md) upon the ['erets](../../strongs/h/h776.md), both of [ʿôp̄](../../strongs/h/h5775.md), and of [bĕhemah](../../strongs/h/h929.md), and of [chay](../../strongs/h/h2416.md), and of every [šereṣ](../../strongs/h/h8318.md) that [šāraṣ](../../strongs/h/h8317.md) upon the ['erets](../../strongs/h/h776.md), and every ['adam](../../strongs/h/h120.md):

<a name="genesis_7_22"></a>Genesis 7:22

All in whose ['aph](../../strongs/h/h639.md) was the [neshamah](../../strongs/h/h5397.md) [ruwach](../../strongs/h/h7307.md) of [chay](../../strongs/h/h2416.md), of all that was in the [ḥārāḇâ](../../strongs/h/h2724.md), [muwth](../../strongs/h/h4191.md).

<a name="genesis_7_23"></a>Genesis 7:23

And every [yᵊqûm](../../strongs/h/h3351.md) was [māḥâ](../../strongs/h/h4229.md) which was upon the [paniym](../../strongs/h/h6440.md) of the ['adamah](../../strongs/h/h127.md), both ['adam](../../strongs/h/h120.md), and [bĕhemah](../../strongs/h/h929.md), and the [remeś](../../strongs/h/h7431.md), and the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md); and they were [māḥâ](../../strongs/h/h4229.md) from the ['erets](../../strongs/h/h776.md): and [Nōaḥ](../../strongs/h/h5146.md) only [šā'ar](../../strongs/h/h7604.md), and they that were with him in the [tēḇâ](../../strongs/h/h8392.md).

<a name="genesis_7_24"></a>Genesis 7:24

And the [mayim](../../strongs/h/h4325.md) [gabar](../../strongs/h/h1396.md) upon the ['erets](../../strongs/h/h776.md) an hundred and fifty [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 6](genesis_6.md) - [Genesis 8](genesis_8.md)