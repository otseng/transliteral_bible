# [Genesis 3](https://www.blueletterbible.org/lxx/genesis/3)

<a name="genesis_3_1"></a>Genesis 3:1

But the [ophis](../../strongs/g/g3789.md) was most [phronimos](../../strongs/g/g5429.md) of all the [thērion](../../strongs/g/g2342.md), of the ones upon the [gē](../../strongs/g/g1093.md) whom [poieō](../../strongs/g/g4160.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md). And [eipon](../../strongs/g/g2036.md) the [ophis](../../strongs/g/g3789.md) to the [gynē](../../strongs/g/g1135.md), For why [eipon](../../strongs/g/g2036.md) [theos](../../strongs/g/g2316.md), No way should you [esthiō](../../strongs/g/g2068.md) from all of a [xylon](../../strongs/g/g3586.md) of the [paradeisos](../../strongs/g/g3857.md)? 

<a name="genesis_3_2"></a>Genesis 3:2

And [eipon](../../strongs/g/g2036.md) the [gynē](../../strongs/g/g1135.md), From [karpos](../../strongs/g/g2590.md) of the [xylon](../../strongs/g/g3586.md) of the [paradeisos](../../strongs/g/g3857.md) we shall [esthiō](../../strongs/g/g2068.md); 

<a name="genesis_3_3"></a>Genesis 3:3

but from the [karpos](../../strongs/g/g2590.md) of the [xylon](../../strongs/g/g3586.md) which is in the [mesos](../../strongs/g/g3319.md) of the [paradeisos](../../strongs/g/g3857.md), [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md), [esthiō](../../strongs/g/g2068.md) not from it, nor [haptomai](../../strongs/g/g680.md) it! that you should not [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_3_4"></a>Genesis 3:4

And [eipon](../../strongs/g/g2036.md) the [ophis](../../strongs/g/g3789.md) to the [gynē](../../strongs/g/g1135.md), Not to [thanatos](../../strongs/g/g2288.md) will you [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_3_5"></a>Genesis 3:5

[eidō](../../strongs/g/g1492.md) for [theos](../../strongs/g/g2316.md) that in which ever [hēmera](../../strongs/g/g2250.md) you should [esthiō](../../strongs/g/g2068.md) of it, will be [dianoigō](../../strongs/g/g1272.md) your [ophthalmos](../../strongs/g/g3788.md), and you will be as [theos](../../strongs/g/g2316.md), [ginōskō](../../strongs/g/g1097.md) [kalos](../../strongs/g/g2570.md) and [ponēros](../../strongs/g/g4190.md). 

<a name="genesis_3_6"></a>Genesis 3:6

And [eidō](../../strongs/g/g1492.md) the [gynē](../../strongs/g/g1135.md) that is [kalos](../../strongs/g/g2570.md) the [xylon](../../strongs/g/g3586.md) for [brōsis](../../strongs/g/g1035.md), and that it is [arestos](../../strongs/g/g701.md) to the [ophthalmos](../../strongs/g/g3788.md) to [eidō](../../strongs/g/g1492.md), and is [hōraios](../../strongs/g/g5611.md) for [katanoeō](../../strongs/g/g2657.md). And having [lambanō](../../strongs/g/g2983.md) the [karpos](../../strongs/g/g2590.md) of it, she [esthiō](../../strongs/g/g2068.md), and she [didōmi](../../strongs/g/g1325.md) also to her [anēr](../../strongs/g/g435.md) with her, and they [esthiō](../../strongs/g/g2068.md). 

<a name="genesis_3_7"></a>Genesis 3:7

And were [dianoigō](../../strongs/g/g1272.md) the [ophthalmos](../../strongs/g/g3788.md) of the [dyo](../../strongs/g/g1417.md), and they [ginōskō](../../strongs/g/g1097.md) that they were [gymnos](../../strongs/g/g1131.md). And they sewed (errapsan) [phyllon](../../strongs/g/g5444.md) of a [sykē](../../strongs/g/g4808.md), and [poieō](../../strongs/g/g4160.md) to themselves loincloths (perizōmata).

<a name="genesis_3_8"></a>Genesis 3:8

And they [akouō](../../strongs/g/g191.md) the [phōnē](../../strongs/g/g5456.md) of the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) [peripateō](../../strongs/g/g4043.md) in the [paradeisos](../../strongs/g/g3857.md) at dusk (deilinon). And [kryptō](../../strongs/g/g2928.md) both [Adam](../../strongs/g/g76.md) and his [gynē](../../strongs/g/g1135.md) from the [prosōpon](../../strongs/g/g4383.md) of the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) in the [mesos](../../strongs/g/g3319.md) of the [xylon](../../strongs/g/g3586.md) of the [paradeisos](../../strongs/g/g3857.md). 

<a name="genesis_3_9"></a>Genesis 3:9

And [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) [kaleō](../../strongs/g/g2564.md) [Adam](../../strongs/g/g76.md), and [eipon](../../strongs/g/g2036.md) to him, [Adam](../../strongs/g/g76.md), Where are you?

<a name="genesis_3_10"></a>Genesis 3:10

And he [eipon](../../strongs/g/g2036.md) to him, your [phōnē](../../strongs/g/g5456.md) I [akouō](../../strongs/g/g191.md) while [peripateō](../../strongs/g/g4043.md) in the [paradeisos](../../strongs/g/g3857.md), and I [phobeō](../../strongs/g/g5399.md), for I am [gymnos](../../strongs/g/g1131.md), and I [kryptō](../../strongs/g/g2928.md). 

<a name="genesis_3_11"></a>Genesis 3:11

And [eipon](../../strongs/g/g2036.md) to him [theos](../../strongs/g/g2316.md), Who [anaggellō](../../strongs/g/g312.md) to you that you are [gymnos](../../strongs/g/g1131.md), unless from the [xylon](../../strongs/g/g3586.md) of which I [entellō](../../strongs/g/g1781.md) to you, saying , This [monos](../../strongs/g/g3441.md) you are not to [esthiō](../../strongs/g/g2068.md) from it -- you [esthiō](../../strongs/g/g2068.md). 

<a name="genesis_3_12"></a>Genesis 3:12

And [Adam](../../strongs/g/g76.md) [eipon](../../strongs/g/g2036.md), The [gynē](../../strongs/g/g1135.md) whom you [didōmi](../../strongs/g/g1325.md) to be with me, she [didōmi](../../strongs/g/g1325.md) to me from the [xylon](../../strongs/g/g3586.md), and I [esthiō](../../strongs/g/g2068.md). 

<a name="genesis_3_13"></a>Genesis 3:13

And [eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) to the [gynē](../../strongs/g/g1135.md), What is this you [poieō](../../strongs/g/g4160.md)? And [eipon](../../strongs/g/g2036.md) the [gynē](../../strongs/g/g1135.md), The [ophis](../../strongs/g/g3789.md) [apataō](../../strongs/g/g538.md) me, and I [esthiō](../../strongs/g/g2068.md). 

<a name="genesis_3_14"></a>Genesis 3:14

And [eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) to the [ophis](../../strongs/g/g3789.md), Because you [poieō](../../strongs/g/g4160.md) this, [epikataratos](../../strongs/g/g1944.md) are you from all the [ktēnos](../../strongs/g/g2934.md), and from all the [thērion](../../strongs/g/g2342.md) of the ones upon the [gē](../../strongs/g/g1093.md). Upon your [stēthos](../../strongs/g/g4738.md) and [koilia](../../strongs/g/g2836.md) you shall [poreuō](../../strongs/g/g4198.md), and [gē](../../strongs/g/g1093.md) you shall [esthiō](../../strongs/g/g2068.md) all the [hēmera](../../strongs/g/g2250.md) of your [zōē](../../strongs/g/g2222.md).

<a name="genesis_3_15"></a>Genesis 3:15

And [echthra](../../strongs/g/g2189.md) I will [tithēmi](../../strongs/g/g5087.md) between you and between the [gynē](../../strongs/g/g1135.md); and between your [sperma](../../strongs/g/g4690.md) and between her [sperma](../../strongs/g/g4690.md). He will [tēreō](../../strongs/g/g5083.md) to your [kephalē](../../strongs/g/g2776.md), and you will [tēreō](../../strongs/g/g5083.md) his [pterna](../../strongs/g/g4418.md). 

<a name="genesis_3_16"></a>Genesis 3:16

And to the [gynē](../../strongs/g/g1135.md) he [eipon](../../strongs/g/g2036.md), In [plēthynō](../../strongs/g/g4129.md) I will [plēthynō](../../strongs/g/g4129.md) your [lypē](../../strongs/g/g3077.md), and your [stenagmos](../../strongs/g/g4726.md). In [lypē](../../strongs/g/g3077.md) you will [tiktō](../../strongs/g/g5088.md) [teknon](../../strongs/g/g5043.md), and to your [anēr](../../strongs/g/g435.md) your submission (apostrophē), and he will [kyrieuō](../../strongs/g/g2961.md) you.

<a name="genesis_3_17"></a>Genesis 3:17

And to [Adam](../../strongs/g/g76.md) he [eipon](../../strongs/g/g2036.md), Because you [akouō](../../strongs/g/g191.md) the [phōnē](../../strongs/g/g5456.md) of your [gynē](../../strongs/g/g1135.md), and [esthiō](../../strongs/g/g2068.md) from the [xylon](../../strongs/g/g3586.md) of which I [entellō](../../strongs/g/g1781.md) to you, saying , This [monos](../../strongs/g/g3441.md) you are not to [esthiō](../../strongs/g/g2068.md) from it -- and you [esthiō](../../strongs/g/g2068.md); [epikataratos](../../strongs/g/g1944.md) is the [gē](../../strongs/g/g1093.md) among your [ergon](../../strongs/g/g2041.md); in [lypē](../../strongs/g/g3077.md) you will [esthiō](../../strongs/g/g2068.md) it all the [hēmera](../../strongs/g/g2250.md) of your [zōē](../../strongs/g/g2222.md).

<a name="genesis_3_18"></a>Genesis 3:18

[akantha](../../strongs/g/g173.md) and [tribolos](../../strongs/g/g5146.md) will [anatellō](../../strongs/g/g393.md) to you, and you will [esthiō](../../strongs/g/g2068.md) the [chortos](../../strongs/g/g5528.md) of the [agros](../../strongs/g/g68.md). 

<a name="genesis_3_19"></a>Genesis 3:19

By [hidrōs](../../strongs/g/g2402.md) of your [prosōpon](../../strongs/g/g4383.md) you will [esthiō](../../strongs/g/g2068.md) your [artos](../../strongs/g/g740.md), until the [apostrephō](../../strongs/g/g654.md) you into the [gē](../../strongs/g/g1093.md) from out of which you were [lambanō](../../strongs/g/g2983.md). For [gē](../../strongs/g/g1093.md) you are and unto [gē](../../strongs/g/g1093.md) you will [aperchomai](../../strongs/g/g565.md). 

<a name="genesis_3_20"></a>Genesis 3:20

And [Adam](../../strongs/g/g76.md) [kaleō](../../strongs/g/g2564.md) the [onoma](../../strongs/g/g3686.md) of his [gynē](../../strongs/g/g1135.md), [zōē](../../strongs/g/g2222.md), for she was [mētēr](../../strongs/g/g3384.md) of all the [zaō](../../strongs/g/g2198.md). 

<a name="genesis_3_21"></a>Genesis 3:21

And [poieō](../../strongs/g/g4160.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) to [Adam](../../strongs/g/g76.md) and his [gynē](../../strongs/g/g1135.md) [chitōn](../../strongs/g/g5509.md) of [dermatinos](../../strongs/g/g1193.md), and he [endyō](../../strongs/g/g1746.md) them. 

<a name="genesis_3_22"></a>Genesis 3:22

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), [Adam](../../strongs/g/g76.md) has [ginomai](../../strongs/g/g1096.md) as one of us, to [ginōskō](../../strongs/g/g1097.md) [kalos](../../strongs/g/g2570.md) and [ponēros](../../strongs/g/g4190.md). And now, lest at any time he might [ekteinō](../../strongs/g/g1614.md) the [cheir](../../strongs/g/g5495.md), and should [lambanō](../../strongs/g/g2983.md) from the [xylon](../../strongs/g/g3586.md) of [zōē](../../strongs/g/g2222.md), and should [esthiō](../../strongs/g/g2068.md), and will [zaō](../../strongs/g/g2198.md) into the [aiōn](../../strongs/g/g165.md)

<a name="genesis_3_23"></a>Genesis 3:23

that [exapostellō](../../strongs/g/g1821.md) him the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) from the [paradeisos](../../strongs/g/g3857.md) of the [tryphē](../../strongs/g/g5172.md), to [ergazomai](../../strongs/g/g2038.md) the [gē](../../strongs/g/g1093.md) from which he was [lambanō](../../strongs/g/g2983.md). 

<a name="genesis_3_24"></a>Genesis 3:24

And he [ekballō](../../strongs/g/g1544.md) [Adam](../../strongs/g/g76.md), and [katoikeō](../../strongs/g/g2730.md) him [apenanti](../../strongs/g/g561.md) the [paradeisos](../../strongs/g/g3857.md) of the [tryphē](../../strongs/g/g5172.md), and [tassō](../../strongs/g/g5021.md) the [cheroub](../../strongs/g/g5502.md), and the flaming (phloginēn) [rhomphaia](../../strongs/g/g4501.md) [strephō](../../strongs/g/g4762.md), to [phylassō](../../strongs/g/g5442.md) the [hodos](../../strongs/g/g3598.md) of the [xylon](../../strongs/g/g3586.md) of [zōē](../../strongs/g/g2222.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 2](genesis_lxx_2.md) - [Genesis 4](genesis_lxx_4.md)