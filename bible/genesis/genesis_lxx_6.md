# [Genesis 6](https://www.blueletterbible.org/lxx/genesis/6)

<a name="genesis_6_1"></a>Genesis 6:1

And [ginomai](../../strongs/g/g1096.md) when [archomai](../../strongs/g/g756.md) [anthrōpos](../../strongs/g/g444.md) [polys](../../strongs/g/g4183.md) to [ginomai](../../strongs/g/g1096.md) upon the [gē](../../strongs/g/g1093.md), and [thygatēr](../../strongs/g/g2364.md) were [gennaō](../../strongs/g/g1080.md) to them. 

<a name="genesis_6_2"></a>Genesis 6:2

were [eidō](../../strongs/g/g1492.md) and the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md) the [thygatēr](../../strongs/g/g2364.md) of [anthrōpos](../../strongs/g/g444.md), that they are [kalos](../../strongs/g/g2570.md), that they [lambanō](../../strongs/g/g2983.md) to themselves [gynē](../../strongs/g/g1135.md) from all of whom they [eklegomai](../../strongs/g/g1586.md). 

<a name="genesis_6_3"></a>Genesis 6:3

And [eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md), No way should [katamenō](../../strongs/g/g2650.md) my [pneuma](../../strongs/g/g4151.md) with these [anthrōpos](../../strongs/g/g444.md), on account of their being [sarx](../../strongs/g/g4561.md); will be and their [hēmera](../../strongs/g/g2250.md) a [hekaton](../../strongs/g/g1540.md) [eikosi](../../strongs/g/g1501.md) [etos](../../strongs/g/g2094.md). 

<a name="genesis_6_4"></a>Genesis 6:4

And the giants (gigantes) were upon the [gē](../../strongs/g/g1093.md) in those [hēmera](../../strongs/g/g2250.md). And after that, [eisporeuomai](../../strongs/g/g1531.md) the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md) to the [thygatēr](../../strongs/g/g2364.md) of [anthrōpos](../../strongs/g/g444.md), and [gennaō](../../strongs/g/g1080.md) for themselves. Those were the giants (gigantes), the ones from the [aiōn](../../strongs/g/g165.md), the [anthrōpos](../../strongs/g/g444.md) renowned (onomastoi).

<a name="genesis_6_5"></a>Genesis 6:5

[eidō](../../strongs/g/g1492.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) that were [plēthynō](../../strongs/g/g4129.md) the [kakia](../../strongs/g/g2549.md) of [anthrōpos](../../strongs/g/g444.md) upon the [gē](../../strongs/g/g1093.md), and all that [tis](../../strongs/g/g5100.md) considered (dianoeitai) in his [kardia](../../strongs/g/g2588.md) was [epimelōs](../../strongs/g/g1960.md) upon the [ponēros](../../strongs/g/g4190.md) all the [hēmera](../../strongs/g/g2250.md), 

<a name="genesis_6_6"></a>Genesis 6:6

and [theos](../../strongs/g/g2316.md) [enthymeomai](../../strongs/g/g1760.md) that he [poieō](../../strongs/g/g4160.md) the [anthrōpos](../../strongs/g/g444.md) upon the [gē](../../strongs/g/g1093.md), and he considered (dienoēthē) it.

<a name="genesis_6_7"></a>Genesis 6:7

And [eipon](../../strongs/g/g2036.md) [theos](../../strongs/g/g2316.md), I will wipe away (apaleipsō) the [anthrōpos](../../strongs/g/g444.md), whom I [poieō](../../strongs/g/g4160.md), from the [prosōpon](../../strongs/g/g4383.md) of the [gē](../../strongs/g/g1093.md); from [anthrōpos](../../strongs/g/g444.md) unto [ktēnos](../../strongs/g/g2934.md), and from the [herpeton](../../strongs/g/g2062.md) unto the [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md); for I [metamelomai](../../strongs/g/g3338.md) that I [poieō](../../strongs/g/g4160.md) them. 

<a name="genesis_6_8"></a>Genesis 6:8

But [Nōe](../../strongs/g/g3575.md) [heuriskō](../../strongs/g/g2147.md) [charis](../../strongs/g/g5485.md) [enantion](../../strongs/g/g1726.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md). 

<a name="genesis_6_9"></a>Genesis 6:9

And these are the [genesis](../../strongs/g/g1078.md) of [Nōe](../../strongs/g/g3575.md). [Nōe](../../strongs/g/g3575.md) [anthrōpos](../../strongs/g/g444.md) was a [dikaios](../../strongs/g/g1342.md) [eimi](../../strongs/g/g1510.md) [teleios](../../strongs/g/g5046.md) in his [genea](../../strongs/g/g1074.md); to [theos](../../strongs/g/g2316.md) was [euaresteō](../../strongs/g/g2100.md) [Nōe](../../strongs/g/g3575.md).

<a name="genesis_6_10"></a>Genesis 6:10

[gennaō](../../strongs/g/g1080.md) [Nōe](../../strongs/g/g3575.md) [treis](../../strongs/g/g5140.md) [huios](../../strongs/g/g5207.md), [Sēm](../../strongs/g/g4590.md), Ham, Japheth.

<a name="genesis_6_11"></a>Genesis 6:11

was [phtheirō](../../strongs/g/g5351.md) the [gē](../../strongs/g/g1093.md) [enantion](../../strongs/g/g1726.md) [theos](../../strongs/g/g2316.md), and was [pimplēmi](../../strongs/g/g4130.md) the [gē](../../strongs/g/g1093.md) with [adikia](../../strongs/g/g93.md). 

<a name="genesis_6_12"></a>Genesis 6:12

And [eidō](../../strongs/g/g1492.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) the [gē](../../strongs/g/g1093.md), and it was being [kataphtheirō](../../strongs/g/g2704.md), for [kataphtheirō](../../strongs/g/g2704.md) all [sarx](../../strongs/g/g4561.md) his [hodos](../../strongs/g/g3598.md) upon the [gē](../../strongs/g/g1093.md). 

<a name="genesis_6_13"></a>Genesis 6:13

And [eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) to [Nōe](../../strongs/g/g3575.md), [kairos](../../strongs/g/g2540.md) for every [anthrōpos](../../strongs/g/g444.md) [hēkō](../../strongs/g/g2240.md) [enantion](../../strongs/g/g1726.md) me; for is [pimplēmi](../../strongs/g/g4130.md) the [gē](../../strongs/g/g1093.md) with [adikia](../../strongs/g/g93.md) by means of them. And [idou](../../strongs/g/g2400.md), I [kataphtheirō](../../strongs/g/g2704.md) them, and the [gē](../../strongs/g/g1093.md). 

<a name="genesis_6_14"></a>Genesis 6:14

[poieō](../../strongs/g/g4160.md) then for yourself an [kibōtos](../../strongs/g/g2787.md) from [xylon](../../strongs/g/g3586.md) [tetragōnos](../../strongs/g/g5068.md)! [nossia](../../strongs/g/g3555.md) you shall [poieō](../../strongs/g/g4160.md) among the [kibōtos](../../strongs/g/g2787.md), and you shall apply asphalt (asphaltōseis) to it -- from [esōthen](../../strongs/g/g2081.md) and from [exōthen](../../strongs/g/g1855.md) with the asphalt (asphaltō).

<a name="genesis_6_15"></a>Genesis 6:15

And so you shall [poieō](../../strongs/g/g4160.md) the [kibōtos](../../strongs/g/g2787.md) [triakosioi](../../strongs/g/g5145.md) [pēchys](../../strongs/g/g4083.md) the [mēkos](../../strongs/g/g3372.md) of the [kibōtos](../../strongs/g/g2787.md), and [pentēkonta](../../strongs/g/g4004.md) [pēchys](../../strongs/g/g4083.md) the [platos](../../strongs/g/g4114.md), and [triakonta](../../strongs/g/g5144.md) [pēchys](../../strongs/g/g4083.md) the [hypsos](../../strongs/g/g5311.md) of it. 

<a name="genesis_6_16"></a>Genesis 6:16

By an [episynagō](../../strongs/g/g1996.md), you shall [poieō](../../strongs/g/g4160.md) the [kibōtos](../../strongs/g/g2787.md); and by a [pēchys](../../strongs/g/g4083.md) you shall [synteleō](../../strongs/g/g4931.md) it from [anōthen](../../strongs/g/g509.md); but the [thyra](../../strongs/g/g2374.md) of the [kibōtos](../../strongs/g/g2787.md) you shall [poieō](../../strongs/g/g4160.md) from out of the side (plagiōn). with ground (katagaia) second stories (diōropha) and third stories (triōropha) You shall [poieō](../../strongs/g/g4160.md) it. 

<a name="genesis_6_17"></a>Genesis 6:17

And I, [idou](../../strongs/g/g2400.md), I [epagō](../../strongs/g/g1863.md) the [kataklysmos](../../strongs/g/g2627.md) of [hydōr](../../strongs/g/g5204.md) upon the [gē](../../strongs/g/g1093.md) to [kataphtheirō](../../strongs/g/g2704.md) all [sarx](../../strongs/g/g4561.md) in which is a [pneuma](../../strongs/g/g4151.md) of [zōē](../../strongs/g/g2222.md) [hypokatō](../../strongs/g/g5270.md) the [ouranos](../../strongs/g/g3772.md). And as much as might be upon the [gē](../../strongs/g/g1093.md) shall come to an [teleutaō](../../strongs/g/g5053.md). 

<a name="genesis_6_18"></a>Genesis 6:18

And I will [histēmi](../../strongs/g/g2476.md) my [diathēkē](../../strongs/g/g1242.md) with you. And you shall [eiserchomai](../../strongs/g/g1525.md) into the [kibōtos](../../strongs/g/g2787.md), you and your [huios](../../strongs/g/g5207.md), and your [gynē](../../strongs/g/g1135.md), and the [gynē](../../strongs/g/g1135.md) of your [huios](../../strongs/g/g5207.md) with you. 

<a name="genesis_6_19"></a>Genesis 6:19

And from all the [ktēnos](../../strongs/g/g2934.md), and from all the [herpeton](../../strongs/g/g2062.md), and from all the [thērion](../../strongs/g/g2342.md), and from all [sarx](../../strongs/g/g4561.md), [dyo](../../strongs/g/g1417.md) by [dyo](../../strongs/g/g1417.md) from all you shall [eisagō](../../strongs/g/g1521.md) into the [kibōtos](../../strongs/g/g2787.md), that you may [trephō](../../strongs/g/g5142.md) with yourself -- [arrēn](../../strongs/g/g730.md) and [thēlys](../../strongs/g/g2338.md) they shall be.

<a name="genesis_6_20"></a>Genesis 6:20

From all the [orneon](../../strongs/g/g3732.md) according to [genos](../../strongs/g/g1085.md), and from all the [ktēnos](../../strongs/g/g2934.md) according to [genos](../../strongs/g/g1085.md), and from all the [herpeton](../../strongs/g/g2062.md) crawling (herpontōn) upon the [gē](../../strongs/g/g1093.md) according to their [genos](../../strongs/g/g1085.md); [dyo](../../strongs/g/g1417.md) by [dyo](../../strongs/g/g1417.md) from all shall [eiserchomai](../../strongs/g/g1525.md) to you, to be [trephō](../../strongs/g/g5142.md) with you, [arrēn](../../strongs/g/g730.md) and [thēlys](../../strongs/g/g2338.md). 

<a name="genesis_6_21"></a>Genesis 6:21

But you shall [lambanō](../../strongs/g/g2983.md) to yourself from all of the [brōma](../../strongs/g/g1033.md) which you shall [esthiō](../../strongs/g/g2068.md), and you shall [synagō](../../strongs/g/g4863.md) them to yourself, and it shall be to you and to them to [esthiō](../../strongs/g/g2068.md). 

<a name="genesis_6_22"></a>Genesis 6:22

And [Nōe](../../strongs/g/g3575.md) [poieō](../../strongs/g/g4160.md) all as much as [entellō](../../strongs/g/g1781.md) to him the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) so he [poieō](../../strongs/g/g4160.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 5](genesis_lxx_5.md) - [Genesis 7](genesis_lxx_7.md)