# [Genesis 11](https://www.blueletterbible.org/kjv/gen/11/1/s_11001)

<a name="genesis_11_1"></a>Genesis 11:1

And the ['erets](../../strongs/h/h776.md) was of one [saphah](../../strongs/h/h8193.md), and of one [dabar](../../strongs/h/h1697.md).

<a name="genesis_11_2"></a>Genesis 11:2

And it came to pass, as they [nāsaʿ](../../strongs/h/h5265.md) from the [qeḏem](../../strongs/h/h6924.md), that they [māṣā'](../../strongs/h/h4672.md) a [biqʿâ](../../strongs/h/h1237.md) in the ['erets](../../strongs/h/h776.md) of [Šinʿār](../../strongs/h/h8152.md); and they [yashab](../../strongs/h/h3427.md) there.

<a name="genesis_11_3"></a>Genesis 11:3

And they ['āmar](../../strongs/h/h559.md) ['iysh](../../strongs/h/h376.md) to [rea'](../../strongs/h/h7453.md), [yāhaḇ](../../strongs/h/h3051.md), let us [lāḇan](../../strongs/h/h3835.md) [lᵊḇēnâ](../../strongs/h/h3843.md), and [śārap̄](../../strongs/h/h8313.md) [śᵊrēp̄â](../../strongs/h/h8316.md) them. And they had [lᵊḇēnâ](../../strongs/h/h3843.md) for ['eben](../../strongs/h/h68.md), and [ḥēmār](../../strongs/h/h2564.md) had they for [ḥōmer](../../strongs/h/h2563.md).

<a name="genesis_11_4"></a>Genesis 11:4

And they ['āmar](../../strongs/h/h559.md), [yāhaḇ](../../strongs/h/h3051.md), let us [bānâ](../../strongs/h/h1129.md) us an [ʿîr](../../strongs/h/h5892.md) and a [miḡdāl](../../strongs/h/h4026.md), whose [ro'sh](../../strongs/h/h7218.md) may reach unto [shamayim](../../strongs/h/h8064.md); and let us ['asah](../../strongs/h/h6213.md) us a [shem](../../strongs/h/h8034.md), lest we be [puwts](../../strongs/h/h6327.md) upon the [paniym](../../strongs/h/h6440.md) of the ['erets](../../strongs/h/h776.md).

<a name="genesis_11_5"></a>Genesis 11:5

And [Yĕhovah](../../strongs/h/h3068.md) [yarad](../../strongs/h/h3381.md) to [ra'ah](../../strongs/h/h7200.md) the [ʿîr](../../strongs/h/h5892.md) and the [miḡdāl](../../strongs/h/h4026.md), which the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md) [bānâ](../../strongs/h/h1129.md).

<a name="genesis_11_6"></a>Genesis 11:6

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), Behold, the ['am](../../strongs/h/h5971.md) is ['echad](../../strongs/h/h259.md), and they have all one [saphah](../../strongs/h/h8193.md); and this they [ḥālal](../../strongs/h/h2490.md) to ['asah](../../strongs/h/h6213.md): and now nothing will be [bāṣar](../../strongs/h/h1219.md) from them, which they have [zāmam](../../strongs/h/h2161.md) to ['asah](../../strongs/h/h6213.md).

<a name="genesis_11_7"></a>Genesis 11:7

[yāhaḇ](../../strongs/h/h3051.md), let us [yarad](../../strongs/h/h3381.md), and there [bālal](../../strongs/h/h1101.md) their [saphah](../../strongs/h/h8193.md), that they may not [shama'](../../strongs/h/h8085.md) ['iysh](../../strongs/h/h376.md) [rea'](../../strongs/h/h7453.md) [saphah](../../strongs/h/h8193.md).

<a name="genesis_11_8"></a>Genesis 11:8

So [Yĕhovah](../../strongs/h/h3068.md) [puwts](../../strongs/h/h6327.md) them from thence upon the [paniym](../../strongs/h/h6440.md) of all the ['erets](../../strongs/h/h776.md): and they [ḥāḏal](../../strongs/h/h2308.md) to [bānâ](../../strongs/h/h1129.md) the [ʿîr](../../strongs/h/h5892.md).

<a name="genesis_11_9"></a>Genesis 11:9

Therefore is the [shem](../../strongs/h/h8034.md) of it [qara'](../../strongs/h/h7121.md) [Bāḇel](../../strongs/h/h894.md); because [Yĕhovah](../../strongs/h/h3068.md) did there [bālal](../../strongs/h/h1101.md) the [saphah](../../strongs/h/h8193.md) of all the ['erets](../../strongs/h/h776.md): and from thence did [Yĕhovah](../../strongs/h/h3068.md) [puwts](../../strongs/h/h6327.md) them upon the [paniym](../../strongs/h/h6440.md) of all the ['erets](../../strongs/h/h776.md).

<a name="genesis_11_10"></a>Genesis 11:10

These are the [towlĕdah](../../strongs/h/h8435.md) of [Šēm](../../strongs/h/h8035.md): [Šēm](../../strongs/h/h8035.md) was an hundred [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), and [yalad](../../strongs/h/h3205.md) ['Arpaḵšaḏ](../../strongs/h/h775.md) two [šānâ](../../strongs/h/h8141.md) ['aḥar](../../strongs/h/h310.md) the [mabûl](../../strongs/h/h3999.md):

<a name="genesis_11_11"></a>Genesis 11:11

And [Šēm](../../strongs/h/h8035.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) ['Arpaḵšaḏ](../../strongs/h/h775.md) five hundred [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md).

<a name="genesis_11_12"></a>Genesis 11:12

And ['Arpaḵšaḏ](../../strongs/h/h775.md) [chayay](../../strongs/h/h2425.md) five and thirty [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [Šelaḥ](../../strongs/h/h7974.md):

<a name="genesis_11_13"></a>Genesis 11:13

And ['Arpaḵšaḏ](../../strongs/h/h775.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [Šelaḥ](../../strongs/h/h7974.md) four hundred and three [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md).

<a name="genesis_11_14"></a>Genesis 11:14

And [Šelaḥ](../../strongs/h/h7974.md) [chayay](../../strongs/h/h2425.md) thirty [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ʿēḇer](../../strongs/h/h5677.md):

<a name="genesis_11_15"></a>Genesis 11:15

And [Šelaḥ](../../strongs/h/h7974.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [ʿēḇer](../../strongs/h/h5677.md) four hundred and three [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md).

<a name="genesis_11_16"></a>Genesis 11:16

And [ʿēḇer](../../strongs/h/h5677.md) [ḥāyâ](../../strongs/h/h2421.md) four and thirty [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [Peleḡ](../../strongs/h/h6389.md):

<a name="genesis_11_17"></a>Genesis 11:17

And [ʿēḇer](../../strongs/h/h5677.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [Peleḡ](../../strongs/h/h6389.md) four hundred and thirty [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md).

<a name="genesis_11_18"></a>Genesis 11:18

And [Peleḡ](../../strongs/h/h6389.md) [ḥāyâ](../../strongs/h/h2421.md) thirty [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [Rᵊʿû](../../strongs/h/h7466.md):

<a name="genesis_11_19"></a>Genesis 11:19

And [Peleḡ](../../strongs/h/h6389.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [Rᵊʿû](../../strongs/h/h7466.md) two hundred and nine [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md).

<a name="genesis_11_20"></a>Genesis 11:20

And [Rᵊʿû](../../strongs/h/h7466.md) [ḥāyâ](../../strongs/h/h2421.md) two and thirty [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [Śᵊrûḡ](../../strongs/h/h8286.md):

<a name="genesis_11_21"></a>Genesis 11:21

And [Rᵊʿû](../../strongs/h/h7466.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [Śᵊrûḡ](../../strongs/h/h8286.md) two hundred and seven [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md).

<a name="genesis_11_22"></a>Genesis 11:22

And [Śᵊrûḡ](../../strongs/h/h8286.md) [ḥāyâ](../../strongs/h/h2421.md) thirty [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [Nāḥôr](../../strongs/h/h5152.md):

<a name="genesis_11_23"></a>Genesis 11:23

And [Śᵊrûḡ](../../strongs/h/h8286.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [Nāḥôr](../../strongs/h/h5152.md) two hundred [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md).

<a name="genesis_11_24"></a>Genesis 11:24

And [Nāḥôr](../../strongs/h/h5152.md) [ḥāyâ](../../strongs/h/h2421.md) nine and twenty [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [Teraḥ](../../strongs/h/h8646.md):

<a name="genesis_11_25"></a>Genesis 11:25

And [Nāḥôr](../../strongs/h/h5152.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [Teraḥ](../../strongs/h/h8646.md) an hundred and nineteen [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md).

<a name="genesis_11_26"></a>Genesis 11:26

And [Teraḥ](../../strongs/h/h8646.md) [ḥāyâ](../../strongs/h/h2421.md) seventy [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) ['Aḇrām](../../strongs/h/h87.md), [Nāḥôr](../../strongs/h/h5152.md), and [Hārān](../../strongs/h/h2039.md).

<a name="genesis_11_27"></a>Genesis 11:27

Now these are the [towlĕdah](../../strongs/h/h8435.md) of [Teraḥ](../../strongs/h/h8646.md): [Teraḥ](../../strongs/h/h8646.md) [yalad](../../strongs/h/h3205.md) ['Aḇrām](../../strongs/h/h87.md), [Nāḥôr](../../strongs/h/h5152.md), and [Hārān](../../strongs/h/h2039.md); and [Hārān](../../strongs/h/h2039.md) [yalad](../../strongs/h/h3205.md) [Lôṭ](../../strongs/h/h3876.md).

<a name="genesis_11_28"></a>Genesis 11:28

And [Hārān](../../strongs/h/h2039.md) [muwth](../../strongs/h/h4191.md) [paniym](../../strongs/h/h6440.md) his ['ab](../../strongs/h/h1.md) [Teraḥ](../../strongs/h/h8646.md) in the ['erets](../../strongs/h/h776.md) of his [môleḏeṯ](../../strongs/h/h4138.md), in ['Ûr](../../strongs/h/h218.md) of the [Kaśdîmâ](../../strongs/h/h3778.md).

<a name="genesis_11_29"></a>Genesis 11:29

And ['Aḇrām](../../strongs/h/h87.md) and [Nāḥôr](../../strongs/h/h5152.md) [laqach](../../strongs/h/h3947.md) them ['ishshah](../../strongs/h/h802.md): the [shem](../../strongs/h/h8034.md) of ['Aḇrām](../../strongs/h/h87.md) ['ishshah](../../strongs/h/h802.md) was [Śāray](../../strongs/h/h8297.md); and the [shem](../../strongs/h/h8034.md) of [Nāḥôr](../../strongs/h/h5152.md) ['ishshah](../../strongs/h/h802.md), [Milkâ](../../strongs/h/h4435.md), the [bath](../../strongs/h/h1323.md) of [Hārān](../../strongs/h/h2039.md), the ['ab](../../strongs/h/h1.md) of [Milkâ](../../strongs/h/h4435.md), and the ['ab](../../strongs/h/h1.md) of [yiskâ](../../strongs/h/h3252.md).

<a name="genesis_11_30"></a>Genesis 11:30

But [Śāray](../../strongs/h/h8297.md) was [ʿāqār](../../strongs/h/h6135.md); she had no [vālāḏ](../../strongs/h/h2056.md).

<a name="genesis_11_31"></a>Genesis 11:31

And [Teraḥ](../../strongs/h/h8646.md) [laqach](../../strongs/h/h3947.md) ['Aḇrām](../../strongs/h/h87.md) his [ben](../../strongs/h/h1121.md), and [Lôṭ](../../strongs/h/h3876.md) the [ben](../../strongs/h/h1121.md) of [Hārān](../../strongs/h/h2039.md) his [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md), and [Śāray](../../strongs/h/h8297.md) his [kallâ](../../strongs/h/h3618.md), his [ben](../../strongs/h/h1121.md) ['Aḇrām](../../strongs/h/h87.md) ['ishshah](../../strongs/h/h802.md); and they [yāṣā'](../../strongs/h/h3318.md) with them from ['Ûr](../../strongs/h/h218.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), to [yālaḵ](../../strongs/h/h3212.md) into the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md); and they [bow'](../../strongs/h/h935.md) unto [Ḥārān](../../strongs/h/h2771.md), and [yashab](../../strongs/h/h3427.md) there.

<a name="genesis_11_32"></a>Genesis 11:32

And the [yowm](../../strongs/h/h3117.md) of [Teraḥ](../../strongs/h/h8646.md) were two hundred and five [šānâ](../../strongs/h/h8141.md): and [Teraḥ](../../strongs/h/h8646.md) [muwth](../../strongs/h/h4191.md) in [Ḥārān](../../strongs/h/h2771.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 10](genesis_10.md) - [Genesis 12](genesis_12.md)