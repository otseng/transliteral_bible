# [Genesis 16](https://www.blueletterbible.org/kjv/gen/16/1/s_16001)

<a name="genesis_16_1"></a>Genesis 16:1

Now [Śāray](../../strongs/h/h8297.md) ['Aḇrām](../../strongs/h/h87.md) ['ishshah](../../strongs/h/h802.md) [yalad](../../strongs/h/h3205.md) him none: and she had a [šip̄ḥâ](../../strongs/h/h8198.md), a [Miṣrî](../../strongs/h/h4713.md), whose [shem](../../strongs/h/h8034.md) was [Hāḡār](../../strongs/h/h1904.md).

<a name="genesis_16_2"></a>Genesis 16:2

And [Śāray](../../strongs/h/h8297.md) ['āmar](../../strongs/h/h559.md) unto ['Aḇrām](../../strongs/h/h87.md), Behold now, [Yĕhovah](../../strongs/h/h3068.md) hath [ʿāṣar](../../strongs/h/h6113.md) me from [yalad](../../strongs/h/h3205.md): I pray thee, [bow'](../../strongs/h/h935.md) unto my [šip̄ḥâ](../../strongs/h/h8198.md); it may be that I may [bānâ](../../strongs/h/h1129.md) by her. And ['Aḇrām](../../strongs/h/h87.md) [shama'](../../strongs/h/h8085.md) to the [qowl](../../strongs/h/h6963.md) of [Śāray](../../strongs/h/h8297.md).

<a name="genesis_16_3"></a>Genesis 16:3

And [Śāray](../../strongs/h/h8297.md) ['Aḇrām](../../strongs/h/h87.md) ['ishshah](../../strongs/h/h802.md) [laqach](../../strongs/h/h3947.md) [Hāḡār](../../strongs/h/h1904.md) her [šip̄ḥâ](../../strongs/h/h8198.md) the [Miṣrî](../../strongs/h/h4713.md), [qēṣ](../../strongs/h/h7093.md) ['Aḇrām](../../strongs/h/h87.md) had [yashab](../../strongs/h/h3427.md) ten [šānâ](../../strongs/h/h8141.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), and [nathan](../../strongs/h/h5414.md) her to her ['iysh](../../strongs/h/h376.md) ['Aḇrām](../../strongs/h/h87.md) to be his ['ishshah](../../strongs/h/h802.md).

<a name="genesis_16_4"></a>Genesis 16:4

And he [bow'](../../strongs/h/h935.md) unto [Hāḡār](../../strongs/h/h1904.md), and she [harah](../../strongs/h/h2029.md): and when she [ra'ah](../../strongs/h/h7200.md) that she had [harah](../../strongs/h/h2029.md), her [gᵊḇereṯ](../../strongs/h/h1404.md) was [qālal](../../strongs/h/h7043.md) in her ['ayin](../../strongs/h/h5869.md).

<a name="genesis_16_5"></a>Genesis 16:5

And [Śāray](../../strongs/h/h8297.md) ['āmar](../../strongs/h/h559.md) unto ['Aḇrām](../../strongs/h/h87.md), My [chamac](../../strongs/h/h2555.md) be upon thee: I have [nathan](../../strongs/h/h5414.md) my [šip̄ḥâ](../../strongs/h/h8198.md) into thy [ḥêq](../../strongs/h/h2436.md); and when she [ra'ah](../../strongs/h/h7200.md) that she had [harah](../../strongs/h/h2029.md), I was [qālal](../../strongs/h/h7043.md) in her ['ayin](../../strongs/h/h5869.md): [Yĕhovah](../../strongs/h/h3068.md) [shaphat](../../strongs/h/h8199.md) between me and thee.

<a name="genesis_16_6"></a>Genesis 16:6

But ['Aḇrām](../../strongs/h/h87.md) ['āmar](../../strongs/h/h559.md) unto [Śāray](../../strongs/h/h8297.md), Behold, thy [šip̄ḥâ](../../strongs/h/h8198.md) is in thine [yad](../../strongs/h/h3027.md); ['asah](../../strongs/h/h6213.md) to her as it [towb](../../strongs/h/h2896.md) ['ayin](../../strongs/h/h5869.md). And when [Śāray](../../strongs/h/h8297.md) [ʿānâ](../../strongs/h/h6031.md) with her, she [bāraḥ](../../strongs/h/h1272.md) from her [paniym](../../strongs/h/h6440.md).

<a name="genesis_16_7"></a>Genesis 16:7

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [māṣā'](../../strongs/h/h4672.md) her by an ['ayin](../../strongs/h/h5869.md) of [mayim](../../strongs/h/h4325.md) in the [midbar](../../strongs/h/h4057.md), by the ['ayin](../../strongs/h/h5869.md) in the [derek](../../strongs/h/h1870.md) to [šûr](../../strongs/h/h7793.md).

<a name="genesis_16_8"></a>Genesis 16:8

And he ['āmar](../../strongs/h/h559.md), [Hāḡār](../../strongs/h/h1904.md), [Śāray](../../strongs/h/h8297.md) [šip̄ḥâ](../../strongs/h/h8198.md), whence [bow'](../../strongs/h/h935.md) thou? and whither wilt thou [yālaḵ](../../strongs/h/h3212.md)? And she ['āmar](../../strongs/h/h559.md), I [bāraḥ](../../strongs/h/h1272.md) from the [paniym](../../strongs/h/h6440.md) of my [gᵊḇereṯ](../../strongs/h/h1404.md) [Śāray](../../strongs/h/h8297.md).

<a name="genesis_16_9"></a>Genesis 16:9

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto her, [shuwb](../../strongs/h/h7725.md) to thy [gᵊḇereṯ](../../strongs/h/h1404.md), and [ʿānâ](../../strongs/h/h6031.md) thyself under her [yad](../../strongs/h/h3027.md).

<a name="genesis_16_10"></a>Genesis 16:10

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto her, I will [rabah](../../strongs/h/h7235.md) thy [zera'](../../strongs/h/h2233.md) [rabah](../../strongs/h/h7235.md), that it shall not be [sāp̄ar](../../strongs/h/h5608.md) for [rōḇ](../../strongs/h/h7230.md).

<a name="genesis_16_11"></a>Genesis 16:11

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto her, [hinneh](../../strongs/h/h2009.md), thou art with [hārê](../../strongs/h/h2030.md) and shalt [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md), and shalt [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Yišmāʿē'l](../../strongs/h/h3458.md); because [Yĕhovah](../../strongs/h/h3068.md) hath [shama'](../../strongs/h/h8085.md) thy ['oniy](../../strongs/h/h6040.md).

<a name="genesis_16_12"></a>Genesis 16:12

And he will be a [pere'](../../strongs/h/h6501.md) ['adam](../../strongs/h/h120.md); his [yad](../../strongs/h/h3027.md) will be against every man, and every man's [yad](../../strongs/h/h3027.md) against him; and he shall [shakan](../../strongs/h/h7931.md) in the [paniym](../../strongs/h/h6440.md) of all his ['ach](../../strongs/h/h251.md).

<a name="genesis_16_13"></a>Genesis 16:13

And she [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) that [dabar](../../strongs/h/h1696.md) unto her, Thou ['el](../../strongs/h/h410.md) [rŏ'î](../../strongs/h/h7210.md) me: for she ['āmar](../../strongs/h/h559.md), Have I also here [ra'ah](../../strongs/h/h7200.md) ['aḥar](../../strongs/h/h310.md) him that [rŏ'î](../../strongs/h/h7210.md) me?

<a name="genesis_16_14"></a>Genesis 16:14

Wherefore the [bᵊ'ēr](../../strongs/h/h875.md) was [qara'](../../strongs/h/h7121.md) [bᵊ'ēr laḥay rō'î](../../strongs/h/h883.md) [chay](../../strongs/h/h2416.md); behold, it is between [Qāḏēš](../../strongs/h/h6946.md) and [bereḏ](../../strongs/h/h1260.md).

<a name="genesis_16_15"></a>Genesis 16:15

And [Hāḡār](../../strongs/h/h1904.md) [yalad](../../strongs/h/h3205.md) ['Aḇrām](../../strongs/h/h87.md) a [ben](../../strongs/h/h1121.md): and ['Aḇrām](../../strongs/h/h87.md) [qara'](../../strongs/h/h7121.md) his [ben](../../strongs/h/h1121.md) [shem](../../strongs/h/h8034.md), which [Hāḡār](../../strongs/h/h1904.md) [yalad](../../strongs/h/h3205.md), [Yišmāʿē'l](../../strongs/h/h3458.md).

<a name="genesis_16_16"></a>Genesis 16:16

And ['Aḇrām](../../strongs/h/h87.md) was fourscore and six [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), when [Hāḡār](../../strongs/h/h1904.md) [yalad](../../strongs/h/h3205.md) [Yišmāʿē'l](../../strongs/h/h3458.md) to ['Aḇrām](../../strongs/h/h87.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 15](genesis_15.md) - [Genesis 17](genesis_17.md)