# [Genesis 25](https://www.blueletterbible.org/lxx/genesis/25)

<a name="genesis_25_1"></a>Genesis 25:1

[prostithēmi](../../strongs/g/g4369.md) [Abraam](../../strongs/g/g11.md) to [lambanō](../../strongs/g/g2983.md) a [gynē](../../strongs/g/g1135.md) whose [onoma](../../strongs/g/g3686.md) was Keturah.

<a name="genesis_25_2"></a>Genesis 25:2

And she [tiktō](../../strongs/g/g5088.md) to him Zimran, and Jokshan, and Medan, and [Madiam](../../strongs/g/g3099.md), and Ishbak, and Shuah.

<a name="genesis_25_3"></a>Genesis 25:3

And Jokshan [gennaō](../../strongs/g/g1080.md) Sheba and Dedan. And [huios](../../strongs/g/g5207.md) of Dedan were the Asshurim, and Letushim, and Leummim.

<a name="genesis_25_4"></a>Genesis 25:4

And the [huios](../../strongs/g/g5207.md) of [Madiam](../../strongs/g/g3099.md) were Ephah, and Epher, and Hanoch, and Abida, and Eldaah. All these were [huios](../../strongs/g/g5207.md) of Keturah.

<a name="genesis_25_5"></a>Genesis 25:5

[didōmi](../../strongs/g/g1325.md) [Abraam](../../strongs/g/g11.md) all his [hyparchonta](../../strongs/g/g5224.md) to [Isaak](../../strongs/g/g2464.md) his [huios](../../strongs/g/g5207.md). 

<a name="genesis_25_6"></a>Genesis 25:6

And to the [huios](../../strongs/g/g5207.md) of his concubines (pallakōn) [Abraam](../../strongs/g/g11.md) [didōmi](../../strongs/g/g1325.md) [doma](../../strongs/g/g1390.md). And he [exapostellō](../../strongs/g/g1821.md) them from [Isaak](../../strongs/g/g2464.md) his [huios](../../strongs/g/g5207.md) still [zaō](../../strongs/g/g2198.md) while he was towards the [anatolē](../../strongs/g/g395.md) into the [gē](../../strongs/g/g1093.md) of the [anatolē](../../strongs/g/g395.md). 

<a name="genesis_25_7"></a>Genesis 25:7

And these were the [etos](../../strongs/g/g2094.md) of the [hēmera](../../strongs/g/g2250.md) of the [zōē](../../strongs/g/g2222.md) of [Abraam](../../strongs/g/g11.md), as many as he [zaō](../../strongs/g/g2198.md), a [hekaton](../../strongs/g/g1540.md) [hebdomēkonta](../../strongs/g/g1440.md) [pente](../../strongs/g/g4002.md) [etos](../../strongs/g/g2094.md). 

<a name="genesis_25_8"></a>Genesis 25:8

And [ekleipō](../../strongs/g/g1587.md), [Abraam](../../strongs/g/g11.md) [apothnēskō](../../strongs/g/g599.md) in [gēras](../../strongs/g/g1094.md) a [kalos](../../strongs/g/g2570.md), an [presbytēs](../../strongs/g/g4246.md) and [plērēs](../../strongs/g/g4134.md) of [hēmera](../../strongs/g/g2250.md). And he was [prostithēmi](../../strongs/g/g4369.md) to his [laos](../../strongs/g/g2992.md). 

<a name="genesis_25_9"></a>Genesis 25:9

And [thaptō](../../strongs/g/g2290.md) him, [Isaak](../../strongs/g/g2464.md) and Ishmael
 two [huios](../../strongs/g/g5207.md) his, in the [spēlaion](../../strongs/g/g4693.md) [diplous](../../strongs/g/g1362.md), in the [agros](../../strongs/g/g68.md) of Ephron of Zohar the Hittite, which is [apenanti](../../strongs/g/g561.md) Mamre;

<a name="genesis_25_10"></a>Genesis 25:10

the [agros](../../strongs/g/g68.md) and the [spēlaion](../../strongs/g/g4693.md) which [Abraam](../../strongs/g/g11.md) [ktaomai](../../strongs/g/g2932.md) from the [huios](../../strongs/g/g5207.md) of Heth. There they [thaptō](../../strongs/g/g2290.md) [Abraam](../../strongs/g/g11.md) and [Sarra](../../strongs/g/g4564.md) his [gynē](../../strongs/g/g1135.md). 

<a name="genesis_25_11"></a>Genesis 25:11

And [ginomai](../../strongs/g/g1096.md) after the [apothnēskō](../../strongs/g/g599.md) of [Abraam](../../strongs/g/g11.md), [theos](../../strongs/g/g2316.md) [eulogeō](../../strongs/g/g2127.md) [Isaak](../../strongs/g/g2464.md) his [huios](../../strongs/g/g5207.md). And [Isaak](../../strongs/g/g2464.md) [katoikeō](../../strongs/g/g2730.md) by the [phrear](../../strongs/g/g5421.md) of the [horasis](../../strongs/g/g3706.md). 

<a name="genesis_25_12"></a>Genesis 25:12

And these are the [genesis](../../strongs/g/g1078.md) of Ishmael, the [huios](../../strongs/g/g5207.md) of [Abraam](../../strongs/g/g11.md), whom [Agar](../../strongs/g/g28.md) [tiktō](../../strongs/g/g5088.md) (the Egyptian [paidiskē](../../strongs/g/g3814.md) of [Sarra](../../strongs/g/g4564.md)) to [Abraam](../../strongs/g/g11.md).

<a name="genesis_25_13"></a>Genesis 25:13

And these are the [onoma](../../strongs/g/g3686.md) of the [huios](../../strongs/g/g5207.md) of Ishmael, according to the [onoma](../../strongs/g/g3686.md) of his [genea](../../strongs/g/g1074.md). [prōtotokos](../../strongs/g/g4416.md) of Ishmael, Nebajoth, and Kedar, and Adbeel, and Mibsam,

<a name="genesis_25_14"></a>Genesis 25:14

and Mishma, and Dumah, and Massa,

<a name="genesis_25_15"></a>Genesis 25:15

and Hadar, and Tema, and Jetur, and Naphish, and Kedemah.

<a name="genesis_25_16"></a>Genesis 25:16

These are the [huios](../../strongs/g/g5207.md) of Ishmael, and these are the [onoma](../../strongs/g/g3686.md) of them by their [skēnē](../../strongs/g/g4633.md), and by their [epaulis](../../strongs/g/g1886.md) -- [dōdeka](../../strongs/g/g1427.md) [archōn](../../strongs/g/g758.md) according to their [ethnos](../../strongs/g/g1484.md). 

<a name="genesis_25_17"></a>Genesis 25:17

And these are the [etos](../../strongs/g/g2094.md) of the [zōē](../../strongs/g/g2222.md) of Ishmael, a [hekaton](../../strongs/g/g1540.md) [triakonta](../../strongs/g/g5144.md) [hepta](../../strongs/g/g2033.md) [etos](../../strongs/g/g2094.md); and [ekleipō](../../strongs/g/g1587.md), he [apothnēskō](../../strongs/g/g599.md). And he was [prostithēmi](../../strongs/g/g4369.md) to his [genos](../../strongs/g/g1085.md). 

<a name="genesis_25_18"></a>Genesis 25:18

And he [katoikeō](../../strongs/g/g2730.md) from Havilah unto Shur, which is against the [prosōpon](../../strongs/g/g4383.md) of [Aigyptos](../../strongs/g/g125.md), unto [erchomai](../../strongs/g/g2064.md) to Assyrian. By the [prosōpon](../../strongs/g/g4383.md) of all his [adelphos](../../strongs/g/g80.md) he [katoikeō](../../strongs/g/g2730.md). 

<a name="genesis_25_19"></a>Genesis 25:19

And these are the [genesis](../../strongs/g/g1078.md) of [Isaak](../../strongs/g/g2464.md) the [huios](../../strongs/g/g5207.md) of [Abraam](../../strongs/g/g11.md).

<a name="genesis_25_20"></a>Genesis 25:20

[Abraam](../../strongs/g/g11.md) [gennaō](../../strongs/g/g1080.md) [Isaak](../../strongs/g/g2464.md). Isaac was [etos](../../strongs/g/g2094.md) [tessarakonta](../../strongs/g/g5062.md) when he [lambanō](../../strongs/g/g2983.md) [Rhebekka](../../strongs/g/g4479.md) [thygatēr](../../strongs/g/g2364.md) of Bethuel the [Syros](../../strongs/g/g4948.md) to himself for [gynē](../../strongs/g/g1135.md). 

<a name="genesis_25_21"></a>Genesis 25:21

"[deomai](../../strongs/g/g1189.md) [Isaak](../../strongs/g/g2464.md) the [kyrios](../../strongs/g/g2962.md) concerning [Rhebekka](../../strongs/g/g4479.md) his [gynē](../../strongs/g/g1135.md), for she was sterile (steira); [eisakouō](../../strongs/g/g1522.md) and his [theos](../../strongs/g/g2316.md), and [syllambanō](../../strongs/g/g4815.md) in the [gastēr](../../strongs/g/g1064.md) [Rhebekka](../../strongs/g/g4479.md) his [gynē](../../strongs/g/g1135.md). "

<a name="genesis_25_22"></a>Genesis 25:22

[skirtaō](../../strongs/g/g4640.md) the [paidion](../../strongs/g/g3813.md) in her. And she [eipon](../../strongs/g/g2036.md), If thus to me is [mellō](../../strongs/g/g3195.md) to [ginomai](../../strongs/g/g1096.md), why to me is this? And she [poreuō](../../strongs/g/g4198.md) to [pynthanomai](../../strongs/g/g4441.md) of the [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_25_23"></a>Genesis 25:23

"And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md) to her, [dyo](../../strongs/g/g1417.md) [ethnos](../../strongs/g/g1484.md) in your [gastēr](../../strongs/g/g1064.md) are; and [dyo](../../strongs/g/g1417.md) [laos](../../strongs/g/g2992.md) from out of your [koilia](../../strongs/g/g2836.md) will [diastellō](../../strongs/g/g1291.md). And one [laos](../../strongs/g/g2992.md) another [laos](../../strongs/g/g2992.md) will be [hyperechō](../../strongs/g/g5242.md); and the [megas](../../strongs/g/g3173.md) will be a [douleuō](../../strongs/g/g1398.md) to the [elassōn](../../strongs/g/g1640.md). "

<a name="genesis_25_24"></a>Genesis 25:24

And were [plēroō](../../strongs/g/g4137.md) the [hēmera](../../strongs/g/g2250.md) for her to [tiktō](../../strongs/g/g5088.md). And thus there were [didymos](../../strongs/g/g1324.md) in her [koilia](../../strongs/g/g2836.md). 

<a name="genesis_25_25"></a>Genesis 25:25

And [exerchomai](../../strongs/g/g1831.md) the [prōtotokos](../../strongs/g/g4416.md) fiery red (pyrrakēs) [holos](../../strongs/g/g3650.md), and of skin (dora) hairy (dasys). And she [eponomazō](../../strongs/g/g2028.md) his [onoma](../../strongs/g/g3686.md) Esau.

<a name="genesis_25_26"></a>Genesis 25:26

And after this came [exerchomai](../../strongs/g/g1831.md) his [adelphos](../../strongs/g/g80.md), and his [cheir](../../strongs/g/g5495.md) [epilambanomai](../../strongs/g/g1949.md) of the [pterna](../../strongs/g/g4418.md) of [Ēsau](../../strongs/g/g2269.md). And she [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md) [Iakōb](../../strongs/g/g2384.md). And [Isaak](../../strongs/g/g2464.md) was [etos](../../strongs/g/g2094.md) [hexēkonta](../../strongs/g/g1835.md) when [tiktō](../../strongs/g/g5088.md) them [Rhebekka](../../strongs/g/g4479.md).

<a name="genesis_25_27"></a>Genesis 25:27

[auxanō](../../strongs/g/g837.md) the [neaniskos](../../strongs/g/g3495.md). And [Ēsau](../../strongs/g/g2269.md) was a [anthrōpos](../../strongs/g/g444.md) [eidō](../../strongs/g/g1492.md) to hunt (kynēgein) -- rugged (agroikos); but [Iakōb](../../strongs/g/g2384.md) [anthrōpos](../../strongs/g/g444.md) was a simple (aplastos) [oikeō](../../strongs/g/g3611.md) a [oikia](../../strongs/g/g3614.md). 

<a name="genesis_25_28"></a>Genesis 25:28

[agapaō](../../strongs/g/g25.md) [Isaak](../../strongs/g/g2464.md) [Ēsau](../../strongs/g/g2269.md), for his [thēra](../../strongs/g/g2339.md) [brōsis](../../strongs/g/g1035.md) for him. But [Rhebekka](../../strongs/g/g4479.md) [agapaō](../../strongs/g/g25.md) [Iakōb](../../strongs/g/g2384.md).

<a name="genesis_25_29"></a>Genesis 25:29

boiled (hēpsēsen) [Iakōb](../../strongs/g/g2384.md) stew (hepsema); [erchomai](../../strongs/g/g2064.md) and [Ēsau](../../strongs/g/g2269.md) from out of the plains (pediou) [ekleipō](../../strongs/g/g1587.md). 

<a name="genesis_25_30"></a>Genesis 25:30

And [Ēsau](../../strongs/g/g2269.md) [eipon](../../strongs/g/g2036.md) to [Iakōb](../../strongs/g/g2384.md), Let me [geuomai](../../strongs/g/g1089.md) from stew (hepsematos) this [pyrros](../../strongs/g/g4450.md) for I [ekleipō](../../strongs/g/g1587.md); on account of this was [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md) Edom.

<a name="genesis_25_31"></a>Genesis 25:31

[eipon](../../strongs/g/g2036.md) [Iakōb](../../strongs/g/g2384.md) to [Ēsau](../../strongs/g/g2269.md), [apodidōmi](../../strongs/g/g591.md) to me [sēmeron](../../strongs/g/g4594.md) your [prōtotokia](../../strongs/g/g4415.md).

<a name="genesis_25_32"></a>Genesis 25:32

And [Ēsau](../../strongs/g/g2269.md) [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), I am [poreuō](../../strongs/g/g4198.md) to come to an [teleutaō](../../strongs/g/g5053.md), and what to me are these, the [prōtotokia](../../strongs/g/g4415.md)? 

<a name="genesis_25_33"></a>Genesis 25:33

And [eipon](../../strongs/g/g2036.md) to him [Iakōb](../../strongs/g/g2384.md), You [omnyō](../../strongs/g/g3660.md) to me [sēmeron](../../strongs/g/g4594.md)! And he [omnyō](../../strongs/g/g3660.md) to him. [apodidōmi](../../strongs/g/g591.md) [Ēsau](../../strongs/g/g2269.md) the [prōtotokia](../../strongs/g/g4415.md) to [Iakōb](../../strongs/g/g2384.md).

<a name="genesis_25_34"></a>Genesis 25:34

And [Iakōb](../../strongs/g/g2384.md) [didōmi](../../strongs/g/g1325.md) to [Ēsau](../../strongs/g/g2269.md) [artos](../../strongs/g/g740.md) and stew (hepsema) of lentils (phakou). And he [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md), and [anistēmi](../../strongs/g/g450.md) he set out (ōcheto). And [Ēsau](../../strongs/g/g2269.md) treated as worthless (ephaulisen) the [prōtotokia](../../strongs/g/g4415.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 24](genesis_lxx_24.md) - [Genesis 26](genesis_lxx_26.md)