# [Genesis 15](https://www.blueletterbible.org/kjv/gen/15/1/s_15001)

<a name="genesis_15_1"></a>Genesis 15:1

['aḥar](../../strongs/h/h310.md) these things the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto ['Aḇrām](../../strongs/h/h87.md) in a [maḥăzê](../../strongs/h/h4236.md), ['āmar](../../strongs/h/h559.md), [yare'](../../strongs/h/h3372.md) not, ['Aḇrām](../../strongs/h/h87.md): I am thy [magen](../../strongs/h/h4043.md), and thy [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md) [śāḵār](../../strongs/h/h7939.md).

<a name="genesis_15_2"></a>Genesis 15:2

And ['Aḇrām](../../strongs/h/h87.md) ['āmar](../../strongs/h/h559.md), ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), what wilt thou [nathan](../../strongs/h/h5414.md) me, seeing I [halak](../../strongs/h/h1980.md) [ʿărîrî](../../strongs/h/h6185.md), and the [ben](../../strongs/h/h1121.md) [mešeq](../../strongs/h/h4943.md) of my [bayith](../../strongs/h/h1004.md) is this ['Ĕlîʿezer](../../strongs/h/h461.md) of [Dammeśeq](../../strongs/h/h1834.md)?

<a name="genesis_15_3"></a>Genesis 15:3

And ['Aḇrām](../../strongs/h/h87.md) ['āmar](../../strongs/h/h559.md), Behold, to me thou hast [nathan](../../strongs/h/h5414.md) no [zera'](../../strongs/h/h2233.md): and, lo, [ben](../../strongs/h/h1121.md) in my [bayith](../../strongs/h/h1004.md) is mine [yarash](../../strongs/h/h3423.md).

<a name="genesis_15_4"></a>Genesis 15:4

And, behold, the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto him, sayin['āmar](../../strongs/h/h559.md), This shall not be thine [yarash](../../strongs/h/h3423.md); but he that shall [yāṣā'](../../strongs/h/h3318.md) out of thine own [me'ah](../../strongs/h/h4578.md) shall be thine [yarash](../../strongs/h/h3423.md).

<a name="genesis_15_5"></a>Genesis 15:5

And he [yāṣā'](../../strongs/h/h3318.md) him [ḥûṣ](../../strongs/h/h2351.md), and ['āmar](../../strongs/h/h559.md), [nabat](../../strongs/h/h5027.md) now toward [shamayim](../../strongs/h/h8064.md), and [sāp̄ar](../../strongs/h/h5608.md) the [kowkab](../../strongs/h/h3556.md), if thou be [yakol](../../strongs/h/h3201.md) to [sāp̄ar](../../strongs/h/h5608.md) them: and he ['āmar](../../strongs/h/h559.md) unto him, So shall thy [zera'](../../strongs/h/h2233.md) be.

<a name="genesis_15_6"></a>Genesis 15:6

And he ['aman](../../strongs/h/h539.md) in [Yĕhovah](../../strongs/h/h3068.md); and he [chashab](../../strongs/h/h2803.md) it to him for [ṣĕdāqāh](../../strongs/h/h6666.md).

<a name="genesis_15_7"></a>Genesis 15:7

And he ['āmar](../../strongs/h/h559.md) unto him, I am [Yĕhovah](../../strongs/h/h3068.md) that [yāṣā'](../../strongs/h/h3318.md) thee out of ['Ûr](../../strongs/h/h218.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), to [nathan](../../strongs/h/h5414.md) thee this ['erets](../../strongs/h/h776.md) to [yarash](../../strongs/h/h3423.md) it.

<a name="genesis_15_8"></a>Genesis 15:8

And he ['āmar](../../strongs/h/h559.md), ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), whereby shall I [yada'](../../strongs/h/h3045.md) that I shall [yarash](../../strongs/h/h3423.md) it?

<a name="genesis_15_9"></a>Genesis 15:9

And he ['āmar](../../strongs/h/h559.md) unto him, [laqach](../../strongs/h/h3947.md) me an [ʿeḡlâ](../../strongs/h/h5697.md) of [šālaš](../../strongs/h/h8027.md), and a [ʿēz](../../strongs/h/h5795.md) of [šālaš](../../strongs/h/h8027.md), and an ['ayil](../../strongs/h/h352.md) of [šālaš](../../strongs/h/h8027.md), and a [tôr](../../strongs/h/h8449.md), and a [gôzāl](../../strongs/h/h1469.md).

<a name="genesis_15_10"></a>Genesis 15:10

And he [laqach](../../strongs/h/h3947.md) unto him all these, and [bāṯar](../../strongs/h/h1334.md) them in the [tavek](../../strongs/h/h8432.md), and [nathan](../../strongs/h/h5414.md) ['iysh](../../strongs/h/h376.md) [beṯer](../../strongs/h/h1335.md) one [qārā'](../../strongs/h/h7125.md) [rea'](../../strongs/h/h7453.md): but the [tsippowr](../../strongs/h/h6833.md) [bāṯar](../../strongs/h/h1334.md) he not.

<a name="genesis_15_11"></a>Genesis 15:11

And when the [ʿayiṭ](../../strongs/h/h5861.md) [yarad](../../strongs/h/h3381.md) upon the [peḡer](../../strongs/h/h6297.md), ['Aḇrām](../../strongs/h/h87.md) [nāšaḇ](../../strongs/h/h5380.md) them.

<a name="genesis_15_12"></a>Genesis 15:12

And when the [šemeš](../../strongs/h/h8121.md) was [bow'](../../strongs/h/h935.md), a [tardēmâ](../../strongs/h/h8639.md) [naphal](../../strongs/h/h5307.md) upon ['Aḇrām](../../strongs/h/h87.md); and, lo, an ['êmâ](../../strongs/h/h367.md) of [gadowl](../../strongs/h/h1419.md) [ḥăšēḵâ](../../strongs/h/h2825.md) [naphal](../../strongs/h/h5307.md) upon him.

<a name="genesis_15_13"></a>Genesis 15:13

And he ['āmar](../../strongs/h/h559.md) unto ['Aḇrām](../../strongs/h/h87.md), [yada'](../../strongs/h/h3045.md) of a surety that thy [zera'](../../strongs/h/h2233.md) shall be a [ger](../../strongs/h/h1616.md) in an ['erets](../../strongs/h/h776.md) that is not their's, and shall ['abad](../../strongs/h/h5647.md) them; and they shall [ʿānâ](../../strongs/h/h6031.md) them four hundred [šānâ](../../strongs/h/h8141.md);

<a name="genesis_15_14"></a>Genesis 15:14

And also that [gowy](../../strongs/h/h1471.md), whom they shall ['abad](../../strongs/h/h5647.md), will I [diyn](../../strongs/h/h1777.md): and ['aḥar](../../strongs/h/h310.md)d shall they [yāṣā'](../../strongs/h/h3318.md) with [gadowl](../../strongs/h/h1419.md) [rᵊḵûš](../../strongs/h/h7399.md).

<a name="genesis_15_15"></a>Genesis 15:15

And thou shalt [bow'](../../strongs/h/h935.md) to thy ['ab](../../strongs/h/h1.md) in [shalowm](../../strongs/h/h7965.md); thou shalt be [qāḇar](../../strongs/h/h6912.md) in a [towb](../../strongs/h/h2896.md) [śêḇâ](../../strongs/h/h7872.md).

<a name="genesis_15_16"></a>Genesis 15:16

But in the fourth [dôr](../../strongs/h/h1755.md) they shall [shuwb](../../strongs/h/h7725.md) hither again: for the ['avon](../../strongs/h/h5771.md) of the ['Ĕmōrî](../../strongs/h/h567.md) is not yet [šālēm](../../strongs/h/h8003.md).

<a name="genesis_15_17"></a>Genesis 15:17

And it came to pass, that, when the [šemeš](../../strongs/h/h8121.md) [bow'](../../strongs/h/h935.md), and it was [ʿălāṭâ](../../strongs/h/h5939.md), behold an ['ashan](../../strongs/h/h6227.md) [tannûr](../../strongs/h/h8574.md), and an ['esh](../../strongs/h/h784.md) [lapîḏ](../../strongs/h/h3940.md) that ['abar](../../strongs/h/h5674.md) between those [gezer](../../strongs/h/h1506.md).

<a name="genesis_15_18"></a>Genesis 15:18

In the same [yowm](../../strongs/h/h3117.md) [Yĕhovah](../../strongs/h/h3068.md) [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with ['Aḇrām](../../strongs/h/h87.md), ['āmar](../../strongs/h/h559.md), Unto thy [zera'](../../strongs/h/h2233.md) have I [nathan](../../strongs/h/h5414.md) this ['erets](../../strongs/h/h776.md), from the [nāhār](../../strongs/h/h5104.md) of [Mitsrayim](../../strongs/h/h4714.md) unto the [gadowl](../../strongs/h/h1419.md) [nāhār](../../strongs/h/h5104.md), the [nāhār](../../strongs/h/h5104.md) [Pᵊrāṯ](../../strongs/h/h6578.md):

<a name="genesis_15_19"></a>Genesis 15:19

The [Qênî](../../strongs/h/h7017.md), and the [qᵊnizzî](../../strongs/h/h7074.md), and the [qaḏmōnî](../../strongs/h/h6935.md),

<a name="genesis_15_20"></a>Genesis 15:20

And the [Ḥitî](../../strongs/h/h2850.md), and the [Pᵊrizzî](../../strongs/h/h6522.md), and the [rᵊp̄ā'îm](../../strongs/h/h7497.md),

<a name="genesis_15_21"></a>Genesis 15:21

And the ['Ĕmōrî](../../strongs/h/h567.md), and the [Kᵊnaʿănî](../../strongs/h/h3669.md), and the [Girgāšî](../../strongs/h/h1622.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 14](genesis_14.md) - [Genesis 16](genesis_16.md)