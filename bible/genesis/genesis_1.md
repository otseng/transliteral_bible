# [Genesis 1](https://www.blueletterbible.org/kjv/gen/1/1/s_1001)

<a name="genesis_1_1"></a>Genesis 1:1

[re'shiyth](../../strongs/h/h7225.md) ['Elohiym](../../strongs/h/h430.md) [bara'](../../strongs/h/h1254.md) the [shamayim](../../strongs/h/h8064.md) and the ['erets](../../strongs/h/h776.md).

<a name="genesis_1_2"></a>Genesis 1:2

And the ['erets](../../strongs/h/h776.md) was [tohuw](../../strongs/h/h8414.md), and [bohuw](../../strongs/h/h922.md); and [choshek](../../strongs/h/h2822.md) upon the [paniym](../../strongs/h/h6440.md) of the [tĕhowm](../../strongs/h/h8415.md). And the [ruwach](../../strongs/h/h7307.md) of ['Elohiym](../../strongs/h/h430.md) [rachaph](../../strongs/h/h7363.md) upon the [paniym](../../strongs/h/h6440.md) of the [mayim](../../strongs/h/h4325.md).

<a name="genesis_1_3"></a>Genesis 1:3

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), Let there be ['owr](../../strongs/h/h216.md): and there was ['owr](../../strongs/h/h216.md).

<a name="genesis_1_4"></a>Genesis 1:4

And ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) the ['owr](../../strongs/h/h216.md), that it was [towb](../../strongs/h/h2896.md): and ['Elohiym](../../strongs/h/h430.md) [bāḏal](../../strongs/h/h914.md) [bayin](../../strongs/h/h996.md) the ['owr](../../strongs/h/h216.md) from the [choshek](../../strongs/h/h2822.md).

<a name="genesis_1_5"></a>Genesis 1:5

And ['Elohiym](../../strongs/h/h430.md) [qara'](../../strongs/h/h7121.md) the ['owr](../../strongs/h/h216.md) [yowm](../../strongs/h/h3117.md), and the [choshek](../../strongs/h/h2822.md) he [qara'](../../strongs/h/h7121.md) [layil](../../strongs/h/h3915.md). And the ['ereb](../../strongs/h/h6153.md) and the [boqer](../../strongs/h/h1242.md) were the first [yowm](../../strongs/h/h3117.md).

<a name="genesis_1_6"></a>Genesis 1:6

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), Let there be a [raqiya'](../../strongs/h/h7549.md) in the [tavek](../../strongs/h/h8432.md) of the [mayim](../../strongs/h/h4325.md), and let it [bāḏal](../../strongs/h/h914.md) the [mayim](../../strongs/h/h4325.md) from the [mayim](../../strongs/h/h4325.md).

<a name="genesis_1_7"></a>Genesis 1:7

And ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) the [raqiya'](../../strongs/h/h7549.md), and [bāḏal](../../strongs/h/h914.md) the [mayim](../../strongs/h/h4325.md) which were [taḥaṯ](../../strongs/h/h8478.md) the [raqiya'](../../strongs/h/h7549.md) from the [mayim](../../strongs/h/h4325.md) which were [ʿal](../../strongs/h/h5921.md) the [raqiya'](../../strongs/h/h7549.md): and it was [kēn](../../strongs/h/h3651.md).

<a name="genesis_1_8"></a>Genesis 1:8

And ['Elohiym](../../strongs/h/h430.md) [qara'](../../strongs/h/h7121.md) the [raqiya'](../../strongs/h/h7549.md) [shamayim](../../strongs/h/h8064.md). And the ['ereb](../../strongs/h/h6153.md) and the [boqer](../../strongs/h/h1242.md) were the second [yowm](../../strongs/h/h3117.md).

<a name="genesis_1_9"></a>Genesis 1:9

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), Let the [mayim](../../strongs/h/h4325.md) under the [shamayim](../../strongs/h/h8064.md) be [qāvâ](../../strongs/h/h6960.md) unto one [maqowm](../../strongs/h/h4725.md), and let the [yabāšâ](../../strongs/h/h3004.md) [ra'ah](../../strongs/h/h7200.md): and it was so.

<a name="genesis_1_10"></a>Genesis 1:10

And ['Elohiym](../../strongs/h/h430.md) [qara'](../../strongs/h/h7121.md) the [yabāšâ](../../strongs/h/h3004.md) ['erets](../../strongs/h/h776.md); and the [miqvê](../../strongs/h/h4723.md) of the [mayim](../../strongs/h/h4325.md) [qara'](../../strongs/h/h7121.md) he [yam](../../strongs/h/h3220.md): and ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) that it was [towb](../../strongs/h/h2896.md).

<a name="genesis_1_11"></a>Genesis 1:11

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), Let the ['erets](../../strongs/h/h776.md) [dāšā'](../../strongs/h/h1876.md) [deše'](../../strongs/h/h1877.md), the ['eseb](../../strongs/h/h6212.md) [zāraʿ](../../strongs/h/h2232.md) [zera'](../../strongs/h/h2233.md), and the [pĕriy](../../strongs/h/h6529.md) ['ets](../../strongs/h/h6086.md) ['asah](../../strongs/h/h6213.md) [pĕriy](../../strongs/h/h6529.md) after his [miyn](../../strongs/h/h4327.md), whose [zera'](../../strongs/h/h2233.md) is in itself, upon the ['erets](../../strongs/h/h776.md): and it was so.

<a name="genesis_1_12"></a>Genesis 1:12

And the ['erets](../../strongs/h/h776.md) [yāṣā'](../../strongs/h/h3318.md) [deše'](../../strongs/h/h1877.md), and ['eseb](../../strongs/h/h6212.md) [zāraʿ](../../strongs/h/h2232.md) [zera'](../../strongs/h/h2233.md) after his [miyn](../../strongs/h/h4327.md), and the ['ets](../../strongs/h/h6086.md) ['asah](../../strongs/h/h6213.md) [pĕriy](../../strongs/h/h6529.md), whose [zera'](../../strongs/h/h2233.md) was in itself, after his [miyn](../../strongs/h/h4327.md): and ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) that it was [towb](../../strongs/h/h2896.md).

<a name="genesis_1_13"></a>Genesis 1:13

And the ['ereb](../../strongs/h/h6153.md) and the [boqer](../../strongs/h/h1242.md) were the third [yowm](../../strongs/h/h3117.md).

<a name="genesis_1_14"></a>Genesis 1:14

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), Let there be [ma'owr](../../strongs/h/h3974.md) in the [raqiya'](../../strongs/h/h7549.md) of the [shamayim](../../strongs/h/h8064.md) to [bāḏal](../../strongs/h/h914.md) the [yowm](../../strongs/h/h3117.md) from the [layil](../../strongs/h/h3915.md); and let them be for ['ôṯ](../../strongs/h/h226.md), and for [môʿēḏ](../../strongs/h/h4150.md), and for [yowm](../../strongs/h/h3117.md), and [šānâ](../../strongs/h/h8141.md):

<a name="genesis_1_15"></a>Genesis 1:15

And let them be for [ma'owr](../../strongs/h/h3974.md) in the [raqiya'](../../strongs/h/h7549.md) of the [shamayim](../../strongs/h/h8064.md) to ['owr](../../strongs/h/h215.md) upon the ['erets](../../strongs/h/h776.md): and it was so.

<a name="genesis_1_16"></a>Genesis 1:16

And ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) [šᵊnayim](../h/h8147.md) [gadowl](../../strongs/h/h1419.md) [ma'owr](../../strongs/h/h3974.md); the [gadowl](../../strongs/h/h1419.md) [ma'owr](../../strongs/h/h3974.md) to [memshalah](../../strongs/h/h4475.md) the [yowm](../../strongs/h/h3117.md), and the [qāṭān](../../strongs/h/h6996.md) [ma'owr](../../strongs/h/h3974.md) to [memshalah](../../strongs/h/h4475.md) the [layil](../../strongs/h/h3915.md): the [kowkab](../../strongs/h/h3556.md) also.

<a name="genesis_1_17"></a>Genesis 1:17

And ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) them in the [raqiya'](../../strongs/h/h7549.md) of the [shamayim](../../strongs/h/h8064.md) to ['owr](../../strongs/h/h215.md) upon the ['erets](../../strongs/h/h776.md),

<a name="genesis_1_18"></a>Genesis 1:18

And to [mashal](../../strongs/h/h4910.md) over the [yowm](../../strongs/h/h3117.md) and over the [layil](../../strongs/h/h3915.md), and to [bāḏal](../../strongs/h/h914.md) the ['owr](../../strongs/h/h216.md) from the [choshek](../../strongs/h/h2822.md): and ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) that it was [towb](../../strongs/h/h2896.md).

<a name="genesis_1_19"></a>Genesis 1:19

And the ['ereb](../../strongs/h/h6153.md) and the [boqer](../../strongs/h/h1242.md) were the fourth [yowm](../../strongs/h/h3117.md).

<a name="genesis_1_20"></a>Genesis 1:20

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), Let the [mayim](../../strongs/h/h4325.md) [šāraṣ](../../strongs/h/h8317.md) the [šereṣ](../../strongs/h/h8318.md) that hath [chay](../../strongs/h/h2416.md) [nephesh](../../strongs/h/h5315.md), and [ʿôp̄](../../strongs/h/h5775.md) that may ['uwph](../../strongs/h/h5774.md) [ʿal](../../strongs/h/h5921.md) the ['erets](../../strongs/h/h776.md) in the [paniym](../../strongs/h/h6440.md) [raqiya'](../../strongs/h/h7549.md) of [shamayim](../../strongs/h/h8064.md).

<a name="genesis_1_21"></a>Genesis 1:21

And ['Elohiym](../../strongs/h/h430.md) [bara'](../../strongs/h/h1254.md) [gadowl](../../strongs/h/h1419.md) [tannîn](../../strongs/h/h8577.md), and every [chay](../../strongs/h/h2416.md) [nephesh](../../strongs/h/h5315.md) that [rāmaś](../../strongs/h/h7430.md), which the [mayim](../../strongs/h/h4325.md) [šāraṣ](../../strongs/h/h8317.md), after their [miyn](../../strongs/h/h4327.md), and every [kanaph](../../strongs/h/h3671.md) [ʿôp̄](../../strongs/h/h5775.md) after his [miyn](../../strongs/h/h4327.md): and ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) that it was [towb](../../strongs/h/h2896.md).

<a name="genesis_1_22"></a>Genesis 1:22

And ['Elohiym](../../strongs/h/h430.md) [barak](../../strongs/h/h1288.md) them, ['āmar](../../strongs/h/h559.md), Be [parah](../../strongs/h/h6509.md), and [rabah](../../strongs/h/h7235.md), and [mālā'](../../strongs/h/h4390.md) the [mayim](../../strongs/h/h4325.md) in the [yam](../../strongs/h/h3220.md), and let [ʿôp̄](../../strongs/h/h5775.md) [rabah](../../strongs/h/h7235.md) in the ['erets](../../strongs/h/h776.md).

<a name="genesis_1_23"></a>Genesis 1:23

And the ['ereb](../../strongs/h/h6153.md) and the [boqer](../../strongs/h/h1242.md) were the fifth [yowm](../../strongs/h/h3117.md).

<a name="genesis_1_24"></a>Genesis 1:24

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), Let the ['erets](../../strongs/h/h776.md) [yāṣā'](../../strongs/h/h3318.md) the [chay](../../strongs/h/h2416.md) [nephesh](../../strongs/h/h5315.md) after his [miyn](../../strongs/h/h4327.md), [bĕhemah](../../strongs/h/h929.md), and [remeś](../../strongs/h/h7431.md), and [chay](../../strongs/h/h2416.md) of the ['erets](../../strongs/h/h776.md) after his [miyn](../../strongs/h/h4327.md): and it was so.

<a name="genesis_1_25"></a>Genesis 1:25

And ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) the [chay](../../strongs/h/h2416.md) of the ['erets](../../strongs/h/h776.md) after his [miyn](../../strongs/h/h4327.md), and [bĕhemah](../../strongs/h/h929.md) after their [miyn](../../strongs/h/h4327.md), and every thing that [remeś](../../strongs/h/h7431.md) upon the ['ăḏāmâ](../../strongs/h/h127.md) after his [miyn](../../strongs/h/h4327.md): and ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) that it was [towb](../../strongs/h/h2896.md).

<a name="genesis_1_26"></a>Genesis 1:26

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), Let us ['asah](../../strongs/h/h6213.md) ['adam](../../strongs/h/h120.md) in our [tselem](../../strongs/h/h6754.md), after our [dĕmuwth](../../strongs/h/h1823.md): and let them have [radah](../../strongs/h/h7287.md) over the [dāḡâ](../../strongs/h/h1710.md) of the [yam](../../strongs/h/h3220.md), and over the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and over the [bĕhemah](../../strongs/h/h929.md), and over all the ['erets](../../strongs/h/h776.md), and over every [remeś](../../strongs/h/h7431.md) that [rāmaś](../../strongs/h/h7430.md) upon the ['erets](../../strongs/h/h776.md).

<a name="genesis_1_27"></a>Genesis 1:27

So ['Elohiym](../../strongs/h/h430.md) [bara'](../../strongs/h/h1254.md) ['adam](../../strongs/h/h120.md) in his [tselem](../../strongs/h/h6754.md), in the [tselem](../../strongs/h/h6754.md) of ['Elohiym](../../strongs/h/h430.md) [bara'](../../strongs/h/h1254.md) he him; [zāḵār](../../strongs/h/h2145.md) and [nᵊqēḇâ](../../strongs/h/h5347.md) [bara'](../../strongs/h/h1254.md) he them.

<a name="genesis_1_28"></a>Genesis 1:28

And ['Elohiym](../../strongs/h/h430.md) [barak](../../strongs/h/h1288.md) them, and ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto them, Be [parah](../../strongs/h/h6509.md), and [rabah](../../strongs/h/h7235.md), and [mālā'](../../strongs/h/h4390.md) the ['erets](../../strongs/h/h776.md), and [kāḇaš](../../strongs/h/h3533.md) it: and have [radah](../../strongs/h/h7287.md) over the [dāḡâ](../../strongs/h/h1710.md) of the [yam](../../strongs/h/h3220.md), and over the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and over every [chay](../../strongs/h/h2416.md) that [rāmaś](../../strongs/h/h7430.md) upon the ['erets](../../strongs/h/h776.md).

<a name="genesis_1_29"></a>Genesis 1:29

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), [hinneh](../../strongs/h/h2009.md), I have [nathan](../../strongs/h/h5414.md) you every ['eseb](../../strongs/h/h6212.md) [zāraʿ](../../strongs/h/h2232.md) [zera'](../../strongs/h/h2233.md), which upon the [paniym](../../strongs/h/h6440.md) of all the ['erets](../../strongs/h/h776.md), and every ['ets](../../strongs/h/h6086.md), in the which the [pĕriy](../../strongs/h/h6529.md) of an ['ets](../../strongs/h/h6086.md) [zāraʿ](../../strongs/h/h2232.md) [zera'](../../strongs/h/h2233.md); to you it shall be for ['oklah](../../strongs/h/h402.md).

<a name="genesis_1_30"></a>Genesis 1:30

And to every [chay](../../strongs/h/h2416.md) of the ['erets](../../strongs/h/h776.md), and to every [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and to every thing that [rāmaś](../../strongs/h/h7430.md) upon the ['erets](../../strongs/h/h776.md), wherein there is [chay](../../strongs/h/h2416.md) [nephesh](../../strongs/h/h5315.md), every [yereq](../../strongs/h/h3418.md) ['eseb](../../strongs/h/h6212.md) for ['oklah](../../strongs/h/h402.md): and it was so.

<a name="genesis_1_31"></a>Genesis 1:31

And ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) every thing that he had ['asah](../../strongs/h/h6213.md), and, [me'od](../../strongs/h/h3966.md) [towb](../../strongs/h/h2896.md). And the ['ereb](../../strongs/h/h6153.md) and the [boqer](../../strongs/h/h1242.md) were the sixth [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 2](genesis_2.md)