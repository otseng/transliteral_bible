# [Genesis 43](https://www.blueletterbible.org/kjv/gen/43/1/s_43001)

<a name="genesis_43_1"></a>Genesis 43:1

And the [rāʿāḇ](../../strongs/h/h7458.md) was [kāḇēḏ](../../strongs/h/h3515.md) in the ['erets](../../strongs/h/h776.md).

<a name="genesis_43_2"></a>Genesis 43:2

And it came to pass, when they had [kalah](../../strongs/h/h3615.md) ['akal](../../strongs/h/h398.md) the [šēḇer](../../strongs/h/h7668.md) which they had [bow'](../../strongs/h/h935.md) out of [Mitsrayim](../../strongs/h/h4714.md), their ['ab](../../strongs/h/h1.md) ['āmar](../../strongs/h/h559.md) unto them, [shuwb](../../strongs/h/h7725.md), [šāḇar](../../strongs/h/h7666.md) us a [mᵊʿaṭ](../../strongs/h/h4592.md) ['ōḵel](../../strongs/h/h400.md).

<a name="genesis_43_3"></a>Genesis 43:3

And [Yehuwdah](../../strongs/h/h3063.md) ['āmar](../../strongs/h/h559.md) unto him, ['āmar](../../strongs/h/h559.md), The ['iysh](../../strongs/h/h376.md) did [ʿûḏ](../../strongs/h/h5749.md) [ʿûḏ](../../strongs/h/h5749.md) unto us, ['āmar](../../strongs/h/h559.md), Ye shall not [ra'ah](../../strongs/h/h7200.md) my [paniym](../../strongs/h/h6440.md), except your ['ach](../../strongs/h/h251.md) be with you.

<a name="genesis_43_4"></a>Genesis 43:4

If thou wilt [shalach](../../strongs/h/h7971.md) our ['ach](../../strongs/h/h251.md) with us, we will [yarad](../../strongs/h/h3381.md) and [šāḇar](../../strongs/h/h7666.md) thee ['ōḵel](../../strongs/h/h400.md):

<a name="genesis_43_5"></a>Genesis 43:5

But if thou wilt not [shalach](../../strongs/h/h7971.md) him, we will not [yarad](../../strongs/h/h3381.md): for the ['iysh](../../strongs/h/h376.md) ['āmar](../../strongs/h/h559.md) unto us, Ye shall not [ra'ah](../../strongs/h/h7200.md) my [paniym](../../strongs/h/h6440.md), except your ['ach](../../strongs/h/h251.md) be with you.

<a name="genesis_43_6"></a>Genesis 43:6

And [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md), Wherefore dealt ye so [ra'a'](../../strongs/h/h7489.md) with me, as to [nāḡaḏ](../../strongs/h/h5046.md) the ['iysh](../../strongs/h/h376.md) whether ye had yet an ['ach](../../strongs/h/h251.md)?

<a name="genesis_43_7"></a>Genesis 43:7

And they ['āmar](../../strongs/h/h559.md), The ['iysh](../../strongs/h/h376.md) [sha'al](../../strongs/h/h7592.md) us of our state, and of our [môleḏeṯ](../../strongs/h/h4138.md), ['āmar](../../strongs/h/h559.md), Is your ['ab](../../strongs/h/h1.md) yet [chay](../../strongs/h/h2416.md)? have ye another ['ach](../../strongs/h/h251.md)? and we [nāḡaḏ](../../strongs/h/h5046.md) him according to the [peh](../../strongs/h/h6310.md) of these [dabar](../../strongs/h/h1697.md): could we certainly [yada'](../../strongs/h/h3045.md) that he would ['āmar](../../strongs/h/h559.md), [yarad](../../strongs/h/h3381.md) your ['ach](../../strongs/h/h251.md) down?

<a name="genesis_43_8"></a>Genesis 43:8

And [Yehuwdah](../../strongs/h/h3063.md) ['āmar](../../strongs/h/h559.md) unto [Yisra'el](../../strongs/h/h3478.md) his ['ab](../../strongs/h/h1.md), [shalach](../../strongs/h/h7971.md) the [naʿar](../../strongs/h/h5288.md) with me, and we will [quwm](../../strongs/h/h6965.md) and [yālaḵ](../../strongs/h/h3212.md); that we may [ḥāyâ](../../strongs/h/h2421.md), and not [muwth](../../strongs/h/h4191.md), both we, and thou, and also our [ṭap̄](../../strongs/h/h2945.md).

<a name="genesis_43_9"></a>Genesis 43:9

I will be [ʿāraḇ](../../strongs/h/h6148.md) for him; of my [yad](../../strongs/h/h3027.md) shalt thou [bāqaš](../../strongs/h/h1245.md) him: if I [bow'](../../strongs/h/h935.md) him not unto thee, and [yāṣaḡ](../../strongs/h/h3322.md) him [paniym](../../strongs/h/h6440.md) thee, then let me bear the [chata'](../../strongs/h/h2398.md) [yowm](../../strongs/h/h3117.md):

<a name="genesis_43_10"></a>Genesis 43:10

For except we had [māhah](../../strongs/h/h4102.md), surely now we had [shuwb](../../strongs/h/h7725.md) this [pa'am](../../strongs/h/h6471.md).

<a name="genesis_43_11"></a>Genesis 43:11

And their ['ab](../../strongs/h/h1.md) [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto them, If it must be so now, ['asah](../../strongs/h/h6213.md) this; [laqach](../../strongs/h/h3947.md) of the [zimrâ](../../strongs/h/h2173.md) in the ['erets](../../strongs/h/h776.md) in your [kĕliy](../../strongs/h/h3627.md), and [yarad](../../strongs/h/h3381.md) the ['iysh](../../strongs/h/h376.md) a [minchah](../../strongs/h/h4503.md), a [mᵊʿaṭ](../../strongs/h/h4592.md) [ṣŏrî](../../strongs/h/h6875.md), and a [mᵊʿaṭ](../../strongs/h/h4592.md) [dĕbash](../../strongs/h/h1706.md), [nᵊḵō'ṯ](../../strongs/h/h5219.md), and [lōṭ](../../strongs/h/h3910.md), [bāṭnîm](../../strongs/h/h992.md), and [šāqēḏ](../../strongs/h/h8247.md):

<a name="genesis_43_12"></a>Genesis 43:12

And [laqach](../../strongs/h/h3947.md) [mišnê](../../strongs/h/h4932.md) [keceph](../../strongs/h/h3701.md) in your [yad](../../strongs/h/h3027.md); and the [keceph](../../strongs/h/h3701.md) that was [shuwb](../../strongs/h/h7725.md) in the [peh](../../strongs/h/h6310.md) of your ['amtaḥaṯ](../../strongs/h/h572.md), [shuwb](../../strongs/h/h7725.md) it in your [yad](../../strongs/h/h3027.md); peradventure it was a [mišgê](../../strongs/h/h4870.md):

<a name="genesis_43_13"></a>Genesis 43:13

[laqach](../../strongs/h/h3947.md) also your ['ach](../../strongs/h/h251.md), and [quwm](../../strongs/h/h6965.md), [shuwb](../../strongs/h/h7725.md) unto the ['iysh](../../strongs/h/h376.md):

<a name="genesis_43_14"></a>Genesis 43:14

And ['el](../../strongs/h/h410.md) [Šaday](../../strongs/h/h7706.md) [nathan](../../strongs/h/h5414.md) you [raḥam](../../strongs/h/h7356.md) [paniym](../../strongs/h/h6440.md) the ['iysh](../../strongs/h/h376.md), that he may [shalach](../../strongs/h/h7971.md) your ['aḥēr](../../strongs/h/h312.md) ['ach](../../strongs/h/h251.md), and [Binyāmîn](../../strongs/h/h1144.md). If I be [šāḵōl](../../strongs/h/h7921.md), I am [šāḵōl](../../strongs/h/h7921.md).

<a name="genesis_43_15"></a>Genesis 43:15

And the ['enowsh](../../strongs/h/h582.md) [laqach](../../strongs/h/h3947.md) that [minchah](../../strongs/h/h4503.md), and they [laqach](../../strongs/h/h3947.md) [mišnê](../../strongs/h/h4932.md) [keceph](../../strongs/h/h3701.md) in their [yad](../../strongs/h/h3027.md) and [Binyāmîn](../../strongs/h/h1144.md); and [quwm](../../strongs/h/h6965.md), and [yarad](../../strongs/h/h3381.md) to [Mitsrayim](../../strongs/h/h4714.md), and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Yôsēp̄](../../strongs/h/h3130.md).

<a name="genesis_43_16"></a>Genesis 43:16

And when [Yôsēp̄](../../strongs/h/h3130.md) [ra'ah](../../strongs/h/h7200.md) [Binyāmîn](../../strongs/h/h1144.md) with them, he ['āmar](../../strongs/h/h559.md) to the ruler of his [bayith](../../strongs/h/h1004.md), [bow'](../../strongs/h/h935.md) these ['enowsh](../../strongs/h/h582.md) [bayith](../../strongs/h/h1004.md), and [ṭāḇaḥ](../../strongs/h/h2873.md) [ṭeḇaḥ](../../strongs/h/h2874.md), and make [kuwn](../../strongs/h/h3559.md); for these ['enowsh](../../strongs/h/h582.md) shall ['akal](../../strongs/h/h398.md) with me at [ṣōhar](../../strongs/h/h6672.md).

<a name="genesis_43_17"></a>Genesis 43:17

And the ['iysh](../../strongs/h/h376.md) ['asah](../../strongs/h/h6213.md) as [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md); and the ['iysh](../../strongs/h/h376.md) [bow'](../../strongs/h/h935.md) the ['enowsh](../../strongs/h/h582.md) into [Yôsēp̄](../../strongs/h/h3130.md) [bayith](../../strongs/h/h1004.md).

<a name="genesis_43_18"></a>Genesis 43:18

And the ['enowsh](../../strongs/h/h582.md) were [yare'](../../strongs/h/h3372.md), because they were [bow'](../../strongs/h/h935.md) into [Yôsēp̄](../../strongs/h/h3130.md) [bayith](../../strongs/h/h1004.md); and they ['āmar](../../strongs/h/h559.md), [dabar](../../strongs/h/h1697.md) of the [keceph](../../strongs/h/h3701.md) that was [shuwb](../../strongs/h/h7725.md) in our ['amtaḥaṯ](../../strongs/h/h572.md) at the [tᵊḥillâ](../../strongs/h/h8462.md) are we [bow'](../../strongs/h/h935.md) in; that he may [gālal](../../strongs/h/h1556.md) against us, and [naphal](../../strongs/h/h5307.md) upon us, and [laqach](../../strongs/h/h3947.md) us for ['ebed](../../strongs/h/h5650.md), and our [chamowr](../../strongs/h/h2543.md).

<a name="genesis_43_19"></a>Genesis 43:19

And they [nāḡaš](../../strongs/h/h5066.md) to the ['iysh](../../strongs/h/h376.md) of [Yôsēp̄](../../strongs/h/h3130.md) [bayith](../../strongs/h/h1004.md), and they [dabar](../../strongs/h/h1696.md) with him at the [peṯaḥ](../../strongs/h/h6607.md) of the [bayith](../../strongs/h/h1004.md),

<a name="genesis_43_20"></a>Genesis 43:20

And ['āmar](../../strongs/h/h559.md), [bî](../../strongs/h/h994.md) ['adown](../../strongs/h/h113.md), we [yarad](../../strongs/h/h3381.md) [yarad](../../strongs/h/h3381.md) at the [tᵊḥillâ](../../strongs/h/h8462.md) to [šāḇar](../../strongs/h/h7666.md) ['ōḵel](../../strongs/h/h400.md):

<a name="genesis_43_21"></a>Genesis 43:21

And it came to pass, when we [bow'](../../strongs/h/h935.md) to the [mālôn](../../strongs/h/h4411.md), that we [pāṯaḥ](../../strongs/h/h6605.md) our ['amtaḥaṯ](../../strongs/h/h572.md), and, behold, every ['iysh](../../strongs/h/h376.md) [keceph](../../strongs/h/h3701.md) was in the [peh](../../strongs/h/h6310.md) of his ['amtaḥaṯ](../../strongs/h/h572.md), our [keceph](../../strongs/h/h3701.md) in full [mišqāl](../../strongs/h/h4948.md): and we have [shuwb](../../strongs/h/h7725.md) in our [yad](../../strongs/h/h3027.md).

<a name="genesis_43_22"></a>Genesis 43:22

And ['aḥēr](../../strongs/h/h312.md) [keceph](../../strongs/h/h3701.md) have we [yarad](../../strongs/h/h3381.md) in our [yad](../../strongs/h/h3027.md) to [šāḇar](../../strongs/h/h7666.md) ['ōḵel](../../strongs/h/h400.md): we cannot [yada'](../../strongs/h/h3045.md) who [śûm](../../strongs/h/h7760.md) our [keceph](../../strongs/h/h3701.md) in our ['amtaḥaṯ](../../strongs/h/h572.md).

<a name="genesis_43_23"></a>Genesis 43:23

And he ['āmar](../../strongs/h/h559.md), [shalowm](../../strongs/h/h7965.md) be to you, [yare'](../../strongs/h/h3372.md) not: your ['Elohiym](../../strongs/h/h430.md), and the ['Elohiym](../../strongs/h/h430.md) of your ['ab](../../strongs/h/h1.md), hath [nathan](../../strongs/h/h5414.md) you [maṭmôn](../../strongs/h/h4301.md) in your ['amtaḥaṯ](../../strongs/h/h572.md): I [bow'](../../strongs/h/h935.md) your [keceph](../../strongs/h/h3701.md). And he [yāṣā'](../../strongs/h/h3318.md) [Šimʿôn](../../strongs/h/h8095.md) out unto them.

<a name="genesis_43_24"></a>Genesis 43:24

And the ['iysh](../../strongs/h/h376.md) [bow'](../../strongs/h/h935.md) the ['enowsh](../../strongs/h/h582.md) into [Yôsēp̄](../../strongs/h/h3130.md) [bayith](../../strongs/h/h1004.md), and [nathan](../../strongs/h/h5414.md) them [mayim](../../strongs/h/h4325.md), and they [rāḥaṣ](../../strongs/h/h7364.md) their [regel](../../strongs/h/h7272.md); and he gave their [chamowr](../../strongs/h/h2543.md) [mispô'](../../strongs/h/h4554.md).

<a name="genesis_43_25"></a>Genesis 43:25

And they made [kuwn](../../strongs/h/h3559.md) the [minchah](../../strongs/h/h4503.md) against [Yôsēp̄](../../strongs/h/h3130.md) [bow'](../../strongs/h/h935.md) at [ṣōhar](../../strongs/h/h6672.md): for they [shama'](../../strongs/h/h8085.md) that they should ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) there.

<a name="genesis_43_26"></a>Genesis 43:26

And when [Yôsēp̄](../../strongs/h/h3130.md) [bow'](../../strongs/h/h935.md) [bayith](../../strongs/h/h1004.md), they [bow'](../../strongs/h/h935.md) him the [minchah](../../strongs/h/h4503.md) which was in their [yad](../../strongs/h/h3027.md) into the [bayith](../../strongs/h/h1004.md), and [shachah](../../strongs/h/h7812.md) themselves to him to the ['erets](../../strongs/h/h776.md).

<a name="genesis_43_27"></a>Genesis 43:27

And he [sha'al](../../strongs/h/h7592.md) them of their [shalowm](../../strongs/h/h7965.md), and ['āmar](../../strongs/h/h559.md), Is your ['ab](../../strongs/h/h1.md) [shalowm](../../strongs/h/h7965.md), the [zāqēn](../../strongs/h/h2205.md) man of whom ye ['āmar](../../strongs/h/h559.md)? Is he yet [chay](../../strongs/h/h2416.md)?

<a name="genesis_43_28"></a>Genesis 43:28

And they ['āmar](../../strongs/h/h559.md), Thy ['ebed](../../strongs/h/h5650.md) our ['ab](../../strongs/h/h1.md) is [shalowm](../../strongs/h/h7965.md), he is yet [chay](../../strongs/h/h2416.md). And they [qāḏaḏ](../../strongs/h/h6915.md), and [shachah](../../strongs/h/h7812.md).

<a name="genesis_43_29"></a>Genesis 43:29

And he [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md) his ['ach](../../strongs/h/h251.md) [Binyāmîn](../../strongs/h/h1144.md), his ['em](../../strongs/h/h517.md) [ben](../../strongs/h/h1121.md), and ['āmar](../../strongs/h/h559.md), Is this your [qāṭān](../../strongs/h/h6996.md) ['ach](../../strongs/h/h251.md), of whom ye ['āmar](../../strongs/h/h559.md) unto me? And he ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) be [chanan](../../strongs/h/h2603.md) unto thee, my [ben](../../strongs/h/h1121.md).

<a name="genesis_43_30"></a>Genesis 43:30

And [Yôsēp̄](../../strongs/h/h3130.md) made [māhar](../../strongs/h/h4116.md); for his [raḥam](../../strongs/h/h7356.md) did [kāmar](../../strongs/h/h3648.md) upon his ['ach](../../strongs/h/h251.md): and he [bāqaš](../../strongs/h/h1245.md) where to [bāḵâ](../../strongs/h/h1058.md); and he [bow'](../../strongs/h/h935.md) into his [ḥeḏer](../../strongs/h/h2315.md), and [bāḵâ](../../strongs/h/h1058.md) there.

<a name="genesis_43_31"></a>Genesis 43:31

And he [rāḥaṣ](../../strongs/h/h7364.md) his [paniym](../../strongs/h/h6440.md), and [yāṣā'](../../strongs/h/h3318.md), and ['āp̄aq](../../strongs/h/h662.md) himself, and ['āmar](../../strongs/h/h559.md), [śûm](../../strongs/h/h7760.md) on [lechem](../../strongs/h/h3899.md).

<a name="genesis_43_32"></a>Genesis 43:32

And they [śûm](../../strongs/h/h7760.md) on for him by himself, and for them by themselves, and for the [Miṣrî](../../strongs/h/h4713.md), which did ['akal](../../strongs/h/h398.md) with him, by themselves: because the [Miṣrî](../../strongs/h/h4713.md) [yakol](../../strongs/h/h3201.md) not ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) with the [ʿiḇrî](../../strongs/h/h5680.md); for that is a [tôʿēḇâ](../../strongs/h/h8441.md) unto the [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_43_33"></a>Genesis 43:33

And they [yashab](../../strongs/h/h3427.md) [paniym](../../strongs/h/h6440.md) him, the [bᵊḵôr](../../strongs/h/h1060.md) according to his [bᵊḵôrâ](../../strongs/h/h1062.md), and the [ṣāʿîr](../../strongs/h/h6810.md) according to his [ṣᵊʿîrâ](../../strongs/h/h6812.md): and the ['enowsh](../../strongs/h/h582.md) [tāmah](../../strongs/h/h8539.md) ['iysh](../../strongs/h/h376.md) at [rea'](../../strongs/h/h7453.md).

<a name="genesis_43_34"></a>Genesis 43:34

And he [nasa'](../../strongs/h/h5375.md) and sent [maśśᵊ'ēṯ](../../strongs/h/h4864.md) unto them from [paniym](../../strongs/h/h6440.md) him: but [Binyāmîn](../../strongs/h/h1144.md) [maśśᵊ'ēṯ](../../strongs/h/h4864.md) was five [yad](../../strongs/h/h3027.md) [rabah](../../strongs/h/h7235.md) any of their's. And they [šāṯâ](../../strongs/h/h8354.md), and were [šāḵar](../../strongs/h/h7937.md) with him.

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 42](genesis_42.md) - [Genesis 44](genesis_44.md)