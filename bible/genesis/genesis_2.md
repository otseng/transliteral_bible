# [Genesis 2](https://www.blueletterbible.org/kjv/gen/2/1/s_1001)

<a name="genesis_2_1"></a>Genesis 2:1

Thus the [shamayim](../../strongs/h/h8064.md) and the ['erets](../../strongs/h/h776.md) were [kalah](../../strongs/h/h3615.md), and all the [tsaba'](../../strongs/h/h6635.md).

<a name="genesis_2_2"></a>Genesis 2:2

And on the seventh [yowm](../../strongs/h/h3117.md) ['Elohiym](../../strongs/h/h430.md) [kalah](../../strongs/h/h3615.md) his [mĕla'kah](../../strongs/h/h4399.md) which he had ['asah](../../strongs/h/h6213.md); and he [shabath](../../strongs/h/h7673.md) on the seventh [yowm](../../strongs/h/h3117.md) from all his [mĕla'kah](../../strongs/h/h4399.md) which he had ['asah](../../strongs/h/h6213.md).

<a name="genesis_2_3"></a>Genesis 2:3

And ['Elohiym](../../strongs/h/h430.md) [barak](../../strongs/h/h1288.md) the seventh [yowm](../../strongs/h/h3117.md), and [qadash](../../strongs/h/h6942.md) it: because that in it he had [shabath](../../strongs/h/h7673.md) from all his [mĕla'kah](../../strongs/h/h4399.md) which ['Elohiym](../../strongs/h/h430.md) [bara'](../../strongs/h/h1254.md) and ['asah](../../strongs/h/h6213.md).

<a name="genesis_2_4"></a>Genesis 2:4

These are the [towlĕdah](../../strongs/h/h8435.md) of the [shamayim](../../strongs/h/h8064.md) and of the ['erets](../../strongs/h/h776.md) when they were [bara'](../../strongs/h/h1254.md), in the [yowm](../../strongs/h/h3117.md) that [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) the ['erets](../../strongs/h/h776.md) and the [shamayim](../../strongs/h/h8064.md),

<a name="genesis_2_5"></a>Genesis 2:5

And every [śîaḥ](../../strongs/h/h7880.md) of the [sadeh](../../strongs/h/h7704.md) before it was in the ['erets](../../strongs/h/h776.md), and every ['eseb](../../strongs/h/h6212.md) of the [sadeh](../../strongs/h/h7704.md) before it [ṣāmaḥ](../../strongs/h/h6779.md): for [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) had not caused it to [matar](../../strongs/h/h4305.md) upon the ['erets](../../strongs/h/h776.md), and there was not an ['adam](../../strongs/h/h120.md) to ['abad](../../strongs/h/h5647.md) the ['adamah](../../strongs/h/h127.md).

<a name="genesis_2_6"></a>Genesis 2:6

But there [ʿālâ](../../strongs/h/h5927.md) an ['ēḏ](../../strongs/h/h108.md) from the ['erets](../../strongs/h/h776.md), and [šāqâ](../../strongs/h/h8248.md) the [paniym](../../strongs/h/h6440.md) of the ['adamah](../../strongs/h/h127.md).

<a name="genesis_2_7"></a>Genesis 2:7

And [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) [yāṣar](../../strongs/h/h3335.md) ['adam](../../strongs/h/h120.md) of the ['aphar](../../strongs/h/h6083.md) of the ['adamah](../../strongs/h/h127.md), and [nāp̄aḥ](../../strongs/h/h5301.md) into his ['aph](../../strongs/h/h639.md) the [neshamah](../../strongs/h/h5397.md) of [chay](../../strongs/h/h2416.md); and ['adam](../../strongs/h/h120.md) became a [chay](../../strongs/h/h2416.md) [nephesh](../../strongs/h/h5315.md).

<a name="genesis_2_8"></a>Genesis 2:8

And [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) [nāṭaʿ](../../strongs/h/h5193.md) a [gan](../../strongs/h/h1588.md) [qeḏem](../../strongs/h/h6924.md) in ['Eden](../../strongs/h/h5731.md); and there he [śûm](../../strongs/h/h7760.md) the ['adam](../../strongs/h/h120.md) whom he had [yāṣar](../../strongs/h/h3335.md).

<a name="genesis_2_9"></a>Genesis 2:9

And out of the ['adamah](../../strongs/h/h127.md) made [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) to [ṣāmaḥ](../../strongs/h/h6779.md) every ['ets](../../strongs/h/h6086.md) that is [chamad](../../strongs/h/h2530.md) to the [mar'ê](../../strongs/h/h4758.md), and [towb](../../strongs/h/h2896.md) for [ma'akal](../../strongs/h/h3978.md); the ['ets](../../strongs/h/h6086.md) of [chay](../../strongs/h/h2416.md) also in the [tavek](../../strongs/h/h8432.md) of the [gan](../../strongs/h/h1588.md), and the ['ets](../../strongs/h/h6086.md) of [da'ath](../../strongs/h/h1847.md) of [towb](../../strongs/h/h2896.md) and [ra'](../../strongs/h/h7451.md).

<a name="genesis_2_10"></a>Genesis 2:10

And a [nāhār](../../strongs/h/h5104.md) [yāṣā'](../../strongs/h/h3318.md) of ['Eden](../../strongs/h/h5731.md) to [šāqâ](../../strongs/h/h8248.md) the [gan](../../strongs/h/h1588.md); and from thence it was [pāraḏ](../../strongs/h/h6504.md), and became into four [ro'sh](../../strongs/h/h7218.md).

<a name="genesis_2_11"></a>Genesis 2:11

The [shem](../../strongs/h/h8034.md) of the first is [pîšôn](../../strongs/h/h6376.md): that is it which [cabab](../../strongs/h/h5437.md) the ['erets](../../strongs/h/h776.md) of [Ḥăvîlâ](../../strongs/h/h2341.md), where there is [zāhāḇ](../../strongs/h/h2091.md);

<a name="genesis_2_12"></a>Genesis 2:12

And the [zāhāḇ](../../strongs/h/h2091.md) of that ['erets](../../strongs/h/h776.md) is [towb](../../strongs/h/h2896.md): there is [bᵊḏōlaḥ](../../strongs/h/h916.md) and the [šōham](../../strongs/h/h7718.md) ['eben](../../strongs/h/h68.md).

<a name="genesis_2_13"></a>Genesis 2:13

And the [shem](../../strongs/h/h8034.md) of the second [nāhār](../../strongs/h/h5104.md) is [gîḥôn](../../strongs/h/h1521.md): the same is it that [cabab](../../strongs/h/h5437.md) the ['erets](../../strongs/h/h776.md) of [Kûš](../../strongs/h/h3568.md).

<a name="genesis_2_14"></a>Genesis 2:14

And the [shem](../../strongs/h/h8034.md) of the third [nāhār](../../strongs/h/h5104.md) is [Ḥideqel](../../strongs/h/h2313.md): that is it which [halak](../../strongs/h/h1980.md) the [qḏmh](../../strongs/h/h6926.md) of ['Aššûr](../../strongs/h/h804.md). And the fourth [nāhār](../../strongs/h/h5104.md) is [Pᵊrāṯ](../../strongs/h/h6578.md).

<a name="genesis_2_15"></a>Genesis 2:15

And [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) [laqach](../../strongs/h/h3947.md) the ['adam](../../strongs/h/h120.md), and [yānaḥ](../../strongs/h/h3240.md) him into the [gan](../../strongs/h/h1588.md) of ['Eden](../../strongs/h/h5731.md) to ['abad](../../strongs/h/h5647.md) it and to [shamar](../../strongs/h/h8104.md) it.

<a name="genesis_2_16"></a>Genesis 2:16

And [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) [tsavah](../../strongs/h/h6680.md) the ['adam](../../strongs/h/h120.md), ['āmar](../../strongs/h/h559.md), Of every ['ets](../../strongs/h/h6086.md) of the [gan](../../strongs/h/h1588.md) thou mayest ['akal](../../strongs/h/h398.md) ['akal](../../strongs/h/h398.md):

<a name="genesis_2_17"></a>Genesis 2:17

But of the ['ets](../../strongs/h/h6086.md) of the [da'ath](../../strongs/h/h1847.md) of [towb](../../strongs/h/h2896.md) and [ra'](../../strongs/h/h7451.md), thou shalt not ['akal](../../strongs/h/h398.md) of it: for in the [yowm](../../strongs/h/h3117.md) that thou ['akal](../../strongs/h/h398.md) thereof thou shalt [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="genesis_2_18"></a>Genesis 2:18

And [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), not [towb](../../strongs/h/h2896.md) that the ['adam](../../strongs/h/h120.md) should be alone; I will ['asah](../../strongs/h/h6213.md) him an ['ezer](../../strongs/h/h5828.md) for him.

<a name="genesis_2_19"></a>Genesis 2:19

And out of the ['adamah](../../strongs/h/h127.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) [yāṣar](../../strongs/h/h3335.md) every [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md), and every [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md); and [bow'](../../strongs/h/h935.md) them unto ['adam](../../strongs/h/h120.md) to [ra'ah](../../strongs/h/h7200.md) what he would [qara'](../../strongs/h/h7121.md) them: and whatsoever ['adam](../../strongs/h/h120.md) called every [chay](../../strongs/h/h2416.md) [nephesh](../../strongs/h/h5315.md), that was the [shem](../../strongs/h/h8034.md) thereof.

<a name="genesis_2_20"></a>Genesis 2:20

And ['Adam](../../strongs/h/h120.md) [qara'](../../strongs/h/h7121.md) [shem](../../strongs/h/h8034.md) to all [bĕhemah](../../strongs/h/h929.md), and to the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and to every [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md); but for ['adam](../../strongs/h/h120.md) there was not [māṣā'](../../strongs/h/h4672.md) an ['ezer](../../strongs/h/h5828.md) for him.

<a name="genesis_2_21"></a>Genesis 2:21

And [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) caused a [tardēmâ](../../strongs/h/h8639.md) to [naphal](../../strongs/h/h5307.md) upon ['Āḏām](../../strongs/h/h121.md), and he [yashen](../../strongs/h/h3462.md): and he [laqach](../../strongs/h/h3947.md) one of his [tsela'](../../strongs/h/h6763.md), and [cagar](../../strongs/h/h5462.md) the [basar](../../strongs/h/h1320.md) instead thereof;

<a name="genesis_2_22"></a>Genesis 2:22

And the [tsela'](../../strongs/h/h6763.md), which [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) had [laqach](../../strongs/h/h3947.md) from ['adam](../../strongs/h/h120.md), [bānâ](../../strongs/h/h1129.md) he an ['ishshah](../../strongs/h/h802.md), and [bow'](../../strongs/h/h935.md) her unto the ['adam](../../strongs/h/h120.md).

<a name="genesis_2_23"></a>Genesis 2:23

And ['Adam](../../strongs/h/h120.md) ['āmar](../../strongs/h/h559.md), This is [pa'am](../../strongs/h/h6471.md) ['etsem](../../strongs/h/h6106.md) of my ['etsem](../../strongs/h/h6106.md), and [basar](../../strongs/h/h1320.md) of my [basar](../../strongs/h/h1320.md): she shall be [qara'](../../strongs/h/h7121.md) ['ishshah](../../strongs/h/h802.md), because she was [laqach](../../strongs/h/h3947.md) out of ['iysh](../../strongs/h/h376.md).

<a name="genesis_2_24"></a>Genesis 2:24

Therefore shall an ['iysh](../../strongs/h/h376.md) ['azab](../../strongs/h/h5800.md) his ['ab](../../strongs/h/h1.md) and his ['em](../../strongs/h/h517.md), and shall [dāḇaq](../../strongs/h/h1692.md) unto his ['ishshah](../../strongs/h/h802.md): and they shall be ['echad](../../strongs/h/h259.md) [basar](../../strongs/h/h1320.md).

<a name="genesis_2_25"></a>Genesis 2:25

And they were both ['arowm](../../strongs/h/h6174.md), the ['adam](../../strongs/h/h120.md) and his ['ishshah](../../strongs/h/h802.md), and were not [buwsh](../../strongs/h/h954.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 1](genesis_1.md) - [Genesis 3](genesis_3.md)