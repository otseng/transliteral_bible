# [Genesis 40](https://www.blueletterbible.org/kjv/gen/40/1/s_40001)

<a name="genesis_40_1"></a>Genesis 40:1

And it came to pass ['aḥar](../../strongs/h/h310.md) these [dabar](../../strongs/h/h1697.md), that the [šāqâ](../../strongs/h/h8248.md) of the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) and his ['āp̄â](../../strongs/h/h644.md) had [chata'](../../strongs/h/h2398.md) their ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_40_2"></a>Genesis 40:2

And [Parʿô](../../strongs/h/h6547.md) was [qāṣap̄](../../strongs/h/h7107.md) against two of his [sārîs](../../strongs/h/h5631.md), against the [śar](../../strongs/h/h8269.md) of the [šāqâ](../../strongs/h/h8248.md), and against the [śar](../../strongs/h/h8269.md) of the ['āp̄â](../../strongs/h/h644.md).

<a name="genesis_40_3"></a>Genesis 40:3

And he [nathan](../../strongs/h/h5414.md) them in [mišmār](../../strongs/h/h4929.md) in the [bayith](../../strongs/h/h1004.md) of the [śar](../../strongs/h/h8269.md) of the [ṭabāḥ](../../strongs/h/h2876.md), into the [bayith](../../strongs/h/h1004.md) [sōhar](../../strongs/h/h5470.md), the [maqowm](../../strongs/h/h4725.md) where [Yôsēp̄](../../strongs/h/h3130.md) was ['āsar](../../strongs/h/h631.md).

<a name="genesis_40_4"></a>Genesis 40:4

And the [śar](../../strongs/h/h8269.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [paqad](../../strongs/h/h6485.md) [Yôsēp̄](../../strongs/h/h3130.md) with them, and he [sharath](../../strongs/h/h8334.md) them: and they continued a [yowm](../../strongs/h/h3117.md) in [mišmār](../../strongs/h/h4929.md).

<a name="genesis_40_5"></a>Genesis 40:5

And they [ḥālam](../../strongs/h/h2492.md) a [ḥălôm](../../strongs/h/h2472.md) both of them, each ['iysh](../../strongs/h/h376.md) his [ḥălôm](../../strongs/h/h2472.md) in one [layil](../../strongs/h/h3915.md), each ['iysh](../../strongs/h/h376.md) according to the [piṯrôn](../../strongs/h/h6623.md) of his [ḥălôm](../../strongs/h/h2472.md), the [šāqâ](../../strongs/h/h8248.md) and the ['āp̄â](../../strongs/h/h644.md) of the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), which were ['āsar](../../strongs/h/h631.md) in the [bayith](../../strongs/h/h1004.md) [sōhar](../../strongs/h/h5470.md).

<a name="genesis_40_6"></a>Genesis 40:6

And [Yôsēp̄](../../strongs/h/h3130.md) [bow'](../../strongs/h/h935.md) unto them in the [boqer](../../strongs/h/h1242.md), and [ra'ah](../../strongs/h/h7200.md) upon them, and, behold, they were [zāʿap̄](../../strongs/h/h2196.md).

<a name="genesis_40_7"></a>Genesis 40:7

And he [sha'al](../../strongs/h/h7592.md) [Parʿô](../../strongs/h/h6547.md) [sārîs](../../strongs/h/h5631.md) that were with him in the [mišmār](../../strongs/h/h4929.md) of his ['adown](../../strongs/h/h113.md) [bayith](../../strongs/h/h1004.md), ['āmar](../../strongs/h/h559.md), Wherefore [paniym](../../strongs/h/h6440.md) ye so [ra'](../../strongs/h/h7451.md) to [yowm](../../strongs/h/h3117.md)?

<a name="genesis_40_8"></a>Genesis 40:8

And they ['āmar](../../strongs/h/h559.md) unto him, We have [ḥālam](../../strongs/h/h2492.md) a [ḥălôm](../../strongs/h/h2472.md), and there is no [pāṯar](../../strongs/h/h6622.md) of it. And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto them, Do not [piṯrôn](../../strongs/h/h6623.md) belong to ['Elohiym](../../strongs/h/h430.md)? [sāp̄ar](../../strongs/h/h5608.md) me them, I pray you.

<a name="genesis_40_9"></a>Genesis 40:9

And the [śar](../../strongs/h/h8269.md) [šāqâ](../../strongs/h/h8248.md) [sāp̄ar](../../strongs/h/h5608.md) his [ḥălôm](../../strongs/h/h2472.md) to [Yôsēp̄](../../strongs/h/h3130.md), and ['āmar](../../strongs/h/h559.md) to him, In my [ḥălôm](../../strongs/h/h2472.md), behold, a [gep̄en](../../strongs/h/h1612.md) was [paniym](../../strongs/h/h6440.md) me;

<a name="genesis_40_10"></a>Genesis 40:10

And in the [gep̄en](../../strongs/h/h1612.md) were three [śārîḡ](../../strongs/h/h8299.md): and it was as though it [pāraḥ](../../strongs/h/h6524.md), and her [nēṣ](../../strongs/h/h5322.md) [ʿālâ](../../strongs/h/h5927.md); and the ['eškōôl](../../strongs/h/h811.md) thereof brought forth [bāšal](../../strongs/h/h1310.md) [ʿēnāḇ](../../strongs/h/h6025.md):

<a name="genesis_40_11"></a>Genesis 40:11

And [Parʿô](../../strongs/h/h6547.md) [kowc](../../strongs/h/h3563.md) was in my [yad](../../strongs/h/h3027.md): and I [laqach](../../strongs/h/h3947.md) the [ʿēnāḇ](../../strongs/h/h6025.md), and [śāḥaṭ](../../strongs/h/h7818.md) them into [Parʿô](../../strongs/h/h6547.md) [kowc](../../strongs/h/h3563.md), and I [nathan](../../strongs/h/h5414.md) the [kowc](../../strongs/h/h3563.md) into [Parʿô](../../strongs/h/h6547.md) [kaph](../../strongs/h/h3709.md).

<a name="genesis_40_12"></a>Genesis 40:12

And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto him, This is the [piṯrôn](../../strongs/h/h6623.md) of it: The three [śārîḡ](../../strongs/h/h8299.md) are three [yowm](../../strongs/h/h3117.md):

<a name="genesis_40_13"></a>Genesis 40:13

Yet within three [yowm](../../strongs/h/h3117.md) shall [Parʿô](../../strongs/h/h6547.md) [nasa'](../../strongs/h/h5375.md) thine [ro'sh](../../strongs/h/h7218.md), and [shuwb](../../strongs/h/h7725.md) thee unto thy [kēn](../../strongs/h/h3653.md): and thou shalt [nathan](../../strongs/h/h5414.md) [Parʿô](../../strongs/h/h6547.md) [kowc](../../strongs/h/h3563.md) into his [yad](../../strongs/h/h3027.md), after the [ri'šôn](../../strongs/h/h7223.md) [mishpat](../../strongs/h/h4941.md) when thou wast his [šāqâ](../../strongs/h/h8248.md).

<a name="genesis_40_14"></a>Genesis 40:14

But [zakar](../../strongs/h/h2142.md) on me when it shall be [yatab](../../strongs/h/h3190.md) with thee, and ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md), I pray thee, unto me, and [zakar](../../strongs/h/h2142.md) of me unto [Parʿô](../../strongs/h/h6547.md), and [yāṣā'](../../strongs/h/h3318.md) out of this [bayith](../../strongs/h/h1004.md):

<a name="genesis_40_15"></a>Genesis 40:15

For indeed I was [ganab](../../strongs/h/h1589.md) out of the ['erets](../../strongs/h/h776.md) of the [ʿiḇrî](../../strongs/h/h5680.md): and here also have I ['asah](../../strongs/h/h6213.md) [mᵊ'ûmâ](../../strongs/h/h3972.md) that they should [śûm](../../strongs/h/h7760.md) me into the [bowr](../../strongs/h/h953.md).

<a name="genesis_40_16"></a>Genesis 40:16

When the [śar](../../strongs/h/h8269.md) ['āp̄â](../../strongs/h/h644.md) [ra'ah](../../strongs/h/h7200.md) that the [pāṯar](../../strongs/h/h6622.md) was [towb](../../strongs/h/h2896.md), he ['āmar](../../strongs/h/h559.md) unto [Yôsēp̄](../../strongs/h/h3130.md), I also was in my [ḥălôm](../../strongs/h/h2472.md), and, behold, I had three [ḥōrî](../../strongs/h/h2751.md) [sal](../../strongs/h/h5536.md) on my [ro'sh](../../strongs/h/h7218.md):

<a name="genesis_40_17"></a>Genesis 40:17

And in the ['elyown](../../strongs/h/h5945.md) [sal](../../strongs/h/h5536.md) there was of all [ma'akal](../../strongs/h/h3978.md) of [ma'aseh](../../strongs/h/h4639.md) ['āp̄â](../../strongs/h/h644.md) for [Parʿô](../../strongs/h/h6547.md); and the [ʿôp̄](../../strongs/h/h5775.md) did ['akal](../../strongs/h/h398.md) them out of the [sal](../../strongs/h/h5536.md) upon my [ro'sh](../../strongs/h/h7218.md).

<a name="genesis_40_18"></a>Genesis 40:18

And [Yôsēp̄](../../strongs/h/h3130.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), This is the [piṯrôn](../../strongs/h/h6623.md) thereof: The three [sal](../../strongs/h/h5536.md) are three [yowm](../../strongs/h/h3117.md):

<a name="genesis_40_19"></a>Genesis 40:19

Yet within three [yowm](../../strongs/h/h3117.md) shall [Parʿô](../../strongs/h/h6547.md) [nasa'](../../strongs/h/h5375.md) thy [ro'sh](../../strongs/h/h7218.md) from off thee, and shall [tālâ](../../strongs/h/h8518.md) thee on an ['ets](../../strongs/h/h6086.md); and the [ʿôp̄](../../strongs/h/h5775.md) shall ['akal](../../strongs/h/h398.md) thy [basar](../../strongs/h/h1320.md) from off thee.

<a name="genesis_40_20"></a>Genesis 40:20

And it came to pass the third [yowm](../../strongs/h/h3117.md), which was [Parʿô](../../strongs/h/h6547.md) [yowm](../../strongs/h/h3117.md) [yalad](../../strongs/h/h3205.md), that he ['asah](../../strongs/h/h6213.md) a [mištê](../../strongs/h/h4960.md) unto all his ['ebed](../../strongs/h/h5650.md): and he [nasa'](../../strongs/h/h5375.md) the [ro'sh](../../strongs/h/h7218.md) of the [śar](../../strongs/h/h8269.md) [šāqâ](../../strongs/h/h8248.md) and of the [śar](../../strongs/h/h8269.md) ['āp̄â](../../strongs/h/h644.md) [tavek](../../strongs/h/h8432.md) his ['ebed](../../strongs/h/h5650.md).

<a name="genesis_40_21"></a>Genesis 40:21

And he [shuwb](../../strongs/h/h7725.md) the [śar](../../strongs/h/h8269.md) [šāqâ](../../strongs/h/h8248.md) unto his [mašqê](../../strongs/h/h4945.md); and he [nathan](../../strongs/h/h5414.md) the [kowc](../../strongs/h/h3563.md) into [Parʿô](../../strongs/h/h6547.md) [kaph](../../strongs/h/h3709.md):

<a name="genesis_40_22"></a>Genesis 40:22

But he [tālâ](../../strongs/h/h8518.md) the [śar](../../strongs/h/h8269.md) ['āp̄â](../../strongs/h/h644.md): as [Yôsēp̄](../../strongs/h/h3130.md) had [pāṯar](../../strongs/h/h6622.md) to them.

<a name="genesis_40_23"></a>Genesis 40:23

Yet did not the [śar](../../strongs/h/h8269.md) [šāqâ](../../strongs/h/h8248.md) [zakar](../../strongs/h/h2142.md) [Yôsēp̄](../../strongs/h/h3130.md), but [shakach](../../strongs/h/h7911.md) him.

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 39](genesis_39.md) - [Genesis 41](genesis_41.md)