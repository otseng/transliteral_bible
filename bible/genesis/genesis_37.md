# [Genesis 37](https://www.blueletterbible.org/kjv/gen/37/1/s_37001)

<a name="genesis_37_1"></a>Genesis 37:1

And [Ya'aqob](../../strongs/h/h3290.md) [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) wherein his ['ab](../../strongs/h/h1.md) was a [māḡûr](../../strongs/h/h4033.md), in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md).

<a name="genesis_37_2"></a>Genesis 37:2

These are the [towlĕdah](../../strongs/h/h8435.md) of [Ya'aqob](../../strongs/h/h3290.md). [Yôsēp̄](../../strongs/h/h3130.md), being seventeen [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.), was [ra'ah](../../strongs/h/h7462.md) the [tso'n](../../strongs/h/h6629.md) with his ['ach](../../strongs/h/h251.md); and the [naʿar](../../strongs/h/h5288.md) was with the [ben](../../strongs/h/h1121.md) of [Bilhâ](../../strongs/h/h1090.md), and with the [ben](../../strongs/h/h1121.md) of [zilpâ](../../strongs/h/h2153.md), his ['ab](../../strongs/h/h1.md) ['ishshah](../../strongs/h/h802.md): and [Yôsēp̄](../../strongs/h/h3130.md) [bow'](../../strongs/h/h935.md) unto his ['ab](../../strongs/h/h1.md) their [ra'](../../strongs/h/h7451.md) [dibâ](../../strongs/h/h1681.md).

<a name="genesis_37_3"></a>Genesis 37:3

Now [Yisra'el](../../strongs/h/h3478.md) ['ahab](../../strongs/h/h157.md) [Yôsēp̄](../../strongs/h/h3130.md) more than all his [ben](../../strongs/h/h1121.md), because he was the [ben](../../strongs/h/h1121.md) of his [zāqun](../../strongs/h/h2208.md): and he ['asah](../../strongs/h/h6213.md) him a [kĕthoneth](../../strongs/h/h3801.md) of [pas](../../strongs/h/h6446.md).

<a name="genesis_37_4"></a>Genesis 37:4

And when his ['ach](../../strongs/h/h251.md) [ra'ah](../../strongs/h/h7200.md) that their ['ab](../../strongs/h/h1.md) ['ahab](../../strongs/h/h157.md) him more than all his ['ach](../../strongs/h/h251.md), they [sane'](../../strongs/h/h8130.md) him, and [yakol](../../strongs/h/h3201.md) not [dabar](../../strongs/h/h1696.md) [shalowm](../../strongs/h/h7965.md) unto him.

<a name="genesis_37_5"></a>Genesis 37:5

And [Yôsēp̄](../../strongs/h/h3130.md) [ḥālam](../../strongs/h/h2492.md) a [ḥălôm](../../strongs/h/h2472.md), and he [nāḡaḏ](../../strongs/h/h5046.md) it his ['ach](../../strongs/h/h251.md): and they [sane'](../../strongs/h/h8130.md) him yet the more.

<a name="genesis_37_6"></a>Genesis 37:6

And he ['āmar](../../strongs/h/h559.md) unto them, [shama'](../../strongs/h/h8085.md), I pray you, this [ḥălôm](../../strongs/h/h2472.md) which I have [ḥālam](../../strongs/h/h2492.md):

<a name="genesis_37_7"></a>Genesis 37:7

For, behold, we were ['ālam](../../strongs/h/h481.md) [tavek](../../strongs/h/h8432.md) ['ălummâ](../../strongs/h/h485.md) in the [sadeh](../../strongs/h/h7704.md), and, lo, my ['ălummâ](../../strongs/h/h485.md) [quwm](../../strongs/h/h6965.md), and also [nāṣaḇ](../../strongs/h/h5324.md); and, behold, your ['ălummâ](../../strongs/h/h485.md) [cabab](../../strongs/h/h5437.md), and [shachah](../../strongs/h/h7812.md) to my ['ălummâ](../../strongs/h/h485.md).

<a name="genesis_37_8"></a>Genesis 37:8

And his ['ach](../../strongs/h/h251.md) ['āmar](../../strongs/h/h559.md) to him, Shalt thou [mālaḵ](../../strongs/h/h4427.md) [mālaḵ](../../strongs/h/h4427.md) over us? or shalt thou [mashal](../../strongs/h/h4910.md) [mashal](../../strongs/h/h4910.md) over us? And they [sane'](../../strongs/h/h8130.md) him yet the more for his [ḥălôm](../../strongs/h/h2472.md), and for his [dabar](../../strongs/h/h1697.md).

<a name="genesis_37_9"></a>Genesis 37:9

And he [ḥālam](../../strongs/h/h2492.md) yet ['aḥēr](../../strongs/h/h312.md) [ḥălôm](../../strongs/h/h2472.md), and [sāp̄ar](../../strongs/h/h5608.md) it his ['ach](../../strongs/h/h251.md), and ['āmar](../../strongs/h/h559.md), Behold, I have [ḥālam](../../strongs/h/h2492.md) a [ḥălôm](../../strongs/h/h2472.md) more; and, behold, the [šemeš](../../strongs/h/h8121.md) and the [yareach](../../strongs/h/h3394.md) and the eleven [kowkab](../../strongs/h/h3556.md) [shachah](../../strongs/h/h7812.md) to me.

<a name="genesis_37_10"></a>Genesis 37:10

And he [sāp̄ar](../../strongs/h/h5608.md) it to his ['ab](../../strongs/h/h1.md), and to his ['ach](../../strongs/h/h251.md): and his ['ab](../../strongs/h/h1.md) [gāʿar](../../strongs/h/h1605.md) him, and ['āmar](../../strongs/h/h559.md) unto him, What is this [ḥălôm](../../strongs/h/h2472.md) that thou hast [ḥālam](../../strongs/h/h2492.md)?  Shall I and thy ['em](../../strongs/h/h517.md) and thy ['ach](../../strongs/h/h251.md) indeed [bow'](../../strongs/h/h935.md) to [shachah](../../strongs/h/h7812.md) ourselves to thee to the ['erets](../../strongs/h/h776.md)?

<a name="genesis_37_11"></a>Genesis 37:11

And his ['ach](../../strongs/h/h251.md) [qānā'](../../strongs/h/h7065.md) him; but his ['ab](../../strongs/h/h1.md) [shamar](../../strongs/h/h8104.md) the [dabar](../../strongs/h/h1697.md).

<a name="genesis_37_12"></a>Genesis 37:12

And his ['ach](../../strongs/h/h251.md) [yālaḵ](../../strongs/h/h3212.md) to [ra'ah](../../strongs/h/h7462.md) their ['ab](../../strongs/h/h1.md) [tso'n](../../strongs/h/h6629.md) in [Šᵊḵem](../../strongs/h/h7927.md).

<a name="genesis_37_13"></a>Genesis 37:13

And [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto [Yôsēp̄](../../strongs/h/h3130.md), Do not thy ['ach](../../strongs/h/h251.md) [ra'ah](../../strongs/h/h7462.md) the flock in [Šᵊḵem](../../strongs/h/h7927.md)? [yālaḵ](../../strongs/h/h3212.md), and I will [shalach](../../strongs/h/h7971.md) thee unto them. And he ['āmar](../../strongs/h/h559.md) to him, Here am I.

<a name="genesis_37_14"></a>Genesis 37:14

And he ['āmar](../../strongs/h/h559.md) to him, [yālaḵ](../../strongs/h/h3212.md), I pray thee, [ra'ah](../../strongs/h/h7200.md) whether it be [shalowm](../../strongs/h/h7965.md) with thy ['ach](../../strongs/h/h251.md), and [shalowm](../../strongs/h/h7965.md) with the [tso'n](../../strongs/h/h6629.md); and [shuwb](../../strongs/h/h7725.md) me [dabar](../../strongs/h/h1697.md) again. So he [shalach](../../strongs/h/h7971.md) him out of the [ʿēmeq](../../strongs/h/h6010.md) of [Ḥeḇrôn](../../strongs/h/h2275.md), and he [bow'](../../strongs/h/h935.md) to [Šᵊḵem](../../strongs/h/h7927.md).

<a name="genesis_37_15"></a>Genesis 37:15

And a certain ['iysh](../../strongs/h/h376.md) [māṣā'](../../strongs/h/h4672.md) him, and, behold, he was [tāʿâ](../../strongs/h/h8582.md) in the [sadeh](../../strongs/h/h7704.md): and the ['iysh](../../strongs/h/h376.md) [sha'al](../../strongs/h/h7592.md) him, ['āmar](../../strongs/h/h559.md), What [bāqaš](../../strongs/h/h1245.md) thou?

<a name="genesis_37_16"></a>Genesis 37:16

And he ['āmar](../../strongs/h/h559.md), I [bāqaš](../../strongs/h/h1245.md) my ['ach](../../strongs/h/h251.md): [nāḡaḏ](../../strongs/h/h5046.md) me, I pray thee, ['êp̄ô](../../strongs/h/h375.md) they [ra'ah](../../strongs/h/h7462.md) their flocks.

<a name="genesis_37_17"></a>Genesis 37:17

And the ['iysh](../../strongs/h/h376.md) ['āmar](../../strongs/h/h559.md), They are [nāsaʿ](../../strongs/h/h5265.md) hence; for I [shama'](../../strongs/h/h8085.md) them ['āmar](../../strongs/h/h559.md), Let us [yālaḵ](../../strongs/h/h3212.md) to [Dōṯān](../../strongs/h/h1886.md). And [Yôsēp̄](../../strongs/h/h3130.md) [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) his ['ach](../../strongs/h/h251.md), and [māṣā'](../../strongs/h/h4672.md) them in [Dōṯān](../../strongs/h/h1886.md).

<a name="genesis_37_18"></a>Genesis 37:18

And when they [ra'ah](../../strongs/h/h7200.md) him [rachowq](../../strongs/h/h7350.md), even before he [qāraḇ](../../strongs/h/h7126.md) unto them, they [nāḵal](../../strongs/h/h5230.md) against him to [muwth](../../strongs/h/h4191.md) him.

<a name="genesis_37_19"></a>Genesis 37:19

And they ['āmar](../../strongs/h/h559.md) ['iysh](../../strongs/h/h376.md) to ['ach](../../strongs/h/h251.md), Behold, this [baʿal](../../strongs/h/h1167.md) [ḥălôm](../../strongs/h/h2472.md) [bow'](../../strongs/h/h935.md).

<a name="genesis_37_20"></a>Genesis 37:20

[yālaḵ](../../strongs/h/h3212.md) now therefore, and let us [harag](../../strongs/h/h2026.md) him, and [shalak](../../strongs/h/h7993.md) him into some [bowr](../../strongs/h/h953.md), and we will ['āmar](../../strongs/h/h559.md), Some [ra'](../../strongs/h/h7451.md) [chay](../../strongs/h/h2416.md) hath ['akal](../../strongs/h/h398.md) him: and we shall [ra'ah](../../strongs/h/h7200.md) what will become of his [ḥălôm](../../strongs/h/h2472.md).

<a name="genesis_37_21"></a>Genesis 37:21

And [Rᵊ'ûḇēn](../../strongs/h/h7205.md) [shama'](../../strongs/h/h8085.md) it, and he [natsal](../../strongs/h/h5337.md) him out of their [yad](../../strongs/h/h3027.md); and ['āmar](../../strongs/h/h559.md), Let us not [nakah](../../strongs/h/h5221.md) [nephesh](../../strongs/h/h5315.md) him.

<a name="genesis_37_22"></a>Genesis 37:22

And [Rᵊ'ûḇēn](../../strongs/h/h7205.md) ['āmar](../../strongs/h/h559.md) unto them, [šāp̄aḵ](../../strongs/h/h8210.md) no [dam](../../strongs/h/h1818.md), but [shalak](../../strongs/h/h7993.md) him into this [bowr](../../strongs/h/h953.md) that is in the [midbar](../../strongs/h/h4057.md), and [shalach](../../strongs/h/h7971.md) no [yad](../../strongs/h/h3027.md) upon him; that he might [natsal](../../strongs/h/h5337.md) him out of their [yad](../../strongs/h/h3027.md), to [shuwb](../../strongs/h/h7725.md) him to his ['ab](../../strongs/h/h1.md) again.

<a name="genesis_37_23"></a>Genesis 37:23

And it came to pass, when [Yôsēp̄](../../strongs/h/h3130.md) was [bow'](../../strongs/h/h935.md) unto his ['ach](../../strongs/h/h251.md), that they [pāšaṭ](../../strongs/h/h6584.md) [Yôsēp̄](../../strongs/h/h3130.md) out of his [kĕthoneth](../../strongs/h/h3801.md), his [kĕthoneth](../../strongs/h/h3801.md) of many [pas](../../strongs/h/h6446.md) that was on him;

<a name="genesis_37_24"></a>Genesis 37:24

And they [laqach](../../strongs/h/h3947.md) him, and [shalak](../../strongs/h/h7993.md) him into a [bowr](../../strongs/h/h953.md): and the [bowr](../../strongs/h/h953.md) was [reyq](../../strongs/h/h7386.md), there was no [mayim](../../strongs/h/h4325.md) in it.

<a name="genesis_37_25"></a>Genesis 37:25

And they [yashab](../../strongs/h/h3427.md) to ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md): and they [nasa'](../../strongs/h/h5375.md) their ['ayin](../../strongs/h/h5869.md) and [ra'ah](../../strongs/h/h7200.md), and, behold, a ['ōrḥâ](../../strongs/h/h736.md) of [Yišmᵊʿē'lî](../../strongs/h/h3459.md) [bow'](../../strongs/h/h935.md) from [Gilʿāḏ](../../strongs/h/h1568.md) with their [gāmāl](../../strongs/h/h1581.md) [nasa'](../../strongs/h/h5375.md) [nᵊḵō'ṯ](../../strongs/h/h5219.md) and [ṣŏrî](../../strongs/h/h6875.md) and [lōṭ](../../strongs/h/h3910.md), [halak](../../strongs/h/h1980.md) to [yarad](../../strongs/h/h3381.md) to [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_37_26"></a>Genesis 37:26

And [Yehuwdah](../../strongs/h/h3063.md) ['āmar](../../strongs/h/h559.md) unto his ['ach](../../strongs/h/h251.md), What [beṣaʿ](../../strongs/h/h1215.md) is it if we [harag](../../strongs/h/h2026.md) our ['ach](../../strongs/h/h251.md), and [kāsâ](../../strongs/h/h3680.md) his [dam](../../strongs/h/h1818.md)?

<a name="genesis_37_27"></a>Genesis 37:27

[yālaḵ](../../strongs/h/h3212.md), and let us [māḵar](../../strongs/h/h4376.md) him to the [Yišmᵊʿē'lî](../../strongs/h/h3459.md), and let not our [yad](../../strongs/h/h3027.md) be upon him; for he is our ['ach](../../strongs/h/h251.md) and our [basar](../../strongs/h/h1320.md). And his ['ach](../../strongs/h/h251.md) were [shama'](../../strongs/h/h8085.md).

<a name="genesis_37_28"></a>Genesis 37:28

Then there ['abar](../../strongs/h/h5674.md) by [Miḏyānî](../../strongs/h/h4084.md) ['enowsh](../../strongs/h/h582.md) [sāḥar](../../strongs/h/h5503.md); and they [mashak](../../strongs/h/h4900.md) and [ʿālâ](../../strongs/h/h5927.md) [Yôsēp̄](../../strongs/h/h3130.md) out of the [bowr](../../strongs/h/h953.md), and [māḵar](../../strongs/h/h4376.md) [Yôsēp̄](../../strongs/h/h3130.md) to the [Yišmᵊʿē'lî](../../strongs/h/h3459.md) for twenty pieces of [keceph](../../strongs/h/h3701.md): and they [bow'](../../strongs/h/h935.md) [Yôsēp̄](../../strongs/h/h3130.md) into [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_37_29"></a>Genesis 37:29

And [Rᵊ'ûḇēn](../../strongs/h/h7205.md) [shuwb](../../strongs/h/h7725.md) unto the [bowr](../../strongs/h/h953.md); and, behold, [Yôsēp̄](../../strongs/h/h3130.md) was not in the [bowr](../../strongs/h/h953.md); and he [qāraʿ](../../strongs/h/h7167.md) his [beḡeḏ](../../strongs/h/h899.md).

<a name="genesis_37_30"></a>Genesis 37:30

And he [shuwb](../../strongs/h/h7725.md) unto his ['ach](../../strongs/h/h251.md), and ['āmar](../../strongs/h/h559.md), The [yeleḏ](../../strongs/h/h3206.md) is not; and I, whither shall I [bow'](../../strongs/h/h935.md)?

<a name="genesis_37_31"></a>Genesis 37:31

And they [laqach](../../strongs/h/h3947.md) [Yôsēp̄](../../strongs/h/h3130.md) [kĕthoneth](../../strongs/h/h3801.md), and [šāḥaṭ](../../strongs/h/h7819.md) a [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md), and [ṭāḇal](../../strongs/h/h2881.md) the [kĕthoneth](../../strongs/h/h3801.md) in the [dam](../../strongs/h/h1818.md);

<a name="genesis_37_32"></a>Genesis 37:32

And they [shalach](../../strongs/h/h7971.md) the [kĕthoneth](../../strongs/h/h3801.md) of [pas](../../strongs/h/h6446.md), and they [bow'](../../strongs/h/h935.md) it to their ['ab](../../strongs/h/h1.md); and ['āmar](../../strongs/h/h559.md), This have we [māṣā'](../../strongs/h/h4672.md): [nāḵar](../../strongs/h/h5234.md) now whether it be thy [ben](../../strongs/h/h1121.md) [kĕthoneth](../../strongs/h/h3801.md) or no.

<a name="genesis_37_33"></a>Genesis 37:33

And he [nāḵar](../../strongs/h/h5234.md) it, and ['āmar](../../strongs/h/h559.md), It is my [ben](../../strongs/h/h1121.md) [kĕthoneth](../../strongs/h/h3801.md); a [ra'](../../strongs/h/h7451.md) [chay](../../strongs/h/h2416.md) hath ['akal](../../strongs/h/h398.md) him; [Yôsēp̄](../../strongs/h/h3130.md) is [taraph](../../strongs/h/h2963.md) [taraph](../../strongs/h/h2963.md).

<a name="genesis_37_34"></a>Genesis 37:34

And [Ya'aqob](../../strongs/h/h3290.md) [qāraʿ](../../strongs/h/h7167.md) his [śimlâ](../../strongs/h/h8071.md), and [śûm](../../strongs/h/h7760.md) [śaq](../../strongs/h/h8242.md) upon his [māṯnayim](../../strongs/h/h4975.md), and ['āḇal](../../strongs/h/h56.md) for his [ben](../../strongs/h/h1121.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md).

<a name="genesis_37_35"></a>Genesis 37:35

And all his [ben](../../strongs/h/h1121.md) and all his [bath](../../strongs/h/h1323.md) [quwm](../../strongs/h/h6965.md) to [nacham](../../strongs/h/h5162.md) him; but he [mā'ēn](../../strongs/h/h3985.md) to be [nacham](../../strongs/h/h5162.md); and he ['āmar](../../strongs/h/h559.md), For I will [yarad](../../strongs/h/h3381.md) into the [shĕ'owl](../../strongs/h/h7585.md) unto my [ben](../../strongs/h/h1121.md) ['āḇēl](../../strongs/h/h57.md). Thus his ['ab](../../strongs/h/h1.md) [bāḵâ](../../strongs/h/h1058.md) for him.

<a name="genesis_37_36"></a>Genesis 37:36

And the [mᵊḏānî](../../strongs/h/h4092.md) [māḵar](../../strongs/h/h4376.md) him into [Mitsrayim](../../strongs/h/h4714.md) unto [p̄vṭyp̄r](../../strongs/h/h6318.md), a [sārîs](../../strongs/h/h5631.md) of [Parʿô](../../strongs/h/h6547.md), and [śar](../../strongs/h/h8269.md) of the [ṭabāḥ](../../strongs/h/h2876.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 36](genesis_36.md) - [Genesis 38](genesis_38.md)