# [Genesis](https://www.blueletterbible.org/kjv/gen/10/1/s_10001)

<a name="genesis_10_1"></a>Genesis 10:1

Now these are the [towlĕdah](../../strongs/h/h8435.md) of the [ben](../../strongs/h/h1121.md) of [Nōaḥ](../../strongs/h/h5146.md), [Šēm](../../strongs/h/h8035.md), [Ḥām](../../strongs/h/h2526.md), and [Yep̄eṯ](../../strongs/h/h3315.md): and unto them were [ben](../../strongs/h/h1121.md) [yalad](../../strongs/h/h3205.md) ['aḥar](../../strongs/h/h310.md) the [mabûl](../../strongs/h/h3999.md).

<a name="genesis_10_2"></a>Genesis 10:2

The [ben](../../strongs/h/h1121.md) of [Yep̄eṯ](../../strongs/h/h3315.md); [Gōmer](../../strongs/h/h1586.md), and [Māḡôḡ](../../strongs/h/h4031.md), and [Māḏay](../../strongs/h/h4074.md), and [Yāvān](../../strongs/h/h3120.md), and [Tuḇal](../../strongs/h/h8422.md), and [Mešeḵ](../../strongs/h/h4902.md), and [Tîrās](../../strongs/h/h8494.md).

<a name="genesis_10_3"></a>Genesis 10:3

And the [ben](../../strongs/h/h1121.md) of [Gōmer](../../strongs/h/h1586.md); ['Aškᵊnaz](../../strongs/h/h813.md), and [Rîp̄aṯ](../../strongs/h/h7384.md), and [Tōḡarmâ](../../strongs/h/h8425.md).

<a name="genesis_10_4"></a>Genesis 10:4

And the [ben](../../strongs/h/h1121.md) of [Yāvān](../../strongs/h/h3120.md); ['Ĕlîšâ](../../strongs/h/h473.md), and [Taršîš](../../strongs/h/h8659.md), [Kitîm](../../strongs/h/h3794.md), and [Dōḏānîm](../../strongs/h/h1721.md).

<a name="genesis_10_5"></a>Genesis 10:5

By these were the ['î](../../strongs/h/h339.md) of the [gowy](../../strongs/h/h1471.md) [pāraḏ](../../strongs/h/h6504.md) in their ['erets](../../strongs/h/h776.md); every ['iysh](../../strongs/h/h376.md) after his [lashown](../../strongs/h/h3956.md), after their [mišpāḥâ](../../strongs/h/h4940.md), in their [gowy](../../strongs/h/h1471.md).

<a name="genesis_10_6"></a>Genesis 10:6

And the [ben](../../strongs/h/h1121.md) of [Ḥām](../../strongs/h/h2526.md); [Kûš](../../strongs/h/h3568.md), and [Mitsrayim](../../strongs/h/h4714.md), and [Pûṭ](../../strongs/h/h6316.md), and [Kĕna'an](../../strongs/h/h3667.md).

<a name="genesis_10_7"></a>Genesis 10:7

And the [ben](../../strongs/h/h1121.md) of [Kûš](../../strongs/h/h3568.md); [Sᵊḇā'](../../strongs/h/h5434.md), and [Ḥăvîlâ](../../strongs/h/h2341.md), and [Saḇtā'](../../strongs/h/h5454.md), and [Raʿmâ](../../strongs/h/h7484.md), and [Saḇtᵊḵā'](../../strongs/h/h5455.md): and the [ben](../../strongs/h/h1121.md) of [Raʿmâ](../../strongs/h/h7484.md); [Šᵊḇā'](../../strongs/h/h7614.md), and [Dᵊḏān](../../strongs/h/h1719.md).

<a name="genesis_10_8"></a>Genesis 10:8

And [Kûš](../../strongs/h/h3568.md) [yalad](../../strongs/h/h3205.md) [Nimrōḏ](../../strongs/h/h5248.md): he [ḥālal](../../strongs/h/h2490.md) to be a [gibôr](../../strongs/h/h1368.md) in the ['erets](../../strongs/h/h776.md).

<a name="genesis_10_9"></a>Genesis 10:9

He was a [gibôr](../../strongs/h/h1368.md) [ṣayiḏ](../../strongs/h/h6718.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): wherefore it is ['āmar](../../strongs/h/h559.md), Even as [Nimrōḏ](../../strongs/h/h5248.md) the [gibôr](../../strongs/h/h1368.md) [ṣayiḏ](../../strongs/h/h6718.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="genesis_10_10"></a>Genesis 10:10

And the [re'shiyth](../../strongs/h/h7225.md) of his [mamlāḵâ](../../strongs/h/h4467.md) was [Bāḇel](../../strongs/h/h894.md), and ['ereḵ](../../strongs/h/h751.md), and ['akaḏ](../../strongs/h/h390.md), and [kalnê](../../strongs/h/h3641.md), in the ['erets](../../strongs/h/h776.md) of [Šinʿār](../../strongs/h/h8152.md).

<a name="genesis_10_11"></a>Genesis 10:11

Out of that ['erets](../../strongs/h/h776.md) [yāṣā'](../../strongs/h/h3318.md) ['Aššûr](../../strongs/h/h804.md), and [bānâ](../../strongs/h/h1129.md) [Nînvê](../../strongs/h/h5210.md), and the [ʿîr](../../strongs/h/h5892.md) [Rᵊḥōḇôṯ](../../strongs/h/h7344.md), and [kelaḥ](../../strongs/h/h3625.md),

<a name="genesis_10_12"></a>Genesis 10:12

And [resen](../../strongs/h/h7449.md) between [Nînvê](../../strongs/h/h5210.md) and [kelaḥ](../../strongs/h/h3625.md): the same is a [gadowl](../../strongs/h/h1419.md) [ʿîr](../../strongs/h/h5892.md).

<a name="genesis_10_13"></a>Genesis 10:13

And [Mitsrayim](../../strongs/h/h4714.md) [yalad](../../strongs/h/h3205.md) [Lûḏîy](../../strongs/h/h3866.md), and [ʿĂnāmîm](../../strongs/h/h6047.md), and [Lᵊhāḇîm](../../strongs/h/h3853.md), and [Nap̄tuḥîm](../../strongs/h/h5320.md),

<a name="genesis_10_14"></a>Genesis 10:14

And [Paṯrusî](../../strongs/h/h6625.md), and [Kasluḥîm](../../strongs/h/h3695.md), (out of whom [yāṣā'](../../strongs/h/h3318.md) [Pᵊlištî](../../strongs/h/h6430.md),) and [Kap̄tōrî](../../strongs/h/h3732.md).

<a name="genesis_10_15"></a>Genesis 10:15

And [Kĕna'an](../../strongs/h/h3667.md) [yalad](../../strongs/h/h3205.md) [Ṣîḏôn](../../strongs/h/h6721.md) his [bᵊḵôr](../../strongs/h/h1060.md), and [Ḥēṯ](../../strongs/h/h2845.md),

<a name="genesis_10_16"></a>Genesis 10:16

And the [Yᵊḇûsî](../../strongs/h/h2983.md), and the ['Ĕmōrî](../../strongs/h/h567.md), and the [Girgāšî](../../strongs/h/h1622.md),

<a name="genesis_10_17"></a>Genesis 10:17

And the [Ḥiûî](../../strongs/h/h2340.md), and the [ʿArqî](../../strongs/h/h6208.md), and the [Sînî](../../strongs/h/h5513.md),

<a name="genesis_10_18"></a>Genesis 10:18

And the ['Arvāḏî](../../strongs/h/h721.md), and the [Ṣᵊmārî](../../strongs/h/h6786.md), and the [Ḥămāṯî](../../strongs/h/h2577.md): and ['aḥar](../../strongs/h/h310.md) were the [mišpāḥâ](../../strongs/h/h4940.md) of the [Kᵊnaʿănî](../../strongs/h/h3669.md) [puwts](../../strongs/h/h6327.md).

<a name="genesis_10_19"></a>Genesis 10:19

And the [gᵊḇûl](../../strongs/h/h1366.md) of the [Kᵊnaʿănî](../../strongs/h/h3669.md) was from [Ṣîḏôn](../../strongs/h/h6721.md), as thou [bow'](../../strongs/h/h935.md) to [gᵊrār](../../strongs/h/h1642.md), unto [ʿAzzâ](../../strongs/h/h5804.md); as thou [bow'](../../strongs/h/h935.md), unto [Sᵊḏōm](../../strongs/h/h5467.md), and [ʿĂmōrâ](../../strongs/h/h6017.md), and ['Aḏmâ](../../strongs/h/h126.md), and [Ṣᵊḇā'îm](../../strongs/h/h6636.md), even unto [lešaʿ](../../strongs/h/h3962.md).

<a name="genesis_10_20"></a>Genesis 10:20

These are the [ben](../../strongs/h/h1121.md) of [Ḥām](../../strongs/h/h2526.md), after their [mišpāḥâ](../../strongs/h/h4940.md), after their [lashown](../../strongs/h/h3956.md), in their ['erets](../../strongs/h/h776.md), and in their [gowy](../../strongs/h/h1471.md).

<a name="genesis_10_21"></a>Genesis 10:21

Unto [Šēm](../../strongs/h/h8035.md) also, the ['ab](../../strongs/h/h1.md) of all the [ben](../../strongs/h/h1121.md) of [ʿēḇer](../../strongs/h/h5677.md), the ['ach](../../strongs/h/h251.md) of [Yep̄eṯ](../../strongs/h/h3315.md) the [gadowl](../../strongs/h/h1419.md), even to him were children [yalad](../../strongs/h/h3205.md).

<a name="genesis_10_22"></a>Genesis 10:22

The [ben](../../strongs/h/h1121.md) of [Šēm](../../strongs/h/h8035.md); [ʿÊlām](../../strongs/h/h5867.md), and ['Aššûr](../../strongs/h/h804.md), and ['Arpaḵšaḏ](../../strongs/h/h775.md), and [Lûḏ](../../strongs/h/h3865.md), and ['Ărām](../../strongs/h/h758.md).

<a name="genesis_10_23"></a>Genesis 10:23

And the [ben](../../strongs/h/h1121.md) of ['Ărām](../../strongs/h/h758.md); [ʿÛṣ](../../strongs/h/h5780.md), and [Ḥûl](../../strongs/h/h2343.md), and [Geṯer](../../strongs/h/h1666.md), and [maš](../../strongs/h/h4851.md).

<a name="genesis_10_24"></a>Genesis 10:24

And ['Arpaḵšaḏ](../../strongs/h/h775.md) [yalad](../../strongs/h/h3205.md) [Šelaḥ](../../strongs/h/h7974.md); and [Šelaḥ](../../strongs/h/h7974.md) [yalad](../../strongs/h/h3205.md) [ʿēḇer](../../strongs/h/h5677.md).

<a name="genesis_10_25"></a>Genesis 10:25

And unto [ʿēḇer](../../strongs/h/h5677.md) were [yalad](../../strongs/h/h3205.md) two [ben](../../strongs/h/h1121.md): the [shem](../../strongs/h/h8034.md) of one was [Peleḡ](../../strongs/h/h6389.md); for in his [yowm](../../strongs/h/h3117.md) was the ['erets](../../strongs/h/h776.md) [pālaḡ](../../strongs/h/h6385.md); and his ['ach](../../strongs/h/h251.md) [shem](../../strongs/h/h8034.md) was [Yāqṭān](../../strongs/h/h3355.md).

<a name="genesis_10_26"></a>Genesis 10:26

And [Yāqṭān](../../strongs/h/h3355.md) [yalad](../../strongs/h/h3205.md) ['Almôḏāḏ](../../strongs/h/h486.md), and [Šelep̄](../../strongs/h/h8026.md), and [Ḥăṣarmāveṯ](../../strongs/h/h2700.md), and [yeraḥ](../../strongs/h/h3392.md),

<a name="genesis_10_27"></a>Genesis 10:27

And [Hăḏôrām](../../strongs/h/h1913.md), and ['Ûzāl](../../strongs/h/h187.md), and [Diqlâ](../../strongs/h/h1853.md),

<a name="genesis_10_28"></a>Genesis 10:28

And [ʿôḇāl](../../strongs/h/h5745.md), and ['Ăḇîmā'ēl](../../strongs/h/h39.md), and [Šᵊḇā'](../../strongs/h/h7614.md),

<a name="genesis_10_29"></a>Genesis 10:29

And ['Ôp̄îr](../../strongs/h/h211.md), and [Ḥăvîlâ](../../strongs/h/h2341.md), and [Yôḇāḇ](../../strongs/h/h3103.md): all these were the [ben](../../strongs/h/h1121.md) of [Yāqṭān](../../strongs/h/h3355.md).

<a name="genesis_10_30"></a>Genesis 10:30

And their [môšāḇ](../../strongs/h/h4186.md) was from [mēšā'](../../strongs/h/h4852.md), as thou [bow'](../../strongs/h/h935.md) unto [sᵊp̄ār](../../strongs/h/h5611.md) a [har](../../strongs/h/h2022.md) of the [qeḏem](../../strongs/h/h6924.md).

<a name="genesis_10_31"></a>Genesis 10:31

These are the [ben](../../strongs/h/h1121.md) of [Šēm](../../strongs/h/h8035.md), after their [mišpāḥâ](../../strongs/h/h4940.md), after their [lashown](../../strongs/h/h3956.md), in their ['erets](../../strongs/h/h776.md), after their [gowy](../../strongs/h/h1471.md).

<a name="genesis_10_32"></a>Genesis 10:32

These are the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Nōaḥ](../../strongs/h/h5146.md), after their [towlĕdah](../../strongs/h/h8435.md), in their [gowy](../../strongs/h/h1471.md): and by these were the [gowy](../../strongs/h/h1471.md) [pāraḏ](../../strongs/h/h6504.md) in the ['erets](../../strongs/h/h776.md) ['aḥar](../../strongs/h/h310.md) the [mabûl](../../strongs/h/h3999.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 9](genesis_9.md) - [Genesis 11](genesis_11.md)