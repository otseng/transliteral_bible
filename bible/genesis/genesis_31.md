# [Genesis 31](https://www.blueletterbible.org/kjv/gen/31/1/s_31001)

<a name="genesis_31_1"></a>Genesis 31:1

And he [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Lāḇān](../../strongs/h/h3837.md) [ben](../../strongs/h/h1121.md), ['āmar](../../strongs/h/h559.md), [Ya'aqob](../../strongs/h/h3290.md) hath [laqach](../../strongs/h/h3947.md) all that was our ['ab](../../strongs/h/h1.md); and of that which was our ['ab](../../strongs/h/h1.md) hath he ['asah](../../strongs/h/h6213.md) all this [kabowd](../../strongs/h/h3519.md).

<a name="genesis_31_2"></a>Genesis 31:2

And [Ya'aqob](../../strongs/h/h3290.md) [ra'ah](../../strongs/h/h7200.md) the [paniym](../../strongs/h/h6440.md) of [Lāḇān](../../strongs/h/h3837.md), and, behold, it was not toward him [tᵊmôl](../../strongs/h/h8543.md) [šilšôm](../../strongs/h/h8032.md).

<a name="genesis_31_3"></a>Genesis 31:3

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Ya'aqob](../../strongs/h/h3290.md), [shuwb](../../strongs/h/h7725.md) unto the ['erets](../../strongs/h/h776.md) of thy ['ab](../../strongs/h/h1.md), and to thy [môleḏeṯ](../../strongs/h/h4138.md); and I will be with thee.

<a name="genesis_31_4"></a>Genesis 31:4

And [Ya'aqob](../../strongs/h/h3290.md) [shalach](../../strongs/h/h7971.md) and [qara'](../../strongs/h/h7121.md) [Rāḥēl](../../strongs/h/h7354.md) and [Lē'â](../../strongs/h/h3812.md) to the [sadeh](../../strongs/h/h7704.md) unto his [tso'n](../../strongs/h/h6629.md),

<a name="genesis_31_5"></a>Genesis 31:5

And ['āmar](../../strongs/h/h559.md) unto them, I [ra'ah](../../strongs/h/h7200.md) your ['ab](../../strongs/h/h1.md) [paniym](../../strongs/h/h6440.md), that it is not toward me [tᵊmôl](../../strongs/h/h8543.md) [šilšôm](../../strongs/h/h8032.md); but the ['Elohiym](../../strongs/h/h430.md) of my ['ab](../../strongs/h/h1.md) hath been with me.

<a name="genesis_31_6"></a>Genesis 31:6

And ye [yada'](../../strongs/h/h3045.md) that with all my [koach](../../strongs/h/h3581.md) I have ['abad](../../strongs/h/h5647.md) your ['ab](../../strongs/h/h1.md).

<a name="genesis_31_7"></a>Genesis 31:7

And your ['ab](../../strongs/h/h1.md) hath [hāṯal](../../strongs/h/h2048.md) me, and [ḥālap̄](../../strongs/h/h2498.md) my [maśkōreṯ](../../strongs/h/h4909.md) ten [mōnê](../../strongs/h/h4489.md); but ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) him not to [ra'a'](../../strongs/h/h7489.md) me.

<a name="genesis_31_8"></a>Genesis 31:8

If he ['āmar](../../strongs/h/h559.md) thus, The [nāqōḏ](../../strongs/h/h5348.md) shall be thy [śāḵār](../../strongs/h/h7939.md); then all the [tso'n](../../strongs/h/h6629.md) [yalad](../../strongs/h/h3205.md) [nāqōḏ](../../strongs/h/h5348.md): and if he ['āmar](../../strongs/h/h559.md) thus, The [ʿāqōḏ](../../strongs/h/h6124.md) shall be thy [śāḵār](../../strongs/h/h7939.md); then [yalad](../../strongs/h/h3205.md) all the [tso'n](../../strongs/h/h6629.md) [ʿāqōḏ](../../strongs/h/h6124.md).

<a name="genesis_31_9"></a>Genesis 31:9

Thus ['Elohiym](../../strongs/h/h430.md) hath [natsal](../../strongs/h/h5337.md) the [miqnê](../../strongs/h/h4735.md) of your ['ab](../../strongs/h/h1.md), and [nathan](../../strongs/h/h5414.md) them to me.

<a name="genesis_31_10"></a>Genesis 31:10

And it came to pass at the [ʿēṯ](../../strongs/h/h6256.md) that the [tso'n](../../strongs/h/h6629.md) [yāḥam](../../strongs/h/h3179.md), that I [nasa'](../../strongs/h/h5375.md) mine ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md) in a [ḥălôm](../../strongs/h/h2472.md), and, behold, the [ʿatûḏ](../../strongs/h/h6260.md) which [ʿālâ](../../strongs/h/h5927.md) upon the [tso'n](../../strongs/h/h6629.md) were [ʿāqōḏ](../../strongs/h/h6124.md), [nāqōḏ](../../strongs/h/h5348.md), and [bārōḏ](../../strongs/h/h1261.md).

<a name="genesis_31_11"></a>Genesis 31:11

And the [mal'ak](../../strongs/h/h4397.md) of ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto me in a [ḥălôm](../../strongs/h/h2472.md), [Ya'aqob](../../strongs/h/h3290.md): And I ['āmar](../../strongs/h/h559.md), Here am I.

<a name="genesis_31_12"></a>Genesis 31:12

And he ['āmar](../../strongs/h/h559.md), [nasa'](../../strongs/h/h5375.md) now thine ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md), all the [ʿatûḏ](../../strongs/h/h6260.md) which [ʿālâ](../../strongs/h/h5927.md) upon the [tso'n](../../strongs/h/h6629.md) are [ʿāqōḏ](../../strongs/h/h6124.md), [nāqōḏ](../../strongs/h/h5348.md), and [bārōḏ](../../strongs/h/h1261.md): for I have [ra'ah](../../strongs/h/h7200.md) all that [Lāḇān](../../strongs/h/h3837.md) ['asah](../../strongs/h/h6213.md) unto thee.

<a name="genesis_31_13"></a>Genesis 31:13

I am the ['el](../../strongs/h/h410.md) of [Bêṯ-'ēl](../../strongs/h/h1008.md), where thou [māšaḥ](../../strongs/h/h4886.md) the [maṣṣēḇâ](../../strongs/h/h4676.md), and where thou [nāḏar](../../strongs/h/h5087.md) a [neḏer](../../strongs/h/h5088.md) unto me: now [quwm](../../strongs/h/h6965.md), [yāṣā'](../../strongs/h/h3318.md) from this ['erets](../../strongs/h/h776.md), and [shuwb](../../strongs/h/h7725.md) unto the ['erets](../../strongs/h/h776.md) of thy [môleḏeṯ](../../strongs/h/h4138.md).

<a name="genesis_31_14"></a>Genesis 31:14

And [Rāḥēl](../../strongs/h/h7354.md) and [Lē'â](../../strongs/h/h3812.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto him, Is there yet any [cheleq](../../strongs/h/h2506.md) or [nachalah](../../strongs/h/h5159.md) for us in our ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md)?

<a name="genesis_31_15"></a>Genesis 31:15

Are we not [chashab](../../strongs/h/h2803.md) of him [nāḵrî](../../strongs/h/h5237.md)? for he hath [māḵar](../../strongs/h/h4376.md) us, and hath ['akal](../../strongs/h/h398.md) ['akal](../../strongs/h/h398.md) also our [keceph](../../strongs/h/h3701.md).

<a name="genesis_31_16"></a>Genesis 31:16

For all the [ʿōšer](../../strongs/h/h6239.md) which ['Elohiym](../../strongs/h/h430.md) hath [natsal](../../strongs/h/h5337.md) from our ['ab](../../strongs/h/h1.md), that is ours, and our [ben](../../strongs/h/h1121.md): now then, whatsoever ['Elohiym](../../strongs/h/h430.md) hath ['āmar](../../strongs/h/h559.md) unto thee, ['asah](../../strongs/h/h6213.md).

<a name="genesis_31_17"></a>Genesis 31:17

Then [Ya'aqob](../../strongs/h/h3290.md) [quwm](../../strongs/h/h6965.md), and [nasa'](../../strongs/h/h5375.md) his [ben](../../strongs/h/h1121.md) and his ['ishshah](../../strongs/h/h802.md) upon [gāmāl](../../strongs/h/h1581.md);

<a name="genesis_31_18"></a>Genesis 31:18

And he [nāhaḡ](../../strongs/h/h5090.md) all his [miqnê](../../strongs/h/h4735.md), and all his [rᵊḵûš](../../strongs/h/h7399.md) which he had [rāḵaš](../../strongs/h/h7408.md), the [miqnê](../../strongs/h/h4735.md) of his [qinyān](../../strongs/h/h7075.md), which he had [rāḵaš](../../strongs/h/h7408.md) in [padān](../../strongs/h/h6307.md), for to [bow'](../../strongs/h/h935.md) to [Yiṣḥāq](../../strongs/h/h3327.md) his ['ab](../../strongs/h/h1.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md).

<a name="genesis_31_19"></a>Genesis 31:19

And [Lāḇān](../../strongs/h/h3837.md) [halak](../../strongs/h/h1980.md) to [gāzaz](../../strongs/h/h1494.md) his [tso'n](../../strongs/h/h6629.md): and [Rāḥēl](../../strongs/h/h7354.md) had [ganab](../../strongs/h/h1589.md) the [tᵊrāp̄îm](../../strongs/h/h8655.md) that were her ['ab](../../strongs/h/h1.md).

<a name="genesis_31_20"></a>Genesis 31:20

And [Ya'aqob](../../strongs/h/h3290.md) [ganab](../../strongs/h/h1589.md) [leb](../../strongs/h/h3820.md) to [Lāḇān](../../strongs/h/h3837.md) the ['Ărammy](../../strongs/h/h761.md), in that he [nāḡaḏ](../../strongs/h/h5046.md) him not that he [bāraḥ](../../strongs/h/h1272.md).

<a name="genesis_31_21"></a>Genesis 31:21

So he [bāraḥ](../../strongs/h/h1272.md) with all that he had; and he [quwm](../../strongs/h/h6965.md), and ['abar](../../strongs/h/h5674.md) the [nāhār](../../strongs/h/h5104.md), and [śûm](../../strongs/h/h7760.md) his [paniym](../../strongs/h/h6440.md) toward the [har](../../strongs/h/h2022.md) [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="genesis_31_22"></a>Genesis 31:22

And it was [nāḡaḏ](../../strongs/h/h5046.md) [Lāḇān](../../strongs/h/h3837.md) on the third [yowm](../../strongs/h/h3117.md) that [Ya'aqob](../../strongs/h/h3290.md) was [bāraḥ](../../strongs/h/h1272.md).

<a name="genesis_31_23"></a>Genesis 31:23

And he [laqach](../../strongs/h/h3947.md) his ['ach](../../strongs/h/h251.md) with him, and [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) him seven [yowm](../../strongs/h/h3117.md) [derek](../../strongs/h/h1870.md); and they [dāḇaq](../../strongs/h/h1692.md) him in the [har](../../strongs/h/h2022.md) [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="genesis_31_24"></a>Genesis 31:24

And ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) to [Lāḇān](../../strongs/h/h3837.md) the ['Ărammy](../../strongs/h/h761.md) in a [ḥălôm](../../strongs/h/h2472.md) by [layil](../../strongs/h/h3915.md), and ['āmar](../../strongs/h/h559.md) unto him, [shamar](../../strongs/h/h8104.md) that thou [dabar](../../strongs/h/h1696.md) not to [Ya'aqob](../../strongs/h/h3290.md) either [towb](../../strongs/h/h2896.md) or [ra'](../../strongs/h/h7451.md).

<a name="genesis_31_25"></a>Genesis 31:25

Then [Lāḇān](../../strongs/h/h3837.md) [nāśaḡ](../../strongs/h/h5381.md) [Ya'aqob](../../strongs/h/h3290.md). Now [Ya'aqob](../../strongs/h/h3290.md) had [tāqaʿ](../../strongs/h/h8628.md) his ['ohel](../../strongs/h/h168.md) in the [har](../../strongs/h/h2022.md): and [Lāḇān](../../strongs/h/h3837.md) with his ['ach](../../strongs/h/h251.md) [tāqaʿ](../../strongs/h/h8628.md) in the [har](../../strongs/h/h2022.md) of [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="genesis_31_26"></a>Genesis 31:26

And [Lāḇān](../../strongs/h/h3837.md) ['āmar](../../strongs/h/h559.md) to [Ya'aqob](../../strongs/h/h3290.md), What hast thou ['asah](../../strongs/h/h6213.md), that thou hast [ganab](../../strongs/h/h1589.md) [lebab](../../strongs/h/h3824.md) to me, and [nāhaḡ](../../strongs/h/h5090.md) my [bath](../../strongs/h/h1323.md), as [šāḇâ](../../strongs/h/h7617.md) taken with the [chereb](../../strongs/h/h2719.md)?

<a name="genesis_31_27"></a>Genesis 31:27

Wherefore didst thou [bāraḥ](../../strongs/h/h1272.md) [chaba'](../../strongs/h/h2244.md), and [ganab](../../strongs/h/h1589.md) from me; and didst not [nāḡaḏ](../../strongs/h/h5046.md) me, that I might have [shalach](../../strongs/h/h7971.md) with [simchah](../../strongs/h/h8057.md), and with [šîr](../../strongs/h/h7892.md), with [tōp̄](../../strongs/h/h8596.md), and with [kinnôr](../../strongs/h/h3658.md)?

<a name="genesis_31_28"></a>Genesis 31:28

And hast not [nāṭaš](../../strongs/h/h5203.md) me to [nashaq](../../strongs/h/h5401.md) my [ben](../../strongs/h/h1121.md) and my [bath](../../strongs/h/h1323.md)? thou hast now [sāḵal](../../strongs/h/h5528.md) in ['asah](../../strongs/h/h6213.md).

<a name="genesis_31_29"></a>Genesis 31:29

It is in the ['el](../../strongs/h/h410.md) of my [yad](../../strongs/h/h3027.md) to ['asah](../../strongs/h/h6213.md) you [ra'](../../strongs/h/h7451.md): but the ['Elohiym](../../strongs/h/h430.md) of your ['ab](../../strongs/h/h1.md) ['āmar](../../strongs/h/h559.md) unto me ['emeš](../../strongs/h/h570.md), ['āmar](../../strongs/h/h559.md), [shamar](../../strongs/h/h8104.md) that thou [dabar](../../strongs/h/h1696.md) not to [Ya'aqob](../../strongs/h/h3290.md) either [towb](../../strongs/h/h2896.md) or [ra'](../../strongs/h/h7451.md).

<a name="genesis_31_30"></a>Genesis 31:30

And now, though thou wouldest [halak](../../strongs/h/h1980.md) [halak](../../strongs/h/h1980.md), because thou [kacaph](../../strongs/h/h3700.md) [kacaph](../../strongs/h/h3700.md) after thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), yet wherefore hast thou [ganab](../../strongs/h/h1589.md) my ['Elohiym](../../strongs/h/h430.md)?

<a name="genesis_31_31"></a>Genesis 31:31

And [Ya'aqob](../../strongs/h/h3290.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) to [Lāḇān](../../strongs/h/h3837.md), Because I was [yare'](../../strongs/h/h3372.md): for I ['āmar](../../strongs/h/h559.md), [pēn](../../strongs/h/h6435.md) thou wouldest [gāzal](../../strongs/h/h1497.md) thy [bath](../../strongs/h/h1323.md) from me.

<a name="genesis_31_32"></a>Genesis 31:32

With whomsoever thou [māṣā'](../../strongs/h/h4672.md) thy ['Elohiym](../../strongs/h/h430.md), let him not [ḥāyâ](../../strongs/h/h2421.md): [neḡeḏ](../../strongs/h/h5048.md) our ['ach](../../strongs/h/h251.md) [nāḵar](../../strongs/h/h5234.md) thou what is thine with me, and [laqach](../../strongs/h/h3947.md) it to thee. For [Ya'aqob](../../strongs/h/h3290.md) [yada'](../../strongs/h/h3045.md) not that [Rāḥēl](../../strongs/h/h7354.md) had [ganab](../../strongs/h/h1589.md) them.

<a name="genesis_31_33"></a>Genesis 31:33

And [Lāḇān](../../strongs/h/h3837.md) [bow'](../../strongs/h/h935.md) into [Ya'aqob](../../strongs/h/h3290.md) ['ohel](../../strongs/h/h168.md), and into [Lē'â](../../strongs/h/h3812.md) ['ohel](../../strongs/h/h168.md), and into the two ['amah](../../strongs/h/h519.md) ['ohel](../../strongs/h/h168.md); but he [māṣā'](../../strongs/h/h4672.md) them not. Then he [yāṣā'](../../strongs/h/h3318.md) of [Lē'â](../../strongs/h/h3812.md) ['ohel](../../strongs/h/h168.md), and [bow'](../../strongs/h/h935.md) into [Rāḥēl](../../strongs/h/h7354.md) ['ohel](../../strongs/h/h168.md).

<a name="genesis_31_34"></a>Genesis 31:34

Now [Rāḥēl](../../strongs/h/h7354.md) had [laqach](../../strongs/h/h3947.md) the [tᵊrāp̄îm](../../strongs/h/h8655.md), and [śûm](../../strongs/h/h7760.md) them in the [gāmāl](../../strongs/h/h1581.md) [kar](../../strongs/h/h3733.md), and [yashab](../../strongs/h/h3427.md) upon them. And [Lāḇān](../../strongs/h/h3837.md) [māšaš](../../strongs/h/h4959.md) all the ['ohel](../../strongs/h/h168.md), but [māṣā'](../../strongs/h/h4672.md) them not.

<a name="genesis_31_35"></a>Genesis 31:35

And she ['āmar](../../strongs/h/h559.md) to her ['ab](../../strongs/h/h1.md), Let it not [ḥārâ](../../strongs/h/h2734.md) ['ayin](../../strongs/h/h5869.md) my ['adown](../../strongs/h/h113.md) that I [yakol](../../strongs/h/h3201.md) [quwm](../../strongs/h/h6965.md) [paniym](../../strongs/h/h6440.md) thee; for the [derek](../../strongs/h/h1870.md) of ['ishshah](../../strongs/h/h802.md) is upon me. And he [ḥāp̄aś](../../strongs/h/h2664.md) but [māṣā'](../../strongs/h/h4672.md) not the [tᵊrāp̄îm](../../strongs/h/h8655.md).

<a name="genesis_31_36"></a>Genesis 31:36

And [Ya'aqob](../../strongs/h/h3290.md) was [ḥārâ](../../strongs/h/h2734.md), and [riyb](../../strongs/h/h7378.md) with [Lāḇān](../../strongs/h/h3837.md): and [Ya'aqob](../../strongs/h/h3290.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) to [Lāḇān](../../strongs/h/h3837.md), What is my [pesha'](../../strongs/h/h6588.md)? what is my [chatta'ath](../../strongs/h/h2403.md), that thou hast [dalaq](../../strongs/h/h1814.md) ['aḥar](../../strongs/h/h310.md) me?

<a name="genesis_31_37"></a>Genesis 31:37

Whereas thou hast [māšaš](../../strongs/h/h4959.md) all my [kĕliy](../../strongs/h/h3627.md), what hast thou [māṣā'](../../strongs/h/h4672.md) of all thy [bayith](../../strongs/h/h1004.md) [kĕliy](../../strongs/h/h3627.md)? [śûm](../../strongs/h/h7760.md) it [kô](../../strongs/h/h3541.md) before my ['ach](../../strongs/h/h251.md) and thy ['ach](../../strongs/h/h251.md), that they may [yakach](../../strongs/h/h3198.md) betwixt us both.

<a name="genesis_31_38"></a>Genesis 31:38

This twenty [šānâ](../../strongs/h/h8141.md) have I been with thee; thy [rāḥēl](../../strongs/h/h7353.md) and thy [ʿēz](../../strongs/h/h5795.md) have not [šāḵōl](../../strongs/h/h7921.md), and the ['ayil](../../strongs/h/h352.md) of thy [tso'n](../../strongs/h/h6629.md) have I not ['akal](../../strongs/h/h398.md).

<a name="genesis_31_39"></a>Genesis 31:39

That which was [ṭᵊrēp̄â](../../strongs/h/h2966.md) I [bow'](../../strongs/h/h935.md) not unto thee; I [chata'](../../strongs/h/h2398.md) of it; of my [yad](../../strongs/h/h3027.md) didst thou [bāqaš](../../strongs/h/h1245.md) it, whether [ganab](../../strongs/h/h1589.md) by [yowm](../../strongs/h/h3117.md), or [ganab](../../strongs/h/h1589.md) by [layil](../../strongs/h/h3915.md).

<a name="genesis_31_40"></a>Genesis 31:40

Thus I was; in the [yowm](../../strongs/h/h3117.md) the [ḥōreḇ](../../strongs/h/h2721.md) ['akal](../../strongs/h/h398.md) me, and the [qeraḥ](../../strongs/h/h7140.md) by [layil](../../strongs/h/h3915.md); and my [šēnā'](../../strongs/h/h8142.md) [nāḏaḏ](../../strongs/h/h5074.md) from mine ['ayin](../../strongs/h/h5869.md).

<a name="genesis_31_41"></a>Genesis 31:41

Thus have I been twenty [šānâ](../../strongs/h/h8141.md) in thy [bayith](../../strongs/h/h1004.md); I ['abad](../../strongs/h/h5647.md) thee fourteen [šānâ](../../strongs/h/h8141.md) for thy two [bath](../../strongs/h/h1323.md), and six [šānâ](../../strongs/h/h8141.md) for thy [tso'n](../../strongs/h/h6629.md): and thou hast [ḥālap̄](../../strongs/h/h2498.md) my [maśkōreṯ](../../strongs/h/h4909.md) ten [mōnê](../../strongs/h/h4489.md).

<a name="genesis_31_42"></a>Genesis 31:42

[lûlē'](../../strongs/h/h3884.md) the ['Elohiym](../../strongs/h/h430.md) of my ['ab](../../strongs/h/h1.md), the ['Elohiym](../../strongs/h/h430.md) of ['Abraham](../../strongs/h/h85.md), and the [paḥaḏ](../../strongs/h/h6343.md) of [Yiṣḥāq](../../strongs/h/h3327.md), had been with me, surely thou hadst [shalach](../../strongs/h/h7971.md) now [rêqām](../../strongs/h/h7387.md). ['Elohiym](../../strongs/h/h430.md) hath [ra'ah](../../strongs/h/h7200.md) mine ['oniy](../../strongs/h/h6040.md) and the [yᵊḡîaʿ](../../strongs/h/h3018.md) of my [kaph](../../strongs/h/h3709.md), and [yakach](../../strongs/h/h3198.md) thee ['emeš](../../strongs/h/h570.md).

<a name="genesis_31_43"></a>Genesis 31:43

And [Lāḇān](../../strongs/h/h3837.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto [Ya'aqob](../../strongs/h/h3290.md), These [bath](../../strongs/h/h1323.md) are my [bath](../../strongs/h/h1323.md), and these [ben](../../strongs/h/h1121.md) are my [ben](../../strongs/h/h1121.md), and these [tso'n](../../strongs/h/h6629.md) are my [tso'n](../../strongs/h/h6629.md), and all that thou [ra'ah](../../strongs/h/h7200.md) is mine: and what can I ['asah](../../strongs/h/h6213.md) this [yowm](../../strongs/h/h3117.md) unto these my [bath](../../strongs/h/h1323.md), or unto their [ben](../../strongs/h/h1121.md) which they have [yalad](../../strongs/h/h3205.md)?

<a name="genesis_31_44"></a>Genesis 31:44

Now therefore [yālaḵ](../../strongs/h/h3212.md) thou, let us [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md), I and thou; and let it be for na ['ed](../../strongs/h/h5707.md) between me and thee.

<a name="genesis_31_45"></a>Genesis 31:45

And [Ya'aqob](../../strongs/h/h3290.md) [laqach](../../strongs/h/h3947.md) an ['eben](../../strongs/h/h68.md), and [ruwm](../../strongs/h/h7311.md) it for a [maṣṣēḇâ](../../strongs/h/h4676.md).

<a name="genesis_31_46"></a>Genesis 31:46

And [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md) unto his ['ach](../../strongs/h/h251.md), [lāqaṭ](../../strongs/h/h3950.md) ['eben](../../strongs/h/h68.md); and they [laqach](../../strongs/h/h3947.md) ['eben](../../strongs/h/h68.md), and ['asah](../../strongs/h/h6213.md) a [gal](../../strongs/h/h1530.md): and they did ['akal](../../strongs/h/h398.md) there upon the [gal](../../strongs/h/h1530.md).

<a name="genesis_31_47"></a>Genesis 31:47

And [Lāḇān](../../strongs/h/h3837.md) [qara'](../../strongs/h/h7121.md) it [yᵊḡar śāhăḏûṯā'](../../strongs/h/h3026.md): but [Ya'aqob](../../strongs/h/h3290.md) [qara'](../../strongs/h/h7121.md) it [Galʿēḏ](../../strongs/h/h1567.md).

<a name="genesis_31_48"></a>Genesis 31:48

And [Lāḇān](../../strongs/h/h3837.md) ['āmar](../../strongs/h/h559.md), This [gal](../../strongs/h/h1530.md) is an ['ed](../../strongs/h/h5707.md) between me and thee this [yowm](../../strongs/h/h3117.md). Therefore was the [shem](../../strongs/h/h8034.md) of it [qara'](../../strongs/h/h7121.md) [Galʿēḏ](../../strongs/h/h1567.md);

<a name="genesis_31_49"></a>Genesis 31:49

And [Miṣpâ](../../strongs/h/h4709.md); for he ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [tsaphah](../../strongs/h/h6822.md) between me and thee, when we are [cathar](../../strongs/h/h5641.md) ['iysh](../../strongs/h/h376.md) from [rea'](../../strongs/h/h7453.md).

<a name="genesis_31_50"></a>Genesis 31:50

If thou shalt [ʿānâ](../../strongs/h/h6031.md) my [bath](../../strongs/h/h1323.md), or if thou shalt [laqach](../../strongs/h/h3947.md) other ['ishshah](../../strongs/h/h802.md) beside my [bath](../../strongs/h/h1323.md), no ['iysh](../../strongs/h/h376.md) is with us; [ra'ah](../../strongs/h/h7200.md), ['Elohiym](../../strongs/h/h430.md) is ['ed](../../strongs/h/h5707.md) betwixt me and thee.

<a name="genesis_31_51"></a>Genesis 31:51

And [Lāḇān](../../strongs/h/h3837.md) ['āmar](../../strongs/h/h559.md) to [Ya'aqob](../../strongs/h/h3290.md), Behold this [gal](../../strongs/h/h1530.md), and behold this [maṣṣēḇâ](../../strongs/h/h4676.md), which I have [yārâ](../../strongs/h/h3384.md) betwixt me and thee:

<a name="genesis_31_52"></a>Genesis 31:52

This [gal](../../strongs/h/h1530.md) be ['ed](../../strongs/h/h5707.md), and this [maṣṣēḇâ](../../strongs/h/h4676.md) be [ʿēḏâ](../../strongs/h/h5713.md), that I will not ['abar](../../strongs/h/h5674.md) over this [gal](../../strongs/h/h1530.md) to thee, and that thou shalt not ['abar](../../strongs/h/h5674.md) over this [gal](../../strongs/h/h1530.md) and this [maṣṣēḇâ](../../strongs/h/h4676.md) unto me, for [ra'](../../strongs/h/h7451.md).

<a name="genesis_31_53"></a>Genesis 31:53

The ['Elohiym](../../strongs/h/h430.md) of ['Abraham](../../strongs/h/h85.md), and the ['Elohiym](../../strongs/h/h430.md) of [Nāḥôr](../../strongs/h/h5152.md), the ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md), [shaphat](../../strongs/h/h8199.md) betwixt us. And [Ya'aqob](../../strongs/h/h3290.md) [shaba'](../../strongs/h/h7650.md) by the [paḥaḏ](../../strongs/h/h6343.md) of his ['ab](../../strongs/h/h1.md) [Yiṣḥāq](../../strongs/h/h3327.md).

<a name="genesis_31_54"></a>Genesis 31:54

Then [Ya'aqob](../../strongs/h/h3290.md) [zabach](../../strongs/h/h2076.md) [zebach](../../strongs/h/h2077.md) upon the [har](../../strongs/h/h2022.md), and [qara'](../../strongs/h/h7121.md) his ['ach](../../strongs/h/h251.md) to ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md): and they did ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md), and [lûn](../../strongs/h/h3885.md) in the [har](../../strongs/h/h2022.md).

<a name="genesis_31_55"></a>Genesis 31:55

And [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md) [Lāḇān](../../strongs/h/h3837.md) [šāḵam](../../strongs/h/h7925.md), and [nashaq](../../strongs/h/h5401.md) his [ben](../../strongs/h/h1121.md) and his [bath](../../strongs/h/h1323.md), and [barak](../../strongs/h/h1288.md) them: and [Lāḇān](../../strongs/h/h3837.md) [yālaḵ](../../strongs/h/h3212.md), and [shuwb](../../strongs/h/h7725.md) unto his [maqowm](../../strongs/h/h4725.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 30](genesis_30.md) - [Genesis 32](genesis_32.md)