# [Genesis 22](https://www.blueletterbible.org/lxx/genesis/22)

<a name="genesis_22_1"></a>Genesis 22:1

And [ginomai](../../strongs/g/g1096.md) after these [rhēma](../../strongs/g/g4487.md), [theos](../../strongs/g/g2316.md) [peirazō](../../strongs/g/g3985.md) [Abraam](../../strongs/g/g11.md), and [eipon](../../strongs/g/g2036.md) to him, [Abraam](../../strongs/g/g11.md), [Abraam](../../strongs/g/g11.md). And he [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), it is I. 

<a name="genesis_22_2"></a>Genesis 22:2

And he [eipon](../../strongs/g/g2036.md), [lambanō](../../strongs/g/g2983.md) your [huios](../../strongs/g/g5207.md), the [agapētos](../../strongs/g/g27.md), whom you [agapaō](../../strongs/g/g25.md) [Isaak](../../strongs/g/g2464.md)! And [poreuō](../../strongs/g/g4198.md) into the [gē](../../strongs/g/g1093.md) [hypsēlos](../../strongs/g/g5308.md), and [anapherō](../../strongs/g/g399.md) him there as a whole offering (holokarpōsin) upon one of the [oros](../../strongs/g/g3735.md)! which ever I may [eipon](../../strongs/g/g2036.md) you.

<a name="genesis_22_3"></a>Genesis 22:3

[anistēmi](../../strongs/g/g450.md) [Abraam](../../strongs/g/g11.md) in the [prōï](../../strongs/g/g4404.md), saddled (epesaxen) his [onos](../../strongs/g/g3688.md), and he [paralambanō](../../strongs/g/g3880.md) with himself [dyo](../../strongs/g/g1417.md) [pais](../../strongs/g/g3816.md), and [Isaak](../../strongs/g/g2464.md) his [huios](../../strongs/g/g5207.md). And [schizō](../../strongs/g/g4977.md) [xylon](../../strongs/g/g3586.md) for a whole offering (holokarpōsin), [anistēmi](../../strongs/g/g450.md) to [poreuō](../../strongs/g/g4198.md), they [erchomai](../../strongs/g/g2064.md) upon the [topos](../../strongs/g/g5117.md) which [eipon](../../strongs/g/g2036.md) to him [theos](../../strongs/g/g2316.md), [hēmera](../../strongs/g/g2250.md) on the [tritos](../../strongs/g/g5154.md). 

<a name="genesis_22_4"></a>Genesis 22:4

And [Abraam](../../strongs/g/g11.md) [anablepō](../../strongs/g/g308.md) with his [ophthalmos](../../strongs/g/g3788.md) [eidō](../../strongs/g/g1492.md) the [topos](../../strongs/g/g5117.md) [makrothen](../../strongs/g/g3113.md). 

<a name="genesis_22_5"></a>Genesis 22:5

And [Abraam](../../strongs/g/g11.md) [eipon](../../strongs/g/g2036.md) to his [pais](../../strongs/g/g3816.md), You [kathizō](../../strongs/g/g2523.md) here with the [onos](../../strongs/g/g3688.md)! and I and the [paidarion](../../strongs/g/g3808.md) will go [dierchomai](../../strongs/g/g1330.md) unto here. And doing [proskyneō](../../strongs/g/g4352.md) we will [anastrephō](../../strongs/g/g390.md) to you. 

<a name="genesis_22_6"></a>Genesis 22:6

[lambanō](../../strongs/g/g2983.md) [Abraam](../../strongs/g/g11.md) the [xylon](../../strongs/g/g3586.md) for the whole offering (holokarpōseōs), and [epitithēmi](../../strongs/g/g2007.md) it [Isaak](../../strongs/g/g2464.md) his [huios](../../strongs/g/g5207.md). And he [lambanō](../../strongs/g/g2983.md) with his [cheir](../../strongs/g/g5495.md) the [pyr](../../strongs/g/g4442.md) and the [machaira](../../strongs/g/g3162.md); and [poreuō](../../strongs/g/g4198.md) the [dyo](../../strongs/g/g1417.md) together. 

<a name="genesis_22_7"></a>Genesis 22:7

[eipon](../../strongs/g/g2036.md) [Isaak](../../strongs/g/g2464.md) to [Abraam](../../strongs/g/g11.md) his [patēr](../../strongs/g/g3962.md), [patēr](../../strongs/g/g3962.md). And he [eipon](../../strongs/g/g2036.md), What is it [teknon](../../strongs/g/g5043.md)? And he [legō](../../strongs/g/g3004.md), [idou](../../strongs/g/g2400.md), the [pyr](../../strongs/g/g4442.md) and the [xylon](../../strongs/g/g3586.md), where is the [probaton](../../strongs/g/g4263.md) the one for a whole offering (holokarpōseōs)?

<a name="genesis_22_8"></a>Genesis 22:8

[eipon](../../strongs/g/g2036.md) [Abraam](../../strongs/g/g11.md), [theos](../../strongs/g/g2316.md) will [horaō](../../strongs/g/g3708.md) to himself a [probaton](../../strongs/g/g4263.md) for a whole offering (holokarpōseōs), [teknon](../../strongs/g/g5043.md). [poreuō](../../strongs/g/g4198.md) [amphoteroi](../../strongs/g/g297.md) [hama](../../strongs/g/g260.md), 

<a name="genesis_22_9"></a>Genesis 22:9

they [erchomai](../../strongs/g/g2064.md) upon the [topos](../../strongs/g/g5117.md) which [eipon](../../strongs/g/g2036.md) to him [theos](../../strongs/g/g2316.md). And [oikodomeō](../../strongs/g/g3618.md) there [Abraam](../../strongs/g/g11.md) the [thysiastērion](../../strongs/g/g2379.md). And he [epitithēmi](../../strongs/g/g2007.md) the [xylon](../../strongs/g/g3586.md). And binding (sympodisas) [Isaak](../../strongs/g/g2464.md) his [huios](../../strongs/g/g5207.md), he [epitithēmi](../../strongs/g/g2007.md) him upon the [thysiastērion](../../strongs/g/g2379.md) upon the [xylon](../../strongs/g/g3586.md). 

<a name="genesis_22_10"></a>Genesis 22:10

And [Abraam](../../strongs/g/g11.md) [ekteinō](../../strongs/g/g1614.md) his [cheir](../../strongs/g/g5495.md) to [lambanō](../../strongs/g/g2983.md) the [machaira](../../strongs/g/g3162.md) to [sphazō](../../strongs/g/g4969.md) his [huios](../../strongs/g/g5207.md). 

<a name="genesis_22_11"></a>Genesis 22:11

And [kaleō](../../strongs/g/g2564.md) him an [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) from out of the [ouranos](../../strongs/g/g3772.md). And he [eipon](../../strongs/g/g2036.md), [Abraam](../../strongs/g/g11.md), [Abraam](../../strongs/g/g11.md). And he [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), it is I. 

<a name="genesis_22_12"></a>Genesis 22:12

And he [eipon](../../strongs/g/g2036.md), You should not [epiballō](../../strongs/g/g1911.md) your [cheir](../../strongs/g/g5495.md) upon the [paidarion](../../strongs/g/g3808.md), nor should you [poieō](../../strongs/g/g4160.md) anything to him by any [mēdeis](../../strongs/g/g3367.md). For now I [ginōskō](../../strongs/g/g1097.md) that you [phobeō](../../strongs/g/g5399.md) [theos](../../strongs/g/g2316.md), and [pheidomai](../../strongs/g/g5339.md) not [huios](../../strongs/g/g5207.md) your [agapētos](../../strongs/g/g27.md) on account of me. 

<a name="genesis_22_13"></a>Genesis 22:13

And [Abraam](../../strongs/g/g11.md) [anablepō](../../strongs/g/g308.md) with his [ophthalmos](../../strongs/g/g3788.md), [eidō](../../strongs/g/g1492.md). And [idou](../../strongs/g/g2400.md), ram (krios) being [katechō](../../strongs/g/g2722.md) in a plant (phytō) of a thicket (sabek) by the [keras](../../strongs/g/g2768.md). And [Abraam](../../strongs/g/g11.md) [poreuō](../../strongs/g/g4198.md) and [lambanō](../../strongs/g/g2983.md) the ram (krion), and he [anapherō](../../strongs/g/g399.md) it for a whole offering (holokarpōseōs) instead of [Isaak](../../strongs/g/g2464.md) his [huios](../../strongs/g/g5207.md). 

<a name="genesis_22_14"></a>Genesis 22:14

And [Abraam](../../strongs/g/g11.md) [kaleō](../../strongs/g/g2564.md) the [onoma](../../strongs/g/g3686.md) of that [topos](../../strongs/g/g5117.md), The [kyrios](../../strongs/g/g2962.md) [eidō](../../strongs/g/g1492.md); that they may [eipon](../../strongs/g/g2036.md) [sēmeron](../../strongs/g/g4594.md), in the [oros](../../strongs/g/g3735.md) the [kyrios](../../strongs/g/g2962.md) was [horaō](../../strongs/g/g3708.md). 

<a name="genesis_22_15"></a>Genesis 22:15

And [kaleō](../../strongs/g/g2564.md) the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) [Abraam](../../strongs/g/g11.md) a [deuteros](../../strongs/g/g1208.md) time from out of the [ouranos](../../strongs/g/g3772.md), [legō](../../strongs/g/g3004.md), 

<a name="genesis_22_16"></a>Genesis 22:16

According to myself I [omnyō](../../strongs/g/g3660.md), [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md), because you [poieō](../../strongs/g/g4160.md) this [rhēma](../../strongs/g/g4487.md), and you did not [pheidomai](../../strongs/g/g5339.md) [huios](../../strongs/g/g5207.md) your [agapētos](../../strongs/g/g27.md) on account of me. 

<a name="genesis_22_17"></a>Genesis 22:17

[ē](../../strongs/g/g2229.md) in [eulogeō](../../strongs/g/g2127.md), I will [eulogeō](../../strongs/g/g2127.md) you, and in [plēthynō](../../strongs/g/g4129.md) I will [plēthynō](../../strongs/g/g4129.md) your [sperma](../../strongs/g/g4690.md) as the [astēr](../../strongs/g/g792.md) of the [ouranos](../../strongs/g/g3772.md), and as the [ammos](../../strongs/g/g285.md) by the [cheilos](../../strongs/g/g5491.md) of the [thalassa](../../strongs/g/g2281.md). And [klēronomeō](../../strongs/g/g2816.md) your [sperma](../../strongs/g/g4690.md) the [polis](../../strongs/g/g4172.md) of your [hypenantios](../../strongs/g/g5227.md). 

<a name="genesis_22_18"></a>Genesis 22:18

And will be [eneulogeō](../../strongs/g/g1757.md) by your [sperma](../../strongs/g/g4690.md) all the [ethnos](../../strongs/g/g1484.md) of the [gē](../../strongs/g/g1093.md); because you [hypakouō](../../strongs/g/g5219.md) my [phōnē](../../strongs/g/g5456.md). 

<a name="genesis_22_19"></a>Genesis 22:19

[apostrephō](../../strongs/g/g654.md) [Abraam](../../strongs/g/g11.md) to his [pais](../../strongs/g/g3816.md). And [anistēmi](../../strongs/g/g450.md) they [poreuō](../../strongs/g/g4198.md) together to the [phrear](../../strongs/g/g5421.md) of the [horkos](../../strongs/g/g3727.md). And [Abraam](../../strongs/g/g11.md) [katoikeō](../../strongs/g/g2730.md) near the [phrear](../../strongs/g/g5421.md) of the [horkos](../../strongs/g/g3727.md). 

<a name="genesis_22_20"></a>Genesis 22:20

And [ginomai](../../strongs/g/g1096.md) after these [rhēma](../../strongs/g/g4487.md), that it was [anaggellō](../../strongs/g/g312.md) to [Abraam](../../strongs/g/g11.md), [legō](../../strongs/g/g3004.md), [idou](../../strongs/g/g2400.md), [tiktō](../../strongs/g/g5088.md) even Milcah herself [huios](../../strongs/g/g5207.md) to [Nachōr](../../strongs/g/g3493.md) your [adelphos](../../strongs/g/g80.md). 

<a name="genesis_22_21"></a>Genesis 22:21

Huz [prōtotokos](../../strongs/g/g4416.md), and Buz his [adelphos](../../strongs/g/g80.md), and Kemuel [patēr](../../strongs/g/g3962.md) of the [Syros](../../strongs/g/g4948.md),

<a name="genesis_22_22"></a>Genesis 22:22

and Chesed, and Hazo, and Pildash, and Jidlaph and Bethuel.

<a name="genesis_22_23"></a>Genesis 22:23

And Bethuel [gennaō](../../strongs/g/g1080.md) [Rhebekka](../../strongs/g/g4479.md). These are the [oktō](../../strongs/g/g3638.md) [huios](../../strongs/g/g5207.md) whom Milcah [tiktō](../../strongs/g/g5088.md) to [Nachōr](../../strongs/g/g3493.md) the [adelphos](../../strongs/g/g80.md) of [Abraam](../../strongs/g/g11.md).

<a name="genesis_22_24"></a>Genesis 22:24

And his concubine (pallakē), whose [onoma](../../strongs/g/g3686.md) was Reumah, [tiktō](../../strongs/g/g5088.md) even herself, Tebah, and Gaham, and Thahash, and Maachah.

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 21](genesis_lxx_21.md) - [Genesis 23](genesis_lxx_23.md)