# [Genesis 11](https://www.blueletterbible.org/lxx/genesis/11)

<a name="genesis_11_1"></a>Genesis 11:1

And was all the [gē](../../strongs/g/g1093.md) [cheilos](../../strongs/g/g5491.md) one, and [phōnē](../../strongs/g/g5456.md) one to all. 

<a name="genesis_11_2"></a>Genesis 11:2

And [ginomai](../../strongs/g/g1096.md) in their [kineō](../../strongs/g/g2795.md) from the [anatolē](../../strongs/g/g395.md), they [heuriskō](../../strongs/g/g2147.md) a plain (pedion) in the [gē](../../strongs/g/g1093.md) of Shinar, and they [katoikeō](../../strongs/g/g2730.md) there. 

<a name="genesis_11_3"></a>Genesis 11:3

And [eipon](../../strongs/g/g2036.md) a [anthrōpos](../../strongs/g/g444.md) to his [plēsion](../../strongs/g/g4139.md), [deute](../../strongs/g/g1205.md) let us make (plintheusōmen) bricks (plinthous), and let us bake (optēsōmen) them in [pyr](../../strongs/g/g4442.md). And [ginomai](../../strongs/g/g1096.md) to them the brick (optēsōmen) as [lithos](../../strongs/g/g3037.md), and asphalt (asphaltos) was their [pēlos](../../strongs/g/g4081.md). 

<a name="genesis_11_4"></a>Genesis 11:4

And they [eipon](../../strongs/g/g2036.md), [deute](../../strongs/g/g1205.md) let us [oikodomeō](../../strongs/g/g3618.md) to ourselves a [polis](../../strongs/g/g4172.md) and [pyrgos](../../strongs/g/g4444.md) of which the [kephalē](../../strongs/g/g2776.md) will be unto the [ouranos](../../strongs/g/g3772.md). And let us [poieō](../../strongs/g/g4160.md) to ourselves a [onoma](../../strongs/g/g3686.md) before the being [diaspeirō](../../strongs/g/g1289.md) upon the [prosōpon](../../strongs/g/g4383.md) of all the [gē](../../strongs/g/g1093.md). 

<a name="genesis_11_5"></a>Genesis 11:5

And the [kyrios](../../strongs/g/g2962.md) [katabainō](../../strongs/g/g2597.md) to [eidō](../../strongs/g/g1492.md) the [polis](../../strongs/g/g4172.md) and the [pyrgos](../../strongs/g/g4444.md), which [oikodomeō](../../strongs/g/g3618.md) the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md). 

<a name="genesis_11_6"></a>Genesis 11:6

And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), there [genos](../../strongs/g/g1085.md) one and [cheilos](../../strongs/g/g5491.md) one for all, and this they [archomai](../../strongs/g/g756.md) to [poieō](../../strongs/g/g4160.md). The things now shall not [ekleipō](../../strongs/g/g1587.md) from them, all as much as they might [epitithēmi](../../strongs/g/g2007.md) to [poieō](../../strongs/g/g4160.md). 

<a name="genesis_11_7"></a>Genesis 11:7

[deute](../../strongs/g/g1205.md), and going [katabainō](../../strongs/g/g2597.md), let us [sygcheō](../../strongs/g/g4797.md) their [glōssa](../../strongs/g/g1100.md), that they should not [akouō](../../strongs/g/g191.md) each the [phōnē](../../strongs/g/g5456.md) of the [plēsion](../../strongs/g/g4139.md). 

<a name="genesis_11_8"></a>Genesis 11:8

And the [kyrios](../../strongs/g/g2962.md) [diaspeirō](../../strongs/g/g1289.md) them from there upon the [prosōpon](../../strongs/g/g4383.md) of all the [gē](../../strongs/g/g1093.md); and they [pauō](../../strongs/g/g3973.md) [oikodomeō](../../strongs/g/g3618.md) the [polis](../../strongs/g/g4172.md) and the [pyrgos](../../strongs/g/g4444.md). 

<a name="genesis_11_9"></a>Genesis 11:9

On account of this was [kaleō](../../strongs/g/g2564.md) the [onoma](../../strongs/g/g3686.md) of it [sygchysis](../../strongs/g/g4799.md), because there the [kyrios](../../strongs/g/g2962.md) [sygcheō](../../strongs/g/g4797.md) the [cheilos](../../strongs/g/g5491.md) of all the [gē](../../strongs/g/g1093.md), and from there [diaspeirō](../../strongs/g/g1289.md) them the [kyrios](../../strongs/g/g2962.md) upon the [prosōpon](../../strongs/g/g4383.md) of all the [gē](../../strongs/g/g1093.md). 

<a name="genesis_11_10"></a>Genesis 11:10

And these are the [genesis](../../strongs/g/g1078.md) of [Sēm](../../strongs/g/g4590.md). And [Sēm](../../strongs/g/g4590.md) was a [hekaton](../../strongs/g/g1540.md) [etos](../../strongs/g/g2094.md) when he [gennaō](../../strongs/g/g1080.md) [Arphaxad](../../strongs/g/g742.md), the [deuteros](../../strongs/g/g1208.md) [etos](../../strongs/g/g2094.md) after the [kataklysmos](../../strongs/g/g2627.md). 

<a name="genesis_11_11"></a>Genesis 11:11

And [Sēm](../../strongs/g/g4590.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Arphaxad](../../strongs/g/g742.md), five and thirty and three-hundred [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_11_12"></a>Genesis 11:12

And [Arphaxad](../../strongs/g/g742.md) [zaō](../../strongs/g/g2198.md) a [hekaton](../../strongs/g/g1540.md) [triakonta](../../strongs/g/g5144.md) [pente](../../strongs/g/g4002.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [Kaïnam](../../strongs/g/g2536.md).

<a name="genesis_11_13"></a>Genesis 11:13

And [Arphaxad](../../strongs/g/g742.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Kaïnam](../../strongs/g/g2536.md), [etos](../../strongs/g/g2094.md) three hundred thirty, and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md), and he [apothnēskō](../../strongs/g/g599.md). And [Kaïnam](../../strongs/g/g2536.md) [zaō](../../strongs/g/g2198.md) a hundred and thirty [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [Sala](../../strongs/g/g4527.md). And [Kaïnam](../../strongs/g/g2536.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Sala](../../strongs/g/g4527.md), [etos](../../strongs/g/g2094.md) three hundred thirty, and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_11_14"></a>Genesis 11:14

And [Sala](../../strongs/g/g4527.md) [zaō](../../strongs/g/g2198.md) a [hekaton](../../strongs/g/g1540.md) [triakonta](../../strongs/g/g5144.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [Eber](../../strongs/g/g1443.md).

<a name="genesis_11_15"></a>Genesis 11:15

And [Sala](../../strongs/g/g4527.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Eber](../../strongs/g/g1443.md), [triakosioi](../../strongs/g/g5145.md) [triakonta](../../strongs/g/g5144.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_11_16"></a>Genesis 11:16

And [Eber](../../strongs/g/g1443.md) [zaō](../../strongs/g/g2198.md) a [hekaton](../../strongs/g/g1540.md) [triakonta](../../strongs/g/g5144.md) [tessares](../../strongs/g/g5064.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [Phalek](../../strongs/g/g5317.md).

<a name="genesis_11_17"></a>Genesis 11:17

And [Eber](../../strongs/g/g1443.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Phalek](../../strongs/g/g5317.md), [etos](../../strongs/g/g2094.md) [triakosioi](../../strongs/g/g5145.md) [hebdomēkonta](../../strongs/g/g1440.md), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_11_18"></a>Genesis 11:18

And [Phalek](../../strongs/g/g5317.md) [zaō](../../strongs/g/g2198.md) [triakonta](../../strongs/g/g5144.md) and a [hekaton](../../strongs/g/g1540.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [Rhagau](../../strongs/g/g4466.md).

<a name="genesis_11_19"></a>Genesis 11:19

And [Phalek](../../strongs/g/g5317.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Rhagau](../../strongs/g/g4466.md), [ennea](../../strongs/g/g1767.md) and [diakosioi](../../strongs/g/g1250.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_11_20"></a>Genesis 11:20

And [Rhagau](../../strongs/g/g4466.md) [zaō](../../strongs/g/g2198.md) a [hekaton](../../strongs/g/g1540.md) [triakonta](../../strongs/g/g5144.md) and [dyo](../../strongs/g/g1417.md) [etos](../../strongs/g/g2094.md). and he [gennaō](../../strongs/g/g1080.md) Serug.

<a name="genesis_11_21"></a>Genesis 11:21

And [Rhagau](../../strongs/g/g4466.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) Serug [diakosioi](../../strongs/g/g1250.md) [hepta](../../strongs/g/g2033.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_11_22"></a>Genesis 11:22

And Serug [zaō](../../strongs/g/g2198.md) a [hekaton](../../strongs/g/g1540.md) [triakonta](../../strongs/g/g5144.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [Nachōr](../../strongs/g/g3493.md).

<a name="genesis_11_23"></a>Genesis 11:23

And Serug [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Nachōr](../../strongs/g/g3493.md), [etos](../../strongs/g/g2094.md) [diakosioi](../../strongs/g/g1250.md), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_11_24"></a>Genesis 11:24

And [Nachōr](../../strongs/g/g3493.md) [zaō](../../strongs/g/g2198.md) [etos](../../strongs/g/g2094.md) [hebdomēkonta](../../strongs/g/g1440.md) [ennea](../../strongs/g/g1767.md), and he [gennaō](../../strongs/g/g1080.md) Terah.

<a name="genesis_11_25"></a>Genesis 11:25

And [Nachōr](../../strongs/g/g3493.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Thara](../../strongs/g/g2291.md), [etos](../../strongs/g/g2094.md) a [hekaton](../../strongs/g/g1540.md) [eikosi](../../strongs/g/g1501.md) [ennea](../../strongs/g/g1767.md), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_11_26"></a>Genesis 11:26

And [Thara](../../strongs/g/g2291.md) [zaō](../../strongs/g/g2198.md) [hebdomēkonta](../../strongs/g/g1440.md) [etos](../../strongs/g/g2094.md) and he [gennaō](../../strongs/g/g1080.md) Abram and [Nachōr](../../strongs/g/g3493.md) and Haran.

<a name="genesis_11_27"></a>Genesis 11:27

And these are the [genesis](../../strongs/g/g1078.md) of [Thara](../../strongs/g/g2291.md). [Thara](../../strongs/g/g2291.md) [gennaō](../../strongs/g/g1080.md) Abram, and [Nachōr](../../strongs/g/g3493.md), and Haran. And Haran [gennaō](../../strongs/g/g1080.md) [Lōt](../../strongs/g/g3091.md).

<a name="genesis_11_28"></a>Genesis 11:28

And Haran [apothnēskō](../../strongs/g/g599.md) in the presence of [Thara](../../strongs/g/g2291.md) his [patēr](../../strongs/g/g3962.md), in the [gē](../../strongs/g/g1093.md) in which he was [gennaō](../../strongs/g/g1080.md), in the [chōra](../../strongs/g/g5561.md) of the [Chaldaios](../../strongs/g/g5466.md).

<a name="genesis_11_29"></a>Genesis 11:29

And [lambanō](../../strongs/g/g2983.md) Abram and [Nachōr](../../strongs/g/g3493.md) to themselves [gynē](../../strongs/g/g1135.md). The [onoma](../../strongs/g/g3686.md) of the [gynē](../../strongs/g/g1135.md) of Abram was Sarai, and the [onoma](../../strongs/g/g3686.md) of the [gynē](../../strongs/g/g1135.md) of [Nachōr](../../strongs/g/g3493.md) was Milcha, [thygatēr](../../strongs/g/g2364.md) of Haran. And he was the [patēr](../../strongs/g/g3962.md) of Milcha, and [patēr](../../strongs/g/g3962.md) of Iscah.

<a name="genesis_11_30"></a>Genesis 11:30

And Sarai was [steira](../../strongs/g/g4723.md), and not able to produce children (eteknopoiei).

<a name="genesis_11_31"></a>Genesis 11:31

And [Thara](../../strongs/g/g2291.md) [lambanō](../../strongs/g/g2983.md) Abram his [huios](../../strongs/g/g5207.md), and [Lōt](../../strongs/g/g3091.md) the [huios](../../strongs/g/g5207.md) of [Charran](../../strongs/g/g5488.md), the [huios](../../strongs/g/g5207.md) of his [huios](../../strongs/g/g5207.md), and Sarai his [nymphē](../../strongs/g/g3565.md), the [gynē](../../strongs/g/g1135.md) of Abram his [huios](../../strongs/g/g5207.md). And he [exagō](../../strongs/g/g1806.md) them from out of the [chōra](../../strongs/g/g5561.md) of the [Chaldaios](../../strongs/g/g5466.md) to [poreuō](../../strongs/g/g4198.md) into the [gē](../../strongs/g/g1093.md) of [Chanaan](../../strongs/g/g5477.md). And they [erchomai](../../strongs/g/g2064.md) unto [Charran](../../strongs/g/g5488.md) and [katoikeō](../../strongs/g/g2730.md) there. 

<a name="genesis_11_32"></a>Genesis 11:32

And [ginomai](../../strongs/g/g1096.md) the [hēmera](../../strongs/g/g2250.md) of [Thara](../../strongs/g/g2291.md) in [Charran](../../strongs/g/g5488.md), [diakosioi](../../strongs/g/g1250.md) [pente](../../strongs/g/g4002.md) [etos](../../strongs/g/g2094.md); and [Thara](../../strongs/g/g2291.md) [apothnēskō](../../strongs/g/g599.md) in [Charran](../../strongs/g/g5488.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 10](genesis_lxx_10.md) - [Genesis 12](genesis_lxx_12.md)