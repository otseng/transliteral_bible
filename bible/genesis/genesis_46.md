# [Genesis 46](https://www.blueletterbible.org/kjv/gen/46/1/s_46001)

<a name="genesis_46_1"></a>Genesis 46:1

And [Yisra'el](../../strongs/h/h3478.md) [nāsaʿ](../../strongs/h/h5265.md) with all that he had, and [bow'](../../strongs/h/h935.md) to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md), and [zabach](../../strongs/h/h2076.md) [zebach](../../strongs/h/h2077.md) unto the ['Elohiym](../../strongs/h/h430.md) of his ['ab](../../strongs/h/h1.md) [Yiṣḥāq](../../strongs/h/h3327.md).

<a name="genesis_46_2"></a>Genesis 46:2

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto [Yisra'el](../../strongs/h/h3478.md) in the [mar'â](../../strongs/h/h4759.md) of the [layil](../../strongs/h/h3915.md), and ['āmar](../../strongs/h/h559.md), [Ya'aqob](../../strongs/h/h3290.md), [Ya'aqob](../../strongs/h/h3290.md). And he ['āmar](../../strongs/h/h559.md), Here am I.

<a name="genesis_46_3"></a>Genesis 46:3

And he ['āmar](../../strongs/h/h559.md), I am ['el](../../strongs/h/h410.md), the ['Elohiym](../../strongs/h/h430.md) of thy ['ab](../../strongs/h/h1.md): [yare'](../../strongs/h/h3372.md) not to [yarad](../../strongs/h/h3381.md) into [Mitsrayim](../../strongs/h/h4714.md); for I will there [śûm](../../strongs/h/h7760.md) of thee a [gadowl](../../strongs/h/h1419.md) [gowy](../../strongs/h/h1471.md):

<a name="genesis_46_4"></a>Genesis 46:4

I will [yarad](../../strongs/h/h3381.md) with thee into [Mitsrayim](../../strongs/h/h4714.md); and I will [ʿālâ](../../strongs/h/h5927.md) [ʿālâ](../../strongs/h/h5927.md) thee up: and [Yôsēp̄](../../strongs/h/h3130.md) shall [shiyth](../../strongs/h/h7896.md) his [yad](../../strongs/h/h3027.md) upon thine ['ayin](../../strongs/h/h5869.md).

<a name="genesis_46_5"></a>Genesis 46:5

And [Ya'aqob](../../strongs/h/h3290.md) [quwm](../../strongs/h/h6965.md) from [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md): and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nasa'](../../strongs/h/h5375.md) [Ya'aqob](../../strongs/h/h3290.md) their ['ab](../../strongs/h/h1.md), and their [ṭap̄](../../strongs/h/h2945.md), and their ['ishshah](../../strongs/h/h802.md), in the [ʿăḡālâ](../../strongs/h/h5699.md) which [Parʿô](../../strongs/h/h6547.md) had [shalach](../../strongs/h/h7971.md) to [nasa'](../../strongs/h/h5375.md) him.

<a name="genesis_46_6"></a>Genesis 46:6

And they [laqach](../../strongs/h/h3947.md) their [miqnê](../../strongs/h/h4735.md), and their [rᵊḵûš](../../strongs/h/h7399.md), which they had [rāḵaš](../../strongs/h/h7408.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), and [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md), [Ya'aqob](../../strongs/h/h3290.md), and all his [zera'](../../strongs/h/h2233.md) with him:

<a name="genesis_46_7"></a>Genesis 46:7

His [ben](../../strongs/h/h1121.md), and his [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md) with him, his [bath](../../strongs/h/h1323.md), and his [ben](../../strongs/h/h1121.md) [bath](../../strongs/h/h1323.md), and all his [zera'](../../strongs/h/h2233.md) [bow'](../../strongs/h/h935.md) he with him into [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_46_8"></a>Genesis 46:8

And these are the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md), [Ya'aqob](../../strongs/h/h3290.md) and his [ben](../../strongs/h/h1121.md): [Rᵊ'ûḇēn](../../strongs/h/h7205.md), [Ya'aqob](../../strongs/h/h3290.md) [bᵊḵôr](../../strongs/h/h1060.md).

<a name="genesis_46_9"></a>Genesis 46:9

And the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md); [Ḥănôḵ](../../strongs/h/h2585.md), and [Pallû'](../../strongs/h/h6396.md), and [Ḥeṣrôn](../../strongs/h/h2696.md), and [Karmî](../../strongs/h/h3756.md).

<a name="genesis_46_10"></a>Genesis 46:10

And the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md); [yᵊmû'ēl](../../strongs/h/h3223.md), and [yāmîn](../../strongs/h/h3226.md), and ['ōhaḏ](../../strongs/h/h161.md), and [Yāḵîn](../../strongs/h/h3199.md), and [Ṣōḥar](../../strongs/h/h6714.md), and [Šā'ûl](../../strongs/h/h7586.md) the [ben](../../strongs/h/h1121.md) of a [Kᵊnaʿănî](../../strongs/h/h3669.md).

<a name="genesis_46_11"></a>Genesis 46:11

And the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md); [Gēršôn](../../strongs/h/h1648.md), [Qᵊhāṯ](../../strongs/h/h6955.md), and [Mᵊrārî](../../strongs/h/h4847.md).

<a name="genesis_46_12"></a>Genesis 46:12

And the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md); [ʿĒr](../../strongs/h/h6147.md), and ['Ônān](../../strongs/h/h209.md), and [Šēlâ](../../strongs/h/h7956.md), and [Pereṣ](../../strongs/h/h6557.md), and [Zeraḥ](../../strongs/h/h2226.md): but [ʿĒr](../../strongs/h/h6147.md) and ['Ônān](../../strongs/h/h209.md) [muwth](../../strongs/h/h4191.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md). And the [ben](../../strongs/h/h1121.md) of [Pereṣ](../../strongs/h/h6557.md) were [Ḥeṣrôn](../../strongs/h/h2696.md) and [Ḥāmûl](../../strongs/h/h2538.md).

<a name="genesis_46_13"></a>Genesis 46:13

And the [ben](../../strongs/h/h1121.md) of [Yiśśāśḵār](../../strongs/h/h3485.md); [Tôlāʿ](../../strongs/h/h8439.md), and [pû'â](../../strongs/h/h6312.md), and [yôḇ](../../strongs/h/h3102.md), and [šimrôn](../../strongs/h/h8110.md).

<a name="genesis_46_14"></a>Genesis 46:14

And the [ben](../../strongs/h/h1121.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md); [sereḏ](../../strongs/h/h5624.md), and ['êlôn](../../strongs/h/h356.md), and [yaḥlᵊ'ēl](../../strongs/h/h3177.md).

<a name="genesis_46_15"></a>Genesis 46:15

These be the [ben](../../strongs/h/h1121.md) of [Lē'â](../../strongs/h/h3812.md), which she [yalad](../../strongs/h/h3205.md) unto [Ya'aqob](../../strongs/h/h3290.md) in [padān](../../strongs/h/h6307.md), with his [bath](../../strongs/h/h1323.md) [Dînâ](../../strongs/h/h1783.md): all the [nephesh](../../strongs/h/h5315.md) of his [ben](../../strongs/h/h1121.md) and his [bath](../../strongs/h/h1323.md) were thirty and three.

<a name="genesis_46_16"></a>Genesis 46:16

And the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md); [ṣip̄yôn](../../strongs/h/h6837.md), and [ḥagî](../../strongs/h/h2291.md), [šûnî](../../strongs/h/h7764.md), and ['Eṣbōn](../../strongs/h/h675.md), [ʿērî](../../strongs/h/h6179.md), and ['ărôḏî](../../strongs/h/h722.md), and ['ar'ēlî](../../strongs/h/h692.md).

<a name="genesis_46_17"></a>Genesis 46:17

And the [ben](../../strongs/h/h1121.md) of ['Āšēr](../../strongs/h/h836.md); [Yimnâ](../../strongs/h/h3232.md), and [Yišvâ](../../strongs/h/h3438.md), and [Yišvî](../../strongs/h/h3440.md), and [Bᵊrîʿâ](../../strongs/h/h1283.md), and [Śeraḥ](../../strongs/h/h8294.md) their ['āḥôṯ](../../strongs/h/h269.md): and the [ben](../../strongs/h/h1121.md) of [Bᵊrîʿâ](../../strongs/h/h1283.md); [Ḥeḇer](../../strongs/h/h2268.md), and [Malkî'ēl](../../strongs/h/h4439.md).

<a name="genesis_46_18"></a>Genesis 46:18

These are the [ben](../../strongs/h/h1121.md) of [zilpâ](../../strongs/h/h2153.md), whom [Lāḇān](../../strongs/h/h3837.md) [nathan](../../strongs/h/h5414.md) to [Lē'â](../../strongs/h/h3812.md) his [bath](../../strongs/h/h1323.md), and these she [yalad](../../strongs/h/h3205.md) unto [Ya'aqob](../../strongs/h/h3290.md), even sixteen [nephesh](../../strongs/h/h5315.md).

<a name="genesis_46_19"></a>Genesis 46:19

The [ben](../../strongs/h/h1121.md) of [Rāḥēl](../../strongs/h/h7354.md) [Ya'aqob](../../strongs/h/h3290.md) ['ishshah](../../strongs/h/h802.md); [Yôsēp̄](../../strongs/h/h3130.md), and [Binyāmîn](../../strongs/h/h1144.md).

<a name="genesis_46_20"></a>Genesis 46:20

And unto [Yôsēp̄](../../strongs/h/h3130.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) were born [Mᵊnaššê](../../strongs/h/h4519.md) and ['Ep̄rayim](../../strongs/h/h669.md), which ['āsnaṯ](../../strongs/h/h621.md) the [bath](../../strongs/h/h1323.md) of [pôṭî p̄eraʿ](../../strongs/h/h6319.md) [kōhēn](../../strongs/h/h3548.md) of ['ôn](../../strongs/h/h204.md) [yalad](../../strongs/h/h3205.md) unto him.

<a name="genesis_46_21"></a>Genesis 46:21

And the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) were [Belaʿ](../../strongs/h/h1106.md), and [Beḵer](../../strongs/h/h1071.md), and ['Ašbēl](../../strongs/h/h788.md), [Gērā'](../../strongs/h/h1617.md), and [Naʿămān](../../strongs/h/h5283.md), ['ēḥî](../../strongs/h/h278.md), and [rō'š](../../strongs/h/h7220.md), [mupîm](../../strongs/h/h4649.md), and [Ḥupîm](../../strongs/h/h2650.md), and ['ardᵊ](../../strongs/h/h714.md).

<a name="genesis_46_22"></a>Genesis 46:22

These are the [ben](../../strongs/h/h1121.md) of [Rāḥēl](../../strongs/h/h7354.md), which were [yalad](../../strongs/h/h3205.md) to [Ya'aqob](../../strongs/h/h3290.md): all the [nephesh](../../strongs/h/h5315.md) were fourteen.

<a name="genesis_46_23"></a>Genesis 46:23

And the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md); [Ḥûšîm](../../strongs/h/h2366.md).

<a name="genesis_46_24"></a>Genesis 46:24

And the [ben](../../strongs/h/h1121.md) of [Nap̄tālî](../../strongs/h/h5321.md); [yaḥṣᵊ'ēl](../../strongs/h/h3183.md), and [Gûnî](../../strongs/h/h1476.md), and [Yēṣer](../../strongs/h/h3337.md), and [Šillēm](../../strongs/h/h8006.md).

<a name="genesis_46_25"></a>Genesis 46:25

These are the [ben](../../strongs/h/h1121.md) of [Bilhâ](../../strongs/h/h1090.md), which [Lāḇān](../../strongs/h/h3837.md) [nathan](../../strongs/h/h5414.md) unto [Rāḥēl](../../strongs/h/h7354.md) his [bath](../../strongs/h/h1323.md), and she [yalad](../../strongs/h/h3205.md) these unto [Ya'aqob](../../strongs/h/h3290.md): all the [nephesh](../../strongs/h/h5315.md) were seven.

<a name="genesis_46_26"></a>Genesis 46:26

All the [nephesh](../../strongs/h/h5315.md) that [bow'](../../strongs/h/h935.md) with [Ya'aqob](../../strongs/h/h3290.md) into [Mitsrayim](../../strongs/h/h4714.md), which [yāṣā'](../../strongs/h/h3318.md) of his [yārēḵ](../../strongs/h/h3409.md), besides [Ya'aqob](../../strongs/h/h3290.md) [ben](../../strongs/h/h1121.md) ['ishshah](../../strongs/h/h802.md), all the [nephesh](../../strongs/h/h5315.md) were threescore and six;

<a name="genesis_46_27"></a>Genesis 46:27

And the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md), which were [yalad](../../strongs/h/h3205.md) him in [Mitsrayim](../../strongs/h/h4714.md), were two [nephesh](../../strongs/h/h5315.md): all the [nephesh](../../strongs/h/h5315.md) of the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), which [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md), were threescore and ten.

<a name="genesis_46_28"></a>Genesis 46:28

And he [shalach](../../strongs/h/h7971.md) [Yehuwdah](../../strongs/h/h3063.md) [paniym](../../strongs/h/h6440.md) him unto [Yôsēp̄](../../strongs/h/h3130.md), to [yārâ](../../strongs/h/h3384.md) his [paniym](../../strongs/h/h6440.md) unto [gōšen](../../strongs/h/h1657.md); and they [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) of [gōšen](../../strongs/h/h1657.md).

<a name="genesis_46_29"></a>Genesis 46:29

And [Yôsēp̄](../../strongs/h/h3130.md) ['āsar](../../strongs/h/h631.md) his [merkāḇâ](../../strongs/h/h4818.md), and [ʿālâ](../../strongs/h/h5927.md) to [qārā'](../../strongs/h/h7125.md) [Yisra'el](../../strongs/h/h3478.md) his ['ab](../../strongs/h/h1.md), to [gōšen](../../strongs/h/h1657.md), and [ra'ah](../../strongs/h/h7200.md) himself unto him; and he [naphal](../../strongs/h/h5307.md) on his [ṣaûā'r](../../strongs/h/h6677.md), and [bāḵâ](../../strongs/h/h1058.md) on his [ṣaûā'r](../../strongs/h/h6677.md) [ʿôḏ](../../strongs/h/h5750.md).

<a name="genesis_46_30"></a>Genesis 46:30

And [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto [Yôsēp̄](../../strongs/h/h3130.md), [pa'am](../../strongs/h/h6471.md) let me [muwth](../../strongs/h/h4191.md), ['aḥar](../../strongs/h/h310.md) I have [ra'ah](../../strongs/h/h7200.md) thy [paniym](../../strongs/h/h6440.md), because thou art yet [chay](../../strongs/h/h2416.md).

<a name="genesis_46_31"></a>Genesis 46:31

And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto his ['ach](../../strongs/h/h251.md), and unto his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), I will [ʿālâ](../../strongs/h/h5927.md), and [nāḡaḏ](../../strongs/h/h5046.md) [Parʿô](../../strongs/h/h6547.md), and ['āmar](../../strongs/h/h559.md) unto him, My ['ach](../../strongs/h/h251.md), and my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), which were in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), are [bow'](../../strongs/h/h935.md) unto me;

<a name="genesis_46_32"></a>Genesis 46:32

And the ['enowsh](../../strongs/h/h582.md) are [ra'ah](../../strongs/h/h7462.md) [tso'n](../../strongs/h/h6629.md), for their trade hath been to feed [miqnê](../../strongs/h/h4735.md); and they have [bow'](../../strongs/h/h935.md) their [tso'n](../../strongs/h/h6629.md), and their [bāqār](../../strongs/h/h1241.md), and all that they have.

<a name="genesis_46_33"></a>Genesis 46:33

And it shall come to pass, when [Parʿô](../../strongs/h/h6547.md) shall [qara'](../../strongs/h/h7121.md) you, and shall ['āmar](../../strongs/h/h559.md), What is your [ma'aseh](../../strongs/h/h4639.md)?

<a name="genesis_46_34"></a>Genesis 46:34

That ye shall ['āmar](../../strongs/h/h559.md), Thy ['ebed](../../strongs/h/h5650.md) ['enowsh](../../strongs/h/h582.md) hath been about [miqnê](../../strongs/h/h4735.md) from our [nāʿur](../../strongs/h/h5271.md) even until now, both we, and also our ['ab](../../strongs/h/h1.md): that ye may [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [gōšen](../../strongs/h/h1657.md); for every [ra'ah](../../strongs/h/h7462.md) [tso'n](../../strongs/h/h6629.md) is a [tôʿēḇâ](../../strongs/h/h8441.md) unto the [Mitsrayim](../../strongs/h/h4714.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 45](genesis_45.md) - [Genesis 47](genesis_47.md)