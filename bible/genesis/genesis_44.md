# [Genesis 44](https://www.blueletterbible.org/kjv/gen/44/1/s_44001)

<a name="genesis_44_1"></a>Genesis 44:1

And he [tsavah](../../strongs/h/h6680.md) the steward of his [bayith](../../strongs/h/h1004.md), ['āmar](../../strongs/h/h559.md), [mālā'](../../strongs/h/h4390.md) the ['enowsh](../../strongs/h/h582.md) ['amtaḥaṯ](../../strongs/h/h572.md) with ['ōḵel](../../strongs/h/h400.md), as much as they [yakol](../../strongs/h/h3201.md) [nasa'](../../strongs/h/h5375.md), and [śûm](../../strongs/h/h7760.md) every ['iysh](../../strongs/h/h376.md) [keceph](../../strongs/h/h3701.md) in his ['amtaḥaṯ](../../strongs/h/h572.md) [peh](../../strongs/h/h6310.md).

<a name="genesis_44_2"></a>Genesis 44:2

And [śûm](../../strongs/h/h7760.md) my [gāḇîaʿ](../../strongs/h/h1375.md), the [keceph](../../strongs/h/h3701.md) [gāḇîaʿ](../../strongs/h/h1375.md), in the ['amtaḥaṯ](../../strongs/h/h572.md) [peh](../../strongs/h/h6310.md) of the [qāṭān](../../strongs/h/h6996.md), and his [šēḇer](../../strongs/h/h7668.md) [keceph](../../strongs/h/h3701.md). And he ['asah](../../strongs/h/h6213.md) according to the [dabar](../../strongs/h/h1697.md) that [Yôsēp̄](../../strongs/h/h3130.md) had [dabar](../../strongs/h/h1696.md).

<a name="genesis_44_3"></a>Genesis 44:3

As soon as the [boqer](../../strongs/h/h1242.md) was ['owr](../../strongs/h/h215.md), the ['enowsh](../../strongs/h/h582.md) were [shalach](../../strongs/h/h7971.md), they and their [chamowr](../../strongs/h/h2543.md).

<a name="genesis_44_4"></a>Genesis 44:4

And when they were [yāṣā'](../../strongs/h/h3318.md) of the [ʿîr](../../strongs/h/h5892.md), and not yet [rachaq](../../strongs/h/h7368.md), [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto his [bayith](../../strongs/h/h1004.md), [quwm](../../strongs/h/h6965.md), [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) the ['enowsh](../../strongs/h/h582.md); and when thou dost [nāśaḡ](../../strongs/h/h5381.md) them, ['āmar](../../strongs/h/h559.md) unto them, Wherefore have ye [shalam](../../strongs/h/h7999.md) [ra'](../../strongs/h/h7451.md) for [towb](../../strongs/h/h2896.md)?

<a name="genesis_44_5"></a>Genesis 44:5

Is not this it in which my ['adown](../../strongs/h/h113.md) [šāṯâ](../../strongs/h/h8354.md), and whereby indeed he [nāḥaš](../../strongs/h/h5172.md)? ye have done [ra'a'](../../strongs/h/h7489.md) in so ['asah](../../strongs/h/h6213.md).

<a name="genesis_44_6"></a>Genesis 44:6

And he [nāśaḡ](../../strongs/h/h5381.md) them, and he [dabar](../../strongs/h/h1696.md) unto them these same [dabar](../../strongs/h/h1697.md).

<a name="genesis_44_7"></a>Genesis 44:7

And they ['āmar](../../strongs/h/h559.md) unto him, Wherefore [dabar](../../strongs/h/h1696.md) my ['adown](../../strongs/h/h113.md) these [dabar](../../strongs/h/h1697.md)? [ḥālîlâ](../../strongs/h/h2486.md) that thy ['ebed](../../strongs/h/h5650.md) should ['asah](../../strongs/h/h6213.md) according to this [dabar](../../strongs/h/h1697.md):

<a name="genesis_44_8"></a>Genesis 44:8

Behold, the [keceph](../../strongs/h/h3701.md), which we [māṣā'](../../strongs/h/h4672.md) in our ['amtaḥaṯ](../../strongs/h/h572.md) [peh](../../strongs/h/h6310.md), we [shuwb](../../strongs/h/h7725.md) unto thee out of the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md): how then should we [ganab](../../strongs/h/h1589.md) out of thy ['adown](../../strongs/h/h113.md) [bayith](../../strongs/h/h1004.md) [keceph](../../strongs/h/h3701.md) or [zāhāḇ](../../strongs/h/h2091.md)?

<a name="genesis_44_9"></a>Genesis 44:9

With whomsoever of thy ['ebed](../../strongs/h/h5650.md) it be [māṣā'](../../strongs/h/h4672.md), both let him [muwth](../../strongs/h/h4191.md), and we also will be my ['adown](../../strongs/h/h113.md) ['ebed](../../strongs/h/h5650.md).

<a name="genesis_44_10"></a>Genesis 44:10

And he ['āmar](../../strongs/h/h559.md), Now also let it be according unto your [dabar](../../strongs/h/h1697.md): he with whom it is [māṣā'](../../strongs/h/h4672.md) shall be my ['ebed](../../strongs/h/h5650.md); and ye shall be [naqiy](../../strongs/h/h5355.md).

<a name="genesis_44_11"></a>Genesis 44:11

Then they [māhar](../../strongs/h/h4116.md) [yarad](../../strongs/h/h3381.md) every ['iysh](../../strongs/h/h376.md) his ['amtaḥaṯ](../../strongs/h/h572.md) to the ['erets](../../strongs/h/h776.md), and [pāṯaḥ](../../strongs/h/h6605.md) every ['iysh](../../strongs/h/h376.md) his ['amtaḥaṯ](../../strongs/h/h572.md).

<a name="genesis_44_12"></a>Genesis 44:12

And he [ḥāp̄aś](../../strongs/h/h2664.md), and [ḥālal](../../strongs/h/h2490.md) at the [gadowl](../../strongs/h/h1419.md), and [kalah](../../strongs/h/h3615.md) at the [qāṭān](../../strongs/h/h6996.md): and the [gāḇîaʿ](../../strongs/h/h1375.md) was [māṣā'](../../strongs/h/h4672.md) in [Binyāmîn](../../strongs/h/h1144.md) ['amtaḥaṯ](../../strongs/h/h572.md).

<a name="genesis_44_13"></a>Genesis 44:13

Then they [qāraʿ](../../strongs/h/h7167.md) their [śimlâ](../../strongs/h/h8071.md), and [ʿāmas](../../strongs/h/h6006.md) every ['iysh](../../strongs/h/h376.md) his [chamowr](../../strongs/h/h2543.md), and [shuwb](../../strongs/h/h7725.md) to the [ʿîr](../../strongs/h/h5892.md).

<a name="genesis_44_14"></a>Genesis 44:14

And [Yehuwdah](../../strongs/h/h3063.md) and his ['ach](../../strongs/h/h251.md) [bow'](../../strongs/h/h935.md) to [Yôsēp̄](../../strongs/h/h3130.md) [bayith](../../strongs/h/h1004.md); for he was yet there: and they [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) him on the ['erets](../../strongs/h/h776.md).

<a name="genesis_44_15"></a>Genesis 44:15

And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto them, What [ma'aseh](../../strongs/h/h4639.md) is this that ye have ['asah](../../strongs/h/h6213.md)? [yada'](../../strongs/h/h3045.md) ye not that such an ['iysh](../../strongs/h/h376.md) as I can [nāḥaš](../../strongs/h/h5172.md) [nāḥaš](../../strongs/h/h5172.md)?

<a name="genesis_44_16"></a>Genesis 44:16

And [Yehuwdah](../../strongs/h/h3063.md) ['āmar](../../strongs/h/h559.md), What shall we ['āmar](../../strongs/h/h559.md) unto my ['adown](../../strongs/h/h113.md)? what shall we [dabar](../../strongs/h/h1696.md)?  or how shall we [ṣāḏaq](../../strongs/h/h6663.md)? ['Elohiym](../../strongs/h/h430.md) hath [māṣā'](../../strongs/h/h4672.md) the ['avon](../../strongs/h/h5771.md) of thy ['ebed](../../strongs/h/h5650.md): behold, we are my ['adown](../../strongs/h/h113.md) ['ebed](../../strongs/h/h5650.md), both we, and he also with [yad](../../strongs/h/h3027.md) the [gāḇîaʿ](../../strongs/h/h1375.md) is [māṣā'](../../strongs/h/h4672.md).

<a name="genesis_44_17"></a>Genesis 44:17

And he ['āmar](../../strongs/h/h559.md), [ḥālîlâ](../../strongs/h/h2486.md) that I should ['asah](../../strongs/h/h6213.md) so: but the ['iysh](../../strongs/h/h376.md) in whose [yad](../../strongs/h/h3027.md) the [gāḇîaʿ](../../strongs/h/h1375.md) is [māṣā'](../../strongs/h/h4672.md), he shall be my ['ebed](../../strongs/h/h5650.md); and as for you, [ʿālâ](../../strongs/h/h5927.md) you in [shalowm](../../strongs/h/h7965.md) unto your ['ab](../../strongs/h/h1.md).

<a name="genesis_44_18"></a>Genesis 44:18

Then [Yehuwdah](../../strongs/h/h3063.md) [nāḡaš](../../strongs/h/h5066.md) unto him, and ['āmar](../../strongs/h/h559.md), [bî](../../strongs/h/h994.md) my ['adown](../../strongs/h/h113.md), let thy ['ebed](../../strongs/h/h5650.md), I pray thee, [dabar](../../strongs/h/h1696.md) a [dabar](../../strongs/h/h1697.md) in my ['adown](../../strongs/h/h113.md) ['ozen](../../strongs/h/h241.md), and let not thine ['aph](../../strongs/h/h639.md) [ḥārâ](../../strongs/h/h2734.md) against thy ['ebed](../../strongs/h/h5650.md): for thou art even as [Parʿô](../../strongs/h/h6547.md).

<a name="genesis_44_19"></a>Genesis 44:19

My ['adown](../../strongs/h/h113.md) [sha'al](../../strongs/h/h7592.md) his ['ebed](../../strongs/h/h5650.md), ['āmar](../../strongs/h/h559.md), Have ye an ['ab](../../strongs/h/h1.md), or an ['ach](../../strongs/h/h251.md)?

<a name="genesis_44_20"></a>Genesis 44:20

And we ['āmar](../../strongs/h/h559.md) unto my ['adown](../../strongs/h/h113.md), We have an ['ab](../../strongs/h/h1.md), a [zāqēn](../../strongs/h/h2205.md), and a [yeleḏ](../../strongs/h/h3206.md) of his [zāqun](../../strongs/h/h2208.md), a [qāṭān](../../strongs/h/h6996.md); and his ['ach](../../strongs/h/h251.md) is [muwth](../../strongs/h/h4191.md), and he alone [yāṯar](../../strongs/h/h3498.md) of his ['em](../../strongs/h/h517.md), and his ['ab](../../strongs/h/h1.md) ['ahab](../../strongs/h/h157.md) him.

<a name="genesis_44_21"></a>Genesis 44:21

And thou ['āmar](../../strongs/h/h559.md) unto thy ['ebed](../../strongs/h/h5650.md), [yarad](../../strongs/h/h3381.md) him unto me, that I may [śûm](../../strongs/h/h7760.md) mine ['ayin](../../strongs/h/h5869.md) upon him.

<a name="genesis_44_22"></a>Genesis 44:22

And we ['āmar](../../strongs/h/h559.md) unto my ['adown](../../strongs/h/h113.md), The [naʿar](../../strongs/h/h5288.md) [yakol](../../strongs/h/h3201.md) ['azab](../../strongs/h/h5800.md) his ['ab](../../strongs/h/h1.md): for if he should ['azab](../../strongs/h/h5800.md) his ['ab](../../strongs/h/h1.md), his father would [muwth](../../strongs/h/h4191.md).

<a name="genesis_44_23"></a>Genesis 44:23

And thou ['āmar](../../strongs/h/h559.md) unto thy ['ebed](../../strongs/h/h5650.md), Except your [qāṭān](../../strongs/h/h6996.md) ['ach](../../strongs/h/h251.md) [yarad](../../strongs/h/h3381.md) with you, ye shall [ra'ah](../../strongs/h/h7200.md) my [paniym](../../strongs/h/h6440.md) no more.

<a name="genesis_44_24"></a>Genesis 44:24

And it came to pass when we [ʿālâ](../../strongs/h/h5927.md) unto thy ['ebed](../../strongs/h/h5650.md) my ['ab](../../strongs/h/h1.md), we [nāḡaḏ](../../strongs/h/h5046.md) him the [dabar](../../strongs/h/h1697.md) of my ['adown](../../strongs/h/h113.md).

<a name="genesis_44_25"></a>Genesis 44:25

And our ['ab](../../strongs/h/h1.md) ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md), and [šāḇar](../../strongs/h/h7666.md) us a [mᵊʿaṭ](../../strongs/h/h4592.md) ['ōḵel](../../strongs/h/h400.md).

<a name="genesis_44_26"></a>Genesis 44:26

And we ['āmar](../../strongs/h/h559.md), We [yakol](../../strongs/h/h3201.md) [yarad](../../strongs/h/h3381.md): if our [qāṭān](../../strongs/h/h6996.md) ['ach](../../strongs/h/h251.md) be with us, then will we [yarad](../../strongs/h/h3381.md): for we [yakol](../../strongs/h/h3201.md) not [ra'ah](../../strongs/h/h7200.md) the ['iysh](../../strongs/h/h376.md) [paniym](../../strongs/h/h6440.md), except our [qāṭān](../../strongs/h/h6996.md) ['ach](../../strongs/h/h251.md) be with us.

<a name="genesis_44_27"></a>Genesis 44:27

And thy ['ebed](../../strongs/h/h5650.md) my ['ab](../../strongs/h/h1.md) ['āmar](../../strongs/h/h559.md) unto us, Ye [yada'](../../strongs/h/h3045.md) that my ['ishshah](../../strongs/h/h802.md) [yalad](../../strongs/h/h3205.md) me [šᵊnayim](../../strongs/h/h8147.md):

<a name="genesis_44_28"></a>Genesis 44:28

And the one [yāṣā'](../../strongs/h/h3318.md) from me, and I ['āmar](../../strongs/h/h559.md), Surely he is [taraph](../../strongs/h/h2963.md); and I [ra'ah](../../strongs/h/h7200.md) him not since:

<a name="genesis_44_29"></a>Genesis 44:29

And if ye [laqach](../../strongs/h/h3947.md) this also from [paniym](../../strongs/h/h6440.md), and ['āsôn](../../strongs/h/h611.md) [qārâ](../../strongs/h/h7136.md) him, ye shall [yarad](../../strongs/h/h3381.md) my [śêḇâ](../../strongs/h/h7872.md) with [ra'](../../strongs/h/h7451.md) to the [shĕ'owl](../../strongs/h/h7585.md).

<a name="genesis_44_30"></a>Genesis 44:30

Now therefore when I [bow'](../../strongs/h/h935.md) to thy ['ebed](../../strongs/h/h5650.md) my ['ab](../../strongs/h/h1.md), and the [naʿar](../../strongs/h/h5288.md) be not with us; seeing that his [nephesh](../../strongs/h/h5315.md) is [qāšar](../../strongs/h/h7194.md) in the lad's [nephesh](../../strongs/h/h5315.md);

<a name="genesis_44_31"></a>Genesis 44:31

It shall come to pass, when he [ra'ah](../../strongs/h/h7200.md) that the [naʿar](../../strongs/h/h5288.md) is not with us, that he will [muwth](../../strongs/h/h4191.md): and thy ['ebed](../../strongs/h/h5650.md) shall [yarad](../../strongs/h/h3381.md) the [śêḇâ](../../strongs/h/h7872.md) of thy ['ebed](../../strongs/h/h5650.md) our ['ab](../../strongs/h/h1.md) with [yagown](../../strongs/h/h3015.md) to the [shĕ'owl](../../strongs/h/h7585.md).

<a name="genesis_44_32"></a>Genesis 44:32

For thy ['ebed](../../strongs/h/h5650.md) became [ʿāraḇ](../../strongs/h/h6148.md) for the [naʿar](../../strongs/h/h5288.md) unto my ['ab](../../strongs/h/h1.md), ['āmar](../../strongs/h/h559.md), If I [bow'](../../strongs/h/h935.md) him not unto thee, then I shall [chata'](../../strongs/h/h2398.md) to my ['ab](../../strongs/h/h1.md) for [yowm](../../strongs/h/h3117.md).

<a name="genesis_44_33"></a>Genesis 44:33

Now therefore, I pray thee, let thy ['ebed](../../strongs/h/h5650.md) [yashab](../../strongs/h/h3427.md) instead of the [naʿar](../../strongs/h/h5288.md) an ['ebed](../../strongs/h/h5650.md) to my ['adown](../../strongs/h/h113.md); and let the [naʿar](../../strongs/h/h5288.md) [ʿālâ](../../strongs/h/h5927.md) with his ['ach](../../strongs/h/h251.md).

<a name="genesis_44_34"></a>Genesis 44:34

For how shall I [ʿālâ](../../strongs/h/h5927.md) to my ['ab](../../strongs/h/h1.md), and the [naʿar](../../strongs/h/h5288.md) be not with me? lest peradventure I [ra'ah](../../strongs/h/h7200.md) the [ra'](../../strongs/h/h7451.md) that shall [māṣā'](../../strongs/h/h4672.md) my ['ab](../../strongs/h/h1.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 43](genesis_43.md) - [Genesis 45](genesis_45.md)