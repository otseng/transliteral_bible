# [Genesis 29](https://www.blueletterbible.org/kjv/gen/29/1/s_29001)

<a name="genesis_29_1"></a>Genesis 29:1

Then [Ya'aqob](../../strongs/h/h3290.md) [nasa'](../../strongs/h/h5375.md) his [regel](../../strongs/h/h7272.md), and [yālaḵ](../../strongs/h/h3212.md) into the ['erets](../../strongs/h/h776.md) of the [ben](../../strongs/h/h1121.md) of the [qeḏem](../../strongs/h/h6924.md).

<a name="genesis_29_2"></a>Genesis 29:2

And he [ra'ah](../../strongs/h/h7200.md), and behold a [bᵊ'ēr](../../strongs/h/h875.md) in the [sadeh](../../strongs/h/h7704.md), and, lo, there were three [ʿēḏer](../../strongs/h/h5739.md) of [tso'n](../../strongs/h/h6629.md) [rāḇaṣ](../../strongs/h/h7257.md) by it; for out of that [bᵊ'ēr](../../strongs/h/h875.md) they [šāqâ](../../strongs/h/h8248.md) the [ʿēḏer](../../strongs/h/h5739.md): and a [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md) was upon the [bᵊ'ēr](../../strongs/h/h875.md) [peh](../../strongs/h/h6310.md).

<a name="genesis_29_3"></a>Genesis 29:3

And thither were all the [ʿēḏer](../../strongs/h/h5739.md) ['āsap̄](../../strongs/h/h622.md): and they [gālal](../../strongs/h/h1556.md) the ['eben](../../strongs/h/h68.md) from the [bᵊ'ēr](../../strongs/h/h875.md) [peh](../../strongs/h/h6310.md), and [šāqâ](../../strongs/h/h8248.md) the [tso'n](../../strongs/h/h6629.md), and [shuwb](../../strongs/h/h7725.md) the ['eben](../../strongs/h/h68.md) upon the [bᵊ'ēr](../../strongs/h/h875.md) [peh](../../strongs/h/h6310.md) in his [maqowm](../../strongs/h/h4725.md).

<a name="genesis_29_4"></a>Genesis 29:4

And [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md) unto them, My ['ach](../../strongs/h/h251.md), whence be ye? And they ['āmar](../../strongs/h/h559.md), Of [Ḥārān](../../strongs/h/h2771.md) are we.

<a name="genesis_29_5"></a>Genesis 29:5

And he ['āmar](../../strongs/h/h559.md) unto them, [yada'](../../strongs/h/h3045.md) ye [Lāḇān](../../strongs/h/h3837.md) the [ben](../../strongs/h/h1121.md) of [Nāḥôr](../../strongs/h/h5152.md)? And they ['āmar](../../strongs/h/h559.md), We [yada'](../../strongs/h/h3045.md) him.

<a name="genesis_29_6"></a>Genesis 29:6

And he ['āmar](../../strongs/h/h559.md) unto them, Is he [shalowm](../../strongs/h/h7965.md)? And they ['āmar](../../strongs/h/h559.md), He is [shalowm](../../strongs/h/h7965.md): and, behold, [Rāḥēl](../../strongs/h/h7354.md) his [bath](../../strongs/h/h1323.md) [bow'](../../strongs/h/h935.md) with the [tso'n](../../strongs/h/h6629.md).

<a name="genesis_29_7"></a>Genesis 29:7

And he ['āmar](../../strongs/h/h559.md), [hen](../../strongs/h/h2005.md), it is yet [gadowl](../../strongs/h/h1419.md) [yowm](../../strongs/h/h3117.md), neither is it [ʿēṯ](../../strongs/h/h6256.md) that the [miqnê](../../strongs/h/h4735.md) should be ['āsap̄](../../strongs/h/h622.md): [šāqâ](../../strongs/h/h8248.md) ye the [tso'n](../../strongs/h/h6629.md), and [yālaḵ](../../strongs/h/h3212.md) and [ra'ah](../../strongs/h/h7462.md) them.

<a name="genesis_29_8"></a>Genesis 29:8

And they ['āmar](../../strongs/h/h559.md), We [yakol](../../strongs/h/h3201.md), until all the [ʿēḏer](../../strongs/h/h5739.md) be ['āsap̄](../../strongs/h/h622.md), and till they [gālal](../../strongs/h/h1556.md) the ['eben](../../strongs/h/h68.md) from the [bᵊ'ēr](../../strongs/h/h875.md) [peh](../../strongs/h/h6310.md); then we [šāqâ](../../strongs/h/h8248.md) the [tso'n](../../strongs/h/h6629.md).

<a name="genesis_29_9"></a>Genesis 29:9

And while he yet [dabar](../../strongs/h/h1696.md) with them, [Rāḥēl](../../strongs/h/h7354.md) [bow'](../../strongs/h/h935.md) with her ['ab](../../strongs/h/h1.md) [tso'n](../../strongs/h/h6629.md); for she [ra'ah](../../strongs/h/h7462.md) them.

<a name="genesis_29_10"></a>Genesis 29:10

And it came to pass, when [Ya'aqob](../../strongs/h/h3290.md) [ra'ah](../../strongs/h/h7200.md) [Rāḥēl](../../strongs/h/h7354.md) the [bath](../../strongs/h/h1323.md) of [Lāḇān](../../strongs/h/h3837.md) his ['em](../../strongs/h/h517.md) ['ach](../../strongs/h/h251.md), and the [tso'n](../../strongs/h/h6629.md) of [Lāḇān](../../strongs/h/h3837.md) his ['em](../../strongs/h/h517.md) ['ach](../../strongs/h/h251.md), that [Ya'aqob](../../strongs/h/h3290.md) went [nāḡaš](../../strongs/h/h5066.md), and [gālal](../../strongs/h/h1556.md) the ['eben](../../strongs/h/h68.md) from the [bᵊ'ēr](../../strongs/h/h875.md) [peh](../../strongs/h/h6310.md), and [šāqâ](../../strongs/h/h8248.md) the [tso'n](../../strongs/h/h6629.md) of [Lāḇān](../../strongs/h/h3837.md) his ['em](../../strongs/h/h517.md) ['ach](../../strongs/h/h251.md).

<a name="genesis_29_11"></a>Genesis 29:11

And [Ya'aqob](../../strongs/h/h3290.md) [nashaq](../../strongs/h/h5401.md) [Rāḥēl](../../strongs/h/h7354.md), and [nasa'](../../strongs/h/h5375.md) his [qowl](../../strongs/h/h6963.md), and [bāḵâ](../../strongs/h/h1058.md).

<a name="genesis_29_12"></a>Genesis 29:12

And [Ya'aqob](../../strongs/h/h3290.md) [nāḡaḏ](../../strongs/h/h5046.md) [Rāḥēl](../../strongs/h/h7354.md) that he was her ['ab](../../strongs/h/h1.md) ['ach](../../strongs/h/h251.md), and that he was [riḇqâ](../../strongs/h/h7259.md) [ben](../../strongs/h/h1121.md): and she [rûṣ](../../strongs/h/h7323.md) and [nāḡaḏ](../../strongs/h/h5046.md) her ['ab](../../strongs/h/h1.md).

<a name="genesis_29_13"></a>Genesis 29:13

And it came to pass, when [Lāḇān](../../strongs/h/h3837.md) [shama'](../../strongs/h/h8085.md) the [šēmaʿ](../../strongs/h/h8088.md) of [Ya'aqob](../../strongs/h/h3290.md) his ['āḥôṯ](../../strongs/h/h269.md) [ben](../../strongs/h/h1121.md), that he [rûṣ](../../strongs/h/h7323.md) to [qārā'](../../strongs/h/h7125.md) him, and [ḥāḇaq](../../strongs/h/h2263.md) him, and [nashaq](../../strongs/h/h5401.md) him, and [bow'](../../strongs/h/h935.md) him to his [bayith](../../strongs/h/h1004.md). And he [sāp̄ar](../../strongs/h/h5608.md) [Lāḇān](../../strongs/h/h3837.md) all these [dabar](../../strongs/h/h1697.md).

<a name="genesis_29_14"></a>Genesis 29:14

And [Lāḇān](../../strongs/h/h3837.md) ['āmar](../../strongs/h/h559.md) to him, ['aḵ](../../strongs/h/h389.md) thou art my ['etsem](../../strongs/h/h6106.md) and my [basar](../../strongs/h/h1320.md). And he [yashab](../../strongs/h/h3427.md) with him the [yowm](../../strongs/h/h3117.md) of a [ḥōḏeš](../../strongs/h/h2320.md).

<a name="genesis_29_15"></a>Genesis 29:15

And [Lāḇān](../../strongs/h/h3837.md) ['āmar](../../strongs/h/h559.md) unto [Ya'aqob](../../strongs/h/h3290.md), Because thou art my ['ach](../../strongs/h/h251.md), shouldest thou therefore ['abad](../../strongs/h/h5647.md) me for [ḥinnām](../../strongs/h/h2600.md)? [nāḡaḏ](../../strongs/h/h5046.md) me, what shall thy [maśkōreṯ](../../strongs/h/h4909.md) be?

<a name="genesis_29_16"></a>Genesis 29:16

And [Lāḇān](../../strongs/h/h3837.md) had two [bath](../../strongs/h/h1323.md): the [shem](../../strongs/h/h8034.md) of the [gadowl](../../strongs/h/h1419.md) was [Lē'â](../../strongs/h/h3812.md), and the [shem](../../strongs/h/h8034.md) of the [qāṭān](../../strongs/h/h6996.md) was [Rāḥēl](../../strongs/h/h7354.md).

<a name="genesis_29_17"></a>Genesis 29:17

[Lē'â](../../strongs/h/h3812.md) was [raḵ](../../strongs/h/h7390.md) ['ayin](../../strongs/h/h5869.md); but [Rāḥēl](../../strongs/h/h7354.md) was [yāp̄ê](../../strongs/h/h3303.md) [tō'ar](../../strongs/h/h8389.md) and [yāp̄ê](../../strongs/h/h3303.md) [mar'ê](../../strongs/h/h4758.md).

<a name="genesis_29_18"></a>Genesis 29:18

And [Ya'aqob](../../strongs/h/h3290.md) ['ahab](../../strongs/h/h157.md) [Rāḥēl](../../strongs/h/h7354.md); and ['āmar](../../strongs/h/h559.md), I will ['abad](../../strongs/h/h5647.md) thee seven [šānâ](../../strongs/h/h8141.md) for [Rāḥēl](../../strongs/h/h7354.md) thy [qāṭān](../../strongs/h/h6996.md) [bath](../../strongs/h/h1323.md).

<a name="genesis_29_19"></a>Genesis 29:19

And [Lāḇān](../../strongs/h/h3837.md) ['āmar](../../strongs/h/h559.md), It is [towb](../../strongs/h/h2896.md) that I [nathan](../../strongs/h/h5414.md) her to thee, than that I should [nathan](../../strongs/h/h5414.md) her to ['aḥēr](../../strongs/h/h312.md) ['iysh](../../strongs/h/h376.md): [yashab](../../strongs/h/h3427.md) with me.

<a name="genesis_29_20"></a>Genesis 29:20

And [Ya'aqob](../../strongs/h/h3290.md) ['abad](../../strongs/h/h5647.md) seven [šānâ](../../strongs/h/h8141.md) for [Rāḥēl](../../strongs/h/h7354.md); and they ['ayin](../../strongs/h/h5869.md) unto him but a few [yowm](../../strongs/h/h3117.md), for the ['ahăḇâ](../../strongs/h/h160.md) he had to her.

<a name="genesis_29_21"></a>Genesis 29:21

And [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md) unto [Lāḇān](../../strongs/h/h3837.md), [yāhaḇ](../../strongs/h/h3051.md) me my ['ishshah](../../strongs/h/h802.md), for my [yowm](../../strongs/h/h3117.md) are [mālā'](../../strongs/h/h4390.md), that I may [bow'](../../strongs/h/h935.md) unto her.

<a name="genesis_29_22"></a>Genesis 29:22

And [Lāḇān](../../strongs/h/h3837.md) ['āsap̄](../../strongs/h/h622.md) all the ['enowsh](../../strongs/h/h582.md) of the [maqowm](../../strongs/h/h4725.md), and ['asah](../../strongs/h/h6213.md) a [mištê](../../strongs/h/h4960.md).

<a name="genesis_29_23"></a>Genesis 29:23

And it came to pass in the ['ereb](../../strongs/h/h6153.md), that he [laqach](../../strongs/h/h3947.md) [Lē'â](../../strongs/h/h3812.md) his [bath](../../strongs/h/h1323.md), and [bow'](../../strongs/h/h935.md) her to him; and he [bow'](../../strongs/h/h935.md) unto her.

<a name="genesis_29_24"></a>Genesis 29:24

And [Lāḇān](../../strongs/h/h3837.md) [nathan](../../strongs/h/h5414.md) unto his [bath](../../strongs/h/h1323.md) [Lē'â](../../strongs/h/h3812.md) [zilpâ](../../strongs/h/h2153.md) his [šip̄ḥâ](../../strongs/h/h8198.md) for a [šip̄ḥâ](../../strongs/h/h8198.md).

<a name="genesis_29_25"></a>Genesis 29:25

And it came to pass, that in the [boqer](../../strongs/h/h1242.md), behold, it was [Lē'â](../../strongs/h/h3812.md): and he ['āmar](../../strongs/h/h559.md) to [Lāḇān](../../strongs/h/h3837.md), What is this thou hast ['asah](../../strongs/h/h6213.md) unto me? did not I ['abad](../../strongs/h/h5647.md) with thee for [Rāḥēl](../../strongs/h/h7354.md)? wherefore then hast thou [rāmâ](../../strongs/h/h7411.md) me?

<a name="genesis_29_26"></a>Genesis 29:26

And [Lāḇān](../../strongs/h/h3837.md) ['āmar](../../strongs/h/h559.md), It must not be so ['asah](../../strongs/h/h6213.md) in our [maqowm](../../strongs/h/h4725.md), to [nathan](../../strongs/h/h5414.md) the [ṣāʿîr](../../strongs/h/h6810.md) [paniym](../../strongs/h/h6440.md) the [bᵊḵîrâ](../../strongs/h/h1067.md).

<a name="genesis_29_27"></a>Genesis 29:27

[mālā'](../../strongs/h/h4390.md) her [šāḇûaʿ](../../strongs/h/h7620.md), and we will [nathan](../../strongs/h/h5414.md) thee this also for the [ʿăḇōḏâ](../../strongs/h/h5656.md) which thou shalt ['abad](../../strongs/h/h5647.md) with me yet seven ['aḥēr](../../strongs/h/h312.md) [šānâ](../../strongs/h/h8141.md).

<a name="genesis_29_28"></a>Genesis 29:28

And [Ya'aqob](../../strongs/h/h3290.md) ['asah](../../strongs/h/h6213.md) so, and [mālā'](../../strongs/h/h4390.md) her [šāḇûaʿ](../../strongs/h/h7620.md): and he [nathan](../../strongs/h/h5414.md) him [Rāḥēl](../../strongs/h/h7354.md) his [bath](../../strongs/h/h1323.md) to ['ishshah](../../strongs/h/h802.md) also.

<a name="genesis_29_29"></a>Genesis 29:29

And [Lāḇān](../../strongs/h/h3837.md) [nathan](../../strongs/h/h5414.md) to [Rāḥēl](../../strongs/h/h7354.md) his [bath](../../strongs/h/h1323.md) [Bilhâ](../../strongs/h/h1090.md) his [šip̄ḥâ](../../strongs/h/h8198.md) to be her [šip̄ḥâ](../../strongs/h/h8198.md).

<a name="genesis_29_30"></a>Genesis 29:30

And he [bow'](../../strongs/h/h935.md) also unto [Rāḥēl](../../strongs/h/h7354.md), and he ['ahab](../../strongs/h/h157.md) also [Rāḥēl](../../strongs/h/h7354.md) more than [Lē'â](../../strongs/h/h3812.md), and ['abad](../../strongs/h/h5647.md) with him yet seven ['aḥēr](../../strongs/h/h312.md) [šānâ](../../strongs/h/h8141.md).

<a name="genesis_29_31"></a>Genesis 29:31

And when [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) that [Lē'â](../../strongs/h/h3812.md) was [sane'](../../strongs/h/h8130.md), he [pāṯaḥ](../../strongs/h/h6605.md) her [reḥem](../../strongs/h/h7358.md): but [Rāḥēl](../../strongs/h/h7354.md) was [ʿāqār](../../strongs/h/h6135.md).

<a name="genesis_29_32"></a>Genesis 29:32

And [Lē'â](../../strongs/h/h3812.md) [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md), and she [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Rᵊ'ûḇēn](../../strongs/h/h7205.md): for she ['āmar](../../strongs/h/h559.md), [kî](../../strongs/h/h3588.md) [Yĕhovah](../../strongs/h/h3068.md) hath [ra'ah](../../strongs/h/h7200.md) upon my ['oniy](../../strongs/h/h6040.md); now therefore my ['iysh](../../strongs/h/h376.md) will ['ahab](../../strongs/h/h157.md) me.

<a name="genesis_29_33"></a>Genesis 29:33

And she [harah](../../strongs/h/h2029.md) again, and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md); and ['āmar](../../strongs/h/h559.md), Because [Yĕhovah](../../strongs/h/h3068.md) hath [shama'](../../strongs/h/h8085.md) I was [sane'](../../strongs/h/h8130.md), he hath therefore [nathan](../../strongs/h/h5414.md) me this son also: and she [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Šimʿôn](../../strongs/h/h8095.md).

<a name="genesis_29_34"></a>Genesis 29:34

And she [harah](../../strongs/h/h2029.md) again, and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md); and ['āmar](../../strongs/h/h559.md), Now this [pa'am](../../strongs/h/h6471.md) will my ['iysh](../../strongs/h/h376.md) be [lāvâ](../../strongs/h/h3867.md) unto me, because I have [yalad](../../strongs/h/h3205.md) him three [ben](../../strongs/h/h1121.md): therefore was his [shem](../../strongs/h/h8034.md) [qara'](../../strongs/h/h7121.md) [Lēvî](../../strongs/h/h3878.md).

<a name="genesis_29_35"></a>Genesis 29:35

And she [harah](../../strongs/h/h2029.md) again, and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md): and she ['āmar](../../strongs/h/h559.md), [pa'am](../../strongs/h/h6471.md) will I [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md): therefore she [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Yehuwdah](../../strongs/h/h3063.md); and ['amad](../../strongs/h/h5975.md) [yalad](../../strongs/h/h3205.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 28](genesis_28.md) - [Genesis 30](genesis_30.md)