# [Genesis 20](https://www.blueletterbible.org/kjv/gen/20/1/s_20001)

<a name="genesis_20_1"></a>Genesis 20:1

And ['Abraham](../../strongs/h/h85.md) [nāsaʿ](../../strongs/h/h5265.md) from thence toward the [neḡeḇ](../../strongs/h/h5045.md) ['erets](../../strongs/h/h776.md), and [yashab](../../strongs/h/h3427.md) between [Qāḏēš](../../strongs/h/h6946.md) and [šûr](../../strongs/h/h7793.md), and [guwr](../../strongs/h/h1481.md) in [gᵊrār](../../strongs/h/h1642.md).

<a name="genesis_20_2"></a>Genesis 20:2

And ['Abraham](../../strongs/h/h85.md) ['āmar](../../strongs/h/h559.md) of [Śārâ](../../strongs/h/h8283.md) his ['ishshah](../../strongs/h/h802.md), She is my ['āḥôṯ](../../strongs/h/h269.md): and ['Ăḇîmeleḵ](../../strongs/h/h40.md) [melek](../../strongs/h/h4428.md) of [gᵊrār](../../strongs/h/h1642.md) [shalach](../../strongs/h/h7971.md), and [laqach](../../strongs/h/h3947.md) [Śārâ](../../strongs/h/h8283.md).

<a name="genesis_20_3"></a>Genesis 20:3

But ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) to ['Ăḇîmeleḵ](../../strongs/h/h40.md) in a [ḥălôm](../../strongs/h/h2472.md) by [layil](../../strongs/h/h3915.md), and ['āmar](../../strongs/h/h559.md) to him, Behold, thou art but [muwth](../../strongs/h/h4191.md), for the ['ishshah](../../strongs/h/h802.md) which thou hast [laqach](../../strongs/h/h3947.md); for she is a [baʿal](../../strongs/h/h1167.md) [bāʿal](../../strongs/h/h1166.md).

<a name="genesis_20_4"></a>Genesis 20:4

But ['Ăḇîmeleḵ](../../strongs/h/h40.md) had not [qāraḇ](../../strongs/h/h7126.md) her: and he ['āmar](../../strongs/h/h559.md),['adonay](../../strongs/h/h136.md), wilt thou [harag](../../strongs/h/h2026.md) also a [tsaddiyq](../../strongs/h/h6662.md) [gowy](../../strongs/h/h1471.md)?

<a name="genesis_20_5"></a>Genesis 20:5

['āmar](../../strongs/h/h559.md) he not unto me, She is my ['āḥôṯ](../../strongs/h/h269.md)? and she, even she herself ['āmar](../../strongs/h/h559.md), He is my ['ach](../../strongs/h/h251.md): in the [tom](../../strongs/h/h8537.md) of my [lebab](../../strongs/h/h3824.md) and [niqqāyôn](../../strongs/h/h5356.md) of my [kaph](../../strongs/h/h3709.md) have I ['asah](../../strongs/h/h6213.md) this.

<a name="genesis_20_6"></a>Genesis 20:6

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto him in a [ḥălôm](../../strongs/h/h2472.md), [gam](../../strongs/h/h1571.md), I [yada'](../../strongs/h/h3045.md) that thou ['asah](../../strongs/h/h6213.md) this in the [tom](../../strongs/h/h8537.md) of thy [lebab](../../strongs/h/h3824.md); for I also [ḥāśaḵ](../../strongs/h/h2820.md) thee from [chata'](../../strongs/h/h2398.md) against me: therefore [nathan](../../strongs/h/h5414.md) I thee not to [naga'](../../strongs/h/h5060.md) her.

<a name="genesis_20_7"></a>Genesis 20:7

Now therefore [shuwb](../../strongs/h/h7725.md) the ['iysh](../../strongs/h/h376.md) his ['ishshah](../../strongs/h/h802.md); for he is a [nāḇî'](../../strongs/h/h5030.md), and he shall [palal](../../strongs/h/h6419.md) for thee, and thou shalt [ḥāyâ](../../strongs/h/h2421.md): and if thou [shuwb](../../strongs/h/h7725.md) her not, [yada'](../../strongs/h/h3045.md) thou that thou shalt [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md), thou, and all that are thine.

<a name="genesis_20_8"></a>Genesis 20:8

Therefore ['Ăḇîmeleḵ](../../strongs/h/h40.md) [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [qara'](../../strongs/h/h7121.md) all his ['ebed](../../strongs/h/h5650.md), and [dabar](../../strongs/h/h1696.md) all these [dabar](../../strongs/h/h1697.md) in their ['ozen](../../strongs/h/h241.md): and the ['enowsh](../../strongs/h/h582.md) were [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md).

<a name="genesis_20_9"></a>Genesis 20:9

Then ['Ăḇîmeleḵ](../../strongs/h/h40.md) [qara'](../../strongs/h/h7121.md) ['Abraham](../../strongs/h/h85.md), and ['āmar](../../strongs/h/h559.md) unto him, What hast thou ['asah](../../strongs/h/h6213.md) unto us? and what have I [chata'](../../strongs/h/h2398.md) thee, that thou hast [bow'](../../strongs/h/h935.md) on me and on my [mamlāḵâ](../../strongs/h/h4467.md) a [gadowl](../../strongs/h/h1419.md) [ḥăṭā'â](../../strongs/h/h2401.md)? thou hast ['asah](../../strongs/h/h6213.md) [ma'aseh](../../strongs/h/h4639.md) unto me that ought not to be ['asah](../../strongs/h/h6213.md).

<a name="genesis_20_10"></a>Genesis 20:10

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) ['āmar](../../strongs/h/h559.md) unto ['Abraham](../../strongs/h/h85.md), What [ra'ah](../../strongs/h/h7200.md) thou, that thou hast ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md)?

<a name="genesis_20_11"></a>Genesis 20:11

And ['Abraham](../../strongs/h/h85.md) ['āmar](../../strongs/h/h559.md), Because I ['āmar](../../strongs/h/h559.md), [raq](../../strongs/h/h7535.md) the [yir'ah](../../strongs/h/h3374.md) of ['Elohiym](../../strongs/h/h430.md) is not in this [maqowm](../../strongs/h/h4725.md); and they will [harag](../../strongs/h/h2026.md) me for my ['ishshah](../../strongs/h/h802.md) [dabar](../../strongs/h/h1697.md).

<a name="genesis_20_12"></a>Genesis 20:12

And yet ['āmnâ](../../strongs/h/h546.md) she is my ['āḥôṯ](../../strongs/h/h269.md); she is the [bath](../../strongs/h/h1323.md) of my ['ab](../../strongs/h/h1.md), but not the [bath](../../strongs/h/h1323.md) of my ['em](../../strongs/h/h517.md); and she became my ['ishshah](../../strongs/h/h802.md).

<a name="genesis_20_13"></a>Genesis 20:13

And it came to pass, when ['Elohiym](../../strongs/h/h430.md) caused me to [tāʿâ](../../strongs/h/h8582.md) from my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), that I ['āmar](../../strongs/h/h559.md) unto her, This is thy [checed](../../strongs/h/h2617.md) which thou shalt ['asah](../../strongs/h/h6213.md) unto me; at every [maqowm](../../strongs/h/h4725.md) whither we shall [bow'](../../strongs/h/h935.md), ['āmar](../../strongs/h/h559.md) of me, He is my ['ach](../../strongs/h/h251.md).

<a name="genesis_20_14"></a>Genesis 20:14

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) [laqach](../../strongs/h/h3947.md) [tso'n](../../strongs/h/h6629.md), and [bāqār](../../strongs/h/h1241.md), and ['ebed](../../strongs/h/h5650.md), and [šip̄ḥâ](../../strongs/h/h8198.md), and [nathan](../../strongs/h/h5414.md) them unto ['Abraham](../../strongs/h/h85.md), and [shuwb](../../strongs/h/h7725.md) him [Śārâ](../../strongs/h/h8283.md) his ['ishshah](../../strongs/h/h802.md).

<a name="genesis_20_15"></a>Genesis 20:15

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) ['āmar](../../strongs/h/h559.md), Behold, my ['erets](../../strongs/h/h776.md) is [paniym](../../strongs/h/h6440.md) thee: [yashab](../../strongs/h/h3427.md) where it [towb](../../strongs/h/h2896.md) ['ayin](../../strongs/h/h5869.md) thee.

<a name="genesis_20_16"></a>Genesis 20:16

And unto [Śārâ](../../strongs/h/h8283.md) he ['āmar](../../strongs/h/h559.md), Behold, I have [nathan](../../strongs/h/h5414.md) thy ['ach](../../strongs/h/h251.md) a thousand pieces of [keceph](../../strongs/h/h3701.md): behold, he is to thee a [kᵊsûṯ](../../strongs/h/h3682.md) of the ['ayin](../../strongs/h/h5869.md), unto all that are with thee, and with all other: thus she was [yakach](../../strongs/h/h3198.md).

<a name="genesis_20_17"></a>Genesis 20:17

So ['Abraham](../../strongs/h/h85.md) [palal](../../strongs/h/h6419.md) unto ['Elohiym](../../strongs/h/h430.md): and ['Elohiym](../../strongs/h/h430.md) [rapha'](../../strongs/h/h7495.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md), and his ['ishshah](../../strongs/h/h802.md), and his ['amah](../../strongs/h/h519.md); and they [yalad](../../strongs/h/h3205.md).

<a name="genesis_20_18"></a>Genesis 20:18

For [Yĕhovah](../../strongs/h/h3068.md) had [ʿāṣar](../../strongs/h/h6113.md) [ʿāṣar](../../strongs/h/h6113.md) all the [reḥem](../../strongs/h/h7358.md) of the [bayith](../../strongs/h/h1004.md) of ['Ăḇîmeleḵ](../../strongs/h/h40.md), [dabar](../../strongs/h/h1697.md) of [Śārâ](../../strongs/h/h8283.md) ['Abraham](../../strongs/h/h85.md) ['ishshah](../../strongs/h/h802.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 19](genesis_19.md) - [Genesis 21](genesis_21.md)