# [Genesis 30](https://www.blueletterbible.org/kjv/gen/30/1/s_30001)

<a name="genesis_30_1"></a>Genesis 30:1

And when [Rāḥēl](../../strongs/h/h7354.md) [ra'ah](../../strongs/h/h7200.md) that she [yalad](../../strongs/h/h3205.md) [Ya'aqob](../../strongs/h/h3290.md) no children, [Rāḥēl](../../strongs/h/h7354.md) [qānā'](../../strongs/h/h7065.md) her ['āḥôṯ](../../strongs/h/h269.md); and ['āmar](../../strongs/h/h559.md) unto [Ya'aqob](../../strongs/h/h3290.md), [yāhaḇ](../../strongs/h/h3051.md) me [ben](../../strongs/h/h1121.md), or else I [muwth](../../strongs/h/h4191.md).

<a name="genesis_30_2"></a>Genesis 30:2

And [Ya'aqob](../../strongs/h/h3290.md) ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md) against [Rāḥēl](../../strongs/h/h7354.md): and he ['āmar](../../strongs/h/h559.md), Am I in ['Elohiym](../../strongs/h/h430.md) stead, who hath [mānaʿ](../../strongs/h/h4513.md) from thee the [pĕriy](../../strongs/h/h6529.md) of the [beten](../../strongs/h/h990.md)?

<a name="genesis_30_3"></a>Genesis 30:3

And she ['āmar](../../strongs/h/h559.md), Behold my ['amah](../../strongs/h/h519.md) [Bilhâ](../../strongs/h/h1090.md), [bow'](../../strongs/h/h935.md) unto her; and she shall [yalad](../../strongs/h/h3205.md) upon my [bereḵ](../../strongs/h/h1290.md), that I may also [bānâ](../../strongs/h/h1129.md) by her.

<a name="genesis_30_4"></a>Genesis 30:4

And she [nathan](../../strongs/h/h5414.md) him [Bilhâ](../../strongs/h/h1090.md) her [šip̄ḥâ](../../strongs/h/h8198.md) to ['ishshah](../../strongs/h/h802.md): and [Ya'aqob](../../strongs/h/h3290.md) [bow'](../../strongs/h/h935.md) unto her.

<a name="genesis_30_5"></a>Genesis 30:5

And [Bilhâ](../../strongs/h/h1090.md) [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) [Ya'aqob](../../strongs/h/h3290.md) a [ben](../../strongs/h/h1121.md).

<a name="genesis_30_6"></a>Genesis 30:6

And [Rāḥēl](../../strongs/h/h7354.md) ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) hath [diyn](../../strongs/h/h1777.md) me, and hath also [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md), and hath [nathan](../../strongs/h/h5414.md) me a [ben](../../strongs/h/h1121.md): therefore [qara'](../../strongs/h/h7121.md) she his [shem](../../strongs/h/h8034.md) [Dān](../../strongs/h/h1835.md).

<a name="genesis_30_7"></a>Genesis 30:7

And [Bilhâ](../../strongs/h/h1090.md) [Rāḥēl](../../strongs/h/h7354.md) [šip̄ḥâ](../../strongs/h/h8198.md) [harah](../../strongs/h/h2029.md) again, and [yalad](../../strongs/h/h3205.md) [Ya'aqob](../../strongs/h/h3290.md) a second [ben](../../strongs/h/h1121.md).

<a name="genesis_30_8"></a>Genesis 30:8

And [Rāḥēl](../../strongs/h/h7354.md) ['āmar](../../strongs/h/h559.md), With ['Elohiym](../../strongs/h/h430.md) [nap̄tûlîm](../../strongs/h/h5319.md) have I [pāṯal](../../strongs/h/h6617.md) with my ['āḥôṯ](../../strongs/h/h269.md), and I have [yakol](../../strongs/h/h3201.md): and she [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Nap̄tālî](../../strongs/h/h5321.md).

<a name="genesis_30_9"></a>Genesis 30:9

When [Lē'â](../../strongs/h/h3812.md) [ra'ah](../../strongs/h/h7200.md) that she had ['amad](../../strongs/h/h5975.md) [yalad](../../strongs/h/h3205.md), she [laqach](../../strongs/h/h3947.md) [zilpâ](../../strongs/h/h2153.md) her [šip̄ḥâ](../../strongs/h/h8198.md), and [nathan](../../strongs/h/h5414.md) her [Ya'aqob](../../strongs/h/h3290.md) to ['ishshah](../../strongs/h/h802.md).

<a name="genesis_30_10"></a>Genesis 30:10

And [zilpâ](../../strongs/h/h2153.md) [Lē'â](../../strongs/h/h3812.md) [šip̄ḥâ](../../strongs/h/h8198.md) [yalad](../../strongs/h/h3205.md) [Ya'aqob](../../strongs/h/h3290.md) a [ben](../../strongs/h/h1121.md).

<a name="genesis_30_11"></a>Genesis 30:11

And [Lē'â](../../strongs/h/h3812.md) ['āmar](../../strongs/h/h559.md), A [Gāḏ](../../strongs/h/h1409.md) [bow'](../../strongs/h/h935.md): and she [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Gāḏ](../../strongs/h/h1410.md).

<a name="genesis_30_12"></a>Genesis 30:12

And [zilpâ](../../strongs/h/h2153.md) [Lē'â](../../strongs/h/h3812.md) [šip̄ḥâ](../../strongs/h/h8198.md) [yalad](../../strongs/h/h3205.md) [Ya'aqob](../../strongs/h/h3290.md) a second [ben](../../strongs/h/h1121.md).

<a name="genesis_30_13"></a>Genesis 30:13

And [Lē'â](../../strongs/h/h3812.md) ['āmar](../../strongs/h/h559.md), ['šer](../../strongs/h/h837.md) am I, for the [bath](../../strongs/h/h1323.md) will call me ['āšar](../../strongs/h/h833.md): and she [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) ['Āšēr](../../strongs/h/h836.md).

<a name="genesis_30_14"></a>Genesis 30:14

And [Rᵊ'ûḇēn](../../strongs/h/h7205.md) [yālaḵ](../../strongs/h/h3212.md) the [yowm](../../strongs/h/h3117.md) of [ḥiṭṭâ](../../strongs/h/h2406.md) [qāṣîr](../../strongs/h/h7105.md), and [māṣā'](../../strongs/h/h4672.md) [dûḏay](../../strongs/h/h1736.md) in the [sadeh](../../strongs/h/h7704.md), and [bow'](../../strongs/h/h935.md) them unto his ['em](../../strongs/h/h517.md) [Lē'â](../../strongs/h/h3812.md). Then [Rāḥēl](../../strongs/h/h7354.md) ['āmar](../../strongs/h/h559.md) to [Lē'â](../../strongs/h/h3812.md), [nathan](../../strongs/h/h5414.md) me, I pray thee, of thy [ben](../../strongs/h/h1121.md) [dûḏay](../../strongs/h/h1736.md).

<a name="genesis_30_15"></a>Genesis 30:15

And she ['āmar](../../strongs/h/h559.md) unto her, Is it a [mᵊʿaṭ](../../strongs/h/h4592.md) that thou hast [laqach](../../strongs/h/h3947.md) my ['iysh](../../strongs/h/h376.md)? and wouldest thou [laqach](../../strongs/h/h3947.md) my [ben](../../strongs/h/h1121.md) [dûḏay](../../strongs/h/h1736.md) also? And [Rāḥēl](../../strongs/h/h7354.md) said, Therefore he shall [shakab](../../strongs/h/h7901.md) with thee to [layil](../../strongs/h/h3915.md) for thy [ben](../../strongs/h/h1121.md) [dûḏay](../../strongs/h/h1736.md).

<a name="genesis_30_16"></a>Genesis 30:16

And [Ya'aqob](../../strongs/h/h3290.md) [bow'](../../strongs/h/h935.md) out of the [sadeh](../../strongs/h/h7704.md) in the ['ereb](../../strongs/h/h6153.md), and [Lē'â](../../strongs/h/h3812.md) [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) him, and ['āmar](../../strongs/h/h559.md), Thou must [bow'](../../strongs/h/h935.md) unto me; for I have [śāḵar](../../strongs/h/h7936.md) [śāḵar](../../strongs/h/h7936.md) thee with my [ben](../../strongs/h/h1121.md) [dûḏay](../../strongs/h/h1736.md). And he [shakab](../../strongs/h/h7901.md) with her that [layil](../../strongs/h/h3915.md).

<a name="genesis_30_17"></a>Genesis 30:17

And ['Elohiym](../../strongs/h/h430.md) [shama'](../../strongs/h/h8085.md) unto [Lē'â](../../strongs/h/h3812.md), and she [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) [Ya'aqob](../../strongs/h/h3290.md) the fifth [ben](../../strongs/h/h1121.md).

<a name="genesis_30_18"></a>Genesis 30:18

And [Lē'â](../../strongs/h/h3812.md) ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) me my [śāḵār](../../strongs/h/h7939.md), because I have [nathan](../../strongs/h/h5414.md) my [šip̄ḥâ](../../strongs/h/h8198.md) to my ['iysh](../../strongs/h/h376.md): and she [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Yiśśāśḵār](../../strongs/h/h3485.md).

<a name="genesis_30_19"></a>Genesis 30:19

And [Lē'â](../../strongs/h/h3812.md) [harah](../../strongs/h/h2029.md) again, and [yalad](../../strongs/h/h3205.md) [Ya'aqob](../../strongs/h/h3290.md) the sixth [ben](../../strongs/h/h1121.md).

<a name="genesis_30_20"></a>Genesis 30:20

And [Lē'â](../../strongs/h/h3812.md) ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) hath [zāḇaḏ](../../strongs/h/h2064.md) me with a [towb](../../strongs/h/h2896.md) [zeḇeḏ](../../strongs/h/h2065.md); [pa'am](../../strongs/h/h6471.md) will my ['iysh](../../strongs/h/h376.md) [zāḇal](../../strongs/h/h2082.md) with me, because I have [yalad](../../strongs/h/h3205.md) him six [ben](../../strongs/h/h1121.md): and she [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Zᵊḇûlûn](../../strongs/h/h2074.md).

<a name="genesis_30_21"></a>Genesis 30:21

And ['aḥar](../../strongs/h/h310.md) she [yalad](../../strongs/h/h3205.md) a [bath](../../strongs/h/h1323.md), and [qara'](../../strongs/h/h7121.md) her [shem](../../strongs/h/h8034.md) [Dînâ](../../strongs/h/h1783.md).

<a name="genesis_30_22"></a>Genesis 30:22

And ['Elohiym](../../strongs/h/h430.md) [zakar](../../strongs/h/h2142.md) [Rāḥēl](../../strongs/h/h7354.md), and ['Elohiym](../../strongs/h/h430.md) [shama'](../../strongs/h/h8085.md) to her, and [pāṯaḥ](../../strongs/h/h6605.md) her [reḥem](../../strongs/h/h7358.md).

<a name="genesis_30_23"></a>Genesis 30:23

And she [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md); and ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) hath ['āsap̄](../../strongs/h/h622.md) my [cherpah](../../strongs/h/h2781.md):

<a name="genesis_30_24"></a>Genesis 30:24

And she [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Yôsēp̄](../../strongs/h/h3130.md); and ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) shall [yāsap̄](../../strongs/h/h3254.md) to me ['aḥēr](../../strongs/h/h312.md) [ben](../../strongs/h/h1121.md).

<a name="genesis_30_25"></a>Genesis 30:25

And it came to pass, when [Rāḥēl](../../strongs/h/h7354.md) had [yalad](../../strongs/h/h3205.md) [Yôsēp̄](../../strongs/h/h3130.md), that [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md) unto [Lāḇān](../../strongs/h/h3837.md), [shalach](../../strongs/h/h7971.md) me, that I may [yālaḵ](../../strongs/h/h3212.md) unto mine own [maqowm](../../strongs/h/h4725.md), and to my ['erets](../../strongs/h/h776.md).

<a name="genesis_30_26"></a>Genesis 30:26

[nathan](../../strongs/h/h5414.md) me my ['ishshah](../../strongs/h/h802.md) and my [yeleḏ](../../strongs/h/h3206.md), for whom I have ['abad](../../strongs/h/h5647.md) thee, and let me [yālaḵ](../../strongs/h/h3212.md): for thou [yada'](../../strongs/h/h3045.md) my [ʿăḇōḏâ](../../strongs/h/h5656.md) which I have ['abad](../../strongs/h/h5647.md) thee.

<a name="genesis_30_27"></a>Genesis 30:27

And [Lāḇān](../../strongs/h/h3837.md) ['āmar](../../strongs/h/h559.md) unto him, I pray thee, if I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thine ['ayin](../../strongs/h/h5869.md), tarry: for I have [nāḥaš](../../strongs/h/h5172.md) that [Yĕhovah](../../strongs/h/h3068.md) hath [barak](../../strongs/h/h1288.md) me for thy sake.

<a name="genesis_30_28"></a>Genesis 30:28

And he ['āmar](../../strongs/h/h559.md), [nāqaḇ](../../strongs/h/h5344.md) me thy [śāḵār](../../strongs/h/h7939.md), and I will [nathan](../../strongs/h/h5414.md) it.

<a name="genesis_30_29"></a>Genesis 30:29

And he ['āmar](../../strongs/h/h559.md) unto him, Thou [yada'](../../strongs/h/h3045.md) how I have ['abad](../../strongs/h/h5647.md) thee, and how thy [miqnê](../../strongs/h/h4735.md) was with me.

<a name="genesis_30_30"></a>Genesis 30:30

For it was [mᵊʿaṭ](../../strongs/h/h4592.md) which thou hadst [paniym](../../strongs/h/h6440.md) I came, and it is now [pāraṣ](../../strongs/h/h6555.md) unto a [rōḇ](../../strongs/h/h7230.md); and [Yĕhovah](../../strongs/h/h3068.md) hath [barak](../../strongs/h/h1288.md) thee since my [regel](../../strongs/h/h7272.md): and now when shall I ['asah](../../strongs/h/h6213.md) for mine own [bayith](../../strongs/h/h1004.md) also?

<a name="genesis_30_31"></a>Genesis 30:31

And he ['āmar](../../strongs/h/h559.md), What shall I [nathan](../../strongs/h/h5414.md) thee? And [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md), Thou shalt not [nathan](../../strongs/h/h5414.md) me [mᵊ'ûmâ](../../strongs/h/h3972.md): if thou wilt ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md) for me, I will [shuwb](../../strongs/h/h7725.md) [ra'ah](../../strongs/h/h7462.md) and [shamar](../../strongs/h/h8104.md) thy [tso'n](../../strongs/h/h6629.md).

<a name="genesis_30_32"></a>Genesis 30:32

I will ['abar](../../strongs/h/h5674.md) through all thy [tso'n](../../strongs/h/h6629.md) to [yowm](../../strongs/h/h3117.md), [cuwr](../../strongs/h/h5493.md) from thence all the [nāqōḏ](../../strongs/h/h5348.md) and [ṭālā'](../../strongs/h/h2921.md) [śê](../../strongs/h/h7716.md), and all the [ḥûm](../../strongs/h/h2345.md) [śê](../../strongs/h/h7716.md) among the [keśeḇ](../../strongs/h/h3775.md), and the [ṭālā'](../../strongs/h/h2921.md) and [nāqōḏ](../../strongs/h/h5348.md) among the [ʿēz](../../strongs/h/h5795.md): and of such shall be my [śāḵār](../../strongs/h/h7939.md).

<a name="genesis_30_33"></a>Genesis 30:33

So shall my [ṣĕdāqāh](../../strongs/h/h6666.md) ['anah](../../strongs/h/h6030.md) for me in [yowm](../../strongs/h/h3117.md) to [māḥār](../../strongs/h/h4279.md), when it shall [bow'](../../strongs/h/h935.md) for my [śāḵār](../../strongs/h/h7939.md) before thy [paniym](../../strongs/h/h6440.md): every one that is not [nāqōḏ](../../strongs/h/h5348.md) and [ṭālā'](../../strongs/h/h2921.md) among the [ʿēz](../../strongs/h/h5795.md), and [ḥûm](../../strongs/h/h2345.md) among the [keśeḇ](../../strongs/h/h3775.md), that shall be counted [ganab](../../strongs/h/h1589.md) with me.

<a name="genesis_30_34"></a>Genesis 30:34

And [Lāḇān](../../strongs/h/h3837.md) ['āmar](../../strongs/h/h559.md), Behold, I would it might be according to thy [dabar](../../strongs/h/h1697.md).

<a name="genesis_30_35"></a>Genesis 30:35

And he [cuwr](../../strongs/h/h5493.md) that [yowm](../../strongs/h/h3117.md) the [tayiš](../../strongs/h/h8495.md) that were [ʿāqōḏ](../../strongs/h/h6124.md) and [ṭālā'](../../strongs/h/h2921.md), and all the [ʿēz](../../strongs/h/h5795.md) that were [nāqōḏ](../../strongs/h/h5348.md) and [ṭālā'](../../strongs/h/h2921.md), and every one that had [lāḇān](../../strongs/h/h3836.md) in it, and all the [ḥûm](../../strongs/h/h2345.md) among the [keśeḇ](../../strongs/h/h3775.md), and [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of his [ben](../../strongs/h/h1121.md).

<a name="genesis_30_36"></a>Genesis 30:36

And he [śûm](../../strongs/h/h7760.md) three [yowm](../../strongs/h/h3117.md) [derek](../../strongs/h/h1870.md) betwixt himself and [Ya'aqob](../../strongs/h/h3290.md): and [Ya'aqob](../../strongs/h/h3290.md) [ra'ah](../../strongs/h/h7462.md) the [yāṯar](../../strongs/h/h3498.md) of [Lāḇān](../../strongs/h/h3837.md) [tso'n](../../strongs/h/h6629.md).

<a name="genesis_30_37"></a>Genesis 30:37

And [Ya'aqob](../../strongs/h/h3290.md) [laqach](../../strongs/h/h3947.md) him [maqqēl](../../strongs/h/h4731.md) of [laḥ](../../strongs/h/h3892.md) [liḇnê](../../strongs/h/h3839.md), and of the [Lûz](../../strongs/h/h3869.md) and [ʿarmôn](../../strongs/h/h6196.md); and [pāṣal](../../strongs/h/h6478.md) [lāḇān](../../strongs/h/h3836.md) [pᵊṣlôṯ](../../strongs/h/h6479.md) in them, and made the [lāḇān](../../strongs/h/h3836.md) [maḥśp̄](../../strongs/h/h4286.md) which was in the [maqqēl](../../strongs/h/h4731.md).

<a name="genesis_30_38"></a>Genesis 30:38

And he [yāṣaḡ](../../strongs/h/h3322.md) the [maqqēl](../../strongs/h/h4731.md) which he had [pāṣal](../../strongs/h/h6478.md) before the [tso'n](../../strongs/h/h6629.md) in the [rahaṭ](../../strongs/h/h7298.md) in the [mayim](../../strongs/h/h4325.md) [šōqeṯ](../../strongs/h/h8268.md) when the [tso'n](../../strongs/h/h6629.md) [bow'](../../strongs/h/h935.md) to [šāṯâ](../../strongs/h/h8354.md), that they should [yāḥam](../../strongs/h/h3179.md) when they [bow'](../../strongs/h/h935.md) to [šāṯâ](../../strongs/h/h8354.md).

<a name="genesis_30_39"></a>Genesis 30:39

And the [tso'n](../../strongs/h/h6629.md) [yāḥam](../../strongs/h/h3179.md) before the [maqqēl](../../strongs/h/h4731.md), and [yalad](../../strongs/h/h3205.md) [tso'n](../../strongs/h/h6629.md) [ʿāqōḏ](../../strongs/h/h6124.md), [nāqōḏ](../../strongs/h/h5348.md), and [ṭālā'](../../strongs/h/h2921.md).

<a name="genesis_30_40"></a>Genesis 30:40

And [Ya'aqob](../../strongs/h/h3290.md) did [pāraḏ](../../strongs/h/h6504.md) the [keśeḇ](../../strongs/h/h3775.md), and [nathan](../../strongs/h/h5414.md) the [paniym](../../strongs/h/h6440.md) of the [tso'n](../../strongs/h/h6629.md) toward the [ʿāqōḏ](../../strongs/h/h6124.md), and all the [ḥûm](../../strongs/h/h2345.md) in the [tso'n](../../strongs/h/h6629.md) of [Lāḇān](../../strongs/h/h3837.md); and he put his own [ʿēḏer](../../strongs/h/h5739.md) by themselves, and [shiyth](../../strongs/h/h7896.md) them not unto [Lāḇān](../../strongs/h/h3837.md) [tso'n](../../strongs/h/h6629.md).

<a name="genesis_30_41"></a>Genesis 30:41

And it came to pass, whensoever the [qāšar](../../strongs/h/h7194.md) [tso'n](../../strongs/h/h6629.md) did [yāḥam](../../strongs/h/h3179.md), that [Ya'aqob](../../strongs/h/h3290.md) [śûm](../../strongs/h/h7760.md) the [maqqēl](../../strongs/h/h4731.md) before the ['ayin](../../strongs/h/h5869.md) of the [tso'n](../../strongs/h/h6629.md) in the [rahaṭ](../../strongs/h/h7298.md), that they might [yāḥam](../../strongs/h/h3179.md) among the [maqqēl](../../strongs/h/h4731.md).

<a name="genesis_30_42"></a>Genesis 30:42

But when the [tso'n](../../strongs/h/h6629.md) were [ʿāṭap̄](../../strongs/h/h5848.md), he [śûm](../../strongs/h/h7760.md) them not: so the [ʿāṭap̄](../../strongs/h/h5848.md) were [Lāḇān](../../strongs/h/h3837.md), and the [qāšar](../../strongs/h/h7194.md) [Ya'aqob](../../strongs/h/h3290.md).

<a name="genesis_30_43"></a>Genesis 30:43

And the ['iysh](../../strongs/h/h376.md) [pāraṣ](../../strongs/h/h6555.md) [me'od](../../strongs/h/h3966.md) [me'od](../../strongs/h/h3966.md), and had [rab](../../strongs/h/h7227.md) [tso'n](../../strongs/h/h6629.md), and [šip̄ḥâ](../../strongs/h/h8198.md), and ['ebed](../../strongs/h/h5650.md), and [gāmāl](../../strongs/h/h1581.md), and [chamowr](../../strongs/h/h2543.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 29](genesis_29.md) - [Genesis 31](genesis_31.md)