# [Genesis 47](https://www.blueletterbible.org/kjv/gen/47/1/s_47001)

<a name="genesis_47_1"></a>Genesis 47:1

Then [Yôsēp̄](../../strongs/h/h3130.md) [bow'](../../strongs/h/h935.md) and [nāḡaḏ](../../strongs/h/h5046.md) [Parʿô](../../strongs/h/h6547.md), and ['āmar](../../strongs/h/h559.md), My ['ab](../../strongs/h/h1.md) and my ['ach](../../strongs/h/h251.md), and their [tso'n](../../strongs/h/h6629.md), and their [bāqār](../../strongs/h/h1241.md), and all that they have, are [bow'](../../strongs/h/h935.md) of the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md); and, behold, they are in the ['erets](../../strongs/h/h776.md) of [gōšen](../../strongs/h/h1657.md).

<a name="genesis_47_2"></a>Genesis 47:2

And he [laqach](../../strongs/h/h3947.md) [qāṣê](../../strongs/h/h7097.md) of his ['ach](../../strongs/h/h251.md), even five ['enowsh](../../strongs/h/h582.md), and [yāṣaḡ](../../strongs/h/h3322.md) them [paniym](../../strongs/h/h6440.md) [Parʿô](../../strongs/h/h6547.md).

<a name="genesis_47_3"></a>Genesis 47:3

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md) unto his ['ach](../../strongs/h/h251.md), What is your [ma'aseh](../../strongs/h/h4639.md)? And they ['āmar](../../strongs/h/h559.md) unto [Parʿô](../../strongs/h/h6547.md), Thy ['ebed](../../strongs/h/h5650.md) are [ra'ah](../../strongs/h/h7462.md) [tso'n](../../strongs/h/h6629.md), both we, and also our ['ab](../../strongs/h/h1.md).

<a name="genesis_47_4"></a>Genesis 47:4

They ['āmar](../../strongs/h/h559.md) morever unto [Parʿô](../../strongs/h/h6547.md), For to [guwr](../../strongs/h/h1481.md) in the ['erets](../../strongs/h/h776.md) are we [bow'](../../strongs/h/h935.md); for thy ['ebed](../../strongs/h/h5650.md) have no [mirʿê](../../strongs/h/h4829.md) for their [tso'n](../../strongs/h/h6629.md); for the [rāʿāḇ](../../strongs/h/h7458.md) is [kāḇēḏ](../../strongs/h/h3515.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md): now therefore, we pray thee, let thy ['ebed](../../strongs/h/h5650.md) [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [gōšen](../../strongs/h/h1657.md).

<a name="genesis_47_5"></a>Genesis 47:5

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md) unto [Yôsēp̄](../../strongs/h/h3130.md), ['āmar](../../strongs/h/h559.md), Thy ['ab](../../strongs/h/h1.md) and thy ['ach](../../strongs/h/h251.md) are [bow'](../../strongs/h/h935.md) unto thee:

<a name="genesis_47_6"></a>Genesis 47:6

The ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) is [paniym](../../strongs/h/h6440.md) thee; in the [mêṭāḇ](../../strongs/h/h4315.md) of the ['erets](../../strongs/h/h776.md) thy ['ab](../../strongs/h/h1.md) and ['ach](../../strongs/h/h251.md) [yashab](../../strongs/h/h3427.md); in the ['erets](../../strongs/h/h776.md) of [gōšen](../../strongs/h/h1657.md) let them [yashab](../../strongs/h/h3427.md): and if thou [yada'](../../strongs/h/h3045.md) any ['enowsh](../../strongs/h/h582.md) of [ḥayil](../../strongs/h/h2428.md) among them, then [śûm](../../strongs/h/h7760.md) them [śar](../../strongs/h/h8269.md) over my [miqnê](../../strongs/h/h4735.md).

<a name="genesis_47_7"></a>Genesis 47:7

And [Yôsēp̄](../../strongs/h/h3130.md) [bow'](../../strongs/h/h935.md) [Ya'aqob](../../strongs/h/h3290.md) his ['ab](../../strongs/h/h1.md), and ['amad](../../strongs/h/h5975.md) him [paniym](../../strongs/h/h6440.md) [Parʿô](../../strongs/h/h6547.md): and [Ya'aqob](../../strongs/h/h3290.md) [barak](../../strongs/h/h1288.md) [Parʿô](../../strongs/h/h6547.md).

<a name="genesis_47_8"></a>Genesis 47:8

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md) unto [Ya'aqob](../../strongs/h/h3290.md), [mah](../../strongs/h/h4100.md) [chay](../../strongs/h/h2416.md) [yowm](../../strongs/h/h3117.md) [šānâ](../../strongs/h/h8141.md) thou?

<a name="genesis_47_9"></a>Genesis 47:9

And [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md) unto [Parʿô](../../strongs/h/h6547.md), The [yowm](../../strongs/h/h3117.md) of the [šānâ](../../strongs/h/h8141.md) of my [māḡûr](../../strongs/h/h4033.md) are an hundred and thirty [šānâ](../../strongs/h/h8141.md): [mᵊʿaṭ](../../strongs/h/h4592.md) and [ra'](../../strongs/h/h7451.md) have the [yowm](../../strongs/h/h3117.md) of the [šānâ](../../strongs/h/h8141.md) of my [chay](../../strongs/h/h2416.md) been, and have not [nāśaḡ](../../strongs/h/h5381.md) unto the [yowm](../../strongs/h/h3117.md) of the [šānâ](../../strongs/h/h8141.md) of the [chay](../../strongs/h/h2416.md) of my ['ab](../../strongs/h/h1.md) in the [yowm](../../strongs/h/h3117.md) of their [māḡûr](../../strongs/h/h4033.md).

<a name="genesis_47_10"></a>Genesis 47:10

And [Ya'aqob](../../strongs/h/h3290.md) [barak](../../strongs/h/h1288.md) [Parʿô](../../strongs/h/h6547.md), and [yāṣā'](../../strongs/h/h3318.md) from [paniym](../../strongs/h/h6440.md) [Parʿô](../../strongs/h/h6547.md).

<a name="genesis_47_11"></a>Genesis 47:11

And [Yôsēp̄](../../strongs/h/h3130.md) [yashab](../../strongs/h/h3427.md) his ['ab](../../strongs/h/h1.md) and his ['ach](../../strongs/h/h251.md), and [nathan](../../strongs/h/h5414.md) them an ['achuzzah](../../strongs/h/h272.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), in the [mêṭāḇ](../../strongs/h/h4315.md) of the ['erets](../../strongs/h/h776.md), in the ['erets](../../strongs/h/h776.md) of [Raʿmᵊsēs](../../strongs/h/h7486.md), as [Parʿô](../../strongs/h/h6547.md) had [tsavah](../../strongs/h/h6680.md).

<a name="genesis_47_12"></a>Genesis 47:12

And [Yôsēp̄](../../strongs/h/h3130.md) [kûl](../../strongs/h/h3557.md) his ['ab](../../strongs/h/h1.md), and his ['ach](../../strongs/h/h251.md), and all his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), with [lechem](../../strongs/h/h3899.md), [peh](../../strongs/h/h6310.md) to their [ṭap̄](../../strongs/h/h2945.md).

<a name="genesis_47_13"></a>Genesis 47:13

And there was no [lechem](../../strongs/h/h3899.md) in all the ['erets](../../strongs/h/h776.md); for the [rāʿāḇ](../../strongs/h/h7458.md) was [me'od](../../strongs/h/h3966.md) [kāḇēḏ](../../strongs/h/h3515.md), so that the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) and all the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md) [lāhâ](../../strongs/h/h3856.md) by [paniym](../../strongs/h/h6440.md) of the [rāʿāḇ](../../strongs/h/h7458.md).

<a name="genesis_47_14"></a>Genesis 47:14

And [Yôsēp̄](../../strongs/h/h3130.md) [lāqaṭ](../../strongs/h/h3950.md) all the [keceph](../../strongs/h/h3701.md) that was [māṣā'](../../strongs/h/h4672.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), for the [šēḇer](../../strongs/h/h7668.md) which they [šāḇar](../../strongs/h/h7666.md): and [Yôsēp̄](../../strongs/h/h3130.md) [bow'](../../strongs/h/h935.md) the [keceph](../../strongs/h/h3701.md) into [Parʿô](../../strongs/h/h6547.md) [bayith](../../strongs/h/h1004.md).

<a name="genesis_47_15"></a>Genesis 47:15

And when [keceph](../../strongs/h/h3701.md) [tamam](../../strongs/h/h8552.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), all the [Mitsrayim](../../strongs/h/h4714.md) [bow'](../../strongs/h/h935.md) unto [Yôsēp̄](../../strongs/h/h3130.md), and ['āmar](../../strongs/h/h559.md), [yāhaḇ](../../strongs/h/h3051.md) us [lechem](../../strongs/h/h3899.md): for why should we [muwth](../../strongs/h/h4191.md) n thy presence? for the [keceph](../../strongs/h/h3701.md) ['āp̄as](../../strongs/h/h656.md).

<a name="genesis_47_16"></a>Genesis 47:16

And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md), [yāhaḇ](../../strongs/h/h3051.md) your [miqnê](../../strongs/h/h4735.md); and I will [nathan](../../strongs/h/h5414.md) you for your [miqnê](../../strongs/h/h4735.md), if [keceph](../../strongs/h/h3701.md) ['āp̄as](../../strongs/h/h656.md).

<a name="genesis_47_17"></a>Genesis 47:17

And they [bow'](../../strongs/h/h935.md) their [miqnê](../../strongs/h/h4735.md) unto [Yôsēp̄](../../strongs/h/h3130.md): and [Yôsēp̄](../../strongs/h/h3130.md) [nathan](../../strongs/h/h5414.md) them [lechem](../../strongs/h/h3899.md) in exchange for [sûs](../../strongs/h/h5483.md), and for the [tso'n](../../strongs/h/h6629.md), and for the [miqnê](../../strongs/h/h4735.md) of the [bāqār](../../strongs/h/h1241.md), and for the [chamowr](../../strongs/h/h2543.md): and he [nāhal](../../strongs/h/h5095.md) them with [lechem](../../strongs/h/h3899.md) for all their [miqnê](../../strongs/h/h4735.md) for that [šānâ](../../strongs/h/h8141.md).

<a name="genesis_47_18"></a>Genesis 47:18

When that [šānâ](../../strongs/h/h8141.md) was [tamam](../../strongs/h/h8552.md), they [bow'](../../strongs/h/h935.md) unto him the second [šānâ](../../strongs/h/h8141.md), and ['āmar](../../strongs/h/h559.md) unto him, We will not [kāḥaḏ](../../strongs/h/h3582.md) it from my ['adown](../../strongs/h/h113.md), how that our [keceph](../../strongs/h/h3701.md) is [tamam](../../strongs/h/h8552.md); my ['adown](../../strongs/h/h113.md) also hath our [miqnê](../../strongs/h/h4735.md) of [bĕhemah](../../strongs/h/h929.md); there is not [šā'ar](../../strongs/h/h7604.md) in the [paniym](../../strongs/h/h6440.md) of my ['adown](../../strongs/h/h113.md), but our [gᵊvîyâ](../../strongs/h/h1472.md), and our ['ăḏāmâ](../../strongs/h/h127.md):

<a name="genesis_47_19"></a>Genesis 47:19

Wherefore shall we [muwth](../../strongs/h/h4191.md) before thine ['ayin](../../strongs/h/h5869.md), both we and our ['ăḏāmâ](../../strongs/h/h127.md)? [qānâ](../../strongs/h/h7069.md) us and our ['ăḏāmâ](../../strongs/h/h127.md) for [lechem](../../strongs/h/h3899.md), and we and our ['ăḏāmâ](../../strongs/h/h127.md) will be ['ebed](../../strongs/h/h5650.md) unto [Parʿô](../../strongs/h/h6547.md): and [nathan](../../strongs/h/h5414.md) us [zera'](../../strongs/h/h2233.md), that we may [ḥāyâ](../../strongs/h/h2421.md), and not [muwth](../../strongs/h/h4191.md), that the ['ăḏāmâ](../../strongs/h/h127.md) be not [yāšam](../../strongs/h/h3456.md).

<a name="genesis_47_20"></a>Genesis 47:20

And [Yôsēp̄](../../strongs/h/h3130.md) [qānâ](../../strongs/h/h7069.md) all the ['ăḏāmâ](../../strongs/h/h127.md) of [Mitsrayim](../../strongs/h/h4714.md) for [Parʿô](../../strongs/h/h6547.md); for the [Mitsrayim](../../strongs/h/h4714.md) [māḵar](../../strongs/h/h4376.md) every ['iysh](../../strongs/h/h376.md) his [sadeh](../../strongs/h/h7704.md), because the [rāʿāḇ](../../strongs/h/h7458.md) [ḥāzaq](../../strongs/h/h2388.md) over them: so the ['erets](../../strongs/h/h776.md) became [Parʿô](../../strongs/h/h6547.md).

<a name="genesis_47_21"></a>Genesis 47:21

And as for the ['am](../../strongs/h/h5971.md), he ['abar](../../strongs/h/h5674.md) them to [ʿîr](../../strongs/h/h5892.md) from one [qāṣê](../../strongs/h/h7097.md) of the [gᵊḇûl](../../strongs/h/h1366.md) of [Mitsrayim](../../strongs/h/h4714.md) even to the other [qāṣê](../../strongs/h/h7097.md) thereof.

<a name="genesis_47_22"></a>Genesis 47:22

Only the ['ăḏāmâ](../../strongs/h/h127.md) of the [kōhēn](../../strongs/h/h3548.md) [qānâ](../../strongs/h/h7069.md) he not; for the [kōhēn](../../strongs/h/h3548.md) had a [choq](../../strongs/h/h2706.md) assigned them of [Parʿô](../../strongs/h/h6547.md), and did ['akal](../../strongs/h/h398.md) their [choq](../../strongs/h/h2706.md) which [Parʿô](../../strongs/h/h6547.md) [nathan](../../strongs/h/h5414.md) them: wherefore they [māḵar](../../strongs/h/h4376.md) not their ['ăḏāmâ](../../strongs/h/h127.md).

<a name="genesis_47_23"></a>Genesis 47:23

Then [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), Behold, I have [qānâ](../../strongs/h/h7069.md) you this [yowm](../../strongs/h/h3117.md) and your ['ăḏāmâ](../../strongs/h/h127.md) for [Parʿô](../../strongs/h/h6547.md): [hē'](../../strongs/h/h1887.md), here is [zera'](../../strongs/h/h2233.md) for you, and ye shall [zāraʿ](../../strongs/h/h2232.md) the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="genesis_47_24"></a>Genesis 47:24

And it shall come to pass in the [tᵊḇû'â](../../strongs/h/h8393.md), that ye shall [nathan](../../strongs/h/h5414.md) the [ḥămîšî](../../strongs/h/h2549.md) unto [Parʿô](../../strongs/h/h6547.md), and four [yad](../../strongs/h/h3027.md) shall be your own, for [zera'](../../strongs/h/h2233.md) of the [sadeh](../../strongs/h/h7704.md), and for your ['ōḵel](../../strongs/h/h400.md), and for them of your [bayith](../../strongs/h/h1004.md), and for ['akal](../../strongs/h/h398.md) for your [ṭap̄](../../strongs/h/h2945.md).

<a name="genesis_47_25"></a>Genesis 47:25

And they ['āmar](../../strongs/h/h559.md), Thou hast [ḥāyâ](../../strongs/h/h2421.md): let us [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in the ['ayin](../../strongs/h/h5869.md) of my ['adown](../../strongs/h/h113.md), and we will be [Parʿô](../../strongs/h/h6547.md) ['ebed](../../strongs/h/h5650.md).

<a name="genesis_47_26"></a>Genesis 47:26

And [Yôsēp̄](../../strongs/h/h3130.md) [śûm](../../strongs/h/h7760.md) it a [choq](../../strongs/h/h2706.md) over the ['ăḏāmâ](../../strongs/h/h127.md) of [Mitsrayim](../../strongs/h/h4714.md) unto this [yowm](../../strongs/h/h3117.md), that [Parʿô](../../strongs/h/h6547.md) should have the [ḥōmeš](../../strongs/h/h2569.md), except the ['ăḏāmâ](../../strongs/h/h127.md) of the [kōhēn](../../strongs/h/h3548.md) only, which became not [Parʿô](../../strongs/h/h6547.md).

<a name="genesis_47_27"></a>Genesis 47:27

And [Yisra'el](../../strongs/h/h3478.md) [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), in the ['erets](../../strongs/h/h776.md) of [gōšen](../../strongs/h/h1657.md); and they had ['āḥaz](../../strongs/h/h270.md) therein, and [parah](../../strongs/h/h6509.md), and [rabah](../../strongs/h/h7235.md) [me'od](../../strongs/h/h3966.md).

<a name="genesis_47_28"></a>Genesis 47:28

And [Ya'aqob](../../strongs/h/h3290.md) [ḥāyâ](../../strongs/h/h2421.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) seventeen years: so the [chay](../../strongs/h/h2416.md) [yowm](../../strongs/h/h3117.md) of [Ya'aqob](../../strongs/h/h3290.md) was an hundred forty and seven [šānâ](../../strongs/h/h8141.md).

<a name="genesis_47_29"></a>Genesis 47:29

And the [yowm](../../strongs/h/h3117.md) [qāraḇ](../../strongs/h/h7126.md) that [Yisra'el](../../strongs/h/h3478.md) must [muwth](../../strongs/h/h4191.md): and he [qara'](../../strongs/h/h7121.md) his [ben](../../strongs/h/h1121.md) [Yôsēp̄](../../strongs/h/h3130.md), and ['āmar](../../strongs/h/h559.md) unto him, If now I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md), [śûm](../../strongs/h/h7760.md), I pray thee, thy [yad](../../strongs/h/h3027.md) under my [yārēḵ](../../strongs/h/h3409.md), and ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md) with me; [qāḇar](../../strongs/h/h6912.md) me not, I pray thee, in [Mitsrayim](../../strongs/h/h4714.md):

<a name="genesis_47_30"></a>Genesis 47:30

But I will [shakab](../../strongs/h/h7901.md) with my ['ab](../../strongs/h/h1.md), and thou shalt [nasa'](../../strongs/h/h5375.md) me out of [Mitsrayim](../../strongs/h/h4714.md), and [qāḇar](../../strongs/h/h6912.md) me in their [qᵊḇûrâ](../../strongs/h/h6900.md). And he ['āmar](../../strongs/h/h559.md), I will ['asah](../../strongs/h/h6213.md) as thou hast [dabar](../../strongs/h/h1697.md).

<a name="genesis_47_31"></a>Genesis 47:31

And he ['āmar](../../strongs/h/h559.md), [shaba'](../../strongs/h/h7650.md) unto me. And he [shaba'](../../strongs/h/h7650.md) unto him. And [Yisra'el](../../strongs/h/h3478.md) [shachah](../../strongs/h/h7812.md) himself upon the [mittah](../../strongs/h/h4296.md) [ro'sh](../../strongs/h/h7218.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 46](genesis_46.md) - [Genesis 48](genesis_48.md)