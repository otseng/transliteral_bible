# [Genesis 17](https://www.blueletterbible.org/lxx/genesis/17)

<a name="genesis_17_1"></a>Genesis 17:1

[ginomai](../../strongs/g/g1096.md) Abram [etos](../../strongs/g/g2094.md) ninety (enenēkonta) [ennea](../../strongs/g/g1767.md), and the [kyrios](../../strongs/g/g2962.md) [horaō](../../strongs/g/g3708.md) to Abram, and he [eipon](../../strongs/g/g2036.md) to him, I am your [theos](../../strongs/g/g2316.md), you are [euaresteō](../../strongs/g/g2100.md) before me -- then be [amemptos](../../strongs/g/g273.md)! 

<a name="genesis_17_2"></a>Genesis 17:2

And I will [tithēmi](../../strongs/g/g5087.md) my [diathēkē](../../strongs/g/g1242.md) between me and between you, and I will [plēthynō](../../strongs/g/g4129.md) you [sphodra](../../strongs/g/g4970.md). 

<a name="genesis_17_3"></a>Genesis 17:3

And Abram [piptō](../../strongs/g/g4098.md) upon his [prosōpon](../../strongs/g/g4383.md). And [laleō](../../strongs/g/g2980.md) to him [theos](../../strongs/g/g2316.md), [legō](../../strongs/g/g3004.md), 

<a name="genesis_17_4"></a>Genesis 17:4

And [idou](../../strongs/g/g2400.md), my [diathēkē](../../strongs/g/g1242.md) is with you, and you will be [patēr](../../strongs/g/g3962.md) of a [plēthos](../../strongs/g/g4128.md) of [ethnos](../../strongs/g/g1484.md). 

<a name="genesis_17_5"></a>Genesis 17:5

And will not be [kaleō](../../strongs/g/g2564.md) any longer your [onoma](../../strongs/g/g3686.md), Abram; but will be your [onoma](../../strongs/g/g3686.md) [Abraam](../../strongs/g/g11.md), for [patēr](../../strongs/g/g3962.md) of [polys](../../strongs/g/g4183.md) [ethnos](../../strongs/g/g1484.md) I have [tithēmi](../../strongs/g/g5087.md) you. 

<a name="genesis_17_6"></a>Genesis 17:6

And I will [auxanō](../../strongs/g/g837.md) you [sphodra](../../strongs/g/g4970.md) [sphodra](../../strongs/g/g4970.md). And I will [tithēmi](../../strongs/g/g5087.md) you for [ethnos](../../strongs/g/g1484.md); and [basileus](../../strongs/g/g935.md) from you will [exerchomai](../../strongs/g/g1831.md). 

<a name="genesis_17_7"></a>Genesis 17:7

And I will [histēmi](../../strongs/g/g2476.md) my [diathēkē](../../strongs/g/g1242.md) between me and between you, and between your [sperma](../../strongs/g/g4690.md) after you, unto their [genea](../../strongs/g/g1074.md), for [diathēkē](../../strongs/g/g1242.md) an [aiōnios](../../strongs/g/g166.md), to be your [theos](../../strongs/g/g2316.md), and with your [sperma](../../strongs/g/g4690.md) after you. 

<a name="genesis_17_8"></a>Genesis 17:8

And I will [didōmi](../../strongs/g/g1325.md) to you and to your [sperma](../../strongs/g/g4690.md) after you the [gē](../../strongs/g/g1093.md) which you [paroikeō](../../strongs/g/g3939.md), all the [gē](../../strongs/g/g1093.md) of [Chanaan](../../strongs/g/g5477.md) for [kataschesis](../../strongs/g/g2697.md) an [aiōnios](../../strongs/g/g166.md). And I myself will be to them for [theos](../../strongs/g/g2316.md). 

<a name="genesis_17_9"></a>Genesis 17:9

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md) to [Abraam](../../strongs/g/g11.md), You also my [diathēkē](../../strongs/g/g1242.md) shall [diatēreō](../../strongs/g/g1301.md), and your [sperma](../../strongs/g/g4690.md) after you for their [genea](../../strongs/g/g1074.md). 

<a name="genesis_17_10"></a>Genesis 17:10

And this is the [diathēkē](../../strongs/g/g1242.md) which you shall [diatēreō](../../strongs/g/g1301.md) between me and you, and between your [sperma](../../strongs/g/g4690.md) after you. shall be [peritemnō](../../strongs/g/g4059.md) to you every male (arsenikon).

<a name="genesis_17_11"></a>Genesis 17:11

And you shall be [peritemnō](../../strongs/g/g4059.md) the [sarx](../../strongs/g/g4561.md) of your [akrobystia](../../strongs/g/g203.md). And it will be for a [sēmeion](../../strongs/g/g4592.md) of [diathēkē](../../strongs/g/g1242.md) between me and you. 

<a name="genesis_17_12"></a>Genesis 17:12

And a [paidion](../../strongs/g/g3813.md) [oktō](../../strongs/g/g3638.md) [hēmera](../../strongs/g/g2250.md) old shall be [peritemnō](../../strongs/g/g4059.md) by you -- every male (arsenikon) into your [genea](../../strongs/g/g1074.md); the native-born servant (oikogenēs) of your [oikia](../../strongs/g/g3614.md), and the one bought with silver (argyrōnētos), from every [huios](../../strongs/g/g5207.md) of an [allotrios](../../strongs/g/g245.md) who is not from your [sperma](../../strongs/g/g4690.md). 

<a name="genesis_17_13"></a>Genesis 17:13

By [peritomē](../../strongs/g/g4061.md) he shall [peritemnō](../../strongs/g/g4059.md) the native-born servant (oikogenēs) of your [oikia](../../strongs/g/g3614.md), and the one bought with silver (argyrōnētos). And will be my [diathēkē](../../strongs/g/g1242.md) upon your [sarx](../../strongs/g/g4561.md) for [diathēkē](../../strongs/g/g1242.md) an [aiōnios](../../strongs/g/g166.md). 

<a name="genesis_17_14"></a>Genesis 17:14

And the [aperitmētos](../../strongs/g/g564.md) [arrēn](../../strongs/g/g730.md) who shall not be [peritemnō](../../strongs/g/g4059.md) in the [sarx](../../strongs/g/g4561.md) of his [akrobystia](../../strongs/g/g203.md) in the [hēmera](../../strongs/g/g2250.md) [ogdoos](../../strongs/g/g3590.md), shall be [exolethreuō](../../strongs/g/g1842.md) that [psychē](../../strongs/g/g5590.md) from its [genos](../../strongs/g/g1085.md), for my [diathēkē](../../strongs/g/g1242.md) he effaced (dieskedasen).

<a name="genesis_17_15"></a>Genesis 17:15

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md) to [Abraam](../../strongs/g/g11.md), Sarai your [gynē](../../strongs/g/g1135.md) -- shall not be [kaleō](../../strongs/g/g2564.md) her [onoma](../../strongs/g/g3686.md) Sarai, but, [Sarra](../../strongs/g/g4564.md) will be her [onoma](../../strongs/g/g3686.md). 

<a name="genesis_17_16"></a>Genesis 17:16

And I will [eulogeō](../../strongs/g/g2127.md) her, and [didōmi](../../strongs/g/g1325.md) to you from her a [teknon](../../strongs/g/g5043.md). And I will [eulogeō](../../strongs/g/g2127.md) it, and it will be for [ethnos](../../strongs/g/g1484.md); and [basileus](../../strongs/g/g935.md) of [ethnos](../../strongs/g/g1484.md) from him will be.

<a name="genesis_17_17"></a>Genesis 17:17

And [Abraam](../../strongs/g/g11.md) [piptō](../../strongs/g/g4098.md) upon his [prosōpon](../../strongs/g/g4383.md) and [gelaō](../../strongs/g/g1070.md). And he [eipon](../../strongs/g/g2036.md) in his [dianoia](../../strongs/g/g1271.md), [legō](../../strongs/g/g3004.md), Shall to [hekatontaetēs](../../strongs/g/g1541.md) be a [huios](../../strongs/g/g5207.md)? And shall [Sarra](../../strongs/g/g4564.md) at ninety (enenēkonta) [etos](../../strongs/g/g2094.md) [tiktō](../../strongs/g/g5088.md)? 

<a name="genesis_17_18"></a>Genesis 17:18

[eipon](../../strongs/g/g2036.md) [Abraam](../../strongs/g/g11.md) to [theos](../../strongs/g/g2316.md), Ishmael (Ismaēl), this one, let him [zaō](../../strongs/g/g2198.md) [enantion](../../strongs/g/g1726.md) you! 

<a name="genesis_17_19"></a>Genesis 17:19

[eipon](../../strongs/g/g2036.md) [theos](../../strongs/g/g2316.md) to [Abraam](../../strongs/g/g11.md), [nai](../../strongs/g/g3483.md), [idou](../../strongs/g/g2400.md), [Sarra](../../strongs/g/g4564.md) your [gynē](../../strongs/g/g1135.md) will [tiktō](../../strongs/g/g5088.md) to you a [huios](../../strongs/g/g5207.md), and you shall [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md) [Isaak](../../strongs/g/g2464.md); and I will [histēmi](../../strongs/g/g2476.md) my [diathēkē](../../strongs/g/g1242.md) with him, for [diathēkē](../../strongs/g/g1242.md) an [aiōnios](../../strongs/g/g166.md), and to his [sperma](../../strongs/g/g4690.md) after him. 

<a name="genesis_17_20"></a>Genesis 17:20

And concerning Ishmael (Ismaēl), [idou](../../strongs/g/g2400.md), I [epakouō](../../strongs/g/g1873.md) you. And [idou](../../strongs/g/g2400.md), I shall [eulogeō](../../strongs/g/g2127.md) him, and I will [auxanō](../../strongs/g/g837.md) him, and I will [plēthynō](../../strongs/g/g4129.md) him [sphodra](../../strongs/g/g4970.md). [dōdeka](../../strongs/g/g1427.md) [ethnos](../../strongs/g/g1484.md) he will [gennaō](../../strongs/g/g1080.md), and I will [didōmi](../../strongs/g/g1325.md) him for [ethnos](../../strongs/g/g1484.md) [megas](../../strongs/g/g3173.md). 

<a name="genesis_17_21"></a>Genesis 17:21

 But my [diathēkē](../../strongs/g/g1242.md) I will [histēmi](../../strongs/g/g2476.md) with [Isaak](../../strongs/g/g2464.md), whom shall [tiktō](../../strongs/g/g5088.md) to you [Sarra](../../strongs/g/g4564.md) at this [kairos](../../strongs/g/g2540.md) in [eniautos](../../strongs/g/g1763.md) another. 

<a name="genesis_17_22"></a>Genesis 17:22

And he [synteleō](../../strongs/g/g4931.md) [laleō](../../strongs/g/g2980.md) to him. And [theos](../../strongs/g/g2316.md) [anabainō](../../strongs/g/g305.md) from [Abraam](../../strongs/g/g11.md).

<a name="genesis_17_23"></a>Genesis 17:23

And [Abraam](../../strongs/g/g11.md) [lambanō](../../strongs/g/g2983.md) Ishmael (Ismaēl), his [huios](../../strongs/g/g5207.md), and all his native-born servants (oikogeneis), and all the ones bought with silver (argyrōnētous), and every [arrēn](../../strongs/g/g730.md) of the [anēr](../../strongs/g/g435.md) in the [oikos](../../strongs/g/g3624.md) of [Abraam](../../strongs/g/g11.md). And he [peritemnō](../../strongs/g/g4059.md) their [akrobystia](../../strongs/g/g203.md) in [kairos](../../strongs/g/g2540.md) of the [hēmera](../../strongs/g/g2250.md) that as [laleō](../../strongs/g/g2980.md) to him [theos](../../strongs/g/g2316.md). 

<a name="genesis_17_24"></a>Genesis 17:24

And [Abraam](../../strongs/g/g11.md) ninety (enenēkonta) [ennea](../../strongs/g/g1767.md) was [etos](../../strongs/g/g2094.md) when he [peritemnō](../../strongs/g/g4059.md) the [sarx](../../strongs/g/g4561.md) of his [akrobystia](../../strongs/g/g203.md).

<a name="genesis_17_25"></a>Genesis 17:25

And Ishmael (Ismaēl) his [huios](../../strongs/g/g5207.md) was [etos](../../strongs/g/g2094.md) [deka](../../strongs/g/g1176.md) [treis](../../strongs/g/g5140.md) when he [peritemnō](../../strongs/g/g4059.md) the [sarx](../../strongs/g/g4561.md) of his [akrobystia](../../strongs/g/g203.md).

<a name="genesis_17_26"></a>Genesis 17:26

In [kairos](../../strongs/g/g2540.md) of the [hēmera](../../strongs/g/g2250.md) that [Abraam](../../strongs/g/g11.md) was [peritemnō](../../strongs/g/g4059.md), and Ishmael (Ismaēl) his [huios](../../strongs/g/g5207.md), 

<a name="genesis_17_27"></a>Genesis 17:27

and all the [anēr](../../strongs/g/g435.md) of his [oikos](../../strongs/g/g3624.md), and the native-born servants (oikogeneis), and the ones bought with silver (argyrōnētoi) from [allogenēs](../../strongs/g/g241.md) [ethnos](../../strongs/g/g1484.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 16](genesis_lxx_16.md) - [Genesis 18](genesis_lxx_18.md)