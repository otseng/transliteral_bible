# [Genesis 16](https://www.blueletterbible.org/lxx/genesis/16)

<a name="genesis_16_1"></a>Genesis 16:1

And Sarai, the [gynē](../../strongs/g/g1135.md) of Abram, [tiktō](../../strongs/g/g5088.md) not to him. But there was to her a [paidiskē](../../strongs/g/g3814.md), an Egyptian (Aigyptia), whose [onoma](../../strongs/g/g3686.md) was [Agar](../../strongs/g/g28.md).

<a name="genesis_16_2"></a>Genesis 16:2

[eipon](../../strongs/g/g2036.md) Sarai to Abram, [idou](../../strongs/g/g2400.md), closed me up (synekleisen) the [kyrios](../../strongs/g/g2962.md) to not [tiktō](../../strongs/g/g5088.md). [eiserchomai](../../strongs/g/g1525.md) then to my [paidiskē](../../strongs/g/g3814.md), that I may produce children (teknopoiēsēs) from her! [hypakouō](../../strongs/g/g5219.md) Abram the [phōnē](../../strongs/g/g5456.md) of Sarai.

<a name="genesis_16_3"></a>Genesis 16:3

And [lambanō](../../strongs/g/g2983.md) the [gynē](../../strongs/g/g1135.md) of Abram, [Agar](../../strongs/g/g28.md) the Egyptian (Aigyptian) her [paidiskē](../../strongs/g/g3814.md), after [deka](../../strongs/g/g1176.md) [etos](../../strongs/g/g2094.md) of [oikeō](../../strongs/g/g3611.md) with Abram in the [gē](../../strongs/g/g1093.md) of [Chanaan](../../strongs/g/g5477.md), and she [didōmi](../../strongs/g/g1325.md) her to Abram her [anēr](../../strongs/g/g435.md) to him for [gynē](../../strongs/g/g1135.md). 

<a name="genesis_16_4"></a>Genesis 16:4

And he [eiserchomai](../../strongs/g/g1525.md) to [Agar](../../strongs/g/g28.md), and she [syllambanō](../../strongs/g/g4815.md). And she [eidō](../../strongs/g/g1492.md) that in the [gastēr](../../strongs/g/g1064.md) she had a child, and was [atimazō](../../strongs/g/g818.md) the [kyria](../../strongs/g/g2959.md) [enantion](../../strongs/g/g1726.md) her. 

<a name="genesis_16_5"></a>Genesis 16:5

[eipon](../../strongs/g/g2036.md) Sarai to Abram, I am being [adikeō](../../strongs/g/g91.md) because of you. I [didōmi](../../strongs/g/g1325.md) my [paidiskē](../../strongs/g/g3814.md) to your [kolpos](../../strongs/g/g2859.md). And [eidō](../../strongs/g/g1492.md) that a child in the [gastēr](../../strongs/g/g1064.md) she had, I was [atimazō](../../strongs/g/g818.md) [enantion](../../strongs/g/g1726.md) her. May [theos](../../strongs/g/g2316.md) [krinō](../../strongs/g/g2919.md) between me and you. 

<a name="genesis_16_6"></a>Genesis 16:6

[eipon](../../strongs/g/g2036.md) Abram to Sarai, [idou](../../strongs/g/g2400.md), your [paidiskē](../../strongs/g/g3814.md) is in your [cheir](../../strongs/g/g5495.md), [chraomai](../../strongs/g/g5530.md) her however pleasing (areston) to you might be. And [kakoō](../../strongs/g/g2559.md) her Sarai, and she ran away (apedra) from her [prosōpon](../../strongs/g/g4383.md). 

<a name="genesis_16_7"></a>Genesis 16:7

[heuriskō](../../strongs/g/g2147.md) her the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) upon the [pēgē](../../strongs/g/g4077.md) of [hydōr](../../strongs/g/g5204.md) in the [erēmos](../../strongs/g/g2048.md), upon the [gē](../../strongs/g/g1093.md) in the [hodos](../../strongs/g/g3598.md) of Shur.

<a name="genesis_16_8"></a>Genesis 16:8

And [eipon](../../strongs/g/g2036.md) to her the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md), [Agar](../../strongs/g/g28.md), [paidiskē](../../strongs/g/g3814.md) of Sarai, [pothen](../../strongs/g/g4159.md) do you [erchomai](../../strongs/g/g2064.md), and where do you [poreuō](../../strongs/g/g4198.md)? And she [eipon](../../strongs/g/g2036.md), from the [prosōpon](../../strongs/g/g4383.md) of Sarai my [kyria](../../strongs/g/g2959.md) I am running away (apodidraskō).

<a name="genesis_16_9"></a>Genesis 16:9

[eipon](../../strongs/g/g2036.md) to her the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md), You [apostrephō](../../strongs/g/g654.md) to your [kyria](../../strongs/g/g2959.md), and be [tapeinoō](../../strongs/g/g5013.md) under her [cheir](../../strongs/g/g5495.md)! 

<a name="genesis_16_10"></a>Genesis 16:10

And [eipon](../../strongs/g/g2036.md) to her the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md), In [plēthynō](../../strongs/g/g4129.md) I will [plēthynō](../../strongs/g/g4129.md) your [sperma](../../strongs/g/g4690.md), and it shall not be [arithmeō](../../strongs/g/g705.md) because of the [plēthos](../../strongs/g/g4128.md). 

<a name="genesis_16_11"></a>Genesis 16:11

And [eipon](../../strongs/g/g2036.md) to her the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md), [idou](../../strongs/g/g2400.md), you in the [gastēr](../../strongs/g/g1064.md) have a child, and you shall [tiktō](../../strongs/g/g5088.md) a [huios](../../strongs/g/g5207.md), and you shall [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md), Ishmael, for the [kyrios](../../strongs/g/g2962.md) [epakouō](../../strongs/g/g1873.md) your [tapeinōsis](../../strongs/g/g5014.md). 

<a name="genesis_16_12"></a>Genesis 16:12

This one will be a rugged (agroikos) [anthrōpos](../../strongs/g/g444.md); his [cheir](../../strongs/g/g5495.md) will be upon all, and the [cheir](../../strongs/g/g5495.md) of all upon him. And before the [prosōpon](../../strongs/g/g4383.md) of all his [adelphos](../../strongs/g/g80.md) he will [katoikeō](../../strongs/g/g2730.md). 

<a name="genesis_16_13"></a>Genesis 16:13

And she [kaleō](../../strongs/g/g2564.md) the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md), the one [laleō](../../strongs/g/g2980.md) to her, You, the [theos](../../strongs/g/g2316.md), the one [epeidon](../../strongs/g/g1896.md) me; for she [eipon](../../strongs/g/g2036.md), For even [enōpion](../../strongs/g/g1799.md) I [eidō](../../strongs/g/g1492.md) the one [horaō](../../strongs/g/g3708.md) to me. 

<a name="genesis_16_14"></a>Genesis 16:14

Because of this she [kaleō](../../strongs/g/g2564.md) the [phrear](../../strongs/g/g5421.md), [phrear](../../strongs/g/g5421.md) of which [enōpion](../../strongs/g/g1799.md) [horaō](../../strongs/g/g3708.md). [idou](../../strongs/g/g2400.md), it is [mesos](../../strongs/g/g3319.md) Kadesh and [mesos](../../strongs/g/g3319.md) Bared.

<a name="genesis_16_15"></a>Genesis 16:15

And [Agar](../../strongs/g/g28.md) [tiktō](../../strongs/g/g5088.md) to Abram a [huios](../../strongs/g/g5207.md). And Abram [kaleō](../../strongs/g/g2564.md) the [onoma](../../strongs/g/g3686.md) of his son, whom [tiktō](../../strongs/g/g5088.md) to him [Agar](../../strongs/g/g28.md), Ishmael.

<a name="genesis_16_16"></a>Genesis 16:16

And Abram was [etos](../../strongs/g/g2094.md) [ogdoēkonta](../../strongs/g/g3589.md) [ex](../../strongs/g/g1803.md) when [Agar](../../strongs/g/g28.md) [tiktō](../../strongs/g/g5088.md) to Abram, Ishmael.

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 15](genesis_lxx_15.md) - [Genesis 17](genesis_lxx_17.md)