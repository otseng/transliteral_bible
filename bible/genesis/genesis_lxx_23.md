# [Genesis 23](https://www.blueletterbible.org/lxx/genesis/23)

<a name="genesis_23_1"></a>Genesis 23:1

[ginomai](../../strongs/g/g1096.md) the [zōē](../../strongs/g/g2222.md) of [Sarra](../../strongs/g/g4564.md) [etos](../../strongs/g/g2094.md) a [hekaton](../../strongs/g/g1540.md) [eikosi](../../strongs/g/g1501.md) [hepta](../../strongs/g/g2033.md).

<a name="genesis_23_2"></a>Genesis 23:2

And [Sarra](../../strongs/g/g4564.md) [apothnēskō](../../strongs/g/g599.md) in the [polis](../../strongs/g/g4172.md) of Arba, which is in the hollow (koilōmati) -- this is Hebron in the [gē](../../strongs/g/g1093.md) of [Chanaan](../../strongs/g/g5477.md). [erchomai](../../strongs/g/g2064.md) [Abraam](../../strongs/g/g11.md) to [koptō](../../strongs/g/g2875.md) [Sarra](../../strongs/g/g4564.md), and to [pentheō](../../strongs/g/g3996.md). 

<a name="genesis_23_3"></a>Genesis 23:3

And [Abraam](../../strongs/g/g11.md) rose [anabainō](../../strongs/g/g305.md) from his [nekros](../../strongs/g/g3498.md). And [Abraam](../../strongs/g/g11.md) [eipon](../../strongs/g/g2036.md) to the [huios](../../strongs/g/g5207.md) of Heth, [legō](../../strongs/g/g3004.md), 

<a name="genesis_23_4"></a>Genesis 23:4

A [paroikos](../../strongs/g/g3941.md) and an [parepidēmos](../../strongs/g/g3927.md) I am among you. [didōmi](../../strongs/g/g1325.md) then to me a possession (ktēsin) of a [taphos](../../strongs/g/g5028.md) among you! and I will [thaptō](../../strongs/g/g2290.md) my [nekros](../../strongs/g/g3498.md) away from me. 

<a name="genesis_23_5"></a>Genesis 23:5

And [apokrinomai](../../strongs/g/g611.md) the [huios](../../strongs/g/g5207.md) of Heth to [Abraam](../../strongs/g/g11.md), [legō](../../strongs/g/g3004.md), No, O [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_23_6"></a>Genesis 23:6

But [akouō](../../strongs/g/g191.md) us; as a [basileus](../../strongs/g/g935.md) by [theos](../../strongs/g/g2316.md) you are with us. [eklektos](../../strongs/g/g1588.md) [mnēmeion](../../strongs/g/g3419.md) our You [thaptō](../../strongs/g/g2290.md) your [nekros](../../strongs/g/g3498.md)! for not one of us in any way will [kōlyō](../../strongs/g/g2967.md) his [mnēmeion](../../strongs/g/g3419.md) from you to [thaptō](../../strongs/g/g2290.md) your [nekros](../../strongs/g/g3498.md) there. 

<a name="genesis_23_7"></a>Genesis 23:7

And [anistēmi](../../strongs/g/g450.md), [Abraam](../../strongs/g/g11.md) did [proskyneō](../../strongs/g/g4352.md) to the [laos](../../strongs/g/g2992.md) of the [gē](../../strongs/g/g1093.md), to the [huios](../../strongs/g/g5207.md) of Heth.

<a name="genesis_23_8"></a>Genesis 23:8

And [laleō](../../strongs/g/g2980.md) to them [Abraam](../../strongs/g/g11.md), [legō](../../strongs/g/g3004.md), If you have it in your [psychē](../../strongs/g/g5590.md) so as to [thaptō](../../strongs/g/g2290.md) my [nekros](../../strongs/g/g3498.md) away from my [prosōpon](../../strongs/g/g4383.md), [akouō](../../strongs/g/g191.md) me, and [laleō](../../strongs/g/g2980.md) about me to Ephron, the son of Zohar!

<a name="genesis_23_9"></a>Genesis 23:9

And let him [didōmi](../../strongs/g/g1325.md) to me the [spēlaion](../../strongs/g/g4693.md) [diplous](../../strongs/g/g1362.md) which is his -- the one being in a [meros](../../strongs/g/g3313.md) of his [agros](../../strongs/g/g68.md). of [argyrion](../../strongs/g/g694.md) for its [axios](../../strongs/g/g514.md) Let him [didōmi](../../strongs/g/g1325.md) it to me, among you for a possession (ktēsin) of a [mnēmeion](../../strongs/g/g3419.md). 

<a name="genesis_23_10"></a>Genesis 23:10

"And Ephron was [kathēmai](../../strongs/g/g2521.md) in the [mesos](../../strongs/g/g3319.md) of the [huios](../../strongs/g/g5207.md) of Heth. And [apokrinomai](../../strongs/g/g611.md) Ephron the Hittite to [Abraam](../../strongs/g/g11.md) [eipon](../../strongs/g/g2036.md) in the [akouō](../../strongs/g/g191.md) of the [huios](../../strongs/g/g5207.md) of Heth, and the ones [eisporeuomai](../../strongs/g/g1531.md) into the [polis](../../strongs/g/g4172.md) of all, [legō](../../strongs/g/g3004.md), "

<a name="genesis_23_11"></a>Genesis 23:11

By me [ginomai](../../strongs/g/g1096.md), O [kyrios](../../strongs/g/g2962.md), and [akouō](../../strongs/g/g191.md) me! The [agros](../../strongs/g/g68.md) and the [spēlaion](../../strongs/g/g4693.md) in it I [didōmi](../../strongs/g/g1325.md) to you. [enantion](../../strongs/g/g1726.md) all of my [politēs](../../strongs/g/g4177.md) I have [didōmi](../../strongs/g/g1325.md) to you. You [thaptō](../../strongs/g/g2290.md) your [nekros](../../strongs/g/g3498.md). 

<a name="genesis_23_12"></a>Genesis 23:12

And [Abraam](../../strongs/g/g11.md) did [proskyneō](../../strongs/g/g4352.md) [enantion](../../strongs/g/g1726.md) the [laos](../../strongs/g/g2992.md) of the [gē](../../strongs/g/g1093.md). 

<a name="genesis_23_13"></a>Genesis 23:13

And he [eipon](../../strongs/g/g2036.md) to Ephron in the [ous](../../strongs/g/g3775.md) [enantion](../../strongs/g/g1726.md) the [laos](../../strongs/g/g2992.md) of the [gē](../../strongs/g/g1093.md), Since with me you are [akouō](../../strongs/g/g191.md) me! The [argyrion](../../strongs/g/g694.md) for the [agros](../../strongs/g/g68.md), you [lambanō](../../strongs/g/g2983.md) from me! and I will [thaptō](../../strongs/g/g2290.md) my [nekros](../../strongs/g/g3498.md) there. 

<a name="genesis_23_14"></a>Genesis 23:14

[apokrinomai](../../strongs/g/g611.md) Ephron [Abraam](../../strongs/g/g11.md), [legō](../../strongs/g/g3004.md), 

<a name="genesis_23_15"></a>Genesis 23:15

Not so O [kyrios](../../strongs/g/g2962.md), for I [akouō](../../strongs/g/g191.md) [tetrakosioi](../../strongs/g/g5071.md) [didrachmon](../../strongs/g/g1323.md) of [argyrion](../../strongs/g/g694.md); but what ever may this be between me and you? But you your [nekros](../../strongs/g/g3498.md) [thaptō](../../strongs/g/g2290.md)! 

<a name="genesis_23_16"></a>Genesis 23:16

And [Abraam](../../strongs/g/g11.md) [akouō](../../strongs/g/g191.md) to Ephron. And [Abraam](../../strongs/g/g11.md) [apokathistēmi](../../strongs/g/g600.md) to Ephron the [argyrion](../../strongs/g/g694.md), which he [laleō](../../strongs/g/g2980.md) into the [ous](../../strongs/g/g3775.md) of the [huios](../../strongs/g/g5207.md) of Heth -- [tetrakosioi](../../strongs/g/g5071.md) [didrachmon](../../strongs/g/g1323.md) [argyrion](../../strongs/g/g694.md) of [dokimos](../../strongs/g/g1384.md) of [emporos](../../strongs/g/g1713.md). 

<a name="genesis_23_17"></a>Genesis 23:17

And [histēmi](../../strongs/g/g2476.md) the [agros](../../strongs/g/g68.md) of Ephron which was in [diplous](../../strongs/g/g1362.md) [spēlaion](../../strongs/g/g4693.md), which is against the [prosōpon](../../strongs/g/g4383.md) of Mamre. The [agros](../../strongs/g/g68.md) and the [spēlaion](../../strongs/g/g4693.md) which was in it, and every [dendron](../../strongs/g/g1186.md) which was in the [agros](../../strongs/g/g68.md), which is in its [horion](../../strongs/g/g3725.md) [kyklō](../../strongs/g/g2945.md), 

<a name="genesis_23_18"></a>Genesis 23:18

came to [Abraam](../../strongs/g/g11.md) for a possession (ktēsin) [enantion](../../strongs/g/g1726.md) the [huios](../../strongs/g/g5207.md) of Heth, and all the ones [eisporeuomai](../../strongs/g/g1531.md) into the [polis](../../strongs/g/g4172.md). 

<a name="genesis_23_19"></a>Genesis 23:19

And after these things [Abraam](../../strongs/g/g11.md) [thaptō](../../strongs/g/g2290.md) [Sarra](../../strongs/g/g4564.md) his [gynē](../../strongs/g/g1135.md) in the [spēlaion](../../strongs/g/g4693.md) of the [agros](../../strongs/g/g68.md) at [diplous](../../strongs/g/g1362.md) cave (spēlaiō) , which is [apenanti](../../strongs/g/g561.md) Mamre; this is Hebron in the [gē](../../strongs/g/g1093.md) of [Chanaan](../../strongs/g/g5477.md).

<a name="genesis_23_20"></a>Genesis 23:20

And was [kyroō](../../strongs/g/g2964.md) the [agros](../../strongs/g/g68.md), and the [spēlaion](../../strongs/g/g4693.md) which was in it to [Abraam](../../strongs/g/g11.md) for a possession (ktēsin) of a [taphos](../../strongs/g/g5028.md) from the [huios](../../strongs/g/g5207.md) of Heth.

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 22](genesis_lxx_22.md) - [Genesis 24](genesis_lxx_24.md)