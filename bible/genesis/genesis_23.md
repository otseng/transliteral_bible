# [Genesis 23](https://www.blueletterbible.org/kjv/gen/23/1/s_23001)

<a name="genesis_23_1"></a>Genesis 23:1

And [Śārâ](../../strongs/h/h8283.md) was an hundred and seven and twenty years old: these were the [šānâ](../../strongs/h/h8141.md) of the [chay](../../strongs/h/h2416.md) of [Śārâ](../../strongs/h/h8283.md).

<a name="genesis_23_2"></a>Genesis 23:2

And [Śārâ](../../strongs/h/h8283.md) [muwth](../../strongs/h/h4191.md) in [qiryaṯ 'arbaʿ](../../strongs/h/h7153.md); the same is [Ḥeḇrôn](../../strongs/h/h2275.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md): and ['Abraham](../../strongs/h/h85.md) [bow'](../../strongs/h/h935.md) to [sāp̄aḏ](../../strongs/h/h5594.md) for [Śārâ](../../strongs/h/h8283.md), and to [bāḵâ](../../strongs/h/h1058.md) for her.

<a name="genesis_23_3"></a>Genesis 23:3

And ['Abraham](../../strongs/h/h85.md) [quwm](../../strongs/h/h6965.md) from [paniym](../../strongs/h/h6440.md) his [muwth](../../strongs/h/h4191.md), and [dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Ḥēṯ](../../strongs/h/h2845.md), ['āmar](../../strongs/h/h559.md),

<a name="genesis_23_4"></a>Genesis 23:4

I am a [ger](../../strongs/h/h1616.md) and a [tôšāḇ](../../strongs/h/h8453.md) with you: [nathan](../../strongs/h/h5414.md) me an ['achuzzah](../../strongs/h/h272.md) of a [qeber](../../strongs/h/h6913.md) with you, that I may [qāḇar](../../strongs/h/h6912.md) my [muwth](../../strongs/h/h4191.md) out of my [paniym](../../strongs/h/h6440.md).

<a name="genesis_23_5"></a>Genesis 23:5

And the [ben](../../strongs/h/h1121.md) of [Ḥēṯ](../../strongs/h/h2845.md) ['anah](../../strongs/h/h6030.md) ['Abraham](../../strongs/h/h85.md), ['āmar](../../strongs/h/h559.md) unto him,

<a name="genesis_23_6"></a>Genesis 23:6

[shama'](../../strongs/h/h8085.md) us, my ['adown](../../strongs/h/h113.md): thou art an ['Elohiym](../../strongs/h/h430.md) [nāśî'](../../strongs/h/h5387.md) [tavek](../../strongs/h/h8432.md): in the [miḇḥār](../../strongs/h/h4005.md) of our [qeber](../../strongs/h/h6913.md) [qāḇar](../../strongs/h/h6912.md) thy [muwth](../../strongs/h/h4191.md); [lō'](../../strongs/h/h3808.md) ['iysh](../../strongs/h/h376.md) of us shall [kālā'](../../strongs/h/h3607.md) from thee his [qeber](../../strongs/h/h6913.md), but that thou mayest [qāḇar](../../strongs/h/h6912.md) thy [muwth](../../strongs/h/h4191.md).

<a name="genesis_23_7"></a>Genesis 23:7

And ['Abraham](../../strongs/h/h85.md) [quwm](../../strongs/h/h6965.md), and [shachah](../../strongs/h/h7812.md) himself to the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), even to the [ben](../../strongs/h/h1121.md) of [Ḥēṯ](../../strongs/h/h2845.md).

<a name="genesis_23_8"></a>Genesis 23:8

And he [dabar](../../strongs/h/h1696.md) with them, ['āmar](../../strongs/h/h559.md), If it be your [nephesh](../../strongs/h/h5315.md) that I should [qāḇar](../../strongs/h/h6912.md) my [muwth](../../strongs/h/h4191.md) out of my [paniym](../../strongs/h/h6440.md); [shama'](../../strongs/h/h8085.md) me, and [pāḡaʿ](../../strongs/h/h6293.md) for me to [ʿep̄rôn](../../strongs/h/h6085.md) the [ben](../../strongs/h/h1121.md) of [Ṣōḥar](../../strongs/h/h6714.md),

<a name="genesis_23_9"></a>Genesis 23:9

That he may [nathan](../../strongs/h/h5414.md) me the [mᵊʿārâ](../../strongs/h/h4631.md) of [maḵpēlâ](../../strongs/h/h4375.md), which he hath, which is in the [qāṣê](../../strongs/h/h7097.md) of his [sadeh](../../strongs/h/h7704.md); for as [mālē'](../../strongs/h/h4392.md) [keceph](../../strongs/h/h3701.md) as it is [mālē'](../../strongs/h/h4392.md) he shall [nathan](../../strongs/h/h5414.md) it me for an ['achuzzah](../../strongs/h/h272.md) of a [qeber](../../strongs/h/h6913.md) [tavek](../../strongs/h/h8432.md) you.

<a name="genesis_23_10"></a>Genesis 23:10

And [ʿep̄rôn](../../strongs/h/h6085.md) [yashab](../../strongs/h/h3427.md) [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Ḥēṯ](../../strongs/h/h2845.md): and [ʿep̄rôn](../../strongs/h/h6085.md) the [Ḥitî](../../strongs/h/h2850.md) ['anah](../../strongs/h/h6030.md) ['Abraham](../../strongs/h/h85.md) in the ['ozen](../../strongs/h/h241.md) of the [ben](../../strongs/h/h1121.md) of [Ḥēṯ](../../strongs/h/h2845.md), even of all that [bow'](../../strongs/h/h935.md) at the [sha'ar](../../strongs/h/h8179.md) of his [ʿîr](../../strongs/h/h5892.md), ['āmar](../../strongs/h/h559.md),

<a name="genesis_23_11"></a>Genesis 23:11

[lō'](../../strongs/h/h3808.md), my ['adown](../../strongs/h/h113.md), [shama'](../../strongs/h/h8085.md) me: the [sadeh](../../strongs/h/h7704.md) [nathan](../../strongs/h/h5414.md) I thee, and the [mᵊʿārâ](../../strongs/h/h4631.md) that is therein, I [nathan](../../strongs/h/h5414.md) it thee; in the ['ayin](../../strongs/h/h5869.md) of the [ben](../../strongs/h/h1121.md) of my ['am](../../strongs/h/h5971.md) [nathan](../../strongs/h/h5414.md) I it thee: [qāḇar](../../strongs/h/h6912.md) thy [muwth](../../strongs/h/h4191.md).

<a name="genesis_23_12"></a>Genesis 23:12

And ['Abraham](../../strongs/h/h85.md) [shachah](../../strongs/h/h7812.md) himself [paniym](../../strongs/h/h6440.md) the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md).

<a name="genesis_23_13"></a>Genesis 23:13

And he [dabar](../../strongs/h/h1696.md) unto [ʿep̄rôn](../../strongs/h/h6085.md) in the ['ozen](../../strongs/h/h241.md) of the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), ['āmar](../../strongs/h/h559.md), But if thou wilt give it, I pray thee, [shama'](../../strongs/h/h8085.md) me: I will [nathan](../../strongs/h/h5414.md) thee [keceph](../../strongs/h/h3701.md) for the [sadeh](../../strongs/h/h7704.md); [laqach](../../strongs/h/h3947.md) it of me, and I will [qāḇar](../../strongs/h/h6912.md) my [muwth](../../strongs/h/h4191.md) there.

<a name="genesis_23_14"></a>Genesis 23:14

And [ʿep̄rôn](../../strongs/h/h6085.md) ['anah](../../strongs/h/h6030.md) ['Abraham](../../strongs/h/h85.md), ['āmar](../../strongs/h/h559.md) unto him,

<a name="genesis_23_15"></a>Genesis 23:15

My ['adown](../../strongs/h/h113.md), [shama'](../../strongs/h/h8085.md) unto me: the ['erets](../../strongs/h/h776.md) is worth four hundred [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md); what is that [bayin](../../strongs/h/h996.md) me and thee? [qāḇar](../../strongs/h/h6912.md) therefore thy [muwth](../../strongs/h/h4191.md).

<a name="genesis_23_16"></a>Genesis 23:16

And ['Abraham](../../strongs/h/h85.md) [shama'](../../strongs/h/h8085.md) unto [ʿep̄rôn](../../strongs/h/h6085.md); and ['Abraham](../../strongs/h/h85.md) [šāqal](../../strongs/h/h8254.md) to [ʿep̄rôn](../../strongs/h/h6085.md) the [keceph](../../strongs/h/h3701.md), which he had [dabar](../../strongs/h/h1696.md) in the ['ozen](../../strongs/h/h241.md) of the [ben](../../strongs/h/h1121.md) of [Ḥēṯ](../../strongs/h/h2845.md), four hundred [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md), ['abar](../../strongs/h/h5674.md) with the [sāḥar](../../strongs/h/h5503.md).

<a name="genesis_23_17"></a>Genesis 23:17

And the [sadeh](../../strongs/h/h7704.md) of [ʿep̄rôn](../../strongs/h/h6085.md) which was in [maḵpēlâ](../../strongs/h/h4375.md), which was [paniym](../../strongs/h/h6440.md) [mamrē'](../../strongs/h/h4471.md), the [sadeh](../../strongs/h/h7704.md), and the [mᵊʿārâ](../../strongs/h/h4631.md) which was therein, and all the ['ets](../../strongs/h/h6086.md) that were in the [sadeh](../../strongs/h/h7704.md), that were in all the [gᵊḇûl](../../strongs/h/h1366.md) [cabiyb](../../strongs/h/h5439.md), were [quwm](../../strongs/h/h6965.md)

<a name="genesis_23_18"></a>Genesis 23:18

Unto ['Abraham](../../strongs/h/h85.md) for a [miqnâ](../../strongs/h/h4736.md) in the ['ayin](../../strongs/h/h5869.md) of the [ben](../../strongs/h/h1121.md) of [Ḥēṯ](../../strongs/h/h2845.md), before all that [bow'](../../strongs/h/h935.md) at the [sha'ar](../../strongs/h/h8179.md) of his [ʿîr](../../strongs/h/h5892.md).

<a name="genesis_23_19"></a>Genesis 23:19

And ['aḥar](../../strongs/h/h310.md) this, ['Abraham](../../strongs/h/h85.md) [qāḇar](../../strongs/h/h6912.md) [Śārâ](../../strongs/h/h8283.md) his ['ishshah](../../strongs/h/h802.md) in the [mᵊʿārâ](../../strongs/h/h4631.md) of the [sadeh](../../strongs/h/h7704.md) of [maḵpēlâ](../../strongs/h/h4375.md) [paniym](../../strongs/h/h6440.md) [mamrē'](../../strongs/h/h4471.md): the same is [Ḥeḇrôn](../../strongs/h/h2275.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md).

<a name="genesis_23_20"></a>Genesis 23:20

And the [sadeh](../../strongs/h/h7704.md), and the [mᵊʿārâ](../../strongs/h/h4631.md) that is therein, were [quwm](../../strongs/h/h6965.md) unto ['Abraham](../../strongs/h/h85.md) for an ['achuzzah](../../strongs/h/h272.md) of a [qeber](../../strongs/h/h6913.md) by the [ben](../../strongs/h/h1121.md) of [Ḥēṯ](../../strongs/h/h2845.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 22](genesis_22.md) - [Genesis 24](genesis_24.md)