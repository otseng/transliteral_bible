# [Genesis 7](https://www.blueletterbible.org/lxx/genesis/7)

<a name="genesis_7_1"></a>Genesis 7:1

And [eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) to [Nōe](../../strongs/g/g3575.md), [eiserchomai](../../strongs/g/g1525.md), you and all your [oikos](../../strongs/g/g3624.md) into the [kibōtos](../../strongs/g/g2787.md)! for I [horaō](../../strongs/g/g3708.md) you as [dikaios](../../strongs/g/g1342.md) [enantion](../../strongs/g/g1726.md) me among this [genea](../../strongs/g/g1074.md). 

<a name="genesis_7_2"></a>Genesis 7:2

And of the [ktēnos](../../strongs/g/g2934.md) [katharos](../../strongs/g/g2513.md), [eisagō](../../strongs/g/g1521.md) for yourself [hepta](../../strongs/g/g2033.md) by [hepta](../../strongs/g/g2033.md), [arrēn](../../strongs/g/g730.md) and [thēlys](../../strongs/g/g2338.md)! but from the [ktēnos](../../strongs/g/g2934.md) of the ones not [katharos](../../strongs/g/g2513.md), [dyo](../../strongs/g/g1417.md) by [dyo](../../strongs/g/g1417.md), [arrēn](../../strongs/g/g730.md) and [thēlys](../../strongs/g/g2338.md). 

<a name="genesis_7_3"></a>Genesis 7:3

And from the [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md), of the [katharos](../../strongs/g/g2513.md), [hepta](../../strongs/g/g2033.md) by [hepta](../../strongs/g/g2033.md), [arrēn](../../strongs/g/g730.md) and [thēlys](../../strongs/g/g2338.md); and from all the [peteinon](../../strongs/g/g4071.md) of the ones not [katharos](../../strongs/g/g2513.md), [dyo](../../strongs/g/g1417.md) by [dyo](../../strongs/g/g1417.md), [arrēn](../../strongs/g/g730.md) and [thēlys](../../strongs/g/g2338.md), to maintain (diathrepsai) [sperma](../../strongs/g/g4690.md) upon all the [gē](../../strongs/g/g1093.md). 

<a name="genesis_7_4"></a>Genesis 7:4

For yet [hēmera](../../strongs/g/g2250.md) [hepta](../../strongs/g/g2033.md) I will [epagō](../../strongs/g/g1863.md) [hyetos](../../strongs/g/g5205.md) upon the [gē](../../strongs/g/g1093.md) [tessarakonta](../../strongs/g/g5062.md) [hēmera](../../strongs/g/g2250.md) and [tessarakonta](../../strongs/g/g5062.md) [nyx](../../strongs/g/g3571.md). And I will [exaleiphō](../../strongs/g/g1813.md) [pas](../../strongs/g/g3956.md) [exanastasis](../../strongs/g/g1815.md) which I [poieō](../../strongs/g/g4160.md) from the [prosōpon](../../strongs/g/g4383.md) of all the [gē](../../strongs/g/g1093.md). 

<a name="genesis_7_5"></a>Genesis 7:5

And [Nōe](../../strongs/g/g3575.md) [poieō](../../strongs/g/g4160.md) all as much as [entellō](../../strongs/g/g1781.md) to him the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md). 

<a name="genesis_7_6"></a>Genesis 7:6

And [Nōe](../../strongs/g/g3575.md) was [etos](../../strongs/g/g2094.md) [hexakosioi](../../strongs/g/g1812.md), and the [kataklysmos](../../strongs/g/g2627.md) of the [hydōr](../../strongs/g/g5204.md) [ginomai](../../strongs/g/g1096.md) upon the [gē](../../strongs/g/g1093.md). 

<a name="genesis_7_7"></a>Genesis 7:7

[eiserchomai](../../strongs/g/g1525.md) [Nōe](../../strongs/g/g3575.md), and his [huios](../../strongs/g/g5207.md), and his [gynē](../../strongs/g/g1135.md), and the [gynē](../../strongs/g/g1135.md) of his [huios](../../strongs/g/g5207.md) with him, into the [kibōtos](../../strongs/g/g2787.md), because of the [hydōr](../../strongs/g/g5204.md) of the [kataklysmos](../../strongs/g/g2627.md). 

<a name="genesis_7_8"></a>Genesis 7:8

And from the [peteinon](../../strongs/g/g4071.md) of the [katharos](../../strongs/g/g2513.md), and from the [peteinon](../../strongs/g/g4071.md) of the ones not [katharos](../../strongs/g/g2513.md), and from the [ktēnos](../../strongs/g/g2934.md) of the ones [katharos](../../strongs/g/g2513.md), and from the [ktēnos](../../strongs/g/g2934.md) of the ones not [katharos](../../strongs/g/g2513.md), and from the [thērion](../../strongs/g/g2342.md), and from all of the [herpeton](../../strongs/g/g2062.md) upon the [gē](../../strongs/g/g1093.md), 

<a name="genesis_7_9"></a>Genesis 7:9

[dyo](../../strongs/g/g1417.md) by [dyo](../../strongs/g/g1417.md) they [eiserchomai](../../strongs/g/g1525.md) with [Nōe](../../strongs/g/g3575.md) into the [kibōtos](../../strongs/g/g2787.md), [arrēn](../../strongs/g/g730.md) and [thēlys](../../strongs/g/g2338.md), as [entellō](../../strongs/g/g1781.md) to him [theos](../../strongs/g/g2316.md). 

<a name="genesis_7_10"></a>Genesis 7:10

And [ginomai](../../strongs/g/g1096.md) after the [hepta](../../strongs/g/g2033.md) [hēmera](../../strongs/g/g2250.md), and the [hydōr](../../strongs/g/g5204.md) of the [kataklysmos](../../strongs/g/g2627.md) [ginomai](../../strongs/g/g1096.md) upon the [gē](../../strongs/g/g1093.md). 

<a name="genesis_7_11"></a>Genesis 7:11

In the [hexakosioi](../../strongs/g/g1812.md) [etos](../../strongs/g/g2094.md) in the [zōē](../../strongs/g/g2222.md) of [Nōe](../../strongs/g/g3575.md), the [deuteros](../../strongs/g/g1208.md) [mēn](../../strongs/g/g3376.md), [ebdomos](../../strongs/g/g1442.md) and twentieth (eikadi) of the [mēn](../../strongs/g/g3376.md), in [sēmeron](../../strongs/g/g4594.md), [rhēgnymi](../../strongs/g/g4486.md) all the [pēgē](../../strongs/g/g4077.md) of the [abyssos](../../strongs/g/g12.md), and the torrents (katarraktai) of the [ouranos](../../strongs/g/g3772.md) were [anoigō](../../strongs/g/g455.md). 

<a name="genesis_7_12"></a>Genesis 7:12

And [ginomai](../../strongs/g/g1096.md) [hyetos](../../strongs/g/g5205.md) upon the [gē](../../strongs/g/g1093.md) [tessarakonta](../../strongs/g/g5062.md) [hēmera](../../strongs/g/g2250.md) and [tessarakonta](../../strongs/g/g5062.md) [nyx](../../strongs/g/g3571.md). 

<a name="genesis_7_13"></a>Genesis 7:13

In this [hēmera](../../strongs/g/g2250.md) [eiserchomai](../../strongs/g/g1525.md) [Nōe](../../strongs/g/g3575.md), [Sēm](../../strongs/g/g4590.md), Ham, Japheth, the [huios](../../strongs/g/g5207.md) of [Nōe](../../strongs/g/g3575.md), and the [gynē](../../strongs/g/g1135.md) of [Nōe](../../strongs/g/g3575.md), and the [treis](../../strongs/g/g5140.md) [gynē](../../strongs/g/g1135.md) of his [huios](../../strongs/g/g5207.md) with him into the [kibōtos](../../strongs/g/g2787.md). 

<a name="genesis_7_14"></a>Genesis 7:14

And all the [thērion](../../strongs/g/g2342.md) according to [genos](../../strongs/g/g1085.md), and all the [ktēnos](../../strongs/g/g2934.md) according to [genos](../../strongs/g/g1085.md), and every [herpeton](../../strongs/g/g2062.md) [kineō](../../strongs/g/g2795.md) upon the [gē](../../strongs/g/g1093.md), according to [genos](../../strongs/g/g1085.md), and every [peteinon](../../strongs/g/g4071.md) according to [genos](../../strongs/g/g1085.md), 

<a name="genesis_7_15"></a>Genesis 7:15

they [eiserchomai](../../strongs/g/g1525.md) with [Nōe](../../strongs/g/g3575.md), into the [kibōtos](../../strongs/g/g2787.md), [dyo](../../strongs/g/g1417.md) by [dyo](../../strongs/g/g1417.md), [arrēn](../../strongs/g/g730.md) and [thēlys](../../strongs/g/g2338.md), from all [sarx](../../strongs/g/g4561.md) in which there is a [pneuma](../../strongs/g/g4151.md) of [zōē](../../strongs/g/g2222.md). 

<a name="genesis_7_16"></a>Genesis 7:16

And the ones [eisporeuomai](../../strongs/g/g1531.md), [arrēn](../../strongs/g/g730.md) and [thēlys](../../strongs/g/g2338.md), from all [sarx](../../strongs/g/g4561.md), [eiserchomai](../../strongs/g/g1525.md) as [theos](../../strongs/g/g2316.md) [entellō](../../strongs/g/g1781.md) to [Nōe](../../strongs/g/g3575.md). And [kleiō](../../strongs/g/g2808.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) the [kibōtos](../../strongs/g/g2787.md) from [exōthen](../../strongs/g/g1855.md) of it. 

<a name="genesis_7_17"></a>Genesis 7:17

And [ginomai](../../strongs/g/g1096.md) the [kataklysmos](../../strongs/g/g2627.md) [tessarakonta](../../strongs/g/g5062.md) [hēmera](../../strongs/g/g2250.md) and [tessarakonta](../../strongs/g/g5062.md) [nyx](../../strongs/g/g3571.md). And [plēthynō](../../strongs/g/g4129.md) the [hydōr](../../strongs/g/g5204.md) and [epairō](../../strongs/g/g1869.md) the [kibōtos](../../strongs/g/g2787.md), and [hypsoō](../../strongs/g/g5312.md) it from the [gē](../../strongs/g/g1093.md). 

<a name="genesis_7_18"></a>Genesis 7:18

And prevailed (epekratei) the [hydōr](../../strongs/g/g5204.md) and [plēthynō](../../strongs/g/g4129.md) [sphodra](../../strongs/g/g4970.md) upon the [gē](../../strongs/g/g1093.md). And was [epipherō](../../strongs/g/g2018.md) the [kibōtos](../../strongs/g/g2787.md) upon the [hydōr](../../strongs/g/g5204.md). 

<a name="genesis_7_19"></a>Genesis 7:19

But the [hydōr](../../strongs/g/g5204.md) prevailed (epekratei) [sphodra](../../strongs/g/g4970.md) [sphodrōs](../../strongs/g/g4971.md) upon the [gē](../../strongs/g/g1093.md), and [kalyptō](../../strongs/g/g2572.md) all the [oros](../../strongs/g/g3735.md) [hypsēlos](../../strongs/g/g5308.md) which were [hypokatō](../../strongs/g/g5270.md) the [ouranos](../../strongs/g/g3772.md). 

<a name="genesis_7_20"></a>Genesis 7:20

[deka](../../strongs/g/g1176.md) [pente](../../strongs/g/g4002.md) [pēchys](../../strongs/g/g4083.md) above was [hypsoō](../../strongs/g/g5312.md) the [hydōr](../../strongs/g/g5204.md), and it [epikalyptō](../../strongs/g/g1943.md) all the [oros](../../strongs/g/g3735.md) [hypsēlos](../../strongs/g/g5308.md). 

<a name="genesis_7_21"></a>Genesis 7:21

And there [apothnēskō](../../strongs/g/g599.md) all [sarx](../../strongs/g/g4561.md) [kineō](../../strongs/g/g2795.md) upon the [gē](../../strongs/g/g1093.md) of the [peteinon](../../strongs/g/g4071.md), and of the [ktēnos](../../strongs/g/g2934.md), and from [thērion](../../strongs/g/g2342.md), and every [herpeton](../../strongs/g/g2062.md) [kineō](../../strongs/g/g2795.md) upon the [gē](../../strongs/g/g1093.md), and every [anthrōpos](../../strongs/g/g444.md), 

<a name="genesis_7_22"></a>Genesis 7:22

and all as much as have the [pnoē](../../strongs/g/g4157.md) of [zōē](../../strongs/g/g2222.md), and all which was upon the [xēros](../../strongs/g/g3584.md) [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_7_23"></a>Genesis 7:23

And he [exaleiphō](../../strongs/g/g1813.md) every height (anastēma) which was upon the [prosōpon](../../strongs/g/g4383.md) of all the [gē](../../strongs/g/g1093.md), from [anthrōpos](../../strongs/g/g444.md) unto [ktēnos](../../strongs/g/g2934.md), and [herpeton](../../strongs/g/g2062.md), and the [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md); and they were wiped [exaleiphō](../../strongs/g/g1813.md) from the [gē](../../strongs/g/g1093.md); and he [kataleipō](../../strongs/g/g2641.md) only [Nōe](../../strongs/g/g3575.md) and the ones with him in the [kibōtos](../../strongs/g/g2787.md). 

<a name="genesis_7_24"></a>Genesis 7:24

And was [hypsoō](../../strongs/g/g5312.md) the [hydōr](../../strongs/g/g5204.md) from the [gē](../../strongs/g/g1093.md) [hēmera](../../strongs/g/g2250.md) a [hekaton](../../strongs/g/g1540.md) [pentēkonta](../../strongs/g/g4004.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 6](genesis_lxx_6.md) - [Genesis 8](genesis_lxx_8.md)