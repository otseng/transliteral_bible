# [Genesis 1](https://www.blueletterbible.org/lxx/genesis/1)

<a name="genesis_1_1"></a>Genesis 1:1

In the [archē](../../strongs/g/g746.md) [theos](../../strongs/g/g2316.md) [poieō](../../strongs/g/g4160.md) the [ouranos](../../strongs/g/g3772.md) and the [gē](../../strongs/g/g1093.md). 

<a name="genesis_1_2"></a>Genesis 1:2

But the [gē](../../strongs/g/g1093.md) was [aoratos](../../strongs/g/g517.md) and unready (akataskeuastos), and [skotos](../../strongs/g/g4655.md) was upon the [abyssos](../../strongs/g/g12.md), and [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md) [epipherō](../../strongs/g/g2018.md) upon the [hydōr](../../strongs/g/g5204.md). 

<a name="genesis_1_3"></a>Genesis 1:3

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md), Let there be [phōs](../../strongs/g/g5457.md)! And there was [phōs](../../strongs/g/g5457.md). 

<a name="genesis_1_4"></a>Genesis 1:4

And [theos](../../strongs/g/g2316.md) [horaō](../../strongs/g/g3708.md) the [phōs](../../strongs/g/g5457.md) that it was [kalos](../../strongs/g/g2570.md). And [theos](../../strongs/g/g2316.md) [diachōrizō](../../strongs/g/g1316.md) the [phōs](../../strongs/g/g5457.md) and between the [skotos](../../strongs/g/g4655.md). 

<a name="genesis_1_5"></a>Genesis 1:5

And [theos](../../strongs/g/g2316.md) [kaleō](../../strongs/g/g2564.md) the [phōs](../../strongs/g/g5457.md), [hēmera](../../strongs/g/g2250.md), and the [skotos](../../strongs/g/g4655.md) he [kaleō](../../strongs/g/g2564.md), [nyx](../../strongs/g/g3571.md); and there was [hespera](../../strongs/g/g2073.md) and there was [prōï](../../strongs/g/g4404.md), [hēmera](../../strongs/g/g2250.md) [heis](../../strongs/g/g1520.md). 

<a name="genesis_1_6"></a>Genesis 1:6

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md), Let there be a [stereōma](../../strongs/g/g4733.md) in the [mesos](../../strongs/g/g3319.md) of the [hydōr](../../strongs/g/g5204.md), and let it be for [diachōrizō](../../strongs/g/g1316.md) between [hydōr](../../strongs/g/g5204.md) and [hydōr](../../strongs/g/g5204.md)! 

<a name="genesis_1_7"></a>Genesis 1:7

And [theos](../../strongs/g/g2316.md) [poieō](../../strongs/g/g4160.md) the [stereōma](../../strongs/g/g4733.md), and [theos](../../strongs/g/g2316.md) [diachōrizō](../../strongs/g/g1316.md) the [hydōr](../../strongs/g/g5204.md) which was [hypokatō](../../strongs/g/g5270.md) the [stereōma](../../strongs/g/g4733.md), and [mesos](../../strongs/g/g3319.md) the [hydōr](../../strongs/g/g5204.md) [epanō](../../strongs/g/g1883.md) the [stereōma](../../strongs/g/g4733.md). 

<a name="genesis_1_8"></a>Genesis 1:8

And [theos](../../strongs/g/g2316.md) [kaleō](../../strongs/g/g2564.md) the [stereōma](../../strongs/g/g4733.md), [ouranos](../../strongs/g/g3772.md). And [theos](../../strongs/g/g2316.md) [horaō](../../strongs/g/g3708.md) that it was [kalos](../../strongs/g/g2570.md); and there was [hespera](../../strongs/g/g2073.md) and there was [prōï](../../strongs/g/g4404.md), [hēmera](../../strongs/g/g2250.md) the [deuteros](../../strongs/g/g1208.md). 

<a name="genesis_1_9"></a>Genesis 1:9

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md), Let [synagō](../../strongs/g/g4863.md) the [hydōr](../../strongs/g/g5204.md) [hypokatō](../../strongs/g/g5270.md) the [ouranos](../../strongs/g/g3772.md) into [synagōgē](../../strongs/g/g4864.md) [heis](../../strongs/g/g1520.md), and let [horaō](../../strongs/g/g3708.md) the [xēros](../../strongs/g/g3584.md)! And it was so. And [synagō](../../strongs/g/g4863.md) the [hydōr](../../strongs/g/g5204.md) [hypokatō](../../strongs/g/g5270.md) the [ouranos](../../strongs/g/g3772.md) into their [synagōgē](../../strongs/g/g4864.md), and [horaō](../../strongs/g/g3708.md) the [xēros](../../strongs/g/g3584.md). 

<a name="genesis_1_10"></a>Genesis 1:10

And [theos](../../strongs/g/g2316.md) [kaleō](../../strongs/g/g2564.md) the [xēros](../../strongs/g/g3584.md), [gē](../../strongs/g/g1093.md); and the collections (systema) of the [hydōr](../../strongs/g/g5204.md) he [kaleō](../../strongs/g/g2564.md), [thalassa](../../strongs/g/g2281.md). And [theos](../../strongs/g/g2316.md) beheld that it was [kalos](../../strongs/g/g2570.md). 

<a name="genesis_1_11"></a>Genesis 1:11

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md), Let [blastanō](../../strongs/g/g985.md) the [gē](../../strongs/g/g1093.md) [botanē](../../strongs/g/g1008.md) of [chortos](../../strongs/g/g5528.md) [speirō](../../strongs/g/g4687.md) [sperma](../../strongs/g/g4690.md) according to [genos](../../strongs/g/g1085.md), and according to [homoiotēs](../../strongs/g/g3665.md), and [xylon](../../strongs/g/g3586.md) [poieō](../../strongs/g/g4160.md) [karpos](../../strongs/g/g2590.md) which the [sperma](../../strongs/g/g4690.md) of it is in it, according to [genos](../../strongs/g/g1085.md) upon the [gē](../../strongs/g/g1093.md)! And it was so. 

<a name="genesis_1_12"></a>Genesis 1:12

And [ekpherō](../../strongs/g/g1627.md) the [gē](../../strongs/g/g1093.md) [botanē](../../strongs/g/g1008.md) of [chortos](../../strongs/g/g5528.md) [speirō](../../strongs/g/g4687.md) [sperma](../../strongs/g/g4690.md), according to [genos](../../strongs/g/g1085.md), and according to [homoiotēs](../../strongs/g/g3665.md); and [xylon](../../strongs/g/g3586.md) [poieō](../../strongs/g/g4160.md) [karpos](../../strongs/g/g2590.md) which the [sperma](../../strongs/g/g4690.md) of it is in it, according to [genos](../../strongs/g/g1085.md) upon the [gē](../../strongs/g/g1093.md). 

<a name="genesis_1_13"></a>Genesis 1:13

And [theos](../../strongs/g/g2316.md) beheld that it was [kalos](../../strongs/g/g2570.md). And there was [hespera](../../strongs/g/g2073.md) and there was [prōï](../../strongs/g/g4404.md), [hēmera](../../strongs/g/g2250.md) the [tritos](../../strongs/g/g5154.md). 

<a name="genesis_1_14"></a>Genesis 1:14

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md), Let there be [phōstēr](../../strongs/g/g5458.md) in the [stereōma](../../strongs/g/g4733.md) of the [ouranos](../../strongs/g/g3772.md) for giving light (phausin) upon the [gē](../../strongs/g/g1093.md), to [diachōrizō](../../strongs/g/g1316.md) between the [hēmera](../../strongs/g/g2250.md) and between the [nyx](../../strongs/g/g3571.md)! And let them be for [sēmeion](../../strongs/g/g4592.md), and for [kairos](../../strongs/g/g2540.md), and for [hēmera](../../strongs/g/g2250.md), and for [eniautos](../../strongs/g/g1763.md)! 

<a name="genesis_1_15"></a>Genesis 1:15

And let them be for giving light (phausin) in the [stereōma](../../strongs/g/g4733.md) of the [ouranos](../../strongs/g/g3772.md), so as to [phainō](../../strongs/g/g5316.md) upon the [gē](../../strongs/g/g1093.md)! And it was so. 

<a name="genesis_1_16"></a>Genesis 1:16

And [theos](../../strongs/g/g2316.md) made the [dyo](../../strongs/g/g1417.md) [phōstēr](../../strongs/g/g5458.md) [megas](../../strongs/g/g3173.md); the [phōstēr](../../strongs/g/g5458.md) [megas](../../strongs/g/g3173.md) for [archē](../../strongs/g/g746.md) of the [hēmera](../../strongs/g/g2250.md), and the[phōstēr](../../strongs/g/g5458.md) [elassōn](../../strongs/g/g1640.md) for [archē](../../strongs/g/g746.md) of the [nyx](../../strongs/g/g3571.md), and the [astēr](../../strongs/g/g792.md). 

<a name="genesis_1_17"></a>Genesis 1:17

And [tithēmi](../../strongs/g/g5087.md) them [theos](../../strongs/g/g2316.md) in the [stereōma](../../strongs/g/g4733.md) of the [ouranos](../../strongs/g/g3772.md), so as to [phainō](../../strongs/g/g5316.md) upon the [gē](../../strongs/g/g1093.md), 

<a name="genesis_1_18"></a>Genesis 1:18

and to [archomai](../../strongs/g/g756.md) the [hēmera](../../strongs/g/g2250.md) and the [nyx](../../strongs/g/g3571.md), and to [diachōrizō](../../strongs/g/g1316.md) between the [phōs](../../strongs/g/g5457.md) and between the [skotos](../../strongs/g/g4655.md). And [theos](../../strongs/g/g2316.md) beheld that it was [kalos](../../strongs/g/g2570.md). 

<a name="genesis_1_19"></a>Genesis 1:19

And there was [hespera](../../strongs/g/g2073.md) and there was [prōï](../../strongs/g/g4404.md), [hēmera](../../strongs/g/g2250.md) the [tetartos](../../strongs/g/g5067.md). 

<a name="genesis_1_20"></a>Genesis 1:20

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md), Let [exagō](../../strongs/g/g1806.md) the [hydōr](../../strongs/g/g5204.md) [herpeton](../../strongs/g/g2062.md) [psychē](../../strongs/g/g5590.md) of [zaō](../../strongs/g/g2198.md), and [peteinon](../../strongs/g/g4071.md) [petomai](../../strongs/g/g4072.md) upon the [gē](../../strongs/g/g1093.md) below the [stereōma](../../strongs/g/g4733.md) of the [ouranos](../../strongs/g/g3772.md)! And it was so. 

<a name="genesis_1_21"></a>Genesis 1:21

And [theos](../../strongs/g/g2316.md) [poieō](../../strongs/g/g4160.md) the [kētos](../../strongs/g/g2785.md) [megas](../../strongs/g/g3173.md), and every [psychē](../../strongs/g/g5590.md) of [zōon](../../strongs/g/g2226.md) of [herpeton](../../strongs/g/g2062.md) which [exagō](../../strongs/g/g1806.md) the [hydōr](../../strongs/g/g5204.md) according to their [genos](../../strongs/g/g1085.md); and every [peteinon](../../strongs/g/g4071.md) according to [genos](../../strongs/g/g1085.md). And [theos](../../strongs/g/g2316.md) beheld that it was [kalos](../../strongs/g/g2570.md). 

<a name="genesis_1_22"></a>Genesis 1:22

And [eulogeō](../../strongs/g/g2127.md) them [theos](../../strongs/g/g2316.md), [legō](../../strongs/g/g3004.md), [auxanō](../../strongs/g/g837.md) and [plēthynō](../../strongs/g/g4129.md), and [plēroō](../../strongs/g/g4137.md) the [hydōr](../../strongs/g/g5204.md) in the [thalassa](../../strongs/g/g2281.md)! And let [peteinon](../../strongs/g/g4071.md) be [plēthynō](../../strongs/g/g4129.md) upon the [gē](../../strongs/g/g1093.md)! 

<a name="genesis_1_23"></a>Genesis 1:23

And there was [hespera](../../strongs/g/g2073.md) and there was [prōï](../../strongs/g/g4404.md), [hēmera](../../strongs/g/g2250.md) the [pemptos](../../strongs/g/g3991.md). 

<a name="genesis_1_24"></a>Genesis 1:24

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md), Let [exagō](../../strongs/g/g1806.md) the [gē](../../strongs/g/g1093.md) [psychē](../../strongs/g/g5590.md) [zaō](../../strongs/g/g2198.md) according to its type -- [genos](../../strongs/g/g1085.md) [tetrapous](../../strongs/g/g5074.md), and [herpeton](../../strongs/g/g2062.md), and [thērion](../../strongs/g/g2342.md) of the [gē](../../strongs/g/g1093.md) according to [genos](../../strongs/g/g1085.md)! And it was so. 

<a name="genesis_1_25"></a>Genesis 1:25

And [theos](../../strongs/g/g2316.md) made the [thērion](../../strongs/g/g2342.md) of the [gē](../../strongs/g/g1093.md) according to [genos](../../strongs/g/g1085.md), and the [ktēnos](../../strongs/g/g2934.md) according to their [genos](../../strongs/g/g1085.md), and all the [herpeton](../../strongs/g/g2062.md) of the [gē](../../strongs/g/g1093.md) according to [genos](../../strongs/g/g1085.md). And [theos](../../strongs/g/g2316.md) beheld that it was [kalos](../../strongs/g/g2570.md). 

<a name="genesis_1_26"></a>Genesis 1:26

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md), Let us [poieō](../../strongs/g/g4160.md) [anthrōpos](../../strongs/g/g444.md) according to our [eikōn](../../strongs/g/g1504.md), and according to [homoiōsis](../../strongs/g/g3669.md)! And let them [archomai](../../strongs/g/g756.md) the [ichthys](../../strongs/g/g2486.md) of the [thalassa](../../strongs/g/g2281.md), and the [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md), and the [ktēnos](../../strongs/g/g2934.md), and all the [gē](../../strongs/g/g1093.md), and all the [herpeton](../../strongs/g/g2062.md) of the ones crawling (herpontōn) upon the [gē](../../strongs/g/g1093.md)! 

<a name="genesis_1_27"></a>Genesis 1:27

And [theos](../../strongs/g/g2316.md) [poieō](../../strongs/g/g4160.md) [anthrōpos](../../strongs/g/g444.md). According to the [eikōn](../../strongs/g/g1504.md) of [theos](../../strongs/g/g2316.md) he [poieō](../../strongs/g/g4160.md) him. [arrēn](../../strongs/g/g730.md) and [thēlys](../../strongs/g/g2338.md) he [poieō](../../strongs/g/g4160.md) them. 

<a name="genesis_1_28"></a>Genesis 1:28

And [eulogeō](../../strongs/g/g2127.md) them [theos](../../strongs/g/g2316.md), [legō](../../strongs/g/g3004.md), [auxanō](../../strongs/g/g837.md) and [plēthynō](../../strongs/g/g4129.md), and [plēroō](../../strongs/g/g4137.md) the [gē](../../strongs/g/g1093.md), and [katakyrieuō](../../strongs/g/g2634.md) it! And [archomai](../../strongs/g/g756.md) the [ichthys](../../strongs/g/g2486.md) of the [thalassa](../../strongs/g/g2281.md), and the [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md), and all the [ktēnos](../../strongs/g/g2934.md), and all of the [gē](../../strongs/g/g1093.md), and all of the [herpeton](../../strongs/g/g2062.md) of the ones crawling (herpontōn) upon the [gē](../../strongs/g/g1093.md)! 

<a name="genesis_1_29"></a>Genesis 1:29

And [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), I have [didōmi](../../strongs/g/g1325.md) to you every [chortos](../../strongs/g/g5528.md) fit for [sporimos](../../strongs/g/g4702.md) a [speirō](../../strongs/g/g4687.md) of [sperma](../../strongs/g/g4690.md) which is upon the [gē](../../strongs/g/g1093.md), and every [xylon](../../strongs/g/g3586.md) which has in itself a [karpos](../../strongs/g/g2590.md) [sperma](../../strongs/g/g4690.md) fit for [sporimos](../../strongs/g/g4702.md); to you it will be for [brōsis](../../strongs/g/g1035.md), 

<a name="genesis_1_30"></a>Genesis 1:30

and to all the [thērion](../../strongs/g/g2342.md) of the [gē](../../strongs/g/g1093.md), and to all the [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md), and to every [herpeton](../../strongs/g/g2062.md) crawling (herponti) upon the [gē](../../strongs/g/g1093.md), which has in itself [psychē](../../strongs/g/g5590.md) of [zōē](../../strongs/g/g2222.md), even every [chortos](../../strongs/g/g5528.md) [chlōros](../../strongs/g/g5515.md) for [brōsis](../../strongs/g/g1035.md). And it was so. 

<a name="genesis_1_31"></a>Genesis 1:31

And [theos](../../strongs/g/g2316.md) beheld all as much as he [poieō](../../strongs/g/g4160.md). And behold, it [idou](../../strongs/g/g2400.md) [kalos](../../strongs/g/g2570.md) [lian](../../strongs/g/g3029.md). And there was [hespera](../../strongs/g/g2073.md) and there was [prōï](../../strongs/g/g4404.md), [hēmera](../../strongs/g/g2250.md) the [ektos](../../strongs/g/g1623.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 2](genesis_lxx_2.md)