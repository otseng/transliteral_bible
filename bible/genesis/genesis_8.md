# [Genesis 8](https://www.blueletterbible.org/kjv/gen/8/1/s_8001)

<a name="genesis_8_1"></a>Genesis 8:1

And ['Elohiym](../../strongs/h/h430.md) [zakar](../../strongs/h/h2142.md) [Nōaḥ](../../strongs/h/h5146.md), and every [chay](../../strongs/h/h2416.md), and all the [bĕhemah](../../strongs/h/h929.md) that was with him in the [tēḇâ](../../strongs/h/h8392.md): and ['Elohiym](../../strongs/h/h430.md) made a [ruwach](../../strongs/h/h7307.md) to ['abar](../../strongs/h/h5674.md) [ʿal](../../strongs/h/h5921.md) the ['erets](../../strongs/h/h776.md), and the [mayim](../../strongs/h/h4325.md) [šāḵaḵ](../../strongs/h/h7918.md);

<a name="genesis_8_2"></a>Genesis 8:2

The [maʿyān](../../strongs/h/h4599.md) also of the [tĕhowm](../../strongs/h/h8415.md) and the ['ărubâ](../../strongs/h/h699.md) of [shamayim](../../strongs/h/h8064.md) were [sāḵar](../../strongs/h/h5534.md), and the [gešem](../../strongs/h/h1653.md) from [shamayim](../../strongs/h/h8064.md) was [kālā'](../../strongs/h/h3607.md);

<a name="genesis_8_3"></a>Genesis 8:3

And the [mayim](../../strongs/h/h4325.md) [shuwb](../../strongs/h/h7725.md) from off the ['erets](../../strongs/h/h776.md) [halak](../../strongs/h/h1980.md) [shuwb](../../strongs/h/h7725.md): and after the [qāṣê](../../strongs/h/h7097.md) of the hundred and fifty [yowm](../../strongs/h/h3117.md) the [mayim](../../strongs/h/h4325.md) were [ḥāsēr](../../strongs/h/h2637.md).

<a name="genesis_8_4"></a>Genesis 8:4

And the [tēḇâ](../../strongs/h/h8392.md) [nuwach](../../strongs/h/h5117.md) in the seventh [ḥōḏeš](../../strongs/h/h2320.md), on the seventeenth [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md), upon the [har](../../strongs/h/h2022.md) of ['Ărārāṭ](../../strongs/h/h780.md).

<a name="genesis_8_5"></a>Genesis 8:5

And the [mayim](../../strongs/h/h4325.md) [ḥāsēr](../../strongs/h/h2637.md) [halak](../../strongs/h/h1980.md) until the tenth [ḥōḏeš](../../strongs/h/h2320.md): in the tenth, on the first of the [ḥōḏeš](../../strongs/h/h2320.md), were the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md) [ra'ah](../../strongs/h/h7200.md).

<a name="genesis_8_6"></a>Genesis 8:6

And it came to pass at the [qēṣ](../../strongs/h/h7093.md) of forty [yowm](../../strongs/h/h3117.md), that [Nōaḥ](../../strongs/h/h5146.md) [pāṯaḥ](../../strongs/h/h6605.md) the [ḥallôn](../../strongs/h/h2474.md) of the [tēḇâ](../../strongs/h/h8392.md) which he had ['asah](../../strongs/h/h6213.md):

<a name="genesis_8_7"></a>Genesis 8:7

And he [shalach](../../strongs/h/h7971.md) an [ʿōrēḇ](../../strongs/h/h6158.md), which [yāṣā'](../../strongs/h/h3318.md) [yāṣā'](../../strongs/h/h3318.md) and [shuwb](../../strongs/h/h7725.md), until the [mayim](../../strongs/h/h4325.md) were [yāḇēš](../../strongs/h/h3001.md) from off the ['erets](../../strongs/h/h776.md).

<a name="genesis_8_8"></a>Genesis 8:8

Also he [shalach](../../strongs/h/h7971.md) a [yônâ](../../strongs/h/h3123.md) from him, to [ra'ah](../../strongs/h/h7200.md) if the [mayim](../../strongs/h/h4325.md) were [qālal](../../strongs/h/h7043.md) from off the [paniym](../../strongs/h/h6440.md) of the ['adamah](../../strongs/h/h127.md);

<a name="genesis_8_9"></a>Genesis 8:9

But the [yônâ](../../strongs/h/h3123.md) [māṣā'](../../strongs/h/h4672.md) no [mānôaḥ](../../strongs/h/h4494.md) for the [kaph](../../strongs/h/h3709.md) of her [regel](../../strongs/h/h7272.md), and she [shuwb](../../strongs/h/h7725.md) unto him into the [tēḇâ](../../strongs/h/h8392.md), for the [mayim](../../strongs/h/h4325.md) were on the [paniym](../../strongs/h/h6440.md) of the ['erets](../../strongs/h/h776.md): then he [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md), and [laqach](../../strongs/h/h3947.md) her, and [bow'](../../strongs/h/h935.md) her in unto him into the [tēḇâ](../../strongs/h/h8392.md).

<a name="genesis_8_10"></a>Genesis 8:10

And he [chuwl](../../strongs/h/h2342.md) yet ['aḥēr](../../strongs/h/h312.md) [šeḇaʿ](../../strongs/h/h7651.md) [yowm](../../strongs/h/h3117.md); and again he [shalach](../../strongs/h/h7971.md) the [yônâ](../../strongs/h/h3123.md) out of the [tēḇâ](../../strongs/h/h8392.md);

<a name="genesis_8_11"></a>Genesis 8:11

And the [yônâ](../../strongs/h/h3123.md) [bow'](../../strongs/h/h935.md) to him in the [ʿēṯ](../../strongs/h/h6256.md) ['ereb](../../strongs/h/h6153.md); and, lo, in her [peh](../../strongs/h/h6310.md) was a [zayiṯ](../../strongs/h/h2132.md) ['aleh](../../strongs/h/h5929.md) [ṭārāp̄](../../strongs/h/h2965.md): so [Nōaḥ](../../strongs/h/h5146.md) [yada'](../../strongs/h/h3045.md) that the [mayim](../../strongs/h/h4325.md) were [qālal](../../strongs/h/h7043.md) from off the ['erets](../../strongs/h/h776.md).

<a name="genesis_8_12"></a>Genesis 8:12

And he [yāḥal](../../strongs/h/h3176.md) yet ['aḥēr](../../strongs/h/h312.md) [šeḇaʿ](../../strongs/h/h7651.md) [yowm](../../strongs/h/h3117.md); and [shalach](../../strongs/h/h7971.md) the [yônâ](../../strongs/h/h3123.md); which [shuwb](../../strongs/h/h7725.md) not again unto him any more.

<a name="genesis_8_13"></a>Genesis 8:13

And it came to pass in the six hundredth and first [šānâ](../../strongs/h/h8141.md), in the [ri'šôn](../../strongs/h/h7223.md), the ['echad](../../strongs/h/h259.md) of the [ḥōḏeš](../../strongs/h/h2320.md), the [mayim](../../strongs/h/h4325.md) were [ḥāraḇ](../../strongs/h/h2717.md) from off the ['erets](../../strongs/h/h776.md): and [Nōaḥ](../../strongs/h/h5146.md) [cuwr](../../strongs/h/h5493.md) the [miḵsê](../../strongs/h/h4372.md) of the [tēḇâ](../../strongs/h/h8392.md), and [ra'ah](../../strongs/h/h7200.md), and, behold, the [paniym](../../strongs/h/h6440.md) of the ['adamah](../../strongs/h/h127.md) was [ḥāraḇ](../../strongs/h/h2717.md).

<a name="genesis_8_14"></a>Genesis 8:14

And in the second [ḥōḏeš](../../strongs/h/h2320.md), on the seven and twentieth [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md), was the ['erets](../../strongs/h/h776.md) [yāḇēš](../../strongs/h/h3001.md).

<a name="genesis_8_15"></a>Genesis 8:15

And ['Elohiym](../../strongs/h/h430.md) [dabar](../../strongs/h/h1696.md) unto [Nōaḥ](../../strongs/h/h5146.md), ['āmar](../../strongs/h/h559.md),

<a name="genesis_8_16"></a>Genesis 8:16

[yāṣā'](../../strongs/h/h3318.md) of the [tēḇâ](../../strongs/h/h8392.md), thou, and thy ['ishshah](../../strongs/h/h802.md), and thy [ben](../../strongs/h/h1121.md), and thy [ben](../../strongs/h/h1121.md) ['ishshah](../../strongs/h/h802.md) with thee.

<a name="genesis_8_17"></a>Genesis 8:17

[yāṣā'](../../strongs/h/h3318.md) with thee every [chay](../../strongs/h/h2416.md) that is with thee, of all [basar](../../strongs/h/h1320.md), both of [ʿôp̄](../../strongs/h/h5775.md), and of [bĕhemah](../../strongs/h/h929.md), and of every [remeś](../../strongs/h/h7431.md) that [rāmaś](../../strongs/h/h7430.md) upon the ['erets](../../strongs/h/h776.md); that they may [šāraṣ](../../strongs/h/h8317.md) in the ['erets](../../strongs/h/h776.md), and be [parah](../../strongs/h/h6509.md), and [rabah](../../strongs/h/h7235.md) upon the ['erets](../../strongs/h/h776.md).

<a name="genesis_8_18"></a>Genesis 8:18

And [Nōaḥ](../../strongs/h/h5146.md) [yāṣā'](../../strongs/h/h3318.md), and his [ben](../../strongs/h/h1121.md), and his ['ishshah](../../strongs/h/h802.md), and his [ben](../../strongs/h/h1121.md) ['ishshah](../../strongs/h/h802.md) with him:

<a name="genesis_8_19"></a>Genesis 8:19

Every [chay](../../strongs/h/h2416.md), every [remeś](../../strongs/h/h7431.md), and every [ʿôp̄](../../strongs/h/h5775.md), and [kōl](../../strongs/h/h3605.md) [rāmaś](../../strongs/h/h7430.md) upon the ['erets](../../strongs/h/h776.md), after their [mišpāḥâ](../../strongs/h/h4940.md), [yāṣā'](../../strongs/h/h3318.md) out of the [tēḇâ](../../strongs/h/h8392.md).

<a name="genesis_8_20"></a>Genesis 8:20

And [Nōaḥ](../../strongs/h/h5146.md) [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md); and [laqach](../../strongs/h/h3947.md) of every [tahowr](../../strongs/h/h2889.md) [bĕhemah](../../strongs/h/h929.md), and of every [tahowr](../../strongs/h/h2889.md) [ʿôp̄](../../strongs/h/h5775.md), and [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) on the [mizbeach](../../strongs/h/h4196.md).

<a name="genesis_8_21"></a>Genesis 8:21

And [Yĕhovah](../../strongs/h/h3068.md) [rîaḥ](../../strongs/h/h7306.md) a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md); and [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) in his [leb](../../strongs/h/h3820.md), I will not again [qālal](../../strongs/h/h7043.md) the ['adamah](../../strongs/h/h127.md) any more for ['adam](../../strongs/h/h120.md) [ʿăḇûr](../../strongs/h/h5668.md); for the [yetser](../../strongs/h/h3336.md) of ['adam](../../strongs/h/h120.md) [leb](../../strongs/h/h3820.md) is [ra'](../../strongs/h/h7451.md) from his [nāʿur](../../strongs/h/h5271.md); neither will I again [nakah](../../strongs/h/h5221.md) any more every [chay](../../strongs/h/h2416.md), as I have ['asah](../../strongs/h/h6213.md).

<a name="genesis_8_22"></a>Genesis 8:22

While the ['erets](../../strongs/h/h776.md) [yowm](../../strongs/h/h3117.md), [zera'](../../strongs/h/h2233.md) and [qāṣîr](../../strongs/h/h7105.md), and [qōr](../../strongs/h/h7120.md) and [ḥōm](../../strongs/h/h2527.md), and [qayiṣ](../../strongs/h/h7019.md) and [ḥōrep̄](../../strongs/h/h2779.md), and [yowm](../../strongs/h/h3117.md) and [layil](../../strongs/h/h3915.md) shall not [shabath](../../strongs/h/h7673.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 7](genesis_7.md) - [Genesis 9](genesis_9.md)