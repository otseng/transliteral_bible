# [Genesis 28](https://www.blueletterbible.org/lxx/genesis/28)

<a name="genesis_28_1"></a>Genesis 28:1

[having [proskaleō](../../strongs/g/g4341.md) [Isaak](../../strongs/g/g2464.md) [Iakōb](../../strongs/g/g2384.md), [eulogeō](../../strongs/g/g2127.md) him, and ≠[entellō](../../strongs/g/g1781.md) to him, [legō](../../strongs/g/g3004.md), Not shall you [lambanō](../../strongs/g/g2983.md) a [gynē](../../strongs/g/g1135.md) from the [thygatēr](../../strongs/g/g2364.md) of the [Chananaios](../../strongs/g/g5478.md).

<a name="genesis_28_2"></a>Genesis 28:2

Having [anistēmi](../../strongs/g/g450.md), you run away (apodrathi) unto [Mesopotamia](../../strongs/g/g3318.md) to the [oikos](../../strongs/g/g3624.md) of Bethuel the [patēr](../../strongs/g/g3962.md) of your [mētēr](../../strongs/g/g3384.md)! and [lambanō](../../strongs/g/g2983.md) to yourself from there a [gynē](../../strongs/g/g1135.md) from the [thygatēr](../../strongs/g/g2364.md) of Laban the [adelphos](../../strongs/g/g80.md) of your [mētēr](../../strongs/g/g3384.md)!

<a name="genesis_28_3"></a>Genesis 28:3

 And my [theos](../../strongs/g/g2316.md) may [eulogeō](../../strongs/g/g2127.md) you, and [auxanō](../../strongs/g/g837.md) you, and [plēthynō](../../strongs/g/g4129.md) you; and you will be for [synagōgē](../../strongs/g/g4864.md) of [ethnos](../../strongs/g/g1484.md). 

<a name="genesis_28_4"></a>Genesis 28:4

And may he [didōmi](../../strongs/g/g1325.md) to you the [eulogia](../../strongs/g/g2129.md) of [Abraam](../../strongs/g/g11.md), my [patēr](../../strongs/g/g3962.md), to you, and your [sperma](../../strongs/g/g4690.md) after you; to [klēronomeō](../../strongs/g/g2816.md) the [gē](../../strongs/g/g1093.md) of your sojourning (paroikēseōs), which [theos](../../strongs/g/g2316.md) [didōmi](../../strongs/g/g1325.md) to [Abraam](../../strongs/g/g11.md).

<a name="genesis_28_5"></a>Genesis 28:5

And [Isaak](../../strongs/g/g2464.md) [apostellō](../../strongs/g/g649.md) [Iakōb](../../strongs/g/g2384.md). And he [poreuō](../../strongs/g/g4198.md) into [Mesopotamia](../../strongs/g/g3318.md) to Laban the [huios](../../strongs/g/g5207.md) of Bethuel the Syrian, [adelphos](../../strongs/g/g80.md) of [Rhebekka](../../strongs/g/g4479.md), the [mētēr](../../strongs/g/g3384.md) of [Iakōb](../../strongs/g/g2384.md) and [Ēsau](../../strongs/g/g2269.md).

<a name="genesis_28_6"></a>Genesis 28:6

having [eidō](../../strongs/g/g1492.md) [Ēsau](../../strongs/g/g2269.md) that [Isaak](../../strongs/g/g2464.md) [eulogeō](../../strongs/g/g2127.md) [Iakōb](../../strongs/g/g2384.md), and [apostellō](../../strongs/g/g649.md) into [Mesopotamia](../../strongs/g/g3318.md) of [Syria](../../strongs/g/g4947.md) to [lambanō](../../strongs/g/g2983.md) to himself a [gynē](../../strongs/g/g1135.md) from there -- in the [eulogeō](../../strongs/g/g2127.md) him, and [entellō](../../strongs/g/g1781.md) to him, [legō](../../strongs/g/g3004.md), You shall not [lambanō](../../strongs/g/g2983.md) a [gynē](../../strongs/g/g1135.md) of the [thygatēr](../../strongs/g/g2364.md) of the [Chananaios](../../strongs/g/g5478.md),

<a name="genesis_28_7"></a>Genesis 28:7

that [Iakōb](../../strongs/g/g2384.md) [akouō](../../strongs/g/g191.md) to [patēr](../../strongs/g/g3962.md) and [mētēr](../../strongs/g/g3384.md) his] , and [poreuō](../../strongs/g/g4198.md) into [Mesopotamia](../../strongs/g/g3318.md) of [Syria](../../strongs/g/g4947.md),

<a name="genesis_28_8"></a>Genesis 28:8

and [eidō](../../strongs/g/g1492.md), even Esau, that are [ponēros](../../strongs/g/g4190.md) the [thygatēr](../../strongs/g/g2364.md) of [Chanaan](../../strongs/g/g5477.md) [enantion](../../strongs/g/g1726.md) [Isaak](../../strongs/g/g2464.md) his [patēr](../../strongs/g/g3962.md), 

<a name="genesis_28_9"></a>Genesis 28:9

that [Ēsau](../../strongs/g/g2269.md) [poreuō](../../strongs/g/g4198.md) to Ishmael, and he [lambanō](../../strongs/g/g2983.md) Mahalath [thygatēr](../../strongs/g/g2364.md) of Ishmael, the [huios](../../strongs/g/g5207.md) of [Abraam](../../strongs/g/g11.md), [adelphē](../../strongs/g/g79.md) of Nebajoth, to his [gynē](../../strongs/g/g1135.md), as a [gynē](../../strongs/g/g1135.md). 

<a name="genesis_28_10"></a>Genesis 28:10

And [Iakōb](../../strongs/g/g2384.md) [exerchomai](../../strongs/g/g1831.md) from the [phrear](../../strongs/g/g5421.md) of the [horkos](../../strongs/g/g3727.md), and [poreuō](../../strongs/g/g4198.md) into Haran.

<a name="genesis_28_11"></a>Genesis 28:11

And he [apantaō](../../strongs/g/g528.md) a [topos](../../strongs/g/g5117.md), and [koimaō](../../strongs/g/g2837.md) there, [dynō](../../strongs/g/g1416.md) for the [hēlios](../../strongs/g/g2246.md). And he [lambanō](../../strongs/g/g2983.md) from one of the [lithos](../../strongs/g/g3037.md) of the [topos](../../strongs/g/g5117.md), and [tithēmi](../../strongs/g/g5087.md) at his [kephalē](../../strongs/g/g2776.md), and went to [koimaō](../../strongs/g/g2837.md) in that [topos](../../strongs/g/g5117.md). 

<a name="genesis_28_12"></a>Genesis 28:12

And he [enypniazomai](../../strongs/g/g1797.md). And [idou](../../strongs/g/g2400.md), there was a stairway (klimax) being [stērizō](../../strongs/g/g4741.md) by the [gē](../../strongs/g/g1093.md), of which the [kephalē](../../strongs/g/g2776.md) [aphikneomai](../../strongs/g/g864.md) in the [ouranos](../../strongs/g/g3772.md). And the [aggelos](../../strongs/g/g32.md) of [theos](../../strongs/g/g2316.md) [anabainō](../../strongs/g/g305.md) and [katabainō](../../strongs/g/g2597.md) upon it. 

<a name="genesis_28_13"></a>Genesis 28:13

And the [kyrios](../../strongs/g/g2962.md) [epistērizō](../../strongs/g/g1991.md) upon it. And he [eipon](../../strongs/g/g2036.md), I am the [theos](../../strongs/g/g2316.md) of [Abraam](../../strongs/g/g11.md) your [patēr](../../strongs/g/g3962.md), and the [theos](../../strongs/g/g2316.md) of [Isaak](../../strongs/g/g2464.md). Do not [phobeō](../../strongs/g/g5399.md)! The [gē](../../strongs/g/g1093.md) of which you [katheudō](../../strongs/g/g2518.md) upon it, to you I will [didōmi](../../strongs/g/g1325.md) it, and to your [sperma](../../strongs/g/g4690.md). 

<a name="genesis_28_14"></a>Genesis 28:14

And will be your [sperma](../../strongs/g/g4690.md) as [ammos](../../strongs/g/g285.md) of the [gē](../../strongs/g/g1093.md). And it will be [platynō](../../strongs/g/g4115.md) unto the [thalassa](../../strongs/g/g2281.md), and [lips](../../strongs/g/g3047.md), and [borras](../../strongs/g/g1005.md), and unto the [anatolē](../../strongs/g/g395.md). And shall be [eneulogeō](../../strongs/g/g1757.md) by you all the [phylē](../../strongs/g/g5443.md) of the [gē](../../strongs/g/g1093.md), and by your [sperma](../../strongs/g/g4690.md). 

<a name="genesis_28_15"></a>Genesis 28:15

And [idou](../../strongs/g/g2400.md), I am with you, [diaphylassō](../../strongs/g/g1314.md) you in [hodos](../../strongs/g/g3598.md) every of which ever you may [poreuō](../../strongs/g/g4198.md). And I will [apostrephō](../../strongs/g/g654.md) you to this [gē](../../strongs/g/g1093.md), for no way would I [egkataleipō](../../strongs/g/g1459.md) you until my [poieō](../../strongs/g/g4160.md) all as much as I [laleō](../../strongs/g/g2980.md) to you. 

<a name="genesis_28_16"></a>Genesis 28:16

And [Iakōb](../../strongs/g/g2384.md) [exegeirō](../../strongs/g/g1825.md) from out of his [hypnos](../../strongs/g/g5258.md). And he [eipon](../../strongs/g/g2036.md) that, The [kyrios](../../strongs/g/g2962.md) is in this [topos](../../strongs/g/g5117.md), but I had not [eidō](../../strongs/g/g1492.md) it. 

<a name="genesis_28_17"></a>Genesis 28:17

And he [phobeō](../../strongs/g/g5399.md), and [eipon](../../strongs/g/g2036.md), How [phoberos](../../strongs/g/g5398.md) this [topos](../../strongs/g/g5117.md). Is not this none other than the [oikos](../../strongs/g/g3624.md) of [theos](../../strongs/g/g2316.md), and this the [pylē](../../strongs/g/g4439.md) of [ouranos](../../strongs/g/g3772.md). 

<a name="genesis_28_18"></a>Genesis 28:18

And [Iakōb](../../strongs/g/g2384.md) [anistēmi](../../strongs/g/g450.md) in the [prōï](../../strongs/g/g4404.md), and [lambanō](../../strongs/g/g2983.md) the [lithos](../../strongs/g/g3037.md) which he [hypotithēmi](../../strongs/g/g5294.md) there at his [kephalē](../../strongs/g/g2776.md). And he [histēmi](../../strongs/g/g2476.md) it as a monument (stēlēn), and [epicheō](../../strongs/g/g2022.md) [elaion](../../strongs/g/g1637.md) upon the [akron](../../strongs/g/g206.md) of it. 

<a name="genesis_28_19"></a>Genesis 28:19

And he [kaleō](../../strongs/g/g2564.md) the [onoma](../../strongs/g/g3686.md) of that [topos](../../strongs/g/g5117.md), [oikos](../../strongs/g/g3624.md) of [theos](../../strongs/g/g2316.md), and Lam-luz was the [onoma](../../strongs/g/g3686.md) to the [polis](../../strongs/g/g4172.md) [proteros](../../strongs/g/g4387.md). 

<a name="genesis_28_20"></a>Genesis 28:20

And [Iakōb](../../strongs/g/g2384.md) [euchomai](../../strongs/g/g2172.md) a [euchē](../../strongs/g/g2171.md), [legō](../../strongs/g/g3004.md), If should be the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) with me, and should [diaphylassō](../../strongs/g/g1314.md) me in this [hodos](../../strongs/g/g3598.md) which I [poreuō](../../strongs/g/g4198.md), and should [didōmi](../../strongs/g/g1325.md) to me [artos](../../strongs/g/g740.md) to [esthiō](../../strongs/g/g2068.md), and a [himation](../../strongs/g/g2440.md) to put [periballō](../../strongs/g/g4016.md), 

<a name="genesis_28_21"></a>Genesis 28:21

and will [apostrephō](../../strongs/g/g654.md) me with [sōtēria](../../strongs/g/g4991.md) unto the [oikos](../../strongs/g/g3624.md) of my [patēr](../../strongs/g/g3962.md), and the [kyrios](../../strongs/g/g2962.md) will be to me for [theos](../../strongs/g/g2316.md), 

<a name="genesis_28_22"></a>Genesis 28:22

and this [lithos](../../strongs/g/g3037.md) which I [histēmi](../../strongs/g/g2476.md) as a monument (stēlēn), will be to me a [oikos](../../strongs/g/g3624.md) of [theos](../../strongs/g/g2316.md). And all, what ever, you should [didōmi](../../strongs/g/g1325.md) to me, a [dekatē](../../strongs/g/g1181.md) I will [apodekatoō](../../strongs/g/g586.md) of it to you. 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 27](genesis_lxx_27.md) - [Genesis 29](genesis_lxx_29.md)