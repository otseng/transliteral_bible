# [Genesis 2](https://www.blueletterbible.org/lxx/genesis/2)

<a name="genesis_2_1"></a>Genesis 2:1

And were [synteleō](../../strongs/g/g4931.md) the [ouranos](../../strongs/g/g3772.md) and the [gē](../../strongs/g/g1093.md), and all the [kosmos](../../strongs/g/g2889.md) of them. 

<a name="genesis_2_2"></a>Genesis 2:2

And [theos](../../strongs/g/g2316.md) [synteleō](../../strongs/g/g4931.md) in the [hēmera](../../strongs/g/g2250.md) [ektos](../../strongs/g/g1623.md) his [ergon](../../strongs/g/g2041.md) which he [poieō](../../strongs/g/g4160.md). And he [katapauō](../../strongs/g/g2664.md) on the [hēmera](../../strongs/g/g2250.md) [ebdomos](../../strongs/g/g1442.md) from all his [ergon](../../strongs/g/g2041.md) which he [poieō](../../strongs/g/g4160.md). 

<a name="genesis_2_3"></a>Genesis 2:3

And [theos](../../strongs/g/g2316.md) [eulogeō](../../strongs/g/g2127.md) the [hēmera](../../strongs/g/g2250.md) [ebdomos](../../strongs/g/g1442.md), and [hagiazō](../../strongs/g/g37.md) it; for in it he [katapauō](../../strongs/g/g2664.md) from all his [ergon](../../strongs/g/g2041.md) -- which [theos](../../strongs/g/g2316.md) [archō](../../strongs/g/g757.md) to [poieō](../../strongs/g/g4160.md). 

<a name="genesis_2_4"></a>Genesis 2:4

This is the [biblos](../../strongs/g/g976.md) of the [genesis](../../strongs/g/g1078.md) of [ouranos](../../strongs/g/g3772.md) and [gē](../../strongs/g/g1093.md), when it became, in the [hēmera](../../strongs/g/g2250.md) [poieō](../../strongs/g/g4160.md) [theos](../../strongs/g/g2316.md) the [ouranos](../../strongs/g/g3772.md) and the [gē](../../strongs/g/g1093.md), 

<a name="genesis_2_5"></a>Genesis 2:5

and every [chlōros](../../strongs/g/g5515.md) [agros](../../strongs/g/g68.md) before it [ginomai](../../strongs/g/g1096.md) upon the [gē](../../strongs/g/g1093.md), and all [chortos](../../strongs/g/g5528.md) of the [agros](../../strongs/g/g68.md) before its [anatellō](../../strongs/g/g393.md), did not for [brechō](../../strongs/g/g1026.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) upon the [gē](../../strongs/g/g1093.md), and [anthrōpos](../../strongs/g/g444.md) no there was to [ergazomai](../../strongs/g/g2038.md) it.

<a name="genesis_2_6"></a>Genesis 2:6

But a [pēgē](../../strongs/g/g4077.md) [anabainō](../../strongs/g/g305.md) from out of the [gē](../../strongs/g/g1093.md), and it [potizō](../../strongs/g/g4222.md) all the [prosōpon](../../strongs/g/g4383.md) of the [gē](../../strongs/g/g1093.md). 

<a name="genesis_2_7"></a>Genesis 2:7

And [theos](../../strongs/g/g2316.md) [plassō](../../strongs/g/g4111.md) the [anthrōpos](../../strongs/g/g444.md), [chous](../../strongs/g/g5522.md) [lambanō](../../strongs/g/g2983.md) from the [gē](../../strongs/g/g1093.md). And he [emphysaō](../../strongs/g/g1720.md) into his [prosōpon](../../strongs/g/g4383.md) [pnoē](../../strongs/g/g4157.md) of [zōē](../../strongs/g/g2222.md), and [anthrōpos](../../strongs/g/g444.md) a [psychē](../../strongs/g/g5590.md) [zaō](../../strongs/g/g2198.md). 

<a name="genesis_2_8"></a>Genesis 2:8

And [theos](../../strongs/g/g2316.md) [phyteuō](../../strongs/g/g5452.md) [paradeisos](../../strongs/g/g3857.md) in Eden according to the [anatolē](../../strongs/g/g395.md), and he [tithēmi](../../strongs/g/g5087.md) there the [anthrōpos](../../strongs/g/g444.md) whom he [plassō](../../strongs/g/g4111.md). 

<a name="genesis_2_9"></a>Genesis 2:9

And [exanatellō](../../strongs/g/g1816.md) [theos](../../strongs/g/g2316.md) yet from the [gē](../../strongs/g/g1093.md) every [xylon](../../strongs/g/g3586.md) [hōraios](../../strongs/g/g5611.md) to the [horasis](../../strongs/g/g3706.md), and [kalos](../../strongs/g/g2570.md) for [brōsis](../../strongs/g/g1035.md), and the [xylon](../../strongs/g/g3586.md) of [zōē](../../strongs/g/g2222.md) in the [mesos](../../strongs/g/g3319.md) of the [paradeisos](../../strongs/g/g3857.md), and the [xylon](../../strongs/g/g3586.md), the one to [eidō](../../strongs/g/g1492.md) [gnōstos](../../strongs/g/g1110.md) [kalos](../../strongs/g/g2570.md) and [ponēros](../../strongs/g/g4190.md). 

<a name="genesis_2_10"></a>Genesis 2:10

And a [potamos](../../strongs/g/g4215.md) goes [ekporeuomai](../../strongs/g/g1607.md) from Eden to [potizō](../../strongs/g/g4222.md) the [paradeisos](../../strongs/g/g3857.md); from there it [aphorizō](../../strongs/g/g873.md) into [tessares](../../strongs/g/g5064.md) [archē](../../strongs/g/g746.md). 

<a name="genesis_2_11"></a>Genesis 2:11

The [onoma](../../strongs/g/g3686.md) to the one is Phison. This is the one [kykloō](../../strongs/g/g2944.md) all the [gē](../../strongs/g/g1093.md) of Havilah (Euilat) -- there where is the [chrysion](../../strongs/g/g5553.md). 

<a name="genesis_2_12"></a>Genesis 2:12

And the [chrysion](../../strongs/g/g5553.md) of that [gē](../../strongs/g/g1093.md) is [kalos](../../strongs/g/g2570.md), and there is the [anthrax](../../strongs/g/g440.md) and the [lithos](../../strongs/g/g3037.md) leek colored (prasinos).

<a name="genesis_2_13"></a>Genesis 2:13

And the [onoma](../../strongs/g/g3686.md) to the [potamos](../../strongs/g/g4215.md) [deuteros](../../strongs/g/g1208.md) is Gihon. This is the one [kykloō](../../strongs/g/g2944.md) all the [gē](../../strongs/g/g1093.md) of Ethiopia.

<a name="genesis_2_14"></a>Genesis 2:14

And the [potamos](../../strongs/g/g4215.md) [tritos](../../strongs/g/g5154.md) is the Tigris (Tigris). This is the one [proporeuomai](../../strongs/g/g4313.md) over [katenanti](../../strongs/g/g2713.md) the Assyrians. And the [potamos](../../strongs/g/g4215.md) [tetartos](../../strongs/g/g5067.md) is the Euphrates.

<a name="genesis_2_15"></a>Genesis 2:15

And [lambanō](../../strongs/g/g2983.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) the [anthrōpos](../../strongs/g/g444.md) whom he [plassō](../../strongs/g/g4111.md), and [tithēmi](../../strongs/g/g5087.md) him in the [paradeisos](../../strongs/g/g3857.md) to [ergazomai](../../strongs/g/g2038.md) it and to [phylassō](../../strongs/g/g5442.md). 

<a name="genesis_2_16"></a>Genesis 2:16

And [entellō](../../strongs/g/g1781.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) to [Adam](../../strongs/g/g76.md), [legō](../../strongs/g/g3004.md), From all of a [xylon](../../strongs/g/g3586.md) of the one in the [paradeisos](../../strongs/g/g3857.md) [brōsis](../../strongs/g/g1035.md) you shall [esthiō](../../strongs/g/g2068.md), 

<a name="genesis_2_17"></a>Genesis 2:17

but from the [xylon](../../strongs/g/g3586.md) of the [ginōskō](../../strongs/g/g1097.md) [kalos](../../strongs/g/g2570.md) and [ponēros](../../strongs/g/g4190.md), you shall not [esthiō](../../strongs/g/g2068.md) from it; but in whatever [hēmera](../../strongs/g/g2250.md) you should [esthiō](../../strongs/g/g2068.md) from it, to [thanatos](../../strongs/g/g2288.md) you shall [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_2_18"></a>Genesis 2:18

And [eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md), It is not [kalos](../../strongs/g/g2570.md) [anthrōpos](../../strongs/g/g444.md) [monon](../../strongs/g/g3440.md), let us [poieō](../../strongs/g/g4160.md) for him a [boēthos](../../strongs/g/g998.md) according to him! 

<a name="genesis_2_19"></a>Genesis 2:19

And [theos](../../strongs/g/g2316.md) [plassō](../../strongs/g/g4111.md) yet from out of the [gē](../../strongs/g/g1093.md) all the [thērion](../../strongs/g/g2342.md) of the [agros](../../strongs/g/g68.md), and all the [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md). And he [agō](../../strongs/g/g71.md) them to [Adam](../../strongs/g/g76.md), to [eidō](../../strongs/g/g1492.md) what he would [kaleō](../../strongs/g/g2564.md) them. And all what ever [kaleō](../../strongs/g/g2564.md) it [Adam](../../strongs/g/g76.md) -- [psychē](../../strongs/g/g5590.md) the [zaō](../../strongs/g/g2198.md), this was the [onoma](../../strongs/g/g3686.md) to it. 

<a name="genesis_2_20"></a>Genesis 2:20

And [Adam](../../strongs/g/g76.md) [kaleō](../../strongs/g/g2564.md) [onoma](../../strongs/g/g3686.md) to all the [ktēnos](../../strongs/g/g2934.md), and to all the [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md), and to all the [thērion](../../strongs/g/g2342.md) of the [agros](../../strongs/g/g68.md); but to [[Adam](../../strongs/g/g76.md)] there was not [heuriskō](../../strongs/g/g2147.md) a [boēthos](../../strongs/g/g998.md) [homoios](../../strongs/g/g3664.md) to him. 

<a name="genesis_2_21"></a>Genesis 2:21

And [epiballō](../../strongs/g/g1911.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) a change of [ekstasis](../../strongs/g/g1611.md) over [[Adam](../../strongs/g/g76.md)], and he slept (hypnōsen). And he [lambanō](../../strongs/g/g2983.md) one of his [pleura](../../strongs/g/g4125.md), and [anaplēroō](../../strongs/g/g378.md) [sarx](../../strongs/g/g4561.md) against it. 

<a name="genesis_2_22"></a>Genesis 2:22

And [oikodomeō](../../strongs/g/g3618.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) the [pleura](../../strongs/g/g4125.md) which he [lambanō](../../strongs/g/g2983.md) from [Adam](../../strongs/g/g76.md) into a [gynē](../../strongs/g/g1135.md). And he [agō](../../strongs/g/g71.md) her to [Adam](../../strongs/g/g76.md).

<a name="genesis_2_23"></a>Genesis 2:23

And [Adam](../../strongs/g/g76.md) [eipon](../../strongs/g/g2036.md), This now is [osteon](../../strongs/g/g3747.md) from my [osteon](../../strongs/g/g3747.md), and [sarx](../../strongs/g/g4561.md) from my [sarx](../../strongs/g/g4561.md); she shall be [kaleō](../../strongs/g/g2564.md) [gynē](../../strongs/g/g1135.md), for from the [anēr](../../strongs/g/g435.md) she was [lambanō](../../strongs/g/g2983.md). 

<a name="genesis_2_24"></a>Genesis 2:24

Because of this [kataleipō](../../strongs/g/g2641.md) [anthrōpos](../../strongs/g/g444.md) his [patēr](../../strongs/g/g3962.md) and [mētēr](../../strongs/g/g3384.md), and shall [proskollaō](../../strongs/g/g4347.md) his [gynē](../../strongs/g/g1135.md), and shall be the [dyo](../../strongs/g/g1417.md) for [sarx](../../strongs/g/g4561.md) [heis](../../strongs/g/g1520.md). 

<a name="genesis_2_25"></a>Genesis 2:25

And were the [dyo](../../strongs/g/g1417.md) [gymnos](../../strongs/g/g1131.md), both [Adam](../../strongs/g/g76.md) and his [gynē](../../strongs/g/g1135.md). And they were not [aischynō](../../strongs/g/g153.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 1](genesis_lxx_1.md) - [Genesis 3](genesis_lxx_3.md)