# [Genesis 5](https://www.blueletterbible.org/lxx/genesis/5)

<a name="genesis_5_1"></a>Genesis 5:1

This is the [biblos](../../strongs/g/g976.md) of the [genesis](../../strongs/g/g1078.md) of [anthrōpos](../../strongs/g/g444.md) in which day [theos](../../strongs/g/g2316.md) [poieō](../../strongs/g/g4160.md) [Adam](../../strongs/g/g76.md). According to the [eikōn](../../strongs/g/g1504.md) of [theos](../../strongs/g/g2316.md) he [poieō](../../strongs/g/g4160.md) him. 

<a name="genesis_5_2"></a>Genesis 5:2

[arrēn](../../strongs/g/g730.md) and [thēlys](../../strongs/g/g2338.md) he [poieō](../../strongs/g/g4160.md) them, and he [eulogeō](../../strongs/g/g2127.md) them. And he [eponomazō](../../strongs/g/g2028.md) his [onoma](../../strongs/g/g3686.md) [Adam](../../strongs/g/g76.md), in which day he [poieō](../../strongs/g/g4160.md) them. 

<a name="genesis_5_3"></a>Genesis 5:3

[zaō](../../strongs/g/g2198.md) and [Adam](../../strongs/g/g76.md) [triakonta](../../strongs/g/g5144.md) and [diakosioi](../../strongs/g/g1250.md) [etos](../../strongs/g/g2094.md). And he [gennaō](../../strongs/g/g1080.md) according to his [idea](../../strongs/g/g2397.md), and according to his [eikōn](../../strongs/g/g1504.md); and he [eponomazō](../../strongs/g/g2028.md) his [onoma](../../strongs/g/g3686.md), [Sēth](../../strongs/g/g4589.md).

<a name="genesis_5_4"></a>Genesis 5:4

And [ginomai](../../strongs/g/g1096.md) the [hēmera](../../strongs/g/g2250.md) of [Adam](../../strongs/g/g76.md) which he [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Sēth](../../strongs/g/g4589.md), [etos](../../strongs/g/g2094.md) seven hundred (heptakosia), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md). 

<a name="genesis_5_5"></a>Genesis 5:5

And [ginomai](../../strongs/g/g1096.md) all the [hēmera](../../strongs/g/g2250.md) of [Adam](../../strongs/g/g76.md) which he [zaō](../../strongs/g/g2198.md), [triakonta](../../strongs/g/g5144.md) and nine hundred (ennakosia) [etos](../../strongs/g/g2094.md), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_5_6"></a>Genesis 5:6

[zaō](../../strongs/g/g2198.md) And [Sēth](../../strongs/g/g4589.md) [pente](../../strongs/g/g4002.md) and [diakosioi](../../strongs/g/g1250.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [Enōs](../../strongs/g/g1800.md).

<a name="genesis_5_7"></a>Genesis 5:7

And [Sēth](../../strongs/g/g4589.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Enōs](../../strongs/g/g1800.md), [hepta](../../strongs/g/g2033.md) [etos](../../strongs/g/g2094.md) and seven hundred (heptakosia); and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md). 

<a name="genesis_5_8"></a>Genesis 5:8

And [ginomai](../../strongs/g/g1096.md) all the [hēmera](../../strongs/g/g2250.md) of [Sēth](../../strongs/g/g4589.md), [dōdeka](../../strongs/g/g1427.md) and nine hundred (ennakosia) [etos](../../strongs/g/g2094.md), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_5_9"></a>Genesis 5:9

And [Enōs](../../strongs/g/g1800.md) [zaō](../../strongs/g/g2198.md) [etos](../../strongs/g/g2094.md) a [hekaton](../../strongs/g/g1540.md) ninety (enenēkonta), and he [gennaō](../../strongs/g/g1080.md) [Kaïnam](../../strongs/g/g2536.md).

<a name="genesis_5_10"></a>Genesis 5:10

And [Enōs](../../strongs/g/g1800.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Kaïnam](../../strongs/g/g2536.md), [deka](../../strongs/g/g1176.md) [pente](../../strongs/g/g4002.md) [etos](../../strongs/g/g2094.md) and seven hundred (heptakosia), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md). 

<a name="genesis_5_11"></a>Genesis 5:11

And [ginomai](../../strongs/g/g1096.md) all the [hēmera](../../strongs/g/g2250.md) of [Enōs](../../strongs/g/g1800.md), [pente](../../strongs/g/g4002.md) [etos](../../strongs/g/g2094.md) and nine hundred (ennakosia), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_5_12"></a>Genesis 5:12

And [Kaïnam](../../strongs/g/g2536.md) [zaō](../../strongs/g/g2198.md) [hebdomēkonta](../../strongs/g/g1440.md) and a [hekaton](../../strongs/g/g1540.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [Maleleēl](../../strongs/g/g3121.md).

<a name="genesis_5_13"></a>Genesis 5:13

And [Kaïnam](../../strongs/g/g2536.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Maleleēl](../../strongs/g/g3121.md), [tessarakonta](../../strongs/g/g5062.md) and seven hundred (heptakosia) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md). 

<a name="genesis_5_14"></a>Genesis 5:14

And [ginomai](../../strongs/g/g1096.md) all the [hēmera](../../strongs/g/g2250.md) of [Kaïnam](../../strongs/g/g2536.md), [deka](../../strongs/g/g1176.md) [etos](../../strongs/g/g2094.md) and nine hundred (ennakosia), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_5_15"></a>Genesis 5:15

And [Maleleēl](../../strongs/g/g3121.md) [zaō](../../strongs/g/g2198.md) [pente](../../strongs/g/g4002.md) and [hexēkonta](../../strongs/g/g1835.md) and a [hekaton](../../strongs/g/g1540.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [Iaret](../../strongs/g/g2391.md).

<a name="genesis_5_16"></a>Genesis 5:16

And [Maleleēl](../../strongs/g/g3121.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Iaret](../../strongs/g/g2391.md), [etos](../../strongs/g/g2094.md) [triakonta](../../strongs/g/g5144.md) and seven hundred (heptakosia), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md). 

<a name="genesis_5_17"></a>Genesis 5:17

And [ginomai](../../strongs/g/g1096.md) all the [hēmera](../../strongs/g/g2250.md) of [Maleleēl](../../strongs/g/g3121.md) [etos](../../strongs/g/g2094.md) [pente](../../strongs/g/g4002.md) and ninety (enenēkonta) and eight hundred (oktakosia), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_5_18"></a>Genesis 5:18

And [Iaret](../../strongs/g/g2391.md) [zaō](../../strongs/g/g2198.md) [dyo](../../strongs/g/g1417.md) and [hexēkonta](../../strongs/g/g1835.md) [etos](../../strongs/g/g2094.md) and a [hekaton](../../strongs/g/g1540.md), and he [gennaō](../../strongs/g/g1080.md) [Henōch](../../strongs/g/g1802.md).

<a name="genesis_5_19"></a>Genesis 5:19

And [Iaret](../../strongs/g/g2391.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Henōch](../../strongs/g/g1802.md), eight hundred (oktakosia) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md). 

<a name="genesis_5_20"></a>Genesis 5:20

And [ginomai](../../strongs/g/g1096.md) all the [hēmera](../../strongs/g/g2250.md) of [Iaret](../../strongs/g/g2391.md), [dyo](../../strongs/g/g1417.md) and [hexēkonta](../../strongs/g/g1835.md) and nine hundred (ennakosia) [etos](../../strongs/g/g2094.md), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_5_21"></a>Genesis 5:21

And [Henōch](../../strongs/g/g1802.md) [zaō](../../strongs/g/g2198.md) [pente](../../strongs/g/g4002.md) and [hexēkonta](../../strongs/g/g1835.md) and a [hekaton](../../strongs/g/g1540.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [Mathousala](../../strongs/g/g3103.md).

<a name="genesis_5_22"></a>Genesis 5:22

was [euaresteō](../../strongs/g/g2100.md) [Henōch](../../strongs/g/g1802.md) to [theos](../../strongs/g/g2316.md). And [Henōch](../../strongs/g/g1802.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Mathousala](../../strongs/g/g3103.md), [diakosioi](../../strongs/g/g1250.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md). 

<a name="genesis_5_23"></a>Genesis 5:23

And [ginomai](../../strongs/g/g1096.md) all the [hēmera](../../strongs/g/g2250.md) of [Henōch](../../strongs/g/g1802.md), [pente](../../strongs/g/g4002.md) and [hexēkonta](../../strongs/g/g1835.md) and [triakosioi](../../strongs/g/g5145.md) [etos](../../strongs/g/g2094.md). 

<a name="genesis_5_24"></a>Genesis 5:24

And [Henōch](../../strongs/g/g1802.md) was [euaresteō](../../strongs/g/g2100.md) to [theos](../../strongs/g/g2316.md). And he was not [heuriskō](../../strongs/g/g2147.md), for [metatithēmi](../../strongs/g/g3346.md) him [theos](../../strongs/g/g2316.md). 

<a name="genesis_5_25"></a>Genesis 5:25

And [Mathousala](../../strongs/g/g3103.md) [zaō](../../strongs/g/g2198.md) [hepta](../../strongs/g/g2033.md) and [hexēkonta](../../strongs/g/g1835.md) and a [hekaton](../../strongs/g/g1540.md) [etos](../../strongs/g/g2094.md) , and he [gennaō](../../strongs/g/g1080.md) [Lamech](../../strongs/g/g2984.md).

<a name="genesis_5_26"></a>Genesis 5:26

And [Mathousala](../../strongs/g/g3103.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Lamech](../../strongs/g/g2984.md), [dyo](../../strongs/g/g1417.md) and eight hundred (oktakosia) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md). 

<a name="genesis_5_27"></a>Genesis 5:27

And [ginomai](../../strongs/g/g1096.md) all the [hēmera](../../strongs/g/g2250.md) of [Mathousala](../../strongs/g/g3103.md) which he [zaō](../../strongs/g/g2198.md), [ennea](../../strongs/g/g1767.md) and [hexēkonta](../../strongs/g/g1835.md) and nine hundred (ennakosia) [etos](../../strongs/g/g2094.md), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_5_28"></a>Genesis 5:28

And [Lamech](../../strongs/g/g2984.md) [zaō](../../strongs/g/g2198.md) [oktō](../../strongs/g/g3638.md) and [ogdoēkonta](../../strongs/g/g3589.md) and a [hekaton](../../strongs/g/g1540.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) a [huios](../../strongs/g/g5207.md). 

<a name="genesis_5_29"></a>Genesis 5:29

And he [eponomazō](../../strongs/g/g2028.md) his [onoma](../../strongs/g/g3686.md), [Nōe](../../strongs/g/g3575.md), [legō](../../strongs/g/g3004.md), This one will rest (dianapausei) us from our [ergon](../../strongs/g/g2041.md), and from the [lypē](../../strongs/g/g3077.md) of our [cheir](../../strongs/g/g5495.md), and from the [gē](../../strongs/g/g1093.md) of which  [kataraomai](../../strongs/g/g2672.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md). 

<a name="genesis_5_30"></a>Genesis 5:30

And [Lamech](../../strongs/g/g2984.md) [zaō](../../strongs/g/g2198.md) after his [gennaō](../../strongs/g/g1080.md) [Nōe](../../strongs/g/g3575.md), [pentakosioi](../../strongs/g/g4001.md) and [hexēkonta](../../strongs/g/g1835.md) and [pente](../../strongs/g/g4002.md) [etos](../../strongs/g/g2094.md), and he [gennaō](../../strongs/g/g1080.md) [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md). 

<a name="genesis_5_31"></a>Genesis 5:31

And [ginomai](../../strongs/g/g1096.md) all the [hēmera](../../strongs/g/g2250.md) of [Lamech](../../strongs/g/g2984.md), seven hundred (heptakosia) and [pentēkonta](../../strongs/g/g4004.md) [treis](../../strongs/g/g5140.md) [etos](../../strongs/g/g2094.md), and he [apothnēskō](../../strongs/g/g599.md). 

<a name="genesis_5_32"></a>Genesis 5:32

And [Nōe](../../strongs/g/g3575.md) was [etos](../../strongs/g/g2094.md) [pentakosioi](../../strongs/g/g4001.md) and he [gennaō](../../strongs/g/g1080.md) three [huios](../../strongs/g/g5207.md), [Sēm](../../strongs/g/g4590.md), Ham, and Japheth.

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 4](genesis_lxx_4.md) - [Genesis 6](genesis_lxx_6.md)