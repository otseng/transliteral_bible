# [Genesis 42](https://www.blueletterbible.org/kjv/gen/42/1/s_42001)

<a name="genesis_42_1"></a>Genesis 42:1

Now when [Ya'aqob](../../strongs/h/h3290.md) [ra'ah](../../strongs/h/h7200.md) that there was [šēḇer](../../strongs/h/h7668.md) in [Mitsrayim](../../strongs/h/h4714.md), [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md) unto his [ben](../../strongs/h/h1121.md), Why do ye [ra'ah](../../strongs/h/h7200.md) one upon another?

<a name="genesis_42_2"></a>Genesis 42:2

And he ['āmar](../../strongs/h/h559.md), Behold, I have [shama'](../../strongs/h/h8085.md) that there is [šēḇer](../../strongs/h/h7668.md) in [Mitsrayim](../../strongs/h/h4714.md): [yarad](../../strongs/h/h3381.md) you thither, and [šāḇar](../../strongs/h/h7666.md) for us from thence; that we may [ḥāyâ](../../strongs/h/h2421.md), and not [muwth](../../strongs/h/h4191.md).

<a name="genesis_42_3"></a>Genesis 42:3

And [Yôsēp̄](../../strongs/h/h3130.md) ten ['ach](../../strongs/h/h251.md) [yarad](../../strongs/h/h3381.md) to [šāḇar](../../strongs/h/h7666.md) [bar](../../strongs/h/h1250.md) in [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_42_4"></a>Genesis 42:4

But [Binyāmîn](../../strongs/h/h1144.md), [Yôsēp̄](../../strongs/h/h3130.md) ['ach](../../strongs/h/h251.md), [Ya'aqob](../../strongs/h/h3290.md) [shalach](../../strongs/h/h7971.md) not with his ['ach](../../strongs/h/h251.md); for he ['āmar](../../strongs/h/h559.md), Lest peradventure ['āsôn](../../strongs/h/h611.md) [qārā'](../../strongs/h/h7122.md) him.

<a name="genesis_42_5"></a>Genesis 42:5

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) to [šāḇar](../../strongs/h/h7666.md) [tavek](../../strongs/h/h8432.md) those that [bow'](../../strongs/h/h935.md): for the [rāʿāḇ](../../strongs/h/h7458.md) was in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md).

<a name="genesis_42_6"></a>Genesis 42:6

And [Yôsēp̄](../../strongs/h/h3130.md) was the [šallîṭ](../../strongs/h/h7989.md) over the ['erets](../../strongs/h/h776.md), and he it was that [šāḇar](../../strongs/h/h7666.md) to all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md): and [Yôsēp̄](../../strongs/h/h3130.md) ['ach](../../strongs/h/h251.md) [bow'](../../strongs/h/h935.md), and [shachah](../../strongs/h/h7812.md) themselves before him with their ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md).

<a name="genesis_42_7"></a>Genesis 42:7

And [Yôsēp̄](../../strongs/h/h3130.md) [ra'ah](../../strongs/h/h7200.md) his ['ach](../../strongs/h/h251.md), and he [nāḵar](../../strongs/h/h5234.md) them, but [nāḵar](../../strongs/h/h5234.md) himself unto them, and [dabar](../../strongs/h/h1696.md) [qāšê](../../strongs/h/h7186.md) unto them; and he ['āmar](../../strongs/h/h559.md) unto them, Whence [bow'](../../strongs/h/h935.md) ye? And they ['āmar](../../strongs/h/h559.md), From the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md) to [šāḇar](../../strongs/h/h7666.md) ['ōḵel](../../strongs/h/h400.md).

<a name="genesis_42_8"></a>Genesis 42:8

And [Yôsēp̄](../../strongs/h/h3130.md) [nāḵar](../../strongs/h/h5234.md) his ['ach](../../strongs/h/h251.md), but they [nāḵar](../../strongs/h/h5234.md) not him.

<a name="genesis_42_9"></a>Genesis 42:9

And [Yôsēp̄](../../strongs/h/h3130.md) [zakar](../../strongs/h/h2142.md) the [ḥălôm](../../strongs/h/h2472.md) which he [ḥālam](../../strongs/h/h2492.md) of them, and ['āmar](../../strongs/h/h559.md) unto them, Ye are [ragal](../../strongs/h/h7270.md); to [ra'ah](../../strongs/h/h7200.md) the [ʿervâ](../../strongs/h/h6172.md) of the ['erets](../../strongs/h/h776.md) ye are [bow'](../../strongs/h/h935.md).

<a name="genesis_42_10"></a>Genesis 42:10

And they ['āmar](../../strongs/h/h559.md) unto him, Nay, my ['adown](../../strongs/h/h113.md), but to [šāḇar](../../strongs/h/h7666.md) ['ōḵel](../../strongs/h/h400.md) are thy ['ebed](../../strongs/h/h5650.md) [bow'](../../strongs/h/h935.md).

<a name="genesis_42_11"></a>Genesis 42:11

We are all ['echad](../../strongs/h/h259.md) ['iysh](../../strongs/h/h376.md) [ben](../../strongs/h/h1121.md); we are [kēn](../../strongs/h/h3651.md) men, thy ['ebed](../../strongs/h/h5650.md) are no [ragal](../../strongs/h/h7270.md).

<a name="genesis_42_12"></a>Genesis 42:12

And he ['āmar](../../strongs/h/h559.md) unto them, Nay, but to [ra'ah](../../strongs/h/h7200.md) the [ʿervâ](../../strongs/h/h6172.md) of the ['erets](../../strongs/h/h776.md) ye are [bow'](../../strongs/h/h935.md).

<a name="genesis_42_13"></a>Genesis 42:13

And they ['āmar](../../strongs/h/h559.md), Thy ['ebed](../../strongs/h/h5650.md) are twelve ['ach](../../strongs/h/h251.md), the [ben](../../strongs/h/h1121.md) of one ['iysh](../../strongs/h/h376.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md); and, behold, the [qāṭān](../../strongs/h/h6996.md) is this [yowm](../../strongs/h/h3117.md) with our ['ab](../../strongs/h/h1.md), and one is not.

<a name="genesis_42_14"></a>Genesis 42:14

And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto them, That is it that I [dabar](../../strongs/h/h1696.md) unto you, ['āmar](../../strongs/h/h559.md), Ye are [ragal](../../strongs/h/h7270.md):

<a name="genesis_42_15"></a>Genesis 42:15

Hereby ye shall be [bachan](../../strongs/h/h974.md): By the [chay](../../strongs/h/h2416.md) of [Parʿô](../../strongs/h/h6547.md) ye shall not [yāṣā'](../../strongs/h/h3318.md) hence, except your [qāṭān](../../strongs/h/h6996.md) ['ach](../../strongs/h/h251.md) [bow'](../../strongs/h/h935.md) hither.

<a name="genesis_42_16"></a>Genesis 42:16

[shalach](../../strongs/h/h7971.md) one of you, and let him [laqach](../../strongs/h/h3947.md) your ['ach](../../strongs/h/h251.md), and ye shall be kept in ['āsar](../../strongs/h/h631.md), that your [dabar](../../strongs/h/h1697.md) may be [bachan](../../strongs/h/h974.md), whether there be any ['emeth](../../strongs/h/h571.md) in you: or else by the [chay](../../strongs/h/h2416.md) of [Parʿô](../../strongs/h/h6547.md) surely ye are [ragal](../../strongs/h/h7270.md).

<a name="genesis_42_17"></a>Genesis 42:17

And he put them all ['āsap̄](../../strongs/h/h622.md) into [mišmār](../../strongs/h/h4929.md) three [yowm](../../strongs/h/h3117.md).

<a name="genesis_42_18"></a>Genesis 42:18

And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto them the third [yowm](../../strongs/h/h3117.md), This ['asah](../../strongs/h/h6213.md), and [ḥāyâ](../../strongs/h/h2421.md); for I [yārē'](../../strongs/h/h3373.md) ['Elohiym](../../strongs/h/h430.md):

<a name="genesis_42_19"></a>Genesis 42:19

If ye be [kēn](../../strongs/h/h3651.md), let one of your ['ach](../../strongs/h/h251.md) be ['āsar](../../strongs/h/h631.md) in the [bayith](../../strongs/h/h1004.md) of your [mišmār](../../strongs/h/h4929.md): [yālaḵ](../../strongs/h/h3212.md) ye, [bow'](../../strongs/h/h935.md) [šēḇer](../../strongs/h/h7668.md) for the [rᵊʿāḇôn](../../strongs/h/h7459.md) of your [bayith](../../strongs/h/h1004.md):

<a name="genesis_42_20"></a>Genesis 42:20

But [bow'](../../strongs/h/h935.md) your [qāṭān](../../strongs/h/h6996.md) ['ach](../../strongs/h/h251.md) unto me; so shall your [dabar](../../strongs/h/h1697.md) be ['aman](../../strongs/h/h539.md), and ye shall not [muwth](../../strongs/h/h4191.md). And they ['asah](../../strongs/h/h6213.md) so.

<a name="genesis_42_21"></a>Genesis 42:21

And they ['āmar](../../strongs/h/h559.md) ['iysh](../../strongs/h/h376.md) to ['ach](../../strongs/h/h251.md), We are ['ăḇāl](../../strongs/h/h61.md) ['āšēm](../../strongs/h/h818.md) concerning our ['ach](../../strongs/h/h251.md), in that we [ra'ah](../../strongs/h/h7200.md) the [tsarah](../../strongs/h/h6869.md) of his [nephesh](../../strongs/h/h5315.md), when he [chanan](../../strongs/h/h2603.md) us, and we would not [shama'](../../strongs/h/h8085.md); therefore is this [tsarah](../../strongs/h/h6869.md) [bow'](../../strongs/h/h935.md) upon us.

<a name="genesis_42_22"></a>Genesis 42:22

And [Rᵊ'ûḇēn](../../strongs/h/h7205.md) ['anah](../../strongs/h/h6030.md) them, ['āmar](../../strongs/h/h559.md), ['āmar](../../strongs/h/h559.md) I not unto you, ['āmar](../../strongs/h/h559.md), Do not [chata'](../../strongs/h/h2398.md) against the [yeleḏ](../../strongs/h/h3206.md); and ye would not [shama'](../../strongs/h/h8085.md)? therefore, behold, also his [dam](../../strongs/h/h1818.md) is [darash](../../strongs/h/h1875.md).

<a name="genesis_42_23"></a>Genesis 42:23

And they [yada'](../../strongs/h/h3045.md) not that [Yôsēp̄](../../strongs/h/h3130.md) [shama'](../../strongs/h/h8085.md) them; for he spake unto them by a [luwts](../../strongs/h/h3887.md).

<a name="genesis_42_24"></a>Genesis 42:24

And he [cabab](../../strongs/h/h5437.md) from them, and [bāḵâ](../../strongs/h/h1058.md); and [shuwb](../../strongs/h/h7725.md) to them again, and [dabar](../../strongs/h/h1696.md) with them, and [laqach](../../strongs/h/h3947.md) from them [Šimʿôn](../../strongs/h/h8095.md), and ['āsar](../../strongs/h/h631.md) him before their ['ayin](../../strongs/h/h5869.md).

<a name="genesis_42_25"></a>Genesis 42:25

Then [Yôsēp̄](../../strongs/h/h3130.md) [tsavah](../../strongs/h/h6680.md) to [mālā'](../../strongs/h/h4390.md) their [kĕliy](../../strongs/h/h3627.md) with [bar](../../strongs/h/h1250.md), and to [shuwb](../../strongs/h/h7725.md) every ['iysh](../../strongs/h/h376.md) [keceph](../../strongs/h/h3701.md) into his [śaq](../../strongs/h/h8242.md), and to [nathan](../../strongs/h/h5414.md) them [ṣêḏâ](../../strongs/h/h6720.md) for the [derek](../../strongs/h/h1870.md): and thus ['asah](../../strongs/h/h6213.md) he unto them.

<a name="genesis_42_26"></a>Genesis 42:26

And they [nasa'](../../strongs/h/h5375.md) their [chamowr](../../strongs/h/h2543.md) with the [šēḇer](../../strongs/h/h7668.md), and [yālaḵ](../../strongs/h/h3212.md) thence.

<a name="genesis_42_27"></a>Genesis 42:27

And as one of them [pāṯaḥ](../../strongs/h/h6605.md) his [śaq](../../strongs/h/h8242.md) to [nathan](../../strongs/h/h5414.md) his [chamowr](../../strongs/h/h2543.md) [mispô'](../../strongs/h/h4554.md) in the [mālôn](../../strongs/h/h4411.md), he [ra'ah](../../strongs/h/h7200.md) his [keceph](../../strongs/h/h3701.md); for, behold, it was in his ['amtaḥaṯ](../../strongs/h/h572.md) [peh](../../strongs/h/h6310.md).

<a name="genesis_42_28"></a>Genesis 42:28

And he ['āmar](../../strongs/h/h559.md) unto his ['ach](../../strongs/h/h251.md), My [keceph](../../strongs/h/h3701.md) is [shuwb](../../strongs/h/h7725.md); and, [hinneh](../../strongs/h/h2009.md), it is even in my ['amtaḥaṯ](../../strongs/h/h572.md): and their [leb](../../strongs/h/h3820.md) [yāṣā'](../../strongs/h/h3318.md) them, and they were [ḥārēḏ](../../strongs/h/h2729.md), ['āmar](../../strongs/h/h559.md) ['iysh](../../strongs/h/h376.md) to ['ach](../../strongs/h/h251.md), What is this that ['Elohiym](../../strongs/h/h430.md) hath ['asah](../../strongs/h/h6213.md) unto us?

<a name="genesis_42_29"></a>Genesis 42:29

And they [bow'](../../strongs/h/h935.md) unto [Ya'aqob](../../strongs/h/h3290.md) their ['ab](../../strongs/h/h1.md) unto the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), and [nāḡaḏ](../../strongs/h/h5046.md) him all that [qārâ](../../strongs/h/h7136.md) unto them; ['āmar](../../strongs/h/h559.md),

<a name="genesis_42_30"></a>Genesis 42:30

The ['iysh](../../strongs/h/h376.md), who is ['adown](../../strongs/h/h113.md) of the ['erets](../../strongs/h/h776.md), [dabar](../../strongs/h/h1696.md) [qāšê](../../strongs/h/h7186.md) to us, and [nathan](../../strongs/h/h5414.md) us for [ragal](../../strongs/h/h7270.md) of the ['erets](../../strongs/h/h776.md).

<a name="genesis_42_31"></a>Genesis 42:31

And we ['āmar](../../strongs/h/h559.md) unto him, We are [kēn](../../strongs/h/h3651.md) men; we are no [ragal](../../strongs/h/h7270.md):

<a name="genesis_42_32"></a>Genesis 42:32

We be twelve ['ach](../../strongs/h/h251.md), [ben](../../strongs/h/h1121.md) of our ['ab](../../strongs/h/h1.md); ['echad](../../strongs/h/h259.md) is not, and the [qāṭān](../../strongs/h/h6996.md) is this [yowm](../../strongs/h/h3117.md) with our ['ab](../../strongs/h/h1.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md).

<a name="genesis_42_33"></a>Genesis 42:33

And the ['iysh](../../strongs/h/h376.md), ['adown](../../strongs/h/h113.md) of the ['erets](../../strongs/h/h776.md), ['āmar](../../strongs/h/h559.md) unto us, Hereby shall I [yada'](../../strongs/h/h3045.md) that ye are [kēn](../../strongs/h/h3651.md) men; [yānaḥ](../../strongs/h/h3240.md) ['echad](../../strongs/h/h259.md) of your ['ach](../../strongs/h/h251.md) here with me, and [laqach](../../strongs/h/h3947.md) food for the [rᵊʿāḇôn](../../strongs/h/h7459.md) of your [bayith](../../strongs/h/h1004.md), and [yālaḵ](../../strongs/h/h3212.md):

<a name="genesis_42_34"></a>Genesis 42:34

And [bow'](../../strongs/h/h935.md) your [qāṭān](../../strongs/h/h6996.md) ['ach](../../strongs/h/h251.md) unto me: then shall I [yada'](../../strongs/h/h3045.md) that ye are no [ragal](../../strongs/h/h7270.md), but that ye are [kēn](../../strongs/h/h3651.md) men: so will I [nathan](../../strongs/h/h5414.md) you your ['ach](../../strongs/h/h251.md), and ye shall [sāḥar](../../strongs/h/h5503.md) in the ['erets](../../strongs/h/h776.md).

<a name="genesis_42_35"></a>Genesis 42:35

And it came to pass as they [rîq](../../strongs/h/h7324.md) their [śaq](../../strongs/h/h8242.md), that, behold, every ['iysh](../../strongs/h/h376.md) [ṣᵊrôr](../../strongs/h/h6872.md) of [keceph](../../strongs/h/h3701.md) was in his [śaq](../../strongs/h/h8242.md): and when both they and their ['ab](../../strongs/h/h1.md) [ra'ah](../../strongs/h/h7200.md) the [ṣᵊrôr](../../strongs/h/h6872.md) of [keceph](../../strongs/h/h3701.md), they were [yare'](../../strongs/h/h3372.md).

<a name="genesis_42_36"></a>Genesis 42:36

And [Ya'aqob](../../strongs/h/h3290.md) their ['ab](../../strongs/h/h1.md) ['āmar](../../strongs/h/h559.md) unto them, Me have ye [šāḵōl](../../strongs/h/h7921.md) of my children: [Yôsēp̄](../../strongs/h/h3130.md) is not, and [Šimʿôn](../../strongs/h/h8095.md) is not, and ye will [laqach](../../strongs/h/h3947.md) [Binyāmîn](../../strongs/h/h1144.md) away: all these things are against me.

<a name="genesis_42_37"></a>Genesis 42:37

And [Rᵊ'ûḇēn](../../strongs/h/h7205.md) ['āmar](../../strongs/h/h559.md) unto his ['ab](../../strongs/h/h1.md), ['āmar](../../strongs/h/h559.md), [muwth](../../strongs/h/h4191.md) my two [ben](../../strongs/h/h1121.md), if I [bow'](../../strongs/h/h935.md) him not to thee: [nathan](../../strongs/h/h5414.md) him into my [yad](../../strongs/h/h3027.md), and I will [shuwb](../../strongs/h/h7725.md) him to thee again.

<a name="genesis_42_38"></a>Genesis 42:38

And he ['āmar](../../strongs/h/h559.md), My [ben](../../strongs/h/h1121.md) shall not [yarad](../../strongs/h/h3381.md) with you; for his ['ach](../../strongs/h/h251.md) is [muwth](../../strongs/h/h4191.md), and he is [šā'ar](../../strongs/h/h7604.md) alone: if ['āsôn](../../strongs/h/h611.md) [qārā'](../../strongs/h/h7122.md) him by the [derek](../../strongs/h/h1870.md) in the which ye [yālaḵ](../../strongs/h/h3212.md), then shall ye [yarad](../../strongs/h/h3381.md) my [śêḇâ](../../strongs/h/h7872.md) with [yagown](../../strongs/h/h3015.md) to the [shĕ'owl](../../strongs/h/h7585.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 41](genesis_41.md) - [Genesis 43](genesis_43.md)