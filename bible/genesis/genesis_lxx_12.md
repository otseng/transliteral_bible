# [Genesis 12](https://www.blueletterbible.org/lxx/genesis/12)

<a name="genesis_12_1"></a>Genesis 12:1

And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md) to Abram, [exerchomai](../../strongs/g/g1831.md) from out of your [gē](../../strongs/g/g1093.md), and from your [syggeneia](../../strongs/g/g4772.md), and from the [oikos](../../strongs/g/g3624.md) of your [patēr](../../strongs/g/g3962.md), and [deuro](../../strongs/g/g1204.md) into the [gē](../../strongs/g/g1093.md) which ever I shall [deiknyō](../../strongs/g/g1166.md) to you.

<a name="genesis_12_2"></a>Genesis 12:2

And I will [poieō](../../strongs/g/g4160.md) you into [ethnos](../../strongs/g/g1484.md) a [megas](../../strongs/g/g3173.md), and I will [eulogeō](../../strongs/g/g2127.md) you, and I will [megalynō](../../strongs/g/g3170.md) your [onoma](../../strongs/g/g3686.md), and you will be a [eulogeō](../../strongs/g/g2127.md). 

<a name="genesis_12_3"></a>Genesis 12:3

And I will [eulogeō](../../strongs/g/g2127.md) the ones [eulogeō](../../strongs/g/g2127.md) you; and the ones [kataraomai](../../strongs/g/g2672.md) you, I will [kataraomai](../../strongs/g/g2672.md). And will be [eneulogeō](../../strongs/g/g1757.md) by you all the [phylē](../../strongs/g/g5443.md) of the [gē](../../strongs/g/g1093.md). 

<a name="genesis_12_4"></a>Genesis 12:4

And Abram [poreuō](../../strongs/g/g4198.md) just as [laleō](../../strongs/g/g2980.md) to him the [kyrios](../../strongs/g/g2962.md). And set out with him [Lōt](../../strongs/g/g3091.md). And Abram was [etos](../../strongs/g/g2094.md) [hebdomēkonta](../../strongs/g/g1440.md) [pente](../../strongs/g/g4002.md) when he [exerchomai](../../strongs/g/g1831.md) from out of [Charran](../../strongs/g/g5488.md).

<a name="genesis_12_5"></a>Genesis 12:5

And Abram [lambanō](../../strongs/g/g2983.md) Sarai his [gynē](../../strongs/g/g1135.md), and [Lōt](../../strongs/g/g3091.md) the [huios](../../strongs/g/g5207.md) of his [adelphos](../../strongs/g/g80.md), and all their [hyparchonta](../../strongs/g/g5224.md), as much as they [ktaomai](../../strongs/g/g2932.md), and every [psychē](../../strongs/g/g5590.md) which they [ktaomai](../../strongs/g/g2932.md) from out of [Charran](../../strongs/g/g5488.md). And they went [exerchomai](../../strongs/g/g1831.md) to [poreuō](../../strongs/g/g4198.md) into the [gē](../../strongs/g/g1093.md) of [Chanaan](../../strongs/g/g5477.md). And they [eiserchomai](../../strongs/g/g1525.md) into the [gē](../../strongs/g/g1093.md) of [Chanaan](../../strongs/g/g5477.md).

<a name="genesis_12_6"></a>Genesis 12:6

And Abram [diodeuō](../../strongs/g/g1353.md) the [gē](../../strongs/g/g1093.md) unto the [topos](../../strongs/g/g5117.md) of [Sychem](../../strongs/g/g4966.md) unto the [hypsēlos](../../strongs/g/g5308.md). And the [Chananaios](../../strongs/g/g5478.md) then [katoikeō](../../strongs/g/g2730.md) the [gē](../../strongs/g/g1093.md). 

<a name="genesis_12_7"></a>Genesis 12:7

And the [kyrios](../../strongs/g/g2962.md) [horaō](../../strongs/g/g3708.md) to Abram, and [eipon](../../strongs/g/g2036.md) to him, to your [sperma](../../strongs/g/g4690.md) I will [didōmi](../../strongs/g/g1325.md) this [gē](../../strongs/g/g1093.md). And [oikodomeō](../../strongs/g/g3618.md) there Abram an [thysiastērion](../../strongs/g/g2379.md) to the [kyrios](../../strongs/g/g2962.md), to the one [horaō](../../strongs/g/g3708.md) to him. 

<a name="genesis_12_8"></a>Genesis 12:8

And he [aphistēmi](../../strongs/g/g868.md) from there into the [oros](../../strongs/g/g3735.md) according to the [anatolē](../../strongs/g/g395.md) of Beth-el. And he set [histēmi](../../strongs/g/g2476.md) there his [skēnē](../../strongs/g/g4633.md) in Beth-el according to the [thalassa](../../strongs/g/g2281.md), and Hai according to the [anatolē](../../strongs/g/g395.md). And he [oikodomeō](../../strongs/g/g3618.md) there an [thysiastērion](../../strongs/g/g2379.md) to the [kyrios](../../strongs/g/g2962.md), and he [epikaleō](../../strongs/g/g1941.md) upon the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_12_9"></a>Genesis 12:9

And Abram [apairō](../../strongs/g/g522.md); and [poreuō](../../strongs/g/g4198.md) he encamped (estratopedeusen) in the [erēmos](../../strongs/g/g2048.md). 

<a name="genesis_12_10"></a>Genesis 12:10

And there was a [limos](../../strongs/g/g3042.md) upon the [gē](../../strongs/g/g1093.md). And Abram [katabainō](../../strongs/g/g2597.md) into [Aigyptos](../../strongs/g/g125.md) to [paroikeō](../../strongs/g/g3939.md) there, for [enischyō](../../strongs/g/g1765.md) the [limos](../../strongs/g/g3042.md) upon the [gē](../../strongs/g/g1093.md). 

<a name="genesis_12_11"></a>Genesis 12:11

And it was when Abram [eggizō](../../strongs/g/g1448.md) to [eiserchomai](../../strongs/g/g1525.md) into [Aigyptos](../../strongs/g/g125.md), Abram [eipon](../../strongs/g/g2036.md) to Sarai his [gynē](../../strongs/g/g1135.md), I [ginōskō](../../strongs/g/g1097.md) that [gynē](../../strongs/g/g1135.md) a good-looking (euprosōpos) you are.

<a name="genesis_12_12"></a>Genesis 12:12

It will be then as when [eidō](../../strongs/g/g1492.md) you the [Aigyptios](../../strongs/g/g124.md) they will [eipon](../../strongs/g/g2046.md) that his [gynē](../../strongs/g/g1135.md) is this one, and they will [apokteinō](../../strongs/g/g615.md) me, you and [peripoieō](../../strongs/g/g4046.md). 

<a name="genesis_12_13"></a>Genesis 12:13

[eipon](../../strongs/g/g2036.md) then that! [adelphē](../../strongs/g/g79.md) I am. so that [eu](../../strongs/g/g2095.md) to me it may become on account of you, and shall [zaō](../../strongs/g/g2198.md) my [psychē](../../strongs/g/g5590.md) because of you. 

<a name="genesis_12_14"></a>Genesis 12:14

And [ginomai](../../strongs/g/g1096.md) when Abram [eiserchomai](../../strongs/g/g1525.md) into [Aigyptos](../../strongs/g/g125.md), were [eidō](../../strongs/g/g1492.md) the [Aigyptios](../../strongs/g/g124.md) his [gynē](../../strongs/g/g1135.md), for [kalos](../../strongs/g/g2570.md) she was [sphodra](../../strongs/g/g4970.md). 

<a name="genesis_12_15"></a>Genesis 12:15

And [eidō](../../strongs/g/g1492.md) her the [archōn](../../strongs/g/g758.md) of [Pharaō](../../strongs/g/g5328.md), and [epaineō](../../strongs/g/g1867.md) her to [Pharaō](../../strongs/g/g5328.md); and they [eisagō](../../strongs/g/g1521.md) her into the [oikos](../../strongs/g/g3624.md) to [Pharaō](../../strongs/g/g5328.md).

<a name="genesis_12_16"></a>Genesis 12:16

And Abram [eu](../../strongs/g/g2095.md) they [chraomai](../../strongs/g/g5530.md) on account of her, and there [ginomai](../../strongs/g/g1096.md) to him [probaton](../../strongs/g/g4263.md), and [moschos](../../strongs/g/g3448.md), and [onos](../../strongs/g/g3688.md), and [pais](../../strongs/g/g3816.md), and [paidiskē](../../strongs/g/g3814.md), and mules (hēmionoi) and [kamēlos](../../strongs/g/g2574.md). 

<a name="genesis_12_17"></a>Genesis 12:17

And [theos](../../strongs/g/g2316.md) chastised (ētasen) [Pharaō](../../strongs/g/g5328.md) chastisements (etasmois) with [megas](../../strongs/g/g3173.md) and [ponēros](../../strongs/g/g4190.md), and his [oikos](../../strongs/g/g3624.md), on account of Sarai the [gynē](../../strongs/g/g1135.md) of Abram.

<a name="genesis_12_18"></a>Genesis 12:18

[kaleō](../../strongs/g/g2564.md) [Pharaō](../../strongs/g/g5328.md) Abram, [eipon](../../strongs/g/g2036.md), What is this you [poieō](../../strongs/g/g4160.md) to me, that you [apaggellō](../../strongs/g/g518.md) not to me that [gynē](../../strongs/g/g1135.md) she is your?

<a name="genesis_12_19"></a>Genesis 12:19

Why did you [eipon](../../strongs/g/g2036.md) that, my [adelphē](../../strongs/g/g79.md) She is; and I [lambanō](../../strongs/g/g2983.md) her to myself as [gynē](../../strongs/g/g1135.md)? And now, [idou](../../strongs/g/g2400.md), your [gynē](../../strongs/g/g1135.md) is [enantion](../../strongs/g/g1726.md) you -- [lambanō](../../strongs/g/g2983.md) run (apotreche) from me!

<a name="genesis_12_20"></a>Genesis 12:20

And [Pharaō](../../strongs/g/g5328.md) [entellō](../../strongs/g/g1781.md) the [anēr](../../strongs/g/g435.md) concerning Abram, to escort (sympropempsai) him out , and his [gynē](../../strongs/g/g1135.md), and all, as much as was his, and [Lōt](../../strongs/g/g3091.md) with him. 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 11](genesis_lxx_11.md) - [Genesis 13](genesis_lxx_13.md)