# [Genesis 18](https://www.blueletterbible.org/lxx/genesis/18)

<a name="genesis_18_1"></a>Genesis 18:1

[horaō](../../strongs/g/g3708.md) to him [theos](../../strongs/g/g2316.md) before the oak (drui) in Mamre (Mambrē), at his [kathēmai](../../strongs/g/g2521.md) near the [thyra](../../strongs/g/g2374.md) of his [skēnē](../../strongs/g/g4633.md) at [mesēmbria](../../strongs/g/g3314.md). 

<a name="genesis_18_2"></a>Genesis 18:2

And [anablepō](../../strongs/g/g308.md) his [ophthalmos](../../strongs/g/g3788.md) he [eidō](../../strongs/g/g1492.md); and [idou](../../strongs/g/g2400.md), [treis](../../strongs/g/g5140.md) [anēr](../../strongs/g/g435.md) had [histēmi](../../strongs/g/g2476.md) upon him. And [eidō](../../strongs/g/g1492.md), he ran [prostrechō](../../strongs/g/g4370.md) to [synantēsis](../../strongs/g/g4877.md) them from the [thyra](../../strongs/g/g2374.md) of his [skēnē](../../strongs/g/g4633.md). And he did [proskyneō](../../strongs/g/g4352.md) upon the [gē](../../strongs/g/g1093.md). 

<a name="genesis_18_3"></a>Genesis 18:3

And he [eipon](../../strongs/g/g2036.md), O [kyrios](../../strongs/g/g2962.md), if surely I [heuriskō](../../strongs/g/g2147.md) [charis](../../strongs/g/g5485.md) [enantion](../../strongs/g/g1726.md) you, you should not [parerchomai](../../strongs/g/g3928.md) your [pais](../../strongs/g/g3816.md). 

<a name="genesis_18_4"></a>Genesis 18:4

Let there be [lambanō](../../strongs/g/g2983.md) now [hydōr](../../strongs/g/g5204.md), and let them [niptō](../../strongs/g/g3538.md) your [pous](../../strongs/g/g4228.md), and be [katapsychō](../../strongs/g/g2711.md) under the [dendron](../../strongs/g/g1186.md)! 

<a name="genesis_18_5"></a>Genesis 18:5

And I will [lambanō](../../strongs/g/g2983.md) [artos](../../strongs/g/g740.md), and you shall [esthiō](../../strongs/g/g2068.md), and after this you shall [parerchomai](../../strongs/g/g3928.md) in your [hodos](../../strongs/g/g3598.md), because of which you [ekklinō](../../strongs/g/g1578.md) to your [pais](../../strongs/g/g3816.md). And they [eipon](../../strongs/g/g2036.md), Thus [poieō](../../strongs/g/g4160.md) as you have [eipon](../../strongs/g/g2046.md)! 

<a name="genesis_18_6"></a>Genesis 18:6

And [Abraam](../../strongs/g/g11.md) [speudō](../../strongs/g/g4692.md) unto the [skēnē](../../strongs/g/g4633.md) to [Sarra](../../strongs/g/g4564.md). And he [eipon](../../strongs/g/g2036.md) to her, [speudō](../../strongs/g/g4692.md) and mix up (phyrason) [treis](../../strongs/g/g5140.md) [metron](../../strongs/g/g3358.md) of [semidalis](../../strongs/g/g4585.md), and [poieō](../../strongs/g/g4160.md) a cake in hot ashes (enkryphias)!

<a name="genesis_18_7"></a>Genesis 18:7

And to the [bous](../../strongs/g/g1016.md) [Abraam](../../strongs/g/g11.md) [trechō](../../strongs/g/g5143.md), and he [lambanō](../../strongs/g/g2983.md) a [hapalos](../../strongs/g/g527.md) young calf (moscharion) and [kalos](../../strongs/g/g2570.md), and [didōmi](../../strongs/g/g1325.md) to the [pais](../../strongs/g/g3816.md); and he hastened (etachynen) to [poieō](../../strongs/g/g4160.md) it. 

<a name="genesis_18_8"></a>Genesis 18:8

And he [lambanō](../../strongs/g/g2983.md) butter (boutyron), and [gala](../../strongs/g/g1051.md), and the young calf (moscharion) which he [poieō](../../strongs/g/g4160.md), and [poieō](../../strongs/g/g4160.md) it [paratithēmi](../../strongs/g/g3908.md) to them, and they [esthiō](../../strongs/g/g2068.md). And he [paristēmi](../../strongs/g/g3936.md) them under the [dendron](../../strongs/g/g1186.md). 

<a name="genesis_18_9"></a>Genesis 18:9

And he [eipon](../../strongs/g/g2036.md) to him, Where is [Sarra](../../strongs/g/g4564.md) your [gynē](../../strongs/g/g1135.md)? And [apokrinomai](../../strongs/g/g611.md) he [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), in the [skēnē](../../strongs/g/g4633.md). 

<a name="genesis_18_10"></a>Genesis 18:10

And he [eipon](../../strongs/g/g2036.md), Returning (epanastrephōn), I will [hēkō](../../strongs/g/g2240.md) to you according to this [kairos](../../strongs/g/g2540.md) to the [hōra](../../strongs/g/g5610.md); and will have a [huios](../../strongs/g/g5207.md) [Sarra](../../strongs/g/g4564.md) your [gynē](../../strongs/g/g1135.md). And [Sarra](../../strongs/g/g4564.md) [akouō](../../strongs/g/g191.md) by the [thyra](../../strongs/g/g2374.md) of the [skēnē](../../strongs/g/g4633.md), being [opisthen](../../strongs/g/g3693.md) him. 

<a name="genesis_18_11"></a>Genesis 18:11

And [Abraam](../../strongs/g/g11.md) and [Sarra](../../strongs/g/g4564.md) were [presbyteros](../../strongs/g/g4245.md), [probainō](../../strongs/g/g4260.md) of [hēmera](../../strongs/g/g2250.md), [ekleipō](../../strongs/g/g1587.md) and [Sarra](../../strongs/g/g4564.md) to be in the [gynaikeios](../../strongs/g/g1134.md). 

<a name="genesis_18_12"></a>Genesis 18:12

[gelaō](../../strongs/g/g1070.md) [Sarra](../../strongs/g/g4564.md) in herself, [legō](../../strongs/g/g3004.md), For not yet has it [ginomai](../../strongs/g/g1096.md) to me until now, and my [kyrios](../../strongs/g/g2962.md) is [presbyteros](../../strongs/g/g4245.md). 

<a name="genesis_18_13"></a>Genesis 18:13

And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md) to [Abraam](../../strongs/g/g11.md), Why is it that [Sarra](../../strongs/g/g4564.md) [gelaō](../../strongs/g/g1070.md) in herself, [legō](../../strongs/g/g3004.md), Indeed is it [alēthōs](../../strongs/g/g230.md) I will [tiktō](../../strongs/g/g5088.md), and I have [gēraskō](../../strongs/g/g1095.md)? 

<a name="genesis_18_14"></a>Genesis 18:14

Is [adynateō](../../strongs/g/g101.md) to the [kyrios](../../strongs/g/g2962.md) the [rhēma](../../strongs/g/g4487.md)? At this [kairos](../../strongs/g/g2540.md) to the [hōra](../../strongs/g/g5610.md) I will [anastrephō](../../strongs/g/g390.md) to you, and there will be to [Sarra](../../strongs/g/g4564.md) a [huios](../../strongs/g/g5207.md). 

<a name="genesis_18_15"></a>Genesis 18:15

[arneomai](../../strongs/g/g720.md) [Sarra](../../strongs/g/g4564.md), [legō](../../strongs/g/g3004.md), I did not [gelaō](../../strongs/g/g1070.md); for she [phobeō](../../strongs/g/g5399.md). And he [eipon](../../strongs/g/g2036.md) to her, No, but you [gelaō](../../strongs/g/g1070.md)! 

<a name="genesis_18_16"></a>Genesis 18:16

And having [exanistēmi](../../strongs/g/g1817.md) from there, the [anēr](../../strongs/g/g435.md) looked down (kateblepsan) upon the [prosōpon](../../strongs/g/g4383.md) of [Sodoma](../../strongs/g/g4670.md) and [Gomorra](../../strongs/g/g1116.md). And [Abraam](../../strongs/g/g11.md) [symporeuomai](../../strongs/g/g4848.md) with them, escorting (sympropempōn) them. 

<a name="genesis_18_17"></a>Genesis 18:17

And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md), No way shall I [kryptō](../../strongs/g/g2928.md) from [Abraam](../../strongs/g/g11.md) my [pais](../../strongs/g/g3816.md) what I [poieō](../../strongs/g/g4160.md). 

<a name="genesis_18_18"></a>Genesis 18:18

[Abraam](../../strongs/g/g11.md) [ginomai](../../strongs/g/g1096.md), he will be made into [ethnos](../../strongs/g/g1484.md) a [megas](../../strongs/g/g3173.md) and [polys](../../strongs/g/g4183.md), and shall be [eneulogeō](../../strongs/g/g1757.md) by him all the [ethnos](../../strongs/g/g1484.md) of the [gē](../../strongs/g/g1093.md). 

<a name="genesis_18_19"></a>Genesis 18:19

For I had [eidō](../../strongs/g/g1492.md) that he will [syntassō](../../strongs/g/g4929.md) his [huios](../../strongs/g/g5207.md), and his [oikos](../../strongs/g/g3624.md) after him; and they will [phylassō](../../strongs/g/g5442.md) the [hodos](../../strongs/g/g3598.md) of the [kyrios](../../strongs/g/g2962.md), to [poieō](../../strongs/g/g4160.md) [dikaiosynē](../../strongs/g/g1343.md) and [krisis](../../strongs/g/g2920.md); that the [kyrios](../../strongs/g/g2962.md) may [epagō](../../strongs/g/g1863.md) upon [Abraam](../../strongs/g/g11.md) all as much as he [laleō](../../strongs/g/g2980.md) to him. 

<a name="genesis_18_20"></a>Genesis 18:20

[eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md), The [kraugē](../../strongs/g/g2906.md) of [Sodoma](../../strongs/g/g4670.md) and [Gomorra](../../strongs/g/g1116.md) has [plēthynō](../../strongs/g/g4129.md) towards me, and their [hamartia](../../strongs/g/g266.md) are [megas](../../strongs/g/g3173.md), [sphodra](../../strongs/g/g4970.md). 

<a name="genesis_18_21"></a>Genesis 18:21

[katabainō](../../strongs/g/g2597.md) then, I will [horaō](../../strongs/g/g3708.md) if it is according to their [kraugē](../../strongs/g/g2906.md), the [erchomai](../../strongs/g/g2064.md) to me that they [synteleō](../../strongs/g/g4931.md); and if not, that I may [ginōskō](../../strongs/g/g1097.md). 

<a name="genesis_18_22"></a>Genesis 18:22

And [apostrephō](../../strongs/g/g654.md) from there, the [anēr](../../strongs/g/g435.md) [erchomai](../../strongs/g/g2064.md) unto [Sodoma](../../strongs/g/g4670.md). And [Abraam](../../strongs/g/g11.md) was still [histēmi](../../strongs/g/g2476.md) [enantion](../../strongs/g/g1726.md) the [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_18_23"></a>Genesis 18:23

And [Abraam](../../strongs/g/g11.md) [eggizō](../../strongs/g/g1448.md), [eipon](../../strongs/g/g2036.md), You would not [synapollymi](../../strongs/g/g4881.md) the [dikaios](../../strongs/g/g1342.md) with the [asebēs](../../strongs/g/g765.md), and will be the [dikaios](../../strongs/g/g1342.md) as the [asebēs](../../strongs/g/g765.md)? 

<a name="genesis_18_24"></a>Genesis 18:24

If there might be [pentēkonta](../../strongs/g/g4004.md) [dikaios](../../strongs/g/g1342.md) in the [polis](../../strongs/g/g4172.md), will you [apollymi](../../strongs/g/g622.md) them? Will you not [aniēmi](../../strongs/g/g447.md) all the [topos](../../strongs/g/g5117.md) because of the [pentēkonta](../../strongs/g/g4004.md) [dikaios](../../strongs/g/g1342.md), if there should be so in it? 

<a name="genesis_18_25"></a>Genesis 18:25

By no [mēdamōs](../../strongs/g/g3365.md) shall you [poieō](../../strongs/g/g4160.md) as this [rhēma](../../strongs/g/g4487.md), to [apokteinō](../../strongs/g/g615.md) the [dikaios](../../strongs/g/g1342.md) with the [asebēs](../../strongs/g/g765.md), and shall be the [dikaios](../../strongs/g/g1342.md) as the [asebēs](../../strongs/g/g765.md); by no [mēdamōs](../../strongs/g/g3365.md), O one [krinō](../../strongs/g/g2919.md) all the [gē](../../strongs/g/g1093.md). Will you not [poieō](../../strongs/g/g4160.md) [krisis](../../strongs/g/g2920.md)? 

<a name="genesis_18_26"></a>Genesis 18:26

[eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md), If there should be in [Sodoma](../../strongs/g/g4670.md) [pentēkonta](../../strongs/g/g4004.md) [dikaios](../../strongs/g/g1342.md) in the [polis](../../strongs/g/g4172.md), I will [aphiēmi](../../strongs/g/g863.md) all the [topos](../../strongs/g/g5117.md) on account of them. 

<a name="genesis_18_27"></a>Genesis 18:27

And [Abraam](../../strongs/g/g11.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md), Now that I [archomai](../../strongs/g/g756.md) to [laleō](../../strongs/g/g2980.md) to my [kyrios](../../strongs/g/g2962.md), and I am [gē](../../strongs/g/g1093.md) and [spodos](../../strongs/g/g4700.md). 

<a name="genesis_18_28"></a>Genesis 18:28

But if may be [elattoneō](../../strongs/g/g1641.md) the [pentēkonta](../../strongs/g/g4004.md) [dikaios](../../strongs/g/g1342.md) to forty-five, will you [apollymi](../../strongs/g/g622.md) because of the [pente](../../strongs/g/g4002.md), all the [polis](../../strongs/g/g4172.md)? And he [eipon](../../strongs/g/g2036.md), No way will I [apollymi](../../strongs/g/g622.md) if I [heuriskō](../../strongs/g/g2147.md) there [tessarakonta](../../strongs/g/g5062.md) [pente](../../strongs/g/g4002.md).

<a name="genesis_18_29"></a>Genesis 18:29

And he [prostithēmi](../../strongs/g/g4369.md) yet to [laleō](../../strongs/g/g2980.md) to him. And he [eipon](../../strongs/g/g2036.md), But if there may be [heuriskō](../../strongs/g/g2147.md) there [tessarakonta](../../strongs/g/g5062.md)? And he [eipon](../../strongs/g/g2036.md), No way should I [apollymi](../../strongs/g/g622.md) because of the [tessarakonta](../../strongs/g/g5062.md). 

<a name="genesis_18_30"></a>Genesis 18:30

And he [eipon](../../strongs/g/g2036.md), Much less, O [kyrios](../../strongs/g/g2962.md), if I may [laleō](../../strongs/g/g2980.md), but if there may be [heuriskō](../../strongs/g/g2147.md) there [triakonta](../../strongs/g/g5144.md)? And he [eipon](../../strongs/g/g2036.md), No way will I [apollymi](../../strongs/g/g622.md) because of the [triakonta](../../strongs/g/g5144.md). 

<a name="genesis_18_31"></a>Genesis 18:31

And he [eipon](../../strongs/g/g2036.md), Since I have taken to [laleō](../../strongs/g/g2980.md) to the [kyrios](../../strongs/g/g2962.md), but if there may be [heuriskō](../../strongs/g/g2147.md) there [eikosi](../../strongs/g/g1501.md)? And he [eipon](../../strongs/g/g2036.md), No way will I [apollymi](../../strongs/g/g622.md) if I should [heuriskō](../../strongs/g/g2147.md) there [eikosi](../../strongs/g/g1501.md). 

<a name="genesis_18_32"></a>Genesis 18:32

And he [eipon](../../strongs/g/g2036.md), Much less, O [kyrios](../../strongs/g/g2962.md), if I may [laleō](../../strongs/g/g2980.md) still once more [hapax](../../strongs/g/g530.md), but if there may be [heuriskō](../../strongs/g/g2147.md) there [deka](../../strongs/g/g1176.md)? And he [eipon](../../strongs/g/g2036.md), No way will I [apollymi](../../strongs/g/g622.md) because of the [deka](../../strongs/g/g1176.md). 

<a name="genesis_18_33"></a>Genesis 18:33

[aperchomai](../../strongs/g/g565.md) the [kyrios](../../strongs/g/g2962.md) as he [pauō](../../strongs/g/g3973.md) [laleō](../../strongs/g/g2980.md) to [Abraam](../../strongs/g/g11.md), and [Abraam](../../strongs/g/g11.md) [apostrephō](../../strongs/g/g654.md) to his [topos](../../strongs/g/g5117.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 17](genesis_lxx_17.md) - [Genesis 19](genesis_lxx_19.md)