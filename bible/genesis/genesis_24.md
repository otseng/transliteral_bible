# [Genesis 24](https://www.blueletterbible.org/kjv/gen/24/1/s_24001)

<a name="genesis_24_1"></a>Genesis 24:1

And ['Abraham](../../strongs/h/h85.md) was [zāqēn](../../strongs/h/h2204.md), and [bow'](../../strongs/h/h935.md) in [yowm](../../strongs/h/h3117.md): and [Yĕhovah](../../strongs/h/h3068.md) had [barak](../../strongs/h/h1288.md) ['Abraham](../../strongs/h/h85.md) in all things.

<a name="genesis_24_2"></a>Genesis 24:2

And ['Abraham](../../strongs/h/h85.md) ['āmar](../../strongs/h/h559.md) unto his [zāqēn](../../strongs/h/h2205.md) ['ebed](../../strongs/h/h5650.md) of his [bayith](../../strongs/h/h1004.md), that [mashal](../../strongs/h/h4910.md) over all that he had, [śûm](../../strongs/h/h7760.md), I pray thee, thy [yad](../../strongs/h/h3027.md) under my [yārēḵ](../../strongs/h/h3409.md):

<a name="genesis_24_3"></a>Genesis 24:3

And I will make thee [shaba'](../../strongs/h/h7650.md) by [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [shamayim](../../strongs/h/h8064.md), and the ['Elohiym](../../strongs/h/h430.md) of the ['erets](../../strongs/h/h776.md), that thou shalt not [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) unto my [ben](../../strongs/h/h1121.md) of the [bath](../../strongs/h/h1323.md) of the [Kᵊnaʿănî](../../strongs/h/h3669.md), [qereḇ](../../strongs/h/h7130.md) whom I [yashab](../../strongs/h/h3427.md):

<a name="genesis_24_4"></a>Genesis 24:4

But thou shalt [yālaḵ](../../strongs/h/h3212.md) unto my ['erets](../../strongs/h/h776.md), and to my [môleḏeṯ](../../strongs/h/h4138.md), and [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) unto my [ben](../../strongs/h/h1121.md) [Yiṣḥāq](../../strongs/h/h3327.md).

<a name="genesis_24_5"></a>Genesis 24:5

And the ['ebed](../../strongs/h/h5650.md) ['āmar](../../strongs/h/h559.md) unto him, Peradventure the ['ishshah](../../strongs/h/h802.md) will not be ['āḇâ](../../strongs/h/h14.md) to [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) me unto this ['erets](../../strongs/h/h776.md): must I needs [shuwb](../../strongs/h/h7725.md) thy [ben](../../strongs/h/h1121.md) unto the ['erets](../../strongs/h/h776.md) from whence thou [yāṣā'](../../strongs/h/h3318.md)?

<a name="genesis_24_6"></a>Genesis 24:6

And ['Abraham](../../strongs/h/h85.md) ['āmar](../../strongs/h/h559.md) unto him, [shamar](../../strongs/h/h8104.md) thou that thou [shuwb](../../strongs/h/h7725.md) not my [ben](../../strongs/h/h1121.md) thither again.

<a name="genesis_24_7"></a>Genesis 24:7

[Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [shamayim](../../strongs/h/h8064.md), which [laqach](../../strongs/h/h3947.md) me from my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), and from the ['erets](../../strongs/h/h776.md) of my [môleḏeṯ](../../strongs/h/h4138.md), and which [dabar](../../strongs/h/h1696.md) unto me, and that [shaba'](../../strongs/h/h7650.md) unto me, ['āmar](../../strongs/h/h559.md), Unto thy [zera'](../../strongs/h/h2233.md) will I [nathan](../../strongs/h/h5414.md) this ['erets](../../strongs/h/h776.md); he shall [shalach](../../strongs/h/h7971.md) his [mal'ak](../../strongs/h/h4397.md) [paniym](../../strongs/h/h6440.md) thee, and thou shalt [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) unto my [ben](../../strongs/h/h1121.md) from thence.

<a name="genesis_24_8"></a>Genesis 24:8

And if the ['ishshah](../../strongs/h/h802.md) will not be ['āḇâ](../../strongs/h/h14.md) to [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) thee, then thou shalt be [naqah](../../strongs/h/h5352.md) from this my [šᵊḇûʿâ](../../strongs/h/h7621.md): only [shuwb](../../strongs/h/h7725.md) not my [ben](../../strongs/h/h1121.md) thither again.

<a name="genesis_24_9"></a>Genesis 24:9

And the ['ebed](../../strongs/h/h5650.md) [śûm](../../strongs/h/h7760.md) his [yad](../../strongs/h/h3027.md) under the [yārēḵ](../../strongs/h/h3409.md) of ['Abraham](../../strongs/h/h85.md) his ['adown](../../strongs/h/h113.md), and [shaba'](../../strongs/h/h7650.md) to him concerning that [dabar](../../strongs/h/h1697.md).

<a name="genesis_24_10"></a>Genesis 24:10

And the ['ebed](../../strongs/h/h5650.md) [laqach](../../strongs/h/h3947.md) ten [gāmāl](../../strongs/h/h1581.md) of the [gāmāl](../../strongs/h/h1581.md) of his ['adown](../../strongs/h/h113.md), and [yālaḵ](../../strongs/h/h3212.md); for all the [ṭûḇ](../../strongs/h/h2898.md) of his ['adown](../../strongs/h/h113.md) were in his [yad](../../strongs/h/h3027.md): and he [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) to ['Ăram nahărayim](../../strongs/h/h763.md), unto the [ʿîr](../../strongs/h/h5892.md) of [Nāḥôr](../../strongs/h/h5152.md).

<a name="genesis_24_11"></a>Genesis 24:11

And he made his [gāmāl](../../strongs/h/h1581.md) to [barak](../../strongs/h/h1288.md) [ḥûṣ](../../strongs/h/h2351.md) the [ʿîr](../../strongs/h/h5892.md) by a [bᵊ'ēr](../../strongs/h/h875.md) of [mayim](../../strongs/h/h4325.md) at the [ʿēṯ](../../strongs/h/h6256.md) of the ['ereb](../../strongs/h/h6153.md), even the [ʿēṯ](../../strongs/h/h6256.md) that [yāṣā'](../../strongs/h/h3318.md) to [šā'm](../../strongs/h/h7579.md).

<a name="genesis_24_12"></a>Genesis 24:12

And he ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of my ['adown](../../strongs/h/h113.md) ['Abraham](../../strongs/h/h85.md), I pray thee, [paniym](../../strongs/h/h6440.md) me [qārâ](../../strongs/h/h7136.md) this [yowm](../../strongs/h/h3117.md), and ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) unto my ['adown](../../strongs/h/h113.md) ['Abraham](../../strongs/h/h85.md).

<a name="genesis_24_13"></a>Genesis 24:13

Behold, I [nāṣaḇ](../../strongs/h/h5324.md) here by the ['ayin](../../strongs/h/h5869.md) of [mayim](../../strongs/h/h4325.md); and the [bath](../../strongs/h/h1323.md) of the ['enowsh](../../strongs/h/h582.md) of the [ʿîr](../../strongs/h/h5892.md) [yāṣā'](../../strongs/h/h3318.md) to [šā'm](../../strongs/h/h7579.md) [mayim](../../strongs/h/h4325.md):

<a name="genesis_24_14"></a>Genesis 24:14

And let it come to pass, that the [naʿărâ](../../strongs/h/h5291.md) to whom I shall ['āmar](../../strongs/h/h559.md), [natah](../../strongs/h/h5186.md) thy [kaḏ](../../strongs/h/h3537.md), I pray thee, that I may [šāṯâ](../../strongs/h/h8354.md); and she shall ['āmar](../../strongs/h/h559.md), [šāṯâ](../../strongs/h/h8354.md), and I will give thy [gāmāl](../../strongs/h/h1581.md) [šāqâ](../../strongs/h/h8248.md) also: let the same be she that thou hast [yakach](../../strongs/h/h3198.md) for thy ['ebed](../../strongs/h/h5650.md) [Yiṣḥāq](../../strongs/h/h3327.md); and thereby shall I [yada'](../../strongs/h/h3045.md) that thou hast ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) unto my ['adown](../../strongs/h/h113.md).

<a name="genesis_24_15"></a>Genesis 24:15

And it came to pass, before he had [kalah](../../strongs/h/h3615.md) [dabar](../../strongs/h/h1696.md), that, behold, [riḇqâ](../../strongs/h/h7259.md) [yāṣā'](../../strongs/h/h3318.md), who was [yalad](../../strongs/h/h3205.md) to [bᵊṯû'ēl](../../strongs/h/h1328.md), [ben](../../strongs/h/h1121.md) of [Milkâ](../../strongs/h/h4435.md), the ['ishshah](../../strongs/h/h802.md) of [Nāḥôr](../../strongs/h/h5152.md), ['Abraham](../../strongs/h/h85.md) ['ach](../../strongs/h/h251.md), with her [kaḏ](../../strongs/h/h3537.md) upon her [šᵊḵem](../../strongs/h/h7926.md).

<a name="genesis_24_16"></a>Genesis 24:16

And the [naʿărâ](../../strongs/h/h5291.md) was [me'od](../../strongs/h/h3966.md) [towb](../../strongs/h/h2896.md) to [mar'ê](../../strongs/h/h4758.md), a [bᵊṯûlâ](../../strongs/h/h1330.md), neither had any ['iysh](../../strongs/h/h376.md) [yada'](../../strongs/h/h3045.md) her: and she [yarad](../../strongs/h/h3381.md) to the ['ayin](../../strongs/h/h5869.md), and [mālā'](../../strongs/h/h4390.md) her [kaḏ](../../strongs/h/h3537.md), and [ʿālâ](../../strongs/h/h5927.md).

<a name="genesis_24_17"></a>Genesis 24:17

And the ['ebed](../../strongs/h/h5650.md) [rûṣ](../../strongs/h/h7323.md) to [qārā'](../../strongs/h/h7125.md) her, and ['āmar](../../strongs/h/h559.md), Let me, I pray thee, [gāmā'](../../strongs/h/h1572.md) a [mᵊʿaṭ](../../strongs/h/h4592.md) [mayim](../../strongs/h/h4325.md) of thy [kaḏ](../../strongs/h/h3537.md).

<a name="genesis_24_18"></a>Genesis 24:18

And she ['āmar](../../strongs/h/h559.md), [šāṯâ](../../strongs/h/h8354.md), my ['adown](../../strongs/h/h113.md): and she [māhar](../../strongs/h/h4116.md), and [yarad](../../strongs/h/h3381.md) her [kaḏ](../../strongs/h/h3537.md) upon her [yad](../../strongs/h/h3027.md), and gave him [šāqâ](../../strongs/h/h8248.md).

<a name="genesis_24_19"></a>Genesis 24:19

And when she had [kalah](../../strongs/h/h3615.md) giving him [šāqâ](../../strongs/h/h8248.md), she ['āmar](../../strongs/h/h559.md), I will [šā'm](../../strongs/h/h7579.md) water for thy [gāmāl](../../strongs/h/h1581.md) also, until they have done [šāṯâ](../../strongs/h/h8354.md).

<a name="genesis_24_20"></a>Genesis 24:20

And she [māhar](../../strongs/h/h4116.md), and [ʿārâ](../../strongs/h/h6168.md) her [kaḏ](../../strongs/h/h3537.md) into the [šōqeṯ](../../strongs/h/h8268.md), and [rûṣ](../../strongs/h/h7323.md) again unto the [bᵊ'ēr](../../strongs/h/h875.md) to [šā'm](../../strongs/h/h7579.md), and [šā'aḇ](../../strongs/h/h7579.md) for all his [gāmāl](../../strongs/h/h1581.md).

<a name="genesis_24_21"></a>Genesis 24:21

And the ['iysh](../../strongs/h/h376.md) [šā'â](../../strongs/h/h7583.md) at her held his [ḥāraš](../../strongs/h/h2790.md), to [yada'](../../strongs/h/h3045.md) whether [Yĕhovah](../../strongs/h/h3068.md) had made his [derek](../../strongs/h/h1870.md) [tsalach](../../strongs/h/h6743.md) or not.

<a name="genesis_24_22"></a>Genesis 24:22

And it came to pass, as the [gāmāl](../../strongs/h/h1581.md) had [kalah](../../strongs/h/h3615.md) [šāṯâ](../../strongs/h/h8354.md), that the ['iysh](../../strongs/h/h376.md) [laqach](../../strongs/h/h3947.md) a [zāhāḇ](../../strongs/h/h2091.md) [nezem](../../strongs/h/h5141.md) of [beqaʿ](../../strongs/h/h1235.md) [mišqāl](../../strongs/h/h4948.md), and two [ṣāmîḏ](../../strongs/h/h6781.md) for her [yad](../../strongs/h/h3027.md) of ten shekels [mišqāl](../../strongs/h/h4948.md) of [zāhāḇ](../../strongs/h/h2091.md);

<a name="genesis_24_23"></a>Genesis 24:23

And ['āmar](../../strongs/h/h559.md), Whose [bath](../../strongs/h/h1323.md) art thou? [nāḡaḏ](../../strongs/h/h5046.md) me, I pray thee: is there [maqowm](../../strongs/h/h4725.md) in thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) for us to [lûn](../../strongs/h/h3885.md) in?

<a name="genesis_24_24"></a>Genesis 24:24

And she ['āmar](../../strongs/h/h559.md) unto him, I am the [bath](../../strongs/h/h1323.md) of [bᵊṯû'ēl](../../strongs/h/h1328.md) the [ben](../../strongs/h/h1121.md) of [Milkâ](../../strongs/h/h4435.md), which she [yalad](../../strongs/h/h3205.md) unto [Nāḥôr](../../strongs/h/h5152.md).

<a name="genesis_24_25"></a>Genesis 24:25

She ['āmar](../../strongs/h/h559.md) moreover unto him, We have both [teḇen](../../strongs/h/h8401.md) and [mispô'](../../strongs/h/h4554.md) [rab](../../strongs/h/h7227.md), and [maqowm](../../strongs/h/h4725.md) to [lûn](../../strongs/h/h3885.md) in.

<a name="genesis_24_26"></a>Genesis 24:26

And the ['iysh](../../strongs/h/h376.md) [qāḏaḏ](../../strongs/h/h6915.md), and [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="genesis_24_27"></a>Genesis 24:27

And he ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of my ['adown](../../strongs/h/h113.md) ['Abraham](../../strongs/h/h85.md), who hath not ['azab](../../strongs/h/h5800.md) my ['adown](../../strongs/h/h113.md) of his [checed](../../strongs/h/h2617.md) and his ['emeth](../../strongs/h/h571.md): I being in the [derek](../../strongs/h/h1870.md), [Yĕhovah](../../strongs/h/h3068.md) [nachah](../../strongs/h/h5148.md) me to the [bayith](../../strongs/h/h1004.md) of my ['adown](../../strongs/h/h113.md) ['ach](../../strongs/h/h251.md).

<a name="genesis_24_28"></a>Genesis 24:28

And the [naʿărâ](../../strongs/h/h5291.md) [rûṣ](../../strongs/h/h7323.md), and [nāḡaḏ](../../strongs/h/h5046.md) them of her ['em](../../strongs/h/h517.md) [bayith](../../strongs/h/h1004.md) these [dabar](../../strongs/h/h1697.md).

<a name="genesis_24_29"></a>Genesis 24:29

And [riḇqâ](../../strongs/h/h7259.md) had an ['ach](../../strongs/h/h251.md), and his [shem](../../strongs/h/h8034.md) was [Lāḇān](../../strongs/h/h3837.md): and [Lāḇān](../../strongs/h/h3837.md) [rûṣ](../../strongs/h/h7323.md) [ḥûṣ](../../strongs/h/h2351.md) unto the ['iysh](../../strongs/h/h376.md), unto the ['ayin](../../strongs/h/h5869.md).

<a name="genesis_24_30"></a>Genesis 24:30

And it came to pass, when he [ra'ah](../../strongs/h/h7200.md) the [nezem](../../strongs/h/h5141.md) and [ṣāmîḏ](../../strongs/h/h6781.md) upon his ['āḥôṯ](../../strongs/h/h269.md) [yad](../../strongs/h/h3027.md), and when he [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [riḇqâ](../../strongs/h/h7259.md) his ['āḥôṯ](../../strongs/h/h269.md), ['āmar](../../strongs/h/h559.md), Thus [dabar](../../strongs/h/h1696.md) the ['iysh](../../strongs/h/h376.md) unto me; that he [bow'](../../strongs/h/h935.md) unto the ['iysh](../../strongs/h/h376.md); and, behold, he ['amad](../../strongs/h/h5975.md) by the [gāmāl](../../strongs/h/h1581.md) at the ['ayin](../../strongs/h/h5869.md).

<a name="genesis_24_31"></a>Genesis 24:31

And he ['āmar](../../strongs/h/h559.md), [bow'](../../strongs/h/h935.md), thou [barak](../../strongs/h/h1288.md) of [Yĕhovah](../../strongs/h/h3068.md); wherefore ['amad](../../strongs/h/h5975.md) thou [ḥûṣ](../../strongs/h/h2351.md)? for I have [panah](../../strongs/h/h6437.md) the [bayith](../../strongs/h/h1004.md), and [maqowm](../../strongs/h/h4725.md) for the [gāmāl](../../strongs/h/h1581.md).

<a name="genesis_24_32"></a>Genesis 24:32

And the ['iysh](../../strongs/h/h376.md) [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md): and he [pāṯaḥ](../../strongs/h/h6605.md) his [gāmāl](../../strongs/h/h1581.md), and [nathan](../../strongs/h/h5414.md) [teḇen](../../strongs/h/h8401.md) and [mispô'](../../strongs/h/h4554.md) for the [gāmāl](../../strongs/h/h1581.md), and [mayim](../../strongs/h/h4325.md) to [rāḥaṣ](../../strongs/h/h7364.md) his [regel](../../strongs/h/h7272.md), and the ['enowsh](../../strongs/h/h582.md) [regel](../../strongs/h/h7272.md) that were with him.

<a name="genesis_24_33"></a>Genesis 24:33

And there was [śûm](../../strongs/h/h7760.md) [yāśam](../../strongs/h/h3455.md) [paniym](../../strongs/h/h6440.md) him to ['akal](../../strongs/h/h398.md): but he ['āmar](../../strongs/h/h559.md), I will not ['akal](../../strongs/h/h398.md), until I have [dabar](../../strongs/h/h1696.md) mine [dabar](../../strongs/h/h1697.md). And he ['āmar](../../strongs/h/h559.md), [dabar](../../strongs/h/h1696.md).

<a name="genesis_24_34"></a>Genesis 24:34

And he ['āmar](../../strongs/h/h559.md), I am ['Abraham](../../strongs/h/h85.md) ['ebed](../../strongs/h/h5650.md).

<a name="genesis_24_35"></a>Genesis 24:35

And [Yĕhovah](../../strongs/h/h3068.md) hath [barak](../../strongs/h/h1288.md) my ['adown](../../strongs/h/h113.md) [me'od](../../strongs/h/h3966.md); and he is become [gāḏal](../../strongs/h/h1431.md): and he hath [nathan](../../strongs/h/h5414.md) him [tso'n](../../strongs/h/h6629.md), and [bāqār](../../strongs/h/h1241.md), and [keceph](../../strongs/h/h3701.md), and [zāhāḇ](../../strongs/h/h2091.md), and ['ebed](../../strongs/h/h5650.md), and [šip̄ḥâ](../../strongs/h/h8198.md), and [gāmāl](../../strongs/h/h1581.md), and [chamowr](../../strongs/h/h2543.md).

<a name="genesis_24_36"></a>Genesis 24:36

And [Śārâ](../../strongs/h/h8283.md) my ['adown](../../strongs/h/h113.md) ['ishshah](../../strongs/h/h802.md) [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md) to my ['adown](../../strongs/h/h113.md) ['aḥar](../../strongs/h/h310.md) she was [ziqnâ](../../strongs/h/h2209.md): and unto him hath he [nathan](../../strongs/h/h5414.md) all that he hath.

<a name="genesis_24_37"></a>Genesis 24:37

And my ['adown](../../strongs/h/h113.md) made me [shaba'](../../strongs/h/h7650.md), ['āmar](../../strongs/h/h559.md), Thou shalt not [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) to my [ben](../../strongs/h/h1121.md) of the [bath](../../strongs/h/h1323.md) of the [Kᵊnaʿănî](../../strongs/h/h3669.md), in whose ['erets](../../strongs/h/h776.md) I [yashab](../../strongs/h/h3427.md):

<a name="genesis_24_38"></a>Genesis 24:38

But thou shalt [yālaḵ](../../strongs/h/h3212.md) unto my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), and to my [mišpāḥâ](../../strongs/h/h4940.md), and [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) unto my [ben](../../strongs/h/h1121.md).

<a name="genesis_24_39"></a>Genesis 24:39

And I ['āmar](../../strongs/h/h559.md) unto my ['adown](../../strongs/h/h113.md), Peradventure the ['ishshah](../../strongs/h/h802.md) will not [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) me.

<a name="genesis_24_40"></a>Genesis 24:40

And he ['āmar](../../strongs/h/h559.md) unto me, [Yĕhovah](../../strongs/h/h3068.md), [paniym](../../strongs/h/h6440.md) whom I [halak](../../strongs/h/h1980.md), will [shalach](../../strongs/h/h7971.md) his [mal'ak](../../strongs/h/h4397.md) with thee, and [tsalach](../../strongs/h/h6743.md) thy [derek](../../strongs/h/h1870.md); and thou shalt [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) for my [ben](../../strongs/h/h1121.md) of my [mišpāḥâ](../../strongs/h/h4940.md), and of my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md):

<a name="genesis_24_41"></a>Genesis 24:41

Then shalt thou be [naqah](../../strongs/h/h5352.md) from this my ['alah](../../strongs/h/h423.md), when thou [bow'](../../strongs/h/h935.md) to my [mišpāḥâ](../../strongs/h/h4940.md); and if they [nathan](../../strongs/h/h5414.md) not thee one, thou shalt be [naqiy](../../strongs/h/h5355.md) from my ['alah](../../strongs/h/h423.md).

<a name="genesis_24_42"></a>Genesis 24:42

And I [bow'](../../strongs/h/h935.md) this [yowm](../../strongs/h/h3117.md) unto the ['ayin](../../strongs/h/h5869.md), and ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of my ['adown](../../strongs/h/h113.md) ['Abraham](../../strongs/h/h85.md), if now thou do [tsalach](../../strongs/h/h6743.md) my [derek](../../strongs/h/h1870.md) which I [halak](../../strongs/h/h1980.md):

<a name="genesis_24_43"></a>Genesis 24:43

Behold, I [nāṣaḇ](../../strongs/h/h5324.md) by the ['ayin](../../strongs/h/h5869.md) of [mayim](../../strongs/h/h4325.md); and it shall come to pass, that when the [ʿalmâ](../../strongs/h/h5959.md) [yāṣā'](../../strongs/h/h3318.md) to [šā'm](../../strongs/h/h7579.md), and I ['āmar](../../strongs/h/h559.md) to her, Give me, I pray thee, a [mᵊʿaṭ](../../strongs/h/h4592.md) [mayim](../../strongs/h/h4325.md) of thy [kaḏ](../../strongs/h/h3537.md) to [šāqâ](../../strongs/h/h8248.md);

<a name="genesis_24_44"></a>Genesis 24:44

And she ['āmar](../../strongs/h/h559.md) to me, Both [šāṯâ](../../strongs/h/h8354.md) thou, and I will also [šā'm](../../strongs/h/h7579.md) for thy [gāmāl](../../strongs/h/h1581.md): let the same be the ['ishshah](../../strongs/h/h802.md) whom [Yĕhovah](../../strongs/h/h3068.md) hath [yakach](../../strongs/h/h3198.md) for my ['adown](../../strongs/h/h113.md) [ben](../../strongs/h/h1121.md).

<a name="genesis_24_45"></a>Genesis 24:45

And before I had [kalah](../../strongs/h/h3615.md) [dabar](../../strongs/h/h1696.md) in mine [leb](../../strongs/h/h3820.md), behold, [riḇqâ](../../strongs/h/h7259.md) [yāṣā'](../../strongs/h/h3318.md) with her [kaḏ](../../strongs/h/h3537.md) on her [šᵊḵem](../../strongs/h/h7926.md); and she [yarad](../../strongs/h/h3381.md) unto the ['ayin](../../strongs/h/h5869.md), and [šā'aḇ](../../strongs/h/h7579.md): and I ['āmar](../../strongs/h/h559.md) unto her, Let me [šāqâ](../../strongs/h/h8248.md), I pray thee.

<a name="genesis_24_46"></a>Genesis 24:46

And she made [māhar](../../strongs/h/h4116.md), and [yarad](../../strongs/h/h3381.md) her [kaḏ](../../strongs/h/h3537.md) from her shoulder, and ['āmar](../../strongs/h/h559.md), [šāṯâ](../../strongs/h/h8354.md), and I will [šāqâ](../../strongs/h/h8248.md) thy [gāmāl](../../strongs/h/h1581.md) [šāqâ](../../strongs/h/h8248.md) also: so I [šāṯâ](../../strongs/h/h8354.md), and she [šāqâ](../../strongs/h/h8248.md) the [gāmāl](../../strongs/h/h1581.md) [šāqâ](../../strongs/h/h8248.md) also.

<a name="genesis_24_47"></a>Genesis 24:47

And I [sha'al](../../strongs/h/h7592.md) her, and ['āmar](../../strongs/h/h559.md), Whose [bath](../../strongs/h/h1323.md) art thou? And she ['āmar](../../strongs/h/h559.md), the [bath](../../strongs/h/h1323.md) of [bᵊṯû'ēl](../../strongs/h/h1328.md), [Nāḥôr](../../strongs/h/h5152.md) [ben](../../strongs/h/h1121.md), whom [Milkâ](../../strongs/h/h4435.md) [yalad](../../strongs/h/h3205.md) unto him: and I [śûm](../../strongs/h/h7760.md) the [nezem](../../strongs/h/h5141.md) upon her ['aph](../../strongs/h/h639.md), and the [ṣāmîḏ](../../strongs/h/h6781.md) upon her [yad](../../strongs/h/h3027.md).

<a name="genesis_24_48"></a>Genesis 24:48

And I [qāḏaḏ](../../strongs/h/h6915.md), and [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md), and [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of my ['adown](../../strongs/h/h113.md) ['Abraham](../../strongs/h/h85.md), which had [nachah](../../strongs/h/h5148.md) me in the ['emeth](../../strongs/h/h571.md) [derek](../../strongs/h/h1870.md) to [laqach](../../strongs/h/h3947.md) my ['adown](../../strongs/h/h113.md) ['ach](../../strongs/h/h251.md) [bath](../../strongs/h/h1323.md) unto his [ben](../../strongs/h/h1121.md).

<a name="genesis_24_49"></a>Genesis 24:49

And now if ye will ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md) with my ['adown](../../strongs/h/h113.md), [nāḡaḏ](../../strongs/h/h5046.md) me: and if not, [nāḡaḏ](../../strongs/h/h5046.md) me; that I may [panah](../../strongs/h/h6437.md) to the [yamiyn](../../strongs/h/h3225.md), or to the [śᵊmō'l](../../strongs/h/h8040.md).

<a name="genesis_24_50"></a>Genesis 24:50

Then [Lāḇān](../../strongs/h/h3837.md) and [bᵊṯû'ēl](../../strongs/h/h1328.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), The [dabar](../../strongs/h/h1697.md) [yāṣā'](../../strongs/h/h3318.md) from [Yĕhovah](../../strongs/h/h3068.md): we [yakol](../../strongs/h/h3201.md) [dabar](../../strongs/h/h1696.md) unto thee [ra'](../../strongs/h/h7451.md) or [towb](../../strongs/h/h2896.md).

<a name="genesis_24_51"></a>Genesis 24:51

Behold, [riḇqâ](../../strongs/h/h7259.md) is [paniym](../../strongs/h/h6440.md), [laqach](../../strongs/h/h3947.md) her, and [yālaḵ](../../strongs/h/h3212.md), and let her be thy ['adown](../../strongs/h/h113.md) [ben](../../strongs/h/h1121.md) ['ishshah](../../strongs/h/h802.md), as [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md).

<a name="genesis_24_52"></a>Genesis 24:52

And it came to pass, that, when ['Abraham](../../strongs/h/h85.md) ['ebed](../../strongs/h/h5650.md) [shama'](../../strongs/h/h8085.md) their [dabar](../../strongs/h/h1697.md), he [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md), bowing himself to the ['erets](../../strongs/h/h776.md).

<a name="genesis_24_53"></a>Genesis 24:53

And the ['ebed](../../strongs/h/h5650.md) [yāṣā'](../../strongs/h/h3318.md) [kĕliy](../../strongs/h/h3627.md) of [keceph](../../strongs/h/h3701.md), and [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md), and [beḡeḏ](../../strongs/h/h899.md), and [nathan](../../strongs/h/h5414.md) them to [riḇqâ](../../strongs/h/h7259.md): he [nathan](../../strongs/h/h5414.md) also to her ['ach](../../strongs/h/h251.md) and to her ['em](../../strongs/h/h517.md) [miḡdānôṯ](../../strongs/h/h4030.md).

<a name="genesis_24_54"></a>Genesis 24:54

And they did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), he and the ['enowsh](../../strongs/h/h582.md) that were with him, and [lûn](../../strongs/h/h3885.md); and they [quwm](../../strongs/h/h6965.md) in the [boqer](../../strongs/h/h1242.md), and he ['āmar](../../strongs/h/h559.md), [shalach](../../strongs/h/h7971.md) me away unto my ['adown](../../strongs/h/h113.md).

<a name="genesis_24_55"></a>Genesis 24:55

And her ['ach](../../strongs/h/h251.md) and her ['em](../../strongs/h/h517.md) ['āmar](../../strongs/h/h559.md), Let the [naʿărâ](../../strongs/h/h5291.md) [yashab](../../strongs/h/h3427.md) with us a few [yowm](../../strongs/h/h3117.md), at the least ten; ['aḥar](../../strongs/h/h310.md) that she shall [yālaḵ](../../strongs/h/h3212.md).

<a name="genesis_24_56"></a>Genesis 24:56

And he ['āmar](../../strongs/h/h559.md) unto them, ['āḥar](../../strongs/h/h309.md) me not, seeing [Yĕhovah](../../strongs/h/h3068.md) hath [tsalach](../../strongs/h/h6743.md) my [derek](../../strongs/h/h1870.md); [shalach](../../strongs/h/h7971.md) me away that I may [yālaḵ](../../strongs/h/h3212.md) to my ['adown](../../strongs/h/h113.md).

<a name="genesis_24_57"></a>Genesis 24:57

And they ['āmar](../../strongs/h/h559.md), We will [qara'](../../strongs/h/h7121.md) the [naʿărâ](../../strongs/h/h5291.md), and [sha'al](../../strongs/h/h7592.md) at her [peh](../../strongs/h/h6310.md).

<a name="genesis_24_58"></a>Genesis 24:58

And they [qara'](../../strongs/h/h7121.md) [riḇqâ](../../strongs/h/h7259.md), and ['āmar](../../strongs/h/h559.md) unto her, Wilt thou [yālaḵ](../../strongs/h/h3212.md) with this ['iysh](../../strongs/h/h376.md)? And she ['āmar](../../strongs/h/h559.md), I will [yālaḵ](../../strongs/h/h3212.md).

<a name="genesis_24_59"></a>Genesis 24:59

And they [shalach](../../strongs/h/h7971.md) away [riḇqâ](../../strongs/h/h7259.md) their ['āḥôṯ](../../strongs/h/h269.md), and her [yānaq](../../strongs/h/h3243.md), and ['Abraham](../../strongs/h/h85.md) ['ebed](../../strongs/h/h5650.md), and his ['enowsh](../../strongs/h/h582.md).

<a name="genesis_24_60"></a>Genesis 24:60

And they [barak](../../strongs/h/h1288.md) [riḇqâ](../../strongs/h/h7259.md), and ['āmar](../../strongs/h/h559.md) unto her, Thou art our ['āḥôṯ](../../strongs/h/h269.md), be thou the mother of ['elep̄](../../strongs/h/h505.md) of [rᵊḇāḇâ](../../strongs/h/h7233.md), and let thy [zera'](../../strongs/h/h2233.md) [yarash](../../strongs/h/h3423.md) the [sha'ar](../../strongs/h/h8179.md) of those which [sane'](../../strongs/h/h8130.md) them.

<a name="genesis_24_61"></a>Genesis 24:61

And [riḇqâ](../../strongs/h/h7259.md) [quwm](../../strongs/h/h6965.md), and her [naʿărâ](../../strongs/h/h5291.md), and they [rāḵaḇ](../../strongs/h/h7392.md) upon the [gāmāl](../../strongs/h/h1581.md), and [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) the ['iysh](../../strongs/h/h376.md): and the ['ebed](../../strongs/h/h5650.md) [laqach](../../strongs/h/h3947.md) [riḇqâ](../../strongs/h/h7259.md), and [yālaḵ](../../strongs/h/h3212.md).

<a name="genesis_24_62"></a>Genesis 24:62

And [Yiṣḥāq](../../strongs/h/h3327.md) [bow'](../../strongs/h/h935.md) from the way of [bᵊ'ēr laḥay rō'î](../../strongs/h/h883.md); for he [yashab](../../strongs/h/h3427.md) in the [neḡeḇ](../../strongs/h/h5045.md) ['erets](../../strongs/h/h776.md).

<a name="genesis_24_63"></a>Genesis 24:63

And [Yiṣḥāq](../../strongs/h/h3327.md) [yāṣā'](../../strongs/h/h3318.md) to [śûaḥ](../../strongs/h/h7742.md) in the [sadeh](../../strongs/h/h7704.md) [panah](../../strongs/h/h6437.md) ['ereb](../../strongs/h/h6153.md): and he [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md), and, behold, the [gāmāl](../../strongs/h/h1581.md) were [bow'](../../strongs/h/h935.md).

<a name="genesis_24_64"></a>Genesis 24:64

And [riḇqâ](../../strongs/h/h7259.md) [nasa'](../../strongs/h/h5375.md) her ['ayin](../../strongs/h/h5869.md), and when she [ra'ah](../../strongs/h/h7200.md) [Yiṣḥāq](../../strongs/h/h3327.md), she [naphal](../../strongs/h/h5307.md) off the [gāmāl](../../strongs/h/h1581.md).

<a name="genesis_24_65"></a>Genesis 24:65

For she had ['āmar](../../strongs/h/h559.md) unto the ['ebed](../../strongs/h/h5650.md), What ['iysh](../../strongs/h/h376.md) is this that [halak](../../strongs/h/h1980.md) in the [sadeh](../../strongs/h/h7704.md) to [qārā'](../../strongs/h/h7125.md) us? And the ['ebed](../../strongs/h/h5650.md) had ['āmar](../../strongs/h/h559.md), It is my ['adown](../../strongs/h/h113.md): therefore she [laqach](../../strongs/h/h3947.md) a [ṣāʿîp̄](../../strongs/h/h6809.md), and [kāsâ](../../strongs/h/h3680.md) herself.

<a name="genesis_24_66"></a>Genesis 24:66

And the ['ebed](../../strongs/h/h5650.md) [sāp̄ar](../../strongs/h/h5608.md) [Yiṣḥāq](../../strongs/h/h3327.md) all [dabar](../../strongs/h/h1697.md) that he had ['asah](../../strongs/h/h6213.md).

<a name="genesis_24_67"></a>Genesis 24:67

And [Yiṣḥāq](../../strongs/h/h3327.md) [bow'](../../strongs/h/h935.md) her into his ['em](../../strongs/h/h517.md) [Śārâ](../../strongs/h/h8283.md) ['ohel](../../strongs/h/h168.md), and [laqach](../../strongs/h/h3947.md) [riḇqâ](../../strongs/h/h7259.md), and she became his ['ishshah](../../strongs/h/h802.md); and he ['ahab](../../strongs/h/h157.md) her: and [Yiṣḥāq](../../strongs/h/h3327.md) was [nacham](../../strongs/h/h5162.md) ['aḥar](../../strongs/h/h310.md) his ['em](../../strongs/h/h517.md) death.

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 23](genesis_23.md) - [Genesis 25](genesis_25.md)