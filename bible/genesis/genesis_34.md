# [Genesis 34](https://www.blueletterbible.org/kjv/gen/34/1/s_34001)

<a name="genesis_34_1"></a>Genesis 34:1

And [Dînâ](../../strongs/h/h1783.md) the [bath](../../strongs/h/h1323.md) of [Lē'â](../../strongs/h/h3812.md), which she [yalad](../../strongs/h/h3205.md) unto [Ya'aqob](../../strongs/h/h3290.md), [yāṣā'](../../strongs/h/h3318.md) to [ra'ah](../../strongs/h/h7200.md) the [bath](../../strongs/h/h1323.md) of the ['erets](../../strongs/h/h776.md).

<a name="genesis_34_2"></a>Genesis 34:2

And when [Šᵊḵem](../../strongs/h/h7927.md) the [ben](../../strongs/h/h1121.md) of [ḥămôr](../../strongs/h/h2544.md) the [Ḥiûî](../../strongs/h/h2340.md), [nāśî'](../../strongs/h/h5387.md) of the ['erets](../../strongs/h/h776.md), [ra'ah](../../strongs/h/h7200.md) her, he [laqach](../../strongs/h/h3947.md) her, and [shakab](../../strongs/h/h7901.md) with her, and [ʿānâ](../../strongs/h/h6031.md) her.

<a name="genesis_34_3"></a>Genesis 34:3

And his [nephesh](../../strongs/h/h5315.md) [dāḇaq](../../strongs/h/h1692.md) unto [Dînâ](../../strongs/h/h1783.md) the [bath](../../strongs/h/h1323.md) of [Ya'aqob](../../strongs/h/h3290.md), and he ['ahab](../../strongs/h/h157.md) the [naʿărâ](../../strongs/h/h5291.md), and [dabar](../../strongs/h/h1696.md) [leb](../../strongs/h/h3820.md) unto the [naʿărâ](../../strongs/h/h5291.md).

<a name="genesis_34_4"></a>Genesis 34:4

And [Šᵊḵem](../../strongs/h/h7927.md) ['āmar](../../strongs/h/h559.md) unto his ['ab](../../strongs/h/h1.md) [ḥămôr](../../strongs/h/h2544.md), ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) me this [yaldâ](../../strongs/h/h3207.md) to ['ishshah](../../strongs/h/h802.md).

<a name="genesis_34_5"></a>Genesis 34:5

And [Ya'aqob](../../strongs/h/h3290.md) [shama'](../../strongs/h/h8085.md) that he had [ṭāmē'](../../strongs/h/h2930.md) [Dînâ](../../strongs/h/h1783.md) his [bath](../../strongs/h/h1323.md): now his [ben](../../strongs/h/h1121.md) were with his [miqnê](../../strongs/h/h4735.md) in the [sadeh](../../strongs/h/h7704.md): and [Ya'aqob](../../strongs/h/h3290.md) [ḥāraš](../../strongs/h/h2790.md) until they were [bow'](../../strongs/h/h935.md).

<a name="genesis_34_6"></a>Genesis 34:6

And [ḥămôr](../../strongs/h/h2544.md) the ['ab](../../strongs/h/h1.md) of [Šᵊḵem](../../strongs/h/h7927.md) [yāṣā'](../../strongs/h/h3318.md) unto [Ya'aqob](../../strongs/h/h3290.md) to [dabar](../../strongs/h/h1696.md) with him.

<a name="genesis_34_7"></a>Genesis 34:7

And the [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md) [bow'](../../strongs/h/h935.md) out of the [sadeh](../../strongs/h/h7704.md) when they [shama'](../../strongs/h/h8085.md) it: and the ['enowsh](../../strongs/h/h582.md) were [ʿāṣaḇ](../../strongs/h/h6087.md), and they were [me'od](../../strongs/h/h3966.md) [ḥārâ](../../strongs/h/h2734.md), because he had ['asah](../../strongs/h/h6213.md) [nᵊḇālâ](../../strongs/h/h5039.md) in [Yisra'el](../../strongs/h/h3478.md) in [shakab](../../strongs/h/h7901.md) with [Ya'aqob](../../strongs/h/h3290.md) [bath](../../strongs/h/h1323.md): which thing ought not to be ['asah](../../strongs/h/h6213.md).

<a name="genesis_34_8"></a>Genesis 34:8

And [ḥămôr](../../strongs/h/h2544.md) [dabar](../../strongs/h/h1696.md) with them, ['āmar](../../strongs/h/h559.md), The [nephesh](../../strongs/h/h5315.md) of my [ben](../../strongs/h/h1121.md) [Šᵊḵem](../../strongs/h/h7927.md) [ḥāšaq](../../strongs/h/h2836.md) for your [bath](../../strongs/h/h1323.md): I pray you [nathan](../../strongs/h/h5414.md) her him to ['ishshah](../../strongs/h/h802.md).

<a name="genesis_34_9"></a>Genesis 34:9

And make ye [ḥāṯan](../../strongs/h/h2859.md) with us, and [nathan](../../strongs/h/h5414.md) your [bath](../../strongs/h/h1323.md) unto us, and [laqach](../../strongs/h/h3947.md) our [bath](../../strongs/h/h1323.md) unto you.

<a name="genesis_34_10"></a>Genesis 34:10

And ye shall [yashab](../../strongs/h/h3427.md) with us: and the ['erets](../../strongs/h/h776.md) shall be [paniym](../../strongs/h/h6440.md) you; [yashab](../../strongs/h/h3427.md) and [sāḥar](../../strongs/h/h5503.md) ye therein, and get you ['āḥaz](../../strongs/h/h270.md) therein.

<a name="genesis_34_11"></a>Genesis 34:11

And [Šᵊḵem](../../strongs/h/h7927.md) ['āmar](../../strongs/h/h559.md) unto her ['ab](../../strongs/h/h1.md) and unto her ['ach](../../strongs/h/h251.md), Let me [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in your ['ayin](../../strongs/h/h5869.md), and what ye shall ['āmar](../../strongs/h/h559.md) unto me I will [nathan](../../strongs/h/h5414.md).

<a name="genesis_34_12"></a>Genesis 34:12

[rabah](../../strongs/h/h7235.md) me never so [me'od](../../strongs/h/h3966.md) [mōhar](../../strongs/h/h4119.md) and [matān](../../strongs/h/h4976.md), and I will [nathan](../../strongs/h/h5414.md) according as ye shall ['āmar](../../strongs/h/h559.md) unto me: but [nathan](../../strongs/h/h5414.md) me the [naʿărâ](../../strongs/h/h5291.md) to ['ishshah](../../strongs/h/h802.md).

<a name="genesis_34_13"></a>Genesis 34:13

And the [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md) ['anah](../../strongs/h/h6030.md) [Šᵊḵem](../../strongs/h/h7927.md) and [ḥămôr](../../strongs/h/h2544.md) his ['ab](../../strongs/h/h1.md) [mirmah](../../strongs/h/h4820.md), and [dabar](../../strongs/h/h1696.md), because he had [ṭāmē'](../../strongs/h/h2930.md) [Dînâ](../../strongs/h/h1783.md) their ['āḥôṯ](../../strongs/h/h269.md):

<a name="genesis_34_14"></a>Genesis 34:14

And they ['āmar](../../strongs/h/h559.md) unto them, We [yakol](../../strongs/h/h3201.md) ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md), to [nathan](../../strongs/h/h5414.md) our ['āḥôṯ](../../strongs/h/h269.md) to ['iysh](../../strongs/h/h376.md) that is [ʿārlâ](../../strongs/h/h6190.md); for that were a [cherpah](../../strongs/h/h2781.md) unto us:

<a name="genesis_34_15"></a>Genesis 34:15

But in this will we ['ôṯ](../../strongs/h/h225.md) unto you: If ye will be as we be, that every [zāḵār](../../strongs/h/h2145.md) of you be [muwl](../../strongs/h/h4135.md);

<a name="genesis_34_16"></a>Genesis 34:16

Then will we [nathan](../../strongs/h/h5414.md) our [bath](../../strongs/h/h1323.md) unto you, and we will [laqach](../../strongs/h/h3947.md) your [bath](../../strongs/h/h1323.md) to us, and we will [yashab](../../strongs/h/h3427.md) with you, and we will become ['echad](../../strongs/h/h259.md) ['am](../../strongs/h/h5971.md).

<a name="genesis_34_17"></a>Genesis 34:17

But if ye will not [shama'](../../strongs/h/h8085.md) unto us, to be [muwl](../../strongs/h/h4135.md); then will we [laqach](../../strongs/h/h3947.md) our [bath](../../strongs/h/h1323.md), and we will [halak](../../strongs/h/h1980.md).

<a name="genesis_34_18"></a>Genesis 34:18

And their [dabar](../../strongs/h/h1697.md) [yatab](../../strongs/h/h3190.md) ['ayin](../../strongs/h/h5869.md) [ḥămôr](../../strongs/h/h2544.md), and [Šᵊḵem](../../strongs/h/h7927.md) [ḥămôr](../../strongs/h/h2544.md) [ben](../../strongs/h/h1121.md).

<a name="genesis_34_19"></a>Genesis 34:19

And the [naʿar](../../strongs/h/h5288.md) ['āḥar](../../strongs/h/h309.md) not to ['asah](../../strongs/h/h6213.md) the [dabar](../../strongs/h/h1697.md), because he had [ḥāp̄ēṣ](../../strongs/h/h2654.md) in [Ya'aqob](../../strongs/h/h3290.md) [bath](../../strongs/h/h1323.md): and he was more [kabad](../../strongs/h/h3513.md) than all the [bayith](../../strongs/h/h1004.md) of his ['ab](../../strongs/h/h1.md).

<a name="genesis_34_20"></a>Genesis 34:20

And [ḥămôr](../../strongs/h/h2544.md) and [Šᵊḵem](../../strongs/h/h7927.md) his [ben](../../strongs/h/h1121.md) [bow'](../../strongs/h/h935.md) unto the [sha'ar](../../strongs/h/h8179.md) of their [ʿîr](../../strongs/h/h5892.md), and [dabar](../../strongs/h/h1696.md) with the ['enowsh](../../strongs/h/h582.md) of their [ʿîr](../../strongs/h/h5892.md), ['āmar](../../strongs/h/h559.md),

<a name="genesis_34_21"></a>Genesis 34:21

These ['enowsh](../../strongs/h/h582.md) are [šālēm](../../strongs/h/h8003.md) with us; therefore let them [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md), and [sāḥar](../../strongs/h/h5503.md) therein; for the ['erets](../../strongs/h/h776.md), behold, it is [rāḥāḇ](../../strongs/h/h7342.md) [yad](../../strongs/h/h3027.md) for [paniym](../../strongs/h/h6440.md); let us [laqach](../../strongs/h/h3947.md) their [bath](../../strongs/h/h1323.md) to us for ['ishshah](../../strongs/h/h802.md), and let us [nathan](../../strongs/h/h5414.md) them our [bath](../../strongs/h/h1323.md).

<a name="genesis_34_22"></a>Genesis 34:22

Only herein will the ['enowsh](../../strongs/h/h582.md) ['ôṯ](../../strongs/h/h225.md) unto us for to [yashab](../../strongs/h/h3427.md) with us, to be ['echad](../../strongs/h/h259.md) ['am](../../strongs/h/h5971.md), if every [zāḵār](../../strongs/h/h2145.md) among us be [muwl](../../strongs/h/h4135.md), as they are [muwl](../../strongs/h/h4135.md).

<a name="genesis_34_23"></a>Genesis 34:23

Shall not their [miqnê](../../strongs/h/h4735.md) and their [qinyān](../../strongs/h/h7075.md) and every [bĕhemah](../../strongs/h/h929.md) of their's be our's? only let us ['ôṯ](../../strongs/h/h225.md) unto them, and they will [yashab](../../strongs/h/h3427.md) with us.

<a name="genesis_34_24"></a>Genesis 34:24

And unto [ḥămôr](../../strongs/h/h2544.md) and unto [Šᵊḵem](../../strongs/h/h7927.md) his [ben](../../strongs/h/h1121.md) [shama'](../../strongs/h/h8085.md) all that [yāṣā'](../../strongs/h/h3318.md) of the [sha'ar](../../strongs/h/h8179.md) of his [ʿîr](../../strongs/h/h5892.md); and every [zāḵār](../../strongs/h/h2145.md) was [muwl](../../strongs/h/h4135.md), all that [yāṣā'](../../strongs/h/h3318.md) of the [sha'ar](../../strongs/h/h8179.md) of his [ʿîr](../../strongs/h/h5892.md).

<a name="genesis_34_25"></a>Genesis 34:25

And it came to pass on the [šᵊlîšî](../../strongs/h/h7992.md) [yowm](../../strongs/h/h3117.md), when they were [kā'aḇ](../../strongs/h/h3510.md), that two of the [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md), [Šimʿôn](../../strongs/h/h8095.md) and [Lēvî](../../strongs/h/h3878.md), [Dînâ](../../strongs/h/h1783.md) ['ach](../../strongs/h/h251.md), [laqach](../../strongs/h/h3947.md) each ['iysh](../../strongs/h/h376.md) his [chereb](../../strongs/h/h2719.md), and [bow'](../../strongs/h/h935.md) upon the [ʿîr](../../strongs/h/h5892.md) [betach](../../strongs/h/h983.md), and [harag](../../strongs/h/h2026.md) all the [zāḵār](../../strongs/h/h2145.md).

<a name="genesis_34_26"></a>Genesis 34:26

And they [harag](../../strongs/h/h2026.md) [ḥămôr](../../strongs/h/h2544.md) and [Šᵊḵem](../../strongs/h/h7927.md) his [ben](../../strongs/h/h1121.md) with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), and [laqach](../../strongs/h/h3947.md) [Dînâ](../../strongs/h/h1783.md) out of [Šᵊḵem](../../strongs/h/h7927.md) [bayith](../../strongs/h/h1004.md), and [yāṣā'](../../strongs/h/h3318.md).

<a name="genesis_34_27"></a>Genesis 34:27

The [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md) [bow'](../../strongs/h/h935.md) upon the [ḥālāl](../../strongs/h/h2491.md), and [bāzaz](../../strongs/h/h962.md) the [ʿîr](../../strongs/h/h5892.md), because they had [ṭāmē'](../../strongs/h/h2930.md) their ['āḥôṯ](../../strongs/h/h269.md).

<a name="genesis_34_28"></a>Genesis 34:28

They [laqach](../../strongs/h/h3947.md) their [tso'n](../../strongs/h/h6629.md), and their [bāqār](../../strongs/h/h1241.md), and their [chamowr](../../strongs/h/h2543.md), and that which was in the [ʿîr](../../strongs/h/h5892.md), and that which was in the [sadeh](../../strongs/h/h7704.md),

<a name="genesis_34_29"></a>Genesis 34:29

And all their [ḥayil](../../strongs/h/h2428.md), and all their [ṭap̄](../../strongs/h/h2945.md), and their ['ishshah](../../strongs/h/h802.md) [šāḇâ](../../strongs/h/h7617.md), and [bāzaz](../../strongs/h/h962.md) even all that was in the [bayith](../../strongs/h/h1004.md).

<a name="genesis_34_30"></a>Genesis 34:30

And [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md) to [Šimʿôn](../../strongs/h/h8095.md) and [Lēvî](../../strongs/h/h3878.md), Ye have [ʿāḵar](../../strongs/h/h5916.md) me to make me to [bā'aš](../../strongs/h/h887.md) among the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md), among the [Kᵊnaʿănî](../../strongs/h/h3669.md) and the [Pᵊrizzî](../../strongs/h/h6522.md): and I being [math](../../strongs/h/h4962.md) in [mispār](../../strongs/h/h4557.md), they shall ['āsap̄](../../strongs/h/h622.md) themselves against me, and [nakah](../../strongs/h/h5221.md) me; and I shall be [šāmaḏ](../../strongs/h/h8045.md), I and my [bayith](../../strongs/h/h1004.md).

<a name="genesis_34_31"></a>Genesis 34:31

And they ['āmar](../../strongs/h/h559.md), Should he ['asah](../../strongs/h/h6213.md) with our ['āḥôṯ](../../strongs/h/h269.md) as with a [zānâ](../../strongs/h/h2181.md)?

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 33](genesis_33.md) - [Genesis 35](genesis_35.md)