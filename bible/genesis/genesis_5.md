# [Genesis 5](https://www.blueletterbible.org/kjv/gen/5/1/s_5001)

<a name="genesis_5_1"></a>Genesis 5:1

This is the [sēp̄er](../../strongs/h/h5612.md) of the [towlĕdah](../../strongs/h/h8435.md) of ['Āḏām](../../strongs/h/h121.md). In the [yowm](../../strongs/h/h3117.md) that ['Elohiym](../../strongs/h/h430.md) [bara'](../../strongs/h/h1254.md) ['adam](../../strongs/h/h120.md), in the [dĕmuwth](../../strongs/h/h1823.md) of ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) he him;

<a name="genesis_5_2"></a>Genesis 5:2

[zāḵār](../../strongs/h/h2145.md) and [nᵊqēḇâ](../../strongs/h/h5347.md) [bara'](../../strongs/h/h1254.md) he them; and [barak](../../strongs/h/h1288.md) them, and [qara'](../../strongs/h/h7121.md) their [shem](../../strongs/h/h8034.md) ['adam](../../strongs/h/h120.md), in the [yowm](../../strongs/h/h3117.md) when they were [bara'](../../strongs/h/h1254.md).

<a name="genesis_5_3"></a>Genesis 5:3

And ['Āḏām](../../strongs/h/h121.md) [ḥāyâ](../../strongs/h/h2421.md) an hundred and thirty [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) in his own [dĕmuwth](../../strongs/h/h1823.md), and after his [tselem](../../strongs/h/h6754.md); and [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Šēṯ](../../strongs/h/h8352.md):

<a name="genesis_5_4"></a>Genesis 5:4

And the [yowm](../../strongs/h/h3117.md) of ['Āḏām](../../strongs/h/h121.md) ['aḥar](../../strongs/h/h310.md) he had [yalad](../../strongs/h/h3205.md) [Šēṯ](../../strongs/h/h8352.md) were eight hundred [šānâ](../../strongs/h/h8141.md): and he [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md):

<a name="genesis_5_5"></a>Genesis 5:5

And all the [yowm](../../strongs/h/h3117.md) that ['Āḏām](../../strongs/h/h121.md) [chayay](../../strongs/h/h2425.md) were nine hundred and thirty [šānâ](../../strongs/h/h8141.md): and he [muwth](../../strongs/h/h4191.md).

<a name="genesis_5_6"></a>Genesis 5:6

And [Šēṯ](../../strongs/h/h8352.md) [ḥāyâ](../../strongs/h/h2421.md) an hundred and five [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) ['Ĕnôš](../../strongs/h/h583.md):

<a name="genesis_5_7"></a>Genesis 5:7

And [Šēṯ](../../strongs/h/h8352.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) ['Ĕnôš](../../strongs/h/h583.md) eight hundred and seven [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md):

<a name="genesis_5_8"></a>Genesis 5:8

And all the [yowm](../../strongs/h/h3117.md) of [Šēṯ](../../strongs/h/h8352.md) were nine hundred and twelve [šānâ](../../strongs/h/h8141.md): and he [muwth](../../strongs/h/h4191.md).

<a name="genesis_5_9"></a>Genesis 5:9

And ['Ĕnôš](../../strongs/h/h583.md) [ḥāyâ](../../strongs/h/h2421.md) ninety [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [Qênān](../../strongs/h/h7018.md):

<a name="genesis_5_10"></a>Genesis 5:10

And ['Ĕnôš](../../strongs/h/h583.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [Qênān](../../strongs/h/h7018.md) eight hundred and fifteen [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md):

<a name="genesis_5_11"></a>Genesis 5:11

And all the [yowm](../../strongs/h/h3117.md) of ['Ĕnôš](../../strongs/h/h583.md) were nine hundred and five [šānâ](../../strongs/h/h8141.md): and he [muwth](../../strongs/h/h4191.md).

<a name="genesis_5_12"></a>Genesis 5:12

And [Qênān](../../strongs/h/h7018.md) [ḥāyâ](../../strongs/h/h2421.md) seventy [šānâ](../../strongs/h/h8141.md) and [yalad](../../strongs/h/h3205.md) [Mahălal'ēl](../../strongs/h/h4111.md):

<a name="genesis_5_13"></a>Genesis 5:13

And [Qênān](../../strongs/h/h7018.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [Mahălal'ēl](../../strongs/h/h4111.md) eight hundred and forty [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md):

<a name="genesis_5_14"></a>Genesis 5:14

And all the [yowm](../../strongs/h/h3117.md) of [Qênān](../../strongs/h/h7018.md) were nine hundred and ten [šānâ](../../strongs/h/h8141.md): and he [muwth](../../strongs/h/h4191.md).

<a name="genesis_5_15"></a>Genesis 5:15

And [Mahălal'ēl](../../strongs/h/h4111.md) [ḥāyâ](../../strongs/h/h2421.md) sixty and five [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [Yereḏ](../../strongs/h/h3382.md):

<a name="genesis_5_16"></a>Genesis 5:16

And [Mahălal'ēl](../../strongs/h/h4111.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [Yereḏ](../../strongs/h/h3382.md) eight hundred and thirty [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md):

<a name="genesis_5_17"></a>Genesis 5:17

And all the [yowm](../../strongs/h/h3117.md) of [Mahălal'ēl](../../strongs/h/h4111.md) were eight hundred ninety and five [šānâ](../../strongs/h/h8141.md): and he [muwth](../../strongs/h/h4191.md).

<a name="genesis_5_18"></a>Genesis 5:18

And [Yereḏ](../../strongs/h/h3382.md) [ḥāyâ](../../strongs/h/h2421.md) an hundred sixty and two [šānâ](../../strongs/h/h8141.md), and he [yalad](../../strongs/h/h3205.md) [Ḥănôḵ](../../strongs/h/h2585.md):

<a name="genesis_5_19"></a>Genesis 5:19

And [Yereḏ](../../strongs/h/h3382.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [Ḥănôḵ](../../strongs/h/h2585.md) eight hundred [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md):

<a name="genesis_5_20"></a>Genesis 5:20

And all the [yowm](../../strongs/h/h3117.md) of [Yereḏ](../../strongs/h/h3382.md) were nine hundred sixty and two [šānâ](../../strongs/h/h8141.md): and he [muwth](../../strongs/h/h4191.md).

<a name="genesis_5_21"></a>Genesis 5:21

And [Ḥănôḵ](../../strongs/h/h2585.md) [ḥāyâ](../../strongs/h/h2421.md) sixty and five [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [Mᵊṯûšelaḥ](../../strongs/h/h4968.md):

<a name="genesis_5_22"></a>Genesis 5:22

And [Ḥănôḵ](../../strongs/h/h2585.md) [halak](../../strongs/h/h1980.md) with ['Elohiym](../../strongs/h/h430.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [Mᵊṯûšelaḥ](../../strongs/h/h4968.md) three hundred [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md):

<a name="genesis_5_23"></a>Genesis 5:23

And all the [yowm](../../strongs/h/h3117.md) of [Ḥănôḵ](../../strongs/h/h2585.md) were three hundred sixty and five [šānâ](../../strongs/h/h8141.md):

<a name="genesis_5_24"></a>Genesis 5:24

And [Ḥănôḵ](../../strongs/h/h2585.md) [halak](../../strongs/h/h1980.md) with ['Elohiym](../../strongs/h/h430.md): and he was not; for ['Elohiym](../../strongs/h/h430.md) [laqach](../../strongs/h/h3947.md) him.

<a name="genesis_5_25"></a>Genesis 5:25

And [Mᵊṯûšelaḥ](../../strongs/h/h4968.md) [ḥāyâ](../../strongs/h/h2421.md) an hundred eighty and seven [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [Lemeḵ](../../strongs/h/h3929.md).

<a name="genesis_5_26"></a>Genesis 5:26

And [Mᵊṯûšelaḥ](../../strongs/h/h4968.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [Lemeḵ](../../strongs/h/h3929.md) seven hundred eighty and two [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md):

<a name="genesis_5_27"></a>Genesis 5:27

And all the [yowm](../../strongs/h/h3117.md) of [Mᵊṯûšelaḥ](../../strongs/h/h4968.md) were nine hundred sixty and nine [šānâ](../../strongs/h/h8141.md): and he [muwth](../../strongs/h/h4191.md).

<a name="genesis_5_28"></a>Genesis 5:28

And [Lemeḵ](../../strongs/h/h3929.md) [ḥāyâ](../../strongs/h/h2421.md) an hundred eighty and two [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md):

<a name="genesis_5_29"></a>Genesis 5:29

And he [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Nōaḥ](../../strongs/h/h5146.md), ['āmar](../../strongs/h/h559.md), This same shall [nacham](../../strongs/h/h5162.md) us concerning our [ma'aseh](../../strongs/h/h4639.md) and ['itstsabown](../../strongs/h/h6093.md) of our [yad](../../strongs/h/h3027.md), because of the ['adamah](../../strongs/h/h127.md) which [Yĕhovah](../../strongs/h/h3068.md) hath ['arar](../../strongs/h/h779.md).

<a name="genesis_5_30"></a>Genesis 5:30

And [Lemeḵ](../../strongs/h/h3929.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) he [yalad](../../strongs/h/h3205.md) [Nōaḥ](../../strongs/h/h5146.md) five hundred ninety and five [šānâ](../../strongs/h/h8141.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md):

<a name="genesis_5_31"></a>Genesis 5:31

And all the [yowm](../../strongs/h/h3117.md) of [Lemeḵ](../../strongs/h/h3929.md) were seven hundred seventy and seven [šānâ](../../strongs/h/h8141.md): and he [muwth](../../strongs/h/h4191.md).

<a name="genesis_5_32"></a>Genesis 5:32

And [Nōaḥ](../../strongs/h/h5146.md) was five hundred [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md): and [Nōaḥ](../../strongs/h/h5146.md) [yalad](../../strongs/h/h3205.md) [Šēm](../../strongs/h/h8035.md), [Ḥām](../../strongs/h/h2526.md), and [Yep̄eṯ](../../strongs/h/h3315.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 4](genesis_4.md) - [Genesis 6](genesis_6.md)