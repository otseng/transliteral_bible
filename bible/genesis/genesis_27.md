# [Genesis 27](https://www.blueletterbible.org/kjv/gen/27/1/s_27001)

<a name="genesis_27_1"></a>Genesis 27:1

And it [hayah](../../strongs/h/h1961.md), that when [Yiṣḥāq](../../strongs/h/h3327.md) was [zāqēn](../../strongs/h/h2204.md), and his ['ayin](../../strongs/h/h5869.md) were [kāhâ](../../strongs/h/h3543.md), so that he could not [ra'ah](../../strongs/h/h7200.md), he [qara'](../../strongs/h/h7121.md) [ʿĒśāv](../../strongs/h/h6215.md) his [gadowl](../../strongs/h/h1419.md) [ben](../../strongs/h/h1121.md), and ['āmar](../../strongs/h/h559.md) unto him, My [ben](../../strongs/h/h1121.md): and he ['āmar](../../strongs/h/h559.md) unto him, Behold, here am I.

<a name="genesis_27_2"></a>Genesis 27:2

And he ['āmar](../../strongs/h/h559.md), Behold now, I am [zāqēn](../../strongs/h/h2204.md), I [yada'](../../strongs/h/h3045.md) not the [yowm](../../strongs/h/h3117.md) of my [maveth](../../strongs/h/h4194.md):

<a name="genesis_27_3"></a>Genesis 27:3

Now therefore [nasa'](../../strongs/h/h5375.md), I pray thee, thy [kĕliy](../../strongs/h/h3627.md), thy [tᵊlî](../../strongs/h/h8522.md) and thy [qesheth](../../strongs/h/h7198.md), and [yāṣā'](../../strongs/h/h3318.md) to the [sadeh](../../strongs/h/h7704.md), and [ṣûḏ](../../strongs/h/h6679.md) me some [ṣayiḏ](../../strongs/h/h6718.md) [ṣêḏâ](../../strongs/h/h6720.md);

<a name="genesis_27_4"></a>Genesis 27:4

And ['asah](../../strongs/h/h6213.md) me [maṭʿām](../../strongs/h/h4303.md), such as I ['ahab](../../strongs/h/h157.md), and [bow'](../../strongs/h/h935.md) it to me, that I may ['akal](../../strongs/h/h398.md); that my [nephesh](../../strongs/h/h5315.md) may [barak](../../strongs/h/h1288.md) thee before I [muwth](../../strongs/h/h4191.md).

<a name="genesis_27_5"></a>Genesis 27:5

And [riḇqâ](../../strongs/h/h7259.md) [shama'](../../strongs/h/h8085.md) when [Yiṣḥāq](../../strongs/h/h3327.md) [dabar](../../strongs/h/h1696.md) to [ʿĒśāv](../../strongs/h/h6215.md) his [ben](../../strongs/h/h1121.md). And [ʿĒśāv](../../strongs/h/h6215.md) [yālaḵ](../../strongs/h/h3212.md) to the [sadeh](../../strongs/h/h7704.md) to [ṣûḏ](../../strongs/h/h6679.md) for [ṣayiḏ](../../strongs/h/h6718.md), and to [bow'](../../strongs/h/h935.md) it.

<a name="genesis_27_6"></a>Genesis 27:6

And [riḇqâ](../../strongs/h/h7259.md) ['āmar](../../strongs/h/h559.md) unto [Ya'aqob](../../strongs/h/h3290.md) her [ben](../../strongs/h/h1121.md), ['āmar](../../strongs/h/h559.md), Behold, I [shama'](../../strongs/h/h8085.md) thy ['ab](../../strongs/h/h1.md) [dabar](../../strongs/h/h1696.md) unto [ʿĒśāv](../../strongs/h/h6215.md) thy ['ach](../../strongs/h/h251.md), ['āmar](../../strongs/h/h559.md),

<a name="genesis_27_7"></a>Genesis 27:7

[bow'](../../strongs/h/h935.md) me [ṣayiḏ](../../strongs/h/h6718.md), and ['asah](../../strongs/h/h6213.md) me [maṭʿām](../../strongs/h/h4303.md), that I may ['akal](../../strongs/h/h398.md), and [barak](../../strongs/h/h1288.md) thee [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) before my [maveth](../../strongs/h/h4194.md).

<a name="genesis_27_8"></a>Genesis 27:8

Now therefore, my [ben](../../strongs/h/h1121.md), [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md) according to that which I [tsavah](../../strongs/h/h6680.md) thee.

<a name="genesis_27_9"></a>Genesis 27:9

[yālaḵ](../../strongs/h/h3212.md) now to the [tso'n](../../strongs/h/h6629.md), and [laqach](../../strongs/h/h3947.md) me from thence two [towb](../../strongs/h/h2896.md) [gᵊḏî](../../strongs/h/h1423.md) of the [ʿēz](../../strongs/h/h5795.md); and I will ['asah](../../strongs/h/h6213.md) them [maṭʿām](../../strongs/h/h4303.md) for thy ['ab](../../strongs/h/h1.md), such as he ['ahab](../../strongs/h/h157.md):

<a name="genesis_27_10"></a>Genesis 27:10

And thou shalt [bow'](../../strongs/h/h935.md) it to thy ['ab](../../strongs/h/h1.md), that he may ['akal](../../strongs/h/h398.md), and that he may [barak](../../strongs/h/h1288.md) thee [paniym](../../strongs/h/h6440.md) his [maveth](../../strongs/h/h4194.md).

<a name="genesis_27_11"></a>Genesis 27:11

And [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md) to [riḇqâ](../../strongs/h/h7259.md) his ['em](../../strongs/h/h517.md), Behold, [ʿĒśāv](../../strongs/h/h6215.md) my ['ach](../../strongs/h/h251.md) is a [śāʿîr](../../strongs/h/h8163.md) ['iysh](../../strongs/h/h376.md), and I am a [ḥālāq](../../strongs/h/h2509.md) ['iysh](../../strongs/h/h376.md):

<a name="genesis_27_12"></a>Genesis 27:12

My ['ab](../../strongs/h/h1.md) peradventure will [māšaš](../../strongs/h/h4959.md) me, and I shall ['ayin](../../strongs/h/h5869.md) to him as a [tāʿaʿ](../../strongs/h/h8591.md); and I shall [bow'](../../strongs/h/h935.md) a [qᵊlālâ](../../strongs/h/h7045.md) upon me, and not a [bĕrakah](../../strongs/h/h1293.md).

<a name="genesis_27_13"></a>Genesis 27:13

And his ['em](../../strongs/h/h517.md) ['āmar](../../strongs/h/h559.md) unto him, Upon me be thy [qᵊlālâ](../../strongs/h/h7045.md), my [ben](../../strongs/h/h1121.md): only [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md), and [yālaḵ](../../strongs/h/h3212.md) [laqach](../../strongs/h/h3947.md) me them.

<a name="genesis_27_14"></a>Genesis 27:14

And he [yālaḵ](../../strongs/h/h3212.md), and [laqach](../../strongs/h/h3947.md), and [bow'](../../strongs/h/h935.md) them to his ['em](../../strongs/h/h517.md): and his ['em](../../strongs/h/h517.md) ['asah](../../strongs/h/h6213.md) [maṭʿām](../../strongs/h/h4303.md), such as his ['ab](../../strongs/h/h1.md) ['ahab](../../strongs/h/h157.md).

<a name="genesis_27_15"></a>Genesis 27:15

And [riḇqâ](../../strongs/h/h7259.md) [laqach](../../strongs/h/h3947.md) [ḥemdâ](../../strongs/h/h2532.md) [beḡeḏ](../../strongs/h/h899.md) of her [gadowl](../../strongs/h/h1419.md) [ben](../../strongs/h/h1121.md) [ʿĒśāv](../../strongs/h/h6215.md), which were with her in the [bayith](../../strongs/h/h1004.md), and [labash](../../strongs/h/h3847.md) [Ya'aqob](../../strongs/h/h3290.md) her [qāṭān](../../strongs/h/h6996.md) [ben](../../strongs/h/h1121.md):

<a name="genesis_27_16"></a>Genesis 27:16

And she [labash](../../strongs/h/h3847.md) the ['owr](../../strongs/h/h5785.md) of the [gᵊḏî](../../strongs/h/h1423.md) of the [ʿēz](../../strongs/h/h5795.md) upon his [yad](../../strongs/h/h3027.md), and upon the [ḥelqâ](../../strongs/h/h2513.md) of his [ṣaûā'r](../../strongs/h/h6677.md):

<a name="genesis_27_17"></a>Genesis 27:17

And she [nathan](../../strongs/h/h5414.md) the [maṭʿām](../../strongs/h/h4303.md) and the [lechem](../../strongs/h/h3899.md), which she had ['asah](../../strongs/h/h6213.md), into the [yad](../../strongs/h/h3027.md) of her [ben](../../strongs/h/h1121.md) [Ya'aqob](../../strongs/h/h3290.md).

<a name="genesis_27_18"></a>Genesis 27:18

And he [bow'](../../strongs/h/h935.md) unto his ['ab](../../strongs/h/h1.md), and ['āmar](../../strongs/h/h559.md), My ['ab](../../strongs/h/h1.md): and he ['āmar](../../strongs/h/h559.md), Here am I; who art thou, my [ben](../../strongs/h/h1121.md)?

<a name="genesis_27_19"></a>Genesis 27:19

And [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md) unto his ['ab](../../strongs/h/h1.md), I am [ʿĒśāv](../../strongs/h/h6215.md) thy [bᵊḵôr](../../strongs/h/h1060.md); I have ['asah](../../strongs/h/h6213.md) according as thou [dabar](../../strongs/h/h1696.md) me: [quwm](../../strongs/h/h6965.md), I pray thee, [yashab](../../strongs/h/h3427.md) and ['akal](../../strongs/h/h398.md) of my [ṣayiḏ](../../strongs/h/h6718.md), that thy [nephesh](../../strongs/h/h5315.md) may [barak](../../strongs/h/h1288.md) me.

<a name="genesis_27_20"></a>Genesis 27:20

And [Yiṣḥāq](../../strongs/h/h3327.md) ['āmar](../../strongs/h/h559.md) unto his [ben](../../strongs/h/h1121.md), How is it that thou hast [māṣā'](../../strongs/h/h4672.md) it so [māhar](../../strongs/h/h4116.md), my [ben](../../strongs/h/h1121.md)? And he ['āmar](../../strongs/h/h559.md), Because [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [qārâ](../../strongs/h/h7136.md) it [paniym](../../strongs/h/h6440.md).

<a name="genesis_27_21"></a>Genesis 27:21

And [Yiṣḥāq](../../strongs/h/h3327.md) ['āmar](../../strongs/h/h559.md) unto [Ya'aqob](../../strongs/h/h3290.md), [nāḡaš](../../strongs/h/h5066.md), I pray thee, that I may [mûš](../../strongs/h/h4184.md) thee, my [ben](../../strongs/h/h1121.md), whether thou be my very [ben](../../strongs/h/h1121.md) [ʿĒśāv](../../strongs/h/h6215.md) or not.

<a name="genesis_27_22"></a>Genesis 27:22

And [Ya'aqob](../../strongs/h/h3290.md) [nāḡaš](../../strongs/h/h5066.md) unto [Yiṣḥāq](../../strongs/h/h3327.md) his ['ab](../../strongs/h/h1.md); and he [māšaš](../../strongs/h/h4959.md) him, and ['āmar](../../strongs/h/h559.md), The [qowl](../../strongs/h/h6963.md) is [Ya'aqob](../../strongs/h/h3290.md) [qowl](../../strongs/h/h6963.md), but the [yad](../../strongs/h/h3027.md) are the [yad](../../strongs/h/h3027.md) of [ʿĒśāv](../../strongs/h/h6215.md).

<a name="genesis_27_23"></a>Genesis 27:23

And he [nāḵar](../../strongs/h/h5234.md) him not, because his [yad](../../strongs/h/h3027.md) were [śāʿîr](../../strongs/h/h8163.md), as his ['ach](../../strongs/h/h251.md) [ʿĒśāv](../../strongs/h/h6215.md) [yad](../../strongs/h/h3027.md): so he [barak](../../strongs/h/h1288.md) him.

<a name="genesis_27_24"></a>Genesis 27:24

And he ['āmar](../../strongs/h/h559.md), Art thou my very [ben](../../strongs/h/h1121.md) [ʿĒśāv](../../strongs/h/h6215.md)? And he ['āmar](../../strongs/h/h559.md), I am.

<a name="genesis_27_25"></a>Genesis 27:25

And he ['āmar](../../strongs/h/h559.md), [nāḡaš](../../strongs/h/h5066.md) to me, and I will ['akal](../../strongs/h/h398.md) of my [ben](../../strongs/h/h1121.md) [ṣayiḏ](../../strongs/h/h6718.md), that my [nephesh](../../strongs/h/h5315.md) may [barak](../../strongs/h/h1288.md) thee. And he [nāḡaš](../../strongs/h/h5066.md) to him, and he did ['akal](../../strongs/h/h398.md): and he [bow'](../../strongs/h/h935.md) him [yayin](../../strongs/h/h3196.md) and he [šāṯâ](../../strongs/h/h8354.md).

<a name="genesis_27_26"></a>Genesis 27:26

And his ['ab](../../strongs/h/h1.md) [Yiṣḥāq](../../strongs/h/h3327.md) ['āmar](../../strongs/h/h559.md) unto him, [nāḡaš](../../strongs/h/h5066.md) now, and [nashaq](../../strongs/h/h5401.md) me, my [ben](../../strongs/h/h1121.md).

<a name="genesis_27_27"></a>Genesis 27:27

And he [nāḡaš](../../strongs/h/h5066.md), and [nashaq](../../strongs/h/h5401.md) him: and he [rîaḥ](../../strongs/h/h7306.md) the [rêaḥ](../../strongs/h/h7381.md) of his [beḡeḏ](../../strongs/h/h899.md), and [barak](../../strongs/h/h1288.md) him, and ['āmar](../../strongs/h/h559.md), [ra'ah](../../strongs/h/h7200.md), the [rêaḥ](../../strongs/h/h7381.md) of my [ben](../../strongs/h/h1121.md) is as the [rêaḥ](../../strongs/h/h7381.md) of a [sadeh](../../strongs/h/h7704.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [barak](../../strongs/h/h1288.md):

<a name="genesis_27_28"></a>Genesis 27:28

Therefore ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee of the [ṭal](../../strongs/h/h2919.md) of [shamayim](../../strongs/h/h8064.md), and the [mišmān](../../strongs/h/h4924.md) of the ['erets](../../strongs/h/h776.md), and [rōḇ](../../strongs/h/h7230.md) of [dagan](../../strongs/h/h1715.md) and [tiyrowsh](../../strongs/h/h8492.md):

<a name="genesis_27_29"></a>Genesis 27:29

Let ['am](../../strongs/h/h5971.md) ['abad](../../strongs/h/h5647.md) thee, and [lĕom](../../strongs/h/h3816.md) [shachah](../../strongs/h/h7812.md) to thee: be [gᵊḇîr](../../strongs/h/h1376.md) over thy ['ach](../../strongs/h/h251.md), and let thy ['em](../../strongs/h/h517.md) [ben](../../strongs/h/h1121.md) [shachah](../../strongs/h/h7812.md) to thee: ['arar](../../strongs/h/h779.md) be every one that ['arar](../../strongs/h/h779.md) thee, and [barak](../../strongs/h/h1288.md) be he that [barak](../../strongs/h/h1288.md) thee.

<a name="genesis_27_30"></a>Genesis 27:30

And it came to pass, as soon as [Yiṣḥāq](../../strongs/h/h3327.md) had made a [kalah](../../strongs/h/h3615.md) of [barak](../../strongs/h/h1288.md) [Ya'aqob](../../strongs/h/h3290.md), and [Ya'aqob](../../strongs/h/h3290.md) was yet [yāṣā'](../../strongs/h/h3318.md) [yāṣā'](../../strongs/h/h3318.md) from the [paniym](../../strongs/h/h6440.md) of [Yiṣḥāq](../../strongs/h/h3327.md) his ['ab](../../strongs/h/h1.md), that [ʿĒśāv](../../strongs/h/h6215.md) his ['ach](../../strongs/h/h251.md) [bow'](../../strongs/h/h935.md) from his [ṣayiḏ](../../strongs/h/h6718.md).

<a name="genesis_27_31"></a>Genesis 27:31

And he also had ['asah](../../strongs/h/h6213.md) [maṭʿām](../../strongs/h/h4303.md), and [bow'](../../strongs/h/h935.md) it unto his ['ab](../../strongs/h/h1.md), and ['āmar](../../strongs/h/h559.md) unto his ['ab](../../strongs/h/h1.md), Let my ['ab](../../strongs/h/h1.md) [quwm](../../strongs/h/h6965.md), and ['akal](../../strongs/h/h398.md) of his [ben](../../strongs/h/h1121.md) [ṣayiḏ](../../strongs/h/h6718.md), that thy [nephesh](../../strongs/h/h5315.md) may [barak](../../strongs/h/h1288.md) me.

<a name="genesis_27_32"></a>Genesis 27:32

And [Yiṣḥāq](../../strongs/h/h3327.md) his ['ab](../../strongs/h/h1.md) ['āmar](../../strongs/h/h559.md) unto him, Who art thou? And he ['āmar](../../strongs/h/h559.md), I am thy [ben](../../strongs/h/h1121.md), thy [bᵊḵôr](../../strongs/h/h1060.md) [ʿĒśāv](../../strongs/h/h6215.md).

<a name="genesis_27_33"></a>Genesis 27:33

And [Yiṣḥāq](../../strongs/h/h3327.md) [ḥārēḏ](../../strongs/h/h2729.md) [gadowl](../../strongs/h/h1419.md) [me'od](../../strongs/h/h3966.md) [ḥărāḏâ](../../strongs/h/h2731.md), and ['āmar](../../strongs/h/h559.md), Who? where is he that hath [ṣûḏ](../../strongs/h/h6679.md) [ṣayiḏ](../../strongs/h/h6718.md), and [bow'](../../strongs/h/h935.md) it me, and I have ['akal](../../strongs/h/h398.md) of all before thou [bow'](../../strongs/h/h935.md), and have [barak](../../strongs/h/h1288.md) him? yea, and he shall be [barak](../../strongs/h/h1288.md).

<a name="genesis_27_34"></a>Genesis 27:34

And when [ʿĒśāv](../../strongs/h/h6215.md) [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of his ['ab](../../strongs/h/h1.md), he [ṣāʿaq](../../strongs/h/h6817.md) with a [gadowl](../../strongs/h/h1419.md) and [me'od](../../strongs/h/h3966.md) [mar](../../strongs/h/h4751.md) [tsa'aqah](../../strongs/h/h6818.md), and ['āmar](../../strongs/h/h559.md) unto his ['ab](../../strongs/h/h1.md), [barak](../../strongs/h/h1288.md) me, even me also, O my ['ab](../../strongs/h/h1.md).

<a name="genesis_27_35"></a>Genesis 27:35

And he ['āmar](../../strongs/h/h559.md), Thy ['ach](../../strongs/h/h251.md) [bow'](../../strongs/h/h935.md) with [mirmah](../../strongs/h/h4820.md), and hath [laqach](../../strongs/h/h3947.md) thy [bĕrakah](../../strongs/h/h1293.md).

<a name="genesis_27_36"></a>Genesis 27:36

And he ['āmar](../../strongs/h/h559.md), Is not he rightly [qara'](../../strongs/h/h7121.md) [shem](../../strongs/h/h8034.md) [Ya'aqob](../../strongs/h/h3290.md)? for he hath [ʿāqaḇ](../../strongs/h/h6117.md) me these two [pa'am](../../strongs/h/h6471.md): he [laqach](../../strongs/h/h3947.md) my [bᵊḵôrâ](../../strongs/h/h1062.md); and, behold, now he hath [laqach](../../strongs/h/h3947.md) my [bĕrakah](../../strongs/h/h1293.md). And he ['āmar](../../strongs/h/h559.md), Hast thou not ['āṣal](../../strongs/h/h680.md) a [bĕrakah](../../strongs/h/h1293.md) for me?

<a name="genesis_27_37"></a>Genesis 27:37

And [Yiṣḥāq](../../strongs/h/h3327.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto [ʿĒśāv](../../strongs/h/h6215.md), Behold, I have [śûm](../../strongs/h/h7760.md) him thy [gᵊḇîr](../../strongs/h/h1376.md), and all his ['ach](../../strongs/h/h251.md) have I [nathan](../../strongs/h/h5414.md) to him for ['ebed](../../strongs/h/h5650.md); and with [dagan](../../strongs/h/h1715.md) and [tiyrowsh](../../strongs/h/h8492.md) have I [camak](../../strongs/h/h5564.md) him: and what shall I ['asah](../../strongs/h/h6213.md) ['ēp̄ô](../../strongs/h/h645.md) unto thee, my [ben](../../strongs/h/h1121.md)?

<a name="genesis_27_38"></a>Genesis 27:38

And [ʿĒśāv](../../strongs/h/h6215.md) ['āmar](../../strongs/h/h559.md) unto his ['ab](../../strongs/h/h1.md), Hast thou but one [bĕrakah](../../strongs/h/h1293.md), my ['ab](../../strongs/h/h1.md)? [barak](../../strongs/h/h1288.md) me, even me also, O my ['ab](../../strongs/h/h1.md). And [ʿĒśāv](../../strongs/h/h6215.md) [nasa'](../../strongs/h/h5375.md) his [qowl](../../strongs/h/h6963.md), and [bāḵâ](../../strongs/h/h1058.md).

<a name="genesis_27_39"></a>Genesis 27:39

And [Yiṣḥāq](../../strongs/h/h3327.md) his ['ab](../../strongs/h/h1.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto him, Behold, thy [môšāḇ](../../strongs/h/h4186.md) shall be the [mišmān](../../strongs/h/h4924.md) of the ['erets](../../strongs/h/h776.md), and of the [ṭal](../../strongs/h/h2919.md) of [shamayim](../../strongs/h/h8064.md) from [ʿal](../../strongs/h/h5920.md);

<a name="genesis_27_40"></a>Genesis 27:40

And by thy [chereb](../../strongs/h/h2719.md) shalt thou [ḥāyâ](../../strongs/h/h2421.md), and shalt ['abad](../../strongs/h/h5647.md) thy ['ach](../../strongs/h/h251.md); and it shall come to pass when thou shalt have the [rûḏ](../../strongs/h/h7300.md), that thou shalt [paraq](../../strongs/h/h6561.md) his [ʿōl](../../strongs/h/h5923.md) from off thy [ṣaûā'r](../../strongs/h/h6677.md).

<a name="genesis_27_41"></a>Genesis 27:41

And [ʿĒśāv](../../strongs/h/h6215.md) [śāṭam](../../strongs/h/h7852.md) [Ya'aqob](../../strongs/h/h3290.md) because of the [bĕrakah](../../strongs/h/h1293.md) wherewith his ['ab](../../strongs/h/h1.md) [barak](../../strongs/h/h1288.md) him: and [ʿĒśāv](../../strongs/h/h6215.md) ['āmar](../../strongs/h/h559.md) in his [leb](../../strongs/h/h3820.md), The [yowm](../../strongs/h/h3117.md) of ['ēḇel](../../strongs/h/h60.md) for my ['ab](../../strongs/h/h1.md) are at [qāraḇ](../../strongs/h/h7126.md); then will I [harag](../../strongs/h/h2026.md) my ['ach](../../strongs/h/h251.md) [Ya'aqob](../../strongs/h/h3290.md).

<a name="genesis_27_42"></a>Genesis 27:42

And these [dabar](../../strongs/h/h1697.md) of [ʿĒśāv](../../strongs/h/h6215.md) her [gadowl](../../strongs/h/h1419.md) [ben](../../strongs/h/h1121.md) were [nāḡaḏ](../../strongs/h/h5046.md) to [riḇqâ](../../strongs/h/h7259.md): and she [shalach](../../strongs/h/h7971.md) and [qara'](../../strongs/h/h7121.md) [Ya'aqob](../../strongs/h/h3290.md) her [qāṭān](../../strongs/h/h6996.md) [ben](../../strongs/h/h1121.md), and ['āmar](../../strongs/h/h559.md) unto him, Behold, thy ['ach](../../strongs/h/h251.md) [ʿĒśāv](../../strongs/h/h6215.md), as touching thee, doth [nacham](../../strongs/h/h5162.md) himself, purposing to [harag](../../strongs/h/h2026.md) thee.

<a name="genesis_27_43"></a>Genesis 27:43

Now therefore, my [ben](../../strongs/h/h1121.md), [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md); [quwm](../../strongs/h/h6965.md), [bāraḥ](../../strongs/h/h1272.md) thou to [Lāḇān](../../strongs/h/h3837.md) my ['ach](../../strongs/h/h251.md) to [Ḥārān](../../strongs/h/h2771.md);

<a name="genesis_27_44"></a>Genesis 27:44

And [yashab](../../strongs/h/h3427.md) with him an ['echad](../../strongs/h/h259.md) [yowm](../../strongs/h/h3117.md), until thy ['ach](../../strongs/h/h251.md) [chemah](../../strongs/h/h2534.md) [shuwb](../../strongs/h/h7725.md);

<a name="genesis_27_45"></a>Genesis 27:45

Until thy ['ach](../../strongs/h/h251.md) ['aph](../../strongs/h/h639.md) [shuwb](../../strongs/h/h7725.md) from thee, and he [shakach](../../strongs/h/h7911.md) that which thou hast ['asah](../../strongs/h/h6213.md) to him: then I will [shalach](../../strongs/h/h7971.md), and [laqach](../../strongs/h/h3947.md) thee from thence: why should I be [šāḵōl](../../strongs/h/h7921.md) also of you both in one [yowm](../../strongs/h/h3117.md)?

<a name="genesis_27_46"></a>Genesis 27:46

And [riḇqâ](../../strongs/h/h7259.md) ['āmar](../../strongs/h/h559.md) to [Yiṣḥāq](../../strongs/h/h3327.md), I am [qûṣ](../../strongs/h/h6973.md) of my [chay](../../strongs/h/h2416.md) [paniym](../../strongs/h/h6440.md) of the [bath](../../strongs/h/h1323.md) of [Ḥēṯ](../../strongs/h/h2845.md): if [Ya'aqob](../../strongs/h/h3290.md) [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) of the [bath](../../strongs/h/h1323.md) of [Ḥēṯ](../../strongs/h/h2845.md), such as these which are of the [bath](../../strongs/h/h1323.md) of the ['erets](../../strongs/h/h776.md), [mah](../../strongs/h/h4100.md) shall my [chay](../../strongs/h/h2416.md) do me?

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 26](genesis_26.md) - [Genesis 28](genesis_28.md)