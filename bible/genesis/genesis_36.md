# [Genesis 36](https://www.blueletterbible.org/kjv/gen/36/1/s_36001)

<a name="genesis_36_1"></a>Genesis 36:1

Now these are the [towlĕdah](../../strongs/h/h8435.md) of [ʿĒśāv](../../strongs/h/h6215.md), who is ['Ĕḏōm](../../strongs/h/h123.md).

<a name="genesis_36_2"></a>Genesis 36:2

[ʿĒśāv](../../strongs/h/h6215.md) [laqach](../../strongs/h/h3947.md) his ['ishshah](../../strongs/h/h802.md) of the [bath](../../strongs/h/h1323.md) of [Kĕna'an](../../strongs/h/h3667.md); [ʿāḏâ](../../strongs/h/h5711.md) the [bath](../../strongs/h/h1323.md) of ['êlôn](../../strongs/h/h356.md) the [Ḥitî](../../strongs/h/h2850.md), and ['Āhŏlîḇāmâ](../../strongs/h/h173.md) the [bath](../../strongs/h/h1323.md) of [ʿĂnâ](../../strongs/h/h6034.md) the [bath](../../strongs/h/h1323.md) of [Ṣiḇʿôn](../../strongs/h/h6649.md) the [Ḥiûî](../../strongs/h/h2340.md);

<a name="genesis_36_3"></a>Genesis 36:3

And [bāśmaṯ](../../strongs/h/h1315.md) [Yišmāʿē'l](../../strongs/h/h3458.md) [bath](../../strongs/h/h1323.md), ['āḥôṯ](../../strongs/h/h269.md) of [Nᵊḇāyôṯ](../../strongs/h/h5032.md).

<a name="genesis_36_4"></a>Genesis 36:4

And [ʿāḏâ](../../strongs/h/h5711.md) [yalad](../../strongs/h/h3205.md) to [ʿĒśāv](../../strongs/h/h6215.md) ['Ĕlîp̄az](../../strongs/h/h464.md); and [bāśmaṯ](../../strongs/h/h1315.md) [yalad](../../strongs/h/h3205.md) [Rᵊʿû'ēl](../../strongs/h/h7467.md);

<a name="genesis_36_5"></a>Genesis 36:5

And ['Āhŏlîḇāmâ](../../strongs/h/h173.md) [yalad](../../strongs/h/h3205.md) [yᵊʿîš](../../strongs/h/h3274.md), and [Yaʿlām](../../strongs/h/h3281.md), and [Qōraḥ](../../strongs/h/h7141.md): these are the [ben](../../strongs/h/h1121.md) of [ʿĒśāv](../../strongs/h/h6215.md), which were [yalad](../../strongs/h/h3205.md) unto him in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md).

<a name="genesis_36_6"></a>Genesis 36:6

And [ʿĒśāv](../../strongs/h/h6215.md) [laqach](../../strongs/h/h3947.md) his ['ishshah](../../strongs/h/h802.md), and his [ben](../../strongs/h/h1121.md), and his [bath](../../strongs/h/h1323.md), and all the [nephesh](../../strongs/h/h5315.md) of his [bayith](../../strongs/h/h1004.md), and his [miqnê](../../strongs/h/h4735.md), and all his [bĕhemah](../../strongs/h/h929.md), and all his [qinyān](../../strongs/h/h7075.md), which he had [rāḵaš](../../strongs/h/h7408.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md); and [yālaḵ](../../strongs/h/h3212.md) into the ['erets](../../strongs/h/h776.md) from the [paniym](../../strongs/h/h6440.md) of his ['ach](../../strongs/h/h251.md) [Ya'aqob](../../strongs/h/h3290.md).

<a name="genesis_36_7"></a>Genesis 36:7

For their [rᵊḵûš](../../strongs/h/h7399.md) were [rab](../../strongs/h/h7227.md) than that they might [yashab](../../strongs/h/h3427.md) [yaḥaḏ](../../strongs/h/h3162.md); and the ['erets](../../strongs/h/h776.md) wherein they were [māḡûr](../../strongs/h/h4033.md) [yakol](../../strongs/h/h3201.md) not [nasa'](../../strongs/h/h5375.md) them [paniym](../../strongs/h/h6440.md) of their [miqnê](../../strongs/h/h4735.md).

<a name="genesis_36_8"></a>Genesis 36:8

Thus [yashab](../../strongs/h/h3427.md) [ʿĒśāv](../../strongs/h/h6215.md) in [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md): [ʿĒśāv](../../strongs/h/h6215.md) is ['Ĕḏōm](../../strongs/h/h123.md).

<a name="genesis_36_9"></a>Genesis 36:9

And these are the [towlĕdah](../../strongs/h/h8435.md) of [ʿĒśāv](../../strongs/h/h6215.md) the ['ab](../../strongs/h/h1.md) of the ['Ĕḏōm](../../strongs/h/h123.md) in [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md):

<a name="genesis_36_10"></a>Genesis 36:10

These are the [shem](../../strongs/h/h8034.md) of [ʿĒśāv](../../strongs/h/h6215.md) [ben](../../strongs/h/h1121.md); ['Ĕlîp̄az](../../strongs/h/h464.md) the [ben](../../strongs/h/h1121.md) of [ʿāḏâ](../../strongs/h/h5711.md) the ['ishshah](../../strongs/h/h802.md) of [ʿĒśāv](../../strongs/h/h6215.md), [Rᵊʿû'ēl](../../strongs/h/h7467.md) the [ben](../../strongs/h/h1121.md) of [bāśmaṯ](../../strongs/h/h1315.md) the ['ishshah](../../strongs/h/h802.md) of [ʿĒśāv](../../strongs/h/h6215.md).

<a name="genesis_36_11"></a>Genesis 36:11

And the [ben](../../strongs/h/h1121.md) of ['Ĕlîp̄az](../../strongs/h/h464.md) were [Têmān](../../strongs/h/h8487.md), ['Ômār](../../strongs/h/h201.md), [ṣᵊp̄ô](../../strongs/h/h6825.md), and [Gaʿtām](../../strongs/h/h1609.md), and [Qᵊnaz](../../strongs/h/h7073.md).

<a name="genesis_36_12"></a>Genesis 36:12

And [Timnāʿ](../../strongs/h/h8555.md) was [pîleḡeš](../../strongs/h/h6370.md) to ['Ĕlîp̄az](../../strongs/h/h464.md) [ʿĒśāv](../../strongs/h/h6215.md) [ben](../../strongs/h/h1121.md); and she [yalad](../../strongs/h/h3205.md) to ['Ĕlîp̄az](../../strongs/h/h464.md) [ʿĂmālēq](../../strongs/h/h6002.md): these were the [ben](../../strongs/h/h1121.md) of [ʿāḏâ](../../strongs/h/h5711.md) [ʿĒśāv](../../strongs/h/h6215.md) ['ishshah](../../strongs/h/h802.md).

<a name="genesis_36_13"></a>Genesis 36:13

And these are the [ben](../../strongs/h/h1121.md) of [Rᵊʿû'ēl](../../strongs/h/h7467.md); [Naḥaṯ](../../strongs/h/h5184.md), and [Zeraḥ](../../strongs/h/h2226.md), [Šammâ](../../strongs/h/h8048.md), and [Mizzê](../../strongs/h/h4199.md): these were the [ben](../../strongs/h/h1121.md) of [bāśmaṯ](../../strongs/h/h1315.md) [ʿĒśāv](../../strongs/h/h6215.md) ['ishshah](../../strongs/h/h802.md).

<a name="genesis_36_14"></a>Genesis 36:14

And these were the [ben](../../strongs/h/h1121.md) of ['Āhŏlîḇāmâ](../../strongs/h/h173.md), the [bath](../../strongs/h/h1323.md) of [ʿĂnâ](../../strongs/h/h6034.md) the [bath](../../strongs/h/h1323.md) of [Ṣiḇʿôn](../../strongs/h/h6649.md), [ʿĒśāv](../../strongs/h/h6215.md) ['ishshah](../../strongs/h/h802.md): and she [yalad](../../strongs/h/h3205.md) to [ʿĒśāv](../../strongs/h/h6215.md) [yᵊʿîš](../../strongs/h/h3274.md), and [Yaʿlām](../../strongs/h/h3281.md), and [Qōraḥ](../../strongs/h/h7141.md).

<a name="genesis_36_15"></a>Genesis 36:15

These were ['allûp̄](../../strongs/h/h441.md) of the [ben](../../strongs/h/h1121.md) of [ʿĒśāv](../../strongs/h/h6215.md): the [ben](../../strongs/h/h1121.md) of ['Ĕlîp̄az](../../strongs/h/h464.md) the [bᵊḵôr](../../strongs/h/h1060.md) son of [ʿĒśāv](../../strongs/h/h6215.md); ['allûp̄](../../strongs/h/h441.md) [Têmān](../../strongs/h/h8487.md), ['allûp̄](../../strongs/h/h441.md) ['Ômār](../../strongs/h/h201.md), ['allûp̄](../../strongs/h/h441.md) [ṣᵊp̄ô](../../strongs/h/h6825.md), ['allûp̄](../../strongs/h/h441.md) [Qᵊnaz](../../strongs/h/h7073.md),

<a name="genesis_36_16"></a>Genesis 36:16

['allûp̄](../../strongs/h/h441.md) [Qōraḥ](../../strongs/h/h7141.md), ['allûp̄](../../strongs/h/h441.md) [Gaʿtām](../../strongs/h/h1609.md), and ['allûp̄](../../strongs/h/h441.md) [ʿĂmālēq](../../strongs/h/h6002.md): these are the ['allûp̄](../../strongs/h/h441.md) that came of ['Ĕlîp̄az](../../strongs/h/h464.md) in the ['erets](../../strongs/h/h776.md) of ['Ĕḏōm](../../strongs/h/h123.md); these were the [ben](../../strongs/h/h1121.md) of [ʿāḏâ](../../strongs/h/h5711.md).

<a name="genesis_36_17"></a>Genesis 36:17

And these are the [ben](../../strongs/h/h1121.md) of [Rᵊʿû'ēl](../../strongs/h/h7467.md) [ʿĒśāv](../../strongs/h/h6215.md) [ben](../../strongs/h/h1121.md); ['allûp̄](../../strongs/h/h441.md) [Naḥaṯ](../../strongs/h/h5184.md), ['allûp̄](../../strongs/h/h441.md) [Zeraḥ](../../strongs/h/h2226.md), ['allûp̄](../../strongs/h/h441.md) [Šammâ](../../strongs/h/h8048.md), ['allûp̄](../../strongs/h/h441.md) [Mizzê](../../strongs/h/h4199.md): these are the ['allûp̄](../../strongs/h/h441.md) that came of [Rᵊʿû'ēl](../../strongs/h/h7467.md) in the ['erets](../../strongs/h/h776.md) of ['Ĕḏōm](../../strongs/h/h123.md); these are the [ben](../../strongs/h/h1121.md) of [bāśmaṯ](../../strongs/h/h1315.md) [ʿĒśāv](../../strongs/h/h6215.md) ['ishshah](../../strongs/h/h802.md).

<a name="genesis_36_18"></a>Genesis 36:18

And these are the [ben](../../strongs/h/h1121.md) of ['Āhŏlîḇāmâ](../../strongs/h/h173.md) [ʿĒśāv](../../strongs/h/h6215.md) ['ishshah](../../strongs/h/h802.md); ['allûp̄](../../strongs/h/h441.md) [Yᵊʿûš](../../strongs/h/h3266.md), ['allûp̄](../../strongs/h/h441.md) [Yaʿlām](../../strongs/h/h3281.md), ['allûp̄](../../strongs/h/h441.md) [Qōraḥ](../../strongs/h/h7141.md): these were the ['allûp̄](../../strongs/h/h441.md) that came of ['Āhŏlîḇāmâ](../../strongs/h/h173.md) the [bath](../../strongs/h/h1323.md) of [ʿĂnâ](../../strongs/h/h6034.md), [ʿĒśāv](../../strongs/h/h6215.md) ['ishshah](../../strongs/h/h802.md).

<a name="genesis_36_19"></a>Genesis 36:19

These are the [ben](../../strongs/h/h1121.md) of [ʿĒśāv](../../strongs/h/h6215.md), who is ['Ĕḏōm](../../strongs/h/h123.md), and these are their ['allûp̄](../../strongs/h/h441.md).

<a name="genesis_36_20"></a>Genesis 36:20

These are the [ben](../../strongs/h/h1121.md) of [Śēʿîr](../../strongs/h/h8165.md) the [ḥōrî](../../strongs/h/h2752.md), who [yashab](../../strongs/h/h3427.md) the ['erets](../../strongs/h/h776.md); [Lôṭān](../../strongs/h/h3877.md), and [Šôḇāl](../../strongs/h/h7732.md), and [Ṣiḇʿôn](../../strongs/h/h6649.md), and [ʿĂnâ](../../strongs/h/h6034.md),

<a name="genesis_36_21"></a>Genesis 36:21

And [Dîšôn](../../strongs/h/h1787.md), and ['Ēṣer](../../strongs/h/h687.md), and [Dîšān](../../strongs/h/h1789.md): these are the ['allûp̄](../../strongs/h/h441.md) of the [ḥōrî](../../strongs/h/h2752.md), the [ben](../../strongs/h/h1121.md) of [Śēʿîr](../../strongs/h/h8165.md) in the ['erets](../../strongs/h/h776.md) of ['Ĕḏōm](../../strongs/h/h123.md).

<a name="genesis_36_22"></a>Genesis 36:22

And the [ben](../../strongs/h/h1121.md) of [Lôṭān](../../strongs/h/h3877.md) were [Ḥōrî](../../strongs/h/h2753.md) and [hêmām](../../strongs/h/h1967.md); and [Lôṭān](../../strongs/h/h3877.md) ['āḥôṯ](../../strongs/h/h269.md) was [Timnāʿ](../../strongs/h/h8555.md).

<a name="genesis_36_23"></a>Genesis 36:23

And the [ben](../../strongs/h/h1121.md) of [Šôḇāl](../../strongs/h/h7732.md) were these; [ʿAlvān](../../strongs/h/h5935.md), and [Mānaḥaṯ](../../strongs/h/h4506.md), and [ʿÊḇāl](../../strongs/h/h5858.md), [Šᵊp̄ô](../../strongs/h/h8195.md), and ['Ônām](../../strongs/h/h208.md).

<a name="genesis_36_24"></a>Genesis 36:24

And these are the [ben](../../strongs/h/h1121.md) of [Ṣiḇʿôn](../../strongs/h/h6649.md); both ['Ayyâ](../../strongs/h/h345.md), and [ʿĂnâ](../../strongs/h/h6034.md): this was that [ʿĂnâ](../../strongs/h/h6034.md) that [māṣā'](../../strongs/h/h4672.md) the [yēmîm](../../strongs/h/h3222.md) in the [midbar](../../strongs/h/h4057.md), as he [ra'ah](../../strongs/h/h7462.md) the [chamowr](../../strongs/h/h2543.md) of [Ṣiḇʿôn](../../strongs/h/h6649.md) his ['ab](../../strongs/h/h1.md).

<a name="genesis_36_25"></a>Genesis 36:25

And the [ben](../../strongs/h/h1121.md) of [ʿĂnâ](../../strongs/h/h6034.md) were these; [Dîšôn](../../strongs/h/h1787.md), and ['Āhŏlîḇāmâ](../../strongs/h/h173.md) the [bath](../../strongs/h/h1323.md) of [ʿĂnâ](../../strongs/h/h6034.md).

<a name="genesis_36_26"></a>Genesis 36:26

And these are the [ben](../../strongs/h/h1121.md) of [Dîšôn](../../strongs/h/h1787.md); [ḥemdān](../../strongs/h/h2533.md), and ['Ešbān](../../strongs/h/h790.md), and [Yiṯrān](../../strongs/h/h3506.md), and [kᵊrān](../../strongs/h/h3763.md).

<a name="genesis_36_27"></a>Genesis 36:27

The [ben](../../strongs/h/h1121.md) of ['Ēṣer](../../strongs/h/h687.md) are these; [Bilhān](../../strongs/h/h1092.md), and [Zaʿăvān](../../strongs/h/h2190.md), and [ʿăqān](../../strongs/h/h6130.md).

<a name="genesis_36_28"></a>Genesis 36:28

The [ben](../../strongs/h/h1121.md) of [Dîšān](../../strongs/h/h1789.md) are these; [ʿÛṣ](../../strongs/h/h5780.md), and ['Ărān](../../strongs/h/h765.md).

<a name="genesis_36_29"></a>Genesis 36:29

These are the ['allûp̄](../../strongs/h/h441.md) that came of the [ḥōrî](../../strongs/h/h2752.md); ['allûp̄](../../strongs/h/h441.md) [Lôṭān](../../strongs/h/h3877.md), ['allûp̄](../../strongs/h/h441.md) [Šôḇāl](../../strongs/h/h7732.md), ['allûp̄](../../strongs/h/h441.md) [Ṣiḇʿôn](../../strongs/h/h6649.md), ['allûp̄](../../strongs/h/h441.md) [ʿĂnâ](../../strongs/h/h6034.md),

<a name="genesis_36_30"></a>Genesis 36:30

['allûp̄](../../strongs/h/h441.md) [Dîšôn](../../strongs/h/h1787.md), ['allûp̄](../../strongs/h/h441.md) ['Ēṣer](../../strongs/h/h687.md), ['allûp̄](../../strongs/h/h441.md) [Dîšān](../../strongs/h/h1789.md): these are the ['allûp̄](../../strongs/h/h441.md) that came of [Ḥōrî](../../strongs/h/h2753.md), among their ['allûp̄](../../strongs/h/h441.md) in the ['erets](../../strongs/h/h776.md) of [Śēʿîr](../../strongs/h/h8165.md).

<a name="genesis_36_31"></a>Genesis 36:31

And these are the [melek](../../strongs/h/h4428.md) that [mālaḵ](../../strongs/h/h4427.md) in the ['erets](../../strongs/h/h776.md) of ['Ĕḏōm](../../strongs/h/h123.md), [paniym](../../strongs/h/h6440.md) there [mālaḵ](../../strongs/h/h4427.md) any [melek](../../strongs/h/h4428.md) over the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="genesis_36_32"></a>Genesis 36:32

And [Belaʿ](../../strongs/h/h1106.md) the [ben](../../strongs/h/h1121.md) of [Bᵊʿôr](../../strongs/h/h1160.md) [mālaḵ](../../strongs/h/h4427.md) in ['Ĕḏōm](../../strongs/h/h123.md): and the [shem](../../strongs/h/h8034.md) of his [ʿîr](../../strongs/h/h5892.md) was [Dinhāḇâ](../../strongs/h/h1838.md).

<a name="genesis_36_33"></a>Genesis 36:33

And [Belaʿ](../../strongs/h/h1106.md) [muwth](../../strongs/h/h4191.md), and [Yôḇāḇ](../../strongs/h/h3103.md) the [ben](../../strongs/h/h1121.md) of [Zeraḥ](../../strongs/h/h2226.md) of [Bāṣrâ](../../strongs/h/h1224.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="genesis_36_34"></a>Genesis 36:34

And [Yôḇāḇ](../../strongs/h/h3103.md) [muwth](../../strongs/h/h4191.md), and [Ḥûšām](../../strongs/h/h2367.md) of the ['erets](../../strongs/h/h776.md) of [têmānî](../../strongs/h/h8489.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="genesis_36_35"></a>Genesis 36:35

And [Ḥûšām](../../strongs/h/h2367.md) [muwth](../../strongs/h/h4191.md), and [Hăḏaḏ](../../strongs/h/h1908.md) the [ben](../../strongs/h/h1121.md) of [Bᵊḏaḏ](../../strongs/h/h911.md), who [nakah](../../strongs/h/h5221.md) [Miḏyān](../../strongs/h/h4080.md) in the [sadeh](../../strongs/h/h7704.md) of [Mô'āḇ](../../strongs/h/h4124.md), [mālaḵ](../../strongs/h/h4427.md) in his stead: and the [shem](../../strongs/h/h8034.md) of his [ʿîr](../../strongs/h/h5892.md) was [ʿĂvîṯ](../../strongs/h/h5762.md).

<a name="genesis_36_36"></a>Genesis 36:36

And [Hăḏaḏ](../../strongs/h/h1908.md) [muwth](../../strongs/h/h4191.md), and [Śamlâ](../../strongs/h/h8072.md) of [maśrēqâ](../../strongs/h/h4957.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="genesis_36_37"></a>Genesis 36:37

And [Śamlâ](../../strongs/h/h8072.md) [muwth](../../strongs/h/h4191.md), and [Šā'ûl](../../strongs/h/h7586.md) of [Rᵊḥōḇôṯ](../../strongs/h/h7344.md) by the [nāhār](../../strongs/h/h5104.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="genesis_36_38"></a>Genesis 36:38

And [Šā'ûl](../../strongs/h/h7586.md) [muwth](../../strongs/h/h4191.md), and [Baʿal ḥānān](../../strongs/h/h1177.md) the [ben](../../strongs/h/h1121.md) of [ʿAḵbôr](../../strongs/h/h5907.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="genesis_36_39"></a>Genesis 36:39

And [Baʿal ḥānān](../../strongs/h/h1177.md) the [ben](../../strongs/h/h1121.md) of [ʿAḵbôr](../../strongs/h/h5907.md) [muwth](../../strongs/h/h4191.md), and [hăḏar](../../strongs/h/h1924.md) [mālaḵ](../../strongs/h/h4427.md) in his stead: and the [shem](../../strongs/h/h8034.md) of his [ʿîr](../../strongs/h/h5892.md) was [Pāʿû](../../strongs/h/h6464.md); and his ['ishshah](../../strongs/h/h802.md) [shem](../../strongs/h/h8034.md) was [Mᵊhêṭaḇ'ēl](../../strongs/h/h4105.md), the [bath](../../strongs/h/h1323.md) of [Maṭrēḏ](../../strongs/h/h4308.md), the [bath](../../strongs/h/h1323.md) of [Mê zāhāḇ](../../strongs/h/h4314.md).

<a name="genesis_36_40"></a>Genesis 36:40

And these are the [shem](../../strongs/h/h8034.md) of the ['allûp̄](../../strongs/h/h441.md) that came of [ʿĒśāv](../../strongs/h/h6215.md), according to their [mišpāḥâ](../../strongs/h/h4940.md), after their [maqowm](../../strongs/h/h4725.md), by their [shem](../../strongs/h/h8034.md); ['allûp̄](../../strongs/h/h441.md) [Timnāʿ](../../strongs/h/h8555.md), ['allûp̄](../../strongs/h/h441.md) [ʿAlvâ](../../strongs/h/h5933.md), ['allûp̄](../../strongs/h/h441.md) [Yᵊṯēṯ](../../strongs/h/h3509.md),

<a name="genesis_36_41"></a>Genesis 36:41

['allûp̄](../../strongs/h/h441.md) ['Āhŏlîḇāmâ](../../strongs/h/h173.md), ['allûp̄](../../strongs/h/h441.md) ['Ēlâ](../../strongs/h/h425.md), ['allûp̄](../../strongs/h/h441.md) [Pînōn](../../strongs/h/h6373.md),

<a name="genesis_36_42"></a>Genesis 36:42

['allûp̄](../../strongs/h/h441.md) [Qᵊnaz](../../strongs/h/h7073.md), ['allûp̄](../../strongs/h/h441.md) [Têmān](../../strongs/h/h8487.md), ['allûp̄](../../strongs/h/h441.md) [Miḇṣār](../../strongs/h/h4014.md),

<a name="genesis_36_43"></a>Genesis 36:43

['allûp̄](../../strongs/h/h441.md) [Maḡdî'ēl](../../strongs/h/h4025.md), ['allûp̄](../../strongs/h/h441.md) [ʿÎrām](../../strongs/h/h5902.md): these be the ['allûp̄](../../strongs/h/h441.md) of ['Ĕḏōm](../../strongs/h/h123.md), according to their [môšāḇ](../../strongs/h/h4186.md) in the ['erets](../../strongs/h/h776.md) of their ['achuzzah](../../strongs/h/h272.md): he is [ʿĒśāv](../../strongs/h/h6215.md) the ['ab](../../strongs/h/h1.md) of the ['Ĕḏōm](../../strongs/h/h123.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 35](genesis_35.md) - [Genesis 37](genesis_37.md)