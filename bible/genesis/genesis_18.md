# [Genesis 18](https://www.blueletterbible.org/kjv/gen/18/1/s_18001)

<a name="genesis_18_1"></a>Genesis 18:1

And [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) unto him in the ['ēlôn](../../strongs/h/h436.md) of [mamrē'](../../strongs/h/h4471.md): and he [yashab](../../strongs/h/h3427.md) in the ['ohel](../../strongs/h/h168.md) [peṯaḥ](../../strongs/h/h6607.md) in the [ḥōm](../../strongs/h/h2527.md) of the [yowm](../../strongs/h/h3117.md);

<a name="genesis_18_2"></a>Genesis 18:2

And he [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md) and [ra'ah](../../strongs/h/h7200.md), and, lo, three ['enowsh](../../strongs/h/h582.md) [nāṣaḇ](../../strongs/h/h5324.md) by him: and when he [ra'ah](../../strongs/h/h7200.md) them, he [rûṣ](../../strongs/h/h7323.md) to [qārā'](../../strongs/h/h7125.md) them from the ['ohel](../../strongs/h/h168.md) [peṯaḥ](../../strongs/h/h6607.md), and [shachah](../../strongs/h/h7812.md) himself toward the ['erets](../../strongs/h/h776.md),

<a name="genesis_18_3"></a>Genesis 18:3

And ['āmar](../../strongs/h/h559.md), ['adonay](../../strongs/h/h136.md), if now I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md), ['abar](../../strongs/h/h5674.md) not away, I pray thee, from thy ['ebed](../../strongs/h/h5650.md):

<a name="genesis_18_4"></a>Genesis 18:4

Let a [mᵊʿaṭ](../../strongs/h/h4592.md) [mayim](../../strongs/h/h4325.md), [na'](../../strongs/h/h4994.md), [laqach](../../strongs/h/h3947.md), and [rāḥaṣ](../../strongs/h/h7364.md) your [regel](../../strongs/h/h7272.md), and [šāʿan](../../strongs/h/h8172.md) yourselves under the ['ets](../../strongs/h/h6086.md):

<a name="genesis_18_5"></a>Genesis 18:5

And I will [laqach](../../strongs/h/h3947.md) a [paṯ](../../strongs/h/h6595.md) of [lechem](../../strongs/h/h3899.md), and [sāʿaḏ](../../strongs/h/h5582.md) ye your [leb](../../strongs/h/h3820.md); ['aḥar](../../strongs/h/h310.md) that ye shall ['abar](../../strongs/h/h5674.md): for therefore are ye ['abar](../../strongs/h/h5674.md) to your ['ebed](../../strongs/h/h5650.md). And they [dabar](../../strongs/h/h1696.md), ['asah](../../strongs/h/h6213.md), as thou hast ['āmar](../../strongs/h/h559.md).

<a name="genesis_18_6"></a>Genesis 18:6

And ['Abraham](../../strongs/h/h85.md) [māhar](../../strongs/h/h4116.md) into the ['ohel](../../strongs/h/h168.md) unto [Śārâ](../../strongs/h/h8283.md), and ['āmar](../../strongs/h/h559.md), [māhar](../../strongs/h/h4116.md) three [sᵊ'â](../../strongs/h/h5429.md) of [sōleṯ](../../strongs/h/h5560.md) [qemaḥ](../../strongs/h/h7058.md), [lûš](../../strongs/h/h3888.md) it, and ['asah](../../strongs/h/h6213.md) [ʿugâ](../../strongs/h/h5692.md) upon the hearth.

<a name="genesis_18_7"></a>Genesis 18:7

And ['Abraham](../../strongs/h/h85.md) [rûṣ](../../strongs/h/h7323.md) unto the [bāqār](../../strongs/h/h1241.md), and [laqach](../../strongs/h/h3947.md) a [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [raḵ](../../strongs/h/h7390.md) and [towb](../../strongs/h/h2896.md), and [nathan](../../strongs/h/h5414.md) it unto a [naʿar](../../strongs/h/h5288.md); and he [māhar](../../strongs/h/h4116.md) to ['asah](../../strongs/h/h6213.md) it.

<a name="genesis_18_8"></a>Genesis 18:8

And he [laqach](../../strongs/h/h3947.md) [ḥem'â](../../strongs/h/h2529.md), and [chalab](../../strongs/h/h2461.md), and the [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) which he had ['asah](../../strongs/h/h6213.md), and [nathan](../../strongs/h/h5414.md) it [paniym](../../strongs/h/h6440.md) them; and he ['amad](../../strongs/h/h5975.md) by them under the ['ets](../../strongs/h/h6086.md), and they did ['akal](../../strongs/h/h398.md).

<a name="genesis_18_9"></a>Genesis 18:9

And they ['āmar](../../strongs/h/h559.md) unto him, ['ayyê](../../strongs/h/h346.md) is [Śārâ](../../strongs/h/h8283.md) thy ['ishshah](../../strongs/h/h802.md)? And he ['āmar](../../strongs/h/h559.md), Behold, in the ['ohel](../../strongs/h/h168.md).

<a name="genesis_18_10"></a>Genesis 18:10

And he ['āmar](../../strongs/h/h559.md), I will [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) unto thee according to the [ʿēṯ](../../strongs/h/h6256.md) of [chay](../../strongs/h/h2416.md); and, lo, [Śārâ](../../strongs/h/h8283.md) thy ['ishshah](../../strongs/h/h802.md) shall have a [ben](../../strongs/h/h1121.md). And [Śārâ](../../strongs/h/h8283.md) [shama'](../../strongs/h/h8085.md) it in the ['ohel](../../strongs/h/h168.md) [peṯaḥ](../../strongs/h/h6607.md), which was ['aḥar](../../strongs/h/h310.md) him.

<a name="genesis_18_11"></a>Genesis 18:11

Now ['Abraham](../../strongs/h/h85.md) and [Śārâ](../../strongs/h/h8283.md) were [zāqēn](../../strongs/h/h2205.md) and [bow'](../../strongs/h/h935.md) in [yowm](../../strongs/h/h3117.md); and it [ḥāḏal](../../strongs/h/h2308.md) to be with [Śārâ](../../strongs/h/h8283.md) after the ['orach](../../strongs/h/h734.md) of ['ishshah](../../strongs/h/h802.md).

<a name="genesis_18_12"></a>Genesis 18:12

Therefore [Śārâ](../../strongs/h/h8283.md) [ṣāḥaq](../../strongs/h/h6711.md) [qereḇ](../../strongs/h/h7130.md), ['āmar](../../strongs/h/h559.md), ['aḥar](../../strongs/h/h310.md) I am [bālâ](../../strongs/h/h1086.md) shall I have [ʿēḏen](../../strongs/h/h5730.md), my ['adown](../../strongs/h/h113.md) being [zāqēn](../../strongs/h/h2204.md) also?

<a name="genesis_18_13"></a>Genesis 18:13

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto ['Abraham](../../strongs/h/h85.md), Wherefore did [Śārâ](../../strongs/h/h8283.md) [ṣāḥaq](../../strongs/h/h6711.md), ['āmar](../../strongs/h/h559.md), Shall I ['umnām](../../strongs/h/h552.md) [yalad](../../strongs/h/h3205.md), which am [zāqēn](../../strongs/h/h2204.md)?

<a name="genesis_18_14"></a>Genesis 18:14

Is [dabar](../../strongs/h/h1697.md) [pala'](../../strongs/h/h6381.md) for [Yĕhovah](../../strongs/h/h3068.md)? At the [môʿēḏ](../../strongs/h/h4150.md) I will [shuwb](../../strongs/h/h7725.md) unto thee, according to the [ʿēṯ](../../strongs/h/h6256.md) of [chay](../../strongs/h/h2416.md), and [Śārâ](../../strongs/h/h8283.md) shall have a [ben](../../strongs/h/h1121.md).

<a name="genesis_18_15"></a>Genesis 18:15

Then [Śārâ](../../strongs/h/h8283.md) [kāḥaš](../../strongs/h/h3584.md), ['āmar](../../strongs/h/h559.md), I [ṣāḥaq](../../strongs/h/h6711.md) not; for she was [yare'](../../strongs/h/h3372.md). And he ['āmar](../../strongs/h/h559.md), [lō'](../../strongs/h/h3808.md); but thou didst [ṣāḥaq](../../strongs/h/h6711.md).

<a name="genesis_18_16"></a>Genesis 18:16

And the ['enowsh](../../strongs/h/h582.md) [quwm](../../strongs/h/h6965.md) from thence, and [šāqap̄](../../strongs/h/h8259.md) [paniym](../../strongs/h/h6440.md) [Sᵊḏōm](../../strongs/h/h5467.md): and ['Abraham](../../strongs/h/h85.md) [halak](../../strongs/h/h1980.md) with them to [shalach](../../strongs/h/h7971.md) them.

<a name="genesis_18_17"></a>Genesis 18:17

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), Shall I [kāsâ](../../strongs/h/h3680.md) from ['Abraham](../../strongs/h/h85.md) that thing which I ['asah](../../strongs/h/h6213.md);

<a name="genesis_18_18"></a>Genesis 18:18

Seeing that ['Abraham](../../strongs/h/h85.md) shall surely become a [gadowl](../../strongs/h/h1419.md) and ['atsuwm](../../strongs/h/h6099.md) [gowy](../../strongs/h/h1471.md), and all the [gowy](../../strongs/h/h1471.md) of the ['erets](../../strongs/h/h776.md) shall be [barak](../../strongs/h/h1288.md) in him?

<a name="genesis_18_19"></a>Genesis 18:19

For I [yada'](../../strongs/h/h3045.md) him, that he will [tsavah](../../strongs/h/h6680.md) his [ben](../../strongs/h/h1121.md) and his [bayith](../../strongs/h/h1004.md) ['aḥar](../../strongs/h/h310.md) him, and they shall [shamar](../../strongs/h/h8104.md) the [derek](../../strongs/h/h1870.md) of [Yĕhovah](../../strongs/h/h3068.md), to ['asah](../../strongs/h/h6213.md) [tsedaqah](../../strongs/h/h6666.md) and [mishpat](../../strongs/h/h4941.md); that [Yĕhovah](../../strongs/h/h3068.md) may [bow'](../../strongs/h/h935.md) upon ['Abraham](../../strongs/h/h85.md) that which he hath [dabar](../../strongs/h/h1696.md) of him.

<a name="genesis_18_20"></a>Genesis 18:20

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), Because the [zaʿaq](../../strongs/h/h2201.md) of [Sᵊḏōm](../../strongs/h/h5467.md) and [ʿĂmōrâ](../../strongs/h/h6017.md) is [rab](../../strongs/h/h7227.md), and because their [chatta'ath](../../strongs/h/h2403.md) is [me'od](../../strongs/h/h3966.md) [kabad](../../strongs/h/h3513.md);

<a name="genesis_18_21"></a>Genesis 18:21

I will [yarad](../../strongs/h/h3381.md) now, and [ra'ah](../../strongs/h/h7200.md) whether they have ['asah](../../strongs/h/h6213.md) [kālâ](../../strongs/h/h3617.md) according to the [tsa'aqah](../../strongs/h/h6818.md) of it, which is [bow'](../../strongs/h/h935.md) unto me; and if not, I will [yada'](../../strongs/h/h3045.md).

<a name="genesis_18_22"></a>Genesis 18:22

And the ['enowsh](../../strongs/h/h582.md) [panah](../../strongs/h/h6437.md) from thence, and [yālaḵ](../../strongs/h/h3212.md) toward [Sᵊḏōm](../../strongs/h/h5467.md): but ['Abraham](../../strongs/h/h85.md) ['amad](../../strongs/h/h5975.md) yet [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="genesis_18_23"></a>Genesis 18:23

And ['Abraham](../../strongs/h/h85.md) [nāḡaš](../../strongs/h/h5066.md), and ['āmar](../../strongs/h/h559.md), Wilt thou also [sāp̄â](../../strongs/h/h5595.md) the [tsaddiyq](../../strongs/h/h6662.md) with the [rasha'](../../strongs/h/h7563.md)?

<a name="genesis_18_24"></a>Genesis 18:24

Peradventure there be fifty [tsaddiyq](../../strongs/h/h6662.md) [tavek](../../strongs/h/h8432.md) the [ʿîr](../../strongs/h/h5892.md): wilt thou also [sāp̄â](../../strongs/h/h5595.md) and not [nasa'](../../strongs/h/h5375.md) the [maqowm](../../strongs/h/h4725.md) for the fifty [tsaddiyq](../../strongs/h/h6662.md) that are [qereḇ](../../strongs/h/h7130.md)?

<a name="genesis_18_25"></a>Genesis 18:25

[ḥālîlâ](../../strongs/h/h2486.md) from thee to ['asah](../../strongs/h/h6213.md) after this [dabar](../../strongs/h/h1697.md), to [muwth](../../strongs/h/h4191.md) the [tsaddiyq](../../strongs/h/h6662.md) with the [rasha'](../../strongs/h/h7563.md): and that the [tsaddiyq](../../strongs/h/h6662.md) should be as the [rasha'](../../strongs/h/h7563.md), [ḥālîlâ](../../strongs/h/h2486.md) from thee: Shall not the [shaphat](../../strongs/h/h8199.md) of all the ['erets](../../strongs/h/h776.md) do [mishpat](../../strongs/h/h4941.md)?

<a name="genesis_18_26"></a>Genesis 18:26

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), If I [māṣā'](../../strongs/h/h4672.md) in [Sᵊḏōm](../../strongs/h/h5467.md) fifty [tsaddiyq](../../strongs/h/h6662.md) [tavek](../../strongs/h/h8432.md) the [ʿîr](../../strongs/h/h5892.md), then I will [nasa'](../../strongs/h/h5375.md) all the [maqowm](../../strongs/h/h4725.md) for their sakes.

<a name="genesis_18_27"></a>Genesis 18:27

And ['Abraham](../../strongs/h/h85.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), Behold now, I have [yā'al](../../strongs/h/h2974.md) me to [dabar](../../strongs/h/h1696.md) unto ['adonay](../../strongs/h/h136.md), which am but ['aphar](../../strongs/h/h6083.md) and ['ēp̄er](../../strongs/h/h665.md):

<a name="genesis_18_28"></a>Genesis 18:28

Peradventure there shall [ḥāsēr](../../strongs/h/h2637.md) five of the fifty [tsaddiyq](../../strongs/h/h6662.md): wilt thou [shachath](../../strongs/h/h7843.md) all the [ʿîr](../../strongs/h/h5892.md) for lack of five? And he ['āmar](../../strongs/h/h559.md), If I [māṣā'](../../strongs/h/h4672.md) there forty and five, I will not [shachath](../../strongs/h/h7843.md) it.

<a name="genesis_18_29"></a>Genesis 18:29

And he [dabar](../../strongs/h/h1696.md) unto him yet again, and ['āmar](../../strongs/h/h559.md), Peradventure there shall be forty [māṣā'](../../strongs/h/h4672.md) there. And he ['āmar](../../strongs/h/h559.md), I will not ['asah](../../strongs/h/h6213.md) it for forty's sake.

<a name="genesis_18_30"></a>Genesis 18:30

And he ['āmar](../../strongs/h/h559.md) unto him, [na'](../../strongs/h/h4994.md) let not ['adonay](../../strongs/h/h136.md) be [ḥārâ](../../strongs/h/h2734.md), and I will [dabar](../../strongs/h/h1696.md): Peradventure there shall thirty be [māṣā'](../../strongs/h/h4672.md) there. And he ['āmar](../../strongs/h/h559.md), I will not ['asah](../../strongs/h/h6213.md) it, if I find thirty there.

<a name="genesis_18_31"></a>Genesis 18:31

And he ['āmar](../../strongs/h/h559.md), Behold now, I have [yā'al](../../strongs/h/h2974.md) to [dabar](../../strongs/h/h1696.md) unto ['adonay](../../strongs/h/h136.md): Peradventure there shall be twenty [māṣā'](../../strongs/h/h4672.md) there. And he ['āmar](../../strongs/h/h559.md), I will not [shachath](../../strongs/h/h7843.md) it for twenty's sake.

<a name="genesis_18_32"></a>Genesis 18:32

And he said, Oh let not ['adonay](../../strongs/h/h136.md) be [ḥārâ](../../strongs/h/h2734.md), and I will [dabar](../../strongs/h/h1696.md) yet but this [pa'am](../../strongs/h/h6471.md): Peradventure ten shall be [māṣā'](../../strongs/h/h4672.md) there. And he ['āmar](../../strongs/h/h559.md), I will not [shachath](../../strongs/h/h7843.md) it for ten's sake.

<a name="genesis_18_33"></a>Genesis 18:33

And [Yĕhovah](../../strongs/h/h3068.md) [yālaḵ](../../strongs/h/h3212.md), as soon as he had [kalah](../../strongs/h/h3615.md) [dabar](../../strongs/h/h1696.md) with ['Abraham](../../strongs/h/h85.md): and ['Abraham](../../strongs/h/h85.md) [shuwb](../../strongs/h/h7725.md) unto his [maqowm](../../strongs/h/h4725.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 17](genesis_17.md) - [Genesis 19](genesis_19.md)