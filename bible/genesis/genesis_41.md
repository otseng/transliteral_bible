# [Genesis 41](https://www.blueletterbible.org/kjv/gen/41/1/s_41001)

<a name="genesis_41_1"></a>Genesis 41:1

And it came to pass at the [qēṣ](../../strongs/h/h7093.md) of two [yowm](../../strongs/h/h3117.md) [šānâ](../../strongs/h/h8141.md), that [Parʿô](../../strongs/h/h6547.md) [ḥālam](../../strongs/h/h2492.md): and, behold, he ['amad](../../strongs/h/h5975.md) by the [yᵊ'ōr](../../strongs/h/h2975.md).

<a name="genesis_41_2"></a>Genesis 41:2

And, behold, there [ʿālâ](../../strongs/h/h5927.md) out of the [yᵊ'ōr](../../strongs/h/h2975.md) seven [yāp̄ê](../../strongs/h/h3303.md) [mar'ê](../../strongs/h/h4758.md) [pārâ](../../strongs/h/h6510.md) and [bārî'](../../strongs/h/h1277.md) [basar](../../strongs/h/h1320.md); and they [ra'ah](../../strongs/h/h7462.md) in an ['āḥû](../../strongs/h/h260.md).

<a name="genesis_41_3"></a>Genesis 41:3

And, behold, seven ['aḥēr](../../strongs/h/h312.md) [pārâ](../../strongs/h/h6510.md) [ʿālâ](../../strongs/h/h5927.md) ['aḥar](../../strongs/h/h310.md) them out of the [yᵊ'ōr](../../strongs/h/h2975.md), [ra'](../../strongs/h/h7451.md) [mar'ê](../../strongs/h/h4758.md) and [daq](../../strongs/h/h1851.md) [basar](../../strongs/h/h1320.md); and ['amad](../../strongs/h/h5975.md) by the other [pārâ](../../strongs/h/h6510.md) upon the [saphah](../../strongs/h/h8193.md) of the [yᵊ'ōr](../../strongs/h/h2975.md).

<a name="genesis_41_4"></a>Genesis 41:4

And the [ra'](../../strongs/h/h7451.md) [mar'ê](../../strongs/h/h4758.md) and [daq](../../strongs/h/h1851.md) [basar](../../strongs/h/h1320.md) [pārâ](../../strongs/h/h6510.md) did ['akal](../../strongs/h/h398.md) up the seven [yāp̄ê](../../strongs/h/h3303.md) [mar'ê](../../strongs/h/h4758.md) and [bārî'](../../strongs/h/h1277.md) [pārâ](../../strongs/h/h6510.md). So [Parʿô](../../strongs/h/h6547.md) [yāqaṣ](../../strongs/h/h3364.md).

<a name="genesis_41_5"></a>Genesis 41:5

And he [yashen](../../strongs/h/h3462.md) and [ḥālam](../../strongs/h/h2492.md) the second time: and, behold, seven [šibōleṯ](../../strongs/h/h7641.md) [ʿālâ](../../strongs/h/h5927.md) upon one [qānê](../../strongs/h/h7070.md), [bārî'](../../strongs/h/h1277.md) and [towb](../../strongs/h/h2896.md).

<a name="genesis_41_6"></a>Genesis 41:6

And, behold, seven [daq](../../strongs/h/h1851.md) [šibōleṯ](../../strongs/h/h7641.md) and [šāḏap̄](../../strongs/h/h7710.md) with the [qāḏîm](../../strongs/h/h6921.md) [ṣāmaḥ](../../strongs/h/h6779.md) ['aḥar](../../strongs/h/h310.md) them.

<a name="genesis_41_7"></a>Genesis 41:7

And the seven [daq](../../strongs/h/h1851.md) [šibōleṯ](../../strongs/h/h7641.md) [bālaʿ](../../strongs/h/h1104.md) the seven [bārî'](../../strongs/h/h1277.md) and [mālē'](../../strongs/h/h4392.md) [šibōleṯ](../../strongs/h/h7641.md). And [Parʿô](../../strongs/h/h6547.md) [yāqaṣ](../../strongs/h/h3364.md), and, behold, it was a [ḥălôm](../../strongs/h/h2472.md).

<a name="genesis_41_8"></a>Genesis 41:8

And it came to pass in the [boqer](../../strongs/h/h1242.md) that his [ruwach](../../strongs/h/h7307.md) was [pāʿam](../../strongs/h/h6470.md); and he [shalach](../../strongs/h/h7971.md) and [qara'](../../strongs/h/h7121.md) for all the [ḥarṭōm](../../strongs/h/h2748.md) of [Mitsrayim](../../strongs/h/h4714.md), and all the [ḥāḵām](../../strongs/h/h2450.md) thereof: and [Parʿô](../../strongs/h/h6547.md) [sāp̄ar](../../strongs/h/h5608.md) them his [ḥălôm](../../strongs/h/h2472.md); but there was none that could [pāṯar](../../strongs/h/h6622.md) them unto [Parʿô](../../strongs/h/h6547.md).

<a name="genesis_41_9"></a>Genesis 41:9

Then [dabar](../../strongs/h/h1696.md) the [śar](../../strongs/h/h8269.md) [šāqâ](../../strongs/h/h8248.md) unto [Parʿô](../../strongs/h/h6547.md), ['āmar](../../strongs/h/h559.md), I do [zakar](../../strongs/h/h2142.md) my [ḥēṭĕ'](../../strongs/h/h2399.md) this [yowm](../../strongs/h/h3117.md):

<a name="genesis_41_10"></a>Genesis 41:10

[Parʿô](../../strongs/h/h6547.md) was [qāṣap̄](../../strongs/h/h7107.md) with his ['ebed](../../strongs/h/h5650.md), and [nathan](../../strongs/h/h5414.md) me in [mišmār](../../strongs/h/h4929.md) in the [śar](../../strongs/h/h8269.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [bayith](../../strongs/h/h1004.md), both me and the [śar](../../strongs/h/h8269.md) ['āp̄â](../../strongs/h/h644.md):

<a name="genesis_41_11"></a>Genesis 41:11

And we [ḥālam](../../strongs/h/h2492.md) a [ḥălôm](../../strongs/h/h2472.md) in one [layil](../../strongs/h/h3915.md), I and he; we [ḥālam](../../strongs/h/h2492.md) each ['iysh](../../strongs/h/h376.md) according to the [piṯrôn](../../strongs/h/h6623.md) of his [ḥălôm](../../strongs/h/h2472.md).

<a name="genesis_41_12"></a>Genesis 41:12

And there was there with us a [naʿar](../../strongs/h/h5288.md), an [ʿiḇrî](../../strongs/h/h5680.md), ['ebed](../../strongs/h/h5650.md) to the [śar](../../strongs/h/h8269.md) of the [ṭabāḥ](../../strongs/h/h2876.md); and we [sāp̄ar](../../strongs/h/h5608.md) him, and he [pāṯar](../../strongs/h/h6622.md) to us our [ḥălôm](../../strongs/h/h2472.md); to each ['iysh](../../strongs/h/h376.md) according to his [ḥălôm](../../strongs/h/h2472.md) he did [pāṯar](../../strongs/h/h6622.md).

<a name="genesis_41_13"></a>Genesis 41:13

And it came to pass, as he [pāṯar](../../strongs/h/h6622.md) to us, so it was; me he [shuwb](../../strongs/h/h7725.md) unto mine [kēn](../../strongs/h/h3653.md), and him he [tālâ](../../strongs/h/h8518.md).

<a name="genesis_41_14"></a>Genesis 41:14

Then [Parʿô](../../strongs/h/h6547.md) [shalach](../../strongs/h/h7971.md) and [qara'](../../strongs/h/h7121.md) [Yôsēp̄](../../strongs/h/h3130.md), and they [rûṣ](../../strongs/h/h7323.md) him out of the [bowr](../../strongs/h/h953.md): and he [gālaḥ](../../strongs/h/h1548.md) himself, and [ḥālap̄](../../strongs/h/h2498.md) his [śimlâ](../../strongs/h/h8071.md), and [bow'](../../strongs/h/h935.md) unto [Parʿô](../../strongs/h/h6547.md).

<a name="genesis_41_15"></a>Genesis 41:15

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md) unto [Yôsēp̄](../../strongs/h/h3130.md), I have [ḥālam](../../strongs/h/h2492.md) a [ḥălôm](../../strongs/h/h2472.md), and there is none that can [pāṯar](../../strongs/h/h6622.md) it: and I have [shama'](../../strongs/h/h8085.md) ['āmar](../../strongs/h/h559.md) of thee, that thou canst [shama'](../../strongs/h/h8085.md) a [ḥălôm](../../strongs/h/h2472.md) to [pāṯar](../../strongs/h/h6622.md) it.

<a name="genesis_41_16"></a>Genesis 41:16

And [Yôsēp̄](../../strongs/h/h3130.md) ['anah](../../strongs/h/h6030.md) [Parʿô](../../strongs/h/h6547.md), ['āmar](../../strongs/h/h559.md), It is [bilʿăḏê](../../strongs/h/h1107.md): ['Elohiym](../../strongs/h/h430.md) shall ['anah](../../strongs/h/h6030.md) [Parʿô](../../strongs/h/h6547.md) an ['anah](../../strongs/h/h6030.md) of [shalowm](../../strongs/h/h7965.md).

<a name="genesis_41_17"></a>Genesis 41:17

And [Parʿô](../../strongs/h/h6547.md) [dabar](../../strongs/h/h1696.md) unto [Yôsēp̄](../../strongs/h/h3130.md), In my [ḥălôm](../../strongs/h/h2472.md), behold, I ['amad](../../strongs/h/h5975.md) upon the [saphah](../../strongs/h/h8193.md) of the [yᵊ'ōr](../../strongs/h/h2975.md):

<a name="genesis_41_18"></a>Genesis 41:18

And, behold, there [ʿālâ](../../strongs/h/h5927.md) out of the [yᵊ'ōr](../../strongs/h/h2975.md) seven [pārâ](../../strongs/h/h6510.md), [bārî'](../../strongs/h/h1277.md) [basar](../../strongs/h/h1320.md) and [yāp̄ê](../../strongs/h/h3303.md) [tō'ar](../../strongs/h/h8389.md); and they [ra'ah](../../strongs/h/h7462.md) in an ['āḥû](../../strongs/h/h260.md):

<a name="genesis_41_19"></a>Genesis 41:19

And, behold, seven ['aḥēr](../../strongs/h/h312.md) [pārâ](../../strongs/h/h6510.md) [ʿālâ](../../strongs/h/h5927.md) ['aḥar](../../strongs/h/h310.md) them, [dallâ](../../strongs/h/h1803.md) and [me'od](../../strongs/h/h3966.md) [ra'](../../strongs/h/h7451.md) [tō'ar](../../strongs/h/h8389.md) and [raq](../../strongs/h/h7534.md) [basar](../../strongs/h/h1320.md), such as I never [ra'ah](../../strongs/h/h7200.md) in all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) for [rōaʿ](../../strongs/h/h7455.md):

<a name="genesis_41_20"></a>Genesis 41:20

And the [raq](../../strongs/h/h7534.md) and the [ra'](../../strongs/h/h7451.md) [pārâ](../../strongs/h/h6510.md) did ['akal](../../strongs/h/h398.md) up the [ri'šôn](../../strongs/h/h7223.md) seven [bārî'](../../strongs/h/h1277.md) [pārâ](../../strongs/h/h6510.md):

<a name="genesis_41_21"></a>Genesis 41:21

And when they had [bow'](../../strongs/h/h935.md) [qereḇ](../../strongs/h/h7130.md), it could not be [yada'](../../strongs/h/h3045.md) that they had [bow'](../../strongs/h/h935.md) [qereḇ](../../strongs/h/h7130.md); but they were [mar'ê](../../strongs/h/h4758.md) [ra'](../../strongs/h/h7451.md), as at the [tᵊḥillâ](../../strongs/h/h8462.md). So I [yāqaṣ](../../strongs/h/h3364.md).

<a name="genesis_41_22"></a>Genesis 41:22

And I [ra'ah](../../strongs/h/h7200.md) in my [ḥălôm](../../strongs/h/h2472.md), and, behold, seven [šibōleṯ](../../strongs/h/h7641.md) [ʿālâ](../../strongs/h/h5927.md) in one [qānê](../../strongs/h/h7070.md), [mālē'](../../strongs/h/h4392.md) and [towb](../../strongs/h/h2896.md):

<a name="genesis_41_23"></a>Genesis 41:23

And, behold, seven [šibōleṯ](../../strongs/h/h7641.md), [ṣānam](../../strongs/h/h6798.md), [daq](../../strongs/h/h1851.md), and [šāḏap̄](../../strongs/h/h7710.md) with the [qāḏîm](../../strongs/h/h6921.md), [ṣāmaḥ](../../strongs/h/h6779.md) ['aḥar](../../strongs/h/h310.md) them:

<a name="genesis_41_24"></a>Genesis 41:24

And the [daq](../../strongs/h/h1851.md) [šibōleṯ](../../strongs/h/h7641.md) [bālaʿ](../../strongs/h/h1104.md) the seven [towb](../../strongs/h/h2896.md) [šibōleṯ](../../strongs/h/h7641.md): and I ['āmar](../../strongs/h/h559.md) this unto the [ḥarṭōm](../../strongs/h/h2748.md); but there was none that could [nāḡaḏ](../../strongs/h/h5046.md) it to me.

<a name="genesis_41_25"></a>Genesis 41:25

And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto [Parʿô](../../strongs/h/h6547.md), The [ḥălôm](../../strongs/h/h2472.md) of [Parʿô](../../strongs/h/h6547.md) is ['echad](../../strongs/h/h259.md): ['Elohiym](../../strongs/h/h430.md) hath [nāḡaḏ](../../strongs/h/h5046.md) [Parʿô](../../strongs/h/h6547.md) what he is about to ['asah](../../strongs/h/h6213.md).

<a name="genesis_41_26"></a>Genesis 41:26

The seven [towb](../../strongs/h/h2896.md) [pārâ](../../strongs/h/h6510.md) are seven [šānâ](../../strongs/h/h8141.md); and the seven [towb](../../strongs/h/h2896.md) [šibōleṯ](../../strongs/h/h7641.md) are seven [šānâ](../../strongs/h/h8141.md): the [ḥălôm](../../strongs/h/h2472.md) is ['echad](../../strongs/h/h259.md).

<a name="genesis_41_27"></a>Genesis 41:27

And the seven [raq](../../strongs/h/h7534.md) and [ra'](../../strongs/h/h7451.md) [pārâ](../../strongs/h/h6510.md) that [ʿālâ](../../strongs/h/h5927.md) ['aḥar](../../strongs/h/h310.md) them are seven [šānâ](../../strongs/h/h8141.md); and the seven [reyq](../../strongs/h/h7386.md) [šibōleṯ](../../strongs/h/h7641.md) [šāḏap̄](../../strongs/h/h7710.md) with the [qāḏîm](../../strongs/h/h6921.md) shall be seven [šānâ](../../strongs/h/h8141.md) of [rāʿāḇ](../../strongs/h/h7458.md).

<a name="genesis_41_28"></a>Genesis 41:28

This is the [dabar](../../strongs/h/h1697.md) which I have [dabar](../../strongs/h/h1696.md) unto [Parʿô](../../strongs/h/h6547.md): What ['Elohiym](../../strongs/h/h430.md) is about to ['asah](../../strongs/h/h6213.md) he [ra'ah](../../strongs/h/h7200.md) unto [Parʿô](../../strongs/h/h6547.md).

<a name="genesis_41_29"></a>Genesis 41:29

Behold, there [bow'](../../strongs/h/h935.md) seven [šānâ](../../strongs/h/h8141.md) of [gadowl](../../strongs/h/h1419.md) [śāḇāʿ](../../strongs/h/h7647.md) throughout all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md):

<a name="genesis_41_30"></a>Genesis 41:30

And there shall [quwm](../../strongs/h/h6965.md) ['aḥar](../../strongs/h/h310.md) them seven [šānâ](../../strongs/h/h8141.md) of [rāʿāḇ](../../strongs/h/h7458.md); and all the [śāḇāʿ](../../strongs/h/h7647.md) shall be [shakach](../../strongs/h/h7911.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md); and the [rāʿāḇ](../../strongs/h/h7458.md) shall [kalah](../../strongs/h/h3615.md) the ['erets](../../strongs/h/h776.md);

<a name="genesis_41_31"></a>Genesis 41:31

And the [śāḇāʿ](../../strongs/h/h7647.md) shall not be [yada'](../../strongs/h/h3045.md) in the ['erets](../../strongs/h/h776.md) by [paniym](../../strongs/h/h6440.md) of that [rāʿāḇ](../../strongs/h/h7458.md) ['aḥar](../../strongs/h/h310.md); for it shall be [me'od](../../strongs/h/h3966.md) [kāḇēḏ](../../strongs/h/h3515.md).

<a name="genesis_41_32"></a>Genesis 41:32

And for that the [ḥălôm](../../strongs/h/h2472.md) was [šānâ](../../strongs/h/h8138.md) unto [Parʿô](../../strongs/h/h6547.md) [pa'am](../../strongs/h/h6471.md); it is because the [dabar](../../strongs/h/h1697.md) is [kuwn](../../strongs/h/h3559.md) by ['Elohiym](../../strongs/h/h430.md), and ['Elohiym](../../strongs/h/h430.md) will [māhar](../../strongs/h/h4116.md) ['asah](../../strongs/h/h6213.md).

<a name="genesis_41_33"></a>Genesis 41:33

Now therefore let [Parʿô](../../strongs/h/h6547.md) [ra'ah](../../strongs/h/h7200.md) an ['iysh](../../strongs/h/h376.md) [bîn](../../strongs/h/h995.md) and [ḥāḵām](../../strongs/h/h2450.md), and [shiyth](../../strongs/h/h7896.md) him over the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_41_34"></a>Genesis 41:34

Let [Parʿô](../../strongs/h/h6547.md) ['asah](../../strongs/h/h6213.md) this, and let him [paqad](../../strongs/h/h6485.md) [pāqîḏ](../../strongs/h/h6496.md) over the ['erets](../../strongs/h/h776.md), and [ḥāmaš](../../strongs/h/h2567.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) in the seven [śāḇāʿ](../../strongs/h/h7647.md) [šānâ](../../strongs/h/h8141.md).

<a name="genesis_41_35"></a>Genesis 41:35

And let them [qāḇaṣ](../../strongs/h/h6908.md) all the ['ōḵel](../../strongs/h/h400.md) of those [towb](../../strongs/h/h2896.md) [šānâ](../../strongs/h/h8141.md) that [bow'](../../strongs/h/h935.md), and [ṣāḇar](../../strongs/h/h6651.md) [bar](../../strongs/h/h1250.md) under the [yad](../../strongs/h/h3027.md) of [Parʿô](../../strongs/h/h6547.md), and let them [shamar](../../strongs/h/h8104.md) ['ōḵel](../../strongs/h/h400.md) in the [ʿîr](../../strongs/h/h5892.md).

<a name="genesis_41_36"></a>Genesis 41:36

And that ['ōḵel](../../strongs/h/h400.md) shall be for [piqqāḏôn](../../strongs/h/h6487.md) to the ['erets](../../strongs/h/h776.md) against the seven [šānâ](../../strongs/h/h8141.md) of [rāʿāḇ](../../strongs/h/h7458.md), which shall be in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md); that the ['erets](../../strongs/h/h776.md) [karath](../../strongs/h/h3772.md) not through the [rāʿāḇ](../../strongs/h/h7458.md).

<a name="genesis_41_37"></a>Genesis 41:37

And the [dabar](../../strongs/h/h1697.md) was [yatab](../../strongs/h/h3190.md) in the ['ayin](../../strongs/h/h5869.md) of [Parʿô](../../strongs/h/h6547.md), and in the ['ayin](../../strongs/h/h5869.md) of all his ['ebed](../../strongs/h/h5650.md).

<a name="genesis_41_38"></a>Genesis 41:38

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md) unto his ['ebed](../../strongs/h/h5650.md), Can we [māṣā'](../../strongs/h/h4672.md) such a one as this is, an ['iysh](../../strongs/h/h376.md) in whom the [ruwach](../../strongs/h/h7307.md) of ['Elohiym](../../strongs/h/h430.md) is?

<a name="genesis_41_39"></a>Genesis 41:39

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md) unto [Yôsēp̄](../../strongs/h/h3130.md), ['aḥar](../../strongs/h/h310.md) as ['Elohiym](../../strongs/h/h430.md) hath [yada'](../../strongs/h/h3045.md) thee all this, there is none so [bîn](../../strongs/h/h995.md) and [ḥāḵām](../../strongs/h/h2450.md) as thou art:

<a name="genesis_41_40"></a>Genesis 41:40

Thou shalt be over my [bayith](../../strongs/h/h1004.md), and according unto thy [peh](../../strongs/h/h6310.md) shall all my ['am](../../strongs/h/h5971.md) be [nashaq](../../strongs/h/h5401.md): only in the [kicce'](../../strongs/h/h3678.md) will I be [gāḏal](../../strongs/h/h1431.md) than thou.

<a name="genesis_41_41"></a>Genesis 41:41

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md) unto [Yôsēp̄](../../strongs/h/h3130.md), [ra'ah](../../strongs/h/h7200.md), I have [nathan](../../strongs/h/h5414.md) thee over all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_41_42"></a>Genesis 41:42

And [Parʿô](../../strongs/h/h6547.md) [cuwr](../../strongs/h/h5493.md) his [ṭabaʿaṯ](../../strongs/h/h2885.md) from his [yad](../../strongs/h/h3027.md), and [nathan](../../strongs/h/h5414.md) it upon [Yôsēp̄](../../strongs/h/h3130.md) [yad](../../strongs/h/h3027.md), and [labash](../../strongs/h/h3847.md) him in [beḡeḏ](../../strongs/h/h899.md) of [šēš](../../strongs/h/h8336.md), and [śûm](../../strongs/h/h7760.md) a [zāhāḇ](../../strongs/h/h2091.md) [rāḇîḏ](../../strongs/h/h7242.md) about his [ṣaûā'r](../../strongs/h/h6677.md);

<a name="genesis_41_43"></a>Genesis 41:43

And he made him to [rāḵaḇ](../../strongs/h/h7392.md) in the [mišnê](../../strongs/h/h4932.md) [merkāḇâ](../../strongs/h/h4818.md) which he had; and they [qara'](../../strongs/h/h7121.md) [paniym](../../strongs/h/h6440.md) him, ['aḇrēḵ](../../strongs/h/h86.md): and he [nathan](../../strongs/h/h5414.md) him ruler over all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_41_44"></a>Genesis 41:44

And [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md) unto [Yôsēp̄](../../strongs/h/h3130.md), I am [Parʿô](../../strongs/h/h6547.md), and [bilʿăḏê](../../strongs/h/h1107.md) thee shall no ['iysh](../../strongs/h/h376.md) [ruwm](../../strongs/h/h7311.md) his [yad](../../strongs/h/h3027.md) or [regel](../../strongs/h/h7272.md) in all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_41_45"></a>Genesis 41:45

And [Parʿô](../../strongs/h/h6547.md) [qara'](../../strongs/h/h7121.md) [Yôsēp̄](../../strongs/h/h3130.md) [shem](../../strongs/h/h8034.md) [ṣāp̄naṯ paʿnēaḥ](../../strongs/h/h6847.md); and he [nathan](../../strongs/h/h5414.md) him to ['ishshah](../../strongs/h/h802.md) ['āsnaṯ](../../strongs/h/h621.md) the [bath](../../strongs/h/h1323.md) of [pôṭî p̄eraʿ](../../strongs/h/h6319.md) [kōhēn](../../strongs/h/h3548.md) of ['ôn](../../strongs/h/h204.md). And [Yôsēp̄](../../strongs/h/h3130.md) [yāṣā'](../../strongs/h/h3318.md) over all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_41_46"></a>Genesis 41:46

And [Yôsēp̄](../../strongs/h/h3130.md) was thirty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he ['amad](../../strongs/h/h5975.md) before [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md). And [Yôsēp̄](../../strongs/h/h3130.md) [yāṣā'](../../strongs/h/h3318.md) from the [paniym](../../strongs/h/h6440.md) of [Parʿô](../../strongs/h/h6547.md), and ['abar](../../strongs/h/h5674.md) all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_41_47"></a>Genesis 41:47

And in the seven [śāḇāʿ](../../strongs/h/h7647.md) [šānâ](../../strongs/h/h8141.md) the ['erets](../../strongs/h/h776.md) ['asah](../../strongs/h/h6213.md) by [qōmeṣ](../../strongs/h/h7062.md).

<a name="genesis_41_48"></a>Genesis 41:48

And he [qāḇaṣ](../../strongs/h/h6908.md) all the ['ōḵel](../../strongs/h/h400.md) of the seven [šānâ](../../strongs/h/h8141.md), which were in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and [nathan](../../strongs/h/h5414.md) the ['ōḵel](../../strongs/h/h400.md) in the [ʿîr](../../strongs/h/h5892.md): the ['ōḵel](../../strongs/h/h400.md) of the [sadeh](../../strongs/h/h7704.md), which was [cabiyb](../../strongs/h/h5439.md) every [ʿîr](../../strongs/h/h5892.md), [nathan](../../strongs/h/h5414.md) he in the [tavek](../../strongs/h/h8432.md).

<a name="genesis_41_49"></a>Genesis 41:49

And [Yôsēp̄](../../strongs/h/h3130.md) [ṣāḇar](../../strongs/h/h6651.md) [bar](../../strongs/h/h1250.md) as the [ḥôl](../../strongs/h/h2344.md) of the [yam](../../strongs/h/h3220.md), [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md), until he [ḥāḏal](../../strongs/h/h2308.md) [sāp̄ar](../../strongs/h/h5608.md); for it was without [mispār](../../strongs/h/h4557.md).

<a name="genesis_41_50"></a>Genesis 41:50

And unto [Yôsēp̄](../../strongs/h/h3130.md) were [yalad](../../strongs/h/h3205.md) two [ben](../../strongs/h/h1121.md) before the [šānâ](../../strongs/h/h8141.md) of [rāʿāḇ](../../strongs/h/h7458.md) [bow'](../../strongs/h/h935.md), which ['āsnaṯ](../../strongs/h/h621.md) the [bath](../../strongs/h/h1323.md) of [pôṭî p̄eraʿ](../../strongs/h/h6319.md) [kōhēn](../../strongs/h/h3548.md) of ['ôn](../../strongs/h/h204.md) [yalad](../../strongs/h/h3205.md) unto him.

<a name="genesis_41_51"></a>Genesis 41:51

And [Yôsēp̄](../../strongs/h/h3130.md) [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of the [bᵊḵôr](../../strongs/h/h1060.md) [Mᵊnaššê](../../strongs/h/h4519.md): For ['Elohiym](../../strongs/h/h430.md), said he, hath made me [nāšâ](../../strongs/h/h5382.md) all my ['amal](../../strongs/h/h5999.md), and all my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md).

<a name="genesis_41_52"></a>Genesis 41:52

And the [shem](../../strongsstrongs/h/h8034.md) of the second [qara'](../../strongs/h/h7121.md) he ['Ep̄rayim](../../strongs/h/h669.md): For ['Elohiym](../../strongs/h/h430.md) hath caused me to be [parah](../../strongs/h/h6509.md) in the ['erets](../../strongs/h/h776.md) of my ['oniy](../../strongs/h/h6040.md).

<a name="genesis_41_53"></a>Genesis 41:53

And the seven [šānâ](../../strongs/h/h8141.md) of [śāḇāʿ](../../strongs/h/h7647.md), that was in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), were [kalah](../../strongs/h/h3615.md).

<a name="genesis_41_54"></a>Genesis 41:54

And the seven [šānâ](../../strongs/h/h8141.md) of [rāʿāḇ](../../strongs/h/h7458.md) [ḥālal](../../strongs/h/h2490.md) to [bow'](../../strongs/h/h935.md), according as [Yôsēp̄](../../strongs/h/h3130.md) had ['āmar](../../strongs/h/h559.md): and the [rāʿāḇ](../../strongs/h/h7458.md) was in all ['erets](../../strongs/h/h776.md); but in all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) there was [lechem](../../strongs/h/h3899.md).

<a name="genesis_41_55"></a>Genesis 41:55

And when all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) was [rāʿēḇ](../../strongs/h/h7456.md), the ['am](../../strongs/h/h5971.md) [ṣāʿaq](../../strongs/h/h6817.md) to [Parʿô](../../strongs/h/h6547.md) for [lechem](../../strongs/h/h3899.md): and [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md) unto all the [Mitsrayim](../../strongs/h/h4714.md), [yālaḵ](../../strongs/h/h3212.md) unto [Yôsēp̄](../../strongs/h/h3130.md); what he ['āmar](../../strongs/h/h559.md) to you, ['asah](../../strongs/h/h6213.md).

<a name="genesis_41_56"></a>Genesis 41:56

And the [rāʿāḇ](../../strongs/h/h7458.md) was over all the [paniym](../../strongs/h/h6440.md) of the ['erets](../../strongs/h/h776.md): and [Yôsēp̄](../../strongs/h/h3130.md) [pāṯaḥ](../../strongs/h/h6605.md) all the storehouses, and [šāḇar](../../strongs/h/h7666.md) unto the [Mitsrayim](../../strongs/h/h4714.md); and the [rāʿāḇ](../../strongs/h/h7458.md) [ḥāzaq](../../strongs/h/h2388.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="genesis_41_57"></a>Genesis 41:57

And all ['erets](../../strongs/h/h776.md) [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md) to [Yôsēp̄](../../strongs/h/h3130.md) for to [šāḇar](../../strongs/h/h7666.md); because that the [rāʿāḇ](../../strongs/h/h7458.md) was so [ḥāzaq](../../strongs/h/h2388.md) in all ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 40](genesis_40.md) - [Genesis 42](genesis_42.md)