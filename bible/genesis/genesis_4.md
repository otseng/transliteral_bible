# [Genesis 4](https://www.blueletterbible.org/kjv/gen/4/1/s_4001)

<a name="genesis_4_1"></a>Genesis 4:1

And ['adam](../../strongs/h/h120.md) [yada'](../../strongs/h/h3045.md) [Chavvah](../../strongs/h/h2332.md) his ['ishshah](../../strongs/h/h802.md); and she [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) [Qayin](../../strongs/h/h7014.md), and ['āmar](../../strongs/h/h559.md), I have [qānâ](../../strongs/h/h7069.md) an ['iysh](../../strongs/h/h376.md) from [Yĕhovah](../../strongs/h/h3068.md).

<a name="genesis_4_2"></a>Genesis 4:2

And she again [yalad](../../strongs/h/h3205.md) his ['ach](../../strongs/h/h251.md) [Hebel](../../strongs/h/h1893.md). And [Hebel](../../strongs/h/h1893.md) was a [ra'ah](../../strongs/h/h7462.md) of [tso'n](../../strongs/h/h6629.md), but [Qayin](../../strongs/h/h7014.md) was an ['abad](../../strongs/h/h5647.md) of the ['adamah](../../strongs/h/h127.md).

<a name="genesis_4_3"></a>Genesis 4:3

And in [qēṣ](../../strongs/h/h7093.md) of [yowm](../../strongs/h/h3117.md), that [Qayin](../../strongs/h/h7014.md) [bow'](../../strongs/h/h935.md) of the [pĕriy](../../strongs/h/h6529.md) of ['adamah](../../strongs/h/h127.md) a [minchah](../../strongs/h/h4503.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="genesis_4_4"></a>Genesis 4:4

And [Hebel](../../strongs/h/h1893.md), he also [bow'](../../strongs/h/h935.md) of the [bᵊḵôrâ](../../strongs/h/h1062.md) of his [tso'n](../../strongs/h/h6629.md) and of the [cheleb](../../strongs/h/h2459.md) thereof. And [Yĕhovah](../../strongs/h/h3068.md) had [šāʿâ](../../strongs/h/h8159.md) unto [Hebel](../../strongs/h/h1893.md) and to his [minchah](../../strongs/h/h4503.md):

<a name="genesis_4_5"></a>Genesis 4:5

But unto [Qayin](../../strongs/h/h7014.md) and to his [minchah](../../strongs/h/h4503.md) he had not [šāʿâ](../../strongs/h/h8159.md). And [Qayin](../../strongs/h/h7014.md) was [me'od](../../strongs/h/h3966.md) [ḥārâ](../../strongs/h/h2734.md), and his [paniym](../../strongs/h/h6440.md) [naphal](../../strongs/h/h5307.md).

<a name="genesis_4_6"></a>Genesis 4:6

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Qayin](../../strongs/h/h7014.md), Why art thou [ḥārâ](../../strongs/h/h2734.md)? and why is thy [paniym](../../strongs/h/h6440.md) [naphal](../../strongs/h/h5307.md)?

<a name="genesis_4_7"></a>Genesis 4:7

If thou [yatab](../../strongs/h/h3190.md), shalt thou not be [śᵊ'ēṯ](../../strongs/h/h7613.md)? and if thou not [yatab](../../strongs/h/h3190.md), [chatta'ath](../../strongs/h/h2403.md) [rāḇaṣ](../../strongs/h/h7257.md) at the [peṯaḥ](../../strongs/h/h6607.md). And unto thee shall be his [tᵊšûqâ](../../strongs/h/h8669.md), and thou shalt [mashal](../../strongs/h/h4910.md) over him. [^1]

<a name="genesis_4_8"></a>Genesis 4:8

And [Qayin](../../strongs/h/h7014.md) ['āmar](../../strongs/h/h559.md) with [Hebel](../../strongs/h/h1893.md) his ['ach](../../strongs/h/h251.md): and it came to pass, when they were in the [sadeh](../../strongs/h/h7704.md), that [Qayin](../../strongs/h/h7014.md) [quwm](../../strongs/h/h6965.md) against [Hebel](../../strongs/h/h1893.md) his ['ach](../../strongs/h/h251.md), and [harag](../../strongs/h/h2026.md) him.

<a name="genesis_4_9"></a>Genesis 4:9

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Qayin](../../strongs/h/h7014.md), ['ay](../../strongs/h/h335.md) is [Hebel](../../strongs/h/h1893.md) thy ['ach](../../strongs/h/h251.md)? And he ['āmar](../../strongs/h/h559.md), I [yada'](../../strongs/h/h3045.md) not: Am I my ['ach](../../strongs/h/h251.md) [shamar](../../strongs/h/h8104.md)?

<a name="genesis_4_10"></a>Genesis 4:10

And he ['āmar](../../strongs/h/h559.md), What hast thou ['asah](../../strongs/h/h6213.md)? the [qowl](../../strongs/h/h6963.md) of thy ['ach](../../strongs/h/h251.md) [dam](../../strongs/h/h1818.md) [ṣāʿaq](../../strongs/h/h6817.md) unto me from the ['adamah](../../strongs/h/h127.md).

<a name="genesis_4_11"></a>Genesis 4:11

And now art thou ['arar](../../strongs/h/h779.md) from the ['ăḏāmâ](../../strongs/h/h127.md), which hath [pāṣâ](../../strongs/h/h6475.md) her [peh](../../strongs/h/h6310.md) to [laqach](../../strongs/h/h3947.md) thy ['ach](../../strongs/h/h251.md) [dam](../../strongs/h/h1818.md) from thy [yad](../../strongs/h/h3027.md);

<a name="genesis_4_12"></a>Genesis 4:12

When thou ['abad](../../strongs/h/h5647.md) the ['adamah](../../strongs/h/h127.md), it shall not [yāsap̄](../../strongs/h/h3254.md) [nathan](../../strongs/h/h5414.md) unto thee her [koach](../../strongs/h/h3581.md); a [nûaʿ](../../strongs/h/h5128.md) and a [nuwd](../../strongs/h/h5110.md) shalt thou be in the ['erets](../../strongs/h/h776.md).

<a name="genesis_4_13"></a>Genesis 4:13

And [Qayin](../../strongs/h/h7014.md) ['āmar](../../strongs/h/h559.md) unto [Yĕhovah](../../strongs/h/h3068.md), My ['avon](../../strongs/h/h5771.md) is [gadowl](../../strongs/h/h1419.md) than I can [nasa'](../../strongs/h/h5375.md).

<a name="genesis_4_14"></a>Genesis 4:14

Behold, thou hast [gāraš](../../strongs/h/h1644.md) me this [yowm](../../strongs/h/h3117.md) from the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md); and from thy [paniym](../../strongs/h/h6440.md) shall I be [cathar](../../strongs/h/h5641.md); and I shall be a [nûaʿ](../../strongs/h/h5128.md) and a [nuwd](../../strongs/h/h5110.md) in the ['erets](../../strongs/h/h776.md); and it shall come to pass, that every one that [māṣā'](../../strongs/h/h4672.md) me shall [harag](../../strongs/h/h2026.md) me.

<a name="genesis_4_15"></a>Genesis 4:15

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, Therefore whosoever [harag](../../strongs/h/h2026.md) [Qayin](../../strongs/h/h7014.md), [naqam](../../strongs/h/h5358.md) shall be taken on him sevenfold. And [Yĕhovah](../../strongs/h/h3068.md) [śûm](../../strongs/h/h7760.md) an ['ôṯ](../../strongs/h/h226.md) upon [Qayin](../../strongs/h/h7014.md), lest any [māṣā'](../../strongs/h/h4672.md) him should [nakah](../../strongs/h/h5221.md) him.

<a name="genesis_4_16"></a>Genesis 4:16

And [Qayin](../../strongs/h/h7014.md) [yāṣā'](../../strongs/h/h3318.md) from the [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md), and [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [nôḏ](../../strongs/h/h5113.md), on the [qḏmh](../../strongs/h/h6926.md) of ['Eden](../../strongs/h/h5731.md).

<a name="genesis_4_17"></a>Genesis 4:17

And [Qayin](../../strongs/h/h7014.md) [yada'](../../strongs/h/h3045.md) his ['ishshah](../../strongs/h/h802.md); and she [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) [Ḥănôḵ](../../strongs/h/h2585.md): and he [bānâ](../../strongs/h/h1129.md) an [ʿîr](../../strongs/h/h5892.md), and [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of the [ʿîr](../../strongs/h/h5892.md), after the [shem](../../strongs/h/h8034.md) of his [ben](../../strongs/h/h1121.md), [Ḥănôḵ](../../strongs/h/h2585.md).

<a name="genesis_4_18"></a>Genesis 4:18

And unto [Ḥănôḵ](../../strongs/h/h2585.md) [yalad](../../strongs/h/h3205.md) [ʿîrāḏ](../../strongs/h/h5897.md): and [ʿîrāḏ](../../strongs/h/h5897.md) [yalad](../../strongs/h/h3205.md) [mᵊḥûyā'ēl](../../strongs/h/h4232.md): and [mᵊḥûyā'ēl](../../strongs/h/h4232.md) [yalad](../../strongs/h/h3205.md) [mᵊṯûšā'ēl](../../strongs/h/h4967.md): and [mᵊṯûšā'ēl](../../strongs/h/h4967.md) [yalad](../../strongs/h/h3205.md) [Lemeḵ](../../strongs/h/h3929.md).

<a name="genesis_4_19"></a>Genesis 4:19

And [Lemeḵ](../../strongs/h/h3929.md) [laqach](../../strongs/h/h3947.md) unto him two ['ishshah](../../strongs/h/h802.md): the [shem](../../strongs/h/h8034.md) of the one was [ʿāḏâ](../../strongs/h/h5711.md), and the [shem](../../strongs/h/h8034.md) of the other [Ṣillâ](../../strongs/h/h6741.md).

<a name="genesis_4_20"></a>Genesis 4:20

And [ʿāḏâ](../../strongs/h/h5711.md) [yalad](../../strongs/h/h3205.md) [Yāḇāl](../../strongs/h/h2989.md): he was the ['ab](../../strongs/h/h1.md) of such as [yashab](../../strongs/h/h3427.md) in ['ohel](../../strongs/h/h168.md), and of such as have [miqnê](../../strongs/h/h4735.md).

<a name="genesis_4_21"></a>Genesis 4:21

And his ['ach](../../strongs/h/h251.md) [shem](../../strongs/h/h8034.md) was [Yûḇāl](../../strongs/h/h3106.md): he was the ['ab](../../strongs/h/h1.md) of all such as [tāp̄aś](../../strongs/h/h8610.md) the [kinnôr](../../strongs/h/h3658.md) and [ʿûḡāḇ](../../strongs/h/h5748.md).

<a name="genesis_4_22"></a>Genesis 4:22

And [Ṣillâ](../../strongs/h/h6741.md), she also [yalad](../../strongs/h/h3205.md) [Tûḇal qayin](../../strongs/h/h8423.md), a [lāṭaš](../../strongs/h/h3913.md) of every [ḥōrēš](../../strongs/h/h2794.md) in [nᵊḥšeṯ](../../strongs/h/h5178.md) and [barzel](../../strongs/h/h1270.md): and the ['āḥôṯ](../../strongs/h/h269.md) of [Tûḇal qayin](../../strongs/h/h8423.md) was [naʿămâ](../../strongs/h/h5279.md).

<a name="genesis_4_23"></a>Genesis 4:23

And [Lemeḵ](../../strongs/h/h3929.md) ['āmar](../../strongs/h/h559.md) unto his ['ishshah](../../strongs/h/h802.md), [ʿāḏâ](../../strongs/h/h5711.md) and [Ṣillâ](../../strongs/h/h6741.md), [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md); ye ['ishshah](../../strongs/h/h802.md) of [Lemeḵ](../../strongs/h/h3929.md), ['azan](../../strongs/h/h238.md) unto my ['imrah](../../strongs/h/h565.md): for I have [harag](../../strongs/h/h2026.md) an ['iysh](../../strongs/h/h376.md) to my [peṣaʿ](../../strongs/h/h6482.md), and a [yeleḏ](../../strongs/h/h3206.md) to my [ḥabûrâ](../../strongs/h/h2250.md).

<a name="genesis_4_24"></a>Genesis 4:24

If [Qayin](../../strongs/h/h7014.md) shall be [naqam](../../strongs/h/h5358.md) sevenfold, truly [Lemeḵ](../../strongs/h/h3929.md) seventy and sevenfold.

<a name="genesis_4_25"></a>Genesis 4:25

And ['adam](../../strongs/h/h120.md) [yada'](../../strongs/h/h3045.md) his ['ishshah](../../strongs/h/h802.md) [ʿôḏ](../../strongs/h/h5750.md); and she [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md), and [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Šēṯ](../../strongs/h/h8352.md): For ['Elohiym](../../strongs/h/h430.md), said she, hath [shiyth](../../strongs/h/h7896.md) me ['aḥēr](../../strongs/h/h312.md) [zera'](../../strongs/h/h2233.md) instead of [Hebel](../../strongs/h/h1893.md), whom [Qayin](../../strongs/h/h7014.md) [harag](../../strongs/h/h2026.md).

<a name="genesis_4_26"></a>Genesis 4:26

And to [Šēṯ](../../strongs/h/h8352.md), to him also there was [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md); and he [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) ['Ĕnôš](../../strongs/h/h583.md): then [ḥālal](../../strongs/h/h2490.md) to [qara'](../../strongs/h/h7121.md) upon the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 3](genesis_3.md) - [Genesis 5](genesis_5.md)

---

[^1]: [Genesis 4:7 Commentary](../../commentary/genesis/genesis_4_commentary.md#genesis_4_7)
