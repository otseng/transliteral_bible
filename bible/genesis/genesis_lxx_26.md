# [Genesis 26](https://www.blueletterbible.org/lxx/genesis/26)

<a name="genesis_26_1"></a>Genesis 26:1

And [ginomai](../../strongs/g/g1096.md) a [limos](../../strongs/g/g3042.md) upon the [gē](../../strongs/g/g1093.md), [chōris](../../strongs/g/g5565.md) from the [limos](../../strongs/g/g3042.md) [proteros](../../strongs/g/g4387.md), which happened in the [chronos](../../strongs/g/g5550.md) of [Abraam](../../strongs/g/g11.md). [poreuō](../../strongs/g/g4198.md) [Isaak](../../strongs/g/g2464.md) to Abimelech [basileus](../../strongs/g/g935.md) of the Philistines in Gerar.

<a name="genesis_26_2"></a>Genesis 26:2

[horaō](../../strongs/g/g3708.md) to him the [kyrios](../../strongs/g/g2962.md), and [eipon](../../strongs/g/g2036.md), You should not go [katabainō](../../strongs/g/g2597.md) into [Aigyptos](../../strongs/g/g125.md), but [katoikeō](../../strongs/g/g2730.md) in the [gē](../../strongs/g/g1093.md)! which ever I should [eipon](../../strongs/g/g2036.md) you.

<a name="genesis_26_3"></a>Genesis 26:3

And [paroikeō](../../strongs/g/g3939.md) in this [gē](../../strongs/g/g1093.md)! And I will be with you, and I will [eulogeō](../../strongs/g/g2127.md) you. For to you and your [sperma](../../strongs/g/g4690.md) I will [didōmi](../../strongs/g/g1325.md) all this [gē](../../strongs/g/g1093.md). And I will [histēmi](../../strongs/g/g2476.md) my [horkos](../../strongs/g/g3727.md), which I [omnyō](../../strongs/g/g3660.md) to [Abraam](../../strongs/g/g11.md) your [patēr](../../strongs/g/g3962.md). 

<a name="genesis_26_4"></a>Genesis 26:4

And I will [plēthynō](../../strongs/g/g4129.md) your [sperma](../../strongs/g/g4690.md) as the [astēr](../../strongs/g/g792.md) of the [ouranos](../../strongs/g/g3772.md). And I will [didōmi](../../strongs/g/g1325.md) your [sperma](../../strongs/g/g4690.md) all this [gē](../../strongs/g/g1093.md). And shall be [eulogeō](../../strongs/g/g2127.md) by your [sperma](../../strongs/g/g4690.md) all the [ethnos](../../strongs/g/g1484.md) of the [gē](../../strongs/g/g1093.md); 

<a name="genesis_26_5"></a>Genesis 26:5

because [hypakouō](../../strongs/g/g5219.md) [Abraam](../../strongs/g/g11.md) your [patēr](../../strongs/g/g3962.md) my [phōnē](../../strongs/g/g5456.md), and [phylassō](../../strongs/g/g5442.md) my orders (prostagmata), and my [entolē](../../strongs/g/g1785.md), and my [dikaiōma](../../strongs/g/g1345.md), and my laws (nomima).

<a name="genesis_26_6"></a>Genesis 26:6

[katoikeō](../../strongs/g/g2730.md) [Isaak](../../strongs/g/g2464.md) in Gerar.

<a name="genesis_26_7"></a>Genesis 26:7

[eperōtaō](../../strongs/g/g1905.md) the [anēr](../../strongs/g/g435.md) of the [topos](../../strongs/g/g5117.md) concerning [Rhebekka](../../strongs/g/g4479.md) his [gynē](../../strongs/g/g1135.md). And he [eipon](../../strongs/g/g2036.md), [adelphē](../../strongs/g/g79.md) She is my, for he [phobeō](../../strongs/g/g5399.md) to [eipon](../../strongs/g/g2036.md) that, [gynē](../../strongs/g/g1135.md) She is my, lest at any time should [apokteinō](../../strongs/g/g615.md) him the [anēr](../../strongs/g/g435.md) of the [topos](../../strongs/g/g5117.md) on account of [Rhebekka](../../strongs/g/g4479.md); for [hōraios](../../strongs/g/g5611.md) her [opsis](../../strongs/g/g3799.md) was.

<a name="genesis_26_8"></a>Genesis 26:8

And he became long-lived (polychronios) there. And [parakyptō](../../strongs/g/g3879.md) Abimelech the [basileus](../../strongs/g/g935.md) of Gerar through the [thyris](../../strongs/g/g2376.md), and [eidō](../../strongs/g/g1492.md) [Isaak](../../strongs/g/g2464.md) [paizō](../../strongs/g/g3815.md) with [Rhebekka](../../strongs/g/g4479.md) his [gynē](../../strongs/g/g1135.md). 

<a name="genesis_26_9"></a>Genesis 26:9

[kaleō](../../strongs/g/g2564.md) Abimelech [Isaak](../../strongs/g/g2464.md), and [eipon](../../strongs/g/g2036.md) to him, So indeed your [gynē](../../strongs/g/g1135.md) is she? Why is it that you [eipon](../../strongs/g/g2036.md), [adelphē](../../strongs/g/g79.md) She is my? [eipon](../../strongs/g/g2036.md) to him [Isaak](../../strongs/g/g2464.md), I [eipon](../../strongs/g/g2036.md) for lest at any time I might [apothnēskō](../../strongs/g/g599.md) on account of her. 

<a name="genesis_26_10"></a>Genesis 26:10

[eipon](../../strongs/g/g2036.md) to him Abimelech, What is this you [poieō](../../strongs/g/g4160.md) to us? Is it a [mikron](../../strongs/g/g3397.md) went to [koimaō](../../strongs/g/g2837.md) if someone [genos](../../strongs/g/g1085.md) with your [gynē](../../strongs/g/g1135.md), and you [epagō](../../strongs/g/g1863.md) upon us in [agnoia](../../strongs/g/g52.md)? 

<a name="genesis_26_11"></a>Genesis 26:11

[syntassō](../../strongs/g/g4929.md) Abimelech to all his [laos](../../strongs/g/g2992.md), [legō](../../strongs/g/g3004.md), Any one [haptomai](../../strongs/g/g680.md) this [anthrōpos](../../strongs/g/g444.md) and his [gynē](../../strongs/g/g1135.md) unto [thanatos](../../strongs/g/g2288.md) [enochos](../../strongs/g/g1777.md) will be.

<a name="genesis_26_12"></a>Genesis 26:12

[speirō](../../strongs/g/g4687.md) [Isaak](../../strongs/g/g2464.md) in that [gē](../../strongs/g/g1093.md). And he [heuriskō](../../strongs/g/g2147.md) in that [eniautos](../../strongs/g/g1763.md) bearing a hundred fold (hekatosteuousan) [krithē](../../strongs/g/g2915.md), [eulogeō](../../strongs/g/g2127.md) and him the [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_26_13"></a>Genesis 26:13

And was [hypsoō](../../strongs/g/g5312.md) the [anthrōpos](../../strongs/g/g444.md), and [probainō](../../strongs/g/g4260.md) [megas](../../strongs/g/g3173.md) was, [megas](../../strongs/g/g3173.md) [ginomai](../../strongs/g/g1096.md) [sphodra](../../strongs/g/g4970.md). 

<a name="genesis_26_14"></a>Genesis 26:14

And [ginomai](../../strongs/g/g1096.md) to him [ktēnos](../../strongs/g/g2934.md) of [probaton](../../strongs/g/g4263.md), and [ktēnos](../../strongs/g/g2934.md) of [bous](../../strongs/g/g1016.md), and [geōrgion](../../strongs/g/g1091.md) [polys](../../strongs/g/g4183.md). [zēloō](../../strongs/g/g2206.md) him the Philistines (Phylistiim).

<a name="genesis_26_15"></a>Genesis 26:15

And all the [phrear](../../strongs/g/g5421.md) which [oryssō](../../strongs/g/g3736.md) the [pais](../../strongs/g/g3816.md) of his [patēr](../../strongs/g/g3962.md) obstructed (enephraxan) them in the [chronos](../../strongs/g/g5550.md) of his [patēr](../../strongs/g/g3962.md) the Philistines (Phylistiim), and [pimplēmi](../../strongs/g/g4130.md) them with [gē](../../strongs/g/g1093.md). 

<a name="genesis_26_16"></a>Genesis 26:16

[eipon](../../strongs/g/g2036.md) Abimelech to [Isaak](../../strongs/g/g2464.md), [aperchomai](../../strongs/g/g565.md) from us! for [dynatos](../../strongs/g/g1415.md) us you became [sphodra](../../strongs/g/g4970.md). 

<a name="genesis_26_17"></a>Genesis 26:17

And [Isaak](../../strongs/g/g2464.md) [aperchomai](../../strongs/g/g565.md) from there. And he [katalyō](../../strongs/g/g2647.md) in the [pharagx](../../strongs/g/g5327.md) of Gerar, and [katoikeō](../../strongs/g/g2730.md) there. 

<a name="genesis_26_18"></a>Genesis 26:18

And again [Isaak](../../strongs/g/g2464.md) [oryssō](../../strongs/g/g3736.md) the [phrear](../../strongs/g/g5421.md) of [hydōr](../../strongs/g/g5204.md) which [oryssō](../../strongs/g/g3736.md) the [pais](../../strongs/g/g3816.md) of [Abraam](../../strongs/g/g11.md) his [patēr](../../strongs/g/g3962.md); and obstructed (enephraxan) them the Philistines, after the [apothnēskō](../../strongs/g/g599.md) of [Abraam](../../strongs/g/g11.md) his [patēr](../../strongs/g/g3962.md). And he [eponomazō](../../strongs/g/g2028.md) them with [onoma](../../strongs/g/g3686.md) according to the [onoma](../../strongs/g/g3686.md) which [onomazō](../../strongs/g/g3687.md) his [patēr](../../strongs/g/g3962.md). 

<a name="genesis_26_19"></a>Genesis 26:19

And [oryssō](../../strongs/g/g3736.md) the [pais](../../strongs/g/g3816.md) of [Isaak](../../strongs/g/g2464.md) in the [pharagx](../../strongs/g/g5327.md) of Gerar, and [heuriskō](../../strongs/g/g2147.md) there a [phrear](../../strongs/g/g5421.md) [hydōr](../../strongs/g/g5204.md) [zaō](../../strongs/g/g2198.md). 

<a name="genesis_26_20"></a>Genesis 26:20

And did [machomai](../../strongs/g/g3164.md) the [poimēn](../../strongs/g/g4166.md) of Gerar with the [poimēn](../../strongs/g/g4166.md) of [Isaak](../../strongs/g/g2464.md), [phasko](../../strongs/g/g5335.md) theirs to be the [hydōr](../../strongs/g/g5204.md). And they [kaleō](../../strongs/g/g2564.md) the [onoma](../../strongs/g/g3686.md) of the [phrear](../../strongs/g/g5421.md), [adikia](../../strongs/g/g93.md), for they [adikeō](../../strongs/g/g91.md) him. 

<a name="genesis_26_21"></a>Genesis 26:21

And [apairō](../../strongs/g/g522.md) [Isaak](../../strongs/g/g2464.md) from there he [oryssō](../../strongs/g/g3736.md) [phrear](../../strongs/g/g5421.md) another. And they [krinō](../../strongs/g/g2919.md) also on account of that well . And he [eponomazō](../../strongs/g/g2028.md) the [onoma](../../strongs/g/g3686.md) of it, Hatred (echthria).

<a name="genesis_26_22"></a>Genesis 26:22

"And [apairō](../../strongs/g/g522.md) from there, he [oryssō](../../strongs/g/g3736.md) [well [phrear](../../strongs/g/g5421.md) another] . And they did not do [machomai](../../strongs/g/g3164.md) over it. And he [eponomazō](../../strongs/g/g2028.md) the [onoma](../../strongs/g/g3686.md) of it, Expanse (eurychōria), [legō](../../strongs/g/g3004.md), Because now the [kyrios](../../strongs/g/g2962.md) [platynō](../../strongs/g/g4115.md) us, and caused us to [auxanō](../../strongs/g/g837.md) upon the [gē](../../strongs/g/g1093.md). "

<a name="genesis_26_23"></a>Genesis 26:23

And he [anabainō](../../strongs/g/g305.md) from there towards the [phrear](../../strongs/g/g5421.md) of the [horkos](../../strongs/g/g3727.md). 

<a name="genesis_26_24"></a>Genesis 26:24

And [horaō](../../strongs/g/g3708.md) to him the [kyrios](../../strongs/g/g2962.md) in that [nyx](../../strongs/g/g3571.md), and [eipon](../../strongs/g/g2036.md), I am the [theos](../../strongs/g/g2316.md) of [Abraam](../../strongs/g/g11.md) your [patēr](../../strongs/g/g3962.md). Do not [phobeō](../../strongs/g/g5399.md)! with you for I am. And I will [eulogeō](../../strongs/g/g2127.md) you, and I will [plēthynō](../../strongs/g/g4129.md) your [sperma](../../strongs/g/g4690.md) because of [Abraam](../../strongs/g/g11.md) your [patēr](../../strongs/g/g3962.md). 

<a name="genesis_26_25"></a>Genesis 26:25

And he [oikodomeō](../../strongs/g/g3618.md) there an [thysiastērion](../../strongs/g/g2379.md), and [epikaleō](../../strongs/g/g1941.md) the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md), and he [pēgnymi](../../strongs/g/g4078.md) there his [skēnē](../../strongs/g/g4633.md). [oryssō](../../strongs/g/g3736.md) there the [pais](../../strongs/g/g3816.md) of [Isaak](../../strongs/g/g2464.md) a [phrear](../../strongs/g/g5421.md) in the [pharagx](../../strongs/g/g5327.md) Gerar.

<a name="genesis_26_26"></a>Genesis 26:26

And Abimelech [poreuō](../../strongs/g/g4198.md) to him from Gerar, and Ahuzzath his groomsman (nymphagōgos), and Phichol the commander-in-chief (archistratēgos) of his [dynamis](../../strongs/g/g1411.md).

<a name="genesis_26_27"></a>Genesis 26:27

And [eipon](../../strongs/g/g2036.md) to them [Isaak](../../strongs/g/g2464.md), Why did you [erchomai](../../strongs/g/g2064.md) to me; for you [miseō](../../strongs/g/g3404.md) me, and you [apostellō](../../strongs/g/g649.md) me from you? 

<a name="genesis_26_28"></a>Genesis 26:28

And they [eipon](../../strongs/g/g2036.md), [eidō](../../strongs/g/g1492.md), we have [horaō](../../strongs/g/g3708.md) that the [kyrios](../../strongs/g/g2962.md) was with you. And we [eipon](../../strongs/g/g2036.md), Let there become an [ara](../../strongs/g/g685.md) between us and between you! And we will [diatithēmi](../../strongs/g/g1303.md) with you a [diathēkē](../../strongs/g/g1242.md). 

<a name="genesis_26_29"></a>Genesis 26:29

You shall not [poieō](../../strongs/g/g4160.md) with us [kakos](../../strongs/g/g2556.md), in so far as did not [bdelyssō](../../strongs/g/g948.md) you we, and which [tropos](../../strongs/g/g5158.md) we [chraomai](../../strongs/g/g5530.md) you [kalōs](../../strongs/g/g2573.md), and we [exapostellō](../../strongs/g/g1821.md) you out in [eirēnē](../../strongs/g/g1515.md). And now, you are being [eulogētos](../../strongs/g/g2128.md) by the [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_26_30"></a>Genesis 26:30

And he [poieō](../../strongs/g/g4160.md) a [dochē](../../strongs/g/g1403.md) for them. And they [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md). 

<a name="genesis_26_31"></a>Genesis 26:31

And [anistēmi](../../strongs/g/g450.md) in the [prōï](../../strongs/g/g4404.md), [omnyō](../../strongs/g/g3660.md) [anthrōpos](../../strongs/g/g444.md) to the [plēsion](../../strongs/g/g4139.md). And sent (exapesteilen) them out [Isaak](../../strongs/g/g2464.md), and they moved away (apōchonto) from him with [sōtēria](../../strongs/g/g4991.md). 

<a name="genesis_26_32"></a>Genesis 26:32

And [ginomai](../../strongs/g/g1096.md) in that [hēmera](../../strongs/g/g2250.md), also [paraginomai](../../strongs/g/g3854.md) the [pais](../../strongs/g/g3816.md) of [Isaak](../../strongs/g/g2464.md) [apaggellō](../../strongs/g/g518.md) to him concerning the [phrear](../../strongs/g/g5421.md) which they [oryssō](../../strongs/g/g3736.md), and [eipon](../../strongs/g/g2036.md), We did not [heuriskō](../../strongs/g/g2147.md) [hydōr](../../strongs/g/g5204.md). 

<a name="genesis_26_33"></a>Genesis 26:33

And he [kaleō](../../strongs/g/g2564.md) it, [horkos](../../strongs/g/g3727.md). On account of this he [kaleō](../../strongs/g/g2564.md) the [onoma](../../strongs/g/g3686.md) of that [polis](../../strongs/g/g4172.md), [phrear](../../strongs/g/g5421.md) of [horkos](../../strongs/g/g3727.md), unto [sēmeron](../../strongs/g/g4594.md) [hēmera](../../strongs/g/g2250.md). 

<a name="genesis_26_34"></a>Genesis 26:34

was Esau [etos](../../strongs/g/g2094.md) [tessarakonta](../../strongs/g/g5062.md) and he [lambanō](../../strongs/g/g2983.md) a [gynē](../../strongs/g/g1135.md) Judith the [thygatēr](../../strongs/g/g2364.md) of Beeri the Hittite, and Bashemath [thygatēr](../../strongs/g/g2364.md) of Elon the Hittite.

<a name="genesis_26_35"></a>Genesis 26:35

And they were [erizō](../../strongs/g/g2051.md) with [Isaak](../../strongs/g/g2464.md) and [Rhebekka](../../strongs/g/g4479.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 25](genesis_lxx_25.md) - [Genesis 27](genesis_lxx_27.md)