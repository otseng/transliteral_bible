# [Genesis 48](https://www.blueletterbible.org/kjv/gen/48/1/s_48001)

<a name="genesis_48_1"></a>Genesis 48:1

And it came to pass ['aḥar](../../strongs/h/h310.md) these [dabar](../../strongs/h/h1697.md), that one ['āmar](../../strongs/h/h559.md) [Yôsēp̄](../../strongs/h/h3130.md), Behold, thy ['ab](../../strongs/h/h1.md) is [ḥālâ](../../strongs/h/h2470.md): and he [laqach](../../strongs/h/h3947.md) with him his two [ben](../../strongs/h/h1121.md), [Mᵊnaššê](../../strongs/h/h4519.md) and ['Ep̄rayim](../../strongs/h/h669.md).

<a name="genesis_48_2"></a>Genesis 48:2

And one [nāḡaḏ](../../strongs/h/h5046.md) [Ya'aqob](../../strongs/h/h3290.md), and ['āmar](../../strongs/h/h559.md), Behold, thy [ben](../../strongs/h/h1121.md) [Yôsēp̄](../../strongs/h/h3130.md) [bow'](../../strongs/h/h935.md) unto thee: and [Yisra'el](../../strongs/h/h3478.md) [ḥāzaq](../../strongs/h/h2388.md) himself, and [yashab](../../strongs/h/h3427.md) upon the [mittah](../../strongs/h/h4296.md).

<a name="genesis_48_3"></a>Genesis 48:3

And [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md) unto [Yôsēp̄](../../strongs/h/h3130.md), ['el](../../strongs/h/h410.md) [Šaday](../../strongs/h/h7706.md) [ra'ah](../../strongs/h/h7200.md) unto me at [Lûz](../../strongs/h/h3870.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), and [barak](../../strongs/h/h1288.md) me,

<a name="genesis_48_4"></a>Genesis 48:4

And ['āmar](../../strongs/h/h559.md) unto me, Behold, I will make thee [parah](../../strongs/h/h6509.md), and [rabah](../../strongs/h/h7235.md) thee, and I will [nathan](../../strongs/h/h5414.md) of thee a [qāhēl](../../strongs/h/h6951.md) of ['am](../../strongs/h/h5971.md); and will [nathan](../../strongs/h/h5414.md) this ['erets](../../strongs/h/h776.md) to thy [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) thee for an ['owlam](../../strongs/h/h5769.md) ['achuzzah](../../strongs/h/h272.md).

<a name="genesis_48_5"></a>Genesis 48:5

And now thy two [ben](../../strongs/h/h1121.md), ['Ep̄rayim](../../strongs/h/h669.md) and [Mᵊnaššê](../../strongs/h/h4519.md), which were [yalad](../../strongs/h/h3205.md) unto thee in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) before I [bow'](../../strongs/h/h935.md) unto thee into [Mitsrayim](../../strongs/h/h4714.md), are mine; as [Rᵊ'ûḇēn](../../strongs/h/h7205.md) and [Šimʿôn](../../strongs/h/h8095.md), they shall be mine.

<a name="genesis_48_6"></a>Genesis 48:6

And thy [môleḏeṯ](../../strongs/h/h4138.md), which thou [yalad](../../strongs/h/h3205.md) ['aḥar](../../strongs/h/h310.md) them, shall be thine, and shall be [qara'](../../strongs/h/h7121.md) [ʿal](../../strongs/h/h5921.md) the [shem](../../strongs/h/h8034.md) of their ['ach](../../strongs/h/h251.md) in their [nachalah](../../strongs/h/h5159.md).

<a name="genesis_48_7"></a>Genesis 48:7

And as for me, when I [bow'](../../strongs/h/h935.md) from [padān](../../strongs/h/h6307.md), [Rāḥēl](../../strongs/h/h7354.md) [muwth](../../strongs/h/h4191.md) by me in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md) in the [derek](../../strongs/h/h1870.md), when yet there was but a [kiḇrâ](../../strongs/h/h3530.md) ['erets](../../strongs/h/h776.md) to [bow'](../../strongs/h/h935.md) unto ['Ep̄rāṯ](../../strongs/h/h672.md): and I [qāḇar](../../strongs/h/h6912.md) her there in the [derek](../../strongs/h/h1870.md) of ['Ep̄rāṯ](../../strongs/h/h672.md); the same is [Bêṯ leḥem](../../strongs/h/h1035.md).

<a name="genesis_48_8"></a>Genesis 48:8

And [Yisra'el](../../strongs/h/h3478.md) [ra'ah](../../strongs/h/h7200.md) [Yôsēp̄](../../strongs/h/h3130.md) [ben](../../strongs/h/h1121.md), and ['āmar](../../strongs/h/h559.md), Who are these?

<a name="genesis_48_9"></a>Genesis 48:9

And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto his ['ab](../../strongs/h/h1.md), They are my [ben](../../strongs/h/h1121.md), whom ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) me in this place. And he ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) them, I pray thee, unto me, and I will [barak](../../strongs/h/h1288.md) them.

<a name="genesis_48_10"></a>Genesis 48:10

Now the ['ayin](../../strongs/h/h5869.md) of [Yisra'el](../../strongs/h/h3478.md) were [kabad](../../strongs/h/h3513.md) for [zōqen](../../strongs/h/h2207.md), so that he [yakol](../../strongs/h/h3201.md) not [ra'ah](../../strongs/h/h7200.md). And he [nāḡaš](../../strongs/h/h5066.md) them unto him; and he [nashaq](../../strongs/h/h5401.md) them, and [ḥāḇaq](../../strongs/h/h2263.md) them.

<a name="genesis_48_11"></a>Genesis 48:11

And [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto [Yôsēp̄](../../strongs/h/h3130.md), I had not [palal](../../strongs/h/h6419.md) to [ra'ah](../../strongs/h/h7200.md) thy [paniym](../../strongs/h/h6440.md): and, lo, ['Elohiym](../../strongs/h/h430.md) hath [ra'ah](../../strongs/h/h7200.md) me also thy [zera'](../../strongs/h/h2233.md).

<a name="genesis_48_12"></a>Genesis 48:12

And [Yôsēp̄](../../strongs/h/h3130.md) [yāṣā'](../../strongs/h/h3318.md) them out from between his [bereḵ](../../strongs/h/h1290.md), and he [shachah](../../strongs/h/h7812.md) himself with his ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md).

<a name="genesis_48_13"></a>Genesis 48:13

And [Yôsēp̄](../../strongs/h/h3130.md) [laqach](../../strongs/h/h3947.md) them both, ['Ep̄rayim](../../strongs/h/h669.md) in his [yamiyn](../../strongs/h/h3225.md) toward [Yisra'el](../../strongs/h/h3478.md) [śᵊmō'l](../../strongs/h/h8040.md), and [Mᵊnaššê](../../strongs/h/h4519.md) in his [śᵊmō'l](../../strongs/h/h8040.md) toward [Yisra'el](../../strongs/h/h3478.md) [yamiyn](../../strongs/h/h3225.md), and [nāḡaš](../../strongs/h/h5066.md) them near unto him.

<a name="genesis_48_14"></a>Genesis 48:14

And [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md) his [yamiyn](../../strongs/h/h3225.md), and [shiyth](../../strongs/h/h7896.md) it upon ['Ep̄rayim](../../strongs/h/h669.md) [ro'sh](../../strongs/h/h7218.md), who was the [ṣāʿîr](../../strongs/h/h6810.md), and his [śᵊmō'l](../../strongs/h/h8040.md) upon [Mᵊnaššê](../../strongs/h/h4519.md) [ro'sh](../../strongs/h/h7218.md), [sakal](../../strongs/h/h7919.md) his [yad](../../strongs/h/h3027.md) ; for [Mᵊnaššê](../../strongs/h/h4519.md) was the [bᵊḵôr](../../strongs/h/h1060.md).

<a name="genesis_48_15"></a>Genesis 48:15

And he [barak](../../strongs/h/h1288.md) [Yôsēp̄](../../strongs/h/h3130.md), and ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md), [paniym](../../strongs/h/h6440.md) whom my ['ab](../../strongs/h/h1.md) ['Abraham](../../strongs/h/h85.md) and [Yiṣḥāq](../../strongs/h/h3327.md) did [halak](../../strongs/h/h1980.md), the ['Elohiym](../../strongs/h/h430.md) which [ra'ah](../../strongs/h/h7462.md) me [ʿôḏ](../../strongs/h/h5750.md) unto this [yowm](../../strongs/h/h3117.md),

<a name="genesis_48_16"></a>Genesis 48:16

The [mal'ak](../../strongs/h/h4397.md) which [gā'al](../../strongs/h/h1350.md) me from all [ra'](../../strongs/h/h7451.md), [barak](../../strongs/h/h1288.md) the [naʿar](../../strongs/h/h5288.md); and let my [shem](../../strongs/h/h8034.md) be [qara'](../../strongs/h/h7121.md) on them, and the [shem](../../strongs/h/h8034.md) of my ['ab](../../strongs/h/h1.md) ['Abraham](../../strongs/h/h85.md) and [Yiṣḥāq](../../strongs/h/h3327.md); and let them [dāḡâ](../../strongs/h/h1711.md) into a [rōḇ](../../strongs/h/h7230.md) in the [qereḇ](../../strongs/h/h7130.md) of the ['erets](../../strongs/h/h776.md).

<a name="genesis_48_17"></a>Genesis 48:17

And when [Yôsēp̄](../../strongs/h/h3130.md) [ra'ah](../../strongs/h/h7200.md) that his ['ab](../../strongs/h/h1.md) [shiyth](../../strongs/h/h7896.md) his [yamiyn](../../strongs/h/h3225.md) [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of ['Ep̄rayim](../../strongs/h/h669.md), it [yāraʿ](../../strongs/h/h3415.md) ['ayin](../../strongs/h/h5869.md) him: and he [tamak](../../strongs/h/h8551.md) his ['ab](../../strongs/h/h1.md) [yad](../../strongs/h/h3027.md), to [cuwr](../../strongs/h/h5493.md) it from ['Ep̄rayim](../../strongs/h/h669.md) [ro'sh](../../strongs/h/h7218.md) unto [Mᵊnaššê](../../strongs/h/h4519.md) [ro'sh](../../strongs/h/h7218.md).

<a name="genesis_48_18"></a>Genesis 48:18

And [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md) unto his ['ab](../../strongs/h/h1.md), Not so, my ['ab](../../strongs/h/h1.md): for this is the [bᵊḵôr](../../strongs/h/h1060.md); [śûm](../../strongs/h/h7760.md) thy [yamiyn](../../strongs/h/h3225.md) upon his [ro'sh](../../strongs/h/h7218.md).

<a name="genesis_48_19"></a>Genesis 48:19

And his ['ab](../../strongs/h/h1.md) [mā'ēn](../../strongs/h/h3985.md), and ['āmar](../../strongs/h/h559.md), I [yada'](../../strongs/h/h3045.md) it, my [ben](../../strongs/h/h1121.md), I [yada'](../../strongs/h/h3045.md) it: he also shall become an ['am](../../strongs/h/h5971.md), and he also shall be [gāḏal](../../strongs/h/h1431.md): ['ûlām](../../strongs/h/h199.md) his [qāṭān](../../strongs/h/h6996.md) ['ach](../../strongs/h/h251.md) shall be [gāḏal](../../strongs/h/h1431.md) than he, and his [zera'](../../strongs/h/h2233.md) shall become a [mᵊlō'](../../strongs/h/h4393.md) of [gowy](../../strongs/h/h1471.md).

<a name="genesis_48_20"></a>Genesis 48:20

And he [barak](../../strongs/h/h1288.md) them that [yowm](../../strongs/h/h3117.md), ['āmar](../../strongs/h/h559.md), In thee shall [Yisra'el](../../strongs/h/h3478.md) [barak](../../strongs/h/h1288.md), ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) [śûm](../../strongs/h/h7760.md) thee as ['Ep̄rayim](../../strongs/h/h669.md) and as [Mᵊnaššê](../../strongs/h/h4519.md): and he [śûm](../../strongs/h/h7760.md) ['Ep̄rayim](../../strongs/h/h669.md) [paniym](../../strongs/h/h6440.md) [Mᵊnaššê](../../strongs/h/h4519.md).

<a name="genesis_48_21"></a>Genesis 48:21

And [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto [Yôsēp̄](../../strongs/h/h3130.md), Behold, I [muwth](../../strongs/h/h4191.md): but ['Elohiym](../../strongs/h/h430.md) shall be with you, and [shuwb](../../strongs/h/h7725.md)you unto the ['erets](../../strongs/h/h776.md) of your ['ab](../../strongs/h/h1.md).

<a name="genesis_48_22"></a>Genesis 48:22

Moreover I have [nathan](../../strongs/h/h5414.md) to thee one [šᵊḵem](../../strongs/h/h7926.md) above thy ['ach](../../strongs/h/h251.md), which I [laqach](../../strongs/h/h3947.md) out of the [yad](../../strongs/h/h3027.md) of the ['Ĕmōrî](../../strongs/h/h567.md) with my [chereb](../../strongs/h/h2719.md) and with my [qesheth](../../strongs/h/h7198.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 47](genesis_47.md) - [Genesis 49](genesis_49.md)