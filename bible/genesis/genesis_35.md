# [Genesis 35](https://www.blueletterbible.org/kjv/gen/35/1/s_35001)

<a name="genesis_35_1"></a>Genesis 35:1

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto [Ya'aqob](../../strongs/h/h3290.md), [quwm](../../strongs/h/h6965.md), [ʿālâ](../../strongs/h/h5927.md) to [Bêṯ-'ēl](../../strongs/h/h1008.md), and [yashab](../../strongs/h/h3427.md) there: and ['asah](../../strongs/h/h6213.md) there a [mizbeach](../../strongs/h/h4196.md) unt['el](../../strongs/h/h410.md), that [ra'ah](../../strongs/h/h7200.md) unto thee when thou [bāraḥ](../../strongs/h/h1272.md) from the [paniym](../../strongs/h/h6440.md) of [ʿĒśāv](../../strongs/h/h6215.md) thy ['ach](../../strongs/h/h251.md).

<a name="genesis_35_2"></a>Genesis 35:2

Then [Ya'aqob](../../strongs/h/h3290.md) ['āmar](../../strongs/h/h559.md) unto his [bayith](../../strongs/h/h1004.md), and to all that were with him, [cuwr](../../strongs/h/h5493.md) the [nēḵār](../../strongs/h/h5236.md) ['Elohiym](../../strongs/h/h430.md) that are [tavek](../../strongs/h/h8432.md) you, and be [ṭāhēr](../../strongs/h/h2891.md), and [ḥālap̄](../../strongs/h/h2498.md) your [śimlâ](../../strongs/h/h8071.md):

<a name="genesis_35_3"></a>Genesis 35:3

And let us [quwm](../../strongs/h/h6965.md), and [ʿālâ](../../strongs/h/h5927.md) to [Bêṯ-'ēl](../../strongs/h/h1008.md); and I will ['asah](../../strongs/h/h6213.md) there a [mizbeach](../../strongs/h/h4196.md) unto ['el](../../strongs/h/h410.md), who ['anah](../../strongs/h/h6030.md) me in the [yowm](../../strongs/h/h3117.md) of my [tsarah](../../strongs/h/h6869.md), and was with me in the [derek](../../strongs/h/h1870.md) which I [halak](../../strongs/h/h1980.md).

<a name="genesis_35_4"></a>Genesis 35:4

And they [nathan](../../strongs/h/h5414.md) unto [Ya'aqob](../../strongs/h/h3290.md) all the [nēḵār](../../strongs/h/h5236.md) ['Elohiym](../../strongs/h/h430.md) which were in their [yad](../../strongs/h/h3027.md), and all their [nezem](../../strongs/h/h5141.md) which were in their ['ozen](../../strongs/h/h241.md); and [Ya'aqob](../../strongs/h/h3290.md) [taman](../../strongs/h/h2934.md) them under the ['ēlâ](../../strongs/h/h424.md) which was by [Šᵊḵem](../../strongs/h/h7927.md).

<a name="genesis_35_5"></a>Genesis 35:5

And they [nāsaʿ](../../strongs/h/h5265.md): and the [ḥitâ](../../strongs/h/h2847.md) of ['Elohiym](../../strongs/h/h430.md) was upon the [ʿîr](../../strongs/h/h5892.md) that were [cabiyb](../../strongs/h/h5439.md) them, and they did not [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) the [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md).

<a name="genesis_35_6"></a>Genesis 35:6

So [Ya'aqob](../../strongs/h/h3290.md) [bow'](../../strongs/h/h935.md) to [Lûz](../../strongs/h/h3870.md), which is in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), that is, [Bêṯ-'ēl](../../strongs/h/h1008.md), he and all the ['am](../../strongs/h/h5971.md) that were with him.

<a name="genesis_35_7"></a>Genesis 35:7

And he [bānâ](../../strongs/h/h1129.md) there a [mizbeach](../../strongs/h/h4196.md), and [qara'](../../strongs/h/h7121.md) the [maqowm](../../strongs/h/h4725.md) ['l ḇyṯ-'l](../../strongs/h/h416.md): because there ['Elohiym](../../strongs/h/h430.md) [gālâ](../../strongs/h/h1540.md) unto him, when he [bāraḥ](../../strongs/h/h1272.md) from the [paniym](../../strongs/h/h6440.md) of his ['ach](../../strongs/h/h251.md).

<a name="genesis_35_8"></a>Genesis 35:8

But [Dᵊḇôrâ](../../strongs/h/h1683.md) [riḇqâ](../../strongs/h/h7259.md) [yānaq](../../strongs/h/h3243.md) [muwth](../../strongs/h/h4191.md), and she was [qāḇar](../../strongs/h/h6912.md) beneath [Bêṯ-'ēl](../../strongs/h/h1008.md) under an ['allôn](../../strongs/h/h437.md): and the [shem](../../strongs/h/h8034.md) of it was [qara'](../../strongs/h/h7121.md) ['allôn bāḵûṯ](../../strongs/h/h439.md).

<a name="genesis_35_9"></a>Genesis 35:9

And ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) unto [Ya'aqob](../../strongs/h/h3290.md) again, when he [bow'](../../strongs/h/h935.md) out of [padān](../../strongs/h/h6307.md), and [barak](../../strongs/h/h1288.md) him.

<a name="genesis_35_10"></a>Genesis 35:10

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto him, Thy [shem](../../strongs/h/h8034.md) is [Ya'aqob](../../strongs/h/h3290.md): thy [shem](../../strongs/h/h8034.md) shall not be [qara'](../../strongs/h/h7121.md) any more [Ya'aqob](../../strongs/h/h3290.md), but [Yisra'el](../../strongs/h/h3478.md) shall be thy [shem](../../strongs/h/h8034.md): and he [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="genesis_35_11"></a>Genesis 35:11

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto him, I am ['el](../../strongs/h/h410.md) [Šaday](../../strongs/h/h7706.md): be [parah](../../strongs/h/h6509.md) and [rabah](../../strongs/h/h7235.md); a [gowy](../../strongs/h/h1471.md) and a [qāhēl](../../strongs/h/h6951.md) of [gowy](../../strongs/h/h1471.md) shall be of thee, and [melek](../../strongs/h/h4428.md) shall [yāṣā'](../../strongs/h/h3318.md) of thy [ḥālāṣ](../../strongs/h/h2504.md);

<a name="genesis_35_12"></a>Genesis 35:12

And the ['erets](../../strongs/h/h776.md) which I [nathan](../../strongs/h/h5414.md) ['Abraham](../../strongs/h/h85.md) and [Yiṣḥāq](../../strongs/h/h3327.md), to thee I will [nathan](../../strongs/h/h5414.md) it, and to thy [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) thee will I [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md).

<a name="genesis_35_13"></a>Genesis 35:13

And ['Elohiym](../../strongs/h/h430.md) [ʿālâ](../../strongs/h/h5927.md) from him in the [maqowm](../../strongs/h/h4725.md) where he [dabar](../../strongs/h/h1696.md) with him.

<a name="genesis_35_14"></a>Genesis 35:14

And [Ya'aqob](../../strongs/h/h3290.md) [nāṣaḇ](../../strongs/h/h5324.md) a [maṣṣēḇâ](../../strongs/h/h4676.md) in the [maqowm](../../strongs/h/h4725.md) where he [dabar](../../strongs/h/h1696.md) with him, even a [maṣṣeḇeṯ](../../strongs/h/h4678.md) of ['eben](../../strongs/h/h68.md): and he [nacak](../../strongs/h/h5258.md) a [necek](../../strongs/h/h5262.md) thereon, and he [yāṣaq](../../strongs/h/h3332.md) [šemen](../../strongs/h/h8081.md) thereon.

<a name="genesis_35_15"></a>Genesis 35:15

And [Ya'aqob](../../strongs/h/h3290.md) [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of the [maqowm](../../strongs/h/h4725.md) where ['Elohiym](../../strongs/h/h430.md) [dabar](../../strongs/h/h1696.md) with him, [Bêṯ-'ēl](../../strongs/h/h1008.md).

<a name="genesis_35_16"></a>Genesis 35:16

And they [nāsaʿ](../../strongs/h/h5265.md) from [Bêṯ-'ēl](../../strongs/h/h1008.md); and there was but a [kiḇrâ](../../strongs/h/h3530.md) ['erets](../../strongs/h/h776.md) to [bow'](../../strongs/h/h935.md) to ['Ep̄rāṯ](../../strongs/h/h672.md): and [Rāḥēl](../../strongs/h/h7354.md) [yalad](../../strongs/h/h3205.md), and she had [qāšâ](../../strongs/h/h7185.md) [yalad](../../strongs/h/h3205.md).

<a name="genesis_35_17"></a>Genesis 35:17

And it came to pass, when she was in [qāšâ](../../strongs/h/h7185.md) [yalad](../../strongs/h/h3205.md), that the [yalad](../../strongs/h/h3205.md) ['āmar](../../strongs/h/h559.md) unto her, [yare'](../../strongs/h/h3372.md) not; thou shalt have this [ben](../../strongs/h/h1121.md) also.

<a name="genesis_35_18"></a>Genesis 35:18

And it came to pass, as her [nephesh](../../strongs/h/h5315.md) was in [yāṣā'](../../strongs/h/h3318.md), (for she [muwth](../../strongs/h/h4191.md)) that she [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [ben-'ônî](../../strongs/h/h1126.md): but his ['ab](../../strongs/h/h1.md) [qara'](../../strongs/h/h7121.md) him [Binyāmîn](../../strongs/h/h1144.md).

<a name="genesis_35_19"></a>Genesis 35:19

And [Rāḥēl](../../strongs/h/h7354.md) [muwth](../../strongs/h/h4191.md), and was [qāḇar](../../strongs/h/h6912.md) in the [derek](../../strongs/h/h1870.md) to ['Ep̄rāṯ](../../strongs/h/h672.md), which is [Bêṯ leḥem](../../strongs/h/h1035.md).

<a name="genesis_35_20"></a>Genesis 35:20

And [Ya'aqob](../../strongs/h/h3290.md) [nāṣaḇ](../../strongs/h/h5324.md) a [maṣṣēḇâ](../../strongs/h/h4676.md) upon her [qᵊḇûrâ](../../strongs/h/h6900.md): that is the [maṣṣeḇeṯ](../../strongs/h/h4678.md) of [Rāḥēl](../../strongs/h/h7354.md) [qᵊḇûrâ](../../strongs/h/h6900.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="genesis_35_21"></a>Genesis 35:21

And [Yisra'el](../../strongs/h/h3478.md) [nāsaʿ](../../strongs/h/h5265.md), and [natah](../../strongs/h/h5186.md) his ['ohel](../../strongs/h/h168.md) [hāl'â](../../strongs/h/h1973.md) the [miḡdāl](../../strongs/h/h4026.md) of [miḡdal-ʿēḏer](../../strongs/h/h4029.md).

<a name="genesis_35_22"></a>Genesis 35:22

And it came to pass, when [Yisra'el](../../strongs/h/h3478.md) [shakan](../../strongs/h/h7931.md) in that ['erets](../../strongs/h/h776.md), that [Rᵊ'ûḇēn](../../strongs/h/h7205.md) [yālaḵ](../../strongs/h/h3212.md) and [shakab](../../strongs/h/h7901.md) with [Bilhâ](../../strongs/h/h1090.md) his ['ab](../../strongs/h/h1.md) [pîleḡeš](../../strongs/h/h6370.md): and [Yisra'el](../../strongs/h/h3478.md) [shama'](../../strongs/h/h8085.md) it. Now the [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md) were twelve:

<a name="genesis_35_23"></a>Genesis 35:23

The [ben](../../strongs/h/h1121.md) of [Lē'â](../../strongs/h/h3812.md); [Rᵊ'ûḇēn](../../strongs/h/h7205.md), [Ya'aqob](../../strongs/h/h3290.md) [bᵊḵôr](../../strongs/h/h1060.md), and [Šimʿôn](../../strongs/h/h8095.md), and [Lēvî](../../strongs/h/h3878.md), and [Yehuwdah](../../strongs/h/h3063.md), and [Yiśśāśḵār](../../strongs/h/h3485.md), and [Zᵊḇûlûn](../../strongs/h/h2074.md):

<a name="genesis_35_24"></a>Genesis 35:24

The [ben](../../strongs/h/h1121.md) of [Rāḥēl](../../strongs/h/h7354.md); [Yôsēp̄](../../strongs/h/h3130.md), and [Binyāmîn](../../strongs/h/h1144.md):

<a name="genesis_35_25"></a>Genesis 35:25

And the [ben](../../strongs/h/h1121.md) of [Bilhâ](../../strongs/h/h1090.md), [Rāḥēl](../../strongs/h/h7354.md) [šip̄ḥâ](../../strongs/h/h8198.md); [Dān](../../strongs/h/h1835.md), and [Nap̄tālî](../../strongs/h/h5321.md):

<a name="genesis_35_26"></a>Genesis 35:26

And the [ben](../../strongs/h/h1121.md) of [zilpâ](../../strongs/h/h2153.md), [Lē'â](../../strongs/h/h3812.md) [šip̄ḥâ](../../strongs/h/h8198.md): [Gāḏ](../../strongs/h/h1410.md), and ['Āšēr](../../strongs/h/h836.md): these are the [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md), which were [yalad](../../strongs/h/h3205.md) to him in [padān](../../strongs/h/h6307.md).

<a name="genesis_35_27"></a>Genesis 35:27

And [Ya'aqob](../../strongs/h/h3290.md) [bow'](../../strongs/h/h935.md) unto [Yiṣḥāq](../../strongs/h/h3327.md) his ['ab](../../strongs/h/h1.md) unto [mamrē'](../../strongs/h/h4471.md), unto [qiryaṯ 'arbaʿ](../../strongs/h/h7153.md), which is [Ḥeḇrôn](../../strongs/h/h2275.md), where ['Abraham](../../strongs/h/h85.md) and [Yiṣḥāq](../../strongs/h/h3327.md) [guwr](../../strongs/h/h1481.md).

<a name="genesis_35_28"></a>Genesis 35:28

And the [yowm](../../strongs/h/h3117.md) of [Yiṣḥāq](../../strongs/h/h3327.md) were an hundred and fourscore [šānâ](../../strongs/h/h8141.md).

<a name="genesis_35_29"></a>Genesis 35:29

And [Yiṣḥāq](../../strongs/h/h3327.md) [gāvaʿ](../../strongs/h/h1478.md), and [muwth](../../strongs/h/h4191.md), and was ['āsap̄](../../strongs/h/h622.md) unto his ['am](../../strongs/h/h5971.md), being [zāqēn](../../strongs/h/h2205.md) and [śāḇēaʿ](../../strongs/h/h7649.md) of [yowm](../../strongs/h/h3117.md): and his [ben](../../strongs/h/h1121.md) [ʿĒśāv](../../strongs/h/h6215.md) and [Ya'aqob](../../strongs/h/h3290.md) [qāḇar](../../strongs/h/h6912.md) him.

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 34](genesis_34.md) - [Genesis 36](genesis_36.md)