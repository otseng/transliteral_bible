# [Genesis 21](https://www.blueletterbible.org/lxx/genesis/21)

<a name="genesis_21_1"></a>Genesis 21:1

And the [kyrios](../../strongs/g/g2962.md) [episkeptomai](../../strongs/g/g1980.md) [Sarra](../../strongs/g/g4564.md) as he [eipon](../../strongs/g/g2036.md), and the [kyrios](../../strongs/g/g2962.md) did to [Sarra](../../strongs/g/g4564.md) as he [laleō](../../strongs/g/g2980.md). 

<a name="genesis_21_2"></a>Genesis 21:2

And [syllambanō](../../strongs/g/g4815.md), she [tiktō](../../strongs/g/g5088.md) to [Abraam](../../strongs/g/g11.md) a [huios](../../strongs/g/g5207.md) in his [gēras](../../strongs/g/g1094.md), in the [kairos](../../strongs/g/g2540.md) as [laleō](../../strongs/g/g2980.md) to him the [kyrios](../../strongs/g/g2962.md). 

<a name="genesis_21_3"></a>Genesis 21:3

And [Abraam](../../strongs/g/g11.md) [kaleō](../../strongs/g/g2564.md) the [onoma](../../strongs/g/g3686.md) of his [huios](../../strongs/g/g5207.md), the one [ginomai](../../strongs/g/g1096.md) to him whom [tiktō](../../strongs/g/g5088.md) to him [Sarra](../../strongs/g/g4564.md) -- [Isaak](../../strongs/g/g2464.md).

<a name="genesis_21_4"></a>Genesis 21:4

[peritemnō](../../strongs/g/g4059.md) [Abraam](../../strongs/g/g11.md) [Isaak](../../strongs/g/g2464.md) on the [ogdoos](../../strongs/g/g3590.md) [hēmera](../../strongs/g/g2250.md) as [entellō](../../strongs/g/g1781.md) to him [theos](../../strongs/g/g2316.md). 

<a name="genesis_21_5"></a>Genesis 21:5

And [Abraam](../../strongs/g/g11.md) was a [hekaton](../../strongs/g/g1540.md) [etos](../../strongs/g/g2094.md) when was [ginomai](../../strongs/g/g1096.md) to him [Isaak](../../strongs/g/g2464.md) his [huios](../../strongs/g/g5207.md). 

<a name="genesis_21_6"></a>Genesis 21:6

[eipon](../../strongs/g/g2036.md) [Sarra](../../strongs/g/g4564.md), [gelōs](../../strongs/g/g1071.md) to me [poieō](../../strongs/g/g4160.md) the [kyrios](../../strongs/g/g2962.md), for whoever may [akouō](../../strongs/g/g191.md) will [sygchairō](../../strongs/g/g4796.md) me. 

<a name="genesis_21_7"></a>Genesis 21:7

And she [eipon](../../strongs/g/g2036.md), Who will [anaggellō](../../strongs/g/g312.md) to [Abraam](../../strongs/g/g11.md) that [thēlazō](../../strongs/g/g2337.md) a [paidion](../../strongs/g/g3813.md) [Sarra](../../strongs/g/g4564.md), for I [tiktō](../../strongs/g/g5088.md) a [huios](../../strongs/g/g5207.md) in my [gēras](../../strongs/g/g1094.md).

<a name="genesis_21_8"></a>Genesis 21:8

And [auxanō](../../strongs/g/g837.md) the [paidion](../../strongs/g/g3813.md) and was weaned (apegalaktisthē). And [Abraam](../../strongs/g/g11.md) [poieō](../../strongs/g/g4160.md) [dochē](../../strongs/g/g1403.md) a [megas](../../strongs/g/g3173.md) the [hēmera](../../strongs/g/g2250.md) was weaned (apegalaktisthē) [Isaak](../../strongs/g/g2464.md) his [huios](../../strongs/g/g5207.md). 

<a name="genesis_21_9"></a>Genesis 21:9

[eidō](../../strongs/g/g1492.md) [Sarra](../../strongs/g/g4564.md) the [huios](../../strongs/g/g5207.md) of [Agar](../../strongs/g/g28.md) the Egyptian (Eigyptias), who was [ginomai](../../strongs/g/g1096.md) to [Abraam](../../strongs/g/g11.md), [paizō](../../strongs/g/g3815.md) with [Isaak](../../strongs/g/g2464.md) her [huios](../../strongs/g/g5207.md), 

<a name="genesis_21_10"></a>Genesis 21:10

that she [eipon](../../strongs/g/g2036.md) to [Abraam](../../strongs/g/g11.md), You [ekballō](../../strongs/g/g1544.md) this [paidiskē](../../strongs/g/g3814.md) and her [huios](../../strongs/g/g5207.md), for shall not be [klēronomeō](../../strongs/g/g2816.md) the [huios](../../strongs/g/g5207.md) of the [paidiskē](../../strongs/g/g3814.md) with my [huios](../../strongs/g/g5207.md) [Isaak](../../strongs/g/g2464.md).

<a name="genesis_21_11"></a>Genesis 21:11

[sklēros](../../strongs/g/g4642.md) but [phainō](../../strongs/g/g5316.md) the [rhēma](../../strongs/g/g4487.md) [sphodra](../../strongs/g/g4970.md) [enantion](../../strongs/g/g1726.md) [Abraam](../../strongs/g/g11.md) concerning his [huios](../../strongs/g/g5207.md) Ishmael.

<a name="genesis_21_12"></a>Genesis 21:12

[eipon](../../strongs/g/g2036.md) [theos](../../strongs/g/g2316.md) to [Abraam](../../strongs/g/g11.md), not [sklēros](../../strongs/g/g4642.md) let it be [rhēma](../../strongs/g/g4487.md) [enantion](../../strongs/g/g1726.md) you on account of the [paidion](../../strongs/g/g3813.md), and on account of the [paidiskē](../../strongs/g/g3814.md)! All as much as should have [eipon](../../strongs/g/g2036.md) to you [Sarra](../../strongs/g/g4564.md), you [akouō](../../strongs/g/g191.md) her [phōnē](../../strongs/g/g5456.md)! for by [Isaak](../../strongs/g/g2464.md) shall be [kaleō](../../strongs/g/g2564.md) to you a [sperma](../../strongs/g/g4690.md). 

<a name="genesis_21_13"></a>Genesis 21:13

also the [huios](../../strongs/g/g5207.md) of this [paidiskē](../../strongs/g/g3814.md) into [ethnos](../../strongs/g/g1484.md) a [megas](../../strongs/g/g3173.md) I will [poieō](../../strongs/g/g4160.md), for he [sperma](../../strongs/g/g4690.md) your is.

<a name="genesis_21_14"></a>Genesis 21:14

[anistēmi](../../strongs/g/g450.md) [Abraam](../../strongs/g/g11.md) in the [prōï](../../strongs/g/g4404.md), and [lambanō](../../strongs/g/g2983.md) [artos](../../strongs/g/g740.md) and a [askos](../../strongs/g/g779.md) of [hydōr](../../strongs/g/g5204.md); and he [didōmi](../../strongs/g/g1325.md) to [Agar](../../strongs/g/g28.md), and he [epitithēmi](../../strongs/g/g2007.md) the [paidion](../../strongs/g/g3813.md) upon her [ōmos](../../strongs/g/g5606.md), and he [apostellō](../../strongs/g/g649.md) her. And she [aperchomai](../../strongs/g/g565.md) [planaō](../../strongs/g/g4105.md) about the [erēmos](../../strongs/g/g2048.md) by the [phrear](../../strongs/g/g5421.md) of the [horkos](../../strongs/g/g3727.md). 

<a name="genesis_21_15"></a>Genesis 21:15

[ekleipō](../../strongs/g/g1587.md) the [hydōr](../../strongs/g/g5204.md) coming from out of the [askos](../../strongs/g/g779.md), and she [rhipteō](../../strongs/g/g4495.md) the [paidion](../../strongs/g/g3813.md) [hypokatō](../../strongs/g/g5270.md) one fir tree (elatēs).

<a name="genesis_21_16"></a>Genesis 21:16

And [aperchomai](../../strongs/g/g565.md) she [kathēmai](../../strongs/g/g2521.md) [apenanti](../../strongs/g/g561.md) him, [makrothen](../../strongs/g/g3113.md) as a [toxon](../../strongs/g/g5115.md) [bolē](../../strongs/g/g1000.md). For she [eipon](../../strongs/g/g2036.md), no way shall I [eidō](../../strongs/g/g1492.md) the [thanatos](../../strongs/g/g2288.md) of my [paidion](../../strongs/g/g3813.md). And she [kathizō](../../strongs/g/g2523.md) [apenanti](../../strongs/g/g561.md) him. And [anaboaō](../../strongs/g/g310.md) the [paidion](../../strongs/g/g3813.md) [klaiō](../../strongs/g/g2799.md). 

<a name="genesis_21_17"></a>Genesis 21:17

[eisakouō](../../strongs/g/g1522.md) [theos](../../strongs/g/g2316.md) the [phōnē](../../strongs/g/g5456.md) of the [paidion](../../strongs/g/g3813.md) from the [topos](../../strongs/g/g5117.md) where he was. And [kaleō](../../strongs/g/g2564.md) an [aggelos](../../strongs/g/g32.md) of [theos](../../strongs/g/g2316.md) [Agar](../../strongs/g/g28.md) from out of the [ouranos](../../strongs/g/g3772.md), and [eipon](../../strongs/g/g2036.md) to her, What is it, [Agar](../../strongs/g/g28.md)? [phobeō](../../strongs/g/g5399.md) not! has [epakouō](../../strongs/g/g1873.md) for [theos](../../strongs/g/g2316.md) the [phōnē](../../strongs/g/g5456.md) of your [paidion](../../strongs/g/g3813.md) from out of the [topos](../../strongs/g/g5117.md) where he is.

<a name="genesis_21_18"></a>Genesis 21:18

[anistēmi](../../strongs/g/g450.md) and [lambanō](../../strongs/g/g2983.md) the [paidion](../../strongs/g/g3813.md), and [krateō](../../strongs/g/g2902.md) in your [cheir](../../strongs/g/g5495.md) it! For [ethnos](../../strongs/g/g1484.md) a [megas](../../strongs/g/g3173.md) I will [poieō](../../strongs/g/g4160.md) it. 

<a name="genesis_21_19"></a>Genesis 21:19

And [theos](../../strongs/g/g2316.md) [anoigō](../../strongs/g/g455.md) her [ophthalmos](../../strongs/g/g3788.md). And she [eidō](../../strongs/g/g1492.md) a [phrear](../../strongs/g/g5421.md) [hydōr](../../strongs/g/g5204.md) of [zaō](../../strongs/g/g2198.md). And she [poreuō](../../strongs/g/g4198.md) and [pimplēmi](../../strongs/g/g4130.md) the [askos](../../strongs/g/g779.md) of [hydōr](../../strongs/g/g5204.md), and [potizō](../../strongs/g/g4222.md) to the [paidion](../../strongs/g/g3813.md). 

<a name="genesis_21_20"></a>Genesis 21:20

And [theos](../../strongs/g/g2316.md) was with the [paidion](../../strongs/g/g3813.md). And he [auxanō](../../strongs/g/g837.md), and he [katoikeō](../../strongs/g/g2730.md) in the [erēmos](../../strongs/g/g2048.md). And he became a bowman (toxotēs).

<a name="genesis_21_21"></a>Genesis 21:21

And he [katoikeō](../../strongs/g/g2730.md) in the [erēmos](../../strongs/g/g2048.md) in Parah. And [lambanō](../../strongs/g/g2983.md) to him his [mētēr](../../strongs/g/g3384.md) a [gynē](../../strongs/g/g1135.md) from the [gē](../../strongs/g/g1093.md) of [Aigyptos](../../strongs/g/g125.md).

<a name="genesis_21_22"></a>Genesis 21:22

And [ginomai](../../strongs/g/g1096.md) in that [kairos](../../strongs/g/g2540.md), that Abimelech [eipon](../../strongs/g/g2036.md), along with Ahuzzath his groomsman (nymphagōgos), and Phichol the commander-in-chief (archistratēgos) of his [dynamis](../../strongs/g/g1411.md), to [Abraam](../../strongs/g/g11.md), [legō](../../strongs/g/g3004.md), [theos](../../strongs/g/g2316.md) is with you in all things what ever you should [poieō](../../strongs/g/g4160.md). 

<a name="genesis_21_23"></a>Genesis 21:23

Now then [omnyō](../../strongs/g/g3660.md) by an [omnyō](../../strongs/g/g3660.md) to me by [theos](../../strongs/g/g2316.md) that you will not [adikeō](../../strongs/g/g91.md) me, nor my [sperma](../../strongs/g/g4690.md), nor my [onoma](../../strongs/g/g3686.md)! but according to the [dikaiosynē](../../strongs/g/g1343.md) which I [poieō](../../strongs/g/g4160.md) with you, you shall [poieō](../../strongs/g/g4160.md) with me, and in the [gē](../../strongs/g/g1093.md) where you [paroikeō](../../strongs/g/g3939.md) in it. 

<a name="genesis_21_24"></a>Genesis 21:24

And [Abraam](../../strongs/g/g11.md) [eipon](../../strongs/g/g2036.md), I [omnyō](../../strongs/g/g3660.md). 

<a name="genesis_21_25"></a>Genesis 21:25

And [Abraam](../../strongs/g/g11.md) [elegchō](../../strongs/g/g1651.md) Abimelech on account of the [phrear](../../strongs/g/g5421.md) of [hydōr](../../strongs/g/g5204.md), which [aphaireō](../../strongs/g/g851.md) the [pais](../../strongs/g/g3816.md) of Abimelech.

<a name="genesis_21_26"></a>Genesis 21:26

And [eipon](../../strongs/g/g2036.md) to him Abimelech, I do not [ginōskō](../../strongs/g/g1097.md) who [poieō](../../strongs/g/g4160.md) this [rhēma](../../strongs/g/g4487.md), nor you [apaggellō](../../strongs/g/g518.md) it to me, nor I [akouō](../../strongs/g/g191.md) but only [sēmeron](../../strongs/g/g4594.md). 

<a name="genesis_21_27"></a>Genesis 21:27

And [Abraam](../../strongs/g/g11.md) [lambanō](../../strongs/g/g2983.md) [probaton](../../strongs/g/g4263.md) and [moschos](../../strongs/g/g3448.md), and [didōmi](../../strongs/g/g1325.md) to Abimelech, and [diatithēmi](../../strongs/g/g1303.md) both a [diathēkē](../../strongs/g/g1242.md). 

<a name="genesis_21_28"></a>Genesis 21:28

And [Abraam](../../strongs/g/g11.md) [histēmi](../../strongs/g/g2476.md) [hepta](../../strongs/g/g2033.md) ewe-lambs (amnadas) of [probaton](../../strongs/g/g4263.md) alone. 

<a name="genesis_21_29"></a>Genesis 21:29

And Abimelech [eipon](../../strongs/g/g2036.md) to [Abraam](../../strongs/g/g11.md), What are the [hepta](../../strongs/g/g2033.md) ewe-lambs (amnadas) of [probaton](../../strongs/g/g4263.md) these, which you [histēmi](../../strongs/g/g2476.md) alone? 

<a name="genesis_21_30"></a>Genesis 21:30

And he [eipon](../../strongs/g/g2036.md), that, [hepta](../../strongs/g/g2033.md) ewe-lambs (amnadas) these you shall [lambanō](../../strongs/g/g2983.md) of of mine, that they should be to me for a [martyrion](../../strongs/g/g3142.md) that I [oryssō](../../strongs/g/g3736.md) this [phrear](../../strongs/g/g5421.md). 

<a name="genesis_21_31"></a>Genesis 21:31

Because of this was [eponomazō](../../strongs/g/g2028.md) the [onoma](../../strongs/g/g3686.md) of that [topos](../../strongs/g/g5117.md), [phrear](../../strongs/g/g5421.md) of the Oath (horkismou), for there [omnyō](../../strongs/g/g3660.md) both. 

<a name="genesis_21_32"></a>Genesis 21:32

And they [diatithēmi](../../strongs/g/g1303.md) a [diathēkē](../../strongs/g/g1242.md) at the [phrear](../../strongs/g/g5421.md) of the [horkos](../../strongs/g/g3727.md). [anistēmi](../../strongs/g/g450.md) and Abimelech, and Ahuzzath his groomsman (nymphagōgos), and Phichol the commander-in-chief (archistratēgos) of his [dynamis](../../strongs/g/g1411.md), and they [epistrephō](../../strongs/g/g1994.md) to the [gē](../../strongs/g/g1093.md) of the Philistines (Phylistiim).

<a name="genesis_21_33"></a>Genesis 21:33

And [Abraam](../../strongs/g/g11.md) [phyteuō](../../strongs/g/g5452.md) fields (arouran) near the [phrear](../../strongs/g/g5421.md) of the [horkos](../../strongs/g/g3727.md). And he [epikaleō](../../strongs/g/g1941.md) there the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md), [theos](../../strongs/g/g2316.md) [aiōnios](../../strongs/g/g166.md). 

<a name="genesis_21_34"></a>Genesis 21:34

[paroikeō](../../strongs/g/g3939.md) [Abraam](../../strongs/g/g11.md) in the [gē](../../strongs/g/g1093.md) of the Philistines (Phylistiim) [hēmera](../../strongs/g/g2250.md) [polys](../../strongs/g/g4183.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 20](genesis_lxx_20.md) - [Genesis 22](genesis_lxx_22.md)