# [Genesis 15](https://www.blueletterbible.org/lxx/genesis/15)

<a name="genesis_15_1"></a>Genesis 15:1

And after these [rhēma](../../strongs/g/g4487.md) [ginomai](../../strongs/g/g1096.md) the [rhēma](../../strongs/g/g4487.md) of the [kyrios](../../strongs/g/g2962.md) to Abram in a [horama](../../strongs/g/g3705.md), [legō](../../strongs/g/g3004.md), Do not [phobeō](../../strongs/g/g5399.md) Abram, I will shield (hyperaspizō) you. Your [misthos](../../strongs/g/g3408.md) [polys](../../strongs/g/g4183.md) will be [sphodra](../../strongs/g/g4970.md). 

<a name="genesis_15_2"></a>Genesis 15:2

[legō](../../strongs/g/g3004.md) Abram, [despotēs](../../strongs/g/g1203.md), O [kyrios](../../strongs/g/g2962.md), what will you [didōmi](../../strongs/g/g1325.md) to me, for I am [apolyō](../../strongs/g/g630.md) [ateknos](../../strongs/g/g815.md), but the [huios](../../strongs/g/g5207.md) of Masek of my native-born maid servant (oikogenous), this [Damaskos](../../strongs/g/g1154.md) [Eliezer](../../strongs/g/g1663.md) is heir?

<a name="genesis_15_3"></a>Genesis 15:3

And Abram [eipon](../../strongs/g/g2036.md), Since to me you [didōmi](../../strongs/g/g1325.md) not a [sperma](../../strongs/g/g4690.md), then my native-born servant (oikogenēs) will be [klēronomeō](../../strongs/g/g2816.md) to me. 

<a name="genesis_15_4"></a>Genesis 15:4

And [euthys](../../strongs/g/g2117.md) the [phōnē](../../strongs/g/g5456.md) of the [kyrios](../../strongs/g/g2962.md) [ginomai](../../strongs/g/g1096.md) to him, [legō](../../strongs/g/g3004.md), will not be [klēronomeō](../../strongs/g/g2816.md) to you this one, another will [exerchomai](../../strongs/g/g1831.md) from you, this one will be [klēronomeō](../../strongs/g/g2816.md) to you. 

<a name="genesis_15_5"></a>Genesis 15:5

And he [exagō](../../strongs/g/g1806.md) him [exō](../../strongs/g/g1854.md), and [eipon](../../strongs/g/g2036.md) to him, [anablepō](../../strongs/g/g308.md) indeed into the [ouranos](../../strongs/g/g3772.md), and [arithmeō](../../strongs/g/g705.md) the [astēr](../../strongs/g/g792.md), if you are able to count (exarithmēsai) them! And he [eipon](../../strongs/g/g2036.md), Thus will be your [sperma](../../strongs/g/g4690.md). 

<a name="genesis_15_6"></a>Genesis 15:6

And Abram [pisteuō](../../strongs/g/g4100.md) in [theos](../../strongs/g/g2316.md), and it was [logizomai](../../strongs/g/g3049.md) to him for [dikaiosynē](../../strongs/g/g1343.md). 

<a name="genesis_15_7"></a>Genesis 15:7

And he [eipon](../../strongs/g/g2036.md) to him, I am the [theos](../../strongs/g/g2316.md) [exagō](../../strongs/g/g1806.md) you from out of the [chōra](../../strongs/g/g5561.md) of the [Chaldaios](../../strongs/g/g5466.md), so as to [didōmi](../../strongs/g/g1325.md) to you this [gē](../../strongs/g/g1093.md) to [klēronomeō](../../strongs/g/g2816.md). 

<a name="genesis_15_8"></a>Genesis 15:8

And he [eipon](../../strongs/g/g2036.md), [despotēs](../../strongs/g/g1203.md), O [kyrios](../../strongs/g/g2962.md), how will I [ginōskō](../../strongs/g/g1097.md) that I will [klēronomeō](../../strongs/g/g2816.md) it? 

<a name="genesis_15_9"></a>Genesis 15:9

And he [eipon](../../strongs/g/g2036.md) to him, [lambanō](../../strongs/g/g2983.md) for me a [damalis](../../strongs/g/g1151.md) being three years old (trietizousan), and a goat (aiga) being three years old (trietizousan), and a ram (krion) being three years old (trietizonta), and a [trygōn](../../strongs/g/g5167.md), and a [peristera](../../strongs/g/g4058.md)! 

<a name="genesis_15_10"></a>Genesis 15:10

And he [lambanō](../../strongs/g/g2983.md) to himself all these, and he [diaireō](../../strongs/g/g1244.md) them in the [mesos](../../strongs/g/g3319.md), and [tithēmi](../../strongs/g/g5087.md) them facing one (antiprosōpa) [allēlōn](../../strongs/g/g240.md); but the [orneon](../../strongs/g/g3732.md) he did not [diaireō](../../strongs/g/g1244.md). 

<a name="genesis_15_11"></a>Genesis 15:11

[katabainō](../../strongs/g/g2597.md) [orneon](../../strongs/g/g3732.md) upon the [sōma](../../strongs/g/g4983.md) pieces of their, and [sygkathizō](../../strongs/g/g4776.md) with them Abram.

<a name="genesis_15_12"></a>Genesis 15:12

And about of the [hēlios](../../strongs/g/g2246.md) the [dysmē](../../strongs/g/g1424.md) a change of [ekstasis](../../strongs/g/g1611.md) [epipiptō](../../strongs/g/g1968.md) Abram. And [idou](../../strongs/g/g2400.md), [phobos](../../strongs/g/g5401.md) [skoteinos](../../strongs/g/g4652.md) [megas](../../strongs/g/g3173.md) [epipiptō](../../strongs/g/g1968.md) him. 

<a name="genesis_15_13"></a>Genesis 15:13

And it was [eipon](../../strongs/g/g2046.md) to Abram, In [ginōskō](../../strongs/g/g1097.md) you will [ginōskō](../../strongs/g/g1097.md) that a [paroikos](../../strongs/g/g3941.md) will be your [sperma](../../strongs/g/g4690.md) in a [gē](../../strongs/g/g1093.md) not their own, and they will [douloō](../../strongs/g/g1402.md) them, and will [kakoō](../../strongs/g/g2559.md) them, and [tapeinoō](../../strongs/g/g5013.md) them [tetrakosioi](../../strongs/g/g5071.md) [etos](../../strongs/g/g2094.md). 

<a name="genesis_15_14"></a>Genesis 15:14

But the [ethnos](../../strongs/g/g1484.md) which ever they may be [douleuō](../../strongs/g/g1398.md), I will [krinō](../../strongs/g/g2919.md). And after these things they shall [exerchomai](../../strongs/g/g1831.md) here with belongings (aposkeuēs) [polys](../../strongs/g/g4183.md). 

<a name="genesis_15_15"></a>Genesis 15:15

But you shall [aperchomai](../../strongs/g/g565.md) to your [patēr](../../strongs/g/g3962.md) with [eirēnē](../../strongs/g/g1515.md), being [thaptō](../../strongs/g/g2290.md) in [gēras](../../strongs/g/g1094.md) [kalos](../../strongs/g/g2570.md). 

<a name="genesis_15_16"></a>Genesis 15:16

And the [tetartos](../../strongs/g/g5067.md) [genea](../../strongs/g/g1074.md) shall [apostrephō](../../strongs/g/g654.md) here, for not yet have been [anaplēroō](../../strongs/g/g378.md) the [hamartia](../../strongs/g/g266.md) of the Amorites unto the [nyn](../../strongs/g/g3568.md). 

<a name="genesis_15_17"></a>Genesis 15:17

And when the [hēlios](../../strongs/g/g2246.md) was in [dysmē](../../strongs/g/g1424.md), a [phlox](../../strongs/g/g5395.md) [ginomai](../../strongs/g/g1096.md), and [idou](../../strongs/g/g2400.md), there was an [klibanos](../../strongs/g/g2823.md) smoking (kapnizomenos); and there were [lampas](../../strongs/g/g2985.md) of [pyr](../../strongs/g/g4442.md) which went [dierchomai](../../strongs/g/g1330.md) in the [mesos](../../strongs/g/g3319.md) of these pieces (dichotomēmatōn).

<a name="genesis_15_18"></a>Genesis 15:18

In that [hēmera](../../strongs/g/g2250.md) the [kyrios](../../strongs/g/g2962.md) [diathēkē](../../strongs/g/g1242.md) with Abram, [legō](../../strongs/g/g3004.md), To your [sperma](../../strongs/g/g4690.md) I will [didōmi](../../strongs/g/g1325.md) this [gē](../../strongs/g/g1093.md), from the [potamos](../../strongs/g/g4215.md) of [Aigyptos](../../strongs/g/g125.md) unto the [potamos](../../strongs/g/g4215.md) of the [megas](../../strongs/g/g3173.md) [Euphratēs](../../strongs/g/g2166.md),

<a name="genesis_15_19"></a>Genesis 15:19

the Kenites, and the Kenizites, and the Kadmonites,

<a name="genesis_15_20"></a>Genesis 15:20

and the Hittites, and the Perizzites, and the Raphaim,

<a name="genesis_15_21"></a>Genesis 15:21

and the Amorites, and the [Chananaios](../../strongs/g/g5478.md), and the Girgashites, and the Jebusites.

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 14](genesis_lxx_14.md) - [Genesis 16](genesis_lxx_16.md)