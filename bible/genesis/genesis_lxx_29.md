# [Genesis 29](https://www.blueletterbible.org/lxx/genesis/29)

<a name="genesis_29_1"></a>Genesis 29:1

And [Iakōb](../../strongs/g/g2384.md) [exairō](../../strongs/g/g1808.md) his [pous](../../strongs/g/g4228.md), [poreuō](../../strongs/g/g4198.md) into the [gē](../../strongs/g/g1093.md) of the [anatolē](../../strongs/g/g395.md) to Laban, the [huios](../../strongs/g/g5207.md) of Bethuel the Syrian, and [adelphos](../../strongs/g/g80.md) of [Rhebekka](../../strongs/g/g4479.md), [mētēr](../../strongs/g/g3384.md) of [Iakōb](../../strongs/g/g2384.md) and [Ēsau](../../strongs/g/g2269.md).

<a name="genesis_29_2"></a>Genesis 29:2

And he [horaō](../../strongs/g/g3708.md), and [idou](../../strongs/g/g2400.md), there was a [phrear](../../strongs/g/g5421.md) in the plain (pediō). And there were there [treis](../../strongs/g/g5140.md) [poimnion](../../strongs/g/g4168.md) of [probaton](../../strongs/g/g4263.md) [anapauō](../../strongs/g/g373.md) near it, for from out of that [phrear](../../strongs/g/g5421.md) they [potizō](../../strongs/g/g4222.md) the [poimnion](../../strongs/g/g4168.md). [lithos](../../strongs/g/g3037.md) there was a [megas](../../strongs/g/g3173.md) upon the [stoma](../../strongs/g/g4750.md) of the [phrear](../../strongs/g/g5421.md). 

<a name="genesis_29_3"></a>Genesis 29:3

And [synagō](../../strongs/g/g4863.md) there all the [poimnion](../../strongs/g/g4168.md). And they [apokyliō](../../strongs/g/g617.md) the [lithos](../../strongs/g/g3037.md) from the [stoma](../../strongs/g/g4750.md) of the [phrear](../../strongs/g/g5421.md), and they [potizō](../../strongs/g/g4222.md) the [probaton](../../strongs/g/g4263.md), and [apokathistēmi](../../strongs/g/g600.md) the [lithos](../../strongs/g/g3037.md) upon the [stoma](../../strongs/g/g4750.md) of the [phrear](../../strongs/g/g5421.md) into its [topos](../../strongs/g/g5117.md). 

<a name="genesis_29_4"></a>Genesis 29:4

[eipon](../../strongs/g/g2036.md) to them [Iakōb](../../strongs/g/g2384.md), [adelphos](../../strongs/g/g80.md), [pothen](../../strongs/g/g4159.md) are you? And they [eipon](../../strongs/g/g2036.md), from [Charran](../../strongs/g/g5488.md) we are.

<a name="genesis_29_5"></a>Genesis 29:5

And he [eipon](../../strongs/g/g2036.md) to them, You [ginōskō](../../strongs/g/g1097.md) Laban the [huios](../../strongs/g/g5207.md) of Nahor? And they [eipon](../../strongs/g/g2036.md), We [ginōskō](../../strongs/g/g1097.md) him. 

<a name="genesis_29_6"></a>Genesis 29:6

And he [eipon](../../strongs/g/g2036.md) to them, Is he in [hygiainō](../../strongs/g/g5198.md)? And they [eipon](../../strongs/g/g2036.md), He is in [hygiainō](../../strongs/g/g5198.md), and [idou](../../strongs/g/g2400.md), [Rhachēl](../../strongs/g/g4478.md) his [thygatēr](../../strongs/g/g2364.md) [erchomai](../../strongs/g/g2064.md) with the [probaton](../../strongs/g/g4263.md). 

<a name="genesis_29_7"></a>Genesis 29:7

And [Iakōb](../../strongs/g/g2384.md) [eipon](../../strongs/g/g2036.md), There is still [hēmera](../../strongs/g/g2250.md) [polys](../../strongs/g/g4183.md), for not yet is the [hōra](../../strongs/g/g5610.md) to [synagō](../../strongs/g/g4863.md) for the [ktēnos](../../strongs/g/g2934.md); having [potizō](../../strongs/g/g4222.md) the [probaton](../../strongs/g/g4263.md), [aperchomai](../../strongs/g/g565.md) them [boskō](../../strongs/g/g1006.md)! 

<a name="genesis_29_8"></a>Genesis 29:8

And they [eipon](../../strongs/g/g2036.md), We are not able until the [synagō](../../strongs/g/g4863.md) of all the [poimēn](../../strongs/g/g4166.md), for they should [apokyliō](../../strongs/g/g617.md) the [lithos](../../strongs/g/g3037.md) from the [stoma](../../strongs/g/g4750.md) of the [phrear](../../strongs/g/g5421.md), and we shall [potizō](../../strongs/g/g4222.md) the [probaton](../../strongs/g/g4263.md). 

<a name="genesis_29_9"></a>Genesis 29:9

While he was [laleō](../../strongs/g/g2980.md) to them, and [idou](../../strongs/g/g2400.md), [Rhachēl](../../strongs/g/g4478.md) the [thygatēr](../../strongs/g/g2364.md) of Laban [erchomai](../../strongs/g/g2064.md) with the [probaton](../../strongs/g/g4263.md) of her [patēr](../../strongs/g/g3962.md). For she [boskō](../../strongs/g/g1006.md) the [probaton](../../strongs/g/g4263.md) of her [patēr](../../strongs/g/g3962.md).

<a name="genesis_29_10"></a>Genesis 29:10

And [ginomai](../../strongs/g/g1096.md) as [Iakōb](../../strongs/g/g2384.md) [eidō](../../strongs/g/g1492.md) [Rhachēl](../../strongs/g/g4478.md), the [thygatēr](../../strongs/g/g2364.md) of Laban, [adelphos](../../strongs/g/g80.md) of his [mētēr](../../strongs/g/g3384.md), and the [probaton](../../strongs/g/g4263.md) of Laban, the [adelphos](../../strongs/g/g80.md) of his [mētēr](../../strongs/g/g3384.md); and [proserchomai](../../strongs/g/g4334.md), [Iakōb](../../strongs/g/g2384.md) [apokyliō](../../strongs/g/g617.md) the [lithos](../../strongs/g/g3037.md) from the [stoma](../../strongs/g/g4750.md) of the [phrear](../../strongs/g/g5421.md). And he [potizō](../../strongs/g/g4222.md) the [probaton](../../strongs/g/g4263.md) of Laban, the [adelphos](../../strongs/g/g80.md) of his [mētēr](../../strongs/g/g3384.md).

<a name="genesis_29_11"></a>Genesis 29:11

And [Iakōb](../../strongs/g/g2384.md) [phileō](../../strongs/g/g5368.md) [Rhachēl](../../strongs/g/g4478.md). And [boaō](../../strongs/g/g994.md) with his [phōnē](../../strongs/g/g5456.md) he [klaiō](../../strongs/g/g2799.md). 

<a name="genesis_29_12"></a>Genesis 29:12

And he [apaggellō](../../strongs/g/g518.md) to [Rhachēl](../../strongs/g/g4478.md) that a [adelphos](../../strongs/g/g80.md) of her [patēr](../../strongs/g/g3962.md) he is, and that a [huios](../../strongs/g/g5207.md) of [Rhebekka](../../strongs/g/g4479.md) he is. And she [trechō](../../strongs/g/g5143.md) to [apaggellō](../../strongs/g/g518.md) to her [patēr](../../strongs/g/g3962.md) concerning these [rhēma](../../strongs/g/g4487.md). 

<a name="genesis_29_13"></a>Genesis 29:13

And [ginomai](../../strongs/g/g1096.md) as Laban [akouō](../../strongs/g/g191.md) the [onoma](../../strongs/g/g3686.md) [Iakōb](../../strongs/g/g2384.md), the [huios](../../strongs/g/g5207.md) of his [adelphē](../../strongs/g/g79.md), he [trechō](../../strongs/g/g5143.md) to [synantēsis](../../strongs/g/g4877.md) him. And taking hold (perilabōn) of him, he [phileō](../../strongs/g/g5368.md) and [eisagō](../../strongs/g/g1521.md) him into his [oikos](../../strongs/g/g3624.md). And he [diēgeomai](../../strongs/g/g1334.md) to Laban all these [logos](../../strongs/g/g3056.md). 

<a name="genesis_29_14"></a>Genesis 29:14

And [eipon](../../strongs/g/g2036.md) to him Laban, From out of my [osteon](../../strongs/g/g3747.md), and from out of my [sarx](../../strongs/g/g4561.md) are you. And he was with him a [mēn](../../strongs/g/g3376.md) of [hēmera](../../strongs/g/g2250.md). 

<a name="genesis_29_15"></a>Genesis 29:15

[eipon](../../strongs/g/g2036.md) Laban to [Iakōb](../../strongs/g/g2384.md) that, For [adelphos](../../strongs/g/g80.md) you are my, you shall not be a [douleuō](../../strongs/g/g1398.md) to me without [dōrea](../../strongs/g/g1431.md), you [apaggellō](../../strongs/g/g518.md) to me what your [misthos](../../strongs/g/g3408.md) is?

<a name="genesis_29_16"></a>Genesis 29:16

And to Laban there were [dyo](../../strongs/g/g1417.md) [thygatēr](../../strongs/g/g2364.md), the [onoma](../../strongs/g/g3686.md) of the [megas](../../strongs/g/g3173.md) Leah, and the [onoma](../../strongs/g/g3686.md) of the [neos](../../strongs/g/g3501.md) [Rhachēl](../../strongs/g/g4478.md).

<a name="genesis_29_17"></a>Genesis 29:17

But the [ophthalmos](../../strongs/g/g3788.md) of Leah were [asthenēs](../../strongs/g/g772.md), but [Rhachēl](../../strongs/g/g4478.md) was [kalos](../../strongs/g/g2570.md) to the [eidos](../../strongs/g/g1491.md), and [hōraios](../../strongs/g/g5611.md) in [opsis](../../strongs/g/g3799.md) [sphodra](../../strongs/g/g4970.md). 

<a name="genesis_29_18"></a>Genesis 29:18

[agapaō](../../strongs/g/g25.md) [Iakōb](../../strongs/g/g2384.md) [Rhachēl](../../strongs/g/g4478.md). And he [eipon](../../strongs/g/g2036.md), I will [douleuō](../../strongs/g/g1398.md) you [hepta](../../strongs/g/g2033.md) [etos](../../strongs/g/g2094.md) for [Rhachēl](../../strongs/g/g4478.md) [thygatēr](../../strongs/g/g2364.md) your [neos](../../strongs/g/g3501.md). 

<a name="genesis_29_19"></a>Genesis 29:19

[eipon](../../strongs/g/g2036.md) to him Laban, [beltiōn](../../strongs/g/g957.md) for me to [didōmi](../../strongs/g/g1325.md) her to you, than for me to [didōmi](../../strongs/g/g1325.md) her [anēr](../../strongs/g/g435.md) to another. You [oikeō](../../strongs/g/g3611.md) with me! 

<a name="genesis_29_20"></a>Genesis 29:20

And [Iakōb](../../strongs/g/g2384.md) [douleuō](../../strongs/g/g1398.md) for [Rhachēl](../../strongs/g/g4478.md) [hepta](../../strongs/g/g2033.md) [etos](../../strongs/g/g2094.md), and they were [enantion](../../strongs/g/g1726.md) him as [hēmera](../../strongs/g/g2250.md) a few[oligos](../../strongs/g/g3641.md), because of his [agapaō](../../strongs/g/g25.md) for her. 

<a name="genesis_29_21"></a>Genesis 29:21

[eipon](../../strongs/g/g2036.md) [Iakōb](../../strongs/g/g2384.md) to Laban, [didōmi](../../strongs/g/g1325.md) to me my [gynē](../../strongs/g/g1135.md), are [plēroō](../../strongs/g/g4137.md) for the [hēmera](../../strongs/g/g2250.md) so as to [eiserchomai](../../strongs/g/g1525.md) to her! 

<a name="genesis_29_22"></a>Genesis 29:22

[synagō](../../strongs/g/g4863.md) Laban all the [anēr](../../strongs/g/g435.md) of the [topos](../../strongs/g/g5117.md), and he [poieō](../../strongs/g/g4160.md) a [gamos](../../strongs/g/g1062.md). 

<a name="genesis_29_23"></a>Genesis 29:23

And it became [hespera](../../strongs/g/g2073.md). And [lambanō](../../strongs/g/g2983.md) Leah his [thygatēr](../../strongs/g/g2364.md), he [eisagō](../../strongs/g/g1521.md) her to [Iakōb](../../strongs/g/g2384.md), and [eiserchomai](../../strongs/g/g1525.md) to her [Iakōb](../../strongs/g/g2384.md).

<a name="genesis_29_24"></a>Genesis 29:24

[didōmi](../../strongs/g/g1325.md) Laban Leah his [thygatēr](../../strongs/g/g2364.md) Zilpah his [paidiskē](../../strongs/g/g3814.md). 

<a name="genesis_29_25"></a>Genesis 29:25

And it [ginomai](../../strongs/g/g1096.md) in the [prōï](../../strongs/g/g4404.md), and [idou](../../strongs/g/g2400.md), there was Leah. [eipon](../../strongs/g/g2036.md) [Iakōb](../../strongs/g/g2384.md) to Laban, What is this you [poieō](../../strongs/g/g4160.md) to me, was it not on account of [Rhachēl](../../strongs/g/g4478.md) I [douleuō](../../strongs/g/g1398.md) for you, and why did you [paralogizomai](../../strongs/g/g3884.md) me? 

<a name="genesis_29_26"></a>Genesis 29:26

[eipon](../../strongs/g/g2036.md) Laban, It is not so in our [topos](../../strongs/g/g5117.md) to [didōmi](../../strongs/g/g1325.md) the [neos](../../strongs/g/g3501.md) before the [presbyteros](../../strongs/g/g4245.md). 

<a name="genesis_29_27"></a>Genesis 29:27

You [synteleō](../../strongs/g/g4931.md) then these [ebdomos](../../strongs/g/g1442.md) and I will [didōmi](../../strongs/g/g1325.md) to you also this for the [ergasia](../../strongs/g/g2039.md) of which you will [ergazomai](../../strongs/g/g2038.md) for me, yet [hepta](../../strongs/g/g2033.md) [etos](../../strongs/g/g2094.md) another. 

<a name="genesis_29_28"></a>Genesis 29:28

did [poieō](../../strongs/g/g4160.md) Jacob so, and [anaplēroō](../../strongs/g/g378.md) these her [ebdomos](../../strongs/g/g1442.md). And [didōmi](../../strongs/g/g1325.md) to him Laban [Rhachēl](../../strongs/g/g4478.md) his [thygatēr](../../strongs/g/g2364.md), to him as [gynē](../../strongs/g/g1135.md). 

<a name="genesis_29_29"></a>Genesis 29:29

[didōmi](../../strongs/g/g1325.md) Laban to his [thygatēr](../../strongs/g/g2364.md) Bilhah the [paidiskē](../../strongs/g/g3814.md). 

<a name="genesis_29_30"></a>Genesis 29:30

And he [eiserchomai](../../strongs/g/g1525.md) to [Rhachēl](../../strongs/g/g4478.md). And he [agapaō](../../strongs/g/g25.md) [Rhachēl](../../strongs/g/g4478.md) rather than Leah. And he [douleuō](../../strongs/g/g1398.md) him [hepta](../../strongs/g/g2033.md) [etos](../../strongs/g/g2094.md) another. 

<a name="genesis_29_31"></a>Genesis 29:31

[eidō](../../strongs/g/g1492.md) the [kyrios](../../strongs/g/g2962.md) that Leah was [miseō](../../strongs/g/g3404.md), he [anoigō](../../strongs/g/g455.md) her [mētra](../../strongs/g/g3388.md). But [Rhachēl](../../strongs/g/g4478.md) was [steira](../../strongs/g/g4723.md). 

<a name="genesis_29_32"></a>Genesis 29:32

And Leah [syllambanō](../../strongs/g/g4815.md) and [tiktō](../../strongs/g/g5088.md) a [huios](../../strongs/g/g5207.md) to [Iakōb](../../strongs/g/g2384.md). And she [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md), [Rhoubēn](../../strongs/g/g4502.md), [legō](../../strongs/g/g3004.md), Because [eidō](../../strongs/g/g1492.md) my the [kyrios](../../strongs/g/g2962.md) the [tapeinōsis](../../strongs/g/g5014.md), and he [didōmi](../../strongs/g/g1325.md) to me a [huios](../../strongs/g/g5207.md); now then will [agapaō](../../strongs/g/g25.md) me my [anēr](../../strongs/g/g435.md). 

<a name="genesis_29_33"></a>Genesis 29:33

And [syllambanō](../../strongs/g/g4815.md) again Leah and [tiktō](../../strongs/g/g5088.md) [huios](../../strongs/g/g5207.md) a [deuteros](../../strongs/g/g1208.md) to [Iakōb](../../strongs/g/g2384.md). And she [eipon](../../strongs/g/g2036.md), For the [kyrios](../../strongs/g/g2962.md) [akouō](../../strongs/g/g191.md) that I am [miseō](../../strongs/g/g3404.md), and he gave in addition (prosedōken) to me also this one. And she [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md), [Symeōn](../../strongs/g/g4826.md).

<a name="genesis_29_34"></a>Genesis 29:34

And she [syllambanō](../../strongs/g/g4815.md) again and [tiktō](../../strongs/g/g5088.md) a [huios](../../strongs/g/g5207.md). And she [eipon](../../strongs/g/g2036.md), In the present [kairos](../../strongs/g/g2540.md) by me will be my [anēr](../../strongs/g/g435.md), for I [tiktō](../../strongs/g/g5088.md) to him [treis](../../strongs/g/g5140.md) [huios](../../strongs/g/g5207.md). On account of this she [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md), [Leui](../../strongs/g/g3017.md).

<a name="genesis_29_35"></a>Genesis 29:35

And [syllambanō](../../strongs/g/g4815.md) again she [tiktō](../../strongs/g/g5088.md) a [huios](../../strongs/g/g5207.md). And she [eipon](../../strongs/g/g2036.md), Now yet this I will [exomologeō](../../strongs/g/g1843.md) to the [kyrios](../../strongs/g/g2962.md). On account of this she [kaleō](../../strongs/g/g2564.md) his [onoma](../../strongs/g/g3686.md), [Iouda](../../strongs/g/g2448.md). And she [histēmi](../../strongs/g/g2476.md) [tiktō](../../strongs/g/g5088.md). 

---

[Transliteral Bible](../bible.md)

[Genesis](genesis_lxx.md)

[Genesis 28](genesis_lxx_28.md) - [Genesis 30](genesis_lxx_30.md)