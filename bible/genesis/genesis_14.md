# [Genesis 14](https://www.blueletterbible.org/kjv/gen/14/1/s_14001)

<a name="genesis_14_1"></a>Genesis 14:1

And it came to pass in the [yowm](../../strongs/h/h3117.md) of  ['amrāp̄el](../../strongs/h/h569.md) [melek](../../strongs/h/h4428.md) of [Šinʿār](../../strongs/h/h8152.md), ['aryôḵ](../../strongs/h/h746.md) [melek](../../strongs/h/h4428.md) of ['ellāsār](../../strongs/h/h495.md), [Kᵊḏārlāʿōmer](../../strongs/h/h3540.md) [melek](../../strongs/h/h4428.md) of [ʿÊlām](../../strongs/h/h5867.md), and [tiḏʿāl](../../strongs/h/h8413.md) [melek](../../strongs/h/h4428.md) of [gowy](../../strongs/h/h1471.md);

<a name="genesis_14_2"></a>Genesis 14:2

That these ['asah](../../strongs/h/h6213.md) [milḥāmâ](../../strongs/h/h4421.md) with [beraʿ](../../strongs/h/h1298.md) [melek](../../strongs/h/h4428.md) of [Sᵊḏōm](../../strongs/h/h5467.md), and with [biršaʿ](../../strongs/h/h1306.md) [melek](../../strongs/h/h4428.md) of [ʿĂmōrâ](../../strongs/h/h6017.md), [šin'āḇ](../../strongs/h/h8134.md) [melek](../../strongs/h/h4428.md) of ['Aḏmâ](../../strongs/h/h126.md), and [šem'ēḇer](../../strongs/h/h8038.md) [melek](../../strongs/h/h4428.md) of [Ṣᵊḇā'îm](../../strongs/h/h6636.md), and the [melek](../../strongs/h/h4428.md) of [Belaʿ](../../strongs/h/h1106.md), which is [Ṣōʿar](../../strongs/h/h6820.md).

<a name="genesis_14_3"></a>Genesis 14:3

All these were [ḥāḇar](../../strongs/h/h2266.md) in the [ʿēmeq](../../strongs/h/h6010.md) of [śidîm](../../strongs/h/h7708.md), which is the [melaḥ](../../strongs/h/h4417.md) [yam](../../strongs/h/h3220.md).

<a name="genesis_14_4"></a>Genesis 14:4

Twelve [šānâ](../../strongs/h/h8141.md) they ['abad](../../strongs/h/h5647.md) [Kᵊḏārlāʿōmer](../../strongs/h/h3540.md), and in the thirteenth [šānâ](../../strongs/h/h8141.md) they [māraḏ](../../strongs/h/h4775.md).

<a name="genesis_14_5"></a>Genesis 14:5

And in the fourteenth [šānâ](../../strongs/h/h8141.md) [bow'](../../strongs/h/h935.md) [Kᵊḏārlāʿōmer](../../strongs/h/h3540.md), and the [melek](../../strongs/h/h4428.md) that were with him, and [nakah](../../strongs/h/h5221.md) the [rᵊp̄ā'îm](../../strongs/h/h7497.md) in [ʿaštᵊrôṯ qarnayim](../../strongs/h/h6255.md), and the [zûzîm](../../strongs/h/h2104.md) in [ham](../../strongs/h/h1990.md), and the ['Êmîm](../../strongs/h/h368.md) in [šāvê qiriyāṯayim](../../strongs/h/h7741.md),

<a name="genesis_14_6"></a>Genesis 14:6

And the [ḥōrî](../../strongs/h/h2752.md) in their [har](../../strongs/h/h2042.md) [Śēʿîr](../../strongs/h/h8165.md), unto ['êl pā'rān](../../strongs/h/h364.md), which is by the [midbar](../../strongs/h/h4057.md).

<a name="genesis_14_7"></a>Genesis 14:7

And they [shuwb](../../strongs/h/h7725.md), and [bow'](../../strongs/h/h935.md) to [ʿên mišp̄āṭ](../../strongs/h/h5880.md), which is [Qāḏēš](../../strongs/h/h6946.md), and [nakah](../../strongs/h/h5221.md) all the [sadeh](../../strongs/h/h7704.md) of the [ʿămālēqî](../../strongs/h/h6003.md), and also the ['Ĕmōrî](../../strongs/h/h567.md), that [yashab](../../strongs/h/h3427.md) in [Ḥaṣṣôn Tāmār](../../strongs/h/h2688.md).

<a name="genesis_14_8"></a>Genesis 14:8

And there [yāṣā'](../../strongs/h/h3318.md) the [melek](../../strongs/h/h4428.md) of [Sᵊḏōm](../../strongs/h/h5467.md), and the [melek](../../strongs/h/h4428.md) of [ʿĂmōrâ](../../strongs/h/h6017.md), and the [melek](../../strongs/h/h4428.md) of ['Aḏmâ](../../strongs/h/h126.md), and the [melek](../../strongs/h/h4428.md) of [Ṣᵊḇā'îm](../../strongs/h/h6636.md), and the [melek](../../strongs/h/h4428.md) of [Belaʿ](../../strongs/h/h1106.md) (the same is [Ṣōʿar](../../strongs/h/h6820.md);) and they ['arak](../../strongs/h/h6186.md) [milḥāmâ](../../strongs/h/h4421.md) with them in the [ʿēmeq](../../strongs/h/h6010.md) of [śidîm](../../strongs/h/h7708.md);

<a name="genesis_14_9"></a>Genesis 14:9

With [Kᵊḏārlāʿōmer](../../strongs/h/h3540.md) the [melek](../../strongs/h/h4428.md) of [ʿÊlām](../../strongs/h/h5867.md), and with [tiḏʿāl](../../strongs/h/h8413.md) [melek](../../strongs/h/h4428.md) of [gowy](../../strongs/h/h1471.md), and ['amrāp̄el](../../strongs/h/h569.md) [melek](../../strongs/h/h4428.md) of [Šinʿār](../../strongs/h/h8152.md), and ['aryôḵ](../../strongs/h/h746.md) [melek](../../strongs/h/h4428.md) of ['ellāsār](../../strongs/h/h495.md); four kings with five.

<a name="genesis_14_10"></a>Genesis 14:10

And the [ʿēmeq](../../strongs/h/h6010.md) of [śidîm](../../strongs/h/h7708.md) was full of [bᵊ'ēr](../../strongs/h/h875.md) [ḥēmār](../../strongs/h/h2564.md); and the [melek](../../strongs/h/h4428.md) of [Sᵊḏōm](../../strongs/h/h5467.md) and [ʿĂmōrâ](../../strongs/h/h6017.md) [nûs](../../strongs/h/h5127.md), and [naphal](../../strongs/h/h5307.md) there; and they that [šā'ar](../../strongs/h/h7604.md) [nûs](../../strongs/h/h5127.md) to the [har](../../strongs/h/h2022.md).

<a name="genesis_14_11"></a>Genesis 14:11

And they [laqach](../../strongs/h/h3947.md) all the [rᵊḵûš](../../strongs/h/h7399.md) of [Sᵊḏōm](../../strongs/h/h5467.md) and [ʿĂmōrâ](../../strongs/h/h6017.md), and all their ['ōḵel](../../strongs/h/h400.md), and [yālaḵ](../../strongs/h/h3212.md).

<a name="genesis_14_12"></a>Genesis 14:12

And they [laqach](../../strongs/h/h3947.md) [Lôṭ](../../strongs/h/h3876.md), ['Aḇrām](../../strongs/h/h87.md) ['ach](../../strongs/h/h251.md) [ben](../../strongs/h/h1121.md), who [yashab](../../strongs/h/h3427.md) in [Sᵊḏōm](../../strongs/h/h5467.md), and his [rᵊḵûš](../../strongs/h/h7399.md), and [yālaḵ](../../strongs/h/h3212.md).

<a name="genesis_14_13"></a>Genesis 14:13

And there [bow'](../../strongs/h/h935.md) one that had [pālîṭ](../../strongs/h/h6412.md), and [nāḡaḏ](../../strongs/h/h5046.md) ['Aḇrām](../../strongs/h/h87.md) the [ʿiḇrî](../../strongs/h/h5680.md); for he [shakan](../../strongs/h/h7931.md) in the ['ēlôn](../../strongs/h/h436.md) of [mamrē'](../../strongs/h/h4471.md) the ['Ĕmōrî](../../strongs/h/h567.md), ['ach](../../strongs/h/h251.md) of ['eškōl](../../strongs/h/h812.md), and ['ach](../../strongs/h/h251.md) of [ʿānēr](../../strongs/h/h6063.md): and these were [baʿal](../../strongs/h/h1167.md) [bĕriyth](../../strongs/h/h1285.md) with ['Aḇrām](../../strongs/h/h87.md).

<a name="genesis_14_14"></a>Genesis 14:14

And when ['Aḇrām](../../strongs/h/h87.md) [shama'](../../strongs/h/h8085.md) that his ['ach](../../strongs/h/h251.md) was taken [šāḇâ](../../strongs/h/h7617.md), he [rîq](../../strongs/h/h7324.md) his [ḥānîḵ](../../strongs/h/h2593.md), [yālîḏ](../../strongs/h/h3211.md) in his own [bayith](../../strongs/h/h1004.md), three hundred and eighteen, and [radaph](../../strongs/h/h7291.md) them unto [Dān](../../strongs/h/h1835.md).

<a name="genesis_14_15"></a>Genesis 14:15

And he [chalaq](../../strongs/h/h2505.md) himself against them, he and his ['ebed](../../strongs/h/h5650.md), by [layil](../../strongs/h/h3915.md), and [nakah](../../strongs/h/h5221.md) them, and [radaph](../../strongs/h/h7291.md) them unto [ḥôḇâ](../../strongs/h/h2327.md), which is on the [śᵊmō'l](../../strongs/h/h8040.md) of [Dammeśeq](../../strongs/h/h1834.md).

<a name="genesis_14_16"></a>Genesis 14:16

And he [shuwb](../../strongs/h/h7725.md) all the [rᵊḵûš](../../strongs/h/h7399.md), and also [shuwb](../../strongs/h/h7725.md) his ['ach](../../strongs/h/h251.md) [Lôṭ](../../strongs/h/h3876.md), and his [rᵊḵûš](../../strongs/h/h7399.md), and the ['ishshah](../../strongs/h/h802.md) also, and the ['am](../../strongs/h/h5971.md).

<a name="genesis_14_17"></a>Genesis 14:17

And the [melek](../../strongs/h/h4428.md) of [Sᵊḏōm](../../strongs/h/h5467.md) [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) him ['aḥar](../../strongs/h/h310.md) his [shuwb](../../strongs/h/h7725.md) from the [nakah](../../strongs/h/h5221.md) of [Kᵊḏārlāʿōmer](../../strongs/h/h3540.md), and of the [melek](../../strongs/h/h4428.md) that were with him, at the [ʿēmeq](../../strongs/h/h6010.md) of [šāvê](../../strongs/h/h7740.md), which is the [melek](../../strongs/h/h4428.md) [ʿēmeq](../../strongs/h/h6010.md).

<a name="genesis_14_18"></a>Genesis 14:18

And [Malkî-ṣeḏeq](../../strongs/h/h4442.md) [melek](../../strongs/h/h4428.md) of [šālēm](../../strongs/h/h8004.md) [yāṣā'](../../strongs/h/h3318.md) [lechem](../../strongs/h/h3899.md) and [yayin](../../strongs/h/h3196.md): and he was the [kōhēn](../../strongs/h/h3548.md) of the ['elyown](../../strongs/h/h5945.md) ['el](../../strongs/h/h410.md).

<a name="genesis_14_19"></a>Genesis 14:19

And he [barak](../../strongs/h/h1288.md) him, and ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be ['Aḇrām](../../strongs/h/h87.md) of the ['elyown](../../strongs/h/h5945.md) ['el](../../strongs/h/h410.md), [qānâ](../../strongs/h/h7069.md) of [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md):

<a name="genesis_14_20"></a>Genesis 14:20

And [barak](../../strongs/h/h1288.md) be the ['elyown](../../strongs/h/h5945.md) ['el](../../strongs/h/h410.md), which hath [māḡan](../../strongs/h/h4042.md) thine [tsar](../../strongs/h/h6862.md) into thy [yad](../../strongs/h/h3027.md). And he [nathan](../../strongs/h/h5414.md) him [maʿăśēr](../../strongs/h/h4643.md) of all.

<a name="genesis_14_21"></a>Genesis 14:21

And the [melek](../../strongs/h/h4428.md) of [Sᵊḏōm](../../strongs/h/h5467.md) ['āmar](../../strongs/h/h559.md) unto ['Aḇrām](../../strongs/h/h87.md), [nathan](../../strongs/h/h5414.md) me the [nephesh](../../strongs/h/h5315.md), and [laqach](../../strongs/h/h3947.md) the [rᵊḵûš](../../strongs/h/h7399.md) to thyself.

<a name="genesis_14_22"></a>Genesis 14:22

And ['Aḇrām](../../strongs/h/h87.md) ['āmar](../../strongs/h/h559.md) to the [melek](../../strongs/h/h4428.md) of [Sᵊḏōm](../../strongs/h/h5467.md), I have [ruwm](../../strongs/h/h7311.md) mine [yad](../../strongs/h/h3027.md) unto [Yĕhovah](../../strongs/h/h3068.md), the ['elyown](../../strongs/h/h5945.md) ['el](../../strongs/h/h410.md), the [qānâ](../../strongs/h/h7069.md) of [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md),

<a name="genesis_14_23"></a>Genesis 14:23

That I will not take from a [ḥûṭ](../../strongs/h/h2339.md) even to a [naʿal](../../strongs/h/h5275.md) [śᵊrôḵ](../../strongs/h/h8288.md), and that I will not [laqach](../../strongs/h/h3947.md) any thing that is thine, lest thou shouldest ['āmar](../../strongs/h/h559.md), I have made ['Aḇrām](../../strongs/h/h87.md) [ʿāšar](../../strongs/h/h6238.md):

<a name="genesis_14_24"></a>Genesis 14:24

[bilʿăḏê](../../strongs/h/h1107.md) only that which the [naʿar](../../strongs/h/h5288.md) have ['akal](../../strongs/h/h398.md), and the [cheleq](../../strongs/h/h2506.md) of the ['enowsh](../../strongs/h/h582.md) which [halak](../../strongs/h/h1980.md) with me, [ʿānēr](../../strongs/h/h6063.md), ['eškōl](../../strongs/h/h812.md), and [mamrē'](../../strongs/h/h4471.md); let them [laqach](../../strongs/h/h3947.md) their [cheleq](../../strongs/h/h2506.md).

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 13](genesis_13.md) - [Genesis 15](genesis_15.md)