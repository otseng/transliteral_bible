# [Genesis 6](https://www.blueletterbible.org/kjv/gen/6/1/s_6001)

<a name="genesis_6_1"></a>Genesis 6:1

And it came to pass, when ['adam](../../strongs/h/h120.md) [ḥālal](../../strongs/h/h2490.md) to [rabab](../../strongs/h/h7231.md) on the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md), and [bath](../../strongs/h/h1323.md) were [yalad](../../strongs/h/h3205.md) unto them,

<a name="genesis_6_2"></a>Genesis 6:2

That the [ben](../../strongs/h/h1121.md) of ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) the [bath](../../strongs/h/h1323.md) of ['adam](../../strongs/h/h120.md) that they were [towb](../../strongs/h/h2896.md); and they [laqach](../../strongs/h/h3947.md) them ['ishshah](../../strongs/h/h802.md) of all which they [bāḥar](../../strongs/h/h977.md).

<a name="genesis_6_3"></a>Genesis 6:3

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), My [ruwach](../../strongs/h/h7307.md) shall not ['owlam](../../strongs/h/h5769.md) [diyn](../../strongs/h/h1777.md) with ['adam](../../strongs/h/h120.md), for that he also is [basar](../../strongs/h/h1320.md) [šāḡaḡ](../../strongs/h/h7683.md): yet his [yowm](../../strongs/h/h3117.md) shall be an hundred and twenty [šānâ](../../strongs/h/h8141.md).

<a name="genesis_6_4"></a>Genesis 6:4

There were [nāp̄îl](../../strongs/h/h5303.md) in the ['erets](../../strongs/h/h776.md) in those [yowm](../../strongs/h/h3117.md); and also ['aḥar](../../strongs/h/h310.md) that, when the [ben](../../strongs/h/h1121.md) of ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) unto the [bath](../../strongs/h/h1323.md) of ['adam](../../strongs/h/h120.md), and they [yalad](../../strongs/h/h3205.md) children to them, the same became [gibôr](../../strongs/h/h1368.md) which were of ['owlam](../../strongs/h/h5769.md), ['enowsh](../../strongs/h/h582.md) of [shem](../../strongs/h/h8034.md).

<a name="genesis_6_5"></a>Genesis 6:5

And [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) that the [ra'](../../strongs/h/h7451.md) of ['adam](../../strongs/h/h120.md) was [rab](../../strongs/h/h7227.md) in the ['erets](../../strongs/h/h776.md), and that every [yetser](../../strongs/h/h3336.md) of the [maḥăšāḇâ](../../strongs/h/h4284.md) of his [leb](../../strongs/h/h3820.md) was only [ra'](../../strongs/h/h7451.md) [yowm](../../strongs/h/h3117.md).

<a name="genesis_6_6"></a>Genesis 6:6

And it [nacham](../../strongs/h/h5162.md) [Yĕhovah](../../strongs/h/h3068.md) that he had ['asah](../../strongs/h/h6213.md) ['adam](../../strongs/h/h120.md) on the ['erets](../../strongs/h/h776.md), and it [ʿāṣaḇ](../../strongs/h/h6087.md) him at his [leb](../../strongs/h/h3820.md).

<a name="genesis_6_7"></a>Genesis 6:7

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), I will [māḥâ](../../strongs/h/h4229.md) ['adam](../../strongs/h/h120.md) whom I have [bara'](../../strongs/h/h1254.md) from the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md); both ['adam](../../strongs/h/h120.md), and [bĕhemah](../../strongs/h/h929.md), and the [remeś](../../strongs/h/h7431.md), and the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md); for it [nacham](../../strongs/h/h5162.md) me that I have ['asah](../../strongs/h/h6213.md) them.

<a name="genesis_6_8"></a>Genesis 6:8

But [Nōaḥ](../../strongs/h/h5146.md) [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="genesis_6_9"></a>Genesis 6:9

These are the [towlĕdah](../../strongs/h/h8435.md) of [Nōaḥ](../../strongs/h/h5146.md): [Nōaḥ](../../strongs/h/h5146.md) was a [tsaddiyq](../../strongs/h/h6662.md) ['iysh](../../strongs/h/h376.md) and [tamiym](../../strongs/h/h8549.md) in his [dôr](../../strongs/h/h1755.md), and [Nōaḥ](../../strongs/h/h5146.md) [halak](../../strongs/h/h1980.md) with ['Elohiym](../../strongs/h/h430.md).

<a name="genesis_6_10"></a>Genesis 6:10

And [Nōaḥ](../../strongs/h/h5146.md) [yalad](../../strongs/h/h3205.md) three [ben](../../strongs/h/h1121.md), [Šēm](../../strongs/h/h8035.md), [Ḥām](../../strongs/h/h2526.md), and [Yep̄eṯ](../../strongs/h/h3315.md).

<a name="genesis_6_11"></a>Genesis 6:11

The ['erets](../../strongs/h/h776.md) also was [shachath](../../strongs/h/h7843.md) [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md), and the ['erets](../../strongs/h/h776.md) was [mālā'](../../strongs/h/h4390.md) with [chamac](../../strongs/h/h2555.md).

<a name="genesis_6_12"></a>Genesis 6:12

And ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) upon the ['erets](../../strongs/h/h776.md), and, behold, it was [shachath](../../strongs/h/h7843.md); for all [basar](../../strongs/h/h1320.md) had [shachath](../../strongs/h/h7843.md) his [derek](../../strongs/h/h1870.md) upon the ['erets](../../strongs/h/h776.md).

<a name="genesis_6_13"></a>Genesis 6:13

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto [Nōaḥ](../../strongs/h/h5146.md), The [qēṣ](../../strongs/h/h7093.md) of all [basar](../../strongs/h/h1320.md) is [bow'](../../strongs/h/h935.md) before [paniym](../../strongs/h/h6440.md); for the ['erets](../../strongs/h/h776.md) is [mālā'](../../strongs/h/h4390.md) with [chamac](../../strongs/h/h2555.md) through [paniym](../../strongs/h/h6440.md); and, behold, I will [shachath](../../strongs/h/h7843.md) them with the ['erets](../../strongs/h/h776.md).

<a name="genesis_6_14"></a>Genesis 6:14

Make thee a [tēḇâ](../../strongs/h/h8392.md) of [gōp̄er](../../strongs/h/h1613.md) ['ets](../../strongs/h/h6086.md); [qēn](../../strongs/h/h7064.md) shalt thou ['asah](../../strongs/h/h6213.md) in the [tēḇâ](../../strongs/h/h8392.md), and shalt [kāp̄ar](../../strongs/h/h3722.md) it [bayith](../../strongs/h/h1004.md) and [ḥûṣ](../../strongs/h/h2351.md) with [kōp̄er](../../strongs/h/h3724.md).

<a name="genesis_6_15"></a>Genesis 6:15

And this thou shalt ['asah](../../strongs/h/h6213.md) it of: The ['ōreḵ](../../strongs/h/h753.md) of the [tēḇâ](../../strongs/h/h8392.md) shall be three hundred ['ammâ](../../strongs/h/h520.md), the [rōḥaḇ](../../strongs/h/h7341.md) of it fifty ['ammâ](../../strongs/h/h520.md), and the [qômâ](../../strongs/h/h6967.md) of it thirty ['ammâ](../../strongs/h/h520.md).

<a name="genesis_6_16"></a>Genesis 6:16

A [ṣōhar](../../strongs/h/h6672.md) shalt thou ['asah](../../strongs/h/h6213.md) to the [tēḇâ](../../strongs/h/h8392.md), and in an ['ammâ](../../strongs/h/h520.md) shalt thou [kalah](../../strongs/h/h3615.md) it [maʿal](../../strongs/h/h4605.md); and the [peṯaḥ](../../strongs/h/h6607.md) of the [tēḇâ](../../strongs/h/h8392.md) shalt thou [śûm](../../strongs/h/h7760.md) in the [ṣaḏ](../../strongs/h/h6654.md) thereof; with [taḥtî](../../strongs/h/h8482.md), [šēnî](../../strongs/h/h8145.md), and [šᵊlîšî](../../strongs/h/h7992.md) stories shalt thou ['asah](../../strongs/h/h6213.md) it.

<a name="genesis_6_17"></a>Genesis 6:17

And, [hinneh](../../strongs/h/h2009.md), I, even I, do [bow'](../../strongs/h/h935.md) a [mabûl](../../strongs/h/h3999.md) of [mayim](../../strongs/h/h4325.md) upon the ['erets](../../strongs/h/h776.md), to [shachath](../../strongs/h/h7843.md) all [basar](../../strongs/h/h1320.md), wherein is the [ruwach](../../strongs/h/h7307.md) of [chay](../../strongs/h/h2416.md), from under [shamayim](../../strongs/h/h8064.md); and [kōl](../../strongs/h/h3605.md) that is in the ['erets](../../strongs/h/h776.md) shall [gāvaʿ](../../strongs/h/h1478.md).

<a name="genesis_6_18"></a>Genesis 6:18

But with thee will I [quwm](../../strongs/h/h6965.md) my [bĕriyth](../../strongs/h/h1285.md); and thou shalt [bow'](../../strongs/h/h935.md) into the [tēḇâ](../../strongs/h/h8392.md), thou, and thy [ben](../../strongs/h/h1121.md), and thy ['ishshah](../../strongs/h/h802.md), and thy [ben](../../strongs/h/h1121.md) ['ishshah](../../strongs/h/h802.md) with thee.

<a name="genesis_6_19"></a>Genesis 6:19

And of every [chay](../../strongs/h/h2416.md) of all [basar](../../strongs/h/h1320.md), [šᵊnayim](../../strongs/h/h8147.md) of every sort shalt thou [bow'](../../strongs/h/h935.md) into the [tēḇâ](../../strongs/h/h8392.md), to keep them [ḥāyâ](../../strongs/h/h2421.md) with thee; they shall be [zāḵār](../../strongs/h/h2145.md) and [nᵊqēḇâ](../../strongs/h/h5347.md).

<a name="genesis_6_20"></a>Genesis 6:20

Of [ʿôp̄](../../strongs/h/h5775.md) after their [miyn](../../strongs/h/h4327.md), and of [bĕhemah](../../strongs/h/h929.md) after their [miyn](../../strongs/h/h4327.md), of every [remeś](../../strongs/h/h7431.md) of the ['ăḏāmâ](../../strongs/h/h127.md) after his [miyn](../../strongs/h/h4327.md), [šᵊnayim](../../strongs/h/h8147.md) of every sort shall [bow'](../../strongs/h/h935.md) unto thee, to keep them [ḥāyâ](../../strongs/h/h2421.md).

<a name="genesis_6_21"></a>Genesis 6:21

And [laqach](../../strongs/h/h3947.md) thou unto thee of all [ma'akal](../../strongs/h/h3978.md) that is ['akal](../../strongs/h/h398.md), and thou shalt ['āsap̄](../../strongs/h/h622.md) it to thee; and it shall be for ['oklah](../../strongs/h/h402.md) for thee, and for them.

<a name="genesis_6_22"></a>Genesis 6:22

Thus ['asah](../../strongs/h/h6213.md) [Nōaḥ](../../strongs/h/h5146.md); according to all that ['Elohiym](../../strongs/h/h430.md) [tsavah](../../strongs/h/h6680.md) him, so ['asah](../../strongs/h/h6213.md) he.

---

[Transliteral Bible](../bible.md)

[Genesis](genesis.md)

[Genesis 5](genesis_5.md) - [Genesis 7](genesis_7.md)