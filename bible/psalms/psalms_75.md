# [Psalms 75](https://www.blueletterbible.org/kjv/psa/75)

<a name="psalms_75_1"></a>Psalms 75:1

To the chief [nāṣaḥ](../../strongs/h/h5329.md), ['al-tašḥēṯ](../../strongs/h/h516.md), A [mizmôr](../../strongs/h/h4210.md) or [šîr](../../strongs/h/h7892.md) of ['Āsāp̄](../../strongs/h/h623.md). Unto thee, ['Elohiym](../../strongs/h/h430.md), do we [yadah](../../strongs/h/h3034.md), unto thee do we [yadah](../../strongs/h/h3034.md): for that thy [shem](../../strongs/h/h8034.md) is [qarowb](../../strongs/h/h7138.md) thy [pala'](../../strongs/h/h6381.md) [sāp̄ar](../../strongs/h/h5608.md).

<a name="psalms_75_2"></a>Psalms 75:2

When I shall [laqach](../../strongs/h/h3947.md) the [môʿēḏ](../../strongs/h/h4150.md) I will [shaphat](../../strongs/h/h8199.md) [meyshar](../../strongs/h/h4339.md).

<a name="psalms_75_3"></a>Psalms 75:3

The ['erets](../../strongs/h/h776.md) and all the [yashab](../../strongs/h/h3427.md) thereof are [mûḡ](../../strongs/h/h4127.md): I [tāḵan](../../strongs/h/h8505.md) the [ʿammûḏ](../../strongs/h/h5982.md) of it. [Celah](../../strongs/h/h5542.md).

<a name="psalms_75_4"></a>Psalms 75:4

I ['āmar](../../strongs/h/h559.md) unto the [halal](../../strongs/h/h1984.md), [halal](../../strongs/h/h1984.md) not: and to the [rasha'](../../strongs/h/h7563.md), [ruwm](../../strongs/h/h7311.md) not the [qeren](../../strongs/h/h7161.md):

<a name="psalms_75_5"></a>Psalms 75:5

[ruwm](../../strongs/h/h7311.md) not your [qeren](../../strongs/h/h7161.md) on [marowm](../../strongs/h/h4791.md): [dabar](../../strongs/h/h1696.md) not with a [ʿāṯāq](../../strongs/h/h6277.md) [ṣaûā'r](../../strongs/h/h6677.md).

<a name="psalms_75_6"></a>Psalms 75:6

For [ruwm](../../strongs/h/h7311.md) [har](../../strongs/h/h2022.md) cometh neither from the [môṣā'](../../strongs/h/h4161.md), nor from the [ma'arab](../../strongs/h/h4628.md), nor from the [midbar](../../strongs/h/h4057.md).

<a name="psalms_75_7"></a>Psalms 75:7

But ['Elohiym](../../strongs/h/h430.md) is the [shaphat](../../strongs/h/h8199.md): he [šāp̄ēl](../../strongs/h/h8213.md) one, and [ruwm](../../strongs/h/h7311.md) another.

<a name="psalms_75_8"></a>Psalms 75:8

For in the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) there is a [kowc](../../strongs/h/h3563.md), and the [yayin](../../strongs/h/h3196.md) is [ḥāmar](../../strongs/h/h2560.md); it is [mālē'](../../strongs/h/h4392.md) of [meseḵ](../../strongs/h/h4538.md); and he [nāḡar](../../strongs/h/h5064.md) of the [zê](../../strongs/h/h2088.md): but the [šemer](../../strongs/h/h8105.md) thereof, all the [rasha'](../../strongs/h/h7563.md) of the ['erets](../../strongs/h/h776.md) shall [māṣâ](../../strongs/h/h4680.md) them, and [šāṯâ](../../strongs/h/h8354.md) them.

<a name="psalms_75_9"></a>Psalms 75:9

But I will [nāḡaḏ](../../strongs/h/h5046.md) for ['owlam](../../strongs/h/h5769.md); I will sing [zamar](../../strongs/h/h2167.md) to the ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md).

<a name="psalms_75_10"></a>Psalms 75:10

All the [qeren](../../strongs/h/h7161.md) of the [rasha'](../../strongs/h/h7563.md) also will I [gāḏaʿ](../../strongs/h/h1438.md); but the [qeren](../../strongs/h/h7161.md) of the [tsaddiyq](../../strongs/h/h6662.md) shall be [ruwm](../../strongs/h/h7311.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 74](psalms_74.md) - [Psalms 76](psalms_76.md)