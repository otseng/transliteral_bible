# [Psalms 111](https://www.blueletterbible.org/kjv/psalms/111)

<a name="psalms_111_1"></a>Psalms 111:1

[halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md). I will [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md) with my whole [lebab](../../strongs/h/h3824.md), in the [sôḏ](../../strongs/h/h5475.md) of the [yashar](../../strongs/h/h3477.md), and in the ['edah](../../strongs/h/h5712.md).

<a name="psalms_111_2"></a>Psalms 111:2

The [ma'aseh](../../strongs/h/h4639.md) of [Yĕhovah](../../strongs/h/h3068.md) are [gadowl](../../strongs/h/h1419.md), [darash](../../strongs/h/h1875.md) of all them that have [chephets](../../strongs/h/h2656.md) therein.

<a name="psalms_111_3"></a>Psalms 111:3

His [pōʿal](../../strongs/h/h6467.md) is [howd](../../strongs/h/h1935.md) and [hadar](../../strongs/h/h1926.md): and his [tsedaqah](../../strongs/h/h6666.md) ['amad](../../strongs/h/h5975.md) for [ʿaḏ](../../strongs/h/h5703.md).

<a name="psalms_111_4"></a>Psalms 111:4

He hath ['asah](../../strongs/h/h6213.md) his [pala'](../../strongs/h/h6381.md) to be [zeker](../../strongs/h/h2143.md): [Yĕhovah](../../strongs/h/h3068.md) is [ḥanwn](../../strongs/h/h2587.md) and full of [raḥwm](../../strongs/h/h7349.md).

<a name="psalms_111_5"></a>Psalms 111:5

He hath [nathan](../../strongs/h/h5414.md) [ṭerep̄](../../strongs/h/h2964.md) unto them that [yārē'](../../strongs/h/h3373.md) him: he will ['owlam](../../strongs/h/h5769.md) be [zakar](../../strongs/h/h2142.md) of his [bĕriyth](../../strongs/h/h1285.md).

<a name="psalms_111_6"></a>Psalms 111:6

He hath [nāḡaḏ](../../strongs/h/h5046.md) his ['am](../../strongs/h/h5971.md) the [koach](../../strongs/h/h3581.md) of his [ma'aseh](../../strongs/h/h4639.md), that he may [nathan](../../strongs/h/h5414.md) them the [nachalah](../../strongs/h/h5159.md) of the [gowy](../../strongs/h/h1471.md).

<a name="psalms_111_7"></a>Psalms 111:7

The [ma'aseh](../../strongs/h/h4639.md) of his [yad](../../strongs/h/h3027.md) are ['emeth](../../strongs/h/h571.md) and [mishpat](../../strongs/h/h4941.md); all his [piqquwd](../../strongs/h/h6490.md) are ['aman](../../strongs/h/h539.md).

<a name="psalms_111_8"></a>Psalms 111:8

They [camak](../../strongs/h/h5564.md) for [ʿaḏ](../../strongs/h/h5703.md) and ['owlam](../../strongs/h/h5769.md), and are ['asah](../../strongs/h/h6213.md) in ['emeth](../../strongs/h/h571.md) and [yashar](../../strongs/h/h3477.md).

<a name="psalms_111_9"></a>Psalms 111:9

He [shalach](../../strongs/h/h7971.md) [pᵊḏûṯ](../../strongs/h/h6304.md) unto his ['am](../../strongs/h/h5971.md): he hath [tsavah](../../strongs/h/h6680.md) his [bĕriyth](../../strongs/h/h1285.md) ['owlam](../../strongs/h/h5769.md): [qadowsh](../../strongs/h/h6918.md) and [yare'](../../strongs/h/h3372.md) is his [shem](../../strongs/h/h8034.md).

<a name="psalms_111_10"></a>Psalms 111:10

The [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) is the [re'shiyth](../../strongs/h/h7225.md) of [ḥāḵmâ](../../strongs/h/h2451.md): a [towb](../../strongs/h/h2896.md) [śēḵel](../../strongs/h/h7922.md) have all they that ['asah](../../strongs/h/h6213.md) his commandments: his [tehillah](../../strongs/h/h8416.md) ['amad](../../strongs/h/h5975.md) for [ʿaḏ](../../strongs/h/h5703.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 110](psalms_110.md) - [Psalms 112](psalms_112.md)