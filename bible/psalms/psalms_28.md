# [Psalms 28](https://www.blueletterbible.org/kjv/psa/28/1/s_506001)

<a name="psalms_28_1"></a>Psalms 28:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). Unto thee will I [qara'](../../strongs/h/h7121.md), [Yĕhovah](../../strongs/h/h3068.md) my [tsuwr](../../strongs/h/h6697.md); be not [ḥāšâ](../../strongs/h/h2814.md) to me: lest, if thou be [ḥāraš](../../strongs/h/h2790.md) to me, I become like them that [yarad](../../strongs/h/h3381.md) into the [bowr](../../strongs/h/h953.md).

<a name="psalms_28_2"></a>Psalms 28:2

[shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of my [taḥănûn](../../strongs/h/h8469.md), when I [šāvaʿ](../../strongs/h/h7768.md) unto thee, when I [nasa'](../../strongs/h/h5375.md) my [yad](../../strongs/h/h3027.md) toward thy [qodesh](../../strongs/h/h6944.md) [dᵊḇîr](../../strongs/h/h1687.md).

<a name="psalms_28_3"></a>Psalms 28:3

[mashak](../../strongs/h/h4900.md) me not away with the [rasha'](../../strongs/h/h7563.md), and with the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md), which [dabar](../../strongs/h/h1696.md) [shalowm](../../strongs/h/h7965.md) to their [rea'](../../strongs/h/h7453.md), but [ra'](../../strongs/h/h7451.md) is in their [lebab](../../strongs/h/h3824.md).

<a name="psalms_28_4"></a>Psalms 28:4

[nathan](../../strongs/h/h5414.md) them according to their [pōʿal](../../strongs/h/h6467.md), and according to the [rōaʿ](../../strongs/h/h7455.md) of their [maʿălāl](../../strongs/h/h4611.md): [nathan](../../strongs/h/h5414.md) them after the [ma'aseh](../../strongs/h/h4639.md) of their [yad](../../strongs/h/h3027.md); [shuwb](../../strongs/h/h7725.md) to them their [gĕmwl](../../strongs/h/h1576.md).

<a name="psalms_28_5"></a>Psalms 28:5

Because they [bîn](../../strongs/h/h995.md) not the [pe'ullah](../../strongs/h/h6468.md) of [Yĕhovah](../../strongs/h/h3068.md), nor the [ma'aseh](../../strongs/h/h4639.md) of his [yad](../../strongs/h/h3027.md), he shall [harac](../../strongs/h/h2040.md) them, and not [bānâ](../../strongs/h/h1129.md) them up.

<a name="psalms_28_6"></a>Psalms 28:6

[barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md), because he hath [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of my [taḥănûn](../../strongs/h/h8469.md).

<a name="psalms_28_7"></a>Psalms 28:7

[Yĕhovah](../../strongs/h/h3068.md) is my ['oz](../../strongs/h/h5797.md) and my [magen](../../strongs/h/h4043.md); my [leb](../../strongs/h/h3820.md) [batach](../../strongs/h/h982.md) in him, and I am [ʿāzar](../../strongs/h/h5826.md): therefore my [leb](../../strongs/h/h3820.md) greatly [ʿālaz](../../strongs/h/h5937.md); and with my [šîr](../../strongs/h/h7892.md) will I [yadah](../../strongs/h/h3034.md) him.

<a name="psalms_28_8"></a>Psalms 28:8

[Yĕhovah](../../strongs/h/h3068.md) is their ['oz](../../strongs/h/h5797.md), and he is the [yĕshuw'ah](../../strongs/h/h3444.md) [māʿôz](../../strongs/h/h4581.md) of his [mashiyach](../../strongs/h/h4899.md).

<a name="psalms_28_9"></a>Psalms 28:9

[yasha'](../../strongs/h/h3467.md) thy ['am](../../strongs/h/h5971.md), and [barak](../../strongs/h/h1288.md) thine [nachalah](../../strongs/h/h5159.md): [ra'ah](../../strongs/h/h7462.md) them also, and [nasa'](../../strongs/h/h5375.md) them up ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 27](psalms_27.md) - [Psalms 29](psalms_29.md)