# [Psalms 53](https://www.blueletterbible.org/kjv/psa/53)

<a name="psalms_53_1"></a>Psalms 53:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon [Maḥălaṯ](../../strongs/h/h4257.md), [Maśkîl](../../strongs/h/h4905.md), [A Psalm] of [Dāviḏ](../../strongs/h/h1732.md). The [nabal](../../strongs/h/h5036.md) hath ['āmar](../../strongs/h/h559.md) in his [leb](../../strongs/h/h3820.md), There is no ['Elohiym](../../strongs/h/h430.md). [shachath](../../strongs/h/h7843.md) are they, and have [ta'ab](../../strongs/h/h8581.md) ['evel](../../strongs/h/h5766.md): there is none that ['asah](../../strongs/h/h6213.md) [towb](../../strongs/h/h2896.md).

<a name="psalms_53_2"></a>Psalms 53:2

['Elohiym](../../strongs/h/h430.md) [šāqap̄](../../strongs/h/h8259.md) from [shamayim](../../strongs/h/h8064.md) upon the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md), to [ra'ah](../../strongs/h/h7200.md) if there were any that did [sakal](../../strongs/h/h7919.md), that did [darash](../../strongs/h/h1875.md) ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_53_3"></a>Psalms 53:3

Every one of them is [sûḡ](../../strongs/h/h5472.md): they are [yaḥaḏ](../../strongs/h/h3162.md) become ['alach](../../strongs/h/h444.md); there is none that ['asah](../../strongs/h/h6213.md) [towb](../../strongs/h/h2896.md), no, not one.

<a name="psalms_53_4"></a>Psalms 53:4

Have the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md) no [yada'](../../strongs/h/h3045.md)? who ['akal](../../strongs/h/h398.md) my ['am](../../strongs/h/h5971.md) as they ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md): they have not [qara'](../../strongs/h/h7121.md) upon ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_53_5"></a>Psalms 53:5

There were they in [paḥaḏ](../../strongs/h/h6343.md) [pachad](../../strongs/h/h6342.md), where no [paḥaḏ](../../strongs/h/h6343.md) was: for ['Elohiym](../../strongs/h/h430.md) hath [p̄zr](../../strongs/h/h6340.md) the ['etsem](../../strongs/h/h6106.md) of him that [ḥānâ](../../strongs/h/h2583.md) against thee: thou hast [buwsh](../../strongs/h/h954.md) them, because ['Elohiym](../../strongs/h/h430.md) hath [mā'as](../../strongs/h/h3988.md) them.

<a name="psalms_53_6"></a>Psalms 53:6

[nathan](../../strongs/h/h5414.md) the [yĕshuw'ah](../../strongs/h/h3444.md) of [Yisra'el](../../strongs/h/h3478.md) were come out of [Tsiyown](../../strongs/h/h6726.md)! When ['Elohiym](../../strongs/h/h430.md) [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) of his ['am](../../strongs/h/h5971.md), [Ya'aqob](../../strongs/h/h3290.md) shall [giyl](../../strongs/h/h1523.md), and [Yisra'el](../../strongs/h/h3478.md) shall be [samach](../../strongs/h/h8055.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 52](psalms_52.md) - [Psalms 54](psalms_54.md)