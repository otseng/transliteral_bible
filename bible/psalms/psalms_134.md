# [Psalms 134](https://www.blueletterbible.org/kjv/psalms/134)

<a name="psalms_134_1"></a>Psalms 134:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md). Behold, [barak](../../strongs/h/h1288.md) ye [Yĕhovah](../../strongs/h/h3068.md), all ye ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md), which by [layil](../../strongs/h/h3915.md) ['amad](../../strongs/h/h5975.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_134_2"></a>Psalms 134:2

[nasa'](../../strongs/h/h5375.md) your [yad](../../strongs/h/h3027.md) in the [qodesh](../../strongs/h/h6944.md), and [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_134_3"></a>Psalms 134:3

[Yĕhovah](../../strongs/h/h3068.md) that ['asah](../../strongs/h/h6213.md) [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md) [barak](../../strongs/h/h1288.md) thee out of [Tsiyown](../../strongs/h/h6726.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 133](psalms_133.md) - [Psalms 135](psalms_135.md)