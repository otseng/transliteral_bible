# [Psalms 150](https://www.blueletterbible.org/kjv/psalms/150)

<a name="psalms_150_1"></a>Psalms 150:1

[halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md). [halal](../../strongs/h/h1984.md) ['el](../../strongs/h/h410.md) in his [qodesh](../../strongs/h/h6944.md): [halal](../../strongs/h/h1984.md) him in the [raqiya'](../../strongs/h/h7549.md) of his ['oz](../../strongs/h/h5797.md).

<a name="psalms_150_2"></a>Psalms 150:2

[halal](../../strongs/h/h1984.md) him for his [gᵊḇûrâ](../../strongs/h/h1369.md): [halal](../../strongs/h/h1984.md) him according to his [rōḇ](../../strongs/h/h7230.md) [gōḏel](../../strongs/h/h1433.md).

<a name="psalms_150_3"></a>Psalms 150:3

[halal](../../strongs/h/h1984.md) him with the [tēqaʿ](../../strongs/h/h8629.md) of the [šôp̄ār](../../strongs/h/h7782.md): [halal](../../strongs/h/h1984.md) him with the [neḇel](../../strongs/h/h5035.md) and [kinnôr](../../strongs/h/h3658.md).

<a name="psalms_150_4"></a>Psalms 150:4

[halal](../../strongs/h/h1984.md) him with the [tōp̄](../../strongs/h/h8596.md) and [māḥôl](../../strongs/h/h4234.md): [halal](../../strongs/h/h1984.md) him with [mēn](../../strongs/h/h4482.md) and [ʿûḡāḇ](../../strongs/h/h5748.md).

<a name="psalms_150_5"></a>Psalms 150:5

[halal](../../strongs/h/h1984.md) him upon the [šēmaʿ](../../strongs/h/h8088.md) [ṣᵊlāṣal](../../strongs/h/h6767.md): [halal](../../strongs/h/h1984.md) him upon the [tᵊrûʿâ](../../strongs/h/h8643.md) [ṣᵊlāṣal](../../strongs/h/h6767.md).

<a name="psalms_150_6"></a>Psalms 150:6

Let every thing that hath [neshamah](../../strongs/h/h5397.md) [halal](../../strongs/h/h1984.md) the [Yahh](../../strongs/h/h3050.md). [halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 149](psalms_149.md)