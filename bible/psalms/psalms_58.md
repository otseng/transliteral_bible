# [Psalms 58](https://www.blueletterbible.org/kjv/psa/58)

<a name="psalms_58_1"></a>Psalms 58:1

To the [nāṣaḥ](../../strongs/h/h5329.md), ['al-tašḥēṯ](../../strongs/h/h516.md), [Miḵtām](../../strongs/h/h4387.md) of [Dāviḏ](../../strongs/h/h1732.md). Do ye ['umnām](../../strongs/h/h552.md) [dabar](../../strongs/h/h1696.md) [tsedeq](../../strongs/h/h6664.md), O ['ēlem](../../strongs/h/h482.md)? do ye [shaphat](../../strongs/h/h8199.md) [meyshar](../../strongs/h/h4339.md), O ye [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md)?

<a name="psalms_58_2"></a>Psalms 58:2

Yea, in [leb](../../strongs/h/h3820.md) ye [pa'al](../../strongs/h/h6466.md) ['evel](../../strongs/h/h5766.md); ye [pālas](../../strongs/h/h6424.md) the [chamac](../../strongs/h/h2555.md) of your [yad](../../strongs/h/h3027.md) in the ['erets](../../strongs/h/h776.md).

<a name="psalms_58_3"></a>Psalms 58:3

The [rasha'](../../strongs/h/h7563.md) are [zûr](../../strongs/h/h2114.md) from the [reḥem](../../strongs/h/h7358.md): they [tāʿâ](../../strongs/h/h8582.md) as soon as they be [beten](../../strongs/h/h990.md), [dabar](../../strongs/h/h1696.md) [kazab](../../strongs/h/h3577.md).

<a name="psalms_58_4"></a>Psalms 58:4

Their [chemah](../../strongs/h/h2534.md) is [dĕmuwth](../../strongs/h/h1823.md) the [chemah](../../strongs/h/h2534.md) of a [nachash](../../strongs/h/h5175.md): they are like the [ḥērēš](../../strongs/h/h2795.md) [peṯen](../../strongs/h/h6620.md) that ['āṭam](../../strongs/h/h331.md) her ['ozen](../../strongs/h/h241.md);

<a name="psalms_58_5"></a>Psalms 58:5

Which will not [shama'](../../strongs/h/h8085.md) to the [qowl](../../strongs/h/h6963.md) of [lāḥaš](../../strongs/h/h3907.md), [ḥāḇar](../../strongs/h/h2266.md) [Ḥeḇer](../../strongs/h/h2267.md) never so [ḥāḵam](../../strongs/h/h2449.md).

<a name="psalms_58_6"></a>Psalms 58:6

[harac](../../strongs/h/h2040.md) their [šēn](../../strongs/h/h8127.md), ['Elohiym](../../strongs/h/h430.md), in their [peh](../../strongs/h/h6310.md): [nāṯaṣ](../../strongs/h/h5422.md) the [maltāʿôṯ](../../strongs/h/h4459.md) of the [kephiyr](../../strongs/h/h3715.md), [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_58_7"></a>Psalms 58:7

Let them [mā'as](../../strongs/h/h3988.md) as [mayim](../../strongs/h/h4325.md) which [halak](../../strongs/h/h1980.md): when he [dāraḵ](../../strongs/h/h1869.md) his bow to shoot his [chets](../../strongs/h/h2671.md), let them be as [muwl](../../strongs/h/h4135.md).

<a name="psalms_58_8"></a>Psalms 58:8

As a [šaḇlûl](../../strongs/h/h7642.md) which [temes](../../strongs/h/h8557.md), let every one of them [halak](../../strongs/h/h1980.md): like the [nep̄el](../../strongs/h/h5309.md) of an ['ishshah](../../strongs/h/h802.md), that they may not [chazah](../../strongs/h/h2372.md) the [šemeš](../../strongs/h/h8121.md).

<a name="psalms_58_9"></a>Psalms 58:9

Before your [sîr](../../strongs/h/h5518.md) can [bîn](../../strongs/h/h995.md) the ['āṭāḏ](../../strongs/h/h329.md), he shall take them away as with a [śāʿar](../../strongs/h/h8175.md), both [chay](../../strongs/h/h2416.md), and in his [charown](../../strongs/h/h2740.md).

<a name="psalms_58_10"></a>Psalms 58:10

The [tsaddiyq](../../strongs/h/h6662.md) shall [samach](../../strongs/h/h8055.md) when he [chazah](../../strongs/h/h2372.md) the [nāqām](../../strongs/h/h5359.md): he shall [rāḥaṣ](../../strongs/h/h7364.md) his [pa'am](../../strongs/h/h6471.md) in the [dam](../../strongs/h/h1818.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="psalms_58_11"></a>Psalms 58:11

So that an ['adam](../../strongs/h/h120.md) shall ['āmar](../../strongs/h/h559.md), Verily there is a [pĕriy](../../strongs/h/h6529.md) for the [tsaddiyq](../../strongs/h/h6662.md): verily he is an ['Elohiym](../../strongs/h/h430.md) that [shaphat](../../strongs/h/h8199.md) in the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 57](psalms_57.md) - [Psalms 59](psalms_59.md)