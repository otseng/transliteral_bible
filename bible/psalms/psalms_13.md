# [Psalms 13](https://www.blueletterbible.org/kjv/psa/13/1/s_491001)

<a name="psalms_13_1"></a>Psalms 13:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). How long wilt thou [shakach](../../strongs/h/h7911.md) me, [Yĕhovah](../../strongs/h/h3068.md)? for ever? how long wilt thou [cathar](../../strongs/h/h5641.md) thy [paniym](../../strongs/h/h6440.md) from me?

<a name="psalms_13_2"></a>Psalms 13:2

How long shall I [shiyth](../../strongs/h/h7896.md) ['etsah](../../strongs/h/h6098.md) in my [nephesh](../../strongs/h/h5315.md), having [yagown](../../strongs/h/h3015.md) in my [lebab](../../strongs/h/h3824.md) [yômām](../../strongs/h/h3119.md)? how long shall mine ['oyeb](../../strongs/h/h341.md) be [ruwm](../../strongs/h/h7311.md) over me?

<a name="psalms_13_3"></a>Psalms 13:3

[nabat](../../strongs/h/h5027.md) and ['anah](../../strongs/h/h6030.md) me, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md): lighten mine ['ayin](../../strongs/h/h5869.md), lest I [yashen](../../strongs/h/h3462.md) [maveth](../../strongs/h/h4194.md);

<a name="psalms_13_4"></a>Psalms 13:4

Lest mine ['oyeb](../../strongs/h/h341.md) say, I have [yakol](../../strongs/h/h3201.md) against him; and those that [tsar](../../strongs/h/h6862.md) me [giyl](../../strongs/h/h1523.md) when I am [mowt](../../strongs/h/h4131.md).

<a name="psalms_13_5"></a>Psalms 13:5

But I have [batach](../../strongs/h/h982.md) in thy [checed](../../strongs/h/h2617.md); my [leb](../../strongs/h/h3820.md) shall [giyl](../../strongs/h/h1523.md) in thy [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="psalms_13_6"></a>Psalms 13:6

I will [shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md), because he hath [gamal](../../strongs/h/h1580.md) me.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 12](psalms_12.md) - [Psalms 14](psalms_14.md)