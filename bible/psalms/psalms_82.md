# [Psalms 82](https://www.blueletterbible.org/kjv/psalms/82)

<a name="psalms_82_1"></a>Psalms 82:1

A [mizmôr](../../strongs/h/h4210.md) of ['Āsāp̄](../../strongs/h/h623.md). ['Elohiym](../../strongs/h/h430.md) [nāṣaḇ](../../strongs/h/h5324.md) in the ['edah](../../strongs/h/h5712.md) of the ['el](../../strongs/h/h410.md); he [shaphat](../../strongs/h/h8199.md) [qereḇ](../../strongs/h/h7130.md) the ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_82_2"></a>Psalms 82:2

How long will ye [shaphat](../../strongs/h/h8199.md) ['evel](../../strongs/h/h5766.md), and [nasa'](../../strongs/h/h5375.md) the [paniym](../../strongs/h/h6440.md) of the [rasha'](../../strongs/h/h7563.md) ? [Celah](../../strongs/h/h5542.md).

<a name="psalms_82_3"></a>Psalms 82:3

[shaphat](../../strongs/h/h8199.md) the [dal](../../strongs/h/h1800.md) and [yathowm](../../strongs/h/h3490.md): do [ṣāḏaq](../../strongs/h/h6663.md) to the ['aniy](../../strongs/h/h6041.md) and [rûš](../../strongs/h/h7326.md).

<a name="psalms_82_4"></a>Psalms 82:4

[palat](../../strongs/h/h6403.md) the [dal](../../strongs/h/h1800.md) and ['ebyown](../../strongs/h/h34.md): [natsal](../../strongs/h/h5337.md) them out of the [yad](../../strongs/h/h3027.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="psalms_82_5"></a>Psalms 82:5

They [yada'](../../strongs/h/h3045.md) not, neither will they [bîn](../../strongs/h/h995.md); they [halak](../../strongs/h/h1980.md) on in [ḥăšēḵâ](../../strongs/h/h2825.md): all the [môsāḏ](../../strongs/h/h4144.md) of the ['erets](../../strongs/h/h776.md) are out of [mowt](../../strongs/h/h4131.md).

<a name="psalms_82_6"></a>Psalms 82:6

I have ['āmar](../../strongs/h/h559.md), Ye are ['Elohiym](../../strongs/h/h430.md); and all of you are [ben](../../strongs/h/h1121.md) of the most ['elyown](../../strongs/h/h5945.md).

<a name="psalms_82_7"></a>Psalms 82:7

['āḵēn](../../strongs/h/h403.md) ye shall [muwth](../../strongs/h/h4191.md) like ['āḏām](../../strongs/h/h120.md), and [naphal](../../strongs/h/h5307.md) like ['echad](../../strongs/h/h259.md) of the [śar](../../strongs/h/h8269.md).

<a name="psalms_82_8"></a>Psalms 82:8

[quwm](../../strongs/h/h6965.md), ['Elohiym](../../strongs/h/h430.md), [shaphat](../../strongs/h/h8199.md) the ['erets](../../strongs/h/h776.md): for thou shalt [nāḥal](../../strongs/h/h5157.md) all [gowy](../../strongs/h/h1471.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 81](psalms_81.md) - [Psalms 83](psalms_83.md)