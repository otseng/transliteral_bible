# [Psalms 29](https://www.blueletterbible.org/kjv/psa/29/1/s_507001)

<a name="psalms_29_1"></a>Psalms 29:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [yāhaḇ](../../strongs/h/h3051.md) unto [Yĕhovah](../../strongs/h/h3068.md), [ben](../../strongs/h/h1121.md) ['el](../../strongs/h/h410.md), [yāhaḇ](../../strongs/h/h3051.md) unto [Yĕhovah](../../strongs/h/h3068.md) [kabowd](../../strongs/h/h3519.md) and ['oz](../../strongs/h/h5797.md).

<a name="psalms_29_2"></a>Psalms 29:2

[yāhaḇ](../../strongs/h/h3051.md) unto [Yĕhovah](../../strongs/h/h3068.md) the [kabowd](../../strongs/h/h3519.md) due unto his [shem](../../strongs/h/h8034.md); [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md) in the [hăḏārâ](../../strongs/h/h1927.md) of [qodesh](../../strongs/h/h6944.md).

<a name="psalms_29_3"></a>Psalms 29:3

The [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) is upon the [mayim](../../strongs/h/h4325.md): the ['el](../../strongs/h/h410.md) of [kabowd](../../strongs/h/h3519.md) [ra'am](../../strongs/h/h7481.md): [Yĕhovah](../../strongs/h/h3068.md) is upon [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md).

<a name="psalms_29_4"></a>Psalms 29:4

The [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) is [koach](../../strongs/h/h3581.md); the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) is [hadar](../../strongs/h/h1926.md).

<a name="psalms_29_5"></a>Psalms 29:5

The [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) [shabar](../../strongs/h/h7665.md) the ['erez](../../strongs/h/h730.md); yea, [Yĕhovah](../../strongs/h/h3068.md) [shabar](../../strongs/h/h7665.md) the ['erez](../../strongs/h/h730.md) of [Lᵊḇānôn](../../strongs/h/h3844.md).

<a name="psalms_29_6"></a>Psalms 29:6

He maketh them also to [rāqaḏ](../../strongs/h/h7540.md) like a [ʿēḡel](../../strongs/h/h5695.md); [Lᵊḇānôn](../../strongs/h/h3844.md) and [širyôn](../../strongs/h/h8303.md) like a [ben](../../strongs/h/h1121.md) [rᵊ'ēm](../../strongs/h/h7214.md).

<a name="psalms_29_7"></a>Psalms 29:7

The [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) [ḥāṣaḇ](../../strongs/h/h2672.md) the [lehāḇâ](../../strongs/h/h3852.md) of ['esh](../../strongs/h/h784.md).

<a name="psalms_29_8"></a>Psalms 29:8

The [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) [chuwl](../../strongs/h/h2342.md) the [midbar](../../strongs/h/h4057.md); [Yĕhovah](../../strongs/h/h3068.md) [chuwl](../../strongs/h/h2342.md) the [midbar](../../strongs/h/h4057.md) of [Qāḏēš](../../strongs/h/h6946.md).

<a name="psalms_29_9"></a>Psalms 29:9

The [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) maketh the ['ayyālâ](../../strongs/h/h355.md) to [chuwl](../../strongs/h/h2342.md), and [ḥāśap̄](../../strongs/h/h2834.md) the [yaʿărâ](../../strongs/h/h3295.md): and in his [heykal](../../strongs/h/h1964.md) doth every one ['āmar](../../strongs/h/h559.md) of his [kabowd](../../strongs/h/h3519.md).

<a name="psalms_29_10"></a>Psalms 29:10

[Yĕhovah](../../strongs/h/h3068.md) [yashab](../../strongs/h/h3427.md) upon the [mabûl](../../strongs/h/h3999.md); yea, [Yĕhovah](../../strongs/h/h3068.md) [yashab](../../strongs/h/h3427.md) [melek](../../strongs/h/h4428.md) ['owlam](../../strongs/h/h5769.md).

<a name="psalms_29_11"></a>Psalms 29:11

[Yĕhovah](../../strongs/h/h3068.md) will [nathan](../../strongs/h/h5414.md) ['oz](../../strongs/h/h5797.md) unto his ['am](../../strongs/h/h5971.md); [Yĕhovah](../../strongs/h/h3068.md) will [barak](../../strongs/h/h1288.md) his ['am](../../strongs/h/h5971.md) with [shalowm](../../strongs/h/h7965.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 28](psalms_28.md) - [Psalms 30](psalms_30.md)