# [Psalms 107](https://www.blueletterbible.org/kjv/psalms/107)

<a name="psalms_107_1"></a>Psalms 107:1

[yadah](../../strongs/h/h3034.md) unto [Yĕhovah](../../strongs/h/h3068.md), for he is [towb](../../strongs/h/h2896.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_107_2"></a>Psalms 107:2

Let the [gā'al](../../strongs/h/h1350.md) of [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) so, whom he hath [gā'al](../../strongs/h/h1350.md) from the [yad](../../strongs/h/h3027.md) of the [tsar](../../strongs/h/h6862.md);

<a name="psalms_107_3"></a>Psalms 107:3

And [qāḇaṣ](../../strongs/h/h6908.md) them out of the ['erets](../../strongs/h/h776.md), from the [mizrach](../../strongs/h/h4217.md), and from the [ma'arab](../../strongs/h/h4628.md), from the [ṣāp̄ôn](../../strongs/h/h6828.md), and from the [yam](../../strongs/h/h3220.md).

<a name="psalms_107_4"></a>Psalms 107:4

They [tāʿâ](../../strongs/h/h8582.md) in the [midbar](../../strongs/h/h4057.md) in a [yᵊšîmôn](../../strongs/h/h3452.md) [derek](../../strongs/h/h1870.md); they [māṣā'](../../strongs/h/h4672.md) no [ʿîr](../../strongs/h/h5892.md) to [môšāḇ](../../strongs/h/h4186.md) in.

<a name="psalms_107_5"></a>Psalms 107:5

[rāʿēḇ](../../strongs/h/h7457.md) and [ṣāmē'](../../strongs/h/h6771.md), their [nephesh](../../strongs/h/h5315.md) [ʿāṭap̄](../../strongs/h/h5848.md) in them.

<a name="psalms_107_6"></a>Psalms 107:6

Then they [ṣāʿaq](../../strongs/h/h6817.md) unto [Yĕhovah](../../strongs/h/h3068.md) in their [tsar](../../strongs/h/h6862.md), and he [natsal](../../strongs/h/h5337.md) them out of their [mᵊṣûqâ](../../strongs/h/h4691.md).

<a name="psalms_107_7"></a>Psalms 107:7

And he [dāraḵ](../../strongs/h/h1869.md) them by the [yashar](../../strongs/h/h3477.md) [derek](../../strongs/h/h1870.md), that they might [yālaḵ](../../strongs/h/h3212.md) to a [ʿîr](../../strongs/h/h5892.md) of [môšāḇ](../../strongs/h/h4186.md).

<a name="psalms_107_8"></a>Psalms 107:8

Oh that men would [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md) for his [checed](../../strongs/h/h2617.md), and for his [pala'](../../strongs/h/h6381.md) to the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md)!

<a name="psalms_107_9"></a>Psalms 107:9

For he [sāׂbaʿ](../../strongs/h/h7646.md) the [šāqaq](../../strongs/h/h8264.md) [nephesh](../../strongs/h/h5315.md), and [mālā'](../../strongs/h/h4390.md) the [rāʿēḇ](../../strongs/h/h7457.md) [nephesh](../../strongs/h/h5315.md) with [towb](../../strongs/h/h2896.md).

<a name="psalms_107_10"></a>Psalms 107:10

Such as [yashab](../../strongs/h/h3427.md) in [choshek](../../strongs/h/h2822.md) and in the [ṣalmāveṯ](../../strongs/h/h6757.md), being ['āsîr](../../strongs/h/h615.md) in ['oniy](../../strongs/h/h6040.md) and [barzel](../../strongs/h/h1270.md);

<a name="psalms_107_11"></a>Psalms 107:11

Because they [marah](../../strongs/h/h4784.md) against the ['emer](../../strongs/h/h561.md) of ['el](../../strongs/h/h410.md), and [na'ats](../../strongs/h/h5006.md) the ['etsah](../../strongs/h/h6098.md) of the ['elyown](../../strongs/h/h5945.md):

<a name="psalms_107_12"></a>Psalms 107:12

Therefore he [kānaʿ](../../strongs/h/h3665.md) their [leb](../../strongs/h/h3820.md) with ['amal](../../strongs/h/h5999.md); they [kashal](../../strongs/h/h3782.md), and there was none to [ʿāzar](../../strongs/h/h5826.md).

<a name="psalms_107_13"></a>Psalms 107:13

Then they [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md) in their [tsar](../../strongs/h/h6862.md), and he [yasha'](../../strongs/h/h3467.md) them out of their [mᵊṣûqâ](../../strongs/h/h4691.md).

<a name="psalms_107_14"></a>Psalms 107:14

He [yāṣā'](../../strongs/h/h3318.md) them of [choshek](../../strongs/h/h2822.md) and the [ṣalmāveṯ](../../strongs/h/h6757.md), and [nathaq](../../strongs/h/h5423.md) their [mowcer](../../strongs/h/h4147.md) in [nathaq](../../strongs/h/h5423.md).

<a name="psalms_107_15"></a>Psalms 107:15

Oh that men would [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md) for his [checed](../../strongs/h/h2617.md), and for his [pala'](../../strongs/h/h6381.md) to the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md)!

<a name="psalms_107_16"></a>Psalms 107:16

For he hath [shabar](../../strongs/h/h7665.md) the [deleṯ](../../strongs/h/h1817.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md), and [gāḏaʿ](../../strongs/h/h1438.md) the [bᵊrîaḥ](../../strongs/h/h1280.md) of [barzel](../../strongs/h/h1270.md) in [gāḏaʿ](../../strongs/h/h1438.md).

<a name="psalms_107_17"></a>Psalms 107:17

['ĕvîl](../../strongs/h/h191.md) [derek](../../strongs/h/h1870.md) of their [pesha'](../../strongs/h/h6588.md), and because of their ['avon](../../strongs/h/h5771.md), are [ʿānâ](../../strongs/h/h6031.md).

<a name="psalms_107_18"></a>Psalms 107:18

Their [nephesh](../../strongs/h/h5315.md) [ta'ab](../../strongs/h/h8581.md) all ['ōḵel](../../strongs/h/h400.md); and they [naga'](../../strongs/h/h5060.md) unto the [sha'ar](../../strongs/h/h8179.md) of [maveth](../../strongs/h/h4194.md).

<a name="psalms_107_19"></a>Psalms 107:19

Then they [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md) in their [tsar](../../strongs/h/h6862.md), and he [yasha'](../../strongs/h/h3467.md) them out of their [mᵊṣûqâ](../../strongs/h/h4691.md).

<a name="psalms_107_20"></a>Psalms 107:20

He [shalach](../../strongs/h/h7971.md) his [dabar](../../strongs/h/h1697.md), and [rapha'](../../strongs/h/h7495.md) them, and [mālaṭ](../../strongs/h/h4422.md) them from their [šᵊḥîṯ](../../strongs/h/h7825.md).

<a name="psalms_107_21"></a>Psalms 107:21

Oh that men would [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md) for his [checed](../../strongs/h/h2617.md), and for his [pala'](../../strongs/h/h6381.md) to the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md)!

<a name="psalms_107_22"></a>Psalms 107:22

And let them [zabach](../../strongs/h/h2076.md) the [zebach](../../strongs/h/h2077.md) of [tôḏâ](../../strongs/h/h8426.md), and [sāp̄ar](../../strongs/h/h5608.md) his [ma'aseh](../../strongs/h/h4639.md) with [rinnah](../../strongs/h/h7440.md).

<a name="psalms_107_23"></a>Psalms 107:23

They that [yarad](../../strongs/h/h3381.md) to the [yam](../../strongs/h/h3220.md) in ['ŏnîyâ](../../strongs/h/h591.md), that ['asah](../../strongs/h/h6213.md) [mĕla'kah](../../strongs/h/h4399.md) in [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md);

<a name="psalms_107_24"></a>Psalms 107:24

These [ra'ah](../../strongs/h/h7200.md) the [ma'aseh](../../strongs/h/h4639.md) of [Yĕhovah](../../strongs/h/h3068.md), and his [pala'](../../strongs/h/h6381.md) in the [mᵊṣôlâ](../../strongs/h/h4688.md).

<a name="psalms_107_25"></a>Psalms 107:25

For he ['āmar](../../strongs/h/h559.md), and ['amad](../../strongs/h/h5975.md) the [saʿar](../../strongs/h/h5591.md) [ruwach](../../strongs/h/h7307.md), which [ruwm](../../strongs/h/h7311.md) the [gal](../../strongs/h/h1530.md) thereof.

<a name="psalms_107_26"></a>Psalms 107:26

They [ʿālâ](../../strongs/h/h5927.md) to the [shamayim](../../strongs/h/h8064.md), they [yarad](../../strongs/h/h3381.md) again to the [tĕhowm](../../strongs/h/h8415.md): their [nephesh](../../strongs/h/h5315.md) is [mûḡ](../../strongs/h/h4127.md) because of [ra'](../../strongs/h/h7451.md).

<a name="psalms_107_27"></a>Psalms 107:27

They [ḥāḡaḡ](../../strongs/h/h2287.md), and [nûaʿ](../../strongs/h/h5128.md) like a [šikôr](../../strongs/h/h7910.md), and are at their [ḥāḵmâ](../../strongs/h/h2451.md) [bālaʿ](../../strongs/h/h1104.md).

<a name="psalms_107_28"></a>Psalms 107:28

Then they [ṣāʿaq](../../strongs/h/h6817.md) unto [Yĕhovah](../../strongs/h/h3068.md) in their [tsar](../../strongs/h/h6862.md), and he [yāṣā'](../../strongs/h/h3318.md) them of their [mᵊṣûqâ](../../strongs/h/h4691.md).

<a name="psalms_107_29"></a>Psalms 107:29

He [quwm](../../strongs/h/h6965.md) the [saʿar](../../strongs/h/h5591.md) a [dᵊmāmâ](../../strongs/h/h1827.md), so that the [gal](../../strongs/h/h1530.md) thereof are [ḥāšâ](../../strongs/h/h2814.md).

<a name="psalms_107_30"></a>Psalms 107:30

Then are they [samach](../../strongs/h/h8055.md) because they be [šāṯaq](../../strongs/h/h8367.md); so he [nachah](../../strongs/h/h5148.md) them unto their [chephets](../../strongs/h/h2656.md) [māḥôz](../../strongs/h/h4231.md).

<a name="psalms_107_31"></a>Psalms 107:31

Oh that men would [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md) for his [checed](../../strongs/h/h2617.md), and for his [pala'](../../strongs/h/h6381.md) to the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md)!

<a name="psalms_107_32"></a>Psalms 107:32

Let them [ruwm](../../strongs/h/h7311.md) him also in the [qāhēl](../../strongs/h/h6951.md) of the ['am](../../strongs/h/h5971.md), and [halal](../../strongs/h/h1984.md) him in the [môšāḇ](../../strongs/h/h4186.md) of the [zāqēn](../../strongs/h/h2205.md).

<a name="psalms_107_33"></a>Psalms 107:33

He [śûm](../../strongs/h/h7760.md) [nāhār](../../strongs/h/h5104.md) into a [midbar](../../strongs/h/h4057.md), and the [mayim](../../strongs/h/h4325.md) [môṣā'](../../strongs/h/h4161.md) into [ṣimmā'ôn](../../strongs/h/h6774.md);

<a name="psalms_107_34"></a>Psalms 107:34

A [pĕriy](../../strongs/h/h6529.md) ['erets](../../strongs/h/h776.md) into [mᵊlēḥâ](../../strongs/h/h4420.md), for the [ra'](../../strongs/h/h7451.md) of them that [yashab](../../strongs/h/h3427.md) therein.

<a name="psalms_107_35"></a>Psalms 107:35

He [śûm](../../strongs/h/h7760.md) the [midbar](../../strongs/h/h4057.md) into a ['ăḡam](../../strongs/h/h98.md) [mayim](../../strongs/h/h4325.md), and [ṣîyâ](../../strongs/h/h6723.md) ['erets](../../strongs/h/h776.md) into [mayim](../../strongs/h/h4325.md) [môṣā'](../../strongs/h/h4161.md).

<a name="psalms_107_36"></a>Psalms 107:36

And there he maketh the [rāʿēḇ](../../strongs/h/h7457.md) to [yashab](../../strongs/h/h3427.md), that they may [kuwn](../../strongs/h/h3559.md) a [ʿîr](../../strongs/h/h5892.md) for [môšāḇ](../../strongs/h/h4186.md);

<a name="psalms_107_37"></a>Psalms 107:37

And [zāraʿ](../../strongs/h/h2232.md) the [sadeh](../../strongs/h/h7704.md), and [nāṭaʿ](../../strongs/h/h5193.md) [kerem](../../strongs/h/h3754.md), which may ['asah](../../strongs/h/h6213.md) [pĕriy](../../strongs/h/h6529.md) of [tᵊḇû'â](../../strongs/h/h8393.md).

<a name="psalms_107_38"></a>Psalms 107:38

He [barak](../../strongs/h/h1288.md) them also, so that they are [rabah](../../strongs/h/h7235.md) [me'od](../../strongs/h/h3966.md); and [māʿaṭ](../../strongs/h/h4591.md) not their [bĕhemah](../../strongs/h/h929.md) to [māʿaṭ](../../strongs/h/h4591.md).

<a name="psalms_107_39"></a>Psalms 107:39

Again, they are [māʿaṭ](../../strongs/h/h4591.md) and [shachach](../../strongs/h/h7817.md) through [ʿōṣer](../../strongs/h/h6115.md), [ra'](../../strongs/h/h7451.md), and [yagown](../../strongs/h/h3015.md).

<a name="psalms_107_40"></a>Psalms 107:40

He [šāp̄aḵ](../../strongs/h/h8210.md) [bûz](../../strongs/h/h937.md) upon [nāḏîḇ](../../strongs/h/h5081.md), and causeth them to [tāʿâ](../../strongs/h/h8582.md) in the [tohuw](../../strongs/h/h8414.md), where there is no [derek](../../strongs/h/h1870.md).

<a name="psalms_107_41"></a>Psalms 107:41

Yet setteth he the ['ebyown](../../strongs/h/h34.md) on [śāḡaḇ](../../strongs/h/h7682.md) from ['oniy](../../strongs/h/h6040.md), and [śûm](../../strongs/h/h7760.md) him [mišpāḥâ](../../strongs/h/h4940.md) like a [tso'n](../../strongs/h/h6629.md).

<a name="psalms_107_42"></a>Psalms 107:42

The [yashar](../../strongs/h/h3477.md) shall [ra'ah](../../strongs/h/h7200.md) it, and [samach](../../strongs/h/h8055.md): and all ['evel](../../strongs/h/h5766.md) shall [qāp̄aṣ](../../strongs/h/h7092.md) her [peh](../../strongs/h/h6310.md).

<a name="psalms_107_43"></a>Psalms 107:43

Whoso is [ḥāḵām](../../strongs/h/h2450.md), and will [shamar](../../strongs/h/h8104.md) these things, even they shall [bîn](../../strongs/h/h995.md) the [checed](../../strongs/h/h2617.md) of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 106](psalms_106.md) - [Psalms 108](psalms_108.md)