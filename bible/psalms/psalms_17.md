# [Psalms 17](https://www.blueletterbible.org/kjv/psa/17/1/s_495001)

<a name="psalms_17_1"></a>Psalms 17:1

A [tĕphillah](../../strongs/h/h8605.md) of [Dāviḏ](../../strongs/h/h1732.md). [shama'](../../strongs/h/h8085.md) the [tsedeq](../../strongs/h/h6664.md), [Yĕhovah](../../strongs/h/h3068.md), attend unto my [rinnah](../../strongs/h/h7440.md), ['azan](../../strongs/h/h238.md) unto my [tĕphillah](../../strongs/h/h8605.md), that goeth not out of [mirmah](../../strongs/h/h4820.md) [saphah](../../strongs/h/h8193.md).

<a name="psalms_17_2"></a>Psalms 17:2

Let my [mishpat](../../strongs/h/h4941.md) [yāṣā'](../../strongs/h/h3318.md) from thy [paniym](../../strongs/h/h6440.md); let thine ['ayin](../../strongs/h/h5869.md) [chazah](../../strongs/h/h2372.md) the things that are [meyshar](../../strongs/h/h4339.md).

<a name="psalms_17_3"></a>Psalms 17:3

Thou hast [bachan](../../strongs/h/h974.md) mine [leb](../../strongs/h/h3820.md); thou hast [paqad](../../strongs/h/h6485.md) me in the [layil](../../strongs/h/h3915.md); thou hast [tsaraph](../../strongs/h/h6884.md) me, and shalt [māṣā'](../../strongs/h/h4672.md) nothing; I am [zāmam](../../strongs/h/h2161.md) that my [peh](../../strongs/h/h6310.md) shall not ['abar](../../strongs/h/h5674.md).

<a name="psalms_17_4"></a>Psalms 17:4

Concerning the [p?'ullah](../../strongs/h/h6468.md) of ['adam](../../strongs/h/h120.md), by the [dabar](../../strongs/h/h1697.md) of thy [saphah](../../strongs/h/h8193.md) I have [shamar](../../strongs/h/h8104.md) from the ['orach](../../strongs/h/h734.md) of the [periyts](../../strongs/h/h6530.md).

<a name="psalms_17_5"></a>Psalms 17:5

[tamak](../../strongs/h/h8551.md) my ['ashuwr](../../strongs/h/h838.md) in thy [ma'gal](../../strongs/h/h4570.md), my [pa'am](../../strongs/h/h6471.md) [mowt](../../strongs/h/h4131.md) not.

<a name="psalms_17_6"></a>Psalms 17:6

I have [qara'](../../strongs/h/h7121.md) upon thee, for thou wilt ['anah](../../strongs/h/h6030.md) me, ['el](../../strongs/h/h410.md): [natah](../../strongs/h/h5186.md) thine ['ozen](../../strongs/h/h241.md) unto me, and [shama'](../../strongs/h/h8085.md) my ['imrah](../../strongs/h/h565.md).

<a name="psalms_17_7"></a>Psalms 17:7

[palah](../../strongs/h/h6395.md) thy [checed](../../strongs/h/h2617.md), O thou that [yasha'](../../strongs/h/h3467.md) by thy [yamiyn](../../strongs/h/h3225.md) them which put their [chacah](../../strongs/h/h2620.md) from those that [quwm](../../strongs/h/h6965.md).

<a name="psalms_17_8"></a>Psalms 17:8

[shamar](../../strongs/h/h8104.md) me as the ['iyshown](../../strongs/h/h380.md) [bath](../../strongs/h/h1323.md) ['ayin](../../strongs/h/h5869.md), [cathar](../../strongs/h/h5641.md) me under the [ṣēl](../../strongs/h/h6738.md) of thy [kanaph](../../strongs/h/h3671.md),

<a name="psalms_17_9"></a>Psalms 17:9

From the [rasha'](../../strongs/h/h7563.md) that [shadad](../../strongs/h/h7703.md) me, from my [nephesh](../../strongs/h/h5315.md) ['oyeb](../../strongs/h/h341.md), [naqaph](../../strongs/h/h5362.md).

<a name="psalms_17_10"></a>Psalms 17:10

They are [cagar](../../strongs/h/h5462.md) in their own [cheleb](../../strongs/h/h2459.md): with their [peh](../../strongs/h/h6310.md) they [dabar](../../strongs/h/h1696.md) [ge'uwth](../../strongs/h/h1348.md).

<a name="psalms_17_11"></a>Psalms 17:11

They have now [cabab](../../strongs/h/h5437.md) us in our ['ashuwr](../../strongs/h/h838.md): they have [shiyth](../../strongs/h/h7896.md) their ['ayin](../../strongs/h/h5869.md) [natah](../../strongs/h/h5186.md) to the ['erets](../../strongs/h/h776.md);

<a name="psalms_17_12"></a>Psalms 17:12

Like as an ['ariy](../../strongs/h/h738.md) that is [kacaph](../../strongs/h/h3700.md) of his [taaph](../../strongs/h/h2963.md), and as it were a [kephiyr](../../strongs/h/h3715.md) [yasha](../../strongs/h/h3427.md) in [mictar](../../strongs/h/h4565.md).

<a name="psalms_17_13"></a>Psalms 17:13

[quwm](../../strongs/h/h6965.md), [Yĕhovah](../../strongs/h/h3068.md), [qadam](../../strongs/h/h6923.md) him, [kara'](../../strongs/h/h3766.md): [palat](../../strongs/h/h6403.md) my [nephesh](../../strongs/h/h5315.md) from the [rasha'](../../strongs/h/h7563.md), which is thy [chereb](../../strongs/h/h2719.md):

<a name="psalms_17_14"></a>Psalms 17:14

From [math](../../strongs/h/h4962.md) thy [yad](../../strongs/h/h3027.md), [Yĕhovah](../../strongs/h/h3068.md), from [math](../../strongs/h/h4962.md) of the [cheled](../../strongs/h/h2465.md), their portion in [chay](../../strongs/h/h2416.md), and whose [beten](../../strongs/h/h990.md) thou [mālā'](../../strongs/h/h4390.md) with thy [ṣāp̄în](../../strongs/h/h6840.md): they are [sāׂbaʿ](../../strongs/h/h7646.md) of [ben](../../strongs/h/h1121.md), and [yānaḥ](../../strongs/h/h3240.md) the [yeṯer](../../strongs/h/h3499.md) of their to their ['owlel](../../strongs/h/h5768.md).

<a name="psalms_17_15"></a>Psalms 17:15

As for me, I will [chazah](../../strongs/h/h2372.md) thy [paniym](../../strongs/h/h6440.md) in [tsedeq](../../strongs/h/h6664.md): I shall be [sāׂbaʿ](../../strongs/h/h7646.md), when I [quwts](../../strongs/h/h6974.md), with thy [tĕmuwnah](../../strongs/h/h8544.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 16](psalms_16.md) - [Psalms 18](psalms_18.md)