# [Psalms 54](https://www.blueletterbible.org/kjv/psa/54)

<a name="psalms_54_1"></a>Psalms 54:1

To the [nāṣaḥ](../../strongs/h/h5329.md) on [Nᵊḡînâ](../../strongs/h/h5058.md), [Maśkîl](../../strongs/h/h4905.md), [A Psalm] of [Dāviḏ](../../strongs/h/h1732.md), when the [Zîp̄î](../../strongs/h/h2130.md) [bow'](../../strongs/h/h935.md) and ['āmar](../../strongs/h/h559.md) to [Šā'ûl](../../strongs/h/h7586.md), Doth not [Dāviḏ](../../strongs/h/h1732.md) [cathar](../../strongs/h/h5641.md) himself with us? [yasha'](../../strongs/h/h3467.md) me, ['Elohiym](../../strongs/h/h430.md), by thy [shem](../../strongs/h/h8034.md), and [diyn](../../strongs/h/h1777.md) me by thy [gᵊḇûrâ](../../strongs/h/h1369.md).

<a name="psalms_54_2"></a>Psalms 54:2

[shama'](../../strongs/h/h8085.md) my [tĕphillah](../../strongs/h/h8605.md), ['Elohiym](../../strongs/h/h430.md); ['azan](../../strongs/h/h238.md) to the ['emer](../../strongs/h/h561.md) of my [peh](../../strongs/h/h6310.md).

<a name="psalms_54_3"></a>Psalms 54:3

For [zûr](../../strongs/h/h2114.md) are [quwm](../../strongs/h/h6965.md) against me, and [ʿārîṣ](../../strongs/h/h6184.md) [bāqaš](../../strongs/h/h1245.md) after my [nephesh](../../strongs/h/h5315.md): they have not [śûm](../../strongs/h/h7760.md) ['Elohiym](../../strongs/h/h430.md) before them. [Celah](../../strongs/h/h5542.md).

<a name="psalms_54_4"></a>Psalms 54:4

Behold, ['Elohiym](../../strongs/h/h430.md) is mine [ʿāzar](../../strongs/h/h5826.md): ['adonay](../../strongs/h/h136.md) is with them that [camak](../../strongs/h/h5564.md) my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_54_5"></a>Psalms 54:5

He shall [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) [ra'](../../strongs/h/h7451.md) unto mine [sharar](../../strongs/h/h8324.md): [ṣāmaṯ](../../strongs/h/h6789.md) them in thy ['emeth](../../strongs/h/h571.md).

<a name="psalms_54_6"></a>Psalms 54:6

I will [nᵊḏāḇâ](../../strongs/h/h5071.md) [zabach](../../strongs/h/h2076.md) unto thee: I will [yadah](../../strongs/h/h3034.md) thy [shem](../../strongs/h/h8034.md), [Yĕhovah](../../strongs/h/h3068.md); for it is [towb](../../strongs/h/h2896.md).

<a name="psalms_54_7"></a>Psalms 54:7

For he hath [natsal](../../strongs/h/h5337.md) me out of all [tsarah](../../strongs/h/h6869.md): and mine ['ayin](../../strongs/h/h5869.md) hath [ra'ah](../../strongs/h/h7200.md) his desire upon mine ['oyeb](../../strongs/h/h341.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 53](psalms_53.md) - [Psalms 55](psalms_55.md)