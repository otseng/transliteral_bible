# [Psalms 119](https://www.blueletterbible.org/kjv/psalms/119)

<a name="psalms_119_1"></a>Psalms 119:1

ALEPH. ['esher](../../strongs/h/h835.md) are the [tamiym](../../strongs/h/h8549.md) in the [derek](../../strongs/h/h1870.md), who [halak](../../strongs/h/h1980.md) in the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_119_2"></a>Psalms 119:2

['esher](../../strongs/h/h835.md) are they that [nāṣar](../../strongs/h/h5341.md) his [ʿēḏâ](../../strongs/h/h5713.md), and that [darash](../../strongs/h/h1875.md) him with the whole [leb](../../strongs/h/h3820.md).

<a name="psalms_119_3"></a>Psalms 119:3

They also [pa'al](../../strongs/h/h6466.md) no ['evel](../../strongs/h/h5766.md): they [halak](../../strongs/h/h1980.md) in his [derek](../../strongs/h/h1870.md).

<a name="psalms_119_4"></a>Psalms 119:4

Thou hast [tsavah](../../strongs/h/h6680.md) us to [shamar](../../strongs/h/h8104.md) thy [piqquwd](../../strongs/h/h6490.md) [me'od](../../strongs/h/h3966.md).

<a name="psalms_119_5"></a>Psalms 119:5

['aḥălay](../../strongs/h/h305.md) my [derek](../../strongs/h/h1870.md) were [kuwn](../../strongs/h/h3559.md) to [shamar](../../strongs/h/h8104.md) thy [choq](../../strongs/h/h2706.md)!

<a name="psalms_119_6"></a>Psalms 119:6

Then shall I not be [buwsh](../../strongs/h/h954.md), when I have [nabat](../../strongs/h/h5027.md) unto all thy [mitsvah](../../strongs/h/h4687.md).

<a name="psalms_119_7"></a>Psalms 119:7

I will [yadah](../../strongs/h/h3034.md) thee with [yōšer](../../strongs/h/h3476.md) of [lebab](../../strongs/h/h3824.md), when I shall have [lamad](../../strongs/h/h3925.md) thy [tsedeq](../../strongs/h/h6664.md) [mishpat](../../strongs/h/h4941.md).

<a name="psalms_119_8"></a>Psalms 119:8

I will [shamar](../../strongs/h/h8104.md) thy [choq](../../strongs/h/h2706.md): O ['azab](../../strongs/h/h5800.md) me not [me'od](../../strongs/h/h3966.md).

<a name="psalms_119_9"></a>Psalms 119:9

BETH. Wherewithal shall a [naʿar](../../strongs/h/h5288.md) [zāḵâ](../../strongs/h/h2135.md) his ['orach](../../strongs/h/h734.md)? by taking [shamar](../../strongs/h/h8104.md) thereto according to thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_10"></a>Psalms 119:10

With my whole [leb](../../strongs/h/h3820.md) have I [darash](../../strongs/h/h1875.md) thee: O let me not [šāḡâ](../../strongs/h/h7686.md) from thy [mitsvah](../../strongs/h/h4687.md).

<a name="psalms_119_11"></a>Psalms 119:11

Thy ['imrah](../../strongs/h/h565.md) have I [tsaphan](../../strongs/h/h6845.md) in mine [leb](../../strongs/h/h3820.md), that I might not [chata'](../../strongs/h/h2398.md) against thee.

<a name="psalms_119_12"></a>Psalms 119:12

[barak](../../strongs/h/h1288.md) art thou, [Yĕhovah](../../strongs/h/h3068.md): [lamad](../../strongs/h/h3925.md) me thy [choq](../../strongs/h/h2706.md).

<a name="psalms_119_13"></a>Psalms 119:13

With my [saphah](../../strongs/h/h8193.md) have I [sāp̄ar](../../strongs/h/h5608.md) all the [mishpat](../../strongs/h/h4941.md) of thy [peh](../../strongs/h/h6310.md).

<a name="psalms_119_14"></a>Psalms 119:14

I have [śûś](../../strongs/h/h7797.md) in the [derek](../../strongs/h/h1870.md) of thy [ʿēḏûṯ](../../strongs/h/h5715.md), as much as [ʿal](../../strongs/h/h5921.md) all [hôn](../../strongs/h/h1952.md).

<a name="psalms_119_15"></a>Psalms 119:15

I will [śîaḥ](../../strongs/h/h7878.md) in thy [piqquwd](../../strongs/h/h6490.md), and have [nabat](../../strongs/h/h5027.md) unto thy ['orach](../../strongs/h/h734.md).

<a name="psalms_119_16"></a>Psalms 119:16

I will [šāʿaʿ](../../strongs/h/h8173.md) myself in thy [chuqqah](../../strongs/h/h2708.md): I will not [shakach](../../strongs/h/h7911.md) thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_17"></a>Psalms 119:17

GIMEL. [gamal](../../strongs/h/h1580.md) with thy ['ebed](../../strongs/h/h5650.md), that I may [ḥāyâ](../../strongs/h/h2421.md), and [shamar](../../strongs/h/h8104.md) thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_18"></a>Psalms 119:18

[gālâ](../../strongs/h/h1540.md) thou mine ['ayin](../../strongs/h/h5869.md), that I may [nabat](../../strongs/h/h5027.md) [pala'](../../strongs/h/h6381.md) out of thy [towrah](../../strongs/h/h8451.md).

<a name="psalms_119_19"></a>Psalms 119:19

I am a [ger](../../strongs/h/h1616.md) in the ['erets](../../strongs/h/h776.md): [cathar](../../strongs/h/h5641.md) not thy [mitsvah](../../strongs/h/h4687.md) from me.

<a name="psalms_119_20"></a>Psalms 119:20

My [nephesh](../../strongs/h/h5315.md) [gāras](../../strongs/h/h1638.md) for the [ta'ăḇâ](../../strongs/h/h8375.md) that it hath unto thy [mishpat](../../strongs/h/h4941.md) at all [ʿēṯ](../../strongs/h/h6256.md).

<a name="psalms_119_21"></a>Psalms 119:21

Thou hast [gāʿar](../../strongs/h/h1605.md) the [zed](../../strongs/h/h2086.md) that are ['arar](../../strongs/h/h779.md), which do [šāḡâ](../../strongs/h/h7686.md) from thy [mitsvah](../../strongs/h/h4687.md).

<a name="psalms_119_22"></a>Psalms 119:22

[gālal](../../strongs/h/h1556.md) from me [cherpah](../../strongs/h/h2781.md) and [bûz](../../strongs/h/h937.md); for I have [nāṣar](../../strongs/h/h5341.md) thy [ʿēḏâ](../../strongs/h/h5713.md).

<a name="psalms_119_23"></a>Psalms 119:23

[śar](../../strongs/h/h8269.md) also did [yashab](../../strongs/h/h3427.md) and [dabar](../../strongs/h/h1696.md) against me: but thy ['ebed](../../strongs/h/h5650.md) did [śîaḥ](../../strongs/h/h7878.md) in thy [choq](../../strongs/h/h2706.md).

<a name="psalms_119_24"></a>Psalms 119:24

Thy [ʿēḏâ](../../strongs/h/h5713.md) also are my [šaʿăšûʿîm](../../strongs/h/h8191.md) and my ['enowsh](../../strongs/h/h582.md) ['etsah](../../strongs/h/h6098.md).

<a name="psalms_119_25"></a>Psalms 119:25

DALETH. My [nephesh](../../strongs/h/h5315.md) [dāḇaq](../../strongs/h/h1692.md) unto the ['aphar](../../strongs/h/h6083.md): [ḥāyâ](../../strongs/h/h2421.md) thou me according to thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_26"></a>Psalms 119:26

I have [sāp̄ar](../../strongs/h/h5608.md) my [derek](../../strongs/h/h1870.md), and thou ['anah](../../strongs/h/h6030.md) me: [lamad](../../strongs/h/h3925.md) me thy [choq](../../strongs/h/h2706.md).

<a name="psalms_119_27"></a>Psalms 119:27

Make me to [bîn](../../strongs/h/h995.md) the [derek](../../strongs/h/h1870.md) of thy [piqquwd](../../strongs/h/h6490.md): so shall I [śîaḥ](../../strongs/h/h7878.md) of thy [pala'](../../strongs/h/h6381.md).

<a name="psalms_119_28"></a>Psalms 119:28

My [nephesh](../../strongs/h/h5315.md) [dālap̄](../../strongs/h/h1811.md) for [tûḡâ](../../strongs/h/h8424.md): [quwm](../../strongs/h/h6965.md) thou me according unto thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_29"></a>Psalms 119:29

[cuwr](../../strongs/h/h5493.md) from me the [derek](../../strongs/h/h1870.md) of [sheqer](../../strongs/h/h8267.md): and grant me thy [towrah](../../strongs/h/h8451.md) [chanan](../../strongs/h/h2603.md).

<a name="psalms_119_30"></a>Psalms 119:30

I have [bāḥar](../../strongs/h/h977.md) the [derek](../../strongs/h/h1870.md) of ['ĕmûnâ](../../strongs/h/h530.md): thy [mishpat](../../strongs/h/h4941.md) have I [šāvâ](../../strongs/h/h7737.md) before me.

<a name="psalms_119_31"></a>Psalms 119:31

I have [dāḇaq](../../strongs/h/h1692.md) unto thy [ʿēḏûṯ](../../strongs/h/h5715.md): [Yĕhovah](../../strongs/h/h3068.md), put me not to [buwsh](../../strongs/h/h954.md).

<a name="psalms_119_32"></a>Psalms 119:32

I will [rûṣ](../../strongs/h/h7323.md) the [derek](../../strongs/h/h1870.md) of thy [mitsvah](../../strongs/h/h4687.md), when thou shalt [rāḥaḇ](../../strongs/h/h7337.md) my [leb](../../strongs/h/h3820.md).

<a name="psalms_119_33"></a>Psalms 119:33

HE. [yārâ](../../strongs/h/h3384.md) me, [Yĕhovah](../../strongs/h/h3068.md), the [derek](../../strongs/h/h1870.md) of thy [choq](../../strongs/h/h2706.md); and I shall [nāṣar](../../strongs/h/h5341.md) it unto the [ʿēqeḇ](../../strongs/h/h6118.md).

<a name="psalms_119_34"></a>Psalms 119:34

Give me [bîn](../../strongs/h/h995.md), and I shall [nāṣar](../../strongs/h/h5341.md) thy [towrah](../../strongs/h/h8451.md); yea, I shall [shamar](../../strongs/h/h8104.md) it with my whole [leb](../../strongs/h/h3820.md).

<a name="psalms_119_35"></a>Psalms 119:35

Make me to [dāraḵ](../../strongs/h/h1869.md) in the [nāṯîḇ](../../strongs/h/h5410.md) of thy [mitsvah](../../strongs/h/h4687.md); for therein do I [ḥāp̄ēṣ](../../strongs/h/h2654.md).

<a name="psalms_119_36"></a>Psalms 119:36

[natah](../../strongs/h/h5186.md) my [leb](../../strongs/h/h3820.md) unto thy [ʿēḏûṯ](../../strongs/h/h5715.md), and not to [beṣaʿ](../../strongs/h/h1215.md).

<a name="psalms_119_37"></a>Psalms 119:37

['abar](../../strongs/h/h5674.md) mine ['ayin](../../strongs/h/h5869.md) from [ra'ah](../../strongs/h/h7200.md) [shav'](../../strongs/h/h7723.md); and [ḥāyâ](../../strongs/h/h2421.md) thou me in thy [derek](../../strongs/h/h1870.md).

<a name="psalms_119_38"></a>Psalms 119:38

[quwm](../../strongs/h/h6965.md) thy ['imrah](../../strongs/h/h565.md) unto thy ['ebed](../../strongs/h/h5650.md), who is devoted to thy [yir'ah](../../strongs/h/h3374.md).

<a name="psalms_119_39"></a>Psalms 119:39

['abar](../../strongs/h/h5674.md) my [cherpah](../../strongs/h/h2781.md) which I [yāḡōr](../../strongs/h/h3025.md): for thy [mishpat](../../strongs/h/h4941.md) are [towb](../../strongs/h/h2896.md).

<a name="psalms_119_40"></a>Psalms 119:40

Behold, I have [tā'aḇ](../../strongs/h/h8373.md) after thy [piqquwd](../../strongs/h/h6490.md): [ḥāyâ](../../strongs/h/h2421.md) me in thy [tsedaqah](../../strongs/h/h6666.md).

<a name="psalms_119_41"></a>Psalms 119:41

VAU. Let thy [checed](../../strongs/h/h2617.md) [bow'](../../strongs/h/h935.md) also unto me, [Yĕhovah](../../strongs/h/h3068.md), even thy [tᵊšûʿâ](../../strongs/h/h8668.md), according to thy ['imrah](../../strongs/h/h565.md).

<a name="psalms_119_42"></a>Psalms 119:42

So shall I have [dabar](../../strongs/h/h1697.md) to ['anah](../../strongs/h/h6030.md) him that [ḥārap̄](../../strongs/h/h2778.md) me: for I [batach](../../strongs/h/h982.md) in thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_43"></a>Psalms 119:43

And [natsal](../../strongs/h/h5337.md) not the [dabar](../../strongs/h/h1697.md) of ['emeth](../../strongs/h/h571.md) [me'od](../../strongs/h/h3966.md) out of my [peh](../../strongs/h/h6310.md); for I have [yāḥal](../../strongs/h/h3176.md) in thy [mishpat](../../strongs/h/h4941.md).

<a name="psalms_119_44"></a>Psalms 119:44

So shall I [shamar](../../strongs/h/h8104.md) thy [towrah](../../strongs/h/h8451.md) [tāmîḏ](../../strongs/h/h8548.md) ['owlam](../../strongs/h/h5769.md) and [ʿaḏ](../../strongs/h/h5703.md).

<a name="psalms_119_45"></a>Psalms 119:45

And I will [halak](../../strongs/h/h1980.md) at [rāḥāḇ](../../strongs/h/h7342.md): for I [darash](../../strongs/h/h1875.md) thy [piqquwd](../../strongs/h/h6490.md).

<a name="psalms_119_46"></a>Psalms 119:46

I will [dabar](../../strongs/h/h1696.md) of thy [ʿēḏâ](../../strongs/h/h5713.md) also before [melek](../../strongs/h/h4428.md), and will not be [buwsh](../../strongs/h/h954.md).

<a name="psalms_119_47"></a>Psalms 119:47

And I will [šāʿaʿ](../../strongs/h/h8173.md) myself in thy [mitsvah](../../strongs/h/h4687.md), which I have ['ahab](../../strongs/h/h157.md).

<a name="psalms_119_48"></a>Psalms 119:48

My [kaph](../../strongs/h/h3709.md) also will I [nasa'](../../strongs/h/h5375.md) unto thy [mitsvah](../../strongs/h/h4687.md), which I have ['ahab](../../strongs/h/h157.md); and I will [śîaḥ](../../strongs/h/h7878.md) in thy [choq](../../strongs/h/h2706.md).

<a name="psalms_119_49"></a>Psalms 119:49

ZAIN. [zakar](../../strongs/h/h2142.md) the [dabar](../../strongs/h/h1697.md) unto thy ['ebed](../../strongs/h/h5650.md), upon which thou hast caused me to [yāḥal](../../strongs/h/h3176.md).

<a name="psalms_119_50"></a>Psalms 119:50

This is my [neḥāmâ](../../strongs/h/h5165.md) in my ['oniy](../../strongs/h/h6040.md): for thy ['imrah](../../strongs/h/h565.md) hath [ḥāyâ](../../strongs/h/h2421.md) me.

<a name="psalms_119_51"></a>Psalms 119:51

The [zed](../../strongs/h/h2086.md) have had me [me'od](../../strongs/h/h3966.md) in [luwts](../../strongs/h/h3887.md): yet have I not [natah](../../strongs/h/h5186.md) from thy [towrah](../../strongs/h/h8451.md).

<a name="psalms_119_52"></a>Psalms 119:52

I [zakar](../../strongs/h/h2142.md) thy [mishpat](../../strongs/h/h4941.md) of ['owlam](../../strongs/h/h5769.md), [Yĕhovah](../../strongs/h/h3068.md); and have [nacham](../../strongs/h/h5162.md) myself.

<a name="psalms_119_53"></a>Psalms 119:53

[zal'aphah](../../strongs/h/h2152.md) hath taken ['āḥaz](../../strongs/h/h270.md) upon me because of the [rasha'](../../strongs/h/h7563.md) that ['azab](../../strongs/h/h5800.md) thy [towrah](../../strongs/h/h8451.md).

<a name="psalms_119_54"></a>Psalms 119:54

Thy [choq](../../strongs/h/h2706.md) have been my [zᵊmîr](../../strongs/h/h2158.md) in the [bayith](../../strongs/h/h1004.md) of my [māḡûr](../../strongs/h/h4033.md).

<a name="psalms_119_55"></a>Psalms 119:55

I have [zakar](../../strongs/h/h2142.md) thy [shem](../../strongs/h/h8034.md), [Yĕhovah](../../strongs/h/h3068.md), in the [layil](../../strongs/h/h3915.md), and have [shamar](../../strongs/h/h8104.md) thy [towrah](../../strongs/h/h8451.md).

<a name="psalms_119_56"></a>Psalms 119:56

This I had, because I [nāṣar](../../strongs/h/h5341.md) thy [piqquwd](../../strongs/h/h6490.md).

<a name="psalms_119_57"></a>Psalms 119:57

CHETH. Thou art my [cheleq](../../strongs/h/h2506.md), [Yĕhovah](../../strongs/h/h3068.md): I have ['āmar](../../strongs/h/h559.md) that I would [shamar](../../strongs/h/h8104.md) thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_58"></a>Psalms 119:58

I [ḥālâ](../../strongs/h/h2470.md) thy [paniym](../../strongs/h/h6440.md) with my whole [leb](../../strongs/h/h3820.md): be [chanan](../../strongs/h/h2603.md) unto me according to thy ['imrah](../../strongs/h/h565.md).

<a name="psalms_119_59"></a>Psalms 119:59

I [chashab](../../strongs/h/h2803.md) on my [derek](../../strongs/h/h1870.md), and [shuwb](../../strongs/h/h7725.md) my [regel](../../strongs/h/h7272.md) unto thy [ʿēḏâ](../../strongs/h/h5713.md).

<a name="psalms_119_60"></a>Psalms 119:60

I made [ḥûš](../../strongs/h/h2363.md), and [māhah](../../strongs/h/h4102.md) not to [shamar](../../strongs/h/h8104.md) thy [mitsvah](../../strongs/h/h4687.md).

<a name="psalms_119_61"></a>Psalms 119:61

The [chebel](../../strongs/h/h2256.md) of the [rasha'](../../strongs/h/h7563.md) have [ʿûḏ](../../strongs/h/h5749.md) me: but I have not [shakach](../../strongs/h/h7911.md) thy [towrah](../../strongs/h/h8451.md).

<a name="psalms_119_62"></a>Psalms 119:62

At [ḥāṣôṯ](../../strongs/h/h2676.md) [layil](../../strongs/h/h3915.md) I will [quwm](../../strongs/h/h6965.md) to [yadah](../../strongs/h/h3034.md) unto thee because of thy [tsedeq](../../strongs/h/h6664.md) [mishpat](../../strongs/h/h4941.md).

<a name="psalms_119_63"></a>Psalms 119:63

I am a [ḥāḇēr](../../strongs/h/h2270.md) of all them that [yare'](../../strongs/h/h3372.md) thee, and of them that [shamar](../../strongs/h/h8104.md) thy [piqquwd](../../strongs/h/h6490.md).

<a name="psalms_119_64"></a>Psalms 119:64

The ['erets](../../strongs/h/h776.md), [Yĕhovah](../../strongs/h/h3068.md), is [mālā'](../../strongs/h/h4390.md) of thy [checed](../../strongs/h/h2617.md): [lamad](../../strongs/h/h3925.md) me thy [choq](../../strongs/h/h2706.md).

<a name="psalms_119_65"></a>Psalms 119:65

TETH. Thou hast ['asah](../../strongs/h/h6213.md) [towb](../../strongs/h/h2896.md) with thy ['ebed](../../strongs/h/h5650.md), [Yĕhovah](../../strongs/h/h3068.md), according unto thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_66"></a>Psalms 119:66

[lamad](../../strongs/h/h3925.md) me [ṭûḇ](../../strongs/h/h2898.md) [ṭaʿam](../../strongs/h/h2940.md) and [da'ath](../../strongs/h/h1847.md): for I have ['aman](../../strongs/h/h539.md) thy [mitsvah](../../strongs/h/h4687.md).

<a name="psalms_119_67"></a>Psalms 119:67

Before I was [ʿānâ](../../strongs/h/h6031.md) I went [šāḡaḡ](../../strongs/h/h7683.md): but now have I [shamar](../../strongs/h/h8104.md) thy ['imrah](../../strongs/h/h565.md).

<a name="psalms_119_68"></a>Psalms 119:68

Thou art [towb](../../strongs/h/h2896.md), and doest [ṭôḇ](../../strongs/h/h2895.md); [lamad](../../strongs/h/h3925.md) me thy [choq](../../strongs/h/h2706.md).

<a name="psalms_119_69"></a>Psalms 119:69

The [zed](../../strongs/h/h2086.md) have [ṭāp̄al](../../strongs/h/h2950.md) a [sheqer](../../strongs/h/h8267.md) against me: but I will [nāṣar](../../strongs/h/h5341.md) thy [piqquwd](../../strongs/h/h6490.md) with my whole [leb](../../strongs/h/h3820.md).

<a name="psalms_119_70"></a>Psalms 119:70

Their [leb](../../strongs/h/h3820.md) is as [ṭāp̄aš](../../strongs/h/h2954.md) as [cheleb](../../strongs/h/h2459.md); but I [šāʿaʿ](../../strongs/h/h8173.md) in thy [towrah](../../strongs/h/h8451.md).

<a name="psalms_119_71"></a>Psalms 119:71

It is [towb](../../strongs/h/h2896.md) for me that I have been [ʿānâ](../../strongs/h/h6031.md); that I might [lamad](../../strongs/h/h3925.md) thy [choq](../../strongs/h/h2706.md).

<a name="psalms_119_72"></a>Psalms 119:72

The [towrah](../../strongs/h/h8451.md) of thy [peh](../../strongs/h/h6310.md) is [towb](../../strongs/h/h2896.md) unto me than ['elep̄](../../strongs/h/h505.md) of [zāhāḇ](../../strongs/h/h2091.md) and [keceph](../../strongs/h/h3701.md).

<a name="psalms_119_73"></a>Psalms 119:73

JOD. Thy [yad](../../strongs/h/h3027.md) have ['asah](../../strongs/h/h6213.md) me and [kuwn](../../strongs/h/h3559.md) me: give me [bîn](../../strongs/h/h995.md), that I may [lamad](../../strongs/h/h3925.md) thy [mitsvah](../../strongs/h/h4687.md).

<a name="psalms_119_74"></a>Psalms 119:74

They that [yārē'](../../strongs/h/h3373.md) thee will be [samach](../../strongs/h/h8055.md) when they [ra'ah](../../strongs/h/h7200.md) me; because I have [yāḥal](../../strongs/h/h3176.md) in thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_75"></a>Psalms 119:75

I [yada'](../../strongs/h/h3045.md), [Yĕhovah](../../strongs/h/h3068.md), that thy [mishpat](../../strongs/h/h4941.md) are [tsedeq](../../strongs/h/h6664.md), and that thou in ['ĕmûnâ](../../strongs/h/h530.md) hast [ʿānâ](../../strongs/h/h6031.md) me.

<a name="psalms_119_76"></a>Psalms 119:76

Let, I pray thee, thy [checed](../../strongs/h/h2617.md) be for my [nacham](../../strongs/h/h5162.md), according to thy ['imrah](../../strongs/h/h565.md) unto thy ['ebed](../../strongs/h/h5650.md).

<a name="psalms_119_77"></a>Psalms 119:77

Let thy [raḥam](../../strongs/h/h7356.md) [bow'](../../strongs/h/h935.md) unto me, that I may [ḥāyâ](../../strongs/h/h2421.md): for thy [towrah](../../strongs/h/h8451.md) is my [šaʿăšûʿîm](../../strongs/h/h8191.md).

<a name="psalms_119_78"></a>Psalms 119:78

Let the [zed](../../strongs/h/h2086.md) be [buwsh](../../strongs/h/h954.md); for they dealt [ʿāvaṯ](../../strongs/h/h5791.md) with me without a [sheqer](../../strongs/h/h8267.md): but I will [śîaḥ](../../strongs/h/h7878.md) in thy [piqquwd](../../strongs/h/h6490.md).

<a name="psalms_119_79"></a>Psalms 119:79

Let those that [yārē'](../../strongs/h/h3373.md) thee [shuwb](../../strongs/h/h7725.md) unto me, and those that have [yada'](../../strongs/h/h3045.md) [yada'](../../strongs/h/h3045.md) thy [ʿēḏâ](../../strongs/h/h5713.md).

<a name="psalms_119_80"></a>Psalms 119:80

Let my [leb](../../strongs/h/h3820.md) be [tamiym](../../strongs/h/h8549.md) in thy [choq](../../strongs/h/h2706.md); that I be not [buwsh](../../strongs/h/h954.md).

<a name="psalms_119_81"></a>Psalms 119:81

CAPH. My [nephesh](../../strongs/h/h5315.md) [kalah](../../strongs/h/h3615.md) for thy [tᵊšûʿâ](../../strongs/h/h8668.md): but I [yāḥal](../../strongs/h/h3176.md) in thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_82"></a>Psalms 119:82

Mine ['ayin](../../strongs/h/h5869.md) [kalah](../../strongs/h/h3615.md) for thy ['imrah](../../strongs/h/h565.md), ['āmar](../../strongs/h/h559.md), When wilt thou [nacham](../../strongs/h/h5162.md) me?

<a name="psalms_119_83"></a>Psalms 119:83

For I am become like a [nō'ḏ](../../strongs/h/h4997.md) in the [qîṭôr](../../strongs/h/h7008.md); yet do I not [shakach](../../strongs/h/h7911.md) thy [choq](../../strongs/h/h2706.md).

<a name="psalms_119_84"></a>Psalms 119:84

How many are the [yowm](../../strongs/h/h3117.md) of thy ['ebed](../../strongs/h/h5650.md)? when wilt thou ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) on them that [radaph](../../strongs/h/h7291.md) me?

<a name="psalms_119_85"></a>Psalms 119:85

The [zed](../../strongs/h/h2086.md) have [karah](../../strongs/h/h3738.md) [šîḥâ](../../strongs/h/h7882.md) for me, which are not after thy [towrah](../../strongs/h/h8451.md).

<a name="psalms_119_86"></a>Psalms 119:86

All thy [mitsvah](../../strongs/h/h4687.md) are ['ĕmûnâ](../../strongs/h/h530.md): they [radaph](../../strongs/h/h7291.md) me [sheqer](../../strongs/h/h8267.md); [ʿāzar](../../strongs/h/h5826.md) thou me.

<a name="psalms_119_87"></a>Psalms 119:87

They had [mᵊʿaṭ](../../strongs/h/h4592.md) [kalah](../../strongs/h/h3615.md) me upon ['erets](../../strongs/h/h776.md); but I ['azab](../../strongs/h/h5800.md) not thy [piqquwd](../../strongs/h/h6490.md).

<a name="psalms_119_88"></a>Psalms 119:88

[ḥāyâ](../../strongs/h/h2421.md) me after thy [checed](../../strongs/h/h2617.md); so shall I [shamar](../../strongs/h/h8104.md) the [ʿēḏûṯ](../../strongs/h/h5715.md) of thy [peh](../../strongs/h/h6310.md).

<a name="psalms_119_89"></a>Psalms 119:89

LAMED. For ['owlam](../../strongs/h/h5769.md), [Yĕhovah](../../strongs/h/h3068.md), thy [dabar](../../strongs/h/h1697.md) is [nāṣaḇ](../../strongs/h/h5324.md) in [shamayim](../../strongs/h/h8064.md).

<a name="psalms_119_90"></a>Psalms 119:90

Thy ['ĕmûnâ](../../strongs/h/h530.md) is unto [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md): thou hast [kuwn](../../strongs/h/h3559.md) the ['erets](../../strongs/h/h776.md), and it ['amad](../../strongs/h/h5975.md).

<a name="psalms_119_91"></a>Psalms 119:91

They ['amad](../../strongs/h/h5975.md) this [yowm](../../strongs/h/h3117.md) according to thine [mishpat](../../strongs/h/h4941.md): for all are thy ['ebed](../../strongs/h/h5650.md).

<a name="psalms_119_92"></a>Psalms 119:92

[lûlē'](../../strongs/h/h3884.md) thy [towrah](../../strongs/h/h8451.md) had been my [šaʿăšûʿîm](../../strongs/h/h8191.md), I should then have ['abad](../../strongs/h/h6.md) in mine ['oniy](../../strongs/h/h6040.md).

<a name="psalms_119_93"></a>Psalms 119:93

I will ['owlam](../../strongs/h/h5769.md) [shakach](../../strongs/h/h7911.md) thy [piqquwd](../../strongs/h/h6490.md): for with them thou hast [ḥāyâ](../../strongs/h/h2421.md) me.

<a name="psalms_119_94"></a>Psalms 119:94

I am thine, [yasha'](../../strongs/h/h3467.md) me; for I have [darash](../../strongs/h/h1875.md) thy [piqquwd](../../strongs/h/h6490.md).

<a name="psalms_119_95"></a>Psalms 119:95

The [rasha'](../../strongs/h/h7563.md) have [qāvâ](../../strongs/h/h6960.md) for me to ['abad](../../strongs/h/h6.md) me: but I will [bîn](../../strongs/h/h995.md) thy [ʿēḏâ](../../strongs/h/h5713.md).

<a name="psalms_119_96"></a>Psalms 119:96

I have [ra'ah](../../strongs/h/h7200.md) a [qēṣ](../../strongs/h/h7093.md) of all [tiḵlâ](../../strongs/h/h8502.md): but thy [mitsvah](../../strongs/h/h4687.md) is [me'od](../../strongs/h/h3966.md) [rāḥāḇ](../../strongs/h/h7342.md).

<a name="psalms_119_97"></a>Psalms 119:97

MEM. O how I ['ahab](../../strongs/h/h157.md) thy [towrah](../../strongs/h/h8451.md)! it is my [śîḥâ](../../strongs/h/h7881.md) all the [yowm](../../strongs/h/h3117.md).

<a name="psalms_119_98"></a>Psalms 119:98

Thou through thy [mitsvah](../../strongs/h/h4687.md) hast made me [ḥāḵam](../../strongs/h/h2449.md) than mine ['oyeb](../../strongs/h/h341.md): for they are ['owlam](../../strongs/h/h5769.md) with me.

<a name="psalms_119_99"></a>Psalms 119:99

I have more [sakal](../../strongs/h/h7919.md) than all my [lamad](../../strongs/h/h3925.md): for thy [ʿēḏûṯ](../../strongs/h/h5715.md) are my [śîḥâ](../../strongs/h/h7881.md).

<a name="psalms_119_100"></a>Psalms 119:100

I [bîn](../../strongs/h/h995.md) more than the [zāqēn](../../strongs/h/h2205.md), because I [nāṣar](../../strongs/h/h5341.md) thy [piqquwd](../../strongs/h/h6490.md).

<a name="psalms_119_101"></a>Psalms 119:101

I have [kālā'](../../strongs/h/h3607.md) my [regel](../../strongs/h/h7272.md) from every [ra'](../../strongs/h/h7451.md) ['orach](../../strongs/h/h734.md), that I might [shamar](../../strongs/h/h8104.md) thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_102"></a>Psalms 119:102

I have not [cuwr](../../strongs/h/h5493.md) from thy [mishpat](../../strongs/h/h4941.md): for thou hast [yārâ](../../strongs/h/h3384.md) me.

<a name="psalms_119_103"></a>Psalms 119:103

How [mālaṣ](../../strongs/h/h4452.md) are thy ['imrah](../../strongs/h/h565.md) unto my [ḥēḵ](../../strongs/h/h2441.md)! yea, sweeter than [dĕbash](../../strongs/h/h1706.md) to my [peh](../../strongs/h/h6310.md)!

<a name="psalms_119_104"></a>Psalms 119:104

Through thy [piqquwd](../../strongs/h/h6490.md) I get [bîn](../../strongs/h/h995.md): therefore I [sane'](../../strongs/h/h8130.md) every [sheqer](../../strongs/h/h8267.md) ['orach](../../strongs/h/h734.md).

<a name="psalms_119_105"></a>Psalms 119:105

NUN. Thy [dabar](../../strongs/h/h1697.md) is a [nîr](../../strongs/h/h5216.md) unto my [regel](../../strongs/h/h7272.md), and a ['owr](../../strongs/h/h216.md) unto my [nāṯîḇ](../../strongs/h/h5410.md).

<a name="psalms_119_106"></a>Psalms 119:106

I have [shaba'](../../strongs/h/h7650.md), and I will [quwm](../../strongs/h/h6965.md) it, that I will [shamar](../../strongs/h/h8104.md) thy [tsedeq](../../strongs/h/h6664.md) [mishpat](../../strongs/h/h4941.md).

<a name="psalms_119_107"></a>Psalms 119:107

I am [ʿānâ](../../strongs/h/h6031.md) [me'od](../../strongs/h/h3966.md): [ḥāyâ](../../strongs/h/h2421.md) me, [Yĕhovah](../../strongs/h/h3068.md), according unto thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_108"></a>Psalms 119:108

[ratsah](../../strongs/h/h7521.md), I beseech thee, the [nᵊḏāḇâ](../../strongs/h/h5071.md) of my [peh](../../strongs/h/h6310.md), [Yĕhovah](../../strongs/h/h3068.md), and [lamad](../../strongs/h/h3925.md) me thy [mishpat](../../strongs/h/h4941.md).

<a name="psalms_119_109"></a>Psalms 119:109

My [nephesh](../../strongs/h/h5315.md) is [tāmîḏ](../../strongs/h/h8548.md) in my [kaph](../../strongs/h/h3709.md): yet do I not [shakach](../../strongs/h/h7911.md) thy [towrah](../../strongs/h/h8451.md).

<a name="psalms_119_110"></a>Psalms 119:110

The [rasha'](../../strongs/h/h7563.md) have [nathan](../../strongs/h/h5414.md) a [paḥ](../../strongs/h/h6341.md) for me: yet I [tāʿâ](../../strongs/h/h8582.md) not from thy [piqquwd](../../strongs/h/h6490.md).

<a name="psalms_119_111"></a>Psalms 119:111

Thy [ʿēḏûṯ](../../strongs/h/h5715.md) have I taken as a [nāḥal](../../strongs/h/h5157.md) ['owlam](../../strongs/h/h5769.md): for they are the [śāśôn](../../strongs/h/h8342.md) of my [leb](../../strongs/h/h3820.md).

<a name="psalms_119_112"></a>Psalms 119:112

I have [natah](../../strongs/h/h5186.md) mine [leb](../../strongs/h/h3820.md) to ['asah](../../strongs/h/h6213.md) thy [choq](../../strongs/h/h2706.md) ['owlam](../../strongs/h/h5769.md), even unto the [ʿēqeḇ](../../strongs/h/h6118.md).

<a name="psalms_119_113"></a>Psalms 119:113

SAMECH. I [sane'](../../strongs/h/h8130.md) [sēʿēp̄](../../strongs/h/h5588.md): but thy [towrah](../../strongs/h/h8451.md) do I ['ahab](../../strongs/h/h157.md).

<a name="psalms_119_114"></a>Psalms 119:114

Thou art my [cether](../../strongs/h/h5643.md) and my [magen](../../strongs/h/h4043.md): I [yāḥal](../../strongs/h/h3176.md) in thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_115"></a>Psalms 119:115

[cuwr](../../strongs/h/h5493.md) from me, ye [ra'a'](../../strongs/h/h7489.md): for I will [nāṣar](../../strongs/h/h5341.md) the [mitsvah](../../strongs/h/h4687.md) of my ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_119_116"></a>Psalms 119:116

[camak](../../strongs/h/h5564.md) me according unto thy ['imrah](../../strongs/h/h565.md), that I may [ḥāyâ](../../strongs/h/h2421.md): and let me not be [buwsh](../../strongs/h/h954.md) of my [śēḇer](../../strongs/h/h7664.md).

<a name="psalms_119_117"></a>Psalms 119:117

Hold thou me [sāʿaḏ](../../strongs/h/h5582.md), and I shall be [yasha'](../../strongs/h/h3467.md): and I will have [šāʿâ](../../strongs/h/h8159.md) unto thy [choq](../../strongs/h/h2706.md) [tāmîḏ](../../strongs/h/h8548.md).

<a name="psalms_119_118"></a>Psalms 119:118

Thou hast [sālâ](../../strongs/h/h5541.md) all them that [šāḡâ](../../strongs/h/h7686.md) from thy [choq](../../strongs/h/h2706.md): for their [tārmâ](../../strongs/h/h8649.md) is [sheqer](../../strongs/h/h8267.md).

<a name="psalms_119_119"></a>Psalms 119:119

Thou [shabath](../../strongs/h/h7673.md) all the [rasha'](../../strongs/h/h7563.md) of the ['erets](../../strongs/h/h776.md) like [sîḡ](../../strongs/h/h5509.md): therefore I ['ahab](../../strongs/h/h157.md) thy [ʿēḏâ](../../strongs/h/h5713.md).

<a name="psalms_119_120"></a>Psalms 119:120

My [basar](../../strongs/h/h1320.md) [sāmar](../../strongs/h/h5568.md) for [paḥaḏ](../../strongs/h/h6343.md) of thee; and I am [yare'](../../strongs/h/h3372.md) of thy [mishpat](../../strongs/h/h4941.md).

<a name="psalms_119_121"></a>Psalms 119:121

AIN. I have ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) and [tsedeq](../../strongs/h/h6664.md): [yānaḥ](../../strongs/h/h3240.md) me not to mine [ʿāšaq](../../strongs/h/h6231.md).

<a name="psalms_119_122"></a>Psalms 119:122

Be [ʿāraḇ](../../strongs/h/h6148.md) for thy ['ebed](../../strongs/h/h5650.md) for [towb](../../strongs/h/h2896.md): let not the [zed](../../strongs/h/h2086.md) [ʿāšaq](../../strongs/h/h6231.md) me.

<a name="psalms_119_123"></a>Psalms 119:123

Mine ['ayin](../../strongs/h/h5869.md) [kalah](../../strongs/h/h3615.md) for thy [yĕshuw'ah](../../strongs/h/h3444.md), and for the ['imrah](../../strongs/h/h565.md) of thy [tsedeq](../../strongs/h/h6664.md).

<a name="psalms_119_124"></a>Psalms 119:124

['asah](../../strongs/h/h6213.md) with thy ['ebed](../../strongs/h/h5650.md) according unto thy [checed](../../strongs/h/h2617.md), and [lamad](../../strongs/h/h3925.md) me thy [choq](../../strongs/h/h2706.md).

<a name="psalms_119_125"></a>Psalms 119:125

I am thy ['ebed](../../strongs/h/h5650.md); give me [bîn](../../strongs/h/h995.md), that I may [yada'](../../strongs/h/h3045.md) thy [ʿēḏâ](../../strongs/h/h5713.md).

<a name="psalms_119_126"></a>Psalms 119:126

It is [ʿēṯ](../../strongs/h/h6256.md) for thee, [Yĕhovah](../../strongs/h/h3068.md), to ['asah](../../strongs/h/h6213.md): for they have [pārar](../../strongs/h/h6565.md) thy [towrah](../../strongs/h/h8451.md).

<a name="psalms_119_127"></a>Psalms 119:127

Therefore I ['ahab](../../strongs/h/h157.md) thy [mitsvah](../../strongs/h/h4687.md) above [zāhāḇ](../../strongs/h/h2091.md); yea, above [pāz](../../strongs/h/h6337.md).

<a name="psalms_119_128"></a>Psalms 119:128

Therefore I esteem all thy [piqquwd](../../strongs/h/h6490.md) concerning all things to be [yashar](../../strongs/h/h3474.md); and I [sane'](../../strongs/h/h8130.md) every [sheqer](../../strongs/h/h8267.md) ['orach](../../strongs/h/h734.md).

<a name="psalms_119_129"></a>Psalms 119:129

PE. Thy [ʿēḏûṯ](../../strongs/h/h5715.md) are [pele'](../../strongs/h/h6382.md): therefore doth my [nephesh](../../strongs/h/h5315.md) [nāṣar](../../strongs/h/h5341.md) them.

<a name="psalms_119_130"></a>Psalms 119:130

The [pēṯaḥ](../../strongs/h/h6608.md) of thy [dabar](../../strongs/h/h1697.md) giveth ['owr](../../strongs/h/h215.md); it giveth [bîn](../../strongs/h/h995.md) unto the [pᵊṯî](../../strongs/h/h6612.md).

<a name="psalms_119_131"></a>Psalms 119:131

I [pāʿar](../../strongs/h/h6473.md) my [peh](../../strongs/h/h6310.md), and [šā'ap̄](../../strongs/h/h7602.md): for I [yā'aḇ](../../strongs/h/h2968.md) for thy [mitsvah](../../strongs/h/h4687.md).

<a name="psalms_119_132"></a>Psalms 119:132

[panah](../../strongs/h/h6437.md) thou upon me, and be [chanan](../../strongs/h/h2603.md) unto me, as thou [mishpat](../../strongs/h/h4941.md) to do unto those that ['ahab](../../strongs/h/h157.md) thy [shem](../../strongs/h/h8034.md).

<a name="psalms_119_133"></a>Psalms 119:133

[kuwn](../../strongs/h/h3559.md) my [pa'am](../../strongs/h/h6471.md) in thy ['imrah](../../strongs/h/h565.md): and let not any ['aven](../../strongs/h/h205.md) have [šālaṭ](../../strongs/h/h7980.md) over me.

<a name="psalms_119_134"></a>Psalms 119:134

[pāḏâ](../../strongs/h/h6299.md) me from the [ʿōšeq](../../strongs/h/h6233.md) of ['āḏām](../../strongs/h/h120.md): so will I [shamar](../../strongs/h/h8104.md) thy [piqquwd](../../strongs/h/h6490.md).

<a name="psalms_119_135"></a>Psalms 119:135

Make thy [paniym](../../strongs/h/h6440.md) to ['owr](../../strongs/h/h215.md) upon thy ['ebed](../../strongs/h/h5650.md); and [lamad](../../strongs/h/h3925.md) me thy [choq](../../strongs/h/h2706.md).

<a name="psalms_119_136"></a>Psalms 119:136

[peleḡ](../../strongs/h/h6388.md) of [mayim](../../strongs/h/h4325.md) run [yarad](../../strongs/h/h3381.md) mine ['ayin](../../strongs/h/h5869.md), because they [shamar](../../strongs/h/h8104.md) not thy [towrah](../../strongs/h/h8451.md).

<a name="psalms_119_137"></a>Psalms 119:137

TZADDI. [tsaddiyq](../../strongs/h/h6662.md) art thou, [Yĕhovah](../../strongs/h/h3068.md), and [yashar](../../strongs/h/h3477.md) are thy [mishpat](../../strongs/h/h4941.md).

<a name="psalms_119_138"></a>Psalms 119:138

Thy [ʿēḏâ](../../strongs/h/h5713.md) that thou hast [tsavah](../../strongs/h/h6680.md) are [tsedeq](../../strongs/h/h6664.md) and [me'od](../../strongs/h/h3966.md) ['ĕmûnâ](../../strongs/h/h530.md).

<a name="psalms_119_139"></a>Psalms 119:139

My [qin'â](../../strongs/h/h7068.md) hath [ṣāmaṯ](../../strongs/h/h6789.md) me, because mine [tsar](../../strongs/h/h6862.md) have [shakach](../../strongs/h/h7911.md) thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_140"></a>Psalms 119:140

Thy ['imrah](../../strongs/h/h565.md) is [me'od](../../strongs/h/h3966.md) [tsaraph](../../strongs/h/h6884.md): therefore thy ['ebed](../../strongs/h/h5650.md) ['ahab](../../strongs/h/h157.md) it.

<a name="psalms_119_141"></a>Psalms 119:141

I am [ṣāʿîr](../../strongs/h/h6810.md) and [bazah](../../strongs/h/h959.md): yet do not I [shakach](../../strongs/h/h7911.md) thy [piqquwd](../../strongs/h/h6490.md).

<a name="psalms_119_142"></a>Psalms 119:142

Thy [tsedaqah](../../strongs/h/h6666.md) is an ['owlam](../../strongs/h/h5769.md) [tsedeq](../../strongs/h/h6664.md), and thy [towrah](../../strongs/h/h8451.md) is the ['emeth](../../strongs/h/h571.md).

<a name="psalms_119_143"></a>Psalms 119:143

[tsar](../../strongs/h/h6862.md) and [māṣôq](../../strongs/h/h4689.md) have taken [māṣā'](../../strongs/h/h4672.md) on me: yet thy [mitsvah](../../strongs/h/h4687.md) are my [šaʿăšûʿîm](../../strongs/h/h8191.md).

<a name="psalms_119_144"></a>Psalms 119:144

The [tsedeq](../../strongs/h/h6664.md) of thy [ʿēḏûṯ](../../strongs/h/h5715.md) is ['owlam](../../strongs/h/h5769.md): give me [bîn](../../strongs/h/h995.md), and I shall [ḥāyâ](../../strongs/h/h2421.md).

<a name="psalms_119_145"></a>Psalms 119:145

KOPH. I [qara'](../../strongs/h/h7121.md) with my whole [leb](../../strongs/h/h3820.md); ['anah](../../strongs/h/h6030.md) me, [Yĕhovah](../../strongs/h/h3068.md): I will [nāṣar](../../strongs/h/h5341.md) thy [choq](../../strongs/h/h2706.md).

<a name="psalms_119_146"></a>Psalms 119:146

I [qara'](../../strongs/h/h7121.md) unto thee; [yasha'](../../strongs/h/h3467.md) me, and I shall [shamar](../../strongs/h/h8104.md) thy [ʿēḏâ](../../strongs/h/h5713.md).

<a name="psalms_119_147"></a>Psalms 119:147

I [qadam](../../strongs/h/h6923.md) the dawning of the [nešep̄](../../strongs/h/h5399.md), and [šāvaʿ](../../strongs/h/h7768.md): I [yāḥal](../../strongs/h/h3176.md) in thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_148"></a>Psalms 119:148

Mine ['ayin](../../strongs/h/h5869.md) [qadam](../../strongs/h/h6923.md) the night ['ašmurâ](../../strongs/h/h821.md), that I might [śîaḥ](../../strongs/h/h7878.md) in thy ['imrah](../../strongs/h/h565.md).

<a name="psalms_119_149"></a>Psalms 119:149

[shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md) according unto thy [checed](../../strongs/h/h2617.md): [Yĕhovah](../../strongs/h/h3068.md), [ḥāyâ](../../strongs/h/h2421.md) me according to thy [mishpat](../../strongs/h/h4941.md).

<a name="psalms_119_150"></a>Psalms 119:150

They draw [qāraḇ](../../strongs/h/h7126.md) that [radaph](../../strongs/h/h7291.md) after [zimmâ](../../strongs/h/h2154.md): they are [rachaq](../../strongs/h/h7368.md) from thy [towrah](../../strongs/h/h8451.md).

<a name="psalms_119_151"></a>Psalms 119:151

Thou art [qarowb](../../strongs/h/h7138.md), [Yĕhovah](../../strongs/h/h3068.md); and all thy [mitsvah](../../strongs/h/h4687.md) are ['emeth](../../strongs/h/h571.md).

<a name="psalms_119_152"></a>Psalms 119:152

Concerning thy [ʿēḏâ](../../strongs/h/h5713.md), I have [yada'](../../strongs/h/h3045.md) of [qeḏem](../../strongs/h/h6924.md) that thou hast [yacad](../../strongs/h/h3245.md) them ['owlam](../../strongs/h/h5769.md).

<a name="psalms_119_153"></a>Psalms 119:153

RESH. [ra'ah](../../strongs/h/h7200.md) mine ['oniy](../../strongs/h/h6040.md), and [chalats](../../strongs/h/h2502.md) me: for I do not [shakach](../../strongs/h/h7911.md) thy [towrah](../../strongs/h/h8451.md).

<a name="psalms_119_154"></a>Psalms 119:154

[riyb](../../strongs/h/h7378.md) my [rîḇ](../../strongs/h/h7379.md), and [gā'al](../../strongs/h/h1350.md) me: [ḥāyâ](../../strongs/h/h2421.md) me according to thy ['imrah](../../strongs/h/h565.md).

<a name="psalms_119_155"></a>Psalms 119:155

[yĕshuw'ah](../../strongs/h/h3444.md) is [rachowq](../../strongs/h/h7350.md) from the [rasha'](../../strongs/h/h7563.md): for they [darash](../../strongs/h/h1875.md) not thy [choq](../../strongs/h/h2706.md).

<a name="psalms_119_156"></a>Psalms 119:156

[rab](../../strongs/h/h7227.md) are thy [raḥam](../../strongs/h/h7356.md), [Yĕhovah](../../strongs/h/h3068.md): [ḥāyâ](../../strongs/h/h2421.md) me according to thy [mishpat](../../strongs/h/h4941.md).

<a name="psalms_119_157"></a>Psalms 119:157

[rab](../../strongs/h/h7227.md) are my [radaph](../../strongs/h/h7291.md) and mine [tsar](../../strongs/h/h6862.md); yet do I not [natah](../../strongs/h/h5186.md) from thy [ʿēḏûṯ](../../strongs/h/h5715.md).

<a name="psalms_119_158"></a>Psalms 119:158

I [ra'ah](../../strongs/h/h7200.md) the [bāḡaḏ](../../strongs/h/h898.md), and was [qûṭ](../../strongs/h/h6962.md); because they [shamar](../../strongs/h/h8104.md) not thy ['imrah](../../strongs/h/h565.md).

<a name="psalms_119_159"></a>Psalms 119:159

[ra'ah](../../strongs/h/h7200.md) how I ['ahab](../../strongs/h/h157.md) thy [piqquwd](../../strongs/h/h6490.md): [ḥāyâ](../../strongs/h/h2421.md) me, [Yĕhovah](../../strongs/h/h3068.md), according to thy [checed](../../strongs/h/h2617.md).

<a name="psalms_119_160"></a>Psalms 119:160

Thy [dabar](../../strongs/h/h1697.md) is ['emeth](../../strongs/h/h571.md) from the [ro'sh](../../strongs/h/h7218.md): and every one of thy [tsedeq](../../strongs/h/h6664.md) [mishpat](../../strongs/h/h4941.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_119_161"></a>Psalms 119:161

SCHIN. [śar](../../strongs/h/h8269.md) have [radaph](../../strongs/h/h7291.md) me without a [ḥinnām](../../strongs/h/h2600.md): but my [leb](../../strongs/h/h3820.md) standeth in [pachad](../../strongs/h/h6342.md) of thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_162"></a>Psalms 119:162

I [śûś](../../strongs/h/h7797.md) at thy ['imrah](../../strongs/h/h565.md), as one that [māṣā'](../../strongs/h/h4672.md) [rab](../../strongs/h/h7227.md) [šālāl](../../strongs/h/h7998.md).

<a name="psalms_119_163"></a>Psalms 119:163

I [sane'](../../strongs/h/h8130.md) and [ta'ab](../../strongs/h/h8581.md) [sheqer](../../strongs/h/h8267.md): but thy [towrah](../../strongs/h/h8451.md) do I ['ahab](../../strongs/h/h157.md).

<a name="psalms_119_164"></a>Psalms 119:164

[šeḇaʿ](../../strongs/h/h7651.md) a [yowm](../../strongs/h/h3117.md) do I [halal](../../strongs/h/h1984.md) thee because of thy [tsedeq](../../strongs/h/h6664.md) [mishpat](../../strongs/h/h4941.md).

<a name="psalms_119_165"></a>Psalms 119:165

[rab](../../strongs/h/h7227.md) [shalowm](../../strongs/h/h7965.md) have they which ['ahab](../../strongs/h/h157.md) thy [towrah](../../strongs/h/h8451.md): and nothing shall [miḵšôl](../../strongs/h/h4383.md) them.

<a name="psalms_119_166"></a>Psalms 119:166

[Yĕhovah](../../strongs/h/h3068.md), I have [śāḇar](../../strongs/h/h7663.md) for thy [yĕshuw'ah](../../strongs/h/h3444.md), and ['asah](../../strongs/h/h6213.md) thy [mitsvah](../../strongs/h/h4687.md).

<a name="psalms_119_167"></a>Psalms 119:167

My [nephesh](../../strongs/h/h5315.md) hath [shamar](../../strongs/h/h8104.md) thy [ʿēḏâ](../../strongs/h/h5713.md); and I ['ahab](../../strongs/h/h157.md) them [me'od](../../strongs/h/h3966.md).

<a name="psalms_119_168"></a>Psalms 119:168

I have [shamar](../../strongs/h/h8104.md) thy [piqquwd](../../strongs/h/h6490.md) and thy [ʿēḏâ](../../strongs/h/h5713.md): for all my [derek](../../strongs/h/h1870.md) are before thee.

<a name="psalms_119_169"></a>Psalms 119:169

TAU. Let my [rinnah](../../strongs/h/h7440.md) [qāraḇ](../../strongs/h/h7126.md) [paniym](../../strongs/h/h6440.md) thee, [Yĕhovah](../../strongs/h/h3068.md): give me [bîn](../../strongs/h/h995.md) according to thy [dabar](../../strongs/h/h1697.md).

<a name="psalms_119_170"></a>Psalms 119:170

Let my [tĕchinnah](../../strongs/h/h8467.md) [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) thee: [natsal](../../strongs/h/h5337.md) me according to thy ['imrah](../../strongs/h/h565.md).

<a name="psalms_119_171"></a>Psalms 119:171

My [saphah](../../strongs/h/h8193.md) shall [nāḇaʿ](../../strongs/h/h5042.md) [tehillah](../../strongs/h/h8416.md), when thou hast [lamad](../../strongs/h/h3925.md) me thy [choq](../../strongs/h/h2706.md).

<a name="psalms_119_172"></a>Psalms 119:172

My [lashown](../../strongs/h/h3956.md) shall ['anah](../../strongs/h/h6030.md) of thy ['imrah](../../strongs/h/h565.md): for all thy [mitsvah](../../strongs/h/h4687.md) are [tsedeq](../../strongs/h/h6664.md).

<a name="psalms_119_173"></a>Psalms 119:173

Let thine [yad](../../strongs/h/h3027.md) [ʿāzar](../../strongs/h/h5826.md) me; for I have [bāḥar](../../strongs/h/h977.md) thy [piqquwd](../../strongs/h/h6490.md).

<a name="psalms_119_174"></a>Psalms 119:174

I have [tā'aḇ](../../strongs/h/h8373.md) for thy [yĕshuw'ah](../../strongs/h/h3444.md), [Yĕhovah](../../strongs/h/h3068.md); and thy [towrah](../../strongs/h/h8451.md) is my [šaʿăšûʿîm](../../strongs/h/h8191.md).

<a name="psalms_119_175"></a>Psalms 119:175

Let my [nephesh](../../strongs/h/h5315.md) [ḥāyâ](../../strongs/h/h2421.md), and it shall [halal](../../strongs/h/h1984.md) thee; and let thy [mishpat](../../strongs/h/h4941.md) [ʿāzar](../../strongs/h/h5826.md) me.

<a name="psalms_119_176"></a>Psalms 119:176

I have [tāʿâ](../../strongs/h/h8582.md) like an ['abad](../../strongs/h/h6.md) [śê](../../strongs/h/h7716.md); [bāqaš](../../strongs/h/h1245.md) thy ['ebed](../../strongs/h/h5650.md); for I do not [shakach](../../strongs/h/h7911.md) thy [mitsvah](../../strongs/h/h4687.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 118](psalms_118.md) - [Psalms 120](psalms_120.md)