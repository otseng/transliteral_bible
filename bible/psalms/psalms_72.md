# [Psalms 72](https://www.blueletterbible.org/kjv/psa/72)

<a name="psalms_72_1"></a>Psalms 72:1

A Psalm for [Šᵊlōmô](../../strongs/h/h8010.md). [nathan](../../strongs/h/h5414.md) the [melek](../../strongs/h/h4428.md) thy [mishpat](../../strongs/h/h4941.md), ['Elohiym](../../strongs/h/h430.md), and thy [tsedaqah](../../strongs/h/h6666.md) unto the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md).

<a name="psalms_72_2"></a>Psalms 72:2

He shall [diyn](../../strongs/h/h1777.md) thy ['am](../../strongs/h/h5971.md) with [tsedeq](../../strongs/h/h6664.md), and thy ['aniy](../../strongs/h/h6041.md) with [mishpat](../../strongs/h/h4941.md).

<a name="psalms_72_3"></a>Psalms 72:3

The [har](../../strongs/h/h2022.md) shall [nasa'](../../strongs/h/h5375.md) [shalowm](../../strongs/h/h7965.md) to the ['am](../../strongs/h/h5971.md), and the little [giḇʿâ](../../strongs/h/h1389.md), by [tsedaqah](../../strongs/h/h6666.md).

<a name="psalms_72_4"></a>Psalms 72:4

He shall [shaphat](../../strongs/h/h8199.md) the ['aniy](../../strongs/h/h6041.md) of the ['am](../../strongs/h/h5971.md), he shall [yasha'](../../strongs/h/h3467.md) the [ben](../../strongs/h/h1121.md) of the ['ebyown](../../strongs/h/h34.md), and shall break in [dāḵā'](../../strongs/h/h1792.md) the [ʿāšaq](../../strongs/h/h6231.md).

<a name="psalms_72_5"></a>Psalms 72:5

They shall [yare'](../../strongs/h/h3372.md) thee as long [ʿim](../../strongs/h/h5973.md) the [šemeš](../../strongs/h/h8121.md) and [yareach](../../strongs/h/h3394.md) [paniym](../../strongs/h/h6440.md), throughout [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md).

<a name="psalms_72_6"></a>Psalms 72:6

He shall [yarad](../../strongs/h/h3381.md) like [māṭār](../../strongs/h/h4306.md) upon the [gēz](../../strongs/h/h1488.md): as [rᵊḇîḇîm](../../strongs/h/h7241.md) that [zarzîp̄](../../strongs/h/h2222.md) the ['erets](../../strongs/h/h776.md).

<a name="psalms_72_7"></a>Psalms 72:7

In his [yowm](../../strongs/h/h3117.md) shall the [tsaddiyq](../../strongs/h/h6662.md) [pāraḥ](../../strongs/h/h6524.md); and [rōḇ](../../strongs/h/h7230.md) of [shalowm](../../strongs/h/h7965.md) so long as the [yareach](../../strongs/h/h3394.md) endureth.

<a name="psalms_72_8"></a>Psalms 72:8

He shall have [radah](../../strongs/h/h7287.md) also from [yam](../../strongs/h/h3220.md) to [yam](../../strongs/h/h3220.md), and from the [nāhār](../../strongs/h/h5104.md) unto the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md).

<a name="psalms_72_9"></a>Psalms 72:9

They that dwell in the [ṣîyî](../../strongs/h/h6728.md) shall [kara'](../../strongs/h/h3766.md) [paniym](../../strongs/h/h6440.md) him; and his ['oyeb](../../strongs/h/h341.md) shall [lāḥaḵ](../../strongs/h/h3897.md) the ['aphar](../../strongs/h/h6083.md).

<a name="psalms_72_10"></a>Psalms 72:10

The [melek](../../strongs/h/h4428.md) of [Taršîš](../../strongs/h/h8659.md) and of the ['î](../../strongs/h/h339.md) shall [shuwb](../../strongs/h/h7725.md) [minchah](../../strongs/h/h4503.md): the [melek](../../strongs/h/h4428.md) of [Šᵊḇā'](../../strongs/h/h7614.md) and [Sᵊḇā'](../../strongs/h/h5434.md) shall [qāraḇ](../../strongs/h/h7126.md) ['eškār](../../strongs/h/h814.md).

<a name="psalms_72_11"></a>Psalms 72:11

Yea, all [melek](../../strongs/h/h4428.md) shall [shachah](../../strongs/h/h7812.md) before him: all [gowy](../../strongs/h/h1471.md) shall ['abad](../../strongs/h/h5647.md) him.

<a name="psalms_72_12"></a>Psalms 72:12

For he shall [natsal](../../strongs/h/h5337.md) the ['ebyown](../../strongs/h/h34.md) when he [šāvaʿ](../../strongs/h/h7768.md); the ['aniy](../../strongs/h/h6041.md) also, and him that hath no [ʿāzar](../../strongs/h/h5826.md).

<a name="psalms_72_13"></a>Psalms 72:13

He shall [ḥûs](../../strongs/h/h2347.md) the [dal](../../strongs/h/h1800.md) and ['ebyown](../../strongs/h/h34.md), and shall [yasha'](../../strongs/h/h3467.md) the [nephesh](../../strongs/h/h5315.md) of the ['ebyown](../../strongs/h/h34.md).

<a name="psalms_72_14"></a>Psalms 72:14

He shall [gā'al](../../strongs/h/h1350.md) their [nephesh](../../strongs/h/h5315.md) from [tok](../../strongs/h/h8496.md) and [chamac](../../strongs/h/h2555.md): and [yāqar](../../strongs/h/h3365.md) shall their [dam](../../strongs/h/h1818.md) be in his ['ayin](../../strongs/h/h5869.md).

<a name="psalms_72_15"></a>Psalms 72:15

And he shall [ḥāyâ](../../strongs/h/h2421.md), and to him shall be [nathan](../../strongs/h/h5414.md) of the [zāhāḇ](../../strongs/h/h2091.md) of [Šᵊḇā'](../../strongs/h/h7614.md): prayer also shall be [palal](../../strongs/h/h6419.md) for him [tāmîḏ](../../strongs/h/h8548.md); and [yowm](../../strongs/h/h3117.md) shall he be [barak](../../strongs/h/h1288.md).

<a name="psalms_72_16"></a>Psalms 72:16

There shall be a [pissâ](../../strongs/h/h6451.md) of [bar](../../strongs/h/h1250.md) in the ['erets](../../strongs/h/h776.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md); the [pĕriy](../../strongs/h/h6529.md) thereof shall [rāʿaš](../../strongs/h/h7493.md) like [Lᵊḇānôn](../../strongs/h/h3844.md): and they of the [ʿîr](../../strongs/h/h5892.md) shall [tsuwts](../../strongs/h/h6692.md) like ['eseb](../../strongs/h/h6212.md) of the ['erets](../../strongs/h/h776.md).

<a name="psalms_72_17"></a>Psalms 72:17

His [shem](../../strongs/h/h8034.md) shall endure for ['owlam](../../strongs/h/h5769.md): his [shem](../../strongs/h/h8034.md) shall be [nûn](../../strongs/h/h5125.md) [nûn](../../strongs/h/h5125.md) as long [paniym](../../strongs/h/h6440.md) the [šemeš](../../strongs/h/h8121.md): and men shall be [barak](../../strongs/h/h1288.md) in him: all [gowy](../../strongs/h/h1471.md) shall call him ['āšar](../../strongs/h/h833.md).

<a name="psalms_72_18"></a>Psalms 72:18

[barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), who only ['asah](../../strongs/h/h6213.md) [pala'](../../strongs/h/h6381.md).

<a name="psalms_72_19"></a>Psalms 72:19

And [barak](../../strongs/h/h1288.md) be his [kabowd](../../strongs/h/h3519.md) [shem](../../strongs/h/h8034.md) for ['owlam](../../strongs/h/h5769.md): and let the ['erets](../../strongs/h/h776.md) be [mālā'](../../strongs/h/h4390.md) with his [kabowd](../../strongs/h/h3519.md); ['amen](../../strongs/h/h543.md), and ['amen](../../strongs/h/h543.md).

<a name="psalms_72_20"></a>Psalms 72:20

The [tĕphillah](../../strongs/h/h8605.md) of [Dāviḏ](../../strongs/h/h1732.md) the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md) are [kalah](../../strongs/h/h3615.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 71](psalms_71.md) - [Psalms 73](psalms_73.md)