# [Psalms 124](https://www.blueletterbible.org/kjv/psalms/124)

<a name="psalms_124_1"></a>Psalms 124:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md) of [Dāviḏ](../../strongs/h/h1732.md). [lûlē'](../../strongs/h/h3884.md) it had not been [Yĕhovah](../../strongs/h/h3068.md) who was on our side, now may [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md);

<a name="psalms_124_2"></a>Psalms 124:2

[lûlē'](../../strongs/h/h3884.md) it had not been [Yĕhovah](../../strongs/h/h3068.md) who was on our side, when ['āḏām](../../strongs/h/h120.md) [quwm](../../strongs/h/h6965.md) against us:

<a name="psalms_124_3"></a>Psalms 124:3

['ăzay](../../strongs/h/h233.md) they had [bālaʿ](../../strongs/h/h1104.md) us [chay](../../strongs/h/h2416.md), when their ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md) against us:

<a name="psalms_124_4"></a>Psalms 124:4

['ăzay](../../strongs/h/h233.md) the [mayim](../../strongs/h/h4325.md) had [šāṭap̄](../../strongs/h/h7857.md) us, the [nachal](../../strongs/h/h5158.md) had gone ['abar](../../strongs/h/h5674.md) our [nephesh](../../strongs/h/h5315.md):

<a name="psalms_124_5"></a>Psalms 124:5

['ăzay](../../strongs/h/h233.md) the [zêḏôn](../../strongs/h/h2121.md) [mayim](../../strongs/h/h4325.md) had ['abar](../../strongs/h/h5674.md) our [nephesh](../../strongs/h/h5315.md).

<a name="psalms_124_6"></a>Psalms 124:6

[barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md), who hath not [nathan](../../strongs/h/h5414.md) us as a [ṭerep̄](../../strongs/h/h2964.md) to their [šēn](../../strongs/h/h8127.md).

<a name="psalms_124_7"></a>Psalms 124:7

Our [nephesh](../../strongs/h/h5315.md) is [mālaṭ](../../strongs/h/h4422.md) as a [tsippowr](../../strongs/h/h6833.md) out of the [paḥ](../../strongs/h/h6341.md) of the [yāqōš](../../strongs/h/h3369.md): the [paḥ](../../strongs/h/h6341.md) is [shabar](../../strongs/h/h7665.md), and we are [mālaṭ](../../strongs/h/h4422.md).

<a name="psalms_124_8"></a>Psalms 124:8

Our ['ezer](../../strongs/h/h5828.md) is in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), who ['asah](../../strongs/h/h6213.md) [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 123](psalms_123.md) - [Psalms 125](psalms_125.md)