# [Psalms 126](https://www.blueletterbible.org/kjv/psalms/126)

<a name="psalms_126_1"></a>Psalms 126:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md). When [Yĕhovah](../../strongs/h/h3068.md) [shuwb](../../strongs/h/h7725.md) the [šîḇâ](../../strongs/h/h7870.md) of [Tsiyown](../../strongs/h/h6726.md), we were like them that [ḥālam](../../strongs/h/h2492.md).

<a name="psalms_126_2"></a>Psalms 126:2

Then was our [peh](../../strongs/h/h6310.md) [mālā'](../../strongs/h/h4390.md) with [śᵊḥôq](../../strongs/h/h7814.md), and our [lashown](../../strongs/h/h3956.md) with [rinnah](../../strongs/h/h7440.md): then ['āmar](../../strongs/h/h559.md) they among the [gowy](../../strongs/h/h1471.md), [Yĕhovah](../../strongs/h/h3068.md) hath ['asah](../../strongs/h/h6213.md) [gāḏal](../../strongs/h/h1431.md) for them.

<a name="psalms_126_3"></a>Psalms 126:3

[Yĕhovah](../../strongs/h/h3068.md) hath ['asah](../../strongs/h/h6213.md) [gāḏal](../../strongs/h/h1431.md) for us; whereof we are [śāmēaḥ](../../strongs/h/h8056.md).

<a name="psalms_126_4"></a>Psalms 126:4

[shuwb](../../strongs/h/h7725.md) our [shebuwth](../../strongs/h/h7622.md) [shebuwth](../../strongs/h/h7622.md), [Yĕhovah](../../strongs/h/h3068.md), as the ['āp̄îq](../../strongs/h/h650.md) in the [neḡeḇ](../../strongs/h/h5045.md).

<a name="psalms_126_5"></a>Psalms 126:5

They that [zāraʿ](../../strongs/h/h2232.md) in [dim'ah](../../strongs/h/h1832.md) shall [qāṣar](../../strongs/h/h7114.md) in [rinnah](../../strongs/h/h7440.md).

<a name="psalms_126_6"></a>Psalms 126:6

He that [yālaḵ](../../strongs/h/h3212.md) [halak](../../strongs/h/h1980.md) and [bāḵâ](../../strongs/h/h1058.md), [nasa'](../../strongs/h/h5375.md) [Mešeḵ](../../strongs/h/h4901.md) [zera'](../../strongs/h/h2233.md), shall [bow'](../../strongs/h/h935.md) [bow'](../../strongs/h/h935.md) again with [rinnah](../../strongs/h/h7440.md), [nasa'](../../strongs/h/h5375.md) his ['ălummâ](../../strongs/h/h485.md) with him.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 125](psalms_125.md) - [Psalms 127](psalms_127.md)