# [Psalms 65](https://www.blueletterbible.org/kjv/psa/65)

<a name="psalms_65_1"></a>Psalms 65:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) [and] [šîr](../../strongs/h/h7892.md) of [Dāviḏ](../../strongs/h/h1732.md). [tehillah](../../strongs/h/h8416.md) [dûmîyâ](../../strongs/h/h1747.md) for thee, ['Elohiym](../../strongs/h/h430.md), in [Tsiyown](../../strongs/h/h6726.md): and unto thee shall the [neḏer](../../strongs/h/h5088.md) be [shalam](../../strongs/h/h7999.md).

<a name="psalms_65_2"></a>Psalms 65:2

O thou that [shama'](../../strongs/h/h8085.md) [tĕphillah](../../strongs/h/h8605.md), unto thee shall all [basar](../../strongs/h/h1320.md) [bow'](../../strongs/h/h935.md).

<a name="psalms_65_3"></a>Psalms 65:3

[dabar](../../strongs/h/h1697.md) ['avon](../../strongs/h/h5771.md) [gabar](../../strongs/h/h1396.md) against me: as for our [pesha'](../../strongs/h/h6588.md), thou shalt [kāp̄ar](../../strongs/h/h3722.md).

<a name="psalms_65_4"></a>Psalms 65:4

['esher](../../strongs/h/h835.md) is the man whom thou [bāḥar](../../strongs/h/h977.md), and causest to [qāraḇ](../../strongs/h/h7126.md) unto thee, that he may [shakan](../../strongs/h/h7931.md) in thy [ḥāṣēr](../../strongs/h/h2691.md): we shall be [sāׂbaʿ](../../strongs/h/h7646.md) with the [ṭûḇ](../../strongs/h/h2898.md) of thy [bayith](../../strongs/h/h1004.md), even of thy [qadowsh](../../strongs/h/h6918.md) [heykal](../../strongs/h/h1964.md).

<a name="psalms_65_5"></a>Psalms 65:5

By [yare'](../../strongs/h/h3372.md) in [tsedeq](../../strongs/h/h6664.md) wilt thou ['anah](../../strongs/h/h6030.md) us, ['Elohiym](../../strongs/h/h430.md) of our [yesha'](../../strongs/h/h3468.md); who art the [miḇṭāḥ](../../strongs/h/h4009.md) of all the [qeṣev](../../strongs/h/h7099.md) of the ['erets](../../strongs/h/h776.md), and of them that are [rachowq](../../strongs/h/h7350.md) upon the [yam](../../strongs/h/h3220.md):

<a name="psalms_65_6"></a>Psalms 65:6

Which by his [koach](../../strongs/h/h3581.md) [kuwn](../../strongs/h/h3559.md) the [har](../../strongs/h/h2022.md); being ['āzar](../../strongs/h/h247.md) with [gᵊḇûrâ](../../strongs/h/h1369.md):

<a name="psalms_65_7"></a>Psalms 65:7

Which [shabach](../../strongs/h/h7623.md) the [shā'ôn](../../strongs/h/h7588.md) of the [yam](../../strongs/h/h3220.md), the [shā'ôn](../../strongs/h/h7588.md) of their [gal](../../strongs/h/h1530.md), and the [hāmôn](../../strongs/h/h1995.md) of the [lĕom](../../strongs/h/h3816.md).

<a name="psalms_65_8"></a>Psalms 65:8

They also that [yashab](../../strongs/h/h3427.md) in the [qeṣev](../../strongs/h/h7099.md) are [yare'](../../strongs/h/h3372.md) at thy ['ôṯ](../../strongs/h/h226.md): thou makest the [môṣā'](../../strongs/h/h4161.md) of the [boqer](../../strongs/h/h1242.md) and ['ereb](../../strongs/h/h6153.md) to [ranan](../../strongs/h/h7442.md).

<a name="psalms_65_9"></a>Psalms 65:9

Thou [paqad](../../strongs/h/h6485.md) the ['erets](../../strongs/h/h776.md), and [šûq](../../strongs/h/h7783.md) it: thou [rab](../../strongs/h/h7227.md) [ʿāšar](../../strongs/h/h6238.md) it with the [peleḡ](../../strongs/h/h6388.md) of ['Elohiym](../../strongs/h/h430.md), which is [mālā'](../../strongs/h/h4390.md) of [mayim](../../strongs/h/h4325.md): thou [kuwn](../../strongs/h/h3559.md) them [dagan](../../strongs/h/h1715.md), when thou hast so [kuwn](../../strongs/h/h3559.md) for it.

<a name="psalms_65_10"></a>Psalms 65:10

Thou [rāvâ](../../strongs/h/h7301.md) the [telem](../../strongs/h/h8525.md) thereof [rāvâ](../../strongs/h/h7301.md): thou [nāḥaṯ](../../strongs/h/h5181.md) the [gᵊḏûḏ](../../strongs/h/h1417.md) [gᵊḏûḏâ](../../strongs/h/h1418.md) thereof: thou makest it [mûḡ](../../strongs/h/h4127.md) with [rᵊḇîḇîm](../../strongs/h/h7241.md): thou [barak](../../strongs/h/h1288.md) the [ṣemaḥ](../../strongs/h/h6780.md) thereof.

<a name="psalms_65_11"></a>Psalms 65:11

Thou ['atar](../../strongs/h/h5849.md) the [šānâ](../../strongs/h/h8141.md) with thy [towb](../../strongs/h/h2896.md); and thy [ma'gal](../../strongs/h/h4570.md) [rāʿap̄](../../strongs/h/h7491.md) [dešen](../../strongs/h/h1880.md).

<a name="psalms_65_12"></a>Psalms 65:12

They [rāʿap̄](../../strongs/h/h7491.md) upon the [nā'â](../../strongs/h/h4999.md) of the [midbar](../../strongs/h/h4057.md): and the [giḇʿâ](../../strongs/h/h1389.md) [gîl](../../strongs/h/h1524.md) on every [ḥāḡar](../../strongs/h/h2296.md).

<a name="psalms_65_13"></a>Psalms 65:13

The [kar](../../strongs/h/h3733.md) are [labash](../../strongs/h/h3847.md) with [tso'n](../../strongs/h/h6629.md); the [ʿēmeq](../../strongs/h/h6010.md) also are [ʿāṭap̄](../../strongs/h/h5848.md) with [bar](../../strongs/h/h1250.md); they [rûaʿ](../../strongs/h/h7321.md), they also [shiyr](../../strongs/h/h7891.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 64](psalms_64.md) - [Psalms 66](psalms_66.md)