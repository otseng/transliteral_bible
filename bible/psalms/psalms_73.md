# [Psalms 73](https://www.blueletterbible.org/kjv/psa/73)

<a name="psalms_73_1"></a>Psalms 73:1

A [mizmôr](../../strongs/h/h4210.md) of ['Āsāp̄](../../strongs/h/h623.md). Truly ['Elohiym](../../strongs/h/h430.md) is [towb](../../strongs/h/h2896.md) to [Yisra'el](../../strongs/h/h3478.md), even to such as are of a [bar](../../strongs/h/h1249.md) [lebab](../../strongs/h/h3824.md).

<a name="psalms_73_2"></a>Psalms 73:2

But as for me, my [regel](../../strongs/h/h7272.md) were [mᵊʿaṭ](../../strongs/h/h4592.md) [natah](../../strongs/h/h5186.md) [natah](../../strongs/h/h5186.md); my ['ashuwr](../../strongs/h/h838.md) had ['în](../../strongs/h/h369.md) [šāp̄aḵ](../../strongs/h/h8210.md).

<a name="psalms_73_3"></a>Psalms 73:3

For I was [qānā'](../../strongs/h/h7065.md) at the [halal](../../strongs/h/h1984.md), when I [ra'ah](../../strongs/h/h7200.md) the [shalowm](../../strongs/h/h7965.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="psalms_73_4"></a>Psalms 73:4

For there are no [ḥarṣubâ](../../strongs/h/h2784.md) in their [maveth](../../strongs/h/h4194.md): but their ['ûl](../../strongs/h/h193.md) is [bārî'](../../strongs/h/h1277.md).

<a name="psalms_73_5"></a>Psalms 73:5

They are not in ['amal](../../strongs/h/h5999.md) as other ['enowsh](../../strongs/h/h582.md); neither are they [naga'](../../strongs/h/h5060.md) [ʿim](../../strongs/h/h5973.md) other ['āḏām](../../strongs/h/h120.md).

<a name="psalms_73_6"></a>Psalms 73:6

Therefore [ga'avah](../../strongs/h/h1346.md) [ʿānaq](../../strongs/h/h6059.md) them; [chamac](../../strongs/h/h2555.md) [ʿāṭap̄](../../strongs/h/h5848.md) them as a [šîṯ](../../strongs/h/h7897.md).

<a name="psalms_73_7"></a>Psalms 73:7

Their ['ayin](../../strongs/h/h5869.md) [yāṣā'](../../strongs/h/h3318.md) with [cheleb](../../strongs/h/h2459.md): they have ['abar](../../strongs/h/h5674.md) than [lebab](../../strongs/h/h3824.md) could [maśkîṯ](../../strongs/h/h4906.md).

<a name="psalms_73_8"></a>Psalms 73:8

They are [mûq](../../strongs/h/h4167.md), and [dabar](../../strongs/h/h1696.md) [ra'](../../strongs/h/h7451.md) concerning [ʿōšeq](../../strongs/h/h6233.md): they [dabar](../../strongs/h/h1696.md) [marowm](../../strongs/h/h4791.md).

<a name="psalms_73_9"></a>Psalms 73:9

They [šāṯaṯ](../../strongs/h/h8371.md) their [peh](../../strongs/h/h6310.md) against the [shamayim](../../strongs/h/h8064.md), and their [lashown](../../strongs/h/h3956.md) [halak](../../strongs/h/h1980.md) through the ['erets](../../strongs/h/h776.md).

<a name="psalms_73_10"></a>Psalms 73:10

Therefore his ['am](../../strongs/h/h5971.md) [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) [hălōm](../../strongs/h/h1988.md): and [mayim](../../strongs/h/h4325.md) of a [mālē'](../../strongs/h/h4392.md) cup are [māṣâ](../../strongs/h/h4680.md) to them.

<a name="psalms_73_11"></a>Psalms 73:11

And they ['āmar](../../strongs/h/h559.md), How doth ['el](../../strongs/h/h410.md) [yada'](../../strongs/h/h3045.md) ? and is [yēš](../../strongs/h/h3426.md) [dēʿâ](../../strongs/h/h1844.md) in the ['elyown](../../strongs/h/h5945.md) ?

<a name="psalms_73_12"></a>Psalms 73:12

Behold, these are the [rasha'](../../strongs/h/h7563.md), who [šālēv](../../strongs/h/h7961.md) in the ['owlam](../../strongs/h/h5769.md); they [śāḡâ](../../strongs/h/h7685.md) in [ḥayil](../../strongs/h/h2428.md).

<a name="psalms_73_13"></a>Psalms 73:13

Verily I have [zāḵâ](../../strongs/h/h2135.md) my [lebab](../../strongs/h/h3824.md) in [riyq](../../strongs/h/h7385.md), and [rāḥaṣ](../../strongs/h/h7364.md) my [kaph](../../strongs/h/h3709.md) in [niqqāyôn](../../strongs/h/h5356.md).

<a name="psalms_73_14"></a>Psalms 73:14

For all the [yowm](../../strongs/h/h3117.md) long have I been [naga'](../../strongs/h/h5060.md), and [tôḵēḥâ](../../strongs/h/h8433.md) every [boqer](../../strongs/h/h1242.md).

<a name="psalms_73_15"></a>Psalms 73:15

If I ['āmar](../../strongs/h/h559.md), I will [sāp̄ar](../../strongs/h/h5608.md) [kᵊmô](../../strongs/h/h3644.md); behold, I should [bāḡaḏ](../../strongs/h/h898.md) against the [dôr](../../strongs/h/h1755.md) of thy [ben](../../strongs/h/h1121.md).

<a name="psalms_73_16"></a>Psalms 73:16

When I [chashab](../../strongs/h/h2803.md) to [yada'](../../strongs/h/h3045.md) this, it was too ['amal](../../strongs/h/h5999.md) for ['ayin](../../strongs/h/h5869.md);

<a name="psalms_73_17"></a>Psalms 73:17

Until I [bow'](../../strongs/h/h935.md) into the [miqdash](../../strongs/h/h4720.md) of ['el](../../strongs/h/h410.md); then [bîn](../../strongs/h/h995.md) I their ['aḥărîṯ](../../strongs/h/h319.md).

<a name="psalms_73_18"></a>Psalms 73:18

Surely thou didst [shiyth](../../strongs/h/h7896.md) them in [ḥelqâ](../../strongs/h/h2513.md): thou castedst them [naphal](../../strongs/h/h5307.md) into [maššû'â](../../strongs/h/h4876.md).

<a name="psalms_73_19"></a>Psalms 73:19

How are they brought into [šammâ](../../strongs/h/h8047.md), as in a [reḡaʿ](../../strongs/h/h7281.md)! they are [sûp̄](../../strongs/h/h5486.md) [tamam](../../strongs/h/h8552.md) with [ballāhâ](../../strongs/h/h1091.md).

<a name="psalms_73_20"></a>Psalms 73:20

As a [ḥălôm](../../strongs/h/h2472.md) when one [quwts](../../strongs/h/h6974.md); so, ['Adonay](../../strongs/h/h136.md), when thou [ʿûr](../../strongs/h/h5782.md), thou shalt [bazah](../../strongs/h/h959.md) their [tselem](../../strongs/h/h6754.md).

<a name="psalms_73_21"></a>Psalms 73:21

Thus my [lebab](../../strongs/h/h3824.md) was [ḥāmēṣ](../../strongs/h/h2556.md), and I was [šānan](../../strongs/h/h8150.md) in my [kilyah](../../strongs/h/h3629.md).

<a name="psalms_73_22"></a>Psalms 73:22

So [baʿar](../../strongs/h/h1198.md) was I, and [yada'](../../strongs/h/h3045.md): I was as a [bĕhemah](../../strongs/h/h929.md) before thee.

<a name="psalms_73_23"></a>Psalms 73:23

Nevertheless I am [tāmîḏ](../../strongs/h/h8548.md) with thee: thou hast ['āḥaz](../../strongs/h/h270.md) me by my [yamiyn](../../strongs/h/h3225.md) [yad](../../strongs/h/h3027.md).

<a name="psalms_73_24"></a>Psalms 73:24

Thou shalt [nachah](../../strongs/h/h5148.md) me with thy ['etsah](../../strongs/h/h6098.md), and ['aḥar](../../strongs/h/h310.md) [laqach](../../strongs/h/h3947.md) me to [kabowd](../../strongs/h/h3519.md).

<a name="psalms_73_25"></a>Psalms 73:25

Whom have I in [shamayim](../../strongs/h/h8064.md) but thee? and there is none upon ['erets](../../strongs/h/h776.md) that I [ḥāp̄ēṣ](../../strongs/h/h2654.md) beside thee.

<a name="psalms_73_26"></a>Psalms 73:26

My [šᵊ'ēr](../../strongs/h/h7607.md) and my [lebab](../../strongs/h/h3824.md) [kalah](../../strongs/h/h3615.md): but ['Elohiym](../../strongs/h/h430.md) is the [tsuwr](../../strongs/h/h6697.md) of my [lebab](../../strongs/h/h3824.md), and my [cheleq](../../strongs/h/h2506.md) for ['owlam](../../strongs/h/h5769.md).

<a name="psalms_73_27"></a>Psalms 73:27

For, lo, they that are [rāḥēq](../../strongs/h/h7369.md) from thee shall ['abad](../../strongs/h/h6.md): thou hast [ṣāmaṯ](../../strongs/h/h6789.md) all them that go a [zānâ](../../strongs/h/h2181.md) from thee.

<a name="psalms_73_28"></a>Psalms 73:28

['ănî](../../strongs/h/h589.md) it is [towb](../../strongs/h/h2896.md) for me to [qᵊrāḇâ](../../strongs/h/h7132.md) to ['Elohiym](../../strongs/h/h430.md): I have [shiyth](../../strongs/h/h7896.md) my [machaceh](../../strongs/h/h4268.md) in the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), that I may [sāp̄ar](../../strongs/h/h5608.md) all thy [mĕla'kah](../../strongs/h/h4399.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 72](psalms_72.md) - [Psalms 74](psalms_74.md)