# [Psalms 109](https://www.blueletterbible.org/kjv/psalms/109)

<a name="psalms_109_1"></a>Psalms 109:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). Hold not thy [ḥāraš](../../strongs/h/h2790.md), ['Elohiym](../../strongs/h/h430.md) of my [tehillah](../../strongs/h/h8416.md);

<a name="psalms_109_2"></a>Psalms 109:2

For the [peh](../../strongs/h/h6310.md) of the [rasha'](../../strongs/h/h7563.md) and the [peh](../../strongs/h/h6310.md) of the [mirmah](../../strongs/h/h4820.md) are [pāṯaḥ](../../strongs/h/h6605.md) against me: they have [dabar](../../strongs/h/h1696.md) against me with a [sheqer](../../strongs/h/h8267.md) [lashown](../../strongs/h/h3956.md).

<a name="psalms_109_3"></a>Psalms 109:3

They [cabab](../../strongs/h/h5437.md) me about also with [dabar](../../strongs/h/h1697.md) of [śin'â](../../strongs/h/h8135.md); and [lāḥam](../../strongs/h/h3898.md) against me without a [ḥinnām](../../strongs/h/h2600.md).

<a name="psalms_109_4"></a>Psalms 109:4

For my ['ahăḇâ](../../strongs/h/h160.md) they are my [śāṭan](../../strongs/h/h7853.md): but I give myself unto [tĕphillah](../../strongs/h/h8605.md).

<a name="psalms_109_5"></a>Psalms 109:5

And they have [śûm](../../strongs/h/h7760.md) me [ra'](../../strongs/h/h7451.md) for [towb](../../strongs/h/h2896.md), and [śin'â](../../strongs/h/h8135.md) for my ['ahăḇâ](../../strongs/h/h160.md).

<a name="psalms_109_6"></a>Psalms 109:6

[paqad](../../strongs/h/h6485.md) thou a [rasha'](../../strongs/h/h7563.md) over him: and let [satan](../../strongs/h/h7854.md) ['amad](../../strongs/h/h5975.md) at his [yamiyn](../../strongs/h/h3225.md).

<a name="psalms_109_7"></a>Psalms 109:7

When he shall be [shaphat](../../strongs/h/h8199.md), let him [yāṣā'](../../strongs/h/h3318.md) [rasha'](../../strongs/h/h7563.md): and let his [tĕphillah](../../strongs/h/h8605.md) become [ḥăṭā'â](../../strongs/h/h2401.md).

<a name="psalms_109_8"></a>Psalms 109:8

Let his [yowm](../../strongs/h/h3117.md) be [mᵊʿaṭ](../../strongs/h/h4592.md); and let ['aḥēr](../../strongs/h/h312.md) [laqach](../../strongs/h/h3947.md) his [pᵊqudâ](../../strongs/h/h6486.md).

<a name="psalms_109_9"></a>Psalms 109:9

Let his [ben](../../strongs/h/h1121.md) be [yathowm](../../strongs/h/h3490.md), and his ['ishshah](../../strongs/h/h802.md) an ['almānâ](../../strongs/h/h490.md).

<a name="psalms_109_10"></a>Psalms 109:10

Let his [ben](../../strongs/h/h1121.md) be [nûaʿ](../../strongs/h/h5128.md) [nûaʿ](../../strongs/h/h5128.md), and [sha'al](../../strongs/h/h7592.md): let them [darash](../../strongs/h/h1875.md) their bread also out of their [chorbah](../../strongs/h/h2723.md).

<a name="psalms_109_11"></a>Psalms 109:11

Let the [nāšâ](../../strongs/h/h5383.md) [nāqaš](../../strongs/h/h5367.md) all that he hath; and let the [zûr](../../strongs/h/h2114.md) [bāzaz](../../strongs/h/h962.md) his [yᵊḡîaʿ](../../strongs/h/h3018.md).

<a name="psalms_109_12"></a>Psalms 109:12

Let there be none to [mashak](../../strongs/h/h4900.md) [checed](../../strongs/h/h2617.md) unto him: neither let there be any to [chanan](../../strongs/h/h2603.md) his [yathowm](../../strongs/h/h3490.md).

<a name="psalms_109_13"></a>Psalms 109:13

Let his ['aḥărîṯ](../../strongs/h/h319.md) be [karath](../../strongs/h/h3772.md); and in the [dôr](../../strongs/h/h1755.md) ['aḥēr](../../strongs/h/h312.md) let their [shem](../../strongs/h/h8034.md) be [māḥâ](../../strongs/h/h4229.md).

<a name="psalms_109_14"></a>Psalms 109:14

Let the ['avon](../../strongs/h/h5771.md) of his ['ab](../../strongs/h/h1.md) be [zakar](../../strongs/h/h2142.md) with [Yĕhovah](../../strongs/h/h3068.md); and let not the [chatta'ath](../../strongs/h/h2403.md) of his ['em](../../strongs/h/h517.md) be [māḥâ](../../strongs/h/h4229.md).

<a name="psalms_109_15"></a>Psalms 109:15

Let them be before [Yĕhovah](../../strongs/h/h3068.md) [tāmîḏ](../../strongs/h/h8548.md), that he may [karath](../../strongs/h/h3772.md) the [zeker](../../strongs/h/h2143.md) of them from the ['erets](../../strongs/h/h776.md).

<a name="psalms_109_16"></a>Psalms 109:16

Because that he [zakar](../../strongs/h/h2142.md) not to ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md), but [radaph](../../strongs/h/h7291.md) the ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md) ['iysh](../../strongs/h/h376.md), that he might even [muwth](../../strongs/h/h4191.md) the [kā'â](../../strongs/h/h3512.md) in [lebab](../../strongs/h/h3824.md).

<a name="psalms_109_17"></a>Psalms 109:17

As he ['ahab](../../strongs/h/h157.md) [qᵊlālâ](../../strongs/h/h7045.md), so let it [bow'](../../strongs/h/h935.md) unto him: as he [ḥāp̄ēṣ](../../strongs/h/h2654.md) not in [bĕrakah](../../strongs/h/h1293.md), so let it be [rachaq](../../strongs/h/h7368.md) from him.

<a name="psalms_109_18"></a>Psalms 109:18

As he [labash](../../strongs/h/h3847.md) himself with [qᵊlālâ](../../strongs/h/h7045.md) like as with his [maḏ](../../strongs/h/h4055.md), so let it [bow'](../../strongs/h/h935.md) into his [qereḇ](../../strongs/h/h7130.md) like [mayim](../../strongs/h/h4325.md), and like [šemen](../../strongs/h/h8081.md) into his ['etsem](../../strongs/h/h6106.md).

<a name="psalms_109_19"></a>Psalms 109:19

Let it be unto him as the [beḡeḏ](../../strongs/h/h899.md) which [ʿāṭâ](../../strongs/h/h5844.md) him, and for a [mᵊzîaḥ](../../strongs/h/h4206.md) wherewith he is [ḥāḡar](../../strongs/h/h2296.md) [tāmîḏ](../../strongs/h/h8548.md).

<a name="psalms_109_20"></a>Psalms 109:20

Let this be the [pe'ullah](../../strongs/h/h6468.md) of mine [śāṭan](../../strongs/h/h7853.md) from [Yĕhovah](../../strongs/h/h3068.md), and of them that [dabar](../../strongs/h/h1696.md) [ra'](../../strongs/h/h7451.md) against my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_109_21"></a>Psalms 109:21

But ['asah](../../strongs/h/h6213.md) thou for me, O [Yᵊhōvâ](../../strongs/h/h3069.md) the ['adonay](../../strongs/h/h136.md), for thy [shem](../../strongs/h/h8034.md) sake: because thy [checed](../../strongs/h/h2617.md) is [towb](../../strongs/h/h2896.md), [natsal](../../strongs/h/h5337.md) thou me.

<a name="psalms_109_22"></a>Psalms 109:22

For I am ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md), and my [leb](../../strongs/h/h3820.md) is [ḥālal](../../strongs/h/h2490.md) [qereḇ](../../strongs/h/h7130.md) me.

<a name="psalms_109_23"></a>Psalms 109:23

I am [halak](../../strongs/h/h1980.md) like the [ṣēl](../../strongs/h/h6738.md) when it [natah](../../strongs/h/h5186.md): I am tossed up and [nāʿar](../../strongs/h/h5287.md) as the ['arbê](../../strongs/h/h697.md).

<a name="psalms_109_24"></a>Psalms 109:24

My [bereḵ](../../strongs/h/h1290.md) are [kashal](../../strongs/h/h3782.md) through [ṣôm](../../strongs/h/h6685.md); and my [basar](../../strongs/h/h1320.md) [kāḥaš](../../strongs/h/h3584.md) of [šemen](../../strongs/h/h8081.md).

<a name="psalms_109_25"></a>Psalms 109:25

I became also a [cherpah](../../strongs/h/h2781.md) unto them: when they [ra'ah](../../strongs/h/h7200.md) upon me they [nûaʿ](../../strongs/h/h5128.md) their [ro'sh](../../strongs/h/h7218.md).

<a name="psalms_109_26"></a>Psalms 109:26

[ʿāzar](../../strongs/h/h5826.md) me, [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md): O [yasha'](../../strongs/h/h3467.md) me according to thy [checed](../../strongs/h/h2617.md):

<a name="psalms_109_27"></a>Psalms 109:27

That they may [yada'](../../strongs/h/h3045.md) that this is thy [yad](../../strongs/h/h3027.md); that thou, [Yĕhovah](../../strongs/h/h3068.md), hast ['asah](../../strongs/h/h6213.md) it.

<a name="psalms_109_28"></a>Psalms 109:28

Let them [qālal](../../strongs/h/h7043.md), but [barak](../../strongs/h/h1288.md) thou: when they [quwm](../../strongs/h/h6965.md), let them be [buwsh](../../strongs/h/h954.md); but let thy ['ebed](../../strongs/h/h5650.md) [samach](../../strongs/h/h8055.md).

<a name="psalms_109_29"></a>Psalms 109:29

Let mine [śāṭan](../../strongs/h/h7853.md) be [labash](../../strongs/h/h3847.md) with [kĕlimmah](../../strongs/h/h3639.md), and let them [ʿāṭâ](../../strongs/h/h5844.md) themselves with their own [bšeṯ](../../strongs/h/h1322.md), as with a [mᵊʿîl](../../strongs/h/h4598.md).

<a name="psalms_109_30"></a>Psalms 109:30

I will [me'od](../../strongs/h/h3966.md) [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md) with my [peh](../../strongs/h/h6310.md); yea, I will [halal](../../strongs/h/h1984.md) him [tavek](../../strongs/h/h8432.md) the [rab](../../strongs/h/h7227.md).

<a name="psalms_109_31"></a>Psalms 109:31

For he shall ['amad](../../strongs/h/h5975.md) at the [yamiyn](../../strongs/h/h3225.md) of the ['ebyown](../../strongs/h/h34.md), to [yasha'](../../strongs/h/h3467.md) him from those that [shaphat](../../strongs/h/h8199.md) his [nephesh](../../strongs/h/h5315.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 108](psalms_108.md) - [Psalms 110](psalms_110.md)