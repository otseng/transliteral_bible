# [Psalms 76](https://www.blueletterbible.org/kjv/psa/76)

<a name="psalms_76_1"></a>Psalms 76:1

To the [nāṣaḥ](../../strongs/h/h5329.md) on [Nᵊḡînâ](../../strongs/h/h5058.md), A [mizmôr](../../strongs/h/h4210.md) or [šîr](../../strongs/h/h7892.md) of ['Āsāp̄](../../strongs/h/h623.md). In [Yehuwdah](../../strongs/h/h3063.md) is ['Elohiym](../../strongs/h/h430.md) [yada'](../../strongs/h/h3045.md): his [shem](../../strongs/h/h8034.md) is [gadowl](../../strongs/h/h1419.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="psalms_76_2"></a>Psalms 76:2

In [šālēm](../../strongs/h/h8004.md) also is his [sōḵ](../../strongs/h/h5520.md), and his [mᵊʿônâ](../../strongs/h/h4585.md) in [Tsiyown](../../strongs/h/h6726.md).

<a name="psalms_76_3"></a>Psalms 76:3

There [shabar](../../strongs/h/h7665.md) he the [rešep̄](../../strongs/h/h7565.md) of the [qesheth](../../strongs/h/h7198.md), the [magen](../../strongs/h/h4043.md), and the [chereb](../../strongs/h/h2719.md), and the [milḥāmâ](../../strongs/h/h4421.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_76_4"></a>Psalms 76:4

Thou art more ['owr](../../strongs/h/h215.md) and ['addiyr](../../strongs/h/h117.md) than the [har](../../strongs/h/h2042.md) of [ṭerep̄](../../strongs/h/h2964.md).

<a name="psalms_76_5"></a>Psalms 76:5

The ['abîr](../../strongs/h/h47.md) [leb](../../strongs/h/h3820.md) are [šālal](../../strongs/h/h7997.md), they have [nûm](../../strongs/h/h5123.md) their [šēnā'](../../strongs/h/h8142.md): and none of the ['enowsh](../../strongs/h/h582.md) of [ḥayil](../../strongs/h/h2428.md) have [māṣā'](../../strongs/h/h4672.md) their [yad](../../strongs/h/h3027.md).

<a name="psalms_76_6"></a>Psalms 76:6

At thy [ge'arah](../../strongs/h/h1606.md), ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md), both the [reḵeḇ](../../strongs/h/h7393.md) and [sûs](../../strongs/h/h5483.md) are [rāḏam](../../strongs/h/h7290.md).

<a name="psalms_76_7"></a>Psalms 76:7

Thou, even thou, art to be [yare'](../../strongs/h/h3372.md): and who may ['amad](../../strongs/h/h5975.md) in thy [paniym](../../strongs/h/h6440.md) ['āz](../../strongs/h/h227.md) once thou art ['aph](../../strongs/h/h639.md) ?

<a name="psalms_76_8"></a>Psalms 76:8

Thou didst cause [diyn](../../strongs/h/h1779.md) to be [shama'](../../strongs/h/h8085.md) from [shamayim](../../strongs/h/h8064.md); the ['erets](../../strongs/h/h776.md) [yare'](../../strongs/h/h3372.md), and was [šāqaṭ](../../strongs/h/h8252.md),

<a name="psalms_76_9"></a>Psalms 76:9

When ['Elohiym](../../strongs/h/h430.md) [quwm](../../strongs/h/h6965.md) to [mishpat](../../strongs/h/h4941.md), to [yasha'](../../strongs/h/h3467.md) all the ['anav](../../strongs/h/h6035.md) of the ['erets](../../strongs/h/h776.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_76_10"></a>Psalms 76:10

Surely the [chemah](../../strongs/h/h2534.md) of ['āḏām](../../strongs/h/h120.md) shall [yadah](../../strongs/h/h3034.md) thee: the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [chemah](../../strongs/h/h2534.md) shalt thou [ḥāḡar](../../strongs/h/h2296.md).

<a name="psalms_76_11"></a>Psalms 76:11

[nāḏar](../../strongs/h/h5087.md), and [shalam](../../strongs/h/h7999.md) unto [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md): let all that be [cabiyb](../../strongs/h/h5439.md) him [yāḇal](../../strongs/h/h2986.md) [šay](../../strongs/h/h7862.md) unto him that ought to be [mowra'](../../strongs/h/h4172.md).

<a name="psalms_76_12"></a>Psalms 76:12

He shall [bāṣar](../../strongs/h/h1219.md) the [ruwach](../../strongs/h/h7307.md) of [nāḡîḏ](../../strongs/h/h5057.md): he is [yare'](../../strongs/h/h3372.md) to the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 75](psalms_75.md) - [Psalms 77](psalms_77.md)