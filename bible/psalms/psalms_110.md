# [Psalms 110](https://www.blueletterbible.org/kjv/psalms/110)

<a name="psalms_110_1"></a>Psalms 110:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [Yĕhovah](../../strongs/h/h3068.md) [nᵊ'um](../../strongs/h/h5002.md) unto my ['adown](../../strongs/h/h113.md), [yashab](../../strongs/h/h3427.md) thou at my [yamiyn](../../strongs/h/h3225.md), until I [shiyth](../../strongs/h/h7896.md) thine ['oyeb](../../strongs/h/h341.md) thy [hăḏōm](../../strongs/h/h1916.md) [regel](../../strongs/h/h7272.md).

<a name="psalms_110_2"></a>Psalms 110:2

[Yĕhovah](../../strongs/h/h3068.md) shall [shalach](../../strongs/h/h7971.md) the [maṭṭê](../../strongs/h/h4294.md) of thy ['oz](../../strongs/h/h5797.md) out of [Tsiyown](../../strongs/h/h6726.md): [radah](../../strongs/h/h7287.md) thou in the [qereḇ](../../strongs/h/h7130.md) of thine ['oyeb](../../strongs/h/h341.md).

<a name="psalms_110_3"></a>Psalms 110:3

Thy ['am](../../strongs/h/h5971.md) shall be [nᵊḏāḇâ](../../strongs/h/h5071.md) in the [yowm](../../strongs/h/h3117.md) of thy [ḥayil](../../strongs/h/h2428.md), in the [hadar](../../strongs/h/h1926.md) of [qodesh](../../strongs/h/h6944.md) from the [reḥem](../../strongs/h/h7358.md) of the [mišḥār](../../strongs/h/h4891.md): thou hast the [ṭal](../../strongs/h/h2919.md) of thy [yalḏûṯ](../../strongs/h/h3208.md).

<a name="psalms_110_4"></a>Psalms 110:4

[Yĕhovah](../../strongs/h/h3068.md) hath [shaba'](../../strongs/h/h7650.md), and will not [nacham](../../strongs/h/h5162.md), Thou art a [kōhēn](../../strongs/h/h3548.md) ['owlam](../../strongs/h/h5769.md) after the [diḇrâ](../../strongs/h/h1700.md) of [Malkî-ṣeḏeq](../../strongs/h/h4442.md).

<a name="psalms_110_5"></a>Psalms 110:5

The ['adonay](../../strongs/h/h136.md) at thy [yamiyn](../../strongs/h/h3225.md) shall [māḥaṣ](../../strongs/h/h4272.md) through [melek](../../strongs/h/h4428.md) in the [yowm](../../strongs/h/h3117.md) of his ['aph](../../strongs/h/h639.md).

<a name="psalms_110_6"></a>Psalms 110:6

He shall [diyn](../../strongs/h/h1777.md) among the [gowy](../../strongs/h/h1471.md), he shall [mālā'](../../strongs/h/h4390.md) the places with the [gᵊvîyâ](../../strongs/h/h1472.md); he shall [māḥaṣ](../../strongs/h/h4272.md) the [ro'sh](../../strongs/h/h7218.md) over [rab](../../strongs/h/h7227.md) ['erets](../../strongs/h/h776.md).

<a name="psalms_110_7"></a>Psalms 110:7

He shall [šāṯâ](../../strongs/h/h8354.md) of the [nachal](../../strongs/h/h5158.md) in the [derek](../../strongs/h/h1870.md): therefore shall he [ruwm](../../strongs/h/h7311.md) the [ro'sh](../../strongs/h/h7218.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 109](psalms_109.md) - [Psalms 111](psalms_111.md)