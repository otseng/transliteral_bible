# [Psalms 12](https://www.blueletterbible.org/kjv/psa/12/1/s_490001)

<a name="psalms_12_1"></a>Psalms 12:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon [šᵊmînîṯ](../../strongs/h/h8067.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [yasha'](../../strongs/h/h3467.md), [Yĕhovah](../../strongs/h/h3068.md); for the [chaciyd](../../strongs/h/h2623.md) [gāmar](../../strongs/h/h1584.md); for the ['aman](../../strongs/h/h539.md) [pāsas](../../strongs/h/h6461.md) from among the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md).

<a name="psalms_12_2"></a>Psalms 12:2

They [dabar](../../strongs/h/h1696.md) [shav'](../../strongs/h/h7723.md) every one with his [rea'](../../strongs/h/h7453.md): with [ḥelqâ](../../strongs/h/h2513.md) [saphah](../../strongs/h/h8193.md) and with a [leb](../../strongs/h/h3820.md) [leb](../../strongs/h/h3820.md) do they [dabar](../../strongs/h/h1696.md).

<a name="psalms_12_3"></a>Psalms 12:3

[Yĕhovah](../../strongs/h/h3068.md) shall [karath](../../strongs/h/h3772.md) all [ḥelqâ](../../strongs/h/h2513.md) [saphah](../../strongs/h/h8193.md), and the [lashown](../../strongs/h/h3956.md) that [dabar](../../strongs/h/h1696.md) [gadowl](../../strongs/h/h1419.md) things:

<a name="psalms_12_4"></a>Psalms 12:4

Who have ['āmar](../../strongs/h/h559.md), With our [lashown](../../strongs/h/h3956.md) will we [gabar](../../strongs/h/h1396.md); our [saphah](../../strongs/h/h8193.md) are our own: who is ['adown](../../strongs/h/h113.md) over us?

<a name="psalms_12_5"></a>Psalms 12:5

For the [shod](../../strongs/h/h7701.md) of the ['aniy](../../strongs/h/h6041.md), for the sighing of the ['ebyown](../../strongs/h/h34.md), now will I [quwm](../../strongs/h/h6965.md), saith [Yĕhovah](../../strongs/h/h3068.md); I will [shiyth](../../strongs/h/h7896.md) him in [yesha'](../../strongs/h/h3468.md) from him that [puwach](../../strongs/h/h6315.md) at him.

<a name="psalms_12_6"></a>Psalms 12:6

The ['imrah](../../strongs/h/h565.md) of [Yĕhovah](../../strongs/h/h3068.md) are [tahowr](../../strongs/h/h2889.md) ['imrah](../../strongs/h/h565.md): as [keceph](../../strongs/h/h3701.md) [tsaraph](../../strongs/h/h6884.md) in a [ʿălîl](../../strongs/h/h5948.md) of ['erets](../../strongs/h/h776.md), [zaqaq](../../strongs/h/h2212.md) seven times.

<a name="psalms_12_7"></a>Psalms 12:7

Thou shalt [shamar](../../strongs/h/h8104.md) them, [Yĕhovah](../../strongs/h/h3068.md), thou shalt [nāṣar](../../strongs/h/h5341.md) them from this [dôr](../../strongs/h/h1755.md) ['owlam](../../strongs/h/h5769.md).

<a name="psalms_12_8"></a>Psalms 12:8

The [rasha'](../../strongs/h/h7563.md) [halak](../../strongs/h/h1980.md) [cabiyb](../../strongs/h/h5439.md), when the [zullûṯ](../../strongs/h/h2149.md) [ben](../../strongs/h/h1121.md) ['āḏām](../../strongs/h/h120.md) are [ruwm](../../strongs/h/h7311.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 11](psalms_11.md) - [Psalms 13](psalms_13.md)