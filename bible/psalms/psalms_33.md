# [Psalms 33]()

<a name="psalms_33_1"></a>Psalms 33:1

[ranan](../../strongs/h/h7442.md) in [Yĕhovah](../../strongs/h/h3068.md), O ye [tsaddiyq](../../strongs/h/h6662.md): for [tehillah](../../strongs/h/h8416.md) is [nā'vê](../../strongs/h/h5000.md) for the [yashar](../../strongs/h/h3477.md).

<a name="psalms_33_2"></a>Psalms 33:2

[yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md) with [kinnôr](../../strongs/h/h3658.md): [zamar](../../strongs/h/h2167.md) unto him with the [neḇel](../../strongs/h/h5035.md) and an [ʿāśôr](../../strongs/h/h6218.md).

<a name="psalms_33_3"></a>Psalms 33:3

[shiyr](../../strongs/h/h7891.md) unto him a [ḥāḏāš](../../strongs/h/h2319.md) [šîr](../../strongs/h/h7892.md); [nāḡan](../../strongs/h/h5059.md) [yatab](../../strongs/h/h3190.md) with a [tᵊrûʿâ](../../strongs/h/h8643.md).

<a name="psalms_33_4"></a>Psalms 33:4

For the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) is [yashar](../../strongs/h/h3477.md); and all his [ma'aseh](../../strongs/h/h4639.md) are done in ['ĕmûnâ](../../strongs/h/h530.md).

<a name="psalms_33_5"></a>Psalms 33:5

He ['ahab](../../strongs/h/h157.md) [ṣĕdāqāh](../../strongs/h/h6666.md) and [mishpat](../../strongs/h/h4941.md): the ['erets](../../strongs/h/h776.md) is [mālā'](../../strongs/h/h4390.md) of the [checed](../../strongs/h/h2617.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_33_6"></a>Psalms 33:6

By the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) were the [shamayim](../../strongs/h/h8064.md) ['asah](../../strongs/h/h6213.md); and all the [tsaba'](../../strongs/h/h6635.md) of them by the [ruwach](../../strongs/h/h7307.md) of his [peh](../../strongs/h/h6310.md).

<a name="psalms_33_7"></a>Psalms 33:7

He [kānas](../../strongs/h/h3664.md) the [mayim](../../strongs/h/h4325.md) of the [yam](../../strongs/h/h3220.md) together as a [nēḏ](../../strongs/h/h5067.md): he [nathan](../../strongs/h/h5414.md) the [tĕhowm](../../strongs/h/h8415.md) in ['ôṣār](../../strongs/h/h214.md).

<a name="psalms_33_8"></a>Psalms 33:8

Let all the ['erets](../../strongs/h/h776.md) [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md): let all the [yashab](../../strongs/h/h3427.md) of the [tebel](../../strongs/h/h8398.md) [guwr](../../strongs/h/h1481.md) of him.

<a name="psalms_33_9"></a>Psalms 33:9

For he ['āmar](../../strongs/h/h559.md), and it was done; he [tsavah](../../strongs/h/h6680.md), and it ['amad](../../strongs/h/h5975.md).

<a name="psalms_33_10"></a>Psalms 33:10

[Yĕhovah](../../strongs/h/h3068.md) [pûr](../../strongs/h/h6331.md) the ['etsah](../../strongs/h/h6098.md) of the [gowy](../../strongs/h/h1471.md) [pûr](../../strongs/h/h6331.md): he [nô'](../../strongs/h/h5106.md) the [maḥăšāḇâ](../../strongs/h/h4284.md) of the ['am](../../strongs/h/h5971.md) [nô'](../../strongs/h/h5106.md).

<a name="psalms_33_11"></a>Psalms 33:11

The ['etsah](../../strongs/h/h6098.md) of [Yĕhovah](../../strongs/h/h3068.md) ['amad](../../strongs/h/h5975.md) ['owlam](../../strongs/h/h5769.md), the [maḥăšāḇâ](../../strongs/h/h4284.md) of his [leb](../../strongs/h/h3820.md) to all [dôr](../../strongs/h/h1755.md).

<a name="psalms_33_12"></a>Psalms 33:12

['esher](../../strongs/h/h835.md) is the [gowy](../../strongs/h/h1471.md) whose ['Elohiym](../../strongs/h/h430.md) is [Yĕhovah](../../strongs/h/h3068.md); and the ['am](../../strongs/h/h5971.md) whom he hath [bāḥar](../../strongs/h/h977.md) for his own [nachalah](../../strongs/h/h5159.md).

<a name="psalms_33_13"></a>Psalms 33:13

[Yĕhovah](../../strongs/h/h3068.md) [nabat](../../strongs/h/h5027.md) from [shamayim](../../strongs/h/h8064.md); he [ra'ah](../../strongs/h/h7200.md) all the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md).

<a name="psalms_33_14"></a>Psalms 33:14

From the [māḵôn](../../strongs/h/h4349.md) of his [yashab](../../strongs/h/h3427.md) he [šāḡaḥ](../../strongs/h/h7688.md) upon all the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md).

<a name="psalms_33_15"></a>Psalms 33:15

He [yāṣar](../../strongs/h/h3335.md) their [leb](../../strongs/h/h3820.md) [yaḥaḏ](../../strongs/h/h3162.md); he [bîn](../../strongs/h/h995.md) all their [ma'aseh](../../strongs/h/h4639.md).

<a name="psalms_33_16"></a>Psalms 33:16

There is no [melek](../../strongs/h/h4428.md) [yasha'](../../strongs/h/h3467.md) by the [rōḇ](../../strongs/h/h7230.md) of an [ḥayil](../../strongs/h/h2428.md): a [gibôr](../../strongs/h/h1368.md) is not [natsal](../../strongs/h/h5337.md) by much [koach](../../strongs/h/h3581.md).

<a name="psalms_33_17"></a>Psalms 33:17

A [sûs](../../strongs/h/h5483.md) is a [sheqer](../../strongs/h/h8267.md) for [tᵊšûʿâ](../../strongs/h/h8668.md): neither shall he [mālaṭ](../../strongs/h/h4422.md) any by his [rōḇ](../../strongs/h/h7230.md) [ḥayil](../../strongs/h/h2428.md).

<a name="psalms_33_18"></a>Psalms 33:18

Behold, the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) is upon them that [yārē'](../../strongs/h/h3373.md) him, upon them that [yāḥal](../../strongs/h/h3176.md) in his [checed](../../strongs/h/h2617.md);

<a name="psalms_33_19"></a>Psalms 33:19

To [natsal](../../strongs/h/h5337.md) their [nephesh](../../strongs/h/h5315.md) from [maveth](../../strongs/h/h4194.md), and to keep them [ḥāyâ](../../strongs/h/h2421.md) in [rāʿāḇ](../../strongs/h/h7458.md).

<a name="psalms_33_20"></a>Psalms 33:20

Our [nephesh](../../strongs/h/h5315.md) [ḥāḵâ](../../strongs/h/h2442.md) for [Yĕhovah](../../strongs/h/h3068.md): he is our ['ezer](../../strongs/h/h5828.md) and our [magen](../../strongs/h/h4043.md).

<a name="psalms_33_21"></a>Psalms 33:21

For our [leb](../../strongs/h/h3820.md) shall [samach](../../strongs/h/h8055.md) in him, because we have [batach](../../strongs/h/h982.md) in his [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md).

<a name="psalms_33_22"></a>Psalms 33:22

Let thy [checed](../../strongs/h/h2617.md), [Yĕhovah](../../strongs/h/h3068.md), be upon us, according as we [yāḥal](../../strongs/h/h3176.md) in thee.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 32](psalms_32.md) - [Psalms 34](psalms_34.md)