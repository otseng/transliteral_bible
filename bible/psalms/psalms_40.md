# [Psalms 40](https://www.blueletterbible.org/kjv/psa/40/1/)

<a name="psalms_40_1"></a>Psalms 40:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). I [qāvâ](../../strongs/h/h6960.md) [qāvâ](../../strongs/h/h6960.md) for [Yĕhovah](../../strongs/h/h3068.md); and he [natah](../../strongs/h/h5186.md) unto me, and [shama'](../../strongs/h/h8085.md) my [shav'ah](../../strongs/h/h7775.md).

<a name="psalms_40_2"></a>Psalms 40:2

He [ʿālâ](../../strongs/h/h5927.md) me also out of a [shā'ôn](../../strongs/h/h7588.md) [bowr](../../strongs/h/h953.md), out of the [yāvēn](../../strongs/h/h3121.md) [ṭîṭ](../../strongs/h/h2916.md), and [quwm](../../strongs/h/h6965.md) my [regel](../../strongs/h/h7272.md) upon a [cela'](../../strongs/h/h5553.md), and [kuwn](../../strongs/h/h3559.md) my ['ashuwr](../../strongs/h/h838.md).

<a name="psalms_40_3"></a>Psalms 40:3

And he hath [nathan](../../strongs/h/h5414.md) a [ḥāḏāš](../../strongs/h/h2319.md) [šîr](../../strongs/h/h7892.md) in my [peh](../../strongs/h/h6310.md), even [tehillah](../../strongs/h/h8416.md) unto our ['Elohiym](../../strongs/h/h430.md): many shall [ra'ah](../../strongs/h/h7200.md) it, and [yare'](../../strongs/h/h3372.md), and shall [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_40_4"></a>Psalms 40:4

['esher](../../strongs/h/h835.md) is that [geḇer](../../strongs/h/h1397.md) that [śûm](../../strongs/h/h7760.md) [Yĕhovah](../../strongs/h/h3068.md) his [miḇṭāḥ](../../strongs/h/h4009.md), and [panah](../../strongs/h/h6437.md) not the [rāhāḇ](../../strongs/h/h7295.md), nor such as [śûṭ](../../strongs/h/h7750.md) to [kazab](../../strongs/h/h3577.md).

<a name="psalms_40_5"></a>Psalms 40:5

[rab](../../strongs/h/h7227.md), [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), are thy [pala'](../../strongs/h/h6381.md) which thou hast ['asah](../../strongs/h/h6213.md), and thy [maḥăšāḇâ](../../strongs/h/h4284.md) which are to us-ward: they cannot be ['arak](../../strongs/h/h6186.md) unto thee: if I would [nāḡaḏ](../../strongs/h/h5046.md) and [dabar](../../strongs/h/h1696.md) of them, they are [ʿāṣam](../../strongs/h/h6105.md) than can be [sāp̄ar](../../strongs/h/h5608.md).

<a name="psalms_40_6"></a>Psalms 40:6

[zebach](../../strongs/h/h2077.md) and [minchah](../../strongs/h/h4503.md) thou didst not [ḥāp̄ēṣ](../../strongs/h/h2654.md); mine ['ozen](../../strongs/h/h241.md) hast thou [karah](../../strongs/h/h3738.md): an [ʿōlâ](../../strongs/h/h5930.md) and [ḥăṭā'â](../../strongs/h/h2401.md) hast thou not [sha'al](../../strongs/h/h7592.md).

<a name="psalms_40_7"></a>Psalms 40:7

Then ['āmar](../../strongs/h/h559.md) I, Lo, I [bow'](../../strongs/h/h935.md): in the [mᵊḡillâ](../../strongs/h/h4039.md) of the [sēp̄er](../../strongs/h/h5612.md) it is [kāṯaḇ](../../strongs/h/h3789.md) of me,

<a name="psalms_40_8"></a>Psalms 40:8

I [ḥāp̄ēṣ](../../strongs/h/h2654.md) to ['asah](../../strongs/h/h6213.md) thy [ratsown](../../strongs/h/h7522.md), ['Elohiym](../../strongs/h/h430.md): yea, thy [towrah](../../strongs/h/h8451.md) is [tavek](../../strongs/h/h8432.md) my [me'ah](../../strongs/h/h4578.md).

<a name="psalms_40_9"></a>Psalms 40:9

I have [bāśar](../../strongs/h/h1319.md) [tsedeq](../../strongs/h/h6664.md) in the [rab](../../strongs/h/h7227.md) [qāhēl](../../strongs/h/h6951.md): lo, I have not [kālā'](../../strongs/h/h3607.md) my [saphah](../../strongs/h/h8193.md), [Yĕhovah](../../strongs/h/h3068.md), thou [yada'](../../strongs/h/h3045.md).

<a name="psalms_40_10"></a>Psalms 40:10

I have not [kāsâ](../../strongs/h/h3680.md) thy [ṣĕdāqāh](../../strongs/h/h6666.md) within my [leb](../../strongs/h/h3820.md); I have ['āmar](../../strongs/h/h559.md) thy ['ĕmûnâ](../../strongs/h/h530.md) and thy [tᵊšûʿâ](../../strongs/h/h8668.md): I have not [kāḥaḏ](../../strongs/h/h3582.md) thy [checed](../../strongs/h/h2617.md) and thy ['emeth](../../strongs/h/h571.md) from the [rab](../../strongs/h/h7227.md) [qāhēl](../../strongs/h/h6951.md).

<a name="psalms_40_11"></a>Psalms 40:11

[kālā'](../../strongs/h/h3607.md) not thou thy [raḥam](../../strongs/h/h7356.md) from me, [Yĕhovah](../../strongs/h/h3068.md): let thy [checed](../../strongs/h/h2617.md) and thy ['emeth](../../strongs/h/h571.md) [tāmîḏ](../../strongs/h/h8548.md) [nāṣar](../../strongs/h/h5341.md) me.

<a name="psalms_40_12"></a>Psalms 40:12

For [mispār](../../strongs/h/h4557.md) [ra'](../../strongs/h/h7451.md) have ['āp̄ap̄](../../strongs/h/h661.md) me about: mine ['avon](../../strongs/h/h5771.md) have [nāśaḡ](../../strongs/h/h5381.md) upon me, so that I am not able to [ra'ah](../../strongs/h/h7200.md); they are [ʿāṣam](../../strongs/h/h6105.md) than the [śaʿărâ](../../strongs/h/h8185.md) of mine [ro'sh](../../strongs/h/h7218.md): therefore my [leb](../../strongs/h/h3820.md) ['azab](../../strongs/h/h5800.md) me.

<a name="psalms_40_13"></a>Psalms 40:13

Be [ratsah](../../strongs/h/h7521.md), [Yĕhovah](../../strongs/h/h3068.md), to [natsal](../../strongs/h/h5337.md) me: [Yĕhovah](../../strongs/h/h3068.md), make [ḥûš](../../strongs/h/h2363.md) to [ʿezrâ](../../strongs/h/h5833.md) me.

<a name="psalms_40_14"></a>Psalms 40:14

Let them be [buwsh](../../strongs/h/h954.md) and [ḥāp̄ēr](../../strongs/h/h2659.md) [yaḥaḏ](../../strongs/h/h3162.md) that [bāqaš](../../strongs/h/h1245.md) after my [nephesh](../../strongs/h/h5315.md) to [sāp̄â](../../strongs/h/h5595.md) it; let them be [sûḡ](../../strongs/h/h5472.md) ['āḥôr](../../strongs/h/h268.md) and put to [kālam](../../strongs/h/h3637.md) that [chaphets](../../strongs/h/h2655.md) me [ra'](../../strongs/h/h7451.md).

<a name="psalms_40_15"></a>Psalms 40:15

Let them be [šāmēm](../../strongs/h/h8074.md) for a [ʿēqeḇ](../../strongs/h/h6118.md) of their [bšeṯ](../../strongs/h/h1322.md) that ['āmar](../../strongs/h/h559.md) unto me, [he'āḥ](../../strongs/h/h1889.md), [he'āḥ](../../strongs/h/h1889.md).

<a name="psalms_40_16"></a>Psalms 40:16

Let all those that [bāqaš](../../strongs/h/h1245.md) thee [śûś](../../strongs/h/h7797.md) and be [samach](../../strongs/h/h8055.md) in thee: let such as ['ahab](../../strongs/h/h157.md) thy [tᵊšûʿâ](../../strongs/h/h8668.md) ['āmar](../../strongs/h/h559.md) [tāmîḏ](../../strongs/h/h8548.md), [Yĕhovah](../../strongs/h/h3068.md) be [gāḏal](../../strongs/h/h1431.md).

<a name="psalms_40_17"></a>Psalms 40:17

But I am ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md); yet ['adonay](../../strongs/h/h136.md) [chashab](../../strongs/h/h2803.md) upon me: thou art my [ʿezrâ](../../strongs/h/h5833.md) and my [palat](../../strongs/h/h6403.md); make no ['āḥar](../../strongs/h/h309.md), ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 39](psalms_39.md) - [Psalms 41](psalms_41.md)