# [Psalms 25](https://www.blueletterbible.org/kjv/psa/25/1/s_503001)

<a name="psalms_25_1"></a>Psalms 25:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). Unto thee, [Yĕhovah](../../strongs/h/h3068.md), do I [nasa'](../../strongs/h/h5375.md) my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_25_2"></a>Psalms 25:2

['Elohiym](../../strongs/h/h430.md), I [batach](../../strongs/h/h982.md) in thee: let me not be [buwsh](../../strongs/h/h954.md), let not mine ['oyeb](../../strongs/h/h341.md) ['alats](../../strongs/h/h5970.md) over me.

<a name="psalms_25_3"></a>Psalms 25:3

Yea, let none that [qāvâ](../../strongs/h/h6960.md) on thee be [buwsh](../../strongs/h/h954.md): let them be [buwsh](../../strongs/h/h954.md) which [bāḡaḏ](../../strongs/h/h898.md) [rêqām](../../strongs/h/h7387.md).

<a name="psalms_25_4"></a>Psalms 25:4

[yada'](../../strongs/h/h3045.md) me thy [derek](../../strongs/h/h1870.md), [Yĕhovah](../../strongs/h/h3068.md); [lamad](../../strongs/h/h3925.md) me thy ['orach](../../strongs/h/h734.md).

<a name="psalms_25_5"></a>Psalms 25:5

[dāraḵ](../../strongs/h/h1869.md) me in thy ['emeth](../../strongs/h/h571.md), and [lamad](../../strongs/h/h3925.md) me: for thou art the ['Elohiym](../../strongs/h/h430.md) of my [yesha'](../../strongs/h/h3468.md); on thee do I [qāvâ](../../strongs/h/h6960.md) all the [yowm](../../strongs/h/h3117.md).

<a name="psalms_25_6"></a>Psalms 25:6

[zakar](../../strongs/h/h2142.md), [Yĕhovah](../../strongs/h/h3068.md), thy [raḥam](../../strongs/h/h7356.md) and thy [checed](../../strongs/h/h2617.md); for they have been ['owlam](../../strongs/h/h5769.md).

<a name="psalms_25_7"></a>Psalms 25:7

[zakar](../../strongs/h/h2142.md) not the [chatta'ath](../../strongs/h/h2403.md) of my [nāʿur](../../strongs/h/h5271.md), nor my [pesha'](../../strongs/h/h6588.md): according to thy [checed](../../strongs/h/h2617.md) [zakar](../../strongs/h/h2142.md)thou me for thy [ṭûḇ](../../strongs/h/h2898.md) sake, [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_25_8"></a>Psalms 25:8

[towb](../../strongs/h/h2896.md) and [yashar](../../strongs/h/h3477.md) is [Yĕhovah](../../strongs/h/h3068.md): therefore will he [yārâ](../../strongs/h/h3384.md) [chatta'](../../strongs/h/h2400.md) in the [derek](../../strongs/h/h1870.md).

<a name="psalms_25_9"></a>Psalms 25:9

The ['anav](../../strongs/h/h6035.md) will he [dāraḵ](../../strongs/h/h1869.md) in [mishpat](../../strongs/h/h4941.md): and the ['anav](../../strongs/h/h6035.md) will he [lamad](../../strongs/h/h3925.md) his [derek](../../strongs/h/h1870.md).

<a name="psalms_25_10"></a>Psalms 25:10

All the ['orach](../../strongs/h/h734.md) of [Yĕhovah](../../strongs/h/h3068.md) are [checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md) unto such as [nāṣar](../../strongs/h/h5341.md) his [bĕriyth](../../strongs/h/h1285.md) and his [ʿēḏâ](../../strongs/h/h5713.md).

<a name="psalms_25_11"></a>Psalms 25:11

For thy [shem](../../strongs/h/h8034.md) sake, [Yĕhovah](../../strongs/h/h3068.md), [sālaḥ](../../strongs/h/h5545.md) mine ['avon](../../strongs/h/h5771.md); for it is [rab](../../strongs/h/h7227.md).

<a name="psalms_25_12"></a>Psalms 25:12

What ['iysh](../../strongs/h/h376.md) is he that [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md)? him shall he [yārâ](../../strongs/h/h3384.md) in the [derek](../../strongs/h/h1870.md) that he shall [bāḥar](../../strongs/h/h977.md).

<a name="psalms_25_13"></a>Psalms 25:13

His [nephesh](../../strongs/h/h5315.md) shall [lûn](../../strongs/h/h3885.md) at [towb](../../strongs/h/h2896.md); and his [zera'](../../strongs/h/h2233.md) shall [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md).

<a name="psalms_25_14"></a>Psalms 25:14

The [sôḏ](../../strongs/h/h5475.md) of [Yĕhovah](../../strongs/h/h3068.md) is with them that [yārē'](../../strongs/h/h3373.md) him; and he will [yada'](../../strongs/h/h3045.md) them his [bĕriyth](../../strongs/h/h1285.md).

<a name="psalms_25_15"></a>Psalms 25:15

Mine ['ayin](../../strongs/h/h5869.md) are ever toward [Yĕhovah](../../strongs/h/h3068.md); for he shall [yāṣā'](../../strongs/h/h3318.md) my [regel](../../strongs/h/h7272.md) out of the [rešeṯ](../../strongs/h/h7568.md).

<a name="psalms_25_16"></a>Psalms 25:16

[panah](../../strongs/h/h6437.md) thee unto me, and have [chanan](../../strongs/h/h2603.md) upon me; for I am [yāḥîḏ](../../strongs/h/h3173.md) and ['aniy](../../strongs/h/h6041.md).

<a name="psalms_25_17"></a>Psalms 25:17

The [tsarah](../../strongs/h/h6869.md) of my [lebab](../../strongs/h/h3824.md) are [rāḥaḇ](../../strongs/h/h7337.md): O bring thou me out of my [mᵊṣûqâ](../../strongs/h/h4691.md).

<a name="psalms_25_18"></a>Psalms 25:18

[ra'ah](../../strongs/h/h7200.md) upon mine ['oniy](../../strongs/h/h6040.md) and my ['amal](../../strongs/h/h5999.md); and [nasa'](../../strongs/h/h5375.md) all my [chatta'ath](../../strongs/h/h2403.md).

<a name="psalms_25_19"></a>Psalms 25:19

[ra'ah](../../strongs/h/h7200.md) mine ['oyeb](../../strongs/h/h341.md); for they are many; and they [sane'](../../strongs/h/h8130.md) me with [chamac](../../strongs/h/h2555.md) [śin'â](../../strongs/h/h8135.md).

<a name="psalms_25_20"></a>Psalms 25:20

O [shamar](../../strongs/h/h8104.md) my [nephesh](../../strongs/h/h5315.md), and [natsal](../../strongs/h/h5337.md) me: let me not be [buwsh](../../strongs/h/h954.md); for I put my [chacah](../../strongs/h/h2620.md) in thee.

<a name="psalms_25_21"></a>Psalms 25:21

Let [tom](../../strongs/h/h8537.md) and [yōšer](../../strongs/h/h3476.md) [nāṣar](../../strongs/h/h5341.md) me; for I [qāvâ](../../strongs/h/h6960.md) on thee.

<a name="psalms_25_22"></a>Psalms 25:22

[pāḏâ](../../strongs/h/h6299.md) [Yisra'el](../../strongs/h/h3478.md), O ['Elohiym](../../strongs/h/h430.md), out of all his [tsarah](../../strongs/h/h6869.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 24](psalms_24.md) - [Psalms 26](psalms_26.md)