# [Psalms 128](https://www.blueletterbible.org/kjv/psalms/128)

<a name="psalms_128_1"></a>Psalms 128:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md). ['esher](../../strongs/h/h835.md) is every one that [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md); that [halak](../../strongs/h/h1980.md) in his [derek](../../strongs/h/h1870.md).

<a name="psalms_128_2"></a>Psalms 128:2

For thou shalt ['akal](../../strongs/h/h398.md) the [yᵊḡîaʿ](../../strongs/h/h3018.md) of thine [kaph](../../strongs/h/h3709.md): ['esher](../../strongs/h/h835.md) shalt thou be, and it shall be [towb](../../strongs/h/h2896.md) with thee.

<a name="psalms_128_3"></a>Psalms 128:3

Thy ['ishshah](../../strongs/h/h802.md) shall be as a [parah](../../strongs/h/h6509.md) [gep̄en](../../strongs/h/h1612.md) by the [yᵊrēḵâ](../../strongs/h/h3411.md) of thine [bayith](../../strongs/h/h1004.md): thy [ben](../../strongs/h/h1121.md) like [zayiṯ](../../strongs/h/h2132.md) [šāṯîl](../../strongs/h/h8363.md) [cabiyb](../../strongs/h/h5439.md) thy [šulḥān](../../strongs/h/h7979.md).

<a name="psalms_128_4"></a>Psalms 128:4

Behold, that thus shall the [geḇer](../../strongs/h/h1397.md) be [barak](../../strongs/h/h1288.md) that [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_128_5"></a>Psalms 128:5

[Yĕhovah](../../strongs/h/h3068.md) shall [barak](../../strongs/h/h1288.md) thee out of [Tsiyown](../../strongs/h/h6726.md): and thou shalt [ra'ah](../../strongs/h/h7200.md) the [ṭûḇ](../../strongs/h/h2898.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) all the [yowm](../../strongs/h/h3117.md) of thy [chay](../../strongs/h/h2416.md).

<a name="psalms_128_6"></a>Psalms 128:6

Yea, thou shalt [ra'ah](../../strongs/h/h7200.md) thy [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md), and [shalowm](../../strongs/h/h7965.md) upon [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 127](psalms_127.md) - [Psalms 129](psalms_129.md)