# [Psalms 122](https://www.blueletterbible.org/kjv/psalms/122)

<a name="psalms_122_1"></a>Psalms 122:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md) of [Dāviḏ](../../strongs/h/h1732.md). I was [samach](../../strongs/h/h8055.md) when they ['āmar](../../strongs/h/h559.md) unto me, Let us [yālaḵ](../../strongs/h/h3212.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_122_2"></a>Psalms 122:2

Our [regel](../../strongs/h/h7272.md) shall ['amad](../../strongs/h/h5975.md) within thy [sha'ar](../../strongs/h/h8179.md), [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="psalms_122_3"></a>Psalms 122:3

[Yĕruwshalaim](../../strongs/h/h3389.md) is [bānâ](../../strongs/h/h1129.md) as a [ʿîr](../../strongs/h/h5892.md) that is [ḥāḇar](../../strongs/h/h2266.md) [yaḥaḏ](../../strongs/h/h3162.md):

<a name="psalms_122_4"></a>Psalms 122:4

[šām](../../strongs/h/h8033.md) the [shebet](../../strongs/h/h7626.md) [ʿālâ](../../strongs/h/h5927.md), the [shebet](../../strongs/h/h7626.md) of the [Yahh](../../strongs/h/h3050.md), unto the [ʿēḏûṯ](../../strongs/h/h5715.md) of [Yisra'el](../../strongs/h/h3478.md), to [yadah](../../strongs/h/h3034.md) unto the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_122_5"></a>Psalms 122:5

For there are [yashab](../../strongs/h/h3427.md) [kicce'](../../strongs/h/h3678.md) of [mishpat](../../strongs/h/h4941.md), the [kicce'](../../strongs/h/h3678.md) of the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="psalms_122_6"></a>Psalms 122:6

[sha'al](../../strongs/h/h7592.md) for the [shalowm](../../strongs/h/h7965.md) of [Yĕruwshalaim](../../strongs/h/h3389.md): they shall [šālâ](../../strongs/h/h7951.md) that ['ahab](../../strongs/h/h157.md) thee.

<a name="psalms_122_7"></a>Psalms 122:7

[shalowm](../../strongs/h/h7965.md) be within thy [cheyl](../../strongs/h/h2426.md), and [šalvâ](../../strongs/h/h7962.md) within thy ['armôn](../../strongs/h/h759.md).

<a name="psalms_122_8"></a>Psalms 122:8

For my ['ach](../../strongs/h/h251.md) and [rea'](../../strongs/h/h7453.md) sakes, I will now [dabar](../../strongs/h/h1696.md), [shalowm](../../strongs/h/h7965.md) be within thee.

<a name="psalms_122_9"></a>Psalms 122:9

Because of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) I will [bāqaš](../../strongs/h/h1245.md) thy [towb](../../strongs/h/h2896.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 121](psalms_121.md) - [Psalms 123](psalms_123.md)