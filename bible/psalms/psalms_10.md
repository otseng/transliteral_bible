# [Psalms 10](https://www.blueletterbible.org/kjv/psa/10/1/s_488001)

<a name="psalms_10_1"></a>Psalms 10:1

Why ['amad](../../strongs/h/h5975.md) thou [rachowq](../../strongs/h/h7350.md), [Yĕhovah](../../strongs/h/h3068.md)? ['alam](../../strongs/h/h5956.md) thou in times of [tsarah](../../strongs/h/h6869.md?

<a name="psalms_10_2"></a>Psalms 10:2

The [rasha'](../../strongs/h/h7563.md) in [ga'avah](../../strongs/h/h1346.md) doth [dalaq](../../strongs/h/h1814.md) the ['aniy](../../strongs/h/h6041.md): let them be [tāp̄aś](../../strongs/h/h8610.md) in the [mezimmah](../../strongs/h/h4209.md) that they have [chashab](../../strongs/h/h2803.md).

<a name="psalms_10_3"></a>Psalms 10:3

For the [rasha'](../../strongs/h/h7563.md) [halal](../../strongs/h/h1984.md) of his [nephesh](../../strongs/h/h5315.md) [ta'avah](../../strongs/h/h8378.md), and [barak](../../strongs/h/h1288.md) the [batsa'](../../strongs/h/h1214.md), whom [Yĕhovah](../../strongs/h/h3068.md) [na'ats](../../strongs/h/h5006.md).

<a name="psalms_10_4"></a>Psalms 10:4

The [rasha'](../../strongs/h/h7563.md), through the [gobahh](../../strongs/h/h1363.md) of his ['aph](../../strongs/h/h639.md), will not [darash](../../strongs/h/h1875.md) after: ['Elohiym](../../strongs/h/h430.md) [mezimmah](../../strongs/h/h4209.md).

<a name="psalms_10_5"></a>Psalms 10:5

His [derek](../../strongs/h/h1870.md) are always [chuwl](../../strongs/h/h2342.md); thy [mishpat](../../strongs/h/h4941.md) are [marowm](../../strongs/h/h4791.md) out of his sight: all his [tsarar](../../strongs/h/h6887.md), he [puwach](../../strongs/h/h6315.md) at them.

<a name="psalms_10_6"></a>Psalms 10:6

He hath ['āmar](../../strongs/h/h559.md) in his [leb](../../strongs/h/h3820.md), I shall not be [mowt](../../strongs/h/h4131.md): for I shall never be in [ra'](../../strongs/h/h7451.md).

<a name="psalms_10_7"></a>Psalms 10:7

His [peh](../../strongs/h/h6310.md) is full of ['alah](../../strongs/h/h423.md) and [mirmah](../../strongs/h/h4820.md) and [tok](../../strongs/h/h8496.md): under his [lashown](../../strongs/h/h3956.md) is ['amal](../../strongs/h/h5999.md) and ['aven](../../strongs/h/h205.md).

<a name="psalms_10_8"></a>Psalms 10:8

He [yashab](../../strongs/h/h3427.md) in the [ma'ărāḇ](../../strongs/h/h3993.md) of the [ḥāṣēr](../../strongs/h/h2691.md): in the [mictar](../../strongs/h/h4565.md) doth he [harag](../../strongs/h/h2026.md) the [naqiy](../../strongs/h/h5355.md): his ['ayin](../../strongs/h/h5869.md) are [tsaphan](../../strongs/h/h6845.md) against the [cheleka'](../../strongs/h/h2489.md).

<a name="psalms_10_9"></a>Psalms 10:9

He ['arab](../../strongs/h/h693.md) [mictar](../../strongs/h/h4565.md) as an ['ariy](../../strongs/h/h738.md) in his [sōḵ](../../strongs/h/h5520.md): he ['arab](../../strongs/h/h693.md) to catch the ['aniy](../../strongs/h/h6041.md): he doth catch the ['aniy](../../strongs/h/h6041.md), when he [mashak](../../strongs/h/h4900.md) him into his [rešeṯ](../../strongs/h/h7568.md).

<a name="psalms_10_10"></a>Psalms 10:10

He [dakah](../../strongs/h/h1794.md), and [shachach](../../strongs/h/h7817.md), that the [cheyl](../../strongs/h/h2426.md) [cheleka'](../../strongs/h/h2489.md) may [naphal](../../strongs/h/h5307.md) by ['atsuwm](../../strongs/h/h6099.md).

<a name="psalms_10_11"></a>Psalms 10:11

He hath ['āmar](../../strongs/h/h559.md) in his [leb](../../strongs/h/h3820.md), ['el](../../strongs/h/h410.md) hath [shakach](../../strongs/h/h7911.md): he [cathar](../../strongs/h/h5641.md) his [paniym](../../strongs/h/h6440.md); he will never [ra'ah](../../strongs/h/h7200.md) it.

<a name="psalms_10_12"></a>Psalms 10:12

[quwm](../../strongs/h/h6965.md), [Yĕhovah](../../strongs/h/h3068.md); ['el](../../strongs/h/h410.md), [nasa'](../../strongs/h/h5375.md) thine [yad](../../strongs/h/h3027.md): [shakach](../../strongs/h/h7911.md) not the ['anav](../../strongs/h/h6035.md) ['aniy](../../strongs/h/h6041.md).

<a name="psalms_10_13"></a>Psalms 10:13

Wherefore doth the [rasha'](../../strongs/h/h7563.md) [na'ats](../../strongs/h/h5006.md) ['Elohiym](../../strongs/h/h430.md)? he hath ['āmar](../../strongs/h/h559.md) in his [leb](../../strongs/h/h3820.md), Thou wilt not [darash](../../strongs/h/h1875.md) it.

<a name="psalms_10_14"></a>Psalms 10:14

Thou hast [ra'ah](../../strongs/h/h7200.md) it; for thou [nabat](../../strongs/h/h5027.md) ['amal](../../strongs/h/h5999.md) and [ka'ac](../../strongs/h/h3708.md), to [nathan](../../strongs/h/h5414.md) it with thy [yad](../../strongs/h/h3027.md): the [cheleka'](../../strongs/h/h2489.md) ['azab](../../strongs/h/h5800.md) himself unto thee; thou art the [ʿāzar](../../strongs/h/h5826.md) of the [yathowm](../../strongs/h/h3490.md).

<a name="psalms_10_15"></a>Psalms 10:15

[shabar](../../strongs/h/h7665.md) thou the [zerowa'](../../strongs/h/h2220.md) of the [rasha'](../../strongs/h/h7563.md) and the [ra'](../../strongs/h/h7451.md): [darash](../../strongs/h/h1875.md) out his [resha'](../../strongs/h/h7562.md) till thou [māṣā'](../../strongs/h/h4672.md) none.

<a name="psalms_10_16"></a>Psalms 10:16

[Yĕhovah](../../strongs/h/h3068.md) is [melek](../../strongs/h/h4428.md) ['owlam](../../strongs/h/h5769.md) and ever: the [gowy](../../strongs/h/h1471.md) are ['abad](../../strongs/h/h6.md) out of his ['erets](../../strongs/h/h776.md).

<a name="psalms_10_17"></a>Psalms 10:17

[Yĕhovah](../../strongs/h/h3068.md), thou hast [shama'](../../strongs/h/h8085.md) the [ta'avah](../../strongs/h/h8378.md) of the ['anav](../../strongs/h/h6035.md): thou wilt [kuwn](../../strongs/h/h3559.md) their [leb](../../strongs/h/h3820.md), thou wilt cause thine ['ozen](../../strongs/h/h241.md) to [qashab](../../strongs/h/h7181.md):

<a name="psalms_10_18"></a>Psalms 10:18

To [shaphat](../../strongs/h/h8199.md) the [yathowm](../../strongs/h/h3490.md) and the [dak](../../strongs/h/h1790.md), that the ['enowsh](../../strongs/h/h582.md) of the ['erets](../../strongs/h/h776.md) may no more [ʿāraṣ](../../strongs/h/h6206.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 9](psalms_9.md) - [Psalms 11](psalms_11.md)