# [Psalms 136](https://www.blueletterbible.org/kjv/psalms/136)

<a name="psalms_136_1"></a>Psalms 136:1

[yadah](../../strongs/h/h3034.md) unto [Yĕhovah](../../strongs/h/h3068.md); for he is [towb](../../strongs/h/h2896.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_136_2"></a>Psalms 136:2

[yadah](../../strongs/h/h3034.md) unto the ['Elohiym](../../strongs/h/h430.md) of ['Elohiym](../../strongs/h/h430.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_136_3"></a>Psalms 136:3

[yadah](../../strongs/h/h3034.md) to the ['adown](../../strongs/h/h113.md) of ['adown](../../strongs/h/h113.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_136_4"></a>Psalms 136:4

To him who alone ['asah](../../strongs/h/h6213.md) [gadowl](../../strongs/h/h1419.md) [pala'](../../strongs/h/h6381.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_136_5"></a>Psalms 136:5

To him that by [tāḇûn](../../strongs/h/h8394.md) ['asah](../../strongs/h/h6213.md) the [shamayim](../../strongs/h/h8064.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_136_6"></a>Psalms 136:6

To him that stretched [rāqaʿ](../../strongs/h/h7554.md) the ['erets](../../strongs/h/h776.md) above the [mayim](../../strongs/h/h4325.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_136_7"></a>Psalms 136:7

To him that ['asah](../../strongs/h/h6213.md) [gadowl](../../strongs/h/h1419.md) ['owr](../../strongs/h/h216.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md):

<a name="psalms_136_8"></a>Psalms 136:8

The [šemeš](../../strongs/h/h8121.md) to [memshalah](../../strongs/h/h4475.md) by [yowm](../../strongs/h/h3117.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md):

<a name="psalms_136_9"></a>Psalms 136:9

The [yareach](../../strongs/h/h3394.md) and [kowkab](../../strongs/h/h3556.md) to [memshalah](../../strongs/h/h4475.md) by [layil](../../strongs/h/h3915.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_136_10"></a>Psalms 136:10

To him that [nakah](../../strongs/h/h5221.md) [Mitsrayim](../../strongs/h/h4714.md) in their [bᵊḵôr](../../strongs/h/h1060.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md):

<a name="psalms_136_11"></a>Psalms 136:11

And [yāṣā'](../../strongs/h/h3318.md) [Yisra'el](../../strongs/h/h3478.md) from [tavek](../../strongs/h/h8432.md) them: for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md):

<a name="psalms_136_12"></a>Psalms 136:12

With a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md), and with a stretched [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_136_13"></a>Psalms 136:13

To him which [gāzar](../../strongs/h/h1504.md) the [sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md) into [gezer](../../strongs/h/h1506.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md):

<a name="psalms_136_14"></a>Psalms 136:14

And made [Yisra'el](../../strongs/h/h3478.md) to ['abar](../../strongs/h/h5674.md) the [tavek](../../strongs/h/h8432.md) of it: for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md):

<a name="psalms_136_15"></a>Psalms 136:15

But [nāʿar](../../strongs/h/h5287.md) [Parʿô](../../strongs/h/h6547.md) and his [ḥayil](../../strongs/h/h2428.md) in the [sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_136_16"></a>Psalms 136:16

To him which [yālaḵ](../../strongs/h/h3212.md) his ['am](../../strongs/h/h5971.md) through the [midbar](../../strongs/h/h4057.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_136_17"></a>Psalms 136:17

To him which [nakah](../../strongs/h/h5221.md) [gadowl](../../strongs/h/h1419.md) [melek](../../strongs/h/h4428.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md):

<a name="psalms_136_18"></a>Psalms 136:18

And [harag](../../strongs/h/h2026.md) ['addiyr](../../strongs/h/h117.md) [melek](../../strongs/h/h4428.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md):

<a name="psalms_136_19"></a>Psalms 136:19

[Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md):

<a name="psalms_136_20"></a>Psalms 136:20

And [ʿÔḡ](../../strongs/h/h5747.md) the [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md):

<a name="psalms_136_21"></a>Psalms 136:21

And [nathan](../../strongs/h/h5414.md) their ['erets](../../strongs/h/h776.md) for a [nachalah](../../strongs/h/h5159.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md):

<a name="psalms_136_22"></a>Psalms 136:22

Even a [nachalah](../../strongs/h/h5159.md) unto [Yisra'el](../../strongs/h/h3478.md) his ['ebed](../../strongs/h/h5650.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_136_23"></a>Psalms 136:23

Who [zakar](../../strongs/h/h2142.md) us in our low [šep̄el](../../strongs/h/h8216.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md):

<a name="psalms_136_24"></a>Psalms 136:24

And hath [paraq](../../strongs/h/h6561.md) us from our [tsar](../../strongs/h/h6862.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_136_25"></a>Psalms 136:25

Who [nathan](../../strongs/h/h5414.md) [lechem](../../strongs/h/h3899.md) to all [basar](../../strongs/h/h1320.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_136_26"></a>Psalms 136:26

[yadah](../../strongs/h/h3034.md) unto the ['el](../../strongs/h/h410.md) of [shamayim](../../strongs/h/h8064.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 135](psalms_135.md) - [Psalms 137](psalms_137.md)