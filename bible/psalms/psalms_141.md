# [Psalms 141](https://www.blueletterbible.org/kjv/psalms/141)

<a name="psalms_141_1"></a>Psalms 141:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [Yĕhovah](../../strongs/h/h3068.md), I [qara'](../../strongs/h/h7121.md) unto thee: [ḥûš](../../strongs/h/h2363.md) unto me; ['azan](../../strongs/h/h238.md) unto my [qowl](../../strongs/h/h6963.md), when I [qara'](../../strongs/h/h7121.md) unto thee.

<a name="psalms_141_2"></a>Psalms 141:2

Let my [tĕphillah](../../strongs/h/h8605.md) be set [kuwn](../../strongs/h/h3559.md) [paniym](../../strongs/h/h6440.md) thee as [qᵊṭōreṯ](../../strongs/h/h7004.md); and the [maśśᵊ'ēṯ](../../strongs/h/h4864.md) of my [kaph](../../strongs/h/h3709.md) as the ['ereb](../../strongs/h/h6153.md) [minchah](../../strongs/h/h4503.md).

<a name="psalms_141_3"></a>Psalms 141:3

[shiyth](../../strongs/h/h7896.md) a [šāmrâ](../../strongs/h/h8108.md), [Yĕhovah](../../strongs/h/h3068.md), before my [peh](../../strongs/h/h6310.md); [nāṣar](../../strongs/h/h5341.md) the [deleṯ](../../strongs/h/h1817.md) of my [saphah](../../strongs/h/h8193.md).

<a name="psalms_141_4"></a>Psalms 141:4

[natah](../../strongs/h/h5186.md) not my [leb](../../strongs/h/h3820.md) to any [ra'](../../strongs/h/h7451.md) [dabar](../../strongs/h/h1697.md), to [ʿālal](../../strongs/h/h5953.md) [resha'](../../strongs/h/h7562.md) ['aliylah](../../strongs/h/h5949.md) with ['iysh](../../strongs/h/h376.md) that [pa'al](../../strongs/h/h6466.md) ['aven](../../strongs/h/h205.md): and let me not [lāḥam](../../strongs/h/h3898.md) of their [manʿammîm](../../strongs/h/h4516.md).

<a name="psalms_141_5"></a>Psalms 141:5

Let the [tsaddiyq](../../strongs/h/h6662.md) [hālam](../../strongs/h/h1986.md) me; it shall be a [checed](../../strongs/h/h2617.md): and let him [yakach](../../strongs/h/h3198.md) me; it shall be a [ro'sh](../../strongs/h/h7218.md) [šemen](../../strongs/h/h8081.md), which shall not [nô'](../../strongs/h/h5106.md) my [ro'sh](../../strongs/h/h7218.md): for yet my [tĕphillah](../../strongs/h/h8605.md) also shall be in their [ra'](../../strongs/h/h7451.md).

<a name="psalms_141_6"></a>Psalms 141:6

When their [shaphat](../../strongs/h/h8199.md) are [šāmaṭ](../../strongs/h/h8058.md) in [cela'](../../strongs/h/h5553.md) [yad](../../strongs/h/h3027.md), they shall [shama'](../../strongs/h/h8085.md) my ['emer](../../strongs/h/h561.md); for they are [nāʿēm](../../strongs/h/h5276.md).

<a name="psalms_141_7"></a>Psalms 141:7

Our ['etsem](../../strongs/h/h6106.md) are [p̄zr](../../strongs/h/h6340.md) at the [shĕ'owl](../../strongs/h/h7585.md) [peh](../../strongs/h/h6310.md), as when one [pālaḥ](../../strongs/h/h6398.md) and [bāqaʿ](../../strongs/h/h1234.md) wood upon the ['erets](../../strongs/h/h776.md).

<a name="psalms_141_8"></a>Psalms 141:8

But mine ['ayin](../../strongs/h/h5869.md) are unto thee, [Yᵊhōvâ](../../strongs/h/h3069.md) the ['adonay](../../strongs/h/h136.md): in thee is my [chacah](../../strongs/h/h2620.md); [ʿārâ](../../strongs/h/h6168.md) not my [nephesh](../../strongs/h/h5315.md) [ʿārâ](../../strongs/h/h6168.md).

<a name="psalms_141_9"></a>Psalms 141:9

[shamar](../../strongs/h/h8104.md) me from the [yad](../../strongs/h/h3027.md) [paḥ](../../strongs/h/h6341.md) which they have [yāqōš](../../strongs/h/h3369.md) for me, and the [mowqesh](../../strongs/h/h4170.md) of the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md).

<a name="psalms_141_10"></a>Psalms 141:10

Let the [rasha'](../../strongs/h/h7563.md) [naphal](../../strongs/h/h5307.md) into their own [miḵmār](../../strongs/h/h4364.md), whilst that I [yaḥaḏ](../../strongs/h/h3162.md) ['abar](../../strongs/h/h5674.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 140](psalms_140.md) - [Psalms 142](psalms_142.md)