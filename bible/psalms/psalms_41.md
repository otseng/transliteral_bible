# [Psalms 41](https://www.blueletterbible.org/kjv/psa/41)

<a name="psalms_41_1"></a>Psalms 41:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). ['esher](../../strongs/h/h835.md) is he that [sakal](../../strongs/h/h7919.md) the [dal](../../strongs/h/h1800.md): [Yĕhovah](../../strongs/h/h3068.md) will [mālaṭ](../../strongs/h/h4422.md) him in [yowm](../../strongs/h/h3117.md) of [ra'](../../strongs/h/h7451.md).

<a name="psalms_41_2"></a>Psalms 41:2

[Yĕhovah](../../strongs/h/h3068.md) will [shamar](../../strongs/h/h8104.md) him, and keep him [ḥāyâ](../../strongs/h/h2421.md); and he shall be ['āšar](../../strongs/h/h833.md) upon the ['erets](../../strongs/h/h776.md): and thou wilt not [nathan](../../strongs/h/h5414.md) him unto the [nephesh](../../strongs/h/h5315.md) of his ['oyeb](../../strongs/h/h341.md).

<a name="psalms_41_3"></a>Psalms 41:3

[Yĕhovah](../../strongs/h/h3068.md) will [sāʿaḏ](../../strongs/h/h5582.md) him upon the ['eres](../../strongs/h/h6210.md) of [dᵊvay](../../strongs/h/h1741.md): thou wilt [hāp̄aḵ](../../strongs/h/h2015.md) all his [miškāḇ](../../strongs/h/h4904.md) in his [ḥŏlî](../../strongs/h/h2483.md).

<a name="psalms_41_4"></a>Psalms 41:4

I ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md), be [chanan](../../strongs/h/h2603.md) unto me: [rapha'](../../strongs/h/h7495.md) my [nephesh](../../strongs/h/h5315.md); for I have [chata'](../../strongs/h/h2398.md) against thee.

<a name="psalms_41_5"></a>Psalms 41:5

Mine ['oyeb](../../strongs/h/h341.md) ['āmar](../../strongs/h/h559.md) [ra'](../../strongs/h/h7451.md) of me, When shall he [muwth](../../strongs/h/h4191.md), and his [shem](../../strongs/h/h8034.md) ['abad](../../strongs/h/h6.md)?

<a name="psalms_41_6"></a>Psalms 41:6

And if he [bow'](../../strongs/h/h935.md) to [ra'ah](../../strongs/h/h7200.md) me, he [dabar](../../strongs/h/h1696.md) [shav'](../../strongs/h/h7723.md): his [leb](../../strongs/h/h3820.md) [qāḇaṣ](../../strongs/h/h6908.md) ['aven](../../strongs/h/h205.md) to itself; when he [yāṣā'](../../strongs/h/h3318.md) [ḥûṣ](../../strongs/h/h2351.md), he [dabar](../../strongs/h/h1696.md) it.

<a name="psalms_41_7"></a>Psalms 41:7

All that [sane'](../../strongs/h/h8130.md) me [lāḥaš](../../strongs/h/h3907.md) [yaḥaḏ](../../strongs/h/h3162.md) against me: against me do they [chashab](../../strongs/h/h2803.md) my [ra'](../../strongs/h/h7451.md).

<a name="psalms_41_8"></a>Psalms 41:8

A [beliya'al](../../strongs/h/h1100.md) [dabar](../../strongs/h/h1697.md), say they, [yāṣaq](../../strongs/h/h3332.md) unto him: and now that he [shakab](../../strongs/h/h7901.md) he shall [quwm](../../strongs/h/h6965.md) no [yāsap̄](../../strongs/h/h3254.md).

<a name="psalms_41_9"></a>Psalms 41:9

Yea, mine own [shalowm](../../strongs/h/h7965.md) ['iysh](../../strongs/h/h376.md), in whom I [batach](../../strongs/h/h982.md), which did ['akal](../../strongs/h/h398.md) of my [lechem](../../strongs/h/h3899.md), hath [gāḏal](../../strongs/h/h1431.md) his ['aqeb](../../strongs/h/h6119.md) against me.

<a name="psalms_41_10"></a>Psalms 41:10

But thou, [Yĕhovah](../../strongs/h/h3068.md), be [chanan](../../strongs/h/h2603.md) unto me, and [quwm](../../strongs/h/h6965.md) me, that I may [shalam](../../strongs/h/h7999.md) them.

<a name="psalms_41_11"></a>Psalms 41:11

By this I [yada'](../../strongs/h/h3045.md) that thou [ḥāp̄ēṣ](../../strongs/h/h2654.md) me, because mine ['oyeb](../../strongs/h/h341.md) doth not [rûaʿ](../../strongs/h/h7321.md) over me.

<a name="psalms_41_12"></a>Psalms 41:12

And as for me, thou [tamak](../../strongs/h/h8551.md) me in mine [tom](../../strongs/h/h8537.md), and [nāṣaḇ](../../strongs/h/h5324.md) me before thy [paniym](../../strongs/h/h6440.md) ['owlam](../../strongs/h/h5769.md).

<a name="psalms_41_13"></a>Psalms 41:13

[barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) from ['owlam](../../strongs/h/h5769.md), and to ['owlam](../../strongs/h/h5769.md). ['amen](../../strongs/h/h543.md), and ['amen](../../strongs/h/h543.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 40](psalms_40.md) - [Psalms 42](psalms_42.md)