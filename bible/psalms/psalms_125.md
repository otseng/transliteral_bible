# [Psalms 125](https://www.blueletterbible.org/kjv/psalms/125)

<a name="psalms_125_1"></a>Psalms 125:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md). They that [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md) shall be as [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md), which cannot be [mowt](../../strongs/h/h4131.md), but [yashab](../../strongs/h/h3427.md) ['owlam](../../strongs/h/h5769.md).

<a name="psalms_125_2"></a>Psalms 125:2

As the [har](../../strongs/h/h2022.md) are [cabiyb](../../strongs/h/h5439.md) [Yĕruwshalaim](../../strongs/h/h3389.md), so [Yĕhovah](../../strongs/h/h3068.md) is [cabiyb](../../strongs/h/h5439.md) his ['am](../../strongs/h/h5971.md) from henceforth even [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md).

<a name="psalms_125_3"></a>Psalms 125:3

For the [shebet](../../strongs/h/h7626.md) of the [resha'](../../strongs/h/h7562.md) shall not [nuwach](../../strongs/h/h5117.md) upon the [gôrāl](../../strongs/h/h1486.md) of the [tsaddiyq](../../strongs/h/h6662.md); [maʿan](../../strongs/h/h4616.md) the [tsaddiyq](../../strongs/h/h6662.md) [shalach](../../strongs/h/h7971.md) their [yad](../../strongs/h/h3027.md) unto ['evel](../../strongs/h/h5766.md).

<a name="psalms_125_4"></a>Psalms 125:4

Do [ṭôḇ](../../strongs/h/h2895.md), [Yĕhovah](../../strongs/h/h3068.md), unto those that be [towb](../../strongs/h/h2896.md), and to them that are [yashar](../../strongs/h/h3477.md) in their [libbah](../../strongs/h/h3826.md).

<a name="psalms_125_5"></a>Psalms 125:5

As for such as [natah](../../strongs/h/h5186.md) unto their [ʿăqalqāl](../../strongs/h/h6128.md), [Yĕhovah](../../strongs/h/h3068.md) shall lead them [yālaḵ](../../strongs/h/h3212.md) with the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md): but [shalowm](../../strongs/h/h7965.md) shall be upon [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 124](psalms_124.md) - [Psalms 126](psalms_126.md)