# [Psalms 45](https://www.blueletterbible.org/kjv/psa/45)

<a name="psalms_45_1"></a>Psalms 45:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon [šûšan](../../strongs/h/h7799.md), for the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md), [Maśkîl](../../strongs/h/h4905.md), A [šîr](../../strongs/h/h7892.md) of [yāḏîḏ](../../strongs/h/h3039.md). My [leb](../../strongs/h/h3820.md) is [rāḥaš](../../strongs/h/h7370.md) a [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md): I ['āmar](../../strongs/h/h559.md) of the things which I have [ma'aseh](../../strongs/h/h4639.md) touching the [melek](../../strongs/h/h4428.md): my [lashown](../../strongs/h/h3956.md) is the [ʿēṭ](../../strongs/h/h5842.md) of a [māhîr](../../strongs/h/h4106.md) [sāp̄ar](../../strongs/h/h5608.md).

<a name="psalms_45_2"></a>Psalms 45:2

Thou art [yāp̄â](../../strongs/h/h3302.md) than the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md): [ḥēn](../../strongs/h/h2580.md) is [yāṣaq](../../strongs/h/h3332.md) into thy [saphah](../../strongs/h/h8193.md): therefore ['Elohiym](../../strongs/h/h430.md) hath [barak](../../strongs/h/h1288.md) thee ['owlam](../../strongs/h/h5769.md).

<a name="psalms_45_3"></a>Psalms 45:3

[ḥāḡar](../../strongs/h/h2296.md) thy [chereb](../../strongs/h/h2719.md) upon thy [yārēḵ](../../strongs/h/h3409.md), O [gibôr](../../strongs/h/h1368.md), with thy [howd](../../strongs/h/h1935.md) and thy [hadar](../../strongs/h/h1926.md).

<a name="psalms_45_4"></a>Psalms 45:4

And in thy [hadar](../../strongs/h/h1926.md) [rāḵaḇ](../../strongs/h/h7392.md) [tsalach](../../strongs/h/h6743.md) because of ['emeth](../../strongs/h/h571.md) and [ʿanvâ](../../strongs/h/h6037.md) and [tsedeq](../../strongs/h/h6664.md); and thy [yamiyn](../../strongs/h/h3225.md) shall [yārâ](../../strongs/h/h3384.md) thee [yare'](../../strongs/h/h3372.md).

<a name="psalms_45_5"></a>Psalms 45:5

Thine [chets](../../strongs/h/h2671.md) are [šānan](../../strongs/h/h8150.md) in the [leb](../../strongs/h/h3820.md) of the [melek](../../strongs/h/h4428.md) ['oyeb](../../strongs/h/h341.md); whereby the ['am](../../strongs/h/h5971.md) [naphal](../../strongs/h/h5307.md) under thee.

<a name="psalms_45_6"></a>Psalms 45:6

Thy [kicce'](../../strongs/h/h3678.md), ['Elohiym](../../strongs/h/h430.md), is ['owlam](../../strongs/h/h5769.md) and [ʿaḏ](../../strongs/h/h5703.md): the [shebet](../../strongs/h/h7626.md) of thy [malkuwth](../../strongs/h/h4438.md) is a [mîšôr](../../strongs/h/h4334.md) [shebet](../../strongs/h/h7626.md).

<a name="psalms_45_7"></a>Psalms 45:7

Thou ['ahab](../../strongs/h/h157.md) [tsedeq](../../strongs/h/h6664.md), and [sane'](../../strongs/h/h8130.md) [resha'](../../strongs/h/h7562.md): therefore ['Elohiym](../../strongs/h/h430.md), thy ['Elohiym](../../strongs/h/h430.md), hath [māšaḥ](../../strongs/h/h4886.md) thee with the [šemen](../../strongs/h/h8081.md) of [śāśôn](../../strongs/h/h8342.md) above thy [ḥāḇēr](../../strongs/h/h2270.md).

<a name="psalms_45_8"></a>Psalms 45:8

All thy [beḡeḏ](../../strongs/h/h899.md) smell of [mōr](../../strongs/h/h4753.md), and ['ăhālîm](../../strongs/h/h174.md), and [qᵊṣîʿâ](../../strongs/h/h7102.md), out of the [šēn](../../strongs/h/h8127.md) [heykal](../../strongs/h/h1964.md), whereby they have made thee [samach](../../strongs/h/h8055.md).

<a name="psalms_45_9"></a>Psalms 45:9

[melek](../../strongs/h/h4428.md) [bath](../../strongs/h/h1323.md) were among thy [yāqār](../../strongs/h/h3368.md): upon thy [yamiyn](../../strongs/h/h3225.md) did [nāṣaḇ](../../strongs/h/h5324.md) the [šēḡāl](../../strongs/h/h7694.md) in [keṯem](../../strongs/h/h3800.md) of Ophir.

<a name="psalms_45_10"></a>Psalms 45:10

[shama'](../../strongs/h/h8085.md), O [bath](../../strongs/h/h1323.md), and [ra'ah](../../strongs/h/h7200.md), and [natah](../../strongs/h/h5186.md) thine ['ozen](../../strongs/h/h241.md); [shakach](../../strongs/h/h7911.md) also thine own ['am](../../strongs/h/h5971.md), and thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md);

<a name="psalms_45_11"></a>Psalms 45:11

So shall the [melek](../../strongs/h/h4428.md) ['āvâ](../../strongs/h/h183.md) thy [yᵊp̄î](../../strongs/h/h3308.md): for he is thy ['adown](../../strongs/h/h113.md); and [shachah](../../strongs/h/h7812.md) thou him.

<a name="psalms_45_12"></a>Psalms 45:12

And the [bath](../../strongs/h/h1323.md) of [Ṣōr](../../strongs/h/h6865.md) shall be there with a [minchah](../../strongs/h/h4503.md); even the [ʿāšîr](../../strongs/h/h6223.md) among the ['am](../../strongs/h/h5971.md) shall [ḥālâ](../../strongs/h/h2470.md) thy [paniym](../../strongs/h/h6440.md).

<a name="psalms_45_13"></a>Psalms 45:13

The [melek](../../strongs/h/h4428.md) [bath](../../strongs/h/h1323.md) is all [kᵊḇûdâ](../../strongs/h/h3520.md) [pᵊnîmâ](../../strongs/h/h6441.md): her [lᵊḇûš](../../strongs/h/h3830.md) is of [mišbᵊṣôṯ](../../strongs/h/h4865.md) [zāhāḇ](../../strongs/h/h2091.md).

<a name="psalms_45_14"></a>Psalms 45:14

She shall be [yāḇal](../../strongs/h/h2986.md) unto the [melek](../../strongs/h/h4428.md) in [riqmâ](../../strongs/h/h7553.md): the [bᵊṯûlâ](../../strongs/h/h1330.md) her [rēʿâ](../../strongs/h/h7464.md) that ['aḥar](../../strongs/h/h310.md) her shall be [bow'](../../strongs/h/h935.md) unto thee.

<a name="psalms_45_15"></a>Psalms 45:15

With [simchah](../../strongs/h/h8057.md) and [gîl](../../strongs/h/h1524.md) shall they be [yāḇal](../../strongs/h/h2986.md): they shall [bow'](../../strongs/h/h935.md) into the [melek](../../strongs/h/h4428.md) [heykal](../../strongs/h/h1964.md).

<a name="psalms_45_16"></a>Psalms 45:16

Instead of thy ['ab](../../strongs/h/h1.md) shall be thy [ben](../../strongs/h/h1121.md), whom thou mayest [shiyth](../../strongs/h/h7896.md) [śar](../../strongs/h/h8269.md) in all the ['erets](../../strongs/h/h776.md).

<a name="psalms_45_17"></a>Psalms 45:17

I will make thy [shem](../../strongs/h/h8034.md) to be [zakar](../../strongs/h/h2142.md) in [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md): therefore shall the ['am](../../strongs/h/h5971.md) [yadah](../../strongs/h/h3034.md) thee ['owlam](../../strongs/h/h5769.md) and [ʿaḏ](../../strongs/h/h5703.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 44](psalms_44.md) - [Psalms 46](psalms_46.md)