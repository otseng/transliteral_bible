# [Psalms 121](https://www.blueletterbible.org/kjv/psalms/121)

<a name="psalms_121_1"></a>Psalms 121:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md). I will [nasa'](../../strongs/h/h5375.md) mine ['ayin](../../strongs/h/h5869.md) unto the [har](../../strongs/h/h2022.md), from ['ayin](../../strongs/h/h370.md) [bow'](../../strongs/h/h935.md) my ['ezer](../../strongs/h/h5828.md).

<a name="psalms_121_2"></a>Psalms 121:2

My ['ezer](../../strongs/h/h5828.md) cometh from [Yĕhovah](../../strongs/h/h3068.md), which ['asah](../../strongs/h/h6213.md) [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md).

<a name="psalms_121_3"></a>Psalms 121:3

He will not [nathan](../../strongs/h/h5414.md) thy [regel](../../strongs/h/h7272.md) to be [môṭ](../../strongs/h/h4132.md): he that [shamar](../../strongs/h/h8104.md) thee will not [nûm](../../strongs/h/h5123.md).

<a name="psalms_121_4"></a>Psalms 121:4

Behold, he that [shamar](../../strongs/h/h8104.md) [Yisra'el](../../strongs/h/h3478.md) shall neither [nûm](../../strongs/h/h5123.md) nor [yashen](../../strongs/h/h3462.md).

<a name="psalms_121_5"></a>Psalms 121:5

[Yĕhovah](../../strongs/h/h3068.md) is thy [shamar](../../strongs/h/h8104.md): [Yĕhovah](../../strongs/h/h3068.md) is thy [ṣēl](../../strongs/h/h6738.md) upon thy [yamiyn](../../strongs/h/h3225.md) [yad](../../strongs/h/h3027.md).

<a name="psalms_121_6"></a>Psalms 121:6

The [šemeš](../../strongs/h/h8121.md) shall not [nakah](../../strongs/h/h5221.md) thee by [yômām](../../strongs/h/h3119.md), nor the [yareach](../../strongs/h/h3394.md) by [layil](../../strongs/h/h3915.md).

<a name="psalms_121_7"></a>Psalms 121:7

[Yĕhovah](../../strongs/h/h3068.md) shall [shamar](../../strongs/h/h8104.md) thee from all [ra'](../../strongs/h/h7451.md): he shall [shamar](../../strongs/h/h8104.md) thy [nephesh](../../strongs/h/h5315.md).

<a name="psalms_121_8"></a>Psalms 121:8

[Yĕhovah](../../strongs/h/h3068.md) shall [shamar](../../strongs/h/h8104.md) thy [yāṣā'](../../strongs/h/h3318.md) and thy [bow'](../../strongs/h/h935.md) from this time forth, and even [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 120](psalms_120.md) - [Psalms 122](psalms_122.md)