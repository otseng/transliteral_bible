# [Psalms 66](https://www.blueletterbible.org/kjv/psa/66)

<a name="psalms_66_1"></a>Psalms 66:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [šîr](../../strongs/h/h7892.md) [or] [mizmôr](../../strongs/h/h4210.md). Make a [rûaʿ](../../strongs/h/h7321.md) unto ['Elohiym](../../strongs/h/h430.md), all ye ['erets](../../strongs/h/h776.md):

<a name="psalms_66_2"></a>Psalms 66:2

[zamar](../../strongs/h/h2167.md) the [kabowd](../../strongs/h/h3519.md) of his [shem](../../strongs/h/h8034.md): [śûm](../../strongs/h/h7760.md) his [tehillah](../../strongs/h/h8416.md) [kabowd](../../strongs/h/h3519.md).

<a name="psalms_66_3"></a>Psalms 66:3

['āmar](../../strongs/h/h559.md) unto ['Elohiym](../../strongs/h/h430.md), How [yare'](../../strongs/h/h3372.md) art thou in thy [ma'aseh](../../strongs/h/h4639.md)! through the [rōḇ](../../strongs/h/h7230.md) of thy ['oz](../../strongs/h/h5797.md) shall thine ['oyeb](../../strongs/h/h341.md) [kāḥaš](../../strongs/h/h3584.md) themselves unto thee.

<a name="psalms_66_4"></a>Psalms 66:4

All the ['erets](../../strongs/h/h776.md) shall [shachah](../../strongs/h/h7812.md) thee, and shall [zamar](../../strongs/h/h2167.md) unto thee; they shall [zamar](../../strongs/h/h2167.md) to thy [shem](../../strongs/h/h8034.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_66_5"></a>Psalms 66:5

[yālaḵ](../../strongs/h/h3212.md) and [ra'ah](../../strongs/h/h7200.md) the [mip̄ʿāl](../../strongs/h/h4659.md) of ['Elohiym](../../strongs/h/h430.md): he is [yare'](../../strongs/h/h3372.md) in his ['aliylah](../../strongs/h/h5949.md) toward the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md).

<a name="psalms_66_6"></a>Psalms 66:6

He [hāp̄aḵ](../../strongs/h/h2015.md) the [yam](../../strongs/h/h3220.md) into [yabāšâ](../../strongs/h/h3004.md) land: they ['abar](../../strongs/h/h5674.md) through the [nāhār](../../strongs/h/h5104.md) on [regel](../../strongs/h/h7272.md): there did we [samach](../../strongs/h/h8055.md) in him.

<a name="psalms_66_7"></a>Psalms 66:7

He [mashal](../../strongs/h/h4910.md) by his [gᵊḇûrâ](../../strongs/h/h1369.md) ['owlam](../../strongs/h/h5769.md); his ['ayin](../../strongs/h/h5869.md) [tsaphah](../../strongs/h/h6822.md) the [gowy](../../strongs/h/h1471.md): let not the [sārar](../../strongs/h/h5637.md) [ruwm](../../strongs/h/h7311.md) [ruwm](../../strongs/h/h7311.md) themselves. [Celah](../../strongs/h/h5542.md).

<a name="psalms_66_8"></a>Psalms 66:8

O [barak](../../strongs/h/h1288.md) our ['Elohiym](../../strongs/h/h430.md), ye ['am](../../strongs/h/h5971.md), and make the [qowl](../../strongs/h/h6963.md) of his [tehillah](../../strongs/h/h8416.md) to be [shama'](../../strongs/h/h8085.md):

<a name="psalms_66_9"></a>Psalms 66:9

Which [śûm](../../strongs/h/h7760.md) our [nephesh](../../strongs/h/h5315.md) in [chay](../../strongs/h/h2416.md), and [nathan](../../strongs/h/h5414.md) not our [regel](../../strongs/h/h7272.md) to be [môṭ](../../strongs/h/h4132.md).

<a name="psalms_66_10"></a>Psalms 66:10

For thou, ['Elohiym](../../strongs/h/h430.md), hast [bachan](../../strongs/h/h974.md) us: thou hast [tsaraph](../../strongs/h/h6884.md) us, as [keceph](../../strongs/h/h3701.md) is [tsaraph](../../strongs/h/h6884.md).

<a name="psalms_66_11"></a>Psalms 66:11

Thou [bow'](../../strongs/h/h935.md) us into the [matsuwd](../../strongs/h/h4686.md); thou [śûm](../../strongs/h/h7760.md) [mûʿāqâ](../../strongs/h/h4157.md) upon our [māṯnayim](../../strongs/h/h4975.md).

<a name="psalms_66_12"></a>Psalms 66:12

Thou hast caused ['enowsh](../../strongs/h/h582.md) to [rāḵaḇ](../../strongs/h/h7392.md) over our [ro'sh](../../strongs/h/h7218.md); we [bow'](../../strongs/h/h935.md) through ['esh](../../strongs/h/h784.md) and through [mayim](../../strongs/h/h4325.md): but thou [yāṣā'](../../strongs/h/h3318.md) us into a [rᵊvāyâ](../../strongs/h/h7310.md) place.

<a name="psalms_66_13"></a>Psalms 66:13

I will [bow'](../../strongs/h/h935.md) into thy [bayith](../../strongs/h/h1004.md) with [ʿōlâ](../../strongs/h/h5930.md): I will [shalam](../../strongs/h/h7999.md) thee my [neḏer](../../strongs/h/h5088.md),

<a name="psalms_66_14"></a>Psalms 66:14

Which my [saphah](../../strongs/h/h8193.md) have [pāṣâ](../../strongs/h/h6475.md), and my [peh](../../strongs/h/h6310.md) hath [dabar](../../strongs/h/h1696.md), when I was in [tsar](../../strongs/h/h6862.md).

<a name="psalms_66_15"></a>Psalms 66:15

I will [ʿālâ](../../strongs/h/h5927.md) unto thee [ʿōlâ](../../strongs/h/h5930.md) of [mēaḥ](../../strongs/h/h4220.md), with the [qᵊṭōreṯ](../../strongs/h/h7004.md) of ['ayil](../../strongs/h/h352.md); I will ['asah](../../strongs/h/h6213.md) [bāqār](../../strongs/h/h1241.md) with [ʿatûḏ](../../strongs/h/h6260.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_66_16"></a>Psalms 66:16

[yālaḵ](../../strongs/h/h3212.md) and [shama'](../../strongs/h/h8085.md), all ye that [yārē'](../../strongs/h/h3373.md) ['Elohiym](../../strongs/h/h430.md), and I will [sāp̄ar](../../strongs/h/h5608.md) what he hath ['asah](../../strongs/h/h6213.md) for my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_66_17"></a>Psalms 66:17

I [qara'](../../strongs/h/h7121.md) unto him with my [peh](../../strongs/h/h6310.md), and he was [ruwm](../../strongs/h/h7311.md) [rômām](../../strongs/h/h7318.md) [taḥaṯ](../../strongs/h/h8478.md) my [lashown](../../strongs/h/h3956.md).

<a name="psalms_66_18"></a>Psalms 66:18

If I [ra'ah](../../strongs/h/h7200.md) ['aven](../../strongs/h/h205.md) in my [leb](../../strongs/h/h3820.md), the ['adonay](../../strongs/h/h136.md) will not [shama'](../../strongs/h/h8085.md) me:

<a name="psalms_66_19"></a>Psalms 66:19

But ['āḵēn](../../strongs/h/h403.md) ['Elohiym](../../strongs/h/h430.md) hath [shama'](../../strongs/h/h8085.md) me; he hath [qashab](../../strongs/h/h7181.md) to the [qowl](../../strongs/h/h6963.md) of my [tĕphillah](../../strongs/h/h8605.md).

<a name="psalms_66_20"></a>Psalms 66:20

[barak](../../strongs/h/h1288.md) be ['Elohiym](../../strongs/h/h430.md), which hath not [cuwr](../../strongs/h/h5493.md) my [tĕphillah](../../strongs/h/h8605.md), nor his [checed](../../strongs/h/h2617.md) from me.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 65](psalms_65.md) - [Psalms 67](psalms_67.md)