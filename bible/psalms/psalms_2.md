# [Psalms 2](https://www.blueletterbible.org/kjv/psa/2/1/s_480001)

<a name="psalms_2_1"></a>Psalms 2:1

Why do the [gowy](../../strongs/h/h1471.md) [ragash](../../strongs/h/h7283.md), and the [lĕom](../../strongs/h/h3816.md) [hagah](../../strongs/h/h1897.md) a [riyq](../../strongs/h/h7385.md)?

<a name="psalms_2_2"></a>Psalms 2:2

The [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md) set themselves, and the [razan](../../strongs/h/h7336.md) [yacad](../../strongs/h/h3245.md) together, against [Yĕhovah](../../strongs/h/h3068.md), and against his [mashiyach](../../strongs/h/h4899.md), saying,

<a name="psalms_2_3"></a>Psalms 2:3

Let us [nathaq](../../strongs/h/h5423.md) their [mowcer](../../strongs/h/h4147.md), and [shalak](../../strongs/h/h7993.md) their [ʿăḇōṯ](../../strongs/h/h5688.md) from us.

<a name="psalms_2_4"></a>Psalms 2:4

He that [yashab](../../strongs/h/h3427.md) in the [shamayim](../../strongs/h/h8064.md) shall [śāḥaq](../../strongs/h/h7832.md): ['adonay](../../strongs/h/h136.md) shall [lāʿaḡ](../../strongs/h/h3932.md) them.

<a name="psalms_2_5"></a>Psalms 2:5

Then shall he [dabar](../../strongs/h/h1696.md) unto them in his ['aph](../../strongs/h/h639.md), and [bahal](../../strongs/h/h926.md) them in his [charown](../../strongs/h/h2740.md).

<a name="psalms_2_6"></a>Psalms 2:6

Yet have I set my [melek](../../strongs/h/h4428.md) upon my [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md) of [Tsiyown](../../strongs/h/h6726.md).

<a name="psalms_2_7"></a>Psalms 2:7

I will [sāp̄ar](../../strongs/h/h5608.md) the [choq](../../strongs/h/h2706.md): [Yĕhovah](../../strongs/h/h3068.md) hath ['āmar](../../strongs/h/h559.md) unto me, Thou art my [ben](../../strongs/h/h1121.md); this [yowm](../../strongs/h/h3117.md) have I [yalad](../../strongs/h/h3205.md) thee.

<a name="psalms_2_8"></a>Psalms 2:8

[sha'al](../../strongs/h/h7592.md) of me, and I shall [nathan](../../strongs/h/h5414.md) the [gowy](../../strongs/h/h1471.md) thine [nachalah](../../strongs/h/h5159.md), and the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md) thy ['achuzzah](../../strongs/h/h272.md).

<a name="psalms_2_9"></a>Psalms 2:9

Thou shalt [ra'a'](../../strongs/h/h7489.md) them with a [shebet](../../strongs/h/h7626.md) of [barzel](../../strongs/h/h1270.md); thou shalt [naphats](../../strongs/h/h5310.md) like a [yāṣar](../../strongs/h/h3335.md) [kĕliy](../../strongs/h/h3627.md).

<a name="psalms_2_10"></a>Psalms 2:10

Be [sakal](../../strongs/h/h7919.md) now therefore, O ye [melek](../../strongs/h/h4428.md): be [yacar](../../strongs/h/h3256.md), ye [shaphat](../../strongs/h/h8199.md) of the ['erets](../../strongs/h/h776.md).

<a name="psalms_2_11"></a>Psalms 2:11

['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) with [yir'ah](../../strongs/h/h3374.md), and [giyl](../../strongs/h/h1523.md) with [ra'ad](../../strongs/h/h7461.md).

<a name="psalms_2_12"></a>Psalms 2:12

[nashaq](../../strongs/h/h5401.md) the [bar](../../strongs/h/h1248.md), lest he be ['anaph](../../strongs/h/h599.md), and ye ['abad](../../strongs/h/h6.md) from the way, when his ['aph](../../strongs/h/h639.md) is [bāʿar](../../strongs/h/h1197.md) but a [mᵊʿaṭ](../../strongs/h/h4592.md). ['esher](../../strongs/h/h835.md) are all they that put their [chacah](../../strongs/h/h2620.md) in him.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 1](psalms_1.md) - [Psalms 3](psalms_3.md)