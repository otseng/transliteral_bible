# [Psalms 35](https://www.blueletterbible.org/kjv/psa/35/1/s_513001)

<a name="psalms_35_1"></a>Psalms 35:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [riyb](../../strongs/h/h7378.md) my cause, [Yĕhovah](../../strongs/h/h3068.md), with them that [yārîḇ](../../strongs/h/h3401.md) with me: [lāḥam](../../strongs/h/h3898.md) against them that [lāḥam](../../strongs/h/h3898.md) against me.

<a name="psalms_35_2"></a>Psalms 35:2

[ḥāzaq](../../strongs/h/h2388.md) of [magen](../../strongs/h/h4043.md) and [tsinnah](../../strongs/h/h6793.md), and [quwm](../../strongs/h/h6965.md) for mine [ʿezrâ](../../strongs/h/h5833.md).

<a name="psalms_35_3"></a>Psalms 35:3

[rîq](../../strongs/h/h7324.md) also the [ḥănîṯ](../../strongs/h/h2595.md), and [cagar](../../strongs/h/h5462.md) the way [qārā'](../../strongs/h/h7125.md) them that [radaph](../../strongs/h/h7291.md) me: ['āmar](../../strongs/h/h559.md) unto my [nephesh](../../strongs/h/h5315.md), I am thy [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="psalms_35_4"></a>Psalms 35:4

Let them be [buwsh](../../strongs/h/h954.md) and [kālam](../../strongs/h/h3637.md) that [bāqaš](../../strongs/h/h1245.md) after my [nephesh](../../strongs/h/h5315.md): let them be [sûḡ](../../strongs/h/h5472.md) ['āḥôr](../../strongs/h/h268.md) and [ḥāp̄ēr](../../strongs/h/h2659.md) that [chashab](../../strongs/h/h2803.md) my [ra'](../../strongs/h/h7451.md).

<a name="psalms_35_5"></a>Psalms 35:5

Let them be as [mots](../../strongs/h/h4671.md) [paniym](../../strongs/h/h6440.md) the [ruwach](../../strongs/h/h7307.md): and let the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [dāḥâ](../../strongs/h/h1760.md) them.

<a name="psalms_35_6"></a>Psalms 35:6

Let their [derek](../../strongs/h/h1870.md) be [choshek](../../strongs/h/h2822.md) and [ḥălaqlaqqôṯ](../../strongs/h/h2519.md): and let the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [radaph](../../strongs/h/h7291.md) them.

<a name="psalms_35_7"></a>Psalms 35:7

For [ḥinnām](../../strongs/h/h2600.md) have they [taman](../../strongs/h/h2934.md) for me their [rešeṯ](../../strongs/h/h7568.md) in a [shachath](../../strongs/h/h7845.md), which [ḥinnām](../../strongs/h/h2600.md) they have [chaphar](../../strongs/h/h2658.md) for my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_35_8"></a>Psalms 35:8

Let [šô'](../../strongs/h/h7722.md) [bow'](../../strongs/h/h935.md) upon him [yada'](../../strongs/h/h3045.md) [lō'](../../strongs/h/h3808.md); and let his [rešeṯ](../../strongs/h/h7568.md) that he hath [taman](../../strongs/h/h2934.md) [lāḵaḏ](../../strongs/h/h3920.md) himself: into that very [šô'](../../strongs/h/h7722.md) let him [naphal](../../strongs/h/h5307.md).

<a name="psalms_35_9"></a>Psalms 35:9

And my [nephesh](../../strongs/h/h5315.md) shall be [giyl](../../strongs/h/h1523.md) in [Yĕhovah](../../strongs/h/h3068.md): it shall [śûś](../../strongs/h/h7797.md) in his [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="psalms_35_10"></a>Psalms 35:10

All my ['etsem](../../strongs/h/h6106.md) shall ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md), who is like unto thee, which [natsal](../../strongs/h/h5337.md) the ['aniy](../../strongs/h/h6041.md) from him that is too [ḥāzāq](../../strongs/h/h2389.md) for him, yea, the ['aniy](../../strongs/h/h6041.md) and the ['ebyown](../../strongs/h/h34.md) from him that [gāzal](../../strongs/h/h1497.md) him?

<a name="psalms_35_11"></a>Psalms 35:11

[chamac](../../strongs/h/h2555.md) ['ed](../../strongs/h/h5707.md) did [quwm](../../strongs/h/h6965.md); they [sha'al](../../strongs/h/h7592.md) things that I [yada'](../../strongs/h/h3045.md) not.

<a name="psalms_35_12"></a>Psalms 35:12

They [shalam](../../strongs/h/h7999.md) me [ra'](../../strongs/h/h7451.md) for [towb](../../strongs/h/h2896.md) to the [šᵊḵôl](../../strongs/h/h7908.md) of my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_35_13"></a>Psalms 35:13

But as for me, when they were [ḥālâ](../../strongs/h/h2470.md), my [lᵊḇûš](../../strongs/h/h3830.md) was [śaq](../../strongs/h/h8242.md): I [ʿānâ](../../strongs/h/h6031.md) my [nephesh](../../strongs/h/h5315.md) with [ṣôm](../../strongs/h/h6685.md); and my [tĕphillah](../../strongs/h/h8605.md) [shuwb](../../strongs/h/h7725.md) into mine own [ḥêq](../../strongs/h/h2436.md).

<a name="psalms_35_14"></a>Psalms 35:14

I [halak](../../strongs/h/h1980.md) myself as though he had been my [rea'](../../strongs/h/h7453.md) or ['ach](../../strongs/h/h251.md): I [shachach](../../strongs/h/h7817.md) [qāḏar](../../strongs/h/h6937.md), as one that ['āḇēl](../../strongs/h/h57.md) for his ['em](../../strongs/h/h517.md).

<a name="psalms_35_15"></a>Psalms 35:15

But in mine [ṣelaʿ](../../strongs/h/h6761.md) they [samach](../../strongs/h/h8055.md), and gathered themselves ['āsap̄](../../strongs/h/h622.md): yea, the [nēḵê](../../strongs/h/h5222.md) gathered themselves ['āsap̄](../../strongs/h/h622.md) against me, and I [yada'](../../strongs/h/h3045.md) it not; they did [qāraʿ](../../strongs/h/h7167.md) me, and [damam](../../strongs/h/h1826.md) not:

<a name="psalms_35_16"></a>Psalms 35:16

With [ḥānēp̄](../../strongs/h/h2611.md) [lāʿēḡ](../../strongs/h/h3934.md) in [māʿôḡ](../../strongs/h/h4580.md), they [ḥāraq](../../strongs/h/h2786.md) upon me with their [šēn](../../strongs/h/h8127.md).

<a name="psalms_35_17"></a>Psalms 35:17

['adonay](../../strongs/h/h136.md), how long wilt thou [ra'ah](../../strongs/h/h7200.md) on? [shuwb](../../strongs/h/h7725.md) my [nephesh](../../strongs/h/h5315.md) from their [šô'](../../strongs/h/h7722.md), my [yāḥîḏ](../../strongs/h/h3173.md) from the [kephiyr](../../strongs/h/h3715.md).

<a name="psalms_35_18"></a>Psalms 35:18

I will [yadah](../../strongs/h/h3034.md) thee in the [rab](../../strongs/h/h7227.md) [qāhēl](../../strongs/h/h6951.md): I will [halal](../../strongs/h/h1984.md) thee among ['atsuwm](../../strongs/h/h6099.md) ['am](../../strongs/h/h5971.md).

<a name="psalms_35_19"></a>Psalms 35:19

Let not them that are mine ['oyeb](../../strongs/h/h341.md) [sheqer](../../strongs/h/h8267.md) [samach](../../strongs/h/h8055.md) over me: neither let them [qāraṣ](../../strongs/h/h7169.md) with the ['ayin](../../strongs/h/h5869.md) that [sane'](../../strongs/h/h8130.md) me [ḥinnām](../../strongs/h/h2600.md).

<a name="psalms_35_20"></a>Psalms 35:20

For they [dabar](../../strongs/h/h1696.md) not [shalowm](../../strongs/h/h7965.md): but they [chashab](../../strongs/h/h2803.md) [mirmah](../../strongs/h/h4820.md) [dabar](../../strongs/h/h1697.md) against them that are [rāḡēaʿ](../../strongs/h/h7282.md) in the ['erets](../../strongs/h/h776.md).

<a name="psalms_35_21"></a>Psalms 35:21

Yea, they opened their [peh](../../strongs/h/h6310.md) [rāḥaḇ](../../strongs/h/h7337.md) against me, and ['āmar](../../strongs/h/h559.md), [he'āḥ](../../strongs/h/h1889.md), [he'āḥ](../../strongs/h/h1889.md), our ['ayin](../../strongs/h/h5869.md) hath [ra'ah](../../strongs/h/h7200.md) it.

<a name="psalms_35_22"></a>Psalms 35:22

This thou hast [ra'ah](../../strongs/h/h7200.md), [Yĕhovah](../../strongs/h/h3068.md): keep not [ḥāraš](../../strongs/h/h2790.md): ['adonay](../../strongs/h/h136.md), be not [rachaq](../../strongs/h/h7368.md) from me.

<a name="psalms_35_23"></a>Psalms 35:23

[ʿûr](../../strongs/h/h5782.md) thyself, and [quwts](../../strongs/h/h6974.md) to my [mishpat](../../strongs/h/h4941.md), even unto my [rîḇ](../../strongs/h/h7379.md), my ['Elohiym](../../strongs/h/h430.md) and my ['adonay](../../strongs/h/h136.md).

<a name="psalms_35_24"></a>Psalms 35:24

[shaphat](../../strongs/h/h8199.md) me, [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), according to thy [tsedeq](../../strongs/h/h6664.md); and let them not [samach](../../strongs/h/h8055.md) over me.

<a name="psalms_35_25"></a>Psalms 35:25

Let them not ['āmar](../../strongs/h/h559.md) in their [leb](../../strongs/h/h3820.md), [he'āḥ](../../strongs/h/h1889.md), so would we [nephesh](../../strongs/h/h5315.md): let them not ['āmar](../../strongs/h/h559.md), We have [bālaʿ](../../strongs/h/h1104.md) him.

<a name="psalms_35_26"></a>Psalms 35:26

Let them be [buwsh](../../strongs/h/h954.md) and [ḥāp̄ēr](../../strongs/h/h2659.md) [yaḥaḏ](../../strongs/h/h3162.md) that [śāmēaḥ](../../strongs/h/h8056.md) at mine [ra'](../../strongs/h/h7451.md): let them be [labash](../../strongs/h/h3847.md) with [bšeṯ](../../strongs/h/h1322.md) and [kĕlimmah](../../strongs/h/h3639.md) that [gāḏal](../../strongs/h/h1431.md) themselves against me.

<a name="psalms_35_27"></a>Psalms 35:27

Let them [ranan](../../strongs/h/h7442.md), and be [samach](../../strongs/h/h8055.md), that [chaphets](../../strongs/h/h2655.md) my [tsedeq](../../strongs/h/h6664.md) cause: yea, let them ['āmar](../../strongs/h/h559.md) [tāmîḏ](../../strongs/h/h8548.md), Let [Yĕhovah](../../strongs/h/h3068.md) be [gāḏal](../../strongs/h/h1431.md), which hath [chaphets](../../strongs/h/h2655.md) in the [shalowm](../../strongs/h/h7965.md) of his ['ebed](../../strongs/h/h5650.md).

<a name="psalms_35_28"></a>Psalms 35:28

And my [lashown](../../strongs/h/h3956.md) shall [hagah](../../strongs/h/h1897.md) of thy [tsedeq](../../strongs/h/h6664.md) and of thy [tehillah](../../strongs/h/h8416.md) all the [yowm](../../strongs/h/h3117.md) long.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 34](psalms_34.md) - [Psalms 36](psalms_36.md)