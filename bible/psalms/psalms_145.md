# [Psalms 145](https://www.blueletterbible.org/kjv/psalms/145)

<a name="psalms_145_1"></a>Psalms 145:1

[Dāviḏ](../../strongs/h/h1732.md) Psalm of [tehillah](../../strongs/h/h8416.md). I will [ruwm](../../strongs/h/h7311.md) thee, my ['Elohiym](../../strongs/h/h430.md), [melek](../../strongs/h/h4428.md); and I will [barak](../../strongs/h/h1288.md) thy [shem](../../strongs/h/h8034.md) ['owlam](../../strongs/h/h5769.md) and [ʿaḏ](../../strongs/h/h5703.md).

<a name="psalms_145_2"></a>Psalms 145:2

Every [yowm](../../strongs/h/h3117.md) will I [barak](../../strongs/h/h1288.md) thee; and I will [halal](../../strongs/h/h1984.md) thy [shem](../../strongs/h/h8034.md) ['owlam](../../strongs/h/h5769.md) and [ʿaḏ](../../strongs/h/h5703.md).

<a name="psalms_145_3"></a>Psalms 145:3

[gadowl](../../strongs/h/h1419.md) is [Yĕhovah](../../strongs/h/h3068.md), and [me'od](../../strongs/h/h3966.md) to be [halal](../../strongs/h/h1984.md); and his [gᵊḏûlâ](../../strongs/h/h1420.md) is [ḥēqer](../../strongs/h/h2714.md).

<a name="psalms_145_4"></a>Psalms 145:4

[dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md) shall [shabach](../../strongs/h/h7623.md) thy [ma'aseh](../../strongs/h/h4639.md) to another, and shall [nāḡaḏ](../../strongs/h/h5046.md) thy [gᵊḇûrâ](../../strongs/h/h1369.md).

<a name="psalms_145_5"></a>Psalms 145:5

I will [śîaḥ](../../strongs/h/h7878.md) of the [kabowd](../../strongs/h/h3519.md) [hadar](../../strongs/h/h1926.md) of thy [howd](../../strongs/h/h1935.md), and of thy [pala'](../../strongs/h/h6381.md) [dabar](../../strongs/h/h1697.md).

<a name="psalms_145_6"></a>Psalms 145:6

And men shall ['āmar](../../strongs/h/h559.md) of the [ʿĕzûz](../../strongs/h/h5807.md) of thy [yare'](../../strongs/h/h3372.md): and I will [sāp̄ar](../../strongs/h/h5608.md) thy [gᵊḏûlâ](../../strongs/h/h1420.md).

<a name="psalms_145_7"></a>Psalms 145:7

They shall [nāḇaʿ](../../strongs/h/h5042.md) the [zeker](../../strongs/h/h2143.md) of thy [rab](../../strongs/h/h7227.md) [ṭûḇ](../../strongs/h/h2898.md), and shall [ranan](../../strongs/h/h7442.md) of thy [tsedaqah](../../strongs/h/h6666.md).

<a name="psalms_145_8"></a>Psalms 145:8

[Yĕhovah](../../strongs/h/h3068.md) is [ḥanwn](../../strongs/h/h2587.md), and full of [raḥwm](../../strongs/h/h7349.md); ['ārēḵ](../../strongs/h/h750.md) to ['aph](../../strongs/h/h639.md), and of [gadowl](../../strongs/h/h1419.md) [checed](../../strongs/h/h2617.md).

<a name="psalms_145_9"></a>Psalms 145:9

[Yĕhovah](../../strongs/h/h3068.md) is [towb](../../strongs/h/h2896.md) to all: and his [raḥam](../../strongs/h/h7356.md) are over all his [ma'aseh](../../strongs/h/h4639.md).

<a name="psalms_145_10"></a>Psalms 145:10

All thy [ma'aseh](../../strongs/h/h4639.md) shall [yadah](../../strongs/h/h3034.md) thee, [Yĕhovah](../../strongs/h/h3068.md); and thy [chaciyd](../../strongs/h/h2623.md) shall [barak](../../strongs/h/h1288.md) thee.

<a name="psalms_145_11"></a>Psalms 145:11

They shall ['āmar](../../strongs/h/h559.md) of the [kabowd](../../strongs/h/h3519.md) of thy [malkuwth](../../strongs/h/h4438.md), and [dabar](../../strongs/h/h1696.md) of thy [gᵊḇûrâ](../../strongs/h/h1369.md);

<a name="psalms_145_12"></a>Psalms 145:12

To [yada'](../../strongs/h/h3045.md) to the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md) his [gᵊḇûrâ](../../strongs/h/h1369.md), and the [kabowd](../../strongs/h/h3519.md) [hadar](../../strongs/h/h1926.md) of his [malkuwth](../../strongs/h/h4438.md).

<a name="psalms_145_13"></a>Psalms 145:13

Thy [malkuwth](../../strongs/h/h4438.md) is an ['owlam](../../strongs/h/h5769.md) [malkuwth](../../strongs/h/h4438.md), and thy [memshalah](../../strongs/h/h4475.md) endureth throughout [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md).

<a name="psalms_145_14"></a>Psalms 145:14

[Yĕhovah](../../strongs/h/h3068.md) [camak](../../strongs/h/h5564.md) all that [naphal](../../strongs/h/h5307.md), and [zāqap̄](../../strongs/h/h2210.md) all those that be [kāp̄ap̄](../../strongs/h/h3721.md).

<a name="psalms_145_15"></a>Psalms 145:15

The ['ayin](../../strongs/h/h5869.md) of all [śāḇar](../../strongs/h/h7663.md) upon thee; and thou [nathan](../../strongs/h/h5414.md) them their ['ōḵel](../../strongs/h/h400.md) in [ʿēṯ](../../strongs/h/h6256.md).

<a name="psalms_145_16"></a>Psalms 145:16

Thou [pāṯaḥ](../../strongs/h/h6605.md) thine [yad](../../strongs/h/h3027.md), and [sāׂbaʿ](../../strongs/h/h7646.md) the [ratsown](../../strongs/h/h7522.md) of every [chay](../../strongs/h/h2416.md).

<a name="psalms_145_17"></a>Psalms 145:17

[Yĕhovah](../../strongs/h/h3068.md) is [tsaddiyq](../../strongs/h/h6662.md) in all his [derek](../../strongs/h/h1870.md), and [chaciyd](../../strongs/h/h2623.md) in all his [ma'aseh](../../strongs/h/h4639.md).

<a name="psalms_145_18"></a>Psalms 145:18

[Yĕhovah](../../strongs/h/h3068.md) is [qarowb](../../strongs/h/h7138.md) unto all them that [qara'](../../strongs/h/h7121.md) upon him, to all that [qara'](../../strongs/h/h7121.md) upon him in ['emeth](../../strongs/h/h571.md).

<a name="psalms_145_19"></a>Psalms 145:19

He will ['asah](../../strongs/h/h6213.md) the [ratsown](../../strongs/h/h7522.md) of them that [yārē'](../../strongs/h/h3373.md) him: he also will [shama'](../../strongs/h/h8085.md) their [shav'ah](../../strongs/h/h7775.md), and will [yasha'](../../strongs/h/h3467.md) them.

<a name="psalms_145_20"></a>Psalms 145:20

[Yĕhovah](../../strongs/h/h3068.md) [shamar](../../strongs/h/h8104.md) all them that ['ahab](../../strongs/h/h157.md) him: but all the [rasha'](../../strongs/h/h7563.md) will he [šāmaḏ](../../strongs/h/h8045.md).

<a name="psalms_145_21"></a>Psalms 145:21

My [peh](../../strongs/h/h6310.md) shall [dabar](../../strongs/h/h1696.md) the [tehillah](../../strongs/h/h8416.md) of [Yĕhovah](../../strongs/h/h3068.md): and let all [basar](../../strongs/h/h1320.md) [barak](../../strongs/h/h1288.md) his [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md) ['owlam](../../strongs/h/h5769.md) and [ʿaḏ](../../strongs/h/h5703.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 144](psalms_144.md) - [Psalms 146](psalms_146.md)