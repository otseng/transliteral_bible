# [Psalms 1](https://www.blueletterbible.org/kjv/psa/1/1/s_479001)

<a name="psalms_1_1"></a>Psalms 1:1

['esher](../../strongs/h/h835.md) the ['iysh](../../strongs/h/h376.md) that [halak](../../strongs/h/h1980.md) not in the ['etsah](../../strongs/h/h6098.md) of the [rasha'](../../strongs/h/h7563.md), nor ['amad](../../strongs/h/h5975.md) in the [derek](../../strongs/h/h1870.md) of [chatta'](../../strongs/h/h2400.md), nor [yashab](../../strongs/h/h3427.md) in the [môšāḇ](../../strongs/h/h4186.md) of the [luwts](../../strongs/h/h3887.md).

<a name="psalms_1_2"></a>Psalms 1:2

But his [chephets](../../strongs/h/h2656.md) in the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md); and in his [towrah](../../strongs/h/h8451.md) doth he [hagah](../../strongs/h/h1897.md) [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md).

<a name="psalms_1_3"></a>Psalms 1:3

And he shall be like an ['ets](../../strongs/h/h6086.md) [šāṯal](../../strongs/h/h8362.md) by the [peleḡ](../../strongs/h/h6388.md) of [mayim](../../strongs/h/h4325.md), that [nathan](../../strongs/h/h5414.md) his [pĕriy](../../strongs/h/h6529.md) in his [ʿēṯ](../../strongs/h/h6256.md); his ['aleh](../../strongs/h/h5929.md) also shall not [nabel](../../strongs/h/h5034.md); and whatsoever he ['asah](../../strongs/h/h6213.md) shall [tsalach](../../strongs/h/h6743.md).

<a name="psalms_1_4"></a>Psalms 1:4

The [rasha'](../../strongs/h/h7563.md) not so: but like the [mots](../../strongs/h/h4671.md) which the [ruwach](../../strongs/h/h7307.md) [nāḏap̄](../../strongs/h/h5086.md).

<a name="psalms_1_5"></a>Psalms 1:5

Therefore the [rasha'](../../strongs/h/h7563.md) shall not [quwm](../../strongs/h/h6965.md) in the [mishpat](../../strongs/h/h4941.md), nor [chatta'](../../strongs/h/h2400.md) in the ['edah](../../strongs/h/h5712.md) of the [tsaddiyq](../../strongs/h/h6662.md).

<a name="psalms_1_6"></a>Psalms 1:6

For [Yĕhovah](../../strongs/h/h3068.md) [yada'](../../strongs/h/h3045.md) the [derek](../../strongs/h/h1870.md) of the [tsaddiyq](../../strongs/h/h6662.md): but the [derek](../../strongs/h/h1870.md) of the [rasha'](../../strongs/h/h7563.md) shall ['abad](../../strongs/h/h6.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 2](psalms_2.md)