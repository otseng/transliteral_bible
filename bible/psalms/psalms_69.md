# [Psalms 69](https://www.blueletterbible.org/kjv/psa/69)

<a name="psalms_69_1"></a>Psalms 69:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon [šûšan](../../strongs/h/h7799.md), A Psalm of [Dāviḏ](../../strongs/h/h1732.md). [yasha'](../../strongs/h/h3467.md) me, ['Elohiym](../../strongs/h/h430.md); for the [mayim](../../strongs/h/h4325.md) are [bow'](../../strongs/h/h935.md) in unto my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_69_2"></a>Psalms 69:2

I [ṭāḇaʿ](../../strongs/h/h2883.md) in [mᵊṣôlâ](../../strongs/h/h4688.md) [yāvēn](../../strongs/h/h3121.md), where there is no [māʿŏmāḏ](../../strongs/h/h4613.md): I am [bow'](../../strongs/h/h935.md) into [maʿămaqqîm](../../strongs/h/h4615.md) [mayim](../../strongs/h/h4325.md), where the [šibōleṯ](../../strongs/h/h7641.md) [šāṭap̄](../../strongs/h/h7857.md) me.

<a name="psalms_69_3"></a>Psalms 69:3

I am [yaga'](../../strongs/h/h3021.md) of my [qara'](../../strongs/h/h7121.md): my [garown](../../strongs/h/h1627.md) is [ḥārar](../../strongs/h/h2787.md): mine ['ayin](../../strongs/h/h5869.md) [kalah](../../strongs/h/h3615.md) while I [yāḥal](../../strongs/h/h3176.md) for my ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_69_4"></a>Psalms 69:4

They that [sane'](../../strongs/h/h8130.md) me without a [ḥinnām](../../strongs/h/h2600.md) are [rabab](../../strongs/h/h7231.md) than the [śaʿărâ](../../strongs/h/h8185.md) of mine [ro'sh](../../strongs/h/h7218.md): they that would [ṣāmaṯ](../../strongs/h/h6789.md) me, being mine ['oyeb](../../strongs/h/h341.md) [sheqer](../../strongs/h/h8267.md), are [ʿāṣam](../../strongs/h/h6105.md): then I [shuwb](../../strongs/h/h7725.md) that which I [gāzal](../../strongs/h/h1497.md) not.

<a name="psalms_69_5"></a>Psalms 69:5

['Elohiym](../../strongs/h/h430.md), thou [yada'](../../strongs/h/h3045.md) my ['iûeleṯ](../../strongs/h/h200.md); and my ['ašmâ](../../strongs/h/h819.md) are not [kāḥaḏ](../../strongs/h/h3582.md) from thee

<a name="psalms_69_6"></a>Psalms 69:6

Let not them that [qāvâ](../../strongs/h/h6960.md) on thee, O ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md), be [buwsh](../../strongs/h/h954.md) for my sake: let not those that [bāqaš](../../strongs/h/h1245.md) thee be [kālam](../../strongs/h/h3637.md) for my sake, ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="psalms_69_7"></a>Psalms 69:7

Because for thy sake I have [nasa'](../../strongs/h/h5375.md) [cherpah](../../strongs/h/h2781.md); [kĕlimmah](../../strongs/h/h3639.md) hath [kāsâ](../../strongs/h/h3680.md) my [paniym](../../strongs/h/h6440.md).

<a name="psalms_69_8"></a>Psalms 69:8

I am become a [zûr](../../strongs/h/h2114.md) unto my ['ach](../../strongs/h/h251.md), and a [nāḵrî](../../strongs/h/h5237.md) unto my ['em](../../strongs/h/h517.md) [ben](../../strongs/h/h1121.md).

<a name="psalms_69_9"></a>Psalms 69:9

For the [qin'â](../../strongs/h/h7068.md) of thine [bayith](../../strongs/h/h1004.md) hath ['akal](../../strongs/h/h398.md) me; and the [cherpah](../../strongs/h/h2781.md) of them that [ḥārap̄](../../strongs/h/h2778.md) thee are [naphal](../../strongs/h/h5307.md) upon me.

<a name="psalms_69_10"></a>Psalms 69:10

When I [bāḵâ](../../strongs/h/h1058.md), and chastened my [nephesh](../../strongs/h/h5315.md) with [ṣôm](../../strongs/h/h6685.md), that was to my [cherpah](../../strongs/h/h2781.md).

<a name="psalms_69_11"></a>Psalms 69:11

I [nathan](../../strongs/h/h5414.md) [śaq](../../strongs/h/h8242.md) also my [lᵊḇûš](../../strongs/h/h3830.md); and I became a [māšāl](../../strongs/h/h4912.md) to them.

<a name="psalms_69_12"></a>Psalms 69:12

They that [yashab](../../strongs/h/h3427.md) the [sha'ar](../../strongs/h/h8179.md) [śîaḥ](../../strongs/h/h7878.md) against me; and I was the [Nᵊḡînâ](../../strongs/h/h5058.md) of the [šāṯâ](../../strongs/h/h8354.md) [šēḵār](../../strongs/h/h7941.md)

<a name="psalms_69_13"></a>Psalms 69:13

But as for me, my [tĕphillah](../../strongs/h/h8605.md) is unto thee, [Yĕhovah](../../strongs/h/h3068.md), in a [ratsown](../../strongs/h/h7522.md) [ʿēṯ](../../strongs/h/h6256.md): ['Elohiym](../../strongs/h/h430.md), in the [rōḇ](../../strongs/h/h7230.md) of thy [checed](../../strongs/h/h2617.md) ['anah](../../strongs/h/h6030.md) me, in the ['emeth](../../strongs/h/h571.md) of thy [yesha'](../../strongs/h/h3468.md).

<a name="psalms_69_14"></a>Psalms 69:14

[natsal](../../strongs/h/h5337.md) me out of the [ṭîṭ](../../strongs/h/h2916.md), and let me not [ṭāḇaʿ](../../strongs/h/h2883.md): let me be [natsal](../../strongs/h/h5337.md) from them that [sane'](../../strongs/h/h8130.md) me, and out of the [maʿămaqqîm](../../strongs/h/h4615.md) [mayim](../../strongs/h/h4325.md).

<a name="psalms_69_15"></a>Psalms 69:15

Let not the [mayim](../../strongs/h/h4325.md) [šibōleṯ](../../strongs/h/h7641.md) [šāṭap̄](../../strongs/h/h7857.md) me, neither let the [mᵊṣôlâ](../../strongs/h/h4688.md) [bālaʿ](../../strongs/h/h1104.md) me, and let not the [bᵊ'ēr](../../strongs/h/h875.md) ['āṭar](../../strongs/h/h332.md) her [peh](../../strongs/h/h6310.md) upon me.

<a name="psalms_69_16"></a>Psalms 69:16

['anah](../../strongs/h/h6030.md) me, [Yĕhovah](../../strongs/h/h3068.md); for thy [checed](../../strongs/h/h2617.md) is [towb](../../strongs/h/h2896.md): [panah](../../strongs/h/h6437.md) unto me according to the [rōḇ](../../strongs/h/h7230.md) of thy [raḥam](../../strongs/h/h7356.md).

<a name="psalms_69_17"></a>Psalms 69:17

And [cathar](../../strongs/h/h5641.md) not thy [paniym](../../strongs/h/h6440.md) from thy ['ebed](../../strongs/h/h5650.md); for I am in [tsarar](../../strongs/h/h6887.md): ['anah](../../strongs/h/h6030.md) me [mahēr](../../strongs/h/h4118.md).

<a name="psalms_69_18"></a>Psalms 69:18

[qāraḇ](../../strongs/h/h7126.md) unto my [nephesh](../../strongs/h/h5315.md), and [gā'al](../../strongs/h/h1350.md) it: [pāḏâ](../../strongs/h/h6299.md) me because of mine ['oyeb](../../strongs/h/h341.md).


<a name="psalms_69_19"></a>Psalms 69:19

Thou hast [yada'](../../strongs/h/h3045.md) my [cherpah](../../strongs/h/h2781.md), and my [bšeṯ](../../strongs/h/h1322.md), and my [kĕlimmah](../../strongs/h/h3639.md): mine [tsarar](../../strongs/h/h6887.md) are all before thee.

<a name="psalms_69_20"></a>Psalms 69:20

[cherpah](../../strongs/h/h2781.md) hath [shabar](../../strongs/h/h7665.md) my [leb](../../strongs/h/h3820.md); and I am full of [nûš](../../strongs/h/h5136.md): and I [qāvâ](../../strongs/h/h6960.md) for some to [nuwd](../../strongs/h/h5110.md), but there was none; and for [nacham](../../strongs/h/h5162.md), but I [māṣā'](../../strongs/h/h4672.md) none.

<a name="psalms_69_21"></a>Psalms 69:21

They [nathan](../../strongs/h/h5414.md) me also [rō'š](../../strongs/h/h7219.md) for my [bārûṯ](../../strongs/h/h1267.md); and in my [ṣāmā'](../../strongs/h/h6772.md) they gave me [ḥōmeṣ](../../strongs/h/h2558.md) to [šāqâ](../../strongs/h/h8248.md).

<a name="psalms_69_22"></a>Psalms 69:22

Let their [šulḥān](../../strongs/h/h7979.md) become a [paḥ](../../strongs/h/h6341.md) [paniym](../../strongs/h/h6440.md) them: and that which should have been for their [shalowm](../../strongs/h/h7965.md), let it become a [mowqesh](../../strongs/h/h4170.md).

<a name="psalms_69_23"></a>Psalms 69:23

Let their ['ayin](../../strongs/h/h5869.md) be [ḥāšaḵ](../../strongs/h/h2821.md), that they [ra'ah](../../strongs/h/h7200.md) not; and make their [māṯnayim](../../strongs/h/h4975.md) [tāmîḏ](../../strongs/h/h8548.md) to [māʿaḏ](../../strongs/h/h4571.md).

<a name="psalms_69_24"></a>Psalms 69:24

Pour [šāp̄aḵ](../../strongs/h/h8210.md) thine [zaʿam](../../strongs/h/h2195.md) upon them, and let thy [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) [nāśaḡ](../../strongs/h/h5381.md) them.

<a name="psalms_69_25"></a>Psalms 69:25

Let their [ṭîrâ](../../strongs/h/h2918.md) be [šāmēm](../../strongs/h/h8074.md); and let none [yashab](../../strongs/h/h3427.md) in their ['ohel](../../strongs/h/h168.md).

<a name="psalms_69_26"></a>Psalms 69:26

For they [radaph](../../strongs/h/h7291.md) him whom thou hast [nakah](../../strongs/h/h5221.md); and they [sāp̄ar](../../strongs/h/h5608.md) to the [maḵ'ōḇ](../../strongs/h/h4341.md) of those whom thou hast [ḥālāl](../../strongs/h/h2491.md).

<a name="psalms_69_27"></a>Psalms 69:27

[nathan](../../strongs/h/h5414.md) ['avon](../../strongs/h/h5771.md) unto their ['avon](../../strongs/h/h5771.md): and let them not [bow'](../../strongs/h/h935.md) into thy [tsedaqah](../../strongs/h/h6666.md).

<a name="psalms_69_28"></a>Psalms 69:28

Let them be [māḥâ](../../strongs/h/h4229.md) out of the [sēp̄er](../../strongs/h/h5612.md) of the [chay](../../strongs/h/h2416.md), and not be [kāṯaḇ](../../strongs/h/h3789.md) with the [tsaddiyq](../../strongs/h/h6662.md).

<a name="psalms_69_29"></a>Psalms 69:29

But I am ['aniy](../../strongs/h/h6041.md) and [kā'aḇ](../../strongs/h/h3510.md): let thy [yĕshuw'ah](../../strongs/h/h3444.md), ['Elohiym](../../strongs/h/h430.md), set me up on [śāḡaḇ](../../strongs/h/h7682.md).

<a name="psalms_69_30"></a>Psalms 69:30

I will [halal](../../strongs/h/h1984.md) the [shem](../../strongs/h/h8034.md) of ['Elohiym](../../strongs/h/h430.md) with a [šîr](../../strongs/h/h7892.md), and will [gāḏal](../../strongs/h/h1431.md) him with [tôḏâ](../../strongs/h/h8426.md).

<a name="psalms_69_31"></a>Psalms 69:31

This also shall [yatab](../../strongs/h/h3190.md) the [Yĕhovah](../../strongs/h/h3068.md) [yatab](../../strongs/h/h3190.md) than a [showr](../../strongs/h/h7794.md) or [par](../../strongs/h/h6499.md) that hath [qāran](../../strongs/h/h7160.md) and [Pāras](../../strongs/h/h6536.md).

<a name="psalms_69_32"></a>Psalms 69:32

The ['anav](../../strongs/h/h6035.md) shall [ra'ah](../../strongs/h/h7200.md) this, and be [samach](../../strongs/h/h8055.md): and your [lebab](../../strongs/h/h3824.md) shall [ḥāyâ](../../strongs/h/h2421.md) that [darash](../../strongs/h/h1875.md) ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_69_33"></a>Psalms 69:33

For [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) the ['ebyown](../../strongs/h/h34.md), and [bazah](../../strongs/h/h959.md) not his ['āsîr](../../strongs/h/h615.md).

<a name="psalms_69_34"></a>Psalms 69:34

Let the [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md) [halal](../../strongs/h/h1984.md) him, the [yam](../../strongs/h/h3220.md), and every thing that [rāmaś](../../strongs/h/h7430.md) therein.

<a name="psalms_69_35"></a>Psalms 69:35

For ['Elohiym](../../strongs/h/h430.md) will [yasha'](../../strongs/h/h3467.md) [Tsiyown](../../strongs/h/h6726.md), and will [bānâ](../../strongs/h/h1129.md) the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md): that they may [yashab](../../strongs/h/h3427.md) there, and have it in [yarash](../../strongs/h/h3423.md).

<a name="psalms_69_36"></a>Psalms 69:36

The [zera'](../../strongs/h/h2233.md) also of his ['ebed](../../strongs/h/h5650.md) shall [nāḥal](../../strongs/h/h5157.md) it: and they that ['ahab](../../strongs/h/h157.md) his [shem](../../strongs/h/h8034.md) shall [shakan](../../strongs/h/h7931.md) therein.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 68](psalms_68.md) - [Psalms 70](psalms_70.md)