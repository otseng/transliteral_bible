# [Psalms 32](https://www.blueletterbible.org/kjv/psa/32/1/s_510001)

<a name="psalms_32_1"></a>Psalms 32:1

[A Psalm] of [Dāviḏ](../../strongs/h/h1732.md), [Maśkîl](../../strongs/h/h4905.md). ['esher](../../strongs/h/h835.md) is he whose [pesha'](../../strongs/h/h6588.md) is [nasa'](../../strongs/h/h5375.md), whose [ḥăṭā'â](../../strongs/h/h2401.md) is [kāsâ](../../strongs/h/h3680.md).

<a name="psalms_32_2"></a>Psalms 32:2

['esher](../../strongs/h/h835.md) is the ['adam](../../strongs/h/h120.md) unto whom [Yĕhovah](../../strongs/h/h3068.md) [chashab](../../strongs/h/h2803.md) not ['avon](../../strongs/h/h5771.md), and in whose [ruwach](../../strongs/h/h7307.md) there is no [rᵊmîyâ](../../strongs/h/h7423.md).

<a name="psalms_32_3"></a>Psalms 32:3

When I kept [ḥāraš](../../strongs/h/h2790.md), my ['etsem](../../strongs/h/h6106.md) [bālâ](../../strongs/h/h1086.md) through my [šᵊ'āḡâ](../../strongs/h/h7581.md) all the [yowm](../../strongs/h/h3117.md) long.

<a name="psalms_32_4"></a>Psalms 32:4

For [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md) thy [yad](../../strongs/h/h3027.md) was [kabad](../../strongs/h/h3513.md) upon me: my [lᵊšaḏ](../../strongs/h/h3955.md) is [hāp̄aḵ](../../strongs/h/h2015.md) into the [ḥărāḇôn](../../strongs/h/h2725.md) of [qayiṣ](../../strongs/h/h7019.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_32_5"></a>Psalms 32:5

I [yada'](../../strongs/h/h3045.md) my [chatta'ath](../../strongs/h/h2403.md) unto thee, and mine ['avon](../../strongs/h/h5771.md) have I not [kāsâ](../../strongs/h/h3680.md). I ['āmar](../../strongs/h/h559.md), I will [yadah](../../strongs/h/h3034.md) my [pesha'](../../strongs/h/h6588.md) unto [Yĕhovah](../../strongs/h/h3068.md); and thou [nasa'](../../strongs/h/h5375.md) the ['avon](../../strongs/h/h5771.md) of my [chatta'ath](../../strongs/h/h2403.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_32_6"></a>Psalms 32:6

For this shall every one that is [chaciyd](../../strongs/h/h2623.md) [palal](../../strongs/h/h6419.md) unto thee in a [ʿēṯ](../../strongs/h/h6256.md) when thou mayest be [māṣā'](../../strongs/h/h4672.md): surely in the [šeṭep̄](../../strongs/h/h7858.md) of [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md) they shall not [naga'](../../strongs/h/h5060.md) unto him.

<a name="psalms_32_7"></a>Psalms 32:7

Thou art my [cether](../../strongs/h/h5643.md); thou shalt [nāṣar](../../strongs/h/h5341.md) me from [tsar](../../strongs/h/h6862.md); thou shalt [cabab](../../strongs/h/h5437.md) me about with [rōn](../../strongs/h/h7438.md) of [pallēṭ](../../strongs/h/h6405.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_32_8"></a>Psalms 32:8

I will [sakal](../../strongs/h/h7919.md) thee and [yārâ](../../strongs/h/h3384.md) thee in the [derek](../../strongs/h/h1870.md) which thou shalt [yālaḵ](../../strongs/h/h3212.md): I will [ya'ats](../../strongs/h/h3289.md) thee with mine ['ayin](../../strongs/h/h5869.md).

<a name="psalms_32_9"></a>Psalms 32:9

Be ye not as the [sûs](../../strongs/h/h5483.md), or as the [pereḏ](../../strongs/h/h6505.md), which have no [bîn](../../strongs/h/h995.md): whose [ʿădiy](../../strongs/h/h5716.md) must be [bālam](../../strongs/h/h1102.md) in with [meṯeḡ](../../strongs/h/h4964.md) and [resen](../../strongs/h/h7448.md), lest they [qāraḇ](../../strongs/h/h7126.md) unto thee.

<a name="psalms_32_10"></a>Psalms 32:10

[rab](../../strongs/h/h7227.md) [maḵ'ōḇ](../../strongs/h/h4341.md) shall be to the [rasha'](../../strongs/h/h7563.md): but he that [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md), [checed](../../strongs/h/h2617.md) shall [cabab](../../strongs/h/h5437.md) him about.

<a name="psalms_32_11"></a>Psalms 32:11

Be [samach](../../strongs/h/h8055.md) in [Yĕhovah](../../strongs/h/h3068.md), and [giyl](../../strongs/h/h1523.md), ye [tsaddiyq](../../strongs/h/h6662.md): and [ranan](../../strongs/h/h7442.md), all ye that are [yashar](../../strongs/h/h3477.md) in [leb](../../strongs/h/h3820.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 31](psalms_31.md) - [Psalms 33](psalms_33.md)