# [Psalms 142](https://www.blueletterbible.org/kjv/psalms/142)

<a name="psalms_142_1"></a>Psalms 142:1

[Maśkîl](../../strongs/h/h4905.md) of [Dāviḏ](../../strongs/h/h1732.md); A [tĕphillah](../../strongs/h/h8605.md) when he was in the [mᵊʿārâ](../../strongs/h/h4631.md). I [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md) with my [qowl](../../strongs/h/h6963.md); with my [qowl](../../strongs/h/h6963.md) unto [Yĕhovah](../../strongs/h/h3068.md) did I make my [chanan](../../strongs/h/h2603.md).

<a name="psalms_142_2"></a>Psalms 142:2

I [šāp̄aḵ](../../strongs/h/h8210.md) my [śîaḥ](../../strongs/h/h7879.md) [paniym](../../strongs/h/h6440.md) him; I [nāḡaḏ](../../strongs/h/h5046.md) [paniym](../../strongs/h/h6440.md) him my [tsarah](../../strongs/h/h6869.md).

<a name="psalms_142_3"></a>Psalms 142:3

When my [ruwach](../../strongs/h/h7307.md) was [ʿāṭap̄](../../strongs/h/h5848.md) within me, then thou [yada'](../../strongs/h/h3045.md) my [nāṯîḇ](../../strongs/h/h5410.md). In the ['orach](../../strongs/h/h734.md) [zû](../../strongs/h/h2098.md) I [halak](../../strongs/h/h1980.md) have they [taman](../../strongs/h/h2934.md) a [paḥ](../../strongs/h/h6341.md) for me.

<a name="psalms_142_4"></a>Psalms 142:4

I [nabat](../../strongs/h/h5027.md) on my [yamiyn](../../strongs/h/h3225.md), and [ra'ah](../../strongs/h/h7200.md), but there was no man that would [nāḵar](../../strongs/h/h5234.md) me: [mānôs](../../strongs/h/h4498.md) ['abad](../../strongs/h/h6.md) me; no man [darash](../../strongs/h/h1875.md) for my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_142_5"></a>Psalms 142:5

I [zāʿaq](../../strongs/h/h2199.md) unto thee, [Yĕhovah](../../strongs/h/h3068.md): I ['āmar](../../strongs/h/h559.md), Thou art my [machaceh](../../strongs/h/h4268.md) and my [cheleq](../../strongs/h/h2506.md) in the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md).

<a name="psalms_142_6"></a>Psalms 142:6

[qashab](../../strongs/h/h7181.md) unto my [rinnah](../../strongs/h/h7440.md); for I am [dālal](../../strongs/h/h1809.md) [me'od](../../strongs/h/h3966.md) [dālal](../../strongs/h/h1809.md): [natsal](../../strongs/h/h5337.md) me from my [radaph](../../strongs/h/h7291.md); for they are ['amats](../../strongs/h/h553.md) than I.

<a name="psalms_142_7"></a>Psalms 142:7

[yāṣā'](../../strongs/h/h3318.md) my [nephesh](../../strongs/h/h5315.md) out of [masgēr](../../strongs/h/h4525.md), that I may [yadah](../../strongs/h/h3034.md) thy [shem](../../strongs/h/h8034.md): the [tsaddiyq](../../strongs/h/h6662.md) shall [kāṯar](../../strongs/h/h3803.md) me; for thou shalt deal [gamal](../../strongs/h/h1580.md) with me.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 141](psalms_141.md) - [Psalms 143](psalms_143.md)