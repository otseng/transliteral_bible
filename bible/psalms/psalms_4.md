# [Psalms 4](https://www.blueletterbible.org/kjv/psa/4/1/s_482001)

<a name="psalms_4_1"></a>Psalms 4:1

To the [nāṣaḥ](../../strongs/h/h5329.md) on [Nᵊḡînâ](../../strongs/h/h5058.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). ['anah](../../strongs/h/h6030.md) me when I [qara'](../../strongs/h/h7121.md), ['Elohiym](../../strongs/h/h430.md) of my [tsedeq](../../strongs/h/h6664.md): thou hast [rāḥaḇ](../../strongs/h/h7337.md) me when I was in [tsar](../../strongs/h/h6862.md); have [chanan](../../strongs/h/h2603.md) upon me, and [shama'](../../strongs/h/h8085.md) my [tĕphillah](../../strongs/h/h8605.md).

<a name="psalms_4_2"></a>Psalms 4:2

O ye [ben](../../strongs/h/h1121.md) of ['iysh](../../strongs/h/h376.md), how long will ye turn my [kabowd](../../strongs/h/h3519.md) into [kĕlimmah](../../strongs/h/h3639.md)? how long will ye ['ahab](../../strongs/h/h157.md) [riyq](../../strongs/h/h7385.md), and [bāqaš](../../strongs/h/h1245.md) after [kazab](../../strongs/h/h3577.md)? [Celah](../../strongs/h/h5542.md).

<a name="psalms_4_3"></a>Psalms 4:3

But [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) hath [palah](../../strongs/h/h6395.md) him that is [chaciyd](../../strongs/h/h2623.md) for himself: [Yĕhovah](../../strongs/h/h3068.md) will [shama'](../../strongs/h/h8085.md) when I [qara'](../../strongs/h/h7121.md) unto him.

<a name="psalms_4_4"></a>Psalms 4:4

[ragaz](../../strongs/h/h7264.md), and [chata'](../../strongs/h/h2398.md) not: ['āmar](../../strongs/h/h559.md) with your own [lebab](../../strongs/h/h3824.md) upon your [miškāḇ](../../strongs/h/h4904.md), and [damam](../../strongs/h/h1826.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_4_5"></a>Psalms 4:5

[zabach](../../strongs/h/h2076.md) the [zebach](../../strongs/h/h2077.md) of [tsedeq](../../strongs/h/h6664.md), and put your [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_4_6"></a>Psalms 4:6

There be many that ['āmar](../../strongs/h/h559.md), Who will [ra'ah](../../strongs/h/h7200.md) us any [towb](../../strongs/h/h2896.md)? [Yĕhovah](../../strongs/h/h3068.md), [nasa'](../../strongs/h/h5375.md) thou the ['owr](../../strongs/h/h216.md) of thy [paniym](../../strongs/h/h6440.md) upon us.

<a name="psalms_4_7"></a>Psalms 4:7

Thou hast [nathan](../../strongs/h/h5414.md) [simchah](../../strongs/h/h8057.md) in my [leb](../../strongs/h/h3820.md), more than in the time their [dagan](../../strongs/h/h1715.md) and their [tiyrowsh](../../strongs/h/h8492.md) [rabab](../../strongs/h/h7231.md).

<a name="psalms_4_8"></a>Psalms 4:8

I will both [shakab](../../strongs/h/h7901.md) me in [shalowm](../../strongs/h/h7965.md), and [yashen](../../strongs/h/h3462.md): for thou, [Yĕhovah](../../strongs/h/h3068.md), only makest me [yashab](../../strongs/h/h3427.md) in [betach](../../strongs/h/h983.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 3](psalms_3.md) - [Psalms 5](psalms_5.md)