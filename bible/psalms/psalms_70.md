# [Psalms 70](https://www.blueletterbible.org/kjv/psa/70)

<a name="psalms_70_1"></a>Psalms 70:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A Psalm of [Dāviḏ](../../strongs/h/h1732.md), to bring to [zakar](../../strongs/h/h2142.md). Make haste, ['Elohiym](../../strongs/h/h430.md), to [natsal](../../strongs/h/h5337.md) me; make [ḥûš](../../strongs/h/h2363.md) to [ʿezrâ](../../strongs/h/h5833.md) me, [Yĕhovah](../../strongs/h/h3068.md).


<a name="psalms_70_2"></a>Psalms 70:2

Let them be [buwsh](../../strongs/h/h954.md) and [ḥāp̄ēr](../../strongs/h/h2659.md) that [bāqaš](../../strongs/h/h1245.md) after my [nephesh](../../strongs/h/h5315.md): let them be [sûḡ](../../strongs/h/h5472.md) ['āḥôr](../../strongs/h/h268.md), and put to [kālam](../../strongs/h/h3637.md), that [chaphets](../../strongs/h/h2655.md) my [ra'](../../strongs/h/h7451.md).


<a name="psalms_70_3"></a>Psalms 70:3

Let them be [shuwb](../../strongs/h/h7725.md) for a [ʿēqeḇ](../../strongs/h/h6118.md) of their [bšeṯ](../../strongs/h/h1322.md) that ['āmar](../../strongs/h/h559.md), [he'āḥ](../../strongs/h/h1889.md), [he'āḥ](../../strongs/h/h1889.md).


<a name="psalms_70_4"></a>Psalms 70:4

Let all those that [bāqaš](../../strongs/h/h1245.md) thee [śûś](../../strongs/h/h7797.md) and be [samach](../../strongs/h/h8055.md) in thee: and let such as ['ahab](../../strongs/h/h157.md) thy [yĕshuw'ah](../../strongs/h/h3444.md) ['āmar](../../strongs/h/h559.md) [tāmîḏ](../../strongs/h/h8548.md), Let ['Elohiym](../../strongs/h/h430.md) be [gāḏal](../../strongs/h/h1431.md).


<a name="psalms_70_5"></a>Psalms 70:5

But I am ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md): make [ḥûš](../../strongs/h/h2363.md) unto me, ['Elohiym](../../strongs/h/h430.md): thou art my ['ezer](../../strongs/h/h5828.md) and my [palat](../../strongs/h/h6403.md); O [Yĕhovah](../../strongs/h/h3068.md), make no ['āḥar](../../strongs/h/h309.md)

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 69](psalms_69.md) - [Psalms 71](psalms_71.md)