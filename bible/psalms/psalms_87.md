# [Psalms 87](https://www.blueletterbible.org/kjv/psalms/87)

<a name="psalms_87_1"></a>Psalms 87:1

A [mizmôr](../../strongs/h/h4210.md) or [šîr](../../strongs/h/h7892.md) for the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md). His [yᵊsûḏâ](../../strongs/h/h3248.md) is in the [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2042.md).

<a name="psalms_87_2"></a>Psalms 87:2

[Yĕhovah](../../strongs/h/h3068.md) ['ahab](../../strongs/h/h157.md) the [sha'ar](../../strongs/h/h8179.md) of [Tsiyown](../../strongs/h/h6726.md) more than all the [miškān](../../strongs/h/h4908.md) of [Ya'aqob](../../strongs/h/h3290.md).

<a name="psalms_87_3"></a>Psalms 87:3

[kabad](../../strongs/h/h3513.md) are [dabar](../../strongs/h/h1696.md) of thee, O [ʿîr](../../strongs/h/h5892.md) of ['Elohiym](../../strongs/h/h430.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_87_4"></a>Psalms 87:4

I will make [zakar](../../strongs/h/h2142.md) of [rahaḇ](../../strongs/h/h7294.md) and [Bāḇel](../../strongs/h/h894.md) to them that [yada'](../../strongs/h/h3045.md) me: behold [pᵊlešeṯ](../../strongs/h/h6429.md), and [Ṣōr](../../strongs/h/h6865.md), with [Kûš](../../strongs/h/h3568.md); this man was [yalad](../../strongs/h/h3205.md) there.

<a name="psalms_87_5"></a>Psalms 87:5

And of [Tsiyown](../../strongs/h/h6726.md) it shall be ['āmar](../../strongs/h/h559.md), This and that ['iysh](../../strongs/h/h376.md) was [yalad](../../strongs/h/h3205.md) in her: and the ['elyown](../../strongs/h/h5945.md) himself shall [kuwn](../../strongs/h/h3559.md) her.

<a name="psalms_87_6"></a>Psalms 87:6

[Yĕhovah](../../strongs/h/h3068.md) shall [sāp̄ar](../../strongs/h/h5608.md), when he [kāṯaḇ](../../strongs/h/h3789.md) the ['am](../../strongs/h/h5971.md), that this man was [yalad](../../strongs/h/h3205.md) there. [Celah](../../strongs/h/h5542.md).

<a name="psalms_87_7"></a>Psalms 87:7

As well the [shiyr](../../strongs/h/h7891.md) as the [ḥālal](../../strongs/h/h2490.md) on instruments shall be there: all my [maʿyān](../../strongs/h/h4599.md) are in thee.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 86](psalms_86.md) - [Psalms 88](psalms_88.md)