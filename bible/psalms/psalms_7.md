# [Psalms 7](https://www.blueletterbible.org/kjv/psa/7/1/s_485001)

<a name="psalms_7_1"></a>Psalms 7:1

[Šigāyôn](../../strongs/h/h7692.md) of [Dāviḏ](../../strongs/h/h1732.md), which he [shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md), concerning the [dabar](../../strongs/h/h1697.md) of [Kûš](../../strongs/h/h3568.md) the [ben](../../strongs/h/h1121.md) [Ben-yᵊmînî](../../strongs/h/h1145.md).  [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), in thee do I put my [chacah](../../strongs/h/h2620.md): [yasha'](../../strongs/h/h3467.md) me from all them that [radaph](../../strongs/h/h7291.md) me, and [natsal](../../strongs/h/h5337.md) me:

<a name="psalms_7_2"></a>Psalms 7:2

Lest he [taraph](../../strongs/h/h2963.md) my [nephesh](../../strongs/h/h5315.md) like an ['ariy](../../strongs/h/h738.md), [paraq](../../strongs/h/h6561.md), while none to [natsal](../../strongs/h/h5337.md).

<a name="psalms_7_3"></a>Psalms 7:3

[Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), If I have done this; if there be ['evel](../../strongs/h/h5766.md) in my [kaph](../../strongs/h/h3709.md);

<a name="psalms_7_4"></a>Psalms 7:4

If I have rewarded [ra'](../../strongs/h/h7451.md) unto him that was at [shalam](../../strongs/h/h7999.md) with me; (yea, I have [chalats](../../strongs/h/h2502.md) him that without cause is mine [tsarar](../../strongs/h/h6887.md):)

<a name="psalms_7_5"></a>Psalms 7:5

Let the ['oyeb](../../strongs/h/h341.md) [radaph](../../strongs/h/h7291.md) my [nephesh](../../strongs/h/h5315.md), and [nāśaḡ](../../strongs/h/h5381.md) it; yea, let him [rāmas](../../strongs/h/h7429.md) my [chay](../../strongs/h/h2416.md) upon the ['erets](../../strongs/h/h776.md), and lay mine [kabowd](../../strongs/h/h3519.md) in the ['aphar](../../strongs/h/h6083.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_7_6"></a>Psalms 7:6

[quwm](../../strongs/h/h6965.md), [Yĕhovah](../../strongs/h/h3068.md), in thine ['aph](../../strongs/h/h639.md), [nasa'](../../strongs/h/h5375.md) thyself because of the ['ebrah](../../strongs/h/h5678.md) of mine [tsarar](../../strongs/h/h6887.md): and [ʿûr](../../strongs/h/h5782.md) for me to the [mishpat](../../strongs/h/h4941.md) that thou hast [tsavah](../../strongs/h/h6680.md).

<a name="psalms_7_7"></a>Psalms 7:7

So shall the ['edah](../../strongs/h/h5712.md) of the [lĕom](../../strongs/h/h3816.md) [cabab](../../strongs/h/h5437.md) thee about: for their sakes therefore [shuwb](../../strongs/h/h7725.md) thou on [marowm](../../strongs/h/h4791.md).

<a name="psalms_7_8"></a>Psalms 7:8

[Yĕhovah](../../strongs/h/h3068.md) shall [diyn](../../strongs/h/h1777.md) the ['am](../../strongs/h/h5971.md): [shaphat](../../strongs/h/h8199.md), [Yĕhovah](../../strongs/h/h3068.md), according to my [tsedeq](../../strongs/h/h6664.md), and according to mine [tom](../../strongs/h/h8537.md) that is in me.

<a name="psalms_7_9"></a>Psalms 7:9

Oh let the [ra'](../../strongs/h/h7451.md) of the [rasha'](../../strongs/h/h7563.md) [gāmar](../../strongs/h/h1584.md); but [kuwn](../../strongs/h/h3559.md) the [tsaddiyq](../../strongs/h/h6662.md): for the [tsaddiyq](../../strongs/h/h6662.md) ['Elohiym](../../strongs/h/h430.md) trieth the [libbah](../../strongs/h/h3826.md) and [kilyah](../../strongs/h/h3629.md).

<a name="psalms_7_10"></a>Psalms 7:10

My [magen](../../strongs/h/h4043.md) of ['Elohiym](../../strongs/h/h430.md), which [yasha'](../../strongs/h/h3467.md) the [yashar](../../strongs/h/h3477.md) in [leb](../../strongs/h/h3820.md).

<a name="psalms_7_11"></a>Psalms 7:11

['Elohiym](../../strongs/h/h430.md) [shaphat](../../strongs/h/h8199.md) the [tsaddiyq](../../strongs/h/h6662.md), and ['el](../../strongs/h/h410.md) is [za'am](../../strongs/h/h2194.md) every [yowm](../../strongs/h/h3117.md).

<a name="psalms_7_12"></a>Psalms 7:12

If he [shuwb](../../strongs/h/h7725.md), he will whet his [chereb](../../strongs/h/h2719.md); he hath bent his [qesheth](../../strongs/h/h7198.md), and [kuwn](../../strongs/h/h3559.md).

<a name="psalms_7_13"></a>Psalms 7:13

He hath also [kuwn](../../strongs/h/h3559.md) for him the [kĕliy](../../strongs/h/h3627.md) of [maveth](../../strongs/h/h4194.md); he [pa'al](../../strongs/h/h6466.md) his [chets](../../strongs/h/h2671.md) [dalaq](../../strongs/h/h1814.md).

<a name="psalms_7_14"></a>Psalms 7:14

[chabal](../../strongs/h/h2254.md) with ['aven](../../strongs/h/h205.md), and hath conceived ['amal](../../strongs/h/h5999.md), and [yalad](../../strongs/h/h3205.md) [sheqer](../../strongs/h/h8267.md).

<a name="psalms_7_15"></a>Psalms 7:15

He [karah](../../strongs/h/h3738.md) a [bowr](../../strongs/h/h953.md), and [chaphar](../../strongs/h/h2658.md) it, and is [naphal](../../strongs/h/h5307.md) into the [shachath](../../strongs/h/h7845.md) he made.

<a name="psalms_7_16"></a>Psalms 7:16

His ['amal](../../strongs/h/h5999.md) shall [shuwb](../../strongs/h/h7725.md) upon his own [ro'sh](../../strongs/h/h7218.md), and his [chamac](../../strongs/h/h2555.md) shall [yarad](../../strongs/h/h3381.md) upon his own [qodqod](../../strongs/h/h6936.md).

<a name="psalms_7_17"></a>Psalms 7:17

I will [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md) according to his [tsedeq](../../strongs/h/h6664.md): and will [zamar](../../strongs/h/h2167.md) to the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) ['elyown](../../strongs/h/h5945.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 6](psalms_6.md) - [Psalms 8](psalms_8.md)