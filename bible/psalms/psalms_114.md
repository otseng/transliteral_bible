# [Psalms 114](https://www.blueletterbible.org/kjv/psalms/114)

<a name="psalms_114_1"></a>Psalms 114:1

When [Yisra'el](../../strongs/h/h3478.md) [yāṣā'](../../strongs/h/h3318.md) of [Mitsrayim](../../strongs/h/h4714.md), the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md) from an ['am](../../strongs/h/h5971.md) of [lāʿaz](../../strongs/h/h3937.md);

<a name="psalms_114_2"></a>Psalms 114:2

[Yehuwdah](../../strongs/h/h3063.md) was his [qodesh](../../strongs/h/h6944.md), and [Yisra'el](../../strongs/h/h3478.md) his [memshalah](../../strongs/h/h4475.md).

<a name="psalms_114_3"></a>Psalms 114:3

The [yam](../../strongs/h/h3220.md) [ra'ah](../../strongs/h/h7200.md) it, and [nûs](../../strongs/h/h5127.md): [Yardēn](../../strongs/h/h3383.md) was [cabab](../../strongs/h/h5437.md) ['āḥôr](../../strongs/h/h268.md).

<a name="psalms_114_4"></a>Psalms 114:4

The [har](../../strongs/h/h2022.md) [rāqaḏ](../../strongs/h/h7540.md) like ['ayil](../../strongs/h/h352.md), and the [giḇʿâ](../../strongs/h/h1389.md) like [ben](../../strongs/h/h1121.md) [tso'n](../../strongs/h/h6629.md).

<a name="psalms_114_5"></a>Psalms 114:5

What ailed thee, O thou [yam](../../strongs/h/h3220.md), that thou [nûs](../../strongs/h/h5127.md)? thou [Yardēn](../../strongs/h/h3383.md), that thou wast [cabab](../../strongs/h/h5437.md) ['āḥôr](../../strongs/h/h268.md)?

<a name="psalms_114_6"></a>Psalms 114:6

Ye [har](../../strongs/h/h2022.md), that ye [rāqaḏ](../../strongs/h/h7540.md) like ['ayil](../../strongs/h/h352.md); and ye [giḇʿâ](../../strongs/h/h1389.md), like [ben](../../strongs/h/h1121.md) [tso'n](../../strongs/h/h6629.md)?

<a name="psalms_114_7"></a>Psalms 114:7

[chuwl](../../strongs/h/h2342.md), thou ['erets](../../strongs/h/h776.md), at the [paniym](../../strongs/h/h6440.md) of the ['adown](../../strongs/h/h113.md), at the [paniym](../../strongs/h/h6440.md) of the ['ĕlvôha](../../strongs/h/h433.md) of [Ya'aqob](../../strongs/h/h3290.md);

<a name="psalms_114_8"></a>Psalms 114:8

Which [hāp̄aḵ](../../strongs/h/h2015.md) the [tsuwr](../../strongs/h/h6697.md) into a ['ăḡam](../../strongs/h/h98.md) [mayim](../../strongs/h/h4325.md), the [ḥallāmîš](../../strongs/h/h2496.md) into a [maʿyān](../../strongs/h/h4599.md) of [mayim](../../strongs/h/h4325.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 113](psalms_113.md) - [Psalms 115](psalms_115.md)