# [Psalms 6](https://www.blueletterbible.org/kjv/psa/6/1/s_484001)

<a name="psalms_6_1"></a>Psalms 6:1

To the [nāṣaḥ](../../strongs/h/h5329.md) on [Nᵊḡînâ](../../strongs/h/h5058.md) upon [šᵊmînîṯ](../../strongs/h/h8067.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [Yĕhovah](../../strongs/h/h3068.md), [yakach](../../strongs/h/h3198.md) me not in thine ['aph](../../strongs/h/h639.md), neither [yacar](../../strongs/h/h3256.md) me in thy [chemah](../../strongs/h/h2534.md).

<a name="psalms_6_2"></a>Psalms 6:2

Have [chanan](../../strongs/h/h2603.md) upon me, [Yĕhovah](../../strongs/h/h3068.md); for I am ['umlal](../../strongs/h/h536.md): [Yĕhovah](../../strongs/h/h3068.md), [rapha'](../../strongs/h/h7495.md) me; for my ['etsem](../../strongs/h/h6106.md) are [bahal](../../strongs/h/h926.md).

<a name="psalms_6_3"></a>Psalms 6:3

My [nephesh](../../strongs/h/h5315.md) is also sore [bahal](../../strongs/h/h926.md): but thou, [Yĕhovah](../../strongs/h/h3068.md), how long?

<a name="psalms_6_4"></a>Psalms 6:4

[shuwb](../../strongs/h/h7725.md), [Yĕhovah](../../strongs/h/h3068.md), [chalats](../../strongs/h/h2502.md) my [nephesh](../../strongs/h/h5315.md): oh [yasha'](../../strongs/h/h3467.md) me for thy [checed](../../strongs/h/h2617.md) sake.

<a name="psalms_6_5"></a>Psalms 6:5

For in [maveth](../../strongs/h/h4194.md) no [zeker](../../strongs/h/h2143.md) of thee: in the [shĕ'owl](../../strongs/h/h7585.md) who shall [yadah](../../strongs/h/h3034.md) thee?

<a name="psalms_6_6"></a>Psalms 6:6

I am [yaga'](../../strongs/h/h3021.md) with my ['anachah](../../strongs/h/h585.md); all the [layil](../../strongs/h/h3915.md) make I my [mittah](../../strongs/h/h4296.md) to [sachah](../../strongs/h/h7811.md); I [macah](../../strongs/h/h4529.md) my ['eres](../../strongs/h/h6210.md) with my [dim'ah](../../strongs/h/h1832.md).

<a name="psalms_6_7"></a>Psalms 6:7

Mine ['ayin](../../strongs/h/h5869.md) is [ʿāšaš](../../strongs/h/h6244.md) because of [ka'ac](../../strongs/h/h3708.md); it ['athaq](../../strongs/h/h6275.md) because of all mine [tsarar](../../strongs/h/h6887.md).

<a name="psalms_6_8"></a>Psalms 6:8

[cuwr](../../strongs/h/h5493.md) from me, all ye [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md); for [Yĕhovah](../../strongs/h/h3068.md) hath [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of my [Bĕkiy](../../strongs/h/h1065.md).

<a name="psalms_6_9"></a>Psalms 6:9

[Yĕhovah](../../strongs/h/h3068.md) hath [shama'](../../strongs/h/h8085.md) my [tĕchinnah](../../strongs/h/h8467.md); [Yĕhovah](../../strongs/h/h3068.md) will receive my [tĕphillah](../../strongs/h/h8605.md).

<a name="psalms_6_10"></a>Psalms 6:10

Let all mine ['oyeb](../../strongs/h/h341.md) be [buwsh](../../strongs/h/h954.md) and [me'od](../../strongs/h/h3966.md) [bahal](../../strongs/h/h926.md): let them [shuwb](../../strongs/h/h7725.md) and be [buwsh](../../strongs/h/h954.md) suddenly.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 5](psalms_5.md) - [Psalms 7](psalms_7.md)