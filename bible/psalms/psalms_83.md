# [Psalms 83](https://www.blueletterbible.org/kjv/psalms/83)

<a name="psalms_83_1"></a>Psalms 83:1

A [šîr](../../strongs/h/h7892.md) or [mizmôr](../../strongs/h/h4210.md) of ['Āsāp̄](../../strongs/h/h623.md). Keep not thou [dᵊmî](../../strongs/h/h1824.md), ['Elohiym](../../strongs/h/h430.md): hold not thy [ḥāraš](../../strongs/h/h2790.md), and be not [šāqaṭ](../../strongs/h/h8252.md), O ['el](../../strongs/h/h410.md).

<a name="psalms_83_2"></a>Psalms 83:2

For, lo, thine ['oyeb](../../strongs/h/h341.md) make a [hāmâ](../../strongs/h/h1993.md): and they that [sane'](../../strongs/h/h8130.md) thee have lifted [nasa'](../../strongs/h/h5375.md) the [ro'sh](../../strongs/h/h7218.md).

<a name="psalms_83_3"></a>Psalms 83:3

They have [ʿāram](../../strongs/h/h6191.md) [sôḏ](../../strongs/h/h5475.md) against thy ['am](../../strongs/h/h5971.md), and [ya'ats](../../strongs/h/h3289.md) against thy [tsaphan](../../strongs/h/h6845.md).

<a name="psalms_83_4"></a>Psalms 83:4

They have ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), and let us [kāḥaḏ](../../strongs/h/h3582.md) them from being a [gowy](../../strongs/h/h1471.md); that the [shem](../../strongs/h/h8034.md) of [Yisra'el](../../strongs/h/h3478.md) may be no more in [zakar](../../strongs/h/h2142.md).

<a name="psalms_83_5"></a>Psalms 83:5

For they have [ya'ats](../../strongs/h/h3289.md) [yaḥaḏ](../../strongs/h/h3162.md) with one [leb](../../strongs/h/h3820.md): they [karath](../../strongs/h/h3772.md) [bĕriyth](../../strongs/h/h1285.md) against thee:

<a name="psalms_83_6"></a>Psalms 83:6

The ['ohel](../../strongs/h/h168.md) of ['Ĕḏōm](../../strongs/h/h123.md), and the [Yišmᵊʿē'lî](../../strongs/h/h3459.md); of [Mô'āḇ](../../strongs/h/h4124.md), and the [Haḡrî](../../strongs/h/h1905.md);

<a name="psalms_83_7"></a>Psalms 83:7

[Gᵊḇāl](../../strongs/h/h1381.md), and [ʿAmmôn](../../strongs/h/h5983.md), and [ʿĂmālēq](../../strongs/h/h6002.md); the [pᵊlešeṯ](../../strongs/h/h6429.md) with the [yashab](../../strongs/h/h3427.md) of [Ṣōr](../../strongs/h/h6865.md);

<a name="psalms_83_8"></a>Psalms 83:8

['Aššûr](../../strongs/h/h804.md) also is [lāvâ](../../strongs/h/h3867.md) with them: they have [zerowa'](../../strongs/h/h2220.md) the [ben](../../strongs/h/h1121.md) of [Lôṭ](../../strongs/h/h3876.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_83_9"></a>Psalms 83:9

['asah](../../strongs/h/h6213.md) unto them as unto the [Miḏyān](../../strongs/h/h4080.md); as to [Sîsrā'](../../strongs/h/h5516.md), as to [Yāḇîn](../../strongs/h/h2985.md), at the [nachal](../../strongs/h/h5158.md) of [Qîšôn](../../strongs/h/h7028.md):

<a name="psalms_83_10"></a>Psalms 83:10

Which [šāmaḏ](../../strongs/h/h8045.md) at [ʿÊn-Ḏôr](../../strongs/h/h5874.md): they became as [dōmen](../../strongs/h/h1828.md) for the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="psalms_83_11"></a>Psalms 83:11

[shiyth](../../strongs/h/h7896.md) their [nāḏîḇ](../../strongs/h/h5081.md) like [ʿōrēḇ](../../strongs/h/h6159.md), and like [Zᵊ'Ēḇ](../../strongs/h/h2062.md): yea, all their [nāsîḵ](../../strongs/h/h5257.md) as [Zeḇaḥ](../../strongs/h/h2078.md), and as [Ṣalmunnāʿ](../../strongs/h/h6759.md):

<a name="psalms_83_12"></a>Psalms 83:12

Who ['āmar](../../strongs/h/h559.md), Let us take to ourselves the [nā'â](../../strongs/h/h4999.md) of ['Elohiym](../../strongs/h/h430.md) in [yarash](../../strongs/h/h3423.md).

<a name="psalms_83_13"></a>Psalms 83:13

O my ['Elohiym](../../strongs/h/h430.md), [shiyth](../../strongs/h/h7896.md) them like a [galgal](../../strongs/h/h1534.md); as the [qaš](../../strongs/h/h7179.md) [paniym](../../strongs/h/h6440.md) the [ruwach](../../strongs/h/h7307.md).

<a name="psalms_83_14"></a>Psalms 83:14

As the ['esh](../../strongs/h/h784.md) [bāʿar](../../strongs/h/h1197.md) a [yaʿar](../../strongs/h/h3293.md), and as the [lehāḇâ](../../strongs/h/h3852.md) [lāhaṭ](../../strongs/h/h3857.md) the [har](../../strongs/h/h2022.md) on [lāhaṭ](../../strongs/h/h3857.md);

<a name="psalms_83_15"></a>Psalms 83:15

So [radaph](../../strongs/h/h7291.md) them with thy [saʿar](../../strongs/h/h5591.md), and make them [bahal](../../strongs/h/h926.md) with thy [sûp̄â](../../strongs/h/h5492.md).

<a name="psalms_83_16"></a>Psalms 83:16

[mālā'](../../strongs/h/h4390.md) their [paniym](../../strongs/h/h6440.md) with [qālôn](../../strongs/h/h7036.md); that they may [bāqaš](../../strongs/h/h1245.md) thy [shem](../../strongs/h/h8034.md), [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_83_17"></a>Psalms 83:17

Let them be [buwsh](../../strongs/h/h954.md) and [bahal](../../strongs/h/h926.md) for [ʿaḏ](../../strongs/h/h5703.md); yea, let them be put to [ḥāp̄ēr](../../strongs/h/h2659.md), and ['abad](../../strongs/h/h6.md):

<a name="psalms_83_18"></a>Psalms 83:18

That men may [yada'](../../strongs/h/h3045.md) that thou, whose [shem](../../strongs/h/h8034.md) alone is [Yĕhovah](../../strongs/h/h3068.md), art the ['elyown](../../strongs/h/h5945.md) over all the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 82](psalms_82.md) - [Psalms 84](psalms_84.md)