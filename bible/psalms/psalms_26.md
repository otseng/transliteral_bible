# [Psalms 26](https://www.blueletterbible.org/kjv/psa/26/1/s_504001)

<a name="psalms_26_1"></a>Psalms 26:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [shaphat](../../strongs/h/h8199.md) me, [Yĕhovah](../../strongs/h/h3068.md); for I have [halak](../../strongs/h/h1980.md) in mine [tom](../../strongs/h/h8537.md): I have [batach](../../strongs/h/h982.md) also in [Yĕhovah](../../strongs/h/h3068.md); therefore I shall not [māʿaḏ](../../strongs/h/h4571.md).

<a name="psalms_26_2"></a>Psalms 26:2

[bachan](../../strongs/h/h974.md) me, [Yĕhovah](../../strongs/h/h3068.md), and [nāsâ](../../strongs/h/h5254.md) me; [tsaraph](../../strongs/h/h6884.md) my [kilyah](../../strongs/h/h3629.md) and my [leb](../../strongs/h/h3820.md).

<a name="psalms_26_3"></a>Psalms 26:3

For thy [checed](../../strongs/h/h2617.md) is before mine ['ayin](../../strongs/h/h5869.md): and I have [halak](../../strongs/h/h1980.md) in thy ['emeth](../../strongs/h/h571.md).

<a name="psalms_26_4"></a>Psalms 26:4

I have not [yashab](../../strongs/h/h3427.md) with [shav'](../../strongs/h/h7723.md) [math](../../strongs/h/h4962.md), neither will I go in with ['alam](../../strongs/h/h5956.md).

<a name="psalms_26_5"></a>Psalms 26:5

I have [sane'](../../strongs/h/h8130.md) the [qāhēl](../../strongs/h/h6951.md) of [ra'a'](../../strongs/h/h7489.md); and will not [yashab](../../strongs/h/h3427.md) with the [rasha'](../../strongs/h/h7563.md).

<a name="psalms_26_6"></a>Psalms 26:6

I will [rāḥaṣ](../../strongs/h/h7364.md) mine [kaph](../../strongs/h/h3709.md) in [niqqāyôn](../../strongs/h/h5356.md): so will I [cabab](../../strongs/h/h5437.md) thine [mizbeach](../../strongs/h/h4196.md), [Yĕhovah](../../strongs/h/h3068.md):

<a name="psalms_26_7"></a>Psalms 26:7

That I may [shama'](../../strongs/h/h8085.md) with the [qowl](../../strongs/h/h6963.md) of [tôḏâ](../../strongs/h/h8426.md), and [sāp̄ar](../../strongs/h/h5608.md) of all thy [pala'](../../strongs/h/h6381.md).

<a name="psalms_26_8"></a>Psalms 26:8

[Yĕhovah](../../strongs/h/h3068.md), I have ['ahab](../../strongs/h/h157.md) the [māʿôn](../../strongs/h/h4583.md) of thy [bayith](../../strongs/h/h1004.md), and the [maqowm](../../strongs/h/h4725.md) where thine [kabowd](../../strongs/h/h3519.md) [miškān](../../strongs/h/h4908.md).

<a name="psalms_26_9"></a>Psalms 26:9

['āsap̄](../../strongs/h/h622.md) not my [nephesh](../../strongs/h/h5315.md) with [chatta'](../../strongs/h/h2400.md), nor my [chay](../../strongs/h/h2416.md) with [dam](../../strongs/h/h1818.md) ['enowsh](../../strongs/h/h582.md):

<a name="psalms_26_10"></a>Psalms 26:10

In whose [yad](../../strongs/h/h3027.md) is [zimmâ](../../strongs/h/h2154.md), and their [yamiyn](../../strongs/h/h3225.md) is [mālā'](../../strongs/h/h4390.md) of [shachad](../../strongs/h/h7810.md).

<a name="psalms_26_11"></a>Psalms 26:11

But as for me, I will [yālaḵ](../../strongs/h/h3212.md) in mine [tom](../../strongs/h/h8537.md): [pāḏâ](../../strongs/h/h6299.md) me, and be [chanan](../../strongs/h/h2603.md) unto me.

<a name="psalms_26_12"></a>Psalms 26:12

My [regel](../../strongs/h/h7272.md) ['amad](../../strongs/h/h5975.md) in a [mîšôr](../../strongs/h/h4334.md): in the [maqhēl](../../strongs/h/h4721.md) will I [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 25](psalms_25.md) - [Psalms 27](psalms_27.md)