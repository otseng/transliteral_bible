# [Psalms 47](https://www.blueletterbible.org/kjv/psa/47)

<a name="psalms_47_1"></a>Psalms 47:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) for the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md). [tāqaʿ](../../strongs/h/h8628.md) your [kaph](../../strongs/h/h3709.md), all ye ['am](../../strongs/h/h5971.md); [rûaʿ](../../strongs/h/h7321.md) unto ['Elohiym](../../strongs/h/h430.md) with the [qowl](../../strongs/h/h6963.md) of [rinnah](../../strongs/h/h7440.md).

<a name="psalms_47_2"></a>Psalms 47:2

For [Yĕhovah](../../strongs/h/h3068.md) ['elyown](../../strongs/h/h5945.md) is [yare'](../../strongs/h/h3372.md); he is a [gadowl](../../strongs/h/h1419.md) [melek](../../strongs/h/h4428.md) over all the ['erets](../../strongs/h/h776.md).

<a name="psalms_47_3"></a>Psalms 47:3

He shall [dabar](../../strongs/h/h1696.md) the ['am](../../strongs/h/h5971.md) under us, and the [lĕom](../../strongs/h/h3816.md) under our [regel](../../strongs/h/h7272.md).

<a name="psalms_47_4"></a>Psalms 47:4

He shall [bāḥar](../../strongs/h/h977.md) our [nachalah](../../strongs/h/h5159.md) for us, the [gā'ôn](../../strongs/h/h1347.md) of [Ya'aqob](../../strongs/h/h3290.md) whom he ['ahab](../../strongs/h/h157.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_47_5"></a>Psalms 47:5

['Elohiym](../../strongs/h/h430.md) is [ʿālâ](../../strongs/h/h5927.md) with a [tᵊrûʿâ](../../strongs/h/h8643.md), [Yĕhovah](../../strongs/h/h3068.md) with the [qowl](../../strongs/h/h6963.md) of a [šôp̄ār](../../strongs/h/h7782.md).

<a name="psalms_47_6"></a>Psalms 47:6

[zamar](../../strongs/h/h2167.md) to ['Elohiym](../../strongs/h/h430.md), [zamar](../../strongs/h/h2167.md): [zamar](../../strongs/h/h2167.md) unto our [melek](../../strongs/h/h4428.md), [zamar](../../strongs/h/h2167.md).

<a name="psalms_47_7"></a>Psalms 47:7

For ['Elohiym](../../strongs/h/h430.md) is the [melek](../../strongs/h/h4428.md) of all the ['erets](../../strongs/h/h776.md): [zamar](../../strongs/h/h2167.md) with [sakal](../../strongs/h/h7919.md).

<a name="psalms_47_8"></a>Psalms 47:8

['Elohiym](../../strongs/h/h430.md) [mālaḵ](../../strongs/h/h4427.md) over the [gowy](../../strongs/h/h1471.md): ['Elohiym](../../strongs/h/h430.md) [yashab](../../strongs/h/h3427.md) upon the [kicce'](../../strongs/h/h3678.md) of his [qodesh](../../strongs/h/h6944.md).

<a name="psalms_47_9"></a>Psalms 47:9

The [nāḏîḇ](../../strongs/h/h5081.md) of the ['am](../../strongs/h/h5971.md) are ['āsap̄](../../strongs/h/h622.md), even the ['am](../../strongs/h/h5971.md) of the ['Elohiym](../../strongs/h/h430.md) of ['Abraham](../../strongs/h/h85.md): for the [magen](../../strongs/h/h4043.md) of the ['erets](../../strongs/h/h776.md) belong unto ['Elohiym](../../strongs/h/h430.md): he is [me'od](../../strongs/h/h3966.md) [ʿālâ](../../strongs/h/h5927.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 46](psalms_46.md) - [Psalms 48](psalms_48.md)