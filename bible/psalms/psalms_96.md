# [Psalms 96](https://www.blueletterbible.org/kjv/psalms/96)

<a name="psalms_96_1"></a>Psalms 96:1

O [shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md) a [ḥāḏāš](../../strongs/h/h2319.md) [šîr](../../strongs/h/h7892.md): [shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md), all the ['erets](../../strongs/h/h776.md).

<a name="psalms_96_2"></a>Psalms 96:2

[shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md), [barak](../../strongs/h/h1288.md) his [shem](../../strongs/h/h8034.md); [bāśar](../../strongs/h/h1319.md) his [yĕshuw'ah](../../strongs/h/h3444.md) from [yowm](../../strongs/h/h3117.md) to [yowm](../../strongs/h/h3117.md).

<a name="psalms_96_3"></a>Psalms 96:3

[sāp̄ar](../../strongs/h/h5608.md) his [kabowd](../../strongs/h/h3519.md) among the [gowy](../../strongs/h/h1471.md), his [pala'](../../strongs/h/h6381.md) among all ['am](../../strongs/h/h5971.md).

<a name="psalms_96_4"></a>Psalms 96:4

For [Yĕhovah](../../strongs/h/h3068.md) is [gadowl](../../strongs/h/h1419.md), and [me'od](../../strongs/h/h3966.md) to be [halal](../../strongs/h/h1984.md): he is to be [yare'](../../strongs/h/h3372.md) above all ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_96_5"></a>Psalms 96:5

For all the ['Elohiym](../../strongs/h/h430.md) of the ['am](../../strongs/h/h5971.md) are ['ĕlîl](../../strongs/h/h457.md): but [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) the [shamayim](../../strongs/h/h8064.md).

<a name="psalms_96_6"></a>Psalms 96:6

[howd](../../strongs/h/h1935.md) and [hadar](../../strongs/h/h1926.md) are [paniym](../../strongs/h/h6440.md) him: ['oz](../../strongs/h/h5797.md) and [tip̄'ārâ](../../strongs/h/h8597.md) are in his [miqdash](../../strongs/h/h4720.md).

<a name="psalms_96_7"></a>Psalms 96:7

[yāhaḇ](../../strongs/h/h3051.md) unto [Yĕhovah](../../strongs/h/h3068.md), O ye [mišpāḥâ](../../strongs/h/h4940.md) of the ['am](../../strongs/h/h5971.md), [yāhaḇ](../../strongs/h/h3051.md) unto [Yĕhovah](../../strongs/h/h3068.md) [kabowd](../../strongs/h/h3519.md) and ['oz](../../strongs/h/h5797.md).

<a name="psalms_96_8"></a>Psalms 96:8

[yāhaḇ](../../strongs/h/h3051.md) unto [Yĕhovah](../../strongs/h/h3068.md) the [kabowd](../../strongs/h/h3519.md) due unto his [shem](../../strongs/h/h8034.md): [nasa'](../../strongs/h/h5375.md) a [minchah](../../strongs/h/h4503.md), and [bow'](../../strongs/h/h935.md) into his [ḥāṣēr](../../strongs/h/h2691.md).

<a name="psalms_96_9"></a>Psalms 96:9

[shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md) in the [hăḏārâ](../../strongs/h/h1927.md) of [qodesh](../../strongs/h/h6944.md): [chuwl](../../strongs/h/h2342.md) [paniym](../../strongs/h/h6440.md) him, all the ['erets](../../strongs/h/h776.md).

<a name="psalms_96_10"></a>Psalms 96:10

['āmar](../../strongs/h/h559.md) among the [gowy](../../strongs/h/h1471.md) that [Yĕhovah](../../strongs/h/h3068.md) [mālaḵ](../../strongs/h/h4427.md): the [tebel](../../strongs/h/h8398.md) also shall be [kuwn](../../strongs/h/h3559.md) that it shall not be [mowt](../../strongs/h/h4131.md): he shall [diyn](../../strongs/h/h1777.md) the ['am](../../strongs/h/h5971.md) [meyshar](../../strongs/h/h4339.md).

<a name="psalms_96_11"></a>Psalms 96:11

Let the [shamayim](../../strongs/h/h8064.md) [samach](../../strongs/h/h8055.md), and let the ['erets](../../strongs/h/h776.md) be [giyl](../../strongs/h/h1523.md); let the [yam](../../strongs/h/h3220.md) [ra'am](../../strongs/h/h7481.md), and the [mᵊlō'](../../strongs/h/h4393.md) thereof.

<a name="psalms_96_12"></a>Psalms 96:12

Let the [sadeh](../../strongs/h/h7704.md) be [ʿālaz](../../strongs/h/h5937.md), and all that is therein: then shall all the ['ets](../../strongs/h/h6086.md) of the [yaʿar](../../strongs/h/h3293.md) [ranan](../../strongs/h/h7442.md)

<a name="psalms_96_13"></a>Psalms 96:13

[paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): for he [bow'](../../strongs/h/h935.md), for he [bow'](../../strongs/h/h935.md) to [shaphat](../../strongs/h/h8199.md) the ['erets](../../strongs/h/h776.md): he shall [shaphat](../../strongs/h/h8199.md) the [tebel](../../strongs/h/h8398.md) with [tsedeq](../../strongs/h/h6664.md), and the ['am](../../strongs/h/h5971.md) with his ['ĕmûnâ](../../strongs/h/h530.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 95](psalms_95.md) - [Psalms 97](psalms_97.md)