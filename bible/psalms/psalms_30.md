# [Psalms 30](https://www.blueletterbible.org/kjv/psa/30/1/s_508001)

<a name="psalms_30_1"></a>Psalms 30:1

A [mizmôr](../../strongs/h/h4210.md) [and] [šîr](../../strongs/h/h7892.md) [at] the [ḥănukâ](../../strongs/h/h2598.md) of the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md). I will [ruwm](../../strongs/h/h7311.md) thee, [Yĕhovah](../../strongs/h/h3068.md); for thou hast [dālâ](../../strongs/h/h1802.md) me, and hast not made my ['oyeb](../../strongs/h/h341.md) to [samach](../../strongs/h/h8055.md) over me.

<a name="psalms_30_2"></a>Psalms 30:2

[Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), I [šāvaʿ](../../strongs/h/h7768.md) unto thee, and thou hast [rapha'](../../strongs/h/h7495.md) me.

<a name="psalms_30_3"></a>Psalms 30:3

[Yĕhovah](../../strongs/h/h3068.md), thou hast [ʿālâ](../../strongs/h/h5927.md) my [nephesh](../../strongs/h/h5315.md) from the [shĕ'owl](../../strongs/h/h7585.md): thou hast kept me [ḥāyâ](../../strongs/h/h2421.md), that I should not [yarad](../../strongs/h/h3381.md) [yarad](../../strongs/h/h3381.md) to the [bowr](../../strongs/h/h953.md).

<a name="psalms_30_4"></a>Psalms 30:4

[zamar](../../strongs/h/h2167.md) unto [Yĕhovah](../../strongs/h/h3068.md), O ye [chaciyd](../../strongs/h/h2623.md) of his, and [yadah](../../strongs/h/h3034.md) at the [zeker](../../strongs/h/h2143.md) of his [qodesh](../../strongs/h/h6944.md).

<a name="psalms_30_5"></a>Psalms 30:5

For his ['aph](../../strongs/h/h639.md) endureth but a [reḡaʿ](../../strongs/h/h7281.md); in his [ratsown](../../strongs/h/h7522.md) is [chay](../../strongs/h/h2416.md): [bĕkiy](../../strongs/h/h1065.md) may [lûn](../../strongs/h/h3885.md) for an ['ereb](../../strongs/h/h6153.md), but [rinnah](../../strongs/h/h7440.md) cometh in the [boqer](../../strongs/h/h1242.md).

<a name="psalms_30_6"></a>Psalms 30:6

And in my [šelev](../../strongs/h/h7959.md) I ['āmar](../../strongs/h/h559.md), I shall never be [mowt](../../strongs/h/h4131.md).

<a name="psalms_30_7"></a>Psalms 30:7

[Yĕhovah](../../strongs/h/h3068.md), by thy [ratsown](../../strongs/h/h7522.md) thou hast made my [har](../../strongs/h/h2042.md) to ['amad](../../strongs/h/h5975.md) ['oz](../../strongs/h/h5797.md): thou didst [cathar](../../strongs/h/h5641.md) thy [paniym](../../strongs/h/h6440.md), and I was [bahal](../../strongs/h/h926.md).

<a name="psalms_30_8"></a>Psalms 30:8

I [qara'](../../strongs/h/h7121.md) to thee, [Yĕhovah](../../strongs/h/h3068.md); and unto [Yĕhovah](../../strongs/h/h3068.md) I made [chanan](../../strongs/h/h2603.md).

<a name="psalms_30_9"></a>Psalms 30:9

What [beṣaʿ](../../strongs/h/h1215.md) is there in my [dam](../../strongs/h/h1818.md), when I [yarad](../../strongs/h/h3381.md) to the [shachath](../../strongs/h/h7845.md)? Shall the ['aphar](../../strongs/h/h6083.md) [yadah](../../strongs/h/h3034.md) thee? shall it [nāḡaḏ](../../strongs/h/h5046.md) thy ['emeth](../../strongs/h/h571.md)?

<a name="psalms_30_10"></a>Psalms 30:10

[shama'](../../strongs/h/h8085.md), [Yĕhovah](../../strongs/h/h3068.md), and have [chanan](../../strongs/h/h2603.md) upon me: [Yĕhovah](../../strongs/h/h3068.md), be thou my helper.

<a name="psalms_30_11"></a>Psalms 30:11

Thou hast [hāp̄aḵ](../../strongs/h/h2015.md) for me my [mispēḏ](../../strongs/h/h4553.md) into [māḥôl](../../strongs/h/h4234.md): thou hast [pāṯaḥ](../../strongs/h/h6605.md) my [śaq](../../strongs/h/h8242.md), and ['āzar](../../strongs/h/h247.md) me with [simchah](../../strongs/h/h8057.md);

<a name="psalms_30_12"></a>Psalms 30:12

To the end that my [kabowd](../../strongs/h/h3519.md) may [zamar](../../strongs/h/h2167.md) praise to thee, and not be [damam](../../strongs/h/h1826.md). [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), I will [yadah](../../strongs/h/h3034.md) unto thee ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 29](psalms_29.md) - [Psalms 31](psalms_31.md)