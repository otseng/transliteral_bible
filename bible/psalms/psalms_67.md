# [Psalms 67](https://www.blueletterbible.org/kjv/psa/67)

<a name="psalms_67_1"></a>Psalms 67:1

To the [nāṣaḥ](../../strongs/h/h5329.md) on [Nᵊḡînâ](../../strongs/h/h5058.md), A [mizmôr](../../strongs/h/h4210.md) or [šîr](../../strongs/h/h7892.md).  ['Elohiym](../../strongs/h/h430.md) be [chanan](../../strongs/h/h2603.md) unto us, and [barak](../../strongs/h/h1288.md) us; and cause his [paniym](../../strongs/h/h6440.md) to ['owr](../../strongs/h/h215.md) upon us; [Celah](../../strongs/h/h5542.md).

<a name="psalms_67_2"></a>Psalms 67:2

That thy [derek](../../strongs/h/h1870.md) may be [yada'](../../strongs/h/h3045.md) upon ['erets](../../strongs/h/h776.md), thy [yĕshuw'ah](../../strongs/h/h3444.md) among all [gowy](../../strongs/h/h1471.md).

<a name="psalms_67_3"></a>Psalms 67:3

Let the ['am](../../strongs/h/h5971.md) [yadah](../../strongs/h/h3034.md) thee, ['Elohiym](../../strongs/h/h430.md) ; let all the ['am](../../strongs/h/h5971.md) [yadah](../../strongs/h/h3034.md) thee.

<a name="psalms_67_4"></a>Psalms 67:4

O let the [lĕom](../../strongs/h/h3816.md) be [samach](../../strongs/h/h8055.md) and sing for [ranan](../../strongs/h/h7442.md) : for thou shalt [shaphat](../../strongs/h/h8199.md) the ['am](../../strongs/h/h5971.md) [mîšôr](../../strongs/h/h4334.md), and [nachah](../../strongs/h/h5148.md) the [lĕom](../../strongs/h/h3816.md) upon ['erets](../../strongs/h/h776.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_67_5"></a>Psalms 67:5

Let the ['am](../../strongs/h/h5971.md) [yadah](../../strongs/h/h3034.md) thee, ['Elohiym](../../strongs/h/h430.md) ; let all the ['am](../../strongs/h/h5971.md) [yadah](../../strongs/h/h3034.md) thee.

<a name="psalms_67_6"></a>Psalms 67:6

Then shall the ['erets](../../strongs/h/h776.md) [nathan](../../strongs/h/h5414.md) her [yᵊḇûl](../../strongs/h/h2981.md); and ['Elohiym](../../strongs/h/h430.md), even our own ['Elohiym](../../strongs/h/h430.md), shall [barak](../../strongs/h/h1288.md) us.

<a name="psalms_67_7"></a>Psalms 67:7

['Elohiym](../../strongs/h/h430.md) shall [barak](../../strongs/h/h1288.md) us; and all the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md) shall [yare'](../../strongs/h/h3372.md) him.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 66](psalms_66.md) - [Psalms 68](psalms_68.md)