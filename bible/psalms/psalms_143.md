# [Psalms 143](https://www.blueletterbible.org/kjv/psalms/143)

<a name="psalms_143_1"></a>Psalms 143:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [shama'](../../strongs/h/h8085.md) my [tĕphillah](../../strongs/h/h8605.md), [Yĕhovah](../../strongs/h/h3068.md), ['azan](../../strongs/h/h238.md) to my [taḥănûn](../../strongs/h/h8469.md): in thy ['ĕmûnâ](../../strongs/h/h530.md) ['anah](../../strongs/h/h6030.md) me, and in thy [tsedaqah](../../strongs/h/h6666.md).

<a name="psalms_143_2"></a>Psalms 143:2

And [bow'](../../strongs/h/h935.md) not into [mishpat](../../strongs/h/h4941.md) with thy ['ebed](../../strongs/h/h5650.md): for in thy [paniym](../../strongs/h/h6440.md) shall no [chay](../../strongs/h/h2416.md) be [ṣāḏaq](../../strongs/h/h6663.md).

<a name="psalms_143_3"></a>Psalms 143:3

For the ['oyeb](../../strongs/h/h341.md) hath [radaph](../../strongs/h/h7291.md) my [nephesh](../../strongs/h/h5315.md); he hath [dāḵā'](../../strongs/h/h1792.md) my [chay](../../strongs/h/h2416.md) [dāḵā'](../../strongs/h/h1792.md) to the ['erets](../../strongs/h/h776.md); he hath made me to [yashab](../../strongs/h/h3427.md) in [maḥšāḵ](../../strongs/h/h4285.md), as those that have been ['owlam](../../strongs/h/h5769.md) [muwth](../../strongs/h/h4191.md).

<a name="psalms_143_4"></a>Psalms 143:4

Therefore is my [ruwach](../../strongs/h/h7307.md) [ʿāṭap̄](../../strongs/h/h5848.md) within me; my [leb](../../strongs/h/h3820.md) [tavek](../../strongs/h/h8432.md) me is [šāmēm](../../strongs/h/h8074.md).

<a name="psalms_143_5"></a>Psalms 143:5

I [zakar](../../strongs/h/h2142.md) the [yowm](../../strongs/h/h3117.md) of [qeḏem](../../strongs/h/h6924.md); I [hagah](../../strongs/h/h1897.md) on all thy [pōʿal](../../strongs/h/h6467.md); I [śîaḥ](../../strongs/h/h7878.md) on the [ma'aseh](../../strongs/h/h4639.md) of thy [yad](../../strongs/h/h3027.md).

<a name="psalms_143_6"></a>Psalms 143:6

I [pāraś](../../strongs/h/h6566.md) my [yad](../../strongs/h/h3027.md) unto thee: my [nephesh](../../strongs/h/h5315.md) thirsteth after thee, as an [ʿāyēp̄](../../strongs/h/h5889.md) ['erets](../../strongs/h/h776.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_143_7"></a>Psalms 143:7

['anah](../../strongs/h/h6030.md) me [mahēr](../../strongs/h/h4118.md), [Yĕhovah](../../strongs/h/h3068.md): my [ruwach](../../strongs/h/h7307.md) [kalah](../../strongs/h/h3615.md): [cathar](../../strongs/h/h5641.md) not thy [paniym](../../strongs/h/h6440.md) from me, lest I be [māšal](../../strongs/h/h4911.md) unto them that [yarad](../../strongs/h/h3381.md) into the [bowr](../../strongs/h/h953.md).

<a name="psalms_143_8"></a>Psalms 143:8

Cause me to [shama'](../../strongs/h/h8085.md) thy [checed](../../strongs/h/h2617.md) in the [boqer](../../strongs/h/h1242.md); for in thee do I [batach](../../strongs/h/h982.md): cause me to [yada'](../../strongs/h/h3045.md) the [derek](../../strongs/h/h1870.md) [zû](../../strongs/h/h2098.md) I should [yālaḵ](../../strongs/h/h3212.md); for I [nasa'](../../strongs/h/h5375.md) my [nephesh](../../strongs/h/h5315.md) unto thee.

<a name="psalms_143_9"></a>Psalms 143:9

[natsal](../../strongs/h/h5337.md) me, [Yĕhovah](../../strongs/h/h3068.md), from mine ['oyeb](../../strongs/h/h341.md): I flee unto thee to [kāsâ](../../strongs/h/h3680.md) me.

<a name="psalms_143_10"></a>Psalms 143:10

[lamad](../../strongs/h/h3925.md) me to ['asah](../../strongs/h/h6213.md) thy [ratsown](../../strongs/h/h7522.md); for thou art my ['Elohiym](../../strongs/h/h430.md): thy [ruwach](../../strongs/h/h7307.md) is [towb](../../strongs/h/h2896.md); [nachah](../../strongs/h/h5148.md) me into the ['erets](../../strongs/h/h776.md) of [mîšôr](../../strongs/h/h4334.md).

<a name="psalms_143_11"></a>Psalms 143:11

[ḥāyâ](../../strongs/h/h2421.md) me, [Yĕhovah](../../strongs/h/h3068.md), for thy [shem](../../strongs/h/h8034.md) sake: for thy [tsedaqah](../../strongs/h/h6666.md) sake [yāṣā'](../../strongs/h/h3318.md) my [nephesh](../../strongs/h/h5315.md) out of [tsarah](../../strongs/h/h6869.md).

<a name="psalms_143_12"></a>Psalms 143:12

And of thy [checed](../../strongs/h/h2617.md) [ṣāmaṯ](../../strongs/h/h6789.md) mine ['oyeb](../../strongs/h/h341.md), and ['abad](../../strongs/h/h6.md) all them that [tsarar](../../strongs/h/h6887.md) my [nephesh](../../strongs/h/h5315.md): for I am thy ['ebed](../../strongs/h/h5650.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 142](psalms_142.md) - [Psalms 144](psalms_144.md)