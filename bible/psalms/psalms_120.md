# [Psalms 120](https://www.blueletterbible.org/kjv/psalms/120)

<a name="psalms_120_1"></a>Psalms 120:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md). In my [tsarah](../../strongs/h/h6869.md) I [qara'](../../strongs/h/h7121.md) unto [Yĕhovah](../../strongs/h/h3068.md), and he ['anah](../../strongs/h/h6030.md) me.

<a name="psalms_120_2"></a>Psalms 120:2

[natsal](../../strongs/h/h5337.md) my [nephesh](../../strongs/h/h5315.md), [Yĕhovah](../../strongs/h/h3068.md), from [sheqer](../../strongs/h/h8267.md) [saphah](../../strongs/h/h8193.md), and from a [rᵊmîyâ](../../strongs/h/h7423.md) [lashown](../../strongs/h/h3956.md).

<a name="psalms_120_3"></a>Psalms 120:3

What shall be [nathan](../../strongs/h/h5414.md) unto thee? or what shall be [yāsap̄](../../strongs/h/h3254.md) unto thee, thou [rᵊmîyâ](../../strongs/h/h7423.md) [lashown](../../strongs/h/h3956.md)?

<a name="psalms_120_4"></a>Psalms 120:4

[šānan](../../strongs/h/h8150.md) [chets](../../strongs/h/h2671.md) of the [gibôr](../../strongs/h/h1368.md), with [gechel](../../strongs/h/h1513.md) of [rōṯem](../../strongs/h/h7574.md).

<a name="psalms_120_5"></a>Psalms 120:5

['ôyâ](../../strongs/h/h190.md) is me, that I [guwr](../../strongs/h/h1481.md) in [Mešeḵ](../../strongs/h/h4902.md), that I [shakan](../../strongs/h/h7931.md) in the ['ohel](../../strongs/h/h168.md) of [Qēḏār](../../strongs/h/h6938.md)!

<a name="psalms_120_6"></a>Psalms 120:6

My [nephesh](../../strongs/h/h5315.md) hath [rab](../../strongs/h/h7227.md) [shakan](../../strongs/h/h7931.md) with him that [sane'](../../strongs/h/h8130.md) [shalowm](../../strongs/h/h7965.md).

<a name="psalms_120_7"></a>Psalms 120:7

I am for [shalowm](../../strongs/h/h7965.md): but when I [dabar](../../strongs/h/h1696.md), they are for [milḥāmâ](../../strongs/h/h4421.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 119](psalms_119.md) - [Psalms 121](psalms_121.md)