# [Psalms 48](https://www.blueletterbible.org/kjv/psa/48)

<a name="psalms_48_1"></a>Psalms 48:1

A [šîr](../../strongs/h/h7892.md) [and] [mizmôr](../../strongs/h/h4210.md) for the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md). [gadowl](../../strongs/h/h1419.md) is [Yĕhovah](../../strongs/h/h3068.md), and [me'od](../../strongs/h/h3966.md) to be [halal](../../strongs/h/h1984.md) in the [ʿîr](../../strongs/h/h5892.md) of our ['Elohiym](../../strongs/h/h430.md), in the [har](../../strongs/h/h2022.md) of his [qodesh](../../strongs/h/h6944.md).

<a name="psalms_48_2"></a>Psalms 48:2

[yāp̄ê](../../strongs/h/h3303.md) for [nôp̄](../../strongs/h/h5131.md), the [māśôś](../../strongs/h/h4885.md) of the ['erets](../../strongs/h/h776.md), is [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md), on the [yᵊrēḵâ](../../strongs/h/h3411.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md), the [qiryâ](../../strongs/h/h7151.md) of the [rab](../../strongs/h/h7227.md) [melek](../../strongs/h/h4428.md).

<a name="psalms_48_3"></a>Psalms 48:3

['Elohiym](../../strongs/h/h430.md) is [yada'](../../strongs/h/h3045.md) in her ['armôn](../../strongs/h/h759.md) for a [misgab](../../strongs/h/h4869.md).

<a name="psalms_48_4"></a>Psalms 48:4

For, lo, the [melek](../../strongs/h/h4428.md) were [yāʿaḏ](../../strongs/h/h3259.md), they ['abar](../../strongs/h/h5674.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="psalms_48_5"></a>Psalms 48:5

They [ra'ah](../../strongs/h/h7200.md) it, and so they [tāmah](../../strongs/h/h8539.md); they were [bahal](../../strongs/h/h926.md), and [ḥāp̄az](../../strongs/h/h2648.md).

<a name="psalms_48_6"></a>Psalms 48:6

[ra'ad](../../strongs/h/h7461.md) ['āḥaz](../../strongs/h/h270.md) upon them there, and [ḥîl](../../strongs/h/h2427.md), as of a [yalad](../../strongs/h/h3205.md).

<a name="psalms_48_7"></a>Psalms 48:7

Thou [shabar](../../strongs/h/h7665.md) the ['ŏnîyâ](../../strongs/h/h591.md) of [Taršîš](../../strongs/h/h8659.md) with a [qāḏîm](../../strongs/h/h6921.md) [ruwach](../../strongs/h/h7307.md).

<a name="psalms_48_8"></a>Psalms 48:8

As we have [shama'](../../strongs/h/h8085.md), so have we [ra'ah](../../strongs/h/h7200.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), in the [ʿîr](../../strongs/h/h5892.md) of our ['Elohiym](../../strongs/h/h430.md): ['Elohiym](../../strongs/h/h430.md) will [kuwn](../../strongs/h/h3559.md) it ['owlam](../../strongs/h/h5769.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_48_9"></a>Psalms 48:9

We have [dāmâ](../../strongs/h/h1819.md) of thy [checed](../../strongs/h/h2617.md), ['Elohiym](../../strongs/h/h430.md), in the [qereḇ](../../strongs/h/h7130.md) of thy [heykal](../../strongs/h/h1964.md).

<a name="psalms_48_10"></a>Psalms 48:10

According to thy [shem](../../strongs/h/h8034.md), ['Elohiym](../../strongs/h/h430.md), so is thy [tehillah](../../strongs/h/h8416.md) unto the [qeṣev](../../strongs/h/h7099.md) of the ['erets](../../strongs/h/h776.md): thy [yamiyn](../../strongs/h/h3225.md) is [mālā'](../../strongs/h/h4390.md) of [tsedeq](../../strongs/h/h6664.md).

<a name="psalms_48_11"></a>Psalms 48:11

Let [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md) [samach](../../strongs/h/h8055.md), let the [bath](../../strongs/h/h1323.md) of [Yehuwdah](../../strongs/h/h3063.md) be [giyl](../../strongs/h/h1523.md), because of thy [mishpat](../../strongs/h/h4941.md).

<a name="psalms_48_12"></a>Psalms 48:12

[cabab](../../strongs/h/h5437.md) [Tsiyown](../../strongs/h/h6726.md), and [naqaph](../../strongs/h/h5362.md) her: [sāp̄ar](../../strongs/h/h5608.md) the [miḡdāl](../../strongs/h/h4026.md) thereof.

<a name="psalms_48_13"></a>Psalms 48:13

[shiyth](../../strongs/h/h7896.md) ye [leb](../../strongs/h/h3820.md) her [ḥêlâ](../../strongs/h/h2430.md), [pāsaḡ](../../strongs/h/h6448.md) her ['armôn](../../strongs/h/h759.md); that ye may [sāp̄ar](../../strongs/h/h5608.md) it to the [dôr](../../strongs/h/h1755.md) ['aḥărôn](../../strongs/h/h314.md).

<a name="psalms_48_14"></a>Psalms 48:14

For this ['Elohiym](../../strongs/h/h430.md) is our ['Elohiym](../../strongs/h/h430.md) ['owlam](../../strongs/h/h5769.md) and [ʿaḏ](../../strongs/h/h5703.md): he will be our [nāhaḡ](../../strongs/h/h5090.md) even unto [mûṯ](../../strongs/h/h4192.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 47](psalms_47.md) - [Psalms 49](psalms_49.md)