# [Psalms 147](https://www.blueletterbible.org/kjv/psalms/147)

<a name="psalms_147_1"></a>Psalms 147:1

[halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md): for it is [towb](../../strongs/h/h2896.md) to [zamar](../../strongs/h/h2167.md) unto our ['Elohiym](../../strongs/h/h430.md); for it is [na'iym](../../strongs/h/h5273.md); and [tehillah](../../strongs/h/h8416.md) is [nā'vê](../../strongs/h/h5000.md).

<a name="psalms_147_2"></a>Psalms 147:2

[Yĕhovah](../../strongs/h/h3068.md) doth [bānâ](../../strongs/h/h1129.md) [Yĕruwshalaim](../../strongs/h/h3389.md): he [kānas](../../strongs/h/h3664.md) the [dāḥâ](../../strongs/h/h1760.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="psalms_147_3"></a>Psalms 147:3

He [rapha'](../../strongs/h/h7495.md) the [shabar](../../strongs/h/h7665.md) in [leb](../../strongs/h/h3820.md), and [ḥāḇaš](../../strongs/h/h2280.md) their ['atstsebeth](../../strongs/h/h6094.md).

<a name="psalms_147_4"></a>Psalms 147:4

He [mānâ](../../strongs/h/h4487.md) the [mispār](../../strongs/h/h4557.md) of the [kowkab](../../strongs/h/h3556.md); he [qara'](../../strongs/h/h7121.md) them all by their [shem](../../strongs/h/h8034.md).

<a name="psalms_147_5"></a>Psalms 147:5

[gadowl](../../strongs/h/h1419.md) is our ['adown](../../strongs/h/h113.md), and of [rab](../../strongs/h/h7227.md) [koach](../../strongs/h/h3581.md): his [tāḇûn](../../strongs/h/h8394.md) is [mispār](../../strongs/h/h4557.md).

<a name="psalms_147_6"></a>Psalms 147:6

[Yĕhovah](../../strongs/h/h3068.md) [ʿûḏ](../../strongs/h/h5749.md) the ['anav](../../strongs/h/h6035.md): he [šāp̄ēl](../../strongs/h/h8213.md) the [rasha'](../../strongs/h/h7563.md) [šāp̄ēl](../../strongs/h/h8213.md) to the ['erets](../../strongs/h/h776.md).

<a name="psalms_147_7"></a>Psalms 147:7

['anah](../../strongs/h/h6030.md) unto [Yĕhovah](../../strongs/h/h3068.md) with [tôḏâ](../../strongs/h/h8426.md); [zamar](../../strongs/h/h2167.md) upon the [kinnôr](../../strongs/h/h3658.md) unto our ['Elohiym](../../strongs/h/h430.md):

<a name="psalms_147_8"></a>Psalms 147:8

Who [kāsâ](../../strongs/h/h3680.md) the [shamayim](../../strongs/h/h8064.md) with ['ab](../../strongs/h/h5645.md), who [kuwn](../../strongs/h/h3559.md) [māṭār](../../strongs/h/h4306.md) for the ['erets](../../strongs/h/h776.md), who maketh [chatsiyr](../../strongs/h/h2682.md) to [ṣāmaḥ](../../strongs/h/h6779.md) upon the [har](../../strongs/h/h2022.md).

<a name="psalms_147_9"></a>Psalms 147:9

He [nathan](../../strongs/h/h5414.md) to the [bĕhemah](../../strongs/h/h929.md) his [lechem](../../strongs/h/h3899.md), and to the [ben](../../strongs/h/h1121.md) [ʿōrēḇ](../../strongs/h/h6158.md) which [qara'](../../strongs/h/h7121.md).

<a name="psalms_147_10"></a>Psalms 147:10

He [ḥāp̄ēṣ](../../strongs/h/h2654.md) not in the [gᵊḇûrâ](../../strongs/h/h1369.md) of the [sûs](../../strongs/h/h5483.md): he taketh not [ratsah](../../strongs/h/h7521.md) in the [šôq](../../strongs/h/h7785.md) of an ['iysh](../../strongs/h/h376.md).

<a name="psalms_147_11"></a>Psalms 147:11

[Yĕhovah](../../strongs/h/h3068.md) [ratsah](../../strongs/h/h7521.md) in them that [yārē'](../../strongs/h/h3373.md) him, in those that [yāḥal](../../strongs/h/h3176.md) in his [checed](../../strongs/h/h2617.md).

<a name="psalms_147_12"></a>Psalms 147:12

[shabach](../../strongs/h/h7623.md) [Yĕhovah](../../strongs/h/h3068.md), [Yĕruwshalaim](../../strongs/h/h3389.md); [halal](../../strongs/h/h1984.md) thy ['Elohiym](../../strongs/h/h430.md), [Tsiyown](../../strongs/h/h6726.md).

<a name="psalms_147_13"></a>Psalms 147:13

For he hath [ḥāzaq](../../strongs/h/h2388.md) the [bᵊrîaḥ](../../strongs/h/h1280.md) of thy [sha'ar](../../strongs/h/h8179.md); he hath [barak](../../strongs/h/h1288.md) thy [ben](../../strongs/h/h1121.md) [qereḇ](../../strongs/h/h7130.md) thee.

<a name="psalms_147_14"></a>Psalms 147:14

He [śûm](../../strongs/h/h7760.md) [shalowm](../../strongs/h/h7965.md) in thy [gᵊḇûl](../../strongs/h/h1366.md), and [sāׂbaʿ](../../strongs/h/h7646.md) thee with the [cheleb](../../strongs/h/h2459.md) of the [ḥiṭṭâ](../../strongs/h/h2406.md).

<a name="psalms_147_15"></a>Psalms 147:15

He [shalach](../../strongs/h/h7971.md) his ['imrah](../../strongs/h/h565.md) upon ['erets](../../strongs/h/h776.md): his [dabar](../../strongs/h/h1697.md) [rûṣ](../../strongs/h/h7323.md) [mᵊhērâ](../../strongs/h/h4120.md).

<a name="psalms_147_16"></a>Psalms 147:16

He [nathan](../../strongs/h/h5414.md) [šeleḡ](../../strongs/h/h7950.md) like [ṣemer](../../strongs/h/h6785.md): he [p̄zr](../../strongs/h/h6340.md) the [kᵊp̄ôr](../../strongs/h/h3713.md) like ['ēp̄er](../../strongs/h/h665.md).

<a name="psalms_147_17"></a>Psalms 147:17

He [shalak](../../strongs/h/h7993.md) his [qeraḥ](../../strongs/h/h7140.md) like [paṯ](../../strongs/h/h6595.md): who can ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) his [qārâ](../../strongs/h/h7135.md)?

<a name="psalms_147_18"></a>Psalms 147:18

He [shalach](../../strongs/h/h7971.md) his [dabar](../../strongs/h/h1697.md), and [macah](../../strongs/h/h4529.md) them: he causeth his [ruwach](../../strongs/h/h7307.md) to [nāšaḇ](../../strongs/h/h5380.md), and the [mayim](../../strongs/h/h4325.md) [nāzal](../../strongs/h/h5140.md).

<a name="psalms_147_19"></a>Psalms 147:19

He [nāḡaḏ](../../strongs/h/h5046.md) his [dabar](../../strongs/h/h1697.md) [dabar](../../strongs/h/h1697.md) unto [Ya'aqob](../../strongs/h/h3290.md), his [choq](../../strongs/h/h2706.md) and his [mishpat](../../strongs/h/h4941.md) unto [Yisra'el](../../strongs/h/h3478.md).

<a name="psalms_147_20"></a>Psalms 147:20

He hath not ['asah](../../strongs/h/h6213.md) so with any [gowy](../../strongs/h/h1471.md): and as for his [mishpat](../../strongs/h/h4941.md), they have not [yada'](../../strongs/h/h3045.md) them. [halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 146](psalms_146.md) - [Psalms 148](psalms_148.md)