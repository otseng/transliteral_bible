# [Psalms 139](https://www.blueletterbible.org/kjv/psalms/139)

<a name="psalms_139_1"></a>Psalms 139:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [Yĕhovah](../../strongs/h/h3068.md), thou hast [chaqar](../../strongs/h/h2713.md) me, and [yada'](../../strongs/h/h3045.md) me.

<a name="psalms_139_2"></a>Psalms 139:2

Thou [yada'](../../strongs/h/h3045.md) my [yashab](../../strongs/h/h3427.md) and mine [quwm](../../strongs/h/h6965.md), thou [bîn](../../strongs/h/h995.md) my [rēaʿ](../../strongs/h/h7454.md) afar [rachowq](../../strongs/h/h7350.md).

<a name="psalms_139_3"></a>Psalms 139:3

Thou [zārâ](../../strongs/h/h2219.md) my ['orach](../../strongs/h/h734.md) and my [reḇaʿ](../../strongs/h/h7252.md), and art [sāḵan](../../strongs/h/h5532.md) with all my [derek](../../strongs/h/h1870.md).

<a name="psalms_139_4"></a>Psalms 139:4

For there is not a [millâ](../../strongs/h/h4405.md) in my [lashown](../../strongs/h/h3956.md), but, lo, [Yĕhovah](../../strongs/h/h3068.md), thou [yada'](../../strongs/h/h3045.md) it altogether.

<a name="psalms_139_5"></a>Psalms 139:5

Thou hast [ṣûr](../../strongs/h/h6696.md) me ['āḥôr](../../strongs/h/h268.md) and [qeḏem](../../strongs/h/h6924.md), and [shiyth](../../strongs/h/h7896.md) thine [kaph](../../strongs/h/h3709.md) upon me.

<a name="psalms_139_6"></a>Psalms 139:6

Such [da'ath](../../strongs/h/h1847.md) is [pil'î](../../strongs/h/h6383.md) [pil'î](../../strongs/h/h6383.md) for me; it is [śāḡaḇ](../../strongs/h/h7682.md), I [yakol](../../strongs/h/h3201.md) attain unto it.

<a name="psalms_139_7"></a>Psalms 139:7

Whither shall I [yālaḵ](../../strongs/h/h3212.md) from thy [ruwach](../../strongs/h/h7307.md)? or whither shall I [bāraḥ](../../strongs/h/h1272.md) from thy [paniym](../../strongs/h/h6440.md)?

<a name="psalms_139_8"></a>Psalms 139:8

If I [nāsaq](../../strongs/h/h5266.md) into [shamayim](../../strongs/h/h8064.md), thou art there: if I [yāṣaʿ](../../strongs/h/h3331.md) in [shĕ'owl](../../strongs/h/h7585.md), behold, thou art there.

<a name="psalms_139_9"></a>Psalms 139:9

If I [nasa'](../../strongs/h/h5375.md) the [kanaph](../../strongs/h/h3671.md) of the [šaḥar](../../strongs/h/h7837.md), and [shakan](../../strongs/h/h7931.md) in the ['aḥărîṯ](../../strongs/h/h319.md) of the [yam](../../strongs/h/h3220.md);

<a name="psalms_139_10"></a>Psalms 139:10

Even there shall thy [yad](../../strongs/h/h3027.md) [nachah](../../strongs/h/h5148.md) me, and thy [yamiyn](../../strongs/h/h3225.md) shall ['āḥaz](../../strongs/h/h270.md) me.

<a name="psalms_139_11"></a>Psalms 139:11

If I ['āmar](../../strongs/h/h559.md), Surely the [choshek](../../strongs/h/h2822.md) shall [shuwph](../../strongs/h/h7779.md) me; even the [layil](../../strongs/h/h3915.md) shall be ['owr](../../strongs/h/h216.md) about [bᵊʿaḏ](../../strongs/h/h1157.md).

<a name="psalms_139_12"></a>Psalms 139:12

Yea, the [choshek](../../strongs/h/h2822.md) [ḥāšaḵ](../../strongs/h/h2821.md) not from thee; but the [layil](../../strongs/h/h3915.md) ['owr](../../strongs/h/h215.md) as the [yowm](../../strongs/h/h3117.md): the [ḥăšēḵâ](../../strongs/h/h2825.md) and the ['ôrâ](../../strongs/h/h219.md) are both alike to thee.

<a name="psalms_139_13"></a>Psalms 139:13

For thou hast [qānâ](../../strongs/h/h7069.md) my [kilyah](../../strongs/h/h3629.md): thou hast [cakak](../../strongs/h/h5526.md) me in my ['em](../../strongs/h/h517.md) [beten](../../strongs/h/h990.md).

<a name="psalms_139_14"></a>Psalms 139:14

I will [yadah](../../strongs/h/h3034.md) thee; for I am [yare'](../../strongs/h/h3372.md) and [palah](../../strongs/h/h6395.md): [pala'](../../strongs/h/h6381.md) are thy [ma'aseh](../../strongs/h/h4639.md); and that my [nephesh](../../strongs/h/h5315.md) [yada'](../../strongs/h/h3045.md) right [me'od](../../strongs/h/h3966.md).

<a name="psalms_139_15"></a>Psalms 139:15

My [ʿōṣem](../../strongs/h/h6108.md) was not [kāḥaḏ](../../strongs/h/h3582.md) from thee, when I was ['asah](../../strongs/h/h6213.md) in [cether](../../strongs/h/h5643.md), and curiously [rāqam](../../strongs/h/h7551.md) in the [taḥtî](../../strongs/h/h8482.md) of the ['erets](../../strongs/h/h776.md).

<a name="psalms_139_16"></a>Psalms 139:16

Thine ['ayin](../../strongs/h/h5869.md) did [ra'ah](../../strongs/h/h7200.md) my substance, yet being [gōlem](../../strongs/h/h1564.md); and in thy [sēp̄er](../../strongs/h/h5612.md) all my members were [kāṯaḇ](../../strongs/h/h3789.md), which in [yowm](../../strongs/h/h3117.md) were [yāṣar](../../strongs/h/h3335.md), when as yet there was ['echad](../../strongs/h/h259.md) of them.

<a name="psalms_139_17"></a>Psalms 139:17

How [yāqar](../../strongs/h/h3365.md) also are thy [rēaʿ](../../strongs/h/h7454.md) unto me, ['el](../../strongs/h/h410.md)! how [ʿāṣam](../../strongs/h/h6105.md) is the [ro'sh](../../strongs/h/h7218.md) of them!

<a name="psalms_139_18"></a>Psalms 139:18

If I should [sāp̄ar](../../strongs/h/h5608.md) them, they are more in [rabah](../../strongs/h/h7235.md) than the [ḥôl](../../strongs/h/h2344.md): when I [quwts](../../strongs/h/h6974.md), I am [ʿôḏ](../../strongs/h/h5750.md) with thee.

<a name="psalms_139_19"></a>Psalms 139:19

Surely thou wilt [qāṭal](../../strongs/h/h6991.md) the [rasha'](../../strongs/h/h7563.md), ['ĕlvôha](../../strongs/h/h433.md): [cuwr](../../strongs/h/h5493.md) from me therefore, ye [dam](../../strongs/h/h1818.md) ['enowsh](../../strongs/h/h582.md).

<a name="psalms_139_20"></a>Psalms 139:20

For they ['āmar](../../strongs/h/h559.md) against thee [mezimmah](../../strongs/h/h4209.md), and thine [ʿĀr](../../strongs/h/h6145.md) [nasa'](../../strongs/h/h5375.md) thy name in [shav'](../../strongs/h/h7723.md).

<a name="psalms_139_21"></a>Psalms 139:21

Do not I [sane'](../../strongs/h/h8130.md) them, [Yĕhovah](../../strongs/h/h3068.md), that [sane'](../../strongs/h/h8130.md) thee? and am not I [qûṭ](../../strongs/h/h6962.md) with [tᵊqômēm](../../strongs/h/h8618.md) against thee?

<a name="psalms_139_22"></a>Psalms 139:22

I [sane'](../../strongs/h/h8130.md) them with [taḵlîṯ](../../strongs/h/h8503.md) [śin'â](../../strongs/h/h8135.md): I count them mine ['oyeb](../../strongs/h/h341.md).

<a name="psalms_139_23"></a>Psalms 139:23

[chaqar](../../strongs/h/h2713.md) me, ['el](../../strongs/h/h410.md), and [yada'](../../strongs/h/h3045.md) my [lebab](../../strongs/h/h3824.md): [bachan](../../strongs/h/h974.md) me, and [yada'](../../strongs/h/h3045.md) my [śarʿapîm](../../strongs/h/h8312.md):

<a name="psalms_139_24"></a>Psalms 139:24

And [ra'ah](../../strongs/h/h7200.md) if there be any [ʿōṣeḇ](../../strongs/h/h6090.md) [derek](../../strongs/h/h1870.md) in me, and [nachah](../../strongs/h/h5148.md) me in the [derek](../../strongs/h/h1870.md) ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 138](psalms_138.md) - [Psalms 140](psalms_140.md)