# [Psalms 79](https://www.blueletterbible.org/kjv/psa/79)

<a name="psalms_79_1"></a>Psalms 79:1

A [mizmôr](../../strongs/h/h4210.md) of ['Āsāp̄](../../strongs/h/h623.md). ['Elohiym](../../strongs/h/h430.md), the [gowy](../../strongs/h/h1471.md) are [bow'](../../strongs/h/h935.md) into thine [nachalah](../../strongs/h/h5159.md); thy [qodesh](../../strongs/h/h6944.md) [heykal](../../strongs/h/h1964.md) have they [ṭāmē'](../../strongs/h/h2930.md); they have [śûm](../../strongs/h/h7760.md) [Yĕruwshalaim](../../strongs/h/h3389.md) on [ʿî](../../strongs/h/h5856.md).

<a name="psalms_79_2"></a>Psalms 79:2

The [nᵊḇēlâ](../../strongs/h/h5038.md) of thy ['ebed](../../strongs/h/h5650.md) have they [nathan](../../strongs/h/h5414.md) to be [ma'akal](../../strongs/h/h3978.md) unto the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), the [basar](../../strongs/h/h1320.md) of thy [chaciyd](../../strongs/h/h2623.md) unto the [chay](../../strongs/h/h2416.md) of the ['erets](../../strongs/h/h776.md).

<a name="psalms_79_3"></a>Psalms 79:3

Their [dam](../../strongs/h/h1818.md) have they [šāp̄aḵ](../../strongs/h/h8210.md) like [mayim](../../strongs/h/h4325.md) [cabiyb](../../strongs/h/h5439.md) [Yĕruwshalaim](../../strongs/h/h3389.md); and there was none to [qāḇar](../../strongs/h/h6912.md) them.

<a name="psalms_79_4"></a>Psalms 79:4

We are become a [cherpah](../../strongs/h/h2781.md) to our [šāḵēn](../../strongs/h/h7934.md), a [laʿaḡ](../../strongs/h/h3933.md) and [qeles](../../strongs/h/h7047.md) to them that are [cabiyb](../../strongs/h/h5439.md) us.

<a name="psalms_79_5"></a>Psalms 79:5

How long, [Yĕhovah](../../strongs/h/h3068.md) ? wilt thou be ['anaph](../../strongs/h/h599.md) for [netsach](../../strongs/h/h5331.md) ? shall thy [qin'â](../../strongs/h/h7068.md) [bāʿar](../../strongs/h/h1197.md) like ['esh](../../strongs/h/h784.md) ?

<a name="psalms_79_6"></a>Psalms 79:6

[šāp̄aḵ](../../strongs/h/h8210.md) thy [chemah](../../strongs/h/h2534.md) upon the [gowy](../../strongs/h/h1471.md) that have not [yada'](../../strongs/h/h3045.md) thee, and upon the [mamlāḵâ](../../strongs/h/h4467.md) that have not [qara'](../../strongs/h/h7121.md) upon thy [shem](../../strongs/h/h8034.md).

<a name="psalms_79_7"></a>Psalms 79:7

For they have ['akal](../../strongs/h/h398.md) [Ya'aqob](../../strongs/h/h3290.md), and laid [šāmēm](../../strongs/h/h8074.md) his [nāvê](../../strongs/h/h5116.md).

<a name="psalms_79_8"></a>Psalms 79:8

[zakar](../../strongs/h/h2142.md) not against us [ri'šôn](../../strongs/h/h7223.md) ['avon](../../strongs/h/h5771.md): let thy [raḥam](../../strongs/h/h7356.md) [mahēr](../../strongs/h/h4118.md) [qadam](../../strongs/h/h6923.md) us: for we are [dālal](../../strongs/h/h1809.md) [me'od](../../strongs/h/h3966.md) [dālal](../../strongs/h/h1809.md).

<a name="psalms_79_9"></a>Psalms 79:9

[ʿāzar](../../strongs/h/h5826.md) us, ['Elohiym](../../strongs/h/h430.md) of our [yesha'](../../strongs/h/h3468.md), [dabar](../../strongs/h/h1697.md) the [kabowd](../../strongs/h/h3519.md) of thy [shem](../../strongs/h/h8034.md): and [natsal](../../strongs/h/h5337.md) us, and purge [kāp̄ar](../../strongs/h/h3722.md) our [chatta'ath](../../strongs/h/h2403.md), for thy [shem](../../strongs/h/h8034.md) sake.

<a name="psalms_79_10"></a>Psalms 79:10

Wherefore should the [gowy](../../strongs/h/h1471.md) ['āmar](../../strongs/h/h559.md), Where is their ['Elohiym](../../strongs/h/h430.md) ? let him be [yada'](../../strongs/h/h3045.md) among the [gowy](../../strongs/h/h1471.md) in our ['ayin](../../strongs/h/h5869.md) by the [nᵊqāmâ](../../strongs/h/h5360.md) of the [dam](../../strongs/h/h1818.md) of thy ['ebed](../../strongs/h/h5650.md) which is [šāp̄aḵ](../../strongs/h/h8210.md).

<a name="psalms_79_11"></a>Psalms 79:11

Let the ['ănāqâ](../../strongs/h/h603.md) of the ['āsîr](../../strongs/h/h615.md) [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) thee; according to the [gōḏel](../../strongs/h/h1433.md) of thy [zerowa'](../../strongs/h/h2220.md) [yāṯar](../../strongs/h/h3498.md) thou those that are [ben](../../strongs/h/h1121.md) to [tᵊmûṯâ](../../strongs/h/h8546.md);

<a name="psalms_79_12"></a>Psalms 79:12

And [shuwb](../../strongs/h/h7725.md) unto our [šāḵēn](../../strongs/h/h7934.md) [šiḇʿāṯayim](../../strongs/h/h7659.md) into their [ḥêq](../../strongs/h/h2436.md) their [cherpah](../../strongs/h/h2781.md), wherewith they have [ḥārap̄](../../strongs/h/h2778.md) thee, ['Adonay](../../strongs/h/h136.md).

<a name="psalms_79_13"></a>Psalms 79:13

So we thy ['am](../../strongs/h/h5971.md) and [tso'n](../../strongs/h/h6629.md) of thy [marʿîṯ](../../strongs/h/h4830.md) will [yadah](../../strongs/h/h3034.md) thee for ['owlam](../../strongs/h/h5769.md): we will [sāp̄ar](../../strongs/h/h5608.md) thy [tehillah](../../strongs/h/h8416.md) to [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md).


---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 78](psalms_78.md) - [Psalms 80](psalms_80.md)