# [Psalms 49](https://www.blueletterbible.org/kjv/psa/49)

<a name="psalms_49_1"></a>Psalms 49:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) for the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md).[shama'](../../strongs/h/h8085.md) this, all ye ['am](../../strongs/h/h5971.md); ['azan](../../strongs/h/h238.md), all ye [yashab](../../strongs/h/h3427.md) of the [cheled](../../strongs/h/h2465.md):

<a name="psalms_49_2"></a>Psalms 49:2

Both [ben](../../strongs/h/h1121.md) ['āḏām](../../strongs/h/h120.md) and [ben](../../strongs/h/h1121.md) ['iysh](../../strongs/h/h376.md), [ʿāšîr](../../strongs/h/h6223.md) and ['ebyown](../../strongs/h/h34.md), [yaḥaḏ](../../strongs/h/h3162.md).

<a name="psalms_49_3"></a>Psalms 49:3

My [peh](../../strongs/h/h6310.md) shall [dabar](../../strongs/h/h1696.md) of [ḥāḵmôṯ](../../strongs/h/h2454.md); and the [hāḡûṯ](../../strongs/h/h1900.md) of my [leb](../../strongs/h/h3820.md) shall be of [tāḇûn](../../strongs/h/h8394.md).

<a name="psalms_49_4"></a>Psalms 49:4

I will [natah](../../strongs/h/h5186.md) mine ['ozen](../../strongs/h/h241.md) to a [māšāl](../../strongs/h/h4912.md): I will [pāṯaḥ](../../strongs/h/h6605.md) my [ḥîḏâ](../../strongs/h/h2420.md) upon the [kinnôr](../../strongs/h/h3658.md).

<a name="psalms_49_5"></a>Psalms 49:5

Wherefore should I [yare'](../../strongs/h/h3372.md) in the [yowm](../../strongs/h/h3117.md) of [ra'](../../strongs/h/h7451.md), when the ['avon](../../strongs/h/h5771.md) of my [ʿāqēḇ](../../strongs/h/h6120.md) shall [cabab](../../strongs/h/h5437.md) me about?

<a name="psalms_49_6"></a>Psalms 49:6

They that [batach](../../strongs/h/h982.md) in their [ḥayil](../../strongs/h/h2428.md), and [halal](../../strongs/h/h1984.md) themselves in the [rōḇ](../../strongs/h/h7230.md) of their [ʿōšer](../../strongs/h/h6239.md);

<a name="psalms_49_7"></a>Psalms 49:7

No ['iysh](../../strongs/h/h376.md) can [pāḏâ](../../strongs/h/h6299.md) [pāḏâ](../../strongs/h/h6299.md) his ['ach](../../strongs/h/h251.md), nor [nathan](../../strongs/h/h5414.md) to ['Elohiym](../../strongs/h/h430.md) a [kōp̄er](../../strongs/h/h3724.md) for him:

<a name="psalms_49_8"></a>Psalms 49:8

(For the [piḏyôm](../../strongs/h/h6306.md) of their [nephesh](../../strongs/h/h5315.md) is [yāqar](../../strongs/h/h3365.md), and it [ḥāḏal](../../strongs/h/h2308.md) ['owlam](../../strongs/h/h5769.md):)

<a name="psalms_49_9"></a>Psalms 49:9

That he should still [ḥāyâ](../../strongs/h/h2421.md) [netsach](../../strongs/h/h5331.md), and not [ra'ah](../../strongs/h/h7200.md) [shachath](../../strongs/h/h7845.md).

<a name="psalms_49_10"></a>Psalms 49:10

For he [ra'ah](../../strongs/h/h7200.md) that [ḥāḵām](../../strongs/h/h2450.md) [muwth](../../strongs/h/h4191.md), likewise the [kᵊsîl](../../strongs/h/h3684.md) and the [baʿar](../../strongs/h/h1198.md) ['abad](../../strongs/h/h6.md), and ['azab](../../strongs/h/h5800.md) their [ḥayil](../../strongs/h/h2428.md) to ['aḥēr](../../strongs/h/h312.md).

<a name="psalms_49_11"></a>Psalms 49:11

Their [qereḇ](../../strongs/h/h7130.md) thought is, that their [bayith](../../strongs/h/h1004.md) shall continue ['owlam](../../strongs/h/h5769.md), and their [miškān](../../strongs/h/h4908.md) to [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md); they [qara'](../../strongs/h/h7121.md) their ['ăḏāmâ](../../strongs/h/h127.md) after their own [shem](../../strongs/h/h8034.md).

<a name="psalms_49_12"></a>Psalms 49:12

Nevertheless ['adam](../../strongs/h/h120.md) [yᵊqār](../../strongs/h/h3366.md) [lûn](../../strongs/h/h3885.md) not: he is like the [bĕhemah](../../strongs/h/h929.md) that [damah](../../strongs/h/h1820.md).

<a name="psalms_49_13"></a>Psalms 49:13

This their [derek](../../strongs/h/h1870.md) is their [kesel](../../strongs/h/h3689.md): yet their ['aḥar](../../strongs/h/h310.md) [ratsah](../../strongs/h/h7521.md) their [peh](../../strongs/h/h6310.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_49_14"></a>Psalms 49:14

Like [tso'n](../../strongs/h/h6629.md) they are [šāṯaṯ](../../strongs/h/h8371.md) in the [shĕ'owl](../../strongs/h/h7585.md); [maveth](../../strongs/h/h4194.md) shall [ra'ah](../../strongs/h/h7462.md) on them; and the [yashar](../../strongs/h/h3477.md) shall have [radah](../../strongs/h/h7287.md) over them in the [boqer](../../strongs/h/h1242.md); and their [tsuwr](../../strongs/h/h6697.md) [ṣîr](../../strongs/h/h6736.md) shall [bālâ](../../strongs/h/h1086.md) in the [shĕ'owl](../../strongs/h/h7585.md) from their [zᵊḇûl](../../strongs/h/h2073.md).

<a name="psalms_49_15"></a>Psalms 49:15

But ['Elohiym](../../strongs/h/h430.md) will [pāḏâ](../../strongs/h/h6299.md) my [nephesh](../../strongs/h/h5315.md) from the [yad](../../strongs/h/h3027.md) of the [shĕ'owl](../../strongs/h/h7585.md): for he shall [laqach](../../strongs/h/h3947.md) me. [Celah](../../strongs/h/h5542.md).

<a name="psalms_49_16"></a>Psalms 49:16

Be not thou [yare'](../../strongs/h/h3372.md) when ['iysh](../../strongs/h/h376.md) is made [ʿāšar](../../strongs/h/h6238.md), when the [kabowd](../../strongs/h/h3519.md) of his [bayith](../../strongs/h/h1004.md) is [rabah](../../strongs/h/h7235.md);

<a name="psalms_49_17"></a>Psalms 49:17

For when he [maveth](../../strongs/h/h4194.md) he shall [laqach](../../strongs/h/h3947.md) nothing: his [kabowd](../../strongs/h/h3519.md) shall not [yarad](../../strongs/h/h3381.md) after him.

<a name="psalms_49_18"></a>Psalms 49:18

Though while he [chay](../../strongs/h/h2416.md) he [barak](../../strongs/h/h1288.md) his [nephesh](../../strongs/h/h5315.md): and men will [yadah](../../strongs/h/h3034.md) thee, when thou doest [yatab](../../strongs/h/h3190.md) to thyself.

<a name="psalms_49_19"></a>Psalms 49:19

He shall [bow'](../../strongs/h/h935.md) to the [dôr](../../strongs/h/h1755.md) of his ['ab](../../strongs/h/h1.md); they shall [lō'](../../strongs/h/h3808.md) [netsach](../../strongs/h/h5331.md) [ra'ah](../../strongs/h/h7200.md) ['owr](../../strongs/h/h216.md).

<a name="psalms_49_20"></a>Psalms 49:20

['adam](../../strongs/h/h120.md) that is in [yᵊqār](../../strongs/h/h3366.md), and [bîn](../../strongs/h/h995.md) not, is [māšal](../../strongs/h/h4911.md) the [bĕhemah](../../strongs/h/h929.md) that [damah](../../strongs/h/h1820.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 48](psalms_48.md) - [Psalms 50](psalms_50.md)