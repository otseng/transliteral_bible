# [Psalms 57](https://www.blueletterbible.org/kjv/psa/57)

<a name="psalms_57_1"></a>Psalms 57:1

To the [nāṣaḥ](../../strongs/h/h5329.md), ['Al-tašḥēṯ](../../strongs/h/h516.md), [Miḵtām](../../strongs/h/h4387.md) of [Dāviḏ](../../strongs/h/h1732.md), when he [bāraḥ](../../strongs/h/h1272.md) from [Šā'ûl](../../strongs/h/h7586.md) in the [mᵊʿārâ](../../strongs/h/h4631.md). Be [chanan](../../strongs/h/h2603.md) unto me, ['Elohiym](../../strongs/h/h430.md), be [chanan](../../strongs/h/h2603.md) unto me: for my [nephesh](../../strongs/h/h5315.md) [chacah](../../strongs/h/h2620.md) in thee: yea, in the [ṣēl](../../strongs/h/h6738.md) of thy [kanaph](../../strongs/h/h3671.md) will I [chacah](../../strongs/h/h2620.md), until these [havvah](../../strongs/h/h1942.md) be ['abar](../../strongs/h/h5674.md).

<a name="psalms_57_2"></a>Psalms 57:2

I will [qara'](../../strongs/h/h7121.md) unto ['Elohiym](../../strongs/h/h430.md) ['elyown](../../strongs/h/h5945.md); unto ['el](../../strongs/h/h410.md) that [gāmar](../../strongs/h/h1584.md) all things for me.

<a name="psalms_57_3"></a>Psalms 57:3

He shall [shalach](../../strongs/h/h7971.md) from [shamayim](../../strongs/h/h8064.md), and [yasha'](../../strongs/h/h3467.md) me from the [ḥārap̄](../../strongs/h/h2778.md) of him that would [šā'ap̄](../../strongs/h/h7602.md) me. [Celah](../../strongs/h/h5542.md). ['Elohiym](../../strongs/h/h430.md) shall [shalach](../../strongs/h/h7971.md) his [checed](../../strongs/h/h2617.md) and his ['emeth](../../strongs/h/h571.md).

<a name="psalms_57_4"></a>Psalms 57:4

My [nephesh](../../strongs/h/h5315.md) is [tavek](../../strongs/h/h8432.md) lions: and I [shakab](../../strongs/h/h7901.md) even among them that are [lāhaṭ](../../strongs/h/h3857.md), even the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md), whose [šēn](../../strongs/h/h8127.md) are [ḥănîṯ](../../strongs/h/h2595.md) and [chets](../../strongs/h/h2671.md), and their [lashown](../../strongs/h/h3956.md) a [ḥaḏ](../../strongs/h/h2299.md) [chereb](../../strongs/h/h2719.md).

<a name="psalms_57_5"></a>Psalms 57:5

Be thou [ruwm](../../strongs/h/h7311.md), ['Elohiym](../../strongs/h/h430.md), above the [shamayim](../../strongs/h/h8064.md); let thy [kabowd](../../strongs/h/h3519.md) be above all the ['erets](../../strongs/h/h776.md).

<a name="psalms_57_6"></a>Psalms 57:6

They have [kuwn](../../strongs/h/h3559.md) a [rešeṯ](../../strongs/h/h7568.md) for my [pa'am](../../strongs/h/h6471.md); my [nephesh](../../strongs/h/h5315.md) is [kāp̄ap̄](../../strongs/h/h3721.md): they have [karah](../../strongs/h/h3738.md) a [šîḥâ](../../strongs/h/h7882.md) [paniym](../../strongs/h/h6440.md) me, into the [tavek](../../strongs/h/h8432.md) whereof they are [naphal](../../strongs/h/h5307.md) themselves. [Celah](../../strongs/h/h5542.md).

<a name="psalms_57_7"></a>Psalms 57:7

My [leb](../../strongs/h/h3820.md) is [kuwn](../../strongs/h/h3559.md), ['Elohiym](../../strongs/h/h430.md), my [leb](../../strongs/h/h3820.md) is [kuwn](../../strongs/h/h3559.md): I will [shiyr](../../strongs/h/h7891.md) and [zamar](../../strongs/h/h2167.md).

<a name="psalms_57_8"></a>Psalms 57:8

[ʿûr](../../strongs/h/h5782.md), my [kabowd](../../strongs/h/h3519.md); [ʿûr](../../strongs/h/h5782.md), [neḇel](../../strongs/h/h5035.md) and [kinnôr](../../strongs/h/h3658.md): I myself will [ʿûr](../../strongs/h/h5782.md) [šaḥar](../../strongs/h/h7837.md).

<a name="psalms_57_9"></a>Psalms 57:9

I will [yadah](../../strongs/h/h3034.md) thee, ['adonay](../../strongs/h/h136.md), among the ['am](../../strongs/h/h5971.md): I will [zamar](../../strongs/h/h2167.md) unto thee among the [lĕom](../../strongs/h/h3816.md).

<a name="psalms_57_10"></a>Psalms 57:10

For thy [checed](../../strongs/h/h2617.md) is [gadowl](../../strongs/h/h1419.md) unto the [shamayim](../../strongs/h/h8064.md), and thy ['emeth](../../strongs/h/h571.md) unto the [shachaq](../../strongs/h/h7834.md).

<a name="psalms_57_11"></a>Psalms 57:11

Be thou [ruwm](../../strongs/h/h7311.md), ['Elohiym](../../strongs/h/h430.md), above the [shamayim](../../strongs/h/h8064.md): let thy [kabowd](../../strongs/h/h3519.md) be above all the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 56](psalms_56.md) - [Psalms 58](psalms_58.md)