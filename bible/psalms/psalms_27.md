# [Psalms 27](https://www.blueletterbible.org/kjv/psa/27/1/s_505001)

<a name="psalms_27_1"></a>Psalms 27:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [Yĕhovah](../../strongs/h/h3068.md) is my ['owr](../../strongs/h/h216.md) and my [yesha'](../../strongs/h/h3468.md); whom shall I [yare'](../../strongs/h/h3372.md)? [Yĕhovah](../../strongs/h/h3068.md) is the [māʿôz](../../strongs/h/h4581.md) of my [chay](../../strongs/h/h2416.md); of whom shall I be [pachad](../../strongs/h/h6342.md)?

<a name="psalms_27_2"></a>Psalms 27:2

When the [ra'a'](../../strongs/h/h7489.md), even mine [tsar](../../strongs/h/h6862.md) and my ['oyeb](../../strongs/h/h341.md), came upon me to ['akal](../../strongs/h/h398.md) up my [basar](../../strongs/h/h1320.md), they [kashal](../../strongs/h/h3782.md) and [naphal](../../strongs/h/h5307.md).

<a name="psalms_27_3"></a>Psalms 27:3

Though a [maḥănê](../../strongs/h/h4264.md) should [ḥānâ](../../strongs/h/h2583.md) against me, my [leb](../../strongs/h/h3820.md) shall not [yare'](../../strongs/h/h3372.md): though [milḥāmâ](../../strongs/h/h4421.md) should [quwm](../../strongs/h/h6965.md) against me, in this will I be [batach](../../strongs/h/h982.md).

<a name="psalms_27_4"></a>Psalms 27:4

['echad](../../strongs/h/h259.md) thing have I [sha'al](../../strongs/h/h7592.md) of [Yĕhovah](../../strongs/h/h3068.md), that will I [bāqaš](../../strongs/h/h1245.md) after; that I may [yashab](../../strongs/h/h3427.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) all the [yowm](../../strongs/h/h3117.md) of my [chay](../../strongs/h/h2416.md), to [chazah](../../strongs/h/h2372.md) the [nōʿam](../../strongs/h/h5278.md) of [Yĕhovah](../../strongs/h/h3068.md), and to [bāqar](../../strongs/h/h1239.md) in his [heykal](../../strongs/h/h1964.md).

<a name="psalms_27_5"></a>Psalms 27:5

For in the [yowm](../../strongs/h/h3117.md) of [ra'](../../strongs/h/h7451.md) he shall [tsaphan](../../strongs/h/h6845.md) me in his [sōḵ](../../strongs/h/h5520.md): in the [cether](../../strongs/h/h5643.md) of his ['ohel](../../strongs/h/h168.md) shall he [cathar](../../strongs/h/h5641.md) me; he shall [ruwm](../../strongs/h/h7311.md) me upon a [tsuwr](../../strongs/h/h6697.md).

<a name="psalms_27_6"></a>Psalms 27:6

And now shall mine [ro'sh](../../strongs/h/h7218.md) be [ruwm](../../strongs/h/h7311.md) above mine ['oyeb](../../strongs/h/h341.md) round about me: therefore will I [zabach](../../strongs/h/h2076.md) in his ['ohel](../../strongs/h/h168.md) [zebach](../../strongs/h/h2077.md) of [tᵊrûʿâ](../../strongs/h/h8643.md); I will [shiyr](../../strongs/h/h7891.md), yea, I will [zamar](../../strongs/h/h2167.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_27_7"></a>Psalms 27:7

[shama'](../../strongs/h/h8085.md), [Yĕhovah](../../strongs/h/h3068.md), when I [qara'](../../strongs/h/h7121.md) with my [qowl](../../strongs/h/h6963.md): have [chanan](../../strongs/h/h2603.md) also upon me, and ['anah](../../strongs/h/h6030.md) me.

<a name="psalms_27_8"></a>Psalms 27:8

When thou saidst, [bāqaš](../../strongs/h/h1245.md) ye my [paniym](../../strongs/h/h6440.md); my [leb](../../strongs/h/h3820.md) said unto thee, Thy [paniym](../../strongs/h/h6440.md), [Yĕhovah](../../strongs/h/h3068.md), will I [bāqaš](../../strongs/h/h1245.md).

<a name="psalms_27_9"></a>Psalms 27:9

[cathar](../../strongs/h/h5641.md) not thy [paniym](../../strongs/h/h6440.md) far from me; put not thy ['ebed](../../strongs/h/h5650.md) away in ['aph](../../strongs/h/h639.md): thou hast been my [ʿezrâ](../../strongs/h/h5833.md); [nāṭaš](../../strongs/h/h5203.md) me not, neither ['azab](../../strongs/h/h5800.md) me, O ['Elohiym](../../strongs/h/h430.md) of my [yesha'](../../strongs/h/h3468.md).

<a name="psalms_27_10"></a>Psalms 27:10

When my ['ab](../../strongs/h/h1.md) and my ['em](../../strongs/h/h517.md) ['azab](../../strongs/h/h5800.md) me, then [Yĕhovah](../../strongs/h/h3068.md) will ['āsap̄](../../strongs/h/h622.md) me.

<a name="psalms_27_11"></a>Psalms 27:11

[yārâ](../../strongs/h/h3384.md) me thy [derek](../../strongs/h/h1870.md), [Yĕhovah](../../strongs/h/h3068.md), and [nachah](../../strongs/h/h5148.md) me in a [mîšôr](../../strongs/h/h4334.md) ['orach](../../strongs/h/h734.md), because of mine [sharar](../../strongs/h/h8324.md).

<a name="psalms_27_12"></a>Psalms 27:12

[nathan](../../strongs/h/h5414.md) me not over unto the [nephesh](../../strongs/h/h5315.md) of mine [tsar](../../strongs/h/h6862.md): for [sheqer](../../strongs/h/h8267.md) ['ed](../../strongs/h/h5707.md) are [quwm](../../strongs/h/h6965.md) against me, and such as [yāp̄ēaḥ](../../strongs/h/h3307.md) [chamac](../../strongs/h/h2555.md).

<a name="psalms_27_13"></a>Psalms 27:13

I had fainted, unless I had ['aman](../../strongs/h/h539.md) to [ra'ah](../../strongs/h/h7200.md) the [ṭûḇ](../../strongs/h/h2898.md) of [Yĕhovah](../../strongs/h/h3068.md) in the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md).

<a name="psalms_27_14"></a>Psalms 27:14

[qāvâ](../../strongs/h/h6960.md) on [Yĕhovah](../../strongs/h/h3068.md): be [ḥāzaq](../../strongs/h/h2388.md), and he shall ['amats](../../strongs/h/h553.md) thine [leb](../../strongs/h/h3820.md): [qāvâ](../../strongs/h/h6960.md), I say, on [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 26](psalms_26.md) - [Psalms 28](psalms_28.md)