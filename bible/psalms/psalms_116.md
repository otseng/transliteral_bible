# [Psalms 116](https://www.blueletterbible.org/kjv/psalms/116)

<a name="psalms_116_1"></a>Psalms 116:1

I ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md), because he hath [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md) and my [taḥănûn](../../strongs/h/h8469.md).

<a name="psalms_116_2"></a>Psalms 116:2

Because he hath [natah](../../strongs/h/h5186.md) his ['ozen](../../strongs/h/h241.md) unto me, therefore will I [qara'](../../strongs/h/h7121.md) upon him as long as I [yowm](../../strongs/h/h3117.md).

<a name="psalms_116_3"></a>Psalms 116:3

The [chebel](../../strongs/h/h2256.md) of [maveth](../../strongs/h/h4194.md) ['āp̄ap̄](../../strongs/h/h661.md) me, and the [mēṣar](../../strongs/h/h4712.md) of [shĕ'owl](../../strongs/h/h7585.md) gat [māṣā'](../../strongs/h/h4672.md) upon me: I [māṣā'](../../strongs/h/h4672.md) [tsarah](../../strongs/h/h6869.md) and [yagown](../../strongs/h/h3015.md).

<a name="psalms_116_4"></a>Psalms 116:4

Then [qara'](../../strongs/h/h7121.md) I upon the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md); [Yĕhovah](../../strongs/h/h3068.md), I ['ānnā'](../../strongs/h/h577.md) thee, [mālaṭ](../../strongs/h/h4422.md) my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_116_5"></a>Psalms 116:5

[ḥanwn](../../strongs/h/h2587.md) is [Yĕhovah](../../strongs/h/h3068.md), and [tsaddiyq](../../strongs/h/h6662.md); yea, our ['Elohiym](../../strongs/h/h430.md) is [racham](../../strongs/h/h7355.md).

<a name="psalms_116_6"></a>Psalms 116:6

[Yĕhovah](../../strongs/h/h3068.md) [shamar](../../strongs/h/h8104.md) the [pᵊṯî](../../strongs/h/h6612.md): I was [dālal](../../strongs/h/h1809.md), and he [yasha'](../../strongs/h/h3467.md) me.

<a name="psalms_116_7"></a>Psalms 116:7

[shuwb](../../strongs/h/h7725.md) unto thy [mānôaḥ](../../strongs/h/h4494.md), O my [nephesh](../../strongs/h/h5315.md); for [Yĕhovah](../../strongs/h/h3068.md) hath dealt [gamal](../../strongs/h/h1580.md) with thee.

<a name="psalms_116_8"></a>Psalms 116:8

For thou hast [chalats](../../strongs/h/h2502.md) my [nephesh](../../strongs/h/h5315.md) from [maveth](../../strongs/h/h4194.md), mine ['ayin](../../strongs/h/h5869.md) from [dim'ah](../../strongs/h/h1832.md), and my [regel](../../strongs/h/h7272.md) from [dᵊḥî](../../strongs/h/h1762.md).

<a name="psalms_116_9"></a>Psalms 116:9

I will [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) in the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md).

<a name="psalms_116_10"></a>Psalms 116:10

I ['aman](../../strongs/h/h539.md), therefore have I [dabar](../../strongs/h/h1696.md): I was [me'od](../../strongs/h/h3966.md) [ʿānâ](../../strongs/h/h6031.md):

<a name="psalms_116_11"></a>Psalms 116:11

I ['āmar](../../strongs/h/h559.md) in my [ḥāp̄az](../../strongs/h/h2648.md), All ['āḏām](../../strongs/h/h120.md) are [kāzaḇ](../../strongs/h/h3576.md).

<a name="psalms_116_12"></a>Psalms 116:12

What shall I [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md) for all his [taḡmûl](../../strongs/h/h8408.md) toward me?

<a name="psalms_116_13"></a>Psalms 116:13

I will [nasa'](../../strongs/h/h5375.md) the [kowc](../../strongs/h/h3563.md) of [yĕshuw'ah](../../strongs/h/h3444.md), and [qara'](../../strongs/h/h7121.md) upon the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_116_14"></a>Psalms 116:14

I will [shalam](../../strongs/h/h7999.md) my [neḏer](../../strongs/h/h5088.md) unto [Yĕhovah](../../strongs/h/h3068.md) now in the presence of all his ['am](../../strongs/h/h5971.md).

<a name="psalms_116_15"></a>Psalms 116:15

[yāqār](../../strongs/h/h3368.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) is the [maveth](../../strongs/h/h4194.md) of his [chaciyd](../../strongs/h/h2623.md).

<a name="psalms_116_16"></a>Psalms 116:16

[Yĕhovah](../../strongs/h/h3068.md), ['ānnā'](../../strongs/h/h577.md) I am thy ['ebed](../../strongs/h/h5650.md); I am thy ['ebed](../../strongs/h/h5650.md), and the [ben](../../strongs/h/h1121.md) of thine ['amah](../../strongs/h/h519.md): thou hast [pāṯaḥ](../../strongs/h/h6605.md) my [mowcer](../../strongs/h/h4147.md).

<a name="psalms_116_17"></a>Psalms 116:17

I will [zabach](../../strongs/h/h2076.md) to thee the [zebach](../../strongs/h/h2077.md) of [tôḏâ](../../strongs/h/h8426.md), and will [qara'](../../strongs/h/h7121.md) upon the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_116_18"></a>Psalms 116:18

I will [shalam](../../strongs/h/h7999.md) my [neḏer](../../strongs/h/h5088.md) unto [Yĕhovah](../../strongs/h/h3068.md) now in the presence of all his ['am](../../strongs/h/h5971.md),

<a name="psalms_116_19"></a>Psalms 116:19

In the [ḥāṣēr](../../strongs/h/h2691.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md), in the [tavek](../../strongs/h/h8432.md) of thee, O [Yĕruwshalaim](../../strongs/h/h3389.md). [halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 115](psalms_115.md) - [Psalms 117](psalms_117.md)