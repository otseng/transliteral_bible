# [Psalms 46](https://www.blueletterbible.org/kjv/psa/46)

<a name="psalms_46_1"></a>Psalms 46:1

To the [nāṣaḥ](../../strongs/h/h5329.md) for the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md), A [šîr](../../strongs/h/h7892.md) upon [ʿĂlāmôṯ](../../strongs/h/h5961.md). ['Elohiym](../../strongs/h/h430.md) is our [machaceh](../../strongs/h/h4268.md) and ['oz](../../strongs/h/h5797.md), a [me'od](../../strongs/h/h3966.md) [māṣā'](../../strongs/h/h4672.md) [ʿezrâ](../../strongs/h/h5833.md) in [tsarah](../../strongs/h/h6869.md).

<a name="psalms_46_2"></a>Psalms 46:2

Therefore will not we [yare'](../../strongs/h/h3372.md), though the ['erets](../../strongs/h/h776.md) be [mûr](../../strongs/h/h4171.md), and though the [har](../../strongs/h/h2022.md) be [mowt](../../strongs/h/h4131.md) into the [leb](../../strongs/h/h3820.md) of the [yam](../../strongs/h/h3220.md);

<a name="psalms_46_3"></a>Psalms 46:3

Though the [mayim](../../strongs/h/h4325.md) thereof [hāmâ](../../strongs/h/h1993.md) and be [ḥāmar](../../strongs/h/h2560.md), though the [har](../../strongs/h/h2022.md) [rāʿaš](../../strongs/h/h7493.md) with the [ga'avah](../../strongs/h/h1346.md) thereof. [Celah](../../strongs/h/h5542.md).

<a name="psalms_46_4"></a>Psalms 46:4

There is a [nāhār](../../strongs/h/h5104.md), the [peleḡ](../../strongs/h/h6388.md) whereof shall [samach](../../strongs/h/h8055.md) the [ʿîr](../../strongs/h/h5892.md) of ['Elohiym](../../strongs/h/h430.md), the [qadowsh](../../strongs/h/h6918.md) of the [miškān](../../strongs/h/h4908.md) of the ['elyown](../../strongs/h/h5945.md).

<a name="psalms_46_5"></a>Psalms 46:5

['Elohiym](../../strongs/h/h430.md) is in the [qereḇ](../../strongs/h/h7130.md) of her; she shall not be [mowt](../../strongs/h/h4131.md): ['Elohiym](../../strongs/h/h430.md) shall [ʿāzar](../../strongs/h/h5826.md) her, [panah](../../strongs/h/h6437.md) [boqer](../../strongs/h/h1242.md).

<a name="psalms_46_6"></a>Psalms 46:6

The [gowy](../../strongs/h/h1471.md) [hāmâ](../../strongs/h/h1993.md), the [mamlāḵâ](../../strongs/h/h4467.md) were [mowt](../../strongs/h/h4131.md): he [nathan](../../strongs/h/h5414.md) his [qowl](../../strongs/h/h6963.md), the ['erets](../../strongs/h/h776.md) [mûḡ](../../strongs/h/h4127.md).

<a name="psalms_46_7"></a>Psalms 46:7

[Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is with us; the ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md) is our [misgab](../../strongs/h/h4869.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_46_8"></a>Psalms 46:8

[yālaḵ](../../strongs/h/h3212.md), [chazah](../../strongs/h/h2372.md) the [mip̄ʿāl](../../strongs/h/h4659.md) of [Yĕhovah](../../strongs/h/h3068.md), what [šammâ](../../strongs/h/h8047.md) he hath [śûm](../../strongs/h/h7760.md) in the ['erets](../../strongs/h/h776.md).

<a name="psalms_46_9"></a>Psalms 46:9

He maketh [milḥāmâ](../../strongs/h/h4421.md) to [shabath](../../strongs/h/h7673.md) unto the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md); he [shabar](../../strongs/h/h7665.md) the [qesheth](../../strongs/h/h7198.md), and [qāṣaṣ](../../strongs/h/h7112.md) the [ḥănîṯ](../../strongs/h/h2595.md); he [śārap̄](../../strongs/h/h8313.md) the [ʿăḡālâ](../../strongs/h/h5699.md) in the ['esh](../../strongs/h/h784.md).

<a name="psalms_46_10"></a>Psalms 46:10

Be [rāp̄â](../../strongs/h/h7503.md), and [yada'](../../strongs/h/h3045.md) that I am ['Elohiym](../../strongs/h/h430.md): I will be [ruwm](../../strongs/h/h7311.md) among the [gowy](../../strongs/h/h1471.md), I will be [ruwm](../../strongs/h/h7311.md) in the ['erets](../../strongs/h/h776.md).

<a name="psalms_46_11"></a>Psalms 46:11

[Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is with us; the ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md) is our [misgab](../../strongs/h/h4869.md). [Celah](../../strongs/h/h5542.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 45](psalms_45.md) - [Psalms 47](psalms_47.md)