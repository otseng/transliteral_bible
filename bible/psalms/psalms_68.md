# [Psalms 68](https://www.blueletterbible.org/kjv/psa/68)

<a name="psalms_68_1"></a>Psalms 68:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) or [šîr](../../strongs/h/h7892.md) of [Dāviḏ](../../strongs/h/h1732.md). Let ['Elohiym](../../strongs/h/h430.md) [quwm](../../strongs/h/h6965.md), let his ['oyeb](../../strongs/h/h341.md) be [puwts](../../strongs/h/h6327.md) : let them also that [sane'](../../strongs/h/h8130.md) him [nûs](../../strongs/h/h5127.md) before H6440 him.


<a name="psalms_68_2"></a>Psalms 68:2

As ['ashan](../../strongs/h/h6227.md) is [nāḏap̄](../../strongs/h/h5086.md), so [nāḏap̄](../../strongs/h/h5086.md) them: as [dônāḡ](../../strongs/h/h1749.md) [māsas](../../strongs/h/h4549.md) [paniym](../../strongs/h/h6440.md) the ['esh](../../strongs/h/h784.md), so let the [rasha'](../../strongs/h/h7563.md) ['abad](../../strongs/h/h6.md) at the [paniym](../../strongs/h/h6440.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_68_3"></a>Psalms 68:3

But let the [tsaddiyq](../../strongs/h/h6662.md) be [samach](../../strongs/h/h8055.md), let them ['alats](../../strongs/h/h5970.md) [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md) : yea, let them [simchah](../../strongs/h/h8057.md) [śûś](../../strongs/h/h7797.md).

<a name="psalms_68_4"></a>Psalms 68:4

[shiyr](../../strongs/h/h7891.md) unto ['Elohiym](../../strongs/h/h430.md), [zamar](../../strongs/h/h2167.md) to his [shem](../../strongs/h/h8034.md) : [sālal](../../strongs/h/h5549.md) him that [rāḵaḇ](../../strongs/h/h7392.md) upon the ['arabah](../../strongs/h/h6160.md) by his [shem](../../strongs/h/h8034.md) [Yahh](../../strongs/h/h3050.md), and [ʿālaz](../../strongs/h/h5937.md) [paniym](../../strongs/h/h6440.md) him.

<a name="psalms_68_5"></a>Psalms 68:5

An ['ab](../../strongs/h/h1.md) of the [yathowm](../../strongs/h/h3490.md), and a [dayyān](../../strongs/h/h1781.md) of the ['almānâ](../../strongs/h/h490.md), is ['Elohiym](../../strongs/h/h430.md) in his [qodesh](../../strongs/h/h6944.md) [māʿôn](../../strongs/h/h4583.md).

<a name="psalms_68_6"></a>Psalms 68:6

['Elohiym](../../strongs/h/h430.md) [yashab](../../strongs/h/h3427.md) the [yāḥîḏ](../../strongs/h/h3173.md) in [bayith](../../strongs/h/h1004.md) : he [yāṣā'](../../strongs/h/h3318.md) those which are ['āsîr](../../strongs/h/h615.md) with [kôšārâ](../../strongs/h/h3574.md): but the [sārar](../../strongs/h/h5637.md) [shakan](../../strongs/h/h7931.md) in a [ṣᵊḥîḥâ](../../strongs/h/h6707.md) land.

<a name="psalms_68_7"></a>Psalms 68:7

O ['Elohiym](../../strongs/h/h430.md), when thou [yāṣā'](../../strongs/h/h3318.md) [paniym](../../strongs/h/h6440.md) thy ['am](../../strongs/h/h5971.md), when thou didst [ṣāʿaḏ](../../strongs/h/h6805.md) through the [yᵊšîmôn](../../strongs/h/h3452.md), [Celah](../../strongs/h/h5542.

<a name="psalms_68_8"></a>Psalms 68:8

The ['erets](../../strongs/h/h776.md) [rāʿaš](../../strongs/h/h7493.md), the [shamayim](../../strongs/h/h8064.md) also [nāṭap̄](../../strongs/h/h5197.md) at the [paniym](../../strongs/h/h6440.md) of ['Elohiym](../../strongs/h/h430.md): even [Sînay](../../strongs/h/h5514.md) itself was moved at the [paniym](../../strongs/h/h6440.md) of ['Elohiym](../../strongs/h/h430.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="psalms_68_9"></a>Psalms 68:9

Thou, ['Elohiym](../../strongs/h/h430.md), didst [nûp̄](../../strongs/h/h5130.md) a [nᵊḏāḇâ](../../strongs/h/h5071.md) [gešem](../../strongs/h/h1653.md), whereby thou didst [kuwn](../../strongs/h/h3559.md) thine [nachalah](../../strongs/h/h5159.md), when it was [lā'â](../../strongs/h/h3811.md).

<a name="psalms_68_10"></a>Psalms 68:10

Thy [chay](../../strongs/h/h2416.md) hath [yashab](../../strongs/h/h3427.md) therein: thou, ['Elohiym](../../strongs/h/h430.md), hast [kuwn](../../strongs/h/h3559.md) of thy [towb](../../strongs/h/h2896.md) for the ['aniy](../../strongs/h/h6041.md).

<a name="psalms_68_11"></a>Psalms 68:11

The ['adonay](../../strongs/h/h136.md) [nathan](../../strongs/h/h5414.md) the ['omer](../../strongs/h/h562.md): [rab](../../strongs/h/h7227.md) was the [tsaba'](../../strongs/h/h6635.md) of those that [bāśar](../../strongs/h/h1319.md) it.

<a name="psalms_68_12"></a>Psalms 68:12

[melek](../../strongs/h/h4428.md) of [tsaba'](../../strongs/h/h6635.md) did [nāḏaḏ](../../strongs/h/h5074.md) [nāḏaḏ](../../strongs/h/h5074.md) : and she that [nāvê](../../strongs/h/h5116.md) at [bayith](../../strongs/h/h1004.md) [chalaq](../../strongs/h/h2505.md) the [šālāl](../../strongs/h/h7998.md).


<a name="psalms_68_13"></a>Psalms 68:13

Though ye have [shakab](../../strongs/h/h7901.md) among the [šᵊp̄atayim](../../strongs/h/h8240.md), yet shall ye be as the [kanaph](../../strongs/h/h3671.md) of a [yônâ](../../strongs/h/h3123.md) [ḥāp̄â](../../strongs/h/h2645.md) with [keceph](../../strongs/h/h3701.md), and her ['ēḇrâ](../../strongs/h/h84.md) with [yᵊraqraq](../../strongs/h/h3422.md) [ḥārûṣ](../../strongs/h/h2742.md).

<a name="psalms_68_14"></a>Psalms 68:14

When the [Šaday](../../strongs/h/h7706.md) [pāraś](../../strongs/h/h6566.md) [melek](../../strongs/h/h4428.md) in it, it was white as [šeleḡ](../../strongs/h/h7949.md) in [Ṣalmôn](../../strongs/h/h6756.md)

<a name="psalms_68_15"></a>Psalms 68:15

The [har](../../strongs/h/h2022.md) of ['Elohiym](../../strongs/h/h430.md) is as the [har](../../strongs/h/h2022.md) of [Bāšān](../../strongs/h/h1316.md), a [gaḇnunnîm](../../strongs/h/h1386.md) [har](../../strongs/h/h2022.md) as the [har](../../strongs/h/h2022.md) of [Bāšān](../../strongs/h/h1316.md).

<a name="psalms_68_16"></a>Psalms 68:16

Why [rāṣaḏ](../../strongs/h/h7520.md) ye, ye [gaḇnunnîm](../../strongs/h/h1386.md) [har](../../strongs/h/h2022.md)? this is the [har](../../strongs/h/h2022.md) which ['Elohiym](../../strongs/h/h430.md) [chamad](../../strongs/h/h2530.md) to [yashab](../../strongs/h/h3427.md), yea, the [Yĕhovah](../../strongs/h/h3068.md) will [shakan](../../strongs/h/h7931.md) in it for [netsach](../../strongs/h/h5331.md).

<a name="psalms_68_17"></a>Psalms 68:17

The [reḵeḇ](../../strongs/h/h7393.md) of ['Elohiym](../../strongs/h/h430.md) are twenty [ribô'](../../strongs/h/h7239.md), even ['elep̄](../../strongs/h/h505.md) of [šin'ān](../../strongs/h/h8136.md) : the ['adonay](../../strongs/h/h136.md) is among them, as in [Sînay](../../strongs/h/h5514.md), in the [qodesh](../../strongs/h/h6944.md) place.

<a name="psalms_68_18"></a>Psalms 68:18

Thou hast [ʿālâ](../../strongs/h/h5927.md) on [marowm](../../strongs/h/h4791.md), thou hast led [šᵊḇî](../../strongs/h/h7628.md) [šāḇâ](../../strongs/h/h7617.md) : thou hast [laqach](../../strongs/h/h3947.md) [matānâ](../../strongs/h/h4979.md) for ['āḏām](../../strongs/h/h120.md), yea, for the [sārar](../../strongs/h/h5637.md) also, that the [Yahh](../../strongs/h/h3050.md) ['Elohiym](../../strongs/h/h430.md) might [shakan](../../strongs/h/h7931.md) among them.

<a name="psalms_68_19"></a>Psalms 68:19

[barak](../../strongs/h/h1288.md) be the ['adonay](../../strongs/h/h136.md), who [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) [ʿāmas](../../strongs/h/h6006.md) us with benefits, even the ['el](../../strongs/h/h410.md) of our [yĕshuw'ah](../../strongs/h/h3444.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_68_20"></a>Psalms 68:20

He that is our ['el](../../strongs/h/h410.md) is the ['el](../../strongs/h/h410.md) of [môšāʿâ](../../strongs/h/h4190.md), and unto [Yᵊhōvâ](../../strongs/h/h3069.md) the ['adonay](../../strongs/h/h136.md) belong the [tôṣā'ôṯ](../../strongs/h/h8444.md) from [maveth](../../strongs/h/h4194.md).

<a name="psalms_68_21"></a>Psalms 68:21

But ['Elohiym](../../strongs/h/h430.md) shall [māḥaṣ](../../strongs/h/h4272.md) the [ro'sh](../../strongs/h/h7218.md) of his ['oyeb](../../strongs/h/h341.md), and the [śēʿār](../../strongs/h/h8181.md) [qodqod](../../strongs/h/h6936.md) of such an one as goeth on [halak](../../strongs/h/h1980.md) in his ['āšām](../../strongs/h/h817.md).

<a name="psalms_68_22"></a>Psalms 68:22

The ['adonay](../../strongs/h/h136.md) ['āmar](../../strongs/h/h559.md), I will bring [shuwb](../../strongs/h/h7725.md) from [Bāšān](../../strongs/h/h1316.md), I will [shuwb](../../strongs/h/h7725.md) my people from the [mᵊṣôlâ](../../strongs/h/h4688.md) of the [yam](../../strongs/h/h3220.md):

<a name="psalms_68_23"></a>Psalms 68:23

That thy [regel](../../strongs/h/h7272.md) may be [māḥaṣ](../../strongs/h/h4272.md) in the [dam](../../strongs/h/h1818.md) of thine ['oyeb](../../strongs/h/h341.md), and the [lashown](../../strongs/h/h3956.md) of thy [keleḇ](../../strongs/h/h3611.md) in the same.


<a name="psalms_68_24"></a>Psalms 68:24

They have [ra'ah](../../strongs/h/h7200.md) thy [hălîḵâ](../../strongs/h/h1979.md), ['Elohiym](../../strongs/h/h430.md); even the [hălîḵâ](../../strongs/h/h1979.md) of my ['el](../../strongs/h/h410.md), my [melek](../../strongs/h/h4428.md), in the [qodesh](../../strongs/h/h6944.md)

<a name="psalms_68_25"></a>Psalms 68:25

The [shiyr](../../strongs/h/h7891.md) went [qadam](../../strongs/h/h6923.md), the [nāḡan](../../strongs/h/h5059.md) ['aḥar](../../strongs/h/h310.md); [tavek](../../strongs/h/h8432.md) them were the [ʿalmâ](../../strongs/h/h5959.md) [tāp̄ap̄](../../strongs/h/h8608.md).

<a name="psalms_68_26"></a>Psalms 68:26

[barak](../../strongs/h/h1288.md) ye ['Elohiym](../../strongs/h/h430.md) in the [maqhēl](../../strongs/h/h4721.md), even the ['adonay](../../strongs/h/h136.md), from the [māqôr](../../strongs/h/h4726.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="psalms_68_27"></a>Psalms 68:27

There is [ṣāʿîr](../../strongs/h/h6810.md) [Binyāmîn](../../strongs/h/h1144.md) with their [radah](../../strongs/h/h7287.md), the [śar](../../strongs/h/h8269.md) of [Yehuwdah](../../strongs/h/h3063.md) and their [riḡmâ](../../strongs/h/h7277.md), the [śar](../../strongs/h/h8269.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md), and the [śar](../../strongs/h/h8269.md) of [Nap̄tālî](../../strongs/h/h5321.md).


<a name="psalms_68_28"></a>Psalms 68:28

Thy ['Elohiym](../../strongs/h/h430.md) hath [tsavah](../../strongs/h/h6680.md) thy ['oz](../../strongs/h/h5797.md): ['azaz](../../strongs/h/h5810.md), ['Elohiym](../../strongs/h/h430.md), that [zû](../../strongs/h/h2098.md) thou hast [pa'al](../../strongs/h/h6466.md) for us.

<a name="psalms_68_29"></a>Psalms 68:29

Because of thy [heykal](../../strongs/h/h1964.md) at [Yĕruwshalaim](../../strongs/h/h3389.md) shall [melek](../../strongs/h/h4428.md) [yāḇal](../../strongs/h/h2986.md) [šay](../../strongs/h/h7862.md) unto thee.

<a name="psalms_68_30"></a>Psalms 68:30

[gāʿar](../../strongs/h/h1605.md) the [chay](../../strongs/h/h2416.md) of [qānê](../../strongs/h/h7070.md), the ['edah](../../strongs/h/h5712.md) of the ['abîr](../../strongs/h/h47.md), with the [ʿēḡel](../../strongs/h/h5695.md) of the ['am](../../strongs/h/h5971.md), till every one [rāp̄aś](../../strongs/h/h7511.md) himself with [raṣ](../../strongs/h/h7518.md) of [keceph](../../strongs/h/h3701.md): [bāzar](../../strongs/h/h967.md) thou the ['am](../../strongs/h/h5971.md) that [ḥāp̄ēṣ](../../strongs/h/h2654.md) in [qᵊrāḇ](../../strongs/h/h7128.md).

<a name="psalms_68_31"></a>Psalms 68:31

[ḥašman](../../strongs/h/h2831.md) shall ['āṯâ](../../strongs/h/h857.md) of [Mitsrayim](../../strongs/h/h4714.md); [Kûš](../../strongs/h/h3568.md) shall soon [rûṣ](../../strongs/h/h7323.md) her [yad](../../strongs/h/h3027.md) unto ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_68_32"></a>Psalms 68:32

[shiyr](../../strongs/h/h7891.md) unto ['Elohiym](../../strongs/h/h430.md), ye [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md); [zamar](../../strongs/h/h2167.md) unto the ['adonay](../../strongs/h/h136.md); [Celah](../../strongs/h/h5542.md):


<a name="psalms_68_33"></a>Psalms 68:33

To him that [rāḵaḇ](../../strongs/h/h7392.md) upon the [shamayim](../../strongs/h/h8064.md) of [shamayim](../../strongs/h/h8064.md), which were of [qeḏem](../../strongs/h/h6924.md); lo, he doth [nathan](../../strongs/h/h5414.md) his [qowl](../../strongs/h/h6963.md), and that an ['oz](../../strongs/h/h5797.md) [qowl](../../strongs/h/h6963.md).

<a name="psalms_68_34"></a>Psalms 68:34

[nathan](../../strongs/h/h5414.md) ye ['oz](../../strongs/h/h5797.md) unto ['Elohiym](../../strongs/h/h430.md): his [ga'avah](../../strongs/h/h1346.md) is over [Yisra'el](../../strongs/h/h3478.md), and his ['oz](../../strongs/h/h5797.md) is in the [shachaq](../../strongs/h/h7834.md).

<a name="psalms_68_35"></a>Psalms 68:35

['Elohiym](../../strongs/h/h430.md), thou art [yare'](../../strongs/h/h3372.md) out of thy [miqdash](../../strongs/h/h4720.md): the ['el](../../strongs/h/h410.md) of [Yisra'el](../../strongs/h/h3478.md) is he that [nathan](../../strongs/h/h5414.md) ['oz](../../strongs/h/h5797.md) and [taʿăṣumôṯ](../../strongs/h/h8592.md) unto his ['am](../../strongs/h/h5971.md). [barak](../../strongs/h/h1288.md) be ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 67](psalms_67.md) - [Psalms 69](psalms_69.md)