# [Psalms 92](https://www.blueletterbible.org/kjv/psalms/92)

<a name="psalms_92_1"></a>Psalms 92:1

A [mizmôr](../../strongs/h/h4210.md) or [šîr](../../strongs/h/h7892.md) for the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md). It is a [towb](../../strongs/h/h2896.md) thing to [yadah](../../strongs/h/h3034.md) unto [Yĕhovah](../../strongs/h/h3068.md), and to [zamar](../../strongs/h/h2167.md) unto thy [shem](../../strongs/h/h8034.md), O ['elyown](../../strongs/h/h5945.md):

<a name="psalms_92_2"></a>Psalms 92:2

To [nāḡaḏ](../../strongs/h/h5046.md) thy [checed](../../strongs/h/h2617.md) in the [boqer](../../strongs/h/h1242.md), and thy ['ĕmûnâ](../../strongs/h/h530.md) every [layil](../../strongs/h/h3915.md),

<a name="psalms_92_3"></a>Psalms 92:3

Upon an [ʿāśôr](../../strongs/h/h6218.md), and upon the [neḇel](../../strongs/h/h5035.md); upon the [kinnôr](../../strongs/h/h3658.md) with a [higgayown](../../strongs/h/h1902.md).

<a name="psalms_92_4"></a>Psalms 92:4

For thou, [Yĕhovah](../../strongs/h/h3068.md), hast made me [samach](../../strongs/h/h8055.md) through thy [pōʿal](../../strongs/h/h6467.md): I will [ranan](../../strongs/h/h7442.md) in the [ma'aseh](../../strongs/h/h4639.md) of thy [yad](../../strongs/h/h3027.md).

<a name="psalms_92_5"></a>Psalms 92:5

[Yĕhovah](../../strongs/h/h3068.md), how [gāḏal](../../strongs/h/h1431.md) are thy [ma'aseh](../../strongs/h/h4639.md)! and thy [maḥăšāḇâ](../../strongs/h/h4284.md) are [me'od](../../strongs/h/h3966.md) [ʿāmaq](../../strongs/h/h6009.md).

<a name="psalms_92_6"></a>Psalms 92:6

A [baʿar](../../strongs/h/h1198.md) ['iysh](../../strongs/h/h376.md) [yada'](../../strongs/h/h3045.md) not; neither doth a [kᵊsîl](../../strongs/h/h3684.md) [bîn](../../strongs/h/h995.md) this.

<a name="psalms_92_7"></a>Psalms 92:7

When the [rasha'](../../strongs/h/h7563.md) [pāraḥ](../../strongs/h/h6524.md) as the ['eseb](../../strongs/h/h6212.md), and when all the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md) do [tsuwts](../../strongs/h/h6692.md); it is that they shall be [šāmaḏ](../../strongs/h/h8045.md) for [ʿaḏ](../../strongs/h/h5703.md):

<a name="psalms_92_8"></a>Psalms 92:8

But thou, [Yĕhovah](../../strongs/h/h3068.md), art [marowm](../../strongs/h/h4791.md) ['owlam](../../strongs/h/h5769.md).

<a name="psalms_92_9"></a>Psalms 92:9

For, lo, thine ['oyeb](../../strongs/h/h341.md), [Yĕhovah](../../strongs/h/h3068.md), for, lo, thine ['oyeb](../../strongs/h/h341.md) shall ['abad](../../strongs/h/h6.md); all the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md) shall be [pāraḏ](../../strongs/h/h6504.md).

<a name="psalms_92_10"></a>Psalms 92:10

But my [qeren](../../strongs/h/h7161.md) shalt thou [ruwm](../../strongs/h/h7311.md) like the horn of a [rᵊ'ēm](../../strongs/h/h7214.md): I shall be [bālal](../../strongs/h/h1101.md) with [raʿănān](../../strongs/h/h7488.md) [šemen](../../strongs/h/h8081.md).

<a name="psalms_92_11"></a>Psalms 92:11

Mine ['ayin](../../strongs/h/h5869.md) also shall [nabat](../../strongs/h/h5027.md) my desire on mine [šûr](../../strongs/h/h7790.md), and mine ['ozen](../../strongs/h/h241.md) shall [shama'](../../strongs/h/h8085.md) my desire of the [ra'a'](../../strongs/h/h7489.md) that [quwm](../../strongs/h/h6965.md) against me.

<a name="psalms_92_12"></a>Psalms 92:12

The [tsaddiyq](../../strongs/h/h6662.md) shall [pāraḥ](../../strongs/h/h6524.md) like the [tāmār](../../strongs/h/h8558.md): he shall [śāḡâ](../../strongs/h/h7685.md) like an ['erez](../../strongs/h/h730.md) in [Lᵊḇānôn](../../strongs/h/h3844.md).

<a name="psalms_92_13"></a>Psalms 92:13

Those that be [šāṯal](../../strongs/h/h8362.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) shall [pāraḥ](../../strongs/h/h6524.md) in the [ḥāṣēr](../../strongs/h/h2691.md) of our ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_92_14"></a>Psalms 92:14

They shall [nûḇ](../../strongs/h/h5107.md) in [śêḇâ](../../strongs/h/h7872.md); they shall be [dāšēn](../../strongs/h/h1879.md) and [raʿănān](../../strongs/h/h7488.md);

<a name="psalms_92_15"></a>Psalms 92:15

To [nāḡaḏ](../../strongs/h/h5046.md) that [Yĕhovah](../../strongs/h/h3068.md) is [yashar](../../strongs/h/h3477.md): he is my [tsuwr](../../strongs/h/h6697.md), and there is no ['evel](../../strongs/h/h5766.md) ['evel](../../strongs/h/h5766.md) in him.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 91](psalms_91.md) - [Psalms 93](psalms_93.md)