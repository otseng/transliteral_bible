# [Psalms 127](https://www.blueletterbible.org/kjv/psalms/127)

<a name="psalms_127_1"></a>Psalms 127:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md) for [Šᵊlōmô](../../strongs/h/h8010.md). Except [Yĕhovah](../../strongs/h/h3068.md) [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md), they [ʿāmal](../../strongs/h/h5998.md) in [shav'](../../strongs/h/h7723.md) that [bānâ](../../strongs/h/h1129.md) it: except [Yĕhovah](../../strongs/h/h3068.md) [shamar](../../strongs/h/h8104.md) the [ʿîr](../../strongs/h/h5892.md), the [shamar](../../strongs/h/h8104.md) [šāqaḏ](../../strongs/h/h8245.md) but in [shav'](../../strongs/h/h7723.md).

<a name="psalms_127_2"></a>Psalms 127:2

It is [shav'](../../strongs/h/h7723.md) for you to [quwm](../../strongs/h/h6965.md) [šāḵam](../../strongs/h/h7925.md), to sit [yashab](../../strongs/h/h3427.md) ['āḥar](../../strongs/h/h309.md), to ['akal](../../strongs/h/h398.md) the [lechem](../../strongs/h/h3899.md) of ['etseb](../../strongs/h/h6089.md): for so he [nathan](../../strongs/h/h5414.md) his [yāḏîḏ](../../strongs/h/h3039.md) [šēnā'](../../strongs/h/h8142.md).

<a name="psalms_127_3"></a>Psalms 127:3

Lo, [ben](../../strongs/h/h1121.md) are a [nachalah](../../strongs/h/h5159.md) of [Yĕhovah](../../strongs/h/h3068.md): and the [pĕriy](../../strongs/h/h6529.md) of the [beten](../../strongs/h/h990.md) is his [śāḵār](../../strongs/h/h7939.md).

<a name="psalms_127_4"></a>Psalms 127:4

As [chets](../../strongs/h/h2671.md) are in the [yad](../../strongs/h/h3027.md) of a [gibôr](../../strongs/h/h1368.md); so are [ben](../../strongs/h/h1121.md) of the [nāʿur](../../strongs/h/h5271.md).

<a name="psalms_127_5"></a>Psalms 127:5

['esher](../../strongs/h/h835.md) is the [geḇer](../../strongs/h/h1397.md) that hath his ['ašpâ](../../strongs/h/h827.md) [mālā'](../../strongs/h/h4390.md) of them: they shall not be [buwsh](../../strongs/h/h954.md), but they shall [dabar](../../strongs/h/h1696.md) with the ['oyeb](../../strongs/h/h341.md) in the [sha'ar](../../strongs/h/h8179.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 126](psalms_126.md) - [Psalms 128](psalms_128.md)