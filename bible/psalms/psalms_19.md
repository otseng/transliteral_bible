# [Psalms 19](https://www.blueletterbible.org/kjv/psa/19/1/s_497001)

<a name="psalms_19_1"></a>Psalms 19:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). The [shamayim](../../strongs/h/h8064.md) [sāp̄ar](../../strongs/h/h5608.md) the [kabowd](../../strongs/h/h3519.md) of ['el](../../strongs/h/h410.md); and the [raqiya'](../../strongs/h/h7549.md) [nāḡaḏ](../../strongs/h/h5046.md) his [ma'aseh](../../strongs/h/h4639.md).

<a name="psalms_19_2"></a>Psalms 19:2

[yowm](../../strongs/h/h3117.md) unto [yowm](../../strongs/h/h3117.md) [nāḇaʿ](../../strongs/h/h5042.md) ['omer](../../strongs/h/h562.md), and [layil](../../strongs/h/h3915.md) unto [layil](../../strongs/h/h3915.md) [ḥāvâ](../../strongs/h/h2331.md) [da'ath](../../strongs/h/h1847.md).

<a name="psalms_19_3"></a>Psalms 19:3

no ['omer](../../strongs/h/h562.md) nor [dabar](../../strongs/h/h1697.md), their [qowl](../../strongs/h/h6963.md) is not [shama'](../../strongs/h/h8085.md).

<a name="psalms_19_4"></a>Psalms 19:4

Their [qāv](../../strongs/h/h6957.md) is [yāṣā'](../../strongs/h/h3318.md) through all the ['erets](../../strongs/h/h776.md), and their [millâ](../../strongs/h/h4405.md) to the [qāṣê](../../strongs/h/h7097.md) of the [tebel](../../strongs/h/h8398.md). In them hath he [śûm](../../strongs/h/h7760.md) an ['ohel](../../strongs/h/h168.md) for the [šemeš](../../strongs/h/h8121.md),

<a name="psalms_19_5"></a>Psalms 19:5

Which as a [ḥāṯān](../../strongs/h/h2860.md) [yāṣā'](../../strongs/h/h3318.md) of his [ḥupâ](../../strongs/h/h2646.md), and [śûś](../../strongs/h/h7797.md) as a [gibôr](../../strongs/h/h1368.md) to [rûṣ](../../strongs/h/h7323.md) a ['orach](../../strongs/h/h734.md).

<a name="psalms_19_6"></a>Psalms 19:6

His [môṣā'](../../strongs/h/h4161.md) from the [qāṣê](../../strongs/h/h7097.md) of the [shamayim](../../strongs/h/h8064.md), and his [tᵊqûp̄â](../../strongs/h/h8622.md) unto the [qāṣâ](../../strongs/h/h7098.md) of it: and there is nothing [cathar](../../strongs/h/h5641.md) from the [ḥammâ](../../strongs/h/h2535.md) thereof.

<a name="psalms_19_7"></a>Psalms 19:7

The [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md) [tamiym](../../strongs/h/h8549.md), [shuwb](../../strongs/h/h7725.md) the [nephesh](../../strongs/h/h5315.md): the [ʿēḏûṯ](../../strongs/h/h5715.md) of [Yĕhovah](../../strongs/h/h3068.md) ['aman](../../strongs/h/h539.md), [ḥāḵam](../../strongs/h/h2449.md) the [pᵊṯî](../../strongs/h/h6612.md).

<a name="psalms_19_8"></a>Psalms 19:8

The [piqquwd](../../strongs/h/h6490.md) of [Yĕhovah](../../strongs/h/h3068.md) [yashar](../../strongs/h/h3477.md), [samach](../../strongs/h/h8055.md) the [leb](../../strongs/h/h3820.md): the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) [bar](../../strongs/h/h1249.md), ['owr](../../strongs/h/h215.md) the ['ayin](../../strongs/h/h5869.md).

<a name="psalms_19_9"></a>Psalms 19:9

The [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) [tahowr](../../strongs/h/h2889.md), ['amad](../../strongs/h/h5975.md) [ʿaḏ](../../strongs/h/h5703.md): the [mishpat](../../strongs/h/h4941.md) of [Yĕhovah](../../strongs/h/h3068.md) ['emeth](../../strongs/h/h571.md) [ṣāḏaq](../../strongs/h/h6663.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="psalms_19_10"></a>Psalms 19:10

More to be [chamad](../../strongs/h/h2530.md) than [zāhāḇ](../../strongs/h/h2091.md), yea, than much [pāz](../../strongs/h/h6337.md): [māṯôq](../../strongs/h/h4966.md) also than [dĕbash](../../strongs/h/h1706.md) and the [nōp̄eṯ](../../strongs/h/h5317.md) [ṣûp̄](../../strongs/h/h6688.md).

<a name="psalms_19_11"></a>Psalms 19:11

Moreover by them is thy ['ebed](../../strongs/h/h5650.md) [zāhar](../../strongs/h/h2094.md): in [shamar](../../strongs/h/h8104.md) of them [rab](../../strongs/h/h7227.md) [ʿēqeḇ](../../strongs/h/h6118.md).

<a name="psalms_19_12"></a>Psalms 19:12

Who can [bîn](../../strongs/h/h995.md) [šᵊḡî'â](../../strongs/h/h7691.md)? [naqah](../../strongs/h/h5352.md) thou me from [cathar](../../strongs/h/h5641.md).

<a name="psalms_19_13"></a>Psalms 19:13

[ḥāśaḵ](../../strongs/h/h2820.md) thy ['ebed](../../strongs/h/h5650.md) also from [zed](../../strongs/h/h2086.md); let them not have [mashal](../../strongs/h/h4910.md) over me: then shall I be [tamam](../../strongs/h/h8552.md), and I shall be [naqah](../../strongs/h/h5352.md) from the [rab](../../strongs/h/h7227.md) [pesha'](../../strongs/h/h6588.md).

<a name="psalms_19_14"></a>Psalms 19:14

Let the ['emer](../../strongs/h/h561.md) of my [peh](../../strongs/h/h6310.md), and the [higgayown](../../strongs/h/h1902.md) of my [leb](../../strongs/h/h3820.md), be [ratsown](../../strongs/h/h7522.md) in thy [paniym](../../strongs/h/h6440.md), [Yĕhovah](../../strongs/h/h3068.md), my [tsuwr](../../strongs/h/h6697.md), and my [gā'al](../../strongs/h/h1350.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 18](psalms_18.md) - [Psalms 20](psalms_20.md)