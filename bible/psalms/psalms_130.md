# [Psalms 130](https://www.blueletterbible.org/kjv/psalms/130)

<a name="psalms_130_1"></a>Psalms 130:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md). Out of the [maʿămaqqîm](../../strongs/h/h4615.md) have I [qara'](../../strongs/h/h7121.md) unto thee, [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_130_2"></a>Psalms 130:2

['adonay](../../strongs/h/h136.md), [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md): let thine ['ozen](../../strongs/h/h241.md) be [qaššāḇ](../../strongs/h/h7183.md) to the [qowl](../../strongs/h/h6963.md) of my [taḥănûn](../../strongs/h/h8469.md).

<a name="psalms_130_3"></a>Psalms 130:3

If thou, [Yahh](../../strongs/h/h3050.md), shouldest [shamar](../../strongs/h/h8104.md) ['avon](../../strongs/h/h5771.md), ['Adonay](../../strongs/h/h136.md), who shall ['amad](../../strongs/h/h5975.md)?

<a name="psalms_130_4"></a>Psalms 130:4

But there is [sᵊlîḥâ](../../strongs/h/h5547.md) with thee, that thou mayest be [yare'](../../strongs/h/h3372.md).

<a name="psalms_130_5"></a>Psalms 130:5

I [qāvâ](../../strongs/h/h6960.md) for [Yĕhovah](../../strongs/h/h3068.md), my [nephesh](../../strongs/h/h5315.md) doth [qāvâ](../../strongs/h/h6960.md), and in his [dabar](../../strongs/h/h1697.md) do I [yāḥal](../../strongs/h/h3176.md).

<a name="psalms_130_6"></a>Psalms 130:6

My [nephesh](../../strongs/h/h5315.md) waiteth for the ['adonay](../../strongs/h/h136.md) more than they that [shamar](../../strongs/h/h8104.md) for the [boqer](../../strongs/h/h1242.md): I say, more than they that [shamar](../../strongs/h/h8104.md) for the [boqer](../../strongs/h/h1242.md).

<a name="psalms_130_7"></a>Psalms 130:7

Let [Yisra'el](../../strongs/h/h3478.md) [yāḥal](../../strongs/h/h3176.md) in [Yĕhovah](../../strongs/h/h3068.md): for with [Yĕhovah](../../strongs/h/h3068.md) there is [checed](../../strongs/h/h2617.md), and with him is [rabah](../../strongs/h/h7235.md) [pᵊḏûṯ](../../strongs/h/h6304.md).

<a name="psalms_130_8"></a>Psalms 130:8

And he shall [pāḏâ](../../strongs/h/h6299.md) [Yisra'el](../../strongs/h/h3478.md) from all his ['avon](../../strongs/h/h5771.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 129](psalms_129.md) - [Psalms 131](psalms_131.md)