# [Psalms 132](https://www.blueletterbible.org/kjv/psalms/132)

<a name="psalms_132_1"></a>Psalms 132:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md). [Yĕhovah](../../strongs/h/h3068.md), [zakar](../../strongs/h/h2142.md) [Dāviḏ](../../strongs/h/h1732.md), and all his [ʿānâ](../../strongs/h/h6031.md):

<a name="psalms_132_2"></a>Psalms 132:2

How he [shaba'](../../strongs/h/h7650.md) unto [Yĕhovah](../../strongs/h/h3068.md), and [nāḏar](../../strongs/h/h5087.md) unto the ['abiyr](../../strongs/h/h46.md) of [Ya'aqob](../../strongs/h/h3290.md);

<a name="psalms_132_3"></a>Psalms 132:3

Surely I will not [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md) of my [bayith](../../strongs/h/h1004.md), nor [ʿālâ](../../strongs/h/h5927.md) into my ['eres](../../strongs/h/h6210.md) [yāṣûaʿ](../../strongs/h/h3326.md);

<a name="psalms_132_4"></a>Psalms 132:4

I will not [nathan](../../strongs/h/h5414.md) [šᵊnāṯ](../../strongs/h/h8153.md) to mine ['ayin](../../strongs/h/h5869.md), or [tᵊnûmâ](../../strongs/h/h8572.md) to mine ['aph'aph](../../strongs/h/h6079.md),

<a name="psalms_132_5"></a>Psalms 132:5

Until I [māṣā'](../../strongs/h/h4672.md) a [maqowm](../../strongs/h/h4725.md) for [Yĕhovah](../../strongs/h/h3068.md), a [miškān](../../strongs/h/h4908.md) for the ['abiyr](../../strongs/h/h46.md) of [Ya'aqob](../../strongs/h/h3290.md).

<a name="psalms_132_6"></a>Psalms 132:6

Lo, we [shama'](../../strongs/h/h8085.md) of it at ['Ep̄rāṯ](../../strongs/h/h672.md): we [māṣā'](../../strongs/h/h4672.md) it in the [sadeh](../../strongs/h/h7704.md) of the [yaʿar](../../strongs/h/h3293.md).

<a name="psalms_132_7"></a>Psalms 132:7

We will [bow'](../../strongs/h/h935.md) into his [miškān](../../strongs/h/h4908.md): we will [shachah](../../strongs/h/h7812.md) at his [hăḏōm](../../strongs/h/h1916.md) [regel](../../strongs/h/h7272.md).

<a name="psalms_132_8"></a>Psalms 132:8

[quwm](../../strongs/h/h6965.md), [Yĕhovah](../../strongs/h/h3068.md), into thy [mᵊnûḥâ](../../strongs/h/h4496.md); thou, and the ['ārôn](../../strongs/h/h727.md) of thy ['oz](../../strongs/h/h5797.md).

<a name="psalms_132_9"></a>Psalms 132:9

Let thy [kōhēn](../../strongs/h/h3548.md) be [labash](../../strongs/h/h3847.md) with [tsedeq](../../strongs/h/h6664.md); and let thy [chaciyd](../../strongs/h/h2623.md) [ranan](../../strongs/h/h7442.md).

<a name="psalms_132_10"></a>Psalms 132:10

For thy ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) sake [shuwb](../../strongs/h/h7725.md) not the [paniym](../../strongs/h/h6440.md) of thine [mashiyach](../../strongs/h/h4899.md).

<a name="psalms_132_11"></a>Psalms 132:11

[Yĕhovah](../../strongs/h/h3068.md) hath [shaba'](../../strongs/h/h7650.md) in ['emeth](../../strongs/h/h571.md) unto [Dāviḏ](../../strongs/h/h1732.md); he will not [shuwb](../../strongs/h/h7725.md) from it; Of the [pĕriy](../../strongs/h/h6529.md) of thy [beten](../../strongs/h/h990.md) will I [shiyth](../../strongs/h/h7896.md) upon thy [kicce'](../../strongs/h/h3678.md).

<a name="psalms_132_12"></a>Psalms 132:12

If thy [ben](../../strongs/h/h1121.md) will [shamar](../../strongs/h/h8104.md) my [bĕriyth](../../strongs/h/h1285.md) and my [ʿēḏâ](../../strongs/h/h5713.md) [zô](../../strongs/h/h2090.md) [zô](../../strongs/h/h2097.md) I shall [lamad](../../strongs/h/h3925.md) them, their [ben](../../strongs/h/h1121.md) shall also [yashab](../../strongs/h/h3427.md) upon thy [kicce'](../../strongs/h/h3678.md) for [ʿaḏ](../../strongs/h/h5703.md).

<a name="psalms_132_13"></a>Psalms 132:13

For [Yĕhovah](../../strongs/h/h3068.md) hath [bāḥar](../../strongs/h/h977.md) [Tsiyown](../../strongs/h/h6726.md); he hath ['āvâ](../../strongs/h/h183.md) it for his [môšāḇ](../../strongs/h/h4186.md).

<a name="psalms_132_14"></a>Psalms 132:14

This is my [mᵊnûḥâ](../../strongs/h/h4496.md) for [ʿaḏ](../../strongs/h/h5703.md): here will I [yashab](../../strongs/h/h3427.md); for I have ['āvâ](../../strongs/h/h183.md) it.

<a name="psalms_132_15"></a>Psalms 132:15

I will [barak](../../strongs/h/h1288.md) [barak](../../strongs/h/h1288.md) her [ṣayiḏ](../../strongs/h/h6718.md): I will [sāׂbaʿ](../../strongs/h/h7646.md) her ['ebyown](../../strongs/h/h34.md) with [lechem](../../strongs/h/h3899.md).

<a name="psalms_132_16"></a>Psalms 132:16

I will also [labash](../../strongs/h/h3847.md) her [kōhēn](../../strongs/h/h3548.md) with [yesha'](../../strongs/h/h3468.md): and her [chaciyd](../../strongs/h/h2623.md) shall [ranan](../../strongs/h/h7442.md) for [rannēn](../../strongs/h/h7444.md).

<a name="psalms_132_17"></a>Psalms 132:17

There will I [ṣāmaḥ](../../strongs/h/h6779.md) the [qeren](../../strongs/h/h7161.md) of [Dāviḏ](../../strongs/h/h1732.md) to [ṣāmaḥ](../../strongs/h/h6779.md): I have ['arak](../../strongs/h/h6186.md) a [nîr](../../strongs/h/h5216.md) for mine [mashiyach](../../strongs/h/h4899.md).

<a name="psalms_132_18"></a>Psalms 132:18

His ['oyeb](../../strongs/h/h341.md) will I [labash](../../strongs/h/h3847.md) with [bšeṯ](../../strongs/h/h1322.md): but upon himself shall his [nēzer](../../strongs/h/h5145.md) [tsuwts](../../strongs/h/h6692.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 131](psalms_131.md) - [Psalms 133](psalms_133.md)