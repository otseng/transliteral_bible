# [Psalms 16](https://www.blueletterbible.org/kjv/psa/16)

<a name="psalms_16_1"></a>Psalms 16:1

[Miḵtām](../../strongs/h/h4387.md) of [Dāviḏ](../../strongs/h/h1732.md). [shamar](../../strongs/h/h8104.md) me, ['el](../../strongs/h/h410.md): for in thee do I put my [chacah](../../strongs/h/h2620.md).

<a name="psalms_16_2"></a>Psalms 16:2

thou hast ['āmar](../../strongs/h/h559.md) unto [Yĕhovah](../../strongs/h/h3068.md), Thou my ['adonay](../../strongs/h/h136.md): my [towb](../../strongs/h/h2896.md) not to thee;

<a name="psalms_16_3"></a>Psalms 16:3

to the [qadowsh](../../strongs/h/h6918.md) that in the ['erets](../../strongs/h/h776.md), and the ['addiyr](../../strongs/h/h117.md), in whom all my [chephets](../../strongs/h/h2656.md).

<a name="psalms_16_4"></a>Psalms 16:4

Their ['atstsebeth](../../strongs/h/h6094.md) shall be [rabah](../../strongs/h/h7235.md) [māhar](../../strongs/h/h4116.md) another: their [necek](../../strongs/h/h5262.md) of [dam](../../strongs/h/h1818.md) will I not [nacak](../../strongs/h/h5258.md), nor [nasa'](../../strongs/h/h5375.md) their [shem](../../strongs/h/h8034.md) into my [saphah](../../strongs/h/h8193.md).

<a name="psalms_16_5"></a>Psalms 16:5

[Yĕhovah](../../strongs/h/h3068.md) the [mānâ](../../strongs/h/h4490.md) of mine [cheleq](../../strongs/h/h2506.md) and of my [kowc](../../strongs/h/h3563.md): thou [tamak](../../strongs/h/h8551.md) my [gôrāl](../../strongs/h/h1486.md).

<a name="psalms_16_6"></a>Psalms 16:6

The [chebel](../../strongs/h/h2256.md) are [naphal](../../strongs/h/h5307.md) unto me in [na'iym](../../strongs/h/h5273.md); yea, I have a [shaphar](../../strongs/h/h8231.md) [nachalah](../../strongs/h/h5159.md).

<a name="psalms_16_7"></a>Psalms 16:7

I will [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md), who hath given me [ya'ats](../../strongs/h/h3289.md): my [kilyah](../../strongs/h/h3629.md) also [yacar](../../strongs/h/h3256.md) me in the [layil](../../strongs/h/h3915.md).

<a name="psalms_16_8"></a>Psalms 16:8

I have [šāvâ](../../strongs/h/h7737.md) [Yĕhovah](../../strongs/h/h3068.md) [tāmîḏ](../../strongs/h/h8548.md) before me: because at my [yamiyn](../../strongs/h/h3225.md), I shall not be [mowt](../../strongs/h/h4131.md).

<a name="psalms_16_9"></a>Psalms 16:9

Therefore my [leb](../../strongs/h/h3820.md) is [samach](../../strongs/h/h8055.md), and my [kabowd](../../strongs/h/h3519.md) [giyl](../../strongs/h/h1523.md): my [basar](../../strongs/h/h1320.md) also shall [shakan](../../strongs/h/h7931.md) in [betach](../../strongs/h/h983.md).

<a name="psalms_16_10"></a>Psalms 16:10

For thou wilt not ['azab](../../strongs/h/h5800.md) my [nephesh](../../strongs/h/h5315.md) in [shĕ'owl](../../strongs/h/h7585.md); neither wilt thou [nathan](../../strongs/h/h5414.md) thine [chaciyd](../../strongs/h/h2623.md) to [ra'ah](../../strongs/h/h7200.md) [shachath](../../strongs/h/h7845.md).

<a name="psalms_16_11"></a>Psalms 16:11

Thou wilt [yada'](../../strongs/h/h3045.md) me the ['orach](../../strongs/h/h734.md) of [chay](../../strongs/h/h2416.md): in thy [paniym](../../strongs/h/h6440.md) is [śōḇaʿ](../../strongs/h/h7648.md) of [simchah](../../strongs/h/h8057.md); at thy [yamiyn](../../strongs/h/h3225.md) there are [na'iym](../../strongs/h/h5273.md) [netsach](../../strongs/h/h5331.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 15](psalms_15.md) - [Psalms 17](psalms_17.md)