# [Psalms 37](https://www.blueletterbible.org/kjv/psa/37/1/)

<a name="psalms_37_1"></a>Psalms 37:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [ḥārâ](../../strongs/h/h2734.md) not thyself because of [ra'a'](../../strongs/h/h7489.md), neither be thou [qānā'](../../strongs/h/h7065.md) against the ['asah](../../strongs/h/h6213.md) of ['evel](../../strongs/h/h5766.md).

<a name="psalms_37_2"></a>Psalms 37:2

For they shall [mᵊhērâ](../../strongs/h/h4120.md) be [namal](../../strongs/h/h5243.md) like the [chatsiyr](../../strongs/h/h2682.md), and [nabel](../../strongs/h/h5034.md) as the [yereq](../../strongs/h/h3418.md) [deše'](../../strongs/h/h1877.md).

<a name="psalms_37_3"></a>Psalms 37:3

[batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md), and ['asah](../../strongs/h/h6213.md) [towb](../../strongs/h/h2896.md); so shalt thou [shakan](../../strongs/h/h7931.md) in the ['erets](../../strongs/h/h776.md), and ['ĕmûnâ](../../strongs/h/h530.md) thou shalt be [ra'ah](../../strongs/h/h7462.md).

<a name="psalms_37_4"></a>Psalms 37:4

[ʿānaḡ](../../strongs/h/h6026.md) thyself also in [Yĕhovah](../../strongs/h/h3068.md): and he shall [nathan](../../strongs/h/h5414.md) thee the [miš'ālâ](../../strongs/h/h4862.md) of thine [leb](../../strongs/h/h3820.md).

<a name="psalms_37_5"></a>Psalms 37:5

[gālal](../../strongs/h/h1556.md) thy [derek](../../strongs/h/h1870.md) unto [Yĕhovah](../../strongs/h/h3068.md); [batach](../../strongs/h/h982.md) also in him; and he shall ['asah](../../strongs/h/h6213.md).

<a name="psalms_37_6"></a>Psalms 37:6

And he shall [yāṣā'](../../strongs/h/h3318.md) thy [tsedeq](../../strongs/h/h6664.md) as the ['owr](../../strongs/h/h216.md), and thy [mishpat](../../strongs/h/h4941.md) as the [ṣōhar](../../strongs/h/h6672.md).

<a name="psalms_37_7"></a>Psalms 37:7

[damam](../../strongs/h/h1826.md) in [Yĕhovah](../../strongs/h/h3068.md), and [chuwl](../../strongs/h/h2342.md) for him: [ḥārâ](../../strongs/h/h2734.md) not thyself because of him who [tsalach](../../strongs/h/h6743.md) in his [derek](../../strongs/h/h1870.md), because of the ['iysh](../../strongs/h/h376.md) who ['asah](../../strongs/h/h6213.md) [mezimmah](../../strongs/h/h4209.md) to ['asah](../../strongs/h/h6213.md).

<a name="psalms_37_8"></a>Psalms 37:8

[rāp̄â](../../strongs/h/h7503.md) from ['aph](../../strongs/h/h639.md), and ['azab](../../strongs/h/h5800.md) [chemah](../../strongs/h/h2534.md): [ḥārâ](../../strongs/h/h2734.md) not thyself in ['aḵ](../../strongs/h/h389.md) to do [ra'a'](../../strongs/h/h7489.md).

<a name="psalms_37_9"></a>Psalms 37:9

For [ra'a'](../../strongs/h/h7489.md) shall be [karath](../../strongs/h/h3772.md): but those that [qāvâ](../../strongs/h/h6960.md) upon [Yĕhovah](../../strongs/h/h3068.md), they shall [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md).

<a name="psalms_37_10"></a>Psalms 37:10

For yet a [mᵊʿaṭ](../../strongs/h/h4592.md), and the [rasha'](../../strongs/h/h7563.md) shall not be: yea, thou shalt diligently [bîn](../../strongs/h/h995.md) his [maqowm](../../strongs/h/h4725.md), and it shall not be.

<a name="psalms_37_11"></a>Psalms 37:11

But the ['anav](../../strongs/h/h6035.md) shall [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md); and shall [ʿānaḡ](../../strongs/h/h6026.md) themselves in the [rōḇ](../../strongs/h/h7230.md) of [shalowm](../../strongs/h/h7965.md).

<a name="psalms_37_12"></a>Psalms 37:12

The [rasha'](../../strongs/h/h7563.md) [zāmam](../../strongs/h/h2161.md) against the [tsaddiyq](../../strongs/h/h6662.md), and [ḥāraq](../../strongs/h/h2786.md) upon him with his [šēn](../../strongs/h/h8127.md).

<a name="psalms_37_13"></a>Psalms 37:13

['adonay](../../strongs/h/h136.md) shall [śāḥaq](../../strongs/h/h7832.md) at him: for he [ra'ah](../../strongs/h/h7200.md) that his [yowm](../../strongs/h/h3117.md) is [bow'](../../strongs/h/h935.md).

<a name="psalms_37_14"></a>Psalms 37:14

The [rasha'](../../strongs/h/h7563.md) have [pāṯaḥ](../../strongs/h/h6605.md) the [chereb](../../strongs/h/h2719.md), and have [dāraḵ](../../strongs/h/h1869.md) their [qesheth](../../strongs/h/h7198.md), to [naphal](../../strongs/h/h5307.md) the ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md), and to [ṭāḇaḥ](../../strongs/h/h2873.md) such as be of [yashar](../../strongs/h/h3477.md) [derek](../../strongs/h/h1870.md).

<a name="psalms_37_15"></a>Psalms 37:15

Their [chereb](../../strongs/h/h2719.md) shall [bow'](../../strongs/h/h935.md) into their own [leb](../../strongs/h/h3820.md), and their [qesheth](../../strongs/h/h7198.md) shall be [shabar](../../strongs/h/h7665.md).

<a name="psalms_37_16"></a>Psalms 37:16

A [mᵊʿaṭ](../../strongs/h/h4592.md) that a [tsaddiyq](../../strongs/h/h6662.md) hath is [towb](../../strongs/h/h2896.md) than the [hāmôn](../../strongs/h/h1995.md) of [rab](../../strongs/h/h7227.md) [rasha'](../../strongs/h/h7563.md).

<a name="psalms_37_17"></a>Psalms 37:17

For the [zerowa'](../../strongs/h/h2220.md) of the [rasha'](../../strongs/h/h7563.md) shall be [shabar](../../strongs/h/h7665.md): but [Yĕhovah](../../strongs/h/h3068.md) [camak](../../strongs/h/h5564.md) the [tsaddiyq](../../strongs/h/h6662.md).

<a name="psalms_37_18"></a>Psalms 37:18

[Yĕhovah](../../strongs/h/h3068.md) [yada'](../../strongs/h/h3045.md) the [yowm](../../strongs/h/h3117.md) of the [tamiym](../../strongs/h/h8549.md): and their [nachalah](../../strongs/h/h5159.md) shall be ['owlam](../../strongs/h/h5769.md).

<a name="psalms_37_19"></a>Psalms 37:19

They shall not be [buwsh](../../strongs/h/h954.md) in the [ra'](../../strongs/h/h7451.md) [ʿēṯ](../../strongs/h/h6256.md): and in the [yowm](../../strongs/h/h3117.md) of [rᵊʿāḇôn](../../strongs/h/h7459.md) they shall be [sāׂbaʿ](../../strongs/h/h7646.md).

<a name="psalms_37_20"></a>Psalms 37:20

But the [rasha'](../../strongs/h/h7563.md) shall ['abad](../../strongs/h/h6.md), and the ['oyeb](../../strongs/h/h341.md) of [Yĕhovah](../../strongs/h/h3068.md) shall be as the [yāqār](../../strongs/h/h3368.md) of [kar](../../strongs/h/h3733.md): they shall [kalah](../../strongs/h/h3615.md); into ['ashan](../../strongs/h/h6227.md) shall they [kalah](../../strongs/h/h3615.md).

<a name="psalms_37_21"></a>Psalms 37:21

The [rasha'](../../strongs/h/h7563.md) [lāvâ](../../strongs/h/h3867.md), and [shalam](../../strongs/h/h7999.md) not again: but the [tsaddiyq](../../strongs/h/h6662.md) sheweth [chanan](../../strongs/h/h2603.md), and [nathan](../../strongs/h/h5414.md).

<a name="psalms_37_22"></a>Psalms 37:22

For such as be [barak](../../strongs/h/h1288.md) of him shall [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md); and they that be [qālal](../../strongs/h/h7043.md) of him shall be [karath](../../strongs/h/h3772.md).

<a name="psalms_37_23"></a>Psalms 37:23

The [miṣʿāḏ](../../strongs/h/h4703.md) of a [geḇer](../../strongs/h/h1397.md) are [kuwn](../../strongs/h/h3559.md) by [Yĕhovah](../../strongs/h/h3068.md): and he [ḥāp̄ēṣ](../../strongs/h/h2654.md) in his [derek](../../strongs/h/h1870.md).

<a name="psalms_37_24"></a>Psalms 37:24

Though he [naphal](../../strongs/h/h5307.md), he shall not be [ṭûl](../../strongs/h/h2904.md): for [Yĕhovah](../../strongs/h/h3068.md) [camak](../../strongs/h/h5564.md) him with his [yad](../../strongs/h/h3027.md).

<a name="psalms_37_25"></a>Psalms 37:25

I have been [naʿar](../../strongs/h/h5288.md), and now am [zāqēn](../../strongs/h/h2204.md); yet have I not [ra'ah](../../strongs/h/h7200.md) the [tsaddiyq](../../strongs/h/h6662.md) ['azab](../../strongs/h/h5800.md), nor his [zera'](../../strongs/h/h2233.md) [bāqaš](../../strongs/h/h1245.md) [lechem](../../strongs/h/h3899.md).

<a name="psalms_37_26"></a>Psalms 37:26

He is [yowm](../../strongs/h/h3117.md) [chanan](../../strongs/h/h2603.md), and [lāvâ](../../strongs/h/h3867.md); and his [zera'](../../strongs/h/h2233.md) is [bĕrakah](../../strongs/h/h1293.md).

<a name="psalms_37_27"></a>Psalms 37:27

[cuwr](../../strongs/h/h5493.md) from [ra'](../../strongs/h/h7451.md), and ['asah](../../strongs/h/h6213.md) [towb](../../strongs/h/h2896.md); and [shakan](../../strongs/h/h7931.md) ['owlam](../../strongs/h/h5769.md).

<a name="psalms_37_28"></a>Psalms 37:28

For [Yĕhovah](../../strongs/h/h3068.md) ['ahab](../../strongs/h/h157.md) [mishpat](../../strongs/h/h4941.md), and ['azab](../../strongs/h/h5800.md) not his [chaciyd](../../strongs/h/h2623.md); they are [shamar](../../strongs/h/h8104.md) ['owlam](../../strongs/h/h5769.md): but the [zera'](../../strongs/h/h2233.md) of the [rasha'](../../strongs/h/h7563.md) shall be [karath](../../strongs/h/h3772.md).

<a name="psalms_37_29"></a>Psalms 37:29

The [tsaddiyq](../../strongs/h/h6662.md) shall [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md), and [shakan](../../strongs/h/h7931.md) therein [ʿaḏ](../../strongs/h/h5703.md).

<a name="psalms_37_30"></a>Psalms 37:30

The [peh](../../strongs/h/h6310.md) of the [tsaddiyq](../../strongs/h/h6662.md) [hagah](../../strongs/h/h1897.md) [ḥāḵmâ](../../strongs/h/h2451.md), and his [lashown](../../strongs/h/h3956.md) [dabar](../../strongs/h/h1696.md) of [mishpat](../../strongs/h/h4941.md).

<a name="psalms_37_31"></a>Psalms 37:31

The [towrah](../../strongs/h/h8451.md) of his ['Elohiym](../../strongs/h/h430.md) is in his [leb](../../strongs/h/h3820.md); none of his ['ashuwr](../../strongs/h/h838.md) shall [māʿaḏ](../../strongs/h/h4571.md).

<a name="psalms_37_32"></a>Psalms 37:32

The [rasha'](../../strongs/h/h7563.md) [tsaphah](../../strongs/h/h6822.md) the [tsaddiyq](../../strongs/h/h6662.md), and [bāqaš](../../strongs/h/h1245.md) to [muwth](../../strongs/h/h4191.md) him.

<a name="psalms_37_33"></a>Psalms 37:33

[Yĕhovah](../../strongs/h/h3068.md) will not ['azab](../../strongs/h/h5800.md) him in his [yad](../../strongs/h/h3027.md), nor [rāšaʿ](../../strongs/h/h7561.md) him when he is [shaphat](../../strongs/h/h8199.md).

<a name="psalms_37_34"></a>Psalms 37:34

[qāvâ](../../strongs/h/h6960.md) on [Yĕhovah](../../strongs/h/h3068.md), and [shamar](../../strongs/h/h8104.md) his [derek](../../strongs/h/h1870.md), and he shall [ruwm](../../strongs/h/h7311.md) thee to [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md): when the [rasha'](../../strongs/h/h7563.md) are [karath](../../strongs/h/h3772.md), thou shalt [ra'ah](../../strongs/h/h7200.md) it.

<a name="psalms_37_35"></a>Psalms 37:35

I have [ra'ah](../../strongs/h/h7200.md) the [rasha'](../../strongs/h/h7563.md) in [ʿārîṣ](../../strongs/h/h6184.md), and [ʿārâ](../../strongs/h/h6168.md) himself like a [raʿănān](../../strongs/h/h7488.md) ['ezrāḥ](../../strongs/h/h249.md).

<a name="psalms_37_36"></a>Psalms 37:36

Yet he ['abar](../../strongs/h/h5674.md), and, lo, he was not: yea, I [bāqaš](../../strongs/h/h1245.md) him, but he could not be [māṣā'](../../strongs/h/h4672.md).

<a name="psalms_37_37"></a>Psalms 37:37

[shamar](../../strongs/h/h8104.md) the [tām](../../strongs/h/h8535.md), and [ra'ah](../../strongs/h/h7200.md) the [yashar](../../strongs/h/h3477.md): for the ['aḥărîṯ](../../strongs/h/h319.md) of that ['iysh](../../strongs/h/h376.md) is [shalowm](../../strongs/h/h7965.md).

<a name="psalms_37_38"></a>Psalms 37:38

But the [pāšaʿ](../../strongs/h/h6586.md) shall be [šāmaḏ](../../strongs/h/h8045.md) [yaḥaḏ](../../strongs/h/h3162.md): the ['aḥărîṯ](../../strongs/h/h319.md) of the [rasha'](../../strongs/h/h7563.md) shall be [karath](../../strongs/h/h3772.md).

<a name="psalms_37_39"></a>Psalms 37:39

But the [tᵊšûʿâ](../../strongs/h/h8668.md) of the [tsaddiyq](../../strongs/h/h6662.md) is of [Yĕhovah](../../strongs/h/h3068.md): he is their [māʿôz](../../strongs/h/h4581.md) in the [ʿēṯ](../../strongs/h/h6256.md) of [tsarah](../../strongs/h/h6869.md).

<a name="psalms_37_40"></a>Psalms 37:40

And [Yĕhovah](../../strongs/h/h3068.md) shall [ʿāzar](../../strongs/h/h5826.md) them, and [palat](../../strongs/h/h6403.md) them: he shall [palat](../../strongs/h/h6403.md) them from the [rasha'](../../strongs/h/h7563.md), and [yasha'](../../strongs/h/h3467.md) them, because they [chacah](../../strongs/h/h2620.md) in him.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 36](psalms_36.md) - [Psalms 38](psalms_38.md)