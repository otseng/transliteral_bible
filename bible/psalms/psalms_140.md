# [Psalms 140](https://www.blueletterbible.org/kjv/psalms/140)

<a name="psalms_140_1"></a>Psalms 140:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [chalats](../../strongs/h/h2502.md) me, [Yĕhovah](../../strongs/h/h3068.md), from the [ra'](../../strongs/h/h7451.md) ['āḏām](../../strongs/h/h120.md): [nāṣar](../../strongs/h/h5341.md) me from the [chamac](../../strongs/h/h2555.md) ['iysh](../../strongs/h/h376.md);

<a name="psalms_140_2"></a>Psalms 140:2

Which [chashab](../../strongs/h/h2803.md) [ra'](../../strongs/h/h7451.md) in their [leb](../../strongs/h/h3820.md); [yowm](../../strongs/h/h3117.md) are they [guwr](../../strongs/h/h1481.md) for [milḥāmâ](../../strongs/h/h4421.md).

<a name="psalms_140_3"></a>Psalms 140:3

They have [šānan](../../strongs/h/h8150.md) their [lashown](../../strongs/h/h3956.md) like a [nachash](../../strongs/h/h5175.md); [ʿaḵšûḇ](../../strongs/h/h5919.md) [chemah](../../strongs/h/h2534.md) is under their [saphah](../../strongs/h/h8193.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_140_4"></a>Psalms 140:4

[shamar](../../strongs/h/h8104.md) me, [Yĕhovah](../../strongs/h/h3068.md), from the [yad](../../strongs/h/h3027.md) of the [rasha'](../../strongs/h/h7563.md); [nāṣar](../../strongs/h/h5341.md) me from the [chamac](../../strongs/h/h2555.md) ['iysh](../../strongs/h/h376.md); who have [chashab](../../strongs/h/h2803.md) to [dāḥâ](../../strongs/h/h1760.md) my [pa'am](../../strongs/h/h6471.md).

<a name="psalms_140_5"></a>Psalms 140:5

The [gē'ê](../../strongs/h/h1343.md) have [taman](../../strongs/h/h2934.md) a [paḥ](../../strongs/h/h6341.md) for me, and [chebel](../../strongs/h/h2256.md); they have [pāraś](../../strongs/h/h6566.md) a [rešeṯ](../../strongs/h/h7568.md) by the [yad](../../strongs/h/h3027.md) [ma'gal](../../strongs/h/h4570.md); they have [shiyth](../../strongs/h/h7896.md) [mowqesh](../../strongs/h/h4170.md) for me. [Celah](../../strongs/h/h5542.md).

<a name="psalms_140_6"></a>Psalms 140:6

I ['āmar](../../strongs/h/h559.md) unto [Yĕhovah](../../strongs/h/h3068.md), Thou art my ['el](../../strongs/h/h410.md): ['azan](../../strongs/h/h238.md) the [qowl](../../strongs/h/h6963.md) of my [taḥănûn](../../strongs/h/h8469.md), [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_140_7"></a>Psalms 140:7

[Yᵊhōvâ](../../strongs/h/h3069.md) the ['adonay](../../strongs/h/h136.md), the ['oz](../../strongs/h/h5797.md) of my [yĕshuw'ah](../../strongs/h/h3444.md), thou hast [cakak](../../strongs/h/h5526.md) my [ro'sh](../../strongs/h/h7218.md) in the [yowm](../../strongs/h/h3117.md) of [nešeq](../../strongs/h/h5402.md).

<a name="psalms_140_8"></a>Psalms 140:8

[nathan](../../strongs/h/h5414.md) not, [Yĕhovah](../../strongs/h/h3068.md), the [ma'ăvay](../../strongs/h/h3970.md) of the [rasha'](../../strongs/h/h7563.md): [pûq](../../strongs/h/h6329.md) not his [zāmām](../../strongs/h/h2162.md); lest they [ruwm](../../strongs/h/h7311.md) themselves. [Celah](../../strongs/h/h5542.md).

<a name="psalms_140_9"></a>Psalms 140:9

As for the [ro'sh](../../strongs/h/h7218.md) of those that [mēsaḇ](../../strongs/h/h4524.md) me, let the ['amal](../../strongs/h/h5999.md) of their own [saphah](../../strongs/h/h8193.md) [kāsâ](../../strongs/h/h3680.md) them.

<a name="psalms_140_10"></a>Psalms 140:10

Let [gechel](../../strongs/h/h1513.md) [mowt](../../strongs/h/h4131.md) [mowt](../../strongs/h/h4131.md) upon them: let them be [naphal](../../strongs/h/h5307.md) into the ['esh](../../strongs/h/h784.md); into [mahămōrôṯ](../../strongs/h/h4113.md), that they not [quwm](../../strongs/h/h6965.md).

<a name="psalms_140_11"></a>Psalms 140:11

Let not an ['iysh](../../strongs/h/h376.md) [lashown](../../strongs/h/h3956.md) be [kuwn](../../strongs/h/h3559.md) in the ['erets](../../strongs/h/h776.md): [ra'](../../strongs/h/h7451.md) shall [ṣûḏ](../../strongs/h/h6679.md) the [chamac](../../strongs/h/h2555.md) ['iysh](../../strongs/h/h376.md) to [maḏḥēp̄ô](../../strongs/h/h4073.md) him.

<a name="psalms_140_12"></a>Psalms 140:12

I [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) will ['asah](../../strongs/h/h6213.md) the [diyn](../../strongs/h/h1779.md) of the ['aniy](../../strongs/h/h6041.md), and the [mishpat](../../strongs/h/h4941.md) of the ['ebyown](../../strongs/h/h34.md).

<a name="psalms_140_13"></a>Psalms 140:13

Surely the [tsaddiyq](../../strongs/h/h6662.md) shall [yadah](../../strongs/h/h3034.md) unto thy [shem](../../strongs/h/h8034.md): the [yashar](../../strongs/h/h3477.md) shall [yashab](../../strongs/h/h3427.md) in thy [paniym](../../strongs/h/h6440.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 139](psalms_139.md) - [Psalms 141](psalms_141.md)