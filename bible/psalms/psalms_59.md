# [Psalms 59](https://www.blueletterbible.org/kjv/psa/59)

<a name="psalms_59_1"></a>Psalms 59:1

To the [nāṣaḥ](../../strongs/h/h5329.md), ['Al-tašḥēṯ](../../strongs/h/h516.md), [Miḵtām](../../strongs/h/h4387.md) of [Dāviḏ](../../strongs/h/h1732.md); when [Šā'ûl](../../strongs/h/h7586.md) [shalach](../../strongs/h/h7971.md), and they [shamar](../../strongs/h/h8104.md) the [bayith](../../strongs/h/h1004.md) to [muwth](../../strongs/h/h4191.md) him. [natsal](../../strongs/h/h5337.md) me from mine ['oyeb](../../strongs/h/h341.md), ['Elohiym](../../strongs/h/h430.md): [śāḡaḇ](../../strongs/h/h7682.md) me from them that [quwm](../../strongs/h/h6965.md) against me.

<a name="psalms_59_2"></a>Psalms 59:2

[natsal](../../strongs/h/h5337.md) me from the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md), and [yasha'](../../strongs/h/h3467.md) me from [dam](../../strongs/h/h1818.md) ['enowsh](../../strongs/h/h582.md).

<a name="psalms_59_3"></a>Psalms 59:3

For, lo, they ['arab](../../strongs/h/h693.md) for my [nephesh](../../strongs/h/h5315.md): the ['az](../../strongs/h/h5794.md) are [guwr](../../strongs/h/h1481.md) against me; not for my [pesha'](../../strongs/h/h6588.md), nor for my [chatta'ath](../../strongs/h/h2403.md), [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_59_4"></a>Psalms 59:4

They [rûṣ](../../strongs/h/h7323.md) and [kuwn](../../strongs/h/h3559.md) themselves without my ['avon](../../strongs/h/h5771.md): [ʿûr](../../strongs/h/h5782.md) to [qārā'](../../strongs/h/h7125.md) me, and [ra'ah](../../strongs/h/h7200.md).

<a name="psalms_59_5"></a>Psalms 59:5

Thou therefore, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), [quwts](../../strongs/h/h6974.md) to [paqad](../../strongs/h/h6485.md) all the [gowy](../../strongs/h/h1471.md): be not [chanan](../../strongs/h/h2603.md) to any ['aven](../../strongs/h/h205.md) [bāḡaḏ](../../strongs/h/h898.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_59_6"></a>Psalms 59:6

They [shuwb](../../strongs/h/h7725.md) at ['ereb](../../strongs/h/h6153.md): they make a [hāmâ](../../strongs/h/h1993.md) like a [keleḇ](../../strongs/h/h3611.md), and [cabab](../../strongs/h/h5437.md) the [ʿîr](../../strongs/h/h5892.md).

<a name="psalms_59_7"></a>Psalms 59:7

Behold, they [nāḇaʿ](../../strongs/h/h5042.md) with their [peh](../../strongs/h/h6310.md): [chereb](../../strongs/h/h2719.md) are in their [saphah](../../strongs/h/h8193.md): for who, say they, doth [shama'](../../strongs/h/h8085.md)?

<a name="psalms_59_8"></a>Psalms 59:8

But thou, [Yĕhovah](../../strongs/h/h3068.md), shalt [śāḥaq](../../strongs/h/h7832.md) at them; thou shalt have all the [gowy](../../strongs/h/h1471.md) in [lāʿaḡ](../../strongs/h/h3932.md).

<a name="psalms_59_9"></a>Psalms 59:9

Because of his ['oz](../../strongs/h/h5797.md) will I [shamar](../../strongs/h/h8104.md) upon thee: for ['Elohiym](../../strongs/h/h430.md) is my [misgab](../../strongs/h/h4869.md).

<a name="psalms_59_10"></a>Psalms 59:10

The ['Elohiym](../../strongs/h/h430.md) of my [checed](../../strongs/h/h2617.md) shall [qadam](../../strongs/h/h6923.md) me: ['Elohiym](../../strongs/h/h430.md) shall let me [ra'ah](../../strongs/h/h7200.md) my desire upon mine [sharar](../../strongs/h/h8324.md).

<a name="psalms_59_11"></a>Psalms 59:11

[harag](../../strongs/h/h2026.md) them not, lest my ['am](../../strongs/h/h5971.md) [shakach](../../strongs/h/h7911.md): [nûaʿ](../../strongs/h/h5128.md) them by thy [ḥayil](../../strongs/h/h2428.md); and [yarad](../../strongs/h/h3381.md) them, ['adonay](../../strongs/h/h136.md) our [magen](../../strongs/h/h4043.md).

<a name="psalms_59_12"></a>Psalms 59:12

For the [chatta'ath](../../strongs/h/h2403.md) of their [peh](../../strongs/h/h6310.md) and the [dabar](../../strongs/h/h1697.md) of their [saphah](../../strongs/h/h8193.md) let them even be [lāḵaḏ](../../strongs/h/h3920.md) in their [gā'ôn](../../strongs/h/h1347.md): and for ['alah](../../strongs/h/h423.md) and [kaḥaš](../../strongs/h/h3585.md) which they [sāp̄ar](../../strongs/h/h5608.md).

<a name="psalms_59_13"></a>Psalms 59:13

[kalah](../../strongs/h/h3615.md) them in [chemah](../../strongs/h/h2534.md), [kalah](../../strongs/h/h3615.md) them, that they may not be: and let them [yada'](../../strongs/h/h3045.md) that ['Elohiym](../../strongs/h/h430.md) [mashal](../../strongs/h/h4910.md) in [Ya'aqob](../../strongs/h/h3290.md) unto the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_59_14"></a>Psalms 59:14

And at ['ereb](../../strongs/h/h6153.md) let them [shuwb](../../strongs/h/h7725.md); and let them make a [hāmâ](../../strongs/h/h1993.md) like a [keleḇ](../../strongs/h/h3611.md), and [cabab](../../strongs/h/h5437.md) about the [ʿîr](../../strongs/h/h5892.md).

<a name="psalms_59_15"></a>Psalms 59:15

Let them [nûaʿ](../../strongs/h/h5128.md) [nûaʿ](../../strongs/h/h5128.md) for ['akal](../../strongs/h/h398.md), and [lûn](../../strongs/h/h3885.md) if they be not [sāׂbaʿ](../../strongs/h/h7646.md).

<a name="psalms_59_16"></a>Psalms 59:16

But I will [shiyr](../../strongs/h/h7891.md) of thy ['oz](../../strongs/h/h5797.md); yea, I will [ranan](../../strongs/h/h7442.md) of thy [checed](../../strongs/h/h2617.md) in the [boqer](../../strongs/h/h1242.md): for thou hast been my [misgab](../../strongs/h/h4869.md) and [mānôs](../../strongs/h/h4498.md) in the [yowm](../../strongs/h/h3117.md) of my [tsar](../../strongs/h/h6862.md).

<a name="psalms_59_17"></a>Psalms 59:17

Unto thee, O my ['oz](../../strongs/h/h5797.md), will I [zamar](../../strongs/h/h2167.md): for ['Elohiym](../../strongs/h/h430.md) is my [misgab](../../strongs/h/h4869.md), and the ['Elohiym](../../strongs/h/h430.md) of my [checed](../../strongs/h/h2617.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 58](psalms_58.md) - [Psalms 60](psalms_60.md)