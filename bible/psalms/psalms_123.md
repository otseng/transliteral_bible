# [Psalms 123](https://www.blueletterbible.org/kjv/psalms/123)

<a name="psalms_123_1"></a>Psalms 123:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md). Unto thee I [nasa'](../../strongs/h/h5375.md) mine ['ayin](../../strongs/h/h5869.md), O thou that [yashab](../../strongs/h/h3427.md) in the [shamayim](../../strongs/h/h8064.md).

<a name="psalms_123_2"></a>Psalms 123:2

Behold, as the ['ayin](../../strongs/h/h5869.md) of ['ebed](../../strongs/h/h5650.md) look unto the [yad](../../strongs/h/h3027.md) of their ['adown](../../strongs/h/h113.md), and as the ['ayin](../../strongs/h/h5869.md) of a [šip̄ḥâ](../../strongs/h/h8198.md) unto the [yad](../../strongs/h/h3027.md) of her [gᵊḇereṯ](../../strongs/h/h1404.md); so our ['ayin](../../strongs/h/h5869.md) wait upon [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), until that he have [chanan](../../strongs/h/h2603.md) upon us.

<a name="psalms_123_3"></a>Psalms 123:3

Have [chanan](../../strongs/h/h2603.md) upon us, [Yĕhovah](../../strongs/h/h3068.md), have [chanan](../../strongs/h/h2603.md) upon us: for we are [rab](../../strongs/h/h7227.md) [sāׂbaʿ](../../strongs/h/h7646.md) with [bûz](../../strongs/h/h937.md).

<a name="psalms_123_4"></a>Psalms 123:4

Our [nephesh](../../strongs/h/h5315.md) is [rab](../../strongs/h/h7227.md) [sāׂbaʿ](../../strongs/h/h7646.md) with the [laʿaḡ](../../strongs/h/h3933.md) of those that are at [ša'ănān](../../strongs/h/h7600.md), and with the [bûz](../../strongs/h/h937.md) of the [yānâ](../../strongs/h/h3238.md) [gē'ê](../../strongs/h/h1343.md) [ga'ăyôn](../../strongs/h/h1349.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 122](psalms_122.md) - [Psalms 124](psalms_124.md)