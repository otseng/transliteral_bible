# [Psalms 103](https://www.blueletterbible.org/kjv/psalms/103)

<a name="psalms_103_1"></a>Psalms 103:1

A Psalm of [Dāviḏ](../../strongs/h/h1732.md). [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md), O my [nephesh](../../strongs/h/h5315.md): and all that is [qereḇ](../../strongs/h/h7130.md) me, bless his [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md).

<a name="psalms_103_2"></a>Psalms 103:2

[barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md), O my [nephesh](../../strongs/h/h5315.md), and [shakach](../../strongs/h/h7911.md) not all his [gĕmwl](../../strongs/h/h1576.md):

<a name="psalms_103_3"></a>Psalms 103:3

Who [sālaḥ](../../strongs/h/h5545.md) all thine ['avon](../../strongs/h/h5771.md); who [rapha'](../../strongs/h/h7495.md) all thy [taḥălu'iym](../../strongs/h/h8463.md);

<a name="psalms_103_4"></a>Psalms 103:4

Who [gā'al](../../strongs/h/h1350.md) thy [chay](../../strongs/h/h2416.md) from [shachath](../../strongs/h/h7845.md); who ['atar](../../strongs/h/h5849.md) thee with [checed](../../strongs/h/h2617.md) and tender [raḥam](../../strongs/h/h7356.md);

<a name="psalms_103_5"></a>Psalms 103:5

Who [sāׂbaʿ](../../strongs/h/h7646.md) thy [ʿădiy](../../strongs/h/h5716.md) with [towb](../../strongs/h/h2896.md) things; so that thy [nāʿur](../../strongs/h/h5271.md) is [ḥādaš](../../strongs/h/h2318.md) like the [nesheׁr](../../strongs/h/h5404.md).

<a name="psalms_103_6"></a>Psalms 103:6

[Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) [tsedaqah](../../strongs/h/h6666.md) and [mishpat](../../strongs/h/h4941.md) for all that are [ʿāšaq](../../strongs/h/h6231.md).

<a name="psalms_103_7"></a>Psalms 103:7

He made [yada'](../../strongs/h/h3045.md) his [derek](../../strongs/h/h1870.md) unto [Mōshe](../../strongs/h/h4872.md), his ['aliylah](../../strongs/h/h5949.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="psalms_103_8"></a>Psalms 103:8

[Yĕhovah](../../strongs/h/h3068.md) is [raḥwm](../../strongs/h/h7349.md) and [ḥanwn](../../strongs/h/h2587.md), ['ārēḵ](../../strongs/h/h750.md) to ['aph](../../strongs/h/h639.md), and [rab](../../strongs/h/h7227.md) in [checed](../../strongs/h/h2617.md).

<a name="psalms_103_9"></a>Psalms 103:9

He will not [netsach](../../strongs/h/h5331.md) [riyb](../../strongs/h/h7378.md): neither will he [nāṭar](../../strongs/h/h5201.md) his anger ['owlam](../../strongs/h/h5769.md).

<a name="psalms_103_10"></a>Psalms 103:10

He hath not ['asah](../../strongs/h/h6213.md) with us after our [ḥēṭĕ'](../../strongs/h/h2399.md); nor [gamal](../../strongs/h/h1580.md) us according to our ['avon](../../strongs/h/h5771.md).

<a name="psalms_103_11"></a>Psalms 103:11

For as the [shamayim](../../strongs/h/h8064.md) is [gāḇah](../../strongs/h/h1361.md) the ['erets](../../strongs/h/h776.md), so [gabar](../../strongs/h/h1396.md) is his [checed](../../strongs/h/h2617.md) toward them that [yārē'](../../strongs/h/h3373.md) him.

<a name="psalms_103_12"></a>Psalms 103:12

[rachaq](../../strongs/h/h7368.md) the [mizrach](../../strongs/h/h4217.md) is from the [ma'arab](../../strongs/h/h4628.md), so far hath he [rachaq](../../strongs/h/h7368.md) our [pesha'](../../strongs/h/h6588.md) from us.

<a name="psalms_103_13"></a>Psalms 103:13

Like as an ['ab](../../strongs/h/h1.md) [racham](../../strongs/h/h7355.md) his [ben](../../strongs/h/h1121.md), so [Yĕhovah](../../strongs/h/h3068.md) [racham](../../strongs/h/h7355.md) them that [yārē'](../../strongs/h/h3373.md) him.

<a name="psalms_103_14"></a>Psalms 103:14

For he [yada'](../../strongs/h/h3045.md) our [yetser](../../strongs/h/h3336.md); he [zakar](../../strongs/h/h2142.md) that we are ['aphar](../../strongs/h/h6083.md).

<a name="psalms_103_15"></a>Psalms 103:15

As for ['enowsh](../../strongs/h/h582.md), his [yowm](../../strongs/h/h3117.md) are as [chatsiyr](../../strongs/h/h2682.md): as a [tsiyts](../../strongs/h/h6731.md) of the [sadeh](../../strongs/h/h7704.md), so he [tsuwts](../../strongs/h/h6692.md).

<a name="psalms_103_16"></a>Psalms 103:16

For the [ruwach](../../strongs/h/h7307.md) ['abar](../../strongs/h/h5674.md) it, and it is gone; and the [maqowm](../../strongs/h/h4725.md) thereof shall [nāḵar](../../strongs/h/h5234.md) it no more.

<a name="psalms_103_17"></a>Psalms 103:17

But the [checed](../../strongs/h/h2617.md) of [Yĕhovah](../../strongs/h/h3068.md) is from ['owlam](../../strongs/h/h5769.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md) upon them that [yārē'](../../strongs/h/h3373.md) him, and his [tsedaqah](../../strongs/h/h6666.md) unto [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md);

<a name="psalms_103_18"></a>Psalms 103:18

To such as [shamar](../../strongs/h/h8104.md) his [bĕriyth](../../strongs/h/h1285.md), and to those that [zakar](../../strongs/h/h2142.md) his [piqquwd](../../strongs/h/h6490.md) to ['asah](../../strongs/h/h6213.md) them.

<a name="psalms_103_19"></a>Psalms 103:19

[Yĕhovah](../../strongs/h/h3068.md) hath [kuwn](../../strongs/h/h3559.md) his [kicce'](../../strongs/h/h3678.md) in the [shamayim](../../strongs/h/h8064.md); and his [malkuwth](../../strongs/h/h4438.md) [mashal](../../strongs/h/h4910.md) over all.

<a name="psalms_103_20"></a>Psalms 103:20

[barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md), ye his [mal'ak](../../strongs/h/h4397.md), that [gibôr](../../strongs/h/h1368.md) in [koach](../../strongs/h/h3581.md), that ['asah](../../strongs/h/h6213.md) his [dabar](../../strongs/h/h1697.md), [shama'](../../strongs/h/h8085.md) unto the [qowl](../../strongs/h/h6963.md) of his [dabar](../../strongs/h/h1697.md).

<a name="psalms_103_21"></a>Psalms 103:21

[barak](../../strongs/h/h1288.md) ye [Yĕhovah](../../strongs/h/h3068.md), all ye his [tsaba'](../../strongs/h/h6635.md); ye [sharath](../../strongs/h/h8334.md) of his, that ['asah](../../strongs/h/h6213.md) his [ratsown](../../strongs/h/h7522.md).

<a name="psalms_103_22"></a>Psalms 103:22

[barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md), all his [ma'aseh](../../strongs/h/h4639.md) in all [maqowm](../../strongs/h/h4725.md) of his [memshalah](../../strongs/h/h4475.md): [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md), O my [nephesh](../../strongs/h/h5315.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 102](psalms_102.md) - [Psalms 104](psalms_104.md)