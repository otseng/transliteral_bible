# [Psalms 102](https://www.blueletterbible.org/kjv/psalms/102)

<a name="psalms_102_1"></a>Psalms 102:1

A [tĕphillah](../../strongs/h/h8605.md) of the ['aniy](../../strongs/h/h6041.md), when he is [ʿāṭap̄](../../strongs/h/h5848.md), and [šāp̄aḵ](../../strongs/h/h8210.md) his [śîaḥ](../../strongs/h/h7879.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md). [shama'](../../strongs/h/h8085.md) my [tĕphillah](../../strongs/h/h8605.md), [Yĕhovah](../../strongs/h/h3068.md), and let my [shav'ah](../../strongs/h/h7775.md) [bow'](../../strongs/h/h935.md) unto thee.

<a name="psalms_102_2"></a>Psalms 102:2

[cathar](../../strongs/h/h5641.md) not thy [paniym](../../strongs/h/h6440.md) from me in the [yowm](../../strongs/h/h3117.md) when I am in [tsar](../../strongs/h/h6862.md); [natah](../../strongs/h/h5186.md) thine ['ozen](../../strongs/h/h241.md) unto me: in the [yowm](../../strongs/h/h3117.md) when I [qara'](../../strongs/h/h7121.md) ['anah](../../strongs/h/h6030.md) me [mahēr](../../strongs/h/h4118.md).

<a name="psalms_102_3"></a>Psalms 102:3

For my [yowm](../../strongs/h/h3117.md) are [kalah](../../strongs/h/h3615.md) like ['ashan](../../strongs/h/h6227.md), and my ['etsem](../../strongs/h/h6106.md) are [ḥārar](../../strongs/h/h2787.md) as a [môqēḏ](../../strongs/h/h4168.md).

<a name="psalms_102_4"></a>Psalms 102:4

My [leb](../../strongs/h/h3820.md) is [nakah](../../strongs/h/h5221.md), and [yāḇēš](../../strongs/h/h3001.md) like ['eseb](../../strongs/h/h6212.md); so that I [shakach](../../strongs/h/h7911.md) to ['akal](../../strongs/h/h398.md) my [lechem](../../strongs/h/h3899.md).

<a name="psalms_102_5"></a>Psalms 102:5

By reason of the [qowl](../../strongs/h/h6963.md) of my ['anachah](../../strongs/h/h585.md) my ['etsem](../../strongs/h/h6106.md) [dāḇaq](../../strongs/h/h1692.md) to my [basar](../../strongs/h/h1320.md).

<a name="psalms_102_6"></a>Psalms 102:6

I am [dāmâ](../../strongs/h/h1819.md) a [qā'aṯ](../../strongs/h/h6893.md) of the [midbar](../../strongs/h/h4057.md): I am like a [kowc](../../strongs/h/h3563.md) of the [chorbah](../../strongs/h/h2723.md).

<a name="psalms_102_7"></a>Psalms 102:7

I [šāqaḏ](../../strongs/h/h8245.md), and am as a [tsippowr](../../strongs/h/h6833.md) [bāḏaḏ](../../strongs/h/h909.md) upon the [gāḡ](../../strongs/h/h1406.md).

<a name="psalms_102_8"></a>Psalms 102:8

Mine ['oyeb](../../strongs/h/h341.md) [ḥārap̄](../../strongs/h/h2778.md) me all the [yowm](../../strongs/h/h3117.md); and they that are [halal](../../strongs/h/h1984.md) against me are [shaba'](../../strongs/h/h7650.md) against me.

<a name="psalms_102_9"></a>Psalms 102:9

For I have ['akal](../../strongs/h/h398.md) ['ēp̄er](../../strongs/h/h665.md) like [lechem](../../strongs/h/h3899.md), and [māsaḵ](../../strongs/h/h4537.md) my [šiqquvay](../../strongs/h/h8249.md) with [bĕkiy](../../strongs/h/h1065.md),

<a name="psalms_102_10"></a>Psalms 102:10

[paniym](../../strongs/h/h6440.md) of thine [zaʿam](../../strongs/h/h2195.md) and thy [qeṣep̄](../../strongs/h/h7110.md): for thou hast [nasa'](../../strongs/h/h5375.md) me, and [shalak](../../strongs/h/h7993.md) me.

<a name="psalms_102_11"></a>Psalms 102:11

My [yowm](../../strongs/h/h3117.md) are like a [ṣēl](../../strongs/h/h6738.md) that [natah](../../strongs/h/h5186.md); and I am [yāḇēš](../../strongs/h/h3001.md) like ['eseb](../../strongs/h/h6212.md).

<a name="psalms_102_12"></a>Psalms 102:12

But thou, [Yĕhovah](../../strongs/h/h3068.md), shalt [yashab](../../strongs/h/h3427.md) ['owlam](../../strongs/h/h5769.md); and thy [zeker](../../strongs/h/h2143.md) unto [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md).

<a name="psalms_102_13"></a>Psalms 102:13

Thou shalt [quwm](../../strongs/h/h6965.md), and have [racham](../../strongs/h/h7355.md) upon [Tsiyown](../../strongs/h/h6726.md): for the [ʿēṯ](../../strongs/h/h6256.md) to [chanan](../../strongs/h/h2603.md) her, yea, the [môʿēḏ](../../strongs/h/h4150.md), is [bow'](../../strongs/h/h935.md).

<a name="psalms_102_14"></a>Psalms 102:14

For thy ['ebed](../../strongs/h/h5650.md) [ratsah](../../strongs/h/h7521.md) in her ['eben](../../strongs/h/h68.md), and [chanan](../../strongs/h/h2603.md) the ['aphar](../../strongs/h/h6083.md) thereof.

<a name="psalms_102_15"></a>Psalms 102:15

So the [gowy](../../strongs/h/h1471.md) shall [yare'](../../strongs/h/h3372.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), and all the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md) thy [kabowd](../../strongs/h/h3519.md).

<a name="psalms_102_16"></a>Psalms 102:16

When [Yĕhovah](../../strongs/h/h3068.md) shall [bānâ](../../strongs/h/h1129.md) [Tsiyown](../../strongs/h/h6726.md), he shall [ra'ah](../../strongs/h/h7200.md) in his [kabowd](../../strongs/h/h3519.md).

<a name="psalms_102_17"></a>Psalms 102:17

He will [panah](../../strongs/h/h6437.md) the [tĕphillah](../../strongs/h/h8605.md) of the [ʿarʿōr](../../strongs/h/h6199.md), and not [bazah](../../strongs/h/h959.md) their [tĕphillah](../../strongs/h/h8605.md).

<a name="psalms_102_18"></a>Psalms 102:18

This shall be [kāṯaḇ](../../strongs/h/h3789.md) for the [dôr](../../strongs/h/h1755.md) to ['aḥărôn](../../strongs/h/h314.md): and the ['am](../../strongs/h/h5971.md) which shall be [bara'](../../strongs/h/h1254.md) shall [halal](../../strongs/h/h1984.md) the [Yahh](../../strongs/h/h3050.md).

<a name="psalms_102_19"></a>Psalms 102:19

For he hath [šāqap̄](../../strongs/h/h8259.md) from the [marowm](../../strongs/h/h4791.md) of his [qodesh](../../strongs/h/h6944.md); from [shamayim](../../strongs/h/h8064.md) did [Yĕhovah](../../strongs/h/h3068.md) [nabat](../../strongs/h/h5027.md) the ['erets](../../strongs/h/h776.md);

<a name="psalms_102_20"></a>Psalms 102:20

To [shama'](../../strongs/h/h8085.md) the ['ănāqâ](../../strongs/h/h603.md) of the ['āsîr](../../strongs/h/h615.md); to [pāṯaḥ](../../strongs/h/h6605.md) those that are [ben](../../strongs/h/h1121.md) to [tᵊmûṯâ](../../strongs/h/h8546.md);

<a name="psalms_102_21"></a>Psalms 102:21

To [sāp̄ar](../../strongs/h/h5608.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) in [Tsiyown](../../strongs/h/h6726.md), and his [tehillah](../../strongs/h/h8416.md) in [Yĕruwshalaim](../../strongs/h/h3389.md);

<a name="psalms_102_22"></a>Psalms 102:22

When the ['am](../../strongs/h/h5971.md) are [qāḇaṣ](../../strongs/h/h6908.md) [yaḥaḏ](../../strongs/h/h3162.md), and the [mamlāḵâ](../../strongs/h/h4467.md), to ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_102_23"></a>Psalms 102:23

He [ʿānâ](../../strongs/h/h6031.md) my [koach](../../strongs/h/h3581.md) in the [derek](../../strongs/h/h1870.md); he [qāṣar](../../strongs/h/h7114.md) my [yowm](../../strongs/h/h3117.md).

<a name="psalms_102_24"></a>Psalms 102:24

I ['āmar](../../strongs/h/h559.md), O my ['el](../../strongs/h/h410.md), [ʿālâ](../../strongs/h/h5927.md) me not in the [ḥēṣî](../../strongs/h/h2677.md) of my [yowm](../../strongs/h/h3117.md): thy [šānâ](../../strongs/h/h8141.md) are throughout [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md).

<a name="psalms_102_25"></a>Psalms 102:25

Of [paniym](../../strongs/h/h6440.md) hast thou [yacad](../../strongs/h/h3245.md) the ['erets](../../strongs/h/h776.md): and the [shamayim](../../strongs/h/h8064.md) are the [ma'aseh](../../strongs/h/h4639.md) of thy [yad](../../strongs/h/h3027.md).

<a name="psalms_102_26"></a>Psalms 102:26

They shall ['abad](../../strongs/h/h6.md), but thou shalt ['amad](../../strongs/h/h5975.md): yea, all of them shall [bālâ](../../strongs/h/h1086.md) like a [beḡeḏ](../../strongs/h/h899.md); as a [lᵊḇûš](../../strongs/h/h3830.md) shalt thou [ḥālap̄](../../strongs/h/h2498.md) them, and they shall be [ḥālap̄](../../strongs/h/h2498.md):

<a name="psalms_102_27"></a>Psalms 102:27

But thou art the same, and thy [šānâ](../../strongs/h/h8141.md) shall have no [tamam](../../strongs/h/h8552.md).

<a name="psalms_102_28"></a>Psalms 102:28

The [ben](../../strongs/h/h1121.md) of thy ['ebed](../../strongs/h/h5650.md) shall [shakan](../../strongs/h/h7931.md), and their [zera'](../../strongs/h/h2233.md) shall be [kuwn](../../strongs/h/h3559.md) [paniym](../../strongs/h/h6440.md) thee.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 101](psalms_101.md) - [Psalms 103](psalms_103.md)