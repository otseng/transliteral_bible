# [Psalms 81](https://www.blueletterbible.org/kjv/psalms/81)

<a name="psalms_81_1"></a>Psalms 81:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon [gitîṯ](../../strongs/h/h1665.md), A Psalm of ['Āsāp̄](../../strongs/h/h623.md). Sing [ranan](../../strongs/h/h7442.md) unto ['Elohiym](../../strongs/h/h430.md) our ['oz](../../strongs/h/h5797.md): make a [rûaʿ](../../strongs/h/h7321.md) unto the ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md).

<a name="psalms_81_2"></a>Psalms 81:2

[nasa'](../../strongs/h/h5375.md) a [zimrâ](../../strongs/h/h2172.md), and [nathan](../../strongs/h/h5414.md) hither the [tōp̄](../../strongs/h/h8596.md), the [na'iym](../../strongs/h/h5273.md) [kinnôr](../../strongs/h/h3658.md) with the [neḇel](../../strongs/h/h5035.md).

<a name="psalms_81_3"></a>Psalms 81:3

[tāqaʿ](../../strongs/h/h8628.md) the [šôp̄ār](../../strongs/h/h7782.md) in the [ḥōḏeš](../../strongs/h/h2320.md), in the [kese'](../../strongs/h/h3677.md), on our [ḥāḡ](../../strongs/h/h2282.md) [yowm](../../strongs/h/h3117.md).

<a name="psalms_81_4"></a>Psalms 81:4

For this was a [choq](../../strongs/h/h2706.md) for [Yisra'el](../../strongs/h/h3478.md), and a [mishpat](../../strongs/h/h4941.md) of the ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md).

<a name="psalms_81_5"></a>Psalms 81:5

This he [śûm](../../strongs/h/h7760.md) in [Yᵊhôsēp̄](../../strongs/h/h3084.md) for a [ʿēḏûṯ](../../strongs/h/h5715.md), when he [yāṣā'](../../strongs/h/h3318.md) through the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): where I [shama'](../../strongs/h/h8085.md) a [saphah](../../strongs/h/h8193.md) that I [yada'](../../strongs/h/h3045.md) not.

<a name="psalms_81_6"></a>Psalms 81:6

I [cuwr](../../strongs/h/h5493.md) his [šᵊḵem](../../strongs/h/h7926.md) from the [sēḇel](../../strongs/h/h5447.md): his [kaph](../../strongs/h/h3709.md) were ['abar](../../strongs/h/h5674.md) from the [dûḏ](../../strongs/h/h1731.md).

<a name="psalms_81_7"></a>Psalms 81:7

Thou [qara'](../../strongs/h/h7121.md) in [tsarah](../../strongs/h/h6869.md), and I [chalats](../../strongs/h/h2502.md) thee; I ['anah](../../strongs/h/h6030.md) thee in the [cether](../../strongs/h/h5643.md) of [raʿam](../../strongs/h/h7482.md): I [bachan](../../strongs/h/h974.md) thee at the [mayim](../../strongs/h/h4325.md) of [Mᵊrîḇâ](../../strongs/h/h4809.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_81_8"></a>Psalms 81:8

[shama'](../../strongs/h/h8085.md), O my ['am](../../strongs/h/h5971.md), and I will [ʿûḏ](../../strongs/h/h5749.md) unto thee: O [Yisra'el](../../strongs/h/h3478.md), if thou wilt [shama'](../../strongs/h/h8085.md) unto me;

<a name="psalms_81_9"></a>Psalms 81:9

There shall no [zûr](../../strongs/h/h2114.md) ['el](../../strongs/h/h410.md) be in thee; neither shalt thou [shachah](../../strongs/h/h7812.md) any [nēḵār](../../strongs/h/h5236.md) ['el](../../strongs/h/h410.md).

<a name="psalms_81_10"></a>Psalms 81:10

I am [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), which [ʿālâ](../../strongs/h/h5927.md) thee out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): open thy [peh](../../strongs/h/h6310.md) [rāḥaḇ](../../strongs/h/h7337.md), and I will [mālā'](../../strongs/h/h4390.md) it.

<a name="psalms_81_11"></a>Psalms 81:11

But my ['am](../../strongs/h/h5971.md) would not [shama'](../../strongs/h/h8085.md) to my [qowl](../../strongs/h/h6963.md); and [Yisra'el](../../strongs/h/h3478.md) ['āḇâ](../../strongs/h/h14.md) none of me.

<a name="psalms_81_12"></a>Psalms 81:12

So I gave them [shalach](../../strongs/h/h7971.md) unto their own [leb](../../strongs/h/h3820.md) [šᵊrîrûṯ](../../strongs/h/h8307.md): and they [yālaḵ](../../strongs/h/h3212.md) in their own [mow'etsah](../../strongs/h/h4156.md).

<a name="psalms_81_13"></a>Psalms 81:13

[lû'](../../strongs/h/h3863.md) that my ['am](../../strongs/h/h5971.md) had [shama'](../../strongs/h/h8085.md) unto me, and [Yisra'el](../../strongs/h/h3478.md) had [halak](../../strongs/h/h1980.md) in my [derek](../../strongs/h/h1870.md)!

<a name="psalms_81_14"></a>Psalms 81:14

I should [mᵊʿaṭ](../../strongs/h/h4592.md) have [kānaʿ](../../strongs/h/h3665.md) their ['oyeb](../../strongs/h/h341.md), and [shuwb](../../strongs/h/h7725.md) my [yad](../../strongs/h/h3027.md) against their [tsar](../../strongs/h/h6862.md).

<a name="psalms_81_15"></a>Psalms 81:15

The [sane'](../../strongs/h/h8130.md) of [Yĕhovah](../../strongs/h/h3068.md) should have [kāḥaš](../../strongs/h/h3584.md) themselves unto him: but their [ʿēṯ](../../strongs/h/h6256.md) should have endured for ['owlam](../../strongs/h/h5769.md).

<a name="psalms_81_16"></a>Psalms 81:16

He should have ['akal](../../strongs/h/h398.md) them also with the [cheleb](../../strongs/h/h2459.md) of the [ḥiṭṭâ](../../strongs/h/h2406.md): and with [dĕbash](../../strongs/h/h1706.md) out of the [tsuwr](../../strongs/h/h6697.md) should I have [sāׂbaʿ](../../strongs/h/h7646.md) thee.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 80](psalms_80.md) - [Psalms 82](psalms_82.md)