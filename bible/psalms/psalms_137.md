# [Psalms 137](https://www.blueletterbible.org/kjv/psalms/137)

<a name="psalms_137_1"></a>Psalms 137:1

By the [nāhār](../../strongs/h/h5104.md) of [Bāḇel](../../strongs/h/h894.md), there we [yashab](../../strongs/h/h3427.md), yea, we [bāḵâ](../../strongs/h/h1058.md), when we [zakar](../../strongs/h/h2142.md) [Tsiyown](../../strongs/h/h6726.md).

<a name="psalms_137_2"></a>Psalms 137:2

We [tālâ](../../strongs/h/h8518.md) our [kinnôr](../../strongs/h/h3658.md) upon the [ʿărāḇâ](../../strongs/h/h6155.md) in the [tavek](../../strongs/h/h8432.md) thereof.

<a name="psalms_137_3"></a>Psalms 137:3

For there they that [šāḇâ](../../strongs/h/h7617.md) us [sha'al](../../strongs/h/h7592.md) of us a [dabar](../../strongs/h/h1697.md) [šîr](../../strongs/h/h7892.md); and they that [tôlāl](../../strongs/h/h8437.md) us required of us [simchah](../../strongs/h/h8057.md), saying, [shiyr](../../strongs/h/h7891.md) us one of the [šîr](../../strongs/h/h7892.md) of [Tsiyown](../../strongs/h/h6726.md).

<a name="psalms_137_4"></a>Psalms 137:4

How shall we [shiyr](../../strongs/h/h7891.md) [Yĕhovah](../../strongs/h/h3068.md) [šîr](../../strongs/h/h7892.md) in a [nēḵār](../../strongs/h/h5236.md) ['ăḏāmâ](../../strongs/h/h127.md)?

<a name="psalms_137_5"></a>Psalms 137:5

If I [shakach](../../strongs/h/h7911.md) thee, [Yĕruwshalaim](../../strongs/h/h3389.md), let my [yamiyn](../../strongs/h/h3225.md) [shakach](../../strongs/h/h7911.md) her cunning.

<a name="psalms_137_6"></a>Psalms 137:6

If I do not [zakar](../../strongs/h/h2142.md) thee, let my [lashown](../../strongs/h/h3956.md) [dāḇaq](../../strongs/h/h1692.md) to the roof of my [ḥēḵ](../../strongs/h/h2441.md); if I [ʿālâ](../../strongs/h/h5927.md) not [Yĕruwshalaim](../../strongs/h/h3389.md) above my [ro'sh](../../strongs/h/h7218.md) [simchah](../../strongs/h/h8057.md).

<a name="psalms_137_7"></a>Psalms 137:7

[zakar](../../strongs/h/h2142.md), [Yĕhovah](../../strongs/h/h3068.md), the [ben](../../strongs/h/h1121.md) of ['Ĕḏōm](../../strongs/h/h123.md) in the [yowm](../../strongs/h/h3117.md) of [Yĕruwshalaim](../../strongs/h/h3389.md); who ['āmar](../../strongs/h/h559.md), [ʿārâ](../../strongs/h/h6168.md) it, [ʿārâ](../../strongs/h/h6168.md) it, even to the [yᵊsôḏ](../../strongs/h/h3247.md) thereof.

<a name="psalms_137_8"></a>Psalms 137:8

[bath](../../strongs/h/h1323.md) of [Bāḇel](../../strongs/h/h894.md), who art to be [shadad](../../strongs/h/h7703.md); ['esher](../../strongs/h/h835.md) shall he be, that [shalam](../../strongs/h/h7999.md) thee as thou hast [gamal](../../strongs/h/h1580.md) [gĕmwl](../../strongs/h/h1576.md) us.

<a name="psalms_137_9"></a>Psalms 137:9

['esher](../../strongs/h/h835.md) shall he be, that ['āḥaz](../../strongs/h/h270.md) and [naphats](../../strongs/h/h5310.md) thy little ['owlel](../../strongs/h/h5768.md) against the [cela'](../../strongs/h/h5553.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 136](psalms_136.md) - [Psalms 138](psalms_138.md)