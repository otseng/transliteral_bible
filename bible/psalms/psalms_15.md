# [Psalms 15](https://www.blueletterbible.org/kjv/psa/15/1/s_493001)

<a name="psalms_15_1"></a>Psalms 15:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [Yĕhovah](../../strongs/h/h3068.md), who shall [guwr](../../strongs/h/h1481.md) in thy ['ohel](../../strongs/h/h168.md)? who shall [shakan](../../strongs/h/h7931.md) in thy [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md)?

<a name="psalms_15_2"></a>Psalms 15:2

He that [halak](../../strongs/h/h1980.md) [tamiym](../../strongs/h/h8549.md), and worketh [tsedeq](../../strongs/h/h6664.md), and [dabar](../../strongs/h/h1696.md) the ['emeth](../../strongs/h/h571.md) in his [lebab](../../strongs/h/h3824.md).

<a name="psalms_15_3"></a>Psalms 15:3

He that [ragal](../../strongs/h/h7270.md) not with his [lashown](../../strongs/h/h3956.md), nor ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) to his [rea'](../../strongs/h/h7453.md), nor [nasa'](../../strongs/h/h5375.md) a [cherpah](../../strongs/h/h2781.md) against his [qarowb](../../strongs/h/h7138.md).

<a name="psalms_15_4"></a>Psalms 15:4

In whose ['ayin](../../strongs/h/h5869.md) a [mā'as](../../strongs/h/h3988.md) is [bazah](../../strongs/h/h959.md); but he [kabad](../../strongs/h/h3513.md) them that [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md). He that [shaba'](../../strongs/h/h7650.md) to his own [ra'a'](../../strongs/h/h7489.md), and [mûr](../../strongs/h/h4171.md) not.

<a name="psalms_15_5"></a>Psalms 15:5

He that [nathan](../../strongs/h/h5414.md) not his [keceph](../../strongs/h/h3701.md) to [neshek](../../strongs/h/h5392.md), nor [laqach](../../strongs/h/h3947.md) [shachad](../../strongs/h/h7810.md) against the [naqiy](../../strongs/h/h5355.md). He that ['asah](../../strongs/h/h6213.md) these things shall never be [mowt](../../strongs/h/h4131.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 14](psalms_14.md) - [Psalms 16](psalms_16.md)