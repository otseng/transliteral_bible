# [Psalms 91](https://www.blueletterbible.org/kjv/psalms/91)

<a name="psalms_91_1"></a>Psalms 91:1

He that [yashab](../../strongs/h/h3427.md) in the [cether](../../strongs/h/h5643.md) place of the ['elyown](../../strongs/h/h5945.md) shall [lûn](../../strongs/h/h3885.md) under the [ṣēl](../../strongs/h/h6738.md) of the [Šaday](../../strongs/h/h7706.md).

<a name="psalms_91_2"></a>Psalms 91:2

I will ['āmar](../../strongs/h/h559.md) of [Yĕhovah](../../strongs/h/h3068.md), He is my [machaceh](../../strongs/h/h4268.md) and my [matsuwd](../../strongs/h/h4686.md): my ['Elohiym](../../strongs/h/h430.md); in him will I [batach](../../strongs/h/h982.md).

<a name="psalms_91_3"></a>Psalms 91:3

Surely he shall [natsal](../../strongs/h/h5337.md) thee from the [paḥ](../../strongs/h/h6341.md) of the [yāqôš](../../strongs/h/h3353.md), and from the [havvah](../../strongs/h/h1942.md) [deḇer](../../strongs/h/h1698.md).

<a name="psalms_91_4"></a>Psalms 91:4

He shall [cakak](../../strongs/h/h5526.md) thee with his ['ēḇrâ](../../strongs/h/h84.md), and under his [kanaph](../../strongs/h/h3671.md) shalt thou [chacah](../../strongs/h/h2620.md): his ['emeth](../../strongs/h/h571.md) shall be thy [tsinnah](../../strongs/h/h6793.md) and [sōḥērâ](../../strongs/h/h5507.md).

<a name="psalms_91_5"></a>Psalms 91:5

Thou shalt not be [yare'](../../strongs/h/h3372.md) for the [paḥaḏ](../../strongs/h/h6343.md) by [layil](../../strongs/h/h3915.md); nor for the [chets](../../strongs/h/h2671.md) that ['uwph](../../strongs/h/h5774.md) by [yômām](../../strongs/h/h3119.md);

<a name="psalms_91_6"></a>Psalms 91:6

Nor for the [deḇer](../../strongs/h/h1698.md) that [halak](../../strongs/h/h1980.md) in ['ōp̄el](../../strongs/h/h652.md); nor for the [qeṭeḇ](../../strongs/h/h6986.md) that [šûḏ](../../strongs/h/h7736.md) at [ṣōhar](../../strongs/h/h6672.md).

<a name="psalms_91_7"></a>Psalms 91:7

An ['elep̄](../../strongs/h/h505.md) shall [naphal](../../strongs/h/h5307.md) at thy [ṣaḏ](../../strongs/h/h6654.md), and ten [rᵊḇāḇâ](../../strongs/h/h7233.md) at thy [yamiyn](../../strongs/h/h3225.md); but it shall not come [nāḡaš](../../strongs/h/h5066.md) thee.

<a name="psalms_91_8"></a>Psalms 91:8

Only with thine ['ayin](../../strongs/h/h5869.md) shalt thou [nabat](../../strongs/h/h5027.md) and [ra'ah](../../strongs/h/h7200.md) the [šillumâ](../../strongs/h/h8011.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="psalms_91_9"></a>Psalms 91:9

Because thou hast [śûm](../../strongs/h/h7760.md) [Yĕhovah](../../strongs/h/h3068.md), which is my [machaceh](../../strongs/h/h4268.md), even the ['elyown](../../strongs/h/h5945.md), thy [māʿôn](../../strongs/h/h4583.md);

<a name="psalms_91_10"></a>Psalms 91:10

There shall no [ra'](../../strongs/h/h7451.md) ['ānâ](../../strongs/h/h579.md) thee, neither shall any [neḡaʿ](../../strongs/h/h5061.md) [qāraḇ](../../strongs/h/h7126.md) thy ['ohel](../../strongs/h/h168.md).

<a name="psalms_91_11"></a>Psalms 91:11

For he shall give his [mal'ak](../../strongs/h/h4397.md) [tsavah](../../strongs/h/h6680.md) over thee, to [shamar](../../strongs/h/h8104.md) thee in all thy [derek](../../strongs/h/h1870.md).

<a name="psalms_91_12"></a>Psalms 91:12

They shall [nasa'](../../strongs/h/h5375.md) thee in their [kaph](../../strongs/h/h3709.md), lest thou [nāḡap̄](../../strongs/h/h5062.md) thy [regel](../../strongs/h/h7272.md) against an ['eben](../../strongs/h/h68.md).

<a name="psalms_91_13"></a>Psalms 91:13

Thou shalt [dāraḵ](../../strongs/h/h1869.md) upon the [šāḥal](../../strongs/h/h7826.md) and [peṯen](../../strongs/h/h6620.md): the [kephiyr](../../strongs/h/h3715.md) and the [tannîn](../../strongs/h/h8577.md) shalt thou [rāmas](../../strongs/h/h7429.md).

<a name="psalms_91_14"></a>Psalms 91:14

Because he hath set his [ḥāšaq](../../strongs/h/h2836.md) upon me, therefore will I [palat](../../strongs/h/h6403.md) him: I will set him on [śāḡaḇ](../../strongs/h/h7682.md), because he hath [yada'](../../strongs/h/h3045.md) my [shem](../../strongs/h/h8034.md).

<a name="psalms_91_15"></a>Psalms 91:15

He shall [qara'](../../strongs/h/h7121.md) upon me, and I will ['anah](../../strongs/h/h6030.md) him: I will be with him in [tsarah](../../strongs/h/h6869.md); I will [chalats](../../strongs/h/h2502.md) him, and [kabad](../../strongs/h/h3513.md) him.

<a name="psalms_91_16"></a>Psalms 91:16

With ['ōreḵ](../../strongs/h/h753.md) [yowm](../../strongs/h/h3117.md) will I [sāׂbaʿ](../../strongs/h/h7646.md) him, and [ra'ah](../../strongs/h/h7200.md) him my [yĕshuw'ah](../../strongs/h/h3444.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 90](psalms_90.md) - [Psalms 92](psalms_92.md)