# [Psalms 98](https://www.blueletterbible.org/kjv/psalms/98)

<a name="psalms_98_1"></a>Psalms 98:1

A [mizmôr](../../strongs/h/h4210.md). [shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md) a [ḥāḏāš](../../strongs/h/h2319.md) [šîr](../../strongs/h/h7892.md); for he hath ['asah](../../strongs/h/h6213.md) [pala'](../../strongs/h/h6381.md): his [yamiyn](../../strongs/h/h3225.md), and his [qodesh](../../strongs/h/h6944.md) [zerowa'](../../strongs/h/h2220.md), hath gotten him the [yasha'](../../strongs/h/h3467.md).

<a name="psalms_98_2"></a>Psalms 98:2

[Yĕhovah](../../strongs/h/h3068.md) hath [yada'](../../strongs/h/h3045.md) his [yĕshuw'ah](../../strongs/h/h3444.md): his [tsedaqah](../../strongs/h/h6666.md) hath he [gālâ](../../strongs/h/h1540.md) in the ['ayin](../../strongs/h/h5869.md) of the [gowy](../../strongs/h/h1471.md).

<a name="psalms_98_3"></a>Psalms 98:3

He hath [zakar](../../strongs/h/h2142.md) his [checed](../../strongs/h/h2617.md) and his ['ĕmûnâ](../../strongs/h/h530.md) toward the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md): all the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md) have [ra'ah](../../strongs/h/h7200.md) the [yĕshuw'ah](../../strongs/h/h3444.md) of our ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_98_4"></a>Psalms 98:4

Make a [rûaʿ](../../strongs/h/h7321.md) unto [Yĕhovah](../../strongs/h/h3068.md), all the ['erets](../../strongs/h/h776.md): make a [pāṣaḥ](../../strongs/h/h6476.md), and [ranan](../../strongs/h/h7442.md), and [zamar](../../strongs/h/h2167.md) praise.

<a name="psalms_98_5"></a>Psalms 98:5

[zamar](../../strongs/h/h2167.md) unto [Yĕhovah](../../strongs/h/h3068.md) with the [kinnôr](../../strongs/h/h3658.md); with the [kinnôr](../../strongs/h/h3658.md), and the [qowl](../../strongs/h/h6963.md) of a [zimrâ](../../strongs/h/h2172.md).

<a name="psalms_98_6"></a>Psalms 98:6

With [ḥăṣōṣrâ](../../strongs/h/h2689.md) and [qowl](../../strongs/h/h6963.md) of [šôp̄ār](../../strongs/h/h7782.md) make a [rûaʿ](../../strongs/h/h7321.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), the [melek](../../strongs/h/h4428.md).

<a name="psalms_98_7"></a>Psalms 98:7

Let the [yam](../../strongs/h/h3220.md) [ra'am](../../strongs/h/h7481.md), and the [mᵊlō'](../../strongs/h/h4393.md) thereof; the [tebel](../../strongs/h/h8398.md), and they that [yashab](../../strongs/h/h3427.md) therein.

<a name="psalms_98_8"></a>Psalms 98:8

Let the [nāhār](../../strongs/h/h5104.md) [māḥā'](../../strongs/h/h4222.md) their [kaph](../../strongs/h/h3709.md): let the [har](../../strongs/h/h2022.md) be [ranan](../../strongs/h/h7442.md) [yaḥaḏ](../../strongs/h/h3162.md)

<a name="psalms_98_9"></a>Psalms 98:9

[paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); for he [bow'](../../strongs/h/h935.md) to [shaphat](../../strongs/h/h8199.md) the ['erets](../../strongs/h/h776.md): with [tsedeq](../../strongs/h/h6664.md) shall he [shaphat](../../strongs/h/h8199.md) the [tebel](../../strongs/h/h8398.md), and the ['am](../../strongs/h/h5971.md) with [meyshar](../../strongs/h/h4339.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 97](psalms_97.md) - [Psalms 99](psalms_99.md)