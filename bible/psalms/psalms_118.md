# [Psalms 118](https://www.blueletterbible.org/kjv/psalms/118)

<a name="psalms_118_1"></a>Psalms 118:1

[yadah](../../strongs/h/h3034.md) unto [Yĕhovah](../../strongs/h/h3068.md); for he is [towb](../../strongs/h/h2896.md): because his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_118_2"></a>Psalms 118:2

Let [Yisra'el](../../strongs/h/h3478.md) now ['āmar](../../strongs/h/h559.md), that his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_118_3"></a>Psalms 118:3

Let the [bayith](../../strongs/h/h1004.md) of ['Ahărôn](../../strongs/h/h175.md) now ['āmar](../../strongs/h/h559.md), that his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_118_4"></a>Psalms 118:4

Let them now that [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), that his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_118_5"></a>Psalms 118:5

I [qara'](../../strongs/h/h7121.md) upon the [Yahh](../../strongs/h/h3050.md) in [mēṣar](../../strongs/h/h4712.md): the [Yahh](../../strongs/h/h3050.md) ['anah](../../strongs/h/h6030.md) me, and set me in a [merḥāḇ](../../strongs/h/h4800.md).

<a name="psalms_118_6"></a>Psalms 118:6

[Yĕhovah](../../strongs/h/h3068.md) is on my side; I will not [yare'](../../strongs/h/h3372.md): what ['asah](../../strongs/h/h6213.md) ['āḏām](../../strongs/h/h120.md) ['asah](../../strongs/h/h6213.md) unto me?

<a name="psalms_118_7"></a>Psalms 118:7

[Yĕhovah](../../strongs/h/h3068.md) taketh my part with them that [ʿāzar](../../strongs/h/h5826.md) me: therefore shall I [ra'ah](../../strongs/h/h7200.md) my desire upon them that [sane'](../../strongs/h/h8130.md) me.

<a name="psalms_118_8"></a>Psalms 118:8

It is [towb](../../strongs/h/h2896.md) to [chacah](../../strongs/h/h2620.md) in [Yĕhovah](../../strongs/h/h3068.md) than to put [batach](../../strongs/h/h982.md) in ['āḏām](../../strongs/h/h120.md).

<a name="psalms_118_9"></a>Psalms 118:9

It is [towb](../../strongs/h/h2896.md) to [chacah](../../strongs/h/h2620.md) in [Yĕhovah](../../strongs/h/h3068.md) than to put [batach](../../strongs/h/h982.md) in [nāḏîḇ](../../strongs/h/h5081.md).

<a name="psalms_118_10"></a>Psalms 118:10

All [gowy](../../strongs/h/h1471.md) [cabab](../../strongs/h/h5437.md) me: but in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) will I [muwl](../../strongs/h/h4135.md) them.

<a name="psalms_118_11"></a>Psalms 118:11

They [cabab](../../strongs/h/h5437.md) me; yea, they [cabab](../../strongs/h/h5437.md) me: but in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) I will [muwl](../../strongs/h/h4135.md) them.

<a name="psalms_118_12"></a>Psalms 118:12

They [cabab](../../strongs/h/h5437.md) me like [dᵊḇôrâ](../../strongs/h/h1682.md); they are [dāʿaḵ](../../strongs/h/h1846.md) as the ['esh](../../strongs/h/h784.md) of [qowts](../../strongs/h/h6975.md): for in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) I will [muwl](../../strongs/h/h4135.md) them.

<a name="psalms_118_13"></a>Psalms 118:13

Thou hast [dāḥâ](../../strongs/h/h1760.md) [dāḥâ](../../strongs/h/h1760.md) at me that I might [naphal](../../strongs/h/h5307.md): but [Yĕhovah](../../strongs/h/h3068.md) [ʿāzar](../../strongs/h/h5826.md) me.

<a name="psalms_118_14"></a>Psalms 118:14

The [Yahh](../../strongs/h/h3050.md) is my ['oz](../../strongs/h/h5797.md) and [zimrāṯ](../../strongs/h/h2176.md), and is become my [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="psalms_118_15"></a>Psalms 118:15

The [qowl](../../strongs/h/h6963.md) of [rinnah](../../strongs/h/h7440.md) and [yĕshuw'ah](../../strongs/h/h3444.md) is in the ['ohel](../../strongs/h/h168.md) of the [tsaddiyq](../../strongs/h/h6662.md): the [yamiyn](../../strongs/h/h3225.md) of [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) [ḥayil](../../strongs/h/h2428.md).

<a name="psalms_118_16"></a>Psalms 118:16

The [yamiyn](../../strongs/h/h3225.md) of [Yĕhovah](../../strongs/h/h3068.md) is [rāmam](../../strongs/h/h7426.md): the [yamiyn](../../strongs/h/h3225.md) of [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) [ḥayil](../../strongs/h/h2428.md).

<a name="psalms_118_17"></a>Psalms 118:17

I shall not [muwth](../../strongs/h/h4191.md), but [ḥāyâ](../../strongs/h/h2421.md), and [sāp̄ar](../../strongs/h/h5608.md) the [ma'aseh](../../strongs/h/h4639.md) of the [Yahh](../../strongs/h/h3050.md).

<a name="psalms_118_18"></a>Psalms 118:18

The [Yahh](../../strongs/h/h3050.md) hath [yacar](../../strongs/h/h3256.md) me [yacar](../../strongs/h/h3256.md): but he hath not [nathan](../../strongs/h/h5414.md) me unto [maveth](../../strongs/h/h4194.md).

<a name="psalms_118_19"></a>Psalms 118:19

[pāṯaḥ](../../strongs/h/h6605.md) to me the [sha'ar](../../strongs/h/h8179.md) of [tsedeq](../../strongs/h/h6664.md): I will [bow'](../../strongs/h/h935.md) into them, and I will [yadah](../../strongs/h/h3034.md) the [Yahh](../../strongs/h/h3050.md):

<a name="psalms_118_20"></a>Psalms 118:20

This [sha'ar](../../strongs/h/h8179.md) of [Yĕhovah](../../strongs/h/h3068.md), into which the [tsaddiyq](../../strongs/h/h6662.md) shall [bow'](../../strongs/h/h935.md).

<a name="psalms_118_21"></a>Psalms 118:21

I will [yadah](../../strongs/h/h3034.md) thee: for thou hast ['anah](../../strongs/h/h6030.md) me, and art become my [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="psalms_118_22"></a>Psalms 118:22

The ['eben](../../strongs/h/h68.md) which the [bānâ](../../strongs/h/h1129.md) [mā'as](../../strongs/h/h3988.md) is become the [ro'sh](../../strongs/h/h7218.md) of the [pinnâ](../../strongs/h/h6438.md).

<a name="psalms_118_23"></a>Psalms 118:23

This is [Yĕhovah](../../strongs/h/h3068.md) doing; it is [pala'](../../strongs/h/h6381.md) in our ['ayin](../../strongs/h/h5869.md).

<a name="psalms_118_24"></a>Psalms 118:24

This is the [yowm](../../strongs/h/h3117.md) which [Yĕhovah](../../strongs/h/h3068.md) hath ['asah](../../strongs/h/h6213.md); we will [giyl](../../strongs/h/h1523.md) and be [samach](../../strongs/h/h8055.md) in it.

<a name="psalms_118_25"></a>Psalms 118:25

[yasha'](../../strongs/h/h3467.md) now, I ['ānnā'](../../strongs/h/h577.md) thee, [Yĕhovah](../../strongs/h/h3068.md): [Yĕhovah](../../strongs/h/h3068.md), I ['ānnā'](../../strongs/h/h577.md) thee, send now [tsalach](../../strongs/h/h6743.md).

<a name="psalms_118_26"></a>Psalms 118:26

[barak](../../strongs/h/h1288.md) be he that [bow'](../../strongs/h/h935.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md): we have [barak](../../strongs/h/h1288.md) you out of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_118_27"></a>Psalms 118:27

['el](../../strongs/h/h410.md) is [Yĕhovah](../../strongs/h/h3068.md), which hath shewed us ['owr](../../strongs/h/h215.md): ['āsar](../../strongs/h/h631.md) the [ḥāḡ](../../strongs/h/h2282.md) with [ʿăḇōṯ](../../strongs/h/h5688.md), even unto the [qeren](../../strongs/h/h7161.md) of the [mizbeach](../../strongs/h/h4196.md).

<a name="psalms_118_28"></a>Psalms 118:28

Thou art my ['el](../../strongs/h/h410.md), and I will [yadah](../../strongs/h/h3034.md) thee: thou art my ['Elohiym](../../strongs/h/h430.md), I will [ruwm](../../strongs/h/h7311.md) thee.

<a name="psalms_118_29"></a>Psalms 118:29

[yadah](../../strongs/h/h3034.md) unto [Yĕhovah](../../strongs/h/h3068.md); for he is [towb](../../strongs/h/h2896.md): for his [checed](../../strongs/h/h2617.md) ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 117](psalms_117.md) - [Psalms 119](psalms_119.md)