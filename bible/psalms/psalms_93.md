# [Psalms 93](https://www.blueletterbible.org/kjv/psalms/93)

<a name="psalms_93_1"></a>Psalms 93:1

[Yĕhovah](../../strongs/h/h3068.md) [mālaḵ](../../strongs/h/h4427.md), he is [labash](../../strongs/h/h3847.md) with [ge'uwth](../../strongs/h/h1348.md); [Yĕhovah](../../strongs/h/h3068.md) is [labash](../../strongs/h/h3847.md) with ['oz](../../strongs/h/h5797.md), wherewith he hath ['āzar](../../strongs/h/h247.md) himself: the [tebel](../../strongs/h/h8398.md) also is [kuwn](../../strongs/h/h3559.md), that it cannot be [mowt](../../strongs/h/h4131.md).

<a name="psalms_93_2"></a>Psalms 93:2

Thy [kicce'](../../strongs/h/h3678.md) is [kuwn](../../strongs/h/h3559.md) of ['āz](../../strongs/h/h227.md): thou art from ['owlam](../../strongs/h/h5769.md).

<a name="psalms_93_3"></a>Psalms 93:3

The [nāhār](../../strongs/h/h5104.md) have [nasa'](../../strongs/h/h5375.md), [Yĕhovah](../../strongs/h/h3068.md), the [nāhār](../../strongs/h/h5104.md) have [nasa'](../../strongs/h/h5375.md) their [qowl](../../strongs/h/h6963.md); the [nāhār](../../strongs/h/h5104.md) [nasa'](../../strongs/h/h5375.md) their [dŏḵî](../../strongs/h/h1796.md).

<a name="psalms_93_4"></a>Psalms 93:4

[Yĕhovah](../../strongs/h/h3068.md) on [marowm](../../strongs/h/h4791.md) is ['addiyr](../../strongs/h/h117.md) than the [qowl](../../strongs/h/h6963.md) of [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md), yea, than the ['addiyr](../../strongs/h/h117.md) [mišbār](../../strongs/h/h4867.md) of the [yam](../../strongs/h/h3220.md).

<a name="psalms_93_5"></a>Psalms 93:5

Thy [ʿēḏâ](../../strongs/h/h5713.md) are [me'od](../../strongs/h/h3966.md) ['aman](../../strongs/h/h539.md): [qodesh](../../strongs/h/h6944.md) [nā'â](../../strongs/h/h4998.md) thine [bayith](../../strongs/h/h1004.md), [Yĕhovah](../../strongs/h/h3068.md), for ['ōreḵ](../../strongs/h/h753.md) [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 92](psalms_92.md) - [Psalms 94](psalms_94.md)