# [Psalms 74](https://www.blueletterbible.org/kjv/psa/74)

<a name="psalms_74_1"></a>Psalms 74:1

[Maśkîl](../../strongs/h/h4905.md) of ['Āsāp̄](../../strongs/h/h623.md). ['Elohiym](../../strongs/h/h430.md), why hast thou [zānaḥ](../../strongs/h/h2186.md) us for [netsach](../../strongs/h/h5331.md) ? why doth thine ['aph](../../strongs/h/h639.md) [ʿāšēn](../../strongs/h/h6225.md) against the [tso'n](../../strongs/h/h6629.md) of thy [marʿîṯ](../../strongs/h/h4830.md) ?

<a name="psalms_74_2"></a>Psalms 74:2

[zakar](../../strongs/h/h2142.md) thy ['edah](../../strongs/h/h5712.md), which thou hast [qānâ](../../strongs/h/h7069.md) of [qeḏem](../../strongs/h/h6924.md); the [shebet](../../strongs/h/h7626.md) of thine [nachalah](../../strongs/h/h5159.md), which thou hast [gā'al](../../strongs/h/h1350.md); this [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md), wherein thou hast [shakan](../../strongs/h/h7931.md).

<a name="psalms_74_3"></a>Psalms 74:3

[ruwm](../../strongs/h/h7311.md) thy [pa'am](../../strongs/h/h6471.md) unto the [netsach](../../strongs/h/h5331.md) [maššû'â](../../strongs/h/h4876.md); even all that the ['oyeb](../../strongs/h/h341.md) hath done [ra'a'](../../strongs/h/h7489.md) in the [qodesh](../../strongs/h/h6944.md).

<a name="psalms_74_4"></a>Psalms 74:4

Thine [tsarar](../../strongs/h/h6887.md) [šā'aḡ](../../strongs/h/h7580.md) in the [qereḇ](../../strongs/h/h7130.md) of thy [môʿēḏ](../../strongs/h/h4150.md); they [śûm](../../strongs/h/h7760.md) their ['ôṯ](../../strongs/h/h226.md) for ['ôṯ](../../strongs/h/h226.md).

<a name="psalms_74_5"></a>Psalms 74:5

A man was [yada'](../../strongs/h/h3045.md) according as he had [bow'](../../strongs/h/h935.md) [maʿal](../../strongs/h/h4605.md) [qardōm](../../strongs/h/h7134.md) upon the [sᵊḇāḵ](../../strongs/h/h5442.md) ['ets](../../strongs/h/h6086.md).

<a name="psalms_74_6"></a>Psalms 74:6

But now they [hālam](../../strongs/h/h1986.md) the [pitûaḥ](../../strongs/h/h6603.md) thereof at [yaḥaḏ](../../strongs/h/h3162.md) with [kaššîl](../../strongs/h/h3781.md) and [kêlapôṯ](../../strongs/h/h3597.md).

<a name="psalms_74_7"></a>Psalms 74:7

They have [shalach](../../strongs/h/h7971.md) ['esh](../../strongs/h/h784.md) into thy [miqdash](../../strongs/h/h4720.md), they have [ḥālal](../../strongs/h/h2490.md) by casting down the [miškān](../../strongs/h/h4908.md) of thy [shem](../../strongs/h/h8034.md) to the ['erets](../../strongs/h/h776.md).

<a name="psalms_74_8"></a>Psalms 74:8

They ['āmar](../../strongs/h/h559.md) in their [leb](../../strongs/h/h3820.md), Let us [yānâ](../../strongs/h/h3238.md) them [yaḥaḏ](../../strongs/h/h3162.md): they have [śārap̄](../../strongs/h/h8313.md) all the [môʿēḏ](../../strongs/h/h4150.md) of ['el](../../strongs/h/h410.md) in the ['erets](../../strongs/h/h776.md).

<a name="psalms_74_9"></a>Psalms 74:9

We [ra'ah](../../strongs/h/h7200.md) not our ['ôṯ](../../strongs/h/h226.md): there is no more any [nāḇî'](../../strongs/h/h5030.md): neither is there among us any that [yada'](../../strongs/h/h3045.md) how [ʿaḏ](../../strongs/h/h5704.md).

<a name="psalms_74_10"></a>Psalms 74:10

['Elohiym](../../strongs/h/h430.md), how long shall the [tsar](../../strongs/h/h6862.md) [ḥārap̄](../../strongs/h/h2778.md) ? shall the ['oyeb](../../strongs/h/h341.md) [na'ats](../../strongs/h/h5006.md) thy [shem](../../strongs/h/h8034.md) for [netsach](../../strongs/h/h5331.md) ?

<a name="psalms_74_11"></a>Psalms 74:11

Why [shuwb](../../strongs/h/h7725.md) thou thy [yad](../../strongs/h/h3027.md), even thy [yamiyn](../../strongs/h/h3225.md) ? [kalah](../../strongs/h/h3615.md) it out [qereḇ](../../strongs/h/h7130.md) thy [ḥêq](../../strongs/h/h2436.md) [ḥêq](../../strongs/h/h2436.md).

<a name="psalms_74_12"></a>Psalms 74:12

For ['Elohiym](../../strongs/h/h430.md) is my [melek](../../strongs/h/h4428.md) of [qeḏem](../../strongs/h/h6924.md), [pa'al](../../strongs/h/h6466.md) [yĕshuw'ah](../../strongs/h/h3444.md) in the [qereḇ](../../strongs/h/h7130.md) of the ['erets](../../strongs/h/h776.md).

<a name="psalms_74_13"></a>Psalms 74:13

Thou didst [pārar](../../strongs/h/h6565.md) the [yam](../../strongs/h/h3220.md) by thy ['oz](../../strongs/h/h5797.md): thou [shabar](../../strongs/h/h7665.md) the [ro'sh](../../strongs/h/h7218.md) of the [tannîn](../../strongs/h/h8577.md) in the [mayim](../../strongs/h/h4325.md).

<a name="psalms_74_14"></a>Psalms 74:14

Thou [rāṣaṣ](../../strongs/h/h7533.md) the [ro'sh](../../strongs/h/h7218.md) of [livyāṯān](../../strongs/h/h3882.md) in pieces, and [nathan](../../strongs/h/h5414.md) him to be [ma'akal](../../strongs/h/h3978.md) to the ['am](../../strongs/h/h5971.md) inhabiting the [ṣîyî](../../strongs/h/h6728.md).

<a name="psalms_74_15"></a>Psalms 74:15

Thou didst [bāqaʿ](../../strongs/h/h1234.md) the [maʿyān](../../strongs/h/h4599.md) and the [nachal](../../strongs/h/h5158.md): thou [yāḇēš](../../strongs/h/h3001.md) ['êṯān](../../strongs/h/h386.md) [nāhār](../../strongs/h/h5104.md).

<a name="psalms_74_16"></a>Psalms 74:16

The [yowm](../../strongs/h/h3117.md) is thine, the [layil](../../strongs/h/h3915.md) also is thine: thou hast [kuwn](../../strongs/h/h3559.md) the [ma'owr](../../strongs/h/h3974.md) and the [šemeš](../../strongs/h/h8121.md).

<a name="psalms_74_17"></a>Psalms 74:17

Thou hast [nāṣaḇ](../../strongs/h/h5324.md) all the [gᵊḇûlâ](../../strongs/h/h1367.md) of the ['erets](../../strongs/h/h776.md): thou hast [yāṣar](../../strongs/h/h3335.md) [qayiṣ](../../strongs/h/h7019.md) and [ḥōrep̄](../../strongs/h/h2779.md).

<a name="psalms_74_18"></a>Psalms 74:18

[zakar](../../strongs/h/h2142.md) this, that the ['oyeb](../../strongs/h/h341.md) hath [ḥārap̄](../../strongs/h/h2778.md), [Yĕhovah](../../strongs/h/h3068.md), and that the [nabal](../../strongs/h/h5036.md) ['am](../../strongs/h/h5971.md) have [na'ats](../../strongs/h/h5006.md) thy [shem](../../strongs/h/h8034.md).

<a name="psalms_74_19"></a>Psalms 74:19

[nathan](../../strongs/h/h5414.md) not the [nephesh](../../strongs/h/h5315.md) of thy [tôr](../../strongs/h/h8449.md) unto the [chay](../../strongs/h/h2416.md) of the wicked: [shakach](../../strongs/h/h7911.md) not the [chay](../../strongs/h/h2416.md) of thy ['aniy](../../strongs/h/h6041.md) for [netsach](../../strongs/h/h5331.md).

<a name="psalms_74_20"></a>Psalms 74:20

Have [nabat](../../strongs/h/h5027.md) unto the [bĕriyth](../../strongs/h/h1285.md): for the [maḥšāḵ](../../strongs/h/h4285.md) places of the ['erets](../../strongs/h/h776.md) are [mālā'](../../strongs/h/h4390.md) of the [nā'â](../../strongs/h/h4999.md) of [chamac](../../strongs/h/h2555.md).

<a name="psalms_74_21"></a>Psalms 74:21

O let not the [dak](../../strongs/h/h1790.md) [shuwb](../../strongs/h/h7725.md) [kālam](../../strongs/h/h3637.md): let the ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md) [halal](../../strongs/h/h1984.md) thy [shem](../../strongs/h/h8034.md).

<a name="psalms_74_22"></a>Psalms 74:22

[quwm](../../strongs/h/h6965.md), ['Elohiym](../../strongs/h/h430.md), [riyb](../../strongs/h/h7378.md) thine own [rîḇ](../../strongs/h/h7379.md): [zakar](../../strongs/h/h2142.md) how the [nabal](../../strongs/h/h5036.md) [cherpah](../../strongs/h/h2781.md) thee [yowm](../../strongs/h/h3117.md).

<a name="psalms_74_23"></a>Psalms 74:23

[shakach](../../strongs/h/h7911.md) not the [qowl](../../strongs/h/h6963.md) of thine [tsarar](../../strongs/h/h6887.md): the [shā'ôn](../../strongs/h/h7588.md) of those that [quwm](../../strongs/h/h6965.md) against thee [ʿālâ](../../strongs/h/h5927.md) [tāmîḏ](../../strongs/h/h8548.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 73](psalms_73.md) - [Psalms 75](psalms_75.md)