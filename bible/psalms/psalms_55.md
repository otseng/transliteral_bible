# [Psalms 55](https://www.blueletterbible.org/kjv/psa/55)

<a name="psalms_55_1"></a>Psalms 55:1

Acts 9To the [nāṣaḥ](../../strongs/h/h5329.md) on [Nᵊḡînâ](../../strongs/h/h5058.md), [Maśkîl](../../strongs/h/h4905.md), [A Psalm] of [Dāviḏ](../../strongs/h/h1732.md). ['azan](../../strongs/h/h238.md) to my [tĕphillah](../../strongs/h/h8605.md), ['Elohiym](../../strongs/h/h430.md); and ['alam](../../strongs/h/h5956.md) not thyself from my [tĕchinnah](../../strongs/h/h8467.md).

<a name="psalms_55_2"></a>Psalms 55:2

[qashab](../../strongs/h/h7181.md) unto me, and ['anah](../../strongs/h/h6030.md) me: I [rûḏ](../../strongs/h/h7300.md) in my [śîaḥ](../../strongs/h/h7879.md), and [huwm](../../strongs/h/h1949.md);

<a name="psalms_55_3"></a>Psalms 55:3

Because of the [qowl](../../strongs/h/h6963.md) of the ['oyeb](../../strongs/h/h341.md), because of the [ʿāqâ](../../strongs/h/h6125.md) of the [rasha'](../../strongs/h/h7563.md): for they [mowt](../../strongs/h/h4131.md) ['aven](../../strongs/h/h205.md) upon me, and in ['aph](../../strongs/h/h639.md) they [śāṭam](../../strongs/h/h7852.md) me.

<a name="psalms_55_4"></a>Psalms 55:4

My [leb](../../strongs/h/h3820.md) is [chuwl](../../strongs/h/h2342.md) within me: and the ['êmâ](../../strongs/h/h367.md) of [maveth](../../strongs/h/h4194.md) are [naphal](../../strongs/h/h5307.md) upon me.

<a name="psalms_55_5"></a>Psalms 55:5

[yir'ah](../../strongs/h/h3374.md) and [ra'ad](../../strongs/h/h7461.md) are [bow'](../../strongs/h/h935.md) upon me, and [pallāṣûṯ](../../strongs/h/h6427.md) hath [kāsâ](../../strongs/h/h3680.md) me.

<a name="psalms_55_6"></a>Psalms 55:6

And I ['āmar](../../strongs/h/h559.md), Oh [nathan](../../strongs/h/h5414.md) me ['ēḇer](../../strongs/h/h83.md) like a [yônâ](../../strongs/h/h3123.md) ! for then would I ['uwph](../../strongs/h/h5774.md), and be at [shakan](../../strongs/h/h7931.md).

<a name="psalms_55_7"></a>Psalms 55:7

Lo, then would I [nāḏaḏ](../../strongs/h/h5074.md) [rachaq](../../strongs/h/h7368.md), and [lûn](../../strongs/h/h3885.md) in the [midbar](../../strongs/h/h4057.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_55_8"></a>Psalms 55:8

I would [ḥûš](../../strongs/h/h2363.md) my [mip̄lāṭ](../../strongs/h/h4655.md) from the [ruwach](../../strongs/h/h7307.md) [sāʿâ](../../strongs/h/h5584.md) and [saʿar](../../strongs/h/h5591.md).

<a name="psalms_55_9"></a>Psalms 55:9

[bālaʿ](../../strongs/h/h1104.md), ['adonay](../../strongs/h/h136.md), and [pālaḡ](../../strongs/h/h6385.md) their [lashown](../../strongs/h/h3956.md): for I have [ra'ah](../../strongs/h/h7200.md) [chamac](../../strongs/h/h2555.md) and [rîḇ](../../strongs/h/h7379.md) in the [ʿîr](../../strongs/h/h5892.md).

<a name="psalms_55_10"></a>Psalms 55:10

[yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md) they [cabab](../../strongs/h/h5437.md) it upon the [ḥômâ](../../strongs/h/h2346.md) thereof: ['aven](../../strongs/h/h205.md) also and ['amal](../../strongs/h/h5999.md) are in the [qereḇ](../../strongs/h/h7130.md) of it.

<a name="psalms_55_11"></a>Psalms 55:11

[havvah](../../strongs/h/h1942.md) is in the [qereḇ](../../strongs/h/h7130.md) thereof: [tok](../../strongs/h/h8496.md) and [mirmah](../../strongs/h/h4820.md) [mûš](../../strongs/h/h4185.md) not from her [rᵊḥōḇ](../../strongs/h/h7339.md).

<a name="psalms_55_12"></a>Psalms 55:12

For it was not an ['oyeb](../../strongs/h/h341.md) that [ḥārap̄](../../strongs/h/h2778.md) me; then I could have [nasa'](../../strongs/h/h5375.md) it: neither was it he that [sane'](../../strongs/h/h8130.md) me that did [gāḏal](../../strongs/h/h1431.md) himself against me; then I would have [cathar](../../strongs/h/h5641.md) myself from him:

<a name="psalms_55_13"></a>Psalms 55:13

But it was thou, an ['enowsh](../../strongs/h/h582.md) mine [ʿēreḵ](../../strongs/h/h6187.md), my ['allûp̄](../../strongs/h/h441.md), and mine [yada'](../../strongs/h/h3045.md).

<a name="psalms_55_14"></a>Psalms 55:14

We took [māṯaq](../../strongs/h/h4985.md) [sôḏ](../../strongs/h/h5475.md) [yaḥaḏ](../../strongs/h/h3162.md), and [halak](../../strongs/h/h1980.md) unto the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) in [reḡeš](../../strongs/h/h7285.md).

<a name="psalms_55_15"></a>Psalms 55:15

Let [maveth](../../strongs/h/h4194.md) [nasha'](../../strongs/h/h5377.md) [yᵊšîmâ](../../strongs/h/h3451.md) upon them, and let them [yarad](../../strongs/h/h3381.md) [chay](../../strongs/h/h2416.md) into [shĕ'owl](../../strongs/h/h7585.md): for [ra'](../../strongs/h/h7451.md) is in their [māḡûr](../../strongs/h/h4033.md), and [qereḇ](../../strongs/h/h7130.md) them.

<a name="psalms_55_16"></a>Psalms 55:16

As for me, I will [qara'](../../strongs/h/h7121.md) upon ['Elohiym](../../strongs/h/h430.md); and [Yĕhovah](../../strongs/h/h3068.md) shall [yasha'](../../strongs/h/h3467.md) me.

<a name="psalms_55_17"></a>Psalms 55:17

['ereb](../../strongs/h/h6153.md), and [boqer](../../strongs/h/h1242.md), and at [ṣōhar](../../strongs/h/h6672.md), will I [śîaḥ](../../strongs/h/h7878.md), and [hāmâ](../../strongs/h/h1993.md): and he shall [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md).

<a name="psalms_55_18"></a>Psalms 55:18

He hath [pāḏâ](../../strongs/h/h6299.md) my [nephesh](../../strongs/h/h5315.md) in [shalowm](../../strongs/h/h7965.md) from the [qᵊrāḇ](../../strongs/h/h7128.md) that was against me: for there were [rab](../../strongs/h/h7227.md) with me.

<a name="psalms_55_19"></a>Psalms 55:19

['el](../../strongs/h/h410.md) shall [shama'](../../strongs/h/h8085.md), and ['anah](../../strongs/h/h6030.md) them, even he that [yashab](../../strongs/h/h3427.md) of [qeḏem](../../strongs/h/h6924.md). [Celah](../../strongs/h/h5542.md). Because they have no [ḥălîp̄â](../../strongs/h/h2487.md), therefore they [yare'](../../strongs/h/h3372.md) not ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_55_20"></a>Psalms 55:20

He hath [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) against such as be at [shalowm](../../strongs/h/h7965.md) with him: he hath [ḥālal](../../strongs/h/h2490.md) his [bĕriyth](../../strongs/h/h1285.md).

<a name="psalms_55_21"></a>Psalms 55:21

The words of his [peh](../../strongs/h/h6310.md) were [chalaq](../../strongs/h/h2505.md) than [maḥămā'ôṯ](../../strongs/h/h4260.md), but [qᵊrāḇ](../../strongs/h/h7128.md) was in his [leb](../../strongs/h/h3820.md): his [dabar](../../strongs/h/h1697.md) were [rāḵaḵ](../../strongs/h/h7401.md) than [šemen](../../strongs/h/h8081.md), yet were they [pᵊṯîḥâ](../../strongs/h/h6609.md).

<a name="psalms_55_22"></a>Psalms 55:22

[shalak](../../strongs/h/h7993.md) thy [yᵊhāḇ](../../strongs/h/h3053.md) upon [Yĕhovah](../../strongs/h/h3068.md), and he shall [kûl](../../strongs/h/h3557.md) thee: he shall never [nathan](../../strongs/h/h5414.md) the [tsaddiyq](../../strongs/h/h6662.md) to be [mowt](../../strongs/h/h4131.md).

<a name="psalms_55_23"></a>Psalms 55:23

But thou, ['Elohiym](../../strongs/h/h430.md), shalt [yarad](../../strongs/h/h3381.md) them into the [bᵊ'ēr](../../strongs/h/h875.md) of [shachath](../../strongs/h/h7845.md): [dam](../../strongs/h/h1818.md) and [mirmah](../../strongs/h/h4820.md) ['enowsh](../../strongs/h/h582.md) shall not [ḥāṣâ](../../strongs/h/h2673.md) their [yowm](../../strongs/h/h3117.md); but I will [batach](../../strongs/h/h982.md) in thee.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 54](psalms_54.md) - [Psalms 56](psalms_56.md)