# [Psalms 39](https://www.blueletterbible.org/kjv/psa/39/1/)

<a name="psalms_39_1"></a>Psalms 39:1

To the [nāṣaḥ](../../strongs/h/h5329.md), [even] to [Yᵊḏûṯûn](../../strongs/h/h3038.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). I ['āmar](../../strongs/h/h559.md), I will [shamar](../../strongs/h/h8104.md) to my [derek](../../strongs/h/h1870.md), that I [chata'](../../strongs/h/h2398.md) not with my [lashown](../../strongs/h/h3956.md): I will [shamar](../../strongs/h/h8104.md) my [peh](../../strongs/h/h6310.md) with a [maḥsôm](../../strongs/h/h4269.md), while the [rasha'](../../strongs/h/h7563.md) is before me.

<a name="psalms_39_2"></a>Psalms 39:2

I was ['ālam](../../strongs/h/h481.md) with [dûmîyâ](../../strongs/h/h1747.md), I [ḥāšâ](../../strongs/h/h2814.md), even from [towb](../../strongs/h/h2896.md); and my [kᵊ'ēḇ](../../strongs/h/h3511.md) was [ʿāḵar](../../strongs/h/h5916.md).

<a name="psalms_39_3"></a>Psalms 39:3

My [leb](../../strongs/h/h3820.md) was [ḥāmam](../../strongs/h/h2552.md) within me, while I was [hagiyg](../../strongs/h/h1901.md) the ['esh](../../strongs/h/h784.md) [bāʿar](../../strongs/h/h1197.md): then [dabar](../../strongs/h/h1696.md) I with my [lashown](../../strongs/h/h3956.md),

<a name="psalms_39_4"></a>Psalms 39:4

[Yĕhovah](../../strongs/h/h3068.md), make me to [yada'](../../strongs/h/h3045.md) mine [qēṣ](../../strongs/h/h7093.md), and the [midâ](../../strongs/h/h4060.md) of my [yowm](../../strongs/h/h3117.md), what it is: that I may [yada'](../../strongs/h/h3045.md) how [ḥāḏēl](../../strongs/h/h2310.md) I am.

<a name="psalms_39_5"></a>Psalms 39:5

Behold, thou hast [nathan](../../strongs/h/h5414.md) my [yowm](../../strongs/h/h3117.md) as a [ṭep̄aḥ](../../strongs/h/h2947.md); and mine [cheled](../../strongs/h/h2465.md) is as nothing before thee: verily [kōl](../../strongs/h/h3605.md) ['adam](../../strongs/h/h120.md) at his [nāṣaḇ](../../strongs/h/h5324.md) is [kōl](../../strongs/h/h3605.md) [heḇel](../../strongs/h/h1892.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_39_6"></a>Psalms 39:6

Surely every ['iysh](../../strongs/h/h376.md) [halak](../../strongs/h/h1980.md) in a [tselem](../../strongs/h/h6754.md): surely they are [hāmâ](../../strongs/h/h1993.md) in [heḇel](../../strongs/h/h1892.md): he [ṣāḇar](../../strongs/h/h6651.md), and knoweth not who shall ['āsap̄](../../strongs/h/h622.md) them.

<a name="psalms_39_7"></a>Psalms 39:7

And now, ['adonay](../../strongs/h/h136.md), what [qāvâ](../../strongs/h/h6960.md) I for? my [tôḥeleṯ](../../strongs/h/h8431.md) is in thee.

<a name="psalms_39_8"></a>Psalms 39:8

[natsal](../../strongs/h/h5337.md) me from all my [pesha'](../../strongs/h/h6588.md): [śûm](../../strongs/h/h7760.md) me not the [cherpah](../../strongs/h/h2781.md) of the [nabal](../../strongs/h/h5036.md).

<a name="psalms_39_9"></a>Psalms 39:9

I was ['ālam](../../strongs/h/h481.md), I [pāṯaḥ](../../strongs/h/h6605.md) not my [peh](../../strongs/h/h6310.md); because thou ['asah](../../strongs/h/h6213.md) it.

<a name="psalms_39_10"></a>Psalms 39:10

[cuwr](../../strongs/h/h5493.md) thy [neḡaʿ](../../strongs/h/h5061.md) from me: I am [kalah](../../strongs/h/h3615.md) by the [tiḡrâ](../../strongs/h/h8409.md) of thine [yad](../../strongs/h/h3027.md).

<a name="psalms_39_11"></a>Psalms 39:11

When thou with [tôḵēḥâ](../../strongs/h/h8433.md) dost [yacar](../../strongs/h/h3256.md) ['iysh](../../strongs/h/h376.md) for ['avon](../../strongs/h/h5771.md), thou makest his [chamad](../../strongs/h/h2530.md) to [macah](../../strongs/h/h4529.md) like an [ʿāš](../../strongs/h/h6211.md): surely every ['adam](../../strongs/h/h120.md) is [heḇel](../../strongs/h/h1892.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_39_12"></a>Psalms 39:12

[shama'](../../strongs/h/h8085.md) my [tĕphillah](../../strongs/h/h8605.md), [Yĕhovah](../../strongs/h/h3068.md), and ['azan](../../strongs/h/h238.md) unto my [shav'ah](../../strongs/h/h7775.md); [ḥāraš](../../strongs/h/h2790.md) not at my [dim'ah](../../strongs/h/h1832.md): for I am a [ger](../../strongs/h/h1616.md) with thee, and a [tôšāḇ](../../strongs/h/h8453.md), as all my ['ab](../../strongs/h/h1.md) were.

<a name="psalms_39_13"></a>Psalms 39:13

O [šāʿâ](../../strongs/h/h8159.md) me, that I may [bālaḡ](../../strongs/h/h1082.md), before I [yālaḵ](../../strongs/h/h3212.md), and be no more.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 38](psalms_38.md) - [Psalms 40](psalms_40.md)