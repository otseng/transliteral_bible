# [Psalms 101](https://www.blueletterbible.org/kjv/psalms/101)

<a name="psalms_101_1"></a>Psalms 101:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). I will [shiyr](../../strongs/h/h7891.md) of [checed](../../strongs/h/h2617.md) and [mishpat](../../strongs/h/h4941.md): unto thee, [Yĕhovah](../../strongs/h/h3068.md), will I [zamar](../../strongs/h/h2167.md).

<a name="psalms_101_2"></a>Psalms 101:2

I will [sakal](../../strongs/h/h7919.md) myself in a [tamiym](../../strongs/h/h8549.md) [derek](../../strongs/h/h1870.md). O when wilt thou [bow'](../../strongs/h/h935.md) unto me? I will [halak](../../strongs/h/h1980.md) [qereḇ](../../strongs/h/h7130.md) my [bayith](../../strongs/h/h1004.md) with a [tom](../../strongs/h/h8537.md) [lebab](../../strongs/h/h3824.md).

<a name="psalms_101_3"></a>Psalms 101:3

I will [shiyth](../../strongs/h/h7896.md) no [beliya'al](../../strongs/h/h1100.md) [dabar](../../strongs/h/h1697.md) before mine ['ayin](../../strongs/h/h5869.md): I [sane'](../../strongs/h/h8130.md) the ['asah](../../strongs/h/h6213.md) of them that [śûṭ](../../strongs/h/h7750.md); it shall not [dāḇaq](../../strongs/h/h1692.md) to me.

<a name="psalms_101_4"></a>Psalms 101:4

An [ʿiqqēš](../../strongs/h/h6141.md) [lebab](../../strongs/h/h3824.md) shall [cuwr](../../strongs/h/h5493.md) from me: I will not [yada'](../../strongs/h/h3045.md) a [ra'](../../strongs/h/h7451.md).

<a name="psalms_101_5"></a>Psalms 101:5

Whoso [cether](../../strongs/h/h5643.md) [lāšan](../../strongs/h/h3960.md) [lāšan](../../strongs/h/h3960.md) his [rea'](../../strongs/h/h7453.md), him will I [ṣāmaṯ](../../strongs/h/h6789.md): him that hath a [gāḇâ](../../strongs/h/h1362.md) ['ayin](../../strongs/h/h5869.md) and a [rāḥāḇ](../../strongs/h/h7342.md) [lebab](../../strongs/h/h3824.md) will not I [yakol](../../strongs/h/h3201.md).

<a name="psalms_101_6"></a>Psalms 101:6

Mine ['ayin](../../strongs/h/h5869.md) shall be upon the ['aman](../../strongs/h/h539.md) of the ['erets](../../strongs/h/h776.md), that they may [yashab](../../strongs/h/h3427.md) with me: he that [halak](../../strongs/h/h1980.md) in a [tamiym](../../strongs/h/h8549.md) [derek](../../strongs/h/h1870.md), he shall [sharath](../../strongs/h/h8334.md) me.

<a name="psalms_101_7"></a>Psalms 101:7

He that ['asah](../../strongs/h/h6213.md) [rᵊmîyâ](../../strongs/h/h7423.md) shall not [yashab](../../strongs/h/h3427.md) [qereḇ](../../strongs/h/h7130.md) my [bayith](../../strongs/h/h1004.md): he that [dabar](../../strongs/h/h1696.md) [sheqer](../../strongs/h/h8267.md) shall not [kuwn](../../strongs/h/h3559.md) in my ['ayin](../../strongs/h/h5869.md).

<a name="psalms_101_8"></a>Psalms 101:8

I will [boqer](../../strongs/h/h1242.md) [ṣāmaṯ](../../strongs/h/h6789.md) all the [rasha'](../../strongs/h/h7563.md) of the ['erets](../../strongs/h/h776.md); that I may [karath](../../strongs/h/h3772.md) all ['aven](../../strongs/h/h205.md) [pa'al](../../strongs/h/h6466.md) from the [ʿîr](../../strongs/h/h5892.md) of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 100](psalms_100.md) - [Psalms 102](psalms_102.md)