# [Psalms 62](https://www.blueletterbible.org/kjv/psa/62)

<a name="psalms_62_1"></a>Psalms 62:1

To the [nāṣaḥ](../../strongs/h/h5329.md), to [Yᵊḏûṯûn](../../strongs/h/h3038.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md).  Truly my [nephesh](../../strongs/h/h5315.md) [dûmîyâ](../../strongs/h/h1747.md) upon ['Elohiym](../../strongs/h/h430.md): from him cometh my [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="psalms_62_2"></a>Psalms 62:2

He only is my [tsuwr](../../strongs/h/h6697.md) and my [yĕshuw'ah](../../strongs/h/h3444.md); he is my [misgab](../../strongs/h/h4869.md); I shall not be [rab](../../strongs/h/h7227.md) [mowt](../../strongs/h/h4131.md).

<a name="psalms_62_3"></a>Psalms 62:3

How long will ye [hāṯaṯ](../../strongs/h/h2050.md) against an ['iysh](../../strongs/h/h376.md)? ye shall be [ratsach](../../strongs/h/h7523.md) all of you: as a [natah](../../strongs/h/h5186.md) [qîr](../../strongs/h/h7023.md) shall ye be, and as a [dāḥâ](../../strongs/h/h1760.md) [gāḏēr](../../strongs/h/h1447.md).

<a name="psalms_62_4"></a>Psalms 62:4

They only [ya'ats](../../strongs/h/h3289.md) to [nāḏaḥ](../../strongs/h/h5080.md) him from his [śᵊ'ēṯ](../../strongs/h/h7613.md): they [ratsah](../../strongs/h/h7521.md) in [kazab](../../strongs/h/h3577.md): they [barak](../../strongs/h/h1288.md) with their [peh](../../strongs/h/h6310.md), but they [qālal](../../strongs/h/h7043.md) [qereḇ](../../strongs/h/h7130.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_62_5"></a>Psalms 62:5

My [nephesh](../../strongs/h/h5315.md), [damam](../../strongs/h/h1826.md) thou only upon ['Elohiym](../../strongs/h/h430.md); for my [tiqvâ](../../strongs/h/h8615.md) is from him.

<a name="psalms_62_6"></a>Psalms 62:6

He only is my [tsuwr](../../strongs/h/h6697.md) and my [yĕshuw'ah](../../strongs/h/h3444.md): he is my [misgab](../../strongs/h/h4869.md); I shall not be [mowt](../../strongs/h/h4131.md).

<a name="psalms_62_7"></a>Psalms 62:7

In ['Elohiym](../../strongs/h/h430.md) is my [yesha'](../../strongs/h/h3468.md) and my [kabowd](../../strongs/h/h3519.md): the [tsuwr](../../strongs/h/h6697.md) of my ['oz](../../strongs/h/h5797.md), and my [machaceh](../../strongs/h/h4268.md), is in ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_62_8"></a>Psalms 62:8

[batach](../../strongs/h/h982.md) in him at all [ʿēṯ](../../strongs/h/h6256.md); ye ['am](../../strongs/h/h5971.md), [šāp̄aḵ](../../strongs/h/h8210.md) your [lebab](../../strongs/h/h3824.md) [paniym](../../strongs/h/h6440.md) him: ['Elohiym](../../strongs/h/h430.md) is a [machaceh](../../strongs/h/h4268.md) for us. [Celah](../../strongs/h/h5542.md).

<a name="psalms_62_9"></a>Psalms 62:9

Surely ['adam](../../strongs/h/h120.md) [ben](../../strongs/h/h1121.md) of low degree are [heḇel](../../strongs/h/h1892.md), and ['iysh](../../strongs/h/h376.md) of high degree are a [kazab](../../strongs/h/h3577.md): to be [ʿālâ](../../strongs/h/h5927.md) in the [mō'znayim](../../strongs/h/h3976.md), they are [yaḥaḏ](../../strongs/h/h3162.md) lighter than [heḇel](../../strongs/h/h1892.md).

<a name="psalms_62_10"></a>Psalms 62:10

[batach](../../strongs/h/h982.md) not in [ʿōšeq](../../strongs/h/h6233.md), and become not [hāḇal](../../strongs/h/h1891.md) in [gāzēl](../../strongs/h/h1498.md): if [ḥayil](../../strongs/h/h2428.md) [nûḇ](../../strongs/h/h5107.md), [shiyth](../../strongs/h/h7896.md) not your [leb](../../strongs/h/h3820.md) upon them.

<a name="psalms_62_11"></a>Psalms 62:11

['Elohiym](../../strongs/h/h430.md) hath [dabar](../../strongs/h/h1696.md) once; twice have I [shama'](../../strongs/h/h8085.md) this; that ['oz](../../strongs/h/h5797.md) belongeth unt['Elohiym](../../strongs/h/h430.md).

<a name="psalms_62_12"></a>Psalms 62:12

Also unto thee, O ['adonay](../../strongs/h/h136.md), belongeth [checed](../../strongs/h/h2617.md): for thou [shalam](../../strongs/h/h7999.md) to every ['iysh](../../strongs/h/h376.md) according to his [ma'aseh](../../strongs/h/h4639.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 61](psalms_61.md) - [Psalms 63](psalms_63.md)