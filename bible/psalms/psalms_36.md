# [Psalms 36](https://www.blueletterbible.org/kjv/psa/36/1/)

<a name="psalms_36_1"></a>Psalms 36:1

To the [nāṣaḥ](../../strongs/h/h5329.md), [A Psalm] of [Dāviḏ](../../strongs/h/h1732.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md). The [pesha'](../../strongs/h/h6588.md) of the [rasha'](../../strongs/h/h7563.md) [nᵊ'um](../../strongs/h/h5002.md) within my [leb](../../strongs/h/h3820.md), that there is no [paḥaḏ](../../strongs/h/h6343.md) of ['Elohiym](../../strongs/h/h430.md) before his ['ayin](../../strongs/h/h5869.md).

<a name="psalms_36_2"></a>Psalms 36:2

For he [chalaq](../../strongs/h/h2505.md) himself in his own ['ayin](../../strongs/h/h5869.md), until his ['avon](../../strongs/h/h5771.md) be [māṣā'](../../strongs/h/h4672.md) to be [sane'](../../strongs/h/h8130.md).

<a name="psalms_36_3"></a>Psalms 36:3

The [dabar](../../strongs/h/h1697.md) of his [peh](../../strongs/h/h6310.md) are ['aven](../../strongs/h/h205.md) and [mirmah](../../strongs/h/h4820.md): he hath [ḥāḏal](../../strongs/h/h2308.md) to be [sakal](../../strongs/h/h7919.md), and to do [yatab](../../strongs/h/h3190.md).

<a name="psalms_36_4"></a>Psalms 36:4

He [chashab](../../strongs/h/h2803.md) ['aven](../../strongs/h/h205.md) upon his [miškāḇ](../../strongs/h/h4904.md); he [yatsab](../../strongs/h/h3320.md) himself in a [derek](../../strongs/h/h1870.md) that is not [towb](../../strongs/h/h2896.md); he [mā'as](../../strongs/h/h3988.md) not [ra'](../../strongs/h/h7451.md).

<a name="psalms_36_5"></a>Psalms 36:5

Thy [checed](../../strongs/h/h2617.md), [Yĕhovah](../../strongs/h/h3068.md), is in the [shamayim](../../strongs/h/h8064.md); and thy ['ĕmûnâ](../../strongs/h/h530.md) reacheth unto the [shachaq](../../strongs/h/h7834.md).

<a name="psalms_36_6"></a>Psalms 36:6

Thy [ṣĕdāqāh](../../strongs/h/h6666.md) is like the ['el](../../strongs/h/h410.md) [har](../../strongs/h/h2042.md); thy [mishpat](../../strongs/h/h4941.md) are a [rab](../../strongs/h/h7227.md) [tĕhowm](../../strongs/h/h8415.md): [Yĕhovah](../../strongs/h/h3068.md), thou [yasha'](../../strongs/h/h3467.md) ['adam](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md).

<a name="psalms_36_7"></a>Psalms 36:7

How [yāqār](../../strongs/h/h3368.md) is thy [checed](../../strongs/h/h2617.md), O ['Elohiym](../../strongs/h/h430.md)! therefore the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md) put their [chacah](../../strongs/h/h2620.md) under the [ṣēl](../../strongs/h/h6738.md) of thy [kanaph](../../strongs/h/h3671.md).

<a name="psalms_36_8"></a>Psalms 36:8

They shall be [rāvâ](../../strongs/h/h7301.md) with the [dešen](../../strongs/h/h1880.md) of thy [bayith](../../strongs/h/h1004.md); and thou shalt make them [šāqâ](../../strongs/h/h8248.md) of the [nachal](../../strongs/h/h5158.md) of thy [ʿēḏen](../../strongs/h/h5730.md).

<a name="psalms_36_9"></a>Psalms 36:9

For with thee is the [māqôr](../../strongs/h/h4726.md) of [chay](../../strongs/h/h2416.md): in thy ['owr](../../strongs/h/h216.md) shall we [ra'ah](../../strongs/h/h7200.md) ['owr](../../strongs/h/h216.md).

<a name="psalms_36_10"></a>Psalms 36:10

O [mashak](../../strongs/h/h4900.md) thy [checed](../../strongs/h/h2617.md) unto them that [yada'](../../strongs/h/h3045.md) thee; and thy [ṣĕdāqāh](../../strongs/h/h6666.md) to the [yashar](../../strongs/h/h3477.md) in [leb](../../strongs/h/h3820.md).

<a name="psalms_36_11"></a>Psalms 36:11

Let not the [regel](../../strongs/h/h7272.md) of [ga'avah](../../strongs/h/h1346.md) [bow'](../../strongs/h/h935.md) against me, and let not the [yad](../../strongs/h/h3027.md) of the [rasha'](../../strongs/h/h7563.md) [nuwd](../../strongs/h/h5110.md) me.

<a name="psalms_36_12"></a>Psalms 36:12

There are the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md) [naphal](../../strongs/h/h5307.md): they are [dāḥâ](../../strongs/h/h1760.md), and shall not be [yakol](../../strongs/h/h3201.md) to [quwm](../../strongs/h/h6965.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 35](psalms_35.md) - [Psalms 37](psalms_37.md)