# [Psalms 105](https://www.blueletterbible.org/kjv/psalms/105)

<a name="psalms_105_1"></a>Psalms 105:1

O [yadah](../../strongs/h/h3034.md) unto [Yĕhovah](../../strongs/h/h3068.md); [qara'](../../strongs/h/h7121.md) upon his [shem](../../strongs/h/h8034.md): [yada'](../../strongs/h/h3045.md) his ['aliylah](../../strongs/h/h5949.md) among the ['am](../../strongs/h/h5971.md).

<a name="psalms_105_2"></a>Psalms 105:2

[shiyr](../../strongs/h/h7891.md) unto him, [zamar](../../strongs/h/h2167.md) unto him: [śîaḥ](../../strongs/h/h7878.md) ye of all his [pala'](../../strongs/h/h6381.md).

<a name="psalms_105_3"></a>Psalms 105:3

[halal](../../strongs/h/h1984.md) ye in his [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md): let the [leb](../../strongs/h/h3820.md) of them [samach](../../strongs/h/h8055.md) that [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_105_4"></a>Psalms 105:4

[darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md), and his ['oz](../../strongs/h/h5797.md): [bāqaš](../../strongs/h/h1245.md) his [paniym](../../strongs/h/h6440.md) [tāmîḏ](../../strongs/h/h8548.md).

<a name="psalms_105_5"></a>Psalms 105:5

[zakar](../../strongs/h/h2142.md) his [pala'](../../strongs/h/h6381.md) that he hath ['asah](../../strongs/h/h6213.md); his [môp̄ēṯ](../../strongs/h/h4159.md), and the [mishpat](../../strongs/h/h4941.md) of his [peh](../../strongs/h/h6310.md);

<a name="psalms_105_6"></a>Psalms 105:6

O ye [zera'](../../strongs/h/h2233.md) of ['Abraham](../../strongs/h/h85.md) his ['ebed](../../strongs/h/h5650.md), ye [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md) his [bāḥîr](../../strongs/h/h972.md).

<a name="psalms_105_7"></a>Psalms 105:7

He is [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md): his [mishpat](../../strongs/h/h4941.md) are in all the ['erets](../../strongs/h/h776.md).

<a name="psalms_105_8"></a>Psalms 105:8

He hath [zakar](../../strongs/h/h2142.md) his [bĕriyth](../../strongs/h/h1285.md) ['owlam](../../strongs/h/h5769.md), the [dabar](../../strongs/h/h1697.md) which he [tsavah](../../strongs/h/h6680.md) to an ['elep̄](../../strongs/h/h505.md) [dôr](../../strongs/h/h1755.md).

<a name="psalms_105_9"></a>Psalms 105:9

Which covenant he [karath](../../strongs/h/h3772.md) with ['Abraham](../../strongs/h/h85.md), and his [šᵊḇûʿâ](../../strongs/h/h7621.md) unto [Yišḥāq](../../strongs/h/h3446.md);

<a name="psalms_105_10"></a>Psalms 105:10

And ['amad](../../strongs/h/h5975.md) the same unto [Ya'aqob](../../strongs/h/h3290.md) for a [choq](../../strongs/h/h2706.md), and to [Yisra'el](../../strongs/h/h3478.md) for an ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md):

<a name="psalms_105_11"></a>Psalms 105:11

['āmar](../../strongs/h/h559.md), Unto thee will I [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) of [kĕna'an](../../strongs/h/h3667.md), the [chebel](../../strongs/h/h2256.md) of your [nachalah](../../strongs/h/h5159.md):

<a name="psalms_105_12"></a>Psalms 105:12

When they were but a few [math](../../strongs/h/h4962.md) in [mispār](../../strongs/h/h4557.md); yea, very [mᵊʿaṭ](../../strongs/h/h4592.md), and [guwr](../../strongs/h/h1481.md) in it.

<a name="psalms_105_13"></a>Psalms 105:13

When they [halak](../../strongs/h/h1980.md) from one [gowy](../../strongs/h/h1471.md) to another, from one [mamlāḵâ](../../strongs/h/h4467.md) to ['aḥēr](../../strongs/h/h312.md) ['am](../../strongs/h/h5971.md);

<a name="psalms_105_14"></a>Psalms 105:14

He [yānaḥ](../../strongs/h/h3240.md) no ['āḏām](../../strongs/h/h120.md) to do them [ʿāšaq](../../strongs/h/h6231.md): yea, he [yakach](../../strongs/h/h3198.md) [melek](../../strongs/h/h4428.md) for their sakes;

<a name="psalms_105_15"></a>Psalms 105:15

Saying, [naga'](../../strongs/h/h5060.md) not mine [mashiyach](../../strongs/h/h4899.md), and do my [nāḇî'](../../strongs/h/h5030.md) no [ra'a'](../../strongs/h/h7489.md).

<a name="psalms_105_16"></a>Psalms 105:16

Moreover he [qara'](../../strongs/h/h7121.md) for a [rāʿāḇ](../../strongs/h/h7458.md) upon the ['erets](../../strongs/h/h776.md): he [shabar](../../strongs/h/h7665.md) the [maṭṭê](../../strongs/h/h4294.md) of [lechem](../../strongs/h/h3899.md).

<a name="psalms_105_17"></a>Psalms 105:17

He [shalach](../../strongs/h/h7971.md) an ['iysh](../../strongs/h/h376.md) [paniym](../../strongs/h/h6440.md) them, even [Yôsēp̄](../../strongs/h/h3130.md), who was [māḵar](../../strongs/h/h4376.md) for an ['ebed](../../strongs/h/h5650.md):

<a name="psalms_105_18"></a>Psalms 105:18

Whose [regel](../../strongs/h/h7272.md) they [ʿānâ](../../strongs/h/h6031.md) with [keḇel](../../strongs/h/h3525.md): [nephesh](../../strongs/h/h5315.md) was [bow'](../../strongs/h/h935.md) in [barzel](../../strongs/h/h1270.md):

<a name="psalms_105_19"></a>Psalms 105:19

Until the [ʿēṯ](../../strongs/h/h6256.md) that his [dabar](../../strongs/h/h1697.md) [bow'](../../strongs/h/h935.md): the ['imrah](../../strongs/h/h565.md) of [Yĕhovah](../../strongs/h/h3068.md) [tsaraph](../../strongs/h/h6884.md) him.

<a name="psalms_105_20"></a>Psalms 105:20

The [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md) and [nāṯar](../../strongs/h/h5425.md) him; even the [mashal](../../strongs/h/h4910.md) of the ['am](../../strongs/h/h5971.md), and let him [pāṯaḥ](../../strongs/h/h6605.md).

<a name="psalms_105_21"></a>Psalms 105:21

He [śûm](../../strongs/h/h7760.md) him ['adown](../../strongs/h/h113.md) of his [bayith](../../strongs/h/h1004.md), and [mashal](../../strongs/h/h4910.md) of all his [qinyān](../../strongs/h/h7075.md):

<a name="psalms_105_22"></a>Psalms 105:22

To ['āsar](../../strongs/h/h631.md) his [śar](../../strongs/h/h8269.md) at his [nephesh](../../strongs/h/h5315.md); and teach his [zāqēn](../../strongs/h/h2205.md) [ḥāḵam](../../strongs/h/h2449.md).

<a name="psalms_105_23"></a>Psalms 105:23

[Yisra'el](../../strongs/h/h3478.md) also [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md); and [Ya'aqob](../../strongs/h/h3290.md) [guwr](../../strongs/h/h1481.md) in the ['erets](../../strongs/h/h776.md) of [Ḥām](../../strongs/h/h2526.md).

<a name="psalms_105_24"></a>Psalms 105:24

And he [parah](../../strongs/h/h6509.md) his ['am](../../strongs/h/h5971.md) [me'od](../../strongs/h/h3966.md); and made them [ʿāṣam](../../strongs/h/h6105.md) than their [tsar](../../strongs/h/h6862.md).

<a name="psalms_105_25"></a>Psalms 105:25

He [hāp̄aḵ](../../strongs/h/h2015.md) their [leb](../../strongs/h/h3820.md) to [sane'](../../strongs/h/h8130.md) his ['am](../../strongs/h/h5971.md), to deal [nāḵal](../../strongs/h/h5230.md) with his ['ebed](../../strongs/h/h5650.md).

<a name="psalms_105_26"></a>Psalms 105:26

He [shalach](../../strongs/h/h7971.md) [Mōshe](../../strongs/h/h4872.md) his ['ebed](../../strongs/h/h5650.md); and ['Ahărôn](../../strongs/h/h175.md) whom he had [bāḥar](../../strongs/h/h977.md).

<a name="psalms_105_27"></a>Psalms 105:27

They [śûm](../../strongs/h/h7760.md) his ['ôṯ](../../strongs/h/h226.md) [dabar](../../strongs/h/h1697.md) among them, and [môp̄ēṯ](../../strongs/h/h4159.md) in the ['erets](../../strongs/h/h776.md) of [Ḥām](../../strongs/h/h2526.md).

<a name="psalms_105_28"></a>Psalms 105:28

He [shalach](../../strongs/h/h7971.md) [choshek](../../strongs/h/h2822.md), and made it [ḥāšaḵ](../../strongs/h/h2821.md); and they [marah](../../strongs/h/h4784.md) not against his [dabar](../../strongs/h/h1697.md).

<a name="psalms_105_29"></a>Psalms 105:29

He [hāp̄aḵ](../../strongs/h/h2015.md) their [mayim](../../strongs/h/h4325.md) into [dam](../../strongs/h/h1818.md), and [muwth](../../strongs/h/h4191.md) their [dāḡâ](../../strongs/h/h1710.md).

<a name="psalms_105_30"></a>Psalms 105:30

Their ['erets](../../strongs/h/h776.md) [šāraṣ](../../strongs/h/h8317.md) [ṣᵊp̄ardēaʿ](../../strongs/h/h6854.md) in [šāraṣ](../../strongs/h/h8317.md), in the [ḥeḏer](../../strongs/h/h2315.md) of their [melek](../../strongs/h/h4428.md).

<a name="psalms_105_31"></a>Psalms 105:31

He ['āmar](../../strongs/h/h559.md), and there [bow'](../../strongs/h/h935.md) [ʿārōḇ](../../strongs/h/h6157.md), and [kēn](../../strongs/h/h3654.md) in all their [gᵊḇûl](../../strongs/h/h1366.md).

<a name="psalms_105_32"></a>Psalms 105:32

He [nathan](../../strongs/h/h5414.md) them [barad](../../strongs/h/h1259.md) for [gešem](../../strongs/h/h1653.md), and [lehāḇâ](../../strongs/h/h3852.md) ['esh](../../strongs/h/h784.md) in their ['erets](../../strongs/h/h776.md).

<a name="psalms_105_33"></a>Psalms 105:33

He [nakah](../../strongs/h/h5221.md) their [gep̄en](../../strongs/h/h1612.md) also and their [tĕ'en](../../strongs/h/h8384.md); and [shabar](../../strongs/h/h7665.md) the ['ets](../../strongs/h/h6086.md) of their [gᵊḇûl](../../strongs/h/h1366.md).

<a name="psalms_105_34"></a>Psalms 105:34

He ['āmar](../../strongs/h/h559.md), and the ['arbê](../../strongs/h/h697.md) [bow'](../../strongs/h/h935.md), and [yeleq](../../strongs/h/h3218.md), and that without [mispār](../../strongs/h/h4557.md),

<a name="psalms_105_35"></a>Psalms 105:35

And did ['akal](../../strongs/h/h398.md) all the ['eseb](../../strongs/h/h6212.md) in their ['erets](../../strongs/h/h776.md), and ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) of their ['ăḏāmâ](../../strongs/h/h127.md).

<a name="psalms_105_36"></a>Psalms 105:36

He [nakah](../../strongs/h/h5221.md) also all the [bᵊḵôr](../../strongs/h/h1060.md) in their ['erets](../../strongs/h/h776.md), the [re'shiyth](../../strongs/h/h7225.md) of all their ['ôn](../../strongs/h/h202.md).

<a name="psalms_105_37"></a>Psalms 105:37

He [yāṣā'](../../strongs/h/h3318.md) them also with [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md): and there was not one [kashal](../../strongs/h/h3782.md) among their [shebet](../../strongs/h/h7626.md).

<a name="psalms_105_38"></a>Psalms 105:38

[Mitsrayim](../../strongs/h/h4714.md) was [samach](../../strongs/h/h8055.md) when they [yāṣā'](../../strongs/h/h3318.md): for the [paḥaḏ](../../strongs/h/h6343.md) of them [naphal](../../strongs/h/h5307.md) upon them.

<a name="psalms_105_39"></a>Psalms 105:39

He [pāraś](../../strongs/h/h6566.md) an [ʿānān](../../strongs/h/h6051.md) for a [māsāḵ](../../strongs/h/h4539.md); and ['esh](../../strongs/h/h784.md) to give ['owr](../../strongs/h/h215.md) in the [layil](../../strongs/h/h3915.md).

<a name="psalms_105_40"></a>Psalms 105:40

The people [sha'al](../../strongs/h/h7592.md), and he [bow'](../../strongs/h/h935.md) [śᵊlav](../../strongs/h/h7958.md), and [sāׂbaʿ](../../strongs/h/h7646.md) them with the [lechem](../../strongs/h/h3899.md) of [shamayim](../../strongs/h/h8064.md).

<a name="psalms_105_41"></a>Psalms 105:41

He [pāṯaḥ](../../strongs/h/h6605.md) the [tsuwr](../../strongs/h/h6697.md), and the [mayim](../../strongs/h/h4325.md) gushed [zûḇ](../../strongs/h/h2100.md); they [halak](../../strongs/h/h1980.md) in the dry [ṣîyâ](../../strongs/h/h6723.md) like a [nāhār](../../strongs/h/h5104.md).

<a name="psalms_105_42"></a>Psalms 105:42

For he [zakar](../../strongs/h/h2142.md) his [qodesh](../../strongs/h/h6944.md) [dabar](../../strongs/h/h1697.md), and ['Abraham](../../strongs/h/h85.md) his ['ebed](../../strongs/h/h5650.md).

<a name="psalms_105_43"></a>Psalms 105:43

And he [yāṣā'](../../strongs/h/h3318.md) his ['am](../../strongs/h/h5971.md) with [śāśôn](../../strongs/h/h8342.md), and his [bāḥîr](../../strongs/h/h972.md) with [rinnah](../../strongs/h/h7440.md):

<a name="psalms_105_44"></a>Psalms 105:44

And [nathan](../../strongs/h/h5414.md) them the ['erets](../../strongs/h/h776.md) of the [gowy](../../strongs/h/h1471.md): and they [yarash](../../strongs/h/h3423.md) the ['amal](../../strongs/h/h5999.md) of the [lĕom](../../strongs/h/h3816.md);

<a name="psalms_105_45"></a>Psalms 105:45

That they might [shamar](../../strongs/h/h8104.md) his [choq](../../strongs/h/h2706.md), and [nāṣar](../../strongs/h/h5341.md) his [towrah](../../strongs/h/h8451.md). [halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 104](psalms_104.md) - [Psalms 106](psalms_106.md)