# [Psalms 88](https://www.blueletterbible.org/kjv/psalms/88)

<a name="psalms_88_1"></a>Psalms 88:1

A [šîr](../../strongs/h/h7892.md) or [mizmôr](../../strongs/h/h4210.md) for the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md), to the chief [nāṣaḥ](../../strongs/h/h5329.md) upon [Maḥălaṯ](../../strongs/h/h4257.md) [ʿānâ](../../strongs/h/h6031.md), [Maśkîl](../../strongs/h/h4905.md) of [Hêmān](../../strongs/h/h1968.md) the ['ezrāḥî](../../strongs/h/h250.md). [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of my [yĕshuw'ah](../../strongs/h/h3444.md), I have [ṣāʿaq](../../strongs/h/h6817.md) [yowm](../../strongs/h/h3117.md) and [layil](../../strongs/h/h3915.md) before thee:

<a name="psalms_88_2"></a>Psalms 88:2

Let my [tĕphillah](../../strongs/h/h8605.md) [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) thee: [natah](../../strongs/h/h5186.md) thine ['ozen](../../strongs/h/h241.md) unto my [rinnah](../../strongs/h/h7440.md);

<a name="psalms_88_3"></a>Psalms 88:3

For my [nephesh](../../strongs/h/h5315.md) is [sāׂbaʿ](../../strongs/h/h7646.md) of [ra'](../../strongs/h/h7451.md): and my [chay](../../strongs/h/h2416.md) draweth [naga'](../../strongs/h/h5060.md) unto the [shĕ'owl](../../strongs/h/h7585.md).

<a name="psalms_88_4"></a>Psalms 88:4

I am [chashab](../../strongs/h/h2803.md) with them that go [yarad](../../strongs/h/h3381.md) into the [bowr](../../strongs/h/h953.md): I am as a [geḇer](../../strongs/h/h1397.md) that hath no ['ĕyāl](../../strongs/h/h353.md):

<a name="psalms_88_5"></a>Psalms 88:5

[ḥāp̄šî](../../strongs/h/h2670.md) among the [muwth](../../strongs/h/h4191.md), like the [ḥālāl](../../strongs/h/h2491.md) that [shakab](../../strongs/h/h7901.md) in the [qeber](../../strongs/h/h6913.md), whom thou [zakar](../../strongs/h/h2142.md) no more: and they are [gāzar](../../strongs/h/h1504.md) from thy [yad](../../strongs/h/h3027.md).

<a name="psalms_88_6"></a>Psalms 88:6

Thou hast [shiyth](../../strongs/h/h7896.md) me in the [taḥtî](../../strongs/h/h8482.md) [bowr](../../strongs/h/h953.md), in [maḥšāḵ](../../strongs/h/h4285.md), in the [mᵊṣôlâ](../../strongs/h/h4688.md).

<a name="psalms_88_7"></a>Psalms 88:7

Thy [chemah](../../strongs/h/h2534.md) [camak](../../strongs/h/h5564.md) upon me, and thou hast [ʿānâ](../../strongs/h/h6031.md) me with all thy [mišbār](../../strongs/h/h4867.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_88_8"></a>Psalms 88:8

Thou hast [rachaq](../../strongs/h/h7368.md) mine [yada'](../../strongs/h/h3045.md) [rachaq](../../strongs/h/h7368.md) from me; thou hast [shiyth](../../strongs/h/h7896.md) me a [tôʿēḇâ](../../strongs/h/h8441.md) unto them: I am [kālā'](../../strongs/h/h3607.md), and I cannot [yāṣā'](../../strongs/h/h3318.md).

<a name="psalms_88_9"></a>Psalms 88:9

Mine ['ayin](../../strongs/h/h5869.md) [dā'aḇ](../../strongs/h/h1669.md) by reason of ['oniy](../../strongs/h/h6040.md): [Yĕhovah](../../strongs/h/h3068.md), I have [qara'](../../strongs/h/h7121.md) [yowm](../../strongs/h/h3117.md) upon thee, I have [šāṭaḥ](../../strongs/h/h7849.md) my [kaph](../../strongs/h/h3709.md) unto thee.

<a name="psalms_88_10"></a>Psalms 88:10

Wilt thou ['asah](../../strongs/h/h6213.md) [pele'](../../strongs/h/h6382.md) to the [muwth](../../strongs/h/h4191.md) ? shall the [rᵊp̄ā'îm](../../strongs/h/h7496.md) [quwm](../../strongs/h/h6965.md) and [yadah](../../strongs/h/h3034.md) thee? [Celah](../../strongs/h/h5542.md).

<a name="psalms_88_11"></a>Psalms 88:11

Shall thy [checed](../../strongs/h/h2617.md) be [sāp̄ar](../../strongs/h/h5608.md) in the [qeber](../../strongs/h/h6913.md) ? or thy ['ĕmûnâ](../../strongs/h/h530.md) in ['Ăḇadôn](../../strongs/h/h11.md) ?

<a name="psalms_88_12"></a>Psalms 88:12

Shall thy [pele'](../../strongs/h/h6382.md) be [yada'](../../strongs/h/h3045.md) in the [choshek](../../strongs/h/h2822.md) ? and thy [tsedaqah](../../strongs/h/h6666.md) in the ['erets](../../strongs/h/h776.md) of [nᵊšîyâ](../../strongs/h/h5388.md) ?

<a name="psalms_88_13"></a>Psalms 88:13

But unto thee have I [šāvaʿ](../../strongs/h/h7768.md), [Yĕhovah](../../strongs/h/h3068.md); and in the [boqer](../../strongs/h/h1242.md) shall my [tĕphillah](../../strongs/h/h8605.md) [qadam](../../strongs/h/h6923.md) thee.

<a name="psalms_88_14"></a>Psalms 88:14

[Yĕhovah](../../strongs/h/h3068.md), why [zānaḥ](../../strongs/h/h2186.md) thou my [nephesh](../../strongs/h/h5315.md) ? why [cathar](../../strongs/h/h5641.md) thou thy [paniym](../../strongs/h/h6440.md) from me?

<a name="psalms_88_15"></a>Psalms 88:15

I am ['aniy](../../strongs/h/h6041.md) and ready to [gāvaʿ](../../strongs/h/h1478.md) from my [nōʿar](../../strongs/h/h5290.md) up: while I [nasa'](../../strongs/h/h5375.md) thy ['êmâ](../../strongs/h/h367.md) I am [pûn](../../strongs/h/h6323.md).

<a name="psalms_88_16"></a>Psalms 88:16

Thy [charown](../../strongs/h/h2740.md) ['abar](../../strongs/h/h5674.md) me; thy [biʿûṯîm](../../strongs/h/h1161.md) have [ṣāmaṯ](../../strongs/h/h6789.md) me.

<a name="psalms_88_17"></a>Psalms 88:17

They [cabab](../../strongs/h/h5437.md) me [yowm](../../strongs/h/h3117.md) like [mayim](../../strongs/h/h4325.md); they [naqaph](../../strongs/h/h5362.md) me about [yaḥaḏ](../../strongs/h/h3162.md).

<a name="psalms_88_18"></a>Psalms 88:18

['ahab](../../strongs/h/h157.md) and [rea'](../../strongs/h/h7453.md) hast thou put [rachaq](../../strongs/h/h7368.md) from me, and mine [yada'](../../strongs/h/h3045.md) into [maḥšāḵ](../../strongs/h/h4285.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 87](psalms_87.md) - [Psalms 89](psalms_89.md)