# [Psalms 3](https://www.blueletterbible.org/kjv/psa/3/1/s_481001)

<a name="psalms_3_1"></a>Psalms 3:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md), when he [bāraḥ](../../strongs/h/h1272.md) from ['Ăbyšālôm](../../strongs/h/h53.md) his [ben](../../strongs/h/h1121.md). [Yĕhovah](../../strongs/h/h3068.md), how are they [rabab](../../strongs/h/h7231.md) that [tsar](../../strongs/h/h6862.md) me! many are they that [quwm](../../strongs/h/h6965.md) against me.

<a name="psalms_3_2"></a>Psalms 3:2

Many which ['āmar](../../strongs/h/h559.md) of my [nephesh](../../strongs/h/h5315.md), no [yĕshuw'ah](../../strongs/h/h3444.md) for him in ['Elohiym](../../strongs/h/h430.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_3_3"></a>Psalms 3:3

[Yĕhovah](../../strongs/h/h3068.md), a [magen](../../strongs/h/h4043.md) for me; my [kabowd](../../strongs/h/h3519.md), and the [ruwm](../../strongs/h/h7311.md) of mine [ro'sh](../../strongs/h/h7218.md).

<a name="psalms_3_4"></a>Psalms 3:4

I [qara'](../../strongs/h/h7121.md) unto [Yĕhovah](../../strongs/h/h3068.md) with my [qowl](../../strongs/h/h6963.md), and he ['anah](../../strongs/h/h6030.md) me out of his [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_3_5"></a>Psalms 3:5

I [shakab](../../strongs/h/h7901.md) and [yashen](../../strongs/h/h3462.md); I [quwts](../../strongs/h/h6974.md); for [Yĕhovah](../../strongs/h/h3068.md) [camak](../../strongs/h/h5564.md) me.

<a name="psalms_3_6"></a>Psalms 3:6

I will not [yare'](../../strongs/h/h3372.md) of ten thousands of ['am](../../strongs/h/h5971.md), that have [shiyth](../../strongs/h/h7896.md) themselves against me [cabiyb](../../strongs/h/h5439.md).

<a name="psalms_3_7"></a>Psalms 3:7

[quwm](../../strongs/h/h6965.md), [Yĕhovah](../../strongs/h/h3068.md); [yasha'](../../strongs/h/h3467.md) me, ['Elohiym](../../strongs/h/h430.md): for thou hast [nakah](../../strongs/h/h5221.md) all mine ['oyeb](../../strongs/h/h341.md) upon the [lᵊḥî](../../strongs/h/h3895.md); thou hast [shabar](../../strongs/h/h7665.md) the [šēn](../../strongs/h/h8127.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="psalms_3_8"></a>Psalms 3:8

[yĕshuw'ah](../../strongs/h/h3444.md) unto [Yĕhovah](../../strongs/h/h3068.md): thy [bĕrakah](../../strongs/h/h1293.md) upon thy ['am](../../strongs/h/h5971.md). [Celah](../../strongs/h/h5542.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 2](psalms_2.md) - [Psalms 4](psalms_4.md)