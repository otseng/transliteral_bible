# [Psalms 131](https://www.blueletterbible.org/kjv/psalms/131)

<a name="psalms_131_1"></a>Psalms 131:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md) of [Dāviḏ](../../strongs/h/h1732.md). [Yĕhovah](../../strongs/h/h3068.md), my [leb](../../strongs/h/h3820.md) is not [gāḇah](../../strongs/h/h1361.md), nor mine ['ayin](../../strongs/h/h5869.md) [ruwm](../../strongs/h/h7311.md): neither do I [halak](../../strongs/h/h1980.md) myself in [gadowl](../../strongs/h/h1419.md), or in things too [pala'](../../strongs/h/h6381.md) for me.

<a name="psalms_131_2"></a>Psalms 131:2

Surely I have [šāvâ](../../strongs/h/h7737.md) and [damam](../../strongs/h/h1826.md) [nephesh](../../strongs/h/h5315.md), as a child that is [gamal](../../strongs/h/h1580.md) of his ['em](../../strongs/h/h517.md): my [nephesh](../../strongs/h/h5315.md) is even as a [gamal](../../strongs/h/h1580.md).

<a name="psalms_131_3"></a>Psalms 131:3

Let [Yisra'el](../../strongs/h/h3478.md) [yāḥal](../../strongs/h/h3176.md) in [Yĕhovah](../../strongs/h/h3068.md) from henceforth and [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 130](psalms_130.md) - [Psalms 132](psalms_132.md)