# [Psalms 146](https://www.blueletterbible.org/kjv/psalms/146)

<a name="psalms_146_1"></a>Psalms 146:1

[halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md). [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md), O my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_146_2"></a>Psalms 146:2

While I [chay](../../strongs/h/h2416.md) will I [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md): I will [zamar](../../strongs/h/h2167.md) unto my ['Elohiym](../../strongs/h/h430.md) while I have any being.

<a name="psalms_146_3"></a>Psalms 146:3

Put not your [batach](../../strongs/h/h982.md) in [nāḏîḇ](../../strongs/h/h5081.md), nor in the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), in whom there is no [tᵊšûʿâ](../../strongs/h/h8668.md).

<a name="psalms_146_4"></a>Psalms 146:4

His [ruwach](../../strongs/h/h7307.md) [yāṣā'](../../strongs/h/h3318.md), he [shuwb](../../strongs/h/h7725.md) to his ['ăḏāmâ](../../strongs/h/h127.md); in that very [yowm](../../strongs/h/h3117.md) his [ʿeštōnôṯ](../../strongs/h/h6250.md) ['abad](../../strongs/h/h6.md).

<a name="psalms_146_5"></a>Psalms 146:5

['esher](../../strongs/h/h835.md) is he that hath the ['el](../../strongs/h/h410.md) of [Ya'aqob](../../strongs/h/h3290.md) for his ['ezer](../../strongs/h/h5828.md), whose [śēḇer](../../strongs/h/h7664.md) is in [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md):

<a name="psalms_146_6"></a>Psalms 146:6

Which ['asah](../../strongs/h/h6213.md) [shamayim](../../strongs/h/h8064.md), and ['erets](../../strongs/h/h776.md), the [yam](../../strongs/h/h3220.md), and all that therein is: which [shamar](../../strongs/h/h8104.md) ['emeth](../../strongs/h/h571.md) ['owlam](../../strongs/h/h5769.md):

<a name="psalms_146_7"></a>Psalms 146:7

Which ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) for the [ʿāšaq](../../strongs/h/h6231.md): which [nathan](../../strongs/h/h5414.md) [lechem](../../strongs/h/h3899.md) to the [rāʿēḇ](../../strongs/h/h7457.md). [Yĕhovah](../../strongs/h/h3068.md) [nāṯar](../../strongs/h/h5425.md) the ['āsar](../../strongs/h/h631.md):

<a name="psalms_146_8"></a>Psalms 146:8

[Yĕhovah](../../strongs/h/h3068.md) [paqach](../../strongs/h/h6491.md) the eyes of the [ʿiûēr](../../strongs/h/h5787.md): [Yĕhovah](../../strongs/h/h3068.md) [zāqap̄](../../strongs/h/h2210.md) them that are [kāp̄ap̄](../../strongs/h/h3721.md): [Yĕhovah](../../strongs/h/h3068.md) ['ahab](../../strongs/h/h157.md) the [tsaddiyq](../../strongs/h/h6662.md):

<a name="psalms_146_9"></a>Psalms 146:9

[Yĕhovah](../../strongs/h/h3068.md) [shamar](../../strongs/h/h8104.md) the [ger](../../strongs/h/h1616.md); he [ʿûḏ](../../strongs/h/h5749.md) the [yathowm](../../strongs/h/h3490.md) and ['almānâ](../../strongs/h/h490.md): but the [derek](../../strongs/h/h1870.md) of the [rasha'](../../strongs/h/h7563.md) he [ʿāvaṯ](../../strongs/h/h5791.md).

<a name="psalms_146_10"></a>Psalms 146:10

[Yĕhovah](../../strongs/h/h3068.md) shall [mālaḵ](../../strongs/h/h4427.md) ['owlam](../../strongs/h/h5769.md), even thy ['Elohiym](../../strongs/h/h430.md), [Tsiyown](../../strongs/h/h6726.md), unto [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md). [halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 145](psalms_145.md) - [Psalms 147](psalms_147.md)