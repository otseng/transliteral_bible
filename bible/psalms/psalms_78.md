# [Psalms 78](https://www.blueletterbible.org/kjv/psa/78)

<a name="psalms_78_1"></a>Psalms 78:1

[Maśkîl](../../strongs/h/h4905.md) of ['Āsāp̄](../../strongs/h/h623.md). ['azan](../../strongs/h/h238.md), O my ['am](../../strongs/h/h5971.md), to my [towrah](../../strongs/h/h8451.md): [natah](../../strongs/h/h5186.md) your ['ozen](../../strongs/h/h241.md) to the ['emer](../../strongs/h/h561.md) of my [peh](../../strongs/h/h6310.md).

<a name="psalms_78_2"></a>Psalms 78:2

I will [pāṯaḥ](../../strongs/h/h6605.md) my [peh](../../strongs/h/h6310.md) in a [māšāl](../../strongs/h/h4912.md): I will [nāḇaʿ](../../strongs/h/h5042.md) [ḥîḏâ](../../strongs/h/h2420.md) of [qeḏem](../../strongs/h/h6924.md):

<a name="psalms_78_3"></a>Psalms 78:3

Which we have [shama'](../../strongs/h/h8085.md) and [yada'](../../strongs/h/h3045.md), and our ['ab](../../strongs/h/h1.md) have [sāp̄ar](../../strongs/h/h5608.md) us.

<a name="psalms_78_4"></a>Psalms 78:4

We will not [kāḥaḏ](../../strongs/h/h3582.md) them from their [ben](../../strongs/h/h1121.md), [sāp̄ar](../../strongs/h/h5608.md) to the [dôr](../../strongs/h/h1755.md) to ['aḥărôn](../../strongs/h/h314.md) the [tehillah](../../strongs/h/h8416.md) of [Yĕhovah](../../strongs/h/h3068.md), and his [ʿĕzûz](../../strongs/h/h5807.md), and his [pala'](../../strongs/h/h6381.md) that he hath ['asah](../../strongs/h/h6213.md).

<a name="psalms_78_5"></a>Psalms 78:5

For he [quwm](../../strongs/h/h6965.md) a [ʿēḏûṯ](../../strongs/h/h5715.md) in [Ya'aqob](../../strongs/h/h3290.md), and [śûm](../../strongs/h/h7760.md) a [towrah](../../strongs/h/h8451.md) in [Yisra'el](../../strongs/h/h3478.md), which he [tsavah](../../strongs/h/h6680.md) our ['ab](../../strongs/h/h1.md), that they should make them [yada'](../../strongs/h/h3045.md) to their [ben](../../strongs/h/h1121.md):

<a name="psalms_78_6"></a>Psalms 78:6

That the [dôr](../../strongs/h/h1755.md) to ['aḥărôn](../../strongs/h/h314.md) might [yada'](../../strongs/h/h3045.md) them, even the [ben](../../strongs/h/h1121.md) which should be [yalad](../../strongs/h/h3205.md); who should [quwm](../../strongs/h/h6965.md) and [sāp̄ar](../../strongs/h/h5608.md) them to their [ben](../../strongs/h/h1121.md):

<a name="psalms_78_7"></a>Psalms 78:7

That they might [śûm](../../strongs/h/h7760.md) their [kesel](../../strongs/h/h3689.md) in ['Elohiym](../../strongs/h/h430.md), and not [shakach](../../strongs/h/h7911.md) the [maʿălāl](../../strongs/h/h4611.md) of ['el](../../strongs/h/h410.md), but [nāṣar](../../strongs/h/h5341.md) his [mitsvah](../../strongs/h/h4687.md):

<a name="psalms_78_8"></a>Psalms 78:8

And might not be as their ['ab](../../strongs/h/h1.md), a [sārar](../../strongs/h/h5637.md) and [marah](../../strongs/h/h4784.md) [dôr](../../strongs/h/h1755.md); a [dôr](../../strongs/h/h1755.md) that [kuwn](../../strongs/h/h3559.md) not their [leb](../../strongs/h/h3820.md) [kuwn](../../strongs/h/h3559.md), and whose [ruwach](../../strongs/h/h7307.md) was not ['aman](../../strongs/h/h539.md) with ['el](../../strongs/h/h410.md).

<a name="psalms_78_9"></a>Psalms 78:9

The [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md), being [nashaq](../../strongs/h/h5401.md), and [rāmâ](../../strongs/h/h7411.md) [qesheth](../../strongs/h/h7198.md), [hāp̄aḵ](../../strongs/h/h2015.md) in the [yowm](../../strongs/h/h3117.md) of [qᵊrāḇ](../../strongs/h/h7128.md).

<a name="psalms_78_10"></a>Psalms 78:10

They [shamar](../../strongs/h/h8104.md) not the [bĕriyth](../../strongs/h/h1285.md) of ['Elohiym](../../strongs/h/h430.md), and [mā'ēn](../../strongs/h/h3985.md) to [yālaḵ](../../strongs/h/h3212.md) in his [towrah](../../strongs/h/h8451.md);

<a name="psalms_78_11"></a>Psalms 78:11

And [shakach](../../strongs/h/h7911.md) his ['aliylah](../../strongs/h/h5949.md), and his [pala'](../../strongs/h/h6381.md) that he had [ra'ah](../../strongs/h/h7200.md) them.

<a name="psalms_78_12"></a>Psalms 78:12

[pele'](../../strongs/h/h6382.md) ['asah](../../strongs/h/h6213.md) he in the [neḡeḏ](../../strongs/h/h5048.md) of their ['ab](../../strongs/h/h1.md), in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), in the [sadeh](../../strongs/h/h7704.md) of [Ṣōʿan](../../strongs/h/h6814.md).

<a name="psalms_78_13"></a>Psalms 78:13

He [bāqaʿ](../../strongs/h/h1234.md) the [yam](../../strongs/h/h3220.md), and caused them to ['abar](../../strongs/h/h5674.md); and he made the [mayim](../../strongs/h/h4325.md) to [nāṣaḇ](../../strongs/h/h5324.md) as a [nēḏ](../../strongs/h/h5067.md).

<a name="psalms_78_14"></a>Psalms 78:14

In the [yômām](../../strongs/h/h3119.md) also he [nachah](../../strongs/h/h5148.md) them with a [ʿānān](../../strongs/h/h6051.md), and all the [layil](../../strongs/h/h3915.md) with a ['owr](../../strongs/h/h216.md) of ['esh](../../strongs/h/h784.md).

<a name="psalms_78_15"></a>Psalms 78:15

He [bāqaʿ](../../strongs/h/h1234.md) the [tsuwr](../../strongs/h/h6697.md) in the [midbar](../../strongs/h/h4057.md), and [šāqâ](../../strongs/h/h8248.md) them as out of the [rab](../../strongs/h/h7227.md) [tĕhowm](../../strongs/h/h8415.md).

<a name="psalms_78_16"></a>Psalms 78:16

He [yāṣā'](../../strongs/h/h3318.md) [nāzal](../../strongs/h/h5140.md) also out of the [cela'](../../strongs/h/h5553.md), and caused [mayim](../../strongs/h/h4325.md) to [yarad](../../strongs/h/h3381.md) like [nāhār](../../strongs/h/h5104.md).

<a name="psalms_78_17"></a>Psalms 78:17

And they [chata'](../../strongs/h/h2398.md) yet [yāsap̄](../../strongs/h/h3254.md) against him by [marah](../../strongs/h/h4784.md) the most ['elyown](../../strongs/h/h5945.md) in the [ṣîyâ](../../strongs/h/h6723.md).

<a name="psalms_78_18"></a>Psalms 78:18

And they [nāsâ](../../strongs/h/h5254.md) ['el](../../strongs/h/h410.md) in their [lebab](../../strongs/h/h3824.md) by [sha'al](../../strongs/h/h7592.md) ['ōḵel](../../strongs/h/h400.md) for their [nephesh](../../strongs/h/h5315.md).

<a name="psalms_78_19"></a>Psalms 78:19

Yea, they [dabar](../../strongs/h/h1696.md) against ['Elohiym](../../strongs/h/h430.md); they ['āmar](../../strongs/h/h559.md), [yakol](../../strongs/h/h3201.md) ['el](../../strongs/h/h410.md) ['arak](../../strongs/h/h6186.md) a [šulḥān](../../strongs/h/h7979.md) in the [midbar](../../strongs/h/h4057.md) ?

<a name="psalms_78_20"></a>Psalms 78:20

Behold, he [nakah](../../strongs/h/h5221.md) the [tsuwr](../../strongs/h/h6697.md), that the [mayim](../../strongs/h/h4325.md) [zûḇ](../../strongs/h/h2100.md), and the [nachal](../../strongs/h/h5158.md) [šāṭap̄](../../strongs/h/h7857.md); [yakol](../../strongs/h/h3201.md) he [nathan](../../strongs/h/h5414.md) [lechem](../../strongs/h/h3899.md) also? [yakol](../../strongs/h/h3201.md) he [kuwn](../../strongs/h/h3559.md) [šᵊ'ēr](../../strongs/h/h7607.md) for his ['am](../../strongs/h/h5971.md) ?

<a name="psalms_78_21"></a>Psalms 78:21

Therefore [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) this, and was ['abar](../../strongs/h/h5674.md): so an ['esh](../../strongs/h/h784.md) was [nāśaq](../../strongs/h/h5400.md) against [Ya'aqob](../../strongs/h/h3290.md), and ['aph](../../strongs/h/h639.md) also came [ʿālâ](../../strongs/h/h5927.md) against [Yisra'el](../../strongs/h/h3478.md);

<a name="psalms_78_22"></a>Psalms 78:22

Because they ['aman](../../strongs/h/h539.md) not in ['Elohiym](../../strongs/h/h430.md), and [batach](../../strongs/h/h982.md) not in his [yĕshuw'ah](../../strongs/h/h3444.md):

<a name="psalms_78_23"></a>Psalms 78:23

Though he had [tsavah](../../strongs/h/h6680.md) the [shachaq](../../strongs/h/h7834.md) from [maʿal](../../strongs/h/h4605.md), and [pāṯaḥ](../../strongs/h/h6605.md) the [deleṯ](../../strongs/h/h1817.md) of [shamayim](../../strongs/h/h8064.md),

<a name="psalms_78_24"></a>Psalms 78:24

And had [matar](../../strongs/h/h4305.md) [man](../../strongs/h/h4478.md) upon them to ['akal](../../strongs/h/h398.md), and had [nathan](../../strongs/h/h5414.md) them of the [dagan](../../strongs/h/h1715.md) of [shamayim](../../strongs/h/h8064.md).

<a name="psalms_78_25"></a>Psalms 78:25

['iysh](../../strongs/h/h376.md) did ['akal](../../strongs/h/h398.md) ['abîr](../../strongs/h/h47.md) [lechem](../../strongs/h/h3899.md): he [shalach](../../strongs/h/h7971.md) them [ṣêḏâ](../../strongs/h/h6720.md) to the [śōḇaʿ](../../strongs/h/h7648.md).

<a name="psalms_78_26"></a>Psalms 78:26

He caused a [qāḏîm](../../strongs/h/h6921.md) to [nāsaʿ](../../strongs/h/h5265.md) in the [shamayim](../../strongs/h/h8064.md): and by his ['oz](../../strongs/h/h5797.md) he [nāhaḡ](../../strongs/h/h5090.md) the [têmān](../../strongs/h/h8486.md).

<a name="psalms_78_27"></a>Psalms 78:27

He [matar](../../strongs/h/h4305.md) [šᵊ'ēr](../../strongs/h/h7607.md) also upon them as ['aphar](../../strongs/h/h6083.md), and [kanaph](../../strongs/h/h3671.md) [ʿôp̄](../../strongs/h/h5775.md) like as the [ḥôl](../../strongs/h/h2344.md) of the [yam](../../strongs/h/h3220.md):

<a name="psalms_78_28"></a>Psalms 78:28

And he let it [naphal](../../strongs/h/h5307.md) in the [qereḇ](../../strongs/h/h7130.md) of their [maḥănê](../../strongs/h/h4264.md), [cabiyb](../../strongs/h/h5439.md) their [miškān](../../strongs/h/h4908.md).

<a name="psalms_78_29"></a>Psalms 78:29

So they did ['akal](../../strongs/h/h398.md), and were [me'od](../../strongs/h/h3966.md) [sāׂbaʿ](../../strongs/h/h7646.md): for he [bow'](../../strongs/h/h935.md) them their own [ta'avah](../../strongs/h/h8378.md);

<a name="psalms_78_30"></a>Psalms 78:30

They were not [zûr](../../strongs/h/h2114.md) from their [ta'avah](../../strongs/h/h8378.md). But while their ['ōḵel](../../strongs/h/h400.md) was yet in their [peh](../../strongs/h/h6310.md),

<a name="psalms_78_31"></a>Psalms 78:31

The ['aph](../../strongs/h/h639.md) of ['Elohiym](../../strongs/h/h430.md) [ʿālâ](../../strongs/h/h5927.md) upon them, and [harag](../../strongs/h/h2026.md) the [mišmān](../../strongs/h/h4924.md) of them, and smote [kara'](../../strongs/h/h3766.md) the [bāḥûr](../../strongs/h/h970.md) men of [Yisra'el](../../strongs/h/h3478.md).

<a name="psalms_78_32"></a>Psalms 78:32

For all this they [chata'](../../strongs/h/h2398.md) still, and ['aman](../../strongs/h/h539.md) not for his [pala'](../../strongs/h/h6381.md).

<a name="psalms_78_33"></a>Psalms 78:33

Therefore their [yowm](../../strongs/h/h3117.md) did he [kalah](../../strongs/h/h3615.md) in [heḇel](../../strongs/h/h1892.md), and their [šānâ](../../strongs/h/h8141.md) in [behālâ](../../strongs/h/h928.md).

<a name="psalms_78_34"></a>Psalms 78:34

When he [harag](../../strongs/h/h2026.md) them, then they [darash](../../strongs/h/h1875.md) him: and they [shuwb](../../strongs/h/h7725.md) and [šāḥar](../../strongs/h/h7836.md) after ['el](../../strongs/h/h410.md).

<a name="psalms_78_35"></a>Psalms 78:35

And they [zakar](../../strongs/h/h2142.md) that ['Elohiym](../../strongs/h/h430.md) was their [tsuwr](../../strongs/h/h6697.md), and the ['elyown](../../strongs/h/h5945.md) ['el](../../strongs/h/h410.md) their [gā'al](../../strongs/h/h1350.md).

<a name="psalms_78_36"></a>Psalms 78:36

Nevertheless they did [pāṯâ](../../strongs/h/h6601.md) him with their [peh](../../strongs/h/h6310.md), and they [kāzaḇ](../../strongs/h/h3576.md) unto him with their [lashown](../../strongs/h/h3956.md).

<a name="psalms_78_37"></a>Psalms 78:37

For their [leb](../../strongs/h/h3820.md) was not [kuwn](../../strongs/h/h3559.md) with him, neither were they ['aman](../../strongs/h/h539.md) in his [bĕriyth](../../strongs/h/h1285.md).

<a name="psalms_78_38"></a>Psalms 78:38

But he, being full of [raḥwm](../../strongs/h/h7349.md), [kāp̄ar](../../strongs/h/h3722.md) their ['avon](../../strongs/h/h5771.md), and [shachath](../../strongs/h/h7843.md) them not: yea, many a [rabah](../../strongs/h/h7235.md) [shuwb](../../strongs/h/h7725.md) he his ['aph](../../strongs/h/h639.md) [shuwb](../../strongs/h/h7725.md), and did not [ʿûr](../../strongs/h/h5782.md) all his [chemah](../../strongs/h/h2534.md).

<a name="psalms_78_39"></a>Psalms 78:39

For he [zakar](../../strongs/h/h2142.md) that they were but [basar](../../strongs/h/h1320.md); a [ruwach](../../strongs/h/h7307.md) that passeth [halak](../../strongs/h/h1980.md), and [shuwb](../../strongs/h/h7725.md) not.

<a name="psalms_78_40"></a>Psalms 78:40

[mah](../../strongs/h/h4100.md) oft did they [marah](../../strongs/h/h4784.md) him in the [midbar](../../strongs/h/h4057.md), and [ʿāṣaḇ](../../strongs/h/h6087.md) him in the [yᵊšîmôn](../../strongs/h/h3452.md)!

<a name="psalms_78_41"></a>Psalms 78:41

Yea, they [shuwb](../../strongs/h/h7725.md) and [nāsâ](../../strongs/h/h5254.md) ['el](../../strongs/h/h410.md), and [tāvâ](../../strongs/h/h8428.md) the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="psalms_78_42"></a>Psalms 78:42

They [zakar](../../strongs/h/h2142.md) not his [yad](../../strongs/h/h3027.md), nor the [yowm](../../strongs/h/h3117.md) when he [pāḏâ](../../strongs/h/h6299.md) them from the [tsar](../../strongs/h/h6862.md).

<a name="psalms_78_43"></a>Psalms 78:43

How he had [śûm](../../strongs/h/h7760.md) his ['ôṯ](../../strongs/h/h226.md) in [Mitsrayim](../../strongs/h/h4714.md), and his [môp̄ēṯ](../../strongs/h/h4159.md) in the [sadeh](../../strongs/h/h7704.md) of [Ṣōʿan](../../strongs/h/h6814.md):

<a name="psalms_78_44"></a>Psalms 78:44

And had [hāp̄aḵ](../../strongs/h/h2015.md) their [yᵊ'ōr](../../strongs/h/h2975.md) into [dam](../../strongs/h/h1818.md); and their [nāzal](../../strongs/h/h5140.md), that they could not [šāṯâ](../../strongs/h/h8354.md).

<a name="psalms_78_45"></a>Psalms 78:45

He [shalach](../../strongs/h/h7971.md) [ʿārōḇ](../../strongs/h/h6157.md) among them, which ['akal](../../strongs/h/h398.md) them; and [ṣᵊp̄ardēaʿ](../../strongs/h/h6854.md), which [shachath](../../strongs/h/h7843.md) them.

<a name="psalms_78_46"></a>Psalms 78:46

He [nathan](../../strongs/h/h5414.md) also their [yᵊḇûl](../../strongs/h/h2981.md) unto the [ḥāsîl](../../strongs/h/h2625.md), and their [yᵊḡîaʿ](../../strongs/h/h3018.md) unto the ['arbê](../../strongs/h/h697.md).

<a name="psalms_78_47"></a>Psalms 78:47

He [harag](../../strongs/h/h2026.md) their [gep̄en](../../strongs/h/h1612.md) with [barad](../../strongs/h/h1259.md), and their [šiqmâ](../../strongs/h/h8256.md) with [ḥănāmāl](../../strongs/h/h2602.md).

<a name="psalms_78_48"></a>Psalms 78:48

He [cagar](../../strongs/h/h5462.md) their [bᵊʿîr](../../strongs/h/h1165.md) also to the [barad](../../strongs/h/h1259.md), and their [miqnê](../../strongs/h/h4735.md) to [rešep̄](../../strongs/h/h7565.md).

<a name="psalms_78_49"></a>Psalms 78:49

He [shalach](../../strongs/h/h7971.md) upon them the [charown](../../strongs/h/h2740.md) of his ['aph](../../strongs/h/h639.md), ['ebrah](../../strongs/h/h5678.md), and [zaʿam](../../strongs/h/h2195.md), and [tsarah](../../strongs/h/h6869.md), by [mišlaḥaṯ](../../strongs/h/h4917.md) [ra'](../../strongs/h/h7451.md) [mal'ak](../../strongs/h/h4397.md) among them.

<a name="psalms_78_50"></a>Psalms 78:50

He [pālas](../../strongs/h/h6424.md) a [nāṯîḇ](../../strongs/h/h5410.md) to his ['aph](../../strongs/h/h639.md); he [ḥāśaḵ](../../strongs/h/h2820.md) not their [nephesh](../../strongs/h/h5315.md) from [maveth](../../strongs/h/h4194.md), but [cagar](../../strongs/h/h5462.md) their [chay](../../strongs/h/h2416.md) [cagar](../../strongs/h/h5462.md) to the [deḇer](../../strongs/h/h1698.md);

<a name="psalms_78_51"></a>Psalms 78:51

And [nakah](../../strongs/h/h5221.md) all the [bᵊḵôr](../../strongs/h/h1060.md) in [Mitsrayim](../../strongs/h/h4714.md); the [re'shiyth](../../strongs/h/h7225.md) of their ['ôn](../../strongs/h/h202.md) in the ['ohel](../../strongs/h/h168.md) of [Ḥām](../../strongs/h/h2526.md):

<a name="psalms_78_52"></a>Psalms 78:52

But made his own ['am](../../strongs/h/h5971.md) to go [nāsaʿ](../../strongs/h/h5265.md) like [tso'n](../../strongs/h/h6629.md), and [nāhaḡ](../../strongs/h/h5090.md) them in the [midbar](../../strongs/h/h4057.md) like a [ʿēḏer](../../strongs/h/h5739.md).

<a name="psalms_78_53"></a>Psalms 78:53

And he [nachah](../../strongs/h/h5148.md) them on [betach](../../strongs/h/h983.md), so that they [pachad](../../strongs/h/h6342.md) not: but the [yam](../../strongs/h/h3220.md) [kāsâ](../../strongs/h/h3680.md) their ['oyeb](../../strongs/h/h341.md).

<a name="psalms_78_54"></a>Psalms 78:54

And he [bow'](../../strongs/h/h935.md) them to the [gᵊḇûl](../../strongs/h/h1366.md) of his [qodesh](../../strongs/h/h6944.md), even to this [har](../../strongs/h/h2022.md), which his [yamiyn](../../strongs/h/h3225.md) had [qānâ](../../strongs/h/h7069.md).

<a name="psalms_78_55"></a>Psalms 78:55

He [gāraš](../../strongs/h/h1644.md) the [gowy](../../strongs/h/h1471.md) also [paniym](../../strongs/h/h6440.md) them, and [naphal](../../strongs/h/h5307.md) them a [nachalah](../../strongs/h/h5159.md) by [chebel](../../strongs/h/h2256.md), and made the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) to [shakan](../../strongs/h/h7931.md) in their ['ohel](../../strongs/h/h168.md).

<a name="psalms_78_56"></a>Psalms 78:56

Yet they [nāsâ](../../strongs/h/h5254.md) and [marah](../../strongs/h/h4784.md) the ['elyown](../../strongs/h/h5945.md) ['Elohiym](../../strongs/h/h430.md), and [shamar](../../strongs/h/h8104.md) not his [ʿēḏâ](../../strongs/h/h5713.md):

<a name="psalms_78_57"></a>Psalms 78:57

But [sûḡ](../../strongs/h/h5472.md), and [bāḡaḏ](../../strongs/h/h898.md) like their ['ab](../../strongs/h/h1.md): they were [hāp̄aḵ](../../strongs/h/h2015.md) like a [rᵊmîyâ](../../strongs/h/h7423.md) [qesheth](../../strongs/h/h7198.md).

<a name="psalms_78_58"></a>Psalms 78:58

For they [kāʿas](../../strongs/h/h3707.md) him with their [bāmâ](../../strongs/h/h1116.md), and [qānā'](../../strongs/h/h7065.md) him with their [pāsîl](../../strongs/h/h6456.md).

<a name="psalms_78_59"></a>Psalms 78:59

When ['Elohiym](../../strongs/h/h430.md) [shama'](../../strongs/h/h8085.md) this, he was ['abar](../../strongs/h/h5674.md), and [me'od](../../strongs/h/h3966.md) [mā'as](../../strongs/h/h3988.md) [Yisra'el](../../strongs/h/h3478.md):

<a name="psalms_78_60"></a>Psalms 78:60

So that he [nāṭaš](../../strongs/h/h5203.md) the [miškān](../../strongs/h/h4908.md) of [Šîlô](../../strongs/h/h7887.md), the ['ohel](../../strongs/h/h168.md) which he [shakan](../../strongs/h/h7931.md) among ['āḏām](../../strongs/h/h120.md);

<a name="psalms_78_61"></a>Psalms 78:61

And [nathan](../../strongs/h/h5414.md) his ['oz](../../strongs/h/h5797.md) into [šᵊḇî](../../strongs/h/h7628.md), and his [tip̄'ārâ](../../strongs/h/h8597.md) into the [tsar](../../strongs/h/h6862.md) [yad](../../strongs/h/h3027.md).

<a name="psalms_78_62"></a>Psalms 78:62

He [cagar](../../strongs/h/h5462.md) his ['am](../../strongs/h/h5971.md) [cagar](../../strongs/h/h5462.md) also unto the [chereb](../../strongs/h/h2719.md); and was ['abar](../../strongs/h/h5674.md) with his [nachalah](../../strongs/h/h5159.md).

<a name="psalms_78_63"></a>Psalms 78:63

The ['esh](../../strongs/h/h784.md) ['akal](../../strongs/h/h398.md) their young [bāḥûr](../../strongs/h/h970.md); and their [bᵊṯûlâ](../../strongs/h/h1330.md) were not given to [halal](../../strongs/h/h1984.md).

<a name="psalms_78_64"></a>Psalms 78:64

Their [kōhēn](../../strongs/h/h3548.md) [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md); and their ['almānâ](../../strongs/h/h490.md) made no [bāḵâ](../../strongs/h/h1058.md).

<a name="psalms_78_65"></a>Psalms 78:65

Then the ['adonay](../../strongs/h/h136.md) [yāqaṣ](../../strongs/h/h3364.md) as one out of [yāšēn](../../strongs/h/h3463.md), and like a mighty [gibôr](../../strongs/h/h1368.md) that [ranan](../../strongs/h/h7442.md) by reason of [yayin](../../strongs/h/h3196.md).

<a name="psalms_78_66"></a>Psalms 78:66

And he [nakah](../../strongs/h/h5221.md) his [tsar](../../strongs/h/h6862.md) in the ['āḥôr](../../strongs/h/h268.md): he [nathan](../../strongs/h/h5414.md) them to a ['owlam](../../strongs/h/h5769.md) [cherpah](../../strongs/h/h2781.md).

<a name="psalms_78_67"></a>Psalms 78:67

Moreover he [mā'as](../../strongs/h/h3988.md) the ['ohel](../../strongs/h/h168.md) of [Yôsēp̄](../../strongs/h/h3130.md), and [bāḥar](../../strongs/h/h977.md) not the [shebet](../../strongs/h/h7626.md) of ['Ep̄rayim](../../strongs/h/h669.md):

<a name="psalms_78_68"></a>Psalms 78:68

But [bāḥar](../../strongs/h/h977.md) the [shebet](../../strongs/h/h7626.md) of [Yehuwdah](../../strongs/h/h3063.md), the [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md) which he ['ahab](../../strongs/h/h157.md).

<a name="psalms_78_69"></a>Psalms 78:69

And he [bānâ](../../strongs/h/h1129.md) his [miqdash](../../strongs/h/h4720.md) like [ruwm](../../strongs/h/h7311.md) palaces, like the ['erets](../../strongs/h/h776.md) which he hath [yacad](../../strongs/h/h3245.md) for ['owlam](../../strongs/h/h5769.md).

<a name="psalms_78_70"></a>Psalms 78:70

He [bāḥar](../../strongs/h/h977.md) [Dāviḏ](../../strongs/h/h1732.md) also his ['ebed](../../strongs/h/h5650.md), and [laqach](../../strongs/h/h3947.md) him from the [miḵlâ](../../strongs/h/h4356.md) [tso'n](../../strongs/h/h6629.md):

<a name="psalms_78_71"></a>Psalms 78:71

From ['aḥar](../../strongs/h/h310.md) the [ʿûl](../../strongs/h/h5763.md) he [bow'](../../strongs/h/h935.md) him to [ra'ah](../../strongs/h/h7462.md) [Ya'aqob](../../strongs/h/h3290.md) his ['am](../../strongs/h/h5971.md), and [Yisra'el](../../strongs/h/h3478.md) his [nachalah](../../strongs/h/h5159.md).

<a name="psalms_78_72"></a>Psalms 78:72

So he [ra'ah](../../strongs/h/h7462.md) them according to the [tom](../../strongs/h/h8537.md) of his [lebab](../../strongs/h/h3824.md); and [nachah](../../strongs/h/h5148.md) them by the [tāḇûn](../../strongs/h/h8394.md) of his [kaph](../../strongs/h/h3709.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 77](psalms_77.md) - [Psalms 79](psalms_79.md)