# [Psalms 52](https://www.blueletterbible.org/kjv/psa/52)

<a name="psalms_52_1"></a>Psalms 52:1

To the [nāṣaḥ](../../strongs/h/h5329.md), [Maśkîl](../../strongs/h/h4905.md), [A Psalm] of [Dāviḏ](../../strongs/h/h1732.md), when [Dō'Ēḡ](../../strongs/h/h1673.md) the ['Ăḏōmî](../../strongs/h/h130.md) [bow'](../../strongs/h/h935.md) and [nāḡaḏ](../../strongs/h/h5046.md) [Šā'ûl](../../strongs/h/h7586.md), and ['āmar](../../strongs/h/h559.md) unto him, [Dāviḏ](../../strongs/h/h1732.md) is [bow'](../../strongs/h/h935.md) to the [bayith](../../strongs/h/h1004.md) of ['Ăḥîmeleḵ](../../strongs/h/h288.md). Why [halal](../../strongs/h/h1984.md) thou thyself in [ra'](../../strongs/h/h7451.md), O [gibôr](../../strongs/h/h1368.md)? the [checed](../../strongs/h/h2617.md) of ['el](../../strongs/h/h410.md) [yowm](../../strongs/h/h3117.md).

<a name="psalms_52_2"></a>Psalms 52:2

The [lashown](../../strongs/h/h3956.md) [chashab](../../strongs/h/h2803.md) [havvah](../../strongs/h/h1942.md); like a [lāṭaš](../../strongs/h/h3913.md) [taʿar](../../strongs/h/h8593.md), ['asah](../../strongs/h/h6213.md) [rᵊmîyâ](../../strongs/h/h7423.md).

<a name="psalms_52_3"></a>Psalms 52:3

Thou ['ahab](../../strongs/h/h157.md) [ra'](../../strongs/h/h7451.md) more than [towb](../../strongs/h/h2896.md); and [sheqer](../../strongs/h/h8267.md) rather than to [dabar](../../strongs/h/h1696.md) [tsedeq](../../strongs/h/h6664.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_52_4"></a>Psalms 52:4

Thou ['ahab](../../strongs/h/h157.md) all [belaʿ](../../strongs/h/h1105.md) [dabar](../../strongs/h/h1697.md), O thou [mirmah](../../strongs/h/h4820.md) [lashown](../../strongs/h/h3956.md).

<a name="psalms_52_5"></a>Psalms 52:5

['el](../../strongs/h/h410.md) shall likewise [nāṯaṣ](../../strongs/h/h5422.md) thee [netsach](../../strongs/h/h5331.md), he shall [ḥāṯâ](../../strongs/h/h2846.md) thee, and [nāsaḥ](../../strongs/h/h5255.md) thee of thy ['ohel](../../strongs/h/h168.md), and [šērēš](../../strongs/h/h8327.md) thee of the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_52_6"></a>Psalms 52:6

The [tsaddiyq](../../strongs/h/h6662.md) also shall [ra'ah](../../strongs/h/h7200.md), and [yare'](../../strongs/h/h3372.md), and shall [śāḥaq](../../strongs/h/h7832.md) at him:

<a name="psalms_52_7"></a>Psalms 52:7

Lo, this is the [geḇer](../../strongs/h/h1397.md) that [śûm](../../strongs/h/h7760.md) not ['Elohiym](../../strongs/h/h430.md) his [māʿôz](../../strongs/h/h4581.md); but [batach](../../strongs/h/h982.md) in the [rōḇ](../../strongs/h/h7230.md) of his [ʿōšer](../../strongs/h/h6239.md), and ['azaz](../../strongs/h/h5810.md) himself in his [havvah](../../strongs/h/h1942.md).

<a name="psalms_52_8"></a>Psalms 52:8

But I am like a [raʿănān](../../strongs/h/h7488.md) [zayiṯ](../../strongs/h/h2132.md) in the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md): I [batach](../../strongs/h/h982.md) in the [checed](../../strongs/h/h2617.md) of ['Elohiym](../../strongs/h/h430.md) ['owlam](../../strongs/h/h5769.md) and [ʿaḏ](../../strongs/h/h5703.md).

<a name="psalms_52_9"></a>Psalms 52:9

I will [yadah](../../strongs/h/h3034.md) thee ['owlam](../../strongs/h/h5769.md), because thou hast ['asah](../../strongs/h/h6213.md) it: and I will [qāvâ](../../strongs/h/h6960.md) on thy [shem](../../strongs/h/h8034.md); for it is [towb](../../strongs/h/h2896.md) before thy [chaciyd](../../strongs/h/h2623.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 51](psalms_51.md) - [Psalms 53](psalms_53.md)