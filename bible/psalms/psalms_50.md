# [Psalms 50](https://www.blueletterbible.org/kjv/psa/50)

<a name="psalms_50_1"></a>Psalms 50:1

A [mizmôr](../../strongs/h/h4210.md) of ['Āsāp̄](../../strongs/h/h623.md). The ['el](../../strongs/h/h410.md) ['Elohiym](../../strongs/h/h430.md), even [Yĕhovah](../../strongs/h/h3068.md), hath [dabar](../../strongs/h/h1696.md), and [qara'](../../strongs/h/h7121.md) the ['erets](../../strongs/h/h776.md) from the [mizrach](../../strongs/h/h4217.md) of the [šemeš](../../strongs/h/h8121.md) unto the [māḇô'](../../strongs/h/h3996.md) thereof.

<a name="psalms_50_2"></a>Psalms 50:2

Out of [Tsiyown](../../strongs/h/h6726.md), the [miḵlāl](../../strongs/h/h4359.md) of [yᵊp̄î](../../strongs/h/h3308.md), ['Elohiym](../../strongs/h/h430.md) hath [yāp̄aʿ](../../strongs/h/h3313.md).

<a name="psalms_50_3"></a>Psalms 50:3

Our ['Elohiym](../../strongs/h/h430.md) shall [bow'](../../strongs/h/h935.md), and shall not [ḥāraš](../../strongs/h/h2790.md): an ['esh](../../strongs/h/h784.md) shall ['akal](../../strongs/h/h398.md) [paniym](../../strongs/h/h6440.md) him, and it shall be [me'od](../../strongs/h/h3966.md) [śāʿar](../../strongs/h/h8175.md) [cabiyb](../../strongs/h/h5439.md) him.

<a name="psalms_50_4"></a>Psalms 50:4

He shall [qara'](../../strongs/h/h7121.md) to the [shamayim](../../strongs/h/h8064.md) from [ʿal](../../strongs/h/h5920.md), and to the ['erets](../../strongs/h/h776.md), that he may [diyn](../../strongs/h/h1777.md) his ['am](../../strongs/h/h5971.md).

<a name="psalms_50_5"></a>Psalms 50:5

['āsap̄](../../strongs/h/h622.md) my [chaciyd](../../strongs/h/h2623.md) together unto me; those that have [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with me by [zebach](../../strongs/h/h2077.md).

<a name="psalms_50_6"></a>Psalms 50:6

And the [shamayim](../../strongs/h/h8064.md) shall [nāḡaḏ](../../strongs/h/h5046.md) his [tsedeq](../../strongs/h/h6664.md): for ['Elohiym](../../strongs/h/h430.md) is [shaphat](../../strongs/h/h8199.md) himself. [Celah](../../strongs/h/h5542.md).

<a name="psalms_50_7"></a>Psalms 50:7

[shama'](../../strongs/h/h8085.md), O my ['am](../../strongs/h/h5971.md), and I will [dabar](../../strongs/h/h1696.md); O [Yisra'el](../../strongs/h/h3478.md), and I will [ʿûḏ](../../strongs/h/h5749.md) against thee: I am ['Elohiym](../../strongs/h/h430.md), even thy ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_50_8"></a>Psalms 50:8

I will not [yakach](../../strongs/h/h3198.md) thee for thy [zebach](../../strongs/h/h2077.md) or thy an [ʿōlâ](../../strongs/h/h5930.md), to have been [tāmîḏ](../../strongs/h/h8548.md) before me.

<a name="psalms_50_9"></a>Psalms 50:9

I will [laqach](../../strongs/h/h3947.md) no [par](../../strongs/h/h6499.md) out of thy [bayith](../../strongs/h/h1004.md), nor [ʿatûḏ](../../strongs/h/h6260.md) out of thy [miḵlâ](../../strongs/h/h4356.md).

<a name="psalms_50_10"></a>Psalms 50:10

For every [chay](../../strongs/h/h2416.md) of the [yaʿar](../../strongs/h/h3293.md) is mine, and the [bĕhemah](../../strongs/h/h929.md) upon an ['elep̄](../../strongs/h/h505.md) [har](../../strongs/h/h2042.md).

<a name="psalms_50_11"></a>Psalms 50:11

I [yada'](../../strongs/h/h3045.md) all the [ʿôp̄](../../strongs/h/h5775.md) of the [har](../../strongs/h/h2022.md): and the [zîz](../../strongs/h/h2123.md) of the [sadeh](../../strongs/h/h7704.md) are mine.

<a name="psalms_50_12"></a>Psalms 50:12

If I were [rāʿēḇ](../../strongs/h/h7456.md), I would not ['āmar](../../strongs/h/h559.md) thee: for the [tebel](../../strongs/h/h8398.md) is mine, and the [mᵊlō'](../../strongs/h/h4393.md) thereof.

<a name="psalms_50_13"></a>Psalms 50:13

Will I ['akal](../../strongs/h/h398.md) the [basar](../../strongs/h/h1320.md) of ['abîr](../../strongs/h/h47.md), or [šāṯâ](../../strongs/h/h8354.md) the [dam](../../strongs/h/h1818.md) of [ʿatûḏ](../../strongs/h/h6260.md)?

<a name="psalms_50_14"></a>Psalms 50:14

[zabach](../../strongs/h/h2076.md) unto ['Elohiym](../../strongs/h/h430.md) [tôḏâ](../../strongs/h/h8426.md); and [shalam](../../strongs/h/h7999.md) thy [neḏer](../../strongs/h/h5088.md) unto the ['elyown](../../strongs/h/h5945.md):

<a name="psalms_50_15"></a>Psalms 50:15

And [qara'](../../strongs/h/h7121.md) upon me in the [yowm](../../strongs/h/h3117.md) of [tsarah](../../strongs/h/h6869.md): I will [chalats](../../strongs/h/h2502.md) thee, and thou shalt [kabad](../../strongs/h/h3513.md) me.

<a name="psalms_50_16"></a>Psalms 50:16

But unto the [rasha'](../../strongs/h/h7563.md) ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), What hast thou to do to [sāp̄ar](../../strongs/h/h5608.md) my [choq](../../strongs/h/h2706.md), or that thou shouldest [nasa'](../../strongs/h/h5375.md) my [bĕriyth](../../strongs/h/h1285.md) in thy [peh](../../strongs/h/h6310.md)?

<a name="psalms_50_17"></a>Psalms 50:17

Seeing thou [sane'](../../strongs/h/h8130.md) [mûsār](../../strongs/h/h4148.md), and [shalak](../../strongs/h/h7993.md) my [dabar](../../strongs/h/h1697.md) ['aḥar](../../strongs/h/h310.md) thee.

<a name="psalms_50_18"></a>Psalms 50:18

When thou [ra'ah](../../strongs/h/h7200.md) a [gannāḇ](../../strongs/h/h1590.md), then thou [ratsah](../../strongs/h/h7521.md) with him, and hast been [cheleq](../../strongs/h/h2506.md) with [na'aph](../../strongs/h/h5003.md).

<a name="psalms_50_19"></a>Psalms 50:19

Thou [shalach](../../strongs/h/h7971.md) thy [peh](../../strongs/h/h6310.md) to [ra'](../../strongs/h/h7451.md), and thy [lashown](../../strongs/h/h3956.md) [ṣāmaḏ](../../strongs/h/h6775.md) [mirmah](../../strongs/h/h4820.md).

<a name="psalms_50_20"></a>Psalms 50:20

Thou [yashab](../../strongs/h/h3427.md) and [dabar](../../strongs/h/h1696.md) against thy ['ach](../../strongs/h/h251.md); thou [dŏp̄î](../../strongs/h/h1848.md) thine own ['em](../../strongs/h/h517.md) [ben](../../strongs/h/h1121.md).

<a name="psalms_50_21"></a>Psalms 50:21

These things hast thou ['asah](../../strongs/h/h6213.md), and I [ḥāraš](../../strongs/h/h2790.md); thou [dāmâ](../../strongs/h/h1819.md) that I was [hayah](../../strongs/h/h1961.md) such an one as thyself: but I will [yakach](../../strongs/h/h3198.md) thee, and ['arak](../../strongs/h/h6186.md) them before thine ['ayin](../../strongs/h/h5869.md).

<a name="psalms_50_22"></a>Psalms 50:22

Now [bîn](../../strongs/h/h995.md) this, ye that [shakach](../../strongs/h/h7911.md) ['ĕlvôha](../../strongs/h/h433.md), lest I [taraph](../../strongs/h/h2963.md), and there be none to [natsal](../../strongs/h/h5337.md).

<a name="psalms_50_23"></a>Psalms 50:23

Whoso [zabach](../../strongs/h/h2076.md) [tôḏâ](../../strongs/h/h8426.md) [kabad](../../strongs/h/h3513.md) me: and to him that [śûm](../../strongs/h/h7760.md) his [derek](../../strongs/h/h1870.md) will I [ra'ah](../../strongs/h/h7200.md) the [yesha'](../../strongs/h/h3468.md) of ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 49](psalms_49.md) - [Psalms 51](psalms_51.md)