# [Psalms 64](https://www.blueletterbible.org/kjv/psa/64)

<a name="psalms_64_1"></a>Psalms 64:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md), ['Elohiym](../../strongs/h/h430.md), in my [śîaḥ](../../strongs/h/h7879.md): [nāṣar](../../strongs/h/h5341.md) my [chay](../../strongs/h/h2416.md) from [paḥaḏ](../../strongs/h/h6343.md) of the ['oyeb](../../strongs/h/h341.md).

<a name="psalms_64_2"></a>Psalms 64:2

[cathar](../../strongs/h/h5641.md) me from the [sôḏ](../../strongs/h/h5475.md) of the [ra'a'](../../strongs/h/h7489.md); from the [reḡeš](../../strongs/h/h7285.md) of the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md):

<a name="psalms_64_3"></a>Psalms 64:3

Who [šānan](../../strongs/h/h8150.md) their [lashown](../../strongs/h/h3956.md) like a [chereb](../../strongs/h/h2719.md), and [dāraḵ](../../strongs/h/h1869.md) their [chets](../../strongs/h/h2671.md), even [mar](../../strongs/h/h4751.md) [dabar](../../strongs/h/h1697.md):

<a name="psalms_64_4"></a>Psalms 64:4

That they may [yārâ](../../strongs/h/h3384.md) in [mictar](../../strongs/h/h4565.md) at the [tām](../../strongs/h/h8535.md): [piṯ'ōm](../../strongs/h/h6597.md) do they [yārâ](../../strongs/h/h3384.md) at him, and [yare'](../../strongs/h/h3372.md) not.

<a name="psalms_64_5"></a>Psalms 64:5

They [ḥāzaq](../../strongs/h/h2388.md) themselves in a [ra'](../../strongs/h/h7451.md) [dabar](../../strongs/h/h1697.md): they [sāp̄ar](../../strongs/h/h5608.md) of [taman](../../strongs/h/h2934.md) [mowqesh](../../strongs/h/h4170.md); they ['āmar](../../strongs/h/h559.md), Who shall [ra'ah](../../strongs/h/h7200.md) them?

<a name="psalms_64_6"></a>Psalms 64:6

They [ḥāp̄aś](../../strongs/h/h2664.md) ['evel](../../strongs/h/h5766.md); they [tamam](../../strongs/h/h8552.md) a [ḥāp̄aś](../../strongs/h/h2664.md) [ḥēp̄eś](../../strongs/h/h2665.md): both the [qereḇ](../../strongs/h/h7130.md) thought of every ['iysh](../../strongs/h/h376.md) of them, and the [leb](../../strongs/h/h3820.md), is [ʿāmōq](../../strongs/h/h6013.md).

<a name="psalms_64_7"></a>Psalms 64:7

But ['Elohiym](../../strongs/h/h430.md) shall [yārâ](../../strongs/h/h3384.md) at them with a [chets](../../strongs/h/h2671.md); [piṯ'ōm](../../strongs/h/h6597.md) shall they be [makâ](../../strongs/h/h4347.md).

<a name="psalms_64_8"></a>Psalms 64:8

So they shall make their own [lashown](../../strongs/h/h3956.md) to [kashal](../../strongs/h/h3782.md) upon themselves: all that [ra'ah](../../strongs/h/h7200.md) them shall [nāḏaḏ](../../strongs/h/h5074.md).

<a name="psalms_64_9"></a>Psalms 64:9

And all ['adam](../../strongs/h/h120.md) shall [yare'](../../strongs/h/h3372.md), and shall [nāḡaḏ](../../strongs/h/h5046.md) the [pōʿal](../../strongs/h/h6467.md) of ['Elohiym](../../strongs/h/h430.md); for they shall [sakal](../../strongs/h/h7919.md) of his [ma'aseh](../../strongs/h/h4639.md).

<a name="psalms_64_10"></a>Psalms 64:10

The [tsaddiyq](../../strongs/h/h6662.md) shall be [samach](../../strongs/h/h8055.md) in [Yĕhovah](../../strongs/h/h3068.md), and shall [chacah](../../strongs/h/h2620.md) in him; and all the [yashar](../../strongs/h/h3477.md) in [leb](../../strongs/h/h3820.md) shall [halal](../../strongs/h/h1984.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 63](psalms_63.md) - [Psalms 65](psalms_65.md)