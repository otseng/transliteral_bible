# [Psalms 89](https://www.blueletterbible.org/kjv/psalms/89)

<a name="psalms_89_1"></a>Psalms 89:1

[Maśkîl](../../strongs/h/h4905.md) of ['Êṯān](../../strongs/h/h387.md) the ['ezrāḥî](../../strongs/h/h250.md). I will [shiyr](../../strongs/h/h7891.md) of the [checed](../../strongs/h/h2617.md) of [Yĕhovah](../../strongs/h/h3068.md) for ['owlam](../../strongs/h/h5769.md): with my [peh](../../strongs/h/h6310.md) will I make [yada'](../../strongs/h/h3045.md) thy ['ĕmûnâ](../../strongs/h/h530.md) to [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md).

<a name="psalms_89_2"></a>Psalms 89:2

For I have ['āmar](../../strongs/h/h559.md), [checed](../../strongs/h/h2617.md) shall be [bānâ](../../strongs/h/h1129.md) for ['owlam](../../strongs/h/h5769.md): thy ['ĕmûnâ](../../strongs/h/h530.md) shalt thou [kuwn](../../strongs/h/h3559.md) in the very [shamayim](../../strongs/h/h8064.md).

<a name="psalms_89_3"></a>Psalms 89:3

I have [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with my [bāḥîr](../../strongs/h/h972.md), I have [shaba'](../../strongs/h/h7650.md) unto [Dāviḏ](../../strongs/h/h1732.md) my ['ebed](../../strongs/h/h5650.md),

<a name="psalms_89_4"></a>Psalms 89:4

Thy [zera'](../../strongs/h/h2233.md) will I [kuwn](../../strongs/h/h3559.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md), and [bānâ](../../strongs/h/h1129.md) thy [kicce'](../../strongs/h/h3678.md) to [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_89_5"></a>Psalms 89:5

And the [shamayim](../../strongs/h/h8064.md) shall [yadah](../../strongs/h/h3034.md) thy [pele'](../../strongs/h/h6382.md), [Yĕhovah](../../strongs/h/h3068.md): thy ['ĕmûnâ](../../strongs/h/h530.md) also in the [qāhēl](../../strongs/h/h6951.md) of the [qadowsh](../../strongs/h/h6918.md).

<a name="psalms_89_6"></a>Psalms 89:6

For who in the [shachaq](../../strongs/h/h7834.md) can be ['arak](../../strongs/h/h6186.md) unto [Yĕhovah](../../strongs/h/h3068.md) ? who among the [ben](../../strongs/h/h1121.md) of the ['el](../../strongs/h/h410.md) can be [dāmâ](../../strongs/h/h1819.md) unto [Yĕhovah](../../strongs/h/h3068.md) ?

<a name="psalms_89_7"></a>Psalms 89:7

['el](../../strongs/h/h410.md) is [rab](../../strongs/h/h7227.md) to be [ʿāraṣ](../../strongs/h/h6206.md) in the [sôḏ](../../strongs/h/h5475.md) of the [qadowsh](../../strongs/h/h6918.md), and to be had in [yare'](../../strongs/h/h3372.md) of all them that are [cabiyb](../../strongs/h/h5439.md) him.

<a name="psalms_89_8"></a>Psalms 89:8

[Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), who is a [ḥăsîn](../../strongs/h/h2626.md) [Yahh](../../strongs/h/h3050.md) like unto thee? or to thy ['ĕmûnâ](../../strongs/h/h530.md) [cabiyb](../../strongs/h/h5439.md) thee?

<a name="psalms_89_9"></a>Psalms 89:9

Thou [mashal](../../strongs/h/h4910.md) the [ge'uwth](../../strongs/h/h1348.md) of the [yam](../../strongs/h/h3220.md): when the [gal](../../strongs/h/h1530.md) thereof [śô'](../../strongs/h/h7721.md), thou [shabach](../../strongs/h/h7623.md) them.

<a name="psalms_89_10"></a>Psalms 89:10

Thou hast [dāḵā'](../../strongs/h/h1792.md) [rahaḇ](../../strongs/h/h7294.md) in [dāḵā'](../../strongs/h/h1792.md), as one that is [ḥālāl](../../strongs/h/h2491.md); thou hast [p̄zr](../../strongs/h/h6340.md) thine ['oyeb](../../strongs/h/h341.md) with thy ['oz](../../strongs/h/h5797.md) [zerowa'](../../strongs/h/h2220.md).

<a name="psalms_89_11"></a>Psalms 89:11

The [shamayim](../../strongs/h/h8064.md) are thine, the ['erets](../../strongs/h/h776.md) also is thine: as for the [tebel](../../strongs/h/h8398.md) and the [mᵊlō'](../../strongs/h/h4393.md) thereof, thou hast [yacad](../../strongs/h/h3245.md) them.

<a name="psalms_89_12"></a>Psalms 89:12

The [ṣāp̄ôn](../../strongs/h/h6828.md) and the [yamiyn](../../strongs/h/h3225.md) thou hast [bara'](../../strongs/h/h1254.md) them: [Tāḇôr](../../strongs/h/h8396.md) and [Ḥermôn](../../strongs/h/h2768.md) shall [ranan](../../strongs/h/h7442.md) in thy [shem](../../strongs/h/h8034.md).

<a name="psalms_89_13"></a>Psalms 89:13

Thou hast a [gᵊḇûrâ](../../strongs/h/h1369.md) [zerowa'](../../strongs/h/h2220.md): ['azaz](../../strongs/h/h5810.md) is thy [yad](../../strongs/h/h3027.md), and [ruwm](../../strongs/h/h7311.md) is thy right [yamiyn](../../strongs/h/h3225.md).

<a name="psalms_89_14"></a>Psalms 89:14

[tsedeq](../../strongs/h/h6664.md) and [mishpat](../../strongs/h/h4941.md) are the [māḵôn](../../strongs/h/h4349.md) of thy [kicce'](../../strongs/h/h3678.md): [checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md) shall [qadam](../../strongs/h/h6923.md) before thy [paniym](../../strongs/h/h6440.md).

<a name="psalms_89_15"></a>Psalms 89:15

['esher](../../strongs/h/h835.md) is the ['am](../../strongs/h/h5971.md) that [yada'](../../strongs/h/h3045.md) the joyful [tᵊrûʿâ](../../strongs/h/h8643.md): they shall [halak](../../strongs/h/h1980.md), [Yĕhovah](../../strongs/h/h3068.md), in the ['owr](../../strongs/h/h216.md) of thy [paniym](../../strongs/h/h6440.md).

<a name="psalms_89_16"></a>Psalms 89:16

In thy [shem](../../strongs/h/h8034.md) shall they [giyl](../../strongs/h/h1523.md) all the [yowm](../../strongs/h/h3117.md): and in thy [tsedaqah](../../strongs/h/h6666.md) shall they be [ruwm](../../strongs/h/h7311.md).

<a name="psalms_89_17"></a>Psalms 89:17

For thou art the [tip̄'ārâ](../../strongs/h/h8597.md) of their ['oz](../../strongs/h/h5797.md): and in thy [ratsown](../../strongs/h/h7522.md) our [qeren](../../strongs/h/h7161.md) shall be [ruwm](../../strongs/h/h7311.md) [ruwm](../../strongs/h/h7311.md).

<a name="psalms_89_18"></a>Psalms 89:18

For [Yĕhovah](../../strongs/h/h3068.md) is our [magen](../../strongs/h/h4043.md); and the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md) is our [melek](../../strongs/h/h4428.md).

<a name="psalms_89_19"></a>Psalms 89:19

Then thou [dabar](../../strongs/h/h1696.md) in [ḥāzôn](../../strongs/h/h2377.md) to thy holy [chaciyd](../../strongs/h/h2623.md), and ['āmar](../../strongs/h/h559.md), I have [šāvâ](../../strongs/h/h7737.md) ['ezer](../../strongs/h/h5828.md) upon one that is [gibôr](../../strongs/h/h1368.md); I have [ruwm](../../strongs/h/h7311.md) one [bāḥar](../../strongs/h/h977.md) out of the ['am](../../strongs/h/h5971.md).

<a name="psalms_89_20"></a>Psalms 89:20

I have [māṣā'](../../strongs/h/h4672.md) [Dāviḏ](../../strongs/h/h1732.md) my ['ebed](../../strongs/h/h5650.md); with my [qodesh](../../strongs/h/h6944.md) [šemen](../../strongs/h/h8081.md) have I [māšaḥ](../../strongs/h/h4886.md) him:

<a name="psalms_89_21"></a>Psalms 89:21

With whom my [yad](../../strongs/h/h3027.md) shall be [kuwn](../../strongs/h/h3559.md): mine [zerowa'](../../strongs/h/h2220.md) also shall ['amats](../../strongs/h/h553.md) him.

<a name="psalms_89_22"></a>Psalms 89:22

The ['oyeb](../../strongs/h/h341.md) shall not [nāšā'](../../strongs/h/h5378.md) upon him; nor the [ben](../../strongs/h/h1121.md) of ['evel](../../strongs/h/h5766.md) [ʿānâ](../../strongs/h/h6031.md) him.

<a name="psalms_89_23"></a>Psalms 89:23

And I will [kāṯaṯ](../../strongs/h/h3807.md) his [tsar](../../strongs/h/h6862.md) before his [paniym](../../strongs/h/h6440.md), and [nāḡap̄](../../strongs/h/h5062.md) them that [sane'](../../strongs/h/h8130.md) him.

<a name="psalms_89_24"></a>Psalms 89:24

But my ['ĕmûnâ](../../strongs/h/h530.md) and my [checed](../../strongs/h/h2617.md) shall be with him: and in my [shem](../../strongs/h/h8034.md) shall his [qeren](../../strongs/h/h7161.md) be [ruwm](../../strongs/h/h7311.md).

<a name="psalms_89_25"></a>Psalms 89:25

I will [śûm](../../strongs/h/h7760.md) his [yad](../../strongs/h/h3027.md) also in the [yam](../../strongs/h/h3220.md), and his [yamiyn](../../strongs/h/h3225.md) in the [nāhār](../../strongs/h/h5104.md).

<a name="psalms_89_26"></a>Psalms 89:26

He shall [qara'](../../strongs/h/h7121.md) unto me, Thou art my ['ab](../../strongs/h/h1.md), my ['el](../../strongs/h/h410.md), and the [tsuwr](../../strongs/h/h6697.md) of my [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="psalms_89_27"></a>Psalms 89:27

Also I will [nathan](../../strongs/h/h5414.md) him my [bᵊḵôr](../../strongs/h/h1060.md), ['elyown](../../strongs/h/h5945.md) than the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md).

<a name="psalms_89_28"></a>Psalms 89:28

My [checed](../../strongs/h/h2617.md) will I [shamar](../../strongs/h/h8104.md) for him for ['owlam](../../strongs/h/h5769.md), and my [bĕriyth](../../strongs/h/h1285.md) shall stand ['aman](../../strongs/h/h539.md) with him.

<a name="psalms_89_29"></a>Psalms 89:29

His [zera'](../../strongs/h/h2233.md) also will I [śûm](../../strongs/h/h7760.md) to endure for [ʿaḏ](../../strongs/h/h5703.md), and his [kicce'](../../strongs/h/h3678.md) as the [yowm](../../strongs/h/h3117.md) of [shamayim](../../strongs/h/h8064.md).

<a name="psalms_89_30"></a>Psalms 89:30

If his [ben](../../strongs/h/h1121.md) ['azab](../../strongs/h/h5800.md) my [towrah](../../strongs/h/h8451.md), and [yālaḵ](../../strongs/h/h3212.md) not in my [mishpat](../../strongs/h/h4941.md);

<a name="psalms_89_31"></a>Psalms 89:31

If they [ḥālal](../../strongs/h/h2490.md) my [chuqqah](../../strongs/h/h2708.md), and [shamar](../../strongs/h/h8104.md) not my [mitsvah](../../strongs/h/h4687.md);

<a name="psalms_89_32"></a>Psalms 89:32

Then will I [paqad](../../strongs/h/h6485.md) their [pesha'](../../strongs/h/h6588.md) with the [shebet](../../strongs/h/h7626.md), and their ['avon](../../strongs/h/h5771.md) with [neḡaʿ](../../strongs/h/h5061.md).

<a name="psalms_89_33"></a>Psalms 89:33

Nevertheless my [checed](../../strongs/h/h2617.md) will I not [pûr](../../strongs/h/h6331.md) from him, nor suffer my ['ĕmûnâ](../../strongs/h/h530.md) to [šāqar](../../strongs/h/h8266.md).

<a name="psalms_89_34"></a>Psalms 89:34

My [bĕriyth](../../strongs/h/h1285.md) will I not [ḥālal](../../strongs/h/h2490.md), nor [šānâ](../../strongs/h/h8138.md) the thing that is [môṣā'](../../strongs/h/h4161.md) of my [saphah](../../strongs/h/h8193.md).

<a name="psalms_89_35"></a>Psalms 89:35

['echad](../../strongs/h/h259.md) have I [shaba'](../../strongs/h/h7650.md) by my [qodesh](../../strongs/h/h6944.md) that I will not [kāzaḇ](../../strongs/h/h3576.md) unto [Dāviḏ](../../strongs/h/h1732.md).

<a name="psalms_89_36"></a>Psalms 89:36

His [zera'](../../strongs/h/h2233.md) shall endure ['owlam](../../strongs/h/h5769.md), and his [kicce'](../../strongs/h/h3678.md) as the [šemeš](../../strongs/h/h8121.md) before me.

<a name="psalms_89_37"></a>Psalms 89:37

It shall be [kuwn](../../strongs/h/h3559.md) for ['owlam](../../strongs/h/h5769.md) as the [yareach](../../strongs/h/h3394.md), and as an ['aman](../../strongs/h/h539.md) ['ed](../../strongs/h/h5707.md) in [shachaq](../../strongs/h/h7834.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_89_38"></a>Psalms 89:38

But thou hast [zānaḥ](../../strongs/h/h2186.md) and [mā'as](../../strongs/h/h3988.md), thou hast been ['abar](../../strongs/h/h5674.md) with thine [mashiyach](../../strongs/h/h4899.md).

<a name="psalms_89_39"></a>Psalms 89:39

Thou hast made [nā'ar](../../strongs/h/h5010.md) the [bĕriyth](../../strongs/h/h1285.md) of thy ['ebed](../../strongs/h/h5650.md): thou hast [ḥālal](../../strongs/h/h2490.md) his [nēzer](../../strongs/h/h5145.md) by casting it to the ['erets](../../strongs/h/h776.md).

<a name="psalms_89_40"></a>Psalms 89:40

Thou hast [pāraṣ](../../strongs/h/h6555.md) all his [gᵊḏērâ](../../strongs/h/h1448.md); thou hast [śûm](../../strongs/h/h7760.md) his [miḇṣār](../../strongs/h/h4013.md) to [mᵊḥitâ](../../strongs/h/h4288.md).

<a name="psalms_89_41"></a>Psalms 89:41

All that ['abar](../../strongs/h/h5674.md) the [derek](../../strongs/h/h1870.md) [šāsas](../../strongs/h/h8155.md) him: he is a [cherpah](../../strongs/h/h2781.md) to his [šāḵēn](../../strongs/h/h7934.md).

<a name="psalms_89_42"></a>Psalms 89:42

Thou hast [ruwm](../../strongs/h/h7311.md) the [yamiyn](../../strongs/h/h3225.md) of his [tsar](../../strongs/h/h6862.md); thou hast made all his ['oyeb](../../strongs/h/h341.md) to [samach](../../strongs/h/h8055.md).

<a name="psalms_89_43"></a>Psalms 89:43

Thou hast also [shuwb](../../strongs/h/h7725.md) the [tsuwr](../../strongs/h/h6697.md) of his [chereb](../../strongs/h/h2719.md), and hast not made him to [quwm](../../strongs/h/h6965.md) in the [milḥāmâ](../../strongs/h/h4421.md).

<a name="psalms_89_44"></a>Psalms 89:44

Thou hast made his [ṭōhar](../../strongs/h/h2892.md) to [shabath](../../strongs/h/h7673.md), and [māḡar](../../strongs/h/h4048.md) his [kicce'](../../strongs/h/h3678.md) [māḡar](../../strongs/h/h4048.md) to the ['erets](../../strongs/h/h776.md).

<a name="psalms_89_45"></a>Psalms 89:45

The [yowm](../../strongs/h/h3117.md) of his [ʿălûmîm](../../strongs/h/h5934.md) hast thou [qāṣar](../../strongs/h/h7114.md): thou hast [ʿāṭâ](../../strongs/h/h5844.md) him with [bûšâ](../../strongs/h/h955.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_89_46"></a>Psalms 89:46

How long, [Yĕhovah](../../strongs/h/h3068.md)? wilt thou [cathar](../../strongs/h/h5641.md) thyself for [netsach](../../strongs/h/h5331.md) ? shall thy [chemah](../../strongs/h/h2534.md) [bāʿar](../../strongs/h/h1197.md) like ['esh](../../strongs/h/h784.md) ?

<a name="psalms_89_47"></a>Psalms 89:47

[zakar](../../strongs/h/h2142.md) how [cheled](../../strongs/h/h2465.md) my time is: wherefore hast thou [bara'](../../strongs/h/h1254.md) all [ben](../../strongs/h/h1121.md) ['āḏām](../../strongs/h/h120.md) in [shav'](../../strongs/h/h7723.md) ?

<a name="psalms_89_48"></a>Psalms 89:48

What [geḇer](../../strongs/h/h1397.md) is he that [ḥāyâ](../../strongs/h/h2421.md), and shall not [ra'ah](../../strongs/h/h7200.md) [maveth](../../strongs/h/h4194.md) ? shall he [mālaṭ](../../strongs/h/h4422.md) his [nephesh](../../strongs/h/h5315.md) from the [yad](../../strongs/h/h3027.md) of the [shĕ'owl](../../strongs/h/h7585.md) ? [Celah](../../strongs/h/h5542.md).

<a name="psalms_89_49"></a>Psalms 89:49

['adonay](../../strongs/h/h136.md), where are thy [ri'šôn](../../strongs/h/h7223.md) [checed](../../strongs/h/h2617.md), which thou [shaba'](../../strongs/h/h7650.md) unto [Dāviḏ](../../strongs/h/h1732.md) in thy ['ĕmûnâ](../../strongs/h/h530.md)?

<a name="psalms_89_50"></a>Psalms 89:50

[zakar](../../strongs/h/h2142.md), ['adonay](../../strongs/h/h136.md), the [cherpah](../../strongs/h/h2781.md) of thy ['ebed](../../strongs/h/h5650.md); how I do [nasa'](../../strongs/h/h5375.md) in my [ḥêq](../../strongs/h/h2436.md) the reproach of all the [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md);

<a name="psalms_89_51"></a>Psalms 89:51

Wherewith thine ['oyeb](../../strongs/h/h341.md) have [ḥārap̄](../../strongs/h/h2778.md), [Yĕhovah](../../strongs/h/h3068.md); wherewith they have [ḥārap̄](../../strongs/h/h2778.md) the ['aqeb](../../strongs/h/h6119.md) of thine [mashiyach](../../strongs/h/h4899.md).

<a name="psalms_89_52"></a>Psalms 89:52

[barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) for ['owlam](../../strongs/h/h5769.md). ['amen](../../strongs/h/h543.md), and ['amen](../../strongs/h/h543.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 88](psalms_88.md) - [Psalms 90](psalms_90.md)