# [Psalms 63](https://www.blueletterbible.org/kjv/psa/63)

<a name="psalms_63_1"></a>Psalms 63:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md), when he was in the [midbar](../../strongs/h/h4057.md) of [Yehuwdah](../../strongs/h/h3063.md). ['Elohiym](../../strongs/h/h430.md), thou art my ['el](../../strongs/h/h410.md); early will I [šāḥar](../../strongs/h/h7836.md) thee: my [nephesh](../../strongs/h/h5315.md) [ṣāmē'](../../strongs/h/h6770.md) for thee, my [basar](../../strongs/h/h1320.md) [kāmah](../../strongs/h/h3642.md) for thee in a [ṣîyâ](../../strongs/h/h6723.md) and [ʿāyēp̄](../../strongs/h/h5889.md) ['erets](../../strongs/h/h776.md), where no [mayim](../../strongs/h/h4325.md) is;

<a name="psalms_63_2"></a>Psalms 63:2

To [ra'ah](../../strongs/h/h7200.md) thy ['oz](../../strongs/h/h5797.md) and thy [kabowd](../../strongs/h/h3519.md), so as I have [chazah](../../strongs/h/h2372.md) thee in the [qodesh](../../strongs/h/h6944.md).

<a name="psalms_63_3"></a>Psalms 63:3

Because thy [checed](../../strongs/h/h2617.md) is [towb](../../strongs/h/h2896.md) than [chay](../../strongs/h/h2416.md), my [saphah](../../strongs/h/h8193.md) shall [shabach](../../strongs/h/h7623.md) thee.

<a name="psalms_63_4"></a>Psalms 63:4

Thus will I [barak](../../strongs/h/h1288.md) thee while I [chay](../../strongs/h/h2416.md): I will [nasa'](../../strongs/h/h5375.md) my [kaph](../../strongs/h/h3709.md) in thy [shem](../../strongs/h/h8034.md).

<a name="psalms_63_5"></a>Psalms 63:5

My [nephesh](../../strongs/h/h5315.md) shall be [sāׂbaʿ](../../strongs/h/h7646.md) as with [cheleb](../../strongs/h/h2459.md) and [dešen](../../strongs/h/h1880.md); and my [peh](../../strongs/h/h6310.md) shall [halal](../../strongs/h/h1984.md) thee with [rᵊnānâ](../../strongs/h/h7445.md) [saphah](../../strongs/h/h8193.md):

<a name="psalms_63_6"></a>Psalms 63:6

When I [zakar](../../strongs/h/h2142.md) thee upon my [yāṣûaʿ](../../strongs/h/h3326.md), and [hagah](../../strongs/h/h1897.md) on thee in the ['ašmurâ](../../strongs/h/h821.md).

<a name="psalms_63_7"></a>Psalms 63:7

Because thou hast been my [ʿezrâ](../../strongs/h/h5833.md), therefore in the [ṣēl](../../strongs/h/h6738.md) of thy [kanaph](../../strongs/h/h3671.md) will I [ranan](../../strongs/h/h7442.md).

<a name="psalms_63_8"></a>Psalms 63:8

My [nephesh](../../strongs/h/h5315.md) [dāḇaq](../../strongs/h/h1692.md) ['aḥar](../../strongs/h/h310.md) thee: thy [yamiyn](../../strongs/h/h3225.md) [tamak](../../strongs/h/h8551.md) me.

<a name="psalms_63_9"></a>Psalms 63:9

But those that [bāqaš](../../strongs/h/h1245.md) my [nephesh](../../strongs/h/h5315.md), to [šô'](../../strongs/h/h7722.md) it, shall [bow'](../../strongs/h/h935.md) into the [taḥtî](../../strongs/h/h8482.md) of the ['erets](../../strongs/h/h776.md).

<a name="psalms_63_10"></a>Psalms 63:10

They shall [nāḡar](../../strongs/h/h5064.md) by the [yad](../../strongs/h/h3027.md) [chereb](../../strongs/h/h2719.md): they shall be a [mᵊnāṯ](../../strongs/h/h4521.md) for [šûʿāl](../../strongs/h/h7776.md).

<a name="psalms_63_11"></a>Psalms 63:11

But the [melek](../../strongs/h/h4428.md) shall [samach](../../strongs/h/h8055.md) in ['Elohiym](../../strongs/h/h430.md); every one that [shaba'](../../strongs/h/h7650.md) by him shall [halal](../../strongs/h/h1984.md): but the [peh](../../strongs/h/h6310.md) of them that [dabar](../../strongs/h/h1696.md) [sheqer](../../strongs/h/h8267.md) shall be [sāḵar](../../strongs/h/h5534.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 62](psalms_62.md) - [Psalms 64](psalms_64.md)