# [Psalms 129](https://www.blueletterbible.org/kjv/psalms/129)

<a name="psalms_129_1"></a>Psalms 129:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md). Many a [rab](../../strongs/h/h7227.md) have they [tsarar](../../strongs/h/h6887.md) me from my [nāʿur](../../strongs/h/h5271.md), may [Yisra'el](../../strongs/h/h3478.md) now ['āmar](../../strongs/h/h559.md):

<a name="psalms_129_2"></a>Psalms 129:2

Many a [rab](../../strongs/h/h7227.md) have they [tsarar](../../strongs/h/h6887.md) me from my [nāʿur](../../strongs/h/h5271.md): yet they have not [yakol](../../strongs/h/h3201.md) against me.

<a name="psalms_129_3"></a>Psalms 129:3

The [ḥāraš](../../strongs/h/h2790.md) [ḥāraš](../../strongs/h/h2790.md) upon my [gaḇ](../../strongs/h/h1354.md): they made ['arak](../../strongs/h/h748.md) their [maʿănâ](../../strongs/h/h4618.md) [maʿănâ](../../strongs/h/h4618.md).

<a name="psalms_129_4"></a>Psalms 129:4

[Yĕhovah](../../strongs/h/h3068.md) is [tsaddiyq](../../strongs/h/h6662.md): he hath [qāṣaṣ](../../strongs/h/h7112.md) the [ʿăḇōṯ](../../strongs/h/h5688.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="psalms_129_5"></a>Psalms 129:5

Let them all be [buwsh](../../strongs/h/h954.md) and [sûḡ](../../strongs/h/h5472.md) ['āḥôr](../../strongs/h/h268.md) that [sane'](../../strongs/h/h8130.md) [Tsiyown](../../strongs/h/h6726.md).

<a name="psalms_129_6"></a>Psalms 129:6

Let them be as the [chatsiyr](../../strongs/h/h2682.md) upon the [gāḡ](../../strongs/h/h1406.md), which [yāḇēš](../../strongs/h/h3001.md) [qaḏmâ](../../strongs/h/h6927.md) it [šālap̄](../../strongs/h/h8025.md):

<a name="psalms_129_7"></a>Psalms 129:7

Wherewith the [qāṣar](../../strongs/h/h7114.md) [mālā'](../../strongs/h/h4390.md) not his [kaph](../../strongs/h/h3709.md); nor he that [ʿāmar](../../strongs/h/h6014.md) his [ḥēṣen](../../strongs/h/h2683.md).

<a name="psalms_129_8"></a>Psalms 129:8

Neither do they which ['abar](../../strongs/h/h5674.md) ['āmar](../../strongs/h/h559.md), The [bĕrakah](../../strongs/h/h1293.md) of [Yĕhovah](../../strongs/h/h3068.md) be upon you: we [barak](../../strongs/h/h1288.md) you in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 128](psalms_128.md) - [Psalms 130](psalms_130.md)