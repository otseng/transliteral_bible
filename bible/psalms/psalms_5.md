# [Psalms 5](https://www.blueletterbible.org/kjv/psa/5/1/s_483001)

<a name="psalms_5_1"></a>Psalms 5:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon [Nᵊḥîlâ](../../strongs/h/h5155.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). ['azan](../../strongs/h/h238.md) to my ['emer](../../strongs/h/h561.md), [Yĕhovah](../../strongs/h/h3068.md), consider my [hagiyg](../../strongs/h/h1901.md).

<a name="psalms_5_2"></a>Psalms 5:2

[qashab](../../strongs/h/h7181.md) unto the [qowl](../../strongs/h/h6963.md) of my [sheva'](../../strongs/h/h7773.md), my [melek](../../strongs/h/h4428.md), and my ['Elohiym](../../strongs/h/h430.md): for unto thee will I [palal](../../strongs/h/h6419.md).

<a name="psalms_5_3"></a>Psalms 5:3

My [qowl](../../strongs/h/h6963.md) shalt thou [shama'](../../strongs/h/h8085.md) in the [boqer](../../strongs/h/h1242.md), [Yĕhovah](../../strongs/h/h3068.md); in the [boqer](../../strongs/h/h1242.md) will I ['arak](../../strongs/h/h6186.md) unto thee, and will [tsaphah](../../strongs/h/h6822.md).

<a name="psalms_5_4"></a>Psalms 5:4

For thou not an ['el](../../strongs/h/h410.md) that hath [chaphets](../../strongs/h/h2655.md) in [resha'](../../strongs/h/h7562.md): neither shall [ra'](../../strongs/h/h7451.md) [guwr](../../strongs/h/h1481.md) with thee.

<a name="psalms_5_5"></a>Psalms 5:5

The [halal](../../strongs/h/h1984.md) shall not [yatsab](../../strongs/h/h3320.md) in thy ['ayin](../../strongs/h/h5869.md): thou [sane'](../../strongs/h/h8130.md) all [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md).

<a name="psalms_5_6"></a>Psalms 5:6

Thou shalt ['abad](../../strongs/h/h6.md) them that [dabar](../../strongs/h/h1696.md) [kazab](../../strongs/h/h3577.md): [Yĕhovah](../../strongs/h/h3068.md) will [ta'ab](../../strongs/h/h8581.md) the [dam](../../strongs/h/h1818.md) and [mirmah](../../strongs/h/h4820.md) ['iysh](../../strongs/h/h376.md).

<a name="psalms_5_7"></a>Psalms 5:7

But as for me, I will [bow'](../../strongs/h/h935.md) thy [bayith](../../strongs/h/h1004.md) in the multitude of thy [checed](../../strongs/h/h2617.md): in thy [yir'ah](../../strongs/h/h3374.md) will I [shachah](../../strongs/h/h7812.md) toward thy [qodesh](../../strongs/h/h6944.md) [heykal](../../strongs/h/h1964.md).

<a name="psalms_5_8"></a>Psalms 5:8

[nachah](../../strongs/h/h5148.md) me, [Yĕhovah](../../strongs/h/h3068.md), in thy [tsedaqah](../../strongs/h/h6666.md) because of mine [sharar](../../strongs/h/h8324.md); make thy [derek](../../strongs/h/h1870.md) [yashar](../../strongs/h/h3474.md) before my [paniym](../../strongs/h/h6440.md).

<a name="psalms_5_9"></a>Psalms 5:9

For no [kuwn](../../strongs/h/h3559.md) in their [peh](../../strongs/h/h6310.md); their [qereḇ](../../strongs/h/h7130.md) is [havvah](../../strongs/h/h1942.md); their [garown](../../strongs/h/h1627.md) is an open [qeber](../../strongs/h/h6913.md); they [chalaq](../../strongs/h/h2505.md) with their [lashown](../../strongs/h/h3956.md).

<a name="psalms_5_10"></a>Psalms 5:10

['asham](../../strongs/h/h816.md) thou them, ['Elohiym](../../strongs/h/h430.md); let them [naphal](../../strongs/h/h5307.md) by their own [mow'etsah](../../strongs/h/h4156.md); [nāḏaḥ](../../strongs/h/h5080.md) them out in the [rōḇ](../../strongs/h/h7230.md) of their [pesha'](../../strongs/h/h6588.md); for they have [marah](../../strongs/h/h4784.md) against thee.

<a name="psalms_5_11"></a>Psalms 5:11

But let all those that [chacah](../../strongs/h/h2620.md) in thee [samach](../../strongs/h/h8055.md): let them ever [ranan](../../strongs/h/h7442.md), because thou [cakak](../../strongs/h/h5526.md) them: let them also that ['ahab](../../strongs/h/h157.md) thy [shem](../../strongs/h/h8034.md) ['alats](../../strongs/h/h5970.md) in thee.

<a name="psalms_5_12"></a>Psalms 5:12

For thou, [Yĕhovah](../../strongs/h/h3068.md), wilt [barak](../../strongs/h/h1288.md) the [tsaddiyq](../../strongs/h/h6662.md); with [ratsown](../../strongs/h/h7522.md) wilt thou ['atar](../../strongs/h/h5849.md) him as a [tsinnah](../../strongs/h/h6793.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 4](psalms_4.md) - [Psalms 6](psalms_6.md)