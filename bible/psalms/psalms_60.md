# [Psalms 60](https://www.blueletterbible.org/kjv/psa/60)

<a name="psalms_60_1"></a>Psalms 60:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon [šûšan ʿēḏûṯ](../../strongs/h/h7802.md), [Miḵtām](../../strongs/h/h4387.md) of [Dāviḏ](../../strongs/h/h1732.md), to [lamad](../../strongs/h/h3925.md); when he [nāṣâ](../../strongs/h/h5327.md) with ['Ăram nahărayim](../../strongs/h/h763.md) [nāhār](../../strongs/h/h5104.md) and with ['rm ṣvḇh](../../strongs/h/h760.md), when [Yô'āḇ](../../strongs/h/h3097.md) [shuwb](../../strongs/h/h7725.md), and [nakah](../../strongs/h/h5221.md) of ['Ĕḏōm](../../strongs/h/h123.md) in the [gay'](../../strongs/h/h1516.md) of [melaḥ](../../strongs/h/h4417.md) twelve thousand. ['Elohiym](../../strongs/h/h430.md), thou hast [zānaḥ](../../strongs/h/h2186.md) us, thou hast [pāraṣ](../../strongs/h/h6555.md) us, thou hast been ['anaph](../../strongs/h/h599.md); [shuwb](../../strongs/h/h7725.md) thyself to us.

<a name="psalms_60_2"></a>Psalms 60:2

Thou hast made the ['erets](../../strongs/h/h776.md) to [rāʿaš](../../strongs/h/h7493.md); thou hast [pāṣam](../../strongs/h/h6480.md) it: [rapha'](../../strongs/h/h7495.md) the [šeḇar](../../strongs/h/h7667.md) thereof; for it [mowt](../../strongs/h/h4131.md).

<a name="psalms_60_3"></a>Psalms 60:3

Thou hast [ra'ah](../../strongs/h/h7200.md) thy ['am](../../strongs/h/h5971.md) [qāšê](../../strongs/h/h7186.md): thou hast made us to [šāqâ](../../strongs/h/h8248.md) the [yayin](../../strongs/h/h3196.md) of [tarʿēlâ](../../strongs/h/h8653.md).

<a name="psalms_60_4"></a>Psalms 60:4

Thou hast [nathan](../../strongs/h/h5414.md) a [nēs](../../strongs/h/h5251.md) to them that [yārē'](../../strongs/h/h3373.md) thee, that it may be [nûs](../../strongs/h/h5127.md) because of the [qōšeṭ](../../strongs/h/h7189.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_60_5"></a>Psalms 60:5

That thy [yāḏîḏ](../../strongs/h/h3039.md) may be [chalats](../../strongs/h/h2502.md); [yasha'](../../strongs/h/h3467.md) with thy [yamiyn](../../strongs/h/h3225.md), and ['anah](../../strongs/h/h6030.md) me.

<a name="psalms_60_6"></a>Psalms 60:6

['Elohiym](../../strongs/h/h430.md) hath [dabar](../../strongs/h/h1696.md) in his [qodesh](../../strongs/h/h6944.md); I will [ʿālaz](../../strongs/h/h5937.md), I will [chalaq](../../strongs/h/h2505.md) [Šᵊḵem](../../strongs/h/h7927.md), and [māḏaḏ](../../strongs/h/h4058.md) the [ʿēmeq](../../strongs/h/h6010.md) of [Sukôṯ](../../strongs/h/h5523.md).

<a name="psalms_60_7"></a>Psalms 60:7

[Gilʿāḏ](../../strongs/h/h1568.md) is mine, and [Mᵊnaššê](../../strongs/h/h4519.md) is mine; ['Ep̄rayim](../../strongs/h/h669.md) also is the [māʿôz](../../strongs/h/h4581.md) of mine [ro'sh](../../strongs/h/h7218.md); [Yehuwdah](../../strongs/h/h3063.md) is my [ḥāqaq](../../strongs/h/h2710.md);

<a name="psalms_60_8"></a>Psalms 60:8

[Mô'āḇ](../../strongs/h/h4124.md) is my [sîr](../../strongs/h/h5518.md) [raḥaṣ](../../strongs/h/h7366.md); over ['Ĕḏōm](../../strongs/h/h123.md) will I [shalak](../../strongs/h/h7993.md) my [naʿal](../../strongs/h/h5275.md): [pᵊlešeṯ](../../strongs/h/h6429.md), [rûaʿ](../../strongs/h/h7321.md) thou because of me.

<a name="psalms_60_9"></a>Psalms 60:9

Who will [yāḇal](../../strongs/h/h2986.md) me into the [māṣôr](../../strongs/h/h4692.md) [ʿîr](../../strongs/h/h5892.md)? who will [nachah](../../strongs/h/h5148.md) me into ['Ĕḏōm](../../strongs/h/h123.md)?

<a name="psalms_60_10"></a>Psalms 60:10

Wilt not thou, ['Elohiym](../../strongs/h/h430.md), which hadst [zānaḥ](../../strongs/h/h2186.md) us off? and thou, ['Elohiym](../../strongs/h/h430.md), which didst not [yāṣā'](../../strongs/h/h3318.md) with our [tsaba'](../../strongs/h/h6635.md)?

<a name="psalms_60_11"></a>Psalms 60:11

[yāhaḇ](../../strongs/h/h3051.md) us [ʿezrâ](../../strongs/h/h5833.md) from [tsar](../../strongs/h/h6862.md): for [shav'](../../strongs/h/h7723.md) is the [tᵊšûʿâ](../../strongs/h/h8668.md) of ['adam](../../strongs/h/h120.md).

<a name="psalms_60_12"></a>Psalms 60:12

Through ['Elohiym](../../strongs/h/h430.md) we shall ['asah](../../strongs/h/h6213.md) [ḥayil](../../strongs/h/h2428.md): for he it is that shall [bûs](../../strongs/h/h947.md) our [tsar](../../strongs/h/h6862.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 59](psalms_59.md) - [Psalms 61](psalms_61.md)