# [Psalms 108](https://www.blueletterbible.org/kjv/psalms/108)

<a name="psalms_108_1"></a>Psalms 108:1

A [šîr](../../strongs/h/h7892.md) or [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). ['Elohiym](../../strongs/h/h430.md), my [leb](../../strongs/h/h3820.md) is [kuwn](../../strongs/h/h3559.md); I will [shiyr](../../strongs/h/h7891.md) and [zamar](../../strongs/h/h2167.md), even with my [kabowd](../../strongs/h/h3519.md).

<a name="psalms_108_2"></a>Psalms 108:2

[ʿûr](../../strongs/h/h5782.md), [neḇel](../../strongs/h/h5035.md) and [kinnôr](../../strongs/h/h3658.md): I myself will [ʿûr](../../strongs/h/h5782.md) [šaḥar](../../strongs/h/h7837.md).

<a name="psalms_108_3"></a>Psalms 108:3

I will [yadah](../../strongs/h/h3034.md) thee, [Yĕhovah](../../strongs/h/h3068.md), among the ['am](../../strongs/h/h5971.md): and I will [zamar](../../strongs/h/h2167.md) unto thee among the [lĕom](../../strongs/h/h3816.md).

<a name="psalms_108_4"></a>Psalms 108:4

For thy [checed](../../strongs/h/h2617.md) is [gadowl](../../strongs/h/h1419.md) above the [shamayim](../../strongs/h/h8064.md): and thy ['emeth](../../strongs/h/h571.md) reacheth unto the [shachaq](../../strongs/h/h7834.md).

<a name="psalms_108_5"></a>Psalms 108:5

Be thou [ruwm](../../strongs/h/h7311.md), ['Elohiym](../../strongs/h/h430.md), above the [shamayim](../../strongs/h/h8064.md): and thy [kabowd](../../strongs/h/h3519.md) above all the ['erets](../../strongs/h/h776.md);

<a name="psalms_108_6"></a>Psalms 108:6

That thy [yāḏîḏ](../../strongs/h/h3039.md) may be [chalats](../../strongs/h/h2502.md): [yasha'](../../strongs/h/h3467.md) with thy [yamiyn](../../strongs/h/h3225.md), and ['anah](../../strongs/h/h6030.md) me.

<a name="psalms_108_7"></a>Psalms 108:7

['Elohiym](../../strongs/h/h430.md) hath [dabar](../../strongs/h/h1696.md) in his [qodesh](../../strongs/h/h6944.md); I will [ʿālaz](../../strongs/h/h5937.md), I will [chalaq](../../strongs/h/h2505.md) [Šᵊḵem](../../strongs/h/h7927.md), and mete [māḏaḏ](../../strongs/h/h4058.md) the [ʿēmeq](../../strongs/h/h6010.md) of [Sukôṯ](../../strongs/h/h5523.md).

<a name="psalms_108_8"></a>Psalms 108:8

[Gilʿāḏ](../../strongs/h/h1568.md) is mine; [Mᵊnaššê](../../strongs/h/h4519.md) is mine; ['Ep̄rayim](../../strongs/h/h669.md) also is the [māʿôz](../../strongs/h/h4581.md) of mine [ro'sh](../../strongs/h/h7218.md); [Yehuwdah](../../strongs/h/h3063.md) is my [ḥāqaq](../../strongs/h/h2710.md);

<a name="psalms_108_9"></a>Psalms 108:9

[Mô'āḇ](../../strongs/h/h4124.md) is my [raḥaṣ](../../strongs/h/h7366.md) [sîr](../../strongs/h/h5518.md); over ['Ĕḏōm](../../strongs/h/h123.md) will I [shalak](../../strongs/h/h7993.md) my [naʿal](../../strongs/h/h5275.md); over [pᵊlešeṯ](../../strongs/h/h6429.md) will I [rûaʿ](../../strongs/h/h7321.md).

<a name="psalms_108_10"></a>Psalms 108:10

Who will [yāḇal](../../strongs/h/h2986.md) me into the [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md)? who will [nachah](../../strongs/h/h5148.md) me into ['Ĕḏōm](../../strongs/h/h123.md)?

<a name="psalms_108_11"></a>Psalms 108:11

Wilt not thou, ['Elohiym](../../strongs/h/h430.md), who hast [zānaḥ](../../strongs/h/h2186.md) us? and wilt not thou, ['Elohiym](../../strongs/h/h430.md), [yāṣā'](../../strongs/h/h3318.md) with our [tsaba'](../../strongs/h/h6635.md)?

<a name="psalms_108_12"></a>Psalms 108:12

[yāhaḇ](../../strongs/h/h3051.md) us [ʿezrâ](../../strongs/h/h5833.md) from [tsar](../../strongs/h/h6862.md): for [shav'](../../strongs/h/h7723.md) is the [tᵊšûʿâ](../../strongs/h/h8668.md) of ['āḏām](../../strongs/h/h120.md).

<a name="psalms_108_13"></a>Psalms 108:13

Through ['Elohiym](../../strongs/h/h430.md) we shall ['asah](../../strongs/h/h6213.md) [ḥayil](../../strongs/h/h2428.md): for he it is that shall [bûs](../../strongs/h/h947.md) our [tsar](../../strongs/h/h6862.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 107](psalms_107.md) - [Psalms 109](psalms_109.md)