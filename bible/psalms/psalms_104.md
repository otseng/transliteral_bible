# [Psalms 104](https://www.blueletterbible.org/kjv/psalms/104)

<a name="psalms_104_1"></a>Psalms 104:1

[barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md), O my [nephesh](../../strongs/h/h5315.md). [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), thou art [me'od](../../strongs/h/h3966.md) [gāḏal](../../strongs/h/h1431.md); thou art [labash](../../strongs/h/h3847.md) with [howd](../../strongs/h/h1935.md) and [hadar](../../strongs/h/h1926.md).

<a name="psalms_104_2"></a>Psalms 104:2

Who [ʿāṭâ](../../strongs/h/h5844.md) thyself with ['owr](../../strongs/h/h216.md) as with a [śalmâ](../../strongs/h/h8008.md): who [natah](../../strongs/h/h5186.md) the [shamayim](../../strongs/h/h8064.md) like a [yᵊrîʿâ](../../strongs/h/h3407.md):

<a name="psalms_104_3"></a>Psalms 104:3

Who layeth the [qārâ](../../strongs/h/h7136.md) of his [ʿălîyâ](../../strongs/h/h5944.md) in the [mayim](../../strongs/h/h4325.md): who [śûm](../../strongs/h/h7760.md) the ['ab](../../strongs/h/h5645.md) his [rᵊḵûḇ](../../strongs/h/h7398.md): who [halak](../../strongs/h/h1980.md) upon the [kanaph](../../strongs/h/h3671.md) of the [ruwach](../../strongs/h/h7307.md):

<a name="psalms_104_4"></a>Psalms 104:4

Who ['asah](../../strongs/h/h6213.md) his [mal'ak](../../strongs/h/h4397.md) [ruwach](../../strongs/h/h7307.md); his [sharath](../../strongs/h/h8334.md) a [lāhaṭ](../../strongs/h/h3857.md) ['esh](../../strongs/h/h784.md):

<a name="psalms_104_5"></a>Psalms 104:5

Who [yacad](../../strongs/h/h3245.md) the [māḵôn](../../strongs/h/h4349.md) of the ['erets](../../strongs/h/h776.md), that it should not be [mowt](../../strongs/h/h4131.md) ['owlam](../../strongs/h/h5769.md) [ʿaḏ](../../strongs/h/h5703.md).

<a name="psalms_104_6"></a>Psalms 104:6

Thou [kāsâ](../../strongs/h/h3680.md) it with the [tĕhowm](../../strongs/h/h8415.md) as with a [lᵊḇûš](../../strongs/h/h3830.md): the [mayim](../../strongs/h/h4325.md) ['amad](../../strongs/h/h5975.md) above the [har](../../strongs/h/h2022.md).

<a name="psalms_104_7"></a>Psalms 104:7

[min](../../strongs/h/h4480.md) thy [ge'arah](../../strongs/h/h1606.md) they [nûs](../../strongs/h/h5127.md); at the [qowl](../../strongs/h/h6963.md) of thy [raʿam](../../strongs/h/h7482.md) they [ḥāp̄az](../../strongs/h/h2648.md) away.

<a name="psalms_104_8"></a>Psalms 104:8

They [ʿālâ](../../strongs/h/h5927.md) by the [har](../../strongs/h/h2022.md); they [yarad](../../strongs/h/h3381.md) by the [biqʿâ](../../strongs/h/h1237.md) unto the [maqowm](../../strongs/h/h4725.md) [zê](../../strongs/h/h2088.md) thou hast [yacad](../../strongs/h/h3245.md) for them.

<a name="psalms_104_9"></a>Psalms 104:9

Thou hast [śûm](../../strongs/h/h7760.md) a [gᵊḇûl](../../strongs/h/h1366.md) that they may not ['abar](../../strongs/h/h5674.md); that they [shuwb](../../strongs/h/h7725.md) not to [kāsâ](../../strongs/h/h3680.md) the ['erets](../../strongs/h/h776.md).

<a name="psalms_104_10"></a>Psalms 104:10

He [shalach](../../strongs/h/h7971.md) the [maʿyān](../../strongs/h/h4599.md) into the [nachal](../../strongs/h/h5158.md), which [halak](../../strongs/h/h1980.md) among the [har](../../strongs/h/h2022.md).

<a name="psalms_104_11"></a>Psalms 104:11

They give [šāqâ](../../strongs/h/h8248.md) to every [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md): the [pere'](../../strongs/h/h6501.md) [shabar](../../strongs/h/h7665.md) their [ṣāmā'](../../strongs/h/h6772.md).

<a name="psalms_104_12"></a>Psalms 104:12

By them shall the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md) have their [shakan](../../strongs/h/h7931.md), which [nathan](../../strongs/h/h5414.md) [qowl](../../strongs/h/h6963.md) [bayin](../../strongs/h/h996.md) the [ʿŏp̄î](../../strongs/h/h6073.md).

<a name="psalms_104_13"></a>Psalms 104:13

He [šāqâ](../../strongs/h/h8248.md) the [har](../../strongs/h/h2022.md) from his [ʿălîyâ](../../strongs/h/h5944.md): the ['erets](../../strongs/h/h776.md) is [sāׂbaʿ](../../strongs/h/h7646.md) with the [pĕriy](../../strongs/h/h6529.md) of thy [ma'aseh](../../strongs/h/h4639.md).

<a name="psalms_104_14"></a>Psalms 104:14

He causeth the [chatsiyr](../../strongs/h/h2682.md) to [ṣāmaḥ](../../strongs/h/h6779.md) for the [bĕhemah](../../strongs/h/h929.md), and ['eseb](../../strongs/h/h6212.md) for the [ʿăḇōḏâ](../../strongs/h/h5656.md) of ['āḏām](../../strongs/h/h120.md): that he may [yāṣā'](../../strongs/h/h3318.md) [lechem](../../strongs/h/h3899.md) out of the ['erets](../../strongs/h/h776.md);

<a name="psalms_104_15"></a>Psalms 104:15

And [yayin](../../strongs/h/h3196.md) that [samach](../../strongs/h/h8055.md) the [lebab](../../strongs/h/h3824.md) of ['enowsh](../../strongs/h/h582.md), and [šemen](../../strongs/h/h8081.md) to make his [paniym](../../strongs/h/h6440.md) to [ṣāhal](../../strongs/h/h6670.md), and [lechem](../../strongs/h/h3899.md) which [sāʿaḏ](../../strongs/h/h5582.md) ['enowsh](../../strongs/h/h582.md) [lebab](../../strongs/h/h3824.md).

<a name="psalms_104_16"></a>Psalms 104:16

The ['ets](../../strongs/h/h6086.md) of [Yĕhovah](../../strongs/h/h3068.md) are [sāׂbaʿ](../../strongs/h/h7646.md) of sap; the ['erez](../../strongs/h/h730.md) of [Lᵊḇānôn](../../strongs/h/h3844.md), which he hath [nāṭaʿ](../../strongs/h/h5193.md);

<a name="psalms_104_17"></a>Psalms 104:17

Where the [tsippowr](../../strongs/h/h6833.md) make their [qānan](../../strongs/h/h7077.md): as for the [ḥăsîḏâ](../../strongs/h/h2624.md), the [bᵊrôš](../../strongs/h/h1265.md) are her [bayith](../../strongs/h/h1004.md).

<a name="psalms_104_18"></a>Psalms 104:18

The [gāḇōha](../../strongs/h/h1364.md) [har](../../strongs/h/h2022.md) are a [machaceh](../../strongs/h/h4268.md) for the [yāʿēl](../../strongs/h/h3277.md); and the [cela'](../../strongs/h/h5553.md) for the [šāp̄ān](../../strongs/h/h8227.md).

<a name="psalms_104_19"></a>Psalms 104:19

He ['asah](../../strongs/h/h6213.md) the [yareach](../../strongs/h/h3394.md) for [môʿēḏ](../../strongs/h/h4150.md): the [šemeš](../../strongs/h/h8121.md) [yada'](../../strongs/h/h3045.md) his going [māḇô'](../../strongs/h/h3996.md).

<a name="psalms_104_20"></a>Psalms 104:20

Thou [shiyth](../../strongs/h/h7896.md) [choshek](../../strongs/h/h2822.md), and it is [layil](../../strongs/h/h3915.md): wherein all the [chay](../../strongs/h/h2416.md) of the [yaʿar](../../strongs/h/h3293.md) do [rāmaś](../../strongs/h/h7430.md) forth.

<a name="psalms_104_21"></a>Psalms 104:21

The [kephiyr](../../strongs/h/h3715.md) [šā'aḡ](../../strongs/h/h7580.md) after their [ṭerep̄](../../strongs/h/h2964.md), and [bāqaš](../../strongs/h/h1245.md) their ['ōḵel](../../strongs/h/h400.md) from ['el](../../strongs/h/h410.md).

<a name="psalms_104_22"></a>Psalms 104:22

The [šemeš](../../strongs/h/h8121.md) [zāraḥ](../../strongs/h/h2224.md), they gather themselves ['āsap̄](../../strongs/h/h622.md), and lay them [rāḇaṣ](../../strongs/h/h7257.md) in their [mᵊʿônâ](../../strongs/h/h4585.md).

<a name="psalms_104_23"></a>Psalms 104:23

['āḏām](../../strongs/h/h120.md) [yāṣā'](../../strongs/h/h3318.md) unto his [pōʿal](../../strongs/h/h6467.md) and to his [ʿăḇōḏâ](../../strongs/h/h5656.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="psalms_104_24"></a>Psalms 104:24

[Yĕhovah](../../strongs/h/h3068.md), how [rabab](../../strongs/h/h7231.md) are thy [ma'aseh](../../strongs/h/h4639.md)! in [ḥāḵmâ](../../strongs/h/h2451.md) hast thou ['asah](../../strongs/h/h6213.md) them all: the ['erets](../../strongs/h/h776.md) is [mālā'](../../strongs/h/h4390.md) of thy [qinyān](../../strongs/h/h7075.md).

<a name="psalms_104_25"></a>Psalms 104:25

So is this [gadowl](../../strongs/h/h1419.md) and [rāḥāḇ](../../strongs/h/h7342.md) [yad](../../strongs/h/h3027.md) [yam](../../strongs/h/h3220.md), wherein are things [remeś](../../strongs/h/h7431.md) [mispār](../../strongs/h/h4557.md), both [qāṭān](../../strongs/h/h6996.md) and [gadowl](../../strongs/h/h1419.md) [chay](../../strongs/h/h2416.md).

<a name="psalms_104_26"></a>Psalms 104:26

There [halak](../../strongs/h/h1980.md) the ['ŏnîyâ](../../strongs/h/h591.md): there is that [livyāṯān](../../strongs/h/h3882.md), whom thou hast [yāṣar](../../strongs/h/h3335.md) to [śāḥaq](../../strongs/h/h7832.md) therein.

<a name="psalms_104_27"></a>Psalms 104:27

These [śāḇar](../../strongs/h/h7663.md) all upon thee; that thou mayest [nathan](../../strongs/h/h5414.md) them their ['ōḵel](../../strongs/h/h400.md) in due [ʿēṯ](../../strongs/h/h6256.md).

<a name="psalms_104_28"></a>Psalms 104:28

That thou [nathan](../../strongs/h/h5414.md) them they [lāqaṭ](../../strongs/h/h3950.md): thou [pāṯaḥ](../../strongs/h/h6605.md) thine [yad](../../strongs/h/h3027.md), they are [sāׂbaʿ](../../strongs/h/h7646.md) with [towb](../../strongs/h/h2896.md).

<a name="psalms_104_29"></a>Psalms 104:29

Thou [cathar](../../strongs/h/h5641.md) thy [paniym](../../strongs/h/h6440.md), they are [bahal](../../strongs/h/h926.md): thou ['āsap̄](../../strongs/h/h622.md) their [ruwach](../../strongs/h/h7307.md), they [gāvaʿ](../../strongs/h/h1478.md), and [shuwb](../../strongs/h/h7725.md) to their ['aphar](../../strongs/h/h6083.md).

<a name="psalms_104_30"></a>Psalms 104:30

Thou [shalach](../../strongs/h/h7971.md) thy [ruwach](../../strongs/h/h7307.md), they are [bara'](../../strongs/h/h1254.md): and thou [ḥādaš](../../strongs/h/h2318.md) the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="psalms_104_31"></a>Psalms 104:31

The [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) shall endure ['owlam](../../strongs/h/h5769.md): [Yĕhovah](../../strongs/h/h3068.md) shall [samach](../../strongs/h/h8055.md) in his [ma'aseh](../../strongs/h/h4639.md).

<a name="psalms_104_32"></a>Psalms 104:32

He [nabat](../../strongs/h/h5027.md) on the ['erets](../../strongs/h/h776.md), and it [rāʿaḏ](../../strongs/h/h7460.md): he [naga'](../../strongs/h/h5060.md) the [har](../../strongs/h/h2022.md), and they [ʿāšēn](../../strongs/h/h6225.md).

<a name="psalms_104_33"></a>Psalms 104:33

I will [shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md) as long as I [chay](../../strongs/h/h2416.md): I will [zamar](../../strongs/h/h2167.md) praise to my ['Elohiym](../../strongs/h/h430.md) while I have my [ʿôḏ](../../strongs/h/h5750.md).

<a name="psalms_104_34"></a>Psalms 104:34

My [śîaḥ](../../strongs/h/h7879.md) of him shall be [ʿāraḇ](../../strongs/h/h6149.md): I will be [samach](../../strongs/h/h8055.md) in [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_104_35"></a>Psalms 104:35

Let the [chatta'](../../strongs/h/h2400.md) be [tamam](../../strongs/h/h8552.md) out of the ['erets](../../strongs/h/h776.md), and let the [rasha'](../../strongs/h/h7563.md) be no more. [barak](../../strongs/h/h1288.md) thou [Yĕhovah](../../strongs/h/h3068.md), O my [nephesh](../../strongs/h/h5315.md). [halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 103](psalms_103.md) - [Psalms 105](psalms_105.md)