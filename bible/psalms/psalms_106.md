# [Psalms 106](https://www.blueletterbible.org/kjv/psalms/106)

<a name="psalms_106_1"></a>Psalms 106:1

[halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md). [yadah](../../strongs/h/h3034.md) unto [Yĕhovah](../../strongs/h/h3068.md); for he is [towb](../../strongs/h/h2896.md): for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="psalms_106_2"></a>Psalms 106:2

Who can [mālal](../../strongs/h/h4448.md) the [gᵊḇûrâ](../../strongs/h/h1369.md) of [Yĕhovah](../../strongs/h/h3068.md)? who can [shama'](../../strongs/h/h8085.md) all his [tehillah](../../strongs/h/h8416.md)?

<a name="psalms_106_3"></a>Psalms 106:3

['esher](../../strongs/h/h835.md) are they that [shamar](../../strongs/h/h8104.md) [mishpat](../../strongs/h/h4941.md), and he that ['asah](../../strongs/h/h6213.md) [tsedaqah](../../strongs/h/h6666.md) at all [ʿēṯ](../../strongs/h/h6256.md).

<a name="psalms_106_4"></a>Psalms 106:4

[zakar](../../strongs/h/h2142.md) me, [Yĕhovah](../../strongs/h/h3068.md), with the [ratsown](../../strongs/h/h7522.md) that thou bearest unto thy ['am](../../strongs/h/h5971.md): O [paqad](../../strongs/h/h6485.md) me with thy [yĕshuw'ah](../../strongs/h/h3444.md);

<a name="psalms_106_5"></a>Psalms 106:5

That I may [ra'ah](../../strongs/h/h7200.md) the [towb](../../strongs/h/h2896.md) of thy [bāḥîr](../../strongs/h/h972.md), that I may [samach](../../strongs/h/h8055.md) in the [simchah](../../strongs/h/h8057.md) of thy [gowy](../../strongs/h/h1471.md), that I may [halal](../../strongs/h/h1984.md) with thine [nachalah](../../strongs/h/h5159.md).

<a name="psalms_106_6"></a>Psalms 106:6

We have [chata'](../../strongs/h/h2398.md) with our ['ab](../../strongs/h/h1.md), we have [ʿāvâ](../../strongs/h/h5753.md), we have [rāšaʿ](../../strongs/h/h7561.md).

<a name="psalms_106_7"></a>Psalms 106:7

Our ['ab](../../strongs/h/h1.md) [sakal](../../strongs/h/h7919.md) not thy [pala'](../../strongs/h/h6381.md) in [Mitsrayim](../../strongs/h/h4714.md); they [zakar](../../strongs/h/h2142.md) not the [rōḇ](../../strongs/h/h7230.md) of thy [checed](../../strongs/h/h2617.md); but [marah](../../strongs/h/h4784.md) him at the [yam](../../strongs/h/h3220.md), even at the [sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md).

<a name="psalms_106_8"></a>Psalms 106:8

Nevertheless he [yasha'](../../strongs/h/h3467.md) them for his [shem](../../strongs/h/h8034.md) sake, that he might make his [gᵊḇûrâ](../../strongs/h/h1369.md) to be [yada'](../../strongs/h/h3045.md).

<a name="psalms_106_9"></a>Psalms 106:9

He [gāʿar](../../strongs/h/h1605.md) the [sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md) also, and it was [ḥāraḇ](../../strongs/h/h2717.md): so he [yālaḵ](../../strongs/h/h3212.md) them through the [tĕhowm](../../strongs/h/h8415.md), as through the [midbar](../../strongs/h/h4057.md).

<a name="psalms_106_10"></a>Psalms 106:10

And he [yasha'](../../strongs/h/h3467.md) them from the [yad](../../strongs/h/h3027.md) of him that [sane'](../../strongs/h/h8130.md) them, and [gā'al](../../strongs/h/h1350.md) them from the [yad](../../strongs/h/h3027.md) of the ['oyeb](../../strongs/h/h341.md).

<a name="psalms_106_11"></a>Psalms 106:11

And the [mayim](../../strongs/h/h4325.md) [kāsâ](../../strongs/h/h3680.md) their [tsar](../../strongs/h/h6862.md): there was not ['echad](../../strongs/h/h259.md) of them [yāṯar](../../strongs/h/h3498.md).

<a name="psalms_106_12"></a>Psalms 106:12

Then ['aman](../../strongs/h/h539.md) they his [dabar](../../strongs/h/h1697.md); they [shiyr](../../strongs/h/h7891.md) his [tehillah](../../strongs/h/h8416.md).

<a name="psalms_106_13"></a>Psalms 106:13

They [māhar](../../strongs/h/h4116.md) [shakach](../../strongs/h/h7911.md) his [ma'aseh](../../strongs/h/h4639.md); they [ḥāḵâ](../../strongs/h/h2442.md) not for his ['etsah](../../strongs/h/h6098.md):

<a name="psalms_106_14"></a>Psalms 106:14

But ['āvâ](../../strongs/h/h183.md) [ta'avah](../../strongs/h/h8378.md) in the [midbar](../../strongs/h/h4057.md), and [nāsâ](../../strongs/h/h5254.md) ['el](../../strongs/h/h410.md) in the [yᵊšîmôn](../../strongs/h/h3452.md).

<a name="psalms_106_15"></a>Psalms 106:15

And he [nathan](../../strongs/h/h5414.md) them their [šᵊ'ēlâ](../../strongs/h/h7596.md); but [shalach](../../strongs/h/h7971.md) [rāzôn](../../strongs/h/h7332.md) into their [nephesh](../../strongs/h/h5315.md).

<a name="psalms_106_16"></a>Psalms 106:16

They [qānā'](../../strongs/h/h7065.md) [Mōshe](../../strongs/h/h4872.md) also in the [maḥănê](../../strongs/h/h4264.md), and ['Ahărôn](../../strongs/h/h175.md) the [qadowsh](../../strongs/h/h6918.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_106_17"></a>Psalms 106:17

The ['erets](../../strongs/h/h776.md) [pāṯaḥ](../../strongs/h/h6605.md) and [bālaʿ](../../strongs/h/h1104.md) [Dāṯān](../../strongs/h/h1885.md), and [kāsâ](../../strongs/h/h3680.md) the ['edah](../../strongs/h/h5712.md) of ['Ăḇîrām](../../strongs/h/h48.md).

<a name="psalms_106_18"></a>Psalms 106:18

And an ['esh](../../strongs/h/h784.md) was [bāʿar](../../strongs/h/h1197.md) in their ['edah](../../strongs/h/h5712.md); the [lehāḇâ](../../strongs/h/h3852.md) [lāhaṭ](../../strongs/h/h3857.md) the [rasha'](../../strongs/h/h7563.md).

<a name="psalms_106_19"></a>Psalms 106:19

They ['asah](../../strongs/h/h6213.md) an [ʿēḡel](../../strongs/h/h5695.md) in [ḥōrēḇ](../../strongs/h/h2722.md), and [shachah](../../strongs/h/h7812.md) the [massēḵâ](../../strongs/h/h4541.md).

<a name="psalms_106_20"></a>Psalms 106:20

Thus they [mûr](../../strongs/h/h4171.md) their [kabowd](../../strongs/h/h3519.md) into the [taḇnîṯ](../../strongs/h/h8403.md) of a [showr](../../strongs/h/h7794.md) that ['akal](../../strongs/h/h398.md) ['eseb](../../strongs/h/h6212.md).

<a name="psalms_106_21"></a>Psalms 106:21

They [shakach](../../strongs/h/h7911.md) ['el](../../strongs/h/h410.md) their [yasha'](../../strongs/h/h3467.md), which had ['asah](../../strongs/h/h6213.md) [gadowl](../../strongs/h/h1419.md) in [Mitsrayim](../../strongs/h/h4714.md);

<a name="psalms_106_22"></a>Psalms 106:22

[pala'](../../strongs/h/h6381.md) in the ['erets](../../strongs/h/h776.md) of [Ḥām](../../strongs/h/h2526.md), and [yare'](../../strongs/h/h3372.md) by the [sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md).

<a name="psalms_106_23"></a>Psalms 106:23

Therefore he ['āmar](../../strongs/h/h559.md) that he would [šāmaḏ](../../strongs/h/h8045.md) them, had [lûlē'](../../strongs/h/h3884.md) [Mōshe](../../strongs/h/h4872.md) his [bāḥîr](../../strongs/h/h972.md) ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) him in the [pereṣ](../../strongs/h/h6556.md), to [shuwb](../../strongs/h/h7725.md) his [chemah](../../strongs/h/h2534.md), lest he should [shachath](../../strongs/h/h7843.md) them.

<a name="psalms_106_24"></a>Psalms 106:24

Yea, they [mā'as](../../strongs/h/h3988.md) the [ḥemdâ](../../strongs/h/h2532.md) ['erets](../../strongs/h/h776.md), they ['aman](../../strongs/h/h539.md) not his [dabar](../../strongs/h/h1697.md):

<a name="psalms_106_25"></a>Psalms 106:25

But [rāḡan](../../strongs/h/h7279.md) in their ['ohel](../../strongs/h/h168.md), and [shama'](../../strongs/h/h8085.md) not unto the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_106_26"></a>Psalms 106:26

Therefore he [nasa'](../../strongs/h/h5375.md) his [yad](../../strongs/h/h3027.md) against them, to [naphal](../../strongs/h/h5307.md) them in the [midbar](../../strongs/h/h4057.md):

<a name="psalms_106_27"></a>Psalms 106:27

To [naphal](../../strongs/h/h5307.md) their [zera'](../../strongs/h/h2233.md) also among the [gowy](../../strongs/h/h1471.md), and to [zārâ](../../strongs/h/h2219.md) them in the ['erets](../../strongs/h/h776.md).

<a name="psalms_106_28"></a>Psalms 106:28

They [ṣāmaḏ](../../strongs/h/h6775.md) themselves also unto [Baʿal pᵊʿôr](../../strongs/h/h1187.md), and ['akal](../../strongs/h/h398.md) the [zebach](../../strongs/h/h2077.md) of the [muwth](../../strongs/h/h4191.md).

<a name="psalms_106_29"></a>Psalms 106:29

Thus they [kāʿas](../../strongs/h/h3707.md) him with their [maʿălāl](../../strongs/h/h4611.md): and the [magēp̄â](../../strongs/h/h4046.md) [pāraṣ](../../strongs/h/h6555.md) upon them.

<a name="psalms_106_30"></a>Psalms 106:30

Then ['amad](../../strongs/h/h5975.md) [Pînḥās](../../strongs/h/h6372.md), and executed [palal](../../strongs/h/h6419.md): and so the [magēp̄â](../../strongs/h/h4046.md) was [ʿāṣar](../../strongs/h/h6113.md).

<a name="psalms_106_31"></a>Psalms 106:31

And that was [chashab](../../strongs/h/h2803.md) unto him for [tsedaqah](../../strongs/h/h6666.md) unto [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md).

<a name="psalms_106_32"></a>Psalms 106:32

They [qāṣap̄](../../strongs/h/h7107.md) him also at the [mayim](../../strongs/h/h4325.md) of [Mᵊrîḇâ](../../strongs/h/h4808.md) [Mᵊrîḇâ](../../strongs/h/h4809.md), so that it went [yāraʿ](../../strongs/h/h3415.md) with [Mōshe](../../strongs/h/h4872.md) for their sakes:

<a name="psalms_106_33"></a>Psalms 106:33

Because they [marah](../../strongs/h/h4784.md) his [ruwach](../../strongs/h/h7307.md), so that he spake [bāṭā'](../../strongs/h/h981.md) with his [saphah](../../strongs/h/h8193.md).

<a name="psalms_106_34"></a>Psalms 106:34

They did not [šāmaḏ](../../strongs/h/h8045.md) the ['am](../../strongs/h/h5971.md), concerning whom [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) them:

<a name="psalms_106_35"></a>Psalms 106:35

But were [ʿāraḇ](../../strongs/h/h6148.md) among the [gowy](../../strongs/h/h1471.md), and [lamad](../../strongs/h/h3925.md) their [ma'aseh](../../strongs/h/h4639.md).

<a name="psalms_106_36"></a>Psalms 106:36

And they ['abad](../../strongs/h/h5647.md) their [ʿāṣāḇ](../../strongs/h/h6091.md): which were a [mowqesh](../../strongs/h/h4170.md) unto them.

<a name="psalms_106_37"></a>Psalms 106:37

Yea, they [zabach](../../strongs/h/h2076.md) their [ben](../../strongs/h/h1121.md) and their [bath](../../strongs/h/h1323.md) unto [šēḏ](../../strongs/h/h7700.md),

<a name="psalms_106_38"></a>Psalms 106:38

And [šāp̄aḵ](../../strongs/h/h8210.md) [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md), even the [dam](../../strongs/h/h1818.md) of their [ben](../../strongs/h/h1121.md) and of their [bath](../../strongs/h/h1323.md), whom they [zabach](../../strongs/h/h2076.md) unto the [ʿāṣāḇ](../../strongs/h/h6091.md) of [kĕna'an](../../strongs/h/h3667.md): and the ['erets](../../strongs/h/h776.md) was [ḥānēp̄](../../strongs/h/h2610.md) with [dam](../../strongs/h/h1818.md).

<a name="psalms_106_39"></a>Psalms 106:39

Thus were they [ṭāmē'](../../strongs/h/h2930.md) with their own [ma'aseh](../../strongs/h/h4639.md), and went a [zānâ](../../strongs/h/h2181.md) with their own [maʿălāl](../../strongs/h/h4611.md).

<a name="psalms_106_40"></a>Psalms 106:40

Therefore was the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) [ḥārâ](../../strongs/h/h2734.md) against his ['am](../../strongs/h/h5971.md), insomuch that he [ta'ab](../../strongs/h/h8581.md) his own [nachalah](../../strongs/h/h5159.md).

<a name="psalms_106_41"></a>Psalms 106:41

And he [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of the [gowy](../../strongs/h/h1471.md); and they that [sane'](../../strongs/h/h8130.md) them [mashal](../../strongs/h/h4910.md) over them.

<a name="psalms_106_42"></a>Psalms 106:42

Their ['oyeb](../../strongs/h/h341.md) also [lāḥaṣ](../../strongs/h/h3905.md) them, and they were [kānaʿ](../../strongs/h/h3665.md) under their [yad](../../strongs/h/h3027.md).

<a name="psalms_106_43"></a>Psalms 106:43

[rab](../../strongs/h/h7227.md) [pa'am](../../strongs/h/h6471.md) did he [natsal](../../strongs/h/h5337.md) them; but they [marah](../../strongs/h/h4784.md) him with their ['etsah](../../strongs/h/h6098.md), and were [māḵaḵ](../../strongs/h/h4355.md) for their ['avon](../../strongs/h/h5771.md).

<a name="psalms_106_44"></a>Psalms 106:44

Nevertheless he [ra'ah](../../strongs/h/h7200.md) their [tsar](../../strongs/h/h6862.md), when he [shama'](../../strongs/h/h8085.md) their [rinnah](../../strongs/h/h7440.md):

<a name="psalms_106_45"></a>Psalms 106:45

And he [zakar](../../strongs/h/h2142.md) for them his [bĕriyth](../../strongs/h/h1285.md), and [nacham](../../strongs/h/h5162.md) according to the [rōḇ](../../strongs/h/h7230.md) of his [checed](../../strongs/h/h2617.md).

<a name="psalms_106_46"></a>Psalms 106:46

He [nathan](../../strongs/h/h5414.md) them also to be [raḥam](../../strongs/h/h7356.md) [paniym](../../strongs/h/h6440.md) all those that carried them [šāḇâ](../../strongs/h/h7617.md).

<a name="psalms_106_47"></a>Psalms 106:47

[yasha'](../../strongs/h/h3467.md) us, [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), and [qāḇaṣ](../../strongs/h/h6908.md) us from among the [gowy](../../strongs/h/h1471.md), to [yadah](../../strongs/h/h3034.md) unto thy [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md), and to [shabach](../../strongs/h/h7623.md) in thy [tehillah](../../strongs/h/h8416.md).

<a name="psalms_106_48"></a>Psalms 106:48

[barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) from ['owlam](../../strongs/h/h5769.md) to ['owlam](../../strongs/h/h5769.md): and let all the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md). [halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 105](psalms_105.md) - [Psalms 107](psalms_107.md)