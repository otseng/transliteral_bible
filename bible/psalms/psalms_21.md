# [Psalms 21](https://www.blueletterbible.org/kjv/psa/21/1/s_499001)

<a name="psalms_21_1"></a>Psalms 21:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). The [melek](../../strongs/h/h4428.md) shall [samach](../../strongs/h/h8055.md) in thy ['oz](../../strongs/h/h5797.md), [Yĕhovah](../../strongs/h/h3068.md); and in thy [yĕshuw'ah](../../strongs/h/h3444.md) how [me'od](../../strongs/h/h3966.md) shall he [giyl](../../strongs/h/h1523.md)!

<a name="psalms_21_2"></a>Psalms 21:2

Thou hast [nathan](../../strongs/h/h5414.md) him his [leb](../../strongs/h/h3820.md) [ta'avah](../../strongs/h/h8378.md), and hast not [mānaʿ](../../strongs/h/h4513.md) the ['ărešeṯ](../../strongs/h/h782.md) of his [saphah](../../strongs/h/h8193.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_21_3"></a>Psalms 21:3

For thou [qadam](../../strongs/h/h6923.md) him with the [bĕrakah](../../strongs/h/h1293.md) of [towb](../../strongs/h/h2896.md): thou [shiyth](../../strongs/h/h7896.md) a [ʿăṭārâ](../../strongs/h/h5850.md) of [pāz](../../strongs/h/h6337.md) on his [ro'sh](../../strongs/h/h7218.md).

<a name="psalms_21_4"></a>Psalms 21:4

He [sha'al](../../strongs/h/h7592.md) [chay](../../strongs/h/h2416.md) of thee, thou [nathan](../../strongs/h/h5414.md) him, ['ōreḵ](../../strongs/h/h753.md) of [yowm](../../strongs/h/h3117.md) ['owlam](../../strongs/h/h5769.md) and [ʿaḏ](../../strongs/h/h5703.md).

<a name="psalms_21_5"></a>Psalms 21:5

His [kabowd](../../strongs/h/h3519.md) is [gadowl](../../strongs/h/h1419.md) in thy [yĕshuw'ah](../../strongs/h/h3444.md): [howd](../../strongs/h/h1935.md) and [hadar](../../strongs/h/h1926.md) hast thou [šāvâ](../../strongs/h/h7737.md) upon him.

<a name="psalms_21_6"></a>Psalms 21:6

For thou hast [shiyth](../../strongs/h/h7896.md) him [bĕrakah](../../strongs/h/h1293.md) for ever: thou hast [ḥāḏâ](../../strongs/h/h2302.md) him [simchah](../../strongs/h/h8057.md) with thy [paniym](../../strongs/h/h6440.md).

<a name="psalms_21_7"></a>Psalms 21:7

For the [melek](../../strongs/h/h4428.md) [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md), and through the [checed](../../strongs/h/h2617.md) of the ['elyown](../../strongs/h/h5945.md) he shall not be [mowt](../../strongs/h/h4131.md).

<a name="psalms_21_8"></a>Psalms 21:8

Thine [yad](../../strongs/h/h3027.md) shall [māṣā'](../../strongs/h/h4672.md) all thine ['oyeb](../../strongs/h/h341.md): thy [yamiyn](../../strongs/h/h3225.md) shall [māṣā'](../../strongs/h/h4672.md) those that [sane'](../../strongs/h/h8130.md) thee.

<a name="psalms_21_9"></a>Psalms 21:9

Thou shalt [shiyth](../../strongs/h/h7896.md) them as an ['esh](../../strongs/h/h784.md) [tannûr](../../strongs/h/h8574.md) in the [ʿēṯ](../../strongs/h/h6256.md) of thine [paniym](../../strongs/h/h6440.md): [Yĕhovah](../../strongs/h/h3068.md) shall [bālaʿ](../../strongs/h/h1104.md) them up in his ['aph](../../strongs/h/h639.md), and the ['esh](../../strongs/h/h784.md) shall ['akal](../../strongs/h/h398.md) them.

<a name="psalms_21_10"></a>Psalms 21:10

Their [pĕriy](../../strongs/h/h6529.md) shalt thou ['abad](../../strongs/h/h6.md) from the ['erets](../../strongs/h/h776.md), and their [zera'](../../strongs/h/h2233.md) from among the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md).

<a name="psalms_21_11"></a>Psalms 21:11

For they intended [ra'](../../strongs/h/h7451.md) against thee: they [chashab](../../strongs/h/h2803.md) [mezimmah](../../strongs/h/h4209.md), they are not [yakol](../../strongs/h/h3201.md).

<a name="psalms_21_12"></a>Psalms 21:12

Therefore shalt thou [shiyth](../../strongs/h/h7896.md) them turn their [šᵊḵem](../../strongs/h/h7926.md), thou shalt [kuwn](../../strongs/h/h3559.md) upon thy [mêṯār](../../strongs/h/h4340.md) against the [paniym](../../strongs/h/h6440.md) of them.

<a name="psalms_21_13"></a>Psalms 21:13

Be thou [ruwm](../../strongs/h/h7311.md), [Yĕhovah](../../strongs/h/h3068.md), in thine own ['oz](../../strongs/h/h5797.md): so will we [shiyr](../../strongs/h/h7891.md) and [zamar](../../strongs/h/h2167.md) thy [gᵊḇûrâ](../../strongs/h/h1369.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 20](psalms_20.md) - [Psalms 22](psalms_22.md)