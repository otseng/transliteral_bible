# [Psalms 138](https://www.blueletterbible.org/kjv/psalms/138)

<a name="psalms_138_1"></a>Psalms 138:1

A Psalm of [Dāviḏ](../../strongs/h/h1732.md). I will [yadah](../../strongs/h/h3034.md) thee with my whole [leb](../../strongs/h/h3820.md): before the ['Elohiym](../../strongs/h/h430.md) will I [zamar](../../strongs/h/h2167.md) unto thee.

<a name="psalms_138_2"></a>Psalms 138:2

I will [shachah](../../strongs/h/h7812.md) toward thy [qodesh](../../strongs/h/h6944.md) [heykal](../../strongs/h/h1964.md), and [yadah](../../strongs/h/h3034.md) thy [shem](../../strongs/h/h8034.md) for thy [checed](../../strongs/h/h2617.md) and for thy ['emeth](../../strongs/h/h571.md): for thou hast [gāḏal](../../strongs/h/h1431.md) thy ['imrah](../../strongs/h/h565.md) above all thy [shem](../../strongs/h/h8034.md).

<a name="psalms_138_3"></a>Psalms 138:3

In the [yowm](../../strongs/h/h3117.md) when I [qara'](../../strongs/h/h7121.md) thou ['anah](../../strongs/h/h6030.md) me, and [rāhaḇ](../../strongs/h/h7292.md) me with ['oz](../../strongs/h/h5797.md) in my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_138_4"></a>Psalms 138:4

All the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md) shall [yadah](../../strongs/h/h3034.md) thee, [Yĕhovah](../../strongs/h/h3068.md), when they [shama'](../../strongs/h/h8085.md) the ['emer](../../strongs/h/h561.md) of thy [peh](../../strongs/h/h6310.md).

<a name="psalms_138_5"></a>Psalms 138:5

Yea, they shall [shiyr](../../strongs/h/h7891.md) in the [derek](../../strongs/h/h1870.md) of [Yĕhovah](../../strongs/h/h3068.md): for [gadowl](../../strongs/h/h1419.md) is the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_138_6"></a>Psalms 138:6

Though [Yĕhovah](../../strongs/h/h3068.md) be [ruwm](../../strongs/h/h7311.md), yet hath he [ra'ah](../../strongs/h/h7200.md) unto the [šāp̄āl](../../strongs/h/h8217.md): but the [gāḇōha](../../strongs/h/h1364.md) he [yada'](../../strongs/h/h3045.md) afar [merḥāq](../../strongs/h/h4801.md).

<a name="psalms_138_7"></a>Psalms 138:7

Though I [yālaḵ](../../strongs/h/h3212.md) in the [qereḇ](../../strongs/h/h7130.md) of [tsarah](../../strongs/h/h6869.md), thou wilt [ḥāyâ](../../strongs/h/h2421.md) me: thou shalt [shalach](../../strongs/h/h7971.md) thine [yad](../../strongs/h/h3027.md) against the ['aph](../../strongs/h/h639.md) of mine ['oyeb](../../strongs/h/h341.md), and thy [yamiyn](../../strongs/h/h3225.md) shall [yasha'](../../strongs/h/h3467.md) me.

<a name="psalms_138_8"></a>Psalms 138:8

[Yĕhovah](../../strongs/h/h3068.md) will [gāmar](../../strongs/h/h1584.md) that which concerneth me: thy [checed](../../strongs/h/h2617.md), [Yĕhovah](../../strongs/h/h3068.md), endureth ['owlam](../../strongs/h/h5769.md): [rāp̄â](../../strongs/h/h7503.md) not the [ma'aseh](../../strongs/h/h4639.md) of thine own [yad](../../strongs/h/h3027.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 137](psalms_137.md) - [Psalms 139](psalms_139.md)