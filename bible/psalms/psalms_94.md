# [Psalms 94](https://www.blueletterbible.org/kjv/psalms/94)

<a name="psalms_94_1"></a>Psalms 94:1

[Yĕhovah](../../strongs/h/h3068.md) ['el](../../strongs/h/h410.md), to whom [nᵊqāmâ](../../strongs/h/h5360.md) belongeth; ['el](../../strongs/h/h410.md), to whom [nᵊqāmâ](../../strongs/h/h5360.md) belongeth, [yāp̄aʿ](../../strongs/h/h3313.md) thyself.

<a name="psalms_94_2"></a>Psalms 94:2

[nasa'](../../strongs/h/h5375.md) thyself, thou [shaphat](../../strongs/h/h8199.md) of the ['erets](../../strongs/h/h776.md): [shuwb](../../strongs/h/h7725.md) a [gĕmwl](../../strongs/h/h1576.md) to the [gē'ê](../../strongs/h/h1343.md).

<a name="psalms_94_3"></a>Psalms 94:3

[Yĕhovah](../../strongs/h/h3068.md), how long shall the [rasha'](../../strongs/h/h7563.md), how long shall the [rasha'](../../strongs/h/h7563.md) [ʿālaz](../../strongs/h/h5937.md)?

<a name="psalms_94_4"></a>Psalms 94:4

How long shall they [nāḇaʿ](../../strongs/h/h5042.md) and [dabar](../../strongs/h/h1696.md) [ʿāṯāq](../../strongs/h/h6277.md)? and all the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md) ['āmar](../../strongs/h/h559.md) themselves?

<a name="psalms_94_5"></a>Psalms 94:5

They break in [dāḵā'](../../strongs/h/h1792.md) thy ['am](../../strongs/h/h5971.md), [Yĕhovah](../../strongs/h/h3068.md), and [ʿānâ](../../strongs/h/h6031.md) thine [nachalah](../../strongs/h/h5159.md).

<a name="psalms_94_6"></a>Psalms 94:6

They [harag](../../strongs/h/h2026.md) the ['almānâ](../../strongs/h/h490.md) and the [ger](../../strongs/h/h1616.md), and [ratsach](../../strongs/h/h7523.md) the [yathowm](../../strongs/h/h3490.md).

<a name="psalms_94_7"></a>Psalms 94:7

Yet they ['āmar](../../strongs/h/h559.md), The [Yahh](../../strongs/h/h3050.md) shall not [ra'ah](../../strongs/h/h7200.md), neither shall the ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md) [bîn](../../strongs/h/h995.md) it.

<a name="psalms_94_8"></a>Psalms 94:8

[bîn](../../strongs/h/h995.md), ye [bāʿar](../../strongs/h/h1197.md) among the ['am](../../strongs/h/h5971.md): and ye [kᵊsîl](../../strongs/h/h3684.md), when will ye be [sakal](../../strongs/h/h7919.md)?

<a name="psalms_94_9"></a>Psalms 94:9

He that [nāṭaʿ](../../strongs/h/h5193.md) the ['ozen](../../strongs/h/h241.md), shall he not [shama'](../../strongs/h/h8085.md)? he that [yāṣar](../../strongs/h/h3335.md) the ['ayin](../../strongs/h/h5869.md), shall he not [nabat](../../strongs/h/h5027.md)?

<a name="psalms_94_10"></a>Psalms 94:10

He that [yacar](../../strongs/h/h3256.md) the [gowy](../../strongs/h/h1471.md), shall not he [yakach](../../strongs/h/h3198.md)? he that [lamad](../../strongs/h/h3925.md) ['āḏām](../../strongs/h/h120.md) [da'ath](../../strongs/h/h1847.md), shall not he know?

<a name="psalms_94_11"></a>Psalms 94:11

[Yĕhovah](../../strongs/h/h3068.md) [yada'](../../strongs/h/h3045.md) the [maḥăšāḇâ](../../strongs/h/h4284.md) of ['āḏām](../../strongs/h/h120.md), that they are [heḇel](../../strongs/h/h1892.md).

<a name="psalms_94_12"></a>Psalms 94:12

['esher](../../strongs/h/h835.md) is the [geḇer](../../strongs/h/h1397.md) whom thou [yacar](../../strongs/h/h3256.md), [Yahh](../../strongs/h/h3050.md), and [lamad](../../strongs/h/h3925.md) him out of thy [towrah](../../strongs/h/h8451.md);

<a name="psalms_94_13"></a>Psalms 94:13

That thou mayest give him [šāqaṭ](../../strongs/h/h8252.md) from the [yowm](../../strongs/h/h3117.md) of [ra'](../../strongs/h/h7451.md), until the [shachath](../../strongs/h/h7845.md) be [karah](../../strongs/h/h3738.md) for the [rasha'](../../strongs/h/h7563.md).

<a name="psalms_94_14"></a>Psalms 94:14

For [Yĕhovah](../../strongs/h/h3068.md) will not [nāṭaš](../../strongs/h/h5203.md) his ['am](../../strongs/h/h5971.md), neither will he ['azab](../../strongs/h/h5800.md) his [nachalah](../../strongs/h/h5159.md).

<a name="psalms_94_15"></a>Psalms 94:15

But [mishpat](../../strongs/h/h4941.md) shall [shuwb](../../strongs/h/h7725.md) unto [tsedeq](../../strongs/h/h6664.md): and all the [yashar](../../strongs/h/h3477.md) in [leb](../../strongs/h/h3820.md) shall ['aḥar](../../strongs/h/h310.md) it.

<a name="psalms_94_16"></a>Psalms 94:16

Who will [quwm](../../strongs/h/h6965.md) for me against the [ra'a'](../../strongs/h/h7489.md)? or who will [yatsab](../../strongs/h/h3320.md) for me against the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md)?

<a name="psalms_94_17"></a>Psalms 94:17

[lûlē'](../../strongs/h/h3884.md) [Yĕhovah](../../strongs/h/h3068.md) had been my [ʿezrâ](../../strongs/h/h5833.md), my [nephesh](../../strongs/h/h5315.md) had [mᵊʿaṭ](../../strongs/h/h4592.md) [shakan](../../strongs/h/h7931.md) in [dûmâ](../../strongs/h/h1745.md).

<a name="psalms_94_18"></a>Psalms 94:18

When I ['āmar](../../strongs/h/h559.md), My [regel](../../strongs/h/h7272.md) [mowt](../../strongs/h/h4131.md); thy [checed](../../strongs/h/h2617.md), [Yĕhovah](../../strongs/h/h3068.md), held me [sāʿaḏ](../../strongs/h/h5582.md).

<a name="psalms_94_19"></a>Psalms 94:19

In the [rōḇ](../../strongs/h/h7230.md) of my [śarʿapîm](../../strongs/h/h8312.md) [qereḇ](../../strongs/h/h7130.md) me thy [tanḥûmôṯ](../../strongs/h/h8575.md) [šāʿaʿ](../../strongs/h/h8173.md) my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_94_20"></a>Psalms 94:20

Shall the [kicce'](../../strongs/h/h3678.md) of [havvah](../../strongs/h/h1942.md) have [ḥāḇar](../../strongs/h/h2266.md) with thee, which [yāṣar](../../strongs/h/h3335.md) ['amal](../../strongs/h/h5999.md) by a [choq](../../strongs/h/h2706.md)?

<a name="psalms_94_21"></a>Psalms 94:21

They gather themselves [gāḏaḏ](../../strongs/h/h1413.md) against the [nephesh](../../strongs/h/h5315.md) of the [tsaddiyq](../../strongs/h/h6662.md), and [rāšaʿ](../../strongs/h/h7561.md) the [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md).

<a name="psalms_94_22"></a>Psalms 94:22

But [Yĕhovah](../../strongs/h/h3068.md) is my [misgab](../../strongs/h/h4869.md); and my ['Elohiym](../../strongs/h/h430.md) is the [tsuwr](../../strongs/h/h6697.md) of my [machaceh](../../strongs/h/h4268.md).

<a name="psalms_94_23"></a>Psalms 94:23

And he shall [shuwb](../../strongs/h/h7725.md) upon them their own ['aven](../../strongs/h/h205.md), and shall [ṣāmaṯ](../../strongs/h/h6789.md) them in their own [ra'](../../strongs/h/h7451.md); yea, [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) shall [ṣāmaṯ](../../strongs/h/h6789.md) them.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 93](psalms_93.md) - [Psalms 95](psalms_95.md)