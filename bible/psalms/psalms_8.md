# [Psalms 8](https://www.blueletterbible.org/kjv/psa/8/1/s_486001)

<a name="psalms_8_1"></a>Psalms 8:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon [gitîṯ](../../strongs/h/h1665.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [Yĕhovah](../../strongs/h/h3068.md), ['adown](../../strongs/h/h113.md), how ['addiyr](../../strongs/h/h117.md) is thy [shem](../../strongs/h/h8034.md) in all the ['erets](../../strongs/h/h776.md)! who hast set thy [howd](../../strongs/h/h1935.md) above the [shamayim](../../strongs/h/h8064.md).

<a name="psalms_8_2"></a>Psalms 8:2

Out of the [peh](../../strongs/h/h6310.md) of ['owlel](../../strongs/h/h5768.md) and [yānaq](../../strongs/h/h3243.md) hast thou [yacad](../../strongs/h/h3245.md) ['oz](../../strongs/h/h5797.md) because of thine [tsarar](../../strongs/h/h6887.md), that thou [shabath](../../strongs/h/h7673.md) the ['oyeb](../../strongs/h/h341.md) and the [naqam](../../strongs/h/h5358.md).

<a name="psalms_8_3"></a>Psalms 8:3

When I [ra'ah](../../strongs/h/h7200.md) thy [shamayim](../../strongs/h/h8064.md), the [ma'aseh](../../strongs/h/h4639.md) of thy ['etsba'](../../strongs/h/h676.md), the [yareach](../../strongs/h/h3394.md) and the [kowkab](../../strongs/h/h3556.md), which thou hast [kuwn](../../strongs/h/h3559.md);

<a name="psalms_8_4"></a>Psalms 8:4

What is ['enowsh](../../strongs/h/h582.md), that thou art [zakar](../../strongs/h/h2142.md) of him? and the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md), that thou [paqad](../../strongs/h/h6485.md) him?

<a name="psalms_8_5"></a>Psalms 8:5

For thou hast made him a [mᵊʿaṭ](../../strongs/h/h4592.md) [ḥāsēr](../../strongs/h/h2637.md) than the ['Elohiym](../../strongs/h/h430.md), and hast ['atar](../../strongs/h/h5849.md) him with [kabowd](../../strongs/h/h3519.md) and [hadar](../../strongs/h/h1926.md).

<a name="psalms_8_6"></a>Psalms 8:6

Thou madest him to have [mashal](../../strongs/h/h4910.md) over the [ma'aseh](../../strongs/h/h4639.md) of thy [yad](../../strongs/h/h3027.md); thou hast [shiyth](../../strongs/h/h7896.md) all things under his [regel](../../strongs/h/h7272.md):

<a name="psalms_8_7"></a>Psalms 8:7

All [tsone'](../../strongs/h/h6792.md) and ['eleph](../../strongs/h/h504.md), yea, and the [bĕhemah](../../strongs/h/h929.md) of the [sadeh](../../strongs/h/h7704.md);

<a name="psalms_8_8"></a>Psalms 8:8

The [tsippowr](../../strongs/h/h6833.md) of the [shamayim](../../strongs/h/h8064.md), and the [dag](../../strongs/h/h1709.md) of the [yam](../../strongs/h/h3220.md), and whatsoever ['abar](../../strongs/h/h5674.md) the ['orach](../../strongs/h/h734.md) of the [yam](../../strongs/h/h3220.md).

<a name="psalms_8_9"></a>Psalms 8:9

[Yĕhovah](../../strongs/h/h3068.md) ['adown](../../strongs/h/h113.md), how ['addiyr](../../strongs/h/h117.md) is thy [shem](../../strongs/h/h8034.md) in all the ['erets](../../strongs/h/h776.md)!

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 7](psalms_7.md) - [Psalms 9](psalms_9.md)