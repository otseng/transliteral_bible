# [Psalms 80](https://www.blueletterbible.org/kjv/psa/80)

<a name="psalms_80_1"></a>Psalms 80:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon [šûšan ʿēḏûṯ](../../strongs/h/h7802.md), A [mizmôr](../../strongs/h/h4210.md) of ['Āsāp̄](../../strongs/h/h623.md). ['azan](../../strongs/h/h238.md), [ra'ah](../../strongs/h/h7462.md) of [Yisra'el](../../strongs/h/h3478.md), thou that [nāhaḡ](../../strongs/h/h5090.md) [Yôsēp̄](../../strongs/h/h3130.md) like a [tso'n](../../strongs/h/h6629.md); thou that [yashab](../../strongs/h/h3427.md) between the [kĕruwb](../../strongs/h/h3742.md), [yāp̄aʿ](../../strongs/h/h3313.md).

<a name="psalms_80_2"></a>Psalms 80:2

[paniym](../../strongs/h/h6440.md) ['Ep̄rayim](../../strongs/h/h669.md) and [Binyāmîn](../../strongs/h/h1144.md) and [Mᵊnaššê](../../strongs/h/h4519.md) [ʿûr](../../strongs/h/h5782.md) thy [gᵊḇûrâ](../../strongs/h/h1369.md), and [yālaḵ](../../strongs/h/h3212.md) and [yĕshuw'ah](../../strongs/h/h3444.md) us.

<a name="psalms_80_3"></a>Psalms 80:3

[shuwb](../../strongs/h/h7725.md) us, ['Elohiym](../../strongs/h/h430.md), and cause thy [paniym](../../strongs/h/h6440.md) to ['owr](../../strongs/h/h215.md); and we shall be [yasha'](../../strongs/h/h3467.md).

<a name="psalms_80_4"></a>Psalms 80:4

[Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), how long wilt thou be [ʿāšēn](../../strongs/h/h6225.md) against the [tĕphillah](../../strongs/h/h8605.md) of thy ['am](../../strongs/h/h5971.md) ?

<a name="psalms_80_5"></a>Psalms 80:5

Thou ['akal](../../strongs/h/h398.md) them with the [lechem](../../strongs/h/h3899.md) of [dim'ah](../../strongs/h/h1832.md); and givest them [dim'ah](../../strongs/h/h1832.md) to [šāqâ](../../strongs/h/h8248.md) in [šālîš](../../strongs/h/h7991.md).

<a name="psalms_80_6"></a>Psalms 80:6

Thou [śûm](../../strongs/h/h7760.md) us a [māḏôn](../../strongs/h/h4066.md) unto our [šāḵēn](../../strongs/h/h7934.md): and our ['oyeb](../../strongs/h/h341.md) [lāʿaḡ](../../strongs/h/h3932.md) among themselves.

<a name="psalms_80_7"></a>Psalms 80:7

[shuwb](../../strongs/h/h7725.md) us, ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), and cause thy [paniym](../../strongs/h/h6440.md) to ['owr](../../strongs/h/h215.md); and we shall be [yasha'](../../strongs/h/h3467.md).

<a name="psalms_80_8"></a>Psalms 80:8

Thou hast [nāsaʿ](../../strongs/h/h5265.md) a [gep̄en](../../strongs/h/h1612.md) out of [Mitsrayim](../../strongs/h/h4714.md): thou hast [gāraš](../../strongs/h/h1644.md) the [gowy](../../strongs/h/h1471.md), and [nāṭaʿ](../../strongs/h/h5193.md) it.

<a name="psalms_80_9"></a>Psalms 80:9

Thou [panah](../../strongs/h/h6437.md) room [paniym](../../strongs/h/h6440.md) it, and didst cause it to [šereš](../../strongs/h/h8328.md) [šērēš](../../strongs/h/h8327.md), and it [mālā'](../../strongs/h/h4390.md) the ['erets](../../strongs/h/h776.md).

<a name="psalms_80_10"></a>Psalms 80:10

The [har](../../strongs/h/h2022.md) were [kāsâ](../../strongs/h/h3680.md) with the [ṣēl](../../strongs/h/h6738.md) of it, and the [ʿānāp̄](../../strongs/h/h6057.md) thereof were like the ['el](../../strongs/h/h410.md) ['erez](../../strongs/h/h730.md).

<a name="psalms_80_11"></a>Psalms 80:11

She [shalach](../../strongs/h/h7971.md) her [qāṣîr](../../strongs/h/h7105.md) unto the [yam](../../strongs/h/h3220.md), and her [yôneqeṯ](../../strongs/h/h3127.md) unto the [nāhār](../../strongs/h/h5104.md).

<a name="psalms_80_12"></a>Psalms 80:12

Why hast thou then [pāraṣ](../../strongs/h/h6555.md) her [gāḏēr](../../strongs/h/h1447.md), so that all they which ['abar](../../strongs/h/h5674.md) by the [derek](../../strongs/h/h1870.md) do ['ārâ](../../strongs/h/h717.md) her?

<a name="psalms_80_13"></a>Psalms 80:13

The [ḥăzîr](../../strongs/h/h2386.md) out of the [yaʿar](../../strongs/h/h3293.md) doth [kirsēm](../../strongs/h/h3765.md) it, and the [zîz](../../strongs/h/h2123.md) of the [sadeh](../../strongs/h/h7704.md) doth [ra'ah](../../strongs/h/h7462.md) it.

<a name="psalms_80_14"></a>Psalms 80:14

[shuwb](../../strongs/h/h7725.md), we beseech thee, ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md): [nabat](../../strongs/h/h5027.md) from [shamayim](../../strongs/h/h8064.md), and [ra'ah](../../strongs/h/h7200.md), and [paqad](../../strongs/h/h6485.md) this [gep̄en](../../strongs/h/h1612.md);

<a name="psalms_80_15"></a>Psalms 80:15

And the [kannâ](../../strongs/h/h3657.md) [kānan](../../strongs/h/h3661.md) which thy [yamiyn](../../strongs/h/h3225.md) hath [nāṭaʿ](../../strongs/h/h5193.md), and the [ben](../../strongs/h/h1121.md) that thou ['amats](../../strongs/h/h553.md) for thyself.

<a name="psalms_80_16"></a>Psalms 80:16

It is [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md), it is [kāsaḥ](../../strongs/h/h3683.md): they ['abad](../../strongs/h/h6.md) at the [ge'arah](../../strongs/h/h1606.md) of thy [paniym](../../strongs/h/h6440.md).

<a name="psalms_80_17"></a>Psalms 80:17

Let thy [yad](../../strongs/h/h3027.md) be upon the ['iysh](../../strongs/h/h376.md) of thy [yamiyn](../../strongs/h/h3225.md), upon the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md) whom thou madest ['amats](../../strongs/h/h553.md) for thyself.

<a name="psalms_80_18"></a>Psalms 80:18

So will not we [sûḡ](../../strongs/h/h5472.md) from thee: [ḥāyâ](../../strongs/h/h2421.md) us, and we will [qara'](../../strongs/h/h7121.md) upon thy [shem](../../strongs/h/h8034.md).

<a name="psalms_80_19"></a>Psalms 80:19

[shuwb](../../strongs/h/h7725.md) us, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), cause thy [paniym](../../strongs/h/h6440.md) to ['owr](../../strongs/h/h215.md); and we shall be [yasha'](../../strongs/h/h3467.md).


---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 79](psalms_79.md) - [Psalms 81](psalms_81.md)