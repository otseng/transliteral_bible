# [Psalms 9](https://www.blueletterbible.org/kjv/psa/9/1/s_487001)

<a name="psalms_9_1"></a>Psalms 9:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon [Mûṯ](../../strongs/h/h4192.md) [ben](../../strongs/h/h1121.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). I will [yadah](../../strongs/h/h3034.md) thee, [Yĕhovah](../../strongs/h/h3068.md), with my whole [leb](../../strongs/h/h3820.md); I will [sāp̄ar](../../strongs/h/h5608.md) all thy [pala'](../../strongs/h/h6381.md).

<a name="psalms_9_2"></a>Psalms 9:2

I will be [samach](../../strongs/h/h8055.md) and ['alats](../../strongs/h/h5970.md) in thee: I will [zamar](../../strongs/h/h2167.md) to thy [shem](../../strongs/h/h8034.md), ['elyown](../../strongs/h/h5945.md).

<a name="psalms_9_3"></a>Psalms 9:3

When mine ['oyeb](../../strongs/h/h341.md) are [shuwb](../../strongs/h/h7725.md) ['āḥôr](../../strongs/h/h268.md), they shall [kashal](../../strongs/h/h3782.md) and ['abad](../../strongs/h/h6.md) at thy [paniym](../../strongs/h/h6440.md).

<a name="psalms_9_4"></a>Psalms 9:4

For thou hast ['asah](../../strongs/h/h6213.md) my [mishpat](../../strongs/h/h4941.md) and my [diyn](../../strongs/h/h1779.md); thou [yashab](../../strongs/h/h3427.md) in the [kicce'](../../strongs/h/h3678.md) [shaphat](../../strongs/h/h8199.md) [tsedeq](../../strongs/h/h6664.md).

<a name="psalms_9_5"></a>Psalms 9:5

Thou hast [gāʿar](../../strongs/h/h1605.md) the [gowy](../../strongs/h/h1471.md), thou hast ['abad](../../strongs/h/h6.md) the [rasha'](../../strongs/h/h7563.md), thou hast [māḥâ](../../strongs/h/h4229.md) their [shem](../../strongs/h/h8034.md) ['owlam](../../strongs/h/h5769.md) and [ʿaḏ](../../strongs/h/h5703.md).

<a name="psalms_9_6"></a>Psalms 9:6

O thou ['oyeb](../../strongs/h/h341.md), [chorbah](../../strongs/h/h2723.md) are come to a [netsach](../../strongs/h/h5331.md) [tamam](../../strongs/h/h8552.md): and thou hast [nathash](../../strongs/h/h5428.md) [ʿār](../../strongs/h/h6145.md) [ʿîr](../../strongs/h/h5892.md); their [zeker](../../strongs/h/h2143.md) is ['abad](../../strongs/h/h6.md) with them.

<a name="psalms_9_7"></a>Psalms 9:7

But [Yĕhovah](../../strongs/h/h3068.md) shall [yashab](../../strongs/h/h3427.md) ['owlam](../../strongs/h/h5769.md): he hath [kuwn](../../strongs/h/h3559.md) his [kicce'](../../strongs/h/h3678.md) for [mishpat](../../strongs/h/h4941.md).

<a name="psalms_9_8"></a>Psalms 9:8

And he shall [shaphat](../../strongs/h/h8199.md) the [tebel](../../strongs/h/h8398.md) in [tsedeq](../../strongs/h/h6664.md), he shall [diyn](../../strongs/h/h1777.md) to the [lĕom](../../strongs/h/h3816.md) in [meyshar](../../strongs/h/h4339.md).

<a name="psalms_9_9"></a>Psalms 9:9

[Yĕhovah](../../strongs/h/h3068.md) also will be a [misgab](../../strongs/h/h4869.md) for the [dak](../../strongs/h/h1790.md), a [misgab](../../strongs/h/h4869.md) in times of [tsarah](../../strongs/h/h6869.md).

<a name="psalms_9_10"></a>Psalms 9:10

And they that [yada'](../../strongs/h/h3045.md) thy [shem](../../strongs/h/h8034.md) will put their [batach](../../strongs/h/h982.md) in thee: for thou, [Yĕhovah](../../strongs/h/h3068.md), hast not ['azab](../../strongs/h/h5800.md) them that [darash](../../strongs/h/h1875.md) thee.

<a name="psalms_9_11"></a>Psalms 9:11

[zamar](../../strongs/h/h2167.md) to [Yĕhovah](../../strongs/h/h3068.md), which dwelleth in [Tsiyown](../../strongs/h/h6726.md): [nāḡaḏ](../../strongs/h/h5046.md) among the ['am](../../strongs/h/h5971.md) his ['aliylah](../../strongs/h/h5949.md).

<a name="psalms_9_12"></a>Psalms 9:12

When he [darash](../../strongs/h/h1875.md) for [dam](../../strongs/h/h1818.md), he [zakar](../../strongs/h/h2142.md) them: he [shakach](../../strongs/h/h7911.md) not the [tsa'aqah](../../strongs/h/h6818.md) of the ['anav](../../strongs/h/h6035.md) .

<a name="psalms_9_13"></a>Psalms 9:13

Have [chanan](../../strongs/h/h2603.md) upon me, [Yĕhovah](../../strongs/h/h3068.md); consider my ['oniy](../../strongs/h/h6040.md) of them that [sane'](../../strongs/h/h8130.md) me, thou that [ruwm](../../strongs/h/h7311.md) me from the [sha'ar](../../strongs/h/h8179.md) of [maveth](../../strongs/h/h4194.md):

<a name="psalms_9_14"></a>Psalms 9:14

That I may [sāp̄ar](../../strongs/h/h5608.md) all thy [tehillah](../../strongs/h/h8416.md) in the [sha'ar](../../strongs/h/h8179.md) of the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md): I will [giyl](../../strongs/h/h1523.md) in thy [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="psalms_9_15"></a>Psalms 9:15

The [gowy](../../strongs/h/h1471.md) are [ṭāḇaʿ](../../strongs/h/h2883.md) in the [shachath](../../strongs/h/h7845.md) that they made: in the [rešeṯ](../../strongs/h/h7568.md) which they [taman](../../strongs/h/h2934.md) is their own [regel](../../strongs/h/h7272.md) taken.

<a name="psalms_9_16"></a>Psalms 9:16

[Yĕhovah](../../strongs/h/h3068.md) is [yada'](../../strongs/h/h3045.md) by the [mishpat](../../strongs/h/h4941.md) which he ['asah](../../strongs/h/h6213.md): the [rasha'](../../strongs/h/h7563.md) is [nāqaš](../../strongs/h/h5367.md) in the [pōʿal](../../strongs/h/h6467.md) of his own [kaph](../../strongs/h/h3709.md). [higgayown](../../strongs/h/h1902.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_9_17"></a>Psalms 9:17

The [rasha'](../../strongs/h/h7563.md) shall be [shuwb](../../strongs/h/h7725.md) into [shĕ'owl](../../strongs/h/h7585.md), and all the [gowy](../../strongs/h/h1471.md) that [shakeach](../../strongs/h/h7913.md) ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_9_18"></a>Psalms 9:18

For the ['ebyown](../../strongs/h/h34.md) shall not alway be [shakach](../../strongs/h/h7911.md): the [tiqvâ](../../strongs/h/h8615.md) of the ['aniy](../../strongs/h/h6041.md) shall not ['abad](../../strongs/h/h6.md) for ever.

<a name="psalms_9_19"></a>Psalms 9:19

[quwm](../../strongs/h/h6965.md), [Yĕhovah](../../strongs/h/h3068.md); let not man ['azaz](../../strongs/h/h5810.md): let the [gowy](../../strongs/h/h1471.md) be [shaphat](../../strongs/h/h8199.md) in thy [paniym](../../strongs/h/h6440.md).

<a name="psalms_9_20"></a>Psalms 9:20

[shiyth](../../strongs/h/h7896.md) them in [mowra'](../../strongs/h/h4172.md), [Yĕhovah](../../strongs/h/h3068.md): that the [gowy](../../strongs/h/h1471.md) may [yada'](../../strongs/h/h3045.md) themselves to be but ['enowsh](../../strongs/h/h582.md). [Celah](../../strongs/h/h5542.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 8](psalms_8.md) - [Psalms 10](psalms_10.md)