# [Psalms 133](https://www.blueletterbible.org/kjv/psalms/133)

<a name="psalms_133_1"></a>Psalms 133:1

A [šîr](../../strongs/h/h7892.md) of [maʿălâ](../../strongs/h/h4609.md) of [Dāviḏ](../../strongs/h/h1732.md). Behold, how [towb](../../strongs/h/h2896.md) and how [na'iym](../../strongs/h/h5273.md) it is for ['ach](../../strongs/h/h251.md) to [yashab](../../strongs/h/h3427.md) [yaḥaḏ](../../strongs/h/h3162.md) in [yaḥaḏ](../../strongs/h/h3162.md)!

<a name="psalms_133_2"></a>Psalms 133:2

It is like the [towb](../../strongs/h/h2896.md) [šemen](../../strongs/h/h8081.md) upon the [ro'sh](../../strongs/h/h7218.md), that n [yarad](../../strongs/h/h3381.md) upon the [zāqān](../../strongs/h/h2206.md), even ['Ahărôn](../../strongs/h/h175.md) [zāqān](../../strongs/h/h2206.md): that [yarad](../../strongs/h/h3381.md) to the [peh](../../strongs/h/h6310.md) of his [midâ](../../strongs/h/h4060.md);

<a name="psalms_133_3"></a>Psalms 133:3

As the [ṭal](../../strongs/h/h2919.md) of [Ḥermôn](../../strongs/h/h2768.md), and as the dew that [yarad](../../strongs/h/h3381.md) upon the [har](../../strongs/h/h2042.md) of [Tsiyown](../../strongs/h/h6726.md): for there [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) the [bĕrakah](../../strongs/h/h1293.md), even [chay](../../strongs/h/h2416.md) ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 132](psalms_132.md) - [Psalms 134](psalms_134.md)