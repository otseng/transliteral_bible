# [Psalms 18](https://www.blueletterbible.org/kjv/psa/18/1/s_496001)

<a name="psalms_18_1"></a>Psalms 18:1

To the [nāṣaḥ](../../strongs/h/h5329.md), of [Dāviḏ](../../strongs/h/h1732.md), the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md), who [dabar](../../strongs/h/h1696.md) unto [Yĕhovah](../../strongs/h/h3068.md) the [dabar](../../strongs/h/h1697.md) of this [šîr](../../strongs/h/h7892.md) in the [yowm](../../strongs/h/h3117.md) that [Yĕhovah](../../strongs/h/h3068.md) [natsal](../../strongs/h/h5337.md) him from the [kaph](../../strongs/h/h3709.md) of all his ['oyeb](../../strongs/h/h341.md), and from the [yad](../../strongs/h/h3027.md) of [Šā'ûl](../../strongs/h/h7586.md): And he ['āmar](../../strongs/h/h559.md), I will [racham](../../strongs/h/h7355.md) thee, [Yĕhovah](../../strongs/h/h3068.md), my [chezeq](../../strongs/h/h2391.md).

<a name="psalms_18_2"></a>Psalms 18:2

[Yĕhovah](../../strongs/h/h3068.md) my [cela'](../../strongs/h/h5553.md), and my [matsuwd](../../strongs/h/h4686.md), and my [palat](../../strongs/h/h6403.md); my ['el](../../strongs/h/h410.md), my [tsuwr](../../strongs/h/h6697.md), in whom I will [chacah](../../strongs/h/h2620.md); my [magen](../../strongs/h/h4043.md), and the [qeren](../../strongs/h/h7161.md) of my [yesha'](../../strongs/h/h3468.md), and my [misgab](../../strongs/h/h4869.md).

<a name="psalms_18_3"></a>Psalms 18:3

I will [qara'](../../strongs/h/h7121.md) upon [Yĕhovah](../../strongs/h/h3068.md), to be [halal](../../strongs/h/h1984.md): so shall I be [yasha'](../../strongs/h/h3467.md) from mine ['oyeb](../../strongs/h/h341.md).

<a name="psalms_18_4"></a>Psalms 18:4

The [chebel](../../strongs/h/h2256.md) of [maveth](../../strongs/h/h4194.md) ['āp̄ap̄](../../strongs/h/h661.md) me, and the [nachal](../../strongs/h/h5158.md) of [beliya'al](../../strongs/h/h1100.md) [ba'ath](../../strongs/h/h1204.md) me.

<a name="psalms_18_5"></a>Psalms 18:5

The [chebel](../../strongs/h/h2256.md) of [shĕ'owl](../../strongs/h/h7585.md) [cabab](../../strongs/h/h5437.md) me: the [mowqesh](../../strongs/h/h4170.md) of [maveth](../../strongs/h/h4194.md) [qadam](../../strongs/h/h6923.md) me.

<a name="psalms_18_6"></a>Psalms 18:6

In my [tsar](../../strongs/h/h6862.md) I [qara'](../../strongs/h/h7121.md) upon [Yĕhovah](../../strongs/h/h3068.md), and [šāvaʿ](../../strongs/h/h7768.md) unto ['Elohiym](../../strongs/h/h430.md): he [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md) out of his [heykal](../../strongs/h/h1964.md), and my [shav'ah](../../strongs/h/h7775.md) [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) him, even into his ['ozen](../../strongs/h/h241.md).

<a name="psalms_18_7"></a>Psalms 18:7

Then the ['erets](../../strongs/h/h776.md) [gāʿaš](../../strongs/h/h1607.md) and [rāʿaš](../../strongs/h/h7493.md); the [mowcadah](../../strongs/h/h4146.md) also of the [har](../../strongs/h/h2022.md) [ragaz](../../strongs/h/h7264.md) and were [gāʿaš](../../strongs/h/h1607.md), because he was [ḥārâ](../../strongs/h/h2734.md).

<a name="psalms_18_8"></a>Psalms 18:8

There [ʿālâ](../../strongs/h/h5927.md) an ['ashan](../../strongs/h/h6227.md) out of his ['aph](../../strongs/h/h639.md), and ['esh](../../strongs/h/h784.md) out of his [peh](../../strongs/h/h6310.md) ['akal](../../strongs/h/h398.md): [gechel](../../strongs/h/h1513.md) were kindled by it.

<a name="psalms_18_9"></a>Psalms 18:9

He [natah](../../strongs/h/h5186.md) the [shamayim](../../strongs/h/h8064.md) also, and [yarad](../../strongs/h/h3381.md): and ['araphel](../../strongs/h/h6205.md) was under his [regel](../../strongs/h/h7272.md).

<a name="psalms_18_10"></a>Psalms 18:10

And he [rāḵaḇ](../../strongs/h/h7392.md) upon a [kĕruwb](../../strongs/h/h3742.md), and did ['uwph](../../strongs/h/h5774.md): yea, he did [da'ah](../../strongs/h/h1675.md) upon the [kanaph](../../strongs/h/h3671.md) of the [ruwach](../../strongs/h/h7307.md).

<a name="psalms_18_11"></a>Psalms 18:11

He [shiyth](../../strongs/h/h7896.md) [choshek](../../strongs/h/h2822.md) his [cether](../../strongs/h/h5643.md); his [cukkah](../../strongs/h/h5521.md) [cabiyb](../../strongs/h/h5439.md) him were [cheshkah](../../strongs/h/h2824.md) [mayim](../../strongs/h/h4325.md) and thick ['ab](../../strongs/h/h5645.md) of the [shachaq](../../strongs/h/h7834.md).

<a name="psalms_18_12"></a>Psalms 18:12

At the [nogahh](../../strongs/h/h5051.md) before him his ['ab](../../strongs/h/h5645.md) ['abar](../../strongs/h/h5674.md), [barad](../../strongs/h/h1259.md) and [gechel](../../strongs/h/h1513.md).

<a name="psalms_18_13"></a>Psalms 18:13

[Yĕhovah](../../strongs/h/h3068.md) also [ra'am](../../strongs/h/h7481.md) in the [shamayim](../../strongs/h/h8064.md), and the ['elyown](../../strongs/h/h5945.md) [nathan](../../strongs/h/h5414.md) his [qowl](../../strongs/h/h6963.md); [barad](../../strongs/h/h1259.md) and [gechel](../../strongs/h/h1513.md) of ['esh](../../strongs/h/h784.md).

<a name="psalms_18_14"></a>Psalms 18:14

Yea, he [shalach](../../strongs/h/h7971.md) his [chets](../../strongs/h/h2671.md), and [puwts](../../strongs/h/h6327.md) them; and he [rabab](../../strongs/h/h7232.md) [baraq](../../strongs/h/h1300.md), and [hāmam](../../strongs/h/h2000.md) them.

<a name="psalms_18_15"></a>Psalms 18:15

Then the ['āp̄îq](../../strongs/h/h650.md) of [mayim](../../strongs/h/h4325.md) were [ra'ah](../../strongs/h/h7200.md), and the [mowcadah](../../strongs/h/h4146.md) of the [tebel](../../strongs/h/h8398.md) were [gālâ](../../strongs/h/h1540.md) at thy [ge'arah](../../strongs/h/h1606.md), [Yĕhovah](../../strongs/h/h3068.md), at the [neshamah](../../strongs/h/h5397.md) of the [ruwach](../../strongs/h/h7307.md) of thy ['aph](../../strongs/h/h639.md).

<a name="psalms_18_16"></a>Psalms 18:16

He [shalach](../../strongs/h/h7971.md) from above, he [laqach](../../strongs/h/h3947.md) me, he [mashah](../../strongs/h/h4871.md) me out of many [mayim](../../strongs/h/h4325.md).

<a name="psalms_18_17"></a>Psalms 18:17

He [natsal](../../strongs/h/h5337.md) me from my ['az](../../strongs/h/h5794.md) ['oyeb](../../strongs/h/h341.md), and from them which [sane'](../../strongs/h/h8130.md) me: for they were too ['amats](../../strongs/h/h553.md) for me.

<a name="psalms_18_18"></a>Psalms 18:18

They [qadam](../../strongs/h/h6923.md) me in the [yowm](../../strongs/h/h3117.md) of my ['êḏ](../../strongs/h/h343.md): but [Yĕhovah](../../strongs/h/h3068.md) was my [mašʿēn](../../strongs/h/h4937.md).

<a name="psalms_18_19"></a>Psalms 18:19

He [yāṣā'](../../strongs/h/h3318.md) me also into a [merḥāḇ](../../strongs/h/h4800.md); he [chalats](../../strongs/h/h2502.md) me, because he [ḥāp̄ēṣ](../../strongs/h/h2654.md) in me.

<a name="psalms_18_20"></a>Psalms 18:20

[Yĕhovah](../../strongs/h/h3068.md) [gamal](../../strongs/h/h1580.md) me according to my [tsedeq](../../strongs/h/h6664.md); according to the [bōr](../../strongs/h/h1252.md) of my [yad](../../strongs/h/h3027.md) hath he [shuwb](../../strongs/h/h7725.md) me.

<a name="psalms_18_21"></a>Psalms 18:21

For I have [shamar](../../strongs/h/h8104.md) the [derek](../../strongs/h/h1870.md) of [Yĕhovah](../../strongs/h/h3068.md), and have not [rāšaʿ](../../strongs/h/h7561.md) from my ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_18_22"></a>Psalms 18:22

For all his [mishpat](../../strongs/h/h4941.md) were before me, and I did not [cuwr](../../strongs/h/h5493.md) his [chuqqah](../../strongs/h/h2708.md) from me.

<a name="psalms_18_23"></a>Psalms 18:23

I was also [tamiym](../../strongs/h/h8549.md) before him, and I [shamar](../../strongs/h/h8104.md) myself from mine ['avon](../../strongs/h/h5771.md).

<a name="psalms_18_24"></a>Psalms 18:24

Therefore hath [Yĕhovah](../../strongs/h/h3068.md) [shuwb](../../strongs/h/h7725.md) me according to my [tsedeq](../../strongs/h/h6664.md), according to the [bōr](../../strongs/h/h1252.md) of my [yad](../../strongs/h/h3027.md) in his ['ayin](../../strongs/h/h5869.md).

<a name="psalms_18_25"></a>Psalms 18:25

With the [chaciyd](../../strongs/h/h2623.md) thou wilt [ḥāsaḏ](../../strongs/h/h2616.md); with a [tamiym](../../strongs/h/h8549.md) [gᵊḇar](../../strongs/h/h1399.md) thou wilt [tamam](../../strongs/h/h8552.md);

<a name="psalms_18_26"></a>Psalms 18:26

With the [bārar](../../strongs/h/h1305.md) thou wilt shew thyself [bārar](../../strongs/h/h1305.md); and with the [ʿiqqēš](../../strongs/h/h6141.md) thou wilt shew thyself [pāṯal](../../strongs/h/h6617.md).

<a name="psalms_18_27"></a>Psalms 18:27

For thou wilt [yasha'](../../strongs/h/h3467.md) the ['aniy](../../strongs/h/h6041.md) ['am](../../strongs/h/h5971.md); but wilt [šāp̄ēl](../../strongs/h/h8213.md) [ruwm](../../strongs/h/h7311.md) ['ayin](../../strongs/h/h5869.md).

<a name="psalms_18_28"></a>Psalms 18:28

For thou wilt ['owr](../../strongs/h/h215.md) my [nîr](../../strongs/h/h5216.md): [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md) will [nāḡahh](../../strongs/h/h5050.md) my [choshek](../../strongs/h/h2822.md).

<a name="psalms_18_29"></a>Psalms 18:29

For by thee I have [rûṣ](../../strongs/h/h7323.md) through a [gᵊḏûḏ](../../strongs/h/h1416.md); and by my ['Elohiym](../../strongs/h/h430.md) have I [dālaḡ](../../strongs/h/h1801.md) over a [šûrâ](../../strongs/h/h7791.md).

<a name="psalms_18_30"></a>Psalms 18:30

['el](../../strongs/h/h410.md), his way [tamiym](../../strongs/h/h8549.md): the ['imrah](../../strongs/h/h565.md) of [Yĕhovah](../../strongs/h/h3068.md) is [tsaraph](../../strongs/h/h6884.md): he a [magen](../../strongs/h/h4043.md) to all those that [chacah](../../strongs/h/h2620.md) in him.

<a name="psalms_18_31"></a>Psalms 18:31

For who ['ĕlvôha](../../strongs/h/h433.md) [bilʿăḏê](../../strongs/h/h1107.md) [Yĕhovah](../../strongs/h/h3068.md)? or who a [tsuwr](../../strongs/h/h6697.md) [zûlâ](../../strongs/h/h2108.md) our ['Elohiym](../../strongs/h/h430.md)?

<a name="psalms_18_32"></a>Psalms 18:32

['el](../../strongs/h/h410.md) that ['āzar](../../strongs/h/h247.md) me with [ḥayil](../../strongs/h/h2428.md), and [nathan](../../strongs/h/h5414.md) my [derek](../../strongs/h/h1870.md) [tamiym](../../strongs/h/h8549.md).

<a name="psalms_18_33"></a>Psalms 18:33

He [šāvâ](../../strongs/h/h7737.md) my [regel](../../strongs/h/h7272.md) like ['ayyālâ](../../strongs/h/h355.md), and ['amad](../../strongs/h/h5975.md) me upon my [bāmâ](../../strongs/h/h1116.md).

<a name="psalms_18_34"></a>Psalms 18:34

He [lamad](../../strongs/h/h3925.md) my [yad](../../strongs/h/h3027.md) to [milḥāmâ](../../strongs/h/h4421.md), so that a [qesheth](../../strongs/h/h7198.md) of [nᵊḥûšâ](../../strongs/h/h5154.md) is [nāḥaṯ](../../strongs/h/h5181.md) by mine [zerowa'](../../strongs/h/h2220.md).

<a name="psalms_18_35"></a>Psalms 18:35

Thou hast also [nathan](../../strongs/h/h5414.md) me the [magen](../../strongs/h/h4043.md) of thy [yesha'](../../strongs/h/h3468.md): and thy [yamiyn](../../strongs/h/h3225.md) hath [sāʿaḏ](../../strongs/h/h5582.md) me, and thy [ʿanvâ](../../strongs/h/h6037.md) hath made me [rabah](../../strongs/h/h7235.md).

<a name="psalms_18_36"></a>Psalms 18:36

Thou hast [rāḥaḇ](../../strongs/h/h7337.md) my [ṣaʿaḏ](../../strongs/h/h6806.md) under me, that my [qarsōl](../../strongs/h/h7166.md) did not [māʿaḏ](../../strongs/h/h4571.md).

<a name="psalms_18_37"></a>Psalms 18:37

I have [radaph](../../strongs/h/h7291.md) mine ['oyeb](../../strongs/h/h341.md), and overtaken them: neither did I [shuwb](../../strongs/h/h7725.md) till they were [kalah](../../strongs/h/h3615.md).

<a name="psalms_18_38"></a>Psalms 18:38

I have [māḥaṣ](../../strongs/h/h4272.md) them that they were not able to [quwm](../../strongs/h/h6965.md): they are [naphal](../../strongs/h/h5307.md) under my [regel](../../strongs/h/h7272.md).

<a name="psalms_18_39"></a>Psalms 18:39

For thou hast ['āzar](../../strongs/h/h247.md) me with [ḥayil](../../strongs/h/h2428.md) unto the [milḥāmâ](../../strongs/h/h4421.md): thou hast [kara'](../../strongs/h/h3766.md) under me those that [quwm](../../strongs/h/h6965.md) against me.

<a name="psalms_18_40"></a>Psalms 18:40

Thou hast also [nathan](../../strongs/h/h5414.md) me the [ʿōrep̄](../../strongs/h/h6203.md) of mine ['oyeb](../../strongs/h/h341.md); that I might [ṣāmaṯ](../../strongs/h/h6789.md) them that [sane'](../../strongs/h/h8130.md) me.

<a name="psalms_18_41"></a>Psalms 18:41

They [šāvaʿ](../../strongs/h/h7768.md), but there was none to [yasha'](../../strongs/h/h3467.md) them: even unto [Yĕhovah](../../strongs/h/h3068.md), but he ['anah](../../strongs/h/h6030.md) them not.

<a name="psalms_18_42"></a>Psalms 18:42

Then did I [šāḥaq](../../strongs/h/h7833.md) them small as the ['aphar](../../strongs/h/h6083.md) before the [ruwach](../../strongs/h/h7307.md): I did [rîq](../../strongs/h/h7324.md) as the [ṭîṭ](../../strongs/h/h2916.md) in the [ḥûṣ](../../strongs/h/h2351.md).

<a name="psalms_18_43"></a>Psalms 18:43

Thou hast [palat](../../strongs/h/h6403.md) me from the [rîḇ](../../strongs/h/h7379.md) of the ['am](../../strongs/h/h5971.md); and thou hast made me the [ro'sh](../../strongs/h/h7218.md) of the [gowy](../../strongs/h/h1471.md): an ['am](../../strongs/h/h5971.md) whom I have not [yada'](../../strongs/h/h3045.md) shall ['abad](../../strongs/h/h5647.md) me.

<a name="psalms_18_44"></a>Psalms 18:44

As soon as they [šēmaʿ](../../strongs/h/h8088.md) of me, they shall [shama'](../../strongs/h/h8085.md) me: the [ben](../../strongs/h/h1121.md) shall [kāḥaš](../../strongs/h/h3584.md) themselves unto me.

<a name="psalms_18_45"></a>Psalms 18:45

The [ben](../../strongs/h/h1121.md) shall [nabel](../../strongs/h/h5034.md), and be [ḥāraḡ](../../strongs/h/h2727.md) out of their [misgereṯ](../../strongs/h/h4526.md).

<a name="psalms_18_46"></a>Psalms 18:46

[Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md); and [barak](../../strongs/h/h1288.md) be my [tsuwr](../../strongs/h/h6697.md); and let the ['Elohiym](../../strongs/h/h430.md) of my [yesha'](../../strongs/h/h3468.md) be [ruwm](../../strongs/h/h7311.md).

<a name="psalms_18_47"></a>Psalms 18:47

It is ['el](../../strongs/h/h410.md) that [nathan](../../strongs/h/h5414.md) me, and [dabar](../../strongs/h/h1696.md) the ['am](../../strongs/h/h5971.md) under me.

<a name="psalms_18_48"></a>Psalms 18:48

He [palat](../../strongs/h/h6403.md) me from mine ['oyeb](../../strongs/h/h341.md): yea, thou [ruwm](../../strongs/h/h7311.md) me up above those that [quwm](../../strongs/h/h6965.md) against me: thou hast [natsal](../../strongs/h/h5337.md) me from the [chamac](../../strongs/h/h2555.md) man.

<a name="psalms_18_49"></a>Psalms 18:49

Therefore will I [yadah](../../strongs/h/h3034.md) unto thee, [Yĕhovah](../../strongs/h/h3068.md), among the [gowy](../../strongs/h/h1471.md), and [zamar](../../strongs/h/h2167.md) unto thy [shem](../../strongs/h/h8034.md).

<a name="psalms_18_50"></a>Psalms 18:50

[gāḏal](../../strongs/h/h1431.md) [yĕshuw'ah](../../strongs/h/h3444.md) giveth he to his [melek](../../strongs/h/h4428.md); and sheweth [checed](../../strongs/h/h2617.md) to his [mashiyach](../../strongs/h/h4899.md), to [Dāviḏ](../../strongs/h/h1732.md), and to his [zera'](../../strongs/h/h2233.md) for ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 17](psalms_17.md) - [Psalms 19](psalms_19.md)