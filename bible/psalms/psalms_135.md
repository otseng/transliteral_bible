# [Psalms 135](https://www.blueletterbible.org/kjv/psalms/135)

<a name="psalms_135_1"></a>Psalms 135:1

[halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md). [halal](../../strongs/h/h1984.md) ye the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md); [halal](../../strongs/h/h1984.md) him, O ye ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_135_2"></a>Psalms 135:2

Ye that ['amad](../../strongs/h/h5975.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), in the [ḥāṣēr](../../strongs/h/h2691.md) of the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md),

<a name="psalms_135_3"></a>Psalms 135:3

[halal](../../strongs/h/h1984.md) the [Yahh](../../strongs/h/h3050.md); for [Yĕhovah](../../strongs/h/h3068.md) is [towb](../../strongs/h/h2896.md): [zamar](../../strongs/h/h2167.md) unto his [shem](../../strongs/h/h8034.md); for it is [na'iym](../../strongs/h/h5273.md).

<a name="psalms_135_4"></a>Psalms 135:4

For the [Yahh](../../strongs/h/h3050.md) hath [bāḥar](../../strongs/h/h977.md) [Ya'aqob](../../strongs/h/h3290.md) unto himself, and [Yisra'el](../../strongs/h/h3478.md) for his [sᵊḡullâ](../../strongs/h/h5459.md).

<a name="psalms_135_5"></a>Psalms 135:5

For I [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) is [gadowl](../../strongs/h/h1419.md), and that our ['adown](../../strongs/h/h113.md) is above all ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_135_6"></a>Psalms 135:6

Whatsoever [Yĕhovah](../../strongs/h/h3068.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md), that ['asah](../../strongs/h/h6213.md) he in [shamayim](../../strongs/h/h8064.md), and in ['erets](../../strongs/h/h776.md), in the [yam](../../strongs/h/h3220.md), and all [tĕhowm](../../strongs/h/h8415.md).

<a name="psalms_135_7"></a>Psalms 135:7

He causeth the [nāśî'](../../strongs/h/h5387.md) to [ʿālâ](../../strongs/h/h5927.md) from the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md); he ['asah](../../strongs/h/h6213.md) [baraq](../../strongs/h/h1300.md) for the [māṭār](../../strongs/h/h4306.md); he [yāṣā'](../../strongs/h/h3318.md) the [ruwach](../../strongs/h/h7307.md) out of his ['ôṣār](../../strongs/h/h214.md).

<a name="psalms_135_8"></a>Psalms 135:8

Who [nakah](../../strongs/h/h5221.md) the [bᵊḵôr](../../strongs/h/h1060.md) of [Mitsrayim](../../strongs/h/h4714.md), both of ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md).

<a name="psalms_135_9"></a>Psalms 135:9

Who [shalach](../../strongs/h/h7971.md) ['ôṯ](../../strongs/h/h226.md) and [môp̄ēṯ](../../strongs/h/h4159.md) into the [tavek](../../strongs/h/h8432.md) of thee, [Mitsrayim](../../strongs/h/h4714.md), upon [Parʿô](../../strongs/h/h6547.md), and upon all his ['ebed](../../strongs/h/h5650.md).

<a name="psalms_135_10"></a>Psalms 135:10

Who [nakah](../../strongs/h/h5221.md) [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md), and [harag](../../strongs/h/h2026.md) ['atsuwm](../../strongs/h/h6099.md) [melek](../../strongs/h/h4428.md);

<a name="psalms_135_11"></a>Psalms 135:11

[Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), and [ʿÔḡ](../../strongs/h/h5747.md) [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md), and all the [mamlāḵâ](../../strongs/h/h4467.md) of [kĕna'an](../../strongs/h/h3667.md):

<a name="psalms_135_12"></a>Psalms 135:12

And [nathan](../../strongs/h/h5414.md) their ['erets](../../strongs/h/h776.md) for a [nachalah](../../strongs/h/h5159.md), a [nachalah](../../strongs/h/h5159.md) unto [Yisra'el](../../strongs/h/h3478.md) his ['am](../../strongs/h/h5971.md).

<a name="psalms_135_13"></a>Psalms 135:13

Thy [shem](../../strongs/h/h8034.md), [Yĕhovah](../../strongs/h/h3068.md), ['owlam](../../strongs/h/h5769.md); and thy [zeker](../../strongs/h/h2143.md), [Yĕhovah](../../strongs/h/h3068.md), throughout [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md).

<a name="psalms_135_14"></a>Psalms 135:14

For [Yĕhovah](../../strongs/h/h3068.md) will [diyn](../../strongs/h/h1777.md) his ['am](../../strongs/h/h5971.md), and he will [nacham](../../strongs/h/h5162.md) himself concerning his ['ebed](../../strongs/h/h5650.md).

<a name="psalms_135_15"></a>Psalms 135:15

The [ʿāṣāḇ](../../strongs/h/h6091.md) of the [gowy](../../strongs/h/h1471.md) are [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md), the [ma'aseh](../../strongs/h/h4639.md) of ['āḏām](../../strongs/h/h120.md) [yad](../../strongs/h/h3027.md).

<a name="psalms_135_16"></a>Psalms 135:16

They have [peh](../../strongs/h/h6310.md), but they [dabar](../../strongs/h/h1696.md) not; ['ayin](../../strongs/h/h5869.md) have they, but they [ra'ah](../../strongs/h/h7200.md) not;

<a name="psalms_135_17"></a>Psalms 135:17

They have ['ozen](../../strongs/h/h241.md), but they ['azan](../../strongs/h/h238.md) not; neither is [yēš](../../strongs/h/h3426.md) any [ruwach](../../strongs/h/h7307.md) in their [peh](../../strongs/h/h6310.md).

<a name="psalms_135_18"></a>Psalms 135:18

They that ['asah](../../strongs/h/h6213.md) them are like unto them: so is every one that [batach](../../strongs/h/h982.md) in them.

<a name="psalms_135_19"></a>Psalms 135:19

[barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md), [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md): [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md), [bayith](../../strongs/h/h1004.md) of ['Ahărôn](../../strongs/h/h175.md):

<a name="psalms_135_20"></a>Psalms 135:20

[barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md), [bayith](../../strongs/h/h1004.md) of [Lēvî](../../strongs/h/h3878.md): ye that [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md), [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_135_21"></a>Psalms 135:21

[barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) out of [Tsiyown](../../strongs/h/h6726.md), which [shakan](../../strongs/h/h7931.md) at [Yĕruwshalaim](../../strongs/h/h3389.md). [halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 134](psalms_134.md) - [Psalms 136](psalms_136.md)