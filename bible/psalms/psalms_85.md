# [Psalms 85](https://www.blueletterbible.org/kjv/psalms/85)

<a name="psalms_85_1"></a>Psalms 85:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) for the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md).  [Yĕhovah](../../strongs/h/h3068.md), thou hast been [ratsah](../../strongs/h/h7521.md) unto thy ['erets](../../strongs/h/h776.md): thou hast [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) [shebuwth](../../strongs/h/h7622.md) of [Ya'aqob](../../strongs/h/h3290.md).

<a name="psalms_85_2"></a>Psalms 85:2

Thou hast [nasa'](../../strongs/h/h5375.md) the ['avon](../../strongs/h/h5771.md) of thy ['am](../../strongs/h/h5971.md), thou hast [kāsâ](../../strongs/h/h3680.md) all their [chatta'ath](../../strongs/h/h2403.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_85_3"></a>Psalms 85:3

Thou hast ['āsap̄](../../strongs/h/h622.md) all thy ['ebrah](../../strongs/h/h5678.md): thou hast [shuwb](../../strongs/h/h7725.md) thyself from the [charown](../../strongs/h/h2740.md) of thine ['aph](../../strongs/h/h639.md).

<a name="psalms_85_4"></a>Psalms 85:4

[shuwb](../../strongs/h/h7725.md) us, ['Elohiym](../../strongs/h/h430.md) of our [yesha'](../../strongs/h/h3468.md), and cause thine [ka'ac](../../strongs/h/h3708.md) toward us to [pārar](../../strongs/h/h6565.md).

<a name="psalms_85_5"></a>Psalms 85:5

Wilt thou be ['anaph](../../strongs/h/h599.md) with us for ['owlam](../../strongs/h/h5769.md) ? wilt thou [mashak](../../strongs/h/h4900.md) thine ['aph](../../strongs/h/h639.md) to [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md) ?

<a name="psalms_85_6"></a>Psalms 85:6

Wilt thou not [ḥāyâ](../../strongs/h/h2421.md) us [shuwb](../../strongs/h/h7725.md): that thy ['am](../../strongs/h/h5971.md) may [samach](../../strongs/h/h8055.md) in thee?

<a name="psalms_85_7"></a>Psalms 85:7

[ra'ah](../../strongs/h/h7200.md) us thy [checed](../../strongs/h/h2617.md), [Yĕhovah](../../strongs/h/h3068.md), and [nathan](../../strongs/h/h5414.md) us thy [yesha'](../../strongs/h/h3468.md).

<a name="psalms_85_8"></a>Psalms 85:8

I will [shama'](../../strongs/h/h8085.md) what ['el](../../strongs/h/h410.md) [Yĕhovah](../../strongs/h/h3068.md) will [dabar](../../strongs/h/h1696.md): for he will [dabar](../../strongs/h/h1696.md) [shalowm](../../strongs/h/h7965.md) unto his ['am](../../strongs/h/h5971.md), and to his [chaciyd](../../strongs/h/h2623.md): but let them not [shuwb](../../strongs/h/h7725.md) to [kislâ](../../strongs/h/h3690.md).

<a name="psalms_85_9"></a>Psalms 85:9

Surely his [yesha'](../../strongs/h/h3468.md) is [qarowb](../../strongs/h/h7138.md) them that [yārē'](../../strongs/h/h3373.md) him; that [kabowd](../../strongs/h/h3519.md) may [shakan](../../strongs/h/h7931.md) in our ['erets](../../strongs/h/h776.md).

<a name="psalms_85_10"></a>Psalms 85:10

[checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md) are [pāḡaš](../../strongs/h/h6298.md); [tsedeq](../../strongs/h/h6664.md) and [shalowm](../../strongs/h/h7965.md) have [nashaq](../../strongs/h/h5401.md) each other.

<a name="psalms_85_11"></a>Psalms 85:11

['emeth](../../strongs/h/h571.md) shall [ṣāmaḥ](../../strongs/h/h6779.md) of the ['erets](../../strongs/h/h776.md); and [tsedeq](../../strongs/h/h6664.md) shall [šāqap̄](../../strongs/h/h8259.md) from [shamayim](../../strongs/h/h8064.md).

<a name="psalms_85_12"></a>Psalms 85:12

Yea, [Yĕhovah](../../strongs/h/h3068.md) shall [nathan](../../strongs/h/h5414.md) that which is [towb](../../strongs/h/h2896.md); and our ['erets](../../strongs/h/h776.md) shall [nathan](../../strongs/h/h5414.md) her [yᵊḇûl](../../strongs/h/h2981.md).

<a name="psalms_85_13"></a>Psalms 85:13

[tsedeq](../../strongs/h/h6664.md) shall [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) him; and shall [śûm](../../strongs/h/h7760.md) us in the [derek](../../strongs/h/h1870.md) of his [pa'am](../../strongs/h/h6471.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 84](psalms_84.md) - [Psalms 86](psalms_86.md)