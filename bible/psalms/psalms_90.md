# [Psalms 90](https://www.blueletterbible.org/kjv/psalms/90)

<a name="psalms_90_1"></a>Psalms 90:1

A [tĕphillah](../../strongs/h/h8605.md) of [Mōshe](../../strongs/h/h4872.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md). ['adonay](../../strongs/h/h136.md), thou hast been our [māʿôn](../../strongs/h/h4583.md) in [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md).

<a name="psalms_90_2"></a>Psalms 90:2

Before the [har](../../strongs/h/h2022.md) were [yalad](../../strongs/h/h3205.md), or ever thou hadst [chuwl](../../strongs/h/h2342.md) the ['erets](../../strongs/h/h776.md) and the [tebel](../../strongs/h/h8398.md), even from ['owlam](../../strongs/h/h5769.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md), thou art ['el](../../strongs/h/h410.md).

<a name="psalms_90_3"></a>Psalms 90:3

Thou [shuwb](../../strongs/h/h7725.md) ['enowsh](../../strongs/h/h582.md) to [dakā'](../../strongs/h/h1793.md); and ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md), ye [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md).

<a name="psalms_90_4"></a>Psalms 90:4

For an ['elep̄](../../strongs/h/h505.md) [šānâ](../../strongs/h/h8141.md) in thy ['ayin](../../strongs/h/h5869.md) are but [yowm](../../strongs/h/h3117.md) ['eṯmôl](../../strongs/h/h865.md) when it is ['abar](../../strongs/h/h5674.md), and as an ['ašmurâ](../../strongs/h/h821.md) in the [layil](../../strongs/h/h3915.md).

<a name="psalms_90_5"></a>Psalms 90:5

Thou carriest them away as with a [zāram](../../strongs/h/h2229.md); they are as a [šēnā'](../../strongs/h/h8142.md): in the [boqer](../../strongs/h/h1242.md) they are like [chatsiyr](../../strongs/h/h2682.md) which [ḥālap̄](../../strongs/h/h2498.md).

<a name="psalms_90_6"></a>Psalms 90:6

In the [boqer](../../strongs/h/h1242.md) it [tsuwts](../../strongs/h/h6692.md), and [ḥālap̄](../../strongs/h/h2498.md); in the ['ereb](../../strongs/h/h6153.md) it is [muwl](../../strongs/h/h4135.md), and [yāḇēš](../../strongs/h/h3001.md).

<a name="psalms_90_7"></a>Psalms 90:7

For we are [kalah](../../strongs/h/h3615.md) by thine ['aph](../../strongs/h/h639.md), and by thy [chemah](../../strongs/h/h2534.md) are we [bahal](../../strongs/h/h926.md).

<a name="psalms_90_8"></a>Psalms 90:8

Thou hast [shiyth](../../strongs/h/h7896.md) our ['avon](../../strongs/h/h5771.md) before thee, our ['alam](../../strongs/h/h5956.md) sins in the [ma'owr](../../strongs/h/h3974.md) of thy [paniym](../../strongs/h/h6440.md).

<a name="psalms_90_9"></a>Psalms 90:9

For all our [yowm](../../strongs/h/h3117.md) are [panah](../../strongs/h/h6437.md) in thy ['ebrah](../../strongs/h/h5678.md): we [kalah](../../strongs/h/h3615.md) our [šānâ](../../strongs/h/h8141.md) as a [heḡê](../../strongs/h/h1899.md) that is told.

<a name="psalms_90_10"></a>Psalms 90:10

The [yowm](../../strongs/h/h3117.md) of our [šānâ](../../strongs/h/h8141.md) are [šiḇʿîm](../../strongs/h/h7657.md) [šānâ](../../strongs/h/h8141.md) and [šiḇʿîm](../../strongs/h/h7657.md); and if by [gᵊḇûrâ](../../strongs/h/h1369.md) they be [šᵊmōnîm](../../strongs/h/h8084.md) [šānâ](../../strongs/h/h8141.md), yet is their [rōhaḇ](../../strongs/h/h7296.md) ['amal](../../strongs/h/h5999.md) and ['aven](../../strongs/h/h205.md); for it is [ḥîš](../../strongs/h/h2440.md) cut [gûz](../../strongs/h/h1468.md), and we ['uwph](../../strongs/h/h5774.md).

<a name="psalms_90_11"></a>Psalms 90:11

Who [yada'](../../strongs/h/h3045.md) the ['oz](../../strongs/h/h5797.md) of thine ['aph](../../strongs/h/h639.md)? even according to thy [yir'ah](../../strongs/h/h3374.md), so is thy ['ebrah](../../strongs/h/h5678.md).

<a name="psalms_90_12"></a>Psalms 90:12

So [yada'](../../strongs/h/h3045.md) us to [mānâ](../../strongs/h/h4487.md) our [yowm](../../strongs/h/h3117.md), that we may [bow'](../../strongs/h/h935.md) our [lebab](../../strongs/h/h3824.md) unto [ḥāḵmâ](../../strongs/h/h2451.md).

<a name="psalms_90_13"></a>Psalms 90:13

[shuwb](../../strongs/h/h7725.md), [Yĕhovah](../../strongs/h/h3068.md), how long? and let it [nacham](../../strongs/h/h5162.md) thee concerning thy ['ebed](../../strongs/h/h5650.md).

<a name="psalms_90_14"></a>Psalms 90:14

[sāׂbaʿ](../../strongs/h/h7646.md) us [boqer](../../strongs/h/h1242.md) with thy [checed](../../strongs/h/h2617.md); that we may [ranan](../../strongs/h/h7442.md) and be [samach](../../strongs/h/h8055.md) all our [yowm](../../strongs/h/h3117.md).

<a name="psalms_90_15"></a>Psalms 90:15

Make us [samach](../../strongs/h/h8055.md) according to the [yowm](../../strongs/h/h3117.md) wherein thou hast [ʿānâ](../../strongs/h/h6031.md) us, and the [šānâ](../../strongs/h/h8141.md) wherein we have [ra'ah](../../strongs/h/h7200.md) [ra'](../../strongs/h/h7451.md).

<a name="psalms_90_16"></a>Psalms 90:16

Let thy [pōʿal](../../strongs/h/h6467.md) [ra'ah](../../strongs/h/h7200.md) unto thy ['ebed](../../strongs/h/h5650.md), and thy [hadar](../../strongs/h/h1926.md) unto their [ben](../../strongs/h/h1121.md).

<a name="psalms_90_17"></a>Psalms 90:17

And let the [nōʿam](../../strongs/h/h5278.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) be upon us: and [kuwn](../../strongs/h/h3559.md) thou the [ma'aseh](../../strongs/h/h4639.md) of our [yad](../../strongs/h/h3027.md) upon us; yea, the [ma'aseh](../../strongs/h/h4639.md) of our [yad](../../strongs/h/h3027.md) [kuwn](../../strongs/h/h3559.md) thou it.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 89](psalms_89.md) - [Psalms 91](psalms_91.md)