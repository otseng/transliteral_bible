# [Psalms 61](https://www.blueletterbible.org/kjv/psa/61)

<a name="psalms_61_1"></a>Psalms 61:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon [Nᵊḡînâ](../../strongs/h/h5058.md), [A Psalm] of [Dāviḏ](../../strongs/h/h1732.md). [shama'](../../strongs/h/h8085.md) my [rinnah](../../strongs/h/h7440.md), ['Elohiym](../../strongs/h/h430.md); [qashab](../../strongs/h/h7181.md) unto my [tĕphillah](../../strongs/h/h8605.md).

<a name="psalms_61_2"></a>Psalms 61:2

From the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md) will I [qara'](../../strongs/h/h7121.md) unto thee, when my [leb](../../strongs/h/h3820.md) is [ʿāṭap̄](../../strongs/h/h5848.md): [nachah](../../strongs/h/h5148.md) me to the [tsuwr](../../strongs/h/h6697.md) that is [ruwm](../../strongs/h/h7311.md) than I.

<a name="psalms_61_3"></a>Psalms 61:3

For thou hast been a [machaceh](../../strongs/h/h4268.md) for me, and a ['oz](../../strongs/h/h5797.md) [miḡdāl](../../strongs/h/h4026.md) [paniym](../../strongs/h/h6440.md) the ['oyeb](../../strongs/h/h341.md).

<a name="psalms_61_4"></a>Psalms 61:4

I will [guwr](../../strongs/h/h1481.md) in thy ['ohel](../../strongs/h/h168.md) ['owlam](../../strongs/h/h5769.md): I will [chacah](../../strongs/h/h2620.md) in the [cether](../../strongs/h/h5643.md) of thy [kanaph](../../strongs/h/h3671.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_61_5"></a>Psalms 61:5

For thou, ['Elohiym](../../strongs/h/h430.md), hast [shama'](../../strongs/h/h8085.md) my [neḏer](../../strongs/h/h5088.md): thou hast [nathan](../../strongs/h/h5414.md) me the [yᵊruššâ](../../strongs/h/h3425.md) of those that [yārē'](../../strongs/h/h3373.md) thy [shem](../../strongs/h/h8034.md).

<a name="psalms_61_6"></a>Psalms 61:6

Thou wilt [yāsap̄](../../strongs/h/h3254.md) the [melek](../../strongs/h/h4428.md) [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md): and his [šānâ](../../strongs/h/h8141.md) as [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md).

<a name="psalms_61_7"></a>Psalms 61:7

He shall [yashab](../../strongs/h/h3427.md) [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md) ['owlam](../../strongs/h/h5769.md): O [mānâ](../../strongs/h/h4487.md) [checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md), which may [nāṣar](../../strongs/h/h5341.md) him.

<a name="psalms_61_8"></a>Psalms 61:8

So will I [zamar](../../strongs/h/h2167.md) praise unto thy [shem](../../strongs/h/h8034.md) for [ʿaḏ](../../strongs/h/h5703.md), that I may [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) [shalam](../../strongs/h/h7999.md) my [neḏer](../../strongs/h/h5088.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 60](psalms_60.md) - [Psalms 62](psalms_62.md)