# [Psalms 42](https://www.blueletterbible.org/kjv/psa/42)

<a name="psalms_42_1"></a>Psalms 42:1

To the [nāṣaḥ](../../strongs/h/h5329.md), [Maśkîl](../../strongs/h/h4905.md), for the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md). As the ['ayyāl](../../strongs/h/h354.md) [ʿāraḡ](../../strongs/h/h6165.md) after the [mayim](../../strongs/h/h4325.md) ['āp̄îq](../../strongs/h/h650.md), so [ʿāraḡ](../../strongs/h/h6165.md) my [nephesh](../../strongs/h/h5315.md) after thee, ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_42_2"></a>Psalms 42:2

My [nephesh](../../strongs/h/h5315.md) [ṣāmē'](../../strongs/h/h6770.md) for ['Elohiym](../../strongs/h/h430.md), for the [chay](../../strongs/h/h2416.md) ['el](../../strongs/h/h410.md): when shall I [bow'](../../strongs/h/h935.md) and [ra'ah](../../strongs/h/h7200.md) [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md)?

<a name="psalms_42_3"></a>Psalms 42:3

My [dim'ah](../../strongs/h/h1832.md) have been my [lechem](../../strongs/h/h3899.md) [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md), while they [yowm](../../strongs/h/h3117.md) ['āmar](../../strongs/h/h559.md) unto me, Where is thy ['Elohiym](../../strongs/h/h430.md)?

<a name="psalms_42_4"></a>Psalms 42:4

When I [zakar](../../strongs/h/h2142.md) these things, I [šāp̄aḵ](../../strongs/h/h8210.md) my [nephesh](../../strongs/h/h5315.md) in me: for I had ['abar](../../strongs/h/h5674.md) with the [sāḵ](../../strongs/h/h5519.md), I [dāḏâ](../../strongs/h/h1718.md) with them to the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), with the [qowl](../../strongs/h/h6963.md) of [rinnah](../../strongs/h/h7440.md) and [tôḏâ](../../strongs/h/h8426.md), with a [hāmôn](../../strongs/h/h1995.md) that [ḥāḡaḡ](../../strongs/h/h2287.md).

<a name="psalms_42_5"></a>Psalms 42:5

Why art thou [shachach](../../strongs/h/h7817.md), O my [nephesh](../../strongs/h/h5315.md)? and why art thou [hāmâ](../../strongs/h/h1993.md) in me?  [yāḥal](../../strongs/h/h3176.md) thou in ['Elohiym](../../strongs/h/h430.md): for I shall yet [yadah](../../strongs/h/h3034.md) him for the [yĕshuw'ah](../../strongs/h/h3444.md) of his [paniym](../../strongs/h/h6440.md).

<a name="psalms_42_6"></a>Psalms 42:6

['Elohiym](../../strongs/h/h430.md), my [nephesh](../../strongs/h/h5315.md) is [shachach](../../strongs/h/h7817.md) within me: therefore will I [zakar](../../strongs/h/h2142.md) thee from the ['erets](../../strongs/h/h776.md) of [Yardēn](../../strongs/h/h3383.md), and of the [ḥermônîm](../../strongs/h/h2769.md), from the [har](../../strongs/h/h2022.md) [miṣʿār](../../strongs/h/h4706.md).

<a name="psalms_42_7"></a>Psalms 42:7

[tĕhowm](../../strongs/h/h8415.md) [qara'](../../strongs/h/h7121.md) unto [tĕhowm](../../strongs/h/h8415.md) at the [qowl](../../strongs/h/h6963.md) of thy [ṣinnôr](../../strongs/h/h6794.md): all thy [mišbār](../../strongs/h/h4867.md) and thy [gal](../../strongs/h/h1530.md) are ['abar](../../strongs/h/h5674.md) over me.

<a name="psalms_42_8"></a>Psalms 42:8

Yet [Yĕhovah](../../strongs/h/h3068.md) will [tsavah](../../strongs/h/h6680.md) his [checed](../../strongs/h/h2617.md) in the [yômām](../../strongs/h/h3119.md), and in the [layil](../../strongs/h/h3915.md) his [šîr](../../strongs/h/h7892.md) shall be with me, and my [tĕphillah](../../strongs/h/h8605.md) unto the ['el](../../strongs/h/h410.md) of my [chay](../../strongs/h/h2416.md).

<a name="psalms_42_9"></a>Psalms 42:9

I will ['āmar](../../strongs/h/h559.md) unto ['el](../../strongs/h/h410.md) my [cela'](../../strongs/h/h5553.md), Why hast thou [shakach](../../strongs/h/h7911.md) me? why [yālaḵ](../../strongs/h/h3212.md) I [qāḏar](../../strongs/h/h6937.md) because of the [laḥaṣ](../../strongs/h/h3906.md) of the ['oyeb](../../strongs/h/h341.md)?

<a name="psalms_42_10"></a>Psalms 42:10

As with a [reṣaḥ](../../strongs/h/h7524.md) in my ['etsem](../../strongs/h/h6106.md), mine [tsarar](../../strongs/h/h6887.md) [ḥārap̄](../../strongs/h/h2778.md) me; while they ['āmar](../../strongs/h/h559.md) [yowm](../../strongs/h/h3117.md) unto me, Where is thy ['Elohiym](../../strongs/h/h430.md)?

<a name="psalms_42_11"></a>Psalms 42:11

Why art thou [shachach](../../strongs/h/h7817.md), O my [nephesh](../../strongs/h/h5315.md)? and why art thou [hāmâ](../../strongs/h/h1993.md) within me? [yāḥal](../../strongs/h/h3176.md) thou in ['Elohiym](../../strongs/h/h430.md): for I shall yet [yadah](../../strongs/h/h3034.md) him, who is the [yĕshuw'ah](../../strongs/h/h3444.md) of my [paniym](../../strongs/h/h6440.md), and my ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 41](psalms_41.md) - [Psalms 43](psalms_43.md)