# [Psalms 117](https://www.blueletterbible.org/kjv/psa/117/1/s_595001)

<a name="psalms_117_1"></a>Psalms 117:1

O [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md), all ye [gowy](../../strongs/h/h1471.md): [shabach](../../strongs/h/h7623.md) him, all ye ['ummah](../../strongs/h/h523.md).

<a name="psalms_117_2"></a>Psalms 117:2

For his [checed](../../strongs/h/h2617.md) [checed](../../strongs/h/h2617.md) is [gabar](../../strongs/h/h1396.md) toward us: and the ['emeth](../../strongs/h/h571.md) of [Yĕhovah](../../strongs/h/h3068.md) ['owlam](../../strongs/h/h5769.md). [halal](../../strongs/h/h1984.md) ye [Yahh](../../strongs/h/h3050.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 116](psalms_116.md) - [Psalms 118](psalms_118.md)