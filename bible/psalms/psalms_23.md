# [Psalms 23](https://www.blueletterbible.org/kjv/psa/23/1/s_501001)

<a name="psalms_23_1"></a>Psalms 23:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). [Yĕhovah](../../strongs/h/h3068.md) is my [ra'ah](../../strongs/h/h7462.md); I shall not [ḥāsēr](../../strongs/h/h2637.md).

<a name="psalms_23_2"></a>Psalms 23:2

He maketh me to [rāḇaṣ](../../strongs/h/h7257.md) in [deše'](../../strongs/h/h1877.md) [nā'â](../../strongs/h/h4999.md): he [nāhal](../../strongs/h/h5095.md) me beside the [mᵊnûḥâ](../../strongs/h/h4496.md) [mayim](../../strongs/h/h4325.md).

<a name="psalms_23_3"></a>Psalms 23:3

He [shuwb](../../strongs/h/h7725.md) my [nephesh](../../strongs/h/h5315.md): he [nachah](../../strongs/h/h5148.md) me in the [ma'gal](../../strongs/h/h4570.md) of [tsedeq](../../strongs/h/h6664.md) for his [shem](../../strongs/h/h8034.md) sake.

<a name="psalms_23_4"></a>Psalms 23:4

Yea, though I [yālaḵ](../../strongs/h/h3212.md) through the [gay'](../../strongs/h/h1516.md) of the [ṣalmāveṯ](../../strongs/h/h6757.md), I will [yare'](../../strongs/h/h3372.md) no [ra'](../../strongs/h/h7451.md): for thou art with me; thy [shebet](../../strongs/h/h7626.md) and thy [mašʿēnâ](../../strongs/h/h4938.md) they [nacham](../../strongs/h/h5162.md) me.

<a name="psalms_23_5"></a>Psalms 23:5

Thou ['arak](../../strongs/h/h6186.md) a [šulḥān](../../strongs/h/h7979.md) before me in the presence of mine [tsarar](../../strongs/h/h6887.md): thou [dāšēn](../../strongs/h/h1878.md) my [ro'sh](../../strongs/h/h7218.md) with [šemen](../../strongs/h/h8081.md); my [kowc](../../strongs/h/h3563.md) [rᵊvāyâ](../../strongs/h/h7310.md).

<a name="psalms_23_6"></a>Psalms 23:6

Surely [towb](../../strongs/h/h2896.md) and [checed](../../strongs/h/h2617.md) shall [radaph](../../strongs/h/h7291.md) me all the [yowm](../../strongs/h/h3117.md) of my [chay](../../strongs/h/h2416.md): and I will [yashab](../../strongs/h/h3427.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) ['ōreḵ](../../strongs/h/h753.md) [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 22](psalms_22.md) - [Psalms 24](psalms_24.md)