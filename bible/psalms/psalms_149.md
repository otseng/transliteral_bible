# [Psalms 149](https://www.blueletterbible.org/kjv/psalms/149)

<a name="psalms_149_1"></a>Psalms 149:1

[halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md). [shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md) a [ḥāḏāš](../../strongs/h/h2319.md) [šîr](../../strongs/h/h7892.md), and his [tehillah](../../strongs/h/h8416.md) in the [qāhēl](../../strongs/h/h6951.md) of [chaciyd](../../strongs/h/h2623.md).

<a name="psalms_149_2"></a>Psalms 149:2

Let [Yisra'el](../../strongs/h/h3478.md) [samach](../../strongs/h/h8055.md) in him that ['asah](../../strongs/h/h6213.md) him: let the [ben](../../strongs/h/h1121.md) of [Tsiyown](../../strongs/h/h6726.md) be [giyl](../../strongs/h/h1523.md) in their [melek](../../strongs/h/h4428.md).

<a name="psalms_149_3"></a>Psalms 149:3

Let them [halal](../../strongs/h/h1984.md) his [shem](../../strongs/h/h8034.md) in the [māḥôl](../../strongs/h/h4234.md): let them [zamar](../../strongs/h/h2167.md) unto him with the [tōp̄](../../strongs/h/h8596.md) and [kinnôr](../../strongs/h/h3658.md).

<a name="psalms_149_4"></a>Psalms 149:4

For [Yĕhovah](../../strongs/h/h3068.md) [ratsah](../../strongs/h/h7521.md) in his ['am](../../strongs/h/h5971.md): he will [pā'ar](../../strongs/h/h6286.md) the ['anav](../../strongs/h/h6035.md) with [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="psalms_149_5"></a>Psalms 149:5

Let the [chaciyd](../../strongs/h/h2623.md) be [ʿālaz](../../strongs/h/h5937.md) in [kabowd](../../strongs/h/h3519.md): let them [ranan](../../strongs/h/h7442.md) upon their [miškāḇ](../../strongs/h/h4904.md).

<a name="psalms_149_6"></a>Psalms 149:6

Let the [rômmâ](../../strongs/h/h7319.md) praises of ['el](../../strongs/h/h410.md) be in their [garown](../../strongs/h/h1627.md), and a [pîp̄îyôṯ](../../strongs/h/h6374.md) [chereb](../../strongs/h/h2719.md) in their [yad](../../strongs/h/h3027.md);

<a name="psalms_149_7"></a>Psalms 149:7

To ['asah](../../strongs/h/h6213.md) [nᵊqāmâ](../../strongs/h/h5360.md) upon the [gowy](../../strongs/h/h1471.md), and [tôḵēḥâ](../../strongs/h/h8433.md) upon the [lĕom](../../strongs/h/h3816.md);

<a name="psalms_149_8"></a>Psalms 149:8

To ['āsar](../../strongs/h/h631.md) their [melek](../../strongs/h/h4428.md) with [zîqôṯ](../../strongs/h/h2131.md), and their [kabad](../../strongs/h/h3513.md) with [keḇel](../../strongs/h/h3525.md) of [barzel](../../strongs/h/h1270.md);

<a name="psalms_149_9"></a>Psalms 149:9

To ['asah](../../strongs/h/h6213.md) upon them the [mishpat](../../strongs/h/h4941.md) [kāṯaḇ](../../strongs/h/h3789.md): this [hadar](../../strongs/h/h1926.md) have all his [chaciyd](../../strongs/h/h2623.md). [halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 148](psalms_148.md) - [Psalms 150](psalms_150.md)