# [Psalms 148](https://www.blueletterbible.org/kjv/psalms/148)

<a name="psalms_148_1"></a>Psalms 148:1

[halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md). [halal](../../strongs/h/h1984.md) ye [Yĕhovah](../../strongs/h/h3068.md) from the [shamayim](../../strongs/h/h8064.md): [halal](../../strongs/h/h1984.md) him in the [marowm](../../strongs/h/h4791.md).

<a name="psalms_148_2"></a>Psalms 148:2

[halal](../../strongs/h/h1984.md) ye him, all his [mal'ak](../../strongs/h/h4397.md): [halal](../../strongs/h/h1984.md) ye him, all his [tsaba'](../../strongs/h/h6635.md).

<a name="psalms_148_3"></a>Psalms 148:3

[halal](../../strongs/h/h1984.md) ye him, [šemeš](../../strongs/h/h8121.md) and [yareach](../../strongs/h/h3394.md): [halal](../../strongs/h/h1984.md) him, all ye [kowkab](../../strongs/h/h3556.md) of ['owr](../../strongs/h/h216.md).

<a name="psalms_148_4"></a>Psalms 148:4

[halal](../../strongs/h/h1984.md) him, ye [shamayim](../../strongs/h/h8064.md) of [shamayim](../../strongs/h/h8064.md), and ye [mayim](../../strongs/h/h4325.md) that be above the [shamayim](../../strongs/h/h8064.md).

<a name="psalms_148_5"></a>Psalms 148:5

Let them [halal](../../strongs/h/h1984.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md): for he [tsavah](../../strongs/h/h6680.md), and they were [bara'](../../strongs/h/h1254.md).

<a name="psalms_148_6"></a>Psalms 148:6

He hath also ['amad](../../strongs/h/h5975.md) them for [ʿaḏ](../../strongs/h/h5703.md) and ['owlam](../../strongs/h/h5769.md): he hath [nathan](../../strongs/h/h5414.md) a [choq](../../strongs/h/h2706.md) which shall not ['abar](../../strongs/h/h5674.md).

<a name="psalms_148_7"></a>Psalms 148:7

[halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md) from the ['erets](../../strongs/h/h776.md), ye [tannîn](../../strongs/h/h8577.md), and all [tĕhowm](../../strongs/h/h8415.md):

<a name="psalms_148_8"></a>Psalms 148:8

['esh](../../strongs/h/h784.md), and [barad](../../strongs/h/h1259.md); [šeleḡ](../../strongs/h/h7950.md), and [qîṭôr](../../strongs/h/h7008.md); [saʿar](../../strongs/h/h5591.md) [ruwach](../../strongs/h/h7307.md) ['asah](../../strongs/h/h6213.md) his [dabar](../../strongs/h/h1697.md):

<a name="psalms_148_9"></a>Psalms 148:9

[har](../../strongs/h/h2022.md), and all [giḇʿâ](../../strongs/h/h1389.md); [pĕriy](../../strongs/h/h6529.md) ['ets](../../strongs/h/h6086.md), and all ['erez](../../strongs/h/h730.md):

<a name="psalms_148_10"></a>Psalms 148:10

[chay](../../strongs/h/h2416.md), and all [bĕhemah](../../strongs/h/h929.md); [remeś](../../strongs/h/h7431.md), and [kanaph](../../strongs/h/h3671.md) [tsippowr](../../strongs/h/h6833.md):

<a name="psalms_148_11"></a>Psalms 148:11

[melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md), and all [lĕom](../../strongs/h/h3816.md); [śar](../../strongs/h/h8269.md), and all [shaphat](../../strongs/h/h8199.md) of the ['erets](../../strongs/h/h776.md):

<a name="psalms_148_12"></a>Psalms 148:12

Both [bāḥûr](../../strongs/h/h970.md), and [bᵊṯûlâ](../../strongs/h/h1330.md); [zāqēn](../../strongs/h/h2205.md), and [naʿar](../../strongs/h/h5288.md):

<a name="psalms_148_13"></a>Psalms 148:13

Let them [halal](../../strongs/h/h1984.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md): for his [shem](../../strongs/h/h8034.md) alone is [śāḡaḇ](../../strongs/h/h7682.md); his [howd](../../strongs/h/h1935.md) is above the ['erets](../../strongs/h/h776.md) and [shamayim](../../strongs/h/h8064.md).

<a name="psalms_148_14"></a>Psalms 148:14

He also [ruwm](../../strongs/h/h7311.md) the [qeren](../../strongs/h/h7161.md) of his ['am](../../strongs/h/h5971.md), the [tehillah](../../strongs/h/h8416.md) of all his [chaciyd](../../strongs/h/h2623.md); even of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), an ['am](../../strongs/h/h5971.md) [qarowb](../../strongs/h/h7138.md) unto him. [halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 147](psalms_147.md) - [Psalms 149](psalms_149.md)