# [Psalms 71](https://www.blueletterbible.org/kjv/psa/71)

<a name="psalms_71_1"></a>Psalms 71:1

In thee, [Yĕhovah](../../strongs/h/h3068.md), do I put my [chacah](../../strongs/h/h2620.md): let me ['al](../../strongs/h/h408.md) ['owlam](../../strongs/h/h5769.md) be put to [buwsh](../../strongs/h/h954.md).

<a name="psalms_71_2"></a>Psalms 71:2

[natsal](../../strongs/h/h5337.md) me in thy [tsedaqah](../../strongs/h/h6666.md), and cause me to [palat](../../strongs/h/h6403.md): [natah](../../strongs/h/h5186.md) thine ['ozen](../../strongs/h/h241.md) unto me, and [yasha'](../../strongs/h/h3467.md) me.

<a name="psalms_71_3"></a>Psalms 71:3

Be thou my [tsuwr](../../strongs/h/h6697.md) [māʿôn](../../strongs/h/h4583.md), whereunto I may [tāmîḏ](../../strongs/h/h8548.md) [bow'](../../strongs/h/h935.md): thou hast given [tsavah](../../strongs/h/h6680.md) to [yasha'](../../strongs/h/h3467.md) me; for thou art my [cela'](../../strongs/h/h5553.md) and my [matsuwd](../../strongs/h/h4686.md).

<a name="psalms_71_4"></a>Psalms 71:4

[palat](../../strongs/h/h6403.md) me, O my ['Elohiym](../../strongs/h/h430.md), out of the [yad](../../strongs/h/h3027.md) of the [rasha'](../../strongs/h/h7563.md), out of the [kaph](../../strongs/h/h3709.md) of the [ʿāval](../../strongs/h/h5765.md) and [ḥāmēṣ](../../strongs/h/h2556.md).

<a name="psalms_71_5"></a>Psalms 71:5

For thou art my [tiqvâ](../../strongs/h/h8615.md), ['Adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md): thou art my [miḇṭāḥ](../../strongs/h/h4009.md) from my [nāʿur](../../strongs/h/h5271.md).

<a name="psalms_71_6"></a>Psalms 71:6

By thee have I been [camak](../../strongs/h/h5564.md) from the [beten](../../strongs/h/h990.md): thou art he that [gāzâ](../../strongs/h/h1491.md) me out of my ['em](../../strongs/h/h517.md) [me'ah](../../strongs/h/h4578.md): my [tehillah](../../strongs/h/h8416.md) shall be [tāmîḏ](../../strongs/h/h8548.md) of thee.

<a name="psalms_71_7"></a>Psalms 71:7

I am as a [môp̄ēṯ](../../strongs/h/h4159.md) unto [rab](../../strongs/h/h7227.md); but thou art my ['oz](../../strongs/h/h5797.md) [machaceh](../../strongs/h/h4268.md).

<a name="psalms_71_8"></a>Psalms 71:8

Let my [peh](../../strongs/h/h6310.md) be [mālā'](../../strongs/h/h4390.md) with thy [tehillah](../../strongs/h/h8416.md) and with thy [tip̄'ārâ](../../strongs/h/h8597.md) all the [yowm](../../strongs/h/h3117.md).

<a name="psalms_71_9"></a>Psalms 71:9

[shalak](../../strongs/h/h7993.md) me not in the [ʿēṯ](../../strongs/h/h6256.md) of old [ziqnâ](../../strongs/h/h2209.md); ['azab](../../strongs/h/h5800.md) me not when my [koach](../../strongs/h/h3581.md) [kalah](../../strongs/h/h3615.md).

<a name="psalms_71_10"></a>Psalms 71:10

For mine ['oyeb](../../strongs/h/h341.md) ['āmar](../../strongs/h/h559.md) against me; and they that [shamar](../../strongs/h/h8104.md) for my [nephesh](../../strongs/h/h5315.md) [ya'ats](../../strongs/h/h3289.md) [yaḥaḏ](../../strongs/h/h3162.md),

<a name="psalms_71_11"></a>Psalms 71:11

['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) hath ['azab](../../strongs/h/h5800.md) him: [radaph](../../strongs/h/h7291.md) and [tāp̄aś](../../strongs/h/h8610.md) him; for there is none to [natsal](../../strongs/h/h5337.md) him.

<a name="psalms_71_12"></a>Psalms 71:12

['Elohiym](../../strongs/h/h430.md), be not [rachaq](../../strongs/h/h7368.md) from me: O my ['Elohiym](../../strongs/h/h430.md), make [ḥûš](../../strongs/h/h2363.md) [ḥîš](../../strongs/h/h2439.md) for my [ʿezrâ](../../strongs/h/h5833.md).

<a name="psalms_71_13"></a>Psalms 71:13

Let them be [buwsh](../../strongs/h/h954.md) and [kalah](../../strongs/h/h3615.md) that are [śāṭan](../../strongs/h/h7853.md) to my [nephesh](../../strongs/h/h5315.md); let them be [ʿāṭâ](../../strongs/h/h5844.md) with [cherpah](../../strongs/h/h2781.md) and [kĕlimmah](../../strongs/h/h3639.md) that [bāqaš](../../strongs/h/h1245.md) my [ra'](../../strongs/h/h7451.md).

<a name="psalms_71_14"></a>Psalms 71:14

But I will [yāḥal](../../strongs/h/h3176.md) [tāmîḏ](../../strongs/h/h8548.md), and will yet [tehillah](../../strongs/h/h8416.md) thee [yāsap̄](../../strongs/h/h3254.md) and [yāsap̄](../../strongs/h/h3254.md).

<a name="psalms_71_15"></a>Psalms 71:15

My [peh](../../strongs/h/h6310.md) shall shew [sāp̄ar](../../strongs/h/h5608.md) thy [tsedaqah](../../strongs/h/h6666.md) and thy [tᵊšûʿâ](../../strongs/h/h8668.md) all the [yowm](../../strongs/h/h3117.md); for I [yada'](../../strongs/h/h3045.md) not the [sᵊp̄ōrâ](../../strongs/h/h5615.md) thereof.

<a name="psalms_71_16"></a>Psalms 71:16

I will go [bow'](../../strongs/h/h935.md) the [gᵊḇûrâ](../../strongs/h/h1369.md) of the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md): I will make [zakar](../../strongs/h/h2142.md) of thy [tsedaqah](../../strongs/h/h6666.md), even of thine only.

<a name="psalms_71_17"></a>Psalms 71:17

['Elohiym](../../strongs/h/h430.md), thou hast [lamad](../../strongs/h/h3925.md) me from my [nāʿur](../../strongs/h/h5271.md): and hitherto have I [nāḡaḏ](../../strongs/h/h5046.md) thy [pala'](../../strongs/h/h6381.md).

<a name="psalms_71_18"></a>Psalms 71:18

Now also [ʿaḏ](../../strongs/h/h5704.md) I am [ziqnâ](../../strongs/h/h2209.md) and [śêḇâ](../../strongs/h/h7872.md), ['Elohiym](../../strongs/h/h430.md), ['azab](../../strongs/h/h5800.md) me not; until I have [nāḡaḏ](../../strongs/h/h5046.md) thy [zerowa'](../../strongs/h/h2220.md) unto this [dôr](../../strongs/h/h1755.md), and thy [gᵊḇûrâ](../../strongs/h/h1369.md) to every one that is to [bow'](../../strongs/h/h935.md).

<a name="psalms_71_19"></a>Psalms 71:19

Thy [tsedaqah](../../strongs/h/h6666.md) also, ['Elohiym](../../strongs/h/h430.md), is very [marowm](../../strongs/h/h4791.md), who hast ['asah](../../strongs/h/h6213.md) [gadowl](../../strongs/h/h1419.md): ['Elohiym](../../strongs/h/h430.md), who is like unto thee!

<a name="psalms_71_20"></a>Psalms 71:20

Thou, which hast [ra'ah](../../strongs/h/h7200.md) me [rab](../../strongs/h/h7227.md) and [ra'](../../strongs/h/h7451.md) [tsarah](../../strongs/h/h6869.md), shalt [ḥāyâ](../../strongs/h/h2421.md) me [shuwb](../../strongs/h/h7725.md), and shalt [ʿālâ](../../strongs/h/h5927.md) me [shuwb](../../strongs/h/h7725.md) from the [tĕhowm](../../strongs/h/h8415.md) of the ['erets](../../strongs/h/h776.md).

<a name="psalms_71_21"></a>Psalms 71:21

Thou shalt [rabah](../../strongs/h/h7235.md) my [gᵊḏûlâ](../../strongs/h/h1420.md), and [nacham](../../strongs/h/h5162.md) me on every [cabab](../../strongs/h/h5437.md).

<a name="psalms_71_22"></a>Psalms 71:22

I will also [yadah](../../strongs/h/h3034.md) thee with the [kĕliy](../../strongs/h/h3627.md) [neḇel](../../strongs/h/h5035.md), even thy ['emeth](../../strongs/h/h571.md), O my ['Elohiym](../../strongs/h/h430.md): unto thee will I [zamar](../../strongs/h/h2167.md) with the [kinnôr](../../strongs/h/h3658.md), O thou [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="psalms_71_23"></a>Psalms 71:23

My [saphah](../../strongs/h/h8193.md) shall [ranan](../../strongs/h/h7442.md) when I [zamar](../../strongs/h/h2167.md) unto thee; and my [nephesh](../../strongs/h/h5315.md), which thou hast [pāḏâ](../../strongs/h/h6299.md).

<a name="psalms_71_24"></a>Psalms 71:24

My [lashown](../../strongs/h/h3956.md) also shall [hagah](../../strongs/h/h1897.md) of thy [tsedaqah](../../strongs/h/h6666.md) all the [yowm](../../strongs/h/h3117.md) long: for they are [buwsh](../../strongs/h/h954.md), for they are [ḥāp̄ēr](../../strongs/h/h2659.md), that [bāqaš](../../strongs/h/h1245.md) my [ra'](../../strongs/h/h7451.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 70](psalms_70.md) - [Psalms 72](psalms_72.md)