# [Psalms 97](https://www.blueletterbible.org/kjv/psalms/97)

<a name="psalms_97_1"></a>Psalms 97:1

[Yĕhovah](../../strongs/h/h3068.md) [mālaḵ](../../strongs/h/h4427.md); let the ['erets](../../strongs/h/h776.md) [giyl](../../strongs/h/h1523.md); let the [rab](../../strongs/h/h7227.md) of ['î](../../strongs/h/h339.md) be [samach](../../strongs/h/h8055.md) thereof.

<a name="psalms_97_2"></a>Psalms 97:2

[ʿānān](../../strongs/h/h6051.md) and ['araphel](../../strongs/h/h6205.md) are [cabiyb](../../strongs/h/h5439.md) him: [tsedeq](../../strongs/h/h6664.md) and [mishpat](../../strongs/h/h4941.md) are the [māḵôn](../../strongs/h/h4349.md) of his [kicce'](../../strongs/h/h3678.md).

<a name="psalms_97_3"></a>Psalms 97:3

An ['esh](../../strongs/h/h784.md) [yālaḵ](../../strongs/h/h3212.md) [paniym](../../strongs/h/h6440.md) him, and [lāhaṭ](../../strongs/h/h3857.md) his [tsar](../../strongs/h/h6862.md) [cabiyb](../../strongs/h/h5439.md).

<a name="psalms_97_4"></a>Psalms 97:4

His [baraq](../../strongs/h/h1300.md) ['owr](../../strongs/h/h215.md) the [tebel](../../strongs/h/h8398.md): the ['erets](../../strongs/h/h776.md) [ra'ah](../../strongs/h/h7200.md), and [chuwl](../../strongs/h/h2342.md).

<a name="psalms_97_5"></a>Psalms 97:5

The [har](../../strongs/h/h2022.md) [māsas](../../strongs/h/h4549.md) like [dônāḡ](../../strongs/h/h1749.md) at the [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md), at the [paniym](../../strongs/h/h6440.md) of the ['adown](../../strongs/h/h113.md) of the ['erets](../../strongs/h/h776.md).

<a name="psalms_97_6"></a>Psalms 97:6

The [shamayim](../../strongs/h/h8064.md) [nāḡaḏ](../../strongs/h/h5046.md) his [tsedeq](../../strongs/h/h6664.md), and all the ['am](../../strongs/h/h5971.md) [ra'ah](../../strongs/h/h7200.md) his [kabowd](../../strongs/h/h3519.md).

<a name="psalms_97_7"></a>Psalms 97:7

[buwsh](../../strongs/h/h954.md) be all they that ['abad](../../strongs/h/h5647.md) [pecel](../../strongs/h/h6459.md), that [halal](../../strongs/h/h1984.md) themselves of ['ĕlîl](../../strongs/h/h457.md): [shachah](../../strongs/h/h7812.md) him, all ye ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_97_8"></a>Psalms 97:8

[Tsiyown](../../strongs/h/h6726.md) [shama'](../../strongs/h/h8085.md), and was [samach](../../strongs/h/h8055.md); and the [bath](../../strongs/h/h1323.md) of [Yehuwdah](../../strongs/h/h3063.md) [giyl](../../strongs/h/h1523.md) because of thy [mishpat](../../strongs/h/h4941.md), [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_97_9"></a>Psalms 97:9

For thou, [Yĕhovah](../../strongs/h/h3068.md), art ['elyown](../../strongs/h/h5945.md) all the ['erets](../../strongs/h/h776.md): thou art [ʿālâ](../../strongs/h/h5927.md) [me'od](../../strongs/h/h3966.md) all ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_97_10"></a>Psalms 97:10

Ye that ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md), [sane'](../../strongs/h/h8130.md) [ra'](../../strongs/h/h7451.md): he [shamar](../../strongs/h/h8104.md) the [nephesh](../../strongs/h/h5315.md) of his [chaciyd](../../strongs/h/h2623.md); he [natsal](../../strongs/h/h5337.md) them out of the [yad](../../strongs/h/h3027.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="psalms_97_11"></a>Psalms 97:11

['owr](../../strongs/h/h216.md) is [zāraʿ](../../strongs/h/h2232.md) for the [tsaddiyq](../../strongs/h/h6662.md), and [simchah](../../strongs/h/h8057.md) for the [yashar](../../strongs/h/h3477.md) in [leb](../../strongs/h/h3820.md).

<a name="psalms_97_12"></a>Psalms 97:12

[samach](../../strongs/h/h8055.md) in [Yĕhovah](../../strongs/h/h3068.md), ye [tsaddiyq](../../strongs/h/h6662.md); and [yadah](../../strongs/h/h3034.md) at the [zeker](../../strongs/h/h2143.md) of his [qodesh](../../strongs/h/h6944.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 96](psalms_96.md) - [Psalms 98](psalms_98.md)