# [Psalms 51](https://www.blueletterbible.org/kjv/psa/51/1)

<a name="psalms_51_1"></a>Psalms 51:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md), when [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md) [bow'](../../strongs/h/h935.md) unto him, after he had [bow'](../../strongs/h/h935.md) to [Baṯ-Šeḇaʿ](../../strongs/h/h1339.md). Have [chanan](../../strongs/h/h2603.md) upon me, O ['Elohiym](../../strongs/h/h430.md), according to thy [checed](../../strongs/h/h2617.md): according unto the [rōḇ](../../strongs/h/h7230.md) of thy [raḥam](../../strongs/h/h7356.md) [māḥâ](../../strongs/h/h4229.md) my [pesha'](../../strongs/h/h6588.md).

<a name="psalms_51_2"></a>Psalms 51:2

[kāḇas](../../strongs/h/h3526.md) [rabah](../../strongs/h/h7235.md) me from mine ['avon](../../strongs/h/h5771.md), and [ṭāhēr](../../strongs/h/h2891.md) me from my [chatta'ath](../../strongs/h/h2403.md).

<a name="psalms_51_3"></a>Psalms 51:3

For I [yada'](../../strongs/h/h3045.md) my [pesha'](../../strongs/h/h6588.md): and my [chatta'ath](../../strongs/h/h2403.md) is [tāmîḏ](../../strongs/h/h8548.md) before me.

<a name="psalms_51_4"></a>Psalms 51:4

Against thee, thee only, have I [chata'](../../strongs/h/h2398.md), and ['asah](../../strongs/h/h6213.md) this [ra'](../../strongs/h/h7451.md) in thy ['ayin](../../strongs/h/h5869.md): that thou mightest be [ṣāḏaq](../../strongs/h/h6663.md) when thou [dabar](../../strongs/h/h1696.md), and be [zāḵâ](../../strongs/h/h2135.md) when thou [shaphat](../../strongs/h/h8199.md).

<a name="psalms_51_5"></a>Psalms 51:5

Behold, I was [chuwl](../../strongs/h/h2342.md) in ['avon](../../strongs/h/h5771.md); and in [ḥēṭĕ'](../../strongs/h/h2399.md) did my ['em](../../strongs/h/h517.md) [yāḥam](../../strongs/h/h3179.md) me.

<a name="psalms_51_6"></a>Psalms 51:6

Behold, thou [ḥāp̄ēṣ](../../strongs/h/h2654.md) ['emeth](../../strongs/h/h571.md) in the [ṭuḥâ](../../strongs/h/h2910.md): and in the [sāṯam](../../strongs/h/h5640.md) part thou shalt make me to [yada'](../../strongs/h/h3045.md) [ḥāḵmâ](../../strongs/h/h2451.md).

<a name="psalms_51_7"></a>Psalms 51:7

[chata'](../../strongs/h/h2398.md) me with ['ēzôḇ](../../strongs/h/h231.md), and I shall be [ṭāhēr](../../strongs/h/h2891.md): [kāḇas](../../strongs/h/h3526.md) me, and I shall be [lāḇan](../../strongs/h/h3835.md) than [šeleḡ](../../strongs/h/h7950.md).

<a name="psalms_51_8"></a>Psalms 51:8

Make me to [shama'](../../strongs/h/h8085.md) [śāśôn](../../strongs/h/h8342.md) and [simchah](../../strongs/h/h8057.md); that the ['etsem](../../strongs/h/h6106.md) which thou hast [dakah](../../strongs/h/h1794.md) may [giyl](../../strongs/h/h1523.md).

<a name="psalms_51_9"></a>Psalms 51:9

[cathar](../../strongs/h/h5641.md) thy [paniym](../../strongs/h/h6440.md) from my [ḥēṭĕ'](../../strongs/h/h2399.md), and [māḥâ](../../strongs/h/h4229.md) all mine ['avon](../../strongs/h/h5771.md).

<a name="psalms_51_10"></a>Psalms 51:10

[bara'](../../strongs/h/h1254.md) in me a [tahowr](../../strongs/h/h2889.md) [leb](../../strongs/h/h3820.md), ['Elohiym](../../strongs/h/h430.md); and [ḥādaš](../../strongs/h/h2318.md) a [kuwn](../../strongs/h/h3559.md) [ruwach](../../strongs/h/h7307.md) [qereḇ](../../strongs/h/h7130.md) me.

<a name="psalms_51_11"></a>Psalms 51:11

[shalak](../../strongs/h/h7993.md) me not from thy [paniym](../../strongs/h/h6440.md); and [laqach](../../strongs/h/h3947.md) not thy [qodesh](../../strongs/h/h6944.md) [ruwach](../../strongs/h/h7307.md) from me.

<a name="psalms_51_12"></a>Psalms 51:12

[shuwb](../../strongs/h/h7725.md) unto me the [śāśôn](../../strongs/h/h8342.md) of thy [yesha'](../../strongs/h/h3468.md); and [camak](../../strongs/h/h5564.md) me with thy [nāḏîḇ](../../strongs/h/h5081.md) [ruwach](../../strongs/h/h7307.md).

<a name="psalms_51_13"></a>Psalms 51:13

Then will I [lamad](../../strongs/h/h3925.md) [pāšaʿ](../../strongs/h/h6586.md) thy [derek](../../strongs/h/h1870.md); and [chatta'](../../strongs/h/h2400.md) shall be [shuwb](../../strongs/h/h7725.md) unto thee.

<a name="psalms_51_14"></a>Psalms 51:14

[natsal](../../strongs/h/h5337.md) me from [dam](../../strongs/h/h1818.md), ['Elohiym](../../strongs/h/h430.md), thou ['Elohiym](../../strongs/h/h430.md) of my [tᵊšûʿâ](../../strongs/h/h8668.md): and my [lashown](../../strongs/h/h3956.md) shall [ranan](../../strongs/h/h7442.md) of thy [ṣĕdāqāh](../../strongs/h/h6666.md).

<a name="psalms_51_15"></a>Psalms 51:15

['adonay](../../strongs/h/h136.md), [pāṯaḥ](../../strongs/h/h6605.md) thou my [saphah](../../strongs/h/h8193.md); and my [peh](../../strongs/h/h6310.md) shall [nāḡaḏ](../../strongs/h/h5046.md) thy [tehillah](../../strongs/h/h8416.md).

<a name="psalms_51_16"></a>Psalms 51:16

For thou [ḥāp̄ēṣ](../../strongs/h/h2654.md) not [zebach](../../strongs/h/h2077.md); else would I [nathan](../../strongs/h/h5414.md) it: thou [ratsah](../../strongs/h/h7521.md) not in an [ʿōlâ](../../strongs/h/h5930.md).

<a name="psalms_51_17"></a>Psalms 51:17

The [zebach](../../strongs/h/h2077.md) of ['Elohiym](../../strongs/h/h430.md) are a [shabar](../../strongs/h/h7665.md) [ruwach](../../strongs/h/h7307.md): a [shabar](../../strongs/h/h7665.md) and a [dakah](../../strongs/h/h1794.md) [leb](../../strongs/h/h3820.md), ['Elohiym](../../strongs/h/h430.md), thou wilt not [bazah](../../strongs/h/h959.md).

<a name="psalms_51_18"></a>Psalms 51:18

[yatab](../../strongs/h/h3190.md) in thy [ratsown](../../strongs/h/h7522.md) unto [Tsiyown](../../strongs/h/h6726.md): [bānâ](../../strongs/h/h1129.md) thou the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="psalms_51_19"></a>Psalms 51:19

Then shalt thou be [ḥāp̄ēṣ](../../strongs/h/h2654.md) with the [zebach](../../strongs/h/h2077.md) of [tsedeq](../../strongs/h/h6664.md), with an [ʿōlâ](../../strongs/h/h5930.md) and [kālîl](../../strongs/h/h3632.md) an [ʿōlâ](../../strongs/h/h5930.md): then shall they [ʿālâ](../../strongs/h/h5927.md) [par](../../strongs/h/h6499.md) upon thine [mizbeach](../../strongs/h/h4196.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 50](psalms_50.md) - [Psalms 52](psalms_52.md)