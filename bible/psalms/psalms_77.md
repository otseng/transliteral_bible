# [Psalms 77](https://www.blueletterbible.org/kjv/psa/77)

<a name="psalms_77_1"></a>Psalms 77:1

To the [nāṣaḥ](../../strongs/h/h5329.md), to [Yᵊḏûṯûn](../../strongs/h/h3038.md), A [mizmôr](../../strongs/h/h4210.md) of ['Āsāp̄](../../strongs/h/h623.md). I [ṣāʿaq](../../strongs/h/h6817.md) unto ['Elohiym](../../strongs/h/h430.md) with my [qowl](../../strongs/h/h6963.md), even unto ['Elohiym](../../strongs/h/h430.md) with my [qowl](../../strongs/h/h6963.md); and he ['azan](../../strongs/h/h238.md) unto me.

<a name="psalms_77_2"></a>Psalms 77:2

In the [yowm](../../strongs/h/h3117.md) of my [tsarah](../../strongs/h/h6869.md) I [darash](../../strongs/h/h1875.md) the ['adonay](../../strongs/h/h136.md): my [yad](../../strongs/h/h3027.md) [nāḡar](../../strongs/h/h5064.md) in the [layil](../../strongs/h/h3915.md), and [pûḡ](../../strongs/h/h6313.md) not: my [nephesh](../../strongs/h/h5315.md) [mā'ēn](../../strongs/h/h3985.md) to be [nacham](../../strongs/h/h5162.md).

<a name="psalms_77_3"></a>Psalms 77:3

I [zakar](../../strongs/h/h2142.md) ['Elohiym](../../strongs/h/h430.md), and was [hāmâ](../../strongs/h/h1993.md): I [śîaḥ](../../strongs/h/h7878.md), and my [ruwach](../../strongs/h/h7307.md) was [ʿāṭap̄](../../strongs/h/h5848.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_77_4"></a>Psalms 77:4

Thou ['āḥaz](../../strongs/h/h270.md) mine ['ayin](../../strongs/h/h5869.md) [šᵊmurâ](../../strongs/h/h8109.md): I am so [pāʿam](../../strongs/h/h6470.md) that I cannot [dabar](../../strongs/h/h1696.md).

<a name="psalms_77_5"></a>Psalms 77:5

I have [chashab](../../strongs/h/h2803.md) the [yowm](../../strongs/h/h3117.md) of [qeḏem](../../strongs/h/h6924.md), the [šānâ](../../strongs/h/h8141.md) of ['owlam](../../strongs/h/h5769.md).

<a name="psalms_77_6"></a>Psalms 77:6

I [zakar](../../strongs/h/h2142.md) my [Nᵊḡînâ](../../strongs/h/h5058.md) in the [layil](../../strongs/h/h3915.md): I [śîaḥ](../../strongs/h/h7878.md) with mine own [lebab](../../strongs/h/h3824.md): and my [ruwach](../../strongs/h/h7307.md) made [ḥāp̄aś](../../strongs/h/h2664.md).

<a name="psalms_77_7"></a>Psalms 77:7

Will the ['adonay](../../strongs/h/h136.md) [zānaḥ](../../strongs/h/h2186.md) for ['owlam](../../strongs/h/h5769.md) ? and [yāsap̄](../../strongs/h/h3254.md) he be [ratsah](../../strongs/h/h7521.md) no more?

<a name="psalms_77_8"></a>Psalms 77:8

Is his [checed](../../strongs/h/h2617.md) ['āp̄as](../../strongs/h/h656.md) for [netsach](../../strongs/h/h5331.md) ? doth his ['omer](../../strongs/h/h562.md) [gāmar](../../strongs/h/h1584.md) for [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md) ?

<a name="psalms_77_9"></a>Psalms 77:9

Hath ['el](../../strongs/h/h410.md) [shakach](../../strongs/h/h7911.md) to be [ḥānnôṯ](../../strongs/h/h2589.md) ? hath he in ['aph](../../strongs/h/h639.md) shut [qāp̄aṣ](../../strongs/h/h7092.md) his [raḥam](../../strongs/h/h7356.md) ? [Celah](../../strongs/h/h5542.md).

<a name="psalms_77_10"></a>Psalms 77:10

And I ['āmar](../../strongs/h/h559.md), This is my [ḥālâ](../../strongs/h/h2470.md): but I will remember the [šānâ](../../strongs/h/h8141.md) of the right [yamiyn](../../strongs/h/h3225.md) of the ['elyown](../../strongs/h/h5945.md).

<a name="psalms_77_11"></a>Psalms 77:11

I will [zakar](../../strongs/h/h2142.md) [zakar](../../strongs/h/h2142.md) the [maʿălāl](../../strongs/h/h4611.md) of the [Yahh](../../strongs/h/h3050.md): surely I will [zakar](../../strongs/h/h2142.md) thy [pele'](../../strongs/h/h6382.md) of [qeḏem](../../strongs/h/h6924.md).

<a name="psalms_77_12"></a>Psalms 77:12

I will [hagah](../../strongs/h/h1897.md) also of all thy [pōʿal](../../strongs/h/h6467.md), and [śîaḥ](../../strongs/h/h7878.md) of thy ['aliylah](../../strongs/h/h5949.md).

<a name="psalms_77_13"></a>Psalms 77:13

Thy [derek](../../strongs/h/h1870.md), ['Elohiym](../../strongs/h/h430.md), is in the [qodesh](../../strongs/h/h6944.md): who is so [gadowl](../../strongs/h/h1419.md) an ['el](../../strongs/h/h410.md) as our ['Elohiym](../../strongs/h/h430.md) ?

<a name="psalms_77_14"></a>Psalms 77:14

Thou art the ['el](../../strongs/h/h410.md) that ['asah](../../strongs/h/h6213.md) [pele'](../../strongs/h/h6382.md): thou hast [yada'](../../strongs/h/h3045.md) thy ['oz](../../strongs/h/h5797.md) among the ['am](../../strongs/h/h5971.md).

<a name="psalms_77_15"></a>Psalms 77:15

Thou hast with thine [zerowa'](../../strongs/h/h2220.md) [gā'al](../../strongs/h/h1350.md) thy ['am](../../strongs/h/h5971.md), the [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md) and [Yôsēp̄](../../strongs/h/h3130.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_77_16"></a>Psalms 77:16

The [mayim](../../strongs/h/h4325.md) [ra'ah](../../strongs/h/h7200.md) thee, ['Elohiym](../../strongs/h/h430.md), the [mayim](../../strongs/h/h4325.md) [ra'ah](../../strongs/h/h7200.md) thee; they were [chuwl](../../strongs/h/h2342.md): the [tĕhowm](../../strongs/h/h8415.md) also were [ragaz](../../strongs/h/h7264.md).

<a name="psalms_77_17"></a>Psalms 77:17

The ['ab](../../strongs/h/h5645.md) [zāram](../../strongs/h/h2229.md) [mayim](../../strongs/h/h4325.md): the [shachaq](../../strongs/h/h7834.md) sent [nathan](../../strongs/h/h5414.md) a [qowl](../../strongs/h/h6963.md): thine [ḥāṣāṣ](../../strongs/h/h2687.md) also went [halak](../../strongs/h/h1980.md).

<a name="psalms_77_18"></a>Psalms 77:18

The [qowl](../../strongs/h/h6963.md) of thy [raʿam](../../strongs/h/h7482.md) was in the [galgal](../../strongs/h/h1534.md): the [baraq](../../strongs/h/h1300.md) ['owr](../../strongs/h/h215.md) the [tebel](../../strongs/h/h8398.md): the ['erets](../../strongs/h/h776.md) [ragaz](../../strongs/h/h7264.md) and [rāʿaš](../../strongs/h/h7493.md).

<a name="psalms_77_19"></a>Psalms 77:19

Thy [derek](../../strongs/h/h1870.md) is in the [yam](../../strongs/h/h3220.md), and thy [šᵊḇîl](../../strongs/h/h7635.md) in the [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md), and thy ['aqeb](../../strongs/h/h6119.md) are not [yada'](../../strongs/h/h3045.md).

<a name="psalms_77_20"></a>Psalms 77:20

Thou [nachah](../../strongs/h/h5148.md) thy ['am](../../strongs/h/h5971.md) like a [tso'n](../../strongs/h/h6629.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 76](psalms_76.md) - [Psalms 78](psalms_78.md)