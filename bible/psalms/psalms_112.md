# [Psalms 112](https://www.blueletterbible.org/kjv/psalms/112)

<a name="psalms_112_1"></a>Psalms 112:1

[halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md). ['esher](../../strongs/h/h835.md) is the ['iysh](../../strongs/h/h376.md) that [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md), that [ḥāp̄ēṣ](../../strongs/h/h2654.md) [me'od](../../strongs/h/h3966.md) in his [mitsvah](../../strongs/h/h4687.md).

<a name="psalms_112_2"></a>Psalms 112:2

His [zera'](../../strongs/h/h2233.md) shall be [gibôr](../../strongs/h/h1368.md) upon ['erets](../../strongs/h/h776.md): the [dôr](../../strongs/h/h1755.md) of the [yashar](../../strongs/h/h3477.md) shall be [barak](../../strongs/h/h1288.md).

<a name="psalms_112_3"></a>Psalms 112:3

[hôn](../../strongs/h/h1952.md) and [ʿōšer](../../strongs/h/h6239.md) shall be in his [bayith](../../strongs/h/h1004.md): and his [tsedaqah](../../strongs/h/h6666.md) ['amad](../../strongs/h/h5975.md) for [ʿaḏ](../../strongs/h/h5703.md).

<a name="psalms_112_4"></a>Psalms 112:4

Unto the [yashar](../../strongs/h/h3477.md) there [zāraḥ](../../strongs/h/h2224.md) ['owr](../../strongs/h/h216.md) in the [choshek](../../strongs/h/h2822.md): he is [ḥanwn](../../strongs/h/h2587.md), and full of [raḥwm](../../strongs/h/h7349.md), and [tsaddiyq](../../strongs/h/h6662.md).

<a name="psalms_112_5"></a>Psalms 112:5

A [towb](../../strongs/h/h2896.md) ['iysh](../../strongs/h/h376.md) sheweth [chanan](../../strongs/h/h2603.md), and [lāvâ](../../strongs/h/h3867.md): he will [kûl](../../strongs/h/h3557.md) his [dabar](../../strongs/h/h1697.md) with [mishpat](../../strongs/h/h4941.md).

<a name="psalms_112_6"></a>Psalms 112:6

Surely he shall not be [mowt](../../strongs/h/h4131.md) ['owlam](../../strongs/h/h5769.md): the [tsaddiyq](../../strongs/h/h6662.md) shall be in ['owlam](../../strongs/h/h5769.md) [zeker](../../strongs/h/h2143.md).

<a name="psalms_112_7"></a>Psalms 112:7

He shall not be [yare'](../../strongs/h/h3372.md) of [ra'](../../strongs/h/h7451.md) [šᵊmûʿâ](../../strongs/h/h8052.md): his [leb](../../strongs/h/h3820.md) is [kuwn](../../strongs/h/h3559.md), [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_112_8"></a>Psalms 112:8

His [leb](../../strongs/h/h3820.md) is [camak](../../strongs/h/h5564.md), he shall not be [yare'](../../strongs/h/h3372.md), until he [ra'ah](../../strongs/h/h7200.md) his desire upon his [tsar](../../strongs/h/h6862.md).

<a name="psalms_112_9"></a>Psalms 112:9

He hath [p̄zr](../../strongs/h/h6340.md), he hath [nathan](../../strongs/h/h5414.md) to the ['ebyown](../../strongs/h/h34.md); his [tsedaqah](../../strongs/h/h6666.md) ['amad](../../strongs/h/h5975.md) for [ʿaḏ](../../strongs/h/h5703.md); his [qeren](../../strongs/h/h7161.md) shall be [ruwm](../../strongs/h/h7311.md) with [kabowd](../../strongs/h/h3519.md).

<a name="psalms_112_10"></a>Psalms 112:10

The [rasha'](../../strongs/h/h7563.md) shall [ra'ah](../../strongs/h/h7200.md) it, and be [kāʿas](../../strongs/h/h3707.md); he shall [ḥāraq](../../strongs/h/h2786.md) with his [šēn](../../strongs/h/h8127.md), and [māsas](../../strongs/h/h4549.md): the [ta'avah](../../strongs/h/h8378.md) of the [rasha'](../../strongs/h/h7563.md) shall ['abad](../../strongs/h/h6.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 111](psalms_111.md) - [Psalms 113](psalms_113.md)