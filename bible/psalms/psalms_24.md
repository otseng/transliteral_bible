# [Psalms 24](https://www.blueletterbible.org/kjv/psa/24/1/s_502001)

<a name="psalms_24_1"></a>Psalms 24:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). The ['erets](../../strongs/h/h776.md) is [Yĕhovah](../../strongs/h/h3068.md), and the [mᵊlō'](../../strongs/h/h4393.md) thereof; the [tebel](../../strongs/h/h8398.md), and they that [yashab](../../strongs/h/h3427.md) therein.

<a name="psalms_24_2"></a>Psalms 24:2

For he hath [yacad](../../strongs/h/h3245.md) it upon the [yam](../../strongs/h/h3220.md), and [kuwn](../../strongs/h/h3559.md) it upon the [nāhār](../../strongs/h/h5104.md).

<a name="psalms_24_3"></a>Psalms 24:3

Who shall [ʿālâ](../../strongs/h/h5927.md) into the [har](../../strongs/h/h2022.md) of [Yĕhovah](../../strongs/h/h3068.md)? or who shall [quwm](../../strongs/h/h6965.md) in his [qodesh](../../strongs/h/h6944.md) [maqowm](../../strongs/h/h4725.md)?

<a name="psalms_24_4"></a>Psalms 24:4

He that hath [naqiy](../../strongs/h/h5355.md) [kaph](../../strongs/h/h3709.md), and a [bar](../../strongs/h/h1249.md) [lebab](../../strongs/h/h3824.md); who hath not [nasa'](../../strongs/h/h5375.md) his [nephesh](../../strongs/h/h5315.md) unto [shav'](../../strongs/h/h7723.md), nor [shaba'](../../strongs/h/h7650.md) [mirmah](../../strongs/h/h4820.md).

<a name="psalms_24_5"></a>Psalms 24:5

He shall [nasa'](../../strongs/h/h5375.md) the [bĕrakah](../../strongs/h/h1293.md) from [Yĕhovah](../../strongs/h/h3068.md), and [ṣĕdāqāh](../../strongs/h/h6666.md) from the ['Elohiym](../../strongs/h/h430.md) of his [yesha'](../../strongs/h/h3468.md).

<a name="psalms_24_6"></a>Psalms 24:6

This is the [dôr](../../strongs/h/h1755.md) of them that [darash](../../strongs/h/h1875.md) him, that [bāqaš](../../strongs/h/h1245.md) thy [paniym](../../strongs/h/h6440.md), O [Ya'aqob](../../strongs/h/h3290.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_24_7"></a>Psalms 24:7

[nasa'](../../strongs/h/h5375.md) your [ro'sh](../../strongs/h/h7218.md), O ye [sha'ar](../../strongs/h/h8179.md); and be ye [nasa'](../../strongs/h/h5375.md), ye ['owlam](../../strongs/h/h5769.md) [peṯaḥ](../../strongs/h/h6607.md); and the [melek](../../strongs/h/h4428.md) of [kabowd](../../strongs/h/h3519.md) shall [bow'](../../strongs/h/h935.md).

<a name="psalms_24_8"></a>Psalms 24:8

Who is this [melek](../../strongs/h/h4428.md) of [kabowd](../../strongs/h/h3519.md)? [Yĕhovah](../../strongs/h/h3068.md) [ʿizzûz](../../strongs/h/h5808.md) and [gibôr](../../strongs/h/h1368.md), [Yĕhovah](../../strongs/h/h3068.md) [gibôr](../../strongs/h/h1368.md) in [milḥāmâ](../../strongs/h/h4421.md).

<a name="psalms_24_9"></a>Psalms 24:9

[nasa'](../../strongs/h/h5375.md) your [ro'sh](../../strongs/h/h7218.md), O ye [sha'ar](../../strongs/h/h8179.md); even [nasa'](../../strongs/h/h5375.md) them up, ye ['owlam](../../strongs/h/h5769.md) [peṯaḥ](../../strongs/h/h6607.md); and the [melek](../../strongs/h/h4428.md) of [kabowd](../../strongs/h/h3519.md) shall [bow'](../../strongs/h/h935.md) in.

<a name="psalms_24_10"></a>Psalms 24:10

Who is this [melek](../../strongs/h/h4428.md) of [kabowd](../../strongs/h/h3519.md)? [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), he is the [melek](../../strongs/h/h4428.md) of [kabowd](../../strongs/h/h3519.md). [Celah](../../strongs/h/h5542.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 23](psalms_23.md) - [Psalms 25](psalms_25.md)