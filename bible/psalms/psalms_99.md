# [Psalms 99](https://www.blueletterbible.org/kjv/psalms/99)

<a name="psalms_99_1"></a>Psalms 99:1

[Yĕhovah](../../strongs/h/h3068.md) [mālaḵ](../../strongs/h/h4427.md); let the ['am](../../strongs/h/h5971.md) [ragaz](../../strongs/h/h7264.md): he [yashab](../../strongs/h/h3427.md) between the [kĕruwb](../../strongs/h/h3742.md); let the ['erets](../../strongs/h/h776.md) be [nûṭ](../../strongs/h/h5120.md).

<a name="psalms_99_2"></a>Psalms 99:2

[Yĕhovah](../../strongs/h/h3068.md) is [gadowl](../../strongs/h/h1419.md) in [Tsiyown](../../strongs/h/h6726.md); and he is [ruwm](../../strongs/h/h7311.md) above all the ['am](../../strongs/h/h5971.md).

<a name="psalms_99_3"></a>Psalms 99:3

Let them [yadah](../../strongs/h/h3034.md) thy [gadowl](../../strongs/h/h1419.md) and [yare'](../../strongs/h/h3372.md) [shem](../../strongs/h/h8034.md); for it is [qadowsh](../../strongs/h/h6918.md).

<a name="psalms_99_4"></a>Psalms 99:4

The [melek](../../strongs/h/h4428.md) ['oz](../../strongs/h/h5797.md) also ['ahab](../../strongs/h/h157.md) [mishpat](../../strongs/h/h4941.md); thou dost [kuwn](../../strongs/h/h3559.md) [meyshar](../../strongs/h/h4339.md), thou ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md) in [Ya'aqob](../../strongs/h/h3290.md).

<a name="psalms_99_5"></a>Psalms 99:5

[ruwm](../../strongs/h/h7311.md) ye [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) at his [regel](../../strongs/h/h7272.md) [hăḏōm](../../strongs/h/h1916.md); for he is [qadowsh](../../strongs/h/h6918.md).

<a name="psalms_99_6"></a>Psalms 99:6

[Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) among his [kōhēn](../../strongs/h/h3548.md), and [Šᵊmû'Ēl](../../strongs/h/h8050.md) among them that [qara'](../../strongs/h/h7121.md) upon his [shem](../../strongs/h/h8034.md); they [qara'](../../strongs/h/h7121.md) upon [Yĕhovah](../../strongs/h/h3068.md), and he ['anah](../../strongs/h/h6030.md) them.

<a name="psalms_99_7"></a>Psalms 99:7

He [dabar](../../strongs/h/h1696.md) unto them in the [ʿānān](../../strongs/h/h6051.md) [ʿammûḏ](../../strongs/h/h5982.md): they [shamar](../../strongs/h/h8104.md) his [ʿēḏâ](../../strongs/h/h5713.md), and the [choq](../../strongs/h/h2706.md) that he [nathan](../../strongs/h/h5414.md) them.

<a name="psalms_99_8"></a>Psalms 99:8

Thou ['anah](../../strongs/h/h6030.md) them, [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md): thou wast an ['el](../../strongs/h/h410.md) that [nasa'](../../strongs/h/h5375.md) them, though thou tookest [naqam](../../strongs/h/h5358.md) of their ['aliylah](../../strongs/h/h5949.md).

<a name="psalms_99_9"></a>Psalms 99:9

[ruwm](../../strongs/h/h7311.md) [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) at his [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md); for [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) is [qadowsh](../../strongs/h/h6918.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 98](psalms_98.md) - [Psalms 100](psalms_100.md)