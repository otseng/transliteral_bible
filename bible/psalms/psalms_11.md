# [Psalms 11](https://www.blueletterbible.org/kjv/psa/11/1/s_489001)

<a name="psalms_11_1"></a>Psalms 11:1

To the [nāṣaḥ](../../strongs/h/h5329.md), [A Psalm] of [Dāviḏ](../../strongs/h/h1732.md). In [Yĕhovah](../../strongs/h/h3068.md) put I my [chacah](../../strongs/h/h2620.md): how ['āmar](../../strongs/h/h559.md) ye to my [nephesh](../../strongs/h/h5315.md), [nuwd](../../strongs/h/h5110.md) as a [tsippowr](../../strongs/h/h6833.md) to your [har](../../strongs/h/h2022.md)?

<a name="psalms_11_2"></a>Psalms 11:2

For, lo, the [rasha'](../../strongs/h/h7563.md) [dāraḵ](../../strongs/h/h1869.md) [qesheth](../../strongs/h/h7198.md), they [kuwn](../../strongs/h/h3559.md) their [chets](../../strongs/h/h2671.md) upon the [yeṯer](../../strongs/h/h3499.md), that they may ['ōp̄el](../../strongs/h/h652.md) [yārâ](../../strongs/h/h3384.md) at the [yashar](../../strongs/h/h3477.md) in [leb](../../strongs/h/h3820.md).

<a name="psalms_11_3"></a>Psalms 11:3

If the [shathah](../../strongs/h/h8356.md) [harac](../../strongs/h/h2040.md), what can the [tsaddiyq](../../strongs/h/h6662.md) [pa'al](../../strongs/h/h6466.md)?

<a name="psalms_11_4"></a>Psalms 11:4

[Yĕhovah](../../strongs/h/h3068.md) is in his [qodesh](../../strongs/h/h6944.md) [heykal](../../strongs/h/h1964.md), [Yĕhovah](../../strongs/h/h3068.md) [kicce'](../../strongs/h/h3678.md) is in [shamayim](../../strongs/h/h8064.md): his ['ayin](../../strongs/h/h5869.md) behold, his ['aph'aph](../../strongs/h/h6079.md) [bachan](../../strongs/h/h974.md), the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md).

<a name="psalms_11_5"></a>Psalms 11:5

[Yĕhovah](../../strongs/h/h3068.md) [bachan](../../strongs/h/h974.md) the [tsaddiyq](../../strongs/h/h6662.md): but the [rasha'](../../strongs/h/h7563.md) and him that ['ahab](../../strongs/h/h157.md) [chamac](../../strongs/h/h2555.md) his [nephesh](../../strongs/h/h5315.md) [sane'](../../strongs/h/h8130.md).

<a name="psalms_11_6"></a>Psalms 11:6

Upon the [rasha'](../../strongs/h/h7563.md) he shall [matar](../../strongs/h/h4305.md) snares, ['esh](../../strongs/h/h784.md) and [gophriyth](../../strongs/h/h1614.md), and [zal'aphah](../../strongs/h/h2152.md) [ruwach](../../strongs/h/h7307.md): this shall be the [mᵊnāṯ](../../strongs/h/h4521.md) of their [kowc](../../strongs/h/h3563.md).

<a name="psalms_11_7"></a>Psalms 11:7

For the [tsaddiyq](../../strongs/h/h6662.md) [Yĕhovah](../../strongs/h/h3068.md) ['ahab](../../strongs/h/h157.md) [ṣĕdāqāh](../../strongs/h/h6666.md); his [paniym](../../strongs/h/h6440.md) doth [chazah](../../strongs/h/h2372.md) the [yashar](../../strongs/h/h3477.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 10](psalms_10.md) - [Psalms 12](psalms_12.md)