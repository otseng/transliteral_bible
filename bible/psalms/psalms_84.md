# [Psalms 84](https://www.blueletterbible.org/kjv/psalms/84)

<a name="psalms_84_1"></a>Psalms 84:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon [gitîṯ](../../strongs/h/h1665.md), A [mizmôr](../../strongs/h/h4210.md) for the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md). How [yāḏîḏ](../../strongs/h/h3039.md) are thy [miškān](../../strongs/h/h4908.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md)!

<a name="psalms_84_2"></a>Psalms 84:2

My [nephesh](../../strongs/h/h5315.md) [kacaph](../../strongs/h/h3700.md), yea, even [kalah](../../strongs/h/h3615.md) for the [ḥāṣēr](../../strongs/h/h2691.md) of [Yĕhovah](../../strongs/h/h3068.md): my [leb](../../strongs/h/h3820.md) and my [basar](../../strongs/h/h1320.md) crieth [ranan](../../strongs/h/h7442.md) for the [chay](../../strongs/h/h2416.md) ['el](../../strongs/h/h410.md).

<a name="psalms_84_3"></a>Psalms 84:3

Yea, the [tsippowr](../../strongs/h/h6833.md) hath [māṣā'](../../strongs/h/h4672.md) a [bayith](../../strongs/h/h1004.md), and the [dᵊrôr](../../strongs/h/h1866.md) a [qēn](../../strongs/h/h7064.md) for herself, where she may [shiyth](../../strongs/h/h7896.md) her ['ep̄rōaḥ](../../strongs/h/h667.md), even thine [mizbeach](../../strongs/h/h4196.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), my [melek](../../strongs/h/h4428.md), and my ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_84_4"></a>Psalms 84:4

['esher](../../strongs/h/h835.md) are they that [yashab](../../strongs/h/h3427.md) in thy [bayith](../../strongs/h/h1004.md): they will be still [halal](../../strongs/h/h1984.md) thee. [Celah](../../strongs/h/h5542.md).

<a name="psalms_84_5"></a>Psalms 84:5

['esher](../../strongs/h/h835.md) is the ['āḏām](../../strongs/h/h120.md) whose ['oz](../../strongs/h/h5797.md) is in thee; in whose [lebab](../../strongs/h/h3824.md) are the [mĕcillah](../../strongs/h/h4546.md) of them.

<a name="psalms_84_6"></a>Psalms 84:6

Who ['abar](../../strongs/h/h5674.md) through the [ʿēmeq](../../strongs/h/h6010.md) of [Bāḵā'](../../strongs/h/h1056.md) [bāḵā'](../../strongs/h/h1057.md) [shiyth](../../strongs/h/h7896.md) it a [maʿyān](../../strongs/h/h4599.md); the [môrê](../../strongs/h/h4175.md) also [ʿāṭâ](../../strongs/h/h5844.md) the [bĕrakah](../../strongs/h/h1293.md).

<a name="psalms_84_7"></a>Psalms 84:7

They [yālaḵ](../../strongs/h/h3212.md) from [ḥayil](../../strongs/h/h2428.md) to [ḥayil](../../strongs/h/h2428.md), every one of them in [Tsiyown](../../strongs/h/h6726.md) [ra'ah](../../strongs/h/h7200.md) before ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_84_8"></a>Psalms 84:8

[Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), [shama'](../../strongs/h/h8085.md) my [tĕphillah](../../strongs/h/h8605.md): ['azan](../../strongs/h/h238.md), ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_84_9"></a>Psalms 84:9

[ra'ah](../../strongs/h/h7200.md), ['Elohiym](../../strongs/h/h430.md) our [magen](../../strongs/h/h4043.md), and [nabat](../../strongs/h/h5027.md) upon the [paniym](../../strongs/h/h6440.md) of thine [mashiyach](../../strongs/h/h4899.md).

<a name="psalms_84_10"></a>Psalms 84:10

For a [yowm](../../strongs/h/h3117.md) in thy [ḥāṣēr](../../strongs/h/h2691.md) is [towb](../../strongs/h/h2896.md) than an ['elep̄](../../strongs/h/h505.md). I had [bāḥar](../../strongs/h/h977.md) be a [sāp̄ap̄](../../strongs/h/h5605.md) in the [bayith](../../strongs/h/h1004.md) of my ['Elohiym](../../strongs/h/h430.md), than to [dûr](../../strongs/h/h1752.md) in the ['ohel](../../strongs/h/h168.md) of [resha'](../../strongs/h/h7562.md).

<a name="psalms_84_11"></a>Psalms 84:11

For [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) is a [šemeš](../../strongs/h/h8121.md) and [magen](../../strongs/h/h4043.md): [Yĕhovah](../../strongs/h/h3068.md) will [nathan](../../strongs/h/h5414.md) [ḥēn](../../strongs/h/h2580.md) and [kabowd](../../strongs/h/h3519.md): no [towb](../../strongs/h/h2896.md) thing will he [mānaʿ](../../strongs/h/h4513.md) from them that [halak](../../strongs/h/h1980.md) [tamiym](../../strongs/h/h8549.md).

<a name="psalms_84_12"></a>Psalms 84:12

[Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), ['esher](../../strongs/h/h835.md) is the ['āḏām](../../strongs/h/h120.md) that [batach](../../strongs/h/h982.md) in thee.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 83](psalms_83.md) - [Psalms 85](psalms_85.md)