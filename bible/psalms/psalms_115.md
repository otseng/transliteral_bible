# [Psalms 115](https://www.blueletterbible.org/kjv/psalms/115)

<a name="psalms_115_1"></a>Psalms 115:1

Not unto us, [Yĕhovah](../../strongs/h/h3068.md), not unto us, but unto thy [shem](../../strongs/h/h8034.md) [nathan](../../strongs/h/h5414.md) [kabowd](../../strongs/h/h3519.md), for thy [checed](../../strongs/h/h2617.md), and for thy ['emeth](../../strongs/h/h571.md) sake.

<a name="psalms_115_2"></a>Psalms 115:2

Wherefore should the [gowy](../../strongs/h/h1471.md) ['āmar](../../strongs/h/h559.md), Where is now their ['Elohiym](../../strongs/h/h430.md)?

<a name="psalms_115_3"></a>Psalms 115:3

But our ['Elohiym](../../strongs/h/h430.md) is in the [shamayim](../../strongs/h/h8064.md): he hath ['asah](../../strongs/h/h6213.md) whatsoever he hath [ḥāp̄ēṣ](../../strongs/h/h2654.md).

<a name="psalms_115_4"></a>Psalms 115:4

Their [ʿāṣāḇ](../../strongs/h/h6091.md) are [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md), the [ma'aseh](../../strongs/h/h4639.md) of ['āḏām](../../strongs/h/h120.md) [yad](../../strongs/h/h3027.md).

<a name="psalms_115_5"></a>Psalms 115:5

They have [peh](../../strongs/h/h6310.md), but they [dabar](../../strongs/h/h1696.md) not: ['ayin](../../strongs/h/h5869.md) have they, but they [ra'ah](../../strongs/h/h7200.md) not:

<a name="psalms_115_6"></a>Psalms 115:6

They have ['ozen](../../strongs/h/h241.md), but they [shama'](../../strongs/h/h8085.md) not: ['aph](../../strongs/h/h639.md) have they, but they [rîaḥ](../../strongs/h/h7306.md) not:

<a name="psalms_115_7"></a>Psalms 115:7

They have [yad](../../strongs/h/h3027.md), but they [mûš](../../strongs/h/h4184.md) not: [regel](../../strongs/h/h7272.md) have they, but they [halak](../../strongs/h/h1980.md) not: neither [hagah](../../strongs/h/h1897.md) they through their [garown](../../strongs/h/h1627.md).

<a name="psalms_115_8"></a>Psalms 115:8

They that ['asah](../../strongs/h/h6213.md) them are like unto them; so is every one that [batach](../../strongs/h/h982.md) in them.

<a name="psalms_115_9"></a>Psalms 115:9

[Yisra'el](../../strongs/h/h3478.md), [batach](../../strongs/h/h982.md) thou in [Yĕhovah](../../strongs/h/h3068.md): he is their ['ezer](../../strongs/h/h5828.md) and their [magen](../../strongs/h/h4043.md).

<a name="psalms_115_10"></a>Psalms 115:10

[bayith](../../strongs/h/h1004.md) of ['Ahărôn](../../strongs/h/h175.md), [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md): he is their ['ezer](../../strongs/h/h5828.md) and their [magen](../../strongs/h/h4043.md).

<a name="psalms_115_11"></a>Psalms 115:11

Ye that [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md), [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md): he is their ['ezer](../../strongs/h/h5828.md) and their [magen](../../strongs/h/h4043.md).

<a name="psalms_115_12"></a>Psalms 115:12

[Yĕhovah](../../strongs/h/h3068.md) hath been [zakar](../../strongs/h/h2142.md) of us: he will [barak](../../strongs/h/h1288.md) us; he will [barak](../../strongs/h/h1288.md) the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md); he will [barak](../../strongs/h/h1288.md) the [bayith](../../strongs/h/h1004.md) of ['Ahărôn](../../strongs/h/h175.md).

<a name="psalms_115_13"></a>Psalms 115:13

He will [barak](../../strongs/h/h1288.md) them that [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md), both [qāṭān](../../strongs/h/h6996.md) and [gadowl](../../strongs/h/h1419.md).

<a name="psalms_115_14"></a>Psalms 115:14

[Yĕhovah](../../strongs/h/h3068.md) shall [yāsap̄](../../strongs/h/h3254.md) you more and more, you and your [ben](../../strongs/h/h1121.md).

<a name="psalms_115_15"></a>Psalms 115:15

Ye are [barak](../../strongs/h/h1288.md) of [Yĕhovah](../../strongs/h/h3068.md) which ['asah](../../strongs/h/h6213.md) [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md).

<a name="psalms_115_16"></a>Psalms 115:16

The [shamayim](../../strongs/h/h8064.md), even the [shamayim](../../strongs/h/h8064.md), are [Yĕhovah](../../strongs/h/h3068.md): but the ['erets](../../strongs/h/h776.md) hath he [nathan](../../strongs/h/h5414.md) to the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md).

<a name="psalms_115_17"></a>Psalms 115:17

The [muwth](../../strongs/h/h4191.md) [halal](../../strongs/h/h1984.md) not the [Yahh](../../strongs/h/h3050.md), neither any that [yarad](../../strongs/h/h3381.md) into [dûmâ](../../strongs/h/h1745.md).

<a name="psalms_115_18"></a>Psalms 115:18

But we will [barak](../../strongs/h/h1288.md) the [Yahh](../../strongs/h/h3050.md) from this time forth and [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md). [halal](../../strongs/h/h1984.md) the [Yahh](../../strongs/h/h3050.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 114](psalms_114.md) - [Psalms 116](psalms_116.md)