# [Psalms 38](https://www.blueletterbible.org/kjv/psa/38/1/)

<a name="psalms_38_1"></a>Psalms 38:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md), to [zakar](../../strongs/h/h2142.md). [Yĕhovah](../../strongs/h/h3068.md), [yakach](../../strongs/h/h3198.md) me not in thy [qeṣep̄](../../strongs/h/h7110.md): neither [yacar](../../strongs/h/h3256.md) me in thy [chemah](../../strongs/h/h2534.md).

<a name="psalms_38_2"></a>Psalms 38:2

For thine [chets](../../strongs/h/h2671.md) [nāḥaṯ](../../strongs/h/h5181.md) in me, and thy [yad](../../strongs/h/h3027.md) [nāḥaṯ](../../strongs/h/h5181.md).

<a name="psalms_38_3"></a>Psalms 38:3

There is no [mᵊṯōm](../../strongs/h/h4974.md) in my [basar](../../strongs/h/h1320.md) because of thine [zaʿam](../../strongs/h/h2195.md); neither is there any [shalowm](../../strongs/h/h7965.md) in my ['etsem](../../strongs/h/h6106.md) because of my [chatta'ath](../../strongs/h/h2403.md).

<a name="psalms_38_4"></a>Psalms 38:4

For mine ['avon](../../strongs/h/h5771.md) are ['abar](../../strongs/h/h5674.md) mine [ro'sh](../../strongs/h/h7218.md): as a [kāḇēḏ](../../strongs/h/h3515.md) [maśśā'](../../strongs/h/h4853.md) they are too [kabad](../../strongs/h/h3513.md) for me.

<a name="psalms_38_5"></a>Psalms 38:5

My [ḥabûrâ](../../strongs/h/h2250.md) [bā'aš](../../strongs/h/h887.md) and are [māqaq](../../strongs/h/h4743.md) [paniym](../../strongs/h/h6440.md) of my ['iûeleṯ](../../strongs/h/h200.md).

<a name="psalms_38_6"></a>Psalms 38:6

I am [ʿāvâ](../../strongs/h/h5753.md); I am [shachach](../../strongs/h/h7817.md) [me'od](../../strongs/h/h3966.md); I [halak](../../strongs/h/h1980.md) [qāḏar](../../strongs/h/h6937.md) all the [yowm](../../strongs/h/h3117.md) long.

<a name="psalms_38_7"></a>Psalms 38:7

For my [kesel](../../strongs/h/h3689.md) are [mālā'](../../strongs/h/h4390.md) with a [qālâ](../../strongs/h/h7033.md) disease: and there is no [mᵊṯōm](../../strongs/h/h4974.md) in my [basar](../../strongs/h/h1320.md).

<a name="psalms_38_8"></a>Psalms 38:8

I am [pûḡ](../../strongs/h/h6313.md) and [me'od](../../strongs/h/h3966.md) [dakah](../../strongs/h/h1794.md): I have [šā'aḡ](../../strongs/h/h7580.md) by reason of the [nᵊhāmâ](../../strongs/h/h5100.md) of my [leb](../../strongs/h/h3820.md).

<a name="psalms_38_9"></a>Psalms 38:9

['adonay](../../strongs/h/h136.md), all my [ta'avah](../../strongs/h/h8378.md) is before thee; and my ['anachah](../../strongs/h/h585.md) is not [cathar](../../strongs/h/h5641.md) from thee.

<a name="psalms_38_10"></a>Psalms 38:10

My [leb](../../strongs/h/h3820.md) [sāḥar](../../strongs/h/h5503.md), my [koach](../../strongs/h/h3581.md) ['azab](../../strongs/h/h5800.md) me: as for the ['owr](../../strongs/h/h216.md) of mine ['ayin](../../strongs/h/h5869.md), it also is ['în](../../strongs/h/h369.md) from me.

<a name="psalms_38_11"></a>Psalms 38:11

My ['ahab](../../strongs/h/h157.md) and my [rea'](../../strongs/h/h7453.md) ['amad](../../strongs/h/h5975.md) aloof from my [neḡaʿ](../../strongs/h/h5061.md); and my [qarowb](../../strongs/h/h7138.md) ['amad](../../strongs/h/h5975.md) [rachowq](../../strongs/h/h7350.md).

<a name="psalms_38_12"></a>Psalms 38:12

They also that [bāqaš](../../strongs/h/h1245.md) after my [nephesh](../../strongs/h/h5315.md) [nāqaš](../../strongs/h/h5367.md) for me: and they that [darash](../../strongs/h/h1875.md) my [ra'](../../strongs/h/h7451.md) [dabar](../../strongs/h/h1696.md) [havvah](../../strongs/h/h1942.md), and [hagah](../../strongs/h/h1897.md) [mirmah](../../strongs/h/h4820.md) all the [yowm](../../strongs/h/h3117.md) long.

<a name="psalms_38_13"></a>Psalms 38:13

But I, as a [ḥērēš](../../strongs/h/h2795.md), [shama'](../../strongs/h/h8085.md) not; and I was as an ['illēm](../../strongs/h/h483.md) that [pāṯaḥ](../../strongs/h/h6605.md) not his [peh](../../strongs/h/h6310.md).

<a name="psalms_38_14"></a>Psalms 38:14

Thus I was as an ['iysh](../../strongs/h/h376.md) that [shama'](../../strongs/h/h8085.md) not, and in whose [peh](../../strongs/h/h6310.md) are no [tôḵēḥâ](../../strongs/h/h8433.md).

<a name="psalms_38_15"></a>Psalms 38:15

For in thee, [Yĕhovah](../../strongs/h/h3068.md), do I [yāḥal](../../strongs/h/h3176.md): thou wilt ['anah](../../strongs/h/h6030.md), O ['adonay](../../strongs/h/h136.md) my ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_38_16"></a>Psalms 38:16

For I ['āmar](../../strongs/h/h559.md), Hear me, lest otherwise they should [samach](../../strongs/h/h8055.md) over me: when my [regel](../../strongs/h/h7272.md) [mowt](../../strongs/h/h4131.md), they [gāḏal](../../strongs/h/h1431.md) themselves against me.

<a name="psalms_38_17"></a>Psalms 38:17

For I am [kuwn](../../strongs/h/h3559.md) to [ṣelaʿ](../../strongs/h/h6761.md), and my [maḵ'ōḇ](../../strongs/h/h4341.md) is [tāmîḏ](../../strongs/h/h8548.md) before me.

<a name="psalms_38_18"></a>Psalms 38:18

For I will [nāḡaḏ](../../strongs/h/h5046.md) mine ['avon](../../strongs/h/h5771.md); I will be [dā'aḡ](../../strongs/h/h1672.md) for my [chatta'ath](../../strongs/h/h2403.md).

<a name="psalms_38_19"></a>Psalms 38:19

But mine ['oyeb](../../strongs/h/h341.md) are [chay](../../strongs/h/h2416.md), and they are [ʿāṣam](../../strongs/h/h6105.md): and they that [sane'](../../strongs/h/h8130.md) me [sheqer](../../strongs/h/h8267.md) are [rabab](../../strongs/h/h7231.md).

<a name="psalms_38_20"></a>Psalms 38:20

They also that [shalam](../../strongs/h/h7999.md) [ra'](../../strongs/h/h7451.md) for [towb](../../strongs/h/h2896.md) are mine [śāṭan](../../strongs/h/h7853.md); because I [radaph](../../strongs/h/h7291.md) the thing that [towb](../../strongs/h/h2896.md) is.

<a name="psalms_38_21"></a>Psalms 38:21

['azab](../../strongs/h/h5800.md) me not, [Yĕhovah](../../strongs/h/h3068.md): ['Elohiym](../../strongs/h/h430.md), be not [rachaq](../../strongs/h/h7368.md) from me.

<a name="psalms_38_22"></a>Psalms 38:22

[ḥûš](../../strongs/h/h2363.md) to [ʿezrâ](../../strongs/h/h5833.md) me, O ['adonay](../../strongs/h/h136.md) my [tᵊšûʿâ](../../strongs/h/h8668.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 37](psalms_37.md) - [Psalms 39](psalms_39.md)