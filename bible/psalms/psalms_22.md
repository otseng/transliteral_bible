# [Psalms 22](https://www.blueletterbible.org/kjv/psa/22/1/s_500001)

<a name="psalms_22_1"></a>Psalms 22:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon ['ayyeleṯ](../../strongs/h/h365.md) [šaḥar](../../strongs/h/h7837.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). ['el](../../strongs/h/h410.md), ['el](../../strongs/h/h410.md), why hast thou ['azab](../../strongs/h/h5800.md) me? why art thou so far from [yĕshuw'ah](../../strongs/h/h3444.md) me, and from the [dabar](../../strongs/h/h1697.md) of my [šᵊ'āḡâ](../../strongs/h/h7581.md)?

<a name="psalms_22_2"></a>Psalms 22:2

['Elohiym](../../strongs/h/h430.md), I [qara'](../../strongs/h/h7121.md) in [yômām](../../strongs/h/h3119.md), but thou ['anah](../../strongs/h/h6030.md) not; and in [layil](../../strongs/h/h3915.md), and am not [dûmîyâ](../../strongs/h/h1747.md).

<a name="psalms_22_3"></a>Psalms 22:3

But thou art [qadowsh](../../strongs/h/h6918.md), O thou that [yashab](../../strongs/h/h3427.md) the [tehillah](../../strongs/h/h8416.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="psalms_22_4"></a>Psalms 22:4

Our ['ab](../../strongs/h/h1.md) [batach](../../strongs/h/h982.md) in thee: they [batach](../../strongs/h/h982.md), and thou didst [palat](../../strongs/h/h6403.md) them.

<a name="psalms_22_5"></a>Psalms 22:5

They [zāʿaq](../../strongs/h/h2199.md) unto thee, and were [mālaṭ](../../strongs/h/h4422.md): they [batach](../../strongs/h/h982.md) in thee, and were not [buwsh](../../strongs/h/h954.md).

<a name="psalms_22_6"></a>Psalms 22:6

But I am a [tôlāʿ](../../strongs/h/h8438.md), and no ['iysh](../../strongs/h/h376.md); a [cherpah](../../strongs/h/h2781.md) of ['adam](../../strongs/h/h120.md), and [bazah](../../strongs/h/h959.md) of the ['am](../../strongs/h/h5971.md).

<a name="psalms_22_7"></a>Psalms 22:7

All they that [ra'ah](../../strongs/h/h7200.md) me [lāʿaḡ](../../strongs/h/h3932.md): they [p̄ṭvr](../../strongs/h/h6358.md) the [saphah](../../strongs/h/h8193.md), they [nûaʿ](../../strongs/h/h5128.md) the [ro'sh](../../strongs/h/h7218.md), saying,

<a name="psalms_22_8"></a>Psalms 22:8

He [gālal](../../strongs/h/h1556.md) on [Yĕhovah](../../strongs/h/h3068.md) that he would [palat](../../strongs/h/h6403.md) him: let him [natsal](../../strongs/h/h5337.md) him, seeing he [ḥāp̄ēṣ](../../strongs/h/h2654.md) in him.

<a name="psalms_22_9"></a>Psalms 22:9

But thou art he that [gîaḥ](../../strongs/h/h1518.md) me out of the [beten](../../strongs/h/h990.md): thou didst make me [batach](../../strongs/h/h982.md) when I was upon my ['em](../../strongs/h/h517.md) [šaḏ](../../strongs/h/h7699.md).

<a name="psalms_22_10"></a>Psalms 22:10

I was [shalak](../../strongs/h/h7993.md) upon thee from the [reḥem](../../strongs/h/h7358.md): thou art my ['el](../../strongs/h/h410.md) from my ['em](../../strongs/h/h517.md) [beten](../../strongs/h/h990.md).

<a name="psalms_22_11"></a>Psalms 22:11

Be not [rachaq](../../strongs/h/h7368.md) from me; for [tsarah](../../strongs/h/h6869.md) is [qarowb](../../strongs/h/h7138.md); for there is none to [ʿāzar](../../strongs/h/h5826.md).

<a name="psalms_22_12"></a>Psalms 22:12

Many [par](../../strongs/h/h6499.md) have [cabab](../../strongs/h/h5437.md) me: ['abîr](../../strongs/h/h47.md) bulls of [Bāšān](../../strongs/h/h1316.md) have [kāṯar](../../strongs/h/h3803.md).

<a name="psalms_22_13"></a>Psalms 22:13

They [pāṣâ](../../strongs/h/h6475.md) upon me with their [peh](../../strongs/h/h6310.md), as a [taraph](../../strongs/h/h2963.md) and a [šā'aḡ](../../strongs/h/h7580.md) ['ariy](../../strongs/h/h738.md).

<a name="psalms_22_14"></a>Psalms 22:14

I am [šāp̄aḵ](../../strongs/h/h8210.md) like [mayim](../../strongs/h/h4325.md), and all my ['etsem](../../strongs/h/h6106.md) are [pāraḏ](../../strongs/h/h6504.md): my [leb](../../strongs/h/h3820.md) is like [dônāḡ](../../strongs/h/h1749.md); it is [māsas](../../strongs/h/h4549.md) in the midst of my [me'ah](../../strongs/h/h4578.md).

<a name="psalms_22_15"></a>Psalms 22:15

My [koach](../../strongs/h/h3581.md) is [yāḇēš](../../strongs/h/h3001.md) like a [ḥereś](../../strongs/h/h2789.md); and my [lashown](../../strongs/h/h3956.md) [dāḇaq](../../strongs/h/h1692.md) to my [malqôaḥ](../../strongs/h/h4455.md); and thou hast [šāp̄aṯ](../../strongs/h/h8239.md) me into the ['aphar](../../strongs/h/h6083.md) of [maveth](../../strongs/h/h4194.md).

<a name="psalms_22_16"></a>Psalms 22:16

For [keleḇ](../../strongs/h/h3611.md) have [cabab](../../strongs/h/h5437.md) me: the ['edah](../../strongs/h/h5712.md) of the [ra'a'](../../strongs/h/h7489.md) have [naqaph](../../strongs/h/h5362.md) me: they [karah](../../strongs/h/h3738.md) ['ariy](../../strongs/h/h738.md) my [yad](../../strongs/h/h3027.md) and my [regel](../../strongs/h/h7272.md).

<a name="psalms_22_17"></a>Psalms 22:17

I may [sāp̄ar](../../strongs/h/h5608.md) all my ['etsem](../../strongs/h/h6106.md): they [nabat](../../strongs/h/h5027.md) and [ra'ah](../../strongs/h/h7200.md) upon me.

<a name="psalms_22_18"></a>Psalms 22:18

They [chalaq](../../strongs/h/h2505.md) my [beḡeḏ](../../strongs/h/h899.md) among them, and [naphal](../../strongs/h/h5307.md) [gôrāl](../../strongs/h/h1486.md) upon my [lᵊḇûš](../../strongs/h/h3830.md).

<a name="psalms_22_19"></a>Psalms 22:19

But be not thou [rachaq](../../strongs/h/h7368.md) from me, [Yĕhovah](../../strongs/h/h3068.md): O my ['ĕyālûṯ](../../strongs/h/h360.md), [ḥûš](../../strongs/h/h2363.md) thee to [ʿezrâ](../../strongs/h/h5833.md) me.

<a name="psalms_22_20"></a>Psalms 22:20

[natsal](../../strongs/h/h5337.md) my [nephesh](../../strongs/h/h5315.md) from the [chereb](../../strongs/h/h2719.md); my [yāḥîḏ](../../strongs/h/h3173.md) from the [yad](../../strongs/h/h3027.md) of the [keleḇ](../../strongs/h/h3611.md).

<a name="psalms_22_21"></a>Psalms 22:21

[yasha'](../../strongs/h/h3467.md) me from the ['ariy](../../strongs/h/h738.md) [peh](../../strongs/h/h6310.md): for thou hast ['anah](../../strongs/h/h6030.md) me from the [qeren](../../strongs/h/h7161.md) of the [rᵊ'ēm](../../strongs/h/h7214.md).

<a name="psalms_22_22"></a>Psalms 22:22

I will [sāp̄ar](../../strongs/h/h5608.md) thy [shem](../../strongs/h/h8034.md) unto my ['ach](../../strongs/h/h251.md): in the [tavek](../../strongs/h/h8432.md) of the [qāhēl](../../strongs/h/h6951.md) will I [halal](../../strongs/h/h1984.md) thee.

<a name="psalms_22_23"></a>Psalms 22:23

Ye that [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md), [halal](../../strongs/h/h1984.md) him; all ye the [zera'](../../strongs/h/h2233.md) of [Ya'aqob](../../strongs/h/h3290.md), [kabad](../../strongs/h/h3513.md) him; and [guwr](../../strongs/h/h1481.md) him, all ye the [zera'](../../strongs/h/h2233.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="psalms_22_24"></a>Psalms 22:24

For he hath not [bazah](../../strongs/h/h959.md) nor [šāqaṣ](../../strongs/h/h8262.md) the [ʿĕnûṯ](../../strongs/h/h6039.md) of the ['aniy](../../strongs/h/h6041.md); neither hath he [cathar](../../strongs/h/h5641.md) his [paniym](../../strongs/h/h6440.md) from him; but when he [šāvaʿ](../../strongs/h/h7768.md) unto him, he [shama'](../../strongs/h/h8085.md).

<a name="psalms_22_25"></a>Psalms 22:25

My [tehillah](../../strongs/h/h8416.md) shall be of thee in the [rab](../../strongs/h/h7227.md) [qāhēl](../../strongs/h/h6951.md): I will [shalam](../../strongs/h/h7999.md) my [neḏer](../../strongs/h/h5088.md) before them that [yārē'](../../strongs/h/h3373.md) him.

<a name="psalms_22_26"></a>Psalms 22:26

The ['anav](../../strongs/h/h6035.md) shall ['akal](../../strongs/h/h398.md) and be [sāׂbaʿ](../../strongs/h/h7646.md): they shall [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md) that [darash](../../strongs/h/h1875.md) him: your [lebab](../../strongs/h/h3824.md) shall [ḥāyâ](../../strongs/h/h2421.md) for ever.

<a name="psalms_22_27"></a>Psalms 22:27

All the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md) shall [zakar](../../strongs/h/h2142.md) and [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md): and all the [mišpāḥâ](../../strongs/h/h4940.md) of the [gowy](../../strongs/h/h1471.md) shall [shachah](../../strongs/h/h7812.md) before thee.

<a name="psalms_22_28"></a>Psalms 22:28

For the [mᵊlûḵâ](../../strongs/h/h4410.md) is [Yĕhovah](../../strongs/h/h3068.md): and he is the [mashal](../../strongs/h/h4910.md) among the [gowy](../../strongs/h/h1471.md).

<a name="psalms_22_29"></a>Psalms 22:29

All they that be [dāšēn](../../strongs/h/h1879.md) upon ['erets](../../strongs/h/h776.md) shall ['akal](../../strongs/h/h398.md) and [shachah](../../strongs/h/h7812.md): all they that go down to the ['aphar](../../strongs/h/h6083.md) shall [kara'](../../strongs/h/h3766.md) before him: and none can keep [ḥāyâ](../../strongs/h/h2421.md) his own [nephesh](../../strongs/h/h5315.md).

<a name="psalms_22_30"></a>Psalms 22:30

A [zera'](../../strongs/h/h2233.md) shall ['abad](../../strongs/h/h5647.md) him; it shall be [sāp̄ar](../../strongs/h/h5608.md) to ['adonay](../../strongs/h/h136.md) for a [dôr](../../strongs/h/h1755.md).

<a name="psalms_22_31"></a>Psalms 22:31

They shall [bow'](../../strongs/h/h935.md), and shall [nāḡaḏ](../../strongs/h/h5046.md) his [tsedaqah](../../strongs/h/h6666.md) unto an ['am](../../strongs/h/h5971.md) that shall be [yalad](../../strongs/h/h3205.md), that he hath ['asah](../../strongs/h/h6213.md) this.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 21](psalms_21.md) - [Psalms 23](psalms_23.md)