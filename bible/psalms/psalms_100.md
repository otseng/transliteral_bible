# [Psalms 100](https://www.blueletterbible.org/kjv/psa/100/1/s_578001)

<a name="psalms_100_1"></a>Psalms 100:1
A [mizmôr](../../strongs/h/h4210.md) of [tôḏâ](../../strongs/h/h8426.md). Make a [rûaʿ](../../strongs/h/h7321.md) unto [Yĕhovah](../../strongs/h/h3068.md), all ye ['erets](../../strongs/h/h776.md).

<a name="psalms_100_2"></a>Psalms 100:2

['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) with [simchah](../../strongs/h/h8057.md): [bow'](../../strongs/h/h935.md) before his [paniym](../../strongs/h/h6440.md) with [rᵊnānâ](../../strongs/h/h7445.md).

<a name="psalms_100_3"></a>Psalms 100:3

[yada'](../../strongs/h/h3045.md) ye that [Yĕhovah](../../strongs/h/h3068.md) he is ['Elohiym](../../strongs/h/h430.md): it is he that hath ['asah](../../strongs/h/h6213.md) us, and not we ourselves; we are his ['am](../../strongs/h/h5971.md), and the [tso'n](../../strongs/h/h6629.md) of his [marʿîṯ](../../strongs/h/h4830.md).

<a name="psalms_100_4"></a>Psalms 100:4

[bow'](../../strongs/h/h935.md) into his [sha'ar](../../strongs/h/h8179.md) with [tôḏâ](../../strongs/h/h8426.md), and into his [ḥāṣēr](../../strongs/h/h2691.md) with [tehillah](../../strongs/h/h8416.md): be [yadah](../../strongs/h/h3034.md) unto him, and [barak](../../strongs/h/h1288.md) his [shem](../../strongs/h/h8034.md).

<a name="psalms_100_5"></a>Psalms 100:5

For [Yĕhovah](../../strongs/h/h3068.md) is [towb](../../strongs/h/h2896.md); his [checed](../../strongs/h/h2617.md) is ['owlam](../../strongs/h/h5769.md); and his ['ĕmûnâ](../../strongs/h/h530.md) [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 99](psalms_99.md) - [Psalms 101](psalms_101.md)