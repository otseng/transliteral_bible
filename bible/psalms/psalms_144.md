# [Psalms 144](https://www.blueletterbible.org/kjv/psalms/144)

<a name="psalms_144_1"></a>Psalms 144:1

A Psalm of [Dāviḏ](../../strongs/h/h1732.md). [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) my [tsuwr](../../strongs/h/h6697.md), which [lamad](../../strongs/h/h3925.md) my [yad](../../strongs/h/h3027.md) to [qᵊrāḇ](../../strongs/h/h7128.md), and my ['etsba'](../../strongs/h/h676.md) to [milḥāmâ](../../strongs/h/h4421.md):

<a name="psalms_144_2"></a>Psalms 144:2

My [checed](../../strongs/h/h2617.md), and my [matsuwd](../../strongs/h/h4686.md); my [misgab](../../strongs/h/h4869.md), and my [palat](../../strongs/h/h6403.md); my [magen](../../strongs/h/h4043.md), and he in whom I [chacah](../../strongs/h/h2620.md); who [rāḏaḏ](../../strongs/h/h7286.md) my ['am](../../strongs/h/h5971.md) under me.

<a name="psalms_144_3"></a>Psalms 144:3

[Yĕhovah](../../strongs/h/h3068.md), what is ['āḏām](../../strongs/h/h120.md), that thou [yada'](../../strongs/h/h3045.md) of him! or the [ben](../../strongs/h/h1121.md) of ['enowsh](../../strongs/h/h582.md), that thou [chashab](../../strongs/h/h2803.md) of him!

<a name="psalms_144_4"></a>Psalms 144:4

['āḏām](../../strongs/h/h120.md) is [dāmâ](../../strongs/h/h1819.md) to [heḇel](../../strongs/h/h1892.md): his [yowm](../../strongs/h/h3117.md) are as a [ṣēl](../../strongs/h/h6738.md) that ['abar](../../strongs/h/h5674.md).

<a name="psalms_144_5"></a>Psalms 144:5

[natah](../../strongs/h/h5186.md) thy [shamayim](../../strongs/h/h8064.md), [Yĕhovah](../../strongs/h/h3068.md), and [yarad](../../strongs/h/h3381.md): [naga'](../../strongs/h/h5060.md) the [har](../../strongs/h/h2022.md), and they shall [ʿāšēn](../../strongs/h/h6225.md).

<a name="psalms_144_6"></a>Psalms 144:6

[bāraq](../../strongs/h/h1299.md) [baraq](../../strongs/h/h1300.md), and [puwts](../../strongs/h/h6327.md) them: [shalach](../../strongs/h/h7971.md) thine [chets](../../strongs/h/h2671.md), and [hāmam](../../strongs/h/h2000.md) them.

<a name="psalms_144_7"></a>Psalms 144:7

[shalach](../../strongs/h/h7971.md) thine [yad](../../strongs/h/h3027.md) from [marowm](../../strongs/h/h4791.md); [pāṣâ](../../strongs/h/h6475.md) me, and [natsal](../../strongs/h/h5337.md) me out of [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md), from the [yad](../../strongs/h/h3027.md) of [nēḵār](../../strongs/h/h5236.md) [ben](../../strongs/h/h1121.md);

<a name="psalms_144_8"></a>Psalms 144:8

Whose [peh](../../strongs/h/h6310.md) [dabar](../../strongs/h/h1696.md) [shav'](../../strongs/h/h7723.md), and their [yamiyn](../../strongs/h/h3225.md) is a [yamiyn](../../strongs/h/h3225.md) of [sheqer](../../strongs/h/h8267.md).

<a name="psalms_144_9"></a>Psalms 144:9

I will [shiyr](../../strongs/h/h7891.md) a [ḥāḏāš](../../strongs/h/h2319.md) [šîr](../../strongs/h/h7892.md) unto thee, ['Elohiym](../../strongs/h/h430.md): upon a [neḇel](../../strongs/h/h5035.md) and an [ʿāśôr](../../strongs/h/h6218.md) will I [zamar](../../strongs/h/h2167.md) unto thee.

<a name="psalms_144_10"></a>Psalms 144:10

It is he that [nathan](../../strongs/h/h5414.md) [tᵊšûʿâ](../../strongs/h/h8668.md) unto [melek](../../strongs/h/h4428.md): who [pāṣâ](../../strongs/h/h6475.md) [Dāviḏ](../../strongs/h/h1732.md) his ['ebed](../../strongs/h/h5650.md) from the [ra'](../../strongs/h/h7451.md) [chereb](../../strongs/h/h2719.md).

<a name="psalms_144_11"></a>Psalms 144:11

[pāṣâ](../../strongs/h/h6475.md) me, and [natsal](../../strongs/h/h5337.md) me from the [yad](../../strongs/h/h3027.md) of [nēḵār](../../strongs/h/h5236.md) [ben](../../strongs/h/h1121.md), whose [peh](../../strongs/h/h6310.md) [dabar](../../strongs/h/h1696.md) [shav'](../../strongs/h/h7723.md), and their [yamiyn](../../strongs/h/h3225.md) is a [yamiyn](../../strongs/h/h3225.md) of [sheqer](../../strongs/h/h8267.md):

<a name="psalms_144_12"></a>Psalms 144:12

That our [ben](../../strongs/h/h1121.md) may be as [nāṭîaʿ](../../strongs/h/h5195.md) [gāḏal](../../strongs/h/h1431.md) in their [nāʿur](../../strongs/h/h5271.md); that our [bath](../../strongs/h/h1323.md) may be as [zāvîṯ](../../strongs/h/h2106.md), [ḥāṭaḇ](../../strongs/h/h2404.md) after the [taḇnîṯ](../../strongs/h/h8403.md) of a [heykal](../../strongs/h/h1964.md):

<a name="psalms_144_13"></a>Psalms 144:13

That our [mezev](../../strongs/h/h4200.md) may be [mālē'](../../strongs/h/h4392.md), [pûq](../../strongs/h/h6329.md) all manner of [zan](../../strongs/h/h2177.md) [zan](../../strongs/h/h2177.md): that our [tso'n](../../strongs/h/h6629.md) may ['ālap̄](../../strongs/h/h503.md) and [rabab](../../strongs/h/h7231.md) in our [ḥûṣ](../../strongs/h/h2351.md):

<a name="psalms_144_14"></a>Psalms 144:14

That our ['allûp̄](../../strongs/h/h441.md) may be strong to [sāḇal](../../strongs/h/h5445.md); that there be no [pereṣ](../../strongs/h/h6556.md), nor going [yāṣā'](../../strongs/h/h3318.md); that there be no [ṣᵊvāḥâ](../../strongs/h/h6682.md) in our [rᵊḥōḇ](../../strongs/h/h7339.md).

<a name="psalms_144_15"></a>Psalms 144:15

['esher](../../strongs/h/h835.md) is that ['am](../../strongs/h/h5971.md), that is in such a [kāḵâ](../../strongs/h/h3602.md): yea, ['esher](../../strongs/h/h835.md) is that ['am](../../strongs/h/h5971.md), whose ['Elohiym](../../strongs/h/h430.md) is [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 143](psalms_143.md) - [Psalms 145](psalms_145.md)