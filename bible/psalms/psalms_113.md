# [Psalms 113](https://www.blueletterbible.org/kjv/psalms/113)

<a name="psalms_113_1"></a>Psalms 113:1

[halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md). [halal](../../strongs/h/h1984.md), O ye ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md), [halal](../../strongs/h/h1984.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_113_2"></a>Psalms 113:2

[barak](../../strongs/h/h1288.md) be the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) from [ʿatâ](../../strongs/h/h6258.md) and [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md).

<a name="psalms_113_3"></a>Psalms 113:3

From the [mizrach](../../strongs/h/h4217.md) of the [šemeš](../../strongs/h/h8121.md) unto the [māḇô'](../../strongs/h/h3996.md) of the same [Yĕhovah](../../strongs/h/h3068.md) [shem](../../strongs/h/h8034.md) is to be [halal](../../strongs/h/h1984.md).

<a name="psalms_113_4"></a>Psalms 113:4

[Yĕhovah](../../strongs/h/h3068.md) is [ruwm](../../strongs/h/h7311.md) above all [gowy](../../strongs/h/h1471.md), and his [kabowd](../../strongs/h/h3519.md) above the [shamayim](../../strongs/h/h8064.md).

<a name="psalms_113_5"></a>Psalms 113:5

Who is like unto [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), who [yashab](../../strongs/h/h3427.md) on [gāḇah](../../strongs/h/h1361.md),

<a name="psalms_113_6"></a>Psalms 113:6

Who [šāp̄ēl](../../strongs/h/h8213.md) himself to [ra'ah](../../strongs/h/h7200.md) the things that are in [shamayim](../../strongs/h/h8064.md), and in the ['erets](../../strongs/h/h776.md)!

<a name="psalms_113_7"></a>Psalms 113:7

He [quwm](../../strongs/h/h6965.md) the [dal](../../strongs/h/h1800.md) out of the ['aphar](../../strongs/h/h6083.md), and [ruwm](../../strongs/h/h7311.md) the ['ebyown](../../strongs/h/h34.md) out of the ['ašpōṯ](../../strongs/h/h830.md);

<a name="psalms_113_8"></a>Psalms 113:8

That he may [yashab](../../strongs/h/h3427.md) him with [nāḏîḇ](../../strongs/h/h5081.md), even with the [nāḏîḇ](../../strongs/h/h5081.md) of his ['am](../../strongs/h/h5971.md).

<a name="psalms_113_9"></a>Psalms 113:9

He maketh the [ʿāqār](../../strongs/h/h6135.md) to [yashab](../../strongs/h/h3427.md) [bayith](../../strongs/h/h1004.md), and to be a [śāmēaḥ](../../strongs/h/h8056.md) ['em](../../strongs/h/h517.md) of [ben](../../strongs/h/h1121.md). [halal](../../strongs/h/h1984.md) ye the [Yahh](../../strongs/h/h3050.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 112](psalms_112.md) - [Psalms 114](psalms_114.md)