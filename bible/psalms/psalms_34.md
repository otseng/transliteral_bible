# [Psalms 34](https://www.blueletterbible.org/kjv/psa/34/1/s_512001)

<a name="psalms_34_1"></a>Psalms 34:1

A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md), when he [šānâ](../../strongs/h/h8138.md) his [ṭaʿam](../../strongs/h/h2940.md) before ['Ăḇîmeleḵ](../../strongs/h/h40.md); who [gāraš](../../strongs/h/h1644.md) him, and he [yālaḵ](../../strongs/h/h3212.md). I will [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md) at all times: his [tehillah](../../strongs/h/h8416.md) shall [tāmîḏ](../../strongs/h/h8548.md) be in my [peh](../../strongs/h/h6310.md).

<a name="psalms_34_2"></a>Psalms 34:2

My [nephesh](../../strongs/h/h5315.md) shall make her [halal](../../strongs/h/h1984.md) in [Yĕhovah](../../strongs/h/h3068.md): the ['anav](../../strongs/h/h6035.md) shall [shama'](../../strongs/h/h8085.md) thereof, and be [samach](../../strongs/h/h8055.md).

<a name="psalms_34_3"></a>Psalms 34:3

O [gāḏal](../../strongs/h/h1431.md) [Yĕhovah](../../strongs/h/h3068.md) with me, and let us [ruwm](../../strongs/h/h7311.md) his [shem](../../strongs/h/h8034.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="psalms_34_4"></a>Psalms 34:4

I [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md), and he ['anah](../../strongs/h/h6030.md) me, and [natsal](../../strongs/h/h5337.md) me from all my [mᵊḡûrâ](../../strongs/h/h4035.md).

<a name="psalms_34_5"></a>Psalms 34:5

They [nabat](../../strongs/h/h5027.md) unto him, and were [nāhar](../../strongs/h/h5102.md): and their [paniym](../../strongs/h/h6440.md) were not [ḥāp̄ēr](../../strongs/h/h2659.md).

<a name="psalms_34_6"></a>Psalms 34:6

This ['aniy](../../strongs/h/h6041.md) [qara'](../../strongs/h/h7121.md), and [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) him, and [yasha'](../../strongs/h/h3467.md) him out of all his [tsarah](../../strongs/h/h6869.md).

<a name="psalms_34_7"></a>Psalms 34:7

The [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [ḥānâ](../../strongs/h/h2583.md) [cabiyb](../../strongs/h/h5439.md) them that [yārē'](../../strongs/h/h3373.md) him, and [chalats](../../strongs/h/h2502.md) them.

<a name="psalms_34_8"></a>Psalms 34:8

O [ṭāʿam](../../strongs/h/h2938.md) and [ra'ah](../../strongs/h/h7200.md) that [Yĕhovah](../../strongs/h/h3068.md) is [towb](../../strongs/h/h2896.md): ['esher](../../strongs/h/h835.md) is the [geḇer](../../strongs/h/h1397.md) that [chacah](../../strongs/h/h2620.md) in him.

<a name="psalms_34_9"></a>Psalms 34:9

O [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md), ye his [qadowsh](../../strongs/h/h6918.md): for there is no [maḥsôr](../../strongs/h/h4270.md) to them that [yārē'](../../strongs/h/h3373.md) him.

<a name="psalms_34_10"></a>Psalms 34:10

The [kephiyr](../../strongs/h/h3715.md) do [rûš](../../strongs/h/h7326.md), and [rāʿēḇ](../../strongs/h/h7456.md): but they that [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md) shall not [ḥāsēr](../../strongs/h/h2637.md) any [towb](../../strongs/h/h2896.md).

<a name="psalms_34_11"></a>Psalms 34:11

[yālaḵ](../../strongs/h/h3212.md), ye [ben](../../strongs/h/h1121.md), [shama'](../../strongs/h/h8085.md) unto me: I will [lamad](../../strongs/h/h3925.md) you the [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_34_12"></a>Psalms 34:12

What ['iysh](../../strongs/h/h376.md) is he that [chaphets](../../strongs/h/h2655.md) [chay](../../strongs/h/h2416.md), and ['ahab](../../strongs/h/h157.md) many [yowm](../../strongs/h/h3117.md), that he may [ra'ah](../../strongs/h/h7200.md) [towb](../../strongs/h/h2896.md)?

<a name="psalms_34_13"></a>Psalms 34:13

[nāṣar](../../strongs/h/h5341.md) thy [lashown](../../strongs/h/h3956.md) from [ra'](../../strongs/h/h7451.md), and thy [saphah](../../strongs/h/h8193.md) from [dabar](../../strongs/h/h1696.md) [mirmah](../../strongs/h/h4820.md).

<a name="psalms_34_14"></a>Psalms 34:14

[cuwr](../../strongs/h/h5493.md) from [ra'](../../strongs/h/h7451.md), and ['asah](../../strongs/h/h6213.md) [towb](../../strongs/h/h2896.md); [bāqaš](../../strongs/h/h1245.md) [shalowm](../../strongs/h/h7965.md), and [radaph](../../strongs/h/h7291.md) it.

<a name="psalms_34_15"></a>Psalms 34:15

The ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) are upon the [tsaddiyq](../../strongs/h/h6662.md), and his ['ozen](../../strongs/h/h241.md) are open unto their [shav'ah](../../strongs/h/h7775.md).

<a name="psalms_34_16"></a>Psalms 34:16

The [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md) is against them that ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md), to [karath](../../strongs/h/h3772.md) the [zeker](../../strongs/h/h2143.md) of them from the ['erets](../../strongs/h/h776.md).

<a name="psalms_34_17"></a>Psalms 34:17

The [ṣāʿaq](../../strongs/h/h6817.md), and [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md), and [natsal](../../strongs/h/h5337.md) them out of all their [tsarah](../../strongs/h/h6869.md).

<a name="psalms_34_18"></a>Psalms 34:18

[Yĕhovah](../../strongs/h/h3068.md) is [qarowb](../../strongs/h/h7138.md) unto them that are of a [shabar](../../strongs/h/h7665.md) [leb](../../strongs/h/h3820.md); and [yasha'](../../strongs/h/h3467.md) such as be of a [dakā'](../../strongs/h/h1793.md) [ruwach](../../strongs/h/h7307.md).

<a name="psalms_34_19"></a>Psalms 34:19

[rab](../../strongs/h/h7227.md) are the [ra'](../../strongs/h/h7451.md) of the [tsaddiyq](../../strongs/h/h6662.md): but [Yĕhovah](../../strongs/h/h3068.md) [natsal](../../strongs/h/h5337.md) him out of them all.

<a name="psalms_34_20"></a>Psalms 34:20

He [shamar](../../strongs/h/h8104.md) all his ['etsem](../../strongs/h/h6106.md): not one of them is [shabar](../../strongs/h/h7665.md).

<a name="psalms_34_21"></a>Psalms 34:21

[ra'](../../strongs/h/h7451.md) shall [muwth](../../strongs/h/h4191.md) the [rasha'](../../strongs/h/h7563.md): and they that [sane'](../../strongs/h/h8130.md) the [tsaddiyq](../../strongs/h/h6662.md) shall be ['asham](../../strongs/h/h816.md).

<a name="psalms_34_22"></a>Psalms 34:22

[Yĕhovah](../../strongs/h/h3068.md) [pāḏâ](../../strongs/h/h6299.md) the [nephesh](../../strongs/h/h5315.md) of his ['ebed](../../strongs/h/h5650.md): and none of them that [chacah](../../strongs/h/h2620.md) in him shall be ['asham](../../strongs/h/h816.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 33](psalms_33.md) - [Psalms 35](psalms_35.md)