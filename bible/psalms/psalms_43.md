# [Psalms 43](https://www.blueletterbible.org/kjv/psa/43)

<a name="psalms_43_1"></a>Psalms 43:1

[shaphat](../../strongs/h/h8199.md) me, ['Elohiym](../../strongs/h/h430.md), and [riyb](../../strongs/h/h7378.md) my [rîḇ](../../strongs/h/h7379.md) against a [lō'](../../strongs/h/h3808.md) [gowy](../../strongs/h/h1471.md): O [palat](../../strongs/h/h6403.md) me from the [mirmah](../../strongs/h/h4820.md) and ['evel](../../strongs/h/h5766.md) ['iysh](../../strongs/h/h376.md).

<a name="psalms_43_2"></a>Psalms 43:2

For thou art the ['Elohiym](../../strongs/h/h430.md) of my [māʿôz](../../strongs/h/h4581.md): why dost thou [zānaḥ](../../strongs/h/h2186.md) me? why [halak](../../strongs/h/h1980.md) I [qāḏar](../../strongs/h/h6937.md) because of the [laḥaṣ](../../strongs/h/h3906.md) of the ['oyeb](../../strongs/h/h341.md)?

<a name="psalms_43_3"></a>Psalms 43:3

O [shalach](../../strongs/h/h7971.md) thy ['owr](../../strongs/h/h216.md) and thy ['emeth](../../strongs/h/h571.md): let them [nachah](../../strongs/h/h5148.md) me; let them [bow'](../../strongs/h/h935.md) me unto thy [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md), and to thy [miškān](../../strongs/h/h4908.md).

<a name="psalms_43_4"></a>Psalms 43:4

Then will I [bow'](../../strongs/h/h935.md) unto the [mizbeach](../../strongs/h/h4196.md) of ['Elohiym](../../strongs/h/h430.md), unto ['el](../../strongs/h/h410.md) my [simchah](../../strongs/h/h8057.md) [gîl](../../strongs/h/h1524.md): yea, upon the [kinnôr](../../strongs/h/h3658.md) will I [yadah](../../strongs/h/h3034.md) thee, ['Elohiym](../../strongs/h/h430.md) my ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_43_5"></a>Psalms 43:5

Why art thou [shachach](../../strongs/h/h7817.md), O my [nephesh](../../strongs/h/h5315.md)? and why art thou [hāmâ](../../strongs/h/h1993.md) within me? [yāḥal](../../strongs/h/h3176.md) in ['Elohiym](../../strongs/h/h430.md): for I shall yet [yadah](../../strongs/h/h3034.md) him, who is the [yĕshuw'ah](../../strongs/h/h3444.md) of my [paniym](../../strongs/h/h6440.md), and my ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 42](psalms_42.md) - [Psalms 44](psalms_44.md)