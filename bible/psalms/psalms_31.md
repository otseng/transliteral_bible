# [Psalms 31](https://www.blueletterbible.org/kjv/psa/31/1/s_509001)

<a name="psalms_31_1"></a>Psalms 31:1

To the [nāṣaḥ](../../strongs/h/h5329.md), A [mizmôr](../../strongs/h/h4210.md) of [Dāviḏ](../../strongs/h/h1732.md). In thee, [Yĕhovah](../../strongs/h/h3068.md), do I put my [chacah](../../strongs/h/h2620.md); let me never be [buwsh](../../strongs/h/h954.md): [palat](../../strongs/h/h6403.md) me in thy [ṣĕdāqāh](../../strongs/h/h6666.md).

<a name="psalms_31_2"></a>Psalms 31:2

[natah](../../strongs/h/h5186.md) thine ['ozen](../../strongs/h/h241.md) to me; [natsal](../../strongs/h/h5337.md) me [mᵊhērâ](../../strongs/h/h4120.md): be thou my [māʿôz](../../strongs/h/h4581.md) [tsuwr](../../strongs/h/h6697.md), for a [bayith](../../strongs/h/h1004.md) of [matsuwd](../../strongs/h/h4686.md) to [yasha'](../../strongs/h/h3467.md) me.

<a name="psalms_31_3"></a>Psalms 31:3

For thou art my [cela'](../../strongs/h/h5553.md) and my [matsuwd](../../strongs/h/h4686.md); therefore for thy [shem](../../strongs/h/h8034.md) sake [nachah](../../strongs/h/h5148.md) me, and [nāhal](../../strongs/h/h5095.md) me.

<a name="psalms_31_4"></a>Psalms 31:4

[yāṣā'](../../strongs/h/h3318.md) of the [rešeṯ](../../strongs/h/h7568.md) that they have [taman](../../strongs/h/h2934.md) for me: for thou art my [māʿôz](../../strongs/h/h4581.md).

<a name="psalms_31_5"></a>Psalms 31:5

Into thine [yad](../../strongs/h/h3027.md) I [paqad](../../strongs/h/h6485.md) my [ruwach](../../strongs/h/h7307.md): thou hast [pāḏâ](../../strongs/h/h6299.md) me, [Yĕhovah](../../strongs/h/h3068.md) ['el](../../strongs/h/h410.md) of ['emeth](../../strongs/h/h571.md).

<a name="psalms_31_6"></a>Psalms 31:6

I have [sane'](../../strongs/h/h8130.md) them that [shamar](../../strongs/h/h8104.md) [shav'](../../strongs/h/h7723.md) [heḇel](../../strongs/h/h1892.md): but I [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_31_7"></a>Psalms 31:7

I will be [giyl](../../strongs/h/h1523.md) and [samach](../../strongs/h/h8055.md) in thy [checed](../../strongs/h/h2617.md): for thou hast [ra'ah](../../strongs/h/h7200.md) my ['oniy](../../strongs/h/h6040.md); thou hast [yada'](../../strongs/h/h3045.md) my [nephesh](../../strongs/h/h5315.md) in [tsarah](../../strongs/h/h6869.md);

<a name="psalms_31_8"></a>Psalms 31:8

And hast not [cagar](../../strongs/h/h5462.md) me up into the [yad](../../strongs/h/h3027.md) of the ['oyeb](../../strongs/h/h341.md): thou hast ['amad](../../strongs/h/h5975.md) my [regel](../../strongs/h/h7272.md) in a [merḥāḇ](../../strongs/h/h4800.md).

<a name="psalms_31_9"></a>Psalms 31:9

Have [chanan](../../strongs/h/h2603.md) upon me, [Yĕhovah](../../strongs/h/h3068.md), for I am in [tsarar](../../strongs/h/h6887.md): mine ['ayin](../../strongs/h/h5869.md) is [ʿāšaš](../../strongs/h/h6244.md) with [ka'ac](../../strongs/h/h3708.md), yea, my [nephesh](../../strongs/h/h5315.md) and my [beten](../../strongs/h/h990.md).

<a name="psalms_31_10"></a>Psalms 31:10

For my [chay](../../strongs/h/h2416.md) is [kalah](../../strongs/h/h3615.md) with [yagown](../../strongs/h/h3015.md), and my [šānâ](../../strongs/h/h8141.md) with ['anachah](../../strongs/h/h585.md): my [koach](../../strongs/h/h3581.md) [kashal](../../strongs/h/h3782.md) because of mine ['avon](../../strongs/h/h5771.md), and my ['etsem](../../strongs/h/h6106.md) are [ʿāšaš](../../strongs/h/h6244.md).

<a name="psalms_31_11"></a>Psalms 31:11

I was a [cherpah](../../strongs/h/h2781.md) among all mine [tsarar](../../strongs/h/h6887.md), but [me'od](../../strongs/h/h3966.md) among my [šāḵēn](../../strongs/h/h7934.md), and a [paḥaḏ](../../strongs/h/h6343.md) to [yada'](../../strongs/h/h3045.md): they that did [ra'ah](../../strongs/h/h7200.md) me without [nāḏaḏ](../../strongs/h/h5074.md) from me.

<a name="psalms_31_12"></a>Psalms 31:12

I am [shakach](../../strongs/h/h7911.md) as a [muwth](../../strongs/h/h4191.md) out of [leb](../../strongs/h/h3820.md): I am like an ['abad](../../strongs/h/h6.md) [kĕliy](../../strongs/h/h3627.md).

<a name="psalms_31_13"></a>Psalms 31:13

For I have [shama'](../../strongs/h/h8085.md) the [dibâ](../../strongs/h/h1681.md) of [rab](../../strongs/h/h7227.md): [māḡôr](../../strongs/h/h4032.md) was [cabiyb](../../strongs/h/h5439.md): while they took [yacad](../../strongs/h/h3245.md) [yaḥaḏ](../../strongs/h/h3162.md) against me, they [zāmam](../../strongs/h/h2161.md) to [laqach](../../strongs/h/h3947.md) my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_31_14"></a>Psalms 31:14

But I [batach](../../strongs/h/h982.md) in thee, [Yĕhovah](../../strongs/h/h3068.md): I ['āmar](../../strongs/h/h559.md), Thou art my ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_31_15"></a>Psalms 31:15

My [ʿēṯ](../../strongs/h/h6256.md) are in thy [yad](../../strongs/h/h3027.md): [natsal](../../strongs/h/h5337.md) me from the [yad](../../strongs/h/h3027.md) of mine ['oyeb](../../strongs/h/h341.md), and from them that [radaph](../../strongs/h/h7291.md) me.

<a name="psalms_31_16"></a>Psalms 31:16

Make thy [paniym](../../strongs/h/h6440.md) to ['owr](../../strongs/h/h215.md) upon thy ['ebed](../../strongs/h/h5650.md): [yasha'](../../strongs/h/h3467.md) me for thy [checed](../../strongs/h/h2617.md) sake.

<a name="psalms_31_17"></a>Psalms 31:17

Let me not be [buwsh](../../strongs/h/h954.md), [Yĕhovah](../../strongs/h/h3068.md); for I have [qara'](../../strongs/h/h7121.md) upon thee: let the [rasha'](../../strongs/h/h7563.md) be [buwsh](../../strongs/h/h954.md), and let them be [damam](../../strongs/h/h1826.md) in the [shĕ'owl](../../strongs/h/h7585.md).

<a name="psalms_31_18"></a>Psalms 31:18

Let the [sheqer](../../strongs/h/h8267.md) [saphah](../../strongs/h/h8193.md) be ['ālam](../../strongs/h/h481.md); which [dabar](../../strongs/h/h1696.md) [ʿāṯāq](../../strongs/h/h6277.md) things [ga'avah](../../strongs/h/h1346.md) and [bûz](../../strongs/h/h937.md) against the [tsaddiyq](../../strongs/h/h6662.md).

<a name="psalms_31_19"></a>Psalms 31:19

Oh how [rab](../../strongs/h/h7227.md) is thy [ṭûḇ](../../strongs/h/h2898.md), which thou hast [tsaphan](../../strongs/h/h6845.md) for them that [yārē'](../../strongs/h/h3373.md) thee; which thou hast [pa'al](../../strongs/h/h6466.md) for them that [chacah](../../strongs/h/h2620.md) in thee before the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md)!

<a name="psalms_31_20"></a>Psalms 31:20

Thou shalt [cathar](../../strongs/h/h5641.md) them in the [cether](../../strongs/h/h5643.md) of thy [paniym](../../strongs/h/h6440.md) from the [rōḵes](../../strongs/h/h7407.md) of ['iysh](../../strongs/h/h376.md): thou shalt [tsaphan](../../strongs/h/h6845.md) them in a [cukkah](../../strongs/h/h5521.md) from the [rîḇ](../../strongs/h/h7379.md) of [lashown](../../strongs/h/h3956.md).

<a name="psalms_31_21"></a>Psalms 31:21

[barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md): for he hath shewed me his [pala'](../../strongs/h/h6381.md) [checed](../../strongs/h/h2617.md) in a [māṣôr](../../strongs/h/h4692.md) [ʿîr](../../strongs/h/h5892.md).

<a name="psalms_31_22"></a>Psalms 31:22

For I ['āmar](../../strongs/h/h559.md) in my [ḥāp̄az](../../strongs/h/h2648.md), I am [gāraz](../../strongs/h/h1629.md) from [neḡeḏ](../../strongs/h/h5048.md) thine ['ayin](../../strongs/h/h5869.md): ['āḵēn](../../strongs/h/h403.md) thou [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of my [taḥănûn](../../strongs/h/h8469.md) when I [šāvaʿ](../../strongs/h/h7768.md) unto thee.

<a name="psalms_31_23"></a>Psalms 31:23

O ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md), all ye his [chaciyd](../../strongs/h/h2623.md): for [Yĕhovah](../../strongs/h/h3068.md) [nāṣar](../../strongs/h/h5341.md) the ['aman](../../strongs/h/h539.md), and [yeṯer](../../strongs/h/h3499.md) [shalam](../../strongs/h/h7999.md) the [ga'avah](../../strongs/h/h1346.md) ['asah](../../strongs/h/h6213.md).

<a name="psalms_31_24"></a>Psalms 31:24

Be of good [ḥāzaq](../../strongs/h/h2388.md), and he shall ['amats](../../strongs/h/h553.md) your [lebab](../../strongs/h/h3824.md), all ye that [yāḥal](../../strongs/h/h3176.md) in [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 30](psalms_30.md) - [Psalms 32](psalms_32.md)