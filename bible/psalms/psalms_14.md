# [Psalms 14](https://www.blueletterbible.org/kjv/psa/14/1/s_492001)

<a name="psalms_14_1"></a>Psalms 14:1

To the [nāṣaḥ](../../strongs/h/h5329.md), of [Dāviḏ](../../strongs/h/h1732.md). The [nabal](../../strongs/h/h5036.md) hath ['āmar](../../strongs/h/h559.md) in his [leb](../../strongs/h/h3820.md), There is no ['Elohiym](../../strongs/h/h430.md). They are [shachath](../../strongs/h/h7843.md), they have done [ta'ab](../../strongs/h/h8581.md) ['aliylah](../../strongs/h/h5949.md), there is none that ['asah](../../strongs/h/h6213.md) [towb](../../strongs/h/h2896.md).

<a name="psalms_14_2"></a>Psalms 14:2

[Yĕhovah](../../strongs/h/h3068.md) [šāqap̄](../../strongs/h/h8259.md) from [shamayim](../../strongs/h/h8064.md) upon the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md), to [ra'ah](../../strongs/h/h7200.md) if there were any that did [sakal](../../strongs/h/h7919.md), and [darash](../../strongs/h/h1875.md) ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_14_3"></a>Psalms 14:3

They [cuwr](../../strongs/h/h5493.md), they are [yaḥaḏ](../../strongs/h/h3162.md) ['alach](../../strongs/h/h444.md): there is none that ['asah](../../strongs/h/h6213.md) [towb](../../strongs/h/h2896.md), no, not one.

<a name="psalms_14_4"></a>Psalms 14:4

Have all the [pa'al](../../strongs/h/h6466.md) of ['aven](../../strongs/h/h205.md) no [yada'](../../strongs/h/h3045.md)? who ['akal](../../strongs/h/h398.md) my ['am](../../strongs/h/h5971.md) as they ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md), and [qara'](../../strongs/h/h7121.md) not upon [Yĕhovah](../../strongs/h/h3068.md).

<a name="psalms_14_5"></a>Psalms 14:5

There were they in [paḥaḏ](../../strongs/h/h6343.md) [pachad](../../strongs/h/h6342.md): for ['Elohiym](../../strongs/h/h430.md) is in the [dôr](../../strongs/h/h1755.md) of the [tsaddiyq](../../strongs/h/h6662.md).

<a name="psalms_14_6"></a>Psalms 14:6

Ye have [buwsh](../../strongs/h/h954.md) the ['etsah](../../strongs/h/h6098.md) of the ['aniy](../../strongs/h/h6041.md), because [Yĕhovah](../../strongs/h/h3068.md) is his [machaceh](../../strongs/h/h4268.md).

<a name="psalms_14_7"></a>Psalms 14:7

Oh that the [yĕshuw'ah](../../strongs/h/h3444.md) of [Yisra'el](../../strongs/h/h3478.md) out of [Tsiyown](../../strongs/h/h6726.md)! when [Yĕhovah](../../strongs/h/h3068.md) [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) of his ['am](../../strongs/h/h5971.md), [Ya'aqob](../../strongs/h/h3290.md) shall [giyl](../../strongs/h/h1523.md), and [Yisra'el](../../strongs/h/h3478.md) shall be [samach](../../strongs/h/h8055.md).

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 13](psalms_13.md) - [Psalms 15](psalms_15.md)