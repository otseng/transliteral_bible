# [Psalms 56](https://www.blueletterbible.org/kjv/psa/56)

<a name="psalms_56_1"></a>Psalms 56:1

To the [nāṣaḥ](../../strongs/h/h5329.md) upon [Yônaṯ 'ēlem rᵊḥōqîm](../../strongs/h/h3128.md), [Miḵtām](../../strongs/h/h4387.md) of [Dāviḏ](../../strongs/h/h1732.md), when the [Pᵊlištî](../../strongs/h/h6430.md) ['āḥaz](../../strongs/h/h270.md) him in [Gaṯ](../../strongs/h/h1661.md). Be [chanan](../../strongs/h/h2603.md) unto me, ['Elohiym](../../strongs/h/h430.md): for ['enowsh](../../strongs/h/h582.md) would [šā'ap̄](../../strongs/h/h7602.md) me; he [lāḥam](../../strongs/h/h3898.md) [yowm](../../strongs/h/h3117.md) [lāḥaṣ](../../strongs/h/h3905.md) me.

<a name="psalms_56_2"></a>Psalms 56:2

Mine [sharar](../../strongs/h/h8324.md) would [yowm](../../strongs/h/h3117.md) [šā'ap̄](../../strongs/h/h7602.md) me up: for they be [rab](../../strongs/h/h7227.md) that [lāḥam](../../strongs/h/h3898.md) against me, O thou [marowm](../../strongs/h/h4791.md).

<a name="psalms_56_3"></a>Psalms 56:3

What [yowm](../../strongs/h/h3117.md) I am [yare'](../../strongs/h/h3372.md), I will [batach](../../strongs/h/h982.md) in thee.

<a name="psalms_56_4"></a>Psalms 56:4

In ['Elohiym](../../strongs/h/h430.md) I will [halal](../../strongs/h/h1984.md) his [dabar](../../strongs/h/h1697.md), in ['Elohiym](../../strongs/h/h430.md) I have put my [batach](../../strongs/h/h982.md); I will not [yare'](../../strongs/h/h3372.md) what [basar](../../strongs/h/h1320.md) can ['asah](../../strongs/h/h6213.md) unto me.

<a name="psalms_56_5"></a>Psalms 56:5

Every [yowm](../../strongs/h/h3117.md) they [ʿāṣaḇ](../../strongs/h/h6087.md) my [dabar](../../strongs/h/h1697.md): all their [maḥăšāḇâ](../../strongs/h/h4284.md) are against me for [ra'](../../strongs/h/h7451.md).

<a name="psalms_56_6"></a>Psalms 56:6

They [guwr](../../strongs/h/h1481.md), they [tsaphan](../../strongs/h/h6845.md) themselves, they [shamar](../../strongs/h/h8104.md) my ['aqeb](../../strongs/h/h6119.md), when they [qāvâ](../../strongs/h/h6960.md) for my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_56_7"></a>Psalms 56:7

Shall they [pallēṭ](../../strongs/h/h6405.md) by ['aven](../../strongs/h/h205.md)? in thine ['aph](../../strongs/h/h639.md) [yarad](../../strongs/h/h3381.md) the ['am](../../strongs/h/h5971.md), ['Elohiym](../../strongs/h/h430.md).

<a name="psalms_56_8"></a>Psalms 56:8

Thou [sāp̄ar](../../strongs/h/h5608.md) my [nôḏ](../../strongs/h/h5112.md): [śûm](../../strongs/h/h7760.md) thou my [dim'ah](../../strongs/h/h1832.md) into thy [nō'ḏ](../../strongs/h/h4997.md): are they not in thy [sēp̄er](../../strongs/h/h5612.md)?

<a name="psalms_56_9"></a>Psalms 56:9

When I [qara'](../../strongs/h/h7121.md) unto thee, then shall mine ['oyeb](../../strongs/h/h341.md) [shuwb](../../strongs/h/h7725.md) ['āḥôr](../../strongs/h/h268.md): this I [yada'](../../strongs/h/h3045.md); for ['Elohiym](../../strongs/h/h430.md) is for me.

<a name="psalms_56_10"></a>Psalms 56:10

In ['Elohiym](../../strongs/h/h430.md) will I [halal](../../strongs/h/h1984.md) his [dabar](../../strongs/h/h1697.md): in [Yĕhovah](../../strongs/h/h3068.md) will I [halal](../../strongs/h/h1984.md) his [dabar](../../strongs/h/h1697.md).

<a name="psalms_56_11"></a>Psalms 56:11

In ['Elohiym](../../strongs/h/h430.md) have I put my [batach](../../strongs/h/h982.md): I will not be [yare'](../../strongs/h/h3372.md) what ['adam](../../strongs/h/h120.md) can ['asah](../../strongs/h/h6213.md) unto me.

<a name="psalms_56_12"></a>Psalms 56:12

Thy [neḏer](../../strongs/h/h5088.md) are upon me, ['Elohiym](../../strongs/h/h430.md): I will [shalam](../../strongs/h/h7999.md) [tôḏâ](../../strongs/h/h8426.md) unto thee.

<a name="psalms_56_13"></a>Psalms 56:13

For thou hast [natsal](../../strongs/h/h5337.md) my [nephesh](../../strongs/h/h5315.md) from [maveth](../../strongs/h/h4194.md): wilt not my [regel](../../strongs/h/h7272.md) from [dᵊḥî](../../strongs/h/h1762.md), that I may [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md) in the ['owr](../../strongs/h/h216.md) of the [chay](../../strongs/h/h2416.md)?

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 55](psalms_55.md) - [Psalms 57](psalms_57.md)