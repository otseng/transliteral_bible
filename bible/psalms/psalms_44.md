# [Psalms 44](https://www.blueletterbible.org/kjv/psa/44)

<a name="psalms_44_1"></a>Psalms 44:1

To the [nāṣaḥ](../../strongs/h/h5329.md) for the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md), [Maśkîl](../../strongs/h/h4905.md). We have [shama'](../../strongs/h/h8085.md) with our ['ozen](../../strongs/h/h241.md), ['Elohiym](../../strongs/h/h430.md), our ['ab](../../strongs/h/h1.md) have [sāp̄ar](../../strongs/h/h5608.md) us, what [pōʿal](../../strongs/h/h6467.md) thou [pa'al](../../strongs/h/h6466.md) in their [yowm](../../strongs/h/h3117.md), in the [yowm](../../strongs/h/h3117.md) of [qeḏem](../../strongs/h/h6924.md).

<a name="psalms_44_2"></a>Psalms 44:2

How thou didst [yarash](../../strongs/h/h3423.md) the [gowy](../../strongs/h/h1471.md) with thy [yad](../../strongs/h/h3027.md), and [nāṭaʿ](../../strongs/h/h5193.md) them; how thou didst [ra'a'](../../strongs/h/h7489.md) the [lĕom](../../strongs/h/h3816.md), and [shalach](../../strongs/h/h7971.md) them.

<a name="psalms_44_3"></a>Psalms 44:3

For they [yarash](../../strongs/h/h3423.md) not the ['erets](../../strongs/h/h776.md) by their own [chereb](../../strongs/h/h2719.md), neither did their own [zerowa'](../../strongs/h/h2220.md) [yasha'](../../strongs/h/h3467.md) them: but thy [yamiyn](../../strongs/h/h3225.md), and thine [zerowa'](../../strongs/h/h2220.md), and the ['owr](../../strongs/h/h216.md) of thy [paniym](../../strongs/h/h6440.md), because thou hadst a [ratsah](../../strongs/h/h7521.md) unto them.

<a name="psalms_44_4"></a>Psalms 44:4

Thou art my [melek](../../strongs/h/h4428.md), ['Elohiym](../../strongs/h/h430.md): [tsavah](../../strongs/h/h6680.md) [yĕshuw'ah](../../strongs/h/h3444.md) for [Ya'aqob](../../strongs/h/h3290.md).

<a name="psalms_44_5"></a>Psalms 44:5

Through thee will we [nāḡaḥ](../../strongs/h/h5055.md) our [tsar](../../strongs/h/h6862.md): through thy [shem](../../strongs/h/h8034.md) will we [bûs](../../strongs/h/h947.md) them that [quwm](../../strongs/h/h6965.md) against us.

<a name="psalms_44_6"></a>Psalms 44:6

For I will not [batach](../../strongs/h/h982.md) in my [qesheth](../../strongs/h/h7198.md), neither shall my [chereb](../../strongs/h/h2719.md) [yasha'](../../strongs/h/h3467.md) me.

<a name="psalms_44_7"></a>Psalms 44:7

But thou hast [yasha'](../../strongs/h/h3467.md) us from our [tsar](../../strongs/h/h6862.md), and hast put them to [buwsh](../../strongs/h/h954.md) that [sane'](../../strongs/h/h8130.md) us.

<a name="psalms_44_8"></a>Psalms 44:8

In ['Elohiym](../../strongs/h/h430.md) we [halal](../../strongs/h/h1984.md) all the [yowm](../../strongs/h/h3117.md) long, and [yadah](../../strongs/h/h3034.md) thy [shem](../../strongs/h/h8034.md) ['owlam](../../strongs/h/h5769.md). [Celah](../../strongs/h/h5542.md).

<a name="psalms_44_9"></a>Psalms 44:9

But thou hast [zānaḥ](../../strongs/h/h2186.md), and put us to [kālam](../../strongs/h/h3637.md); and [yāṣā'](../../strongs/h/h3318.md) not with our [tsaba'](../../strongs/h/h6635.md).

<a name="psalms_44_10"></a>Psalms 44:10

Thou makest us to [shuwb](../../strongs/h/h7725.md) ['āḥôr](../../strongs/h/h268.md) from the [tsar](../../strongs/h/h6862.md): and they which [sane'](../../strongs/h/h8130.md) us [šāsâ](../../strongs/h/h8154.md) for themselves.

<a name="psalms_44_11"></a>Psalms 44:11

Thou hast [nathan](../../strongs/h/h5414.md) us like [tso'n](../../strongs/h/h6629.md) appointed for [ma'akal](../../strongs/h/h3978.md); and hast [zārâ](../../strongs/h/h2219.md) us among the [gowy](../../strongs/h/h1471.md).

<a name="psalms_44_12"></a>Psalms 44:12

Thou [māḵar](../../strongs/h/h4376.md) thy ['am](../../strongs/h/h5971.md) for [hôn](../../strongs/h/h1952.md), and dost not [rabah](../../strongs/h/h7235.md) thy wealth by their [mᵊḥîr](../../strongs/h/h4242.md).

<a name="psalms_44_13"></a>Psalms 44:13

Thou [śûm](../../strongs/h/h7760.md) us a [cherpah](../../strongs/h/h2781.md) to our [šāḵēn](../../strongs/h/h7934.md), a [laʿaḡ](../../strongs/h/h3933.md) and a [qeles](../../strongs/h/h7047.md) to them that are [cabiyb](../../strongs/h/h5439.md) us.

<a name="psalms_44_14"></a>Psalms 44:14

Thou [śûm](../../strongs/h/h7760.md) us a [māšāl](../../strongs/h/h4912.md) among the [gowy](../../strongs/h/h1471.md), a [mānôḏ](../../strongs/h/h4493.md) of the [ro'sh](../../strongs/h/h7218.md) among the [lĕom](../../strongs/h/h3816.md).

<a name="psalms_44_15"></a>Psalms 44:15

My [kĕlimmah](../../strongs/h/h3639.md) is [yowm](../../strongs/h/h3117.md) before me, and the [bšeṯ](../../strongs/h/h1322.md) of my [paniym](../../strongs/h/h6440.md) hath [kāsâ](../../strongs/h/h3680.md) me,

<a name="psalms_44_16"></a>Psalms 44:16

For the [qowl](../../strongs/h/h6963.md) of him that [ḥārap̄](../../strongs/h/h2778.md) and [gāḏap̄](../../strongs/h/h1442.md); by [paniym](../../strongs/h/h6440.md) of the ['oyeb](../../strongs/h/h341.md) and [naqam](../../strongs/h/h5358.md).

<a name="psalms_44_17"></a>Psalms 44:17

All this is [bow'](../../strongs/h/h935.md) upon us; yet have we not [shakach](../../strongs/h/h7911.md) thee, neither have we [šāqar](../../strongs/h/h8266.md) in thy [bĕriyth](../../strongs/h/h1285.md).

<a name="psalms_44_18"></a>Psalms 44:18

Our [leb](../../strongs/h/h3820.md) is not [sûḡ](../../strongs/h/h5472.md) ['āḥôr](../../strongs/h/h268.md), neither have our ['ashuwr](../../strongs/h/h838.md) [natah](../../strongs/h/h5186.md) from thy ['orach](../../strongs/h/h734.md);

<a name="psalms_44_19"></a>Psalms 44:19

Though thou hast sore [dakah](../../strongs/h/h1794.md) us in the [maqowm](../../strongs/h/h4725.md) of [tannîn](../../strongs/h/h8577.md), and [kāsâ](../../strongs/h/h3680.md) us with the [ṣalmāveṯ](../../strongs/h/h6757.md).

<a name="psalms_44_20"></a>Psalms 44:20

If we have [shakach](../../strongs/h/h7911.md) the [shem](../../strongs/h/h8034.md) of our ['Elohiym](../../strongs/h/h430.md), or [pāraś](../../strongs/h/h6566.md) our [kaph](../../strongs/h/h3709.md) to a [zûr](../../strongs/h/h2114.md) ['el](../../strongs/h/h410.md);

<a name="psalms_44_21"></a>Psalms 44:21

Shall not ['Elohiym](../../strongs/h/h430.md) [chaqar](../../strongs/h/h2713.md) this out? for he [yada'](../../strongs/h/h3045.md) the [taʿălumâ](../../strongs/h/h8587.md) of the [leb](../../strongs/h/h3820.md).

<a name="psalms_44_22"></a>Psalms 44:22

Yea, for thy sake are we [harag](../../strongs/h/h2026.md) all the [yowm](../../strongs/h/h3117.md) long; we are [chashab](../../strongs/h/h2803.md) as [tso'n](../../strongs/h/h6629.md) for the [ṭiḇḥâ](../../strongs/h/h2878.md).

<a name="psalms_44_23"></a>Psalms 44:23

[ʿûr](../../strongs/h/h5782.md), why [yashen](../../strongs/h/h3462.md) thou, ['adonay](../../strongs/h/h136.md)? [quwts](../../strongs/h/h6974.md), [zānaḥ](../../strongs/h/h2186.md) us not [netsach](../../strongs/h/h5331.md).

<a name="psalms_44_24"></a>Psalms 44:24

Wherefore [cathar](../../strongs/h/h5641.md) thou thy [paniym](../../strongs/h/h6440.md), and [shakach](../../strongs/h/h7911.md) our ['oniy](../../strongs/h/h6040.md) and our [laḥaṣ](../../strongs/h/h3906.md)?

<a name="psalms_44_25"></a>Psalms 44:25

For our [nephesh](../../strongs/h/h5315.md) is [šûaḥ](../../strongs/h/h7743.md) to the ['aphar](../../strongs/h/h6083.md): our [beten](../../strongs/h/h990.md) [dāḇaq](../../strongs/h/h1692.md) unto the ['erets](../../strongs/h/h776.md).

<a name="psalms_44_26"></a>Psalms 44:26

[quwm](../../strongs/h/h6965.md) for our [ʿezrâ](../../strongs/h/h5833.md), and [pāḏâ](../../strongs/h/h6299.md) us for thy [checed](../../strongs/h/h2617.md) sake.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 43](psalms_43.md) - [Psalms 45](psalms_45.md)