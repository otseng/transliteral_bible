# [Psalms 86](https://www.blueletterbible.org/kjv/psalms/86)

<a name="psalms_86_1"></a>Psalms 86:1

A [tĕphillah](../../strongs/h/h8605.md) of [Dāviḏ](../../strongs/h/h1732.md). [natah](../../strongs/h/h5186.md) thine ['ozen](../../strongs/h/h241.md), [Yĕhovah](../../strongs/h/h3068.md), ['anah](../../strongs/h/h6030.md) me: for I am ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md).

<a name="psalms_86_2"></a>Psalms 86:2

[shamar](../../strongs/h/h8104.md) my [nephesh](../../strongs/h/h5315.md); for I am [chaciyd](../../strongs/h/h2623.md): O thou my ['Elohiym](../../strongs/h/h430.md), [yasha'](../../strongs/h/h3467.md) thy ['ebed](../../strongs/h/h5650.md) that [batach](../../strongs/h/h982.md) in thee.

<a name="psalms_86_3"></a>Psalms 86:3

Be [chanan](../../strongs/h/h2603.md) unto me, ['Adonay](../../strongs/h/h136.md): for I [qara'](../../strongs/h/h7121.md) unto thee [yowm](../../strongs/h/h3117.md).

<a name="psalms_86_4"></a>Psalms 86:4

[samach](../../strongs/h/h8055.md) the [nephesh](../../strongs/h/h5315.md) of thy ['ebed](../../strongs/h/h5650.md): for unto thee, ['Adonay](../../strongs/h/h136.md), do I [nasa'](../../strongs/h/h5375.md) my [nephesh](../../strongs/h/h5315.md).

<a name="psalms_86_5"></a>Psalms 86:5

For thou, ['adonay](../../strongs/h/h136.md), art [towb](../../strongs/h/h2896.md), and ready to [sallāḥ](../../strongs/h/h5546.md); and [rab](../../strongs/h/h7227.md) in [checed](../../strongs/h/h2617.md) unto all them that [qara'](../../strongs/h/h7121.md) upon thee.

<a name="psalms_86_6"></a>Psalms 86:6

['azan](../../strongs/h/h238.md), [Yĕhovah](../../strongs/h/h3068.md), unto my [tĕphillah](../../strongs/h/h8605.md); and [qashab](../../strongs/h/h7181.md) to the [qowl](../../strongs/h/h6963.md) of my [taḥănûn](../../strongs/h/h8469.md).

<a name="psalms_86_7"></a>Psalms 86:7

In the [yowm](../../strongs/h/h3117.md) of my [tsarah](../../strongs/h/h6869.md) I will [qara'](../../strongs/h/h7121.md) upon thee: for thou wilt ['anah](../../strongs/h/h6030.md) me.

<a name="psalms_86_8"></a>Psalms 86:8

Among the ['Elohiym](../../strongs/h/h430.md) there is none like unto thee, ['Adonay](../../strongs/h/h136.md); neither are there any works like unto thy [ma'aseh](../../strongs/h/h4639.md).

<a name="psalms_86_9"></a>Psalms 86:9

All [gowy](../../strongs/h/h1471.md) whom thou hast ['asah](../../strongs/h/h6213.md) shall [bow'](../../strongs/h/h935.md) and [shachah](../../strongs/h/h7812.md) [paniym](../../strongs/h/h6440.md) thee, ['Adonay](../../strongs/h/h136.md); and shall [kabad](../../strongs/h/h3513.md) thy [shem](../../strongs/h/h8034.md).

<a name="psalms_86_10"></a>Psalms 86:10

For thou art [gadowl](../../strongs/h/h1419.md), and ['asah](../../strongs/h/h6213.md) [pala'](../../strongs/h/h6381.md): thou art ['Elohiym](../../strongs/h/h430.md) alone.

<a name="psalms_86_11"></a>Psalms 86:11

[yārâ](../../strongs/h/h3384.md) me thy [derek](../../strongs/h/h1870.md), [Yĕhovah](../../strongs/h/h3068.md); I will [halak](../../strongs/h/h1980.md) in thy ['emeth](../../strongs/h/h571.md): [yāḥaḏ](../../strongs/h/h3161.md) my [lebab](../../strongs/h/h3824.md) to [yare'](../../strongs/h/h3372.md) thy [shem](../../strongs/h/h8034.md).

<a name="psalms_86_12"></a>Psalms 86:12

I will [yadah](../../strongs/h/h3034.md) thee, ['Adonay](../../strongs/h/h136.md) my ['Elohiym](../../strongs/h/h430.md), with all my [lebab](../../strongs/h/h3824.md): and I will [kabad](../../strongs/h/h3513.md) thy [shem](../../strongs/h/h8034.md) for ['owlam](../../strongs/h/h5769.md).

<a name="psalms_86_13"></a>Psalms 86:13

For [gadowl](../../strongs/h/h1419.md) is thy [checed](../../strongs/h/h2617.md) toward me: and thou hast [natsal](../../strongs/h/h5337.md) my [nephesh](../../strongs/h/h5315.md) from the [taḥtî](../../strongs/h/h8482.md) [shĕ'owl](../../strongs/h/h7585.md).

<a name="psalms_86_14"></a>Psalms 86:14

['Elohiym](../../strongs/h/h430.md), the [zed](../../strongs/h/h2086.md) are [quwm](../../strongs/h/h6965.md) against me, and the ['edah](../../strongs/h/h5712.md) of [ʿārîṣ](../../strongs/h/h6184.md) men have [bāqaš](../../strongs/h/h1245.md) after my [nephesh](../../strongs/h/h5315.md); and have not [śûm](../../strongs/h/h7760.md) thee before them.

<a name="psalms_86_15"></a>Psalms 86:15

But thou, ['Adonay](../../strongs/h/h136.md), art an ['el](../../strongs/h/h410.md) full of [raḥwm](../../strongs/h/h7349.md), and [ḥanwn](../../strongs/h/h2587.md), ['ārēḵ](../../strongs/h/h750.md) ['aph](../../strongs/h/h639.md), and [rab](../../strongs/h/h7227.md) in [checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md).

<a name="psalms_86_16"></a>Psalms 86:16

O [panah](../../strongs/h/h6437.md) unto me, and have [chanan](../../strongs/h/h2603.md) upon me; [nathan](../../strongs/h/h5414.md) thy ['oz](../../strongs/h/h5797.md) unto thy ['ebed](../../strongs/h/h5650.md), and [yasha'](../../strongs/h/h3467.md) the [ben](../../strongs/h/h1121.md) of thine ['amah](../../strongs/h/h519.md).

<a name="psalms_86_17"></a>Psalms 86:17

['asah](../../strongs/h/h6213.md) me a ['ôṯ](../../strongs/h/h226.md) for [towb](../../strongs/h/h2896.md); that they which [sane'](../../strongs/h/h8130.md) me may [ra'ah](../../strongs/h/h7200.md) it, and be [buwsh](../../strongs/h/h954.md): because thou, [Yĕhovah](../../strongs/h/h3068.md), hast [ʿāzar](../../strongs/h/h5826.md) me, and [nacham](../../strongs/h/h5162.md) me.

---

[Transliteral Bible](../bible.md)

[Psalms](psalms.md)

[Psalms 85](psalms_85.md) - [Psalms 87](psalms_87.md)