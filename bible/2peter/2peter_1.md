# [2 Peter 1](https://www.blueletterbible.org/kjv/2pe/1/1/s_1157001)

<a name="2peter_1_1"></a>2 Peter 1:1

[Symeōn](../../strongs/g/g4826.md) [Petros](../../strongs/g/g4074.md), a [doulos](../../strongs/g/g1401.md) and an [apostolos](../../strongs/g/g652.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), to them that have [lagchanō](../../strongs/g/g2975.md) [isotimos](../../strongs/g/g2472.md) [pistis](../../strongs/g/g4102.md) with us through the [dikaiosynē](../../strongs/g/g1343.md) of [theos](../../strongs/g/g2316.md) and our [sōtēr](../../strongs/g/g4990.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md):

<a name="2peter_1_2"></a>2 Peter 1:2

[charis](../../strongs/g/g5485.md) and [eirēnē](../../strongs/g/g1515.md) be [plēthynō](../../strongs/g/g4129.md) unto you through the [epignōsis](../../strongs/g/g1922.md) of [theos](../../strongs/g/g2316.md), and of [Iēsous](../../strongs/g/g2424.md) our [kyrios](../../strongs/g/g2962.md),

<a name="2peter_1_3"></a>2 Peter 1:3

According as his [theios](../../strongs/g/g2304.md) [dynamis](../../strongs/g/g1411.md) hath [dōreomai](../../strongs/g/g1433.md) unto us all things that unto [zōē](../../strongs/g/g2222.md) and [eusebeia](../../strongs/g/g2150.md), through the [epignōsis](../../strongs/g/g1922.md) of him that hath [kaleō](../../strongs/g/g2564.md) us to [doxa](../../strongs/g/g1391.md) and [aretē](../../strongs/g/g703.md):

<a name="2peter_1_4"></a>2 Peter 1:4

Whereby are [dōreomai](../../strongs/g/g1433.md) unto us [megistos](../../strongs/g/g3176.md) and [timios](../../strongs/g/g5093.md) [epangelma](../../strongs/g/g1862.md): that by these ye might be [koinōnos](../../strongs/g/g2844.md) of the [theios](../../strongs/g/g2304.md) [physis](../../strongs/g/g5449.md), having [apopheugō](../../strongs/g/g668.md) the [phthora](../../strongs/g/g5356.md) that is in the [kosmos](../../strongs/g/g2889.md) through [epithymia](../../strongs/g/g1939.md).

<a name="2peter_1_5"></a>2 Peter 1:5

And beside this, [pareispherō](../../strongs/g/g3923.md) all [spoudē](../../strongs/g/g4710.md), [epichorēgeō](../../strongs/g/g2023.md) to your [pistis](../../strongs/g/g4102.md) [aretē](../../strongs/g/g703.md); and to [aretē](../../strongs/g/g703.md) [gnōsis](../../strongs/g/g1108.md);

<a name="2peter_1_6"></a>2 Peter 1:6

And to [gnōsis](../../strongs/g/g1108.md) [egkrateia](../../strongs/g/g1466.md); and to [egkrateia](../../strongs/g/g1466.md) [hypomonē](../../strongs/g/g5281.md); and to [hypomonē](../../strongs/g/g5281.md) [eusebeia](../../strongs/g/g2150.md);

<a name="2peter_1_7"></a>2 Peter 1:7

And to [eusebeia](../../strongs/g/g2150.md) [philadelphia](../../strongs/g/g5360.md); and to [philadelphia](../../strongs/g/g5360.md) [agapē](../../strongs/g/g26.md).

<a name="2peter_1_8"></a>2 Peter 1:8

For if these things be in you, and [pleonazō](../../strongs/g/g4121.md), they [kathistēmi](../../strongs/g/g2525.md) neither be [argos](../../strongs/g/g692.md) nor [akarpos](../../strongs/g/g175.md) in the [epignōsis](../../strongs/g/g1922.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="2peter_1_9"></a>2 Peter 1:9

But he that [mē](../../strongs/g/g3361.md) [pareimi](../../strongs/g/g3918.md) these things is [typhlos](../../strongs/g/g5185.md), and [myōpazō](../../strongs/g/g3467.md), and hath [lēthē](../../strongs/g/g3024.md) [lambanō](../../strongs/g/g2983.md) that he was [katharismos](../../strongs/g/g2512.md) from his [palai](../../strongs/g/g3819.md) [hamartia](../../strongs/g/g266.md).

<a name="2peter_1_10"></a>2 Peter 1:10

Wherefore the rather, [adelphos](../../strongs/g/g80.md), [spoudazō](../../strongs/g/g4704.md) to [poieō](../../strongs/g/g4160.md) your [klēsis](../../strongs/g/g2821.md) and [eklogē](../../strongs/g/g1589.md) [bebaios](../../strongs/g/g949.md): for if ye [poieō](../../strongs/g/g4160.md) these things, ye shall never [pote](../../strongs/g/g4218.md) [ptaiō](../../strongs/g/g4417.md):

<a name="2peter_1_11"></a>2 Peter 1:11

For so an [eisodos](../../strongs/g/g1529.md) shall be [epichorēgeō](../../strongs/g/g2023.md) unto you [plousiōs](../../strongs/g/g4146.md) into the [aiōnios](../../strongs/g/g166.md) [basileia](../../strongs/g/g932.md) of our [kyrios](../../strongs/g/g2962.md) and [sōtēr](../../strongs/g/g4990.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="2peter_1_12"></a>2 Peter 1:12

Wherefore I will not be [ameleō](../../strongs/g/g272.md) to [hypomimnēskō](../../strongs/g/g5279.md) you always of these things, though ye [eidō](../../strongs/g/g1492.md), and be [stērizō](../../strongs/g/g4741.md) in the [pareimi](../../strongs/g/g3918.md) [alētheia](../../strongs/g/g225.md).

<a name="2peter_1_13"></a>2 Peter 1:13

[de](../../strongs/g/g1161.md), I [hēgeomai](../../strongs/g/g2233.md) [dikaios](../../strongs/g/g1342.md), as long as I am in this [skēnōma](../../strongs/g/g4638.md), to [diegeirō](../../strongs/g/g1326.md) you by [hypomnēsis](../../strongs/g/g5280.md);

<a name="2peter_1_14"></a>2 Peter 1:14

[eidō](../../strongs/g/g1492.md) that [tachinos](../../strongs/g/g5031.md) I must [apothesis](../../strongs/g/g595.md) my [skēnōma](../../strongs/g/g4638.md), even as our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) hath [dēloō](../../strongs/g/g1213.md) me.

<a name="2peter_1_15"></a>2 Peter 1:15

Moreover I will [spoudazō](../../strongs/g/g4704.md) that ye may be able after my [exodos](../../strongs/g/g1841.md) to [poieō](../../strongs/g/g4160.md) these things [hekastote](../../strongs/g/g1539.md) in [mnēmē](../../strongs/g/g3420.md).

<a name="2peter_1_16"></a>2 Peter 1:16

For we have not [exakoloutheō](../../strongs/g/g1811.md) [sophizō](../../strongs/g/g4679.md) [mythos](../../strongs/g/g3454.md), when we [gnōrizō](../../strongs/g/g1107.md) unto you the [dynamis](../../strongs/g/g1411.md) and [parousia](../../strongs/g/g3952.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), but were [epoptēs](../../strongs/g/g2030.md) of his [megaleiotēs](../../strongs/g/g3168.md).

<a name="2peter_1_17"></a>2 Peter 1:17

For he [lambanō](../../strongs/g/g2983.md) from [theos](../../strongs/g/g2316.md) the [patēr](../../strongs/g/g3962.md) [timē](../../strongs/g/g5092.md) and [doxa](../../strongs/g/g1391.md), when there [pherō](../../strongs/g/g5342.md) [toiosde](../../strongs/g/g5107.md) a [phōnē](../../strongs/g/g5456.md) to him from the [megaloprepēs](../../strongs/g/g3169.md) [doxa](../../strongs/g/g1391.md), This is my [agapētos](../../strongs/g/g27.md) [huios](../../strongs/g/g5207.md), in whom I am [eudokeō](../../strongs/g/g2106.md).

<a name="2peter_1_18"></a>2 Peter 1:18

And this [phōnē](../../strongs/g/g5456.md) which [pherō](../../strongs/g/g5342.md) from [ouranos](../../strongs/g/g3772.md) we [akouō](../../strongs/g/g191.md), when we were with him in the [hagios](../../strongs/g/g40.md) [oros](../../strongs/g/g3735.md).

<a name="2peter_1_19"></a>2 Peter 1:19

We have also a [bebaios](../../strongs/g/g949.md) [logos](../../strongs/g/g3056.md) of [prophētikos](../../strongs/g/g4397.md); whereunto ye [poieō](../../strongs/g/g4160.md) [kalōs](../../strongs/g/g2573.md) that ye [prosechō](../../strongs/g/g4337.md), as unto a [lychnos](../../strongs/g/g3088.md) that [phainō](../../strongs/g/g5316.md) in an [auchmēros](../../strongs/g/g850.md) [topos](../../strongs/g/g5117.md), until the [hēmera](../../strongs/g/g2250.md) [diaugazō](../../strongs/g/g1306.md), and the [phōsphoros](../../strongs/g/g5459.md) [anatellō](../../strongs/g/g393.md) in your [kardia](../../strongs/g/g2588.md):

<a name="2peter_1_20"></a>2 Peter 1:20

[ginōskō](../../strongs/g/g1097.md) this first, that no [prophēteia](../../strongs/g/g4394.md) of the [graphē](../../strongs/g/g1124.md) is of any [idios](../../strongs/g/g2398.md) [epilysis](../../strongs/g/g1955.md).

<a name="2peter_1_21"></a>2 Peter 1:21

For the [prophēteia](../../strongs/g/g4394.md) [pherō](../../strongs/g/g5342.md) not in [pote](../../strongs/g/g4218.md) by the [thelēma](../../strongs/g/g2307.md) of [anthrōpos](../../strongs/g/g444.md): but [hagios](../../strongs/g/g40.md) [anthrōpos](../../strongs/g/g444.md) of [theos](../../strongs/g/g2316.md) [laleō](../../strongs/g/g2980.md) as they were [pherō](../../strongs/g/g5342.md) by the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md). [^1]

---

[Transliteral Bible](../bible.md)

[2 Peter](2peter.md)

[2 Peter 2](2peter_2.md)

---

[^1]: [2 Peter 1:21 Commentary](../../commentary/2peter/2peter_1_commentary.md#2peter_1_21)
