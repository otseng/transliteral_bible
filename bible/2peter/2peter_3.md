# [2 Peter 3](https://www.blueletterbible.org/kjv/2pe/3/1/s_1159001)

<a name="2peter_3_1"></a>2 Peter 3:1

This second [epistolē](../../strongs/g/g1992.md), [agapētos](../../strongs/g/g27.md), I now [graphō](../../strongs/g/g1125.md) unto you; in which I [diegeirō](../../strongs/g/g1326.md) your [eilikrinēs](../../strongs/g/g1506.md) [dianoia](../../strongs/g/g1271.md) by way of [hypomnēsis](../../strongs/g/g5280.md):

<a name="2peter_3_2"></a>2 Peter 3:2

That ye may be [mnaomai](../../strongs/g/g3415.md) of the [rhēma](../../strongs/g/g4487.md) which were [proereō](../../strongs/g/g4280.md) by the [hagios](../../strongs/g/g40.md) [prophētēs](../../strongs/g/g4396.md), and of the [entolē](../../strongs/g/g1785.md) of us the [apostolos](../../strongs/g/g652.md) of the [kyrios](../../strongs/g/g2962.md) and [sōtēr](../../strongs/g/g4990.md):

<a name="2peter_3_3"></a>2 Peter 3:3

[ginōskō](../../strongs/g/g1097.md) this first, that there shall [erchomai](../../strongs/g/g2064.md) in the [eschatos](../../strongs/g/g2078.md) [hēmera](../../strongs/g/g2250.md) [empaiktēs](../../strongs/g/g1703.md), [poreuō](../../strongs/g/g4198.md) after their own [epithymia](../../strongs/g/g1939.md),

<a name="2peter_3_4"></a>2 Peter 3:4

And [legō](../../strongs/g/g3004.md), Where is the [epaggelia](../../strongs/g/g1860.md) of his [parousia](../../strongs/g/g3952.md)? for since the [patēr](../../strongs/g/g3962.md) [koimaō](../../strongs/g/g2837.md), all things [houtō(s)](../../strongs/g/g3779.md) [diamenō](../../strongs/g/g1265.md) as from the [archē](../../strongs/g/g746.md) of the [ktisis](../../strongs/g/g2937.md).

<a name="2peter_3_5"></a>2 Peter 3:5

For this they [thelō](../../strongs/g/g2309.md) are [lanthanō](../../strongs/g/g2990.md), that by the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) the [ouranos](../../strongs/g/g3772.md) were of [ekpalai](../../strongs/g/g1597.md), and [gē](../../strongs/g/g1093.md) [synistēmi](../../strongs/g/g4921.md) out of [hydōr](../../strongs/g/g5204.md) and in [hydōr](../../strongs/g/g5204.md):

<a name="2peter_3_6"></a>2 Peter 3:6

Whereby the [kosmos](../../strongs/g/g2889.md) that then was, being [kataklyzō](../../strongs/g/g2626.md) with [hydōr](../../strongs/g/g5204.md), [apollymi](../../strongs/g/g622.md):

<a name="2peter_3_7"></a>2 Peter 3:7

But the [ouranos](../../strongs/g/g3772.md) and [gē](../../strongs/g/g1093.md), which are now, by the same [logos](../../strongs/g/g3056.md) are [thēsaurizō](../../strongs/g/g2343.md), [tēreō](../../strongs/g/g5083.md) unto [pyr](../../strongs/g/g4442.md) against the [hēmera](../../strongs/g/g2250.md) of [krisis](../../strongs/g/g2920.md) and [apōleia](../../strongs/g/g684.md) of [asebēs](../../strongs/g/g765.md) [anthrōpos](../../strongs/g/g444.md).

<a name="2peter_3_8"></a>2 Peter 3:8

But, [agapētos](../../strongs/g/g27.md), be not [lanthanō](../../strongs/g/g2990.md) of this one thing, that one [hēmera](../../strongs/g/g2250.md) with the [kyrios](../../strongs/g/g2962.md) as a [chilioi](../../strongs/g/g5507.md) [etos](../../strongs/g/g2094.md), and a [chilioi](../../strongs/g/g5507.md) [etos](../../strongs/g/g2094.md) as one [hēmera](../../strongs/g/g2250.md).

<a name="2peter_3_9"></a>2 Peter 3:9

The [kyrios](../../strongs/g/g2962.md) is not [bradynō](../../strongs/g/g1019.md) [epaggelia](../../strongs/g/g1860.md), as [tis](../../strongs/g/g5100.md) [hēgeomai](../../strongs/g/g2233.md) [bradytēs](../../strongs/g/g1022.md); but is [makrothymeō](../../strongs/g/g3114.md) to us-ward, not [boulomai](../../strongs/g/g1014.md) that any should [apollymi](../../strongs/g/g622.md), but that all should [chōreō](../../strongs/g/g5562.md) to [metanoia](../../strongs/g/g3341.md).

<a name="2peter_3_10"></a>2 Peter 3:10

But the [hēmera](../../strongs/g/g2250.md) of the [kyrios](../../strongs/g/g2962.md) will [hēkō](../../strongs/g/g2240.md) as a [kleptēs](../../strongs/g/g2812.md) in the [nyx](../../strongs/g/g3571.md); in the which the [ouranos](../../strongs/g/g3772.md) shall [parerchomai](../../strongs/g/g3928.md) with a [rhoizēdon](../../strongs/g/g4500.md), and the [stoicheion](../../strongs/g/g4747.md) shall [lyō](../../strongs/g/g3089.md) with [kausoō](../../strongs/g/g2741.md), the [gē](../../strongs/g/g1093.md) also and the [ergon](../../strongs/g/g2041.md) that are therein shall be [katakaiō](../../strongs/g/g2618.md).

<a name="2peter_3_11"></a>2 Peter 3:11

then all these things shall be [lyō](../../strongs/g/g3089.md), [potapos](../../strongs/g/g4217.md) ought ye to be in all [hagios](../../strongs/g/g40.md) [anastrophē](../../strongs/g/g391.md) and [eusebeia](../../strongs/g/g2150.md),

<a name="2peter_3_12"></a>2 Peter 3:12

[prosdokaō](../../strongs/g/g4328.md) and [speudō](../../strongs/g/g4692.md) unto the [parousia](../../strongs/g/g3952.md) of the [hēmera](../../strongs/g/g2250.md) of [theos](../../strongs/g/g2316.md), wherein the [ouranos](../../strongs/g/g3772.md) being on [pyroō](../../strongs/g/g4448.md) shall be [lyō](../../strongs/g/g3089.md), and the [stoicheion](../../strongs/g/g4747.md) shall [tēkō](../../strongs/g/g5080.md) with [kausoō](../../strongs/g/g2741.md)?

<a name="2peter_3_13"></a>2 Peter 3:13

Nevertheless we, according to his [epangelma](../../strongs/g/g1862.md), [prosdokaō](../../strongs/g/g4328.md) [kainos](../../strongs/g/g2537.md) [ouranos](../../strongs/g/g3772.md) and a [kainos](../../strongs/g/g2537.md) [gē](../../strongs/g/g1093.md), wherein [katoikeō](../../strongs/g/g2730.md) [dikaiosynē](../../strongs/g/g1343.md).

<a name="2peter_3_14"></a>2 Peter 3:14

Wherefore, [agapētos](../../strongs/g/g27.md), seeing that ye [prosdokaō](../../strongs/g/g4328.md) such things, be [spoudazō](../../strongs/g/g4704.md) that ye may be [heuriskō](../../strongs/g/g2147.md) of him in [eirēnē](../../strongs/g/g1515.md), [aspilos](../../strongs/g/g784.md), and [amōmētos](../../strongs/g/g298.md).

<a name="2peter_3_15"></a>2 Peter 3:15

And [hēgeomai](../../strongs/g/g2233.md) the [makrothymia](../../strongs/g/g3115.md) of our [kyrios](../../strongs/g/g2962.md) is [sōtēria](../../strongs/g/g4991.md); even as our [agapētos](../../strongs/g/g27.md) [adelphos](../../strongs/g/g80.md) [Paulos](../../strongs/g/g3972.md) also according to the [sophia](../../strongs/g/g4678.md) [didōmi](../../strongs/g/g1325.md) unto him hath [graphō](../../strongs/g/g1125.md) unto you;

<a name="2peter_3_16"></a>2 Peter 3:16

As also in all [epistolē](../../strongs/g/g1992.md), [laleō](../../strongs/g/g2980.md) in them of these things; in which are some things [dysnoētos](../../strongs/g/g1425.md), which they that are [amathēs](../../strongs/g/g261.md) and [astēriktos](../../strongs/g/g793.md) [strebloō](../../strongs/g/g4761.md), as also the [loipos](../../strongs/g/g3062.md) [graphē](../../strongs/g/g1124.md), unto their own [apōleia](../../strongs/g/g684.md).

<a name="2peter_3_17"></a>2 Peter 3:17

Ye therefore, [agapētos](../../strongs/g/g27.md), seeing ye [proginōskō](../../strongs/g/g4267.md), [phylassō](../../strongs/g/g5442.md) lest ye also, being [synapagō](../../strongs/g/g4879.md) with the [planē](../../strongs/g/g4106.md) of the [athesmos](../../strongs/g/g113.md), [ekpiptō](../../strongs/g/g1601.md) from your own [stērigmos](../../strongs/g/g4740.md).

<a name="2peter_3_18"></a>2 Peter 3:18

But [auxanō](../../strongs/g/g837.md) in [charis](../../strongs/g/g5485.md), and the [gnōsis](../../strongs/g/g1108.md) of our [kyrios](../../strongs/g/g2962.md) and [sōtēr](../../strongs/g/g4990.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md). To him [doxa](../../strongs/g/g1391.md) both [nyn](../../strongs/g/g3568.md) and [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) [hēmera](../../strongs/g/g2250.md). [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[2 Peter](2peter.md)

[2 Peter 2](2peter_2.md)