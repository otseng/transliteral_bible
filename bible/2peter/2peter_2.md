# [2 Peter 2](https://www.blueletterbible.org/kjv/2pe/2/1/s_1158001)

<a name="2peter_2_1"></a>2 Peter 2:1

But there were [pseudoprophētēs](../../strongs/g/g5578.md) also among the [laos](../../strongs/g/g2992.md), even as there shall be [pseudodidaskalos](../../strongs/g/g5572.md) among you, who shall [pareisagō](../../strongs/g/g3919.md) [apōleia](../../strongs/g/g684.md) [hairesis](../../strongs/g/g139.md), even [arneomai](../../strongs/g/g720.md) the [despotēs](../../strongs/g/g1203.md) that [agorazō](../../strongs/g/g59.md) them, and [epagō](../../strongs/g/g1863.md) upon themselves [tachinos](../../strongs/g/g5031.md) [apōleia](../../strongs/g/g684.md).

<a name="2peter_2_2"></a>2 Peter 2:2

And [polys](../../strongs/g/g4183.md) shall [exakoloutheō](../../strongs/g/g1811.md) their [apōleia](../../strongs/g/g684.md); by reason of whom the [hodos](../../strongs/g/g3598.md) of [alētheia](../../strongs/g/g225.md) shall [blasphēmeō](../../strongs/g/g987.md).

<a name="2peter_2_3"></a>2 Peter 2:3

And through [pleonexia](../../strongs/g/g4124.md) shall they with [plastos](../../strongs/g/g4112.md) [logos](../../strongs/g/g3056.md) [emporeuomai](../../strongs/g/g1710.md) of you: whose [krima](../../strongs/g/g2917.md) [ekpalai](../../strongs/g/g1597.md) [argeō](../../strongs/g/g691.md) not, and their [apōleia](../../strongs/g/g684.md) [nystazō](../../strongs/g/g3573.md) not.

<a name="2peter_2_4"></a>2 Peter 2:4

For if [theos](../../strongs/g/g2316.md) [pheidomai](../../strongs/g/g5339.md) not the [aggelos](../../strongs/g/g32.md) that [hamartanō](../../strongs/g/g264.md), but [tartaroō](../../strongs/g/g5020.md), and [paradidōmi](../../strongs/g/g3860.md) into [seira](../../strongs/g/g4577.md) of [zophos](../../strongs/g/g2217.md), to be [tēreō](../../strongs/g/g5083.md) unto [krisis](../../strongs/g/g2920.md);

<a name="2peter_2_5"></a>2 Peter 2:5

And [pheidomai](../../strongs/g/g5339.md) not the [archaios](../../strongs/g/g744.md) [kosmos](../../strongs/g/g2889.md), but [phylassō](../../strongs/g/g5442.md) [Nōe](../../strongs/g/g3575.md) the eighth, a [kēryx](../../strongs/g/g2783.md) of [dikaiosynē](../../strongs/g/g1343.md), [epagō](../../strongs/g/g1863.md) in the [kataklysmos](../../strongs/g/g2627.md) upon the [kosmos](../../strongs/g/g2889.md) of the [asebēs](../../strongs/g/g765.md);

<a name="2peter_2_6"></a>2 Peter 2:6

And [tephroō](../../strongs/g/g5077.md) the [polis](../../strongs/g/g4172.md) of [Sodoma](../../strongs/g/g4670.md) and [Gomorra](../../strongs/g/g1116.md) [katakrinō](../../strongs/g/g2632.md) with a [katastrophē](../../strongs/g/g2692.md), [tithēmi](../../strongs/g/g5087.md) an [hypodeigma](../../strongs/g/g5262.md) unto those that after should [asebeō](../../strongs/g/g764.md);

<a name="2peter_2_7"></a>2 Peter 2:7

And [rhyomai](../../strongs/g/g4506.md) [dikaios](../../strongs/g/g1342.md) [Lōt](../../strongs/g/g3091.md), [kataponeō](../../strongs/g/g2669.md) with the [aselgeia](../../strongs/g/g766.md) [anastrophē](../../strongs/g/g391.md) of the [athesmos](../../strongs/g/g113.md):

<a name="2peter_2_8"></a>2 Peter 2:8

(For that [dikaios](../../strongs/g/g1342.md) [egkatoikeō](../../strongs/g/g1460.md) among them, in [blemma](../../strongs/g/g990.md) and [akoē](../../strongs/g/g189.md), [basanizō](../../strongs/g/g928.md) his [dikaios](../../strongs/g/g1342.md) [psychē](../../strongs/g/g5590.md) from day to day with [anomos](../../strongs/g/g459.md) [ergon](../../strongs/g/g2041.md);)

<a name="2peter_2_9"></a>2 Peter 2:9

The [kyrios](../../strongs/g/g2962.md) [eidō](../../strongs/g/g1492.md) how to [rhyomai](../../strongs/g/g4506.md) the [eusebēs](../../strongs/g/g2152.md) out of [peirasmos](../../strongs/g/g3986.md), and to [tēreō](../../strongs/g/g5083.md) the [adikos](../../strongs/g/g94.md) unto the [hēmera](../../strongs/g/g2250.md) of [krisis](../../strongs/g/g2920.md) to be [kolazō](../../strongs/g/g2849.md):

<a name="2peter_2_10"></a>2 Peter 2:10

But [malista](../../strongs/g/g3122.md) them that [poreuō](../../strongs/g/g4198.md) after the [sarx](../../strongs/g/g4561.md) in the [epithymia](../../strongs/g/g1939.md) of [miasmos](../../strongs/g/g3394.md), and [kataphroneō](../../strongs/g/g2706.md) [kyriotēs](../../strongs/g/g2963.md). [tolmētēs](../../strongs/g/g5113.md) , [authadēs](../../strongs/g/g829.md), they are not [tremō](../../strongs/g/g5141.md) to [blasphēmeō](../../strongs/g/g987.md) of [doxa](../../strongs/g/g1391.md).

<a name="2peter_2_11"></a>2 Peter 2:11

Whereas [aggelos](../../strongs/g/g32.md), which are [meizōn](../../strongs/g/g3187.md) [ischys](../../strongs/g/g2479.md) and [dynamis](../../strongs/g/g1411.md), [pherō](../../strongs/g/g5342.md) not [blasphēmos](../../strongs/g/g989.md) [krisis](../../strongs/g/g2920.md) against them before the [kyrios](../../strongs/g/g2962.md).

<a name="2peter_2_12"></a>2 Peter 2:12

But these, as [physikos](../../strongs/g/g5446.md) [alogos](../../strongs/g/g249.md) [zōon](../../strongs/g/g2226.md), [gennaō](../../strongs/g/g1080.md) to be [halōsis](../../strongs/g/g259.md) and [phthora](../../strongs/g/g5356.md), [blasphēmeō](../../strongs/g/g987.md) of the things that they [agnoeō](../../strongs/g/g50.md); and shall [kataphtheirō](../../strongs/g/g2704.md) in their own [phthora](../../strongs/g/g5356.md);

<a name="2peter_2_13"></a>2 Peter 2:13

And shall [komizō](../../strongs/g/g2865.md) the [misthos](../../strongs/g/g3408.md) of [adikia](../../strongs/g/g93.md), as they that [hēgeomai](../../strongs/g/g2233.md) it [hēdonē](../../strongs/g/g2237.md) to [tryphē](../../strongs/g/g5172.md) in the [hēmera](../../strongs/g/g2250.md). [spilos](../../strongs/g/g4696.md) and [mōmos](../../strongs/g/g3470.md), [entryphaō](../../strongs/g/g1792.md) with their own [apatē](../../strongs/g/g539.md) while they [syneuōcheomai](../../strongs/g/g4910.md) you;

<a name="2peter_2_14"></a>2 Peter 2:14

Having [ophthalmos](../../strongs/g/g3788.md) [mestos](../../strongs/g/g3324.md) of [moichalis](../../strongs/g/g3428.md), and that [akatapaustos](../../strongs/g/g180.md) from [hamartia](../../strongs/g/g266.md); [deleazō](../../strongs/g/g1185.md) [astēriktos](../../strongs/g/g793.md) [psychē](../../strongs/g/g5590.md): a [kardia](../../strongs/g/g2588.md) they have [gymnazō](../../strongs/g/g1128.md) with [pleonexia](../../strongs/g/g4124.md); [katara](../../strongs/g/g2671.md) [teknon](../../strongs/g/g5043.md):

<a name="2peter_2_15"></a>2 Peter 2:15

Which have [kataleipō](../../strongs/g/g2641.md) the [euthys](../../strongs/g/g2117.md) [hodos](../../strongs/g/g3598.md), and are [planaō](../../strongs/g/g4105.md), [exakoloutheō](../../strongs/g/g1811.md) the way of [Balaam](../../strongs/g/g903.md) of [bosor](../../strongs/g/g1007.md), who [agapaō](../../strongs/g/g25.md) the [misthos](../../strongs/g/g3408.md) of [adikia](../../strongs/g/g93.md);

<a name="2peter_2_16"></a>2 Peter 2:16

But was [elegxis](../../strongs/g/g1649.md) for his [paranomia](../../strongs/g/g3892.md): the [aphōnos](../../strongs/g/g880.md) [hypozygion](../../strongs/g/g5268.md) [phtheggomai](../../strongs/g/g5350.md) with [anthrōpos](../../strongs/g/g444.md) [phōnē](../../strongs/g/g5456.md) [kōlyō](../../strongs/g/g2967.md) the [paraphronia](../../strongs/g/g3913.md) of the [prophētēs](../../strongs/g/g4396.md).

<a name="2peter_2_17"></a>2 Peter 2:17

These are [pēgē](../../strongs/g/g4077.md) [anydros](../../strongs/g/g504.md), [nephelē](../../strongs/g/g3507.md) that are [elaunō](../../strongs/g/g1643.md) with a [lailaps](../../strongs/g/g2978.md); to whom the [zophos](../../strongs/g/g2217.md) of [skotos](../../strongs/g/g4655.md) is [tēreō](../../strongs/g/g5083.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md).

<a name="2peter_2_18"></a>2 Peter 2:18

For when they [phtheggomai](../../strongs/g/g5350.md) [hyperogkos](../../strongs/g/g5246.md) of [mataiotēs](../../strongs/g/g3153.md), they [deleazō](../../strongs/g/g1185.md) through the [epithymia](../../strongs/g/g1939.md) of the [sarx](../../strongs/g/g4561.md), [aselgeia](../../strongs/g/g766.md), those that were [ontōs](../../strongs/g/g3689.md) [apopheugō](../../strongs/g/g668.md) from them who [anastrephō](../../strongs/g/g390.md) in [planē](../../strongs/g/g4106.md).

<a name="2peter_2_19"></a>2 Peter 2:19

While they [epaggellomai](../../strongs/g/g1861.md) them [eleutheria](../../strongs/g/g1657.md), they themselves are the [doulos](../../strongs/g/g1401.md) of [phthora](../../strongs/g/g5356.md): for of whom a man is [hēttaomai](../../strongs/g/g2274.md), of the same is he [douloō](../../strongs/g/g1402.md).

<a name="2peter_2_20"></a>2 Peter 2:20

For if after they have [apopheugō](../../strongs/g/g668.md) the [miasma](../../strongs/g/g3393.md) of the [kosmos](../../strongs/g/g2889.md) through the [epignōsis](../../strongs/g/g1922.md) of the [kyrios](../../strongs/g/g2962.md) and [sōtēr](../../strongs/g/g4990.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), they are again [emplekō](../../strongs/g/g1707.md) therein, and [ginomai](../../strongs/g/g1096.md) [hēttaomai](../../strongs/g/g2274.md), the [eschatos](../../strongs/g/g2078.md) is [cheirōn](../../strongs/g/g5501.md) with them than the [prōtos](../../strongs/g/g4413.md).

<a name="2peter_2_21"></a>2 Peter 2:21

For it had been [kreittōn](../../strongs/g/g2909.md) for them not to have [epiginōskō](../../strongs/g/g1921.md) the [hodos](../../strongs/g/g3598.md) of [dikaiosynē](../../strongs/g/g1343.md), than, after they have [epiginōskō](../../strongs/g/g1921.md), to [epistrephō](../../strongs/g/g1994.md) from the [hagios](../../strongs/g/g40.md) [entolē](../../strongs/g/g1785.md) [paradidōmi](../../strongs/g/g3860.md) unto them.

<a name="2peter_2_22"></a>2 Peter 2:22

But [symbainō](../../strongs/g/g4819.md) unto them according to the [alēthēs](../../strongs/g/g227.md) [paroimia](../../strongs/g/g3942.md), The [kyōn](../../strongs/g/g2965.md) [epistrephō](../../strongs/g/g1994.md) to his own [exerama](../../strongs/g/g1829.md) again; and the [hys](../../strongs/g/g5300.md) that was [louō](../../strongs/g/g3068.md) to her [kylismos](../../strongs/g/g2946.md) in the [borboros](../../strongs/g/g1004.md).

---

[Transliteral Bible](../bible.md)

[2 Peter](2peter.md)

[2 Peter 1](2peter_1.md) - [2 Peter 3](2peter_3.md)