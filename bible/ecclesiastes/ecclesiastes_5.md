# [Ecclesiastes 5](https://www.blueletterbible.org/kjv/ecclesiastes/5)

<a name="ecclesiastes_5_1"></a>Ecclesiastes 5:1

[shamar](../../strongs/h/h8104.md) thy [regel](../../strongs/h/h7272.md) when thou [yālaḵ](../../strongs/h/h3212.md) to the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), and be more [qarowb](../../strongs/h/h7138.md) to [shama'](../../strongs/h/h8085.md), than to [nathan](../../strongs/h/h5414.md) the [zebach](../../strongs/h/h2077.md) of [kᵊsîl](../../strongs/h/h3684.md): for they [yada'](../../strongs/h/h3045.md) not that they ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md).

<a name="ecclesiastes_5_2"></a>Ecclesiastes 5:2

Be not [bahal](../../strongs/h/h926.md) with thy [peh](../../strongs/h/h6310.md), and let not thine [leb](../../strongs/h/h3820.md) be [māhar](../../strongs/h/h4116.md) to [yāṣā'](../../strongs/h/h3318.md) any [dabar](../../strongs/h/h1697.md) [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md): for ['Elohiym](../../strongs/h/h430.md) is in [shamayim](../../strongs/h/h8064.md), and thou upon ['erets](../../strongs/h/h776.md): therefore let thy [dabar](../../strongs/h/h1697.md) be [mᵊʿaṭ](../../strongs/h/h4592.md).

<a name="ecclesiastes_5_3"></a>Ecclesiastes 5:3

For a [ḥălôm](../../strongs/h/h2472.md) [bow'](../../strongs/h/h935.md) through the [rōḇ](../../strongs/h/h7230.md) of [ʿinyān](../../strongs/h/h6045.md); and a [kᵊsîl](../../strongs/h/h3684.md) [qowl](../../strongs/h/h6963.md) is known by [rōḇ](../../strongs/h/h7230.md) of [dabar](../../strongs/h/h1697.md).

<a name="ecclesiastes_5_4"></a>Ecclesiastes 5:4

When thou [nāḏar](../../strongs/h/h5087.md) a [neḏer](../../strongs/h/h5088.md) unto ['Elohiym](../../strongs/h/h430.md), ['āḥar](../../strongs/h/h309.md) not to [shalam](../../strongs/h/h7999.md) it; for he hath no [chephets](../../strongs/h/h2656.md) in [kᵊsîl](../../strongs/h/h3684.md): [shalam](../../strongs/h/h7999.md) that which thou hast [nāḏar](../../strongs/h/h5087.md).

<a name="ecclesiastes_5_5"></a>Ecclesiastes 5:5

[towb](../../strongs/h/h2896.md) is it that thou shouldest not [nāḏar](../../strongs/h/h5087.md), than that thou shouldest [nāḏar](../../strongs/h/h5087.md) and not [shalam](../../strongs/h/h7999.md).

<a name="ecclesiastes_5_6"></a>Ecclesiastes 5:6

[nathan](../../strongs/h/h5414.md) not thy [peh](../../strongs/h/h6310.md) to cause thy [basar](../../strongs/h/h1320.md) to [chata'](../../strongs/h/h2398.md); neither ['āmar](../../strongs/h/h559.md) thou [paniym](../../strongs/h/h6440.md) the [mal'ak](../../strongs/h/h4397.md), that it was an [šᵊḡāḡâ](../../strongs/h/h7684.md): wherefore should ['Elohiym](../../strongs/h/h430.md) be [qāṣap̄](../../strongs/h/h7107.md) at thy [qowl](../../strongs/h/h6963.md), and [chabal](../../strongs/h/h2254.md) the [ma'aseh](../../strongs/h/h4639.md) of thine [yad](../../strongs/h/h3027.md)?

<a name="ecclesiastes_5_7"></a>Ecclesiastes 5:7

For in the [rōḇ](../../strongs/h/h7230.md) of [ḥălôm](../../strongs/h/h2472.md) and [rabah](../../strongs/h/h7235.md) [dabar](../../strongs/h/h1697.md) there are also [heḇel](../../strongs/h/h1892.md): but [yare'](../../strongs/h/h3372.md) thou ['Elohiym](../../strongs/h/h430.md).

<a name="ecclesiastes_5_8"></a>Ecclesiastes 5:8

If thou [ra'ah](../../strongs/h/h7200.md) the [ʿōšeq](../../strongs/h/h6233.md) of the [rûš](../../strongs/h/h7326.md), and [gēzel](../../strongs/h/h1499.md) of [mishpat](../../strongs/h/h4941.md) and [tsedeq](../../strongs/h/h6664.md) in a [mᵊḏînâ](../../strongs/h/h4082.md), [tāmah](../../strongs/h/h8539.md) not at the [chephets](../../strongs/h/h2656.md): for he that is [gāḇōha](../../strongs/h/h1364.md) than the [gāḇōha](../../strongs/h/h1364.md) [shamar](../../strongs/h/h8104.md); and there be [gāḇōha](../../strongs/h/h1364.md) than they.

<a name="ecclesiastes_5_9"></a>Ecclesiastes 5:9

Moreover the [yiṯrôn](../../strongs/h/h3504.md) of the ['erets](../../strongs/h/h776.md) is for all: the [melek](../../strongs/h/h4428.md) himself is ['abad](../../strongs/h/h5647.md) by the [sadeh](../../strongs/h/h7704.md).

<a name="ecclesiastes_5_10"></a>Ecclesiastes 5:10

He that ['ahab](../../strongs/h/h157.md) [keceph](../../strongs/h/h3701.md) shall not be [sāׂbaʿ](../../strongs/h/h7646.md) with [keceph](../../strongs/h/h3701.md); nor he that ['ahab](../../strongs/h/h157.md) [hāmôn](../../strongs/h/h1995.md) with [tᵊḇû'â](../../strongs/h/h8393.md): this is also [heḇel](../../strongs/h/h1892.md).

<a name="ecclesiastes_5_11"></a>Ecclesiastes 5:11

When [towb](../../strongs/h/h2896.md) [rabah](../../strongs/h/h7235.md), they are [rabab](../../strongs/h/h7231.md) that ['akal](../../strongs/h/h398.md) them: and what [kišrôn](../../strongs/h/h3788.md) is there to the [baʿal](../../strongs/h/h1167.md) thereof, saving the [ra'ăvâ](../../strongs/h/h7207.md) [rᵊ'îṯ](../../strongs/h/h7212.md) of them with their ['ayin](../../strongs/h/h5869.md)?

<a name="ecclesiastes_5_12"></a>Ecclesiastes 5:12

The [šēnā'](../../strongs/h/h8142.md) of a ['abad](../../strongs/h/h5647.md) is [māṯôq](../../strongs/h/h4966.md), whether he ['akal](../../strongs/h/h398.md) [mᵊʿaṭ](../../strongs/h/h4592.md) or [rabah](../../strongs/h/h7235.md): but the [śāḇāʿ](../../strongs/h/h7647.md) of the [ʿāšîr](../../strongs/h/h6223.md) will not [yānaḥ](../../strongs/h/h3240.md) him to [yashen](../../strongs/h/h3462.md).

<a name="ecclesiastes_5_13"></a>Ecclesiastes 5:13

There is a [ḥālâ](../../strongs/h/h2470.md) [ra'](../../strongs/h/h7451.md) which I have [ra'ah](../../strongs/h/h7200.md) under the [šemeš](../../strongs/h/h8121.md), namely, [ʿōšer](../../strongs/h/h6239.md) [shamar](../../strongs/h/h8104.md) for the [baʿal](../../strongs/h/h1167.md) thereof to their [ra'](../../strongs/h/h7451.md).

<a name="ecclesiastes_5_14"></a>Ecclesiastes 5:14

But those [ʿōšer](../../strongs/h/h6239.md) ['abad](../../strongs/h/h6.md) by [ra'](../../strongs/h/h7451.md) [ʿinyān](../../strongs/h/h6045.md): and he [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md), and there is [mᵊ'ûmâ](../../strongs/h/h3972.md) in his [yad](../../strongs/h/h3027.md).

<a name="ecclesiastes_5_15"></a>Ecclesiastes 5:15

As he [yāṣā'](../../strongs/h/h3318.md) of his ['em](../../strongs/h/h517.md) [beten](../../strongs/h/h990.md), ['arowm](../../strongs/h/h6174.md) shall he [shuwb](../../strongs/h/h7725.md) to [yālaḵ](../../strongs/h/h3212.md) as he [bow'](../../strongs/h/h935.md), and shall [nasa'](../../strongs/h/h5375.md) [mᵊ'ûmâ](../../strongs/h/h3972.md) of his ['amal](../../strongs/h/h5999.md), which he may [yālaḵ](../../strongs/h/h3212.md) in his [yad](../../strongs/h/h3027.md).

<a name="ecclesiastes_5_16"></a>Ecclesiastes 5:16

And [zô](../../strongs/h/h2090.md) also is a [ḥālâ](../../strongs/h/h2470.md) [ra'](../../strongs/h/h7451.md), that in all [ʿummâ](../../strongs/h/h5980.md) as he [bow'](../../strongs/h/h935.md), so shall he [yālaḵ](../../strongs/h/h3212.md): and what [yiṯrôn](../../strongs/h/h3504.md) hath he that hath [ʿāmal](../../strongs/h/h5998.md) for the [ruwach](../../strongs/h/h7307.md)?

<a name="ecclesiastes_5_17"></a>Ecclesiastes 5:17

All his [yowm](../../strongs/h/h3117.md) also he ['akal](../../strongs/h/h398.md) in [choshek](../../strongs/h/h2822.md), and he hath [rabah](../../strongs/h/h7235.md) [kāʿas](../../strongs/h/h3707.md) and [qeṣep̄](../../strongs/h/h7110.md) with his [ḥŏlî](../../strongs/h/h2483.md).

<a name="ecclesiastes_5_18"></a>Ecclesiastes 5:18

Behold that which I have [ra'ah](../../strongs/h/h7200.md): it is [towb](../../strongs/h/h2896.md) and [yāp̄ê](../../strongs/h/h3303.md) for one to ['akal](../../strongs/h/h398.md) and to [šāṯâ](../../strongs/h/h8354.md), and to [ra'ah](../../strongs/h/h7200.md) the [towb](../../strongs/h/h2896.md) of all his ['amal](../../strongs/h/h5999.md) that he [ʿāmal](../../strongs/h/h5998.md) under the [šemeš](../../strongs/h/h8121.md) [mispār](../../strongs/h/h4557.md) the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md), which ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) him: for it is his [cheleq](../../strongs/h/h2506.md).

<a name="ecclesiastes_5_19"></a>Ecclesiastes 5:19

Every ['āḏām](../../strongs/h/h120.md) also to whom ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) [ʿōšer](../../strongs/h/h6239.md) and [neḵes](../../strongs/h/h5233.md), and hath given him [šālaṭ](../../strongs/h/h7980.md) to ['akal](../../strongs/h/h398.md) thereof, and to [nasa'](../../strongs/h/h5375.md) his [cheleq](../../strongs/h/h2506.md), and to [samach](../../strongs/h/h8055.md) in his ['amal](../../strongs/h/h5999.md); [zô](../../strongs/h/h2090.md) is the [mataṯ](../../strongs/h/h4991.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="ecclesiastes_5_20"></a>Ecclesiastes 5:20

For he shall not [rabah](../../strongs/h/h7235.md) [zakar](../../strongs/h/h2142.md) the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md); because ['Elohiym](../../strongs/h/h430.md) [ʿānâ](../../strongs/h/h6031.md) him in the [simchah](../../strongs/h/h8057.md) of his [leb](../../strongs/h/h3820.md).

---

[Transliteral Bible](../bible.md)

[Ecclesiastes](ecclesiastes.md)

[Ecclesiastes 4](ecclesiastes_4.md) - [Ecclesiastes 6](ecclesiastes_6.md)