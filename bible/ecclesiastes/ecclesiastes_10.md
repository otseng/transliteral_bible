# [Ecclesiastes 10](https://www.blueletterbible.org/kjv/ecclesiastes/10)

<a name="ecclesiastes_10_1"></a>Ecclesiastes 10:1

[maveth](../../strongs/h/h4194.md) [zᵊḇûḇ](../../strongs/h/h2070.md) cause the [šemen](../../strongs/h/h8081.md) of the [rāqaḥ](../../strongs/h/h7543.md) to send [nāḇaʿ](../../strongs/h/h5042.md) a [bā'aš](../../strongs/h/h887.md): so doth a [mᵊʿaṭ](../../strongs/h/h4592.md) [siḵlûṯ](../../strongs/h/h5531.md) him that is in [yāqār](../../strongs/h/h3368.md) for [ḥāḵmâ](../../strongs/h/h2451.md) and [kabowd](../../strongs/h/h3519.md).

<a name="ecclesiastes_10_2"></a>Ecclesiastes 10:2

A [ḥāḵām](../../strongs/h/h2450.md) [leb](../../strongs/h/h3820.md) is at his [yamiyn](../../strongs/h/h3225.md); but a [kᵊsîl](../../strongs/h/h3684.md) [leb](../../strongs/h/h3820.md) at his [śᵊmō'l](../../strongs/h/h8040.md).

<a name="ecclesiastes_10_3"></a>Ecclesiastes 10:3

Yea also, when he that is a [sāḵāl](../../strongs/h/h5530.md) [halak](../../strongs/h/h1980.md) by the [derek](../../strongs/h/h1870.md), his [leb](../../strongs/h/h3820.md) [ḥāsēr](../../strongs/h/h2638.md) him, and he ['āmar](../../strongs/h/h559.md) to every one that he is a [sāḵāl](../../strongs/h/h5530.md).

<a name="ecclesiastes_10_4"></a>Ecclesiastes 10:4

If the [ruwach](../../strongs/h/h7307.md) of the [mashal](../../strongs/h/h4910.md) [ʿālâ](../../strongs/h/h5927.md) against thee, [yānaḥ](../../strongs/h/h3240.md) not thy [maqowm](../../strongs/h/h4725.md); for [marpē'](../../strongs/h/h4832.md) [yānaḥ](../../strongs/h/h3240.md) [gadowl](../../strongs/h/h1419.md) [ḥēṭĕ'](../../strongs/h/h2399.md).

<a name="ecclesiastes_10_5"></a>Ecclesiastes 10:5

There is an [ra'](../../strongs/h/h7451.md) which I have [ra'ah](../../strongs/h/h7200.md) under the [šemeš](../../strongs/h/h8121.md), as an [šᵊḡāḡâ](../../strongs/h/h7684.md) which [yāṣā'](../../strongs/h/h3318.md) [paniym](../../strongs/h/h6440.md) the [šallîṭ](../../strongs/h/h7989.md):

<a name="ecclesiastes_10_6"></a>Ecclesiastes 10:6

[seḵel](../../strongs/h/h5529.md) is [nathan](../../strongs/h/h5414.md) in [rab](../../strongs/h/h7227.md) [marowm](../../strongs/h/h4791.md), and the [ʿāšîr](../../strongs/h/h6223.md) [yashab](../../strongs/h/h3427.md) in low [šep̄el](../../strongs/h/h8216.md).

<a name="ecclesiastes_10_7"></a>Ecclesiastes 10:7

I have [ra'ah](../../strongs/h/h7200.md) ['ebed](../../strongs/h/h5650.md) upon [sûs](../../strongs/h/h5483.md), and [śar](../../strongs/h/h8269.md) [halak](../../strongs/h/h1980.md) as ['ebed](../../strongs/h/h5650.md) upon the ['erets](../../strongs/h/h776.md).

<a name="ecclesiastes_10_8"></a>Ecclesiastes 10:8

He that [chaphar](../../strongs/h/h2658.md) a [gûmmāṣ](../../strongs/h/h1475.md) shall [naphal](../../strongs/h/h5307.md) into it; and whoso [pāraṣ](../../strongs/h/h6555.md) an [gāḏēr](../../strongs/h/h1447.md), a [nachash](../../strongs/h/h5175.md) shall [nāšaḵ](../../strongs/h/h5391.md) him.

<a name="ecclesiastes_10_9"></a>Ecclesiastes 10:9

Whoso [nāsaʿ](../../strongs/h/h5265.md) ['eben](../../strongs/h/h68.md) shall be [ʿāṣaḇ](../../strongs/h/h6087.md) therewith; and he that [bāqaʿ](../../strongs/h/h1234.md) ['ets](../../strongs/h/h6086.md) shall be [sāḵan](../../strongs/h/h5533.md) thereby.

<a name="ecclesiastes_10_10"></a>Ecclesiastes 10:10

If the [barzel](../../strongs/h/h1270.md) be [qāhâ](../../strongs/h/h6949.md), and he do not [qālal](../../strongs/h/h7043.md) the [paniym](../../strongs/h/h6440.md), then must he [gabar](../../strongs/h/h1396.md) to more [ḥayil](../../strongs/h/h2428.md): but [ḥāḵmâ](../../strongs/h/h2451.md) is [yiṯrôn](../../strongs/h/h3504.md) to [kāšēr](../../strongs/h/h3787.md).

<a name="ecclesiastes_10_11"></a>Ecclesiastes 10:11

Surely the [nachash](../../strongs/h/h5175.md) will [nāšaḵ](../../strongs/h/h5391.md) without [laḥaš](../../strongs/h/h3908.md); and a [lashown](../../strongs/h/h3956.md) [baʿal](../../strongs/h/h1167.md) is no [yiṯrôn](../../strongs/h/h3504.md).

<a name="ecclesiastes_10_12"></a>Ecclesiastes 10:12

The [dabar](../../strongs/h/h1697.md) of a [ḥāḵām](../../strongs/h/h2450.md) [peh](../../strongs/h/h6310.md) are [ḥēn](../../strongs/h/h2580.md); but the [saphah](../../strongs/h/h8193.md) of a [kᵊsîl](../../strongs/h/h3684.md) will [bālaʿ](../../strongs/h/h1104.md) himself.

<a name="ecclesiastes_10_13"></a>Ecclesiastes 10:13

The [tᵊḥillâ](../../strongs/h/h8462.md) of the [dabar](../../strongs/h/h1697.md) of his [peh](../../strongs/h/h6310.md) is [siḵlûṯ](../../strongs/h/h5531.md): and the ['aḥărîṯ](../../strongs/h/h319.md) of his [peh](../../strongs/h/h6310.md) is [ra'](../../strongs/h/h7451.md) [hôlēlûṯ](../../strongs/h/h1948.md).

<a name="ecclesiastes_10_14"></a>Ecclesiastes 10:14

A [sāḵāl](../../strongs/h/h5530.md) also is [rabah](../../strongs/h/h7235.md) of [dabar](../../strongs/h/h1697.md): a ['āḏām](../../strongs/h/h120.md) cannot [yada'](../../strongs/h/h3045.md) what shall be; and what shall be ['aḥar](../../strongs/h/h310.md) him, who can [nāḡaḏ](../../strongs/h/h5046.md) him?

<a name="ecclesiastes_10_15"></a>Ecclesiastes 10:15

The ['amal](../../strongs/h/h5999.md) of the [kᵊsîl](../../strongs/h/h3684.md) [yaga'](../../strongs/h/h3021.md) every one of them, because he [yada'](../../strongs/h/h3045.md) not how to [yālaḵ](../../strongs/h/h3212.md) to the [ʿîr](../../strongs/h/h5892.md).

<a name="ecclesiastes_10_16"></a>Ecclesiastes 10:16

['î](../../strongs/h/h337.md) to thee, O ['erets](../../strongs/h/h776.md), when thy [melek](../../strongs/h/h4428.md) is a [naʿar](../../strongs/h/h5288.md), and thy [śar](../../strongs/h/h8269.md) ['akal](../../strongs/h/h398.md) in the [boqer](../../strongs/h/h1242.md)!

<a name="ecclesiastes_10_17"></a>Ecclesiastes 10:17

['esher](../../strongs/h/h835.md) art thou, O ['erets](../../strongs/h/h776.md), when thy [melek](../../strongs/h/h4428.md) is the [ben](../../strongs/h/h1121.md) of [ḥōr](../../strongs/h/h2715.md), and thy [śar](../../strongs/h/h8269.md) ['akal](../../strongs/h/h398.md) in due [ʿēṯ](../../strongs/h/h6256.md), for [gᵊḇûrâ](../../strongs/h/h1369.md), and not for [šᵊṯî](../../strongs/h/h8358.md)!

<a name="ecclesiastes_10_18"></a>Ecclesiastes 10:18

By much [ʿaṣlâ](../../strongs/h/h6103.md) the [mᵊqārê](../../strongs/h/h4746.md) [māḵaḵ](../../strongs/h/h4355.md); and through [šip̄lûṯ](../../strongs/h/h8220.md) of the [yad](../../strongs/h/h3027.md) the [bayith](../../strongs/h/h1004.md) droppeth [dālap̄](../../strongs/h/h1811.md).

<a name="ecclesiastes_10_19"></a>Ecclesiastes 10:19

A [lechem](../../strongs/h/h3899.md) is ['asah](../../strongs/h/h6213.md) for [śᵊḥôq](../../strongs/h/h7814.md), and [yayin](../../strongs/h/h3196.md) maketh [samach](../../strongs/h/h8055.md) [chay](../../strongs/h/h2416.md): but [keceph](../../strongs/h/h3701.md) ['anah](../../strongs/h/h6030.md) all things.

<a name="ecclesiastes_10_20"></a>Ecclesiastes 10:20

[qālal](../../strongs/h/h7043.md) not the [melek](../../strongs/h/h4428.md), no not in thy [madāʿ](../../strongs/h/h4093.md); and [qālal](../../strongs/h/h7043.md) not the [ʿāšîr](../../strongs/h/h6223.md) in thy [ḥeḏer](../../strongs/h/h2315.md) [miškāḇ](../../strongs/h/h4904.md): for a [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md) shall [yālaḵ](../../strongs/h/h3212.md) the [qowl](../../strongs/h/h6963.md), and that which [baʿal](../../strongs/h/h1167.md) [kanaph](../../strongs/h/h3671.md) shall [nāḡaḏ](../../strongs/h/h5046.md) the [dabar](../../strongs/h/h1697.md).

---

[Transliteral Bible](../bible.md)

[Ecclesiastes](ecclesiastes.md)

[Ecclesiastes 9](ecclesiastes_9.md) - [Ecclesiastes 11](ecclesiastes_11.md)