# [Ecclesiastes 2](https://www.blueletterbible.org/kjv/ecclesiastes/2)

<a name="ecclesiastes_2_1"></a>Ecclesiastes 2:1

I ['āmar](../../strongs/h/h559.md) in mine [leb](../../strongs/h/h3820.md), [yālaḵ](../../strongs/h/h3212.md), I will [nāsâ](../../strongs/h/h5254.md) thee with [simchah](../../strongs/h/h8057.md), therefore [ra'ah](../../strongs/h/h7200.md) [towb](../../strongs/h/h2896.md): and, behold, this also is [heḇel](../../strongs/h/h1892.md).

<a name="ecclesiastes_2_2"></a>Ecclesiastes 2:2

I ['āmar](../../strongs/h/h559.md) of [śᵊḥôq](../../strongs/h/h7814.md), It is [halal](../../strongs/h/h1984.md): and of [simchah](../../strongs/h/h8057.md), [zô](../../strongs/h/h2090.md) ['asah](../../strongs/h/h6213.md) it?

<a name="ecclesiastes_2_3"></a>Ecclesiastes 2:3

I [tûr](../../strongs/h/h8446.md) in mine [leb](../../strongs/h/h3820.md) to [mashak](../../strongs/h/h4900.md) [basar](../../strongs/h/h1320.md) unto [yayin](../../strongs/h/h3196.md), yet [nāhaḡ](../../strongs/h/h5090.md) mine [leb](../../strongs/h/h3820.md) with [ḥāḵmâ](../../strongs/h/h2451.md); and to ['āḥaz](../../strongs/h/h270.md) on [siḵlûṯ](../../strongs/h/h5531.md), till I might [ra'ah](../../strongs/h/h7200.md) what was that [towb](../../strongs/h/h2896.md) for the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), which they should ['asah](../../strongs/h/h6213.md) under the [shamayim](../../strongs/h/h8064.md) [mispār](../../strongs/h/h4557.md) the [yowm](../../strongs/h/h3117.md) of their [chay](../../strongs/h/h2416.md).

<a name="ecclesiastes_2_4"></a>Ecclesiastes 2:4

I made me [gāḏal](../../strongs/h/h1431.md) [ma'aseh](../../strongs/h/h4639.md); I [bānâ](../../strongs/h/h1129.md) me [bayith](../../strongs/h/h1004.md); I [nāṭaʿ](../../strongs/h/h5193.md) me [kerem](../../strongs/h/h3754.md):

<a name="ecclesiastes_2_5"></a>Ecclesiastes 2:5

I ['asah](../../strongs/h/h6213.md) me [gannâ](../../strongs/h/h1593.md) and [pardēs](../../strongs/h/h6508.md), and I [nāṭaʿ](../../strongs/h/h5193.md) ['ets](../../strongs/h/h6086.md) in them of all kind of [pĕriy](../../strongs/h/h6529.md):

<a name="ecclesiastes_2_6"></a>Ecclesiastes 2:6

I ['asah](../../strongs/h/h6213.md) me [bᵊrēḵâ](../../strongs/h/h1295.md) of [mayim](../../strongs/h/h4325.md), to [šāqâ](../../strongs/h/h8248.md) therewith the [yaʿar](../../strongs/h/h3293.md) that [ṣāmaḥ](../../strongs/h/h6779.md) ['ets](../../strongs/h/h6086.md):

<a name="ecclesiastes_2_7"></a>Ecclesiastes 2:7

I [qānâ](../../strongs/h/h7069.md) me ['ebed](../../strongs/h/h5650.md) and [šip̄ḥâ](../../strongs/h/h8198.md), and had [ben](../../strongs/h/h1121.md) in my [bayith](../../strongs/h/h1004.md); also I had [rabah](../../strongs/h/h7235.md) [miqnê](../../strongs/h/h4735.md) of [bāqār](../../strongs/h/h1241.md) and [tso'n](../../strongs/h/h6629.md) above all that were in [Yĕruwshalaim](../../strongs/h/h3389.md) [paniym](../../strongs/h/h6440.md) me:

<a name="ecclesiastes_2_8"></a>Ecclesiastes 2:8

I [kānas](../../strongs/h/h3664.md) me also [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md), and the peculiar [sᵊḡullâ](../../strongs/h/h5459.md) of [melek](../../strongs/h/h4428.md) and of the [mᵊḏînâ](../../strongs/h/h4082.md): I ['asah](../../strongs/h/h6213.md) me men [shiyr](../../strongs/h/h7891.md) and women [shiyr](../../strongs/h/h7891.md), and the [taʿănûḡ](../../strongs/h/h8588.md) of the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), as [šidâ](../../strongs/h/h7705.md) [šidâ](../../strongs/h/h7705.md), and that of all sorts.

<a name="ecclesiastes_2_9"></a>Ecclesiastes 2:9

So I was [gāḏal](../../strongs/h/h1431.md), and increased more than all that were [paniym](../../strongs/h/h6440.md) me in [Yĕruwshalaim](../../strongs/h/h3389.md): also my [ḥāḵmâ](../../strongs/h/h2451.md) ['amad](../../strongs/h/h5975.md) with me.

<a name="ecclesiastes_2_10"></a>Ecclesiastes 2:10

And whatsoever mine ['ayin](../../strongs/h/h5869.md) [sha'al](../../strongs/h/h7592.md) I ['āṣal](../../strongs/h/h680.md) not from them, I [mānaʿ](../../strongs/h/h4513.md) not my [leb](../../strongs/h/h3820.md) from any [simchah](../../strongs/h/h8057.md); for my [leb](../../strongs/h/h3820.md) [śāmēaḥ](../../strongs/h/h8056.md) in all my ['amal](../../strongs/h/h5999.md): and this was my [cheleq](../../strongs/h/h2506.md) of all my ['amal](../../strongs/h/h5999.md).

<a name="ecclesiastes_2_11"></a>Ecclesiastes 2:11

Then I [panah](../../strongs/h/h6437.md) on all the [ma'aseh](../../strongs/h/h4639.md) that my [yad](../../strongs/h/h3027.md) had ['asah](../../strongs/h/h6213.md), and on the ['amal](../../strongs/h/h5999.md) that I had [ʿāmal](../../strongs/h/h5998.md) to ['asah](../../strongs/h/h6213.md): and, behold, all was [heḇel](../../strongs/h/h1892.md) and [rᵊʿûṯ](../../strongs/h/h7469.md) of [ruwach](../../strongs/h/h7307.md), and there was no [yiṯrôn](../../strongs/h/h3504.md) under the [šemeš](../../strongs/h/h8121.md).

<a name="ecclesiastes_2_12"></a>Ecclesiastes 2:12

And I [panah](../../strongs/h/h6437.md) myself to [ra'ah](../../strongs/h/h7200.md) [ḥāḵmâ](../../strongs/h/h2451.md), and [hôlēlâ](../../strongs/h/h1947.md), and [siḵlûṯ](../../strongs/h/h5531.md): for what can the ['āḏām](../../strongs/h/h120.md) do that [bow'](../../strongs/h/h935.md) ['aḥar](../../strongs/h/h310.md) the [melek](../../strongs/h/h4428.md)? even that which hath been [kᵊḇār](../../strongs/h/h3528.md) ['asah](../../strongs/h/h6213.md).

<a name="ecclesiastes_2_13"></a>Ecclesiastes 2:13

Then I [ra'ah](../../strongs/h/h7200.md) that [ḥāḵmâ](../../strongs/h/h2451.md) [yiṯrôn](../../strongs/h/h3504.md) [siḵlûṯ](../../strongs/h/h5531.md), as far as ['owr](../../strongs/h/h216.md) [yiṯrôn](../../strongs/h/h3504.md) [choshek](../../strongs/h/h2822.md).

<a name="ecclesiastes_2_14"></a>Ecclesiastes 2:14

The [ḥāḵām](../../strongs/h/h2450.md) ['ayin](../../strongs/h/h5869.md) are in his [ro'sh](../../strongs/h/h7218.md); but the [kᵊsîl](../../strongs/h/h3684.md) [halak](../../strongs/h/h1980.md) in [choshek](../../strongs/h/h2822.md): and I myself [yada'](../../strongs/h/h3045.md) also that one [miqrê](../../strongs/h/h4745.md) [qārâ](../../strongs/h/h7136.md) to them all.

<a name="ecclesiastes_2_15"></a>Ecclesiastes 2:15

Then ['āmar](../../strongs/h/h559.md) I in my [leb](../../strongs/h/h3820.md), As it [miqrê](../../strongs/h/h4745.md) to the [kᵊsîl](../../strongs/h/h3684.md), so it [qārâ](../../strongs/h/h7136.md) even to me; and why was I then [yôṯēr](../../strongs/h/h3148.md) [ḥāḵam](../../strongs/h/h2449.md)? Then I [dabar](../../strongs/h/h1696.md) in my [leb](../../strongs/h/h3820.md), that this also is [heḇel](../../strongs/h/h1892.md).

<a name="ecclesiastes_2_16"></a>Ecclesiastes 2:16

For there is no [zikārôn](../../strongs/h/h2146.md) of the [ḥāḵām](../../strongs/h/h2450.md) more than of the [kᵊsîl](../../strongs/h/h3684.md) ['owlam](../../strongs/h/h5769.md); seeing that which [kᵊḇār](../../strongs/h/h3528.md) is in the [yowm](../../strongs/h/h3117.md) to [bow'](../../strongs/h/h935.md) shall all be [shakach](../../strongs/h/h7911.md). And how [muwth](../../strongs/h/h4191.md) the [ḥāḵām](../../strongs/h/h2450.md) man? as the [kᵊsîl](../../strongs/h/h3684.md).

<a name="ecclesiastes_2_17"></a>Ecclesiastes 2:17

Therefore I [sane'](../../strongs/h/h8130.md) [chay](../../strongs/h/h2416.md); because the [ma'aseh](../../strongs/h/h4639.md) that is ['asah](../../strongs/h/h6213.md) under the [šemeš](../../strongs/h/h8121.md) is [ra'](../../strongs/h/h7451.md) unto me: for all is [heḇel](../../strongs/h/h1892.md) and [rᵊʿûṯ](../../strongs/h/h7469.md) of [ruwach](../../strongs/h/h7307.md).

<a name="ecclesiastes_2_18"></a>Ecclesiastes 2:18

Yea, I [sane'](../../strongs/h/h8130.md) all my ['amal](../../strongs/h/h5999.md) which I had [ʿāmēl](../../strongs/h/h6001.md) under the [šemeš](../../strongs/h/h8121.md): because I should [yānaḥ](../../strongs/h/h3240.md) it unto the ['āḏām](../../strongs/h/h120.md) that shall be ['aḥar](../../strongs/h/h310.md) me.

<a name="ecclesiastes_2_19"></a>Ecclesiastes 2:19

And who [yada'](../../strongs/h/h3045.md) whether he shall be a [ḥāḵām](../../strongs/h/h2450.md) man or a [sāḵāl](../../strongs/h/h5530.md)? yet shall he have [šālaṭ](../../strongs/h/h7980.md) over all my ['amal](../../strongs/h/h5999.md) wherein I have [ʿāmal](../../strongs/h/h5998.md), and wherein I have shewed myself [ḥāḵam](../../strongs/h/h2449.md) under the [šemeš](../../strongs/h/h8121.md). This is also [heḇel](../../strongs/h/h1892.md).

<a name="ecclesiastes_2_20"></a>Ecclesiastes 2:20

Therefore I went [cabab](../../strongs/h/h5437.md) to [yā'aš](../../strongs/h/h2976.md) my [leb](../../strongs/h/h3820.md) to [yā'aš](../../strongs/h/h2976.md) of all the ['amal](../../strongs/h/h5999.md) which I [ʿāmal](../../strongs/h/h5998.md) under the [šemeš](../../strongs/h/h8121.md).

<a name="ecclesiastes_2_21"></a>Ecclesiastes 2:21

For there is a ['āḏām](../../strongs/h/h120.md) whose ['amal](../../strongs/h/h5999.md) is in [ḥāḵmâ](../../strongs/h/h2451.md), and in [da'ath](../../strongs/h/h1847.md), and in [kišrôn](../../strongs/h/h3788.md); yet to a ['āḏām](../../strongs/h/h120.md) that hath not [ʿāmal](../../strongs/h/h5998.md) therein shall he [nathan](../../strongs/h/h5414.md) it for his [cheleq](../../strongs/h/h2506.md). This also is [heḇel](../../strongs/h/h1892.md) and a [rab](../../strongs/h/h7227.md) [ra'](../../strongs/h/h7451.md).

<a name="ecclesiastes_2_22"></a>Ecclesiastes 2:22

For what hath ['āḏām](../../strongs/h/h120.md) of all his ['amal](../../strongs/h/h5999.md), and of the [raʿyôn](../../strongs/h/h7475.md) of his [leb](../../strongs/h/h3820.md), wherein he hath [ʿāmēl](../../strongs/h/h6001.md) under the [šemeš](../../strongs/h/h8121.md)?

<a name="ecclesiastes_2_23"></a>Ecclesiastes 2:23

For all his [yowm](../../strongs/h/h3117.md) are [maḵ'ōḇ](../../strongs/h/h4341.md), and his [ʿinyān](../../strongs/h/h6045.md) [ka'ac](../../strongs/h/h3708.md); yea, his [leb](../../strongs/h/h3820.md) taketh not [shakab](../../strongs/h/h7901.md) in the [layil](../../strongs/h/h3915.md). This is also [heḇel](../../strongs/h/h1892.md).

<a name="ecclesiastes_2_24"></a>Ecclesiastes 2:24

There is nothing [towb](../../strongs/h/h2896.md) for a ['āḏām](../../strongs/h/h120.md), than that he should ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and that he should make his [nephesh](../../strongs/h/h5315.md) [ra'ah](../../strongs/h/h7200.md) [towb](../../strongs/h/h2896.md) in his ['amal](../../strongs/h/h5999.md). [zô](../../strongs/h/h2090.md) also I [ra'ah](../../strongs/h/h7200.md), that it was from the [yad](../../strongs/h/h3027.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="ecclesiastes_2_25"></a>Ecclesiastes 2:25

For who can ['akal](../../strongs/h/h398.md), or who else can [ḥûš](../../strongs/h/h2363.md) hereunto, [ḥûṣ](../../strongs/h/h2351.md) than I?

<a name="ecclesiastes_2_26"></a>Ecclesiastes 2:26

For [nathan](../../strongs/h/h5414.md) to a ['āḏām](../../strongs/h/h120.md) that is [towb](../../strongs/h/h2896.md) in his [paniym](../../strongs/h/h6440.md) [ḥāḵmâ](../../strongs/h/h2451.md), and [da'ath](../../strongs/h/h1847.md), and [simchah](../../strongs/h/h8057.md): but to the [chata'](../../strongs/h/h2398.md) he [nathan](../../strongs/h/h5414.md) [ʿinyān](../../strongs/h/h6045.md), to ['āsap̄](../../strongs/h/h622.md) and to [kānas](../../strongs/h/h3664.md), that he may [nathan](../../strongs/h/h5414.md) to him that is [towb](../../strongs/h/h2896.md) [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md). This also is [heḇel](../../strongs/h/h1892.md) and [rᵊʿûṯ](../../strongs/h/h7469.md) of [ruwach](../../strongs/h/h7307.md).

---

[Transliteral Bible](../bible.md)

[Ecclesiastes](ecclesiastes.md)

[Ecclesiastes 1](ecclesiastes_1.md) - [Ecclesiastes 3](ecclesiastes_3.md)