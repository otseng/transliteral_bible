# [Ecclesiastes 9](https://www.blueletterbible.org/kjv/ecclesiastes/9)

<a name="ecclesiastes_9_1"></a>Ecclesiastes 9:1

For all this I [nathan](../../strongs/h/h5414.md) in my [leb](../../strongs/h/h3820.md) even to [bûr](../../strongs/h/h952.md) all this, that the [tsaddiyq](../../strongs/h/h6662.md), and the [ḥāḵām](../../strongs/h/h2450.md), and their [ʿăḇāḏ](../../strongs/h/h5652.md), are in the [yad](../../strongs/h/h3027.md) of ['Elohiym](../../strongs/h/h430.md): no ['āḏām](../../strongs/h/h120.md) [yada'](../../strongs/h/h3045.md) either ['ahăḇâ](../../strongs/h/h160.md) or [śin'â](../../strongs/h/h8135.md) by all that is [paniym](../../strongs/h/h6440.md) them.

<a name="ecclesiastes_9_2"></a>Ecclesiastes 9:2

one [miqrê](../../strongs/h/h4745.md) to the [tsaddiyq](../../strongs/h/h6662.md), and to the [rasha'](../../strongs/h/h7563.md); to the [towb](../../strongs/h/h2896.md) and to the [tahowr](../../strongs/h/h2889.md), and to the [tame'](../../strongs/h/h2931.md); to him that [zabach](../../strongs/h/h2076.md), and to him that [zabach](../../strongs/h/h2076.md) not: as is the [towb](../../strongs/h/h2896.md), so is the [chata'](../../strongs/h/h2398.md); and he that [shaba'](../../strongs/h/h7650.md), as he that [yārē'](../../strongs/h/h3373.md) an [šᵊḇûʿâ](../../strongs/h/h7621.md).

<a name="ecclesiastes_9_3"></a>Ecclesiastes 9:3

This is an [ra'](../../strongs/h/h7451.md) among all things that are ['asah](../../strongs/h/h6213.md) under the [šemeš](../../strongs/h/h8121.md), that there is one [miqrê](../../strongs/h/h4745.md) unto all: yea, also the [leb](../../strongs/h/h3820.md) of the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md) is [mālā'](../../strongs/h/h4390.md) of [ra'](../../strongs/h/h7451.md), and [hôlēlâ](../../strongs/h/h1947.md) is in their [lebab](../../strongs/h/h3824.md) while they [chay](../../strongs/h/h2416.md), and ['aḥar](../../strongs/h/h310.md) that they go to the [muwth](../../strongs/h/h4191.md).

<a name="ecclesiastes_9_4"></a>Ecclesiastes 9:4

For to him that is [ḥāḇar](../../strongs/h/h2266.md) [bāḥar](../../strongs/h/h977.md) to all the [chay](../../strongs/h/h2416.md) there is [biṭṭāḥôn](../../strongs/h/h986.md): for a [chay](../../strongs/h/h2416.md) [keleḇ](../../strongs/h/h3611.md) is [towb](../../strongs/h/h2896.md) than a [muwth](../../strongs/h/h4191.md) ['ariy](../../strongs/h/h738.md).

<a name="ecclesiastes_9_5"></a>Ecclesiastes 9:5

For the [chay](../../strongs/h/h2416.md) [yada'](../../strongs/h/h3045.md) that they shall [muwth](../../strongs/h/h4191.md): but the [muwth](../../strongs/h/h4191.md) [yada'](../../strongs/h/h3045.md) not any [mᵊ'ûmâ](../../strongs/h/h3972.md), neither have they any more a [śāḵār](../../strongs/h/h7939.md); for the [zeker](../../strongs/h/h2143.md) of them is [shakach](../../strongs/h/h7911.md).

<a name="ecclesiastes_9_6"></a>Ecclesiastes 9:6

Also their ['ahăḇâ](../../strongs/h/h160.md), and their [śin'â](../../strongs/h/h8135.md), and their [qin'â](../../strongs/h/h7068.md), is [kᵊḇār](../../strongs/h/h3528.md) ['abad](../../strongs/h/h6.md); neither have they any more a [cheleq](../../strongs/h/h2506.md) ['owlam](../../strongs/h/h5769.md) in any thing that is ['asah](../../strongs/h/h6213.md) under the [šemeš](../../strongs/h/h8121.md).

<a name="ecclesiastes_9_7"></a>Ecclesiastes 9:7

[yālaḵ](../../strongs/h/h3212.md), ['akal](../../strongs/h/h398.md) thy [lechem](../../strongs/h/h3899.md) with [simchah](../../strongs/h/h8057.md), and [šāṯâ](../../strongs/h/h8354.md) thy [yayin](../../strongs/h/h3196.md) with a [towb](../../strongs/h/h2896.md) [leb](../../strongs/h/h3820.md); for ['Elohiym](../../strongs/h/h430.md) [kᵊḇār](../../strongs/h/h3528.md) [ratsah](../../strongs/h/h7521.md) thy [ma'aseh](../../strongs/h/h4639.md).

<a name="ecclesiastes_9_8"></a>Ecclesiastes 9:8

Let thy [beḡeḏ](../../strongs/h/h899.md) be [ʿēṯ](../../strongs/h/h6256.md) [lāḇān](../../strongs/h/h3836.md); and let thy [ro'sh](../../strongs/h/h7218.md) [ḥāsēr](../../strongs/h/h2637.md) no [šemen](../../strongs/h/h8081.md).

<a name="ecclesiastes_9_9"></a>Ecclesiastes 9:9

[chay](../../strongs/h/h2416.md) [ra'ah](../../strongs/h/h7200.md) with the ['ishshah](../../strongs/h/h802.md) whom thou ['ahab](../../strongs/h/h157.md) all the [yowm](../../strongs/h/h3117.md) of the [chay](../../strongs/h/h2416.md) of thy [heḇel](../../strongs/h/h1892.md), which he hath [nathan](../../strongs/h/h5414.md) thee under the [šemeš](../../strongs/h/h8121.md), all the [yowm](../../strongs/h/h3117.md) of thy [heḇel](../../strongs/h/h1892.md): for that is thy [cheleq](../../strongs/h/h2506.md) in this [chay](../../strongs/h/h2416.md), and in thy ['amal](../../strongs/h/h5999.md) which thou [ʿāmēl](../../strongs/h/h6001.md) under the [šemeš](../../strongs/h/h8121.md).

<a name="ecclesiastes_9_10"></a>Ecclesiastes 9:10

Whatsoever thy [yad](../../strongs/h/h3027.md) [māṣā'](../../strongs/h/h4672.md) to ['asah](../../strongs/h/h6213.md), ['asah](../../strongs/h/h6213.md) it with thy [koach](../../strongs/h/h3581.md); for there is no [ma'aseh](../../strongs/h/h4639.md), nor [ḥešbôn](../../strongs/h/h2808.md), nor [da'ath](../../strongs/h/h1847.md), nor [ḥāḵmâ](../../strongs/h/h2451.md), in the [shĕ'owl](../../strongs/h/h7585.md), whither thou [halak](../../strongs/h/h1980.md).

<a name="ecclesiastes_9_11"></a>Ecclesiastes 9:11

I [shuwb](../../strongs/h/h7725.md), and [ra'ah](../../strongs/h/h7200.md) under the [šemeš](../../strongs/h/h8121.md), that the [mērôṣ](../../strongs/h/h4793.md) is not to the [qal](../../strongs/h/h7031.md), nor the [milḥāmâ](../../strongs/h/h4421.md) to the [gibôr](../../strongs/h/h1368.md), neither yet [lechem](../../strongs/h/h3899.md) to the [ḥāḵām](../../strongs/h/h2450.md), nor yet [ʿōšer](../../strongs/h/h6239.md) to men of [bîn](../../strongs/h/h995.md), nor yet [ḥēn](../../strongs/h/h2580.md) to men of [yada'](../../strongs/h/h3045.md); but [ʿēṯ](../../strongs/h/h6256.md) and [peḡaʿ](../../strongs/h/h6294.md) [qārâ](../../strongs/h/h7136.md) to them all.

<a name="ecclesiastes_9_12"></a>Ecclesiastes 9:12

For ['āḏām](../../strongs/h/h120.md) also [yada'](../../strongs/h/h3045.md) not his [ʿēṯ](../../strongs/h/h6256.md): as the [dag](../../strongs/h/h1709.md) that are ['āḥaz](../../strongs/h/h270.md) in an [ra'](../../strongs/h/h7451.md) [māṣôḏ](../../strongs/h/h4685.md), and as the [tsippowr](../../strongs/h/h6833.md) that are ['āḥaz](../../strongs/h/h270.md) in the [paḥ](../../strongs/h/h6341.md); so are the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md) [yāqōš](../../strongs/h/h3369.md) in an [ra'](../../strongs/h/h7451.md) [ʿēṯ](../../strongs/h/h6256.md), when it [naphal](../../strongs/h/h5307.md) [piṯ'ōm](../../strongs/h/h6597.md) upon them.

<a name="ecclesiastes_9_13"></a>Ecclesiastes 9:13

[zô](../../strongs/h/h2090.md) [ḥāḵmâ](../../strongs/h/h2451.md) have I [ra'ah](../../strongs/h/h7200.md) also under the [šemeš](../../strongs/h/h8121.md), and it [gadowl](../../strongs/h/h1419.md) unto me:

<a name="ecclesiastes_9_14"></a>Ecclesiastes 9:14

There was a [qāṭān](../../strongs/h/h6996.md) [ʿîr](../../strongs/h/h5892.md), and [mᵊʿaṭ](../../strongs/h/h4592.md) ['enowsh](../../strongs/h/h582.md) within it; and there [bow'](../../strongs/h/h935.md) a [gadowl](../../strongs/h/h1419.md) [melek](../../strongs/h/h4428.md) against it, and [cabab](../../strongs/h/h5437.md) it, and [bānâ](../../strongs/h/h1129.md) [gadowl](../../strongs/h/h1419.md) [māṣôḏ](../../strongs/h/h4685.md) against it:

<a name="ecclesiastes_9_15"></a>Ecclesiastes 9:15

Now there was [māṣā'](../../strongs/h/h4672.md) in it a [miskēn](../../strongs/h/h4542.md) [ḥāḵām](../../strongs/h/h2450.md) ['iysh](../../strongs/h/h376.md), and he by his [ḥāḵmâ](../../strongs/h/h2451.md) [mālaṭ](../../strongs/h/h4422.md) the [ʿîr](../../strongs/h/h5892.md); yet no ['āḏām](../../strongs/h/h120.md) [zakar](../../strongs/h/h2142.md) that same [miskēn](../../strongs/h/h4542.md) ['iysh](../../strongs/h/h376.md).

<a name="ecclesiastes_9_16"></a>Ecclesiastes 9:16

Then ['āmar](../../strongs/h/h559.md) I, [ḥāḵmâ](../../strongs/h/h2451.md) is [towb](../../strongs/h/h2896.md) than [gᵊḇûrâ](../../strongs/h/h1369.md): nevertheless the [miskēn](../../strongs/h/h4542.md) [ḥāḵmâ](../../strongs/h/h2451.md) is [bazah](../../strongs/h/h959.md), and his [dabar](../../strongs/h/h1697.md) are not [shama'](../../strongs/h/h8085.md).

<a name="ecclesiastes_9_17"></a>Ecclesiastes 9:17

The [dabar](../../strongs/h/h1697.md) of [ḥāḵām](../../strongs/h/h2450.md) are [shama'](../../strongs/h/h8085.md) in [naḥaṯ](../../strongs/h/h5183.md) more than the [zaʿaq](../../strongs/h/h2201.md) of him that [mashal](../../strongs/h/h4910.md) among [kᵊsîl](../../strongs/h/h3684.md).

<a name="ecclesiastes_9_18"></a>Ecclesiastes 9:18

[ḥāḵmâ](../../strongs/h/h2451.md) is [towb](../../strongs/h/h2896.md) than [kĕliy](../../strongs/h/h3627.md) of [qᵊrāḇ](../../strongs/h/h7128.md): but one [chata'](../../strongs/h/h2398.md) ['abad](../../strongs/h/h6.md) [rabah](../../strongs/h/h7235.md) [towb](../../strongs/h/h2896.md).

---

[Transliteral Bible](../bible.md)

[Ecclesiastes](ecclesiastes.md)

[Ecclesiastes 8](ecclesiastes_8.md) - [Ecclesiastes 10](ecclesiastes_10.md)