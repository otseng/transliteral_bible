# [Ecclesiastes 7](https://www.blueletterbible.org/kjv/ecclesiastes/7)

<a name="ecclesiastes_7_1"></a>Ecclesiastes 7:1

A good [shem](../../strongs/h/h8034.md) is [towb](../../strongs/h/h2896.md) than [towb](../../strongs/h/h2896.md) [šemen](../../strongs/h/h8081.md); and the [yowm](../../strongs/h/h3117.md) of [maveth](../../strongs/h/h4194.md) than the [yowm](../../strongs/h/h3117.md) of one's [yalad](../../strongs/h/h3205.md).

<a name="ecclesiastes_7_2"></a>Ecclesiastes 7:2

It is [towb](../../strongs/h/h2896.md) to [yālaḵ](../../strongs/h/h3212.md) to the [bayith](../../strongs/h/h1004.md) of ['ēḇel](../../strongs/h/h60.md), than to [yālaḵ](../../strongs/h/h3212.md) to the [bayith](../../strongs/h/h1004.md) of [mištê](../../strongs/h/h4960.md): for that is the [sôp̄](../../strongs/h/h5490.md) of all ['āḏām](../../strongs/h/h120.md); and the [chay](../../strongs/h/h2416.md) will [nathan](../../strongs/h/h5414.md) it to his [leb](../../strongs/h/h3820.md).

<a name="ecclesiastes_7_3"></a>Ecclesiastes 7:3

[ka'ac](../../strongs/h/h3708.md) is [towb](../../strongs/h/h2896.md) than [śᵊḥôq](../../strongs/h/h7814.md): for by the [rōaʿ](../../strongs/h/h7455.md) of the [paniym](../../strongs/h/h6440.md) the [leb](../../strongs/h/h3820.md) is made [yatab](../../strongs/h/h3190.md).

<a name="ecclesiastes_7_4"></a>Ecclesiastes 7:4

The [leb](../../strongs/h/h3820.md) of the [ḥāḵām](../../strongs/h/h2450.md) is in the [bayith](../../strongs/h/h1004.md) of ['ēḇel](../../strongs/h/h60.md); but the [leb](../../strongs/h/h3820.md) of [kᵊsîl](../../strongs/h/h3684.md) is in the [bayith](../../strongs/h/h1004.md) of [simchah](../../strongs/h/h8057.md).

<a name="ecclesiastes_7_5"></a>Ecclesiastes 7:5

It is [towb](../../strongs/h/h2896.md) to [shama'](../../strongs/h/h8085.md) the [ge'arah](../../strongs/h/h1606.md) of the [ḥāḵām](../../strongs/h/h2450.md), than for an ['iysh](../../strongs/h/h376.md) to [shama'](../../strongs/h/h8085.md) the [šîr](../../strongs/h/h7892.md) of [kᵊsîl](../../strongs/h/h3684.md).

<a name="ecclesiastes_7_6"></a>Ecclesiastes 7:6

For as the [qowl](../../strongs/h/h6963.md) of [sîr](../../strongs/h/h5518.md) under a [sîr](../../strongs/h/h5518.md), so is the [śᵊḥôq](../../strongs/h/h7814.md) of the [kᵊsîl](../../strongs/h/h3684.md): this also is [heḇel](../../strongs/h/h1892.md).

<a name="ecclesiastes_7_7"></a>Ecclesiastes 7:7

Surely [ʿōšeq](../../strongs/h/h6233.md) maketh a [ḥāḵām](../../strongs/h/h2450.md) [halal](../../strongs/h/h1984.md); and a [matānâ](../../strongs/h/h4979.md) ['abad](../../strongs/h/h6.md) the [leb](../../strongs/h/h3820.md).

<a name="ecclesiastes_7_8"></a>Ecclesiastes 7:8

[towb](../../strongs/h/h2896.md) is the ['aḥărîṯ](../../strongs/h/h319.md) of a [dabar](../../strongs/h/h1697.md) than the [re'shiyth](../../strongs/h/h7225.md) thereof: and the ['ārēḵ](../../strongs/h/h750.md) in [ruwach](../../strongs/h/h7307.md) is [towb](../../strongs/h/h2896.md) than the [gāḇâ](../../strongs/h/h1362.md) in [ruwach](../../strongs/h/h7307.md).

<a name="ecclesiastes_7_9"></a>Ecclesiastes 7:9

Be not [bahal](../../strongs/h/h926.md) in thy [ruwach](../../strongs/h/h7307.md) to be [kāʿas](../../strongs/h/h3707.md): for [ka'ac](../../strongs/h/h3708.md) [nuwach](../../strongs/h/h5117.md) in the [ḥêq](../../strongs/h/h2436.md) of [kᵊsîl](../../strongs/h/h3684.md).

<a name="ecclesiastes_7_10"></a>Ecclesiastes 7:10

['āmar](../../strongs/h/h559.md) not thou, What is the cause that the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) were [towb](../../strongs/h/h2896.md) than these? for thou dost not [sha'al](../../strongs/h/h7592.md) [ḥāḵmâ](../../strongs/h/h2451.md) concerning this.

<a name="ecclesiastes_7_11"></a>Ecclesiastes 7:11

[ḥāḵmâ](../../strongs/h/h2451.md) is [towb](../../strongs/h/h2896.md) with an [nachalah](../../strongs/h/h5159.md): and by it there is [yôṯēr](../../strongs/h/h3148.md) to them that [ra'ah](../../strongs/h/h7200.md) the [šemeš](../../strongs/h/h8121.md).

<a name="ecclesiastes_7_12"></a>Ecclesiastes 7:12

For [ḥāḵmâ](../../strongs/h/h2451.md) is a [ṣēl](../../strongs/h/h6738.md), and [keceph](../../strongs/h/h3701.md) is a [ṣēl](../../strongs/h/h6738.md): but the [yiṯrôn](../../strongs/h/h3504.md) of [da'ath](../../strongs/h/h1847.md) is, that [ḥāḵmâ](../../strongs/h/h2451.md) giveth [ḥāyâ](../../strongs/h/h2421.md) to them that [baʿal](../../strongs/h/h1167.md) it.

<a name="ecclesiastes_7_13"></a>Ecclesiastes 7:13

[ra'ah](../../strongs/h/h7200.md) the [ma'aseh](../../strongs/h/h4639.md) of ['Elohiym](../../strongs/h/h430.md): for who [yakol](../../strongs/h/h3201.md) make that [tāqan](../../strongs/h/h8626.md), which he hath made [ʿāvaṯ](../../strongs/h/h5791.md)?

<a name="ecclesiastes_7_14"></a>Ecclesiastes 7:14

In the [yowm](../../strongs/h/h3117.md) of [towb](../../strongs/h/h2896.md) be [towb](../../strongs/h/h2896.md), but in the [yowm](../../strongs/h/h3117.md) of [ra'](../../strongs/h/h7451.md) [ra'ah](../../strongs/h/h7200.md): ['Elohiym](../../strongs/h/h430.md) also hath ['asah](../../strongs/h/h6213.md) the one over [ʿummâ](../../strongs/h/h5980.md) the other, to the [diḇrâ](../../strongs/h/h1700.md) that ['āḏām](../../strongs/h/h120.md) should [māṣā'](../../strongs/h/h4672.md) [mᵊ'ûmâ](../../strongs/h/h3972.md) ['aḥar](../../strongs/h/h310.md) him.

<a name="ecclesiastes_7_15"></a>Ecclesiastes 7:15

All things have I [ra'ah](../../strongs/h/h7200.md) in the [yowm](../../strongs/h/h3117.md) of my [heḇel](../../strongs/h/h1892.md): there is a [tsaddiyq](../../strongs/h/h6662.md) man that ['abad](../../strongs/h/h6.md) in his [tsedeq](../../strongs/h/h6664.md), and there is a [rasha'](../../strongs/h/h7563.md) man that ['arak](../../strongs/h/h748.md) his life in his [ra'](../../strongs/h/h7451.md).

<a name="ecclesiastes_7_16"></a>Ecclesiastes 7:16

Be not [tsaddiyq](../../strongs/h/h6662.md) over [rabah](../../strongs/h/h7235.md); neither make thyself [yôṯēr](../../strongs/h/h3148.md) [ḥāḵam](../../strongs/h/h2449.md): why shouldest thou [šāmēm](../../strongs/h/h8074.md) thyself?

<a name="ecclesiastes_7_17"></a>Ecclesiastes 7:17

Be not over [rabah](../../strongs/h/h7235.md) [rāšaʿ](../../strongs/h/h7561.md), neither be thou [sāḵāl](../../strongs/h/h5530.md): why shouldest thou [muwth](../../strongs/h/h4191.md) before thy [ʿēṯ](../../strongs/h/h6256.md)?

<a name="ecclesiastes_7_18"></a>Ecclesiastes 7:18

It is [towb](../../strongs/h/h2896.md) that thou shouldest take ['āḥaz](../../strongs/h/h270.md) of this; yea, also from this [yānaḥ](../../strongs/h/h3240.md) not thine [yad](../../strongs/h/h3027.md): for he that [yārē'](../../strongs/h/h3373.md) ['Elohiym](../../strongs/h/h430.md) shall [yāṣā'](../../strongs/h/h3318.md) of them all.

<a name="ecclesiastes_7_19"></a>Ecclesiastes 7:19

[ḥāḵmâ](../../strongs/h/h2451.md) ['azaz](../../strongs/h/h5810.md) the [ḥāḵām](../../strongs/h/h2450.md) more than ten [šallîṭ](../../strongs/h/h7989.md) men which are in the [ʿîr](../../strongs/h/h5892.md).

<a name="ecclesiastes_7_20"></a>Ecclesiastes 7:20

For there is not a [tsaddiyq](../../strongs/h/h6662.md) ['āḏām](../../strongs/h/h120.md) upon ['erets](../../strongs/h/h776.md), that ['asah](../../strongs/h/h6213.md) [towb](../../strongs/h/h2896.md), and [chata'](../../strongs/h/h2398.md) not.

<a name="ecclesiastes_7_21"></a>Ecclesiastes 7:21

Also [nathan](../../strongs/h/h5414.md) no [leb](../../strongs/h/h3820.md) unto all [dabar](../../strongs/h/h1697.md) that are [dabar](../../strongs/h/h1696.md); lest thou [shama'](../../strongs/h/h8085.md) thy ['ebed](../../strongs/h/h5650.md) [qālal](../../strongs/h/h7043.md) thee:

<a name="ecclesiastes_7_22"></a>Ecclesiastes 7:22

For oftentimes [rab](../../strongs/h/h7227.md) also thine own [leb](../../strongs/h/h3820.md) [yada'](../../strongs/h/h3045.md) that thou thyself likewise hast [qālal](../../strongs/h/h7043.md) ['aḥēr](../../strongs/h/h312.md).

<a name="ecclesiastes_7_23"></a>Ecclesiastes 7:23

All [zô](../../strongs/h/h2090.md) have I [nāsâ](../../strongs/h/h5254.md) by [ḥāḵmâ](../../strongs/h/h2451.md): I ['āmar](../../strongs/h/h559.md), I will be [ḥāḵam](../../strongs/h/h2449.md); but it was [rachowq](../../strongs/h/h7350.md) from me.

<a name="ecclesiastes_7_24"></a>Ecclesiastes 7:24

That which is [rachowq](../../strongs/h/h7350.md), and [ʿāmōq](../../strongs/h/h6013.md), who can [māṣā'](../../strongs/h/h4672.md) it?

<a name="ecclesiastes_7_25"></a>Ecclesiastes 7:25

I [cabab](../../strongs/h/h5437.md) mine [leb](../../strongs/h/h3820.md) to [yada'](../../strongs/h/h3045.md), and to [tûr](../../strongs/h/h8446.md), and to [bāqaš](../../strongs/h/h1245.md) [ḥāḵmâ](../../strongs/h/h2451.md), and the [ḥešbôn](../../strongs/h/h2808.md) of things, and to [yada'](../../strongs/h/h3045.md) the [resha'](../../strongs/h/h7562.md) of [kesel](../../strongs/h/h3689.md), even of [siḵlûṯ](../../strongs/h/h5531.md) and [hôlēlâ](../../strongs/h/h1947.md):

<a name="ecclesiastes_7_26"></a>Ecclesiastes 7:26

And I [māṣā'](../../strongs/h/h4672.md) more [mar](../../strongs/h/h4751.md) than [maveth](../../strongs/h/h4194.md) the ['ishshah](../../strongs/h/h802.md), whose [leb](../../strongs/h/h3820.md) is [māṣôḏ](../../strongs/h/h4685.md) and [ḥērem](../../strongs/h/h2764.md), and her [yad](../../strongs/h/h3027.md) as ['ēsûr](../../strongs/h/h612.md): whoso [towb](../../strongs/h/h2896.md) [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md) shall [mālaṭ](../../strongs/h/h4422.md) from her; but the [chata'](../../strongs/h/h2398.md) shall be [lāḵaḏ](../../strongs/h/h3920.md) by her.

<a name="ecclesiastes_7_27"></a>Ecclesiastes 7:27

[ra'ah](../../strongs/h/h7200.md), this have I [māṣā'](../../strongs/h/h4672.md), ['āmar](../../strongs/h/h559.md) the [qōheleṯ](../../strongs/h/h6953.md), ['echad](../../strongs/h/h259.md) by ['echad](../../strongs/h/h259.md), to [māṣā'](../../strongs/h/h4672.md) the [ḥešbôn](../../strongs/h/h2808.md):

<a name="ecclesiastes_7_28"></a>Ecclesiastes 7:28

Which yet my [nephesh](../../strongs/h/h5315.md) [bāqaš](../../strongs/h/h1245.md), but I [māṣā'](../../strongs/h/h4672.md) not: one ['āḏām](../../strongs/h/h120.md) among a thousand have I [māṣā'](../../strongs/h/h4672.md); but a ['ishshah](../../strongs/h/h802.md) among all those have I not [māṣā'](../../strongs/h/h4672.md).

<a name="ecclesiastes_7_29"></a>Ecclesiastes 7:29

[ra'ah](../../strongs/h/h7200.md), this [baḏ](../../strongs/h/h905.md) have I [māṣā'](../../strongs/h/h4672.md), that ['Elohiym](../../strongs/h/h430.md) hath ['asah](../../strongs/h/h6213.md) ['āḏām](../../strongs/h/h120.md) [yashar](../../strongs/h/h3477.md); but they have sought [bāqaš](../../strongs/h/h1245.md) [rab](../../strongs/h/h7227.md) [ḥiššāḇôn](../../strongs/h/h2810.md).

---

[Transliteral Bible](../bible.md)

[Ecclesiastes](ecclesiastes.md)

[Ecclesiastes 6](ecclesiastes_6.md) - [Ecclesiastes 8](ecclesiastes_8.md)