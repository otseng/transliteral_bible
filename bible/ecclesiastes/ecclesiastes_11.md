# [Ecclesiastes 11](https://www.blueletterbible.org/kjv/ecclesiastes/11)

<a name="ecclesiastes_11_1"></a>Ecclesiastes 11:1

[shalach](../../strongs/h/h7971.md) thy [lechem](../../strongs/h/h3899.md) [paniym](../../strongs/h/h6440.md) the [mayim](../../strongs/h/h4325.md): for thou shalt [māṣā'](../../strongs/h/h4672.md) it after [rōḇ](../../strongs/h/h7230.md) [yowm](../../strongs/h/h3117.md).

<a name="ecclesiastes_11_2"></a>Ecclesiastes 11:2

[nathan](../../strongs/h/h5414.md) a [cheleq](../../strongs/h/h2506.md) to seven, and also to eight; for thou [yada'](../../strongs/h/h3045.md) not what [ra'](../../strongs/h/h7451.md) shall be upon the ['erets](../../strongs/h/h776.md).

<a name="ecclesiastes_11_3"></a>Ecclesiastes 11:3

If the ['ab](../../strongs/h/h5645.md) be [mālā'](../../strongs/h/h4390.md) of [gešem](../../strongs/h/h1653.md), they [rîq](../../strongs/h/h7324.md) themselves upon the ['erets](../../strongs/h/h776.md): and if the ['ets](../../strongs/h/h6086.md) [naphal](../../strongs/h/h5307.md) toward the [dārôm](../../strongs/h/h1864.md), or toward the [ṣāp̄ôn](../../strongs/h/h6828.md), in the [maqowm](../../strongs/h/h4725.md) where the ['ets](../../strongs/h/h6086.md) [naphal](../../strongs/h/h5307.md), there it shall be.

<a name="ecclesiastes_11_4"></a>Ecclesiastes 11:4

He that [shamar](../../strongs/h/h8104.md) the [ruwach](../../strongs/h/h7307.md) shall not [zāraʿ](../../strongs/h/h2232.md); and he that [ra'ah](../../strongs/h/h7200.md) the ['ab](../../strongs/h/h5645.md) shall not [qāṣar](../../strongs/h/h7114.md).

<a name="ecclesiastes_11_5"></a>Ecclesiastes 11:5

As thou [yada'](../../strongs/h/h3045.md) not what is the [derek](../../strongs/h/h1870.md) of the [ruwach](../../strongs/h/h7307.md), nor how the ['etsem](../../strongs/h/h6106.md) in the [beten](../../strongs/h/h990.md) of her that is with [mālē'](../../strongs/h/h4392.md): even so thou [yada'](../../strongs/h/h3045.md) not the [ma'aseh](../../strongs/h/h4639.md) of ['Elohiym](../../strongs/h/h430.md) who ['asah](../../strongs/h/h6213.md) all.

<a name="ecclesiastes_11_6"></a>Ecclesiastes 11:6

In the [boqer](../../strongs/h/h1242.md) [zāraʿ](../../strongs/h/h2232.md) thy [zera'](../../strongs/h/h2233.md), and in the ['ereb](../../strongs/h/h6153.md) [yānaḥ](../../strongs/h/h3240.md) not thine [yad](../../strongs/h/h3027.md): for thou [yada'](../../strongs/h/h3045.md) not whether shall [kāšēr](../../strongs/h/h3787.md), either this or that, or whether they both shall be alike [towb](../../strongs/h/h2896.md).

<a name="ecclesiastes_11_7"></a>Ecclesiastes 11:7

Truly the ['owr](../../strongs/h/h216.md) is [māṯôq](../../strongs/h/h4966.md), and a [towb](../../strongs/h/h2896.md) thing it is for the ['ayin](../../strongs/h/h5869.md) to [ra'ah](../../strongs/h/h7200.md) the [šemeš](../../strongs/h/h8121.md):

<a name="ecclesiastes_11_8"></a>Ecclesiastes 11:8

But if a ['āḏām](../../strongs/h/h120.md) [ḥāyâ](../../strongs/h/h2421.md) [rabah](../../strongs/h/h7235.md) [šānâ](../../strongs/h/h8141.md), and [samach](../../strongs/h/h8055.md) in them all; yet let him [zakar](../../strongs/h/h2142.md) the [yowm](../../strongs/h/h3117.md) of [choshek](../../strongs/h/h2822.md); for they shall be [rabah](../../strongs/h/h7235.md). All that [bow'](../../strongs/h/h935.md) is [heḇel](../../strongs/h/h1892.md).

<a name="ecclesiastes_11_9"></a>Ecclesiastes 11:9

[samach](../../strongs/h/h8055.md), O [bāḥûr](../../strongs/h/h970.md), in thy [yalḏûṯ](../../strongs/h/h3208.md); and let thy [leb](../../strongs/h/h3820.md) [ṭôḇ](../../strongs/h/h2895.md) thee in the [yowm](../../strongs/h/h3117.md) of thy [bᵊḥurîm](../../strongs/h/h979.md), and [halak](../../strongs/h/h1980.md) in the [derek](../../strongs/h/h1870.md) of thine [leb](../../strongs/h/h3820.md), and in the [mar'ê](../../strongs/h/h4758.md) of thine ['ayin](../../strongs/h/h5869.md): but [yada'](../../strongs/h/h3045.md) thou, that for all these things ['Elohiym](../../strongs/h/h430.md) will [bow'](../../strongs/h/h935.md) thee into [mishpat](../../strongs/h/h4941.md).

<a name="ecclesiastes_11_10"></a>Ecclesiastes 11:10

Therefore [cuwr](../../strongs/h/h5493.md) [ka'ac](../../strongs/h/h3708.md) from thy [leb](../../strongs/h/h3820.md), and ['abar](../../strongs/h/h5674.md) [ra'](../../strongs/h/h7451.md) from thy [basar](../../strongs/h/h1320.md): for [yalḏûṯ](../../strongs/h/h3208.md) and [šaḥărûṯ](../../strongs/h/h7839.md) are [heḇel](../../strongs/h/h1892.md).

---

[Transliteral Bible](../bible.md)

[Ecclesiastes](ecclesiastes.md)

[Ecclesiastes 10](ecclesiastes_10.md) - [Ecclesiastes 12](ecclesiastes_12.md)