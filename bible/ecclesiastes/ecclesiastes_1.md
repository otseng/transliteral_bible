# [Ecclesiastes 1](https://www.blueletterbible.org/kjv/ecclesiastes/1)

<a name="ecclesiastes_1_1"></a>Ecclesiastes 1:1

The [dabar](../../strongs/h/h1697.md) of the [qōheleṯ](../../strongs/h/h6953.md), the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md), [melek](../../strongs/h/h4428.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="ecclesiastes_1_2"></a>Ecclesiastes 1:2

[heḇel](../../strongs/h/h1892.md) of [heḇel](../../strongs/h/h1892.md), ['āmar](../../strongs/h/h559.md) the [qōheleṯ](../../strongs/h/h6953.md), [heḇel](../../strongs/h/h1892.md) of [heḇel](../../strongs/h/h1892.md); all is [heḇel](../../strongs/h/h1892.md).

<a name="ecclesiastes_1_3"></a>Ecclesiastes 1:3

What [yiṯrôn](../../strongs/h/h3504.md) hath a ['āḏām](../../strongs/h/h120.md) of all his ['amal](../../strongs/h/h5999.md) which he [ʿāmal](../../strongs/h/h5998.md) under the [šemeš](../../strongs/h/h8121.md)?

<a name="ecclesiastes_1_4"></a>Ecclesiastes 1:4

One [dôr](../../strongs/h/h1755.md) [halak](../../strongs/h/h1980.md), and another [dôr](../../strongs/h/h1755.md) [bow'](../../strongs/h/h935.md) [bow'](../../strongs/h/h935.md): but the ['erets](../../strongs/h/h776.md) ['amad](../../strongs/h/h5975.md) ['owlam](../../strongs/h/h5769.md).

<a name="ecclesiastes_1_5"></a>Ecclesiastes 1:5

The [šemeš](../../strongs/h/h8121.md) also [zāraḥ](../../strongs/h/h2224.md), and the [šemeš](../../strongs/h/h8121.md) [bow'](../../strongs/h/h935.md), and [šā'ap̄](../../strongs/h/h7602.md) to his [maqowm](../../strongs/h/h4725.md) where he [zāraḥ](../../strongs/h/h2224.md).

<a name="ecclesiastes_1_6"></a>Ecclesiastes 1:6

The [ruwach](../../strongs/h/h7307.md) [halak](../../strongs/h/h1980.md) toward the [dārôm](../../strongs/h/h1864.md), and [cabab](../../strongs/h/h5437.md) unto the [ṣāp̄ôn](../../strongs/h/h6828.md); it whirleth about [halak](../../strongs/h/h1980.md), and the [ruwach](../../strongs/h/h7307.md) [shuwb](../../strongs/h/h7725.md) according to his [cabiyb](../../strongs/h/h5439.md).

<a name="ecclesiastes_1_7"></a>Ecclesiastes 1:7

All the [nachal](../../strongs/h/h5158.md) [halak](../../strongs/h/h1980.md) into the [yam](../../strongs/h/h3220.md); yet the [yam](../../strongs/h/h3220.md) is not [mālē'](../../strongs/h/h4392.md); unto the [maqowm](../../strongs/h/h4725.md) from whence the [nachal](../../strongs/h/h5158.md) [halak](../../strongs/h/h1980.md), thither they [shuwb](../../strongs/h/h7725.md) [yālaḵ](../../strongs/h/h3212.md).

<a name="ecclesiastes_1_8"></a>Ecclesiastes 1:8

All [dabar](../../strongs/h/h1697.md) are full of [yāḡēaʿ](../../strongs/h/h3023.md); ['iysh](../../strongs/h/h376.md) [yakol](../../strongs/h/h3201.md) [dabar](../../strongs/h/h1696.md) it: the ['ayin](../../strongs/h/h5869.md) is not [sāׂbaʿ](../../strongs/h/h7646.md) with [ra'ah](../../strongs/h/h7200.md), nor the ['ozen](../../strongs/h/h241.md) [mālā'](../../strongs/h/h4390.md) with [shama'](../../strongs/h/h8085.md).

<a name="ecclesiastes_1_9"></a>Ecclesiastes 1:9

The thing that hath been, it is that which shall be; and that which is ['asah](../../strongs/h/h6213.md) is that which shall be ['asah](../../strongs/h/h6213.md): and there is no [ḥāḏāš](../../strongs/h/h2319.md) thing under the [šemeš](../../strongs/h/h8121.md).

<a name="ecclesiastes_1_10"></a>Ecclesiastes 1:10

Is there any [dabar](../../strongs/h/h1697.md) whereof it may be ['āmar](../../strongs/h/h559.md), [ra'ah](../../strongs/h/h7200.md), this is [ḥāḏāš](../../strongs/h/h2319.md)? it hath been [kᵊḇār](../../strongs/h/h3528.md) of ['owlam](../../strongs/h/h5769.md), which was [paniym](../../strongs/h/h6440.md) us.

<a name="ecclesiastes_1_11"></a>Ecclesiastes 1:11

There is no [zikārôn](../../strongs/h/h2146.md) of [ri'šôn](../../strongs/h/h7223.md) things; neither shall there be any [zikārôn](../../strongs/h/h2146.md) of things that are to ['aḥărôn](../../strongs/h/h314.md) with those that shall come ['aḥărôn](../../strongs/h/h314.md).

<a name="ecclesiastes_1_12"></a>Ecclesiastes 1:12

I the [qōheleṯ](../../strongs/h/h6953.md) was [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="ecclesiastes_1_13"></a>Ecclesiastes 1:13

And I [nathan](../../strongs/h/h5414.md) my [leb](../../strongs/h/h3820.md) to [darash](../../strongs/h/h1875.md) and [tûr](../../strongs/h/h8446.md) by [ḥāḵmâ](../../strongs/h/h2451.md) concerning all things that are ['asah](../../strongs/h/h6213.md) under [shamayim](../../strongs/h/h8064.md): this [ra'](../../strongs/h/h7451.md) [ʿinyān](../../strongs/h/h6045.md) hath ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) to the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md) to be [ʿānâ](../../strongs/h/h6031.md) therewith.

<a name="ecclesiastes_1_14"></a>Ecclesiastes 1:14

I have [ra'ah](../../strongs/h/h7200.md) all the [ma'aseh](../../strongs/h/h4639.md) that are ['asah](../../strongs/h/h6213.md) under the [šemeš](../../strongs/h/h8121.md); and, behold, all is [heḇel](../../strongs/h/h1892.md) and [rᵊʿûṯ](../../strongs/h/h7469.md) of [ruwach](../../strongs/h/h7307.md).

<a name="ecclesiastes_1_15"></a>Ecclesiastes 1:15

That which is [ʿāvaṯ](../../strongs/h/h5791.md) [yakol](../../strongs/h/h3201.md) be made [tāqan](../../strongs/h/h8626.md): and that which is [ḥesrôn](../../strongs/h/h2642.md) [yakol](../../strongs/h/h3201.md) be [mānâ](../../strongs/h/h4487.md).

<a name="ecclesiastes_1_16"></a>Ecclesiastes 1:16

I [dabar](../../strongs/h/h1696.md) with mine own [leb](../../strongs/h/h3820.md), ['āmar](../../strongs/h/h559.md), Lo, I am come to [gāḏal](../../strongs/h/h1431.md), and have gotten more [ḥāḵmâ](../../strongs/h/h2451.md) than all they that have been [paniym](../../strongs/h/h6440.md) me in [Yĕruwshalaim](../../strongs/h/h3389.md): yea, my [leb](../../strongs/h/h3820.md) had [rabah](../../strongs/h/h7235.md) [ra'ah](../../strongs/h/h7200.md) of [ḥāḵmâ](../../strongs/h/h2451.md) and [da'ath](../../strongs/h/h1847.md).

<a name="ecclesiastes_1_17"></a>Ecclesiastes 1:17

And I [nathan](../../strongs/h/h5414.md) my [leb](../../strongs/h/h3820.md) to [yada'](../../strongs/h/h3045.md) [ḥāḵmâ](../../strongs/h/h2451.md), and to [yada'](../../strongs/h/h3045.md) [hôlēlâ](../../strongs/h/h1947.md) and [siḵlûṯ](../../strongs/h/h5531.md): I [yada'](../../strongs/h/h3045.md) that this also is [raʿyôn](../../strongs/h/h7475.md) of [ruwach](../../strongs/h/h7307.md).

<a name="ecclesiastes_1_18"></a>Ecclesiastes 1:18

For in [rōḇ](../../strongs/h/h7230.md) [ḥāḵmâ](../../strongs/h/h2451.md) is [rōḇ](../../strongs/h/h7230.md) [ka'ac](../../strongs/h/h3708.md): and he that increaseth [da'ath](../../strongs/h/h1847.md) increaseth [maḵ'ōḇ](../../strongs/h/h4341.md).

---

[Transliteral Bible](../bible.md)

[Ecclesiastes](ecclesiastes.md)

[Ecclesiastes 2](ecclesiastes_2.md)