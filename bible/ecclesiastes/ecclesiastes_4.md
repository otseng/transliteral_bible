# [Ecclesiastes 4](https://www.blueletterbible.org/kjv/ecclesiastes/4)

<a name="ecclesiastes_4_1"></a>Ecclesiastes 4:1

So I [shuwb](../../strongs/h/h7725.md), and [ra'ah](../../strongs/h/h7200.md) all the [ʿăšûqîm](../../strongs/h/h6217.md) that are ['asah](../../strongs/h/h6213.md) under the [šemeš](../../strongs/h/h8121.md): and behold the [dim'ah](../../strongs/h/h1832.md) of such as were [ʿāšaq](../../strongs/h/h6231.md), and they had no [nacham](../../strongs/h/h5162.md); and on the [yad](../../strongs/h/h3027.md) of their [ʿāšaq](../../strongs/h/h6231.md) there was [koach](../../strongs/h/h3581.md); but they had no [nacham](../../strongs/h/h5162.md).

<a name="ecclesiastes_4_2"></a>Ecclesiastes 4:2

Wherefore I [shabach](../../strongs/h/h7623.md) the [muwth](../../strongs/h/h4191.md) which are [kᵊḇār](../../strongs/h/h3528.md) [muwth](../../strongs/h/h4191.md) more than the [chay](../../strongs/h/h2416.md) which are [ʿăḏen](../../strongs/h/h5728.md) [chay](../../strongs/h/h2416.md).

<a name="ecclesiastes_4_3"></a>Ecclesiastes 4:3

Yea, [towb](../../strongs/h/h2896.md) is he than both they, which hath not [ʿăḏen](../../strongs/h/h5728.md) been, who hath not [ra'ah](../../strongs/h/h7200.md) the [ra'](../../strongs/h/h7451.md) [ma'aseh](../../strongs/h/h4639.md) that is ['asah](../../strongs/h/h6213.md) under the [šemeš](../../strongs/h/h8121.md).

<a name="ecclesiastes_4_4"></a>Ecclesiastes 4:4

Again, I [ra'ah](../../strongs/h/h7200.md) all ['amal](../../strongs/h/h5999.md), and every [kišrôn](../../strongs/h/h3788.md) [ma'aseh](../../strongs/h/h4639.md), that for this an ['iysh](../../strongs/h/h376.md) is [qin'â](../../strongs/h/h7068.md) of his [rea'](../../strongs/h/h7453.md). This is also [heḇel](../../strongs/h/h1892.md) and [rᵊʿûṯ](../../strongs/h/h7469.md) of [ruwach](../../strongs/h/h7307.md).

<a name="ecclesiastes_4_5"></a>Ecclesiastes 4:5

The [kᵊsîl](../../strongs/h/h3684.md) [ḥāḇaq](../../strongs/h/h2263.md) his [yad](../../strongs/h/h3027.md) [ḥāḇaq](../../strongs/h/h2263.md), and ['akal](../../strongs/h/h398.md) his own [basar](../../strongs/h/h1320.md).

<a name="ecclesiastes_4_6"></a>Ecclesiastes 4:6

[towb](../../strongs/h/h2896.md) is an [kaph](../../strongs/h/h3709.md) with [naḥaṯ](../../strongs/h/h5183.md), than both the [ḥōp̄en](../../strongs/h/h2651.md) [mᵊlō'](../../strongs/h/h4393.md) with ['amal](../../strongs/h/h5999.md) and [rᵊʿûṯ](../../strongs/h/h7469.md) of [ruwach](../../strongs/h/h7307.md).

<a name="ecclesiastes_4_7"></a>Ecclesiastes 4:7

Then I [shuwb](../../strongs/h/h7725.md), and I [ra'ah](../../strongs/h/h7200.md) [heḇel](../../strongs/h/h1892.md) under the [šemeš](../../strongs/h/h8121.md).

<a name="ecclesiastes_4_8"></a>Ecclesiastes 4:8

There is ['echad](../../strongs/h/h259.md), and there is not a [šēnî](../../strongs/h/h8145.md); yea, he hath neither [ben](../../strongs/h/h1121.md) nor ['ach](../../strongs/h/h251.md): yet is there no [qēṣ](../../strongs/h/h7093.md) of all his ['amal](../../strongs/h/h5999.md); neither is his ['ayin](../../strongs/h/h5869.md) [sāׂbaʿ](../../strongs/h/h7646.md) with [ʿōšer](../../strongs/h/h6239.md); neither, For whom do I [ʿāmēl](../../strongs/h/h6001.md), and [ḥāsēr](../../strongs/h/h2637.md) my [nephesh](../../strongs/h/h5315.md) of [towb](../../strongs/h/h2896.md)? This is also [heḇel](../../strongs/h/h1892.md), yea, it is a [ra'](../../strongs/h/h7451.md) [ʿinyān](../../strongs/h/h6045.md).

<a name="ecclesiastes_4_9"></a>Ecclesiastes 4:9

Two are [towb](../../strongs/h/h2896.md) than one; because they have a [towb](../../strongs/h/h2896.md) [śāḵār](../../strongs/h/h7939.md) for their ['amal](../../strongs/h/h5999.md).

<a name="ecclesiastes_4_10"></a>Ecclesiastes 4:10

For if they [naphal](../../strongs/h/h5307.md), the one will [quwm](../../strongs/h/h6965.md) his [ḥāḇēr](../../strongs/h/h2270.md): but ['î](../../strongs/h/h337.md) to him that is alone when he [naphal](../../strongs/h/h5307.md); for he hath not another to help him [quwm](../../strongs/h/h6965.md).

<a name="ecclesiastes_4_11"></a>Ecclesiastes 4:11

Again, if two [shakab](../../strongs/h/h7901.md), then they have [ḥāmam](../../strongs/h/h2552.md): but how can one be [yāḥam](../../strongs/h/h3179.md) alone?

<a name="ecclesiastes_4_12"></a>Ecclesiastes 4:12

And if one [tāqap̄](../../strongs/h/h8630.md) against him, two shall ['amad](../../strongs/h/h5975.md) him; and a [šālaš](../../strongs/h/h8027.md) [ḥûṭ](../../strongs/h/h2339.md) is not [mᵊhērâ](../../strongs/h/h4120.md) [nathaq](../../strongs/h/h5423.md).

<a name="ecclesiastes_4_13"></a>Ecclesiastes 4:13

[towb](../../strongs/h/h2896.md) is a [miskēn](../../strongs/h/h4542.md) and a [ḥāḵām](../../strongs/h/h2450.md) [yeleḏ](../../strongs/h/h3206.md) than an [zāqēn](../../strongs/h/h2205.md) and [kᵊsîl](../../strongs/h/h3684.md) [melek](../../strongs/h/h4428.md), who [yada'](../../strongs/h/h3045.md) no more be [zāhar](../../strongs/h/h2094.md).

<a name="ecclesiastes_4_14"></a>Ecclesiastes 4:14

For out of ['āsar](../../strongs/h/h631.md) [bayith](../../strongs/h/h1004.md) he [yāṣā'](../../strongs/h/h3318.md) to [mālaḵ](../../strongs/h/h4427.md); whereas also he that is [yalad](../../strongs/h/h3205.md) in his [malkuwth](../../strongs/h/h4438.md) becometh [rûš](../../strongs/h/h7326.md).

<a name="ecclesiastes_4_15"></a>Ecclesiastes 4:15

I [ra'ah](../../strongs/h/h7200.md) all the [chay](../../strongs/h/h2416.md) which [halak](../../strongs/h/h1980.md) under the [šemeš](../../strongs/h/h8121.md), with the second [yeleḏ](../../strongs/h/h3206.md) that shall ['amad](../../strongs/h/h5975.md) in his stead.

<a name="ecclesiastes_4_16"></a>Ecclesiastes 4:16

There is no [qēṣ](../../strongs/h/h7093.md) of all the ['am](../../strongs/h/h5971.md), even of all that have been [paniym](../../strongs/h/h6440.md) them: they also that ['aḥărôn](../../strongs/h/h314.md) shall not [samach](../../strongs/h/h8055.md) in him. Surely this also is [heḇel](../../strongs/h/h1892.md) and [raʿyôn](../../strongs/h/h7475.md) of [ruwach](../../strongs/h/h7307.md).

---

[Transliteral Bible](../bible.md)

[Ecclesiastes](ecclesiastes.md)

[Ecclesiastes 3](ecclesiastes_3.md) - [Ecclesiastes 5](ecclesiastes_5.md)