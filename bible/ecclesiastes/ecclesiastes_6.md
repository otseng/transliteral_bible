# [Ecclesiastes 6](https://www.blueletterbible.org/kjv/ecclesiastes/6)

<a name="ecclesiastes_6_1"></a>Ecclesiastes 6:1

There is an [ra'](../../strongs/h/h7451.md) which I have [ra'ah](../../strongs/h/h7200.md) under the [šemeš](../../strongs/h/h8121.md), and it is [rab](../../strongs/h/h7227.md) among ['āḏām](../../strongs/h/h120.md):

<a name="ecclesiastes_6_2"></a>Ecclesiastes 6:2

An ['iysh](../../strongs/h/h376.md) to whom ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) [ʿōšer](../../strongs/h/h6239.md), [neḵes](../../strongs/h/h5233.md), and [kabowd](../../strongs/h/h3519.md), so that he [ḥāsēr](../../strongs/h/h2638.md) nothing for his [nephesh](../../strongs/h/h5315.md) of all that he ['āvâ](../../strongs/h/h183.md), yet ['Elohiym](../../strongs/h/h430.md) giveth him not [šālaṭ](../../strongs/h/h7980.md) to ['akal](../../strongs/h/h398.md) thereof, but an ['iysh](../../strongs/h/h376.md) [nāḵrî](../../strongs/h/h5237.md) ['akal](../../strongs/h/h398.md) it: this is [heḇel](../../strongs/h/h1892.md), and it is an [ra'](../../strongs/h/h7451.md) [ḥŏlî](../../strongs/h/h2483.md).

<a name="ecclesiastes_6_3"></a>Ecclesiastes 6:3

If an ['iysh](../../strongs/h/h376.md) [yalad](../../strongs/h/h3205.md) an hundred, and [ḥāyâ](../../strongs/h/h2421.md) [rab](../../strongs/h/h7227.md) [šānâ](../../strongs/h/h8141.md), so that the [yowm](../../strongs/h/h3117.md) of his [šānâ](../../strongs/h/h8141.md) be [rab](../../strongs/h/h7227.md), and his [nephesh](../../strongs/h/h5315.md) be not [sāׂbaʿ](../../strongs/h/h7646.md) with [towb](../../strongs/h/h2896.md), and also that he have no [qᵊḇûrâ](../../strongs/h/h6900.md); I ['āmar](../../strongs/h/h559.md), that an untimely [nep̄el](../../strongs/h/h5309.md) is [towb](../../strongs/h/h2896.md) than he.

<a name="ecclesiastes_6_4"></a>Ecclesiastes 6:4

For he [bow'](../../strongs/h/h935.md) in with [heḇel](../../strongs/h/h1892.md), and [yālaḵ](../../strongs/h/h3212.md) in [choshek](../../strongs/h/h2822.md), and his [shem](../../strongs/h/h8034.md) shall be [kāsâ](../../strongs/h/h3680.md) with [choshek](../../strongs/h/h2822.md).

<a name="ecclesiastes_6_5"></a>Ecclesiastes 6:5

Moreover he hath not [ra'ah](../../strongs/h/h7200.md) the [šemeš](../../strongs/h/h8121.md), nor [yada'](../../strongs/h/h3045.md) any thing: this hath more [naḥaṯ](../../strongs/h/h5183.md) than the other.

<a name="ecclesiastes_6_6"></a>Ecclesiastes 6:6

Yea, ['illû](../../strongs/h/h432.md) he [ḥāyâ](../../strongs/h/h2421.md) a thousand [šānâ](../../strongs/h/h8141.md) twice told, yet hath he [ra'ah](../../strongs/h/h7200.md) no [towb](../../strongs/h/h2896.md): do not all [halak](../../strongs/h/h1980.md) to one [maqowm](../../strongs/h/h4725.md)?

<a name="ecclesiastes_6_7"></a>Ecclesiastes 6:7

All the ['amal](../../strongs/h/h5999.md) of ['āḏām](../../strongs/h/h120.md) is for his [peh](../../strongs/h/h6310.md), and yet the [nephesh](../../strongs/h/h5315.md) is not [mālā'](../../strongs/h/h4390.md).

<a name="ecclesiastes_6_8"></a>Ecclesiastes 6:8

For what hath the [ḥāḵām](../../strongs/h/h2450.md) [yôṯēr](../../strongs/h/h3148.md) than the [kᵊsîl](../../strongs/h/h3684.md)? what hath the ['aniy](../../strongs/h/h6041.md), that [yada'](../../strongs/h/h3045.md) to [halak](../../strongs/h/h1980.md) before the [chay](../../strongs/h/h2416.md)?

<a name="ecclesiastes_6_9"></a>Ecclesiastes 6:9

[towb](../../strongs/h/h2896.md) is the [mar'ê](../../strongs/h/h4758.md) of the ['ayin](../../strongs/h/h5869.md) than the [halak](../../strongs/h/h1980.md) of the [nephesh](../../strongs/h/h5315.md): this is also [heḇel](../../strongs/h/h1892.md) and [rᵊʿûṯ](../../strongs/h/h7469.md) of [ruwach](../../strongs/h/h7307.md).

<a name="ecclesiastes_6_10"></a>Ecclesiastes 6:10

That which hath been is [qara'](../../strongs/h/h7121.md) [shem](../../strongs/h/h8034.md) [kᵊḇār](../../strongs/h/h3528.md), and it is [yada'](../../strongs/h/h3045.md) that it is ['āḏām](../../strongs/h/h120.md): neither [yakol](../../strongs/h/h3201.md) he [diyn](../../strongs/h/h1777.md) with him that is [taqqîp̄](../../strongs/h/h8623.md) than he.

<a name="ecclesiastes_6_11"></a>Ecclesiastes 6:11

Seeing there be [rabah](../../strongs/h/h7235.md) [dabar](../../strongs/h/h1697.md) that [rabah](../../strongs/h/h7235.md) [heḇel](../../strongs/h/h1892.md), what is ['āḏām](../../strongs/h/h120.md) the [yôṯēr](../../strongs/h/h3148.md)?

<a name="ecclesiastes_6_12"></a>Ecclesiastes 6:12

For who [yada'](../../strongs/h/h3045.md) what is [towb](../../strongs/h/h2896.md) for ['āḏām](../../strongs/h/h120.md) in this [chay](../../strongs/h/h2416.md), [mispār](../../strongs/h/h4557.md) the [yowm](../../strongs/h/h3117.md) of his [heḇel](../../strongs/h/h1892.md) [chay](../../strongs/h/h2416.md) which he ['asah](../../strongs/h/h6213.md) as a [ṣēl](../../strongs/h/h6738.md)? for who can [nāḡaḏ](../../strongs/h/h5046.md) a ['āḏām](../../strongs/h/h120.md) what shall be ['aḥar](../../strongs/h/h310.md) him under the [šemeš](../../strongs/h/h8121.md)?

---

[Transliteral Bible](../bible.md)

[Ecclesiastes](ecclesiastes.md)

[Ecclesiastes 5](ecclesiastes_5.md) - [Ecclesiastes 7](ecclesiastes_7.md)