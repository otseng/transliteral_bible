# [Ecclesiastes 8](https://www.blueletterbible.org/kjv/ecclesiastes/8)

<a name="ecclesiastes_8_1"></a>Ecclesiastes 8:1

Who is as the [ḥāḵām](../../strongs/h/h2450.md) man? and who [yada'](../../strongs/h/h3045.md) the [pēšer](../../strongs/h/h6592.md) of a [dabar](../../strongs/h/h1697.md)? a ['āḏām](../../strongs/h/h120.md) [ḥāḵmâ](../../strongs/h/h2451.md) maketh his [paniym](../../strongs/h/h6440.md) to ['owr](../../strongs/h/h215.md), and the ['oz](../../strongs/h/h5797.md) of his [paniym](../../strongs/h/h6440.md) shall be [šānā'](../../strongs/h/h8132.md).

<a name="ecclesiastes_8_2"></a>Ecclesiastes 8:2

[shamar](../../strongs/h/h8104.md) the [melek](../../strongs/h/h4428.md) [peh](../../strongs/h/h6310.md), and that in [diḇrâ](../../strongs/h/h1700.md) of the [šᵊḇûʿâ](../../strongs/h/h7621.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="ecclesiastes_8_3"></a>Ecclesiastes 8:3

Be not [bahal](../../strongs/h/h926.md) to go [yālaḵ](../../strongs/h/h3212.md) of his [paniym](../../strongs/h/h6440.md): ['amad](../../strongs/h/h5975.md) not in an [ra'](../../strongs/h/h7451.md) [dabar](../../strongs/h/h1697.md); for he ['asah](../../strongs/h/h6213.md) whatsoever [ḥāp̄ēṣ](../../strongs/h/h2654.md) him.

<a name="ecclesiastes_8_4"></a>Ecclesiastes 8:4

Where the [dabar](../../strongs/h/h1697.md) of a [melek](../../strongs/h/h4428.md) is, there is [šilṭôn](../../strongs/h/h7983.md): and who may ['āmar](../../strongs/h/h559.md) unto him, What ['asah](../../strongs/h/h6213.md) thou?

<a name="ecclesiastes_8_5"></a>Ecclesiastes 8:5

Whoso [shamar](../../strongs/h/h8104.md) the [mitsvah](../../strongs/h/h4687.md) shall [yada'](../../strongs/h/h3045.md) no [ra'](../../strongs/h/h7451.md) [dabar](../../strongs/h/h1697.md): and a [ḥāḵām](../../strongs/h/h2450.md) [leb](../../strongs/h/h3820.md) [yada'](../../strongs/h/h3045.md) both [ʿēṯ](../../strongs/h/h6256.md) and [mishpat](../../strongs/h/h4941.md).

<a name="ecclesiastes_8_6"></a>Ecclesiastes 8:6

Because to every [chephets](../../strongs/h/h2656.md) there is [ʿēṯ](../../strongs/h/h6256.md) and [mishpat](../../strongs/h/h4941.md), therefore the [ra'](../../strongs/h/h7451.md) of ['āḏām](../../strongs/h/h120.md) is [rab](../../strongs/h/h7227.md) upon him.

<a name="ecclesiastes_8_7"></a>Ecclesiastes 8:7

For he [yada'](../../strongs/h/h3045.md) not that which shall be: for who can [nāḡaḏ](../../strongs/h/h5046.md) him when it shall be?

<a name="ecclesiastes_8_8"></a>Ecclesiastes 8:8

There is no ['āḏām](../../strongs/h/h120.md) that hath [šallîṭ](../../strongs/h/h7989.md) over the [ruwach](../../strongs/h/h7307.md) to [kālā'](../../strongs/h/h3607.md) the [ruwach](../../strongs/h/h7307.md); neither hath he [šilṭôn](../../strongs/h/h7983.md) in the [yowm](../../strongs/h/h3117.md) of [maveth](../../strongs/h/h4194.md): and there is no [mišlaḥaṯ](../../strongs/h/h4917.md) in that [milḥāmâ](../../strongs/h/h4421.md); neither shall [resha'](../../strongs/h/h7562.md) [mālaṭ](../../strongs/h/h4422.md) those that are [baʿal](../../strongs/h/h1167.md) to it.

<a name="ecclesiastes_8_9"></a>Ecclesiastes 8:9

All this have I [ra'ah](../../strongs/h/h7200.md), and [nathan](../../strongs/h/h5414.md) my [leb](../../strongs/h/h3820.md) unto every [ma'aseh](../../strongs/h/h4639.md) that is ['asah](../../strongs/h/h6213.md) under the [šemeš](../../strongs/h/h8121.md): there is a [ʿēṯ](../../strongs/h/h6256.md) wherein one ['āḏām](../../strongs/h/h120.md) [šālaṭ](../../strongs/h/h7980.md) over another to his own [ra'](../../strongs/h/h7451.md).

<a name="ecclesiastes_8_10"></a>Ecclesiastes 8:10

And so I [ra'ah](../../strongs/h/h7200.md) the [rasha'](../../strongs/h/h7563.md) [qāḇar](../../strongs/h/h6912.md), who had [bow'](../../strongs/h/h935.md) and [halak](../../strongs/h/h1980.md) from the [maqowm](../../strongs/h/h4725.md) of the [qadowsh](../../strongs/h/h6918.md), and they were [shakach](../../strongs/h/h7911.md) in the [ʿîr](../../strongs/h/h5892.md) where they had so ['asah](../../strongs/h/h6213.md): this is also [heḇel](../../strongs/h/h1892.md).

<a name="ecclesiastes_8_11"></a>Ecclesiastes 8:11

Because [piṯgām](../../strongs/h/h6599.md) against an [ra'](../../strongs/h/h7451.md) [ma'aseh](../../strongs/h/h4639.md) is not ['asah](../../strongs/h/h6213.md) [mᵊhērâ](../../strongs/h/h4120.md), therefore the [leb](../../strongs/h/h3820.md) of the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md) is [mālā'](../../strongs/h/h4390.md) in them to ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md).

<a name="ecclesiastes_8_12"></a>Ecclesiastes 8:12

Though a [chata'](../../strongs/h/h2398.md) ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) an hundred times, and be ['arak](../../strongs/h/h748.md), yet surely I [yada'](../../strongs/h/h3045.md) that it shall be [towb](../../strongs/h/h2896.md) with them that [yārē'](../../strongs/h/h3373.md) ['Elohiym](../../strongs/h/h430.md), which [yare'](../../strongs/h/h3372.md) [paniym](../../strongs/h/h6440.md) him:

<a name="ecclesiastes_8_13"></a>Ecclesiastes 8:13

But it shall not be [towb](../../strongs/h/h2896.md) with the [rasha'](../../strongs/h/h7563.md), neither shall he ['arak](../../strongs/h/h748.md) his [yowm](../../strongs/h/h3117.md), which are as a [ṣēl](../../strongs/h/h6738.md); because he [yārē'](../../strongs/h/h3373.md) not [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md).

<a name="ecclesiastes_8_14"></a>Ecclesiastes 8:14

There is a [heḇel](../../strongs/h/h1892.md) which is ['asah](../../strongs/h/h6213.md) upon the ['erets](../../strongs/h/h776.md); that there be [tsaddiyq](../../strongs/h/h6662.md) men, unto whom it [naga'](../../strongs/h/h5060.md) according to the [ma'aseh](../../strongs/h/h4639.md) of the [rasha'](../../strongs/h/h7563.md); again, there be [rasha'](../../strongs/h/h7563.md) men, to whom it [naga'](../../strongs/h/h5060.md) according to the [ma'aseh](../../strongs/h/h4639.md) of the [tsaddiyq](../../strongs/h/h6662.md): I ['āmar](../../strongs/h/h559.md) that this also is [heḇel](../../strongs/h/h1892.md).

<a name="ecclesiastes_8_15"></a>Ecclesiastes 8:15

Then I [shabach](../../strongs/h/h7623.md) [simchah](../../strongs/h/h8057.md), because a ['āḏām](../../strongs/h/h120.md) hath no better [towb](../../strongs/h/h2896.md) under the [šemeš](../../strongs/h/h8121.md), than to ['akal](../../strongs/h/h398.md), and to [šāṯâ](../../strongs/h/h8354.md), and to be [samach](../../strongs/h/h8055.md): for that shall [lāvâ](../../strongs/h/h3867.md) with him of his ['amal](../../strongs/h/h5999.md) the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md), which ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) him under the [šemeš](../../strongs/h/h8121.md).

<a name="ecclesiastes_8_16"></a>Ecclesiastes 8:16

When I [nathan](../../strongs/h/h5414.md) mine [leb](../../strongs/h/h3820.md) to [yada'](../../strongs/h/h3045.md) [ḥāḵmâ](../../strongs/h/h2451.md), and to [ra'ah](../../strongs/h/h7200.md) the [ʿinyān](../../strongs/h/h6045.md) that is ['asah](../../strongs/h/h6213.md) upon the ['erets](../../strongs/h/h776.md): (for also there is that neither [yowm](../../strongs/h/h3117.md) nor [layil](../../strongs/h/h3915.md) [ra'ah](../../strongs/h/h7200.md) [šēnā'](../../strongs/h/h8142.md) with his ['ayin](../../strongs/h/h5869.md):)

<a name="ecclesiastes_8_17"></a>Ecclesiastes 8:17

Then I [ra'ah](../../strongs/h/h7200.md) all the [ma'aseh](../../strongs/h/h4639.md) of ['Elohiym](../../strongs/h/h430.md), that a ['āḏām](../../strongs/h/h120.md) [yakol](../../strongs/h/h3201.md) [māṣā'](../../strongs/h/h4672.md) the [ma'aseh](../../strongs/h/h4639.md) that is ['asah](../../strongs/h/h6213.md) under the [šemeš](../../strongs/h/h8121.md): because [šel](../../strongs/h/h7945.md) a ['āḏām](../../strongs/h/h120.md) [ʿāmal](../../strongs/h/h5998.md) to [bāqaš](../../strongs/h/h1245.md) it, yet he shall not [māṣā'](../../strongs/h/h4672.md) it; yea further; though a [ḥāḵām](../../strongs/h/h2450.md) ['āmar](../../strongs/h/h559.md) to [yada'](../../strongs/h/h3045.md) it, yet shall he not be [yakol](../../strongs/h/h3201.md) to [māṣā'](../../strongs/h/h4672.md) it.

---

[Transliteral Bible](../bible.md)

[Ecclesiastes](ecclesiastes.md)

[Ecclesiastes 7](ecclesiastes_7.md) - [Ecclesiastes 9](ecclesiastes_9.md)