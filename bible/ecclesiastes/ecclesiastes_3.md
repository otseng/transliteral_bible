# [Ecclesiastes 3](https://www.blueletterbible.org/kjv/ecclesiastes/3)

<a name="ecclesiastes_3_1"></a>Ecclesiastes 3:1

To every thing there is a [zᵊmān](../../strongs/h/h2165.md), and a [ʿēṯ](../../strongs/h/h6256.md) to every [chephets](../../strongs/h/h2656.md) under the [shamayim](../../strongs/h/h8064.md):

<a name="ecclesiastes_3_2"></a>Ecclesiastes 3:2

A [ʿēṯ](../../strongs/h/h6256.md) to be [yalad](../../strongs/h/h3205.md), and a [ʿēṯ](../../strongs/h/h6256.md) to [muwth](../../strongs/h/h4191.md); a [ʿēṯ](../../strongs/h/h6256.md) to [nāṭaʿ](../../strongs/h/h5193.md), and a [ʿēṯ](../../strongs/h/h6256.md) to pluck [ʿāqar](../../strongs/h/h6131.md) that which is [nāṭaʿ](../../strongs/h/h5193.md);

<a name="ecclesiastes_3_3"></a>Ecclesiastes 3:3

A [ʿēṯ](../../strongs/h/h6256.md) to [harag](../../strongs/h/h2026.md), and a [ʿēṯ](../../strongs/h/h6256.md) to [rapha'](../../strongs/h/h7495.md); a [ʿēṯ](../../strongs/h/h6256.md) to [pāraṣ](../../strongs/h/h6555.md), and a [ʿēṯ](../../strongs/h/h6256.md) to [bānâ](../../strongs/h/h1129.md);

<a name="ecclesiastes_3_4"></a>Ecclesiastes 3:4

A [ʿēṯ](../../strongs/h/h6256.md) to [bāḵâ](../../strongs/h/h1058.md), and a [ʿēṯ](../../strongs/h/h6256.md) to [śāḥaq](../../strongs/h/h7832.md); a [ʿēṯ](../../strongs/h/h6256.md) to [sāp̄aḏ](../../strongs/h/h5594.md), and a [ʿēṯ](../../strongs/h/h6256.md) to [rāqaḏ](../../strongs/h/h7540.md);

<a name="ecclesiastes_3_5"></a>Ecclesiastes 3:5

A [ʿēṯ](../../strongs/h/h6256.md) to [shalak](../../strongs/h/h7993.md) ['eben](../../strongs/h/h68.md), and a [ʿēṯ](../../strongs/h/h6256.md) to [kānas](../../strongs/h/h3664.md) ['eben](../../strongs/h/h68.md) [kānas](../../strongs/h/h3664.md); a [ʿēṯ](../../strongs/h/h6256.md) to [ḥāḇaq](../../strongs/h/h2263.md), and a [ʿēṯ](../../strongs/h/h6256.md) to [rachaq](../../strongs/h/h7368.md) from [ḥāḇaq](../../strongs/h/h2263.md);

<a name="ecclesiastes_3_6"></a>Ecclesiastes 3:6

A [ʿēṯ](../../strongs/h/h6256.md) to [bāqaš](../../strongs/h/h1245.md), and a [ʿēṯ](../../strongs/h/h6256.md) to ['abad](../../strongs/h/h6.md); a [ʿēṯ](../../strongs/h/h6256.md) to [shamar](../../strongs/h/h8104.md), and a [ʿēṯ](../../strongs/h/h6256.md) to [shalak](../../strongs/h/h7993.md);

<a name="ecclesiastes_3_7"></a>Ecclesiastes 3:7

A [ʿēṯ](../../strongs/h/h6256.md) to [qāraʿ](../../strongs/h/h7167.md), and a [ʿēṯ](../../strongs/h/h6256.md) to [taphar](../../strongs/h/h8609.md); a [ʿēṯ](../../strongs/h/h6256.md) to keep [ḥāšâ](../../strongs/h/h2814.md), and a [ʿēṯ](../../strongs/h/h6256.md) to [dabar](../../strongs/h/h1696.md);

<a name="ecclesiastes_3_8"></a>Ecclesiastes 3:8

A [ʿēṯ](../../strongs/h/h6256.md) to ['ahab](../../strongs/h/h157.md), and a [ʿēṯ](../../strongs/h/h6256.md) to [sane'](../../strongs/h/h8130.md); a [ʿēṯ](../../strongs/h/h6256.md) of [milḥāmâ](../../strongs/h/h4421.md), and a [ʿēṯ](../../strongs/h/h6256.md) of [shalowm](../../strongs/h/h7965.md).

<a name="ecclesiastes_3_9"></a>Ecclesiastes 3:9

What [yiṯrôn](../../strongs/h/h3504.md) hath he that ['asah](../../strongs/h/h6213.md) in that wherein he [ʿāmēl](../../strongs/h/h6001.md)?

<a name="ecclesiastes_3_10"></a>Ecclesiastes 3:10

I have [ra'ah](../../strongs/h/h7200.md) the [ʿinyān](../../strongs/h/h6045.md), which ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) to the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md) to be [ʿānâ](../../strongs/h/h6031.md) in it.

<a name="ecclesiastes_3_11"></a>Ecclesiastes 3:11

He hath ['asah](../../strongs/h/h6213.md) every thing [yāp̄ê](../../strongs/h/h3303.md) in his [ʿēṯ](../../strongs/h/h6256.md): also he hath [nathan](../../strongs/h/h5414.md) the ['owlam](../../strongs/h/h5769.md) in their [leb](../../strongs/h/h3820.md), so that no ['āḏām](../../strongs/h/h120.md) can [māṣā'](../../strongs/h/h4672.md) the [ma'aseh](../../strongs/h/h4639.md) that ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) from the [ro'sh](../../strongs/h/h7218.md) to the [sôp̄](../../strongs/h/h5490.md).

<a name="ecclesiastes_3_12"></a>Ecclesiastes 3:12

I [yada'](../../strongs/h/h3045.md) that there is no [towb](../../strongs/h/h2896.md) in them, but for a man to [samach](../../strongs/h/h8055.md), and to ['asah](../../strongs/h/h6213.md) [towb](../../strongs/h/h2896.md) in his [chay](../../strongs/h/h2416.md).

<a name="ecclesiastes_3_13"></a>Ecclesiastes 3:13

And also that every ['āḏām](../../strongs/h/h120.md) should ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and [ra'ah](../../strongs/h/h7200.md) the [towb](../../strongs/h/h2896.md) of all his ['amal](../../strongs/h/h5999.md), it is the [mataṯ](../../strongs/h/h4991.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="ecclesiastes_3_14"></a>Ecclesiastes 3:14

I [yada'](../../strongs/h/h3045.md) that, whatsoever ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md), it shall be ['owlam](../../strongs/h/h5769.md): nothing can be put to it, nor any thing [gāraʿ](../../strongs/h/h1639.md) from it: and ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) it, that men should [yare'](../../strongs/h/h3372.md) [paniym](../../strongs/h/h6440.md) him.

<a name="ecclesiastes_3_15"></a>Ecclesiastes 3:15

That which hath been is [kᵊḇār](../../strongs/h/h3528.md); and that which is to be hath [kᵊḇār](../../strongs/h/h3528.md) been; and ['Elohiym](../../strongs/h/h430.md) [bāqaš](../../strongs/h/h1245.md) that which is [radaph](../../strongs/h/h7291.md).

<a name="ecclesiastes_3_16"></a>Ecclesiastes 3:16

And moreover I [ra'ah](../../strongs/h/h7200.md) under the [šemeš](../../strongs/h/h8121.md) the [maqowm](../../strongs/h/h4725.md) of [mishpat](../../strongs/h/h4941.md), that [resha'](../../strongs/h/h7562.md) was there; and the [maqowm](../../strongs/h/h4725.md) of [tsedeq](../../strongs/h/h6664.md), that [resha'](../../strongs/h/h7562.md) was there.

<a name="ecclesiastes_3_17"></a>Ecclesiastes 3:17

I ['āmar](../../strongs/h/h559.md) in mine [leb](../../strongs/h/h3820.md), ['Elohiym](../../strongs/h/h430.md) shall [shaphat](../../strongs/h/h8199.md) the [tsaddiyq](../../strongs/h/h6662.md) and the [rasha'](../../strongs/h/h7563.md): for there is a [ʿēṯ](../../strongs/h/h6256.md) there for every [chephets](../../strongs/h/h2656.md) and for every [ma'aseh](../../strongs/h/h4639.md).

<a name="ecclesiastes_3_18"></a>Ecclesiastes 3:18

I ['āmar](../../strongs/h/h559.md) in mine [leb](../../strongs/h/h3820.md) concerning the [diḇrâ](../../strongs/h/h1700.md) of the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md), that ['Elohiym](../../strongs/h/h430.md) might [bārar](../../strongs/h/h1305.md) them, and that they might [ra'ah](../../strongs/h/h7200.md) that they themselves are [bĕhemah](../../strongs/h/h929.md).

<a name="ecclesiastes_3_19"></a>Ecclesiastes 3:19

For that which [miqrê](../../strongs/h/h4745.md) the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md) [miqrê](../../strongs/h/h4745.md) [bĕhemah](../../strongs/h/h929.md); even one thing [miqrê](../../strongs/h/h4745.md) them: as the one [maveth](../../strongs/h/h4194.md), so [maveth](../../strongs/h/h4194.md) the other; yea, they have all one [ruwach](../../strongs/h/h7307.md); so that a ['āḏām](../../strongs/h/h120.md) hath no [môṯār](../../strongs/h/h4195.md) above a [bĕhemah](../../strongs/h/h929.md): for all is [heḇel](../../strongs/h/h1892.md).

<a name="ecclesiastes_3_20"></a>Ecclesiastes 3:20

All [halak](../../strongs/h/h1980.md) unto one [maqowm](../../strongs/h/h4725.md); all are of the ['aphar](../../strongs/h/h6083.md), and all [shuwb](../../strongs/h/h7725.md) to ['aphar](../../strongs/h/h6083.md) [shuwb](../../strongs/h/h7725.md).

<a name="ecclesiastes_3_21"></a>Ecclesiastes 3:21

Who [yada'](../../strongs/h/h3045.md) the [ruwach](../../strongs/h/h7307.md) of [ben](../../strongs/h/h1121.md) ['āḏām](../../strongs/h/h120.md) that [ʿālâ](../../strongs/h/h5927.md) [maʿal](../../strongs/h/h4605.md), and the [ruwach](../../strongs/h/h7307.md) of the [bĕhemah](../../strongs/h/h929.md) that [yarad](../../strongs/h/h3381.md) [maṭṭâ](../../strongs/h/h4295.md) to the ['erets](../../strongs/h/h776.md)?

<a name="ecclesiastes_3_22"></a>Ecclesiastes 3:22

Wherefore I [ra'ah](../../strongs/h/h7200.md) that there is nothing [towb](../../strongs/h/h2896.md), than that a ['āḏām](../../strongs/h/h120.md) should [samach](../../strongs/h/h8055.md) in his own [ma'aseh](../../strongs/h/h4639.md); for that is his [cheleq](../../strongs/h/h2506.md): for who shall [bow'](../../strongs/h/h935.md) him to [ra'ah](../../strongs/h/h7200.md) what shall be ['aḥar](../../strongs/h/h310.md) him?

---

[Transliteral Bible](../bible.md)

[Ecclesiastes](ecclesiastes.md)

[Ecclesiastes 2](ecclesiastes_2.md) - [Ecclesiastes 4](ecclesiastes_4.md)