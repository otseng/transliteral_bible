# [Ecclesiastes 12](https://www.blueletterbible.org/kjv/ecclesiastes/12)

<a name="ecclesiastes_12_1"></a>Ecclesiastes 12:1

[zakar](../../strongs/h/h2142.md) now thy [bara'](../../strongs/h/h1254.md) in the [yowm](../../strongs/h/h3117.md) of thy [bᵊḥurîm](../../strongs/h/h979.md), while the [ra'](../../strongs/h/h7451.md) [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md) not, nor the [šānâ](../../strongs/h/h8141.md) [naga'](../../strongs/h/h5060.md), when thou shalt ['āmar](../../strongs/h/h559.md), I have no [chephets](../../strongs/h/h2656.md) in them;

<a name="ecclesiastes_12_2"></a>Ecclesiastes 12:2

While the [šemeš](../../strongs/h/h8121.md), or the ['owr](../../strongs/h/h216.md), or the [yareach](../../strongs/h/h3394.md), or the [kowkab](../../strongs/h/h3556.md), be not [ḥāšaḵ](../../strongs/h/h2821.md), nor the ['ab](../../strongs/h/h5645.md) [shuwb](../../strongs/h/h7725.md) ['aḥar](../../strongs/h/h310.md) the [gešem](../../strongs/h/h1653.md):

<a name="ecclesiastes_12_3"></a>Ecclesiastes 12:3

In the [yowm](../../strongs/h/h3117.md) when the [shamar](../../strongs/h/h8104.md) of the [bayith](../../strongs/h/h1004.md) shall [zûaʿ](../../strongs/h/h2111.md), and the [ḥayil](../../strongs/h/h2428.md) ['enowsh](../../strongs/h/h582.md) shall [ʿāvaṯ](../../strongs/h/h5791.md) themselves, and the [ṭāḥan](../../strongs/h/h2912.md) [bāṭal](../../strongs/h/h988.md) because they are [māʿaṭ](../../strongs/h/h4591.md), and those that [ra'ah](../../strongs/h/h7200.md) of the ['ărubâ](../../strongs/h/h699.md) be [ḥāšaḵ](../../strongs/h/h2821.md),

<a name="ecclesiastes_12_4"></a>Ecclesiastes 12:4

And the [deleṯ](../../strongs/h/h1817.md) shall be [cagar](../../strongs/h/h5462.md) in the [šûq](../../strongs/h/h7784.md), when the [qowl](../../strongs/h/h6963.md) of the [ṭaḥănâ](../../strongs/h/h2913.md) is [šāp̄āl](../../strongs/h/h8217.md), and he shall [quwm](../../strongs/h/h6965.md) at the [qowl](../../strongs/h/h6963.md) of the [tsippowr](../../strongs/h/h6833.md), and all the [bath](../../strongs/h/h1323.md) of [šîr](../../strongs/h/h7892.md) shall be [shachach](../../strongs/h/h7817.md);

<a name="ecclesiastes_12_5"></a>Ecclesiastes 12:5

Also when they shall be [yare'](../../strongs/h/h3372.md) of that which is [gāḇōha](../../strongs/h/h1364.md), and [ḥaṯḥaṯ](../../strongs/h/h2849.md) shall be in the [derek](../../strongs/h/h1870.md), and the [šāqēḏ](../../strongs/h/h8247.md) shall [na'ats](../../strongs/h/h5006.md), and the [ḥāḡāḇ](../../strongs/h/h2284.md) shall be a [sāḇal](../../strongs/h/h5445.md), and ['ăḇîyônâ](../../strongs/h/h35.md) shall [pārar](../../strongs/h/h6565.md): because ['āḏām](../../strongs/h/h120.md) [halak](../../strongs/h/h1980.md) to his ['owlam](../../strongs/h/h5769.md) [bayith](../../strongs/h/h1004.md), and the [sāp̄aḏ](../../strongs/h/h5594.md) go [cabab](../../strongs/h/h5437.md) the [šûq](../../strongs/h/h7784.md):

<a name="ecclesiastes_12_6"></a>Ecclesiastes 12:6

Or ever the [keceph](../../strongs/h/h3701.md) [chebel](../../strongs/h/h2256.md) be [rāṯaq](../../strongs/h/h7576.md) [rachaq](../../strongs/h/h7368.md), or the [zāhāḇ](../../strongs/h/h2091.md) [gullâ](../../strongs/h/h1543.md) be [rāṣaṣ](../../strongs/h/h7533.md), or the [kaḏ](../../strongs/h/h3537.md) be [shabar](../../strongs/h/h7665.md) at the [mabûaʿ](../../strongs/h/h4002.md), or the [galgal](../../strongs/h/h1534.md) [rāṣaṣ](../../strongs/h/h7533.md) at the [bowr](../../strongs/h/h953.md).

<a name="ecclesiastes_12_7"></a>Ecclesiastes 12:7

Then shall the ['aphar](../../strongs/h/h6083.md) [shuwb](../../strongs/h/h7725.md) to the ['erets](../../strongs/h/h776.md) as it was: and the [ruwach](../../strongs/h/h7307.md) shall [shuwb](../../strongs/h/h7725.md) unto ['Elohiym](../../strongs/h/h430.md) who [nathan](../../strongs/h/h5414.md) it.

<a name="ecclesiastes_12_8"></a>Ecclesiastes 12:8

[heḇel](../../strongs/h/h1892.md) of [heḇel](../../strongs/h/h1892.md), ['āmar](../../strongs/h/h559.md) the [qōheleṯ](../../strongs/h/h6953.md); all is [heḇel](../../strongs/h/h1892.md).

<a name="ecclesiastes_12_9"></a>Ecclesiastes 12:9

And [yôṯēr](../../strongs/h/h3148.md), because the [qōheleṯ](../../strongs/h/h6953.md) was [ḥāḵām](../../strongs/h/h2450.md), he still [lamad](../../strongs/h/h3925.md) the ['am](../../strongs/h/h5971.md) [da'ath](../../strongs/h/h1847.md); yea, he ['āzan](../../strongs/h/h239.md) , and [chaqar](../../strongs/h/h2713.md), and [tāqan](../../strongs/h/h8626.md) [rabah](../../strongs/h/h7235.md) [māšāl](../../strongs/h/h4912.md).

<a name="ecclesiastes_12_10"></a>Ecclesiastes 12:10

The [qōheleṯ](../../strongs/h/h6953.md) [bāqaš](../../strongs/h/h1245.md) to [māṣā'](../../strongs/h/h4672.md) [chephets](../../strongs/h/h2656.md) [dabar](../../strongs/h/h1697.md): and that which was [kāṯaḇ](../../strongs/h/h3789.md) was [yōšer](../../strongs/h/h3476.md), even [dabar](../../strongs/h/h1697.md) of ['emeth](../../strongs/h/h571.md).

<a name="ecclesiastes_12_11"></a>Ecclesiastes 12:11

The [dabar](../../strongs/h/h1697.md) of the [ḥāḵām](../../strongs/h/h2450.md) are as [dārḇôn](../../strongs/h/h1861.md), and as [maśmērâ](../../strongs/h/h4930.md) [nāṭaʿ](../../strongs/h/h5193.md) by the [baʿal](../../strongs/h/h1167.md) of ['ăsupâ](../../strongs/h/h627.md), which are [nathan](../../strongs/h/h5414.md) from one [ra'ah](../../strongs/h/h7462.md).

<a name="ecclesiastes_12_12"></a>Ecclesiastes 12:12

And [yôṯēr](../../strongs/h/h3148.md), by these, my [ben](../../strongs/h/h1121.md), be [zāhar](../../strongs/h/h2094.md): of ['asah](../../strongs/h/h6213.md) [rabah](../../strongs/h/h7235.md) [sēp̄er](../../strongs/h/h5612.md) there is no [qēṣ](../../strongs/h/h7093.md); and [rabah](../../strongs/h/h7235.md) [lahaḡ](../../strongs/h/h3854.md) is a [yᵊḡîʿâ](../../strongs/h/h3024.md) of the [basar](../../strongs/h/h1320.md).

<a name="ecclesiastes_12_13"></a>Ecclesiastes 12:13

Let us [shama'](../../strongs/h/h8085.md) the [sôp̄](../../strongs/h/h5490.md) of the [dabar](../../strongs/h/h1697.md): [yare'](../../strongs/h/h3372.md) ['Elohiym](../../strongs/h/h430.md), and [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md): for this is the whole of ['āḏām](../../strongs/h/h120.md).

<a name="ecclesiastes_12_14"></a>Ecclesiastes 12:14

For ['Elohiym](../../strongs/h/h430.md) shall [bow'](../../strongs/h/h935.md) every [ma'aseh](../../strongs/h/h4639.md) into [mishpat](../../strongs/h/h4941.md), with every ['alam](../../strongs/h/h5956.md), whether it be [towb](../../strongs/h/h2896.md), or whether it be [ra'](../../strongs/h/h7451.md).

---

[Transliteral Bible](../bible.md)

[Ecclesiastes](ecclesiastes.md)

[Ecclesiastes 11](ecclesiastes_11.md)