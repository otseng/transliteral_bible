# [1Chronicles 9](https://www.blueletterbible.org/kjv/1chronicles/9)

<a name="1chronicles_9_1"></a>1Chronicles 9:1

So all [Yisra'el](../../strongs/h/h3478.md) were [yāḥaś](../../strongs/h/h3187.md); and, behold, they were [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) and [Yehuwdah](../../strongs/h/h3063.md), who were [gālâ](../../strongs/h/h1540.md) to [Bāḇel](../../strongs/h/h894.md) for their [maʿal](../../strongs/h/h4604.md).

<a name="1chronicles_9_2"></a>1Chronicles 9:2

Now the [ri'šôn](../../strongs/h/h7223.md) [yashab](../../strongs/h/h3427.md) that dwelt in their ['achuzzah](../../strongs/h/h272.md) in their [ʿîr](../../strongs/h/h5892.md) were, the [Yisra'el](../../strongs/h/h3478.md), the [kōhēn](../../strongs/h/h3548.md), [Lᵊvî](../../strongs/h/h3881.md), and the [Nāṯîn](../../strongs/h/h5411.md).

<a name="1chronicles_9_3"></a>1Chronicles 9:3

And in [Yĕruwshalaim](../../strongs/h/h3389.md) [yashab](../../strongs/h/h3427.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md), and of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md), and of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md), and [Mᵊnaššê](../../strongs/h/h4519.md);

<a name="1chronicles_9_4"></a>1Chronicles 9:4

[ʿÛṯay](../../strongs/h/h5793.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmîhûḏ](../../strongs/h/h5989.md), the [ben](../../strongs/h/h1121.md) of [ʿĀmrî](../../strongs/h/h6018.md), the [ben](../../strongs/h/h1121.md) of ['Imrî](../../strongs/h/h566.md), the [ben](../../strongs/h/h1121.md) of [Bānî](../../strongs/h/h1137.md), of the [ben](../../strongs/h/h1121.md) of [Pereṣ](../../strongs/h/h6557.md) the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="1chronicles_9_5"></a>1Chronicles 9:5

And of the [Šîlōnî](../../strongs/h/h7888.md); [ʿĂśāyâ](../../strongs/h/h6222.md) the [bᵊḵôr](../../strongs/h/h1060.md), and his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_9_6"></a>1Chronicles 9:6

And of the [ben](../../strongs/h/h1121.md) of [Zeraḥ](../../strongs/h/h2226.md); [YᵊʿÛ'Ēl](../../strongs/h/h3262.md), and their ['ach](../../strongs/h/h251.md), six hundred and ninety.

<a name="1chronicles_9_7"></a>1Chronicles 9:7

And of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md); [Sallû](../../strongs/h/h5543.md) the [ben](../../strongs/h/h1121.md) of [Mᵊšullām](../../strongs/h/h4918.md), the [ben](../../strongs/h/h1121.md) of [Hôḏavyâ](../../strongs/h/h1938.md), the [ben](../../strongs/h/h1121.md) of [Sᵊnû'Â](../../strongs/h/h5574.md),

<a name="1chronicles_9_8"></a>1Chronicles 9:8

And [Yiḇnᵊyâ](../../strongs/h/h2997.md) the [ben](../../strongs/h/h1121.md) of [Yᵊrōḥām](../../strongs/h/h3395.md), and ['Ēlâ](../../strongs/h/h425.md) the [ben](../../strongs/h/h1121.md) of [ʿUzzî](../../strongs/h/h5813.md), the [ben](../../strongs/h/h1121.md) of [Miḵrî](../../strongs/h/h4381.md), and [Mᵊšullām](../../strongs/h/h4918.md) the [ben](../../strongs/h/h1121.md) of [Šᵊp̄Aṭyâ](../../strongs/h/h8203.md), the [ben](../../strongs/h/h1121.md) of [Rᵊʿû'ēl](../../strongs/h/h7467.md), the [ben](../../strongs/h/h1121.md) of [Yiḇnîyâ](../../strongs/h/h2998.md);

<a name="1chronicles_9_9"></a>1Chronicles 9:9

And their ['ach](../../strongs/h/h251.md), according to their [towlĕdah](../../strongs/h/h8435.md), nine hundred and fifty and six. All these ['enowsh](../../strongs/h/h582.md) were [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) in the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md).

<a name="1chronicles_9_10"></a>1Chronicles 9:10

And of the [kōhēn](../../strongs/h/h3548.md); [YᵊḏaʿYâ](../../strongs/h/h3048.md), and [Yᵊhôyārîḇ](../../strongs/h/h3080.md), and [Yāḵîn](../../strongs/h/h3199.md),

<a name="1chronicles_9_11"></a>1Chronicles 9:11

And [ʿĂzaryâ](../../strongs/h/h5838.md) the [ben](../../strongs/h/h1121.md) of [Ḥilqîyâ](../../strongs/h/h2518.md), the [ben](../../strongs/h/h1121.md) of [Mᵊšullām](../../strongs/h/h4918.md), the [ben](../../strongs/h/h1121.md) of [Ṣāḏôq](../../strongs/h/h6659.md), the [ben](../../strongs/h/h1121.md) of [Mᵊrāyôṯ](../../strongs/h/h4812.md), the [ben](../../strongs/h/h1121.md) of ['Ăḥîṭûḇ](../../strongs/h/h285.md), the [nāḡîḏ](../../strongs/h/h5057.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md);

<a name="1chronicles_9_12"></a>1Chronicles 9:12

And [ʿĂḏāyâ](../../strongs/h/h5718.md) the [ben](../../strongs/h/h1121.md) of [Yᵊrōḥām](../../strongs/h/h3395.md), the [ben](../../strongs/h/h1121.md) of [Pašḥûr](../../strongs/h/h6583.md), the [ben](../../strongs/h/h1121.md) of [Malkîyâ](../../strongs/h/h4441.md), and [MaʿĂśay](../../strongs/h/h4640.md) the [ben](../../strongs/h/h1121.md) of [ʿĂḏî'Ēl](../../strongs/h/h5717.md), the [ben](../../strongs/h/h1121.md) of [Yaḥzērâ](../../strongs/h/h3170.md), the [ben](../../strongs/h/h1121.md) of [Mᵊšullām](../../strongs/h/h4918.md), the [ben](../../strongs/h/h1121.md) of [Mᵊšillēmîṯ](../../strongs/h/h4921.md), the [ben](../../strongs/h/h1121.md) of ['Immēr](../../strongs/h/h564.md);

<a name="1chronicles_9_13"></a>1Chronicles 9:13

And their ['ach](../../strongs/h/h251.md), [ro'sh](../../strongs/h/h7218.md) of the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), a thousand and seven hundred and threescore; [ḥayil](../../strongs/h/h2428.md) [gibôr](../../strongs/h/h1368.md) for the [mĕla'kah](../../strongs/h/h4399.md) of the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="1chronicles_9_14"></a>1Chronicles 9:14

And of the [Lᵊvî](../../strongs/h/h3881.md); [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the [ben](../../strongs/h/h1121.md) of [Ḥaššûḇ](../../strongs/h/h2815.md), the [ben](../../strongs/h/h1121.md) of [ʿAzrîqām](../../strongs/h/h5840.md), the [ben](../../strongs/h/h1121.md) of [Ḥăšaḇyâ](../../strongs/h/h2811.md), of the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md);

<a name="1chronicles_9_15"></a>1Chronicles 9:15

And [Baqbaqqar](../../strongs/h/h1230.md), [Ḥereš](../../strongs/h/h2792.md), and [Gālāl](../../strongs/h/h1559.md), and [Matanyâ](../../strongs/h/h4983.md) the [ben](../../strongs/h/h1121.md) of [Mîḵā'](../../strongs/h/h4316.md), the [ben](../../strongs/h/h1121.md) of [Ziḵrî](../../strongs/h/h2147.md), the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md);

<a name="1chronicles_9_16"></a>1Chronicles 9:16

And [ʿŌḇaḏyâ](../../strongs/h/h5662.md) the [ben](../../strongs/h/h1121.md) of [ŠᵊmaʿYâ](../../strongs/h/h8098.md), the [ben](../../strongs/h/h1121.md) of [Gālāl](../../strongs/h/h1559.md), the [ben](../../strongs/h/h1121.md) of [Yᵊḏûṯûn](../../strongs/h/h3038.md), and [Bereḵyâ](../../strongs/h/h1296.md) the [ben](../../strongs/h/h1121.md) of ['Āsā'](../../strongs/h/h609.md), the [ben](../../strongs/h/h1121.md) of ['Elqānâ](../../strongs/h/h511.md), that [yashab](../../strongs/h/h3427.md) in the [ḥāṣēr](../../strongs/h/h2691.md) of the [Nᵊṭōp̄āṯî](../../strongs/h/h5200.md).

<a name="1chronicles_9_17"></a>1Chronicles 9:17

And the [šôʿēr](../../strongs/h/h7778.md) were, [Šallûm](../../strongs/h/h7967.md), and [ʿAqqûḇ](../../strongs/h/h6126.md), and [Ṭalmôn](../../strongs/h/h2929.md), and ['Ăḥîmān](../../strongs/h/h289.md), and their ['ach](../../strongs/h/h251.md): [Šallûm](../../strongs/h/h7967.md) was the [ro'sh](../../strongs/h/h7218.md);

<a name="1chronicles_9_18"></a>1Chronicles 9:18

Who hitherto waited in the [melek](../../strongs/h/h4428.md) [sha'ar](../../strongs/h/h8179.md) [mizrach](../../strongs/h/h4217.md): they were [šôʿēr](../../strongs/h/h7778.md) in the [maḥănê](../../strongs/h/h4264.md) of the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md).

<a name="1chronicles_9_19"></a>1Chronicles 9:19

And [Šallûm](../../strongs/h/h7967.md) the [ben](../../strongs/h/h1121.md) of [Qōrē'](../../strongs/h/h6981.md), the [ben](../../strongs/h/h1121.md) of ['Eḇyāsāp̄](../../strongs/h/h43.md), the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md), and his ['ach](../../strongs/h/h251.md), of the [bayith](../../strongs/h/h1004.md) of his ['ab](../../strongs/h/h1.md), the [Qārḥî](../../strongs/h/h7145.md), were over the [mĕla'kah](../../strongs/h/h4399.md) of the [ʿăḇōḏâ](../../strongs/h/h5656.md), [shamar](../../strongs/h/h8104.md) of the [caph](../../strongs/h/h5592.md) of the ['ohel](../../strongs/h/h168.md): and their ['ab](../../strongs/h/h1.md), being over the [maḥănê](../../strongs/h/h4264.md) of [Yĕhovah](../../strongs/h/h3068.md), were [shamar](../../strongs/h/h8104.md) of the [māḇô'](../../strongs/h/h3996.md).

<a name="1chronicles_9_20"></a>1Chronicles 9:20

And [Pînḥās](../../strongs/h/h6372.md) the [ben](../../strongs/h/h1121.md) of ['Elʿāzār](../../strongs/h/h499.md) was the [nāḡîḏ](../../strongs/h/h5057.md) over them in time [paniym](../../strongs/h/h6440.md), and [Yĕhovah](../../strongs/h/h3068.md) was with him.

<a name="1chronicles_9_21"></a>1Chronicles 9:21

And [Zᵊḵaryâ](../../strongs/h/h2148.md) the [ben](../../strongs/h/h1121.md) of [Mᵊšelemyâ](../../strongs/h/h4920.md) was [šôʿēr](../../strongs/h/h7778.md) of the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="1chronicles_9_22"></a>1Chronicles 9:22

All these which were [bārar](../../strongs/h/h1305.md) to be [šôʿēr](../../strongs/h/h7778.md) in the [caph](../../strongs/h/h5592.md) were two hundred and twelve . These were reckoned by their [yāḥaś](../../strongs/h/h3187.md) in their [ḥāṣēr](../../strongs/h/h2691.md), whom [Dāviḏ](../../strongs/h/h1732.md) and [Šᵊmû'Ēl](../../strongs/h/h8050.md) the [ra'ah](../../strongs/h/h7200.md) did [yacad](../../strongs/h/h3245.md) in their ['ĕmûnâ](../../strongs/h/h530.md).

<a name="1chronicles_9_23"></a>1Chronicles 9:23

So they and their [ben](../../strongs/h/h1121.md) had the oversight of the [sha'ar](../../strongs/h/h8179.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), namely, the [bayith](../../strongs/h/h1004.md) of the ['ohel](../../strongs/h/h168.md), by [mišmereṯ](../../strongs/h/h4931.md).

<a name="1chronicles_9_24"></a>1Chronicles 9:24

In four [ruwach](../../strongs/h/h7307.md) were the [šôʿēr](../../strongs/h/h7778.md), toward the [mizrach](../../strongs/h/h4217.md), [yam](../../strongs/h/h3220.md), [ṣāp̄ôn](../../strongs/h/h6828.md), and [neḡeḇ](../../strongs/h/h5045.md).

<a name="1chronicles_9_25"></a>1Chronicles 9:25

And their ['ach](../../strongs/h/h251.md), which were in their [ḥāṣēr](../../strongs/h/h2691.md), were to [bow'](../../strongs/h/h935.md) after seven [yowm](../../strongs/h/h3117.md) from [ʿēṯ](../../strongs/h/h6256.md) to [ʿēṯ](../../strongs/h/h6256.md) with them.

<a name="1chronicles_9_26"></a>1Chronicles 9:26

For these [Lᵊvî](../../strongs/h/h3881.md), the four [gibôr](../../strongs/h/h1368.md) [šôʿēr](../../strongs/h/h7778.md), were in their set ['ĕmûnâ](../../strongs/h/h530.md), and were over the [liškâ](../../strongs/h/h3957.md) and ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="1chronicles_9_27"></a>1Chronicles 9:27

And they [lûn](../../strongs/h/h3885.md) [cabiyb](../../strongs/h/h5439.md) the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), because the [mišmereṯ](../../strongs/h/h4931.md) was upon them, and the [map̄tēaḥ](../../strongs/h/h4668.md) thereof every [boqer](../../strongs/h/h1242.md) pertained to them.

<a name="1chronicles_9_28"></a>1Chronicles 9:28

And certain of them had the charge of the [ʿăḇōḏâ](../../strongs/h/h5656.md) [kĕliy](../../strongs/h/h3627.md), that they should bring them [bow'](../../strongs/h/h935.md) and [yāṣā'](../../strongs/h/h3318.md) by [mispār](../../strongs/h/h4557.md) [mispār](../../strongs/h/h4557.md).

<a name="1chronicles_9_29"></a>1Chronicles 9:29

Some of them also were [mānâ](../../strongs/h/h4487.md) to oversee the [kĕliy](../../strongs/h/h3627.md), and all the [kĕliy](../../strongs/h/h3627.md) of the [qodesh](../../strongs/h/h6944.md), and the fine [sōleṯ](../../strongs/h/h5560.md), and the [yayin](../../strongs/h/h3196.md), and the [šemen](../../strongs/h/h8081.md), and the [lᵊḇônâ](../../strongs/h/h3828.md), and the [beśem](../../strongs/h/h1314.md).

<a name="1chronicles_9_30"></a>1Chronicles 9:30

And some of the [ben](../../strongs/h/h1121.md) of the [kōhēn](../../strongs/h/h3548.md) [rāqaḥ](../../strongs/h/h7543.md) the [mirqaḥaṯ](../../strongs/h/h4842.md) of the [beśem](../../strongs/h/h1314.md).

<a name="1chronicles_9_31"></a>1Chronicles 9:31

And [Mataṯyâ](../../strongs/h/h4993.md), one of the [Lᵊvî](../../strongs/h/h3881.md), who was the [bᵊḵôr](../../strongs/h/h1060.md) of [Šallûm](../../strongs/h/h7967.md) the [Qārḥî](../../strongs/h/h7145.md), had the set ['ĕmûnâ](../../strongs/h/h530.md) over the things that were [ma'aseh](../../strongs/h/h4639.md) in the [ḥăḇitîm](../../strongs/h/h2281.md).

<a name="1chronicles_9_32"></a>1Chronicles 9:32

And other of their ['ach](../../strongs/h/h251.md), of the [ben](../../strongs/h/h1121.md) of the [Qᵊhāṯî](../../strongs/h/h6956.md), were over the [lechem](../../strongs/h/h3899.md) [maʿăreḵeṯ](../../strongs/h/h4635.md), to [kuwn](../../strongs/h/h3559.md) it every [shabbath](../../strongs/h/h7676.md).

<a name="1chronicles_9_33"></a>1Chronicles 9:33

And these are the [shiyr](../../strongs/h/h7891.md), [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of the [Lᵊvî](../../strongs/h/h3881.md), who remaining in the [liškâ](../../strongs/h/h3957.md) were [pāṭar](../../strongs/h/h6362.md) h6359: for they were employed in that [mĕla'kah](../../strongs/h/h4399.md) [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md).

<a name="1chronicles_9_34"></a>1Chronicles 9:34

These [ro'sh](../../strongs/h/h7218.md) ['ab](../../strongs/h/h1.md) of the [Lᵊvî](../../strongs/h/h3881.md) were [ro'sh](../../strongs/h/h7218.md) throughout their [towlĕdah](../../strongs/h/h8435.md); these [yashab](../../strongs/h/h3427.md) at [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="1chronicles_9_35"></a>1Chronicles 9:35

And in [Giḇʿôn](../../strongs/h/h1391.md) [yashab](../../strongs/h/h3427.md) the ['ab](../../strongs/h/h1.md) of [Giḇʿôn](../../strongs/h/h1391.md) h25, [YᵊʿÎ'Ēl](../../strongs/h/h3273.md), whose ['ishshah](../../strongs/h/h802.md) [shem](../../strongs/h/h8034.md) was [Maʿăḵâ](../../strongs/h/h4601.md):

<a name="1chronicles_9_36"></a>1Chronicles 9:36

And his [bᵊḵôr](../../strongs/h/h1060.md) [ben](../../strongs/h/h1121.md) [ʿAḇdôn](../../strongs/h/h5658.md), then [Ṣaûār](../../strongs/h/h6698.md), and [Qîš](../../strongs/h/h7027.md), and [BaʿAl](../../strongs/h/h1168.md), and [Nēr](../../strongs/h/h5369.md), and [Nāḏāḇ](../../strongs/h/h5070.md),

<a name="1chronicles_9_37"></a>1Chronicles 9:37

And [Gᵊḏōr](../../strongs/h/h1446.md), and ['Aḥyô](../../strongs/h/h283.md), and [Zᵊḵaryâ](../../strongs/h/h2148.md), and [Miqlôṯ](../../strongs/h/h4732.md).

<a name="1chronicles_9_38"></a>1Chronicles 9:38

And [Miqlôṯ](../../strongs/h/h4732.md) [yalad](../../strongs/h/h3205.md) [Šim'Ām](../../strongs/h/h8043.md). And they also [yashab](../../strongs/h/h3427.md) with their ['ach](../../strongs/h/h251.md) at [Yĕruwshalaim](../../strongs/h/h3389.md), over against their ['ach](../../strongs/h/h251.md).

<a name="1chronicles_9_39"></a>1Chronicles 9:39

And [Nēr](../../strongs/h/h5369.md) [yalad](../../strongs/h/h3205.md) [Qîš](../../strongs/h/h7027.md); and [Qîš](../../strongs/h/h7027.md) [yalad](../../strongs/h/h3205.md) [Šā'ûl](../../strongs/h/h7586.md); and [Šā'ûl](../../strongs/h/h7586.md) [yalad](../../strongs/h/h3205.md) [Yᵊhônāṯān](../../strongs/h/h3083.md), and [Malkîšûaʿ](../../strongs/h/h4444.md), and ['Ăḇînāḏāḇ](../../strongs/h/h41.md), and ['EšbaʿAl](../../strongs/h/h792.md).

<a name="1chronicles_9_40"></a>1Chronicles 9:40

And the [ben](../../strongs/h/h1121.md) of [Yᵊhônāṯān](../../strongs/h/h3083.md) was [Mᵊrîḇ BaʿAl](../../strongs/h/h4807.md): and [Mᵊrî ḆaʿAl](../../strongs/h/h4810.md) [yalad](../../strongs/h/h3205.md) [Mîḵâ](../../strongs/h/h4318.md).

<a name="1chronicles_9_41"></a>1Chronicles 9:41

And the [ben](../../strongs/h/h1121.md) of [Mîḵâ](../../strongs/h/h4318.md) were, [Pîṯôn](../../strongs/h/h6377.md), and [Meleḵ](../../strongs/h/h4429.md), and [Taḥrēaʿ](../../strongs/h/h8475.md), and Ahaz. [^1]

<a name="1chronicles_9_42"></a>1Chronicles 9:42

And ['Āḥāz](../../strongs/h/h271.md) [yalad](../../strongs/h/h3205.md) [YaʿRâ](../../strongs/h/h3294.md); and [YaʿRâ](../../strongs/h/h3294.md) [yalad](../../strongs/h/h3205.md) [ʿĀlemeṯ](../../strongs/h/h5964.md), and [ʿAzmāveṯ](../../strongs/h/h5820.md), and [Zimrî](../../strongs/h/h2174.md); and [Zimrî](../../strongs/h/h2174.md) [yalad](../../strongs/h/h3205.md) [Môṣā'](../../strongs/h/h4162.md);

<a name="1chronicles_9_43"></a>1Chronicles 9:43

And [Môṣā'](../../strongs/h/h4162.md) [yalad](../../strongs/h/h3205.md) [BinʿĀ'](../../strongs/h/h1150.md); and [Rᵊp̄Āyâ](../../strongs/h/h7509.md) his [ben](../../strongs/h/h1121.md), ['ElʿĀśâ](../../strongs/h/h501.md) his [ben](../../strongs/h/h1121.md), ['Āṣēl](../../strongs/h/h682.md) his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_9_44"></a>1Chronicles 9:44

And ['Āṣēl](../../strongs/h/h682.md) had six [ben](../../strongs/h/h1121.md), whose [shem](../../strongs/h/h8034.md) are these, [ʿAzrîqām](../../strongs/h/h5840.md), [Bōḵrû](../../strongs/h/h1074.md), and [Yišmāʿē'l](../../strongs/h/h3458.md), and [ŠᵊʿAryâ](../../strongs/h/h8187.md), and [ʿŌḇaḏyâ](../../strongs/h/h5662.md), and [Ḥānān](../../strongs/h/h2605.md): these were the [ben](../../strongs/h/h1121.md) of ['Āṣēl](../../strongs/h/h682.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 8](1chronicles_8.md) - [1Chronicles 10](1chronicles_10.md)

---

[^1]: [1 Chronicles 9:41 Commentary](../../commentary/1chronicles/1chronicles_9_commentary.md#1chronicles_9_41)

