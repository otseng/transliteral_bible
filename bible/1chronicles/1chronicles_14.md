# [1Chronicles 14](https://www.blueletterbible.org/kjv/1chronicles/14)

<a name="1chronicles_14_1"></a>1Chronicles 14:1

Now [Ḥîrām](../../strongs/h/h2438.md) [Ḥûrām](../../strongs/h/h2361.md) [melek](../../strongs/h/h4428.md) of [Ṣōr](../../strongs/h/h6865.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to [Dāviḏ](../../strongs/h/h1732.md), and ['ets](../../strongs/h/h6086.md) of ['erez](../../strongs/h/h730.md), with [qîr](../../strongs/h/h7023.md) and [ḥārāš](../../strongs/h/h2796.md), to [bānâ](../../strongs/h/h1129.md) him a [bayith](../../strongs/h/h1004.md).

<a name="1chronicles_14_2"></a>1Chronicles 14:2

And [Dāviḏ](../../strongs/h/h1732.md) [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) had [kuwn](../../strongs/h/h3559.md) him [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md), for his [malkuwth](../../strongs/h/h4438.md) was [nasa'](../../strongs/h/h5375.md) on [maʿal](../../strongs/h/h4605.md), because of his ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_14_3"></a>1Chronicles 14:3

And [Dāviḏ](../../strongs/h/h1732.md) [laqach](../../strongs/h/h3947.md) more ['ishshah](../../strongs/h/h802.md) at [Yĕruwshalaim](../../strongs/h/h3389.md): and [Dāviḏ](../../strongs/h/h1732.md) [yalad](../../strongs/h/h3205.md) more [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md).

<a name="1chronicles_14_4"></a>1Chronicles 14:4

Now these are the [shem](../../strongs/h/h8034.md) of his [yalad](../../strongs/h/h3205.md) which he had in [Yĕruwshalaim](../../strongs/h/h3389.md); [Šammûaʿ](../../strongs/h/h8051.md), and [Šôḇāḇ](../../strongs/h/h7727.md), [Nāṯān](../../strongs/h/h5416.md), and [Šᵊlōmô](../../strongs/h/h8010.md),

<a name="1chronicles_14_5"></a>1Chronicles 14:5

And [Yiḇḥār](../../strongs/h/h2984.md), and ['Ĕlîšûaʿ](../../strongs/h/h474.md), and ['Ĕlîp̄Eleṭ](../../strongs/h/h467.md),

<a name="1chronicles_14_6"></a>1Chronicles 14:6

And [Nōḡah](../../strongs/h/h5052.md), and [Nep̄eḡ](../../strongs/h/h5298.md), and [Yāp̄îaʿ](../../strongs/h/h3309.md),

<a name="1chronicles_14_7"></a>1Chronicles 14:7

And ['Ĕlîšāmāʿ](../../strongs/h/h476.md), and [BᵊʿElyāḏāʿ](../../strongs/h/h1182.md), and ['Ĕlîp̄Eleṭ](../../strongs/h/h467.md).

<a name="1chronicles_14_8"></a>1Chronicles 14:8

And when the [Pᵊlištî](../../strongs/h/h6430.md) [shama'](../../strongs/h/h8085.md) that [Dāviḏ](../../strongs/h/h1732.md) was [māšaḥ](../../strongs/h/h4886.md) [melek](../../strongs/h/h4428.md) over all [Yisra'el](../../strongs/h/h3478.md), all the [Pᵊlištî](../../strongs/h/h6430.md) [ʿālâ](../../strongs/h/h5927.md) to [bāqaš](../../strongs/h/h1245.md) [Dāviḏ](../../strongs/h/h1732.md). And [Dāviḏ](../../strongs/h/h1732.md) [shama'](../../strongs/h/h8085.md) of it, and [yāṣā'](../../strongs/h/h3318.md) [paniym](../../strongs/h/h6440.md) them.

<a name="1chronicles_14_9"></a>1Chronicles 14:9

And the [Pᵊlištî](../../strongs/h/h6430.md) [bow'](../../strongs/h/h935.md) and [pāšaṭ](../../strongs/h/h6584.md) themselves in the [ʿēmeq](../../strongs/h/h6010.md) of [Rᵊp̄ā'îm](../../strongs/h/h7497.md).

<a name="1chronicles_14_10"></a>1Chronicles 14:10

And [Dāviḏ](../../strongs/h/h1732.md) [sha'al](../../strongs/h/h7592.md) of ['Elohiym](../../strongs/h/h430.md), ['āmar](../../strongs/h/h559.md), Shall I [ʿālâ](../../strongs/h/h5927.md) against the [Pᵊlištî](../../strongs/h/h6430.md)? and wilt thou [nathan](../../strongs/h/h5414.md) them into mine [yad](../../strongs/h/h3027.md)? And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, [ʿālâ](../../strongs/h/h5927.md); for I will [nathan](../../strongs/h/h5414.md) them into thine [yad](../../strongs/h/h3027.md).

<a name="1chronicles_14_11"></a>1Chronicles 14:11

So they [ʿālâ](../../strongs/h/h5927.md) to [BaʿAl-Pᵊrāṣîm](../../strongs/h/h1188.md); and [Dāviḏ](../../strongs/h/h1732.md) [nakah](../../strongs/h/h5221.md) them there. Then [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) hath [pāraṣ](../../strongs/h/h6555.md) upon mine ['oyeb](../../strongs/h/h341.md) by mine [yad](../../strongs/h/h3027.md) like the [pereṣ](../../strongs/h/h6556.md) of [mayim](../../strongs/h/h4325.md): therefore they [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of that [maqowm](../../strongs/h/h4725.md) [BaʿAl-Pᵊrāṣîm](../../strongs/h/h1188.md).

<a name="1chronicles_14_12"></a>1Chronicles 14:12

And when they had ['azab](../../strongs/h/h5800.md) their ['Elohiym](../../strongs/h/h430.md) there, [Dāviḏ](../../strongs/h/h1732.md) gave an ['āmar](../../strongs/h/h559.md), and they were [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md).

<a name="1chronicles_14_13"></a>1Chronicles 14:13

And the [Pᵊlištî](../../strongs/h/h6430.md) yet again [pāšaṭ](../../strongs/h/h6584.md) themselves in the [ʿēmeq](../../strongs/h/h6010.md).

<a name="1chronicles_14_14"></a>1Chronicles 14:14

Therefore [Dāviḏ](../../strongs/h/h1732.md) [sha'al](../../strongs/h/h7592.md) again of ['Elohiym](../../strongs/h/h430.md); and ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto him, [ʿālâ](../../strongs/h/h5927.md) not ['aḥar](../../strongs/h/h310.md) them; [cabab](../../strongs/h/h5437.md) from them, and [bow'](../../strongs/h/h935.md) upon them over [môl](../../strongs/h/h4136.md) the [bāḵā'](../../strongs/h/h1057.md).

<a name="1chronicles_14_15"></a>1Chronicles 14:15

And it shall be, when thou shalt [shama'](../../strongs/h/h8085.md) a [qowl](../../strongs/h/h6963.md) of [ṣᵊʿāḏâ](../../strongs/h/h6807.md) in the [ro'sh](../../strongs/h/h7218.md) of the [bāḵā'](../../strongs/h/h1057.md), that then thou shalt [yāṣā'](../../strongs/h/h3318.md) to [milḥāmâ](../../strongs/h/h4421.md): for ['Elohiym](../../strongs/h/h430.md) is [yāṣā'](../../strongs/h/h3318.md) [paniym](../../strongs/h/h6440.md) thee to [nakah](../../strongs/h/h5221.md) the [maḥănê](../../strongs/h/h4264.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1chronicles_14_16"></a>1Chronicles 14:16

[Dāviḏ](../../strongs/h/h1732.md) therefore ['asah](../../strongs/h/h6213.md) as ['Elohiym](../../strongs/h/h430.md) [tsavah](../../strongs/h/h6680.md) him: and they [nakah](../../strongs/h/h5221.md) the [maḥănê](../../strongs/h/h4264.md) of the [Pᵊlištî](../../strongs/h/h6430.md) from [Giḇʿôn](../../strongs/h/h1391.md) even to [Gezer](../../strongs/h/h1507.md).

<a name="1chronicles_14_17"></a>1Chronicles 14:17

And the [shem](../../strongs/h/h8034.md) of [Dāviḏ](../../strongs/h/h1732.md) [yāṣā'](../../strongs/h/h3318.md) into all ['erets](../../strongs/h/h776.md); and [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) the [paḥaḏ](../../strongs/h/h6343.md) of him upon all [gowy](../../strongs/h/h1471.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 13](1chronicles_13.md) - [1Chronicles 15](1chronicles_15.md)