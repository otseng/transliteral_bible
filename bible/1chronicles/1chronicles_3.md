# [1Chronicles 3](https://www.blueletterbible.org/kjv/1chronicles/3)

<a name="1chronicles_3_1"></a>1Chronicles 3:1

Now these were the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md), which were [yalad](../../strongs/h/h3205.md) unto him in [Ḥeḇrôn](../../strongs/h/h2275.md); the [bᵊḵôr](../../strongs/h/h1060.md) ['Amnôn](../../strongs/h/h550.md), of ['ĂḥînōʿAm](../../strongs/h/h293.md) the [Yizrᵊʿē'lîṯ](../../strongs/h/h3159.md); the second [Dinîyē'L](../../strongs/h/h1840.md), of ['Ăḇîḡayil](../../strongs/h/h26.md) the [Karmᵊlîṯ](../../strongs/h/h3762.md):

<a name="1chronicles_3_2"></a>1Chronicles 3:2

The third, ['Ăbyšālôm](../../strongs/h/h53.md) the [ben](../../strongs/h/h1121.md) of [Maʿăḵâ](../../strongs/h/h4601.md) the [bath](../../strongs/h/h1323.md) of [Talmay](../../strongs/h/h8526.md) [melek](../../strongs/h/h4428.md) of [Gᵊšûr](../../strongs/h/h1650.md): the fourth, ['Ăḏōnîyâ](../../strongs/h/h138.md) the [ben](../../strongs/h/h1121.md) of [Ḥagîṯ](../../strongs/h/h2294.md):

<a name="1chronicles_3_3"></a>1Chronicles 3:3

The fifth, [Šᵊp̄Aṭyâ](../../strongs/h/h8203.md) of ['Ăḇîṭal](../../strongs/h/h37.md): the sixth, [YiṯrᵊʿĀm](../../strongs/h/h3507.md) by [ʿEḡlâ](../../strongs/h/h5698.md) his ['ishshah](../../strongs/h/h802.md).

<a name="1chronicles_3_4"></a>1Chronicles 3:4

These six were [yalad](../../strongs/h/h3205.md) unto him in [Ḥeḇrôn](../../strongs/h/h2275.md); and there he [mālaḵ](../../strongs/h/h4427.md) seven [šānâ](../../strongs/h/h8141.md) and six [ḥōḏeš](../../strongs/h/h2320.md): and in [Yĕruwshalaim](../../strongs/h/h3389.md) he [mālaḵ](../../strongs/h/h4427.md) thirty and three [šānâ](../../strongs/h/h8141.md).

<a name="1chronicles_3_5"></a>1Chronicles 3:5

And these were [yalad](../../strongs/h/h3205.md) unto him in [Yĕruwshalaim](../../strongs/h/h3389.md); [ŠimʿĀ'](../../strongs/h/h8092.md), and [Šôḇāḇ](../../strongs/h/h7727.md), and [Nāṯān](../../strongs/h/h5416.md), and [Šᵊlōmô](../../strongs/h/h8010.md), four, of [Baṯ-Šeûaʿ](../../strongs/h/h1340.md) the [bath](../../strongs/h/h1323.md) of [ʿAmmî'ēl](../../strongs/h/h5988.md):

<a name="1chronicles_3_6"></a>1Chronicles 3:6

[Yiḇḥār](../../strongs/h/h2984.md) also, and ['Ĕlîšāmāʿ](../../strongs/h/h476.md), and ['Ĕlîp̄Eleṭ](../../strongs/h/h467.md),

<a name="1chronicles_3_7"></a>1Chronicles 3:7

And [Nōḡah](../../strongs/h/h5052.md), and [Nep̄eḡ](../../strongs/h/h5298.md), and [Yāp̄îaʿ](../../strongs/h/h3309.md),

<a name="1chronicles_3_8"></a>1Chronicles 3:8

And ['Ĕlîšāmāʿ](../../strongs/h/h476.md), and ['Elyāḏāʿ](../../strongs/h/h450.md), and ['Ĕlîp̄Eleṭ](../../strongs/h/h467.md), nine.

<a name="1chronicles_3_9"></a>1Chronicles 3:9

These were all the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md), beside the [ben](../../strongs/h/h1121.md) of the [pîleḡeš](../../strongs/h/h6370.md), and [Tāmār](../../strongs/h/h8559.md) their ['āḥôṯ](../../strongs/h/h269.md).

<a name="1chronicles_3_10"></a>1Chronicles 3:10

And [Šᵊlōmô](../../strongs/h/h8010.md) [ben](../../strongs/h/h1121.md) was [RᵊḥaḇʿĀm](../../strongs/h/h7346.md), ['Ăḇîâ](../../strongs/h/h29.md) his [ben](../../strongs/h/h1121.md), ['Āsā'](../../strongs/h/h609.md) his [ben](../../strongs/h/h1121.md), [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_3_11"></a>1Chronicles 3:11

[Yôrām](../../strongs/h/h3141.md) his [ben](../../strongs/h/h1121.md), ['Ăḥazyâ](../../strongs/h/h274.md) his [ben](../../strongs/h/h1121.md), [Yô'Āš](../../strongs/h/h3101.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_3_12"></a>1Chronicles 3:12

['Ămaṣyâ](../../strongs/h/h558.md) his [ben](../../strongs/h/h1121.md), [ʿĂzaryâ](../../strongs/h/h5838.md) his [ben](../../strongs/h/h1121.md), [Yôṯām](../../strongs/h/h3147.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_3_13"></a>1Chronicles 3:13

['Āḥāz](../../strongs/h/h271.md) his [ben](../../strongs/h/h1121.md), [Ḥizqîyâ](../../strongs/h/h2396.md) his [ben](../../strongs/h/h1121.md), [Mᵊnaššê](../../strongs/h/h4519.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_3_14"></a>1Chronicles 3:14

['Āmôn](../../strongs/h/h526.md) his [ben](../../strongs/h/h1121.md), [Yō'Šîyâ](../../strongs/h/h2977.md) his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_3_15"></a>1Chronicles 3:15

And the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) were, the [bᵊḵôr](../../strongs/h/h1060.md) [Yôḥānān](../../strongs/h/h3110.md), the second [Yᵊhôyāqîm](../../strongs/h/h3079.md), the third [Ṣḏqyh](../../strongs/h/h6667.md), the fourth [Šallûm](../../strongs/h/h7967.md).

<a name="1chronicles_3_16"></a>1Chronicles 3:16

And the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md): [Yᵊḵānyâ](../../strongs/h/h3204.md) his [ben](../../strongs/h/h1121.md), [Ṣḏqyh](../../strongs/h/h6667.md) his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_3_17"></a>1Chronicles 3:17

And the [ben](../../strongs/h/h1121.md) of [Yᵊḵānyâ](../../strongs/h/h3204.md); ['Assîr](../../strongs/h/h617.md), [Šᵊ'Altî'Ēl](../../strongs/h/h7597.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_3_18"></a>1Chronicles 3:18

[Malkîrām](../../strongs/h/h4443.md) also, and [Pᵊḏāyâ](../../strongs/h/h6305.md), and [Šen'Aṣṣar](../../strongs/h/h8137.md), [Yᵊqamyâ](../../strongs/h/h3359.md), [Hôšāmāʿ](../../strongs/h/h1953.md), and [Nᵊḏaḇyâ](../../strongs/h/h5072.md).

<a name="1chronicles_3_19"></a>1Chronicles 3:19

And the [ben](../../strongs/h/h1121.md) of [Pᵊḏāyâ](../../strongs/h/h6305.md) were, [Zᵊrubāḇel](../../strongs/h/h2216.md), and [Šimʿî](../../strongs/h/h8096.md): and the [ben](../../strongs/h/h1121.md) of [Zᵊrubāḇel](../../strongs/h/h2216.md); [Mᵊšullām](../../strongs/h/h4918.md), and [Ḥănanyâ](../../strongs/h/h2608.md), and [Šᵊlōmîṯ](../../strongs/h/h8019.md) their ['āḥôṯ](../../strongs/h/h269.md):

<a name="1chronicles_3_20"></a>1Chronicles 3:20

And [Ḥăšuḇâ](../../strongs/h/h2807.md), and ['Ōhel](../../strongs/h/h169.md), and [Bereḵyâ](../../strongs/h/h1296.md), and [Ḥăsaḏyâ](../../strongs/h/h2619.md), [Yûšaḇ Ḥeseḏ](../../strongs/h/h3142.md), five.

<a name="1chronicles_3_21"></a>1Chronicles 3:21

And the [ben](../../strongs/h/h1121.md) of [Ḥănanyâ](../../strongs/h/h2608.md); [Pᵊlaṭyâ](../../strongs/h/h6410.md), and [Yᵊšaʿyâ](../../strongs/h/h3470.md): the [ben](../../strongs/h/h1121.md) of [Rᵊp̄Āyâ](../../strongs/h/h7509.md), the [ben](../../strongs/h/h1121.md) of ['Arnān](../../strongs/h/h770.md), the [ben](../../strongs/h/h1121.md) of [ʿŌḇaḏyâ](../../strongs/h/h5662.md), the [ben](../../strongs/h/h1121.md) of [Šᵊḵanyâ](../../strongs/h/h7935.md).

<a name="1chronicles_3_22"></a>1Chronicles 3:22

And the [ben](../../strongs/h/h1121.md) of [Šᵊḵanyâ](../../strongs/h/h7935.md); [ŠᵊmaʿYâ](../../strongs/h/h8098.md): and the [ben](../../strongs/h/h1121.md) of [ŠᵊmaʿYâ](../../strongs/h/h8098.md); [Ḥaṭṭûš](../../strongs/h/h2407.md), and [Yiḡ'āl](../../strongs/h/h3008.md), and [Bārîaḥ](../../strongs/h/h1282.md), and [NᵊʿAryâ](../../strongs/h/h5294.md), and [Šāp̄āṭ](../../strongs/h/h8202.md), six.

<a name="1chronicles_3_23"></a>1Chronicles 3:23

And the [ben](../../strongs/h/h1121.md) of [NᵊʿAryâ](../../strongs/h/h5294.md); ['ElyᵊhôʿÊnay](../../strongs/h/h454.md), and [Ḥizqîyâ](../../strongs/h/h2396.md), and [ʿAzrîqām](../../strongs/h/h5840.md), three.

<a name="1chronicles_3_24"></a>1Chronicles 3:24

And the [ben](../../strongs/h/h1121.md) of ['ElyᵊhôʿÊnay](../../strongs/h/h454.md) were, [Hôḏavyâû](../../strongs/h/h1939.md), and ['Elyāšîḇ](../../strongs/h/h475.md), and [Pᵊlāyâ](../../strongs/h/h6411.md), and [ʿAqqûḇ](../../strongs/h/h6126.md), and [Yôḥānān](../../strongs/h/h3110.md), and [Dᵊlāyâ](../../strongs/h/h1806.md), and [ʿĂnānî](../../strongs/h/h6054.md), seven.

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 2](1chronicles_2.md) - [1Chronicles 4](1chronicles_4.md)