# [1Chronicles 2](https://www.blueletterbible.org/kjv/1chronicles/2)

<a name="1chronicles_2_1"></a>1Chronicles 2:1

These are the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); [Rᵊ'ûḇēn](../../strongs/h/h7205.md), [Šimʿôn](../../strongs/h/h8095.md), [Lēvî](../../strongs/h/h3878.md), and [Yehuwdah](../../strongs/h/h3063.md), [Yiśśāśḵār](../../strongs/h/h3485.md), and [Zᵊḇûlûn](../../strongs/h/h2074.md),

<a name="1chronicles_2_2"></a>1Chronicles 2:2

[Dān](../../strongs/h/h1835.md), [Yôsēp̄](../../strongs/h/h3130.md), and [Binyāmîn](../../strongs/h/h1144.md), [Nap̄tālî](../../strongs/h/h5321.md), [Gāḏ](../../strongs/h/h1410.md), and ['Āšēr](../../strongs/h/h836.md).

<a name="1chronicles_2_3"></a>1Chronicles 2:3

The [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md); [ʿĒr](../../strongs/h/h6147.md), and ['Ônān](../../strongs/h/h209.md), and [Šēlâ](../../strongs/h/h7956.md): which three were [yalad](../../strongs/h/h3205.md) unto him of the [bath](../../strongs/h/h1323.md) of [ŠûʿĀ'](../../strongs/h/h7774.md) [Baṯ-Šeûaʿ](../../strongs/h/h1340.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md). And [ʿĒr](../../strongs/h/h6147.md), the [bᵊḵôr](../../strongs/h/h1060.md) of [Yehuwdah](../../strongs/h/h3063.md), was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md); and he [muwth](../../strongs/h/h4191.md) him.

<a name="1chronicles_2_4"></a>1Chronicles 2:4

And [Tāmār](../../strongs/h/h8559.md) his [kallâ](../../strongs/h/h3618.md) [yalad](../../strongs/h/h3205.md) him [Pereṣ](../../strongs/h/h6557.md) and [Zeraḥ](../../strongs/h/h2226.md). All the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) were five.

<a name="1chronicles_2_5"></a>1Chronicles 2:5

The [ben](../../strongs/h/h1121.md) of [Pereṣ](../../strongs/h/h6557.md); [Ḥeṣrôn](../../strongs/h/h2696.md), and [Ḥāmûl](../../strongs/h/h2538.md).

<a name="1chronicles_2_6"></a>1Chronicles 2:6

And the [ben](../../strongs/h/h1121.md) of [Zeraḥ](../../strongs/h/h2226.md); [Zimrî](../../strongs/h/h2174.md), and ['Êṯān](../../strongs/h/h387.md), and [Hêmān](../../strongs/h/h1968.md), and [Kalkōl](../../strongs/h/h3633.md), and [Deraʿ](../../strongs/h/h1873.md): five of them in all.

<a name="1chronicles_2_7"></a>1Chronicles 2:7

And the [ben](../../strongs/h/h1121.md) of [Karmî](../../strongs/h/h3756.md); [ʿĀḵār](../../strongs/h/h5917.md), the [ʿāḵar](../../strongs/h/h5916.md) of [Yisra'el](../../strongs/h/h3478.md), who [māʿal](../../strongs/h/h4603.md) in the thing [ḥērem](../../strongs/h/h2764.md).

<a name="1chronicles_2_8"></a>1Chronicles 2:8

And the [ben](../../strongs/h/h1121.md) of ['Êṯān](../../strongs/h/h387.md); [ʿĂzaryâ](../../strongs/h/h5838.md).

<a name="1chronicles_2_9"></a>1Chronicles 2:9

The [ben](../../strongs/h/h1121.md) also of [Ḥeṣrôn](../../strongs/h/h2696.md), that were [yalad](../../strongs/h/h3205.md) unto him; [Yᵊraḥmᵊ'Ēl](../../strongs/h/h3396.md), and [Rām](../../strongs/h/h7410.md), and [Kᵊlûḇay](../../strongs/h/h3621.md).

<a name="1chronicles_2_10"></a>1Chronicles 2:10

And [Rām](../../strongs/h/h7410.md) [yalad](../../strongs/h/h3205.md) [ʿAmmînāḏāḇ](../../strongs/h/h5992.md); and [ʿAmmînāḏāḇ](../../strongs/h/h5992.md) [yalad](../../strongs/h/h3205.md) [Naḥšôn](../../strongs/h/h5177.md), [nāśî'](../../strongs/h/h5387.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md);

<a name="1chronicles_2_11"></a>1Chronicles 2:11

And [Naḥšôn](../../strongs/h/h5177.md) [yalad](../../strongs/h/h3205.md) [Śalmā'](../../strongs/h/h8007.md), and [Śalmā'](../../strongs/h/h8007.md) [yalad](../../strongs/h/h3205.md) [BōʿAz](../../strongs/h/h1162.md),

<a name="1chronicles_2_12"></a>1Chronicles 2:12

And [BōʿAz](../../strongs/h/h1162.md) [yalad](../../strongs/h/h3205.md) [ʿÔḇēḏ](../../strongs/h/h5744.md), and [ʿÔḇēḏ](../../strongs/h/h5744.md) [yalad](../../strongs/h/h3205.md) [Yišay](../../strongs/h/h3448.md),

<a name="1chronicles_2_13"></a>1Chronicles 2:13

And [Yišay](../../strongs/h/h3448.md) [yalad](../../strongs/h/h3205.md) his [bᵊḵôr](../../strongs/h/h1060.md) ['Ĕlî'āḇ](../../strongs/h/h446.md), and ['Ăḇînāḏāḇ](../../strongs/h/h41.md) the second, and [ŠimʿĀ'](../../strongs/h/h8092.md) the third,

<a name="1chronicles_2_14"></a>1Chronicles 2:14

[Nᵊṯan'ēl](../../strongs/h/h5417.md) the fourth, [Raday](../../strongs/h/h7288.md) the fifth,

<a name="1chronicles_2_15"></a>1Chronicles 2:15

['Ōṣem](../../strongs/h/h684.md) the sixth, [Dāviḏ](../../strongs/h/h1732.md) the seventh:

<a name="1chronicles_2_16"></a>1Chronicles 2:16

Whose ['āḥôṯ](../../strongs/h/h269.md) were [Ṣᵊrûyâ](../../strongs/h/h6870.md), and ['Ăḇîḡayil](../../strongs/h/h26.md). And the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md); ['Ăḇîšay](../../strongs/h/h52.md), and [Yô'āḇ](../../strongs/h/h3097.md), and [ʿĂśâ'Ēl](../../strongs/h/h6214.md), three.

<a name="1chronicles_2_17"></a>1Chronicles 2:17

And ['Ăḇîḡayil](../../strongs/h/h26.md) [yalad](../../strongs/h/h3205.md) [ʿĂmāśā'](../../strongs/h/h6021.md): and the ['ab](../../strongs/h/h1.md) of [ʿĂmāśā'](../../strongs/h/h6021.md) was [Yeṯer](../../strongs/h/h3500.md) the [Yišmᵊʿē'lî](../../strongs/h/h3459.md).

<a name="1chronicles_2_18"></a>1Chronicles 2:18

And [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Ḥeṣrôn](../../strongs/h/h2696.md) [yalad](../../strongs/h/h3205.md) children of [ʿĂzûḇâ](../../strongs/h/h5806.md) his ['ishshah](../../strongs/h/h802.md), and of [YᵊrîʿÔṯ](../../strongs/h/h3408.md): her [ben](../../strongs/h/h1121.md) are these; [Yēšer](../../strongs/h/h3475.md), and [Šôḇāḇ](../../strongs/h/h7727.md), and ['Ardôn](../../strongs/h/h715.md).

<a name="1chronicles_2_19"></a>1Chronicles 2:19

And when [ʿĂzûḇâ](../../strongs/h/h5806.md) was [muwth](../../strongs/h/h4191.md), [Kālēḇ](../../strongs/h/h3612.md) [laqach](../../strongs/h/h3947.md) unto him ['Ep̄rāṯ](../../strongs/h/h672.md), which [yalad](../../strongs/h/h3205.md) him [Ḥûr](../../strongs/h/h2354.md).

<a name="1chronicles_2_20"></a>1Chronicles 2:20

And [Ḥûr](../../strongs/h/h2354.md) [yalad](../../strongs/h/h3205.md) ['Ûrî](../../strongs/h/h221.md), and ['Ûrî](../../strongs/h/h221.md) [yalad](../../strongs/h/h3205.md) [Bᵊṣal'ēl](../../strongs/h/h1212.md).

<a name="1chronicles_2_21"></a>1Chronicles 2:21

And ['aḥar](../../strongs/h/h310.md) [Ḥeṣrôn](../../strongs/h/h2696.md) [bow'](../../strongs/h/h935.md) to the [bath](../../strongs/h/h1323.md) of [Māḵîr](../../strongs/h/h4353.md) the ['ab](../../strongs/h/h1.md) of [Gilʿāḏ](../../strongs/h/h1568.md), whom he [laqach](../../strongs/h/h3947.md) when he was threescore [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md); and she [yalad](../../strongs/h/h3205.md) him [Śᵊḡûḇ](../../strongs/h/h7687.md).

<a name="1chronicles_2_22"></a>1Chronicles 2:22

And [Śᵊḡûḇ](../../strongs/h/h7687.md) [yalad](../../strongs/h/h3205.md) [Yā'Îr](../../strongs/h/h2971.md), who had three and twenty [ʿîr](../../strongs/h/h5892.md) in the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="1chronicles_2_23"></a>1Chronicles 2:23

And he [laqach](../../strongs/h/h3947.md) [Gᵊšûr](../../strongs/h/h1650.md), and ['Ărām](../../strongs/h/h758.md), with the [ḥaûâ](../../strongs/h/h2333.md) of [Yā'Îr](../../strongs/h/h2971.md), from them, with [Qᵊnāṯ](../../strongs/h/h7079.md), and the [bath](../../strongs/h/h1323.md) thereof, even threescore [ʿîr](../../strongs/h/h5892.md). All these belonged to the [ben](../../strongs/h/h1121.md) of [Māḵîr](../../strongs/h/h4353.md) the ['ab](../../strongs/h/h1.md) of [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="1chronicles_2_24"></a>1Chronicles 2:24

And ['aḥar](../../strongs/h/h310.md) that [Ḥeṣrôn](../../strongs/h/h2696.md) was [maveth](../../strongs/h/h4194.md) in [Kālēḇ 'Ep̄Rāṯâ](../../strongs/h/h3613.md), then ['Ăḇîâ](../../strongs/h/h29.md) [Ḥeṣrôn](../../strongs/h/h2696.md) ['ishshah](../../strongs/h/h802.md) [yalad](../../strongs/h/h3205.md) him ['Ašḥûr](../../strongs/h/h806.md) the ['ab](../../strongs/h/h1.md) of [Tᵊqôaʿ](../../strongs/h/h8620.md).

<a name="1chronicles_2_25"></a>1Chronicles 2:25

And the [ben](../../strongs/h/h1121.md) of [Yᵊraḥmᵊ'Ēl](../../strongs/h/h3396.md) the [bᵊḵôr](../../strongs/h/h1060.md) of [Ḥeṣrôn](../../strongs/h/h2696.md) were, [Rām](../../strongs/h/h7410.md) the [bᵊḵôr](../../strongs/h/h1060.md), and [Bûnâ](../../strongs/h/h946.md), and ['Ōren](../../strongs/h/h767.md), and ['Ōṣem](../../strongs/h/h684.md), and ['Ăḥîyâ](../../strongs/h/h281.md).

<a name="1chronicles_2_26"></a>1Chronicles 2:26

[Yᵊraḥmᵊ'Ēl](../../strongs/h/h3396.md) had also ['aḥēr](../../strongs/h/h312.md) ['ishshah](../../strongs/h/h802.md), whose [shem](../../strongs/h/h8034.md) was [ʿĂṭārâ](../../strongs/h/h5851.md); she was the ['em](../../strongs/h/h517.md) of ['Ônām](../../strongs/h/h208.md).

<a name="1chronicles_2_27"></a>1Chronicles 2:27

And the [ben](../../strongs/h/h1121.md) of [Rām](../../strongs/h/h7410.md) the [bᵊḵôr](../../strongs/h/h1060.md) of [Yᵊraḥmᵊ'Ēl](../../strongs/h/h3396.md) were, [MaʿAṣ](../../strongs/h/h4619.md), and [yāmîn](../../strongs/h/h3226.md), and [ʿĒqer](../../strongs/h/h6134.md).

<a name="1chronicles_2_28"></a>1Chronicles 2:28

And the [ben](../../strongs/h/h1121.md) of ['Ônām](../../strongs/h/h208.md) were, [Šammay](../../strongs/h/h8060.md), and [Yāḏāʿ](../../strongs/h/h3047.md). And the [ben](../../strongs/h/h1121.md) of [Šammay](../../strongs/h/h8060.md); [Nāḏāḇ](../../strongs/h/h5070.md), and ['Ŏḇîšûr](../../strongs/h/h51.md).

<a name="1chronicles_2_29"></a>1Chronicles 2:29

And the [shem](../../strongs/h/h8034.md) of the ['ishshah](../../strongs/h/h802.md) of ['Ŏḇîšûr](../../strongs/h/h51.md) was ['Ăḇîhayil](../../strongs/h/h32.md), and she [yalad](../../strongs/h/h3205.md) him ['Aḥbān](../../strongs/h/h257.md), and [Môlîḏ](../../strongs/h/h4140.md).

<a name="1chronicles_2_30"></a>1Chronicles 2:30

And the [ben](../../strongs/h/h1121.md) of [Nāḏāḇ](../../strongs/h/h5070.md); [Seleḏ](../../strongs/h/h5540.md), and ['Apayim](../../strongs/h/h649.md): but [Seleḏ](../../strongs/h/h5540.md) [muwth](../../strongs/h/h4191.md) without [ben](../../strongs/h/h1121.md).

<a name="1chronicles_2_31"></a>1Chronicles 2:31

And the [ben](../../strongs/h/h1121.md) of ['Apayim](../../strongs/h/h649.md); [YišʿÎ](../../strongs/h/h3469.md). And the [ben](../../strongs/h/h1121.md) of [YišʿÎ](../../strongs/h/h3469.md); [Šēšān](../../strongs/h/h8348.md). And the [ben](../../strongs/h/h1121.md) of [Šēšān](../../strongs/h/h8348.md); ['Aḥlāy](../../strongs/h/h304.md).

<a name="1chronicles_2_32"></a>1Chronicles 2:32

And the [ben](../../strongs/h/h1121.md) of [Yāḏāʿ](../../strongs/h/h3047.md) the ['ach](../../strongs/h/h251.md) of [Šammay](../../strongs/h/h8060.md); [Yeṯer](../../strongs/h/h3500.md), and [Yônāṯān](../../strongs/h/h3129.md): and [Yeṯer](../../strongs/h/h3500.md) [muwth](../../strongs/h/h4191.md) without [ben](../../strongs/h/h1121.md).

<a name="1chronicles_2_33"></a>1Chronicles 2:33

And the [ben](../../strongs/h/h1121.md) of [Yônāṯān](../../strongs/h/h3129.md); [peleṯ](../../strongs/h/h6431.md), and [Zāzā'](../../strongs/h/h2117.md). These were the [ben](../../strongs/h/h1121.md) of [Yᵊraḥmᵊ'Ēl](../../strongs/h/h3396.md).

<a name="1chronicles_2_34"></a>1Chronicles 2:34

Now [Šēšān](../../strongs/h/h8348.md) had no [ben](../../strongs/h/h1121.md), but [bath](../../strongs/h/h1323.md). And [Šēšān](../../strongs/h/h8348.md) had an ['ebed](../../strongs/h/h5650.md), an [Miṣrî](../../strongs/h/h4713.md), whose [shem](../../strongs/h/h8034.md) was [Yarḥāʿ](../../strongs/h/h3398.md).

<a name="1chronicles_2_35"></a>1Chronicles 2:35

And [Šēšān](../../strongs/h/h8348.md) [nathan](../../strongs/h/h5414.md) his [bath](../../strongs/h/h1323.md) to [Yarḥāʿ](../../strongs/h/h3398.md) his ['ebed](../../strongs/h/h5650.md) to ['ishshah](../../strongs/h/h802.md); and she [yalad](../../strongs/h/h3205.md) him [ʿAtay](../../strongs/h/h6262.md).

<a name="1chronicles_2_36"></a>1Chronicles 2:36

And [ʿAtay](../../strongs/h/h6262.md) [yalad](../../strongs/h/h3205.md) [Nāṯān](../../strongs/h/h5416.md), and [Nāṯān](../../strongs/h/h5416.md) [yalad](../../strongs/h/h3205.md) [Zāḇāḏ](../../strongs/h/h2066.md),

<a name="1chronicles_2_37"></a>1Chronicles 2:37

And [Zāḇāḏ](../../strongs/h/h2066.md) [yalad](../../strongs/h/h3205.md) ['Ep̄Lāl](../../strongs/h/h654.md), and ['Ep̄Lāl](../../strongs/h/h654.md) [yalad](../../strongs/h/h3205.md) [ʿÔḇēḏ](../../strongs/h/h5744.md),

<a name="1chronicles_2_38"></a>1Chronicles 2:38

And [ʿÔḇēḏ](../../strongs/h/h5744.md) [yalad](../../strongs/h/h3205.md) [Yêû'](../../strongs/h/h3058.md), and [Yêû'](../../strongs/h/h3058.md) [yalad](../../strongs/h/h3205.md) [ʿĂzaryâ](../../strongs/h/h5838.md),

<a name="1chronicles_2_39"></a>1Chronicles 2:39

And [ʿĂzaryâ](../../strongs/h/h5838.md) [yalad](../../strongs/h/h3205.md) [Ḥeleṣ](../../strongs/h/h2503.md), and [Ḥeleṣ](../../strongs/h/h2503.md) [yalad](../../strongs/h/h3205.md) ['ElʿĀśâ](../../strongs/h/h501.md),

<a name="1chronicles_2_40"></a>1Chronicles 2:40

And ['ElʿĀśâ](../../strongs/h/h501.md) [yalad](../../strongs/h/h3205.md) [Sismay](../../strongs/h/h5581.md), and [Sismay](../../strongs/h/h5581.md) [yalad](../../strongs/h/h3205.md) [Šallûm](../../strongs/h/h7967.md),

<a name="1chronicles_2_41"></a>1Chronicles 2:41

And [Šallûm](../../strongs/h/h7967.md) [yalad](../../strongs/h/h3205.md) [Yᵊqamyâ](../../strongs/h/h3359.md), and [Yᵊqamyâ](../../strongs/h/h3359.md) [yalad](../../strongs/h/h3205.md) ['Ĕlîšāmāʿ](../../strongs/h/h476.md).

<a name="1chronicles_2_42"></a>1Chronicles 2:42

Now the [ben](../../strongs/h/h1121.md) of [Kālēḇ](../../strongs/h/h3612.md) the ['ach](../../strongs/h/h251.md) of [Yᵊraḥmᵊ'Ēl](../../strongs/h/h3396.md) were, [Mêšāʿ](../../strongs/h/h4337.md) his [bᵊḵôr](../../strongs/h/h1060.md), which was the ['ab](../../strongs/h/h1.md) of [Zîp̄](../../strongs/h/h2128.md); and the [ben](../../strongs/h/h1121.md) of [Mārē'Šâ](../../strongs/h/h4762.md) the ['ab](../../strongs/h/h1.md) of [Ḥeḇrôn](../../strongs/h/h2275.md).

<a name="1chronicles_2_43"></a>1Chronicles 2:43

And the [ben](../../strongs/h/h1121.md) of [Ḥeḇrôn](../../strongs/h/h2275.md); [Qōraḥ](../../strongs/h/h7141.md), and [Tapûaḥ](../../strongs/h/h8599.md), and [Reqem](../../strongs/h/h7552.md), and [Šemaʿ](../../strongs/h/h8087.md).

<a name="1chronicles_2_44"></a>1Chronicles 2:44

And [Šemaʿ](../../strongs/h/h8087.md) [yalad](../../strongs/h/h3205.md) [Raḥam](../../strongs/h/h7357.md), the ['ab](../../strongs/h/h1.md) of [YārqᵊʿĀm](../../strongs/h/h3421.md): and [Reqem](../../strongs/h/h7552.md) [yalad](../../strongs/h/h3205.md) [Šammay](../../strongs/h/h8060.md).

<a name="1chronicles_2_45"></a>1Chronicles 2:45

And the [ben](../../strongs/h/h1121.md) of [Šammay](../../strongs/h/h8060.md) was [MāʿÔn](../../strongs/h/h4584.md): and [MāʿÔn](../../strongs/h/h4584.md) was the ['ab](../../strongs/h/h1.md) of [Bêṯ-Ṣûr](../../strongs/h/h1049.md).

<a name="1chronicles_2_46"></a>1Chronicles 2:46

And [ʿÊp̄â](../../strongs/h/h5891.md), [Kālēḇ](../../strongs/h/h3612.md) [pîleḡeš](../../strongs/h/h6370.md), [yalad](../../strongs/h/h3205.md) [Ḥārān](../../strongs/h/h2771.md), and [Môṣā'](../../strongs/h/h4162.md), and [Gāzēz](../../strongs/h/h1495.md): and [Ḥārān](../../strongs/h/h2771.md) [yalad](../../strongs/h/h3205.md) [Gāzēz](../../strongs/h/h1495.md).

<a name="1chronicles_2_47"></a>1Chronicles 2:47

And the [ben](../../strongs/h/h1121.md) of [Yêday](../../strongs/h/h3056.md); [Reḡem](../../strongs/h/h7276.md), and [Yôṯām](../../strongs/h/h3147.md), and [Gêšān](../../strongs/h/h1529.md), and [Peleṭ](../../strongs/h/h6404.md), and [ʿÊp̄â](../../strongs/h/h5891.md), and [ŠaʿAp̄](../../strongs/h/h8174.md).

<a name="1chronicles_2_48"></a>1Chronicles 2:48

[Maʿăḵâ](../../strongs/h/h4601.md), [Kālēḇ](../../strongs/h/h3612.md) [pîleḡeš](../../strongs/h/h6370.md), [yalad](../../strongs/h/h3205.md) [Šēḇer](../../strongs/h/h7669.md), and [Tirḥănâ](../../strongs/h/h8647.md).

<a name="1chronicles_2_49"></a>1Chronicles 2:49

She [yalad](../../strongs/h/h3205.md) also [ŠaʿAp̄](../../strongs/h/h8174.md) the ['ab](../../strongs/h/h1.md) of [Maḏmannâ](../../strongs/h/h4089.md), [Šᵊvā'](../../strongs/h/h7724.md) the ['ab](../../strongs/h/h1.md) of [Maḵbēnā'](../../strongs/h/h4343.md), and the ['ab](../../strongs/h/h1.md) of [GiḇʿĀ'](../../strongs/h/h1388.md): and the [bath](../../strongs/h/h1323.md) of [Kālēḇ](../../strongs/h/h3612.md) was [ʿAḵsâ](../../strongs/h/h5915.md).

<a name="1chronicles_2_50"></a>1Chronicles 2:50

These were the [ben](../../strongs/h/h1121.md) of [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Ḥûr](../../strongs/h/h2354.md), the [bᵊḵôr](../../strongs/h/h1060.md) of ['Ep̄rāṯ](../../strongs/h/h672.md); [Šôḇāl](../../strongs/h/h7732.md) the ['ab](../../strongs/h/h1.md) of [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md),

<a name="1chronicles_2_51"></a>1Chronicles 2:51

[Śalmā'](../../strongs/h/h8007.md) the ['ab](../../strongs/h/h1.md) of [Bêṯ leḥem](../../strongs/h/h1035.md), [Ḥārēp̄](../../strongs/h/h2780.md) the ['ab](../../strongs/h/h1.md) of [Bêṯ-Gāḏēr](../../strongs/h/h1013.md).

<a name="1chronicles_2_52"></a>1Chronicles 2:52

And [Šôḇāl](../../strongs/h/h7732.md) the ['ab](../../strongs/h/h1.md) of [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md) had [ben](../../strongs/h/h1121.md); [Rō'Ê](../../strongs/h/h7204.md), and [ḥēṣî](../../strongs/h/h2677.md) of the [Ḥăṣî hammᵊnuḥôṯ](../../strongs/h/h2679.md).

<a name="1chronicles_2_53"></a>1Chronicles 2:53

And the [mišpāḥâ](../../strongs/h/h4940.md) of [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md); the [Yiṯrî](../../strongs/h/h3505.md), and the [Pûṯî](../../strongs/h/h6336.md), and the [Šumāṯî](../../strongs/h/h8126.md), and the [Mišrāʿî](../../strongs/h/h4954.md); of them [yāṣā'](../../strongs/h/h3318.md) the [Ṣārʿî](../../strongs/h/h6882.md), and the ['Eštā'ulî](../../strongs/h/h848.md).

<a name="1chronicles_2_54"></a>1Chronicles 2:54

The [ben](../../strongs/h/h1121.md) of [Śalmā'](../../strongs/h/h8007.md); [Bêṯ leḥem](../../strongs/h/h1035.md), and the [Nᵊṭōp̄āṯî](../../strongs/h/h5200.md), [ʿĂṭārôṯ](../../strongs/h/h5852.md), the house of [ʿAṭrôṯ Bêṯ Yô'Āḇ](../../strongs/h/h5854.md), and [ḥēṣî](../../strongs/h/h2677.md) of the [Ḥăṣî hammᵊnaḥtî](../../strongs/h/h2680.md), the [Ṣārʿî](../../strongs/h/h6882.md).

<a name="1chronicles_2_55"></a>1Chronicles 2:55

And the [mišpāḥâ](../../strongs/h/h4940.md) of the [sāp̄ar](../../strongs/h/h5608.md) which [yashab](../../strongs/h/h3427.md) [yashab](../../strongs/h/h3427.md) at [YaʿBēṣ](../../strongs/h/h3258.md); the [TirʿĀṯî](../../strongs/h/h8654.md), the [ŠimʿĀṯî](../../strongs/h/h8101.md), and [Śûḵāṯî](../../strongs/h/h7756.md). These are the [Qênî](../../strongs/h/h7017.md) that [bow'](../../strongs/h/h935.md) of [Ḥămāṯ](../../strongs/h/h2574.md), the ['ab](../../strongs/h/h1.md) of the [bayith](../../strongs/h/h1004.md) of [Rēḵāḇ](../../strongs/h/h7394.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 1](1chronicles_1.md) - [1Chronicles 3](1chronicles_3.md)