# [1Chronicles 27](https://www.blueletterbible.org/kjv/1chronicles/27)

<a name="1chronicles_27_1"></a>1Chronicles 27:1

Now the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) after their [mispār](../../strongs/h/h4557.md), to wit, the [ro'sh](../../strongs/h/h7218.md) ['ab](../../strongs/h/h1.md) and [śar](../../strongs/h/h8269.md) of thousands and hundreds, and their [šāṭar](../../strongs/h/h7860.md) that [sharath](../../strongs/h/h8334.md) the [melek](../../strongs/h/h4428.md) in any [dabar](../../strongs/h/h1697.md) of the [maḥălōqeṯ](../../strongs/h/h4256.md), which [bow'](../../strongs/h/h935.md) and [yāṣā'](../../strongs/h/h3318.md) [ḥōḏeš](../../strongs/h/h2320.md) by [ḥōḏeš](../../strongs/h/h2320.md) throughout all the [ḥōḏeš](../../strongs/h/h2320.md) of the [šānâ](../../strongs/h/h8141.md), of every [maḥălōqeṯ](../../strongs/h/h4256.md) were twenty and four thousand.

<a name="1chronicles_27_2"></a>1Chronicles 27:2

Over the [ri'šôn](../../strongs/h/h7223.md) [maḥălōqeṯ](../../strongs/h/h4256.md) for the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md) was [YāšāḇʿĀm](../../strongs/h/h3434.md) the [ben](../../strongs/h/h1121.md) of [Zaḇdî'Ēl](../../strongs/h/h2068.md): and in his [maḥălōqeṯ](../../strongs/h/h4256.md) were twenty and four thousand.

<a name="1chronicles_27_3"></a>1Chronicles 27:3

Of the [ben](../../strongs/h/h1121.md) of [Pereṣ](../../strongs/h/h6557.md) was the [ro'sh](../../strongs/h/h7218.md) of all the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) for the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md).

<a name="1chronicles_27_4"></a>1Chronicles 27:4

And over the [maḥălōqeṯ](../../strongs/h/h4256.md) of the second [ḥōḏeš](../../strongs/h/h2320.md) was [Dôḏay](../../strongs/h/h1737.md) an ['Ăḥôḥî](../../strongs/h/h266.md), and of his [maḥălōqeṯ](../../strongs/h/h4256.md) was [Miqlôṯ](../../strongs/h/h4732.md) also the [nāḡîḏ](../../strongs/h/h5057.md): in his [maḥălōqeṯ](../../strongs/h/h4256.md) likewise were twenty and four thousand.

<a name="1chronicles_27_5"></a>1Chronicles 27:5

The third [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) for the third [ḥōḏeš](../../strongs/h/h2320.md) was [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md), a [ro'sh](../../strongs/h/h7218.md) [kōhēn](../../strongs/h/h3548.md): and in his [maḥălōqeṯ](../../strongs/h/h4256.md) were twenty and four thousand.

<a name="1chronicles_27_6"></a>1Chronicles 27:6

This is that [Bᵊnāyâ](../../strongs/h/h1141.md), who was [gibôr](../../strongs/h/h1368.md) among the thirty, and above the thirty: and in his [maḥălōqeṯ](../../strongs/h/h4256.md) was [ʿAmmîzāḇāḏ](../../strongs/h/h5990.md) his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_27_7"></a>1Chronicles 27:7

The fourth captain for the fourth [ḥōḏeš](../../strongs/h/h2320.md) was [ʿĂśâ'Ēl](../../strongs/h/h6214.md) the ['ach](../../strongs/h/h251.md) of [Yô'āḇ](../../strongs/h/h3097.md), and [Zᵊḇaḏyâ](../../strongs/h/h2069.md) his [ben](../../strongs/h/h1121.md) ['aḥar](../../strongs/h/h310.md) him: and in his [maḥălōqeṯ](../../strongs/h/h4256.md) were twenty and four thousand.

<a name="1chronicles_27_8"></a>1Chronicles 27:8

The fifth [śar](../../strongs/h/h8269.md) for the fifth [ḥōḏeš](../../strongs/h/h2320.md) was [Šamhûṯ](../../strongs/h/h8049.md) the [yizrāḥ](../../strongs/h/h3155.md): and in his [maḥălōqeṯ](../../strongs/h/h4256.md) were twenty and four thousand.

<a name="1chronicles_27_9"></a>1Chronicles 27:9

The sixth for the sixth [ḥōḏeš](../../strongs/h/h2320.md) was [ʿÎrā'](../../strongs/h/h5896.md) the [ben](../../strongs/h/h1121.md) of [ʿIqqēš](../../strongs/h/h6142.md) the [Tᵊqôʿî](../../strongs/h/h8621.md): and in his [maḥălōqeṯ](../../strongs/h/h4256.md) were twenty and four thousand.

<a name="1chronicles_27_10"></a>1Chronicles 27:10

The seventh for the seventh [ḥōḏeš](../../strongs/h/h2320.md) was [Ḥeleṣ](../../strongs/h/h2503.md) the [Palônî](../../strongs/h/h6397.md), of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md): and in his [maḥălōqeṯ](../../strongs/h/h4256.md) were twenty and four thousand.

<a name="1chronicles_27_11"></a>1Chronicles 27:11

The eighth for the eighth [ḥōḏeš](../../strongs/h/h2320.md) was [Sibḵay](../../strongs/h/h5444.md) the [Ḥušāṯî](../../strongs/h/h2843.md), of the [Zarḥî](../../strongs/h/h2227.md): and in his [maḥălōqeṯ](../../strongs/h/h4256.md) were twenty and four thousand.

<a name="1chronicles_27_12"></a>1Chronicles 27:12

The ninth for the ninth [ḥōḏeš](../../strongs/h/h2320.md) was ['ĂḇîʿEzer](../../strongs/h/h44.md) the [ʿAnnᵊṯōṯî](../../strongs/h/h6069.md), of the [Ben-yᵊmînî](../../strongs/h/h1145.md): and in his [maḥălōqeṯ](../../strongs/h/h4256.md) were twenty and four thousand.

<a name="1chronicles_27_13"></a>1Chronicles 27:13

The tenth for the tenth [ḥōḏeš](../../strongs/h/h2320.md) was [Mahăray](../../strongs/h/h4121.md) the [Nᵊṭōp̄āṯî](../../strongs/h/h5200.md), of the [Zarḥî](../../strongs/h/h2227.md): and in his [maḥălōqeṯ](../../strongs/h/h4256.md) were twenty and four thousand.

<a name="1chronicles_27_14"></a>1Chronicles 27:14

The eleventh for the eleventh [ḥōḏeš](../../strongs/h/h2320.md) was [Bᵊnāyâ](../../strongs/h/h1141.md) the [Pirʿāṯônî](../../strongs/h/h6553.md), of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md): and in his [maḥălōqeṯ](../../strongs/h/h4256.md) were twenty and four thousand.

<a name="1chronicles_27_15"></a>1Chronicles 27:15

The twelfth for the twelfth [ḥōḏeš](../../strongs/h/h2320.md) was [Ḥelday](../../strongs/h/h2469.md) the [Nᵊṭōp̄āṯî](../../strongs/h/h5200.md), of [ʿĀṯnî'Ēl](../../strongs/h/h6274.md): and in his [maḥălōqeṯ](../../strongs/h/h4256.md) were twenty and four thousand.

<a name="1chronicles_27_16"></a>1Chronicles 27:16

Furthermore over the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md): the [nāḡîḏ](../../strongs/h/h5057.md) of the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md) was ['Ĕlîʿezer](../../strongs/h/h461.md) the [ben](../../strongs/h/h1121.md) of [Ziḵrî](../../strongs/h/h2147.md): of the [Šimʿōnî](../../strongs/h/h8099.md), [Šᵊp̄Aṭyâ](../../strongs/h/h8203.md) the [ben](../../strongs/h/h1121.md) of [Maʿăḵâ](../../strongs/h/h4601.md):

<a name="1chronicles_27_17"></a>1Chronicles 27:17

Of the [Lᵊvî](../../strongs/h/h3881.md), [Ḥăšaḇyâ](../../strongs/h/h2811.md) the [ben](../../strongs/h/h1121.md) of [Qᵊmû'ēl](../../strongs/h/h7055.md): of the ['Ahărôn](../../strongs/h/h175.md), [Ṣāḏôq](../../strongs/h/h6659.md):

<a name="1chronicles_27_18"></a>1Chronicles 27:18

Of [Yehuwdah](../../strongs/h/h3063.md), ['Ĕlîhû](../../strongs/h/h453.md), one of the ['ach](../../strongs/h/h251.md) of [Dāviḏ](../../strongs/h/h1732.md): of [Yiśśāśḵār](../../strongs/h/h3485.md), [ʿĀmrî](../../strongs/h/h6018.md) the [ben](../../strongs/h/h1121.md) of [Mîḵā'ēl](../../strongs/h/h4317.md):

<a name="1chronicles_27_19"></a>1Chronicles 27:19

Of [Zᵊḇûlûn](../../strongs/h/h2074.md), [YišmaʿYâ](../../strongs/h/h3460.md) the [ben](../../strongs/h/h1121.md) of [ʿŌḇaḏyâ](../../strongs/h/h5662.md): of [Nap̄tālî](../../strongs/h/h5321.md), [Yᵊrêmôṯ](../../strongs/h/h3406.md) the [ben](../../strongs/h/h1121.md) of [ʿAzrî'Ēl](../../strongs/h/h5837.md):

<a name="1chronicles_27_20"></a>1Chronicles 27:20

Of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md), [Hôšēaʿ](../../strongs/h/h1954.md) the [ben](../../strongs/h/h1121.md) of [ʿĀzazyâû](../../strongs/h/h5812.md): of the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md), [Yô'Ēl](../../strongs/h/h3100.md) the [ben](../../strongs/h/h1121.md) of [Pᵊḏāyâ](../../strongs/h/h6305.md):

<a name="1chronicles_27_21"></a>1Chronicles 27:21

Of the [ḥēṣî](../../strongs/h/h2677.md) tribe of [Mᵊnaššê](../../strongs/h/h4519.md) in [Gilʿāḏ](../../strongs/h/h1568.md), [Yidô](../../strongs/h/h3035.md) the [ben](../../strongs/h/h1121.md) of [Zᵊḵaryâ](../../strongs/h/h2148.md): of [Binyāmîn](../../strongs/h/h1144.md), [YaʿĂśî'Ēl](../../strongs/h/h3300.md) the [ben](../../strongs/h/h1121.md) of ['Aḇnēr](../../strongs/h/h74.md):

<a name="1chronicles_27_22"></a>1Chronicles 27:22

Of [Dān](../../strongs/h/h1835.md), [ʿĂzar'Ēl](../../strongs/h/h5832.md) the [ben](../../strongs/h/h1121.md) of [Yᵊrōḥām](../../strongs/h/h3395.md). These were the [śar](../../strongs/h/h8269.md) of the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_27_23"></a>1Chronicles 27:23

But [Dāviḏ](../../strongs/h/h1732.md) [nasa'](../../strongs/h/h5375.md) not the [mispār](../../strongs/h/h4557.md) of them from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maṭṭâ](../../strongs/h/h4295.md): because [Yĕhovah](../../strongs/h/h3068.md) had ['āmar](../../strongs/h/h559.md) he would [rabah](../../strongs/h/h7235.md) [Yisra'el](../../strongs/h/h3478.md) like to the [kowkab](../../strongs/h/h3556.md) of the [shamayim](../../strongs/h/h8064.md).

<a name="1chronicles_27_24"></a>1Chronicles 27:24

[Yô'āḇ](../../strongs/h/h3097.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md) [ḥālal](../../strongs/h/h2490.md) to [mānâ](../../strongs/h/h4487.md), but he [kalah](../../strongs/h/h3615.md) not, because there fell [qeṣep̄](../../strongs/h/h7110.md) for it against [Yisra'el](../../strongs/h/h3478.md); neither was the [mispār](../../strongs/h/h4557.md) [ʿālâ](../../strongs/h/h5927.md) in the [mispār](../../strongs/h/h4557.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="1chronicles_27_25"></a>1Chronicles 27:25

And over the [melek](../../strongs/h/h4428.md) ['ôṣār](../../strongs/h/h214.md) was [ʿAzmāveṯ](../../strongs/h/h5820.md) the [ben](../../strongs/h/h1121.md) of [ʿĂḏî'Ēl](../../strongs/h/h5717.md): and over the ['ôṣār](../../strongs/h/h214.md) in the [sadeh](../../strongs/h/h7704.md), in the [ʿîr](../../strongs/h/h5892.md), and in the [kāp̄ār](../../strongs/h/h3723.md), and in the [miḡdāl](../../strongs/h/h4026.md), was [Yᵊhônāṯān](../../strongs/h/h3083.md) the [ben](../../strongs/h/h1121.md) of ['Uzziyah](../../strongs/h/h5818.md):

<a name="1chronicles_27_26"></a>1Chronicles 27:26

And over them that ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md) of the [sadeh](../../strongs/h/h7704.md) for [ʿăḇōḏâ](../../strongs/h/h5656.md) of the ['ăḏāmâ](../../strongs/h/h127.md) was [ʿEzrî](../../strongs/h/h5836.md) the [ben](../../strongs/h/h1121.md) of [Kᵊlûḇ](../../strongs/h/h3620.md):

<a name="1chronicles_27_27"></a>1Chronicles 27:27

And over the [kerem](../../strongs/h/h3754.md) was [Šimʿî](../../strongs/h/h8096.md) the [rāmāṯî](../../strongs/h/h7435.md): over the increase of the [kerem](../../strongs/h/h3754.md) for the [yayin](../../strongs/h/h3196.md) ['ôṣār](../../strongs/h/h214.md) was [Zaḇdî](../../strongs/h/h2067.md) the [šip̄mî](../../strongs/h/h8225.md):

<a name="1chronicles_27_28"></a>1Chronicles 27:28

And over the [zayiṯ](../../strongs/h/h2132.md) and the [šiqmâ](../../strongs/h/h8256.md) that were in the [šᵊp̄ēlâ](../../strongs/h/h8219.md) was [Baʿal ḥānān](../../strongs/h/h1177.md) the [gᵊḏērî](../../strongs/h/h1451.md): and over the ['ôṣār](../../strongs/h/h214.md) of [šemen](../../strongs/h/h8081.md) was [YôʿĀš](../../strongs/h/h3135.md):

<a name="1chronicles_27_29"></a>1Chronicles 27:29

And over the [bāqār](../../strongs/h/h1241.md) that [ra'ah](../../strongs/h/h7462.md) in [Šārôn](../../strongs/h/h8289.md) was [Šiṭray](../../strongs/h/h7861.md) the [šārônî](../../strongs/h/h8290.md): and over the [bāqār](../../strongs/h/h1241.md) that were in the [ʿēmeq](../../strongs/h/h6010.md) was [Šāp̄āṭ](../../strongs/h/h8202.md) the [ben](../../strongs/h/h1121.md) of [ʿAḏlay](../../strongs/h/h5724.md):

<a name="1chronicles_27_30"></a>1Chronicles 27:30

Over the [gāmāl](../../strongs/h/h1581.md) also was ['Ôḇîl](../../strongs/h/h179.md) the [Yišmᵊʿē'lî](../../strongs/h/h3459.md): and over the ['āṯôn](../../strongs/h/h860.md) was [Yeḥdᵊyâû](../../strongs/h/h3165.md) the [mērōnōṯî](../../strongs/h/h4824.md):

<a name="1chronicles_27_31"></a>1Chronicles 27:31

And over the [tso'n](../../strongs/h/h6629.md) was [Yāzîz](../../strongs/h/h3151.md) the [Haḡrî](../../strongs/h/h1905.md). All these were the [śar](../../strongs/h/h8269.md) of the [rᵊḵûš](../../strongs/h/h7399.md) which was [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="1chronicles_27_32"></a>1Chronicles 27:32

Also [Yᵊhônāṯān](../../strongs/h/h3083.md) [Dāviḏ](../../strongs/h/h1732.md) [dôḏ](../../strongs/h/h1730.md) was a [ya'ats](../../strongs/h/h3289.md), a [bîn](../../strongs/h/h995.md) ['iysh](../../strongs/h/h376.md), and a [sāp̄ar](../../strongs/h/h5608.md): and [Yᵊḥî'Ēl](../../strongs/h/h3171.md) the [ben](../../strongs/h/h1121.md) of [Ḥaḵmōnî](../../strongs/h/h2453.md) was with the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md):

<a name="1chronicles_27_33"></a>1Chronicles 27:33

And ['Ăḥîṯōp̄El](../../strongs/h/h302.md) was the [melek](../../strongs/h/h4428.md) [ya'ats](../../strongs/h/h3289.md): and [Ḥûšay](../../strongs/h/h2365.md) the ['arkî](../../strongs/h/h757.md) was the [melek](../../strongs/h/h4428.md) [rea'](../../strongs/h/h7453.md):

<a name="1chronicles_27_34"></a>1Chronicles 27:34

And ['aḥar](../../strongs/h/h310.md) ['Ăḥîṯōp̄El](../../strongs/h/h302.md) was [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [ben](../../strongs/h/h1121.md) of [Bᵊnāyâ](../../strongs/h/h1141.md), and ['Eḇyāṯār](../../strongs/h/h54.md): and the [śar](../../strongs/h/h8269.md) of the [melek](../../strongs/h/h4428.md) [tsaba'](../../strongs/h/h6635.md) was [Yô'āḇ](../../strongs/h/h3097.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 26](1chronicles_26.md) - [1Chronicles 28](1chronicles_28.md)