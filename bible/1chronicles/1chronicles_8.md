# [1Chronicles 8](https://www.blueletterbible.org/kjv/1chronicles/8)

<a name="1chronicles_8_1"></a>1Chronicles 8:1

Now [Binyāmîn](../../strongs/h/h1144.md) [yalad](../../strongs/h/h3205.md) [Belaʿ](../../strongs/h/h1106.md) his [bᵊḵôr](../../strongs/h/h1060.md), ['Ašbēl](../../strongs/h/h788.md) the second, and ['Aḥraḥ](../../strongs/h/h315.md) the third,

<a name="1chronicles_8_2"></a>1Chronicles 8:2

[Nôḥâ](../../strongs/h/h5119.md) the fourth, and [Rāp̄Ā'](../../strongs/h/h7498.md) the fifth.

<a name="1chronicles_8_3"></a>1Chronicles 8:3

And the [ben](../../strongs/h/h1121.md) of [Belaʿ](../../strongs/h/h1106.md) were, ['Adār](../../strongs/h/h146.md), and [Gērā'](../../strongs/h/h1617.md), and ['Ăḇîhûḏ](../../strongs/h/h31.md),

<a name="1chronicles_8_4"></a>1Chronicles 8:4

And ['Ăḇîšûaʿ](../../strongs/h/h50.md), and [Naʿămān](../../strongs/h/h5283.md), and ['Ăḥôaḥ](../../strongs/h/h265.md),

<a name="1chronicles_8_5"></a>1Chronicles 8:5

And [Gērā'](../../strongs/h/h1617.md), and [Šᵊp̄Ûp̄Ām](../../strongs/h/h8197.md), and [Ḥûrām](../../strongs/h/h2361.md).

<a name="1chronicles_8_6"></a>1Chronicles 8:6

And these are the [ben](../../strongs/h/h1121.md) of ['Ēḥûḏ](../../strongs/h/h261.md): these are the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of the [yashab](../../strongs/h/h3427.md) of [Geḇaʿ](../../strongs/h/h1387.md), and they [gālâ](../../strongs/h/h1540.md) them to [Mānaḥaṯ](../../strongs/h/h4506.md):

<a name="1chronicles_8_7"></a>1Chronicles 8:7

And [Naʿămān](../../strongs/h/h5283.md), and ['Ăḥîyâ](../../strongs/h/h281.md), and [Gērā'](../../strongs/h/h1617.md), he [gālâ](../../strongs/h/h1540.md) them, and [yalad](../../strongs/h/h3205.md) [ʿUzzā'](../../strongs/h/h5798.md), and ['Ăḥîḥuḏ](../../strongs/h/h284.md).

<a name="1chronicles_8_8"></a>1Chronicles 8:8

And [Šaḥărayim](../../strongs/h/h7842.md) [yalad](../../strongs/h/h3205.md) in the [sadeh](../../strongs/h/h7704.md) of [Mô'āḇ](../../strongs/h/h4124.md), after he had [shalach](../../strongs/h/h7971.md) them; [Ḥûšîm](../../strongs/h/h2366.md) and [BaʿĂrā'](../../strongs/h/h1199.md) were his ['ishshah](../../strongs/h/h802.md).

<a name="1chronicles_8_9"></a>1Chronicles 8:9

And he [yalad](../../strongs/h/h3205.md) of [Ḥāḏši](../../strongs/h/h2321.md) his ['ishshah](../../strongs/h/h802.md), [Yôḇāḇ](../../strongs/h/h3103.md), and [Ṣiḇyā'](../../strongs/h/h6644.md), and [Mêšā'](../../strongs/h/h4331.md), and [Malkām](../../strongs/h/h4445.md),

<a name="1chronicles_8_10"></a>1Chronicles 8:10

And [YᵊʿÛṣ](../../strongs/h/h3263.md), and [Śāḵyâ](../../strongs/h/h7634.md), and [Mirmâ](../../strongs/h/h4821.md). These were his [ben](../../strongs/h/h1121.md), [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md).

<a name="1chronicles_8_11"></a>1Chronicles 8:11

And of [Ḥûšîm](../../strongs/h/h2366.md) he [yalad](../../strongs/h/h3205.md) ['Ăḇîṭûḇ](../../strongs/h/h36.md), and ['ElpaʿAl](../../strongs/h/h508.md).

<a name="1chronicles_8_12"></a>1Chronicles 8:12

The [ben](../../strongs/h/h1121.md) of ['ElpaʿAl](../../strongs/h/h508.md); [ʿēḇer](../../strongs/h/h5677.md), and [MišʿĀm](../../strongs/h/h4936.md), and [Šemer](../../strongs/h/h8106.md), who [bānâ](../../strongs/h/h1129.md) ['Ônô](../../strongs/h/h207.md), and [Lōḏ](../../strongs/h/h3850.md), with the [bath](../../strongs/h/h1323.md) thereof:

<a name="1chronicles_8_13"></a>1Chronicles 8:13

[Bᵊrîʿâ](../../strongs/h/h1283.md) also, and [Šemaʿ](../../strongs/h/h8087.md), who were [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of the [yashab](../../strongs/h/h3427.md) of ['Ayyālôn](../../strongs/h/h357.md), who [bāraḥ](../../strongs/h/h1272.md) the [yashab](../../strongs/h/h3427.md) of [Gaṯ](../../strongs/h/h1661.md):

<a name="1chronicles_8_14"></a>1Chronicles 8:14

And ['Aḥyô](../../strongs/h/h283.md), [Šāšaq](../../strongs/h/h8349.md), and [Yᵊrêmôṯ](../../strongs/h/h3406.md),

<a name="1chronicles_8_15"></a>1Chronicles 8:15

And [Zᵊḇaḏyâ](../../strongs/h/h2069.md), and [ʿĂrāḏ](../../strongs/h/h6166.md), and [ʿEḏer](../../strongs/h/h5738.md),

<a name="1chronicles_8_16"></a>1Chronicles 8:16

And [Mîḵā'ēl](../../strongs/h/h4317.md), and [Yišpâ](../../strongs/h/h3472.md), and [Yôḥā'](../../strongs/h/h3109.md), the [ben](../../strongs/h/h1121.md) of [Bᵊrîʿâ](../../strongs/h/h1283.md);

<a name="1chronicles_8_17"></a>1Chronicles 8:17

And [Zᵊḇaḏyâ](../../strongs/h/h2069.md), and [Mᵊšullām](../../strongs/h/h4918.md), and [Ḥizqî](../../strongs/h/h2395.md), and [Ḥeḇer](../../strongs/h/h2268.md),

<a name="1chronicles_8_18"></a>1Chronicles 8:18

[Yišmᵊray](../../strongs/h/h3461.md) also, and [Yizlî'Â](../../strongs/h/h3152.md), and [Yôḇāḇ](../../strongs/h/h3103.md), the [ben](../../strongs/h/h1121.md) of ['ElpaʿAl](../../strongs/h/h508.md);

<a name="1chronicles_8_19"></a>1Chronicles 8:19

And [Yāqîm](../../strongs/h/h3356.md), and [Ziḵrî](../../strongs/h/h2147.md), and [Zaḇdî](../../strongs/h/h2067.md),

<a name="1chronicles_8_20"></a>1Chronicles 8:20

And ['ĔlîʿÊnay](../../strongs/h/h462.md), and [Ṣillᵊṯay](../../strongs/h/h6769.md), and ['Ĕlî'Ēl](../../strongs/h/h447.md),

<a name="1chronicles_8_21"></a>1Chronicles 8:21

And [ʿĂḏāyâ](../../strongs/h/h5718.md), and [Bᵊrā'Yâ](../../strongs/h/h1256.md), and [Šimrāṯ](../../strongs/h/h8119.md), the [ben](../../strongs/h/h1121.md) of [Šimʿî](../../strongs/h/h8096.md);

<a name="1chronicles_8_22"></a>1Chronicles 8:22

And [Yišpān](../../strongs/h/h3473.md), and [ʿēḇer](../../strongs/h/h5677.md), and ['Ĕlî'Ēl](../../strongs/h/h447.md),

<a name="1chronicles_8_23"></a>1Chronicles 8:23

And [ʿAḇdôn](../../strongs/h/h5658.md), and [Ziḵrî](../../strongs/h/h2147.md), and [Ḥānān](../../strongs/h/h2605.md),

<a name="1chronicles_8_24"></a>1Chronicles 8:24

And [Ḥănanyâ](../../strongs/h/h2608.md), and [ʿÊlām](../../strongs/h/h5867.md), and [ʿAnṯōṯîyâ](../../strongs/h/h6070.md),

<a name="1chronicles_8_25"></a>1Chronicles 8:25

And [Yip̄Dᵊyâ](../../strongs/h/h3301.md), and [Pᵊnû'ēl](../../strongs/h/h6439.md), the [ben](../../strongs/h/h1121.md) of [Šāšaq](../../strongs/h/h8349.md);

<a name="1chronicles_8_26"></a>1Chronicles 8:26

And [Šamšᵊray](../../strongs/h/h8125.md), and [Šᵊḥaryâ](../../strongs/h/h7841.md), and [ʿĂṯalyâ](../../strongs/h/h6271.md),

<a name="1chronicles_8_27"></a>1Chronicles 8:27

And [YaʿĂrešyâ](../../strongs/h/h3298.md), and ['Ēlîyâ](../../strongs/h/h452.md), and [Ziḵrî](../../strongs/h/h2147.md), the [ben](../../strongs/h/h1121.md) of [Yᵊrōḥām](../../strongs/h/h3395.md).

<a name="1chronicles_8_28"></a>1Chronicles 8:28

These were [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md), by their [towlĕdah](../../strongs/h/h8435.md), [ro'sh](../../strongs/h/h7218.md) men. These [yashab](../../strongs/h/h3427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="1chronicles_8_29"></a>1Chronicles 8:29

And at [Giḇʿôn](../../strongs/h/h1391.md) [yashab](../../strongs/h/h3427.md) the ['ab](../../strongs/h/h1.md) of [Giḇʿôn](../../strongs/h/h1391.md) h25; whose ['ishshah](../../strongs/h/h802.md) [shem](../../strongs/h/h8034.md) was [Maʿăḵâ](../../strongs/h/h4601.md):

<a name="1chronicles_8_30"></a>1Chronicles 8:30

And his [bᵊḵôr](../../strongs/h/h1060.md) [ben](../../strongs/h/h1121.md) [ʿAḇdôn](../../strongs/h/h5658.md), and [Ṣaûār](../../strongs/h/h6698.md), and [Qîš](../../strongs/h/h7027.md), and [BaʿAl](../../strongs/h/h1168.md), and [Nāḏāḇ](../../strongs/h/h5070.md),

<a name="1chronicles_8_31"></a>1Chronicles 8:31

And [Gᵊḏōr](../../strongs/h/h1446.md), and ['Aḥyô](../../strongs/h/h283.md), and [Zēḵer](../../strongs/h/h2144.md).

<a name="1chronicles_8_32"></a>1Chronicles 8:32

And [Miqlôṯ](../../strongs/h/h4732.md) [yalad](../../strongs/h/h3205.md) [Šim'Â](../../strongs/h/h8039.md). And these also [yashab](../../strongs/h/h3427.md) with their ['ach](../../strongs/h/h251.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), over against [neḡeḏ](../../strongs/h/h5048.md).

<a name="1chronicles_8_33"></a>1Chronicles 8:33

And [Nēr](../../strongs/h/h5369.md) [yalad](../../strongs/h/h3205.md) [Qîš](../../strongs/h/h7027.md), and [Qîš](../../strongs/h/h7027.md) [yalad](../../strongs/h/h3205.md) [Šā'ûl](../../strongs/h/h7586.md), and [Šā'ûl](../../strongs/h/h7586.md) [yalad](../../strongs/h/h3205.md) [Yᵊhônāṯān](../../strongs/h/h3083.md), and [Malkîšûaʿ](../../strongs/h/h4444.md), and ['Ăḇînāḏāḇ](../../strongs/h/h41.md), and ['EšbaʿAl](../../strongs/h/h792.md).

<a name="1chronicles_8_34"></a>1Chronicles 8:34

And the [ben](../../strongs/h/h1121.md) of [Yᵊhônāṯān](../../strongs/h/h3083.md) was [Mᵊrîḇ BaʿAl](../../strongs/h/h4807.md); and [Mᵊrîḇ BaʿAl](../../strongs/h/h4807.md) [yalad](../../strongs/h/h3205.md) [Mîḵâ](../../strongs/h/h4318.md).

<a name="1chronicles_8_35"></a>1Chronicles 8:35

And the [ben](../../strongs/h/h1121.md) of [Mîḵâ](../../strongs/h/h4318.md) were, [Pîṯôn](../../strongs/h/h6377.md), and [Meleḵ](../../strongs/h/h4429.md), and [Ta'Rēaʿ](../../strongs/h/h8390.md), and ['Āḥāz](../../strongs/h/h271.md).

<a name="1chronicles_8_36"></a>1Chronicles 8:36

And ['Āḥāz](../../strongs/h/h271.md) [yalad](../../strongs/h/h3205.md) [YᵊhôʿAdâ](../../strongs/h/h3085.md); and [YᵊhôʿAdâ](../../strongs/h/h3085.md) [yalad](../../strongs/h/h3205.md) [ʿĀlemeṯ](../../strongs/h/h5964.md), and [ʿAzmāveṯ](../../strongs/h/h5820.md), and [Zimrî](../../strongs/h/h2174.md); and [Zimrî](../../strongs/h/h2174.md) [yalad](../../strongs/h/h3205.md) [Môṣā'](../../strongs/h/h4162.md),

<a name="1chronicles_8_37"></a>1Chronicles 8:37

And [Môṣā'](../../strongs/h/h4162.md) [yalad](../../strongs/h/h3205.md) [BinʿĀ'](../../strongs/h/h1150.md): [Rāp̄Ā'](../../strongs/h/h7498.md) was his [ben](../../strongs/h/h1121.md), ['ElʿĀśâ](../../strongs/h/h501.md) his [ben](../../strongs/h/h1121.md), ['Āṣēl](../../strongs/h/h682.md) his [ben](../../strongs/h/h1121.md):

<a name="1chronicles_8_38"></a>1Chronicles 8:38

And ['Āṣēl](../../strongs/h/h682.md) had six [ben](../../strongs/h/h1121.md), whose [shem](../../strongs/h/h8034.md) are these, [ʿAzrîqām](../../strongs/h/h5840.md), [Bōḵrû](../../strongs/h/h1074.md), and [Yišmāʿē'l](../../strongs/h/h3458.md), and [ŠᵊʿAryâ](../../strongs/h/h8187.md), and [ʿŌḇaḏyâ](../../strongs/h/h5662.md), and [Ḥānān](../../strongs/h/h2605.md). All these were the [ben](../../strongs/h/h1121.md) of ['Āṣēl](../../strongs/h/h682.md).

<a name="1chronicles_8_39"></a>1Chronicles 8:39

And the [ben](../../strongs/h/h1121.md) of [ʿĒšeq](../../strongs/h/h6232.md) his ['ach](../../strongs/h/h251.md) were, ['Ûlām](../../strongs/h/h198.md) his [bᵊḵôr](../../strongs/h/h1060.md), [Yᵊʿûš](../../strongs/h/h3266.md) the second, and ['Ĕlîp̄Eleṭ](../../strongs/h/h467.md) the third.

<a name="1chronicles_8_40"></a>1Chronicles 8:40

And the [ben](../../strongs/h/h1121.md) of ['Ûlām](../../strongs/h/h198.md) were [gibôr](../../strongs/h/h1368.md) ['enowsh](../../strongs/h/h582.md) of [ḥayil](../../strongs/h/h2428.md), [dāraḵ](../../strongs/h/h1869.md) [qesheth](../../strongs/h/h7198.md), and had [rabah](../../strongs/h/h7235.md) [ben](../../strongs/h/h1121.md), and [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md), an hundred and fifty. All these are of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 7](1chronicles_7.md) - [1Chronicles 9](1chronicles_9.md)