# [1Chronicles 29](https://www.blueletterbible.org/kjv/1chronicles/29)

<a name="1chronicles_29_1"></a>1Chronicles 29:1

Furthermore [Dāviḏ](../../strongs/h/h1732.md) the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto all the [qāhēl](../../strongs/h/h6951.md), [Šᵊlōmô](../../strongs/h/h8010.md) my [ben](../../strongs/h/h1121.md), whom alone ['Elohiym](../../strongs/h/h430.md) hath [bāḥar](../../strongs/h/h977.md), is yet [naʿar](../../strongs/h/h5288.md) and [raḵ](../../strongs/h/h7390.md), and the [mĕla'kah](../../strongs/h/h4399.md) is [gadowl](../../strongs/h/h1419.md): for the [bîrâ](../../strongs/h/h1002.md) is not for ['āḏām](../../strongs/h/h120.md), but for [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md).

<a name="1chronicles_29_2"></a>1Chronicles 29:2

Now I have [kuwn](../../strongs/h/h3559.md) with all my [koach](../../strongs/h/h3581.md) for the [bayith](../../strongs/h/h1004.md) of my ['Elohiym](../../strongs/h/h430.md) the [zāhāḇ](../../strongs/h/h2091.md) for things to be made of [zāhāḇ](../../strongs/h/h2091.md), and the [keceph](../../strongs/h/h3701.md) for things of [keceph](../../strongs/h/h3701.md), and the [nᵊḥšeṯ](../../strongs/h/h5178.md) for things of [nᵊḥšeṯ](../../strongs/h/h5178.md), the [barzel](../../strongs/h/h1270.md) for things of [barzel](../../strongs/h/h1270.md), and ['ets](../../strongs/h/h6086.md) for things of ['ets](../../strongs/h/h6086.md); [šōham](../../strongs/h/h7718.md) ['eben](../../strongs/h/h68.md), and stones to be [millu'](../../strongs/h/h4394.md), [pûḵ](../../strongs/h/h6320.md) ['eben](../../strongs/h/h68.md), and of divers [riqmâ](../../strongs/h/h7553.md), and all manner of [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md), and [šayiš](../../strongs/h/h7893.md) ['eben](../../strongs/h/h68.md) in [rōḇ](../../strongs/h/h7230.md).

<a name="1chronicles_29_3"></a>1Chronicles 29:3

Moreover, because I have set my [ratsah](../../strongs/h/h7521.md) to the [bayith](../../strongs/h/h1004.md) of my ['Elohiym](../../strongs/h/h430.md), I have of mine own [sᵊḡullâ](../../strongs/h/h5459.md), of [zāhāḇ](../../strongs/h/h2091.md) and [keceph](../../strongs/h/h3701.md), which I have [nathan](../../strongs/h/h5414.md) to the [bayith](../../strongs/h/h1004.md) of my ['Elohiym](../../strongs/h/h430.md), over and [maʿal](../../strongs/h/h4605.md) all that I have [kuwn](../../strongs/h/h3559.md) for the [qodesh](../../strongs/h/h6944.md) [bayith](../../strongs/h/h1004.md),

<a name="1chronicles_29_4"></a>1Chronicles 29:4

Even three thousand [kikār](../../strongs/h/h3603.md) of [zāhāḇ](../../strongs/h/h2091.md), of the [zāhāḇ](../../strongs/h/h2091.md) of ['Ôp̄îr](../../strongs/h/h211.md), and seven thousand [kikār](../../strongs/h/h3603.md) of [zaqaq](../../strongs/h/h2212.md) [keceph](../../strongs/h/h3701.md), to [ṭûaḥ](../../strongs/h/h2902.md) the [qîr](../../strongs/h/h7023.md) of the [bayith](../../strongs/h/h1004.md) withal:

<a name="1chronicles_29_5"></a>1Chronicles 29:5

The [zāhāḇ](../../strongs/h/h2091.md) for things of [zāhāḇ](../../strongs/h/h2091.md), and the [keceph](../../strongs/h/h3701.md) for things of [keceph](../../strongs/h/h3701.md), and for all manner of [mĕla'kah](../../strongs/h/h4399.md) to be made by the [yad](../../strongs/h/h3027.md) of [ḥārāš](../../strongs/h/h2796.md). And who then is [nāḏaḇ](../../strongs/h/h5068.md) to [mālā'](../../strongs/h/h4390.md) his [yad](../../strongs/h/h3027.md) this [yowm](../../strongs/h/h3117.md) unto [Yĕhovah](../../strongs/h/h3068.md)?

<a name="1chronicles_29_6"></a>1Chronicles 29:6

Then the [śar](../../strongs/h/h8269.md) of the ['ab](../../strongs/h/h1.md) and [śar](../../strongs/h/h8269.md) of the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), and the [śar](../../strongs/h/h8269.md) of thousands and of hundreds, with the [śar](../../strongs/h/h8269.md) of the [melek](../../strongs/h/h4428.md) [mĕla'kah](../../strongs/h/h4399.md), [nāḏaḇ](../../strongs/h/h5068.md),

<a name="1chronicles_29_7"></a>1Chronicles 29:7

And [nathan](../../strongs/h/h5414.md) for the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) of [zāhāḇ](../../strongs/h/h2091.md) five thousand [kikār](../../strongs/h/h3603.md) and ten [ribô'](../../strongs/h/h7239.md) ['ăḏarkôn](../../strongs/h/h150.md), and of [keceph](../../strongs/h/h3701.md) ten thousand [kikār](../../strongs/h/h3603.md), and of [nᵊḥšeṯ](../../strongs/h/h5178.md) eighteen [ribô'](../../strongs/h/h7239.md) thousand [kikār](../../strongs/h/h3603.md), and one hundred thousand [kikār](../../strongs/h/h3603.md) of [barzel](../../strongs/h/h1270.md).

<a name="1chronicles_29_8"></a>1Chronicles 29:8

And they with whom ['eben](../../strongs/h/h68.md) were [māṣā'](../../strongs/h/h4672.md) [nathan](../../strongs/h/h5414.md) them to the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), by the [yad](../../strongs/h/h3027.md) of [Yᵊḥî'Ēl](../../strongs/h/h3171.md) the [Gēršunnî](../../strongs/h/h1649.md).

<a name="1chronicles_29_9"></a>1Chronicles 29:9

Then the ['am](../../strongs/h/h5971.md) [samach](../../strongs/h/h8055.md), for that they [nāḏaḇ](../../strongs/h/h5068.md), because with [šālēm](../../strongs/h/h8003.md) [leb](../../strongs/h/h3820.md) they offered [nāḏaḇ](../../strongs/h/h5068.md) to [Yĕhovah](../../strongs/h/h3068.md): and [Dāviḏ](../../strongs/h/h1732.md) the [melek](../../strongs/h/h4428.md) also [samach](../../strongs/h/h8055.md) with [gadowl](../../strongs/h/h1419.md) [simchah](../../strongs/h/h8057.md).

<a name="1chronicles_29_10"></a>1Chronicles 29:10

Wherefore [Dāviḏ](../../strongs/h/h1732.md) [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md) ['ayin](../../strongs/h/h5869.md) all the [qāhēl](../../strongs/h/h6951.md): and [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be thou, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) our ['ab](../../strongs/h/h1.md), ['owlam](../../strongs/h/h5769.md) and ['owlam](../../strongs/h/h5769.md).

<a name="1chronicles_29_11"></a>1Chronicles 29:11

Thine, [Yĕhovah](../../strongs/h/h3068.md), is the [gᵊḏûlâ](../../strongs/h/h1420.md), and the [gᵊḇûrâ](../../strongs/h/h1369.md), and the [tip̄'ārâ](../../strongs/h/h8597.md), and the [netsach](../../strongs/h/h5331.md), and the [howd](../../strongs/h/h1935.md): for all that is in the [shamayim](../../strongs/h/h8064.md) and in the ['erets](../../strongs/h/h776.md) is thine; thine is the [mamlāḵâ](../../strongs/h/h4467.md), [Yĕhovah](../../strongs/h/h3068.md), and thou art [miṯnaśśē'](../../strongs/h/h4984.md) as [ro'sh](../../strongs/h/h7218.md) above all.

<a name="1chronicles_29_12"></a>1Chronicles 29:12

Both [ʿōšer](../../strongs/h/h6239.md) and [kabowd](../../strongs/h/h3519.md) [paniym](../../strongs/h/h6440.md), and thou [mashal](../../strongs/h/h4910.md) over all; and in thine [yad](../../strongs/h/h3027.md) is [koach](../../strongs/h/h3581.md) and [gᵊḇûrâ](../../strongs/h/h1369.md); and in thine [yad](../../strongs/h/h3027.md) it is to make [gāḏal](../../strongs/h/h1431.md), and to [ḥāzaq](../../strongs/h/h2388.md) unto all.

<a name="1chronicles_29_13"></a>1Chronicles 29:13

Now therefore, our ['Elohiym](../../strongs/h/h430.md), we [yadah](../../strongs/h/h3034.md) thee, and [halal](../../strongs/h/h1984.md) thy [tip̄'ārâ](../../strongs/h/h8597.md) [shem](../../strongs/h/h8034.md).

<a name="1chronicles_29_14"></a>1Chronicles 29:14

But who am I, and what is my ['am](../../strongs/h/h5971.md), that we should [ʿāṣar](../../strongs/h/h6113.md) [koach](../../strongs/h/h3581.md) to offer so [nāḏaḇ](../../strongs/h/h5068.md) after this sort? for all things come of thee, and of thine [yad](../../strongs/h/h3027.md) have we [nathan](../../strongs/h/h5414.md) thee.

<a name="1chronicles_29_15"></a>1Chronicles 29:15

For we are [ger](../../strongs/h/h1616.md) [paniym](../../strongs/h/h6440.md) thee, and [tôšāḇ](../../strongs/h/h8453.md), as were all our ['ab](../../strongs/h/h1.md): our [yowm](../../strongs/h/h3117.md) on the ['erets](../../strongs/h/h776.md) are as a [ṣēl](../../strongs/h/h6738.md), and there is none [miqvê](../../strongs/h/h4723.md).

<a name="1chronicles_29_16"></a>1Chronicles 29:16

[Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), all this [hāmôn](../../strongs/h/h1995.md) that we have [kuwn](../../strongs/h/h3559.md) to [bānâ](../../strongs/h/h1129.md) thee a [bayith](../../strongs/h/h1004.md) for thine [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md) cometh of thine [yad](../../strongs/h/h3027.md), and is all thine own.

<a name="1chronicles_29_17"></a>1Chronicles 29:17

I [yada'](../../strongs/h/h3045.md) also, my ['Elohiym](../../strongs/h/h430.md), that thou [bachan](../../strongs/h/h974.md) the [lebab](../../strongs/h/h3824.md), and hast [ratsah](../../strongs/h/h7521.md) in [yōšer](../../strongs/h/h3476.md). As for me, in the [meyshar](../../strongs/h/h4339.md) of mine [lebab](../../strongs/h/h3824.md) I have [nāḏaḇ](../../strongs/h/h5068.md) all these things: and now have I [ra'ah](../../strongs/h/h7200.md) with [simchah](../../strongs/h/h8057.md) thy ['am](../../strongs/h/h5971.md), which are [māṣā'](../../strongs/h/h4672.md) here, to [nāḏaḇ](../../strongs/h/h5068.md) unto thee.

<a name="1chronicles_29_18"></a>1Chronicles 29:18

[Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of ['Abraham](../../strongs/h/h85.md), [Yiṣḥāq](../../strongs/h/h3327.md), and of [Yisra'el](../../strongs/h/h3478.md), our ['ab](../../strongs/h/h1.md), [shamar](../../strongs/h/h8104.md) this ['owlam](../../strongs/h/h5769.md) in the [yetser](../../strongs/h/h3336.md) of the [maḥăšāḇâ](../../strongs/h/h4284.md) of the [lebab](../../strongs/h/h3824.md) of thy ['am](../../strongs/h/h5971.md), and [kuwn](../../strongs/h/h3559.md) their [lebab](../../strongs/h/h3824.md) unto thee:

<a name="1chronicles_29_19"></a>1Chronicles 29:19

And [nathan](../../strongs/h/h5414.md) unto [Šᵊlōmô](../../strongs/h/h8010.md) my [ben](../../strongs/h/h1121.md) a [šālēm](../../strongs/h/h8003.md) [lebab](../../strongs/h/h3824.md), to [shamar](../../strongs/h/h8104.md) thy [mitsvah](../../strongs/h/h4687.md), thy [ʿēḏûṯ](../../strongs/h/h5715.md), and thy [choq](../../strongs/h/h2706.md), and to ['asah](../../strongs/h/h6213.md) all these things, and to [bānâ](../../strongs/h/h1129.md) the [bîrâ](../../strongs/h/h1002.md), for the which I have made [kuwn](../../strongs/h/h3559.md).

<a name="1chronicles_29_20"></a>1Chronicles 29:20

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to all the [qāhēl](../../strongs/h/h6951.md), Now [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md). And all the [qāhēl](../../strongs/h/h6951.md) [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md), and bowed down their [qāḏaḏ](../../strongs/h/h6915.md), and [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md), and the [melek](../../strongs/h/h4428.md).

<a name="1chronicles_29_21"></a>1Chronicles 29:21

And they [zabach](../../strongs/h/h2076.md) [zebach](../../strongs/h/h2077.md) unto [Yĕhovah](../../strongs/h/h3068.md), and [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md), on the [māḥŏrāṯ](../../strongs/h/h4283.md) after that [yowm](../../strongs/h/h3117.md), even a thousand [par](../../strongs/h/h6499.md), a thousand ['ayil](../../strongs/h/h352.md), and a thousand [keḇeś](../../strongs/h/h3532.md), with their [necek](../../strongs/h/h5262.md), and [zebach](../../strongs/h/h2077.md) in [rōḇ](../../strongs/h/h7230.md) for all [Yisra'el](../../strongs/h/h3478.md):

<a name="1chronicles_29_22"></a>1Chronicles 29:22

And did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) on that [yowm](../../strongs/h/h3117.md) with [gadowl](../../strongs/h/h1419.md) [simchah](../../strongs/h/h8057.md). And they made [Šᵊlōmô](../../strongs/h/h8010.md) the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md) [mālaḵ](../../strongs/h/h4427.md) the second time, and [māšaḥ](../../strongs/h/h4886.md) him unto [Yĕhovah](../../strongs/h/h3068.md) to be the [nāḡîḏ](../../strongs/h/h5057.md), and [Ṣāḏôq](../../strongs/h/h6659.md) to be [kōhēn](../../strongs/h/h3548.md).

<a name="1chronicles_29_23"></a>1Chronicles 29:23

Then [Šᵊlōmô](../../strongs/h/h8010.md) [yashab](../../strongs/h/h3427.md) the [kicce'](../../strongs/h/h3678.md) of [Yĕhovah](../../strongs/h/h3068.md) as [melek](../../strongs/h/h4428.md) instead of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md), and [tsalach](../../strongs/h/h6743.md); and all [Yisra'el](../../strongs/h/h3478.md) [shama'](../../strongs/h/h8085.md) him.

<a name="1chronicles_29_24"></a>1Chronicles 29:24

And all the [śar](../../strongs/h/h8269.md), and the [gibôr](../../strongs/h/h1368.md), and all the [ben](../../strongs/h/h1121.md) likewise of [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md), [nathan](../../strongs/h/h5414.md) [yad](../../strongs/h/h3027.md) [taḥaṯ](../../strongs/h/h8478.md) unto [Šᵊlōmô](../../strongs/h/h8010.md) the [melek](../../strongs/h/h4428.md).

<a name="1chronicles_29_25"></a>1Chronicles 29:25

And [Yĕhovah](../../strongs/h/h3068.md) [gāḏal](../../strongs/h/h1431.md) [Šᵊlōmô](../../strongs/h/h8010.md) [maʿal](../../strongs/h/h4605.md) in the ['ayin](../../strongs/h/h5869.md) of all [Yisra'el](../../strongs/h/h3478.md), and [nathan](../../strongs/h/h5414.md) upon him such [malkuwth](../../strongs/h/h4438.md) [howd](../../strongs/h/h1935.md) as had not been on any [melek](../../strongs/h/h4428.md) [paniym](../../strongs/h/h6440.md) him in [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_29_26"></a>1Chronicles 29:26

Thus [Dāviḏ](../../strongs/h/h1732.md) the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md) [mālaḵ](../../strongs/h/h4427.md) over all [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_29_27"></a>1Chronicles 29:27

And the [yowm](../../strongs/h/h3117.md) that he [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) was forty [šānâ](../../strongs/h/h8141.md); seven [šānâ](../../strongs/h/h8141.md) [mālaḵ](../../strongs/h/h4427.md) he in [Ḥeḇrôn](../../strongs/h/h2275.md), and thirty and three years [mālaḵ](../../strongs/h/h4427.md) he in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="1chronicles_29_28"></a>1Chronicles 29:28

And he [muwth](../../strongs/h/h4191.md) in a [towb](../../strongs/h/h2896.md) [śêḇâ](../../strongs/h/h7872.md), [śāḇēaʿ](../../strongs/h/h7649.md) of [yowm](../../strongs/h/h3117.md), [ʿōšer](../../strongs/h/h6239.md), and [kabowd](../../strongs/h/h3519.md): and [Šᵊlōmô](../../strongs/h/h8010.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1chronicles_29_29"></a>1Chronicles 29:29

Now the [dabar](../../strongs/h/h1697.md) of [Dāviḏ](../../strongs/h/h1732.md) the [melek](../../strongs/h/h4428.md), [ri'šôn](../../strongs/h/h7223.md) and ['aḥărôn](../../strongs/h/h314.md), behold, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [dabar](../../strongs/h/h1697.md) of [Šᵊmû'Ēl](../../strongs/h/h8050.md) the [ra'ah](../../strongs/h/h7200.md), and in the [dabar](../../strongs/h/h1697.md) of [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md), and in the [dabar](../../strongs/h/h1697.md) of [Gāḏ](../../strongs/h/h1410.md) the [ḥōzê](../../strongs/h/h2374.md),

<a name="1chronicles_29_30"></a>1Chronicles 29:30

With all his [malkuwth](../../strongs/h/h4438.md) and his [gᵊḇûrâ](../../strongs/h/h1369.md), and the [ʿēṯ](../../strongs/h/h6256.md) that ['abar](../../strongs/h/h5674.md) him, and over [Yisra'el](../../strongs/h/h3478.md), and over all the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 28](1chronicles_28.md) - [1Chronicles 30](1chronicles_30.md)