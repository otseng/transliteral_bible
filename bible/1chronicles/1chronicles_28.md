# [1Chronicles 28](https://www.blueletterbible.org/kjv/1chronicles/28)

<a name="1chronicles_28_1"></a>1Chronicles 28:1

And [Dāviḏ](../../strongs/h/h1732.md) [qāhal](../../strongs/h/h6950.md) all the [śar](../../strongs/h/h8269.md) of [Yisra'el](../../strongs/h/h3478.md), the [śar](../../strongs/h/h8269.md) of the [shebet](../../strongs/h/h7626.md), and the [śar](../../strongs/h/h8269.md) of the [maḥălōqeṯ](../../strongs/h/h4256.md) that [sharath](../../strongs/h/h8334.md) to the [melek](../../strongs/h/h4428.md) by [maḥălōqeṯ](../../strongs/h/h4256.md), and the [śar](../../strongs/h/h8269.md) over the thousands, and [śar](../../strongs/h/h8269.md) over the hundreds, and the [śar](../../strongs/h/h8269.md) over all the [rᵊḵûš](../../strongs/h/h7399.md) and [miqnê](../../strongs/h/h4735.md) of the [melek](../../strongs/h/h4428.md), and of his [ben](../../strongs/h/h1121.md), with the [sārîs](../../strongs/h/h5631.md), and with the [gibôr](../../strongs/h/h1368.md), and with all the valiant [ḥayil](../../strongs/h/h2428.md), unto [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="1chronicles_28_2"></a>1Chronicles 28:2

Then [Dāviḏ](../../strongs/h/h1732.md) the [melek](../../strongs/h/h4428.md) [quwm](../../strongs/h/h6965.md) upon his [regel](../../strongs/h/h7272.md), and ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) me, my ['ach](../../strongs/h/h251.md), and my ['am](../../strongs/h/h5971.md): As for me, I had in mine [lebab](../../strongs/h/h3824.md) to [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) of [mᵊnûḥâ](../../strongs/h/h4496.md) for the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md), and for the [hăḏōm](../../strongs/h/h1916.md) [regel](../../strongs/h/h7272.md) of our ['Elohiym](../../strongs/h/h430.md), and had [kuwn](../../strongs/h/h3559.md) for the [bānâ](../../strongs/h/h1129.md):

<a name="1chronicles_28_3"></a>1Chronicles 28:3

But ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto me, Thou shalt not [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) for my [shem](../../strongs/h/h8034.md), because thou hast been an ['iysh](../../strongs/h/h376.md) of [milḥāmâ](../../strongs/h/h4421.md), and hast [šāp̄aḵ](../../strongs/h/h8210.md) [dam](../../strongs/h/h1818.md).

<a name="1chronicles_28_4"></a>1Chronicles 28:4

Howbeit [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [bāḥar](../../strongs/h/h977.md) me before all the [bayith](../../strongs/h/h1004.md) of my ['ab](../../strongs/h/h1.md) to be [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md) ['owlam](../../strongs/h/h5769.md): for he hath [bāḥar](../../strongs/h/h977.md) [Yehuwdah](../../strongs/h/h3063.md) to be the [nāḡîḏ](../../strongs/h/h5057.md); and of the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md), the [bayith](../../strongs/h/h1004.md) of my ['ab](../../strongs/h/h1.md); and among the [ben](../../strongs/h/h1121.md) of my ['ab](../../strongs/h/h1.md) he [ratsah](../../strongs/h/h7521.md) me to make me [mālaḵ](../../strongs/h/h4427.md) over all [Yisra'el](../../strongs/h/h3478.md):

<a name="1chronicles_28_5"></a>1Chronicles 28:5

And of all my [ben](../../strongs/h/h1121.md), (for [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) me [rab](../../strongs/h/h7227.md) [ben](../../strongs/h/h1121.md),) he hath [bāḥar](../../strongs/h/h977.md) [Šᵊlōmô](../../strongs/h/h8010.md) my [ben](../../strongs/h/h1121.md) to [yashab](../../strongs/h/h3427.md) upon the [kicce'](../../strongs/h/h3678.md) of the [malkuwth](../../strongs/h/h4438.md) of [Yĕhovah](../../strongs/h/h3068.md) over [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_28_6"></a>1Chronicles 28:6

And he ['āmar](../../strongs/h/h559.md) unto me, [Šᵊlōmô](../../strongs/h/h8010.md) thy [ben](../../strongs/h/h1121.md), he shall [bānâ](../../strongs/h/h1129.md) my [bayith](../../strongs/h/h1004.md) and my [ḥāṣēr](../../strongs/h/h2691.md): for I have [bāḥar](../../strongs/h/h977.md) him to be my [ben](../../strongs/h/h1121.md), and I will be his ['ab](../../strongs/h/h1.md).

<a name="1chronicles_28_7"></a>1Chronicles 28:7

Moreover I will [kuwn](../../strongs/h/h3559.md) his [malkuwth](../../strongs/h/h4438.md) ['owlam](../../strongs/h/h5769.md), if he be [ḥāzaq](../../strongs/h/h2388.md) to ['asah](../../strongs/h/h6213.md) my [mitsvah](../../strongs/h/h4687.md) and my [mishpat](../../strongs/h/h4941.md), as at this [yowm](../../strongs/h/h3117.md).

<a name="1chronicles_28_8"></a>1Chronicles 28:8

Now therefore in the ['ayin](../../strongs/h/h5869.md) of all [Yisra'el](../../strongs/h/h3478.md) the [qāhēl](../../strongs/h/h6951.md) of [Yĕhovah](../../strongs/h/h3068.md), and in the ['ozen](../../strongs/h/h241.md) of our ['Elohiym](../../strongs/h/h430.md), [shamar](../../strongs/h/h8104.md) and [darash](../../strongs/h/h1875.md) for all the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md): that ye may [yarash](../../strongs/h/h3423.md) this [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md), and leave it for a [nāḥal](../../strongs/h/h5157.md) for your [ben](../../strongs/h/h1121.md) ['aḥar](../../strongs/h/h310.md) you ['owlam](../../strongs/h/h5769.md).

<a name="1chronicles_28_9"></a>1Chronicles 28:9

And thou, [Šᵊlōmô](../../strongs/h/h8010.md) my [ben](../../strongs/h/h1121.md), [yada'](../../strongs/h/h3045.md) thou the ['Elohiym](../../strongs/h/h430.md) of thy ['ab](../../strongs/h/h1.md), and ['abad](../../strongs/h/h5647.md) him with a [šālēm](../../strongs/h/h8003.md) [leb](../../strongs/h/h3820.md) and with a [chaphets](../../strongs/h/h2655.md) [nephesh](../../strongs/h/h5315.md): for [Yĕhovah](../../strongs/h/h3068.md) [darash](../../strongs/h/h1875.md) all [lebab](../../strongs/h/h3824.md), and [bîn](../../strongs/h/h995.md) all the [yetser](../../strongs/h/h3336.md) of the [maḥăšāḇâ](../../strongs/h/h4284.md): if thou [darash](../../strongs/h/h1875.md) him, he will be [māṣā'](../../strongs/h/h4672.md) of thee; but if thou ['azab](../../strongs/h/h5800.md) him, he will [zānaḥ](../../strongs/h/h2186.md) thee for [ʿaḏ](../../strongs/h/h5703.md).

<a name="1chronicles_28_10"></a>1Chronicles 28:10

[ra'ah](../../strongs/h/h7200.md) now; for [Yĕhovah](../../strongs/h/h3068.md) hath [bāḥar](../../strongs/h/h977.md) thee to [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) for the [miqdash](../../strongs/h/h4720.md): be [ḥāzaq](../../strongs/h/h2388.md), and ['asah](../../strongs/h/h6213.md) it.

<a name="1chronicles_28_11"></a>1Chronicles 28:11

Then [Dāviḏ](../../strongs/h/h1732.md) [nathan](../../strongs/h/h5414.md) to [Šᵊlōmô](../../strongs/h/h8010.md) his [ben](../../strongs/h/h1121.md) the [taḇnîṯ](../../strongs/h/h8403.md) of the ['ûlām](../../strongs/h/h197.md), and of the [bayith](../../strongs/h/h1004.md) thereof, and of the [ganzaḵ](../../strongs/h/h1597.md) thereof, and of the [ʿălîyâ](../../strongs/h/h5944.md) thereof, and of the [pᵊnîmî](../../strongs/h/h6442.md) [ḥeḏer](../../strongs/h/h2315.md) thereof, and of the [bayith](../../strongs/h/h1004.md) of the [kapōreṯ](../../strongs/h/h3727.md),

<a name="1chronicles_28_12"></a>1Chronicles 28:12

And the [taḇnîṯ](../../strongs/h/h8403.md) of all that he had by the [ruwach](../../strongs/h/h7307.md), of the [ḥāṣēr](../../strongs/h/h2691.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and of all the [liškâ](../../strongs/h/h3957.md) [cabiyb](../../strongs/h/h5439.md), of the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), and of the ['ôṣār](../../strongs/h/h214.md) of the [qodesh](../../strongs/h/h6944.md):

<a name="1chronicles_28_13"></a>1Chronicles 28:13

Also for the [maḥălōqeṯ](../../strongs/h/h4256.md) of the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md), and for all the [mĕla'kah](../../strongs/h/h4399.md) of the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and for all the [kĕliy](../../strongs/h/h3627.md) of [ʿăḇōḏâ](../../strongs/h/h5656.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1chronicles_28_14"></a>1Chronicles 28:14

He gave of [zāhāḇ](../../strongs/h/h2091.md) by [mišqāl](../../strongs/h/h4948.md) for things of [zāhāḇ](../../strongs/h/h2091.md), for all [kĕliy](../../strongs/h/h3627.md) of all manner of [ʿăḇōḏâ](../../strongs/h/h5656.md); silver also for all [kĕliy](../../strongs/h/h3627.md) of [keceph](../../strongs/h/h3701.md) by [mišqāl](../../strongs/h/h4948.md), for all [kĕliy](../../strongs/h/h3627.md) of every kind of [ʿăḇōḏâ](../../strongs/h/h5656.md):

<a name="1chronicles_28_15"></a>1Chronicles 28:15

Even the [mišqāl](../../strongs/h/h4948.md) for the [mᵊnôrâ](../../strongs/h/h4501.md) of [zāhāḇ](../../strongs/h/h2091.md), and for their [nîr](../../strongs/h/h5216.md) of [zāhāḇ](../../strongs/h/h2091.md), by [mišqāl](../../strongs/h/h4948.md) for every [mᵊnôrâ](../../strongs/h/h4501.md), and for the [nîr](../../strongs/h/h5216.md) thereof: and for the [mᵊnôrâ](../../strongs/h/h4501.md) of [keceph](../../strongs/h/h3701.md) by [mišqāl](../../strongs/h/h4948.md), both for the [mᵊnôrâ](../../strongs/h/h4501.md), and also for the [nîr](../../strongs/h/h5216.md) thereof, according to the [ʿăḇōḏâ](../../strongs/h/h5656.md) of every [mᵊnôrâ](../../strongs/h/h4501.md).

<a name="1chronicles_28_16"></a>1Chronicles 28:16

And by [mišqāl](../../strongs/h/h4948.md) he gave [zāhāḇ](../../strongs/h/h2091.md) for the [šulḥān](../../strongs/h/h7979.md) of [maʿăreḵeṯ](../../strongs/h/h4635.md), for every [šulḥān](../../strongs/h/h7979.md); and likewise [keceph](../../strongs/h/h3701.md) for the [šulḥān](../../strongs/h/h7979.md) of [keceph](../../strongs/h/h3701.md):

<a name="1chronicles_28_17"></a>1Chronicles 28:17

Also [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md) for the [mazlēḡ](../../strongs/h/h4207.md), and the [mizrāq](../../strongs/h/h4219.md), and the [qaśvâ](../../strongs/h/h7184.md): and for the [zāhāḇ](../../strongs/h/h2091.md) [kᵊp̄ôr](../../strongs/h/h3713.md) he [mišqāl](../../strongs/h/h4948.md) for every [kᵊp̄ôr](../../strongs/h/h3713.md); and [mišqāl](../../strongs/h/h4948.md) for every [kᵊp̄ôr](../../strongs/h/h3713.md) of [keceph](../../strongs/h/h3701.md):

<a name="1chronicles_28_18"></a>1Chronicles 28:18

And for the [mizbeach](../../strongs/h/h4196.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md) [zaqaq](../../strongs/h/h2212.md) [zāhāḇ](../../strongs/h/h2091.md) by [mišqāl](../../strongs/h/h4948.md); and [zāhāḇ](../../strongs/h/h2091.md) for the [taḇnîṯ](../../strongs/h/h8403.md) of the [merkāḇâ](../../strongs/h/h4818.md) of the [kĕruwb](../../strongs/h/h3742.md), that [pāraś](../../strongs/h/h6566.md), and [cakak](../../strongs/h/h5526.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1chronicles_28_19"></a>1Chronicles 28:19

[Yĕhovah](../../strongs/h/h3068.md) made me [sakal](../../strongs/h/h7919.md) in [kᵊṯāḇ](../../strongs/h/h3791.md) by his [yad](../../strongs/h/h3027.md) upon me, even all the [mĕla'kah](../../strongs/h/h4399.md) of this [taḇnîṯ](../../strongs/h/h8403.md).

<a name="1chronicles_28_20"></a>1Chronicles 28:20

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to [Šᵊlōmô](../../strongs/h/h8010.md) his [ben](../../strongs/h/h1121.md), Be [ḥāzaq](../../strongs/h/h2388.md) and ['amats](../../strongs/h/h553.md), and ['asah](../../strongs/h/h6213.md) it: [yare'](../../strongs/h/h3372.md) not, nor be [ḥāṯaṯ](../../strongs/h/h2865.md): for [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), even my ['Elohiym](../../strongs/h/h430.md), will be with thee; he will not [rāp̄â](../../strongs/h/h7503.md) thee, nor ['azab](../../strongs/h/h5800.md) thee, until thou hast [kalah](../../strongs/h/h3615.md) all the [mĕla'kah](../../strongs/h/h4399.md) for the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1chronicles_28_21"></a>1Chronicles 28:21

And, behold, the [maḥălōqeṯ](../../strongs/h/h4256.md) of the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md), even they shall be with thee for all the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md): and there shall be with thee for all manner of [mĕla'kah](../../strongs/h/h4399.md) every [nāḏîḇ](../../strongs/h/h5081.md) [ḥāḵmâ](../../strongs/h/h2451.md), for any manner of [ʿăḇōḏâ](../../strongs/h/h5656.md): also the [śar](../../strongs/h/h8269.md) and all the ['am](../../strongs/h/h5971.md) will be wholly at thy [dabar](../../strongs/h/h1697.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 27](1chronicles_27.md) - [1Chronicles 29](1chronicles_29.md)