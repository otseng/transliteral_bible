# [1Chronicles 12](https://www.blueletterbible.org/kjv/1chronicles/12)

<a name="1chronicles_12_1"></a>1Chronicles 12:1

Now these are they that [bow'](../../strongs/h/h935.md) to [Dāviḏ](../../strongs/h/h1732.md) to [Ṣiqlāḡ](../../strongs/h/h6860.md), while he [ʿāṣar](../../strongs/h/h6113.md) [paniym](../../strongs/h/h6440.md) of [Šā'ûl](../../strongs/h/h7586.md) the [ben](../../strongs/h/h1121.md) of [Qîš](../../strongs/h/h7027.md): and they were among the [gibôr](../../strongs/h/h1368.md), [ʿāzar](../../strongs/h/h5826.md) of the [milḥāmâ](../../strongs/h/h4421.md).

<a name="1chronicles_12_2"></a>1Chronicles 12:2

They were [nashaq](../../strongs/h/h5401.md) with [qesheth](../../strongs/h/h7198.md), and could use both the [yāman](../../strongs/h/h3231.md) and the [śam'al](../../strongs/h/h8041.md) in ['eben](../../strongs/h/h68.md) and [chets](../../strongs/h/h2671.md) out of a [qesheth](../../strongs/h/h7198.md), even of [Šā'ûl](../../strongs/h/h7586.md) ['ach](../../strongs/h/h251.md) of [Binyāmîn](../../strongs/h/h1144.md).

<a name="1chronicles_12_3"></a>1Chronicles 12:3

The [ro'sh](../../strongs/h/h7218.md) was ['ăḥîʿezer](../../strongs/h/h295.md), then [Yô'Āš](../../strongs/h/h3101.md), the [ben](../../strongs/h/h1121.md) of [ŠᵊmāʿÂ](../../strongs/h/h8094.md) the [giḇʿāṯî](../../strongs/h/h1395.md); and [Yᵊzî'Ēl](../../strongs/h/h3149.md), and [Peleṭ](../../strongs/h/h6404.md), the [ben](../../strongs/h/h1121.md) of [ʿAzmāveṯ](../../strongs/h/h5820.md); and [Bᵊrāḵâ](../../strongs/h/h1294.md), and [Yêû'](../../strongs/h/h3058.md) the [ʿAnnᵊṯōṯî](../../strongs/h/h6069.md),

<a name="1chronicles_12_4"></a>1Chronicles 12:4

And [YišmaʿYâ](../../strongs/h/h3460.md) the [Giḇʿōnî](../../strongs/h/h1393.md), a [gibôr](../../strongs/h/h1368.md) among the thirty, and over the thirty; and [Yirmᵊyâ](../../strongs/h/h3414.md), and [Yaḥăzî'Ēl](../../strongs/h/h3166.md), and [Yôḥānān](../../strongs/h/h3110.md), and [Yôzāḇāḏ](../../strongs/h/h3107.md) the [gᵊḏērāṯî](../../strongs/h/h1452.md),

<a name="1chronicles_12_5"></a>1Chronicles 12:5

['ElʿÛzay](../../strongs/h/h498.md), and [Yᵊrêmôṯ](../../strongs/h/h3406.md), and [BaʿAlyâ](../../strongs/h/h1183.md), and [Šᵊmaryâ](../../strongs/h/h8114.md), and [Šᵊp̄Aṭyâ](../../strongs/h/h8203.md) the [Ḥărûp̄î](../../strongs/h/h2741.md),

<a name="1chronicles_12_6"></a>1Chronicles 12:6

['Elqānâ](../../strongs/h/h511.md), and [Yiššîyâ](../../strongs/h/h3449.md), and [ʿĂzar'Ēl](../../strongs/h/h5832.md), and [YôʿEzer](../../strongs/h/h3134.md), and [YāšāḇʿĀm](../../strongs/h/h3434.md), the [Qārḥî](../../strongs/h/h7145.md),

<a name="1chronicles_12_7"></a>1Chronicles 12:7

And [YôʿĒ'Lâ](../../strongs/h/h3132.md), and [Zᵊḇaḏyâ](../../strongs/h/h2069.md), the [ben](../../strongs/h/h1121.md) of [Yᵊrōḥām](../../strongs/h/h3395.md) of [Gᵊḏōr](../../strongs/h/h1446.md).

<a name="1chronicles_12_8"></a>1Chronicles 12:8

And of the [Gāḏî](../../strongs/h/h1425.md) there [bāḏal](../../strongs/h/h914.md) themselves unto [Dāviḏ](../../strongs/h/h1732.md) into the [mᵊṣāḏ](../../strongs/h/h4679.md) to the [midbar](../../strongs/h/h4057.md) [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), and ['enowsh](../../strongs/h/h582.md) of [tsaba'](../../strongs/h/h6635.md) fit for the [milḥāmâ](../../strongs/h/h4421.md), that could ['arak](../../strongs/h/h6186.md) [tsinnah](../../strongs/h/h6793.md) and [rōmaḥ](../../strongs/h/h7420.md), whose [paniym](../../strongs/h/h6440.md) were like the [paniym](../../strongs/h/h6440.md) of ['ariy](../../strongs/h/h738.md), and were as [māhar](../../strongs/h/h4116.md) as the [ṣᵊḇî](../../strongs/h/h6643.md) upon the [har](../../strongs/h/h2022.md);

<a name="1chronicles_12_9"></a>1Chronicles 12:9

[ʿĒzer](../../strongs/h/h5829.md) the [ro'sh](../../strongs/h/h7218.md), [ʿŌḇaḏyâ](../../strongs/h/h5662.md) the second, ['Ĕlî'āḇ](../../strongs/h/h446.md) the third,

<a name="1chronicles_12_10"></a>1Chronicles 12:10

[Mišmannâ](../../strongs/h/h4925.md) the fourth, [Yirmᵊyâ](../../strongs/h/h3414.md) the fifth,

<a name="1chronicles_12_11"></a>1Chronicles 12:11

[ʿAtay](../../strongs/h/h6262.md) the sixth, ['Ĕlî'Ēl](../../strongs/h/h447.md) the seventh,

<a name="1chronicles_12_12"></a>1Chronicles 12:12

[Yôḥānān](../../strongs/h/h3110.md) the eighth, ['Elzāḇāḏ](../../strongs/h/h443.md) the ninth,

<a name="1chronicles_12_13"></a>1Chronicles 12:13

[Yirmᵊyâ](../../strongs/h/h3414.md) the tenth, [Maḵbannay](../../strongs/h/h4344.md) the eleventh .

<a name="1chronicles_12_14"></a>1Chronicles 12:14

These were of the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md), [ro'sh](../../strongs/h/h7218.md) of the [tsaba'](../../strongs/h/h6635.md): one of the [qāṭān](../../strongs/h/h6996.md) was over an hundred, and the [gadowl](../../strongs/h/h1419.md) over a thousand.

<a name="1chronicles_12_15"></a>1Chronicles 12:15

These are they that ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) in the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md), when it had [mālā'](../../strongs/h/h4390.md) all his [gāḏâ](../../strongs/h/h1415.md) h1428; and they put to [bāraḥ](../../strongs/h/h1272.md) all them of the [ʿēmeq](../../strongs/h/h6010.md), both toward the [mizrach](../../strongs/h/h4217.md), and toward the [ma'arab](../../strongs/h/h4628.md).

<a name="1chronicles_12_16"></a>1Chronicles 12:16

And there [bow'](../../strongs/h/h935.md) of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) and [Yehuwdah](../../strongs/h/h3063.md) to the [mᵊṣāḏ](../../strongs/h/h4679.md) unto [Dāviḏ](../../strongs/h/h1732.md).

<a name="1chronicles_12_17"></a>1Chronicles 12:17

And [Dāviḏ](../../strongs/h/h1732.md) [yāṣā'](../../strongs/h/h3318.md) to [paniym](../../strongs/h/h6440.md) them, and ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto them, If ye be [bow'](../../strongs/h/h935.md) [shalowm](../../strongs/h/h7965.md) unto me to [ʿāzar](../../strongs/h/h5826.md) me, mine [lebab](../../strongs/h/h3824.md) shall be [yaḥaḏ](../../strongs/h/h3162.md) unto you: but if ye be come to [rāmâ](../../strongs/h/h7411.md) me to mine [tsar](../../strongs/h/h6862.md), seeing there is no [chamac](../../strongs/h/h2555.md) in mine [kaph](../../strongs/h/h3709.md), the ['Elohiym](../../strongs/h/h430.md) of our ['ab](../../strongs/h/h1.md) [ra'ah](../../strongs/h/h7200.md) thereon, and [yakach](../../strongs/h/h3198.md) it.

<a name="1chronicles_12_18"></a>1Chronicles 12:18

Then the [ruwach](../../strongs/h/h7307.md) [labash](../../strongs/h/h3847.md) upon [ʿĂmāśay](../../strongs/h/h6022.md), who was [ro'sh](../../strongs/h/h7218.md) of the [šᵊlōšîm](../../strongs/h/h7970.md) [šālîš](../../strongs/h/h7991.md), Thine are we, [Dāviḏ](../../strongs/h/h1732.md), and on thy side, thou [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md): [shalowm](../../strongs/h/h7965.md), [shalowm](../../strongs/h/h7965.md) be unto thee, and [shalowm](../../strongs/h/h7965.md) be to thine [ʿāzar](../../strongs/h/h5826.md); for thy ['Elohiym](../../strongs/h/h430.md) [ʿāzar](../../strongs/h/h5826.md) thee. Then [Dāviḏ](../../strongs/h/h1732.md) [qāḇal](../../strongs/h/h6901.md) them, and [nathan](../../strongs/h/h5414.md) them [ro'sh](../../strongs/h/h7218.md) of the [gᵊḏûḏ](../../strongs/h/h1416.md).

<a name="1chronicles_12_19"></a>1Chronicles 12:19

And there [naphal](../../strongs/h/h5307.md) some of [Mᵊnaššê](../../strongs/h/h4519.md) to [Dāviḏ](../../strongs/h/h1732.md), when he [bow'](../../strongs/h/h935.md) with the [Pᵊlištî](../../strongs/h/h6430.md) against [Šā'ûl](../../strongs/h/h7586.md) to [milḥāmâ](../../strongs/h/h4421.md): but they [ʿāzar](../../strongs/h/h5826.md) them not: for the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md) upon ['etsah](../../strongs/h/h6098.md) [shalach](../../strongs/h/h7971.md) him away, ['āmar](../../strongs/h/h559.md), He will [naphal](../../strongs/h/h5307.md) to his ['adown](../../strongs/h/h113.md) [Šā'ûl](../../strongs/h/h7586.md) of our [ro'sh](../../strongs/h/h7218.md).

<a name="1chronicles_12_20"></a>1Chronicles 12:20

As he [yālaḵ](../../strongs/h/h3212.md) to [Ṣiqlāḡ](../../strongs/h/h6860.md), there [naphal](../../strongs/h/h5307.md) to him of [Mᵊnaššê](../../strongs/h/h4519.md), [ʿAḏnâ](../../strongs/h/h5734.md), and [Yôzāḇāḏ](../../strongs/h/h3107.md), and [YᵊḏîʿĂ'Ēl](../../strongs/h/h3043.md), and [Mîḵā'ēl](../../strongs/h/h4317.md), and [Yôzāḇāḏ](../../strongs/h/h3107.md), and ['Ĕlîhû](../../strongs/h/h453.md), and [Ṣillᵊṯay](../../strongs/h/h6769.md), [ro'sh](../../strongs/h/h7218.md) of the thousands that were of [Mᵊnaššê](../../strongs/h/h4519.md).

<a name="1chronicles_12_21"></a>1Chronicles 12:21

And they [ʿāzar](../../strongs/h/h5826.md) [Dāviḏ](../../strongs/h/h1732.md) against the [gᵊḏûḏ](../../strongs/h/h1416.md) of the rovers: for they were all [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), and were [śar](../../strongs/h/h8269.md) in the [tsaba'](../../strongs/h/h6635.md).

<a name="1chronicles_12_22"></a>1Chronicles 12:22

For at that [ʿēṯ](../../strongs/h/h6256.md) [yowm](../../strongs/h/h3117.md) by [yowm](../../strongs/h/h3117.md) there [bow'](../../strongs/h/h935.md) to [Dāviḏ](../../strongs/h/h1732.md) to [ʿāzar](../../strongs/h/h5826.md) him, until it was a [gadowl](../../strongs/h/h1419.md) [maḥănê](../../strongs/h/h4264.md), like the [maḥănê](../../strongs/h/h4264.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="1chronicles_12_23"></a>1Chronicles 12:23

And these are the [mispār](../../strongs/h/h4557.md) of the [ro'sh](../../strongs/h/h7218.md) that were ready [chalats](../../strongs/h/h2502.md) to the [tsaba'](../../strongs/h/h6635.md), and [bow'](../../strongs/h/h935.md) to [Dāviḏ](../../strongs/h/h1732.md) to [Ḥeḇrôn](../../strongs/h/h2275.md), to [cabab](../../strongs/h/h5437.md) the [malkuwth](../../strongs/h/h4438.md) of [Šā'ûl](../../strongs/h/h7586.md) to him, according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1chronicles_12_24"></a>1Chronicles 12:24

The [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) that [nasa'](../../strongs/h/h5375.md) [tsinnah](../../strongs/h/h6793.md) and [rōmaḥ](../../strongs/h/h7420.md) were six thousand and eight hundred, ready [chalats](../../strongs/h/h2502.md) to the [tsaba'](../../strongs/h/h6635.md).

<a name="1chronicles_12_25"></a>1Chronicles 12:25

Of the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md), [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md) for the [tsaba'](../../strongs/h/h6635.md), seven thousand and one hundred.

<a name="1chronicles_12_26"></a>1Chronicles 12:26

Of the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md) four thousand and six hundred.

<a name="1chronicles_12_27"></a>1Chronicles 12:27

And [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) was the [nāḡîḏ](../../strongs/h/h5057.md) of the ['Ahărôn](../../strongs/h/h175.md), and with him were three thousand and seven hundred;

<a name="1chronicles_12_28"></a>1Chronicles 12:28

And [Ṣāḏôq](../../strongs/h/h6659.md), a [naʿar](../../strongs/h/h5288.md) [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), and of his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) twenty and two [śar](../../strongs/h/h8269.md).

<a name="1chronicles_12_29"></a>1Chronicles 12:29

And of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md), the ['ach](../../strongs/h/h251.md) of [Šā'ûl](../../strongs/h/h7586.md), three thousand: for hitherto the [marbîṯ](../../strongs/h/h4768.md) part of them had [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of the [bayith](../../strongs/h/h1004.md) of [Šā'ûl](../../strongs/h/h7586.md).

<a name="1chronicles_12_30"></a>1Chronicles 12:30

And of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md) twenty thousand and eight hundred, [gibôr](../../strongs/h/h1368.md) ['enowsh](../../strongs/h/h582.md) of [ḥayil](../../strongs/h/h2428.md), [shem](../../strongs/h/h8034.md) throughout the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md).

<a name="1chronicles_12_31"></a>1Chronicles 12:31

And of the [ḥēṣî](../../strongs/h/h2677.md) [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md) eighteen thousand, which were [nāqaḇ](../../strongs/h/h5344.md) by [shem](../../strongs/h/h8034.md), to [bow'](../../strongs/h/h935.md) and make [Dāviḏ](../../strongs/h/h1732.md) [mālaḵ](../../strongs/h/h4427.md).

<a name="1chronicles_12_32"></a>1Chronicles 12:32

And of the [ben](../../strongs/h/h1121.md) of [Yiśśāśḵār](../../strongs/h/h3485.md), that had [bînâ](../../strongs/h/h998.md) [yada'](../../strongs/h/h3045.md) of the [ʿēṯ](../../strongs/h/h6256.md), to [yada'](../../strongs/h/h3045.md) what [Yisra'el](../../strongs/h/h3478.md) ought to ['asah](../../strongs/h/h6213.md); the [ro'sh](../../strongs/h/h7218.md) of them were two hundred; and all their ['ach](../../strongs/h/h251.md) were at their [peh](../../strongs/h/h6310.md).

<a name="1chronicles_12_33"></a>1Chronicles 12:33

Of [Zᵊḇûlûn](../../strongs/h/h2074.md), such as [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md), ['arak](../../strongs/h/h6186.md) in [milḥāmâ](../../strongs/h/h4421.md), with all [kĕliy](../../strongs/h/h3627.md) of [milḥāmâ](../../strongs/h/h4421.md), fifty thousand, which could [ʿāḏar](../../strongs/h/h5737.md): they were not of [leb](../../strongs/h/h3820.md) [leb](../../strongs/h/h3820.md).

<a name="1chronicles_12_34"></a>1Chronicles 12:34

And of [Nap̄tālî](../../strongs/h/h5321.md) a thousand [śar](../../strongs/h/h8269.md), and with them with [tsinnah](../../strongs/h/h6793.md) and [ḥănîṯ](../../strongs/h/h2595.md) thirty and seven thousand.

<a name="1chronicles_12_35"></a>1Chronicles 12:35

And of the [dānî](../../strongs/h/h1839.md) ['arak](../../strongs/h/h6186.md) in [milḥāmâ](../../strongs/h/h4421.md) twenty and eight thousand and six hundred.

<a name="1chronicles_12_36"></a>1Chronicles 12:36

And of ['Āšēr](../../strongs/h/h836.md), such as [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md), ['arak](../../strongs/h/h6186.md) in [milḥāmâ](../../strongs/h/h4421.md), forty thousand.

<a name="1chronicles_12_37"></a>1Chronicles 12:37

And on the other [ʿēḇer](../../strongs/h/h5676.md) of [Yardēn](../../strongs/h/h3383.md), of the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md), and the [Gāḏî](../../strongs/h/h1425.md), and of the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md), with all manner of [kĕliy](../../strongs/h/h3627.md) of [tsaba'](../../strongs/h/h6635.md) for the [milḥāmâ](../../strongs/h/h4421.md), an hundred and twenty thousand.

<a name="1chronicles_12_38"></a>1Chronicles 12:38

All these ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md), that could [ʿāḏar](../../strongs/h/h5737.md) [maʿărāḵâ](../../strongs/h/h4634.md), [bow'](../../strongs/h/h935.md) with a [šālēm](../../strongs/h/h8003.md) [leb](../../strongs/h/h3820.md) to [Ḥeḇrôn](../../strongs/h/h2275.md), to make [Dāviḏ](../../strongs/h/h1732.md) [mālaḵ](../../strongs/h/h4427.md) over all [Yisra'el](../../strongs/h/h3478.md): and all the [šᵊ'ērîṯ](../../strongs/h/h7611.md) also of [Yisra'el](../../strongs/h/h3478.md) were of one [lebab](../../strongs/h/h3824.md) to make [Dāviḏ](../../strongs/h/h1732.md) [mālaḵ](../../strongs/h/h4427.md).

<a name="1chronicles_12_39"></a>1Chronicles 12:39

And there they were with [Dāviḏ](../../strongs/h/h1732.md) three [yowm](../../strongs/h/h3117.md), ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md): for their ['ach](../../strongs/h/h251.md) had [kuwn](../../strongs/h/h3559.md) for them.

<a name="1chronicles_12_40"></a>1Chronicles 12:40

Moreover they that were [qarowb](../../strongs/h/h7138.md) them, even unto [Yiśśāśḵār](../../strongs/h/h3485.md) and [Zᵊḇûlûn](../../strongs/h/h2074.md) and [Nap̄tālî](../../strongs/h/h5321.md), [bow'](../../strongs/h/h935.md) [lechem](../../strongs/h/h3899.md) on [chamowr](../../strongs/h/h2543.md), and on [gāmāl](../../strongs/h/h1581.md), and on [pereḏ](../../strongs/h/h6505.md), and on [bāqār](../../strongs/h/h1241.md), and [ma'akal](../../strongs/h/h3978.md), [qemaḥ](../../strongs/h/h7058.md), [dᵊḇēlâ](../../strongs/h/h1690.md), and [ṣmvqym](../../strongs/h/h6778.md), and [yayin](../../strongs/h/h3196.md), and [šemen](../../strongs/h/h8081.md), and [bāqār](../../strongs/h/h1241.md), and [tso'n](../../strongs/h/h6629.md) [rōḇ](../../strongs/h/h7230.md): for there was [simchah](../../strongs/h/h8057.md) in [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 11](1chronicles_11.md) - [1Chronicles 13](1chronicles_13.md)