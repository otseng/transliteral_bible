# [1Chronicles 21](https://www.blueletterbible.org/kjv/1chronicles/21)

<a name="1chronicles_21_1"></a>1Chronicles 21:1

And [satan](../../strongs/h/h7854.md) ['amad](../../strongs/h/h5975.md) against [Yisra'el](../../strongs/h/h3478.md), and [sûṯ](../../strongs/h/h5496.md) [Dāviḏ](../../strongs/h/h1732.md) to [mānâ](../../strongs/h/h4487.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_21_2"></a>1Chronicles 21:2

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to [Yô'āḇ](../../strongs/h/h3097.md) and to the [śar](../../strongs/h/h8269.md) of the ['am](../../strongs/h/h5971.md), [yālaḵ](../../strongs/h/h3212.md), [sāp̄ar](../../strongs/h/h5608.md) [Yisra'el](../../strongs/h/h3478.md) from [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md) even to [Dān](../../strongs/h/h1835.md); and [bow'](../../strongs/h/h935.md) the [mispār](../../strongs/h/h4557.md) of them to me, that I may [yada'](../../strongs/h/h3045.md) it.

<a name="1chronicles_21_3"></a>1Chronicles 21:3

And [Yô'āḇ](../../strongs/h/h3097.md) ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) make his ['am](../../strongs/h/h5971.md) an hundred [pa'am](../../strongs/h/h6471.md) so many more as they be: but, my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md), are they not all my ['adown](../../strongs/h/h113.md) ['ebed](../../strongs/h/h5650.md)? why then doth my ['adown](../../strongs/h/h113.md) [bāqaš](../../strongs/h/h1245.md) this thing? why will he be a cause of ['ašmâ](../../strongs/h/h819.md) to [Yisra'el](../../strongs/h/h3478.md)?

<a name="1chronicles_21_4"></a>1Chronicles 21:4

Nevertheless the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) [ḥāzaq](../../strongs/h/h2388.md) against [Yô'āḇ](../../strongs/h/h3097.md). Wherefore [Yô'āḇ](../../strongs/h/h3097.md) [yāṣā'](../../strongs/h/h3318.md), and [halak](../../strongs/h/h1980.md) throughout all [Yisra'el](../../strongs/h/h3478.md), and [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="1chronicles_21_5"></a>1Chronicles 21:5

And [Yô'āḇ](../../strongs/h/h3097.md) [nathan](../../strongs/h/h5414.md) the [mispār](../../strongs/h/h4557.md) of the [mip̄qāḏ](../../strongs/h/h4662.md) of the ['am](../../strongs/h/h5971.md) unto [Dāviḏ](../../strongs/h/h1732.md). And all they of [Yisra'el](../../strongs/h/h3478.md) were a thousand thousand and an hundred thousand ['iysh](../../strongs/h/h376.md) that [šālap̄](../../strongs/h/h8025.md) [chereb](../../strongs/h/h2719.md): and [Yehuwdah](../../strongs/h/h3063.md) was four hundred threescore and ten thousand ['iysh](../../strongs/h/h376.md) that [šālap̄](../../strongs/h/h8025.md) [chereb](../../strongs/h/h2719.md).

<a name="1chronicles_21_6"></a>1Chronicles 21:6

But [Lēvî](../../strongs/h/h3878.md) and [Binyāmîn](../../strongs/h/h1144.md) [paqad](../../strongs/h/h6485.md) he not [tavek](../../strongs/h/h8432.md) them: for the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) was [ta'ab](../../strongs/h/h8581.md) to [Yô'āḇ](../../strongs/h/h3097.md).

<a name="1chronicles_21_7"></a>1Chronicles 21:7

And ['Elohiym](../../strongs/h/h430.md) was [yāraʿ](../../strongs/h/h3415.md) ['ayin](../../strongs/h/h5869.md) with this [dabar](../../strongs/h/h1697.md); therefore he [nakah](../../strongs/h/h5221.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_21_8"></a>1Chronicles 21:8

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto ['Elohiym](../../strongs/h/h430.md), I have [chata'](../../strongs/h/h2398.md) [me'od](../../strongs/h/h3966.md), because I have ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md): but now, I beseech thee, do ['abar](../../strongs/h/h5674.md) the ['avon](../../strongs/h/h5771.md) of thy ['ebed](../../strongs/h/h5650.md); for I have done [me'od](../../strongs/h/h3966.md) [sāḵal](../../strongs/h/h5528.md).

<a name="1chronicles_21_9"></a>1Chronicles 21:9

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Gāḏ](../../strongs/h/h1410.md), [Dāviḏ](../../strongs/h/h1732.md) [ḥōzê](../../strongs/h/h2374.md), ['āmar](../../strongs/h/h559.md),

<a name="1chronicles_21_10"></a>1Chronicles 21:10

[yālaḵ](../../strongs/h/h3212.md) and [dabar](../../strongs/h/h1696.md) [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), I [natah](../../strongs/h/h5186.md) thee three things: [bāḥar](../../strongs/h/h977.md) thee one of them, that I may ['asah](../../strongs/h/h6213.md) it unto thee.

<a name="1chronicles_21_11"></a>1Chronicles 21:11

So [Gāḏ](../../strongs/h/h1410.md) [bow'](../../strongs/h/h935.md) to [Dāviḏ](../../strongs/h/h1732.md), and ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), [qāḇal](../../strongs/h/h6901.md) thee

<a name="1chronicles_21_12"></a>1Chronicles 21:12

Either three [šānâ](../../strongs/h/h8141.md) [rāʿāḇ](../../strongs/h/h7458.md); or three [ḥōḏeš](../../strongs/h/h2320.md) to be [sāp̄â](../../strongs/h/h5595.md) [paniym](../../strongs/h/h6440.md) thy [tsar](../../strongs/h/h6862.md), while that the [chereb](../../strongs/h/h2719.md) of thine ['oyeb](../../strongs/h/h341.md) [nāśaḡ](../../strongs/h/h5381.md) thee; or else three [yowm](../../strongs/h/h3117.md) the [chereb](../../strongs/h/h2719.md) of [Yĕhovah](../../strongs/h/h3068.md), even the [deḇer](../../strongs/h/h1698.md), in the ['erets](../../strongs/h/h776.md), and the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [shachath](../../strongs/h/h7843.md) throughout all the [gᵊḇûl](../../strongs/h/h1366.md) of [Yisra'el](../../strongs/h/h3478.md). Now therefore [ra'ah](../../strongs/h/h7200.md) thyself what [dabar](../../strongs/h/h1697.md) I shall [shuwb](../../strongs/h/h7725.md) to him that [shalach](../../strongs/h/h7971.md) me.

<a name="1chronicles_21_13"></a>1Chronicles 21:13

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto [Gāḏ](../../strongs/h/h1410.md), I am in a [me'od](../../strongs/h/h3966.md) [tsarar](../../strongs/h/h6887.md): let me [naphal](../../strongs/h/h5307.md) now into the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md); for [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md) are his [raḥam](../../strongs/h/h7356.md): but let me not [naphal](../../strongs/h/h5307.md) into the [yad](../../strongs/h/h3027.md) of ['āḏām](../../strongs/h/h120.md).

<a name="1chronicles_21_14"></a>1Chronicles 21:14

So [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) [deḇer](../../strongs/h/h1698.md) upon [Yisra'el](../../strongs/h/h3478.md): and there [naphal](../../strongs/h/h5307.md) of [Yisra'el](../../strongs/h/h3478.md) seventy thousand ['iysh](../../strongs/h/h376.md).

<a name="1chronicles_21_15"></a>1Chronicles 21:15

And ['Elohiym](../../strongs/h/h430.md) [shalach](../../strongs/h/h7971.md) a [mal'ak](../../strongs/h/h4397.md) unto [Yĕruwshalaim](../../strongs/h/h3389.md) to [shachath](../../strongs/h/h7843.md) it: and as he was [shachath](../../strongs/h/h7843.md), [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md), and he [nacham](../../strongs/h/h5162.md) him of the [ra'](../../strongs/h/h7451.md), and ['āmar](../../strongs/h/h559.md) to the [mal'ak](../../strongs/h/h4397.md) that [shachath](../../strongs/h/h7843.md), It is [rab](../../strongs/h/h7227.md), [rāp̄â](../../strongs/h/h7503.md) now thine [yad](../../strongs/h/h3027.md). And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['amad](../../strongs/h/h5975.md) by the [gōren](../../strongs/h/h1637.md) of ['Ārnān](../../strongs/h/h771.md) the [Yᵊḇûsî](../../strongs/h/h2983.md).

<a name="1chronicles_21_16"></a>1Chronicles 21:16

And [Dāviḏ](../../strongs/h/h1732.md) [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md) the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['amad](../../strongs/h/h5975.md) between the ['erets](../../strongs/h/h776.md) and the [shamayim](../../strongs/h/h8064.md), having a [šālap̄](../../strongs/h/h8025.md) [chereb](../../strongs/h/h2719.md) in his [yad](../../strongs/h/h3027.md) [natah](../../strongs/h/h5186.md) over [Yĕruwshalaim](../../strongs/h/h3389.md). Then [Dāviḏ](../../strongs/h/h1732.md) and the [zāqēn](../../strongs/h/h2205.md) of Israel, who were [kāsâ](../../strongs/h/h3680.md) in [śaq](../../strongs/h/h8242.md), [naphal](../../strongs/h/h5307.md) upon their [paniym](../../strongs/h/h6440.md).

<a name="1chronicles_21_17"></a>1Chronicles 21:17

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto ['Elohiym](../../strongs/h/h430.md), Is it not I that ['āmar](../../strongs/h/h559.md) the ['am](../../strongs/h/h5971.md) to be [mānâ](../../strongs/h/h4487.md)? even I it is that have [chata'](../../strongs/h/h2398.md) and done [ra'a'](../../strongs/h/h7489.md) [ra'a'](../../strongs/h/h7489.md); but as for these [tso'n](../../strongs/h/h6629.md), what have they ['asah](../../strongs/h/h6213.md)? let thine [yad](../../strongs/h/h3027.md), I pray thee, [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), be on me, and on my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md); but not on thy ['am](../../strongs/h/h5971.md), that they should be [magēp̄â](../../strongs/h/h4046.md).

<a name="1chronicles_21_18"></a>1Chronicles 21:18

Then the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) [Gāḏ](../../strongs/h/h1410.md) to ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), that [Dāviḏ](../../strongs/h/h1732.md) should [ʿālâ](../../strongs/h/h5927.md), and [quwm](../../strongs/h/h6965.md) a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md) in the [gōren](../../strongs/h/h1637.md) of ['Ārnān](../../strongs/h/h771.md) the [Yᵊḇûsî](../../strongs/h/h2983.md).

<a name="1chronicles_21_19"></a>1Chronicles 21:19

And [Dāviḏ](../../strongs/h/h1732.md) [ʿālâ](../../strongs/h/h5927.md) at the [dabar](../../strongs/h/h1697.md) of [Gāḏ](../../strongs/h/h1410.md), which he [dabar](../../strongs/h/h1696.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1chronicles_21_20"></a>1Chronicles 21:20

And ['Ārnān](../../strongs/h/h771.md) [shuwb](../../strongs/h/h7725.md), and [ra'ah](../../strongs/h/h7200.md) the [mal'ak](../../strongs/h/h4397.md); and his four [ben](../../strongs/h/h1121.md) with him [chaba'](../../strongs/h/h2244.md) themselves. Now ['Ārnān](../../strongs/h/h771.md) was [dûš](../../strongs/h/h1758.md) [ḥiṭṭâ](../../strongs/h/h2406.md).

<a name="1chronicles_21_21"></a>1Chronicles 21:21

And as [Dāviḏ](../../strongs/h/h1732.md) [bow'](../../strongs/h/h935.md) to ['Ārnān](../../strongs/h/h771.md), ['Ārnān](../../strongs/h/h771.md) [nabat](../../strongs/h/h5027.md) and [ra'ah](../../strongs/h/h7200.md) [Dāviḏ](../../strongs/h/h1732.md), and [yāṣā'](../../strongs/h/h3318.md) of the [gōren](../../strongs/h/h1637.md), and [shachah](../../strongs/h/h7812.md) himself to [Dāviḏ](../../strongs/h/h1732.md) with his ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md).

<a name="1chronicles_21_22"></a>1Chronicles 21:22

Then [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to ['Ārnān](../../strongs/h/h771.md), [nathan](../../strongs/h/h5414.md) me the [maqowm](../../strongs/h/h4725.md) of this [gōren](../../strongs/h/h1637.md), that I may [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) therein unto [Yĕhovah](../../strongs/h/h3068.md): thou shalt [nathan](../../strongs/h/h5414.md) it me for the [mālē'](../../strongs/h/h4392.md) [keceph](../../strongs/h/h3701.md): that the [magēp̄â](../../strongs/h/h4046.md) may be [ʿāṣar](../../strongs/h/h6113.md) from the ['am](../../strongs/h/h5971.md).

<a name="1chronicles_21_23"></a>1Chronicles 21:23

And ['Ārnān](../../strongs/h/h771.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), [laqach](../../strongs/h/h3947.md) it to thee, and let my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) ['asah](../../strongs/h/h6213.md) that which is [towb](../../strongs/h/h2896.md) in his ['ayin](../../strongs/h/h5869.md): [ra'ah](../../strongs/h/h7200.md), I [nathan](../../strongs/h/h5414.md) thee the [bāqār](../../strongs/h/h1241.md) also for [ʿōlâ](../../strongs/h/h5930.md), and the [môraḡ](../../strongs/h/h4173.md) for ['ets](../../strongs/h/h6086.md), and the [ḥiṭṭâ](../../strongs/h/h2406.md) for the [minchah](../../strongs/h/h4503.md); I [nathan](../../strongs/h/h5414.md) it all.

<a name="1chronicles_21_24"></a>1Chronicles 21:24

And [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to ['Ārnān](../../strongs/h/h771.md), Nay; but I will [qānâ](../../strongs/h/h7069.md) [qānâ](../../strongs/h/h7069.md) it for the [mālē'](../../strongs/h/h4392.md) [keceph](../../strongs/h/h3701.md): for I will not [nasa'](../../strongs/h/h5375.md) that which is thine for [Yĕhovah](../../strongs/h/h3068.md), nor [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) without [ḥinnām](../../strongs/h/h2600.md).

<a name="1chronicles_21_25"></a>1Chronicles 21:25

So [Dāviḏ](../../strongs/h/h1732.md) [nathan](../../strongs/h/h5414.md) to ['Ārnān](../../strongs/h/h771.md) for the [maqowm](../../strongs/h/h4725.md) six hundred [šeqel](../../strongs/h/h8255.md) of [zāhāḇ](../../strongs/h/h2091.md) by [mišqāl](../../strongs/h/h4948.md).

<a name="1chronicles_21_26"></a>1Chronicles 21:26

And [Dāviḏ](../../strongs/h/h1732.md) [bānâ](../../strongs/h/h1129.md) there a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md), and [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) and [šelem](../../strongs/h/h8002.md), and [qara'](../../strongs/h/h7121.md) upon [Yĕhovah](../../strongs/h/h3068.md); and he ['anah](../../strongs/h/h6030.md) him from [shamayim](../../strongs/h/h8064.md) by ['esh](../../strongs/h/h784.md) upon the [mizbeach](../../strongs/h/h4196.md) of [ʿōlâ](../../strongs/h/h5930.md).

<a name="1chronicles_21_27"></a>1Chronicles 21:27

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) the [mal'ak](../../strongs/h/h4397.md); and he put [shuwb](../../strongs/h/h7725.md) his [chereb](../../strongs/h/h2719.md) [shuwb](../../strongs/h/h7725.md) into the [nāḏān](../../strongs/h/h5084.md) thereof.

<a name="1chronicles_21_28"></a>1Chronicles 21:28

At that [ʿēṯ](../../strongs/h/h6256.md) when [Dāviḏ](../../strongs/h/h1732.md) [ra'ah](../../strongs/h/h7200.md) that [Yĕhovah](../../strongs/h/h3068.md) had ['anah](../../strongs/h/h6030.md) him in the [gōren](../../strongs/h/h1637.md) of ['Ārnān](../../strongs/h/h771.md) the [Yᵊḇûsî](../../strongs/h/h2983.md), then he [zabach](../../strongs/h/h2076.md) there.

<a name="1chronicles_21_29"></a>1Chronicles 21:29

For the [miškān](../../strongs/h/h4908.md) of [Yĕhovah](../../strongs/h/h3068.md), which [Mōshe](../../strongs/h/h4872.md) ['asah](../../strongs/h/h6213.md) in the [midbar](../../strongs/h/h4057.md), and the [mizbeach](../../strongs/h/h4196.md) of the [ʿōlâ](../../strongs/h/h5930.md), were at that [ʿēṯ](../../strongs/h/h6256.md) in the [bāmâ](../../strongs/h/h1116.md) at [Giḇʿôn](../../strongs/h/h1391.md).

<a name="1chronicles_21_30"></a>1Chronicles 21:30

But [Dāviḏ](../../strongs/h/h1732.md) [yakol](../../strongs/h/h3201.md) not [yālaḵ](../../strongs/h/h3212.md) [paniym](../../strongs/h/h6440.md) it to [darash](../../strongs/h/h1875.md) of ['Elohiym](../../strongs/h/h430.md): for he was [ba'ath](../../strongs/h/h1204.md) [paniym](../../strongs/h/h6440.md) of the [chereb](../../strongs/h/h2719.md) of the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 20](1chronicles_20.md) - [1Chronicles 22](1chronicles_22.md)