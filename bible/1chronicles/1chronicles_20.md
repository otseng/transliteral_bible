# [1Chronicles 20](https://www.blueletterbible.org/kjv/1chronicles/20)

<a name="1chronicles_20_1"></a>1Chronicles 20:1

And it came to pass, that [ʿēṯ](../../strongs/h/h6256.md) the [šānâ](../../strongs/h/h8141.md) was [tᵊšûḇâ](../../strongs/h/h8666.md), at the [ʿēṯ](../../strongs/h/h6256.md) that [melek](../../strongs/h/h4428.md) [yāṣā'](../../strongs/h/h3318.md), [Yô'āḇ](../../strongs/h/h3097.md) led [nāhaḡ](../../strongs/h/h5090.md) the [ḥayil](../../strongs/h/h2428.md) of the [tsaba'](../../strongs/h/h6635.md), and [shachath](../../strongs/h/h7843.md) the ['erets](../../strongs/h/h776.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and [bow'](../../strongs/h/h935.md) and [ṣûr](../../strongs/h/h6696.md) [Rabâ](../../strongs/h/h7237.md). But [Dāviḏ](../../strongs/h/h1732.md) [yashab](../../strongs/h/h3427.md) at [Yĕruwshalaim](../../strongs/h/h3389.md). And [Yô'āḇ](../../strongs/h/h3097.md) [nakah](../../strongs/h/h5221.md) [Rabâ](../../strongs/h/h7237.md), and [harac](../../strongs/h/h2040.md) it.

<a name="1chronicles_20_2"></a>1Chronicles 20:2

And [Dāviḏ](../../strongs/h/h1732.md) [laqach](../../strongs/h/h3947.md) the [ʿăṭārâ](../../strongs/h/h5850.md) of their [melek](../../strongs/h/h4428.md) from off his [ro'sh](../../strongs/h/h7218.md), and [māṣā'](../../strongs/h/h4672.md) it to [mišqāl](../../strongs/h/h4948.md) a [kikār](../../strongs/h/h3603.md) of [zāhāḇ](../../strongs/h/h2091.md), and there were [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md) in it; and it was set upon [Dāviḏ](../../strongs/h/h1732.md) [ro'sh](../../strongs/h/h7218.md): and he [yāṣā'](../../strongs/h/h3318.md) also [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md) [šālāl](../../strongs/h/h7998.md) out of the [ʿîr](../../strongs/h/h5892.md).

<a name="1chronicles_20_3"></a>1Chronicles 20:3

And he [yāṣā'](../../strongs/h/h3318.md) the ['am](../../strongs/h/h5971.md) that were in it, and [śûr](../../strongs/h/h7787.md) them with [mᵊḡērâ](../../strongs/h/h4050.md), and with [ḥārîṣ](../../strongs/h/h2757.md) of [barzel](../../strongs/h/h1270.md), and with [mᵊḡērâ](../../strongs/h/h4050.md). Even so ['asah](../../strongs/h/h6213.md) [Dāviḏ](../../strongs/h/h1732.md) with all the [ʿîr](../../strongs/h/h5892.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md). And [Dāviḏ](../../strongs/h/h1732.md) and all the ['am](../../strongs/h/h5971.md) [shuwb](../../strongs/h/h7725.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="1chronicles_20_4"></a>1Chronicles 20:4

And it came to pass ['aḥar](../../strongs/h/h310.md), that there ['amad](../../strongs/h/h5975.md) [milḥāmâ](../../strongs/h/h4421.md) at [Gezer](../../strongs/h/h1507.md) with the [Pᵊlištî](../../strongs/h/h6430.md); at which time [Sibḵay](../../strongs/h/h5444.md) the [Ḥušāṯî](../../strongs/h/h2843.md) [nakah](../../strongs/h/h5221.md) [Sipay](../../strongs/h/h5598.md), that was of the [yālîḏ](../../strongs/h/h3211.md) of the [rᵊp̄ā'îm](../../strongs/h/h7497.md): and they were [kānaʿ](../../strongs/h/h3665.md).

<a name="1chronicles_20_5"></a>1Chronicles 20:5

And there was [milḥāmâ](../../strongs/h/h4421.md) again with the [Pᵊlištî](../../strongs/h/h6430.md); and ['Elḥānān](../../strongs/h/h445.md) the [ben](../../strongs/h/h1121.md) of [YāʿÛr](../../strongs/h/h3265.md) [nakah](../../strongs/h/h5221.md) [Laḥmî](../../strongs/h/h3902.md) the ['ach](../../strongs/h/h251.md) of [Gālyaṯ](../../strongs/h/h1555.md) the [Gitî](../../strongs/h/h1663.md), whose [ḥănîṯ](../../strongs/h/h2595.md) ['ets](../../strongs/h/h6086.md) was like an ['āraḡ](../../strongs/h/h707.md) [mānôr](../../strongs/h/h4500.md).

<a name="1chronicles_20_6"></a>1Chronicles 20:6

And yet again there was [milḥāmâ](../../strongs/h/h4421.md) at [Gaṯ](../../strongs/h/h1661.md), where was an ['iysh](../../strongs/h/h376.md) of great [midâ](../../strongs/h/h4060.md), whose ['etsba'](../../strongs/h/h676.md) and ['etsba'](../../strongs/h/h676.md) were four and twenty, six, and six: and he also was the [yalad](../../strongs/h/h3205.md) of the [rᵊp̄ā'îm](../../strongs/h/h7497.md).

<a name="1chronicles_20_7"></a>1Chronicles 20:7

But when he [ḥārap̄](../../strongs/h/h2778.md) [Yisra'el](../../strongs/h/h3478.md), [Yᵊhônāṯān](../../strongs/h/h3083.md) the [ben](../../strongs/h/h1121.md) of [ŠimʿĀ'](../../strongs/h/h8092.md) [Dāviḏ](../../strongs/h/h1732.md) ['ach](../../strongs/h/h251.md) [nakah](../../strongs/h/h5221.md) him.

<a name="1chronicles_20_8"></a>1Chronicles 20:8

These were [yalad](../../strongs/h/h3205.md) unto the [rᵊp̄ā'îm](../../strongs/h/h7497.md) in [Gaṯ](../../strongs/h/h1661.md); and they [naphal](../../strongs/h/h5307.md) by the [yad](../../strongs/h/h3027.md) of [Dāviḏ](../../strongs/h/h1732.md), and by the [yad](../../strongs/h/h3027.md) of his ['ebed](../../strongs/h/h5650.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 19](1chronicles_19.md) - [1Chronicles 21](1chronicles_21.md)