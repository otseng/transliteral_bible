# [1Chronicles 10](https://www.blueletterbible.org/kjv/1chronicles/10)

<a name="1chronicles_10_1"></a>1Chronicles 10:1

Now the [Pᵊlištî](../../strongs/h/h6430.md) [lāḥam](../../strongs/h/h3898.md) against [Yisra'el](../../strongs/h/h3478.md); and the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [nûs](../../strongs/h/h5127.md) from [paniym](../../strongs/h/h6440.md) the [Pᵊlištî](../../strongs/h/h6430.md), and [naphal](../../strongs/h/h5307.md) [ḥālāl](../../strongs/h/h2491.md) in [har](../../strongs/h/h2022.md) [Gilbōaʿ](../../strongs/h/h1533.md).

<a name="1chronicles_10_2"></a>1Chronicles 10:2

And the [Pᵊlištî](../../strongs/h/h6430.md) [dāḇaq](../../strongs/h/h1692.md) ['aḥar](../../strongs/h/h310.md) [Šā'ûl](../../strongs/h/h7586.md), and ['aḥar](../../strongs/h/h310.md) his [ben](../../strongs/h/h1121.md); and the [Pᵊlištî](../../strongs/h/h6430.md) [nakah](../../strongs/h/h5221.md) [Yônāṯān](../../strongs/h/h3129.md), and ['Ăḇînāḏāḇ](../../strongs/h/h41.md), and [Malkîšûaʿ](../../strongs/h/h4444.md), the [ben](../../strongs/h/h1121.md) of [Šā'ûl](../../strongs/h/h7586.md).

<a name="1chronicles_10_3"></a>1Chronicles 10:3

And the [milḥāmâ](../../strongs/h/h4421.md) went [kabad](../../strongs/h/h3513.md) against [Šā'ûl](../../strongs/h/h7586.md), and the [yārâ](../../strongs/h/h3384.md) [qesheth](../../strongs/h/h7198.md) [māṣā'](../../strongs/h/h4672.md) him, and he was [chuwl](../../strongs/h/h2342.md) of the [yārâ](../../strongs/h/h3384.md).

<a name="1chronicles_10_4"></a>1Chronicles 10:4

Then ['āmar](../../strongs/h/h559.md) [Šā'ûl](../../strongs/h/h7586.md) to his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md), [šālap̄](../../strongs/h/h8025.md) thy [chereb](../../strongs/h/h2719.md), and [dāqar](../../strongs/h/h1856.md) me through therewith; lest these [ʿārēl](../../strongs/h/h6189.md) [bow'](../../strongs/h/h935.md) and [ʿālal](../../strongs/h/h5953.md) me. But his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md) ['āḇâ](../../strongs/h/h14.md) not; for he was [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md). So [Šā'ûl](../../strongs/h/h7586.md) [laqach](../../strongs/h/h3947.md) a [chereb](../../strongs/h/h2719.md), and [naphal](../../strongs/h/h5307.md) upon it.

<a name="1chronicles_10_5"></a>1Chronicles 10:5

And when his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md) [ra'ah](../../strongs/h/h7200.md) that [Šā'ûl](../../strongs/h/h7586.md) was [muwth](../../strongs/h/h4191.md), he [naphal](../../strongs/h/h5307.md) likewise on the [chereb](../../strongs/h/h2719.md), and [muwth](../../strongs/h/h4191.md).

<a name="1chronicles_10_6"></a>1Chronicles 10:6

So [Šā'ûl](../../strongs/h/h7586.md) [muwth](../../strongs/h/h4191.md), and his three [ben](../../strongs/h/h1121.md), and all his [bayith](../../strongs/h/h1004.md) [muwth](../../strongs/h/h4191.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="1chronicles_10_7"></a>1Chronicles 10:7

And when all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) that were in the [ʿēmeq](../../strongs/h/h6010.md) [ra'ah](../../strongs/h/h7200.md) that they [nûs](../../strongs/h/h5127.md), and that [Šā'ûl](../../strongs/h/h7586.md) and his [ben](../../strongs/h/h1121.md) were [muwth](../../strongs/h/h4191.md), then they ['azab](../../strongs/h/h5800.md) their [ʿîr](../../strongs/h/h5892.md), and [nûs](../../strongs/h/h5127.md): and the [Pᵊlištî](../../strongs/h/h6430.md) [bow'](../../strongs/h/h935.md) and [yashab](../../strongs/h/h3427.md) in them.

<a name="1chronicles_10_8"></a>1Chronicles 10:8

And it came to pass on the [māḥŏrāṯ](../../strongs/h/h4283.md), when the [Pᵊlištî](../../strongs/h/h6430.md) [bow'](../../strongs/h/h935.md) to [pāšaṭ](../../strongs/h/h6584.md) the [ḥālāl](../../strongs/h/h2491.md), that they [māṣā'](../../strongs/h/h4672.md) [Šā'ûl](../../strongs/h/h7586.md) and his [ben](../../strongs/h/h1121.md) [naphal](../../strongs/h/h5307.md) in [har](../../strongs/h/h2022.md) [Gilbōaʿ](../../strongs/h/h1533.md).

<a name="1chronicles_10_9"></a>1Chronicles 10:9

And when they had [pāšaṭ](../../strongs/h/h6584.md) him, they [nasa'](../../strongs/h/h5375.md) his [ro'sh](../../strongs/h/h7218.md), and his [kĕliy](../../strongs/h/h3627.md), and [shalach](../../strongs/h/h7971.md) into the ['erets](../../strongs/h/h776.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [cabiyb](../../strongs/h/h5439.md), to [bāśar](../../strongs/h/h1319.md) unto their [ʿāṣāḇ](../../strongs/h/h6091.md), and to the ['am](../../strongs/h/h5971.md).

<a name="1chronicles_10_10"></a>1Chronicles 10:10

And they [śûm](../../strongs/h/h7760.md) his [kĕliy](../../strongs/h/h3627.md) in the [bayith](../../strongs/h/h1004.md) of their ['Elohiym](../../strongs/h/h430.md), and [tāqaʿ](../../strongs/h/h8628.md) his [gulgōleṯ](../../strongs/h/h1538.md) in the [bayith](../../strongs/h/h1004.md) of [Dāḡôn](../../strongs/h/h1712.md).

<a name="1chronicles_10_11"></a>1Chronicles 10:11

And when all [Yāḇēš](../../strongs/h/h3003.md) [Gilʿāḏ](../../strongs/h/h1568.md) [shama'](../../strongs/h/h8085.md) all that the [Pᵊlištî](../../strongs/h/h6430.md) had ['asah](../../strongs/h/h6213.md) to [Šā'ûl](../../strongs/h/h7586.md),

<a name="1chronicles_10_12"></a>1Chronicles 10:12

They [quwm](../../strongs/h/h6965.md), all the [ḥayil](../../strongs/h/h2428.md) ['iysh](../../strongs/h/h376.md), and [nasa'](../../strongs/h/h5375.md) the [gûp̄â](../../strongs/h/h1480.md) of [Šā'ûl](../../strongs/h/h7586.md), and the [gûp̄â](../../strongs/h/h1480.md) of his [ben](../../strongs/h/h1121.md), and [bow'](../../strongs/h/h935.md) them to [Yāḇēš](../../strongs/h/h3003.md), and [qāḇar](../../strongs/h/h6912.md) their ['etsem](../../strongs/h/h6106.md) under the ['ēlâ](../../strongs/h/h424.md) in [Yāḇēš](../../strongs/h/h3003.md), and [ṣûm](../../strongs/h/h6684.md) seven [yowm](../../strongs/h/h3117.md).

<a name="1chronicles_10_13"></a>1Chronicles 10:13

So [Šā'ûl](../../strongs/h/h7586.md) [muwth](../../strongs/h/h4191.md) for his [maʿal](../../strongs/h/h4604.md) which he [māʿal](../../strongs/h/h4603.md) against [Yĕhovah](../../strongs/h/h3068.md), even against the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [shamar](../../strongs/h/h8104.md) not, and also for [sha'al](../../strongs/h/h7592.md) counsel of one that had a ['ôḇ](../../strongs/h/h178.md), to [darash](../../strongs/h/h1875.md) of it;

<a name="1chronicles_10_14"></a>1Chronicles 10:14

And [darash](../../strongs/h/h1875.md) not of [Yĕhovah](../../strongs/h/h3068.md): therefore he [muwth](../../strongs/h/h4191.md) him, and [cabab](../../strongs/h/h5437.md) the [mᵊlûḵâ](../../strongs/h/h4410.md) unto [Dāviḏ](../../strongs/h/h1732.md) the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 9](1chronicles_9.md) - [1Chronicles 11](1chronicles_11.md)