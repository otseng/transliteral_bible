# [1Chronicles 4](https://www.blueletterbible.org/kjv/1chronicles/4)

<a name="1chronicles_4_1"></a>1Chronicles 4:1

The [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md); [Pereṣ](../../strongs/h/h6557.md), [Ḥeṣrôn](../../strongs/h/h2696.md), and [Karmî](../../strongs/h/h3756.md), and [Ḥûr](../../strongs/h/h2354.md), and [Šôḇāl](../../strongs/h/h7732.md).

<a name="1chronicles_4_2"></a>1Chronicles 4:2

And [Rᵊ'Āyâ](../../strongs/h/h7211.md) the [ben](../../strongs/h/h1121.md) of [Šôḇāl](../../strongs/h/h7732.md) [yalad](../../strongs/h/h3205.md) [Yaḥaṯ](../../strongs/h/h3189.md); and [Yaḥaṯ](../../strongs/h/h3189.md) [yalad](../../strongs/h/h3205.md) ['Ăḥûmay](../../strongs/h/h267.md), and [Lahaḏ](../../strongs/h/h3855.md). These are the [mišpāḥâ](../../strongs/h/h4940.md) of the [Ṣārʿî](../../strongs/h/h6882.md).

<a name="1chronicles_4_3"></a>1Chronicles 4:3

And these were of the ['ab](../../strongs/h/h1.md) of [ʿÊṭām](../../strongs/h/h5862.md); [YizrᵊʿE'L](../../strongs/h/h3157.md), and [Yišmā'](../../strongs/h/h3457.md), and [Yiḏbāš](../../strongs/h/h3031.md): and the [shem](../../strongs/h/h8034.md) of their ['āḥôṯ](../../strongs/h/h269.md) was [Ṣᵊlelpônî](../../strongs/h/h6753.md):

<a name="1chronicles_4_4"></a>1Chronicles 4:4

And [Pᵊnû'ēl](../../strongs/h/h6439.md) the ['ab](../../strongs/h/h1.md) of [Gᵊḏōr](../../strongs/h/h1446.md), and [ʿĒzer](../../strongs/h/h5829.md) the ['ab](../../strongs/h/h1.md) of [Ḥûšâ](../../strongs/h/h2364.md). These are the [ben](../../strongs/h/h1121.md) of [Ḥûr](../../strongs/h/h2354.md), the [bᵊḵôr](../../strongs/h/h1060.md) of ['Ep̄rāṯ](../../strongs/h/h672.md), the ['ab](../../strongs/h/h1.md) of [Bêṯ leḥem](../../strongs/h/h1035.md).

<a name="1chronicles_4_5"></a>1Chronicles 4:5

And ['Ašḥûr](../../strongs/h/h806.md) the ['ab](../../strongs/h/h1.md) of [Tᵊqôaʿ](../../strongs/h/h8620.md) had two ['ishshah](../../strongs/h/h802.md), [Ḥel'Â](../../strongs/h/h2458.md) and [NaʿĂrâ](../../strongs/h/h5292.md).

<a name="1chronicles_4_6"></a>1Chronicles 4:6

And [NaʿĂrâ](../../strongs/h/h5292.md) [yalad](../../strongs/h/h3205.md) him ['Ăḥuzzām](../../strongs/h/h275.md), and [Ḥēp̄er](../../strongs/h/h2660.md), and [Têmnî](../../strongs/h/h8488.md), and ['Ăḥaštārî](../../strongs/h/h326.md). These were the [ben](../../strongs/h/h1121.md) of [NaʿĂrâ](../../strongs/h/h5292.md).

<a name="1chronicles_4_7"></a>1Chronicles 4:7

And the [ben](../../strongs/h/h1121.md) of [Ḥel'Â](../../strongs/h/h2458.md) were, [Ṣereṯ](../../strongs/h/h6889.md), and [Ṣōḥar](../../strongs/h/h6714.md) [Yiṣḥār](../../strongs/h/h3328.md) , and ['Eṯnan](../../strongs/h/h869.md).

<a name="1chronicles_4_8"></a>1Chronicles 4:8

And [Qôṣ](../../strongs/h/h6976.md) [yalad](../../strongs/h/h3205.md) [ʿĀnûḇ](../../strongs/h/h6036.md), and [Ṣōḇēḇâ](../../strongs/h/h6637.md), and the [mišpāḥâ](../../strongs/h/h4940.md) of ['Ăḥarḥēl](../../strongs/h/h316.md) the [ben](../../strongs/h/h1121.md) of [Hārum](../../strongs/h/h2037.md).

<a name="1chronicles_4_9"></a>1Chronicles 4:9

And [YaʿBēṣ](../../strongs/h/h3258.md) was more [kabad](../../strongs/h/h3513.md) than his ['ach](../../strongs/h/h251.md): and his ['em](../../strongs/h/h517.md) [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [YaʿBēṣ](../../strongs/h/h3258.md), ['āmar](../../strongs/h/h559.md), Because I [yalad](../../strongs/h/h3205.md) him with [ʿōṣeḇ](../../strongs/h/h6090.md).

<a name="1chronicles_4_10"></a>1Chronicles 4:10

And [YaʿBēṣ](../../strongs/h/h3258.md) [qara'](../../strongs/h/h7121.md) on the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), Oh that thou wouldest [barak](../../strongs/h/h1288.md) me [barak](../../strongs/h/h1288.md), and [rabah](../../strongs/h/h7235.md) my [gᵊḇûl](../../strongs/h/h1366.md), and that thine [yad](../../strongs/h/h3027.md) might be with me, and that thou wouldest ['asah](../../strongs/h/h6213.md) me from [ra'](../../strongs/h/h7451.md), that it may not [ʿāṣaḇ](../../strongs/h/h6087.md) me! And ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) him that which he [sha'al](../../strongs/h/h7592.md).

<a name="1chronicles_4_11"></a>1Chronicles 4:11

And [Kᵊlûḇ](../../strongs/h/h3620.md) the ['ach](../../strongs/h/h251.md) of [Šûḥâ](../../strongs/h/h7746.md) [yalad](../../strongs/h/h3205.md) [Mᵊḥîr](../../strongs/h/h4243.md), which was the ['ab](../../strongs/h/h1.md) of ['Eštôn](../../strongs/h/h850.md).

<a name="1chronicles_4_12"></a>1Chronicles 4:12

And ['Eštôn](../../strongs/h/h850.md) [yalad](../../strongs/h/h3205.md) [Bêṯ Rāp̄Ā'](../../strongs/h/h1051.md), and [Pāsēaḥ](../../strongs/h/h6454.md), and [Tᵊḥinnâ](../../strongs/h/h8468.md) the ['ab](../../strongs/h/h1.md) of [ʿÎr Nāḥāš](../../strongs/h/h5904.md). These are the ['enowsh](../../strongs/h/h582.md) of [Rēḵâ](../../strongs/h/h7397.md).

<a name="1chronicles_4_13"></a>1Chronicles 4:13

And the [ben](../../strongs/h/h1121.md) of [Qᵊnaz](../../strongs/h/h7073.md); [ʿĀṯnî'Ēl](../../strongs/h/h6274.md), and [Śᵊrāyâ](../../strongs/h/h8304.md): and the [ben](../../strongs/h/h1121.md) of [ʿĀṯnî'Ēl](../../strongs/h/h6274.md); [Ḥăṯaṯ](../../strongs/h/h2867.md).

<a name="1chronicles_4_14"></a>1Chronicles 4:14

And [Mᵊʿônōṯay](../../strongs/h/h4587.md) [yalad](../../strongs/h/h3205.md) [ʿĀp̄Râ](../../strongs/h/h6084.md): and [Śᵊrāyâ](../../strongs/h/h8304.md) [yalad](../../strongs/h/h3205.md) [Yô'āḇ](../../strongs/h/h3097.md), the ['ab](../../strongs/h/h1.md) of the [gay'](../../strongs/h/h1516.md) of [Ḥărāšîm](../../strongs/h/h2798.md); for they were [ḥereš](../../strongs/h/h2791.md).

<a name="1chronicles_4_15"></a>1Chronicles 4:15

And the [ben](../../strongs/h/h1121.md) of [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md); [ʿÎrû](../../strongs/h/h5900.md), ['Ēlâ](../../strongs/h/h425.md), and [NaʿAm](../../strongs/h/h5277.md): and the [ben](../../strongs/h/h1121.md) of ['Ēlâ](../../strongs/h/h425.md), even [Qᵊnaz](../../strongs/h/h7073.md).

<a name="1chronicles_4_16"></a>1Chronicles 4:16

And the [ben](../../strongs/h/h1121.md) of [Yᵊhallel'Ēl](../../strongs/h/h3094.md); [Zîp̄](../../strongs/h/h2128.md), and [Zîp̄Â](../../strongs/h/h2129.md), [Tîryā'](../../strongs/h/h8493.md), and ['Ăśar'Ēl](../../strongs/h/h840.md).

<a name="1chronicles_4_17"></a>1Chronicles 4:17

And the [ben](../../strongs/h/h1121.md) of [ʿEzrâ](../../strongs/h/h5834.md) were, [Yeṯer](../../strongs/h/h3500.md), and [Mereḏ](../../strongs/h/h4778.md), and [ʿēp̄er](../../strongs/h/h6081.md), and [Yālôn](../../strongs/h/h3210.md): and she [harah](../../strongs/h/h2029.md) [Miryām](../../strongs/h/h4813.md), and [Šammay](../../strongs/h/h8060.md), and [Yišbaḥ](../../strongs/h/h3431.md) the ['ab](../../strongs/h/h1.md) of ['Eštᵊmōaʿ](../../strongs/h/h851.md).

<a name="1chronicles_4_18"></a>1Chronicles 4:18

And his ['ishshah](../../strongs/h/h802.md) [Yᵊhuḏîyâ](../../strongs/h/h3057.md) [yalad](../../strongs/h/h3205.md) [Yereḏ](../../strongs/h/h3382.md) the ['ab](../../strongs/h/h1.md) of [Gᵊḏōr](../../strongs/h/h1446.md), and [Ḥeḇer](../../strongs/h/h2268.md) the ['ab](../../strongs/h/h1.md) of [Śôḵô](../../strongs/h/h7755.md), and [Yᵊqûṯî'Ēl](../../strongs/h/h3354.md) the ['ab](../../strongs/h/h1.md) of [Zānôaḥ](../../strongs/h/h2182.md). And these are the [ben](../../strongs/h/h1121.md) of [Biṯyâ](../../strongs/h/h1332.md) the [bath](../../strongs/h/h1323.md) of [Parʿô](../../strongs/h/h6547.md), which [Mereḏ](../../strongs/h/h4778.md) [laqach](../../strongs/h/h3947.md).

<a name="1chronicles_4_19"></a>1Chronicles 4:19

And the [ben](../../strongs/h/h1121.md) of his ['ishshah](../../strongs/h/h802.md) [Hôḏîyâ](../../strongs/h/h1940.md) the ['āḥôṯ](../../strongs/h/h269.md) of [Naḥam](../../strongs/h/h5163.md), the ['ab](../../strongs/h/h1.md) of [QᵊʿÎlâ](../../strongs/h/h7084.md) the [garmî](../../strongs/h/h1636.md), and ['Eštᵊmōaʿ](../../strongs/h/h851.md) the [Maʿăḵāṯî](../../strongs/h/h4602.md).

<a name="1chronicles_4_20"></a>1Chronicles 4:20

And the [ben](../../strongs/h/h1121.md) of [Šîmôn](../../strongs/h/h7889.md) were, ['Amnôn](../../strongs/h/h550.md), and [Rinnâ](../../strongs/h/h7441.md), [Ben-Ḥānān](../../strongs/h/h1135.md), and [Tûlûn](../../strongs/h/h8436.md). And the [ben](../../strongs/h/h1121.md) of [YišʿÎ](../../strongs/h/h3469.md) were, [Zôḥēṯ](../../strongs/h/h2105.md), and [Ben-Zôḥēṯ](../../strongs/h/h1132.md).

<a name="1chronicles_4_21"></a>1Chronicles 4:21

The [ben](../../strongs/h/h1121.md) of [šēlâ](../../strongs/h/h7956.md) the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) were, [ʿĒr](../../strongs/h/h6147.md) the ['ab](../../strongs/h/h1.md) of [Lēḵâ](../../strongs/h/h3922.md), and [LaʿDâ](../../strongs/h/h3935.md) the ['ab](../../strongs/h/h1.md) of [Mārē'Šâ](../../strongs/h/h4762.md), and the [mišpāḥâ](../../strongs/h/h4940.md) of the [bayith](../../strongs/h/h1004.md) of them that [ʿăḇōḏâ](../../strongs/h/h5656.md) [bûṣ](../../strongs/h/h948.md), of the [bayith](../../strongs/h/h1004.md) of ['Ašbēaʿ](../../strongs/h/h791.md),

<a name="1chronicles_4_22"></a>1Chronicles 4:22

And [Yôqîm](../../strongs/h/h3137.md), and the ['enowsh](../../strongs/h/h582.md) of [Kōzēḇā'](../../strongs/h/h3578.md), and [Yô'Āš](../../strongs/h/h3101.md), and [Śārāp̄](../../strongs/h/h8315.md), who had the [bāʿal](../../strongs/h/h1166.md) in [Mô'āḇ](../../strongs/h/h4124.md), and [Yāšuḇî Leḥem](../../strongs/h/h3433.md). And these are [ʿatîq](../../strongs/h/h6267.md) [dabar](../../strongs/h/h1697.md).

<a name="1chronicles_4_23"></a>1Chronicles 4:23

These were the [yāṣar](../../strongs/h/h3335.md), and those that [yashab](../../strongs/h/h3427.md) among [nᵊṭiʿîm](../../strongs/h/h5196.md) and [gᵊḏērâ](../../strongs/h/h1448.md): there they [yashab](../../strongs/h/h3427.md) with the [melek](../../strongs/h/h4428.md) for his [mĕla'kah](../../strongs/h/h4399.md).

<a name="1chronicles_4_24"></a>1Chronicles 4:24

The [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md) were, [Nᵊmû'ēl](../../strongs/h/h5241.md), and [yāmîn](../../strongs/h/h3226.md), [Yārîḇ](../../strongs/h/h3402.md), [Zeraḥ](../../strongs/h/h2226.md), and [Šā'ûl](../../strongs/h/h7586.md):

<a name="1chronicles_4_25"></a>1Chronicles 4:25

[Šallûm](../../strongs/h/h7967.md) his [ben](../../strongs/h/h1121.md), [Miḇśām](../../strongs/h/h4017.md) his [ben](../../strongs/h/h1121.md), [Mišmāʿ](../../strongs/h/h4927.md) his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_4_26"></a>1Chronicles 4:26

And the [ben](../../strongs/h/h1121.md) of [Mišmāʿ](../../strongs/h/h4927.md); [Ḥammû'Ēl](../../strongs/h/h2536.md) his [ben](../../strongs/h/h1121.md), [Zakûr](../../strongs/h/h2139.md) his [ben](../../strongs/h/h1121.md), [Šimʿî](../../strongs/h/h8096.md) his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_4_27"></a>1Chronicles 4:27

And [Šimʿî](../../strongs/h/h8096.md) had sixteen [ben](../../strongs/h/h1121.md) and six [bath](../../strongs/h/h1323.md); but his ['ach](../../strongs/h/h251.md) had not [rab](../../strongs/h/h7227.md) [ben](../../strongs/h/h1121.md), neither did all their [mišpāḥâ](../../strongs/h/h4940.md) [rabah](../../strongs/h/h7235.md), like to the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="1chronicles_4_28"></a>1Chronicles 4:28

And they [yashab](../../strongs/h/h3427.md) at [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md), and [Môlāḏâ](../../strongs/h/h4137.md), and [Ḥăṣar ShÛʿĀl](../../strongs/h/h2705.md),

<a name="1chronicles_4_29"></a>1Chronicles 4:29

And at [Bilhâ](../../strongs/h/h1090.md), and at [ʿEṣem](../../strongs/h/h6107.md), and at [Tôlaḏ](../../strongs/h/h8434.md),

<a name="1chronicles_4_30"></a>1Chronicles 4:30

And at [bᵊṯû'ēl](../../strongs/h/h1328.md), and at [Ḥārmâ](../../strongs/h/h2767.md), and at [Ṣiqlāḡ](../../strongs/h/h6860.md),

<a name="1chronicles_4_31"></a>1Chronicles 4:31

And at [Bêṯ-Hammarkāḇôṯ](../../strongs/h/h1024.md), and [Ḥăṣar Sûsîm](../../strongs/h/h2702.md), and at [Ḇêṯ Bir'Î](../../strongs/h/h1011.md), and at [ŠaʿĂrayim](../../strongs/h/h8189.md). These were their [ʿîr](../../strongs/h/h5892.md) unto the [mālaḵ](../../strongs/h/h4427.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="1chronicles_4_32"></a>1Chronicles 4:32

And their [ḥāṣēr](../../strongs/h/h2691.md) were, [ʿÊṭām](../../strongs/h/h5862.md), and [ʿAyin](../../strongs/h/h5871.md), [Rimmôn](../../strongs/h/h7417.md), and [Tōḵen](../../strongs/h/h8507.md), and [ʿĀšān](../../strongs/h/h6228.md), five [ʿîr](../../strongs/h/h5892.md):

<a name="1chronicles_4_33"></a>1Chronicles 4:33

And all their [ḥāṣēr](../../strongs/h/h2691.md) that were [cabiyb](../../strongs/h/h5439.md) the same [ʿîr](../../strongs/h/h5892.md), unto [BaʿAl](../../strongs/h/h1168.md). These were their [môšāḇ](../../strongs/h/h4186.md), and their [yāḥaś](../../strongs/h/h3187.md).

<a name="1chronicles_4_34"></a>1Chronicles 4:34

And [Mᵊšôḇāḇ](../../strongs/h/h4877.md), and [Yamlēḵ](../../strongs/h/h3230.md), and [Yôšâ](../../strongs/h/h3144.md) the [ben](../../strongs/h/h1121.md) of ['Ămaṣyâ](../../strongs/h/h558.md),

<a name="1chronicles_4_35"></a>1Chronicles 4:35

And [Yô'Ēl](../../strongs/h/h3100.md), and [Yêû'](../../strongs/h/h3058.md) the [ben](../../strongs/h/h1121.md) of [Yôšiḇyâ](../../strongs/h/h3143.md), the [ben](../../strongs/h/h1121.md) of [Śᵊrāyâ](../../strongs/h/h8304.md), the [ben](../../strongs/h/h1121.md) of [ʿĂśî'Ēl](../../strongs/h/h6221.md),

<a name="1chronicles_4_36"></a>1Chronicles 4:36

And ['ElyᵊhôʿÊnay](../../strongs/h/h454.md), and [YaʿĂqōḇâ](../../strongs/h/h3291.md), and [YᵊshÔḥāyâ](../../strongs/h/h3439.md), and [ʿĂśāyâ](../../strongs/h/h6222.md), and [ʿĂḏî'Ēl](../../strongs/h/h5717.md), and [Yᵊšîmi'Ēl](../../strongs/h/h3450.md), and [Bᵊnāyâ](../../strongs/h/h1141.md),

<a name="1chronicles_4_37"></a>1Chronicles 4:37

And [Zîzā'](../../strongs/h/h2124.md) the [ben](../../strongs/h/h1121.md) of [Šip̄ʿÎ](../../strongs/h/h8230.md), the [ben](../../strongs/h/h1121.md) of ['Allôn](../../strongs/h/h438.md), the [ben](../../strongs/h/h1121.md) of [Yᵊḏāyâ](../../strongs/h/h3042.md), the [ben](../../strongs/h/h1121.md) of [Šimrî](../../strongs/h/h8113.md), the [ben](../../strongs/h/h1121.md) of [ŠᵊmaʿYâ](../../strongs/h/h8098.md);

<a name="1chronicles_4_38"></a>1Chronicles 4:38

These [bow'](../../strongs/h/h935.md) by their [shem](../../strongs/h/h8034.md) were [nāśî'](../../strongs/h/h5387.md) in their [mišpāḥâ](../../strongs/h/h4940.md): and the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md) [pāraṣ](../../strongs/h/h6555.md) [rōḇ](../../strongs/h/h7230.md).

<a name="1chronicles_4_39"></a>1Chronicles 4:39

And they [yālaḵ](../../strongs/h/h3212.md) to the [māḇô'](../../strongs/h/h3996.md) of [Gᵊḏōr](../../strongs/h/h1446.md), even unto the [mizrach](../../strongs/h/h4217.md) of the [gay'](../../strongs/h/h1516.md), to [bāqaš](../../strongs/h/h1245.md) [mirʿê](../../strongs/h/h4829.md) for their [tso'n](../../strongs/h/h6629.md).

<a name="1chronicles_4_40"></a>1Chronicles 4:40

And they [māṣā'](../../strongs/h/h4672.md) [šāmēn](../../strongs/h/h8082.md) [mirʿê](../../strongs/h/h4829.md) and [towb](../../strongs/h/h2896.md), and the ['erets](../../strongs/h/h776.md) was [rāḥāḇ](../../strongs/h/h7342.md) [yad](../../strongs/h/h3027.md), and [šāqaṭ](../../strongs/h/h8252.md), and [šālēv](../../strongs/h/h7961.md); for they of [Ḥām](../../strongs/h/h2526.md) had [yashab](../../strongs/h/h3427.md) there of [paniym](../../strongs/h/h6440.md).

<a name="1chronicles_4_41"></a>1Chronicles 4:41

And these [kāṯaḇ](../../strongs/h/h3789.md) by [shem](../../strongs/h/h8034.md) [bow'](../../strongs/h/h935.md) in the [yowm](../../strongs/h/h3117.md) of [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and [nakah](../../strongs/h/h5221.md) their ['ohel](../../strongs/h/h168.md), and the [māʿôn](../../strongs/h/h4583.md) that were [māṣā'](../../strongs/h/h4672.md) there, and [ḥāram](../../strongs/h/h2763.md) them utterly unto this [yowm](../../strongs/h/h3117.md), and [yashab](../../strongs/h/h3427.md) in their rooms: because there was [mirʿê](../../strongs/h/h4829.md) there for their [tso'n](../../strongs/h/h6629.md).

<a name="1chronicles_4_42"></a>1Chronicles 4:42

And some of them, even of the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md), five hundred ['enowsh](../../strongs/h/h582.md), [halak](../../strongs/h/h1980.md) to [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md), having for their [ro'sh](../../strongs/h/h7218.md) [Pᵊlaṭyâ](../../strongs/h/h6410.md), and [NᵊʿAryâ](../../strongs/h/h5294.md), and [Rᵊp̄Āyâ](../../strongs/h/h7509.md), and [ʿUzzî'ēl](../../strongs/h/h5816.md), the [ben](../../strongs/h/h1121.md) of [YišʿÎ](../../strongs/h/h3469.md).

<a name="1chronicles_4_43"></a>1Chronicles 4:43

And they [nakah](../../strongs/h/h5221.md) the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the [ʿĂmālēq](../../strongs/h/h6002.md) that were [pᵊlêṭâ](../../strongs/h/h6413.md), and [yashab](../../strongs/h/h3427.md) there unto this [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 3](1chronicles_3.md) - [1Chronicles 5](1chronicles_5.md)