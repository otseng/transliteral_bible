# [1Chronicles 22](https://www.blueletterbible.org/kjv/1chronicles/22)

<a name="1chronicles_22_1"></a>1Chronicles 22:1

Then [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), This is the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), and this is the [mizbeach](../../strongs/h/h4196.md) of the [ʿōlâ](../../strongs/h/h5930.md) for [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_22_2"></a>1Chronicles 22:2

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to [kānas](../../strongs/h/h3664.md) the [ger](../../strongs/h/h1616.md) that were in the ['erets](../../strongs/h/h776.md) of [Yisra'el](../../strongs/h/h3478.md); and he ['amad](../../strongs/h/h5975.md) [ḥāṣaḇ](../../strongs/h/h2672.md) to [ḥāṣaḇ](../../strongs/h/h2672.md) [gāzîṯ](../../strongs/h/h1496.md) ['eben](../../strongs/h/h68.md) to [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="1chronicles_22_3"></a>1Chronicles 22:3

And [Dāviḏ](../../strongs/h/h1732.md) [kuwn](../../strongs/h/h3559.md) [barzel](../../strongs/h/h1270.md) in [rōḇ](../../strongs/h/h7230.md) for the [masmēr](../../strongs/h/h4548.md) for the [deleṯ](../../strongs/h/h1817.md) of the [sha'ar](../../strongs/h/h8179.md), and for the [mᵊḥabrâ](../../strongs/h/h4226.md); and [nᵊḥšeṯ](../../strongs/h/h5178.md) in [rōḇ](../../strongs/h/h7230.md) without [mišqāl](../../strongs/h/h4948.md);

<a name="1chronicles_22_4"></a>1Chronicles 22:4

Also ['erez](../../strongs/h/h730.md) ['ets](../../strongs/h/h6086.md) ['în](../../strongs/h/h369.md) [mispār](../../strongs/h/h4557.md): for the [Ṣîḏōnî](../../strongs/h/h6722.md) and they of [ṣōrî](../../strongs/h/h6876.md) [bow'](../../strongs/h/h935.md) [rōḇ](../../strongs/h/h7230.md) ['erez](../../strongs/h/h730.md) ['ets](../../strongs/h/h6086.md) to [Dāviḏ](../../strongs/h/h1732.md).

<a name="1chronicles_22_5"></a>1Chronicles 22:5

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), [Šᵊlōmô](../../strongs/h/h8010.md) my [ben](../../strongs/h/h1121.md) is [naʿar](../../strongs/h/h5288.md) and [raḵ](../../strongs/h/h7390.md), and the [bayith](../../strongs/h/h1004.md) that is to be [bānâ](../../strongs/h/h1129.md) for [Yĕhovah](../../strongs/h/h3068.md) must be [maʿal](../../strongs/h/h4605.md) [gāḏal](../../strongs/h/h1431.md), of [shem](../../strongs/h/h8034.md) and of [tip̄'ārâ](../../strongs/h/h8597.md) throughout all ['erets](../../strongs/h/h776.md): I will therefore now make [kuwn](../../strongs/h/h3559.md) for it. So [Dāviḏ](../../strongs/h/h1732.md) [kuwn](../../strongs/h/h3559.md) [rōḇ](../../strongs/h/h7230.md) [paniym](../../strongs/h/h6440.md) his [maveth](../../strongs/h/h4194.md).

<a name="1chronicles_22_6"></a>1Chronicles 22:6

Then he [qara'](../../strongs/h/h7121.md) for [Šᵊlōmô](../../strongs/h/h8010.md) his [ben](../../strongs/h/h1121.md), and [tsavah](../../strongs/h/h6680.md) him to [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) for [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_22_7"></a>1Chronicles 22:7

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to [Šᵊlōmô](../../strongs/h/h8010.md), My [ben](../../strongs/h/h1121.md), as for me, it was in my [lebab](../../strongs/h/h3824.md) to [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) unto the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md):

<a name="1chronicles_22_8"></a>1Chronicles 22:8

But the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to me, ['āmar](../../strongs/h/h559.md), Thou hast [šāp̄aḵ](../../strongs/h/h8210.md) [dam](../../strongs/h/h1818.md) [rōḇ](../../strongs/h/h7230.md), and hast ['asah](../../strongs/h/h6213.md) [gadowl](../../strongs/h/h1419.md) [milḥāmâ](../../strongs/h/h4421.md): thou shalt not [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) unto my [shem](../../strongs/h/h8034.md), because thou hast [šāp̄aḵ](../../strongs/h/h8210.md) [rab](../../strongs/h/h7227.md) [dam](../../strongs/h/h1818.md) upon the ['erets](../../strongs/h/h776.md) in my [paniym](../../strongs/h/h6440.md).

<a name="1chronicles_22_9"></a>1Chronicles 22:9

Behold, a [ben](../../strongs/h/h1121.md) shall be [yalad](../../strongs/h/h3205.md) to thee, who shall be an ['iysh](../../strongs/h/h376.md) of [mᵊnûḥâ](../../strongs/h/h4496.md); and I will give him [nuwach](../../strongs/h/h5117.md) from all his ['oyeb](../../strongs/h/h341.md) [cabiyb](../../strongs/h/h5439.md): for his [shem](../../strongs/h/h8034.md) shall be [Šᵊlōmô](../../strongs/h/h8010.md), and I will [nathan](../../strongs/h/h5414.md) [shalowm](../../strongs/h/h7965.md) and [šeqeṭ](../../strongs/h/h8253.md) unto [Yisra'el](../../strongs/h/h3478.md) in his [yowm](../../strongs/h/h3117.md).

<a name="1chronicles_22_10"></a>1Chronicles 22:10

He shall [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) for my [shem](../../strongs/h/h8034.md); and he shall be my [ben](../../strongs/h/h1121.md), and I will be his ['ab](../../strongs/h/h1.md); and I will [kuwn](../../strongs/h/h3559.md) the [kicce'](../../strongs/h/h3678.md) of his [malkuwth](../../strongs/h/h4438.md) over [Yisra'el](../../strongs/h/h3478.md) ['owlam](../../strongs/h/h5769.md).

<a name="1chronicles_22_11"></a>1Chronicles 22:11

Now, my [ben](../../strongs/h/h1121.md), [Yĕhovah](../../strongs/h/h3068.md) be with thee; and [tsalach](../../strongs/h/h6743.md) thou, and [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), as he hath [dabar](../../strongs/h/h1696.md) of thee.

<a name="1chronicles_22_12"></a>1Chronicles 22:12

Only [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) thee [śēḵel](../../strongs/h/h7922.md) and [bînâ](../../strongs/h/h998.md), and give thee [tsavah](../../strongs/h/h6680.md) concerning [Yisra'el](../../strongs/h/h3478.md), that thou mayest [shamar](../../strongs/h/h8104.md) the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="1chronicles_22_13"></a>1Chronicles 22:13

Then shalt thou [tsalach](../../strongs/h/h6743.md), if thou takest [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) the [choq](../../strongs/h/h2706.md) and [mishpat](../../strongs/h/h4941.md) which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md) with concerning [Yisra'el](../../strongs/h/h3478.md): be [ḥāzaq](../../strongs/h/h2388.md), and of ['amats](../../strongs/h/h553.md); [yare'](../../strongs/h/h3372.md) not, nor be [ḥāṯaṯ](../../strongs/h/h2865.md).

<a name="1chronicles_22_14"></a>1Chronicles 22:14

Now, behold, in my ['oniy](../../strongs/h/h6040.md) I have [kuwn](../../strongs/h/h3559.md) for the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) an hundred thousand [kikār](../../strongs/h/h3603.md) of [zāhāḇ](../../strongs/h/h2091.md), and a thousand thousand [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md); and of [nᵊḥšeṯ](../../strongs/h/h5178.md) and [barzel](../../strongs/h/h1270.md) without [mišqāl](../../strongs/h/h4948.md); for it is in [rōḇ](../../strongs/h/h7230.md): ['ets](../../strongs/h/h6086.md) also and ['eben](../../strongs/h/h68.md) have I [kuwn](../../strongs/h/h3559.md); and thou mayest add thereto.

<a name="1chronicles_22_15"></a>1Chronicles 22:15

Moreover there are ['asah](../../strongs/h/h6213.md) [mĕla'kah](../../strongs/h/h4399.md) with thee in [rōḇ](../../strongs/h/h7230.md), [ḥāṣaḇ](../../strongs/h/h2672.md) and [ḥārāš](../../strongs/h/h2796.md) of ['eben](../../strongs/h/h68.md) and ['ets](../../strongs/h/h6086.md), and [ḥāḵām](../../strongs/h/h2450.md) for every manner of [mĕla'kah](../../strongs/h/h4399.md).

<a name="1chronicles_22_16"></a>1Chronicles 22:16

Of the [zāhāḇ](../../strongs/h/h2091.md), the [keceph](../../strongs/h/h3701.md), and the [nᵊḥšeṯ](../../strongs/h/h5178.md), and the [barzel](../../strongs/h/h1270.md), there is no [mispār](../../strongs/h/h4557.md). [quwm](../../strongs/h/h6965.md) therefore, and be ['asah](../../strongs/h/h6213.md), and [Yĕhovah](../../strongs/h/h3068.md) be with thee.

<a name="1chronicles_22_17"></a>1Chronicles 22:17

[Dāviḏ](../../strongs/h/h1732.md) also [tsavah](../../strongs/h/h6680.md) all the [śar](../../strongs/h/h8269.md) of [Yisra'el](../../strongs/h/h3478.md) to [ʿāzar](../../strongs/h/h5826.md) [Šᵊlōmô](../../strongs/h/h8010.md) his [ben](../../strongs/h/h1121.md), saying,

<a name="1chronicles_22_18"></a>1Chronicles 22:18

Is not [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) with you? and hath he not given you [nuwach](../../strongs/h/h5117.md) [cabiyb](../../strongs/h/h5439.md)? for he hath [nathan](../../strongs/h/h5414.md) the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) into mine [yad](../../strongs/h/h3027.md); and the ['erets](../../strongs/h/h776.md) is [kāḇaš](../../strongs/h/h3533.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and [paniym](../../strongs/h/h6440.md) his ['am](../../strongs/h/h5971.md).

<a name="1chronicles_22_19"></a>1Chronicles 22:19

Now [nathan](../../strongs/h/h5414.md) your [lebab](../../strongs/h/h3824.md) and your [nephesh](../../strongs/h/h5315.md) to [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md); [quwm](../../strongs/h/h6965.md) therefore, and [bānâ](../../strongs/h/h1129.md) ye the [miqdash](../../strongs/h/h4720.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), to [bow'](../../strongs/h/h935.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [qodesh](../../strongs/h/h6944.md) [kĕliy](../../strongs/h/h3627.md) of ['Elohiym](../../strongs/h/h430.md), into the [bayith](../../strongs/h/h1004.md) that is to be [bānâ](../../strongs/h/h1129.md) to the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 21](1chronicles_21.md) - [1Chronicles 23](1chronicles_23.md)