# [1Chronicles 24](https://www.blueletterbible.org/kjv/1chronicles/24)

<a name="1chronicles_24_1"></a>1Chronicles 24:1

Now these are the [maḥălōqeṯ](../../strongs/h/h4256.md) of the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md). The [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md); [Nāḏāḇ](../../strongs/h/h5070.md), and ['Ăḇîhû'](../../strongs/h/h30.md), ['Elʿāzār](../../strongs/h/h499.md), and ['Îṯāmār](../../strongs/h/h385.md).

<a name="1chronicles_24_2"></a>1Chronicles 24:2

But [Nāḏāḇ](../../strongs/h/h5070.md) and ['Ăḇîhû'](../../strongs/h/h30.md) [muwth](../../strongs/h/h4191.md) [paniym](../../strongs/h/h6440.md) their ['ab](../../strongs/h/h1.md), and had no [ben](../../strongs/h/h1121.md): therefore ['Elʿāzār](../../strongs/h/h499.md) and ['Îṯāmār](../../strongs/h/h385.md) [kāhan](../../strongs/h/h3547.md).

<a name="1chronicles_24_3"></a>1Chronicles 24:3

And [Dāviḏ](../../strongs/h/h1732.md) [chalaq](../../strongs/h/h2505.md) them, both [Ṣāḏôq](../../strongs/h/h6659.md) of the [ben](../../strongs/h/h1121.md) of ['Elʿāzār](../../strongs/h/h499.md), and ['Ăḥîmeleḵ](../../strongs/h/h288.md) of the [ben](../../strongs/h/h1121.md) of ['Îṯāmār](../../strongs/h/h385.md), according to their [pᵊqudâ](../../strongs/h/h6486.md) in their [ʿăḇōḏâ](../../strongs/h/h5656.md).

<a name="1chronicles_24_4"></a>1Chronicles 24:4

And there were [rab](../../strongs/h/h7227.md) [ro'sh](../../strongs/h/h7218.md) [geḇer](../../strongs/h/h1397.md) [māṣā'](../../strongs/h/h4672.md) of the [ben](../../strongs/h/h1121.md) of ['Elʿāzār](../../strongs/h/h499.md) than of the [ben](../../strongs/h/h1121.md) of ['Îṯāmār](../../strongs/h/h385.md); and thus were they [chalaq](../../strongs/h/h2505.md). Among the [ben](../../strongs/h/h1121.md) of ['Elʿāzār](../../strongs/h/h499.md) there were sixteen [ro'sh](../../strongs/h/h7218.md) of the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), and eight among the [ben](../../strongs/h/h1121.md) of ['Îṯāmār](../../strongs/h/h385.md) according to the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md).

<a name="1chronicles_24_5"></a>1Chronicles 24:5

Thus were they [chalaq](../../strongs/h/h2505.md) by [gôrāl](../../strongs/h/h1486.md), one sort with another; for the [śar](../../strongs/h/h8269.md) of the [qodesh](../../strongs/h/h6944.md), and [śar](../../strongs/h/h8269.md) of ['Elohiym](../../strongs/h/h430.md), were of the [ben](../../strongs/h/h1121.md) of ['Elʿāzār](../../strongs/h/h499.md), and of the [ben](../../strongs/h/h1121.md) of ['Îṯāmār](../../strongs/h/h385.md).

<a name="1chronicles_24_6"></a>1Chronicles 24:6

And [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯan'ēl](../../strongs/h/h5417.md) the [sāp̄ar](../../strongs/h/h5608.md), of the [Lᵊvî](../../strongs/h/h3881.md), [kāṯaḇ](../../strongs/h/h3789.md) them [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md), and the [śar](../../strongs/h/h8269.md), and [Ṣāḏôq](../../strongs/h/h6659.md) the [kōhēn](../../strongs/h/h3548.md), and ['Ăḥîmeleḵ](../../strongs/h/h288.md) the [ben](../../strongs/h/h1121.md) of ['Eḇyāṯār](../../strongs/h/h54.md), and before the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of the [kōhēn](../../strongs/h/h3548.md) and [Lᵊvî](../../strongs/h/h3881.md): one ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) being ['āḥaz](../../strongs/h/h270.md) for ['Elʿāzār](../../strongs/h/h499.md), and ['āḥaz](../../strongs/h/h270.md) for ['Îṯāmār](../../strongs/h/h385.md).

<a name="1chronicles_24_7"></a>1Chronicles 24:7

Now the [ri'šôn](../../strongs/h/h7223.md) [gôrāl](../../strongs/h/h1486.md) [yāṣā'](../../strongs/h/h3318.md) to [Yᵊhôyārîḇ](../../strongs/h/h3080.md), the second to [YᵊḏaʿYâ](../../strongs/h/h3048.md),

<a name="1chronicles_24_8"></a>1Chronicles 24:8

The third to [Ḥārim](../../strongs/h/h2766.md), the fourth to [ŚᵊʿŌrîm](../../strongs/h/h8188.md),

<a name="1chronicles_24_9"></a>1Chronicles 24:9

The fifth to [Malkîyâ](../../strongs/h/h4441.md), the sixth to [Minyāmîn](../../strongs/h/h4326.md),

<a name="1chronicles_24_10"></a>1Chronicles 24:10

The seventh to [Qôṣ](../../strongs/h/h6976.md), the eighth to ['Ăḇîâ](../../strongs/h/h29.md),

<a name="1chronicles_24_11"></a>1Chronicles 24:11

The ninth to [Yēšûaʿ](../../strongs/h/h3442.md), the tenth to [Šᵊḵanyâ](../../strongs/h/h7935.md),

<a name="1chronicles_24_12"></a>1Chronicles 24:12

The eleventh to ['Elyāšîḇ](../../strongs/h/h475.md), the twelfth to [Yāqîm](../../strongs/h/h3356.md),

<a name="1chronicles_24_13"></a>1Chronicles 24:13

The thirteenth to [Ḥupâ](../../strongs/h/h2647.md), the fourteenth to [Yešeḇ'Āḇ](../../strongs/h/h3428.md),

<a name="1chronicles_24_14"></a>1Chronicles 24:14

The fifteenth to [Bilgâ](../../strongs/h/h1083.md), the sixteenth to ['Immēr](../../strongs/h/h564.md),

<a name="1chronicles_24_15"></a>1Chronicles 24:15

The seventeenth to [Ḥēzîr](../../strongs/h/h2387.md), the eighteenth to [Piṣṣēṣ](../../strongs/h/h6483.md),

<a name="1chronicles_24_16"></a>1Chronicles 24:16

The nineteenth to [Pᵊṯaḥyâ](../../strongs/h/h6611.md), the twentieth to [Yᵊḥezqē'L](../../strongs/h/h3168.md),

<a name="1chronicles_24_17"></a>1Chronicles 24:17

The one and twentieth to [Yāḵîn](../../strongs/h/h3199.md), the two and twentieth to [Gāmûl](../../strongs/h/h1577.md),

<a name="1chronicles_24_18"></a>1Chronicles 24:18

The three and twentieth to [Dᵊlāyâ](../../strongs/h/h1806.md), the four and twentieth to [MaʿAzyâ](../../strongs/h/h4590.md).

<a name="1chronicles_24_19"></a>1Chronicles 24:19

These were the [pᵊqudâ](../../strongs/h/h6486.md) of them in their [ʿăḇōḏâ](../../strongs/h/h5656.md) to [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), according to their [mishpat](../../strongs/h/h4941.md), [yad](../../strongs/h/h3027.md) ['Ahărôn](../../strongs/h/h175.md) their ['ab](../../strongs/h/h1.md), as [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) had [tsavah](../../strongs/h/h6680.md) him.

<a name="1chronicles_24_20"></a>1Chronicles 24:20

And the [yāṯar](../../strongs/h/h3498.md) of the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md) were these: Of the [ben](../../strongs/h/h1121.md) of [ʿAmrām](../../strongs/h/h6019.md); [Šᵊḇû'Ēl](../../strongs/h/h7619.md): of the [ben](../../strongs/h/h1121.md) of [Šᵊḇû'Ēl](../../strongs/h/h7619.md); [Yeḥdᵊyâû](../../strongs/h/h3165.md).

<a name="1chronicles_24_21"></a>1Chronicles 24:21

Concerning [Rᵊḥaḇyâ](../../strongs/h/h7345.md): of the [ben](../../strongs/h/h1121.md) of [Rᵊḥaḇyâ](../../strongs/h/h7345.md), the [ro'sh](../../strongs/h/h7218.md) was [Yiššîyâ](../../strongs/h/h3449.md).

<a name="1chronicles_24_22"></a>1Chronicles 24:22

Of the [Yiṣhārî](../../strongs/h/h3325.md); [Śᵊlōmôṯ](../../strongs/h/h8013.md): of the [ben](../../strongs/h/h1121.md) of [Śᵊlōmôṯ](../../strongs/h/h8013.md); [Yaḥaṯ](../../strongs/h/h3189.md).

<a name="1chronicles_24_23"></a>1Chronicles 24:23

And the [ben](../../strongs/h/h1121.md); [Yᵊrîyâ](../../strongs/h/h3404.md) the first, ['Ămaryâ](../../strongs/h/h568.md) the second, [Yaḥăzî'Ēl](../../strongs/h/h3166.md) the third, [YᵊqamʿĀm](../../strongs/h/h3360.md) the fourth.

<a name="1chronicles_24_24"></a>1Chronicles 24:24

Of the [ben](../../strongs/h/h1121.md) of [ʿUzzî'ēl](../../strongs/h/h5816.md); [Mîḵâ](../../strongs/h/h4318.md): of the [ben](../../strongs/h/h1121.md) of [Mîḵâ](../../strongs/h/h4318.md); [Šāmûr](../../strongs/h/h8053.md).

<a name="1chronicles_24_25"></a>1Chronicles 24:25

The ['ach](../../strongs/h/h251.md) of [Mîḵâ](../../strongs/h/h4318.md) was [Yiššîyâ](../../strongs/h/h3449.md): of the [ben](../../strongs/h/h1121.md) of [Yiššîyâ](../../strongs/h/h3449.md); [Zᵊḵaryâ](../../strongs/h/h2148.md).

<a name="1chronicles_24_26"></a>1Chronicles 24:26

The [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md) were [Maḥlî](../../strongs/h/h4249.md) and [Mûšî](../../strongs/h/h4187.md): the [ben](../../strongs/h/h1121.md) of [YaʿĂzîyâû](../../strongs/h/h3269.md); [ben](../../strongs/h/h1121.md).

<a name="1chronicles_24_27"></a>1Chronicles 24:27

The [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md) by [YaʿĂzîyâû](../../strongs/h/h3269.md); [ben](../../strongs/h/h1121.md), and [Šōham](../../strongs/h/h7719.md), and [Zakûr](../../strongs/h/h2139.md), and [ʿIḇrî](../../strongs/h/h5681.md).

<a name="1chronicles_24_28"></a>1Chronicles 24:28

Of [Maḥlî](../../strongs/h/h4249.md) came ['Elʿāzār](../../strongs/h/h499.md), who had no [ben](../../strongs/h/h1121.md).

<a name="1chronicles_24_29"></a>1Chronicles 24:29

Concerning [Qîš](../../strongs/h/h7027.md): the [ben](../../strongs/h/h1121.md) of [Qîš](../../strongs/h/h7027.md) was [Yᵊraḥmᵊ'Ēl](../../strongs/h/h3396.md).

<a name="1chronicles_24_30"></a>1Chronicles 24:30

The [ben](../../strongs/h/h1121.md) also of [Mûšî](../../strongs/h/h4187.md); [Maḥlî](../../strongs/h/h4249.md), and [ʿĒḏer](../../strongs/h/h5740.md), and [Yᵊrêmôṯ](../../strongs/h/h3406.md). These were the [ben](../../strongs/h/h1121.md) of the [Lᵊvî](../../strongs/h/h3881.md) after the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md).

<a name="1chronicles_24_31"></a>1Chronicles 24:31

These likewise [naphal](../../strongs/h/h5307.md) [gôrāl](../../strongs/h/h1486.md) over [ʿummâ](../../strongs/h/h5980.md) their ['ach](../../strongs/h/h251.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) in the [paniym](../../strongs/h/h6440.md) of [Dāviḏ](../../strongs/h/h1732.md) the [melek](../../strongs/h/h4428.md), and [Ṣāḏôq](../../strongs/h/h6659.md), and ['Ăḥîmeleḵ](../../strongs/h/h288.md), and the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of the [kōhēn](../../strongs/h/h3548.md) and [Lᵊvî](../../strongs/h/h3881.md), even the [ro'sh](../../strongs/h/h7218.md) ['ab](../../strongs/h/h1.md) over [ʿummâ](../../strongs/h/h5980.md) their [qāṭān](../../strongs/h/h6996.md) ['ach](../../strongs/h/h251.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 23](1chronicles_23.md) - [1Chronicles 25](1chronicles_25.md)