# [1Chronicles 6](https://www.blueletterbible.org/kjv/1chronicles/6)

<a name="1chronicles_6_1"></a>1Chronicles 6:1

The [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md); [Gēršôn](../../strongs/h/h1648.md), [Qᵊhāṯ](../../strongs/h/h6955.md), and [Mᵊrārî](../../strongs/h/h4847.md).

<a name="1chronicles_6_2"></a>1Chronicles 6:2

And the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md); [ʿAmrām](../../strongs/h/h6019.md), [Yiṣhār](../../strongs/h/h3324.md), and [Ḥeḇrôn](../../strongs/h/h2275.md), and [ʿUzzî'ēl](../../strongs/h/h5816.md).

<a name="1chronicles_6_3"></a>1Chronicles 6:3

And the [ben](../../strongs/h/h1121.md) of [ʿAmrām](../../strongs/h/h6019.md); ['Ahărôn](../../strongs/h/h175.md), and [Mōshe](../../strongs/h/h4872.md), and [Miryām](../../strongs/h/h4813.md). The [ben](../../strongs/h/h1121.md) also of ['Ahărôn](../../strongs/h/h175.md); [Nāḏāḇ](../../strongs/h/h5070.md), and ['Ăḇîhû'](../../strongs/h/h30.md), ['Elʿāzār](../../strongs/h/h499.md), and ['Îṯāmār](../../strongs/h/h385.md).

<a name="1chronicles_6_4"></a>1Chronicles 6:4

['Elʿāzār](../../strongs/h/h499.md) [yalad](../../strongs/h/h3205.md) [Pînḥās](../../strongs/h/h6372.md), [Pînḥās](../../strongs/h/h6372.md) [yalad](../../strongs/h/h3205.md) ['Ăḇîšûaʿ](../../strongs/h/h50.md),

<a name="1chronicles_6_5"></a>1Chronicles 6:5

And ['Ăḇîšûaʿ](../../strongs/h/h50.md) [yalad](../../strongs/h/h3205.md) [Buqqî](../../strongs/h/h1231.md), and [Buqqî](../../strongs/h/h1231.md) [yalad](../../strongs/h/h3205.md) [ʿUzzî](../../strongs/h/h5813.md),

<a name="1chronicles_6_6"></a>1Chronicles 6:6

And [ʿUzzî](../../strongs/h/h5813.md) [yalad](../../strongs/h/h3205.md) [Zᵊraḥyâ](../../strongs/h/h2228.md), and [Zᵊraḥyâ](../../strongs/h/h2228.md) [yalad](../../strongs/h/h3205.md) [Mᵊrāyôṯ](../../strongs/h/h4812.md),

<a name="1chronicles_6_7"></a>1Chronicles 6:7

[Mᵊrāyôṯ](../../strongs/h/h4812.md) [yalad](../../strongs/h/h3205.md) ['Ămaryâ](../../strongs/h/h568.md), and ['Ămaryâ](../../strongs/h/h568.md) [yalad](../../strongs/h/h3205.md) ['Ăḥîṭûḇ](../../strongs/h/h285.md),

<a name="1chronicles_6_8"></a>1Chronicles 6:8

And ['Ăḥîṭûḇ](../../strongs/h/h285.md) [yalad](../../strongs/h/h3205.md) [Ṣāḏôq](../../strongs/h/h6659.md), and [Ṣāḏôq](../../strongs/h/h6659.md) [yalad](../../strongs/h/h3205.md) ['ĂḥîmaʿAṣ](../../strongs/h/h290.md),

<a name="1chronicles_6_9"></a>1Chronicles 6:9

And ['ĂḥîmaʿAṣ](../../strongs/h/h290.md) [yalad](../../strongs/h/h3205.md) [ʿĂzaryâ](../../strongs/h/h5838.md), and [ʿĂzaryâ](../../strongs/h/h5838.md) [yalad](../../strongs/h/h3205.md) [Yôḥānān](../../strongs/h/h3110.md),

<a name="1chronicles_6_10"></a>1Chronicles 6:10

And [Yôḥānān](../../strongs/h/h3110.md) [yalad](../../strongs/h/h3205.md) [ʿĂzaryâ](../../strongs/h/h5838.md), (he it is that [kāhan](../../strongs/h/h3547.md) in the [bayith](../../strongs/h/h1004.md) that [Šᵊlōmô](../../strongs/h/h8010.md) [bānâ](../../strongs/h/h1129.md) in [Yĕruwshalaim](../../strongs/h/h3389.md):)

<a name="1chronicles_6_11"></a>1Chronicles 6:11

And [ʿĂzaryâ](../../strongs/h/h5838.md) [yalad](../../strongs/h/h3205.md) ['Ămaryâ](../../strongs/h/h568.md), and ['Ămaryâ](../../strongs/h/h568.md) [yalad](../../strongs/h/h3205.md) ['Ăḥîṭûḇ](../../strongs/h/h285.md),

<a name="1chronicles_6_12"></a>1Chronicles 6:12

And ['Ăḥîṭûḇ](../../strongs/h/h285.md) [yalad](../../strongs/h/h3205.md) [Ṣāḏôq](../../strongs/h/h6659.md), and [Ṣāḏôq](../../strongs/h/h6659.md) [yalad](../../strongs/h/h3205.md) [Šallûm](../../strongs/h/h7967.md),

<a name="1chronicles_6_13"></a>1Chronicles 6:13

And [Šallûm](../../strongs/h/h7967.md) [yalad](../../strongs/h/h3205.md) [Ḥilqîyâ](../../strongs/h/h2518.md), and [Ḥilqîyâ](../../strongs/h/h2518.md) [yalad](../../strongs/h/h3205.md) [ʿĂzaryâ](../../strongs/h/h5838.md),

<a name="1chronicles_6_14"></a>1Chronicles 6:14

And [ʿĂzaryâ](../../strongs/h/h5838.md) [yalad](../../strongs/h/h3205.md) [Śᵊrāyâ](../../strongs/h/h8304.md), and [Śᵊrāyâ](../../strongs/h/h8304.md) [yalad](../../strongs/h/h3205.md) [Yᵊhôṣāḏāq](../../strongs/h/h3087.md),

<a name="1chronicles_6_15"></a>1Chronicles 6:15

And [Yᵊhôṣāḏāq](../../strongs/h/h3087.md) [halak](../../strongs/h/h1980.md) into captivity, when [Yĕhovah](../../strongs/h/h3068.md) [gālâ](../../strongs/h/h1540.md) [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md) by the [yad](../../strongs/h/h3027.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md).

<a name="1chronicles_6_16"></a>1Chronicles 6:16

The [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md); [Gēršōm](../../strongs/h/h1647.md), [Qᵊhāṯ](../../strongs/h/h6955.md), and [Mᵊrārî](../../strongs/h/h4847.md).

<a name="1chronicles_6_17"></a>1Chronicles 6:17

And these be the [shem](../../strongs/h/h8034.md) of the [ben](../../strongs/h/h1121.md) of [Gēršōm](../../strongs/h/h1647.md); [Liḇnî](../../strongs/h/h3845.md), and [Šimʿî](../../strongs/h/h8096.md).

<a name="1chronicles_6_18"></a>1Chronicles 6:18

And the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md) were, [ʿAmrām](../../strongs/h/h6019.md), and [Yiṣhār](../../strongs/h/h3324.md), and [Ḥeḇrôn](../../strongs/h/h2275.md), and [ʿUzzî'ēl](../../strongs/h/h5816.md).

<a name="1chronicles_6_19"></a>1Chronicles 6:19

The [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md); [Maḥlî](../../strongs/h/h4249.md), and [Mûšî](../../strongs/h/h4187.md). And these are the [mišpāḥâ](../../strongs/h/h4940.md) of the [Lᵊvî](../../strongs/h/h3881.md) according to their ['ab](../../strongs/h/h1.md).

<a name="1chronicles_6_20"></a>1Chronicles 6:20

Of [Gēršōm](../../strongs/h/h1647.md); [Liḇnî](../../strongs/h/h3845.md) his [ben](../../strongs/h/h1121.md), [Yaḥaṯ](../../strongs/h/h3189.md) his [ben](../../strongs/h/h1121.md), [Zimmâ](../../strongs/h/h2155.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_6_21"></a>1Chronicles 6:21

[Yô'āḥ](../../strongs/h/h3098.md) his [ben](../../strongs/h/h1121.md), [ʿIdô](../../strongs/h/h5714.md) his [ben](../../strongs/h/h1121.md), [Zeraḥ](../../strongs/h/h2226.md) his [ben](../../strongs/h/h1121.md), [Yᵊ'Aṯray](../../strongs/h/h2979.md) his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_6_22"></a>1Chronicles 6:22

The [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md); [ʿAmmînāḏāḇ](../../strongs/h/h5992.md) his [ben](../../strongs/h/h1121.md), [Qōraḥ](../../strongs/h/h7141.md) his [ben](../../strongs/h/h1121.md), ['Assîr](../../strongs/h/h617.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_6_23"></a>1Chronicles 6:23

['Elqānâ](../../strongs/h/h511.md) his [ben](../../strongs/h/h1121.md), and ['Eḇyāsāp̄](../../strongs/h/h43.md) his [ben](../../strongs/h/h1121.md), and ['Assîr](../../strongs/h/h617.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_6_24"></a>1Chronicles 6:24

[Taḥaṯ](../../strongs/h/h8480.md) his [ben](../../strongs/h/h1121.md), ['Ûrî'Ēl](../../strongs/h/h222.md) his [ben](../../strongs/h/h1121.md), ['Uzziyah](../../strongs/h/h5818.md) his [ben](../../strongs/h/h1121.md), and [Šā'ûl](../../strongs/h/h7586.md) his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_6_25"></a>1Chronicles 6:25

And the [ben](../../strongs/h/h1121.md) of ['Elqānâ](../../strongs/h/h511.md); [ʿĂmāśay](../../strongs/h/h6022.md), and ['Ăḥîmôṯ](../../strongs/h/h287.md).

<a name="1chronicles_6_26"></a>1Chronicles 6:26

As for ['Elqānâ](../../strongs/h/h511.md): the [ben](../../strongs/h/h1121.md) of ['Elqānâ](../../strongs/h/h511.md); [Ṣûp̄](../../strongs/h/h6689.md) his [ben](../../strongs/h/h1121.md), and [Naḥaṯ](../../strongs/h/h5184.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_6_27"></a>1Chronicles 6:27

['Ĕlî'āḇ](../../strongs/h/h446.md) his [ben](../../strongs/h/h1121.md), [Yᵊrōḥām](../../strongs/h/h3395.md) his [ben](../../strongs/h/h1121.md), ['Elqānâ](../../strongs/h/h511.md) his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_6_28"></a>1Chronicles 6:28

And the [ben](../../strongs/h/h1121.md) of [Šᵊmû'Ēl](../../strongs/h/h8050.md); the [bᵊḵôr](../../strongs/h/h1060.md) [Vašnî](../../strongs/h/h2059.md), and ['Ăḇîâ](../../strongs/h/h29.md).

<a name="1chronicles_6_29"></a>1Chronicles 6:29

The [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md); [Maḥlî](../../strongs/h/h4249.md), [Liḇnî](../../strongs/h/h3845.md) his [ben](../../strongs/h/h1121.md), [Šimʿî](../../strongs/h/h8096.md) his [ben](../../strongs/h/h1121.md), [ʿUzzā'](../../strongs/h/h5798.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_6_30"></a>1Chronicles 6:30

[ŠimʿĀ'](../../strongs/h/h8092.md) his [ben](../../strongs/h/h1121.md), [Ḥagîyâ](../../strongs/h/h2293.md) his [ben](../../strongs/h/h1121.md), [ʿĂśāyâ](../../strongs/h/h6222.md) his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_6_31"></a>1Chronicles 6:31

And these are they whom [Dāviḏ](../../strongs/h/h1732.md) ['amad](../../strongs/h/h5975.md) over the [yad](../../strongs/h/h3027.md) of [šîr](../../strongs/h/h7892.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), after that the ['ārôn](../../strongs/h/h727.md) had [mānôaḥ](../../strongs/h/h4494.md).

<a name="1chronicles_6_32"></a>1Chronicles 6:32

And they [sharath](../../strongs/h/h8334.md) [paniym](../../strongs/h/h6440.md) the [miškān](../../strongs/h/h4908.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) with [šîr](../../strongs/h/h7892.md), until [Šᵊlōmô](../../strongs/h/h8010.md) had [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) in [Yĕruwshalaim](../../strongs/h/h3389.md): and then they ['amad](../../strongs/h/h5975.md) on their [ʿăḇōḏâ](../../strongs/h/h5656.md) according to their [mishpat](../../strongs/h/h4941.md).

<a name="1chronicles_6_33"></a>1Chronicles 6:33

And these are they that ['amad](../../strongs/h/h5975.md) with their [ben](../../strongs/h/h1121.md). Of the [ben](../../strongs/h/h1121.md) of the [Qᵊhāṯî](../../strongs/h/h6956.md): [Hêmān](../../strongs/h/h1968.md) a [shiyr](../../strongs/h/h7891.md), the [ben](../../strongs/h/h1121.md) of [Yô'Ēl](../../strongs/h/h3100.md), the [ben](../../strongs/h/h1121.md) of [Šᵊmû'Ēl](../../strongs/h/h8050.md),

<a name="1chronicles_6_34"></a>1Chronicles 6:34

The [ben](../../strongs/h/h1121.md) of ['Elqānâ](../../strongs/h/h511.md), the [ben](../../strongs/h/h1121.md) of [Yᵊrōḥām](../../strongs/h/h3395.md), the [ben](../../strongs/h/h1121.md) of ['Ĕlî'Ēl](../../strongs/h/h447.md), the [ben](../../strongs/h/h1121.md) of [Tôaḥ](../../strongs/h/h8430.md),

<a name="1chronicles_6_35"></a>1Chronicles 6:35

The [ben](../../strongs/h/h1121.md) of [Ṣûp̄](../../strongs/h/h6689.md), the [ben](../../strongs/h/h1121.md) of ['Elqānâ](../../strongs/h/h511.md), the [ben](../../strongs/h/h1121.md) of [Maḥaṯ](../../strongs/h/h4287.md), the [ben](../../strongs/h/h1121.md) of [ʿĂmāśay](../../strongs/h/h6022.md),

<a name="1chronicles_6_36"></a>1Chronicles 6:36

The [ben](../../strongs/h/h1121.md) of ['Elqānâ](../../strongs/h/h511.md), the [ben](../../strongs/h/h1121.md) of [Yô'Ēl](../../strongs/h/h3100.md), the [ben](../../strongs/h/h1121.md) of [ʿĂzaryâ](../../strongs/h/h5838.md), the [ben](../../strongs/h/h1121.md) of [Ṣᵊp̄Anyâ](../../strongs/h/h6846.md),

<a name="1chronicles_6_37"></a>1Chronicles 6:37

The [ben](../../strongs/h/h1121.md) of [Taḥaṯ](../../strongs/h/h8480.md), the [ben](../../strongs/h/h1121.md) of ['Assîr](../../strongs/h/h617.md), the [ben](../../strongs/h/h1121.md) of ['Eḇyāsāp̄](../../strongs/h/h43.md), the [ben](../../strongs/h/h1121.md) of [Qōraḥ](../../strongs/h/h7141.md),

<a name="1chronicles_6_38"></a>1Chronicles 6:38

The [ben](../../strongs/h/h1121.md) of [Yiṣhār](../../strongs/h/h3324.md), the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md), the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md), the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_6_39"></a>1Chronicles 6:39

And his ['ach](../../strongs/h/h251.md) ['Āsāp̄](../../strongs/h/h623.md), who ['amad](../../strongs/h/h5975.md) on his [yamiyn](../../strongs/h/h3225.md), even ['Āsāp̄](../../strongs/h/h623.md) the [ben](../../strongs/h/h1121.md) of [Bereḵyâ](../../strongs/h/h1296.md), the [ben](../../strongs/h/h1121.md) of [ŠimʿĀ'](../../strongs/h/h8092.md),

<a name="1chronicles_6_40"></a>1Chronicles 6:40

The [ben](../../strongs/h/h1121.md) of [Mîḵā'ēl](../../strongs/h/h4317.md), the [ben](../../strongs/h/h1121.md) of [BaʿĂśêâ](../../strongs/h/h1202.md), the [ben](../../strongs/h/h1121.md) of [Malkîyâ](../../strongs/h/h4441.md),

<a name="1chronicles_6_41"></a>1Chronicles 6:41

The [ben](../../strongs/h/h1121.md) of ['Eṯnî](../../strongs/h/h867.md), the [ben](../../strongs/h/h1121.md) of [Zeraḥ](../../strongs/h/h2226.md), the [ben](../../strongs/h/h1121.md) of [ʿĂḏāyâ](../../strongs/h/h5718.md),

<a name="1chronicles_6_42"></a>1Chronicles 6:42

The [ben](../../strongs/h/h1121.md) of ['Êṯān](../../strongs/h/h387.md), the [ben](../../strongs/h/h1121.md) of [Zimmâ](../../strongs/h/h2155.md), the [ben](../../strongs/h/h1121.md) of [Šimʿî](../../strongs/h/h8096.md),

<a name="1chronicles_6_43"></a>1Chronicles 6:43

The [ben](../../strongs/h/h1121.md) of [Yaḥaṯ](../../strongs/h/h3189.md), the [ben](../../strongs/h/h1121.md) of [Gēršōm](../../strongs/h/h1647.md), the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md).

<a name="1chronicles_6_44"></a>1Chronicles 6:44

And their ['ach](../../strongs/h/h251.md) the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md) on the [śᵊmō'l](../../strongs/h/h8040.md): ['Êṯān](../../strongs/h/h387.md) the [ben](../../strongs/h/h1121.md) of [Qîšî](../../strongs/h/h7029.md), the [ben](../../strongs/h/h1121.md) of [ʿAḇdî](../../strongs/h/h5660.md), the [ben](../../strongs/h/h1121.md) of [Mallûḵ](../../strongs/h/h4409.md),

<a name="1chronicles_6_45"></a>1Chronicles 6:45

The [ben](../../strongs/h/h1121.md) of [Ḥăšaḇyâ](../../strongs/h/h2811.md), the [ben](../../strongs/h/h1121.md) of ['Ămaṣyâ](../../strongs/h/h558.md), the [ben](../../strongs/h/h1121.md) of [Ḥilqîyâ](../../strongs/h/h2518.md),

<a name="1chronicles_6_46"></a>1Chronicles 6:46

The [ben](../../strongs/h/h1121.md) of ['Amṣî](../../strongs/h/h557.md), the [ben](../../strongs/h/h1121.md) of [Bānî](../../strongs/h/h1137.md), the [ben](../../strongs/h/h1121.md) of [Šemer](../../strongs/h/h8106.md),

<a name="1chronicles_6_47"></a>1Chronicles 6:47

The [ben](../../strongs/h/h1121.md) of [Maḥlî](../../strongs/h/h4249.md), the [ben](../../strongs/h/h1121.md) of [Mûšî](../../strongs/h/h4187.md), the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md), the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md).

<a name="1chronicles_6_48"></a>1Chronicles 6:48

Their ['ach](../../strongs/h/h251.md) also the [Lᵊvî](../../strongs/h/h3881.md) were [nathan](../../strongs/h/h5414.md) unto all manner of [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [miškān](../../strongs/h/h4908.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="1chronicles_6_49"></a>1Chronicles 6:49

But ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) [qāṭar](../../strongs/h/h6999.md) upon the [mizbeach](../../strongs/h/h4196.md) of the [ʿōlâ](../../strongs/h/h5930.md), and on the [mizbeach](../../strongs/h/h4196.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md), and were appointed for all the [mĕla'kah](../../strongs/h/h4399.md) of the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md), and to [kāp̄ar](../../strongs/h/h3722.md) for [Yisra'el](../../strongs/h/h3478.md), according to all that [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of ['Elohiym](../../strongs/h/h430.md) had [tsavah](../../strongs/h/h6680.md).

<a name="1chronicles_6_50"></a>1Chronicles 6:50

And these are the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md); ['Elʿāzār](../../strongs/h/h499.md) his [ben](../../strongs/h/h1121.md), [Pînḥās](../../strongs/h/h6372.md) his [ben](../../strongs/h/h1121.md), ['Ăḇîšûaʿ](../../strongs/h/h50.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_6_51"></a>1Chronicles 6:51

[Buqqî](../../strongs/h/h1231.md) his [ben](../../strongs/h/h1121.md), [ʿUzzî](../../strongs/h/h5813.md) his [ben](../../strongs/h/h1121.md), [Zᵊraḥyâ](../../strongs/h/h2228.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_6_52"></a>1Chronicles 6:52

[Mᵊrāyôṯ](../../strongs/h/h4812.md) his [ben](../../strongs/h/h1121.md), ['Ămaryâ](../../strongs/h/h568.md) his [ben](../../strongs/h/h1121.md), ['Ăḥîṭûḇ](../../strongs/h/h285.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_6_53"></a>1Chronicles 6:53

[Ṣāḏôq](../../strongs/h/h6659.md) his [ben](../../strongs/h/h1121.md), ['ĂḥîmaʿAṣ](../../strongs/h/h290.md) his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_6_54"></a>1Chronicles 6:54

Now these are their [môšāḇ](../../strongs/h/h4186.md) throughout their [ṭîrâ](../../strongs/h/h2918.md) in their [gᵊḇûl](../../strongs/h/h1366.md), of the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), of the [mišpāḥâ](../../strongs/h/h4940.md) of the [Qᵊhāṯî](../../strongs/h/h6956.md): for theirs was the [gôrāl](../../strongs/h/h1486.md).

<a name="1chronicles_6_55"></a>1Chronicles 6:55

And they [nathan](../../strongs/h/h5414.md) them [Ḥeḇrôn](../../strongs/h/h2275.md) in the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md), and the [miḡrāš](../../strongs/h/h4054.md) thereof [cabiyb](../../strongs/h/h5439.md) it.

<a name="1chronicles_6_56"></a>1Chronicles 6:56

But the [sadeh](../../strongs/h/h7704.md) of the [ʿîr](../../strongs/h/h5892.md), and the [ḥāṣēr](../../strongs/h/h2691.md) thereof, they [nathan](../../strongs/h/h5414.md) to [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md).

<a name="1chronicles_6_57"></a>1Chronicles 6:57

And to the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) they [nathan](../../strongs/h/h5414.md) the [ʿîr](../../strongs/h/h5892.md), namely, [Ḥeḇrôn](../../strongs/h/h2275.md), of [miqlāṭ](../../strongs/h/h4733.md), and [Liḇnâ](../../strongs/h/h3841.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Yatîr](../../strongs/h/h3492.md), and ['Eštᵊmōaʿ](../../strongs/h/h851.md), with their [miḡrāš](../../strongs/h/h4054.md),

<a name="1chronicles_6_58"></a>1Chronicles 6:58

And [Ḥîlēn](../../strongs/h/h2432.md) with her [miḡrāš](../../strongs/h/h4054.md), [dᵊḇîr](../../strongs/h/h1688.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="1chronicles_6_59"></a>1Chronicles 6:59

And [ʿĀšān](../../strongs/h/h6228.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Bêṯ Šemeš](../../strongs/h/h1053.md) with her [miḡrāš](../../strongs/h/h4054.md):

<a name="1chronicles_6_60"></a>1Chronicles 6:60

And out of the [maṭṭê](../../strongs/h/h4294.md) of [Binyāmîn](../../strongs/h/h1144.md); [Geḇaʿ](../../strongs/h/h1387.md) with her [miḡrāš](../../strongs/h/h4054.md), and [ʿĀlemeṯ](../../strongs/h/h5964.md) with her [miḡrāš](../../strongs/h/h4054.md), and [ʿĂnāṯôṯ](../../strongs/h/h6068.md) with her [miḡrāš](../../strongs/h/h4054.md). All their [ʿîr](../../strongs/h/h5892.md) throughout their [mišpāḥâ](../../strongs/h/h4940.md) were thirteen [ʿîr](../../strongs/h/h5892.md).

<a name="1chronicles_6_61"></a>1Chronicles 6:61

And unto the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md), which were [yāṯar](../../strongs/h/h3498.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of that [maṭṭê](../../strongs/h/h4294.md), were cities given out of the [ḥēṣî](../../strongs/h/h2677.md) [maṭṭê](../../strongs/h/h4294.md), namely, out of the [maḥăṣîṯ](../../strongs/h/h4276.md) tribe of [Mᵊnaššê](../../strongs/h/h4519.md), by [gôrāl](../../strongs/h/h1486.md), ten [ʿîr](../../strongs/h/h5892.md).

<a name="1chronicles_6_62"></a>1Chronicles 6:62

And to the [ben](../../strongs/h/h1121.md) of [Gēršōm](../../strongs/h/h1647.md) throughout their [mišpāḥâ](../../strongs/h/h4940.md) out of the [maṭṭê](../../strongs/h/h4294.md) of [Yiśśāśḵār](../../strongs/h/h3485.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of ['Āšēr](../../strongs/h/h836.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of [Nap̄tālî](../../strongs/h/h5321.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md) in [Bāšān](../../strongs/h/h1316.md), thirteen [ʿîr](../../strongs/h/h5892.md).

<a name="1chronicles_6_63"></a>1Chronicles 6:63

Unto the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md) were given by [gôrāl](../../strongs/h/h1486.md), throughout their [mišpāḥâ](../../strongs/h/h4940.md), out of the [maṭṭê](../../strongs/h/h4294.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of [Gāḏ](../../strongs/h/h1410.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md), twelve [ʿîr](../../strongs/h/h5892.md).

<a name="1chronicles_6_64"></a>1Chronicles 6:64

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nathan](../../strongs/h/h5414.md) to the [Lᵊvî](../../strongs/h/h3881.md) these [ʿîr](../../strongs/h/h5892.md) with their [miḡrāš](../../strongs/h/h4054.md).

<a name="1chronicles_6_65"></a>1Chronicles 6:65

And they [nathan](../../strongs/h/h5414.md) by [gôrāl](../../strongs/h/h1486.md) out of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md), these [ʿîr](../../strongs/h/h5892.md), which are [qara'](../../strongs/h/h7121.md) by their [shem](../../strongs/h/h8034.md).

<a name="1chronicles_6_66"></a>1Chronicles 6:66

And the residue of the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md) had [ʿîr](../../strongs/h/h5892.md) of their [gᵊḇûl](../../strongs/h/h1366.md) out of the [maṭṭê](../../strongs/h/h4294.md) of ['Ep̄rayim](../../strongs/h/h669.md).

<a name="1chronicles_6_67"></a>1Chronicles 6:67

And they [nathan](../../strongs/h/h5414.md) unto them, of the [ʿîr](../../strongs/h/h5892.md) of [miqlāṭ](../../strongs/h/h4733.md), [Šᵊḵem](../../strongs/h/h7927.md) in [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md) with her [miḡrāš](../../strongs/h/h4054.md); they gave also [Gezer](../../strongs/h/h1507.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="1chronicles_6_68"></a>1Chronicles 6:68

And [YāqmᵊʿĀm](../../strongs/h/h3361.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="1chronicles_6_69"></a>1Chronicles 6:69

And ['Ayyālôn](../../strongs/h/h357.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Gaṯ-Rimmôn](../../strongs/h/h1667.md) with her [miḡrāš](../../strongs/h/h4054.md):

<a name="1chronicles_6_70"></a>1Chronicles 6:70

And out of the [maḥăṣîṯ](../../strongs/h/h4276.md) [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md); [ʿānēr](../../strongs/h/h6063.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Bilʿām](../../strongs/h/h1109.md) with her [miḡrāš](../../strongs/h/h4054.md), for the [mišpāḥâ](../../strongs/h/h4940.md) of the [yāṯar](../../strongs/h/h3498.md) of the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md).

<a name="1chronicles_6_71"></a>1Chronicles 6:71

Unto the [ben](../../strongs/h/h1121.md) of [Gēršōm](../../strongs/h/h1647.md) were given out of the [mišpāḥâ](../../strongs/h/h4940.md) of the [ḥēṣî](../../strongs/h/h2677.md) [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md), [Gôlān](../../strongs/h/h1474.md) in [Bāšān](../../strongs/h/h1316.md) with her [miḡrāš](../../strongs/h/h4054.md), and [ʿAštārōṯ](../../strongs/h/h6252.md) with her [miḡrāš](../../strongs/h/h4054.md):

<a name="1chronicles_6_72"></a>1Chronicles 6:72

And out of the [maṭṭê](../../strongs/h/h4294.md) of [Yiśśāśḵār](../../strongs/h/h3485.md); [Qeḏeš](../../strongs/h/h6943.md) with her [miḡrāš](../../strongs/h/h4054.md), [Dāḇraṯ](../../strongs/h/h1705.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="1chronicles_6_73"></a>1Chronicles 6:73

And [Rā'Môṯ](../../strongs/h/h7216.md) with her [miḡrāš](../../strongs/h/h4054.md), and [ʿĀnēm](../../strongs/h/h6046.md) with her [miḡrāš](../../strongs/h/h4054.md):

<a name="1chronicles_6_74"></a>1Chronicles 6:74

And out of the [maṭṭê](../../strongs/h/h4294.md) of ['Āšēr](../../strongs/h/h836.md); [Māšāl](../../strongs/h/h4913.md) with her [miḡrāš](../../strongs/h/h4054.md), and [ʿAḇdôn](../../strongs/h/h5658.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="1chronicles_6_75"></a>1Chronicles 6:75

And [Ḥuqqōq](../../strongs/h/h2712.md) with her [miḡrāš](../../strongs/h/h4054.md), and [rᵊḥōḇ](../../strongs/h/h7340.md) with her [miḡrāš](../../strongs/h/h4054.md):

<a name="1chronicles_6_76"></a>1Chronicles 6:76

And out of the [maṭṭê](../../strongs/h/h4294.md) of [Nap̄tālî](../../strongs/h/h5321.md); [Qeḏeš](../../strongs/h/h6943.md) in [Gālîl](../../strongs/h/h1551.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Ḥammôn](../../strongs/h/h2540.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Qiryāṯayim](../../strongs/h/h7156.md) with her [miḡrāš](../../strongs/h/h4054.md).

<a name="1chronicles_6_77"></a>1Chronicles 6:77

Unto the [yāṯar](../../strongs/h/h3498.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md) out of the [maṭṭê](../../strongs/h/h4294.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md), [Rimmôn](../../strongs/h/h7417.md) with her [miḡrāš](../../strongs/h/h4054.md), [Tāḇôr](../../strongs/h/h8396.md) with her [miḡrāš](../../strongs/h/h4054.md):

<a name="1chronicles_6_78"></a>1Chronicles 6:78

And on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) by [Yᵊrēḥô](../../strongs/h/h3405.md), on the [mizrach](../../strongs/h/h4217.md) of [Yardēn](../../strongs/h/h3383.md), out of the [maṭṭê](../../strongs/h/h4294.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), [Beṣer](../../strongs/h/h1221.md) in the [midbar](../../strongs/h/h4057.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Yahaṣ](../../strongs/h/h3096.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="1chronicles_6_79"></a>1Chronicles 6:79

[Qᵊḏēmôṯ](../../strongs/h/h6932.md) also with her [miḡrāš](../../strongs/h/h4054.md), and [Mēvp̄AʿAṯ](../../strongs/h/h4158.md) with her [miḡrāš](../../strongs/h/h4054.md):

<a name="1chronicles_6_80"></a>1Chronicles 6:80

And out of the [maṭṭê](../../strongs/h/h4294.md) of [Gāḏ](../../strongs/h/h1410.md); [Rā'Môṯ](../../strongs/h/h7216.md) in [Gilʿāḏ](../../strongs/h/h1568.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Maḥănayim](../../strongs/h/h4266.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="1chronicles_6_81"></a>1Chronicles 6:81

And [Hešbôn](../../strongs/h/h2809.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Yaʿzêr](../../strongs/h/h3270.md) with her [miḡrāš](../../strongs/h/h4054.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 5](1chronicles_5.md) - [1Chronicles 7](1chronicles_7.md)