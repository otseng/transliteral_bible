# [1Chronicles 19](https://www.blueletterbible.org/kjv/1chronicles/19)

<a name="1chronicles_19_1"></a>1Chronicles 19:1

Now it came to pass ['aḥar](../../strongs/h/h310.md), that [Nāḥāš](../../strongs/h/h5176.md) the [melek](../../strongs/h/h4428.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [muwth](../../strongs/h/h4191.md), and his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1chronicles_19_2"></a>1Chronicles 19:2

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), I will ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) unto [Ḥānûn](../../strongs/h/h2586.md) the [ben](../../strongs/h/h1121.md) of [Nāḥāš](../../strongs/h/h5176.md), because his ['ab](../../strongs/h/h1.md) ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) to me. And [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to [nacham](../../strongs/h/h5162.md) him concerning his ['ab](../../strongs/h/h1.md). So the ['ebed](../../strongs/h/h5650.md) of [Dāviḏ](../../strongs/h/h1732.md) [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) to [Ḥānûn](../../strongs/h/h2586.md), to [nacham](../../strongs/h/h5162.md) him.

<a name="1chronicles_19_3"></a>1Chronicles 19:3

But the [śar](../../strongs/h/h8269.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) ['āmar](../../strongs/h/h559.md) to [Ḥānûn](../../strongs/h/h2586.md), ['ayin](../../strongs/h/h5869.md) thou that [Dāviḏ](../../strongs/h/h1732.md) doth [kabad](../../strongs/h/h3513.md) thy ['ab](../../strongs/h/h1.md), that he hath [shalach](../../strongs/h/h7971.md) [nacham](../../strongs/h/h5162.md) unto thee? are not his ['ebed](../../strongs/h/h5650.md) [bow'](../../strongs/h/h935.md) unto thee for to [chaqar](../../strongs/h/h2713.md), and to [hāp̄aḵ](../../strongs/h/h2015.md), and to [ragal](../../strongs/h/h7270.md) the ['erets](../../strongs/h/h776.md)?

<a name="1chronicles_19_4"></a>1Chronicles 19:4

Wherefore [Ḥānûn](../../strongs/h/h2586.md) [laqach](../../strongs/h/h3947.md) [Dāviḏ](../../strongs/h/h1732.md) ['ebed](../../strongs/h/h5650.md), and [gālaḥ](../../strongs/h/h1548.md) them, and [karath](../../strongs/h/h3772.md) their [meḏev](../../strongs/h/h4063.md) in the [ḥēṣî](../../strongs/h/h2677.md) by their [mip̄śāʿâ](../../strongs/h/h4667.md), and [shalach](../../strongs/h/h7971.md) them.

<a name="1chronicles_19_5"></a>1Chronicles 19:5

Then there [yālaḵ](../../strongs/h/h3212.md) certain, and [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md) the ['enowsh](../../strongs/h/h582.md). And he [shalach](../../strongs/h/h7971.md) to [qārā'](../../strongs/h/h7125.md) them: for the ['enowsh](../../strongs/h/h582.md) were [me'od](../../strongs/h/h3966.md) [kālam](../../strongs/h/h3637.md). And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), [yashab](../../strongs/h/h3427.md) at [Yᵊrēḥô](../../strongs/h/h3405.md) until your [zāqān](../../strongs/h/h2206.md) be [ṣāmaḥ](../../strongs/h/h6779.md), and then [shuwb](../../strongs/h/h7725.md).

<a name="1chronicles_19_6"></a>1Chronicles 19:6

And when the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [ra'ah](../../strongs/h/h7200.md) that they had made themselves [bā'aš](../../strongs/h/h887.md) to [Dāviḏ](../../strongs/h/h1732.md), [Ḥānûn](../../strongs/h/h2586.md) and the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [shalach](../../strongs/h/h7971.md) a thousand [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md) to [śāḵar](../../strongs/h/h7936.md) them [reḵeḇ](../../strongs/h/h7393.md) and [pārāš](../../strongs/h/h6571.md) out of ['Ăram nahărayim](../../strongs/h/h763.md), and out of ['Ărām](../../strongs/h/h758.md) [Maʿăḵâ](../../strongs/h/h4601.md), and out of [Ṣôḇā'](../../strongs/h/h6678.md).

<a name="1chronicles_19_7"></a>1Chronicles 19:7

So they [śāḵar](../../strongs/h/h7936.md) thirty and two thousand [reḵeḇ](../../strongs/h/h7393.md), and the [melek](../../strongs/h/h4428.md) of [Maʿăḵâ](../../strongs/h/h4601.md) and his ['am](../../strongs/h/h5971.md); who [bow'](../../strongs/h/h935.md) and [ḥānâ](../../strongs/h/h2583.md) [paniym](../../strongs/h/h6440.md) [Mêḏḇā'](../../strongs/h/h4311.md). And the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) gathered themselves ['āsap̄](../../strongs/h/h622.md) from their [ʿîr](../../strongs/h/h5892.md), and [bow'](../../strongs/h/h935.md) to [milḥāmâ](../../strongs/h/h4421.md).

<a name="1chronicles_19_8"></a>1Chronicles 19:8

And when [Dāviḏ](../../strongs/h/h1732.md) [shama'](../../strongs/h/h8085.md) of it, he [shalach](../../strongs/h/h7971.md) [Yô'āḇ](../../strongs/h/h3097.md), and all the [tsaba'](../../strongs/h/h6635.md) of the [gibôr](../../strongs/h/h1368.md).

<a name="1chronicles_19_9"></a>1Chronicles 19:9

And the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [yāṣā'](../../strongs/h/h3318.md), and put the [milḥāmâ](../../strongs/h/h4421.md) in ['arak](../../strongs/h/h6186.md) before the [peṯaḥ](../../strongs/h/h6607.md) of the [ʿîr](../../strongs/h/h5892.md): and the [melek](../../strongs/h/h4428.md) that were [bow'](../../strongs/h/h935.md) were by themselves in the [sadeh](../../strongs/h/h7704.md).

<a name="1chronicles_19_10"></a>1Chronicles 19:10

Now when [Yô'āḇ](../../strongs/h/h3097.md) [ra'ah](../../strongs/h/h7200.md) that the [milḥāmâ](../../strongs/h/h4421.md) was set against him [paniym](../../strongs/h/h6440.md) and ['āḥôr](../../strongs/h/h268.md), he [bāḥar](../../strongs/h/h977.md) of all the [bāḥar](../../strongs/h/h977.md) of [Yisra'el](../../strongs/h/h3478.md), and put them in ['arak](../../strongs/h/h6186.md) [qārā'](../../strongs/h/h7125.md) the ['Ărām](../../strongs/h/h758.md).

<a name="1chronicles_19_11"></a>1Chronicles 19:11

And the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md) he [nathan](../../strongs/h/h5414.md) unto the [yad](../../strongs/h/h3027.md) of ['Ăḇîšay](../../strongs/h/h52.md) his ['ach](../../strongs/h/h251.md), and they set themselves in ['arak](../../strongs/h/h6186.md) [qārā'](../../strongs/h/h7125.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md).

<a name="1chronicles_19_12"></a>1Chronicles 19:12

And he ['āmar](../../strongs/h/h559.md), If the ['Ărām](../../strongs/h/h758.md) be too [ḥāzaq](../../strongs/h/h2388.md) for me, then thou shalt [tᵊšûʿâ](../../strongs/h/h8668.md) me: but if the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) be too [ḥāzaq](../../strongs/h/h2388.md) for thee, then I will [yasha'](../../strongs/h/h3467.md) thee.

<a name="1chronicles_19_13"></a>1Chronicles 19:13

Be of [ḥāzaq](../../strongs/h/h2388.md), and let us behave ourselves [ḥāzaq](../../strongs/h/h2388.md) for our ['am](../../strongs/h/h5971.md), and for the [ʿîr](../../strongs/h/h5892.md) of our ['Elohiym](../../strongs/h/h430.md): and let [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) that which is [towb](../../strongs/h/h2896.md) in his ['ayin](../../strongs/h/h5869.md).

<a name="1chronicles_19_14"></a>1Chronicles 19:14

So [Yô'āḇ](../../strongs/h/h3097.md) and the ['am](../../strongs/h/h5971.md) that were with him [nāḡaš](../../strongs/h/h5066.md) [paniym](../../strongs/h/h6440.md) the ['Ărām](../../strongs/h/h758.md) unto the [milḥāmâ](../../strongs/h/h4421.md); and they [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) him.

<a name="1chronicles_19_15"></a>1Chronicles 19:15

And when the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [ra'ah](../../strongs/h/h7200.md) that the ['Ărām](../../strongs/h/h758.md) were [nûs](../../strongs/h/h5127.md), they likewise [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) ['Ăḇîšay](../../strongs/h/h52.md) his ['ach](../../strongs/h/h251.md), and [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md). Then [Yô'āḇ](../../strongs/h/h3097.md) [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="1chronicles_19_16"></a>1Chronicles 19:16

And when the ['Ărām](../../strongs/h/h758.md) [ra'ah](../../strongs/h/h7200.md) that they were put to the [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) [Yisra'el](../../strongs/h/h3478.md), they [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md), and drew [yāṣā'](../../strongs/h/h3318.md) the ['Ărām](../../strongs/h/h758.md) that were [ʿēḇer](../../strongs/h/h5676.md) the [nāhār](../../strongs/h/h5104.md): and [Šôp̄Āḵ](../../strongs/h/h7780.md) the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) of [HăḏarʿEzer](../../strongs/h/h1928.md) went [paniym](../../strongs/h/h6440.md) them.

<a name="1chronicles_19_17"></a>1Chronicles 19:17

And it was [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md); and he ['āsap̄](../../strongs/h/h622.md) all [Yisra'el](../../strongs/h/h3478.md), and ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md), and [bow'](../../strongs/h/h935.md) upon them, and set in ['arak](../../strongs/h/h6186.md) against them. So when [Dāviḏ](../../strongs/h/h1732.md) had put the [milḥāmâ](../../strongs/h/h4421.md) in ['arak](../../strongs/h/h6186.md) [qārā'](../../strongs/h/h7125.md) the ['Ărām](../../strongs/h/h758.md), they [lāḥam](../../strongs/h/h3898.md) with him.

<a name="1chronicles_19_18"></a>1Chronicles 19:18

But the ['Ărām](../../strongs/h/h758.md) [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) [Yisra'el](../../strongs/h/h3478.md); and [Dāviḏ](../../strongs/h/h1732.md) [harag](../../strongs/h/h2026.md) of the ['Ărām](../../strongs/h/h758.md) seven thousand in [reḵeḇ](../../strongs/h/h7393.md), and forty thousand ['iysh](../../strongs/h/h376.md) [raḡlî](../../strongs/h/h7273.md), and [muwth](../../strongs/h/h4191.md) [Šôp̄Āḵ](../../strongs/h/h7780.md) the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md).

<a name="1chronicles_19_19"></a>1Chronicles 19:19

And when the ['ebed](../../strongs/h/h5650.md) of [HăḏarʿEzer](../../strongs/h/h1928.md) [ra'ah](../../strongs/h/h7200.md) that they were put to the [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) [Yisra'el](../../strongs/h/h3478.md), they made [shalam](../../strongs/h/h7999.md) with [Dāviḏ](../../strongs/h/h1732.md), and became his ['abad](../../strongs/h/h5647.md): neither ['āḇâ](../../strongs/h/h14.md) the ['Ărām](../../strongs/h/h758.md) [yasha'](../../strongs/h/h3467.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) any more.

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 18](1chronicles_18.md) - [1Chronicles 20](1chronicles_20.md)