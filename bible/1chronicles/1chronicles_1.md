# [1Chronicles 1](https://www.blueletterbible.org/kjv/1chronicles/1)

<a name="1chronicles_1_1"></a>1Chronicles 1:1

['Āḏām](../../strongs/h/h121.md), [Šēṯ](../../strongs/h/h8352.md), ['Ĕnôš](../../strongs/h/h583.md),

<a name="1chronicles_1_2"></a>1Chronicles 1:2

[Qênān](../../strongs/h/h7018.md), [Mahălal'ēl](../../strongs/h/h4111.md), [Yereḏ](../../strongs/h/h3382.md),

<a name="1chronicles_1_3"></a>1Chronicles 1:3

[Ḥănôḵ](../../strongs/h/h2585.md), [Mᵊṯûšelaḥ](../../strongs/h/h4968.md), [Lemeḵ](../../strongs/h/h3929.md),

<a name="1chronicles_1_4"></a>1Chronicles 1:4

[Nōaḥ](../../strongs/h/h5146.md), [Šēm](../../strongs/h/h8035.md), [Ḥām](../../strongs/h/h2526.md), and [Yep̄eṯ](../../strongs/h/h3315.md).

<a name="1chronicles_1_5"></a>1Chronicles 1:5

The [ben](../../strongs/h/h1121.md) of [Yep̄eṯ](../../strongs/h/h3315.md); [Gōmer](../../strongs/h/h1586.md), and [Māḡôḡ](../../strongs/h/h4031.md), and [Māḏay](../../strongs/h/h4074.md), and [Yāvān](../../strongs/h/h3120.md), and [Tuḇal](../../strongs/h/h8422.md), and [Mešeḵ](../../strongs/h/h4902.md), and [Tîrās](../../strongs/h/h8494.md).

<a name="1chronicles_1_6"></a>1Chronicles 1:6

And the [ben](../../strongs/h/h1121.md) of [Gōmer](../../strongs/h/h1586.md); ['Aškᵊnaz](../../strongs/h/h813.md), and [Rîp̄aṯ](../../strongs/h/h7384.md), and [Tōḡarmâ](../../strongs/h/h8425.md).

<a name="1chronicles_1_7"></a>1Chronicles 1:7

And the [ben](../../strongs/h/h1121.md) of [Yāvān](../../strongs/h/h3120.md); ['Ĕlîšâ](../../strongs/h/h473.md), and [Taršîš](../../strongs/h/h8659.md), [Kitîm](../../strongs/h/h3794.md), and [Dōḏānîm](../../strongs/h/h1721.md).

<a name="1chronicles_1_8"></a>1Chronicles 1:8

The [ben](../../strongs/h/h1121.md) of [Ḥām](../../strongs/h/h2526.md); [Kûš](../../strongs/h/h3568.md), and [Mitsrayim](../../strongs/h/h4714.md), [Pûṭ](../../strongs/h/h6316.md), and [Kĕna'an](../../strongs/h/h3667.md).

<a name="1chronicles_1_9"></a>1Chronicles 1:9

And the [ben](../../strongs/h/h1121.md) of [Kûš](../../strongs/h/h3568.md); [Sᵊḇā'](../../strongs/h/h5434.md), and [Ḥăvîlâ](../../strongs/h/h2341.md), and [Saḇtā'](../../strongs/h/h5454.md), and [Raʿmâ](../../strongs/h/h7484.md), and [Saḇtᵊḵā'](../../strongs/h/h5455.md). And the [ben](../../strongs/h/h1121.md) of [Raʿmâ](../../strongs/h/h7484.md); [Šᵊḇā'](../../strongs/h/h7614.md), and [Dᵊḏān](../../strongs/h/h1719.md).

<a name="1chronicles_1_10"></a>1Chronicles 1:10

And [Kûš](../../strongs/h/h3568.md) [yalad](../../strongs/h/h3205.md) [Nimrōḏ](../../strongs/h/h5248.md): he [ḥālal](../../strongs/h/h2490.md) to be [gibôr](../../strongs/h/h1368.md) upon the ['erets](../../strongs/h/h776.md).

<a name="1chronicles_1_11"></a>1Chronicles 1:11

And [Mitsrayim](../../strongs/h/h4714.md) [yalad](../../strongs/h/h3205.md) [Lûḏîy](../../strongs/h/h3866.md), and [ʿĂnāmîm](../../strongs/h/h6047.md), and [Lᵊhāḇîm](../../strongs/h/h3853.md), and [Nap̄tuḥîm](../../strongs/h/h5320.md),

<a name="1chronicles_1_12"></a>1Chronicles 1:12

And [Paṯrusî](../../strongs/h/h6625.md), and [Kasluḥîm](../../strongs/h/h3695.md), (of whom [yāṣā'](../../strongs/h/h3318.md) the [Pᵊlištî](../../strongs/h/h6430.md),) and [Kap̄tōrî](../../strongs/h/h3732.md).

<a name="1chronicles_1_13"></a>1Chronicles 1:13

And [Kĕna'an](../../strongs/h/h3667.md) [yalad](../../strongs/h/h3205.md) [Ṣîḏôn](../../strongs/h/h6721.md) his [bᵊḵôr](../../strongs/h/h1060.md), and [Ḥēṯ](../../strongs/h/h2845.md),

<a name="1chronicles_1_14"></a>1Chronicles 1:14

The [Yᵊḇûsî](../../strongs/h/h2983.md) also, and the ['Ĕmōrî](../../strongs/h/h567.md), and the [Girgāšî](../../strongs/h/h1622.md),

<a name="1chronicles_1_15"></a>1Chronicles 1:15

And the [Ḥiûî](../../strongs/h/h2340.md), and the [ʿArqî](../../strongs/h/h6208.md), and the [Sînî](../../strongs/h/h5513.md),

<a name="1chronicles_1_16"></a>1Chronicles 1:16

And the ['Arvāḏî](../../strongs/h/h721.md), and the [Ṣᵊmārî](../../strongs/h/h6786.md), and the [Ḥămāṯî](../../strongs/h/h2577.md).

<a name="1chronicles_1_17"></a>1Chronicles 1:17

The [ben](../../strongs/h/h1121.md) of [Šēm](../../strongs/h/h8035.md); [ʿÊlām](../../strongs/h/h5867.md), and ['Aššûr](../../strongs/h/h804.md), and ['Arpaḵšaḏ](../../strongs/h/h775.md), and [Lûḏ](../../strongs/h/h3865.md), and ['Ărām](../../strongs/h/h758.md), and [ʿÛṣ](../../strongs/h/h5780.md), and [Ḥûl](../../strongs/h/h2343.md), and [Geṯer](../../strongs/h/h1666.md), and [Mešeḵ](../../strongs/h/h4902.md).

<a name="1chronicles_1_18"></a>1Chronicles 1:18

And ['Arpaḵšaḏ](../../strongs/h/h775.md) [yalad](../../strongs/h/h3205.md) [Šelaḥ](../../strongs/h/h7974.md), and [Šelaḥ](../../strongs/h/h7974.md) [yalad](../../strongs/h/h3205.md) [ʿēḇer](../../strongs/h/h5677.md).

<a name="1chronicles_1_19"></a>1Chronicles 1:19

And unto [ʿēḇer](../../strongs/h/h5677.md) were [yalad](../../strongs/h/h3205.md) two [ben](../../strongs/h/h1121.md): the [shem](../../strongs/h/h8034.md) of the one was [Peleḡ](../../strongs/h/h6389.md); because in his [yowm](../../strongs/h/h3117.md) the ['erets](../../strongs/h/h776.md) was [pālaḡ](../../strongs/h/h6385.md): and his ['ach](../../strongs/h/h251.md) [shem](../../strongs/h/h8034.md) was [Yāqṭān](../../strongs/h/h3355.md).

<a name="1chronicles_1_20"></a>1Chronicles 1:20

And [Yāqṭān](../../strongs/h/h3355.md) [yalad](../../strongs/h/h3205.md) ['Almôḏāḏ](../../strongs/h/h486.md), and [Šelep̄](../../strongs/h/h8026.md), and [Ḥăṣarmāveṯ](../../strongs/h/h2700.md), and [Yeraḥ](../../strongs/h/h3392.md),

<a name="1chronicles_1_21"></a>1Chronicles 1:21

[Hăḏôrām](../../strongs/h/h1913.md) also, and ['Ûzāl](../../strongs/h/h187.md), and [Diqlâ](../../strongs/h/h1853.md),

<a name="1chronicles_1_22"></a>1Chronicles 1:22

And [ʿÊḇāl](../../strongs/h/h5858.md), and ['Ăḇîmā'ēl](../../strongs/h/h39.md), and [Šᵊḇā'](../../strongs/h/h7614.md),

<a name="1chronicles_1_23"></a>1Chronicles 1:23

And ['Ôp̄îr](../../strongs/h/h211.md), and [Ḥăvîlâ](../../strongs/h/h2341.md), and [Yôḇāḇ](../../strongs/h/h3103.md). All these were the [ben](../../strongs/h/h1121.md) of [Yāqṭān](../../strongs/h/h3355.md).

<a name="1chronicles_1_24"></a>1Chronicles 1:24

[Šēm](../../strongs/h/h8035.md), ['Arpaḵšaḏ](../../strongs/h/h775.md), [Šelaḥ](../../strongs/h/h7974.md),

<a name="1chronicles_1_25"></a>1Chronicles 1:25

[ʿēḇer](../../strongs/h/h5677.md), [Peleḡ](../../strongs/h/h6389.md), [Rᵊʿû](../../strongs/h/h7466.md),

<a name="1chronicles_1_26"></a>1Chronicles 1:26

[Śᵊrûḡ](../../strongs/h/h8286.md), [Nāḥôr](../../strongs/h/h5152.md), [Teraḥ](../../strongs/h/h8646.md),

<a name="1chronicles_1_27"></a>1Chronicles 1:27

['Aḇrām](../../strongs/h/h87.md); the same is ['Abraham](../../strongs/h/h85.md).

<a name="1chronicles_1_28"></a>1Chronicles 1:28

The [ben](../../strongs/h/h1121.md) of ['Abraham](../../strongs/h/h85.md); [Yiṣḥāq](../../strongs/h/h3327.md), and [Yišmāʿē'l](../../strongs/h/h3458.md).

<a name="1chronicles_1_29"></a>1Chronicles 1:29

These are their [towlĕdah](../../strongs/h/h8435.md): The [bᵊḵôr](../../strongs/h/h1060.md) of [Yišmāʿē'l](../../strongs/h/h3458.md), [Nᵊḇāyôṯ](../../strongs/h/h5032.md); then [Qēḏār](../../strongs/h/h6938.md), and ['Aḏbᵊ'ēl](../../strongs/h/h110.md), and [Miḇśām](../../strongs/h/h4017.md),

<a name="1chronicles_1_30"></a>1Chronicles 1:30

[Mišmāʿ](../../strongs/h/h4927.md), and [Dûmâ](../../strongs/h/h1746.md), [Maśśā'](../../strongs/h/h4854.md), [Ḥăḏaḏ](../../strongs/h/h2301.md), and [Têmā'](../../strongs/h/h8485.md),

<a name="1chronicles_1_31"></a>1Chronicles 1:31

[Yᵊṭûr](../../strongs/h/h3195.md), [Nāp̄îš](../../strongs/h/h5305.md), and [Qēḏmâ](../../strongs/h/h6929.md). These are the [ben](../../strongs/h/h1121.md) of [Yišmāʿē'l](../../strongs/h/h3458.md).

<a name="1chronicles_1_32"></a>1Chronicles 1:32

Now the [ben](../../strongs/h/h1121.md) of [Qᵊṭûrâ](../../strongs/h/h6989.md), ['Abraham](../../strongs/h/h85.md) [pîleḡeš](../../strongs/h/h6370.md): she [yalad](../../strongs/h/h3205.md) [Zimrān](../../strongs/h/h2175.md), and [Yāqšān](../../strongs/h/h3370.md), and [Mᵊḏān](../../strongs/h/h4091.md), and [Miḏyān](../../strongs/h/h4080.md), and [Yišbāq](../../strongs/h/h3435.md), and [Šûaḥ](../../strongs/h/h7744.md). And the [ben](../../strongs/h/h1121.md) of [Yāqšān](../../strongs/h/h3370.md); [Šᵊḇā'](../../strongs/h/h7614.md), and [Dᵊḏān](../../strongs/h/h1719.md).

<a name="1chronicles_1_33"></a>1Chronicles 1:33

And the [ben](../../strongs/h/h1121.md) of [Miḏyān](../../strongs/h/h4080.md); [ʿÊp̄â](../../strongs/h/h5891.md), and [ʿĒp̄er](../../strongs/h/h6081.md), and [Ḥănôḵ](../../strongs/h/h2585.md), and ['Ăḇîḏāʿ](../../strongs/h/h28.md), and ['Eldāʿâ](../../strongs/h/h420.md). All these are the [ben](../../strongs/h/h1121.md) of [Qᵊṭûrâ](../../strongs/h/h6989.md).

<a name="1chronicles_1_34"></a>1Chronicles 1:34

And ['Abraham](../../strongs/h/h85.md) [yalad](../../strongs/h/h3205.md) [Yiṣḥāq](../../strongs/h/h3327.md). The [ben](../../strongs/h/h1121.md) of [Yiṣḥāq](../../strongs/h/h3327.md); [ʿĒśāv](../../strongs/h/h6215.md) and [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_1_35"></a>1Chronicles 1:35

The [ben](../../strongs/h/h1121.md) of [ʿĒśāv](../../strongs/h/h6215.md); ['Ĕlîp̄az](../../strongs/h/h464.md), [Rᵊʿû'ēl](../../strongs/h/h7467.md), and [Yᵊʿûš](../../strongs/h/h3266.md), and [Yaʿlām](../../strongs/h/h3281.md), and [Qōraḥ](../../strongs/h/h7141.md).

<a name="1chronicles_1_36"></a>1Chronicles 1:36

The [ben](../../strongs/h/h1121.md) of ['Ĕlîp̄az](../../strongs/h/h464.md); [Têmān](../../strongs/h/h8487.md), and ['Ômār](../../strongs/h/h201.md), [Ṣᵊp̄ô](../../strongs/h/h6825.md), and [Gaʿtām](../../strongs/h/h1609.md), [Qᵊnaz](../../strongs/h/h7073.md), and [Timnāʿ](../../strongs/h/h8555.md), and [ʿĂmālēq](../../strongs/h/h6002.md).

<a name="1chronicles_1_37"></a>1Chronicles 1:37

The [ben](../../strongs/h/h1121.md) of [Rᵊʿû'ēl](../../strongs/h/h7467.md); [Naḥaṯ](../../strongs/h/h5184.md), [Zeraḥ](../../strongs/h/h2226.md), [Šammâ](../../strongs/h/h8048.md), and [Mizzê](../../strongs/h/h4199.md).

<a name="1chronicles_1_38"></a>1Chronicles 1:38

And the [ben](../../strongs/h/h1121.md) of [Śēʿîr](../../strongs/h/h8165.md); [Lôṭān](../../strongs/h/h3877.md), and [Šôḇāl](../../strongs/h/h7732.md), and [Ṣiḇʿôn](../../strongs/h/h6649.md), and [ʿĂnâ](../../strongs/h/h6034.md), and [Dîšôn](../../strongs/h/h1787.md), and ['Ēṣer](../../strongs/h/h687.md), and [Dîšān](../../strongs/h/h1789.md).

<a name="1chronicles_1_39"></a>1Chronicles 1:39

And the [ben](../../strongs/h/h1121.md) of [Lôṭān](../../strongs/h/h3877.md); [Ḥōrî](../../strongs/h/h2753.md), and [Hômām](../../strongs/h/h1950.md): and [Timnāʿ](../../strongs/h/h8555.md) was [Lôṭān](../../strongs/h/h3877.md) ['āḥôṯ](../../strongs/h/h269.md).

<a name="1chronicles_1_40"></a>1Chronicles 1:40

The [ben](../../strongs/h/h1121.md) of [Šôḇāl](../../strongs/h/h7732.md); [ʿAlvān](../../strongs/h/h5935.md), and [Mānaḥaṯ](../../strongs/h/h4506.md), and [ʿÊḇāl](../../strongs/h/h5858.md), [Šᵊp̄ô](../../strongs/h/h8195.md), and ['Ônām](../../strongs/h/h208.md). And the [ben](../../strongs/h/h1121.md) of [Ṣiḇʿôn](../../strongs/h/h6649.md); ['Ayyâ](../../strongs/h/h345.md), and [ʿĂnâ](../../strongs/h/h6034.md).

<a name="1chronicles_1_41"></a>1Chronicles 1:41

The [ben](../../strongs/h/h1121.md) of [ʿĂnâ](../../strongs/h/h6034.md); [Dîšôn](../../strongs/h/h1787.md). And the [ben](../../strongs/h/h1121.md) of [Dîšôn](../../strongs/h/h1787.md); [Ḥamrān](../../strongs/h/h2566.md), and ['Ešbān](../../strongs/h/h790.md), and [Yiṯrān](../../strongs/h/h3506.md), and [kᵊrān](../../strongs/h/h3763.md).

<a name="1chronicles_1_42"></a>1Chronicles 1:42

The [ben](../../strongs/h/h1121.md) of ['Ēṣer](../../strongs/h/h687.md); [Bilhān](../../strongs/h/h1092.md), and [Zaʿăvān](../../strongs/h/h2190.md), and [YaʿĂqān](../../strongs/h/h3292.md). The [ben](../../strongs/h/h1121.md) of [Dîšān](../../strongs/h/h1789.md); [ʿÛṣ](../../strongs/h/h5780.md), and ['Ărān](../../strongs/h/h765.md).

<a name="1chronicles_1_43"></a>1Chronicles 1:43

Now these are the [melek](../../strongs/h/h4428.md) that [mālaḵ](../../strongs/h/h4427.md) in the ['erets](../../strongs/h/h776.md) of ['Ĕḏōm](../../strongs/h/h123.md) [paniym](../../strongs/h/h6440.md) any [melek](../../strongs/h/h4428.md) [mālaḵ](../../strongs/h/h4427.md) over the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); [Belaʿ](../../strongs/h/h1106.md) the [ben](../../strongs/h/h1121.md) of [Bᵊʿôr](../../strongs/h/h1160.md): and the [shem](../../strongs/h/h8034.md) of his [ʿîr](../../strongs/h/h5892.md) was [Dinhāḇâ](../../strongs/h/h1838.md).

<a name="1chronicles_1_44"></a>1Chronicles 1:44

And when [Belaʿ](../../strongs/h/h1106.md) was [muwth](../../strongs/h/h4191.md), [Yôḇāḇ](../../strongs/h/h3103.md) the [ben](../../strongs/h/h1121.md) of [Zeraḥ](../../strongs/h/h2226.md) of [Bāṣrâ](../../strongs/h/h1224.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1chronicles_1_45"></a>1Chronicles 1:45

And when [Yôḇāḇ](../../strongs/h/h3103.md) was [muwth](../../strongs/h/h4191.md), [Ḥûšām](../../strongs/h/h2367.md) of the ['erets](../../strongs/h/h776.md) of the [têmānî](../../strongs/h/h8489.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1chronicles_1_46"></a>1Chronicles 1:46

And when [Ḥûšām](../../strongs/h/h2367.md) was [muwth](../../strongs/h/h4191.md), [Hăḏaḏ](../../strongs/h/h1908.md) the [ben](../../strongs/h/h1121.md) of [Bᵊḏaḏ](../../strongs/h/h911.md), which [nakah](../../strongs/h/h5221.md) [Miḏyān](../../strongs/h/h4080.md) in the [sadeh](../../strongs/h/h7704.md) of [Mô'āḇ](../../strongs/h/h4124.md), [mālaḵ](../../strongs/h/h4427.md) in his stead: and the [shem](../../strongs/h/h8034.md) of his [ʿîr](../../strongs/h/h5892.md) was [ʿĂvîṯ](../../strongs/h/h5762.md).

<a name="1chronicles_1_47"></a>1Chronicles 1:47

And when [Hăḏaḏ](../../strongs/h/h1908.md) was [muwth](../../strongs/h/h4191.md), [Śamlâ](../../strongs/h/h8072.md) of [maśrēqâ](../../strongs/h/h4957.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1chronicles_1_48"></a>1Chronicles 1:48

And when [Śamlâ](../../strongs/h/h8072.md) was [muwth](../../strongs/h/h4191.md), [Šā'ûl](../../strongs/h/h7586.md) of [Rᵊḥōḇôṯ](../../strongs/h/h7344.md) by the [nāhār](../../strongs/h/h5104.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1chronicles_1_49"></a>1Chronicles 1:49

And when [Šā'ûl](../../strongs/h/h7586.md) was [muwth](../../strongs/h/h4191.md), [Baʿal ḥānān](../../strongs/h/h1177.md) the [ben](../../strongs/h/h1121.md) of [ʿAḵbôr](../../strongs/h/h5907.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1chronicles_1_50"></a>1Chronicles 1:50

And when [Baʿal ḥānān](../../strongs/h/h1177.md) was [muwth](../../strongs/h/h4191.md), [Hăḏaḏ](../../strongs/h/h1908.md) [mālaḵ](../../strongs/h/h4427.md) in his stead: and the [shem](../../strongs/h/h8034.md) of his [ʿîr](../../strongs/h/h5892.md) was [Pāʿû](../../strongs/h/h6464.md); and his ['ishshah](../../strongs/h/h802.md) [shem](../../strongs/h/h8034.md) was [Mᵊhêṭaḇ'ēl](../../strongs/h/h4105.md), the [bath](../../strongs/h/h1323.md) of [Maṭrēḏ](../../strongs/h/h4308.md), the [bath](../../strongs/h/h1323.md) of [Mê zāhāḇ](../../strongs/h/h4314.md).

<a name="1chronicles_1_51"></a>1Chronicles 1:51

[Hăḏaḏ](../../strongs/h/h1908.md) [muwth](../../strongs/h/h4191.md) also. And the ['allûp̄](../../strongs/h/h441.md) of ['Ĕḏōm](../../strongs/h/h123.md) were; ['allûp̄](../../strongs/h/h441.md) [Timnāʿ](../../strongs/h/h8555.md), ['allûp̄](../../strongs/h/h441.md) [ʿAlvâ](../../strongs/h/h5933.md), ['allûp̄](../../strongs/h/h441.md) [Yᵊṯēṯ](../../strongs/h/h3509.md),

<a name="1chronicles_1_52"></a>1Chronicles 1:52

['allûp̄](../../strongs/h/h441.md) ['Āhŏlîḇāmâ](../../strongs/h/h173.md), ['allûp̄](../../strongs/h/h441.md) ['Ēlâ](../../strongs/h/h425.md), ['allûp̄](../../strongs/h/h441.md) [Pînōn](../../strongs/h/h6373.md),

<a name="1chronicles_1_53"></a>1Chronicles 1:53

['allûp̄](../../strongs/h/h441.md) [Qᵊnaz](../../strongs/h/h7073.md), ['allûp̄](../../strongs/h/h441.md) [Têmān](../../strongs/h/h8487.md), ['allûp̄](../../strongs/h/h441.md) [Miḇṣār](../../strongs/h/h4014.md),

<a name="1chronicles_1_54"></a>1Chronicles 1:54

['allûp̄](../../strongs/h/h441.md) [Maḡdî'ēl](../../strongs/h/h4025.md), ['allûp̄](../../strongs/h/h441.md) [ʿÎrām](../../strongs/h/h5902.md). These are the ['allûp̄](../../strongs/h/h441.md) of ['Ĕḏōm](../../strongs/h/h123.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 2](1chronicles_2.md)