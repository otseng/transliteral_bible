# [1Chronicles 15](https://www.blueletterbible.org/kjv/1chronicles/15)

<a name="1chronicles_15_1"></a>1Chronicles 15:1

And ['asah](../../strongs/h/h6213.md) him [bayith](../../strongs/h/h1004.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), and [kuwn](../../strongs/h/h3559.md) a [maqowm](../../strongs/h/h4725.md) for the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md), and [natah](../../strongs/h/h5186.md) for it a ['ohel](../../strongs/h/h168.md).

<a name="1chronicles_15_2"></a>1Chronicles 15:2

Then [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), None ought to [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) but the [Lᵊvî](../../strongs/h/h3881.md): for them hath [Yĕhovah](../../strongs/h/h3068.md) [bāḥar](../../strongs/h/h977.md) to [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md), and to [sharath](../../strongs/h/h8334.md) unto him ['owlam](../../strongs/h/h5769.md).

<a name="1chronicles_15_3"></a>1Chronicles 15:3

And [Dāviḏ](../../strongs/h/h1732.md) [qāhal](../../strongs/h/h6950.md) all [Yisra'el](../../strongs/h/h3478.md) [qāhal](../../strongs/h/h6950.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), to [ʿālâ](../../strongs/h/h5927.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) unto his [maqowm](../../strongs/h/h4725.md), which he had [kuwn](../../strongs/h/h3559.md) for it.

<a name="1chronicles_15_4"></a>1Chronicles 15:4

And [Dāviḏ](../../strongs/h/h1732.md) ['āsap̄](../../strongs/h/h622.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), and the [Lᵊvî](../../strongs/h/h3881.md):

<a name="1chronicles_15_5"></a>1Chronicles 15:5

Of the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md); ['Ûrî'Ēl](../../strongs/h/h222.md) the [śar](../../strongs/h/h8269.md), and his ['ach](../../strongs/h/h251.md) an hundred and twenty:

<a name="1chronicles_15_6"></a>1Chronicles 15:6

Of the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md); [ʿĂśāyâ](../../strongs/h/h6222.md) the [śar](../../strongs/h/h8269.md), and his ['ach](../../strongs/h/h251.md) two hundred and twenty:

<a name="1chronicles_15_7"></a>1Chronicles 15:7

Of the [ben](../../strongs/h/h1121.md) of [Gēršōm](../../strongs/h/h1647.md); [Yô'Ēl](../../strongs/h/h3100.md) the [śar](../../strongs/h/h8269.md), and his ['ach](../../strongs/h/h251.md) an hundred and thirty:

<a name="1chronicles_15_8"></a>1Chronicles 15:8

Of the [ben](../../strongs/h/h1121.md) of ['Ĕlîṣāp̄ān](../../strongs/h/h469.md); [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the [śar](../../strongs/h/h8269.md), and his ['ach](../../strongs/h/h251.md) two hundred:

<a name="1chronicles_15_9"></a>1Chronicles 15:9

Of the [ben](../../strongs/h/h1121.md) of [Ḥeḇrôn](../../strongs/h/h2275.md); ['Ĕlî'Ēl](../../strongs/h/h447.md) the [śar](../../strongs/h/h8269.md), and his ['ach](../../strongs/h/h251.md) fourscore:

<a name="1chronicles_15_10"></a>1Chronicles 15:10

Of the [ben](../../strongs/h/h1121.md) of [ʿUzzî'ēl](../../strongs/h/h5816.md); [ʿAmmînāḏāḇ](../../strongs/h/h5992.md) the [śar](../../strongs/h/h8269.md), and his ['ach](../../strongs/h/h251.md) an hundred and twelve .

<a name="1chronicles_15_11"></a>1Chronicles 15:11

And [Dāviḏ](../../strongs/h/h1732.md) [qara'](../../strongs/h/h7121.md) for [Ṣāḏôq](../../strongs/h/h6659.md) and ['Eḇyāṯār](../../strongs/h/h54.md) the [kōhēn](../../strongs/h/h3548.md), and for the [Lᵊvî](../../strongs/h/h3881.md), for ['Ûrî'Ēl](../../strongs/h/h222.md), [ʿĂśāyâ](../../strongs/h/h6222.md), and [Yô'Ēl](../../strongs/h/h3100.md), [ŠᵊmaʿYâ](../../strongs/h/h8098.md), and ['Ĕlî'Ēl](../../strongs/h/h447.md), and [ʿAmmînāḏāḇ](../../strongs/h/h5992.md),

<a name="1chronicles_15_12"></a>1Chronicles 15:12

And ['āmar](../../strongs/h/h559.md) unto them, Ye are the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of the [Lᵊvî](../../strongs/h/h3881.md): [qadash](../../strongs/h/h6942.md) yourselves, both ye and your ['ach](../../strongs/h/h251.md), that ye may [ʿālâ](../../strongs/h/h5927.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) unto the place that I have [kuwn](../../strongs/h/h3559.md) for it.

<a name="1chronicles_15_13"></a>1Chronicles 15:13

For because ye did it not at the [ri'šôn](../../strongs/h/h7223.md), [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) made a [pāraṣ](../../strongs/h/h6555.md) upon us, for that we [darash](../../strongs/h/h1875.md) him not after the due [mishpat](../../strongs/h/h4941.md).

<a name="1chronicles_15_14"></a>1Chronicles 15:14

So the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md) [qadash](../../strongs/h/h6942.md) themselves to [ʿālâ](../../strongs/h/h5927.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_15_15"></a>1Chronicles 15:15

And the [ben](../../strongs/h/h1121.md) of the [Lᵊvî](../../strongs/h/h3881.md) [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) upon their [kāṯēp̄](../../strongs/h/h3802.md) with the [môṭâ](../../strongs/h/h4133.md) thereon, as [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md) according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1chronicles_15_16"></a>1Chronicles 15:16

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to the [śar](../../strongs/h/h8269.md) of the [Lᵊvî](../../strongs/h/h3881.md) to ['amad](../../strongs/h/h5975.md) their ['ach](../../strongs/h/h251.md) to be the [shiyr](../../strongs/h/h7891.md) with [kĕliy](../../strongs/h/h3627.md) of [šîr](../../strongs/h/h7892.md), [neḇel](../../strongs/h/h5035.md) and [kinnôr](../../strongs/h/h3658.md) and [mᵊṣēleṯ](../../strongs/h/h4700.md), [shama'](../../strongs/h/h8085.md), by [ruwm](../../strongs/h/h7311.md) the [qowl](../../strongs/h/h6963.md) with [simchah](../../strongs/h/h8057.md).

<a name="1chronicles_15_17"></a>1Chronicles 15:17

So the [Lᵊvî](../../strongs/h/h3881.md) ['amad](../../strongs/h/h5975.md) [Hêmān](../../strongs/h/h1968.md) the [ben](../../strongs/h/h1121.md) of [Yô'Ēl](../../strongs/h/h3100.md); and of his ['ach](../../strongs/h/h251.md), ['Āsāp̄](../../strongs/h/h623.md) the [ben](../../strongs/h/h1121.md) of [Bereḵyâ](../../strongs/h/h1296.md); and of the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md) their ['ach](../../strongs/h/h251.md), ['Êṯān](../../strongs/h/h387.md) the [ben](../../strongs/h/h1121.md) of [Qûšāyâû](../../strongs/h/h6984.md);

<a name="1chronicles_15_18"></a>1Chronicles 15:18

And with them their ['ach](../../strongs/h/h251.md) of the [mišnê](../../strongs/h/h4932.md), [Zᵊḵaryâ](../../strongs/h/h2148.md), [Bēn](../../strongs/h/h1122.md), and [YaʿĂzî'Ēl](../../strongs/h/h3268.md), and [Šᵊmîrāmôṯ](../../strongs/h/h8070.md), and [Yᵊḥî'Ēl](../../strongs/h/h3171.md), and [ʿUnnî](../../strongs/h/h6042.md), ['Ĕlî'āḇ](../../strongs/h/h446.md), and [Bᵊnāyâ](../../strongs/h/h1141.md), and [MaʿĂśêâ](../../strongs/h/h4641.md), and [Mataṯyâ](../../strongs/h/h4993.md), and ['Ĕlîp̄Lêû](../../strongs/h/h466.md), and [Miqnêâû](../../strongs/h/h4737.md), and [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md), and [YᵊʿÎ'Ēl](../../strongs/h/h3273.md), the [šôʿēr](../../strongs/h/h7778.md).

<a name="1chronicles_15_19"></a>1Chronicles 15:19

So the [shiyr](../../strongs/h/h7891.md), [Hêmān](../../strongs/h/h1968.md), ['Āsāp̄](../../strongs/h/h623.md), and ['Êṯān](../../strongs/h/h387.md), were appointed to [shama'](../../strongs/h/h8085.md) with [mᵊṣēleṯ](../../strongs/h/h4700.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md);

<a name="1chronicles_15_20"></a>1Chronicles 15:20

And [Zᵊḵaryâ](../../strongs/h/h2148.md), and [ʿĂzî'Ēl](../../strongs/h/h5815.md), and [Šᵊmîrāmôṯ](../../strongs/h/h8070.md), and [Yᵊḥî'Ēl](../../strongs/h/h3171.md), and [ʿUnnî](../../strongs/h/h6042.md), and ['Ĕlî'āḇ](../../strongs/h/h446.md), and [MaʿĂśêâ](../../strongs/h/h4641.md), and [Bᵊnāyâ](../../strongs/h/h1141.md), with [neḇel](../../strongs/h/h5035.md) on [ʿĂlāmôṯ](../../strongs/h/h5961.md);

<a name="1chronicles_15_21"></a>1Chronicles 15:21

And [Mataṯyâ](../../strongs/h/h4993.md), and ['Ĕlîp̄Lêû](../../strongs/h/h466.md), and [Miqnêâû](../../strongs/h/h4737.md), and [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md), and [YᵊʿÎ'Ēl](../../strongs/h/h3273.md), and [ʿĀzazyâû](../../strongs/h/h5812.md), with [kinnôr](../../strongs/h/h3658.md) on the [šᵊmînîṯ](../../strongs/h/h8067.md) to [nāṣaḥ](../../strongs/h/h5329.md).

<a name="1chronicles_15_22"></a>1Chronicles 15:22

And [Kᵊnanyâ](../../strongs/h/h3663.md), [śar](../../strongs/h/h8269.md) of the [Lᵊvî](../../strongs/h/h3881.md), was for [maśśā'](../../strongs/h/h4853.md): he [yacar](../../strongs/h/h3256.md) about the [maśśā'](../../strongs/h/h4853.md), because he was [bîn](../../strongs/h/h995.md).

<a name="1chronicles_15_23"></a>1Chronicles 15:23

And [Bereḵyâ](../../strongs/h/h1296.md) and ['Elqānâ](../../strongs/h/h511.md) were [šôʿēr](../../strongs/h/h7778.md) for the ['ārôn](../../strongs/h/h727.md).

<a name="1chronicles_15_24"></a>1Chronicles 15:24

And [Šᵊḇanyâ](../../strongs/h/h7645.md), and [YôshĀp̄Āṭ](../../strongs/h/h3146.md), and [Nᵊṯan'ēl](../../strongs/h/h5417.md), and [ʿĂmāśay](../../strongs/h/h6022.md), and [Zᵊḵaryâ](../../strongs/h/h2148.md), and [Bᵊnāyâ](../../strongs/h/h1141.md), and ['Ĕlîʿezer](../../strongs/h/h461.md), the [kōhēn](../../strongs/h/h3548.md), did [ḥāṣar](../../strongs/h/h2690.md) [ḥāṣar](../../strongs/h/h2690.md) with the [ḥăṣōṣrâ](../../strongs/h/h2689.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md): and [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md) and [Yᵊḥîyâ](../../strongs/h/h3174.md) were [šôʿēr](../../strongs/h/h7778.md) for the ['ārôn](../../strongs/h/h727.md).

<a name="1chronicles_15_25"></a>1Chronicles 15:25

So [Dāviḏ](../../strongs/h/h1732.md), and the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md), and the [śar](../../strongs/h/h8269.md) over thousands, [halak](../../strongs/h/h1980.md) to [ʿālâ](../../strongs/h/h5927.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) out of the [bayith](../../strongs/h/h1004.md) of [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md) with [simchah](../../strongs/h/h8057.md).

<a name="1chronicles_15_26"></a>1Chronicles 15:26

And it came to pass, when ['Elohiym](../../strongs/h/h430.md) [ʿāzar](../../strongs/h/h5826.md) the [Lᵊvî](../../strongs/h/h3881.md) that [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md), that they [zabach](../../strongs/h/h2076.md) seven [par](../../strongs/h/h6499.md) and seven ['ayil](../../strongs/h/h352.md).

<a name="1chronicles_15_27"></a>1Chronicles 15:27

And [Dāviḏ](../../strongs/h/h1732.md) was [kirbēl](../../strongs/h/h3736.md) with a [mᵊʿîl](../../strongs/h/h4598.md) of [bûṣ](../../strongs/h/h948.md), and all the [Lᵊvî](../../strongs/h/h3881.md) that [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md), and the [shiyr](../../strongs/h/h7891.md), and [Kᵊnanyâ](../../strongs/h/h3663.md) the [śar](../../strongs/h/h8269.md) of the [maśśā'](../../strongs/h/h4853.md) with the [shiyr](../../strongs/h/h7891.md): [Dāviḏ](../../strongs/h/h1732.md) also had upon him an ['ēp̄ôḏ](../../strongs/h/h646.md) of [baḏ](../../strongs/h/h906.md).

<a name="1chronicles_15_28"></a>1Chronicles 15:28

Thus all [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) with [tᵊrûʿâ](../../strongs/h/h8643.md), and with [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md), and with [ḥăṣōṣrâ](../../strongs/h/h2689.md), and with [mᵊṣēleṯ](../../strongs/h/h4700.md), making a [shama'](../../strongs/h/h8085.md) with [neḇel](../../strongs/h/h5035.md) and [kinnôr](../../strongs/h/h3658.md).

<a name="1chronicles_15_29"></a>1Chronicles 15:29

And it came to pass, as the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md) to the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), that [Mîḵāl](../../strongs/h/h4324.md) the [bath](../../strongs/h/h1323.md) of [Šā'ûl](../../strongs/h/h7586.md) looking [šāqap̄](../../strongs/h/h8259.md) at a [ḥallôn](../../strongs/h/h2474.md) [ra'ah](../../strongs/h/h7200.md) [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) [rāqaḏ](../../strongs/h/h7540.md) and [śāḥaq](../../strongs/h/h7832.md): and she [bazah](../../strongs/h/h959.md) him in her [leb](../../strongs/h/h3820.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 14](1chronicles_14.md) - [1Chronicles 16](1chronicles_16.md)