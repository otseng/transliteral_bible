# [1Chronicles 26](https://www.blueletterbible.org/kjv/1chronicles/26)

<a name="1chronicles_26_1"></a>1Chronicles 26:1

Concerning the [maḥălōqeṯ](../../strongs/h/h4256.md) of the [šôʿēr](../../strongs/h/h7778.md): Of the [Qārḥî](../../strongs/h/h7145.md) was [Mᵊšelemyâ](../../strongs/h/h4920.md) the [ben](../../strongs/h/h1121.md) of [Qōrē'](../../strongs/h/h6981.md), of the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md).

<a name="1chronicles_26_2"></a>1Chronicles 26:2

And the [ben](../../strongs/h/h1121.md) of [Mᵊšelemyâ](../../strongs/h/h4920.md) were, [Zᵊḵaryâ](../../strongs/h/h2148.md) the [bᵊḵôr](../../strongs/h/h1060.md), [YᵊḏîʿĂ'Ēl](../../strongs/h/h3043.md) the second, [Zᵊḇaḏyâ](../../strongs/h/h2069.md) the third, [Yaṯnî'Ēl](../../strongs/h/h3496.md) the fourth,

<a name="1chronicles_26_3"></a>1Chronicles 26:3

[ʿÊlām](../../strongs/h/h5867.md) the fifth, [Yᵊhôḥānān](../../strongs/h/h3076.md) the sixth, ['ElyᵊhôʿÊnay](../../strongs/h/h454.md) the seventh.

<a name="1chronicles_26_4"></a>1Chronicles 26:4

Moreover the [ben](../../strongs/h/h1121.md) of [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md) were, [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the [bᵊḵôr](../../strongs/h/h1060.md), [Yᵊhôzāḇāḏ](../../strongs/h/h3075.md) the second, [Yô'āḥ](../../strongs/h/h3098.md) the third, and [Śāḵār](../../strongs/h/h7940.md) the fourth, and [Nᵊṯan'ēl](../../strongs/h/h5417.md) the fifth,

<a name="1chronicles_26_5"></a>1Chronicles 26:5

[ʿammî'ēl](../../strongs/h/h5988.md) the sixth, [Yiśśāśḵār](../../strongs/h/h3485.md) the seventh, [PᵊʿUllᵊṯay](../../strongs/h/h6469.md) the eighth: for ['Elohiym](../../strongs/h/h430.md) [barak](../../strongs/h/h1288.md) him.

<a name="1chronicles_26_6"></a>1Chronicles 26:6

Also unto [ŠᵊmaʿYâ](../../strongs/h/h8098.md) his [ben](../../strongs/h/h1121.md) were [ben](../../strongs/h/h1121.md) [yalad](../../strongs/h/h3205.md), that [mimšāl](../../strongs/h/h4474.md) throughout the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md): for they were [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md).

<a name="1chronicles_26_7"></a>1Chronicles 26:7

The [ben](../../strongs/h/h1121.md) of [ŠᵊmaʿYâ](../../strongs/h/h8098.md); [ʿĀṯnî](../../strongs/h/h6273.md), and [Rᵊp̄Ā'Ēl](../../strongs/h/h7501.md), and [ʿÔḇēḏ](../../strongs/h/h5744.md), ['Elzāḇāḏ](../../strongs/h/h443.md), whose ['ach](../../strongs/h/h251.md) were [ḥayil](../../strongs/h/h2428.md) [ben](../../strongs/h/h1121.md), ['Ĕlîhû](../../strongs/h/h453.md), and [Sᵊmaḵyâû](../../strongs/h/h5565.md).

<a name="1chronicles_26_8"></a>1Chronicles 26:8

All these of the [ben](../../strongs/h/h1121.md) of [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md): they and their [ben](../../strongs/h/h1121.md) and their ['ach](../../strongs/h/h251.md), [ḥayil](../../strongs/h/h2428.md) ['iysh](../../strongs/h/h376.md) for [koach](../../strongs/h/h3581.md) for the [ʿăḇōḏâ](../../strongs/h/h5656.md), were threescore and two of [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md).

<a name="1chronicles_26_9"></a>1Chronicles 26:9

And [Mᵊšelemyâ](../../strongs/h/h4920.md) had [ben](../../strongs/h/h1121.md) and ['ach](../../strongs/h/h251.md), [ḥayil](../../strongs/h/h2428.md) [ben](../../strongs/h/h1121.md), eighteen .

<a name="1chronicles_26_10"></a>1Chronicles 26:10

Also [Ḥōsâ](../../strongs/h/h2621.md), of the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md), had [ben](../../strongs/h/h1121.md); [Šimrî](../../strongs/h/h8113.md) the [ro'sh](../../strongs/h/h7218.md), (for though he was not the [bᵊḵôr](../../strongs/h/h1060.md), yet his ['ab](../../strongs/h/h1.md) [śûm](../../strongs/h/h7760.md) him the [ro'sh](../../strongs/h/h7218.md);)

<a name="1chronicles_26_11"></a>1Chronicles 26:11

[Ḥilqîyâ](../../strongs/h/h2518.md) the second, [Ṭᵊḇalyâû](../../strongs/h/h2882.md) the third, [Zᵊḵaryâ](../../strongs/h/h2148.md) the fourth: all the [ben](../../strongs/h/h1121.md) and ['ach](../../strongs/h/h251.md) of [Ḥōsâ](../../strongs/h/h2621.md) were thirteen .

<a name="1chronicles_26_12"></a>1Chronicles 26:12

Among these were the [maḥălōqeṯ](../../strongs/h/h4256.md) of the [šôʿēr](../../strongs/h/h7778.md), even among the [ro'sh](../../strongs/h/h7218.md) [geḇer](../../strongs/h/h1397.md), having [mišmereṯ](../../strongs/h/h4931.md) one [ʿummâ](../../strongs/h/h5980.md) ['ach](../../strongs/h/h251.md), to [sharath](../../strongs/h/h8334.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1chronicles_26_13"></a>1Chronicles 26:13

And they [naphal](../../strongs/h/h5307.md) [gôrāl](../../strongs/h/h1486.md), as well the [qāṭān](../../strongs/h/h6996.md) as the [gadowl](../../strongs/h/h1419.md), according to the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), for every [sha'ar](../../strongs/h/h8179.md).

<a name="1chronicles_26_14"></a>1Chronicles 26:14

And the [gôrāl](../../strongs/h/h1486.md) [mizrach](../../strongs/h/h4217.md) [naphal](../../strongs/h/h5307.md) to [Šelemyâ](../../strongs/h/h8018.md). Then for [Zᵊḵaryâ](../../strongs/h/h2148.md) his [ben](../../strongs/h/h1121.md), a [śēḵel](../../strongs/h/h7922.md) [ya'ats](../../strongs/h/h3289.md), they [naphal](../../strongs/h/h5307.md) [gôrāl](../../strongs/h/h1486.md); and his [gôrāl](../../strongs/h/h1486.md) [yāṣā'](../../strongs/h/h3318.md) [ṣāp̄ôn](../../strongs/h/h6828.md).

<a name="1chronicles_26_15"></a>1Chronicles 26:15

To [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md) [neḡeḇ](../../strongs/h/h5045.md); and to his [ben](../../strongs/h/h1121.md) the [bayith](../../strongs/h/h1004.md) of ['āsōp̄](../../strongs/h/h624.md).

<a name="1chronicles_26_16"></a>1Chronicles 26:16

To [Šupîm](../../strongs/h/h8206.md) and [Ḥōsâ](../../strongs/h/h2621.md) the lot came forth [ma'arab](../../strongs/h/h4628.md), with the [sha'ar](../../strongs/h/h8179.md) [Šalleḵeṯ](../../strongs/h/h7996.md), by the [mĕcillah](../../strongs/h/h4546.md) of the [ʿālâ](../../strongs/h/h5927.md), [mišmār](../../strongs/h/h4929.md) [ʿummâ](../../strongs/h/h5980.md) [mišmār](../../strongs/h/h4929.md).

<a name="1chronicles_26_17"></a>1Chronicles 26:17

[mizrach](../../strongs/h/h4217.md) were six [Lᵊvî](../../strongs/h/h3881.md), [ṣāp̄ôn](../../strongs/h/h6828.md) four a [yowm](../../strongs/h/h3117.md), [neḡeḇ](../../strongs/h/h5045.md) four a [yowm](../../strongs/h/h3117.md), and toward ['āsōp̄](../../strongs/h/h624.md) two and two.

<a name="1chronicles_26_18"></a>1Chronicles 26:18

At [Parbār](../../strongs/h/h6503.md) [ma'arab](../../strongs/h/h4628.md), four at the [mĕcillah](../../strongs/h/h4546.md), and two at [Parbār](../../strongs/h/h6503.md).

<a name="1chronicles_26_19"></a>1Chronicles 26:19

These are the [maḥălōqeṯ](../../strongs/h/h4256.md) of the [šôʿēr](../../strongs/h/h7778.md) among the [ben](../../strongs/h/h1121.md) of [Qārḥî](../../strongs/h/h7145.md), and among the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md).

<a name="1chronicles_26_20"></a>1Chronicles 26:20

And of the [Lᵊvî](../../strongs/h/h3881.md), ['Ăḥîyâ](../../strongs/h/h281.md) was over the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), and over the ['ôṣār](../../strongs/h/h214.md) of the [qodesh](../../strongs/h/h6944.md).

<a name="1chronicles_26_21"></a>1Chronicles 26:21

As concerning the [ben](../../strongs/h/h1121.md) of [LaʿDān](../../strongs/h/h3936.md); the [ben](../../strongs/h/h1121.md) of the [Gēršunnî](../../strongs/h/h1649.md) [LaʿDān](../../strongs/h/h3936.md), [ro'sh](../../strongs/h/h7218.md) ['ab](../../strongs/h/h1.md), even of [LaʿDān](../../strongs/h/h3936.md) the [Gēršunnî](../../strongs/h/h1649.md), were [Yᵊḥî'Ēlî](../../strongs/h/h3172.md).

<a name="1chronicles_26_22"></a>1Chronicles 26:22

The [ben](../../strongs/h/h1121.md) of [Yᵊḥî'Ēlî](../../strongs/h/h3172.md); [Zēṯām](../../strongs/h/h2241.md), and [Yô'Ēl](../../strongs/h/h3100.md) his ['ach](../../strongs/h/h251.md), which were over the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1chronicles_26_23"></a>1Chronicles 26:23

Of the [ʿAmrāmî](../../strongs/h/h6020.md), and the [Yiṣhārî](../../strongs/h/h3325.md), the [Ḥeḇrônî](../../strongs/h/h2276.md), and the [ʿĀzzî'ēlî](../../strongs/h/h5817.md):

<a name="1chronicles_26_24"></a>1Chronicles 26:24

And [Šᵊḇû'Ēl](../../strongs/h/h7619.md) the [ben](../../strongs/h/h1121.md) of [Gēršōm](../../strongs/h/h1647.md), the [ben](../../strongs/h/h1121.md) of [Mōshe](../../strongs/h/h4872.md), was [nāḡîḏ](../../strongs/h/h5057.md) of the ['ôṣār](../../strongs/h/h214.md).

<a name="1chronicles_26_25"></a>1Chronicles 26:25

And his ['ach](../../strongs/h/h251.md) by ['Ĕlîʿezer](../../strongs/h/h461.md); [Rᵊḥaḇyâ](../../strongs/h/h7345.md) his [ben](../../strongs/h/h1121.md), and [Yᵊšaʿyâ](../../strongs/h/h3470.md) his [ben](../../strongs/h/h1121.md), and [Yôrām](../../strongs/h/h3141.md) his [ben](../../strongs/h/h1121.md), and [Ziḵrî](../../strongs/h/h2147.md) his [ben](../../strongs/h/h1121.md), and [Šᵊlōmîṯ](../../strongs/h/h8019.md) [Śᵊlōmôṯ](../../strongs/h/h8013.md) his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_26_26"></a>1Chronicles 26:26

Which [Śᵊlōmôṯ](../../strongs/h/h8013.md) and his ['ach](../../strongs/h/h251.md) were over all the ['ôṣār](../../strongs/h/h214.md) of the [qodesh](../../strongs/h/h6944.md), which [Dāviḏ](../../strongs/h/h1732.md) the [melek](../../strongs/h/h4428.md), and the [ro'sh](../../strongs/h/h7218.md) ['ab](../../strongs/h/h1.md), the [śar](../../strongs/h/h8269.md) over thousands and hundreds, and the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md), had [qadash](../../strongs/h/h6942.md).

<a name="1chronicles_26_27"></a>1Chronicles 26:27

Out of the [šālāl](../../strongs/h/h7998.md) won in [milḥāmâ](../../strongs/h/h4421.md) did they [qadash](../../strongs/h/h6942.md) to [ḥāzaq](../../strongs/h/h2388.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1chronicles_26_28"></a>1Chronicles 26:28

And all that [Šᵊmû'Ēl](../../strongs/h/h8050.md) the [ra'ah](../../strongs/h/h7200.md), and [Šā'ûl](../../strongs/h/h7586.md) the [ben](../../strongs/h/h1121.md) of [Qîš](../../strongs/h/h7027.md), and ['Aḇnēr](../../strongs/h/h74.md) the [ben](../../strongs/h/h1121.md) of [Nēr](../../strongs/h/h5369.md), and [Yô'āḇ](../../strongs/h/h3097.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md), had [qadash](../../strongs/h/h6942.md); and whosoever had [qadash](../../strongs/h/h6942.md) any thing, it was under the [yad](../../strongs/h/h3027.md) of [Šᵊlōmîṯ](../../strongs/h/h8019.md), and of his ['ach](../../strongs/h/h251.md).

<a name="1chronicles_26_29"></a>1Chronicles 26:29

Of the [Yiṣhārî](../../strongs/h/h3325.md), [Kᵊnanyâ](../../strongs/h/h3663.md) and his [ben](../../strongs/h/h1121.md) were for the [ḥîṣôn](../../strongs/h/h2435.md) [mĕla'kah](../../strongs/h/h4399.md) over [Yisra'el](../../strongs/h/h3478.md), for [šāṭar](../../strongs/h/h7860.md) and [shaphat](../../strongs/h/h8199.md).

<a name="1chronicles_26_30"></a>1Chronicles 26:30

And of the [Ḥeḇrônî](../../strongs/h/h2276.md), [Ḥăšaḇyâ](../../strongs/h/h2811.md) and his ['ach](../../strongs/h/h251.md), [ben](../../strongs/h/h1121.md) of [ḥayil](../../strongs/h/h2428.md), a thousand and seven hundred, were [pᵊqudâ](../../strongs/h/h6486.md) among them of [Yisra'el](../../strongs/h/h3478.md) on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) [ma'arab](../../strongs/h/h4628.md) in all the [mĕla'kah](../../strongs/h/h4399.md) of [Yĕhovah](../../strongs/h/h3068.md), and in the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [melek](../../strongs/h/h4428.md).

<a name="1chronicles_26_31"></a>1Chronicles 26:31

Among the [Ḥeḇrônî](../../strongs/h/h2276.md) was [Yᵊrîyâ](../../strongs/h/h3404.md) the [ro'sh](../../strongs/h/h7218.md), even among the [Ḥeḇrônî](../../strongs/h/h2276.md), according to the [towlĕdah](../../strongs/h/h8435.md) of his ['ab](../../strongs/h/h1.md). In the fortieth [šānâ](../../strongs/h/h8141.md) of the [malkuwth](../../strongs/h/h4438.md) of [Dāviḏ](../../strongs/h/h1732.md) they were [darash](../../strongs/h/h1875.md) for, and there were [māṣā'](../../strongs/h/h4672.md) among them [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md) at [Yaʿzêr](../../strongs/h/h3270.md) of [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="1chronicles_26_32"></a>1Chronicles 26:32

And his ['ach](../../strongs/h/h251.md), [ben](../../strongs/h/h1121.md) of [ḥayil](../../strongs/h/h2428.md), were two thousand and seven hundred [ro'sh](../../strongs/h/h7218.md) ['ab](../../strongs/h/h1.md), whom [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) made [paqad](../../strongs/h/h6485.md) over the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md), the [Gāḏî](../../strongs/h/h1425.md), and the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4520.md), for every [dabar](../../strongs/h/h1697.md) pertaining to ['Elohiym](../../strongs/h/h430.md), and [dabar](../../strongs/h/h1697.md) of the [melek](../../strongs/h/h4428.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 25](1chronicles_25.md) - [1Chronicles 27](1chronicles_27.md)