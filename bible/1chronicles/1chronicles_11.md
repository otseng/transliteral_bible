# [1Chronicles 11](https://www.blueletterbible.org/kjv/1chronicles/11)

<a name="1chronicles_11_1"></a>1Chronicles 11:1

Then all [Yisra'el](../../strongs/h/h3478.md) [qāḇaṣ](../../strongs/h/h6908.md) themselves to [Dāviḏ](../../strongs/h/h1732.md) unto [Ḥeḇrôn](../../strongs/h/h2275.md), ['āmar](../../strongs/h/h559.md), Behold, we are thy ['etsem](../../strongs/h/h6106.md) and thy [basar](../../strongs/h/h1320.md).

<a name="1chronicles_11_2"></a>1Chronicles 11:2

And moreover in [tᵊmôl](../../strongs/h/h8543.md) [šilšôm](../../strongs/h/h8032.md), even when [Šā'ûl](../../strongs/h/h7586.md) was [melek](../../strongs/h/h4428.md), thou wast he that [yāṣā'](../../strongs/h/h3318.md) and [bow'](../../strongs/h/h935.md) [Yisra'el](../../strongs/h/h3478.md): and [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto thee, Thou shalt [ra'ah](../../strongs/h/h7462.md) my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), and thou shalt be [nāḡîḏ](../../strongs/h/h5057.md) over [gam](../../strongs/h/h1571.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_11_3"></a>1Chronicles 11:3

Therefore [bow'](../../strongs/h/h935.md) all the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md) to the [melek](../../strongs/h/h4428.md) to [Ḥeḇrôn](../../strongs/h/h2275.md); and [Dāviḏ](../../strongs/h/h1732.md) [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with them in [Ḥeḇrôn](../../strongs/h/h2275.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); and they [māšaḥ](../../strongs/h/h4886.md) [Dāviḏ](../../strongs/h/h1732.md) [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md), according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) [yad](../../strongs/h/h3027.md) [Šᵊmû'Ēl](../../strongs/h/h8050.md).

<a name="1chronicles_11_4"></a>1Chronicles 11:4

And [Dāviḏ](../../strongs/h/h1732.md) and all [Yisra'el](../../strongs/h/h3478.md) [yālaḵ](../../strongs/h/h3212.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), which is [Yᵊḇûs](../../strongs/h/h2982.md); where the [Yᵊḇûsî](../../strongs/h/h2983.md) were, the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md).

<a name="1chronicles_11_5"></a>1Chronicles 11:5

And the [yashab](../../strongs/h/h3427.md) of [Yᵊḇûs](../../strongs/h/h2982.md) ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), Thou shalt not [bow'](../../strongs/h/h935.md) hither. Nevertheless [Dāviḏ](../../strongs/h/h1732.md) [lāḵaḏ](../../strongs/h/h3920.md) the [matsuwd](../../strongs/h/h4686.md) of [Tsiyown](../../strongs/h/h6726.md), which is the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="1chronicles_11_6"></a>1Chronicles 11:6

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), Whosoever [nakah](../../strongs/h/h5221.md) the [Yᵊḇûsî](../../strongs/h/h2983.md) [ri'šôn](../../strongs/h/h7223.md) shall be [ro'sh](../../strongs/h/h7218.md) and [śar](../../strongs/h/h8269.md). So [Yô'āḇ](../../strongs/h/h3097.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md) [ʿālâ](../../strongs/h/h5927.md) [ri'šôn](../../strongs/h/h7223.md) [ʿālâ](../../strongs/h/h5927.md), and was [ro'sh](../../strongs/h/h7218.md).

<a name="1chronicles_11_7"></a>1Chronicles 11:7

And [Dāviḏ](../../strongs/h/h1732.md) [yashab](../../strongs/h/h3427.md) in the [mᵊṣāḏ](../../strongs/h/h4679.md); therefore they [qara'](../../strongs/h/h7121.md) it the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="1chronicles_11_8"></a>1Chronicles 11:8

And he [bānâ](../../strongs/h/h1129.md) the [ʿîr](../../strongs/h/h5892.md) [cabiyb](../../strongs/h/h5439.md), even from [Millô'](../../strongs/h/h4407.md) [cabiyb](../../strongs/h/h5439.md): and [Yô'āḇ](../../strongs/h/h3097.md) [ḥāyâ](../../strongs/h/h2421.md) the [šᵊ'ār](../../strongs/h/h7605.md) of the [ʿîr](../../strongs/h/h5892.md).

<a name="1chronicles_11_9"></a>1Chronicles 11:9

So [Dāviḏ](../../strongs/h/h1732.md) [yālaḵ](../../strongs/h/h3212.md) [halak](../../strongs/h/h1980.md) and [gadowl](../../strongs/h/h1419.md): for [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) was with him.

<a name="1chronicles_11_10"></a>1Chronicles 11:10

These also are the [ro'sh](../../strongs/h/h7218.md) of the [gibôr](../../strongs/h/h1368.md) whom [Dāviḏ](../../strongs/h/h1732.md) had, who [ḥāzaq](../../strongs/h/h2388.md) themselves with him in his [malkuwth](../../strongs/h/h4438.md), and with all [Yisra'el](../../strongs/h/h3478.md), to make him [mālaḵ](../../strongs/h/h4427.md), according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) concerning [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_11_11"></a>1Chronicles 11:11

And this is the [mispār](../../strongs/h/h4557.md) of the [gibôr](../../strongs/h/h1368.md) whom [Dāviḏ](../../strongs/h/h1732.md) had; [YāšāḇʿĀm](../../strongs/h/h3434.md), an [Ḥaḵmōnî](../../strongs/h/h2453.md), the [ro'sh](../../strongs/h/h7218.md) of the [šālîš](../../strongs/h/h7991.md) : he [ʿûr](../../strongs/h/h5782.md) his [ḥănîṯ](../../strongs/h/h2595.md) against three hundred [ḥālāl](../../strongs/h/h2491.md) by him at one [pa'am](../../strongs/h/h6471.md).

<a name="1chronicles_11_12"></a>1Chronicles 11:12

And ['aḥar](../../strongs/h/h310.md) him was ['Elʿāzār](../../strongs/h/h499.md) the [ben](../../strongs/h/h1121.md) of [Dôḏô](../../strongs/h/h1734.md), the ['Ăḥôḥî](../../strongs/h/h266.md), who was one of the three [gibôr](../../strongs/h/h1368.md).

<a name="1chronicles_11_13"></a>1Chronicles 11:13

He was with [Dāviḏ](../../strongs/h/h1732.md) at [Pas Dammîm](../../strongs/h/h6450.md), and there the [Pᵊlištî](../../strongs/h/h6430.md) were ['āsap̄](../../strongs/h/h622.md) to [milḥāmâ](../../strongs/h/h4421.md), where was a [ḥelqâ](../../strongs/h/h2513.md) of [sadeh](../../strongs/h/h7704.md) [mālē'](../../strongs/h/h4392.md) of [śᵊʿōrâ](../../strongs/h/h8184.md); and the ['am](../../strongs/h/h5971.md) [nûs](../../strongs/h/h5127.md) from [paniym](../../strongs/h/h6440.md) the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1chronicles_11_14"></a>1Chronicles 11:14

And they [yatsab](../../strongs/h/h3320.md) themselves in the [tavek](../../strongs/h/h8432.md) of that [ḥelqâ](../../strongs/h/h2513.md), and [natsal](../../strongs/h/h5337.md) it, and [nakah](../../strongs/h/h5221.md) the [Pᵊlištî](../../strongs/h/h6430.md); and [Yĕhovah](../../strongs/h/h3068.md) [yasha'](../../strongs/h/h3467.md) them by a [gadowl](../../strongs/h/h1419.md) [tᵊšûʿâ](../../strongs/h/h8668.md).

<a name="1chronicles_11_15"></a>1Chronicles 11:15

Now three of the thirty [ro'sh](../../strongs/h/h7218.md) [yarad](../../strongs/h/h3381.md) to the [tsuwr](../../strongs/h/h6697.md) to [Dāviḏ](../../strongs/h/h1732.md), into the [mᵊʿārâ](../../strongs/h/h4631.md) of [ʿĂḏullām](../../strongs/h/h5725.md); and the [maḥănê](../../strongs/h/h4264.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [ḥānâ](../../strongs/h/h2583.md) in the [ʿēmeq](../../strongs/h/h6010.md) of [rᵊp̄ā'îm](../../strongs/h/h7497.md).

<a name="1chronicles_11_16"></a>1Chronicles 11:16

And [Dāviḏ](../../strongs/h/h1732.md) was then in the [matsuwd](../../strongs/h/h4686.md), and the [Pᵊlištî](../../strongs/h/h6430.md) [nᵊṣîḇ](../../strongs/h/h5333.md) was then at [Bêṯ leḥem](../../strongs/h/h1035.md).

<a name="1chronicles_11_17"></a>1Chronicles 11:17

And [Dāviḏ](../../strongs/h/h1732.md) ['āvâ](../../strongs/h/h183.md), and ['āmar](../../strongs/h/h559.md), Oh that one would give me [šāqâ](../../strongs/h/h8248.md) of the [mayim](../../strongs/h/h4325.md) of the [bowr](../../strongs/h/h953.md) of [Bêṯ leḥem](../../strongs/h/h1035.md), that is at the [sha'ar](../../strongs/h/h8179.md)!

<a name="1chronicles_11_18"></a>1Chronicles 11:18

And the three [bāqaʿ](../../strongs/h/h1234.md) the [maḥănê](../../strongs/h/h4264.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and [šā'aḇ](../../strongs/h/h7579.md) [mayim](../../strongs/h/h4325.md) out of the [bowr](../../strongs/h/h953.md) of [Bêṯ leḥem](../../strongs/h/h1035.md), that was by the [sha'ar](../../strongs/h/h8179.md), and [nasa'](../../strongs/h/h5375.md) it, and [bow'](../../strongs/h/h935.md) it to [Dāviḏ](../../strongs/h/h1732.md): but [Dāviḏ](../../strongs/h/h1732.md) ['āḇâ](../../strongs/h/h14.md) not [šāṯâ](../../strongs/h/h8354.md) of it, but [nacak](../../strongs/h/h5258.md) it to [Yĕhovah](../../strongs/h/h3068.md),

<a name="1chronicles_11_19"></a>1Chronicles 11:19

And ['āmar](../../strongs/h/h559.md), My ['Elohiym](../../strongs/h/h430.md) [ḥālîlâ](../../strongs/h/h2486.md) it me, that I should ['asah](../../strongs/h/h6213.md) this thing: shall I [šāṯâ](../../strongs/h/h8354.md) the [dam](../../strongs/h/h1818.md) of these ['enowsh](../../strongs/h/h582.md) that have put their [nephesh](../../strongs/h/h5315.md)? for with their [nephesh](../../strongs/h/h5315.md) they [bow'](../../strongs/h/h935.md) it. Therefore he ['āḇâ](../../strongs/h/h14.md) not [šāṯâ](../../strongs/h/h8354.md) it. These things ['asah](../../strongs/h/h6213.md) these three [gibôr](../../strongs/h/h1368.md).

<a name="1chronicles_11_20"></a>1Chronicles 11:20

And ['Ăḇîšay](../../strongs/h/h52.md) the ['ach](../../strongs/h/h251.md) of [Yô'āḇ](../../strongs/h/h3097.md), he was [ro'sh](../../strongs/h/h7218.md) of the three: for [ʿûr](../../strongs/h/h5782.md) his [ḥănîṯ](../../strongs/h/h2595.md) against three hundred, he [ḥālāl](../../strongs/h/h2491.md) them, and had a [shem](../../strongs/h/h8034.md) among the three.

<a name="1chronicles_11_21"></a>1Chronicles 11:21

Of the three, he was more [kabad](../../strongs/h/h3513.md) than the two; for he was their [śar](../../strongs/h/h8269.md): howbeit he [bow'](../../strongs/h/h935.md) not to the first three.

<a name="1chronicles_11_22"></a>1Chronicles 11:22

[Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md), the [ben](../../strongs/h/h1121.md) of a [ḥayil](../../strongs/h/h2428.md) ['iysh](../../strongs/h/h376.md) of [Qaḇṣᵊ'Ēl](../../strongs/h/h6909.md), who had done [rab](../../strongs/h/h7227.md) [pōʿal](../../strongs/h/h6467.md); he [nakah](../../strongs/h/h5221.md) two ['ărî'ēl](../../strongs/h/h739.md) of [Mô'āḇ](../../strongs/h/h4124.md): also he [yarad](../../strongs/h/h3381.md) and [nakah](../../strongs/h/h5221.md) an ['ariy](../../strongs/h/h738.md) [tavek](../../strongs/h/h8432.md) a [bowr](../../strongs/h/h953.md) in a [šeleḡ](../../strongs/h/h7950.md) [yowm](../../strongs/h/h3117.md).

<a name="1chronicles_11_23"></a>1Chronicles 11:23

And he [nakah](../../strongs/h/h5221.md) an [Miṣrî](../../strongs/h/h4713.md), an ['iysh](../../strongs/h/h376.md) of [midâ](../../strongs/h/h4060.md), five ['ammâ](../../strongs/h/h520.md) high; and in the [Miṣrî](../../strongs/h/h4713.md) [yad](../../strongs/h/h3027.md) was a [ḥănîṯ](../../strongs/h/h2595.md) like an ['āraḡ](../../strongs/h/h707.md) [mānôr](../../strongs/h/h4500.md); and he [yarad](../../strongs/h/h3381.md) to him with a [shebet](../../strongs/h/h7626.md), and [gāzal](../../strongs/h/h1497.md) the [ḥănîṯ](../../strongs/h/h2595.md) out of the [Miṣrî](../../strongs/h/h4713.md) [yad](../../strongs/h/h3027.md), and [harag](../../strongs/h/h2026.md) him with his own [ḥănîṯ](../../strongs/h/h2595.md).

<a name="1chronicles_11_24"></a>1Chronicles 11:24

These things ['asah](../../strongs/h/h6213.md) [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md), and had the [shem](../../strongs/h/h8034.md) among the three [gibôr](../../strongs/h/h1368.md).

<a name="1chronicles_11_25"></a>1Chronicles 11:25

[hinneh](../../strongs/h/h2009.md), he was [kabad](../../strongs/h/h3513.md) among the thirty, but [bow'](../../strongs/h/h935.md) not to the first three: and [Dāviḏ](../../strongs/h/h1732.md) [śûm](../../strongs/h/h7760.md) him over his [mišmaʿaṯ](../../strongs/h/h4928.md).

<a name="1chronicles_11_26"></a>1Chronicles 11:26

Also the valiant [gibôr](../../strongs/h/h1368.md) of the [ḥayil](../../strongs/h/h2428.md) were, [ʿĂśâ'Ēl](../../strongs/h/h6214.md) the ['ach](../../strongs/h/h251.md) of [Yô'āḇ](../../strongs/h/h3097.md), ['Elḥānān](../../strongs/h/h445.md) the [ben](../../strongs/h/h1121.md) of [Dôḏô](../../strongs/h/h1734.md) of [Bêṯ leḥem](../../strongs/h/h1035.md),

<a name="1chronicles_11_27"></a>1Chronicles 11:27

[Šammôṯ](../../strongs/h/h8054.md) the [Hărôrî](../../strongs/h/h2033.md), [Ḥeleṣ](../../strongs/h/h2503.md) the [Palônî](../../strongs/h/h6397.md),

<a name="1chronicles_11_28"></a>1Chronicles 11:28

[ʿÎrā'](../../strongs/h/h5896.md) the [ben](../../strongs/h/h1121.md) of [ʿIqqēš](../../strongs/h/h6142.md) the [Tᵊqôʿî](../../strongs/h/h8621.md), ['ĂḇîʿEzer](../../strongs/h/h44.md) the [ʿAnnᵊṯōṯî](../../strongs/h/h6069.md),

<a name="1chronicles_11_29"></a>1Chronicles 11:29

[Sibḵay](../../strongs/h/h5444.md) the [Ḥušāṯî](../../strongs/h/h2843.md), [ʿÎlay](../../strongs/h/h5866.md) the ['Ăḥôḥî](../../strongs/h/h266.md),

<a name="1chronicles_11_30"></a>1Chronicles 11:30

[Mahăray](../../strongs/h/h4121.md) the [Nᵊṭōp̄āṯî](../../strongs/h/h5200.md), [Ḥēleḏ](../../strongs/h/h2466.md) the [ben](../../strongs/h/h1121.md) of [BaʿĂnâ](../../strongs/h/h1196.md) the [Nᵊṭōp̄āṯî](../../strongs/h/h5200.md),

<a name="1chronicles_11_31"></a>1Chronicles 11:31

['Itay](../../strongs/h/h863.md) the [ben](../../strongs/h/h1121.md) of [Rîḇay](../../strongs/h/h7380.md) of [giḇʿâ](../../strongs/h/h1390.md), that pertained to the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md), [Bᵊnāyâ](../../strongs/h/h1141.md) the [Pirʿāṯônî](../../strongs/h/h6553.md),

<a name="1chronicles_11_32"></a>1Chronicles 11:32

[Ḥûray](../../strongs/h/h2360.md) of the [nachal](../../strongs/h/h5158.md) of [GaʿAš](../../strongs/h/h1608.md), ['Ăḇî'Ēl](../../strongs/h/h22.md) the [ʿArḇāṯî](../../strongs/h/h6164.md),

<a name="1chronicles_11_33"></a>1Chronicles 11:33

[ʿAzmāveṯ](../../strongs/h/h5820.md) the [baḥărûmî](../../strongs/h/h978.md), ['Elyaḥbā'](../../strongs/h/h455.md) the [ŠaʿAlḇōnî](../../strongs/h/h8170.md),

<a name="1chronicles_11_34"></a>1Chronicles 11:34

The [ben](../../strongs/h/h1121.md) of [Hāšēm](../../strongs/h/h2044.md) the [Gizônî](../../strongs/h/h1493.md), [Yônāṯān](../../strongs/h/h3129.md) the [ben](../../strongs/h/h1121.md) of [Šāḡê](../../strongs/h/h7681.md) the [Hărārî](../../strongs/h/h2043.md),

<a name="1chronicles_11_35"></a>1Chronicles 11:35

['Ăḥî'Ām](../../strongs/h/h279.md) the [ben](../../strongs/h/h1121.md) of [Śāḵār](../../strongs/h/h7940.md) the [Hărārî](../../strongs/h/h2043.md), ['Ĕlîp̄Āl](../../strongs/h/h465.md) the [ben](../../strongs/h/h1121.md) of ['Ûr](../../strongs/h/h218.md),

<a name="1chronicles_11_36"></a>1Chronicles 11:36

[Ḥēp̄er](../../strongs/h/h2660.md) the [Mᵊḵērāṯî](../../strongs/h/h4382.md), ['Ăḥîyâ](../../strongs/h/h281.md) the [Palônî](../../strongs/h/h6397.md),

<a name="1chronicles_11_37"></a>1Chronicles 11:37

[Ḥeṣrô](../../strongs/h/h2695.md) the [Karmᵊlî](../../strongs/h/h3761.md), [NaʿĂray](../../strongs/h/h5293.md) the [ben](../../strongs/h/h1121.md) of ['Ezbay](../../strongs/h/h229.md),

<a name="1chronicles_11_38"></a>1Chronicles 11:38

[Yô'Ēl](../../strongs/h/h3100.md) the ['ach](../../strongs/h/h251.md) of [Nāṯān](../../strongs/h/h5416.md), [Miḇḥār](../../strongs/h/h4006.md) the [ben](../../strongs/h/h1121.md) of [Haḡrî](../../strongs/h/h1905.md),

<a name="1chronicles_11_39"></a>1Chronicles 11:39

[Ṣeleq](../../strongs/h/h6768.md) the [ʿAmmôn](../../strongs/h/h5984.md), [Naḥăray](../../strongs/h/h5171.md) the [Bērōṯî](../../strongs/h/h1307.md), the [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md) of [Yô'āḇ](../../strongs/h/h3097.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md),

<a name="1chronicles_11_40"></a>1Chronicles 11:40

[ʿÎrā'](../../strongs/h/h5896.md) the [Yiṯrî](../../strongs/h/h3505.md), [Gārēḇ](../../strongs/h/h1619.md) the [Yiṯrî](../../strongs/h/h3505.md),

<a name="1chronicles_11_41"></a>1Chronicles 11:41

['Ûrîyâ](../../strongs/h/h223.md) the [Ḥitî](../../strongs/h/h2850.md), [Zāḇāḏ](../../strongs/h/h2066.md) the [ben](../../strongs/h/h1121.md) of ['Aḥlāy](../../strongs/h/h304.md),

<a name="1chronicles_11_42"></a>1Chronicles 11:42

[ʿĂḏînā'](../../strongs/h/h5721.md) the [ben](../../strongs/h/h1121.md) of [Šîzā'](../../strongs/h/h7877.md) the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md), a [ro'sh](../../strongs/h/h7218.md) of the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md), and thirty with him,

<a name="1chronicles_11_43"></a>1Chronicles 11:43

[Ḥānān](../../strongs/h/h2605.md) the [ben](../../strongs/h/h1121.md) of [Maʿăḵâ](../../strongs/h/h4601.md), and [YôshĀp̄Āṭ](../../strongs/h/h3146.md) the [Miṯnî](../../strongs/h/h4981.md),

<a name="1chronicles_11_44"></a>1Chronicles 11:44

[ʿUzziyā'](../../strongs/h/h5814.md) the [ʿAštᵊrāṯî](../../strongs/h/h6254.md), [Šāmāʿ](../../strongs/h/h8091.md) and [YᵊʿÎ'Ēl](../../strongs/h/h3273.md) the [ben](../../strongs/h/h1121.md) of [Ḥôṯām](../../strongs/h/h2369.md) the [ʿĂrōʿērî](../../strongs/h/h6200.md),

<a name="1chronicles_11_45"></a>1Chronicles 11:45

[YᵊḏîʿĂ'Ēl](../../strongs/h/h3043.md) the [ben](../../strongs/h/h1121.md) of [Šimrî](../../strongs/h/h8113.md), and [Yôḥā'](../../strongs/h/h3109.md) his ['ach](../../strongs/h/h251.md), the [Tîṣî](../../strongs/h/h8491.md),

<a name="1chronicles_11_46"></a>1Chronicles 11:46

['Ĕlî'Ēl](../../strongs/h/h447.md) the [Maḥăvîm](../../strongs/h/h4233.md), and [Yᵊrîḇay](../../strongs/h/h3403.md), and [Yôšavyâ](../../strongs/h/h3145.md), the [ben](../../strongs/h/h1121.md) of ['ElnaʿAm](../../strongs/h/h493.md), and [Yiṯmâ](../../strongs/h/h3495.md) the [Mô'āḇî](../../strongs/h/h4125.md),

<a name="1chronicles_11_47"></a>1Chronicles 11:47

['Ĕlî'Ēl](../../strongs/h/h447.md), and [ʿÔḇēḏ](../../strongs/h/h5744.md), and [YaʿĂśî'Ēl](../../strongs/h/h3300.md) the [Mᵊṣōḇāyâ](../../strongs/h/h4677.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 10](1chronicles_10.md) - [1Chronicles 12](1chronicles_12.md)