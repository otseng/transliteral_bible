# [1Chronicles 23](https://www.blueletterbible.org/kjv/1chronicles/23)

<a name="1chronicles_23_1"></a>1Chronicles 23:1

So when [Dāviḏ](../../strongs/h/h1732.md) was [zāqēn](../../strongs/h/h2204.md) and [sāׂbaʿ](../../strongs/h/h7646.md) of [yowm](../../strongs/h/h3117.md), he made [Šᵊlōmô](../../strongs/h/h8010.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_23_2"></a>1Chronicles 23:2

And he ['āsap̄](../../strongs/h/h622.md) all the [śar](../../strongs/h/h8269.md) of [Yisra'el](../../strongs/h/h3478.md), with the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md).

<a name="1chronicles_23_3"></a>1Chronicles 23:3

Now the [Lᵊvî](../../strongs/h/h3881.md) were [sāp̄ar](../../strongs/h/h5608.md) from the [ben](../../strongs/h/h1121.md) of thirty [šānâ](../../strongs/h/h8141.md) and [maʿal](../../strongs/h/h4605.md): and their [mispār](../../strongs/h/h4557.md) by their [gulgōleṯ](../../strongs/h/h1538.md), man by [geḇer](../../strongs/h/h1397.md), was thirty and eight thousand.

<a name="1chronicles_23_4"></a>1Chronicles 23:4

Of which, twenty and four thousand were to set [nāṣaḥ](../../strongs/h/h5329.md) the [mĕla'kah](../../strongs/h/h4399.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md); and six thousand were [šāṭar](../../strongs/h/h7860.md) and [shaphat](../../strongs/h/h8199.md):

<a name="1chronicles_23_5"></a>1Chronicles 23:5

Moreover four thousand were [šôʿēr](../../strongs/h/h7778.md); and four thousand [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md) with the [kĕliy](../../strongs/h/h3627.md) which I ['asah](../../strongs/h/h6213.md), to [halal](../../strongs/h/h1984.md) therewith.

<a name="1chronicles_23_6"></a>1Chronicles 23:6

And [Dāviḏ](../../strongs/h/h1732.md) [chalaq](../../strongs/h/h2505.md) them into [maḥălōqeṯ](../../strongs/h/h4256.md) among the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md), namely, [Gēršôn](../../strongs/h/h1648.md), [Qᵊhāṯ](../../strongs/h/h6955.md), and [Mᵊrārî](../../strongs/h/h4847.md).

<a name="1chronicles_23_7"></a>1Chronicles 23:7

Of the [Gēršunnî](../../strongs/h/h1649.md) were, [LaʿDān](../../strongs/h/h3936.md), and [Šimʿî](../../strongs/h/h8096.md).

<a name="1chronicles_23_8"></a>1Chronicles 23:8

The [ben](../../strongs/h/h1121.md) of [LaʿDān](../../strongs/h/h3936.md); the [ro'sh](../../strongs/h/h7218.md) was [Yᵊḥî'Ēl](../../strongs/h/h3171.md), and [Zēṯām](../../strongs/h/h2241.md), and [Yô'Ēl](../../strongs/h/h3100.md), three.

<a name="1chronicles_23_9"></a>1Chronicles 23:9

The [ben](../../strongs/h/h1121.md) of [Šimʿî](../../strongs/h/h8096.md); [Šᵊlōmîṯ](../../strongs/h/h8019.md) [Śᵊlōmôṯ](../../strongs/h/h8013.md), and [Ḥăzî'Ēl](../../strongs/h/h2381.md), and [Hārān](../../strongs/h/h2039.md), three. These were the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of [LaʿDān](../../strongs/h/h3936.md).

<a name="1chronicles_23_10"></a>1Chronicles 23:10

And the [ben](../../strongs/h/h1121.md) of [Šimʿî](../../strongs/h/h8096.md) were, [Yaḥaṯ](../../strongs/h/h3189.md), [Zînā'](../../strongs/h/h2126.md), and [Yᵊʿûš](../../strongs/h/h3266.md), and [Bᵊrîʿâ](../../strongs/h/h1283.md). These four were the [ben](../../strongs/h/h1121.md) of [Šimʿî](../../strongs/h/h8096.md).

<a name="1chronicles_23_11"></a>1Chronicles 23:11

And [Yaḥaṯ](../../strongs/h/h3189.md) was the [ro'sh](../../strongs/h/h7218.md), and [Zîzâ](../../strongs/h/h2125.md) the second: but [Yᵊʿûš](../../strongs/h/h3266.md) and [Bᵊrîʿâ](../../strongs/h/h1283.md) had not [rabah](../../strongs/h/h7235.md) [ben](../../strongs/h/h1121.md); therefore they were in one [pᵊqudâ](../../strongs/h/h6486.md), according to their ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md).

<a name="1chronicles_23_12"></a>1Chronicles 23:12

The [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md); [ʿAmrām](../../strongs/h/h6019.md), [Yiṣhār](../../strongs/h/h3324.md), [Ḥeḇrôn](../../strongs/h/h2275.md), and [ʿUzzî'ēl](../../strongs/h/h5816.md), four.

<a name="1chronicles_23_13"></a>1Chronicles 23:13

The [ben](../../strongs/h/h1121.md) of [ʿAmrām](../../strongs/h/h6019.md); ['Ahărôn](../../strongs/h/h175.md) and [Mōshe](../../strongs/h/h4872.md): and ['Ahărôn](../../strongs/h/h175.md) was [bāḏal](../../strongs/h/h914.md), that he should [qadash](../../strongs/h/h6942.md) the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md), he and his [ben](../../strongs/h/h1121.md) ['owlam](../../strongs/h/h5769.md), to [qāṭar](../../strongs/h/h6999.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), to [sharath](../../strongs/h/h8334.md) unto him, and to [barak](../../strongs/h/h1288.md) in his [shem](../../strongs/h/h8034.md) ['owlam](../../strongs/h/h5769.md).

<a name="1chronicles_23_14"></a>1Chronicles 23:14

Now concerning [Mōshe](../../strongs/h/h4872.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), his [ben](../../strongs/h/h1121.md) were [qara'](../../strongs/h/h7121.md) of the [shebet](../../strongs/h/h7626.md) of [Lēvî](../../strongs/h/h3878.md).

<a name="1chronicles_23_15"></a>1Chronicles 23:15

The [ben](../../strongs/h/h1121.md) of [Mōshe](../../strongs/h/h4872.md) were, [Gēršōm](../../strongs/h/h1647.md), and ['Ĕlîʿezer](../../strongs/h/h461.md).

<a name="1chronicles_23_16"></a>1Chronicles 23:16

Of the [ben](../../strongs/h/h1121.md) of [Gēršōm](../../strongs/h/h1647.md), [Šᵊḇû'Ēl](../../strongs/h/h7619.md) was the [ro'sh](../../strongs/h/h7218.md).

<a name="1chronicles_23_17"></a>1Chronicles 23:17

And the [ben](../../strongs/h/h1121.md) of ['Ĕlîʿezer](../../strongs/h/h461.md) were, [Rᵊḥaḇyâ](../../strongs/h/h7345.md) the [ro'sh](../../strongs/h/h7218.md). And ['Ĕlîʿezer](../../strongs/h/h461.md) had none ['aḥēr](../../strongs/h/h312.md) [ben](../../strongs/h/h1121.md); but the [ben](../../strongs/h/h1121.md) of [Rᵊḥaḇyâ](../../strongs/h/h7345.md) were [maʿal](../../strongs/h/h4605.md) [rabah](../../strongs/h/h7235.md).

<a name="1chronicles_23_18"></a>1Chronicles 23:18

Of the [ben](../../strongs/h/h1121.md) of [Yiṣhār](../../strongs/h/h3324.md); [Šᵊlōmîṯ](../../strongs/h/h8019.md) the [ro'sh](../../strongs/h/h7218.md).

<a name="1chronicles_23_19"></a>1Chronicles 23:19

Of the [ben](../../strongs/h/h1121.md) of [Ḥeḇrôn](../../strongs/h/h2275.md); [Yᵊrîyâ](../../strongs/h/h3404.md) the [ro'sh](../../strongs/h/h7218.md), ['Ămaryâ](../../strongs/h/h568.md) the second, [Yaḥăzî'Ēl](../../strongs/h/h3166.md) the third, and [YᵊqamʿĀm](../../strongs/h/h3360.md) the fourth.

<a name="1chronicles_23_20"></a>1Chronicles 23:20

Of the [ben](../../strongs/h/h1121.md) of [ʿUzzî'ēl](../../strongs/h/h5816.md); [Mîḵâ](../../strongs/h/h4318.md) the [ro'sh](../../strongs/h/h7218.md), and [Yiššîyâ](../../strongs/h/h3449.md) the second.

<a name="1chronicles_23_21"></a>1Chronicles 23:21

The [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md); [Maḥlî](../../strongs/h/h4249.md), and [Mûšî](../../strongs/h/h4187.md). The [ben](../../strongs/h/h1121.md) of [Maḥlî](../../strongs/h/h4249.md); ['Elʿāzār](../../strongs/h/h499.md), and [Qîš](../../strongs/h/h7027.md).

<a name="1chronicles_23_22"></a>1Chronicles 23:22

And ['Elʿāzār](../../strongs/h/h499.md) [muwth](../../strongs/h/h4191.md), and had no [ben](../../strongs/h/h1121.md), but [bath](../../strongs/h/h1323.md): and their ['ach](../../strongs/h/h251.md) the [ben](../../strongs/h/h1121.md) of [Qîš](../../strongs/h/h7027.md) [nasa'](../../strongs/h/h5375.md) them.

<a name="1chronicles_23_23"></a>1Chronicles 23:23

The [ben](../../strongs/h/h1121.md) of [Mûšî](../../strongs/h/h4187.md); [Maḥlî](../../strongs/h/h4249.md), and [ʿĒḏer](../../strongs/h/h5740.md), and [Yᵊrêmôṯ](../../strongs/h/h3406.md), three.

<a name="1chronicles_23_24"></a>1Chronicles 23:24

These were the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md) after the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md); even the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md), as they were [paqad](../../strongs/h/h6485.md) by [mispār](../../strongs/h/h4557.md) of [shem](../../strongs/h/h8034.md) by their [gulgōleṯ](../../strongs/h/h1538.md), that ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md) for the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), from the [ben](../../strongs/h/h1121.md) of twenty [šānâ](../../strongs/h/h8141.md) and [maʿal](../../strongs/h/h4605.md).

<a name="1chronicles_23_25"></a>1Chronicles 23:25

For [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) hath given [nuwach](../../strongs/h/h5117.md) unto his ['am](../../strongs/h/h5971.md), that they may [shakan](../../strongs/h/h7931.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) ['owlam](../../strongs/h/h5769.md):

<a name="1chronicles_23_26"></a>1Chronicles 23:26

And also unto the [Lᵊvî](../../strongs/h/h3881.md); they shall no more [nasa'](../../strongs/h/h5375.md) the [miškān](../../strongs/h/h4908.md), nor any [kĕliy](../../strongs/h/h3627.md) of it for the [ʿăḇōḏâ](../../strongs/h/h5656.md) thereof.

<a name="1chronicles_23_27"></a>1Chronicles 23:27

For by the ['aḥărôn](../../strongs/h/h314.md) [dabar](../../strongs/h/h1697.md) of [Dāviḏ](../../strongs/h/h1732.md) the [Lᵊvî](../../strongs/h/h3881.md) were [mispār](../../strongs/h/h4557.md) from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md):

<a name="1chronicles_23_28"></a>1Chronicles 23:28

Because their [maʿămāḏ](../../strongs/h/h4612.md) was to [yad](../../strongs/h/h3027.md) on the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) for the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), in the [ḥāṣēr](../../strongs/h/h2691.md), and in the [liškâ](../../strongs/h/h3957.md), and in the [ṭāhŏrâ](../../strongs/h/h2893.md) of all [qodesh](../../strongs/h/h6944.md), and the [ma'aseh](../../strongs/h/h4639.md) of the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md);

<a name="1chronicles_23_29"></a>1Chronicles 23:29

Both for the [lechem](../../strongs/h/h3899.md) [maʿăreḵeṯ](../../strongs/h/h4635.md), and for the [sōleṯ](../../strongs/h/h5560.md) for [minchah](../../strongs/h/h4503.md), and for the [maṣṣâ](../../strongs/h/h4682.md) [rāqîq](../../strongs/h/h7550.md), and in the [maḥăḇaṯ](../../strongs/h/h4227.md), and for that which is [rāḇaḵ](../../strongs/h/h7246.md), and for all manner of [mᵊśûrâ](../../strongs/h/h4884.md) and [midâ](../../strongs/h/h4060.md);

<a name="1chronicles_23_30"></a>1Chronicles 23:30

And to ['amad](../../strongs/h/h5975.md) every [boqer](../../strongs/h/h1242.md) to [yadah](../../strongs/h/h3034.md) and [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md), and likewise at ['ereb](../../strongs/h/h6153.md);

<a name="1chronicles_23_31"></a>1Chronicles 23:31

And to [ʿālâ](../../strongs/h/h5927.md) all [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md) in the [shabbath](../../strongs/h/h7676.md), in the [ḥōḏeš](../../strongs/h/h2320.md), and on the set [môʿēḏ](../../strongs/h/h4150.md), by [mispār](../../strongs/h/h4557.md), according to the [mishpat](../../strongs/h/h4941.md) commanded unto them, [tāmîḏ](../../strongs/h/h8548.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md):

<a name="1chronicles_23_32"></a>1Chronicles 23:32

And that they should [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and the [mišmereṯ](../../strongs/h/h4931.md) of the [qodesh](../../strongs/h/h6944.md), and the [mišmereṯ](../../strongs/h/h4931.md) of the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) their ['ach](../../strongs/h/h251.md), in the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 22](1chronicles_22.md) - [1Chronicles 24](1chronicles_24.md)