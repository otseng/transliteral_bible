# [1Chronicles 17](https://www.blueletterbible.org/kjv/1chronicles/17)

<a name="1chronicles_17_1"></a>1Chronicles 17:1

Now it came to pass, as [Dāviḏ](../../strongs/h/h1732.md) [yashab](../../strongs/h/h3427.md) in his [bayith](../../strongs/h/h1004.md), that [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md), Lo, I [yashab](../../strongs/h/h3427.md) in a [bayith](../../strongs/h/h1004.md) of ['erez](../../strongs/h/h730.md), but the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) remaineth under [yᵊrîʿâ](../../strongs/h/h3407.md).

<a name="1chronicles_17_2"></a>1Chronicles 17:2

Then [Nāṯān](../../strongs/h/h5416.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), ['asah](../../strongs/h/h6213.md) all that is in thine [lebab](../../strongs/h/h3824.md); for ['Elohiym](../../strongs/h/h430.md) is with thee.

<a name="1chronicles_17_3"></a>1Chronicles 17:3

And it came to pass the same [layil](../../strongs/h/h3915.md), that the [dabar](../../strongs/h/h1697.md) of ['Elohiym](../../strongs/h/h430.md) came to [Nāṯān](../../strongs/h/h5416.md), ['āmar](../../strongs/h/h559.md),

<a name="1chronicles_17_4"></a>1Chronicles 17:4

[yālaḵ](../../strongs/h/h3212.md) and ['āmar](../../strongs/h/h559.md) [Dāviḏ](../../strongs/h/h1732.md) my ['ebed](../../strongs/h/h5650.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Thou shalt not [bānâ](../../strongs/h/h1129.md) me a [bayith](../../strongs/h/h1004.md) to [yashab](../../strongs/h/h3427.md):

<a name="1chronicles_17_5"></a>1Chronicles 17:5

For I have not [yashab](../../strongs/h/h3427.md) a [bayith](../../strongs/h/h1004.md) since the [yowm](../../strongs/h/h3117.md) that I [ʿālâ](../../strongs/h/h5927.md) [Yisra'el](../../strongs/h/h3478.md) unto this [yowm](../../strongs/h/h3117.md); but have gone from ['ohel](../../strongs/h/h168.md) to ['ohel](../../strongs/h/h168.md), and from one [miškān](../../strongs/h/h4908.md) to another.

<a name="1chronicles_17_6"></a>1Chronicles 17:6

Wheresoever I have [halak](../../strongs/h/h1980.md) with all [Yisra'el](../../strongs/h/h3478.md), [dabar](../../strongs/h/h1696.md) I a [dabar](../../strongs/h/h1697.md) to any of the [shaphat](../../strongs/h/h8199.md) of [Yisra'el](../../strongs/h/h3478.md), whom I [tsavah](../../strongs/h/h6680.md) to [ra'ah](../../strongs/h/h7462.md) my ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), Why have ye not [bānâ](../../strongs/h/h1129.md) me a [bayith](../../strongs/h/h1004.md) of ['erez](../../strongs/h/h730.md)?

<a name="1chronicles_17_7"></a>1Chronicles 17:7

Now therefore thus shalt thou ['āmar](../../strongs/h/h559.md) unto my ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), I [laqach](../../strongs/h/h3947.md) thee from the [nāvê](../../strongs/h/h5116.md), even from ['aḥar](../../strongs/h/h310.md) the [tso'n](../../strongs/h/h6629.md), that thou shouldest be [nāḡîḏ](../../strongs/h/h5057.md) over my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md):

<a name="1chronicles_17_8"></a>1Chronicles 17:8

And I have been with thee whithersoever thou hast [halak](../../strongs/h/h1980.md), and have [karath](../../strongs/h/h3772.md) all thine ['oyeb](../../strongs/h/h341.md) from [paniym](../../strongs/h/h6440.md) thee, and have ['asah](../../strongs/h/h6213.md) thee a [shem](../../strongs/h/h8034.md) like the [shem](../../strongs/h/h8034.md) of the [gadowl](../../strongs/h/h1419.md) that are in the ['erets](../../strongs/h/h776.md).

<a name="1chronicles_17_9"></a>1Chronicles 17:9

Also I will [śûm](../../strongs/h/h7760.md) a [maqowm](../../strongs/h/h4725.md) for my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), and will [nāṭaʿ](../../strongs/h/h5193.md) them, and they shall [shakan](../../strongs/h/h7931.md) in their place, and shall be [ragaz](../../strongs/h/h7264.md) no more; neither shall the [ben](../../strongs/h/h1121.md) of ['evel](../../strongs/h/h5766.md) [bālâ](../../strongs/h/h1086.md) them any more, as at the [ri'šôn](../../strongs/h/h7223.md),

<a name="1chronicles_17_10"></a>1Chronicles 17:10

And since the [yowm](../../strongs/h/h3117.md) that I [tsavah](../../strongs/h/h6680.md) [shaphat](../../strongs/h/h8199.md) to be over my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md). Moreover I will [kānaʿ](../../strongs/h/h3665.md) all thine ['oyeb](../../strongs/h/h341.md). Furthermore I [nāḡaḏ](../../strongs/h/h5046.md) thee that [Yĕhovah](../../strongs/h/h3068.md) will [bānâ](../../strongs/h/h1129.md) thee a [bayith](../../strongs/h/h1004.md).

<a name="1chronicles_17_11"></a>1Chronicles 17:11

And it shall come to pass, when thy [yowm](../../strongs/h/h3117.md) be [mālā'](../../strongs/h/h4390.md) that thou must [yālaḵ](../../strongs/h/h3212.md) to be with thy ['ab](../../strongs/h/h1.md), that I will [quwm](../../strongs/h/h6965.md) thy [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) thee, which shall be of thy [ben](../../strongs/h/h1121.md); and I will [kuwn](../../strongs/h/h3559.md) his [malkuwth](../../strongs/h/h4438.md).

<a name="1chronicles_17_12"></a>1Chronicles 17:12

He shall [bānâ](../../strongs/h/h1129.md) me a [bayith](../../strongs/h/h1004.md), and I will [kuwn](../../strongs/h/h3559.md) his [kicce'](../../strongs/h/h3678.md) ['owlam](../../strongs/h/h5769.md).

<a name="1chronicles_17_13"></a>1Chronicles 17:13

I will be his ['ab](../../strongs/h/h1.md), and he shall be my [ben](../../strongs/h/h1121.md): and I will not [cuwr](../../strongs/h/h5493.md) my [checed](../../strongs/h/h2617.md) [cuwr](../../strongs/h/h5493.md) from him, as I [cuwr](../../strongs/h/h5493.md) it from him that was [paniym](../../strongs/h/h6440.md) thee:

<a name="1chronicles_17_14"></a>1Chronicles 17:14

But I will ['amad](../../strongs/h/h5975.md) him in mine [bayith](../../strongs/h/h1004.md) and in my [malkuwth](../../strongs/h/h4438.md) ['owlam](../../strongs/h/h5769.md): and his [kicce'](../../strongs/h/h3678.md) shall be [kuwn](../../strongs/h/h3559.md) ['owlam](../../strongs/h/h5769.md).

<a name="1chronicles_17_15"></a>1Chronicles 17:15

According to all these [dabar](../../strongs/h/h1697.md), and according to all this [ḥāzôn](../../strongs/h/h2377.md), so did [Nāṯān](../../strongs/h/h5416.md) [dabar](../../strongs/h/h1696.md) unto [Dāviḏ](../../strongs/h/h1732.md).

<a name="1chronicles_17_16"></a>1Chronicles 17:16

And [Dāviḏ](../../strongs/h/h1732.md) the [melek](../../strongs/h/h4428.md) [bow'](../../strongs/h/h935.md) and [yashab](../../strongs/h/h3427.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), Who am I, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), and what is mine [bayith](../../strongs/h/h1004.md), that thou hast [bow'](../../strongs/h/h935.md) me hitherto?

<a name="1chronicles_17_17"></a>1Chronicles 17:17

And yet this was a [qāṭōn](../../strongs/h/h6994.md) in thine ['ayin](../../strongs/h/h5869.md), ['Elohiym](../../strongs/h/h430.md); for thou hast also [dabar](../../strongs/h/h1696.md) of thy ['ebed](../../strongs/h/h5650.md) [bayith](../../strongs/h/h1004.md) for a great while to [rachowq](../../strongs/h/h7350.md), and hast [ra'ah](../../strongs/h/h7200.md) me according to the [tôr](../../strongs/h/h8448.md) of an ['āḏām](../../strongs/h/h120.md) of [maʿălâ](../../strongs/h/h4609.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md).

<a name="1chronicles_17_18"></a>1Chronicles 17:18

What can [Dāviḏ](../../strongs/h/h1732.md) speak more to thee for the [kabowd](../../strongs/h/h3519.md) of thy ['ebed](../../strongs/h/h5650.md)? for thou [yada'](../../strongs/h/h3045.md) thy ['ebed](../../strongs/h/h5650.md).

<a name="1chronicles_17_19"></a>1Chronicles 17:19

[Yĕhovah](../../strongs/h/h3068.md), for thy ['ebed](../../strongs/h/h5650.md) sake, and according to thine own [leb](../../strongs/h/h3820.md), hast thou ['asah](../../strongs/h/h6213.md) all this [gᵊḏûlâ](../../strongs/h/h1420.md), in making [yada'](../../strongs/h/h3045.md) all these [gᵊḏûlâ](../../strongs/h/h1420.md).

<a name="1chronicles_17_20"></a>1Chronicles 17:20

[Yĕhovah](../../strongs/h/h3068.md), there is none like thee, neither is there any ['Elohiym](../../strongs/h/h430.md) [zûlâ](../../strongs/h/h2108.md) thee, according to all that we have [shama'](../../strongs/h/h8085.md) with our ['ozen](../../strongs/h/h241.md).

<a name="1chronicles_17_21"></a>1Chronicles 17:21

And what one [gowy](../../strongs/h/h1471.md) in the ['erets](../../strongs/h/h776.md) is like thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), whom ['Elohiym](../../strongs/h/h430.md) [halak](../../strongs/h/h1980.md) to [pāḏâ](../../strongs/h/h6299.md) to be his own ['am](../../strongs/h/h5971.md), to [śûm](../../strongs/h/h7760.md) thee a [shem](../../strongs/h/h8034.md) of [gᵊḏûlâ](../../strongs/h/h1420.md) and [yare'](../../strongs/h/h3372.md), by driving [gāraš](../../strongs/h/h1644.md) [gowy](../../strongs/h/h1471.md) from [paniym](../../strongs/h/h6440.md) thy ['am](../../strongs/h/h5971.md), whom thou hast [pāḏâ](../../strongs/h/h6299.md) out of [Mitsrayim](../../strongs/h/h4714.md)?

<a name="1chronicles_17_22"></a>1Chronicles 17:22

For thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) didst thou [nathan](../../strongs/h/h5414.md) thine own ['am](../../strongs/h/h5971.md) ['owlam](../../strongs/h/h5769.md); and thou, [Yĕhovah](../../strongs/h/h3068.md), becamest their ['Elohiym](../../strongs/h/h430.md).

<a name="1chronicles_17_23"></a>1Chronicles 17:23

Therefore now, [Yĕhovah](../../strongs/h/h3068.md), let the [dabar](../../strongs/h/h1697.md) that thou hast [dabar](../../strongs/h/h1696.md) concerning thy ['ebed](../../strongs/h/h5650.md) and concerning his [bayith](../../strongs/h/h1004.md) be ['aman](../../strongs/h/h539.md) ['owlam](../../strongs/h/h5769.md), and ['asah](../../strongs/h/h6213.md) as thou hast [dabar](../../strongs/h/h1696.md).

<a name="1chronicles_17_24"></a>1Chronicles 17:24

Let it even be ['aman](../../strongs/h/h539.md), that thy [shem](../../strongs/h/h8034.md) may be [gāḏal](../../strongs/h/h1431.md) ['owlam](../../strongs/h/h5769.md), ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), even an ['Elohiym](../../strongs/h/h430.md) to [Yisra'el](../../strongs/h/h3478.md): and let the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md) thy ['ebed](../../strongs/h/h5650.md) be [kuwn](../../strongs/h/h3559.md) [paniym](../../strongs/h/h6440.md) thee.

<a name="1chronicles_17_25"></a>1Chronicles 17:25

For thou, O my ['Elohiym](../../strongs/h/h430.md), hast [gālâ](../../strongs/h/h1540.md) ['ozen](../../strongs/h/h241.md) thy ['ebed](../../strongs/h/h5650.md) that thou wilt [bānâ](../../strongs/h/h1129.md) him a [bayith](../../strongs/h/h1004.md): therefore thy ['ebed](../../strongs/h/h5650.md) hath [māṣā'](../../strongs/h/h4672.md) to [palal](../../strongs/h/h6419.md) [paniym](../../strongs/h/h6440.md) thee.

<a name="1chronicles_17_26"></a>1Chronicles 17:26

And now, [Yĕhovah](../../strongs/h/h3068.md), thou art ['Elohiym](../../strongs/h/h430.md), and hast [dabar](../../strongs/h/h1696.md) this [towb](../../strongs/h/h2896.md) unto thy ['ebed](../../strongs/h/h5650.md):

<a name="1chronicles_17_27"></a>1Chronicles 17:27

Now therefore let it [yā'al](../../strongs/h/h2974.md) thee to [barak](../../strongs/h/h1288.md) the [bayith](../../strongs/h/h1004.md) of thy ['ebed](../../strongs/h/h5650.md), that it may be [paniym](../../strongs/h/h6440.md) thee ['owlam](../../strongs/h/h5769.md): for thou [barak](../../strongs/h/h1288.md), [Yĕhovah](../../strongs/h/h3068.md), and it shall be [barak](../../strongs/h/h1288.md) ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 16](1chronicles_16.md) - [1Chronicles 18](1chronicles_18.md)