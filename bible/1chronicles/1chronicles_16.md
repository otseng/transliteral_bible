# [1Chronicles 16](https://www.blueletterbible.org/kjv/1chronicles/16)

<a name="1chronicles_16_1"></a>1Chronicles 16:1

So they [bow'](../../strongs/h/h935.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md), and [yāṣaḡ](../../strongs/h/h3322.md) it in the [tavek](../../strongs/h/h8432.md) of the ['ohel](../../strongs/h/h168.md) that [Dāviḏ](../../strongs/h/h1732.md) had [natah](../../strongs/h/h5186.md) for it: and they [qāraḇ](../../strongs/h/h7126.md) [ʿōlâ](../../strongs/h/h5930.md) and [šelem](../../strongs/h/h8002.md) [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md).

<a name="1chronicles_16_2"></a>1Chronicles 16:2

And when [Dāviḏ](../../strongs/h/h1732.md) had made a [kalah](../../strongs/h/h3615.md) of [ʿālâ](../../strongs/h/h5927.md) the [ʿōlâ](../../strongs/h/h5930.md) and the [šelem](../../strongs/h/h8002.md), he [barak](../../strongs/h/h1288.md) the ['am](../../strongs/h/h5971.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1chronicles_16_3"></a>1Chronicles 16:3

And he [chalaq](../../strongs/h/h2505.md) to every ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md), both ['iysh](../../strongs/h/h376.md) and ['ishshah](../../strongs/h/h802.md), to every ['iysh](../../strongs/h/h376.md) a [kikār](../../strongs/h/h3603.md) of [lechem](../../strongs/h/h3899.md), and an ['ešpār](../../strongs/h/h829.md), and a ['ăšîšâ](../../strongs/h/h809.md).

<a name="1chronicles_16_4"></a>1Chronicles 16:4

And he [nathan](../../strongs/h/h5414.md) certain of the [Lᵊvî](../../strongs/h/h3881.md) to [sharath](../../strongs/h/h8334.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md), and to [zakar](../../strongs/h/h2142.md), and to [yadah](../../strongs/h/h3034.md) and [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="1chronicles_16_5"></a>1Chronicles 16:5

['Āsāp̄](../../strongs/h/h623.md) the [ro'sh](../../strongs/h/h7218.md), and [mišnê](../../strongs/h/h4932.md) to him [Zᵊḵaryâ](../../strongs/h/h2148.md), [YᵊʿÎ'Ēl](../../strongs/h/h3273.md), and [Šᵊmîrāmôṯ](../../strongs/h/h8070.md), and [Yᵊḥî'Ēl](../../strongs/h/h3171.md), and [Mataṯyâ](../../strongs/h/h4993.md), and ['Ĕlî'āḇ](../../strongs/h/h446.md), and [Bᵊnāyâ](../../strongs/h/h1141.md), and [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md): and [YᵊʿÎ'Ēl](../../strongs/h/h3273.md) with [neḇel](../../strongs/h/h5035.md) [kĕliy](../../strongs/h/h3627.md) and with [kinnôr](../../strongs/h/h3658.md); but ['Āsāp̄](../../strongs/h/h623.md) made a [shama'](../../strongs/h/h8085.md) with [mᵊṣēleṯ](../../strongs/h/h4700.md);

<a name="1chronicles_16_6"></a>1Chronicles 16:6

[Bᵊnāyâ](../../strongs/h/h1141.md) also and [Yaḥăzî'Ēl](../../strongs/h/h3166.md) the [kōhēn](../../strongs/h/h3548.md) with [ḥăṣōṣrâ](../../strongs/h/h2689.md) [tāmîḏ](../../strongs/h/h8548.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="1chronicles_16_7"></a>1Chronicles 16:7

Then on that [yowm](../../strongs/h/h3117.md) [Dāviḏ](../../strongs/h/h1732.md) [nathan](../../strongs/h/h5414.md) [ro'sh](../../strongs/h/h7218.md) this to [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md) into the [yad](../../strongs/h/h3027.md) of ['Āsāp̄](../../strongs/h/h623.md) and his ['ach](../../strongs/h/h251.md).

<a name="1chronicles_16_8"></a>1Chronicles 16:8

[yadah](../../strongs/h/h3034.md) unto [Yĕhovah](../../strongs/h/h3068.md), [qara'](../../strongs/h/h7121.md) upon his [shem](../../strongs/h/h8034.md), [yada'](../../strongs/h/h3045.md) his ['aliylah](../../strongs/h/h5949.md) among the ['am](../../strongs/h/h5971.md).

<a name="1chronicles_16_9"></a>1Chronicles 16:9

[shiyr](../../strongs/h/h7891.md) unto him, [zamar](../../strongs/h/h2167.md) unto him, [śîaḥ](../../strongs/h/h7878.md) ye of all his [pala'](../../strongs/h/h6381.md).

<a name="1chronicles_16_10"></a>1Chronicles 16:10

[halal](../../strongs/h/h1984.md) ye in his [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md): let the [leb](../../strongs/h/h3820.md) of them [samach](../../strongs/h/h8055.md) that [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="1chronicles_16_11"></a>1Chronicles 16:11

[darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md) and his ['oz](../../strongs/h/h5797.md), [bāqaš](../../strongs/h/h1245.md) his [paniym](../../strongs/h/h6440.md) [tāmîḏ](../../strongs/h/h8548.md).

<a name="1chronicles_16_12"></a>1Chronicles 16:12

[zakar](../../strongs/h/h2142.md) his [pala'](../../strongs/h/h6381.md) that he hath ['asah](../../strongs/h/h6213.md), his [môp̄ēṯ](../../strongs/h/h4159.md), and the [mishpat](../../strongs/h/h4941.md) of his [peh](../../strongs/h/h6310.md);

<a name="1chronicles_16_13"></a>1Chronicles 16:13

O ye [zera'](../../strongs/h/h2233.md) of [Yisra'el](../../strongs/h/h3478.md) his ['ebed](../../strongs/h/h5650.md), ye [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md), his [bāḥîr](../../strongs/h/h972.md).

<a name="1chronicles_16_14"></a>1Chronicles 16:14

He is [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md); his [mishpat](../../strongs/h/h4941.md) are in all the ['erets](../../strongs/h/h776.md).

<a name="1chronicles_16_15"></a>1Chronicles 16:15

Be ye [zakar](../../strongs/h/h2142.md) ['owlam](../../strongs/h/h5769.md) of his [bĕriyth](../../strongs/h/h1285.md); the [dabar](../../strongs/h/h1697.md) which he [tsavah](../../strongs/h/h6680.md) to a thousand [dôr](../../strongs/h/h1755.md);

<a name="1chronicles_16_16"></a>1Chronicles 16:16

Even which he [karath](../../strongs/h/h3772.md) with ['Abraham](../../strongs/h/h85.md), and of his [šᵊḇûʿâ](../../strongs/h/h7621.md) unto [Yiṣḥāq](../../strongs/h/h3327.md);

<a name="1chronicles_16_17"></a>1Chronicles 16:17

And hath ['amad](../../strongs/h/h5975.md) the same to [Ya'aqob](../../strongs/h/h3290.md) for a [choq](../../strongs/h/h2706.md), and to [Yisra'el](../../strongs/h/h3478.md) for an ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md),

<a name="1chronicles_16_18"></a>1Chronicles 16:18

['āmar](../../strongs/h/h559.md), Unto thee will I [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), the [chebel](../../strongs/h/h2256.md) of your [nachalah](../../strongs/h/h5159.md);

<a name="1chronicles_16_19"></a>1Chronicles 16:19

When ye were but [mispār](../../strongs/h/h4557.md) [math](../../strongs/h/h4962.md), even a [mᵊʿaṭ](../../strongs/h/h4592.md), and [guwr](../../strongs/h/h1481.md) in it.

<a name="1chronicles_16_20"></a>1Chronicles 16:20

And when they [halak](../../strongs/h/h1980.md) from [gowy](../../strongs/h/h1471.md) to [gowy](../../strongs/h/h1471.md), and from one [mamlāḵâ](../../strongs/h/h4467.md) to ['aḥēr](../../strongs/h/h312.md) ['am](../../strongs/h/h5971.md);

<a name="1chronicles_16_21"></a>1Chronicles 16:21

He [yānaḥ](../../strongs/h/h3240.md) no ['iysh](../../strongs/h/h376.md) to do them [ʿāšaq](../../strongs/h/h6231.md): yea, he [yakach](../../strongs/h/h3198.md) [melek](../../strongs/h/h4428.md) for their sakes,

<a name="1chronicles_16_22"></a>1Chronicles 16:22

[naga'](../../strongs/h/h5060.md) not mine [mashiyach](../../strongs/h/h4899.md), and do my [nāḇî'](../../strongs/h/h5030.md) no [ra'a'](../../strongs/h/h7489.md).

<a name="1chronicles_16_23"></a>1Chronicles 16:23

[shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md), all the ['erets](../../strongs/h/h776.md); [bāśar](../../strongs/h/h1319.md) from [yowm](../../strongs/h/h3117.md) to [yowm](../../strongs/h/h3117.md) his [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="1chronicles_16_24"></a>1Chronicles 16:24

[sāp̄ar](../../strongs/h/h5608.md) his [kabowd](../../strongs/h/h3519.md) among the [gowy](../../strongs/h/h1471.md); his [pala'](../../strongs/h/h6381.md) among all ['am](../../strongs/h/h5971.md).

<a name="1chronicles_16_25"></a>1Chronicles 16:25

For [gadowl](../../strongs/h/h1419.md) is [Yĕhovah](../../strongs/h/h3068.md), and [me'od](../../strongs/h/h3966.md) to be [halal](../../strongs/h/h1984.md): he also is to be [yare'](../../strongs/h/h3372.md) above all ['Elohiym](../../strongs/h/h430.md).

<a name="1chronicles_16_26"></a>1Chronicles 16:26

For all the ['Elohiym](../../strongs/h/h430.md) of the ['am](../../strongs/h/h5971.md) are ['ĕlîl](../../strongs/h/h457.md): but [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) the [shamayim](../../strongs/h/h8064.md).

<a name="1chronicles_16_27"></a>1Chronicles 16:27

[howd](../../strongs/h/h1935.md) and [hadar](../../strongs/h/h1926.md) are in his [paniym](../../strongs/h/h6440.md); ['oz](../../strongs/h/h5797.md) and [ḥeḏvâ](../../strongs/h/h2304.md) are in his [maqowm](../../strongs/h/h4725.md).

<a name="1chronicles_16_28"></a>1Chronicles 16:28

[yāhaḇ](../../strongs/h/h3051.md) unto [Yĕhovah](../../strongs/h/h3068.md), ye [mišpāḥâ](../../strongs/h/h4940.md) of the ['am](../../strongs/h/h5971.md), [yāhaḇ](../../strongs/h/h3051.md) unto [Yĕhovah](../../strongs/h/h3068.md) [kabowd](../../strongs/h/h3519.md) and ['oz](../../strongs/h/h5797.md).

<a name="1chronicles_16_29"></a>1Chronicles 16:29

[yāhaḇ](../../strongs/h/h3051.md) unto [Yĕhovah](../../strongs/h/h3068.md) the [kabowd](../../strongs/h/h3519.md) due unto his [shem](../../strongs/h/h8034.md): [nasa'](../../strongs/h/h5375.md) a [minchah](../../strongs/h/h4503.md), and [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) him: [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md) in the [hăḏārâ](../../strongs/h/h1927.md) of [qodesh](../../strongs/h/h6944.md).

<a name="1chronicles_16_30"></a>1Chronicles 16:30

[chuwl](../../strongs/h/h2342.md) [paniym](../../strongs/h/h6440.md) him, all the ['erets](../../strongs/h/h776.md): the [tebel](../../strongs/h/h8398.md) also shall be [kuwn](../../strongs/h/h3559.md), that it be not [mowt](../../strongs/h/h4131.md).

<a name="1chronicles_16_31"></a>1Chronicles 16:31

Let the [shamayim](../../strongs/h/h8064.md) be [samach](../../strongs/h/h8055.md), and let the ['erets](../../strongs/h/h776.md) [giyl](../../strongs/h/h1523.md): and let men ['āmar](../../strongs/h/h559.md) among the [gowy](../../strongs/h/h1471.md), [Yĕhovah](../../strongs/h/h3068.md) [mālaḵ](../../strongs/h/h4427.md).

<a name="1chronicles_16_32"></a>1Chronicles 16:32

Let the [yam](../../strongs/h/h3220.md) [ra'am](../../strongs/h/h7481.md), and the [mᵊlō'](../../strongs/h/h4393.md) thereof: let the [sadeh](../../strongs/h/h7704.md) ['alats](../../strongs/h/h5970.md), and all that is therein.

<a name="1chronicles_16_33"></a>1Chronicles 16:33

Then shall the ['ets](../../strongs/h/h6086.md) of the [yaʿar](../../strongs/h/h3293.md) [ranan](../../strongs/h/h7442.md) at the [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md), because he [bow'](../../strongs/h/h935.md) to [shaphat](../../strongs/h/h8199.md) the ['erets](../../strongs/h/h776.md).

<a name="1chronicles_16_34"></a>1Chronicles 16:34

[yadah](../../strongs/h/h3034.md) unto [Yĕhovah](../../strongs/h/h3068.md); for he is [towb](../../strongs/h/h2896.md); for his [checed](../../strongs/h/h2617.md) ['owlam](../../strongs/h/h5769.md).

<a name="1chronicles_16_35"></a>1Chronicles 16:35

And ['āmar](../../strongs/h/h559.md) ye, [yasha'](../../strongs/h/h3467.md) us, ['Elohiym](../../strongs/h/h430.md) of our [yesha'](../../strongs/h/h3468.md), and [qāḇaṣ](../../strongs/h/h6908.md) us, and [natsal](../../strongs/h/h5337.md) us from the [gowy](../../strongs/h/h1471.md), that we may [yadah](../../strongs/h/h3034.md) to thy [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md), and [shabach](../../strongs/h/h7623.md) in thy [tehillah](../../strongs/h/h8416.md).

<a name="1chronicles_16_36"></a>1Chronicles 16:36

[barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) ['owlam](../../strongs/h/h5769.md) and ['owlam](../../strongs/h/h5769.md). And all the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md), and [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="1chronicles_16_37"></a>1Chronicles 16:37

So he ['azab](../../strongs/h/h5800.md) there [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Āsāp̄](../../strongs/h/h623.md) and his ['ach](../../strongs/h/h251.md), to [sharath](../../strongs/h/h8334.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) [tāmîḏ](../../strongs/h/h8548.md), as every [yowm](../../strongs/h/h3117.md) [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md):

<a name="1chronicles_16_38"></a>1Chronicles 16:38

And [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md) with their ['ach](../../strongs/h/h251.md), threescore and eight; [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md) also the [ben](../../strongs/h/h1121.md) of [Yᵊḏûṯûn](../../strongs/h/h3038.md) and [Ḥōsâ](../../strongs/h/h2621.md) to be [šôʿēr](../../strongs/h/h7778.md):

<a name="1chronicles_16_39"></a>1Chronicles 16:39

And [Ṣāḏôq](../../strongs/h/h6659.md) the [kōhēn](../../strongs/h/h3548.md), and his ['ach](../../strongs/h/h251.md) the [kōhēn](../../strongs/h/h3548.md), [paniym](../../strongs/h/h6440.md) the [miškān](../../strongs/h/h4908.md) of [Yĕhovah](../../strongs/h/h3068.md) in the [bāmâ](../../strongs/h/h1116.md) that was at [Giḇʿôn](../../strongs/h/h1391.md),

<a name="1chronicles_16_40"></a>1Chronicles 16:40

To [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md) upon the [mizbeach](../../strongs/h/h4196.md) of the [ʿōlâ](../../strongs/h/h5930.md) [tāmîḏ](../../strongs/h/h8548.md) [boqer](../../strongs/h/h1242.md) and ['ereb](../../strongs/h/h6153.md), and to do according to all that is [kāṯaḇ](../../strongs/h/h3789.md) in the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [tsavah](../../strongs/h/h6680.md) [Yisra'el](../../strongs/h/h3478.md);

<a name="1chronicles_16_41"></a>1Chronicles 16:41

And with them [Hêmān](../../strongs/h/h1968.md) and [Yᵊḏûṯûn](../../strongs/h/h3038.md), and the [šᵊ'ār](../../strongs/h/h7605.md) that were [bārar](../../strongs/h/h1305.md), who were [nāqaḇ](../../strongs/h/h5344.md) by [shem](../../strongs/h/h8034.md), to [yadah](../../strongs/h/h3034.md) to [Yĕhovah](../../strongs/h/h3068.md), because his [checed](../../strongs/h/h2617.md) ['owlam](../../strongs/h/h5769.md);

<a name="1chronicles_16_42"></a>1Chronicles 16:42

And with them [Hêmān](../../strongs/h/h1968.md) and [Yᵊḏûṯûn](../../strongs/h/h3038.md) with [ḥăṣōṣrâ](../../strongs/h/h2689.md) and [mᵊṣēleṯ](../../strongs/h/h4700.md) for those that should make a [shama'](../../strongs/h/h8085.md), and with [šîr](../../strongs/h/h7892.md) [kĕliy](../../strongs/h/h3627.md) of ['Elohiym](../../strongs/h/h430.md). And the [ben](../../strongs/h/h1121.md) of [Yᵊḏûṯûn](../../strongs/h/h3038.md) were [sha'ar](../../strongs/h/h8179.md).

<a name="1chronicles_16_43"></a>1Chronicles 16:43

And all the ['am](../../strongs/h/h5971.md) [yālaḵ](../../strongs/h/h3212.md) every ['iysh](../../strongs/h/h376.md) to his [bayith](../../strongs/h/h1004.md): and [Dāviḏ](../../strongs/h/h1732.md) [cabab](../../strongs/h/h5437.md) to [barak](../../strongs/h/h1288.md) his [bayith](../../strongs/h/h1004.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 15](1chronicles_15.md) - [1Chronicles 17](1chronicles_17.md)