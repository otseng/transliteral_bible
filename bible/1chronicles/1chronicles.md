# 1 Chronicles

[1 Chronicles Overview](../../commentary/1chronicles/1chronicles_overview.md)

[1 Chronicles 1](1chronicles_1.md)

[1 Chronicles 2](1chronicles_2.md)

[1 Chronicles 3](1chronicles_3.md)

[1 Chronicles 4](1chronicles_4.md)

[1 Chronicles 5](1chronicles_5.md)

[1 Chronicles 6](1chronicles_6.md)

[1 Chronicles 7](1chronicles_7.md)

[1 Chronicles 8](1chronicles_8.md)

[1 Chronicles 9](1chronicles_9.md)

[1 Chronicles 10](1chronicles_10.md)

[1 Chronicles 11](1chronicles_11.md)

[1 Chronicles 12](1chronicles_12.md)

[1 Chronicles 13](1chronicles_13.md)

[1 Chronicles 14](1chronicles_14.md)

[1 Chronicles 15](1chronicles_15.md)

[1 Chronicles 16](1chronicles_16.md)

[1 Chronicles 17](1chronicles_17.md)

[1 Chronicles 18](1chronicles_18.md)

[1 Chronicles 19](1chronicles_19.md)

[1 Chronicles 20](1chronicles_20.md)

[1 Chronicles 21](1chronicles_21.md)

[1 Chronicles 22](1chronicles_22.md)

[1 Chronicles 23](1chronicles_23.md)

[1 Chronicles 24](1chronicles_24.md)

[1 Chronicles 25](1chronicles_25.md)

[1 Chronicles 26](1chronicles_26.md)

[1 Chronicles 27](1chronicles_27.md)

[1 Chronicles 28](1chronicles_28.md)

[1 Chronicles 29](1chronicles_29.md)

---

[Transliteral Bible](../index.md)