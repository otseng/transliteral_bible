# [1Chronicles 7](https://www.blueletterbible.org/kjv/1chronicles/7)

<a name="1chronicles_7_1"></a>1Chronicles 7:1

Now the [ben](../../strongs/h/h1121.md) of [Yiśśāśḵār](../../strongs/h/h3485.md) were, [Tôlāʿ](../../strongs/h/h8439.md), and [pû'â](../../strongs/h/h6312.md), [yāšûḇ](../../strongs/h/h3437.md), and [šimrôn](../../strongs/h/h8110.md), four.

<a name="1chronicles_7_2"></a>1Chronicles 7:2

And the [ben](../../strongs/h/h1121.md) of [Tôlāʿ](../../strongs/h/h8439.md); [ʿUzzî](../../strongs/h/h5813.md), and [Rᵊp̄Āyâ](../../strongs/h/h7509.md), and [Yᵊrî'Ēl](../../strongs/h/h3400.md), and [Yaḥmay](../../strongs/h/h3181.md), and [Yiḇśām](../../strongs/h/h3005.md), and [Šᵊmû'Ēl](../../strongs/h/h8050.md), [ro'sh](../../strongs/h/h7218.md) of their ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), to wit, of [Tôlāʿ](../../strongs/h/h8439.md): they were [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md) in their [towlĕdah](../../strongs/h/h8435.md); whose [mispār](../../strongs/h/h4557.md) was in the [yowm](../../strongs/h/h3117.md) of [Dāviḏ](../../strongs/h/h1732.md) two and twenty thousand and six hundred.

<a name="1chronicles_7_3"></a>1Chronicles 7:3

And the [ben](../../strongs/h/h1121.md) of [ʿUzzî](../../strongs/h/h5813.md); [Yizraḥyâ](../../strongs/h/h3156.md): and the [ben](../../strongs/h/h1121.md) of [Yizraḥyâ](../../strongs/h/h3156.md); [Mîḵā'ēl](../../strongs/h/h4317.md), and [ʿŌḇaḏyâ](../../strongs/h/h5662.md), and [Yô'Ēl](../../strongs/h/h3100.md), [Yiššîyâ](../../strongs/h/h3449.md), five: all of them [ro'sh](../../strongs/h/h7218.md).

<a name="1chronicles_7_4"></a>1Chronicles 7:4

And with them, by their [towlĕdah](../../strongs/h/h8435.md), after the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), were [gᵊḏûḏ](../../strongs/h/h1416.md) of [tsaba'](../../strongs/h/h6635.md) for [milḥāmâ](../../strongs/h/h4421.md), six and thirty thousand men: for they had [rabah](../../strongs/h/h7235.md) ['ishshah](../../strongs/h/h802.md) and [ben](../../strongs/h/h1121.md).

<a name="1chronicles_7_5"></a>1Chronicles 7:5

And their ['ach](../../strongs/h/h251.md) among all the [mišpāḥâ](../../strongs/h/h4940.md) of [Yiśśāśḵār](../../strongs/h/h3485.md) were [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), reckoned in all by their [yāḥaś](../../strongs/h/h3187.md) fourscore and seven thousand.

<a name="1chronicles_7_6"></a>1Chronicles 7:6

The sons of [Binyāmîn](../../strongs/h/h1144.md); [Belaʿ](../../strongs/h/h1106.md), and [Beḵer](../../strongs/h/h1071.md), and [YᵊḏîʿĂ'Ēl](../../strongs/h/h3043.md), three.

<a name="1chronicles_7_7"></a>1Chronicles 7:7

And the [ben](../../strongs/h/h1121.md) of [Belaʿ](../../strongs/h/h1106.md); ['Eṣbōn](../../strongs/h/h675.md), and [ʿUzzî](../../strongs/h/h5813.md), and [ʿUzzî'ēl](../../strongs/h/h5816.md), and [Yᵊrêmôṯ](../../strongs/h/h3406.md), and [ʿÎrî](../../strongs/h/h5901.md), five; [ro'sh](../../strongs/h/h7218.md) of the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md); and were reckoned by their [yāḥaś](../../strongs/h/h3187.md) twenty and two thousand and thirty and four.

<a name="1chronicles_7_8"></a>1Chronicles 7:8

And the [ben](../../strongs/h/h1121.md) of [Beḵer](../../strongs/h/h1071.md); [Zᵊmîrâ](../../strongs/h/h2160.md), and [YôʿĀš](../../strongs/h/h3135.md), and ['Ĕlîʿezer](../../strongs/h/h461.md), and ['ElyᵊhôʿÊnay](../../strongs/h/h454.md), and [ʿĀmrî](../../strongs/h/h6018.md), and [Yᵊrêmôṯ](../../strongs/h/h3406.md), and ['Ăḇîâ](../../strongs/h/h29.md), and [ʿĂnāṯôṯ](../../strongs/h/h6068.md), and [ʿĀlemeṯ](../../strongs/h/h5964.md). All these are the [ben](../../strongs/h/h1121.md) of [Beḵer](../../strongs/h/h1071.md).

<a name="1chronicles_7_9"></a>1Chronicles 7:9

And the [yāḥaś](../../strongs/h/h3187.md) of them, after their genealogy by their [towlĕdah](../../strongs/h/h8435.md), [ro'sh](../../strongs/h/h7218.md) of the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), was twenty thousand and two hundred.

<a name="1chronicles_7_10"></a>1Chronicles 7:10

The [ben](../../strongs/h/h1121.md) also of [YᵊḏîʿĂ'Ēl](../../strongs/h/h3043.md); [Bilhān](../../strongs/h/h1092.md): and the [ben](../../strongs/h/h1121.md) of [Bilhān](../../strongs/h/h1092.md); [yᵊʿîš](../../strongs/h/h3274.md), and [Binyāmîn](../../strongs/h/h1144.md), and ['Êûḏ](../../strongs/h/h164.md), and [KᵊnaʿĂnâ](../../strongs/h/h3668.md), and [Zêṯān](../../strongs/h/h2133.md), and [Taršîš](../../strongs/h/h8659.md), and ['Ăḥîšaḥar](../../strongs/h/h300.md).

<a name="1chronicles_7_11"></a>1Chronicles 7:11

All these the [ben](../../strongs/h/h1121.md) of [YᵊḏîʿĂ'Ēl](../../strongs/h/h3043.md), by the [ro'sh](../../strongs/h/h7218.md) of their ['ab](../../strongs/h/h1.md), [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), were seventeen thousand and two hundred soldiers, fit to [yāṣā'](../../strongs/h/h3318.md) for [tsaba'](../../strongs/h/h6635.md) and [milḥāmâ](../../strongs/h/h4421.md).

<a name="1chronicles_7_12"></a>1Chronicles 7:12

[Šupîm](../../strongs/h/h8206.md) also, and [Ḥupîm](../../strongs/h/h2650.md), the [ben](../../strongs/h/h1121.md) of [ʿÎr](../../strongs/h/h5893.md), and [Ḥûšîm](../../strongs/h/h2366.md), the [ben](../../strongs/h/h1121.md) of ['Aḥēr](../../strongs/h/h313.md).

<a name="1chronicles_7_13"></a>1Chronicles 7:13

The [ben](../../strongs/h/h1121.md) of [Nap̄tālî](../../strongs/h/h5321.md); [Yaḥăṣî'Ēl](../../strongs/h/h3185.md), and [Gûnî](../../strongs/h/h1476.md), and [Yēṣer](../../strongs/h/h3337.md), and [Šallûm](../../strongs/h/h7967.md), the [ben](../../strongs/h/h1121.md) of [Bilhâ](../../strongs/h/h1090.md).

<a name="1chronicles_7_14"></a>1Chronicles 7:14

The [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md); ['Aśrî'Ēl](../../strongs/h/h844.md), whom she [yalad](../../strongs/h/h3205.md): (but his [pîleḡeš](../../strongs/h/h6370.md) the ['Ărammy](../../strongs/h/h761.md) [yalad](../../strongs/h/h3205.md) [Māḵîr](../../strongs/h/h4353.md) the ['ab](../../strongs/h/h1.md) of [Gilʿāḏ](../../strongs/h/h1568.md):

<a name="1chronicles_7_15"></a>1Chronicles 7:15

And [Māḵîr](../../strongs/h/h4353.md) [laqach](../../strongs/h/h3947.md) to ['ishshah](../../strongs/h/h802.md) of [Ḥupîm](../../strongs/h/h2650.md) and [Šupîm](../../strongs/h/h8206.md), whose ['āḥôṯ](../../strongs/h/h269.md) [shem](../../strongs/h/h8034.md) was [Maʿăḵâ](../../strongs/h/h4601.md);) and the [shem](../../strongs/h/h8034.md) of the second was [Ṣᵊlāp̄Ḥāḏ](../../strongs/h/h6765.md): and [Ṣᵊlāp̄Ḥāḏ](../../strongs/h/h6765.md) had [bath](../../strongs/h/h1323.md).

<a name="1chronicles_7_16"></a>1Chronicles 7:16

And [Maʿăḵâ](../../strongs/h/h4601.md) the ['ishshah](../../strongs/h/h802.md) of [Māḵîr](../../strongs/h/h4353.md) [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md), and she [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Pereš](../../strongs/h/h6570.md); and the [shem](../../strongs/h/h8034.md) of his ['ach](../../strongs/h/h251.md) was [Šereš](../../strongs/h/h8329.md); and his [ben](../../strongs/h/h1121.md) were ['Ûlām](../../strongs/h/h198.md) and [Reqem](../../strongs/h/h7552.md).

<a name="1chronicles_7_17"></a>1Chronicles 7:17

And the [ben](../../strongs/h/h1121.md) of ['Ûlām](../../strongs/h/h198.md); [Bᵊḏān](../../strongs/h/h917.md). These were the [ben](../../strongs/h/h1121.md) of [Gilʿāḏ](../../strongs/h/h1568.md), the [ben](../../strongs/h/h1121.md) of [Māḵîr](../../strongs/h/h4353.md), the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md).

<a name="1chronicles_7_18"></a>1Chronicles 7:18

And his ['āḥôṯ](../../strongs/h/h269.md) [Mōleḵeṯ](../../strongs/h/h4447.md) [yalad](../../strongs/h/h3205.md) ['Îšhôḏ](../../strongs/h/h379.md), and ['ĂḇîʿEzer](../../strongs/h/h44.md), and [Maḥlâ](../../strongs/h/h4244.md).

<a name="1chronicles_7_19"></a>1Chronicles 7:19

And the [ben](../../strongs/h/h1121.md) of [Šᵊmîḏāʿ](../../strongs/h/h8061.md) were, ['Aḥyān](../../strongs/h/h291.md), and [Šeḵem](../../strongs/h/h7928.md), and [Liqḥî](../../strongs/h/h3949.md), and ['ĂnîʿĀm](../../strongs/h/h593.md).

<a name="1chronicles_7_20"></a>1Chronicles 7:20

And the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md); [Šûṯelaḥ](../../strongs/h/h7803.md), and [bereḏ](../../strongs/h/h1260.md) his [ben](../../strongs/h/h1121.md), and [Taḥaṯ](../../strongs/h/h8480.md) his [ben](../../strongs/h/h1121.md), and ['ElʿĀḏâ](../../strongs/h/h497.md) his [ben](../../strongs/h/h1121.md), and [Taḥaṯ](../../strongs/h/h8480.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_7_21"></a>1Chronicles 7:21

And [Zāḇāḏ](../../strongs/h/h2066.md) his [ben](../../strongs/h/h1121.md), and [Šûṯelaḥ](../../strongs/h/h7803.md) his [ben](../../strongs/h/h1121.md), and [ʿEzer](../../strongs/h/h5827.md), and ['ElʿĀḏ](../../strongs/h/h496.md), whom the ['enowsh](../../strongs/h/h582.md) of [Gaṯ](../../strongs/h/h1661.md) that were [yalad](../../strongs/h/h3205.md) in that ['erets](../../strongs/h/h776.md) [harag](../../strongs/h/h2026.md), because they [yarad](../../strongs/h/h3381.md) to [laqach](../../strongs/h/h3947.md) their [miqnê](../../strongs/h/h4735.md).

<a name="1chronicles_7_22"></a>1Chronicles 7:22

And ['Ep̄rayim](../../strongs/h/h669.md) their ['ab](../../strongs/h/h1.md) ['āḇal](../../strongs/h/h56.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md), and his ['ach](../../strongs/h/h251.md) [bow'](../../strongs/h/h935.md) to [nacham](../../strongs/h/h5162.md) him.

<a name="1chronicles_7_23"></a>1Chronicles 7:23

And when he [bow'](../../strongs/h/h935.md) to his ['ishshah](../../strongs/h/h802.md), she [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md), and he [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Bᵊrîʿâ](../../strongs/h/h1283.md), because it went [ra'](../../strongs/h/h7451.md) with his [bayith](../../strongs/h/h1004.md).

<a name="1chronicles_7_24"></a>1Chronicles 7:24

(And his [bath](../../strongs/h/h1323.md) was [Še'Ĕrâ](../../strongs/h/h7609.md), who [bānâ](../../strongs/h/h1129.md) [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md) the [taḥtôn](../../strongs/h/h8481.md), and the ['elyown](../../strongs/h/h5945.md), and ['Uzzen Še'Ĕrâ](../../strongs/h/h242.md).)

<a name="1chronicles_7_25"></a>1Chronicles 7:25

And [Rep̄Aḥ](../../strongs/h/h7506.md) was his [ben](../../strongs/h/h1121.md), also [Rešep̄](../../strongs/h/h7566.md), and [Telaḥ](../../strongs/h/h8520.md) his [ben](../../strongs/h/h1121.md), and [Taḥan](../../strongs/h/h8465.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_7_26"></a>1Chronicles 7:26

[LaʿDān](../../strongs/h/h3936.md) his [ben](../../strongs/h/h1121.md), [ʿAmmîhûḏ](../../strongs/h/h5989.md) his [ben](../../strongs/h/h1121.md), ['Ĕlîšāmāʿ](../../strongs/h/h476.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_7_27"></a>1Chronicles 7:27

[Nûn](../../strongs/h/h5126.md) his [ben](../../strongs/h/h1121.md), [Yᵊhôšûaʿ](../../strongs/h/h3091.md) his [ben](../../strongs/h/h1121.md).

<a name="1chronicles_7_28"></a>1Chronicles 7:28

And their ['achuzzah](../../strongs/h/h272.md) and [môšāḇ](../../strongs/h/h4186.md) were, [Bêṯ-'ēl](../../strongs/h/h1008.md) and the [bath](../../strongs/h/h1323.md) thereof, and [mizrach](../../strongs/h/h4217.md) [NaʿĂrān](../../strongs/h/h5295.md), and [ma'arab](../../strongs/h/h4628.md) [Gezer](../../strongs/h/h1507.md), with the [bath](../../strongs/h/h1323.md) thereof; [Šᵊḵem](../../strongs/h/h7927.md) also and the [bath](../../strongs/h/h1323.md) thereof, unto [ʿAzzâ](../../strongs/h/h5804.md) and the [bath](../../strongs/h/h1323.md) thereof:

<a name="1chronicles_7_29"></a>1Chronicles 7:29

And by the [yad](../../strongs/h/h3027.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md), [Bêṯ Šᵊ'Ān](../../strongs/h/h1052.md) and her [bath](../../strongs/h/h1323.md), [TaʿNāḵ](../../strongs/h/h8590.md) and her [bath](../../strongs/h/h1323.md), [Mᵊḡidôn](../../strongs/h/h4023.md) and her [bath](../../strongs/h/h1323.md), [Dôr](../../strongs/h/h1756.md) and her [bath](../../strongs/h/h1323.md). In these [yashab](../../strongs/h/h3427.md) the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_7_30"></a>1Chronicles 7:30

The [ben](../../strongs/h/h1121.md) of ['Āšēr](../../strongs/h/h836.md); [Yimnâ](../../strongs/h/h3232.md), and [Yišvî](../../strongs/h/h3440.md), and [Yišvâ](../../strongs/h/h3438.md), and [Bᵊrîʿâ](../../strongs/h/h1283.md), and [Śeraḥ](../../strongs/h/h8294.md) their ['āḥôṯ](../../strongs/h/h269.md).

<a name="1chronicles_7_31"></a>1Chronicles 7:31

And the [ben](../../strongs/h/h1121.md) of [Bᵊrîʿâ](../../strongs/h/h1283.md); [Ḥeḇer](../../strongs/h/h2268.md), and [Malkî'ēl](../../strongs/h/h4439.md), who is the ['ab](../../strongs/h/h1.md) of [Birzôṯ](../../strongs/h/h1269.md).

<a name="1chronicles_7_32"></a>1Chronicles 7:32

And [Ḥeḇer](../../strongs/h/h2268.md) [yalad](../../strongs/h/h3205.md) [Yap̄Lēṭ](../../strongs/h/h3310.md), and [Šōmēr](../../strongs/h/h7763.md), and [Ḥôṯām](../../strongs/h/h2369.md), and [ŠûʿĀ'](../../strongs/h/h7774.md) their ['āḥôṯ](../../strongs/h/h269.md).

<a name="1chronicles_7_33"></a>1Chronicles 7:33

And the [ben](../../strongs/h/h1121.md) of [Yap̄Lēṭ](../../strongs/h/h3310.md); [Pāsaḵ](../../strongs/h/h6457.md), and [Bimhāl](../../strongs/h/h1118.md), and [ʿAšvāṯ](../../strongs/h/h6220.md). These are the [ben](../../strongs/h/h1121.md) of [Yap̄Lēṭ](../../strongs/h/h3310.md).

<a name="1chronicles_7_34"></a>1Chronicles 7:34

And the [ben](../../strongs/h/h1121.md) of [Šemer](../../strongs/h/h8106.md); ['Ăḥî](../../strongs/h/h277.md), and [Rvhḡh](../../strongs/h/h7303.md), [Yᵊḥubâ](../../strongs/h/h3160.md), and ['Ărām](../../strongs/h/h758.md).

<a name="1chronicles_7_35"></a>1Chronicles 7:35

And the [ben](../../strongs/h/h1121.md) of his ['ach](../../strongs/h/h251.md) [Hēlem](../../strongs/h/h1987.md); [Ṣôp̄Aḥ](../../strongs/h/h6690.md), and [Yimnāʿ](../../strongs/h/h3234.md), and [Šeleš](../../strongs/h/h8028.md), and [ʿĀmāl](../../strongs/h/h6000.md).

<a name="1chronicles_7_36"></a>1Chronicles 7:36

The [ben](../../strongs/h/h1121.md) of [Ṣôp̄Aḥ](../../strongs/h/h6690.md); [Sûaḥ](../../strongs/h/h5477.md), and [Ḥarnep̄Er](../../strongs/h/h2774.md), and [ŠûʿĀl](../../strongs/h/h7777.md), and [Bērî](../../strongs/h/h1275.md), and [Yimrâ](../../strongs/h/h3236.md),

<a name="1chronicles_7_37"></a>1Chronicles 7:37

[Beṣer](../../strongs/h/h1221.md), and [Hôḏ](../../strongs/h/h1936.md), and [Šammā'](../../strongs/h/h8037.md), and [Šilšâ](../../strongs/h/h8030.md), and [yiṯrān](../../strongs/h/h3506.md), and [Bᵊ'Ērā'](../../strongs/h/h878.md).

<a name="1chronicles_7_38"></a>1Chronicles 7:38

And the [ben](../../strongs/h/h1121.md) of [Yeṯer](../../strongs/h/h3500.md); [Yᵊp̄unnê](../../strongs/h/h3312.md), and [Pispâ](../../strongs/h/h6462.md), and ['Ărā'](../../strongs/h/h690.md).

<a name="1chronicles_7_39"></a>1Chronicles 7:39

And the [ben](../../strongs/h/h1121.md) of [ʿUllā'](../../strongs/h/h5925.md); ['Āraḥ](../../strongs/h/h733.md), and [Ḥannî'Ēl](../../strongs/h/h2592.md), and [Riṣyā'](../../strongs/h/h7525.md).

<a name="1chronicles_7_40"></a>1Chronicles 7:40

All these were the [ben](../../strongs/h/h1121.md) of ['Āšēr](../../strongs/h/h836.md), [ro'sh](../../strongs/h/h7218.md) of their ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), [bārar](../../strongs/h/h1305.md) and [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), [ro'sh](../../strongs/h/h7218.md) of the [nāśî'](../../strongs/h/h5387.md). And the [mispār](../../strongs/h/h4557.md) throughout the [yāḥaś](../../strongs/h/h3187.md) of them that were apt to the [tsaba'](../../strongs/h/h6635.md) and to [milḥāmâ](../../strongs/h/h4421.md) was twenty and six thousand ['enowsh](../../strongs/h/h582.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 6](1chronicles_6.md) - [1Chronicles 8](1chronicles_8.md)