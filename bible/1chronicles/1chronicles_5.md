# [1Chronicles 5](https://www.blueletterbible.org/kjv/1chronicles/5)

<a name="1chronicles_5_1"></a>1Chronicles 5:1

Now the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) the [bᵊḵôr](../../strongs/h/h1060.md) of [Yisra'el](../../strongs/h/h3478.md), (for he was the [bᵊḵôr](../../strongs/h/h1060.md); but, forasmuch as he [ḥālal](../../strongs/h/h2490.md) his ['ab](../../strongs/h/h1.md) [yāṣûaʿ](../../strongs/h/h3326.md), his [bᵊḵôrâ](../../strongs/h/h1062.md) was [nathan](../../strongs/h/h5414.md) unto the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): and not [yāḥaś](../../strongs/h/h3187.md) after the [bᵊḵôrâ](../../strongs/h/h1062.md).

<a name="1chronicles_5_2"></a>1Chronicles 5:2

For [Yehuwdah](../../strongs/h/h3063.md) [gabar](../../strongs/h/h1396.md) above his ['ach](../../strongs/h/h251.md), and of him came the [nāḡîḏ](../../strongs/h/h5057.md); but the [bᵊḵôrâ](../../strongs/h/h1062.md) was [Yôsēp̄](../../strongs/h/h3130.md):)

<a name="1chronicles_5_3"></a>1Chronicles 5:3

The [ben](../../strongs/h/h1121.md), I say, of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) the [bᵊḵôr](../../strongs/h/h1060.md) of [Yisra'el](../../strongs/h/h3478.md) were, [Ḥănôḵ](../../strongs/h/h2585.md), and [Pallû'](../../strongs/h/h6396.md), [Ḥeṣrôn](../../strongs/h/h2696.md), and [Karmî](../../strongs/h/h3756.md).

<a name="1chronicles_5_4"></a>1Chronicles 5:4

The [ben](../../strongs/h/h1121.md) of [Yô'Ēl](../../strongs/h/h3100.md); [ŠᵊmaʿYâ](../../strongs/h/h8098.md) his [ben](../../strongs/h/h1121.md), [Gôḡ](../../strongs/h/h1463.md) his [ben](../../strongs/h/h1121.md), [Šimʿî](../../strongs/h/h8096.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_5_5"></a>1Chronicles 5:5

[Mîḵâ](../../strongs/h/h4318.md) his [ben](../../strongs/h/h1121.md), [Rᵊ'Āyâ](../../strongs/h/h7211.md) his [ben](../../strongs/h/h1121.md), [BaʿAl](../../strongs/h/h1168.md) his [ben](../../strongs/h/h1121.md),

<a name="1chronicles_5_6"></a>1Chronicles 5:6

[Bᵊ'Ērâ](../../strongs/h/h880.md) his [ben](../../strongs/h/h1121.md), whom [Tiḡlaṯ Pil'Eser](../../strongs/h/h8407.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [gālâ](../../strongs/h/h1540.md): he was [nāśî'](../../strongs/h/h5387.md) of the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md).

<a name="1chronicles_5_7"></a>1Chronicles 5:7

And his ['ach](../../strongs/h/h251.md) by their [mišpāḥâ](../../strongs/h/h4940.md), when the [yāḥaś](../../strongs/h/h3187.md) of their [towlĕdah](../../strongs/h/h8435.md) was [yāḥaś](../../strongs/h/h3187.md), were the [ro'sh](../../strongs/h/h7218.md), [YᵊʿÎ'Ēl](../../strongs/h/h3273.md), and [Zᵊḵaryâ](../../strongs/h/h2148.md),

<a name="1chronicles_5_8"></a>1Chronicles 5:8

And [Belaʿ](../../strongs/h/h1106.md) the [ben](../../strongs/h/h1121.md) of [ʿĀzāz](../../strongs/h/h5811.md), the [ben](../../strongs/h/h1121.md) of [Šemaʿ](../../strongs/h/h8087.md), the [ben](../../strongs/h/h1121.md) of [Yô'Ēl](../../strongs/h/h3100.md), who [yashab](../../strongs/h/h3427.md) in [ʿĂrôʿēr](../../strongs/h/h6177.md), even unto [Nᵊḇô](../../strongs/h/h5015.md) and [BaʿAl MᵊʿÔn](../../strongs/h/h1186.md):

<a name="1chronicles_5_9"></a>1Chronicles 5:9

And [mizrach](../../strongs/h/h4217.md) he [yashab](../../strongs/h/h3427.md) unto the [bow'](../../strongs/h/h935.md) in of the [midbar](../../strongs/h/h4057.md) from the [nāhār](../../strongs/h/h5104.md) [Pᵊrāṯ](../../strongs/h/h6578.md): because their [miqnê](../../strongs/h/h4735.md) were [rabah](../../strongs/h/h7235.md) in the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="1chronicles_5_10"></a>1Chronicles 5:10

And in the [yowm](../../strongs/h/h3117.md) of [Šā'ûl](../../strongs/h/h7586.md) they ['asah](../../strongs/h/h6213.md) [milḥāmâ](../../strongs/h/h4421.md) with the [Haḡrî](../../strongs/h/h1905.md), who [naphal](../../strongs/h/h5307.md) by their [yad](../../strongs/h/h3027.md): and they [yashab](../../strongs/h/h3427.md) in their ['ohel](../../strongs/h/h168.md) [paniym](../../strongs/h/h6440.md) all the [mizrach](../../strongs/h/h4217.md) land of [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="1chronicles_5_11"></a>1Chronicles 5:11

And the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) [yashab](../../strongs/h/h3427.md) over against them, in the ['erets](../../strongs/h/h776.md) of [Bāšān](../../strongs/h/h1316.md) unto [Salḵâ](../../strongs/h/h5548.md):

<a name="1chronicles_5_12"></a>1Chronicles 5:12

[Yô'Ēl](../../strongs/h/h3100.md) the [ro'sh](../../strongs/h/h7218.md), and [Šāp̄Ām](../../strongs/h/h8223.md) the [mišnê](../../strongs/h/h4932.md), and [YaʿĂnay](../../strongs/h/h3285.md), and [Šāp̄āṭ](../../strongs/h/h8202.md) in [Bāšān](../../strongs/h/h1316.md).

<a name="1chronicles_5_13"></a>1Chronicles 5:13

And their ['ach](../../strongs/h/h251.md) of the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md) were, [Mîḵā'ēl](../../strongs/h/h4317.md), and [Mᵊšullām](../../strongs/h/h4918.md), and [Šeḇaʿ](../../strongs/h/h7652.md), and [Yôray](../../strongs/h/h3140.md), and [YaʿKān](../../strongs/h/h3275.md), and [Zîaʿ](../../strongs/h/h2127.md), and [ʿēḇer](../../strongs/h/h5677.md), seven.

<a name="1chronicles_5_14"></a>1Chronicles 5:14

These are the [ben](../../strongs/h/h1121.md) of ['Ăḇîhayil](../../strongs/h/h32.md) the [ben](../../strongs/h/h1121.md) of [Ḥûrî](../../strongs/h/h2359.md), the [ben](../../strongs/h/h1121.md) of [Yārôaḥ](../../strongs/h/h3386.md), the [ben](../../strongs/h/h1121.md) of [Gilʿāḏ](../../strongs/h/h1568.md), the [ben](../../strongs/h/h1121.md) of [Mîḵā'ēl](../../strongs/h/h4317.md), the [ben](../../strongs/h/h1121.md) of [Yᵊšîšay](../../strongs/h/h3454.md), the [ben](../../strongs/h/h1121.md) of [Yaḥdô](../../strongs/h/h3163.md), the [ben](../../strongs/h/h1121.md) of [Bûz](../../strongs/h/h938.md);

<a name="1chronicles_5_15"></a>1Chronicles 5:15

['Ăḥî](../../strongs/h/h277.md) the [ben](../../strongs/h/h1121.md) of [ʿAḇdî'Ēl](../../strongs/h/h5661.md), the [ben](../../strongs/h/h1121.md) of [Gûnî](../../strongs/h/h1476.md), [ro'sh](../../strongs/h/h7218.md) of the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md).

<a name="1chronicles_5_16"></a>1Chronicles 5:16

And they [yashab](../../strongs/h/h3427.md) in [Gilʿāḏ](../../strongs/h/h1568.md) in [Bāšān](../../strongs/h/h1316.md), and in her [bath](../../strongs/h/h1323.md), and in all the [miḡrāš](../../strongs/h/h4054.md) of [Šārôn](../../strongs/h/h8289.md), upon their [tôṣā'ôṯ](../../strongs/h/h8444.md).

<a name="1chronicles_5_17"></a>1Chronicles 5:17

All these were [yāḥaś](../../strongs/h/h3187.md) in the [yowm](../../strongs/h/h3117.md) of [Yôṯām](../../strongs/h/h3147.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and in the [yowm](../../strongs/h/h3117.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1chronicles_5_18"></a>1Chronicles 5:18

The [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), and the [Gāḏî](../../strongs/h/h1425.md), and [ḥēṣî](../../strongs/h/h2677.md) the [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md), of [ḥayil](../../strongs/h/h2428.md), ['enowsh](../../strongs/h/h582.md) able to [nasa'](../../strongs/h/h5375.md) [magen](../../strongs/h/h4043.md) and [chereb](../../strongs/h/h2719.md), and to [dāraḵ](../../strongs/h/h1869.md) with [qesheth](../../strongs/h/h7198.md), and [lamad](../../strongs/h/h3925.md) in [milḥāmâ](../../strongs/h/h4421.md), were four and forty thousand seven hundred and threescore, that [yāṣā'](../../strongs/h/h3318.md) to the [tsaba'](../../strongs/h/h6635.md).

<a name="1chronicles_5_19"></a>1Chronicles 5:19

And they ['asah](../../strongs/h/h6213.md) [milḥāmâ](../../strongs/h/h4421.md) with the [Haḡrî](../../strongs/h/h1905.md), with [Yᵊṭûr](../../strongs/h/h3195.md), and [Nāp̄îš](../../strongs/h/h5305.md), and [Nôḏāḇ](../../strongs/h/h5114.md).

<a name="1chronicles_5_20"></a>1Chronicles 5:20

And they were [ʿāzar](../../strongs/h/h5826.md) against them, and the [Haḡrî](../../strongs/h/h1905.md) were [nathan](../../strongs/h/h5414.md) into their [yad](../../strongs/h/h3027.md), and all that were with them: for they [zāʿaq](../../strongs/h/h2199.md) to ['Elohiym](../../strongs/h/h430.md) in the [milḥāmâ](../../strongs/h/h4421.md), and he was [ʿāṯar](../../strongs/h/h6279.md) of them; because they put their [batach](../../strongs/h/h982.md) in him.

<a name="1chronicles_5_21"></a>1Chronicles 5:21

And they [šāḇâ](../../strongs/h/h7617.md) their [miqnê](../../strongs/h/h4735.md); of their [gāmāl](../../strongs/h/h1581.md) fifty thousand, and of [tso'n](../../strongs/h/h6629.md) two hundred and fifty thousand, and of [chamowr](../../strongs/h/h2543.md) two thousand, and of ['āḏām](../../strongs/h/h120.md) [nephesh](../../strongs/h/h5315.md) an hundred thousand.

<a name="1chronicles_5_22"></a>1Chronicles 5:22

For there [naphal](../../strongs/h/h5307.md) down [rab](../../strongs/h/h7227.md) [ḥālāl](../../strongs/h/h2491.md), because the [milḥāmâ](../../strongs/h/h4421.md) was of ['Elohiym](../../strongs/h/h430.md). And they [yashab](../../strongs/h/h3427.md) in their steads until the [gôlâ](../../strongs/h/h1473.md).

<a name="1chronicles_5_23"></a>1Chronicles 5:23

And the [ben](../../strongs/h/h1121.md) of the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md) [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md): they [rabah](../../strongs/h/h7235.md) from [Bāšān](../../strongs/h/h1316.md) unto [BaʿAl Ḥermôn](../../strongs/h/h1179.md) and [Śᵊnîr](../../strongs/h/h8149.md), and unto [har](../../strongs/h/h2022.md) [Ḥermôn](../../strongs/h/h2768.md).

<a name="1chronicles_5_24"></a>1Chronicles 5:24

And these were the [ro'sh](../../strongs/h/h7218.md) of the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), even [ʿēp̄er](../../strongs/h/h6081.md), and [YišʿÎ](../../strongs/h/h3469.md), and ['Ĕlî'Ēl](../../strongs/h/h447.md), and [ʿAzrî'Ēl](../../strongs/h/h5837.md), and [Yirmᵊyâ](../../strongs/h/h3414.md), and [Hôḏavyâ](../../strongs/h/h1938.md), and [Yiḥdî'Ēl](../../strongs/h/h3164.md), [gibôr](../../strongs/h/h1368.md) ['enowsh](../../strongs/h/h582.md) of [ḥayil](../../strongs/h/h2428.md), [shem](../../strongs/h/h8034.md) ['enowsh](../../strongs/h/h582.md), and [ro'sh](../../strongs/h/h7218.md) of the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md).

<a name="1chronicles_5_25"></a>1Chronicles 5:25

And they [māʿal](../../strongs/h/h4603.md) against the ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md), and [zānâ](../../strongs/h/h2181.md) ['aḥar](../../strongs/h/h310.md) the ['Elohiym](../../strongs/h/h430.md) of the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), whom ['Elohiym](../../strongs/h/h430.md) [šāmaḏ](../../strongs/h/h8045.md) [paniym](../../strongs/h/h6440.md) them.

<a name="1chronicles_5_26"></a>1Chronicles 5:26

And the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿûr](../../strongs/h/h5782.md) the [ruwach](../../strongs/h/h7307.md) of [Pûl](../../strongs/h/h6322.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), and the [ruwach](../../strongs/h/h7307.md) of [Tiḡlaṯ Pil'Eser](../../strongs/h/h8407.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), and he [gālâ](../../strongs/h/h1540.md) them, even the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md), and the [Gāḏî](../../strongs/h/h1425.md), and the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md), and [bow'](../../strongs/h/h935.md) them unto [Ḥălaḥ](../../strongs/h/h2477.md), and [Ḥāḇôr](../../strongs/h/h2249.md), and [Hārā'](../../strongs/h/h2024.md), and to the [nāhār](../../strongs/h/h5104.md) [Gôzān](../../strongs/h/h1470.md), unto this [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 4](1chronicles_4.md) - [1Chronicles 6](1chronicles_6.md)