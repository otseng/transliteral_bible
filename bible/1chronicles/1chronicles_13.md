# [1Chronicles 13](https://www.blueletterbible.org/kjv/1chronicles/13)

<a name="1chronicles_13_1"></a>1Chronicles 13:1

And [Dāviḏ](../../strongs/h/h1732.md) [ya'ats](../../strongs/h/h3289.md) with the [śar](../../strongs/h/h8269.md) of thousands and hundreds, and with every [nāḡîḏ](../../strongs/h/h5057.md).

<a name="1chronicles_13_2"></a>1Chronicles 13:2

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto all the [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md), If it seem [ṭôḇ](../../strongs/h/h2895.md) unto you, and that it be of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), let us [shalach](../../strongs/h/h7971.md) [pāraṣ](../../strongs/h/h6555.md) unto our ['ach](../../strongs/h/h251.md) every where, that are [šā'ar](../../strongs/h/h7604.md) in all the ['erets](../../strongs/h/h776.md) of [Yisra'el](../../strongs/h/h3478.md), and with them also to the [kōhēn](../../strongs/h/h3548.md) and [Lᵊvî](../../strongs/h/h3881.md) which are in their [ʿîr](../../strongs/h/h5892.md) and [miḡrāš](../../strongs/h/h4054.md), that they may [qāḇaṣ](../../strongs/h/h6908.md) themselves unto us:

<a name="1chronicles_13_3"></a>1Chronicles 13:3

And let us [cabab](../../strongs/h/h5437.md) the ['ārôn](../../strongs/h/h727.md) of our ['Elohiym](../../strongs/h/h430.md) to us: for we [darash](../../strongs/h/h1875.md) not at it in the [yowm](../../strongs/h/h3117.md) of [Šā'ûl](../../strongs/h/h7586.md).

<a name="1chronicles_13_4"></a>1Chronicles 13:4

And all the [qāhēl](../../strongs/h/h6951.md) ['āmar](../../strongs/h/h559.md) that they would ['asah](../../strongs/h/h6213.md): for the [dabar](../../strongs/h/h1697.md) was [yashar](../../strongs/h/h3474.md) in the ['ayin](../../strongs/h/h5869.md) of all the ['am](../../strongs/h/h5971.md).

<a name="1chronicles_13_5"></a>1Chronicles 13:5

So [Dāviḏ](../../strongs/h/h1732.md) [qāhal](../../strongs/h/h6950.md) all [Yisra'el](../../strongs/h/h3478.md) [qāhal](../../strongs/h/h6950.md), from [šîḥôr](../../strongs/h/h7883.md) of [Mitsrayim](../../strongs/h/h4714.md) even unto the [bow'](../../strongs/h/h935.md) of [Ḥămāṯ](../../strongs/h/h2574.md), to [bow'](../../strongs/h/h935.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) from [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md).

<a name="1chronicles_13_6"></a>1Chronicles 13:6

And [Dāviḏ](../../strongs/h/h1732.md) [ʿālâ](../../strongs/h/h5927.md), and all [Yisra'el](../../strongs/h/h3478.md), to [BaʿĂlâ](../../strongs/h/h1173.md), that is, to [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md), which belonged to [Yehuwdah](../../strongs/h/h3063.md), to [ʿālâ](../../strongs/h/h5927.md) thence the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) [Yĕhovah](../../strongs/h/h3068.md), that [yashab](../../strongs/h/h3427.md) between the [kĕruwb](../../strongs/h/h3742.md), whose [shem](../../strongs/h/h8034.md) is [qara'](../../strongs/h/h7121.md) on it.

<a name="1chronicles_13_7"></a>1Chronicles 13:7

And they [rāḵaḇ](../../strongs/h/h7392.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) in a [ḥāḏāš](../../strongs/h/h2319.md) [ʿăḡālâ](../../strongs/h/h5699.md) out of the [bayith](../../strongs/h/h1004.md) of ['Ăḇînāḏāḇ](../../strongs/h/h41.md): and [ʿUzzā'](../../strongs/h/h5798.md) and ['Aḥyô](../../strongs/h/h283.md) [nāhaḡ](../../strongs/h/h5090.md) the [ʿăḡālâ](../../strongs/h/h5699.md).

<a name="1chronicles_13_8"></a>1Chronicles 13:8

And [Dāviḏ](../../strongs/h/h1732.md) and all [Yisra'el](../../strongs/h/h3478.md) [śāḥaq](../../strongs/h/h7832.md) [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md) with all their ['oz](../../strongs/h/h5797.md), and with [šîr](../../strongs/h/h7892.md), and with [kinnôr](../../strongs/h/h3658.md), and with [neḇel](../../strongs/h/h5035.md), and with [tōp̄](../../strongs/h/h8596.md), and with [mᵊṣēleṯ](../../strongs/h/h4700.md), and with [ḥăṣōṣrâ](../../strongs/h/h2689.md).

<a name="1chronicles_13_9"></a>1Chronicles 13:9

And when they [bow'](../../strongs/h/h935.md) unto the [gōren](../../strongs/h/h1637.md) of [Kîḏôn](../../strongs/h/h3592.md), [ʿUzzā'](../../strongs/h/h5798.md) [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) to ['āḥaz](../../strongs/h/h270.md) the ['ārôn](../../strongs/h/h727.md); for the [bāqār](../../strongs/h/h1241.md) [šāmaṭ](../../strongs/h/h8058.md).

<a name="1chronicles_13_10"></a>1Chronicles 13:10

And the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against [ʿUzzā'](../../strongs/h/h5798.md), and he [nakah](../../strongs/h/h5221.md) him, because he [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) to the ['ārôn](../../strongs/h/h727.md): and there he [muwth](../../strongs/h/h4191.md) [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md).

<a name="1chronicles_13_11"></a>1Chronicles 13:11

And [Dāviḏ](../../strongs/h/h1732.md) was [ḥārâ](../../strongs/h/h2734.md), because [Yĕhovah](../../strongs/h/h3068.md) had [pāraṣ](../../strongs/h/h6555.md) a [pereṣ](../../strongs/h/h6556.md) upon [ʿUzzā'](../../strongs/h/h5798.md): wherefore that [maqowm](../../strongs/h/h4725.md) is [qara'](../../strongs/h/h7121.md) [Pereṣ](../../strongs/h/h6560.md) to this [yowm](../../strongs/h/h3117.md).

<a name="1chronicles_13_12"></a>1Chronicles 13:12

And [Dāviḏ](../../strongs/h/h1732.md) was [yare'](../../strongs/h/h3372.md) of ['Elohiym](../../strongs/h/h430.md) that [yowm](../../strongs/h/h3117.md), ['āmar](../../strongs/h/h559.md), [hêḵ](../../strongs/h/h1963.md) shall I [bow'](../../strongs/h/h935.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) home to me?

<a name="1chronicles_13_13"></a>1Chronicles 13:13

So [Dāviḏ](../../strongs/h/h1732.md) [cuwr](../../strongs/h/h5493.md) not the ['ārôn](../../strongs/h/h727.md) home to himself to the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), but [natah](../../strongs/h/h5186.md) it into the [bayith](../../strongs/h/h1004.md) of [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md) the [Gitî](../../strongs/h/h1663.md).

<a name="1chronicles_13_14"></a>1Chronicles 13:14

And the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) [yashab](../../strongs/h/h3427.md) with the [bayith](../../strongs/h/h1004.md) of [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md) in his [bayith](../../strongs/h/h1004.md) three [ḥōḏeš](../../strongs/h/h2320.md). And [Yĕhovah](../../strongs/h/h3068.md) [barak](../../strongs/h/h1288.md) the [bayith](../../strongs/h/h1004.md) of [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md), and all that he had.

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 12](1chronicles_12.md) - [1Chronicles 14](1chronicles_14.md)