# [1Chronicles 18](https://www.blueletterbible.org/kjv/1chronicles/18)

<a name="1chronicles_18_1"></a>1Chronicles 18:1

Now ['aḥar](../../strongs/h/h310.md) it came to pass, that [Dāviḏ](../../strongs/h/h1732.md) [nakah](../../strongs/h/h5221.md) the [Pᵊlištî](../../strongs/h/h6430.md), and [kānaʿ](../../strongs/h/h3665.md) them, and [laqach](../../strongs/h/h3947.md) [Gaṯ](../../strongs/h/h1661.md) and her [bath](../../strongs/h/h1323.md) out of the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1chronicles_18_2"></a>1Chronicles 18:2

And he [nakah](../../strongs/h/h5221.md) [Mô'āḇ](../../strongs/h/h4124.md); and the [Mô'āḇ](../../strongs/h/h4124.md) became [Dāviḏ](../../strongs/h/h1732.md) ['ebed](../../strongs/h/h5650.md), and [nasa'](../../strongs/h/h5375.md) [minchah](../../strongs/h/h4503.md).

<a name="1chronicles_18_3"></a>1Chronicles 18:3

And [Dāviḏ](../../strongs/h/h1732.md) [nakah](../../strongs/h/h5221.md) [HăḏarʿEzer](../../strongs/h/h1928.md) [melek](../../strongs/h/h4428.md) of [Ṣôḇā'](../../strongs/h/h6678.md) unto [Ḥămāṯ](../../strongs/h/h2574.md), as he [yālaḵ](../../strongs/h/h3212.md) to [nāṣaḇ](../../strongs/h/h5324.md) his [yad](../../strongs/h/h3027.md) by the [nāhār](../../strongs/h/h5104.md) [Pᵊrāṯ](../../strongs/h/h6578.md).

<a name="1chronicles_18_4"></a>1Chronicles 18:4

And [Dāviḏ](../../strongs/h/h1732.md) [lāḵaḏ](../../strongs/h/h3920.md) from him a thousand [reḵeḇ](../../strongs/h/h7393.md), and seven thousand [pārāš](../../strongs/h/h6571.md), and twenty thousand ['iysh](../../strongs/h/h376.md) [raḡlî](../../strongs/h/h7273.md): [Dāviḏ](../../strongs/h/h1732.md) also [ʿāqar](../../strongs/h/h6131.md) all the [reḵeḇ](../../strongs/h/h7393.md), but [yāṯar](../../strongs/h/h3498.md) of them an hundred [reḵeḇ](../../strongs/h/h7393.md).

<a name="1chronicles_18_5"></a>1Chronicles 18:5

And when the ['Ărām](../../strongs/h/h758.md) of [Dammeśeq](../../strongs/h/h1834.md) [bow'](../../strongs/h/h935.md) to [ʿāzar](../../strongs/h/h5826.md) [HăḏarʿEzer](../../strongs/h/h1928.md) [melek](../../strongs/h/h4428.md) of [Ṣôḇā'](../../strongs/h/h6678.md), [Dāviḏ](../../strongs/h/h1732.md) [nakah](../../strongs/h/h5221.md) of the ['Ărām](../../strongs/h/h758.md) two and twenty thousand ['iysh](../../strongs/h/h376.md).

<a name="1chronicles_18_6"></a>1Chronicles 18:6

Then [Dāviḏ](../../strongs/h/h1732.md) [śûm](../../strongs/h/h7760.md) in ['Ărām](../../strongs/h/h758.md) [Dammeśeq](../../strongs/h/h1834.md); and the ['Ărām](../../strongs/h/h758.md) became [Dāviḏ](../../strongs/h/h1732.md) ['ebed](../../strongs/h/h5650.md), and [nasa'](../../strongs/h/h5375.md) [minchah](../../strongs/h/h4503.md). Thus [Yĕhovah](../../strongs/h/h3068.md) [yasha'](../../strongs/h/h3467.md) [Dāviḏ](../../strongs/h/h1732.md) whithersoever he [halak](../../strongs/h/h1980.md).

<a name="1chronicles_18_7"></a>1Chronicles 18:7

And [Dāviḏ](../../strongs/h/h1732.md) [laqach](../../strongs/h/h3947.md) the [šeleṭ](../../strongs/h/h7982.md) of [zāhāḇ](../../strongs/h/h2091.md) that were on the ['ebed](../../strongs/h/h5650.md) of [HăḏarʿEzer](../../strongs/h/h1928.md), and [bow'](../../strongs/h/h935.md) them to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="1chronicles_18_8"></a>1Chronicles 18:8

Likewise from [Ṭiḇḥaṯ](../../strongs/h/h2880.md), and from [Kûn](../../strongs/h/h3560.md), [ʿîr](../../strongs/h/h5892.md) of [HăḏarʿEzer](../../strongs/h/h1928.md), [laqach](../../strongs/h/h3947.md) [Dāviḏ](../../strongs/h/h1732.md) [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md) [nᵊḥšeṯ](../../strongs/h/h5178.md), wherewith [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) the [nᵊḥšeṯ](../../strongs/h/h5178.md) [yam](../../strongs/h/h3220.md), and the [ʿammûḏ](../../strongs/h/h5982.md), and the [kĕliy](../../strongs/h/h3627.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="1chronicles_18_9"></a>1Chronicles 18:9

Now when [TōʿÛ](../../strongs/h/h8583.md) [melek](../../strongs/h/h4428.md) of [Ḥămāṯ](../../strongs/h/h2574.md) [shama'](../../strongs/h/h8085.md) how [Dāviḏ](../../strongs/h/h1732.md) had [nakah](../../strongs/h/h5221.md) all the [ḥayil](../../strongs/h/h2428.md) of [HăḏarʿEzer](../../strongs/h/h1928.md) [melek](../../strongs/h/h4428.md) of [Ṣôḇā'](../../strongs/h/h6678.md);

<a name="1chronicles_18_10"></a>1Chronicles 18:10

He [shalach](../../strongs/h/h7971.md) [Hăḏôrām](../../strongs/h/h1913.md) his [ben](../../strongs/h/h1121.md) to [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md), to [sha'al](../../strongs/h/h7592.md) of his [shalowm](../../strongs/h/h7965.md), and to [barak](../../strongs/h/h1288.md) him, because he had [lāḥam](../../strongs/h/h3898.md) against [HăḏarʿEzer](../../strongs/h/h1928.md), and [nakah](../../strongs/h/h5221.md) him; (for [HăḏarʿEzer](../../strongs/h/h1928.md) had ['iysh](../../strongs/h/h376.md) [milḥāmâ](../../strongs/h/h4421.md) with [TōʿÛ](../../strongs/h/h8583.md);) and with him all manner of [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md) and [keceph](../../strongs/h/h3701.md) and [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="1chronicles_18_11"></a>1Chronicles 18:11

Them also [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) [qadash](../../strongs/h/h6942.md) unto [Yĕhovah](../../strongs/h/h3068.md), with the [keceph](../../strongs/h/h3701.md) and the [zāhāḇ](../../strongs/h/h2091.md) that he [nasa'](../../strongs/h/h5375.md) from all these [gowy](../../strongs/h/h1471.md); from ['Ĕḏōm](../../strongs/h/h123.md), and from [Mô'āḇ](../../strongs/h/h4124.md), and from the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and from the [Pᵊlištî](../../strongs/h/h6430.md), and from [ʿĂmālēq](../../strongs/h/h6002.md).

<a name="1chronicles_18_12"></a>1Chronicles 18:12

Moreover ['Ăḇîšay](../../strongs/h/h52.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md) [nakah](../../strongs/h/h5221.md) of the ['Ĕḏōm](../../strongs/h/h123.md) in the [gay'](../../strongs/h/h1516.md) of [melaḥ](../../strongs/h/h4417.md) eighteen thousand.

<a name="1chronicles_18_13"></a>1Chronicles 18:13

And he [śûm](../../strongs/h/h7760.md) [nᵊṣîḇ](../../strongs/h/h5333.md) in ['Ĕḏōm](../../strongs/h/h123.md); and all the ['Ĕḏōm](../../strongs/h/h123.md) became [Dāviḏ](../../strongs/h/h1732.md) ['ebed](../../strongs/h/h5650.md). Thus [Yĕhovah](../../strongs/h/h3068.md) [yasha'](../../strongs/h/h3467.md) [Dāviḏ](../../strongs/h/h1732.md) whithersoever he [halak](../../strongs/h/h1980.md).

<a name="1chronicles_18_14"></a>1Chronicles 18:14

So [Dāviḏ](../../strongs/h/h1732.md) [mālaḵ](../../strongs/h/h4427.md) over all [Yisra'el](../../strongs/h/h3478.md), and ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md) among all his ['am](../../strongs/h/h5971.md).

<a name="1chronicles_18_15"></a>1Chronicles 18:15

And [Yô'āḇ](../../strongs/h/h3097.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md) was over the [tsaba'](../../strongs/h/h6635.md); and [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîlûḏ](../../strongs/h/h286.md), [zakar](../../strongs/h/h2142.md).

<a name="1chronicles_18_16"></a>1Chronicles 18:16

And [Ṣāḏôq](../../strongs/h/h6659.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîṭûḇ](../../strongs/h/h285.md), and ['Ăḇîmeleḵ](../../strongs/h/h40.md) the [ben](../../strongs/h/h1121.md) of ['Eḇyāṯār](../../strongs/h/h54.md), were the [kōhēn](../../strongs/h/h3548.md); and [Šavšā'](../../strongs/h/h7798.md) was [sāp̄ar](../../strongs/h/h5608.md);

<a name="1chronicles_18_17"></a>1Chronicles 18:17

And [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) was over the [kᵊrēṯî](../../strongs/h/h3774.md) and the [pᵊlēṯî](../../strongs/h/h6432.md); and the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md) were [ri'šôn](../../strongs/h/h7223.md) [yad](../../strongs/h/h3027.md) the [melek](../../strongs/h/h4428.md).

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 17](1chronicles_17.md) - [1Chronicles 19](1chronicles_19.md)