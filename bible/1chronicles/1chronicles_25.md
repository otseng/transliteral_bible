# [1Chronicles 25](https://www.blueletterbible.org/kjv/1chronicles/25)

<a name="1chronicles_25_1"></a>1Chronicles 25:1

Moreover [Dāviḏ](../../strongs/h/h1732.md) and the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) [bāḏal](../../strongs/h/h914.md) to the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md), and of [Hêmān](../../strongs/h/h1968.md), and of [Yᵊḏûṯûn](../../strongs/h/h3038.md), who should [nāḇā'](../../strongs/h/h5012.md) [nāḇî'](../../strongs/h/h5030.md) with [kinnôr](../../strongs/h/h3658.md), with [neḇel](../../strongs/h/h5035.md), and with [mᵊṣēleṯ](../../strongs/h/h4700.md): and the [mispār](../../strongs/h/h4557.md) of the ['enowsh](../../strongs/h/h582.md) [mĕla'kah](../../strongs/h/h4399.md) according to their [ʿăḇōḏâ](../../strongs/h/h5656.md) was:

<a name="1chronicles_25_2"></a>1Chronicles 25:2

Of the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md); [Zakûr](../../strongs/h/h2139.md), and [Yôsēp̄](../../strongs/h/h3130.md), and [Nᵊṯanyâ](../../strongs/h/h5418.md), and ['ĂshAr'Ēlâ](../../strongs/h/h841.md), the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md) under the [yad](../../strongs/h/h3027.md) of ['Āsāp̄](../../strongs/h/h623.md), which [nāḇā'](../../strongs/h/h5012.md) according to the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md).

<a name="1chronicles_25_3"></a>1Chronicles 25:3

Of [Yᵊḏûṯûn](../../strongs/h/h3038.md): the [ben](../../strongs/h/h1121.md) of [Yᵊḏûṯûn](../../strongs/h/h3038.md); [Gᵊḏalyâ](../../strongs/h/h1436.md), and [Ṣᵊrî](../../strongs/h/h6874.md), and [Yᵊšaʿyâ](../../strongs/h/h3470.md), [Ḥăšaḇyâ](../../strongs/h/h2811.md), and [Mataṯyâ](../../strongs/h/h4993.md), six, under the [yad](../../strongs/h/h3027.md) of their ['ab](../../strongs/h/h1.md) [Yᵊḏûṯûn](../../strongs/h/h3038.md), who [nāḇā'](../../strongs/h/h5012.md) with a [kinnôr](../../strongs/h/h3658.md), to [yadah](../../strongs/h/h3034.md) and to [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="1chronicles_25_4"></a>1Chronicles 25:4

Of [Hêmān](../../strongs/h/h1968.md): the [ben](../../strongs/h/h1121.md) of [Hêmān](../../strongs/h/h1968.md); [Buqqîyâû](../../strongs/h/h1232.md), [Matanyâ](../../strongs/h/h4983.md), [ʿUzzî'ēl](../../strongs/h/h5816.md), [Šᵊḇû'Ēl](../../strongs/h/h7619.md), and [Yᵊrêmôṯ](../../strongs/h/h3406.md), [Ḥănanyâ](../../strongs/h/h2608.md), [Ḥănānî](../../strongs/h/h2607.md), ['Ĕlî'Āṯâ](../../strongs/h/h448.md), [Gidaltî](../../strongs/h/h1437.md), and [Rvmmṯy ʿZr](../../strongs/h/h7320.md), [Yāšbᵊqāšâ](../../strongs/h/h3436.md), [Mallôṯî](../../strongs/h/h4413.md), [Hôṯîr](../../strongs/h/h1956.md), and [Maḥăzî'ôṯ](../../strongs/h/h4238.md):

<a name="1chronicles_25_5"></a>1Chronicles 25:5

All these were the [ben](../../strongs/h/h1121.md) of [Hêmān](../../strongs/h/h1968.md) the [melek](../../strongs/h/h4428.md) [ḥōzê](../../strongs/h/h2374.md) in the [dabar](../../strongs/h/h1697.md) of ['Elohiym](../../strongs/h/h430.md), to [ruwm](../../strongs/h/h7311.md) the [qeren](../../strongs/h/h7161.md). And ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) to [Hêmān](../../strongs/h/h1968.md) fourteen [ben](../../strongs/h/h1121.md) and three [bath](../../strongs/h/h1323.md).

<a name="1chronicles_25_6"></a>1Chronicles 25:6

All these were under the [yad](../../strongs/h/h3027.md) of their ['ab](../../strongs/h/h1.md) for [šîr](../../strongs/h/h7892.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), with [mᵊṣēleṯ](../../strongs/h/h4700.md), [neḇel](../../strongs/h/h5035.md), and [kinnôr](../../strongs/h/h3658.md), for the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), according to the [melek](../../strongs/h/h4428.md) [yad](../../strongs/h/h3027.md) to ['Āsāp̄](../../strongs/h/h623.md), [Yᵊḏûṯûn](../../strongs/h/h3038.md), and [Hêmān](../../strongs/h/h1968.md).

<a name="1chronicles_25_7"></a>1Chronicles 25:7

So the [mispār](../../strongs/h/h4557.md) of them, with their ['ach](../../strongs/h/h251.md) that were [lamad](../../strongs/h/h3925.md) in the [šîr](../../strongs/h/h7892.md) of [Yĕhovah](../../strongs/h/h3068.md), even all that were [bîn](../../strongs/h/h995.md), was two hundred fourscore and eight.

<a name="1chronicles_25_8"></a>1Chronicles 25:8

And they [naphal](../../strongs/h/h5307.md) [gôrāl](../../strongs/h/h1486.md), [mišmereṯ](../../strongs/h/h4931.md) [ʿummâ](../../strongs/h/h5980.md) ward, as well the [qāṭān](../../strongs/h/h6996.md) as the [gadowl](../../strongs/h/h1419.md), the [bîn](../../strongs/h/h995.md) as the [talmîḏ](../../strongs/h/h8527.md).

<a name="1chronicles_25_9"></a>1Chronicles 25:9

Now the [ri'šôn](../../strongs/h/h7223.md) [gôrāl](../../strongs/h/h1486.md) [yāṣā'](../../strongs/h/h3318.md) for ['Āsāp̄](../../strongs/h/h623.md) to [Yôsēp̄](../../strongs/h/h3130.md): the second to [Gᵊḏalyâ](../../strongs/h/h1436.md), who with his ['ach](../../strongs/h/h251.md) and [ben](../../strongs/h/h1121.md) were twelve :

<a name="1chronicles_25_10"></a>1Chronicles 25:10

The third to [Zakûr](../../strongs/h/h2139.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_11"></a>1Chronicles 25:11

The fourth to [Yiṣrî](../../strongs/h/h3339.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_12"></a>1Chronicles 25:12

The fifth to [Nᵊṯanyâ](../../strongs/h/h5418.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_13"></a>1Chronicles 25:13

The sixth to [Buqqîyâû](../../strongs/h/h1232.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_14"></a>1Chronicles 25:14

The seventh to [Yᵊśar'Ēlâ](../../strongs/h/h3480.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_15"></a>1Chronicles 25:15

The eighth to [Yᵊšaʿyâ](../../strongs/h/h3470.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_16"></a>1Chronicles 25:16

The ninth to [Matanyâ](../../strongs/h/h4983.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_17"></a>1Chronicles 25:17

The tenth to [Šimʿî](../../strongs/h/h8096.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_18"></a>1Chronicles 25:18

The eleventh to [ʿĂzar'Ēl](../../strongs/h/h5832.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_19"></a>1Chronicles 25:19

The twelfth to [Ḥăšaḇyâ](../../strongs/h/h2811.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_20"></a>1Chronicles 25:20

The thirteenth to [Šᵊḇû'Ēl](../../strongs/h/h7619.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_21"></a>1Chronicles 25:21

The fourteenth to [Mataṯyâ](../../strongs/h/h4993.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_22"></a>1Chronicles 25:22

The fifteenth to [Yᵊrêmôṯ](../../strongs/h/h3406.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_23"></a>1Chronicles 25:23

The sixteenth to [Ḥănanyâ](../../strongs/h/h2608.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_24"></a>1Chronicles 25:24

The seventeenth to [Yāšbᵊqāšâ](../../strongs/h/h3436.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_25"></a>1Chronicles 25:25

The eighteenth to [Ḥănānî](../../strongs/h/h2607.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_26"></a>1Chronicles 25:26

The nineteenth to [Mallôṯî](../../strongs/h/h4413.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_27"></a>1Chronicles 25:27

The twentieth to ['Ĕlî'Āṯâ](../../strongs/h/h448.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_28"></a>1Chronicles 25:28

The one and twentieth to [Hôṯîr](../../strongs/h/h1956.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_29"></a>1Chronicles 25:29

The two and twentieth to [Gidaltî](../../strongs/h/h1437.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_30"></a>1Chronicles 25:30

The three and twentieth to [Maḥăzî'ôṯ](../../strongs/h/h4238.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve :

<a name="1chronicles_25_31"></a>1Chronicles 25:31

The four and twentieth to [Rvmmṯy ʿZr](../../strongs/h/h7320.md), he, his [ben](../../strongs/h/h1121.md), and his ['ach](../../strongs/h/h251.md), were twelve .

---

[Transliteral Bible](../bible.md)

[1Chronicles](1chronicles.md)

[1Chronicles 24](1chronicles_24.md) - [1Chronicles 26](1chronicles_26.md)