# [Isaiah 8](https://www.blueletterbible.org/kjv/isa/8/1/s_687001)

<a name="isaiah_8_1"></a>Isaiah 8:1

Moreover [Yĕhovah](../../strongs/h/h3068.md) said unto me, [laqach](../../strongs/h/h3947.md) thee a [gadowl](../../strongs/h/h1419.md) [gillāyôn](../../strongs/h/h1549.md), and [kāṯaḇ](../../strongs/h/h3789.md) in it with an ['enowsh](../../strongs/h/h582.md) [ḥereṭ](../../strongs/h/h2747.md) concerning [mahēr shālāl ḥāsh baz](../../strongs/h/h4122.md).

<a name="isaiah_8_2"></a>Isaiah 8:2

And I took unto me ['aman](../../strongs/h/h539.md) ['ed](../../strongs/h/h5707.md) to [ʿûḏ](../../strongs/h/h5749.md), ['Ûrîyâ](../../strongs/h/h223.md) the [kōhēn](../../strongs/h/h3548.md), and [Zᵊḵaryâ](../../strongs/h/h2148.md) the [ben](../../strongs/h/h1121.md) of [yᵊḇereḵyâû](../../strongs/h/h3000.md).

<a name="isaiah_8_3"></a>Isaiah 8:3

And I [qāraḇ](../../strongs/h/h7126.md) unto the [nᵊḇî'â](../../strongs/h/h5031.md); and she [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md). Then ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) to me, [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [mahēr shālāl ḥāsh baz](../../strongs/h/h4122.md).

<a name="isaiah_8_4"></a>Isaiah 8:4

For before the [naʿar](../../strongs/h/h5288.md) shall have knowledge to [qara'](../../strongs/h/h7121.md), ['ab](../../strongs/h/h1.md), and ['em](../../strongs/h/h517.md), the [ḥayil](../../strongs/h/h2428.md) of [Dammeśeq](../../strongs/h/h1834.md) and the [šālāl](../../strongs/h/h7998.md) of [Šōmrôn](../../strongs/h/h8111.md) shall be [nasa'](../../strongs/h/h5375.md) before the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md).

<a name="isaiah_8_5"></a>Isaiah 8:5

[Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) also unto me again, ['āmar](../../strongs/h/h559.md),

<a name="isaiah_8_6"></a>Isaiah 8:6

Forasmuch as this ['am](../../strongs/h/h5971.md) [mā'as](../../strongs/h/h3988.md) the [mayim](../../strongs/h/h4325.md) of [šilōaḥ](../../strongs/h/h7975.md) that [halak](../../strongs/h/h1980.md) ['aṭ](../../strongs/h/h328.md), and [māśôś](../../strongs/h/h4885.md) in [Rᵊṣîn](../../strongs/h/h7526.md) and [Rᵊmalyâû](../../strongs/h/h7425.md) son;

<a name="isaiah_8_7"></a>Isaiah 8:7

Now therefore, behold, ['adonay](../../strongs/h/h136.md) bringeth up upon them the [mayim](../../strongs/h/h4325.md) of the [nāhār](../../strongs/h/h5104.md), ['atsuwm](../../strongs/h/h6099.md) and [rab](../../strongs/h/h7227.md), even the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), and all his [kabowd](../../strongs/h/h3519.md): and he shall [ʿālâ](../../strongs/h/h5927.md) over all his ['āp̄îq](../../strongs/h/h650.md), and [halak](../../strongs/h/h1980.md) all his [gāḏâ](../../strongs/h/h1415.md):

<a name="isaiah_8_8"></a>Isaiah 8:8

And he shall [ḥālap̄](../../strongs/h/h2498.md) through [Yehuwdah](../../strongs/h/h3063.md); he shall [šāṭap̄](../../strongs/h/h7857.md) and ['abar](../../strongs/h/h5674.md), he shall [naga'](../../strongs/h/h5060.md) even to the [ṣaûā'r](../../strongs/h/h6677.md); and the [muṭṭê](../../strongs/h/h4298.md) of his [kanaph](../../strongs/h/h3671.md) shall [mᵊlō'](../../strongs/h/h4393.md) the [rōḥaḇ](../../strongs/h/h7341.md) of thy ['erets](../../strongs/h/h776.md), [ʿimmānû'ēl](../../strongs/h/h6005.md) ['el](../../strongs/h/h410.md).

<a name="isaiah_8_9"></a>Isaiah 8:9

[ra'a'](../../strongs/h/h7489.md) yourselves, O ye ['am](../../strongs/h/h5971.md), and ye shall be [ḥāṯaṯ](../../strongs/h/h2865.md); and ['azan](../../strongs/h/h238.md), all ye of [merḥāq](../../strongs/h/h4801.md) ['erets](../../strongs/h/h776.md): ['āzar](../../strongs/h/h247.md) yourselves, and ye shall be [ḥāṯaṯ](../../strongs/h/h2865.md); ['āzar](../../strongs/h/h247.md) yourselves, and ye shall be [ḥāṯaṯ](../../strongs/h/h2865.md).

<a name="isaiah_8_10"></a>Isaiah 8:10

[ʿÛṣ](../../strongs/h/h5779.md) ['etsah](../../strongs/h/h6098.md), and it shall [pārar](../../strongs/h/h6565.md); [dabar](../../strongs/h/h1696.md) the [dabar](../../strongs/h/h1697.md), and it shall not [quwm](../../strongs/h/h6965.md): for ['el](../../strongs/h/h410.md) is with us.

<a name="isaiah_8_11"></a>Isaiah 8:11

For [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) thus to me with a [ḥezqâ](../../strongs/h/h2393.md) [yad](../../strongs/h/h3027.md), and [yacar](../../strongs/h/h3256.md) me that I should not [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of this ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md),

<a name="isaiah_8_12"></a>Isaiah 8:12

['āmar](../../strongs/h/h559.md) ye not, A [qešer](../../strongs/h/h7195.md), to all them to whom this ['am](../../strongs/h/h5971.md) shall ['āmar](../../strongs/h/h559.md), A [qešer](../../strongs/h/h7195.md); neither [yare'](../../strongs/h/h3372.md) ye their [mowra'](../../strongs/h/h4172.md), nor be [ʿāraṣ](../../strongs/h/h6206.md).

<a name="isaiah_8_13"></a>Isaiah 8:13

[qadash](../../strongs/h/h6942.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) himself; and let him be your [mowra'](../../strongs/h/h4172.md), and let him be your [ʿāraṣ](../../strongs/h/h6206.md).

<a name="isaiah_8_14"></a>Isaiah 8:14

And he shall be for a [miqdash](../../strongs/h/h4720.md); but for an ['eben](../../strongs/h/h68.md) of [neḡep̄](../../strongs/h/h5063.md) and for a [tsuwr](../../strongs/h/h6697.md) of [miḵšôl](../../strongs/h/h4383.md) to both the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), for a [paḥ](../../strongs/h/h6341.md) and for a [mowqesh](../../strongs/h/h4170.md) to the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="isaiah_8_15"></a>Isaiah 8:15

And many among them shall [kashal](../../strongs/h/h3782.md), and [naphal](../../strongs/h/h5307.md), and be [shabar](../../strongs/h/h7665.md), and be [yāqōš](../../strongs/h/h3369.md), and be [lāḵaḏ](../../strongs/h/h3920.md).

<a name="isaiah_8_16"></a>Isaiah 8:16

[tsarar](../../strongs/h/h6887.md) the [tᵊʿûḏâ](../../strongs/h/h8584.md), [ḥāṯam](../../strongs/h/h2856.md) the [towrah](../../strongs/h/h8451.md) among my [limmûḏ](../../strongs/h/h3928.md).

<a name="isaiah_8_17"></a>Isaiah 8:17

And I will [ḥāḵâ](../../strongs/h/h2442.md) upon [Yĕhovah](../../strongs/h/h3068.md), that [cathar](../../strongs/h/h5641.md) his [paniym](../../strongs/h/h6440.md) from the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), and I will [qāvâ](../../strongs/h/h6960.md) for him.

<a name="isaiah_8_18"></a>Isaiah 8:18

Behold, I and the [yeleḏ](../../strongs/h/h3206.md) whom [Yĕhovah](../../strongs/h/h3068.md) hath given me are for ['ôṯ](../../strongs/h/h226.md) and for [môp̄ēṯ](../../strongs/h/h4159.md) in [Yisra'el](../../strongs/h/h3478.md) from [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), which [shakan](../../strongs/h/h7931.md) in [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md).

<a name="isaiah_8_19"></a>Isaiah 8:19

And when they shall ['āmar](../../strongs/h/h559.md) unto you, [darash](../../strongs/h/h1875.md) unto them that have ['ôḇ](../../strongs/h/h178.md), and unto [yiḏʿōnî](../../strongs/h/h3049.md) that [ṣāp̄ap̄](../../strongs/h/h6850.md), and that [hagah](../../strongs/h/h1897.md): should not an ['am](../../strongs/h/h5971.md) [darash](../../strongs/h/h1875.md) unto their ['Elohiym](../../strongs/h/h430.md)? for the [chay](../../strongs/h/h2416.md) to the [muwth](../../strongs/h/h4191.md)?

<a name="isaiah_8_20"></a>Isaiah 8:20

To the [towrah](../../strongs/h/h8451.md) and to the [tᵊʿûḏâ](../../strongs/h/h8584.md): if they ['āmar](../../strongs/h/h559.md) not according to this [dabar](../../strongs/h/h1697.md), it is because there is no [šaḥar](../../strongs/h/h7837.md) in them.

<a name="isaiah_8_21"></a>Isaiah 8:21

And they shall ['abar](../../strongs/h/h5674.md) through it, [qāšâ](../../strongs/h/h7185.md) and [rāʿēḇ](../../strongs/h/h7457.md): and it shall come to pass, that when they shall be [rāʿēḇ](../../strongs/h/h7456.md), they shall [qāṣap̄](../../strongs/h/h7107.md) themselves, and [qālal](../../strongs/h/h7043.md) their [melek](../../strongs/h/h4428.md) and their ['Elohiym](../../strongs/h/h430.md), and [panah](../../strongs/h/h6437.md) [maʿal](../../strongs/h/h4605.md).

<a name="isaiah_8_22"></a>Isaiah 8:22

And they shall [nabat](../../strongs/h/h5027.md) unto the ['erets](../../strongs/h/h776.md); and behold [tsarah](../../strongs/h/h6869.md) and [ḥăšēḵâ](../../strongs/h/h2825.md), [māʿûp̄](../../strongs/h/h4588.md) of [ṣôq](../../strongs/h/h6695.md); and they shall be [nāḏaḥ](../../strongs/h/h5080.md) to ['ăp̄ēlâ](../../strongs/h/h653.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 7](isaiah_7.md) - [Isaiah 9](isaiah_9.md)