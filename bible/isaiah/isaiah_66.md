# [Isaiah 66](https://www.blueletterbible.org/kjv/isa/66/1/s_745001)

<a name="isaiah_66_1"></a>Isaiah 66:1

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), The [shamayim](../../strongs/h/h8064.md) is my [kicce'](../../strongs/h/h3678.md), and the ['erets](../../strongs/h/h776.md) is my [regel](../../strongs/h/h7272.md) [hăḏōm](../../strongs/h/h1916.md): where is the [bayith](../../strongs/h/h1004.md) that ye [bānâ](../../strongs/h/h1129.md) unto me? and where is the [maqowm](../../strongs/h/h4725.md) of my [mᵊnûḥâ](../../strongs/h/h4496.md)?

<a name="isaiah_66_2"></a>Isaiah 66:2

For all those things hath mine [yad](../../strongs/h/h3027.md) ['asah](../../strongs/h/h6213.md), and all those things have been, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): but to this man will I [nabat](../../strongs/h/h5027.md), even to him that is ['aniy](../../strongs/h/h6041.md) and of a [nāḵê](../../strongs/h/h5223.md) [ruwach](../../strongs/h/h7307.md), and [ḥārēḏ](../../strongs/h/h2730.md) at my [dabar](../../strongs/h/h1697.md).

<a name="isaiah_66_3"></a>Isaiah 66:3

He that [šāḥaṭ](../../strongs/h/h7819.md) a [showr](../../strongs/h/h7794.md) is as if he [nakah](../../strongs/h/h5221.md) an ['iysh](../../strongs/h/h376.md); he that [zabach](../../strongs/h/h2076.md) a [śê](../../strongs/h/h7716.md), as if he [ʿārap̄](../../strongs/h/h6202.md) a [keleḇ](../../strongs/h/h3611.md); he that [ʿālâ](../../strongs/h/h5927.md) a [minchah](../../strongs/h/h4503.md), as if he offered [ḥăzîr](../../strongs/h/h2386.md) [dam](../../strongs/h/h1818.md); he that [zakar](../../strongs/h/h2142.md) [lᵊḇônâ](../../strongs/h/h3828.md), as if he [barak](../../strongs/h/h1288.md) an ['aven](../../strongs/h/h205.md). Yea, they have [bāḥar](../../strongs/h/h977.md) their own [derek](../../strongs/h/h1870.md), and their [nephesh](../../strongs/h/h5315.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) in their [šiqqûṣ](../../strongs/h/h8251.md).

<a name="isaiah_66_4"></a>Isaiah 66:4

I also will [bāḥar](../../strongs/h/h977.md) their [taʿălûlîm](../../strongs/h/h8586.md), and will [bow'](../../strongs/h/h935.md) their [mᵊḡûrâ](../../strongs/h/h4035.md) upon them; because when I [qara'](../../strongs/h/h7121.md), none did ['anah](../../strongs/h/h6030.md); when I [dabar](../../strongs/h/h1696.md), they did not [shama'](../../strongs/h/h8085.md): but they did [ra'](../../strongs/h/h7451.md) before mine ['ayin](../../strongs/h/h5869.md), and [bāḥar](../../strongs/h/h977.md) that in which I [ḥāp̄ēṣ](../../strongs/h/h2654.md) not.

<a name="isaiah_66_5"></a>Isaiah 66:5

[shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), ye that [ḥārēḏ](../../strongs/h/h2730.md) at his [dabar](../../strongs/h/h1697.md); Your ['ach](../../strongs/h/h251.md) that [sane'](../../strongs/h/h8130.md) you, that [nāḏâ](../../strongs/h/h5077.md) for my [shem](../../strongs/h/h8034.md) sake, said, Let [Yĕhovah](../../strongs/h/h3068.md) be [kabad](../../strongs/h/h3513.md): but he shall [ra'ah](../../strongs/h/h7200.md) to your [simchah](../../strongs/h/h8057.md), and they shall be [buwsh](../../strongs/h/h954.md).

<a name="isaiah_66_6"></a>Isaiah 66:6

A [qowl](../../strongs/h/h6963.md) of [shā'ôn](../../strongs/h/h7588.md) from the [ʿîr](../../strongs/h/h5892.md), a [qowl](../../strongs/h/h6963.md) from the [heykal](../../strongs/h/h1964.md), a [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) that [shalam](../../strongs/h/h7999.md) [gĕmwl](../../strongs/h/h1576.md) to his ['oyeb](../../strongs/h/h341.md).

<a name="isaiah_66_7"></a>Isaiah 66:7

Before she [chuwl](../../strongs/h/h2342.md), she [yalad](../../strongs/h/h3205.md); before her [chebel](../../strongs/h/h2256.md) [bow'](../../strongs/h/h935.md), she was [mālaṭ](../../strongs/h/h4422.md) of a [zāḵār](../../strongs/h/h2145.md).

<a name="isaiah_66_8"></a>Isaiah 66:8

Who hath [shama'](../../strongs/h/h8085.md) such a thing? who hath [ra'ah](../../strongs/h/h7200.md) such things? Shall the ['erets](../../strongs/h/h776.md) be made to [chuwl](../../strongs/h/h2342.md) in one [yowm](../../strongs/h/h3117.md)? or shall a [gowy](../../strongs/h/h1471.md) be [yalad](../../strongs/h/h3205.md) at once?  for as soon as [Tsiyown](../../strongs/h/h6726.md) [chuwl](../../strongs/h/h2342.md), she [yalad](../../strongs/h/h3205.md) her [ben](../../strongs/h/h1121.md).

<a name="isaiah_66_9"></a>Isaiah 66:9

Shall I [shabar](../../strongs/h/h7665.md), and not cause to [yalad](../../strongs/h/h3205.md)? saith [Yĕhovah](../../strongs/h/h3068.md): shall I cause to [yalad](../../strongs/h/h3205.md), and [ʿāṣar](../../strongs/h/h6113.md) the womb? ['āmar](../../strongs/h/h559.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_66_10"></a>Isaiah 66:10

[samach](../../strongs/h/h8055.md) ye with [Yĕruwshalaim](../../strongs/h/h3389.md), and be [giyl](../../strongs/h/h1523.md) with her, all ye that ['ahab](../../strongs/h/h157.md) her: [śûś](../../strongs/h/h7797.md) for [māśôś](../../strongs/h/h4885.md) with her, all ye that ['āḇal](../../strongs/h/h56.md) for her:

<a name="isaiah_66_11"></a>Isaiah 66:11

That ye may [yānaq](../../strongs/h/h3243.md), and be [sāׂbaʿ](../../strongs/h/h7646.md) with the [šaḏ](../../strongs/h/h7699.md) of her [tanḥûmôṯ](../../strongs/h/h8575.md); that ye may [māṣaṣ](../../strongs/h/h4711.md), and be [ʿānaḡ](../../strongs/h/h6026.md) with the [zîz](../../strongs/h/h2123.md) of her [kabowd](../../strongs/h/h3519.md).

<a name="isaiah_66_12"></a>Isaiah 66:12

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Behold, I will [natah](../../strongs/h/h5186.md) [shalowm](../../strongs/h/h7965.md) to her like a [nāhār](../../strongs/h/h5104.md), and the [kabowd](../../strongs/h/h3519.md) of the [gowy](../../strongs/h/h1471.md) like a [šāṭap̄](../../strongs/h/h7857.md) [nachal](../../strongs/h/h5158.md): then shall ye [yānaq](../../strongs/h/h3243.md), ye shall be [nasa'](../../strongs/h/h5375.md) upon her [ṣaḏ](../../strongs/h/h6654.md), and be [šāʿaʿ](../../strongs/h/h8173.md) upon her [bereḵ](../../strongs/h/h1290.md).

<a name="isaiah_66_13"></a>Isaiah 66:13

As one whom his ['em](../../strongs/h/h517.md) [nacham](../../strongs/h/h5162.md), so will I [nacham](../../strongs/h/h5162.md) you; and ye shall be [nacham](../../strongs/h/h5162.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="isaiah_66_14"></a>Isaiah 66:14

And when ye [ra'ah](../../strongs/h/h7200.md) this, your [leb](../../strongs/h/h3820.md) shall [śûś](../../strongs/h/h7797.md), and your ['etsem](../../strongs/h/h6106.md) shall [pāraḥ](../../strongs/h/h6524.md) like [deše'](../../strongs/h/h1877.md): and the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) shall be [yada'](../../strongs/h/h3045.md) toward his ['ebed](../../strongs/h/h5650.md), and his [za'am](../../strongs/h/h2194.md) toward his ['oyeb](../../strongs/h/h341.md).

<a name="isaiah_66_15"></a>Isaiah 66:15

For, behold, [Yĕhovah](../../strongs/h/h3068.md) will [bow'](../../strongs/h/h935.md) with ['esh](../../strongs/h/h784.md), and with his [merkāḇâ](../../strongs/h/h4818.md) like a [sûp̄â](../../strongs/h/h5492.md), to render his ['aph](../../strongs/h/h639.md) with [chemah](../../strongs/h/h2534.md), and his [ge'arah](../../strongs/h/h1606.md) with [lahaḇ](../../strongs/h/h3851.md) of ['esh](../../strongs/h/h784.md).

<a name="isaiah_66_16"></a>Isaiah 66:16

For by ['esh](../../strongs/h/h784.md) and by his [chereb](../../strongs/h/h2719.md) will [Yĕhovah](../../strongs/h/h3068.md) [shaphat](../../strongs/h/h8199.md) with all [basar](../../strongs/h/h1320.md): and the [ḥālāl](../../strongs/h/h2491.md) of [Yĕhovah](../../strongs/h/h3068.md) shall be many.

<a name="isaiah_66_17"></a>Isaiah 66:17

They that [qadash](../../strongs/h/h6942.md) themselves, and [ṭāhēr](../../strongs/h/h2891.md) themselves in the [gannâ](../../strongs/h/h1593.md) behind ['echad](../../strongs/h/h259.md) tree in the [tavek](../../strongs/h/h8432.md), ['akal](../../strongs/h/h398.md) [ḥăzîr](../../strongs/h/h2386.md) [basar](../../strongs/h/h1320.md), and the [šeqeṣ](../../strongs/h/h8263.md), and the [ʿaḵbār](../../strongs/h/h5909.md), shall be [sûp̄](../../strongs/h/h5486.md) together, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_66_18"></a>Isaiah 66:18

For I know their [ma'aseh](../../strongs/h/h4639.md) and their [maḥăšāḇâ](../../strongs/h/h4284.md): it shall [bow'](../../strongs/h/h935.md), that I will [qāḇaṣ](../../strongs/h/h6908.md) all [gowy](../../strongs/h/h1471.md) and [lashown](../../strongs/h/h3956.md); and they shall [bow'](../../strongs/h/h935.md), and see my [kabowd](../../strongs/h/h3519.md).

<a name="isaiah_66_19"></a>Isaiah 66:19

And I will [śûm](../../strongs/h/h7760.md) an ['ôṯ](../../strongs/h/h226.md) among them, and I will [shalach](../../strongs/h/h7971.md) those that [pālîṭ](../../strongs/h/h6412.md) of them unto the [gowy](../../strongs/h/h1471.md), to [Taršîš](../../strongs/h/h8659.md), [Pûl](../../strongs/h/h6322.md), and [Lûḏ](../../strongs/h/h3865.md), that [mashak](../../strongs/h/h4900.md) the [qesheth](../../strongs/h/h7198.md), to [Tuḇal](../../strongs/h/h8422.md), and [Yāvān](../../strongs/h/h3120.md), to the ['î](../../strongs/h/h339.md) [rachowq](../../strongs/h/h7350.md), that have not [shama'](../../strongs/h/h8085.md) my [šēmaʿ](../../strongs/h/h8088.md), neither have [ra'ah](../../strongs/h/h7200.md) my [kabowd](../../strongs/h/h3519.md); and they shall [nāḡaḏ](../../strongs/h/h5046.md) my [kabowd](../../strongs/h/h3519.md) among the [gowy](../../strongs/h/h1471.md).

<a name="isaiah_66_20"></a>Isaiah 66:20

And they shall [bow'](../../strongs/h/h935.md) all your ['ach](../../strongs/h/h251.md) for a [minchah](../../strongs/h/h4503.md) unto [Yĕhovah](../../strongs/h/h3068.md) out of all [gowy](../../strongs/h/h1471.md) upon [sûs](../../strongs/h/h5483.md), and in [reḵeḇ](../../strongs/h/h7393.md), and in [ṣaḇ](../../strongs/h/h6632.md), and upon [pereḏ](../../strongs/h/h6505.md), and upon [kirkārâ](../../strongs/h/h3753.md), to my [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md) [Yĕruwshalaim](../../strongs/h/h3389.md), saith [Yĕhovah](../../strongs/h/h3068.md), as the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) a [minchah](../../strongs/h/h4503.md) in a [tahowr](../../strongs/h/h2889.md) [kĕliy](../../strongs/h/h3627.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_66_21"></a>Isaiah 66:21

And I will also [laqach](../../strongs/h/h3947.md) of them for [kōhēn](../../strongs/h/h3548.md) and for [Lᵊvî](../../strongs/h/h3881.md), saith [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_66_22"></a>Isaiah 66:22

For as the [ḥāḏāš](../../strongs/h/h2319.md) [shamayim](../../strongs/h/h8064.md) and the [ḥāḏāš](../../strongs/h/h2319.md) ['erets](../../strongs/h/h776.md), which I will ['asah](../../strongs/h/h6213.md), shall ['amad](../../strongs/h/h5975.md) before me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), so shall your [zera'](../../strongs/h/h2233.md) and your [shem](../../strongs/h/h8034.md) remain.

<a name="isaiah_66_23"></a>Isaiah 66:23

And it shall come to pass, that from one [ḥōḏeš](../../strongs/h/h2320.md) to another, and from one [shabbath](../../strongs/h/h7676.md) to another, shall all [basar](../../strongs/h/h1320.md) come to [shachah](../../strongs/h/h7812.md) before me, saith [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_66_24"></a>Isaiah 66:24

And they shall [yāṣā'](../../strongs/h/h3318.md), and [ra'ah](../../strongs/h/h7200.md) upon the [peḡer](../../strongs/h/h6297.md) of the ['enowsh](../../strongs/h/h582.md) that have [pāšaʿ](../../strongs/h/h6586.md) against me: for their [tôlāʿ](../../strongs/h/h8438.md) shall not [muwth](../../strongs/h/h4191.md), neither shall their ['esh](../../strongs/h/h784.md) be [kāḇâ](../../strongs/h/h3518.md); and they shall be a [dᵊrā'ôn](../../strongs/h/h1860.md) unto all [basar](../../strongs/h/h1320.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 65](isaiah_65.md)