# [Isaiah 58](https://www.blueletterbible.org/kjv/isa/58/1/s_737001)

<a name="isaiah_58_1"></a>Isaiah 58:1

[qara'](../../strongs/h/h7121.md) [garown](../../strongs/h/h1627.md), [ḥāśaḵ](../../strongs/h/h2820.md) not, [ruwm](../../strongs/h/h7311.md) up thy [qowl](../../strongs/h/h6963.md) like a [šôp̄ār](../../strongs/h/h7782.md), and [nāḡaḏ](../../strongs/h/h5046.md) my ['am](../../strongs/h/h5971.md) their [pesha'](../../strongs/h/h6588.md), and the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md) their [chatta'ath](../../strongs/h/h2403.md).

<a name="isaiah_58_2"></a>Isaiah 58:2

Yet they [darash](../../strongs/h/h1875.md) me [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md), and [ḥāp̄ēṣ](../../strongs/h/h2654.md) to [da'ath](../../strongs/h/h1847.md) my [derek](../../strongs/h/h1870.md), as a [gowy](../../strongs/h/h1471.md) that did [ṣĕdāqāh](../../strongs/h/h6666.md), and ['azab](../../strongs/h/h5800.md) not the [mishpat](../../strongs/h/h4941.md) of their ['Elohiym](../../strongs/h/h430.md): they [sha'al](../../strongs/h/h7592.md) of me the [mishpat](../../strongs/h/h4941.md) of [tsedeq](../../strongs/h/h6664.md); they take [ḥāp̄ēṣ](../../strongs/h/h2654.md) in [qᵊrāḇâ](../../strongs/h/h7132.md) to ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_58_3"></a>Isaiah 58:3

Wherefore have we [ṣûm](../../strongs/h/h6684.md), say they, and thou [ra'ah](../../strongs/h/h7200.md) not? wherefore have we [ʿānâ](../../strongs/h/h6031.md) our [nephesh](../../strongs/h/h5315.md), and thou takest no [yada'](../../strongs/h/h3045.md)? Behold, in the [yowm](../../strongs/h/h3117.md) of your [ṣôm](../../strongs/h/h6685.md) ye [māṣā'](../../strongs/h/h4672.md) [chephets](../../strongs/h/h2656.md), and [nāḡaś](../../strongs/h/h5065.md) all your [ʿāṣēḇ](../../strongs/h/h6092.md).

<a name="isaiah_58_4"></a>Isaiah 58:4

Behold, ye [ṣûm](../../strongs/h/h6684.md) for [rîḇ](../../strongs/h/h7379.md) and [maṣṣâ](../../strongs/h/h4683.md), and to [nakah](../../strongs/h/h5221.md) with the ['eḡrōp̄](../../strongs/h/h106.md) of [resha'](../../strongs/h/h7562.md): ye shall not [ṣûm](../../strongs/h/h6684.md) as ye do this [yowm](../../strongs/h/h3117.md), to make your [qowl](../../strongs/h/h6963.md) to be [shama'](../../strongs/h/h8085.md) on [marowm](../../strongs/h/h4791.md).

<a name="isaiah_58_5"></a>Isaiah 58:5

Is it such a [ṣôm](../../strongs/h/h6685.md) that I have [bāḥar](../../strongs/h/h977.md)? a [yowm](../../strongs/h/h3117.md) for an ['adam](../../strongs/h/h120.md) to [ʿānâ](../../strongs/h/h6031.md) his [nephesh](../../strongs/h/h5315.md)? is it to [kāp̄ap̄](../../strongs/h/h3721.md) his [ro'sh](../../strongs/h/h7218.md) as an ['aḡmôn](../../strongs/h/h100.md), and to [yāṣaʿ](../../strongs/h/h3331.md) [śaq](../../strongs/h/h8242.md) and ['ēp̄er](../../strongs/h/h665.md) under him? wilt thou [qara'](../../strongs/h/h7121.md) this a [ṣôm](../../strongs/h/h6685.md), and a [ratsown](../../strongs/h/h7522.md) [yowm](../../strongs/h/h3117.md) to [Yĕhovah](../../strongs/h/h3068.md)?

<a name="isaiah_58_6"></a>Isaiah 58:6

Is not this the [ṣôm](../../strongs/h/h6685.md) that I have [bāḥar](../../strongs/h/h977.md)? to [pāṯaḥ](../../strongs/h/h6605.md) the [ḥarṣubâ](../../strongs/h/h2784.md) of [resha'](../../strongs/h/h7562.md), to [nāṯar](../../strongs/h/h5425.md) the [môṭâ](../../strongs/h/h4133.md) ['ăḡudâ](../../strongs/h/h92.md), and to let the [rāṣaṣ](../../strongs/h/h7533.md) [shalach](../../strongs/h/h7971.md) [ḥāp̄šî](../../strongs/h/h2670.md), and that ye [nathaq](../../strongs/h/h5423.md) every [môṭâ](../../strongs/h/h4133.md)?

<a name="isaiah_58_7"></a>Isaiah 58:7

Is it not to [Pāras](../../strongs/h/h6536.md) thy [lechem](../../strongs/h/h3899.md) to the [rāʿēḇ](../../strongs/h/h7457.md), and that thou bring the ['aniy](../../strongs/h/h6041.md) that are [mārûḏ](../../strongs/h/h4788.md) to thy [bayith](../../strongs/h/h1004.md)? when thou seest the ['arowm](../../strongs/h/h6174.md), that thou [kāsâ](../../strongs/h/h3680.md) him; and that thou ['alam](../../strongs/h/h5956.md) not thyself from thine own [basar](../../strongs/h/h1320.md)?

<a name="isaiah_58_8"></a>Isaiah 58:8

Then shall thy ['owr](../../strongs/h/h216.md) [bāqaʿ](../../strongs/h/h1234.md) as the [šaḥar](../../strongs/h/h7837.md), and thine ['ărûḵâ](../../strongs/h/h724.md) shall [ṣāmaḥ](../../strongs/h/h6779.md) [mᵊhērâ](../../strongs/h/h4120.md): and thy [tsedeq](../../strongs/h/h6664.md) shall [halak](../../strongs/h/h1980.md) before thee; the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) shall be thy ['āsap̄](../../strongs/h/h622.md).

<a name="isaiah_58_9"></a>Isaiah 58:9

Then shalt thou [qara'](../../strongs/h/h7121.md), and [Yĕhovah](../../strongs/h/h3068.md) shall ['anah](../../strongs/h/h6030.md); thou shalt [šāvaʿ](../../strongs/h/h7768.md), and he shall ['āmar](../../strongs/h/h559.md), Here I am. If thou [cuwr](../../strongs/h/h5493.md) from the [tavek](../../strongs/h/h8432.md) of thee the [môṭâ](../../strongs/h/h4133.md), the [shalach](../../strongs/h/h7971.md) of the ['etsba'](../../strongs/h/h676.md), and [dabar](../../strongs/h/h1696.md) ['aven](../../strongs/h/h205.md);

<a name="isaiah_58_10"></a>Isaiah 58:10

And if thou [pûq](../../strongs/h/h6329.md) thy [nephesh](../../strongs/h/h5315.md) to the [rāʿēḇ](../../strongs/h/h7457.md), and [sāׂbaʿ](../../strongs/h/h7646.md) the [ʿānâ](../../strongs/h/h6031.md) [nephesh](../../strongs/h/h5315.md); then shall thy ['owr](../../strongs/h/h216.md) [zāraḥ](../../strongs/h/h2224.md) in [choshek](../../strongs/h/h2822.md), and thy ['ăp̄ēlâ](../../strongs/h/h653.md) be as the [ṣōhar](../../strongs/h/h6672.md):

<a name="isaiah_58_11"></a>Isaiah 58:11

And [Yĕhovah](../../strongs/h/h3068.md) shall [nachah](../../strongs/h/h5148.md) thee [tāmîḏ](../../strongs/h/h8548.md), and [sāׂbaʿ](../../strongs/h/h7646.md) thy [nephesh](../../strongs/h/h5315.md) in [ṣaḥṣāḥôṯ](../../strongs/h/h6710.md), and [chalats](../../strongs/h/h2502.md) thy ['etsem](../../strongs/h/h6106.md): and thou shalt be like a [rāvê](../../strongs/h/h7302.md) [gan](../../strongs/h/h1588.md), and like a [môṣā'](../../strongs/h/h4161.md) of [mayim](../../strongs/h/h4325.md), whose [mayim](../../strongs/h/h4325.md) [kāzaḇ](../../strongs/h/h3576.md) not.

<a name="isaiah_58_12"></a>Isaiah 58:12

And they that shall be of thee shall [bānâ](../../strongs/h/h1129.md) the ['owlam](../../strongs/h/h5769.md) [chorbah](../../strongs/h/h2723.md): thou shalt [quwm](../../strongs/h/h6965.md) the [mowcadah](../../strongs/h/h4146.md) of many [dôr](../../strongs/h/h1755.md); and thou shalt be [qara'](../../strongs/h/h7121.md), The [gāḏar](../../strongs/h/h1443.md) of the [pereṣ](../../strongs/h/h6556.md), The [shuwb](../../strongs/h/h7725.md) of [nāṯîḇ](../../strongs/h/h5410.md) to [yashab](../../strongs/h/h3427.md) in.

<a name="isaiah_58_13"></a>Isaiah 58:13

If thou [shuwb](../../strongs/h/h7725.md) thy [regel](../../strongs/h/h7272.md) from the [shabbath](../../strongs/h/h7676.md), from doing thy [chephets](../../strongs/h/h2656.md) on my [qodesh](../../strongs/h/h6944.md) [yowm](../../strongs/h/h3117.md); and [qara'](../../strongs/h/h7121.md) the [shabbath](../../strongs/h/h7676.md) a [ʿōneḡ](../../strongs/h/h6027.md), the [qadowsh](../../strongs/h/h6918.md) of [Yĕhovah](../../strongs/h/h3068.md), [kabad](../../strongs/h/h3513.md); and shalt [kabad](../../strongs/h/h3513.md) him, not ['asah](../../strongs/h/h6213.md) thine own [derek](../../strongs/h/h1870.md), nor [māṣā'](../../strongs/h/h4672.md) thine own [chephets](../../strongs/h/h2656.md), nor [dabar](../../strongs/h/h1696.md) thine own [dabar](../../strongs/h/h1697.md):

<a name="isaiah_58_14"></a>Isaiah 58:14

Then shalt thou [ʿānaḡ](../../strongs/h/h6026.md) thyself in [Yĕhovah](../../strongs/h/h3068.md); and I will cause thee to [rāḵaḇ](../../strongs/h/h7392.md) upon the [bāmâ](../../strongs/h/h1116.md) of the ['erets](../../strongs/h/h776.md), and ['akal](../../strongs/h/h398.md) thee with the [nachalah](../../strongs/h/h5159.md) of [Ya'aqob](../../strongs/h/h3290.md) thy ['ab](../../strongs/h/h1.md): for the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) it.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 57](isaiah_57.md) - [Isaiah 59](isaiah_59.md)