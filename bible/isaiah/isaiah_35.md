# [Isaiah 35](https://www.blueletterbible.org/kjv/isa/35/1/s_714001)

<a name="isaiah_35_1"></a>Isaiah 35:1

The [midbar](../../strongs/h/h4057.md) and the [ṣîyâ](../../strongs/h/h6723.md) shall [śûś](../../strongs/h/h7797.md) for them; and the ['arabah](../../strongs/h/h6160.md) shall [giyl](../../strongs/h/h1523.md), and [pāraḥ](../../strongs/h/h6524.md) as the [ḥăḇaṣṣeleṯ](../../strongs/h/h2261.md).

<a name="isaiah_35_2"></a>Isaiah 35:2

It shall [pāraḥ](../../strongs/h/h6524.md) [pāraḥ](../../strongs/h/h6524.md), and [giyl](../../strongs/h/h1523.md) even with [gîlâ](../../strongs/h/h1525.md) and [rannēn](../../strongs/h/h7444.md): the [kabowd](../../strongs/h/h3519.md) of [Lᵊḇānôn](../../strongs/h/h3844.md) shall be [nathan](../../strongs/h/h5414.md) unto it, the [hadar](../../strongs/h/h1926.md) of [karmel](../../strongs/h/h3760.md) and [Šārôn](../../strongs/h/h8289.md), they shall [ra'ah](../../strongs/h/h7200.md) the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [hadar](../../strongs/h/h1926.md) of our ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_35_3"></a>Isaiah 35:3

[ḥāzaq](../../strongs/h/h2388.md) ye the [rāp̄ê](../../strongs/h/h7504.md) [yad](../../strongs/h/h3027.md), and ['amats](../../strongs/h/h553.md) the [kashal](../../strongs/h/h3782.md) [bereḵ](../../strongs/h/h1290.md).

<a name="isaiah_35_4"></a>Isaiah 35:4

['āmar](../../strongs/h/h559.md) to them that are of a [māhar](../../strongs/h/h4116.md) [leb](../../strongs/h/h3820.md), [ḥāzaq](../../strongs/h/h2388.md), [yare'](../../strongs/h/h3372.md) not: behold, your ['Elohiym](../../strongs/h/h430.md) will [bow'](../../strongs/h/h935.md) with [nāqām](../../strongs/h/h5359.md), even ['Elohiym](../../strongs/h/h430.md) with a [gĕmwl](../../strongs/h/h1576.md); he will [bow'](../../strongs/h/h935.md) and [yasha'](../../strongs/h/h3467.md) you.

<a name="isaiah_35_5"></a>Isaiah 35:5

Then the ['ayin](../../strongs/h/h5869.md) of the [ʿiûēr](../../strongs/h/h5787.md) shall be [paqach](../../strongs/h/h6491.md), and the ['ozen](../../strongs/h/h241.md) of the [ḥērēš](../../strongs/h/h2795.md) shall be [pāṯaḥ](../../strongs/h/h6605.md).

<a name="isaiah_35_6"></a>Isaiah 35:6

Then shall the [pissēaḥ](../../strongs/h/h6455.md) man [dālaḡ](../../strongs/h/h1801.md) as an ['ayyāl](../../strongs/h/h354.md), and the [lashown](../../strongs/h/h3956.md) of the ['illēm](../../strongs/h/h483.md) [ranan](../../strongs/h/h7442.md): for in the [midbar](../../strongs/h/h4057.md) shall [mayim](../../strongs/h/h4325.md) [bāqaʿ](../../strongs/h/h1234.md), and [nachal](../../strongs/h/h5158.md) in the ['arabah](../../strongs/h/h6160.md).

<a name="isaiah_35_7"></a>Isaiah 35:7

And the [šārāḇ](../../strongs/h/h8273.md) shall become a ['ăḡam](../../strongs/h/h98.md), and the [ṣimmā'ôn](../../strongs/h/h6774.md) [mabûaʿ](../../strongs/h/h4002.md) of [mayim](../../strongs/h/h4325.md): in the [nāvê](../../strongs/h/h5116.md) of [tannîn](../../strongs/h/h8577.md), [rēḇeṣ](../../strongs/h/h7258.md), shall be [chatsiyr](../../strongs/h/h2682.md) with [qānê](../../strongs/h/h7070.md) and [gōme'](../../strongs/h/h1573.md).

<a name="isaiah_35_8"></a>Isaiah 35:8

And a [maslûl](../../strongs/h/h4547.md) shall be there, and a [derek](../../strongs/h/h1870.md), and it shall be [qara'](../../strongs/h/h7121.md) The [derek](../../strongs/h/h1870.md) of [qodesh](../../strongs/h/h6944.md); the [tame'](../../strongs/h/h2931.md) shall not ['abar](../../strongs/h/h5674.md) it; but it shall be for those: the [halak](../../strongs/h/h1980.md) men, though ['ĕvîl](../../strongs/h/h191.md), shall not [tāʿâ](../../strongs/h/h8582.md) therein.

<a name="isaiah_35_9"></a>Isaiah 35:9

No ['ariy](../../strongs/h/h738.md) shall be there, nor any [periyts](../../strongs/h/h6530.md) [chay](../../strongs/h/h2416.md) shall [ʿālâ](../../strongs/h/h5927.md) thereon, it shall not be [māṣā'](../../strongs/h/h4672.md) there; but the [gā'al](../../strongs/h/h1350.md) shall [halak](../../strongs/h/h1980.md) there:

<a name="isaiah_35_10"></a>Isaiah 35:10

And the [pāḏâ](../../strongs/h/h6299.md) of [Yĕhovah](../../strongs/h/h3068.md) shall [shuwb](../../strongs/h/h7725.md), and [bow'](../../strongs/h/h935.md) to [Tsiyown](../../strongs/h/h6726.md) with [rinnah](../../strongs/h/h7440.md) and ['owlam](../../strongs/h/h5769.md) [simchah](../../strongs/h/h8057.md) upon their [ro'sh](../../strongs/h/h7218.md): they shall [nāśaḡ](../../strongs/h/h5381.md) [simchah](../../strongs/h/h8057.md) and [śāśôn](../../strongs/h/h8342.md), and [yagown](../../strongs/h/h3015.md) and ['anachah](../../strongs/h/h585.md) shall [nûs](../../strongs/h/h5127.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 34](isaiah_34.md) - [Isaiah 36](isaiah_36.md)