# [Isaiah 63](https://www.blueletterbible.org/kjv/isa/63/1/s_742001)

<a name="isaiah_63_1"></a>Isaiah 63:1

Who is this that [bow'](../../strongs/h/h935.md) from ['Ĕḏōm](../../strongs/h/h123.md), with [ḥāmēṣ](../../strongs/h/h2556.md) [beḡeḏ](../../strongs/h/h899.md) from [Bāṣrâ](../../strongs/h/h1224.md)?  this that is [hāḏar](../../strongs/h/h1921.md) in his [lᵊḇûš](../../strongs/h/h3830.md), [ṣāʿâ](../../strongs/h/h6808.md) in the [rōḇ](../../strongs/h/h7230.md) of his [koach](../../strongs/h/h3581.md)? I that [dabar](../../strongs/h/h1696.md) in [ṣĕdāqāh](../../strongs/h/h6666.md), [rab](../../strongs/h/h7227.md) to [yasha'](../../strongs/h/h3467.md).

<a name="isaiah_63_2"></a>Isaiah 63:2

Wherefore art thou ['āḏōm](../../strongs/h/h122.md) in thine [lᵊḇûš](../../strongs/h/h3830.md), and thy [beḡeḏ](../../strongs/h/h899.md) like him that [dāraḵ](../../strongs/h/h1869.md) in the [gaṯ](../../strongs/h/h1660.md)?

<a name="isaiah_63_3"></a>Isaiah 63:3

I have [dāraḵ](../../strongs/h/h1869.md) the [pûrâ](../../strongs/h/h6333.md) alone; and of the ['am](../../strongs/h/h5971.md) there was none with me: for I will [dāraḵ](../../strongs/h/h1869.md) them in mine ['aph](../../strongs/h/h639.md), and [rāmas](../../strongs/h/h7429.md) them in my [chemah](../../strongs/h/h2534.md); and their [nēṣaḥ](../../strongs/h/h5332.md) shall be [nāzâ](../../strongs/h/h5137.md) upon my [beḡeḏ](../../strongs/h/h899.md), and I will [gā'al](../../strongs/h/h1351.md) all my [malbûš](../../strongs/h/h4403.md).

<a name="isaiah_63_4"></a>Isaiah 63:4

For the [yowm](../../strongs/h/h3117.md) of [nāqām](../../strongs/h/h5359.md) is in mine [leb](../../strongs/h/h3820.md), and the [šānâ](../../strongs/h/h8141.md) of my [gā'al](../../strongs/h/h1350.md) is [bow'](../../strongs/h/h935.md).

<a name="isaiah_63_5"></a>Isaiah 63:5

And I [nabat](../../strongs/h/h5027.md), and there was none to [ʿāzar](../../strongs/h/h5826.md); and I [šāmēm](../../strongs/h/h8074.md) that there was none to [camak](../../strongs/h/h5564.md): therefore mine own [zerowa'](../../strongs/h/h2220.md) brought [yasha'](../../strongs/h/h3467.md) unto me; and my [chemah](../../strongs/h/h2534.md), it [camak](../../strongs/h/h5564.md) me.

<a name="isaiah_63_6"></a>Isaiah 63:6

And I will [bûs](../../strongs/h/h947.md) the ['am](../../strongs/h/h5971.md) in mine ['aph](../../strongs/h/h639.md), and make them [šāḵar](../../strongs/h/h7937.md) in my [chemah](../../strongs/h/h2534.md), and I will [yarad](../../strongs/h/h3381.md) their [nēṣaḥ](../../strongs/h/h5332.md) to the ['erets](../../strongs/h/h776.md).

<a name="isaiah_63_7"></a>Isaiah 63:7

I will [zakar](../../strongs/h/h2142.md) the [checed](../../strongs/h/h2617.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [tehillah](../../strongs/h/h8416.md) of [Yĕhovah](../../strongs/h/h3068.md), according to all that [Yĕhovah](../../strongs/h/h3068.md) hath [gamal](../../strongs/h/h1580.md) on us, and the [rab](../../strongs/h/h7227.md) [ṭûḇ](../../strongs/h/h2898.md) toward the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), which he hath [gamal](../../strongs/h/h1580.md) on them according to his [raḥam](../../strongs/h/h7356.md), and according to the [rōḇ](../../strongs/h/h7230.md) of his [checed](../../strongs/h/h2617.md).

<a name="isaiah_63_8"></a>Isaiah 63:8

For he ['āmar](../../strongs/h/h559.md), Surely they are my ['am](../../strongs/h/h5971.md), [ben](../../strongs/h/h1121.md) that will not [šāqar](../../strongs/h/h8266.md): so he was their [yasha'](../../strongs/h/h3467.md).

<a name="isaiah_63_9"></a>Isaiah 63:9

In all their [tsarah](../../strongs/h/h6869.md) he was [tsar](../../strongs/h/h6862.md), and the [mal'ak](../../strongs/h/h4397.md) of his [paniym](../../strongs/h/h6440.md) [yasha'](../../strongs/h/h3467.md) them: in his ['ahăḇâ](../../strongs/h/h160.md) and in his [ḥemlâ](../../strongs/h/h2551.md) he [gā'al](../../strongs/h/h1350.md) them; and he [nāṭal](../../strongs/h/h5190.md) them, and [nasa'](../../strongs/h/h5375.md) them all the [yowm](../../strongs/h/h3117.md) of ['owlam](../../strongs/h/h5769.md).

<a name="isaiah_63_10"></a>Isaiah 63:10

But they [marah](../../strongs/h/h4784.md), and [ʿāṣaḇ](../../strongs/h/h6087.md) his [qodesh](../../strongs/h/h6944.md) [ruwach](../../strongs/h/h7307.md): therefore he was [hāp̄aḵ](../../strongs/h/h2015.md) to be their ['oyeb](../../strongs/h/h341.md), and he [lāḥam](../../strongs/h/h3898.md) against them.

<a name="isaiah_63_11"></a>Isaiah 63:11

Then he [zakar](../../strongs/h/h2142.md) the [yowm](../../strongs/h/h3117.md) of ['owlam](../../strongs/h/h5769.md), [Mōsheׁh](../../strongs/h/h4872.md), and his ['am](../../strongs/h/h5971.md), saying, Where is he that [ʿālâ](../../strongs/h/h5927.md) them out of the [yam](../../strongs/h/h3220.md) with the [ra'ah](../../strongs/h/h7462.md) of his [tso'n](../../strongs/h/h6629.md)? where is he that [śûm](../../strongs/h/h7760.md) his [qodesh](../../strongs/h/h6944.md) [ruwach](../../strongs/h/h7307.md) within him?

<a name="isaiah_63_12"></a>Isaiah 63:12

That [yālaḵ](../../strongs/h/h3212.md) them by the [yamiyn](../../strongs/h/h3225.md) of [Mōsheׁh](../../strongs/h/h4872.md) with his [tip̄'ārâ](../../strongs/h/h8597.md) [zerowa'](../../strongs/h/h2220.md), [bāqaʿ](../../strongs/h/h1234.md) the [mayim](../../strongs/h/h4325.md) before them, to make himself an ['owlam](../../strongs/h/h5769.md) [shem](../../strongs/h/h8034.md)?

<a name="isaiah_63_13"></a>Isaiah 63:13

That [yālaḵ](../../strongs/h/h3212.md) them through the [tĕhowm](../../strongs/h/h8415.md), as a [sûs](../../strongs/h/h5483.md) in the [midbar](../../strongs/h/h4057.md), that they should not [kashal](../../strongs/h/h3782.md)?

<a name="isaiah_63_14"></a>Isaiah 63:14

As a [bĕhemah](../../strongs/h/h929.md) [yarad](../../strongs/h/h3381.md) into the [biqʿâ](../../strongs/h/h1237.md), the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) caused him to [nuwach](../../strongs/h/h5117.md): so didst thou [nāhaḡ](../../strongs/h/h5090.md) thy ['am](../../strongs/h/h5971.md), to make thyself a [tip̄'ārâ](../../strongs/h/h8597.md) [shem](../../strongs/h/h8034.md).

<a name="isaiah_63_15"></a>Isaiah 63:15

[nabat](../../strongs/h/h5027.md) from [shamayim](../../strongs/h/h8064.md), and [ra'ah](../../strongs/h/h7200.md) from the [zᵊḇûl](../../strongs/h/h2073.md) of thy [qodesh](../../strongs/h/h6944.md) and of thy [tip̄'ārâ](../../strongs/h/h8597.md): where is thy [qin'â](../../strongs/h/h7068.md) and thy [gᵊḇûrâ](../../strongs/h/h1369.md), the [hāmôn](../../strongs/h/h1995.md) of thy [me'ah](../../strongs/h/h4578.md) and of thy [raḥam](../../strongs/h/h7356.md) toward me? are they ['āp̄aq](../../strongs/h/h662.md)?

<a name="isaiah_63_16"></a>Isaiah 63:16

[kî](../../strongs/h/h3588.md) thou art our ['ab](../../strongs/h/h1.md), though ['Abraham](../../strongs/h/h85.md) be [lō'](../../strongs/h/h3808.md) of us, and [Yisra'el](../../strongs/h/h3478.md) [nāḵar](../../strongs/h/h5234.md) us not: thou, [Yĕhovah](../../strongs/h/h3068.md), art our ['ab](../../strongs/h/h1.md), our [gā'al](../../strongs/h/h1350.md); thy [shem](../../strongs/h/h8034.md) is from ['owlam](../../strongs/h/h5769.md).

<a name="isaiah_63_17"></a>Isaiah 63:17

[Yĕhovah](../../strongs/h/h3068.md), why hast thou made us to [tāʿâ](../../strongs/h/h8582.md) from thy [derek](../../strongs/h/h1870.md), and [qāšaḥ](../../strongs/h/h7188.md) our [leb](../../strongs/h/h3820.md) from thy [yir'ah](../../strongs/h/h3374.md)? [shuwb](../../strongs/h/h7725.md) for thy ['ebed](../../strongs/h/h5650.md) sake, the [shebet](../../strongs/h/h7626.md) of thine [nachalah](../../strongs/h/h5159.md).

<a name="isaiah_63_18"></a>Isaiah 63:18

The ['am](../../strongs/h/h5971.md) of thy [qodesh](../../strongs/h/h6944.md) have [yarash](../../strongs/h/h3423.md) it but a little while: our [tsar](../../strongs/h/h6862.md) have [bûs](../../strongs/h/h947.md) thy [miqdash](../../strongs/h/h4720.md).

<a name="isaiah_63_19"></a>Isaiah 63:19

We are thine: thou never [mashal](../../strongs/h/h4910.md) over them; they were not [qara'](../../strongs/h/h7121.md) by thy [shem](../../strongs/h/h8034.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 62](isaiah_62.md) - [Isaiah 64](isaiah_64.md)