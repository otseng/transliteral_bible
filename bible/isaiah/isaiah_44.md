# [Isaiah 44](https://www.blueletterbible.org/kjv/isa/44/1/s_723001)

<a name="isaiah_44_1"></a>Isaiah 44:1

Yet now [shama'](../../strongs/h/h8085.md), O [Ya'aqob](../../strongs/h/h3290.md) my ['ebed](../../strongs/h/h5650.md); and [Yisra'el](../../strongs/h/h3478.md), whom I have [bāḥar](../../strongs/h/h977.md):

<a name="isaiah_44_2"></a>Isaiah 44:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) that ['asah](../../strongs/h/h6213.md) thee, and [yāṣar](../../strongs/h/h3335.md) thee from the [beten](../../strongs/h/h990.md), which will [ʿāzar](../../strongs/h/h5826.md) thee; [yare'](../../strongs/h/h3372.md) not, O [Ya'aqob](../../strongs/h/h3290.md), my ['ebed](../../strongs/h/h5650.md); and thou, [Yᵊšurûn](../../strongs/h/h3484.md), whom I have [bāḥar](../../strongs/h/h977.md).

<a name="isaiah_44_3"></a>Isaiah 44:3

For I will [yāṣaq](../../strongs/h/h3332.md) [mayim](../../strongs/h/h4325.md) upon him that is [ṣāmē'](../../strongs/h/h6771.md), and [nāzal](../../strongs/h/h5140.md) upon the [yabāšâ](../../strongs/h/h3004.md): I will [yāṣaq](../../strongs/h/h3332.md) my [ruwach](../../strongs/h/h7307.md) upon thy [zera'](../../strongs/h/h2233.md), and my [bĕrakah](../../strongs/h/h1293.md) upon thine [ṣe'ĕṣā'îm](../../strongs/h/h6631.md):

<a name="isaiah_44_4"></a>Isaiah 44:4

And they shall [ṣāmaḥ](../../strongs/h/h6779.md) as among the [chatsiyr](../../strongs/h/h2682.md), as [ʿărāḇâ](../../strongs/h/h6155.md) by the [mayim](../../strongs/h/h4325.md) [Yāḇāl](../../strongs/h/h2988.md).

<a name="isaiah_44_5"></a>Isaiah 44:5

One shall ['āmar](../../strongs/h/h559.md), I am [Yĕhovah](../../strongs/h/h3068.md); and another shall [qara'](../../strongs/h/h7121.md) himself by the [shem](../../strongs/h/h8034.md) of [Ya'aqob](../../strongs/h/h3290.md); and another shall [kāṯaḇ](../../strongs/h/h3789.md) with his [yad](../../strongs/h/h3027.md) unto [Yĕhovah](../../strongs/h/h3068.md), and [kānâ](../../strongs/h/h3655.md) himself by the [shem](../../strongs/h/h8034.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_44_6"></a>Isaiah 44:6

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), and his [gā'al](../../strongs/h/h1350.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); I am the [ri'šôn](../../strongs/h/h7223.md), and I am the ['aḥărôn](../../strongs/h/h314.md); and beside me there is no ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_44_7"></a>Isaiah 44:7

And who, as I, shall [qara'](../../strongs/h/h7121.md), and shall [nāḡaḏ](../../strongs/h/h5046.md) it, and set it in ['arak](../../strongs/h/h6186.md) for me, since I [śûm](../../strongs/h/h7760.md) the ['owlam](../../strongs/h/h5769.md) ['am](../../strongs/h/h5971.md)? and the things that are ['āṯâ](../../strongs/h/h857.md), and shall [bow'](../../strongs/h/h935.md), let them [nāḡaḏ](../../strongs/h/h5046.md) unto them.

<a name="isaiah_44_8"></a>Isaiah 44:8

[pachad](../../strongs/h/h6342.md) ye not, neither be [rāhâ](../../strongs/h/h7297.md) [rāhâ](../../strongs/h/h7297.md): have not I [shama'](../../strongs/h/h8085.md) thee from that time, and have [nāḡaḏ](../../strongs/h/h5046.md) it? ye are even my ['ed](../../strongs/h/h5707.md). Is there an ['ĕlvôha](../../strongs/h/h433.md) beside me? yea, there is no [tsuwr](../../strongs/h/h6697.md); I [yada'](../../strongs/h/h3045.md) not any.

<a name="isaiah_44_9"></a>Isaiah 44:9

They that [yāṣar](../../strongs/h/h3335.md) a [pecel](../../strongs/h/h6459.md) are all of them [tohuw](../../strongs/h/h8414.md); and their [chamad](../../strongs/h/h2530.md) shall not [yāʿal](../../strongs/h/h3276.md); and they are their own ['ed](../../strongs/h/h5707.md); they [ra'ah](../../strongs/h/h7200.md) not, nor [yada'](../../strongs/h/h3045.md); that they may be [buwsh](../../strongs/h/h954.md).

<a name="isaiah_44_10"></a>Isaiah 44:10

Who hath [yāṣar](../../strongs/h/h3335.md) an ['el](../../strongs/h/h410.md), or [nacak](../../strongs/h/h5258.md) a [pecel](../../strongs/h/h6459.md) that is [yāʿal](../../strongs/h/h3276.md) for [biltî](../../strongs/h/h1115.md)?

<a name="isaiah_44_11"></a>Isaiah 44:11

Behold, all his [ḥāḇēr](../../strongs/h/h2270.md) shall be [buwsh](../../strongs/h/h954.md): and the [ḥārāš](../../strongs/h/h2796.md), they are of ['adam](../../strongs/h/h120.md): let them all be [qāḇaṣ](../../strongs/h/h6908.md), let them ['amad](../../strongs/h/h5975.md); yet they shall [pachad](../../strongs/h/h6342.md), and they shall be [buwsh](../../strongs/h/h954.md) together.

<a name="isaiah_44_12"></a>Isaiah 44:12

The [barzel](../../strongs/h/h1270.md) [ḥārāš](../../strongs/h/h2796.md) with the [maʿăṣāḏ](../../strongs/h/h4621.md) both [pa'al](../../strongs/h/h6466.md) in the [peḥām](../../strongs/h/h6352.md), and [yāṣar](../../strongs/h/h3335.md) it with [maqqāḇâ](../../strongs/h/h4717.md), and [pa'al](../../strongs/h/h6466.md) it with the [koach](../../strongs/h/h3581.md) of his [zerowa'](../../strongs/h/h2220.md): yea, he is [rāʿēḇ](../../strongs/h/h7457.md), and his [koach](../../strongs/h/h3581.md) ['în](../../strongs/h/h369.md): he [šāṯâ](../../strongs/h/h8354.md) no [mayim](../../strongs/h/h4325.md), and is [yāʿap̄](../../strongs/h/h3286.md).

<a name="isaiah_44_13"></a>Isaiah 44:13

The [ḥārāš](../../strongs/h/h2796.md) ['ets](../../strongs/h/h6086.md) [natah](../../strongs/h/h5186.md) his [qāv](../../strongs/h/h6957.md); he [tā'ar](../../strongs/h/h8388.md) with a [śereḏ](../../strongs/h/h8279.md); he ['asah](../../strongs/h/h6213.md) with [maqṣuʿâ](../../strongs/h/h4741.md), and he [tā'ar](../../strongs/h/h8388.md) with the [mᵊḥûḡâ](../../strongs/h/h4230.md), and ['asah](../../strongs/h/h6213.md) it after the [taḇnîṯ](../../strongs/h/h8403.md) of an ['iysh](../../strongs/h/h376.md), according to the [tip̄'ārâ](../../strongs/h/h8597.md) of an ['adam](../../strongs/h/h120.md); that it may [yashab](../../strongs/h/h3427.md) in the [bayith](../../strongs/h/h1004.md).

<a name="isaiah_44_14"></a>Isaiah 44:14

He [karath](../../strongs/h/h3772.md) ['erez](../../strongs/h/h730.md), and [laqach](../../strongs/h/h3947.md) the [tirzâ](../../strongs/h/h8645.md) and the ['allôn](../../strongs/h/h437.md), which he ['amats](../../strongs/h/h553.md) for himself among the ['ets](../../strongs/h/h6086.md) of the [yaʿar](../../strongs/h/h3293.md): he [nāṭaʿ](../../strongs/h/h5193.md) an ['ōren](../../strongs/h/h766.md), and the [gešem](../../strongs/h/h1653.md) doth [gāḏal](../../strongs/h/h1431.md) it.

<a name="isaiah_44_15"></a>Isaiah 44:15

Then shall it be for an ['adam](../../strongs/h/h120.md) to [bāʿar](../../strongs/h/h1197.md): for he will [laqach](../../strongs/h/h3947.md) thereof, and [ḥāmam](../../strongs/h/h2552.md) himself; yea, he [nāśaq](../../strongs/h/h5400.md) it, and ['āp̄â](../../strongs/h/h644.md) [lechem](../../strongs/h/h3899.md); yea, he maketh an ['el](../../strongs/h/h410.md), and [shachah](../../strongs/h/h7812.md) it; he ['asah](../../strongs/h/h6213.md) it a [pecel](../../strongs/h/h6459.md), and [sāḡaḏ](../../strongs/h/h5456.md) thereto.

<a name="isaiah_44_16"></a>Isaiah 44:16

He [śārap̄](../../strongs/h/h8313.md) [ḥēṣî](../../strongs/h/h2677.md) thereof in the ['esh](../../strongs/h/h784.md); with part thereof he ['akal](../../strongs/h/h398.md) [basar](../../strongs/h/h1320.md); he [ṣālâ](../../strongs/h/h6740.md) [ṣālî](../../strongs/h/h6748.md), and is [sāׂbaʿ](../../strongs/h/h7646.md): yea, he [ḥāmam](../../strongs/h/h2552.md) himself, and ['āmar](../../strongs/h/h559.md), [he'āḥ](../../strongs/h/h1889.md), I am [ḥāmam](../../strongs/h/h2552.md), I have [ra'ah](../../strongs/h/h7200.md) the ['ûr](../../strongs/h/h217.md):

<a name="isaiah_44_17"></a>Isaiah 44:17

And the [šᵊ'ērîṯ](../../strongs/h/h7611.md) thereof he ['asah](../../strongs/h/h6213.md) an ['el](../../strongs/h/h410.md), even his [pecel](../../strongs/h/h6459.md): he [sāḡaḏ](../../strongs/h/h5456.md) unto it, and [shachah](../../strongs/h/h7812.md) it, and [palal](../../strongs/h/h6419.md) unto it, and ['āmar](../../strongs/h/h559.md), [natsal](../../strongs/h/h5337.md) me; for thou art my ['el](../../strongs/h/h410.md).

<a name="isaiah_44_18"></a>Isaiah 44:18

They have not [yada'](../../strongs/h/h3045.md) nor [bîn](../../strongs/h/h995.md): for he hath [ṭûaḥ](../../strongs/h/h2902.md) their ['ayin](../../strongs/h/h5869.md), that they cannot [ra'ah](../../strongs/h/h7200.md); and their [libbah](../../strongs/h/h3826.md), that they cannot [sakal](../../strongs/h/h7919.md).

<a name="isaiah_44_19"></a>Isaiah 44:19

And none [shuwb](../../strongs/h/h7725.md) in his [leb](../../strongs/h/h3820.md), neither is there [da'ath](../../strongs/h/h1847.md) nor [tāḇûn](../../strongs/h/h8394.md) to ['āmar](../../strongs/h/h559.md), I have [śārap̄](../../strongs/h/h8313.md) part of it in the ['esh](../../strongs/h/h784.md); yea, also I have ['āp̄â](../../strongs/h/h644.md) [lechem](../../strongs/h/h3899.md) upon the [gechel](../../strongs/h/h1513.md) thereof; I have [ṣālâ](../../strongs/h/h6740.md) [basar](../../strongs/h/h1320.md), and ['akal](../../strongs/h/h398.md) it: and shall I make the [yeṯer](../../strongs/h/h3499.md) thereof a [tôʿēḇâ](../../strongs/h/h8441.md)? shall I [sāḡaḏ](../../strongs/h/h5456.md) to the [bûl](../../strongs/h/h944.md) of an ['ets](../../strongs/h/h6086.md)?

<a name="isaiah_44_20"></a>Isaiah 44:20

He [ra'ah](../../strongs/h/h7462.md) on ['ēp̄er](../../strongs/h/h665.md): a [hāṯal](../../strongs/h/h2048.md) [leb](../../strongs/h/h3820.md) hath [natah](../../strongs/h/h5186.md) him, that he cannot [natsal](../../strongs/h/h5337.md) his [nephesh](../../strongs/h/h5315.md), nor say, Is there not a [sheqer](../../strongs/h/h8267.md) in my [yamiyn](../../strongs/h/h3225.md)?

<a name="isaiah_44_21"></a>Isaiah 44:21

[zakar](../../strongs/h/h2142.md) these, O [Ya'aqob](../../strongs/h/h3290.md) and [Yisra'el](../../strongs/h/h3478.md); for thou art my ['ebed](../../strongs/h/h5650.md): I have [yāṣar](../../strongs/h/h3335.md) thee; thou art my ['ebed](../../strongs/h/h5650.md): [Yisra'el](../../strongs/h/h3478.md), thou shalt not be [nāšâ](../../strongs/h/h5382.md) of me.

<a name="isaiah_44_22"></a>Isaiah 44:22

I have [māḥâ](../../strongs/h/h4229.md), as an ['ab](../../strongs/h/h5645.md), thy [pesha'](../../strongs/h/h6588.md), and, as a [ʿānān](../../strongs/h/h6051.md), thy [chatta'ath](../../strongs/h/h2403.md): [shuwb](../../strongs/h/h7725.md) unto me; for I have [gā'al](../../strongs/h/h1350.md) thee.

<a name="isaiah_44_23"></a>Isaiah 44:23

[ranan](../../strongs/h/h7442.md), O ye [shamayim](../../strongs/h/h8064.md); for [Yĕhovah](../../strongs/h/h3068.md) hath ['asah](../../strongs/h/h6213.md) it: [rûaʿ](../../strongs/h/h7321.md), ye [taḥtî](../../strongs/h/h8482.md) of the ['erets](../../strongs/h/h776.md): [pāṣaḥ](../../strongs/h/h6476.md) into [rinnah](../../strongs/h/h7440.md), ye [har](../../strongs/h/h2022.md), O [yaʿar](../../strongs/h/h3293.md), and every ['ets](../../strongs/h/h6086.md) therein: for [Yĕhovah](../../strongs/h/h3068.md) hath [gā'al](../../strongs/h/h1350.md) [Ya'aqob](../../strongs/h/h3290.md), and [pā'ar](../../strongs/h/h6286.md) himself in [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_44_24"></a>Isaiah 44:24

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), thy [gā'al](../../strongs/h/h1350.md), and he that [yāṣar](../../strongs/h/h3335.md) thee from the [beten](../../strongs/h/h990.md), I am [Yĕhovah](../../strongs/h/h3068.md) that ['asah](../../strongs/h/h6213.md) all things; that [natah](../../strongs/h/h5186.md) the [shamayim](../../strongs/h/h8064.md) alone; that [rāqaʿ](../../strongs/h/h7554.md) the ['erets](../../strongs/h/h776.md) by myself;

<a name="isaiah_44_25"></a>Isaiah 44:25

That [pārar](../../strongs/h/h6565.md) the ['ôṯ](../../strongs/h/h226.md) of the [baḏ](../../strongs/h/h907.md), and [qāsam](../../strongs/h/h7080.md) [halal](../../strongs/h/h1984.md); that [shuwb](../../strongs/h/h7725.md) [ḥāḵām](../../strongs/h/h2450.md) ['āḥôr](../../strongs/h/h268.md), and maketh their [da'ath](../../strongs/h/h1847.md) [sāḵal](../../strongs/h/h5528.md);

<a name="isaiah_44_26"></a>Isaiah 44:26

That [quwm](../../strongs/h/h6965.md) the [dabar](../../strongs/h/h1697.md) of his ['ebed](../../strongs/h/h5650.md), and [shalam](../../strongs/h/h7999.md) the ['etsah](../../strongs/h/h6098.md) of his [mal'ak](../../strongs/h/h4397.md); that ['āmar](../../strongs/h/h559.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), Thou shalt be [yashab](../../strongs/h/h3427.md); and to the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), Ye shall be [bānâ](../../strongs/h/h1129.md), and I will [quwm](../../strongs/h/h6965.md) the [chorbah](../../strongs/h/h2723.md) thereof:

<a name="isaiah_44_27"></a>Isaiah 44:27

That ['āmar](../../strongs/h/h559.md) to the [ṣûlâ](../../strongs/h/h6683.md), Be [ḥāraḇ](../../strongs/h/h2717.md), and I will [yāḇēš](../../strongs/h/h3001.md) thy [nāhār](../../strongs/h/h5104.md):

<a name="isaiah_44_28"></a>Isaiah 44:28

That ['āmar](../../strongs/h/h559.md) of [Kôreš](../../strongs/h/h3566.md), He is my [ra'ah](../../strongs/h/h7462.md), and shall [shalam](../../strongs/h/h7999.md) all my [chephets](../../strongs/h/h2656.md): even ['āmar](../../strongs/h/h559.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), Thou shalt be [bānâ](../../strongs/h/h1129.md); and to the [heykal](../../strongs/h/h1964.md), Thy [yacad](../../strongs/h/h3245.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 43](isaiah_43.md) - [Isaiah 45](isaiah_45.md)