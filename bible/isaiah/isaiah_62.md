# [Isaiah 62](https://www.blueletterbible.org/kjv/isa/62/1/s_741001)

<a name="isaiah_62_1"></a>Isaiah 62:1

For [Tsiyown](../../strongs/h/h6726.md) sake will I not [ḥāšâ](../../strongs/h/h2814.md), and for [Yĕruwshalaim](../../strongs/h/h3389.md) sake I will not [šāqaṭ](../../strongs/h/h8252.md), until the [tsedeq](../../strongs/h/h6664.md) thereof [yāṣā'](../../strongs/h/h3318.md) as [nogahh](../../strongs/h/h5051.md), and the [yĕshuw'ah](../../strongs/h/h3444.md) thereof as a [lapîḏ](../../strongs/h/h3940.md) that [bāʿar](../../strongs/h/h1197.md).

<a name="isaiah_62_2"></a>Isaiah 62:2

And the [gowy](../../strongs/h/h1471.md) shall [ra'ah](../../strongs/h/h7200.md) thy [tsedeq](../../strongs/h/h6664.md), and all [melek](../../strongs/h/h4428.md) thy [kabowd](../../strongs/h/h3519.md): and thou shalt be [qara'](../../strongs/h/h7121.md) by a [ḥāḏāš](../../strongs/h/h2319.md) [nāqaḇ](../../strongs/h/h5344.md), which the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) shall [shem](../../strongs/h/h8034.md).

<a name="isaiah_62_3"></a>Isaiah 62:3

Thou shalt also be an [ʿăṭārâ](../../strongs/h/h5850.md) of [tip̄'ārâ](../../strongs/h/h8597.md) in the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md), and a [mᵊlûḵâ](../../strongs/h/h4410.md) [ṣānîp̄](../../strongs/h/h6797.md) [ṣānîp̄](../../strongs/h/h6797.md) in the [kaph](../../strongs/h/h3709.md) of thy ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_62_4"></a>Isaiah 62:4

Thou shalt no more be ['āmar](../../strongs/h/h559.md) ['azab](../../strongs/h/h5800.md); neither shall thy ['erets](../../strongs/h/h776.md) any more be ['āmar](../../strongs/h/h559.md) [šᵊmāmâ](../../strongs/h/h8077.md): but thou shalt be [qara'](../../strongs/h/h7121.md) [ḥep̄ṣî-ḇâ](../../strongs/h/h2657.md), and thy ['erets](../../strongs/h/h776.md) [bāʿal](../../strongs/h/h1166.md): for [Yĕhovah](../../strongs/h/h3068.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) in thee, and thy ['erets](../../strongs/h/h776.md) shall be [bāʿal](../../strongs/h/h1166.md).

<a name="isaiah_62_5"></a>Isaiah 62:5

For as a [bāḥûr](../../strongs/h/h970.md) [bāʿal](../../strongs/h/h1166.md) a [bᵊṯûlâ](../../strongs/h/h1330.md), so shall thy [ben](../../strongs/h/h1121.md) [bāʿal](../../strongs/h/h1166.md) thee: and as the [ḥāṯān](../../strongs/h/h2860.md) [māśôś](../../strongs/h/h4885.md) over the [kallâ](../../strongs/h/h3618.md), so shall thy ['Elohiym](../../strongs/h/h430.md) [śûś](../../strongs/h/h7797.md) over thee.

<a name="isaiah_62_6"></a>Isaiah 62:6

I have [paqad](../../strongs/h/h6485.md) [shamar](../../strongs/h/h8104.md) upon thy [ḥômâ](../../strongs/h/h2346.md), O [Yĕruwshalaim](../../strongs/h/h3389.md), which shall never [ḥāšâ](../../strongs/h/h2814.md) [yowm](../../strongs/h/h3117.md) nor [layil](../../strongs/h/h3915.md): ye that make [zakar](../../strongs/h/h2142.md) of [Yĕhovah](../../strongs/h/h3068.md), keep not [dᵊmî](../../strongs/h/h1824.md),

<a name="isaiah_62_7"></a>Isaiah 62:7

And [nathan](../../strongs/h/h5414.md) him no [dᵊmî](../../strongs/h/h1824.md), till he [kuwn](../../strongs/h/h3559.md), and till he make [Yĕruwshalaim](../../strongs/h/h3389.md) a [tehillah](../../strongs/h/h8416.md) in the ['erets](../../strongs/h/h776.md).

<a name="isaiah_62_8"></a>Isaiah 62:8

[Yĕhovah](../../strongs/h/h3068.md) hath [shaba'](../../strongs/h/h7650.md) by his [yamiyn](../../strongs/h/h3225.md), and by the [zerowa'](../../strongs/h/h2220.md) of his ['oz](../../strongs/h/h5797.md), Surely I will no more [nathan](../../strongs/h/h5414.md) thy [dagan](../../strongs/h/h1715.md) to be [ma'akal](../../strongs/h/h3978.md) for thine ['oyeb](../../strongs/h/h341.md); and the [ben](../../strongs/h/h1121.md) of the [nēḵār](../../strongs/h/h5236.md) shall not [šāṯâ](../../strongs/h/h8354.md) thy [tiyrowsh](../../strongs/h/h8492.md), for the which thou hast [yaga'](../../strongs/h/h3021.md):

<a name="isaiah_62_9"></a>Isaiah 62:9

But they that have ['āsap̄](../../strongs/h/h622.md) it shall ['akal](../../strongs/h/h398.md) it, and [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md); and they that have [qāḇaṣ](../../strongs/h/h6908.md) it shall [šāṯâ](../../strongs/h/h8354.md) it in the [ḥāṣēr](../../strongs/h/h2691.md) of my [qodesh](../../strongs/h/h6944.md).

<a name="isaiah_62_10"></a>Isaiah 62:10

['abar](../../strongs/h/h5674.md), ['abar](../../strongs/h/h5674.md) the [sha'ar](../../strongs/h/h8179.md); [panah](../../strongs/h/h6437.md) ye the [derek](../../strongs/h/h1870.md) of the ['am](../../strongs/h/h5971.md); [sālal](../../strongs/h/h5549.md), [sālal](../../strongs/h/h5549.md) the [mĕcillah](../../strongs/h/h4546.md); [sāqal](../../strongs/h/h5619.md) the ['eben](../../strongs/h/h68.md); [ruwm](../../strongs/h/h7311.md) a [nēs](../../strongs/h/h5251.md) for the ['am](../../strongs/h/h5971.md).

<a name="isaiah_62_11"></a>Isaiah 62:11

Behold, [Yĕhovah](../../strongs/h/h3068.md) hath [shama'](../../strongs/h/h8085.md) unto the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md), ['āmar](../../strongs/h/h559.md) ye to the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md), Behold, thy [yesha'](../../strongs/h/h3468.md) [bow'](../../strongs/h/h935.md); behold, his [śāḵār](../../strongs/h/h7939.md) is with him, and his [pe'ullah](../../strongs/h/h6468.md) before him.

<a name="isaiah_62_12"></a>Isaiah 62:12

And they shall [qara'](../../strongs/h/h7121.md) them, The [qodesh](../../strongs/h/h6944.md) ['am](../../strongs/h/h5971.md), The [gā'al](../../strongs/h/h1350.md) of [Yĕhovah](../../strongs/h/h3068.md): and thou shalt be called, [darash](../../strongs/h/h1875.md), an [ʿîr](../../strongs/h/h5892.md) not ['azab](../../strongs/h/h5800.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 61](isaiah_61.md) - [Isaiah 63](isaiah_63.md)