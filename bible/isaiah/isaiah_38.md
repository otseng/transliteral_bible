# [Isaiah 38](https://www.blueletterbible.org/kjv/isa/38/1/s_717001)

<a name="isaiah_38_1"></a>Isaiah 38:1

In those [yowm](../../strongs/h/h3117.md) was [Ḥizqîyâ](../../strongs/h/h2396.md) [ḥālâ](../../strongs/h/h2470.md) unto [muwth](../../strongs/h/h4191.md). And [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [nāḇî'](../../strongs/h/h5030.md) the [ben](../../strongs/h/h1121.md) of ['Āmôṣ](../../strongs/h/h531.md) [bow'](../../strongs/h/h935.md) unto him, and ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Set thine [bayith](../../strongs/h/h1004.md) in [tsavah](../../strongs/h/h6680.md): for thou shalt [muwth](../../strongs/h/h4191.md), and not [ḥāyâ](../../strongs/h/h2421.md).

<a name="isaiah_38_2"></a>Isaiah 38:2

Then [Ḥizqîyâ](../../strongs/h/h2396.md) [cabab](../../strongs/h/h5437.md) his [paniym](../../strongs/h/h6440.md) toward the [qîr](../../strongs/h/h7023.md), and [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md),

<a name="isaiah_38_3"></a>Isaiah 38:3

And ['āmar](../../strongs/h/h559.md), [zakar](../../strongs/h/h2142.md) now, [Yĕhovah](../../strongs/h/h3068.md), ['ānnā'](../../strongs/h/h577.md), how I have [halak](../../strongs/h/h1980.md) before thee in ['emeth](../../strongs/h/h571.md) and with a [šālēm](../../strongs/h/h8003.md) [leb](../../strongs/h/h3820.md), and have ['asah](../../strongs/h/h6213.md) that which is [towb](../../strongs/h/h2896.md) in thy ['ayin](../../strongs/h/h5869.md). And [Ḥizqîyâ](../../strongs/h/h2396.md) [bāḵâ](../../strongs/h/h1058.md) [bĕkiy](../../strongs/h/h1065.md) [gadowl](../../strongs/h/h1419.md).

<a name="isaiah_38_4"></a>Isaiah 38:4

Then came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) to [Yᵊšaʿyâ](../../strongs/h/h3470.md), saying,

<a name="isaiah_38_5"></a>Isaiah 38:5

[halak](../../strongs/h/h1980.md), and ['āmar](../../strongs/h/h559.md) to [Ḥizqîyâ](../../strongs/h/h2396.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Dāviḏ](../../strongs/h/h1732.md) thy ['ab](../../strongs/h/h1.md), I have [shama'](../../strongs/h/h8085.md) thy [tĕphillah](../../strongs/h/h8605.md), I have [ra'ah](../../strongs/h/h7200.md) thy [dim'ah](../../strongs/h/h1832.md): behold, I will [yāsap̄](../../strongs/h/h3254.md) unto thy [yowm](../../strongs/h/h3117.md) fifteen [šānâ](../../strongs/h/h8141.md).

<a name="isaiah_38_6"></a>Isaiah 38:6

And I will [natsal](../../strongs/h/h5337.md) thee and this [ʿîr](../../strongs/h/h5892.md) out of the [kaph](../../strongs/h/h3709.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md): and I will [gānan](../../strongs/h/h1598.md) this [ʿîr](../../strongs/h/h5892.md).

<a name="isaiah_38_7"></a>Isaiah 38:7

And this shall be an ['ôṯ](../../strongs/h/h226.md) unto thee from [Yĕhovah](../../strongs/h/h3068.md), that [Yĕhovah](../../strongs/h/h3068.md) will ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md) that he hath [dabar](../../strongs/h/h1696.md);

<a name="isaiah_38_8"></a>Isaiah 38:8

Behold, I will [shuwb](../../strongs/h/h7725.md) the [ṣēl](../../strongs/h/h6738.md) of the [maʿălâ](../../strongs/h/h4609.md), which is [yarad](../../strongs/h/h3381.md) in the [šemeš](../../strongs/h/h8121.md) [maʿălâ](../../strongs/h/h4609.md) of ['Āḥāz](../../strongs/h/h271.md), ten [maʿălâ](../../strongs/h/h4609.md) backward. So the [šemeš](../../strongs/h/h8121.md) [shuwb](../../strongs/h/h7725.md) ten [maʿălâ](../../strongs/h/h4609.md), by which [maʿălâ](../../strongs/h/h4609.md) it was [yarad](../../strongs/h/h3381.md).

<a name="isaiah_38_9"></a>Isaiah 38:9

The [miḵtāḇ](../../strongs/h/h4385.md) of [Ḥizqîyâ](../../strongs/h/h2396.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), when he had been [ḥālâ](../../strongs/h/h2470.md), and was [ḥāyâ](../../strongs/h/h2421.md) of his [ḥŏlî](../../strongs/h/h2483.md):

<a name="isaiah_38_10"></a>Isaiah 38:10

I ['āmar](../../strongs/h/h559.md) in the [dᵊmî](../../strongs/h/h1824.md) of my [yowm](../../strongs/h/h3117.md), I shall [yālaḵ](../../strongs/h/h3212.md) to the [sha'ar](../../strongs/h/h8179.md) of the [shĕ'owl](../../strongs/h/h7585.md): I am [paqad](../../strongs/h/h6485.md) of the [yeṯer](../../strongs/h/h3499.md) of my [šānâ](../../strongs/h/h8141.md).

<a name="isaiah_38_11"></a>Isaiah 38:11

I ['āmar](../../strongs/h/h559.md), I shall not [ra'ah](../../strongs/h/h7200.md) [Yahh](../../strongs/h/h3050.md), even [Yahh](../../strongs/h/h3050.md), in the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md): I shall [nabat](../../strongs/h/h5027.md) ['adam](../../strongs/h/h120.md) no more with the [yashab](../../strongs/h/h3427.md) of the [ḥeḏel](../../strongs/h/h2309.md).

<a name="isaiah_38_12"></a>Isaiah 38:12

[dôr](../../strongs/h/h1755.md) is [nāsaʿ](../../strongs/h/h5265.md), and is [gālâ](../../strongs/h/h1540.md) from me as a [rōʿê](../../strongs/h/h7473.md) ['ohel](../../strongs/h/h168.md): I have [qāp̄aḏ](../../strongs/h/h7088.md) like an ['āraḡ](../../strongs/h/h707.md) my [chay](../../strongs/h/h2416.md): he will [batsa'](../../strongs/h/h1214.md) with [dallâ](../../strongs/h/h1803.md): from [yowm](../../strongs/h/h3117.md) even to [layil](../../strongs/h/h3915.md) wilt thou make a [shalam](../../strongs/h/h7999.md) of me.

<a name="isaiah_38_13"></a>Isaiah 38:13

I [šāvâ](../../strongs/h/h7737.md) till [boqer](../../strongs/h/h1242.md), that, as an ['ariy](../../strongs/h/h738.md), so will he [shabar](../../strongs/h/h7665.md) all my ['etsem](../../strongs/h/h6106.md): from [yowm](../../strongs/h/h3117.md) even to [layil](../../strongs/h/h3915.md) wilt thou make a [shalam](../../strongs/h/h7999.md) of me.

<a name="isaiah_38_14"></a>Isaiah 38:14

Like a [sûs](../../strongs/h/h5483.md) or a [ʿāḡûr](../../strongs/h/h5693.md), so did I [ṣāp̄ap̄](../../strongs/h/h6850.md): I did [hagah](../../strongs/h/h1897.md) as a [yônâ](../../strongs/h/h3123.md): mine ['ayin](../../strongs/h/h5869.md) [dālal](../../strongs/h/h1809.md) with [marowm](../../strongs/h/h4791.md): [Yĕhovah](../../strongs/h/h3068.md), I am [ʿāšqâ](../../strongs/h/h6234.md); [ʿāraḇ](../../strongs/h/h6148.md) for me.

<a name="isaiah_38_15"></a>Isaiah 38:15

What shall I [dabar](../../strongs/h/h1696.md)? he hath both ['āmar](../../strongs/h/h559.md) unto me, and himself hath ['asah](../../strongs/h/h6213.md) it: I shall [dāḏâ](../../strongs/h/h1718.md) all my [šānâ](../../strongs/h/h8141.md) in the [mar](../../strongs/h/h4751.md) of my [nephesh](../../strongs/h/h5315.md).

<a name="isaiah_38_16"></a>Isaiah 38:16

['adonay](../../strongs/h/h136.md), by these things men [ḥāyâ](../../strongs/h/h2421.md), and in all these things is the [chay](../../strongs/h/h2416.md) of my [ruwach](../../strongs/h/h7307.md): so wilt thou [ḥālam](../../strongs/h/h2492.md) me, and make me to [ḥāyâ](../../strongs/h/h2421.md).

<a name="isaiah_38_17"></a>Isaiah 38:17

Behold, for [shalowm](../../strongs/h/h7965.md) I had [mar](../../strongs/h/h4751.md) [mārar](../../strongs/h/h4843.md): but thou hast in [ḥāšaq](../../strongs/h/h2836.md) to my [nephesh](../../strongs/h/h5315.md) delivered it from the [shachath](../../strongs/h/h7845.md) of [bᵊlî](../../strongs/h/h1097.md): for thou hast [shalak](../../strongs/h/h7993.md) all my [ḥēṭĕ'](../../strongs/h/h2399.md) behind thy [gēv](../../strongs/h/h1460.md).

<a name="isaiah_38_18"></a>Isaiah 38:18

For the [shĕ'owl](../../strongs/h/h7585.md) cannot [yadah](../../strongs/h/h3034.md) thee, [maveth](../../strongs/h/h4194.md) can not [halal](../../strongs/h/h1984.md) thee: they that [yarad](../../strongs/h/h3381.md) into the [bowr](../../strongs/h/h953.md) cannot [śāḇar](../../strongs/h/h7663.md) for thy ['emeth](../../strongs/h/h571.md).

<a name="isaiah_38_19"></a>Isaiah 38:19

The [chay](../../strongs/h/h2416.md), the [chay](../../strongs/h/h2416.md), he shall [yadah](../../strongs/h/h3034.md) thee, as I do this [yowm](../../strongs/h/h3117.md): the ['ab](../../strongs/h/h1.md) to the [ben](../../strongs/h/h1121.md) shall make [yada'](../../strongs/h/h3045.md) thy ['emeth](../../strongs/h/h571.md).

<a name="isaiah_38_20"></a>Isaiah 38:20

[Yĕhovah](../../strongs/h/h3068.md) was ready to [yasha'](../../strongs/h/h3467.md) me: therefore we will [Nᵊḡînâ](../../strongs/h/h5058.md) to the [nāḡan](../../strongs/h/h5059.md) all the [yowm](../../strongs/h/h3117.md) of our [chay](../../strongs/h/h2416.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_38_21"></a>Isaiah 38:21

For [Yᵊšaʿyâ](../../strongs/h/h3470.md) had ['āmar](../../strongs/h/h559.md), Let them [nasa'](../../strongs/h/h5375.md) a [dᵊḇēlâ](../../strongs/h/h1690.md) of [tĕ'en](../../strongs/h/h8384.md), and [māraḥ](../../strongs/h/h4799.md) upon the [šiḥîn](../../strongs/h/h7822.md), and he shall [ḥāyâ](../../strongs/h/h2421.md).

<a name="isaiah_38_22"></a>Isaiah 38:22

[Ḥizqîyâ](../../strongs/h/h2396.md) also had ['āmar](../../strongs/h/h559.md), What is the ['ôṯ](../../strongs/h/h226.md) that I shall [ʿālâ](../../strongs/h/h5927.md) to the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md)?

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 37](isaiah_37.md) - [Isaiah 39](isaiah_39.md)