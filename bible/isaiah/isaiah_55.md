# [Isaiah 55](https://www.blueletterbible.org/kjv/isa/55/1/s_734001)

<a name="isaiah_55_1"></a>Isaiah 55:1

[hôy](../../strongs/h/h1945.md), every one that [ṣāmē'](../../strongs/h/h6771.md), [yālaḵ](../../strongs/h/h3212.md) ye to the [mayim](../../strongs/h/h4325.md), and he that hath no [keceph](../../strongs/h/h3701.md); [yālaḵ](../../strongs/h/h3212.md) ye, [šāḇar](../../strongs/h/h7666.md), and ['akal](../../strongs/h/h398.md); yea, [yālaḵ](../../strongs/h/h3212.md), [šāḇar](../../strongs/h/h7666.md) [yayin](../../strongs/h/h3196.md) and [chalab](../../strongs/h/h2461.md) without [keceph](../../strongs/h/h3701.md) and without [mᵊḥîr](../../strongs/h/h4242.md).

<a name="isaiah_55_2"></a>Isaiah 55:2

Wherefore do ye [šāqal](../../strongs/h/h8254.md) [keceph](../../strongs/h/h3701.md) for that which is not [lechem](../../strongs/h/h3899.md)? and your [yᵊḡîaʿ](../../strongs/h/h3018.md) for that which [śāḇʿâ](../../strongs/h/h7654.md) not? [shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) unto me, and ['akal](../../strongs/h/h398.md) ye that which is [towb](../../strongs/h/h2896.md), and let your [nephesh](../../strongs/h/h5315.md) [ʿānaḡ](../../strongs/h/h6026.md) itself in [dešen](../../strongs/h/h1880.md).

<a name="isaiah_55_3"></a>Isaiah 55:3

[natah](../../strongs/h/h5186.md) your ['ozen](../../strongs/h/h241.md), and [yālaḵ](../../strongs/h/h3212.md) unto me: [shama'](../../strongs/h/h8085.md), and your [nephesh](../../strongs/h/h5315.md) shall [ḥāyâ](../../strongs/h/h2421.md); and I will [karath](../../strongs/h/h3772.md) an ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md) with you, even the ['aman](../../strongs/h/h539.md) [checed](../../strongs/h/h2617.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="isaiah_55_4"></a>Isaiah 55:4

Behold, I have [nathan](../../strongs/h/h5414.md) him for an ['ed](../../strongs/h/h5707.md) to the [lĕom](../../strongs/h/h3816.md), a [nāḡîḏ](../../strongs/h/h5057.md) and [tsavah](../../strongs/h/h6680.md) to the [lĕom](../../strongs/h/h3816.md).

<a name="isaiah_55_5"></a>Isaiah 55:5

Behold, thou shalt [qara'](../../strongs/h/h7121.md) a [gowy](../../strongs/h/h1471.md) that thou [yada'](../../strongs/h/h3045.md) not, and [gowy](../../strongs/h/h1471.md) that [yada'](../../strongs/h/h3045.md) not thee shall [rûṣ](../../strongs/h/h7323.md) unto thee because of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and for the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md); for he hath [pā'ar](../../strongs/h/h6286.md) thee.

<a name="isaiah_55_6"></a>Isaiah 55:6

[darash](../../strongs/h/h1875.md) ye [Yĕhovah](../../strongs/h/h3068.md) while he may be [māṣā'](../../strongs/h/h4672.md), [qara'](../../strongs/h/h7121.md) ye upon him while he is [qarowb](../../strongs/h/h7138.md):

<a name="isaiah_55_7"></a>Isaiah 55:7

Let the [rasha'](../../strongs/h/h7563.md) ['azab](../../strongs/h/h5800.md) his [derek](../../strongs/h/h1870.md), and the ['aven](../../strongs/h/h205.md) ['iysh](../../strongs/h/h376.md) his [maḥăšāḇâ](../../strongs/h/h4284.md): and let him [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md), and he will have [racham](../../strongs/h/h7355.md) upon him; and to our ['Elohiym](../../strongs/h/h430.md), for he will [rabah](../../strongs/h/h7235.md) [sālaḥ](../../strongs/h/h5545.md).

<a name="isaiah_55_8"></a>Isaiah 55:8

For my [maḥăšāḇâ](../../strongs/h/h4284.md) are not your [maḥăšāḇâ](../../strongs/h/h4284.md), neither are your [derek](../../strongs/h/h1870.md) my [derek](../../strongs/h/h1870.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_55_9"></a>Isaiah 55:9

For as the [shamayim](../../strongs/h/h8064.md) are [gāḇah](../../strongs/h/h1361.md) than the ['erets](../../strongs/h/h776.md), so are my [derek](../../strongs/h/h1870.md) [gāḇah](../../strongs/h/h1361.md) than your [derek](../../strongs/h/h1870.md), and my [maḥăšāḇâ](../../strongs/h/h4284.md) than your [maḥăšāḇâ](../../strongs/h/h4284.md).

<a name="isaiah_55_10"></a>Isaiah 55:10

For as the [gešem](../../strongs/h/h1653.md) [yarad](../../strongs/h/h3381.md), and the [šeleḡ](../../strongs/h/h7950.md) from [shamayim](../../strongs/h/h8064.md), and [shuwb](../../strongs/h/h7725.md) not thither, but [rāvâ](../../strongs/h/h7301.md) the ['erets](../../strongs/h/h776.md), and maketh it [yalad](../../strongs/h/h3205.md) and [ṣāmaḥ](../../strongs/h/h6779.md), that it may give [zera'](../../strongs/h/h2233.md) to the [zāraʿ](../../strongs/h/h2232.md), and [lechem](../../strongs/h/h3899.md) to the ['akal](../../strongs/h/h398.md):

<a name="isaiah_55_11"></a>Isaiah 55:11

So shall my [dabar](../../strongs/h/h1697.md) be that [yāṣā'](../../strongs/h/h3318.md) out of my [peh](../../strongs/h/h6310.md): it shall not [shuwb](../../strongs/h/h7725.md) unto me [rêqām](../../strongs/h/h7387.md), but it shall ['asah](../../strongs/h/h6213.md) that which I [ḥāp̄ēṣ](../../strongs/h/h2654.md), and it shall [tsalach](../../strongs/h/h6743.md) in the thing whereto I [shalach](../../strongs/h/h7971.md) it.

<a name="isaiah_55_12"></a>Isaiah 55:12

For ye shall [yāṣā'](../../strongs/h/h3318.md) with [simchah](../../strongs/h/h8057.md), and be [yāḇal](../../strongs/h/h2986.md) with [shalowm](../../strongs/h/h7965.md): the [har](../../strongs/h/h2022.md) and the [giḇʿâ](../../strongs/h/h1389.md) shall [pāṣaḥ](../../strongs/h/h6476.md) before you into [rinnah](../../strongs/h/h7440.md), and all the ['ets](../../strongs/h/h6086.md) of the [sadeh](../../strongs/h/h7704.md) shall [māḥā'](../../strongs/h/h4222.md) their [kaph](../../strongs/h/h3709.md).

<a name="isaiah_55_13"></a>Isaiah 55:13

Instead of the [naʿăṣûṣ](../../strongs/h/h5285.md) shall [ʿālâ](../../strongs/h/h5927.md) the [bᵊrôš](../../strongs/h/h1265.md), and instead of the [sirpāḏ](../../strongs/h/h5636.md) shall [ʿālâ](../../strongs/h/h5927.md) the [hăḏas](../../strongs/h/h1918.md): and it shall be to [Yĕhovah](../../strongs/h/h3068.md) for a [shem](../../strongs/h/h8034.md), for an ['owlam](../../strongs/h/h5769.md) ['ôṯ](../../strongs/h/h226.md) that shall not be [karath](../../strongs/h/h3772.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 54](isaiah_54.md) - [Isaiah 56](isaiah_56.md)