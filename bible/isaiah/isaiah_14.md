# [Isaiah 14](https://www.blueletterbible.org/kjv/isa/14/1/s_693001)

<a name="isaiah_14_1"></a>Isaiah 14:1

For [Yĕhovah](../../strongs/h/h3068.md) will have [racham](../../strongs/h/h7355.md) on [Ya'aqob](../../strongs/h/h3290.md), and will yet [bāḥar](../../strongs/h/h977.md) [Yisra'el](../../strongs/h/h3478.md), and [yānaḥ](../../strongs/h/h3240.md) them in their own ['ăḏāmâ](../../strongs/h/h127.md): and the [ger](../../strongs/h/h1616.md) shall be [lāvâ](../../strongs/h/h3867.md) with them, and they shall [sāp̄aḥ](../../strongs/h/h5596.md) to the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md).

<a name="isaiah_14_2"></a>Isaiah 14:2

And the ['am](../../strongs/h/h5971.md) shall [laqach](../../strongs/h/h3947.md) them, and [bow'](../../strongs/h/h935.md) them to their [maqowm](../../strongs/h/h4725.md): and the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) shall [nāḥal](../../strongs/h/h5157.md) them in the ['ăḏāmâ](../../strongs/h/h127.md) of [Yĕhovah](../../strongs/h/h3068.md) for ['ebed](../../strongs/h/h5650.md) and [šip̄ḥâ](../../strongs/h/h8198.md): and they shall take them [šāḇâ](../../strongs/h/h7617.md), whose [šāḇâ](../../strongs/h/h7617.md) they were; and they shall [radah](../../strongs/h/h7287.md) over their [nāḡaś](../../strongs/h/h5065.md).

<a name="isaiah_14_3"></a>Isaiah 14:3

And it shall come to pass in the [yowm](../../strongs/h/h3117.md) that [Yĕhovah](../../strongs/h/h3068.md) shall give thee [nuwach](../../strongs/h/h5117.md) from thy [ʿōṣeḇ](../../strongs/h/h6090.md), and from thy [rōḡez](../../strongs/h/h7267.md), and from the [qāšê](../../strongs/h/h7186.md) [ʿăḇōḏâ](../../strongs/h/h5656.md) wherein thou wast made to ['abad](../../strongs/h/h5647.md),

<a name="isaiah_14_4"></a>Isaiah 14:4

That thou shalt [nasa'](../../strongs/h/h5375.md) this [māšāl](../../strongs/h/h4912.md) against the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and ['āmar](../../strongs/h/h559.md), How hath the [nāḡaś](../../strongs/h/h5065.md) [shabath](../../strongs/h/h7673.md)! the [maḏhēḇâ](../../strongs/h/h4062.md) [shabath](../../strongs/h/h7673.md)!

<a name="isaiah_14_5"></a>Isaiah 14:5

[Yĕhovah](../../strongs/h/h3068.md) hath [shabar](../../strongs/h/h7665.md) the [maṭṭê](../../strongs/h/h4294.md) of the [rasha'](../../strongs/h/h7563.md), and the [shebet](../../strongs/h/h7626.md) of the [mashal](../../strongs/h/h4910.md).

<a name="isaiah_14_6"></a>Isaiah 14:6

He who [nakah](../../strongs/h/h5221.md) the ['am](../../strongs/h/h5971.md) in ['ebrah](../../strongs/h/h5678.md) with a [biltî](../../strongs/h/h1115.md) [sārâ](../../strongs/h/h5627.md) [makâ](../../strongs/h/h4347.md), he that [radah](../../strongs/h/h7287.md) the [gowy](../../strongs/h/h1471.md) in ['aph](../../strongs/h/h639.md), is [murdāp̄](../../strongs/h/h4783.md), and [bᵊlî](../../strongs/h/h1097.md) [ḥāśaḵ](../../strongs/h/h2820.md).

<a name="isaiah_14_7"></a>Isaiah 14:7

The ['erets](../../strongs/h/h776.md) is at [nuwach](../../strongs/h/h5117.md), and is [šāqaṭ](../../strongs/h/h8252.md): they break forth into [rinnah](../../strongs/h/h7440.md).

<a name="isaiah_14_8"></a>Isaiah 14:8

Yea, the [bᵊrôš](../../strongs/h/h1265.md) [samach](../../strongs/h/h8055.md) at thee, and the ['erez](../../strongs/h/h730.md) of [Lᵊḇānôn](../../strongs/h/h3844.md), saying, Since thou art [shakab](../../strongs/h/h7901.md), no [karath](../../strongs/h/h3772.md) is come up against us.

<a name="isaiah_14_9"></a>Isaiah 14:9

[shĕ'owl](../../strongs/h/h7585.md) from beneath is [ragaz](../../strongs/h/h7264.md) for thee to [qārā'](../../strongs/h/h7125.md) thee at thy [bow'](../../strongs/h/h935.md): it [ʿûr](../../strongs/h/h5782.md) the [rᵊp̄ā'îm](../../strongs/h/h7496.md) for thee, even all the [ʿatûḏ](../../strongs/h/h6260.md) of the ['erets](../../strongs/h/h776.md); it hath [quwm](../../strongs/h/h6965.md) from their [kicce'](../../strongs/h/h3678.md) all the [melek](../../strongs/h/h4428.md) of the [gowy](../../strongs/h/h1471.md).

<a name="isaiah_14_10"></a>Isaiah 14:10

All they shall ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto thee, Art thou also become [ḥālâ](../../strongs/h/h2470.md) as we? art thou become [māšal](../../strongs/h/h4911.md) unto us?

<a name="isaiah_14_11"></a>Isaiah 14:11

Thy [gā'ôn](../../strongs/h/h1347.md) is [yarad](../../strongs/h/h3381.md) to the [shĕ'owl](../../strongs/h/h7585.md), and the [hemyâ](../../strongs/h/h1998.md) of thy [neḇel](../../strongs/h/h5035.md): the [rimmâ](../../strongs/h/h7415.md) is [yāṣaʿ](../../strongs/h/h3331.md) under thee, and the [tôlāʿ](../../strongs/h/h8438.md) [mᵊḵassê](../../strongs/h/h4374.md) thee.

<a name="isaiah_14_12"></a>Isaiah 14:12

How art thou [naphal](../../strongs/h/h5307.md) from [shamayim](../../strongs/h/h8064.md), [hêlēl](../../strongs/h/h1966.md), [ben](../../strongs/h/h1121.md) of the [šaḥar](../../strongs/h/h7837.md) [yālal](../../strongs/h/h3213.md)! how art thou [gāḏaʿ](../../strongs/h/h1438.md) to the ['erets](../../strongs/h/h776.md), which didst [ḥālaš](../../strongs/h/h2522.md) the [gowy](../../strongs/h/h1471.md)!

<a name="isaiah_14_13"></a>Isaiah 14:13

For thou hast ['āmar](../../strongs/h/h559.md) in thine [lebab](../../strongs/h/h3824.md), I will [ʿālâ](../../strongs/h/h5927.md) into [shamayim](../../strongs/h/h8064.md), I will [ruwm](../../strongs/h/h7311.md) my [kicce'](../../strongs/h/h3678.md) above the [kowkab](../../strongs/h/h3556.md) of ['el](../../strongs/h/h410.md): I will [yashab](../../strongs/h/h3427.md) also upon the [har](../../strongs/h/h2022.md) of the [môʿēḏ](../../strongs/h/h4150.md), in the [yᵊrēḵâ](../../strongs/h/h3411.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md):

<a name="isaiah_14_14"></a>Isaiah 14:14

I will [ʿālâ](../../strongs/h/h5927.md) above the [bāmâ](../../strongs/h/h1116.md) of the ['ab](../../strongs/h/h5645.md); I will be like the ['elyown](../../strongs/h/h5945.md).

<a name="isaiah_14_15"></a>Isaiah 14:15

Yet thou shalt be [yarad](../../strongs/h/h3381.md) to [shĕ'owl](../../strongs/h/h7585.md), to the [yᵊrēḵâ](../../strongs/h/h3411.md) of the [bowr](../../strongs/h/h953.md).

<a name="isaiah_14_16"></a>Isaiah 14:16

They that [ra'ah](../../strongs/h/h7200.md) thee shall [šāḡaḥ](../../strongs/h/h7688.md) upon thee, and [bîn](../../strongs/h/h995.md) thee, saying, Is this the ['iysh](../../strongs/h/h376.md) that made the ['erets](../../strongs/h/h776.md) to [ragaz](../../strongs/h/h7264.md), that did [rāʿaš](../../strongs/h/h7493.md) [mamlāḵâ](../../strongs/h/h4467.md);

<a name="isaiah_14_17"></a>Isaiah 14:17

That [śûm](../../strongs/h/h7760.md) the [tebel](../../strongs/h/h8398.md) as a [midbar](../../strongs/h/h4057.md), and [harac](../../strongs/h/h2040.md) the [ʿîr](../../strongs/h/h5892.md) thereof; that [pāṯaḥ](../../strongs/h/h6605.md) not the [bayith](../../strongs/h/h1004.md) of his ['āsîr](../../strongs/h/h615.md)?

<a name="isaiah_14_18"></a>Isaiah 14:18

All the [melek](../../strongs/h/h4428.md) of the [gowy](../../strongs/h/h1471.md), even all of them, [shakab](../../strongs/h/h7901.md) in [kabowd](../../strongs/h/h3519.md), every one in his own [bayith](../../strongs/h/h1004.md).

<a name="isaiah_14_19"></a>Isaiah 14:19

But thou art [shalak](../../strongs/h/h7993.md) of thy [qeber](../../strongs/h/h6913.md) like a [ta'ab](../../strongs/h/h8581.md) [nēṣer](../../strongs/h/h5342.md), and as the [lᵊḇûš](../../strongs/h/h3830.md) of those that are [harag](../../strongs/h/h2026.md), [ṭāʿan](../../strongs/h/h2944.md) with a [chereb](../../strongs/h/h2719.md), that [yarad](../../strongs/h/h3381.md) to the ['eben](../../strongs/h/h68.md) of the [bowr](../../strongs/h/h953.md); as a [peḡer](../../strongs/h/h6297.md) [bûs](../../strongs/h/h947.md).

<a name="isaiah_14_20"></a>Isaiah 14:20

Thou shalt not be [yāḥaḏ](../../strongs/h/h3161.md) with them in [qᵊḇûrâ](../../strongs/h/h6900.md), because thou hast [shachath](../../strongs/h/h7843.md) thy ['erets](../../strongs/h/h776.md), and [harag](../../strongs/h/h2026.md) thy ['am](../../strongs/h/h5971.md): the [zera'](../../strongs/h/h2233.md) of [ra'a'](../../strongs/h/h7489.md) shall never be [qara'](../../strongs/h/h7121.md).

<a name="isaiah_14_21"></a>Isaiah 14:21

[kuwn](../../strongs/h/h3559.md) [maṭbēaḥ](../../strongs/h/h4293.md) for his [ben](../../strongs/h/h1121.md) for the ['avon](../../strongs/h/h5771.md) of their ['ab](../../strongs/h/h1.md); that they do not [quwm](../../strongs/h/h6965.md), nor [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md), nor [mālā'](../../strongs/h/h4390.md) the [paniym](../../strongs/h/h6440.md) of the [tebel](../../strongs/h/h8398.md) with [ʿĀr](../../strongs/h/h6145.md) [ʿîr](../../strongs/h/h5892.md).

<a name="isaiah_14_22"></a>Isaiah 14:22

For I will [quwm](../../strongs/h/h6965.md) against them, saith [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), and [karath](../../strongs/h/h3772.md) from [Bāḇel](../../strongs/h/h894.md) the [shem](../../strongs/h/h8034.md), and [šᵊ'ār](../../strongs/h/h7605.md), and [nîn](../../strongs/h/h5209.md), and [neḵeḏ](../../strongs/h/h5220.md), saith [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_14_23"></a>Isaiah 14:23

I will also make it a [môrāš](../../strongs/h/h4180.md) for the [qipōḏ](../../strongs/h/h7090.md), and ['ăḡam](../../strongs/h/h98.md) of [mayim](../../strongs/h/h4325.md): and I will [ṭû'](../../strongs/h/h2894.md) it with the [maṭ'ăṭē'](../../strongs/h/h4292.md) of [šāmaḏ](../../strongs/h/h8045.md), saith [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="isaiah_14_24"></a>Isaiah 14:24

[Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) hath [shaba'](../../strongs/h/h7650.md), ['āmar](../../strongs/h/h559.md), Surely as I have [dāmâ](../../strongs/h/h1819.md), so shall it come to pass; and as I have [ya'ats](../../strongs/h/h3289.md), so shall it [quwm](../../strongs/h/h6965.md):

<a name="isaiah_14_25"></a>Isaiah 14:25

That I will [shabar](../../strongs/h/h7665.md) the ['Aššûr](../../strongs/h/h804.md) in my ['erets](../../strongs/h/h776.md), and upon my [har](../../strongs/h/h2022.md) [bûs](../../strongs/h/h947.md): then shall his [ʿōl](../../strongs/h/h5923.md) [cuwr](../../strongs/h/h5493.md) from off them, and his [sōḇel](../../strongs/h/h5448.md) [cuwr](../../strongs/h/h5493.md) from off their [šᵊḵem](../../strongs/h/h7926.md).

<a name="isaiah_14_26"></a>Isaiah 14:26

This is the ['etsah](../../strongs/h/h6098.md) that is [ya'ats](../../strongs/h/h3289.md) upon the ['erets](../../strongs/h/h776.md): and this is the [yad](../../strongs/h/h3027.md) that is [natah](../../strongs/h/h5186.md) upon all the [gowy](../../strongs/h/h1471.md).

<a name="isaiah_14_27"></a>Isaiah 14:27

For [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) hath [ya'ats](../../strongs/h/h3289.md), and who shall [pārar](../../strongs/h/h6565.md) it? and his [yad](../../strongs/h/h3027.md) is [natah](../../strongs/h/h5186.md), and who shall [shuwb](../../strongs/h/h7725.md)?

<a name="isaiah_14_28"></a>Isaiah 14:28

In the [šānâ](../../strongs/h/h8141.md) that [melek](../../strongs/h/h4428.md) ['Āḥāz](../../strongs/h/h271.md) [maveth](../../strongs/h/h4194.md) was this [maśśā'](../../strongs/h/h4853.md).

<a name="isaiah_14_29"></a>Isaiah 14:29

[samach](../../strongs/h/h8055.md) not thou, whole [pᵊlešeṯ](../../strongs/h/h6429.md), because the [shebet](../../strongs/h/h7626.md) of him that [nakah](../../strongs/h/h5221.md) thee is [shabar](../../strongs/h/h7665.md): for out of the [nachash](../../strongs/h/h5175.md) [šereš](../../strongs/h/h8328.md) shall [yāṣā'](../../strongs/h/h3318.md) a [ṣep̄aʿ](../../strongs/h/h6848.md), and his [pĕriy](../../strongs/h/h6529.md) shall be a [saraph](../../strongs/h/h8314.md) ['uwph](../../strongs/h/h5774.md).

<a name="isaiah_14_30"></a>Isaiah 14:30

And the [bᵊḵôr](../../strongs/h/h1060.md) of the [dal](../../strongs/h/h1800.md) shall [ra'ah](../../strongs/h/h7462.md), and the ['ebyown](../../strongs/h/h34.md) shall [rāḇaṣ](../../strongs/h/h7257.md) in [betach](../../strongs/h/h983.md): and I will [muwth](../../strongs/h/h4191.md) thy [šereš](../../strongs/h/h8328.md) with [rāʿāḇ](../../strongs/h/h7458.md), and he shall [harag](../../strongs/h/h2026.md) thy [šᵊ'ērîṯ](../../strongs/h/h7611.md).

<a name="isaiah_14_31"></a>Isaiah 14:31

[yālal](../../strongs/h/h3213.md), O [sha'ar](../../strongs/h/h8179.md); [zāʿaq](../../strongs/h/h2199.md), O [ʿîr](../../strongs/h/h5892.md); thou, [pᵊlešeṯ](../../strongs/h/h6429.md), art [mûḡ](../../strongs/h/h4127.md): for there shall [bow'](../../strongs/h/h935.md) from the [ṣāp̄ôn](../../strongs/h/h6828.md) an ['ashan](../../strongs/h/h6227.md), and none shall be [bāḏaḏ](../../strongs/h/h909.md) in his [môʿāḏ](../../strongs/h/h4151.md).

<a name="isaiah_14_32"></a>Isaiah 14:32

What shall one then ['anah](../../strongs/h/h6030.md) the [mal'ak](../../strongs/h/h4397.md) of the [gowy](../../strongs/h/h1471.md)? That [Yĕhovah](../../strongs/h/h3068.md) hath [yacad](../../strongs/h/h3245.md) [Tsiyown](../../strongs/h/h6726.md), and the ['aniy](../../strongs/h/h6041.md) of his ['am](../../strongs/h/h5971.md) shall [chacah](../../strongs/h/h2620.md) in it.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 13](isaiah_13.md) - [Isaiah 15](isaiah_15.md)